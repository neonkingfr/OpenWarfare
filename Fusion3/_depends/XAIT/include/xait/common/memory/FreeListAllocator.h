// (C) xaitment GmbH 2006-2012

#pragma once

#define XAIT_FREELIST_BLOCKSIZE		16384
#define XAIT_FREELIST_ALIGNMENT		4		// memory alignment of block start address, minimum is 4, must be power of 2 
#define XAIT_FREELIST_MAX_PREALLOC	256		// number of bytes until freelist blocks will be allocated, maximum is 65535, must be power of 2

#ifdef XAIT_USE_VLD
#ifndef XAIT_FREELIST_DIRECT_ALLOC
	#define XAIT_FREELIST_DIRECT_ALLOC		// if defined, the free list allocator will do a direct allocation for all block sizes (use in the GlobalCompileDefines)
#endif
#endif

#if XAIT_FREELIST_ALIGNMENT < 4
#error "Freelist alignment must be greater or equal 4"
#endif

#if XAIT_FREELIST_MAX_PREALLOC > 65536
#error "Preallocation size is too big"
#endif

#define XAIT_FREELIST_BLOCKSLOTS	(XAIT_FREELIST_MAX_PREALLOC/XAIT_FREELIST_ALIGNMENT)

#include <xait/common/memory/Allocator.h>
#include <xait/common/memory/ThreadSafeAllocWrapper.h>

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			//! The memory manager for the complete xaitengine dll. This manager manages freelists for objects of a sizes
			//! up to 255 bytes. For bigger objects the memory will be allocated directly by the allocator function submitted
			//! to the createInstance methode. The freelists will be allocated in blocks of MEMMANAGER_BLOCKSIZE. This reduces
			//! the amount of time for new allocation of free space. Since the memoryblocks are not used up completly there is
			//! always some extra memory which will not be used. The optimal value for MEMMANAGER_BLOCKSIZE depends on the
			//! application. For every requested block by alloc there is an overhead of one byte. For small request <= 4 bytes this
			//! overhead is computed by overhead = (pointersize + 1) - requestsize. This results from the implementation of the freelist.
			//! In 32-bit system the pointersize is 4 byte in 64-bit system the pointersize is 6(8). Freeing an allocated memory
			//! block will not result in a free by the memory manager. The manager will add this memory to the corresponding freelist.
			//! If you want to really free the memory use MemoryManager::release, but this procedure takes some time. Normally
			//! it is not required to release the memory before the end of the application except you know that a huge amount of 
			//! memory has been freed and you want to release the memory.
			//! The memory manager also redefines the global new and delete operator. This brings some attention with it. Since
			//! the new operator uses the memory manager for allocating memory, you cannot use new to instance the memory manager
			//! (chicken egg problem), use MemoryManager::createInstance instead. This has to be done at the very beginning since
			//! even a local creation of a class uses the new operator. In addition you have to pay attention to your global variables.
			//! DON'T USE NONPOINTER GLOBAL CLASSES. If you want to use a global class use a pointer to the class, since you can then control
			//! the creating time of the class. If you do not use a pointer, the class will be created before the memory manager 
			//! is initialized, but the global new operator is already overload by the memory manager version. So the new operator
			//! tries to use the uninitialized memory manager which will result in a segfault. If you get an error using the memory
			//! manager at the beginning of the application this is probably the source of the error.
			//! After all this pay attention thing, there is one good news, the measured performance gain is up to 20x (will raise with
			//! the amount of alloc and free calls).
			//! \remark DO NOT USE NONPOINTER GLOBAL CLASSES, this WILL result in a segfault. Use pointer to global classes.
			//! \brief The memory manager class
			class FreeListAllocator : public Memory::Allocator
			{
			public:
				typedef Ptr::Rebind<FreeListAllocator>::Other		Ptr;
				typedef ThreadSafeAllocWrapper<FreeListAllocator>	ThreadSafe;

				//! \brief Create a new free list allocator
				//! \param alloc	allocator which should be used for internal memory allocation
				XAIT_COMMON_DLLAPI FreeListAllocator(const Memory::Allocator::Ptr& alloc);

				//! \brief destructor
				~FreeListAllocator();

				//! \brief check if the allocator is thread safe
				virtual bool isThreadSafe() const
				{
					return false;
				}


				//! \brief release the unused memory of the memory manager
				//! \returns amount of unreleased memory bytes
				uint64 release();

				//! \brief destroy the complete memory, use with care
				void destroy();

			protected:
				//! \brief allocate a memoryblock of a defined size of bytes
				//! \returns a pointer to the memory block
				//! \exception OutOfMemoryException		throws if no memory available
				virtual void* allocInternal(const uint32 numBytes);

				//! \brief free previously allocated memory
				//! \exception ArgumentNullException	throws if ptr is a null pointer
				virtual void freeInternal(void* ptr);

			private:
				struct AllocBlockHeader 
				{
					AllocBlockHeader*		mNext;			//!< next allocated block
					uint16					mNumElements;	//!< number of element blocks
					uint16					mBlockSlot;		//!< slot of the memtable where this block has been inserted
				};

				//struct FreeListItem
				//{
				//	AllocBlockHeader*		mParent;
				//	union 
				//	{
				//		FreeListItem*			mNext;
				//		void*					mBlockStart;
				//	};
				//};

				typedef void*	Pointer;				//!< typedef for pointer, should have the size of the pointer in the system


				Pointer*			mMemTable[XAIT_FREELIST_BLOCKSLOTS];	//!< memory table with free memory blocks
				AllocBlockHeader*	mAllocBlocks;							//!< allocated blocks during memory manager runtime, needed for correct freeing
				Allocator::Ptr		mAllocator;

				//! Allocates a memory block that holds several items of elementSize size. This method also generates a new
				//! freelist for this block and adds the freelist items to the corresponding entry in the memTable
				//! \brief allocate a new memoryblock for several items
				//! \param elementSize	the size of the elements in the block
				void allocBlock(const uint16 blockSlot);
			};
		}
	}
}

