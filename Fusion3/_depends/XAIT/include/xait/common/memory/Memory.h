// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/container/Vector.h>
#include <memory>

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			template<typename T>
			inline void MemCopy(T* dst, const T* src, const uint32 numElements)
			{
				memcpy(dst,src,sizeof(T) * numElements);
			}

			template<typename T, typename T_ALLOC>
			inline void MemCopy(T* dst, const Container::Vector<T,T_ALLOC>& src)
			{
				if (!src.empty())
					memcpy(dst,&src[0],sizeof(T) * src.size());
			}

			template<typename T>
			inline void MemSet(T* dst, const T val, const uint32 numElements)
			{
				for(uint32 i= 0; i < numElements; ++i)
				{
					dst[i]= val;
				}
			}

			template<typename T>
			inline void MemZero(T* dst, const uint32 numElements)
			{
				memset(dst,0,sizeof(T) * numElements);
			}
		}
	}
}

