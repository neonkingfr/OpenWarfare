// (C) xaitment GmbH 2006-2012

#pragma once

//////////////////////////////////////////////////////////////////////////
///						Line Intersection Functions

#include <xait/common/DLLDefines.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/geom/Triangle3D.h>

namespace XAIT
{
	namespace Common
	{
			namespace Geom
			{
				namespace Intersect
				{
					//! test if two lines intersect
					//! algorithmen from http://astronomy.swin.edu.au/~pbourke/geometry/lineline2d/
					//! \brief test if two lines intersect
					//! \param a1	start of line1
					//! \param a2	end of line1
					//! \param b1	start of line2
					//! \param b2	end of line2
					//! \returns true if lines intersect
					XAIT_COMMON_DLLAPI bool LineLine(const Math::Vec2f& a1, const Math::Vec2f& a2, const Math::Vec2f& b1, const Math::Vec2f& b2);

					//! compute intersection point of two lines
					//! algorithm from http://astronomy.swin.edu.au/~pbourke/geometry/lineline2d/
					//! the function returns in ua and ub two values, that lie between 0 and 1 if a line has an intersection.
					//! You can compute the intersection point by  inter = a1 + ua * (a2 - a1) , same for line b with ub.
					//! \brief compute intersection point of two lines
					//! \param ua	intersection scale for line a
					//! \param ub	intersection scale for line b
					//! \param a1	start of line1
					//! \param a2	end of line1
					//! \param b1	start of line2
					//! \param b2	end of line2
					//! \returns true if lines intersect (only then ua and ub are valid)
					XAIT_COMMON_DLLAPI bool LineLine(float32& ua, float32& ub, const Math::Vec2f& a1, const Math::Vec2f& a2, const Math::Vec2f& b1, const Math::Vec2f& b2);

					//! test if a ray and a line intersect
					//! algorithm from http://astronomy.swin.edu.au/~pbourke/geometry/lineline2d/
					//! \brief test if a ray and a line intersect
					//! \param t		distance of hitpoint from start in multiples of dir (invalid value if the function returns false)
					//! \param start	start of ray
					//! \param dir		direction of the ray (should not be of length zero), must not be normalized
					//! \param lineA	start of line
					//! \param lineB	end of line
					//! \returns true if ray intersects the line
					XAIT_COMMON_DLLAPI bool LineRay(float32& t, const Math::Vec2f& start, const Math::Vec2f& dir, const Math::Vec2f& lineA, const Math::Vec2f& lineB);
					XAIT_COMMON_DLLAPI bool LineRay(float64& t, const Math::Vec2d& start, const Math::Vec2d& dir, const Math::Vec2d& lineA, const Math::Vec2d& lineB);

					//! \brieftest if a ray and a plane intersect
					//! \param intersectionPoint	the intersection point of the line with the plane (invalid value if the function returns false or the ray lies on the plane)
					//! \param linePoint			point on the line
					//! \param lineDirection		direction of the line (should not be of length zero), must not be normalized
					//! \param planePoint			a point on the plane
					//! \param u					vector on the plane
					//! \param v					second vector on the plane (not parallel to u)
					//! \returns true if ray intersects the line or the ray lies on the plane
					XAIT_COMMON_DLLAPI bool RayPlane(Math::Vec3f& intersectionPoint, const Math::Vec3f& linePoint, const Math::Vec3f& lineDirection, const Math::Vec3f& planePoint, const Math::Vec3f& u, const Math::Vec3f& v);

					//! \brief test if a line and a plane intersect
					//! \param intersectionPoint	if intersect, the intersection point
					//! \returns true if the line and the plane intersect and the intersectionPoint contains the intersection position
					XAIT_COMMON_DLLAPI bool LinePlane(Math::Vec3f& intersectionPoint, const Math::Vec3f& lineA, const Math::Vec3f& lineB, const Math::Vec3f& planePoint, const Math::Vec3f& planeNormal );
				}	// namespace Intersect
			}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT
