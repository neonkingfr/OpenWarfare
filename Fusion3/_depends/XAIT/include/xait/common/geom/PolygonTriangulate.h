// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/DLLDefines.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/geom/Triangle3D.h>
#include <xait/common/geom/GeomUtility.h>

namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			class PolygonTriangulate
			{
				Common::Memory::Allocator::Ptr	mAlloc;
				float*							mCosts;
				int32*							mMin;
				int32							mCostsReservedSize;
				int32*							mTriangulation;
				uint32							mCurrTriID;
				bool							mHasCollinearVertices;

			public:
				XAIT_COMMON_DLLAPI PolygonTriangulate();
				XAIT_COMMON_DLLAPI PolygonTriangulate(const Common::Memory::Allocator::Ptr& alloc);
				XAIT_COMMON_DLLAPI ~PolygonTriangulate();

				//! Enables additional checks that are necessary if convex polygons can contain collinear vertices
				void setHasCollinearVertices(const bool enable) { mHasCollinearVertices= enable; }
				bool getHasCollinearVertices() const { return mHasCollinearVertices; }


				//! create a triangulation from a convex polygon
				//! computes a minimal triangulation
				//! the triangles will be returned in triangulation as a sequence of 3 vertexindices foreach triangle
				//! use triangulation algo from http://www.cs.utoronto.ca/~heap/Courses/270F02/A4/chains/node2.html
				//! \param triangulation	array that will receive the triangulation in 3-pair vertex indices. The array must be size (numEdges - 2) * 3
				//! \param polygon			convex polygon
				//! \param numEdges			number of edges (vertices) in the convex polygon
				//! \param vertices			vertex array that suites to the vertex indices in the polygon array
				//! \returns number of triangles
				XAIT_COMMON_DLLAPI int createOptimal(int32* triangulation, const int32* polygon, const int32 numEdges, const Common::Math::Vec3f* vertices);

				//! \brief creates optimal triangulation like createOptimal, assumes that the vertices form a polygon
				XAIT_COMMON_DLLAPI int createOptimal(int32* triangulation, const Common::Math::Vec3f* vertices, const uint32 numVertices);

				inline int getNumOptimalTriIDs(const int32 numEdges) const
				{
					return (numEdges - 2) * 3;
				}

			private:
				void initCostMatrix(const int32 numEdges);

				// T_VACCESS must provide a [] operator that maps vertex id to vertex position - Vec3f::operator[](int id)
				template<typename T_VACCESS>
				float computeTriCosts(const int32 i, const int32 k, const int32 n, const T_VACCESS& vAccess)
				{
					const int32 index= i + k * n;

					if (mCosts[index] >= 0) return mCosts[index];

					// compute tricost
					if (k < i + 2) 
					{
						mCosts[index]= 0.0f;
						return 0.0f;
					} 
					else 
					{
						mCosts[index]= (float32)X_POS_INFINITY_F32;
						for(int32 j= i + 1; j < k; ++j) 
						{
							const float32 c= computeTriCosts(i,j,n,vAccess)
								+ computeTriCosts(j,k,n,vAccess)
								+ computeSingleTriangleCost(vAccess[i],vAccess[j],vAccess[k]);
							if (c < mCosts[index]) 
							{
								mCosts[index]= c;
								mMin[index]= j;
							}
						}
						return mCosts[index];
					}
				}

				float32 computeSingleTriangleCost(const Common::Math::Vec3f& a, const Common::Math::Vec3f& b, const Common::Math::Vec3f& c)
				{
					if (mHasCollinearVertices && Triangle3Df::isDegenerated(a,b,c))
						return X_POS_INFINITY_F32;

					return Utility::ComputeTrianglePerimeter(a,b,c);
				}

				// T_VIDACCESS must provide a [] operator that maps index position in polygon to vertex id - int operator[](int id)
				template<typename T_VIDACCESS>
				void createTriangles(const int32 i, const int32 k, const int32 n, const T_VIDACCESS& vIDAccess)
				{
					const int32 minval= mMin[i + n * k];

					if (minval >= 0) 
					{
						mTriangulation[mCurrTriID++]= vIDAccess[i];
						mTriangulation[mCurrTriID++]= vIDAccess[minval];
						mTriangulation[mCurrTriID++]= vIDAccess[k];

						createTriangles(i,minval,n,vIDAccess);
						createTriangles(minval,k,n,vIDAccess);
					}
				}
				
			};
		}
	}
}

