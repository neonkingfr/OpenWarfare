// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/math/MathDefines.h>
#include <xait/common/math/ValueFunc.h>
#include <xait/common/geom/AABB2D.h>
#include <xait/common/geom/GeomUtility.h>


namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			template<typename T_COMP>
			struct Triangle2D;

			typedef Triangle2D<float32>		Triangle2Df;	//!< always 32-bit
			typedef Triangle2D<float32>		Triangle2Dd;	//!< always 64-bit

			//! \brief template for a triangle in 3D space
			template<typename T_COMP>
			struct Triangle2D
			{
				Math::Vec2<T_COMP>	mA;	
				Math::Vec2<T_COMP>	mB;	
				Math::Vec2<T_COMP>	mC;

				//! \brief compute area of this triangle
				inline float32 getArea() const
				{
					return getArea(mA,mB,mC);
				}

				//! \brief test if this triangle is degenerated
				inline bool isDegenerated() const
				{
					return isDegenerated(mA,mB,mC);
				}

				//! \brief computes bounding box of the triangle
				AABB2D<T_COMP>  computeBoundingBox() const;

				//! \brief checks if a point is inside the triangle
				//! \param point		the point that is checked
				//! \param clockWise	if true triangle is clockwise, if false triangle is anti-clockwise
				//! \returns true if the point is inside; false otherwise
				bool isPointInside(const Common::Math::Vec2<T_COMP>& point, bool clockWise)
				{
					
					// This algorithm checks if the specified point is on the same side 
					// of all triangle line segments. If this is the case, the point is
					// inside. Further we accept points that are on polygon line segments
					// as points inside the triangle.
					float32 clockWiseSwitch= 1.0f;
					if (!clockWise)
					{
						clockWiseSwitch= -1.0f;
					}

					// check each segment
					
					float32 det= Geom::Utility::OrientationDeterminant(mA,mB,point);
					
					// we handle points on the line as points inside the triangle
					// therefore we check for valid det <= 0 

					// we accept some error of points witch are on the right side of the line
					// but inorder to test, we have to compute the distance to the point
					// thus we have to normalize the determinate
					if ((det * clockWiseSwitch) > 0)
					{
						const float32 d2= (det * det) / (mB-mA).getLength2();
						if (d2 > X_EPSILON_F)
							return false;
					}

					det= Geom::Utility::OrientationDeterminant(mB,mC,point);
					if ((det * clockWiseSwitch) > 0)
					{
						const float32 d2= (det * det) / (mC-mB).getLength2();
						if (d2 > X_EPSILON_F)
							return false;
					}

					det= Geom::Utility::OrientationDeterminant(mC,mA,point);
					if ((det * clockWiseSwitch) > 0)
					{
						const float32 d2= (det * det) / (mA-mC).getLength2();
						if (d2 > X_EPSILON_F)
							return false;
					}					
					
					 
					return true;
				}

				//! \brief compute the area of a 2d triangle
				//! \param a	node a of triangle
				//! \param b	node b of triangle
				//! \param c	node c of triangle
				//! \returns the size of the triangle area
				//! \remark nodes can be in cw or ccw direction
				static T_COMP getArea(const Math::Vec2<T_COMP>& a, const Math::Vec2<T_COMP>& b, const Math::Vec2<T_COMP>& c);

				//! \brief check if 2d triangle is degenerated
				//! \param a			node a of triangle
				//! \param b			node b of triangle
				//! \param c			node c of triangle
				//! \param minAngle		minimum angle that is allowed that the triangle is not degenerated in radians
				//! \returns true if the triangle is degenerated
				static bool isDegenerated(const Math::Vec2<T_COMP>& a, const Math::Vec2<T_COMP>& b, const Math::Vec2<T_COMP>& c);

			};	// struct Triangle2D

			template<typename T_COMP>
			inline T_COMP XAIT::Common::Geom::Triangle2D<T_COMP>::getArea( const Math::Vec2<T_COMP>& a, const Math::Vec2<T_COMP>& b, const Math::Vec2<T_COMP>& c )
			{
				Math::Vec2<T_COMP> u;
				Math::Vec2<T_COMP> v;

				u= b - a;
				v= c - a;

				return (T_COMP)0.5 * Math::Abs(u.getDeterminant(v));
			}

			template<typename T_COMP>
			inline bool XAIT::Common::Geom::Triangle2D<T_COMP>::isDegenerated( const Math::Vec2<T_COMP>& a, const Math::Vec2<T_COMP>& b, const Math::Vec2<T_COMP>& c )
			{
				Math::Vec2<T_COMP> u;
				Math::Vec2<T_COMP> v;
				 
				u= b - a;
				v= c - a;

				return Math::Abs(u.getDeterminant(v)) < (T_COMP)(2.0 * X_EPSILON);
			}

			template<typename T_COMP>
			AABB2D<T_COMP>  XAIT::Common::Geom::Triangle2D<T_COMP>::computeBoundingBox() const 
			{
				AABB2D<T_COMP> boundingBox;
				boundingBox.init();
				boundingBox.extend(mA);
				boundingBox.extend(mB);
				boundingBox.extend(mC);

				return boundingBox;
			}
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT

