// (C) xaitment GmbH 2006-2012

#pragma once

// some often needed utility functions
#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/container/Vector.h>
#include <xait/common/container/RingList.h>
#include <xait/common/geom/IndexedTriangle.h>


namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			namespace Utility
			{
				//! \brief rotates a source vector to a destination vector, but only a maximum angle
				//! \param smoothDir	rotated angle, can be the same variable as the srcDir or dstDir (has unitlength)
				//! \param srcDir		initial vector, which should be rotated (must be unitlength)
				//! \param dstDir		direction in which the srcDir should be rotated (must be unitlength)
				//! \param maxAngle		maximum angle, the srcDir can be rotated (in radians)
				void SmoothDirection(Math::Vec3f& smoothDir, const Math::Vec3f& srcDir, const Math::Vec3f& dstDir, const float32 maxAngle);


				//! \brief compute two arbitray axis in the plane, which form a 2D coordinate system
				//! \param xAxis	computed xAxis
				//! \param yAxis	computed yAxis
				//! \param normal	normal of the plane
				void XAIT_COMMON_DLLAPI XAIT_CDECL GetPlaneCoordSystem(Math::Vec3f& xAxis, Math::Vec3f& yAxis, const Math::Vec3f& normal);

				//! \brief transform a 3d point into the room of axis_x and axis_y
				//! \param point2D	returned projected point in 2d
				//! \param point3D	point which should be transformed
				//! \param axis_x	x - axis for the 2D room
				//! \param axis_y	y - axis for the 2D room
				//! \remark axis_x and axis_y must be orthogonal and normalized
				static inline void TransformPoint2D(Math::Vec2f& point2D, const Math::Vec3f& point3D, const Math::Vec3f& axis_x , const Math::Vec3f& axis_y)
				{
					point2D.mX= axis_x.dotProduct(point3D);
					point2D.mY= axis_y.dotProduct(point3D);
				}


				//! \brief test if a 3d point is on a line
				//! \param a		startpoint of line
				//! \param b		endpoint of line
				//! \param point	3d point to test against line
				//! \param maxDist	maximum distance the point can have to the line
				//! \returns true if the point is within the required maxDist, else false
				bool IsPointOnLine3D(const Math::Vec3f& a, const Math::Vec3f& b, const Math::Vec3f& point, float32 maxDist);

				//! \brief test if a 2d point is on a line
				//! \param a		startpoint of line
				//! \param b		endpoint of line
				//! \param point	2d point to test against line
				//! \param maxDist	maximum distance the point can have to the line
				//! \returns true if the point is within the required maxDist, else false
				XAIT_COMMON_DLLAPI bool IsPointOnLine2D(const Math::Vec2f& a, const Math::Vec2f& b, const Math::Vec2f& point, float32 maxDist);

				//! \brief test if a 2d point is on a line
				//! \param a		startpoint of line
				//! \param b		endpoint of line
				//! \param point	2d point to test against line
				//! \returns the distance point->line
				XAIT_COMMON_DLLAPI float32 DistancePointToLine2D(const Math::Vec2f& a, const Math::Vec2f& b, const Math::Vec2f& point);

				//! \brief compute the distance a 3d point is on a line
				//! \param nearestPoint		the nearest Point to the given point on the ray
				//! \param a				startpoint of ray
				//! \param b				endpoint of ray
				//! \param point			3d point to t est against ray
				//! \returns the distance point->line
				XAIT_COMMON_DLLAPI float32 DistancePointToRay3D(Math::Vec3f& nearestPoint, const Math::Vec3f& rayStart, const Math::Vec3f& rayEnd, const Math::Vec3f& point);

				//! \brief Computes the distance of a point to plane
				//! \returns distance of the point to the plane, can be negative if point is below plane
				template<typename T_COMP>
				inline T_COMP DistancePointPlane(const Math::Vec3<T_COMP>& point, const Math::Vec3<T_COMP>& planePoint, const Math::Vec3<T_COMP>& planeNormal)
				{
					const Math::Vec3<T_COMP> dist3D= point - planePoint;
					return planeNormal.dotProduct(dist3D);
				}

				//! \brief compute angle in b for abc in radians (for 2d coordinates)
				//! \param a	node a
				//! \param b	node b
				//! \param c	node c
				//! \returns the angle in radians
				float32 ComputeAngleABC2D(const Math::Vec2f& a, const Math::Vec2f& b, const Math::Vec2f& c);

				//! \brief compute angle in b for abc in radians (for 3d coordinates)
				//! \param a	node a
				//! \param b	node b
				//! \param c	node c
				//! \returns the angle in radians
				float32 ComputeAngleABC3D(const Math::Vec3f& a, const Math::Vec3f& b, const Math::Vec3f& c);

				//! \brief compute the angle between two normalized vectors a and b (for 2d vectors)
				//! \param a	vector a normalized
				//! \param b	vector b normalized
				//! \returns The angle between a and b in the range of ]-PI,+PI]. If the angle is
				//!			smaller zero, vector b points in the right halfspace of vector a, and if
				//!			the angle is greater zero, vector b points in the left halfspace of vector a.
				template<typename NUMBER_T>
				NUMBER_T ComputeNormVecAngle2D(const Math::Vec2<NUMBER_T>& a, const Math::Vec2<NUMBER_T>& b)
				{
					Math::Vec2<NUMBER_T> aPerp;		// perpendicular vector to a
					a.computePerpendicular(aPerp);

					const NUMBER_T detAB= a.getDeterminant(b);
					const NUMBER_T detAPerpB= aPerp.getDeterminant(b);

					if (detAPerpB < 0)
					{
						if (detAB < 0)
						{
							return (NUMBER_T)-X_HALFPI + (NUMBER_T)asin(detAPerpB);
						}
						else
						{
							return (NUMBER_T)X_HALFPI - (NUMBER_T)asin(detAPerpB);
						}
					}
					else
					{
						return (NUMBER_T)asin(detAB);
					}
				}

				//! compute determinant for orientation purpose
				//! formula taken from DSA Script WS03/04 , Geometry Algorithmen , 1 Convex Hull
				//! the returned value D can be used as follow:
				//! if (D > 0) then p,q,r form a left turn
				//! if (D = 0) then p,q,r are collinear
				//! if (D < 0) then p,q,r from a right turn
				//! \brief compute determinant for orientation purpose
				//! \returns determinant value D
				template<typename T_NUMBER>
				static inline T_NUMBER OrientationDeterminant(const Math::Vec2<T_NUMBER>& p, const Math::Vec2<T_NUMBER>& q, const Math::Vec2<T_NUMBER>& r)
				{
					return (p.mX * (q.mY - r.mY) + q.mX * (r.mY - p.mY) + r.mX * (p.mY - q.mY));
				}

				//! \brief remove polygon nodes on straight border lines
				//! \param polygon		polygon to reduce
				//! \param angleEps		allowed error in angle (radians)
				template<typename T_POLYEDGE, typename F_GETVEC3F>
				void RemoveStraightPolygonNodes3D(Container::Vector<T_POLYEDGE>& _polygon, const float32 angleEps)
				{
					// remove parallel edges
					Container::RingList<T_POLYEDGE> polygon(_polygon.getAllocator());

					// convert array polygon into ringlist polygon
					const uint32 numPolygon= (uint32)_polygon.size();
					for(uint32 i= 0; i < numPolygon; ++i)
					{
						polygon.add(_polygon[i]);
					}

					if (polygon.size() < 3) 
					{
						X_ASSERT(polygon.size() >= 3);
						return;
					}				

					typename Container::RingList<T_POLYEDGE>::Iterator iter= polygon.begin();
					typename Container::RingList<T_POLYEDGE>::Iterator end= iter;
					typename Container::RingList<T_POLYEDGE>::Iterator prev;
					typename Container::RingList<T_POLYEDGE>::Iterator next;

					Math::Vec3f edgePrev;
					Math::Vec3f edgeNext;
					const float32 cosMaxAngle= cosf((float32)X_PI - angleEps);

					do 
					{
						prev= iter - 1;
						next= iter + 1;

						edgePrev= F_GETVEC3F::convertToVec3f(*prev) - F_GETVEC3F::convertToVec3f(*iter);
						edgeNext= F_GETVEC3F::convertToVec3f(*next) - F_GETVEC3F::convertToVec3f(*iter);

						edgeNext.normalize();
						edgePrev.normalize();

						const float32 dot= edgePrev.dotProduct(edgeNext);

						if (dot <= cosMaxAngle)
						{
							// stop if polygon is no longer greater than a triangle
							if (polygon.size() <= 3)
							{
								// clear polygon since it is now invalid
								polygon.clear();
								break;
							}

							// fix endcondition
							// iter could only be end, in the first iteration, since this will be checked by the while condition
							// so if iter equals end in the first iteration move back end condition one position
							if (iter == end)	
							{
								end= prev;
							}

							// remove iter node, since it forms a parallel with in- and outedge
							polygon.erase(iter);

							iter= next;
						}
						else
						{
							++iter;
						}
					} while (iter != end);

					if (!polygon.empty()) 
					{
						// test for parallels in endcondition, since this could be not tested
						prev= iter - 1;
						next= iter + 1;

						edgePrev=  F_GETVEC3F::convertToVec3f(*prev) -  F_GETVEC3F::convertToVec3f(*iter);
						edgeNext=  F_GETVEC3F::convertToVec3f(*next) -  F_GETVEC3F::convertToVec3f(*iter);

						edgeNext.normalize();
						edgePrev.normalize();

						const float32 dot= edgePrev.dotProduct(edgeNext);

						if (dot <= cosMaxAngle)
						{
							// stop if polygon is no longer greater than a triangle
							if (polygon.size() <= 3)
							{
								// clear polygon since it is now invalid
								polygon.clear();
							}
							else
							{
								// remove iter node, since it forms a parallel with in- and outedge
								polygon.erase(iter);
							}
						}
					}

					// convert back the ringlist polygon into the static array polygon
					_polygon.resize(polygon.size());
					if (polygon.empty()) return;

					iter= polygon.begin();
					end= iter;
					uint32 i= 0;
					do
					{
						_polygon[i]= *iter;
						++iter;
						++i;
					}
					while (iter != end);
				}

				struct ConvertToVec3f
				{
					static inline const Math::Vec3f& convertToVec3f(const Math::Vec3f& source)
					{
						return source;
					}
				};

				inline void RemoveStraightPolygonNodes3D(Container::Vector<Math::Vec3f>& _polygon, const float32 angleEps)
				{
					RemoveStraightPolygonNodes3D<Math::Vec3f,ConvertToVec3f>(_polygon,angleEps);
				}


				//! \brief remove polygon nodes on straight border lines
				//! \param polygon		polygon to reduce
				//! \param angleEps		allowed error in angle (radians)
				void RemoveStraightPolygonNodes3D(Container::RingList<Math::Vec3f>& polygon, const float32 angleEps);

				//! \brief compute voxels on a 3d line from a to b using 3d-dda
				//! \param voxels		all voxels on the line from a to b where a voxel has its lower bound at 
				//!						voxel[i] and its upper bound at (voxel[i] + (1,1,1))
				//! \param a			start of the line (must be a positive value)
				//! \param b			end of the line (must be a positive value)
				void ComputeVoxelOnLine3D(Container::Vector<Math::Vec3u32>& voxels, const Math::Vec3f& a, const Math::Vec3f& b);

				//! \brief compute perimeter of triangle
				//! \param a		node a of triangle
				//! \param b		node b of triangle
				//! \param c		node c of triangle
				//! \returns the length of the triangle perimeter
				float ComputeTrianglePerimeter(const Math::Vec3f& a, const Math::Vec3f& b, const Math::Vec3f& c);


		
				//! \brief compute normals for a mesh 
				//! \param normals		computed normals
				//! \param vertices		vertices of the mesh
				//! \param triangles	triangles of the mesh in ccw order
				void XAIT_COMMON_DLLAPI XAIT_CDECL ComputeMeshNormals(Container::Vector<Math::Vec3f>& normals, const Container::Vector<Math::Vec3f>& vertices, const Container::Vector<IndexedTriangle>& triangles);

			

				//! \brief Map a 3D point on an arbitrary defined plane
				class ArbitraryPlane2DMapping
				{
					Math::Vec3f	mXAxis;
					Math::Vec3f	mYAxis;
				public:
					//! \brief create a mapping with a defined coordinate system by xAxis and yAxis
					//! \param xAxis	x axis of the 2D coordinate system in the 3d room
					//! \param yAxis	y axis of the 2D coordinate system in the 3d room
					ArbitraryPlane2DMapping(const Math::Vec3f& xAxis, const Math::Vec3f& yAxis)
						: mXAxis(xAxis),mYAxis(yAxis)
					{}

					//! \brief create a mapping for plane defined by its normal
					//! \param normal	normal of the plane
					//! \remark The coordinate system will be a right handed one
					ArbitraryPlane2DMapping(const Math::Vec3f& normal)
					{
						GetPlaneCoordSystem(mXAxis,mYAxis,normal);
					}


					inline Math::Vec2f operator()(const Math::Vec3f& source) const
					{
						Math::Vec2f projected;
						TransformPoint2D(projected,source,mXAxis,mYAxis);
						return projected;
					}
				};

				//! \brief Map a 3D point to a 2D point by simply removing one component
				class Component2DMapping
				{
					uint32	mCompRemoved;

				public:
					Component2DMapping(const uint32 compRemoved)
						: mCompRemoved(compRemoved)
					{}

					inline Math::Vec2f operator()(const Math::Vec3f& source) const
					{
						return source.reduceDimensions(mCompRemoved);
					}
				};

				
				template<template <typename> class T_CONTAINER, typename T_COMP, typename T_ID>
				class IndexedAccessWrapper
				{
					const T_CONTAINER<T_COMP>&	mData;
					const T_CONTAINER<T_ID>&	mIndexArray;
				public:
					IndexedAccessWrapper(const T_CONTAINER<T_COMP>& data, const T_CONTAINER<T_ID>& indexArray)
						: mData(data),mIndexArray(indexArray)
					{}

					inline T_COMP& operator[](const T_ID id)
					{
						return mData[mIndexArray[id]];
					}

					inline const T_COMP& operator[](const T_ID id) const
					{
						return mData[mIndexArray[id]];
					}

					inline uint32 size() const
					{
						return mIndexArray.size();
					}
				};

				template<template <typename ELEM> class T_CONTAINER, typename T_COMP, typename T_ID>
				inline static const IndexedAccessWrapper<T_CONTAINER,T_COMP,T_ID> GetIndexAccessWrapper(const T_CONTAINER<T_COMP>& data, const T_CONTAINER<T_ID>& indexArray)
				{
					return IndexedAccessWrapper<T_CONTAINER,T_COMP,T_ID>(data,indexArray);
				}


				//! Test if a 3d point is in the cylinder going through a ground figure of a convex polygon
				//! \brief test if a 3d point is on a convex 3d polygon
				//! \param point			point to test
				//! \param polygon			convex polygon, order depends on order parameter
				//! \param clockWise		if true convex polygon is clockwise, if false convex polygon is anti-clockwise
				//! \param func2DMapping	functional mapping from 3D point to 2D points
				//! \param T_POLYGON		must be a class that has an index operator that delivers a Vec3f and a size() method that delivers the number of indices.
				//! \param F_2DMAPPING		must be a class that has a conversion from Vec3f to Vec2f : Vec2f operator()(Vec3f) 
				template<typename T_POLYGON, typename F_2DMAPPING>
				bool IsPointInsideConvexPoly3D(const Math::Vec3f& point, const T_POLYGON& polygon, bool clockWise, const F_2DMAPPING& func2DMapping)
				{
					// This algorithm checks if the specified point is on the same side 
					// of all polygon line segments. If this is the case, the point is
					// inside. Further we accept points that are on polygon line segments
					// as points inside the polygon.

					const Math::Vec2f point2D(func2DMapping(point));
					const uint32 n= polygon.size();
					float32 clockWiseSwitch= 1.0f;
					if (!clockWise)
					{
						clockWiseSwitch= -1.0f;
					}

					// initial compute a and b, so that we can reuse a as b in the next iteration
					// for the initial segment we use the last segment n-1 -> 0 . This way
					// we can prevent the modulo operation in the for loop.
					Math::Vec2f a= func2DMapping(polygon[n - 1]);	// start of line segment
					Math::Vec2f b= func2DMapping(polygon[0]);	// end of line segment
					float32 det= OrientationDeterminant(a,b,point2D);

					// we handle points on the line as points inside the polygon
					// therefore we check for valid det <= 0 
					if ((det * clockWiseSwitch) > 0)
					{
						return false;
					}

					for(uint32 i= 1; i < n; ++i)
					{
						a= b;
						b= func2DMapping(polygon[i]);
						det= OrientationDeterminant(a,b,point2D);

						// we handle points on the line as points inside the polygon
						// therefore we check for valid det <= 0 
						if ((det * clockWiseSwitch) > 0)
						{
							return false;
						}
					}
					
					return true;
				}

				//! \brief Test if a point is in a convex polygon
				//! \param point			point to test
				//! \param polygon			convex polygon, order depends on order parameter
				//! \param clockWise		if true convex polygon is clockwise, if false convex polygon is anti-clockwise
				//! \remark This version is considerable faster than the 3D version as there must be no point conversion done.
				template<typename T_POLYGON>
				bool IsPointInsideConvexPoly2D(const Math::Vec2f& point2D, const T_POLYGON& polygon, bool clockWise)
				{
					// This algorithm checks if the specified point is on the same side 
					// of all polygon line segments. If this is the case, the point is
					// inside. Further we accept points that are on polygon line segments
					// as points inside the polygon.
					const uint32 n= polygon.size();
					float32 clockWiseSwitch= 1.0f;
					if (!clockWise)
					{
						clockWiseSwitch= -1.0f;
					}

					// initial compute a and b, so that we can reuse a as b in the next iteration
					// for the initial segment we use the last segment n-1 -> 0 . This way
					// we can prevent the modulo operation in the for loop.
					Math::Vec2f a= polygon[n - 1];	// start of line segment
					Math::Vec2f b= polygon[0];	// end of line segment
					float32 det= OrientationDeterminant(a,b,point2D);

					// we handle points on the line as points inside the polygon
					// therefore we check for valid det <= 0 
					if ((det * clockWiseSwitch) > 0)
					{
						return false;
					}

					for(uint32 i= 1; i < n; ++i)
					{
						a= b;
						b= polygon[i];
						det= OrientationDeterminant(a,b,point2D);

						// we handle points on the line as points inside the polygon
						// therefore we check for valid det <= 0 
						if ((det * clockWiseSwitch) > 0)
						{
							return false;
						}
					}

					return true;
				}

				//! \brief Interset a ray with a plane
				//! \param rayLength		length of the ray, if intersection occurs rayLength will be cut to the length of the intersection, otherwise untouched
				//! \param rayOrigin		start of the ray
				//! \param rayDirection		normalized direction of the ray
				//! \param planePoint		arbitrary point on the plane
				//! \param planeNormal		normal of the plane
				//! \returns True if the ray intersects the plane or false if not.
				XAIT_COMMON_DLLAPI bool IntersectRayPlane(float32& rayLength, const Math::Vec3f& rayOrigin, const Math::Vec3f& rayDirection, const Math::Vec3f& planePoint, const Math::Vec3f& planeNormal);


			}	// namespace Utility
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT
