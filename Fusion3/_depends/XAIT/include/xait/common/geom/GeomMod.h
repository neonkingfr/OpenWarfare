// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/math/Vec3.h>
#include <xait/common/container/Vector.h>
#include <xait/common/geom/Triangle3D.h>
#include <xait/common/geom/AABB2D.h>
#include <xait/common/geom/IndexedTriangle.h>

namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			namespace Mod
			{
				XAIT_COMMON_DLLAPI void CutTrianglePlane(Container::Vector<Triangle3Df>& result, const Triangle3Df& triangle , const Math::Vec3f& planePoint, const Math::Vec3f& planeNormal);

				XAIT_COMMON_DLLAPI void CutTriangleBBox2D(Container::Vector<Triangle3Df>& result, const Triangle3Df& triangle, const AABB2Df& bbox, const uint32 upComp);

				XAIT_COMMON_DLLAPI void CutTrianglesBBox2D(Container::Vector<Math::Vec3f>& vertices, Container::Vector<IndexedTriangle>& triangles, const AABB2Df& bbox, const uint32 upComp);
			}
		}
	}
}

