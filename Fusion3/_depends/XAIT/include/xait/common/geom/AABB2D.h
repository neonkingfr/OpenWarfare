// (C) xaitment GmbH 2006-2012

#pragma once

// ---------------------------------------------------------------------------------------------------
//										Axis Aligned Bounding Box for 2D
//
// Author: Markus Wilhelm
//
// An Axis Aligned Bounding Box for 2D Space
// ---------------------------------------------------------------------------------------------------

#include <xait/common/math/Vec2.h>
#include <xait/common/math/NumericLimits.h>


namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			template<typename T_COMP>
			struct AABB2D;

			typedef AABB2D<float32>		AABB2Df;
			typedef AABB2D<float64>		AABB2Dd;

			typedef AABB2D<uint32>		AABB2Du32;
			typedef AABB2D<int32>		AABB2Di32;


			template<typename T_COMP>
			struct AABB2D
			{
				Math::Vec2<T_COMP>	mMin;		//!< minimum value
				Math::Vec2<T_COMP>	mMax;		//!< maximum value

				//! \brief default constructor
				//! \remark No initialization (performance issue)
				AABB2D()
				{};

				//! \brief init the bounding box with a first value
				//! \param value	first value in the box
				inline void init(const Math::Vec2<T_COMP>& value)
				{
					mMin= value;
					mMax= value;
				}

				//! \brief init the bounding box with default value
				//! \remark The resulting bounding box is not valid until one point has extended the bounding box
				inline void init()
				{
					const T_COMP posInfinity= Math::NumericLimits<T_COMP>::maxPosValue();

					// we build a negative infinity, so we take the positive maximum and decrement it
					// by one for integer values, which have a by one smaller value range in negative
					// area.
					const T_COMP negInfinity= Math::NumericLimits<T_COMP>::maxNegValue();

					mMin.mX= posInfinity;
					mMin.mY= posInfinity;
					mMax.mX= negInfinity;
					mMax.mY= negInfinity;
				}

				//! \brief extend the bounding box by a position
				//! \param pos		position by which the bounding box should be extended
				inline void extend(const Math::Vec2<T_COMP>& pos)
				{
					if (pos.mX < mMin.mX) mMin.mX= pos.mX;
					if (pos.mY < mMin.mY) mMin.mY= pos.mY;
					if (pos.mX > mMax.mX) mMax.mX= pos.mX;
					if (pos.mY > mMax.mY) mMax.mY= pos.mY;
				}

				//! \brief extend the bounding box by another bounding box
				//! \param bbox		bounding box by which this bounding box should be extended
				inline void extend(const AABB2D& bbox)
				{
					extend(bbox.mMin);
					extend(bbox.mMax);
				}

				//! \brief get the size of the bounding box
				inline Math::Vec2<T_COMP> getSize() const
				{
					return mMax - mMin;
				}

				//! \brief check if the bounding box is valid
				//! \returns true if the bounding box is valid
				inline bool isValid() const
				{
					return (mMin.mX <= mMax.mX && mMin.mY <= mMax.mY);
				}

				//! \brief tests if two bounding boxes overlap
				inline bool overlap(const AABB2D& bbox) const
				{
					if (mMax.mY < bbox.mMin.mY) return false;
					if (mMin.mY > bbox.mMax.mY) return false;
					if (mMax.mX < bbox.mMin.mX) return false;
					if (mMin.mX > bbox.mMax.mX) return false;

					return true;
				}

				//! \brief test if a point is in the bounding box
				//! \param pos	point to test
				//! \returns true if the point is inside
				inline bool isPointInside(const Math::Vec2<T_COMP>& pos) const
				{
					if (pos.mX < mMin.mX) return false;
					if (pos.mY < mMin.mY) return false;
					if (pos.mX > mMax.mX) return false;
					if (pos.mY > mMax.mY) return false;

					return true;
				}

				//! \brief test if another box is completly inside this box
				inline bool isBBoxInside(const AABB2D& box) const
				{
					if (box.mMin.mX < mMin.mX) return false;
					if (box.mMin.mY < mMin.mY) return false;
					if (box.mMax.mX > mMax.mX) return false;
					if (box.mMax.mY > mMax.mY) return false;

					return true;
				}

				//! \brief computes the center of the BB
				//! \returns the center of the BB
				inline const Math::Vec2<T_COMP> getCenter() const
				{
					return (mMax + mMin) * 0.5f;
				}
			};	// class AABB2D
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT
