// (C) xaitment GmbH 2006-2012

#pragma once

#include <algorithm>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/memory/Memory.h>


namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			template<typename THEAP_ELEM, typename T_ELEM_COMP= std::less<THEAP_ELEM> >
			class StaticHeap
			{
				friend class Iterator;

			public:
				class Iterator
				{
					StaticHeap<THEAP_ELEM,T_ELEM_COMP>*		mOwner;		//!< heap this iterator is used for
					uint32      							mPos;		//!< current position in heap	

					friend class StaticHeap<THEAP_ELEM,T_ELEM_COMP>;

					Iterator()
						: mOwner(NULL)
						, mPos(0)
					{
					};

					Iterator(StaticHeap<THEAP_ELEM,T_ELEM_COMP>* owner,uint32 pos=0)
						: mOwner(owner),mPos(pos)
					{
					};

				public:

					//! \brief increment to the next valid position or gets invalid if end of data reached (check against end-iterator)
					Iterator& operator++()
					{
						// get the next valid id
						if(mPos <= mOwner->heapsize)
							++mPos;

						return *this;
					}

					Iterator& operator--()
					{
						// get the next valid id
						if(mPos >0)
							--mPos;

						return *this;
					}

					//! \brief get element iterator currently points to
					THEAP_ELEM& operator*()
					{
						return (mOwner->heap[mPos]);
					}

					//! \brief compare to iterators
					bool operator==(const Iterator& that)
					{
						if (mOwner != that.mOwner)
							return false;
						return that.mPos == mPos;
					}

					bool operator!=(const Iterator& that)
					{
						if (mOwner != that.mOwner)
							return true;
						return that.mPos != mPos;
					}

					//! \brief get id this iterator points to
					uint32 getID() const
					{
						return mPos;
					}
				};


				StaticHeap(const Memory::Allocator::Ptr& allocator)
					: mAllocator(allocator),mHeap(NULL),mReserved(0),mHeapsize(0)
				{
					mBeginIter.mOwner = this;
					mBeginIter.mPos   = 0;
					mEndIter.mOwner = this;
					mEndIter.mPos = mHeapsize;
				}

				StaticHeap(const StaticHeap& other)
					: mAllocator(other.mAllocator),mReserved(other.mReserved),mHeapsize(other.mHeapsize),
					mComp(other.mComp)
				{
					// allocate memory for the heap and copy the heap from the other struct
					mHeap= (THEAP_ELEM*)mAllocator->alloc(sizeof(THEAP_ELEM) * mReserved);
					memcpy(mHeap,other.mHeap,sizeof(THEAP_ELEM) * mHeapsize);

					// init begin/end iterator
					mBeginIter.mOwner = this;
					mBeginIter.mPos   = other.mBeginIter.mPos;
					mEndIter.mOwner = this;
					mEndIter.mPos = mHeapsize;
				}

				~StaticHeap()
				{
					destroy();
				}

				// destroys the heap, frees memory
				void destroy()
				{
					clear();

					if (mHeap) mAllocator->free(mHeap);
					mHeap= NULL;
					mReserved= 0;
				}

				// reserve memory for at least _size entries
				void reserve(const uint32 _size)
				{
					if (_size <= mReserved) return;
					THEAP_ELEM* nHeap= (THEAP_ELEM*)mAllocator->alloc(sizeof(THEAP_ELEM) * _size);
					mReserved= _size;

					if (mHeap)
					{
						memcpy(nHeap,mHeap,sizeof(THEAP_ELEM) * mHeapsize);
						mAllocator->free(mHeap);
					}

					mHeap= nHeap;
				}

				// clear heap does not free memory
				void clear()
				{
					for(uint32 i= 0; i < mHeapsize; ++i)
					{
						Memory::CallDestructor(&mHeap[i]);
					}
					mHeapsize= 0;
					mEndIter= mBeginIter;
				}


				// push an element on the heap
				void push(const THEAP_ELEM& elem)
				{
					reserve(mHeapsize + 1);
					Memory::CallCopyConstructor(mHeap[mHeapsize],elem);
					++mHeapsize;
					pushSTLHeap(mHeap,mHeap + mHeapsize);
					mEndIter.mPos = mHeapsize;
				}

				// pop an element from the heap
				void pop()
				{
					popSTLHeap(mHeap,mHeap + mHeapsize);
					--mHeapsize;
					Memory::CallDestructor(&mHeap[mHeapsize]);
					mEndIter.mPos = mHeapsize;
				}

				void remove(THEAP_ELEM* pos)
				{
					X_ASSERT_MSG_DBG(isValidPos(pos),"position out of range");
					pushSTLHeap(mHeap,pos + 1);
					popSTLHeap(mHeap,mHeap + mHeapsize);
					--mHeapsize;

					Memory::CallDestructor(&mHeap[mHeapsize]);
				}

				inline bool isValidPos(const THEAP_ELEM* pos) const
				{
					return pos >= mHeap && pos < (mHeap + mHeapsize);
				}

				// get top element from the heap
				inline const THEAP_ELEM& top() const
				{
					return *mHeap;
				}

				// get top element from the heap
				inline THEAP_ELEM& top()
				{
					return *mHeap;
				}

				// get size of the heap
				inline uint32 size() const
				{
					return mHeapsize;
				}

				// check if heap is empty
				inline bool empty() const
				{
					return mHeapsize == 0;
				}

				// get all nodes on the heap
				// parameter:
				//	nodes		array to hold node indices
				// returns number of nodes 
				uint32 getNodes(Vector<uint32>& nodes)
				{
					if (nodes.size() < mHeapsize) nodes.resize(mHeapsize);

					for(size_t i= 0; i < mHeapsize; ++i) {
						nodes[i]= mHeap[i].nodeIndex;
					}

					return mHeapsize;
				}

				inline Iterator& begin()
				{
					return mBeginIter;
				};

				inline Iterator& end()
				{
					return mEndIter;
				};

			private:
				Memory::Allocator::Ptr	mAllocator;
				THEAP_ELEM*				mHeap;

				uint32					mReserved;		//!< reserverd size of heapmemory
				uint32					mHeapsize;		//!< elements in heap

				T_ELEM_COMP				mComp;			//!< comperator for heap insert

				Iterator				mBeginIter;
				Iterator				mEndIter;

				// for the ms compiler we use directly internal functions since there is a lot of debugging involved in debug
				// mode which considerable slows down computation
				// we create replacements for push and pop heap functions
				// this are basically copied version from the respective sdks with removed debug code.
				void pushSTLHeap(THEAP_ELEM* _First, THEAP_ELEM* _Last)
				{
#if (_MSC_VER >= 1600) || (XAIT_OS == XAIT_OS_XBOX360)	// vs2010 and xbox360
					if (_First != _Last)
					{	// check and push to nontrivial heap
						--_Last;
						std::_Push_heap_0(std::_Unchecked(_First), std::_Unchecked(_Last), mComp,
							std::_Dist_type(_First), std::_Val_type(_First));
					}
#elif _MSC_VER >= 1400	// vs2005/2008
					if (_First != _Last)
					{	// check and push to nontrivial heap
						std::_Push_heap_0(_CHECKED_BASE(_First), _CHECKED_BASE(--_Last), mComp,
							std::_Dist_type(_First), std::_Val_type(_First));
					}
#else
					std::push_heap(_First,_Last,mComp);
#endif
				}

				void popSTLHeap(THEAP_ELEM* _First, THEAP_ELEM* _Last)
				{
#if (_MSC_VER >= 1600) || (XAIT_OS == XAIT_OS_XBOX360)	// vs2010 and xbox360
					if (1 < _Last - _First)
						std::_Pop_heap(std::_Unchecked(_First), std::_Unchecked(_Last), mComp);
#elif _MSC_VER >= 1400	// vs2005/2008
					if (1 < _Last - _First)
						std::_Pop_heap_0(_CHECKED_BASE(_First), _CHECKED_BASE(_Last), mComp, std::_Val_type(_First));
#else
					std::pop_heap(_First,_Last,mComp);
#endif
				}

			};	// class StaticHeap
		}	// namespace Container
	}	// namespace Common
}	// namespace XAIT
