// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/Allocator.h>

#define XAIT_STACK_GROWFACTOR	1.5

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			template<typename T>
			class Stack
			{
				Memory::Allocator::Ptr	mAllocator;
				T*						mStack;
				int32					mReserved;
				int32					mTop;

			public:
				Stack(const Common::Memory::Allocator::Ptr& alloc)
					: mAllocator(alloc)
					, mStack(NULL)
					, mReserved(0)
					, mTop(-1)
				{}

				~Stack()
				{
					destroy();
				}

				void push(const T& value)
				{
					if (mReserved <= (mTop + 1))
						grow();

					++mTop;

					Memory::CallCopyConstructor(mStack[mTop],value);
				}

				T& top()
				{
					return mStack[mTop];
				}

				const T& top() const
				{
					return mStack[mTop];
				}

				void pop()
				{
					Memory::CallDestructor(&mStack[mTop]);
					--mTop;
				}

				bool empty() const
				{
					return mTop == -1;
				}

				void clear()
				{
					while(!empty())
						pop();
				}
			private:
				void grow()
				{
					const uint32 newSize= (uint32)((float)mReserved * XAIT_STACK_GROWFACTOR) + 1;
					T* newStack= Memory::AllocateArray<T>(mAllocator,newSize);

					// copy old elements
					for(int32 i= 0; i <= mTop; ++i)
					{
						Memory::CallCopyConstructor(newStack[i],mStack[i]);
						Memory::CallDestructor(&mStack[i]);
					}

					Memory::Free(mStack,mAllocator);
					mStack= newStack;
					mReserved= newSize;
				}

				void destroy()
				{
					clear();
					Memory::Free(mStack,mAllocator);
					mReserved= 0;
				}
			};
		}
	}
}
