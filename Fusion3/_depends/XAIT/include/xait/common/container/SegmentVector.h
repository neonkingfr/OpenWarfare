// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/container/Vector.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/utility/BitMask.h>

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			template<class VALUE_T, uint08 SEGMENT_BITS = 10>
			class SegmentVector
			{
			public:
				enum
				{
					SEGMENT_MASK= Utility::BitMask<SEGMENT_BITS>::MASK,
				};

				SegmentVector(const Memory::Allocator::Ptr& allocator)
					: mSize(0),
					mReserved(0),
					mSegments(allocator)
				{
				}

				SegmentVector(const SegmentVector& src)
					: mSegments(src.mSegments.getAllocator())
				{
					mSize= src.mSize;
					mReserved= src.mReserved;

					uint32 numSegments= src.mSegments.size();
					uint32 numElements= getSegmentSize();
					if (numSegments == 0) return;

					mSegments.resize(numSegments);
					VALUE_T* newSeg;
					const VALUE_T* oldSeg;

					// copy n - 1 segements
					uint32 i;
					uint32 k;
					for(i= 0; i < numSegments - 1; ++i)
					{
						newSeg= allocateSegment();
						oldSeg= src.mSegments[i];
						
						for(k= 0; k < numElements; ++k);
						{
							newSeg[k]= oldSeg[k];		
						}
						mSegments[i]= newSeg;
					}

					// copy segment n until index
					newSeg= allocateSegment();
					oldSeg= src.mSegments[numSegments - 1];
					numElements= mSize & SEGMENT_MASK;

					for(i= 0; i < numElements; ++i)
					{
						newSeg[i]= oldSeg[i];
					}
					mSegments[numSegments - 1]= newSeg;
				}

				~SegmentVector()
				{
					destroy();
				}

				inline const VALUE_T& operator[](const uint32 index) const
				{
					X_ASSERT_MSG(index < mSize,"index out of range");
					return mSegments[index >> SEGMENT_BITS][index & SEGMENT_MASK];	
				}	

				inline VALUE_T& operator[](const uint32 index)
				{
					X_ASSERT_MSG(index < mSize,"index out of range");
					return mSegments[index >> SEGMENT_BITS][index & SEGMENT_MASK];	
				}

				inline VALUE_T& pushBack(const VALUE_T& elem)
				{
					if (mSize >= mReserved)
					{
						reserveNextSegment();
					}

					VALUE_T& val= mSegments.back()[mSize & SEGMENT_MASK];
					new (&val) VALUE_T(elem);
					++mSize;

					return val;
				}

				inline VALUE_T& back()
				{
					const uint32 last= mSize - 1;
					return mSegments[last >> SEGMENT_BITS][last & SEGMENT_MASK];
				}

				inline const VALUE_T& back() const
				{
					const uint32 last= mSize - 1;
					return mSegments[last >> SEGMENT_BITS][last & SEGMENT_MASK];
				}

				//! \brief clears the vector without deleting the segments
				inline void clear()
				{
					mSize= 0;
				}

				//! \brief clears the vector by destroying the segments
				inline void destroy()
				{
					const int32 numSegments= (int32)mSegments.size();
					const Memory::Allocator::Ptr& alloc= mSegments.getAllocator();

					for(int32 i= 0; i < (numSegments - 1); ++i)
					{
						VALUE_T* currSegment= mSegments[i];
						for(uint32 k= 0; k < getSegmentSize(); ++k)
						{
							Memory::CallDestructor(&currSegment[k]);
						}
						Memory::Free(currSegment,alloc);
					}

					// special handling for last segment
					if (numSegments > 0)
					{
						VALUE_T* currSegment= mSegments[numSegments - 1];
						const uint32 numElements= size() & SEGMENT_MASK;
						for(uint32 k= 0; k < numElements; ++k)
						{
							Memory::CallDestructor(&currSegment[k]);
						}
						Memory::Free(currSegment,alloc);
					}

					mSegments.clear();
					mSize= 0;
					mReserved= 0;
				}

				//! \brief get the number of elements in the vector
				inline uint32 size() const
				{
					return mSize;
				}

				//! \brief get the memory allocator for this vector
				inline Memory::Allocator::Ptr getAllocator() const
				{
					return mSegments.getAllocator();
				}

			private:
				uint32				mSize;
				uint32				mReserved;
				Vector<VALUE_T*>	mSegments;

				inline void reserveNextSegment()
				{
					mSegments.pushBack(allocateSegment());
					mReserved+= getSegmentSize();
				}

				inline VALUE_T* allocateSegment() const
				{
					uint32 segmentSize= getSegmentSize();
					return Memory::AllocateArray<VALUE_T>(mSegments.getAllocator(),segmentSize);
				}

				//! \brief get number of elements in a segment
				inline uint32 getSegmentSize() const
				{
					return SEGMENT_MASK + 1;
				}
			};

		}
	}

}
