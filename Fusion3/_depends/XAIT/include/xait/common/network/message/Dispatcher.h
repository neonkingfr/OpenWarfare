// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>

#include <xait/common/container/LookUp.h>
#include <xait/common/io/Stream.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Message
			{
				class Dispatcher : public Common::Memory::MemoryManaged
				{
				public:
					//! \brief Constructor
					Dispatcher();

					//! \brief Destructor
					virtual ~Dispatcher();

					//! \brief registers a MessageHandler by his MessageTypeID
					//! \param handler	the MessageHandler to register
					//! \returns true, if the MessageHandler could be registered; false, if the MessageTypeID is already registered by another MessageHandler
					bool registerMessageHandler( MessageHandler* handler);

					//! \brief dispatches a message given as a stream by matching the MessageTypeID at the beginning of the stream with registered MessageHandler
					//! \param messageStream	the stream to dispatch
					//! \param connectionID		identifier of the client the message is from
					//! \returns true, if the MessageHandler was found and he handled the stream else returns false
					bool dispatchMessage( IO::Stream* messageStream, const uint32 connectionID );

					//! \brief searches a messageHandler for a given MessageTypeID
					//! \param messageHandler	the found MessageHandler
					//! \param identifier	the MessageTypeID to match the MessageHandler
					//! \returns true, if the MessageHandler was found; false if not found
					bool findMessageHandler( MessageHandler*& messageHandler, const uint32 identifier);

				private:
					Container::LookUp<uint32, MessageHandler*>	mRegisteredHandler;
					bool	mUseTypedData;
				};
			}
		}
	}
}
