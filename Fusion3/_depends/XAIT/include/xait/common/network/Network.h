// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/DLLDefines.h>

namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			//! \brief initialize network structure
			XAIT_COMMON_DLLAPI bool InitializeNetwork();

			//! \brief close network structure
			XAIT_COMMON_DLLAPI void CloseNetwork();
		}
	}
}
