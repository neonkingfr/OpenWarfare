// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/network/NetworkPlatform.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Sockets
			{
				enum AddressFamily
				{
					ADDRFAMILY_INET4		= SOCKET_AF_INET4,			//!< internet protocol version 4
					ADDRFAMILY_INET6		= SOCKET_AF_INET6,			//!< internet protocol version 6
					ADDRFAMILY_UNSPEC		= SOCKET_AF_UNSPEC,			//!< unspecified address family
					ADDRFAMILY_UNSUPPORTED	= SOCKET_AF_UNSUPPORTED,	//!< value of unsupported address family for the current platform
				};
			}
		}
	}
}

