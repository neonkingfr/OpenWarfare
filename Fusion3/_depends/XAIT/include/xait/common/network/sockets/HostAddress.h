// (C) xaitment GmbH 2006-2012

#pragma once 


#include <xait/common/container/Vector.h>
#include <xait/common/String.h>
#include <xait/common/network/NetworkPlatform.h>
#include <xait/common/network/sockets/IP.h>
#include <xait/common/network/sockets/AddressFamily.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Sockets
			{
				class HostAddress
				{
				public:
					//! \brief Creates in invalid hostaddress, used for return values
					HostAddress();

					//! \brief Creates a host address for the localhost on the specified port
					//! \remarks There is no test if the address exists
					HostAddress(const uint16 port);

					//! \brief Creates a host address for a remote host and port in IPV4 format
					//! \remarks There is no test if the address exists
					XAIT_COMMON_DLLAPI HostAddress(const IPV4& ip, const uint16 port);

#ifdef XAIT_NETWORK_HAS_IPV6
					//! \brief Creates a host address for a remote host and port in IPV6 format
					//! \remarks There is no test if the address exists
					HostAddress(const IPV6& ip, const uint16 port);

					//! \brief get IPV6 address
					//! \remarks Works only if this ia a IPV6 host address, check with HostAddress::getAddressFamily
					IPV6 getAddressIPV6() const;
#endif

					//! \brief Creates a host address from a socket address
					HostAddress(const SocketOSAddress* osAddress);

					//! \brief get the port of this address
					uint16 getPort() const;

					//! \brief get the host name 
					inline const Common::String& getHostName() const
					{
						return mHostName;
					}

					//! \brief get address family
					AddressFamily getAddressFamily() const;

					//! \brief get IPV4 address
					//! \remarks Works only if this ia a IPV4 host address, check with HostAddress::getAddressFamily
					IPV4 getAddressIPV4() const;

#ifdef XAIT_NETWORK_HAS_NAMERESOLUTION

					//! \brief resolve a host address from a host name
					static bool resolveHostAddress(HostAddress& foundAddress, const Common::String& hostName, const uint16 port, const AddressFamily addressFamily= ADDRFAMILY_INET4);

#endif

					//! \brief get the os address struct
					inline const SocketOSAddress* getOSAddress() const
					{
						return &mSocketAddress;
					}

				private:
					//! \brief internal constructor used in resolve
					HostAddress(const Common::String& hostName, const IPV4& ip, const uint16 port);

#ifdef XAIT_NETWORK_HAS_IPV6
					//! \brief internal constructor used in resolve
					HostAddress(const Common::String& hostName, const IPV6& ip, const uint16 port);

					void initSocketAddress(const IPV6& hostIP, const uint16 port);
#endif

					template<typename T_IP>
					inline void initHostAddress(const Common::String& hostName, const T_IP& ip, const uint16 port)
					{
						mHostName= hostName;
						initSocketAddress(ip,port);
					}

					void initSocketAddress(const IPV4& hostIP, const uint16 port);

					SocketOSAddress		mSocketAddress;
					Common::String		mHostName;
				};
			}
		}
	}
}

