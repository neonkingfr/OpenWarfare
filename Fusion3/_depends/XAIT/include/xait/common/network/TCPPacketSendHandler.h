// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/OutputHandler.h>
#include <xait/common/network/sockets/TCPSocket.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			class TCPPacketSendHandler : public OutputHandler
			{
			public:
				//! \brief Constructor for sending
				//! \param socket	the socket to send to
				//! \param reactor	the reactor triggering this handler
				TCPPacketSendHandler(Sockets::TCPSocket* socket, Reactor* reactor);

				//! \brief Destructor
				~TCPPacketSendHandler();

				//! \brief	initializes the connection on the socket
				//! \returns true if the connection was set up, false if the connection fails
				//! \remark	see XAIT::Common::Network::Sockets::SocketError::getErrorText()
				bool init();

				//! \brief update function to assign computation time to the handler
				//! \returns true till error occurs, false on error
				bool onTick();

			private:
				//! \brief sends the length of the message through the connection
				//! \returns true if the message length could be send with the first try, false else
				bool sendLength();

				//! \brief sends the data through the connection
				//! \returns true till error occurs, false on error
				bool sendData();

				//! \brief returns true if the message was completely sent
				//! \returns true if the message was completely sent, false else
				bool isDone();

				//! \brief returns the own connection as a TCPSocket
				//! \returns the own connection as a TCPSocket
				Sockets::TCPSocket* getTCPSocket() const;


				uint32		mBytesCurr;		//!< number of bytes already sent of the current message
				uint32		mByteMax;		//!< the total number of bytes of the current message
			};

		} // Network
	} // Common
} // XAIT
