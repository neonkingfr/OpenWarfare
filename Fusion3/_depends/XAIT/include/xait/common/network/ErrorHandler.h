// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/EventHandler.h>
#include <xait/common/network/message/Dispatcher.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			class ErrorHandler : public EventHandler
			{
			public:

			protected:
				//! \brief Constructor for receiving data via Socket
				//! \param socket	the socket to react on Error
				//! \param dispatcher	pointer to the Dispatcher to dispatch reactions on
				ErrorHandler(Sockets::Socket* socket, Message::Dispatcher* dispatcher);

				//! \Destructor
				virtual ~ErrorHandler() {}

				Message::Dispatcher*	mDispatcher;
			};

		} // Network
	} // Common
} // XAIT
