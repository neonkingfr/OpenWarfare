// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/sockets/Socket.h>
#include <xait/common/memory/MemoryStream.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			class EventHandler : public Common::Memory::MemoryManaged
			{
			public:
				enum EventHandlerType
				{
					EHANDLER_INPUT,
					EHANDLER_OUTPUT,
					EHANDLER_ERROR,
					EHANDLER_UNDEFINED
				};

				//! \brief Destructor
				virtual ~EventHandler();

				static const uint32 MAX_NUM_BYTES_TRANSPORT = 256;

				//! \brief	initializes the connection on the socket
				//! \returns true if the connection was set up, false if the connection fails
				//! \remark	see XAIT::Common::Network::Sockets::SocketError::getErrorText()
				virtual bool init() = 0;

				//! \brief update function to assign computation time to the handler
				//! \returns true, while the handler has no problems, false if an error occurred
				virtual bool onTick() = 0;

				//! \brief returns the socket of the event handler
				//! \returns the socket of the event handler
				Sockets::Socket* getSocket() const {return mSocket;}

				//! \brief returns the type of the event handler
				//! \returns the type of the event handler
				EventHandlerType getType() const {return mType;}

			protected:
				//! \brief Constructor
				//! \param type		the type of the EventHandler
				//! \param socket	the connection to react on
				EventHandler(EventHandlerType type, Sockets::Socket* socket);

				EventHandlerType		mType;
				Sockets::Socket*		mSocket;
			};
		} // Network
	} // Common
} // XAIT
