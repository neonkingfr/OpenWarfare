// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/exceptions/Exception.h>


namespace XAIT
{
	namespace Common
	{
		namespace Exceptions
		{
			//! \brief Exception if the called function is not allowed for the current user
			class UnAuthorizedAccessException : public Exception
			{
			public:
				UnAuthorizedAccessException(const Common::String& errorMsg)
					: Exception(errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "UnAuthorizedAccessException";
				}

			};
		}
	}
}
