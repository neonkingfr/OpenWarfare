// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/exceptions/ArgumentException.h>

namespace XAIT
{
	namespace Common
	{
		namespace Exceptions
		{
			//! \brief Exception if an argument is a null pointer
			class ArgumentNullException : public ArgumentException
			{
			public:
				ArgumentNullException(const Common::String& argumentName, const Common::String& errorMsg)
					: ArgumentException(argumentName,errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "ArgumentNullException";
				}

			};
		}
	}
}
