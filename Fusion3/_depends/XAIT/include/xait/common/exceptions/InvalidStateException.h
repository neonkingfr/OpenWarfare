// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/exceptions/Exception.h>

namespace XAIT
{
	namespace Common
	{
		namespace Exceptions
		{
			//! \brief Exception if a class models a state machine and the execute instruction does not fit into the current state of the class (closed file -> read)
			class InvalidStateException : public Exception
			{
			public:
				InvalidStateException(const Common::String& errorMsg)
					: Exception(errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "InvalidStateException";
				}
			};
		}
	}
}
