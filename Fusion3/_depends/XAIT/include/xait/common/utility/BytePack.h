// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/utility/BitMask.h>

namespace XAIT
{
	namespace Common
	{
		namespace Utility
		{
			//! \brief Combine two integer in the next bigger integer as low and high part
			//! \param low		low part in the final integer
			//! \param high		high part in the final integer
			template<typename T_FULL, typename T_HALF>
			inline T_FULL BytePack(const T_HALF low, const T_HALF high)
			{
				return (((T_FULL)high) << (sizeof(T_FULL) * 4)) | low;
			}

			template<typename T_HALF, typename T_FULL>
			inline T_HALF GetLowerBytes(const T_FULL full)
			{
				return (T_HALF)(full & BitMask<sizeof(T_HALF) * 8>::MASK);
			}

			template<typename T_HALF, typename T_FULL>
			inline T_HALF GetHigherBytes(const T_FULL full)
			{
				return (T_HALF)(full >> (sizeof(T_HALF) * 8));
			}
		}
	}
}

