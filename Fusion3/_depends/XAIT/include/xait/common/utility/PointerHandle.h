// (C) xaitment GmbH 2006-2012

#pragma once 

namespace XAIT
{
	namespace Common
	{
		namespace Utility
		{
			template<typename T_HANDLE>
			class PointerHandle
			{
			public:
				PointerHandle()
				{
					mPtr= NULL;
				}

				PointerHandle(const PointerHandle& other)
				{
					if (other.mPtr)
					{
						mHandle= other.mHandle;
						mPtr= &mHandle;
					}
					else
					{
						mPtr= NULL;
					}
				}

				PointerHandle(const T_HANDLE& other)
				{
					mHandle= other;
					mPtr= &mHandle;
				}

				inline bool isValid() const
				{
					return mPtr	!= NULL;
				}

				inline void setPtrNull()
				{
					mPtr= NULL;
				}

				inline void setPtrHandle()
				{
					mPtr= &mHandle;
				}

				inline T_HANDLE* getPtr()
				{
					return mPtr;
				}

				inline const T_HANDLE* getPtr() const
				{
					return mPtr;
				}

				inline T_HANDLE& getHandle()
				{
					return mHandle;
				}

				inline const T_HANDLE& getHandle() const
				{
					return mHandle;
				}

				inline bool operator==(const PointerHandle& other)
				{
					return other.mPtr == mPtr;
				}

				inline bool operator!=(const PointerHandle& other)
				{
					return other.mPtr != mPtr;
				}

				inline PointerHandle& operator=(const PointerHandle& other)
				{
					if (other.mPtr)
					{
						mHandle= other.mHandle;
						mPtr= &mHandle;
					}
					else
					{
						mPtr= NULL;
					}

					return *this;
				}

				inline PointerHandle& operator=(const T_HANDLE& other)
				{
					mHandle= other;
					mPtr= &mHandle;
					return *this;
				}

			private:
				template<typename T_HANDLE_OTHER>
				friend bool operator==(const PointerHandle<T_HANDLE_OTHER>& pHandle, const T_HANDLE_OTHER* nHandle);
				template<typename T_HANDLE_OTHER>
				friend bool operator==(const T_HANDLE_OTHER* nHandle,const PointerHandle<T_HANDLE_OTHER>& pHandle);
				template<typename T_HANDLE_OTHER>
				friend bool operator!=(const PointerHandle<T_HANDLE_OTHER>& pHandle, const T_HANDLE_OTHER* nHandle);
				template<typename T_HANDLE_OTHER>
				friend bool operator!=(const T_HANDLE_OTHER* nHandle,const PointerHandle<T_HANDLE_OTHER>& pHandle);

				T_HANDLE*	mPtr;
				T_HANDLE	mHandle;
			};

		}
	}
	template<typename T_HANDLE>
	inline bool operator==(const Common::Utility::PointerHandle<T_HANDLE>& pHandle, const T_HANDLE* nHandle)
	{
		return pHandle.mPtr == nHandle;
	}

	template<typename T_HANDLE>
	inline bool operator==(const T_HANDLE* nHandle,const Common::Utility::PointerHandle<T_HANDLE>& pHandle)
	{
		return pHandle.mPtr == nHandle;
	}

	template<typename T_HANDLE>
	inline bool operator!=(const Common::Utility::PointerHandle<T_HANDLE>& pHandle, const T_HANDLE* nHandle)
	{
		return pHandle.mPtr != nHandle;
	}

	template<typename T_HANDLE>
	inline bool operator!=(const T_HANDLE* nHandle,const Common::Utility::PointerHandle<T_HANDLE>& pHandle)
	{
		return pHandle.mPtr != nHandle;
	}

}
