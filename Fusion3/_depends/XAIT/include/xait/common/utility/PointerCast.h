// (C) xaitment GmbH 2006-2012

#pragma once 


namespace XAIT
{
	namespace Common
	{
		namespace Utility
		{
			template<typename T_INT,typename T_POINTER>
			inline T_INT PointerCast(const T_POINTER* pointer)
			{
				return *((T_INT*)(&pointer));
			}
		}
	}
}

