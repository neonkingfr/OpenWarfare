// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/StreamDictionary.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/platform/Endian.h>
#include <xait/common/CommonLog.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class PackedStreamDictionary : public StreamDictionary
			{
			public:
				//! \brief create new packed stream directory
				//! \param singleStreams	if true you can only have one stream at a time, two stream with concurrent access will interfere
				//! \param streamBufferSize	size of the buffer on top of each returned stream, useful if singleStreams = false, speeds up the streams by using local storage (buffer)
				XAIT_COMMON_DLLAPI PackedStreamDictionary(bool singleStreams= false, int32 streamBufferSize= 8 * 1024);
				XAIT_COMMON_DLLAPI ~PackedStreamDictionary();

				XAIT_COMMON_DLLAPI bool open(Stream* underlyingStream, bool forceCreateNew= false);

				bool canAdd() const
				{
					return mPackedStream != NULL && mPackedStream->canWrite();
				}
				
				bool canRemove() const
				{
					return false;
				}

				virtual bool isClosed() const
				{
					return mPackedStream == NULL || mPackedStream->isClosed();
				}

				virtual bool isEmpty() const
				{
					return mContentTable.empty();
				}

				const Common::String& getName() const
				{
					return mName;
				}

				bool addStream(Stream* stream, const Common::String& name);
				bool removeStream(const Common::String& name);
				Stream* getStream(const Common::String& name);
				bool containsStream(const Common::String& name) const;
				void getContent(Container::Vector<String>& content) const;
				void clear();
				void close();

			private:
				struct StreamSection
				{
					uint64	mStart;
					uint64	mEnd;
				};

				Stream*												mPackedStream;
				Common::String										mName;
				Container::LookUp<Common::String,StreamSection>		mContentTable;
				uint64												mEndPos;			// end of the dictionary in the packed stream
				uint64												mPackedStreamStart;
				bool												mHasChanged;
				int32												mStreamBufferSize;
				bool												mSingleStreams;

				template<typename T>
				inline void writeToStream(const T& val)
				{
					// all multi byte entries are written in big endian
					T encoded;
					Platform::Endian::convertSystemToBig(encoded,val);
					mPackedStream->write(encoded);
				}

				template<typename T>
				inline bool readFromStream(T& val)
				{
					const bool success= mPackedStream->read(val);
					if (!success)
					{
						XLOG_ERR("Stream corrupted at position: " << mPackedStream->getPosition());
						return false;
					}

					Platform::Endian::convertBigToSystem(val,val);
					return true;
				}

				bool readContentTable(const uint64 tablePos);
				void writeContentTable();
				void setEmptyContentTable();
			};
		}
	}
}
