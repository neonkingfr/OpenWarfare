// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/Stream.h>
#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class FileStream : public Stream
			{
			public:
				//! \brief tells what we can do with the file
				enum FileAccess
				{
					FACCESS_UNDEFINED,		//!< undefined, do not use
					FACCESS_READ,			//!< open file for read only access
					FACCESS_READWRITE,		//!< open file for read and write access
					FACCESS_WRITE,			//!< open file for write only access
				};

				//! \brief tells how the file system should open the file
				enum FileMode
				{
					FMODE_UNDEFINED,		//!< undefined, do not use
					FMODE_OPEN,				//!< open existing file, fails if file does not exist
					FMODE_APPEND,			//!< open a file and seeks to the end, if the file exists, otherwise the file will be created
					FMODE_CREATE,			//!< creates a file, if the file exists, it will be overwritten
					FMODE_CREATENEW,		//!< create a new file, fails if the file exists
					FMODE_OPEN_OR_CREATE,	//!< opens the file if it exists, or creates a new one if it does not.
				};

				//! \brief constructs stream without opening a file
				XAIT_COMMON_DLLAPI FileStream();

				//! \brief constructs stream and opens a file
				//! \param fileName		file to open
				//! \param fAccess		file access mode (\see FileAccess)
				//! \param fMode		file open/creation mode (\see FileMode)
				//! \exception ArgumentException			fAccess or fMode has wrong values
				//! \exception UnAuthorizedAccessException	Given path is a directory, or file is read-only, but an open-for-writing operation was attempted
				//! \exception FileExistException			File has been opened with FMODE_CREATENEW but the file exists already
				//! \exception IOException					No more file descriptors available
				//! \exception FileNotFoundException		File or path not found
				//! \remark All files will be opened in binary mode. If a file must be created, the 
				//!			permission of the file will be set to read/write. The files will be opened
				//!			for shared reading.
				XAIT_COMMON_DLLAPI FileStream(const XAIT::Common::String& fileName, const FileAccess fAccess, const FileMode fMode);

				XAIT_COMMON_DLLAPI ~FileStream();

				//! \brief open a file for streaming
				//! \param fileName		file to open
				//! \param fAccess		file access mode (\see FileAccess)
				//! \param fMode		file open/creation mode (\see FileMode)
				//! \exception ArgumentException			fAccess or fMode has wrong values
				//! \exception UnAuthorizedAccessException	Given path is a directory, or file is read-only, but an open-for-writing operation was attempted
				//! \exception FileExistException			File has been opened with FMODE_CREATENEW but the file exists already
				//! \exception IOException					No more file descriptors available
				//! \exception FileNotFoundException		File or path not found
				//! \remark All files will be opened in binary mode. If a file must be created, the 
				//!			permission of the file will be set to read/write. The files will be opened
				//!			for shared reading.
				virtual void openFile(const XAIT::Common::String& fileName, const FileAccess fAccess, const FileMode fMode);

				//! \brief get the name of the opened file
				//! \exception NotSupportedException	There is no file opened
				virtual const Common::String& getName() const;

				// derived methods
				virtual bool canRead() const;
				virtual bool canWrite() const;
				virtual bool canSeek() const;
				virtual bool isClosed() const;
				virtual uint64 getLength() const;
				virtual uint64 getPosition() const;
				virtual uint64 read(void* dstBuffer, const uint64 numBytes);
				virtual void write(const void* srcBuffer, const uint64 numBytes);
				virtual uint64 seek(const int64 offset, const SeekOrigin origin);
				virtual void flush();
				virtual void close();

			protected:
				FileAccess				mFileAccess;
				FileMode				mFileMode;
				int						mFHandle;
				XAIT::Common::String	mFileName;

			};
		}
	}
}

