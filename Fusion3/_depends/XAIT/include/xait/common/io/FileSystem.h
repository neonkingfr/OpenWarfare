// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/String.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/io/FileStream.h>
#include <xait/common/container/List.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			//! \brief class for encapsulation of file system functionality
			class FileSystem : public Memory::MemoryManaged
			{
			public:
				//! \brief default constructor
				XAIT_COMMON_DLLAPI FileSystem();

				//! \brief default destructor
				XAIT_COMMON_DLLAPI virtual ~FileSystem();

				//! \brief test if we can generally write to the filesystem
				//! \remark If the file system enables write, there is no warranty that we
				//!			can write to every file, but things like temporary files must work.
				XAIT_COMMON_DLLAPI virtual bool canWrite() const;

				//! \brief create a file stream on the virtual file system
				//! \param fileName		file to open
				//! \param fileAccess	file access mode (\see FileStream::FileAccess)
				//! \param fileMode		file open/creation mode (\see FileStream::FileMode)
				//! \returns a new instance of a filestream on the specified file in the requested mode
				XAIT_COMMON_DLLAPI virtual SharedPtr<FileStream> createFileStream(
					const Common::String& fileName, 
					const FileStream::FileAccess fileAccess, 
					const FileStream::FileMode fileMode);

				//! \brief open an existing file for reading only
				//! \param fileName		file to open
				//! \returns a new instance of a FileStream to the opened file
				//! \remark Opens only existing files, fails otherwise.
				//! \exception FileNotFoundException	throw if the file does not exists
				XAIT_COMMON_DLLAPI virtual SharedPtr<FileStream> openFile(const Common::String& fileName);

				//! \brief create a read/write file that does not already exists and return a FileStream on that
				//! \param fileName		file to create
				//! \returns a new instance of a FileStream to the just created file
				//! \exception FileExistsException		throw if file already exists
				//! \exception NotSupportedException	throws if file system is read only
				XAIT_COMMON_DLLAPI virtual SharedPtr<FileStream> createNewFile(const Common::String& fileName);

				//! \brief create a read/write temporary file
				//! \returns a new instance of a FileStream to the just created file
				//! \exception NotSupportedException	throws if file system is read only
				XAIT_COMMON_DLLAPI virtual SharedPtr<FileStream> createTemporaryFile();

				//! \brief open a file in read/write append mode
				//! \param fileName		file to open and append
				//! \returns a new instance of a FileStream to the opened file
				//! \remark If file does not exist, it will be created.
				//! \exception NotSupportedException	throws if file system is read only
				XAIT_COMMON_DLLAPI virtual SharedPtr<FileStream> appendFile(const Common::String& fileName);

				//! \brief tests if a file exists
				//! \param fileName		file name to test for existence
				//! \returns true if the file exists
				XAIT_COMMON_DLLAPI virtual bool existsFile(const Common::String& fileName);

				//! \brief delete a file
				//! \param fileName		file which should be deleted
				//! \exception FileNotFoundException	thrown if the file does not exist
				//! \exception NotSupportedExcetpion	thrown if the file system is read only
				XAIT_COMMON_DLLAPI virtual void deleteFile(const Common::String& fileName); 

				//! \brief get the filename from a path string
				//! \param path		path where we should get the file name from
				//! \returns the file name with its extension
				XAIT_COMMON_DLLAPI virtual Common::String getFileName(const Common::String& path) const;

				//! \brief get the filename from a path string without the file extension
				//! \param path		path where we should get the file name from
				//! \returns the file name without its extension
				XAIT_COMMON_DLLAPI virtual Common::String getFileNameWithoutExtension(const Common::String& path) const;

				//! \brief get filename extension
				//! \param path		path where we should get the file extension from
				//! \returns the extension of the filename 
				//! \remark the extension is without the leading '.'
				XAIT_COMMON_DLLAPI virtual Common::String getFileExtension(const Common::String& path) const;

				//! \brief get the full path of a partial path
				//! \param path		partial path where we should create the full path
				//! \returns the full path
				XAIT_COMMON_DLLAPI virtual Common::String getFullPath(const Common::String& path) const ;

				//! \brief get the directory name from the path string
				//! \param path		path where we should extract the directory name
				//! \returns the directory name from the path string
				//! \remark includes trailing slash
				XAIT_COMMON_DLLAPI virtual Common::String getDirectoryName(const Common::String& path) const;

				//! \brief set the current working directory
				//! \param dirName		directory to which we want to change
				//! \exception DirectoryNotFoundException	thrown if directory is invalid
				XAIT_COMMON_DLLAPI virtual void setCurrentWorkingDir(const Common::String& dirName);

				//! \brief get the current working directory
				//! \returns the current working directory
				XAIT_COMMON_DLLAPI virtual Common::String getCurrentWorkingDir() const;

				//! \brief create a new directory
				//! \param dirName		name of the directory to create
				//! \remark The directory will be also created if recursive directory creation is
				//!			necessary.
				//! \exception NotSupportedException	thrown if the file system is read only
				//! \exception DirectoryExistsException	thrown if the directory already exists
				//! \exception ArgumentException		thrown if the directory name is malformed
				XAIT_COMMON_DLLAPI virtual void createDirectory(const Common::String& dirName);

				//! \brief delete a directory
				//! \param dirName		name of the directory to remove
				//! \param recursive	if true, all stuff inside the directory will be deleted, including files and other directories.
				//! \exception DirectoryNotFoundException	thrown if directory is invalid
				//! \exception ArgumentException			thrown if the directory name is malformed
				//! \exception NotSupportedException		thrown if the file system is read only
				//! \exception UnAuthorizedAccessException	thrown if there is a opened file in the directory
				XAIT_COMMON_DLLAPI virtual void deleteDirectory(const Common::String& dirName, const bool recursive);

				//! \brief test if a directory exists
				//! \param dirName		name of the directory to test for existence
				//! \returns true if the directory exists, false otherwise.
				XAIT_COMMON_DLLAPI virtual bool existsDirectory(const Common::String& dirName);

			protected:
				Container::List<Common::String>		mTempFiles;		//!< list of temporary files
			};
		}
	}
}
