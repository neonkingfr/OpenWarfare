// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/io/IOException.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class DirectoryException : public IOException
			{
			public:
				DirectoryException(const Common::String& dirName, const Common::String& errorMsg)
					: IOException(errorMsg),mDirName(dirName)
				{}

				virtual Common::String getExceptionName() const
				{
					return "DirectoryException";
				}

				//! \brief get the name of the directory which made the trouble
				inline const Common::String& getDirectoryName() const
				{
					return mDirName;
				}

			protected:
				virtual void getExceptionParameter(Container::List<Container::Pair<Common::String,Common::String> >& parameters)
				{
					parameters.pushBack(Container::MakePair(Common::String("Directory"),mDirName));
				}

			private:
				Common::String		mDirName;
			};
		}
	}
}
