// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/IOException.h>

namespace XAIT
{
	namespace Common	
	{
		namespace IO
		{
			//! \brief all exceptions related to files are derived from this exception
			class FileException : public IOException
			{
			public:
				//! \brief create a file exception
				//! \param fileName		filename which raised the exception
				//! \param fullFileName	full name of the file which raised the exception (with path name)
				//! \param errorMsg		error message for the exception
				FileException(const Common::String& fileName, const Common::String& fullFileName, const Common::String& errorMsg)
					: IOException(errorMsg),mFileName(fileName),mFullFileName(fullFileName)
				{}

				virtual Common::String getExceptionName() const
				{
					return "FileException";
				}

				//! \brief get the filename that triggered the error
				inline const Common::String& getFileName() const
				{
					return mFileName;
				}

				//! \brief get full filename that triggered the error
				inline const Common::String& getFullFileName() const
				{
					return mFullFileName;
				}

			protected:
				Common::String		mFileName;
				Common::String		mFullFileName;

				virtual void getExceptionParameter(Container::List<Container::Pair<Common::String,Common::String> >& parameters)
				{
					parameters.pushBack(Container::MakePair(Common::String("Filename"),mFileName));
					parameters.pushBack(Container::MakePair(Common::String("FullFilename"),mFullFileName));
				}


			};

		}
	}
}



