// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/IOException.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class OutOfSpaceException : public IOException
			{
			public:
				OutOfSpaceException(const Common::String& errorMsg)
					: IOException(errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "OutOfSpaceException";
				}
			};
		}
	}
}



