// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/callback/MemoryCallback.h>

#include <revolution/mem.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			//! \brief  Wii Memory Callback (only for testing purposes. Do NOT use this in a real game).
			class MemoryCallbackWii : public Callback::MemoryCallback
			{
			public:
				typedef SharedPtr<MemoryCallback>	Ptr;

				//! \brief constrtuctor
				MemoryCallbackWii()
				: Callback::MemoryCallback()
				{
					//MEM2 adresses external heap
				    void          *arenaMem2Lo= OSGetMEM2ArenaLo();
					void          *arenaMem2Hi= OSGetMEM2ArenaHi();
					// Create a heap region in MEM2 
					mHExpHeap= MEMCreateExpHeap(arenaMem2Lo, (u32)arenaMem2Hi - (u32) arenaMem2Lo);
				}


				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				MemoryCallbackWii(Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: Callback::MemoryCallback(dealloc)
				{
					//MEM2 adresses external heap
					void          *arenaMem2Lo= OSGetMEM2ArenaLo();
					void          *arenaMem2Hi= OSGetMEM2ArenaHi();
					// Create a heap region in MEM2 
					mHExpHeap= MEMCreateExpHeap(arenaMem2Lo, (u32)arenaMem2Hi - (u32) arenaMem2Lo);
				}

				~MemoryCallbackWii()
				{
					MEMDestroyExpHeap(mHExpHeap);
				}
				

				//! \brief allocate a block of memory
				//! \param numBytes		number of bytes to allocate
				//! \returns a pointer to the allocated memory block or NULL of no memory could be allocated
				//! \remarks memory is created on external heap and aligned to 32
				virtual void* allocateMemory(const uint32 numBytes)
				{
					return MEMAllocFromExpHeapEx(mHExpHeap, (u32)numBytes, 32);
				}

				//! \brief delete a previously allocated memory block
				//! \param blockPtr		pointer to the block which should be deleted
				virtual void freeMemory(void* blockPtr)
				{
					MEMFreeToExpHeap( mHExpHeap, blockPtr );
				}

			private:
			MEMHeapHandle  mHExpHeap;
			};
		}
	}
}


