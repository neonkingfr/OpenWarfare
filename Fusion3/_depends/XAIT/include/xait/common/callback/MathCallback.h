// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/AllocatorCaptured.h>
#include <xait/common/SharedPtr.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			class MathCallback : public Memory::AllocatorCaptured, public ReferenceCounted
			{
			public:
				typedef SharedPtr<MathCallback, MemberReferenceCount< > >	Ptr;

				//! \brief constrtuctor
				MathCallback()
				{}

				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				MathCallback(Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: AllocatorCaptured(dealloc)
				{}

				//! \brief initialize the random generator
				//! \param seed		An integer value to be used as seed by the pseudo-random number generator algorithm.
				XAIT_DLLEXPORT virtual void initalizeRandomGenerator(uint32 seed);

				//! \brief generates a random number between 0 and max unsigned int
				//! \returns the random number blockPtr		pointer to the block which should be deleted
				XAIT_DLLEXPORT virtual uint32 getRandomNumber();
			};
		}
	}
}



