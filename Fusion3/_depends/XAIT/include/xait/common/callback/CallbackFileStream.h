// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/FileStream.h>
#include <xait/common/callback/FileIOCallback.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			class CallbackFileStream : public IO::Stream
			{
			public:
				//! \brief constructs a closed file stream
				XAIT_COMMON_DLLAPI CallbackFileStream(const FileIOCallback::Ptr& fileCallback);

				//! \brief destructs the file stream
				//! \remark closes the handles
				~CallbackFileStream();

				//! \brief open a file
				//! \param fileName		file to open
				//! \param fMode		mode in which we should open the file
				//! \exception UnAuthorizedAccessException	Given path is a directory, or file is read-only, but an open-for-writing operation was attempted
				//! \exception FileNotFoundException		File or path not found
				//! \exception IOException					Not explicitly handled file io error
				virtual void openFile(const Common::String& fileName, const FileIOCallback::FileMode fMode);

				//! \brief get the name of the opened file
				//! \exception NotSupportedException	There is no file opened
				virtual const Common::String& getName() const;

				// derived methods
				virtual bool canRead() const;
				virtual bool canWrite() const;
				virtual bool canSeek() const;
				virtual bool isClosed() const;
				virtual uint64 getLength() const;
				virtual uint64 getPosition() const;
				virtual uint64 read(void* dstBuffer, const uint64 numBytes);
				virtual void write(const void* srcBuffer, const uint64 numBytes);
				virtual uint64 seek(const int64 offset, const SeekOrigin origin);
				virtual void flush();
				virtual void close();

			protected:
				FileIOCallback::Ptr			mFileIO;
				Common::String				mFileName;
				FileIOCallback::FileMode	mFMode;
				FileIOCallback::FileHandle*	mFHandle;
			};
		}
	}
}
