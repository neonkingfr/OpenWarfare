// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/memory/AllocatorCaptured.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			class MemoryCallback : public Memory::AllocatorCaptured, public ReferenceCounted
			{
			public:
				typedef SharedPtr<MemoryCallback,MemberReferenceCount< > >	Ptr;

				//! \brief constrtuctor
				MemoryCallback()
				{}

				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				MemoryCallback(Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: AllocatorCaptured(dealloc)
				{}

				//! \brief allocate a block of memory
				//! \param numBytes		number of bytes to allocate
				//! \returns a pointer to the allocated memory block or NULL of no memory could be allocated
				virtual void* allocateMemory(const uint32 numBytes)
				{
					return ::operator new(numBytes);
				}

				//! \brief delete a previously allocated memory block
				//! \param blockPtr		pointer to the block which should be deleted
				virtual void freeMemory(void* blockPtr)
				{
					return ::operator delete(blockPtr);
				}
			};

		}
	}
}


