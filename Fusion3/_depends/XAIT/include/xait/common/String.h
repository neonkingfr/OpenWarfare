// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/platform/RuntimeLib.h>
#include <xait/common/container/Vector.h>
#include <xait/common/memory/StaticSTLAllocator.h>
#include <xait/common/StringCFuncs.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/Library.h>

#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT
#include <tchar.h>
#endif

namespace XAIT
{
	namespace Common
	{
		template<typename CHAR_TYPE>
		class BasicString;

		typedef BasicString<Char>		String;

		typedef BasicString<WChar>		WString;
		typedef BasicString<BChar>		BString;


		//! \brief wrapper for null terminated strings 
		template<typename CHAR_TYPE>
		class BasicString
		{
		public:
			typedef CHAR_TYPE				CharType;

		private:
			template<typename CHAR_OTHER_TYPE>
			friend class BasicString;

			typedef Container::Vector<CharType,Memory::StaticSTLAllocator<CharType,Library::getStringAlloc> >	ArrayType;

			ArrayType										mString;

			static const Common::Memory::Allocator::Ptr		NULLALLOC;

		public:

			//! \brief constructor, using the default allocator for the string
			BasicString()
				: mString(NULLALLOC)
			{
				mString.pushBack((CharType)0);
			}


			//! \brief copy constructor
			//! \remark the destination string will use the same allocator as the source string
			BasicString(const BasicString<CharType>& string)
				: mString(string.mString)
			{}

			//! \brief converting copy constructor
			//! \remark the destination string will use the same allocator as the source string
			template<typename OtherCharType>
			BasicString(const BasicString<OtherCharType>& string)
				: mString(NULLALLOC)
			{
				assignNullTermString(string.getConstCharPtr());
			}

			//! \brief constructor from null terminated string
			//! \param string	null terminated string
			BasicString(const BChar* string)
				: mString(NULLALLOC)
			{
				assignNullTermString(string);
			}

			//! \brief constructor from null terminated string
			//! \param string	null terminated string
			BasicString(const WChar* string)
				: mString(NULLALLOC)
			{
				assignNullTermString(string);
			}

			//! \brief constructor from single character
			//! \param character	single character
			BasicString(const BChar character)
				: mString(NULLALLOC)
			{
				mString.pushBack(StringCFuncs<CharType>::ConvertChar(character));
				mString.pushBack((CharType)0);
			}

			//! \brief constructor from single character
			//! \param character	single character
			BasicString(const WChar character)
				: mString(NULLALLOC)
			{
				mString.pushBack(StringCFuncs<CharType>::ConvertChar(character));
				mString.pushBack((CharType)0);
			}


			// \brief type conversion from int32
			BasicString(const int32 value)
				: mString(NULLALLOC)
			{
				BChar buf[16];
				sprintf_s(buf,16,"%d",value);
				assignNullTermString(buf);
			}

			// \brief type conversion from uint32
			BasicString(const uint32 value)
				: mString(NULLALLOC)
			{
				BChar buf[16];
				sprintf_s(buf,16,"%u",value);

				assignNullTermString(buf);
			}

			//! \brief type conversion from int64
			BasicString(const int64 value)
				: mString(NULLALLOC)
			{
				BChar buf[32];
				sprintf_s(buf,32,"%lld",value);

				assignNullTermString(buf);
			}

			//! \brief type conversion from uint64
			BasicString(const uint64 value)
				: mString(NULLALLOC)
			{
				BChar buf[32];
				sprintf_s(buf,32,"%llu",value);

				assignNullTermString(buf);
			}

			// \brief type conversion from float32
			BasicString(const float32 value)
				: mString(NULLALLOC)
			{
				BChar buf[16];
				sprintf_s(buf,16,"%f",value);

				assignNullTermString(buf);
			}

			// \brief type conversion from float64
			BasicString(const float64 value)
				: mString(NULLALLOC)
			{
				BChar buf[32];
				sprintf_s(buf,32,"%lf",value);

				assignNullTermString(buf);
			}

			BasicString(const Math::Vec2f& value)
				: mString(NULLALLOC)
			{
				BChar buf[51];
				sprintf_s(buf,51,"(%f,%f)",value.mX,value.mY);

				assignNullTermString(buf);
			}

			BasicString(const Math::Vec3f& value)
				: mString(NULLALLOC)
			{
				BChar buf[67];
				sprintf_s(buf,67,"(%f,%f,%f)",value.mX,value.mY,value.mZ);

				assignNullTermString(buf);
			}

			//! \brief assign other string constructor
			BasicString<CharType>& operator=(const BasicString<CharType>& string)
			{
				mString= string.mString;
				return *this;
			}
			
			//! \brief destructor
			~BasicString()
			{}


			//! \brief assign a null terminated string
			template<typename T_CHAR_OTHER>
			void assignNullTermString(const T_CHAR_OTHER* str)
			{
				if (str == NULL)
				{
					mString.clear();
					mString.pushBack((CharType)0);
					return;
				}

				const uint32 size= StringCFuncs<T_CHAR_OTHER>::StrLen(str) + 1;
				mString.reserve(size);

				// we include the null termination also in the internal string, for better conversion
				for (uint32 i=0; i < size; ++i)
				{
					mString.pushBack(StringCFuncs<CharType>::ConvertChar(str[i]));
				}
			}


			//! \brief access the string at an index
			//! \param	index
			//! \returns char
			CharType& operator[](const uint32 index)
			{
				return mString[index];
			}

			//! \brief access the string at an index
			//! \param	index
			//! \returns char
			const CharType& operator[](const uint32 index) const
			{
				return mString[index];
			}

			//! \brief replace a string by another one
			//! \param match	search string to replace
			//! \param str		string to insert instead of old one
			BasicString<CharType> replace(const BasicString<CharType>& match, const BasicString<CharType>& str) const
			{
				if (match.isEmpty()) return *this;

				int32 i;
				int32 posLastReplace= 0;		// mark end position of last replace
				//int32 lenStr= str.getLength();
				int32 lenMatch= match.getLength();

				BasicString<CharType> res;

				while ( (i= findSubStr(match,posLastReplace)) >= 0)
				{
					res+= getSubString(posLastReplace,i - posLastReplace) + str;
					posLastReplace= i + lenMatch;
				}

				// append rest
				res+= getSubString(posLastReplace);

				return res;
			}

			//! \brief find first match of the substr and return the startposition
			//! \param subStr		sub string to search for
			//! \param offset		index in the string, where to start
			//! \returns the startposition of the substr or -1 if not found
			int32 findSubStr(const BasicString<CharType>& subStr, const int32 offset= 0) const
			{
				if (subStr.isEmpty()) return -1;

				int32 posSubStr= 0;
				int32 posStr= offset;
				int32 lastFirstMatch= 0;

				while (mString[posStr] && subStr[posSubStr])
				{
					if (mString[posStr] == subStr[posSubStr])
					{
						if (posSubStr == 0) lastFirstMatch= posStr;
						++posSubStr;
					}
					else
					{
						if (posSubStr > 0) 
						{
							// we set to lastFirstMatch, since it will be incremented at the end of the while loop
							posStr= lastFirstMatch;
							posSubStr= 0;
						}
					}
					++posStr;
				}

				if (subStr[posSubStr] == 0) 
				{
					return lastFirstMatch;
				}
				else
				{
					return -1;
				}
			}


			//! \brief get a contant nullterminated string
			//! \returns a null terminated constant string
			inline const CharType* getConstCharPtr() const
			{
				return &(mString[0]);
			}

			//! \brief   return the length of the string
			//! \returns length of string
			uint32 getLength() const
			{
				return (uint32) mString.size() - 1;
			}

			//! \brief  empty check
			//! returns true if the string is empty, false otherwise
			bool isEmpty() const
			{
				return (mString.size() <= 1);
			}

			//! \brief add a character at the end
			void pushBack(const CharType& c)
			{
				mString.back()= c;
				mString.pushBack((CharType)0);
			}

			//! \brief remove the last character
			void popBack()
			{
				mString.popBack();
				mString.back()= (CharType)0;
			}

			//! \brief generate a new string with all letters as uppercase letters
			//! \returns converted string
			BasicString<CharType> toUpper() const
			{
				BasicString<CharType> newString(*this);

				const uint32 size= getLength();

				for (uint32 i= 0; i < size; ++i)
				{
					if (StringCFuncs<CharType>::IsLower(mString[i]))
						newString.mString[i] = StringCFuncs<CharType>::ToUpper(mString[i]);
					else
						newString.mString[i] = mString[i];
				}
				return newString;
			}

			//! \brief generate a new string with all letters as lowercase letters
			//! \returns converted string
			BasicString<CharType> toLower() const
			{
				BasicString<CharType> newString(*this);

				const uint32 size= getLength();

				for (uint32 i= 0; i < size; ++i)
				{
					if (StringCFuncs<CharType>::IsUpper(mString[i]))
						newString.mString[i] = StringCFuncs<CharType>::ToLower(mString[i]);
					else
						newString.mString[i] = mString[i];
				}
				return newString;
			}

			//! \brief check if a character is part of the string
			//! \param character	a single character
			//! \returns true if the character was found in the string
			bool contains(const CharType character) const
			{
				const uint32 size= getLength();

				for (uint32 i= 0; i < size; ++i)
				{
					if (mString[i] == character)
						return true;
				}
				return false;
			}


			//! \brief check if a one character of a set of characters is part of the string
			//! \param characters	a set of characters
			//! \returns true if a at least once character from the set was found in the string
			bool contains(const BasicString<CharType>& characters) const
			{
				uint32 posThis= 0;
				uint32 posChars= 0;

				const uint32 numCharacters = characters.getLength();

				while (mString[posThis])
				{
					posChars= 0;
					while (posChars < numCharacters && characters[posChars] != mString[posThis]) ++posChars;
					if (characters[posChars] == mString[posThis]) return true;

					++posThis;
				}

				return false;
			}

			//! \brief convert string to a bool value
			//! \returns converted bool value
			//! \remarks valid values are true,false,0,1
			inline bool convertToBool() const
			{
				Common::String temp = toLower();
				if (temp == "1" || temp == "true")
					return true;
				else
					return false;
			}

			//! \brief convert string to a float32 value
			//! \returns converted float32 value
			inline float32 convertToFloat32() const
			{
				return (float32)(StringCFuncs<CharType>::ToFloat64(&(mString[0])));
			}

			//! \brief convert string to a float64 value
			//! \returns converted float64 value
			inline float64 convertToFloat64() const
			{
				return (StringCFuncs<CharType>::ToFloat64(&(mString[0])));
			}

			//! \brief convert string to a int08 value
			//! \returns converted int08 value
			inline int08 convertToInt08() const
			{
				return (int08)StringCFuncs<CharType>::ToInt32(&(mString[0]));
			}

			//! \brief convert string to a int16 value
			//! \returns converted int16 value
			inline int16 convertToInt16() const
			{
				return (int16)StringCFuncs<CharType>::ToInt32(&(mString[0]));
			}

			//! \brief convert string to a int32 value
			//! \returns converted int32 value
			inline int32 convertToInt32() const
			{
				return StringCFuncs<CharType>::ToInt32(&(mString[0]));
			}

			//! \brief convert string to a int64 value
			//! \returns converted int64 value
			inline int64 convertToInt64() const
			{
				return StringCFuncs<CharType>::ToInt64(&(mString[0]));
			}

			//! \brief convert string to a uint08 value
			//! \returns converted uint08 value
			inline uint08 convertToUInt08() const
			{
				return (uint08)StringCFuncs<CharType>::ToInt32(&(mString[0]));
			}

			//! \brief convert string to a uint16 value
			//! \returns converted uint16 value
			inline uint16 convertToUInt16() const
			{
				return (uint16)StringCFuncs<CharType>::ToInt32(&(mString[0]));
			}

			//! \brief convert string to a uint32 value
			//! \returns converted uint32 value
			inline uint32 convertToUInt32() const
			{
				return (uint32)StringCFuncs<CharType>::ToInt32(&(mString[0]));
			}

			//! \brief convert string to a uint64 value
			//! \returns converted uint64 value
			inline uint64 convertToUInt64() const
			{
				return (uint64)StringCFuncs<CharType>::ToInt64(&(mString[0]));
			}


			//! \brief convert to vec2f
			Math::Vec2f convertToVec2f() const
			{
				Math::Vec2f tmp(0,0);
				if (_stscanf_s(getConstCharPtr(),_T("%f %f"),&tmp.mX,&tmp.mY) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%f,%f)"),&tmp.mX,&tmp.mY) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%f/%f)"),&tmp.mX,&tmp.mY) > 0) return tmp;

				return tmp;
			}

			//! \brief convert to vec2d
			Math::Vec2d convertToVec2d() const
			{
				Math::Vec2d tmp(0,0);
				if (_stscanf_s(getConstCharPtr(),_T("%lf %lf"),&tmp.mX,&tmp.mY) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%lf,%lf)"),&tmp.mX,&tmp.mY) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%lf/%lf)"),&tmp.mX,&tmp.mY) > 0) return tmp;

				return tmp;
			}

			//! \brief convert to vec2f
			Math::Vec2i32 convertToVec2i32() const
			{
				Math::Vec2i32 tmp(0,0);
				if (_stscanf_s(getConstCharPtr(),_T("%i %i"),&tmp.mX,&tmp.mY) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%i,%i)"),&tmp.mX,&tmp.mY) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%i/%i)"),&tmp.mX,&tmp.mY) > 0) return tmp;

				return tmp;
			}

			//! \brief convert to vec2d
			Math::Vec2u32 convertToVec2u32() const
			{
				Math::Vec2u32 tmp(0,0);
				if (_stscanf_s(getConstCharPtr(),_T("%u %u"),&tmp.mX,&tmp.mY) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%u,%u)"),&tmp.mX,&tmp.mY) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%u/%u)"),&tmp.mX,&tmp.mY) > 0) return tmp;

				return tmp;
			}

			//! \brief convert to vec3f
			Math::Vec3f convertToVec3f() const
			{
				Math::Vec3f tmp(0,0,0);
				if (_stscanf_s(getConstCharPtr(),_T("%f %f %f"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%f,%f,%f)"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%f/%f/%f)"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;

				return tmp;
			}

			//! \brief convert to vec3d
			Math::Vec3d convertToVec3d() const
			{
				Math::Vec3d tmp(0,0,0);
				if (_stscanf_s(getConstCharPtr(),_T("%lf %lf %lf"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%lf,%lf,%lf)"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%lf/%lf/%lf)"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;

				return tmp;
			}

			//! \brief convert to vec3f
			Math::Vec3i32 convertToVec3i32() const
			{
				Math::Vec3i32 tmp(0,0,0);
				if (_stscanf_s(getConstCharPtr(),_T("%i %i %i"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%i,%i,%i)"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%i/%i/%i)"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;

				return tmp;
			}

			//! \brief convert to vec3d
			Math::Vec3u32 convertToVec3u32() const
			{
				Math::Vec3u32 tmp(0,0,0);
				if (_stscanf_s(getConstCharPtr(),_T("%u %u %u"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%u,%u,%u)"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;
				if (_stscanf_s(getConstCharPtr(),_T("(%u/%u/%u)"),&tmp.mX,&tmp.mY,&tmp.mZ) > 0) return tmp;

				return tmp;
			}


			//! \brief return a substring of the string
			//! \param  start	startindex
			//! \param  length	(optional) length of substring
			//! \returns substring
			BasicString<CharType> getSubString(uint32 start, uint32 length = (uint32)-1) const
			{
				BasicString<CharType> result;

				uint32 temp = 0;

				const uint32 myLength= getLength();
				for (uint32 i=start; i < myLength && temp < length; ++i)
				{
					result.pushBack(mString[i]);
					++temp;
				}
				return result;
			}


			//! \brief find first occurrence of a character
			//! \param  charSet		a set of characters
			//! \param	start		(optional) start index
			//! \returns index of the character if it was found in the string; -1 otherwise
			int32 findFirst(const BasicString<CharType>& charSet, uint32 start=0) const
			{
				uint32 posThis= start;
				uint32 posChars= 0;

				X_ASSERT_MSG(start < getLength(),"start index out of range");

				while (mString[posThis])
				{
					posChars= 0;
					while (charSet[posChars] && (charSet[posChars] != mString[posThis])) ++posChars;
					if (charSet[posChars] == mString[posThis]) return posThis;

					++posThis;
				}

				return -1;
			}


			//! \brief find last occurrence of a character
			//! \param  charSet		a set of characters
			//! \param	start		start index (optional)
			//! \returns index of the character if it was found in the string; -1 otherwise
			int32 findLast(const BasicString<CharType>& charSet,const uint32 start = (uint32)-1) const
			{
				uint32 posThis= start;
				uint32 posChars= 0;
				const uint32 size= getLength();

				if (posThis == (uint32)-1)
				{
					posThis= size-1;
				}

				X_ASSERT_MSG(posThis < size,"start index out of range");

				for(uint32 i= posThis ; i != (uint32)-1; --i)
				{
					posChars= 0;
					while (charSet[posChars] && (charSet[posChars] != mString[i])) ++posChars;
					if (charSet[posChars] == mString[i]) return i;
				}

				return -1;
			}


			//! \brief Determines whether the beginning of the String matches a specified string
			//! \param  string	string to compare against
			//! \returns true if the string starts with the comparestring
			bool startsWith(const BasicString<CharType>& string) const
			{
				const uint32 length = string.getLength();

				if (length > mString.size())
					return false;

				for (uint32 i=0; i<length; ++i)
				{
					if (string.mString[i] != mString[i])
						return false;
				}
				return true;
			}

			//! \brief Determines whether a specific char is a whitespace
			//! \param index	index of the char
			//! \returns true if the char is a whitespace
			bool isWhitespace(uint32 index) const
			{
				X_ASSERT_MSG(index < getLength(),"index out of range");
				return StringCFuncs<CharType>::IsWhiteSpace(mString[index]);
			}


			//! \brief removes all whitespace from the beginning
			//! \remark This functions has runtime O(n)
			void trimFront()
			{
				// search index until which we have whitespaces
				uint32 wsEnd= 0;
				while (mString[wsEnd] && StringCFuncs<CharType>::IsWhiteSpace(mString[wsEnd]))
				{
					++wsEnd;
				}

				typename ArrayType::Iterator iter= mString.begin() + wsEnd;
				mString.erase(mString.begin(),iter);
			}


			//! \brief removes all whitespace at the end
			//! \remark This function has runtime O(n)
			void trimEnd()
			{
				const uint32 size = getLength();

				if (size == 0) 
					return;

				uint32 newSize = size;
				for (uint32 i= size-1; i!= X_MAX_UINT32; --i)
				{
					if (!isWhitespace(i))
						break;

					--newSize;
				}
				mString.resize(newSize + 1);
				mString[newSize]= 0;
			}

			//! \brief removes all whitespace from the beginning and at the end
			//! \remark This function has runtime O(n)
			void trim()
			{
				trimFront();
				trimEnd();
			}


			//! \brief generates a vector of BasicStrings split at whitespaces
			void tokenize(Container::Vector<BasicString<CharType> >& tokens) const
			{
				uint32 start, end = 0, len = getLength();

				while ( end != len )
				{
					start = end;
					while ( start < len && StringCFuncs<CharType>::IsWhiteSpace(mString[start]) )
						start++;
					end = start;
					while ( end < len && !StringCFuncs<CharType>::IsWhiteSpace(mString[end]) )
						end++;
					if ( end == len && start == len )
						break;

					tokens.pushBack( getSubString( start, end-start ) );
				}
			}

			//! \brief generates a vector of BasicStrings split at delimiter character
			void tokenize(Container::Vector<BasicString<CharType> >& tokens, CharType delimiter) const
			{
				uint32 start, end = 0, len = getLength();

				while ( end != len )
				{
					start = end;
					while ( start < len && mString[start] == delimiter) 
						start++;
					end = start;
					while ( end < len && !(mString[end] == delimiter) )
						end++;
					if ( end == len && start == len )
						break;

					tokens.pushBack( getSubString( start, end-start ) );
				}
			}

			//! \brief concatenate two strings
			BasicString<CharType> operator+(const BasicString<CharType>& string) const
			{
				BasicString<CharType> newString(*this);

				const uint32 sizeStr= string.getLength();
				const uint32 sizeThis= getLength();

				// resize new string, and we need the null termination
				const uint32 finalSize= sizeStr + sizeThis + 1;
				newString.mString.resize(finalSize,0);

				for (uint32 i= 0; i < sizeStr; ++i)
				{
					newString.mString[i + sizeThis]= string.mString[i];
				}

				return newString;
			}

			//! \brief concatenate string with another one
			BasicString<CharType>& operator+=(const BasicString<CharType>& string)
			{
				const uint32 sizeStr= string.getLength();
				const uint32 sizeThis= getLength();

				// resize new string, and we need the null termination
				const uint32 finalSize= sizeStr + sizeThis + 1;
				mString.resize(finalSize,0);

				for (uint32 i= 0; i < sizeStr; ++i)
				{
					mString[i + sizeThis]= string.mString[i];
				}

				return *this;
			}

			//! \brief compare equality of two strings
			inline bool operator==(const BasicString<CharType>& string) const
			{
				if (mString.size() != string.mString.size())
					return false;

				return StringCFuncs<CharType>::StrCmp(&mString[0],&string[0]) == 0;
			}

			//! \brief compare equality of two strings
			inline bool operator!=(const BasicString<CharType>& string) const
			{
				if (mString.size() != string.mString.size())
					return true;

				return StringCFuncs<CharType>::StrCmp(&mString[0],&string[0]) != 0;
			}


			inline bool operator<(const BasicString<CharType>& string) const
			{
				return StringCFuncs<CharType>::StrCmp(&mString[0],&string[0]) < 0;
			}

		};

		template<typename T_CHAR> const Memory::Allocator::Ptr BasicString<T_CHAR>::NULLALLOC= Memory::Allocator::Ptr();

		template<typename CharType>
		inline std::ostream& operator<<(std::ostream& os, const XAIT::Common::BasicString<CharType>& string)
		{
			for (uint32 i= 0; i < string.getLength(); ++i)
			{
				os << (const XAIT::BChar)string[i];
			}
			return os;
		}

		template<typename T_CHAR>
		inline XAIT::Common::BasicString<T_CHAR> operator+ (const char* a, const XAIT::Common::BasicString<T_CHAR>& b)
		{
			XAIT::Common::BasicString<T_CHAR> result= XAIT::Common::BasicString<T_CHAR>(a) + b;
			return result;
		}

		template<typename T_CHAR>
		inline XAIT::Common::BasicString<T_CHAR> operator+ (const char a, const XAIT::Common::BasicString<T_CHAR>& b)
		{
			XAIT::Common::BasicString<T_CHAR> result= XAIT::Common::BasicString<T_CHAR>(a) + b;
			return result;
		}
	} // namespace Common

	

}
