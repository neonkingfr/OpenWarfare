// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/exceptions/Exception.h>

namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			//! \brief exception for namespace Serialize
			class SerializeException : public Exceptions::Exception
			{
			public:
				SerializeException(const Common::String& errorMsg)
					: Exception(errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "SerializeException";
				}
			};
		}
	}
}
