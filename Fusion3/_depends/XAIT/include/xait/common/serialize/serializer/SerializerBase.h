// (C) 2011 xaitment GmbH

#pragma once

#include <xait/common/debug/Assert.h>
#include <xait/common/serialize/serializer/SerializerHelper.h>
#include <xait/common/serialize/PatchInterface.h>


namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			namespace Serializer
			{
				//! \brief base class for all serializer
				//! \param OBJECT_T			type which should be serialized by this serializer
				//! \param SERIALIZER_T		type name of the serializer
				template<class OBJECT_T>
				class SerializerBase
				{
				public:
					typedef OBJECT_T		ValueType;

					//! \brief init the object
					//! \param object	object to init
					//! \remark The object is uninitialized before this call
					virtual void init(OBJECT_T& object, ConfigStream* serialStream)= 0;

					//! \brief de/serialize the object
					//! \param object			object to serialize/deserialize, depending on the stream state
					//! \param serialStream		stream taking/giving the data for serialize
					//! \remark The object could be already setup if you deserialize
					virtual void serialize(OBJECT_T& object, SerializeStream* serialStream)= 0;

					//! \brief do something immediately before the serialization of the object starts
					virtual void handleBeforeSerialize(OBJECT_T& object)= 0;

					//! \brief do something after the complete serialization is done
					virtual void handleSerializeDone(OBJECT_T& object)= 0;

					//! \brief the object stored in the stream is of a lower version number. 
					//!	This is only executed in deserialization.
					//! \param object			current instance of the object 
					//!							(has at least initialization)
					//! \param streamVersion	version in the stream
					//! \param patchStream		patched stream
					//! \returns version number, to which the stream has been patched, 
					//!			or -1 if no patch is available for that streamVersion
					//! \remark This function will be called several times, until the returned 
					//!			version is equal to the current object version.
					virtual int32 addPatchToStream(OBJECT_T& object, const int32 streamVersion, PatchInterface* patchInterface) { return -1; };

					//! \brief get the current version of the serializer/object
					//! \returns the current version, where negative versions are invalid version.
					virtual int32 getVersion() const= 0;

					//! \brief get the full/base name of the type to be serialized
					virtual Common::String getTypeName() const= 0;

					//! \brief get the subtype for a specific derived class from TypeName
					//! \returns the sub type name of the streamed type (could also be an empty string if not needed)
					virtual Common::String getSubTypeName() const= 0;
				};

			}
		}
	}
}