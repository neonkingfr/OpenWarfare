// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/io/Stream.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>


#define XAIT_SERIALIZE_READ_VALUE_NAME_CHECK(name,value,stream,objectName) \
			{ \
			const char* _name; \
			stream->readValue(_name,value); \
			X_ASSERT_MSG(!strcmp(_name,name), XAIT::Common::String("Serialization error: attribute with type: \"") + _name + XAIT::Common::String("\"  found, but expected: ") + name + XAIT::Common::String(" in object: ") + objectName) ; \
			} \


#define XAIT_SERIALIZE_VERSION_CONFLICT(currentVersion,objectName) \
	XAIT_FAIL(XAIT::Common::String("Serialization error: unknown serialize version:") + XAIT::Common::String(currentVersion) + XAIT::Common::String(" in object: ") + objectName); 


namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			class SerializeStorage : public Memory::MemoryManaged
			{
			public:
				enum StorageType
				{
					STORAGE_BINARY,
				};

				//! \brief types for basic values stored in the stream
				enum StreamID
				{
					SID_BOOL = 1,
					SID_UINT08,
					SID_INT08,
					SID_UINT16,
					SID_INT16,
					SID_UINT32,
					SID_INT32,
					SID_UINT64,
					SID_INT64,
					SID_FLOAT32,
					SID_FLOAT64,
					SID_BSTRING,
					SID_WSTRING,
					SID_VEC2F,
					SID_VEC3F,
					SID_ARRAY_INT08,
					SID_ARRAY_INT32,
					SID_ARRAY_FLOAT32,
					SID_ARRAY_VEC3F,
					SID_CLASS_ENTER,
					SID_CLASS_EXIT,
				};

				enum SerializeMode
				{
					SMODE_INPUT,		//!< serialize attributes in
					SMODE_OUTPUT,		//!< serialize attributes out
				};

				class Position : public Memory::MemoryManaged
				{
				public:
					inline Position(const StorageType type)
						: mType(type)
					{}

					virtual ~Position() {};

					inline StorageType getStorageType() const
					{
						return mType;
					}

					//! \brief determines if two positions are equal
					virtual bool equals(const SharedPtr<Position>& otherPos) const= 0;

				private:
					StorageType		mType;
				};


				//! \brief create a serialize storage
				//! \param storageType	type of the storage (derived type)
				//! \param serialMode	mode of the serialization 
				SerializeStorage(const StorageType storageType, const SerializeMode serialMode)
					: mSerializeMode(serialMode), mStorageType(storageType)
				{}
				

				//! \brief virtual destructor since base class
				virtual ~SerializeStorage() {}

				//! \brief get the serialize mode of the serializer
				inline SerializeMode getSerializeMode() const
				{
					return mSerializeMode;
				}

				//! \brief get the type of the storage
				inline StorageType getStorageType() const
				{
					return mStorageType;
				}

				//! \brief get the name of a stream id
				inline const char* getStreamIDName(const StreamID streamID) const
				{
					return mStreamIDNames[(int32)streamID - 1];
				}

				//! \brief get the current position in the stream
				virtual SharedPtr<Position> getPosition()= 0;

				//! \brief set the storage back to a specified position
				virtual void setPosition(const SharedPtr<Position>& storagePos)= 0;

				//! \brief start/open the serialize storage on an io stream
				//! \param stream		stream to open the serializer on
				//! \remark This should also check the version
				//! \exception NotSupportedException		thrown if the stream has no the needed capabilities for the current serialize mode
				//! \exception VersionMismatchException		thrown if the stream itself cannot be read by this serialize because of a wrong stream version
				virtual bool open(IO::Stream* stream)= 0;

				//! \brief close the serialize storage
				virtual void close() = 0;


				//! \brief read the next value and skip it, just returning the name which should be skipped
				//! \param name		name which has been skipped
				//! \param streamID	type which has been skipped
				virtual void readSkipValue(const char*& name, StreamID& streamID)= 0;				

				//! \brief reads next stream id , but does not skip the content and reverts to the streamid position 
				virtual StreamID readNextStreamID()= 0;

				//! \brief serialize class/struct
				//! \param name[out]		name of the instance
				//! \param typeName[out]	name of the type
				//! \param subTypeName[out]	name of the subtype
				//! \param version[out]		version of the instance
				virtual void readEnterClassTree(const char*& name, const char*& typeName, const char*& subTypeName, int32& version)= 0;

				//! \brief end serialize of a class/struct
				virtual void readExitClassTree()= 0;

				//! \brief serialize/deserialize values (should also store the value type in the stream)
				virtual void readValue(const char*& name, bool& value)= 0;
				virtual void readValue(const char*& name, uint08& value)= 0;
				virtual void readValue(const char*& name, int08& value)= 0;
				virtual void readValue(const char*& name, uint16& value)= 0;
				virtual void readValue(const char*& name, int16& value)= 0;
				virtual void readValue(const char*& name, uint32& value)= 0;
				virtual void readValue(const char*& name, int32& value)= 0;
				virtual void readValue(const char*& name, uint64& value)= 0;
				virtual void readValue(const char*& name, int64& value)= 0;
				virtual void readValue(const char*& name, float32& value)= 0;
				virtual void readValue(const char*& name, float64& value)= 0;
				virtual void readValue(const char*& name, Common::Math::Vec2f& value)= 0;
				virtual void readValue(const char*& name, Common::Math::Vec3f& value)= 0;
				virtual void readValue(const char*& name, Common::BString& value)= 0;
				virtual void readValue(const char*& name, Common::WString& value)= 0;

				//! \brief read an array of the same type
				//! \remarks This does not store the length of the buffer, the external array must be big enough to maintain all data
				virtual void readArray(const char*& name, int08* buffer, const uint32 bufferLen)= 0;
				virtual void readArray(const char*& name, int32* buffer, const uint32 bufferLen)= 0;
				virtual void readArray(const char*& name, float32* buffer, const uint32 bufferLen)= 0;
				virtual void readArray(const char*& name, Common::Math::Vec3f* buffer, const uint32 bufferLen)= 0;


				//! \brief serialize class/struct
				//! \param name			name of the instance 
				//! \param typeName		name of the type
				//! \param subTypeName	name of the subtype
				//! \param version		version of the instance
				virtual void writeEnterClassTree(const char* name, const char* typeName, const char* subTypeName, const int32 version)= 0;

				//! \brief end serialize of a class/struct
				virtual void writeExitClassTree()= 0;

				//! \brief serialize/deserialize values (should also store the value type in the stream)
				virtual void writeValue(const char* name, const bool value)= 0;
				virtual void writeValue(const char* name, const uint08 value)= 0;
				virtual void writeValue(const char* name, const int08 value)= 0;
				virtual void writeValue(const char* name, const uint16 value)= 0;
				virtual void writeValue(const char* name, const int16 value)= 0;
				virtual void writeValue(const char* name, const uint32 value)= 0;
				virtual void writeValue(const char* name, const int32 value)= 0;
				virtual void writeValue(const char* name, const uint64 value)= 0;
				virtual void writeValue(const char* name, const int64 value)= 0;
				virtual void writeValue(const char* name, const float32 value)= 0;
				virtual void writeValue(const char* name, const float64 value)= 0;
				virtual void writeValue(const char* name, const Common::Math::Vec2f& value)= 0;
				virtual void writeValue(const char* name, const Common::Math::Vec3f& value)= 0;
				virtual void writeValue(const char* name, const Common::BString& value)= 0;
				virtual void writeValue(const char* name, const Common::WString& value)= 0;

				virtual void writeArray(const char* name, const int08* buffer, const uint32 bufferLen)= 0;
				virtual void writeArray(const char* name, const int32* buffer, const uint32 bufferLen)= 0;
				virtual void writeArray(const char* name, const float32* buffer, const uint32 bufferLen)= 0;
				virtual void writeArray(const char* name, const Common::Math::Vec3f* buffer, const uint32 bufferLen)= 0;


			protected:
				friend class PatchInterface;

				SerializeMode				mSerializeMode;

			private:
				StorageType					mStorageType;
				static const char*			mStreamIDNames[];

			};
		}
	}
}
