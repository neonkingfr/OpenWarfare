// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/serialize/SerializeException.h>

namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			class TypeMismatchException : public SerializeException
			{
			public:
				TypeMismatchException(const Common::String& errorMsg, const Common::String& foundType, const Common::String& expectedType)
					: SerializeException(errorMsg),mFoundType(foundType),mExpectedType(expectedType)
				{}

				virtual Common::String getExceptionName() const
				{
					return "TypeMismatchException";
				}

				//! \brief get the type which has been found
				inline Common::String getFoundType() const
				{
					return mFoundType;
				}

				//! \brief get the type which has been excepted
				inline Common::String getExpectedType() const
				{
					return mExpectedType;
				}

			protected:
				virtual void getExceptionParameter(Container::List<Container::Pair<Common::String,Common::String> >& parameters)
				{
					SerializeException::getExceptionParameter(parameters);

					parameters.pushBack(Container::MakePair(Common::String("FoundType"),Common::String(mFoundType)));
					parameters.pushBack(Container::MakePair(Common::String("ExpectedType"),Common::String(mExpectedType)));
				}

			private:
				Common::String		mFoundType;
				Common::String		mExpectedType;
			};
		}
	}
}
