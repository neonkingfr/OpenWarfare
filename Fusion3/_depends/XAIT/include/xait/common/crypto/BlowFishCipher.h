// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>
#include <xait/common/crypto/BlockCipher.h>

typedef struct bf_key_st BF_KEY;

namespace XAIT
{
	namespace Common
	{
		namespace Crypto
		{
			//! \brief A block cipher using the blow fish encryption
			class BlowFishCipher : public BlockCipher
			{
			public:
				//! \brief default constructor
				BlowFishCipher();

				//! \brief constructor, set cipher to specific key
				//! \param keyPhrase	key to used for encryption
				BlowFishCipher(const Common::String& keyPhrase);

				//! \brief destructor
				~BlowFishCipher();

				//! \brief set the key for the encryption
				//! \param keyPhrase	key to used for encryption
				void setKey(const Common::String& keyPhrase);

				void encryptBlock(uint08* encData, const uint08* decData);
				void decryptBlock(uint08* decData, const uint08* encData);

			private:
				BF_KEY*		mKey;
			};
		}
	}
}


