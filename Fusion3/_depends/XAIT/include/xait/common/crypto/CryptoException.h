// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/exceptions/Exception.h>

namespace XAIT
{
	namespace Common
	{
		namespace Crypto
		{
			//! \brief exception for error from the namespace crypto
			class CryptoException : public Exceptions::Exception
			{
			public:
				CryptoException(const Common::String& errorMsg)
					: Exception(errorMsg)
				{}

				//! \brief get the name of the exception
				//! \remark The name is usually the exception class name
				virtual Common::String getExceptionName() const
				{
					return "CryptoException";
				}
			};
		}
	}
}
