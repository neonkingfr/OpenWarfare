// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>

// Text:	The amount of <limitedName> is limited to <maxVal> (reached <currVal>)
// The parameter:
// currVal			value that should be checked if valid
// maxVal			maximum allowed value
// limitedName		see text
// returnStatement	return statement that should be executed to leave the function i.e. return or return false or return INVALID_ID ....
#ifdef XAIT_EVALUATION

#define XAIT_EVALERROR_MAX(currVal,maxVal,limitedName,returnStatement)	\
	if (currVal > maxVal) { XAIT::Common::Crypto::EvalError::show("The amount of "limitedName" is limited to " + String(maxVal) + " (reached " + String(currVal) + " )!"); returnStatement; }

#define XAIT_EVALERROR_MAX_HELP(currVal,maxVal,limitedName,helpMsg,returnStatement)	\
	if (currVal > maxVal) { XAIT::Common::Crypto::EvalError::show("The amount of "limitedName" is limited to " + String(maxVal) + " (reached " + String(currVal) + " )!\n" helpMsg); returnStatement; }

#else	// XAIT_EVALUATION

#define XAIT_EVALERROR_MAX(currVal,maxVal,limitedName,returnStatement)
#define XAIT_EVALERROR_MAX_HELP(currVal,maxVal,limitedName,helpMsg,returnStatement)

#endif	// XAIT_EVALUATION

namespace XAIT
{
	namespace Common
	{
		namespace Crypto
		{
			class EvalError
			{
			public:
				XAIT_COMMON_DLLAPI static void show(const Common::String& message);
			};
		}
	}
}
