// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec3.h>
#include <xait/common/container/List.h>
#include <xait/common/container/Vector.h>
#include <xait/common/container/Pair.h>

namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			//! \brief	class for (cubic) Hermite Splines in R^3
			struct HermiteSplineSegment
			{
			public:
				//! \brief	default constructor
				HermiteSplineSegment()
					: mP0(Vec3f())
					, mP1(Vec3f())
					, mT0(Vec3f())
					, mT1(Vec3f())
					, mFirstFactor(Vec3f())
					, mSecondFactor(Vec3f())
				{};

				//! \brief				constructor
				//! \param allocator	memory allocator
				//! \param	p0			first point of the spline
				//! \param	p1			second point of the spline
				//! \param	t0			tangent at p0
				//! \param	t1			tangent at p1
				//! \param  numSamples	number of samples points used to compute the fitting curve (arc length parametrization)
				HermiteSplineSegment(const Vec3f& p0, const Vec3f& p1, const Vec3f& t0, const Vec3f& t1)
					: mP0(p0)
					, mP1(p1)
					, mT0(t0)
					, mT1(t1)
					, mFirstFactor((mP0-mP1)*2.0f+mT0+mT1)
					, mSecondFactor((mP1-mP0)*3.0f-mT0*2.0f-mT1)
				{};

				//! \brief			copy constructor
				//! \param	that	other spline
				HermiteSplineSegment(const HermiteSplineSegment& that)
					: mP0(that.mP0)
					, mP1(that.mP1)
					, mT0(that.mT0)
					, mT1(that.mT1)
					, mFirstFactor(that.mFirstFactor)
					, mSecondFactor(that.mSecondFactor)
				{};

				//! \brief					approximates a spline through the points given in samplePoints
				//! \param	samplePoints	data points
				//! \param	allocator		memory allocator for new elements
				//! \remarks				the first and the last sample is matched exactly
				//! \remarks				the parameter u is spaced equally w.r.t. [0,1]
				XAIT_COMMON_DLLAPI HermiteSplineSegment(const Container::Vector<Vec3f>& samplePoints, const Memory::AllocatorPtr& allocator);

				//! \brief	destructor
				~HermiteSplineSegment()
				{};

				//! \brief		computes the point at u
				//! \param	u	the argument (length along the spline w.r.t [0,1])
				//! \returns	the point at u
				XAIT_COMMON_DLLAPI Vec3f getPoint(const float32 u) const;

				//! \brief		computes the tangent at u
				//! \param	u	the argument (length along the spline w.r.t [0,1])
				//! \returns	the tangent at u
				XAIT_COMMON_DLLAPI Vec3f getTangent(const float32 u) const;

				//! \brief		computes the curvature
				//! \param	u	the argument (length along the spline w.r.t [0,1])
				//! \returns	the curvature at u
				XAIT_COMMON_DLLAPI Vec3f getCurvatureVector(const float32 u) const;

				//! \brief		computes the norm of the curvature
				//! \param	u	the argument (length along the spline w.r.t [0,1])
				//! \returns	the norm of the curvature at u
				XAIT_COMMON_DLLAPI float32 getCurvature(const float32 u) const;

				//! \brief		computes the torsion of the curvature
				//! \param	u	the argument (length along the spline w.r.t [0,1])
				//! \returns	the torsion of the curvature at u
				XAIT_COMMON_DLLAPI float32 getTorsion(const float32 u) const;

				//! \brief		computes the length of the spline
				//! \returns	the length of the spline
				XAIT_COMMON_DLLAPI float32 getLength(const uint08 numIterations = 32) const;

				//! \brief			computes the argument and the offset of a point to this spline
				//! \param	u		argument where the offset is computed (output parameter)
				//! \param	point	point to compute the offset to the curve from
				//! \returns		the offset at u to point
				//! \remarks		works only if length of tangent equals 1 in every point of the spline
				XAIT_COMMON_DLLAPI float32 getOffset(float32& u, const Vec3f& point) const;

				//! \brief					fills a list with all turning points (in u)
				//! \param	turningPoints	the turning points in u (output parameter)
				XAIT_COMMON_DLLAPI void getTurningPoints(Container::List<float32>& turningPoints);

				//! \brief			assignment operator
				//! \param	that	other spline
				//! \returns		assigned spline
				inline HermiteSplineSegment& operator=(const HermiteSplineSegment& that)
				{
					mP0 = that.mP0;
					mP1 = that.mP1;
					mT0 = that.mT0;
					mT1 = that.mT1;
					mFirstFactor = that.mFirstFactor;
					mSecondFactor = that.mSecondFactor;

					return (*this);
				};

				//! \brief			comparison operator (equality)	
				//! \param	that	other spline to be compared
				//! \returns		true if the splines are equal; false otherwise
				inline bool operator==(const HermiteSplineSegment& that) const
				{
					return (mFirstFactor == that.mFirstFactor && mSecondFactor == that.mSecondFactor);
				};

				//! \brief			comparison operator (inequality)	
				//! \param	that	other spline to be compared
				//! \returns		false if the splines are equal; true otherwise
				bool operator!=(const HermiteSplineSegment& that) const
				{
					return !(this->operator ==(that));
				};

				//only for debugging puroses
				XAIT_COMMON_DLLAPI void draw(uint08 numSamples = 64) const;

				//! \brief				approximates this spline with an arcLength parametrization
				//! \param	allocator	memory allocator for new elements
				//! \param	numSamples	number of samples for reparametrization
				XAIT_COMMON_DLLAPI void reparametrize(const Memory::AllocatorPtr& allocator,const uint08 numSamples = 5 );

				XAIT_COMMON_DLLAPI static void MakeArcParametrization(Container::Vector<HermiteSplineSegment>& spline,const Memory::AllocatorPtr& allocator,const uint08 numSamples = 10);


				Vec3f mP0;	//!< start point of the spline
				Vec3f mP1;	//!< end point of the spline
				Vec3f mT0;	//!< tangent at p0
				Vec3f mT1;	//!< tangent at p1


			private:
				Vec3f mFirstFactor;		//!< precomputed factor for spline : 2.0f*(mP0-mP1)+mT0+mT1
				Vec3f mSecondFactor;	//!< precomputed factor for spline : 3.0f*(mP1-mP0)-2.0f*mT0-mT1

				//! \param	allocator		memory allocator for new elements
				void makeArcLengthParametrization(const Container::Vector<Vec3f>& points, const Container::Vector<float32> parameters, const Memory::AllocatorPtr& allocator);

			};

		}
	}
}
