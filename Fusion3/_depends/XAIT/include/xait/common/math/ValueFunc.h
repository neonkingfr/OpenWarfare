// (C) xaitment GmbH 2006-2012

#pragma once


//////////////////////////////////////////////////////////////////////////
///				Math functions from the C library for single values
//////////////////////////////////////////////////////////////////////////
/// Author: Markus Wilhelm
///
/// The functions are wrapped as static functions into template classes so that we can
/// decide which c-function with template specialization.
//////////////////////////////////////////////////////////////////////////

#include <xait/common/FundamentalTypes.h>
#include <xait/common/platform/RuntimeLib.h>

#include <stdlib.h>
#include <math.h>
#include <float.h>


namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			//////////////////////////////////////////////////////////////////////////
			///								Absolute Value

			//! \brief compute absolute value
			inline float32 Abs(const float32 x)
			{
				return fabsf(x);
			}

			//! \brief compute absolute value
			inline float64 Abs(const float64 x)
			{
				return fabs(x);
			}

			//! \brief compute absolute value
			inline uint32 Abs(const uint32 x)
			{
				return x;
			}

			//! \brief compute absolute value
			inline uint64 Abs(const uint64 x)
			{
				return x;
			}

			//! \brief compute absolute value
			inline int32 Abs(const int32 x)
			{
				return abs(x);
			}


			//////////////////////////////////////////////////////////////////////////
			///								Square Root

			inline float32 Sqrt(const float32 x)
			{
				return sqrtf(x);
			}

			inline float64 Sqrt(const float64 x)
			{
				return sqrt(x);
			}

			template<typename T>
			inline T Sqrt(const T x)
			{
				return (T)sqrt((double)x);
			}

			inline float32 InvSqrt(const float32 number)
			{
				int32 i;
				float32 x2, y;
				const float32 threehalfs = 1.5F;

				x2 = number * 0.5F;
				y  = number;
				i  = * ( int32 * ) &y;                       // evil floating point bit level hacking [sic]
				i  = 0x5f3759df - ( i >> 1 );               // what the fuck? [sic]
				y  = * ( float32 * ) &i;
				y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration

				return y;
			}

			//////////////////////////////////////////////////////////////////////////
			///									Sinus

			inline float32 Sin(const float32 x)
			{
				return sinf(x);
			}

			inline float64 Sin(const float64 x)
			{
				return sin(x);
			}

			inline float32 ASin(const float32 x)
			{
				return asinf(x);
			}

			inline float64 ASin(const float64 x)
			{
				return asin(x);
			}

			//////////////////////////////////////////////////////////////////////////
			///									Cosines

			inline float32 Cos(const float32 x)
			{
				return cosf(x);
			}

			inline float64 Cos(const float64 x)
			{
				return cos(x);
			}

			inline float32 ACos(const float32 x)
			{
				return acosf(x);
			}

			inline float64 ACos(const float64 x)
			{
				return acos(x);
			}

			//////////////////////////////////////////////////////////////////////////
			///								 Tan

			inline float32 Tan(const float32 x)
			{
				return tanf(x);
			}

			inline float64 Tan(const float64 x)
			{
				return tan(x);
			}

			inline float32 ATan(const float32 x)
			{
				return atanf(x);
			}

			inline float64 ATan(const float64 x)
			{
				return atan(x);
			}

			inline float32 ATan2(const float32 y, const float32 x)
			{
				return atan2f(y,x);
			}

			inline float64 ATan2(const float64 y, const float64 x)
			{
				return atan2(y,x);
			}

			//////////////////////////////////////////////////////////////////////////
			///									Min
			template<typename T>
			inline const T& Min(const T& a, const T& b)
			{
				return a < b ? a : b;
			}

			template<typename T>
			inline const T& Min(const T& a, const T& b, const T& c)
			{
				if (a < b)
					if (a < c)
						return a;
					else
						return c;
				else
					if (b < c)
						return b;
					else
						return c;
			}

			//////////////////////////////////////////////////////////////////////////
			///									Max
			template<typename T>
			inline const T& Max(const T& a, const T& b)
			{
				return a < b ? b : a;
			}

			template<typename T>
			inline const T& Max(const T& a, const T& b, const T& c)
			{
				if (a > b)
					if (a > c)
						return a;
					else
						return c;
				else
					if (b > c)
						return b;
					else
						return c;
			}


			//////////////////////////////////////////////////////////////////////////
			///									Sign
			template<typename T>
			inline T Sign(const T x)
			{
				return (x < 0) ? (T)-1 : (T)1;
			}

			inline float64 Sign(const float64 x)
			{
				float64 tmp= 1.0;
				tmp= copysign(tmp,x);
				return tmp;
			}

			//! \brief Check if a and b have the same sign
			//! \remark return value is undefined if a or b is zero
			template<typename T>
			inline bool SameSign(const T a, const T b)
			{
				return (a * b) > 0;
			}

			inline float64 SameSign(const float64 a, const float64 b)
			{
				float64 merged= copysign(a,b);
				return merged == a;
			}

			//////////////////////////////////////////////////////////////////////////
			///						Floor


			inline float32 Floor(const float32 x)
			{
				return floorf(x);
			}

			inline float64 Floor(const float64 x)
			{
				return floor(x);
			}

			//////////////////////////////////////////////////////////////////////////
			///						Ceil

			inline float32 Ceil(const float32 x)
			{
				return ceilf(x);
			}

			inline float64 Ceil(const float64 x)
			{
				return ceil(x);
			}

			//////////////////////////////////////////////////////////////////////////
			///						Round

			inline float32 Round(const float32 x)
			{
				return Floor(x + 0.5f);
			}

			inline float64 Round(const float64 x)
			{
				return Floor(x + 0.5);
			}

			//////////////////////////////////////////////////////////////////////////
			///						Clamp
			template<typename T_VALUE>
			inline T_VALUE Clamp(const T_VALUE currVal, const T_VALUE minVal, const T_VALUE maxVal)
			{
				return Min(Max(minVal,currVal),maxVal);
			}


			//////////////////////////////////////////////////////////////////////////
			///						Logarithmus

			inline float32 Log(const float32 x)
			{
				return logf(x);
			}

			inline float64 Log(const float64 x)
			{
				return log(x);
			}

			inline float32 Log(const float32 x, const float32 base)
			{
				return logf(x) / logf(base);
			}

			inline float64 Log(const float64 x, const float64 base)
			{
				return log(x) / log(base);
			}

			//////////////////////////////////////////////////////////////////////////
			///						Exp

			inline float32 Exp(const float32 x)
			{
				return expf(x);
			}

			inline float64 Exp(const float64 x)
			{
				return exp(x);
			}

			//////////////////////////////////////////////////////////////////////////
			///						Pow

			inline float32 Pow(const float32 base, const float32 exp)
			{
				return powf(base,exp);
			}

			inline float64 Pow(const float64 base, const float64 exp)
			{
				return pow(base,exp);
			}

			//////////////////////////////////////////////////////////////////////////
			///						Swap
			template<typename T>
			inline void Swap(T& a, T& b)
			{
				T tmp= a;
				a= b;
				b= tmp;
			}

		}
	}
}

