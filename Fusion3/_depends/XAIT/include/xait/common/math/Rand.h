// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/DLLDefines.h>

namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			//! \brief Random Number Generators
			class Rand
			{
			public:
				//! \brief sets a random starting point for Rand functions
				static XAIT_COMMON_DLLAPI void setSeed(uint32 seed);

				//! \brief returns a random integer value between x and y
				static XAIT_COMMON_DLLAPI int32 getRandInt32(int32 x,int32 y);

				//! \brief returns a random float value between 0 .. 1
				static XAIT_COMMON_DLLAPI float32 getRandFloat();

				//! \brief returns a random float value between [min,max]
				static XAIT_COMMON_DLLAPI float32 getRandFloat(float32 min, float32 max);

				//! \brief returns a random bool value
				static XAIT_COMMON_DLLAPI bool getRandBool();

				//! \brief returns a random float in the range -1 < n < 1
				static XAIT_COMMON_DLLAPI float32 getRandClamped();

				//! \brief delivers a secure random float from the operating system
				static XAIT_COMMON_DLLAPI uint32 getRandSecureUIntMax();
			};
		}
	}
}
