// (C) xaitment GmbH 2006-2012

#pragma once
// ---------------------------------------------------------------------------
//			A Simple TGA Library
//
//	copied from http://www.lighthouse3d.com/opengl/terrain/index.php3?tgalib
//	and modified to use concepts of the common framework
// 
// ---------------------------------------------------------------------------

#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/io/Stream.h>


namespace XAIT
{
	namespace Common
	{
		namespace Image
		{
			namespace TGALib
			{
				enum TGAState 
				{
					TGA_OK	= 0,				//!< no error
					TGA_ERROR_FILE_OPEN,		//!< cannot open file
					TGA_ERROR_READING_FILE,		//!< cannot read file
					TGA_ERROR_INDEXED_COLOR,	//!< when we're presented with a color indexed file 
					TGA_ERROR_MEMORY,			//!< not enough memory to load image
					TGA_ERROR_COMPRESSED_FILE,	//!< when we're presented with a compressed file 
				};

				struct TGAInfo
				{
					TGAState		mStatus;			//!< state of reading/saving tga file
					uint08			mType;				//!< tga type id .... ?
					uint08			mPixelDepth;		//!< number of bits per pixel
					int16			mWidth;				//!< width of image
					int16			mHeight;			//!< height of image
					uint08*			mImageData;			//!< array holding the image

					TGAInfo()
						: mStatus(TGA_OK),mType(0),mPixelDepth(0),mWidth(0),mHeight(0),mImageData(NULL)
					{}
				};


				//! \brief load a tga from a stream
				//! \param info[out]	TGAInfo struct containing loaded image (should be a destroyed one)
				//! \param stream		readable stream
				//! \param allocator	allocator needed to allocate memory block for image 
				//! \returns true if the image could be loaded, false otherwise (error state in info.mStatus)
				bool Load(TGAInfo& info, IO::Stream* stream, const Memory::Allocator::Ptr& allocator); 

				//! \brief load only the header of a tga from a stream
				//! \param info[out]	TGAInfo struct containing only the header information, mImageData will be NULL (should be a destroyed one)
				//! \param stream		readable stream
				//! \returns true if the header could be loaded, false otherwise (error state in info.mStatus)
				bool LoadHeader(TGAInfo& info, IO::Stream* stream);

				//! \brief save a tga image to a stream
				//! \param outStream	stream to write the image into, including header
				//! \param width		The width of the image 
				//! \param height		The height of the image 
				//! \param pixelDepth	The number of bits per pixel, 8 for greyscale, 24 for RGB, and 32 for RGBA 
				//! \param imageData	The image pixels
				//! \returns TGA Error state, TGA_OK if everything went fine
				TGAState Save(IO::Stream* outStream, const int16 width, const int16 height, uint08 pixelDepth, uint08* imageData);

				//! \brief releases the memory used for the image in TGAInfo
				//! \param allocator	allocator which receives the released memory
				void Destroy(TGAInfo &info, const Memory::Allocator::Ptr& allocator);
			}
		}
	}
}
