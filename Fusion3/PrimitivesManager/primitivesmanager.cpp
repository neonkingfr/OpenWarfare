#include "primitivesmanager.h"
#include "simpleshader.h"

#include <d3d9.h>
#include <d3dx9.h>

struct VertexD3D
{
  float x,y,z,w;
  float r,g,b,a;
};

//  -====================================================-
//                      PRIMITIVE3D
//  -====================================================-

Primitive3D::Primitive3D(int size, PrimitiveType type)
{
  _vertices = new BI_Vector3[size];
  _nVertices = size;
  _type = type;
}

Primitive3D::~Primitive3D()
{
  if (_vertices)
    delete [] _vertices;
}

void Primitive3D::SetColor(const BI_Color& color)
{
  _color = color;
}

void Primitive3D::AddPrimitiveToBuffer(char* buffer, int Nvertex) const
{
  for (int i = 0; i < Nvertex; i++)
  {
    VertexD3D vertex;
    int size = sizeof(VertexD3D);
    vertex.x = _vertices[i][0];
    vertex.y = _vertices[i][1];
    vertex.z = _vertices[i][2];
    vertex.w = 1.0f;
    vertex.r = _color.R();
    vertex.g = _color.G();
    vertex.b = _color.B();
    vertex.a = 1.0f;

    memcpy(buffer, &vertex, size);
    buffer += size;
  }
}

//  -====================================================-
//                        TRIANGLE3D
//  -====================================================-

Triangle3D::Triangle3D(const BI_Vector3& v1, const BI_Vector3& v2, const BI_Vector3& v3) : Primitive3D(3,Triangle)
{
  _vertices[0] = v1;
  _vertices[1] = v2;
  _vertices[2] = v3;
}


//  -====================================================-
//                        Line3D
//  -====================================================-

Line3D::Line3D(const BI_Vector3& v1, const BI_Vector3& v2) : Primitive3D(2,Line)
{
  _vertices[0] = v1;
  _vertices[1] = v2;
}


//  -====================================================-
//                   PRIMITIVES MANAGER
//  -====================================================-

PrimitivesManager::PrimitivesManager()
{
  for (int i = 0; i < Primitive3D::NTypes; i++)
  {
    _vertexBuffer[i] = NULL;
    _vbPrimitives[i] = 0;
  }
  _transform = BI_Matrix4(1,0,0,0,1,0,0,0,1,0,0,0);
  _ps = NULL;
  _vs = NULL;
  _typePrimitives[Primitive3D::Triangle] = D3DPT_TRIANGLELIST; 
  _typePrimitives[Primitive3D::Line] = D3DPT_LINELIST;
}

PrimitivesManager::~PrimitivesManager()
{
  _primitives.clear();
  for (int i = 0; i < Primitive3D::NTypes; i++)
  {
    if (_vertexBuffer[i] != NULL)
    {
      OutputDebugString("PrimitivesManager: Warning - vertex buffer was not released!\n");
    }
  }

  if (_ps || _vs || _vertexDecl) 
  {
    OutputDebugString("PrimitivesManager: Warning - shaders were not released!\n");
  }
}

void PrimitivesManager::CreateShaders(IDirect3DDevice9* device)
{
#if MANAGER_DEBUG
  OutputDebugString("PrimitivesManager - creating shaders.\n");
#endif

  // Compile pixel & vertex shaders
  // The VS & PS are contained in the same HLSL file, using functions "vsmain" and "psmain"
  LPD3DXBUFFER codevs, codeps;
  LPD3DXBUFFER err = NULL;

  if (D3DXCompileShader(SimpleShaderStr, strlen(SimpleShaderStr), NULL, NULL, "vsmain", "vs_3_0", NULL, &codevs, &err, NULL) != D3D_OK)
  {
    if(err) OutputDebugString("PrimitivesManager: Error compiling vertex shader\n");
    else    OutputDebugString("PrimitivesManager: Error compiling vertex shader (file not found?)\n");
    return;
  }

  if (D3DXCompileShader(SimpleShaderStr, strlen(SimpleShaderStr), NULL, NULL, "psmain", "ps_3_0", NULL, &codeps, &err, NULL) != D3D_OK)
  {
    if(err) OutputDebugString("PrimitivesManager: Error compiling pixel shader\n");
    else    OutputDebugString("PrimitivesManager: Error compiling pixel shader (file not found?)\n");
    return;
  }

  device->CreateVertexShader((DWORD*)codevs->GetBufferPointer(), &_vs);
  device->CreatePixelShader((DWORD*)codeps->GetBufferPointer(), &_ps);
 
  codevs->Release();
  codeps->Release();

  // Create vertex declaration
  // Similar to the FVF in fixed function pipeline, this sets the structure of our vertex data,
  // it must match the vertex data we use and the vertex shader input (VS_INPUT in hlsl)
  D3DVERTEXELEMENT9 decl[] = {{0, 0, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0}, 
                              {0, 16, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0}, 
                              D3DDECL_END()};
  if(device->CreateVertexDeclaration(decl, &_vertexDecl) != D3D_OK)
  {
    OutputDebugString("PrimitivesManager: CreateVertexDeclaration failed!\n");
    return;
  }
}

void PrimitivesManager::ReleaseShaders()
{
#if MANAGER_DEBUG
  OutputDebugString("PrimitivesManager - releasing shaders.\n");
#endif

  if (_ps) _ps->Release();
  if (_vs) _vs->Release();
  if (_vertexDecl) _vertexDecl->Release();
  _ps = NULL;
  _vs = NULL;
  _vertexDecl = NULL;
}

void PrimitivesManager::Add(SmartRef<Primitive3D> primitive)
{
  _primitives.push_back(primitive);
}

void PrimitivesManager::Add(std::vector<SmartRef<Primitive3D>>& primitives)
{
  std::vector<SmartRef<Primitive3D>>::iterator it;
  for (it = primitives.begin(); it != primitives.end(); it++)
  {
    _primitives.push_back(*it);
  }
}

void PrimitivesManager::SetCustomTransform(const BI_Matrix4& transform)
{
  _transform = transform;
}

void PrimitivesManager::PrepareDraw(IDirect3DDevice9* device)
{
  if (!_ps && !_vs)
    CreateShaders(device);
  
  ReleaseVB(); // release previous VB

  if (device && _primitives.size() > 0)
  {
    // Create vertex buffer
    AddTotalPrimitives(_vbPrimitives);
    for (int type = 0; type < Primitive3D::NTypes; type++)
    {
      if (_vbPrimitives[type] != 0)
        device->CreateVertexBuffer(sizeof(VertexD3D) * _vbPrimitives[type], D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &_vertexBuffer[type], NULL);
      else 
        _vertexBuffer[type] = NULL;
    }
    
    // lock buffer
    char* buffer[Primitive3D::NTypes];
    for (int i = 0; i < Primitive3D::NTypes; i++)
    {
      buffer[i] = NULL;
      if (_vertexBuffer[i] != NULL)
      {
        VOID* pData;
        _vertexBuffer[i]->Lock(0, 0, (void**)&pData, 0);
        buffer[i] = (char*)pData;
      }
    }

    //copy data to buffer
    std::vector<SmartRef<Primitive3D>>::iterator it;
    for (it = _primitives.begin(); it != _primitives.end(); it++)
    {

      if (_vertexBuffer[(*it)->_type] != 0 && buffer[(*it)->_type] != NULL)
      {
        (*it)->AddPrimitiveToBuffer(buffer[(*it)->_type],(*it)->GetNVertices());
        buffer[(*it)->_type] += sizeof(VertexD3D) * (*it)->GetNVertices();
      }
    }
  
    // unlock buffer
    for (int i = 0; i < Primitive3D::NTypes; i++)
    {
      if (_vertexBuffer[i] != NULL)
       _vertexBuffer[i]->Unlock();
    }
    buffer[0]=NULL;
    buffer[1]=NULL;
  }

  // clear all primitives (they are already in VB)
    _primitives.clear();

}


void PrimitivesManager::Draw(VBS2CBInterface* device)
{
  bool noEmpty = false;
  for (int i = 0; i < Primitive3D::NTypes; i++)
  {
    if (_vbPrimitives[i] > 0)
    {
      noEmpty = true;
      break;
    }
  }
  if (device && _vs && _ps && noEmpty)
  {

    // Set device to default state
    SetDefaultState(device);
    
    // Create projection matrix from engine frustum data
    D3DXMATRIX matProj;
    float m = _frustum.clipDistNear;
    float left = _frustum.projTanLeft * m;
    float right = _frustum.projTanRight * m;
    float bottom = _frustum.projTanBottom * m;
    float top = _frustum.projTanTop * m;
    D3DXMatrixPerspectiveOffCenterLH(&matProj, -left, right, -bottom, top, _frustum.clipDistNear,_frustum.clipDistFar);

    // Create view matrix from engine camera data
    D3DXMATRIX matView;
    D3DXVECTOR3 cpoint = D3DXVECTOR3((float)_frustum.pointPosX, (float)_frustum.pointPosY, (float)_frustum.pointPosZ);
    D3DXVECTOR3 ctarget = cpoint + D3DXVECTOR3(_frustum.viewDirX, _frustum.viewDirY, _frustum.viewDirZ); // Convert camera direction vector to look-at-target
    D3DXVECTOR3 cup = D3DXVECTOR3(_frustum.viewUpX, _frustum.viewUpY, _frustum.viewUpZ);
    D3DXMatrixLookAtLH(&matView, &cpoint, &ctarget, &cup);

    // Create view*projection matrix and transpose
    D3DXMATRIX viewProj = matView * matProj;
    D3DXMatrixTranspose(&viewProj, &viewProj);

    // Set the matrices to VS constants so they can be accessed in the vertex shader
    device->SetVertexShaderConstantF(4, viewProj, 4); // Vertex Shader register C4 = float4x4 MatViewProj in HLSL
    
    device->SetVertexShader(_vs);
    device->SetPixelShader(_ps);  
    device->SetVertexDeclaration(_vertexDecl);

    device->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
    device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
    //set source

    D3DXMATRIX world;
    D3DXMatrixIdentity(&world);
    for (int j = 0; j < 3; j++)
    {
      for (int i = 0; i < 3; i++)
      {
        float& val = world(j, i);
        val = _transform[i][j];
      }
      world(3, j) = _transform.GetPos()[j];
    }
    D3DXMatrixTranspose(&world, &world);
    device->SetVertexShaderConstantF(0, world, 4);     // Vertex Shader register C0 = float4x4 MatWorld in HLSL

   // draw vertex buffer
  for (int type = 0; type < Primitive3D::NTypes; type++)
  {
    if (!_vertexBuffer[type]) continue;
    device->SetStreamSource(0, _vertexBuffer[type], 0, sizeof(VertexD3D));
    if (_vbPrimitives[type] > 0)
    {
      if (type == Primitive3D::Triangle)
        _vbPrimitives[type] /= 3;
      else
        _vbPrimitives[type] /= 2;
      device->DrawPrimitive(_typePrimitives[type], 0, _vbPrimitives[type]);
    }
  }
    
  }

  _transform = BI_Matrix4(1,0,0,0,1,0,0,0,1,0,0,0);
}

void PrimitivesManager::ReleaseVB()
{
  for (int i = 0; i < Primitive3D::NTypes; i ++)
  {
    if (_vertexBuffer[i])
    {
      _vertexBuffer[i]->Release();
      _vertexBuffer[i] = NULL;
      _vbPrimitives[i] = 0;
    }
  }
}

void PrimitivesManager::AddTotalPrimitives(int *vbPrimitives)
{
  std::vector<SmartRef<Primitive3D>>::iterator it;
  for (it = _primitives.begin(); it != _primitives.end(); it++)
  {
    vbPrimitives[(*it)->_type] += (*it)->GetNVertices();
  }
}

void PrimitivesManager::ReleaseAll()
{
  ReleaseShaders();
  ReleaseVB();
}

void PrimitivesManager::SetDefaultState(VBS2CBInterface* d3dd)
{
  // Set D3D device properties to their default state
  // This is to ensure things are in a predictable state when coming from engine rendering code
  const float zerof = 0.0f;
  const float onef = 1.0f;
#define ZEROf	*((DWORD*) (&zerof))
#define ONEf	*((DWORD*) (&onef))

  d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
  d3dd->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
  d3dd->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
  d3dd->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
  d3dd->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_LASTPIXEL, TRUE);
  d3dd->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
  d3dd->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
  d3dd->SetRenderState(D3DRS_ALPHAREF, 0);
  d3dd->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGCOLOR, 0);
  d3dd->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_FOGSTART, ZEROf);
  d3dd->SetRenderState(D3DRS_FOGEND, ONEf);
  d3dd->SetRenderState(D3DRS_FOGDENSITY, ONEf);
  d3dd->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_STENCILREF, 0);
  d3dd->SetRenderState(D3DRS_STENCILMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_TEXTUREFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_WRAP0, 0);
  d3dd->SetRenderState(D3DRS_WRAP1, 0);
  d3dd->SetRenderState(D3DRS_WRAP2, 0);
  d3dd->SetRenderState(D3DRS_WRAP3, 0);
  d3dd->SetRenderState(D3DRS_WRAP4, 0);
  d3dd->SetRenderState(D3DRS_WRAP5, 0);
  d3dd->SetRenderState(D3DRS_WRAP6, 0);
  d3dd->SetRenderState(D3DRS_WRAP7, 0);
  d3dd->SetRenderState(D3DRS_CLIPPING, TRUE);
  d3dd->SetRenderState(D3DRS_AMBIENT, 0);
  d3dd->SetRenderState(D3DRS_LIGHTING, TRUE);
  d3dd->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_COLORVERTEX, TRUE);
  d3dd->SetRenderState(D3DRS_LOCALVIEWER, TRUE);
  d3dd->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
  d3dd->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
  d3dd->SetRenderState(D3DRS_SPECULARMATERIALSOURCE, D3DMCS_COLOR2);
  d3dd->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_CLIPPLANEENABLE, 0);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MIN, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_PATCHEDGESTYLE, D3DPATCHEDGE_DISCRETE);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MAX, ONEf);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_POSITIONDEGREE, D3DDEGREE_CUBIC);
  d3dd->SetRenderState(D3DRS_NORMALDEGREE, D3DDEGREE_LINEAR);
  d3dd->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_MINTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_MAXTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_X, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Y, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Z, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_W, ZEROf);
  d3dd->SetRenderState(D3DRS_ENABLEADAPTIVETESSELLATION, FALSE);
  d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE1, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE2, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE3, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);
  d3dd->SetRenderState(D3DRS_DEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_WRAP8, 0);
  d3dd->SetRenderState(D3DRS_WRAP9, 0);
  d3dd->SetRenderState(D3DRS_WRAP10, 0);
  d3dd->SetRenderState(D3DRS_WRAP11, 0);
  d3dd->SetRenderState(D3DRS_WRAP12, 0);
  d3dd->SetRenderState(D3DRS_WRAP13, 0);
  d3dd->SetRenderState(D3DRS_WRAP14, 0);
  d3dd->SetRenderState(D3DRS_WRAP15, 0);
  d3dd->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_DITHERENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_VERTEXBLEND, D3DVBF_DISABLE);
  d3dd->SetRenderState(D3DRS_POINTSIZE, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_POINTSCALE_A, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_B, ZEROf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_C, ZEROf);
  d3dd->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_TWEENFACTOR, ZEROf);
  d3dd->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, FALSE);

  for(int i=0;i<8;i++) {
    d3dd->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    d3dd->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    d3dd->SetSamplerState(i, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
    d3dd->SetSamplerState(i, D3DSAMP_MIPMAPLODBIAS, 0);
    d3dd->SetSamplerState(i, D3DSAMP_MAXMIPLEVEL, 0);			
    d3dd->SetSamplerState(i, D3DSAMP_MAXANISOTROPY, 1);
    d3dd->SetSamplerState(i, D3DSAMP_SRGBTEXTURE, 0);			
    d3dd->SetSamplerState(i, D3DSAMP_ELEMENTINDEX, 0);
    d3dd->SetSamplerState(i, D3DSAMP_DMAPOFFSET, 0);			
    d3dd->SetSamplerState(i, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
    d3dd->SetSamplerState(i, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
    d3dd->SetSamplerState(i, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);

    d3dd->SetTexture(i, NULL);
  }

#undef ZEROf
#undef ONEf
}
