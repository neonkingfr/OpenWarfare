#include <string>
#include <fstream>

#include "exports.h"
#include "FPSample.h"
#include "VBSFusionCallBackDefinitions.h"


using namespace VBSFusion;

//----------
static string displayString = "Mission Started";
static VBSFusion::Unit player;
static bool started=false;
bool active = false;
bool fusion = false;
static Vehicle vehicle0;
static Group agv_grp;
static Vehicle targetVehi;
static VBSFusion::Unit commander;
static VBSFusion::Unit driver;
static VBSFusion::Unit gunner;
static position3D tempPos;
static VehicleList vl;
static string vehiWeaponName = "vbs2_pkm_veh_coax";
//-------------


// Function will be called each frame
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if(fusion)
    DisplayFunctions::DisplayString(displayString);
};

// Function will be called when this plugin is loaded. It will print in output console.
VBSPLUGIN_EXPORT int WINAPI OnLoadEx (VBSParametersEx *params)
{
  char *outp = "{info} FPSample: OnLoadEx()\n";
  OutputDebugString(outp);
  return 1;
}

// Function will be called when this plugin is unloaded
VBSPLUGIN_EXPORT void WINAPI OnUnload ()
{
  char *outp = "{info} FPSample: OnUnLoad()\n";
  OutputDebugString(outp);
}

// Function will be called when a mission is loaded
// Preview a unit as a Player and message will be displayed
VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  DisplayFunctions::DisplayString(displayString);
}


// Function will be called to execute plugin
// Locate a unit as a player and preview it. Run FusionFunction.
// Location for a tank and target with imput = s automatically.
// res = fusionFunction["FPSample.dll", "s"]. Before to run with options 'm', 'n' or 'o' use script command doTarget. e.g. veh0 doTarget targ
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static const char result[]="[True]";

  fusion = true;
  if (input[0] == 'p') // load from vehicles which are pre created with RTE
    //  vehicle must have "vbs2_pkm_veh_coax" weapon
    //   ( better to use some vehicle type like "VBS2_CZ_Army_T72_W_X" )
  {
    if(!started)
    {
      VehicleListUtilities::loadVehicleList(vl);
      bool foundVehi = false;
      VehicleArmedModule * vam = NULL;
      for(VehicleList::iterator it = vl.begin(); it < vl.end(); it++)
      {
        VehicleUtilities::updateVehicle(*it);
        VehicleUtilities::updateTurrets(*it);
        vam = (*it).getArmedModule();
        if(NULL != vam)
        {
          vehicle0.setNetworkID((*it).getNetworkID());
          ControllableObjectUtilities::updateStaticProperties(vehicle0);
          foundVehi = true;
          displayString = "Found an Armed Vehicle (" + vehicle0.getName() + ") ";
          break;
        }
      }
      if(foundVehi)
      {
        foundVehi = false;
        for(VehicleList::iterator it = vl.begin(); it < vl.end(); it++)
        {
          if(!(vehicle0.getNetworkID() == (*it).getNetworkID()))
          {
            targetVehi.setNetworkID((*it).getNetworkID());
            VehicleUtilities::updateStaticProperties(targetVehi);
            displayString = displayString + "and a target vehicle (" + targetVehi.getName() + ").";
            foundVehi = true;
            started = true;
            break;
          }
        }
        if(!foundVehi)
        {
          displayString = "Please create a target vehicle.";
        }
      }
      else
      {
        displayString = "No Armed Vehicle Found";
      }
    }
  }

  if (input[0] == 's') // initialize the ArmedGroundVehicle and a target vehicle
  {
    if(!started)
    {
      player = MissionUtilities::getPlayer();
      UnitUtilities::updateStaticProperties(player);
      UnitUtilities::updateDynamicProperties(player);

      GroupUtilities::createGroup(agv_grp);
      GroupUtilities::updateGroupStaticProperties(agv_grp);
      GroupUtilities::updateGroupDynamicProperties(agv_grp);

      tempPos.setY(player.getPosition().getY() + 4);
      tempPos.setX(player.getPosition().getX() + 50);
      tempPos.setZ(player.getPosition().getZ() + 50);
      vehicle0.setPosition(tempPos);
      vehicle0.setGroup(agv_grp);
      vehicle0.setType(std::string("VBS2_CZ_Army_T72_W_X"));
      VehicleUtilities::createVehicle(vehicle0);
      ControllableObjectUtilities::updateStaticProperties(vehicle0);
      ControllableObjectUtilities::updateDynamicProperties(vehicle0);

      commander.setPosition(tempPos);
      UnitUtilities::CreateUnit(commander,agv_grp);
      UnitUtilities::updateStaticProperties(commander);
      UnitUtilities::updateDynamicProperties(commander);
      VehicleUtilities::applyMoveInAsCommander(vehicle0, commander);

      driver.setPosition(tempPos);
      UnitUtilities::CreateUnit(driver,agv_grp);
      UnitUtilities::updateStaticProperties(driver);
      UnitUtilities::updateDynamicProperties(driver);
      VehicleUtilities::applyMoveInAsDriver(vehicle0, driver);

      gunner.setPosition(tempPos);
      UnitUtilities::CreateUnit(gunner,agv_grp);
      UnitUtilities::updateStaticProperties(gunner);
      UnitUtilities::updateDynamicProperties(gunner);
      VehicleUtilities::applyMoveInAsGunner(vehicle0, gunner);

      tempPos.setX(player.getPosition().getX() + 10);
      tempPos.setZ(player.getPosition().getZ() + 10);
      targetVehi.setPosition(tempPos);
      VehicleUtilities::createVehicle(targetVehi);

      displayString="Armed Ground Vehicle and a target Vehicle Created";
      started=true;
    }
  }

  else if (input[0] == 'r') // re-create the target vehicle. ( in case the target vehicle is destroyed )
  {
    if(started)
    {
      if(input[1] == 's')
      {
        VehicleUtilities::updateStaticProperties(targetVehi);
        VehicleUtilities::updateDynamicProperties(targetVehi);
        tempPos = targetVehi.getPosition();
        VehicleUtilities::deleteVehicle(targetVehi);
        targetVehi.setPosition(tempPos);
      }
      else
      {
        VehicleUtilities::updateStaticProperties(vehicle0);
        VehicleUtilities::updateDynamicProperties(vehicle0);
        VehicleUtilities::deleteVehicle(targetVehi);
        tempPos.setX(vehicle0.getPosition().getX() - 40);
        tempPos.setZ(vehicle0.getPosition().getZ() - 40);
        targetVehi.setPosition(tempPos);
      }

      VehicleUtilities::createVehicle(targetVehi);
      displayString = "Target vehicle Re-Created";
    }
  }


  else if (input[0] == 'm')
  {
    if(started)
    {
      VehicleUtilities::updateStaticProperties(vehicle0);
      VehicleUtilities::updateDynamicProperties(vehicle0);
      VehicleUtilities::applyFire(vehicle0, vehiWeaponName);
      displayString = "fire() : function invoked";
    }
  }

  else if (input[0] == 'n')
  {
    if(started)
    {
      VehicleUtilities::updateStaticProperties(vehicle0);
      VehicleUtilities::updateDynamicProperties(vehicle0);
      VehicleUtilities::updateStaticProperties(targetVehi);
      VehicleUtilities::updateDynamicProperties(targetVehi);
      VehicleUtilities::doFire(vehicle0,targetVehi);
      displayString = "doFire() : function invoked";
    }
  }

  else if (input[0] == 'o')
  {
    if(started)
    {
      VehicleUtilities::updateStaticProperties(vehicle0);
      VehicleUtilities::updateDynamicProperties(vehicle0);
      VehicleUtilities::updateStaticProperties(targetVehi);
      VehicleUtilities::updateDynamicProperties(targetVehi);
      VehicleUtilities::commandFire(vehicle0,targetVehi);
      displayString = "commandFire() : function invoked";
    }
  }

  else if(input[0] == 'k')
  {	
    started=false;
  }  
  return result;
};


/*!
Do not modify this code. Defines the DLL main code fragment
*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }

  return TRUE;
};


