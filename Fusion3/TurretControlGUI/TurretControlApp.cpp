#include "TurretControlApp.hpp"
#include "TurretControlGUI.hpp"

#include "TurretControl.h"

IMPLEMENT_APP_GET_ONLY(TurretControlApp)

//_________________________________________________________________________
static wxAppConsole *wxCreateApp()
{
  wxAppConsole::CheckBuildOptions(WX_BUILD_OPTIONS_SIGNATURE,"Turret control");
  return new TurretControlApp;
}

//_________________________________________________________________________
void RunApp()
{
  wxApp::SetInitializerFunction(wxCreateApp);
  wxEntry(0,NULL);
}

///////////////////////////////////////////////////////////////////////////
//_________________________________________________________________________
TurretControlApp::TurretControlApp()
:_mainFrame(NULL)
{
};

//_________________________________________________________________________
TurretControlApp::~TurretControlApp()
{
};

//_________________________________________________________________________
bool TurretControlApp::OnInit()
{
  _mainFrame = new MainFrame();
  _mainFrame->Show(true);

  std::vector<std::string>::const_iterator iter;
  std::string activeTurret = "";
  std::vector<std::string> turrets = ::ListTurrets();
  for (iter = turrets.begin(); iter < turrets.end(); iter++)
  {
    if (activeTurret.length() == 0)
      activeTurret = (*iter);
    AddTurret((*iter).c_str());
  }

  SetActiveTurret(activeTurret.c_str());
  ::SetActiveTurret(activeTurret.c_str());

  return true;
}


//_________________________________________________________________________
void TurretControlApp::UpdateVStab(bool val)
{
  SetElevationStabil(val);
}

//_________________________________________________________________________
void TurretControlApp::UpdateHStab(bool val)
{
  SetAzimuthStabil(val);
}

//_________________________________________________________________________
void TurretControlApp::UpdateVSpeed(double val)
{
  SetElevationSpeed(val);
}

//_________________________________________________________________________
void TurretControlApp::UpdateHSpeed(double val)
{
  SetAzimutSpeed(val);
}

#if !TURRET_CONTROL_ONLY_SPEED
//_________________________________________________________________________
void TurretControlApp::UpdateElev(double val)
{
  SetElevation(val);
}

//_________________________________________________________________________
void TurretControlApp::UpdateAzim(double val)
{
  SetAzimut(val);
}

//_________________________________________________________________________
double TurretControlApp::GetAzimut()
{
  return ::GetAzimut();
}

//_________________________________________________________________________
double TurretControlApp::GetElevation()
{
  return ::GetElevation();
}
#endif


//_________________________________________________________________________
// on active turret changed
void TurretControlApp::UpdateActiveTurret(const char* turretName)
{
  ::SetActiveTurret(turretName);
}

//_________________________________________________________________________
// add new turret identifier to the choice list
void TurretControlApp::AddTurret(const char* turretName)
{
  _mainFrame->AddTurret(turretName);
}

//_________________________________________________________________________
// get currently selected item from the choice list
const char* TurretControlApp::GetActiveTurret()
{
  return _mainFrame->GetActiveTurret().data();
}

//_________________________________________________________________________
// set turret as active (returns false if the turret is not in the list)
bool TurretControlApp::SetActiveTurret(const char *turretName)
{
  return _mainFrame->SetActiveTurret(turretName);
}


