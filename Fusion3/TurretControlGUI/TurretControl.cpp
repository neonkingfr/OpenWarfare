/****************************************************************************
* Plugin for functionality demonstration of turrets controllers             *
*   VBSFusion::VehicleUtilities::applyOpticsDirection                      *
*   VBSFusion::VehicleUtilities::applyTurretDirectionChange                *
*   VBSFusion::VehicleUtilities::applyTurretSpeed                          *
*   VBSFusion::VehicleUtilities::applyTurretStabilizaton                   *
****************************************************************************/

#pragma warning(disable: 4996)
#pragma warning(disable: 4251)

#define WEAPON_HANDLING 1
#define _BISIM_DEV_CHANGE_TURRET_DIRECTION 1
#define WEAPON_HANDLING_OLD 1
#define _BISIM_DEV_APPLY_TURRET_FUNCTION 1


#include <stdio.h>
#include <process.h>
#include <map>
#include <VBSFusion.h>
#include "TurretControl.h"
#include "TurretControlApp.hpp"

#include <util/MissionUtilities.h>
#include <util/VehicleUtilities.h>
#include <util/UnitUtilities.h>
#include <data/Turret.h>


#pragma region Global variables

/// Handle of UI application thread
HANDLE gThreadHandle = NULL;

/// Since the UI runs in it's own thread we need critical section to solve thread conflicts
static CRITICAL_SECTION gCriticalSection;

bool gPositionChanged = true; //true to reset VBS2 values when dialog first time opened

double gNewAzimuthSpeed = 0;
double gNewElevationSpeed = 0;
bool gSpeedChanged = true;

bool gNewAzimuthStabil = false;
bool gNewElevationStabil = false;
bool gStabilChanged = true;

std::string gActiveTurret = "";  // string representing currently selected turret
std::map<std::string, std::vector<int>> gTurrets;  // map of <string_identifier, turret_path>
std::vector<std::string> vecTorr;

#pragma endregion Global variables

#define _DIAG 1
#define LOG_HEADER "[TurretControlGUI] "
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugString(buffer);
#endif
}


#pragma region Functions used by UI
void SetAzimutSpeed(double val)
{
  EnterCriticalSection(&gCriticalSection);
  gSpeedChanged = true;
  gNewAzimuthSpeed = val; 
  LeaveCriticalSection(&gCriticalSection);
}

void SetElevationSpeed(double val)
{
  EnterCriticalSection(&gCriticalSection);
  gSpeedChanged = true;
  gNewElevationSpeed = val; 
  LeaveCriticalSection(&gCriticalSection);
}

void SetAzimuthStabil(bool val)
{
  EnterCriticalSection(&gCriticalSection);
  gStabilChanged = true;
  gNewAzimuthStabil = val; 
  LeaveCriticalSection(&gCriticalSection);
}

void SetElevationStabil(bool val)
{
  EnterCriticalSection(&gCriticalSection);
  gStabilChanged = true;
  gNewElevationStabil = val; 
  LeaveCriticalSection(&gCriticalSection);
}

void SetActiveTurret(const char *turret)
{
  EnterCriticalSection(&gCriticalSection);
  gActiveTurret = turret;
  gStabilChanged = true;
  gSpeedChanged = true;
  gPositionChanged = true;
  LeaveCriticalSection(&gCriticalSection);
}
#pragma endregion Functions used by UI


DWORD WINAPI ThreadProc(LPVOID lpParameter)
{
  // Start the UI
  RunApp();
  gThreadHandle = NULL;
  return true;
}

// Call to fill combo box with all available turrets.
std::vector<std::string> GetTurrets()
{
  EnterCriticalSection(&gCriticalSection);
  std::vector<std::string> result;

  VBSFusion::Unit playerUnit = VBSFusion::MissionUtilities::getPlayer();
  VBSFusion::Vehicle tank = VBSFusion::UnitUtilities::getMountedVehicle(playerUnit);
  VBSFusion::VehicleUtilities::updateVehicle(tank);
  VBSFusion::VehicleUtilities::getCrewPosition(tank); //returns all available positions in the vehicle.
  std::vector<VBSFusion::CrewPos> turrets = VBSFusion::VehicleUtilities::getCrewPosition(tank);

  VBSFusion::Turret turret;
  std::vector<VBSFusion::CrewPos>::const_iterator iter;
  for (iter = turrets.begin(); iter<turrets.end(); iter++)
  {
    // If there is something in CrewPos::turret then the position is turret.
    // We are interested only in those positions which are turrets.
    if ((*iter).turret.size() == 0) continue;

    // Get turret path string 
    turret.setTurretPath((*iter).turret);
    std::string turretPathString = turret.getTurretpathString();
    
    result.push_back(turretPathString);
    // Fill the info about the turret into the global map.
    gTurrets[turretPathString] = (*iter).turret;
  }
  LeaveCriticalSection(&gCriticalSection);
  return result;
};

// New TurretControlApp to manage turrets on the tank
std::vector<std::string> ListTurrets()
{
  return vecTorr;
}


VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static const char *result = "";
  //newTurret();

  //Call in main thread to get all turrets
  //VBSFusion::Unit playerUnit = VBSFusion::MissionUtilities::getPlayer();
  vecTorr = GetTurrets();

  // init
  if (gThreadHandle == NULL && stricmp(input, "i") == 0)
  {
    gThreadHandle = CreateThread(NULL,0,ThreadProc,NULL,0,NULL);
  }

  return result;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (gThreadHandle == NULL)
    return;

  if (gActiveTurret.length() == 0) 
    return;

  EnterCriticalSection(&gCriticalSection);
  // For demonstration purposes we use turret of vehicle in which player is located
  VBSFusion::Unit playerUnit = VBSFusion::MissionUtilities::getPlayer();
  VBSFusion::Vehicle tank = VBSFusion::UnitUtilities::getMountedVehicle(playerUnit);
  VBSFusion::VehicleUtilities::updateVehicle(tank);

  VBSFusion::Turret turret;
  // Set the path to currently selected turret.
  turret.setTurretPath(gTurrets[gActiveTurret]);

  // changed of speed of turret
  bool speedChanged = gSpeedChanged;
  gSpeedChanged = false;

  double newAzimuthSpeed = gNewAzimuthSpeed;
  double newElevationSpeed = gNewElevationSpeed;

  // changed stabilization
  bool stabilChanged = gStabilChanged;
  gStabilChanged = false;

  bool newAzimuthStabil = gNewAzimuthStabil;
  bool newElevationStabil = gNewElevationStabil;

  LeaveCriticalSection(&gCriticalSection);
  // Set wanted speed
  if (speedChanged)
    VBSFusion::VehicleUtilities::applyTurretDirectionChange(tank, turret, newAzimuthSpeed, newElevationSpeed, false, false);
  // Set wanted stabilization flags
  if (stabilChanged)
    VBSFusion::VehicleUtilities::applyTurretStabilizaton(tank, turret, newAzimuthStabil, newElevationStabil);

}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    InitializeCriticalSection(&gCriticalSection);
    break;
  case DLL_PROCESS_DETACH:
    // If the UI is running then close it.
    if (gThreadHandle != NULL)
      TerminateThread(gThreadHandle, 0);
      gThreadHandle = NULL;
    DeleteCriticalSection(&gCriticalSection);
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}

