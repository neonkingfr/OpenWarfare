#include <windows.h>
#include "VBSFusion.h"
#include "VBSFusionCallBackDefinitions.h"

#define VBSPLUGIN_EXPORT __declspec(dllexport)

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI RegisterGetMapGeomFnc(void *getMapGeomFnc);
VBSPLUGIN_EXPORT void WINAPI RegisterRelMapGeomFnc(void *params);


// ExecuteCommandType function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);
// GetMapGeometry function declaration
typedef VBSFusion::GeometryObject *(WINAPI * GetMapGeometryType)(float begX, float begZ, float endX, float endZ, int& geometryObjectsSize, int type);
// ReleaseMapGeometry function declaration
typedef void (WINAPI * ReleaseMapGeometryType)(VBSFusion::GeometryObject *objects);
