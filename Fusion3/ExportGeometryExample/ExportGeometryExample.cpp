#include <string>
#include <iostream>
#include <sstream>

#include "VBSFusion.h"
#include "exports.h"
#include "pluginHeader.h"


using namespace VBSFusion;

// helper structure for formatting strings
struct stringbuilder
{
	std::stringstream ss;
	template<typename T>
	stringbuilder & operator << (const T &data)
	{
		ss << data;
		return *this;
	}
	operator std::string() { return ss.str(); }
};

//Command function declaration
ExecuteCommandType ExecuteCommand = NULL;
// GetMapGeometry function definition
GetMapGeometryType GetMapGeometry = NULL;
// ReleaseMapGeometry function definition
ReleaseMapGeometryType ReleaseMapGeometry = NULL;

// Map Object to store our retrieved object
MapObject mapBuilding;
// Player Unit
Unit player;
// Player position
position3D playerPos;

// trace output string
string displayString;

bool started=false;

// Function that will register the GetMapGeomFnc function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterGetMapGeomFnc(void *getMapGeometryFnc)
{
	GetMapGeometry = (GetMapGeometryType)getMapGeometryFnc;
}

// Function that will register the RelMapGeomFnc function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterRelMapGeomFnc(void *releaseGeometryFnc)
{
	ReleaseMapGeometry = (ReleaseMapGeometryType)releaseGeometryFnc;
} 

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
	ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
	if(started)
	{
		DisplayFunctions::DisplayString(displayString);
	}
};

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
	static const char result[]="[True]";

	if (input[0] == 's')
	{	 
		// link player object and get position
		player = MissionUtilities::getPlayer();
		UnitUtilities::updateStaticProperties(player);
		UnitUtilities::updateDynamicProperties(player);
		playerPos = player.getPosition();

		displayString="This is a fusion export geometry sample project";
		started=true;
	}  

	else if (input[0] == 't')
	{
		int objectCount = 0;
		int type = 1; // set the type to house

		// get a range of position near the player
		double bX, bZ, eX, eZ;
		bX = playerPos.getX() - 5;
		bZ = playerPos.getZ() - 5;
		eX = playerPos.getX() + 5;
		eZ = playerPos.getZ() + 5;
		displayString = stringbuilder() << "begX: " << bX;
		displayString += stringbuilder() << "\\nbegZ: " << bZ;
		displayString += stringbuilder() << "\\nendX: " << eX;
		displayString += stringbuilder() << "\\nendX: " << eZ;

		// example call to GetMapGeometry, begx, begz, endx, endz refer to the getPos position of a
		// map object (on Rhamadi) that is baked into the map (its a building in the town).	
		GeometryObject *objects = GetMapGeometry(bX, bZ, eX, eZ, objectCount, type );

		if(objects)
		{
			// output some of the properties of our grabbed objects
			displayString += stringbuilder() << "\\n\\nGetMapGeometry: " << objectCount << " objects (type " << type << ")";
			displayString += stringbuilder() << "\\nFaces = " << objects->nFaces << "\\nVertices = " << objects->nVertices;
			displayString += stringbuilder() << "\\nRealObjectID = " << objects->realObjectId;

			// output a second command which returns the iD of the object using a script command to nearestObject to player
			string nearestObjectCmd = "objToMapId (nearestObject [(getPos player), \"building\"])";
			string nearestObject = ExecutionUtilities::ExecuteStringAndReturn(nearestObjectCmd);
			displayString += "\\n\\nScriptObjIDReturn: " + nearestObject;
			
			// using the id, we call the mapIdToObj. IMPORTANT We need to convert the ID from a 64bit int (long long) to a string
			string mapIDCommand = stringbuilder() << "mapIdToObj[\"" << objects->realObjectId << "\", ["<< playerPos.getX() <<"," << playerPos.getZ() << "]]";
			displayString += "\\n\\n" + mapIDCommand;
			// call and return mapObject
			string mapObject = ExecutionUtilities::ExecuteStringAndReturn(mapIDCommand);
			displayString += "\\n\\nFusionObjReturn: " + mapObject;

			// we must release the geometries now they are not needed anymore
			if (ReleaseMapGeometry) ReleaseMapGeometry(objects);
		}

		
	}

	else if(input[0] == 'k')
	{	
		displayString = "";
		started=false;
	}  
	return result;
};



/*!
Do not modify this code. Defines the DLL main code fragment
*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};