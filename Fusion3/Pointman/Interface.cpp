#include "Pointman.h"
#include "PointmanPose.h"

using namespace VBSFusion;

PoseControlUnit Player;
static std::string Profile = "";

ExecuteCommandType ExecuteCommand = NULL;
int ExecuteCmd(const char* command, char *result, int resultLength)
{ 
  ZeroMemory(result,resultLength);
  if (!ExecuteCommand) return 0;
  return ExecuteCommand(command, result, resultLength);
}

bool LoadPathFromConfig(std::string &path)
{
  char res[512];
  ExecuteCmd("loadconfig \"Pointman.cfg\" getValue \"PointmanPath\"", res, sizeof(res));
  if (strlen(res) > 2) 
  {
    path = res;
    path = path.substr(1, path.size() - 2);
	return true;
  }
  return false;
}

bool LoadIPFromConfig(std::string &ip)
{
  char res[512];
  ExecuteCmd("loadconfig \"Pointman.cfg\" getValue \"PointmanIP\"", res, sizeof(res));
  if (strlen(res) >= 2)
  {
    ip = res;
    ip = ip.substr(1, ip.size() - 2);
	return true;
  }
  return false;
}

void EnableEPC()
{
  SetExternalPose(true);
  SetExternalMovementControlled(true);

  SetExternalCameraControlled(true);
  SetExternalPoseLowerBody(true);

  //SetExternalPoseUpBody(true);
  SetExternalPoseSkeleton(true);
}

void DisableEPC()
{
  SetExternalPose(false);
  SetExternalMovementControlled(false);

  SetExternalCameraControlled(false);
  SetExternalPoseLowerBody(false);

  //SetExternalPoseUpBody(false);
  SetExternalPoseSkeleton(false);
}

void InitPlayer()
{
  Unit PlayerUnit = MissionUtilities::getPlayer();
  PoseControlUnit *pPlayer;
  pPlayer = static_cast<PoseControlUnit*>(&PlayerUnit);
  Player = *pPlayer;

  UnitUtilities::updateStaticProperties(Player);
  UnitUtilities::updateDynamicProperties(Player);	

  EnableEPC();
}

void ValidatePlayer()
{
  if (!PlayerUtilities::isPlayer(Player))
  {
    DisableEPC();
    InitPlayer();
  }
}

std::string GetUserProfileDir()
{
  return GeneralUtilities::getVBS2Directory(0);
}

bool ValidateProfile()
{
  bool valid = true;
  std::string actualProfile = GetUserProfileDir();
  if (Profile.size() > 0)
  {
    if (actualProfile != Profile) valid = false;
  }
  Profile = actualProfile;
  LogF("{info} Pointman: profile = %s", actualProfile.c_str());

  return valid;
}

void SetExternalPose(bool value)
{
  PoseControlUtilities::applyExternalPose(Player, value);
}

void SetExternalMovementControlled(bool value)
{
  PoseControlUtilities::applyExternalMovementControlled(Player, value);
}

void SetExternalCameraControlled(bool value)
{
  PoseControlUtilities::applyExternalCameraControlled(Player, value);
}

void SetExternalPoseLowerBody(bool value)
{
  PoseControlUtilities::applyExternalPoseLowerBody(Player, value);
}

void SetExternalPoseUpBody(bool value)
{
  PoseControlUtilities::applyExternalPoseUpBody(Player, value);
}

void SetExternalPoseSkeleton(bool value)
{
  PoseControlUtilities::applyExternalPoseSkeleton(Player, value);
}

bool GetExternalPoseSkeleton()
{
  return PoseControlUtilities::getExternalPoseSkeleton(Player);
}

void EnableExternalJoystick(bool value)
{
  InputUtilities::applyExternalJoystickEnable(value);
}

void EnableTrackIR(bool value)
{
  InputUtilities::applyTrackerIREnable(value);
}

int GetActUpDegree()
{
  return UnitUtilities::getUpDegree(Player);
}

bool IsAlive()
{
  return UnitUtilities::isAlive(Player);
}

bool IsInVehicle()
{
  return (MissionUtilities::getMountedObject(Player) != Player.getNetworkID());
}

bool IsDriver()
{
  Vehicle veh = MissionUtilities::getMountedObject(Player);
  return (VehicleUtilities::getDriver(veh).getNetworkID() == Player.getNetworkID());
}

bool IsGunner()
{
  Vehicle veh = MissionUtilities::getMountedObject(Player);
  return (VehicleUtilities::getGunner(veh).getNetworkID() == Player.getNetworkID());
}

bool IsOnSomeLadder()
{
  return UnitUtilities::isOnSomeLadder(Player);
}

bool IsSeated()
{
  return UnitUtilities::isSeated(Player);
}

bool IsWeaponOnBack()
{
  return UnitUtilities::isWeaponOnBack(Player);
}

void GetRecoilImpulse(float &impulseAngX, float &impulseAngY, float &impulseZ)
{
  // We want to update recoil only when it is reseted
  if (impulseAngX != 0 || impulseAngY !=0 || impulseZ !=0) return;

  vector3D vec = UnitUtilities::getRecoilImpulse(Player);
  impulseAngX = vec.x();
  impulseAngY = vec.z();
  impulseZ = vec.z();
}

void RaiseWeapon()
{
  ControllableObjectUtilities::applyAction(Player, "WeaponInHand", Player);
}

bool WeaponSafety()
{
  return UnitUtilities::getWeaponSafety(Player);
}

void GetCurrentWeaponInfo(char* weaponName,char* magazineName, int& ammoCount, int& weaponType, float& weaponWeight, int& magazineType, float& magazineWeight, int& magazineAmmoSimulation)
{
  Weapon weapon = UnitUtilities::getCurrentWeapon(Player);
  strcpy(weaponName, weapon.getWeaponName().c_str());
  strcpy(magazineName,  weapon.getMagName().c_str());
  ammoCount = weapon.getAmmoCount();
  weaponType = weapon.getWeaponType();
  weaponWeight = weapon.getWeaponWeight();
  magazineType = weapon.getMagazineType();
  magazineWeight = weapon.getMagazineWeight();
  magazineAmmoSimulation = weapon.getMagazineAmmoSimulation();
}

float GetCameraFOV()
{
  return UnitUtilities::getCameraFOV(Player);
}

void SetCameraFOV(float fov)
{
  CameraUtilities::applyFrustum(true, fov);
}

float GetDamage()
{
  return UnitUtilities::getDamage(Player);
}

float GetFatigue()
{
  return UnitUtilities::getFatigue(Player);
}

char* GetWeaponType (int weaponMask)
{
  switch (weaponMask)
  {
  case MaskSlotPrimary:
    return "PRIMARY";
  case MaskSlotSecondary:
    return "SECONDARY";
  case MaskSlotItem:
    return "ITEM";
  case MaskSlotBinocular:
    return "BINOCULAR";
  case MaskHardMounted:
    return "MOUNTED";
  case MaskSlotHandGun:
    return "HANDGUN";
  case MaskSlotHandGunItem:
    return "HANDGUNITEM";
  case MaskSlotInventory:
    return "INVENTORY";
  }
  return "";
}

void GetWeaponName(char *name, char* type) 
{
  std::string strName = WeaponUtilities::getWeaponName(Player, type);
  strcpy(name, strName.c_str());
}

void GetWeaponBBox(Vector3 &bbMin, Vector3 &bbMax,char* type)
{
  Vector3f bbMin3f;
  Vector3f bbMax3f;
  WeaponUtilities::getWeaponBoundingBox(Player, type, bbMin3f, bbMax3f);
  Vector3fToVector3(bbMin3f, bbMin);
  Vector3fToVector3(bbMax3f, bbMax);
}

Vector3 GetMuzzleCameraPos(char* type)
{
  Vector3 cameraPos;

  Vector3f vec = WeaponUtilities::getMuzzleCameraPos(Player, type);
  Vector3fToVector3(vec, cameraPos);

  return cameraPos;
}

Vector3 GetMuzzlePos(char* type)
{
  Vector3 muzzlePos;

  Vector3f vec = WeaponUtilities::getMuzzlePos(Player, type);
  Vector3fToVector3(vec, muzzlePos);

  return muzzlePos;
}

float GetWeaponWeight(char* type)
{
  return WeaponUtilities::getWeaponWeight(Player, type);
}

float GetWeaponReloadTime(char* type)
{
  return WeaponUtilities::getWeaponReloadTime(Player, type);
}

void GetMuzzleOpticsZoom(float &opticsZoomMin, float &opticsZoomMax, char* type)
{
  WeaponUtilities::getMuzzleOpticsZoom(Player, type, opticsZoomMin, opticsZoomMax);
}

Vector3 GetHeadCenterMoves()
{
  Vector3 headAxis;
 
  Vector3f vec = UnitUtilities::getHeadCenterMoves(Player);
  Vector3fToVector3(vec, headAxis);

  return headAxis;
}

Vector3 GetWeaponCenterMoves()
{
  Vector3 weaponAxis;

  Vector3f vec = UnitUtilities::getWeaponCenterMoves(Player);
  Vector3fToVector3(vec, weaponAxis);

  return weaponAxis;
}

Vector3 GetLeaningCenterMoves()
{
  Vector3 leaningAxis;

  Vector3f vec = UnitUtilities::getLeaningCenterMoves(Player);
  Vector3fToVector3(vec, leaningAxis);

  return leaningAxis;
}

void FireWeapon(int firedCount)
{
  for (int i = 0;i < firedCount; i++)
  {
      UnitUtilities::applyFiringForce(Player, true);
  }
}

int GetCameraType()
{
  int camType;

  std::string camName = CameraUtilities::getCameraView();

  if (_strcmpi(camName.c_str(), "INTERNAL") == 0) camType = CamInternal;
  if (_strcmpi(camName.c_str(), "GUNNER") == 0)   camType = CamGunner;
  if (_strcmpi(camName.c_str(), "EXTERNAL") == 0) camType = CamExternal;
  if (_strcmpi(camName.c_str(), "GROUP") == 0)    camType = CamGroup;
  if (_strcmpi(camName.c_str(), "VIEW") == 0)     camType = CamView;

  return camType;
}

int GetCameraTypeWanted()
{
  int camTypeWanted;

  std::string camName = CameraUtilities::getCameraViewWanted();

  if (_strcmpi(camName.c_str(), "INTERNAL") == 0) camTypeWanted = CamInternal;
  if (_strcmpi(camName.c_str(), "GUNNER") == 0)   camTypeWanted = CamGunner;
  if (_strcmpi(camName.c_str(), "EXTERNAL") == 0) camTypeWanted = CamExternal;
  if (_strcmpi(camName.c_str(), "GROUP") == 0)    camTypeWanted = CamGroup;
  if (_strcmpi(camName.c_str(), "VIEW") == 0)     camTypeWanted = CamView;

  return camTypeWanted;
}

void SetCameraType(int camTypeToSet)
{
  std::string camType;
  switch(camTypeToSet)
  {
  case CamInternal:
    camType = "INTERNAL";
    break;
  case CamGunner:
    camType = "GUNNER";
    break;
  case CamExternal:
    camType = "EXTERNAL";
    break;
  case CamGroup:
    camType = "GROUP";
    break;
  case CamView:
    camType = "VIEW";
    break;
  }

  CameraUtilities::applyCameraSwitch(Player, camType);
}

Vector3 GetPelvisPosition()
{
  Vector3 pelvisOffset;

  Vector3f vec3f;
  vec3f = UnitUtilities::getPelvisPosition(Player);
  Vector3fToVector3(vec3f, pelvisOffset);

  return pelvisOffset;
}

bool IsViewAlignedWithHead()
{
  return PoseControlUtilities::isViewAlignedWithHead();
}

int FindBone(const char* boneName)
{
  std::string bone = boneName;
  return PoseControlUtilities::getBoneID(Player, bone);
}

bool IsCurrentWeaponHandGun()
{
  return ControllableObjectUtilities::getCurrentWeapon(Player) == UnitUtilities::getPistolWeapon(Player);
}

void GetCurrentWeaponMode(char* mode)
{
  std::string strMode = UnitUtilities::getCurrentWeaponMode(Player);
  strcpy(mode, strMode.c_str());
}

int GetMagazinesCountOfType(char * type)
{
  int res = 0;

  UnitUtilities::updateMagazineTypes(Player);
  UnitUtilities::updateMagazineAmmo(Player);
  for(Unit::magazines_const_iterator it = Player.magazines_begin(); it != Player.magazines_end(); it++)
  {
    Magazine mag;
    mag = *it;
    if ((strcmp(mag.getMagName().c_str(), type) == 0) && mag.getAmmoCount() > 0)
    {
      res++;
    }
  }

  return res;
}

int GetMuzzleCount()
{
  std::vector<std::string> muzzles = UnitUtilities::getMuzzles(Player);
  return muzzles.size();
}

bool GetMuzzleInfo(char* type, int muzzle, int mode, int& burst, float& reloadTime)
{
  return WeaponUtilities::getMuzzleInfo(Player, type, muzzle, mode, burst, reloadTime);
}

void GetWeaponMaxBurst(char* type,int& maxBurst,bool& fullAuto)
{
  WeaponUtilities::getWeaponMaxBurst(Player, type, maxBurst, fullAuto);
}

void SetAction(char * action)
{
  std::string act = action;

#if _USE_SWITCH_ACTION
  if (ControllableObjectUtilities::isTouchingWater(Player))
  {
    UnitUtilities::switchAction(Player, "");
    ControllableObjectUtilities::applyKeyAction(act.substr(6), 1);
  }
  else
  {
    UnitUtilities::switchAction(Player, act);
  }
  //LogF("{info} Pointman: switchAction(%s)", act.c_str());
#else
  ControllableObjectUtilities::applyKeyAction(act, 1);
  //LogF("{info} Pointman: applyKeyAction(%s)", act.c_str());
#endif
}

void MoveToSurface()
{
  position3D pos = ControllableObjectUtilities::getPosition(Player);
  pos.setY(0);
  ControllableObjectUtilities::applyPosition(Player, pos);
}