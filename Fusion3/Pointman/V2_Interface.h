///////////////////////////////////////////////////////////////////////////////
// V2_Interface.h
//	VbsPointman2 API constants, type definition and utility functions.
//	For use by VBS client applications which interface with Pointman via the
//		dynamic link library "VbsPointman2_Interface.dll".
//	Exported function declarations are contained in the header file 
//		"VbsPointman2.h".
//	For background and additional information, reference the document 
//		"VBS-Pointman API Version 2.0".
//	Author: Patricia Denbrook
//	Creation Date: 20090929
//	Last modified: 20100222
//	Copyright 2009-2010 Denbrook Computing Services, Inc.
///////////////////////////////////////////////////////////////////////////////
#pragma once

#ifndef _WINDOWS_
#include "windows.h"
#endif

namespace V2_Interface {

///////////////////////////////////////////////////////////////////////////////
//
// Constant Definitions
//
///////////////////////////////////////////////////////////////////////////////

// VBS-Pointman interface type
const WORD V2_INTERFACE_UDP				= 1;
const WORD V2_INTERFACE_DIRECT			= 2;
	
// VBS-Pointman version 2.0 interface packet id
const WORD V2_PACKET_ID					= 0x5632;

// Client command codes
const BYTE Cmd_GetServerStatus			= 0x01;
const BYTE Cmd_GetServerJoycaps			= 0x02;
const BYTE Cmd_SetSkeletalDef			= 0x03;
const BYTE Cmd_SetXmitFreqRanges		= 0x04;
const BYTE Cmd_StartServerInputGen		= 0x05;
const BYTE Cmd_StopServerInputGen		= 0x06;
const BYTE Cmd_StartXmit				= 0x07;
const BYTE Cmd_StopXmit					= 0x08;
const BYTE Cmd_SetMotionState			= 0x10;
const BYTE Cmd_SetUserParams			= 0x11;
const BYTE Cmd_SetDisplayConfig			= 0x12;
const BYTE Cmd_SetUserView				= 0x13;
const BYTE Cmd_SetPrimaryWeapon			= 0x14;
const BYTE Cmd_SetSecondaryWeapon		= 0x15;
const BYTE Cmd_SetPistolWeapon			= 0x16;
const BYTE Cmd_SetMountedWeapon			= 0x17;
const BYTE Cmd_SetBinoculars			= 0x18;
const BYTE Cmd_SetCarriedObject			= 0x19;
const BYTE Cmd_SetDraggedObject			= 0x1A;
const BYTE Cmd_SetFireState				= 0x1B;
const BYTE Cmd_Reload					= 0x1C;
const BYTE Cmd_GetPreSitPelvisRotations	= 0x1D;
const BYTE Cmd_GetSimControlParams		= 0x1E;
const BYTE Cmd_GetPointmanPrimaryWeapon = 0x20;
const BYTE Cmd_GetPointmanSecondaryWeapon = 0x21;
const BYTE Cmd_GetPointmanPistolWeapon	= 0x22;
const BYTE Cmd_GetPointmanBinoculars	= 0x23;
const BYTE Cmd_GetPointmanMountedWeapon = 0x24;
const BYTE Cmd_GetPointmanCarriedObject = 0x25;
const BYTE Cmd_GetPointmanDraggedObject = 0x26;

// Server response codes
const BYTE Rsp_GetServerStatus			= 0x81;
const BYTE Rsp_GetServerJoycaps			= 0x82;
const BYTE Rsp_SetSkeletalDef			= 0x83;
const BYTE Rsp_SetXmitFreqRanges		= 0x84;
const BYTE Rsp_StartServerInputGen		= 0x85;
const BYTE Rsp_StopServerInputGen		= 0x86;
const BYTE Rsp_StartXmit				= 0x87;
const BYTE Rsp_StopXmit					= 0x88;
const BYTE Rsp_SetMotionState			= 0x90;
const BYTE Rsp_SetUserParams			= 0x91;
const BYTE Rsp_SetDisplayConfig			= 0x92;
const BYTE Rsp_SetUserView				= 0x93;
const BYTE Rsp_SetPrimaryWeapon			= 0x94;
const BYTE Rsp_SetSecondaryWeapon		= 0x95;
const BYTE Rsp_SetPistolWeapon			= 0x96;
const BYTE Rsp_SetMountedWeapon			= 0x97;
const BYTE Rsp_SetBinoculars			= 0x98;
const BYTE Rsp_SetCarriedObject			= 0x99;
const BYTE Rsp_SetDraggedObject			= 0x9A;
const BYTE Rsp_SetFireState				= 0x9B;
const BYTE Rsp_Reload					= 0x9C;
const BYTE Rsp_GetPreSitPelvisRotations	= 0x9D;
const BYTE Rsp_GetSimControlParams		= 0x9E;
const BYTE Rsp_GetPointmanPrimaryWeapon = 0xA0;
const BYTE Rsp_GetPointmanSecondaryWeapon = 0xA1;
const BYTE Rsp_GetPointmanPistolWeapon	= 0xA2;
const BYTE Rsp_GetPointmanBinoculars	= 0xA3;
const BYTE Rsp_GetPointmanMountedWeapon = 0xA4;
const BYTE Rsp_GetPointmanCarriedObject = 0xA5;
const BYTE Rsp_GetPointmanDraggedObject = 0xA6;
const BYTE Rsp_UnknownCmd				= 0xBF;

// Xmit packet codes
const BYTE Xmit_ClientStateDat				= 0x70;
const BYTE Xmit_ServerMotionDat			= 0xF0;
const BYTE Xmit_ServerJoyDat			= 0xF1;

// Error codes
const BYTE ERR_NONE						= 0x00;
const BYTE ERR_DATA_XMIT				= 0x01;
const BYTE ERR_INPUT_GEN				= 0x02;
const BYTE ERR_COMMAND_DATA				= 0x04;
const BYTE ERR_DATA_SIZE				= 0x08;
const BYTE ERR_COMM						= 0x10;
const BYTE ERR_SIM_STATE				= 0x20;
const BYTE ERR_UNKNOWN					= 0x80;

// V2_GetServerInputGenStartError error codes (direct interface only)
const BYTE ERR_INPUT_GEN_NONE				= 0x00;
const BYTE ERR_INPUT_GEN_TRACKER_REQUIRED	= 0x01;
const BYTE ERR_INPUT_GEN_JOYSTICK_REQUIRED	= 0x02;
const BYTE ERR_INPUT_GEN_TRACKER_DEV_SETTINGS = 0x03;
const BYTE ERR_INPUT_GEN_SEG_NOT_TRACKED	= 0x04;
const BYTE ERR_INPUT_GEN_SEG_TRACKED_DOF	= 0x05;
const BYTE ERR_INPUT_GEN_TRACKER_BORESIGHT	= 0x06;
const BYTE ERR_INPUT_GEN_TRACKER_FAILED		= 0x07;
const BYTE ERR_INPUT_GEN_JOYSTICK_FAILED	= 0x08;
const BYTE ERR_INPUT_GEN_DEV_FAILED			= 0x09;
const BYTE ERR_INPUT_GEN_DEV_INPUT_THREAD	= 0x0F;
const BYTE ERR_INPUT_GEN_OTHER				= 0x10;

// Status flags
const BYTE STATUS_DATA_XMIT				= 0x01;
const BYTE STATUS_INPUT_GEN				= 0x02;

// Mobility states
const WORD NUM_MOBILITY_STATES			= 7;
const BYTE MOBILITY_GROUNDED			= 0x00;
const BYTE MOBILITY_CLIMBING			= 0x01;
const BYTE MOBILITY_SWIMMING			= 0x02;
const BYTE MOBILITY_DRIVER				= 0x03;
const BYTE MOBILITY_PASSENGER			= 0x04;
const BYTE MOBILITY_GUNNER				= 0x05;
const BYTE MOBILITY_SUSPENDED			= 0x06;

// Motion control levels
const BYTE MOTION_CTRL_NONE				= 0x01;
const BYTE MOTION_CTRL_PARTIAL			= 0x02;
const BYTE MOTION_CTRL_FULL				= 0x04;

// Body postures
const WORD NUM_BODY_POSTURES			= 4;
const BYTE BODY_POSTURE_STAND			= 0x00;
const BYTE BODY_POSTURE_CROUCH			= 0x01;
const BYTE BODY_POSTURE_PRONE			= 0x02;
const BYTE BODY_POSTURE_SEATED			= 0x03;

// Held object types
const WORD NUM_HELD_OBJECT_TYPES		= 9;
const BYTE HELD_OBJECT_HANDS_FREE		= 0x00;
const BYTE HELD_OBJECT_PRIMARY_WEAPON	= 0x01;
const BYTE HELD_OBJECT_SECONDARY_WEAPON	= 0x02;
const BYTE HELD_OBJECT_PISTOL_WEAPON	= 0x03;
const BYTE HELD_OBJECT_MOUNTED_WEAPON	= 0x04;
const BYTE HELD_OBJECT_BINOCULARS		= 0x05;
const BYTE HELD_OBJECT_CARRIED			= 0x06;
const BYTE HELD_OBJECT_DRAGGED			= 0x07;
const BYTE HELD_OBJECT_AMMO				= 0x08;

// Prior held object placement
const WORD NUM_OBJECT_STORAGE_TYPES		= 3;
const BYTE OBJECT_STORAGE_OTHER			= 0x00;
const BYTE OBJECT_STORAGE_STOW			= 0x01;
const BYTE OBJECT_STORAGE_QUICK_STOW	= 0x02;

// LEFT/RIGHT flags
const BYTE LEFT		= 0x01;
const BYTE RIGHT	= 0x02;

// UPPER/LOWER flags
const BYTE LOWER	= 0x10;
const BYTE UPPER	= 0x20;

// Optics types
const WORD NUM_OPTICS_TYPES		= 3;
const BYTE OPTICS_NONE			= 0x00;
const BYTE OPTICS_IRON_SIGHT	= 0x01;
const BYTE OPTICS_SCOPE			= 0x02;

// Dismounted speed states
const WORD NUM_SPEED_STATES	= 4;
const BYTE SPEED_STATIONARY				= 0x00;
const BYTE SPEED_SLOW					= 0x01;
const BYTE SPEED_FAST					= 0x02;
const BYTE SPEED_SPRINT					= 0x03;

// Joystick objects
const WORD NUM_JOY_BUTTONS	= 64;
const WORD NUM_JOY_POVS		= 4;
const WORD NUM_JOY_AXES		= 32;

// Situated elevation offsets
const WORD NUM_SIT_ELEV_OFFSETS = 10;

// User view types
const WORD NUM_USER_VIEW_TYPES = 3;
const BYTE USER_VIEW_FIRST_PERSON	= 0x00;
const BYTE USER_VIEW_THIRD_PERSON	= 0x01;
const BYTE USER_VIEW_OTHER			= 0x02;

// Weapon subsystems
const WORD NUM_FIRE_SUBSYSTEMS = 3;
const BYTE SUBSYS_MAIN		= 0x00;
const BYTE SUBSYS_ATTACHED	= 0x01;
const BYTE SUBSYS_THROW		= 0x02;

// Fire modes
const WORD NUM_FIRE_MODES = 4;
const BYTE FIRE_NONE		= 0x00;
const BYTE FIRE_SINGLE		= 0x01;
const BYTE FIRE_BURST		= 0x02;
const BYTE FIRE_FULL_AUTO	= 0x03;

// Full auto fire flags
const WORD NUM_FULL_AUTO_FLAGS = 3;
const BYTE FULL_AUTO_NONE	= 0x00;
const BYTE FULL_AUTO_HOLD	= 0x01;
const BYTE FULL_AUTO_TOGGLE	= 0x02;

// Server data type flags
const WORD SERVER_MOTION_DATA	= 1;
const WORD SERVER_JOY_DATA		= 2;

// Parameter defaults
const WORD  DEF_MIN_XMIT_FREQ	= 1;		// packets/sec
const WORD	DEF_MAX_XMIT_FREQ	= 1000;		// packets/sec

const float	DEF_NEG_ROT_LIMIT	= -360.f;	// degrees
const float DEF_POS_ROT_LIMIT	= 360.f;	// degrees

const float DEF_SIT_HEADING_MAXVEL	= 360.f;	// degrees/sec
const float DEF_AIM_ROT_MAXVEL		= 720.f;	// degrees/sec

const BYTE	DEF_MOBILITY_STATE	= MOBILITY_GROUNDED;
const BYTE	DEF_MOTION_CTRL_LEVEL = MOTION_CTRL_NONE;
const BYTE	DEF_BODY_POSTURE	= BODY_POSTURE_STAND;
const BYTE	DEF_HELD_OBJECT		= HELD_OBJECT_HANDS_FREE;
const BYTE	DEF_SPEED_STATE		= SPEED_STATIONARY;

const float DEF_ZOOM			= 1.f;
const float DEF_HEALTH			= 1.f;
const float DEF_FATIGUE			= 0.f;

const BYTE	DEF_DOMINANT_SIDE	= RIGHT;
const float DEF_INTEROCULAR_DIST = 0.14f;

const DWORD DEF_TRANSITION_TIME = 0xFFFFFFFF;

// Avatar bone id's
//
const BYTE NUM_AVATAR_BONES				= 56;
// Root
const BYTE AVATAR_PELVIS				= 0;
// Torso
const BYTE AVATAR_SPINE_1				= 1;
const BYTE AVATAR_SPINE_2				= 2;
const BYTE AVATAR_SPINE_3				= 3;
const BYTE AVATAR_SPINE_4				= 4;
// HEAD
const BYTE AVATAR_NECK_1				= 5;
const BYTE AVATAR_NECK_2				= 6;
const BYTE AVATAR_SKULL					= 7;
const BYTE AVATAR_LEFT_EYE				= 8;
const BYTE AVATAR_RIGHT_EYE				= 9;
// Left arm
const BYTE AVATAR_LEFT_CLAVICLE			= 10;
const BYTE AVATAR_LEFT_UPPER_ARM		= 11;
const BYTE AVATAR_LEFT_LOWER_ARM		= 12;
const BYTE AVATAR_LEFT_HAND				= 13;
const BYTE AVATAR_LEFT_MIDDLE_FINGER_1	= 14;
const BYTE AVATAR_LEFT_MIDDLE_FINGER_2	= 15;
const BYTE AVATAR_LEFT_MIDDLE_FINGER_3	= 16;
const BYTE AVATAR_LEFT_RING_FINGER_1	= 17;
const BYTE AVATAR_LEFT_RING_FINGER_2	= 18;
const BYTE AVATAR_LEFT_RING_FINGER_3	= 19;
const BYTE AVATAR_LEFT_PINKY_FINGER_1	= 20;
const BYTE AVATAR_LEFT_PINKY_FINGER_2	= 21;
const BYTE AVATAR_LEFT_PINKY_FINGER_3	= 22;
const BYTE AVATAR_LEFT_INDEX_FINGER_1	= 23;
const BYTE AVATAR_LEFT_INDEX_FINGER_2	= 24;
const BYTE AVATAR_LEFT_INDEX_FINGER_3	= 25;
const BYTE AVATAR_LEFT_THUMB_1			= 26;
const BYTE AVATAR_LEFT_THUMB_2			= 27;
const BYTE AVATAR_LEFT_THUMB_3			= 28;
// Right arm
const BYTE AVATAR_RIGHT_CLAVICLE		= 29;
const BYTE AVATAR_RIGHT_UPPER_ARM		= 30;
const BYTE AVATAR_RIGHT_LOWER_ARM		= 31;
const BYTE AVATAR_RIGHT_HAND			= 32;
const BYTE AVATAR_RIGHT_MIDDLE_FINGER_1	= 33;
const BYTE AVATAR_RIGHT_MIDDLE_FINGER_2	= 34;
const BYTE AVATAR_RIGHT_MIDDLE_FINGER_3	= 35;
const BYTE AVATAR_RIGHT_RING_FINGER_1	= 36;
const BYTE AVATAR_RIGHT_RING_FINGER_2	= 37;
const BYTE AVATAR_RIGHT_RING_FINGER_3	= 38;
const BYTE AVATAR_RIGHT_PINKY_FINGER_1	= 39;
const BYTE AVATAR_RIGHT_PINKY_FINGER_2	= 40;
const BYTE AVATAR_RIGHT_PINKY_FINGER_3	= 41;
const BYTE AVATAR_RIGHT_INDEX_FINGER_1	= 42;
const BYTE AVATAR_RIGHT_INDEX_FINGER_2	= 43;
const BYTE AVATAR_RIGHT_INDEX_FINGER_3	= 44;
const BYTE AVATAR_RIGHT_THUMB_1			= 45;
const BYTE AVATAR_RIGHT_THUMB_2			= 46;
const BYTE AVATAR_RIGHT_THUMB_3			= 47;
// Left leg
const BYTE AVATAR_LEFT_UPPER_LEG		= 48;
const BYTE AVATAR_LEFT_LOWER_LEG		= 49;
const BYTE AVATAR_LEFT_FOOT				= 50;
const BYTE AVATAR_LEFT_TOES				= 51;
// Right leg
const BYTE AVATAR_RIGHT_UPPER_LEG		= 52;
const BYTE AVATAR_RIGHT_LOWER_LEG		= 53;
const BYTE AVATAR_RIGHT_FOOT			= 54;
const BYTE AVATAR_RIGHT_TOES			= 55;

// Parent id's (avatar hierarchy definition)
// Torso
const BYTE PARENT_SPINE_1				= AVATAR_PELVIS;
const BYTE PARENT_SPINE_2				= AVATAR_SPINE_1;
const BYTE PARENT_SPINE_3				= AVATAR_SPINE_2;
const BYTE PARENT_SPINE_4				= AVATAR_SPINE_3;
// HEAD
const BYTE PARENT_NECK_1				= AVATAR_SPINE_4;
const BYTE PARENT_NECK_2				= AVATAR_NECK_1;
const BYTE PARENT_SKULL					= AVATAR_NECK_2;
const BYTE PARENT_LEFT_EYE				= AVATAR_SKULL;
const BYTE PARENT_RIGHT_EYE				= AVATAR_SKULL;
// Left arm
const BYTE PARENT_LEFT_CLAVICLE			= AVATAR_SPINE_4;
const BYTE PARENT_LEFT_UPPER_ARM		= AVATAR_LEFT_CLAVICLE;
const BYTE PARENT_LEFT_LOWER_ARM		= AVATAR_LEFT_UPPER_ARM;
const BYTE PARENT_LEFT_HAND				= AVATAR_LEFT_LOWER_ARM;
const BYTE PARENT_LEFT_MIDDLE_FINGER_1	= AVATAR_LEFT_HAND;
const BYTE PARENT_LEFT_MIDDLE_FINGER_2	= AVATAR_LEFT_MIDDLE_FINGER_1;
const BYTE PARENT_LEFT_MIDDLE_FINGER_3	= AVATAR_LEFT_MIDDLE_FINGER_2;
const BYTE PARENT_LEFT_RING_FINGER_1	= AVATAR_LEFT_HAND;
const BYTE PARENT_LEFT_RING_FINGER_2	= AVATAR_LEFT_RING_FINGER_1;
const BYTE PARENT_LEFT_RING_FINGER_3	= AVATAR_LEFT_RING_FINGER_2;
const BYTE PARENT_LEFT_PINKY_FINGER_1	= AVATAR_LEFT_HAND;
const BYTE PARENT_LEFT_PINKY_FINGER_2	= AVATAR_LEFT_PINKY_FINGER_1;
const BYTE PARENT_LEFT_PINKY_FINGER_3	= AVATAR_LEFT_PINKY_FINGER_2;
const BYTE PARENT_LEFT_INDEX_FINGER_1	= AVATAR_LEFT_HAND;
const BYTE PARENT_LEFT_INDEX_FINGER_2	= AVATAR_LEFT_INDEX_FINGER_1;
const BYTE PARENT_LEFT_INDEX_FINGER_3	= AVATAR_LEFT_INDEX_FINGER_2;
const BYTE PARENT_LEFT_THUMB_1			= AVATAR_LEFT_HAND;
const BYTE PARENT_LEFT_THUMB_2			= AVATAR_LEFT_THUMB_1;
const BYTE PARENT_LEFT_THUMB_3			= AVATAR_LEFT_THUMB_2;
// Right arm
const BYTE PARENT_RIGHT_CLAVICLE		= AVATAR_SPINE_4;
const BYTE PARENT_RIGHT_UPPER_ARM		= AVATAR_RIGHT_CLAVICLE;
const BYTE PARENT_RIGHT_LOWER_ARM		= AVATAR_RIGHT_UPPER_ARM;
const BYTE PARENT_RIGHT_HAND			= AVATAR_RIGHT_LOWER_ARM;
const BYTE PARENT_RIGHT_MIDDLE_FINGER_1	= AVATAR_RIGHT_HAND;
const BYTE PARENT_RIGHT_MIDDLE_FINGER_2	= AVATAR_RIGHT_MIDDLE_FINGER_1;
const BYTE PARENT_RIGHT_MIDDLE_FINGER_3	= AVATAR_RIGHT_MIDDLE_FINGER_2;
const BYTE PARENT_RIGHT_RING_FINGER_1	= AVATAR_RIGHT_HAND;
const BYTE PARENT_RIGHT_RING_FINGER_2	= AVATAR_RIGHT_RING_FINGER_1;
const BYTE PARENT_RIGHT_RING_FINGER_3	= AVATAR_RIGHT_RING_FINGER_2;
const BYTE PARENT_RIGHT_PINKY_FINGER_1	= AVATAR_RIGHT_HAND;
const BYTE PARENT_RIGHT_PINKY_FINGER_2	= AVATAR_RIGHT_PINKY_FINGER_1;
const BYTE PARENT_RIGHT_PINKY_FINGER_3	= AVATAR_RIGHT_PINKY_FINGER_2;
const BYTE PARENT_RIGHT_INDEX_FINGER_1	= AVATAR_RIGHT_HAND;
const BYTE PARENT_RIGHT_INDEX_FINGER_2	= AVATAR_RIGHT_INDEX_FINGER_1;
const BYTE PARENT_RIGHT_INDEX_FINGER_3	= AVATAR_RIGHT_INDEX_FINGER_2;
const BYTE PARENT_RIGHT_THUMB_1			= AVATAR_RIGHT_HAND;
const BYTE PARENT_RIGHT_THUMB_2			= AVATAR_RIGHT_THUMB_1;
const BYTE PARENT_RIGHT_THUMB_3			= AVATAR_RIGHT_THUMB_2;
// Left leg
const BYTE PARENT_LEFT_UPPER_LEG		= AVATAR_PELVIS;
const BYTE PARENT_LEFT_LOWER_LEG		= AVATAR_LEFT_UPPER_LEG;
const BYTE PARENT_LEFT_FOOT				= AVATAR_LEFT_LOWER_LEG;
const BYTE PARENT_LEFT_TOES				= AVATAR_LEFT_FOOT;
// Right leg
const BYTE PARENT_RIGHT_UPPER_LEG		= AVATAR_PELVIS;
const BYTE PARENT_RIGHT_LOWER_LEG		= AVATAR_RIGHT_UPPER_LEG;
const BYTE PARENT_RIGHT_FOOT			= AVATAR_RIGHT_LOWER_LEG;
const BYTE PARENT_RIGHT_TOES			= AVATAR_RIGHT_FOOT;

///////////////////////////////////////////////////////////////////////////////
//
// Function Definitions
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Functions for parsing & constructing a status code
//
inline bool CheckStatusDataXmit(BYTE status) 
{ return ((status & STATUS_DATA_XMIT) != 0); } 

inline bool CheckStatusInputGen(BYTE status) 
{ return ((status & STATUS_INPUT_GEN) != 0); } 

inline BYTE MakeStatusCode(BOOL bDataXmit, BOOL bInputGen) 
{ return (bDataXmit | (bInputGen << 1)); }

///////////////////////////////////////////////////////////////////////////////
// Functions for parsing error code
//
inline bool CheckErrorDataXmit(BYTE error) 
{ return ((error & ERR_DATA_XMIT) != 0); } 

inline bool CheckErrorInputGen(BYTE error) 
{ return ((error & ERR_INPUT_GEN) != 0); } 

inline bool CheckErrorCommandData(BYTE error) 
{ return ((error & ERR_COMMAND_DATA) != 0); } 

inline bool CheckErrorDataSize(BYTE error) 
{ return ((error & ERR_DATA_SIZE) != 0); } 

inline bool CheckErrorComm(BYTE error) 
{ return ((error & ERR_COMM) != 0); } 

inline bool CheckErrorSimState(BYTE error) 
{ return ((error & ERR_SIM_STATE) != 0); } 

inline bool CheckErrorUnknown(BYTE error) 
{ return ((error & ERR_UNKNOWN) != 0); } 

///////////////////////////////////////////////////////////////////////////////
// Functions for checking validity of interface parameter values
//
inline bool CheckBoolParam(BYTE bBool) 
{ return (bBool == 0 || bBool == 1); }

inline bool CheckRotationAngle(double dAngle) 
{ return (dAngle >= -360. && dAngle <= 360.); }

inline bool CheckMobilityState(BYTE bState) 
{ return (bState < NUM_MOBILITY_STATES); }

inline bool CheckMotionControlLevel(BYTE bLevel) 
{ return (bLevel == MOTION_CTRL_NONE || 
		  bLevel == MOTION_CTRL_PARTIAL || 
		  bLevel == MOTION_CTRL_FULL); }

inline bool CheckBodyPosture(BYTE bPosture) 
{ return (bPosture < NUM_BODY_POSTURES); }

inline bool CheckHeldObject(BYTE bHeldObject) 
{ return (bHeldObject < NUM_HELD_OBJECT_TYPES); }

inline bool CheckObjectStorage(BYTE bObjectStorage) 
{ return (bObjectStorage < NUM_OBJECT_STORAGE_TYPES); }

inline bool CheckOpticsType(BYTE bType) 
{ return (bType < NUM_OPTICS_TYPES); }

inline bool CheckFullAutoFlag(BYTE bType) 
{ return (bType < NUM_FULL_AUTO_FLAGS); }

inline bool CheckSpeedState(BYTE bState) 
{ return (bState < NUM_SPEED_STATES); }

inline bool CheckEitherLeftRight(BYTE bFlag) 
{ return (bFlag == LEFT || bFlag == RIGHT); }

inline bool CheckBothLeftRight(BYTE bFlag) 
{ return (bFlag == (LEFT | RIGHT)); }

inline bool CheckUserViewType(BYTE bType) 
{ return (bType < NUM_USER_VIEW_TYPES); }

inline bool CheckFireSubsystem(BYTE bSubsys) 
{ return (bSubsys < NUM_FIRE_SUBSYSTEMS); }

inline bool CheckFireMode(BYTE bMode) 
{ return (bMode < NUM_FIRE_MODES); }

inline bool CheckJoyButton(BYTE bButton) 
{ return (bButton < NUM_JOY_BUTTONS); }

inline bool CheckJoyPov(BYTE bPov) 
{ return (bPov < NUM_JOY_POVS); }

inline bool CheckJoyAxis(BYTE bAxis) 
{ return (bAxis < NUM_JOY_AXES); }

inline bool CheckZoom(double dZoom) 
{ return (dZoom > 0.1); }

inline bool CheckZoomRange(double dZoomMin, double dZoomMax) 
{ return (CheckZoom(dZoomMin) && CheckZoom(dZoomMax) && dZoomMin <= dZoomMax); }

inline bool CheckHealth(double dHealth) 
{ return (dHealth >= 0. && dHealth <= 1.); }

inline bool CheckFatigue(double dFatigue) 
{ return (dFatigue >= 0. && dFatigue <= 1.); }

///////////////////////////////////////////////////////////////////////////////
// Functions for classifying mobility state, held object and stance level
//
inline bool IsDismountedMobilityState(BYTE bMobilityState) 
{ return bMobilityState == MOBILITY_GROUNDED ||
			bMobilityState == MOBILITY_CLIMBING ||
			bMobilityState == MOBILITY_SWIMMING; }

inline bool IsMountedMobilityState(BYTE bMobilityState) 
{ return bMobilityState == MOBILITY_DRIVER ||
			bMobilityState == MOBILITY_PASSENGER ||
			bMobilityState == MOBILITY_GUNNER; }

inline bool IsAimObject(BYTE bHeldObject)
{ return bHeldObject == HELD_OBJECT_PRIMARY_WEAPON ||
		 bHeldObject == HELD_OBJECT_SECONDARY_WEAPON ||
		 bHeldObject == HELD_OBJECT_PISTOL_WEAPON ||
		 bHeldObject == HELD_OBJECT_MOUNTED_WEAPON ||
		 bHeldObject == HELD_OBJECT_BINOCULARS; }

inline bool IsMountedAimObject (BYTE bHeldObject)
{ return bHeldObject == HELD_OBJECT_MOUNTED_WEAPON; }

inline bool IsPersonalAimObject(BYTE bHeldObject)
{ return IsAimObject(bHeldObject) && !IsMountedAimObject(bHeldObject); }

///////////////////////////////////////////////////////////////////////////////
//
// Structure Definitions
//
///////////////////////////////////////////////////////////////////////////////
//#pragma pack(push,1)

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_3DT
//	3-DOF translational coordinate.  
//
struct _V2_3DT
{
	// Data elements
	float	tx;	// Translation along X axis (meters)
	float	ty;	// Ttranslation along Y axis (meters)
	float	tz; // Translation along Z axis (meters)

	// Constructor, equality check and assignment
	_V2_3DT() { Reset(); }
	bool operator==(const _V2_3DT& crd) const
	{ return (0 == memcmp((const void*)this, (const void *)&crd, 
			 sizeof(crd))); }
	const _V2_3DT& operator=(const _V2_3DT& crd)
	{ CopyMemory((void *)this, (const void *)&crd, sizeof(crd)); 
	  return *this; }

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_3DT)); }

	// Data checking
	bool CheckValues() const
	{ return true; }
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_3DR
//	3-DOF rotational coordinate.  
//
struct _V2_3DR
{
	// Data elements
	float	rx;	// Rotation about X axis (degrees)
	float	ry;	// Rotation about Y axis (degrees)
	float	rz; // Rotation about Z axis (degrees)

	// Constructor, equality check and assignment
	_V2_3DR() { Reset(); }
	bool operator==(const _V2_3DR& crd) const
	{ return (0 == memcmp((const void*)this, (const void *)&crd, 
			 sizeof(crd))); }
	const _V2_3DR& operator=(const _V2_3DR& crd)
	{ CopyMemory((void *)this, (const void *)&crd, sizeof(crd)); 
	  return *this; }

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_3DR)); }

	// Data checking
	bool CheckValues() const
	{ return CheckRotationAngle(rx) && 
			 CheckRotationAngle(ry) &&
			 CheckRotationAngle(rz); }
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_6D
//	6-DOF translation and rotation.  
//
struct _V2_6D
{
	// Data elements
	float	tx;	// Translation along X axis (meters)
	float	ty;	// Translation along Y axis (meters)
	float	tz; // Translation along Z axis (meters)
	float	rx;	// Rotation about X axis (degrees)
	float	ry;	// Rotation about Y axis (degrees)
	float	rz; // Rotation about Z axis (degrees)

	// Constructor, equality check and assignment
	_V2_6D() { Reset(); }
	_V2_6D(double _tx, double _ty, double _tz, double _rx, double _ry, double _rz)
	{ tx = (float)_tx; ty = (float)_ty; tz = (float)_tz; 
	  rx = (float)_rx; ry = (float)_ry; rz = (float)_rz; }
	bool operator==(const _V2_6D& crd) const
	{ return (0 == memcmp((const void*)this, (const void *)&crd, 
			 sizeof(crd))); 
	}
	const _V2_6D& operator=(const _V2_6D& crd)
	{ CopyMemory((void *)this, (const void *)&crd, sizeof(crd)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_6D)); }

	// Data checking
	bool CheckValues() const
	{ return CheckRotationAngle(rx) && 
			 CheckRotationAngle(ry) && 
			 CheckRotationAngle(rz); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_TorsoPose
//	Pose of the avatar's torso. 
//	NOTE: All transformations are absolute.
//
struct _V2_TorsoPose
{
	// Data elements
	_V2_6D poseSpine1;	// Pose of the 1st spine bone
	_V2_6D poseSpine2;	// Pose of the 2nd spine bone
	_V2_6D poseSpine3;	// Pose of the 3rd spine bone
	_V2_6D poseSpine4;	// Pose of the 4th spine bone

	// Constructor, equality check and assignment
	_V2_TorsoPose() { Reset(); }
	bool operator==(const _V2_TorsoPose& pose) const
	{ return (0 == memcmp((const void*)this, (const void *)&pose, sizeof(pose))); 
	}
	const _V2_TorsoPose& operator=(const _V2_TorsoPose& pose)
	{ CopyMemory((void *)this, (const void *)&pose, sizeof(pose)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_TorsoPose)); }

	// Data checking
	bool CheckValues() const
	{ return poseSpine1.CheckValues() && 
			 poseSpine1.CheckValues() &&
	         poseSpine1.CheckValues() && 
			 poseSpine1.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_HeadPose
//	Pose of the avatar's head.  
//	NOTE: All transformations are absolute.
//
struct _V2_HeadPose
{
	// Data elements
	_V2_6D poseNeck1;		// Pose of the 1st neck bone
	_V2_6D poseNeck2;		// Pose of the 2nd neck bone
	_V2_6D poseSkull;		// Pose of the skull
	_V2_6D poseLeftEye;		// Pose of the left eye
	_V2_6D poseRightEye;	// Pose of the right eye

	// Constructor, equality check and assignment
	_V2_HeadPose() { Reset(); }
	bool operator==(const _V2_HeadPose& pose) const
	{ return (0 == memcmp((const void*)this, (const void *)&pose, sizeof(pose))); 
	}
	const _V2_HeadPose& operator=(const _V2_HeadPose& pose)
	{ CopyMemory((void *)this, (const void *)&pose, sizeof(pose)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_HeadPose)); }

	// Data checking
	bool CheckValues() const
	{ return poseNeck1.CheckValues() && 
			 poseNeck2.CheckValues() &&
	         poseSkull.CheckValues() && 
			 poseLeftEye.CheckValues() &&
			 poseRightEye.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_FingerPose
//	Pose of an avatar finger.  
//	NOTE: All transformations are absolute.
//
struct _V2_FingerPose
{
	// Data elements
	_V2_6D	poseBone1;		// Pose of the 1st finger bone
	_V2_6D	poseBone2;		// Pose of the 2nd finger bone
	_V2_6D	poseBone3;		// Pose of the 3rd finger bone

	// Constructor, equality check and assignment
	_V2_FingerPose() { Reset(); }
	bool operator==(const _V2_FingerPose& pose) const
	{ return (0 == memcmp((const void*)this, (const void *)&pose, sizeof(pose))); 
	}
	const _V2_FingerPose& operator=(const _V2_FingerPose& pose)
	{ CopyMemory((void *)this, (const void *)&pose, sizeof(pose)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_FingerPose));	}

	// Data checking
	bool CheckValues() const
	{ return poseBone1.CheckValues() && 
			 poseBone2.CheckValues() &&
	         poseBone3.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_ArmPose
//	Pose of an avatar arm.  
//	NOTE: All transformations are absolute.
//
struct _V2_ArmPose
{
	// Data elements
	_V2_6D			poseClavicle;		// Pose of the clavicle
	_V2_6D			poseUpperArm;		// Pose of the upper arm
	_V2_6D			poseLowerArm;		// Pose of the lower arm
	_V2_6D			poseHand;			// Pose of the hand
	_V2_FingerPose	poseThumb;			// Pose of the thumb
	_V2_FingerPose	poseIndexFinger;	// Pose of the index finger
	_V2_FingerPose	poseMiddleFinger;	// Pose of the middle finger
	_V2_FingerPose	poseRingFinger;		// Pose of the ring finger
	_V2_FingerPose	posePinkyFinger;	// Pose of the pinky finger

	// Constructor, equality check and assignment
	_V2_ArmPose() { Reset(); }
	bool operator==(const _V2_ArmPose& pose) const
	{ return (0 == memcmp((const void*)this, (const void *)&pose, sizeof(pose))); 
	}
	const _V2_ArmPose& operator=(const _V2_ArmPose& pose)
	{ CopyMemory((void *)this, (const void *)&pose, sizeof(pose)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_ArmPose)); }

	// Data checking
	bool CheckValues() const
	{ return poseClavicle.CheckValues() && 
			 poseUpperArm.CheckValues() &&
	         poseLowerArm.CheckValues() && 
			 poseHand.CheckValues() &&
			 poseThumb.CheckValues()&& 
			 poseIndexFinger.CheckValues() && 
			 poseMiddleFinger.CheckValues() && 
			 poseRingFinger.CheckValues() && 
			 posePinkyFinger.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_UpperBodyPose
//	Pose of the avatar's upper body.  
//
struct _V2_UpperBodyPose
{
	// Data elements
	_V2_TorsoPose	poseTorso;		// Pose of the torso
	_V2_HeadPose	poseHead;		// Pose of the head
	_V2_ArmPose		poseLeftArm;	// Pose of the left arm
	_V2_ArmPose		poseRightArm;	// Pose of the right arm

	// Constructor, equality check and assignment
	_V2_UpperBodyPose() { Reset(); }
	bool operator==(const _V2_UpperBodyPose& pose) const
	{ return (0 == memcmp((const void*)this, (const void *)&pose, sizeof(pose))); 
	}
	const _V2_UpperBodyPose& operator=(const _V2_UpperBodyPose& pose)
	{ CopyMemory((void *)this, (const void *)&pose, sizeof(pose)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_UpperBodyPose)); }

	// Data checking
	bool CheckValues() const
	{ return poseTorso.CheckValues() && 
			 poseHead.CheckValues() &&
	         poseLeftArm.CheckValues() && 
			 poseRightArm.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_LegPose
//	Pose of an avatar leg.  
//	NOTE: All transformations are absolute.
//
struct _V2_LegPose
{
	// Data elements
	_V2_6D	poseUpperLeg;	// Pose of the upper leg
	_V2_6D	poseLowerLeg;	// Pose of the lower leg
	_V2_6D	poseFoot;		// Pose of the foot
	_V2_6D	poseToes;		// Pose of the toes

	// Constructor, equality check and assignment
	_V2_LegPose() { Reset(); }
	bool operator==(const _V2_LegPose& pose) const
	{ return (0 == memcmp((const void*)this, (const void *)&pose, sizeof(pose))); }
	const _V2_LegPose& operator=(const _V2_LegPose& pose)
	{ CopyMemory((void *)this, (const void *)&pose, sizeof(pose)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_LegPose)); }

	// Data checking
	bool CheckValues() const
	{ return poseUpperLeg.CheckValues() && 
			 poseLowerLeg.CheckValues() &&
	         poseFoot.CheckValues() && 
			 poseToes.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_LowerBodyPose
//	Pose of the avatar's lower body.  
//
struct _V2_LowerBodyPose
{
	// Data elements
	_V2_LegPose	poseLeftLeg;		// Pose of the left leg
	_V2_LegPose	poseRightLeg;		// Pose of the right leg

	// Constructor, equality check and assignment
	_V2_LowerBodyPose() { Reset(); }
	bool operator==(const _V2_LowerBodyPose& pose) const
	{ return (0 == memcmp((const void*)this, (const void *)&pose, sizeof(pose))); 
	}
	const _V2_LowerBodyPose& operator=(const _V2_LowerBodyPose& pose)
	{ CopyMemory((void *)this, (const void *)&pose, sizeof(pose)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_LowerBodyPose)); }

	// Data checking
	bool CheckValues() const
	{ return poseLeftLeg.CheckValues() && 
			 poseRightLeg.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_SkeletalDef
//	Avatar's skeletal definition (binding pose and end bone lengths).  
//
struct _V2_SkeletalDef
{
	// Data elements
	_V2_6D				posePelvis;		// Binding pose of the pelvis
	_V2_LowerBodyPose	poseLowerBody;	// Binding pose of the lower body
	_V2_UpperBodyPose	poseUpperBody;	// Binding pose of the upper body
	float		lenSkullEnd;			// Length of the skull end bone
	float		lenLeftThumbEnd;		// Length of the left thumb end bone
	float		lenLeftIndexFingerEnd;	// Length of the left index finger end bone
	float		lenLeftMiddleFingerEnd;	// Length of the left middle finger end bone
	float		lenLeftRingFingerEnd;	// Length of the left ring finger end bone
	float		lenLeftPinkyFingerEnd;	// Length of the left pinky finger end bone
	float		lenRightThumbEnd;		// Length of the right thumb end bone
	float		lenRightIndexFingerEnd;	// Length of the right index finger end bone
	float		lenRightMiddleFingerEnd;// Length of the lerightft thumb end bone
	float		lenRightRingFingerEnd;	// Length of the right index finger end bone
	float		lenRightPinkyFingerEnd;	// Length of the right middle finger end bone
	float		lenLeftToesEnd;			// Length of the left toes end bone
	float		lenRightToesEnd;		// Length of the right toes end bone

	// Constructor, equality check and assignment
	_V2_SkeletalDef() { Reset(); }
	bool operator==(const _V2_SkeletalDef& def) const
	{ return (0 == memcmp((const void*)this, (const void *)&def, sizeof(def))); 
	}
	const _V2_SkeletalDef& operator=(const _V2_SkeletalDef& def)
	{ CopyMemory((void *)this, (const void *)&def, sizeof(def)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_SkeletalDef)); }

	// Data checking
	bool CheckValues() const
	{ return (posePelvis.CheckValues() && 
			  poseLowerBody.CheckValues() && 
			  poseUpperBody.CheckValues() && 
			  lenSkullEnd >= 0. &&
			  lenLeftThumbEnd >= 0. &&
			  lenLeftIndexFingerEnd >= 0. &&
			  lenLeftMiddleFingerEnd >= 0. &&
			  lenLeftRingFingerEnd >= 0. &&
			  lenLeftPinkyFingerEnd >= 0. &&
			  lenRightThumbEnd >= 0. &&
			  lenRightIndexFingerEnd >= 0. &&
			  lenRightMiddleFingerEnd >= 0. &&
			  lenRightRingFingerEnd >= 0. &&
			  lenRightPinkyFingerEnd >= 0. &&
			  lenLeftToesEnd >= 0. &&
			  lenRightToesEnd >= 0.); 
	}
}; 

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_SitPos
//	The situated position of an object.  
//
struct _V2_SitPos
{
	// Data elements
	_V2_3DT		displacement;	// Situated displacement in global crd space
	float		heading;		// Situated heading rotation about Z (degrees)

	// Constructor, equality check and assignment
	_V2_SitPos() { Reset(); }
	bool operator==(const _V2_SitPos& sitpos) const
	{ return (0 == memcmp((const void*)this, (const void *)&sitpos, sizeof(sitpos))); 
	}
	const _V2_SitPos& operator=(const _V2_SitPos& sitpos)
	{ CopyMemory((void *)this, (const void *)&sitpos, sizeof(sitpos)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_SitPos)); }

	// Data checking
	bool CheckValues() const
	{ return CheckRotationAngle(heading) && displacement.CheckValues(); }
}; 

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_BoundBox
//	Bounding box an object.  
//
struct _V2_BoundBox
{
	// Data elements
	_V2_3DT	posMin;	// Minimum positional bounds relative to the object origin
	_V2_3DT	posMax;	// Maximum positional bounds relative to the object origin

	// Constructor, equality check and assignment
	_V2_BoundBox() { Reset(); }
	bool operator==(const _V2_BoundBox& box) const
	{ return (0 == memcmp((const void*)this, (const void *)&box, sizeof(box))); 
	}
	const _V2_BoundBox& operator=(const _V2_BoundBox& box)
	{ CopyMemory((void *)this, (const void *)&box, sizeof(box)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_BoundBox));	}

	// Data checking
	bool CheckValues() const
	{ return posMin.tx <= posMax.tx && 
			 posMin.ty <= posMax.ty &&
			 posMin.tz <= posMax.tz; 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_RotLimits
//	Negative and positive rotation limits of an object.
//	NOTE: All rotations are absolute.
//
struct _V2_RotLimits
{
	// Data elements
	_V2_3DR	rotNeg;	// Negative rotation limits
	_V2_3DR	rotPos;	// Positive rotation limits

	// Constructor, equality check and assignment
	_V2_RotLimits() { Reset(); }
	bool operator==(const _V2_RotLimits& limits) const
	{ return (0 == memcmp((const void*)this, (const void *)&limits, sizeof(limits))); 
	}
	const _V2_RotLimits& operator=(const _V2_RotLimits& limits)
	{ CopyMemory((void *)this, (const void *)&limits, sizeof(limits)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_RotLimits));
	  rotNeg.rx = rotNeg.ry = rotNeg.rz = -360.f; 
	  rotPos.rx = rotPos.ry = rotPos.rz = 360.f; 
	}

	// Data checking
	bool CheckValues() const
	{ return rotNeg.rx >= -360.f && rotNeg.rx <= 0.f && 
			 rotNeg.ry >= -360.f && rotNeg.ry <= 0.f && 
			 rotNeg.rz >= -360.f && rotNeg.rz <= 0.f && 
			 rotPos.rx <= 360.f && rotPos.rx >= 0.f && 
			 rotPos.ry <= 360.f && rotPos.ry >= 0.f && 
			 rotPos.rz <= 360.f && rotPos.rz >= 0.f; 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_HandGrip
//	Describes a grip (hand position and pose) associated with a held object.  
//	NOTE: All transformations are global within the reference frame of the 
//	held object.
//
struct _V2_HandGrip
{
	// Data elements
	BYTE			bHand;				// 1 = left, 2 = right
	BYTE			bReserved[3];
	_V2_6D			poseHand;			// Pose of the hand
	_V2_FingerPose	poseThumb;			// Pose of the thumb
	_V2_FingerPose	poseIndexFinger;	// Pose of the index finger
	_V2_FingerPose	poseMiddleFinger;	// Pose of the middle finger
	_V2_FingerPose	poseRingFinger;		// Pose of the ring finger
	_V2_FingerPose	posePinkyFinger;	// Pose of the pinky finger

	// Constructor, equality check and assignment
	_V2_HandGrip() { Reset(); }
	bool operator==(const _V2_HandGrip& grip) const
	{ return (0 == memcmp((const void*)this, (const void *)&grip, sizeof(grip))); 
	}
	const _V2_HandGrip& operator=(const _V2_HandGrip& grip)
	{ CopyMemory((void *)this, (const void *)&grip, sizeof(grip)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_HandGrip)); 
	  bHand = LEFT; 
	}

	// Data checking
	bool CheckValues() const
	{ return CheckEitherLeftRight(bHand) && 
			 poseHand.CheckValues() &&
			 poseThumb.CheckValues() &&
			 poseIndexFinger.CheckValues() &&
			 poseMiddleFinger.CheckValues() &&
			 poseRingFinger.CheckValues() &&
			 posePinkyFinger.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_AttachPos
//	Describes how an object is attached to a host object.  
//	NOTE: All transformations are absolute.
//
struct _V2_AttachPos
{
	// Data elements
	WORD	wHostId;			// Host ID
	WORD	wHostBoneId;		// Host bone ID
	_V2_6D	poseObject;			// Pose of the attached object in ref frame of host bone
	DWORD	dwTransitionTime;	// Time in milliseconds required to move object
								//	between its attached and nonattached positions.

	// Constructor, equality check and assignment
	_V2_AttachPos() { Reset(); }
	bool operator==(const _V2_AttachPos& pos) const
	{ return (0 == memcmp((const void*)this, (const void *)&pos, sizeof(pos))); 
	}
	const _V2_AttachPos& operator=(const _V2_AttachPos& pos)
	{ CopyMemory((void *)this, (const void *)&pos, sizeof(pos)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_AttachPos)); 
	  dwTransitionTime = DEF_TRANSITION_TIME; 
	}

	// Data checking
	bool CheckValues() const
	{ return poseObject.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_HeldObject
//	Base definition for a held object.  
//	NOTE: All transformations are absolute with respect to the object's local
//	coord space.
//
struct _V2_HeldObject
{
	// Data elements
	char	strDescription[30];		// Object description
	BYTE	bObjectType;			// Held object type
	BYTE	bStowable;				// 1 = stowable object, 0 = not stowable
	BYTE	bGripType;				// 1 = left hand, 2 = right hand, 3 = both
	BYTE	bReserved;
	_V2_HandGrip	gripLeftHand;	// Left hand grip
	_V2_HandGrip	gripRightHand;	// Right hand grip
	_V2_AttachPos	stowPos;		// Object'stow position (on the avatar)
	_V2_BoundBox	boundBox;		// Object's bounding box
	float			weight;			// Object's weight (kg)

	// Constructor, equality check and assignment
	_V2_HeldObject() { Reset(); }
	bool operator==(const _V2_HeldObject& obj) const
	{ return (0 == memcmp((const void*)this, (const void *)&obj, sizeof(obj))); 
	}
	const _V2_HeldObject& operator=(const _V2_HeldObject& obj)
	{ CopyMemory((void *)this, (const void *)&obj, sizeof(obj)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_HeldObject));
	  bObjectType = HELD_OBJECT_HANDS_FREE; 
	  bStowable = TRUE;
	  bGripType = LEFT | RIGHT; 
	  gripLeftHand.Reset(); 
	  gripLeftHand.bHand = LEFT;
	  gripRightHand.Reset(); 
	  gripRightHand.bHand = RIGHT;
	  stowPos.Reset();
	}

	// Data checking
	//virtual bool CheckValues() const
	bool CheckValues() const
	{ return CheckHeldObject(bObjectType) && 
			 CheckBoolParam(bStowable) &&
			 (CheckEitherLeftRight(bGripType) || CheckBothLeftRight(bGripType)) && 
			 gripLeftHand.CheckValues() && 
			 gripRightHand.CheckValues() && 
			 stowPos.CheckValues() &&
			 boundBox.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_AimSubsysOpParams
//	Operational parameters for an aim object subsystem.  
//
struct _V2_AimSubsysOpParams
{
	// Data elements
	BYTE	bOpticsType;			// Optics type
	BYTE	bTriggered;				// 1 = object is a triggered device, 0 = not
	BYTE	bReloadable;			// 1 = object uses reloadable ammo, 0 = not
	BYTE	bBurstCount;			// Number of shots filed in burst mode
	BYTE	bFullAuto;				// Full auto flag; 0 = none, 1 = hold-type, 2 = toggle-type
	float	dMinOpticZoom;			// Minimum optic zoom level
	float	dMaxOpticZoom;			// Maximum optic zoom level
	_V2_AttachPos	ammoLoadPos;	// Ammo insertion position on the aim object
	float	cyclingRate;			// Cycling rate (rounds per minute)

	// Constructor, equality check and assignment
	_V2_AimSubsysOpParams() { Reset(); }
	bool operator==(const _V2_AimSubsysOpParams& params) const
	{ return (0 == memcmp((const void*)this, (const void *)&params, sizeof(params))); 
	}
	const _V2_AimSubsysOpParams& operator=(const _V2_AimSubsysOpParams& params)
	{ CopyMemory((void *)this, (const void *)&params, sizeof(params)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_AimSubsysOpParams));
	  bOpticsType = OPTICS_IRON_SIGHT; 
	  bTriggered = TRUE; 
	  bReloadable = TRUE;
	  bBurstCount = 3; 
	  bFullAuto = FULL_AUTO_NONE;
	  dMinOpticZoom = 1.f; 
	  dMaxOpticZoom = 1.f;
	  ammoLoadPos.Reset(); 
	  cyclingRate = 20.f; 
	}

	// Data checking
	bool CheckValues() const
	{ return CheckOpticsType(bOpticsType) && 
			 CheckFullAutoFlag(bFullAuto) &&
			 CheckBoolParam(bTriggered) &&
			 CheckBoolParam(bReloadable) && 
			 CheckZoomRange(dMinOpticZoom, dMaxOpticZoom) && 
			 ammoLoadPos.CheckValues() && 
			 cyclingRate >= 0.f; 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_AimObject
//	Definition for a carried aim object (e.g., personal weapon or binoculars)
//	NOTE: All positional coordinates are specified with respect to the object's
//	local coord space, in which the object is aligned so that the bore of the
//	object points down the +Y axis.
//
struct _V2_AimObject : public _V2_HeldObject
{
	// Data elements
	BYTE	bBarrel;			// 1 = object has a barrel, 0 = not
	BYTE	bStock;				// 1 = object has a stock, 0 = not
	BYTE	bAttachedSubsys;	// 1 = object has an attached subsystem, 0 = not
	BYTE	bReserved;	
	_V2_3DT	offsetSight;		// Sight offset from the object origin
	_V2_3DT	offsetBarrelTip;	// Barrel tip offset from the object origin
	_V2_3DT	offsetButtStock;	// Butt stock offset from the object origin
	float	rxAim;				// Rotation about X which brings line of aim 
								//	parallel to +Y axis (degrees)
	float	eyeReliefDist;		// Distance along line of aim from the sight 
								//	position to the avatar's right eye (meters)
	_V2_RotLimits	rotLimits;	// Object rotation limits
	_V2_AttachPos	quickStowPos;		// Object's quick stow position
	_V2_AimSubsysOpParams opMain;		// Main subsystem operational parameters
	_V2_AimSubsysOpParams opAttached;	// Attached subsystem operational parameters 

	// Constructor, equality check and assignment
	_V2_AimObject() { Reset(); }
	bool operator==(const _V2_AimObject& object) const
	{ return (0 == memcmp((const void*)this, (const void *)&object, sizeof(object))); 
	}
	const _V2_AimObject& operator=(const _V2_AimObject& object)
	{ CopyMemory((void *)this, (const void *)&object, sizeof(object)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_AimObject));
	  ((_V2_HeldObject*)this)->Reset();
	  bBarrel = TRUE; 
	  bStock = TRUE; 
	  bAttachedSubsys = FALSE;
	  rxAim = 0.f; 
	  eyeReliefDist = 0.1f; 
	  rotLimits.Reset(); 
	  quickStowPos.Reset(); 
	  opMain.Reset(); 
	  opAttached.Reset(); 
	}

	// Data checking
	//virtual bool CheckValues() const
	bool CheckValues() const
	{ return _V2_HeldObject::CheckValues() && 
			 CheckBoolParam(bBarrel) && 
			 CheckBoolParam(bStock) && 
			 CheckBoolParam(bAttachedSubsys) && 
			 offsetSight.CheckValues() && 
			 offsetBarrelTip.CheckValues() && 
			 offsetButtStock.CheckValues() && 
			 eyeReliefDist > 0.f && 
			 rotLimits.CheckValues() && 
			 quickStowPos.CheckValues() && 
			 opMain.CheckValues() && 
			 opAttached.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_MountedAimObject
//	Definition for a mounted aim object. 
//  NOTE: In its local coord space the object is aligned so that the bore of
//	the object points down the +Y axis.
//
struct _V2_MountedAimObject : public _V2_AimObject
{
	// Data elements
	_V2_6D	poseObject;		// Binding pose of the mounted aim object
	_V2_3DT	offsetPivot;	// Offset of pivot point relative to the object origin

	// Constructor, equality check and assignment
	_V2_MountedAimObject() { Reset(); }
	bool operator==(const _V2_MountedAimObject& object) const
	{ return (0 == memcmp((const void*)this, (const void *)&object, sizeof(object))); 
	}
	const _V2_MountedAimObject& operator=(const _V2_MountedAimObject& object)
	{ CopyMemory((void *)this, (const void *)&object, sizeof(object)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_MountedAimObject)); 
	  ((_V2_AimObject*)this)->Reset(); 
	}

	// Data checking
	//virtual bool CheckValues() const
	bool CheckValues() const
	{ return _V2_AimObject::CheckValues() && 
			 poseObject.CheckValues() && 
			 offsetPivot.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_AmmoObject
//	Definition for an ammunition object.  
//
struct _V2_AmmoObject : public _V2_HeldObject
{
	// Data elements
	BYTE	bMagazine;		// 1 = magazine, 0 = no magazine
	BYTE	bReserved[3];
	DWORD	numRounds;		// Number of rounds or grenades remaining
	float	recoilEnergy;	// Measure of recoil when object is fired (foot-pounds)

	// Constructor, equality check and assignment
	_V2_AmmoObject() { Reset(); }
	bool operator==(const _V2_AmmoObject& object) const
	{ return (0 == memcmp((const void*)this, (const void *)&object, sizeof(object))); 
	}
	const _V2_AmmoObject& operator=(const _V2_AmmoObject& object)
	{ CopyMemory((void *)this, (const void *)&object, sizeof(object)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_AmmoObject)); 
	  ((_V2_HeldObject*)this)->Reset();
	}

	// Data checking
	//virtual bool CheckValues() const
	bool CheckValues() const
	{ return _V2_HeldObject::CheckValues() && 
			 CheckBoolParam(bMagazine) &&
			 recoilEnergy >= 0.f; 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_ServerMotionDat
//	Pointman-generated motion data.  
//
struct _V2_ServerMotionDat
{
	// Data elements
	BYTE	bMobilityState;		// Mobility state identifier
	BYTE	bMotionCtrlLower;	// Lower body motion control level
	BYTE	bMotionCtrlUpper;	// Upper body motion control level
	BYTE	bHeldObject;		// Held object type
	BYTE	bBodyPosture;		// Body posture identifier
	BYTE	bSpeedState;		// Dismounted speed state identifier
	BYTE	bAiming;			// 1 = aim object raised, 0 = not 
	BYTE	bOptics;			// 1 = display optics, 0 = not
	float	fZoom;				// Visual zoom factor
	DWORD	dwFireCount;		// Count of "shots" fired
	_V2_SitPos	sitPos;			// Avatar situated displacement & heading
	_V2_3DT		trnPelvis;		// Pre-situated pelvis displacement
	_V2_3DR		rotPelvis;		// Pre-situated pelvis rotation
	_V2_3DR		rotDirTorso;	// Directed torso rotation
	_V2_3DR		rotDirView;		// Directed view rotation
	_V2_3DR		rotDirAim;		// Directed aim rotation
	_V2_LowerBodyPose poseLowerBody;	// Lower body pose rotations
	_V2_UpperBodyPose poseUpperBody;	// Upper body pose rotations
	_V2_6D	poseRightCamera;	// Pre-situatedright camera pose position
	_V2_6D	poseLeftCamera;		// Pre-situated left camera pose position
	_V2_6D	poseHeldObject;		// Pre-situated held object pose position

	// Constructor, equality check and assignment
	_V2_ServerMotionDat() { Reset(); }
	bool operator==(const _V2_ServerMotionDat& dat) const
	{ return (0 == memcmp((const void*)this, (const void *)&dat, sizeof(dat))); 
	}
	const _V2_ServerMotionDat& operator=(const _V2_ServerMotionDat& dat)
	{ CopyMemory((void *)this, (const void *)&dat, sizeof(dat)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_ServerMotionDat));
	  bMobilityState = DEF_MOBILITY_STATE; 
	  bMotionCtrlLower = DEF_MOTION_CTRL_LEVEL; 
	  bMotionCtrlUpper = DEF_MOTION_CTRL_LEVEL;
	  bHeldObject = DEF_HELD_OBJECT;
	  bBodyPosture = DEF_BODY_POSTURE; 
	  bSpeedState = DEF_SPEED_STATE;
	  bAiming = FALSE; 
	  bOptics = FALSE; 
	  fZoom = DEF_ZOOM; 
	}

	// Data checking
	bool CheckValues() const
	{ return CheckMobilityState(bMobilityState) && 
			 CheckMotionControlLevel(bMotionCtrlLower) &&
			 CheckMotionControlLevel(bMotionCtrlUpper) &&
			 CheckHeldObject(bHeldObject) && 
			 CheckBodyPosture(bBodyPosture) &&
			 CheckSpeedState(bSpeedState) && 
			 CheckBoolParam(bAiming) &&
			 CheckBoolParam(bOptics) && 
			 CheckZoom(fZoom) && 
			 sitPos.CheckValues() && 
			 trnPelvis.CheckValues() && 
			 rotPelvis.CheckValues() && 
			 rotDirTorso.CheckValues() && 
			 rotDirView.CheckValues() && 
			 rotDirAim.CheckValues() && 
			 poseLowerBody.CheckValues() && 
			 poseUpperBody.CheckValues() && 
			 poseRightCamera.CheckValues() && 
			 poseLeftCamera.CheckValues() &&
			 poseHeldObject.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_ServerJoyDat
//	Pointman-generated composite joystick data. 
//
struct _V2_ServerJoyDat
{
	// Data elements
	BYTE	arbButtons[NUM_JOY_BUTTONS];	// Button values (zero if unavailable)
	LONG	arlPovs[NUM_JOY_POVS];			// POV values (0xFFFF if unavailable)
	float	arfAxes[NUM_JOY_AXES];			// Axis values (zero if unavailable)

	// Constructor, equality check and assignment
	_V2_ServerJoyDat() { Reset(); }
	bool operator==(const _V2_ServerJoyDat& dat) const
	{ return (0 == memcmp((const void*)this, (const void *)&dat, sizeof(dat))); 
	}
	const _V2_ServerJoyDat& operator=(const _V2_ServerJoyDat& dat)
	{ CopyMemory((void *)this, (const void *)&dat, sizeof(dat)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_ServerJoyDat));
	  FillMemory((void *)&(arlPovs[0]), NUM_JOY_POVS*sizeof(LONG), 0xFF);
	}

	// Check values
	bool CheckValues() 
	{ for (int ipov = 0; ipov < NUM_JOY_POVS; ipov++) {
		if (arlPovs[ipov] > 36000 || arlPovs[ipov] != 0xFFFF)
			return false; }
	  for (int iaxis = 0; iaxis < NUM_JOY_AXES; iaxis++) {
		if (arfAxes[iaxis] < -1.f || arfAxes[iaxis] > 1.f)
			return false; }
	  return true; 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_Haptic
//	Describes a tactile (haptic) sensation felt by the user.  
//
struct _V2_Haptic
{
	// Data elements
	float	fMagnitude;	// Relative magnitude (0 = min, 1 = max)
	float	fSharpness;	// Relative sharpness (0 = min, 1 = max)

	// Constructor, equality check and assignment
	_V2_Haptic() { Reset(); }
	bool operator==(const _V2_Haptic& dat) const
	{ return (0 == memcmp((const void*)this, (const void *)&dat, sizeof(dat))); 
	}
	const _V2_Haptic& operator=(const _V2_Haptic& dat)
	{ CopyMemory((void *)this, (const void *)&dat, sizeof(dat)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_Haptic)); }

	// Data checking
	bool CheckValues() const
	{ return fMagnitude >= 0.f && fMagnitude <= 1.f && 
			 fSharpness >= 0.f && fSharpness <= 1.f; 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_HapticEvent
//	Describes an event that causes a transient tactile (haptic) sensation
//	felt by the user.  
//
struct _V2_HapticEvent
{
	// Data elements
	DWORD		dwCount;	// Event count
	_V2_Haptic	haptic;		// Tactile sensation

	// Constructor, equality check and assignment
	_V2_HapticEvent() { Reset(); }
	bool operator==(const _V2_HapticEvent& dat) const
	{ return (0 == memcmp((const void*)this, (const void *)&dat, sizeof(dat))); 
	}
	const _V2_HapticEvent& operator=(const _V2_HapticEvent& dat)
	{ CopyMemory((void *)this, (const void *)&dat, sizeof(dat)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_HapticEvent)); }

	// Data checking
	bool CheckValues() const
	{ return haptic.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_ClientStateDat
//	VBS-generated state data.  
//
struct _V2_ClientStateDat
{
	// Data elements
	float	fSitElevOffsets[NUM_SIT_ELEV_OFFSETS]; // Terrain elevation offsets about
									//	the pelvis situated ground projection (meters)
	float	fZoom;			// Optical zoom factor
	float	fHealth;		// Avatar health level
	float	fFatigue;		// Avatar fatigue level
	_V2_Haptic	contactBody;	// Tactile sensation due to contact of the 
								//	avatar's body with surfaces
	_V2_Haptic	contactHeldObject;	// Tactile sensation due to contact of the
									//	held object with surfaces
	_V2_HapticEvent	eventExplosion;	// Haptic event associated with blast wave 
									//	of an explosion
	_V2_HapticEvent	eventBulletStrike;	// Haptic event associated with a bullet
										//	or shrapnel strike

	// Constructor, equality check and assignment
	_V2_ClientStateDat() { Reset(); }
	bool operator==(const _V2_ClientStateDat& dat) const
	{ return (0 == memcmp((const void*)this, (const void *)&dat, sizeof(dat))); 
	}
	const _V2_ClientStateDat& operator=(const _V2_ClientStateDat& dat)
	{ CopyMemory((void *)this, (const void *)&dat, sizeof(dat)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_ClientStateDat));
	  fZoom = DEF_ZOOM; 
	  fHealth = DEF_HEALTH; 
	  fFatigue = DEF_FATIGUE; 
	  contactBody.Reset(); 
	  contactHeldObject.Reset(); 
	  eventExplosion.Reset(); 
	  eventBulletStrike.Reset(); 
	}

	// Data checking
	bool CheckValues() const
	{ return CheckZoom(fZoom) &&
			 CheckHealth(fHealth) &&
			 CheckFatigue(fFatigue) &&
			 contactBody.CheckValues() &&
			 contactHeldObject.CheckValues() &&
			 eventExplosion.CheckValues() &&
			 eventBulletStrike.CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_UserParams
//	VbsPointman user parameters.  
//
struct _V2_UserParams
{
	// Data elements
	BYTE	bDominantEye;		// User's dominant eye
	BYTE	bDominantHand;		// User's dominant hand
	BYTE	bReserved[2];
	float	fInterocularDist;	// User's interocular distance (meters)

	// Constructor, equality check and assignment
	_V2_UserParams() { Reset(); }
	bool operator==(const _V2_UserParams& params) const
	{ return (0 == memcmp((const void*)this, (const void *)&params, sizeof(params))); 
	}
	const _V2_UserParams& operator=(const _V2_UserParams& params)
	{ CopyMemory((void *)this, (const void *)&params, sizeof(params)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_UserParams));
	  bDominantEye = DEF_DOMINANT_SIDE;
	  bDominantHand = DEF_DOMINANT_SIDE;
	  fInterocularDist = DEF_INTEROCULAR_DIST; 
	}

	// Data checking
	bool CheckValues() const
	{ return CheckEitherLeftRight(bDominantEye) && 
			 CheckEitherLeftRight(bDominantHand) && 
			 fInterocularDist >= 0.f; 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_DisplayConfig
//	Video display configuration parameters.  
//
struct _V2_DisplayConfig
{
	// Data elements
	BYTE	bHeadMounted;	// 0 = fixed display, 1 = head-mounted display
	BYTE	bStereo;		// 0 = mono display, 1 = stereo display
	BYTE	bReserved[2];

	// Constructor, equality check and assignment
	_V2_DisplayConfig() { Reset(); }
	bool operator==(const _V2_DisplayConfig& config) const
	{ return (0 == memcmp((const void*)this, (const void *)&config, sizeof(config))); 
	}
	const _V2_DisplayConfig& operator=(const _V2_DisplayConfig& config)
	{ CopyMemory((void *)this, (const void *)&config, sizeof(config)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_DisplayConfig));
	  bHeadMounted = FALSE;
	  bStereo = FALSE; 
	}

	// Data checking
	bool CheckValues() const
	{ return CheckBoolParam(bHeadMounted) && 
			 CheckBoolParam(bStereo); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_MotionState
//	VBS motion state parameters.  
//
struct _V2_MotionState
{
	// Data elements
	BYTE	bMobilityState;		// Mobility state identifier
	BYTE	bMotionCtrlLower;	// Lower body motion control level
	BYTE	bMotionCtrlUpper;	// Upper body motion control level
	BYTE	bBodyPosture;		// Body posture identifier
	BYTE	bHeldObject;		// Held object type
	BYTE	bObjectStorage;		// Object storage source or destination
	BYTE	bAiming;			// 1 = aim object raised, 0 = not 
	BYTE	bOptics;			// 1 = display optics, 0 = not 
	_V2_RotLimits	rotLimitsDirTorso;	// Directed torso rotation limits
	_V2_RotLimits	rotLimitsDirView;	// Directed view rotation limits
	_V2_SitPos	sitPosModelSpace;	// Avatar's situated position in model space
	_V2_6D		posePelvis;			// Pre-situated pelvis position & rotation
	_V2_LowerBodyPose poseLowerBody;	// Lower body stationary pose rotations
	_V2_UpperBodyPose poseUpperBody;	// Upper body pre-directed pose rotations

	// Constructor, equality check and assignment
	_V2_MotionState() { Reset(); }
	bool operator==(const _V2_MotionState& params) const
	{ return (0 == memcmp((const void*)this, (const void *)&params, sizeof(params))); 
	}
	const _V2_MotionState& operator=(const _V2_MotionState& params)
	{ CopyMemory((void *)this, (const void *)&params, sizeof(params)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_MotionState));
	  bMobilityState = MOBILITY_GROUNDED; 
	  bMotionCtrlLower = MOTION_CTRL_FULL; 
	  bMotionCtrlUpper = MOTION_CTRL_FULL;
	  bBodyPosture = BODY_POSTURE_STAND; 
	  bHeldObject = HELD_OBJECT_HANDS_FREE;
	  bObjectStorage = OBJECT_STORAGE_STOW;
	  bAiming = FALSE; 
	  bOptics = FALSE;
	  rotLimitsDirTorso.Reset();
	  rotLimitsDirView.Reset();
	}

	// Data checking
	bool CheckValues() const
	{ return CheckMobilityState(bMobilityState) && 
			 CheckMotionControlLevel(bMotionCtrlLower) &&
			 CheckMotionControlLevel(bMotionCtrlUpper) &&
			 CheckBodyPosture(bBodyPosture) &&
			 CheckHeldObject(bHeldObject) && 
			 CheckObjectStorage(bObjectStorage) &&
			 CheckBoolParam(bAiming) &&
			 CheckBoolParam(bOptics) &&
			 rotLimitsDirTorso.CheckValues() &&
			 rotLimitsDirView.CheckValues() &&
			 sitPosModelSpace.CheckValues() &&
			 posePelvis.CheckValues() &&
			 poseLowerBody.CheckValues() && 
			 poseUpperBody.CheckValues(); 
	} 
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_FireState
//	Describes the current fire state.  
//
struct _V2_FireState
{
	// Data elements
	BYTE	bSubsystem;		// Fire subsystem
	BYTE	bFireMode;		// Fire mode
	BYTE	bReserved[2];
	_V2_AmmoObject ammo;	// Ammunition object

	// Constructor, equality check and assignment
	_V2_FireState() { Reset(); }
	bool operator==(const _V2_FireState& fireState) const
	{ return (0 == memcmp((const void*)this, (const void *)&fireState, sizeof(fireState))); 
	}
	const _V2_FireState& operator=(const _V2_FireState& fireState)
	{ CopyMemory((void *)this, (const void *)&fireState, sizeof(fireState)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_FireState));
	  bSubsystem = SUBSYS_THROW;
	  bFireMode = FIRE_NONE;
	  ammo.Reset(); 
	}

	// Data checking
	bool CheckValues() const
	{ return CheckFireSubsystem(bSubsystem) && 
			 CheckFireMode(bFireMode) &&
			 ((_V2_AmmoObject)ammo).CheckValues(); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_PreSitPelvisRotations
//	Pre-situated pelvis rotations associated with the discrete body postures.  
//
struct _V2_PreSitPelvisRotations
{
	// Data elements
	_V2_3DR	arRotPelvis[NUM_BODY_POSTURES];	 

	// Constructor, equality check and assignment
	_V2_PreSitPelvisRotations() { Reset(); }
	bool operator==(const _V2_PreSitPelvisRotations& rots) const
	{ return (0 == memcmp((const void*)this, (const void *)&rots, sizeof(rots))); 
	}
	const _V2_PreSitPelvisRotations& operator=(const _V2_PreSitPelvisRotations& rots)
	{ CopyMemory((void *)this, (const void *)&rots, sizeof(rots)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_PreSitPelvisRotations));
		for (int iposture = 0; iposture < NUM_BODY_POSTURES; iposture++) 
		{
			if (iposture == BODY_POSTURE_PRONE) arRotPelvis[iposture].rx = -90.; 
		}
	}

	// Data checking
	bool CheckValues() const
	{ for (int iposture = 0; iposture < NUM_BODY_POSTURES; iposture++) 
	  {
		if (!((arRotPelvis[iposture]).CheckValues())) return false; 
	  }
	  return true;
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_SimControlParams
//	Parameters which affect simulator control of the avatar in the virtual 
//	environment.  
//
struct _V2_SimControlParams
{
	// Data elements
	BYTE	bSimCollisionHandling;	// 1 = VBS simulator applies collision 
									//	handling, 0 = not
	BYTE	bSimTerrainFollowing;	// 1 = VBS simulator applies terrain 
									//	following, 0 = not
	BYTE	bReserved[2];

	// Constructor, equality check and assignment
	_V2_SimControlParams() { Reset(); }
	bool operator==(const _V2_SimControlParams& params) const
	{ return (0 == memcmp((const void*)this, (const void *)&params, sizeof(params))); 
	}
	const _V2_SimControlParams& operator=(const _V2_SimControlParams& params)
	{ CopyMemory((void *)this, (const void *)&params, sizeof(params)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_SimControlParams));
	  bSimCollisionHandling = TRUE;
	  bSimTerrainFollowing = TRUE; 
	}

	// Data checking
	bool CheckValues() const
	{ return CheckBoolParam(bSimCollisionHandling) && 
			 CheckBoolParam(bSimTerrainFollowing); 
	}
};

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_XmitFreqRanges
//	Specifies the minimum and maximum data transmission rates for 
//	continuous data transmitted between VBS and Pointman.
///////////////////////////////////////////////////////////////////////////////
struct _V2_XmitFreqRanges
{
	// Data elements
		// min/max xmit rates for Xmit_Server_Motion_Input packets (packets/sec)
	WORD	nMinFreqServerMotion, nMaxFreqServerMotion; 
		// min/max xmit rates for Xmit_Server_Joy_Input (packets/sec)
	WORD	nMinFreqServerJoy, nMaxFreqServerJoy;	
		// min/max xmit rates for Xmit_Client_State packets (packets/sec)
	WORD	nMinFreqClientState, nMaxFreqClientState;

	// Constructor, equality check and assignment
	_V2_XmitFreqRanges() { Reset(); }
	bool operator==(const _V2_XmitFreqRanges& dat) const
	{ return (0 == memcmp((const void*)this, (const void *)&dat, sizeof(dat))); 
	}
	const _V2_XmitFreqRanges& operator=(const _V2_XmitFreqRanges& dat)
	{ CopyMemory((void *)this, (const void *)&dat, sizeof(dat));	
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_XmitFreqRanges));
	  nMinFreqServerMotion = nMinFreqServerJoy = nMinFreqClientState = DEF_MIN_XMIT_FREQ;
	  nMaxFreqServerMotion = nMaxFreqServerJoy = nMaxFreqClientState = DEF_MAX_XMIT_FREQ; 
	}

	// Data checking
	bool CheckValues() const
	{ return nMinFreqServerMotion > 0 && nMaxFreqServerMotion >= nMinFreqServerMotion &&
			 nMinFreqServerJoy > 0 && nMaxFreqServerJoy >= nMinFreqServerJoy &&
			 nMinFreqClientState > 0 && nMaxFreqClientState >= nMinFreqClientState; 
	}
}; 

///////////////////////////////////////////////////////////////////////////////
// Struct _V2_ServerJoyCaps
//	Composite joystick availability info. 
//	The value set for each joystick object (button, pov or axis) is the bit-wise
//	'OR' of the object's availability for each of the three levels of motion
//	control over the avatar's upper body and lower body.
//
struct _V2_ServerJoyCaps
{
	// Data elements
	BYTE	arbAvailButtons[NUM_JOY_BUTTONS];	// button availability
	BYTE	arbAvailPovs[NUM_JOY_POVS];			// pov availability
	BYTE	arbAvailAxes[NUM_JOY_AXES];			// axis availability

	// Constructor, equality check and assignment
	_V2_ServerJoyCaps() { Reset(); }
	bool operator==(const _V2_ServerJoyCaps& dat) const
	{ return (0 == memcmp((const void*)this, (const void *)&dat, sizeof(dat))); 
	}
	const _V2_ServerJoyCaps& operator=(const _V2_ServerJoyCaps& dat)
	{ CopyMemory((void *)this, (const void *)&dat, sizeof(dat)); 
	  return *this; 
	}

	// Data reset
	void Reset()
	{ SecureZeroMemory((void *)this, sizeof(_V2_ServerJoyCaps)); }

	// Data checking
	bool CheckValues() 
	{ return true; }

	// Helper function to query joystick object availability
	bool IsButtonAvailable(int ibutton, BYTE bMotionCtrlLower,
										BYTE bMotionCtrlUpper)
	{ return CheckJoyButton(ibutton) && 
			 CheckMotionControlLevel(bMotionCtrlLower) &&
			 CheckMotionControlLevel(bMotionCtrlUpper) &&
			 (bMotionCtrlLower & arbAvailButtons[ibutton]) != 0 &&
			 ((bMotionCtrlUpper << 4) & arbAvailButtons[ibutton]) != 0; 
	}
	bool IsPovAvailable(int ipov, BYTE bMotionCtrlLower,
								  BYTE bMotionCtrlUpper)
	{ return CheckJoyPov(ipov) && 
			 CheckMotionControlLevel(bMotionCtrlLower) &&
			 CheckMotionControlLevel(bMotionCtrlUpper) &&
			 (bMotionCtrlLower & arbAvailPovs[ipov]) != 0 &&
			 ((bMotionCtrlUpper << 4) & arbAvailPovs[ipov]) != 0; 
	}
	bool IsAxisAvailable(int iaxis, BYTE bMotionCtrlLower,
									BYTE bMotionCtrlUpper)
	{ return CheckJoyAxis(iaxis) && 
			 CheckMotionControlLevel(bMotionCtrlLower) &&
			 CheckMotionControlLevel(bMotionCtrlUpper) &&
			 (bMotionCtrlLower & arbAvailAxes[iaxis]) != 0 &&
			 ((bMotionCtrlUpper << 4) & arbAvailAxes[iaxis]) != 0; 
	}
};

//#pragma pack(pop)

// Maximum data size
const WORD MAX_DATA_SIZE = sizeof(_V2_ServerMotionDat);
const WORD MAX_SERVER_XMIT_DATA_SIZE = 
		   max(sizeof(_V2_ServerMotionDat), sizeof(_V2_ServerJoyDat));

}  // namespace V2_Interface

