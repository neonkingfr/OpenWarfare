#ifndef _POINTMAN_POSE_H
#define _POINTMAN_POSE_H

#include "VBSFusion.h"
#include "VBSFusionDefinitions.h"

#include "mathHelper.h"

#include "vbspointman2.h"

using namespace VBSFusion;

#define DEBUGPOSE  1

#define MaskSlotPrimary     0x00000001  // primary weapons
#define MaskSlotSecondary   0x00000004  // secondary weapons
#define MaskSlotItem        0x00000F00  // items
#define MaskSlotBinocular   0x00003000  // binocular
#define MaskHardMounted     0x00010000  // hard mounted
#define MaskSlotHandGun     0x00000002  // hand gun
#define MaskSlotHandGunItem 0x000000F0  // hand gun magazines
#define MaskSlotInventory   0x001E0000  // hand gun magazines

enum CameraType
{
  CamInternal,
  CamGunner,
  CamExternal,
  CamGroup,
  CamView,
  MaxCameraType
};

enum ManPos
{
  ManPosDead,
  ManPosWeapon, // special weapon - AT only (not a rifle or so)
  ManPosBinocLying,
  ManPosLyingNoWeapon,
  ManPosLying,
  ManPosHandGunLying,
  ManPosCrouch,
  ManPosHandGunCrouch,
  ManPosCombat,
  ManPosHandGunStand, // this is for combat, in fact
  ManPosStand, // moves with weapon on the back or down in hand
  ManPosSwimming,
  ManPosNoWeapon, // civilian moves
  ManPosBinoc, // binocular position
  ManPosBinocStand, // binocular position (weapon on back)
  ManPosLauncherStand, //standing with launcher
  ManPosLauncherCrouch, //kneeling with launcher
  ManPosLauncherLying, //prone with launcher
  ManPosDiving, 
  ManPosBottomDiving,
  ManPosSurfaceDiving,
  ManPosBottomSwimming,
  ManPosSurfaceSwimming,
  ManPosGoingFromWater,
  ManPosLowCrouch, // Kneeling With Rifle Lowered
  ManPosHandGunLowCrouch, // Kneeling With Rifle Lowered
  ManPosHandGunLowStand, // Standing With Pistol Lowered
  ManPosBinocCrouch,
  ManPosLast, // to track count of ManPos items
  ManPosNormalMin = ManPosLyingNoWeapon,
  ManPosNormalMax = ManPosNoWeapon
};

enum AmmoSimulation
{ // basic ammo types
  AmmoNone,
  AmmoShotShell,
  AmmoShotMissile,
  AmmoShotRocket,
  AmmoShotBullet,
  AmmoShotSpread,
  AmmoShotIlluminating,
  AmmoShotSmoke,
  AmmoShotTimeBomb,
  AmmoShotPipeBomb,
  AmmoShotMine,
  AmmoShotStroke,
  AmmoShotLaser, //!< laser designator
  AmmoShotCM, //!< AA counter Measures
  AmmoShotMarker
};

//! Ancestor of all skeleton modifiers (explicit skeleton sources), so far only Pointman
class ExplicitPose
{
protected:
  //!{ Upper body control
  Matrix4 _torso; // Torso rotation angles
  Matrix4 _view;  // Head rotation angles
  Matrix4 _aim;   // Aim rotation angles (where the gun points to)
  //!}
  //! Gun pose
  Matrix4 _gun;

  //! Temp values used for passing results between callbacks
  Matrix4 _gunTrans;
  Matrix4 _gunBodyTrans;
  //! Camera pose
  Matrix4 _camera;

  Matrix4 _relativeChange;
  //! Display optics
  bool _enableOptics;
  int _oldCameraType;
  //! Camera zoom factor
  float _zoom;

  double _explicitSkeletonFactor;
  Vector3 _rtmPelvisOffset;

  int _userControlDisplayed;
  bool _isActive;

  bool _modifyBone;

  //! all VBS2 man skeleton bones containing their IDs
  int  _vid_pelvis;	
  int  _vid_camera;	
  int  _vid_spine;	
  int  _vid_spine1;	
  int  _vid_weapon;	
  int  _vid_launcher;	
  int  _vid_spine2;	
  int  _vid_spine3;	
  int  _vid_neck;	
  int  _vid_neck1;	
  int  _vid_head;	
  int  _vid_lbrow;	
  int  _vid_mbrow;	
  int  _vid_rbrow;	
  int  _vid_lmouth;	
  int  _vid_mmouth;	
  int  _vid_rmouth;	
  int  _vid_eyelids;	
  int  _vid_llip;	
  int  _vid_l_eye;	
  int  _vid_r_eye;	
  int  _vid_l_pupila;	
  int  _vid_r_pupila;	
  int  _vid_cheek_lf;	
  int  _vid_nose_tip;	
  int  _vid_lip_uplb;	
  int  _vid_jaw_ls;	
  int  _vid_lip_uplf;	
  int  _vid_lip_lc;	
  int  _vid_lip_lwlb;	
  int  _vid_lip_lwlf;	
  int  _vid_jaw_lm;	
  int  _vid_zig_lb;	
  int  _vid_lip_lwm;	
  int  _vid_lip_upm;	
  int  _vid_ear_l;	
  int  _vid_corr;	
  int  _vid_tongue_m;	
  int  _vid_tongue_f;	
  int  _vid_eyebrow_lb;	
  int  _vid_eyebrow_lf;	
  int  _vid_eyebrow_lm;	
  int  _vid_zig_lm;	
  int  _vid_eye_upl;	
  int  _vid_eye_lwl;	
  int  _vid_cheek_l;	
  int  _vid_cheek_lb;	
  int  _vid_zig_lt;	
  int  _vid_nose_l;	
  int  _vid_cheek_lm;	
  int  _vid_nose_r;	
  int  _vid_forehead_r;	
  int  _vid_forehead_m;	
  int  _vid_forehead_l;	
  int  _vid_cheek_rb;	
  int  _vid_eye_lwr;	
  int  _vid_cheek_r;	
  int  _vid_zig_rt;	
  int  _vid_zig_rm;	
  int  _vid_cheek_rf;	
  int  _vid_cheek_rm;	
  int  _vid_eyebrow_rm;	
  int  _vid_eyebrow_rf;	
  int  _vid_eye_upr;	
  int  _vid_eyebrow_rb;	
  int  _vid_tongue_b;	
  int  _vid_ear_r;	
  int  _vid_neck_l;	
  int  _vid_lip_uprf;	
  int  _vid_neck_r;	
  int  _vid_lip_uprb;	
  int  _vid_lip_rc;	
  int  _vid_lip_lwrb;	
  int  _vid_lip_lwrf;	
  int  _vid_neck_b;	
  int  _vid_zig_rb;	
  int  _vid_neck_t;	
  int  _vid_jaw_rf;	
  int  _vid_jaw_lf;	
  int  _vid_chin;	
  int  _vid_jaw_rm;	
  int  _vid_jaw_rs;	
  int  _vid_jaw;	
  int  _vid_headcutscene;	
  int  _vid_leftshoulder;	
  int  _vid_leftarm;	
  int  _vid_leftarmroll;	
  int  _vid_leftforearm;	
  int  _vid_leftforearmroll;	
  int  _vid_lefthand;	
  int  _vid_lefthandring;	
  int  _vid_lefthandring1;	
  int  _vid_lefthandring2;	
  int  _vid_lefthandring3;	
  int  _vid_lefthandpinky1;	
  int  _vid_lefthandpinky2;	
  int  _vid_lefthandpinky3;	
  int  _vid_lefthandmiddle1;	
  int  _vid_lefthandmiddle2;	
  int  _vid_lefthandmiddle3;	
  int  _vid_lefthandindex1;	
  int  _vid_lefthandindex2;	
  int  _vid_lefthandindex3;	
  int  _vid_lefthandthumb1;	
  int  _vid_lefthandthumb2;	
  int  _vid_lefthandthumb3;	
  int  _vid_rightshoulder;	
  int  _vid_rightarm;	
  int  _vid_rightarmroll;	
  int  _vid_rightforearm;	
  int  _vid_rightforearmroll;	
  int  _vid_righthand;	
  int  _vid_righthandring;	
  int  _vid_righthandring1;	
  int  _vid_righthandring2;	
  int  _vid_righthandring3;	
  int  _vid_righthandpinky1;	
  int  _vid_righthandpinky2;	
  int  _vid_righthandpinky3;	
  int  _vid_righthandmiddle1;	
  int  _vid_righthandmiddle2;	
  int  _vid_righthandmiddle3;	
  int  _vid_righthandindex1;	
  int  _vid_righthandindex2;	
  int  _vid_righthandindex3;	
  int  _vid_righthandthumb1;	
  int  _vid_righthandthumb2;	
  int  _vid_righthandthumb3;	
  int  _vid_leftupleg;	
  int  _vid_leftuplegroll;	
  int  _vid_leftleg;	
  int  _vid_leftlegroll;	
  int  _vid_leftfoot;	
  int  _vid_lefttoebase;	
  int  _vid_rightupleg;	
  int  _vid_rightuplegroll;	
  int  _vid_rightleg;	
  int  _vid_rightlegroll;	
  int  _vid_rightfoot;	
  int  _vid_righttoebase;	
  int  _vid_slot_backpack;	
  int  _vid_slot_backwpnl;	
  int  _vid_slot_backwpnr;	
  int  _vid_slot_buttpack;	
  int  _vid_slot_patrolwpn;	

  //!{ Skeleton definition matrices
  Matrix4 _b_pelvis;
  Matrix4 _b_lHip;
  Matrix4 _b_lKnee;
  Matrix4 _b_lAnkle;
  Matrix4 _b_lMetatarsal;
  Matrix4 _b_rHip;
  Matrix4 _b_rKnee;
  Matrix4 _b_rAnkle;
  Matrix4 _b_rMetatarsal;
  Matrix4 _b_spine1;
  Matrix4 _b_spine2;
  Matrix4 _b_spine3;
  Matrix4 _b_spine4;
  Matrix4 _b_neck1;
  Matrix4 _b_neck2;
  Matrix4 _b_skull;
  Matrix4 _b_lEye;
  Matrix4 _b_rEye;
  Matrix4 _b_lClavicle;
  Matrix4 _b_lUpperArm;
  Matrix4 _b_lLowerArm;
  Matrix4 _b_lHand;
  Matrix4 _b_lThumbSeg1;
  Matrix4 _b_lThumbSeg2;
  Matrix4 _b_lThumbSeg3;
  Matrix4 _b_lIndexSeg1;
  Matrix4 _b_lIndexSeg2;
  Matrix4 _b_lIndexSeg3;
  Matrix4 _b_lMiddleSeg1;
  Matrix4 _b_lMiddleSeg2;
  Matrix4 _b_lMiddleSeg3;
  Matrix4 _b_lRingSeg1;
  Matrix4 _b_lRingSeg2;
  Matrix4 _b_lRingSeg3;
  Matrix4 _b_lPinkySeg1;
  Matrix4 _b_lPinkySeg2;
  Matrix4 _b_lPinkySeg3;
  Matrix4 _b_rClavicle;
  Matrix4 _b_rUpperArm;
  Matrix4 _b_rLowerArm;
  Matrix4 _b_rHand;
  Matrix4 _b_rThumbSeg1;
  Matrix4 _b_rThumbSeg2;
  Matrix4 _b_rThumbSeg3;
  Matrix4 _b_rIndexSeg1;
  Matrix4 _b_rIndexSeg2;
  Matrix4 _b_rIndexSeg3;
  Matrix4 _b_rMiddleSeg1;
  Matrix4 _b_rMiddleSeg2;
  Matrix4 _b_rMiddleSeg3;
  Matrix4 _b_rRingSeg1;
  Matrix4 _b_rRingSeg2;
  Matrix4 _b_rRingSeg3;
  Matrix4 _b_rPinkySeg1;
  Matrix4 _b_rPinkySeg2;
  Matrix4 _b_rPinkySeg3;

  Matrix4 _w_weapon;
  Matrix4 _w_launcher;
  //!}
  //!{ End bones size
  float offsetLToesEndSize;
  float offsetRToesEndSize;
  float offsetSkullEndSize;
  float offsetLThumbEndSize;
  float offsetLIndexEndSize;
  float offsetLMiddleEndSize;
  float offsetLRingEndSize;
  float offsetLPinkyEndSize;
  float offsetRThumbEndSize;
  float offsetRIndexEndSize;
  float offsetRMiddleEndSize;
  float offsetRRingEndSize;
  float offsetRPinkyEndSize;
  //!}
  //! Fill out the _b_* bones
  void DefineSkeleton(float height);

public:
  //! Constructor
  ExplicitPose() 
    : _enableOptics(false), _zoom(1.0f),
    _explicitSkeletonFactor(1.0), _rtmPelvisOffset(VZero), _oldCameraType(CamInternal), _userControlDisplayed(0), _isActive(false)
  {
    _torso = MIdentity;
    _view = MIdentity;
    _aim = MIdentity;
    _gun = MIdentity;
    _camera = MIdentity;
    _relativeChange = MIdentity;
    _gunTrans = MIdentity;
    _gunBodyTrans = MIdentity;
    _modifyBone = true;
  }

  //! Bone modifications are allowed
  bool CanModifyBone() const { return _modifyBone; }
  void AllowBoneModifications(bool allow) { _modifyBone = allow; }

  //!{ Pose control interface

  //!{ Lower body control
  //! Determine whether explicit torso, view and aim matrices are used or not
  virtual bool IsLowerBodySkeletonControlled() const { return false; }
  virtual bool IsLowerBodySkeletonFullyControlled() const { return false; }
  //! Get explicitly specified bone, return false in case the specified bone is not remotely controlled
  virtual bool GetExplicitBone(Matrix4 &mat, int si) const = NULL;

  virtual void GetSceleton() = NULL;
  //! Get current height of the pelvis (how much above ground it is located)
  virtual float GetPelvisHeight() const = NULL;
  //!}

  //!{ Skeleton control - new way
  void SetMotionControllUpper(BYTE &state, BYTE value);
  void SetMotionControllLower(BYTE &state, BYTE value);
  //!}

  //!{ Skeleton control - same as above without setting the state
  void ApplyMotionControlUpper(BYTE value);
  void ApplyMotionControlLower(BYTE value);
  void ApplyMotionControl(BYTE upperValue, BYTE lowerValue);
  //!}

  //!{ Upper body control
  //! Determine whether explicit torso, view and aim matrices are used or not
  virtual bool IsUpperBodyControlled() const {return true;}
  //! Determine whether the upper body is fully controlled
  virtual bool IsUpperBodyFullyControlled() const { return false; }
  //! Return torso matrix (leaning)
  virtual const Matrix4 &GetTorso() const {return _torso;}
  //! Return view matrix (head)
  virtual const Matrix4 &GetView() const {return _view;}
  //! Return aim matrix (weapon)
  virtual const Matrix4 &GetAim() const {return _aim;}
  //!}

  //!{ Gun control
  //! Determine whether primary gun pose is controlled or not
  virtual bool IsGunControlled() const {return false;}
  //! Determine whether secondary gun pose is controlled or not
  virtual bool IsLauncherControlled() const { return false; }
  //! Determine whether handgun pose is controlled or not
  virtual bool IsPistolControlled() const { return false; }
  //! Determine whether binoculars pose is controlled or not
  virtual bool IsBinocularsControlled() const { return false; }
  //! Callbacks
  virtual void ApplyProxyMatrix(Matrix4 &mat, Matrix4 &proxyMat, int boneIndex, bool proxy) const {};
  virtual bool AnimateProxyMatrix(Matrix4 &mat, int boneIndex){ return false; };
  virtual void ModifyBone(Matrix4& mat, int skeletonIndex){};
  virtual void ModifyGunTrans(Matrix4 &mat){};
  virtual void ModifyGunBodyTrans(Matrix4 &mat){};
  virtual void ModifyHeadTrans(Matrix4 &mat){};
  virtual void ModifyLookTrans(Matrix4 &mat){};
  virtual void ModifyRelativeChange(){};
  
  void SetUserControlDisplayed(int controlsDisplayed) {_userControlDisplayed = controlsDisplayed; }
  int GetUserControlDisplayed() {return _userControlDisplayed; }
  //! Determine whether a fire event occurred
  virtual int Fired() const { return 0; }
  //! Inform explicit pose that fire event occurred
  virtual void SetFired() {}
  //! Inform explicit pose that reload event occurred
  virtual void SetReload() {}
  //! Return gun pose
  virtual const Matrix4 &GetGun() const {return _gun;}
  //!}

  //!{ Camera control

  //! Return camera pose
  virtual const Matrix4 &GetCamera() const {return _camera;}
  //! Return whether optics should be displayed in a 1st person view
  virtual const bool EnableOptics() const { return _enableOptics; }
  //! Return camera zoom factor
  virtual const float GetZoom() const { return _zoom; }
  //!}

  virtual const Matrix4 &GetRelativeChange() const {return _relativeChange;}

  //! Function called every simulation step - inputs from external device can be read there
  virtual void Simulate() = NULL;

  //! Prepare data for the calls of functions like GetExplicitBone and GetTorso. Calculate and return the relative pose. Function is not called, if UseExplicitSkeleton returns false
  virtual Matrix4 UpdatePose() = NULL;
  //!}

  //! Read inputs from joystick
  virtual void SimulateJoystick() {};
  virtual BYTE JoystickArbButtons(int i) const { return NULL; };
  virtual LONG JoystickArlPovs(int i) const { return NULL; };
  virtual float JoystickArfAxes(int i) const { return NULL; };

  bool IsActive() { return _isActive; }
  void SetActive(bool active) { _isActive = active; }
};


//! Interface to the Pointman
class PointmanPose : public ExplicitPose
{
protected:
  //! Last pose retrieved from the Pointman, relative pose can be calculated from this and the next pose
  Matrix4 _lastPose;
  //!{ Bone2Model matrices - inverse of easy to look at Model2Bone matrices (such represent the bone, it's position and orientation in space)
  Matrix4 _b2m_lHip;
  Matrix4 _b2m_lKnee;
  Matrix4 _b2m_lAnkle;
  Matrix4 _b2m_lMetatarsal;
  Matrix4 _b2m_rHip;
  Matrix4 _b2m_rKnee;
  Matrix4 _b2m_rAnkle;
  Matrix4 _b2m_rMetatarsal;
  //!}
  //!{ Variables initialized in Simulate function
  //! Position of the pelvis, as received from the Pointman
  Vector3 _pelvisPosition;
  //! Last non zero pelvis position
  Vector3 _lastPelvisPosition;
  //!{ Modified skeleton bones
  Matrix4 _lHip;
  Matrix4 _lKnee;
  Matrix4 _lAnkle;
  Matrix4 _lMetatarsal;
  Matrix4 _rHip;
  Matrix4 _rKnee;
  Matrix4 _rAnkle;
  Matrix4 _rMetatarsal;
  //!}
  //!}
  //! Determine if view is aligned with head (camera is rotated with the roll rotation) or not
  //bool _alignViewWithHead;
public:
  //! Constructor
  PointmanPose() : ExplicitPose() {}
  //! Implementation of ExplicitPose
  virtual bool IsLowerBodySkeletonControlled() const {return true;}
  virtual bool IsLowerBodySkeletonFullyControlled() const {return true;}
  //! Implementation of ExplicitPose
  virtual bool GetExplicitBone(Matrix4 &mat, int si) const { return false; } ;

  virtual void GetSceleton() {};
  //! Implementation of ExplicitPose
  virtual float GetPelvisHeight() const;
  //! Set suspended motion state, to receive full input from the composite joystick
  virtual void SetMotionStateSuspended() = NULL;
  //! Read inputs from joystick
  virtual void SimulateJoystick() = NULL;
  //!{ Joystick inputs
  virtual BYTE JoystickArbButtons(int i) const = NULL;
  virtual LONG JoystickArlPovs(int i) const = NULL;
  virtual float JoystickArfAxes(int i) const = NULL;
  //!}
};

//! Interface to the Pointman2
class Pointman2Pose : public PointmanPose
{
  //!{ Bone2Model matrices - inverse of easy to look at Model2Bone matrices (such represent the bone, it's position and orientation in space)
  Matrix4 _b2m_spine1;
  Matrix4 _b2m_spine2;
  Matrix4 _b2m_spine3;
  Matrix4 _b2m_spine4;
  Matrix4 _b2m_neck1;
  Matrix4 _b2m_neck2;
  Matrix4 _b2m_skull;
  Matrix4 _b2m_lEye;
  Matrix4 _b2m_rEye;
  Matrix4 _b2m_lClavicle;
  Matrix4 _b2m_lUpperArm;
  Matrix4 _b2m_lLowerArm;
  Matrix4 _b2m_lHand;
  Matrix4 _b2m_lThumbSeg1;
  Matrix4 _b2m_lThumbSeg2;
  Matrix4 _b2m_lThumbSeg3;
  Matrix4 _b2m_lIndexSeg1;
  Matrix4 _b2m_lIndexSeg2;
  Matrix4 _b2m_lIndexSeg3;
  Matrix4 _b2m_lMiddleSeg1;
  Matrix4 _b2m_lMiddleSeg2;
  Matrix4 _b2m_lMiddleSeg3;
  Matrix4 _b2m_lRingSeg1;
  Matrix4 _b2m_lRingSeg2;
  Matrix4 _b2m_lRingSeg3;
  Matrix4 _b2m_lPinkySeg1;
  Matrix4 _b2m_lPinkySeg2;
  Matrix4 _b2m_lPinkySeg3;
  Matrix4 _b2m_rClavicle;
  Matrix4 _b2m_rUpperArm;
  Matrix4 _b2m_rLowerArm;
  Matrix4 _b2m_rHand;
  Matrix4 _b2m_rThumbSeg1;
  Matrix4 _b2m_rThumbSeg2;
  Matrix4 _b2m_rThumbSeg3;
  Matrix4 _b2m_rIndexSeg1;
  Matrix4 _b2m_rIndexSeg2;
  Matrix4 _b2m_rIndexSeg3;
  Matrix4 _b2m_rMiddleSeg1;
  Matrix4 _b2m_rMiddleSeg2;
  Matrix4 _b2m_rMiddleSeg3;
  Matrix4 _b2m_rRingSeg1;
  Matrix4 _b2m_rRingSeg2;
  Matrix4 _b2m_rRingSeg3;
  Matrix4 _b2m_rPinkySeg1;
  Matrix4 _b2m_rPinkySeg2;
  Matrix4 _b2m_rPinkySeg3;
  //!}

  //!{ Variables initialized in Simulate function
  //!{ Modified skeleton bones
  Matrix4 _pelvis;
  Matrix4 _spine1;
  Matrix4 _spine2;
  Matrix4 _spine3;
  Matrix4 _spine4;
  Matrix4 _neck1;
  Matrix4 _neck2;
  Matrix4 _skull;
  Matrix4 _lEye;
  Matrix4 _rEye;
  Matrix4 _lClavicle;
  Matrix4 _lUpperArm;
  Matrix4 _lLowerArm;
  Matrix4 _lHand;
  Matrix4 _lThumbSeg1;
  Matrix4 _lThumbSeg2;
  Matrix4 _lThumbSeg3;
  Matrix4 _lIndexSeg1;
  Matrix4 _lIndexSeg2;
  Matrix4 _lIndexSeg3;
  Matrix4 _lMiddleSeg1;
  Matrix4 _lMiddleSeg2;
  Matrix4 _lMiddleSeg3;
  Matrix4 _lRingSeg1;
  Matrix4 _lRingSeg2;
  Matrix4 _lRingSeg3;
  Matrix4 _lPinkySeg1;
  Matrix4 _lPinkySeg2;
  Matrix4 _lPinkySeg3;
  Matrix4 _rClavicle;
  Matrix4 _rUpperArm;
  Matrix4 _rLowerArm;
  Matrix4 _rHand;
  Matrix4 _rThumbSeg1;
  Matrix4 _rThumbSeg2;
  Matrix4 _rThumbSeg3;
  Matrix4 _rIndexSeg1;
  Matrix4 _rIndexSeg2;
  Matrix4 _rIndexSeg3;
  Matrix4 _rMiddleSeg1;
  Matrix4 _rMiddleSeg2;
  Matrix4 _rMiddleSeg3;
  Matrix4 _rRingSeg1;
  Matrix4 _rRingSeg2;
  Matrix4 _rRingSeg3;
  Matrix4 _rPinkySeg1;
  Matrix4 _rPinkySeg2;
  Matrix4 _rPinkySeg3;
  //!}
  //!}
  Matrix4 _weapon;
  Matrix4 _launcher;
  Matrix4 _righthand;
  Matrix4 _lefthand;

  //!{ Hand grips
  V2_Interface::_V2_HandGrip _primaryGripLeft;
  V2_Interface::_V2_HandGrip _primaryGripRight;
  V2_Interface::_V2_HandGrip _secondaryGripLeft;
  V2_Interface::_V2_HandGrip _secondaryGripRight;
  V2_Interface::_V2_HandGrip _pistolGripLeft;
  V2_Interface::_V2_HandGrip _pistolGripRight;
  V2_Interface::_V2_HandGrip _binocularsGripLeft;
  V2_Interface::_V2_HandGrip _binocularsGripRight;
  //!}

  //!{ Stow positions
  V2_Interface::_V2_6D _primaryStowPos;
  V2_Interface::_V2_6D _secondaryStowPos;
  //!}

  //! Skeleton definition
  V2_Interface::_V2_SkeletalDef _sd;
  //!{ Variables initialized in Simulate function
  //! Motion state (including skeleton pose) received from the server
  V2_Interface::_V2_ServerMotionDat _serverMotionDat;
  //! Motion state from previous frame
  V2_Interface::_V2_ServerMotionDat _oldServerMotionDat;
  //!}
  //! Joystick state received from the server
  V2_Interface::_V2_ServerJoyDat _serverJoyDat;
  //! Fill out hand grips (hand position and pose) associated with a held object
  void DefineHandGrips();
  //! Fill out stow positions (for primary and secondary weapons)
  void DefineStowPos();
  //!{ Lazy settings
  void SetFireState(V2_Interface::_V2_FireState &fs, bool force = false);
  void SetPrimaryWeapon(V2_Interface::_V2_AimObject &ao);
  void SetSecondaryWeapon( V2_Interface::_V2_AimObject &ao );
  void SetPistolWeapon( V2_Interface::_V2_AimObject &ao );
  void SetBinoculars( V2_Interface::_V2_AimObject &ao );
  void SetUserView(BYTE &uv);
  void SetMotionState(V2_Interface::_V2_MotionState &ms);
  //!}
  //! Update Pointman's aim object (primary weapon, secondary weapon, handgun, binoculars) 
  void UpdateAimObject(BYTE aimObjectType);
  //! Was weapon fired in this simulation step (Pointman generated)?
  bool _isFired;
  //! Was weapon reloaded in this simulation step?
  bool _isReloaded;

  //! Recoil
  float _impulseAngX;
  float _impulseAngY;
  float _impulseZ;

  char _lastWeaponName[256];
public:
  //!{ Mobility state, zoom, health, fatigue, client last states
  V2_Interface::_V2_FireState _lastFS;
  bool _lastFSforced;
  V2_Interface::_V2_AimObject _lastAO, _lastAOS, _lastAOP, _lastAOB;
  BYTE _lastUV;
  V2_Interface::_V2_MotionState _lastMS;
  V2_Interface::_V2_ClientStateDat _lastCSD;
  //!}
  //!{ Fire state attributes
  //!}
  //! Constructor
  Pointman2Pose();
  //! Destructor
  ~Pointman2Pose();
  //!{ Implementation of ExplicitPose
  virtual bool GetExplicitBone(Matrix4 &mat, int si) const;
  virtual void GetSceleton();
  virtual bool IsLowerBodySkeletonControlled() const;
  virtual bool IsLowerBodySkeletonFullyControlled() const;
  virtual bool IsUpperBodyControlled() const;
  virtual bool IsUpperBodyFullyControlled() const;
  virtual void Simulate();
  virtual Matrix4 UpdatePose();
  //!}
  //! Gun control
  virtual bool IsGunControlled() const;
  virtual bool IsLauncherControlled() const;
  virtual bool IsPistolControlled() const;
  virtual bool IsBinocularsControlled() const;

  virtual void ApplyProxyMatrix(Matrix4 &mat, Matrix4 &proxyMat, int boneIndex, bool proxy) const;
  virtual bool AnimateProxyMatrix(Matrix4 &mat, int boneIndex);
  virtual void ModifyBone(Matrix4& mat, int skeletonIndex);
  virtual void ModifyGunTrans(Matrix4 &mat);
  virtual void ModifyGunBodyTrans(Matrix4 &mat);
  virtual void ModifyHeadTrans(Matrix4 &mat);
  virtual void ModifyLookTrans(Matrix4 &mat);
  virtual void ModifyRelativeChange();
  virtual int Fired() const;
  virtual void SetFired() {_isFired = true;}
  virtual void SetReload() {_isReloaded = true;}
  //!{ Implementation of PointmanPose
  virtual void SetMotionStateSuspended();
  virtual void SimulateJoystick();
  virtual BYTE JoystickArbButtons(int i) const;
  virtual LONG JoystickArlPovs(int i) const;
  virtual float JoystickArfAxes(int i) const;
  //!}
  void GetStowPosition( Matrix4 &mat, bool isPrimary ) const;
};


extern Pointman2Pose *GExplicitPose;



#endif