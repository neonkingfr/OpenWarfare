// EngagementAPP.cpp : Defines the entry point for the DLL application.
//

#include "EngagementApp.h"
#include "Engagement.h"

#include <VBSFusion.h>

#define _BISIM_DEV_STATS_ENGAGEMENT 1

#include <util/MissionUtilities.h>
#include <util/UnitUtilities.h>
#include <data/Unit.h>
#include <util/ControllableObjectUtilities.h>
#include <data/ControllableObject.h>
#include <util/GeneralUtilities.h>
#include <sstream>


HANDLE threadPanel = NULL;
static CRITICAL_SECTION cs;

std::vector<int> engagements;
std::pair<double,double> times;
std::pair<int,int> engagIDunitID;
std::vector<MUnit> infoUnits;
std::vector<VBSFusion::Unit> Units;

EngagementGUI * mFrame = NULL;
VBSFusion::ControllableObject objArrow;
bool createdOpforArrow = false,  createdBluforArrow = false, createdCivilArrow = false, visited = true;
int selectPrevEng = -1;


// Get all selected values of a engagement and print it
void GetDataEngagement(int Id)
{

  std::vector<MUnit>::iterator it;
  std::vector<VBSFusion::Unit>::iterator iter;
  std::vector<std::vector<std::string>> unitsName;
  std::vector<std::string> itStore;
  std::string sideWest = "WEST", sideEast = "EAST";

  
  if (!infoUnits.empty())
    for (it = infoUnits.begin(); it != infoUnits.end(); it++)
      if ((*it).id == Id)
      {
        infoUnits.erase(it);
        break;
      }

  // Get units from engagement 
  Units = VBSFusion::MissionUtilities::getEngagementUnits(Id);
  for (iter = Units.begin(); iter != Units.end(); iter++)
  {
    VBSFusion::UnitUtilities::updateDynamicProperties((*iter));
    VBSFusion::UnitUtilities::updateStaticProperties((*iter));
    VBSFusion::UnitUtilities::updateUnitPos((*iter));
    std::ostringstream str;
    itStore.push_back((*iter).getName());
    itStore.push_back((*iter).getSideString());
    itStore.push_back((*iter).getRankString());
    str << (*iter).getExperience()*100; itStore.push_back(str.str()); str.str("");
    str << (*iter).getEndurance()*100; itStore.push_back(str.str()); str.str("");
    itStore.push_back((*iter).getGroup());
    str << (*iter).getDamage()*100; itStore.push_back(str.str()); str.str("");
    unitsName.push_back(itStore);
    itStore.clear();
  }

  // Get stats form stats
  std::vector<float> stats = VBSFusion::MissionUtilities::getEngagementStat(Id, sideWest);
  std::vector<float> statsOpfor = VBSFusion::MissionUtilities::getEngagementStat(Id, sideEast);


  for (std::vector<float>::iterator iterStat = statsOpfor.begin(); iterStat != statsOpfor.end(); iterStat++)
    stats.push_back(*iterStat);


  struct MUnit val = {Id,unitsName,stats};
  infoUnits.push_back(val);

  mFrame->Fill(Id);
}

// Select one engagement ID
void EngagementChoice(int Id)
{
  visited = false;
  engagIDunitID.first = Id;
}

// Return timeouts values
void GetTimeouts(double * timesPar)
{
  timesPar[0] = times.first;
  timesPar[1] = times.second;

}

// Function return vector with all values for units that take part into an engagement
std::vector<std::vector<std::string>> GetInfoUnits(int id)
{
  std::vector<MUnit>::iterator it;
  std::vector<std::vector<std::string>> vec;

  for (it = infoUnits.begin(); it != infoUnits.end(); it++)
    if ((*it).id == id)
    {
      vec = (*it).unitsName;
      break;
    }

  return vec;
}

// Function returns all values for stats of engagement
std::vector<float> GetInfoStats(int id)
{
  std::vector<MUnit>::iterator it;
  std::vector<float> vec;

  for (it = infoUnits.begin(); it != infoUnits.end(); it++)
    if ((*it).id == id)
    {
      vec = (*it).stats;
      break;
    }

  return vec;
}

void UnitPos(long id)
{
  engagIDunitID.second = id;
  //selectPrevEng = engagIDunitID.first;
}


//Each new engagement refreshes the engagement choice list
void RefreshListEngagement()
{
  std::vector<int>::iterator it;

  mFrame->ClearEngagement();

  for (it = engagements.begin(); it != engagements.end(); it++)
    mFrame->AddEngagement((*it));
}

// Check if exist new engagement to add to the list
bool IsNewEngagement ()
{
  std::vector<int> engagementsTem = VBSFusion::MissionUtilities::getEngagements();
  
  if(engagementsTem.empty())
    return false;

  if (engagements.size() ==  engagementsTem.size())
    return false;

  engagements.clear();
  engagements = engagementsTem;
  

  return true;

}

// Get value for main Frame
void SetMain(EngagementGUI *frame)
{
  mFrame = frame;
}

// Call for start GUI during thread
DWORD WINAPI ThreadProc(LPVOID lpParameter)
{
  RunApp();
  threadPanel = NULL;
  mFrame = NULL;
  return true;
}

// Entry point funtion for the plugin.
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  times =  VBSFusion::MissionUtilities::getEngagementTimeouts();
  engagIDunitID.first = -1;
  engagIDunitID.second = -1;

  if (threadPanel == NULL && strcmp(input, "i") == 0)
  {
    threadPanel = CreateThread(NULL, 0, ThreadProc, NULL, 0, NULL);
  }
  return "";
}


// Function executed each frame and it manage position for arrows
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  // When thread did not create or if there is new engagement selected
  if ((mFrame == NULL) && (threadPanel == NULL) || (engagIDunitID.first != selectPrevEng && engagIDunitID.first >= 0 && selectPrevEng >= 0))
  {
    VBSFusion::ControllableObjectUtilities::deleteObject(objArrow);
    createdBluforArrow = createdOpforArrow = createdCivilArrow = false;
    selectPrevEng = engagIDunitID.first;
    return;
  }

  EnterCriticalSection(&cs);
  if (IsNewEngagement())
    RefreshListEngagement();

  if (!visited)
  {
    GetDataEngagement(engagIDunitID.first);
    visited = true;
  }

  //If click on some unit, print an arrow for that unit
  if (engagIDunitID.second >= 0)
  {
    VBSFusion::SIDE side;
    VBSFusion::position3D pos;

    VBSFusion::ControllableObjectUtilities::updatePosition ((Units.at(engagIDunitID.second)));
    pos = (Units.at(engagIDunitID.second)).getPosition();
    pos.setY(pos.getY() + 3);

    side = Units.at(engagIDunitID.second).getSide();
    switch(side)
    {
      case (VBSFusion::SIDE::WEST):
        if (!createdBluforArrow)
        {
          VBSFusion::ControllableObjectUtilities::deleteObject(objArrow);
          VBSFusion::ControllableObjectUtilities::createObject(objArrow, "vbs2_visual_arrow_blue", pos);
          createdBluforArrow = true;
          createdCivilArrow = createdOpforArrow = false;
        }
        break;

      case (VBSFusion::SIDE::EAST):
        if (!createdOpforArrow)
        {
          VBSFusion::ControllableObjectUtilities::deleteObject(objArrow);
          VBSFusion::ControllableObjectUtilities::createObject(objArrow, "vbs2_visual_arrow_red", pos);
          createdOpforArrow = true;
          createdBluforArrow = createdCivilArrow = false;
        }
        break;

      case (VBSFusion::SIDE::CIVILIAN):
        if (!createdCivilArrow)
        {
          VBSFusion::ControllableObjectUtilities::deleteObject(objArrow);
          VBSFusion::ControllableObjectUtilities::createObject(objArrow, "vbs2_visual_arrow_green", pos);
          createdCivilArrow = true;
          createdBluforArrow = createdOpforArrow = false;
          break;
        }
    }

    VBSFusion::ControllableObjectUtilities::applyPosition(objArrow, pos);
  }
  LeaveCriticalSection(&cs);

}


BOOL WINAPI DllMain( HINSTANCE hDll, DWORD fdwReason, LPVOID lpReserved)

{
  switch (fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    InitializeCriticalSection(&cs);
    break;
  case DLL_PROCESS_DETACH:
    // If the UI is running then close it.
    if (threadPanel != NULL)
      TerminateThread(threadPanel, 0);
    threadPanel = NULL;
      DeleteCriticalSection(&cs);
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }

    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

