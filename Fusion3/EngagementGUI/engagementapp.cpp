
#include "EngagementApp.h"
#include "Engagement.h"


IMPLEMENT_APP_GET_ONLY(EngagementApp)

EngagementApp::EngagementApp()
:MainFrame(NULL)
{

  for (int i = 0; i < TIMEOUTS; i++)
    Timeout[i] = 0;

};

EngagementApp::~EngagementApp()
{
};

static wxAppConsole *wxCreateApp()
{
  wxAppConsole::CheckBuildOptions(WX_BUILD_OPTIONS_SIGNATURE,"Engagement panel");
  return new EngagementApp;
}

void RunApp()
{
  wxApp::SetInitializerFunction(wxCreateApp);
  wxEntry(0,NULL);
}

bool EngagementApp::OnInit ()
{
  MainFrame = new EngagementGUI();
  MainFrame->Show(true);

  SetMain(MainFrame);
  
  return true;
}


// Add id of one Engagement to the choice list
void EngagementApp::AddEngagement(const int Id)
{
  MainFrame->AddEngagement(Id);
}

// Add to list of unit values
void EngagementApp::PrintPanel(int id)
{

  std::vector<std::vector<std::string>>::iterator iterUnits;
  std::vector<float>::iterator iterStats;
  long index = 0, indexBlufor = 0, indexOpfor = 0;

  // Displaying all values of units
  std::vector<std::vector<std::string>> units = GetInfoUnits(id);
  for(iterUnits = units.begin(); iterUnits != units.end(); iterUnits++)
  {
    std::vector<std::string> str = (*iterUnits);
    MainFrame->AddUnitName(index, str[NAME]);
    for (int column = SIDE; column < NUMFEATURE; column++)
    {
      MainFrame->AddUnitF(index, column, str[column]);
      if (column == DAMAGE)
        index++;
    }
  }

  // Displaying all values of stats engagement
  std::vector<float> stat = GetInfoStats(id);
  for ( iterStats = stat.begin(); iterStats != stat.end(); iterStats++)
  {
    if ( indexBlufor < NUMFEATURE)
      MainFrame->AddStats(indexBlufor++, (*iterStats), BLUFOR);
    else
      MainFrame->AddStats(indexOpfor++, (*iterStats), OPFOR);
  }
}

// Display Timeouts in GUI panel
double EngagementApp::PrintTimeouts(const char* string)
{
  GetTimeouts(Timeout);

  if(strcmp(string, "Interaction") == 0)
    return Timeout[0];

  return Timeout[1];

}


// Sending selected engagement ID to get all data.
void EngagementApp::ActiveEngagement(int id)
{
  EngagementChoice(id);
}

// Sending selected unit ID to get location of a unit.
void EngagementApp::UnitPosition(long id)
{
  UnitPos(id);
}
