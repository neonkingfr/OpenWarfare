
#include <windows.h>
#include <vector>
#include <string>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

struct MUnit{
  int id;
  std::vector<std::vector<std::string>> unitsName;
  std::vector<float> stats;
};

//global functions.
std::vector<std::vector<std::string>> GetInfoUnits(int id);
std::vector<float> GetInfoStats(int id);
void GetDataEngagement(int Id);
void EngagementChoice(int Id);
void GetTimeouts(double * times);
void SetMain(EngagementGUI *frame);
bool IsNewEngagement ();
void RefreshListEngagement();
void UnitPos(long id);
