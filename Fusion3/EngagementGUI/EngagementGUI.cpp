
#include "EngagementGUI.h"
#include "EngagementApp.h"
#include "Engagement.h"

#include <wx/splitter.h>
#include <wx/listctrl.h>
#include <wx/statline.h>

BEGIN_EVENT_TABLE(EngagementGUI, wxFrame)
EVT_CLOSE(EngagementGUI::OnClose)
EVT_LIST_ITEM_ACTIVATED(ID_LISTCTRL_UNIT, EngagementGUI::OnSelectUnit)
EVT_CHOICE(ID_CHOICE_ENGAGEMENT, EngagementGUI::OnSelectEngagement)
END_EVENT_TABLE()

EngagementGUI::EngagementGUI()
:wxFrame(NULL, wxID_ANY, "Engagement panel", wxDefaultPosition, wxSize(1200,800))
{
  wxPanel * mainLayout = new wxPanel (this, wxID_ANY);
  wxBoxSizer * mainSize = new wxBoxSizer (wxVERTICAL);
  {
    wxBoxSizer * upSize = new wxBoxSizer(wxHORIZONTAL);
    {
      // Create list of engagement
      wxBoxSizer *listVert = new wxBoxSizer(wxVERTICAL);
      {
        wxStaticText *labelListEng = new wxStaticText(mainLayout, wxID_ANY, "List of Engagement");
        wxFont *letter = new wxFont(13, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
        labelListEng->SetFont(*letter);
        listVert ->Add(labelListEng, 0, wxALL, 5);

        choiceEngagement = new wxChoice(mainLayout, ID_CHOICE_ENGAGEMENT);
        listVert ->Add(choiceEngagement, 0,  wxALL, 5);

        upSize ->Add(listVert, 0, wxTOP, 5);
      }

      // Timeouts
      wxBoxSizer * timeouts = new wxBoxSizer(wxVERTICAL);
      {
        wxStaticText *labelTimeout = new wxStaticText(mainLayout, wxID_ANY, "Timeouts", wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
        wxFont *letter = new wxFont(13, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
        labelTimeout->SetFont(*letter);
        timeouts ->Add(labelTimeout, 0, wxALL, 5);
        
        wxBoxSizer * ptrnum = new wxBoxSizer(wxHORIZONTAL);
        {
          wxStaticText *labelInter = new wxStaticText(mainLayout, wxID_ANY, "Interaction");
          ptrnum ->Add(labelInter, 0, wxALL, 5);

          wxStaticText *numberInter = new wxStaticText(mainLayout, wxID_ANY, printTim("Interaction"));
          ptrnum ->Add(numberInter, 0, wxALL, 5);

          timeouts ->Add(ptrnum, 0, wxALL, 5);
        }
        wxBoxSizer * ptrnum1 = new wxBoxSizer(wxHORIZONTAL);
        {
          wxStaticText *labelDefeat = new wxStaticText(mainLayout, wxID_ANY, "Defeat       ");
          ptrnum1 ->Add(labelDefeat, 0, wxALL, 5);

          wxStaticText *numberDefeat = new wxStaticText(mainLayout, wxID_ANY, printTim("Defeat"));
          ptrnum1 ->Add(numberDefeat, 0, wxALL, 5);

          timeouts ->Add(ptrnum1, 0, wxALL, 5);
        }
      }
      upSize->Add(timeouts, 0, wxTOP, 5);
    }


    //Display Sets and Units
    wxBoxSizer *reader = new wxBoxSizer(wxHORIZONTAL);
    {
      //panel left side
      wxBoxSizer * leftSize = new wxBoxSizer(wxVERTICAL);
      {
        wxStaticText *labelStat = new wxStaticText(mainLayout, wxID_ANY, "Stats", wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE);
        wxFont *letter = new wxFont(13, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
        labelStat->SetFont(*letter);
        leftSize ->Add(labelStat, 0, wxALL, 5);
        
        listStats = new wxListCtrl(mainLayout, wxID_ANY, wxDefaultPosition, wxSize(300,600), wxLC_REPORT);

        wxListItem col0;
        col0.SetId(EngagementApp::PARAMETERS);
        col0.SetText( _("Param") );
        col0.SetWidth(150);
        listStats->InsertColumn(EngagementApp::PARAMETERS, col0);
        listStats->InsertItem(0, "enemyShotsFired");
        listStats->InsertItem(1, "shotsFired");
        listStats->InsertItem(2, "enemyHits");
        listStats->InsertItem(3, "enemyWounded");
        listStats->InsertItem(4, "friendHits");
        listStats->InsertItem(5, "friendWounded");
        listStats->InsertItem(6, "detectedEnemyCount");

        wxListItem col1;
        col1.SetId(EngagementApp::BLUFOR);
        col1.SetText( _("Values BLUFOR") );
        col1.SetWidth(100);
        listStats->InsertColumn(EngagementApp::BLUFOR, col1);
        
        wxListItem col2;
        col2.SetId(EngagementApp::OPFOR);
        col2.SetText( _("Values OPFOR") );
        col2.SetWidth(100);
        listStats->InsertColumn(EngagementApp::OPFOR, col2);
        leftSize->Add(listStats, 1, wxEXPAND | wxALL, 5);
        reader->Add(leftSize, 1, wxEXPAND | wxALL, 5);
      }  
      
      wxBoxSizer * rightSize = new wxBoxSizer(wxVERTICAL);
      {
        
        wxStaticText *labelUnit = new wxStaticText(mainLayout, wxID_ANY, "Units", wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE);
        wxFont *letter = new wxFont(13, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
        labelUnit->SetFont(*letter);
        rightSize ->Add(labelUnit, 0, wxALL, 5);

        listUnits = new wxListCtrl(mainLayout, ID_LISTCTRL_UNIT, wxDefaultPosition, wxSize(900,600), wxLC_REPORT);

        wxListItem col0; col0.SetId(EngagementApp::NAME); col0.SetText( _("Name") ); col0.SetWidth(50);
        listUnits->InsertColumn(EngagementApp::NAME, col0);

        wxListItem col1; col1.SetId(EngagementApp::SIDE); col1.SetText( _("Side") ); col1.SetWidth(70);
        listUnits->InsertColumn(EngagementApp::SIDE, col1);

        wxListItem col2; col2.SetId(EngagementApp::RANK); col2.SetText( _("Rank") ); col2.SetWidth(80);
        listUnits->InsertColumn(EngagementApp::RANK, col2);

        wxListItem col3; col3.SetId(EngagementApp::EXPERIENCE); col3.SetText( _("Experience") ); col3.SetWidth(80);
        listUnits->InsertColumn(EngagementApp::EXPERIENCE, col3);

        wxListItem col4; col4.SetId(EngagementApp::ENDURANCE); col4.SetText( _("Endurance") ); col4.SetWidth(80);
        listUnits->InsertColumn(EngagementApp::ENDURANCE, col4);

        wxListItem col5; col5.SetId(EngagementApp::GROUP); col5.SetText( _("Group") ); col5.SetWidth(70);
        listUnits->InsertColumn(EngagementApp::GROUP, col5);

        wxListItem col6; col6.SetId(EngagementApp::DAMAGE); col6.SetText( _("Damage") ); col6.SetWidth(80);
        listUnits->InsertColumn(EngagementApp::DAMAGE, col6);

        rightSize->Add(listUnits, 1,wxEXPAND | wxALL, 5);
        reader->Add(rightSize, 1, wxEXPAND | wxALL, 5);

      }
    }
    mainSize->Add(upSize, 0, 0, 0);
    mainSize->Add(reader, 1, wxEXPAND, 0);
      
    mainLayout->SetSizer(mainSize);
  }


  refreshTimer = new wxTimer(this,TIMER_ID);
  refreshTimer->Start(30); 
}

// Removes all panel and all the data
void EngagementGUI::OnClose(wxCloseEvent& e)
{
  Destroy();
}

// Print on the panel Timeout values.s
wxString EngagementGUI::printTim(const char * str)
{
  double num = ::wxGetApp().PrintTimeouts(str);
  wxString wxstr;
  wxstr << num;

  return wxstr;
}

// Send select engagement.
void EngagementGUI::OnSelectEngagement (wxCommandEvent& e)
{
  long val;
  wxString str = GetActiveEngagement();
  if (str.ToLong(&val))
  {
    ::wxGetApp().ActiveEngagement((int)val);
  }
  
}

// Send select unit
void EngagementGUI::OnSelectUnit(wxListEvent& e)
{
  wxListItem list_item;
  long index = e.m_itemIndex;

  ::wxGetApp().UnitPosition(index);

}

// Print values for engagement.
void EngagementGUI::Fill(int id)
{
  listUnits->DeleteAllItems();
  ::wxGetApp().PrintPanel(id);
}
