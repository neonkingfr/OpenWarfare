#pragma once

#ifndef EngagementApp_h__
#define EngagementApp_h__

#include <vector>
#include <string>

#include "EngagementGUI.h"


#define TIMEOUTS 2

#define IMPLEMENT_APP_GET_ONLY(appname)                                             \
  appname& wxGetApp() { return *wx_static_cast(appname*, wxApp::GetInstance()); } \

#define TIMER_ID 123

class EngagementApp: public wxApp
{
private:
  EngagementGUI * MainFrame;
  double Timeout [TIMEOUTS];

public:
  EngagementApp(void);
  ~EngagementApp(void);
  enum ColumUnits { NAME, SIDE, RANK, EXPERIENCE, ENDURANCE, GROUP, DAMAGE, NUMFEATURE};
  enum columStats { PARAMETERS, BLUFOR, OPFOR };

  void ActiveEngagement(int ID);
  void AddEngagement(const int ID);
  void UnitPosition(long id);
  void PrintPanel(int id);

  double PrintTimeouts(const char* str);
  virtual bool OnInit();

  std::vector<std::string>GetStat();
  void SetStat(std::vector<std::string> Stat);

};
static wxAppConsole *wxCreateApp();
void RunApp();

DECLARE_APP(EngagementApp)

#endif