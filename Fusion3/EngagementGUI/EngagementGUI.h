
#pragma once

#ifndef EngagementGUI_h__
#define EngagementGUI_h__


#include <wx/wx.h>
#include <wx/listctrl.h>

#define ID_CHOICE_ENGAGEMENT 100
#define ID_LISTCTRL_UNIT 101

class EngagementGUI : public wxFrame
{
public:
    
    EngagementGUI();

    void AddEngagement(int id) { 
      wxString engagement;
      engagement << id;
      choiceEngagement->AppendString(engagement); 
    }
    wxString GetActiveEngagement() { return choiceEngagement->GetStringSelection(); }
    void ClearEngagement() { choiceEngagement->Clear(); }
    
    //Funtions to call wxFrame and print values
    void AddUnitName(long index, const wxString& unit) { listUnits->InsertItem(index, unit); }
    void AddUnitF(long index, int column, const wxString& unit) { listUnits->SetItem(index, column, unit); }
    void AddStats(long index, float stat, long id)
    {
      wxString statVal;
      statVal << stat;
      listStats->SetItem(index, id, statVal);
    }
    void Fill(int id);


private:

  void OnClose(wxCloseEvent& e);
  void OnSelectEngagement(wxCommandEvent& e);
  void OnSelectUnit(wxListEvent& e);
  wxString printTim (const char* str);

  wxTimer* refreshTimer;
  wxChoice * choiceEngagement;
  wxListCtrl * listUnits;
  wxListCtrl * listStats;

  DECLARE_EVENT_TABLE();
};


// TODO: reference additional headers your program requires here
#endif