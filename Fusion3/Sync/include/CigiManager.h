/*
*
* Copyright(c) 2013 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/
#ifndef _CIGI_MANAGER_H_
#define _CIGI_MANAGER_H_

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

using boost::asio::ip::udp;
using namespace boost::asio::ip::multicast;
using boost::asio::ip::address_v4;
using boost::asio::buffer;
using boost::asio::mutable_buffers_1;

#include "types/r_types.h"
#include "ConfigFile.h"
#include "time/Timer.h"

//=============================================================================

struct pluginLightDir;

//=============================================================================

struct CigiIGCtrlV3_3
{
  u8 packet_id;
  u8 packet_size;
  u8 major_version;
  s8 database_number;
  s8 minor_version:4;
  s8 extrap_interp_enable:1;
  u8 timestamp_valid:1;
  u8 ig_mode:2;
  u8 reserved_uchar;
  u16 byte_swap_magic_num;
  u32 host_frame_number;
  u32 timestamp;
  u32 last_ig_frame;
  u32 reserved_uint;
};

//=============================================================================

class CigiManager
{
public:
  static CigiManager& Get()
  {
    static CigiManager _cigi_manager;
    return _cigi_manager;
  }

  virtual ~CigiManager();

  bool InitializeCigi();
  void OnSimStep(f32 delta_t);
  void MissionStart();
  void MissionEnd();
  void Shutdown();

  void SetAperture(pluginLightDir* plugin_light_dir);
  void SetApertureOverride(f64 aperture);
  void SetApertureSyncEnabled(bool aperture_sync_enabled);

  bool GetApertureSyncEnabled() const
  {
    return _aperture_sync_enabled;
  }

private:
  CigiManager();

  void UDPThread();

  bool InitializeUDP();
  void ReceiveIncoming();
  void ShutdownUDP();

  void UpdateTimestamp(u32 timestamp);

  void ExecuteSync(std::stringstream& command, f64 timestamp);
  void ExecuteAperture(std::stringstream& command);

  void ResetSync();
  f64 ConvertMicrosecondsToSeconds(u32 microseconds);

  boost::shared_ptr<bisim::ConfigFile> _config_reader;

  boost::asio::io_service _io_service;

  boost::thread _receive_thread;

  // UDP Networking
  std::string _multicast_address;
  udp::socket _multicast_socket;

  udp::endpoint _multicast_endpoint;

  boost::recursive_mutex _mutex;

  const u32 _max_buffer_size;
  u32     _receive_buffer_size;
  byte*   _receive_buffer;
  size_t  _read_size;

  f32 _aperture;
  Timer _aperture_timer;

  bool _aperture_timer_started;

  // Settings
  u32 _listen_port;
  f64 _frequency;
  u32 _fps_limit;

  f64 _prev_timestamp;
  u32 _timestamp;
  bool _timestamp_dirty;
  bool _timer_start;

  u8 _log_type;
  u8 _log_level;

  bool _aperture_sync_enabled;
  bool _aperture_override;
  bool _aperture_dirty;
  bool _exit;

  bool _run_once;
};

//=============================================================================

#endif // _CIGI_MANAGER_H_
