/*
*
* Copyright(c) 2013 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/
#ifndef _BISIM_SYNC_H_
#define _BISIM_SYNC_H_

#include "VBSNG_RenderingInterface.h"

#include "types/r_types.h"

//=============================================================================

// Functions exported by this plug-in
VBSPLUGIN_EXPORT int  WINAPI  OnLoad(VBSParameters* params);
VBSPLUGIN_EXPORT int  WINAPI  OnLoadEx(VBSParametersEx* params_ex);
VBSPLUGIN_EXPORT void WINAPI  OnMissionStart();
VBSPLUGIN_EXPORT void WINAPI  OnMissionEnd();
VBSPLUGIN_EXPORT void WINAPI  OnUnload();

VBSPLUGIN_EXPORT void WINAPI  OnSimulationStep(float delta_t);
VBSPLUGIN_EXPORT void WINAPI  RegisterCommandFnc(void *execute_command_func);

VBSPLUGIN_EXPORT const char*  WINAPI  PluginFunction(const char* input);

// Rendering Interface
VBSPLUGIN_EXPORT void WINAPI  OnNgInitDraw(paramInitDraw *param, DWORD paramSize);

//=============================================================================


//=============================================================================

#endif // _BISIM_SYNC_H_
