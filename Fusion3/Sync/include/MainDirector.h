/*
*
* Copyright(c) 2013 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/
#ifndef _MAIN_DIRECTOR_H_
#define _MAIN_DIRECTOR_H_

#include <string>

#include "types/r_types.h"

//=============================================================================

struct pluginLightDir;

//=============================================================================

namespace bisim
{

/**
 @brief   Single point for all Plugin Entry Points.
 */
class MainDirector
{
public:
  /**
   @brief   Gets the MainDirector Singleton.
   @return  The MainDirector Singleton.
   */
  static MainDirector& Get()
  {
    static MainDirector _main_director;
    return _main_director;
  }

  virtual ~MainDirector();

  /**
   @brief   Loads everything that is needed for this plugin.
   */
  bool Load(const char* vbs_install_dir, const char* vbs_user_dir);

  /**
   @brief   Unloads everything that is needed for this plugin.
   */
  void Unload();

  void SimulationStep(f32 delta_t);

  void MissionStart();
  void MissionEnd();

  const char* OnPluginFunction(const char* input);

  void SetPluginLightDir(pluginLightDir* plugin_light_dir);

  /**
   @brief   Gets the stored path to this instance's VBS install.
   @return  The install directory for this instance of VBS.
   */
  inline const std::string& GetVBSInstallDir() const
  {
    return _vbs_install_dir;
  }

  /**
   @brief   Gets the stored path to this user's VBS install.
   @return  The user directory for this instance of VBS.
   */
  inline const std::string& GetVBSUserDir() const
  {
    return _vbs_install_dir;
  }

private:
  MainDirector();
  MainDirector(const MainDirector& rhs) {}
  MainDirector& operator=(const MainDirector& rhs) {}

  void SetLensFlareEnabled(bool enabled);
  void SetRandomWeather(bool enabled);

  std::string _vbs_install_dir;
  std::string _vbs_user_dir;

  bool _plugin_loaded;
};

} // bisim

//=============================================================================

void ExecuteCommandAndForget(const std::string& command);
const std::string ExecuteCommandAndReturn(const std::string& command);

//=============================================================================

#endif // _MAIN_DIRECTOR_H_
