/*
*
* Copyright(c) 2013 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/
#include "Sync.h"

#include <Windows.h>

#include "MainDirector.h"
#include "logger/log.h"

//=============================================================================

bool g_stopped = false;
bool g_plugin_loaded = false;
ExecuteCommandType ExecuteCommand = NULL;

#define VALIDATEPLUGIN() if(!g_plugin_loaded) { return; }
#define VALIDATEPLUGINRETURN(x) if(!g_plugin_loaded) { return x; }

//=============================================================================

VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters* params)
{
  try
  {
    g_plugin_loaded = bisim::MainDirector::Get().Load(params->_dirInstall,
                                                      params->_dirUser);

    if(!g_plugin_loaded)
    {
      return -1;
    }
  }
  catch(std::exception& e)
  {
    bisim::Log::Error( "Exception: OnLoad: %s\n", e.what());
  }
  catch(...)
  {
    bisim::Log::Error("Exception: OnLoad: UNHANDLED EXCEPTION\n");
  }

  return 0;
}

//=============================================================================

VBSPLUGIN_EXPORT int WINAPI OnLoadEx(VBSParametersEx* params_ex)
{
  try
  {
    g_plugin_loaded = bisim::MainDirector::Get().Load(params_ex->_dirInstall,
                                                      params_ex->_dirUser);

    if(!g_plugin_loaded)
    {
      bisim::MainDirector::Get().Unload();
      return -1;
    }
  }
  catch(std::exception& e)
  {
    bisim::Log::Error( "Exception: OnLoadEx: %s\n", e.what());
  }
  catch(...)
  {
    bisim::Log::Error("Exception: OnLoadEx: UNHANDLED EXCEPTION\n");
  }

  return 0;
}

//=============================================================================

VBSPLUGIN_EXPORT void WINAPI  OnMissionStart()
{
  VALIDATEPLUGIN()

  try
  {

    bisim::MainDirector::Get().MissionStart();
  }
  catch(std::exception& e)
  {
    bisim::Log::Error( "Exception: OnMissionStart: %s\n", e.what());
  }
  catch(...)
  {
    bisim::Log::Error("Exception: OnMissionStart: UNHANDLED EXCEPTION\n");
  }
}

//=============================================================================

VBSPLUGIN_EXPORT void WINAPI  OnMissionEnd()
{
  VALIDATEPLUGIN()

  try
  {

    bisim::MainDirector::Get().MissionEnd();
  }
  catch(std::exception& e)
  {
    bisim::Log::Error( "Exception: OnMissionEnd: %s\n", e.what());
  }
  catch(...)
  {
    bisim::Log::Error("Exception: OnMissionEnd: UNHANDLED EXCEPTION\n");
  }
}

//=============================================================================

VBSPLUGIN_EXPORT void WINAPI OnUnload()
{
  VALIDATEPLUGIN()

  try
  {
    bisim::MainDirector::Get().Unload();
  }
  catch(std::exception& e)
  {
    bisim::Log::Error( "Exception: OnUnload: %s\n", e.what());
  }
  catch(...)
  {
    bisim::Log::Error("Exception: OnUnload: UNHANDLED EXCEPTION\n");
  }
}

//=============================================================================

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float delta_t)
{
  VALIDATEPLUGIN()

  try
  {
    bisim::MainDirector::Get().SimulationStep(delta_t);
  }
  catch(std::exception& e)
  {
    bisim::Log::Error( "Exception: OnSimulationStep: %s\n", e.what());
  }
  catch(...)
  {
    bisim::Log::Error("Exception: OnSimulationStep: UNHANDLED EXCEPTION\n");
  }
}

//=============================================================================

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *execute_command_func)
{
  // Could Possibly happen before OnLoad No Sanity check (No VALIDATEPLUGIN)
	ExecuteCommand = (ExecuteCommandType)execute_command_func;
};

//=============================================================================

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char* input)
{
  VALIDATEPLUGINRETURN("")

  return bisim::MainDirector::Get().OnPluginFunction(input);
}

//=============================================================================

VBSPLUGIN_EXPORT void WINAPI  OnNgInitDraw(paramInitDraw *param, DWORD paramSize)
{
  VALIDATEPLUGIN()

  // Get aperture stuff
  bisim::MainDirector::Get().SetPluginLightDir(&param->mainLight);
}

//=============================================================================

/*!
Do not modify this code. Defines the DLL main code fragment
*/

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    {} break;
  case DLL_PROCESS_DETACH:
    {} break;
  case DLL_THREAD_ATTACH:
    {} break;
  case DLL_THREAD_DETACH:
    {} break;
  }

  return TRUE;
};

//=============================================================================
