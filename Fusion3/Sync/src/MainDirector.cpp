/*
*
* Copyright(c) 2013 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/
#include "MainDirector.h"

#pragma warning( push )
#pragma warning(disable:4005)
#include "VBSFusion\VBS2Fusion.h"
#include "VBSFusion\util\ExecutionUtilities.h"

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "logger/log.h"
#include "vbs-support/Sqf.h"

#include "CigiManager.h"
#include "Sync.h"
#pragma warning( pop )

//=============================================================================

namespace bisim
{

// This will be the size of the buffer used for
// ExecuteCommandAndReturn
#define BUFFER_SIZE R_KILOBYTE(32)

//=============================================================================

MainDirector::MainDirector() : _plugin_loaded(false)
{}

//=============================================================================

MainDirector::~MainDirector()
{}

//=============================================================================

bool MainDirector::Load(const char* vbs_install_dir, const char* vbs_user_dir)
{
  // Make sure that VBS2Fusion_2010.dll is actually included
  std::string fusion_test = "";
  VBS2Fusion::ExecutionUtilities::ExecuteStringAndForget(fusion_test);

  // Initialize Code Here
  _vbs_install_dir = vbs_install_dir;
  _vbs_user_dir = vbs_user_dir;

  _plugin_loaded = CigiManager::Get().InitializeCigi();

  // Turn off the lens flare
  SetLensFlareEnabled(false);

  // Turn off random weather
  SetRandomWeather(false);

  return _plugin_loaded;
}

//=============================================================================

void MainDirector::Unload()
{
  // Shutdown Code Here

  CigiManager::Get().Shutdown();

  _plugin_loaded = false;
}

//=============================================================================

void MainDirector::SimulationStep(f32 delta_t)
{
  if(_plugin_loaded)
  {
    // Update Code Here
    CigiManager::Get().OnSimStep(delta_t);
  }
  else
  {

  }
}

//=============================================================================

void MainDirector::MissionStart()
{
  CigiManager::Get().MissionStart();
}

//=============================================================================

void MainDirector::MissionEnd()
{
  CigiManager::Get().MissionEnd();
}

//=============================================================================

const char* MainDirector::OnPluginFunction(const char* input)
{
  // Called with the format fusionFunction["pluginNam",'commandName "data_name", 1.0, true']
    static std::string result_str("");

    // Clear prior results
    result_str = "";

    //Use regular expression to parse input into command and action
    //in the form of commandName [0.1, 1.0, "someData"]
    //std::regex expression("(\\w+)\\s*(\\[.*\\])?");
    boost::regex reg_ex("(\\w+|\"\\w+\"|[(](-?\\d*\\.?\\d*,?)*[)]|\"\")");
    boost::cmatch matches;

    std::string command;
    std::string action;
    if(boost::regex_search(input, matches, reg_ex, boost::match_extra))
    {
      if(matches.size() >= 2)
      {
        command.assign(matches[1].first, matches[1].second);
        action.assign(matches[1].second, matches[2].first);
      }
      if(boost::istarts_with(command, "sync_"))
      {
        Sqf::Value data;

        try
        {
          data = boost::lexical_cast<Sqf::Value>(action);
        }
        catch(std::exception&)
        {
          bisim::Log::Error("Command Malformed\n%s %s\n", command.c_str(), action.c_str());
        }

        if(boost::iends_with(command, "setApertureSyncEnabled"))
        {
          Sqf::Parameters& data_array = boost::get<Sqf::Parameters>(data);

          if(data_array.size() == 1)
          {
            bool aperture_sync_enabled = Sqf::GetBoolAny(data_array[0]);

            CigiManager::Get().SetApertureSyncEnabled(aperture_sync_enabled);
          }
        }
        else if(boost::iends_with(command, "getApertureSyncEnabled"))
        {
          if(CigiManager::Get().GetApertureSyncEnabled())
          {
            result_str = "[true]";
          }
          else
          {
            result_str = "[false]";
          }
        }
        else if(boost::iends_with(command, "setAperture"))
        {
          Sqf::Parameters& data_array = boost::get<Sqf::Parameters>(data);

          if(data_array.size() == 1)
          {
            f64 aperture = Sqf::GetDouble(data_array[0]);

            CigiManager::Get().SetApertureOverride(aperture);
          }
        }
      }
    }

    return result_str.c_str();
}

//=============================================================================

void MainDirector::SetPluginLightDir(pluginLightDir* plugin_light_dir)
{
  CigiManager::Get().SetAperture(plugin_light_dir);
}

//=============================================================================

void MainDirector::SetLensFlareEnabled(bool enabled)
{
  std::stringstream command;

  command << "setLensFlareEnabled "
          << (enabled?"true":"false") << ';';

  ExecuteCommandAndForget(command.str());
}

//=============================================================================

void MainDirector::SetRandomWeather(bool enabled)
{
  std::stringstream command;

  command << "setRandomWeather "
          << (enabled?"true":"false") << ';';

  ExecuteCommandAndForget(command.str());
}

//=============================================================================

} // bisim

//=============================================================================

void ExecuteCommandAndForget(const std::string& command)
{
  ExecuteCommand(command.c_str(), NULL, 0);
}

//=============================================================================

const std::string ExecuteCommandAndReturn(const std::string& command)
{
  static char result_buffer[BUFFER_SIZE] = {};
  ZeroMemory(result_buffer, BUFFER_SIZE);

  // (BUFFER_SIZE - 1) for null terminator
  ExecuteCommand(command.c_str(), result_buffer, BUFFER_SIZE-1);

  return result_buffer;
}

//=============================================================================
