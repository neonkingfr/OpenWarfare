/*
*
* Copyright(c) 2013 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/
#include "CigiManager.h"

#include <boost/make_shared.hpp>

#include "logger/log.h"
#include "math/float3.h"
#include "math/MathFunc.h"

#include "Sync.h"
#include "MainDirector.h"

//=============================================================================

const u8 kIGPacketID = 1;
const u8 kIGPacketSize = 24;

const u8 kIGReset = 0;
const u8 kIGOperate = 1;
const u8 kIGDebug = 2;

//=============================================================================

CigiManager::CigiManager() :
  _multicast_socket(_io_service, udp::v4()),
  _max_buffer_size(16384),
  _aperture(0.0f),
  _aperture_timer_started(false),
  _prev_timestamp(0.0),
  _timestamp(0),
  _timestamp_dirty(false),
  _timer_start(false),
  _aperture_sync_enabled(true),
  _aperture_override(false),
  _aperture_dirty(false),
  _exit(false),
  _run_once(true)
{
  std::string install_dir = bisim::MainDirector::Get().GetVBSInstallDir();
  std::string file_directory = ".\\pluginsFusion\\sync\\";
  std::string file_path = file_directory + "settings.xml";
  _config_reader = boost::make_shared<bisim::ConfigFile>( file_path,
                                                          "Sync_Config",
                                                          true, true);

  _log_type = (u8)_config_reader->GetInteger("Log_Enabled", 0);
  _log_level = (u8)_config_reader->GetInteger("Log_Level", 7);
  _multicast_address = _config_reader->GetString("Multicast_Address", "225.0.0.1");
  _receive_buffer_size = _config_reader->GetInteger("Receive_Buffer_Size", 16384);
  _listen_port = (u32)_config_reader->GetInteger("Listen_UDP_Port", 8005);
  _frequency = _config_reader->GetDouble("Update_Frequency", 1.0);
  _fps_limit = (u32)_config_reader->GetInteger("FPS_Limit", 60);

  _config_reader->Export();

  file_path = file_directory + "Sync.log";
  bisim::Log::Initialize( (bisim::Log::LogType)_log_type,
                          (bisim::Log::LogLevels)_log_level,
                          file_path.c_str());

  // Cap the buffer size
  if(_receive_buffer_size > _max_buffer_size)
  {
    _receive_buffer_size = _max_buffer_size;
    bisim::Log::Warning("Receive Buffer Size is too large, setting to max: %d\n", _max_buffer_size);
  }
}

//=============================================================================

CigiManager::~CigiManager()
{}

//=============================================================================

bool CigiManager::InitializeCigi()
{
  bool success = true;

  _receive_thread = boost::thread(&CigiManager::UDPThread, this);

  return success;
}

//=============================================================================

void CigiManager::OnSimStep(f32 delta_t)
{
  // Register Sync Commands
  if(_run_once)
  {
#ifdef NDEBUG
    std::string plugin_name = "sync";
#else
    std::string plugin_name = "sync_d";
#endif

    std::stringstream command;

    command << "sync_setApertureSyncEnabled = compile 'fusionFunction[\"" << plugin_name
      << "\", format [\"sync_setApertureSyncEnabled"
      << "[%1]\", (_this select 0) ]];';";

    command << "sync_getApertureSyncEnabled = compile 'fusionFunction[\"" << plugin_name
      << "\", format [\"sync_getApertureSyncEnabled"
      << "[]\" ]];';";

    command << "sync_setAperture = compile 'fusionFunction[\"" << plugin_name
      << "\", format [\"sync_setAperture"
      << "[%1]\", (_this select 0) ]];';";

    ExecuteCommandAndForget(command.str());

    _run_once = false;
  }

  _mutex.lock();
  bool start_timer = _timer_start;
  _mutex.unlock();

  if(start_timer)
  {
    f64 seconds = ConvertMicrosecondsToSeconds(_timestamp);
    f64 delta_time = seconds - _prev_timestamp;

    if(delta_time >= _frequency)
    {
      _mutex.lock();
        std::stringstream command;

        ExecuteSync(command, seconds);

        ExecuteCommandAndForget(command.str());
      _mutex.unlock();

      _prev_timestamp = seconds;
    }
    else if(delta_time <= -_frequency)
    {
      ResetSync();
    }
  }

  if(_aperture_timer_started)
  {
    if(math::GreaterThan(_aperture_timer.GetTime(), _frequency))
    {
      std::stringstream command;

      ExecuteAperture(command);

      ExecuteCommandAndForget(command.str());

      _aperture_timer.Reset();
      _aperture_timer.Start();
    }
  }
}

//=============================================================================

void CigiManager::MissionStart()
{
  _run_once = true;
  _aperture_timer.Start();
  _aperture_timer_started = true;
}

//=============================================================================

void CigiManager::MissionEnd()
{
  _aperture_timer.Stop();
  _aperture_timer.Reset();
  _aperture_timer_started = false;
}

//=============================================================================

void CigiManager::ExecuteSync(std::stringstream& command, f64 timestamp)
{
  if(_timestamp_dirty)
  {
    _timestamp_dirty = false;

    command << "ExternalSynchronization ["
            << timestamp
            << ", "
            << _fps_limit
            << "];";

    bisim::Log::Info( "\"ExternalSync\" Called. Time - %f : FPS - %d\n",
                                                  timestamp, _fps_limit);
  }
}

//=============================================================================

void CigiManager::ExecuteAperture(std::stringstream& command)
{
  if(_aperture_sync_enabled && _aperture_dirty)
  {
    _aperture_dirty = false;

    command << "SetAperture " << _aperture << ";";

    bisim::Log::Info("\"SetAperture\" Called. Aperture - %f\n", _aperture);
  }
}

//=============================================================================

void CigiManager::Shutdown()
{
  _mutex.lock();
    _exit = true;
    _timer_start = false;

    // Break out of the receive
    _multicast_socket.close();
  _mutex.unlock();

  _receive_thread.join();
}

//=============================================================================

void CigiManager::UDPThread()
{
  bisim::Log::Info("Sync Thread Started\n");

  if(InitializeUDP())
  {
    ReceiveIncoming();
  }
  ShutdownUDP();

  bisim::Log::Info("Sync Thread Ended\n");
}

//=============================================================================

bool CigiManager::InitializeUDP()
{
  bool success = true;

  // Create the LocalHost and multicast address boost objects
  address_v4 listen_address = address_v4::from_string("0.0.0.0");
  address_v4 multicast_address = address_v4::from_string(_multicast_address);

  udp::endpoint listen_endpoint(listen_address, _listen_port);

  // Create the socket and join the multicast group
  _multicast_socket.set_option(udp::socket::reuse_address(true));
  _multicast_socket.set_option(join_group(multicast_address));
  _multicast_socket.set_option(enable_loopback(true));
  _multicast_socket.set_option(hops(1));
  _multicast_socket.bind(listen_endpoint);

  // Set up the receive buffer
  _receive_buffer = new byte[_receive_buffer_size];

  if(_receive_buffer)
  {
    memset(_receive_buffer, 0, _receive_buffer_size);
  }
  else
  {
    success = false;
  }

  return success;
}

//=============================================================================

void CigiManager::ReceiveIncoming()
{
  auto boost_buffer = boost::asio::buffer(_receive_buffer, _receive_buffer_size);

  _mutex.lock();
  bool thread_exit = _exit;
  _mutex.unlock();

  while(!thread_exit)
  {
    try
    {
      boost::this_thread::sleep(boost::posix_time::milliseconds(1));

      _read_size = _multicast_socket.receive_from(boost_buffer, _multicast_endpoint);

      if(_read_size)
      {
        CigiIGCtrlV3_3* ig_control = (CigiIGCtrlV3_3*)_receive_buffer;

        // Confirm it's an IG Control packet
        if( kIGPacketID == ig_control->packet_id &&
            kIGPacketSize == ig_control->packet_size)
        {
          if(ig_control->timestamp_valid && ig_control->timestamp)
          {
            UpdateTimestamp(ig_control->timestamp);

            _mutex.lock();
            // Needs to run the first time
            if(_timestamp_dirty && !_timer_start)
            {
              _timer_start = true;
              _prev_timestamp = ConvertMicrosecondsToSeconds(ig_control->timestamp);
            }
            _mutex.unlock();
          }
        }

        memset(_receive_buffer, 0, _read_size);
      }
    }
    // Handle the socket closure gracefully
    catch(const boost::exception&)
    {}
    catch(const std::exception& e)
    {
      bisim::Log::Alert("%s\n", e.what());
    }

    _mutex.lock();
    thread_exit = _exit;
    _mutex.unlock();
  }
}

//=============================================================================

void CigiManager::UpdateTimestamp(u32 timestamp)
{
  _mutex.lock();

  _timestamp_dirty |= (_timestamp != timestamp);
  _timestamp = timestamp;

  _mutex.unlock();
}

//=============================================================================

void CigiManager::ShutdownUDP()
{
  if(_receive_buffer)
  {
    delete [] _receive_buffer;
    _receive_buffer = NULL;
  }
}

//=============================================================================

void CigiManager::ResetSync()
{
  _mutex.lock();

  _timestamp = 0;
  _timestamp_dirty = false;
  _timer_start = false;
  _prev_timestamp = 0.0;

  _mutex.unlock();
}

//=============================================================================

void CigiManager::SetAperture(pluginLightDir* plugin_light_dir)
{
  if(_aperture_sync_enabled && !_aperture_override)
  {
    float3 diffuse = float3(plugin_light_dir->diffuse.x,
                            plugin_light_dir->diffuse.y,
                            plugin_light_dir->diffuse.z);

    f32 diffuse_length = diffuse.Length();

    f32 old_aperture = _aperture;

    _aperture = (sqrt(diffuse_length) + 1.0f) * 0.5f;

    _aperture_dirty |= !math::Equal(old_aperture, _aperture);
  }
}

//=============================================================================

void CigiManager::SetApertureOverride(f64 aperture)
{
  _aperture = (f32)aperture;

  _aperture_override = !math::Equal(aperture, -1.0);
  _aperture_dirty = _aperture_override;
}

//=============================================================================

void CigiManager::SetApertureSyncEnabled(bool aperture_sync_enabled)
{
  _aperture_sync_enabled = aperture_sync_enabled;

  // If the aperture sync is disabled
  if(!_aperture_sync_enabled)
  {
    // set the aperture to -1.0f
    _aperture = -1.0f;
    _aperture_dirty = false;

    std::stringstream command;

    // set the engine aperture to -1.0f
    command << "setAperture -1.0;";
    ExecuteCommandAndForget(command.str());
  }

  bisim::Log::Info("Aperture Sync: %s\n", (_aperture_sync_enabled?"Enabled":"Disabled"));
}

//=============================================================================

f64 CigiManager::ConvertMicrosecondsToSeconds(u32 microseconds)
{
  return (f64)microseconds * 1e-5;
}

//=============================================================================
