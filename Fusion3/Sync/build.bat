@rem One Click Build Script
@echo off

@set EXIT_CODE=0

@set OLDDIR=%CD%
@CD ".\build"
@call configure.bat

@call make.bat

@chdir /d %OLDDIR% &rem restore current directory

@echo ======Install Sync Plugin======

@IF NOT "%~1"=="" (set OUTPUT_DIR=%~1)

@IF DEFINED OUTPUT_DIR (

@echo Output Directory %OUTPUT_DIR%

@call ROBOCOPY res\ "%OUTPUT_DIR%\pluginsFusion\sync" /MIR /njh /njs /ndl /nc /ns

@call ROBOCOPY bin\vs2010\x32\ "%OUTPUT_DIR%\pluginsFusion" "sync.dll" /njh /njs /ndl /nc /ns
@call ROBOCOPY bin\vs2010\x64\ "%OUTPUT_DIR%\pluginsFusion64" "sync.dll" /njh /njs /ndl /nc /ns

)

@echo ======Install Sync Plugin Complete======

@IF NOT DEFINED PAUSE_BUILD pause

@echo Build Sync Plugin: Exit Code is %EXIT_CODE%
exit /B %EXIT_CODE%
