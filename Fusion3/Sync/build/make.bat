@echo off



@rem Test if incredibuild is present and automatically use it 
for %%X in (BuildConsole.exe) do (set FOUND=%%~$PATH:X)
if defined FOUND echo Incredibuild found and will be used! && set DEV_ENV="BuildConsole.exe"

IF NOT DEFINED DEV_ENV set DEV_ENV="devenv.com"

IF NOT DEFINED BUILD_SWITCH set BUILD_SWITCH=/build

@echo Building Sync Plugin Libraries...

@rem call "%VS80COMNTOOLS%..\..\VC\vcvarsall.bat" x86

@rem %DEV_ENV% ".\vs2005\sync.sln" %BUILD_SWITCH% "ReleaseWithSymbols|Win32"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%
@rem %DEV_ENV% ".\vs2005\sync.sln" %BUILD_SWITCH% "Release|Win32"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%

@rem %DEV_ENV% ".\vs2005\sync.sln" %BUILD_SWITCH% "ReleaseWithSymbols|x64"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%
@rem %DEV_ENV% ".\vs2005\sync.sln" %BUILD_SWITCH% "Release|x64"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%

@rem call "%VS90COMNTOOLS%..\..\VC\vcvarsall.bat" x86

@rem %DEV_ENV% ".\vs2008\sync.sln" %BUILD_SWITCH% "ReleaseWithSymbols|Win32"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%
@rem %DEV_ENV% ".\vs2008\sync.sln" %BUILD_SWITCH% "Release|Win32"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%

@rem %DEV_ENV% ".\vs2008\sync.sln" %BUILD_SWITCH% "ReleaseWithSymbols|x64"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%
@rem %DEV_ENV% ".\vs2008\sync.sln" %BUILD_SWITCH% "Release|x64"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%

call "%VS100COMNTOOLS%..\..\VC\vcvarsall.bat" x86

@rem %DEV_ENV% ".\vs2010\sync.sln" %BUILD_SWITCH% "ReleaseWithSymbols|Win32"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%
%DEV_ENV% ".\vs2010\sync.sln" %BUILD_SWITCH% "Release|Win32"
IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%

@rem %DEV_ENV% ".\vs2010\sync.sln" %BUILD_SWITCH% "ReleaseWithSymbols|x64"
@rem IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%
%DEV_ENV% ".\vs2010\sync.sln" %BUILD_SWITCH% "Release|x64"
IF ERRORLEVEL 1 set EXIT_CODE=%ERRORLEVEL%

ECHO Exit Code is %EXIT_CODE%

@echo Sync Plugin Build Complete!

IF NOT DEFINED PAUSE_BUILD pause

@echo Build Sync: Exit Code is %EXIT_CODE%
exit /B %EXIT_CODE%