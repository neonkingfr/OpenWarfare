--
-- Copyright(c) 2013 Bohemia Interactive Simulations, Inc.
-- http://www.bisimulations.com
--
-- For information about the licensing and copyright of this software please
-- contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
--

config = {
    location = _ACTION,
    target = "../bin/" .. _ACTION .. "/",
    debug_level = 0,
}

solution "sync"
    language "c++"
    location(config.location)
    includedirs { "../include/" }
    platforms { "x32", "x64" }
    configurations {"ReleaseWithSymbols", "Release"}
    includedirs { "$(BUILDS_ROOT)/boost/1.51/",
                  "$(BUILDS_ROOT)/core/master/include/",
                  "$(BUILDS_ROOT)/vbs-support/master/include/",
                  "$(BUILDS_ROOT)/logger/master/include/",
                  "$(WindowsSdkDir)include",
                  "$(BUILDS_ROOT)/pugixml/1.2/src/",
                  "$(BUILDS_ROOT)/file-parsers/master/include",
                  "$(BUILDS_ROOT)/ccl/3.3.3/include",
                  "$(BUILDS_ROOT)/VBSFusion/3.200.000/includes",
                  "$(BUILDS_ROOT)/VBSFusion/3.200.000/includes/VBSFusion",
                  "$(DXSDK_DIR)include" }
    configuration { "x32" }
        targetdir(config.target.."x32")
        libdirs { "$(BUILDS_ROOT)/boost/1.51/stage/lib",
                  "$(BUILDS_ROOT)/VBSFusion/3.200.000/libs",
                  "$(DXSDK_DIR)lib/x86",
                  "$(BUILDS_ROOT)/core/master/bin/" .. _ACTION .. "/x32",
                  "$(BUILDS_ROOT)/vbs-support/master/bin/" .. _ACTION .. "/x32",
                  "$(BUILDS_ROOT)/logger/master/bin/" .. _ACTION .. "/x32",
                  "$(BUILDS_ROOT)/pugixml/1.2/scripts/" .. _ACTION .. "/x32",
                  "$(BUILDS_ROOT)/file-parsers/master/bin/" .. _ACTION .. "/x32",
                  "$(BUILDS_ROOT)/ccl/3.3.3/bin/".. _ACTION .."/x32" }
     configuration { "x64" }
         targetdir(config.target.."x64")
         libdirs { "$(BUILDS_ROOT)/boost/1.51/stage/x64/lib",
                   "$(BUILDS_ROOT)/VBSFusion/3.200.000/libs64",
                   "$(DXSDK_DIR)lib/x64",
                   "$(BUILDS_ROOT)/core/master/bin/" .. _ACTION .. "/x64",
                   "$(BUILDS_ROOT)/vbs-support/master/bin/" .. _ACTION .. "/x64",
                   "$(BUILDS_ROOT)/logger/master/bin/" .. _ACTION .. "/x64",
                   "$(BUILDS_ROOT)/pugixml/1.2/scripts/" .. _ACTION .. "/x64",
                   "$(BUILDS_ROOT)/file-parsers/master/bin/" .. _ACTION .. "/x64",
                   "$(BUILDS_ROOT)/ccl/3.3.3/bin/".. _ACTION .."/x64" }
    configuration "ReleaseWithSymbols"
        flags { "Symbols", "NoEditAndContinue", "NoPCH", "Optimize" }
        targetsuffix( "_d" )
        linkoptions { "core.lib", "VBSFusion_2010.lib", "vbs-support.lib", "pugixml.lib", "logger.lib", "file-parsers.lib", "ccl_lib.lib", "d3dx9.lib" }
    configuration "Release"
        defines { "NDEBUG", "WIN32" }
        flags { "Optimize" }
        linkoptions { "core.lib", "VBSFusion_2010.lib", "vbs-support.lib", "pugixml.lib", "logger.lib", "file-parsers.lib", "ccl_lib.lib", "d3dx9.lib" }

project "sync"
    targetname("sync")
    kind "SharedLib"
    files { "../src/**.c*" , "../src/**.def" , "../include/**.h" , "../include/**.cpp" }
    defines { "_LIB", "_CRT_SECURE_NO_WARNINGS" }
    linkoptions { "/DEF:\"..\\..\\src\\Sync.def\"" }
    debugargs { "-window", "-admin", "-forcesimul"}
        configuration { "ReleaseWithSymbols", "x32" }
            postbuildcommands { "xcopy \"$(TargetDir)$(TargetName).dll\" \"C:\\Bohemia Interactive\\ng_beta\\pluginsFusion\" /i /y \n" ..
                                "xcopy \"$(TargetDir)$(TargetName).pdb\" \"C:\\Bohemia Interactive\\ng_beta\\pluginsFusion\" /i /y \n" ..
                                "exit /B 0" }
        configuration { "ReleaseWithSymbols", "x64" }
            postbuildcommands { "xcopy \"$(TargetDir)$(TargetName).dll\" \"C:\\Bohemia Interactive\\ng_beta\\pluginsFusion64\" /i /y \n" ..
                                "xcopy \"$(TargetDir)$(TargetName).pdb\" \"C:\\Bohemia Interactive\\ng_beta\\pluginsFusion64\" /i /y \n" ..
                                "exit /B 0" }

project "test"
    targetname("test")
    includedirs { "$(BUILDS_ROOT)/gtest/1.6.0/include", "$(WindowsSdkDir)include"}
    kind "ConsoleApp"
    files { "../test/**.c*" , "../test/**.h" }
    configuration "Debug"
      linkoptions { "gtestd.lib" }
      flags { "Symbols" }
    configuration "Release"
      linkoptions { "gtest.lib" }
    configuration { "x32" }
      libdirs { "$(BUILDS_ROOT)/gtest/1.6.0/bin/".._ACTION.."/x32/gtest-md" }
    configuration { "x64" }
      libdirs { "$(BUILDS_ROOT)/gtest/1.6.0/bin/".._ACTION.."/x64/gtest-md" }

if _ACTION == "test" then
    os.exit(0)
end
