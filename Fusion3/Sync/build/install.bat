@echo off

@echo Installing logger to directory...

@IF NOT "%~1"=="" (set OUTPUT_DIR=%~1)

@IF DEFINED OUTPUT_DIR (

@echo Output Directory %OUTPUT_DIR%

@rem Copy include
@call ROBOCOPY ..\include\ "%OUTPUT_DIR%\include\" /MIR /njh /njs /ndl /nc /ns

@call ROBOCOPY ..\bin\ "%OUTPUT_DIR%\bin\" /MIR /njh /njs /ndl /nc /ns

)


@echo Install Complete

IF NOT DEFINED PAUSE_BUILD pause

@echo Install logger: Exit Code is %EXIT_CODE%
exit /B %EXIT_CODE%