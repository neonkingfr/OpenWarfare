@echo off
@call premake4 test
@IF ERRORLEVEL 1 echo Configure Failed Aborting! && pause && exit /B %EXIT_CODE%

premake4 clean
@rem premake4 vs2005
@rem premake4 vs2008
premake4 vs2010
