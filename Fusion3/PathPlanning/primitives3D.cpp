#include "PathPlanning.h"

#if !USE_PRIMITIVES_MANAGER
#include <d3d9.h>
#include <d3dx9.h>

#define VERTEX_FORMAT (D3DFVF_XYZ | D3DFVF_DIFFUSE)

struct VertexD3D
{
  float x,y,z;
  DWORD color;
};

//  -====================================================-
//                      PRIMITIVE3D
//  -====================================================-

Primitive3D::Primitive3D(int size)
{
  _vertices = new BI_Vector3[size];
  _nVertices = size;
}

Primitive3D::~Primitive3D()
{
  if (_vertices)
    delete [] _vertices;
}

void Primitive3D::SetColor(const BI_Color& color)
{
  _color = color;
}


//  -====================================================-
//                        TRIANGLE3D
//  -====================================================-

Triangle3D::Triangle3D(const BI_Vector3& v1, const BI_Vector3& v2, const BI_Vector3& v3) : Primitive3D(3)
{
  _vertices[0] = v1;
  _vertices[1] = v2;
  _vertices[2] = v3;
}

void Triangle3D::AddTrianglesToBuffer(char* buffer) const
{
  for (int i = 0; i < 3; i++)
  {
    VertexD3D vertex;
    int size = sizeof(VertexD3D);
    vertex.color = D3DCOLOR_COLORVALUE(_color.R(), _color.G(), _color.B(), _color.A());
    vertex.x = _vertices[i][0];
    vertex.y = _vertices[i][1];
    vertex.z = _vertices[i][2];

    memcpy(buffer, &vertex, size);
    buffer += size;
  }
}
#endif