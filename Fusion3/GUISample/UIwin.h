#pragma once

#ifndef UIwin_h__
#define UIwin_h__

#include <wx/wx.h>
#include <wx/listctrl.h>

#define ID_BUTTON 100
#define ID_LISTCTRL_UNIT 101
#define TIMER_ID 123

class UIwin : public wxFrame
{
public:

  UIwin();

  //Funtions to call wxFrame and print values
  void AddUnitName(long index, const wxString& unit) { listUnits->InsertItem(index, unit); }
  void AddUnitSide(long index, const wxString& unit) { listUnits->SetItem(index, 1, unit); }
  void AddUnitRank(long index, const wxString& unit) { listUnits->SetItem(index, 2, unit); }
  void AddUnitExp(long index, const wxString& unit) { listUnits->SetItem(index, 3, unit); }
  void AddUnitEndur(long index, const wxString& unit) { listUnits->SetItem(index, 4, unit); }
  void AddUnitGroup(long index, const wxString& unit) { listUnits->SetItem(index, 5, unit); }
  void AddUnitDamage(long index, const wxString& unit) { listUnits->SetItem(index, 6, unit); }
  void Fill();

private:

  void OnClose(wxCloseEvent& e);
  void OnClick(wxCommandEvent& e);
  wxString printTim (const char* str);

  wxTimer* refreshTimer;
  wxListCtrl * listUnits;
  wxButton * RefreshUn;


  DECLARE_EVENT_TABLE();
};


// TODO: reference additional headers your program requires here
#endif
