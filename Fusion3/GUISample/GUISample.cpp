#include <string>

#include "exports.h"
#include "GUISample.h"

//----------
std::vector<VBSFusion::Unit> play, play_noup;
HANDLE threadPanel = NULL;
bool active = false;
UIwin * mFrame = NULL;
//-------------

void SetMain(UIwin * frame) { mFrame = frame; }

void Activate () { active = true; }

std::vector<std::vector<std::string>> GetInfoUnits()
{
  std::vector<VBSFusion::Unit>::iterator itera;
  std::vector<std::string> inter;
  std::vector<std::vector<std::string>> fin;
  std::ostringstream str;
  
  for(itera = play.begin(); itera != play.end(); itera++)
  {
    //VBSFusion::VehicleUtilities::updateStaticProperties((*itera));
    inter.push_back((*itera).getName());
    inter.push_back((*itera).getSideString());
    inter.push_back((*itera).getRankString());
    str << (*itera).getExperience(); inter.push_back(str.str()); str.str("");
    str << (*itera).getEndurance(); inter.push_back(str.str()); str.str("");
    inter.push_back((*itera).getGroup());
    str << (*itera).getDamage(); inter.push_back(str.str()); str.str("");
    fin.push_back(inter);
    inter.clear();
  }
  play.clear();
  return fin;
}


DWORD WINAPI ThreadProc(LPVOID lpParameter)
{
  RunApp();
  mFrame = NULL;
  threadPanel = NULL;
  return true;
}

//This function creates a thread and it will used to built UI window.
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
	 if (threadPanel == NULL && strcmp(input, "i") == 0)
  {
    threadPanel = CreateThread(NULL, 0, ThreadProc, NULL, 0, NULL);
  }

   play = VBSFusion::MissionUtilities::getAllUnits();

  return "";
};

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (threadPanel == NULL)
    return;

  //When UI is running and click the button, in next frame UI will be reload with new info
  if (active)
  {
    //It is needed to get all new units from Main thread.
    play_noup = VBSFusion::MissionUtilities::getAllUnits();
    for(std::vector<VBSFusion::Unit>::iterator i = play_noup.begin(); i !=play_noup.end(); i++)
    {
      VBSFusion::VehicleUtilities::updateStaticProperties((*i)); play.push_back((*i));
    }
    mFrame->Fill();
    active = false;
  }
}


/*!
Do not modify this code. Defines the DLL main code fragment
*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
    // If the UI is running then close it.
    if (threadPanel != NULL)
      TerminateThread(threadPanel, 0);
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};

