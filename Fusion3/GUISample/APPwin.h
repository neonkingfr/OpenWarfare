#pragma once

#ifndef APPwin_h__
#define APPwin_h__

#include "UIwin.h"
#include "GUISample.h"

#define IMPLEMENT_APP_GET_ONLY(appname)                                             \
  appname& wxGetApp() { return *wx_static_cast(appname*, wxApp::GetInstance()); } \

class APPwin : public wxApp
{
public :

  APPwin(void);
  ~APPwin(void);

  virtual bool OnInit();
  void refUnit();
  void fill();

private :
  UIwin * _mainFrame;

};
void RunApp();
static wxAppConsole *wxCreateApp();

DECLARE_APP(APPwin)
#endif