#include "APPwin.h"

IMPLEMENT_APP_GET_ONLY(APPwin)

APPwin::APPwin()
:_mainFrame(NULL)
{
}

APPwin::~APPwin()
{
}

// This function creates UI window and display data. It is called after RunApp() is finished
bool APPwin::OnInit()
{

  _mainFrame = new UIwin();
  _mainFrame->Show(true);

  SetMain(_mainFrame);
  fill();

  return true;
}

static wxAppConsole *wxCreateApp()
{
  wxAppConsole::CheckBuildOptions(WX_BUILD_OPTIONS_SIGNATURE,"APPwin");
  return new APPwin;
}

void RunApp()
{
  wxApp::SetInitializerFunction(wxCreateApp);
  wxEntry(0,NULL);
}

void APPwin::refUnit()
{
  Activate();
}

void APPwin::fill()
{
  long i = 0;
  std::vector<std::vector<std::string>> units = GetInfoUnits();
  for(std::vector<std::vector<std::string>>::iterator iter = units.begin(); iter != units.end(); iter++)
  {
    std::vector<std::string> str = (*iter);
    _mainFrame->AddUnitName(i, str[0]);
    _mainFrame->AddUnitSide(i, str[1]);
    _mainFrame->AddUnitRank(i, str[2]);
    _mainFrame->AddUnitEndur(i, str[3]);
    _mainFrame->AddUnitExp(i, str[4]);
    _mainFrame->AddUnitGroup(i, str[5]);
    _mainFrame->AddUnitDamage(i++, str[6]);
  }
}