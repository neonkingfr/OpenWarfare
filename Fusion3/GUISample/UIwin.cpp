
#include "UIwin.h"
#include "APPwin.h"
#include <wx/listctrl.h>

BEGIN_EVENT_TABLE(UIwin, wxFrame)
EVT_CLOSE(UIwin::OnClose)
EVT_BUTTON(ID_BUTTON, UIwin::OnClick)
END_EVENT_TABLE()

UIwin::UIwin()
:wxFrame(NULL, wxID_ANY, "Sample panel", wxDefaultPosition, wxSize(800,400))
{
  wxPanel * mainLayout = new wxPanel (this, wxID_ANY);
  wxBoxSizer * mainSize = new wxBoxSizer (wxVERTICAL);
  {
    //Display Units
    wxBoxSizer *reader = new wxBoxSizer(wxVERTICAL);
    {

        wxStaticText *labelUnit = new wxStaticText(mainLayout, wxID_ANY, "Units", wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE);
        wxFont *letter = new wxFont(13, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
        labelUnit->SetFont(*letter);
        reader ->Add(labelUnit, 0, wxALL, 5);

        listUnits = new wxListCtrl(mainLayout, ID_LISTCTRL_UNIT, wxDefaultPosition, wxSize(900,600), wxLC_REPORT);

        wxListItem col0; col0.SetId(0); col0.SetText( _("Name") ); col0.SetWidth(50);
        listUnits->InsertColumn(0, col0);

        wxListItem col1; col1.SetId(1); col1.SetText( _("Side") ); col1.SetWidth(50);
        listUnits->InsertColumn(1, col1);

        wxListItem col2; col2.SetId(2); col2.SetText( _("Rank") ); col2.SetWidth(50);
        listUnits->InsertColumn(2, col2);

        wxListItem col3; col3.SetId(3); col3.SetText( _("Experience") ); col3.SetWidth(80);
        listUnits->InsertColumn(3, col3);

        wxListItem col4; col4.SetId(4); col4.SetText( _("Endurance") ); col4.SetWidth(80);
        listUnits->InsertColumn(4, col4);

        wxListItem col5; col5.SetId(5); col5.SetText( _("Group") ); col5.SetWidth(70);
        listUnits->InsertColumn(5, col5);

        wxListItem col6; col6.SetId(6); col6.SetText( _("Damage") ); col6.SetWidth(80);
        listUnits->InsertColumn(6, col6);
        
        reader->Add(listUnits, 1, wxEXPAND | wxALL, 5);

        RefreshUn = new wxButton (mainLayout, ID_BUTTON, _("Refresh Units"), wxDefaultPosition, wxSize(100,20), 0);
        
        reader->Add(RefreshUn, 1, wxALL, 5);
    }
    mainSize->Add(reader, 1, wxEXPAND, 0);

    mainLayout->SetSizer(mainSize);
  }


  refreshTimer = new wxTimer(this,TIMER_ID);
  refreshTimer->Start(30); 
}

// Removes all panel and all the data
void UIwin::OnClose(wxCloseEvent& e)
{
  Destroy();
}


void UIwin::OnClick(wxCommandEvent& e)
{
  ::wxGetApp().refUnit();
}

void UIwin::Fill()
{
  listUnits->DeleteAllItems();
  ::wxGetApp().fill();
}