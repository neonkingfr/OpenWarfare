#include <windows.h>
#include <Commctrl.h>
#include "VirtualInputDevice.h"
#include "resource.h"

#include "VBSFusionAppContext.h"
VBSFusion::VBSFusionAppContext appContext;

HINSTANCE hInst; ///< app instance
HWND hWnd;       ///< VID window handle
HWND buttonA;    ///< button A
HWND buttonB;    ///< button B
HWND hAxisX;     ///< x axis trackbar handle
HWND hAxisY;     ///< y axis trackbar handle

const char* pluginName = "Virtual Input Device"; ///< plugin name

bool buttons[2] = {false, false}; ///< VID buttons states
float axisX = 0.0f;               ///< VID x axis value in range <-1,1>
float axisY = 0.0f;               ///< VID y axis value in range <-1,1>


//! Dialog's procedure function.
INT_PTR CALLBACK VIDDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
  switch (message)
  {
    case WM_DESTROY:
    {
      hWnd = NULL;
      break;
    }

    case WM_INITDIALOG:
    {
      // get buttons
      buttonA = GetDlgItem(hDlg, ID_A);
      buttonB = GetDlgItem(hDlg, ID_B);

      // get tracbars handles and set range and position
      hAxisX = GetDlgItem(hDlg, IDC_X_ASIX);
      SendMessage(hAxisX, TBM_SETRANGEMIN, TRUE, -1000);
      SendMessage(hAxisX, TBM_SETRANGEMAX, TRUE, 1000);
      SendMessage(hAxisX, TBM_SETPOS, TRUE, 0);

      hAxisY = GetDlgItem(hDlg, IDC_Y_AXIS);
      SendMessage(hAxisY, TBM_SETRANGEMIN, TRUE, -1000);
      SendMessage(hAxisY, TBM_SETRANGEMAX, TRUE, 1000);
      SendMessage(hAxisY, TBM_SETPOS, TRUE, 0);
      break;
    }    

    case WM_QUIT:
    {
      // called when window is about to be destroyed
      PostQuitMessage(0);
      break;
    }
  }

  return FALSE;
}

//! Create VID window.
void CreateVIDWindow(LPVOID hInst)
{
  hWnd = CreateDialog((HINSTANCE)hInst, MAKEINTRESOURCE(IDD_VID_WINDOW), NULL, VIDDlgProc);

  ShowWindow((HWND)hWnd, SW_SHOW);
  UpdateWindow((HWND)hWnd);
}

//! Destroy VID window.
void DoDestroyVIDWindow()
{
  if (hWnd)
  {
    DestroyWindow((HWND)hWnd);
  }
}

struct VBSParametersEx
{
  const char *_dirInstall;  // VBS installation directory
  const char *_dirUser;     // user's VBS directory
};

//! OnLoadEx callback - create window.
int WINAPI OnLoadEx(VBSParametersEx* params)
{
  CreateVIDWindow(hInst);

  return 0;
}

//! OnUnload callback - destroy window.
void WINAPI OnUnload()
{
  DoDestroyVIDWindow();
}

//! Returns value of specific device (button or axis)
float WINAPI GetDeviceValue(UINT device_id)
{
  if (device_id < 2)
  {
    return buttons[device_id];
  }

  // +X axis
  if (device_id == 2)
  {
    if (axisX < 0) return 0; // saturate
    return axisX;
  }

  // -X axis
  if (device_id == 3)
  {
    if (axisX > 0) return 0; // saturate
    return -axisX; // VBS needs positive values
  }

  // +Y axis
  if (device_id == 4)
  {
    if (axisY < 0) return 0; // saturate
    return axisY;
  }

  // -Y axis
  if (device_id == 5)
  {
    if (axisY > 0) return 0; // saturate
    return -axisY; // VBS needs positive values
  }

  return 0;
}

//! Poll VID devices.
void PollVIDDevices()
{
  // poll all buttons & axis
  buttons[0] = (SendMessage(buttonA, BM_GETSTATE, 0, 0) == 620) ? true : false; // 620 constant - retrieved by testing return values, was not able to find it in msdn
  buttons[1] = (SendMessage(buttonB, BM_GETSTATE, 0, 0) == 620) ? true : false;

  int pos = SendMessage(hAxisX, TBM_GETPOS, 0, 0);
  axisX = pos / 1000.0f; // get to <-1,1> range

  pos = SendMessage(hAxisY, TBM_GETPOS, 0, 0);
  axisY = pos / -1000.0f; // negate and get to <-1,1> range
}

void WINAPI OnSimulationStep(float deltaT)
{
  PollVIDDevices();
}

//! Register VID devices.
void WINAPI RegisterVirtualInputDevice(VIRTUALINPUTDEVICE_DEVICE_REGISTERCALLBACK fptr_register_device)
{
  // plugin name, id, (DIGITAL = 0, ANALOG = 1), description, value function
  fptr_register_device(pluginName, 0, 0, "VID Button A", GetDeviceValue);
  fptr_register_device(pluginName, 1, 0, "VID Button B", GetDeviceValue);
  fptr_register_device(pluginName, 2, 1, "VID Axis +X", GetDeviceValue);
  fptr_register_device(pluginName, 3, 1, "VID Axis -X", GetDeviceValue);
  fptr_register_device(pluginName, 4, 1, "VID Axis +Y", GetDeviceValue);
  fptr_register_device(pluginName, 5, 1, "VID Axis -Y", GetDeviceValue);
}

//! Unregister VID devices;
void WINAPI UnregisterVirtualInputDevice(VIRTUALINPUTDEVICE_DEVICE_UNREGISTERCALLBACK fptr_unregister_device)
{
  fptr_unregister_device(pluginName, 0);
  fptr_unregister_device(pluginName, 1);
  fptr_unregister_device(pluginName, 2);
  fptr_unregister_device(pluginName, 3);
  fptr_unregister_device(pluginName, 4);
  fptr_unregister_device(pluginName, 5);
}

//! Main DLL function
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  hInst = hDll;

  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}
