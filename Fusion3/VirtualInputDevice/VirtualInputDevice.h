#ifndef _VIRTUALINPUTDEVICE_H_
#define _VIRTUALINPUTDEVICE_H_

#define VBSPLUGIN_EXPORT __declspec(dllexport)

// VID Register tyepdef's
typedef float (WINAPI *VIRTUALINPUTDEVICE_DEVICE_GETVALUE)(UINT device_id);
typedef UINT  (WINAPI *VIRTUALINPUTDEVICE_DEVICE_REGISTERCALLBACK)(const char *plugin_name, UINT device_id, UINT device_type, const char *device_description, VIRTUALINPUTDEVICE_DEVICE_GETVALUE fptr_get_device_value);
typedef UINT  (WINAPI *VIRTUALINPUTDEVICE_DEVICE_UNREGISTERCALLBACK)(const char *plugin_name, UINT device_id);

VBSPLUGIN_EXPORT void WINAPI RegisterVirtualInputDevice(VIRTUALINPUTDEVICE_DEVICE_REGISTERCALLBACK fptr_register_device);
VBSPLUGIN_EXPORT void WINAPI UnregisterVirtualInputDevice(VIRTUALINPUTDEVICE_DEVICE_UNREGISTERCALLBACK fptr_unregister_device);

#endif
