#include <windows.h>
#include "vbsngplugin.h"

#include <d3d9.h>
#include <d3dx9.h>

#include "../../Common/Essential/AppFrameWork.hpp"

#include "drawsample.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBSFusion interface library forced linking
// Needed for plugins that don't use any of VBSFusion symbols
// This will insure that VBSFusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBSFusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBSFusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBSFusionAppContext.h"
VBSFusion::VBSFusionAppContext appContext;
#if _X64
#pragma comment(lib, "../_depends/Fusion/lib64/VBSFusion_2005.lib")
#else
#pragma comment(lib, "../_depends/Fusion/lib/VBSFusion_2005.lib")
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

drawsample *sample = NULL;


// Global notes for NG drawing:
// - Do not attempt to access the Direct3D device directly in functions where it is not allowed, like in OnNgDrawnPass.
// - Do not use D3DPOOL_MANAGED pool for any resource, it is not usable in Windows Vista/7 due to Direct3D 9Ex.
// - When using Windows Vista/7, device resets behave different to Windows XP and previous VBS2 versions. To test plugin
//   reset compatibility use "-winxp" commandline parameter with VBS2NG.
// - You no longer need to capture and restore the device state when returning from a plugin call (ie. with a state block)
//   it is done automatically by the engine for draw functions


// This function is called once per frame before any drawing is done by engine
// parameters contain real direct 3D device, so resource management can be done here
VBSPLUGIN_EXPORT void WINAPI OnNgPreDraw(paramPreDraw *param, DWORD paramSize)
{
  if(paramSize<sizeof(paramPreDraw))
    return; // Engine is older than our plugin structs

  if(!sample) sample = new drawsample(param->dev);

  // Cycle between overlay buffers to enable.
  // When setting one of the wantOverlayX_feedback values to true engine creates an overlay buffer
  // and later calls OnNgDrawnPass with appropriate pass ID. Plugins can render to that overlay buffer
  // and engine later on draws the buffer contents into the backbuffer with alpha blending.
  // This is more efficient than using OnNgFilledBackbuffer, though the buffers require extra video memory.
  // Note: Do not set the wantOverlayX_feedback values to false - It could override request from another plugin.
  // When these are set to true engine will allocate a vram buffer, and when it is no longer set true the buffer will be freed.
  // So do not set these to true unless you need the overlay, but also prefer to keep the value true if you need the buffer
  // in the near future to avoid frequent allocation/release of large VRAM buffers.
  int t = GetTickCount()%9000;

  // Enable BegPP overlay - Its contents are drawn before any engine side postprocess effects
  if(t < 3000) param->wantOverlayBegPP_feedback = true;
  // Enable EndPP overlay - Drawn after engine postprocess effects but before UI
  else if(t < 6000) param->wantOverlayEndPP_feedback = true;
  // Enable UI overlay - Drawn after UI just before present (similar to OnNgFilledBackbuffer)
  else param->wantOverlayUI_feedback = true;

  // Here we have an opportunity to change the near clip plane by changing the value of param->clipNear_feedback
  // By default the engine will try to move the near clip plane as far as possible, but it cannot consider primitives
  // that are drawn by plugins. Therefore if plugins need to draw primitives very close to the camera, they need to
  // tell here how close they need the near clip plane to be set.

  // It is very important to not set the near clip plane too close, ie. you need to actually determine how close the nearest
  // primitive you are going to draw is and only set the clip to that distance. Simply setting a very low near clip plane
  // at all times will result in severe depth precision issues for the whole scene.
}

// This function is called once per frame, after depth priming is done but before normal rendering is done
// parameters contain real direct 3D device, so resource management can be done here
// At this point some extra information is available like eye accomodation value
VBSPLUGIN_EXPORT void WINAPI OnNgInitDraw(paramInitDraw *param, DWORD paramSize)
{  
  if(sample)
  {
    // drawsample has access to the Direct3D 9 device here
    sample->manageObjects(param->dev, param->deltaT);
    sample->setLights(param->eyeAccom, param->mainLight);
  }
}

// This function is called when engine completes a render pass
// Render passes are done for different types of objects (see drawPassIdentifier) and different render modes (see drawModeIdentifier)
// so this function is called many times per framed, you can pick the required slot to do drawing here
// parameters contain CB interface which must be used to do only drawing here,
// do not attempt to access the D3D device directly and do not release any resources!
VBSPLUGIN_EXPORT void WINAPI OnNgDrawnPass(paramDrawnPass *param, DWORD paramSize)
{
  // This is where we want to do our 3D scene drawing. 

  // We want to only draw in drawModeCommon here, since we do not do depth priming in the plugin
  if(param->modeId != drawModeCommon) return;

  // Only draw at end of drawPass2 - Most of the scene has been drawn at this point excluding the
  // player's own character and vehicle he/she is in.
  // Alternatively we could draw at end of drawPass3, if we were drawing something very close to the player
  // as it is when the player's character and vehicle are drawn with a special depth range
  if(param->passId == drawPass2 && sample)
  {
    sample->drawObjects(param->cbi, param->cameraFrustum);
  }

  // Sample for drawing onto the map (OME & RTE)
  // Red and green squares into different map layers
  if(param->passId == drawPassMap && sample)
  {
    sample->drawBox2D(param->cbi, -0.4f, 0.0f, 0.2f, 0.2f, 1, 0, 0);
  }
  if(param->passId == drawPassMapBg && sample)
  {
    sample->drawBox2D(param->cbi, 0.2f, 0.0f, 0.2f, 0.2f, 0, 1, 0);
  }

  if(sample)
  {
    // Check for overlay passes. If any overlay were enabled earlier in PreDraw we get matching pass call here.
    // A special buffer has been prepared for us to render to here, render with alpha information for correct blending.

    // Draw different color boxes here on different overlays
    float time = ((float)GetTickCount()/1000.0f);
    float x = sin(time)*0.33f;
    float y = cos(time)*0.33f;
    float a = (sin(time*5.0f)*0.5f)+0.5f; // Alpha

    if(param->passId == drawPassPpOverlayBegPP)
    {
      sample->drawBox2D(param->cbi, x, y, 0.2f, 0.2f, 1, 0.5f, 0, a);
    }
    if(param->passId == drawPassPpOverlayEndPP)
    {
      sample->drawBox2D(param->cbi, x, y, 0.2f, 0.2f, 0, 1, 0.5f, a);
    }
    if(param->passId == drawPassPpOverlayUI)
    {
      sample->drawBox2D(param->cbi, x, y, 0.2f, 0.2f, 0, 0.5f, 1, a);
    }
  }
}

// This function is called when the skybox is drawn by the engine
// parameters contain CB interface which must be used to do only drawing here,
// do not attempt to access the D3D device directly and do not release any resources!
VBSPLUGIN_EXPORT void WINAPI OnNgDrawnSky(paramDrawnSky *param, DWORD paramSize)
{
}

// This function is called at end of frame rendering, after postprocessing is done and UI is rendered
// parameters contain CB interface which must be used to do only drawing here,
// do not attempt to access the D3D device directly and do not release any resources!
VBSPLUGIN_EXPORT void WINAPI OnNgFilledBackbuffer(paramFilledBackbuffer *param, DWORD paramSize)
{
  // Draw a blue rectangle at the top of the screen
  if(sample)
  {
    sample->drawBox2D(param->cbi, -0.05f, 0.5f, 0.1f, 0.1f, 0, 0, 1);
  }
}

// This function is called when D3D device is created by engine
// parameters contain real direct 3D device, so resource management can be done here
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceInit(paramDeviceInit *param, DWORD paramSize)
{  
  // Create the sample
  if(!sample) sample = new drawsample(param->dev);
}

// This function is called when D3D device is destroyed or reset by engine
// parameters contain real direct 3D device, so resource management can be done here
// All D3D resources created by the plugin must be released here, otherwise device resets will fail
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceInvalidate(paramDeviceInvalidate *param, DWORD paramSize)
{
  // Delete the sample object, this will release everything it has created
  // Alternatively, we could check for param->isReset and only release Direct3D handles without destroying anything else
  delete sample;
  sample = NULL;
}

// This function is called when D3D device is re-created after reset by engine
// parameters contain real direct 3D device, so resource management can be done here
// Resources released in OnNgDeviceInvalidate should be re-created here
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceRestore(paramDeviceRestore *param, DWORD paramSize)
{
  // Re-create the sample
  if(!sample) sample = new drawsample(param->dev);
}


// Previous plugin functionality, same as before:

void LogF(const char* format, ...)
{
	va_list     argptr;
	char        str[1024];
	va_start (argptr,format);
	vsnprintf (str,1024,format,argptr);
	va_end   (argptr);
  OutputDebugString(str);
}

ExecuteCommandType ExecuteCommand = NULL;

VBSPLUGIN_EXPORT void WINAPI OnUnload() {
  // Whole plugin is unloaded, release the sample
  delete sample;
  sample = NULL;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT) {}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *inp)
{
  return "[]";
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
        break;
      case DLL_PROCESS_DETACH:
        break;
   }
   return TRUE;
}

VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins) {}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

