#include "PCH.h"
#include "VRPNClient.hpp"

#include "ControlledUnit.hpp"
#include "..\Common\MathHelper.hpp"

extern VRPNClient* g_pClient;

//_________________________________________________________________________
void TrackerToMatrix(q_type quat, float pos[3], Matrix4& mat)
{
  float x = quat[0];
  float y = quat[1];
  float z = quat[2];
  float w = quat[3];

  float wx = w*x*2;
  float wy = w*y*2;
  float wz = w*z*2;
  float xx = x*x*2;
  float xy = x*y*2;
  float xz = x*z*2;
  float yy = y*y*2;
  float yz = y*z*2;
  float zz = z*z*2;

  mat.Set(0, 0) = 1 - yy - zz;
  mat.Set(0, 1) = xy - wz;
  mat.Set(0, 2) = xz + wy;
  mat.Set(1, 0) = xy + wz;
  mat.Set(1, 1) = 1 - xx - zz;
  mat.Set(1, 2) = yz - wx;
  mat.Set(2, 0) = xz - wy;
  mat.Set(2, 1) = yz + wx;
  mat.Set(2, 2) = 1 - xx - yy;

  mat.SetTranslation(Vector3(pos[0], pos[1], pos[2]));
}

//_________________________________________________________________________
PoseControlUnit::PoseControlUnit() 
:_pelvisXOffset(0.0f), _pelvisZOffset(0.0f)
{
}

//_________________________________________________________________________
bool PoseControlUnit::onModifyBone(VBSFusion::Matrix4f& mat, VBSFusion::SKELETON_TYPE index)
{
  // get tracker data
  vrpn_TRACKERCB trackerData;
  bool dataInitialized = false;
  BoneTrackerMap::const_iterator itTrackerData = _boneTrackerMap.find(index);
  if (itTrackerData != _boneTrackerMap.end())
  {
    TrackerList::iterator itTracker = g_pClient->_trackers.find((*itTrackerData).second);
    if (itTracker != g_pClient->_trackers.end())
    {
      trackerData = (*itTracker).second._data;
      dataInitialized = (trackerData.sensor == eTRACKER_INITIALIZED); // sensor used as initialization flag
    }
  }

  // process tracker data
  // (if the tracker isn't receiving data from server, identity matrix is set)
  if (dataInitialized)
  {
    q_type boneRotation = { trackerData.quat[0], trackerData.quat[1], trackerData.quat[2], trackerData.quat[3] };
    float bonePosition[3];
    bonePosition[0] = trackerData.pos[0];
    bonePosition[1] = trackerData.pos[1];
    bonePosition[2] = trackerData.pos[2];

    // get absolute transform matrix from tracker
    Matrix4 absoluteTransform = BI_IdentityMatrix4;
    TrackerToMatrix(boneRotation, bonePosition, absoluteTransform);

    // adjust player position in the game
    if (index == ePELVIS)
    {
      _pelvisPrevXOffset = _pelvisXOffset;
      _pelvisPrevZOffset = _pelvisZOffset; 
      _pelvisXOffset = absoluteTransform.GetPos().X();
      _pelvisZOffset = absoluteTransform.GetPos().Z();
      float deltaX = _pelvisXOffset - _pelvisPrevXOffset;
      float deltaZ = _pelvisZOffset - _pelvisPrevZOffset;

      Vector3 unitPos = GetUnitPosASL2(_unitName.c_str());
      SetUnitPosASL2(_unitName.c_str(), unitPos.X() + deltaX, unitPos.Y(), unitPos.Z() + deltaZ);
    }
    
    // update transformation relatively to pelvis
    BI_TVector3<float> pos = absoluteTransform.GetPos();
    pos.SetX(pos.X() - _pelvisXOffset);
    pos.SetZ(pos.Z() - _pelvisZOffset);
    absoluteTransform.SetTranslation(pos);

    // current bone's binding pose
    Matrix4 bindingPose = SkeletonDefinition.Bones[index];

    // inverse binding pose
    BI_TMatrix4<float> invBindingPose;
    invBindingPose.SetInvertGeneral4x4(bindingPose);

    // compute relative-to-binding-pose transform
    Matrix4 relativeToBP = BI_IdentityMatrix4;
    relativeToBP.SetMultiply(absoluteTransform, invBindingPose);

    Matrix4ToMatrix4f(relativeToBP, mat);
  }
  else { mat.setIdentity(); }
 
  return true;
};