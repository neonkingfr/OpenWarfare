#pragma once

#ifndef pluginHeader_h__
#define pluginHeader_h__

#include <windows.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);

int ExecuteCmd(const char* command, char *result = NULL, int resultLength = 0);

Vector3 GetUnitPosASL2(const char* name);
void SetUnitPosASL2(const char* name, const Vector3& pos);
void SetUnitPosASL2(const char* name, float x, float y, float z);
void GetObjectNetID(const char* name, int* val1, int* val2, int* val3);

#endif // pluginHeader_h__