#include "PCH.h"
#include "ControlledVehicle.hpp"
#include "VRPNClient.hpp"

extern VRPNClient* g_pClient;

ControlledVehicle::ControlledVehicle()
{
}

void ControlledVehicle::OnUpdate()
{
  // get tracker data
  vrpn_TRACKERCB trackerData;
  bool dataInitialized = false;
  TrackerList::iterator itTracker = g_pClient->_trackers.find(_trackerName);
  if (itTracker != g_pClient->_trackers.end())
  {
    trackerData = (*itTracker).second._data;
    dataInitialized = trackerData.sensor == eTRACKER_INITIALIZED; // sensor is used as an initialization flag
  }

  // apply thrust/turn
  if (dataInitialized)
  {
    VBSFusion::VehicleUtilities::applyThrustWanted((*this), trackerData.pos[0]);
    VBSFusion::VehicleUtilities::applyTurnWanted((*this), trackerData.pos[1]);
  }
}