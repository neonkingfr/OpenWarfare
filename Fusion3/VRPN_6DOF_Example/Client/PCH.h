
//  STL
#include <string>
#include <map>
#include <fstream>
#include <assert.h>
// fusion     
#include "VBSFusion.h"
#include "VBSFusionDefinitions.h"
#include "Math/Matrix4f.h"
#include "util/PoseControlUtilities.h"
#include "util/CameraUtilities.h"
#include "util/UnitUtilities.h"
#include "util/MissionUtilities.h"
// vrpn
#include "vrpn_Tracker.h"
#include "quat.h" 
// helpers
#include "..\Common\MathHelper.hpp" 
#include "..\Common\VBS2_2xx_Skeleton.hpp"

#include "pluginHeader.h"



