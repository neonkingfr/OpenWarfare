#ifndef __BONETRACKER__
#define __BONETRACKER__

//! Tracker class
/*
 *  This class represents a bone transform as
 *  position (vector) and orientation (quaternion)
 */
class BoneTracker : public vrpn_Tracker
{
public:
  BoneTracker(const char* name, const Matrix4& bindingPose, vrpn_Connection *c = 0);
  virtual ~BoneTracker() {};

  // update
  virtual void mainloop();

  static float ANGLE;

protected:
  struct timeval _timestamp;
  double _bpRotation[4][4]; // binding pose orientation
  double _bpPosition[3];    // binding pose position
};

#endif // __BONETRACKER__