#include "stdafx.h"
#include "BoneTracker.h"

float BoneTracker::ANGLE = 0.0f;

#include "quat.h"

BoneTracker::BoneTracker(const char* name, const Matrix4& bindingPose, vrpn_Connection *c) :
vrpn_Tracker(name, c)
{
  // binding pose rotation matrix
  _bpRotation[0][0] = bindingPose[0][0];  
  _bpRotation[1][0] = bindingPose[1][0];  
  _bpRotation[2][0] = bindingPose[2][0];  
  _bpRotation[3][0] = 0.0;  

  _bpRotation[0][1] = bindingPose[0][1];     
  _bpRotation[1][1] = bindingPose[1][1];  
  _bpRotation[2][1] = bindingPose[2][1];     
  _bpRotation[3][1] = 0.0;    

  _bpRotation[0][2] = bindingPose[0][2];    
  _bpRotation[1][2] = bindingPose[1][2];    
  _bpRotation[2][2] = bindingPose[2][2];  
  _bpRotation[3][2] = 0.0;    

  _bpRotation[0][3] = 0.0;
  _bpRotation[1][3] = 0.0;
  _bpRotation[2][3] = 0.0;
  _bpRotation[3][3] = 1.0;

  // binding pose position vector
  Vector3 bpPos = bindingPose.GetPos();
  _bpPosition[0] = bpPos.X();
  _bpPosition[1] = bpPos.Y();
  _bpPosition[2] = bpPos.Z();
}

void BoneTracker::mainloop()
{
  // Update all tracker data in this method
  // - we will use some fake data for demonstration   
  vrpn_gettimeofday(&_timestamp, NULL);
  vrpn_Tracker::timestamp = _timestamp;

  // sensor index (VBS plugin doesn't use this) 
  d_sensor = 0;

  // send just the binding pose
  q_from_row_matrix(d_quat, _bpRotation);
  pos[0] = _bpPosition[0];
  pos[1] = _bpPosition[1] + 1.014; // add pelvis Y-position
  pos[2] = _bpPosition[2] + sinf(ANGLE);  // just move the character (all bones) forward/backward

  // pack network message
  char msgbuf[1000];
  int  len = vrpn_Tracker::encode_to(msgbuf);
  if (d_connection->pack_message(len, _timestamp, position_m_id, d_sender_id, msgbuf, vrpn_CONNECTION_LOW_LATENCY))
  {
    fprintf(stderr,"can't write message: tossing\n");
  }

  server_mainloop();
}