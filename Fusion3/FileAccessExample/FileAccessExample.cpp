/****************************************************************************
* Plugin to demonstrate functionality needed by WTSS project
****************************************************************************/

#pragma warning(disable: 4996)
#pragma warning(disable: 4251)

#define _BISIM_DEV_GET_SUN_DIRECTION 1

#include <stdio.h>
#include "FileAccessExample.h"

// create dependency on Fusion
#include "VBSFusionAppContext.h"
VBSFusion::VBSFusionAppContext appContext;

typedef void* (WINAPI* OpenFileType)(const char* name);

using namespace VBSFusion;

typedef void* (WINAPI* OpenFileType)(const char* name);
typedef void (WINAPI* CloseFileType)(void* handle);
typedef void (WINAPI* SeekgFileType)(void* handle, int pos, int dir);
typedef int (WINAPI* TellgFileType)(void* handle);
typedef void (WINAPI* ReadFileType)(void* handle, void* buffer, int n);

OpenFileType open = NULL;
CloseFileType close = NULL;
SeekgFileType seekg = NULL;
TellgFileType tellg = NULL;
ReadFileType read = NULL;


#define _DIAG 1
#define LOG_HEADER "[FileAccessExample] "
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugStr(buffer);
#endif
}

VBSPLUGIN_EXPORT void WINAPI RegisterOpenFileFnc(void *fileFnc)
{
  open = (OpenFileType)fileFnc;
}

VBSPLUGIN_EXPORT void WINAPI RegisterCloseFileFnc(void *fileFnc)
{
  close = (CloseFileType)fileFnc;
}

VBSPLUGIN_EXPORT void WINAPI RegisterSeekgFileFnc(void *fileFnc)
{
  seekg = (SeekgFileType)fileFnc;
}

VBSPLUGIN_EXPORT void WINAPI RegisterTellgFileFnc(void *fileFnc)
{
  tellg = (TellgFileType)fileFnc;
}

VBSPLUGIN_EXPORT void WINAPI RegisterReadFileFnc(void *fileFnc)
{
  read = (ReadFileType)fileFnc;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
}

VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  void *handle = open("Test\\test.txt");

  int currentPos = tellg(handle);
  int begin,end;

  seekg(handle, 0, 0);
  begin = tellg(handle);
  seekg(handle, 0, 2);
  end = tellg(handle);
  seekg(handle, currentPos, 0);

  int size = (end-begin);

  char *testBuffer = new char[size+1];
  memset(testBuffer, 0, size+1);
  read(handle, testBuffer, size);
  LogDiagMessage("%s%s", LOG_HEADER,testBuffer);

  close(handle);
};

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}

