#include "poseControlUnit.h"

void PoseControlUnit::setHeadMovementParam(float param)
{
	if (moveVal < 2)
		moveVal += param;
	else
		moveVal =0;
};


bool PoseControlUnit::onModifyBone(Matrix4f& mat, SKELETON_TYPE index)
{
	if (index == HEAD )
	{
		Vector3f headpos = mat.getPosition();
		headpos = headpos + Vector3f(0, (moveVal * 0.05f), 0);

		/* move the units' head up and down */
		mat.setPosition(headpos.X(), headpos.Y(),headpos.Z());
	}
	return true;
};