#ifndef __XAITCONTROL_H__
#define __XAITCONTROL_H__

/****************************************************************************
* Fusion plugin for integration xaiControl into VBS2                        *
*****************************************************************************/

#define _WINSOCKAPI_ // stops windows.h including winsock.h
// Xaitment includes
#include <xait/control/xaitControl.h>
#include <xait/control/FSMManager.h>
#include <xait/control/Interface.h>
#include <xait/control/ExternalVariableCallback.h>

// Fusion includes
#include <data/NetworkID.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

using namespace XAIT;
using namespace XAIT::Common;
using namespace XAIT::Control;

#define LOG_HEADER "[Xaitment Control] "
void LogDiagMessage(const char *format, ...);

struct UnitFSMInfo
{
  std::string unitName;
  long long id[3];
  std::string fsmName;
};

typedef Container::LookUp<VBSFusion::NetworkID, Container::Vector<FSMInstance *> *> UnitFSMLookup;
typedef Container::LookUp<VBSFusion::NetworkID, Container::Vector<FSMInstance *> *>::Iterator UnitFSMLookupIterator;

typedef Container::Vector<FSMInstance *>::Iterator FSMInstancesIterator;

typedef std::vector<UnitFSMInfo> UnitPreMissionInits;
typedef std::vector<UnitFSMInfo>::iterator UnitPreMissionInitsIterator;

// parameters that may be passed into OnLoad
struct IDirect3DDevice9;
struct VBSParameters
{
  const char *_dirInstall;  // VBS installation directory
  const char *_dirUser;     // user's VBS directory
  IDirect3DDevice9 *_device; // rendering D3D device; is NULL if !_VBS3_PLUGIN_DEVICE
};
#endif