#include "XaitControl.h"

#define _DIAG 1 // Enables/disables diagnostic messages
#define _BISIM_DEV_EXTERNAL_FSM 1
#define _BISIM_DEV_DANGER_EH 1
#define _BISIM_DEV_COVER_REACHED_EH 1

#pragma region Servicecode

#pragma region GlobalDefinitions
#include <xait/common/callback/FileIOCallbackWin32.h>

#include <data/Unit.h>
#include <data/ControllableObject.h>
#include <util/ControllableObjectUtilities.h>
#include <util/FSMUtilities.h>

#include "vbs2Wrapper.h"

#define FSM_SET_NAME "xaitControlVBS2.xml"
#define DLL_DIRECTORY "pluginsFusion\\dll"

#if _X64
#if _DEBUG
#define XAITCOMMON_DLL "xaitcommon-win64D-vc80shared.dll"
#define XAITCONTROL_DLL "xaitcontrol-win64D-vc80shared.dll"
#else
#define XAITCOMMON_DLL "xaitcommon-win64-vc80shared.dll"
#define XAITCONTROL_DLL "xaitcontrol-win64-vc80shared.dll"
#endif

#else

#if _DEBUG
#define XAITCOMMON_DLL "xaitcommon-win32D-vc80shared.dll"
#define XAITCONTROL_DLL "xaitcontrol-win32D-vc80shared.dll"
#else
#define XAITCOMMON_DLL "xaitcommon-win32-vc80shared.dll"
#define XAITCONTROL_DLL "xaitcontrol-win32-vc80shared.dll"
#endif
#endif //_X64

#define DELAYED_DLLS 0

HMODULE XaitControlHModule = NULL;
HMODULE XaitCommonHModule = NULL;

FSMSet* gFsmSet = NULL;
float gGameTime = 0.0;
bool gIsControlInitialized = false;
FILETIME gLastWrite;
UnitFSMLookup *gUnitFSMLookup;
bool gMissionStarted = false;

// vector holding information about all SetFSM requests done before mission started
UnitPreMissionInits gPreMissionInits;

#pragma warning (disable: 4244)
VBSFusion::NetworkID NetID(int64 id[3]) { return VBSFusion::NetworkID((int)id[0], (int)id[1], id[2]); }
#pragma endregion GlobalDefinitions

#pragma region Diagnostics
// Logs text message into debug output
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugString(buffer);
#endif
}

void LogMessage(Common::String text)
{
  LogDiagMessage(text.getConstCharPtr());
}

void ShowFSMDiagnostics(float deltaT)
{
#if _DIAG
  // Diagnostics messages about FSMs currently used by unit and about states those FSMs are in.
  // Shows in debug output and also above unit's head in VBS2.
  for (UnitFSMLookupIterator unitIterator = gUnitFSMLookup->begin(); unitIterator != gUnitFSMLookup->end(); unitIterator++)
  {
    Container::Vector<FSMInstance *> *instances = (*unitIterator).second;
    if (!instances) continue;

    Common::String output = "XaitControl";
    for (FSMInstancesIterator fsmIterator = instances->begin(); fsmIterator != instances->end(); fsmIterator++)
    {
      Common::String fsmName =  (*fsmIterator)->getCurrentFSM()->getName();
      VBSFusion::Unit unit((VBSFusion::NetworkID)(*unitIterator).first);
      if(VBSFusion::ControllableObjectUtilities::isAvailable(unit))
      {
        output += "\\n[" + fsmName + "]:" + (*fsmIterator)->getCurrentStateName();
        VBSFusion::FSM::TypeValueContainer object(VBSFusion::FSM::typeObject, (void *)&unit);
        std::string oString(output.getConstCharPtr());
        std::vector<string> oVector;
        oVector.push_back(oString);
        VBSFusion::FSM::TypeValueContainer string(VBSFusion::FSM::typeStringArray, (void *)&oVector);
        VBSFusion::FSM::TypeValueContainer result(VBSFusion::FSM::typeVoid, (void *)NULL);
        VBSFusion::FSM::FSMUtilities::Execute("DisplayObjectText", result, object, string);
      }
#endif
    }
  }
}
#pragma endregion Diagnostics

#pragma region FSMMaintenance
void ClearAllFSM()
{
  UnitFSMLookup::Iterator iter = gUnitFSMLookup->begin();
  UnitFSMLookup::Iterator iterEnd = gUnitFSMLookup->end();

  for(; iter!= iterEnd; ++iter)
  {
    Container::Vector<Common::String>* fsmNames = new Container::Vector<Common::String>(Common::Interface::getGlobalAllocator());

    Container::Vector<FSMInstance *>::Iterator innerIter = (*iter).second->begin();
    Container::Vector<FSMInstance *>::Iterator innerIterEnd = (*iter).second->end();

    for(; innerIter!= innerIterEnd; ++innerIter)
    {
      delete (*innerIter);
    }

    (*iter).second->clear();
  }

  gUnitFSMLookup->clear();
}

void RestartAllFSM(bool force)
{
  if(!gIsControlInitialized)
    return;

  //check for file changed
  WIN32_FILE_ATTRIBUTE_DATA fileInfo;
  GetFileAttributesEx(FSM_SET_NAME, GetFileExInfoStandard, &fileInfo);
  FILETIME lw = fileInfo.ftLastWriteTime;
  if (!force && lw.dwLowDateTime == gLastWrite.dwLowDateTime && lw.dwHighDateTime == gLastWrite.dwHighDateTime)
  {
    LogDiagMessage(LOG_HEADER" [Restart] not needed, file not changed.\n"); 
    return;
  }
  gLastWrite = lw;    

  // delete old FSM pointers (but store the FSM names)
  Container::LookUp<VBSFusion::NetworkID, Container::Vector<Common::String>* > dump(Common::Interface::getGlobalAllocator());

  UnitFSMLookup::Iterator iter = gUnitFSMLookup->begin();
  UnitFSMLookup::Iterator iterEnd = gUnitFSMLookup->end();

  for(; iter!= iterEnd; ++iter)
  {
    Container::Vector<Common::String>* fsmNames = new Container::Vector<Common::String>(Common::Interface::getGlobalAllocator());
    dump.add(iter->first, fsmNames);

    Container::Vector<FSMInstance *>::Iterator innerIter = (*iter).second->begin();
    Container::Vector<FSMInstance *>::Iterator innerIterEnd = (*iter).second->end();

    for(; innerIter!= innerIterEnd; ++innerIter)
    {
      fsmNames->pushBack((*innerIter)->getRootFSM()->getName());
      delete (*innerIter);
    }

    (*iter).second->clear();
  }

  //unload and reload the FSMSet
  FSMManager* fsmManager= Control::Interface::getFSMManager();
  fsmManager->unloadFSMSet(gFsmSet);

  gFsmSet = fsmManager->loadFSMSet(FSM_SET_NAME);

  if (gMissionStarted)
  {
    //restore FSMInstance from names
    Container::LookUp<VBSFusion::NetworkID, Container::Vector<Common::String>* >::Iterator nameIter = dump.begin();
    Container::LookUp<VBSFusion::NetworkID, Container::Vector<Common::String>* >::Iterator nameIterEnd = dump.end();

    for(; nameIter != nameIterEnd; ++ nameIter)
    {
      Container::Vector<FSMInstance *>* insertVec = gUnitFSMLookup->find(nameIter->first)->second;

      X_ASSERT_DBG(insertVec->empty());


      Container::Vector<Common::String>::Iterator innerIter = nameIter->second->begin();
      Container::Vector<Common::String>::Iterator innerIterEnd = nameIter->second->end();

      for(; innerIter!= innerIterEnd; ++innerIter)
      {
        FSM* fsm = gFsmSet->getFSM(*innerIter);
        if (fsm)
        {
          registerDefaultFunctions(fsm);
          FSMInstance* fsmInstance = fsm->createFSMInstance();
          insertVec->pushBack(fsmInstance);
        }
      }

      nameIter->second->clear();
      delete nameIter->second;
    }
  }
}
// Searches for FSM instance of given FSM in vector of instances
// Returns iterator of the instance in the vector if found; returns NULL otherwise
static FSMInstancesIterator FindFSMInstanceByFSM(Container::Vector<FSMInstance *> &instances, FSM *fsm)
{
  if (fsm == NULL)
    return instances.end();

  int i=0;
  for (FSMInstancesIterator iterator = instances.begin(); iterator != instances.end(); iterator++)
  {
    if ((*iterator)->getCurrentFSM() == fsm)
      return iterator;
    i++;
  }

  return instances.end();
}

static FSMInstance *CreateFSMInstance(FSM *fsm, const char *instanceName)
{
  FSMInstance *instance = fsm->createFSMInstance
    (
    instanceName
#if _DIAG
    ,true
#endif
    );
  if (!instance)
  {
    LogDiagMessage(LOG_HEADER"Unable to create instance of FSM '%s'.\n", instanceName);
  }

  return instance;
}

VBSPLUGIN_EXPORT void WINAPI SetFSM(const char * unitName, long long id[3], const char* fsmName)
{
  if (!gIsControlInitialized)
      return;

  if (!gMissionStarted)
  {
    UnitFSMInfo info;
    info.unitName = unitName;
    memcpy(info.id, id, 3*sizeof(int64));
    info.fsmName = fsmName;
    gPreMissionInits.push_back(info);
    return;
  }

  // calling of SetFSM(NULL,"*") causes restart of all FSMs
  if (fsmName && fsmName[0]=='*' && fsmName[1]=='\0') // 
    return RestartAllFSM(false);

  FSM *fsm = gFsmSet->getFSM(fsmName);
  if (!fsm)
  {
    LogDiagMessage(LOG_HEADER"Unable to find FSM of name '%s'.\n", fsmName);
    return;
  }
  
  registerDefaultFunctions(fsm);
  
  VBSFusion::NetworkID nid = NetID(id);
  UnitFSMLookupIterator unitFSMIterator = gUnitFSMLookup->find(nid);

  // No FSM yet for the given unit
  if (unitFSMIterator == gUnitFSMLookup->end())
  {
    FSMInstance *instance = CreateFSMInstance(fsm, unitName);
    if (instance)
    {
      FSMInstanceVariable fsmVar = instance->getVariable("unit", "VBS2");
      XAIT::Common::String unitIdXait(nid.getNetworkIDString().c_str());
      fsmVar.setValue(unitIdXait);

      UnitFSMLookupIterator newItem = gUnitFSMLookup->add(nid, new Container::Vector<FSMInstance *>(Common::Interface::getGlobalAllocator()));
      (*newItem).second->pushBack(instance);
      instance->registerStaticFunction("LogDiagMessage", &LogMessage);
      instance->start();
      // New FSM added
    }
    return;
  }

  // Do not allow two instances of the same FSM for one unit
  Container::Vector<FSMInstance *> *instances  = unitFSMIterator != gUnitFSMLookup->end() ? (*unitFSMIterator).second : NULL;
  if (instances && FindFSMInstanceByFSM(*instances, fsm) != instances->end())
  {
    LogDiagMessage(LOG_HEADER" [SetFSM] %s already running.\n",fsmName);
    return;
  }

  // Unit already has some FSMs assigned but not this one yet, let's assign it then
  FSMInstance *instance = CreateFSMInstance(fsm, unitName);
  if (instance)
  {
    (*unitFSMIterator).second->pushBack(instance);
    instance->start();
  }

  return;
};

// Functions removing FSM of given name from unit identified by given id
VBSPLUGIN_EXPORT void WINAPI RemoveFSM(int64 id[3], const char* fsmName)
{
  //if (!XaitControlLoaded)
  //  return;

  FSM *fsm = gFsmSet->getFSM(fsmName);
  if (!fsm)
  {
    LogDiagMessage(LOG_HEADER"Unable to find FSM of name '%s'.\n", fsmName);
    return;
  }

  VBSFusion::NetworkID nid = NetID(id);

  UnitFSMLookupIterator unitFSMIterator = gUnitFSMLookup->find(nid);
  if (unitFSMIterator != gUnitFSMLookup->end())
    return;

  Container::Vector<FSMInstance *> *instances  = unitFSMIterator != gUnitFSMLookup->end() ? (*unitFSMIterator).second : NULL;
  if (instances)
  {
    FSMInstancesIterator instance = FindFSMInstanceByFSM(*instances, fsm);
    if (instance != instances->end())
      (*unitFSMIterator).second->erase(instance);
  }

  return;
}
#pragma endregion FSMMaintenance

#pragma region Deinit
//! Function delay loading xaitConrtrol dlls
bool LoadXaitmentDlls()
{
	//Load xaitCommon dll
	{
		XaitCommonHModule = LoadLibrary(DLL_DIRECTORY"\\"XAITCOMMON_DLL);
		if (XaitCommonHModule == NULL)
		{
			LogDiagMessage(LOG_HEADER"Library '%s' not found.\n", XAITCOMMON_DLL);
			return false;
		}
	}

	//Load xaitControl dll
	{
		XaitControlHModule = LoadLibrary(DLL_DIRECTORY"\\"XAITCONTROL_DLL);
		if (XaitControlHModule == NULL)
		{
			LogDiagMessage(LOG_HEADER"Library '%s' not found.\n", XAITCONTROL_DLL);
			FreeLibrary(XaitCommonHModule);
			return false;
		}
	}

  gIsControlInitialized = true;
	return true;
}

void Init() 
{
#if DELAYED_DLLS
  if (!LoadXaitmentDlls())
    return;
#endif

  if(gIsControlInitialized)
    return;

  //create callbacks and initialize xaitControl
  Common::Callback::FileIOCallback::Ptr	ioCallback(new Common::Callback::FileIOCallbackWin32());
  Common::Callback::MathCallback::Ptr		mathCallback(new Common::Callback::MathCallback());

  Common::Interface::initLibrary();
  Common::Network::InitializeNetwork();
  Control::Interface::initLibrary(ioCallback, mathCallback);  

  gUnitFSMLookup = new UnitFSMLookup(Common::Interface::getGlobalAllocator());

  //load the FSMSet
  FSMManager* fsmManager= Control::Interface::getFSMManager();
  gFsmSet  = fsmManager->loadFSMSet(FSM_SET_NAME);  
  if (gFsmSet)
    LogDiagMessage(LOG_HEADER" %s loaded\n",FSM_SET_NAME);
  else
    LogDiagMessage(LOG_HEADER" %s load failed\n",FSM_SET_NAME);

  WIN32_FILE_ATTRIBUTE_DATA fileInfo;
  GetFileAttributesEx(FSM_SET_NAME, GetFileExInfoStandard, &fileInfo);
  gLastWrite = fileInfo.ftLastWriteTime;

  gIsControlInitialized = true;
}
#pragma endregion Init

#pragma region Deinit
void FreeXaitmentDlls()
{
  if (XaitControlHModule != NULL)
    FreeLibrary(XaitControlHModule);
  if (XaitCommonHModule != NULL)
    FreeLibrary(XaitCommonHModule);

  XaitControlHModule = NULL;
  XaitCommonHModule = NULL;
}

void Close()
{
  if(!gIsControlInitialized)
    return;

  gIsControlInitialized = false;

  //TODO: delete ptr to vectors too
  delete gUnitFSMLookup;

  // release all FSMs and all Units
  Control::Interface::closeLibrary();
  Common::Network::CloseNetwork();
  Common::Interface::closeLibrary();

#if !DELAYED_DLLS
  FreeXaitmentDlls();
#endif
};
#pragma endregion Deinit

#pragma endregion ServiceCode

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  // Do nothing when the libraries were not loaded
  if (!gIsControlInitialized)
    return;

  // Call the time update for FSMs
  FSMManager* fsmManager= Control::Interface::getFSMManager();
  fsmManager->update(gGameTime * 1000.0f);
  gGameTime += deltaT;
  
#if _DIAG
  // Show diagnostics about FSM and state for each unit in debug console and in VBS2 above unit head.
  ShowFSMDiagnostics(deltaT);
#endif
}

VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters *params)
{
  Init();
  return 0;
}

VBSPLUGIN_EXPORT void WINAPI OnUnload()
{
  Close();
}

VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  gGameTime = 0;
  FSMManager* fsmManager= Control::Interface::getFSMManager(); 
  fsmManager->setGametime( 0 );

  void RegisterFusionEvents();
  RegisterFusionEvents();
  
  // Reload FSMs if needed.
  RestartAllFSM(false);

  gMissionStarted = true;

  // Set all FSMs that were requested to be set before the mission started (like from initialization statements)
  UnitPreMissionInitsIterator iter = gPreMissionInits.begin();
  UnitPreMissionInitsIterator iterEnd = gPreMissionInits.end();

  for (; iter<iterEnd; iter++)
  {
    UnitFSMInfo &info = *(iter);
    SetFSM(info.unitName.c_str(), info.id, info.fsmName.c_str());
  }
  gPreMissionInits.clear();
}

VBSPLUGIN_EXPORT void WINAPI OnMissionEnd()
{
  ClearAllFSM();
  gMissionStarted = false;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
  switch(ul_reason_for_call)
  {
  case DLL_PROCESS_ATTACH:
    Init();
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}