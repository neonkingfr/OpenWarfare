#include "OpenGLDraw.h"

#define SHADER_PATH "OpenGLShader.hlsl"

OpenGLDraw::OpenGLDraw()
{
	m_hDC = NULL;
	m_hRC = NULL;
	m_hWnd = NULL;
	m_hInst = NULL;

	m_glD3DHandle = NULL;
	m_glD3DSharedColorHandle = NULL;

	glGenFramebuffers = NULL;
	glBindFramebuffer = NULL;
	glGenRenderbuffers = NULL;
	glFramebufferRenderbuffer = NULL;
	glFramebufferTexture2D = NULL;
	wglDXOpenDeviceNV = NULL;
	wglDXCloseDeviceNV = NULL;
	wglDXRegisterObjectNV = NULL;
	wglDXUnregisterObjectNV = NULL;
	wglDXLockObjectsNV = NULL;
	wglDXUnlockObjectsNV = NULL;

	m_dxColorBuffer = NULL;
	m_dxColorTexture = NULL;
	m_pRenderSurface = NULL;
	m_pRenderTexture = NULL;
	m_device = NULL;
	m_direct3D = NULL;

	m_ps = NULL;
	m_vs = NULL;
	m_vertDecl = NULL;
}

OpenGLDraw::~OpenGLDraw()
{
	ReleaseInterop();
	ReleaseGL();
	CleanUpDX();
};


bool OpenGLDraw::InitDX()
{
	HRESULT result;
	D3DMULTISAMPLE_TYPE sampleType = D3DMULTISAMPLE_NONE;

	result = m_device->GetDirect3D(&m_direct3D);

	if (result == S_OK)
	{
		result = m_direct3D->GetAdapterDisplayMode( D3DADAPTER_DEFAULT, &d3ddm );
		if(result != S_OK)
		{
			MessageBox(NULL, "Could not get the adapter display mode.","DX ERROR", MB_OK|MB_ICONEXCLAMATION);
			return false;
		}
	}

	// Verify that the device will actually support our desired formats
	VerifyDeviceCapabilities(d3ddm.Format, D3DFMT_D24S8);

	if(m_direct3D) m_direct3D = NULL;

	LPD3DXBUFFER codevs, codeps;
	LPD3DXBUFFER err = NULL;

	// compile pixel shader
	if(D3DXCompileShaderFromFile(SHADER_PATH, NULL, NULL, "psmain", "ps_3_0", NULL, &codeps, &err, NULL) != D3D_OK)
	{
		if(err) LogF("Error compiling pixel shader: %s", (char*)err->GetBufferPointer());
		else	LogF("Error compiling pixel shader (file not found?)");
		MessageBox(NULL, "Error compiling pixel shader", "SHADER ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	m_device->CreatePixelShader((DWORD*) codeps->GetBufferPointer(), &m_ps);
	codeps->Release();

	// compile vertex shader
	if(D3DXCompileShaderFromFile(SHADER_PATH, NULL, NULL, "vsui", "vs_3_0", NULL, &codevs, &err, NULL) != D3D_OK)
	{
		if(err) LogF("Error compiling pixel shader: %s", (char*)err->GetBufferPointer());
		else	LogF("Error compiling pixel shader (file not found?)");
		MessageBox(NULL, "Error compiling vertex shader", "SHADER ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	m_device->CreateVertexShader((DWORD*) codevs->GetBufferPointer(), &m_vs);
	codevs->Release();

	// create vertex declaration
	D3DVERTEXELEMENT9 decl[] = {	{0,
									0,
									D3DDECLTYPE_FLOAT3,
									D3DDECLMETHOD_DEFAULT,
									D3DDECLUSAGE_POSITION,
									0},
									{0,
									12,
									D3DDECLTYPE_FLOAT2,
									D3DDECLMETHOD_DEFAULT,
									D3DDECLUSAGE_TEXCOORD,
									0},
									D3DDECL_END()
								};

	if(m_device->CreateVertexDeclaration(decl, &m_vertDecl))
	{
		MessageBox(NULL, "CreateVertexDeclaration failed !", "SHADER ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}

	return true;
}

bool OpenGLDraw::CreateGLContext(int width, int height, int bits)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		bits,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		8,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		16,											// 16Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	if (!(m_hDC=GetDC(m_hWnd)))							// Did We Get A Device Context?
	{
		ReleaseGL();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(m_hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		ReleaseGL();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if(!SetPixelFormat(m_hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		ReleaseGL();							// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(m_hRC=wglCreateContext(m_hDC)))				// Are We Able To Get A Rendering Context?
	{
		ReleaseGL();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if(!wglMakeCurrent(m_hDC,m_hRC))					// Try To Activate The Rendering Context
	{
		ReleaseGL();								// Reset The Display
		MessageBox(NULL,"Can't Activate The GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (glewInit() != GL_NO_ERROR)									// Initialize Our Newly Created GL Window
	{
		ReleaseGL();								// Reset The Display
		MessageBox(NULL,"Initialization Failed.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	return TRUE;
}

bool OpenGLDraw::InitializeInterop(IDirect3DDevice9 *in_device)
{
	GLenum error;

	if(LoadWglFunctions() == false)
		return false;

	if(m_glD3DHandle == NULL) m_glD3DHandle = wglDXOpenDeviceNV(in_device);
	if(m_glD3DHandle == NULL)
	{
		MessageBox(NULL, "Could not create the GL <-> DirectX Interop.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}

	// Initialize the frame buffer object
	glGenFramebuffers(1, &m_glFbo);
	error = glGetError();
	if(error != GL_NO_ERROR)
	{
		MessageBox(NULL, "Could not create the Framebuffer object.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}

	// Initialize the color and depth buffers
	glGenTextures(1, &m_glColorBuffer);
	error = glGetError();
	if(error != GL_NO_ERROR)
	{
		MessageBox(NULL, "Could not create colour buffer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}

	return true;
}

bool OpenGLDraw::LoadWglFunctions()
{
	// Load the extension functions
	glGenFramebuffers = (PFNGLGENFRAMEBUFFERSPROC)wglGetProcAddress("glGenFramebuffers");
	if(glGenFramebuffers == NULL)
	{
		MessageBox(NULL, "Could not find the glGenFramebuffers function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	glBindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC)wglGetProcAddress("glBindFramebuffer");
	if(glBindFramebuffer == NULL)
	{
		MessageBox(NULL, "Could not find the glBindFramebuffer function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	glGenRenderbuffers = (PFNGLGENRENDERBUFFERSPROC)wglGetProcAddress("glGenRenderbuffers");
	if(glGenRenderbuffers == NULL)
	{
		MessageBox(NULL, "Could not find the glGenRenderbuffers function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	glFramebufferRenderbuffer = (PFNGLFRAMEBUFFERRENDERBUFFERPROC)wglGetProcAddress("glFramebufferRenderbuffer");
	if(glFramebufferRenderbuffer == NULL)
	{
		MessageBox(NULL, "Could not find the glFramebufferRenderbuffer function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	glFramebufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DPROC)wglGetProcAddress("glFramebufferTexture2D");
	if(glFramebufferTexture2D == NULL)
	{
		MessageBox(NULL, "Could not find the glFramebufferTexture2D function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	wglDXOpenDeviceNV = (PFNWGLDXOPENDEVICENVPROC)wglGetProcAddress("wglDXOpenDeviceNV");
	if(wglDXOpenDeviceNV == NULL)
	{
		MessageBox(NULL, "Could not find the wglDXOpenDeviceNV function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	wglDXCloseDeviceNV = (PFNWGLDXCLOSEDEVICENVPROC)wglGetProcAddress("wglDXCloseDeviceNV");
	if(wglDXCloseDeviceNV == NULL)
	{
		MessageBox(NULL, "Could not find the wglDXCloseDeviceNV function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	wglDXRegisterObjectNV = (PFNWGLDXREGISTEROBJECTNVPROC)wglGetProcAddress("wglDXRegisterObjectNV");
	if(wglDXRegisterObjectNV == NULL)
	{
		MessageBox(NULL, "Could not find the wglDXRegisterObjectNV function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	wglDXUnregisterObjectNV = (PFNWGLDXUNREGISTEROBJECTNVPROC)wglGetProcAddress("wglDXUnregisterObjectNV");
	if(wglDXUnregisterObjectNV == NULL)
	{
		MessageBox(NULL, "Could not find the wglDXUnregisterObjectNV function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	wglDXLockObjectsNV = (PFNWGLDXLOCKOBJECTSNVPROC)wglGetProcAddress("wglDXLockObjectsNV");
	if(wglDXLockObjectsNV == NULL)
	{
		MessageBox(NULL, "Could not find the wglDXLockObjectsNV function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	wglDXUnlockObjectsNV = (PFNWGLDXUNLOCKOBJECTSNVPROC)wglGetProcAddress("wglDXUnlockObjectsNV");
	if(wglDXUnlockObjectsNV == NULL)
	{
		MessageBox(NULL, "Could not find the wglDXUnlockObjectsNV function pointer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}
	return true;
}

bool OpenGLDraw::CreateResources()
{
	// Create the rendering texture and surface for openGL interop
	HRESULT result;
	result = m_device->CreateTexture(m_wWidth, m_wHeight, 0, 0, d3ddm.Format, D3DPOOL_DEFAULT, &m_dxColorTexture, NULL);
	if(result != S_OK)
	{
		MessageBox(NULL, "Could not create the DirectX Colour texture.","DX ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}
	if(m_dxColorTexture->GetSurfaceLevel(0, &m_dxColorBuffer) != D3D_OK)
	{
		MessageBox(NULL, "Could not get the DirectX Colour Surface.","DX ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}

	// create texture and surface that will store the back buffer
	result = m_device->CreateTexture(m_wWidth, m_wHeight, 1, D3DUSAGE_RENDERTARGET, d3ddm.Format, D3DPOOL_DEFAULT, &m_pRenderTexture, NULL);
	if(result != S_OK)
	{
		MessageBox(NULL, "Could not create the DirectX rendering texture.","DX ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}
	if(m_pRenderTexture->GetSurfaceLevel(0, &m_pRenderSurface) != D3D_OK)
	{
		MessageBox(NULL, "Could not get the DirectX rendering surface.","DX ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}

	// Register the buffers
	if(RegisterDXColorBuffer(m_dxColorTexture, GL_TEXTURE_2D) == false)
	{
		MessageBox(NULL, "Register colour Buffer Failed.", "DX REGISTER ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
}

/// <summary>
/// Registers the left color buffer with opengl.  You must ensure that the texture type
/// is compatible with the texture type used when creating the directx device.
/// </summary>
bool OpenGLDraw::RegisterDXColorBuffer(void * in_buffer, GLenum in_textureType)
{
	// Register the directx color buffer with OpenGL
	m_glD3DSharedColorHandle = wglDXRegisterObjectNV(m_glD3DHandle, in_buffer, m_glColorBuffer, in_textureType, WGL_ACCESS_READ_WRITE_NV);
	if(m_glD3DSharedColorHandle == NULL)
	{
		MessageBox(NULL, "Could not register colour buffer.", "INTEROP ERROR", MB_OK | MB_ICONINFORMATION);
		return false;
	}

	// Bind the color buffer to the FBO
	glBindFramebuffer(GL_FRAMEBUFFER, m_glFbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, in_textureType, m_glColorBuffer, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return true;
}


void OpenGLDraw::BeginGLDraw()
{
	// Lock for opengl
	if(m_glD3DSharedColorHandle)
		wglDXLockObjectsNV(m_glD3DHandle, 1, &m_glD3DSharedColorHandle);

	// Bind to the frame buffer
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_glFbo);
}

void OpenGLDraw::EndGLDraw()
{
	// Unbind the frame buffer
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	// Unlock for opengl
	if(m_glD3DSharedColorHandle)
		wglDXUnlockObjectsNV(m_glD3DHandle, 1, &m_glD3DSharedColorHandle);
}

int	OpenGLDraw::DrawGLScene(GLvoid)
{
//	glLoadIdentity();									// Reset The Current Modelview Matrix
//	glTranslatef(-1.5f,0.0f,-6.0f);						// Move Left 1.5 Units And Into The Screen 6.0
	glEnable(GL_ALPHA_TEST);            
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_BLEND);             
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_TRIANGLES);
	glColor4f(1.0f, 0.0f, 0.0f, 0.3f);
	glVertex3f( 0.0f, 0.2f, 0.0f);					// Top
	glVertex3f(-0.2f,-0.2f, 0.0f);					// Bottom Left
	glVertex3f( 0.2f,-0.2f, 0.0f);					// Bottom Right
	glEnd();
	return TRUE;
}

GLvoid OpenGLDraw::ReleaseGL(GLvoid)
{
	if (m_hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(m_hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		m_hRC=NULL;										// Set RC To NULL
	}

	if (m_hDC && !ReleaseDC(m_hWnd,m_hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		m_hDC=NULL;										// Set DC To NULL
	}
}

GLvoid	OpenGLDraw::ReleaseInterop(GLvoid)
{
	if(m_glD3DHandle)
	{
		// Unshare all the resources
		if (m_glD3DSharedColorHandle)
		{
			BOOL result = wglDXUnregisterObjectNV(m_glD3DHandle, m_glD3DSharedColorHandle);			
			m_glD3DSharedColorHandle = NULL;
		}
		wglDXCloseDeviceNV(m_glD3DHandle);
		m_glD3DHandle = NULL;
	}

	glGenFramebuffers = NULL;
	glBindFramebuffer = NULL;
	glGenRenderbuffers = NULL;
	glFramebufferRenderbuffer = NULL;
	glFramebufferTexture2D = NULL;
	wglDXOpenDeviceNV = NULL;
	wglDXCloseDeviceNV = NULL;
	wglDXRegisterObjectNV = NULL;
	wglDXUnregisterObjectNV = NULL;
	wglDXLockObjectsNV = NULL;
	wglDXUnlockObjectsNV = NULL;
}

void OpenGLDraw::CleanUpDX()
{
	if(m_vertDecl)
		m_vertDecl->Release();
	if(m_ps)
		m_ps->Release();
	if(m_vs)
		m_vs->Release();
	if (m_dxColorTexture) 
		m_dxColorTexture->Release();
	if (m_dxColorBuffer) 
		m_dxColorBuffer->Release();
	if(m_pRenderTexture)
		m_pRenderTexture->Release();
	if(m_pRenderSurface)
		m_pRenderSurface->Release();
	if (m_device)
		m_device->Release();
	if (m_direct3D)
		m_direct3D->Release();

	m_vertDecl = NULL;
	m_ps = NULL;
	m_vs = NULL;
	m_dxColorTexture = NULL;
	m_dxColorBuffer = NULL;
	m_pRenderTexture = NULL;
	m_pRenderSurface = NULL;
	m_device = NULL;
	m_direct3D = NULL;
}

bool OpenGLDraw::VerifyDeviceCapabilities(D3DFORMAT in_desiredColorFormat, D3DFORMAT in_desiredDepthFormat)
{
	HRESULT result;
	D3DCAPS9 d3dCaps;

	// Make sure the device can support the requested formats
	result =  m_direct3D->CheckDeviceFormat(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, in_desiredColorFormat, 
											D3DUSAGE_DEPTHSTENCIL, D3DRTYPE_SURFACE, in_desiredDepthFormat);
	if(result != S_OK)
	{
		MessageBox(NULL, "Graphics device does not support required display formats.","DX ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}

	// Get the device capabilities
	result = m_direct3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &d3dCaps);
	if(result != S_OK)
	{
		MessageBox(NULL, "Could not retrieve DirectX graphics device capabilities.","DX ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}

	// Check to make sure we have hardware vertex processing
	if( d3dCaps.VertexProcessingCaps == 0 )
	{
		MessageBox(NULL, "Graphics device does not support hardware vertex processing." ,"DX ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}

	return true;
}

void OpenGLDraw::SetDefaultState(VBS2CBInterface *d3dd)
{
	// Set D3D device properties to their default state
	// This is to ensure things are in a predictable state when coming from engine rendering code
	const float zerof = 0.0f;
	const float onef = 1.0f;
#define ZEROf	*((DWORD*) (&zerof))
#define ONEf	*((DWORD*) (&onef))

	d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	d3dd->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	d3dd->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
	d3dd->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	d3dd->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_LASTPIXEL, TRUE);
	d3dd->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	d3dd->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
	d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	d3dd->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
	d3dd->SetRenderState(D3DRS_ALPHAREF, 0);
	d3dd->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
	d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_FOGENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_FOGCOLOR, 0);
	d3dd->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
	d3dd->SetRenderState(D3DRS_FOGSTART, ZEROf);
	d3dd->SetRenderState(D3DRS_FOGEND, ONEf);
	d3dd->SetRenderState(D3DRS_FOGDENSITY, ONEf);
	d3dd->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
	d3dd->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
	d3dd->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
	d3dd->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
	d3dd->SetRenderState(D3DRS_STENCILREF, 0);
	d3dd->SetRenderState(D3DRS_STENCILMASK, 0xffffffff);
	d3dd->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
	d3dd->SetRenderState(D3DRS_TEXTUREFACTOR, 0xffffffff);
	d3dd->SetRenderState(D3DRS_WRAP0, 0);
	d3dd->SetRenderState(D3DRS_WRAP1, 0);
	d3dd->SetRenderState(D3DRS_WRAP2, 0);
	d3dd->SetRenderState(D3DRS_WRAP3, 0);
	d3dd->SetRenderState(D3DRS_WRAP4, 0);
	d3dd->SetRenderState(D3DRS_WRAP5, 0);
	d3dd->SetRenderState(D3DRS_WRAP6, 0);
	d3dd->SetRenderState(D3DRS_WRAP7, 0);
	d3dd->SetRenderState(D3DRS_CLIPPING, TRUE);
	d3dd->SetRenderState(D3DRS_AMBIENT, 0);
	d3dd->SetRenderState(D3DRS_LIGHTING, TRUE);
	d3dd->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
	d3dd->SetRenderState(D3DRS_COLORVERTEX, TRUE);
	d3dd->SetRenderState(D3DRS_LOCALVIEWER, TRUE);
	d3dd->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
	d3dd->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
	d3dd->SetRenderState(D3DRS_SPECULARMATERIALSOURCE, D3DMCS_COLOR2);
	d3dd->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
	d3dd->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_MATERIAL);
	d3dd->SetRenderState(D3DRS_CLIPPLANEENABLE, 0);
	d3dd->SetRenderState(D3DRS_POINTSIZE_MIN, ONEf);
	d3dd->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
	d3dd->SetRenderState(D3DRS_MULTISAMPLEMASK, 0xffffffff);
	d3dd->SetRenderState(D3DRS_PATCHEDGESTYLE, D3DPATCHEDGE_DISCRETE);
	d3dd->SetRenderState(D3DRS_POINTSIZE_MAX, ONEf);
	d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000f);
	d3dd->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	d3dd->SetRenderState(D3DRS_POSITIONDEGREE, D3DDEGREE_CUBIC);
	d3dd->SetRenderState(D3DRS_NORMALDEGREE, D3DDEGREE_LINEAR);
	d3dd->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, 0);
	d3dd->SetRenderState(D3DRS_MINTESSELLATIONLEVEL, ONEf);
	d3dd->SetRenderState(D3DRS_MAXTESSELLATIONLEVEL, ONEf);
	d3dd->SetRenderState(D3DRS_ADAPTIVETESS_X, ZEROf);
	d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Y, ZEROf);
	d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Z, ONEf);
	d3dd->SetRenderState(D3DRS_ADAPTIVETESS_W, ZEROf);
	d3dd->SetRenderState(D3DRS_ENABLEADAPTIVETESSELLATION, FALSE);
	d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
	d3dd->SetRenderState(D3DRS_CCW_STENCILFAIL, D3DSTENCILOP_KEEP);
	d3dd->SetRenderState(D3DRS_CCW_STENCILZFAIL, D3DSTENCILOP_KEEP);
	d3dd->SetRenderState(D3DRS_CCW_STENCILPASS, D3DSTENCILOP_KEEP);
	d3dd->SetRenderState(D3DRS_CCW_STENCILFUNC, D3DCMP_ALWAYS);
	d3dd->SetRenderState(D3DRS_COLORWRITEENABLE1, 0x0000000f);
	d3dd->SetRenderState(D3DRS_COLORWRITEENABLE2, 0x0000000f);
	d3dd->SetRenderState(D3DRS_COLORWRITEENABLE3, 0x0000000f);
	d3dd->SetRenderState(D3DRS_BLENDFACTOR, 0xffffffff);
	d3dd->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);
	d3dd->SetRenderState(D3DRS_DEPTHBIAS, 0);
	d3dd->SetRenderState(D3DRS_WRAP8, 0);
	d3dd->SetRenderState(D3DRS_WRAP9, 0);
	d3dd->SetRenderState(D3DRS_WRAP10, 0);
	d3dd->SetRenderState(D3DRS_WRAP11, 0);
	d3dd->SetRenderState(D3DRS_WRAP12, 0);
	d3dd->SetRenderState(D3DRS_WRAP13, 0);
	d3dd->SetRenderState(D3DRS_WRAP14, 0);
	d3dd->SetRenderState(D3DRS_WRAP15, 0);
	d3dd->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
	d3dd->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_ZERO);
	d3dd->SetRenderState(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);
	d3dd->SetRenderState(D3DRS_DITHERENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_VERTEXBLEND, D3DVBF_DISABLE);
	d3dd->SetRenderState(D3DRS_POINTSIZE, ONEf);
	d3dd->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_POINTSCALE_A, ONEf);
	d3dd->SetRenderState(D3DRS_POINTSCALE_B, ZEROf);
	d3dd->SetRenderState(D3DRS_POINTSCALE_C, ZEROf);
	d3dd->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE);
	d3dd->SetRenderState(D3DRS_TWEENFACTOR, ZEROf);
	d3dd->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, FALSE);

	for(int i=0;i<8;i++) {
		d3dd->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		d3dd->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		d3dd->SetSamplerState(i, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
		d3dd->SetSamplerState(i, D3DSAMP_MIPMAPLODBIAS, 0);
		d3dd->SetSamplerState(i, D3DSAMP_MAXMIPLEVEL, 0);			
		d3dd->SetSamplerState(i, D3DSAMP_MAXANISOTROPY, 1);
		d3dd->SetSamplerState(i, D3DSAMP_SRGBTEXTURE, 0);			
		d3dd->SetSamplerState(i, D3DSAMP_ELEMENTINDEX, 0);
		d3dd->SetSamplerState(i, D3DSAMP_DMAPOFFSET, 0);			
		d3dd->SetSamplerState(i, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		d3dd->SetSamplerState(i, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		d3dd->SetSamplerState(i, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);

		d3dd->SetTexture(i, NULL);
	}

#undef ZEROf
#undef ONEf
}
