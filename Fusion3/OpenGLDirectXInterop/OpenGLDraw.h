#ifndef __OPENGLDRAW_H__
#define __OPENGLDRAW_H__

#include <Windows.h>
#include <d3d9.h>
#include <gl/glew.h>

#include "OpenGLDirectXExample.h"

typedef HANDLE (WINAPI * PFNWGLDXOPENDEVICENVPROC) (void *dxDevice);
typedef BOOL (WINAPI * PFNWGLDXCLOSEDEVICENVPROC) (HANDLE hDevice);
typedef HANDLE (WINAPI * PFNWGLDXREGISTEROBJECTNVPROC) (HANDLE hDevice, void *dxObject, GLuint name, GLenum type, GLenum access);
typedef BOOL (WINAPI * PFNWGLDXUNREGISTEROBJECTNVPROC) (HANDLE hDevice, HANDLE hObject);
typedef BOOL (WINAPI * PFNWGLDXLOCKOBJECTSNVPROC) (HANDLE hDevice, GLint count, HANDLE *hObjects);
typedef BOOL (WINAPI * PFNWGLDXUNLOCKOBJECTSNVPROC) (HANDLE hDevice, GLint count, HANDLE *hObjects);

#define WGL_ACCESS_READ_ONLY_NV             0x0000
#define WGL_ACCESS_READ_WRITE_NV            0x0001
#define WGL_ACCESS_WRITE_DISCARD_NV         0x0002

class OpenGLDraw
{
public:
	OpenGLDraw();
	~OpenGLDraw();

	bool	InitDX();
	void	CleanUpDX();
	bool	InitializeInterop(IDirect3DDevice9 *in_device);
	GLvoid	ReleaseInterop(GLvoid);
	bool	CreateGLContext(int width, int height, int bits);
	GLvoid	ReleaseGL(GLvoid);
	
	int		DrawGLScene(GLvoid);
	bool	LoadWglFunctions();
	bool	VerifyDeviceCapabilities(D3DFORMAT in_desiredColorFormat, D3DFORMAT in_desiredDepthFormat);

	bool	CreateResources();
	bool	RegisterDXColorBuffer(void * in_buffer, GLenum in_textureType);
	void	BeginGLDraw();
	void	EndGLDraw();

	void	SetDefaultState(VBS2CBInterface *d3dd);
	

	/* Getters and Setters */
	void	SetHWND(HWND hWnd)							{m_hWnd = hWnd;};
	void	SetHINST(HINSTANCE hInst)					{m_hInst = hInst;};
	void	SetDirect3D(IDirect3D9 *d3d)				{m_direct3D = d3d;};
	void	SetDirectDevice(IDirect3DDevice9 *device)	{m_device = device;};
	void	SetWindowWidth(int width)					{m_wWidth = width;};
	void	SetWindowHeight(int height)					{m_wHeight = height;};

	void	ReleaseDirect3D()							{m_direct3D = NULL;};
	void	ReleaseDevice()								{m_device = NULL;};
	void	ReleaseHWND()								{m_hWnd = NULL;};
	void	ReleaseHINST()								{m_hInst = NULL;};

	HDC		GetHDC() const								{return m_hDC;};
	HGLRC	GetHGLRC() const							{return m_hRC;};
	IDirect3DTexture9*	GetClourTexture()				{return m_dxColorTexture;};
	IDirect3DSurface9*	GetColourBuffer()				{return m_dxColorBuffer;};
	IDirect3DTexture9*	GetRenderTexture()				{return m_pRenderTexture;};
	IDirect3DSurface9*  GetRenderSurface()				{return m_pRenderSurface;};
	IDirect3DPixelShader9*	GetPixelShader()			{return m_ps;};		
	IDirect3DVertexShader9* GetVertexShader()			{return m_vs;};
	IDirect3DVertexDeclaration9* GetVertexDeclaration() {return m_vertDecl;};
	
	int		GetWindowWidth()			const			{return m_wWidth;};
	int		GetWindowHeight()			const			{return m_wHeight;};

private:
	// window vars
	int		m_wHeight;
	int		m_wWidth;

	// handles
	HDC		m_hDC;			// private GDI context
	HGLRC	m_hRC;			// permanent rendering context
	HWND	m_hWnd;			// handle to window
	HINSTANCE m_hInst;		// handle to instance

	// OpenGL interop variables
	HANDLE m_glD3DHandle;
	HANDLE m_glD3DSharedColorHandle;
	GLuint m_glFbo;
	GLuint m_glColorBuffer;

	// OpenGL extension functions
	PFNGLGENFRAMEBUFFERSPROC glGenFramebuffers;
	PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer;
	PFNGLGENRENDERBUFFERSPROC glGenRenderbuffers;
	PFNGLFRAMEBUFFERRENDERBUFFERPROC glFramebufferRenderbuffer;
	PFNGLFRAMEBUFFERTEXTURE2DPROC glFramebufferTexture2D;
	PFNWGLDXOPENDEVICENVPROC wglDXOpenDeviceNV;
	PFNWGLDXCLOSEDEVICENVPROC wglDXCloseDeviceNV;
	PFNWGLDXREGISTEROBJECTNVPROC wglDXRegisterObjectNV;
	PFNWGLDXUNREGISTEROBJECTNVPROC wglDXUnregisterObjectNV;
	PFNWGLDXLOCKOBJECTSNVPROC wglDXLockObjectsNV;
	PFNWGLDXUNLOCKOBJECTSNVPROC wglDXUnlockObjectsNV;

	// DirectX variables
	IDirect3D9					*m_direct3D;
	IDirect3DDevice9			*m_device;
	D3DPRESENT_PARAMETERS		 m_d3dpp;
	IDirect3DSurface9			*m_dxColorBuffer;
	IDirect3DTexture9			*m_dxColorTexture;
	IDirect3DTexture9			*m_pRenderTexture;
	IDirect3DSurface9			*m_pRenderSurface;
	IDirect3DPixelShader9		*m_ps;
	IDirect3DVertexShader9		*m_vs;
	IDirect3DVertexDeclaration9 *m_vertDecl;
	D3DDISPLAYMODE				d3ddm;
};

#endif