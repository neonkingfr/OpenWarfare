#include "Pipe.h"

#pragma warning(disable: 4996)
#include "../../Common/NamedPipes.h"

NamedPipeClient npc;

bool OpenPipe()
{
  npc.Connect("MPMonitor");
  return npc.GetLastError()==NamedPipeClient::ALL_OK;
}

void ClosePipe()
{
  npc.Disconnect();  
}

void LogDiagMessage(const char *format, ...);
#define LOG_HEADER "[MPMonitor] "

bool UpdatePipe(const UnitMap &list)
{ 
  if (npc.GetLastError()!=NamedPipeClient::ALL_OK)
    return false;

  npc.SendInt((int)list.size()); 
  LogDiagMessage(LOG_HEADER"[PIPE] n %d\n", (int)list.size()); 
  for (UnitMap::const_iterator it = list.begin() ; it != list.end(); ++it)
  {
    npc.SendString(it->second._name);  
    npc.SendString(it->second._ip);  
    LogDiagMessage(LOG_HEADER"[PIPE] msg %s %s %s\n", it->second._name.c_str(), it->second._ip.c_str(), it->first.getNetworkIDString().c_str());     
  }
  return true;
}

