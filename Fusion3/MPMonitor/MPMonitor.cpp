#include "MPMonitor.h"
#include <stdio.h>
#include "unitInfo.h"
#include "Pipe.h"

#pragma warning(disable: 4996)

#define _BISIM_DEV_MP_MONITOR 1

#define _DIAG 1
#define LOG_HEADER "[MPMonitor] "
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugString(buffer);
#endif
}

#include <Events\EventHandlerEx.h>
#include <Util\MissionUtilities.h>
VBSFusion::EventHandlerEx eHandler;

typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);
ExecuteCommandType ExecuteCommand = NULL;
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

UnitMap allPlayers;

std::string IP2Str(const int dpnid)
{
  char res[32],cmd[32];
  sprintf_s(cmd, "getClientIP %d", dpnid);
  if (ExecuteCommand)
    ExecuteCommand(cmd, res, 256);
  return res;
}

void OnPlayerChangedFunc(const VBSFusion::DeleteEvent* event)
{
  LogDiagMessage(LOG_HEADER"OnPlayerChanged type:%s\n", event->getTypeString() );
  std::string eventTypeStr = event->getTypeString();
  const char * eventType = eventTypeStr.c_str();  
  
  if (stricmp(eventType,"selectedPlayer")==0) //or connect
  {
    int id = VBSFusion::MissionUtilities::getClientIDofPlayer(VBSFusion::Unit(event->getObjectID()));
    std::string ip = IP2Str(id);
    UnimInfoPair info = UnimInfoPair(event->getObjectID(),UnitInfo(ip,VBSFusion::MissionUtilities::getClientName(id)));
    
    if(allPlayers.insert(info).second)
    { 
      LogDiagMessage(LOG_HEADER"OnPlayerChanged %s successfully connected(JIP) from %s\n", event->getObjectName().c_str(), ip.c_str()); // OK
      UpdatePipe(allPlayers);
    }
    else
      LogDiagMessage(LOG_HEADER"OnPlayerChanged [ERROR] %s already in list\n", event->getObjectName().c_str()); // FAIL

  }
  else if (stricmp(eventType,"disconnect")==0) //or unSelectedPlayer
  {
    if (allPlayers.erase(event->getObjectID())>0)
    {
      LogDiagMessage(LOG_HEADER"OnPlayerChanged %s disconnected\n", event->getObjectName().c_str()); // OK
      UpdatePipe(allPlayers);
    }
    else
      LogDiagMessage(LOG_HEADER"OnPlayerChanged [ERROR] unable to disconnect %s\n", event->getObjectName().c_str()); // FAIL
  }
}

bool firstStep = false;
VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  firstStep = true;

  // register EH to catch JIP and disconnecting players
  eHandler.addEventHandler(&OnPlayerChangedFunc, "PlayerChanged", VBSFusion::PLAYERCHANGED);
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (!firstStep)
    return;

  vector<int> all = VBSFusion::MissionUtilities::getAllClients();
  for (std::vector<int>::iterator it = all.begin() ; it != all.end(); ++it)
  {
    VBSFusion::NetworkID id = VBSFusion::MissionUtilities::getPlayerOfClient(*it).getNetworkID();
    // this is dirty hack (due to bug in ClientToPlayer)
    if (id.getVal1()==-1 && id.getVal2()==-1 && id.getVal3()==-1)
      return; // we have to wait until all IDs are valid
  }

  allPlayers.clear();

  for (std::vector<int>::iterator it = all.begin() ; it != all.end(); ++it)
  {
    VBSFusion::Unit unit = VBSFusion::MissionUtilities::getPlayerOfClient(*it);
    std::string ip = IP2Str(*it);
    UnimInfoPair info = UnimInfoPair(unit.getNetworkID(), UnitInfo(ip,VBSFusion::MissionUtilities::getClientName(*it)));
    allPlayers.insert(info);
    LogDiagMessage(LOG_HEADER"OnPlayerChanged %s successfully connected from %s\n", VBSFusion::MissionUtilities::getClientName(*it).c_str(), ip.c_str()); // OK    
  }
  UpdatePipe(allPlayers);
  
  firstStep = false;
};


BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    OpenPipe();
    break;
  case DLL_PROCESS_DETACH:
  { 
    if (allPlayers.size()>0)
    {
      allPlayers.clear();
      UpdatePipe(allPlayers);
    }
    ClosePipe();
  } break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}
