#ifndef __PIPE_H__
#define __PIPE_H__

#include "unitInfo.h"

bool OpenPipe();
void ClosePipe();
bool UpdatePipe(const UnitMap &list);

#endif
