#include <windows.h>

#define _BISIM_DEV_EXTERNAL_POSE 1

#define VBSPLUGIN_EXPORT __declspec(dllexport)
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);


VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);

int ExecuteCmd(const char* command, char *result = NULL, int resultLength = 0);

void GetUnitPos(float& x, float& y, float&z);
void SetUnitPos(float x, float y, float z);

bool IsInternalCamera();