#ifndef __POSE_CONTROL_UNIT__
#define __POSE_CONTROL_UNIT__

#include <map>
#include "../vrpn/vrpn/vrpn_Tracker.h"

#include "data/Unit.h"
#include "mathHelper.h"

const double	PI = 3.14159265358979323846;
const double	HALF_PI = 1.57079632679489661923;
const double	R_TO_D = 180.0/PI;
const double	D_TO_R = PI/180.0;

typedef std::map<int,std::string> BoneTrackerMap;



class PoseControlUnit : public VBSFusion::Unit
{
public:
  BoneTrackerMap boneTrackerMap;

private:
  // position updates
  VBSFusion::position3D _prevUnitPos;
  float _pelvisXOffset, _pelvisPrevXOffset, _pelvisZOffset, _pelvisPrevZOffset;

  // Hand bones - uncontrolled 
  VBSFusion::Matrix4f m_lastLeftHandBoneTransformation;
  VBSFusion::Matrix4f m_lastRightHandBoneTransformation;
  VBSFusion::Matrix4f lastHeadBoneTransformation;
  VBSFusion::Matrix4f cameraTransformation;

  // Spine bone (to set correct pelvis transformation)
  VBSFusion::Matrix4f m_lastSpineBoneTransformation;

  bool _internalCamInitialized;

public:
  PoseControlUnit();

  virtual bool onModifyBone(VBSFusion::Matrix4f& mat, VBSFusion::SKELETON_TYPE index);

  // new fusion callbacks
  virtual bool onModifyGunTrans(VBSFusion::Matrix4f& mat4f);  
  virtual bool onModifyHeadTrans(VBSFusion::Matrix4f& mat4f); 
  virtual bool onModifyLookTrans(VBSFusion::Matrix4f& mat4f);
  virtual bool onModifyRelativeChange(VBSFusion::Matrix4f& mat4f);
  virtual bool onReload();
};

#endif