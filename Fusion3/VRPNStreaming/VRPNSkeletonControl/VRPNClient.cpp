
#include <string>
#include <fstream>
#include <assert.h>

#include "../vrpn/vrpn/vrpn_Connection.h" // Missing this file?  Get the latest VRPN distro at
#include "../vrpn/vrpn/vrpn_Tracker.h"    // ftp://ftp.cs.unc.edu/pub/packages/GRIP/vrpn

#include "pluginHeader.h"
#include "poseControlUnit.h"
#include "util/UnitUtilities.h"
#include "util/MissionUtilities.h"
#include "util/PoseControlUtilities.h"

#include "SkeletonDefinition.h"

#include "VRPNClient.h"

//////////////////////////////////////////////////////////////////////////
VRPNClient::VRPNClient()
: _upAxis(UP_AXIS_DEFAULT)
{
  Initialize();
}

//////////////////////////////////////////////////////////////////////////
void VRPNClient::OnSimStep()
{
  if (_pConnection)
  {
    TrackerList::iterator it;
    for (it = _trackers.begin(); it != _trackers.end(); ++it)
    {
      (*it).second._remote->mainloop();
    }
    _pConnection->mainloop();
  }
}

//////////////////////////////////////////////////////////////////////////
bool VRPNClient::Initialize()
{
  char msgbuf[1024];

  // get path to the config file
  LPTSTR  strDLLPath = new TCHAR[2048];
  GetModuleFileName(NULL, strDLLPath, 2048);  //vbs2.exe path
  OutputDebugString(strDLLPath);

  std::string pluginPath = strDLLPath;
  pluginPath = pluginPath.substr(0, (pluginPath.rfind("\\") + 1));

  sprintf(msgbuf, "[VRPNClient]Plugin path:%s\n", pluginPath.c_str());
  OutputDebugString(msgbuf);    

  std::string configFilePath = pluginPath.append("pluginsFusion\\VRPNSkeletonControl.conf");

  sprintf(msgbuf, "[VRPNClient] Config file path:%s\n", configFilePath.c_str());
  OutputDebugString(msgbuf);    

  // try to open config file
  std::string line;
  std::ifstream inConfigFile;

  bool serverConfigFound = false;
  bool trackerMapFound = false;
  
  inConfigFile.open(configFilePath.c_str(), std::ifstream::in);  

  // parse the config file
  if (inConfigFile.is_open())
  {
    while (inConfigFile.good())
    {
      GetUncommentedLine(inConfigFile,line);

      // get server info from config and connect      
      if(line.find("[Server]", 0) != (size_t)-1)
      { 
        if (!InitConnection(line, inConfigFile))
          return false;
      }

      if (line.find("[Cortex]", 0) != (size_t)-1)
      {
        std::string upAxisString;

        GetUncommentedLine(inConfigFile,line);
        if (line.find("up_axis=") != (size_t)-1) 
        {
          OutputDebugString(line.c_str());
          upAxisString = GetConfValue(line);
          OutputDebugString(upAxisString.c_str());

          if (!stricmp(upAxisString.c_str(), "Z"))
          {
            _upAxis = UP_AXIS_Z;
          }
          else
          {
            _upAxis = UP_AXIS_Y;
          }
        }
      }

      // get segment dir adjustmens
      if(line.find("[TransformationAdjustments]", 0) != (size_t)-1)
      {
        ReadAdjustments(line, inConfigFile);        
      }

      // create controlled unit
      if(line.find("[TrackerMap:", 0) != (size_t)-1)
      {
        InitUnit(line, inConfigFile);
      }
    }
    inConfigFile.close();
  }
  else
  {
    OutputDebugString("[VRPNClient]Cannot open VRPNSkeletonControl.conf file!");
    return false;
  }

  // add dir adjustmensts to the initialized trackers
  TrackerList::iterator it;
  for (it = _trackers.begin(); it != _trackers.end(); ++it)
  {
    AdjustmentList::iterator itAdj = _axisAdjustments.find((*it).first);
    if (itAdj != _axisAdjustments.end())
    {
      (*it).second._axisAdjustment = (*itAdj).second._axis;
      (*it).second._rotationAdjustment = (*itAdj).second._rotation;
    }
    else
    {
      (*it).second._axisAdjustment = AXIS_DEFAULT;
      (*it).second._rotationAdjustment = ROTATION_DEFAULT;
    }
  }

  return true;
}

//////////////////////////////////////////////////////////////////////////
void VRPNClient::LoadServerConfig(std::ifstream &inConfigFile, std::string &line)
{
  std::string addrval = "localhost";
  int portval = 3883;

  GetUncommentedLine(inConfigFile,line);
  if (line.find("address=") != (size_t)-1) 
  {
    OutputDebugString(line.c_str());
    addrval = GetConfValue(line);
  }

  GetUncommentedLine(inConfigFile,line);
  if (line.find("port=") != (size_t)-1) 
  {
    OutputDebugString(line.c_str());
    portval = atoi(GetConfValue(line).c_str());
  }   
  assert(portval >= 0);

  sprintf(_connectionName,"%s:%d", addrval.c_str(), portval);  
}
//////////////////////////////////////////////////////////////////////////
bool VRPNClient::NewConfigEntry(const string &line)
{
  size_t left_br_index = line.find("[");
  if (left_br_index == (size_t)-1) {return false;}
  size_t right_br_index = line.find("]");
  if (right_br_index == (size_t)-1) {return false;}

  return true;
}

//////////////////////////////////////////////////////////////////////////
std::string VRPNClient::GetConfValue(std::string confString)
{
  int valIndex = (confString.find("=") + 1);
  if (valIndex == (size_t)-1) {return std::string("");}
  else {return confString.substr(valIndex, confString.size() - valIndex);}
}

//////////////////////////////////////////////////////////////////////////
bool VRPNClient::GetConfigValues(const string &confString, string &confName_out, string &confValue_out)
{
  int valIndex = (confString.find("=") + 1);
  if (valIndex == (size_t)-1)
  {
    confName_out = "";
    confValue_out = "";
    return false;
  }
  else
  {
    confName_out = confString.substr(0, valIndex - 1);
    confValue_out = confString.substr(valIndex, confString.size() - valIndex); 
  }
  return true;
}

//////////////////////////////////////////////////////////////////////////
void VRPNClient::GetUncommentedLine(std::ifstream& confFile, std::string& line)
{
  do 
  {
    if (confFile.eof()) {return;}
    getline (confFile,line); 
  } while (line.find_first_of("#") == 0);
}

//////////////////////////////////////////////////////////////////////////
VRPNClient::~VRPNClient()
{
}

//////////////////////////////////////////////////////////////////////////
bool VRPNClient::InitConnection()
{
  return true;
}

//////////////////////////////////////////////////////////////////////////
bool VRPNClient::InitConnection( std::string &line, std::ifstream& inConfigFile )
{
  OutputDebugString(line.c_str());
  LoadServerConfig(inConfigFile, line);

  // try to connect to the server
  _pConnection = vrpn_get_connection_by_name(_connectionName);
  if (!_pConnection)
  {
    OutputDebugString("[VRPNClient]Cannot connect to VRPN server!\n");
    return false;
  }
  OutputDebugString("[VRPNClient]Connected to the server\n");
  return true;
}

//////////////////////////////////////////////////////////////////////////
void VRPNClient::ReadAdjustments( std::string &line, std::ifstream &inConfigFile )
{  
  OutputDebugString(line.c_str());

  GetUncommentedLine(inConfigFile,line);

  OutputDebugString(line.c_str());
  std::streampos sp = inConfigFile.tellg();
  while (!NewConfigEntry(line))
  {
    if (inConfigFile.eof()) { break; }
    OutputDebugString(line.c_str());

    string trackerName = "";
    string adjString = "";
    if (GetConfigValues(line, trackerName, adjString))
    {
      string axisAdjustment;
      string rotationAdjustment;
      int delimIndex = adjString.find_first_of(',');
      if (delimIndex > -1)
      {
        axisAdjustment = adjString.substr(0, delimIndex);
        OutputDebugString("AXIS ADJ:");
        OutputDebugString(axisAdjustment.c_str());
        rotationAdjustment = adjString.substr(delimIndex + 1, adjString.length() - (delimIndex + 1));
        OutputDebugString("ROLL ADJ:");
        OutputDebugString(rotationAdjustment.c_str());
      }
      else
      {
        axisAdjustment = adjString;
      }
      
      if (!stricmp(axisAdjustment.c_str(), "UP_TO_NEGATIVE_UP"))
      {
        _axisAdjustments[trackerName]._axis = UP_TO_NEGATIVE_UP;
      }
      else if (!stricmp(axisAdjustment.c_str(), "UP_TO_DIR"))
      {
        _axisAdjustments[trackerName]._axis = UP_TO_DIR;
      }
      else if (!stricmp(axisAdjustment.c_str(), "UP_TO_NEGATIVE_DIR"))
      {
        _axisAdjustments[trackerName]._axis = UP_TO_NEGATIVE_DIR;
      }
      else if (!stricmp(axisAdjustment.c_str(), "UP_TO_ASIDE"))
      {
        _axisAdjustments[trackerName]._axis = UP_TO_ASIDE;
      }
      else if (!stricmp(axisAdjustment.c_str(), "UP_TO_NEGATIVE_ASIDE"))
      {
        _axisAdjustments[trackerName]._axis = UP_TO_NEGATIVE_ASIDE;
      }
      else if (!stricmp(axisAdjustment.c_str(), "DIR_TO_ASIDE"))
      {
        _axisAdjustments[trackerName]._axis = DIR_TO_ASIDE;
      }
      else if (!stricmp(axisAdjustment.c_str(), "DIR_TO_NEGATIVE_ASIDE"))
      {
        _axisAdjustments[trackerName]._axis = DIR_TO_NEGATIVE_ASIDE;
      }
      else
      {
        _axisAdjustments[trackerName]._axis = AXIS_DEFAULT;
      }
      
      if (!stricmp(rotationAdjustment.c_str(), "ROTATEX_PI"))
      {
        _axisAdjustments[trackerName]._rotation = ROTATEX_PI;
      }
      else if (!stricmp(rotationAdjustment.c_str(), "ROTATEX_HALF_PI"))
      {
        _axisAdjustments[trackerName]._rotation = ROTATEX_HALF_PI;
      }
      else if (!stricmp(rotationAdjustment.c_str(), "ROTATEX_NEGATIVE_HALF_PI"))
      {
        _axisAdjustments[trackerName]._rotation = ROTATEX_NEGATIVE_HALF_PI;
      }
      else if (!stricmp(rotationAdjustment.c_str(), "ROTATEY_PI"))
      {
        _axisAdjustments[trackerName]._rotation = ROTATEY_PI;
      }
      else if (!stricmp(rotationAdjustment.c_str(), "ROTATEY_HALF_PI"))
      {
        _axisAdjustments[trackerName]._rotation = ROTATEY_HALF_PI;
      }
      else if (!stricmp(rotationAdjustment.c_str(), "ROTATEY_NEGATIVE_HALF_PI"))
      {
        _axisAdjustments[trackerName]._rotation = ROTATEY_NEGATIVE_HALF_PI;
      }
      else if (!stricmp(rotationAdjustment.c_str(), "ROTATEZ_PI"))
      {
        _axisAdjustments[trackerName]._rotation = ROTATEZ_PI;
      }
      else if (!stricmp(rotationAdjustment.c_str(), "ROTATEZ_HALF_PI"))
      {
        _axisAdjustments[trackerName]._rotation = ROTATEZ_HALF_PI;
      }
      else if (!stricmp(rotationAdjustment.c_str(), "ROTATEZ_NEGATIVE_HALF_PI"))
      {
        _axisAdjustments[trackerName]._rotation = ROTATEZ_NEGATIVE_HALF_PI;
      }
      else
      {
        _axisAdjustments[trackerName]._rotation = ROTATION_DEFAULT;
      }
    }

    GetUncommentedLine(inConfigFile,line);
    sp = inConfigFile.tellg();
  }
  inConfigFile.seekg( sp ); // return stream to the prev entry
}

//////////////////////////////////////////////////////////////////////////
void VRPNClient::InitUnit( std::string &line, std::ifstream &inConfigFile )
{
  OutputDebugString(line.c_str());

  // get unit name
  int delimIndex = line.find_first_of(':');
  std::string unitName = line.substr(delimIndex + 1, line.find_first_of(']') - delimIndex - 1);
  OutputDebugString(unitName.c_str());

  // create new unit
  CreateControlledUnit(unitName);

  // connect new unit to the trackers
  GetUncommentedLine(inConfigFile,line);
  OutputDebugString(line.c_str());
  std::streampos sp = inConfigFile.tellg();
  while (!NewConfigEntry(line))
  {
    if (inConfigFile.eof()) { break; }
    OutputDebugString(line.c_str());

    string boneName = "";
    string trackerName = "";
    if (GetConfigValues(line, boneName, trackerName))
    {
      // check if the tracker already exists
      //if (_trackers.find(trackerName) == _trackers.end())
      {
        _trackers[trackerName]._remote = new vrpn_Tracker_Remote(trackerName.c_str(), _pConnection);
        _trackers[trackerName]._remote->register_change_handler(&_trackers[trackerName], HandleVRPNTracker);
      }

      // connect tracker to the unit's bone
      BonesNameIndexMap::const_iterator it = SkeletonDefinition.BonesMap.find(boneName);
      if (it != SkeletonDefinition.BonesMap.end())
      {
        int boneIndex = (*it).second;
        assert((boneIndex >= 0) && (boneIndex < NUM_BONES));

        _units[unitName].boneTrackerMap[boneIndex] = trackerName;
      }
    }
    sp = inConfigFile.tellg();
    GetUncommentedLine(inConfigFile,line);
  }
  inConfigFile.seekg( sp ); // return stream to the prev entry
}

//////////////////////////////////////////////////////////////////////////
void VRPNClient::CreateControlledUnit( std::string &unitName )
{
  VBSFusion::Unit& controlledUnit = _units[unitName];
  if (!stricmp(unitName.c_str(), "player"))
  {
    controlledUnit = VBSFusion::MissionUtilities::getPlayer();
  }
  else
  {
    // create new control unit
    // TODO: better take control of already existing unit (fusion crashes so far)
    VBSFusion::Unit playerUnit = VBSFusion::MissionUtilities::getPlayer();
    VBSFusion::UnitUtilities::updateStaticProperties(playerUnit);
    VBSFusion::UnitUtilities::updateDynamicProperties(playerUnit);

    VBSFusion::position3D pos = playerUnit.getPosition();
    pos.setZ(pos.getZ() + 3);

    VBSFusion::UnitUtilities::CreateUnit(controlledUnit, pos);
    controlledUnit.setName(unitName);
    VBSFusion::UnitUtilities::applyName(controlledUnit);
  }
}

void VRPNClient::SetPoseControlEnabled(bool enabled)
{
  PoseControlUnitList::iterator it;
  for (it = _units.begin(); it != _units.end(); ++it)
  {
     PoseControlUnit& unit = (*it).second;
     // set pose control value to the current unit
     VBSFusion::PoseControlUtilities::applyExternalPose(unit, enabled);
     VBSFusion::PoseControlUtilities::applyExternalPoseUpBody(unit, enabled);
     VBSFusion::PoseControlUtilities::applyExternalPoseSkeleton(unit, enabled);
     VBSFusion::PoseControlUtilities::applyExternalCameraControlled(unit, enabled);
  }

  //ExecuteCmd("\"LookAround\" setAction 1"); 
  ExecuteCmd("player allowDammage false"); 
  ExecuteCmd("player setDisableAnimationMove true");
  ExecuteCmd("player setDir 0"); //reset player orientation
}

//////////////////////////////////////////////////////////////////////////
void	VRPN_CALLBACK HandleVRPNTracker (void *userdata, const vrpn_TRACKERCB t)
{
  if (!userdata) {return;}
  Tracker* pTracker = (Tracker*)userdata;
  if (pTracker) 
  {
    pTracker->_data = t;
    pTracker->_data.sensor = eTRACKER_INITIALIZED;  // used as initilization flag
  }
}

