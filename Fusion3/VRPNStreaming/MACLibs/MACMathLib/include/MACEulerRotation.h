//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACEulerRotation.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Generalised Euler rotation header class.
//		This will be used by all other rotation types as the generic header class.
//		In the main, all accesses to these classes will be via this.
//
//
//
// Log:
//		Created 3/1/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_EULER_ROTATION_H__
#define __MAC_EULER_ROTATION_H__

#include <iostream>
#include <float.h>
#include "MACEulerOrder.h"

MAC_MATH_LIB_BEGIN

#define MAC_EULER_DEFAULT_AXES	(MACEulerOrder::MA_ZYX_S)

//***********************************************************************
//***********************************************************************
//
// Instanciated Rotations
//
// Notes:
// Log:
//		Created 28/2/01, Ian Elsley
//

typedef	MACEulerRotation<double>	MACEARotd;
typedef MACEulerRotation<float>		MACEARotf;


//
// End of instanciation
//
//***********************************************************************
//***********************************************************************


	

//***********************************************************************
//***********************************************************************
//
// Templated Class Definitions
//
template<class T>
class MACEulerRotation : public MACRotation<T>
{
	
	private:	/*** Variables ***/

		
		MACVector3<T>		m_rotation;
		MACEulerOrder		m_order;

		static MACEulerOrder	m_defaultOrder;		
		// NOTE: defaultOrder access MUST be via setDefaultOrder and getDefault order 
		// as the usage is inherited - primarily by MACAziEleRotation. ICE: 12th April 2001


	public:		/*** Constructors, destructors and initialisers ***/

		
		MACEulerRotation<T>	()										: m_rotation((T)0)
																		{ Order(getDefaultOrder()); }
		// construct with ordering
		MACEulerRotation<T>	(const MACEulerOrder& order)			: m_rotation((T)0)
																		{ Order(order); }
		MACEulerRotation<T>	(unsigned int order)					: m_rotation((T)0)
																		{ Order(order); }
		
		// Construct by rotation angle
		MACEulerRotation<T>	(const T& ri, const T& rj, const T& rh)	: m_rotation(ri, rj, rh)	
																		{ Order(getDefaultOrder()); }
		MACEulerRotation<T>	(const T& ri, const T& rj, const T& rh, const MACEulerOrder& order)
																	: m_rotation(ri, rj, rh)
																		{ Order(order); }
		MACEulerRotation<T>	(const T& ri, const T& rj, const T& rh, unsigned int order)
																	: m_rotation(ri, rj, rh), m_order(order) 
																		{ Order(order); }

		// Construct with another rotation type
		MACEulerRotation<T>	(const MACEulerRotation<T>& v)				{ set(v, getDefaultOrder()); }
		MACEulerRotation<T>	(const MACEulerRotation<T>& v, const MACVector3<T>& order)
																		{ set(v, MACEulerOrder(order)); }
		MACEulerRotation<T>	(const MACEulerRotation<T>& v, const MACEulerOrder& order)
																		{ set(v, order); }
		
		
		MACEulerRotation<T>	(const MACAngleAxisRotation<T>& v)			{ Order(getDefaultOrder()); set(v); }
		MACEulerRotation<T>	(const MACAngleAxisRotation<T>& v, const MACVector3<T>& order)
																		{ Order(order); set(v); }
		MACEulerRotation<T>	(const MACAngleAxisRotation<T>& v, const MACEulerOrder& order)
																		{ Order(order); set(v); }
		
		
		MACEulerRotation<T>	(const MACQuaternion<T>& v)					{ Order(getDefaultOrder()); set(v); }
		MACEulerRotation<T>	(const MACQuaternion<T>& v, const MACVector3<T>& order)		
																		{ Order(order); set(v); }
		MACEulerRotation<T>	(const MACQuaternion<T>& v, const MACEulerOrder& order)		
																		{ Order(order); set(v); }
		
		
		MACEulerRotation<T>	(const MACRotationMatrix3<T>& v)				{ Order(getDefaultOrder()); set(v); }
		MACEulerRotation<T>	(const MACRotationMatrix3<T>& v, const MACVector3<T>& order)
																		{ Order(order); set(v); }
		MACEulerRotation<T>	(const MACRotationMatrix3<T>& v, const MACEulerOrder& order)
																		{ Order(order); set(v); }

		virtual ~MACEulerRotation<T>	()								{}


	protected:

	
		// Standard array access
		// Note: this is NOT to be used outside the library itself.
		//
		virtual T&			operator[]	(const unsigned int i)			{ return m_rotation[i]; }
		virtual const T&	operator[]	(const unsigned int i) const	{ return m_rotation[i]; }


	public:		/*** Access - Set and Get ***/

		
		// Get the values
		T					I		()		const			{ return m_rotation.X();	}
		T					J		()		const			{ return m_rotation.Y();	}
		T					H		()		const			{ return m_rotation.Z();	}
		const MACEulerOrder&	Order	()	const			{ return m_order;			}	
		// Bad form for the code standard - fix Order to something else - Larry?
		// Also correct use of const?
		void				getOrd	(unsigned int& i, unsigned int& j, unsigned int& k, unsigned int& h, 
									 unsigned int& n, unsigned int& s, unsigned int& f)	const	
															{ m_order.getOrd(i, j, k, h, n, s, f); }	
		

		// Set the values
		T					I		(T v)					{ return m_rotation.X(v);	}
		T					J		(T v)					{ return m_rotation.Y(v);	}
		T					H		(T v)					{ return m_rotation.Z(v);	}
		const MACEulerOrder&	Order	(const MACEulerOrder& v)	{ return m_order.set(v);	}	
		// Bad form for the code standard - fix Order to something else - Larry?
		// Also correct use of const?


		// Set the values in a group
		MACEulerRotation<T>&	set	(const MACEulerOrder& order)	{ Order(order); return *this; }			
		MACEulerRotation<T>&	set	(unsigned int order)			{ Order(order); return *this; }			
		MACEulerRotation<T>&	set	(const T& ri, const T& rj, const T& rh)	
																	{ I(ri); J(rj); H(rh); return *this; }			
		MACEulerRotation<T>&	set	(const T& ri, const T& rj, const T& rh, const MACEulerOrder& order)
																	{ I(ri); J(rj); H(rh); Order(order); return *this; }	
		
		

		// Conversions
		MACEulerRotation<T>&	set	(const MACEulerRotation<T>& v)
															{ return convert(v); }
															//{ m_rotation.set(v.m_rotation); Order(v.Order()); return *this; }
		MACEulerRotation<T>&	set	(const MACEulerRotation<T>& v, const MACEulerOrder& order)
															{ Order(order); convert(v);	return *this; }
		MACEulerRotation<T>&	set	(const MACAngleAxisRotation<T>& v)	
															{ return set(MACRotationMatrix3<T>(v)); }
		MACEulerRotation<T>&	set	(const MACQuaternion<T>& v);
		MACEulerRotation<T>&	set	(const MACRotationMatrix3<T>& v);
		MACEulerRotation<T>&	set	(const MACRotationMatrix3<T>& v, const MACVector3<T>& order);
	

		MACEulerRotation<T>& convert	(const MACEulerRotation<T>& v);


		
		// default orders
		virtual	MACEulerRotation<T>&	setDefaultOrder	(unsigned int order)		
															{ m_defaultOrder.set(order); return *this; }			
		virtual	MACEulerRotation<T>&	setDefaultOrder	(const MACEulerOrder& order)	
															{ m_defaultOrder.set(order); return *this; }
		virtual const MACEulerOrder& getDefaultOrder ()		{ return m_defaultOrder; }


	public:		/*** General Arithmetic operators ***/


		// Assignment
		MACEulerRotation<T>&	operator=	(const MACEulerRotation& r)	{ return this->set(r); }
		MACEulerRotation<T>&	operator*=	(const MACEulerRotation<T>& v);
		
		// Arithmetic
		MACEulerRotation<T>	operator*	(const MACEulerRotation<T>& v)	const;
		MACEulerRotation<T>	operator-	()								const;
		

	public:		/*** General Arithmetic functions ***/

		
		MACEulerRotation<T>& invert		()			{ _ASSERT(0); return *this; }
		MACEulerRotation<T>	theInverse	()	const	{ _ASSERT(0); return *this }


	public:		/*** Comparitors ***/

		
		bool				equal		(const MACEulerRotation<T>& v, T error = MAC_EULER_ERROR_LIMIT)	const;
		bool				operator==	(const MACEulerRotation<T>& v)								const;
		bool				operator!=	(const MACEulerRotation<T>& v)								const;

	
	public:		/*** Identity Functions ***/

		
		MACEulerRotation<T>		identity	()	const	{ return MACEulerRotation<T>((T)0, (T)0, (T)0); }
		MACEulerRotation<T>&	setIdentity	()			{ set(identity()); return *this; }
		bool					isIdentity	()	const	{ return equal(identity()); }
	

	protected:	/*** IO functions ***/


		friend std::istream& operator>>(std::istream&, MACEulerRotation<T>&);
		friend std::ostream& operator<<(std::ostream&, MACEulerRotation<T>&);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these

};

//
// End of Class Definitions
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Templated Method Code
//

//
// Notes:
//		conversion between rotation orders happens via rotation matrices
// Log:
//		create 4/18/2001, Ian Elsley
//
template<class T> MACEulerRotation<T>&	
MACEulerRotation<T>::convert	(const MACEulerRotation<T>& v)
{
	if (Order()==v.Order())
		m_rotation.set(v.m_rotation);
	else
		set(MACRotationMatrix3<T>(v));
	return *this; 
}


//
// Notes:
// Log:
//		create 4/18/2001, Ian Elsley
//
template<class T> MACEulerRotation<T>&	
MACEulerRotation<T>::set	(const MACQuaternion<T>& v)	
{ 
	return this->set(MACRotationMatrix3<T>(v)); 
}

//
// Notes:
//		Ken Shoemake's recommended algorithm.
//		atan2 templated in vcc? do these need explicit casts?
// Log:
//		Created: 4/2/01, Ian Elsley
//
template<class T> MACEulerRotation<T>&	
MACEulerRotation<T>::set	(const MACRotationMatrix3<T>& m)
{
    unsigned int	i, j, k, h, n, s, f;
	T				ri, rj, rh;
	double			sy, cy;

    MACEulGetOrd (m_order.Order(), i, j, k, h, n, s, f);

    if (s == MACEulRepYes) 
	{
		sy = sqrt( (double) (m[j][i]*m[j][i] + m[k][i]*m[k][i]) );

		if (sy > 16*FLT_EPSILON)
		{
			ri = (T)atan2((double)m[j][i],	(double)m[k][i]);
			rj = (T)atan2(sy,				(double)m[i][i]);
			rh = (T)atan2((double)m[i][j],	(double)-m[i][k]);
		} 
		else
		{
			ri = (T)atan2((double)-m[k][j],	(double)m[j][j]);
			rj = (T)atan2(sy,				(double)m[i][i]);
			rh = (T)0;
		}
    } 
	else 
	{
		cy = sqrt( (double) (m[i][i]*m[i][i] + m[i][j]*m[i][j]) );
		
		if (cy > 16*FLT_EPSILON) 
		{
			ri = (T)atan2((double)m[j][k],	(double)m[k][k]);
			rj = (T)atan2((double)-m[i][k], cy);
			rh = (T)atan2((double)m[i][j],	(double)m[i][i]);
		} 
		else 
		{
			ri = (T)atan2((double)-m[k][j], (double)m[j][j]);
			rj = (T)atan2((double)-m[i][k], cy);
			rh = (T)0;
		}
    }
    
	if (n == MACEulParOdd)	
	{
		ri = -ri; 
		rj = -rj;
		rh = -rh;
	}

	// Keep within [0, PI]
	MAC_BOUND(ri, -(T)MAC_PI, (T)MAC_PI);
	MAC_BOUND(rj, -(T)MAC_PI, (T)MAC_PI);
	MAC_BOUND(rh, -(T)MAC_PI, (T)MAC_PI);

    if (f == MACEulFrmR) 
	{
		I(rh); J(rj); H(ri);
	} 
	else
	{
		I(ri); J(rj); H(rh);
	}

	return *this;
}





//
// Notes:
// Log:
//		Created: 4/2/01, Ian Elsley
//
template<class T> MACEulerRotation<T>&	
MACEulerRotation<T>::set	(const MACRotationMatrix3<T>& m, const MACVector3<T>& order)
{
	this->set(order);
	return this->set(m);
}


//********************************************
//
// General Arithmetic Assignment Operations
//
// Notes:
//
//********************************************

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACEulerRotation<T>&
MACEulerRotation<T>::operator*=	(const MACEulerRotation<T>& v)
{
	_ASSERT(0);
	return *this;
}


//********************************************
//
// General Arithmetic Operations
//
// Notes:
//
//********************************************

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACEulerRotation<T>
MACEulerRotation<T>::operator*	(const MACEulerRotation<T>& v)	const
{
	_ASSERT(0);
	return MACEulerRotation<T>();
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACEulerRotation<T>
MACEulerRotation<T>::operator-	()	const
{
	_ASSERT(0);
	return MACEulerRotation<T>();
}


//********************************************
//
// Comparitor Functions
//
//********************************************

// 
// MACEulerRotation<T>::equal(const MACEulerRotation<T>& v, T error = ERROR_VALUE)	const
// Notes:
//		Allied with this function are the explicit instanciations of the float and double cases
//		These are identical, but are implemented like this for speed.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACEulerRotation<T>::equal(const MACEulerRotation<T>& r, T error)					const
{
	return (((T)fabs((double)(I()-r.I())) < error) && 
			((T)fabs((double)(J()-r.J())) < error) && 
			((T)fabs((double)(H()-r.H())) < error) && 
			(Order() == r.Order()));
}

//
// Equal
// Notes:
//		Double version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACEulerRotation<double>::equal(const MACEulerRotation<double>& r, double error)	const
{
	return ((fabs(I()-r.I()) < error) && 
			(fabs(J()-r.J()) < error) && 
			(fabs(H()-r.H()) < error) && 
			(Order() == r.Order()));
}

//
// Equal
// Notes:
//		Float version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACEulerRotation<float>::equal(const MACEulerRotation<float>& r, float error)		const
{
	return ((fabsf(I()-r.I()) < error) && 
			(fabsf(J()-r.J()) < error) && 
			(fabsf(H()-r.H()) < error) && 
			(Order() == r.Order()));
}


//
// MACEulerRotation<T>::operator==(const MACEulerRotation<T>& v) const
// Notes:
//		This function may not work entirely as expected. 
//		There may be an error factor required.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACEulerRotation<T>::operator==(const MACEulerRotation<T>& r)						const
{
	return ((I()==r.I()) && (J()==r.J()) && (H()==r.H()) && (Order() == r.Order()));
}


//
// MACEulerRotation<T>::operator!=(const MACEulerRotation<T>& v) const
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACEulerRotation<T>::operator!=(const MACEulerRotation<T>& v)						const
{
	return (!((*this) == v));
}



//
// End of Method Code
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// I/O Functions
//
// Notes:
//

#ifdef _NOT_USED_
//
// orderNumber
// Notes:
//		OrderNumber set the value of the index given the character 'X'|'Y'|'Z'
//		*** Throws a fault if it fails ***
// Log:
//		Created 3/6/01, Ian Elsley
//
template<class T> void
MACEulerOrder::setOrderNumber	(char c, int i)
{ 
	if (('x' <= c) && (c <= 'z'))
		c += 'X' - 'x';
	
	if (('X' <= c) && (c <= 'Z'))
		m_order[i] = (EULER_ORDER)(c-'X');
	else
		throw(INCORRECT_EULER_ORDER);
}


/*
//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACEulerOrder& v)
{
	int i;
	char c;

	// Get the rotation  order and convert it.
	for (i=0; i<3; i++) {
		in >> c;
		v.setOrderNumber(c, i);
	}
	return in;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& 
operator<<(std::ostream& out, MACEulerOrder& v)
{
	return out << v.orderChar(0) << ' ' << v.orderChar(1) << ' ' << v.orderChar(2);
}
*/
#endif _NOT_USED_

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACEulerRotation<T>& v)
{
	return in >> m_rotation >> m_order;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& operator<<(std::ostream& out, MACEulerRotation<T>& v)
{
	return out << m_rotation << ' ' << m_order;
}

//
// End of Function Code
//
//***********************************************************************
//***********************************************************************



MAC_MATH_LIB_END

#endif __MAC_ANGLE_AXIS_ROTATION_H__