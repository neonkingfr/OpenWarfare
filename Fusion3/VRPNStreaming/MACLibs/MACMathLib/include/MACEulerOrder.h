//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACEulerRotation.h
// Author:		Ian Elsley
// Created:		9th April, 2001
//
//
// Documentation:
//		Generalised Euler rotation header class.
//		This will be used by all other rotation types as the generic header class.
//		In the main, all accesses to these classes will be via this.
//
//
//
// Log:
//		Created 3/1/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
//		Support for 24 angle schemes based on code by Ken Shoemake,
//		in "Graphics Gems IV", Academic Press, 1994 
//
//		Order type constants, constructors, extractors	
//		There are 24 possible conventions, designated by:
//			  o EulAxI = axis used initially			
//			  o EulPar = parity of axis permutation	
//			  o EulRep = repetition of initial axis as last	 
//			  o EulFrm = frame from which axes are taken
//		Axes I,J,K will be a permutation of X,Y,Z.	
//		Axis H will be either I or K, depending on EulRep. 
//		Frame S takes axes from initial static frame.	
//		If ord = (AxI=X, Par=Even, Rep=No, Frm=S), then	 
//		{a,b,c,ord} means Rz(c)Ry(b)Rx(a), where Rz(c)v	 
//		rotates v around Z by c radians.				
//
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_EULER_ORDER_H__
#define __MAC_EULER_ORDER_H__

MAC_MATH_LIB_BEGIN


#define MACEulFrmS			0
#define MACEulFrmR			1
#define MACEulFrm(ord)		((unsigned)(ord)&1)

#define MACEulRepNo			0
#define MACEulRepYes			1
#define MACEulRep(ord)		(((unsigned)(ord)>>1)&1)

#define MACEulParEven		0
#define MACEulParOdd			1
#define MACEulPar(ord)		(((unsigned)(ord)>>2)&1)

#define MACEulSafe			"\000\001\002\000"
#define MACEulNext			"\001\002\000\001"

#define MACEulAxI(ord)		((int)(MACEulSafe[(((unsigned)(ord)>>3)&3)]))
#define MACEulAxJ(ord)		((int)(MACEulNext[MACEulAxI(ord)+(MACEulPar(ord)==MACEulParOdd)]))
#define MACEulAxK(ord)		((int)(MACEulNext[MACEulAxI(ord)+(MACEulPar(ord)!=MACEulParOdd)]))
#define MACEulAxH(ord)		((MACEulRep(ord)==MACEulRepNo)?MACEulAxK(ord):MACEulAxI(ord))

/* MACEulGetOrd unpacks all useful information about order simultaneously. */
#define MACEulGetOrd(ord,i,j,k,h,n,s,f) {			\
	unsigned o=ord;									\
	f=o&1; o=o>>1; s=o&1; o=o>>1;					\
	n=o&1; o=o>>1;									\
	i=MACEulSafe[o&3];								\
	j=MACEulNext[i+n];								\
	k=MACEulNext[i+1-n];							\
	h=s?k:i;										\
}

/* MACEulOrd creates an order value between 0 and 23 from 4-tuple choices. */
#define MACEulOrd(i,p,r,f)	   (((((((i)<<1)+(p))<<1)+(r))<<1)+(f))





//****************************************************
//****************************************************
//
// Class Definitions
//
//****************************************************
//****************************************************

class MACEulerOrder
{

	private:		/*** Ordering information ***/

		enum {
			XR				=	0,
			YR				=	1,
			ZR				=	2,
		};


	public:	/*** Local rotation ordering ***/

		// i,j,h, where h = i or k. See notes at top of file
		enum {
			IR				=	0,
			JR				=	1,
			HR				=	2,
		};
		
		// Type definition of order definitions in Shoemaker form
		enum {
			
			/* Static axes */
			MACEulOrdXYZs	=	MACEulOrd(XR, MACEulParEven, MACEulRepNo, MACEulFrmS),
			MACEulOrdXYXs	=	MACEulOrd(XR, MACEulParEven, MACEulRepYes, MACEulFrmS),
			MACEulOrdXZYs	=	MACEulOrd(XR, MACEulParOdd, MACEulRepNo, MACEulFrmS),
			MACEulOrdXZXs	=	MACEulOrd(XR, MACEulParOdd, MACEulRepYes, MACEulFrmS),
			MACEulOrdYZXs 	=	MACEulOrd(YR, MACEulParEven, MACEulRepNo, MACEulFrmS),
			MACEulOrdYZYs 	=	MACEulOrd(YR, MACEulParEven, MACEulRepYes, MACEulFrmS),
			MACEulOrdYXZs 	=	MACEulOrd(YR, MACEulParOdd, MACEulRepNo, MACEulFrmS),
			MACEulOrdYXYs	=	MACEulOrd(YR, MACEulParOdd, MACEulRepYes, MACEulFrmS),
			MACEulOrdZXYs	=	MACEulOrd(ZR, MACEulParEven, MACEulRepNo, MACEulFrmS),
			MACEulOrdZXZs	=	MACEulOrd(ZR, MACEulParEven, MACEulRepYes, MACEulFrmS),
			MACEulOrdZYXs	=	MACEulOrd(ZR, MACEulParOdd, MACEulRepNo, MACEulFrmS),
			MACEulOrdZYZs	=	MACEulOrd(ZR, MACEulParOdd, MACEulRepYes, MACEulFrmS),

			/* Rotating axes */
			MACEulOrdZYXr 	=	MACEulOrd(XR, MACEulParEven, MACEulRepNo, MACEulFrmR),
			MACEulOrdXYXr 	=	MACEulOrd(XR, MACEulParEven, MACEulRepYes, MACEulFrmR),
			MACEulOrdYZXr 	=	MACEulOrd(XR, MACEulParOdd, MACEulRepNo, MACEulFrmR),
			MACEulOrdXZXr 	=	MACEulOrd(XR, MACEulParOdd, MACEulRepYes, MACEulFrmR),
			MACEulOrdXZYr 	=	MACEulOrd(YR, MACEulParEven, MACEulRepNo, MACEulFrmR),
			MACEulOrdYZYr 	=	MACEulOrd(YR, MACEulParEven, MACEulRepYes, MACEulFrmR),
			MACEulOrdZXYr  	=	MACEulOrd(YR, MACEulParOdd, MACEulRepNo, MACEulFrmR),
			MACEulOrdYXYr  	=	MACEulOrd(YR, MACEulParOdd, MACEulRepYes, MACEulFrmR),
			MACEulOrdYXZr  	=	MACEulOrd(ZR, MACEulParEven, MACEulRepNo, MACEulFrmR),
			MACEulOrdZXZr 	=	MACEulOrd(ZR, MACEulParEven, MACEulRepYes, MACEulFrmR),
			MACEulOrdXYZr  	=	MACEulOrd(ZR, MACEulParOdd, MACEulRepNo, MACEulFrmR),
			MACEulOrdZYZr  	=	MACEulOrd(ZR, MACEulParOdd, MACEulRepYes, MACEulFrmR),
		};


	public:		/*** Externally usable rotation ordering ***/

		//
		// These are the enumerations that should be used for euler angles
		// Why? Motion Analysis orderings are back to front....
		// These need to be extended to other ordering types but I don;t have time at the moment -- ICE 12th April, 2001
		// 
		enum {
			MA_ZYX_S			= MACEulOrdXYZs,
		};


	private:	/*** Variables ***/

		// Base default order
		unsigned int 	m_order;


	public:		/*** Constructors and Destructors ***/


		// Note. Default given here should ALWAYS be overridden by the default value for the inheriting class.
		MACEulerOrder	()											: m_order(MA_ZYX_S)		{}
		MACEulerOrder	(unsigned int v)							: m_order(v)			{}
		MACEulerOrder	(const MACEulerOrder& v)					: m_order(v.m_order)	{}

		virtual	~MACEulerOrder	()															{}

	
	public:		/*** Access - Set and Get ***/

		// Get the values
		unsigned int	I		()	const							{ return MACEulAxI(m_order); }
		unsigned int	J		()	const							{ return MACEulAxJ(m_order);	}
		unsigned int	K		()	const							{ return MACEulAxK(m_order);	}	
		unsigned int	H		()	const							{ return MACEulAxH(m_order);	}
		unsigned int	Order	()	const							{ return m_order;			}	


		void			getOrd	(unsigned int& i, unsigned int& j, unsigned int& k, unsigned int& h, 
								 unsigned int& n, unsigned int& s, unsigned int& f) const
																	{ MACEulGetOrd(m_order, i, j, k, h, n, s, f); }

		// Set the values in a group
		MACEulerOrder	set		(unsigned int v)					{ m_order = v; return *this;			}
		MACEulerOrder&	set		(const MACEulerOrder& v)			{ m_order = v.m_order; return *this;	}


	public:		/*** Comparitors ***/

		bool			operator==	(const MACEulerOrder& v)	const	{ return m_order == v.m_order;	}
		bool			operator!=	(const MACEulerOrder& v)	const	{ return (!(*this == v));		}

	
	protected:		/*** IO Functions ***/

		// utility functions for printing
		// getOrderChar returns 'X'|'Y'|'Z' given the order index
		//char			getOrderChar	(int i)	const				{ return 'X' + m_order[i]; }

		// setOrderNumber set the value of the index given the character
		//void			setOrderNumber	(char c, int i);

		//friend std::istream& operator>>(std::istream&, MACEulerOrder&);
		//friend std::ostream& operator<<(std::ostream&, MACEulerOrder&);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these

};


//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
#ifdef _NOT_USED_
template<class T> std::istream& 
operator>>(std::istream& in, MACEulerOrder& v)
{
	int i;
	char c;

	// Get the rotation  order and convert it.
	for (i=0; i<3; i++) {
		in >> c;
		v.setOrderNumber(c, i);
	}
	return in;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& 
operator<<(std::ostream& out, MACEulerOrder& v)
{
	return out << v.orderChar(0) << ' ' << v.orderChar(1) << ' ' << v.orderChar(2);
}

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACEulerRotation<T>& v)
{
	return in >> m_rotation >> m_order;
}
#endif _NOT_USED_


MAC_MATH_LIB_END

#endif __MAC_EULER_ORDER_H__