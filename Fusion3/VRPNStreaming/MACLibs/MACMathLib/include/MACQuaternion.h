//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACQuaternion.h
// Author:		Ian Elsley
// Created:		16th March, 2001
//
//
// Documentation:
//		Quaternion Rotation
//		
//
// Log:
//		Created 3/1/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
// 
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************



#ifndef __MAC_QUATERNION_ROTATION_H__
#define __MAC_QUATERNION_ROTATION_H__

#include <iostream>

#include "MACEulerOrder.h"

MAC_MATH_LIB_BEGIN

//*********************************************************************
//*********************************************************************
//
// Instanciated Vectors
//
// Notes:
// Log:
//		Created 28/2/01, Ian Elsley
//

typedef	MACQuaternion<double>	MACQuatd;
typedef MACQuaternion<float>	MACQuatf;


//
// End of instanciation
//
//***********************************************************************
//***********************************************************************


//*********************************************************************
//*********************************************************************
//
// Templated Class Definitions
//
template<class T>
class MACQuaternion : public MACRotation<T>
{

	enum {
		QX		= 0,	// NB: These enums are fixed. The order must not be changed.
		QY,
		QZ,
		QW,
	};


	private:


		MACVector4<T>	m_quat;


	public:		/*** Constructors ***/


		MACQuaternion<T>	()									: m_quat((T)0, (T)0, (T)0, (T)1) {}
		MACQuaternion<T>	(T vx, T vy, T vz, T vw)			: m_quat(vx, vy, vz, vw) {}
		MACQuaternion<T>	(T vw, const MACVector3<T>& vec)	: m_quat(vec) { m_quat.W(vw); }

		MACQuaternion<T>	(const MACAngleAxisRotation<T>& v)		{ this->set(v); }
		MACQuaternion<T>	(const MACEulerRotation<T>& v)			{ this->set(v); }
		MACQuaternion<T>	(const MACQuaternion<T>& v)			: m_quat(v.m_quat) {}
		MACQuaternion<T>	(const MACRotationMatrix3<T>& v)		{ this->set(v); }

		virtual ~MACQuaternion<T>	()									{}


	public:		/*** Access - Set and Get ***/

		
		// Standard array access
		T&			operator[]	(const unsigned int i)				{ return m_quat[i]; }
		const T&	operator[]	(const unsigned int i) const		{ return m_quat[i]; }

	
		// Get the values
		virtual T	X()	const										{ return m_quat.X(); }
		virtual T	Y()	const										{ return m_quat.Y(); }
		virtual T	Z()	const										{ return m_quat.Z(); }	
		virtual T	W()	const										{ return m_quat.W(); }	


		// Set the values
		T			X(T v)											{ return m_quat.X(v); }
		T			Y(T v)											{ return m_quat.Y(v); }
		T			Z(T v)											{ return m_quat.Z(v); }
		T			W(T v)											{ return m_quat.W(v); }


		// Set the values in a group
		MACQuaternion<T>&	set	(const T& vx, const T& vy, const T& vz, const T& vw)	
																	{ m_quat.set(vx, vy, vz, vw); return *this;		}
		MACQuaternion<T>&	set	(T vw, const MACVector3<T>& vec)	{ m_quat.set(vec); m_quat.W(vw); return *this;	}

		virtual MACQuaternion<T>&	set	(const MACAngleAxisRotation<T>& v);
		virtual	MACQuaternion<T>&	set	(const MACEulerRotation<T>& v);		// Euler conversion is very slow. Avoid if possible
		virtual MACQuaternion<T>&	set	(const MACQuaternion<T>& v)	{ m_quat.set(v.m_quat); return *this;			}
		virtual MACQuaternion<T>&	set	(const MACRotationMatrix3<T>& v);

	
	public:		/*** General Arithmetic functions ***/


		MACQuaternion<T>&	invert		()							{ X(-X()); Y(-Y()); Z(-Z()); return *this; }
		MACQuaternion<T>		theInverse	()	const				{ return MACQuaternion<T>(-X(), -Y(), -Z(), W()); }

		MACQuaternion<T>&	normalize	()							{ m_quat.normalize(); return *this; }


	public:		/*** General Arithmetic operators ***/


		// Assignment
		MACQuaternion<T>&	operator=	(const MACQuaternion& r)	{ return this->set(r); }
		MACQuaternion<T>&	operator*=	(const MACQuaternion<T>& v);
		
		// Arithmetic
		MACQuaternion<T>		operator*	(const MACQuaternion<T>& v)	const;
		MACQuaternion<T>		operator-	()	const				{ return theInverse(); }
		

	public:		/*** Comparitors ***/


		bool				equal		(const MACQuaternion<T>& v, T error = MAC_QUATERNION_ERROR_LIMIT)	const
																	{ return m_quat.equal(v.m_quat, error); }
		bool				operator==	(const MACQuaternion<T>& v)							const
																	{ return m_quat == v.m_quat; }
		bool				operator!=	(const MACQuaternion<T>& v)							const
																	{ return !(*this == v); }

	
	public:		/*** Identity Functions ***/

		MACQuaternion<T>	identity	()							{ return MACQuaternion<T>((T)0, (T)0, (T)0, (T)1); }
		MACQuaternion<T>&	setIdentity	()							{ set((T)0, (T)0, (T)0, (T)1); return *this; }
		bool				isIdentity	()							{ return ((X()==(T)0) && (Y()==(T)0) && (Z()==(T)0) && (W()==(T)1)); }
	
		

	protected:	/*** IO functions ***/

//		friend std::istream& operator>>(std::istream&, MACQuaternion<T>&);
//		friend std::ostream& operator<<(std::ostream&, MACQuaternion<T>&);
// Note instanciation of the base class will have the extra effect of correctly instanciating these

};

//
// End of Class Definitions
//
//*********************************************************************
//*********************************************************************


//*********************************************************************
//*********************************************************************
//
// Templated Method Code
//

//
// Notes:
//		Normalize will slow this down but make it safer.
//		Not very fast....
// Log:
//		Created: 16th March, 2001, Ian Elsley
//
template<class T> MACQuaternion<T>&	
MACQuaternion<T>::set	(const MACAngleAxisRotation<T>& v)
{
	double		theta		= ((double)v.A())/2.0;
	T			sinTheta	= (T)sin(theta);
	MACVector3<T>	axis(v.Axis());

	axis.normalize();
	// Note: better than quaternion nomalizing as fewer members and longer vector!!
	
	m_quat.X (axis.X() * sinTheta);
	m_quat.Y (axis.Y() * sinTheta);
	m_quat.Z (axis.Z() * sinTheta);
	m_quat.W ((T)cos(theta));

	return *this;
}

#ifdef _OLD_CODE_
//
// Notes:
//		This is SLOW SLOW SLOW
// Log:
//		Created: 16th March, 2001, Ian Elsley
//
template<class T> MACQuaternion<T>&	
MACQuaternion<T>::set	(const MACEulerRotation<T>& v)
{
	set(MACRotationMatrix3<T>(v));
	return *this;
}
#endif


//
// Notes:
//		Construct quaternion from Euler angles (in radians).
//		Graphics Gems IV
// Log	
//		created: 7/24/01, Ian Elsley
//
template<class T> MACQuaternion<T>&	
MACQuaternion<T>::set	(const MACEulerRotation<T>& v)
{
	MACVector3<T>	a;
    double			ti, tj, th, 
					ci, cj, ch, 
					si, sj, sh, 
					cc, cs, sc, ss;
    unsigned int	i, j, k, h, n, s, f;
    
	v.getOrd(i,j,k,h,n,s,f);

	if (f == MACEulFrmR) 
	{
		ti = (double)v.H() * 0.5; 
		tj = (double)v.J() * 0.5; 
		th = (double)v.I() * 0.5;
	}
	else
	{
		ti = (double)v.I() * 0.5; 
		tj = (double)v.J() * 0.5; 
		th = (double)v.H() * 0.5;
	}

    if (n==MACEulParOdd) 
		tj = -tj;

	ci = cos(ti);	cj = cos(tj);	ch = cos(th);
    si = sin(ti);	sj = sin(tj);	sh = sin(th);
    cc = ci*ch;		cs = ci*sh;		
	sc = si*ch;		ss = si*sh;

    if (s==MACEulRepYes)
	{
		a[i] = (T)(cj*(cs + sc));	/* Could speed up with */
		a[j] = (T)(sj*(cc + ss));	/* trig identities. */
		a[k] = (T)(sj*(cs - sc));
		W((T)(cj*(cc - ss)));
    } 
	else 
	{
		a[i] = (T)(cj*sc - sj*cs);
		a[j] = (T)(cj*ss + sj*cc);
		a[k] = (T)(cj*cs - sj*sc);
		W((T)(cj*cc + sj*ss));
    }

    if (n==MACEulParOdd) a[j] = -a[j];

    X(a[QX]); Y(a[QY]); Z(a[QZ]);

    return *this;
}


//
// Notes:
//		Convert a matrix's rotation part to a quaternion	
//		based on the source in the book 'Advanced Animation	
//		and rendering techniques by Alan Watt & Mark Watt
//		Note: rotMatrix = local rot matrix
// Log:
//		created: 4/2/01, Ian Elsley			
//
template<class T> MACQuaternion<T>&	
MACQuaternion<T>::set	(const MACRotationMatrix3<T>& m)
{
	static	int		next[3]	=	{ QY,QZ,QX };
	MACQuaternion	&q			=	*this;
	
    T				tr,s;
    register int	i,j,k;
	
    tr = m[0][0] + m[1][1] + m[2][2];
    
	if (tr > 0.0)
	{
        s		= (T)sqrt((double)(tr + 1.0));
		q[QW]	= s * (T)0.5;
		s		= (T)0.5 / s;
		q[QX]	= (m[1][2] - m[2][1]) * s;
		q[QY]	= (m[2][0] - m[0][2]) * s;
		q[QZ]	= (m[0][1] - m[1][0]) * s;
    }
    else
    {
		i = QX;
		
		if (m[QY][QY] > m[QX][QX])	i = QY;
		if (m[QZ][QZ] > m[i][i])	i = QZ;
		
		j		= next[i];
		k		= next[j];
			
		s		= (T)sqrt((double) ( (m[i][i] - (m[j][j]+m[k][k])) + 1.0) );
		
		q[i]	= s * (T)0.5;
		s		= (T)0.5 / s;
		q[QW]	= (m[j][k] - m[k][j]) * s;
		q[j]	= (m[i][j] + m[j][i]) * s;
		q[k]	= (m[i][k] + m[k][i]) * s;
	}
	
	return *this;
}
/*


// wrong way up...  
template<class T> MACQuaternion<T>&	
MACQuaternion<T>::set	(const MACRotationMatrix3<T>& rotMatrix)
{
	static	int		VR_NXT[4]	=	{ QW,QY,QZ,QX };
	MACQuaternion	&LQ			=	*this;
	
    T				LTr,LS;
    register int	LI,LJ,LK;
	
    LTr=rotMatrix[0][0]+rotMatrix[1][1]+rotMatrix[2][2];
    if(LTr>0.0)
	{
        LS		= (T)sqrt((double)(LTr+1.0));
		LQ[QW]	= LS*0.5;
		LS		= 0.5/LS;
		LQ[QX]	= (rotMatrix[1][2]-rotMatrix[2][1])*LS;
		LQ[QY]	= (rotMatrix[2][0]-rotMatrix[0][2])*LS;
		LQ[QZ]	= (rotMatrix[0][1]-rotMatrix[1][0])*LS;
    }
    else
    {
		LI=QX;
		
		if(rotMatrix[QY-1][QY-1]>rotMatrix[QX-1][QX-1])
			LI = QY;
		if(rotMatrix[QZ-1][QZ-1]>rotMatrix[LI-1][LI-1])
			LI = QZ;
		
		LJ	= VR_NXT[LI];
		LK	= VR_NXT[LJ];
		
		LS	= (T)sqrt((double) ( (rotMatrix[LI-1][LI-1] - (rotMatrix[LJ-1][LJ-1]+rotMatrix[LK-1][LK-1])) + 1.0) );
		
		LQ[LI]	= LS*0.5;
		LS		= 0.5/LS;
		LQ[QW]	= (rotMatrix[LJ-1][LK-1]-rotMatrix[LK-1][LJ-1])*LS;
		LQ[LJ]	= (rotMatrix[LI-1][LJ-1]+rotMatrix[LJ-1][LI-1])*LS;
		LQ[LK]	= (rotMatrix[LI-1][LK-1]+rotMatrix[LK-1][LI-1])*LS;
	}
	
	return *this;
}
*/


//////////////////////////////////////////////
//
// General Arithmetic Assignment Operations
//
// Notes:
//
template<class T> MACQuaternion<T>&
MACQuaternion<T>::operator*=	(const MACQuaternion<T>& v)
{
	//MACVector3<T>	arg1	(m_quat);   // WHY NOT?
	//MACVector3<T>	arg2	(v.m_quat);
	
	//m_quat.set	( (arg2*W()) + (arg1*v.W()) + cross(arg1, arg2) );
	//m_quat.W	( (W()*v.W()) - dot3(arg1, arg2) );
	
	//return *this;

	MACQuaternion<T>	v0(*this);
	MACQuaternion<T>&	result		=	*this;

	result[QW] = v0[QW]*v[QW] - v0[QX]*v[QX] - v0[QY]*v[QY] - v0[QZ]*v[QZ];
	result[QX] = v0[QW]*v[QX] + v[QW]*v0[QX] + v0[QY]*v[QZ] - v0[QZ]*v[QY];
	result[QY] = v0[QW]*v[QY] + v[QW]*v0[QY] + v0[QZ]*v[QX] - v0[QX]*v[QZ];
	result[QZ] = v0[QW]*v[QZ] + v[QW]*v0[QZ] + v0[QX]*v[QY] - v0[QY]*v[QX];

	return result;
}



//////////////////////////////////////////////
//
// General Arithmetic Operations
//
// Notes:
//
template<class T> MACQuaternion<T>
MACQuaternion<T>::operator*	(const MACQuaternion<T>& v) const
{
	//MACVector3<T>	arg1	(m_quat);
	//MACVector3<T>	arg2	(v.m_quat);
	MACQuaternion<T> result;
	
	// set w and vector part
	//return MACQuaternion<T>	( (W()*v.W()) - dot3(arg1, arg2), (arg2*W()) + (arg1*v.W()) + cross(arg1, arg2) );
	
	result[QW] = (*this)[QW]*v[QW] - (*this)[QX]*v[QX] - (*this)[QY]*v[QY] - (*this)[QZ]*v[QZ];
	result[QX] = (*this)[QW]*v[QX] + v[QW]*(*this)[QX] + (*this)[QY]*v[QZ] - (*this)[QZ]*v[QY];
	result[QY] = (*this)[QW]*v[QY] + v[QW]*(*this)[QY] + (*this)[QZ]*v[QX] - (*this)[QX]*v[QZ];
	result[QZ] = (*this)[QW]*v[QZ] + v[QW]*(*this)[QZ] + (*this)[QX]*v[QY] - (*this)[QY]*v[QX];

	return result;
}


// Log:
//		Created 2/27/01, Ian Elsley

//////////////////////////////////////////////
//
// Comparitor Functions
//
//



//
// End of Method Code
//
//*********************************************************************
//*********************************************************************

//*********************************************************************
//*********************************************************************
//
// Start of Function Code
//



//***********************************************
//
// I/O Functions
// Notes:
//
//***********************************************

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACQuaternion<T>& v)
{
	return in >> m_quat;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& 
operator<<(std::ostream& out, MACQuaternion<T>& v)
{
	return out << m_quat;
}

//
// End of Function Code
//
//*********************************************************************
//*********************************************************************



MAC_MATH_LIB_END


#endif __MAC_QUATERNION_ROTATION_H__