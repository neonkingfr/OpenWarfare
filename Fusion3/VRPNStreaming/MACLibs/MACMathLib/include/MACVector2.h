//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACVector2.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		MACVector2 class and templates for the Solver Interface. This library is the prototye for the
//		maths library in general.
//
//		MACVector2 has allied functions:
//
//			T				dot		(const MACVector2<T>& v1, const MACVector2<T>& v2);
//			MACVector2<T>	cross	(const MACVector2<T>& v1, const MACVector2<T>& v2)
//
//
//
// Log:
//
// Notes:
//
//		There should perhaps be failure with asserts on access to non existant members
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_VECTOR2_H__
#define __MAC_VECTOR2_H__

#include <iostream>

MAC_MATH_LIB_BEGIN

//***********************************************************************
//***********************************************************************
//
// Instanciated Vectors
//
// Notes:
// Log:
//		Created 28/2/01, Ian Elsley
//
typedef	MACVector2<double>		MACVec2d;
typedef MACVector2<float>		MACVec2f;
typedef MACVector2<int>			MACVec2i;
//
// End of instanciation
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Templated Class Definitions
//


//
// MACVector2
//
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T>
class MACVector2 : public MACVector<T>
{

	private:
		

		T		x;
		T		y;


	public:		/*** Constructors ***/

		MACVector2<T>	()										: x((T)0), y((T)0)		{}
		MACVector2<T>	(T v)									: x(v), y(v)			{}
		MACVector2<T>	(T vx, T vy)							: x(vx), y(vy)			{}
		MACVector2<T>	(T vx, T vy, T vz)						: x(vx), y(vy)			{}
		//MACVector2<T>	(const MACVector<T>& v)					: x(v.X()/v.W()), y(v.Y()/v.W())	{}
		MACVector2<T>	(const MACVector<T>& v)					: x(v.X()), y(v.Y())	{}
		// MACVector2<T>	(const MACVector2<T>& v)					: x(v.X()), y(v.Y())	{}
		// MACVector2<T>	(const MACVector3<T>& v)					: x(v.X()), y(v.Y())	{}

		virtual ~MACVector2<T>	()								{}


	public:		/*** Access - Set and Get ***/

		
		// Standard array access
		virtual T&			operator[]	(const unsigned int i)			{ return (&x)[i]; }
		virtual const T&	operator[]	(const unsigned int i) const	{ return (&x)[i]; }

	
		// Get the values
		virtual T	X()	const									{ return x; }
		virtual T	Y()	const									{ return y; }
		virtual T	Z()	const									{ return (T)0; }	
		virtual T	W()	const									{ return (T)0; }	
																/* This should perhaps fail with an assert */

		// Set the values
		T			X(T v)										{ return x=v; }
		T			Y(T v)										{ return y=v; }
		T			Z(T v)										{ _ASSERT(0); return (T)0; }	
		T			W(T v)										{ _ASSERT(0); return (T)0; }
																/* This should perhaps fail with an assert */

		// Set the values in a group
		MACVector2<T>&	set	(const T& v)						{ x=v; y=v; return *this; }
		MACVector2<T>&	set	(const T& vx, const T& vy, const T& vz)	
																{ x=vx; y=vy; return *this; }
		//MACVector2<T>&	set	(const MACVector<T>& v)				{ x=v.X()/v.W(); y=v.Y()/v.W(); return *this; }
		MACVector2<T>&	set	(const MACVector<T>& v)				{ x=v.X(); y=v.Y(); return *this; }


	public:		/*** General Arithmetic operators ***/

		
		// Assignment
		MACVector2<T>&	operator=	(const MACVector<T>& v)		{ return set(v); }
		MACVector2<T>&	operator=	(const T v)					{ return set(v); }
		MACVector2<T>&	operator+=	(const MACVector2<T>& v);
		MACVector2<T>&	operator+=	(const T v);
		MACVector2<T>&	operator-=	(const MACVector2<T>& v);
		MACVector2<T>&	operator-=	(const T v);
		MACVector2<T>&	operator*=	(const MACVector2<T>& v);
		MACVector2<T>&	operator*=	(const T v);
		MACVector2<T>&	operator/=	(const MACVector2<T>& v);
		MACVector2<T>&	operator/=	(const T v);

		// Arithmetic
		MACVector2<T>	operator+	(const MACVector2<T>& v)	const;
		MACVector2<T>	operator+	(const T v)					const;
		MACVector2<T>	operator-	(const MACVector2<T>& v)	const;
		MACVector2<T>	operator-	(const T v)					const;
		MACVector2<T>	operator*	(const MACVector2<T>& v)	const;
		MACVector2<T>	operator*	(const T v)					const;
		MACVector2<T>	operator/	(const MACVector2<T>& v)	const;
		MACVector2<T>	operator/	(const T v)					const;

		MACVector2<T>&	scale		(const T& v)				{ return ((*this) *= v); }
		MACVector2<T>&	scale		(const MACVector2<T>& v)		{ return ((*this) *= v); }


	public:		/*** Comparitors ***/

		
		bool			equal		(const MACVector2<T>& v, T error = ERROR_VALUE)	const;
		bool			operator==	(const MACVector2<T>& v)							const;
		bool			operator!=	(const MACVector2<T>& v)							const;
		

	public:		/*** Geometric Maths functions ***/

		
		T				lengthSquared	()	const				{ return x*x + y*y; }
		T				length			()	const;
		T				mod				()	const				{ return length(); }

		MACVector2<T>&	normalize		();

		T				dot				(const MACVector2<T>& other)	const;
		

	public:		/*** Allied functions ***/

		
		friend T			dot		(const MACVector2<T>& v1, const MACVector2<T>& v2);
		// friend MACVector2<T>	cross	(const MACVector2<T>& v1, const MACVector2<T>& v2);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these


	protected:	/*** IO functions ***/

		
		friend std::istream& operator>>(std::istream&, MACVector2<T>&);
		friend std::ostream& operator<<(std::ostream&, MACVector2<T>&);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these

};

//
// End of Class Definitions
//
//***********************************************************************
//***********************************************************************



//***********************************************************************
//***********************************************************************
//
// Templated Method Code
//

//***********************************************
//
// General Arithmetic Assignment Operations
//
// Notes:
//

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>&
MACVector2<T>::operator+=	(const MACVector2<T>& v)
{
	x += v.X(); y += v.Y();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>&
MACVector2<T>::operator+=	(const T v)
{
	x += v; y += v;
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>&
MACVector2<T>::operator-=	(const MACVector2<T>& v)
{
	x -= v.X(); y -= v.Y();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>&
MACVector2<T>::operator-=	(const T v)
{
	x -= v; y -= v; 
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>&
MACVector2<T>::operator*=	(const MACVector2<T>& v)
{
	x *= v.X(); y *= v.Y(); 
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>&
MACVector2<T>::operator*=	(const T v)
{
	x *= v; y *= v; 
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>&
MACVector2<T>::operator/=	(const MACVector2<T>& v)
{
	x /= v.X(); y /= v.Y(); 
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>&
MACVector2<T>::operator/=	(const T v)
{
	x /= v; y /= v; 
	return *this;
}


//****************************************
//
// General Arithmetic Operations
//
// Notes:
//

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>
MACVector2<T>::operator+	(const MACVector2<T>& v)	const
{
	return MACVector2<T>(x+v.X(), y+v.Y());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>
MACVector2<T>::operator+	(const T v)					const
{
	return MACVector2<T>(x+v, y+v);
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>
MACVector2<T>::operator-	(const MACVector2<T>& v)	const
{
	return MACVector2<T>(x-v.X(), y-v.Y());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>
MACVector2<T>::operator-	(const T v)					const
{
	return MACVector2<T>(x-v, y-v);
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>
MACVector2<T>::operator*	(const MACVector2<T>& v)	const
{
	return MACVector2<T>(x*v.X(), y*v.Y());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>
MACVector2<T>::operator*	(const T v)					const
{
	return MACVector2<T>(x*v, y*v);
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>
MACVector2<T>::operator/	(const MACVector2<T>& v)	const
{
	return MACVector2<T>(x/v.X(), y/v.Y());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector2<T>
MACVector2<T>::operator/	(const T v)					const	
{
	return MACVector2<T>(x/v, y/v);
}


//**********************************************
//
// Comparitor Functions
//
//

// 
// MACVector2<T>::equal(const MACVector2<T>& v, T error = ERROR_VALUE)	const
// Notes:
//		Allied with this function are the explicit instanciations of the float and double cases
//		These are identical, but are implemented like this for speed.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACVector2<T>::equal(const MACVector2<T>& v, T error)					const
{
	return ( ((T)fabs((double)(x-v.X())) < error) && ((T)fabs((double)(y-v.Y())) < error) );
}

//
// Equal
// Notes:
//		Double version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACVector2<double>::equal(const MACVector2<double>& v, double error)	const
{
	return ( (fabs(x-v.X()) < error) && (fabs(y-v.Y()) < error) );
}

//
// Equal
// Notes:
//		Float version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACVector2<float>::equal(const MACVector2<float>& v, float error)		const
{
	return ( (fabsf(x-v.X()) < error) && (fabsf(y-v.Y()) < error) );
}


//
// MACVector2<T>::operator==(const MACVector2<T>& v) const
// Notes:
//		This function may not work entirely as expected. 
//		There may be an error factor required.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACVector2<T>::operator==(const MACVector2<T>& v)						const
{
	return ( (x==v.X()) && (y==v.Y()) );
}


//
// MACVector2<T>::operator!=(const MACVector2<T>& v) const
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACVector2<T>::operator!=(const MACVector2<T>& v)						const
{
	return (!((*this) == v));
}


//***********************************************************************
//***********************************************************************
//
// Geometric Math Functions
//
//

// 
// Length
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline T
MACVector2<T>::length() const
{
	T lenSqr = this->lengthSquared();

	if (lenSqr <= 0) return (T)0;
	return (T)sqrt((double)lenSqr);
}

// 
// Length
// Notes:
// Log:
//		Created 8/2/01, Ian Elsley
//
template<class T>	MACVector2<T>&
MACVector2<T>::normalize		()	
{ 
	T len = this->length();
	if (len > MAC_LENGTH_NEAR_ZERO)
		return ((*this) /= this->length()); 
	else
		return set((T)1, (T)0);
}

// 
// Dot Product
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline T
MACVector2<T>::dot(const MACVector2<T>& v) const
{
	return (x*v.X() + y*v.Y());
}


//
// End of Method Code
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Start of Function Code
//


// 
// Dot Product Function
// Notes:
//		dot product function as opposed to method
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> T
dot(const MACVector2<T>& v1, const MACVector2<T>& v2)
{
	return (v1.X()*v2.X() + v1.Y()*v2.Y());
}


//**************************************************
//
// I/O Functions
//
// Notes:
//

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACVector2<T>& v)
{
	int i;
	for (i=0; i<2; i++)
		in >> v[i];
	return in;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& operator<<(std::ostream& out, MACVector2<T>& v)
{
	out << x << ' ' << y;
	return out;
}

//
// End of Function Code
//
//***********************************************************************
//***********************************************************************


MAC_MATH_LIB_END

#endif __MAC_VECTOR2_H__