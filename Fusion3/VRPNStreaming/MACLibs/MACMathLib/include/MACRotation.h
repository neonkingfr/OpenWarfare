//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACRotation.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Generalised abstract rotation header class.
//		This will be used by all other rotation types as the generic header class.
//		In the main, all accesses to these classes will be via this.
//
//
//
// Log:
//		Created 3/1/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_ROTATION_H__
#define __MAC_ROTATION_H__

MAC_MATH_LIB_BEGIN

//********************************************
//
// Predefinition of classes
//

template<class T> class MACAngleAxisRotation;
template<class T> class MACAzimuthElevationRotation;
template<class T> class MACEulerRotation;
template<class T> class MACQuaternion;
template<class T> class MACRotationMatrix3;
template<class T> class MACHomogeneousMatrix4;


//*********************************************************************
//*********************************************************************
//
// Start of templated class deinitions
//

template<class T>
class MACRotation : public MACTransform<T>
{

	private:

	public:		/*** Contructors and destructors ***/

		virtual ~MACRotation<T>()		{}
	
	public:		/*** shared scaling routines ***/


	public:		/*** shared get members ***/

	//	virtual MACRotation<T>&	set	(const MACAngleAxisRotation<T>& v)	= 0;
	//	virtual MACRotation<T>&	set	(const MACEulerRotation<T>& v)		= 0;
	//	virtual MACRotation<T>&	set	(const MACQuaternion<T>& v)			= 0;
	//	virtual MACRotation<T>&	set	(const MACRotationMatrix3<T>& v)		= 0;
};

//
// End of templated class definitions
//
//*********************************************************************
//*********************************************************************


MAC_MATH_LIB_END

//********************************************
//
// Includes
//

#include "MACAngleAxisRotation.h"
#include "MACAzimuthElevationRotation.h"
#include "MACEulerRotation.h"
#include "MACQuaternion.h"
#include "MACRotationMatrix3.h"


#endif __MAC_ROTATION_H__