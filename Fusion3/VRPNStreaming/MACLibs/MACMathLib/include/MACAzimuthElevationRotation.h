//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACAzimuthElevationRotation.h
// Author:		Ian Elsley
// Created:		12th April, 2001
//
//
// Documentation:
//		Holds a subset of the EulerRotation, with the cases tied to a Azimuth-elevation form
//		Convenience functions are provided
//		
//
//
//
// Log:
//		Created 3/1/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_AZIMUTH_ELEVATION_ROTATION_H__
#define __MAC_AZIMUTH_ELEVATION_ROTATION_H__

#include <iostream>
#include <float.h>
#include "MACEulerRotation.h"

MAC_MATH_LIB_BEGIN

// Hard coded. This is set. 
// NOTE: Update to consts on next revision. Ian
#define MAC_AZIMUTH_ELEVATION_DEFAULT_AXES	(MACEulerOrder::MACEulOrdXZYs)
#define	MAC_AE_AZIMUTH						(2)
#define MAC_AE_ELEVATION					(1)
#define MAC_AE_AIM							(0)


//***********************************************************************
//***********************************************************************
//
// Instanciated Rotations
//
// Notes:
// Log:
//		Created 28/2/01, Ian Elsley
//

typedef	MACAzimuthElevationRotation<double>	MACAzElRotd;
typedef MACAzimuthElevationRotation<float>	MACAzElRotf;


//
// End of instanciation
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Templated Class Definitions
//

//
// MACAzimuthElevationRotation
// Notes:
//		Holds a subset of the EulerRotation, with the cases tied to a Azimuth-elevation form
//		Convenience functions are provided
// Log:
//		Created 12th April 2001, Ian Elsley
//
template<class T>
class MACAzimuthElevationRotation : public MACEulerRotation<T>
{
	private:	/*** Variables ***/

		static MACEulerOrder	m_defaultOrder;		

	
	public:		/*** Constructors and destructors ***/

		MACAzimuthElevationRotation<T>()							: MACEulerRotation<T>()
																	{ Order(getDefaultOrder()); }
		MACAzimuthElevationRotation<T>(T azimuth, T elevation)		: MACEulerRotation<T>()
																	{ Order(getDefaultOrder()); 
																	  setAzimuth(azimuth); 
																	  setElevation(elevation);
																	  setAim((T)0); }
		
		// construct from other rotation types
		MACAzimuthElevationRotation<T>	(const MACEulerRotation<T>& v)		{ set(v, getDefaultOrder());		}
		MACAzimuthElevationRotation<T>	(const MACAngleAxisRotation<T>& v)	{ Order(getDefaultOrder()); set(v); }
		MACAzimuthElevationRotation<T>	(const MACQuaternion<T>& v)			{ Order(getDefaultOrder()); set(v); }
		MACAzimuthElevationRotation<T>	(const MACRotationMatrix3<T>& v)	{ Order(getDefaultOrder()); set(v); }
		
		virtual ~MACAzimuthElevationRotation	()					{}


	public:		/*** Default order calls ***/

		// default orders
		// NOTE: You should not be setting default orders on this
		virtual	MACEulerRotation<T>&	setDefaultOrder	(unsigned int order)			{ _ASSERT(0); return *this; }			
		virtual	MACEulerRotation<T>&	setDefaultOrder	(const MACEulerOrder& order)	{ _ASSERT(0); return *this; }
		virtual const MACEulerOrder&	getDefaultOrder ()								{ return m_defaultOrder; }


	public:		/*** Set and Get ***/

		// Conversions
		MACAzimuthElevationRotation<T>&	set	(const MACEulerRotation<T>& v);
		MACAzimuthElevationRotation<T>&	set	(const MACAngleAxisRotation<T>& v);
		MACAzimuthElevationRotation<T>&	set	(const MACQuaternion<T>& v);
		MACAzimuthElevationRotation<T>&	set	(const MACRotationMatrix3<T>& v);

		MACAzimuthElevationRotation<T>&	set	(const MACVector<T>& v);

		//T	setAzimuth		(T v)			{ return Y(v);	}
		//T	getAzimuth		()				{ return Y();	}
		//T	setElevation	(T v)			{ return X(v);	}
		//T	getElevation	()				{ return X();	}
		T	setAzimuth		(T v)			{ return (*this)[MAC_AE_AZIMUTH] = v;	}
		T	getAzimuth		()				{ return (*this)[MAC_AE_AZIMUTH];		}
		T	setElevation	(T v)			{ return (*this)[MAC_AE_ELEVATION] = v;	}
		T	getElevation	()				{ return (*this)[MAC_AE_ELEVATION];		}

	private:	/** for internal use only ***/

		T	setAim			(T v)			{ return (*this)[MAC_AE_AIM] = v;		}
		T	getAim			()				{ return (*this)[MAC_AE_AIM];			}
	
		MACAzimuthElevationRotation<T>&	verify();
};


//
// End of Templated Class Definitions
//
//***********************************************************************
//***********************************************************************

//*********************************************************************
//*********************************************************************
//
// Templated Method Code
//

template<class T> MACAzimuthElevationRotation<T>&
MACAzimuthElevationRotation<T>::set	(const MACEulerRotation<T>& v)
{
	MACEulerRotation<T>::convert(v);
	return verify();
}

template<class T> MACAzimuthElevationRotation<T>&
MACAzimuthElevationRotation<T>::set	(const MACAngleAxisRotation<T>& v)
{
	MACEulerRotation<T>::set(v);
	return verify();
}

template<class T> MACAzimuthElevationRotation<T>&
MACAzimuthElevationRotation<T>::set	(const MACQuaternion<T>& v)
{
	MACEulerRotation<T>::set(v);
	return verify();
}

template<class T> MACAzimuthElevationRotation<T>&
MACAzimuthElevationRotation<T>::set	(const MACRotationMatrix3<T>& v)
{
	MACEulerRotation<T>::set(v);
	return verify();
}

//
// Notes:
//		***	UNTESTED IN CURRENT FORM ***
//		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
template<class T> MACAzimuthElevationRotation<T>&
MACAzimuthElevationRotation<T>::set	(const MACVector<T>& v)
{
	MACVector3<T>	vec1(v), vec2(v);

	//
	// do azimuth first
	//
	vec1[MAC_AE_AZIMUTH] = (T)0;
	vec1.normalize();

	//
	// do now elevation
	//
	vec2[MAC_AE_ELEVATION] = (T)0;
	vec2.normalize();

	setAzimuth( (T)asin( (double)vec1[MAC_AE_ELEVATION] ) );
	
	// correct for angles > 90
	if (vec1[MAC_AE_AIM] < 0)
	{
		if (getAzimuth() < 0)
			setAzimuth ( ((T)-MAC_PI) - getAzimuth() );

		else
			setAzimuth ( ((T)MAC_PI) - getAzimuth() );
	}

	//
	// do now elevation
	//
	vec2[MAC_AE_ELEVATION] = (T)0;
	vec2.normalize();
	
	setElevation( (T)asin( (double)vec2[MAC_AE_AZIMUTH] ) );

	// correct for angles > 90
	if (vec2[MAC_AE_AIM] < 0)
	{
		if (getElevation() < 0)
			setElevation ( ((T)-MAC_PI) - getElevation() );

		else
			setElevation ( ((T)MAC_PI) - getElevation() );
	}

	//
	setAim( (T)0 );

	return *this;
}



//
// Notes:
//		Verify will test to see if Azimuth and elevation is being adhered to
//		and will correct if not...
// log:
//		Created: 4/18/2001, Ian Elsley		
template<class T> MACAzimuthElevationRotation<T>&
MACAzimuthElevationRotation<T>::verify()
{
	setAim( (T)0 );
	return *this;
}


//
// End of Templated Method Code
//
//*********************************************************************
//*********************************************************************


MAC_MATH_LIB_END

#endif __MAC_AZIMUTH_ELEVATION_ROTATION_H__