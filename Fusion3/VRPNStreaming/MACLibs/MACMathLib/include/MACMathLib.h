//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACMathLibs.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Base
//
//
//
// Log:
//
// Notes:
//
//		There should perhaps be failure with asserts on access to non existant members
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_MATH_LIB_H__
#define __MAC_MATH_LIB_H__

#include <cmath>
#include <iostream>
#include <crtdbg.h>

// NOTE: Convert to consts
#define MAC_ML_VERSION							(0x0)
#define MAC_ML_RELEASE							(0x0)
#define MAC_ML_SUB_VERSION						(0x0)
#define MAC_ML_BUILD							(0x0)
#define MAC_MATH_LIB_VERSION					((MAC_ML_VERSION<<24) | (MAC_ML_RELEASE<<16) | (MAC_ML_SUB_VERSION<<8) | MAC_ML_BUILD)

//#define MATH_LIB_BEGIN	namespace MACMath	{
//#define MATH_LIB_END	};
//Removed due to compiler error on namespace. Reinsert on full revision. NOT BEFORE! Ian 2/9/2001
#define MAC_MATH_LIB_BEGIN						;
#define MAC_MATH_LIB_END						;

MAC_MATH_LIB_BEGIN
// Code inserted here

// NOTE: Convert to consts
#define MAC_N_NEAR_ZERO							(0.000005)
#define	MAC_LENGTH_NEAR_ZERO					(0.00000005)
#define MAC_ANGLE_NEAR_ZERO						(0.000005)

#define MAC_MATRIX_INVERSION_PRECSION_LIMIT		(0.000005)
#define MAC_MATRIX_ERROR_LIMIT					(0.000005)
#define MAC_VECTOR_ERROR_LIMIT					(0.000005)
#define MAC_EULER_ERROR_LIMIT					(0.0005)
#define MAC_ANGLE_AXIS_ERROR_LIMIT				(0.0005)
#define MAC_QUATERNION_ERROR_LIMIT				(0.0005)


#define MAC_2_PI								(6.283185307179586476925286766559)
#define MAC_PI									(3.1415926535897932384626433832795)
#define MAC_PI_2								(1.5707963267948966192313216916398)
#define	MAC_PI_4								(0.7853981633974483096156608458199)
#define MAC_PI_8								(0.3926990816987241548078304229099)


#define	MAC_BOUND(v, low, high)					{if ((v)<(low)) v+=((high)-(low)); else if ((v)>(high)) v-=((high)-(low));}
#define MAC_NEAR(v1, v2, error)					((((v1)-(error))<=(v2)) && (((v1)+(error))>=(v2)))
#define MAC_SQR(val)							((val)*(val))

MAC_MATH_LIB_END

#include "MACVector.h"
#include "MACTransform.h"

#endif __MAC_MATH_LIB_H__