#ifndef __MACLICENSE_H__
#define __MACLICENSE_H__

#include <iostream>
#include <string>

#if defined(WIN32) || defined(WIN64)
	extern "C" int bDongleIsRequired; 
	extern "C" int MaCheckLicense(char *product_name);
#endif

#if defined(WIN32) || defined(WIN64)
	extern int MaCheckLicense(char *product_name);
#endif

#if defined(WIN32) || defined(WIN64)
	bool			MACLIC_IsLicensed(const char *license, bool dongleRequired = false);
	bool			MACLIC_IsLicensed(std::string license, bool dongleRequired = false);
	std::string		MACLIC_GetStatus(std::string license, bool dongleRequired = false);
#else
	bool			MACLIC_IsLicensed(const char *license, bool dongleRequired = true);
	bool			MACLIC_IsLicensed(std::string license, bool dongleRequired = true);
	std::string		MACLIC_GetStatus(std::string license, bool dongleRequired = true);
#endif

#endif
