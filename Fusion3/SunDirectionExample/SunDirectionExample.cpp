/****************************************************************************
* Plugin to demonstrate functionality needed by WTSS project
****************************************************************************/

#pragma warning(disable: 4996)
#pragma warning(disable: 4251)

#define _BISIM_DEV_GET_SUN_DIRECTION 1

#include <stdio.h>
#include "SunDirectionExample.h"

#include <util\WorldUtilities.h>
#include <util\EffectsUtilities.h>

using namespace VBSFusion;

// global variables
float timeCounter = 0.0f;             // variable to count time between 2 logs

// global constants
const float LogPeriod = 2.0f;               // how often should messages be logged and shot created (in seconds)

#define _DIAG 1
#define LOG_HEADER "[SunDirectionExample] "
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugStr(buffer);
#endif
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  timeCounter += deltaT;
  if (timeCounter > LogPeriod)
  {
    VBSFusion::position2D dirToSun = WorldUtilities::getDirectionToSun();
    LogDiagMessage(LOG_HEADER"Sun direction: [%f, %f]\n", dirToSun.getX(), dirToSun.getY());
  }
}

VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  VBSFusion::EffectsUtilities::playMusic("ATrack6", 0);
};

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}

