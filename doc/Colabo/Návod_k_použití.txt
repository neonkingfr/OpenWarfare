﻿[Instalace](https://projects.bistudio.com/Colabo/Návod_k_instalaci.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1520)
=========

Viz [Návod k instalaci](https://projects.bistudio.com/Colabo/Návod_k_instalaci.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1520)


Ovládání
========

Základní operace
-------------------------

- **Založení nového dokumentu**: vlevo nahoře ikonka pro přidání příspěvku (příp. *Ctrl + N*), vyplní se Title a volitelně i URL a CML databáze a příspěvek je možné odeslat. 
- **Příspěvek jako potomek**: zvol příspěvek, kterému chceš přidat potomka, a dej pravou myší konextové menu a v něm Reply (*Ctrl + R*)
- **Poznámka na konec dokumentu**: z kontextového menu pravou myší Append Note (*Ctrl + M*)
- **Editace dokumentu**: double click v seznamu příspěvků, příp. v kontextovém menu Edit
- **Označit jako přečtené**: v kontextovém menu Mark As Read (*M* anebo *Ctrl + Q*)

[Klávesové zkratky](https://projects.bistudio.com/Colabo/Klávesové_zkratky.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1518)
---


[Formátování](https://projects.bistudio.com/Colabo/Coma-tahák.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1517)
======

Text je psán v lightweight markupu inspirovaném Markdownem, ale značně pozměneném. Měl by umožnit snadné prohlížení a psaní v běžném textovém režimu, oproti Markdownu např. i snadnou editaci uvnitř citací. Více viz [formátování](https://projects.bistudio.com/Colabo/Coma-tahák.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1517).



Navigace
======

Tag Cloud
---

Vlevo je zobrazeno pole dostupných kombinací tagů. Myší lze kliknout na libovolný z nich a upřesnit tak hledání. V horní řádce je pak výpis zvolených tagů, podle kterých se pak filtrují zobrazené příspěvky. LMB lze přejít na daný tag (tagy za ním se odeberou), RMB lze odebrat libovolný jednotlivý tag z cesty.


Speciální tagy
---
Pod cloudem jsou některé speciální tagy (nazvané též virtuální), umožňují vybrat specifickou kategorii příspěvků (např.nepřečtené **:to read** či pouze z konkrétní CML databáze **:db colabo**


Rule Set
---
Určuje, jak se zobrazují příspěvky.

Základní konvence
===

V současnosti se při práci řídíme [těmito pravidly](https://projects.bistudio.com/Colabo/Pravidla.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1519), kterým také odpovídají default rule sety.




