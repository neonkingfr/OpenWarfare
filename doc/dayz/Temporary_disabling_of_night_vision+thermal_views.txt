﻿Currently hackers are exploiting these, and we don't have any items with them. If there was a simple way we could disable them for now, it might save problems. We would only want to do this for the retail currently as a temporary measure.

[ondra 17/12/2013 14:45:45]:
 A proper measure would be to acquire a tool they use for the purpose and send it to Valve, so that VAC can add detection for it.

[dhall 17/12/2013 2:58:43 p.m.]:
 Roger, but for the rest of the year it serves no purpose as none of our items utilize it.

[ondra 17/12/2013 15:12:55]:
 > it serves no purpose as none of our items utilize it.
 
 You are correct, however cheaters will soon find other variables to manipulate which we can no longer disable/remove, like exposure variables, or changing the shaders.
 
 I am looking how can NV/TI be disabled, I just warn this is probably not worth the effort anyway.

[ondra 17/12/2013 15:26:36]:
 >  We would only want to do this for the retail currently as a temporary measure.
 
 Should be done in 113762
