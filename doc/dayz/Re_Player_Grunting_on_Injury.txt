﻿Nejprve se hit změní na serveru:

(
>	DayZProfile.exe!Entity::ChangeHit(EntityAI * killer=0x28f05600, int part=3, float newHit=0.531100214, bool showDamage=true)	C++
 	DayZProfile.exe!Entity::ApplyDoDamageLocal(const Object::DoDamageResult & result={...}, EntityAI * owner=0x28f05600, RStringCT<char> ammo={...}, const SkinnedPosition & pos={...})	C++
)

(
		newHit	0.531100214	float
		oldHit	0.445701748	float
)

Potom přijde zpráva na klienta:

(
>	DayZProfile.exe!Entity::ChangeHit(EntityAI * killer=0x00000000, int part=3, float newHit=0.531496048, bool showDamage=true)	C++
 	DayZProfile.exe!Entity::TransferMsg(NetworkMessageContext & ctx={...})	C++
)

(
		newHit	0.531496048	float
		oldHit	0.000000000	float
)

Zpráva na klienta pak chodí opakovaně, oldHit je stále 0.

Ve debuggeru se mi nějak nezobrazuje _hit, a to ani po Rebuild All, takže se mi to debugguje trochu hůř, ale snad to brzy bude.


[ondra 18.10.2013 17:12:02]:
 Hitpointy se resetují voláním `SetDamage` v `Object::TransferMsg` při příjmu zprávy.
 
 To jsem tam dal ve 110306, aby se v reakci na poškození vyvolaly i efekty poškození.
 
 Vznikla tak kruhová závislost.

[Attachment1]: https://projects.bistudio.com/dayz/sampleMission.SampleMap_4.zip