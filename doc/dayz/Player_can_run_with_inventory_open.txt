﻿===Build
0.14.102852

===Problem
Player can move around and run with their inventory screen open.

===Repro
1. Run any mission
2. Open inventory screen
3. Move character

===Expected
Inventory screen should immediately close when player attempts to move their character.

[qeberle 07.03.2013 16:18:09]:
 > Inventory screen should immediately close when player attempts to move their character.
 
 What about being pushed by another player/zombie/car/door?
 (so own movement vs moved by others)

[dhall 7/03/2013 4:18:54 p.m.]:
 That's fine, only input related that concerns me really

[dhall 30/09/2013 11:09:19 a.m.]:
 No longer required, closing
