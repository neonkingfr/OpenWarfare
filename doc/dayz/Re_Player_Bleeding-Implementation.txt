﻿[Skype Ondra 2/07/2013 3:10 p.m.]:
 What I am contemplating is how the bleeding location should be detemined. The easiest option seems to be **if the damage itself would create a "bleeding source" in the damage location, and the script would only control its intensity or eventually remove it.**

 Bleeding source... needs to have a position animated, which is why I do not think it is a good idea to pass the position around.

[Skype Dean 2/07/2013 3:12 p.m.]:
 yes, agreed. I think some kind of "bleeding source" would be best, resulting from the damage, just like you describe

[dhall 2/07/2013 3:14:24 p.m.]:
 Target implementation is next week, under ideal conditions
