﻿Problém, na kterým si láme hlavu i komunita: co s bandity (přesněji: co udělat, aby se banditi udrželi trochu na uzdě).

Někdo může říct, že současný stav je realistický, a že ve skutečném světě by tomu nebylo jinak, ale není tomu tak. Ve skutečném světě je pravděpodobné, že by drastické tresty za vraždy, loupeže apod. brzy udržely banditství na rozumné míře (podobně, jako tomu bylo třeba na divokém západě), protože by usvědčené bandity eliminovaly (vězením nebo smrtí). V současném DayZ je problém, že i když usvědčeného banditu potrestáte (zabijete), může v banditství okamžitě pokračovat, žádný ekvilalent vězení ve hře není a asi ani být nemůže.

Napadají mě jen věci, které se ani mně moc nelíbí třeba něco jako "zákaz činnosti na X dní" (třeba že by bandita po svém zabití několik dní nemohl vůbec střílet) nebo "znamení na X dní" (že by bandita po svém zabití několik dní nosil zvláštní skin).
[jirka 3.6.2012 8:04:55]:
 > „zákaz činnosti na X dní“ (třeba že by bandita po svém zabití několik dní nemohl vůbec střílet) 
 
 Možná ekvivalentní tomu by bylo zhoršení výchozích podmínek po respawnu (žádná zbraň, méně nábojů nebo dalších předmětů, zranění / nemoc). To by se dalo i stupňovat podle úrovně humanity.

[maruk 4.6.2012 11:57:24]:
 Já bych preferoval nějaké stigma, které by ale banditovi zůstalo i po respawnu, které by sice nebylo tak patrné, jako skin, ale i tak rozpoznatelné.
 
 Zákaz činnosti či penalizace mi ale naprosto nesedí, bandity a celkově bojování mezi hráči pokládám naopak za důležitou součást hry.

[ondra 4.6.2012 12:03:58]:
 > bandity a celkově bojování mezi hráči pokládám naopak za důležitou součást hry
 
 To já taky, ale současná implementace je nevyvážená, a jak jsem snad ukázal, i nerealistická - to hlavně proto, že smrt není smrt, ale restart - začnu znovu, ale bez vybavení.
 
 Ve skutečném životě bandita riskuje život, ve hře jen to, že přijde o vybavení a ocitne se někde jinde. To činí banditskou strategii daleko výhodnější, než ve skutečnosti.

 Stigma tohle neřeší - to se týká "skrytých" banditů, kteří se snaží hráče ošálit, ale těch otevřených, kteří střílejí na potkání, se nijak nedotkne.

[maruk 4.6.2012 13:41:06]:
 Tak snad jedině umožnit hráčům banditu nějak chytit, exemplárně pověsit a tím mu na nějaký čas i zakázat se přihlásit do hry, třeba na týden až měsíc.

[ondra 4.6.2012 14:56:36]:
 (Jestli jsi to myslel ironicky, měl tam být smaljík - zatím předpokládám, že to myslíš vážně).
 
 Myslel jsem, že to, že banditu zastřelím, je z herního pohledu docela dostatečné přiblížení tomu "chytit a pověsit". Chápu, že by šlo najít něco dokonalejšího, ale implementačně by to bylo o hodně složitější.
 
 Co se týče penalizace, tak zákaz hry v řádu hodin mi připadá dostatečný (možná i dní, týdnů už snad ani ne), delší trest bych nežádal, Jirkou navržené spawnování beze zbraně taky (začít bezbranný je už docela velká nevýhoda, zbraň se sice 
 najít dá, ale rozhodně se beze zbraně nedá uloupit, leda bys narazil na hodně naivního protihráče, takže to donutí hráče naučit se i jinou strategii, než je loupení).

[maruk 4.6.2012 19:29:11]:
 Já jen asi pořád moc nechápu, o jaký problém se jedná a o co usiluješ. Mě připadá, že možnost být banditou k tomu celému naprosto patří a nemám s tím žádný problém. Rovněž si nemyslím, že to je problém celé komunity, co to hraje, nebo dokonce její většiny.
 
 Dokud byly skiny pro bandity, tak banditi byli obecně něco jako štvaná zvěř. Stav v současné době mi ale připadá horší, než předtím. Z mého pohledu by bandita nejen měl mít nějaké poznávací Kainovo znamení, ale toto znamení by dokonce mělo být trvalejší než pro jednu herní session. Trochu je tam ale pak ten problém, že banditi už moc nemají, jak se v budoucnosti napravit.
 
 Rozhodnutí chovat se jako bandita mi ve hře připadá samo o sobě zcela oprávněné, nechápu, proč a jak za to hráče trestat či jim bránit to dělat i v budoucnu.
 
 

[maruk 4.6.2012 20:47:43]:
 >Ve skutečném životě bandita riskuje život, ve hře jen to, že přijde o vybavení a ocitne se někde jinde. To činí banditskou strategii daleko výhodnější, než ve skutečnosti.
 
 To samé se týká všech hráčů, nejenom banditů. A může se týkat i jiných strategií, které hráči použijí, není mi tedy jasné, proč trestat bandity.
 
 Co možná spíš pauza pro všechny, třeba hodinová. Uvolní se servery a vůbec to řeší i některé jiné exploity (třeba sebevraždy kvůli respawnu jinde).

[maruk 4.6.2012 21:26:42]:
 Možná, že to co ti vadí nejsou samotní banditi, ale spíše to, že nektěří hráči ani tak nehrají "hru na přežití" ale prostě hru s respawnem, ve které mohou prostě v klidu zabíjet ostatní hráče. I tak mi ale připadá, že by bylo vhodnější řešit to nějakým univerzálním zásahem a ne snahou o násilné ohýbání pravidel pro tento konkrétní případ.

[jirka 5.6.2012 7:12:27]:
 Souhlasím. Také mi připadá, že pokud je s bandity nějaký problém, neměli bychom do hry zanášet něco umělého, ale nechat se s tím vypořádat "ekosystém" a případně napomoci vyvážení  mechanismy platnými pro všechny (bez ohledu na to jak se chovají).

[ondra 5.6.2012 9:38:00]:
 Problém s bandity je v nerovnováze - je jich (podle mého názoru) příliš mnoho. (Tohle je samosebou věc mínění - na to, kolik je "příliš mnoho" mohou mít různí lidé různé názory).
 
 Přikládám odkazy [z Google hledání](https://www.google.cz/search?as_q=site:dayzmod.com+bandit+problem na dayzmod fóru), že to za problém nepovažuji jen já (jestli je to ale v většinový názor, nebo ne, to fakt nevím).
 
 http://dayzmod.com/forum/showthread.php?tid=6074
 http://dayzmod.com/forum/showthread.php?tid=5984
 http://dayzmod.com/forum/showthread.php?tid=1548
 http://dayzmod.com/forum/showthread.php?tid=10794
 http://dayzmod.com/forum/showthread.php?tid=1195
 
 > A může se týkat i jiných strategií, které hráči použijí, není mi tedy jasné, proč trestat bandity.
 
 To může, ale v současné době o jiné podobné strategii nevím, sám se snažím vždy přežít  za každou cenu, i v zoufalých situacích. Proč trestat zrovna bandity? Proto, že když jich je příliš mnoho, kazí zábavu hráčům, kteří se snaží hrát realisticky, podobně, jako Team Killeři v běžných hrách.
 
 > Co možná spíš pauza pro všechny, třeba hodinová.
 
 Byl bych pro - snad možná jen s modifikací, kdy se pauza nebude vztahovat na začátečníky (např. pro první 4 smrti žádná pauza nebude), protože z počátku, než se člověk opatrnosti naučí, je pár smrtí nevyhnutelných. Jelikož se mi současný stav zdá příliš nevyvážený ve prospěch banditů, tak bych zvažoval ještě další výjimku, a sice, že se pauza nebude vztahovat na oběti banditů, ale pokud by banditů v důsledku tohoto opatření skutečně ubylo, tak to dost možná nebude ani potřeba (je to ale další parametr, kterým se případně dá opatření zesílit).
 

[bebul 5.6.2012 12:20:23]:
 > Někdo může říct, že současný stav je realistický, a že ve skutečném světě by tomu nebylo jinak, ale není tomu tak. Ve skutečném světě je pravděpodobné, že by drastické tresty za vraždy, loupeže apod. brzy udržely banditství na rozumné míře (podobně, jako tomu bylo třeba na divokém západě), protože by usvědčené bandity eliminovaly (vězením nebo smrtí). 
 
 Apokalyptický svět není reálný, resp. je na hony vzdálen od reality, jak jí vnímáme v našich podmínkách. Nefunguje tam žádná infrastruktura, žádná policie, žádná úřední moc, která by těmto věcem mohla učinit přítrž. Den Trifidů...
 Ostatně svět, ve kterém v průměru přežiješ 29 minut nemá s realitou nic společného. Taky se tam všichni snaží jenom přežít? Co blázní? V reálném svétě...
[bebul 5.6.2012 13:06:20]:
 Zvláštní apokalyptický svět. Vypadá, jako by byl zhusta vydrancovanej, ale krom lootování a loupení tam z toho drancování moc nezbylo. Každej hraje sám za sebe. Noční můra otce rodiny je, že by tam musel chránit celou rodinu před znásilňováním, zotročováním a já nevím čím ještě. Tak by vypadal *reálnej* svět banditů.

[ondra 06/06/2012 10:56:48]:
 > Co možná spíš pauza pro všechny, třeba hodinová.
 
 I někdo na fóru uvažuje stejným směrem (i s podobným zdůvoděním):
 
 http://dayzmod.com/forum/showthread.php?tid=12391&pid=116085#pid116085
 
 > make respawning a finite resource
 > ... risking something be engaging in combat
 
 > it is utterly silly to imagine maniac bandits running around in the real world acting like they do in DayZ. They'd be popped and die out or the entire world would be dead and barren and empty.
