﻿> [dhall 7/02/2013 3:39:37 p.m.]:
>  ... 
>  I confirmed it works with the female model, with a few bugs (shadow lod doesn't seem to work for some reason)

Shadows should be fixed in revision 101881. Please test when it will be in the distribution (compiling now).

[qeberle 11.02.2013 17:47:40]:
 They seem to work basically with normal/high/very high.
 But the womens' hands/fingers look weird compared to males/zombies.
 
 Dean can you please check too.
 [#testInfantry.SampleMap.7z][Attachment1] 

 ![p001.jpg][Attachment3] 

  ![p002.jpg][Attachment2] 

[Attachment1]: https://projects.bistudio.com/dayz/%23testInfantry.SampleMap.7z
[Attachment2]: https://projects.bistudio.com/dayz/p002_3.jpg
[Attachment3]: https://projects.bistudio.com/dayz/p001_3.jpg
[qeberle 11.02.2013 17:56:59]:
 Edit: Reading it again - is it about shadow from characters or clothing (on a character)?
