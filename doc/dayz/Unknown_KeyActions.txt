﻿These are the ones with unknown functionality to me:

* FreeHeadMove - A3 stuff?
* lookShiftUp, lookShiftDown, lookRollLeft, lookRollRight, lookShiftCenter - A3 only stuff?
* Action, ActionContext, ActionFreeLook - whats the difference?
* CarHandBrake - A3 I think
* CarAimUp, CarAimDown, CarAimLeft, CarAimRight - same as AimUp, AimDown, AimLeft, AimRight for infantry? why separate?)
* switchCommand - no idea
[Attachment1]: https://projects.bistudio.com/dayz/DZ_KeyGroups.jpg
[ondra 17.12.2012 8:21:47]:
 > CarAimUp, CarAimDown, CarAimLeft, CarAimRight – same as AimUp, AimDown, AimLeft, AimRight for infantry? why separate?
 
 They are separate so that one can configure different keys for land vehicles and infantry.

[qeberle 17.12.2012 08:50:14]:
 Alright. Thanks
