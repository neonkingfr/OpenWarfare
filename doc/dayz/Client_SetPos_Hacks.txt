﻿There are a large amount of script injection hacks coming out similar to the ArmA, and they are predominantly using `setPos` on the client.

We should probably look at only enabling this for the server in network play while the longer term solution is developed, as it is currently spreading like wildfire as we dont not have battleye enabled yet.

[ondra 17.12.2013 22:36:08]:
 This was expected, however "enabling" only for server is not possible, as client needs to have a control over the playable character movement. SetPos is not done by any special message, it directly controls the character. (At least cheaters are no longer able to teleport other players in dayz)
 
 Server needs to verify the client movements are within reasonable limits, and to reject unreasonable ones.
 
 See [DayZ - soupis programátorských věcí k předání](https://projects.bistudio.com/dayz/DayZ-soupis_programátorských_věcí_k_předání.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2700) item "Kontrola simulace klienta".
 
 

[ondra 17.12.2013 22:39:07]:
 Another similar attack vector, where server verification is intended, but not done yet, is ExplosionDamage processing, where server should verify client has the ammo causing the damage available. See [DayZ - soupis programátorských věcí k předání](https://projects.bistudio.com/dayz/DayZ-soupis_programátorských_věcí_k_předání.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2700)
 
 > ovšem nedělá se zatím žádná zamýšlená kontrola:
 

[ondra 17.12.2013 22:40:36]:
 Similar to [Temporary disabling of night vision + thermal views](https://projects.bistudio.com/dayz/Temporary_disabling_of_night_vision+thermal_views.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=3018), it might be more efficient to get access to the tools used for script injection and report them to VAC for detection.
 
