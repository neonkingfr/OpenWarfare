﻿> Clients currently seem to always show the "modified data" message when joining

Only fist idea: the problem could be caused by original (i.e. canonical) hash missing in the shrinked version of addons (as servers are LITE).
I will look how we have proceeded in arrowhead and how does Jenkins do it now.

[ondra 13.12.2013 12:56:15]:
 Zajímá nás to v Dayz vůbec? Nestačí nám, že mají data podepsaná?
 

[bebul 13.12.2013 13:24:13]:
 Dost možná nás to nezajímá, přesto bych rád věděl, co se stalo. Myslím, že problém je nyní i v Arrowheadu, možná od chvíle, kdy Beta Patche převzal Jenkins jsme na něco zapomněli. Rád bych to měl aspoň zdokumentované.

[dhall 13/12/2013 1:27:58 p.m.]:
 Would be good to confirm how we can enforce correct data, as now players can connect with different data. But perhaps even with different data, it still checks signatures? If so, that is fine. We just need to make sure players don't have hacked data.

[bebul 16.12.2013 16:47:35]:
 Yes, the "modified data" are still subject to signature checking.

[bebul 16.12.2013 17:04:07]:
 ====Observation
 Canonical hash is stored in eacy LITE addon header as property ( hash ), *except for* ( bin.pbo )
 But the lite and full bin.pbo does NOT differ at all!