﻿> Při hraní DayZ člověk občas narazí na nějaku protivnou chybu v A2OA, která třeba ve hře samotné tolik nevadí, ale v DayZ má kvůli odlišné herní mechanice odlišné následky.

Stalo se mi v Gorce:

- plazil jsem se a pokusil jsem se vplížit do budovy dveřmi, které byly cca 15 cm nad úrovní terénu. Jestli si to dobře pamatuji, jednalo se o tuto budovu:
 ![gorka-schudek.jpg][Attachment1])
- moje postava se najednou zranila, udělaly se mi zlomeniny a začal jsem krvácet

Podařilo se mi něco takového reprodukovat, když jsem během plížení do dveří ty dveře zavřel (jednalo se o ty dvojité dveře vlevo). Když to udělám šikovně, něco si přitom zlomím a dostanu šok. Nepamatuji si sice, když se mi to stalo, že bych se snažil dveře zavírat, ale vyloučit to taky nedovedu.

Přikládám [repro][Attachment2] - není to v DayZ, takže neuvidíte zlomeninu, ale hra vás zraní taky.

- lehněte si
- posuňte se cca 50 cm vpravo (abyste měli hranu červeného prahu zrovna uprostřed) a kousek dozadu
- kursorem ukažte na dveře a zavřete je

Pozorované: dveře vás zraní, někdy i zabijí
Očekávané: ačkoliv i ve skutečnosti je možné se sám zranit o dveře, je to velmi řídké a jedná se maximálně o bouli nebo zlomený prst, že by si člověk takhle dokázal nechtěně zlomit celou končetinu, nebo se dokonce zabít to dost pochybuji.
[Attachment1]: https://projects.bistudio.com/dayz/gorka-schudek.jpg
[Attachment2]: https://projects.bistudio.com/dayz/gorka-step.Chernarus.zip