﻿/Multiplayer flow/ is still in unsatisfactory/disappointing state.
Among other things it is buggy.
I would like to 1) identify the buggy/broken parts and 2) fix them.
We can change the flow using some automatic server selection or restart after death only thenafter.

BUG LIST
1. persistent server using ( class missions ) in its ( server.cfg ) is unplayable
 * wait for host screen never ends
2. we would like players connect and start the MP mission using *JIP*
 * note: the player starting the mission is now traversing the client states different way while communicating with server. It is hard to maintain both JIP and player starting the game /connecting\/start mission/ scenarios.
3. ( #missions ) command does not work as supposed

[bebul 21.5.2013 17:48:40]:
 Ani jeden bod v BUG LISTu není tak úplně pravdivej.
 S korektní roleless misí jsem schopen se připojit, odpojit, mít misi presistentní, vždycky, zdá se, se připojuju pomocí JIP. Vypadá to, že více problému bude v připojování na hostujícího klienta.
 Udělal jsem si přehled, kde všude se mění stav klienta a serveru, ale je to strašně divoký.
 Co určitě nefunguje je automatické spuštění persistentní mise, aniž by byl připojen nějaký klient (tj. bez hráče).
 Rád bych ale nějak vizualizoval, jak se vlastně přechází mezi stavy a odkud, ale moc nevím jak (FSMEditor?) a jestli to vůbec bude k něčemu.

[bebul 22.5.2013 11:24:09]:
 Pokud je nyní dedikovaný server specifikován jako persistentní a má pouze jednu misi uvedenou v ( class Missions ), spustí se tato ihned po spuštění serveru. Na žádné hráče se nečeká.
 /rev.105600/
 Test case ečxample:
 (
 persistent=1;
 class Missions
 {
 	class BebulRoleLessTest				// name for the mission, can be anything
 	{
 		template = "RoleLessTest.SampleMap";	// omit the .pbo suffix
 		difficulty = "regular";			// difficulty: recruit, regular, veteran & expert as specified in *.Arma2profile
 	};
 };
 
 )

[bebul 23.5.2013 11:51:50]:
 Testuji následující kombinace:
  1. persistentní mise: ano / ne
  2. klient vyskočí pomocí: ( #login #missions ) nebo (/ESC/ + /Exit/)
 Hra nyní:
  a) někdy crashuje => mám repro, nejspíš půjde o něco z Roleless missions implementace
  b) v případě (/ESC/ + /Exit/) se automaticky spustí mise, jako kdyby byla persistentní
 

[bebul 24.5.2013 14:47:07]:
 /Výše zmíněné problémy poladěny v rev.105704/
 
 Info
 ===
 Hlubším zkoumáním se ukázalo, že /MP Flow/ charakterizovaný posloupností ( NetworkClientState ), kterými klient během spouštění/přihlašování k misi na dedikovaném serveru procházel, je značně chaotický:
 (
     16:05:18 Server: OnClientStateChanged Bebul to MISSION SELECTED
     16:05:18 Server: SetServerState to SELECTING MISSION       //<-- důsledek apply votingu, chaotické, ale vypadá to, že fungující
     16:05:19 Server: SetServerState to ASSIGNING ROLES
     16:05:19 Server: OnClientStateChanged Bebul to LOGGED IN   //<-- důsledek předchozího, reakce OnServerStateChanged na SELECTING MISSION
     16:05:19 Server: SetServerState to SENDING MISSION
     16:05:19 Server: SetServerState to LOADING GAME
     16:05:25 Server: OnClientStateChanged Bebul to MISSION SELECTED
     16:05:25 Server: OnClientStateChanged Bebul to MISSION ASKED
     16:05:26 Server: OnClientStateChanged __SERVER__ to GAME LOADED
     16:05:26 Server: SetServerState to PLAYING
     16:05:26 Server: OnClientStateChanged __SERVER__ to PLAYING
     16:05:26 Server: OnClientStateChanged Bebul to MISSION SELECTED   //<-- NEPŘIJATELNÉ, proč by se měl klient vracet z již jednou nabyté NCSMissionAsked?
     16:05:26 Server: OnClientStateChanged Bebul to MISSION ASKED
     16:05:26 Server: OnClientStateChanged Bebul to PLAYER ASSIGNED
 )
 Po dohledání míst, která přechody mezi stavy způsobují se ukázalo, že jejich odstranění vede ke crashům způsobeným v rozbitém mapování ( PlayerToObjects ). Problém, zdá se, byl nakonec způsoben tím, že v implementaci této optimalizace v rev.99112 (15.11.2012) /Patch: Optimized: Servers with many players (>20) should run faster/, se změnila sémantika klíčové metody ( NetworkObjectInfo::CreatePlayerObjectInfo ), kde před Ondrovou změnou bylo:
 (
    NetworkPlayerObjectInfo *info = GetPlayerObjectInfo(player);
    if (info) return info;
 )
 avšak po rev.99112 pouze přidání natvrdo:
 (
    int index = playerToObjects.Add();
    DoAssert(index==player);
 )
 Ukázalo se, že v okrajových případech může dojít ke dvojímu volání ( NetworkObjectInfo::CreatePlayerObjectInfo ), a to:
  a) v ( CreateObject ), pokud už je klient připojený, tak se pro něj rovnou vytvoří
  b) jednorázově v okamžiku, kdy klient přejde do ( NCSPlayerAssigned ) (/důležité v JIP/)
 Pokud ovšem hráč již je připojený a existuje mapování ( playerToObjects ) (např. po ( #login + #missions )) dochází tak k ( CreatePlayerObjectInfo ) 2x
 Opravil jsem přidáním podmínky:
 (
   if (player < playerToObjects.Size())
   {
     return playerToObjects[player];
   }
 )
 Tj. *dovoluji* volání ( CreatePlayerObjectInfo ) dvakrát!
