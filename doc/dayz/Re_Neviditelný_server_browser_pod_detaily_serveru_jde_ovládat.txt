﻿> dialog jde ovládat, i když vůbec není vidět.

(Ne)šikovným klikáním je pak možné hru i shodit (to jsem zkoušel jen při zobrazeném browseru, ale zdá se mi, že na tom, jestli je zobrazený, vůbec nezáleží).

Stačí kliknout znovu na tlačítko Details, pak to nějak spadne:

(
 	DayZProfile.exe!Display::OnSimulate(vehicle)	C++
 	DayZProfile.exe!DisplayMultiplayerDetails::OnSimulate(vehicle)	C++
>	DayZProfile.exe!Display::SimulateHUD(vehicle)	C++
 	DayZProfile.exe!World::SimulateDisplays(deltaT)	C++
 	DayZProfile.exe!World::Simulate(deltaT, enableSceneDraw, enableHUDDraw)	C++
)

[ondra 19.12.2013 15:56:20]:
 V `CreateChild(new DisplayMultiplayerDetails(this));` se volá `~DisplayMultiplayerDetails`. 
 V tu chvíli ovšem ještě stará instance `DisplayMultiplayerDetails` existuje na callstacku:
 
 n(
  	DayZProfile.exe!DisplayMultiplayerDetails::~DisplayMultiplayerDetails()	C++
  	DayZProfile.exe!DisplayMultiplayerDetails::`scalar deleting destructor'(unsigned int)	C++
  	DayZProfile.exe!enf::Shape::OnUnused()	C++
  	DayZProfile.exe!DisplayMultiplayer::OnButtonClicked(int idc=166)	C++
  	DayZProfile.exe!CButton::DoClick()	C++
  	DayZProfile.exe!CButton::OnLButtonUp(float x=1.08187866, float y=1.07616985)	C++
  	DayZProfile.exe!ControlsContainer::OnLButtonUp(IControl * ctrlFound=0x22c8b90c, float mouseX=1.08187866, float mouseY=1.07616985, bool shift=false, bool control=false, bool alt=false)	C++
  	DayZProfile.exe!ControlsContainer::OnSimulate(EntityAI * vehicle=0x00000000)	C++
  	DayZProfile.exe!Display::OnSimulate(EntityAI * vehicle=0x00000000)	C++
  	DayZProfile.exe!DisplayMultiplayer::OnSimulate(EntityAI * vehicle=0x00000000)	C++
 >	DayZProfile.exe!DisplayMultiplayerDetails::OnSimulate(EntityAI * vehicle=0x00000000)	C++
 )
 

[ondra 19.12.2013 16:18:31]:
 Pád jsem opravil v 113851, přidal jsem metodu `OnSimulateBackground`.
