﻿Anything changed with these scripting commands?

(
clearMagazineCargo _this;
clearWeaponCargo _this;

_weapons = [] call compile preprocessFileLineNumbers "createWeaponList.sqf";
{
	diag_log _x;
	_this addWeaponCargo [_x,1];
} forEach _weapons;

_magazines = _weapons call compile preprocessFileLineNumbers "createMagazineList.sqf";
{
	diag_log _x;
	_this addMagazineCargo [_x,12];
} forEach _magazines;

_items = [] call compile preprocessFileLineNumbers "createItemList.sqf";
{
	diag_log _x;
	_this addWeaponCargo [_x,1];
} forEach _items;
)

Repro:
1) Editor
2) Launch this mission:
https://dayz-svn.bistudio.com/svn/trunk/MissionDesign/testmission/#testWeaponsMagazines.SampleMap
3) Access the ammo create via action menu
4) Note the left container is empty
[jirka 18.2.2013 10:07:46]:
 Are you sure the container is properly configured for a new inventory system? The space available within is set by a config value ( itemsCargoSize[] ). Only items which fits into the storage are spawned using functions you listed.
 
 Remark: Please, try to use function ( createInCargo ) instead, functions working with magazines and weapons will be removed later. 

[qeberle 18.02.2013 11:16:56]:
 Thanks. Problem should be itemsCargoSize. Will fix.
 
 Also thanks for the remark :)

[qeberle 21.02.2013 09:07:25]:
 Done.
 
 Unfortunately one cannot access ammoboxes at all now.
 To clarify: I get no gear action/way to interact with the ammo box.
 
 Maybe gear action still checks for (max)transportXXX config value/classes?

 (
 getArray (configFile >> "CfgVehicles" >> typeof cursorTarget >> "itemsCargoSize")
 => [48,8]
 )

[jirka 21.2.2013 11:35:34]:
 Fixed in revision 102288. (condition when to offer Gear action extended to check itemsCargoSize)
 
 Please, confirm.

[qeberle 21.02.2013 14:54:04]:
 Works. Thanks!
