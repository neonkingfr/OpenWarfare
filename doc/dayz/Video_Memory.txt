﻿We implemented better and easier to understand system for video memory settings (under options, video, textures).

The options are now: 256 MB, 512 MB, 1024 MB, 2048 MB and Auto. Auto is useful mostly for users with different size of VRAM but should work very well on any configuration as an alternative to default presets.

Problem with integrated GPU
--
The game does not work too well with an integrated controller which has static VRAM for frame buffer only.  I can test the game on Intel HD4000 and the game detects:
localVRAM=268435456;
nonlocalVRAM=1711276032;

Dxdiag detects:
     Display Memory: 1696 MB
   Dedicated Memory: 64 MB
      Shared Memory: 1632 MB

[maruk 9.9.2013 15:13:13]:
 First step could be to allow users to manually change the values (cfg atm only has stored values of the engine autodetection).

[bebul 10.9.2013 12:11:49]:
 I have currently the /Video Options/ display under developement in the svn branch. The goal is to make the *presets* working through /Overall settings/.
 Only mentioning it as there could be conflicts if Video Options are about to be changed in the DayZ trunk...
