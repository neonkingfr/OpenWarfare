﻿1) Should we use the server lite to test DZ in MP?
2) Can the lite data also be used by clients to have faster game loading and quicker updating?
[maruk 7.12.2012 8:36:13]:
 Server lite is the only server for Dayz, already distributed via steam.
 
 Lite data in Dayz are not usable for clients in the current form. I think we can create a medium data set for Dayz and allow it to be used optionally perhaps?

[qeberle 07.12.2012 08:49:15]:
 To clarify: Sorry I meant for developers - not players.

 For players the video options should hopefully allow to achieve the same/similar effect in terms of game performance.

[ondra 7.12.2012 8:55:50]:
 I think the data are usable technically (the game shouldl run using them), but most likely are too coarse / ugly to be acceptable. You should be able to try if interested, it should do no harm. Wheen I have tried it, the most obvious issue were fonts: at least those would need to be provided in a higher quality, otherwise the texts are unreadable.

 The location is now https://of.bistudio.com/svn/dist/DayZServer/Addons

[qeberle 07.12.2012 11:29:27]:
 To load the normal uiFonts.pbo along should be simple enough.

[Attachment1]: https://projects.bistudio.com/dayz/LiteDataTest.jpg