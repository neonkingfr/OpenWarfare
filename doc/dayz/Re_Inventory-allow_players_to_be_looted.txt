﻿Currently you can only get gear off a player that is dead.

It would be good to have some kind of lootable flag that we can turn on/off for a player, such as when the player is incapacitated (unconscious, tied up, etc...).

Suggested implementation is a script command:
(
<entity> enableLooting <bool>
)

Perhaps a secondary command for checking state:
(
<bool> = <entity> isLootable
)

Thoughts?

[jirka 24.10.2013 13:39:35]:
 There will be easy to extend looting to `Unconscious` life state (controlled by the scripting command `setUnconscious`).
 
 Only simple change of `Man::GetActions` will be needed probably.

[dhall 24/10/2013 1:41:48 p.m.]:
 I will use `setUnconscious` to change the players lifestate.
 
 What we also need to do, is prevent the player from accessing their /own/ gear menu when their lifestate is anything other than alive (so not unconscious, or dead).

[jirka 25.10.2013 10:27:23]:
 Unconscious characters can be looted in rev. 111585.

[dhall 25/10/2013 1:55:56 p.m.]:
 `lifeState` does not appear to be syncronized currently, so unable to loot these characters in MP when `setUnconscious` is used.
 
 Repro mission:
 
 [unconsciousTest.SampleMap.zip][Attachment1] 
 


[dhall 25/10/2013 2:06:05 p.m.]:
 Confirmed it with a repro mission even for AI characters. The server reports the agent is unconscious, the client reports the agent as alive.


[dhall 26/10/2013 12:10:18 p.m.]:
 I have also added `_captive` to the state condition allowing a player to be looted. Tested with `setCaptive` true/false. This also allows players who are restrained to be looted. Hopefully this will give players options to rob, rather than outright kill, another player.


[jirka 31.10.2013 12:24:09]:
 Ondra added both _lifeState and _captive to the network updates in rev. 111611. Can you check if the issue has gone, please?


[dhall 31/10/2013 12:39:28 p.m.]:
 Issue is confirmed fixed, thanks!

[Attachment1]: https://projects.bistudio.com/dayz/unconsciousTest.SampleMap.zip