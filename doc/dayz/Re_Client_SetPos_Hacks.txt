﻿>  Server needs to verify the client movements are within reasonable limits, and to reject unreasonable ones.

I will implement this as a proof of concept, so that other programmers can add more verification if/as needed.