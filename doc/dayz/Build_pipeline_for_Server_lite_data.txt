﻿===Summary
===
Jenkins build task *DayZ Data* is now configured to build LITE server data too. The lite addons are committed into svn repository at https://of.bistudio.com/svn/dist/DayZ/Addons-lite.
It does not contain all addons yet, but each new build would add lite addon there.

===Technical info
===
The [buildData.xml](https://dayz-svn.bistudio.com/svn/trunk/build/compileData.xml) was configured by converting the [ShrinkPBO.bat](https://dayz-svn.bistudio.com/svn/trunk/Tools/ShrinkPbo/ShrinkPBO.bat), which was changed to match DayZ Server climate before.

The lite addon convertion sections of /Ant/ script ( buildData.xml ) work as follows:

 1. the destination unpacked addon folder prepared by data building ( buildData.xml ) actions i used as an input for lite conversion section
 2. all files, except for ( *.paa,*.wss,*.ogg,*.sqm,*.rtm ) are copied (to 
 3. textures ( *.paa ) are shrinked to 8x8 bits only (the poorest possible quality), configured by Jenkins properties: 
   (
pal2pacE.tool=E:\\Tools\\TexView 2\\Pal2PacE.exe
tex.quality=8
   )
 4. animations ( *.rtm ) are shrinked to contain initial and last phases only (the poorest possible quality). ( ShrinkBinRTM.exe ) has changed (build done on Bebuls notebook, source code commited to [!old Kuba svn branch!](https://dev.bistudio.com/svn/pgm/branches/Kuba/ShrinkBinRTM)) and is defined by Jenkins property: 
   (
shrinkBinRtm.tool=E:\\Tools\\ShrinkPbo\\ShrinkBinRTM.exe
   )
 5. sound ( *.wss ) files are converted to half precision by option ( -d4 ) and resampled to 1kHz by new option ( -r1000 -q4 -convert ). The ( waveToSSS.exe ) has changed, was built on bebuls notebook and commited to [!non DayZ svn trunk!](https://dev.bistudio.com/svn/pgm/trunk/Snd). Tool ( wssDec.exe ) is no longer needed. The command line is:
   (
waveToSSS.exe -d4 -r1000 -q4 -convert dst/filename.wss dstLite/ilename.wss
   )
 6. sound ( *.ogg ) files are resampled to 1kHz too, by ugly old ( ShrinkPbo.bat ) like pipeline:
   - decode to ( wav ) file
   - encoding to destination ( ogg ) file 
   - removing the temporary ( wav ) file
 7. the ( *.sqm ) files are not copied and are deliberately missing in lite addons
 8. the /canonical hash/ is computed from full addon content through ( bankRev.exe -hash fileName ) and stored in lite addon /hash/ property
 9. statistics about file lengths inside ( pbo ) file grouped by file extension is provided in Jenkins /Console Output/.

The /Ant/ script ( buildData.xml ) uses new contribution [Flaka](http://workbench.haefelinger.it/flaka/about). It is mainly used to extract file extension (such as ( *.rtm, *.paa )) using regular expression. It was plugged into the Jenkins, as documented in [Jenkins setup and configuration](https://projects.bistudio.com/dayz/Jenkins_setup_and_configuration.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1889).

Once all addons are shrinked, the server needs to be tested!

I have tested it as a game first (not server) and here is a screenshot :)

![LiteDataTest.jpg][Attachment1]  lite addon)

[bebul 30.10.2012 11:59:02]:
 Ant script knowhow: I has fooled myself uncountedly while working with Ant /properties/ as with *variables*. Beware! The Ant properties are unmutable by default. But thanks to additional  plugins, we reset its value by construction like:
 (
   <var name="relative.filename" unset="true"/>
   <var name="dst.filename" unset="true"/>
   <property name="relative.filename" value="@{filename}" relative="true" basedir="${dst.dir}"/>
   <property name="dst.filename" value="${dst.dir.lite}\${relative.filename}"/>
 )
 Note, that also ( <fl:let>varName := value; </fl:let> ) needs previous ( unset )!

[bebul 30.10.2012 12:10:49]:
 We have problem with some animations, such as (  DZ/weapons/arma2 ). Pettka has tried to solve it but I am not sure, whether the success was ultimate or partial only?
  * the problem was: ( *.rtm ) files were not binarized, (files start with magic ( RTM_0101 ))
  * current state: these ( *.rtm ) files are binarized, but contain only initial and last animation phase.
     * note: it is OK for server purposes, but what about full addons?
 Example: ( weapons_arma2.pbo\Data\Anim\AGS_Gunner.rtm )

[bebul 30.10.2012 12:21:45]:
 Shrink efficiency test:  ( DZ/sounds/arma2 ) 
  * original: 386.104.086 Bytes
  * lite addon using original ( ShrinkPbo.bat )  method 192.641.310 Bytes, (ie. exact 50% of original)
    * it took1 hr 8 min !!!
  * lite addon using new pipeline ( buildData.xml ) method 4.912.678 Bytes (ie. cca 1.3% of original)
    * it took 24 min

[bebul 30.10.2012 16:43:14]:
 *Jenkins caveat*: pro testování Ant skriptů a celého buildování LITE addonů jsem používal *Jenkins/Testing* task, ze kterého ovšem někdo zcela vyřadil volání a konfiguraci toho Ant skriptu. Protože konfiguraci těchto Jenkins tasků nemáme v svn, zřejmě nemám jak se k původní podobě vrátit.

[jirka 31.10.2012 7:08:01]:
 Jenkins nastavení se pravidelně zálohuje, pokud budeš mít zájem tak nastavení jobu dohledáme.
 
 Jinak dát tyhle nastavení a potřebné tooly do SVN určitě chceme, jen na to zatím nebyl čas.




[bebul 1.8.2013 16:02:05]:
 There was a caveat in the LITE pipeline, see [Magazine unable to be attached to weapon in multiplayer](https://projects.bistudio.com/dayz/Magazine_unable_to_be_attached_to_weapon_in_multiplayer.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2714)
 Now, Every time, any DayZ addon is built, the LITE addons directory is searched for obsolete files. Each such is immediately destroyed through ( svn del --force ) 
 (
  pushd E:\DayZServer\Addons
  for /r . %%f in (*.*) do if not exist E:\DayZ\Addons\%%~nxf (
    svn del --force --non-interactive --username jenkins --password j(9KT;C]Eq~I %%f
  ) //rem
  popd
 )

[Attachment1]: https://projects.bistudio.com/dayz/LiteDataTest.jpg