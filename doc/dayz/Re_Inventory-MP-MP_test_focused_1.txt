﻿We did this now - unfortunately did not turn out too well.

1. Taking stuff from someone else backpack doesnt seem to get synced
2. Drop stuff to the ground from backpack causes it to disappear (at times)
3. When taking a weapon from the ground, at times it doesnt end up in one of the containers. Aka just disappears.
4. We got quite some desync at times (red chain)

Now it would be helpful to get your suggestion how to proceed.
I assume you want precise repros for each if possible?

Attaching logs from client and server in case they help.


 [DayZServer.RPT][Attachment1]
  [net.log][Attachment2] 


 [DayZInt.RPT][Attachment3] 
 [debug.log][Attachment4] 



[jirka 27.2.2013 11:13:43]:
 Seems that this session was broken at all. We already noticed such problems today, it is somehow related to changes in handling of server / client states (roleless missions changes).
 
 Important message types in the log:
 (
 NetworkClient::OnClientStateChanged state MISSION ASKED, expected ROLE ASSIGNED
 )
 (
 *** Player identity cannot be applied on person 2:2768 ((null)) - player 243044968 not found
 )
 (
 Client: Object 2:121 (type (null)) not found.
 )


[Attachment1]: https://projects.bistudio.com/dayz/DayZServer_2.RPT
[Attachment2]: https://projects.bistudio.com/dayz/net_3.log
[Attachment3]: https://projects.bistudio.com/dayz/DayZInt_2.RPT
[Attachment4]: https://projects.bistudio.com/dayz/debug_3.log