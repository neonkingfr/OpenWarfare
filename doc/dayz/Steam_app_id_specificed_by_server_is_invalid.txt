﻿![DZ\_SteamAppIdInvalid.jpg][Attachment2] 

Repro:
1) Launch steam
2) Launch local DS
3) RMB on steam icon bottom right
4) Open servers view
5) Open LAN TAB
6) Use connect button or double click on local server
7) Notice the said error msg

[bebul 16.1.2013 11:51:03]:
 I am unable to reproduce this...
 no /Unable to connect to server, .../ error msg shown.
 I have tried it on UK server...

[qeberle 19.01.2013 05:38:00]:
 I no longer get the said error either.
 
 *But* instead of connecting the client, it opens another dedicated server for me (wrong app id?).
 
 The two problems could be connected as I only now have the DZ server also in steam.
 
 Can you please try again Bebul.

[jirka 19.1.2013 8:31:00]:
 Steam probably tries to launch the same application which is hosting the game - DayZ Server. I'm not sure how to force launching other one, we probably need to consult it with Valve people.
 Also, we do not support the command line for joining from Steam yet, however this is easy to do (react to +connect command line argument).

[qeberle 11.02.2013 18:45:35]:
 Here is progress:
 1) The game gets launched correctly.
 2) There is a screen for a very short moment that shows steam uses as paramter.
 3) You end up in the main menu. Probably faulty parameters.
 
 Note: I cannot find the UK DS via steam servers tab.
 Probably DayZ has to be added to games or is not yet available as not yet public.

[Attachment2]: https://projects.bistudio.com/dayz/DZ_SteamAppIdInvalid.jpg
[Attachment3]: https://projects.bistudio.com/dayz/DZ_SteamConnectLAN.png
[bebul 12.2.2013 9:16:05]:
 > we do not support the command line for joining from Steam yet, however this is easy to do (react to +connect command line argument).
 
 ^^

[qeberle 12.02.2013 09:43:19]:
 Ok sorry.. was a while since I read that.. was too happy to see joining work. :)
