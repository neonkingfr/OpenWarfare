﻿===Build
0.12.102642
Singleplayer

===Problem
When AI agent is following the playing (using zombie brain FSM), they tend to create a path made up of zig-zags *when following the player*, as seen here:


 ![zombie\_ai\_zigzag.jpg][Attachment1] 


Note:
* Does not occur plotting to a stationary point, only occurs when the player is moving and the moveTo command is called again and again.
* Occurs for both when zombie is running or forced to walk only, so long as the above occurs.

===Repro
* Load mission: [testZombiePathZigZag.ChernarusPlus.zip][Attachment2] 
* Enable Path Overlay
* Observe zombie running towards you without zig-zag path
* Wait for zombie to get near you
* Run away and look behind. Keep running backwards and note how the path placement alternates left and right of each other resulting in occasionaly left and right occilation of zombie movement.

You can use *forceZombieSpeed* set to true to make the zombies walk.

[ondra 28.2.2013 17:00:50]:
 Snadno by to mohl být důsledek znovupoužívání již naplánované cesty. Pokud je ten, kdo následuje, blízko a hledá cestu často, může vždy třeba přilepit jen malý kousek. Pro podobné účely by možná bylo lepši vytvořit nějaké speciální "stíhání", podobné pohybu ve formaci u A2 podřízených.
 
 Jiná možnost by mohla být nějak znovupoužívání omezit, třeba aby se nepoužívala znovu cesta do určité vzdálenosti cíle hledání.
 


[jirka 8.3.2013 13:51:59]:
 Some improvement should be in revision 102903. Can you please test it?
 
 When command moveTo is used, first we check if simple straight path can be used. If so, the path is created immediatelly. In my testing, following the player in open terrain looks quite fine now.
 
 Possible improvements:
 - if simple path can be used, it can be updated more often (but we need to keep delay when regular path is planned, so maybe we will need to split moveTo command)
   Example:
   (
 if (moveToSimple _pos) then
 {
   // simple path found, we can re-plan soon
   delay = 0.5;
 }
 else
 {
   moveTo _pos;
   // wait longer to let the agent create and use the path
   delay = 2.0;
 }
   )
 - I'm not sure if simple path can be found when player is not moving (locking problems), we can handle this situation somehow if it will appear to be a problem
 - even in situations when no simple path exists, but simple path exists to some point towards player far enough, we can prefer it over full path searching (and delay pathfinding to the moment when it is neccessary)


[dhall 8/03/2013 2:25:53 p.m.]:
 I have tested this and the results are outstanding! The problem is no longer reproducable at all and the zombies are now a real threat because they can easily follow you.


[jirka 8.3.2013 16:05:18]:
 I observed a strange AI behaviour sometimes. They have their plan ready, but are moving very slowly or not at all.
 
 In log, messages appeared:
 none(
  Too slow speed in planned path (0.108592<0.5)
 )
 
 This occurs when (mostly simple) path is planned through fields with GeographyInfo.full set.
 
 I have commited some hotfix in revision 102914, but better fix will be needed:
 - maybe to improve a cost calculation for a simple path
 - check the Man simulation and configs - the minimal speed 0.5 m/s seems to be too low to keep character moving


[jirka 11.3.2013 12:42:27]:
 I changed the way how the cost (speed) is calculated for a simple path (cost calculated inside IsSimplePath is used directly). Now it is more consistent to the cost used in regular path (revision 102946).
 
 I also no more noticed the situation where the unit freezes. This was maybe caused by some bug in animations or configs.


[dhall 11/03/2013 1:00:36 p.m.]:
 Just tested it, it is working really well! Should this bug now be closed as fixed? I feel it is now completely resolved.


[jirka 11.3.2013 13:06:03]:
 If you think improvements offered in (8.3.2013 13:51:59) are not needed, it can be closed (or at least unassigned).


[Attachment1]: https://projects.bistudio.com/dayz/zombie_ai_zigzag.jpg
[Attachment2]: https://projects.bistudio.com/dayz/testZombiePathZigZag.ChernarusPlus.zip