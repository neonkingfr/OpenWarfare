﻿Enclosed sample mission for bleeding development:
 [bleedingSample.samplemap.zip][Attachment1] 

Run mission, player created with weapon.
* AI created who will run between two points
* AI will wait for three seconds at each point
* Player has 4 magazines to fire at AI
* When AI is hit, will bleed but can never die (damage sets to zero)


[ondra 11/07/2013 14:19:40]:
 Sample mission updated, some trivial script cleanup done, paricle source creation simplified and moved to `HitPart` event handler, making the sample not working in MP, but closer to intended future usage.

 (
(

//bleeding function
make_bleed = {
	private["_unit","_source"];
	_unit = _this select 0;
	_source = "#particlesource" createVehicle getPosATL _unit;
	_source setParticleParams
	/*Sprite*/		[["\dz\data\data\ParticleEffects\Universal\Universal", 16, 13, 1],"",// File,Ntieth,Index,Count,Loop(Bool)
	/*Type*/			"Billboard",
	/*TimmerPer*/		1,
	/*Lifetime*/		0.2,
	/*Position*/		[0,0,0],
	/*MoveVelocity*/	[0,0,0.5],
	/*Simulation*/		1,0.32,0.1,0.05,//rotationVel,weight,volume,rubbing
	/*Scale*/			[0.05,0.25],
	/*Color*/			[[0.2,0.01,0.01,1],[0.2,0.01,0.01,0]],
	/*AnimSpeed*/		[0.1],
	/*randDirPeriod*/	0,
	/*randDirIntesity*/	0,
	/*onTimerScript*/	"",
	/*DestroyScript*/	"",
	/*Follow*/			[_unit, _this select 1]];
	_source setParticleRandom [2, [0, 0, 0], [0.0, 0.0, 0.0], 0, 0.5, [0, 0, 0, 0.1], 0, 0, 10];
	_source setDropInterval 0.02;
};

//equip player
_item = player createInInventory "BaseballCap_Blue";
_item = player createInInventory "Jeans_Blue";
_item = player createInInventory "Bag_BAF";
_m4 = player createInInventory "M4A1";
_item = _m4 createInInventory "Attachment_Buttstock_M4OE";
_item = _m4 createInInventory "Attachment_Optic_M4CarryHandle";
_item = _m4 createInInventory "M_STANAG_30Rnd";
_item = player createInInventory "M_STANAG_30Rnd";
_item = player createInInventory "M_STANAG_30Rnd";
_item = player createInInventory "M_STANAG_30Rnd";
_item = player createInInventory "M_STANAG_30Rnd";

_damageEH = man1 addeventhandler ["HandleDamage",{if ((_this select 1)=="") then {[_this select 0, _this select 5] call make_bleed};0} ];

_pos1 = [2541.88,2520.77,0.00143909];
_pos2 = [2506.39,2524.31,0.00143719];

while {alive man1} do
{
	//Make the AI run around so can see movement of damage
	man1 moveTo _pos1;
	waitUntil{man1 distance _pos1 < 2};
	sleep 3;
	man1 moveTo _pos2;
	waitUntil{man1 distance _pos2 < 2};
	sleep 3;
};


)

)




[Attachment1]: https://projects.bistudio.com/dayz/bleedingSample.samplemap_2.zip