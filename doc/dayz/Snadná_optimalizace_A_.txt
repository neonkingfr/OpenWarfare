﻿Při diskusi s Hlaďasem nás napadla vcelku snadná možnost optimalizace vyhledávání cesty.

A* je dosti neefektivní v případech, kdy má cílové políčko (nebo jeho okolí) vysokou cenu. Graf se pak rozvíjí hodně do šířky ve snaze najít nějakou efektivní objížďku.

Často se to řeší tak, že se cesta vyhledává z obou konců zároveň a skončí se, když se obě vyhledávání protnou.

O dost implementačně jednodušší by mohlo být prostě porovnat cenu startovního a cílového políčka a v případě, kdy cílové má cenu vyšší vyhledávání obrátit (vyhledávat cestu z cíle do startu).