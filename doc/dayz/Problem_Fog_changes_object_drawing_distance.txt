﻿Sample pictures:
http://imgur.com/a/7UV8j#0

Note the 2000/2000 at fog 0.
Note it changes to 2000/1047 with setFog applied.


*Obs*
setFog reduces the object drawing distance value

*Exp*
setFog should not change it
Or
Add scripting command ( u:getObjectViewDistance ANY ) to be able to remember the value
before setFog gets applied and reset it back after fog gets removed.