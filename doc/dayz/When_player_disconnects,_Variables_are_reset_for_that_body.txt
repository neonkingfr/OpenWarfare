﻿Problem
===
- When a player is killed, and disconnects, the body locality is transferred to the server.
- The body loses all its variables assigned with the "setVariable" command.

Requested
===
- When a player disconnects from a dead body, and locality is transferred, variables assigned be transferred to the dead body.

Explaination
===
- For reproduction, use the DayZ mod and the "study body" command. It does not work as the variables were wiped when the player disconnects.

Impact
===
- Will fix the occasional "not save" issue due to disconnection of a player.
- Will fix the "study body" command in DayZ.
- Probably affects other ArmA2 missions/mods.

[jirka 26.6.2012 12:51:00]:
 > - When a player is killed, and disconnects, the body locality is transferred 
 > to the server.
 > - The body loses all its variables assigned with the "setVariable" command.
 
 The disconnection of client is not needed, even the death is enough.
 
 The problem is that, unlike for other objects, for persons variables are stored in the brain (unit or agent), not in the body (object). When unit is killed, its brain is deleted after a while. (P.S.: in fact it is even more complicated, the situation differs for agents and units)
 
 The solution would be to transfer variables from the brain to the body whenever the brain is destroyed. I will try to implement it.
 
 

[jirka 26.6.2012 12:51:23]:
 > The solution would be to transfer variables from the brain to the body 
 > whenever the brain is destroyed. I will try to implement it.
 
 Should be done (revision 94033).
 
 Tested only for units, try it for agents as well, please.
 
