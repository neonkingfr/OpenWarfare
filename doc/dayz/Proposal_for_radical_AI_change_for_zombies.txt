﻿===Summary

Currently the AI used for agents is well suited to an entity that is required to negotiate between two positions. However, the "zombies" of DayZ do not require this, they require to move from their position /towards a (usually moving) target/.

Additionally, AI stops and turns on the spot to change direction. This results in very stop-start movement. The current solution is still built around arma's core "a to b" mechanic, which is probably a great deal more complex than we actually need. Our AI needs only be stupid, and chase it's target.

My proposal is that a very simplified AI system be created that simply points the zombie towards it's target, and conducts collision avoidance within a short space, such as 3-5 meters.

===Explanation

Currently the zombies walk around (loiter state) until they have a target. When they have a target, each second they receive an updated position, issued with the *moveTo* scripting function. This results in complicated pathfinding between the zombie and it's target, which is often then scrapped when the player moves a reasonable distance.

This raises several problems:
* **Expensive**; not only is choosing the target part of a complex FSM but also the pathfinding (and monitoring of the success or not). This pathfind is repeated every second, and often changes because the position of the target (a player) has substantially changed. This can often results in zombies running backwards and forwards as they replan their route around obstacles they probably will never actually reach while chasing a player.
* **Ineffective**; The zombies aren't able to effectively chase the players because they are constantly stopping to turn and change direction, and choosing complex routes that appear to the best based on current situation, but don't react fast enough to drastic changes in situation.
* **Same Paths**; the zombies will often end up choosing the same paths, resulting in the zombies all following each other heading to the player. They then slow down to walk, as they are all trying to walk on the /exact/ same line - some of them even end up dying as they get "crushed" in a mob of very slow moving zombies trying to occupy the same "rail" as the player runs away.

===Experimentation

Previously I experimented by constantly playing a running animation, and at the end of each running anim using *setDir* to alter the orientation of the zombie towards the player. This gave the most compelling zombie chasing mechanic that I had been able to reproduce, but as a scripted solution I was not able to develop any collision avoidance.

===Proposal

I would propose that a new, greatly simplified, zombie AI be developed that performs only the most basic of functionality. Either the zombies are entirely engine driven (based perhaps on the current zombie FSM as a prototype), or a scripting command is created to activate a "chase + collision avoidance" brain when given a target.

The zombies will:
* Be given a target /position/ or /entity/.
* will run directly towards their target
* will instantly update their direction to face their target
* when faced with an /immediate/ obstacle, will move into a collision avoidance routine. could be as simple as turning left or right (randomly) and then running until either they see their target or face another obstacle.
* when close, attack sequences will occur in line with the zombie FSM currently in use (long and short distance attacks, eating behavior).
* to help avoid getting stuck, zombies will run towards the /last place they could actually see the player/. When they get their, if they cant see the player any more - they will search.
* They may be given positions to move to based on sounds, such as gunshots. These are treated similar but will be static.

Zombies getting trapped:
* obviously, an extremely simple collision avoidance AI will result in zombies getting stuck in loops sometimes trying to get out. This is accepted and somewhat desired. The zombies are supposed to be humans who have suffered significant brain damage, incapable of reasoning beyond basic movement... i.e. chasing. Players trapping zombies in buildings due to confusion would be advantagous.

Zombies in buildings:
* This could probably be tied with interiors, specifically if a raycast was used to check for collision. Zombies would simply run around until they found an opening, then go through it, and keep searching. All the time, checking to see if they can still  see their target. While they will (as above) get stuck, this is fine.

I think a good initial implementation should be done with a *green sphere* chasing players (no unit or animations, to see how it moves through the world and how it chases players. Once that was perfected we could look at applying that behavior to the zombies and/or faking the animation into it. For zombies, we really don't need perfect animations - but we need the chasing to be extremely compelling.

The zombies don't require complex animations, transitions, or anything like that - so applying some basic state animations on top of what is effectively a "green sphere" would achieve the results from a design perspective. Clients would simply receive "is moving fast... play running anim / is moving slow... play walking anim / is stopped.. play loiter" and update position + direction.

Appreciate discussion and thoughts.

[dhall 19/08/2013 8:11:01 p.m.]:
 For a visual, I did this for a tutorial once in Unity:
 
 http://www.youtube.com/watch?v=vE7PUTv-AB0



[jirka 27.8.2013 8:17:57]:
 I'm afraid with such a simple steering algorithm there will be easy for player (whenever he found how it works) to trap zombies on some places (like fenced gardens, linked houses etc). The algorithm probably will not work well with concave obstacles.
 
 But maybe we can develop some hybrid approach:
 - steering mode when target is quite near and the area is not too complex (in sense of high density of objects)
 - planning mode otherwise or when we detect some fail of the first method
 
 There would be nice to enable switch on / off the simplified AI using scripts to test how it behave in the real situations. I think it will not be very difficult (some basics of steering and obstacle avoidance is already present in the person's simulation), but I would prefer if somebody else will implemented it.



[dhall 27/08/2013 11:48:18 a.m.]:
 Understand the issues with getting the AI trapped. I actually think this could be quite an interesting game mechanic, but as you state we would need to test it.


[dhall 27/08/2013 3:24:51 p.m.]:
 Sample Map:
 [zombie.samplemap.zip][Attachment3] 
 


[dhall 28/08/2013 1:27:29 p.m.]:
 Updated with obs: [zombie.samplemap.zip][Attachment4]  


[Attachment1]: https://projects.bistudio.com/dayz/zombie_close.jpg
[Attachment2]: https://projects.bistudio.com/dayz/zombie_direction.jpg
[Attachment3]: https://projects.bistudio.com/dayz/zombie.samplemap.zip
[Attachment4]: https://projects.bistudio.com/dayz/zombie.samplemap_2.zip