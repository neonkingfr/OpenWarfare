﻿===Build
0.17.108399

===Problem
Attachments on a weapon are correctly saved to the database, but they are not loaded when character is created on a *multiplayer dedicated server*.

===Reproduction
* 100% repro
* Multiplayer DS only
* Both full & lite data checked

1. Install mission:   [testReproLoad.SampleMap.zip][Attachment1]  
2. Execute in *singleplayer* and execute following code to add weapons
 (
			_item = player createInInventory "BaseballCap_Blue";
			_item = player createInInventory "Jeans_Blue";
			_item = player createInInventory "TacticalShirtOlive";
			_gun = player createInInventory "M4A1";
			_item = _gun createInInventory "Attachment_Bayonet_M9A1";
			_item = _gun createInInventory "Attachment_Buttstock_M4OE";
			_item = _gun createInInventory "Attachment_Handguard_M4Plastic";
			_item = _gun createInInventory "Attachment_Optic_M4CarryHandle";
			_item = _gun createInInventory "M_STANAG_30Rnd";
 )
3. Wait five seconds for save to server
4. Close Singleplayer mission
5. Execute mission on *multiplayer dedicated server*
6. Note that previous loadout is loaded from database, except for attachments which are not loaded


[dhall 2/08/2013 12:34:46 p.m.]:
 Confirmed bug with server running *full data* also




[bebul 2.8.2013 16:56:48]:
 Looking at it. 
 My first idea: the ( InventoryItem::_attachments ) does not look to be created as network objects through any ( CreateSubobjects() ). 
 I must first reproduce the issue. And try to fix it then ...


[bebul 2.8.2013 17:54:10]:
 *FIXED: rev.108529*
  * I have tested it the following way:
  1. play MP mission on DS ... there was bayonet attached yet
  2. detach bayonet, wait 5s, logout, play again and see bayonet is detached
  3. atach bayonet again, wait 5s, logout, play again and see bayonet attached
  
 Technically: (I would prefer *Jirka* to check my fix)
  1. added call to ( base::CreateSubobjects() ) to ( Weapon::CreateSubobjects() )
  2. added ( InventoryItem::CreateSubobjects() )
  (
   void InventoryItem::CreateSubobjects()
   {
     for (int i=0; i<_attachments.Size(); i++)
     {
       InventoryItem *item = _attachments[i];
       if (item) GetNetworkManager().CreateVehicle(item, VLTOut, "", -1);
     }
     base::CreateSubobjects(); //<-- because this one was already called for InventoryItems
   }
  )
 


[dhall 2/08/2013 8:56:35 p.m.]:
 Confirmed fixed with 10 users on server

[Attachment1]: https://projects.bistudio.com/dayz/testReproLoad.SampleMap.zip