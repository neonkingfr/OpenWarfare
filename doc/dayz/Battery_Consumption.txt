﻿===Request
Battery consumption begin to be modelled on items, probably the most natural place to start is *ItemTransmitter*

===Configuration
The transmitter has been configured to allow a *MiscItem_BatteryD* to be attached to it, which has the following configuration following the current resource system used for eating, drinking, etc... that utilizes a common resource class under items:
(
	class MiscItem_BatteryD: BatteryBase
	{
		scope = public;
		displayName = "Battery (Alkaline)";
		model = "\DZ\Items\Misc\Loot_BatteryD.p3d";
		inventorySlot = BatteryD;
		class Resources
		{
			/*
				Use http://www.allaboutbatteries.com/Energy-tables.html for power
				values in the batteries
			*/
			power = 74970;	//in joules
		};
	};

)
Note I have recorded the value in joules (J) which I think is probably the most sensible unit for us to use.

Thoughts?

[bebul 26.6.2013 17:03:40]:
 One thought: the bateries are somewhat like a vehicles, so their POWER can be considered as FUEL.
 Then the following would work like charm:
  1. c( transmitter setFuel 1.0; //1.0 is the max capacity of the batery type )
  2. c( fuelCapacity = 74970; //in joules )  instead of c( power = 74970; )
 Then eveything what remains is to implement existing interace and add energy consumption on the bateries powered devices.
 

[dhall 26/06/2013 10:58:06 p.m.]:
 Sounds like a good idea! Only issue, will be if we use different types of fuels... would it be possible to add some kind of *fuelType* that defines what fuel it is, such as "electric" or "butane" etc... so that only a resource item of the required type could be used?
 
 Might not matter a great deal now, but could be an issue in future. Thoughts?

[bebul 27.6.2013 17:16:59]:
 done in rev.107056 using different implementation:
  * it supposes each ( class Resources ) item is mapped to the float ( variable ). This variable is taken and when device is ON, it is consumed through time. 
  * The speed of energy consumption is configured through entry ( energyResources[] )
 (
  class ItemTransmitter: PoweredItem
  {
    attachments[] = {"BatteryD", "PrimusCookerCanister"};
    energyResources[] = {{"power", 4.0}, {"butane", 0.01}};  //in {varName, energyInput in Watts, grams/second, etc.}
  };
 )
  * when no energy remains, the Transmiter is switched OFF and ON/OFF action is no longer available
 

[bebul 28.6.2013 14:48:40]:
 Both Radios and Transmitters are power based now. These do not work without batteries.
 (Transmitters are still configured to work also on butane :-P Very funny, would be interesting to see how fast would players find it as "bug")
 rev.107112
