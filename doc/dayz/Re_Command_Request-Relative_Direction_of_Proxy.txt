﻿Proposed merge of required proxy data functionality, the proposed command name is formatted in keeping with the *selectionPosition* command which gives the relative position of selection.

The proposed command gives the data for a proxy back in an array. An empty array is returned if the selection does not exist, or is not a proxy. The proxy object should be checked for in the memory LOD of the model.

(
	<array> = <object> selectionProxy <string>;
)

The returned *array* consists of:

* *relative position* [x,y,z]
* *Vector Dir and Up* [[x, z, y],[x, z, y]] (in line with the /setVectorDirAndUp/ command)
* *model name* ideally with ".p3d" removed, so the name can be looked up in a config reference

This data will be then be used to spawn the required object, and it's relative position set using the setVectorDirAndUp and setPos (via modelToWorld) commands.

This will then allow me to prototype the spawning mechanisms to find the right process

[dhall 29/10/2012 1:16:28 p.m.]:
 Discussed with Ondrej, resolution LOD not available to simulation. Proxy could be stored in memory LOD with no problem.

[ondra 31.10.2012 11:36:27]:
 There is an implementation complication with this approach. While returning a proxy for given selection may appear trivial, in fact it is not, and the corresponding code is far from being straightforward. Moreover, I need to touch data which are usually not needed (selection face indicies) and I am not 100 % sure I always have them ready.
 
 An approach more in line with current proxy handlng would be to give proxies a "type" (using a named object) which would indicate what kind of loot can spawn in the proxy, let such types handle the spawning (scripted equivalent would be to enumerate all proxies and handle those with known types).
 
 That said, the command is implemented in 98586 and you should be able to start testing it. I did not test it, as I am not aware of any data I could test it against. As the selection matching code is quite complex, I am unsure if it really works. Do not be surprised if it does not, just report it together with a repro.
 
 Limitations I am aware of:
 - when selection contains more proxies, one is returned only
 - when the proxy is animated, default position is returned
 
 I am not sure of:
 - the positions of objects sometimes need to be corrected for the "object centering". This might be visibile as an apparent "offset" of the returned position. If you see this, report it and we will find a solution.

[dhall 31/10/2012 2:45:57 p.m.]:
 Have done an initial test and it appears to be working, at least enough for me to continue testing. I am preparing a test mission for debug purposes and will report back progress.

[ondra 31.10.2012 15:31:45]:
 pos / dir / up changed to world space in 98601.

[dhall 6/11/2012 1:54:33 p.m.]:
 Noticed a couple of problems with using the command.
 
 For my testing building the array returns:
 (
  ["\dz\weapons\proxies\spawnweaponproxy",1,[6385.1,2295.73,1.16492e-017],[0.738444,-0.674315,6385.1],[-5.24696e-015,4.7913e-015,0.738444]]
 )
 
 The fourth item in the array appears to be the vector Dir, but it has the x coordinate in the final value. For example, a comparison of the building VectorDir and the proxy array item:

 Building (expected return):
 (
[0.738444,-0.674315,0]
 )
 Proxy (returned result):
 (
[0.738444,-0.674315,6385.1]
 )
 Worldpos (note x coord):
 (
[6385.1,2295.73,1.27321e-017] 
 )

 Also, as noted, the offset from the proxy object does exist but I can correct this through the proxy object placement for testing and experimentation purposes

[ondra 6.11.2012 14:38:42]:
 > The fourth item in the array appears to be the vector Dir
 
 The third is direction, the fourth is up, The order of components is (as with most scripting functions), X Z Y (X Z being horizontal, Y being up).
 However, the values look like somehow garbled or messed-up. Can you provide a repro?


[ondra 6.11.2012 14:49:42]:
 Obvious:
 
   array[0] = x[0];
   array[1] = x[2];
   array[2] = x[3];
 
 Should be:
 
   array[0] = x[0];
   array[1] = x[2];
   array[2] = x[1];
 
 Fixed in 98759
 

[ondra 29/11/2012 12:24:51]:
 The command should now always return an array of all proxies in the selection.
 
 Note: even for 1 proxy the result will be array of one array, therefore new version is not compatible with current scripts.
 
 (99535)

[dhall 29/11/2012 5:21:03 p.m.]:
 Thank you so much :) Will let you know if any problems.
