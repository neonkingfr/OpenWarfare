﻿Sorry to bother you with this, but we are stuck here.

*Obs*
I hosted on my computer, have 2302/arma ports forwarded. Others cannot find my dedicated DZ server (arma works).
Now we have Johny (from Prague) set up a dedicated server, but Johny, Vrata nor me can find it.
However Radek from Johny's team could. Radek connected from off-site via internet.
Johny also disabled the FW to test - without success

LAN server works fine here though.


*Question*
* Does DZ require anything specific/special due to steam(works) or anything else?
* What else to check or configure?


*Repro*
1) Update DZ svn to latest or the DZ steam client
2) Start game
3) MP
4) See if you see the server in the list
5) If not try remote: 37.220.18.218

[qeberle 20.12.2012 16:25:48]:
 Update: Johny was able to connect one/twice but for the most time doesn't see it.

[jirka 21.12.2012 11:02:29]:
 The matchmaking works other way in DayZ (we are using Steam matchmaking instead of GameSpy one used in Arma line).
 
 For DayZ, the server need to have a public IP address. If the host is behind some NAT devices (routers etc.) all have to be configured to forward both Arma ports and Steam Master server ports (at least 27015 for both UDP and TCP).

[qeberle 21.12.2012 12:07:31]:
 27017 for the steam client and 27018 for the DZ server.
 
 Still didnt help here. :/ Forwarded also to Johny to try with their dedicated server.
 
 

[jirka 21.12.2012 12:40:13]:
 > 27017 for the steam client and 27018 for the DZ server.
 
 I'm not sure if to open 27018 for the server is enough.
 In the discussions I read that 27016 is required by Valve, but 27015 or 27017 could work as well.
 
 In the steam initialization code, I'm passing ports:
 (
   masterServerUpdaterPort = 27016;
   authenticationPort = 8766;
 )
 
 So try to open these or find some documentation for Steam based games.

[jirka 21.12.2012 12:51:52]:
 Some official source found here:
 https://support.steampowered.com/kb_article.php?ref=8571-GLVN-8711

[qeberle 21.12.2012 13:39:15]:
 Sorry I wasnt clear - the range open was from 27015 to 27030
 17 and 18 is what is/was actually used.
 Opened from 27000 to 27050 + 4380 now - no change.
 
 Johny says their server has a public IP (and no NAT from what I understand).

[jirka 13.1.2013 8:29:07]:
 The problem is on the server, not on the client machine. I think we need to ensure that the host has really a public IP (some IT professional need to check it).

[bebul 16.1.2013 11:12:41]:
 I have currently access to the UK server.
 There are two DayZStandalone folders, one from Steam, one on root /C:\/DayZ_standalone/. Both have different firewall settings, but it seems somebody was only desperately trying to sort it out, experimenting.
 I am able to connect using /Remote/, but I do not see the server in the /Internet/ server list.

[bebul 18.1.2013 12:41:54]:
 Steam Support contacted by mail - one from me and another one promised to send by Dean to include other Valve contact. Waiting on response...
 The latest commit has some diagnostics on server Console window left, so don't be surprised, it is intended...

[bebul 22.1.2013 9:22:37]:
 Steam Support has answered. The List of changes tried to fix the problem follows:
 1. *Pieter*: I increased the server version from 0.0.0.0 to 0.0.0.1 as I don’t think all 0’s is valid.  Can you update your call listed below to use that version and let me know if it is working?
 2. *Pieter*: Can you add these calls:
 (
   SteamGameServer()->SetProduct("dayz");
   SteamGameServer()->SetGameDescription( "DayZ" );
 )
  * we have had SetProduct("DayZ"); \/\/different case sensitivity
 3. *Pieter*: Okay, that looks good.  One more test.  What is the IP of your server?  I can try to look it up in our support tool to see if it is talking to our backend at all.
 4. *Pieter*: Can you remove the gamedir from the filters passed in from the client?
   * I have tested previously to skip the gamedir and secure filters (so use no filter at all). Then over 4000 servers were listed, but our DayZ missing.
 5. *Pieter*: Just to check, what AppID are you running as?
   * Client: 221100, Server: 223350 

 It looks to me, Pieter has no idea atm. what the problem is. Maybe we can ask, whether their "support tool" mentioned in No.3 has shown our server connected and has given some diagnostics...
[bebul 22.1.2013 23:43:21]:
 It happened, Pieter Wycoff has found the problem when answering our different, but related question: /"When CONNECT button on Server LAN list is clicked, the DayZ server is launched, but we want to start client application instead"/
 He answered: "One other question about your dedicated server.  Are you launching it from Steam, or outside of Steam?  We need the dedicated server to run as the AppID of the game, not the server.  This can be done with a Steam_appid.txt file if running outside of steam or with and environment variable if running from steam.
 This could be the cause of this issue and the other where the game isn't finding the server on the internet tab."
 
 And he was right. When both ( steam_appid.txt ) files matches, the server is listed ingame! And we *CAN CONNECT* at last using Internet filter. But the server is still not listed in the Steam Internet Tab.
 

[jirka 23.1.2013 7:27:30]:
 Interesting, I hoped there will be some other solution which will enable us to keep different appid for server and client, but this is possible as well. We will need to make ( steam_appid.txt ) part of server distribution (even on Steam).

[qeberle 11.02.2013 18:34:14]:
 I can see now the UK DS and my local DS via server browser. :)

 But no connect (see pic) - via LAN tab/remote still works.
 ![p001.jpg][Attachment1] 

[bebul 12.2.2013 9:26:51]:
 What does no connect means?
 Picture shows you cannot connect to /DayZ Test Server/, while UK Server is listed below?

[qeberle 12.02.2013 09:41:19]:
 Connecting to the UK DS works
 
 DZ Test is my own DS.
 Here via browser does not work, only via remote or LAN tab.
 
 Using custom startup params for the server makes no difference.
 
 

[bebul 12.2.2013 11:25:59]:
 Can anybody else connect to your server through internet browser? (Maybe proper ports are not forwarded? Is it running on 2302?)

[qeberle 12.02.2013 15:24:47]:
 Correction: 
 * Remote with external IP does not show the server.
 * LAN/remote 127.0.0.1 works fine
 * Server brower shows server, but "connecting failed" after initial loading after join. Both for Vrata and me.
 
 Port is 2302.
 
 Forwarded are:
 (
 DZ Server	TCP	2302-2305	192.168.178.27	2302-2305
 DZ Server TCP	TCP	27000-27050	192.168.178.27	27000-27050
 DZ Server UDP	UDP	27000-27050	192.168.178.27	27000-27050
 steam	UDP	4380	192.168.178.27	4380
 8766 TCP	TCP	8766	192.168.178.27	8766
 8766 UDP	UDP	8766	192.168.178.27	8766
 )
 
 I am behind FW of FRITZ!Box Fon 5140 DSL router.
 

[bebul 12.2.2013 15:34:38]:
 And your local IP adress is 192.168.178.27?
 Why are the 2302-2305 forwarded to 192.168.178.21? Maybe you wanna be 6 years younger? :-P



[qeberle 13.02.2013 10:52:37]:
 Well spotted - unfortunately that's not it. No change. :/

[qeberle 13.02.2013 10:57:01]:
 Ok got it. You need to forward UDP, not TCP (for 2302+).

[Attachment1]: https://projects.bistudio.com/dayz/p001_4.jpg