﻿While the alpha has immediate and full priority, I think this topic needs some discussion, preparation and time to realize - **if you decide to go that route**.

What is meant:
* In short: Offer in cooperation with community developers their work as paid DLC
* Worlds, weapons, vehicles, skins (retexs) (sound replacements may be tricky)

* All users get the lite version for free
* (My approach) Small price, large(r) share to the author(s), content packs (instead of single item sales) - the numbers should make the profit
* Best case: Also free (and paid) content updates by BI

Note: These would be to avoid "BI getting greedy" / "Pay2Win" arguments.
If done well, it should be doable to market it as "support of community developers/modding" as well as "lots more content for DZ".


I think the lite version system is a major opportunity you have here - it kinda worked like this with BAF and PMC content used for the mod version.


Why do it:
* New content - don't underestimate a good share of players bored by Chernarus and the existing setting
* It takes considerable time and resource to produce content like this
* The mod shows many "DayZ clones" made for community terrains - which are very popular
* Similar can be said about weapons and vehicles with private hives using custom weapon sets - although only using CO+DLC content so far (probably as not many standalone weapons packs are out there - most are integrated into full mods)
* The upcoming weapon and character customization of DayZ standalone offers a lot of new possibilities
* Ultimately more profit for BI
* Financial support for community developers
 * While this is a large topic on its own, my advice would be to not underestimate this
 * Many move to VBS, sell their work by other means or ultimately stop modding, as it takes a lot of time and skill to create most content
 * The biggest challenge I see is that people, especially modders might see such system just for DayZ standalone as "unfair" (do stuff just for free in Arma, but get money when done for DayZ)
 * The only solution to this I can see is to go the same route with A3 - to give community developers the ability to earn money by selling the HQ variant via BI
 * This is a very tough decision to make I believe - from discussions with other community developers it is inevitable; Iron Front has given them a dream (although it should have been distributed by BI and made in coop with BI in my view..) - while some will always do modding as a hobby, some can not afford to put many hundreds to thousands of hours into modding without compensation - to have students with high skill and enough free time may no longer work as it did in the past and it already became visible with Arma: You mean not realize that the modding community relies (has to) on very few experienced and skilled people (especially outside modelling). If those drop out, the modding scene will take a considerable hit.
 * The only viable alternative I can see is to focus on reducing the time, skill and knowledge (from experience) needed by better tools, documentation and engine design, which I hope to see regardless to some degree - however this is also a tough investment to make for you
 * DZ could work as a test bed for such approach to see how it works and it is received by both modders and players
* With standalone locked down (for a longer while I presume), this is the only doable approach for rapid and enough new content that I can see

There are probably many more points to consider, like legal topics, ownership state, yet I stop now as its way too long already anyway.


Hopefully you can make some benefit from these thoughts in whatever form.
[maruk 30.11.2012 11:02:30]:
 This topic has not real value for the project. Closing.
 
 Relevant information is here: [Closed environment, little or no modding](https://projects.bistudio.com/dayz/Closed_environment,_little_or_no_modding.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1900)
 
 And if there is some change planned it is going to be announced there.
