﻿===Problem
Item damage is not recorded in the database.

===Request
Overall damage value of an item be syncronized, and applied when inventory is loaded.

[jirka 20.6.2013 10:46:17]:
 Should be done in revision 106700. Test it, please.


[dhall 20/06/2013 11:43:44 a.m.]:
 Confirmed working, thanks heaps!

[Attachment1]: https://projects.bistudio.com/dayz/debug_2.log