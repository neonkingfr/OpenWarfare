﻿There was a bug in DayZ Data build Jenkins pipeline causing /\*.dep/ and other garbage files were present in final packed /\*.pbo/ files deployed.

The problem was trivial, as the filebank.exe was called as:
(
  filebank.exe -exclude=filename.lst ...
)
instead of
(
 filebank.exe -exclude filename.lst ...
)

I only wonder whether /binarize.exe/ understand ( -exclude=xxx ) or ( -exlude xxx ) as the ant script uses ther former one. It needs to be checked yet!

[bebul 30.10.2012 17:42:12]:
 Have tried to rebuild ( DZ/anims ) to see the pbo size has shrinked from 9.3MB to 5.8MB, which is more then expected. (( assets_structures.pbo ) from 221MB to 136MB, so it looks like stable shrink to 61% by addressing only ( *.dep ) bug.)
[bebul 31.10.2012 16:48:31]:
 binarize.exe really uses different format with equal sign for excludes.
 (
     else if ((rest = RestArg(argv[1],"-exclude="))!=NULL)
 )
 The ( *.dep ) files are now swept out.
