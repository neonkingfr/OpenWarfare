﻿Mohlo by být zajímavé modernizovat `toIntFloor` funkci. Vyžadujeme už beztak SSE, takže její implementace by nyní mohla být založená na SSE (viz http://aavdev.blogspot.cz/2009/02/fast-and-easy-float-to-int-conversion.html)

(
__forceinline int toInt( float f )
{
return _mm_cvtss_si32( _mm_load_ss( &f ) );
}
)

Funkce by byla zhruba stejně rychlá, jako to, co máme teď, ale netrpěla by přetečeními, a nepotřebovali bychom tedy také ani už `toLargeInt` (`to64bInt` ještě ano).

Takovou implementaci jsme měli připravenou už v r. 2003, tehdy jsem ji nakonec nepoužil s komentářem:

`/// SSE intrinsics are efficient, but require stack alignment`

Třeba už to teď bude fungovat dobře (tady by ve skutečnosti žádný stack alignment neměl být potřeba a dnešní překladač už to snad bude vědět.

[ondra 25.2.2013 14:10:31]:
 Zkusil jsem to, žádný stack alignment kód nikde ve funkci opravdu nevidim. 
 
 V případě, kdy je hodnota spočtená na fp0 (což je typické, pokud se hodnota vrací z funkce) to vygeneruje (např. z `ListResize`):
 
 (
 008CC066  fstp        dword ptr [oper1]  
 008CC069  movss       xmm1,dword ptr [oper1]  
 008CC06E  xorps       xmm0,xmm0  
 008CC071  movss       xmm0,xmm1  
 008CC075  cvtss2si    eax,xmm0
 )
 
 Pokud je hodnota už přímo spočtené v SSE registru (typický stav, pokud není volaná funkce), je to ještě lepší, odpadne fstp a movss z paměti, xorps a movss tam bohužel jako součást `_mm_load_ss` zůstane, bez toho se obejít neumím, ale i tak myslím, že z hlediska rychlosti to je dostatečná náhrada a získaná robustnost vůči přetečení je hezký bonus.
 Je to v 102473
 Pod GCC jsem neimplementoval, takže pod Linuxuem by se zatím problém s přetečením projevit nadále mohl. Trochu se bojím, abychom se na to časem nenapálili (pod Windows bude fungovat OK, na Linuxu nám to nečekaně přeteče).
 Nevím ale, jak to pod Linuxem přeložit nebo vyzkoušet, Linuxový server asi pro Dayz ještě nemáme, znamenalo by to asi přenést to do A2OA. Nevím taky, jestli pod GCC můžeme jen tak povolit USING_SSE - zná GCC `<xmmintrin.h>` (dle Google si myslím, že snad ano, ale možná je třeba použít `-msse` nebo i -mfpmath=sse)? 
