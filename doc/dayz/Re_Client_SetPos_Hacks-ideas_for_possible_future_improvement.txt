﻿> I will implement this as a proof of concept, so that other programmers can add more verification if/as needed.

Besides of ignoring such manipulaton attempts, server could also log them in the player profile if needed, perhaps in some aggregated form (e.g. cheat type - like "setPos" / "damage", count of detected cheat attempts, time of first cheat, time of last cheat) so that such players could be actively punished somehow (perhaps displayed to public ridicule in some clever way, like made harmless and revealed ingame so that other players could lynch them ).

[dhall 18/12/2013 12:43:50 p.m.]:
 That's a good idea, we could record them on the player and store them to the database, possibly issuing some kind of database wide effect on them.
