﻿===Outline
The mod is continued in the most economic method, being the central server reliance is removed, and the HiveEXT.dll searches for a local MySQL server. If one is not located, then it will run in non-persistent mode. If a MySQL server is available, it will use that to record the persistence. The mod will stay largely the same with optimizations, bugfixing, and improvements completed.

===Budget
The only direct costs from this approach is the running of the website (could be moved to BI servers?) and the hosting of content files (could be done entirely using torrents?).

===Plan
Engagement of Vipeax (DayZ Mod Team) to redesign HiveEXT to work according to the requirements outlined above. Suitable community team located to further develop the mod. Website be considered for moving to BI servers, or located as it is with the current community members.
[maruk 30.7.2012 23:28:33]:
 This is essential for smooth transition. Option to allow non-dedicated host for lower player numbers (like 4 maye 8) should be also evaluated for this.

[jirka 31.7.2012 7:08:23]:
 > the HiveEXT.dll searches for a local MySQL server. 
 > If a MySQL server is available, it will use that to record the persistence.
 
 Maybe, the MySQL server would not be needed in this situation. Persistency can be implemented through a local file or registry.

[maruk 31.7.2012 17:25:03]:
 >Maybe, the MySQL server would not be needed in this situation. Persistency can be implemented through a local file or registry.
 
 This is very interesting idea. If this could be combined with peer hosted servers, I think the mod can stay very popular and live for long time without much of active support from our end.

[ondra 31.7.2012 21:21:19]:
 > MySQL server would not be needed in this situation. Persistency can be implemented through a local file or registry.
 
 
 Perhaps [SQLite](http://www.sqlite.org/) might be used for the purpose, using a local file, but providing SQL interface?
