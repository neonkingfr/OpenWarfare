﻿When running server with the mission downloaded from the svn server used during night testing [dayz_Auto.ChernarusPlus.zip][Attachment1] in its ( mpmissions ) folder, there are nasty repetitive logs like:
(
 289.021: Changing from 1(0) for 1906 2140110735 to 2
 289.022: Changing from 0(0) for 1906 2140110735 to 2
10:34:50 Network simulation, time = 209.211
10:34:50 Server: add local message DeleteObject from 2
10:34:50 	id (time = 209.304)
10:34:50 	creator(0x20) = 2
10:34:50 	id(0x28) = 1906
10:34:50 Server: received message DeleteObject from 2
10:34:50 	id (time = 209.304)
10:34:50 	creator(0x20) = 2
10:34:50 	id(0x28) = 1906
)

The ( 2140110735 ) is ( 0x7F8F7F8F ), as `oInfo->currentUpdate[cls]` contains no current data.
Seems like some problem *s bublinama*?

( server.cfg ) mám nastavený tak, aby se mise [dayz\_Auto.ChernarusPlus.zip][Attachment1]  rovnou spustila:
(
persistent=1;
class Missions
{
 class DayZ_Auto				// name for the mission, can be anything
 {
   template = "dayz_Auto.ChernarusPlus";	// omit the .pbo suffix
   difficulty = "regular";
 };
)



[ondra 11.12.2013 13:12:43]:
 > Changing from 1(0) for 1906 2140110735 to 2 
 
 Po prozkoumání mi to připadá jako neškodná věc, která je tam pravděpodobně už dlouho. Stane se to vždy při přijetí prvního updatu svého druhu. Hlášení můžu pročistit, ale nevidím, že by to ukazovalo na nějaký hlubší problém.
 


[ondra 11.12.2013 13:19:17]:
 > Hlášení můžu pročistit
 
 To jsem i udělal, nyní se vypisuje jen skutečná změna zdroje zprávy.
 



[bebul 11.12.2013 14:11:32]:
 Ještě mi přijde trochu podezřelé, proč server vždycky objekt vytvoří a po velmi krátké době ho deleatuje?

[Attachment1]: https://projects.bistudio.com/dayz/dayz_Auto.ChernarusPlus_3.zip