﻿====Request
Request that radios have a configurable distance limitation, set via config such as:
(
 range = 1000; //maximum transmission range in meters
)

====Optional: Degrading transmissions
An additional feature, if it is possible and considered desirable without too much work, would be degrading transmission. A static track would be mixed with the transmission.
* more degradation would see higher volume for static, lower volume for transmission and vice versa
* degradation would be configured:
(
 rangeDegrade = 800; //range at which transmission starts to degrade
)
* Degradation would start at distances above *rangeDegrade* in linear form until maximum degradation at max range
* Default rangeDegrade would be set as max range (ie. no degradation)

[dhall 4/06/2013 12:24:02 p.m.]:
 text degradation could be done by randomly replacing characters with the "." character. The more degradation, the higher the chance a character will be replaced with "."

[bebul 11.6.2013 11:57:09]:
 Have discussed it with Frenkee. I would rather use some Audio Effect applied to given submix.
 In ArmA3 there are already some ( XAudio2 APO ) effects implemented, like /discretizer/, which can be also used as they are... And add some /gaussian noise/ maybe... http://www.musicdsp.org/archive.php?classid=1#168
 

[bebul 20.6.2013 15:42:43]:
 /rev.106737/
 ====range = 1000;
 The distance can be limited by ( range = 1000; ) stuff, but it is currently used globally for all transmitters.
 Note: to enable each radio to have different *range* set, each voice packet on Transmitter channel would need to contain the range parameter. (It is of course possible, not implemented yet, but we currently send the tuned /frequency/ in each transmitter voice packet, so we can do it.)
 
 ====Degradation
 No ( rangeDegrade ) is currently used. The engine uses some APO (Audio Processing Object) to each Transmitter voice.
  * there is some noise added
  * the voice is discretized to less and less levels (ending with only two at large distance)
  * to recognize it is Transmitter sound, some low level noise is added even in short distance
  * it seems, the interpolation between noise and voice is not the best solution. Maybe to make the voice missing more and more often in larger distances would sound better.
 
 When more players are speaking on the same frequencies, the resulting sound is using the APO of the worst one. There is one submix for each transmitter and making new submix for each pair player + transmitter would lead to quardatic count of resources, which I decided to avoid.
 

[bebul 24.6.2013 18:05:43]:
 Text in Chat on Transmitters is also degrading (dots instead of characters with increasing probability). There were other BUGs with transmitters, when switched ON/OFF or the tuned frequency was changed, it was still receiving the voice/text from other transmitters.
 /fixed: rev.106885/
