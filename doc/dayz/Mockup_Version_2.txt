﻿A new mockup idea for inventory that is a hybrid of the proposed together with the current. Should be more workable with less design and usability issues than the previous concept. No style is included, just design elements to give a basic idea.

 ![inventory\_2\_121115.jpg][Attachment1] 

[jirka 14.11.2012 14:25:00]:
 I agree with the separate area for items in pockets. Maybe the number of "slots" will vary later as well based on clothing.
 I'm not sure if items visible directly on the body (like vision on head, primary weapon in hands etc.) need a special slots outside character.
 
 We can also decide about final key bindings for shortcuts. Now, we already freed keys F1 .. F12 (used for units selection). The keys 1 .. 0 are no longer used for invocation of the commanding menu, but can be still used as shortcuts for navigation in its remains.

[maruk 14.11.2012 20:59:57]:
 I certainly would like to use numbers for these user defined shortcuts. I do not believe we need to keep any messages system connected to number in the game anymore and this use is common in other games (e.g. minecraft :)

[jirka 15.11.2012 9:10:16]:
 When thinking about it with in the context of new inventory system and clothing system, to have a single area for items on body have some issues.
 The pockets are probably linked to some clothing. So when I for example drop trousers, the items in its pockets will be dropped as well. In this point of view to have separate areas for each clothing type and moving items between them make a sense.

[dhall 15/11/2012 10:20:10 a.m.]:
 I think it would be best, at least for a start, to have clothing not affect carrying capacity. One thing I don't like about ArmA3's interface is the complexity that having items in the clothing brings.
 
 I think a limited number of body slots forces some restrictions on the player in terms of carrying capacity. Perhaps later we could do the container ideas we had discussed, and clothing could in some ways be an additional container.


[dhall 15/11/2012 3:26:42 p.m.]:
 Revised image uploaded based on discussions with Marek, Jirka, and Hladas.

[maruk 15.11.2012 15:40:04]:
 Re: Clothing and capacity I think the best way in future would be to simply allow each piece of clothing to contain a container with capacity additional to what is generally available to any character regardless of their clothing. Where and how to select active container is probably yet to be determined.

[Attachment1]: https://projects.bistudio.com/dayz/inventory_2_121115_2.jpg