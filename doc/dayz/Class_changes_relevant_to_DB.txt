﻿Here is the "sticky" you requested. :)

* Jeans => Jeans_Black

---

* BearTrap => StaticItem_BearTrap_Open
* FoodApple_Rotten => FoodItem_Apple_Rotten
* FoodBanana_Rotten => FoodItem_Banana_Rotten
* FoodBox0 => CardboardBox_Food
* FoodBreakfastPastryBox => FoodItem_BreakfastPastryBox
* FoodCanBakedBeans => FoodItem_CanBakedBeans
* FoodCanFrankBeans => FoodItem_CanFrankBeans
* FoodCanPasta => FoodItem_CanPasta
* FoodCanSardines => FoodItem_CanSardines
* FoodCerealBoxLarge => FoodItem_CerealBoxLarge
* FoodEdible => FoodItem_Edible
* FoodKiwi_Rotten => FoodItem_Kiwi_Rotten
* FoodSteakCooked => FoodItem_SteakCooked
* FoodSteakRaw => FoodItem_SteakRaw
* Hedgehog => StaticItem_Hedgehog
* ItemAntibiotic => MedicalItem_Antibiotics
* ItemBandage => MedicalItem_Bandage
* ItemBloodbag => MedicalItem_Bloodbag
* ItemDefibrillator => MedicalItem_Defibrillator
* ItemEpinephrine => MedicalItem_Epinephrine
* ItemEtool => StaticItem_EntrenchingTool
* ItemFlashlight => AttachmentsItem_Flashlight
* ItemFlashlightRed  => AttachmentsItem_FlashlightRed
* ItemGenerator => VehiclePartsItem_PortableGenerator
* ItemHatchet => WeaponItem_Hatchet
* ItemHeatPack => MiscItem_HeatPack
* ItemJerrycan => VehiclePartsItem_JerryCan
* ItemJerrycanEmpty => VehiclePartsItem_JerryCanEmpty
* ItemKnife => WeaponItem_Knife
* ItemMatchbox => MiscItem_Matchbox
* ItemMorphine => MedicalItem_Morphine
* ItemPainkiller => MedicalItem_Painkiller
* ItemSandbag => StaticItem_Sandbag
* ItemSodaCoke => DrinksItem_SodaCoke
* ItemSodaEmpty2 => DrinksItem_SodaEmpty2
* ItemSodaEmpty3 => DrinksItem_SodaEmpty3
* ItemSodaMdew => DrinksItem_SodaMdew
* ItemSodaPepsi => DrinksItem_SodaPepsi
* ItemTankTrap => StaticItem_TankTrapKit
* ItemTent => MiscItem_Tent
* ItemToolbox => MiscItem_Toolbox
* ItemWaterbottle => DrinksItem_Waterbottle
* ItemWaterbottleUnfilled => DrinksItem_WaterbottleUnfilled
* ItemWire => StaticItem_WireFencingKit
* MedBox0 => CardboardBox_Medical
* PartEngine => VehiclePartsItem_Engine
* PartFueltank => VehiclePartsItem_Fueltank
* PartGeneric => VehiclePartsItem_ScrapMetal
* PartGlass => VehiclePartsItem_WindscreenGlass
* PartVRotor => VehiclePartsItem_VRotor
* PartWheel => VehiclePartsItem_CarWheel
* PartWoodPile => MiscItem_WoodPile
* Sandbag1 => StaticItem_BagFenceLong
* TrapBear => StaticItem_BearTrap_Closed
* Wire_cat1 => StaticItem_Barbedwire
* Wire_cat2 => StaticItem_Barbedwire2

[dhall 25/02/2013 11:29:57 a.m.]:
 Jeans > Jeans_Black done

[qeberle 26.02.2013 12:10:34]:
 * Remove Put
 * Remove Throw
