﻿Soon we want to have clothing configured to show different models in different situations:
- when lying on ground
- as a proxy on Man
- as a proxy on Woman

I think the best solution will be to have a single config entry for each piece of clothing (new simulation class will be needed) with values:
- model ... will be used when lying on ground
- set of models for different character models
  + which model from this set will be used will depend on the new value of the character config (in engine, values will be represented by DynEnum)

The only problem is the terminology. I need to find names for:
- entry in the character config, telling what set of clothing to use (values "man", "woman" etc.)
- class in the clothing config, where mapping from set of clothing to p3d name will be present (( class XXX {man="dz/...p3d"; woman="dz/...p3d"} ))
- would be nice if both will be the same or similar (singular / plural)

Basic ideas (I do not like none of them): bodyType, modelClass, clothingSet ...


[qeberle 05.02.2013 16:03:29]:
 My suggestion:
 
 (
 clothingType="man/women/..";
 class cfgClothingTypes
 )

[dhall 5/02/2013 4:33:35 p.m.]:
 best to use *male* and *female* rather than man/woman, as it is a more age specific and formal term

[jirka 6.2.2013 7:26:45]:
 OK, so can you please:
 1. add entry ( clothingType="male/female"; ) to the character configs (I will add clothingType="male" to the core, so only change for woman will be needed)
 2. configure a single item in a new way:
  - simulation="clothing" instead inherited "inventoryItem"
  - class ClothingTypes with values male, female (cfg prefix is not needed, it is not a root class)
 
 I will start the engine changes then.

[qeberle 06.02.2013 12:29:16]:
 Sure can do.
 
 Need a few clarifications first though:
 
 1) I am to set to every female unit class:
  > clothingType="female";
  Right?
 
 2) Unsure about that part:
 
  > simulation=„clothing“ instead inherited „inventoryItem“
  > class ClothingTypes with values male, female (cfg prefix is not needed, it is not a root class)
 
 a) Wouldn't it be better to introduce in core config:
 
 (
 class WearableBase: InventoryItemBase// or ClothingBase
 {
  vehicleClass = "Clothing";
   simulation=„clothing";
 )
 
  and adjust all Wearable subclasses alongside. (adjust inheritance to this new subclass and remove vehicleClass definition)
 
 b) class ClothingTypes is a subclass of a clothing item/class, right? Like
 
 (
 class Hoodie: ...
 {
  class ClothingTypes
  {
   male = ..
   female = ..
  };
 };
 )

[jirka 6.2.2013 12:57:02]:
 Ad 1. Right.
 Ad 2.a. Yes, this will be the final solution. For now, I need a single testing item to make it working.
 Ad 2.b. Exactly.

[qeberle 06.02.2013 14:25:20]:
 Ok great.

[dhall 6/02/2013 3:16:30 p.m.]:
 All clothing is required to be unisex. Every clothing item must be usable by both sexes.

[qeberle 07.02.2013 07:22:22]:
 Test class available:
 
 * Units: SurvivorMale and SurvivorFemale
  in ( \DZ\characters\cfgCharacters.hpp )
 * Clothing: BaseballCap
  in ( \DZ\characters\headgear\config.cpp )

[qeberle 07.02.2013 07:24:21]:
 One question about the normal model definition - is it to be empty or to set the male p3d as default/fallback?
 
 (
 //baseballcap
 	class BaseballCap: HeadgearBase
 	{
 		scope = public;
 		displayName="Black Baseball Cap";
 		model = "\DZ\characters\headgear\baseballcap_m.p3d";//
 		simulation = "clothing";
 		class ClothingTypes
 		{
 			male = "\DZ\characters\headgear\baseballcap_m.p3d";
 			female = "\DZ\characters\headgear\baseballcap_f.p3d";
 		};
 	};
 )

[jirka 7.2.2013 7:34:44]:
 model will tell how the object will be shown when lying on ground. It will be probably used also as a fallback and for UI.

[jirka 7.2.2013 8:25:45]:
 Thanks, looks like the configuration is working.
 
 Unfortunately, I cannot test it visually. We do not have proper women model configured yet (the one in game have man's head and no proxy for a cap).
 
 Is it possible to make it working correctly?

[qeberle 07.02.2013 08:32:06]:
 Lukas should be able to take care of that.

[qeberle 07.02.2013 09:34:26]:
 Ok got it sorted (at least Head+Face). Please try again Jirka.

[jirka 7.2.2013 12:47:01]:
 Head is ok now, thanks.

[dhall 7/02/2013 1:09:53 p.m.]:
 Lukas has been on sick leave for the last two weeks. We're very short on people at the moment I'll see if I can assemble the woman (lol).

[jirka 7.2.2013 13:31:23]:
 I tried to enforce female cap for a man character, but I do not see any difference. Are the models so similar or have I some bug in the code?

[jirka 7.2.2013 13:50:51]:
 Seems all is prepared in the engine (with very limited testing so far), maybe we can start to configure other items.
 
 Which of these base classes need to be switched to "clothing" simulation and which could stay as "inventoryItem"?
 - BagBase, EyewearBase
 - TopwearBase, BottomwearBase, VestBase
 - HeadgearBase, FootwearBase, GlovesBase

[qeberle 07.02.2013 14:14:07]:
 Sorry - unlucky pick by me. The caps are identical..
 Will configure some more/all other items now.
 
 The only different of clothing to inventoryItem is the single class with model replacement?
 
 We need the later for (almost) all for the ground vs onBody representation, dont we?

[dhall 7/02/2013 2:29:36 p.m.]:
 Wait, ground + onbody should be the same object - just the engine chooses to display a different p3d when on ground.
 
 All "clothing" items must be those which can be seen when they are worn - such as all those listed by Jirka.
 
 Although, I of that list BagBase and EyewearBase will be the same for both sexes

[qeberle 07.02.2013 14:36:13]:
 > jirka 7.2.2013 7:34:44
 >  model will tell how the object will be shown when lying on ground. It will be probably used also as a fallback and for UI.
 
 Well either I get this wrong or there is a different understanding between you and Jirka here?

[jirka 7.2.2013 14:56:22]:
 Maybe misunderstanding is caused by two meanings of "model".
 
 The clothing will be always a single object (single config class). It will show different p3ds bases on situation.
 
 When on ground, p3d from "model" entry will be used. When wearing, proper model from "ClothingTypes" will be used instead.

[jirka 7.2.2013 15:27:27]:
 > ... need to be switched to „clothing“ simulation
 > - TopwearBase, BottomwearBase, VestBase
 > - HeadgearBase, FootwearBase, GlovesBase
 
 It will be probably a bit more complicated. We will want only real clothing to inherit from this. The default body parts (MaleTorso etc.) should stay with simulation="inventoryItem".

[qeberle 07.02.2013 15:29:56]:
 How about having simulation="clothing" set in base class and set simulation="inventoryItem" to the default body parts?

[dhall 7/02/2013 3:39:37 p.m.]:
 I've made a default example in the tops config:
 (
 	class HoodieBase: TopwearBase
 	{
 		displayName = "Hoodie";
 		model = "\DZ\characters\tops\hoodie_grd.p3d";
 		hiddenSelectionsTextures[] = {"\DZ\characters\tops\Data\Hoodie_blue_co.paa"};
 		hiddenSelections[] = {"camo1"};
 
 		simulation = "clothing"; 
 		class ClothingTypes 
 		{ 
 			male = "\DZ\characters\tops\hoodie_m.p3d"; 
 			female = "\DZ\characters\tops\hoodie_f.p3d"; 
 		};
 	};
 )
 
 If you could configure the hoodies all with this, then we can start testing and move onto the other clothing items.
 
 I confirmed it works with the female model, with a few bugs (shadow lod doesn't seem to work for some reason)

[qeberle 07.02.2013 15:59:03]:
 Well I know what to do - just not exactly for which and for which not. See subpost.

[qeberle 07.02.2013 16:29:10]:
 Should be done. Please test.
 I will try to adjust all class name changes in the source files too.

[qeberle 08.02.2013 08:05:29]:
 Thanks for the clarification and confirm Jirka.
 The clothing should be ready.
 
 Only left:
 * artists to update p3ds for hiddenSelectionsTextures handling
 * adjust related config definitions afterwards
 * decide if simulation="inventoryItem" or simulation="clothing" should be default 
   my vote is for clothing, as only few will have inventoryItem
 * what to do with placeholder p3d (need to check with Lukas)
[jirka 8.2.2013 13:44:57]:
 > decide if simulation=„inventoryItem“ or simulation=„clothing“ should be default
 
 At sure "clothing" should be the default. "inventoryItem" will be used only for body parts.
 
 I will change the classes HeadgearBase, TopwearBase, BottomwearBase, FootwearBase, VestBase and GlovesBase to have simulation="clothing";
 
 Before this step, please change body parts (to avoid warnings) to:
 - inherit from InventoryItemBase
 - define correct inventorySlot (values Body, Legs, Feet, Gloves)

[qeberle 08.02.2013 14:00:14]:
 Done. 
 
 Body parts are now scope=protected now.
 Or do you want them available in the editor?
 
 Removed simulation="clothing" from regular cloths as well.

[jirka 8.2.2013 14:04:30]:
 Great, I will change the core config. (to have body parts protected is OK)

[qeberle 10.02.2013 10:39:40]:
 Is this task done aside from hiddenSelections?

[jirka 11.2.2013 7:27:28]:
 I hope so.

[dhall 12/02/2013 11:35:59 a.m.]:
 Yes it's done
