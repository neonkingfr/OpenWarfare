﻿Jak to je/bude s integrací *BattlEye* do DayZ?

[2:28:00 Skype Bastian (BattlEye)]:
 > bebul is PoC
 > /---Dean, Today 2:26 PM/
 you know about it?

Spolu jsme se dříve bavili, že snad vystačíme pouze s *VAC*. Je to pravda?
Bastian mě s blížícím se vydáním DayZ Alpha bombarduje dotazy a je čím dál těžší neodpovídat.

Nerad bych, aby vznikly nějaké zmatky a nedorozumění. Já osobně se domnívám, že BattEye ochrání DayZ lépe, než VAC a že na něj nakonec, možná brzy, dojde. Ale je to víc pocit, než abych argumentoval jinak než Dwardenem.

[maruk 16. 12. 2013 15:15:03]:
 Mám pocit, že Dean chtěl i BE a že s Bastien je snad dohodnuto, že BE pro Dayz udělá. Smlouva si ještě podepsaná není.

[maruk 16. 12. 2013 15:15:30]:
 Ale podmínky jsou domluvené.

[bebul 16.12.2013 16:16:19]:
 OK, takže přípdně nějspíš můžu s Bastianem začít pracovat na implementaci.
 (Mělo by to být jednoduché, protože věci kolem Steamu, související s BE (autentizace) už vyšlapala ArmA3)
