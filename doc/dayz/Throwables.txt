﻿[jirka 5.2.2013 7:45:12]:
 > * What is to happen with ThrownObjects (JackDaniels, SodaCan, TinCan)?
 
 I think no different objects for empty and full bottles or cans will be needed. Almost all objects will be throwable, we will define it in the config somehow.

[qeberle 06.02.2013 08:47:17]:
 > ThrownObjects
 
 These are based on Grenades atm. As the Throw (and Put) weapon is to be removed, these probably need to get converted to separate weapons.
 Hence also moved to DZ/Weapons/Infantry/Throwables.

[jirka 6.2.2013 9:45:12]:
 > As the Throw (and Put) weapon is to be removed, these probably need to get converted to separate weapons.
 
 Not sure about it. Ability to be thrown (put) will be quite common for almost all items in my opinion.

[qeberle 06.02.2013 11:41:29]:
 Split this a separate topic as it seems to relate very much how the future interaction/use of such items should work.
 
 My suggestion:
 * Make "Switch Weapon Mode" only applied to weapons
 * And no longer throwables like Grenades, Smokegrenades and more
 * These should be only possible to select via the Quick selection bar

 The basic rational is as you say we might have quite a few items to throw. 
 Endless F weapon mode switching or huge action menu list to select the desired throwable is no good.
 So they should be only usable/selectable via quick action bar.
 
 Thoughts?

[jirka 6.2.2013 13:03:50]:
 > So they should be only usable/selectable via quick action bar.
 
 Yes, this was the idea.
 By F key, only modes of the current weapon will be switched. Switching weapons will be possible only through the quick bar.
 
 We have to decide if invoking a throwable object will throw it directly or if it will be taken to hands only (and to throw it later by a different action, like "fire").

[qeberle 06.02.2013 13:43:53]:
 Ok great :)
 
 So i wont touch the throwableObjects for now,
 and wait for possible future config adjustments to be done.
