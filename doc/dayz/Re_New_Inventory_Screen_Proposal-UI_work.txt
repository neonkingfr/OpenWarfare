﻿How is the situation here - should I start the UI work?

If so, what is the current design you want to see done. Thanks
[maruk 15.2.2013 11:57:48]:
 Jirka is on vacation but I think if you can prototype it based on   [Re: New Inventory Screen Proposal - UI work](https://projects.bistudio.com/dayz/Re_New_Inventory_Screen_Proposal-UI_work.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2462) it would be fantastic.

[qeberle 15.02.2013 12:05:24]:
 Sure can do. Your link is bugged though. Can you please post again Marek.

[maruk 19. 2. 2013 9:17:10]:
 [Re: New mockup Variant 3](https://projects.bistudio.com/dayz/Re_New_mockup_Variant_3.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2376)

[dhall 19/02/2013 10:25:02 a.m.]:
 I'm kind of confused, the slot bars are only temporary - and also, the inventory screen has been largely agreed and is implemented by Jirka. So I'm not sure where any of this is coming from?
 
 Once we have the drag and drop on body functionality working the body slot areas will disappear... Then we just have the hands, and a few other slots + the clothing slot areas to place?

[qeberle 19.02.2013 10:54:46]:
 So we wont have 2d areas for all player body slots?
 
 My understanding so far was that the basic technology is done.
 
 This task is merely to make Jirka's basic design more beautiful and usable. That's all to it.

[dhall 19/02/2013 11:13:02 a.m.]:
 Incorrect, see the original design documents. Jirka has noted he added the slots simply because we haven't got the drag and drop implemented yet.
 
 The basic functionality is done, but not the remainder of the inventory system. There has never been a plan to have 2d areas for the body slots beyond initial testing.

[qeberle 19.02.2013 11:23:46]:
 No need to get annoyed Dean.
 
 It is not easy to follow what the current state of afairs is.
 Just tell me what needs to be done and I will see to it.
 
 If we are to improve it later, so be it.
