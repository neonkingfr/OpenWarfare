﻿It would be nice to have inventory items configured in a new way:
- definded in *CfgVehicles*, not in *CfgMagazines* or *CfgWeapons*
- inherits from *class InventoryItemBase*

This will save us some memory and configuration and enable some scripting features (default action processed on weapons and magazines will be hardcoded, on common inventory items will be scripted).

Also, to organize items hierarchically based on usage (Beverage, Food, Junk etc.) would be fine (this will allow us add some scripting to the base class instead each individual one).

[dhall 22/01/2013 4:04:52 p.m.]:
 kju, can you create a base Item based on the can of beans?
 
 This has the current class of  **FoodCanBakedBeans**
 

[qeberle 22.01.2013 16:38:55]:
 Done for FoodCanBakedBeans.
 Defined in .\\DZ\\weapons\\items\\cfgVehicles.hpp.
 
 Please check if everything is correct.
 If alright, I will move all remaining items to cfgVehicles, ok?

[dhall 23/01/2013 1:17:50 p.m.]:
 Would be good to split it out and start breaking down the items.
 
 Might I recommend that we have:
 
 /DZ/Items/
 
 Then have:
 
 /DZ/Items/Food
 
 Etc...? Thoughts?

[qeberle 05.02.2013 06:59:31]:
 Finished the data moval to the new DZ/items container and put the p3d, textures and containers in said subfolder.
[qeberle 06.02.2013 09:23:07]:
 > change all cfgMagazines to cfgVehicles
 > do the inventory size definitions
 
 done
