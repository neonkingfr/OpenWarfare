﻿> - every build of application (exe) or data (pbo) will publish the result to the distribution SVN (https://of.bistudio.com/svn/dist/DayZ)

I'm trying to implement this first step, but no success yet (unable to make Jenkins SVN Publisher plugin working).
[jirka 12.9.2012 10:10:16]:
 I didn't succeed with SVN Publisher plugin and the addon also have quite limited functionality (does not allow check-in messages for example).
 
 Instead, I'm trying to call ( svn import ) from command line now. Some tuning is still needed to allow overwrite files in the SVN repository.

[jirka 12.9.2012 11:28:52]:
 Now, I'm using ( svn delete ) on old data. This works, but I'm not very happy with it (too much commits).
 
 Maybe shallow check-out / check-in would work better.

[jirka 13.9.2012 9:21:13]:
 Internal exe is now published  to the distribution SVN as well, so https://of.bistudio.com/svn/dist/DayZ should contain whole distribution of DayZ game already. (the biggest problem now seems to be a lot of missing strings)

[dhall 13/09/2012 10:27:51 a.m.]:
 For missing string, at the moment I'm just using the pbo from TKOH for language. I'm going to tidy the UI and strings up later once I have ChernarusPlus loading properly.

[jirka 8.10.2012 9:38:57]:
 To avoid too many revisions and support better performance of operations, I re-worked the pipeline this way:
 - on the build server a single shared working copy is maintained
 - each job performs distribution this way:
   + process check-out of working copy (which does nothing in typical situations)
   + copies the build results to the working copy
   + process SVN add operation on the root of working copy to add new files
   + check-in the working copy
