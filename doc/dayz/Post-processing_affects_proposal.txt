﻿Proposal
=======

We  were contacted by the Real Colours Real Nights team (RCRN), that made a mod for The Elder Scrolls Skyrim asking if we would be interested in commissioning their team to work on improving DayZ standalone.They have included a cost estimate that is extremely high but they were open to discussion on this, so we can possible approach them with something that is more realistic.



Key points
=======

* RCRN improved the effects of Skyrim by a great margin.
* RCRN is the most downloaded Skyrim mod at about 200,000 downloads, which is a testament to it's benefit.
* They want to implement their technologies into DayZ;

		* Hybrid shaders - This is the dynamic link between the engine and the postprocessing.

		* Realistic lighting and dynamic depth of field - Method of simulating ambient fog to increase tension.

		* Premium FXAA 3.0 - More efficient menthod of Anti-Aliasing that would need some modification to work with DayZ.

		* TwinPass HDR 3.0 - It renders the HDR a second time  before the final result. Meaning warmer colours.

		* Dynamic interiors - Developed for skyrim, more information is needed for the RCRN team to see if this is portable.




Benefits
===

* They state that by using their postprocess HDR it could reduce system demand and improve performance.
* Reduces the workload for Bohemia Interactive
* Can Improve the visual quality of DayZ exponentially
* Improving the Night effects could also quell complaints we had with the mod about it becomming unplayable during the night.


Cons
===

* We become reliant on their technology and through that their service
* There may be unforseen problems with the implementation
* Unrealistic expectations of payment.



In detail
===


Breakdown of their costs- [RCRN\_quotation\_proposal.pdf][Attachment2] 



Detailed proposal - [RCRN\_proposal.pdf][Attachment1] 

[maruk 12.10.2012 12:26:25]:
 I do not see this important at all at this stage. Even later, this type of improvements can be done realtively easily by any enginner inhouse.

[Attachment1]: https://projects.bistudio.com/dayz/RCRN_proposal.pdf
[Attachment2]: https://projects.bistudio.com/dayz/RCRN_quotation_proposal.pdf