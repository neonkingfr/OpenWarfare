﻿[jirka 17.12.2012 9:30:21]:
 I do not like to have packAllTextures set for regular addons.

 This was ment for addons where most of textures are unmapped (landscape data, UI).
 For addons like this, I'd prefer to list data referenced by config explicitly in textures.lst.

 Otherwise, in long term data will be polluted by old versions of textures or textures for removed models.

[qeberle 17.12.2012 9:52:21]:
 Agreed. I could adjust the xmls accordingly.
 However it might be good to learn first why it was set to true for all/most containers.

 Dean did you set these up? If so, was it set intentional or more like copy+paste? Cheers.

 Probably best to do post MP test to avoid short term problems.
[qeberle 20.01.2013 19:39:51]:
 Removed packAllTextures in all build.xml except:
 * ui
 * uiFonts
 * world/CP/data
 * world/test/data
 
 Rebuilt all containers. Task done.
