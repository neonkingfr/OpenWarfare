﻿> Saw a black mosin on a server, which is weird because it is not configured to spawn at all. If a client manages to somehow manipulate their client to make an object, does it still have the ability to broadcast this over the server and - if so - will the server accept this object as existing?

Processing of `createVehicle` sent from clients was disabled only recently, today in 113782. Until now `createVehicle` was disabled only client-side, which could be hacked.

Moreover, I have found only the array variant of `createVehicle` was disabled client-side, as a result of my mistake in [Disabling Client-side commands](https://projects.bistudio.com/dayz/Disabling_Client-side_commands.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2883) - I found one instance of `createVehicle` and did not look for another. I think it is most likely this was used as an attack vector now. Disabled in 113818


