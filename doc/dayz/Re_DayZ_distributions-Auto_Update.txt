﻿AutoUpdater for DayZ internal versions
======================================


The AutoUpdater for internal DayZ version will be based on [SvnAutoUpdater - poznámky k nasazení](https://of.bistudio.com/svn/of/data/SvnAutoUpdater-poznámky_k_nasazení.txt?server=https%3A%2F%2Fof.bistudio.com%2Fof%2Fresources%2F&database=&id=918).
It would work as follows:

There would be TWO separate folders:
 a) game folder (say /O:\\DayZ/)
 b) working copy of game installation (say /O:\\DayZ\\SvnUpdate/)
/Note: Game itself should be placed outside the working copy in order to make the background download possible. On the other way round the data would be placed in 3 coppies (game, working copy and its .svn folder)/

The game itself would act as the client of some SvnAutoUpdate "server" application.
- DayZ would connect to DayZUpdate application
 - when no DayZUpdate is currently running, spawn it
- when DayZ version update is currently in progress, the DayZ would not start (MessageBox should be shown)
- When new version is available, DayZ would show the MessageBox which would allow you to copy
 - it would be checked on DayZ startup (but the game can be notified later too)
 - the game would start automatically after the copy process is over
 - it should also check for other DayZ instances running... (User should close them manualy).
- copy only those files whose source time is newer than the destination time.
- Deleted files/folders detection and maintenance
 - we are able to detect which files were deleted during working copy update. These would be also deleted from the game directory, one by one.

[jirka 2.10.2012 7:23:21]:
 Remarks:
 
 > when DayZ version update is currently in progress, the DayZ would not start 
 What do you mean by version update exactly? Working copy update need not prevent game from launch, but user should be warned that restart would be needed later.
 
 > it should also check for other DayZ instances running… (User should close them manualy)
 Server should know about all running applications, so the same process will take place (autoupdate will be offered to user in all applications).
 
 We can think how to update also DayZUpdate application. It could be downloaded from SVN together with the other content, but need to be copied and restarted in other way (maybe by DayZ executable, maybe some way by itself, using exe flags forcing loading exe to memory).
 

[bebul 2.10.2012 10:08:45]:
 >> when DayZ version update is currently in progress, the DayZ would not start
 > What do you mean by version update exactly? Working copy update need not prevent game from launch, but user should be warned that restart would be needed later.
 OK, bad terminology. We should make difference between
 a) Svn Update 
 b) Copying of "Working copy" to Game Folder
 When b) is in action, then no running DayZ instance allowed.
 
 > Server should know about all running applications, so the same process will take place (autoupdate will be offered to user in all applications).
 Server/Updater should also try to find all DayZ instances running, even when not communicating to the Server/Updater. It may happen, somebody would close the server but not DayZ. The communication is provided through pipes atm. If we want to make server able to contact already running DayZ instances, we should use some windows messages or something.
