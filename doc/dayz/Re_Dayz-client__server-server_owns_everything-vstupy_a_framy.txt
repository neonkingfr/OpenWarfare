﻿>  Teď jdu na další krok: parallelně s dosavadním `_clientInfo`, které přenášelo na server pozici kamery na klientu, vytvořím `ClientInput` vlastněný klientem, které bude přenášet stav vstupů a `ClientOutput`, vlastněný serverem

Budu muset velmi pečlivě rozmyslet, jak přesně se pracuje s framy. Frame na klientu i na serveru je jiný.

Problémy k řešení:
- ToDo akce se každý frame mažou (zatím řešeno tak, že při přenosu klient-server se nemažou, jen nastavují)
- pokud je klávesa zmáčknuta jen část času, hodnota vstupu odpovídá tomu, jakou část času byla zmáčknuta. Sebekratší stisk je nenulový. Teď se z klienta na server prostě přenáší poslední hodnota. V důsledku se hodnoty z klienta mohou i ztratit, typicky pokud má klient lepší fps, než server



[jirka 24.4.2013 15:58:04]:
 Mám teď se vstupy nějaké problémy.
 
 V situaci s dedikovaným serverem a klientem na jednom PC mi klient špatně reaguje na shortcuty. Logováním jsem zjistil:
 - v `ClientInputLocal::ScanInput` nejsou `_subscribedToDo` pro shortcuty nastaveny v každém volání (jen v cca 10-20% případů), většinou tak akci minou
 - shortcuty by se na serveru měly subscribovat v `Person::SimulateWeaponInputs` (ta se skutečně volá, zkusím ještě prozkoumat zda dost často)



[jirka 25.4.2013 8:20:40]:
 Přidal jsem si teď nějaké diagnostiky na server, jejich výstup je:
 none(
 Keyboard pilot, frame 17732/17736
  - shortcuts subscribed
  - subscription sent in frame 17737
  - subscription sent in frame 17738
 Keyboard pilot, frame 17736/17741
  - shortcuts subscribed
  - subscription sent in frame 17742
  - subscription sent in frame 17743
 Keyboard pilot, frame 17741/17746
  - shortcuts subscribed
  - subscription sent in frame 17747
  - subscription sent in frame 17748
 Keyboard pilot, frame 17746/17751
  - shortcuts subscribed
  - subscription sent in frame 17752
  - subscription sent in frame 17753
 Keyboard pilot, frame 17751/17757
  - shortcuts subscribed
  - subscription sent in frame 17758
  - subscription sent in frame 17759
 Keyboard pilot, frame 17757/17761
  - shortcuts subscribed
  - subscription sent in frame 17762
  - subscription sent in frame 17763
 Keyboard pilot, frame 17761/17765
  - shortcuts subscribed
  - subscription sent in frame 17766
  - subscription sent in frame 17767
 Keyboard pilot, frame 17765/17771
  - shortcuts subscribed
  - subscription sent in frame 17772
  - subscription sent in frame 17773
 )
 Interpretuji je tak, že KeyboardPilot není na DS volaný v každém framu. To vede k tomu, že ve snímku kdy se zavolá pošleme output se správnými subscription, ale hned v následujícím snímku se odešle nový output bez subscription.
 Zvláštní je, že subscription ostatních událostí jsou v každém framu.


[jirka 25.4.2013 8:46:20]:
 A s rozšířenou diagnostikou to zas chápat přestávám:
 none(
 Keyboard pilot, frame 7966/7976
  - shortcuts subscribed
  - subscription sent in frame 7977: ToggleRaiseWeapon Item0 Item1 Item2 Item3 Item4 Item5 Item6 Item7 Item8 Item9 ToggleWeapons OpticsMode DefaultAction LookCenter LookAroundToggle ZoomOutToggle ZoomInToggle ZeroingUp ZeroingDown WalkRunToggle AimWeaponToggle AimWeapon3s LeanLeftToggle LeanRightToggle Headlights Walk MoveUp MoveDown Stand Crouch Prone EvasiveForward EvasiveBack
  - subscription sent in frame 7978:
 Keyboard pilot, frame 7976/7986
  - shortcuts subscribed
  - subscription sent in frame 7987: ToggleRaiseWeapon Item0 Item1 Item2 Item3 Item4 Item5 Item6 Item7 Item8 Item9 ToggleWeapons OpticsMode DefaultAction LookCenter LookAroundToggle ZoomOutToggle ZoomInToggle ZeroingUp ZeroingDown WalkRunToggle AimWeaponToggle AimWeapon3s LeanLeftToggle LeanRightToggle Headlights Walk MoveUp MoveDown Stand Crouch Prone EvasiveForward EvasiveBack
  - subscription sent in frame 7988:
 )
 Zatímco třeba na `UACrouch` se reaguje vždy, na `UAItem0` jen zřídka. Přitom se obě akce subscribují v rámci `Soldier::KeyboardPilot` a odesílají se stejně.


[jirka 25.4.2013 9:38:12]:
 Problém se zdá být na klientu. Zdá se, že správně fungují jen ty klávesy, které se čtou i na klientu (mají na klientu nastavené `_actionToDoStateUsed`).
 
 V KeyboardPilot se SimulateWeaponInputs volá jen na serveru, ostatní akce (např. v rámci UpdateRaiseWeapon nebo ProcessFire) i na klientu:
 (
   bool fullSimulation = !GetNetworkManager().IsClient() || GetNetworkManager().IsServer();
   InputInterface *input = GetNetworkManager().GetInputInterface(GetRemotePlayer());
   UpdateRaiseWeapon(input);
 
   if (fullSimulation)
   {
     SimulateWeaponInputs(input,_brain,context);
   }
   ProcessFire(input);
 )
 
 Diagnostika na klientu pak většinou vypadá:
 none(
 Subscription received in frame 1928: ToggleRaiseWeapon Item0 Item1 Item2 Item3 Item4 Item5 Item6 Item7 Item8 Item9 ToggleWeapons OpticsMode DefaultAction LookCenter LookAroundToggle ZoomOutToggle ZoomInToggle WalkRunToggle AimWeaponToggle AimWeapon3s LeanLeftToggle LeanRightToggle Headlights Walk MoveUp MoveDown Stand Crouch Prone EvasiveForward EvasiveBack
 Updated subscription: ToggleRaiseWeapon Item0 Item1 Item2 Item3 Item4 Item5 Item6 Item7 Item8 Item9 ToggleWeapons OpticsMode DefaultAction LookCenter LookAroundToggle ZoomOutToggle ZoomInToggle WalkRunToggle AimWeaponToggle AimWeapon3s LeanLeftToggle LeanRightToggle Headlights Walk MoveUp MoveDown Stand Crouch Prone EvasiveForward EvasiveBack
 Subscription received in frame 1928:
 Updated subscription:
 Keyboard pilot, frame 1927/1928
  297.147: Subscribed To Do:
  297.147:  - ToggleRaiseWeapon: used
  297.148:  - DefaultAction: used
  297.148:  - LookCenter: used
  297.148:  - LookAroundToggle: used
  297.149:  - ZoomOutToggle: used
  297.149:  - ZoomInToggle: used
  297.149:  - WalkRunToggle: used
  297.150:  - AimWeaponToggle: used
  297.150:  - AimWeapon3s: used
  297.151:  - LeanLeftToggle: used
  297.151:  - LeanRightToggle: used
  297.152:  - Headlights: used
  297.152:  - Walk: used
  297.153:  - MoveUp: used
  297.153:  - MoveDown: used
  297.154:  - Stand: used
  297.154:  - Crouch: used
  297.155:  - Prone: used
  297.155:  - EvasiveForward: used
  297.155:  - EvasiveBack: used
 )
 
 a jen občas:
 
 none(
 Subscription received in frame 1569: ToggleRaiseWeapon Item0 Item1 Item2 Item3 Item4 Item5 Item6 Item7 Item8 Item9 ToggleWeapons OpticsMode DefaultAction LookCenter LookAroundToggle ZoomOutToggle ZoomInToggle WalkRunToggle AimWeaponToggle AimWeapon3s LeanLeftToggle LeanRightToggle Headlights Walk MoveUp MoveDown Stand Crouch Prone EvasiveForward EvasiveBack
 Updated subscription: ToggleRaiseWeapon Item0 Item1 Item2 Item3 Item4 Item5 Item6 Item7 Item8 Item9 ToggleWeapons OpticsMode DefaultAction LookCenter LookAroundToggle ZoomOutToggle ZoomInToggle WalkRunToggle AimWeaponToggle AimWeapon3s LeanLeftToggle LeanRightToggle Headlights Walk MoveUp MoveDown Stand Crouch Prone EvasiveForward EvasiveBack
 Keyboard pilot, frame 1568/1569
  269.304: Subscribed To Do:
  269.305:  - ToggleRaiseWeapon: used
  269.305:  - Item0: ok
  269.306:  - Item1: ok
  269.306:  - Item2: ok
  269.307:  - Item3: ok
  269.307:  - Item4: ok
  269.308:  - Item5: ok
  269.308:  - Item6: ok
  269.309:  - Item7: ok
  269.309:  - Item8: ok
  269.310:  - Item9: ok
  269.310:  - ToggleWeapons: ok
  269.311:  - OpticsMode: ok
  269.312:  - DefaultAction: used
  269.312:  - LookCenter: used
  269.313:  - LookAroundToggle: used
  269.313:  - ZoomOutToggle: used
  269.314:  - ZoomInToggle: used
  269.314:  - WalkRunToggle: used
  269.315:  - AimWeaponToggle: used
  269.315:  - AimWeapon3s: used
  269.316:  - LeanLeftToggle: used
  269.316:  - LeanRightToggle: used
  269.317:  - Headlights: used
  269.317:  - Walk: used
  269.318:  - MoveUp: used
  269.318:  - MoveDown: used
  269.319:  - Stand: used
  269.319:  - Crouch: used
  269.319:  - Prone: used
  269.320:  - EvasiveForward: used
  269.320:  - EvasiveBack: used
 )
 


[jirka 25.4.2013 9:55:29]:
 A tím je to tedy asi jasné. Shrnutí:
 
 1. dedikovaný server nevolá v každém framu KeyboardPilot hráče
 2. v těch framech kde se nevolá se odesílá ClientOutput s prázdným subscription
 3. klient vůbec některé akce posílá jen proto, že je čte i lokálně a při té příležitosti si je subscribne
 
 Jen teď nevím, zda je problém už v 1., nebo by na straně klienta měla existovat nějaká větší setrvačnost subscribcí.


[jirka 25.4.2013 10:08:30]:
 > dedikovaný server nevolá v každém framu KeyboardPilot hráče
 
 Pokud by byl problém v tomhle, asi by to chtělo upravit podmínku v `EntitySimulateAIOptimized::operator()`, aby zohledňovala situaci na serveru:
 (
     if (checkOptimize && (vehicle==_cameraVehicle || vs->Position().Distance2(GScene->GetCamera()->Position())<Square(_minOptimizeDistance)))
       vehicle->SimulateAIRest(_deltaT,_prec);
     else
       vehicle->SimulateAIOptimized(_deltaT,_prec);
 )
 


[jirka 30.4.2013 12:42:23]:
 Mělo by být vyřešeno v revizi 104727.
 
 Na serveru je zabezpečeno, že se KeyboardPilot pro všechny hráče volá v každém framu.

[Attachment1]: https://projects.bistudio.com/dayz/basic.SampleMap.zip
[Attachment2]: https://projects.bistudio.com/dayz/DayZ-minimal.zip