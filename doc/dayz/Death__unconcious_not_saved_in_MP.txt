﻿*Obs*
When you die / get unconcious in MP, leave the game and rejoin, you are alive again at the same position.

*Exp*
You spawn (randomly) anew at the start position / area.

*Note*
For SP this works as expected.

*Repro*
1. Launch DS
2. Launch client
3. Join to game
4. Get yourself killed by zombies (or fall to death?)
5. Leave the game once unconcious(?) / dead
6. Rejoin
7. Notice you are alive again at your former death position
