﻿Marek asked me to replace them.

A few questions about this:
* Amor Serif is the only font to be used (for now) right?
* What about the Bold and Italic variants?
* Can I remove the other fonts? 
 (Zeppelin33Italic, Zeppelin33, Zeppelin32Mono, Zeppelin32, TahomaB, LucidaConsoleB, FontThin, FontNormal, FontMono, FontBold, EtelkaNarrowMediumPro, EtelkaMonospaceProBold, Bitstream)

* These are the fonts most commenly not replaced - what is the mapping? just Amor Serif (no bold or italic)?
> font = "FontMono";
> font = "FontThin";
> font = "TahomaB";

* TahomaB is also used in this specific font parameters - just go ahead and replace em too?

> class CfgInGameUI
>  class DefaultAction
>   fontNext = "TahomaB";
>  class GroupInfo
>   fontUnitID = "TahomaB";
>   fontCommand = "TahomaB";
> class RscMapControl
>  font = "TahomaB";
>  fontGrid = "TahomaB";
>  fontUnits = "TahomaB";
>  fontLevel = "TahomaB";

* Finally three more fonts just used in these definitions - just go ahead and replace em too?

> class RscDisplayGear
>  class controls
>   class CA_CustomDescription: RscStructuredText
>    class Attributes
>     font = "Zeppelin32";
> class Rsc1Button
>  font = "Zeppelin32";
> class RscXKeyShadow
>  class Attributes
>   font = "FontX";
> class CfgInGameUI
>  class DebugFont
>   font = "LucidaConsoleB";

[jirka 4.12.2012 12:44:24]:
 > LucidaConsoleB
 There would be fine to keep some nonproportional font at least for debugging purposes (performance statistics etc.).

[maruk 4.12.2012 12:55:38]:
 We have italic and bold variants already for Amor Serif.
 
 LucidaConsoleB would be good to keep as debugfont.

[qeberle 04.12.2012 16:57:59]: 
 Notes:
 * LucidaConsoleB will stay as its definition both in DebugFont  and CfgFontFamilies is in the core config as well as its fxy data
 * Aside from core config definitions all fonts are assigned via defines in "\\DZ\\ui\\hpp\\defineCommon.inc"
 * I changed all to AmorSerifPro there, but kept the former values as comment just in case
 * Made sure all font definitions across DZ are set via these defines
 * Disabled all fonts in CfgFontFamilies aside AmorSerifPro family - LucidaConsoleB and TahomaB are still defined from the core config
[qeberle 09.01.2013 08:49:02]:
 Removed these now from data as well:
 
 * Teuton23Pro
 * Teuton24Pro
 * Teuton25Pro
 * Zeppelin33
 * Zeppelin33-Italic
 
 Still unclear if these are to be removed too - see subpost:
 
 * EtelkaMonospaceProBold
 * EtelkaNarrowMediumPro

[maruk 9.1.2013 9:35:12]:
 We can use Amore Serif in any variant. So I think having bold and italic would be useful.

[qeberle 09.01.2013 09:56:13]:
 We have bold and italic for AmorSerif. AmorSerifProBold is also in use for some UI elements/menus.
 
 In terms of the LucidaConsoleB and TahomaB still defined in core config - should those be changed there or get overwritten by new ui.pbo definitions?

[qeberle 12.01.2013 05:49:07]:
 Made a new config to remove all the remaining TahomaB cases from core config:
 https://dayz-svn.bistudio.com/svn/trunk/DZ/uiFonts/CoreConfigOverwrite/config.cpp
 
 The config can be reduced once all to DZ irrelevant UI classes are removed in core config.
 
 I consider this task done, unless you have futher requests.

[Attachment1]: https://projects.bistudio.com/dayz/RemainingNonAmorSerif.txt