﻿Core controls
--

mouse - look around
LMB - raise weapon / hands, fire / attack / reload

A, S, D, W - movement
Q, E - lean
Z - prone
X - crouch
C - stand

Spacebar - interact

Tab - inventory

V - step over
R- reload
F - change weapon mode



RMB - optics, use item in hands (iron sight, optics etc. for firearms)
RMB hold - zoom / hold breath


Shift - walk / run (double click toggle)

Numbers - quick bar

F keys - gestures, mimics

 [Controls layout for E3][Attachment1]

[maruk 6.6.2013 10:40:18]:
 I commited first version of default controls, available under Dayz Game preset. It is subject of fixing weapon behaviour.

[dhall 6/06/2013 11:35:42 p.m.]:
 Overall I like them alot, only thing:
 
 I think holding shift to sprint is better because we want to reduce the time players spend using sprint - it should be a "i have to get away". Otherwise we're going to have lots of exhausted characters for players :)

[bebul 7.6.2013 11:34:54]:
 The /DAYZ/ preset is set as default, but it has colided with /Core Config/ default preset. It is now deleted in DayZ ( ui.pbo ), so the core ( CfgDefaultKeysPresets ) is still available as a good startpoint/reference, while hidden in DayZ.

[bebul 7.6.2013 16:12:07]:
 The /ALT+TAB/ should no more activate /TAB, ie. Inventory/ action now.
 (Techn: There is new, dummy user action ( UAAltTab ), which is forced to be mapped to /Alt+Tab/ by engine. Its early reading then makes the /TAB/ key unavailable for other actions)


[bebul 7.6.2013 17:03:28]:
 Fixed: Inventory dialog can be closed by TAB now
 /rev.106263/


[maruk 12.7.2013 13:40:34]:
 I am trying to find a suitable to key that we could use for raising/lowering of hands/weapon. 
 
 One option could be to follow Arma 3 and use C for it with X toggling between crouch and stand.


[maruk 16.8.2013 12:58:35]:
 Notes from discussion with Vespa:
 
 * it may be good to try to see how other games are binding key by default
 * but we do not want to give away lean keys Q, E
 * some games recently use F key for Interactions (Use key)
 * BF3 uses F for melee attack, some games use V for melee attack, some use V for cycle weapoin mode
 * mouse wheel is normally cycling weapons
 * Spacebar is usually jump key
 * Ctrl is often stance (prone) key


[maruk 16.8.2013 13:04:59]:
 [GTA 4 controls reference][http://support.rockstargames.com/entries/479487-GTA-IV-Controls-for-Keyboard-and-Mouse]

[Attachment1]: https://projects.bistudio.com/dayz/DayZ-ovladani_06_2013_297x210.pdf