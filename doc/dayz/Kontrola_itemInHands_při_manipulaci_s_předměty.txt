﻿Zachytil jsem jednu situaci, kdy po odebrání předmětu tento předmět zůstane v rukou.

Repro:
- mise, hráč má bundu, batoh (nebo jiné dva kontejnery) a pistoli v bundě
- odložíme batoh na zem
- vezmeme pistoli do ruky
- přesuneme pistoli z bundy do batohu
- chyba: pistole zůstává v ruce

Jednodušší situace už jsem ošetřil.

Možná řešení (zatím si nejsem jist které zvolit):
- průběžně (v rámci simulace apod.) kontrolujeme, zda předmět v ruce je stále i v inventáři
- při přesouvání předmětu provádíme i složitou kontrolu toho, zda postava nepřestala předmět v ruce vlastnit
- neumožníme vzít do ruky předmět z kontejneru (jen ze slotů)
- neumožníme v obrazovce inventáře manipulaci s předmětem v ruce