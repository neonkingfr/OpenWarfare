﻿Currently I must define proxies for inventorySlots to very specific items, such as:

(
class CfgNonAIVehicles
...
	class ProxyMagazine_STANAG30 : ProxyAttachment
	{
		scope = public;
		inventorySlot = magazineM4A1; // magazine + muzzle name
		model = "\dz\weapons\attachments\magazine_STANAG30.p3d";
	};
...
}
)

Whereas I believe it would be more practical to define the inventory slots in general terms, not at the proxy level, an example:

(
class CfgNonAIVehicles
...
	class ProxyMagazine : ProxyAttachment
	{
		scope = public;
		inventorySlot = magazine; // generic slot name
		model = "\dz\weapons\attachments\magazine.p3d";
	};
};
)
Note: The slot is generalized from muzzle specific to general. Without this, I would need to create a new proxy definition for every weapon - even though all weapons using STANAG magazines will use the same model. The allowed magazines for that would then be stored in the magazines[] attribute.

====Conditions on cfgVehicles reference
This could then be combined with a condition config value on the  that would function in the same was as the conditions value in userActions to allow checks to be performed that restrict certain combinations of attachments, for example:
(
class cfgVehicles
{
...
	class Attachment_Bayonet_M9A1 : AttachmentBayonet
	{
		scope = public;
		name = "M9A1 Bayonet";
		inventorySlot = weaponBayonet; // custom attachment / inventory slot name
		model = "\dz\weapons\attachments\bayonet_m9a1.p3d";
		condition = "isNull(_parent itemInSlot 'weaponSuppressor')";
	};
...
}
)
For standardization it would be great to align available variables with the userAction system:
[Re: Inventory - crafting](https://projects.bistudio.com/dayz/Re_Inventory-crafting.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2553)
The only variable I can think of that really needs to be passed is:
* **_owner** ; the agent executing the attempt to attach the inventory item
* **_this** ; the attachment itself
* **_parent** ; or something similar, which is the item attempt to receive the attachment


[jirka 11.7.2013 7:17:49]:
 > Whereas I believe it would be more practical to define the inventory slots in general terms
 
 The main problem is the slot name has two purposes:
 - to match proxy with the attachment
 - to check what item can be used in the attachment slot
 
 If we will change the slot name to "magazine", we will need to define all magazines with the ( inventorySlot = "magazine" ) definition and all magazines will fit it. To enable it, we can simply add a possibility to enforce different slot name instead of the current ("magazine" + muzzle name).
 
 I can add support for a new muzzle definition value "magazineSlot". When this will be defined and not empty, the name of the slot will force to it, otherwise the original slot name will be used. This will enable to share magazines among different weapons (muzzles).

[jirka 11.7.2013 10:19:26]:
 > I can add support for a new muzzle definition value „magazineSlot“.
 
 Added in rev. 107642.

[dhall 11/07/2013 3:08:50 p.m.]:
 M4 has been configured with two attachments to have a "condition" setting which can be found easily in the M4A1 sample mission.
 
 With the condition working, the silencer and the bayonet would not be able to be used.

[dhall 11/07/2013 3:09:15 p.m.]:
 magazineSlot confirmed working

[jirka 12.7.2013 13:26:15]:
 > Conditions on cfgVehicles reference
 
 Implemented (rev. 107739) as requested with two exceptions:
 - the value name is `attachmentCondition`, simple `condition` sounds too generic in this context by my opinion
 - ( _owner ) is not passed to the expression, only ( _this ) and ( _parent )
   + we can handle the attachments even when item is on ground, so when no owner is defined
   + to propagate person to the context where expression is evaluated would be too arduous

[dhall 12/07/2013 1:40:52 p.m.]:
 Testing results:
 * Correctly prevents attachment via drag and drop onto attachment slot.
 * However, dragging onto the weapon and selecting context menu "attach to ..." still allows attachment.

[jirka 15.7.2013 15:01:16]:
 > dragging onto the weapon and selecting context menu „attach to …“ still allows attachment
 
 Fixed in rev. 107805. Please, confirm.

[dhall 15/07/2013 3:05:57 p.m.]:
 Confirmed fixed, thanks!
 
 Nothing further related to this ticket is left to do.
