﻿Currently, build pipeline is prepared and seems to be working, however distribution and update is not solved yet (it is possible to download and use build results as artifacts, but this is time consuming and user unfriendly).

Using [Build server](http://of.bistudio.com:8080/jenkins/job/DayZ%20Data/build?delay=0sec), there is possible to create addons:
- Core -> dta/core.pbo
- languagecore -> dta/languagecore.pbo
- product -> dta/product.bin
- DZ/worlds/test/Data -> Addons/sampleMap_data.pbo
- DZ/worlds/test/objects -> Addons/sampleMap_objects.pbo
- DZ/worlds/test/terrain -> Addons/sampleMap_terrain.pbo
- DZ/characters -> Addons/characters.pbo

I found some problems when using the data:
- ( Warning Message: Addon 'SampleMap' requires addon 'CAData' )
  + unwanted dependence, however the map seems to work
- no character available in the mission editor
  + probably still work in progress
- folder DZ/data
  + we can create a new addon with prefix DZ/data (it is possible until files at DZ root are not present), but this was not discussed yet
- messages about missing strings in the localization database
  + seems that strings introduced in HeliSim to program and core configs are not in the languagecore.pbo, but in some other TOH addon
- ( Warning Message: Cannot load texture hsim\data_h\data\noise_raw.paa. )
  + probably core SimulWeather configuration is referencing texture outside the core data
- ( Error: Model core\default\default.p3d cannot be used as a sprite - it has not just 4 vertices )
- ( Warning Message: Cannot load texture ca\data\data\halfa.paa. )
  

[jirka 30.8.2012 10:14:45]:
 > - ( File core\data\ticonversion.tga not in bank )
 >   + the packing pipeline does not handle directcopy.lst yet (should it, or would I solve such exceptions in the project file?)
 
 The file ( directcopy.lst ) is handled by the packing process now.
