﻿Currently, the *default* distribution of DayZ on Steam is set to a build from 15th October.

Maybe we can switch it to some more usable version.

Also, we will probably need some politics what build will be declared as "stable" and who will be responsible for setting it live on Steam.
[maruk 20.12.2012 14:48:43]:
 Please set live current version. Re: policy you are right but I think ideally should be something more formal. I also should try how to do it myself  as I am not sure and never did it before.

[jirka 20.12.2012 15:24:16]:
 Latest version of both DayZ and DayZ Server set live as default.
