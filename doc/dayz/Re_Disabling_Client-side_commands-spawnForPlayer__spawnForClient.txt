﻿> It would be good to disable some client-side commands when the game is executed in MP.


Both variants of spawnFor... were allowed on the server only, but the check was only client side so far. Server side check added in 113973.

Without the server side check a modified client was able to spawn a code on the server, possibly installing a publicVariable event handler with it, and using this handler other clients could execute their code on the server as well. This is a likely cause of [this exploit](https://jira.bistudio.com/browse/DAYZ-2418).

[ondra 6.1.2014 17:29:28]:
 Fix was wrong. Fix fixed in 114008
 
 The vulnerability never existed. The server was the only possible origin of the spawnFor.. message, the message received from other clients was ignored.
 
