﻿===Build
0.14.103046

===Problem
When saving on dedicated server dayz mission the database syncronization does not save any *clothing or equipment* on the character, however it successfully saves position and weapon.

===Notes
- Not tested in non-dedicated server.
- Verified the command is being written in the RPT file.

===Reproduction
I will attempt to reproduce on a cutdown version tomorrow, but the best repro is done using the dayz server itself.

1. Join DayZ server
2. Find some loot or dev console spawn equipment
  (
	myItem = player createInInventory "Hoodie_Black";
	myItem = player createInInventory "AK_74";
  )
3. Move to new position
4. Logout
5. Rejoin server
6. verify new position loaded, verify weapon loaded, verify equipment not loaded

I will work on an easy repro tomorrow morning once I am back from foreign police

[dhall 14/03/2013 12:17:55 a.m.]:
 From what i can tell, it appears that the saving may be working but the loading of the inventory is not. So if it saves successfully in singleplayer, it does not successfully load in multiplayer.
 
 It will load the weapon + position, but not anything else.

[jirka 14.3.2013 8:21:19]:
 Because this seems to be a server issue, I will need a repro where local dedicated server is involved (to trace it).
 
 I will try it with the full DayZ mission first, but I'm afraid this could be quite time consuming.

[jirka 14.3.2013 9:07:49]:
 I think I found where the problem is. The items are created correctly on the server, but are not created as network objects and transferred to clients (outdated implementation of Person::CreateSubobjects). Working on fix.

[jirka 14.3.2013 10:42:01]:
 Should be fixed in revision 103064. Please, test it.

[dhall 14/03/2013 4:17:47 p.m.]:
 Matt tested this with 8 players, on server will full loot and zombies and it worked flawlessly. Thank you!
