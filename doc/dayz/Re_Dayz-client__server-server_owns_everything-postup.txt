﻿Branch https://dev.bistudio.com/svn/pgm/DayZ/branches/clientServer
 
Průzkum, kde začít:
 
Vlastnictví hráče se přesouvá z  iniciativy serveru ve `World::UpdateTeamMembers()` voláním `SelectPlayer`.
 
Myslím, že SelectPlayer ještě nechám, ale bude dělat něco úplně jiného, než změnu vlastnictív.

V současné implementaci server v reakci na `case NMTSelectPlayer:` udělá `ChangeOwner` pro osobu a mozek. Vlastnictví grupy a jejích členů už se naštěstí nikde nepřesouvá (takové věci už nemáme), takže to řešit nemusím.
[ondra 06/12/2012 14:53:29]:
 Postavu i mozek hráče už mi vlastní server. Časem nejspíš ChangeOwner zakážu úplně, zatím jsem to jen na příslušných místech vyřadil.
 
 Teď jdu na další krok: parallelně s dosavadním `_clientInfo`, které přenášelo na server pozici kamery na klientu, vytvořím `ClientInput` vlastněný klientem, které bude přenášet stav vstupů a `ClientOutput`, vlastněný serverem, kterým bude server přenášet jednotlivým klientům informace, které se týkají pouze jich (tam by se časem měly ocitnout i dialogy a podobné UI věci). V `ClientOutput` také budou informace, kterými server řídí chování `ClientInput`, například jaké akce server zajímají. (Možná to raději bych to oboje sloučil do jedné třídy, protože mnoho funkcí bude potřebovat přístup na oboje současně, ale uvidím, jestli to s ohledem na konstrukci `TransferMsg` bude vhodné).

[ondra 7.12.2012 12:08:59]:
 V `Soldier::SimulateAI` dělám následující změny (bude třeba analogicky implementovat i ve vozidlech a ptácích):
 
 (   if( QIsManual() && IsLocal()) )
 místo dosavadního 
 (   if( QIsManual() )
 
 (
        else if (QIsManual()) FakePilot(deltaT);
        else if (GetRemotePlayer()!=1) KeyboardPilot(deltaT,prec);
 )
 před 
 (      else if (!IsLocal()) FakePilot(deltaT); )
 
 Tím jsem teď ve stavu, že KeyboardPilot se volá na serveru. NetworkManager musí vstup přesměrovává na ClientInput z příslušného klienta. (Prozatím přesměrovávám jen Forward/Backward). Vstupy se zatím ještě nepřenáší, tomu se budu (byť zatím jen v jednoduché verzi) věnovat teď.

[ondra 12/12/2012 16:04:30]:
 Založil jsem ClientOutput objekt a pak jsem se zarazil, Potřebuji, aby tenhle objekt byl nějak propojený s ClientInput a taky potřebuji, aby se synchronizoval ze serveru na klienta. Budu se o tom potřebovat poradit s Jirkou.

[ondra 13/12/2012 14:10:05]:
 ClientOutput i ClientInput jsou nyní zapojeny velmi jednoduše, jako NetworkSimpleObject, s výslovných zacházením kde je třeba.
 
 Už mám postavu, jejíž běh řídí plně server. Střílet, lehat si a dělat spoustu dalších věcí (vč. menu akcí) nejde zatím vůbec.
 
 Kromě toho postava trpí prodlevou nečekaně velkou vzhledem k tomu, že klient i server jsou na jednou počítači.

[ondra 18/12/2012 12:24:43]:
 ClientOutput už přenáší základní informace, kterými server říká, o jaké akce se zajímá, a dostává jen ty. Dále už funguje i lehání / stoupání (tj. GetActionToDo). Způsob synchronizace stavu budu ještě chtít trochu přepracovat, aby se přenášely jen rozdíly. Další na řadě by mělo být míření, rozhlížení a střelba.

[ondra 15/01/2013 13:53:02]:
 Udělal jsem první velký zpětný merge, takže technologie je volitelně přítomna v trunku (100844). Kdo chce klient-server testovat, musí na straně serveru použít command line parametr -central.
[Attachment1]: https://projects.bistudio.com/dayz/basic.SampleMap.zip
[Attachment2]: https://projects.bistudio.com/dayz/DayZ-minimal.zip