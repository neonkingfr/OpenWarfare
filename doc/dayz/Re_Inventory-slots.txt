﻿Sloty jsou místa na lidech, kde budou umístěny právě používané inventory itemy. Právě tyto itemy budou jediné, které se budou vizualizovat.

Na základě příspěvku [Player Clothing](https://projects.bistudio.com/dayz/Player_Clothing.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1930) se pokusím sumarizovat, jaké sloty budeme mít a co v nich bude obsaženo.

- Eyewear ... brýle, noční vidění
- Headgear ... helma, čepice
- Body ... bunda apod.
- Legs ... kalhoty
- Feet ... boty
- Vest ... (taktická) vesta
- Gloves ... rukavice
- Back ... batoh
- Shoulder ... zbraň přes rameno

Krom toho bude jeden (aktuálně používaný) předmět v rukou. Tento předmět ale (kvůli problémům popsaným v diskusi) zůstane i v původním slotu nebo v inventáři.

Implementace bude znamenat dočasné omezení funkčnosti (než budou vytvořeny a zkonfigurovány příslušné předměty, případně upraven systém animací).

Prosil bych o kontrolu, zda uvedené sloty prozatím stačí (přidat později další by neměl být problém) a o posouzení, zda uvedená omezení nejsou natolik závažná, že bych měl postupovat pomaleji, ale více zpětně kompatibilně.

Postup (vše v trunku):
- zavedeny sloty na člověku
- aktuální zbraň realizována jako předmět v rukou (( _itemInHands ))
 + v rukou je nyní vizualizována jen aktuální zbraň - to vede k tomu, že zbraň není vidět za některých situací, kdy dosud vidět byla - spraví se to až úpravou grafu animací
- program a core config jsou připraveny na nový systém proxy (definované sloty)
 + nová simulační třída ( proxyInventory ), realizující sloty a předmět v rukou
 + vykreslování nových proxy (předmět ve slotu se vykresluje jen tehdy, když zároveň není v rukou)
 + v core configu jsou připraveny základní třídy pro proxy jednotlivých slotů: *ProxyHands*, *ProxyEyewear* , *ProxyHeadgear*, *ProxyBody*, *ProxyLegs*, *ProxyFeet*, *ProxyVest*, *ProxyGloves*, *ProxyBack*, *ProxyShoulder*
- batoh je nyní v inventáři, obsahuje jej slot *Back*
 + batoh se už vykresluje přes nový systém proxy - v configu *Proxyus_bag* dědí z *ProxyBack*
- máme nakonfigurované proxy
- zavedena konfiguraci předmětů, definující jejich příslušnost do slotů a parametry pro UI inventáře
 + inHands ... zda předmět smí být držen v rukou
 + inventorySlot ... zda a do kterého slotu je možné předmět umístit
 + itemSize ... rozměry předmětu v UI inventáře
- vytvořen prototyp postavy skládané z proxy s definicí proxy dle slotů a nakonfigurované předměty (oblečení do jednotlivých slotů)
- používané jsou i zbylé sloty
- zrušeno samostatné cargo pro batohy, batohy sdílejí společný prostor s ostatními předměty

[maruk 21.12.2012 15:42:16]:
 Bavili jsme se o užitečném řesení, kdy by existoval slot dvouruční předmět na zádech, jednoruční předmět napravoboku a jednoruční předmět na levoboku, které by se rovněž vizualizovaly a zároveň by nikdy nebylo možné mít předmět danéo typu jak v rukou, tak v tomto slotu.

[jirka 21.12.2012 17:52:13]:
 Jen upřesním, že se jedná o nejlepší nalezené řešení problému, kdy mám nějakou věc v rukou a chci vzít do rukou něco jiného (kam s tou původní?).

[jirka 2.1.2013 14:24:39]:
 Po diskusi s Ondrou bych upřednostnil mírně modifikované řešení:
 - zůstává myšlenka nových slotů na těle pro zbraně (pro ty předměty, které chceme mít viditelné, ostatní jsou v inventáři člověka)
 - ruce nejsou plnohodnotný slot - předmět v rukou nadále zůstává v inventáři (nebo v nějakém jiném slotu)
 - shortcuty pro vzetí předmětu do ruky se nyní dají vytvořit pro libovolný předmět v inventáři, pokud by to působilo zmatečně, můžeme je později povolit jen pro předměty ve slotech, případně zapojit nějaké prohazování
