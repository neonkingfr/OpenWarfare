﻿Some containers shouldnt be accessable via quickbar, such as your backpack. You should have to stop and transfer a backpack item to your clothing before it can be quickbar used.

Could we have a config flag that declares if a container can host a quickbar item? And then have containers with this config flag removed from quickbar calculation?

[qeberle 13.03.2013 07:40:18]:
 Sounds annoying to me to have to transfer items back and forth between containers to be able to use them.
 
 Instead it should just take longer to use/get them from the backpack.
 
 NTH: Have an anim played depending on which container the item is in when used/taken out.

[dhall 13/03/2013 8:26:07 a.m.]:
 That's a really good idea. If it is using an item from the backpack, it could just stop, play some kind of longer animation :)
