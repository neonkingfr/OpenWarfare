﻿Currently, the scripted spawning system I created for DayZ functions poorly. This proposal seeks to improve on this, both in performance, gameplay, and from a pipeline perspective. The proposal is to include spawn locations inside the P3D model's for buildings, and then define the spawns inside the config.

Placing spawn containers
===
* Spawn containers are placed in proxies inside the building P3D:
 - weapon container proxy (enlongated box)
 - equipment container proxy (square, small)
 - zombie container proxy (man sized)
* The containers are orientated and positioned to suit placement
 - For example on a shelf, or on a table
 - Placed on a per item basis
* These containers are defined in config:
 ( 
class cfgVehicles
{
	class MyHouse: Strategic
	{
		//...//

		class spawn
		{
			class default
			{
				spawnClass = "residential";	//class of spawn (cfgSpawn)
				persistent = false;			//will the loot remain after time expiry
				probability = 0.01;			//probability that something will spawn *per location*
				locations[] = {};			//name of dummy proxy that is location for spawn
			};
			class cupboard1: default
			{	
				locations[] = {"weaponsProxy1","smallProxy1"};
			};
			class room: default
			{
				locations[] = {"zombiespawn1","zombiespawn2","zombiespawn3"};
			};
		};
	};
};
 )

How the items spawn
===
* When a player is near a building, the building is then initialized for its spawned items.
* Each location is assessed on the probability value
* If spawn is to occur, then the engine reads the container type, and looks up cfgSpawn reference:
 (
class cfgSpawn
{
	class residential
	{
		weapons[] = {
			{"ClassName",0.01},
			{"AnotherClassName",0.01}
		};
		magazines[] = {};
		items[] = {};
		zombies[] = {};
	};
};
 )
* Upon reading the container type the engine will select a spawn item based on the weighted probability from the config entry
* If the items are not persistent (persistent == false) then after a period of time, or when no humans are around, they will despawn

Issues
===
* Can the engine assess what the proxy in the model is?
 - The engine will need to check for "zombieDummy.p3d" proxy and know to spawn zombies in that spawn container
* Should these spawn containers be placed inside the memory LOD? or another LOD?
* Is there a performance issue associated with two probability checks (one if to spawn in location, another to perform weighted random selection from array)?
* Can we do this serverside and still keep performance (hundreds, maybe thousands of objects)? 
 - If so, this would be a cruitial win against hacking efforts, items will only be able to be spawned by the server

[dhall 24/10/2012 5:56:23 p.m.]:
 I moved this here, as I had not seen it (I was in wrong colabo view!)

[jirka 24.10.2012 18:07:31]:
 > Can we do this serverside and still keep performance (hundreds, maybe thousands of objects)?
 
 I hope we can if we will do it clever (spawn only what is necessary).

[dhall 24/10/2012 6:09:53 p.m.]:
 Using this building spawn method, combined with a cleanup method, should be able to drastically reduce the amount of spawning occuring.
 
 Same with zombies, if they can be spawned in buildings using a spawn container, it means I do not have to spawn them until the player is inside the building. This will reduce the number of them having to be spawned all the time.

[jirka 25.10.2012 7:45:40]:
 My idea was something like this:
 - the server will have a "database" of all spawned objects
 - the world will be divided to zones (zone can be a single building, a village etc.)
 - each zone could be in a state:
   + real - objects are really spawned
   + potential - object are only in the database
 - zones are switching between actual and potential state based on the presence of players near zone
 
 I hope this approach will enable us to:
 - keep number of really spawned objects low
 - keep some consistency (until we decide to re-spawn some object, their state will be the same during the whole server existence)
 
 Maybe, the database could be somehow persistent to survive restart of servers. If we would like to keep the current independency of players on servers (not sure about it), the database could be even stored on the central server.
