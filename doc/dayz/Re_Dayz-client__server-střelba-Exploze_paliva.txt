﻿> Dívám se, kudy na to, a zdá se mi, že ta správná informace by mohla být zhruba to, co teď dostává funkce `Landscape::ExplosionDamage`.

Většina už je snad hotová, problematické je zpracování explozí vozidel.

- zdá se, že v konfigu třída `FuelExplosion` neexistuje (třeba ověřit), dostávám assert `smokes.cpp(4657) : No explosion type`
- v tomto případě nestačí přenést typ, protože parametry hit apod. se počítají dynamicky (viz `DestructionEffects::SimulateExplosion`)

Nejspíše by to stejně mělo fungovat tak, že se exploze bude simulovat na serveru. takže by se nic ani přenášet nemělo.

Možná bude stačit jen opravit existenci té třídy. Místo vracení do konfigu by možná bylo lepší udělat to jako novou #fuelexplosion "nativní" třídu.

V Arrowheadu byla třída v `ca/Weapons` a vypadala takhle:

(
	class FuelExplosion: Default
	{
		hit=100;
		indirectHit=80;
		indirectHitRange=2.5;
		model="";
		simulation="";
		cost=1;
		soundHit[]=
		{
			"Ca\sounds\Weapons\explosions\explosion_small_11",
			6.3095737,
			1,
			1600
		};
		soundFly[]={"",1,1};
		soundEngine[]={"",1,4};
		explosionEffects="VehicleExplosionEffects";
	};
)

Odkaz na `VehicleExplosionEffects` je na nějakou další třídu, která popisuje particlové efekty exploze, tu jsem zatím nedohledával.

[dhall 9/08/2013 11:05:26 a.m.]:
 Now added too *DZ/weapons/misc*

[ondra 13/08/2013 13:03:32]:
 S přidanou třídou se zdá, že funguje dobře.
