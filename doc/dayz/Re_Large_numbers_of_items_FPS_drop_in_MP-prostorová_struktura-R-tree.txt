﻿> Budu se nyní věnovat r-tree

Hledám nějakou pěknou C++ implementaci s rozumnou licencí. Zkouším:

http://www.ics.uci.edu/~salsubai/rstartree.html (nelíbí se mi, málo používá funktory)
https://github.com/nushoin/RTree

http://www.virtualroadside.com/blog/index.php/2008/10/04/r-tree-implementation-for-cpp/

http://www.virtualroadside.com/software/#RStarTree

[ondra 03/12/2013 10:51:07]:
 Zatím používám implementaci z http://superliminal.com/sources/RTreeTemplate.zip, a to hlavně proto, že se mi zdála nejjednodušší.
 
 Při jejím testování jsem narážel na narušení seznamu `_updating`, nakonec se při porovnání chování vůči referenční naivní implementaci, ukázalo, že jedna položka může být navštívena vícekrát, což jsem nečekal. Zatím si nejsem jist, jestli je to chyba nebo rys, ani co s tím budu dělat.
 

[ondra 03/12/2013 13:24:40]:
 > Zatím si nejsem jist, jestli je to chyba nebo rys, ani co s tím budu dělat.
 
 Byla to moje chyba, při odstraňování jsem použil špatné souřadnice. Opraveno. Teď už r-tree vypadá plně funční. Uvidíme, jaké ukáže při testování zlepšení výkonu.
 

[ondra 6.1.2014 16:40:25]:
 Mimochodem, stejnou R-Tree základní implementaci, jako používám, použili už předtím pro VBS v `_VBS3_LARGE_OBJECTS`/`_VBS3_LARGE_OBJECTS_WRP`. (To taky představuje potenciální konflikt, pokud by tuto technologii v enginu vycházejícím z Dayz povolil, protože já jsem verzi mírně upravil. Asi se to neprojeví, protože to instanciuji s jinými typy, ale vyřešit by se to nejspíš mělo, aby se na to někdo někdy nenapálil).
 Detaily v jejich implementace se mi ale nelíbí: zdroják sice dali do extern, nicméně se nerozpakovali dovnitř přidat friend, aby se dostali na vnitřnosti:
 (
#if _VBS3_LARGE_OBJECTS_RTREE
  friend class Landscape;
#endif
)
 Implementace `SearchStaticRTree` / `SearchDynamicRTree` na to potom navazuje: místo co by použili nějakou funkci interfacu, iterují přes R-Tree sami s použitím znalosti vnitřností.


[ondra 6.1.2014 21:00:54]:
 Přepnul jsem implementaci na nově importované RTree a pročistil ji. Neměl jsem to jak otestovat, ale zdálo se mi to docela přímočaré, tak to snad bude fungovat, pokud to někdo někdy zapne.
 
