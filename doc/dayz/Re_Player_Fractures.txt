﻿> Configed parts could be be assigned some engine dependent action, i.e. fracutred legs could be assigned to the legs effect, so when fractured they activate the standand "can't stand up" mechanic.

I think there already is some system of custom hitpoints in the engine, however it only defines visuals.

Maybe, simple extension would be added to define how damage of hitpoints affects some predefined abilities.

For example:
(
class HitPoints
{
  class HitLeftLeg 
  {
    armor=...;
    material=...;
    name=...;
    visual=...;
    passThrough=...;
    class Inflictions
    {
      canStand = -0.8;
      canAim = 0;
      canTalk = 0;
      canCarry = -0.2;
    };
  };
  ...
};
)
will mean that if left leg is half damaged, the ability of stand will be decreased by 0.4, the man will be able to carry 10% less and ability to aim or talk will not be affected.
We can then react to the final value (sum over all hitpoints) of canStand in the engine in different ways, for example:
a) do not allow to stand up when value is over some limit
b) allow to stand only for a limited amount of time, based of the value