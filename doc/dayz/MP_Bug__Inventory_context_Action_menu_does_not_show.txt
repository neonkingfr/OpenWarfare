﻿===Build
* 0.22.109903
* MP Dedicated only

===Problem
Right click context menu does not show up. Instead only the words "receiving..." are displayed.

===Repro
1. Load mission [m4a1.SampleMap.pbo][Attachment1] on a dedicated server
2. Connect as a client
3. Right click on the magazine, note that "receiving..." text displays
4. Expected behavior would be "strip magazine" action

This occurs for all context menu items, such as taking an attachment off a weapon, etc...

[jirka 9.9.2013 14:31:28]:
 Should be fixed in rev. 109919. Please, re-test.


[dhall 10/09/2013 10:20:48 a.m.]:
 Confirmed fixed, tested on MP dedicated server with two clients

[Attachment1]: https://projects.bistudio.com/dayz/m4a1.SampleMap.pbo