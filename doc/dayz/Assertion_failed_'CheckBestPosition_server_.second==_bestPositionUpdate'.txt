﻿There are thousands of asserts
`Assertion failed 'CheckBestPosition(server).second==_bestPositionUpdate'`

*Repro steps:*
1. use [magazineTest.SampleMap.zip][Attachment1] in your ( DayZ\mpmissions )
2. start dedicated server from Visual Studio (mine params are ( -nofptraps -server ) )
3. start client, connect to it on LAN and run the magazineTest mission
4. exit the mission + reconnect + start this mission again
5. you are supposed to get the assert


[bebul 5.11.2013 14:53:02]:
 More simple repro:
  1. run the dtto mission on dedicated server
  2. connect with client and drop the empty magazine to the ground.
  3. observe the assert



[Attachment1]: https://projects.bistudio.com/dayz/magazineTest.SampleMap.zip