﻿> I need a command to find the *clientID* (I don't think *owner* works any more), then I need the *statusChat* command to be created to allow this assignment. For example:

owner should still work. If you have a player person, owner of it is the ID of the remote client.

This was mentioned in this (not very clear or explicit) quote:

[Re: možnost spustit na klientu nějaký kód - spawnForPlayer](https://projects.bistudio.com/dayz/Re_možnost_spustit_na_klientu_nějaký_kód-spawnForPlayer.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2619)

> Z id postavu udělat umíme (player x), z postavy id asi taky (owner body). 


[dhall 20/06/2013 10:17:58 a.m.]:
 That's awesome :) Thank you for highlighting that.



[Attachment1]: https://projects.bistudio.com/dayz/dayz_gui_text.jpg
[Attachment2]: https://projects.bistudio.com/dayz/dayz_gui_proposal.jpg
[Attachment3]: https://projects.bistudio.com/dayz/description.ext