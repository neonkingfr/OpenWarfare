﻿> Celá věc by se hrozně zjednodušila, kdybychom vše posílali garantovaně.

Začal jsem s implementací, pro první pokusy budu pro updaty používat výhradně garantované zprávy.

[ondra 20.1.2014 15:20:11]:
 Tohle už na straně serveru mám (zatím aktivováno pouze pro sebe, ale jde snadno přepnout, viz `_updatesGuaranteed`), v případě garantovaných zpráv už nyní nepoužívám `AddPendingMessage`, ale rovnou `OnSendComplete`, takže už bych měl být schopen začít dělat diference proti `lastSentMsg`.

[ondra 20.1.2014 15:41:43]:
 TODO:
 
 - `NetworkServer::UpdateObject` zatím posílá negarantovaně
 - `NetworkServer::PrepareNextUpdate` zatím posílá negarantovaně (viz `NMFNone`) 

[ondra 21/01/2014 14:56:04]:
 > NetworkServer::UpdateObject zatím posílá negarantovaně
 > NetworkServer::PrepareNextUpdate zatím posílá negarantovaně (viz NMFNone)
 
 Omyl. Tyhle dvě funkce to jen zařadily do `_messageQueueNonGuaranteed`, odtam se to pak poslalo garantovaně.
 

[ondra 22/01/2014 16:01:35]:
 Server posílá klientovi v rámci `PlayerMessage` (`PLAYER_MSG`) informaci o tom, zda používá garantované (a tedy i diferenciální) updaty.
 
 Už mi funguje klonování odesílaných zpráv na serveru, takže jsem schopen diference dělat, ale ještě je nedělám, nezprovoznil jsem ani nulování shodných polí, protože mi došlo, že tím způsobem nejsem schopen přenést změnu z jiné hodnoty na nulu.
 
 Nyní budu při diferencování vytvářet bitové pole, které bude říkat, které položky se přenášejí, to pole pak přenesu na začátku každé zprávy, prozatím nějak naivně, později bych chtěl i pro tohle pole, u kterého očekávám, že typicky bude velmi řídké, zvolit nějakou kompresi.
 
 Abych to ale udělal, budu muset nějakou informaci propagovat skrz enkódování / dekódování zprávy, a toho se trochu bojím, bude to sice jednoduchá změna, ale na mnoha podobných místech najednou. (Na druhou stranu je možné, že když to udělám pěkně, dosavadní replikování zprávy kvůli počítání diferencí ztratí smysl.)
 

[ondra 22/01/2014 16:04:51]:
 >  ale na mnoha podobných místech najednou
 
 Jak se na to tak dívám, ani jich snad tolik nebude, jedná se o:
 
 `NetworkComponent::EncodeMsg`
 `NetworkComponent::EncodeMsgItem`
 
 `NetworkComponent::DecodeMsgItem`
 `NetworkComponent::DecodeMsg`
 
 Každá z nich obsahuje switch s maximálně desítkami položek.
 

[ondra 22/01/2014 16:39:10]:
 Vypadá to, že vyynechávání polí asi dokážu v `NetworkComponent::DecodeMsg` udělat snadno, ale vlastní diferencování těžko. Abych totiž věděl, vůči čemu diferencovat, musím nejprve aspoň část zprávy dekódovat (aspoň vědět, jaký update to je a k jakému objektu).
 
 K tomu, jestli vynechávat, nebo ne, mi stačí podle typu rozlišit, jestli se jedná o update. Pokud bude rozlišovací funkce stejná na serveru i na klientu, bude to v pořádku. Vynechanou informaci pak budu muset do zprávy vložit vně, až po jejím dekódování. Stejně pak půjde udělat diferencování: během dekódování se dělat nebude, až potom.
 

[ondra 24.1.2014 12:43:03]:
 V rev. 114611 si může kterýkoliv server povolit garantovaný provoz pro updaty hodnotou `guaranteedUpdates` v `server.cfg`. Diferencování ještě není, ale třeba ho ještě dnes stihnu. Bez diferencování to nebude mít žádné viditelné přínosy, nicméně i tak může být užitečné začít to testovat, abychom viděli, jestli je nějaký vitelný negativní dopad na odezvu.


[ondra 27.2.2014 12:47:38]:
 Přenos difovaných zpráv je zveřejněn v rev. 115814 a měl by fungovat každému, kdo si na serveru do server.cfg přidá řádku
 
 `guaranteedUpdates=1`
 
 Ještě tam vidím nějaké věci k dodělání a vylepšení (sepíšu jinam), ale to hlavní už by mělo být funkční a bylo by na čase začít to testovat.
 
 Pro ověření komprese zatím využívám faktu, že statistikách zpráv, co se aktivují přes Alt-;, se při odesílání se měří velikost původní zprávy a při příjmu té nové. Když si tedy otevřu odchozí statistiku na hostiteli a příchozí na klientu, vidím rozdíl ve velikosti zpráv. Na dedikovaném serveru by snad měl být přínos měřitelný i běžnými síťovými utilitami, očekávám snížení přenosů zhruba na polovinu.
