﻿
>  Doplňuji `diffed` do `NetworkFormatSpecs`, takže server klientu spolu s formátem řekne, co je difované.
> 
> [ondra 21.2.2014 17:26:12]:
>  https://dev.bistudio.com/svn/pgm/DayZ/branches/DiffTransfer

Teď mi nastává chyba v `DecodeMsg` volané z `DecodeMsg`, tedy při dekódování podobjektu. Zdá se mi přitom zatím v debuggeru, jako by se příslušné `EncodeMsg` pro podobjekt ani nevolalo, a to v důsledku toho, že `format->IsDiff(i, diff)` pro něj vrací false.

[ondra 26.2.2014 14:52:51]:
 Chyba byla přímo v `NetworkMessageFormat::IsDiff` - když formát říká, že položka není difovaná, musí vracet true, jako že se pořád liší, jinak se vůbec nepřenáší, což je zjevný nesmysl.
 

[ondra 26.2.2014 16:10:48]:
 Dál jsem měl banální chybu v `IsDiffByFormat`, kdy pro `ET_HEADER` jsem vracel také true, takže i tahle pole se při odesílání difovala, což je nemysl.
 
 Po opravě už jsem se dostal trochu dál, chybu dostávám až při dekódování položky `nvg`, což je první položka za `NDTRef flagCarrier`.
 

[ondra 26.2.2014 16:34:55]:
 Nyné je vidět, že targetSide (offset 30) se v `EncodeMsg` vynechalo, ale v `DecodeMsg` ne:
 
 DecodeMsg:
 
 (
  item objectCreator, raw offset 16
  item objectId, raw offset 17
  item objectPosition, raw offset 25
  item guaranteed, raw offset 29
  item targetSide, raw offset 30
  item animations, raw offset 31
  item pilotLight, raw offset 32
 ...
 )
 
 EncodeMsg:
 
 (
 item objectCreator, raw offset 16
 item objectId, raw offset 17
 item objectPosition, raw offset 25
 item guaranteed, raw offset 29
 item targetSide, raw offset 30
 item animations, raw offset 30
 item pilotLight, raw offset 31
 item collisionLight, raw offset 32
 item supply, raw offset 33
 ...
 )
 

[ondra 26.2.2014 16:46:52]:
 .. a to bylo tím, že jsem zapomněl přestěhovat implementaci `IsDiff` z `NetworkMessageFormat` do `NetworkMessageFormatBase`, takže klient informaci sice měl, ale ignoroval.

[ondra 26.2.2014 16:55:51]:
 Po opravě už mi zprávy procházejí bez problémů, takže jsem se snad přehoupl a budu pokračovat už normálně (znovu budu diff používat jako default a udělám merge do trunku).
 
