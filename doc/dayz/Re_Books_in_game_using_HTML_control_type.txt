﻿I have added some books to the game and configured the look of the books in game based on this post:
 https://projects.bistudio.com/dayz/Books_in_game_using_HTML_control_type.txt


The books that i added are public domain books usually from the project Guttenberg.

List of all books:
Short stories
- ItemBookTheMetamorphosis
- ItemBookThePitAndThePendulum
- ItemBookTimeMachine
- ItemBookTheCountryOfTheBlind
- ItemBookYouth
- ItemBookDieVerwandlung (German)
- ItemBookShinel (Russian)
- ItemBookBogPravduVidit (Russian)
- ItemBookTheCallOfCthulhu
- ItemBookTheShunnedHouse
- ItemBookTheShadowOverInnsmouth
- ItemBookTonyAndTheBeetles
- ItemBookATravelerInTimetime
- ItemBookTheHoundOfTheBaskervilles
- ItemBookTheCallOfCthulhu
- ItemBookTheShunnedHouse
- ItemBookTheShadowOverInnsmouth
Fiction
- ItemBookTheWarOfTheWorlds
- ItemBookAroundTheWorldIn80Days
- ItemBookCrimeAndPunishment
- ItemBookTheJungleBook
- ItemBookRobinsonCrusoe
- ItemBookThePictureOfDorianGray
- ItemBookTovarnaNaAbsolutno (Czech)
- ItemBookAndersen
- ItemBookTheCosmicComputer
- ItemBookTheVariableMan
Non-fiction
- ItemBookRussian
- ItemBookRussianCheatSheet
- ItemBookBible
- ItemBookTheArtOfWar
Poetry
- ItemBookTheRaven
- LesFleursDuMal
Drama
- ItemBookHamlet
- ItemBookBilaNemoc (Czech)
- ItemBookTheBrothersKaramazov
Automatically processed (Western)
- ItemBookTheOutlet
- ItemBookTheDesertOfWheat
- ItemBookFlyingURanch
- ItemBookHopalongCassidysRustlerRoundUp
- ItemBookLonesomeLand
- ItemBookTrailin
- ItemBookRonickyDoone
- ItemBookBettyZane
- ItemBookATexasMatchmaker
- ItemBookTheThunderBird
- ItemBookBlackJack
- ItemBookTheLastTrail
- ItemBookSelectedStories
- ItemBookTheHeritageOfTheSioux
- ItemBookDesertGold
- ItemBookTheHiddenChildren
- ItemBookTheLastOfThePlainsmen
- ItemBookGunmansReckoning
- ItemBookSkyrider
- ItemBookSpinifexAndSand
- ItemBookSunsetPass
- ItemBookTheCaveOfGold
- ItemBookTheDayOfTheBeast
- ItemBookTheQuirt
Automatically processed (Gutenbergs list of best books)
- ItemBookAVoyageToArcturus
- ItemBookAnInquiryIntoTheNatureAndCausesOfTheWealthOfNations
- ItemBookAndersensFairyTales
- ItemBookAroundTheWorldIn80Days
- ItemBookBlackBeauty
- ItemBookTheBibleKingJamesVersionBook18Job
- ItemBookBuddenbrooks
- ItemBookCrimeAndPunishment
- ItemBookTheDivineComedyComplete
- ItemBookDeadSouls
- ItemBookErewhon
- ItemBookTheEssaysOfMontaigneComplete
- ItemBookFatherGoriot
- ItemBookGreatExpectations
- ItemBookGulliversTravels
- ItemBookHeartOfDarkness
- ItemBookHerland
- ItemBookAdventuresOfHuckleberryFinnComplete
- ItemBookIvanhoe
- ItemBookKidnapped
- ItemBookLaChartreuseDeParme
- ItemBookLeavesOfGrass
- ItemBookLordJim
- ItemBookMadameBovary
- ItemBookNostromoATaleOfTheSeaboard
- ItemBookOnTheOriginOfSpeciesByMeansOfNaturalSelection
- ItemBookTheMetamorphosesOfOvid
- ItemBookTheMagnaCarta
- ItemBookMobyDick
- ItemBookHamlet
- ItemBookKingLear
- ItemBookLaDbcle
- ItemBookTheWhichWeeFinde
- ItemBookTheCanterburyTalesAndOtherPoems
- ItemBookPangHuang
- ItemBookAVindicationOfTheRightsOfWoman
- ItemBookAJourneyToTheInteriorOfTheEarth
- ItemBookNjalsSaga
- ItemBookTheTrial
- ItemBookDonQuixote
- ItemBookPhilosophiaeNaturalisPrincipiaMathematica
- ItemBookPrideAndPrejudice
- ItemBookDummy
- ItemBookGargantuaAndPantagruelComplete
- ItemBookRasselas
- ItemBookScaramouche
- ItemBookShe
- ItemBookSonsAndLovers
- ItemBookTheCallOfTheWild
- ItemBookTheComingRace
- ItemBookTheFurtherAdventuresOfRobinsonCrusoe
- ItemBookTheIdiotByFyodorDostoyevsky
- ItemBookTheIslandOfDoctorMoreau
- ItemBookTheMahabharataOfKrishnaDwaipayanaVyasaBk4
- ItemBookTheManWhoWasThursday
- ItemBookThePossessed
- ItemBookThePrisonerOfZenda
- ItemBookAPrincessOfMars
- ItemBookKingSolomonsMines
- ItemBookMiddlemarch
- ItemBookTheHouseOfTheSevenGables
- ItemBookTheMonkARomance
- ItemBookThePrivateMemoirsAndConfessionsOfAJustifiedSinner
- ItemBookTheRedBadgeOfCourage
- ItemBookADollsHouse
- ItemBookAfterLondon
- ItemBookAnOldBabylonianVersionOfTheGilgameshEpic
- ItemBookAnnaKarenina
- ItemBookCandide
- ItemBookExperimentalResearchesInElectricityVolume1
- ItemBookFaust
- ItemBookHinduLiterature
- ItemBookHunger
- ItemBookJapaneseLiterature
- ItemBookLeRougeEtLeNoir
- ItemBookOthelloTheMoorOfVenice
- ItemBookTheBrothersKaramazov
- ItemBookTheDecameronOfGiovanniBoccaccio
- ItemBookThePoemsOfGiacomoLeopardi
- ItemBookTheTragediesOfEuripidesVolumeI
- ItemBookAlicesAdventuresInWonderland
- ItemBookTheScarletPimpernel
- ItemBookTheThreeMusketeers
- ItemBookTheTurnOfTheScrew
- ItemBookThroughTheLookingGlass
- ItemBookTranslationsOfShakuntalaAndOtherWorks
- ItemBookTreasureIsland
- ItemBookUlysses
- ItemBookUncleSilas
- ItemBookTheAeneid
- ItemBookWarAndPeace
- ItemBookWielandOrTheTransformation
- ItemBookWutheringHeights
Various languages
- ItemBookCinqSemainesEnBallon
- ItemBookDracula
- ItemBookMrchenFrKinder
- ItemBookLesTroisMousquetaires
- ItemBookDeLorigineDesEspces
- ItemBookDasNibelungenlied
- ItemBookTheLastMan
- ItemBookFaustDerTragdieErsterTeil
- ItemBookLenfer1Of2
- ItemBookVoyageAuCentreDeLaTerre
- ItemBookRomeoUndJulia
- ItemBookFrankenstein
- ItemBookKrasavitseKotorayaNiuhalaTabak
- ItemBookContesMerveilleuxTomeI
- ItemBookDeLaTerreLaLune
- ItemBookLingnieuxChevalierDonQuichotteDeLaManche
- ItemBookLleMystrieuse
- ItemBookLaComdieHumaineVolumeIScnesDeLaViePriveTomeI
- ItemBookLesCorneilles
- ItemBook20000LieuesSousLesMersParts1amp2


The most useful for players is the russian cheat sheet, which contains a brief guide to reading cyrillic alhabet.


[dhall 5/06/2013 5:13:09 p.m.]:
 >The most useful for players is the russian cheat sheet, which contains a brief guide to reading cyrillic alhabet.
 Great idea!
 
 We've also had requests for foreign language books too, maybe there are some good famous German, Czech, and other books that could be included?
 
 Font could be an issue though as I think we only include loaded language characters so we'll need some programmer comment on that

[cokletka 5.6.2013 17:45:12]:
 The font works fine for both Russian and Czech language. I am going to add more books, I will definitely include some Czech and German.

[dhall 5/06/2013 6:01:15 p.m.]:
 Fantastic. I think some Russian books would be great too, Ivan will probably be a great source of inspiration there.

[dhall 6/06/2013 6:07:29 p.m.]:
 New book 3D model done and implemented in game
 
 If you could also add a reference to each book in:
 * https://dayz-svn.bistudio.com/svn/trunk/DZ/weapons/proxies/proxyDefines.hpp
 * https://dayz-svn.bistudio.com/svn/trunk/DZ/data/spawnDefines.hpp
 
 I should have added all the current ones from the config, but if you could add any new ones to that list as you make them, it would be great.

[cokletka 7.6.2013 13:48:04]:
 Ok, both files updated.

[cokletka 10.6.2013 20:38:39]:
 I made a tool that makes importing of the html files about twenty times faster. Therefore I was able to add more than a hundred new books. Should I add more? Maybe some more foreign languages?
 And there is one slight problem that might be discussed: The header with licensing information. I have removed it from each document although it says "do not remove this", because all licensing information is included also in the end of each book. Does anybody think that it might cause some problems, or is it ok this way?

[maruk 19.6.2013 18:09:16]:
 Cool. Please post the tool and description how to use it here. Re: License info it is more likely not a problem, it is questinable whether the books really have any license as they really are public domain. We should however keep the licences at the end of the book, mostly to give credit to people who crated the files. Ideally, we may keep also first part of the license: [Proposed improvements](https://projects.bistudio.com/dayz/Proposed_improvements.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2680) but the goal is to create experience from opening a book as immersive as possible.

[cokletka 20.6.2013 16:03:06]:
 My program is written in c#. The source code including executable can be downloaded from: http://martin.spanel.name/htmlFormating.zip
 It makes a copy of all files in specified folder, changes the header and makes some minor changes neccessary for good look of the books in the html viewer.
 Usage instructions:
 - download books in html from project Gutenberg
 - make sure that all of them are encoded in UTF-8
 - run bin/debug/htmlFormating.exe
 - fill the path of the folder containing downloaded files
 - fill titles if necessary
 - check all of the files in the folder processed. Sometimes happens that whole file is missing or that the header contains informations duplicitly or contains unnecessary informations. This must be corrected manually.
 - copy content of the three text files in the folder processed to
 	- https://dayz-svn.bistudio.com/svn/trunk/DZ/weapons/proxies/proxyDefines.hpp
 	- https://dayz-svn.bistudio.com/svn/trunk/DZ/data/spawnDefines.hpp
 	- https://dayz-svn.bistudio.com/svn/trunk/DZ/books/config.hpp
 - check the books again in game

[bebul 7.4.2014 11:04:13]:
 Tool jsem dal do našeho svn repository pro DayZ pgm: ( Drobnosti/GutenbergHtmlFormating )
 https://dev.bistudio.com/svn/pgm/DayZ/trunk/Drobnosti/GutenbergHtmlFormating
 
 Současně jsem utilitku upravil, aby brala PATH z commandline a dala následující Usage text, pokud PATH dodaná není:
 (
 Usage: htmlFormating.exe PATH
 path to the folder containing html books downloaded from project Gutenberg
 Result will be in the directory "result"
 The source files should be in UTF-8
 )
 *Pozn:* /Předáno Psychotronovi, který to zase deleguje dál./
 /test/