﻿We may have/want to re-enable TahomaB though as it changed from:
 > fonts[] = {"\\dz\\UIFonts\\Data\\Fonts\\EtelkaNarrowMediumPro\\EtelkaNarrowMediumPro6",,,,};
 > spaceWidth = 0.7;
 > spacing = 0.2;
 to
 > fonts[] = {"\\core\\data\\fonts\\tahomab16"};

Finally still quite some TahomaB definitions active from core config. 
See "RemainingNonAmorSerif.txt" attachment. 

*Unable to judge if these should be changed too. Thoughts?*

 [RemainingNonAmorSerif.txt][Attachment1] 
[qeberle 11.01.2013 11:36:19]:
 I went ahead and removed them too as TahomaB set in core config is replaced/overwrriten with a new config in uiFonts.

[Attachment1]: https://projects.bistudio.com/dayz/RemainingNonAmorSerif.txt