﻿Problem
===
* Light Levels are used to detect how easily a zombie can see the player.
* Currently a binary "is it night or day" check is done calculating the position of the sun.
* Very expensive for FPS and also the effect is not good.

Requested
===
* A function be made that returns a floating number representing the ambient light levels.

Explaination
===
* Possibly called "ambientLight".
* Would return a floating value ideally between 0 and 1 (assuming light levels are not open ended).
* Otherwise any value could be used.

Impact
===
* Will reduce FPS and also increase functionality of detection scripts

[hladas 26.6.2012 13:15:15]:
 94001
 added 2 new commands
 
 sunOrMoon - Returns sun to moon transition state.
 moonIntensity - Returns moon light intensity

[dhall 26/06/2012 1:16:47 p.m.]:
 Tested working in DayZ on internal build. Will advise when testing in production has been completed.
