﻿Update of the pause menu available in dev svn.

Notes:
* Reverted to the traditional design Marek requested
* The exit button text is hardcoded via IDC (to suspend when you can save)
* The continue and options button text is hardcoded in MP to uppercase

1h

 ![DZ\_PauseMenuSP.jpg][Attachment1]

 ![DZ\_PauseMenuMP.jpg][Attachment2] 

[Attachment1]: https://projects.bistudio.com/dayz/DZ_PauseMenuSP_3.jpg
[Attachment2]: https://projects.bistudio.com/dayz/DZ_PauseMenuMP_3.jpg