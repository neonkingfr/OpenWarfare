﻿>  ClientOutput už přenáší základní informace, kterými server říká, o jaké akce se zajímá, a dostává jen ty. Dále už funguje i lehání / stoupání (tj. GetActionToDo). Způsob synchronizace stavu budu ještě chtít trochu přepracovat, aby se přenášely jen rozdíly. Další na řadě by mělo být míření, rozhlížení a střelba.

U `ActionWithCurve` variant je situace trochu komplikovanější, protože výsledkem není jen hodnota, ale hodnota + křivka. Mělo by nicméně být možné hodnotu + křivku spočítat už na klientu a předat jen výsledek. Aby se mi to ale dobře dělalo, budu potřebovat výrazně zjednodušit vytváření nových přenášených struktur podobných jako je `ActionSubscription`, protože v `InputInterface` bude muset přibýt několik nových funkcí s odlišnými parametry.

Navíc tam narážím na AutoAim, který se v tuhle chvíli aplikuje někde uprostřed výpočtu - podle mého je autoaim ale stejně minimálně ve vodorovném směru rozbitý od Bébulovy změny 36958 z března 2006, kdy se čtení výsledku aimX přestěhovalo přes volání `AutoAimCursor` funkce, takže je otázka, jestli má vůbec smysl se na něj ohlížet.



[ondra 29.1.2013 17:21:14]:
 `ActionWithCurve` jsem konečně implementoval, zatím jsem to odzkoušel jen simulovaně, přes `ENABLED_WRAPPED_INPUT_`, kvůli [Dedikovaný server neběží](https://projects.bistudio.com/dayz/Dedikovaný_server_neběží.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2355) nejsem zatím schopen v MP testovat (101427)

 Autoaim je zatím vyřazen. Až začnu části simulace přesouvat zase na klienta, možná ho znovu povolím, to uvidíme.

[Attachment1]: https://projects.bistudio.com/dayz/basic.SampleMap.zip
[Attachment2]: https://projects.bistudio.com/dayz/DayZ-minimal.zip
[ondra 4.2.2013 15:40:26]:
 Na serveru to zatím nefunguje. Selhává testování `level`. 
 
 Zatím nerozumím tomu, co se děje špatně, zdá se mi, že ScanInput se v každém framu opravdu volá, ale při debugování vstupu z Gamepadu (to se debuguje výrazně lépe, než myš) vidím, že `xInputMaxLevelLastFrame` je 0, zatímco level je 1.
 
 Zvláštní je, že `GetAction`, které je skoro stejné (`level` také používá), funguje, a to i s gamepadovými vstupy.

[ondra 6.2.2013 13:07:19]:
 Hledal jsem složitou chybu a zase byla banální. Zapomněl jsem ActionWithCurve přidat do síťové zprávy. :(
 
 Aby se to neopakovalo, budu i membery v ClientInput / ClientOutput vytvářet pomocí stejného makra.
 
 Po opravě už mni akce s křivkami fungují. Je okamžitě vidět, že u rozhlížení a míření vadí latence přenosu mnohem víc.
 
 Boj s latencí tedy konečně musí následovat.
