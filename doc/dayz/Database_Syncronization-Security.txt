﻿There will be neccessary to add some security to the communication with the central server.
Now, everybody can send a fake character info and corrupt the player state record.

I would prefer to use some standard and approved solution, for example:
- based on certificates of the game servers, approved by us as a certification authority
- SSL protocol based communication (initial sample - http://curl.haxx.se/libcurl/c/simplessl.html)

I'm not experienced in this area, so any help will be welcome.


[dhall 4/07/2013 2:10:56 p.m.]:
 I will ask the server provider we are going to use for advice, they have a great deal of experience here as they host many MMO's

[lightfoot 12/07/2013 12:43:59]:
 Response from Multiplay;
 
 "SSL / HTTPS communication as you say a standard solution to this.
 
 Most opt to HTTPS as it tends to be simpler to implement, but that
 depends on the style of cummunication e.g. if your doing individual
 requests such as posting stats of player info on a periodic basis.
 
 If however your maintaining a permanent connection SSL may be a
 better way to go as you don't have to rely on how the web servers
 persistent connections works.
 
 Personally I'd recommend HTTPS if you can as maintaining a persistent
 connection which the game relies on brings its own issues as a
 persistent connection across the internet is always going to be
 troublesome.
 
 Key things to think of when doing this are:-
 1. Look to ensure all comms with the masters are async where possible
   e.g. run in a seperate thread. This helps prevent in game lag when
   comms to the masters take longer than usual due to internet issues
   between the server and master, which are not present between the
   server and player.
 2. Consider what your going to do when the communication with the
   masters is not possible or fails. Depending on the situation
   consider request retries or local secure storage which can then
   be retried at a later date. Obviously this is not always possible
   but you should always look to ensure that a match / game on
   a remote server can not to interrupted by the failure to
   communicate with the master server.
 
 All in all seek do loosely couple the server to the master as tight
 coupling will cause "player visible" issues even when the server
 player interaction isn't seeing any problems."
 

[jirka 28.8.2013 9:39:05]:
 Probably related article, describing how to enforce HTTPS on Apache Tomcat: http://www.mulesoft.com/tomcat-ssl
 
 Unfortunately, I do not see the clear description how to autentificate the clients there.
 

[jirka 28.8.2013 9:49:14]:
 This seems to be a nice description of client certificates:
 http://virgo47.wordpress.com/2010/08/23/tomcat-web-application-with-ssl-client-certificates/

[jirka 7.11.2013 9:45:54]:
 Based on what I read in the article (http://virgo47.wordpress.com/2010/08/23/tomcat-web-application-with-ssl-client-certificates/), I would recommend:
 - to implement HTTPS access directly in the Apache Tomcat, including client certification
 - client certification could be wanted, not required to enable requests to present player info on some web page
 - the requests from the game servers (especially when they want to update database) need to be checked if certificate is present
 - some info from certificate could be stored to the database to have a clear identification of possible attack sources
