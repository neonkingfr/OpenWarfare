﻿===Observed
If items are stored in a players backpack upon logout (items that hold a finite amount of uses/capacity eg: magazines, bandages, etc) those items can be duplicated upon logging back in.

This has been isolated as the following:
If a container is placed within a container, the contents of said container will be duplicated within the primary container.
eg:
1) Place a first aid kit inside either a pair of cargo pants, or a backpack
2) Log out
3) Log in
4) Contents of first aid kit should be duplicated

===Expected
Items not to be duped on logout

===Note
Example: The character in the attached image had two magazines, and one of each medical item upon log out.
https://jira.bistudio.com/secure/attachment/13413/2013-11-25_00001.jpg
https://jira.bistudio.com/secure/attachment/13453/table.jpg

Versions reproduced against:
Client: 112782 (Steam & SVN)
Server: 112782 (SVN)


===Repro
1) Equip any backpack
2) Pickup first aid kit (w/ items inside) and place in backpack
3) Log out
4) Log in, check back pack
Currently working on an example mission, but this will take me a few, as it requires writing to the database. (As we need to store player data for the log out / log in steps)


https://www.dropbox.com/s/2ulep1kvrdatui1/Medkit%20Dupe.mp4

[jirka 27.11.2013 9:27:00]:
 Function `ResourceSupply::ForEachItemCargo` (calling `InventoryStorage::ForEachItem`) now works recursively (since rev. 112559). Saving function has its own recursion, so some items are visited twice during save now.
 
 I will add a parameter to `ResourceSupply::ForEachItemCargo` (and `InventoryStorage::ForEachItem`) telling if recursion have to be provided.

[jirka 27.11.2013 10:13:00]:
 Should be fixed in revision 112905.
 
 Can you test it, please?

[lightfoot 28/11/2013 10:24:18]:
 Confirmed fixed by internal QA
