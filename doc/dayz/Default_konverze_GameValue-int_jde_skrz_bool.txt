﻿Následující, na pohled nevinný kód, je rozbitý:

(
GameValue VehSetId(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  int id = oper2;
)

Konverze z oper2 na int jde totiž přes bool, nikoliv float (jak nejspíš ten, kdo to psal,. čekal).

Na tohle jsem narazil v 3D editoru. Není vyloučeno, že je takových míst víc, nicméně když se to stane, dělá to assert, protože ve hře float na bool konvertovat nejde, tak toho snad moc nebude.

