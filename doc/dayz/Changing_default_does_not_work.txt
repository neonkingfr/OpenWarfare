﻿Bebul would you please try to look into this problem as it has MP test prio.

Default controls are not taken from CfgDefaultKeysMapping nor CfgDefaultKeysPresets.
It might relate to A3 changes to introduce Presets or DZ using DZ configs for UI.

**Repro**
1. start DZ
2. configure
3. controls
4. assignments
5. select "interact" group

**Obs**
* Use default action: enter, MMB
* Use selected action:  LMB
* Freelok interaction:  LMB

**Exp**
* Use default action: space, enter, MMB
* Use selected action:  space
* Freelok interaction:  space, LMB

Technically: CfgDefaultKeysMapping or DayZGame used. 


Source:

1. Default
(
class CfgDefaultKeysMapping
	action[] =			{DIK_SPACE,DIK_RETURN,INPUT_DEVICE_MOUSE + 2};
	actionContext[] =		{DIK_SPACE};
	actionFreeLook[] =		{DIK_SPACE,INPUT_DEVICE_MOUSE};
)

2. Not defined in DayZGame preset (which would be default)

3. DayZMod preset
(
action[] =			{DIK_RETURN,INPUT_DEVICE_MOUSE + 2};
actionContext[] =		{INPUT_DEVICE_MOUSE};
actionFreeLook[] =		{INPUT_DEVICE_MOUSE};
)

.. which fits **Obs** (for whatever reason) :|

4. class CfgDefaultKeysPresets

(
	delete Default;
	class DayZGame
		displayName = "DayZ game";
		default = 1;
		class Mappings
			#include "KeyPresets\DayZGame.hpp"//Empty
	class DayZMod
		displayName = "DayZ mod";
		default = 0;
		class Mappings
			#include "KeyPresets\DayZMod.hpp"
)

Source at:
(
.\DZ\ui\hpp
.\DZ\ui\hpp\KeyPresets
)


Note:
Removing class DayZMod, changing config class order, inheritance didnt work. I am clueless how it works currently. :/

[bebul 17.12.2012 11:03:11]:
 The first look: in `DZ\ui\config.cpp`  there is 
 (
 #include "hpp\cfgDefaultKeysPresets.hpp"
 )
 but I *cannot* fined any
 (
 #include "hpp\cfgDefaultKeysMapping.hpp"
 )
 Kju, try it, please, and tell me, whether task is over or something remains.

[qeberle 17.12.2012 11:35:01]:
 Yeah thats it. Lovely. :]