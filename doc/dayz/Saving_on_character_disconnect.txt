﻿Currently when a player disconnects, their agent is deleted. We need to gracefully handle the disconnection of a player by forcing a save prior to their agent being deleted.

We also may want to consider not immediately deleting their agent, so that there exists some penalty to disconnecting, say in the middle of a firefight. World of Warcraft does this with a despawn timer of ten seconds.

Thoughts?

[qeberle 13.03.2013 14:59:42]:
 First I would advise to split the topic "save on disconnect" from the topic "disconnect abuse".
 
 The former should be rather easy as it should be done on the server.
 
 The options for later depends if you can determine if a player just left the the server, from cutting his internet, closing the game's task or a game crash.
