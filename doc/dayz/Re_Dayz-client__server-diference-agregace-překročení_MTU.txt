﻿>  Vše se mi zdá v pořádku, merguji.

Po mergi někdy dochází k překročení velikosti MTU u negarantovaných zpráv. V reportu se pak objevuje např. 

(
 8:25:49 NetServer: trying to send too large non-guaranteed message (3355 bytes long, max 1348 allowed)
)

Zatím neznám jednoduché repro, zatím bylo reprodukováno jen na serveru z mnoha hráči. Je možné, že aby se to stalo, vyžaduje to, aby (trochu nečekaně) byl dostatečný provoz v garantovaných zprávách.

[Attachment1]: https://projects.bistudio.com/dayz/manyMoving.SampleMap.zip