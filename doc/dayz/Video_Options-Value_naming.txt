﻿Jay makes a good point here: news:k9sk1n$6ma$1@new-server.localdomain

In short he suggests to change the system from:
"very low, low, normal, high, very high"
to
"low, standard, high, very high, extreme/ultra"

It is a simply fix, but the phsychology involved is sound.



Personally I think it would be even good add in brackets or as tooltip "CPU/GPU intensive" to certain settings.

A good example is shadow options were normal and high are CPU bound, whereas very high is GPU.
For such specific cases even specific value naming would be good.

Overall some settings have next to no performance impact, whereas others are cruical. 
This should be explained in a simple fashion to the player.


[dhall 10/12/2012 10:29:48 a.m.]:
 Fits with other games I have played, people tend to understand that Ultra/Extreme is far beyond the capability of nearly all computers - but Very High comes with a different connotation.
[maruk 11.12.2012 10:31:14]:
 >„low, standard, high, very high, extreme/ultra“
 
 Good idea, fell free to go ahead.

[qeberle 11.12.2012 11:04:15]:
 Could it be that the values for video settings are hardcoded?
 
 I only found these strings in languagecore for terrain detail (str_terrain_XXX) or are these used for all other settings too?

[maruk 11.12.2012 13:57:45]:
 Maybe used for all settings

[qeberle 12.12.2012 09:09:16]:
 Possible to get access to core config?
 
 Otherwise you need to give me a way to overwrite the value to test it, as since arma a second string definition of the same key is ignored.
