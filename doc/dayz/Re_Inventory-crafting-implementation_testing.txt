﻿I have configured the *MedicalItem_DisinfectantSpray* item to be able to disinfect items.

I have made two receipes for testing purposes, one allows disinfecting on cans of baked beans:

(
	class DisinfectantSprayTest
	{ 
		name = "Disinfect Item (Test)"; 
		tools[] = {"MedicalItem_DisinfectantSpray","FoodItem_CanBakedBeans"}; 
		action = "hint 'crafted'";
	};
)

Orange crafting indicator and context menu appear, however the hint message is not displayed. 
Is the action supposed to be configured in SQF syntax?

[jirka 20.3.2013 15:58:33]:
 Yes, the SQF syntax is expected. Not sure if ( ' ) can be used to encapsulate the string.

[jirka 20.3.2013 16:00:10]:
 I did not test the action and condition yet, so it is possible there is some bug there. Is the recipe in the distribution? I can trace what happened there.

[dhall 20/03/2013 4:09:59 p.m.]:
 Yes, receipe is in distribution.
 
 If you add *MedicalItem_DisinfectantSpray* and *FoodItem_CanBakedBeans* items to your inventory, you can test their interactions.
 
 Possibly is it because there is no "results" items defined, so maybe it doesn't generate the action?

[jirka 21.3.2013 9:22:44]:
 Fixed in revision 103280. (something was missing when expression compiles)
 
 Recipes without *results* are okay.
