﻿Done at rev.99875

Testing mission [RoleLessTest.SampleMap.pbo][Attachment1]

Some more info:
1. use `createPlayers = false;` in mission `description.ext` for /roleless/ missions
2. the player must be selected in init script. My simple approach was:
(
private["_agent"];

if (!isServer || !isDedicated) then {
  _agent = createAgent ["Survivor1_DZ",  [2473,2530,2], [], 0, "NONE"];
  selectPlayer _agent;
};
)
3. the preload around camera is processed after the player is selected and client starts to play after
4. *disputable*: when player escapes from the game, he is moved to /select server/ display `DisplayMultiplayer`
5. more tests needed


[Attachment1]: https://projects.bistudio.com/dayz/RoleLessTest.SampleMap.pbo