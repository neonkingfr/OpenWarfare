﻿Problem
===
The DayZ server authenticates new players against a central database. Information on player setup is sent to the client via published //setVariable// commands, with //publicVariable// used for generating actions by the server. Variables only actioned on the dedicated server are therefore distributed to all clients needlessly.

Requested
===
* Command that allows sending a variable to the server only
 (
publicVariableServer <variable>
)
* Command that allows sending a variable to a specific client
 (
<clientID> publicVariableClient <variable>
)

Explanation
===
The server receives a large amount of imformation from the database about what attributes a player has, such as their equipment loadout. This is sent to the player as a large string attached to them using the setVariable command. This array would be better sent directly to the specific client, which could be discovered using the //owner// command.

Player updates could then be prepared by the client, and the whole database update sent as a string directly. This would remove problems associated with a client disconnecting before their update has been actioned by the server.

Impact
===
This would be very useful for optimizing message transmission for DayZ, and allow more ambitious use of strings for reducing the server load associated with processing of updates.

[jirka 26.6.2012 13:10:07]:
 PublicVariableClient is clear, one question related to publicVariableServer:
 
 How the server handles the variable client sent it? In some event handler? (in other words, how far the variable need to be sent)

[dhall 26/06/2012 1:15:18 p.m.]:
 Currently, during a player's setup FSM, they send their request for authentication via //publicVariable//. The client then goes into a loop waiting for a variable to be set to their body (checks using //getVariable//). Ideally this loop would be done simply with a normal variable to reduce object dependence.

[ondra 26.6.2012 13:27:22]:
 > in other words, how far the variable need to be sent
 
 I guess the variable needs to be sent to the "bot client", as this is the space where the scripts operate, and without sending it there it would make no sense at all.

[dhall 26/06/2012 1:37:59 p.m.]:
 Yes, that's correct.
 
 1. Client requests authentication through publicVariable
 2. Server activates function through publicVariable Event Handler
 3. Server actions authentication, dispatches result through setVariable (ideally, this would be sent via publicVariable sent ONLY to the client in question).

[jirka 26.6.2012 13:46:13]:
 OK, now it's clear.


[jirka 27.6.2012 13:08:29]:
 Should be done (revision 94188).

[jirka 27.6.2012 13:11:39]:
 These commands will not be fully backward compatible. The old exe likely would not crash, but variables will not be transferred at all (they are using a new network message).

[bebul 11.7.2012 12:11:12]:
 NG ticket news:jtjjcd$86g$1@new-server.localdomain
 There are several issues with implementation of publicVariableClient / publicVariableServer
 causing e.g. 4x more traffic than usual publicVariable 
 + publicVariableClient seems to try to send the PV variable to a null object forever.
 https://dev-heaven.net/issues/35176

[jirka 11.7.2012 12:51:20]:
 Dean was reporting some problems related to publicVariableServer, but this ticket is not understandable to me.
 
 - I do not know what is meant by a null object (there is no object as a parameter of publicVariableClient, only client ID and the variable name)
 - in the situation when player with given client ID does not exist (disconnects), the command works like regular publicVariable (should this to be changed to do nothing?)

[jirka 11.7.2012 13:56:36]:
 > should this to be changed to do nothing?
 
 This makes more sense probably, changed this way in 94759.

[dhall 13/07/2012 10:17:33 a.m.]:
 I will try this out and see how it goes. I was only able to see the problems when the server was under significant load.
