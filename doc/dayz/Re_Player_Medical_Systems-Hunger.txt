﻿I've started with the implementation of hunger. This is the record of how hungry a character is, which is affected by:
* Character movement (running etc...)
* Cold temperature
* Medical conditions

[dhall 19/06/2013 2:31:31 p.m.]:
 Hunger is currently calculated in calories, which is a fairly arbitrary unit but useful for this purpose

[dhall 19/06/2013 3:35:17 p.m.]:
 Thirst is now calculated in litres, above zero means thirsty. Status messages are displayed at intervals to warn player

[dhall 19/06/2013 3:56:24 p.m.]:
 calories and water added to item config class called "resources" such as:
 
 (
 	class SodaBase: DrinksItemBase
 	{
 		...
 		class Resources
 		{
 			calories = 140;		//Cola can used as a base
 			water = 355;		//quantity given when consumed, in litres
 		};
 	};
 )
 This is now read when consuming an item, and applied to hunger/thirst

[dhall 1/07/2013 11:56:38 a.m.]:
 * Player can now refill water bottles at ponds and wells.
 * Water Bottles have multiple uses, using a little more each time
