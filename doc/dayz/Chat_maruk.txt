﻿
##### maruk (Marek Španěl) 10:36
Ahoj, jak to vypada s tou mp bublinou, je to hotove, nebo jeste neco na tom delas?

##### ondra (Ondřej Španěl) 10:37
Hotové to je ... skoro :), funguje to velmi pěkně, ale jedna věc nefunguje správně, a to jsou objekty, které nejsou v krajině (posádky vozidel, itemy u člověka apod). Na tom teď pracuji a je to otázka tak 1-2 dní.

##### maruk (Marek Španěl) 10:38
A tim to myslis bude hotove uplne? Dean mi rikal neco o nejakem problemu v mp, co zpusoboval nejake rozklady, ale je to uz par dni, treba to uz zaniklo, nebo je to neco z toho, co pises.

##### ondra (Ondřej Španěl) 10:39
Může to být tohle, Dean se na tohle pravidelně poptává. O ničem jiném nevím.

##### maruk (Marek Španěl) 10:39
Tak to bude to same

##### ondra (Ondřej Španěl) 10:40
Až to bude, jako další můžu zkusit se podívat na garantovaný (později samoopravný) provoz. Měla by to být poměrně málo rozsáhlá změna, která může přinést snížení nároků na pásmo snad i několikanásobné, možná za cenu občasného zpoždění reakcí na střelbu tomu, komu se ztrácejí pakety v míře větší než malé.

##### ondra (Ondřej Španěl) 10:41
[Re: Dayz - client / server - vše garantovaně, příp. s použitím samoopravných kódů](https://projects.bistudio.com/dayz/Re_Dayz-client__server-vše_garantovaně,_příp._s_použitím_samoopravných_kódů.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2890)

##### ondra (Ondřej Španěl) 10:42
... a je to takový druh technologie, který by šel bez problémů mergovat do A3, ba dokonce i do A2OA.

##### maruk (Marek Španěl) 10:42
To zni zajimave. Jinak parkrat jsem to ted hral (tedy dneska je to nejake uplne rozbite: fps mam 10, slysel jsem von nejakeho cloveka, co byl kdovikde, nefunguje mi quick bar) a zacina me to bavit, nekdy doporcuji zkusit, uz to je hra.
