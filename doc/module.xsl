<?xml version="1.0" encoding="windows-1250" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="root">
	<HTML>
		<HEAD>
			<TITLE>Modules documentation</TITLE>
		</HEAD>
		<BODY>
			<H2>Modules documentation</H2>
			<xsl:apply-templates/>
		</BODY>
	</HTML>
</xsl:template>

<xsl:template match="layer">
	<HTML>
		<HEAD>
			<TITLE>Layer <xsl:value-of select="@name"/></TITLE>
		</HEAD>
		<BODY>
			<H2>Layer <xsl:value-of select="@name"/></H2>
			<xsl:apply-templates/>
		</BODY>
	</HTML>
</xsl:template>

<xsl:template match="module">
	<HTML>
		<HEAD>
			<TITLE>Module <xsl:value-of select="@name"/></TITLE>
		</HEAD>
		<BODY>
			<H2>Module <xsl:value-of select="@name"/></H2>
			<xsl:apply-templates/>
		</BODY>
	</HTML>
</xsl:template>

<xsl:template match="structure">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="submodule">
	<H3><U>Submodule <xsl:value-of select="@name"/></U></H3>
	<BLOCKQUOTE>
	<xsl:apply-templates/>
	</BLOCKQUOTE>
</xsl:template>

<xsl:template match="interfaces">
	<H3><U>Implements interfaces:</U></H3>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template name="afile">
	<xsl:param name="file"></xsl:param>
	<A>
		<xsl:attribute name="HREF">/<xsl:value-of select="$file"/></xsl:attribute>
		<xsl:attribute name="target">_blank</xsl:attribute>
		<xsl:value-of select="$file"/>
	</A>
</xsl:template>

<xsl:template match="interface">
	<H4><U>
	Interface <I><xsl:value-of select="@class"/></I> in
	<I>
		<xsl:call-template name="afile">
			<xsl:with-param name="file" select="@file"/>
		</xsl:call-template>
	</I></U></H4>
	<BLOCKQUOTE>
	<xsl:apply-templates/>
	</BLOCKQUOTE>
</xsl:template>

<xsl:template match="implementation">
	<P>Implementated via class <I><xsl:value-of select="@class"/></I>.</P>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="extensions">
	<H3><U>Optional extensions:</U></H3>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="use_interface">
	<H4><U>Through interface <I><xsl:value-of select="@class"/></I> in
	<I>
		<xsl:call-template name="afile">
			<xsl:with-param name="file" select="@file"/>
		</xsl:call-template>
	</I></U></H4>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="use_implementation">
	<H4>Implementation: <xsl:value-of select="@name"/></H4>
	<BLOCKQUOTE>
	<P>Add file
	<I>
		<xsl:call-template name="afile">
			<xsl:with-param name="file" select="@selector"/>
		</xsl:call-template>
	</I> to your project.</P>
	<xsl:apply-templates/>
	</BLOCKQUOTE>
</xsl:template>

<xsl:template match="description">
	<P><xsl:apply-templates/></P>
</xsl:template>

<xsl:template match="layers">
	<P>Layers:</P>
	<UL><xsl:apply-templates/></UL>
</xsl:template>

<xsl:template match="modules">
	<P>Modules:</P>
	<UL><xsl:apply-templates/></UL>
</xsl:template>

<xsl:template match="other_files">
	<P>Other files:</P>
	<UL><xsl:apply-templates/></UL>
</xsl:template>

<xsl:template match="files">
	<P>Files:</P>
	<UL><xsl:apply-templates/></UL>
</xsl:template>

<xsl:template match="ref_file">
	<LI>
		File<xsl:text> </xsl:text>
		<xsl:call-template name="afile">
			<xsl:with-param name="file" select="@name"/>
		</xsl:call-template>
		<xsl:text> </xsl:text>
		<xsl:apply-templates/>
	</LI>
</xsl:template>

<xsl:template match="ref_directory">
	<LI>
		Directory<xsl:text> </xsl:text>
		<xsl:call-template name="afile">
			<xsl:with-param name="file" select="concat(@name, '/')"/>
		</xsl:call-template>
		<xsl:text> </xsl:text>
		<xsl:apply-templates/>
	</LI>
</xsl:template>

<xsl:template match="ref_module">
	<LI>
		Module<xsl:text> </xsl:text>
		<A>
			<xsl:attribute name="HREF">/<xsl:value-of select="@name"/>/module?xdoc</xsl:attribute>
			<xsl:value-of select="@name"/>
		</A>
		<xsl:text> </xsl:text>
		<xsl:apply-templates/>
	</LI>
</xsl:template>

<xsl:template match="ref_layer">
	<LI>
		Layer<xsl:text> </xsl:text>
		<A>
			<xsl:attribute name="HREF">/<xsl:value-of select="@name"/>/layer?xdoc</xsl:attribute>
			<xsl:value-of select="@name"/>
		</A>
		<xsl:text> </xsl:text>
		<xsl:apply-templates/>
	</LI>
</xsl:template>


<xsl:template match="dependencies">
	<P>Depends on:</P>
	<UL><xsl:apply-templates/></UL>
</xsl:template>

</xsl:stylesheet>
