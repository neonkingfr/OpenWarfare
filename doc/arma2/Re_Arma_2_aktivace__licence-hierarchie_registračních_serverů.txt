﻿>  - veřejný klíč bude možno získat REST službou GET, bude rovnou ve formátu .h vhodném pro začlenění do hry. //Dovedu si i představit, že by se tohle dalo automatizovat (klíč stahovat curl utilitou v rámci buildu), ale zatím se mi to nezdá potřebné.//

Sepíšu sem ještě jednu možnost, kterou jsme s Jirkou probírali, ale zdá se nám zatím zbytečně komplikované (možná ale, že se něco takového bude časem hodit třeba pro OF).

Veřejný klíč serveru by hra získávala procesem obdobným registraci (přes tu REST službu) a uložila si ho zas do registry. Jednalo by se vlastně o jakousi jednorázovou "registraci" serveru.

Odpadl by tím jeden ruční krok v rámci buildu, navíc bych si pak byla možná i automatická revokace: Pokud herní server zjistí, že se ho někdo ptá na certifikát, který nezná, seznam uznávaných certifikátů si načte znovu celý nově přes REST službu (typicky bude v seznamu jeden certifikát).

Pak by už všechno mohlo být konfigurované nezávisle na serverech (včetně IP adresy registračního serveru, tu by pak servery musely posílat klientům). Root certifikační autorita by pak byla jediný pevný bod.

Riziko by tu bylo, že by mohly vznikat "clustery nezávislých registrací" - paralelní "pirátské" komunity s vlastními registračními servery (a vlastními seznamy CD klíčů). Proti tomu bychom si museli udělat vlastní root certifikát.

Struktura registračních serverů by pak byla komplikovanější, privátní klíč rootu by nesměly znát registrační servery, jinak to celé nemá smysl. Registrační server by se tedy musel registrovat u nějakého centrálního serveru, procesem obdobným, jakým se teď registrují hráči.

