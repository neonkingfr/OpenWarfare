﻿Když si dám do hry M1A2 a nechám gunnera, aby otáčel věží, moje velitelská věžička se otáčí spolu s ní, což je velmi dezorientující.

Nevím, od kdy to je, ale nejsem si vědom, že by to někdo dělal schválně, takže to považuji za chybu stabilizace velitelské věžičky. U věže gunnera žádný takový problém nepozoruji, ta je stabilizovaná dokonale.

[ondra 27.7.2011 10:19:25]:
 Zatím jsem zjistil, že už verze před zavedením interpolace (79645) se chová stejně.
 
 Problém hodně připomíná [news:cko9t7$hgi$1@new_server.localdomain](Vez ovlivnuje vezicku velitele)
 
 Zdá se ovšem, jako by to tak tentokrát bylo nastaveno v konfigu. Velitelská věžička má v debuggeru jako _stabilizedInAxes hodnotu SAxisNone.
 Příslušný kód pochází z revize 56124, bxbx, 27.3.2009 13:27, new option to disable turret stabilization
[ondra 27.7.2011 11:01:36]:
 Zdá se, že to tak někdo nastavil v konfigu. Dále viz [Re: Non-stabilised turrets - M1A1, T72](news:j0ok4s$uph$1@new-server.localdomain)
