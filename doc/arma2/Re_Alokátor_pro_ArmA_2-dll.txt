﻿> - IntelTBB 3 funguje velmi dobře, ale jen s našimi úpravami. Licence, kterou nám pro takové použití Intel nabízí, je velmi drahá

Zdá se, že GPL + RE by mělo být schůdné. Proto zkouším též následující cestu: pokud se vedle aplikace najde memalloc.dll, použijí se funkce z něj. Funkce jsou následující:

`  size_t __stdcall MemTotalCommitted();
  size_t __stdcall MemTotalReserved();
  size_t __stdcall MemFlushCache(size_t size);
  void __stdcall MemFlushCacheAll();
  size_t __stdcall MemSize(void *mem);
  void *__stdcall MemAlloc(size_t size);
  void __stdcall MemFree(void *mem);`

Interface na straně programu už mám hotový, nyní bych chtěl implementovat dll pomocí všech dosud zkoušených alokátorů, vč. TBB 3 a TBB 4, s tím, že primárně mě v tuto chvíli zajímá TBB 3. Všechny alokátory bych pak chtěl distribuovat se hrou, s tím, že na Webu budou ke všem k dispozici zdrojové kódy s příslušnými licencemi. Jsem docela zvědav, jestli například bude někdo z komunity schopen chyby, které bránily některé alokátory použít, opravit.

