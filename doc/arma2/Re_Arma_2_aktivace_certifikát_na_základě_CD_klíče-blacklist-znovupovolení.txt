﻿> > - každý hráč i server si eviduje pořadové číslo blacklistu, který zná, to se zvyšuje vždy při blacklistování dalšího PlayerId (pokud nebudeme blacklistovaná PlayerId expirovat, tak pořadové číslo blacklistu je vlastně jeho délka)

Zatím se používá právě ta délka. Znamená to ale, že nefunguje správně situace, kdy se klíč považuje za banovaný a následně se odblokuje. To se může stát buď proto, že důvody k blokování jsou tak dávno, že už mezitím vypršely, nebo (a to je představitelnejší) v důsledku manuálního zásahu "Reset history" ze správce klíčů. Tím se seznam zkrátí a nezačne se znovu distribuovat, dokud se znovu neprodlouží nád původní délku.
[ondra 7.1.2013 15:40:42]:
 Teď je například zablokovaný můj klíč s ID=2995654. Nově registrovaní hráči tuto blokaci postupně roznesou mezi servery. I když udělám Reset history a zaregistruji se znovu, nepomůže mi to, protože sice budu mít platný certifikát, ale servery o mně budou vědět z dřívějška, že jsem zablokovaný, a to si budou myslet tak dlouho, dokud se k nim nepřihlásí nějaký uživatel, který se registroval poté, co se zabanovaly ještě dva další klíče. Kdybych synchronizoval správně podle timestampu a ne podle délky, servery by se správný seznam dozvěděly spolehlivě přímo ode mně.

[ondra 7.1.2013 16:11:31]:
 S timestampem tu mám jeden problém: senzam zakázaných klíčů je podepsaný, takže pokud do seznamu něco přidám, musím řešit i otázku podpisu. Pokud timestamp zahrnu do existujícího podpisu, neuznají takový seznam dosavadní servery. Kompatibilnější se tedy jeví přidat navíc podpis nový. Abych si ale otevřel vrátka k tomu, abych v budoucnu nemusel mít podpisy dva, udělám to, že nový podpis bude zahrnovat timestamp i seznam id, takže až se nová verze dostatečně rozšíří (např. v 1.63), můžu starý podpis zase zrušit.

[ondra 7.1.2013 17:18:04]:
 Mělo by to být hotové, zatím jsem nezveřejnil registrační aplikaci kvůli [zapomenutému commitu](https://projects.bistudio.com/Arma2/Re_Arma_2_aktivace-registrace_z_jedné_IP_adresy_2.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2243).
 
 Udělat to zpětně kompatibilní mi dalo trochu víc práce, než jsem čekal, ale snad by to mělo být v pořádku. Nejnepřehlednější situace je nový klient s novým blacklistem, když se připojuje ke starému serveru. Kvůli tomu si klient musí v identitě pamatovat podpis 2x, s timestampem i bez něj.
