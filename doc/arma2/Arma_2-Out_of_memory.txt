﻿Pokus o repro dle http://forums.bistudio.com/showpost.php?p=1947912&postcount=47

Hostoval jsem misi z kampaně Harvest Red/Manhattan na LAN, bez toho, aby se ke mně někdo připojil, misi jsem se snažil hrát dle defaultně přidělených tasků, k tomu jsem občas jen tak létal volnou kamerou.

Pozorování:

Jakmile `Phys. to limit` dosáhlo pod 0, snažil jsem se přimět alokátor, aby vrátil nějakou fyzickou paměť systému. Časově by to zhruba 10-30 minutám hraní mohlo odpovídat.

Zajímavé též může být [postavy - kopírování stavu](https://dev.bistudio.com/svn/pgm/Colabo/data/ArmA2/Re_Interpolace_vizuálního_stavu-postavy-kopírování_stavu.txt?server=https%3A%2F%2Fdev.bistudio.com%2FColabo%2Fresources%2F&database=&id=1625), které může projevy chyb v alokátoru zesilovat.
