﻿Cíl:
- zjistit zatížení pásma na DayZ serveru jednak při běžném provozu, jednak při JIP připojování klientů 
Popis:
- lokální (nededikovaný) DayZ server
- 7 dedikovaných klientů se během krátkého času připojí JIP
- budeme sledovat (lehce vylepšené) statistiky síťového provozu při připojování a pak během hry




[jirka 21.6.2012 14:49:29]:
 Připojování 7 klientů:
 Celkem odesláno ze serveru cca 920 kB (výhradně garantovaně), z toho:
 - 28% ObjectSetVariable
 - 15% MsgFormat
 - 14% AddMagazineCargo
 - 13% CreateVehicle
 - 6% UpdatePositionVehicle
 - 3% PublicVariable
 - 3% UpdatePositionMan
 

[jirka 22.6.2012 8:50:16]:
 Během jedné minuty "normálního" hraní (bez nějaké interakce):
 Celkem odesláno 680kB, z toho:
 - 87% UpdatePositionMan 
 - 5% ObjectSetVariable 
 - 2% AIStatsMPRowUpdate 
 - 2% UpdateClientCameraPosition 
