﻿Problem
===

- When a client disconnects, locality for their controlled agents is transferred to the server.
- Through SQF code locality cannot be manually transferred between clients.
- The current way to deal with this situation is to check for local agents on the server and delete them.

Request
===
- Locality of an agent can be set on the server, e.g:
(
    <object> setOwner <client>;
)    
- Locality Event Handler be created, e.g.
(
    Event: "Local"

    Passed Array: [<object>,<local>]
        * object:   object reference
        * local:    bool (true if local)
)

Explaination
===
- Code example:
(
    if (isLocal _agent) then {
        /*
            ...check for nearby players, if so, select that player...
        */
        _clientID = owner _nearbyPlayer;
        _agent setOwner _clientID;
         /*
            ...on client, locality eventHandler is triggered to activate zombie FSM
        */
    };
)
Impact
===
- Significant positive network performance impact, as zombies would not be deleted then recreated on a new client.
- Removes "disconnect" exploit as a method for getting rid of zombies.
- Improves gameplay as other player controlled zombies don't disappear around you when a player disconnects.
[jirka 26.6.2012 13:05:18]:
 > - Locality of an agent can be set on the server, e.g:
 > 
 >    ( <object> setOwner <client>; )
 
 Done, the command returns true when locality changes, false otherwise.
 
 >     
 > - Locality Event Handler be created, e.g.
 > 
 >     Event: "Local"
 > 
 >     Passed Array: [<object>,<local>]
 >         * object:   object reference
 >         * local:    bool (true if local)
 
 Done (all in revision 94028).
 
 Beware: The event handler is fired whenever locality changes, not only as a reaction to setOwner scripting function. If you need to change this, let me know.
 
 Only basic testing was done, check if everything works as expected, please.
