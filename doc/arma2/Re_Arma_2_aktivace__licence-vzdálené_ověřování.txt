﻿> Platnost licence se kontroluje v multiplayeru (server kontroluje platnost licence všech hráčů, hráče bez licence vykopne.
> Platnost licence se kontroluje při stahování, uživateli bez licence se obsah nepošle.

Základní prvek pro tohle je [ověřená identita hráče](https://projects.bistudio.com/Arma2/Re_Arma_2_aktivace-ověřená_identita_hráče.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1698). Díky ní si můžeme ověřit, že licence, kterou se hráč prokazuje, byla vydána skutečně pro něj. I pokud se ji někomu jinému podaří zachytit, nebude mu nic platná, nebude jí moct použít.

Dokonce bych v tuto chvíli řekl, že taková kontrola se mi zdá dostatečná a nevidím ani nutnost svazovat licenci s [konkrétním počítačem](https://projects.bistudio.com/Arma2/Re_Arma_2_aktivace-ID_počítače.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1696), ale to si ještě rozmyslím. Pro offline hraní, kde ověřování druhými hráči přes síť není, to může mít smysl.

> Jako speciální špek při kontrole licence bych se snažil udělat, že nikdo, kdo kontroluje druhému platnost licence, mu licenci neschválí, pokud oba vycházejí ze stejného CD klíče.

Tenhle špek bych tam opravdu rád udělal, znemožňuje hrát z jednoho uživatelského účtu na více počítačích současně.

