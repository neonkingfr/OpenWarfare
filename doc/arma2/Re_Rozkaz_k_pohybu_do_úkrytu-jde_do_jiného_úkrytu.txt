﻿> Podobně, jako můžu AI dávat rozkazy na pozice v domě, bych rád měl možnost dát AI rozkaz doběhnout k nějakému konkrétnímu úkrytu.

Při testování [Re: AI - Vyhledávání pozice pro útok - úkryty](https://projects.bistudio.com/Arma2/Re_AI-Vyhledávání_pozice_pro_útok-úkryty.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=2016) jsem si zkusil, jak by to dopadlo, když dám rozkaz ručně. Když ale dám AI rozkaz k pohybu do úkrytu na rohu těsně před námi, AI místo toho vleze do sousedního úkrytu vedle stěny.

[command-cover.utes.zip][Attachment1]

V debuggeru vidím, že když se dává rozkaz do úkrytu v `InGameUI::IssueCommand` / `Command::Move`, konkrétní úkryt se nepředává, jen se dává jeho přibližná pozice: (
      command._destination = _cover->_coverPos.FastTransform(Vector3(0,0,-1));
      command._movement = Command::MoveFastIntoCover;
      command._precision = 5.0f; // allow cover finding
)

[ondra 23/10/2012 14:39:28]:
 Byl to tím, jak se postupně přidávaly kandidáti na vstup do úkrytu. Přidávaly se střídavě zleva a zprava, jakmile se přidaly dva, skončilo se. Mohlo se ale stát, že se přidaly dva zprava a žádný zleva. V důsledku toho z naší strany nebyl pro úkryt vhodný vstup. Podobným problémem trpělo mnoho rohů, takže oprava může docela podstatně zlepšit chování AI. Nyní se přidává po jednom z každé strany, pro přestrukturování smyčky jsem využil C++11 lambu. (98343)


[Attachment1]: https://projects.bistudio.com/Arma2/command-cover.utes.zip