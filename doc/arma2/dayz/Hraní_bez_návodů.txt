﻿Je tu jedna taková širší designová otázka, a to nakolik chceme, aby DayZ šlo hrát nez návodů, hledání na internetu, dotazování se ostatních hráčů po Skype nebo globálním chatu apod.

Já se k tomu stavím tak, že chci být to nejvíc ponořen do hry a hledání návodů mimo hru (fóra, youtube videa mě nebaví), ale chápu, že jsou naopak lidé, kterým tento "komunitní" aspekt vyhovuje. To, že se zákonitosti hry učím postupně, mi připadá přijatelné, ale v současném stavu myslím, že sebeschopnější hráč nemá bez "návodů" moc šanci obstát, protože lecjaké věci fungují neintuitivně a zdá se mi vhodné to změnit (některé konkrétní případy rozvedu v samostatných vláknech).

Mám též za to, že leckteré informace, které si komunita v současné době předává, nejsou ani tak získány zkušenostmi ze hry, jako reverzním engineerování modu (např. mapy skladišť a jiných objektů, nebo informace o tom, [kde je jaká šance najít které předměty](http://picacid.com/arma2/loot_en.html), případně k dokonalosti dovedená [kombinace obojího](http://dayzmap.info/)). Tenhle druh "meta-hry" už mě pak nebaví vůbec.

[bebul 31.5.2012 15:28:24]:
 Crafting v Mine Craftu je taky o tom, že si to "komunita" předává dál a informace o tom, jak se ve hře dobrat zajímavých věcí je spíše toho druhu, že "nelze hrát bez návodů, hledání na internetu, dotazování se ostatních hráčů..." Víc hlav víc ví a hře to tak umožňuje skrývat věci daleko rafinovanějším způsobem, který může hráčům dlouho unikat a v okamžiku objevení způsobit velké nadšení. Kdyby takovou věc dlouho nenašli, tak /leak/ z naší strany je možný vždycky. Já bych hádal, že to bude hráče víc bavit, když to bude aspoň v některých věcech (třebas ne tak esenciálních) velmi málo návodný.

[ondra 31.5.2012 16:00:15]:
 Nejde mi ani tak o návody, jako spíš o to, aby se věci chovaly rozumně, intuitivně, aby člověk mohl častěji použít zdravý  rozum a zkušenosti z reálného světa.
