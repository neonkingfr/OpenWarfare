﻿
[jirka 8.6.2012 10:30:58]:
 Nevím zda to není specifikum mé testovací mise, ale při zavádění animací zombie se do reportu píše velké množství hlášení typu:
 none(
  593.504: Warning: unknown value 'AH64_Pilot'
  Invalid move AH64_Pilot in action ah64_pilot (bin\config.bin/CfgMovesBasic/Actions/Action_AnimViewControl/)
 )
 Podstatným způsobem to ovlivňuje dobu spouštění mise.
 


[jirka 12.6.2012 9:25:32]:
 Tohle se sice vypisuje přes RptF, ale jen v interní verzi (_ENABLE_REPORT). I v reportu z hraní DayZ ale vidím hlášení spojená s animacemi:
 
 none(
   bin\config.bin/CfgMovesZombie/States/AinvPknlMstpSnonWnonDnon_medic.ConnectTo: Bad move AinvPknlMstpSnonWnonDnon_medicEnd
 )
 
 none(
 Error: Bone slot_backwpnl doesn't exist in some skeleton 
 )
 
 a podobné.
