﻿Cíl:
- zjistit, co je největším problémem výkonu serveru

Popis:
- lokální dedikovaný server
- připojím obyčejného klienta a spustím misi
- připojím 16 dedikovaných klientů (JIP)
- odpojím obyčejného klienta
- profiluji aplikaci (sleepy, AMD CodeAnalyst)
 
První výsledky (sleepy) jsou celkem překvapivé, přikládám jako přílohu: [capture.sleepy][Attachment1] 
Nejvíce výkonu zdá se stojí:
- ( QuadTreeEx<RoadList, ...>::Get ) ... 27%
- ( GetAllMissionObjects ) ... 15.5%
- ( QuadTreeExRoot<ObjectList>::Get) ... 15.5%
- ( strcmp ) ... 4.3%

[Attachment1]: https://projects.bistudio.com/Arma2/dayz/capture.sleepy
[jirka 22.6.2012 16:07:38]:
 Ten template argument v QuadTree funkcích asi nemusí být přesný, tipoval bych že to bude v obou případech ObjectList a že to bude mít spojitost s allMissionObjects funkcí. Už hledám, kde je ve skriptech použita.

[jirka 25.6.2012 7:21:11]:
 Funkce allMissionObjects (která je velmi neefektivní - prochází všechny objekty v krajině) byla použita na několika místech na serveru i klientech, Dean byl na tento problém upozorněn.
