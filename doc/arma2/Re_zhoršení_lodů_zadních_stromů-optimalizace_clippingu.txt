﻿>  Kde ve statistice vidím strom, který ovšem na obrazovce není. Připadá mi ale, že tady půjde spíš jen o hraniční případ clippingu

Našel jsem jeden problém, způsobený optimalizací, kdy OBB clipping byl nahrazen AABB (dělal Fido už kdysi dávano na základě měření zvolené testovací scény na X360). AABB clipping je výrazně rychlejší, ale někeré případy jím projdou, což ve scénách s výrazným zoomem může měřitelně vadit.

Protože souhlasím, že předchozí metoda, kdy se pro OBB testoval všech 8 rohů proti všem rovinám, je hodně neefektivní, snažím se optimalizovat OBB pomocí Axis Separation Test. Udělal jsem si dvě různé implementace AST, obě mi dávají shodné výsledky, nicméně obě se v některých případech rozcházejí s testem všech rohů. Těd zrovna zkoumám v debuggeru jeden takový případ, abych přišel na to, jak je to možné.

[Attachment1]: https://projects.bistudio.com/Arma2/fps_test_in_dense.Chernarus.zip
[Attachment2]: https://projects.bistudio.com/Arma2/test.Chernarus.zip
[ondra 7.3.2011 13:11:29]:
 Bylo to tím, že jsem mlčky předpokládal, že minmax box je symetrický, což je většinou pravda, ale ne vždy.
 
 Opravil jsem, test je nyní přesný a ještě efektivnější, než bylo to AABB (tam bylo navíc sestavování AABB z OBB).
