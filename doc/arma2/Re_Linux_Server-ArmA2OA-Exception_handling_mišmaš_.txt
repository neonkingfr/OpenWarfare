﻿Momentálně si nevím rady s následující chybou při překladu:
(
g++ -I/media/sf_linux-share/arrowhead -I/media/sf_linux-share/arrowhead/Poseidon -I. -ICloth -I/media/sf_linux-share/arrowhead/Poseidon/extern -I/media/sf_linux-share/arrowhead/Poseidon/extern/libcurl/include -m32 -msse2 -fms-extensions -fno-exceptions -fno-stack-protector -U_FORTIFY_SOURCE -D_GNU_SOURCE -g -O2 -fno-strict-aliasing -D_SUPER_RELEASE=1 -DNDEBUG -D_RELEASE=1 -D_LINUX -DNO_MMAP -std=c++0x -Wno-invalid-offsetof -DNOMINMAX=1 -D_SERVER=1 -fexceptions -c -o sr/registration.o registration.cpp
In file included from /media/sf_linux-share/arrowhead/Poseidon/Es/Strings/rString.hpp:16:0,
                 from registration.hpp:4,
                 from registration.cpp:4:
/media/sf_linux-share/arrowhead/Poseidon/Es/Memory/fastAlloc.hpp:119:39: error: declaration of ‘void operator delete(void*)’ has a different exception specifier
In file included from /media/sf_linux-share/arrowhead/Poseidon/Es/essencepch.hpp:40:0,
                 from /media/sf_linux-share/arrowhead/Poseidon/Es/Framework/debugLog.hpp:8,
                 from /media/sf_linux-share/arrowhead/Poseidon/Es/Containers/typeDefines.hpp:9,
                 from /media/sf_linux-share/arrowhead/Poseidon/Es/Containers/typeOpts.hpp:54,
                 from /media/sf_linux-share/arrowhead/Poseidon/Es/Types/pointers.hpp:15,
                 from /media/sf_linux-share/arrowhead/Poseidon/Es/Strings/rString.hpp:15,
                 from registration.hpp:4,
                 from registration.cpp:4:
/media/sf_linux-share/arrowhead/Poseidon/Es/Memory/checkMem.hpp:139:20: error: from previous declaration ‘void operator delete(void*) noexcept (true)’
make[1]: *** [sr/registration.o] Error 1
make[1]: Leaving directory `/media/sf_linux-share/arrowhead/Poseidon/lib'
make: *** [server] Error 2
)
[ondra 17.8.2012 9:17:52]:
 Nojo, registration.cpp má povolené exceptiony, zbytek projektu ne. Jelikož náš operator new ani delete nikdy nevhrnou, můžeme je taky označit jako noexcept.

[bebul 17.8.2012 14:27:36]:
 Zkoušel jsem řešit pomocí NOTHROW konstrukce, kterou máme na vícero místech v kódu, ale neúspěšně. 
 Nakonec funguje:
 (
 #if defined __GNUC__ && __EXCEPTIONS
   #define NOEXCEPTION  noexcept(true)
 #else
   #define NOEXCEPTION
 #endif
 
 void CCALL operator delete( void *ptr ) NOEXCEPTION;
 )
 
