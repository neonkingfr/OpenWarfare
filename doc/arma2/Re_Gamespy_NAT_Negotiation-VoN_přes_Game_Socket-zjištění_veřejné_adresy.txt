﻿> Nyní budu chtít začít používat adresu hry.

S překvapením zjišťuji, že zatím nejsem schopen nijak snadno zjistit veřejnou adresu hry vč. portu. Přes GameSpy zjistím jen public IP. Server zná adresu klienta vč. portu, ale pokud je klient za stejným firewallem, jako server, bude se jednat o privátní adresu.

Zatím tento problém odložím a budu vždy používat adresu, jakou zná server. Pokud to selže, bude se používat retranslace přes server, což v případě klientů, kteří jsou na stejné LAN, jako server, zas tak moc nevadí.

Možná by ale i tenhle případ stálo za to nějak ošetřit, ale zatím se mi zdá, že pokud bych to měl dělat, nějaký jiný klient, který ma odlišnou veřejnou adresu, by musel zvlášť udělat NAT traversal, a to se mi moc dělat nechce, protože by to bych se tím znovu dostal do stavu, kdy určitou dobu adresu znát nebudu.

Ani zjištění privátní adresy není úplně jednoduché. Nejsprávnější by asi bylo, aby ji poslal o sobě klient sám.

[ondra 3.8.2012 17:19:38]:
 > Ani zjištění privátní adresy není úplně jednoduché. Nejsprávnější by asi bylo, aby ji poslal o sobě klient sám.
 
 Tohle už dělám, je to jednoduché, lokální port každá session o sobě zná, posílám informaci v rámci AskConnectVoiceMessage _privateAddr a _voicePort.

[jirka 3.8.2012 17:27:57]:
 > Přes GameSpy zjistím jen public IP
 
 To je divné. Ve webovém rozhraní (např. http://gstadmin.gamespy.net/masterserver/index.aspx?gamename=arma2oapc&fields=%5Chostname%5Chostport%5Cgamever%5Ccountry%5Cmapname%5Cgametype%5Cgamemode%5Cnumplayers%5Cmaxplayers%5Cgroupid%5Cmission&overridemaster=&filter=mission%3D%27DayZ%27) je vidět i public port.

[ondra 3.8.2012 19:28:09]:
 Ano, to je pravda z GameSpy to pro server v zásadě zjistit jde - ale pouze pro servery, které GameSpy reportují, a pouze ze server browseru ze seznamu session, nikoliv voláním ServerBrowserGetMyPublicIP.
 
 Pro zjištění ze server browseru je pak určitý zádrhel - jak dostanu zrovna tu session, která patří mně? Jde filtrovat podle publicIP?
 

[ondra 3.8.2012 20:52:33]:
 Napadá mě řešení, které bude aspoň fungovat pro server: server nepotřebuje znát adresu, dokud se k němu nepřipojuje klient. Klient, když se připojuje, by měl znát veřejnou i privátní adresu, ke které se připojuje (např. ji dostane z GameSpy). Měl by tedy být schopen je při připojování poslat serveru.
 Technicky: zda se připojuji přes public nebo private se rozhoduji v `DisplayMultiplayer::JoinSession`, tam tedy ještě znám obě. Dál už předávám jen řetězec `guid`, obsahující adresu, ke které se připojuji + informaci, zda je třeba NAT traversal. Myslím, že do tohoto GUIdU můžu přidat libovolnou další informaci.

 
 Neošetří to situaci, kdy se klient připojuje jinak, než přes GameSpy, a ještě navíc po LAN, ale to se asi nedá nic dělat.
 

[ondra 3.8.2012 21:24:07]:
 > Napadá mě řešení, které bude aspoň fungovat pro server:
 
 Blbost. U serveru, který se registruje na GameSpy, je to mnohem jednodušší, ten svoji veřejnou adresu vč. portu získá během QR inicializace v rámci pa_callback volání. Privátní adresu zná od začátku.

[ondra 6.8.2012 10:52:49]:
 Ještě mě napadá: někdy se dá použít "connection reversal". Stačí, aby server znal veřejnou adresu jedné strany a spojení se pak dá navázat, pokud máme štestí na firewall.
  
 Příklad:
  
 - server zná správně veřejnou adresu G, nezná veřejnou adresu B (zná sice IP, ale nezná port)
  
 - G pošle probe B (B nedostane, G netrefil port)
 - B pošle probe G. Závisí na NATu B. Pokud je port restriktivní (a to je myslím častý případ), neprojde, protože port není pro B otevřen pro pakety přicházející z jiného portu
 - pokud prošel, G si z probe přečte veřejnou adresu B a začne ji používat (opraví číslo portu)
  
 Dokonce vidím, že už to je implementováno, stačilo na mé straně jen doplnit "odhad" portu místo dosavadní nuly, aby se G vůbec pokusil probe poslat (jako odhad používám private port - některé NATy fungují tak, že se snaží číslo portu zachovat, vidím takové chování např. u serverů v BIS).
  
 `// test whether IP:PORT of received packet differs from that set in transmit targets`
 `PeerChangedProc`
  
 
