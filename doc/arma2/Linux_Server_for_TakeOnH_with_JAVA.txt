﻿Trochu válčím s Linux Serverem pro Helisim, který už umí skriptování v JAVA, news:jjqe07$d0a$1@new-server.localdomain .

Snažím se použít již nainstalovanou JDK na f5.bistudio.com, která je v /usr/java/include.

Po mnohých peripetiích jsem dostal následující errors, které mi zatím nedávají moc smysl, ale */nejsou/* JNI specific, spíš jde o nějaké nedorozumění v /C++/.

(
g++ -I/home/bebul/helisim -I/home/bebul/helisim/Poseidon -I/usr/java/jdk1.7.0/include -I/usr/java/jdk1.7.0/include/linux 
  -march=pentium3 -fms-extensions -fno-exceptions -g -O2 -fno-strict-aliasing -D_SUPER_RELEASE=1 -DNDEBUG -D_RELEASE=1 
  -D_LINUX -DNO_MMAP -c -o sr/express.o ../Evaluator/express.cpp
../Evaluator/express.cpp: At global scope:
../Evaluator/express.cpp:290: error: too few template-parameter-lists
../Evaluator/express.cpp:291: error: too few template-parameter-lists
../Evaluator/express.cpp:292: error: too few template-parameter-lists
../Evaluator/express.cpp:293: error: too few template-parameter-lists
../Evaluator/express.cpp:294: error: too few template-parameter-lists
../Evaluator/express.cpp:295: error: too few template-parameter-lists
../Evaluator/express.cpp:296: error: too few template-parameter-lists
../Evaluator/express.cpp:297: error: too few template-parameter-lists
)

[bebul 19.3.2012 16:51:24]:
 Všiml jsem si, že některé věci stran JAVA v enginu záleží na *\#if _ENABLE_COMREF* nicméně zapnutí nemělo to na tyto errory vliv (tak jsem to znovu vypnul).

[bebul 19.3.2012 17:05:51]:
 Teď mě napadá... JAVA vlastně nešla ve Win32 zkompilovat ve VS2005, muselo se čekat na konverzi projektu na VS2010. Je dost pravděpodobné, že na linuxu používám příliš staré GCC nebo mám v Makefile vynucení nedostatečného standardu C++ ?
 Bohužel si nepamatuju přesně důvody, proč nešlo engine s JAVA přeložit ve VS2005, ale hádal bych, že budeme muset na linuxu upgradeovat GCC.

[jirka 19.3.2012 17:11:04]:
 Bylo to něco kolem variadic maker, v NG by k tomu mělo být více. Dle chybových hlášení by to souviset mohlo.

[bebul 19.3.2012 17:15:58]:
 Jo, každopádně to vyžadovalo /C++11/ takže vzhledem k tomu, že až  GCC 4.7 and later support -std=c++11 and -std=gnu++11 as well, budeme muset upgradovat GCC. Jednou to přijít muselo. Pravděpodobně pak nepřeložíme nějakou přechodnou dobu nic, ale nastal čas.
 (
 [bebul@f5 helisim]$ g++ --version
 g++ (GCC) 4.1.2 20070925 (Red Hat 4.1.2-33)
 )
 

[bebul 19.3.2012 18:06:45]:
 No, výše uvedné errory mě ještě napadlo opravit klasickým přidáním template\<\>
 (
 template<>
 CachedJavaClass ConvertTypes<&GTypeGameCode>::_cache("com/bistudio/JNIScripting/NativeObject");
 )
 což už se překládá... Uvidíme, kam až se s překladem dostanu.
[bebul 19.3.2012 18:17:12]:
 Tak teď neprojde expanze makra:
 (
 #define FUNCTIONS_ENTITY(Class, XX) \
   XX(Class, "returns the entity position", Vector3, NULL, GetPosition) \
   XX(Class, "sets the entity position", void, DUMMY, SetPosition, Vector3)
 
   JNI_REGISTER_FUNCTIONS(Entity, NO_STATIC_FUNCTIONS, FUNCTIONS_ENTITY)
 )
 což už se aspoň týká maker (nevím, zda variadic)...
  

[jirka 20.3.2012 7:21:53]:
 > (nevím, zda variadic)
 Vzhledem k různému počtu argumentů na jednotlivých řádcích je to zjevné.

[ondra 20.3.2012 9:08:14]:
 GCC by mělo variadická makra umět od verze 3, ale možná se musejí z command line nějak zapnout.

[bebul 20.3.2012 9:36:05]:
 Zkouším kompilovat s -std=c++98 ale i tak dostávám errors... Možná to jsou chyby, které nejsou způsobeny variadickými makry, ale jejich expanze se prostě GCC nelíbí, bude někde chybět template\<\> apod.
 (
 vehicle.hpp:2077: error: explicit specialization in non-namespace scope ‘class Entity’
 vehicle.hpp:2077: error: explicit specialization in non-namespace scope ‘class Entity’
 vehicle.hpp: In static member function ‘static typename JavaTypes<T>::JNIType Entity::JNI_GetPosition<R>::invoke(JNIEnv*, _jobject*, typename JavaTypes<T1>::JNIType, typename JavaTypes<T2>::JNIType, typename JavaTypes<T3>::JNIType, typename JavaTypes<T4>::JNIType)’:
 vehicle.hpp:2077: error: expected `)' before ‘JavaTypes’
 )
 Pozn: vehicle.hpp:2077 je právě ono JNI_REGISTER_FUNCTIONS(Entity, NO_STATIC_FUNCTIONS, FUNCTIONS_ENTITY)

[bebul 20.3.2012 10:46:09]:
 Po výrobě preprocessovaného výstupu na linuxu (-E -P options) a následného rozřádkování a opětovného překladu tentokrát už preprocesovaného souboru je vidno, kde dochází k problematické specializaci:
 (
   template <typename R> class JNI_GetPosition {...};
   template <> class JNI_GetPosition<void> {...}; //<-- here
 )

[bebul 20.3.2012 12:45:18]:
 Jeden erůrek opravenej, už nedostávám 
 (
 vehicle.hpp:2077: error: explicit specialization in non-namespace scope ‘class Entity’
 )
 Ale dostávám mraky dalších chyb, nejčastěji něco na tento způsob
 (
 vehicle.hpp: In static member function ‘static typename JavaTypes<T>::JNIType Entity::JNI_GetPosition<R, DummyPos>::invoke(JNIEnv*, _jobject*, typename JavaTypes<T1>::JNIType, typename JavaTypes<T2>::JNIType, typename JavaTypes<T3>::JNIType, typename JavaTypes<T4>::JNIType)’:
 vehicle.hpp:2077: error: expected `)' before ‘JavaTypes’
 )

[bebul 20.3.2012 13:33:02]:
 Jááááách! Ona tam možná jenom chybí čárka!
 (
   template <typename Dummy, typename T1, typename T2, typename T3> \
   static typename JavaTypes<R>::JNIType JNICALL invoke(JNIEnv *env, jobject obj, \
     typename JavaTypes<T1>::JNIType arg1, \
     typename JavaTypes<T2>::JNIType arg2, \
     typename JavaTypes<T3>::JNIType arg3) \
   { \
     SCOPE_FPU_ENGINE \
     R result = func(JavaTypes<T1>::ToNative(env, arg1), JavaTypes<T2>::ToNative(env, arg2) \  //<---HERE SHOULD BE COMMA
     JavaTypes<T3>::ToNative(env, arg3)); \
     return NativeToJava(env, result); \
   } \
 )

[bebul 20.3.2012 13:50:04]:
 Tohle je dobré:
 (
 #define FUNCTIONS_ENTITY(Class, XX) \
   XX(Class, "returns the entity position", Vector3, NULL, GetPosition) \
   XX(Class, "sets the entity position", void, DUMMY, SetPosition, Vector3)
 )
 co je to DUMMY není GCC jasné a ani mně ne. Nahradil jsem to *\(void\)NULL* a basta.

[bebul 20.3.2012 16:01:46]:
 Makro  
 (
   #define JNI_BRIDGE_IMPL(clstype, description, rettype, retdefault, func, ...) \
 )
 obsahuje na svém konci
 (
   template <typename Dummy> \
   static typename JavaTypes<R>::JNIType JNICALL invoke(JNIEnv *env, jobject obj) \
   { \
     clstype *nativeObj = JavaTypes<clstype>::ToNative(env, obj); \
     if (nativeObj == NULL) return retdefault; \
     SCOPE_FPU_ENGINE \
     R result = nativeObj->func(); \
     return NativeToJava(env, result); \
   } \
 )
 a to volá nativeObj->func(), v našem případě Entity::SetPosition() která neexistuje.
 Na neexistující SetPosition s template argumenty si GCC naštěstí nestěžovalo. Tento konkrétní problém jsem vyřešil implementací
 (
   void Entity::SetPosition() {}
 )
