﻿Dwarden po Skype už po 150-tý apeluje za implementaci následujícího, již 2-3x zamítnutého požadavku:

Presmerovani stazeni misi z FTP / HTTP zdroje news:gd4oqa$qoa$1@new-server.localdomain toto reseni je casto zadano jak hraci tak spravci/vlastniky serveru a tez jsem tento pozadavek zaznamenal od firem co hostuji servery podpora bzip/bzip2, 7zip (lzma/lzma2), zip komprese by byla vice nez vitana nejlepe 1.55 priorita 3
*hlasovani 130:2* http://forums.bistudio.com/showthread.php?t=71274&highlight=forwarding

Prakticky neni tyden, aby o to nekdo z komunity nepozadal, nepsal o tom nekde na forech, skype ci irc

/"Redirect to a FTP\\HTTP (fr large sized files) or auto-download within the game is something thats been around for decades now. I know those newer generations have to have this "instant gratification", hate that. But have to struggle throught forums\\websites to play a game? This isn´t the 90s anymore and BIS is losing costumers right there."/

Tak si říkám: možná by stálo za to tento povyk umlčet.
[ondra 9.2.2012 16:06:03]:
 Jako již typicky. jeden o voze, druhý o koze:
 
 > But have to struggle throught forums\websites to play a game?
 
 Tady se asi nejedná o stažení soubory mise, ten se stahuje automaticky ze serveru a nikdo ho na fóru hledat nemusí. Tady se asi jedná o synchronizaci addonů pro misi.

[maruk 13.2.2012 10:43:29]:
 Jsem ochoten vsadit kozu, že skutečný problém je chybějící "auto downloading" addonů. Automatické stahování misí je vedle toho pouhopouhý detail. Myslím, že navíc problémy při JIP už samy o sobě též nejsou tak vážné, jako bývaly. A samozřejmě hlasování o tom, zda by někdo uvítal nějakou novou feature je odsouzeno k úspěchu vždy a všude.

[ondra 13.2.2012 10:49:16]:
 Mělo by nás to spíš povzbudit k tomu, abychm svoji práci na peer to peer distribuci obsahu vzali vážně. Pokud se nám to povede, získáme technologii, která nějaké http přesměrovávání strčí hravě do kapsy.

[maruk 13.2.2012 14:28:38]:
 Nicméně pokud je snadné jim tohle udělat, samo o sobě mi to nijak nevadí, může to být efektivnější než ztrácet čas diskusemi s dwardenem. Ale opravdu skutečný problém je automatická aktualizace hry a addonů, mise jsou malé a docela bez problémové.
