﻿Při hraní CWR2 jsem narazil na to, že kampaňová mise After Montignac je strašně těžká, a to hlavně proto, že se odehrává v lese a AI tam oproti mě má hrozně navrch. Zkoušel jsem si nějaké syntetické testy, a zatímco já si typicky všimnu nehybně stojícího AI cca na 20 m, on mě asi tak na 50.

Co jsem si to zběžně procházel, zdá se mi, že funkce `SurroundTracker::Update` / `SurroundTracker::Hiding` nejsou pro tuto situaci moc dobře uzpůsobené, a to ze dvou důvodů:

- lesy na Everonu mají poměrně hustý podrost implementovaný přes clutter, ten ovšem zatím tato funkce zcela ignoruje
- les je netypický v tom, že je tam mnoho velkých objektů, které dohromady vytvářejí opticky velmi nepřehledný zmatek, protože jich je tolik, že se skoro vždy najde nějaký, který leží opticky blízko, a to i když jsou vzdálené od vojáka
[ondra 21.7.2011 16:23:30]:
 Na Hlaďasovu radu to nekomplikuji a svázal jsem to s flagem forest v Geography info. Na 50 m je nyn faktor skrytí minimálně 30 %, na 300 m 90 %. Když jsem si jako repro dal nepřátelského vojáka do lesa a blížil se k němu s AI s maximálním skilem, zdál se mi výsledek už docela věrohodný.
 
 (82969)
 
