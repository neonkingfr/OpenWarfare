﻿V přiložené misi [test.Utes.zip][Attachment1]:

- animace spuštěná v init.sqf je vzápětí přebita něčím jiným
- animace spuštěná přes trigger se přehrává trochu cukaně (problém zmizí s vypnutím interpolace)
- pokud poklesne fps, tempo přehrávání se zpomalí, přestože je na vojákovi použito [useAudioTimeForMoves](http://community.bistudio.com/wiki/useAudioTimeForMoves)

Motivace: [Laggy character animations during cutscenes at low FPS](http://dev-heaven.net/issues/22227)

V 81847 (před optimalizací kopírování při streamování) se mi zdá, že nevidím cukání, jen krátce jako přechodový jev po zpomalení času, což mi sice taky není jasné, ale není to tak hrozný problém.

V 81865 už problém vidím v plné míře.

Zkusil jsem vrátit tu část změny, kdy se místo AnimationRTPhase používá Ref<AnimationRTPhase> a vidím to dál. Mělo by to tedy být v AnimationRTBufferCopyOnWrite, ale když i to jsem nahradil triviální implementací, problém vidím dál. :(

Tak nevím, ze zoufalství jsem znovu zkusil 81847 a problém v ní teď vidím. Musel jsem předtím něco v repru dělat špatně. Možná jsem zapomněl přepnout interpolační režim na Mininal / Full?
[ondra 14.7.2011 7:55:15]:
 Hlavní problémy jsou dva:
 
 1) preload neprobíhá dost rychle, a to zejména s pomalým fps, takže v debugu je problém výraznější. To jsem snad opravil, a to částečně i na straně file serveru, což by mohlo preloady zrychlit obecně.
 2) stejný buffer používá Future i Render VS, jenže Render je pozadu. Je třeba zaručit, že zachová dost starých dat na to, aby se na ně Render dostal. Data do bufferu se zavádějí jen v simulaci, kreslení už je pak jen používá, pokud data nejsou, clampuje (nebo někdy i nějak interpoluje k okrajovým fázím).

[ondra 14.7.2011 14:03:52]:
 Problémy s přehráváním vyřešeny (82719)

[Attachment1]: https://projects.bistudio.com/Arma2/test.Utes_2.zip