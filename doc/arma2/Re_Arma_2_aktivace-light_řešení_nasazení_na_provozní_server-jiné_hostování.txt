﻿> Jirka domluví s Hlaváčem nasazení na provozní server, možná na stejný, na jakém běží a2free.

Jen zcela zběžně a orientačně jsem se zkusil podívat na možnosti hostování virtualního GlassFish serveru. Jako první jsem našel společnost nabízející hostování v USA, server s 2 GB RAM u nich stojí cca $35 Kč měsíčně.

https://www.eapps.com/applications/glassfish-hosting.php

Podobnou cenu nabízí za podobný virtuální server i

http://www.oxxus.net/java-hosting/glassfish-hosting
