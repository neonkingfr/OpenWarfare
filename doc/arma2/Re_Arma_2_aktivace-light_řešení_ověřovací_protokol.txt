﻿> Mělo by tedy být možné tímto způsobem realizovat [Re: Arma 2 aktivace / licence - vzdálené ověřování](https://projects.bistudio.com/Arma2/Re_Arma_2_aktivace__licence-vzdálené_ověřování.txt?server=https%3A%2F%2Fdev.bistudio.com%2Fprojects%2Fresources%2F&database=&id=1697)

Takto probíhá při přihlašování hráče komunikace mezi klientem a serverem:

- server dostane low-level zprávu MAGIC_CREATE_PLAYER, uloží si záznam do _createPlayers, následně provede OnServerPlayerCreate / OnPlayerCreate a založí NetworkPlayerInfo a klientovi pošle zprávu NMTPlayer/PlayerMessage
- klient založí PlayerIdentity a pošle ji serveru

PlayerMessage obsahuje (naštěstí, formát téhle zprávy se měnit nedá bez změny MP kompatibility) v současné době nepoužité _steamServerId (64b int), ve kterém můžeme klientovi poslat náhodnou challenge. Klient pošle podepsanou odpověď v rámci PlayerIdentity (NMCUpdateGeneric, zpráva typu NetworkMessageLogin). Server pak podpis ověří v NetworkServer::CreateIdentity, pokud nesouhlasí, klienta vykopne.

[ondra 25.6.2012 11:02:49]:
 Tohle všechno už teď funguje.

[ondra 26.6.2012 17:47:57]:
 Na straně klienta jsem přidal ošetření CURL a HTTP chyb (vypíší se do lokálně do chatu na systémovém kanále)
