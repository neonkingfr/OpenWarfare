﻿Nemám to nijak extra domyšlené, je to jen takový okamžitý nápad, ale momentálně máme poměrně dost větví EXE Arma 2, což je nepříjemné mj. i kvůli případné podpoře (patche).

Arma 2 Free
Arma 2 OA / RFT (to už je víceméně to samé)
Arma 2
Arma 2 Demo
Arma 2 OA Demo

to vše ještě i na Steamu.

V ideálním případě bychom mohli tím novým systémem dosáhnout stavu, kdy by všichni měli to samé exe (výhodou pro nás by bylo usnadnění podpory a také by se mohlo povést převést část uživatelů starších verzí k dokoupení nějakého obsahu).

Technicky a marketingově bychom to mohli propagovat jako nějakou novou verzi Arma 2, kterou sjednocujeme podporu produktu. Na steamu bychom ji dost možná ani nenabízeli, součástí by bylo to, že pro udržitelnost podpory produktu prostě dále držíme jen tuto jednu novou Arma 2 platformu.

Technicky by to pravděpodobně mohlo vyžadovat i nějakou podobu Arma 2 OA Lite, ale to jsmě stějně plánovali udělat kvůli dedikovanému serveru / klientu.