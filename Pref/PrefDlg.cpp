/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPrefDlg.cpp : implementation file
//

#include "stdafx.h"
#include "math.h"
#include "Pref.h"
#include "PrefObj.h"
#include "PrefDlg.h"


// use DX interfaces
//#include <d3d8.h>

#include <El/paramFile/paramFile.hpp>

#pragma comment(lib,"d3d8")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosPrefDlg dialog

CPosPrefDlg::CPosPrefDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPosPrefDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPosPrefDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	_d3d = Direct3DCreate8(D3D_SDK_VERSION);

}

CPosPrefDlg::~CPosPrefDlg()
{
}

BOOL CPosPrefDlg::OnInitDialog()
{
	SetCursor(LoadCursor(NULL, IDC_WAIT));

	CDialog::OnInitDialog();
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Inicializuji data
	ScanD3DAdapters();
	m_cDisplayDevice.ResetContent();
	int i;
	for(i=0; i<CPosPrefObject::TypeMax; i++)
	{
		m_cDisplayDevice.AddString(CPosPrefObject::HWNames[i]);
	}
	m_DisplayAdapter.ResetContent();
	for(i=0; i<m_Preferences.m_Adapters.Size(); i++)
	{
		SAdapter &ad = m_Preferences.m_Adapters[i];
		int indx = m_DisplayAdapter.AddString(ad.name);
		m_DisplayAdapter.SetItemData(indx,ad.ordinal);
		if (ad.ordinal==m_Preferences.m_adapterOrdinal)
		{
			m_DisplayAdapter.SetCurSel(indx);
		}
	}
	//for(i=0; i<CPosPrefObject::CPUMax; i++)
	//{
	//	m_cpuType.AddString(CPosPrefObject::CPUNames[i]);
	//}
	// display device - select
	int index = m_cDisplayDevice.FindString(-1,m_Preferences.m_sHwType);
	if( index >=0 ) 
		m_cDisplayDevice.SetCurSel(index);
	else
	{	// default device
		m_cDisplayDevice.SetCurSel(0);
		OnSelchangeDisplayDevice();
	};

	/*
	index = m_cpuType.FindString(-1,m_Preferences.m_sCpuType);
	if( index >=0 ) 
		m_cpuType.SetCurSel(index);
	else
	{	// default device
		m_cpuType.SetCurSel(0);
		OnSelchangeCpuType();
	};
	*/

	ScanProducts();

	// languages
	int sel = -1;
	CString strid;
	for (i=0; ;i++)
	{
		strid.Format (_T ("PREF_STR_LANGUAGE_%d"), i);
		CString str = ((CPosPrefApp*) AfxGetApp ())->PrefLocalizeString ((LPCTSTR) strid);
		if (str == "#" || str.IsEmpty ()) break;
		int index = m_cLanguage.AddString(str);
		m_cLanguage.SetItemData(index, i);
		strid.Format (_T ("PREF_STR_LANGUAGE_CFG_%d"), i);
		str = ((CPosPrefApp*) AfxGetApp ())->PrefLocalizeString ((LPCTSTR) strid);
		if (str == m_Preferences.m_sLanguage) sel = index;
	}
	if (sel >= 0) 
		m_cLanguage.SetCurSel(sel);
	else
	{	// default language
		m_cLanguage.SetCurSel(0);
		OnSelchangeLanguage();
	};

/*#if  _DEMO
	GetDlgItem(IDC_EDIT_CDKEY)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STATIC_CDKEY_TEXT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STATIC_CDKEY_PICTURE)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STATIC_CDKEY_FRAME)->ShowWindow(SW_HIDE);
#endif*/


	// update zobrazen� dat
	UpdateAll();

	((CPosPrefApp*) AfxGetApp ())->ApplyApplicationNameAndStringTable (this);
	return TRUE;  
}

void CPosPrefDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPosPrefDlg)
	DDX_Control(pDX, IDC_PRODUCT, m_cProducts);
	DDX_Control(pDX, IDC_DISPLAY_ADAPTER, m_DisplayAdapter);
	DDX_Control(pDX, IDC_LANGUAGE, m_cLanguage);
	DDX_Control(pDX, IDC_RESOLUTION, m_cResolution);
	DDX_Control(pDX, IDC_DISPLAY_DEVICE, m_cDisplayDevice);
	//}}AFX_DATA_MAP

	if (SAVEDATA)
	{
		// display device
		// p�b�n� (po zm�n�)

		// display resolution
		{
			int nIndx = m_cResolution.GetCurSel();
			if (nIndx >= 0)
			{
				int nResID = (int)m_cResolution.GetItemData(nIndx);
				m_Preferences.SetResolution(nResID);
			}
		}

	}
}

BEGIN_MESSAGE_MAP(CPosPrefDlg, CDialog)
	//{{AFX_MSG_MAP(CPosPrefDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_MAIN_PLAY, OnMainPlay)
	ON_BN_CLICKED(IDC_MAIN_AUTODETECT, OnMainAutodetect)
	ON_BN_CLICKED(IDC_MAIN_BENCHMARK, OnMainBenchmark)
	ON_BN_CLICKED(IDC_MAIN_ADVANCED, OnMainAdvanced)
	ON_CBN_SELCHANGE(IDC_DISPLAY_DEVICE, OnSelchangeDisplayDevice)
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(IDC_CPU_TYPE, OnSelchangeCpuType)
	ON_BN_CLICKED(IDC_AUDIO_SETUP, OnAudioSetup)
	ON_CBN_SELCHANGE(IDC_LANGUAGE, OnSelchangeLanguage)
	ON_CBN_SELCHANGE(IDC_DISPLAY_ADAPTER, OnSelchangeDisplayAdapter)
	ON_CBN_SELCHANGE(IDC_PRODUCT, OnSelchangeProduct)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CPosPrefDlg::UpdatePerformance()
{
	CString bench;
	bench.Format("%d",m_Preferences.m_benchmark);
	::SetWindowText(DLGCTRL(IDC_ESTIM_PERFORMANCE),m_Preferences.m_sPerform);
	::SetWindowText(DLGCTRL(IDC_BENCHMARK_RESULT),	bench);

	// geometry performance
	//::SetWindowText(DLGCTRL(IDC_GEOMETRY_PERFORMANCE),m_Preferences.m_sDefPerform);
}

// encode / decode resolution to DWORD

void CPosPrefDlg::UpdateAll()
{
	// zobrazen� READ-ONLY text�
	::SetWindowText(DLGCTRL(IDC_CPU),m_Preferences.m_sCpu);
	::SetWindowText(DLGCTRL(IDC_RAM),m_Preferences.m_sRam);

	UpdatePerformance();

	// language
	int index = m_cLanguage.FindString(-1,m_Preferences.m_sLanguage);
	if (index >=0) m_cLanguage.SetCurSel(index);

	// display device - select
	index = m_cDisplayDevice.FindString(-1,m_Preferences.m_sHwType);
	if( index >=0 ) m_cDisplayDevice.SetCurSel(index);

	//index = m_cpuType.FindString(-1,m_Preferences.m_sCpuType);
	//if( index >=0 ) m_cpuType.SetCurSel(index);
	// display device - info
	bool b3Dfx = false;
	if( !strcmpi(m_Preferences.m_sHwType,CPosPrefObject::HWNames[CPosPrefObject::Type3Dfx]) )
	{
		b3Dfx = true;
	}

	ScanResolutions(b3Dfx);
	// access to combo box
	//m_DisplayAdapter.EnableWindow(!b3Dfx);
	m_DisplayAdapter.ShowWindow(b3Dfx ? SW_HIDE : SW_SHOW);
	

	// resolution
	int nCur = m_Preferences.GetResolutionID(m_Preferences.m_sResolW,m_Preferences.m_sResolH,m_Preferences.m_sResolBpp);
	int nSel = -1;
	// update m_Preferences.m_Resolutions list
	int i;
	m_DisplayAdapter.ResetContent();
	for(i=0; i<m_Preferences.m_Adapters.Size(); i++)
	{
		SAdapter &ad = m_Preferences.m_Adapters[i];
		int indx = m_DisplayAdapter.AddString(ad.name);
		m_DisplayAdapter.SetItemData(indx,ad.ordinal);
		if (ad.ordinal==m_Preferences.m_adapterOrdinal)
		{
			nSel = i;
		}
	}
	if (nSel >= 0)
		m_DisplayAdapter.SetCurSel(nSel);
	else
		m_DisplayAdapter.SetCurSel(0);
	// glide
	m_cResolution.ResetContent();
	for(i=0; i<m_Preferences.m_Resolutions.Size(); i++)
	{
		int nIndx = m_cResolution.AddString(m_Preferences.m_Resolutions[i].pText);
		m_cResolution.SetItemData(nIndx,(DWORD)i);
		if (nCur == i)
			nSel = nIndx;
	}
	if (nSel >= 0)
		m_cResolution.SetCurSel(nSel);
	else
		m_cResolution.SetCurSel(0);

	ScanMaxTextureSize(b3Dfx);
};

/////////////////////////////////////////////////////////////////////////////
// CPosPrefDlg message handlers

void CPosPrefDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPosPrefDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


/////////////////////////////////////////////////////////////////////////////
// MAIN ACTIONS

void CPosPrefDlg::OnSelchangeDisplayDevice() 
{
	m_cDisplayDevice.GetWindowText(m_Preferences.m_sHwType);
	// default hodnoty
	m_Preferences.DefaultValues(FALSE,TRUE);
	// zobrazen� hodnot
	// get resolution list

	UpdateAll();	
}

void CPosPrefDlg::OnMainPlay() 
{
	if (UpdateData(TRUE))
		EndDialog(IDC_MAIN_PLAY);
}

void CPosPrefDlg::OnMainAutodetect() 
{
	m_Preferences.Benchmark();
	// default hodnoty
	m_Preferences.DefaultValues(TRUE,TRUE);
	// zobrazen� hodnot
  UpdateAll();
}

void CPosPrefDlg::OnMainBenchmark() 
{
}

extern BOOL DoEditAdvancedPerformance(CPosPrefObject*);
void CPosPrefDlg::OnMainAdvanced() 
{	
	if (DoEditAdvancedPerformance(&m_Preferences))
	{
		// zobrazen� hodnot
//		UpdateAll();
		UpdatePerformance();
	}
}

/////////////////////////////////////////////////////////////////////////////
// SLIDERS

void CPosPrefDlg::UpdateBasic()
{
   m_Preferences.DefaultGeometry();
//    UpdateAll();
	// UpdateAll nen� t�eba -> m�n�m v Advanced
};

void CPosPrefDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CPosPrefDlg::OnSelchangeCpuType() 
{
	//m_cpuType.GetWindowText(m_Preferences.m_sCpuType);
	/*
	// default hodnoty
	m_Preferences.DefaultValues(FALSE,TRUE);
	// zobrazen� hodnot
	UpdateAll();		
	*/
}

void CPosPrefDlg::OnSelchangeLanguage() 
{
	int sel = m_cLanguage.GetCurSel();
	if (sel < 0) return;
	CString strid;
	strid.Format (_T ("PREF_STR_LANGUAGE_CFG_%d"), m_cLanguage.GetItemData(sel));
	CString str = ((CPosPrefApp*) AfxGetApp ())->PrefLocalizeString ((LPCTSTR) strid);
	m_Preferences.m_sLanguage = str;
}

#if _MSC_VER<1300
  typedef DWORD *DWORD_PTR;
#endif

#include <dvoice.h>
#define SAFE_RELEASE(p)							{ if(p) {(p)->Release(); (p) = NULL;} }

void CPosPrefDlg::OnAudioSetup() 
{
	CoInitialize(NULL);
	IDirectPlayVoiceTest *setup;
	HRESULT hr = CoCreateInstance
	(
		CLSID_DirectPlayVoiceTest, NULL, 
    CLSCTX_ALL, IID_IDirectPlayVoiceTest,
		(VOID**)&setup
	);
	if (!FAILED(hr))
	{
		setup->CheckAudioSetup(NULL, NULL, NULL, 0);
	}
	SAFE_RELEASE(setup);
	CoUninitialize();
}

const static SResolution GlideResolutions[] =
{
	SResolution( 640,  480, 16),
	SResolution( 800,  600, 16),
	SResolution( 1024,  768, 16),
	SResolution( 1280, 1024, 16),
	SResolution( 1600, 1200, 16),
};


void CPosPrefDlg::ScanResolutions(bool glide)
{
	// depending on current selection, scan D3D or Glide resolutions
	if (glide)
	{
		m_Preferences.m_Resolutions.Copy
		(
			GlideResolutions,sizeof(GlideResolutions)/sizeof(*GlideResolutions)
		);
	}
	else
	{
		ScanD3DResolutions();
	}
}

bool SResolution::operator == (const SResolution &w) const
{
	return w.nHeight==nHeight && w.nWidth==nWidth && w.nBPP==nBPP;
}


SResolution D3DResolutions[] =
{
	SResolution( 640,  480, 16),
	SResolution( 640,  480, 32),
	SResolution( 800,  600, 16),
	SResolution( 800,  600, 32),
	SResolution(1024,  768, 16),
	SResolution(1024,  768, 32),
	SResolution(1280, 1024, 16),
	SResolution(1280, 1024, 32),
	SResolution(1600, 1200, 16),
	SResolution(1600, 1200, 32),
};


void CPosPrefDlg::ScanD3DAdapters()
{
	m_Preferences.m_Adapters.Clear();
	if (_d3d) for (UINT a = 0; a<_d3d->GetAdapterCount(); a++)
	{
		D3DADAPTER_IDENTIFIER8 id;
		_d3d->GetAdapterIdentifier(a,D3DENUM_NO_WHQL_LEVEL,&id);
		SAdapter ad;
		ad.ordinal = a;
		ad.name = id.Description;
		m_Preferences.m_Adapters.Add(ad);
	}
}

void CPosPrefDlg::ScanD3DResolutions()
{
	// enumerate devices
	m_Preferences.m_Resolutions.Clear();
	UINT adapter = m_Preferences.m_adapterOrdinal;
	if (_d3d) for (UINT mode = 0; mode<_d3d->GetAdapterModeCount(adapter); mode++)
	{
		D3DDISPLAYMODE dmode;
		_d3d->EnumAdapterModes(adapter,mode,&dmode);
		int bpp = 16;
		switch (dmode.Format)
		{
			case D3DFMT_R8G8B8: bpp = 24; break;
			case D3DFMT_A8R8G8B8:
			case D3DFMT_X8R8G8B8: bpp = 32; break;
			case D3DFMT_R3G3B2: bpp = 8; break;
		}
		SResolution res(dmode.Width,dmode.Height,bpp);
		m_Preferences.m_Resolutions.AddUnique(res);

	}
}

void CPosPrefDlg::ScanMaxTextureSize(bool glide)
{
	if (glide)
	{
		m_Preferences.m_MaxTextureSize = 256;
	}
	else if (_d3d) 
	{
		UINT adapter = m_Preferences.m_adapterOrdinal;
		D3DCAPS8 caps;
		_d3d->GetDeviceCaps(adapter, D3DDEVTYPE_HAL, &caps);
		m_Preferences.m_MaxTextureSize = min(caps.MaxTextureHeight, caps.MaxTextureWidth);
	}
	else
	{
		m_Preferences.m_MaxTextureSize = 4096;
	}
}

void CPosPrefDlg::OnSelchangeDisplayAdapter() 
{
	int sel = m_DisplayAdapter.GetCurSel();
	UINT ordinal = m_DisplayAdapter.GetItemData(sel);
	m_Preferences.m_adapterOrdinal = ordinal;
	// get resolution list
	ScanD3DResolutions();
	m_Preferences.DefaultValues(FALSE,TRUE);
	// zobrazen� hodnot

	UpdateAll();	
}

void CPosPrefDlg::ScanProducts()
{
	ParamFile cfg;
	cfg.Parse((LPCTSTR) ((((CPosPrefApp*) AfxGetApp ())->_modulePath) + _T ("products.cfg")));

	int n = cfg.GetEntryCount();
	_products.Resize(n);
	m_cProducts.Clear();

	int sel = 0;
	for (int i=0; i<n; i++)
	{
		ParamEntryVal entry = cfg.GetEntry(i);

		_products[i].className = entry.GetName();
		_products[i].name = entry >> "name";
		_products[i].application = entry >> "application";
    _products[i].cfgFile = entry >> "cfgFile";

		ConstParamEntryPtr arguments = entry.FindEntry("arguments");
		if (arguments) _products[i].arguments = *arguments;

		m_cProducts.AddString(_products[i].name);

		if (stricmp(_products[i].className, m_Preferences._product.className) == 0)
			sel = i;
	}

	if (n > 0)
	{
		m_Preferences._product = _products[sel];
		m_cProducts.SetCurSel(sel);
	}
}

void CPosPrefDlg::OnSelchangeProduct() 
{
	int sel = m_cProducts.GetCurSel();
	if (sel >= 0) m_Preferences._product = _products[sel];
}
