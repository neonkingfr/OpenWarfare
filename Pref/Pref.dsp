# Microsoft Developer Studio Project File - Name="Pref" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 60000
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Pref - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Pref.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Pref.mak" CFG="Pref - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Pref - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Pref - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "Pref - Win32 Demo Version" (based on "Win32 (x86) Application")
!MESSAGE "Pref - Win32 Czech Release" (based on "Win32 (x86) Application")
!MESSAGE "Pref - Win32 MP Demo" (based on "Win32 (x86) Application")
!MESSAGE "Pref - Win32 VBS1" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Pref", QQBAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Pref - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /Zi /O2 /I "W:\c" /D _RELEASE=1 /D "NDEBUG" /D NCZECH=1 /D "WIN32" /D "_WINDOWS" /D MFC_NEW=1 /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "NDEBUG" /d NCZECH=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 winmm.lib dxguid.lib /nologo /subsystem:windows /debug /machine:I386 /out:"o:\StatusQuo\OpFlashPreferences.exe"

!ELSEIF  "$(CFG)" == "Pref - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GR /GX /ZI /Od /I "W:\c" /D "_DEBUG" /D NCZECH=1 /D "WIN32" /D "_WINDOWS" /D MFC_NEW=1 /YX"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "_DEBUG" /d NCZECH=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 winmm.lib dxguid.lib /nologo /subsystem:windows /debug /machine:I386 /out:"Debug\OpFlashPreferences.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "Pref - Win32 Demo Version"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Pref___Win32_Demo_Version"
# PROP BASE Intermediate_Dir "Pref___Win32_Demo_Version"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Demo"
# PROP Intermediate_Dir "Demo"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /Zi /O2 /I "W:\c" /D "NDEBUG" /D _DEMO=1 /D NCZECH=1 /D "WIN32" /D "_WINDOWS" /D MFC_NEW=1 /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG" /d NCZECH=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 winmm.lib /nologo /subsystem:windows /debug /machine:I386 /out:"o:\StatusQuo\OperationFlashpoint.exe"
# ADD LINK32 winmm.lib dxguid.lib /nologo /subsystem:windows /debug /machine:I386 /out:"o:\FPDemo\OpFlashPreferencesDemo.exe"

!ELSEIF  "$(CFG)" == "Pref - Win32 Czech Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Pref___Win32_Czech_Release"
# PROP BASE Intermediate_Dir "Pref___Win32_Czech_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CzechRelease"
# PROP Intermediate_Dir "CzechRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D NCZECH=1 /YX"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /Zi /O2 /I "W:\c" /D "NDEBUG" /D "_RELEASE" /D _CZECH=1 /D "WIN32" /D "_WINDOWS" /D MFC_NEW=1 /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG" /d NCZECH=1
# ADD RSC /l 0x405 /d "NDEBUG" /d _CZECH=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 winmm.lib dxguid.lib /nologo /subsystem:windows /debug /machine:I386 /out:"o:\StatusQuo\OpFlashPreferences.exe"
# ADD LINK32 winmm.lib dxguid.lib /nologo /subsystem:windows /debug /machine:I386 /out:"O:\FPCzechSuperRelease\FlashpointPreferences.exe"

!ELSEIF  "$(CFG)" == "Pref - Win32 MP Demo"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Pref___Win32_MP_Demo"
# PROP BASE Intermediate_Dir "Pref___Win32_MP_Demo"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "MPDemo"
# PROP Intermediate_Dir "MPDemo"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D _DEMO=1 /D NCZECH=1 /YX"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /Zi /O2 /I "W:\c" /D "NDEBUG" /D _MP_DEMO=1 /D NCZECH=1 /D "WIN32" /D "_WINDOWS" /D MFC_NEW=1 /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG" /d NCZECH=1
# ADD RSC /l 0x405 /d "NDEBUG" /d NCZECH=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 winmm.lib dxguid.lib /nologo /subsystem:windows /debug /machine:I386 /out:"o:\FPDemo\OpFlashPreferencesDemo.exe"
# ADD LINK32 winmm.lib dxguid.lib /nologo /subsystem:windows /debug /machine:I386 /out:"o:\MPDemo\OpFlashPreferencesMPDemo.exe"

!ELSEIF  "$(CFG)" == "Pref - Win32 VBS1"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Pref___Win32_VBS1"
# PROP BASE Intermediate_Dir "Pref___Win32_VBS1"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "VBS1"
# PROP Intermediate_Dir "VBS1"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D NCZECH=1 /YX"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /Zi /O2 /I "W:\c" /D "NDEBUG" /D _RELEASE=1 /D NCZECH=1 /D _VBS1=1 /D "WIN32" /D "_WINDOWS" /D MFC_NEW=1 /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG" /d NCZECH=1
# ADD RSC /l 0x405 /d "NDEBUG" /d NCZECH=1 /d _VBS1=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 winmm.lib dxguid.lib /nologo /subsystem:windows /debug /machine:I386 /out:"o:\StatusQuo\OpFlashPreferences.exe"
# ADD LINK32 winmm.lib dxguid.lib /nologo /subsystem:windows /debug /machine:I386 /out:"o:\VBS1\VBS1Preferences.exe"

!ENDIF 

# Begin Target

# Name "Pref - Win32 Release"
# Name "Pref - Win32 Debug"
# Name "Pref - Win32 Demo Version"
# Name "Pref - Win32 Czech Release"
# Name "Pref - Win32 MP Demo"
# Name "Pref - Win32 VBS1"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AdvancedPagesDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PosFuncs.cpp
# End Source File
# Begin Source File

SOURCE=.\Pref.cpp
# End Source File
# Begin Source File

SOURCE=.\Pref.rc
# End Source File
# Begin Source File

SOURCE=.\PrefDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrefErr.cpp
# End Source File
# Begin Source File

SOURCE=.\PrefObj.cpp
# End Source File
# Begin Source File

SOURCE=.\PropBeta.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Pref.h
# End Source File
# Begin Source File

SOURCE=.\PrefDlg.h
# End Source File
# Begin Source File

SOURCE=.\PrefObj.h
# End Source File
# Begin Source File

SOURCE=.\PropBeta.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\wpch.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\_system.ico
# End Source File
# Begin Source File

SOURCE=.\Res\cdkey.ico
# End Source File
# Begin Source File

SOURCE=.\res\display.ico
# End Source File
# Begin Source File

SOURCE=.\Res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\Pref.rc2
# End Source File
# Begin Source File

SOURCE=.\res\vbs1.ico
# End Source File
# End Group
# Begin Group "Essence"

# PROP Default_Filter ""
# Begin Group "Containers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\Es\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\bankArray.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Containers\cachelist.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Containers\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Containers\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Containers\list.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\listBidir.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Containers\rStringArray.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Containers\smallArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\staticArray.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Containers\typeOpts.hpp
# End Source File
# End Group
# Begin Group "Framework"

# PROP Default_Filter ""
# Begin Source File

SOURCE=W:\c\Es\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Framework\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Framework\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Framework\useAppFrameDefault.cpp
# End Source File
# End Group
# Begin Group "Memory"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\Es\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\debugAlloc.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\debugAlloc.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Memory\fastAlloc.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Memory\normalNew.hpp
# End Source File
# End Group
# Begin Group "Types"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\Es\Types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Types\lLinks.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Types\lLinks.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Types\memtype.h
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\softLinks.hpp
# End Source File
# End Group
# Begin Group "Common"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\Es\Common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\langExt.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Common\win.h
# End Source File
# End Group
# Begin Group "Strings"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\Es\Strings\bstring.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Strings\rString.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\Strings\rString.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=w:\c\Es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Files\filenames.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\Es\platform.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Algorithms\qsort.hpp
# End Source File
# End Group
# Begin Group "Element"

# PROP Default_Filter ""
# Begin Group "QStream"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\El\QStream\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\qbstream.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\qstream.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\qstreamUseBankDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\qstreamUseFServerDefault.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\serializeBin.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\QStream\ssCompress.cpp
# End Source File
# End Group
# Begin Group "ParamFile"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\El\ParamFile\paramFile.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\ParamFile\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileDecl.hpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileUseEvalDefault.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\ParamFile\paramFileUseLocalizeStringtable.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\ParamFile\paramFileUsePreprocC.cpp
# End Source File
# End Group
# Begin Group "Stringtable"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\El\Stringtable\localizeStringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Stringtable\localizeStringtable.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\Stringtable\stringtable.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\Stringtable\stringtable.hpp
# End Source File
# End Group
# Begin Group "Math"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\El\Math\math3d.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\Math\math3dP.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\Math\math3dP.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\Math\mathDefs.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\Math\mathOpt.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\Math\mathOpt.hpp
# End Source File
# End Group
# Begin Group "PCH"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\El\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\PCH\normalConfig.h
# End Source File
# Begin Source File

SOURCE=w:\c\El\PCH\stdIncludes.h
# End Source File
# End Group
# Begin Group "PreprocC"

# PROP Default_Filter ""
# Begin Source File

SOURCE=w:\c\El\PreprocC\Preproc.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\PreprocC\Preproc.h
# End Source File
# Begin Source File

SOURCE=w:\c\El\PreprocC\preprocC.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\PreprocC\preprocC.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Stringtable\stringtableUsePreprocDefault.cpp
# End Source File
# End Group
# Begin Group "Interfaces"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Interfaces\iEval.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Interfaces\iLocalize.hpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\Interfaces\iPreproc.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\iQFBank.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Interfaces\iSumCalc.hpp
# End Source File
# End Group
# Begin Group "FreeOnDemand"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\FreeOnDemand\memFreeReq.cpp
# End Source File
# Begin Source File

SOURCE=..\El\FreeOnDemand\memFreeReqUseDefault.cpp
# End Source File
# End Group
# Begin Group "ElCommon"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Common\perfLog.cpp
# End Source File
# Begin Source File

SOURCE=w:\c\El\Common\perfLog.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\perfProf.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=w:\c\El\elementpch.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
