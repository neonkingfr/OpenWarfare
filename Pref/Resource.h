//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Pref.rc
//
#define IDD_POSPREF_DIALOG              102
#define IDD_PROP_PERFORMANCE            106
#define IDD_PROP_EFFECTS                107
#define IDD_PROP_AVANCED                108
#define IDD_PROP_TEXTURES               109
#define IDD_PROP_SOUND                  110
#define IDR_MAINFRAME                   128
#define IDD_POSPREF_CONTROLS            132
#define IDD_GET_KEY                     133
#define IDD_SEND_ERRFILE                134
#define IDD_PROP_BETA                   135
#define IDC_MAIN_PLAY                   1000
#define IDS_ADVANCED_DLG                1000
#define IDI_SYSTEM                      1000
#define IDC_MAIN_AUTODETECT             1001
#define IDS_BTN_CANCEL                  1001
#define IDI_DISPLAY                     1001
#define IDC_MAIN_BENCHMARK              1002
#define IDS_BTN_OK                      1002
#define IDI_CHIP                        1002
#define IDC_MAIN_ADVANCED               1003
#define IDI_CDKEY                       1003
#define IDS_NO_CD_KEY                   1003
#define IDC_CPU                         1004
#define IDC_RAM                         1005
#define IDC_ESTIM_PERFORMANCE           1006
#define IDC_BENCHMARK_RESULT            1007
#define IDC_GEOMETRY_PERFORMANCE        1008
#define IDC_DISPLAY_DEVICE              1009
#define IDC_DISPLAY_INFO                1010
#define IDC_DISPLAY_ADAPTER             1010
#define IDC_RESOLUTION                  1011
#define IDC_SLIDER_VISIBILITY           1012
#define IDC_CPU_TYPE                    1012
#define IDC_SLIDER_SPEED                1013
#define IDC_SHOW_TOTAL                  1013
#define IDC_SLIDER_QUALITY              1014
#define IDC_SHOW_TEXTURE                1014
#define IDC_SHOW_OBJECTS                1014
#define IDC_SHOW_FILE                   1015
#define IDC_SHOW_SHADOWS                1015
#define IDC_MAIN_CONTROLS               1015
#define IDC_SHOW_GEOMETRY               1016
#define IDC_SHOW_MAXOBJECTS             1016
#define IDC_SLIDER_TOTAL                1017
#define IDC_SLIDER_HEAP                 1018
#define IDC_SHOW_LIGHTS                 1018
#define IDC_SLIDER_OBJECTS              1018
#define IDC_SLIDER_FILE                 1019
#define IDC_SLIDER_LIGHTS               1019
#define IDC_SLIDER_SHADOWS              1019
#define IDC_SLIDER_GEOMETRY             1020
#define IDC_CHECK_BACKGROUND            1020
#define IDC_SLIDER_MAXOBJECTS           1020
#define IDC_CHECK_REFLECTION            1021
#define IDC_SHOW_MAXSHADOWS             1021
#define IDC_CHECK_OBJSHADOWS            1022
#define IDC_SLIDER_MAXSHADOWS           1022
#define IDC_ADD_EXPLOSIONS              1023
#define IDC_SHOW_LOD                    1023
#define IDC_ADD_MISSILES                1024
#define IDC_SLIDER_LOD                  1024
#define IDC_ADD_STATIC                  1025
#define IDC_SHOW_LODOBJECT              1025
#define IDC_CHECK_INGAME                1026
#define IDC_SLIDER_LODOBJECT            1026
#define IDC_CHECK_VEHSHADOWS            1026
#define IDC_SHOW_CHANNELS               1027
#define IDC_SHOW_LODSAHDOWS             1027
#define IDC_CHECK_CLOUDLETS             1027
#define IDC_SLIDER_CHANNELS             1028
#define IDC_SLIDER_LODSAHDOWS           1028
#define IDC_COCPITS                     1029
#define IDC_OBJECTS                     1030
#define IDC_SHOW_LANDSCAPE              1030
#define IDC_LANDSCAPE                   1031
#define IDC_SLIDER_LANDSCAPE            1031
#define IDC_EFFECTS                     1032
#define IDC_EDIT_CDKEY                  1032
#define IDC_DROP                        1033
#define IDC_JOYSTICK_ENABLED            1033
#define IDC_LIST                        1034
#define IDC_CHANGE                      1035
#define IDC_DEFAULT                     1036
#define IDC_BUTTON1                     1036
#define IDC_AUDIO_SETUP                 1036
#define IDC_STATIC_CDKEY_TEXT           1037
#define IDC_STATIC_CDKEY_PICTURE        1038
#define IDC_STATIC_CDKEY_FRAME          1039
#define IDC_LANGUAGE                    1040
#define IDC_PRODUCT                     1042

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1043
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
