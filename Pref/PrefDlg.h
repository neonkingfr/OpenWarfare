/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPrefDlg.h : header file


/////////////////////////////////////////////////////////////////////////////
// CPosPrefDlg dialog

#include <d3d8.h>

class CPosPrefDlg : public CDialog
{
// Construction
public:
	void ScanD3DAdapters();
	void ScanResolutions(bool glide);
	void ScanMaxTextureSize(bool glide);
	CPosPrefDlg(CWnd* pParent = NULL);	// standard constructor
	~CPosPrefDlg();	// standard constructor

// Dialog Data
	CPosPrefObject		m_Preferences;
	ComRef<IDirect3D8> _d3d;

	AutoArray<ProductInfo> _products;

	//{{AFX_DATA(CPosPrefDlg)
	enum { IDD = IDD_POSPREF_DIALOG };
	CComboBox	m_cProducts;
	CComboBox	m_DisplayAdapter;
	CComboBox	m_cLanguage;
	CComboBox	m_cResolution;
	CComboBox	m_cDisplayDevice;
	//}}AFX_DATA

			void UpdateAll();
			void UpdatePerformance();
			void UpdateBasic();

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPosPrefDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void ScanD3DResolutions();
	void ScanProducts();
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CPosPrefDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnMainPlay();
	afx_msg void OnMainAutodetect();
	afx_msg void OnMainBenchmark();
	afx_msg void OnMainAdvanced();
	afx_msg void OnSelchangeDisplayDevice();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSelchangeCpuType();
	afx_msg void OnAudioSetup();
	afx_msg void OnSelchangeLanguage();
	afx_msg void OnSelchangeDisplayAdapter();
	afx_msg void OnSelchangeProduct();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

