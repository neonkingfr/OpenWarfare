/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPrefErr.cpp : implementation file
//

#include "stdafx.h"
#include "Pref.h"
#include "PrefObj.h"
#include "io.h"
#include "mapi.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Text
#if 0 // not used 

#if _VBS1
#define ERROR_FILE_NAME	"VBS1.rpt"
#define MSG_SUBJECT			"VBS1 Bug Report"

#define MSG_TO_ALIAS		"VBS1 Developers"
#define MSG_TO_ADRESS		"vyvoj@bohemiainteractive.cz"
#else
#define ERROR_FILE_NAME	"Flashpoint.rpt"
#define MSG_SUBJECT			"Flashpoint Bug Report"

#define MSG_TO_ALIAS		"Flashpoint Developers"
#define MSG_TO_ADRESS		"vyvoj@bohemiainteractive.cz"
#endif

static char *MSG_TEXT=
	"If you can remmember what happened before crash, "
	"it would greatly help us catching the bug.\n\n"
	"Please add anything you consider important.\n"
	"============================================================\n"
	"\n"
	"\n"
	"\n"
	"\n"
	"============================================================\n"
;


/////////////////////////////////////////////////////////////////////////////
// CSendMailDlg dialog

class CSendMailDlg : public CDialog
{
// Construction
public:
	CSendMailDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSendMailDlg)
	enum { IDD = IDD_SEND_ERRFILE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSendMailDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSendMailDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CSendMailDlg::CSendMailDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSendMailDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSendMailDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSendMailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSendMailDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSendMailDlg, CDialog)
	//{{AFX_MSG_MAP(CSendMailDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// DoSendErrorFile()

typedef ULONG FAR PASCAL MAPISaveMailFnc(
LHANDLE lhSession, 
ULONG ulUIParam, 
lpMapiMessage lpMessage, 
FLAGS flFlags, 
ULONG ulReserved, 
LPTSTR lpszMessageID 
);
 

static BOOL DoSendMailFile(LPTSTR pFile,LPTSTR pCfg,LPTSTR pSave)
{
	BOOL bSuccess = FALSE;
	char MsgID[1024];

	MapiRecipDesc MapiRecp;
	memset(&MapiRecp,0,sizeof(MapiRecp));
	MapiRecp.ulRecipClass = MAPI_TO;
	MapiRecp.lpszName   = MSG_TO_ALIAS;
	MapiRecp.lpszAddress = MSG_TO_ADRESS;

	MapiFileDesc MapiFile[3];
	memset(MapiFile,0,sizeof(MapiFile));

	int i=0;
	if (_access(pFile,00)==0)
	{
		MapiFile[i].nPosition = 0xFFFFFFFF;
		MapiFile[i].lpszPathName = pFile;
		i++;
	}
	if (_access(pCfg,00)==0)
	{
		MapiFile[i].nPosition = 0xFFFFFFFF;
		MapiFile[i].lpszPathName = pCfg;
		i++;
	}
	if( pSave && _access(pSave,00)==0)
	{
		MapiFile[i].nPosition = 0xFFFFFFFF;
		MapiFile[i].lpszPathName = pSave;
		i++;
	}


	MapiMessage  MapiMsg;
	memset(&MapiMsg,0,sizeof(MapiMsg));
	MapiMsg.lpszSubject  = MSG_SUBJECT;
	MapiMsg.lpszNoteText = MSG_TEXT;
	MapiMsg.nRecipCount  = 1;
	MapiMsg.lpRecips		 = &MapiRecp;
	MapiMsg.nFileCount	 = i;
	MapiMsg.lpFiles			 = MapiFile;
	
	HINSTANCE hLib = LoadLibrary("mapi32.dll");
	if (hLib == NULL)
	{
		AfxMessageBox("MAPI interface not fond",MB_OK|MB_ICONSTOP);
		return bSuccess;
	};

	MAPISaveMailFnc* pMAPISendMail = (MAPISaveMailFnc*)GetProcAddress(hLib, "MAPISendMail"); 
	try
	{
		if (pMAPISendMail)
		{
			char oldDirectory[1024];
			::GetCurrentDirectory(sizeof(oldDirectory), oldDirectory);
			ULONG nRes = (*pMAPISendMail)(0,0,&MapiMsg,MAPI_DIALOG|MAPI_LOGON_UI|MAPI_NEW_SESSION,0,MsgID);
			bSuccess = (nRes == SUCCESS_SUCCESS); 
			::SetCurrentDirectory(oldDirectory);
		}
		FreeLibrary(hLib); 
	}
	catch( ... )
	{
		AfxMessageBox("MAPI Error",MB_OK);
	}
	return bSuccess;
};

void DoSendErrorFile(LPTSTR pCfg)
{
	char File[300];
	//char Save[300];
	::GetCurrentDirectory(sizeof(File),File);
	
	int nPos = strlen(File);
	if( nPos>0 && File[nPos-1]!='\\' ) strcat(File,"\\");
	
	// pridam hledany soubor
	//strcpy(Save,File);
	strcat(File,ERROR_FILE_NAME);
	// existuje soubor ?
	if (_access(File,00)!=0)
		return;
	
	CSendMailDlg dlg;
	if (dlg.DoModal() == IDOK)
	{
		// ode�lu soubor
		BOOL bSuccess = DoSendMailFile(File,pCfg,NULL);
		// sma�u soubor
		if (bSuccess)
		{
		CFile::Remove(File);
		//CFile::Remove(Save);
		}
	};
};

#endif