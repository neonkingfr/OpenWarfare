// enikItem.h: interface for the CenikItem class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENIKITEM_H__175CA569_A8E0_45B7_9B4F_F8ED717412EB__INCLUDED_)
#define AFX_ENIKITEM_H__175CA569_A8E0_45B7_9B4F_F8ED717412EB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ES\Containers\array.hpp>

class CenikItem  
{
public:
  char *id;
  char *name;
  int hmotnost;
  float cena;
  float cenadph;
public:
	CenikItem();
	CenikItem(const CenikItem &other);
	CenikItem &operator=(const CenikItem &other);
	virtual ~CenikItem();

	void SetID(const char *new_id)
	  {
	  if (id!=new_id) {free(id);id=strdup(new_id);}
	  }

	void SetName(const char *new_name)
	  {
	  if (name!=new_name) {free(name);name=strdup(new_name);}
	  }

};

TypeIsMovable(CenikItem);

typedef AutoArray<CenikItem> Cenik;

bool CtiCenikyURL(const char *url, const char *maska, Cenik &cenik);

template <>
inline void ArchiveVariableExchange(IArchive &arch,CenikItem &cenik)
  {
  arch("id",cenik.id);
  arch("name",cenik.name);
  arch("weight",cenik.hmotnost);
  arch("cost",cenik.cena);
  arch("cost_dph",cenik.cenadph);
  }


#endif // !defined(AFX_ENIKITEM_H__175CA569_A8E0_45B7_9B4F_F8ED717412EB__INCLUDED_)
