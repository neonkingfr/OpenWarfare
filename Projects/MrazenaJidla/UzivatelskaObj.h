// UzivatelskaObj.h: interface for the UzivatelskaObj class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UZIVATELSKAOBJ_H__6D98F014_9EF7_49E3_8A05_B5411A607C99__INCLUDED_)
#define AFX_UZIVATELSKAOBJ_H__6D98F014_9EF7_49E3_8A05_B5411A607C99__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CenikItem.h"

class UzivatelskaObjItem 
{
 public:
	char *_kod;
	int _pockus;
public:
	UzivatelskaObjItem();
	virtual ~UzivatelskaObjItem();

	void SetKod(const char *kod)
	  {
	  if (kod!=_kod) {free(_kod);_kod=strdup(kod);}	  
	  }

	void Objednat(int ks)
	  {
	  _pockus+=ks;
	  }
	void DataExchange(IArchive &arch);
};

TypeIsMovable(UzivatelskaObjItem);

class UzivatelskaObj: public AutoArray<UzivatelskaObjItem>
  {
  unsigned char _heslo[16]; //MD5 heslo;
  float _zaplaceno;
  CString _filename;	  //current filename for safe saving
  bool _dirty;			  //file is dirty, need save
  public:
	UzivatelskaObj() {_dirty=false;_zaplaceno=0;}
	int Add(const char *id, int pockus);
	void Reset(const char *id);
	void ResetAll();
	int Find(const char *id);
	int CalcPocetKusu();
	float CalcCena(bool DPH, Cenik &cenik);

	void SetDirty();
	bool Save();
	bool Load(const char *filename);
	void SetFilename(const char *filename)
	  {_filename=filename;SetDirty();}
	
	void DataExchange(IArchive &arch);

	void SetHeslo(unsigned char heslo[16])
	  {memcpy(_heslo,heslo,sizeof(_heslo));}
	bool CompareHeslo(unsigned char heslo[16])
	  {return !memcmp(_heslo,heslo,sizeof(_heslo));}

	bool IsDirty() {return _dirty;}
    float Zaplaceno() {return _zaplaceno;}
    void Zaplatit(float castka) {_zaplaceno+=castka;}
    void VynulujPlaceni() {_zaplaceno=0;}
  };

#endif // !defined(AFX_UZIVATELSKAOBJ_H__6D98F014_9EF7_49E3_8A05_B5411A607C99__INCLUDED_)
