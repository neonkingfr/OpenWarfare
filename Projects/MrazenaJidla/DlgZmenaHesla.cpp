// DlgZmenaHesla.cpp : implementation file
//

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "DlgZmenaHesla.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgZmenaHesla dialog


DlgZmenaHesla::DlgZmenaHesla(CWnd* pParent /*=NULL*/)
	: CDialog(DlgZmenaHesla::IDD, pParent)
{
disableOld=false;
	//{{AFX_DATA_INIT(DlgZmenaHesla)
	vNoveHeslo = _T("");
	vNoveHesloZnova = _T("");
	vStareHeslo = _T("");
	//}}AFX_DATA_INIT
}


void DlgZmenaHesla::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgZmenaHesla)
	DDX_Text(pDX, IDC_HESLO, vNoveHeslo);
	DDX_Text(pDX, IDC_HESLOZNOVA, vNoveHesloZnova);
	DDX_Text(pDX, IDC_STAREHESLO, vStareHeslo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgZmenaHesla, CDialog)
	//{{AFX_MSG_MAP(DlgZmenaHesla)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgZmenaHesla message handlers

void DlgZmenaHesla::OnOK() 
{
	UpdateData(TRUE);
	if (vNoveHeslo!=vNoveHesloZnova)
	  {
	  AfxMessageBox(IDS_HESLASENESHODUJI,MB_OK|MB_ICONEXCLAMATION);
	  vNoveHeslo="";
	  vNoveHesloZnova="";
	  UpdateData(FALSE);
	  GetDlgItem(IDC_HESLO)->SetFocus();
	  return;
	  }
	
	CDialog::OnOK();
}

BOOL DlgZmenaHesla::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if (disableOld)
	  GetDlgItem(IDC_STAREHESLO)->EnableWindow(FALSE);
	if (vNoveHeslo.GetLength())
	  {
	  GetDlgItem(IDC_HESLOZNOVA)->SetFocus();
	  return FALSE;
	  }
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
