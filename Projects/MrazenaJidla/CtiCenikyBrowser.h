#if !defined(AFX_CTICENIKYBROWSER_H__B15CCE89_CBF0_4DC4_80E3_509EBD0C3D2B__INCLUDED_)
#define AFX_CTICENIKYBROWSER_H__B15CCE89_CBF0_4DC4_80E3_509EBD0C3D2B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CtiCenikyBrowser.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCtiCenikyBrowser html view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif
#include <afxhtml.h>

#define BM_ADDRESSUPDATE (WM_APP+100)
#define IDC_HTMLBROWSER 11895

class CCtiCenikyBrowser : public CHtmlView
{
    CWnd *_parent;
public:
	CCtiCenikyBrowser();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCtiCenikyBrowser)
	virtual ~CCtiCenikyBrowser();

// html Data
public:
	//{{AFX_DATA(CCtiCenikyBrowser)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
	BOOL Create(CDialog *parent, int id);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCtiCenikyBrowser)
	public:
	virtual void OnBeforeNavigate2(LPCTSTR lpszURL, DWORD nFlags, LPCTSTR lpszTargetFrameName, CByteArray& baPostedData, LPCTSTR lpszHeaders, BOOL* pbCancel);
	virtual void OnNavigateComplete2(LPCTSTR strURL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CCtiCenikyBrowser)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTICENIKYBROWSER_H__B15CCE89_CBF0_4DC4_80E3_509EBD0C3D2B__INCLUDED_)
