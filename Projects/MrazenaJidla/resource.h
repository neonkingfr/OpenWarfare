//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by MrazenaJidla.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MRAZENAJIDLA_DIALOG         102
#define IDS_NOVYTERMIN                  102
#define IDS_DATUMVMINULOSTI             103
#define IDS_JIDLOID                     104
#define IDS_JIDLOJMENO                  105
#define IDS_JIDLOVAHA                   106
#define IDS_JIDLOCENA                   107
#define IDS_JIDLOCENADPH                108
#define IDS_DELETETERMASK               109
#define IDS_NOPROFILEFOUND              110
#define IDS_NOSAMEPASSWORD              111
#define IDS_PASSWORDISNOTCORRECT        112
#define IDS_TERMINDESC                  113
#define IDS_JIDLOPOCETOBJ               114
#define IDS_HODNOTAJEPRILISVELKA        115
#define IDS_FORMATKUSU                  116
#define IDS_FORMATCENA                  117
#define IDS_HESLASENESHODUJI            118
#define IDS_STAREHESLONEBYLOZADANESPRAVNE 119
#define IDS_ZMENAHESLAOK                120
#define IDS_ULOZITDOTAZ                 121
#define IDS_SAVERROR                    122
#define IDS_HTMLOBJEDNAVKA              123
#define IDS_CELKEMSESTAVA               124
#define IDS_UZAVRENAOBJEDNAVKA          125
#define IDS_NASTALANEJAKACHYBA          126
#define IDS_TITLEUSERNAME               127
#define IDR_MAINFRAME                   128
#define IDS_TITLEZAPLACENO              128
#define IDD_CTICENIKY                   129
#define IDS_CELKEM                      129
#define IDD_ADMINPAGE                   130
#define IDS_PRIPLATEKSTR                130
#define IDD_NOVYTERMIN                  131
#define IDD_AUTORIZACE                  132
#define IDD_ZMENAHESLA                  134
#define IDD_PRINTPREVIEW                135
#define IDD_CONFIG                      136
#define IDD_DIALOG1                     137
#define IDD_SPRAVASESTAVA               137
#define IDC_NABIDKA                     1000
#define IDC_TERMINYLIST                 1001
#define IDC_BACK                        1002
#define IDC_BOBJEDNAT                   1004
#define IDC_BZRUSIT                     1005
#define IDC_OBJKUSU                     1006
#define IDC_OBJSESTAVIT                 1007
#define IDC_BNASTAVENI                  1008
#define IDC_SUBMIT                      1009
#define IDC_CELKEMKUSU                  1012
#define IDC_CELKEMCENA                  1013
#define IDC_CELKEMVSECHKUSU             1014
#define IDC_MANPRIPLATEK                1015
#define IDC_ADRESA                      1016
#define IDC_BROWSER                     1017
#define IDC_CTI                         1018
#define IDC_NEWTERMIN                   1019
#define IDC_SESTAVY                     1020
#define IDC_TERMIN                      1020
#define IDC_OBJEDNATSI                  1021
#define IDC_SETTERMIN                   1021
#define IDC_CENIKY                      1022
#define IDC_READCENIK                   1023
#define IDC_DELETETERMIN                1026
#define IDC_JMENO                       1027
#define IDC_HESLO                       1028
#define IDC_HESLOZNOVA                  1029
#define IDC_STAREHESLO                  1030
#define IDC_SERVERPATH                  1031
#define IDC_URLCENIK                    1032
#define IDC_OPENCONFIG                  1032
#define IDC_SEZNAMTERMINU               1033
#define IDC_NOVYDATUM                   1034
#define IDC_SEZNAMOBJ                   1035
#define IDC_SESTAVAPROOBJ               1036
#define IDC_ROZPIS                      1037
#define IDC_BNASTAVITNOVYDATUM          1038
#define IDC_CASTKA                      1039
#define IDC_BZAPLATIT                   1040
#define IDC_AKTUZIV                     1041
#define IDC_SEZNAMOBJSUMA               1042
#define IDC_PRIPLATEK                   1043
#define IDC_KOMPLETNIVYPIS              1043
#define IDC_DOPRAVA                     1044

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1045
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
