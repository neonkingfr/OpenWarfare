// ObjTermin.h: interface for the ObjTermin class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OBJTERMIN_H__6C90DDA4_E0C5_4A7E_9C95_4969AA298714__INCLUDED_)
#define AFX_OBJTERMIN_H__6C90DDA4_E0C5_4A7E_9C95_4969AA298714__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CenikItem.h"

union Date
  {
  struct
    {
    char day;
    char month;
	short year;
	};
  unsigned long asLong;
  };

class ObjTermin  
{
  public:
	Cenik _cenik;
	Date _deadline;
	CString _diskname;
  public:
	  void DataExchange(IArchive &arch);

};

#endif // !defined(AFX_OBJTERMIN_H__6C90DDA4_E0C5_4A7E_9C95_4969AA298714__INCLUDED_)
