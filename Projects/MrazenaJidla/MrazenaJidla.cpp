// MrazenaJidla.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "DlgObjednavka.h"
#include "DlgNovyTermin.h"
#include "DlgAdminPage.h"
#include "DlgSestavaSprava.h"
#include "SConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CApplication

BEGIN_MESSAGE_MAP(CApplication, CWinApp)
	//{{AFX_MSG_MAP(CApplication)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CApplication construction

CApplication::CApplication()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CApplication object

CApplication theApp;

/////////////////////////////////////////////////////////////////////////////
// CApplication initialization

CStringRes StrRes;



BOOL CApplication::InitInstance()
{


	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	StrRes.SetInstance(AfxGetInstanceHandle());
	GConfig.Load();


#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
	if (__argc>1 && stricmp(__argv[1],"/admin")==0)
	  {
	  DlgAdminPage admin;
	  int menu=admin.DoModal();
	  while (menu!=IDCANCEL)
		{
		switch (menu)
		  {
		  case IDC_NEWTERMIN: 
			{
			DlgNovyTermin dlg;
			dlg.DoModal();
			}
			break;
		  case IDC_OBJEDNATSI:
			{
			DlgObjednavka dlg;
			dlg.DoModal();
			}
			break;
          case IDC_SESTAVY:
            {
            DlgSestavaSprava dlg;
            dlg.DoModal();
            }
            break;
		  }
		menu=admin.DoModal();
		}	
	  }
	else
	  {
	  DlgObjednavka dlg;
	  dlg.DoModal();
	  }
	return FALSE;
}
