// CtiCenikyBrowser.cpp : implementation file
//

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "CtiCenikyBrowser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCtiCenikyBrowser

IMPLEMENT_DYNCREATE(CCtiCenikyBrowser, CHtmlView)

CCtiCenikyBrowser::CCtiCenikyBrowser()
{
	//{{AFX_DATA_INIT(CCtiCenikyBrowser)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCtiCenikyBrowser::~CCtiCenikyBrowser()
{
}

void CCtiCenikyBrowser::DoDataExchange(CDataExchange* pDX)
{
	CHtmlView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCtiCenikyBrowser)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCtiCenikyBrowser, CHtmlView)
	//{{AFX_MSG_MAP(CCtiCenikyBrowser)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCtiCenikyBrowser diagnostics

#ifdef _DEBUG
void CCtiCenikyBrowser::AssertValid() const
{
	CHtmlView::AssertValid();
}

void CCtiCenikyBrowser::Dump(CDumpContext& dc) const
{
	CHtmlView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCtiCenikyBrowser message handlers

BOOL CCtiCenikyBrowser::Create(CDialog *parent, int id)
  {
  CWnd *rectref=parent->GetDlgItem(IDC_BROWSER);
  CRect rc;
  rectref->GetWindowRect(&rc);
  parent->ScreenToClient(&rc);
  _parent=parent;
  return CHtmlView::Create(AfxRegisterWndClass(0),"Browser",WS_VISIBLE|WS_CHILD,rc,parent,id);
  }

void CCtiCenikyBrowser::OnBeforeNavigate2(LPCTSTR lpszURL, DWORD nFlags, LPCTSTR lpszTargetFrameName, CByteArray& baPostedData, LPCTSTR lpszHeaders, BOOL* pbCancel) 
  {
  CString location=GetLocationURL( );
  _parent->SendMessage(BM_ADDRESSUPDATE,0,(LPARAM)(LPCTSTR)location);
  CHtmlView::OnBeforeNavigate2(lpszURL, nFlags,	lpszTargetFrameName, baPostedData, lpszHeaders, pbCancel);
  }

void CCtiCenikyBrowser::OnNavigateComplete2(LPCTSTR strURL) 
{
	// TODO: Add your specialized code here and/or call the base class
	CString location=GetLocationURL( );
	_parent->SendMessage(BM_ADDRESSUPDATE,0,(LPARAM)(LPCTSTR)location);
	CHtmlView::OnNavigateComplete2(strURL);
}

void CCtiCenikyBrowser::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	
}


