// SConfig.cpp: implementation of the SConfig class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "SConfig.h"
#include <ES\Containers\array.hpp>
#include "ArchiveExt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SConfig GConfig;

void SConfig::DataExchange(IArchive &arch)
  {
  if (arch.Check("MJ_CONFIG")==false || arch.RequestVer(FILEVERSIONS)==false)
	{
	arch.SetError(-1);
	return;
	}
  arch("serverPath",_serverPath);
  arch("cenikyURL",_cenikyURL);
  arch("priplatek",_priplatek);
  arch("doprava",_doprava);
  }

static Pathname GetConfigPath()
  {
  char buff[MAX_PATH*2];
  GetModuleFileName(NULL,buff,MAX_PATH*2);
  Pathname pt(buff);
  pt.SetExtension(".cfg");
  return pt;
  }

void SConfig::Load()
  {
  Pathname cfg=GetConfigPath();
  ArchiveStreamFileMappingIn file(CreateFile(cfg,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL),true);
  ArchiveSimpleBinary arch(file);
  if (!arch) return;
  DataExchange(arch);  
  }

void SConfig::Save()
  {
  Pathname cfg=GetConfigPath();
  ArchiveStreamFileMappingOut file(CreateFile(cfg,GENERIC_WRITE|GENERIC_READ,FILE_SHARE_READ,NULL,CREATE_ALWAYS,0,NULL),true);
  ArchiveSimpleBinary arch(file);
  if (!arch) return;
  DataExchange(arch);  
  }
