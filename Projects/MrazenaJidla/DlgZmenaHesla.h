#if !defined(AFX_DLGZMENAHESLA_H__BCABC716_0855_4F9F_8536_FF261B6CD9EA__INCLUDED_)
#define AFX_DLGZMENAHESLA_H__BCABC716_0855_4F9F_8536_FF261B6CD9EA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgZmenaHesla.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgZmenaHesla dialog

class DlgZmenaHesla : public CDialog
{
// Construction
public:
	DlgZmenaHesla(CWnd* pParent = NULL);   // standard constructor

	bool disableOld;
// Dialog Data
	//{{AFX_DATA(DlgZmenaHesla)
	enum { IDD = IDD_ZMENAHESLA };
	CString	vNoveHeslo;
	CString	vNoveHesloZnova;
	CString	vStareHeslo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgZmenaHesla)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgZmenaHesla)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGZMENAHESLA_H__BCABC716_0855_4F9F_8536_FF261B6CD9EA__INCLUDED_)
