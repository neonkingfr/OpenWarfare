#if !defined(AFX_DLGCONFIG_H__1149EFF6_8FA8_40D5_BAEE_DEAD955D2708__INCLUDED_)
#define AFX_DLGCONFIG_H__1149EFF6_8FA8_40D5_BAEE_DEAD955D2708__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgConfig.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgConfig dialog

class DlgConfig : public CDialog
{
// Construction
public:
	DlgConfig(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgConfig)
	enum { IDD = IDD_CONFIG };
	CString	vServerPath;
	CString	vUrlCenik;
	float	vDoprava;
	float	vPriplatek;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgConfig)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgConfig)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCONFIG_H__1149EFF6_8FA8_40D5_BAEE_DEAD955D2708__INCLUDED_)
