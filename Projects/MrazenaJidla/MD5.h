// MD5.h: interface for the MD5 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MD5_H__0B30BBA2_FD04_4C15_B407_C45214F36D3A__INCLUDED_)
#define AFX_MD5_H__0B30BBA2_FD04_4C15_B407_C45214F36D3A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class MD5  
{
    unsigned long _total[2];
    unsigned long _state[4];
    unsigned char _buffer[64];
	bool _result;

	void Process(unsigned char data[64]);

 public:
	MD5();
	void Add(unsigned char *block, size_t blocksize);
	void Result(unsigned char digest[16]);
	void Reset();

	static void Calc(unsigned char *block, size_t blocksize,unsigned char digest[16]);
	static void HashString(char *string, char hashed[33]);

};

#endif // !defined(AFX_MD5_H__0B30BBA2_FD04_4C15_B407_C45214F36D3A__INCLUDED_)
