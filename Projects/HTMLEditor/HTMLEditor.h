// HTMLEditor.h : main header file for the HTMLEditor application
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols


// CHTMLEditorApp:
// See HTMLEditor.cpp for the implementation of this class
//

class CHTMLEditorApp : public CWinApp
{
public:
	CHTMLEditorApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CHTMLEditorApp theApp;