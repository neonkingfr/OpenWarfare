// ChildView.h : interface of the HTMLEditPad class
//


#pragma once

#include "common.h"
#include "afxhtml.h"
// HTMLEditPad window

typedef ComRef<IHTMLDocument2> PHTMLDocument2;

class HTMLEditBrowser: public CHtmlView
{
public:
  HTMLEditBrowser() {}
  PHTMLDocument2 GetDocument() 
  {
    ComRef<IDispatch> disp=GetHtmlDocument();
    PHTMLDocument2 doc;
    disp->QueryInterface(IID_IHTMLDocument2,doc.InitVoid());
    return doc;
  }
  virtual void PostNcDestroy() {};
};

class HTMLEditPad : public CWnd
{
// Construction
    HTMLEditBrowser editorView;
public:
	HTMLEditPad();

// Attributes
public:

// Operations
public:
  void CreateEditor(const _TCHAR *templateUrl);

// Overrides
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~HTMLEditPad();

	// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnSize(UINT nType, int cx, int cy);
};

