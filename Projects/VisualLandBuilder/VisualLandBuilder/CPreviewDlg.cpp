// CPreviewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VisualLandBuilder.h"
#include "CPreviewDlg.h"


// CPreviewDlg dialog

IMPLEMENT_DYNAMIC(CPreviewDlg, CDialog)

// -------------------------------------------------------------------------- //

CPreviewDlg::CPreviewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPreviewDlg::IDD, pParent)
{
	m_ModuleList    = NULL;
	m_ShowAll       = false;
	m_Panning       = false;
	m_ZoomingWindow = false;
}

// -------------------------------------------------------------------------- //

CPreviewDlg::~CPreviewDlg()
{
}

// -------------------------------------------------------------------------- //

void CPreviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PREVIEWPICTURE, m_PictureCtrl);
	DDX_Control(pDX, IDCANCEL, m_CancelBtn);
	DDX_Control(pDX, IDC_TAB1, m_ToolTab);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CPreviewDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CPreviewDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	HICON hIcon = LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	this->SetIcon(hIcon, FALSE);

	m_ToolTab.InsertItem(TCIF_TEXT | TCIF_IMAGE, PV_TAB_ZOOM     , "Zoom"   , PV_TAB_ZOOM     , 0);
	m_ToolTab.InsertItem(TCIF_TEXT | TCIF_IMAGE, PV_TAB_SHAPES   , "Shapes" , PV_TAB_SHAPES   , 0);
	m_ToolTab.InsertItem(TCIF_TEXT | TCIF_IMAGE, PV_TAB_OBJCOLORS, "Objects", PV_TAB_OBJCOLORS, 0);

	m_ToolTab.Init();

	// this line is used to size the dialog at startup
	OnSize(0, 0, 0);

	// connect the preview dlg to the dialogs in the tools tab
	m_ZooDlg = (CTabZoomDlg*)m_ToolTab.GetDialog(IDD_TABPREVZOOM);
	m_ShaDlg = (CTabShapeListDlg*)m_ToolTab.GetDialog(IDD_TABPREVSHAPELIST);
	m_ColDlg = (CTabObjectColorListDlg*)m_ToolTab.GetDialog(IDD_TABOBJECTCOLOR);

	m_ShaDlg->SetList(m_ModuleList, &m_ShapeOrganizerList, m_ShowAll);
	
	if(m_ShowModulePreview) 
	{
		m_ColDlg->SetList(m_ModuleList, m_ShowAll);
	}
	else
	{
		m_ColDlg->SetList(NULL, false);
	}

	m_PictureCtrl.SetList(m_ModuleList, &m_ShapeOrganizerList, m_ShowAll, m_ShowModulePreview);

	return TRUE;
}

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //

void CPreviewDlg::SetModuleList(LBModuleList* moduleList, bool showAll, bool showModulePreview)
{
	// clears the ShapeOrganizer list
	m_ShapeOrganizerList.Clear();

	m_ModuleList        = moduleList;
	m_ShowAll           = showAll;
	m_ShowModulePreview = showModulePreview;

	if(m_ModuleList)
	{
		INT_PTR moduleCount = m_ModuleList->GetCount();

		BeginWaitCursor();

		// creates a ShapeOrganizer for each module
		m_ShapeOrganizerList.Clear();
		for(int i = 0; i < moduleCount; ++i)
		{
			LBModule* module = m_ModuleList->GetAt(i);

			m_ShapeOrganizerList.Add(new Shapes::ShapeOrganizer);

			// if the module is previewable goes on
			if(module->GetHasPreview() || m_ShowAll)
			{
				// if the module is active, or if showall goes on		
				if(module->IsActive() || m_ShowAll)
				{
					// if the module uses shapefiles loads them
					if(module->GetUseShapefile())
					{
						m_ShapeOrganizerList[i]->ReadShapeFile(Pathname(module->GetShapeFileName().c_str()), 1);
					}
				}
			}
		}

		EndWaitCursor();
	}
	else
	{
		m_ShapeOrganizerList.Clear();
	}
}

// -------------------------------------------------------------------------- //

void CPreviewDlg::SetSelectedShape(int moduleIndex, int shapeIndex)
{
	m_PictureCtrl.SetSelectedShape(moduleIndex, shapeIndex);
}

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

void CPreviewDlg::OnSize(UINT nType, int cx, int cy)
{
	if(cx != 0) CDialog::OnSize(nType, cx, cy);

	CRect dlgRect;
	GetClientRect(dlgRect);

	CWnd* picFrame   = GetDlgItem(IDC_PICFRAME);
	CWnd* picture    = GetDlgItem(IDC_PREVIEWPICTURE);
	CWnd* toolsTab   = GetDlgItem(IDC_TOOLSTAB);
	CWnd* messageGrp = GetDlgItem(IDC_MESSAGEGROUP);
	CWnd* messageLbl = GetDlgItem(IDC_MESSAGETEXT);
	CWnd* cancelBtn  = GetDlgItem(IDCANCEL);

	if(cancelBtn)
	{
		CRect btnRect;
		m_CancelBtn.GetWindowRect(btnRect);
		int btnLeft = dlgRect.right - 138;
		int btnTop  = dlgRect.bottom - 28;
		cancelBtn->MoveWindow(btnLeft, btnTop, btnRect.Width(), btnRect.Height(), TRUE);		
	}

	if(picture)
	{
		int picFrameLeft   = dlgRect.left + 5;
		int picFrameTop    = dlgRect.top + 5;
		int picFrameRight  = dlgRect.right - 200;
		int picFrameBottom = dlgRect.bottom - 5;
		int picFrameWidth  = picFrameRight - picFrameLeft;
		int picFrameHeight = picFrameBottom - picFrameTop;
		toolsTab->MoveWindow(picFrameRight + 10, picFrameTop, 180, picFrameHeight - 90, TRUE);
		messageGrp->MoveWindow(picFrameRight + 10, picFrameBottom - 90, 180, 60, TRUE);
		messageLbl->ShowWindow(SW_HIDE);
		messageLbl->MoveWindow(picFrameRight + 15, picFrameBottom - 80, 170, 45, TRUE);
		messageLbl->ShowWindow(SW_SHOW);
		picFrame->ShowWindow(SW_HIDE);
		picFrame->MoveWindow(picFrameLeft, picFrameTop, picFrameWidth, picFrameHeight, TRUE);		
		picFrame->ShowWindow(SW_SHOW);
		picture->MoveWindow(picFrameLeft + 15, picFrameTop + 15, picFrameWidth - 30, picFrameHeight - 30, TRUE);		
		picture->RedrawWindow();
	}
}

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

void CPreviewDlg::ShowApplyingModuleMessage(LBModule* module)
{
	if(module)
	{
		string msg = MSG_WAITPLEASE + MSG_APPLYINGMODULE + "\n" + module->GetVLBName();
		GetDlgItem(IDC_MESSAGETEXT)->SetWindowTextA(msg.c_str());
	}
	else
	{
		GetDlgItem(IDC_MESSAGETEXT)->SetWindowTextA("");
	}
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CPreviewDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == MY_PAINTMSG)
	{
		m_PictureCtrl.Invalidate();
	}

	return CDialog::PreTranslateMessage(pMsg);
}

// -------------------------------------------------------------------------- //
// SUB DIALOG EVENTS HANDLERS                                                 //
// -------------------------------------------------------------------------- //

void CPreviewDlg::OnPan()
{
	m_PictureCtrl.Pan();
	m_Panning = !m_Panning;
	m_ZooDlg->SetButtonsState(m_PictureCtrl.GetZoomFactor(), m_Panning, false);
	GetDlgItem(IDCANCEL)->EnableWindow(!m_Panning);
}

// -------------------------------------------------------------------------- //

void CPreviewDlg::OnZoomWindow()
{
	m_PictureCtrl.ZoomWindow();
	m_ZoomingWindow = !m_ZoomingWindow;
	m_ZooDlg->SetButtonsState(m_PictureCtrl.GetZoomFactor(), false, m_ZoomingWindow);
	GetDlgItem(IDCANCEL)->EnableWindow(!m_ZoomingWindow);
}

// -------------------------------------------------------------------------- //

void CPreviewDlg::OnZoomPlus()
{
	m_PictureCtrl.ZoomPlus();
	m_ZooDlg->SetButtonsState(m_PictureCtrl.GetZoomFactor(), false, false);
}

// -------------------------------------------------------------------------- //

void CPreviewDlg::OnZoomMinus()
{
	m_PictureCtrl.ZoomMinus();
	m_ZooDlg->SetButtonsState(m_PictureCtrl.GetZoomFactor(), false, false);
}

// -------------------------------------------------------------------------- //

void CPreviewDlg::OnZoomExt()
{
	m_PictureCtrl.ZoomExt();
	m_ZooDlg->SetButtonsState(m_PictureCtrl.GetZoomFactor(), false, false);
}

// -------------------------------------------------------------------------- //
// MOUSE EVENTS HANDLERS                                                      //
// -------------------------------------------------------------------------- //

void CPreviewDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_PictureCtrl.OnLButtonDown(nFlags, point);

	CDialog::OnLButtonDown(nFlags, point);
}

// -------------------------------------------------------------------------- //

void CPreviewDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_PictureCtrl.OnLButtonUp(nFlags, point);

	CDialog::OnLButtonUp(nFlags, point);
}

// -------------------------------------------------------------------------- //

void CPreviewDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	m_PictureCtrl.OnMouseMove(nFlags, point);

	CDialog::OnMouseMove(nFlags, point);
}

// -------------------------------------------------------------------------- //
// CURSOR HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CPreviewDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(m_PictureCtrl.OnSetCursor(pWnd, nHitTest, message))
	{
		return TRUE;
	}

	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}
