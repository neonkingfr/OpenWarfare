// CTabObjectColorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VisualLandBuilder.h"
#include "CTabObjectColorListDlg.h"

// CTabObjectColorDlg dialog

IMPLEMENT_DYNAMIC(CTabObjectColorListDlg, CDialog)

// -------------------------------------------------------------------------- //

CTabObjectColorListDlg::CTabObjectColorListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTabObjectColorListDlg::IDD, pParent)
{
	COLORREF windowBk = RGB(192, 192, 192);
	imageList.Create(IDB_COLORS, 16, 0, windowBk);
}

// -------------------------------------------------------------------------- //

CTabObjectColorListDlg::~CTabObjectColorListDlg()
{
}

// -------------------------------------------------------------------------- //

void CTabObjectColorListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OBJCOLORLIST, m_ObjColorList);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CTabObjectColorListDlg, CDialog)
	ON_WM_SIZE()
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CTabObjectColorListDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	// sets the object list column header
	m_ObjColorList.InsertColumn(0, string(HEAD_OBJECTNAME).c_str(), LVCFMT_LEFT);
	m_ObjColorList.InsertColumn(1, string(HEAD_FILL).c_str(), LVCFMT_CENTER);
	m_ObjColorList.InsertColumn(2, string(HEAD_BORDER).c_str(), LVCFMT_CENTER);
	m_ObjColorList.SetColumnWidth(0, 80);
	m_ObjColorList.SetColumnWidth(1, 40);
	m_ObjColorList.SetColumnWidth(2, 40);

	// sets the object list style
	m_ObjColorList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_SUBITEMIMAGES);

	m_ObjColorList.SetImageList(&imageList, LVSIL_SMALL);

	// sets the in-place editing object for the list 
	m_ObjColorList.SetEditType(ET_COLORCOMBO);

	return TRUE;
}

// -------------------------------------------------------------------------- //
// DIALOG EVENT HANDLERS                                                      //
// -------------------------------------------------------------------------- //

void CTabObjectColorListDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	CRect dlgRect;
	GetClientRect(dlgRect);

	CWnd* group = GetDlgItem(IDC_COLORLISTGROUP);
	CWnd* list = GetDlgItem(IDC_OBJCOLORLIST);

	if(group)
	{
		int groupLeft   = dlgRect.left + 1;
		int groupTop    = dlgRect.top + 1;
		int groupRight  = dlgRect.right - 1;
		int groupBottom = dlgRect.bottom - 1;
		int groupWidth  = groupRight - groupLeft;
		int groupHeight = groupBottom - groupTop;
		group->MoveWindow(groupLeft, groupTop, groupWidth, groupHeight, TRUE);		
		list->MoveWindow(groupLeft + 3, groupTop + 9, groupWidth - 6, groupHeight - 12, TRUE);		
	}
}

// -------------------------------------------------------------------------- //
// INTERFACE                                                                  //
// -------------------------------------------------------------------------- //

void CTabObjectColorListDlg::SetList(LBModuleList* moduleList, bool showAll)
{
	m_ObjColorList.DeleteAllItems();

	m_pModuleList = moduleList;

	if(m_pModuleList != NULL)
	{
		GetDlgItem(IDC_COLORLISTGROUP)->EnableWindow(TRUE);
		GetDlgItem(IDC_OBJCOLORLIST)->EnableWindow(TRUE);

		INT_PTR moduleCount = m_pModuleList->GetCount();
		for(int i = 0; i < moduleCount; ++i)
		{
			LBModule* module = m_pModuleList->GetAt(i);
			if(module->IsActive() || showAll)
			{
				if(module->GetUseObjects() != LBMUO_NONE)
				{
					INT_PTR objCount = module->GetObjectsList().GetCount();
					int counter = 0;
					for(int j = 0; j < objCount; ++j)
					{
						LBObject* object = module->GetObjectsList().GetAt(j);
						if(!AlreadyInList(object->GetName()))
						{
							m_ObjColorList.InsertItem(counter, object->GetName().c_str(), -1);

							LV_ITEM lvitem;

							lvitem.mask     = LVIF_TEXT | LVIF_IMAGE;
							lvitem.iItem    = counter;
							lvitem.iSubItem = 1;
							string a1 = object->GetPvFillColorAsString();
							CString a2 = CString(a1.c_str());
							LPCTSTR a3 = (LPCTSTR)a2;
							LPTSTR  a4 = LPTSTR(a3);
							lvitem.pszText  = a4;
							lvitem.iImage   = object->GetPvFillColor();
							m_ObjColorList.SetItem(&lvitem);

							lvitem.mask     = LVIF_TEXT | LVIF_IMAGE;
							lvitem.iItem    = counter;
							lvitem.iSubItem = 2;
							a1 = object->GetPvBorderColorAsString();
							a2 = CString(a1.c_str());
							a3 = (LPCTSTR)a2;
							a4 = LPTSTR(a3);
							lvitem.pszText  = a4;
							lvitem.iImage   = object->GetPvBorderColor();
							m_ObjColorList.SetItem(&lvitem);

							counter++;
						}
					}
				}
			}
		}
	}
	else
	{
		GetDlgItem(IDC_COLORLISTGROUP)->EnableWindow(FALSE);
		GetDlgItem(IDC_OBJCOLORLIST)->EnableWindow(FALSE);
	}
}

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

bool CTabObjectColorListDlg::AlreadyInList(string name)
{
	int itemCount = m_ObjColorList.GetItemCount();
	for(int i = 0; i < itemCount; ++i)
	{
		CString cItemText = m_ObjColorList.GetItemText(i, 0);
		string itemText = cItemText;
		if(name == itemText) return true;
	}
	return false;
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CTabObjectColorListDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == MY_ENDEDITMSG)
	{
		LV_ITEM lvitem;

		int item    = pMsg->wParam;
		int subItem = pMsg->lParam;

		lvitem.mask     = LVIF_TEXT | LVIF_IMAGE;
		lvitem.iItem    = item;
		lvitem.iSubItem = subItem;
		CString str = m_ObjColorList.GetItemText(item, subItem);
		LPCTSTR a3 = (LPCTSTR)str;
		LPTSTR  a4 = LPTSTR(a3);
		lvitem.pszText  = a4;
		lvitem.iImage   = atoi(string(str).c_str());
		m_ObjColorList.SetItem(&lvitem);

		CString cObjName = m_ObjColorList.GetItemText(item, 0);
		string objName = string(cObjName);

		INT_PTR moduleCount = m_pModuleList->GetCount();
		for(int i = 0; i < moduleCount; ++i)
		{
			LBModule* module = m_pModuleList->GetAt(i);
			INT_PTR objCount = module->GetObjectsList().GetCount();
			for(int j = 0; j < objCount; ++j)
			{
				LBObject* object = module->GetObjectsList().GetAt(j);
				if(objName == object->GetName())
				{
					if(subItem == 1)
					{
						object->SetPvFillColor((LBPreviewColors)lvitem.iImage);
					}
					else if(subItem == 2)
					{
						object->SetPvBorderColor((LBPreviewColors)lvitem.iImage);
					}
				}
			}
		}
		GetParent()->PostMessage(MY_PAINTMSG, (WPARAM)0, (LPARAM)0);
	}

	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}