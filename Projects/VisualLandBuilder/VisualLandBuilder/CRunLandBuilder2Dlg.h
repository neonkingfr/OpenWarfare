//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

// CRunLandBuilder2Dlg dialog

class CRunLandBuilder2Dlg : public CDialog
{
	DECLARE_DYNAMIC( CRunLandBuilder2Dlg )

public:
	CRunLandBuilder2Dlg( CWnd* parent = NULL );   // standard constructor
	virtual ~CRunLandBuilder2Dlg();

  // Dialog Data
	enum { IDD = IDD_RUNLANDBUILDER2 };

protected:
	virtual void DoDataExchange( CDataExchange* dx );    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	// the name ctrl
	CEdit m_runNameCtrl;

	// DATA
	// the pointer to the original data
	string* m_oldName;
	// the working copy of the data
	string m_name;

  // -------------------------------------------------------------------------- //
  // DIALOG HANDLERS                                                            //
  // -------------------------------------------------------------------------- //
	BOOL OnInitDialog();

public:
  // -------------------------------------------------------------------------- //
  // HELPER FUNCTIONS                                                           //
  // -------------------------------------------------------------------------- //
	// SetName()
	// sets the working name and the pointer to the original one
	//  !! must be called before the DoModal function of this dialog !!
	void SetName( string* name );

	// Validate()
	// validates dialogs data
	// returns true if all ok
	bool Validate();

private:
  // -------------------------------------------------------------------------- //
  // BUTTONS HANDLERS                                                           //
  // -------------------------------------------------------------------------- //
	afx_msg void OnBnClickedOk();

  // -------------------------------------------------------------------------- //
  // EDIT HANDLERS                                                              //
  // -------------------------------------------------------------------------- //
	afx_msg void OnEnChangeRunLbEdit();
};

//-----------------------------------------------------------------------------
