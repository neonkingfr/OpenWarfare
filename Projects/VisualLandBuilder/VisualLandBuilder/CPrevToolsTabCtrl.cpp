#include "stdafx.h"

#include ".\CPrevToolsTabCtrl.h"

BEGIN_MESSAGE_MAP(CPrevToolsTabCtrl, CTabCtrl)
	ON_WM_LBUTTONDOWN()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

CPrevToolsTabCtrl::CPrevToolsTabCtrl()
{
	m_TabPages[0] = new CTabZoomDlg();
	m_TabPages[1] = new CTabShapeListDlg();
	m_TabPages[2] = new CTabObjectColorListDlg();

	m_NumberOfPages = 3;

	// sets the image list for the param tabs
	COLORREF windowBk = RGB(192, 192, 192);
	imageList.Create(IDB_TABTOOLS, 16, 0, windowBk);
}

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

CPrevToolsTabCtrl::~CPrevToolsTabCtrl()
{
	for(int i = 0; i < m_NumberOfPages; ++i)
	{
		delete m_TabPages[i];
	}
}

// ************************************************************** //
// Helper functions                                               //
// ************************************************************** //

void CPrevToolsTabCtrl::Init()
{
	SetImageList(&imageList);

	m_CurrentTab = 0;

	m_TabPages[0]->Create(IDD_TABPREVZOOM, this);
	m_TabPages[1]->Create(IDD_TABPREVSHAPELIST, this);
	m_TabPages[2]->Create(IDD_TABOBJECTCOLOR, this);

	m_TabPages[0]->ShowWindow(SW_SHOW);
	m_TabPages[1]->ShowWindow(SW_HIDE);
	m_TabPages[2]->ShowWindow(SW_HIDE);

	SetRectangle();
}

// -------------------------------------------------------------------------- //

void CPrevToolsTabCtrl::SetRectangle()
{
	CRect tabRect, itemRect;

	GetClientRect(&tabRect);

	int nX = tabRect.left + 2;
	int nY = tabRect.top + 23;
	int nXc = tabRect.Width() - 6;
	int nYc = tabRect.Height() - 27;

	for(int i = 0; i < m_NumberOfPages; ++i)
	{
		if(i == m_CurrentTab)
		{
			m_TabPages[i]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW);
		}
		else
		{
			m_TabPages[i]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);
		}
	}
}

// -------------------------------------------------------------------------- //

CDialog* CPrevToolsTabCtrl::GetDialog(UINT index) const
{
	switch(index)
	{
	case IDD_TABPREVZOOM:
		return m_TabPages[0];
	case IDD_TABPREVSHAPELIST:
		return m_TabPages[1];
	case IDD_TABOBJECTCOLOR:
		return m_TabPages[2];
	default:
		return NULL;
	}
}

// ************************************************************** //
// Message handlers                                               //
// ************************************************************** //

void CPrevToolsTabCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CTabCtrl::OnLButtonDown(nFlags, point);

	if(m_CurrentTab != GetCurFocus())
	{
		m_TabPages[m_CurrentTab]->ShowWindow(SW_HIDE);
		m_CurrentTab = GetCurFocus();
		m_TabPages[m_CurrentTab]->ShowWindow(SW_SHOW);
		m_TabPages[m_CurrentTab]->SetFocus();
	}
}

// -------------------------------------------------------------------------- //

BOOL CPrevToolsTabCtrl::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_LEFT || pMsg->wParam == VK_RIGHT)
		{

			m_TabPages[m_CurrentTab]->ShowWindow(SW_HIDE);

			if(pMsg->wParam == VK_LEFT)
			{
				if(m_CurrentTab > 0)
				{
					m_CurrentTab--;
				}
			}
			if(pMsg->wParam == VK_RIGHT)
			{
				if(m_CurrentTab < m_NumberOfPages - 1)
				{
					m_CurrentTab++;
				}
			}
			m_TabPages[m_CurrentTab]->ShowWindow(SW_SHOW);
		}
	}

	return CTabCtrl::PreTranslateMessage(pMsg);
}

// -------------------------------------------------------------------------- //

void CPrevToolsTabCtrl::OnSize(UINT nType, int cx, int cy)
{
	CTabCtrl::OnSize(nType, cx, cy);

	SetRectangle();
}
