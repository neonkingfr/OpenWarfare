// CTabGlobalDlg.cpp : implementation file
//

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CTabGlobalDlg.h"
#include ".\CAddEditModuleDlg.h"

#include "..\..\LandBuilder\HighMapLoaders\include\EtHelperFunctions.h"

// CTabGlobalDlg dialog

IMPLEMENT_DYNAMIC(CTabGlobalDlg, CDialog)

// -------------------------------------------------------------------------- //

CTabGlobalDlg::CTabGlobalDlg(CWnd* pParent /*=NULL*/)
: CDialog(CTabGlobalDlg::IDD, pParent)
, m_Modified(false)
{
}

// -------------------------------------------------------------------------- //

CTabGlobalDlg::~CTabGlobalDlg()
{
}

// -------------------------------------------------------------------------- //

void CTabGlobalDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TABGLOBALPARLIST, m_GlobalParListCtrl);
	DDX_Control(pDX, IDC_TABGLOBALDESCRIPTION, m_GlobalDescriptionCtrl);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CTabGlobalDlg, CDialog)
	ON_NOTIFY(NM_CLICK, IDC_TABGLOBALPARLIST, &CTabGlobalDlg::OnNMClickTabGlobalParList)
	ON_BN_CLICKED(IDC_TABGLOBALEDITBTN, &CTabGlobalDlg::OnBnClickedTabGlobalEditBtn)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_TABGLOBALPARLIST, &CTabGlobalDlg::OnLvnItemActivateTabGlobalParList)
	ON_NOTIFY(LVN_KEYDOWN, IDC_TABGLOBALPARLIST, &CTabGlobalDlg::OnLvnKeydownTabGlobalParList)
	ON_BN_CLICKED(IDC_TABGLOBALCLEARBTN, &CTabGlobalDlg::OnBnClickedTabGlobalClearBtn)
	ON_BN_CLICKED(IDC_TABGLOBALTODBFBTN, &CTabGlobalDlg::OnBnClickedTabGlobalToDbfBtn)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CTabGlobalDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	// sets the object list column header
	m_GlobalParListCtrl.InsertColumn(0, string(HEAD_PARAMETER).c_str(), LVCFMT_LEFT);
	m_GlobalParListCtrl.SetColumnWidth(0, 160);
	m_GlobalParListCtrl.InsertColumn(1, string(HEAD_VALUE).c_str(), LVCFMT_LEFT);
	m_GlobalParListCtrl.SetColumnWidth(1, 200);

	// sets the object list style
	m_GlobalParListCtrl.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	// sets the in-place editing object for the list 
	m_GlobalParListCtrl.SetEditType(ET_EDIT);

	return TRUE;
}

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //

void CTabGlobalDlg::UpdateState()
{
	// gets the index of the selected item in the list if any
	int selItem = m_GlobalParListCtrl.GetNextItem(-1, LVNI_SELECTED);

	// clears the list items
	m_GlobalParListCtrl.DeleteAllItems();

	// enables/disables controls if there are global parameters
	if(m_pModule->GetGlobalParametersList().IsEmpty())
	{
		GetDlgItem(IDC_TABGLOBALGROUP)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABGLOBALPARLIST)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABGLOBALMANDATORY)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABGLOBALDESCRGROUP)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_TABGLOBALGROUP)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABGLOBALPARLIST)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABGLOBALMANDATORY)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABGLOBALDESCRGROUP)->EnableWindow(TRUE);
	}

	// updates the items
	INT_PTR gloParamCount = m_pModule->GetGlobalParametersList().GetCount();
	for(int i = 0; i < gloParamCount; ++i)
	{
		LBParameter* item = m_pModule->GetGlobalParametersList().GetAt(i);
		m_GlobalParListCtrl.InsertItem(i, item->GetName().c_str());
		// if the parameter has no default, show it bolded
		if(!item->GetHasDefault())
		{
			m_GlobalParListCtrl.SetItemStyle(i, 0, LIS_BOLD, true);
		}
		m_GlobalParListCtrl.SetItem(i, 1, LVIF_TEXT, item->GetValue().c_str(), 0, 0, 0, 0);
	}

	if(selItem != -1)
	{
		m_GlobalParListCtrl.EnsureVisible(selItem, FALSE);
		m_GlobalParListCtrl.SetItemState(selItem, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	}

	m_Modified = true;

	// disables edit and todbf buttons
	GetDlgItem(IDC_TABGLOBALEDITBTN)->EnableWindow(FALSE);
	GetDlgItem(IDC_TABGLOBALTODBFBTN)->EnableWindow(FALSE);

	// enables/disables clear button
	if(!m_pModule->GetGlobalParametersList().IsEmpty() && HasData())
	{
		GetDlgItem(IDC_TABGLOBALCLEARBTN)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_TABGLOBALCLEARBTN)->EnableWindow(FALSE);
	}

	// clears the description
	m_GlobalDescriptionCtrl.SetWindowTextA("");

	// disables the module list if there is at least one object
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateModuleListCtrlState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateClearAllDataBtnState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->CheckOkButtonEnabling();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateGlobalTabImage();
}

// -------------------------------------------------------------------------- //

bool CTabGlobalDlg::GetModified() const
{
	return m_Modified;
}

// -------------------------------------------------------------------------- //

bool CTabGlobalDlg::HasData() const
{
	return m_pModule->GetGlobalParametersList().HasSomeValue();
}

// -------------------------------------------------------------------------- //

bool CTabGlobalDlg::HasAllData() const
{
	return m_pModule->GetGlobalParametersList().Validate();
}

// -------------------------------------------------------------------------- //

void CTabGlobalDlg::SetModule(LBModule* module)
{
	m_pModule = module;
}

// -------------------------------------------------------------------------- //

BOOL CTabGlobalDlg::GetClearButtonState() const
{
	return GetDlgItem(IDC_TABGLOBALCLEARBTN)->IsWindowEnabled();	
}

// -------------------------------------------------------------------------- //
// OBJECT LIST EVENT HANDLERS                                                 //
// -------------------------------------------------------------------------- //

void CTabGlobalDlg::OnNMClickTabGlobalParList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// gets the index of the selected item in the list
	int selItem = m_GlobalParListCtrl.GetNextItem(-1, LVNI_SELECTED);

	// enables/disables edit and to dbf button
	if(selItem != -1)
	{
		LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
		if(pLVKeyDow->wVKey == VK_UP) selItem--;
		if(pLVKeyDow->wVKey == VK_DOWN) selItem++;
		if(selItem < 0) selItem = 0;
		if(selItem > m_GlobalParListCtrl.GetItemCount() - 1) selItem = m_GlobalParListCtrl.GetItemCount() - 1;

		// something is selected
		GetDlgItem(IDC_TABGLOBALEDITBTN)->EnableWindow(TRUE);
		if(m_pModule->GetGlobalParametersList().GetAt(selItem)->IsMovable())
		{
			GetDlgItem(IDC_TABGLOBALTODBFBTN)->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem(IDC_TABGLOBALTODBFBTN)->EnableWindow(FALSE);
		}
		if(m_GlobalParListCtrl.GetValidationPassed())
		{
			m_GlobalDescriptionCtrl.SetWindowTextA(m_pModule->GetGlobalParametersList().GetAt(selItem)->GetDescription().c_str());
		}
	}
	else
	{
		// no selection
		GetDlgItem(IDC_TABGLOBALEDITBTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABGLOBALTODBFBTN)->EnableWindow(FALSE);
		m_GlobalDescriptionCtrl.SetWindowTextA("");
	}

	*pResult = 0;
}

// -------------------------------------------------------------------------- //

void CTabGlobalDlg::OnLvnItemActivateTabGlobalParList(NMHDR *pNMHDR, LRESULT *pResult)
{
	OnBnClickedTabGlobalEditBtn();

	*pResult = 0;
}

// -------------------------------------------------------------------------- //

void CTabGlobalDlg::OnLvnKeydownTabGlobalParList(NMHDR *pNMHDR, LRESULT *pResult)
{
	OnNMClickTabGlobalParList(pNMHDR, pResult);

	*pResult = 0;
}

// -------------------------------------------------------------------------- //
// OBJECT LIST BUTTONS HANDLERS                                               //
// -------------------------------------------------------------------------- //

void CTabGlobalDlg::OnBnClickedTabGlobalEditBtn()
{
	// gets the index of the selected item in the list
	int selItem = m_GlobalParListCtrl.GetNextItem(-1, LVNI_SELECTED);

	// sets a copy of the parameter
	LBParameter* lbP = new LBParameter(m_pModule->GetGlobalParametersList().GetAt(selItem));	

	m_EditParameterDlg.SetParameter(lbP);
	if(m_EditParameterDlg.DoModal() == IDOK)
	{
		m_pModule->SetGlobalParameter(selItem, lbP);
		UpdateState();
	}
	else
	{
		delete lbP;
	}
}

// -------------------------------------------------------------------------- //

void CTabGlobalDlg::OnBnClickedTabGlobalClearBtn()
{
	string text = MSG_CONFIRMREMOVEALLGLOB;
	if(MessageBox(text.c_str(), "", MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
		m_pModule->ClearAllGlobalParameters();
		UpdateState();
	}
}

// -------------------------------------------------------------------------- //

void CTabGlobalDlg::OnBnClickedTabGlobalToDbfBtn()
{
	// gets the index of the selected item in the list
	int selItem = m_GlobalParListCtrl.GetNextItem(-1, LVNI_SELECTED);

	LBParameter* par = m_pModule->GetGlobalParametersList().GetAt(selItem);

	// move the parameter from one list to the other
	par->ClearValue();
	// passes as value to create a copy of the data
	m_pModule->GetDbfParametersList().Append(*par);
	m_pModule->GetGlobalParametersList().RemoveAt(selItem);
	UpdateState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateDbfTabDialog(false);
}

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

bool CTabGlobalDlg::ValidateParamCtrlInput(CString data, LBParameter* par)
{
	switch(par->GetType())
	{
	case LBPT_INTEGER:
		{
			if(!IsInteger(string(data), true))
			{
				AfxGetMainWnd()->MessageBox(string(MSG_MUSTBEINTEGER).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
				return false;
			}
			else
			{
				return true;
			}
		}
	case LBPT_FLOAT:
		{
			if(!IsFloatingPoint(string(data), true))
			{
				AfxGetMainWnd()->MessageBox(string(MSG_MUSTBEFLOATING).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
				return false;
			}
			else
			{
				return true;
			}
		}
	case LBPT_BOOL:
		{
			if(!IsBool(string(data), true))
			{
				AfxGetMainWnd()->MessageBox(string(MSG_MUSTBEZEROORONE).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
				return false;
			}
			else
			{
				return true;
			}
		}
	case LBPT_STRING: return true;
	default:
		AfxGetMainWnd()->MessageBox(string(MSG_DATANOTRECOGNIZED).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
		return false;
	}
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CTabGlobalDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == MY_ENDEDITMSG)
	{
		// gets the index of the selected item in the list
		int selItem = static_cast<int>(pMsg->wParam);

		LBParameter* par = m_pModule->GetGlobalParametersList().GetAt(selItem);
		CString data = m_GlobalParListCtrl.GetItemText(selItem, 1);

		if(ValidateParamCtrlInput(data, par))
		{
			par->SetValue(data);
			m_pModule->GetGlobalParametersList().SetAt(selItem, par);
			m_GlobalParListCtrl.SetValidationPassed(true);
			UpdateState();
		}
		else
		{
			m_GlobalParListCtrl.SetValidationPassed(false);
			m_GlobalParListCtrl.SetItemText(selItem, 1, par->GetValue().c_str());
			m_GlobalParListCtrl.SetItemState(selItem, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
			m_GlobalParListCtrl.SetFocus();
		}
	}

	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


