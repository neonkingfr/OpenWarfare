//-----------------------------------------------------------------------------

#include "stdafx.h"

#include ".\LBVisitorMap.h"

//-----------------------------------------------------------------------------

IMPLEMENT_SERIAL( LBVisitorMap, CObject, 1 )

//-----------------------------------------------------------------------------

LBVisitorMap::LBVisitorMap() 
: CObject()
, m_version( VERSION_NUMBER )
{
	SetDefault();
}

//-----------------------------------------------------------------------------

LBVisitorMap::LBVisitorMap( const LBVisitorMap& other ) 
: m_version( other.m_version )
, m_parametersList( other.m_parametersList )
{
}

//-----------------------------------------------------------------------------

LBVisitorMap::~LBVisitorMap()
{
}

//-----------------------------------------------------------------------------

LBVisitorMap& LBVisitorMap::operator = ( const LBVisitorMap& other )
{
	m_version        = other.m_version;
	m_parametersList = other.m_parametersList;

	return (*this);
}

//-----------------------------------------------------------------------------
	
LBParameterList& LBVisitorMap::GetParametersList() 
{
	return (m_parametersList);
}

//-----------------------------------------------------------------------------

void LBVisitorMap::SetParameterList( const LBParameterList& parametersList )
{
	m_parametersList = parametersList;
}

//-----------------------------------------------------------------------------

void LBVisitorMap::SetParameter( int index, LBParameter* parameter )
{
	m_parametersList.SetAt( index, parameter );
}

//-----------------------------------------------------------------------------

void LBVisitorMap::Reset()
{
	m_parametersList.RemoveAll();
}

//-----------------------------------------------------------------------------

void LBVisitorMap::SetDefault()
{
	m_parametersList.Append( new LBParameter( MAPFRAME_LEFT  , "",  PARDESC_LEFT       , LBPT_FLOAT, false, false, false ) );
	m_parametersList.Append( new LBParameter( MAPFRAME_BOTTOM, "",  PARDESC_BOTTOM     , LBPT_FLOAT, false, false, false ) );
	m_parametersList.Append( new LBParameter( MAPFRAME_WIDTH , "",  PARDESC_SIZEX      , LBPT_FLOAT, false, false, false ) );
	m_parametersList.Append( new LBParameter( MAPFRAME_HEIGHT, "",  PARDESC_SIZEY      , LBPT_FLOAT, false, false, false ) );
	m_parametersList.Append( new LBParameter( HIGHMAPGRID_X  , "",  PARDESC_GRIDX      , LBPT_FLOAT, true, false, false ) );
	m_parametersList.Append( new LBParameter( HIGHMAPGRID_Y  , "",  PARDESC_GRIDY      , LBPT_FLOAT, true, false, false ) );
	m_parametersList.Append( new LBParameter( VISITOR_VERSION, "4", PARDESC_VISITORVER , LBPT_INTEGER, true, false, false ) );
}

//-----------------------------------------------------------------------------

LBErrorTypes LBVisitorMap::Validate()
{
	if ( !m_parametersList.Validate() )
	{
		return (LBET_MISSINGMAPPARAMETER);
	}

	return (LBET_NOERROR);
}

//-----------------------------------------------------------------------------

double LBVisitorMap::GetMapLeft() const
{
	INT_PTR visParamCount = m_parametersList.GetCount();
	for ( int i = 0; i < visParamCount; ++i )
	{
		const LBParameter* item = m_parametersList.GetAt( i );
		if ( item->GetName() == MAPFRAME_LEFT )
		{
			return (atof( item->GetValue().c_str() ));
		}
	}
	return (0.0);
}

//-----------------------------------------------------------------------------

double LBVisitorMap::GetMapTop() const
{
	return (GetMapBottom() + GetMapHeight());
}

//-----------------------------------------------------------------------------

double LBVisitorMap::GetMapRight() const
{
	return (GetMapLeft() + GetMapWidth());
}

//-----------------------------------------------------------------------------

double LBVisitorMap::GetMapBottom() const
{
	INT_PTR visParamCount = m_parametersList.GetCount();
	for ( int i = 0; i < visParamCount; ++i )
	{
		const LBParameter* item = m_parametersList.GetAt( i );
		if ( item->GetName() == MAPFRAME_BOTTOM )
		{
			return (atof( item->GetValue().c_str() ));
		}
	}
	return (0.0);
}

//-----------------------------------------------------------------------------

double LBVisitorMap::GetMapWidth() const
{
	INT_PTR visParamCount = m_parametersList.GetCount();
	for ( int i = 0; i < visParamCount; ++i )
	{
		const LBParameter* item = m_parametersList.GetAt( i );
		if ( item->GetName() == MAPFRAME_WIDTH )
		{
			return (atof( item->GetValue().c_str() ));
		}
	}
	return (0.0);
}

//-----------------------------------------------------------------------------

double LBVisitorMap::GetMapHeight() const
{
	INT_PTR visParamCount = m_parametersList.GetCount();
	for ( int i = 0; i < visParamCount; ++i )
	{
		const LBParameter* item = m_parametersList.GetAt( i );
		if ( item->GetName() == MAPFRAME_HEIGHT )
		{
			return (atof( item->GetValue().c_str() ));
		}
	}
	return (0.0);
}

//-----------------------------------------------------------------------------

double LBVisitorMap::GetGridX() const
{
	INT_PTR visParamCount = m_parametersList.GetCount();
	for ( int i = 0; i < visParamCount; ++i )
	{
		const LBParameter* item = m_parametersList.GetAt( i );
		if ( item->GetName() == HIGHMAPGRID_X )
		{
			return (atof( item->GetValue().c_str() ));
		}
	}
	return (0.0);
}

//-----------------------------------------------------------------------------

double LBVisitorMap::GetGridY() const
{
	INT_PTR visParamCount = m_parametersList.GetCount();
	for ( int i = 0; i < visParamCount; ++i )
	{
		const LBParameter* item = m_parametersList.GetAt( i );
		if ( item->GetName() == HIGHMAPGRID_Y )
		{
			return (atof( item->GetValue().c_str() ));
		}
	}
	return (0.0);
}

//-----------------------------------------------------------------------------

int LBVisitorMap::GetVisitorVersion() const
{
	INT_PTR visParamCount = m_parametersList.GetCount();
	for ( int i = 0; i < visParamCount; ++i )
	{
		const LBParameter* item = m_parametersList.GetAt( i );
		if ( item->GetName() == VISITOR_VERSION )
		{
			return (atoi( item->GetValue().c_str() ));
		}
	}
	return (4);
}

//-----------------------------------------------------------------------------

string LBVisitorMap::GetMapLeftAsString() const
{
  char buf[81];
  sprintf_s( buf, 80, "%f", GetMapLeft() );

  string ret = string( buf );
  string::size_type pos = ret.find( '.' );
  if ( pos != string::npos )
  {
    while ( ret[ret.length() - 1] == '0' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }

    if ( ret[ret.length() - 1] == '.' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }
  }
  return (ret);
}

//-----------------------------------------------------------------------------

string LBVisitorMap::GetMapTopAsString() const
{
  char buf[81];
  sprintf_s( buf, 80, "%f", GetMapTop() );

  string ret = string( buf );
  string::size_type pos = ret.find( '.' );
  if ( pos != string::npos )
  {
    while ( ret[ret.length() - 1] == '0' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }

    if ( ret[ret.length() - 1] == '.' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }
  }
  return (ret);
}

//-----------------------------------------------------------------------------

string LBVisitorMap::GetMapRightAsString() const
{
  char buf[81];
  sprintf_s( buf, 80, "%f", GetMapRight() );

  string ret = string( buf );
  string::size_type pos = ret.find( '.' );
  if ( pos != string::npos )
  {
    while ( ret[ret.length() - 1] == '0' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }

    if ( ret[ret.length() - 1] == '.' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }
  }
  return (ret);
}

//-----------------------------------------------------------------------------

string LBVisitorMap::GetMapBottomAsString() const
{
  char buf[81];
  sprintf_s( buf, 80, "%f", GetMapBottom() );

  string ret = string( buf );
  string::size_type pos = ret.find( '.' );
  if ( pos != string::npos )
  {
    while ( ret[ret.length() - 1] == '0' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }

    if ( ret[ret.length() - 1] == '.' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }
  }
  return (ret);
}

//-----------------------------------------------------------------------------

string LBVisitorMap::GetMapWidthAsString() const
{
  char buf[81];
  sprintf_s( buf, 80, "%f", GetMapWidth() );

  string ret = string( buf );
  string::size_type pos = ret.find( '.' );
  if ( pos != string::npos )
  {
    while ( ret[ret.length() - 1] == '0' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }

    if ( ret[ret.length() - 1] == '.' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }
  }
  return (ret);
}

//-----------------------------------------------------------------------------

string LBVisitorMap::GetMapHeightAsString() const
{
  char buf[81];
  sprintf_s( buf, 80, "%f", GetMapHeight() );

  string ret = string( buf );
  string::size_type pos = ret.find( '.' );
  if ( pos != string::npos )
  {
    while ( ret[ret.length() - 1] == '0' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }

    if ( ret[ret.length() - 1] == '.' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }
  }
  return (ret);
}

//-----------------------------------------------------------------------------

string LBVisitorMap::GetGridXAsString() const
{
  char buf[81];
  sprintf_s( buf, 80, "%f", GetGridX() );

  string ret = string( buf );
  string::size_type pos = ret.find( '.' );
  if ( pos != string::npos )
  {
    while ( ret[ret.length() - 1] == '0' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }

    if ( ret[ret.length() - 1] == '.' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }
  }
  return (ret);
}

//-----------------------------------------------------------------------------

string LBVisitorMap::GetGridYAsString() const
{
  char buf[81];
  sprintf_s( buf, 80, "%f", GetGridY() );

  string ret = string( buf );
  string::size_type pos = ret.find( '.' );
  if ( pos != string::npos )
  {
    while ( ret[ret.length() - 1] == '0' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }

    if ( ret[ret.length() - 1] == '.' )
    {
      ret = ret.substr( 0, ret.length() - 1 );
    }
  }
  return (ret);
}

//-----------------------------------------------------------------------------

string LBVisitorMap::GetVisitorVersionAsString() const
{
  int ver = GetVisitorVersion();
  if ( ver != 3 ) { ver = 4; }

  char buf[81];
  sprintf_s( buf, 80, "%d", ver );

  string ret = string( buf );
  return (ret);
}

//-----------------------------------------------------------------------------

void LBVisitorMap::Serialize( CArchive& ar )
{
	CObject::Serialize( ar );

	m_parametersList.Serialize( ar );

	if ( ar.IsStoring() )
	{
		ar << m_version;
	}
	else
	{
		ar >> m_version;
    if ( m_version < 80 )
    {
      string strGridX  = "";
      string strGridY  = "";
      double left;
      double top;
      double right;
      double bottom;
	    INT_PTR visParamCount = m_parametersList.GetCount();
	    for ( int i = 0; i < visParamCount; ++i )
	    {
		    LBParameter* item = m_parametersList.GetAt( i );
		    if ( item->GetName() == MAPAREA_LEFT )
		    {
			    left = atof( item->GetValue().c_str() );
		    }
        else if ( item->GetName() == MAPAREA_TOP )
		    {
			    top = atof( item->GetValue().c_str() );
		    }
        else if ( item->GetName() == MAPAREA_RIGHT )
		    {
			    right = atof( item->GetValue().c_str() );
		    }
        else if ( item->GetName() == MAPAREA_BOTTOM )
		    {
			    bottom = atof( item->GetValue().c_str() );
		    }
        else if ( item->GetName() == HIGHMAPGRID_X )
		    {
			    strGridX = item->GetValue();
		    }
        else if ( item->GetName() == HIGHMAPGRID_Y )
		    {
			    strGridY = item->GetValue();
		    }
	    }
      double width  = abs( right - left );
      double height = abs( top - bottom );

      char buf[81];
      sprintf_s( buf, 80, "%.3f", left );
      string strLeft = string( buf );
      sprintf_s( buf, 80, "%.3f", top ); // top and bottom were swapped before 80
      string strBottom = string( buf );
      sprintf_s( buf, 80, "%.3f", width );
      string strWidth = string( buf );
      sprintf_s( buf, 80, "%.3f", height );
      string strHeight = string( buf );

      m_parametersList.RemoveAll();
      SetDefault();

	    visParamCount = m_parametersList.GetCount();
	    for ( int i = 0; i < visParamCount; ++i )
	    {
		    LBParameter* item = m_parametersList.GetAt( i );

        switch ( i )
        {
        case 0: // MAPFRAME_LEFT
          item->SetValue( strLeft );
          break;
        case 1: // MAPFRAME_BOTTOM
          item->SetValue( strBottom );
          break;
        case 2: // MAPFRAME_WIDTH
          item->SetValue( strWidth );
          break;
        case 3: // MAPFRAME_HEIGHT
          item->SetValue( strHeight );
          break;
        case 4: // HIGHMAPGRID_X
          item->SetValue( strGridX );
          break;
        case 5: // HIGHMAPGRID_Y
          item->SetValue( strGridY );
          break;
        }
      }
    }

    if ( m_version == 80 )
    {
    	m_parametersList.Append( new LBParameter( VISITOR_VERSION, "4", PARDESC_VISITORVER , LBPT_INTEGER, true, false, false ) );
    }

    m_version = VERSION_NUMBER;
	}
}

//-----------------------------------------------------------------------------
