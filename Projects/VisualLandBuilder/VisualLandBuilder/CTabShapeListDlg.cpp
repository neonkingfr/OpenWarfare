// CTabShapeListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VisualLandBuilder.h"
#include "CTabShapeListDlg.h"

#include ".\CPreviewDlg.h"

// CTabShapeListDlg dialog

IMPLEMENT_DYNAMIC(CTabShapeListDlg, CDialog)

// -------------------------------------------------------------------------- //

CTabShapeListDlg::CTabShapeListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTabShapeListDlg::IDD, pParent)
{
}

// -------------------------------------------------------------------------- //

CTabShapeListDlg::~CTabShapeListDlg()
{
}

// -------------------------------------------------------------------------- //

void CTabShapeListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SHAPELIST, m_ShapeList);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CTabShapeListDlg, CDialog)
	ON_WM_SIZE()
	ON_NOTIFY(NM_CLICK, IDC_SHAPELIST, &CTabShapeListDlg::OnNMClickShapeList)
	ON_NOTIFY(TVN_SELCHANGED, IDC_SHAPELIST, &CTabShapeListDlg::OnTvnSelChangedShapeList)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

void CTabShapeListDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	CRect dlgRect;
	GetClientRect(dlgRect);

	CWnd* group = GetDlgItem(IDC_SHAPELISTGROUP);
	CWnd* list = GetDlgItem(IDC_SHAPELIST);

	if(group)
	{
		int groupLeft   = dlgRect.left + 1;
		int groupTop    = dlgRect.top;// + 1;
		int groupRight  = dlgRect.right - 1;
		int groupBottom = dlgRect.bottom - 1;
		int groupWidth  = groupRight - groupLeft;
		int groupHeight = groupBottom - groupTop;
		group->MoveWindow(groupLeft, groupTop, groupWidth, groupHeight, TRUE);		
		list->MoveWindow(groupLeft + 3, groupTop + 9, groupWidth - 6, groupHeight - 12, TRUE);		
	}
}

// -------------------------------------------------------------------------- //
// INTERFACE                                                                  //
// -------------------------------------------------------------------------- //

void CTabShapeListDlg::SetList(LBModuleList* moduleList, AutoArray<pShapeOrganizer>* shapeOrganizerList, bool showAll)
{
	m_ShapeList.DeleteAllItems();

	INT_PTR moduleCount = moduleList->GetCount();
	for(int i = 0; i < moduleCount; ++i)
	{
		LBModule* module = moduleList->GetAt(i);
		if(module->GetHasPreview())
		{
			if(module->IsActive() || showAll)
			{
				if(module->GetUseShapefile())
				{
					HTREEITEM moduleItem = m_ShapeList.InsertItem(module->GetVLBName().c_str(), TVI_ROOT);
					Shapes::ShapeList& shapeList = (*shapeOrganizerList)[i]->GetShapeList();
					int shapesCount = shapeList.Size();
					for(int j = 0; j < shapesCount; ++j)
					{
						char buf[81];
						sprintf_s(buf, 80, "%d", (j + 1));
						HTREEITEM shapeItem = m_ShapeList.InsertItem(string(buf).c_str(), moduleItem, TVI_LAST);
						m_ShapeList.SetItemData(shapeItem, (DWORD)i);
					}
				}
			}
		}
	}
}

// -------------------------------------------------------------------------- //
// LIST HANDLERS                                                              //
// -------------------------------------------------------------------------- //

void CTabShapeListDlg::OnNMClickShapeList(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;
}

// -------------------------------------------------------------------------- //

void CTabShapeListDlg::OnTvnSelChangedShapeList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	HTREEITEM hItem = m_ShapeList.GetSelectedItem();
	if(hItem != NULL)
	{
		if(m_ShapeList.ItemHasChildren(hItem))
		{
			reinterpret_cast<CPreviewDlg*>(GetParent()->GetParent())->SetSelectedShape(-1, -1);
		}
		else
		{
			int moduleIndex = (int)m_ShapeList.GetItemData(hItem);
			CString itemTxt = m_ShapeList.GetItemText(hItem);
			int shapeIndex = atoi(string(itemTxt).c_str()) - 1;
			reinterpret_cast<CPreviewDlg*>(GetParent()->GetParent())->SetSelectedShape(moduleIndex, shapeIndex);
		}
	}

	*pResult = 0;
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CTabShapeListDlg::PreTranslateMessage(MSG* pMsg)
{
	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

// -------------------------------------------------------------------------- //