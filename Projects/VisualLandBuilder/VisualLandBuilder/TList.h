#pragma once

#include <string>

#include ".\TList.h"

using std::string;

template <class T>
class TList : public CObject
{
	DECLARE_SERIAL(TList<T>)

	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the list of parameters
	CTypedPtrList<CObList, T*> m_List;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	TList();

	// copy constructor
	TList(const TList& other);

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~TList();

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	TList& operator = (const LBObjecTList& other);

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //

	// GetList()
	// returns the list
	CTypedPtrList<CObList, T*>& GetList();

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //

	// SetList()
	// sets the list with the given one
	void SetList(CTypedPtrList<CObList, T*>& newList);

	// ************************************************************** //
	// Member variables manipulators                                  //
	// ************************************************************** //
	
	// Append()
	// adds a copy of the the given T at the end of this list
	void Append(const T& t);
	void Append(T* t);

	// GetAt()
	// returns the T of this list at the given index 
	T* GetAt(int index);

	// SetAt()
	// sets the T of this list at the given index with the given T
	void SetAt(int index, T* t);

	// RemoveAt()
	// removes the T at the given index from this list
	void RemoveAt(int index);

	// RemoveAll()
	// removes all the Ts from this list
	void RemoveAll();

	// ************************************************************** //
	// Interface                                                      //
	// ************************************************************** //

	// GetCount()
	// returns the number of items in this list
	INT_PTR GetCount();

	// IsEmpty()
	// returns true if the list is empty
	BOOL IsEmpty();

	// ************************************************************** //
	// Serialization                                                  //
	// ************************************************************** //
	virtual void Serialize(CArchive& ar);
};

#include ".\TList.inl"
