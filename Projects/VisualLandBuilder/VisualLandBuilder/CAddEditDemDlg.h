//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "afxwin.h"
#include "afxcmn.h"

#include ".\LBDem.h"
#include ".\LBVisitorMap.h"
#include ".\ExtendedControls\CListCtrlEx.h"
#include ".\CEditParameterDlg.h"

//-----------------------------------------------------------------------------

class CAddEditDemDlg : public CDialog
{
	DECLARE_DYNAMIC( CAddEditDemDlg )

public:
	CAddEditDemDlg( CWnd* parent = NULL );   // standard constructor
	virtual ~CAddEditDemDlg();

// Dialog Data
	enum 
  { 
    IDD = IDD_ADDEDITDEM 
  };

protected:
	virtual void DoDataExchange( CDataExchange* dx );    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	// the file name control
	CStatic m_fileNameCtrl;
	// the type control
	CComboBox m_typeListCtrl;
	// the parameters list control
	CListCtrlEx m_parametersListCtrl;
	// the dialog for add/edit parameter
	CEditParameterDlg m_editParameterDlg;
	// the description ctrl
	CStatic m_demDescriptionCtrl;

  // will be set to true if the data are modified
	bool m_modified;
	// the working copy of the map data
	const LBVisitorMap* m_visitorMap;

	// DATA
	// the pointer to the original data
	LBDem* m_oldDem;
	// the working copy of the data
	LBDem m_dem;

private:
	virtual BOOL OnInitDialog();

private:
	// updates the list control and sets m_Modified if data have been changed
	void UpdateParametersListCtrl();
	// verify the inputs in the parameters ctrl
	bool ValidateParamCtrlInput( CString data, LBParameter* par );
	// checks if the ok button can be enabled
	void CheckOkButtonEnabling();

public:
	// sets the working object and the pointer to the original one
	//  !! must be called before the DoModal function of this dialog !!
	void SetDem( LBDem* dem );

	void SetVisitorMap( const LBVisitorMap* visitorMap );

private:
	afx_msg void OnNMClickParametersList( NMHDR* nmhdr, LRESULT* result );
	afx_msg void OnLvnItemActivateParametersList( NMHDR* nmhdr, LRESULT* result );
	afx_msg void OnLvnKeydownParametersList( NMHDR* nmhdr, LRESULT* result );

private:
	afx_msg void OnBnClickedEdit();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBrowse();
	afx_msg void OnBnClickedMapData();

	afx_msg void OnCbnSelChangeDemTypeCombo();

private:
	// this function is used to intercept the end label editing in
	// the list ctrl
	virtual BOOL PreTranslateMessage( MSG* msg );
};

//-----------------------------------------------------------------------------
