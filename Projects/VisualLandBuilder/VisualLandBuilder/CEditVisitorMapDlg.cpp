//-----------------------------------------------------------------------------

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CEditVisitorMapDlg.h"

#include "..\..\LandBuilder\HighMapLoaders\include\EtHelperFunctions.h"

//-----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC( CEditVisitorMapDlg, CDialog )

//-----------------------------------------------------------------------------

CEditVisitorMapDlg::CEditVisitorMapDlg( CWnd* parent /*=NULL*/ )
: CDialog( CEditVisitorMapDlg::IDD, parent )
{
}

//-----------------------------------------------------------------------------

CEditVisitorMapDlg::~CEditVisitorMapDlg()
{
}

//-----------------------------------------------------------------------------

void CEditVisitorMapDlg::DoDataExchange( CDataExchange* dx )
{
	CDialog::DoDataExchange( dx );
	DDX_Control( dx, IDC_VISMAPPARAMETERS,  m_parameterListCtrl );
	DDX_Control( dx, IDC_VISMAPDESCRIPTION, m_visMapDescriptionCtrl );
}

//-----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP( CEditVisitorMapDlg, CDialog )
	ON_NOTIFY( LVN_ITEMACTIVATE, IDC_VISMAPPARAMETERS, &CEditVisitorMapDlg::OnLvnItemActivateVisMapParameters )
	ON_NOTIFY( NM_CLICK,         IDC_VISMAPPARAMETERS, &CEditVisitorMapDlg::OnNMClickVisMapParameters )
	ON_NOTIFY( LVN_KEYDOWN,      IDC_VISMAPPARAMETERS, &CEditVisitorMapDlg::OnLvnKeydownVisMapParameters )
	ON_BN_CLICKED( IDC_VISMAPEDITBTN, &CEditVisitorMapDlg::OnBnClickedVisMapEditBtn )
	ON_BN_CLICKED( IDOK,              &CEditVisitorMapDlg::OnBnClickedOk )
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------

BOOL CEditVisitorMapDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// sets the list column header
	m_parameterListCtrl.InsertColumn( 0, string( HEAD_PARAMETER ).c_str(), LVCFMT_LEFT );
	m_parameterListCtrl.SetColumnWidth( 0, 120 );
	m_parameterListCtrl.InsertColumn( 1, string( HEAD_VALUE ).c_str(), LVCFMT_LEFT );
	m_parameterListCtrl.SetColumnWidth( 1, 120 );

	// sets the list style
	m_parameterListCtrl.SetExtendedStyle( LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT );

	// sets the in-place editing object for the list 
	m_parameterListCtrl.SetEditType( ET_EDIT );

	// sets the list data
	UpdateParameterListCtrl();

	return (TRUE);
}

//-----------------------------------------------------------------------------

void CEditVisitorMapDlg::UpdateParameterListCtrl()
{
	bool firstTime = false;

	INT_PTR listCount = m_visitorMap.GetParametersList().GetCount();
	int listCtrlCount = m_parameterListCtrl.GetItemCount();

	// if the counts of item are different it means that this is the first 
	// time we enter this function
  if ( listCount != listCtrlCount ) { firstTime = true; }

	INT_PTR visParamCount = m_visitorMap.GetParametersList().GetCount();
	for ( int i = 0; i < visParamCount; ++i )
	{
		LBParameter* item = m_visitorMap.GetParametersList().GetAt( i );
		if ( firstTime )
		{
			// sets first column
			m_parameterListCtrl.InsertItem( i, item->GetName().c_str() );
		}
		// if the parameter has no default, show it bolded
		if ( !item->GetHasDefault() )
		{
			m_parameterListCtrl.SetItemStyle( i, 0, LIS_BOLD, true );
		}
		// modify the second column only if the data are changed
		string newValue = item->GetValue();
		CString oldValue = m_parameterListCtrl.GetItemText( i, 1 );
		if ( newValue != string( oldValue ) )
		{
			// sets second column
			m_parameterListCtrl.SetItem( i, 1, LVIF_TEXT, newValue.c_str(), 0, 0, 0, 0 );
			
			// if data are changed update m_Modified
      if ( !firstTime ) { m_modified = true; }
		}
	}
}

//-----------------------------------------------------------------------------

void CEditVisitorMapDlg::SetVisitorMap( LBVisitorMap* visitorMap )
{
	// sets the working copy of the data
	m_visitorMap = *visitorMap;

	// sets the pointer to the original data
	m_oldVisitorMap = visitorMap;
}

//-----------------------------------------------------------------------------

bool CEditVisitorMapDlg::ValidateParamCtrlInput( const CString& data, const LBParameter* par )
{
	switch ( par->GetType() )
	{
	case LBPT_INTEGER:
		{
			if ( !IsInteger( string( data ), true ) )
			{
				AfxGetMainWnd()->MessageBox( string( MSG_MUSTBEINTEGER ).c_str(), string( MSG_ERROR ).c_str(), 
                                     MB_OK | MB_ICONSTOP );
				return (false);
			}
			else
			{
				return (true);
			}
		}
	case LBPT_FLOAT:
		{
			if ( !IsFloatingPoint( string( data ), true ) )
			{
				AfxGetMainWnd()->MessageBox( string( MSG_MUSTBEFLOATING ).c_str(), string( MSG_ERROR ).c_str(), 
                                     MB_OK | MB_ICONSTOP );
				return (false);
			}
			else
			{
				return (true);
			}
		}
	case LBPT_BOOL:
		{
			if ( !IsBool( string( data ), true ) )
			{
				AfxGetMainWnd()->MessageBox( string( MSG_MUSTBEZEROORONE ).c_str(), string( MSG_ERROR ).c_str(), 
                                     MB_OK | MB_ICONSTOP );
				return (false);
			}
			else
			{
				return (true);
			}
		}
	case LBPT_STRING: 
    {
      return (true);
    }
	default:
		AfxGetMainWnd()->MessageBox( string( MSG_DATANOTRECOGNIZED ).c_str(), string( MSG_ERROR ).c_str(), 
                                 MB_OK | MB_ICONSTOP );
		return (false);
	}
}

//-----------------------------------------------------------------------------

void CEditVisitorMapDlg::OnLvnItemActivateVisMapParameters( NMHDR* nmhdr, LRESULT* result )
{
	OnBnClickedVisMapEditBtn();

	*result = 0;
}

//-----------------------------------------------------------------------------

void CEditVisitorMapDlg::OnNMClickVisMapParameters( NMHDR* nmhdr, LRESULT* result )
{
	// gets the index of the selected item in the list
	int selItem = m_parameterListCtrl.GetNextItem( -1, LVNI_SELECTED );

	// enables/disables edit and remove button
	if ( selItem != -1 )
	{
		LPNMLVKEYDOWN lVKeyDow = reinterpret_cast< LPNMLVKEYDOWN >( nmhdr );
    if ( lVKeyDow->wVKey == VK_UP )   { selItem--; }
    if ( lVKeyDow->wVKey == VK_DOWN ) { selItem++; }
    if ( selItem < 0 ) { selItem = 0; }
    if ( selItem > m_parameterListCtrl.GetItemCount() - 1 ) { selItem = m_parameterListCtrl.GetItemCount() - 1; }

		// something is selected
		GetDlgItem( IDC_VISMAPEDITBTN )->EnableWindow( TRUE );
		if ( m_parameterListCtrl.GetValidationPassed() )
		{
			m_visMapDescriptionCtrl.SetWindowTextA( m_visitorMap.GetParametersList().GetAt( selItem )->GetDescription().c_str() );
		}
	}
	else
	{
		// no selection
		GetDlgItem( IDC_VISMAPEDITBTN )->EnableWindow( FALSE );
		m_visMapDescriptionCtrl.SetWindowTextA( "" );
	}

	*result = 0;
}

//-----------------------------------------------------------------------------

void CEditVisitorMapDlg::OnLvnKeydownVisMapParameters( NMHDR* nmhdr, LRESULT* result )
{
	OnNMClickVisMapParameters( nmhdr, result );

	*result = 0;
}

//-----------------------------------------------------------------------------

void CEditVisitorMapDlg::OnBnClickedVisMapEditBtn()
{
	// gets the index of the selected item in the list
	int selItem = m_parameterListCtrl.GetNextItem( -1, LVNI_SELECTED );

	// sets a copy of the parameter
	LBParameter* lbP = new LBParameter( m_visitorMap.GetParametersList().GetAt( selItem ) );	

	m_editParameterDlg.SetParameter( lbP );
	if ( m_editParameterDlg.DoModal() == IDOK )
	{
		m_visitorMap.SetParameter( selItem, lbP );
		UpdateParameterListCtrl();
	}
	else
	{
		delete lbP;
	}
}

//-----------------------------------------------------------------------------

void CEditVisitorMapDlg::OnBnClickedOk()
{
	if ( m_modified )
	{
		// updates data
		*m_oldVisitorMap = m_visitorMap;
	}
	OnOK();
}

//-----------------------------------------------------------------------------

BOOL CEditVisitorMapDlg::PreTranslateMessage( MSG* msg )
{
	if ( msg->message == MY_ENDEDITMSG )
	{
		// gets the index of the selected item in the list
		int selItem = msg->wParam;

		LBParameter* par = m_visitorMap.GetParametersList().GetAt( selItem );
		CString data = m_parameterListCtrl.GetItemText( selItem, 1 );

		if ( ValidateParamCtrlInput( data, par ) )
		{
			par->SetValue( data );
			m_visitorMap.GetParametersList().SetAt( selItem, par );
			m_parameterListCtrl.SetValidationPassed( true );
			UpdateParameterListCtrl();
			m_modified = true;
		}
		else
		{
			m_parameterListCtrl.SetValidationPassed( false );
			m_parameterListCtrl.SetItemText( selItem, 1, par->GetValue().c_str() );
			m_parameterListCtrl.SetItemState( selItem, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED );
			m_parameterListCtrl.SetFocus();
		}
	}

	return (CDialog::PreTranslateMessage( msg ));
}

//-----------------------------------------------------------------------------
