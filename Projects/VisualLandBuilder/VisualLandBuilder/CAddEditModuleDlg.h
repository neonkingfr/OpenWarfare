//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "afxwin.h"

#include ".\CParamTabCtrl.h"
#include ".\LBModuleList.h"
#include ".\CPreviewDlg.h"

#include "..\..\LandBuilder\Shapes\ShapeOrganizer.h"

#include ".\External\TransparentImage\TransparentImage.h"

#include "afxcmn.h"

//-----------------------------------------------------------------------------

class CAddEditModuleDlg : public CDialog
{
	DECLARE_DYNAMIC( CAddEditModuleDlg )

public:
	CAddEditModuleDlg( const LBVisitorMap* visitorMap, CWnd* parent = NULL );   // standard constructor
	virtual ~CAddEditModuleDlg();

	enum 
  { 
    IDD = IDD_ADDMODULE 
  };

protected:
	virtual void DoDataExchange( CDataExchange* dX );    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	// the module list control
	CListBox m_moduleListCtrl;
	// the description static control
	CStatic m_descriptionCtrl;
	// the param tab control
	CParamTabCtrl m_paramTabCtrl;
	// the image to draw with transparency
	CTransparentImage m_image;

	// the child dialogs
	CTabObjectDlg*    m_objDlg;
	CTabGlobalDlg*    m_gloDlg;
	CTabDbfDlg*       m_dbfDlg;
	CTabShapefileDlg* m_shaDlg;
	CTabDemsDlg*      m_demDlg;
	CTabNotesDlg*     m_notDlg;

	// the preview dialog
	CPreviewDlg m_previewDlg;

	// the shape organizer used to read shape files
	Shapes::ShapeOrganizer m_shapeOrganizer;

	// DATA
	// the pointer to the original data
	LBModule* m_oldModule;
	// the working copy of the data
	LBModule m_module;

	// the list used for the preview
	LBModuleList m_moduleList;

	// the working copy of the map data
	const LBVisitorMap* m_visitorMap;

private:
	virtual BOOL OnInitDialog();

public:
	// updates the state of the module list
	// disabled if there are data already inputted
	void UpdateModuleListCtrlState();
	// updates the state of the clear all data btn
	// enabled if there is at least another clear btn enabled
	void UpdateClearAllDataBtnState();

private:
	// updates the object tab dialog and sets m_Modified if data have been changed
	// if the given parameter is true, update also the column header
	void UpdateObjectTabDialog( bool header );

public:
	// updates the global tab dialog and sets m_Modified if data have been changed
	void UpdateGlobalTabDialog();
	// updates the dbf tab dialog and sets m_Modified if data have been changed
	void UpdateDbfTabDialog( bool reloadDbfFields );

private:
	// updates the shapefile tab dialog and sets m_Modified if data have been changed
	void UpdateShapeTabDialog();
	// updates the dems tab dialog and sets m_Modified if data have been changed
	void UpdateDemsTabDialog();
	// updates the notes tab dialog and sets m_Modified if data have been changed
	void UpdateNotesTabDialog();

public:
	// updates the image of the object tab
	void UpdateObjectTabImage();
	// updates the image of the global tab
	void UpdateGlobalTabImage();
	// updates the image of the dbf tab
	void UpdateDbfTabImage();
	// updates the image of the shape tab
	void UpdateShapeTabImage();
	// updates the image of the dem tab
	void UpdateDemTabImage();
	// updates the image of the notes tab
	void UpdateNotesTabImage();

private:
	// sets the module list control with the available predefined modules
	void SetModuleListCtrl();

public:
	// load data from shapefile
	void LoadShapeFile();
  // unload the currently loaded shapefile
	void UnloadShapeFile();
  // returns the shape type of the shapes contained in the currently loaded shapefile
	string SelectedShapeType();
	// returns the shape database associated to the shape organizer
	Shapes::ShapeDatabase& GetShapeDatabase();
	// returns the shape list associated to the shape organizer
	Shapes::ShapeList& GetShapeList();
	// returns the number of multipart shapes contained in the the shape list associated 
	// with the shape organizer
	int GetMultipartShapesCount();
	// checks if the ok button can be enabled
	void CheckOkButtonEnabling();

public:
	// sets the working module and the pointer to the original one
	//  !! must be called before the DoModal function of this dialog !!
	void SetModule( LBModule* module );

	// returns a pointer to the working module
	LBModule* GetModule();

private:
	afx_msg void OnLbnSelChangeModuleList();

private:
	afx_msg void OnBnClickedClearAllDataBtn();
	afx_msg void OnBnClickedPreviewBtn();
	afx_msg void OnBnClickedOkBtn();
	afx_msg void OnBnClickedSaveBtn();
	afx_msg void OnBnClickedLoadBtn();
};

//-----------------------------------------------------------------------------
