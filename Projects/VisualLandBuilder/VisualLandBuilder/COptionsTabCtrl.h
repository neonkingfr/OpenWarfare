#pragma once

#include ".\CTabDirectoriesDlg.h"

// ****************** //
// Enum for Tab items //
// ****************** //
typedef enum
{
	OPT_DLG_DIRECTORIES,
	OPT_DLG_NONE
} LBOptTabItems;

class COptionsTabCtrl : public CTabCtrl
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
	CDialog* m_TabPages[1];
	int m_CurrentTab;
	int m_NumberOfPages;

public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	COptionsTabCtrl();

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~COptionsTabCtrl();

	// ************************************************************** //
	// helper functions                                               //
	// ************************************************************** //
public:
	void Init();
private:
	void SetRectangle();
public:
	CDialog* GetDialog(UINT index) const;

	// -------------------------------------------------------------------------- //
	// INTERFACE FUNCTIONS                                                        //
	// -------------------------------------------------------------------------- //
public:
	void SetCurrentTab(LBOptTabItems newCurTab);

	// ************************************************************** //
	// Message handlers                                               //
	// ************************************************************** //
private:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
