// HoverBitmapButton.cpp : implementation file
//

#include "stdafx.h"
#include "HoverBitmapButton.h"

/************************************************************
**
**	Button Hover class that's a subclass of CBitmapButton
**
**	by Rail Jon Rogut Feb 2003
**
*************************************************************/

// CHoverBitmapButton

IMPLEMENT_DYNAMIC(CHoverBitmapButton, CBitmapButton)

CHoverBitmapButton::CHoverBitmapButton()
{
	m_bHovering = FALSE;

	// m_pTheme = &(GetFrame()->m_theme);	//	If Doc/View m_theme is 
											//	a member of CMainFrame

	// m_bXPTheme = m_pTheme->m_bXPTheme;

	m_szXPPrefix = _T("XP");

	m_bThemeChanging = FALSE;
}

CHoverBitmapButton::~CHoverBitmapButton()
{
}


BEGIN_MESSAGE_MAP(CHoverBitmapButton, CBitmapButton)
	ON_WM_MOUSEMOVE()
	ON_WM_DRAWITEM()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_MESSAGE(WM_THEMECHANGED, OnThemeChanged)	// XP only
END_MESSAGE_MAP()


// CHoverBitmapButton message handlers

BOOL	CHoverBitmapButton::Load(UINT nID, CWnd* pParent, CTheme* pTheme)
{
	m_pTheme = pTheme;

	return AutoLoad(nID, pParent);
}


void CHoverBitmapButton::OnMouseMove(UINT nFlags, CPoint point)
{
	if(!m_bHovering)
		{
		m_bHovering = TRUE;

		Invalidate(FALSE);

		TRACKMOUSEEVENT tmEvent;

		tmEvent.cbSize = sizeof(tmEvent);
		tmEvent.dwFlags = TME_LEAVE;
		tmEvent.hwndTrack = m_hWnd;

		::_TrackMouseEvent(&tmEvent);
		}

	CBitmapButton::OnMouseMove(nFlags, point);
}

LRESULT CHoverBitmapButton::OnMouseLeave(WPARAM, LPARAM)
{
	m_bHovering = FALSE;

	Invalidate(FALSE);

	return 0;
}

//////////////////////////////////////////////////////////
//
//	XP Only
//
//	If you want to run on both an older OS and XP.. you'll 
//	have to remove this function
//
//	..but then the buttons won't redraw under XP
//	if the Style is changed while the app is active.

LRESULT CHoverBitmapButton::OnThemeChanged(WPARAM, LPARAM)
{
	// This feature requires Windows XP or greater.
	// The symbol _WIN32_WINNT must be >= 0x0501.
	// TODO: Add your message handler code here and/or call default

	// ReAutoLoad & Redraw

	m_bXPTheme = m_pTheme->m_bXPTheme;

	m_bThemeChanging = TRUE;

	AutoLoad(GetDlgCtrlID(), NULL);

	m_bThemeChanging = FALSE;

	Invalidate(FALSE);

	return 0;
}

////////////////////////////////////////////////////////////////////////

void CHoverBitmapButton::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (!m_bXPTheme)
		return;

	CDC* pDC   = CDC::FromHandle(lpDrawItemStruct->hDC);
    UINT state = lpDrawItemStruct->itemState;

	BOOL	bPressed = state & ODS_SELECTED;
	BOOL	bGotFocus = state & ODS_FOCUS;
	BOOL	bDisabled = state & ODS_DISABLED;

	// BOOL	bFocusRect = !(state & ODS_NOFOCUSRECT);	// Win2K/XP

	int		iMode;

    if (m_bHovering)
		{
		if (m_pTheme)
			{
			if (bPressed)
				{
				iMode = PBS_PRESSED;
				}
			else
				{
				iMode = PBS_HOT;
				}
			}
		}
	else
		{
		if (!bGotFocus && !bDisabled)
			{
			iMode = PBS_NORMAL;
			}

		if (bGotFocus /* && bFocusRect */)
			{
			iMode = PBS_DEFAULTED;
			}

		if (bDisabled)
			{
			iMode = PBS_DISABLED;
			}
		}
	
	if (m_pTheme)
		m_pTheme->DrawThemeBackground(*pDC, &(lpDrawItemStruct->rcItem),
														BP_PUSHBUTTON, iMode);

	DrawButton(lpDrawItemStruct);
}

void CHoverBitmapButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (m_bXPTheme)
		OnDrawItem(lpDrawItemStruct->CtlID, lpDrawItemStruct);
	else
		CBitmapButton::DrawItem(lpDrawItemStruct);
}

void CHoverBitmapButton::DrawButton(LPDRAWITEMSTRUCT lpDIS)
{
	// Copied from the base class

	ASSERT(lpDIS != NULL);
	// must have at least the first bitmap loaded before calling DrawItem
	ASSERT(m_bitmap.m_hObject != NULL);     // required

	// use the main bitmap for up, the selected bitmap for down
	CBitmap* pBitmap = &m_bitmap;
	UINT state = lpDIS->itemState;
	if ((state & ODS_SELECTED) && m_bitmapSel.m_hObject != NULL)
		pBitmap = &m_bitmapSel;
	else if ((state & ODS_FOCUS) && m_bitmapFocus.m_hObject != NULL)
		pBitmap = &m_bitmapFocus;   // third image for focused
	else if ((state & ODS_DISABLED) && m_bitmapDisabled.m_hObject != NULL)
		pBitmap = &m_bitmapDisabled;   // last image for disabled

	// draw the whole button
	CDC* pDC = CDC::FromHandle(lpDIS->hDC);
	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* pOld = memDC.SelectObject(pBitmap);
	if (pOld == NULL)
		return;     // destructors will clean up

	CRect rect;
	rect.CopyRect(&lpDIS->rcItem);

	////////////////////////////////////////////////
	// Changed the last parameter:

	pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(),
		&memDC, 0, 0, SRCAND);

	memDC.SelectObject(pOld);
}

BOOL CHoverBitmapButton::AutoLoad(UINT nID, CWnd* pParent)
{
	// Copied from base class:

	m_bXPTheme = m_pTheme->m_bXPTheme;	// Moved here for Dialog app

	if (!m_bThemeChanging)
		{
		// first attach the CBitmapButton to the dialog control
		if (!SubclassDlgItem(nID, pParent))
			return FALSE;
		}

	CString buttonName;
	GetWindowText(buttonName);
	ASSERT(!buttonName.IsEmpty());      // must provide a title

	///////////////////////////////////////////////
	// Added this:

	if (m_bXPTheme)
		{
		buttonName = m_szXPPrefix + buttonName;
		}

	///////////////////////////////////////////////

	LoadBitmaps(buttonName + _T("U"), buttonName + _T("D"),
	  buttonName + _T("F"), buttonName + _T("X"));

	// we need at least the primary
	if (m_bitmap.m_hObject == NULL)
		return FALSE;

	// size to content
	SizeToContent();
	return TRUE;
}

///////////////////////////////////////////////////////////
//  The default is that you create extra buttons beginning 
//	with "XP".. but this allows you to change the prefix:

void	CHoverBitmapButton::SetPrefix(CString szPrefix)
{
	m_szXPPrefix = szPrefix;
}

///////////////////////////////////////////////////////////
//
//	For Doc View:
//
//	m_theme is a class member of CMainFrame
//
//	call: m_theme.Init(m_hWnd);
//
//	on CMainFrame::OnCreate()
//		
/*

CMainFrame* CHoverBitmapButton::GetFrame(void)
{
	CMainFrame * pFrame = (CMainFrame *)AfxGetApp()->m_pMainWnd;
	ASSERT(pFrame->IsKindOf(RUNTIME_CLASS(CMainFrame)));
	return pFrame;
}

*/

