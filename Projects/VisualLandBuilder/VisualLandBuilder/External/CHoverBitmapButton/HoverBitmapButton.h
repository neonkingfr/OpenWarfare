#pragma once

// #include "MainFrm.h"		// For Doc/View

#include "theme.h"

/************************************************************
**
**	Button Hover class that's a subclass of CBitmapButton
**
**	by Rail Jon Rogut Feb 2003
**
*************************************************************/

// CHoverBitmapButton

class CHoverBitmapButton : public CBitmapButton
{
	DECLARE_DYNAMIC(CHoverBitmapButton)

public:
	CHoverBitmapButton();
	virtual ~CHoverBitmapButton();

	CTheme			*m_pTheme;

protected:
	DECLARE_MESSAGE_MAP()
private:
	BOOL	m_bHovering;
	BOOL	m_bXPTheme;
	CString	m_szXPPrefix;

	BOOL	m_bThemeChanging;

	LRESULT OnMouseLeave(WPARAM, LPARAM);
	LRESULT OnThemeChanged(WPARAM, LPARAM);	// XP only

	//	CMainFrame*		GetFrame(void);		// For Doc/View

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

	BOOL	Load(UINT nID, CWnd* pParent, CTheme* pTheme);

	void	DrawButton(LPDRAWITEMSTRUCT lpDIS);
	BOOL	AutoLoad(UINT nID, CWnd* pParent);
	void	SetPrefix(CString szPrefix);
};


