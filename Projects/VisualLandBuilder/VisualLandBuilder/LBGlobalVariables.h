//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

const int VERSION_NUMBER = 81;

//-----------------------------------------------------------------------------

const string MAPAREA_LEFT   = "MapArea.left";
const string MAPAREA_TOP    = "MapArea.top";
const string MAPAREA_RIGHT  = "MapArea.right";
const string MAPAREA_BOTTOM = "MapArea.bottom";

//-----------------------------------------------------------------------------

const string MAPFRAME_LEFT   = "Mapframe left";
const string MAPFRAME_BOTTOM = "Mapframe bottom";
const string MAPFRAME_WIDTH  = "Mapframe width";
const string MAPFRAME_HEIGHT = "Mapframe height";

//-----------------------------------------------------------------------------

const string HIGHMAPGRID_X  = "HighMapGrid.x";
const string HIGHMAPGRID_Y  = "HighMapGrid.y";

//-----------------------------------------------------------------------------

const string VISITOR_VERSION = "Visitor";

//-----------------------------------------------------------------------------

const string RESNAME_LANDBUILDER2  = "LandBuilder2";
const string RESNAME_WORKINGFOLDER = "Working folder";

//-----------------------------------------------------------------------------

const string PATH_LANDBUILDER2      = "c:\\bis\\landbuilder2\\landbuilder2.exe";
const string PATH_LANDBUILDER2_VBS  = "p:\\tools\\landbuilder\\landbuilder2.exe";
const string PATH_WORKINGFOLDER     = "c:\\lbworkingfolder\\";
const string PATH_WORKINGFOLDER_VBS = "p:\\export\\vlb";

//-----------------------------------------------------------------------------

const string FILTER_LANDBUILDER2  = "LandBuilder2 (*.exe)|*.exe||";
const string FILTER_WORKINGFOLDER = "";

//-----------------------------------------------------------------------------

const string FILE_LANDBUILDER2  = "landbuilder2";
const string FILE_WORKINGFOLDER = "";

//-----------------------------------------------------------------------------

const string EXT_LANDBUILDER2  = "exe";
const string EXT_WORKINGFOLDER = "";
const string EXT_MODULEFILE    = "mdl";

//-----------------------------------------------------------------------------

const string PARNAME_AGGREGATIONCOEFF       = "aggregationCoeff";
const string PARNAME_AMPLITUDEX             = "amplitudex";
const string PARNAME_AMPLITUDEY             = "amplitudey";
const string PARNAME_APPLYSMOOTH            = "applySmooth";
const string PARNAME_BLOCKMINSIZE           = "blockMinSize";
const string PARNAME_BLOCKMAXSUBD           = "blockMaxSubdivisions";
const string PARNAME_BLOCKMINSUBD           = "blockMinSubdivisions";
const string PARNAME_BLOCKMAXSIZE           = "blockMaxSize";
const string PARNAME_BUILDINGTYPE           = "buildingType";
const string PARNAME_BUILDINGMINHEIGHT      = "buildingMinHeight";
const string PARNAME_BUILDINGMAXHEIGHT      = "buildingMaxHeight";
const string PARNAME_CROSSTYPE              = "crossType";
const string PARNAME_DENSITY                = "density";
const string PARNAME_DESIREDHEIGHT          = "desiredHeight";                
const string PARNAME_DIRCORRECTION          = "dirCorrection";
const string PARNAME_DSTBOTTOM              = "dstBottom";
const string PARNAME_DSTLEFT                = "dstLeft";
const string PARNAME_DSTRIGHT               = "dstRight";
const string PARNAME_DSTTOP                 = "dstTop";
const string PARNAME_DXFEXPORT              = "dxfExport";
const string PARNAME_ENTERABLE              = "enterable";
const string PARNAME_FIRSTSTEP              = "firstStep";
const string PARNAME_FLOORHEIGHT            = "floorHeight";
const string PARNAME_FREQUENCY              = "frequency";
const string PARNAME_HECTAREDENSITY         = "hectareDensity";
const string PARNAME_HEIGHT                 = "height";
const string PARNAME_HEIGHTFLOORSCIVIL      = "heightFloorsCivil";
const string PARNAME_HEIGHTFLOORSINDUSTRIAL = "heightFloorsIndustrial";
const string PARNAME_HEIGHTFLOORSMILITARY   = "heightFloorsMilitary";
const string PARNAME_INNER                  = "inner";
const string PARNAME_LAST                   = "last";
const string PARNAME_LENGTH                 = "length";
const string PARNAME_LEVEL                  = "level";
const string PARNAME_LIBRARYFILE            = "libraryFile";
const string PARNAME_LIBRARIESFILE          = "librariesFile";
const string PARNAME_LINEARDENSITY          = "linearDensity";
const string PARNAME_LONGSMOOTHPAR          = "longSmoothPar";
const string PARNAME_LONGSMOOTHTYPE         = "longSmoothType";
const string PARNAME_MAINDIRECTION          = "mainDirection";
const string PARNAME_MASKSIZE               = "maskSize";
const string PARNAME_MASKCOL                = "maskCol";
const string PARNAME_MATCHORIENT            = "matchOrient";
const string PARNAME_MATCHTOL               = "matchTol";
const string PARNAME_MAXDISTANCE            = "maxDistance";
const string PARNAME_MAXHEIGHT              = "maxheight";
const string PARNAME_MAXNOISE               = "maxNoise";
const string PARNAME_MAXNUMFLOORS           = "maxNumFloors";
const string PARNAME_MAXNUMFLOORSCIVIL      = "maxNumFloorsCivil";
const string PARNAME_MAXNUMFLOORSINDUSTRIAL = "maxNumFloorsIndustrial";
const string PARNAME_MAXNUMFLOORSMILITARY   = "maxNumFloorsMilitary";
const string PARNAME_MAXSLOPE               = "maxslope";
const string PARNAME_MINDISTANCE            = "mindist";
const string PARNAME_MINHEIGHT              = "minheight";
const string PARNAME_MINSLOPE               = "minslope";
const string PARNAME_MODELCIVIL             = "modelCivil";
const string PARNAME_MODELINDUSTRIAL        = "modelIndustrial";
const string PARNAME_MODELMILITARY          = "modelMilitary";
const string PARNAME_MODELNAMEBASE          = "modelNameBase";
const string PARNAME_MODELSPATH             = "modelsPath";
const string PARNAME_MODELVARIANTS          = "modelVariants";
const string PARNAME_NAMEPREFIX             = "namePrefix";
const string PARNAME_NOWATERELEV            = "noWaterElev";
const string PARNAME_NUMBOUNDARYOBJECTS     = "numBoundaryObjects";
const string PARNAME_NUMFLOORS              = "numFloors";
const string PARNAME_NUMOFFLOOR             = "numOfFloor";
const string PARNAME_OBJECT                 = "object";
const string PARNAME_OFFSET                 = "offset";
const string PARNAME_OFFSETMAXNOISE         = "offsetMaxNoise";
const string PARNAME_ORIENTATION            = "orientation";
const string PARNAME_PARENTCOUNT            = "parentCount";
const string PARNAME_PARTCOUNT              = "partCount";
const string PARNAME_PARTCOUNT2             = "partCount2";
const string PARNAME_PARTNAMEBASE           = "partNameBase";
const string PARNAME_PERIODX                = "periodx";
const string PARNAME_PERIODY                = "periody";
const string PARNAME_PLACELEFT              = "placeLeft";
const string PARNAME_PLACERIGHT             = "placeRight";
const string PARNAME_PROB                   = "prob";
const string PARNAME_RANDOMSEED             = "randomSeed";
const string PARNAME_RANGE                  = "range";
const string PARNAME_RECTIFY                = "rectify";
const string PARNAME_RECTIFYCORNERS         = "rectifyCorners";
const string PARNAME_ROADMINWIDTH           = "roadMinWidth";
const string PARNAME_ROADMAXWIDTH           = "roadMaxWidth";
const string PARNAME_ROADTYPE				        = "roadType";
const string PARNAME_ROOFTYPE               = "roofType";
const string PARNAME_ROOFRED                = "roofRed";
const string PARNAME_ROOFGREEN              = "roofGreen";
const string PARNAME_ROOFBLUE               = "roofBlue";
const string PARNAME_SCALEX                 = "scaleX";
const string PARNAME_SCALEY                 = "scaleY";
const string PARNAME_SCALEZ                 = "scaleZ";
const string PARNAME_SIMILARITYFACTOR       = "similarityFactor";
const string PARNAME_SMALLATBOUNDARY        = "smallAtBoundary";
const string PARNAME_SRCBOTTOM              = "srcBottom";
const string PARNAME_SRCLEFT                = "srcLeft";
const string PARNAME_SRCRIGHT               = "srcRight";
const string PARNAME_SRCTOP                 = "srcTop";
const string PARNAME_STEP                   = "step";
const string PARNAME_STEPMAXNOISE           = "stepMaxNoise";
const string PARNAME_STRAIGHTPARTS          = "straightParts";
const string PARNAME_STRAIGHTTOLERANCE      = "straightTolerance";
const string PARNAME_SUBSQUARESSIZE         = "subSquaresSize";
const string PARNAME_TERRAINDEPTH           = "terrainDepth";
const string PARNAME_TEXFILE                = "texFile";
const string PARNAME_TRANSFTYPE             = "transfType";
const string PARNAME_TYPE                   = "type";
const string PARNAME_USED                   = "used";
const string PARNAME_USESPLINES             = "useSplines";
const string PARNAME_VERTEXTOL              = "vertexTol";
const string PARNAME_VISITORVER             = "visitorVersion";
const string PARNAME_WATERDEPTH             = "waterDepth";
const string PARNAME_WIDTH                  = "width";
const string PARNAME_X                      = "x";
const string PARNAME_XSIZE                  = "XSize";
const string PARNAME_XPADDING               = "XPadding";
const string PARNAME_XPALETTEFILE           = "XPaletteFile";
const string PARNAME_XFORCEDTEXTURE         = "XForcedTexture";
const string PARNAME_Y                      = "y";

//-----------------------------------------------------------------------------

const string PARDESC_AGGREGATIONCOEFF       = "This parameter controls how strong is the clustering of similar objects. Bigger values go towards a more random distribution. For example, assigning the value 1.0 you obtain a more clustered distribution than assigning the value 10.0.\nIf not specified defaults to 1.0";
const string PARDESC_AMPLITUDEX             = "The amplitude of the wave in X direction.";
const string PARDESC_AMPLITUDEY             = "The amplitude of the wave in Y direction.";
const string PARDESC_APPLYSMOOTH            = "Whether or not to smooth the water surface.\n0 (false) or 1 (true).\nDefaults to 1.";
const string PARDESC_BLOCKMINSIZE           = "The minimum size of blocks (in meters).";
const string PARDESC_BLOCKMINSUBD           = "The minimum number of subdivision for the block division.";
const string PARDESC_BLOCKMAXSIZE           = "The maximum size of blocks (in meters).";
const string PARDESC_BLOCKMAXSUBD           = "The maximum number of subdivision for the block division.";
const string PARDESC_BOTTOM                 = "The UTM coordinate of the bottom edge of the destination Visitor 4's mapframe.";
const string PARDESC_BUILDINGTYPE           = "The type of the building.\nValid values to use in the dbf table are: \'CIVIL\', \'INDUSTRIAL\' and \'MILITARY\'";
const string PARDESC_BUILDINGMINHEIGHT      = "The minimum height of buildings (in meters).";
const string PARDESC_BUILDINGMAXHEIGHT      = "The maximum height of buildings (in meters).";
const string PARDESC_CROSSTYPE              = "The type of crossroads to be generated.\nIt can be 0 (right angles) or 1 (generic angles) or 2 (no crossroads).";
const string PARDESC_DESIREDHEIGHT          = "The height of the model who has to substitute this shape.";
const string PARDESC_DIRCORRECTION          = "The correction (in degrees) to apply to the orientation angle.\nDefaults to 0.0";
const string PARDESC_DSTBOTTOM              = "The bottom edge of the destination data rectangle on the map (in meters).";
const string PARDESC_DSTLEFT                = "The left edge of the destination data rectangle on the map (in meters).";
const string PARDESC_DSTRIGHT               = "The right edge of the destination data rectangle on the map (in meters).";
const string PARDESC_DSTTOP                 = "The top edge of the destination data rectangle on the map (in meters).";
const string PARDESC_DXFEXPORT              = "Whether or not to export a dxf file containing the generated road network.\nThis parameter can have only 2 values: 0 (false) or 1 (true).";
const string PARDESC_ENTERABLE              = "Whether or not the building is enterable.\nThis parameter can have only 2 values: 0 (false) or 1 (true).";
const string PARDESC_FIRSTSTEP              = "The longitudinal offset of the first object (in meters).";
const string PARDESC_FLOORHEIGHT            = "The average height of the floors.";
const string PARDESC_GRIDX                  = "The resolution of the Visitor map grid in x direction (in meters).";
const string PARDESC_GRIDY                  = "The resolution of the Visitor map grid in y direction (in meters).";
const string PARDESC_HECTAREDENSITY         = "The density of the filling in number of objects for hectare.\nFor example 100 objects for hectare means an average distance of about 10 meters between objects.\nIf not specified defaults to 100";
const string PARDESC_HEIGHT                 = "The height, in meters, of the bounding box of this object.";
const string PARDESC_HEIGHTFLOORSCIVIL      = "The average heigth (in meters) of one floor for this type of building.\nUsed only if the dbf parameter \'numOfFloor\' is equal to zero.\nIf not specified defaults to 3.0 meters.";
const string PARDESC_HEIGHTFLOORSINDUSTRIAL = "The average heigth (in meters) of one floor for this type of building.\nUsed only if the dbf parameter \'numOfFloor\' is equal to zero.\nIf not specified defaults to 3.0 meters.";
const string PARDESC_HEIGHTFLOORSMILITARY   = "The average heigth (in meters) of one floor for this type of building.\nUsed only if the dbf parameter \'numOfFloor\' is equal to zero.\nIf not specified defaults to 3.0 meters.";
const string PARDESC_INNER                  = "Whether or not this object must be placed in the inner squares.\nThis parameter can have only 2 values: 0 (false) or 1 (true).\nIf not specified default to 1.";
const string PARDESC_LAST                   = "This parameter can have only 2 values: 0 (false) or 1 (true).\nMust be set to 1 only for the last point in the list.";
const string PARDESC_LENGTH                 = "The length, in meters, of the bounding box of this object.";
const string PARDESC_LEFT                   = "The UTM coordinate of the left edge of the destination Visitor 4's mapframe.";
const string PARDESC_LEVEL                  = "The level of the road. Only roads with the same level will generate a crossroad.";
const string PARDESC_LIBRARYFILE            = "The config file containing the definition of the library used by this module.";
const string PARDESC_LIBRARIESFILE          = "The config file containing the definition of the libraries used by this module.";
const string PARDESC_LINEARDENSITY          = "The density of the filling in number of objects every 100 meters.\nIf not specified defaults to 10.";
const string PARDESC_LONGSMOOTHPAR          = "The fixed absolute elevation to assign to all vertices of the polyline.";
const string PARDESC_LONGSMOOTHPAR2         = "The fixed relative elevation to assign to all vertices of the polyline.";
const string PARDESC_LONGSMOOTHPAR3         = "The max allowed slope, in percent, along the profile of the polyline.";
const string PARDESC_LONGSMOOTHPAR4         = "The extension of the moving average, in meters, along the profile of the polyline.";
const string PARDESC_MAINDIRECTION          = "The main direction of roads. This is the angle (in degrees) formed by one of the two possible road directions with the horizontal direction (line W-E).";
const string PARDESC_MAINDIRECTION2         = "The angle (in degrees) formed by the horizontal direction of the grid and the X axis.";
const string PARDESC_MASKSIZE               = "The size of the mask bitmap to be generated, in pixels\nIf 0 (zero - default value) no mask file will be generated.";
const string PARDESC_MASKCOL                = "The color to apply on the mask bitmap for the road, in the form RRRGGGBBB (defaults to black if not specified).\nExamples:\nRed = 123, Green = 15, Blue = 209 -> 123015209\nRed = 0, Green = 121, Blue = 10 -> 121010";
const string PARDESC_MATCHORIENT            = "Whether or not the orientation of this object must match the one of the shape.\nThis parameter can have only 2 values: 0 (false) or 1 (true).\nIf not specified default to 1.";
const string PARDESC_MATCHTOL               = "The maximum allowed difference in the dimensions of two objects to use the same generated model, in percent.";
const string PARDESC_MAXDISTANCE            = "The maximum distance from the shape in meters\nIf not specified defaults to 10 meters.";
const string PARDESC_MAXHEIGHT2             = "The maximum size of this object (100 corresponds to the size of the original model). \nThis value is used by the module at the boundary if the global parameter \'smallAtBoundary\' is set to 1.\nIf not specified defaults to 100.";
const string PARDESC_MAXHEIGHT3             = "The maximum size of this object (100 corresponds to the size of the original model).\nIf not specified defaults to 100.";
const string PARDESC_MAXNOISE               = "The maximum noise to apply in the determination of the random shifting with respect with the center of the sub square.\n This value should be not greater than half of the square size.";
const string PARDESC_MAXNUMFLOORS           = "The max number of floors for the buildings.\nUsed only if the dbf parameter \'numOfFloor\' is equal to zero.\nIf not specified defaults to 3";
const string PARDESC_MAXNUMFLOORSCIVIL      = "The max number of floors for this type of buildings.\nUsed only if the dbf parameter \'numOfFloor\' is equal to zero.\nIf not specified defaults to 3";
const string PARDESC_MAXNUMFLOORSINDUSTRIAL = "The max number of floors for this type of buildings.\nUsed only if the dbf parameter \'numOfFloor\' is equal to zero.\nIf not specified defaults to 2";
const string PARDESC_MAXNUMFLOORSMILITARY   = "The max number of floors for this type of buildings.\nUsed only if the dbf parameter \'numOfFloor\' is equal to zero.\nIf not specified defaults to 1";
const string PARDESC_MAXSLOPE               = "The maximum value, in percent, of the slope of the terrain to allow the placement of this object type.";
const string PARDESC_MINDISTANCE            = "The minimum distance (in meters) for placing objects in the neighborhood of this type object.\nIf not specified defaults to 5.0 meters.";
const string PARDESC_MINDISTANCE2           = "The minimum distance (in meters) of the center of the square from the polygon to consider the square valid for object placement.\nIf not specified defaults to 0.0 meters.";
const string PARDESC_MINDISTANCE3           = "The minimum distance (in meters) from the polygon that squares must have to be considered inner.\nIf not specified defaults to 0.0 meters.";
const string PARDESC_MINHEIGHT2             = "The minimum size of this object (100 corresponds to the size of the original model). \nThis value is used by the module at the boundary if the global parameter \'smallAtBoundary\' is set to 1.\nIf not specified defaults to 100.";
const string PARDESC_MINHEIGHT3             = "The minimum size of this object (100 corresponds to the size of the original model).\nIf not specified defaults to 100.";
const string PARDESC_MINSLOPE               = "The minimum value, in percent, of the slope of the terrain to allow the placement of this object type.";
const string PARDESC_MODELBUILDING          = "The template name used in Visitor for the p3d model to be used as base for this type of building.";
const string PARDESC_MODELNAMEBASE          = "The prefix of the template names used in Visitor for the p3d models to be used as base for the buildings.";
const string PARDESC_MODELSPATH             = "The path where to save the generated models.";
const string PARDESC_MODELVARIANTS          = "The number of variants of the p3d model to use.";
const string PARDESC_NAMEPREFIX             = "The prefix to add to the filename of the generated objects.";
const string PARDESC_NAMEPREFIX2            = "The prefix to add to the filename of the exported terrains.";
const string PARDESC_NOWATERELEV            = "The elevation to assign to the water heightfield outside the water polygons.";
const string PARDESC_NUMBOUNDARYOBJECTS     = "The number of objects to try to place inside the boundary squares.";
const string PARDESC_NUMFLOORS              = "The number of floors of the building. If -1 the building will have a random number of floors.";
const string PARDESC_NUMOFFLOOR             = "The number of floors of the building. If 0 (zero) the building will have a random number of floors in the range [1..maxNumFloors].";
const string PARDESC_OBJECT                 = "The template name used in Visitor for the model to be placed.";
const string PARDESC_OFFSET                 = "The mean offset, in meters, from the shape.";
const string PARDESC_OFFSETMAXNOISE         = "The max value of the random noise, in meters, to apply to the offset.";
const string PARDESC_ORIENTATION            = "The orientation to be assigned to the object (in degrees).";
const string PARDESC_PARENTCOUNT            = "The number of objects to be used as origins for the placement of all other objects of the same type.";
const string PARDESC_PARENTCOUNT2           = "The number of objects, per hectare, to be used as origins for the placement of all other objects of the same type.";
const string PARDESC_PARTCOUNT              = "...";
const string PARDESC_PARTCOUNT2             = "Must be equal to partCount.";
const string PARDESC_PARTNAMEBASE           = "...";
const string PARDESC_PERIODX                = "The period of the wave in X direction";
const string PARDESC_PERIODY                = "The period of the wave in Y direction";
const string PARDESC_PLACELEFT              = "Whether or not place this object on the left of the shape.\nThis parameter can have only 2 values: 0 (false) or 1 (true).\nIf not specified defaults to 1.";
const string PARDESC_PLACERIGHT             = "Whether or not place this object on the right of the shape.\nThis parameter can have only 2 values: 0 (false) or 1 (true).\nIf not specified defaults to 1.";
const string PARDESC_PROB                   = "The probability to find this object inside the shape (range [0..100]). \nThe sum of all objects probs doesn't need to be equal to 100: the module renormalizes all the values).\nIf not specified defaults to 100.";
const string PARDESC_RANDOMSEED             = "The seed to be used by the random generator. Must be in the range [1 .. 65535]";
const string PARDESC_RECTIFY                = "Whether or not set all the corner of this building to 90 degrees.\nThis parameter can have only 2 values: 0 (false) or 1 (true).";
const string PARDESC_RECTIFYCORNERS         = "Whether or not to rectify the corners of the building.\nThis parameter can have only 2 values: 0 (false) or 1 (true).";
const string PARDESC_RIGHT                  = "The right edge of the Visitor map.";
const string PARDESC_ROADMINWIDTH           = "The minimum width of roads (in meters).";
const string PARDESC_ROADMAXWIDTH           = "The maximum width of roads (in meters).";
const string PARDESC_ROADTYPE               = "...";
const string PARDESC_ROOFTYPE               = "An integer specifying the type of roof to use with this building.\n0 means no roof (planar surface).";
const string PARDESC_ROOFRED                = "The red component of the RGB procedural texture to apply to the roof.\nMust be in the range [0 .. 255]";
const string PARDESC_ROOFGREEN              = "The green component of the RGB procedural texture to apply to the roof.\nMust be in the range [0 .. 255]";
const string PARDESC_ROOFBLUE               = "The blue component of the RGB procedural texture to apply to the roof.\nMust be in the range [0 .. 255]";
const string PARDESC_SCALEX                 = "The scaling factor on X coordinate to apply to the object.\nIf not specified defaults to 1.0";
const string PARDESC_SCALEY                 = "The scaling factor on Y coordinate to apply to the object.\nIf not specified defaults to 1.0";
const string PARDESC_SCALEZ                 = "The scaling factor on Z coordinate to apply to the object.\nIf not specified defaults to 1.0";
const string PARDESC_SIMILARITYFACTOR       = "The similarity factor applied when comparing building footprints.\nIf equal to 1.0 it means exact match.\nHigher values allow for lesser strict matches.";
const string PARDESC_SIZEX                  = "The width of the destination Visitor 4's mapframe.";
const string PARDESC_SIZEY                  = "The height of the destination Visitor 4's mapframe.";
const string PARDESC_SMALLATBOUNDARY        = "Whether or not to place only small (minheight) objects at the shape\'s boundary.\nThis parameter can have only 2 values: 0 (false) or 1 (true).\nIf is set to 1, the module will fill the neighborhood of the shape's perimeter only with objects having their \'minheight\' value as size.\nIf not specified defaults to 1";
const string PARDESC_SRCBOTTOM              = "The bottom edge of the source data rectangle to import (in pixels or nodes)";
const string PARDESC_SRCLEFT                = "The left edge of the source data rectangle to import (in pixels or nodes)";
const string PARDESC_SRCRIGHT               = "The right edge of the source data rectangle to import (in pixels or nodes)";
const string PARDESC_SRCTOP                 = "The top edge of the source data rectangle to import (in pixels or nodes)";
const string PARDESC_STEP                   = "...";
const string PARDESC_STEP2                  = "The mean step (distance between two consecutive objects parallel to the shape), in meters.";
const string PARDESC_STEPMAXNOISE           = "The max value of the random noise, in meters, to apply to the step.";
const string PARDESC_SUBSQUARESSIZE         = "The size of the sub squares (or grid cells) used to divide the polygon and where put every single object.";
const string PARDESC_TERRAINDEPTH           = "The depth of the river's bed (in meters).";
const string PARDESC_TEXFILE                = "The file name of the bitmap to use for the texturing of the road, without extension.";
const string PARDESC_TOP                    = "The top edge of the Visitor map.";
const string PARDESC_TRANSFTYPE             = "The type of transformation to apply to all the shapes.\nPossible values: RIGID, CONFORM, AFFINE, AFFINEEX, HOMOGRAPHIC, BILINEAR, POLYNOMIAL2, BESTFIT.";
const string PARDESC_TYPE                   = "The type of building.\nMust match a building type name contained into the library config file.";
const string PARDESC_USED                   = "This parameter can have only 2 values: 0 (false) or 1 (true).\nIf is set to 1, the point will be used in the calculations.";
const string PARDESC_USESPLINES             = "Whether or not to use splines in place of the original polylines.\nThis parameter can have only 2 values: 0 (false) or 1 (true).\nIt defaults to 0 (false).";
const string PARDESC_VERTEXTOL              = "The maximum value of the changing in direction of polygon edges to be rectified, in degrees.";
const string PARDESC_VISITORVER             = "The used version of Visitor. This parameter can have only 2 values: 3 or 4.";
const string PARDESC_WATERDEPTH             = "The distance between the river bed and the water surface (in meters).";
const string PARDESC_WIDTH                  = "The width of the roads (in meters).";
const string PARDESC_WIDTH2                 = "The width, in meters, of the strip having the polyline as centerline and under which the terrain will be smoothed.";
const string PARDESC_WIDTH3                 = "The width, in meters, of the bounding box of this object.";
const string PARDESC_X                      = "The X coordinate of the points.";
const string PARDESC_XSIZE                  = "Maximum distance from crossroad point, where roads get connected to this crossroad";
const string PARDESC_XPADDING               = "Radius around crossroad, which is used to modify roads to connect properly.\nAlso used to merge crossroad points within this distance into one crossroad.";
const string PARDESC_XPALETTEFILE           = "The file name including the absolute path of the palette to use.";
const string PARDESC_XFORCEDTEXTURE         = "The absolute filename including the path pointing to the texture used for crossing of that road type.";
const string PARDESC_Y                      = "The Y coordinate of the points.";

//-----------------------------------------------------------------------------

const string MODULE_LBNAME_ADVANCEDONPLACEPLACER         = "AdvancedOnPlacePlacer";
const string MODULE_LBNAME_ADVANCEDRANDOMPLACER          = "AdvancedRandomPlacer";
const string MODULE_LBNAME_ADVANCEDRANDOMPLACERSLOPE     = "AdvancedRandomPlacerSlope";
const string MODULE_LBNAME_BESTFITPLACER                 = "BestFitPlacer";
const string MODULE_LBNAME_BIASEDRANDOMPLACER            = "BiasedRandomPlacer";
const string MODULE_LBNAME_BIASEDRANDOMPLACERSLOPE       = "BiasedRandomPlacerSlope";
const string MODULE_LBNAME_BOUNDARYSUBSQUARESFRAMEPLACER = "BoundarySubSquaresFramePlacer";
const string MODULE_LBNAME_BUILDINGCREATOR               = "BuildingCreator";
const string MODULE_LBNAME_CROSSROADPLACER               = "RoadBuilder";
const string MODULE_LBNAME_CROSSROADPLACEREXT            = "RoadBasicPlacer";
const string MODULE_LBNAME_CSSMOOTHTERRAINALONGPATH      = "SmoothTerrainAlongPath";
const string MODULE_LBNAME_DELETEALONGPATH               = "DeleteAlongPath";
const string MODULE_LBNAME_DELETEINPOLYGON               = "DeleteInPolygon";
const string MODULE_LBNAME_DUMMYBUILDINGPLACER           = "DummyBuildingPlacer";
const string MODULE_LBNAME_EDIFICI                       = "Edifici";
const string MODULE_LBNAME_FASMOOTHTERRAINALONGPATH      = "SmoothTerrainAlongPath";
const string MODULE_LBNAME_FRAMEDFORESTPLACER            = "FramedForestPlacer";
const string MODULE_LBNAME_FRSMOOTHTERRAINALONGPATH      = "SmoothTerrainAlongPath";
const string MODULE_LBNAME_GEOPROJECTION                 = "GeoProjection";
const string MODULE_LBNAME_HIGHMAPLOADER                 = "HighMapLoader";
const string MODULE_LBNAME_HIGHMAPTEST                   = "HighMapTest";
const string MODULE_LBNAME_INNERSUBSQUARESFRAMEPLACER    = "InnerSubSquaresFramePlacer";
const string MODULE_LBNAME_MSMOOTHTERRAINALONGPATH       = "SmoothTerrainAlongPath";
const string MODULE_LBNAME_MASMOOTHTERRAINALONGPATH      = "SmoothTerrainAlongPath";
const string MODULE_LBNAME_MSSMOOTHTERRAINALONGPATH      = "SmoothTerrainAlongPath";
const string MODULE_LBNAME_NONE                          = "None";
const string MODULE_LBNAME_ONPLACEPLACER                 = "OnPlacePlacer";
const string MODULE_LBNAME_NYDUMMYBUILDINGPLACER         = "NYDummyBuildingPlacer";
const string MODULE_LBNAME_RANDOMONPATHPLACER            = "RandomOnPathPlacer";
const string MODULE_LBNAME_REGULARONPATHPLACER           = "RegularOnPathPlacer";
const string MODULE_LBNAME_RIVERS                        = "Rivers";
const string MODULE_LBNAME_ROADSGENERATOR                = "RoadsGenerator";
const string MODULE_LBNAME_ROADPLACER                    = "RoadBuilder";
const string MODULE_LBNAME_ROADPLACEREXT                 = "RoadBasicPlacer";
const string MODULE_LBNAME_SCRIPTMODULE                  = "ScriptModule";
const string MODULE_LBNAME_SIMPLERANDOMPLACER            = "SimpleRandomPlacer";
const string MODULE_LBNAME_SIMPLERANDOMPLACERSLOPE       = "SimpleRandomPlacerSlope";
const string MODULE_LBNAME_SMOOTHTERRAINALONGPATH        = "SmoothTerrainAlongPath";
const string MODULE_LBNAME_STRADE                        = "Strade";
const string MODULE_LBNAME_SUBSQUARESRANDOMPLACER        = "SubSquaresRandomPlacer";
const string MODULE_LBNAME_TRANSFORM2D                   = "Transform2D";

//-----------------------------------------------------------------------------

const string MODULE_VLBNAME_ADVANCEDONPLACEPLACER         = MODULE_LBNAME_ADVANCEDONPLACEPLACER;
const string MODULE_VLBNAME_ADVANCEDRANDOMPLACER          = MODULE_LBNAME_ADVANCEDRANDOMPLACER;
const string MODULE_VLBNAME_ADVANCEDRANDOMPLACERSLOPE     = MODULE_LBNAME_ADVANCEDRANDOMPLACERSLOPE;
const string MODULE_VLBNAME_BESTFITPLACER                 = MODULE_LBNAME_BESTFITPLACER;
const string MODULE_VLBNAME_BIASEDRANDOMPLACER            = MODULE_LBNAME_BIASEDRANDOMPLACER;
const string MODULE_VLBNAME_BIASEDRANDOMPLACERSLOPE       = MODULE_LBNAME_BIASEDRANDOMPLACERSLOPE;
const string MODULE_VLBNAME_BOUNDARYSUBSQUARESFRAMEPLACER = MODULE_LBNAME_BOUNDARYSUBSQUARESFRAMEPLACER;
const string MODULE_VLBNAME_BUILDINGCREATOR               = "BuildingCreator (Beta)";
const string MODULE_VLBNAME_CROSSROADPLACER               = "CrossroadPlacer";
const string MODULE_VLBNAME_CSSMOOTHTERRAINALONGPATH      = "ConstantSlopeSmoothTerrainAlongPath";
const string MODULE_VLBNAME_DELETEALONGPATH               = MODULE_LBNAME_DELETEALONGPATH;
const string MODULE_VLBNAME_DELETEINPOLYGON               = MODULE_LBNAME_DELETEINPOLYGON;
const string MODULE_VLBNAME_DUMMYBUILDINGPLACER           = MODULE_LBNAME_DUMMYBUILDINGPLACER;
const string MODULE_VLBNAME_EDIFICI                       = "Edifici (Beta)";
const string MODULE_VLBNAME_FASMOOTHTERRAINALONGPATH      = "FixedAbsoluteSmoothTerrainAlongPath";
const string MODULE_VLBNAME_FRSMOOTHTERRAINALONGPATH      = "FixedRelativeSmoothTerrainAlongPath";
const string MODULE_VLBNAME_FRAMEDFORESTPLACER            = MODULE_LBNAME_FRAMEDFORESTPLACER;
const string MODULE_VLBNAME_GEOPROJECTION                 = MODULE_LBNAME_GEOPROJECTION;
const string MODULE_VLBNAME_HIGHMAPLOADER                 = "HeightMapLoader";
const string MODULE_VLBNAME_HIGHMAPTEST                   = "HeightMapTest";
const string MODULE_VLBNAME_INNERSUBSQUARESFRAMEPLACER    = MODULE_LBNAME_INNERSUBSQUARESFRAMEPLACER;
const string MODULE_VLBNAME_MSMOOTHTERRAINALONGPATH       = "MonotoneSmoothTerrainAlongPath";
const string MODULE_VLBNAME_MASMOOTHTERRAINALONGPATH      = "MovingAverageSmoothTerrainAlongPath";
const string MODULE_VLBNAME_MSSMOOTHTERRAINALONGPATH      = "MaxSlopeSmoothTerrainAlongPath";
const string MODULE_VLBNAME_NONE                          = "SimpleMapper";
const string MODULE_VLBNAME_ONPLACEPLACER                 = MODULE_LBNAME_ONPLACEPLACER;
const string MODULE_VLBNAME_NYDUMMYBUILDINGPLACER         = MODULE_LBNAME_NYDUMMYBUILDINGPLACER;
const string MODULE_VLBNAME_RANDOMONPATHPLACER            = MODULE_LBNAME_RANDOMONPATHPLACER;
const string MODULE_VLBNAME_REGULARONPATHPLACER           = MODULE_LBNAME_REGULARONPATHPLACER;
const string MODULE_VLBNAME_RIVERS                        = "Rivers (Beta)";
const string MODULE_VLBNAME_ROADSGENERATOR                = "RoadsGenerator (Beta)";
const string MODULE_VLBNAME_ROADPLACER                    = "RoadPlacer";
const string MODULE_VLBNAME_SCRIPTMODULE                  = MODULE_LBNAME_SCRIPTMODULE;
const string MODULE_VLBNAME_SIMPLERANDOMPLACER            = MODULE_LBNAME_SIMPLERANDOMPLACER;
const string MODULE_VLBNAME_SIMPLERANDOMPLACERSLOPE       = MODULE_LBNAME_SIMPLERANDOMPLACERSLOPE;
const string MODULE_VLBNAME_SMOOTHTERRAINALONGPATH        = MODULE_LBNAME_SMOOTHTERRAINALONGPATH;
const string MODULE_VLBNAME_STRADE                        = "Strade (Beta)";
const string MODULE_VLBNAME_SUBSQUARESRANDOMPLACER        = MODULE_LBNAME_SUBSQUARESRANDOMPLACER;
const string MODULE_VLBNAME_TRANSFORM2D                   = MODULE_LBNAME_TRANSFORM2D;

//-----------------------------------------------------------------------------

const string MODULE_DESC_ADVANCEDONPLACEPLACER         = "Places one object for every polygonal shape contained in the specified shapefile. The object's insertion point will be in the polygon barycenter, while the orientation is determined from the polygon's geometry.\nOrientation can be corrected using the dirCorrection parameter.";
const string MODULE_DESC_ADVANCEDRANDOMPLACER          = "Random placement of multiple objects inside the polygons specified in the given shapefile.";
const string MODULE_DESC_ADVANCEDRANDOMPLACERSLOPE     = "Random placement of multiple objects inside the polygons specified in the given shapefile.\nObjects are placed only where the terrain has a slope in the specified range.";
const string MODULE_DESC_BESTFITPLACER                 = "Substitute a polygonal shape with the object, from the given list, that best fits the shape."; 
const string MODULE_DESC_BIASEDRANDOMPLACER            = "Biased random placement of multiple objects inside the polygons specified in the given shapefile, the biasing consisting in clusters formations.";
const string MODULE_DESC_BIASEDRANDOMPLACERSLOPE       = "Biased random placement of multiple objects inside the polygons specified in the given shapefile, the biasing consisting in clusters formations.\nObjects are placed only where the terrain has a slope in the specified range.";
const string MODULE_DESC_BOUNDARYSUBSQUARESFRAMEPLACER = "Random placement of multiple objects along the boundary of the polygons, specified in the given shapefile, using a grid pattern.\n This module fills only the squares whose center is inside the polygon at distance greater than or equal to the specified value of minimum distance.";
const string MODULE_DESC_BUILDINGCREATOR               = "Placement of automatically generated buildings in place of the polygons contained in the given shapefile.";
const string MODULE_DESC_CROSSROADPLACER               = "Placement of crossroads in the positions specified by the points contained in the given shapefile.";
const string MODULE_DESC_CSSMOOTHTERRAINALONGPATH      = "First smooths the profile along the polyline specified in the given shapefile using a constant slope, then smooths the terrain in its neighborhood.";
const string MODULE_DESC_DELETEALONGPATH               = "Delete objects along the polylines contained in the given shapefile assuming the polyline having the specified width.";
const string MODULE_DESC_DELETEINPOLYGON               = "Delete objects inside the polygons contained in the given shapefile.";
const string MODULE_DESC_DUMMYBUILDINGPLACER           = "Placement of rectangular and L-shaped box-like buildings specified in the given shapefile.";
const string MODULE_DESC_EDIFICI                       = "Construction and placement of procedurally generated buildings in place of the polygons contained in the given shapefile.";
const string MODULE_DESC_FASMOOTHTERRAINALONGPATH      = "First sets the profile along the polyline specified in the given shapefile at the given absolute elevation, then smooths the terrain in its neighborhood.";
const string MODULE_DESC_FRAMEDFORESTPLACER            = "Differential filling of the polygons specified in the given shapefile.\nThe polygon is divided using a grid having the specified size.\nInner cells will be filled with one of the objects specified as inner, while border cells will be filled with the objects specified as non-inner.";
const string MODULE_DESC_FRSMOOTHTERRAINALONGPATH      = "First sets the profile along the polyline specified in the given shapefile at the given relative elevation, then smooths the terrain in its neighborhood.";
const string MODULE_DESC_GEOPROJECTION                 = "Transformation from geographical to projected coordinates to apply to all the shapes in the project.\nThis module must be specified as first, and must be followed by a Transform2D module.";
const string MODULE_DESC_HIGHMAPLOADER                 = "Import of terrain height fields from DEMs files.";
const string MODULE_DESC_HIGHMAPTEST                   = "Sinusoidal terrain generator."; 
const string MODULE_DESC_INNERSUBSQUARESFRAMEPLACER    = "Random placement of multiple objects inside the polygons specified in the given shapefile using a grid pattern.\n This module fills only the squares completely inside the polygon.";
const string MODULE_DESC_MSMOOTHTERRAINALONGPATH       = "First smooths the profile along the polyline specified in the given shapefile so that it will have a monotonical increase/decrease with the specified max slope, then smooths the terrain in its neighborhood.";
const string MODULE_DESC_MASMOOTHTERRAINALONGPATH      = "First smooths the profile along the polyline specified in the given shapefile using a moving average with the specified max extension (in meters), then smooths the terrain in its neighborhood.";
const string MODULE_DESC_MSSMOOTHTERRAINALONGPATH      = "First smooths the profile along the polyline specified in the given shapefile so that it will have the specified max slope, then smooths the terrain in its neighborhood.";
const string MODULE_DESC_NONE                          = "Initialization module. Needed for georeferencing all the shape in the project. Must be specified as first if not using GeoProjection or Transform2D modules.";
const string MODULE_DESC_ONPLACEPLACER                 = "Placement of objects in the positions specified in the given shapefile.";
const string MODULE_DESC_NYDUMMYBUILDINGPLACER         = "Placement of rectangular box-like buildings in the given shapefile using a New York City pattern (roads intersecting at right angle).";
const string MODULE_DESC_RANDOMONPATHPLACER            = "Random placement of multiple objects along the polylines or the polygons specified in the given shapefiles.";
const string MODULE_DESC_REGULARONPATHPLACER           = "Regular placement of an object type along the polylines or the polygons specified in the given shapefiles.";
const string MODULE_DESC_RIVERS                        = "Models the terrain under rivers and generates the water surface layer.";
const string MODULE_DESC_ROADSGENERATOR                = "Placement of roads and crossroads along the polylines specified in the given shapefile.";
const string MODULE_DESC_ROADPLACER                    = "Placement of roads along the polylines specified in the given shapefile.";
const string MODULE_DESC_SCRIPTMODULE                  = "Run the specified script file.";
const string MODULE_DESC_SIMPLERANDOMPLACER            = "Random placement of one object inside the polygons specified in the given shapefile.";
const string MODULE_DESC_SIMPLERANDOMPLACERSLOPE       = "Random placement of one object inside the polygons specified in the given shapefile.\nObjects are placed only where the terrain has a slope in the specified range.";
const string MODULE_DESC_SMOOTHTERRAINALONGPATH        = "Smooths the terrain in the neighborhood of the polyline specified in the given shapefile without previously smoothing its profile.";
const string MODULE_DESC_STRADE                        = "Construction and placement of roads and crossroads along the polylines specified in the given shapefile.";
const string MODULE_DESC_SUBSQUARESRANDOMPLACER        = "Random placement of multiple objects inside the polygons specified in the given shapefile using a grid pattern.";
const string MODULE_DESC_TRANSFORM2D                   = "Two dimensional transformation to apply to all the shapes in the project.\nIf not using a GeoProjection module, this module must be specified as first.";

//-----------------------------------------------------------------------------

const string MSG_APPLYINGMODULE        = "applying module:";
const string MSG_CONFIRMCLEARDBFPAR    = "Confirm that you want to clear all the dbf parameters ?";
const string MSG_CONFIRMCLEARNOTES     = "Confirm that you want to clear the notes ?";
const string MSG_CONFIRMCLEARSHAPENAME = "Confirm that you want to clear the shapefile name ?";
const string MSG_CONFIRMREMOVEALLDEMS  = "Confirm that you want to remove all the dem files ?";
const string MSG_CONFIRMREMOVEALLGLOB  = "Confirm that you want to remove all the global parameters ?";
const string MSG_CONFIRMREMOVEALLOBJS  = "Confirm that you want to remove all the objects ?";
const string MSG_CONFIRMREMOVEDEMFILE  = "Confirm that you want to remove the selected dem file ?";
const string MSG_CONFIRMREMOVEOBJECT   = "Confirm that you want to remove the selected object ?";
const string MSG_DATANOTRECOGNIZED     = "Data type not recognized.";
const string MSG_DIFFERENTFOLDER       = "The specified file is contained in a folder different from the one specified in the Resource folders.";
const string MSG_ERROR                 = "Error";
const string MSG_ERRORCOPYINGDBF       = "Error while copying dbf files.";
const string MSG_ERRORCOPYINGSHAPEFILE = "Error while copying shapefiles.";
const string MSG_ERRORCOPYINGDEMFILE   = "Error while copying dem files.";
const string MSG_ERRORINMODULES        = "Error in modules";
const string MSG_ERRORLOADINGMODULE    = "Error while loading module file."; 
const string MSG_ERRORMAINCONFIG       = "Error while saving the main config file.";
const string MSG_ERRORRUNLANDBUILDER   = "Error in launching LandBuilder.";
const string MSG_ERRORRUNLANDBUILDER2  = "Error in launching LandBuilder2.";
const string MSG_ERRORSAVINGMODULE     = "Error while saving module file."; 
const string MSG_ERRORSHAPEFILETYPE    = "The selected shapefile doesn't contain the correct shape type for this module.";
const string MSG_ERRORTASKCONFIG       = "Error while saving task config files.";
const string MSG_ERRORWORKINGFOLDER    = "Error creating the working folder.";
const string MSG_GEOPROJECTIONWITHOUT  = "The \'GeoProjection\' module must be used in conjunction with other modules using shapefiles.";
const string MSG_INVALIDSELECTION      = "Invalid selection.";
const string MSG_LANDBUILDER2ISRUNNING = "LandBuilder2 is running";
const string MSG_LANDBUILDER2MISSING   = "Not able to find LandBuilder2.exe.";
const string MSG_LANDBUILDER2NOTFOUND  = "LandBuilder2.exe not found in the specified position.";
const string MSG_LANDBUILD2WILLCREATE  = "LandBuilder2 will create the file: ";
const string MSG_MISSINGVISITORMAPDATA = "Missing data in Visitor Map parameters.";
const string MSG_MUSTBEFLOATING        = "Value must be a floating point number.";
const string MSG_MUSTBEINTEGER         = "Value must be an integer number.";
const string MSG_MUSTBEZEROORONE       = "Value must be equal to 0 or 1.";
const string MSG_NONEALONE             = "The \'SimpleMapper\' module must be used in conjunction with other modules using shapefiles.";
const string MSG_NONENOTNEEDED         = "No \'SimpleMapper\' module is needed by this project.";
const string MSG_NOTDEFINEDYET         = "NOT DEFINED YET";
const string MSG_ONLYONENONE           = "Only one \'SimpleMapper\' module can be used.";
const string MSG_ONLYONEGEOPROJECTION  = "Only one \'GeoProjection\' module can be used.";
const string MSG_ONLYONETRANSFORM2D    = "Only one \'Transform2D\' module can be used.";
const string MSG_TRANSFORM2DALONE      = "The \'Transform2D\' must be used in conjunction with other modules using shapefiles.";
const string MSG_WAITPLEASE            = "Wait please, ";
const string MSG_WHENTHISOPERATION     = "When this operation will be done, you can import this file in Visitor.";
const string MSG_WHENUSINGGEOPROJ      = "The \'GeoProjection\' module must be specified as first and must be followed by the \'Transform2D\' module.";
const string MSG_WHENUSINGSHAPEFILES   = "When using shapefiles the first module must be one from \'SimpleMapper\', \'Transform2D\' or \'GeoProjection\' modules.";
const string MSG_WHENUSINGTRANFORM2D   = "If used without \'GeoProjection\', the module \'Transform2D\' must be specified as first.";
const string MSG_WHENUSINGNONE         = "If used, the module \'SimpleMapper\' must be specified as first.";

//-----------------------------------------------------------------------------

const string HEAD_BORDER      = "Border";
const string HEAD_DBFFIELD    = "Dbf field"; 
const string HEAD_DBFPAR      = "Dbf parameters";
const string HEAD_DEMFILES    = "DEM files";
const string HEAD_DESTBOTTOM  = "Dest Bottom";
const string HEAD_DESTLEFT    = "Dest Left";
const string HEAD_DESTRIGHT   = "Dest Right";
const string HEAD_DESTTOP     = "Dest Top";
const string HEAD_FILENAME    = "File Name";
const string HEAD_FILETYPE    = "File Type";
const string HEAD_FILL        = "Fill";
const string HEAD_GLOBALPAR   = "Global parameters";
const string HEAD_NAME        = "Name";
const string HEAD_NOTES       = "Notes";
const string HEAD_OBJECTNAME  = "Object name";
const string HEAD_OBJECTPAR   = "Objects parameters";
const string HEAD_PARAMETER   = "Parameter";
const string HEAD_PATH        = "Path";
const string HEAD_RESOURCE    = "Resource";
const string HEAD_SHAPEFILE   = "Shapefile";
const string HEAD_SRCBOTTOM   = "Src Bottom";
const string HEAD_SRCLEFT     = "Src Left";
const string HEAD_SRCRIGHT    = "Src Right";
const string HEAD_SRCTOP      = "Src Top";
const string HEAD_VALUE       = "Value"; 

//-----------------------------------------------------------------------------

const string OBJ_NAME_FROMDBF = "From Dbf";

//-----------------------------------------------------------------------------

const unsigned int PREV_COLORS_NUMBER = 16;
const COLORREF PREV_COLORS[PREV_COLORS_NUMBER] = 
{
  RGB(255,   0,   0), // 0 - COLOR_RED
  RGB(  0, 255,   0), // 1 - COLOR_GREEN
  RGB(  0,   0, 255), // 2 - COLOR_BLUE
  RGB(255, 255,   0), // 3 - COLOR_YELLOW
  RGB(255,   0, 255), // 4 - COLOR_MAGENTA
  RGB(  0, 255, 255), // 5 - COLOR_CYAN
  RGB(255, 255, 255), // 6 - COLOR_WHITE
  RGB(128,   0,   0), // 7 - COLOR_DARKRED
  RGB(  0, 128,   0), // 8 - COLOR_DARKGREEN
  RGB(  0,   0, 128), // 9 - COLOR_DARKBLUE
  RGB(128, 128,   0), // 10 - COLOR_DARKYELLOW
  RGB(128,   0, 128), // 11 - COLOR_DARKMAGENTA 
  RGB(  0, 128, 128), // 12 - COLOR_DARKCYAN
  RGB(128, 128, 128), // 13 - COLOR_GREY
  RGB(192, 192, 192), // 14 - COLOR_LIGHTGREY
  RGB(  0,   0,   0)  // 15 - COLOR_BLACK
};

//-----------------------------------------------------------------------------

typedef enum
{
  COLOR_RED,
  COLOR_GREEN,
  COLOR_BLUE,
  COLOR_YELLOW,
  COLOR_MAGENTA,
  COLOR_CYAN,
  COLOR_WHITE,
  COLOR_DARKRED,
  COLOR_DARKGREEN,
  COLOR_DARKBLUE,
  COLOR_DARKYELLOW,
  COLOR_DARKMAGENTA, 
  COLOR_DARKCYAN,
  COLOR_GREY,
  COLOR_LIGHTGREY,
  COLOR_BLACK,
  COLOR_NUM
} LBPreviewColors;

//-----------------------------------------------------------------------------
