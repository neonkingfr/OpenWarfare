//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include ".\LBModuleList.h" 
#include ".\LBOptions.h" 
#include ".\LBError.h"
#include ".\LBVisitorMap.h"

#include ".\CTabDirectoriesDlg.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

class LBProject : public CObject
{
	DECLARE_SERIAL( LBProject )

protected:
	// the version
	int m_version;
	// the output name
	string m_outputName;
	// the Visitor map 
	LBVisitorMap m_visitorMap;
	// the options 
	LBOptions m_options;
	// the list of Land Builder modules
	LBModuleList m_moduleList;

public:
	// Default constructor
	LBProject();

	// copy constructor
	LBProject( const LBProject& other );

	virtual ~LBProject();

	LBProject& operator = ( const LBProject& other );

	// returns the version of this project
	int GetVersion() const;

	// returns the output name of this project
	string GetOutputName() const;

	// returns the visitor map of this project
	LBVisitorMap& GetVisitorMap();
	const LBVisitorMap& GetVisitorMap() const;

	// returns the options of this project
	LBOptions& GetOptions();

	// returns the directory list of this project
	LBDirectoryList& GetDirectoryList();

	// returns the module list of this project
	LBModuleList& GetModuleList();

	// sets the version of this project
	void SetVersion( int version );

	// sets the output name of this project
	void SetOutputName( const string& name );

	// sets the visitor map of this project
	void SetVisitorMap( const LBVisitorMap& visitorMap );

	// sets the options of this project with the given one
	void SetOptions( const LBOptions& options );

	// sets the directory list of this project with the given one
	void SetDirectoryList( const LBDirectoryList& directoryList );

	// sets the module list of this project with the given one
	void SetModuleList( const LBModuleList& moduleList );

	// adds the given module to the module list of this project
	void AddModule( LBModule* module );

	// sets the module of the module list at the given index with 
	// the given module 
	void SetModule( int index, LBModule* module );

	// removes the module of the module list at the given index 
	void RemoveModule( int index );

	// removes all the modules of the module list of this project
	void RemoveAllModules();

	// sets the default visitor map data
	void SetDefaultVisitorMap();

	// sets the default directory list
	void SetDefaultDirectoryList( CTabDirectoriesDlg& dlg );

	// resets this project
	void Reset();

	// returns the result from module validation
	// LBET_NOERROR if all modules can be exported to LandBuilder
	LBError ValidateModules();

	// save the main config file for this project
	// returns true if successfull
	bool SaveMainConfig( const string& fileName, const string& lbVersion );

	// save the task config files for all the modules in this project
	// returns true if successfull
	bool SaveTaskConfigs();

	virtual void Serialize( CArchive& ar );
};

//-----------------------------------------------------------------------------
