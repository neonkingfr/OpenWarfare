// CInPlaceComboBox.cpp : implementation file
//

#include "stdafx.h"
#include ".\CInPlaceComboBox.h"

/////////////////////////////////////////////////////////////////////////////
// CInPlaceList

CInPlaceComboBox::CInPlaceComboBox(int iItem, int iSubItem, vector<string>& plstItems, string selection)
{
	m_iItem = iItem;
	m_iSubItem = iSubItem;

	for(unsigned int i = 0; i < plstItems.size(); ++i)
	{
		m_lstItems.push_back(plstItems[i]);
	}

	m_Selection = selection;
	m_bESC = FALSE;
}

// -------------------------------------------------------------------------- //

CInPlaceComboBox::~CInPlaceComboBox()
{
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CInPlaceComboBox, CComboBox)
	ON_WM_CREATE()
	ON_WM_KILLFOCUS()
	ON_WM_CHAR()
	ON_WM_NCDESTROY()
	ON_CONTROL_REFLECT(CBN_CLOSEUP, OnCloseUp)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

/////////////////////////////////////////////////////////////////////////////
// CInPlaceList message handlers

int CInPlaceComboBox::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CComboBox::OnCreate(lpCreateStruct) == -1) return -1;

	// Set the proper font
	CFont* font = GetParent()->GetFont();
	SetFont(font);

	for(unsigned int i = 0; i < m_lstItems.size(); ++i)
	{
		AddString(m_lstItems[i].c_str());
	}

	SelectString(-1, m_Selection.c_str());
	SetFocus();
	return 0;
}

// -------------------------------------------------------------------------- //

BOOL CInPlaceComboBox::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			::TranslateMessage(pMsg);
			::DispatchMessage(pMsg);
			return TRUE;				// DO NOT process further
		}
	}

	return CComboBox::PreTranslateMessage(pMsg);
}

// -------------------------------------------------------------------------- //

void CInPlaceComboBox::OnKillFocus(CWnd* pNewWnd)
{
	CComboBox::OnKillFocus(pNewWnd);

	CString str;
	GetWindowText(str);

	// Send Notification to parent of ListView ctrl
	LV_DISPINFO dispinfo;
	dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
	dispinfo.hdr.idFrom = GetDlgCtrlID();
	dispinfo.hdr.code = LVN_ENDLABELEDIT;

	dispinfo.item.mask = LVIF_TEXT;
	dispinfo.item.iItem = m_iItem;
	dispinfo.item.iSubItem = m_iSubItem;
	dispinfo.item.pszText = m_bESC ? NULL : LPTSTR((LPCTSTR)str);
	dispinfo.item.cchTextMax = str.GetLength();

	GetParent()->GetParent()->SendMessage(WM_NOTIFY, GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo);

	PostMessage(WM_CLOSE);
}

// -------------------------------------------------------------------------- //

void CInPlaceComboBox::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(nChar == VK_ESCAPE || nChar == VK_RETURN)
	{
		if(nChar == VK_ESCAPE) m_bESC = TRUE;
		GetParent()->SetFocus();
		return;
	}

	CComboBox::OnChar(nChar, nRepCnt, nFlags);
}

// -------------------------------------------------------------------------- //

void CInPlaceComboBox::OnNcDestroy()
{
	CComboBox::OnNcDestroy();

	delete this;
}

// -------------------------------------------------------------------------- //

void CInPlaceComboBox::OnCloseUp()
{
	GetParent()->SetFocus();
}
