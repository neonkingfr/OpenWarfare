#include "stdafx.h"

#include ".\CListCtrlEx.h"
#include ".\CInPlaceEdit.h"
#include ".\CInPlaceComboBox.h"
#include ".\CInPlaceColorComboBox.h"

#include "..\LBGlobalVariables.h"

BEGIN_MESSAGE_MAP(CListCtrlEx, CListCtrl)
// Item editing
    ON_WM_HSCROLL()
    ON_WM_VSCROLL()
    ON_WM_LBUTTONDOWN()
    ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, OnEndLabelEdit)
// Item formatting
    ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomdraw)
	ON_WM_DESTROY()
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// Constructor (used by item formatting)
// -------------------------------------------------------------------------- //

CListCtrlEx::CListCtrlEx()
{
	columns.SetSize(0);
	columns.RemoveAll();

	m_Default_pCFont = NULL;

	// Set default HIGHLIGHT colors
	//
	m_highlighttext = ::GetSysColor(COLOR_HIGHLIGHTTEXT);
	m_highlight     = ::GetSysColor(COLOR_HIGHLIGHT);

	m_EditingRow = -1;
	m_ValidationPassed = true;
}

// -------------------------------------------------------------------------- //
// Destructor (used by item formatting)
// -------------------------------------------------------------------------- //

CListCtrlEx::~CListCtrlEx()
{ 	
	INT_PTR nCols = columns.GetSize();
	for(int nCol = 0; nCol < nCols;nCol++)
	{	
		if(columns[nCol] != NULL) this->Free_LS_item(columns[nCol]);
	}

	columns.RemoveAll();
	columns.SetSize(0);
}

// -------------------------------------------------------------------------- //
// Members functions for item editing
// -------------------------------------------------------------------------- //

void CListCtrlEx::SetEditType(EditTypes editType)
{
	m_EditType = editType;
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetComboItems(const vector<string>& itemList)
{
	m_Items.clear();

	for(unsigned int i = 0; i < itemList.size(); ++i)
	{
		m_Items.push_back(itemList[i]);
	}
}

// -------------------------------------------------------------------------- //
// HitTestEx	- Determine the row index and column index for a point
// Returns	- the row index or -1 if point is not over a row
// point	- point to be tested.
// col		- to hold the column index
int CListCtrlEx::HitTestEx(CPoint& point, int* col) const
{
	int colnum = 0;
	int row = HitTest(point, NULL);

	if(col) *col = 0;

	// Make sure that the ListView is in LVS_REPORT
	if((GetWindowLong(m_hWnd, GWL_STYLE) & LVS_TYPEMASK) != LVS_REPORT) return row;

	// Get the top and bottom row visible
	row = GetTopIndex();
	int bottom = row + GetCountPerPage();
	if(bottom > GetItemCount()) bottom = GetItemCount();

	// Get the number of columns
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumnCount = pHeader->GetItemCount();

	// Loop through the visible rows
	for(;row <= bottom; ++row)
	{
		// Get bounding rect of item and check whether point falls in it.
		CRect rect;
		GetItemRect(row, &rect, LVIR_BOUNDS);
		if(rect.PtInRect(point))
		{
			// Now find the column
			for(colnum = 0; colnum < nColumnCount; ++colnum)
			{
				int colwidth = GetColumnWidth(colnum);
				if(point.x >= rect.left && point.x <= (rect.left + colwidth))
				{
					if(col) *col = colnum;
					return row;
				}
				rect.left += colwidth;
			}
		}
	}
	return -1;
}

// -------------------------------------------------------------------------- //

// EditSubLabel		- Start edit of a sub item label
// Returns		- Temporary pointer to the new edit control
// nItem		- The row index of the item to edit
// nCol			- The column of the sub item.
CEdit* CListCtrlEx::ShowInPlaceEdit(int nItem, int nCol)
{
	// The returned pointer should not be saved

	// Make sure that the item is visible
	if(!EnsureVisible(nItem, TRUE)) return NULL;

	// Make sure that nCol is valid
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumnCount = pHeader->GetItemCount();
	if(nCol >= nColumnCount || GetColumnWidth(nCol) < 5) return NULL;

	// Get the column offset
	int offset = 0;
	for(int i = 0; i < nCol; ++i)
	{
		offset += GetColumnWidth(i);
	}

	CRect rect;
	GetItemRect(nItem, &rect, LVIR_BOUNDS);

	// Now scroll if we need to expose the column
	CRect rcClient;
	GetClientRect(&rcClient);
	if(offset + rect.left < 0 || offset + rect.left > rcClient.right)
	{
		CSize size;
		size.cx = offset + rect.left;
		size.cy = 0;
		Scroll(size);
		rect.left -= size.cx;
	}

	// Get Column alignment
	LV_COLUMN lvcol;
	lvcol.mask = LVCF_FMT;
	GetColumn(nCol, &lvcol);
	DWORD dwStyle ;
	if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
	{
		dwStyle = ES_LEFT;
	}
	else if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
	{
		dwStyle = ES_RIGHT;
	}
	else 
	{
		dwStyle = ES_CENTER;
	}

	rect.left += offset;
	rect.right = rect.left + GetColumnWidth(nCol);
	rect.bottom -= 1;
	if(rect.right > rcClient.right) rect.right = rcClient.right;

	dwStyle |= WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL;
	CEdit* pEdit = new CInPlaceEdit(nItem, nCol, GetItemText(nItem, nCol));
	pEdit->Create(dwStyle, rect, this, 1);

	return pEdit;
}

// -------------------------------------------------------------------------- //

// ShowInPlaceList		- Creates an in-place drop down list for any 
//				- cell in the list view control
// Returns			- A temporary pointer to the combo-box control
// nItem			- The row index of the cell
// nCol				- The column index of the cell
// lstItems			- A list of strings to populate the control with
// nSel				- Index of the initial selection in the drop down list
CComboBox* CListCtrlEx::ShowInPlaceComboBox(int nItem, int nCol)
{
	// The returned pointer should not be saved

	// Make sure that the item is visible
	if(!EnsureVisible(nItem, TRUE)) return NULL;

	// Make sure that nCol is valid 
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumnCount = pHeader->GetItemCount();
	if(nCol >= nColumnCount || GetColumnWidth(nCol) < 10) return NULL;

	// Get the column offset
	int offset = 0;
	for(int i = 0; i < nCol; ++i)
	{
		offset += GetColumnWidth(i);
	}

	CRect rect;
	GetItemRect(nItem, &rect, LVIR_BOUNDS);

	// Now scroll if we need to expose the column
	CRect rcClient;
	GetClientRect(&rcClient);
	if(offset + rect.left < 0 || offset + rect.left > rcClient.right)
	{
		CSize size;
		size.cx = offset + rect.left;
		size.cy = 0;
		Scroll(size);
		rect.left -= size.cx;
	}

	rect.left += offset; 
	rect.right = rect.left + GetColumnWidth(nCol); 
	int height = rect.bottom - rect.top;
	rect.bottom += 7 * height;
	if(rect.right > rcClient.right) rect.right = rcClient.right;

	CString cText = GetItemText(nItem, nCol);
	string text = cText;

	DWORD dwStyle = WS_CHILD|WS_VISIBLE|WS_VSCROLL|CBS_DROPDOWNLIST|CBS_DISABLENOSCROLL;
	CComboBox* pList = new CInPlaceComboBox(nItem, nCol, m_Items, text);
	pList->Create(dwStyle, rect, this, 1);
	pList->SetItemHeight(-1, height);
	pList->SetHorizontalExtent(GetColumnWidth(nCol));

	return pList;
}

// -------------------------------------------------------------------------- //

CComboBox* CListCtrlEx::ShowInPlaceColorComboBox(int nItem, int nCol)
{
	// The returned pointer should not be saved

	// Make sure that the item is visible
	if(!EnsureVisible(nItem, TRUE)) return NULL;

	// Make sure that nCol is valid 
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumnCount = pHeader->GetItemCount();
	if(nCol >= nColumnCount || GetColumnWidth(nCol) < 10) return NULL;

	// Get the column offset
	int offset = 0;
	for(int i = 0; i < nCol; ++i)
	{
		offset += GetColumnWidth(i);
	}

	CRect rect;
	GetItemRect(nItem, &rect, LVIR_BOUNDS);

	// Now scroll if we need to expose the column
	CRect rcClient;
	GetClientRect(&rcClient);
	if(offset + rect.left < 0 || offset + rect.left > rcClient.right)
	{
		CSize size;
		size.cx = offset + rect.left;
		size.cy = 0;
		Scroll(size);
		rect.left -= size.cx;
	}

	rect.left += offset; 
	rect.right = rect.left + GetColumnWidth(nCol); 
	int height = rect.bottom - rect.top;
	rect.bottom += 7 * height;
	if(rect.right > rcClient.right) rect.right = rcClient.right;

	CString cText = GetItemText(nItem, nCol);
	string text = cText;
	UINT nText = atoi(text.c_str());

	vector<COLORREF> colors;
	for(unsigned int i = 0; i < PREV_COLORS_NUMBER; ++i)
	{
		colors.push_back(PREV_COLORS[i]);
	}

	DWORD dwStyle = WS_CHILD|WS_VISIBLE|WS_VSCROLL|CBS_DROPDOWNLIST|CBS_DISABLENOSCROLL|CBS_OWNERDRAWFIXED;
	CComboBox* pList = new CInPlaceColorComboBox(nItem, nCol, colors, nText);
	pList->Create(dwStyle, rect, this, 1);
	pList->SetItemHeight(-1, height);
	pList->SetHorizontalExtent(GetColumnWidth(nCol));

	return pList;
}

// -------------------------------------------------------------------------- //
// Handlers for item editing
// -------------------------------------------------------------------------- //

void CListCtrlEx::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if(GetFocus() != this) SetFocus();
	CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if(GetFocus() != this) SetFocus();
	CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO* plvDispInfo = (LV_DISPINFO*)pNMHDR;
	LV_ITEM* plvItem = &plvDispInfo->item;

	if (plvItem->pszText != NULL)
	{
		SetItemText(plvItem->iItem, plvItem->iSubItem, plvItem->pszText);
		GetParent()->PostMessage(MY_ENDEDITMSG, (WPARAM)plvItem->iItem, (LPARAM)plvItem->iSubItem);
		m_EditingRow = -1;
	}

	*pResult = FALSE;
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::OnLButtonDown(UINT nFlags, CPoint point)
{
	// checks which column has ben selected, if it is the first
	// disable the editing
	int colnum;
	int index = HitTestEx(point, &colnum);

	LONG prevStyle = GetWindowLong(m_hWnd, GWL_STYLE);
	LONG newStyle;
	if(colnum == 0) 
	{
		newStyle = prevStyle & ~LVS_EDITLABELS;
		::SetWindowLong(m_hWnd, GWL_STYLE, newStyle);
	}
	else
	{
		newStyle = prevStyle | LVS_EDITLABELS;
		::SetWindowLong(m_hWnd, GWL_STYLE, newStyle);
	}

	CListCtrl::OnLButtonDown(nFlags, point);

	::SetWindowLong(m_hWnd, GWL_STYLE, prevStyle);

	if(m_ValidationPassed)
	{
		if(index != -1)
		{
			UINT flag = LVIS_FOCUSED;
			if((GetItemState(index, flag) & flag) == flag && colnum > 0)
			{
				// Add check for LVS_EDITLABELS
				if(GetWindowLong(m_hWnd, GWL_STYLE) & LVS_EDITLABELS)
				{
					switch(m_EditType)
					{
					case ET_EDIT:
						m_EditingRow = index;
						ShowInPlaceEdit(index, colnum);
						break;
					case ET_COMBO:
						m_EditingRow = index;
						ShowInPlaceComboBox(index, colnum);
						break;
					case ET_COLORCOMBO:
						m_EditingRow = index;
						ShowInPlaceColorComboBox(index, colnum);
						break;
					default:
						SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
					}
				}
			}
			else
			{
				m_EditingRow = -1;
				SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
			}
		}
	}
	else
	{
		SetItemState(m_EditingRow, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		m_ValidationPassed = true;	
		m_EditingRow = -1;
	}
}

// -------------------------------------------------------------------------- //

int CListCtrlEx::GetEditingRow() const
{
	return m_EditingRow;
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetValidationPassed(bool validationPassed)
{
	m_ValidationPassed = validationPassed;
}

// -------------------------------------------------------------------------- //

bool CListCtrlEx::GetValidationPassed() const
{
	return m_ValidationPassed;
}

// -------------------------------------------------------------------------- //
// Members functions for item formatting
// -------------------------------------------------------------------------- //

int CListCtrlEx::InsertItem(const LVITEM* pItem)
{	
	// So we need to construct our style object for this item, store
	// it into the lParam of pItem. But before this we must store it's lParam
	// for mask this usage for the programmer. lparam must be usable by the same
	// way that the original CListCtrl. then we store it into our style object

	// For let after to modify lParam (pItem is a const pointer)
	//
	LVITEM pItem2 = (LVITEM) *pItem;

	// Create a new LS_item for store data
	//
	LS_item* lpLS_item;
	lpLS_item = NULL;
	lpLS_item = new LS_item;

	Init_LS_item(lpLS_item);		// Init the structure

	// Store lParam from the pItem
	//
	if(pItem2.mask & LVIF_PARAM)
	{
		lpLS_item->lParam = pItem2.lParam;
		lpLS_item->mParam = true;
	}

	// Set the LS_Item to the lParam of pItem
	//
	pItem2.mask = pItem2.mask | LVIF_PARAM;				// Add LVIF_PARAM option
	pItem2.lParam = (LPARAM)(void*)lpLS_item;			// Link our structure

	// Create structure for each subitems (with default style too)
	//
	int nSubItems = this->GetHeaderCtrl()->GetItemCount() - 1;

	LS_item* lpLS_subitem;
	for(int bcl = 0; bcl < nSubItems ;bcl++)
	{	
		lpLS_subitem = NULL;
		lpLS_subitem = new LS_item;

		Init_LS_item(lpLS_subitem,false);				// Init the structure

		lpLS_item->subitems.Add(lpLS_subitem);			// Add the structure of this subitem to the list.
	}

	return (CListCtrl::InsertItem(&pItem2));
}

// -------------------------------------------------------------------------- //

int CListCtrlEx::InsertItem(UINT nMask, int nItem, LPCTSTR lpszItem, UINT nState, UINT nStateMask, int nImage, LPARAM lParam)
{
	// So we must create an LVItem and insert it
	LVITEM lvitem;
	lvitem.mask = nMask;
	lvitem.iItem = nItem;
	lvitem.iSubItem = 0;
	lvitem.pszText = (char*)lpszItem;
	lvitem.cchTextMax = 0;
	if(nMask & LVIF_TEXT) lvitem.cchTextMax = static_cast<int>(strlen(lvitem.pszText));
	lvitem.state = nState;
	lvitem.stateMask = nStateMask;
	lvitem.iImage = nImage;
	lvitem.lParam = lParam;

	// Insert it
	return (this->InsertItem(&lvitem));
}

// -------------------------------------------------------------------------- //

int CListCtrlEx::InsertItem(int nItem, LPCTSTR lpszItem)
{ 
	return (this->InsertItem(LVIF_TEXT, nItem, lpszItem, 0, 0, 0, 0)); 
}

// -------------------------------------------------------------------------- //

int CListCtrlEx::InsertItem(int nItem, LPCTSTR lpszItem, int nImage)
{ 
	return (this->InsertItem(LVIF_TEXT|LVIF_IMAGE, nItem, lpszItem, 0, 0, nImage, 0)); 
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::SetItem(const LVITEM* pItem)
{
	if(pItem->mask & LVIF_PARAM)
	{	// We must assume that lParam member is correctly managed in our derived class
		//
		LVITEM pItem2 = (LVITEM) *pItem;

		// Try to retrieve the root pItem
		//
		LVITEM pRootItem;
		InitLVITEM(pItem2.iItem, 0, &pRootItem);

		LS_item* lpLS_item = NULL;
		lpLS_item = (LS_item*)pRootItem.lParam;

		// Management of lParam
		//
		lpLS_item->lParam = pItem2.lParam;
		lpLS_item->mParam = true;

		// No lParam for this subitem
		//
		pItem2.lParam = NULL;
		pItem2.mask = pItem2.mask & (0xFFFF - LVIF_PARAM);

		return (CListCtrl::SetItem(&pItem2));
	}

	return (CListCtrl::SetItem(pItem));
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::SetItem(int nItem, int nSubItem, UINT nMask, LPCTSTR lpszItem, int nImage, UINT nState, UINT nStateMask, LPARAM lParam)
{	
	return (this->SetItem(nItem, nSubItem, nMask, lpszItem, nImage, nState, nStateMask, lParam, 0)); 
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::SetItem(int nItem, int nSubItem, UINT nMask, LPCTSTR lpszItem, int nImage, UINT nState, UINT nStateMask, LPARAM lParam, int nIndent)
{
	// So we must create an LVItem and insert it
	LVITEM lvitem;
	lvitem.mask = nMask;
	lvitem.iItem = nItem;
	lvitem.iSubItem = nSubItem;
	lvitem.pszText = (char*)lpszItem;
	lvitem.cchTextMax = 0;
	if(nMask & LVIF_TEXT) lvitem.cchTextMax = static_cast<int>(strlen(lvitem.pszText));
	lvitem.state = nState;
	lvitem.stateMask = nStateMask;
	lvitem.iImage = nImage;
	lvitem.lParam = lParam;
	lvitem.iIndent = nIndent;

	// Insert it
	return (this->SetItem(&lvitem));
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::DeleteItem(int nItem)
{
	// We must delete all LS_item before deleting this Item
	// So we must retrieve it for all columns
	//

	// retrieve the LS_item structure for this item
	//
	LVITEM pItem;
	InitLVITEM(nItem, 0, &pItem);

	LS_item* lpLS_item = NULL;
	lpLS_item = (LS_item*)pItem.lParam;

	// Free his structure style
	//
	this->Free_LS_item(lpLS_item);

	return (CListCtrl::DeleteItem(nItem));
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::DeleteAllItems()
{
	// Get number of Rows
	//
	int nItems = CListCtrl::GetItemCount();

	// Delete Each row (One by One for managed our structure destruction)
	//
	for(int nItem = 0; nItem < nItems; nItem++)
	{
		this->DeleteItem(0);
	}

	// Call the base class DeleteAllItems (maybe some treatments must be do)
	//
	return (CListCtrl::DeleteAllItems());
}

// -------------------------------------------------------------------------- //

DWORD CListCtrlEx::GetItemData(int nItem)
{	
	// retrieve the LS_item structure
	//
	LVITEM pItem;
	InitLVITEM(nItem, 0, &pItem);

	LS_item* lpLS_item = NULL;
	lpLS_item = (LS_item*)pItem.lParam;

	if(lpLS_item->mParam)
	{
		return (lpLS_item->lParam);
	}
	else
	{
		return NULL;	// No significant if the item mask hasn't LVIF_PARAM
	}
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::SetItemData(int nItem, DWORD dwData)
{
	// Retrieve the LS_item structure
	//
	LVITEM pItem;
	InitLVITEM(nItem, 0, &pItem);

	LS_item* lpLS_item = NULL;
	lpLS_item = (LS_item*)pItem.lParam;

	// set the new data
	//
	if(lpLS_item->mParam) lpLS_item->lParam = dwData;

	// Return false if the LVIF_PARAM isn't set for this item
	//
	return (lpLS_item->mParam);
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::GetItem(LV_ITEM* pItem)
{
	/*
	typedef struct _LV_ITEM {
    UINT   mask;
    int    iItem;
    int    iSubItem;
    UINT   state;
    UINT   stateMask;
    LPSTR  pszText;
    int    cchTextMax;
    int    iImage;
    LPARAM lParam;       // 32-bit value to associate with item
	} LV_ITEM;
  */

	// Retrieve Information from Base class
	//
	BOOL Result = CListCtrl::GetItem(pItem);
	if(Result)
	{	// Replace the lParam value
		//
		LS_item* lpLS_item = NULL;
		lpLS_item = (LS_item*)pItem->lParam;

		if(lpLS_item->mParam)
		{	// Ok set the original lParam
			//
			pItem->lParam = lpLS_item->lParam;
		}
		else
		{	// No lParam member
			//
			pItem->lParam = NULL;
			pItem->mask = pItem->mask & (0xFFFF - LVIF_PARAM);
		}
	}

	return Result;
}

// -------------------------------------------------------------------------- //

int CListCtrlEx::InsertColumn(int nCol, const LVCOLUMN* pColumn)
{
	int index = CListCtrl::InsertColumn(nCol, pColumn);
	if(index == -1) return index;

	// Adjust Size of columns Array
	//
	this->columns.SetSize(this->GetHeaderCtrl()->GetItemCount());

	// Must update the "columns" array for hold a custom column style if needed
	//
	this->columns.InsertAt(index, NULL, 1);

	// We must adjust all LS_item into the CListCtrl
	//
	int nRows = CListCtrl::GetItemCount();

	// Variables needed
	LVITEM pItem;
	LS_item* lpLS_item;
	LS_item* lpLS_subitem;

	for(int nItem = 0; nItem < 	nRows; nItem++)
	{	// Retrieve the LS_item for this item
		//
		InitLVITEM(nItem, 0, &pItem);

		lpLS_item = NULL;
		lpLS_item = (LS_item*)pItem.lParam;

		// Create the style for the new subitem
		//
		lpLS_subitem = NULL;
		lpLS_subitem = new LS_item;

		this->Init_LS_item(lpLS_subitem, false);  // Init the structure

		// Insert it into his correct place (shift all element above)
		//
		if(index > 0)
		{
			lpLS_item->subitems.InsertAt(index - 1, lpLS_subitem, 1);
		}
		else
		{
			lpLS_item->subitems.InsertAt(index, lpLS_subitem, 1);
		}
	}

	return index;
}

// -------------------------------------------------------------------------- //

int CListCtrlEx::InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat, int nWidth, int nSubItem)
{	// Create an LVCOLUMN item
	/*
	typedef struct _LVCOLUMN {
   	 UINT mask;
   	 int fmt;
   	 int cx;
   	 LPTSTR pszText;
   	 int cchTextMax;
   	 int iSubItem;
	#if (_WIN32_IE >= 0x0300)
   	 int iImage;
   	 int iOrder;
	#endif
	} LVCOLUMN, *LPLVCOLUMN;
	*/
	LVCOLUMN lvColumn;

	lvColumn.mask = LVCF_FMT | LVCF_TEXT;
	if(nWidth != -1) lvColumn.mask = lvColumn.mask | LVCF_WIDTH;
	if(nSubItem != -1) lvColumn.mask = lvColumn.mask | LVCF_SUBITEM;

	lvColumn.fmt = nFormat;
	lvColumn.cx = nWidth;
	lvColumn.pszText = (char*)lpszColumnHeading;
	lvColumn.cchTextMax  = static_cast<int>(strlen(lvColumn.pszText));
	lvColumn.iSubItem = nSubItem;

	return (this->InsertColumn(nCol, &lvColumn));
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::DeleteColumn(int nCol)
{
	// Start be delete the Column Style if needed
	//
	LS_item* lpLS_col = NULL;
	lpLS_col = this->columns[nCol];

	if(lpLS_col != NULL) this->Free_LS_item(lpLS_col);

	// Update the Array
	//
	columns.RemoveAt(nCol, 1);

	// Some Subitems will be deleted, we must delete also theses structure style
	//
	int nItems = CListCtrl::GetItemCount();
	for(int nItem = 0; nItem < nItems; nItem++)
	{ 	
		LVITEM pItem;
		InitLVITEM(nItem, 0, &pItem);

		LS_item* lpLS_root = NULL;
		LS_item* lpLS_item = NULL;
		lpLS_root = (LS_item*) pItem.lParam;

		if(nCol > 0)
		{	
			lpLS_item = lpLS_root->subitems[nCol - 1];
			lpLS_root->subitems.RemoveAt(nCol - 1, 1);
		}
		else
		{	
			if( this->GetHeaderCtrl()->GetItemCount() > 1)
			{	
				lpLS_item = lpLS_root->subitems[0];
				lpLS_root->subitems.RemoveAt(0, 1);
			}
			else
			{	
				this->DeleteAllItems();
				return (CListCtrl::DeleteColumn(nCol));
			}
		}
		this->Free_LS_item(lpLS_item);
	}

	// We can delete the Column now
	//
	return (CListCtrl::DeleteColumn(nCol));
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::SetColumnOrderArray(int iCount, LPINT piArray)
{	// Must rearrange the "columns" array
	//
	CArray<struct iLS_item*, struct iLS_item*> old;

	old.SetSize(columns.GetSize());
	old.RemoveAll();
	old.Copy(this->columns);

	for(int nItem = 0; nItem < iCount; nItem++)
	{
		this->columns.SetAt(nItem, old[piArray[nItem]]);
	}

	old.RemoveAll();

	return (CListCtrl::SetColumnOrderArray(iCount, piArray));
}

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //

PFNLVCOMPARE LIS_CompFunc;															// Store the user-defined callback function

int CALLBACK LIS_CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{	// Must call the origine compare function
	//
	LPARAM lp1;					// the real lParam1
	LPARAM lp2;					// the real lParam2
	LS_item * lpLS_item;

	// Set the correct lParam1
	//
	lpLS_item = NULL;
	lpLS_item = (LS_item*) lParam1;
	lp1 = lpLS_item->lParam;

	// Set the correct lParam2
	//
	lpLS_item = NULL;
	lpLS_item = (LS_item*) lParam2;
	lp2 = lpLS_item->lParam;

	// Call now the real callback function defined by the user
	//
	return LIS_CompFunc(lp1, lp2, lParamSort);
}

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::SortItems(PFNLVCOMPARE pfnCompare, DWORD dwData)
{	
	LIS_CompFunc = pfnCompare;
	PFNLVCOMPARE callback_func = LIS_CompareFunc;
	return (CListCtrl::SortItems(callback_func, dwData));
}

// -------------------------------------------------------------------------- //

int CListCtrlEx::FindItem(LV_FINDINFO* pFindInfo, int nStart)
{	/*
	typedef struct _LV_FINDINFO {
		UINT flags;    //see below
		LPCSTR psz;    //see below
		LPARAM lParam; //see below
	} LV_FINDINFO;
	*/

	if(pFindInfo->flags & LVFI_PARAM)
	{	// We must implement this method ourselve because we can't access directly to the lParam member
		// When LVFI_PARAM is used, all other flag are ignored
		//

		int nItems = this->GetItemCount();
		for(int nItem = nStart + 1; nItem < nItems; nItem++)
		{	// Get lParam value for this item
			//
			LVITEM pItem;
			InitLVITEM(nItem,0,&pItem);

			LS_item * lpLS_item = NULL;
			lpLS_item = (LS_item *)pItem.lParam;

			if(lpLS_item->lParam == pFindInfo->lParam)
				return nItem;
		}
		return -1;
	}
	else
	{	// We can call the base class method
		//
		return CListCtrl::FindItem(pFindInfo,nStart);
	}
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetItemTxtColor(int nItem,int nSubItem,COLORREF txtColor,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nItem,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	if(nSubItem > 0)
		lpLS_item = lpLS_item->subitems[nSubItem - 1];

	// no we can update the style
	//
	lpLS_item->txtColor = txtColor;

	// Redraw it
	if(redraw)	CListCtrl::Update(nItem);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetItemBgColor(int nItem,int nSubItem,COLORREF txtBgColor,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nItem,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	if(nSubItem > 0)
		lpLS_item = lpLS_item->subitems[nSubItem - 1];

	// no we can update the style
	//
	lpLS_item->bgColor = txtBgColor;

	// Redraw it
	if(redraw)	CListCtrl::Update(nItem);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetItemStyle(int nItem,int nSubItem,DWORD Style,bool redraw)
{
	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nItem,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	if(nSubItem > 0)
		lpLS_item = lpLS_item->subitems[ nSubItem - 1];

	// no we can update the style
	//
	lpLS_item->StyleFlag = Style;

	DWORD mask = LIS_BOLD | LIS_ITALIC | LIS_UNDERLINE| LIS_STROKE ;
	lpLS_item->in_use = (Style & mask) > 0;

	// if any font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_item);

	// Redraw it
	if(redraw)	CListCtrl::Update(nItem);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetItemFont(int nItem,int nSubItem,CFont * pFont,bool redraw)
{
	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nItem,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	if(nSubItem > 0)
		lpLS_item = lpLS_item->subitems[ nSubItem - 1];

	// if any internal font exist for this item then delete it
	//
	Free_LS_font(lpLS_item);

	lpLS_item->cfont = pFont;
	lpLS_item->ifont = false;

	lpLS_item->in_use = true;

	// Redraw it
	if(redraw)	CListCtrl::Update(nItem);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetItemSelectedStyle(int nItem,int nSubItem,DWORD Style,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nItem,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	if(nSubItem > 0)
		lpLS_item = lpLS_item->subitems[ nSubItem - 1];

	// Take the selected style structure
	//
	if(lpLS_item->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_item->selected_style = new LS_item;
		this->Init_LS_item( lpLS_item->selected_style );
	}
	lpLS_item = lpLS_item->selected_style;

	// no we can update the style
	//
	lpLS_item->StyleFlag = Style;

	DWORD mask = LIS_BOLD | LIS_ITALIC | LIS_UNDERLINE| LIS_STROKE ;
	lpLS_item->in_use = (Style & mask) > 0;

	// if any font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_item);

	// Redraw it
	if(redraw)	CListCtrl::Update(nItem);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetItemSelectedTxtColor(int nItem,int nSubItem,COLORREF txtColor,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nItem,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	if(nSubItem > 0)
		lpLS_item = lpLS_item->subitems[nSubItem - 1];

	// Take the selected style structure
	//
	if(lpLS_item->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_item->selected_style = new LS_item;
		this->Init_LS_item( lpLS_item->selected_style );
	}
	lpLS_item = lpLS_item->selected_style;

	// no we can update the style
	//
	lpLS_item->txtColor = txtColor;

	// Redraw it
	if(redraw)	CListCtrl::Update(nItem);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetItemSelectedBgColor(int nItem,int nSubItem,COLORREF txtBgColor,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nItem,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	if(nSubItem > 0)
		lpLS_item = lpLS_item->subitems[nSubItem - 1];

	// Take the selected style structure
	//
	if(lpLS_item->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_item->selected_style = new LS_item;
		this->Init_LS_item( lpLS_item->selected_style );
	}
	lpLS_item = lpLS_item->selected_style;

	// no we can update the style
	//
	lpLS_item->bgColor = txtBgColor;

	// Redraw it
	if(redraw)	CListCtrl::Update(nItem);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetItemSelectedFont(int nItem,int nSubItem,CFont * pFont,bool redraw)
{
	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nItem,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	if(nSubItem > 0)
		lpLS_item = lpLS_item->subitems[ nSubItem - 1];

	// Take the selected style structure
	//
	if(lpLS_item->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_item->selected_style = new LS_item;
		this->Init_LS_item( lpLS_item->selected_style );
	}
	lpLS_item = lpLS_item->selected_style;

	// if any internal font exist for this item then delete it
	//
	Free_LS_font(lpLS_item);

	lpLS_item->cfont = pFont;
	lpLS_item->ifont = false;

	lpLS_item->in_use = true;

	// Redraw it
	if(redraw)	CListCtrl::Update(nItem);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetRowStyle(int nRow,DWORD Style,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nRow,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_row = NULL;
	lpLS_row = lpLS_item->row_style;

	if(lpLS_row == NULL)
	{	// We must create one
		//
		lpLS_row = new LS_item;
		this->Init_LS_item(lpLS_row,false);

		// attach to the item
		//
		lpLS_item->row_style = lpLS_row;
	}

	// no we can update the style
	//
	lpLS_row->StyleFlag = Style;

	DWORD mask = LIS_BOLD | LIS_ITALIC | LIS_UNDERLINE| LIS_STROKE ;
	lpLS_row->in_use = (Style & mask) > 0;

	// if any font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_row);

	// Redraw it
	if(redraw)	CListCtrl::Update(nRow);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetRowTxtColor(int nRow,COLORREF txtColor,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nRow,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_row = NULL;
	lpLS_row = lpLS_item->row_style;

	if(lpLS_row == NULL)
	{	// We must create one
		//
		lpLS_row = new LS_item;
		this->Init_LS_item(lpLS_row,false);

		// attach to the item
		//
		lpLS_item->row_style = lpLS_row;
	}

	// no we can update the style
	//
	lpLS_row->txtColor = txtColor;

	// Redraw it
	if(redraw)	CListCtrl::Update(nRow);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetRowBgColor(int nRow,COLORREF txtBgColor,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nRow,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_row = NULL;
	lpLS_row = lpLS_item->row_style;

	if(lpLS_row == NULL)
	{	// We must create one
		//
		lpLS_row = new LS_item;
		this->Init_LS_item(lpLS_row,false);

		// attach to the item
		//
		lpLS_item->row_style = lpLS_row;
	}

	// no we can update the style
	//
	lpLS_row->bgColor = txtBgColor;

	// Redraw it
	if(redraw)	CListCtrl::Update(nRow);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetRowFont(int nRow,CFont * pFont,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nRow,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_row = NULL;
	lpLS_row = lpLS_item->row_style;

	if(lpLS_row == NULL)
	{	// We must create one
		//
		lpLS_row = new LS_item;
		this->Init_LS_item(lpLS_row,false);

		// attach to the item
		//
		lpLS_item->row_style = lpLS_row;
	}

	// if any internal font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_row);

	// no we can update the style
	//
	lpLS_row->cfont = pFont;
	lpLS_row->ifont = false;

	lpLS_row->in_use = true;

	// Redraw it
	if(redraw)	CListCtrl::Update(nRow);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetRowSelectedStyle(int nRow,DWORD Style,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nRow,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_row = NULL;
	lpLS_row = lpLS_item->row_style;

	if(lpLS_row == NULL)
	{	// We must create one
		//
		lpLS_row = new LS_item;
		this->Init_LS_item(lpLS_row,false);

		// attach to the item
		//
		lpLS_item->row_style = lpLS_row;
	}

	// Take the selected style structure
	//
	if(lpLS_row->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_row->selected_style = new LS_item;
		this->Init_LS_item( lpLS_row->selected_style );
	}
	lpLS_row = lpLS_row->selected_style;

	// no we can update the style
	//
	lpLS_row->StyleFlag = Style;

	DWORD mask = LIS_BOLD | LIS_ITALIC | LIS_UNDERLINE| LIS_STROKE ;
	lpLS_row->in_use = (Style & mask) > 0;

	// if any font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_row);

	// Redraw it
	if(redraw)	CListCtrl::Update(nRow);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetRowSelectedTxtColor(int nRow,COLORREF txtColor,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nRow,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_row = NULL;
	lpLS_row = lpLS_item->row_style;

	if(lpLS_row == NULL)
	{	// We must create one
		//
		lpLS_row = new LS_item;
		this->Init_LS_item(lpLS_row,false);

		// attach to the item
		//
		lpLS_item->row_style = lpLS_row;
	}

	// Take the selected style structure
	//
	if(lpLS_row->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_row->selected_style = new LS_item;
		this->Init_LS_item( lpLS_row->selected_style );
	}
	lpLS_row = lpLS_row->selected_style;

	// no we can update the style
	//
	lpLS_row->txtColor = txtColor;

	// Redraw it
	if(redraw)	CListCtrl::Update(nRow);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetRowSelectedBgColor(int nRow,COLORREF txtBgColor,bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nRow,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_row = NULL;
	lpLS_row = lpLS_item->row_style;

	if(lpLS_row == NULL)
	{	// We must create one
		//
		lpLS_row = new LS_item;
		this->Init_LS_item(lpLS_row,false);

		// attach to the item
		//
		lpLS_item->row_style = lpLS_row;
	}

	// Take the selected style structure
	//
	if(lpLS_row->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_row->selected_style = new LS_item;
		this->Init_LS_item( lpLS_row->selected_style );
	}
	lpLS_row = lpLS_row->selected_style;

	// no we can update the style
	//
	lpLS_row->bgColor = txtBgColor;

	// Redraw it
	if(redraw)	CListCtrl::Update(nRow);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetRowSelectedFont(int nRow, CFont* pFont, bool redraw)
{	// We must retrieve the Style info structure of this item
	//
	LVITEM pItem;
	InitLVITEM(nRow,0,&pItem);

	LS_item * lpLS_item = NULL;
	lpLS_item = (LS_item*) pItem.lParam;

	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_row = NULL;
	lpLS_row = lpLS_item->row_style;

	if(lpLS_row == NULL)
	{	// We must create one
		//
		lpLS_row = new LS_item;
		this->Init_LS_item(lpLS_row,false);

		// attach to the item
		//
		lpLS_item->row_style = lpLS_row;
	}

	// Take the selected style structure
	//
	if(lpLS_row->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_row->selected_style = new LS_item;
		this->Init_LS_item( lpLS_row->selected_style );
	}
	lpLS_row = lpLS_row->selected_style;

	// if any internal font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_row);

	// no we can update the style
	//
	lpLS_row->cfont = pFont;
	lpLS_row->ifont = false;

	lpLS_row->in_use = true;

	// Redraw it
	if(redraw)	CListCtrl::Update(nRow);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetColStyle(int nCol,DWORD Style,bool redraw)
{
	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_col = NULL;
	lpLS_col = this->columns[nCol];

	if(lpLS_col == NULL)
	{	// We must create one
		//
		lpLS_col = new LS_item;
		this->Init_LS_item(lpLS_col,false);

		// attach to Array
		//
		this->columns.SetAt(nCol, lpLS_col);
	}

	// no we can update the style
	//
	lpLS_col->StyleFlag = Style;

	DWORD mask = LIS_BOLD | LIS_ITALIC | LIS_UNDERLINE| LIS_STROKE ;
	lpLS_col->in_use = (Style & mask) > 0;

	// if any font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_col);

	// Redraw it
	if(redraw)
	{	this->RedrawItems(0, this->GetItemCount() );
		this->UpdateWindow();
	}
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetColTxtColor(int nCol,COLORREF txtColor,bool redraw)
{
	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_col = NULL;
	lpLS_col = this->columns[nCol];

	if(lpLS_col == NULL)
	{	// We must create one
		//
		lpLS_col = new LS_item;
		this->Init_LS_item(lpLS_col,false);

		// attach to Array
		//
		this->columns.SetAt(nCol, lpLS_col);
	}

	// no we can update the style
	//
	lpLS_col->txtColor = txtColor;

	// Redraw it
	if(redraw)
	{	this->RedrawItems(0, this->GetItemCount() );
		this->UpdateWindow();
	}
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetColBgColor(int nCol,COLORREF txtBgColor,bool redraw)
{
	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_col = NULL;
	lpLS_col = this->columns[nCol];

	if(lpLS_col == NULL)
	{	// We must create one
		//
		lpLS_col = new LS_item;
		this->Init_LS_item(lpLS_col,false);

		// attach to Array
		//
		this->columns.SetAt(nCol, lpLS_col);
	}

	// no we can update the style
	//
	lpLS_col->bgColor = txtBgColor;

	// Redraw it
	if(redraw)
	{	this->RedrawItems(0, this->GetItemCount() );
		this->UpdateWindow();
	}
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetColFont(int nCol,CFont * pFont,bool redraw)
{
	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_col = NULL;
	lpLS_col = this->columns[nCol];

	if(lpLS_col == NULL)
	{	// We must create one
		//
		lpLS_col = new LS_item;
		this->Init_LS_item(lpLS_col,false);

		// attach to Array
		//
		this->columns.SetAt(nCol, lpLS_col);
	}

	// if any internal font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_col);

	// no we can update the style
	//
	lpLS_col->cfont = pFont;
	lpLS_col->ifont = false;

	lpLS_col->in_use = true;

	// Redraw it
	if(redraw)
	{	this->RedrawItems(0, this->GetItemCount() );
		this->UpdateWindow();
	}
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetColSelectedStyle(int nCol,DWORD Style,bool redraw)
{
	// Verify if a style for this Col already exist or not
	//
	LS_item * lpLS_col = NULL;
	lpLS_col = this->columns[nCol];

	if(lpLS_col == NULL)
	{	// We must create one
		//
		lpLS_col = new LS_item;
		this->Init_LS_item(lpLS_col,false);

		// attach to Array
		//
		this->columns.SetAt(nCol, lpLS_col);
	}

	// Take the selected style structure
	//
	if(lpLS_col->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_col->selected_style = new LS_item;
		this->Init_LS_item( lpLS_col->selected_style );
	}
	lpLS_col = lpLS_col->selected_style;

	// no we can update the style
	//
	lpLS_col->StyleFlag = Style;

	DWORD mask = LIS_BOLD | LIS_ITALIC | LIS_UNDERLINE| LIS_STROKE ;
	lpLS_col->in_use = (Style & mask) > 0;

	// if any font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_col);

	// Redraw it
	if(redraw)
	{	this->RedrawItems(0, this->GetItemCount() );
		this->UpdateWindow();
	}
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetColSelectedTxtColor(int nCol,COLORREF txtColor,bool redraw)
{
	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_col = NULL;
	lpLS_col = this->columns[nCol];

	if(lpLS_col == NULL)
	{	// We must create one
		//
		lpLS_col = new LS_item;
		this->Init_LS_item(lpLS_col,false);

		// attach to Array
		//
		this->columns.SetAt(nCol, lpLS_col);
	}

	// Take the selected style structure
	//
	if(lpLS_col->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_col->selected_style = new LS_item;
		this->Init_LS_item( lpLS_col->selected_style );
	}
	lpLS_col = lpLS_col->selected_style;

	// no we can update the style
	//
	lpLS_col->txtColor = txtColor;

	// Redraw it
	if(redraw)
	{	this->RedrawItems(0, this->GetItemCount() );
		this->UpdateWindow();
	}
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetColSelectedBgColor(int nCol,COLORREF txtBgColor,bool redraw)
{
	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_col = NULL;
	lpLS_col = this->columns[nCol];

	if(lpLS_col == NULL)
	{	// We must create one
		//
		lpLS_col = new LS_item;
		this->Init_LS_item(lpLS_col,false);

		// attach to Array
		//
		this->columns.SetAt(nCol, lpLS_col);
	}

	// Take the selected style structure
	//
	if(lpLS_col->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_col->selected_style = new LS_item;
		this->Init_LS_item( lpLS_col->selected_style );
	}
	lpLS_col = lpLS_col->selected_style;

	// no we can update the style
	//
	lpLS_col->bgColor = txtBgColor;

	// Redraw it
	if(redraw)
	{	this->RedrawItems(0, this->GetItemCount() );
		this->UpdateWindow();
	}
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetColSelectedFont(int nCol,CFont * pFont,bool redraw)
{
	// Verify if a style for this Row already exist or not
	//
	LS_item * lpLS_col = NULL;
	lpLS_col = this->columns[nCol];

	if(lpLS_col == NULL)
	{	// We must create one
		//
		lpLS_col = new LS_item;
		this->Init_LS_item(lpLS_col,false);

		// attach to Array
		//
		this->columns.SetAt(nCol, lpLS_col);
	}

	// Take the selected style structure
	//
	if(lpLS_col->selected_style == NULL)
	{	// Create a structure style
		//
		lpLS_col->selected_style = new LS_item;
		this->Init_LS_item( lpLS_col->selected_style );
	}
	lpLS_col = lpLS_col->selected_style;

	// if any internal font exist for this item then delete it
	//
	this->Free_LS_font(lpLS_col);

	// no we can update the style
	//
	lpLS_col->cfont = pFont;
	lpLS_col->ifont = false;

	lpLS_col->in_use = true;

	// Redraw it
	if(redraw)
	{	this->RedrawItems(0, this->GetItemCount() );
		this->UpdateWindow();
	}
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetHighlightTextColor(COLORREF Color)
{	
	this->m_highlighttext = Color; 
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::SetHighlightColor(COLORREF Color)
{	
	this->m_highlight = Color; 
}

// -------------------------------------------------------------------------- //

CFont* CListCtrlEx::FontForItem(int nItem,int nSubItem,LPARAM lParam,NMLVCUSTOMDRAW* pNMLVCUSTOMDRAW)
{
	// Allow to create the correct font for an item of the CListCtrl
	//

	// Cast the lParam into an LS_item object
	LS_item * lpLS_root = NULL;
	lpLS_root = (LS_item*)lParam;

	LS_item * lpLS_item = NULL;
	LS_item * lpLS_row = NULL;
	LS_item * lpLS_col = NULL;

	// Set a mask value
	//
	DWORD mask = LIS_BOLD | LIS_ITALIC | LIS_UNDERLINE| LIS_STROKE ;

	// Take Corects Structure style that we need
	//
	lpLS_item = lpLS_root;
	lpLS_row = lpLS_root->row_style;
	lpLS_col = this->columns[nSubItem];

	if(nSubItem > 0)
		 lpLS_item = lpLS_root->subitems[nSubItem - 1];

	if(m_item_selected)
	{
		if(lpLS_item)
			lpLS_item = lpLS_item->selected_style;

		if(lpLS_row)
			lpLS_row = lpLS_row->selected_style;

		if(lpLS_col)
			lpLS_col = lpLS_col->selected_style;
	}

	// Set if style of columns & rows are enable or not
	//
	bool enable_col_style = true;
	bool enable_row_style = true;

	if(lpLS_item)
	{	enable_col_style = (lpLS_item->StyleFlag & LIS_NO_COL_STYLE) == 0;
		enable_row_style = (lpLS_item->StyleFlag & LIS_NO_ROW_STYLE) == 0;
	}

	if(lpLS_row && enable_col_style && enable_row_style)
		enable_col_style = (lpLS_row->StyleFlag & LIS_NO_COL_STYLE) == 0 ;

	if(lpLS_col && enable_row_style && enable_col_style)
		enable_row_style = (lpLS_col->StyleFlag & LIS_NO_ROW_STYLE) == 0;

	// ---------------------------
	// -- Text Color Management --
	// ---------------------------
	{	// Default
		//
		bool set = false;

		if(m_item_selected)
			pNMLVCUSTOMDRAW->clrText = m_highlighttext;
		else
			pNMLVCUSTOMDRAW->clrText = CListCtrl::GetTextColor();

		// Try ITEM color
		//
		if(lpLS_item)
		{	if(lpLS_item->StyleFlag & LIS_TXTCOLOR)
			{	pNMLVCUSTOMDRAW->clrText = lpLS_item->txtColor;
				set = true;
			}
		}

		// Try LINE color
		//
		if(lpLS_row && !set && enable_row_style)
		{	if(lpLS_row->StyleFlag & LIS_TXTCOLOR)
			{	pNMLVCUSTOMDRAW->clrText = lpLS_row->txtColor;
				set = true;
			}
		}

		// Try COLUMN color
		//
		if(lpLS_col && !set && enable_col_style)
		{	if(lpLS_col->StyleFlag & LIS_TXTCOLOR)
				pNMLVCUSTOMDRAW->clrText = lpLS_col->txtColor;
		}
	}

	// ---------------------------------
	// -- BackGround Color Management --
	// ---------------------------------
	{	// Default
		//
		bool set = false;
		if(!m_item_selected)
			pNMLVCUSTOMDRAW->clrTextBk = CListCtrl::GetTextBkColor();
		else
			pNMLVCUSTOMDRAW->clrTextBk = m_highlight;

		// Try ITEM color
		//
		if(lpLS_item)
		{	if(lpLS_item->StyleFlag & LIS_BGCOLOR)
			{	pNMLVCUSTOMDRAW->clrTextBk = lpLS_item->bgColor;
				set = true;
			}
		}

		// Try LINE color
		//
	   	if(lpLS_row && !set)
		{	if(lpLS_row->StyleFlag & LIS_BGCOLOR && enable_row_style)
			{	pNMLVCUSTOMDRAW->clrTextBk = lpLS_row->bgColor;
				set = true;
			}
		}

		// Try COLUMN color
		//
		if(lpLS_col	&& !set)
		{	if(lpLS_col->StyleFlag & LIS_BGCOLOR && enable_col_style)
				pNMLVCUSTOMDRAW->clrTextBk = lpLS_col->bgColor;
		}
	}

	// Adjusting LS_Structure item for take orignal style if no style is defined for the selected case
	//
	if(m_item_selected)
	{	bool back;

		back = true;
		if(lpLS_item)
			back = !lpLS_item->in_use;

		if(back)
		{	lpLS_item = lpLS_root;
			if(nSubItem > 0)
				lpLS_item = lpLS_root->subitems[nSubItem - 1];
		}

		back = true;
		if(lpLS_row)
			back = !lpLS_row->in_use;

		if(back)
			lpLS_row = lpLS_root->row_style;

		back = true;
		if(lpLS_col)
			back = !lpLS_col->in_use;

		if(back)
			lpLS_col = this->columns[nSubItem];

		// Recompute if style of columns & rows are enable or not
		//
		enable_col_style = true;
		enable_row_style = true;

		if(lpLS_item)
		{	enable_col_style = (lpLS_item->StyleFlag & LIS_NO_COL_STYLE) == 0;
			enable_row_style = (lpLS_item->StyleFlag & LIS_NO_ROW_STYLE) == 0;
		}

		if(lpLS_row && enable_col_style && enable_row_style)
			enable_col_style = (lpLS_row->StyleFlag & LIS_NO_COL_STYLE) == 0 ;

		if(lpLS_col && enable_row_style && enable_col_style)
			enable_row_style = (lpLS_col->StyleFlag & LIS_NO_ROW_STYLE) == 0;
	}

	// ---------------------------
	// -- Fonts Initialisations --
	// ---------------------------
	CFont * pCFontItem = NULL;			// Item Font
	CFont * pCFontRow = NULL;			// Row Font
	CFont * pCFontCol = NULL;			// Column Font

	// -- If a font is created, then the existing merged font can't be available
	// -- This flag prevent this use if it's not permitted
	// --
	bool must_erase_merged_font = false;


	// ---------------
	// -- Item Font --
	// ---------------
	if(lpLS_item)
	{
		if(lpLS_item->cfont != NULL)
			pCFontItem = lpLS_item->cfont;
		else
		{	// Create it if necessary
			//
			if((lpLS_item->StyleFlag & mask) != 0)
			{	pCFontItem = CreateFont(lpLS_item->StyleFlag);
				lpLS_item->cfont = pCFontItem;
				lpLS_item->ifont = true;
				must_erase_merged_font = true;
			}
		}
	}

	// --------------
	// -- Row Font --
	// --------------
	if(lpLS_row && enable_row_style)
	{
		if(lpLS_row->cfont != NULL)
			pCFontRow = lpLS_row->cfont;
		else
		{	// Create it if necessary
			//
			if((lpLS_row->StyleFlag & mask) != 0)
			{	pCFontRow = CreateFont(lpLS_row->StyleFlag);
				lpLS_row->cfont = pCFontRow;
				lpLS_row->ifont = true;
				must_erase_merged_font = true;
			}
		}
	}

	// -----------------
	// -- Column Font --
	// -----------------
	if(lpLS_col && enable_col_style)
	{
		if(lpLS_col->cfont != NULL)
			pCFontCol =	lpLS_col->cfont;
		else
		{	// Create it if necessary
			//
			if((lpLS_col->StyleFlag & mask) != 0)
			{	pCFontCol = CreateFont(lpLS_col->StyleFlag);
				lpLS_col->cfont = pCFontCol;
				lpLS_col->ifont = true;
				must_erase_merged_font = true;
			}
		}
	}

	// -- See if a font is define or not (if not, use default font)
	// --
	if(pCFontItem == NULL && pCFontRow == NULL && pCFontCol == NULL)
		return NULL;

	// Delete the merged font if needed
	//
	if(lpLS_item)
	{	if(lpLS_item->merged_font != NULL && must_erase_merged_font)
			this->Free_LS_font(lpLS_item,FONT_MERGED);
	}

	// -- At least One Font is required, Find it if only one is required
	// --
	CFont * pFont = this->SelectFont(pCFontItem,pCFontRow,pCFontCol);

	// -- If a Font can be selected, then we can return it !! no melting font was needed
	// --
	if(pFont != NULL)
		return pFont;

	// -- Test if a merged font exist, if it's the case, we can directly return it
	// --
	if(lpLS_item)
	{	if(lpLS_item->merged_font != NULL)
			return lpLS_item->merged_font;
	}
	else
		return NULL;

	// -- So, if we can't select a Font, it's because we must created a melted Font from all fonts
	// --
	CFont * pFontMelted = this->MergeFont(pCFontItem,pCFontRow,pCFontCol);

	// -- Attach the melted font
	// --
	lpLS_item->merged_font = pFontMelted;

	// -- Return
	// --
	return pFontMelted;
}

// -------------------------------------------------------------------------- //

CFont* CListCtrlEx::SelectFont(CFont * pCFont1,CFont * pCFont2,CFont * pCFont3)
{	if(pCFont1)
	{	if(!pCFont2 && ! pCFont3)
			return pCFont1;
		else
			return NULL;
	}

	if(pCFont2)
	{	if(!pCFont3)
			return pCFont2;
		else
			return NULL;
	}

	return (pCFont3);
}

// -------------------------------------------------------------------------- //

CFont* CListCtrlEx::MergeFont(CFont * pCFont1,CFont * pCFont2,CFont * pCFont3)
{	LOGFONT merge_logfont;

	// -- Set Base Font Informations
	// --
	merge_logfont = this->m_Default_LOGFONT;

	if(pCFont3)
	{	LOGFONT logfont;
		pCFont3->GetLogFont(&logfont);
		this->MergeLogFont(&logfont,&merge_logfont);
	}

	if(pCFont2)
	{	LOGFONT logfont;
		pCFont2->GetLogFont(&logfont);
		this->MergeLogFont(&logfont,&merge_logfont);
	}

	if(pCFont1)
	{	LOGFONT logfont;
		pCFont1->GetLogFont(&logfont);
		this->MergeLogFont(&logfont,&merge_logfont);
	}

	// -- So now we have the logfont corresponding at our merged font
	// --

	CFont * pCFontMerged;
	pCFontMerged = new(CFont);
	pCFontMerged->CreateFontIndirect(&merge_logfont);

	return pCFontMerged;
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::MergeLogFont(LOGFONT * font,LOGFONT * dest)
{
	/*	typedef struct tagLOGFONT {
  		LONG lfHeight;
  		LONG lfWidth;
  		LONG lfEscapement;
  		LONG lfOrientation;
  		LONG lfWeight;
  		BYTE lfItalic;
  		BYTE lfUnderline;
  		BYTE lfStrikeOut;
  		BYTE lfCharSet;
  		BYTE lfOutPrecision;
  		BYTE lfClipPrecision;
  		BYTE lfQuality;
  		BYTE lfPitchAndFamily;
  		TCHAR lfFaceName[LF_FACESIZE];
		} LOGFONT, *PLOGFONT;
	*/

	if(font->lfHeight != this->m_Default_LOGFONT.lfHeight)
		dest->lfHeight = font->lfHeight;

	if(font->lfWidth != this->m_Default_LOGFONT.lfWidth)
		dest->lfWidth = font->lfWidth;

	if(font->lfEscapement != this->m_Default_LOGFONT.lfEscapement)
		dest->lfEscapement = font->lfEscapement;

	if(font->lfOrientation != this->m_Default_LOGFONT.lfOrientation)
		dest->lfOrientation = font->lfOrientation;

	if(font->lfWeight != this->m_Default_LOGFONT.lfWeight)
		dest->lfWeight = font->lfWeight;

	if(font->lfItalic != this->m_Default_LOGFONT.lfItalic)
		dest->lfItalic = font->lfItalic;

	if(font->lfUnderline != this->m_Default_LOGFONT.lfUnderline)
		dest->lfUnderline = font->lfUnderline;

	if(font->lfStrikeOut != this->m_Default_LOGFONT.lfStrikeOut)
		dest->lfStrikeOut = font->lfStrikeOut;

	if(font->lfCharSet != this->m_Default_LOGFONT.lfCharSet)
		dest->lfCharSet = font->lfCharSet;

	if(font->lfOutPrecision != this->m_Default_LOGFONT.lfOutPrecision)
		dest->lfOutPrecision = font->lfOutPrecision;

	if(font->lfClipPrecision != this->m_Default_LOGFONT.lfClipPrecision)
		dest->lfClipPrecision = font->lfClipPrecision;

	if(font->lfQuality != this->m_Default_LOGFONT.lfQuality)
		dest->lfQuality = font->lfQuality;

	if(font->lfPitchAndFamily != this->m_Default_LOGFONT.lfPitchAndFamily)
		dest->lfPitchAndFamily = font->lfPitchAndFamily;

	if(strcmp(font->lfFaceName,this->m_Default_LOGFONT.lfFaceName) != 0)
		strcpy(dest->lfFaceName,font->lfFaceName);

}

// -------------------------------------------------------------------------- //

CFont* CListCtrlEx::CreateFont(DWORD StyleFlag)
{
	// Retrieve informations about the current font of this item
	//
	LOGFONT logfont = this->m_Default_LOGFONT;					// Information about our new font

	// Update the logfont structure for prepare our new font
	//
    /*	typedef struct tagLOGFONT {
  		LONG lfHeight;
  		LONG lfWidth;
  		LONG lfEscapement;
  		LONG lfOrientation;
  		LONG lfWeight;
  		BYTE lfItalic;
  		BYTE lfUnderline;
  		BYTE lfStrikeOut;
  		BYTE lfCharSet;
  		BYTE lfOutPrecision;
  		BYTE lfClipPrecision;
  		BYTE lfQuality;
  		BYTE lfPitchAndFamily;
  		TCHAR lfFaceName[LF_FACESIZE];
		} LOGFONT, *PLOGFONT;
	*/

	if(StyleFlag & LIS_BOLD)
		logfont.lfWeight = FW_BOLD;
	else
		logfont.lfWeight = FW_NORMAL;

	logfont.lfItalic = (StyleFlag & LIS_ITALIC) > 0;
	logfont.lfUnderline = (StyleFlag & LIS_UNDERLINE) > 0;
	logfont.lfStrikeOut = (StyleFlag & LIS_STROKE) > 0;

	CFont * lpCFont;
	lpCFont = new(CFont);

	// Create the font
	//
	lpCFont->CreateFontIndirect(&logfont);

	return (lpCFont);
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::Init_LS_item(LS_item * lpLS_item,bool allow_subitems)
{	// lParam Init
	//
	lpLS_item->lParam = 0;
	lpLS_item->mParam = false;

	// Set Default Style
	//
	lpLS_item->StyleFlag = 0;
	lpLS_item->in_use = false;

	lpLS_item->txtColor = 0;
	lpLS_item->bgColor = 0;
	lpLS_item->cfont = NULL;
	lpLS_item->ifont = false;
	lpLS_item->merged_font = NULL;
	lpLS_item->row_style = NULL;
	lpLS_item->selected_style = NULL;

	// Init the Array for hold SubItems Style
	//
	if(allow_subitems)
	{	int nSubItems = this->GetHeaderCtrl()->GetItemCount();
		if(nSubItems > 0) nSubItems--;
		lpLS_item->subitems.SetSize( nSubItems );
	}
	else
		lpLS_item->subitems.SetSize(0);

	lpLS_item->subitems.RemoveAll();
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::Free_LS_font(LS_item* lpLS_item,DWORD mask)
{
	// Free his font object if needed (if it's an internal font)
	//
	if(lpLS_item->cfont != NULL && lpLS_item->ifont && (mask & FONT_NORMAL))
	{	delete lpLS_item->cfont;
		lpLS_item->cfont = NULL;
		lpLS_item->ifont = false;
	}

	// Free Merged Font
	//
	if(lpLS_item->merged_font != NULL && (mask & FONT_MERGED))
	{	delete lpLS_item->merged_font;
		lpLS_item->merged_font = NULL;
	}

}

// -------------------------------------------------------------------------- //

void CListCtrlEx::Free_LS_item(LS_item* lpLS_item)
{
	if(lpLS_item == NULL)
		return;

	if(lpLS_item->subitems.GetSize() > 0)
	{	// We must delete the subitems structure also
		//
		INT_PTR nSubItems = lpLS_item->subitems.GetSize();

		for(int nSubItem = 0; nSubItem < nSubItems; nSubItem++)
		{
			this->Free_LS_item(lpLS_item->subitems[nSubItem]);
		}
	}

	// We can reset the list of subitems style
	//
	lpLS_item->subitems.RemoveAll();
	lpLS_item->subitems.SetSize(0);


	// Free CFont objects
	//
	this->Free_LS_font(lpLS_item);

	// Delete the Row Style if needed
	//
	if(lpLS_item->row_style != NULL)
		this->Free_LS_item(lpLS_item->row_style);

	// Delete the Selected Style if needed
	//
	if(lpLS_item->selected_style != NULL)
		this->Free_LS_item(lpLS_item->selected_style);

	// Delete the item structure
	//
	delete lpLS_item;
	lpLS_item = NULL;
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::InitLVITEM(int nItem,int nSubItem,LVITEM * pItem)
{ /*
  typedef struct _LV_ITEM {
    UINT   mask;         // see below
    int    iItem;        // see below
    int    iSubItem;     // see below
    UINT   state;        // see below
    UINT   stateMask;    // see below
    LPSTR  pszText;      // see below
    int    cchTextMax;   // see below
    int    iImage;       // see below
    LPARAM lParam;       // 32-bit value to associate with item
   } LV_ITEM;
  */
	pItem->mask = LVIF_PARAM;
	pItem->iItem = nItem;
	pItem->iSubItem = nSubItem;
	pItem->state = NULL;
	pItem->stateMask = NULL;
	pItem->pszText = NULL;
	pItem->cchTextMax = NULL;
	pItem->iImage = NULL;
	pItem->lParam = NULL;
	CListCtrl::GetItem(pItem);
}

// -------------------------------------------------------------------------- //
// Handlers for item formatting
// -------------------------------------------------------------------------- //

void CListCtrlEx::OnDestroy()
{	// Must free all internal objects for prevent memory leaks
	//
	this->DeleteAllItems();
	CListCtrl::OnDestroy();
}

// -------------------------------------------------------------------------- //

void CListCtrlEx::OnCustomdraw(NMHDR* pNMHDR, LRESULT* pResult)
{
    // first, lets extract data from
    // the message for ease of use later
    NMLVCUSTOMDRAW* pNMLVCUSTOMDRAW = (NMLVCUSTOMDRAW*)pNMHDR;

    // we'll copy the device context into hdc
    // but wont convert it to a pDC* until (and if)
    // we need it as this requires a bit of work
    // internally for MFC to create temporary CDC
    // objects
    HDC hdc = pNMLVCUSTOMDRAW->nmcd.hdc;
    CDC* pDC = NULL;

    // here is the item info
    // note that we don't get the subitem
    // number here, as this may not be
    // valid data except when we are
    // handling a sub item notification
    // so we'll do that separately in
    // the appropriate case statements
    // below.
    int nItem = pNMLVCUSTOMDRAW->nmcd.dwItemSpec;
    UINT nState = pNMLVCUSTOMDRAW->nmcd.uItemState;
    LPARAM lParam = pNMLVCUSTOMDRAW->nmcd.lItemlParam;

    // next we set up flags that will control
    // the return value for *pResult
    bool bNotifyPostPaint = false;
    bool bNotifyItemDraw = false;
    bool bNotifySubItemDraw = false;
    bool bSkipDefault = false;
    bool bNewFont = false;

    // what we do next depends on the
    // drawing stage we are processing
    switch (pNMLVCUSTOMDRAW->nmcd.dwDrawStage) {
    case CDDS_PREPAINT:
        {
            // PrePaint
            m_pOldItemFont = NULL;
            m_pOldSubItemFont = NULL;
            bNotifyPostPaint = false;
            bNotifyItemDraw = true;
        }
        break;
    case CDDS_ITEMPREPAINT:
        {
            // Item PrePaint
			//
			// set up a different font to use, if any
			if (! pDC) pDC = CDC::FromHandle(hdc);

			 m_pOldItemFont = NULL;
            bNotifyPostPaint = false;
            bNotifySubItemDraw = true;

			// Store the first time, information about default font
			//
			if(m_Default_pCFont == NULL)
			{	m_Default_pCFont = pDC->GetCurrentFont();
				m_Default_pCFont->GetLogFont(&m_Default_LOGFONT);
			}

            m_item_selected = false;
            m_item_selected = this->GetItemState(nItem, LVIS_SELECTED) != 0;
			if(m_item_selected)
			{	this->SetItemState(nItem,NULL,LVIS_SELECTED);
				bNotifyPostPaint = true;
			}

            CFont* pNewFont = FontForItem(nItem,0,lParam,pNMLVCUSTOMDRAW);
            if (pNewFont) {
                m_pOldItemFont = pDC->SelectObject(pNewFont);
                bNotifyPostPaint = true;    // need to restore old font
				bNewFont = true;
            }
        }
        break;
    case CDDS_ITEMPREPAINT|CDDS_SUBITEM:
        {	// Sub Item PrePaint
            // set sub item number (data will be valid now)
            int nSubItem = pNMLVCUSTOMDRAW->iSubItem;
            m_pOldSubItemFont = NULL;
            bNotifyPostPaint = false;

            // set up a different font to use, if any
			if (! pDC) pDC = CDC::FromHandle(hdc);
            CFont* pNewFont = FontForItem(nItem,nSubItem,lParam,pNMLVCUSTOMDRAW);

			if (pNewFont) {
                m_pOldSubItemFont = pDC->SelectObject(pNewFont);
                bNotifyPostPaint = true;    // need to restore old font
				bNewFont = true;
            }
			else if(m_pOldItemFont)
			{	m_pOldSubItemFont = pDC->SelectObject(m_pOldItemFont);
                bNotifyPostPaint = true;    // need to restore old font
				bNewFont = true;
			}
        }
        break;
    case CDDS_ITEMPOSTPAINT|CDDS_SUBITEM:
        {
            // Sub Item PostPaint
            // set sub item number (data will be valid now)
            int nSubItem = pNMLVCUSTOMDRAW->iSubItem;
            // restore old font if any
			if (! pDC) pDC = CDC::FromHandle(hdc);
            if (m_pOldSubItemFont) {
                pDC->SelectObject(m_pOldSubItemFont);
                m_pOldSubItemFont = NULL;
            }
		 }
        break;
    case CDDS_ITEMPOSTPAINT:
        {
            // Item PostPaint
            // restore old font if any
			if (! pDC) pDC = CDC::FromHandle(hdc);
            if (m_pOldItemFont) {
                pDC->SelectObject(m_pOldItemFont);
                m_pOldItemFont = NULL;
            }

            if(m_item_selected)
			{	m_item_selected = false;
				this->SetItemState(nItem,LVIS_SELECTED,LVIS_SELECTED);
			}
		}
        break;
    }

    ASSERT(CDRF_DODEFAULT==0);
    *pResult = 0;
    if (bNotifyPostPaint) {
        *pResult |= CDRF_NOTIFYPOSTPAINT;
    }
    if (bNotifyItemDraw) {
        *pResult |= CDRF_NOTIFYITEMDRAW;
    }
    if (bNotifySubItemDraw) {
        *pResult |= CDRF_NOTIFYSUBITEMDRAW;
    }
    if (bNewFont) {
        *pResult |= CDRF_NEWFONT;
    }
    if (bSkipDefault) {
        *pResult |= CDRF_SKIPDEFAULT;
    }
    if (*pResult == 0) {
        // redundant as CDRF_DODEFAULT==0 anyway
        // but shouldn't depend on this in our code
        *pResult = CDRF_DODEFAULT;
    }
}

// -------------------------------------------------------------------------- //

BOOL CListCtrlEx::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_SPACE)
		{
			if(m_EditingRow == -1)
			{
				int selItem = GetNextItem(-1, LVNI_SELECTED);
				// Add check for LVS_EDITLABELS
				if(GetWindowLong(m_hWnd, GWL_STYLE) & LVS_EDITLABELS)
				{
					switch(m_EditType)
					{
					case ET_EDIT:
						{
							m_EditingRow = selItem;
							ShowInPlaceEdit(selItem, 1);
						}
						break;
					case ET_COMBO:
						{
							m_EditingRow = selItem;
							ShowInPlaceComboBox(selItem, 1);
						}
						break;
					case ET_COLORCOMBO:
						{
							m_EditingRow = selItem;
							ShowInPlaceComboBox(selItem, 1);
						}
						break;
					default:
						SetItemState(selItem, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
					}
				}
			}
		}
	}

	return CListCtrl::PreTranslateMessage(pMsg);
}
