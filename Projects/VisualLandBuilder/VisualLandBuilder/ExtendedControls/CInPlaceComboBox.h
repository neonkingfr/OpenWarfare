// CInPlaceComboBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInPlaceComboBox window

#pragma once

#include <string>
#include <vector>

using std::string;
using std::vector;

class CInPlaceComboBox : public CComboBox
{
// Construction
public:
	CInPlaceComboBox(int iItem, int iSubItem, vector<string>& plstItems, string selection);

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

// Destructor
public:
	virtual ~CInPlaceComboBox();

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnNcDestroy();
	afx_msg void OnCloseUp();

	DECLARE_MESSAGE_MAP()

private:
	int            m_iItem;
	int            m_iSubItem;
	vector<string> m_lstItems;
	string         m_Selection;
	BOOL           m_bESC;				// To indicate whether ESC key was pressed
};
