#pragma once

#include <string>
#include <vector>

using std::string;
using std::vector;

class LBDbValuesFunctor
{
	mutable vector<string> m_ValuesList;
	mutable vector<string> m_FieldsList;

public:
	LBDbValuesFunctor();
	bool operator ()(unsigned int shapeId, RStringI paramName, RStringI paramValue) const;
	vector<string>& GetValuesList() const;
	string GetValue(int index, string field);
};