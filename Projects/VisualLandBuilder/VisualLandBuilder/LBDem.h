//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include ".\LBGlobalVariables.h"
#include ".\LBParameterList.h"
#include ".\LBError.h"

#include "..\..\LandBuilder\LandBuilder2\MapInput.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

// ****************** //
// Enum for dem types //
// ****************** //
typedef enum
{
	LBDT_START_ENUM,
	LBDT_DTED2,
	LBDT_USGSDEM,
	LBDT_ARCINFOASCII,
	LBDT_XYZ,
	LBDT_END_ENUM
} LBDemTypes;

//-----------------------------------------------------------------------------

// ****************************** //
// Array for dem types as strings //
// ****************************** //
const string LBDemTypesAsString[] =
{
	"Not Defined",
	"DTED2",
	"USGSDEM",
	"ARCINFOASCII",
	"XYZ",
	"Not Defined"
};

//-----------------------------------------------------------------------------

// ************************** //
// Array for dem file filters //
// ************************** //
const string LBDemFileFilters[] =
{
	"All files (*.*)|*.*||",
	"DTED2 files (*.dt2)|*.dt2||",
	"Usgs Dem files (*.dem)|*.dem||",
	"Arc Info Ascii files (*.asc)|*.asc|Arc Info Ascii files (*.grd)|*.grd||",
	"XYZ files (*.xyz)|*.xyz||",
	"All files (*.*)|*.*||"
};

//-----------------------------------------------------------------------------

// ***************************** //
// Array for dem file extensions //
// ***************************** //
const string LBDemFileExt[] =
{
	"",
	"dt2",
	"dem",
	"",
	"xyz",
	""
};

//-----------------------------------------------------------------------------

class LBDem : public CObject
{
	DECLARE_SERIAL( LBDem )

	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the version
	int m_Version;
	// the dem type
	LBDemTypes m_Type;
	// the dem file name
	string m_FileName;
	// the dem list of parameters
	LBParameterList m_ParametersList;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBDem();

	// Constructs this module with the given parameters
	LBDem( LBDemTypes type, const string& fileName );
	LBDem( LBDemTypes type, const string& fileName, const LBParameterList& parametersList );
	LBDem( LBDem* other );

	// copy constructor
	LBDem( const LBDem& other );

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~LBDem();

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBDem& operator = ( const LBDem& other );

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //
	
	// GetType()
	// returns the type of this dem
	LBDemTypes GetType() const;

	// GetFileName()
	// returns the file name of this dem
	string GetFileName() const;

	// GetTypeAsString()
	// returns a string describing the type of this dem
	string GetTypeAsString() const;

	// GetParameterList()
	// returns the parameter list of this dem
	LBParameterList& GetParametersList();

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //
	
	// SetType()
	// sets the type of this dem with the given one
	void SetType( LBDemTypes newType );

	// SetFileName()
	// sets the file name of this dem with the given one
	void SetFileName( const string& newFileName );

	// SetFileName()
	// sets the file name of this dem with the given one
	void SetFileName( const CString& newFileName );

	// SetParameterList()
	// sets the parameter list of this dem with the given one
	void SetParameterList( const LBParameterList& parametersList );

	// ************************************************************** //
	// Helper functions                                               //
	// ************************************************************** //

	// SetParameterList()
	// sets the default parameter list
	void SetDefaultParameterList();

	// ************************************************************** //
	// Interface                                                      //
	// ************************************************************** //
	
	// Validate()
	// returns the result from dem validation
	// LBET_NOERROR if the dem can be exported to LandBuilder
	LBErrorTypes Validate();

	// AdvancedRandomPlacerSlopeMapInput()
	// returns this dem as input for the AdvancedRandomPlacerSlope module
	LandBuildInt::MapInput AdvancedRandomPlacerSlopeMapInput();

	// BiasedRandomPlacerSlopeMapInput()
	// returns this dem as input for the BiasedRandomPlacerSlope module
	LandBuildInt::MapInput BiasedRandomPlacerSlopeMapInput();

	// SimpleRandomPlacerSlopeMapInput()
	// returns this dem as input for the SimpleRandomPlacerSlope module
	LandBuildInt::MapInput SimpleRandomPlacerSlopeMapInput();

	// ************************************************************** //
	// Serialization                                                  //
	// ************************************************************** //
	virtual void Serialize( CArchive& ar );
};

//-----------------------------------------------------------------------------
