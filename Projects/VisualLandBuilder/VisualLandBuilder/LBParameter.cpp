//-----------------------------------------------------------------------------

#include "stdafx.h"

#include ".\LBParameter.h"
#include ".\LBGlobalVariables.h"

//-----------------------------------------------------------------------------

IMPLEMENT_SERIAL( LBParameter, CObject, 1 )

//-----------------------------------------------------------------------------

LBParameter::LBParameter() 
: CObject()
, m_version( VERSION_NUMBER )
{
}

// -------------------------------------------------------------------------- //

LBParameter::LBParameter( const string& name, const string& value, const string& description,
                          LBParameterTypes type, bool hasDefault, bool isMovable, bool isExtension ) 
: CObject()
, m_version( VERSION_NUMBER )
, m_name( name )
, m_value( value )
, m_description( description )
, m_type( type )
, m_hasDefault( hasDefault )
, m_isMovable( isMovable )
, m_isExtension( isExtension )
{
}

//-----------------------------------------------------------------------------

LBParameter::LBParameter( LBParameter* other )
: m_version( other->m_version )
, m_name( other->m_name )
, m_value( other->m_value )
, m_description( other->m_description )
, m_type( other->m_type )
, m_hasDefault( other->m_hasDefault )
, m_isMovable( other->m_isMovable )
, m_isExtension( other->m_isExtension )
{
}

//-----------------------------------------------------------------------------

LBParameter::LBParameter( const LBParameter& other ) 
: m_version( other.m_version )
, m_name( other.m_name )
, m_value( other.m_value )
, m_description( other.m_description )
, m_type( other.m_type )
, m_hasDefault( other.m_hasDefault )
, m_isMovable( other.m_isMovable )
, m_isExtension( other.m_isExtension )
{
}

//-----------------------------------------------------------------------------

LBParameter& LBParameter::operator = ( const LBParameter& other )
{
	m_version     = other.m_version;
	m_name        = other.m_name;
	m_value       = other.m_value;
	m_description = other.m_description;
	m_type        = other.m_type;
	m_hasDefault  = other.m_hasDefault;
	m_isMovable   = other.m_isMovable;
	m_isExtension = other.m_isExtension;

	return (*this);
}

//-----------------------------------------------------------------------------
	
string LBParameter::GetName() const 
{ 
	return (m_name); 
}

//-----------------------------------------------------------------------------

string LBParameter::GetValue() const 
{ 
	return (m_value); 
}

//-----------------------------------------------------------------------------

string LBParameter::GetDescription() const 
{ 
	return (m_description); 
}

//-----------------------------------------------------------------------------

LBParameterTypes LBParameter::GetType() const 
{ 
	return (m_type); 
}

//-----------------------------------------------------------------------------

bool LBParameter::GetHasDefault() const 
{ 
	return (m_hasDefault); 
}

//-----------------------------------------------------------------------------

bool LBParameter::IsMovable() const
{
	return (m_isMovable); 
}

//-----------------------------------------------------------------------------

bool LBParameter::IsExtension() const
{
	return (m_isExtension); 
}

//-----------------------------------------------------------------------------

void LBParameter::SetName( const string& name ) 
{ 
	m_name = name; 
}

//-----------------------------------------------------------------------------

void LBParameter::SetName( const CString& name ) 
{ 
	m_name = name; 
}

//-----------------------------------------------------------------------------

void LBParameter::SetValue( const string& value ) 
{ 
	m_value = value; 
}

//-----------------------------------------------------------------------------

void LBParameter::SetValue( const CString& value ) 
{ 
	m_value = value; 
}

//-----------------------------------------------------------------------------

void LBParameter::SetDescription( const string& description ) 
{ 
	m_description = description; 
}

//-----------------------------------------------------------------------------

void LBParameter::SetDescription( const CString& description ) 
{ 
	m_description = description; 
}

//-----------------------------------------------------------------------------

void LBParameter::SetType( LBParameterTypes type ) 
{ 
	m_type = type; 
}

//-----------------------------------------------------------------------------

void LBParameter::SetHasDefault( bool hasDefault ) 
{ 
	m_hasDefault = hasDefault; 
}

//-----------------------------------------------------------------------------

void LBParameter::SetIsMovable( bool isMovable )
{
	m_isMovable = isMovable; 
}

//-----------------------------------------------------------------------------

void LBParameter::SetIsExtension( bool isExtension )
{
	m_isExtension = isExtension; 
}

//-----------------------------------------------------------------------------

void LBParameter::ClearValue()
{
	m_value = "";
}

//-----------------------------------------------------------------------------

void LBParameter::Serialize( CArchive& ar )
{
	CString name;
	CString value;
	CString description;
	UINT    type;

	CObject::Serialize( ar );

	if ( ar.IsStoring() )
	{
		name        = CString( m_name.c_str() );
		value       = CString( m_value.c_str() );
		description = CString( m_description.c_str() );
		type  = UINT( m_type );
		ar << m_version;
		ar << name;
		ar << value;
		ar << description;
		ar << type;
		ar << m_hasDefault;
		ar << m_isMovable;
		ar << m_isExtension;
	}
	else
	{
		ar >> m_version;
		ar >> name;
		ar >> value;
		ar >> description;
		ar >> type;
		ar >> m_hasDefault;
		ar >> m_isMovable;
		ar >> m_isExtension;
		m_name        = name;
		m_value       = value;
		m_description = description;
		m_type        = static_cast< LBParameterTypes >( type );
	}
}

//-----------------------------------------------------------------------------
