#pragma once

#include ".\LBRectangle.h"

#include "..\..\LandBuilder\Shapes\IShape.h"
#include "..\..\LandBuilder\Shapes\ShapeList.h"

class CDrawShapesStatic : public CStatic
{
private:
	// the list of shapes to draw
	Shapes::ShapeList* m_ShapeList;

private:
	// the rectangle used to draw
	LBRectangle m_WorldExtents;
	LBRectangle m_ViewExtents;

	// variables to scale drawing
	DrawParams m_DrawParams;

public:
	CDrawShapesStatic();

	DECLARE_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //
public:
	// SetShapeList()
	// sets the shape list to be drawn
	void SetShapeList(Shapes::ShapeList* shapeList);

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	// GetShapesExtents()
	// calculates the extents of the shapefile and stores the result
	// in the member variables
	void GetShapesExtents();

// -------------------------------------------------------------------------- //
// EVENT HANDLERS                                                             //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnPaint();

	void DrawShapes(CPaintDC* dc, CRect& rect);
	void DrawPoints(CPaintDC* dc, CRect& rect, Shapes::IShape* shape);
	void DrawPolygon(CPaintDC* dc, CRect& rect, Shapes::IShape* shape);
	void DrawPolyline(CPaintDC* dc, CRect& rect, Shapes::IShape* shape);
	void DrawSegment(CPaintDC* dc, CRect& rect, const Shapes::DVertex& v1, const Shapes::DVertex& v2);

	void SetWorldExtents();
	void SetViewExtents();

};