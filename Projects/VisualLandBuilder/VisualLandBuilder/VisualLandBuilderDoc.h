//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\LBProject.h" 

#include ".\CAddEditModuleDlg.h"
#include ".\COptionsDlg.h"
#include ".\CTabDirectoriesDlg.h"
#include ".\CEditVisitorMapDlg.h"
#include ".\CRunLandBuilderDlg.h"
#include ".\CRunLandBuilder2Dlg.h"

#include ".\CPreviewDlg.h"

//-----------------------------------------------------------------------------

class CVisualLandBuilderDoc : public CDocument
{
protected: // create from serialization only
	CVisualLandBuilderDoc();
	DECLARE_DYNCREATE( CVisualLandBuilderDoc )

public:
	virtual BOOL OnNewDocument();

	virtual void Serialize( CArchive& ar );

public:
	virtual ~CVisualLandBuilderDoc();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump( CDumpContext& dc ) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

private:
	// the index of the current selected module
	int m_curSelModuleIndex;
	// the project
	LBProject m_project;

	// the dialogs
//	CAddEditModuleDlg   m_addEditModuleDlg;
	CTabDirectoriesDlg  m_directoriesDlg;
	COptionsDlg         m_optionsDlg;
	CEditVisitorMapDlg  m_editVisitorMapDlg;
	CRunLandBuilder2Dlg m_runLandBuilder2Dlg;

	// the preview dialog
	CPreviewDlg m_previewDlg;

public:
	virtual void DeleteContents();

private:
	// SetDefaultVisitorMap()
	// sets the default visitor map data
	void SetDefaultVisitorMap();
	// SetDefaultDirectoryList()
	// sets the default directory list
	void SetDefaultDirectoryList();
	// RemoveModule()
	// removes the module with the given index from the module list
	void RemoveModule( int index );
	// RemoveAllModules()
	// removes all the modules from the module list
	void RemoveAllModules();
	// SetModuleList()
	// sets the module list with the the given one
	void SetModuleList( const LBModuleList& moduleList );
	// ValidateProject()
	// returns true if all data in the project are correct
	bool ValidateProject( const string& lbVersion );

public:
	// UpdateVisitorMapParameter()
	// updates visitor map parameters
	// (the part for highmaps)
	void UpdateVisitorMapParameter();

public:
	// GetModuleList()
	// returns the list of modules
	LBModuleList& GetModuleList();
	// SetCurSelModuleIndex()
	// sets the index of the current selected module
	void SetCurSelModuleIndex( int index );
	
private:
	// map menu
	afx_msg void OnMapVisitorMap();
	// module menu
	afx_msg void OnModuleAddModule();
	afx_msg void OnModuleEditModule();
	afx_msg void OnModuleRemoveModule();
	afx_msg void OnModuleRemoveAllModules();
	afx_msg void OnModuleMoveDownModule();
	afx_msg void OnModuleMoveUpModule();
	afx_msg void OnModuleCheckModule();
	afx_msg void OnModuleCheckAllModules();
	afx_msg void OnModuleUncheckAllModules();
	afx_msg void OnModuleDuplicateModule();
	// tools menu
	afx_msg void OnToolsOptions();
	afx_msg void OnToolsDirectories();
	// landbuilder menu
	afx_msg void OnLandBuilderPreview();
	afx_msg void OnLandBuilderRunLandBuilder2();
};

//-----------------------------------------------------------------------------

