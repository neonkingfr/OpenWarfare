#pragma once

#include <string>

#include ".\LBDem.h"

using std::string;

class LBDemList : public CObject
{
	DECLARE_SERIAL(LBDemList)

	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the version
	int m_Version;
	// the list of parameters
	CTypedPtrList<CObList, LBDem*> m_List;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBDemList();

	// copy constructor
	LBDemList(const LBDemList& other);

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~LBDemList();

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBDemList& operator = (const LBDemList& other);

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //

	// GetList()
	// returns the list
	CTypedPtrList<CObList, LBDem*>& GetList();

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //

	// SetList()
	// sets the list with the given one
	void SetList(CTypedPtrList<CObList, LBDem*>& newList);

	// ************************************************************** //
	// Member variables manipulators                                  //
	// ************************************************************** //
	
	// Append()
	// adds a copy of the the given dem at the end of this list
	void Append(const LBDem& dem);
	void Append(LBDem* dem);

	// GetAt()
	// returns the dem of this list at the given index 
	LBDem* GetAt(int index);

	// SetAt()
	// sets the dem of this list at the given index with the given dem
	void SetAt(int index, LBDem* dem);

	// RemoveAt()
	// removes the dem at the given index from this list
	void RemoveAt(int index);

	// RemoveAll()
	// removes all the dems from this list
	void RemoveAll();

	// ************************************************************** //
	// Interface                                                      //
	// ************************************************************** //

	// GetCount()
	// returns the number of items in this list
	INT_PTR GetCount();

	// IsEmpty()
	// returns true if the list is empty
	BOOL IsEmpty();

	// Validate()
	// returns the result of data validation of the dems of this list
	LBErrorTypes Validate();

	// ************************************************************** //
	// Serialization                                                  //
	// ************************************************************** //
	virtual void Serialize(CArchive& ar);
};
