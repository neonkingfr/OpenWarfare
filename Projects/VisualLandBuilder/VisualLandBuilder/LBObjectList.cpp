#include "stdafx.h"

#include <vector>

#include ".\LBObjectList.h"

using std::vector;

IMPLEMENT_SERIAL(LBObjectList, CObject, 1)

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBObjectList::LBObjectList() 
: CObject()
, m_Version(VERSION_NUMBER)
{
}

// -------------------------------------------------------------------------- //

LBObjectList::LBObjectList(const LBObjectList& other)
: m_Version(other.m_Version)
{
	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		LBObject* item = new LBObject(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

LBObjectList::~LBObjectList()
{
	RemoveAll();
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //

LBObjectList& LBObjectList::operator = (const LBObjectList& other)
{
	m_Version = other.m_Version;

	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		LBObject* item = new LBObject(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //

CTypedPtrList<CObList, LBObject*>& LBObjectList::GetList()
{
	return m_List;
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBObjectList::SetList(CTypedPtrList<CObList, LBObject*>& newList)
{
	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = newList.GetHeadPosition();
	while(pos)
	{
		LBObject* item = new LBObject(newList.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Member variables manipulators                                  //
// ************************************************************** //

void LBObjectList::Append(const LBObject& object)
{
	LBObject* item = new LBObject(object);
	m_List.AddTail(item);
}

// -------------------------------------------------------------------------- //

void LBObjectList::Append(LBObject* object)
{
	m_List.AddTail(object);
}

// -------------------------------------------------------------------------- //

LBObject* LBObjectList::GetAt(int index)
{
	return m_List.GetAt(m_List.FindIndex(index));
}

// -------------------------------------------------------------------------- //

void LBObjectList::SetAt(int index, LBObject* object)
{
	POSITION pos = m_List.FindIndex(index);
//	LBObject* oldObject = GetAt(index);
	m_List.SetAt(pos, new LBObject(object));
//	delete oldObject;
	delete object;
}

// -------------------------------------------------------------------------- //

void LBObjectList::RemoveAt(int index)
{
	CTypedPtrList<CObList, LBObject*> tempList;

	// sets the copy of the list
	// discarding the item to remove
	POSITION pos = m_List.GetHeadPosition();
	int counter = 0;
	while(pos)
	{
		LBObject* item = m_List.GetNext(pos);
		if(counter != index)
		{
			LBObject* copyItem = new LBObject(item);
			tempList.AddTail(copyItem);
		}
		counter++;
	}

	// sets the list to be the temp list
	SetList(tempList);

	// clears the temp list
	pos = tempList.GetHeadPosition();
	while(pos)
	{
		delete tempList.GetNext(pos);
	}
	tempList.RemoveAll();
}

// -------------------------------------------------------------------------- //

void LBObjectList::RemoveAll()
{
	// removes all items from the list
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		delete m_List.GetNext(pos);
	}
	m_List.RemoveAll();
}

// ************************************************************** //
// Interface                                                      //
// ************************************************************** //

INT_PTR LBObjectList::GetCount()
{
	return m_List.GetCount();
}

// -------------------------------------------------------------------------- //

BOOL LBObjectList::IsEmpty()
{
	return m_List.IsEmpty();
}

// -------------------------------------------------------------------------- //

LBErrorTypes LBObjectList::Validate()
{
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		LBObject* item = m_List.GetNext(pos);
		LBErrorTypes res = item->Validate();
		if(res != LBET_NOERROR)
		{
			return res;
		}
	}
	return LBET_NOERROR;
}

// -------------------------------------------------------------------------- //

int LBObjectList::GetFirstFreeDbfIndex()
{
	POSITION pos = m_List.GetHeadPosition();
	vector<int> usedIndex;
	while(pos)
	{
		LBObject* item = m_List.GetNext(pos);
		if(item->GetName() == OBJ_NAME_FROMDBF)
		{
			string data = item->GetParametersList().GetAt(0)->GetValue();
			usedIndex.push_back(atoi(data.c_str()));
		}
	}

	if(usedIndex.size() == 0) return 1;

	int i = 1;

	while(true)
	{
		bool found = false;
		size_t indexCount = usedIndex.size();
		for(unsigned int j = 0; j < indexCount; ++j)
		{
			if(usedIndex[j] == i)
			{
				found = true;
				break;
			}
		}
		if(!found) return i;
		i++;
	}
}

// ************************************************************** //
// Serialization                                                  //
// ************************************************************** //

void LBObjectList::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);

	m_List.Serialize(ar);

	if(ar.IsStoring())
	{
		ar << m_Version;
	}
	else
	{
		ar >> m_Version;
	}
}

// -------------------------------------------------------------------------- //
