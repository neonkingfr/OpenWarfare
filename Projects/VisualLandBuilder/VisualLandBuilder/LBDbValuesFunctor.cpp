#include "stdafx.h"

#include ".\LBDbValuesFunctor.h"

// -------------------------------------------------------------------------- //

LBDbValuesFunctor::LBDbValuesFunctor()
{
}

// -------------------------------------------------------------------------- //

bool LBDbValuesFunctor::operator ()(unsigned int shapeId, RStringI paramName, RStringI paramValue) const
{
	string param = string(paramName.Data());
	string value = string(paramValue.Data());

	m_FieldsList.push_back(param);
	m_ValuesList.push_back(value);

	return false;
}

// -------------------------------------------------------------------------- //

vector<string>& LBDbValuesFunctor::GetValuesList() const
{
	return m_ValuesList;
}

// -------------------------------------------------------------------------- //

string LBDbValuesFunctor::GetValue(int index, string field)
{
	size_t valuesCount = m_ValuesList.size();
	int counter = 0;
	for(unsigned int i = 0; i < valuesCount; ++i)
	{
		CString s1 = m_FieldsList[i].c_str();
		s1.MakeLower();
		CString s2 = field.c_str();
		s2.MakeLower();

		if(s1 == s2)
		{
			if(counter == index)
			{
				return m_ValuesList[i];
			}
			counter++;
		}
	}
	return "";
}
