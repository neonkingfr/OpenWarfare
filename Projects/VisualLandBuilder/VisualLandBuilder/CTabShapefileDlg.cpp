// CTabShapefileDlg.cpp : implementation file
//

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CTabShapefileDlg.h"

#include ".\CAddEditModuleDlg.h"

// CTabShapefileDlg dialog

IMPLEMENT_DYNAMIC(CTabShapefileDlg, CDialog)

// -------------------------------------------------------------------------- //

CTabShapefileDlg::CTabShapefileDlg(CWnd* pParent /*=NULL*/)
: CDialog(CTabShapefileDlg::IDD, pParent)
, m_Modified(false)
{
	m_SelectedShapeType    = "";
	m_ShapesCount          = 0;
	m_MultipartShapesCount = 0;
}

// -------------------------------------------------------------------------- //

CTabShapefileDlg::~CTabShapefileDlg()
{
}

// -------------------------------------------------------------------------- //

void CTabShapefileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TABSHAPENAMETXT, m_ShapeNameTextCtrl);
	DDX_Control(pDX, IDC_SHAPEPICTURE, m_ShapePictureCtrl);
	DDX_Control(pDX, IDC_SHAPESMULTICOUNTLBL, m_MultipartShapesLbl);
	DDX_Control(pDX, IDC_SHAPESMULTICOUNTTEXT, m_MultipartShapesText);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CTabShapefileDlg, CDialog)
	ON_BN_CLICKED(IDC_TABSHAPEBROWSEBTN, &CTabShapefileDlg::OnBnClickedShapeBrowseBtn)
	ON_BN_CLICKED(IDC_TABSHAPECLEARBTN, &CTabShapefileDlg::OnBnClickedTabShapeClearBtn)
	ON_WM_SETCURSOR()
	ON_STN_CLICKED(IDC_SHAPEPICTURE, &CTabShapefileDlg::OnStnClickedShapePicture)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CTabShapefileDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	return TRUE;
}

// -------------------------------------------------------------------------- //

BOOL CTabShapefileDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// Change cursor when over the picture
	if(pWnd == GetDlgItem(IDC_SHAPEPICTURE))
	{
		::SetCursor(::LoadCursor(NULL, IDC_HAND));
		return TRUE;
	}

	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //

void CTabShapefileDlg::UpdateState()
{
	// update controls
	string name = m_pModule->GetShapeFileName();
	m_ShapeNameTextCtrl.SetWindowTextA(name.c_str());
	if(name == "") 
	{
		m_SelectedShapeType = "";
		GetDlgItem(IDC_SHAPESCOUNTTEXT)->SetWindowTextA("");
		GetDlgItem(IDC_SHAPESMULTICOUNTTEXT)->SetWindowTextA("");
	}

	if(!m_pModule->GetUseShapefile())
	{
		GetDlgItem(IDC_TABSHAPEFILEGROUP)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABSHAPENAMELBL)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABSHAPENAMETXT)->EnableWindow(FALSE);		
		GetDlgItem(IDC_TABSHAPECLEARBTN)->EnableWindow(FALSE);		
		GetDlgItem(IDC_TABSHAPEBROWSEBTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABSHAPEDATAGROUP)->EnableWindow(FALSE);
		GetDlgItem(IDC_SHAPESTYPELBL)->EnableWindow(FALSE);
		GetDlgItem(IDC_SHAPESTYPETEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_SHAPESCOUNTLBL)->EnableWindow(FALSE);
		GetDlgItem(IDC_SHAPESCOUNTTEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_SHAPESMULTICOUNTLBL)->EnableWindow(FALSE);
		GetDlgItem(IDC_SHAPESMULTICOUNTTEXT)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_TABSHAPEFILEGROUP)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABSHAPENAMELBL)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABSHAPENAMETXT)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABSHAPEBROWSEBTN)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABSHAPEDATAGROUP)->EnableWindow(TRUE);
		GetDlgItem(IDC_SHAPESTYPELBL)->EnableWindow(TRUE);
		GetDlgItem(IDC_SHAPESTYPETEXT)->EnableWindow(TRUE);
		GetDlgItem(IDC_SHAPESCOUNTLBL)->EnableWindow(TRUE);
		GetDlgItem(IDC_SHAPESCOUNTTEXT)->EnableWindow(TRUE);
		GetDlgItem(IDC_SHAPESMULTICOUNTLBL)->EnableWindow(TRUE);
		GetDlgItem(IDC_SHAPESMULTICOUNTTEXT)->EnableWindow(TRUE);

		if(name == "" || GetFileAttributes(name.c_str()) == INVALID_FILE_ATTRIBUTES)
		{
			GetDlgItem(IDC_TABSHAPECLEARBTN)->EnableWindow(FALSE);
			m_ShapePictureCtrl.SetShapeList(NULL);
			m_ShapePictureCtrl.Invalidate();
			m_ShapePictureCtrl.EnableWindow(FALSE);
		}
		else
		{
			GetDlgItem(IDC_TABSHAPECLEARBTN)->EnableWindow(TRUE);
			Shapes::ShapeList& shapeList = reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->GetShapeList();
			m_ShapePictureCtrl.SetShapeList(&shapeList);
			m_ShapePictureCtrl.Invalidate();
			m_ShapePictureCtrl.EnableWindow(TRUE);
			m_SelectedShapeType = reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->SelectedShapeType();
			m_ShapesCount = shapeList.Size();
			CString data;
			data.Format("%d", m_ShapesCount);
			GetDlgItem(IDC_SHAPESCOUNTTEXT)->SetWindowTextA(data);
			m_MultipartShapesCount = reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->GetMultipartShapesCount();
			data.Format("%d", m_MultipartShapesCount);
			GetDlgItem(IDC_SHAPESMULTICOUNTTEXT)->SetWindowTextA(data);
		}
	}

	GetDlgItem(IDC_SHAPESTYPETEXT)->SetWindowTextA(m_SelectedShapeType.c_str());

	// disables the module list if there is at least one object
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateModuleListCtrlState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateClearAllDataBtnState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->CheckOkButtonEnabling();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateDbfTabDialog(true);
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateShapeTabImage();
}

// -------------------------------------------------------------------------- //

bool CTabShapefileDlg::GetModified() const
{
	return m_Modified;
}

// -------------------------------------------------------------------------- //

bool CTabShapefileDlg::HasData() const
{
	if(m_pModule->GetShapeFileName() == "") return false;
	return true;
}

// -------------------------------------------------------------------------- //

void CTabShapefileDlg::SetModule(LBModule* module)
{
	m_pModule = module;
}

// -------------------------------------------------------------------------- //

BOOL CTabShapefileDlg::GetClearButtonState() const
{
	return GetDlgItem(IDC_TABSHAPECLEARBTN)->IsWindowEnabled();	
}

// -------------------------------------------------------------------------- //

void CTabShapefileDlg::SetMultipartShapesCtrlColor(bool error)
{
	// set to red
	if(error)
	{
		m_MultipartShapesLbl.SetTextColor(RGB(255, 0, 0));
		m_MultipartShapesText.SetTextColor(RGB(255, 0, 0));
	}
	// set to black
	else
	{
		m_MultipartShapesLbl.SetTextColor(RGB(0, 0, 0));
		m_MultipartShapesText.SetTextColor(RGB(0, 0, 0));
	}
}

// -------------------------------------------------------------------------- //
// PICTURE HANDLERS                                                           //
// -------------------------------------------------------------------------- //

void CTabShapefileDlg::OnStnClickedShapePicture()
{
	LBModuleList moduleList;
	LBModule* tempModule = new LBModule(reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->GetModule());
	moduleList.Append(tempModule);

	m_PreviewDlg.SetModuleList(&moduleList, true, false);
	m_PreviewDlg.DoModal();
}

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //

void CTabShapefileDlg::OnBnClickedShapeBrowseBtn()
{
	string strFilter = "Shapefiles (*.shp)|*.shp||";
	string strExt    = "shp";
	string strFile   = m_pModule->GetShapeFileName();

	DWORD flags = OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_EXTENSIONDIFFERENT;
	CFileDialog fileDlg(TRUE, strExt.c_str(), strFile.c_str(), flags, strFilter.c_str(), this);

	if(fileDlg.DoModal() == IDOK)
	{
		string oldName = m_pModule->GetShapeFileName();
		m_pModule->SetShapeFileName(fileDlg.GetPathName());
		reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->LoadShapeFile();

		if(!m_pModule->IsAllowedShapeList(reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->GetShapeList()))
		{
			AfxGetMainWnd()->MessageBox(string(MSG_ERRORSHAPEFILETYPE).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);

			m_pModule->SetShapeFileName(oldName);
			reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->LoadShapeFile();
		}
		m_SelectedShapeType = reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->SelectedShapeType();
		UpdateState();
	}
}

// -------------------------------------------------------------------------- //

void CTabShapefileDlg::OnBnClickedTabShapeClearBtn()
{
	string text = MSG_CONFIRMCLEARSHAPENAME;
	if(MessageBox(text.c_str(), "", MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
		m_pModule->SetShapeFileName(string(""));
		UpdateState();
	}
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CTabShapefileDlg::PreTranslateMessage(MSG* pMsg)
{
	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

// -------------------------------------------------------------------------- //
