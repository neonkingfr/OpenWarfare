#include "stdafx.h"

#include ".\LBRectangle.h"

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBRectangle::LBRectangle()
{
}

// -------------------------------------------------------------------------- //

LBRectangle::LBRectangle(double left, double top, double right, double bottom) 
: m_Left(left)
, m_Top(top)
, m_Right(right)
, m_Bottom(bottom)
{
}

// -------------------------------------------------------------------------- //

LBRectangle::LBRectangle(const LBRectangle& other) 
: m_Left(other.m_Left)
, m_Top(other.m_Top)
, m_Right(other.m_Right)
, m_Bottom(other.m_Bottom)
{
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //

LBRectangle& LBRectangle::operator = (const LBRectangle& other)
{
	m_Left   = other.m_Left;
	m_Top    = other.m_Top; 
	m_Right  = other.m_Right;
	m_Bottom = other.m_Bottom;

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //

double LBRectangle::GetLeft() const
{
	return m_Left;
}

// -------------------------------------------------------------------------- //

double LBRectangle::Left() const
{
	return m_Left;
}

// -------------------------------------------------------------------------- //

double LBRectangle::GetTop() const
{
	return m_Top;
}

// -------------------------------------------------------------------------- //

double LBRectangle::Top() const
{
	return m_Top;
}

// -------------------------------------------------------------------------- //

double LBRectangle::GetRight() const
{
	return m_Right;
}

// -------------------------------------------------------------------------- //

double LBRectangle::Right() const
{
	return m_Right;
}

// -------------------------------------------------------------------------- //

double LBRectangle::GetBottom() const
{
	return m_Bottom;
}

// -------------------------------------------------------------------------- //

double LBRectangle::Bottom() const
{
	return m_Bottom;
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBRectangle::Set(double newLeft, double newTop, double newRight, double newBottom)
{
	m_Left   = newLeft;
	m_Top    = newTop; 
	m_Right  = newRight;
	m_Bottom = newBottom;
}

// -------------------------------------------------------------------------- //

void LBRectangle::SetLeft(double newLeft)
{
	m_Left = newLeft;
}

// -------------------------------------------------------------------------- //

void LBRectangle::Left(double newLeft)
{
	m_Left = newLeft;
}

// -------------------------------------------------------------------------- //

void LBRectangle::SetTop(double newTop)
{
	m_Top = newTop;
}

// -------------------------------------------------------------------------- //

void LBRectangle::Top(double newTop)
{
	m_Top = newTop;
}

// -------------------------------------------------------------------------- //

void LBRectangle::SetRight(double newRight)
{
	m_Right = newRight;
}

// -------------------------------------------------------------------------- //

void LBRectangle::Right(double newRight)
{
	m_Right = newRight;
}

// -------------------------------------------------------------------------- //

void LBRectangle::SetBottom(double newBottom)
{
	m_Bottom = newBottom;
}

// -------------------------------------------------------------------------- //

void LBRectangle::Bottom(double newBottom)
{
	m_Bottom = newBottom;
}

// ************************************************************** //
// Member variables manipulators                                  //
// ************************************************************** //

void LBRectangle::Zero()
{
	m_Left   = 0.0;
	m_Top    = 0.0; 
	m_Right  = 0.0;
	m_Bottom = 0.0;
}

// -------------------------------------------------------------------------- //

void LBRectangle::Scale(double scaleX, double scaleY)
{
	double midX = (m_Left + m_Right) / 2.0;
	double midY = (m_Top + m_Bottom) / 2.0;

	double halfWidth  = Width() / 2.0;
	double halfHeight = Height() / 2.0;

	m_Left  = midX - halfWidth * scaleX;
	m_Right = midX + halfWidth * scaleX;

	m_Top    = midY + halfHeight * scaleY;
	m_Bottom = midY - halfHeight * scaleY;
}

// ************************************************************** //
// Rectangle properties                                           //
// ************************************************************** //

bool LBRectangle::IsZero() const
{
	if(m_Left == 0.0 && m_Right == 0.0 && m_Top == 0.0 && m_Bottom == 0.0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// -------------------------------------------------------------------------- //

double LBRectangle::Width() const
{
	return (m_Right - m_Left);
}

// -------------------------------------------------------------------------- //

double LBRectangle::Height() const
{
	return (m_Top - m_Bottom);
}

// -------------------------------------------------------------------------- //

double LBRectangle::AspectRatio() const
{
	double width = Width();
	if(width == 0.0) return -1.0;
	return Height() / width;
}

// -------------------------------------------------------------------------- //

bool LBRectangle::Contains(double x, double y) const
{
	if(x < m_Left) return false;
	if(x > m_Right) return false;
	if(y > m_Top) return false;
	if(y < m_Bottom) return false;
	return true;
}

// -------------------------------------------------------------------------- //

bool LBRectangle::Contains(const Shapes::DVertex& v1) const
{
	return Contains(v1.x, v1.y);
}

// -------------------------------------------------------------------------- //

Shapes::DVertex LBRectangle::GetBarycenter() const
{
	double x = (m_Left + m_Right) * 0.5;
	double y = (m_Top + m_Bottom) * 0.5;

	return Shapes::DVertex(x, y);
}

// -------------------------------------------------------------------------- //

void LBRectangle::ClipSegment(Shapes::DVertex* v1, Shapes::DVertex* v2)
{
	if(Contains(*v1))
	{
		if(Contains(*v2))
		{
			return;
		}
		else
		{
			Shapes::DVertex* tempV = IntersectionPoint(v1, v2);
			if(tempV != NULL)
			{
				*v2 = *tempV;
				delete tempV;
			}
		}
	}
	else
	{
		if(Contains(*v2))
		{
			Shapes::DVertex* tempV = IntersectionPoint(v2, v1);
			if(tempV != NULL)
			{
				*v1 = *tempV;
				delete tempV;
			}
		}
		else
		{
			*v1 = *v2;
		}
	}
}

// -------------------------------------------------------------------------- //

Shapes::DVertex* LBRectangle::IntersectionPoint(Shapes::DVertex* v1, Shapes::DVertex* v2)
{
	// segment
	double a = v2->y - v1->y;
	double b = v1->x - v2->x;
	double c = v1->x * (v1->y - v2->y) + v1->y * (v2->x - v1->x);

	double minSegX = min(v1->x, v2->x);
	double maxSegX = max(v1->x, v2->x);
	double minSegY = min(v1->y, v2->y);
	double maxSegY = max(v1->y, v2->y);

	// the given segment is nor horizontal nor vertical
	if(a != 0.0 && b != 0.0)
	{
		// searches for intersection with left edge
		double x = m_Left;
		double y = -(a * x + c) / b;
		if((y >= m_Bottom && y <= m_Top) && (x >= minSegX && x <= maxSegX))
		{
			return new Shapes::DVertex(x, y);
		}

		// searches for intersection with right edge
		x = m_Right;
		y = -(a * x + c) / b;
		if((y >= m_Bottom && y <= m_Top) && (x >= minSegX && x <= maxSegX))
		{
			return new Shapes::DVertex(x, y);
		}

		// searches for intersection with bottom edge
		y = m_Bottom;
		x = -(b * y + c) / a;
		if((x >= m_Left && x <= m_Right) && (x >= minSegX && x <= maxSegX))
		{
			return new Shapes::DVertex(x, y);
		}

		// searches for intersection with top edge
		y = m_Top;
		x = -(b * y + c) / a;
		if((x >= m_Left && x <= m_Right) && (x >= minSegX && x <= maxSegX))
		{
			return new Shapes::DVertex(x, y);
		}
	}

	// the given segment is horizontal
	if(a == 0.0)
	{
		// searches for intersection with left edge
		double x = m_Left;
		double y = -(a * x + c) / b;
		if((y >= m_Bottom && y <= m_Top) && (x >= minSegX && x <= maxSegX))
		{
			return new Shapes::DVertex(x, y);
		}

		// searches for intersection with right edge
		x = m_Right;
		y = -(a * x + c) / b;
		if((y >= m_Bottom && y <= m_Top) && (x >= minSegX && x <= maxSegX))
		{
			return new Shapes::DVertex(x, y);
		}
	}

	// the given segment is vertical
	if(b == 0.0)
	{
		// searches for intersection with bottom edge
		double y = m_Bottom;
		double x = -(b * y + c) / a;
		if((x >= m_Left && x <= m_Right) && (y >= minSegY && y <= maxSegY))
		{
			return new Shapes::DVertex(x, y);
		}

		// searches for intersection with top edge
		y = m_Top;
		x = -(b * y + c) / a;
		if((x >= m_Left && x <= m_Right) && (y >= minSegY && y <= maxSegY))
		{
			return new Shapes::DVertex(x, y);
		}
	}

	return NULL;
}