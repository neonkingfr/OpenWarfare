// CAboutVBSDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VisualLandBuilder.h"
#include "CAboutVBSDlg.h"


// CAboutVBSDlg dialog

IMPLEMENT_DYNAMIC(CAboutVBSDlg, CDialog)

CAboutVBSDlg::CAboutVBSDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAboutVBSDlg::IDD, pParent)
{

}

CAboutVBSDlg::~CAboutVBSDlg()
{
}

void CAboutVBSDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAboutVBSDlg, CDialog)
END_MESSAGE_MAP()


// CAboutVBSDlg message handlers
