// CRunLandBuilderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VisualLandBuilder.h"
#include "CRunLandBuilderDlg.h"

// CRunLandBuilderDlg dialog

IMPLEMENT_DYNAMIC(CRunLandBuilderDlg, CDialog)

// -------------------------------------------------------------------------- //

CRunLandBuilderDlg::CRunLandBuilderDlg(CWnd* pParent /*=NULL*/)
: CDialog(CRunLandBuilderDlg::IDD, pParent)
{
}

// -------------------------------------------------------------------------- //

CRunLandBuilderDlg::~CRunLandBuilderDlg()
{
}

// -------------------------------------------------------------------------- //

void CRunLandBuilderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_RunNameCtrl);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CRunLandBuilderDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CRunLandBuilderDlg::OnBnClickedOk)
	ON_EN_CHANGE(IDC_RUNLBEDIT, &CRunLandBuilderDlg::OnEnChangeRunLbEdit)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CRunLandBuilderDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	m_RunNameCtrl.SetWindowTextA(m_Name.c_str());

	return TRUE;
}

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

void CRunLandBuilderDlg::SetName(string* name)
{
	// sets the working copy of the data
	m_Name = *name;

	// sets the pointer to the original data
	m_pOldName = name;
}

// -------------------------------------------------------------------------- //

bool CRunLandBuilderDlg::Validate()
{
	if(m_Name.size() == 0) return false;

	return true;
}

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //

void CRunLandBuilderDlg::OnBnClickedOk()
{
	if(Validate())
	{
		// updates data
		*m_pOldName = m_Name;
	}

	OnOK();
}

// -------------------------------------------------------------------------- //
// EDIT HANDLERS                                                              //
// -------------------------------------------------------------------------- //

void CRunLandBuilderDlg::OnEnChangeRunLbEdit()
{
	CString data;
	m_RunNameCtrl.GetWindowText(data);

	m_Name = data;
}
