#pragma once
#include "afxcmn.h"

#include ".\LBModule.h"
#include ".\ExtendedControls\CListCtrlEx.h"
#include ".\CEditParameterDlg.h"
#include "afxwin.h"

// CTabGlobalDlg dialog

class CTabGlobalDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabGlobalDlg)

public:
	CTabGlobalDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabGlobalDlg();

// Dialog Data
	enum { IDD = IDD_TABGLOBAL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

private:
	// the parameters list control
	CListCtrlEx m_GlobalParListCtrl;
	// the description ctrl
	CStatic m_GlobalDescriptionCtrl;

	// pointer to the current module
	LBModule* m_pModule;

	// will be set to true if the data are modified
	bool m_Modified;

	// the dialog for edit parameters
	CEditParameterDlg m_EditParameterDlg;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //
public:
	// updates the state of the dialog sets m_Modified if data have been changed
	void UpdateState();
	// returns true if data have been modified
	bool GetModified() const;
	// return true if there are data already inputted
	bool HasData() const;
	// return true if there are all the data needed
	bool HasAllData() const;
	// sets the module
	void SetModule(LBModule* module);
	// returns the state of the clear button (true if enabled)
	BOOL GetClearButtonState() const;

// -------------------------------------------------------------------------- //
// OBJECT LIST EVENT HANDLERS                                                 //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnNMClickTabGlobalParList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemActivateTabGlobalParList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownTabGlobalParList(NMHDR *pNMHDR, LRESULT *pResult);

// -------------------------------------------------------------------------- //
// OBJECT LIST BUTTONS HANDLERS                                               //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnBnClickedTabGlobalEditBtn();
	afx_msg void OnBnClickedTabGlobalToDbfBtn();
public:
	afx_msg void OnBnClickedTabGlobalClearBtn();


// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	// ValidateParamCtrlInput()
	// verify the inputs in the parameters ctrl
	bool ValidateParamCtrlInput(CString data, LBParameter* par);
// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	// this function is used to intercept the end label editing in
	// the list ctrl
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
