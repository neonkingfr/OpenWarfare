//-----------------------------------------------------------------------------

#include "stdafx.h"

#include ".\LBDem.h"

//-----------------------------------------------------------------------------

IMPLEMENT_SERIAL( LBDem, CObject, 1 )

//-----------------------------------------------------------------------------

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBDem::LBDem() 
: CObject()
, m_Version( VERSION_NUMBER )
, m_Type( LBDT_START_ENUM )
{
}

//-----------------------------------------------------------------------------

LBDem::LBDem( LBDemTypes type, const string& fileName ) 
: CObject()
, m_Version( VERSION_NUMBER )
, m_Type( type )
, m_FileName( fileName )
{
	SetDefaultParameterList();
}

//-----------------------------------------------------------------------------

LBDem::LBDem( LBDemTypes type, const string& fileName,
              const LBParameterList& parametersList ) 
: CObject()
, m_Version( VERSION_NUMBER )
, m_Type( type )
, m_FileName( fileName )
, m_ParametersList( parametersList )
{
}

//-----------------------------------------------------------------------------

LBDem::LBDem( LBDem* other ) 
: m_Version( VERSION_NUMBER )
, m_Type( other->m_Type )
, m_FileName( other->m_FileName )
, m_ParametersList( other->m_ParametersList )
{
}

//-----------------------------------------------------------------------------

LBDem::LBDem( const LBDem& other ) 
: m_Version( VERSION_NUMBER )
, m_Type( other.m_Type )
, m_FileName( other.m_FileName )
, m_ParametersList( other.m_ParametersList )
{
}

//-----------------------------------------------------------------------------

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

LBDem::~LBDem()
{
}

//-----------------------------------------------------------------------------

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //

LBDem& LBDem::operator = ( const LBDem& other )
{
	m_Version        = other.m_Version;
	m_Type           = other.m_Type;
	m_FileName       = other.m_FileName;
	m_ParametersList = other.m_ParametersList;

	return (*this);
}

//-----------------------------------------------------------------------------

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //
	
LBDemTypes LBDem::GetType() const
{
	return (m_Type);
}

//-----------------------------------------------------------------------------

string LBDem::GetFileName() const
{
	return (m_FileName);
}

//-----------------------------------------------------------------------------

string LBDem::GetTypeAsString() const
{
	return (LBDemTypesAsString[m_Type]);
}

//-----------------------------------------------------------------------------

LBParameterList& LBDem::GetParametersList() 
{
	return (m_ParametersList);
}

//-----------------------------------------------------------------------------

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBDem::SetType( LBDemTypes newType )
{
	m_Type = newType;
}

//-----------------------------------------------------------------------------

void LBDem::SetFileName( const string& newFileName )
{
	m_FileName = newFileName;
}

//-----------------------------------------------------------------------------

void LBDem::SetFileName( const CString& newFileName )
{
	m_FileName = newFileName;
}

//-----------------------------------------------------------------------------

void LBDem::SetParameterList( const LBParameterList& parametersList )
{
	m_ParametersList = parametersList;
}

//-----------------------------------------------------------------------------

// ************************************************************** //
// Helper functions                                               //
// ************************************************************** //

void LBDem::SetDefaultParameterList()
{
	m_ParametersList.Append( new LBParameter( "srcLeft"  , "", PARDESC_SRCLEFT  , LBPT_FLOAT, false, false, false ) );
	m_ParametersList.Append( new LBParameter( "srcBottom", "", PARDESC_SRCBOTTOM, LBPT_FLOAT, false, false, false ) );
	m_ParametersList.Append( new LBParameter( "srcRight" , "", PARDESC_SRCRIGHT , LBPT_FLOAT, false, false, false ) );
	m_ParametersList.Append( new LBParameter( "srcTop"   , "", PARDESC_SRCTOP   , LBPT_FLOAT, false, false, false ) );
	m_ParametersList.Append( new LBParameter( "dstLeft"  , "", PARDESC_DSTLEFT  , LBPT_FLOAT, false, false, false ) );
	m_ParametersList.Append( new LBParameter( "dstBottom", "", PARDESC_DSTBOTTOM, LBPT_FLOAT, false, false, false ) );
	m_ParametersList.Append( new LBParameter( "dstRight" , "", PARDESC_DSTRIGHT , LBPT_FLOAT, false, false, false ) );
	m_ParametersList.Append( new LBParameter( "dstTop"   , "", PARDESC_DSTTOP   , LBPT_FLOAT, false, false, false ) );
}

//-----------------------------------------------------------------------------

// ************************************************************** //
// Interface                                                      //
// ************************************************************** //

LBErrorTypes LBDem::Validate()
{
	// checks if dems file exists
	if ( GetFileAttributes( m_FileName.c_str() ) == INVALID_FILE_ATTRIBUTES )
	{
		return (LBET_DEMFILENOTFOUND);
	}

	// checks if all parameters have been set to a value
	if ( !m_ParametersList.Validate() )
	{
		return (LBET_MISSINGDEMPARAMETER);
	}

	return (LBET_NOERROR);
}

//-----------------------------------------------------------------------------

LandBuildInt::MapInput LBDem::AdvancedRandomPlacerSlopeMapInput()
{
	string demType  = GetTypeAsString();
	string fileName = m_FileName;

	size_t srcLeft;
	string strSrcLeft = m_ParametersList.GetValue( PARNAME_SRCLEFT );
	if ( strSrcLeft != "" )
	{
		srcLeft = static_cast< size_t >( atoi( strSrcLeft.c_str() ) );
	}

	size_t srcTop;
	string strSrcTop = m_ParametersList.GetValue( PARNAME_SRCTOP );
	if ( strSrcTop != "" )
	{
		srcTop = static_cast< size_t >( atoi( strSrcTop.c_str() ) );
	}

	size_t srcRight;
	string strSrcRight = m_ParametersList.GetValue( PARNAME_SRCRIGHT );
	if ( strSrcRight != "" )
	{
		srcRight = static_cast< size_t >( atoi( strSrcRight.c_str() ) );
	}

	size_t srcBottom;
	string strSrcBottom = m_ParametersList.GetValue( PARNAME_SRCBOTTOM );
	if ( strSrcBottom != "" )
	{
		srcBottom = static_cast< size_t >( atoi( strSrcBottom.c_str() ) );
	}

	double dstLeft;
	string strDstLeft = m_ParametersList.GetValue( PARNAME_DSTLEFT );
	if ( strDstLeft != "" )
	{
		dstLeft = atof( strDstLeft.c_str() );
	}

	double dstTop;
	string strDstTop = m_ParametersList.GetValue( PARNAME_DSTTOP );
	if ( strDstTop != "" )
	{
		dstTop = atof( strDstTop.c_str() );
	}

	double dstRight;
	string strDstRight = m_ParametersList.GetValue( PARNAME_DSTRIGHT );
	if ( strDstRight != "" )
	{
		dstRight = atof( strDstRight.c_str() );
	}

	double dstBottom;
	string strDstBottom = m_ParametersList.GetValue( PARNAME_DSTBOTTOM );
	if ( strDstBottom != "" )
	{
		dstBottom = atof( strDstBottom.c_str() );
	}

	return (LandBuildInt::MapInput( demType, fileName, srcLeft, srcTop, srcRight, srcBottom, 
                                  dstLeft, dstTop, dstRight, dstBottom ));
}

// -------------------------------------------------------------------------- //

LandBuildInt::MapInput LBDem::BiasedRandomPlacerSlopeMapInput()
{
	string demType  = GetTypeAsString();
	string fileName = m_FileName;

	size_t srcLeft;
	string strSrcLeft = m_ParametersList.GetValue( PARNAME_SRCLEFT );
	if ( strSrcLeft != "" )
	{
		srcLeft = static_cast< size_t >( atoi( strSrcLeft.c_str() ) );
	}

	size_t srcTop;
	string strSrcTop = m_ParametersList.GetValue( PARNAME_SRCTOP );
	if ( strSrcTop != "" )
	{
		srcTop = static_cast< size_t >( atoi( strSrcTop.c_str() ) );
	}

	size_t srcRight;
	string strSrcRight = m_ParametersList.GetValue( PARNAME_SRCRIGHT );
	if ( strSrcRight != "" )
	{
		srcRight = static_cast< size_t >( atoi( strSrcRight.c_str() ) );
	}

	size_t srcBottom;
	string strSrcBottom = m_ParametersList.GetValue( PARNAME_SRCBOTTOM );
	if ( strSrcBottom != "" )
	{
		srcBottom = static_cast< size_t >( atoi( strSrcBottom.c_str() ) );
	}

	double dstLeft;
	string strDstLeft = m_ParametersList.GetValue( PARNAME_DSTLEFT );
	if ( strDstLeft != "" )
	{
		dstLeft = atof( strDstLeft.c_str() );
	}

	double dstTop;
	string strDstTop = m_ParametersList.GetValue( PARNAME_DSTTOP );
	if ( strDstTop != "" )
	{
		dstTop = atof( strDstTop.c_str() );
	}

	double dstRight;
	string strDstRight = m_ParametersList.GetValue( PARNAME_DSTRIGHT );
	if ( strDstRight != "" )
	{
		dstRight = atof( strDstRight.c_str() );
	}

	double dstBottom;
	string strDstBottom = m_ParametersList.GetValue( PARNAME_DSTBOTTOM );
	if ( strDstBottom != "" )
	{
		dstBottom = atof( strDstBottom.c_str() );
	}

	return (LandBuildInt::MapInput( demType, fileName, srcLeft, srcTop, srcRight, srcBottom, 
                                  dstLeft, dstTop, dstRight, dstBottom ));
}

// -------------------------------------------------------------------------- //

LandBuildInt::MapInput LBDem::SimpleRandomPlacerSlopeMapInput()
{
	string demType  = GetTypeAsString();
	string fileName = m_FileName;

	size_t srcLeft;
	string strSrcLeft = m_ParametersList.GetValue( PARNAME_SRCLEFT );
	if ( strSrcLeft != "" )
	{
		srcLeft = static_cast< size_t >( atoi( strSrcLeft.c_str() ) );
	}

	size_t srcTop;
	string strSrcTop = m_ParametersList.GetValue( PARNAME_SRCTOP );
	if ( strSrcTop != "" )
	{
		srcTop = static_cast< size_t >( atoi( strSrcTop.c_str() ) );
	}

  size_t srcRight;
	string strSrcRight = m_ParametersList.GetValue( PARNAME_SRCRIGHT );
	if ( strSrcRight != "" )
	{
		srcRight = static_cast< size_t >( atoi( strSrcRight.c_str() ) );
	}

	size_t srcBottom;
	string strSrcBottom = m_ParametersList.GetValue( PARNAME_SRCBOTTOM );
	if ( strSrcBottom != "" )
	{
		srcBottom = static_cast< size_t >( atoi( strSrcBottom.c_str() ) );
	}

	double dstLeft;
	string strDstLeft = m_ParametersList.GetValue( PARNAME_DSTLEFT );
	if ( strDstLeft != "" )
	{
		dstLeft = atof( strDstLeft.c_str() );
	}

	double dstTop;
	string strDstTop = m_ParametersList.GetValue( PARNAME_DSTTOP );
	if ( strDstTop != "" )
	{
		dstTop = atof( strDstTop.c_str() );
	}

	double dstRight;
	string strDstRight = m_ParametersList.GetValue( PARNAME_DSTRIGHT );
	if ( strDstRight != "" )
	{
		dstRight = atof( strDstRight.c_str() );
	}

	double dstBottom;
	string strDstBottom = m_ParametersList.GetValue( PARNAME_DSTBOTTOM );
	if ( strDstBottom != "" )
	{
		dstBottom = atof( strDstBottom.c_str() );
	}

	return (LandBuildInt::MapInput( demType, fileName, srcLeft, srcTop, srcRight, srcBottom, 
                                  dstLeft, dstTop, dstRight, dstBottom ));
}

// -------------------------------------------------------------------------- //

// ************************************************************** //
// Serialization                                                  //
// ************************************************************** //

void LBDem::Serialize( CArchive& ar )
{
	CString fileName;
	UINT    type;

	CObject::Serialize( ar );

	m_ParametersList.Serialize( ar );

	if ( ar.IsStoring() )
	{
		fileName = CString( m_FileName.c_str() );
		type = UINT( m_Type );
		ar << m_Version;
		ar << fileName;
		ar << type;
	}
	else
	{
		ar >> m_Version;
		ar >> fileName;
		ar >> type;
		m_FileName = fileName;
		m_Type = static_cast< LBDemTypes >( type );
	}
}

// -------------------------------------------------------------------------- //
