#include "stdafx.h"

#include ".\LBDirectoryItem.h"
#include ".\LBGlobalVariables.h"

IMPLEMENT_SERIAL(LBDirectoryItem, CObject, 1)

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBDirectoryItem::LBDirectoryItem() 
: CObject()
, m_Version(VERSION_NUMBER)
{
}

// -------------------------------------------------------------------------- //

LBDirectoryItem::LBDirectoryItem(const string& resource, const string& resourcePathName, 
								 LBDirectoryItemTypes resourceType,
								 const string& resourceFilter,
								 const string& resourceFileName,
								 const string& resourceExtension) 
: CObject()
, m_Version(VERSION_NUMBER)
, m_Resource(resource)
, m_ResourcePathName(resourcePathName)
, m_ResourceType(resourceType)
, m_ResourceFilter(resourceFilter)
, m_ResourceFileName(resourceFileName)
, m_ResourceExtension(resourceExtension)
{
}

// -------------------------------------------------------------------------- //

LBDirectoryItem::LBDirectoryItem(LBDirectoryItem* other) 
: m_Version(other->m_Version)
, m_Resource(other->m_Resource)
, m_ResourcePathName(other->m_ResourcePathName)
, m_ResourceType(other->m_ResourceType)
, m_ResourceFilter(other->m_ResourceFilter)
, m_ResourceFileName(other->m_ResourceFileName)
, m_ResourceExtension(other->m_ResourceExtension)
{
}

// -------------------------------------------------------------------------- //

LBDirectoryItem::LBDirectoryItem(const LBDirectoryItem& other) 
: m_Version(other.m_Version)
, m_Resource(other.m_Resource)
, m_ResourcePathName(other.m_ResourcePathName)
, m_ResourceType(other.m_ResourceType)
, m_ResourceFilter(other.m_ResourceFilter)
, m_ResourceFileName(other.m_ResourceFileName)
, m_ResourceExtension(other.m_ResourceExtension)
{
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //
LBDirectoryItem& LBDirectoryItem::operator = (const LBDirectoryItem& other)
{
	m_Version           = other.m_Version;
	m_Resource          = other.m_Resource;
	m_ResourcePathName  = other.m_ResourcePathName;
	m_ResourceType      = other.m_ResourceType;
	m_ResourceFilter    = other.m_ResourceFilter;
	m_ResourceFileName  = other.m_ResourceFileName;
	m_ResourceExtension = other.m_ResourceExtension;

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //
	
string LBDirectoryItem::GetResource() const 
{ 
	return m_Resource; 
}

// -------------------------------------------------------------------------- //

string LBDirectoryItem::GetResourcePathName() const 
{ 
	return m_ResourcePathName; 
}

// -------------------------------------------------------------------------- //

LBDirectoryItemTypes LBDirectoryItem::GetResourceType() const 
{ 
	return m_ResourceType; 
}

// -------------------------------------------------------------------------- //

string LBDirectoryItem::GetResourceFilter() const
{
	return m_ResourceFilter; 
}

// -------------------------------------------------------------------------- //

string LBDirectoryItem::GetResourceFileName() const 
{ 
	return m_ResourceFileName; 
}

// -------------------------------------------------------------------------- //

string LBDirectoryItem::GetResourceExtension() const
{
	return m_ResourceExtension; 
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBDirectoryItem::SetResource(const string& newResource) 
{ 
	m_Resource = newResource; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResource(const CString& newResource) 
{ 
	m_Resource = newResource; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResourcePathName(const string& newResourcePathName) 
{ 
	m_ResourcePathName = newResourcePathName; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResourcePathName(const CString& newResourcePathName) 
{ 
	m_ResourcePathName = newResourcePathName; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResourceType(LBDirectoryItemTypes newResourceType) 
{ 
	m_ResourceType = newResourceType; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResourceFilter(const string& newResourceFilter) 
{ 
	m_ResourceFilter = newResourceFilter; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResourceFilter(const CString& newResourceFilter) 
{ 
	m_ResourceFilter = newResourceFilter; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResourceFileName(const string& newResourceFileName) 
{ 
	m_ResourceFileName = newResourceFileName; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResourceFileName(const CString& newResourceFileName) 
{ 
	m_ResourceFileName = newResourceFileName; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResourceExtension(const string& newResourceExtension) 
{ 
	m_ResourceExtension = newResourceExtension; 
}

// -------------------------------------------------------------------------- //

void LBDirectoryItem::SetResourceExtension(const CString& newResourceExtension) 
{ 
	m_ResourceExtension = newResourceExtension; 
}

// ************************************************************** //
// Interface                                                      //
// ************************************************************** //

LBErrorTypes LBDirectoryItem::Validate()
{
	if(m_ResourcePathName == "")
	{
		return LBET_MISSINGDIRECTORY;
	}
	return LBET_NOERROR;
}

// ************************************************************** //
// Serialization                                                  //
// ************************************************************** //

void LBDirectoryItem::Serialize(CArchive& ar)
{
	CString resource;
	CString resourcePathName;
	UINT    resourceType;
	CString resourceFilter;
	CString resourceFileName;
	CString resourceExtension;

	CObject::Serialize(ar);

	if(ar.IsStoring())
	{
		resource          = CString(m_Resource.c_str());
		resourcePathName  = CString(m_ResourcePathName.c_str());
		resourceType      = UINT(m_ResourceType);
		resourceFilter    = CString(m_ResourceFilter.c_str());
		resourceFileName  = CString(m_ResourceFileName.c_str());
		resourceExtension = CString(m_ResourceExtension.c_str());
		ar << m_Version;
		ar << resource;
		ar << resourcePathName;
		ar << resourceType;
		ar << resourceFilter;
		ar << resourceFileName;
		ar << resourceExtension;
	}
	else
	{
		ar >> m_Version;
		ar >> resource;
		ar >> resourcePathName;
		ar >> resourceType;
		ar >> resourceFilter;
		ar >> resourceFileName;
		ar >> resourceExtension;
		m_Resource          = resource;
		m_ResourcePathName  = resourcePathName;
		m_ResourceType      = static_cast<LBDirectoryItemTypes>(resourceType);
		m_ResourceFilter    = resourceFilter;
		m_ResourceFileName  = resourceFileName;
		m_ResourceExtension = resourceExtension;
	}
}