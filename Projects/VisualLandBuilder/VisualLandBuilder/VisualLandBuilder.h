// VisualLandBuilder.h : main header file for the VisualLandBuilder application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols

#include ".\CSplashDlg.h"
#include ".\CSplashLicenseDlg.h"

// CVisualLandBuilderApp:
// See VisualLandBuilder.cpp for the implementation of this class
//

class CVisualLandBuilderApp : public CWinApp
{
public:
	CVisualLandBuilderApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();

	DECLARE_MESSAGE_MAP()
};

extern CVisualLandBuilderApp theApp;