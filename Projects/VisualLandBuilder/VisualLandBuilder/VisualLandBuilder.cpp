// VisualLandBuilder.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\MainFrm.h"

#include ".\External\TransparentStatic\TransparentStatic.h"

#include ".\VisualLandBuilderDoc.h"
#include ".\VisualLandBuilderView.h"
#include ".\CAboutVBSDlg.h"
#include "afxwin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CVisualLandBuilderApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CVisualLandBuilderApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

// CVisualLandBuilderApp construction

CVisualLandBuilderApp::CVisualLandBuilderApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// -------------------------------------------------------------------------- //
// The one and only CVisualLandBuilderApp object

CVisualLandBuilderApp theApp;

// -------------------------------------------------------------------------- //

// CVisualLandBuilderApp initialization

BOOL CVisualLandBuilderApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Bohemia Interactive Studio"));
	LoadStdProfileSettings(4);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views

#ifdef __ONLY_FOR_VBS__
	int agreed = theApp.GetProfileIntA(_T("License"), "Agreed", 0);

	if (agreed == 0)
	{
		AfxInitRichEdit2();
		CSplashLicenseDlg licenseDlg;
		if (licenseDlg.DoModal() != IDOK) 
		{
			return FALSE;
		}

		bool res = theApp.WriteProfileInt(_T("License"), "Agreed", 1);
		if (!res)
		{
			AfxMessageBox("Failed to write to registry", MB_OK | MB_ICONSTOP);
			return FALSE;
		}
	}
#else
	CSplashDlg::ShowSplashScreen(NULL);
#endif

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CVisualLandBuilderDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CVisualLandBuilderView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);


	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();
	return TRUE;
}

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
private:
	// the version static ctrl
	CTransparentStatic m_VersionStaticCtrl;
};

// -------------------------------------------------------------------------- //

CAboutDlg::CAboutDlg() 
: CDialog(CAboutDlg::IDD)
{
}

// -------------------------------------------------------------------------- //

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_VERSION, m_VersionStaticCtrl);
}
// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

// App command to run the dialog
void CVisualLandBuilderApp::OnAppAbout()
{
#ifdef __ONLY_FOR_VBS__
	CAboutVBSDlg aboutDlg;
#else
	CAboutDlg aboutDlg;
#endif

	aboutDlg.DoModal();
}

// -------------------------------------------------------------------------- //
