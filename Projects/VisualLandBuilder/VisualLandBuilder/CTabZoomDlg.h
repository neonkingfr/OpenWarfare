#pragma once
#include "afxwin.h"

#include ".\External\ColorStatic\ColorStaticST.h"

// support for XP style bitmap button
#include ".\External\CHoverBitmapButton\theme.h"
#include ".\External\CHoverBitmapButton\HoverBitmapButton.h"

// CTabZoomDlg dialog

class CTabZoomDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabZoomDlg)

public:
	CTabZoomDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabZoomDlg();

// Dialog Data
	enum { IDD = IDD_TABPREVZOOM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	// theme for XP Style bitmap button
	CTheme m_Theme;

	// the buttons
	CHoverBitmapButton m_PanBtn;
	CHoverBitmapButton m_ZoomWinBtn;
	CHoverBitmapButton m_ZoomPlusBtn;
	CHoverBitmapButton m_ZoomMinusBtn;
	CHoverBitmapButton m_ZoomExtBtn;
	CColorStaticST     m_PanLabel;
	CColorStaticST     m_ZoomWindowLabel;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedPan();
	afx_msg void OnBnClickedZoomWin();
	afx_msg void OnBnClickedZoomPlus();
	afx_msg void OnBnClickedZoomMinus();
	afx_msg void OnBnClickedZoomExt();

public:
	void SetButtonsState(double zoomFactor, bool isPanning, bool isZoomingWindow);

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	// support for XP style bitmap buttons
	LRESULT OnThemeChanged();
};
