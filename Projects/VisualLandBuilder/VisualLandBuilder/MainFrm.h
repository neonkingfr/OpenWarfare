// MainFrm.h : interface of the CMainFrame class
//

#pragma once

class CMainFrame : public CFrameWnd
{
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CToolBar    m_wndMapToolBar;
	CToolBar    m_wndModuleToolBar;
	CToolBar    m_wndLandBuilderToolBar;
	CToolBar    m_wndSetupToolBar;
	CToolBar    m_wndHelpToolBar;

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);


	DECLARE_MESSAGE_MAP()

private:
	void DockControlBarLeftOf(CToolBar* Bar, CToolBar* LeftOf);

public:
	afx_msg void OnViewToolbarModule();
	afx_msg void OnViewToolbarMap();
	afx_msg void OnViewToolbarLandBuilder();
	afx_msg void OnViewToolbarSetup();
	afx_msg void OnViewToolbarHelp();
	afx_msg void OnUpdateViewToolbarModule(CCmdUI *pCmdUI);
	afx_msg void OnUpdateViewToolbarMap(CCmdUI *pCmdUI);
	afx_msg void OnUpdateViewToolbarLandBuilder(CCmdUI *pCmdUI);
	afx_msg void OnUpdateViewToolbarSetup(CCmdUI *pCmdUI);
	afx_msg void OnUpdateViewToolbarHelp(CCmdUI *pCmdUI);
};


