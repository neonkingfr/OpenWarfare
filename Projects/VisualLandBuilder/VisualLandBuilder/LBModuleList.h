#pragma once

#include <string>

#include ".\LBModule.h"

using std::string;

class LBModuleList : public CObject
{
	DECLARE_SERIAL(LBModuleList)

	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the version
	int m_Version;

	// the list of modules
	CTypedPtrList<CObList, LBModule*> m_List;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBModuleList();

	// copy constructor
	LBModuleList(const LBModuleList& other);

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~LBModuleList();

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBModuleList& operator = (const LBModuleList& other);

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //

	// GetList()
	// returns the list
	CTypedPtrList<CObList, LBModule*>& GetList();

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //

	// SetList()
	// sets this list with the given one
	void SetList(CTypedPtrList<CObList, LBModule*>& newList);

	// ************************************************************** //
	// Member variables manipulators                                  //
	// ************************************************************** //
	
	// Append()
	// adds a copy of the the given module at the end of this list
	void Append(const LBModule& module);
	void Append(LBModule* module);

	// GetAt()
	// returns the module of this list at the given index 
	LBModule* GetAt(int index);

	// SetAt()
	// sets the module of this list at the given index with the given module
	void SetAt(int index, LBModule* module);

	// RemoveAt()
	// removes the module at the given index from this list
	void RemoveAt(int index);

	// RemoveAll()
	// removes all the modules from this list
	void RemoveAll();

	// ************************************************************** //
	// Interface                                                      //
	// ************************************************************** //

	// GetCount()
	// returns the number of items in this list
	INT_PTR GetCount() const;

	// IsEmpty()
	// returns true if the list is empty
	BOOL IsEmpty() const;

	// UsesDems()
	// returns true if at least one module of this list uses dems
	bool UsesDems() const;

	// UsesShapefiles()
	// returns true if at least one module of this list uses shapefiles
	bool UsesShapefiles() const;

	// ExportsTerrain()
	// returns true if at least one module of this list exports terrain
	bool ExportsTerrain() const;

	// GetActiveModule()
	// returns a pointer to the active module of this list with the given position, if any.
	// position: 1 for 1st, 2 for 2nd and so on.
	// if not found, returns NULL
	LBModule* GetActiveModule(int position) const;

	// GetLastActiveModuleIndex()
	// returns the index of the last active module of this list if any
	// if all modules are inactive returns -1
	int GetLastActiveModuleIndex() const;

	// GetActiveModuleCount()
	// returns the number of active modules of this list having the given name
	int GetActiveModuleCount(string moduleName) const;

	// GetActiveModuleUsingShapefileCount()
	// returns the number of active modules using shapefile of this list
	int GetActiveModuleUsingShapefileCount() const;

	// GetActiveModulesCount()
	// returns the number of active items in this list
	int GetActiveModulesCount() const;

	// ************************************************************** //
	// Serialization                                                  //
	// ************************************************************** //
	virtual void Serialize(CArchive& ar);
};
