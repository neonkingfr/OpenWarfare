//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include ".\LBParameter.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

class LBParameterList : public CObject
{
	DECLARE_SERIAL( LBParameterList )

protected:
	// the version
	int m_version;

	// the list of parameters
	CTypedPtrList< CObList, LBParameter* > m_list;

public:
	// Default constructor
	LBParameterList();

	// copy constructor
	LBParameterList( const LBParameterList& other );

	virtual ~LBParameterList();

	LBParameterList& operator = ( const LBParameterList& other );

	// returns the list
	CTypedPtrList< CObList, LBParameter* >& GetList();

	// sets the list with the given one
	void SetList( CTypedPtrList< CObList, LBParameter* >& list );

	// adds a copy of the the given parameter at the end of this list
	void Append( const LBParameter& parameter );
	void Append( LBParameter* parameter );

	// returns the parameter of this list at the given index 
	LBParameter* GetAt( int index );
	const LBParameter* GetAt( int index ) const;

	// sets the parameter of this list at the given index with the given parameter
	void SetAt( int index, LBParameter* parameter );

	// sets the all the values of the parameters of this list with the given one
	void SetAllValues( const string& value );

	// removes the parameter at the given index from this list
	void RemoveAt( int index );

	// removes all the parameters from this list
	void RemoveAll();

	// sets all the value fields of the parameters of this list to the
	// empty string
	void Clear();

	// returns the number of items in this list
	INT_PTR GetCount() const;

	// returns true if all the parameters in the list with no default
	// have a value
	bool Validate() const;

	// returns true if at least one parameter in the list has a valid value  
	bool HasSomeValue() const;

	// returns true if the list is empty
	BOOL IsEmpty() const;

	// returns the value associated to the given parameter
	string GetValue( const string& parameter ) const;

	virtual void Serialize( CArchive& ar );
};

//-----------------------------------------------------------------------------
