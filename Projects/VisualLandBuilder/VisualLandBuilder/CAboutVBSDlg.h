#pragma once


// CAboutVBSDlg dialog

class CAboutVBSDlg : public CDialog
{
	DECLARE_DYNAMIC(CAboutVBSDlg)

public:
	CAboutVBSDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAboutVBSDlg();

// Dialog Data
	enum { IDD = IDD_ABOUT_VBS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
