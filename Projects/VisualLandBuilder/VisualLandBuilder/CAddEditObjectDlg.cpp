// CAddEditObject.cpp : implementation file
//

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CAddEditObjectDlg.h"
#include ".\LBGlobalVariables.h"

#include "..\..\LandBuilder\HighMapLoaders\include\EtHelperFunctions.h"

// CAddEditObjectDlg dialog

IMPLEMENT_DYNAMIC(CAddEditObjectDlg, CDialog)

// -------------------------------------------------------------------------- //

CAddEditObjectDlg::CAddEditObjectDlg(CWnd* pParent /*=NULL*/)
: CDialog(CAddEditObjectDlg::IDD, pParent)
, m_Modified(false)
{
}

// -------------------------------------------------------------------------- //

CAddEditObjectDlg::~CAddEditObjectDlg()
{
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OBJECTPARAMETERSLIST, m_ParametersListCtrl);
	DDX_Control(pDX, IDC_OBJECTNAME, m_NameCtrl);
	DDX_Control(pDX, IDC_OBJECTDESCRIPTION, m_DescriptionCtrl);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CAddEditObjectDlg, CDialog)
	ON_EN_CHANGE(IDC_OBJECTNAME, &CAddEditObjectDlg::OnEnChangeName)
	ON_NOTIFY(NM_CLICK, IDC_OBJECTPARAMETERSLIST, &CAddEditObjectDlg::OnNMClickParametersList)
	ON_BN_CLICKED(IDC_OBJECTEDIT, &CAddEditObjectDlg::OnBnClickedEdit)
	ON_BN_CLICKED(IDOK, &CAddEditObjectDlg::OnBnClickedOk)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_OBJECTPARAMETERSLIST, &CAddEditObjectDlg::OnLvnItemActivateParametersList)
	ON_NOTIFY(LVN_KEYDOWN, IDC_OBJECTPARAMETERSLIST, &CAddEditObjectDlg::OnLvnKeydownParametersList)
	ON_BN_CLICKED(IDC_OBJECTFROMDBF, &CAddEditObjectDlg::OnBnClickedObjectFromDbf)
	ON_BN_CLICKED(IDC_MINUS_BTN, &CAddEditObjectDlg::OnBnClickedMinusBtn)
	ON_BN_CLICKED(IDC_PLUS_BTN, &CAddEditObjectDlg::OnBnClickedPlusBtn)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CAddEditObjectDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	// sets the object list control column header
	m_ParametersListCtrl.InsertColumn(0, string(HEAD_NAME).c_str(), LVCFMT_LEFT);
	m_ParametersListCtrl.SetColumnWidth(0, 80);
	m_ParametersListCtrl.InsertColumn(1, string(HEAD_VALUE).c_str(), LVCFMT_LEFT);
	m_ParametersListCtrl.SetColumnWidth(1, 150);

	// sets the object list control style
	m_ParametersListCtrl.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	// sets the in-place editing object for the list 
	m_ParametersListCtrl.SetEditType(ET_EDIT);

	// sets the name control
	m_NameCtrl.SetWindowTextA(m_Object.GetName().c_str());

	// sets the list control
	UpdateParametersListCtrl();

	// disables controls if the object data are from dbf
	if(m_Object.GetName() == OBJ_NAME_FROMDBF)
	{
		m_NameCtrl.EnableWindow(FALSE);
		m_ParametersListCtrl.EnableWindow(FALSE);

		// hides edit btn
		GetDlgItem(IDC_OBJECTEDIT)->ShowWindow(FALSE);
		// shows plus and minus btn
		if(m_pModule->GetUseObjects() == LBMUO_MULTIPLE)
		{
			GetDlgItem(IDC_MINUS_BTN)->ShowWindow(TRUE);
			GetDlgItem(IDC_PLUS_BTN)->ShowWindow(TRUE);
		}
	}

	return TRUE;
}

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::UpdateParametersListCtrl()
{
	bool firstTime = false;

	INT_PTR listCount = m_Object.GetParametersList().GetCount();
	int listCtrlCount = m_ParametersListCtrl.GetItemCount();

	// if the counts of item are different it means that this is the first 
	// time we enter this function
	if(listCount != listCtrlCount) firstTime = true;

	INT_PTR objParCount = m_Object.GetParametersList().GetCount();
	for(int i = 0; i < objParCount; ++i)
	{
		LBParameter* item = m_Object.GetParametersList().GetAt(i);
		if(firstTime)
		{
			// sets first column
			m_ParametersListCtrl.InsertItem(i, item->GetName().c_str());
		}
		// if the parameter has no default, show it bolded
		if(!item->GetHasDefault())
		{
			m_ParametersListCtrl.SetItemStyle(i, 0, LIS_BOLD, true);
		}

		// modify the second column only if the data are changed
		string newValue = item->GetValue();
		CString oldValue = m_ParametersListCtrl.GetItemText(i, 1);
		
		if(newValue != string(oldValue))
		{
			// sets second column
			m_ParametersListCtrl.SetItem(i, 1, LVIF_TEXT, newValue.c_str(), 0, 0, 0, 0);
			
			// if data are changed update m_Modified
			if(!firstTime) m_Modified = true;
		}
	}

	// if the list is empty, disables it
	if(listCount == 0)
	{
		m_ParametersListCtrl.EnableWindow(FALSE);
	}

	// now check if the ok button can be enabled
	CheckOkButtonEnabling();
}

// -------------------------------------------------------------------------- //

bool CAddEditObjectDlg::ValidateParamCtrlInput(CString data, LBParameter* par)
{
	switch(par->GetType())
	{
	case LBPT_INTEGER:
		{
			if(!IsInteger(string(data), true))
			{
				AfxGetMainWnd()->MessageBox(string(MSG_MUSTBEINTEGER).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
				return false;
			}
			else
			{
				return true;
			}
		}
	case LBPT_FLOAT:
		{
			if(!IsFloatingPoint(string(data), true))
			{
				AfxGetMainWnd()->MessageBox(string(MSG_MUSTBEFLOATING).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
				return false;
			}
			else
			{
				return true;
			}
		}
	case LBPT_BOOL:
		{
			if(!IsBool(string(data), true))
			{
				AfxGetMainWnd()->MessageBox(string(MSG_MUSTBEZEROORONE).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
				return false;
			}
			else
			{
				return true;
			}
		}
	case LBPT_STRING: return true;
	default:
		AfxGetMainWnd()->MessageBox(string(MSG_DATANOTRECOGNIZED).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
		return false;
	}
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::CheckOkButtonEnabling()
{
	CString data;
	m_NameCtrl.GetWindowTextA(data);

	if(data.GetLength() == 0)
	{
		GetDlgItem(IDOK)->EnableWindow(FALSE);
		return;
	}

	string strData = data;

	if(strData != OBJ_NAME_FROMDBF)
	{
		if(!m_Object.GetParametersList().Validate())
		{
			GetDlgItem(IDOK)->EnableWindow(FALSE);
			return;
		}
	}

	GetDlgItem(IDOK)->EnableWindow(TRUE);
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::SetObject(LBObject* object)
{
	// sets the working copy of the data
	m_Object = *object;

	// sets the pointer to the original data
	m_pOldObject = object;
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::SetModule(LBModule* module)
{
	m_pModule = module;
}

// -------------------------------------------------------------------------- //
// EDIT HANDLERS                                                              //
// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::OnEnChangeName()
{
	m_Modified = true;

	// now check if the ok button can be enabled
	CheckOkButtonEnabling();
}

// -------------------------------------------------------------------------- //
// LIST HANDLERS                                                              //
// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::OnNMClickParametersList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// gets the index of the selected item in the list
	int selItem = m_ParametersListCtrl.GetNextItem(-1, LVNI_SELECTED);

	// enable/disable change button
	if(selItem != -1)
	{
		LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
		if(pLVKeyDow->wVKey == VK_UP) selItem--;
		if(pLVKeyDow->wVKey == VK_DOWN) selItem++;
		if(selItem < 0) selItem = 0;
		if(selItem > m_ParametersListCtrl.GetItemCount() - 1) selItem = m_ParametersListCtrl.GetItemCount() - 1;

		// something is selected
		GetDlgItem(IDC_OBJECTEDIT)->EnableWindow(TRUE);
		if(m_ParametersListCtrl.GetValidationPassed())
		{
			m_DescriptionCtrl.SetWindowTextA(m_Object.GetParametersList().GetAt(selItem)->GetDescription().c_str());
		}
	}
	else
	{
		// no selection
		GetDlgItem(IDC_OBJECTEDIT)->EnableWindow(FALSE);
		m_DescriptionCtrl.SetWindowTextA("");
	}

	*pResult = 0;
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::OnLvnItemActivateParametersList(NMHDR *pNMHDR, LRESULT *pResult)
{
	OnBnClickedEdit();

	*pResult = 0;
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::OnLvnKeydownParametersList(NMHDR *pNMHDR, LRESULT *pResult)
{
	OnNMClickParametersList(pNMHDR, pResult);
	
	*pResult = 0;
}

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                              //
// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::OnBnClickedEdit()
{
	// gets the index of the selected item in the list
	int selItem = m_ParametersListCtrl.GetNextItem(-1, LVNI_SELECTED);

	// sets a copy of the parameter
	LBParameter* par = new LBParameter(m_Object.GetParametersList().GetAt(selItem));

	// passes the copy to the dialog
	m_EditParameterDlg.SetParameter(par);
	if(m_EditParameterDlg.DoModal() == IDOK)
	{
		m_Object.GetParametersList().SetAt(selItem, par);
		UpdateParametersListCtrl();
	}
	else
	{
		delete par;
	}
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::OnBnClickedOk()
{
	// update data if modified
	if(m_Modified)
	{
		// gets data from dialog
		CString data;
		m_NameCtrl.GetWindowTextA(data);
		m_Object.SetName(data);

		// updates data
		*m_pOldObject = m_Object;
	}
	OnOK();
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::OnBnClickedObjectFromDbf()
{
	// sets the name
	m_Object.SetName(OBJ_NAME_FROMDBF);
	m_NameCtrl.SetWindowTextA(m_Object.GetName().c_str());

	// clears and disables the and the list and the list ctrl
	int index = m_pModule->GetObjectsList().GetFirstFreeDbfIndex();
	CString cIndex;
	cIndex.Format("%d", index);
	string strIndex = cIndex;
	m_Object.GetParametersList().SetAllValues(strIndex);
	UpdateParametersListCtrl();
	m_ParametersListCtrl.EnableWindow(FALSE);

	// disables the edit ctrl
	m_NameCtrl.EnableWindow(FALSE);

	// hides edit btn
	GetDlgItem(IDC_OBJECTEDIT)->ShowWindow(FALSE);
	// shows plus and minus btn
	if(m_pModule->GetUseObjects() == LBMUO_MULTIPLE)
	{
		GetDlgItem(IDC_MINUS_BTN)->ShowWindow(TRUE);
		GetDlgItem(IDC_PLUS_BTN)->ShowWindow(TRUE);
	}
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::OnBnClickedMinusBtn()
{
	CString data = m_ParametersListCtrl.GetItemText(0, 1);
	string strData = data;
	int curIndex = atoi(strData.c_str());
	curIndex--;
	if(curIndex == 0) curIndex = 1;
	CString cIndex;
	cIndex.Format("%d", curIndex);
	string strIndex = cIndex;
	m_Object.GetParametersList().SetAllValues(strIndex);
	UpdateParametersListCtrl();
}

// -------------------------------------------------------------------------- //

void CAddEditObjectDlg::OnBnClickedPlusBtn()
{
	CString data = m_ParametersListCtrl.GetItemText(0, 1);
	string strData = data;
	int curIndex = atoi(strData.c_str());
	curIndex++;
	CString cIndex;
	cIndex.Format("%d", curIndex);
	string strIndex = cIndex;
	m_Object.GetParametersList().SetAllValues(strIndex);
	UpdateParametersListCtrl();
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CAddEditObjectDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == MY_ENDEDITMSG)
	{
		// gets the index of the selected item in the list
		int selItem = static_cast<int>(pMsg->wParam);

		LBParameter* par = m_Object.GetParametersList().GetAt(selItem);
		CString data = m_ParametersListCtrl.GetItemText(selItem, 1);

		if(ValidateParamCtrlInput(data, par))
		{
			par->SetValue(data);
			m_Object.GetParametersList().SetAt(selItem, par);
			m_ParametersListCtrl.SetValidationPassed(true);
			UpdateParametersListCtrl();
		}
		else
		{
			m_ParametersListCtrl.SetValidationPassed(false);
			m_ParametersListCtrl.SetItemText(selItem, 1, par->GetValue().c_str());
			m_ParametersListCtrl.SetItemState(selItem, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
			m_ParametersListCtrl.SetFocus();
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}


