//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include ".\LBParameterList.h"
#include ".\LBError.h"

#include "..\..\LandBuilder\plugins\RandomPlacers\AdvancedRandomPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\AdvancedRandomPlacerSlope.h"
#include "..\..\LandBuilder\plugins\GenericPlacers\BestFitPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\BiasedRandomPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\BiasedRandomPlacerSlope.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\BoundarySubSquaresFramePlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\FramedForestPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\InnerSubSquaresFramePlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\RandomOnPathPlacer.h"
#include "..\..\LandBuilder\plugins\GenericPlacers\RegularOnPathPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\SimpleRandomPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\SimpleRandomPlacerSlope.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\SubSquaresRandomPlacer.h"

#include ".\LBGlobalVariables.h"
#include ".\LBDbValuesFunctor.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

class LBObject : public CObject
{
	DECLARE_SERIAL( LBObject )

	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the version
	int m_Version;
	// the object name
	string m_Name;
	// the object list of parameters
	LBParameterList m_ParametersList;

	// variables for preview
	// the fill color of the object
	LBPreviewColors m_PvFillColor;
	// the color of the border of the object
	LBPreviewColors m_PvBorderColor;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBObject();

	// Constructs this parameter item with the given parameters
	LBObject( const string& name );
	LBObject( const string& name, const LBParameterList& parametersList );
	LBObject( LBObject* other );

	// copy constructor
	LBObject( const LBObject& other );

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~LBObject();

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBObject& operator = ( const LBObject& other );

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //
	
	// GetName()
	// returns the name of this object
	string GetName() const;

	// GetParameterList()
	// returns the parameter list of this object
	LBParameterList& GetParametersList();

	// GetPvFillColor()
	// returns the fill color of this object
	LBPreviewColors GetPvFillColor() const;

	// GetPvFillColorAsString()
	// returns the fill color of this object as a string number
	string GetPvFillColorAsString() const;

	// GetPvBorderColor()
	// returns the color of the border of this object
	LBPreviewColors GetPvBorderColor() const;

	// GetPvBorderColorAsString()
	// returns the color of the border of this object as a string number
	string GetPvBorderColorAsString() const;

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //
	
	// SetName()
	// sets the name of this object with the given one
	void SetName( const string& newName );

	// SetName()
	// sets the name of this object with the given one
	void SetName( const CString& newName );

	// SetParameterList()
	// sets the parameter list of this object with the given one
	void SetParameterList( const LBParameterList& parametersList );

	// SetPvFillColor()
	// sets the fill color of this object
	void SetPvFillColor( LBPreviewColors newPvFillColor );

	// SetPvBorderColor()
	// sets the color of the border of this object
	void SetPvBorderColor( LBPreviewColors newPvBorderColor );

	// ************************************************************** //
	// Interface                                                      //
	// ************************************************************** //
	
	// Validate()
	// returns the result from object validation
	// LBET_NOERROR if the object can be exported to LandBuilder
	LBErrorTypes Validate();

	// AdvancedRandomPlacerObjectInput()
	// returns this object as input for the AdvancedRandomPlacer module
	LandBuildInt::Modules::AdvancedRandomPlacer::ObjectInput AdvancedRandomPlacerObjectInput();
	LandBuildInt::Modules::AdvancedRandomPlacer::ObjectInput AdvancedRandomPlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// AdvancedRandomPlacerSlopeObjectInput()
	// returns this object as input for the AdvancedRandomPlacerSlope module
	LandBuildInt::Modules::AdvancedRandomPlacerSlope::ObjectInput AdvancedRandomPlacerSlopeObjectInput();
	LandBuildInt::Modules::AdvancedRandomPlacerSlope::ObjectInput AdvancedRandomPlacerSlopeObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// RandomOnPathPlacerObjectInput()
	// returns this object as input for the RandomOnPathPlacer module
	LandBuildInt::Modules::RandomOnPathPlacer::ObjectInput RandomOnPathPlacerObjectInput();
	LandBuildInt::Modules::RandomOnPathPlacer::ObjectInput RandomOnPathPlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// BiasedRandomPlacerObjectInput()
	// returns this object as input for the BiasedRandomPlacer module
	LandBuildInt::Modules::BiasedRandomPlacer::ObjectInput BiasedRandomPlacerObjectInput();
	LandBuildInt::Modules::BiasedRandomPlacer::ObjectInput BiasedRandomPlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// BiasedRandomPlacerSlopeObjectInput()
	// returns this object as input for the BiasedRandomPlacerSlope module
	LandBuildInt::Modules::BiasedRandomPlacerSlope::ObjectInput BiasedRandomPlacerSlopeObjectInput();
	LandBuildInt::Modules::BiasedRandomPlacerSlope::ObjectInput BiasedRandomPlacerSlopeObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// SimpleRandomPlacerObjectInput()
	// returns this object as input for the SimpleRandomPlacer module
	LandBuildInt::Modules::SimpleRandomPlacer::ObjectInput SimpleRandomPlacerObjectInput();
	LandBuildInt::Modules::SimpleRandomPlacer::ObjectInput SimpleRandomPlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// SimpleRandomPlacerSlopeObjectInput()
	// returns this object as input for the SimpleRandomPlacerSlope module
	LandBuildInt::Modules::SimpleRandomPlacerSlope::ObjectInput SimpleRandomPlacerSlopeObjectInput();
	LandBuildInt::Modules::SimpleRandomPlacerSlope::ObjectInput SimpleRandomPlacerSlopeObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// SubSquaresRandomPlacerObjectInput()
	// returns this object as input for the SubSquaresRandomPlacer module
	LandBuildInt::Modules::SubSquaresRandomPlacer::ObjectInput SubSquaresRandomPlacerObjectInput();
	LandBuildInt::Modules::SubSquaresRandomPlacer::ObjectInput SubSquaresRandomPlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// InnerSubSquaresFramePlacerObjectInput()
	// returns this object as input for the InnerSubSquaresFramePlacer module
	LandBuildInt::Modules::InnerSubSquaresFramePlacer::ObjectInput InnerSubSquaresFramePlacerObjectInput();
	LandBuildInt::Modules::InnerSubSquaresFramePlacer::ObjectInput InnerSubSquaresFramePlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// BoundarySubSquaresFramePlacerObjectInput()
	// returns this object as input for the BoundarySubSquaresFramePlacer module
	LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ObjectInput BoundarySubSquaresFramePlacerObjectInput();
	LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ObjectInput BoundarySubSquaresFramePlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// FramedForestPlacerObjectInput()
	// returns this object as input for the FramedForestPlacer module
	LandBuildInt::Modules::FramedForestPlacer::ObjectInput FramedForestPlacerObjectInput();
	LandBuildInt::Modules::FramedForestPlacer::ObjectInput FramedForestPlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// BestFitPlacerObjectInput()
	// returns this object as input for the BestFitPlacer module
	LandBuildInt::Modules::BestFitPlacer::ObjectInput BestFitPlacerObjectInput();
	LandBuildInt::Modules::BestFitPlacer::ObjectInput BestFitPlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// RegularOnPathPlacerObjectInput()
	// returns this object as input for the RegularOnPathPlacer module
	LandBuildInt::Modules::RegularOnPathPlacer::ObjectInput RegularOnPathPlacerObjectInput();
	LandBuildInt::Modules::RegularOnPathPlacer::ObjectInput RegularOnPathPlacerObjectInput( int shapeIndex, LBDbValuesFunctor& functor );

	// ************************************************************** //
	// Serialization                                                  //
	// ************************************************************** //
	virtual void Serialize( CArchive& ar );
};

//-----------------------------------------------------------------------------
