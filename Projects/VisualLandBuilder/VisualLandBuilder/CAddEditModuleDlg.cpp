//-----------------------------------------------------------------------------

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CAddEditModuleDlg.h"
#include ".\LBObject.h"

//-----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC( CAddEditModuleDlg, CDialog )

//-----------------------------------------------------------------------------

CAddEditModuleDlg::CAddEditModuleDlg( const LBVisitorMap* visitorMap, CWnd* parent /*=NULL*/ )
: CDialog( CAddEditModuleDlg::IDD, parent )
, m_visitorMap( visitorMap )
{
}

//-----------------------------------------------------------------------------

CAddEditModuleDlg::~CAddEditModuleDlg()
{
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::DoDataExchange( CDataExchange* dX )
{
	CDialog::DoDataExchange( dX );
	DDX_Control( dX, IDC_MODULELIST,  m_moduleListCtrl );
	DDX_Control( dX, IDC_DESCRIPTION, m_descriptionCtrl );
	DDX_Control( dX, IDC_PARAMTAB,    m_paramTabCtrl );
	DDX_Control( dX, IDC_STATELEGEND, m_image );
}

//-----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP( CAddEditModuleDlg, CDialog )
	ON_LBN_SELCHANGE( IDC_MODULELIST, &CAddEditModuleDlg::OnLbnSelChangeModuleList )
	ON_BN_CLICKED( IDC_CLEARALLDATABTN, &CAddEditModuleDlg::OnBnClickedClearAllDataBtn )	
	ON_BN_CLICKED( IDC_PREVIEWBTN,      &CAddEditModuleDlg::OnBnClickedPreviewBtn )	
	ON_BN_CLICKED( IDOK,                &CAddEditModuleDlg::OnBnClickedOkBtn )	
	ON_BN_CLICKED( IDC_SAVEBTN,         &CAddEditModuleDlg::OnBnClickedSaveBtn )	
	ON_BN_CLICKED( IDC_LOADBTN,         &CAddEditModuleDlg::OnBnClickedLoadBtn )	
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------

BOOL CAddEditModuleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// fills the module list control
	SetModuleListCtrl();

	// adds items to the param tabs
	m_paramTabCtrl.InsertItem( TCIF_TEXT | TCIF_IMAGE, TAB_OBJECTS, string( HEAD_OBJECTPAR ).c_str(), TS_NODATA, 0 );
	m_paramTabCtrl.InsertItem( TCIF_TEXT | TCIF_IMAGE, TAB_GLOBALS, string( HEAD_GLOBALPAR ).c_str(), TS_NODATA, 0 );
	m_paramTabCtrl.InsertItem( TCIF_TEXT | TCIF_IMAGE, TAB_DBFS,    string( HEAD_DBFPAR ).c_str(),    TS_NODATA, 0 );
	m_paramTabCtrl.InsertItem( TCIF_TEXT | TCIF_IMAGE, TAB_SHAPES,  string( HEAD_SHAPEFILE ).c_str(), TS_NODATA, 0 );
	m_paramTabCtrl.InsertItem( TCIF_TEXT | TCIF_IMAGE, TAB_DEMS,    string( HEAD_DEMFILES ).c_str(),  TS_NODATA, 0 );
	m_paramTabCtrl.InsertItem( TCIF_TEXT | TCIF_IMAGE, TAB_NOTES,   string( HEAD_NOTES ).c_str(),     TS_NODATA, 0 );

	m_paramTabCtrl.Init();

	// are we editing an existing module ?
	if ( m_module.GetVLBName() != "" )
	{
		// selects the module in the list
		m_moduleListCtrl.SelectString( -1, m_module.GetVLBName().c_str() );

		// this is one hack to allow to change the name of the module
		// if this has changed from a previous version
		// introduced in version 1.0.0.0 to allow to read
		// RoadsGenerator and BuildingsCreator
		// whose names has been converted to
		// "RoadsGenerator (Beta)" and "BuildingsCreator (Beta)")
		int curSel = m_moduleListCtrl.GetCurSel();
		CString curSelText;
		m_moduleListCtrl.GetText( curSel, curSelText );

		if ( m_module.GetVLBName() != (LPCTSTR)curSelText )
		{
			m_module.SetVLBName( (LPCTSTR)curSelText );
		}

		// if has shapefile, read it
		string name = m_module.GetShapeFileName();
		if ( name != "" )
		{
			// but first verifies if it really exists
			if ( GetFileAttributes( name.c_str() ) == INVALID_FILE_ATTRIBUTES )
			{
			}
			else
			{
				LoadShapeFile();
			}
		}
	}
	else
	{
		// selects first module in list
		m_moduleListCtrl.SetCurSel( 0 );
		m_module = LBModule::GetPredefinedModule( (LBPredefinedModules)( 1 ) );	
	}

	// updates description
	m_descriptionCtrl.SetWindowTextA( m_module.GetDescription().c_str() );

	// connect the module to the dialogs in the param tab
	m_objDlg = (CTabObjectDlg*)m_paramTabCtrl.GetDialog( IDD_TABOBJECT );
	m_gloDlg = (CTabGlobalDlg*)m_paramTabCtrl.GetDialog( IDD_TABGLOBAL );
	m_dbfDlg = (CTabDbfDlg*)m_paramTabCtrl.GetDialog( IDD_TABDBF );
	m_shaDlg = (CTabShapefileDlg*)m_paramTabCtrl.GetDialog( IDD_TABSHAPEFILE );
	m_demDlg = (CTabDemsDlg*)m_paramTabCtrl.GetDialog( IDD_TABDEMS );
	m_notDlg = (CTabNotesDlg*)m_paramTabCtrl.GetDialog( IDD_TABNOTES );

	m_objDlg->SetModule( &m_module );
	m_gloDlg->SetModule( &m_module );
	m_dbfDlg->SetModule( &m_module );
	m_shaDlg->SetModule( &m_module );
	m_demDlg->SetModule( &m_module );
	m_notDlg->SetModule( &m_module );

	m_demDlg->SetVisitorMap( m_visitorMap );   

	// update the dialogs in the param tab
	UpdateObjectTabDialog( true );
	UpdateGlobalTabDialog();
	UpdateDbfTabDialog( false );
	UpdateShapeTabDialog();
	UpdateDemsTabDialog();
	UpdateNotesTabDialog();

	return (TRUE);
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateModuleListCtrlState()
{
	if ( (m_objDlg->GetListItemCount() > 0) ||
	     (!m_module.GetGlobalParametersList().IsEmpty() && m_gloDlg->HasData()) ||
	     (!m_module.GetDbfParametersList().IsEmpty() && m_dbfDlg->HasData()) ||
	     (m_module.GetUseShapefile() && m_shaDlg->HasData()) ||
	     (m_demDlg->GetListItemCount() > 0) ||
	     (m_notDlg->HasData()) )
	{
		m_moduleListCtrl.EnableWindow( FALSE );
	}
	else
	{
		m_moduleListCtrl.EnableWindow( TRUE );
	}
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateClearAllDataBtnState()
{
	if ( m_objDlg->GetClearButtonState() || 
	     m_gloDlg->GetClearButtonState() ||
	     m_dbfDlg->GetClearButtonState() || 
	     m_shaDlg->GetClearButtonState() ||
	     m_demDlg->GetClearButtonState() ||
	     m_notDlg->GetClearButtonState() ) 
	{
		GetDlgItem( IDC_CLEARALLDATABTN )->EnableWindow( TRUE );
	}
	else
	{
		GetDlgItem( IDC_CLEARALLDATABTN )->EnableWindow( FALSE );
	}
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateObjectTabDialog( bool header )
{
	m_objDlg->UpdateState( header );
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateGlobalTabDialog()
{
	m_gloDlg->UpdateState();
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateDbfTabDialog( bool reloadDbfFields )
{
	if ( reloadDbfFields )
	{
		m_dbfDlg->GetDbfFields();
	}
	m_dbfDlg->UpdateState();
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateShapeTabDialog()
{
	m_shaDlg->UpdateState();
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateDemsTabDialog()
{
	m_demDlg->UpdateState();
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateNotesTabDialog()
{
	m_notDlg->UpdateState();
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateObjectTabImage()
{
	TCITEM tcItem;
	tcItem.mask   = TCIF_IMAGE;
	tcItem.iImage = TS_NODATA;

	if ( m_module.GetUseObjects() != LBMUO_NONE )
	{
		if ( m_objDlg->GetListItemCount() > 0 )
		{
			tcItem.iImage = TS_DATAOK;
		}
		else
		{
			tcItem.iImage = TS_MISSINGDATA;
		}
	}

	m_paramTabCtrl.SetItem( TAB_OBJECTS, &tcItem );
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateGlobalTabImage()
{
	TCITEM tcItem;
	tcItem.mask   = TCIF_IMAGE;
	tcItem.iImage = TS_NODATA;

	if ( !m_module.GetGlobalParametersList().IsEmpty() )
	{
		if ( m_gloDlg->HasAllData() )
		{
			tcItem.iImage = TS_DATAOK;
		}
		else
		{
			tcItem.iImage = TS_MISSINGDATA;
		}
	}

	m_paramTabCtrl.SetItem( TAB_GLOBALS, &tcItem );
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateDbfTabImage()
{
	TCITEM tcItem;
	tcItem.mask   = TCIF_IMAGE;
	tcItem.iImage = TS_NODATA;

	if ( !m_module.GetDbfParametersList().IsEmpty() )
	{
		if ( m_dbfDlg->Validate() )
		{
			// if data are present but don't match the dbf
			if ( m_dbfDlg->DataMatchFields() == 0 )
			{
				tcItem.iImage = TS_WRONGDATA;
			}
			else
			{
				tcItem.iImage = TS_DATAOK;
			}
		}
		else
		{
			tcItem.iImage = TS_MISSINGDATA;
		}
	}

	m_paramTabCtrl.SetItem( TAB_DBFS, &tcItem );
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateShapeTabImage()
{
	TCITEM tcItem;
	tcItem.mask   = TCIF_IMAGE;
	tcItem.iImage = TS_NODATA;

	if ( m_module.GetUseShapefile() )
	{
		// a filename is specified
		if ( m_shaDlg->HasData() )
		{
			// the filename is incorrect
			if ( GetFileAttributes( m_module.GetShapeFileName().c_str() ) == INVALID_FILE_ATTRIBUTES )
			{
				tcItem.iImage = TS_WRONGDATA;
			}
			else
			{
				tcItem.iImage = TS_DATAOK;
			}
			// the module doesn't accept multipart shapes and the shapefile contains multipart shapes
			if ( !m_module.GetAcceptMultipartShapes() && GetMultipartShapesCount() > 0 )
			{
				tcItem.iImage = TS_WRONGDATA;
				m_shaDlg->SetMultipartShapesCtrlColor( true );
			}
			else
			{
				m_shaDlg->SetMultipartShapesCtrlColor( false );
			}
		}
		else
		{
			tcItem.iImage = TS_MISSINGDATA;
		}
	}
	m_paramTabCtrl.SetItem( TAB_SHAPES, &tcItem );
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateDemTabImage()
{
	TCITEM tcItem;
	tcItem.mask   = TCIF_IMAGE;
	tcItem.iImage = TS_NODATA;

	if ( m_module.GetUseDems() != LBMUD_NONE )
	{
		if ( m_demDlg->GetListItemCount() > 0 )
		{
			tcItem.iImage = TS_DATAOK;		                  
		}
		else
		{
			tcItem.iImage = TS_MISSINGDATA;
		}
	}
	m_paramTabCtrl.SetItem( TAB_DEMS, &tcItem );
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UpdateNotesTabImage()
{
	TCITEM tcItem;
	tcItem.mask   = TCIF_IMAGE;
	tcItem.iImage = TS_DATAOK;

	m_paramTabCtrl.SetItem( TAB_NOTES, &tcItem );
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::SetModuleListCtrl()
{
	for ( int i = LBPM_START_PREDEFINED_MODULES + 1; i < LBPM_END_PREDEFINED_MODULES; ++i )
	{
		LBModule lbM = LBModule::GetPredefinedModule( (LBPredefinedModules)i );

		m_moduleListCtrl.AddString( lbM.GetVLBName().c_str() );
	}
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::LoadShapeFile()
{
	UnloadShapeFile();
	string fileName = m_module.GetShapeFileName();
	if ( fileName.length() > 0 ) 
  {
    m_shapeOrganizer.ReadShapeFile( Pathname( fileName.c_str() ), 1 );
  }
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::UnloadShapeFile()
{
	m_shapeOrganizer.~ShapeOrganizer();
	new (&m_shapeOrganizer) Shapes::ShapeOrganizer();
}

//-----------------------------------------------------------------------------

string CAddEditModuleDlg::SelectedShapeType()
{
	Shapes::ShapeList& shapeList = GetShapeList();
  if ( shapeList.Size() == 0 ) { return (""); }

	Shapes::IShape* shape = shapeList.GetShape( 0 );
	RString type = GetShapeType( shape );

	return string(( type.Data() ));
}

//-----------------------------------------------------------------------------

Shapes::ShapeDatabase& CAddEditModuleDlg::GetShapeDatabase()
{
	return (m_shapeOrganizer.GetShapeDatabase());
}

//-----------------------------------------------------------------------------

Shapes::ShapeList& CAddEditModuleDlg::GetShapeList()
{
	return (m_shapeOrganizer.GetShapeList());
}

//-----------------------------------------------------------------------------

int CAddEditModuleDlg::GetMultipartShapesCount()
{
	Shapes::ShapeList& shapeList = GetShapeList();
	int shapesCount = shapeList.Size();
  if ( shapesCount == 0 ) { return (0); }
	
	int mpCounter = 0;
	for ( int i = 0; i < shapesCount; ++i )
	{
		Shapes::IShape* shape = shapeList.GetShape( i );
		if ( shape->GetPartCount() > 1 )
		{
			mpCounter++;
		}
	}

	return (mpCounter);
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::CheckOkButtonEnabling()
{
	if (
	    ((m_module.GetUseObjects() != LBMUO_NONE && m_objDlg->GetListItemCount() > 0) || (m_module.GetUseObjects() == LBMUO_NONE)) &&
	    ((!m_module.GetGlobalParametersList().IsEmpty() && m_gloDlg->HasAllData())    || (m_module.GetGlobalParametersList().IsEmpty())) &&
	    ((!m_module.GetDbfParametersList().IsEmpty() && m_dbfDlg->Validate())         || (m_module.GetDbfParametersList().IsEmpty())) &&
	    ((m_module.GetUseShapefile() && m_shaDlg->HasData())                          || (!m_module.GetUseShapefile())) &&
	    ((m_module.GetUseDems() != LBMUD_NONE && m_demDlg->GetListItemCount() > 0)    || (m_module.GetUseDems() == LBMUD_NONE))
      )
	{
		GetDlgItem( IDOK )->EnableWindow( TRUE );
		GetDlgItem( IDC_SAVEBTN )->EnableWindow( TRUE );		
		if ( m_module.GetHasPreview() )
		{
			GetDlgItem( IDC_PREVIEWBTN )->EnableWindow( TRUE );		
		}
		else
		{
			GetDlgItem( IDC_PREVIEWBTN )->EnableWindow( FALSE );		
		}
	}
	else
	{
		GetDlgItem( IDOK )->EnableWindow( FALSE );
		GetDlgItem( IDC_PREVIEWBTN )->EnableWindow( FALSE );		
		GetDlgItem( IDC_SAVEBTN )->EnableWindow( FALSE );		
	}
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::SetModule( LBModule* module )
{
	// sets the working copy of the data
	m_module = *module;

	// sets the pointer to the original data
	m_oldModule = module;
}

//-----------------------------------------------------------------------------

LBModule* CAddEditModuleDlg::GetModule()
{
	return (&m_module);
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::OnLbnSelChangeModuleList()
{
	// clears all module's object
	m_module.RemoveAllObjects();
	// clears all module's global parameters
	m_module.RemoveAllGlobalParameters();
	// clears all module's hidden global parameters
	m_module.RemoveAllHiddenGlobalParameters();
	// clears all module's dbf parameters
	m_module.RemoveAllDbfParameters();

	// gets the current selected predefined module
	int selected = m_moduleListCtrl.GetCurSel();
	m_module = LBModule::GetPredefinedModule( (LBPredefinedModules)(selected + 1) );	

	// updates description
	m_descriptionCtrl.SetWindowTextA( m_module.GetDescription().c_str() );

	// updates object tab dialog
	UpdateObjectTabDialog( true );
	// updates global tab dialog
	UpdateGlobalTabDialog();
	// updates global tab dialog
	UpdateDbfTabDialog( false );
	// updates shape tab dialog
	UpdateShapeTabDialog();
	// updates dems tab dialog
	UpdateDemsTabDialog();
	// updates notes tab dialog
	UpdateNotesTabDialog();
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::OnBnClickedClearAllDataBtn()
{
  if ( m_objDlg->GetClearButtonState() ) { m_objDlg->OnBnClickedTabObjClearBtn(); }
	if ( m_gloDlg->GetClearButtonState() ) { m_gloDlg->OnBnClickedTabGlobalClearBtn(); }
	if ( m_dbfDlg->GetClearButtonState() ) { m_dbfDlg->OnBnClickedTabDbfClearBtn(); }
	if ( m_shaDlg->GetClearButtonState() ) { m_shaDlg->OnBnClickedTabShapeClearBtn(); }
	if ( m_demDlg->GetClearButtonState() ) { m_demDlg->OnBnClickedTabDemsClearBtn(); }
	if ( m_notDlg->GetClearButtonState() ) { m_notDlg->OnBnClickedTabNotesClearBtn(); }
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::OnBnClickedPreviewBtn()
{
	m_moduleList.RemoveAll();
	m_moduleList.Append( m_module );

	m_previewDlg.SetModuleList( &m_moduleList, true, true );
	m_previewDlg.DoModal();

	m_module = *(m_moduleList.GetAt( 0 ));
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::OnBnClickedOkBtn()
{
	// update data if modified
	if ( m_objDlg->GetModified() || 
	     m_gloDlg->GetModified() ||
	     m_dbfDlg->GetModified() || 
	     m_shaDlg->GetModified() ||
	     m_demDlg->GetModified() || 
	     m_notDlg->GetModified() )
	{
		int selected = m_moduleListCtrl.GetCurSel();
		m_module.SetType( (LBModuleTypes)(selected - 1) );
		// updates data
		*m_oldModule = m_module;
	}
	OnOK();
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::OnBnClickedSaveBtn()
{
	string strFilter = "VLB Module (*." + EXT_MODULEFILE + ")|*." + EXT_MODULEFILE + "||";
	string strExt    = EXT_MODULEFILE;
	string strFile   = "";

	DWORD flags = OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_EXTENSIONDIFFERENT;
	CFileDialog fileDlg( FALSE, strExt.c_str(), strFile.c_str(), flags, strFilter.c_str(), this );

	if ( fileDlg.DoModal() == IDOK )
	{
		CFile outputFile;
		if ( !outputFile.Open( fileDlg.GetPathName(), CFile::modeCreate | CFile::modeWrite ) )
		{
			AfxGetMainWnd()->MessageBox( string( MSG_ERRORSAVINGMODULE ).c_str(), string( MSG_ERROR ).c_str(), 
                                   MB_OK | MB_ICONSTOP );
			return;
		}

		CArchive ar( &outputFile, CArchive::store );

		int selected = m_moduleListCtrl.GetCurSel();
		m_module.SetType( (LBModuleTypes)(selected - 1) );

		m_module.Serialize(ar);

		ar.Close();
	}
}

//-----------------------------------------------------------------------------

void CAddEditModuleDlg::OnBnClickedLoadBtn()
{
	string strFilter = "VLB Module (*." + EXT_MODULEFILE + ")|*." + EXT_MODULEFILE + "||";
	string strExt    = EXT_MODULEFILE;
	string strFile   = "";

	DWORD flags = OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_EXTENSIONDIFFERENT;
	CFileDialog fileDlg( TRUE, strExt.c_str(), strFile.c_str(), flags, strFilter.c_str(), this );

	if ( fileDlg.DoModal() == IDOK )
	{
		CFile inputFile;
		if ( !inputFile.Open( fileDlg.GetPathName(), CFile::modeRead ) )
		{
			AfxGetMainWnd()->MessageBox( string( MSG_ERRORLOADINGMODULE ).c_str(), string( MSG_ERROR ).c_str(), 
                                   MB_OK | MB_ICONSTOP );
			return;
		}

		CArchive ar( &inputFile, CArchive::load );

		LBModule tempModule;
		tempModule.Serialize( ar );

		m_module = tempModule;

		ar.Close();

		// selects the module in the list
		m_moduleListCtrl.SetCurSel( m_module.GetType() + 1 );

		// if has shapefile, read it
		string name = m_module.GetShapeFileName();
		if ( name != "" )
		{
			// but first verifies if it really exists
			if ( GetFileAttributes( name.c_str() ) == INVALID_FILE_ATTRIBUTES )
			{
			}
			else
			{
				LoadShapeFile();
			}
		}

		// updates description
		m_descriptionCtrl.SetWindowTextA( m_module.GetDescription().c_str() );

		// update the dialogs in the param tab
		UpdateObjectTabDialog( true );
		UpdateGlobalTabDialog();
		UpdateDbfTabDialog( false );
		UpdateShapeTabDialog();
		UpdateDemsTabDialog();
		UpdateNotesTabDialog();
	}
}

//-----------------------------------------------------------------------------
