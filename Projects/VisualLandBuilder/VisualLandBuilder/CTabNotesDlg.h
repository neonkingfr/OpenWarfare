#pragma once
#include "afxwin.h"

#include ".\LBModule.h"

// CTabNotesDlg dialog

class CTabNotesDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabNotesDlg)

public:
	CTabNotesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabNotesDlg();

// Dialog Data
	enum { IDD = IDD_TABNOTES };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

private:
	// pointer to the current module
	LBModule* m_pModule;

	// will be set to true if the data are modified
	bool m_Modified;

	// the edit control
	CEdit m_EditCtrl;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //
public:
	// updates the state (enabled/disabled) of this dialog
	void UpdateState();
	// returns true if data have been modified
	bool GetModified() const;
	// sets the module
	void SetModule(LBModule* module);
	// returns the state of the clear button (true if enabled)
	BOOL GetClearButtonState() const;
	// return true if there are data already inputted
	bool HasData() const;

// -------------------------------------------------------------------------- //
// EDIT CONTROL EVENT HANDLERS                                                //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnEnChangeTabNotesEdit();

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //
public:
	afx_msg void OnBnClickedTabNotesClearBtn();

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
