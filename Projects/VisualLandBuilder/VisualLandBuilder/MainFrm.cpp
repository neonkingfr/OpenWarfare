// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include ".\VisualLandBuilder.h"

#include ".\MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_VIEW_TOOLBARMODULE, &CMainFrame::OnViewToolbarModule)
	ON_COMMAND(ID_VIEW_TOOLBARMAP, &CMainFrame::OnViewToolbarMap)
	ON_COMMAND(ID_VIEW_TOOLBARLANDBUILDER, &CMainFrame::OnViewToolbarLandBuilder)
	ON_COMMAND(ID_VIEW_TOOLBARSETUP, &CMainFrame::OnViewToolbarSetup)
	ON_COMMAND(ID_VIEW_TOOLBARHELP, &CMainFrame::OnViewToolbarHelp)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TOOLBARMODULE, &CMainFrame::OnUpdateViewToolbarModule)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TOOLBARMAP, &CMainFrame::OnUpdateViewToolbarMap)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TOOLBARLANDBUILDER, &CMainFrame::OnUpdateViewToolbarLandBuilder)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TOOLBARSETUP, &CMainFrame::OnUpdateViewToolbarSetup)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TOOLBARHELP, &CMainFrame::OnUpdateViewToolbarHelp)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame construction/destruction

// -------------------------------------------------------------------------- //

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
}

// -------------------------------------------------------------------------- //

CMainFrame::~CMainFrame()
{
}

// -------------------------------------------------------------------------- //

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	m_wndToolBar.SetWindowTextA("File");

	if (!m_wndMapToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndMapToolBar.LoadToolBar(IDR_MAP))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	m_wndMapToolBar.SetWindowTextA("Map");

	if (!m_wndModuleToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndModuleToolBar.LoadToolBar(IDR_MODULE))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	m_wndModuleToolBar.SetWindowTextA("Module");

	if (!m_wndLandBuilderToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndLandBuilderToolBar.LoadToolBar(IDR_LANDBUILDER))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	m_wndLandBuilderToolBar.SetWindowTextA("Land Builder");

	if (!m_wndSetupToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndSetupToolBar.LoadToolBar(IDR_SETUP))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	m_wndSetupToolBar.SetWindowTextA("Tools");

	if (!m_wndHelpToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndHelpToolBar.LoadToolBar(IDR_HELP))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	m_wndHelpToolBar.SetWindowTextA("Help");

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	m_wndMapToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBarLeftOf(&m_wndMapToolBar, &m_wndToolBar);

	m_wndModuleToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBarLeftOf(&m_wndModuleToolBar, &m_wndMapToolBar);

	m_wndLandBuilderToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBarLeftOf(&m_wndLandBuilderToolBar, &m_wndModuleToolBar);

	m_wndSetupToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBarLeftOf(&m_wndSetupToolBar, &m_wndLandBuilderToolBar);

	m_wndHelpToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBarLeftOf(&m_wndHelpToolBar, &m_wndSetupToolBar);

	return 0;
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CFrameWnd::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	ShowWindow(SW_MAXIMIZE);
}

// -------------------------------------------------------------------------- //

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

// -------------------------------------------------------------------------- //

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

// -------------------------------------------------------------------------- //

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

// -------------------------------------------------------------------------- //

void CMainFrame::DockControlBarLeftOf(CToolBar* Bar, CToolBar* LeftOf)
{
	CRect rect;
	DWORD dw;
	UINT n;

	// get MFC to adjust the dimensions of all docked ToolBars
	// so that GetWindowRect will be accurate
	RecalcLayout(TRUE);

	LeftOf->GetWindowRect(&rect);
	rect.OffsetRect(1,0);
	dw=LeftOf->GetBarStyle();
	n = 0;
	n = (dw&CBRS_ALIGN_TOP) ? AFX_IDW_DOCKBAR_TOP : n;
	n = (dw&CBRS_ALIGN_BOTTOM && n==0) ? AFX_IDW_DOCKBAR_BOTTOM : n;
	n = (dw&CBRS_ALIGN_LEFT && n==0) ? AFX_IDW_DOCKBAR_LEFT : n;
	n = (dw&CBRS_ALIGN_RIGHT && n==0) ? AFX_IDW_DOCKBAR_RIGHT : n;

	// When we take the default parameters on rect, DockControlBar will dock
	// each Toolbar on a seperate line. By calculating a rectangle, we
	// are simulating a Toolbar being dragged to that location and docked.
	DockControlBar(Bar,n,&rect);
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnViewToolbarModule()
{
	if(m_wndModuleToolBar.IsVisible())
	{
		ShowControlBar(&m_wndModuleToolBar, FALSE, FALSE);
	}
	else
	{
		ShowControlBar(&m_wndModuleToolBar, TRUE, FALSE);
	}
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnViewToolbarMap()
{
	if(m_wndMapToolBar.IsVisible())
	{
		ShowControlBar(&m_wndMapToolBar, FALSE, FALSE);
	}
	else
	{
		ShowControlBar(&m_wndMapToolBar, TRUE, FALSE);
	}
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnViewToolbarLandBuilder()
{
	if(m_wndLandBuilderToolBar.IsVisible())
	{
		ShowControlBar(&m_wndLandBuilderToolBar, FALSE, FALSE);
	}
	else
	{
		ShowControlBar(&m_wndLandBuilderToolBar, TRUE, FALSE);
	}
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnViewToolbarSetup()
{
	if(m_wndSetupToolBar.IsVisible())
	{
		ShowControlBar(&m_wndSetupToolBar, FALSE, FALSE);
	}
	else
	{
		ShowControlBar(&m_wndSetupToolBar, TRUE, FALSE);
	}
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnViewToolbarHelp()
{
	if(m_wndHelpToolBar.IsVisible())
	{
		ShowControlBar(&m_wndHelpToolBar, FALSE, FALSE);
	}
	else
	{
		ShowControlBar(&m_wndHelpToolBar, TRUE, FALSE);
	}
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnUpdateViewToolbarModule(CCmdUI *pCmdUI)
{
	if(m_wndModuleToolBar.IsVisible())
	{
		pCmdUI->SetCheck(true);
	}
	else
	{
		pCmdUI->SetCheck(false);
	}
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnUpdateViewToolbarMap(CCmdUI *pCmdUI)
{
	if(m_wndMapToolBar.IsVisible())
	{
		pCmdUI->SetCheck(true);
	}
	else
	{
		pCmdUI->SetCheck(false);
	}
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnUpdateViewToolbarLandBuilder(CCmdUI *pCmdUI)
{
	if(m_wndLandBuilderToolBar.IsVisible())
	{
		pCmdUI->SetCheck(true);
	}
	else
	{
		pCmdUI->SetCheck(false);
	}
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnUpdateViewToolbarSetup(CCmdUI *pCmdUI)
{
	if(m_wndSetupToolBar.IsVisible())
	{
		pCmdUI->SetCheck(true);
	}
	else
	{
		pCmdUI->SetCheck(false);
	}
}

// -------------------------------------------------------------------------- //

void CMainFrame::OnUpdateViewToolbarHelp(CCmdUI *pCmdUI)
{
	if(m_wndHelpToolBar.IsVisible())
	{
		pCmdUI->SetCheck(true);
	}
	else
	{
		pCmdUI->SetCheck(false);
	}
}

// -------------------------------------------------------------------------- //

