//-----------------------------------------------------------------------------

#include "stdafx.h"

#include ".\LBParameterList.h"
#include ".\LBGlobalVariables.h"

//-----------------------------------------------------------------------------

IMPLEMENT_SERIAL( LBParameterList, CObject, 1 )

//-----------------------------------------------------------------------------

LBParameterList::LBParameterList() 
: CObject()
, m_version( VERSION_NUMBER )
{
}

//-----------------------------------------------------------------------------

LBParameterList::LBParameterList( const LBParameterList& other )
: m_version( other.m_version )
{
	// copies the new data
	POSITION pos = other.m_list.GetHeadPosition();
	while ( pos )
	{
		LBParameter* item = new LBParameter( other.m_list.GetNext( pos ) );
		m_list.AddTail( item );
	}
}

//-----------------------------------------------------------------------------

LBParameterList::~LBParameterList()
{
	RemoveAll();
}

//-----------------------------------------------------------------------------

LBParameterList& LBParameterList::operator = ( const LBParameterList& other )
{
	m_version = other.m_version;

	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = other.m_list.GetHeadPosition();
	while ( pos )
	{
		LBParameter* item = new LBParameter( other.m_list.GetNext( pos ) );
		m_list.AddTail( item );
	}

	return (*this);
}

//-----------------------------------------------------------------------------

CTypedPtrList< CObList, LBParameter* >& LBParameterList::GetList()
{
	return (m_list);
}

//-----------------------------------------------------------------------------

void LBParameterList::SetList( CTypedPtrList< CObList, LBParameter* >& list )
{
	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = list.GetHeadPosition();
	while ( pos )
	{
		m_list.AddTail( new LBParameter( list.GetNext( pos ) ) );
	}
}

//-----------------------------------------------------------------------------

void LBParameterList::Append( const LBParameter& parameter )
{
	LBParameter* item = new LBParameter( parameter );
	m_list.AddTail( item );
}

//-----------------------------------------------------------------------------

void LBParameterList::Append( LBParameter* parameter )
{
	m_list.AddTail( parameter );
}

//-----------------------------------------------------------------------------

LBParameter* LBParameterList::GetAt( int index )
{
	return (m_list.GetAt( m_list.FindIndex( index ) ));
}

//-----------------------------------------------------------------------------

const LBParameter* LBParameterList::GetAt( int index ) const
{
	return (m_list.GetAt( m_list.FindIndex( index ) ));
}

//-----------------------------------------------------------------------------

void LBParameterList::SetAt( int index, LBParameter* parameter )
{
	POSITION pos = m_list.FindIndex( index );
	LBParameter* oldPar = GetAt( index );
	m_list.SetAt( pos, new LBParameter( parameter ) );
	delete oldPar;
  if ( parameter != oldPar ) { delete parameter; }
}

//-----------------------------------------------------------------------------

void LBParameterList::SetAllValues( const string& value )
{
	POSITION pos = m_list.GetHeadPosition();
	while ( pos )
	{
		LBParameter* item = m_list.GetNext( pos );
		item->SetValue( value );
	}
}

//-----------------------------------------------------------------------------

void LBParameterList::RemoveAt( int index )
{
	CTypedPtrList< CObList, LBParameter* > tempList;

	// sets the copy of the list
	// discarding the item to remove
	POSITION pos = m_list.GetHeadPosition();
	int counter = 0;
	while ( pos )
	{
		LBParameter* item = m_list.GetNext( pos );
		if ( counter != index )
		{
			tempList.AddTail( new LBParameter( item ) );
		}
		counter++;
	}

	// sets the list to be the temp list
	SetList( tempList );

	// clears the temp list
	pos = tempList.GetHeadPosition();
	while ( pos )
	{
		delete tempList.GetNext( pos );
	}
	tempList.RemoveAll();
}

//-----------------------------------------------------------------------------

void LBParameterList::RemoveAll()
{
	// removes all items from the list
	POSITION pos = m_list.GetHeadPosition();
	while ( pos )
	{
		delete m_list.GetNext( pos );
	}
	m_list.RemoveAll();
}

//-----------------------------------------------------------------------------

void LBParameterList::Clear()
{
	// clear the data of the list
	POSITION pos = m_list.GetHeadPosition();
	while ( pos )
	{
		LBParameter* item = m_list.GetNext( pos );
		item->SetValue( string( "" ) );
	}
}

//-----------------------------------------------------------------------------

INT_PTR LBParameterList::GetCount() const
{
	return (m_list.GetCount());
}

//-----------------------------------------------------------------------------

bool LBParameterList::Validate() const
{
	// returns false if at least one item has no default and no value
	POSITION pos = m_list.GetHeadPosition();
	while ( pos )
	{
		LBParameter* item = m_list.GetNext( pos );
		if ( !item->GetHasDefault() && item->GetValue() == "" )
		{
			return (false);
		}
	}
	return (true);
}

//-----------------------------------------------------------------------------
	
bool LBParameterList::HasSomeValue() const
{
	POSITION pos = m_list.GetHeadPosition();
	while ( pos )
	{
		LBParameter* item = m_list.GetNext( pos );
		if ( item->GetValue() != "" )
		{
			return (true);
		}
	}
	return (false);
}

//-----------------------------------------------------------------------------

BOOL LBParameterList::IsEmpty() const
{
	return (m_list.IsEmpty());
}

//-----------------------------------------------------------------------------

string LBParameterList::GetValue( const string& parameter ) const
{
	POSITION pos = m_list.GetHeadPosition();
	while ( pos )
	{
		LBParameter* item = m_list.GetNext(pos);
		if ( item->GetName() == parameter )
		{
			return (item->GetValue());
		}
	}
	return ("");
}

//-----------------------------------------------------------------------------

void LBParameterList::Serialize( CArchive& ar )
{
	CObject::Serialize( ar );

	m_list.Serialize( ar );

	if ( ar.IsStoring() )
	{
		ar << m_version;
	}
	else
	{
		ar >> m_version;
	}
}

//-----------------------------------------------------------------------------
