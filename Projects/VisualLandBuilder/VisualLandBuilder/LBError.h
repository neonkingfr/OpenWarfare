#pragma once

#include <string>

using std::string;

// ******************** //
// Enum for error types //
// ******************** //
typedef enum
{
	LBET_NOERROR,
	LBET_SHAPEFILEFILENOTFOUND,
	LBET_DBFFILENOTFOUND,
	LBET_DEMFILENOTFOUND,
	LBET_MISSINGOBJECTNAME,
	LBET_MISSINGMAPPARAMETER,
	LBET_MISSINGOBJECTPARAMETER,
	LBET_MISSINGDEMPARAMETER,
	LBET_MISSINGGLOBALPARAMETER,
	LBET_MISSINGDBFPARAMETER,
	LBET_MISSINGDIRECTORY,
	LBET_NOACTIVEMODULES
} LBErrorTypes;

class LBError
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the type of error
	LBErrorTypes m_Type;
	// the index of the module triggering the error
	int m_ModuleIndex;
	// the error description
	string m_Description;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBError();

	// Constructs this error with the given parameters
	LBError(LBErrorTypes type, int moduleIndex, const string& description);

	// copy constructor
	LBError(const LBError& other);

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBError& operator = (const LBError& other);

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //
	
	// GetType()
	// returns the type of this error
	LBErrorTypes GetType() const;

	// GetModuleIndex()
	// returns the module index of this error
	int GetModuleIndex() const;

	// GetDescription()
	// returns the description of this error
	string GetDescription();

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //

	// SetType()
	// sets the type of this error
	void SetType(LBErrorTypes newType);

	// SetModuleIndex()
	// sets the module index of this error
	void SetModuleIndex(int newModuleIndex);

	// SetDescription()
	// sets the description of this error
	void SetDescription(const string& newDescription);
};
