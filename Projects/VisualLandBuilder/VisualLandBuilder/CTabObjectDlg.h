#pragma once

#include "afxcmn.h"
#include "resource.h"

#include ".\LBModule.h"
#include ".\CAddEditObjectDlg.h"
#include ".\ExtendedControls\CListCtrlEx.h"

// CTabObjectDlg dialog

class CTabObjectDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabObjectDlg)

public:
	CTabObjectDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabObjectDlg();

// Dialog Data
	enum { IDD = IDD_TABOBJECTDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
private:
	// the object list control
	CListCtrlEx m_ObjectsListCtrl;

	// pointer to the current module
	LBModule* m_pModule;

	// will be set to true if the data are modified
	bool m_Modified;

	// the dialog for add/edit objects
	CAddEditObjectDlg m_AddEditObjectDlg;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //
public:
	// updates the state of the dialog sets m_Modified if data have been changed
	// if the given parameter is true, update also the column header
	void UpdateState(bool header);
	// returns true if data have been modified
	bool GetModified() const;
	// return the list control item count
	int GetListItemCount() const;
	// sets the module
	void SetModule(LBModule* module);
	// returns the state of the clear button (true if enabled)
	BOOL GetClearButtonState() const;

// -------------------------------------------------------------------------- //
// OBJECT LIST EVENT HANDLERS                                                 //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnNMClickTabObjectList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemActivateTabObjectList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownTabObjectList(NMHDR *pNMHDR, LRESULT *pResult);

// -------------------------------------------------------------------------- //
// OBJECT LIST BUTTONS HANDLERS                                               //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnBnClickedTabObjAddBtn();
	afx_msg void OnBnClickedTabObjEditBtn();
	afx_msg void OnBnClickedTabObjRemoveBtn();
public:
	afx_msg void OnBnClickedTabObjClearBtn();

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
