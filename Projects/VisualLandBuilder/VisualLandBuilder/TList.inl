IMPLEMENT_SERIAL(TList<T>, CObject, 1)

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

template <class T>
TList<T>::TList() : CObject()
{
}

// -------------------------------------------------------------------------- //

template <class T>
TList<T>::TList(const TList<T>& other)
{
	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		T* item = new T(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

template <class T>
TList<T>::~TList()
{
	RemoveAll();
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //

template <class T>
TList<T>& TList<T>::operator = (const TList<T>& other)
{
	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		T* item = new T(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //

template <class T>
CTypedPtrList<CObList, T*>& TList<T>::GetList()
{
	return m_List;
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

template <class T>
void TList<T>::SetList(CTypedPtrList<CObList, T*>& newList)
{
	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = newList.GetHeadPosition();
	while(pos)
	{
		T* item = new T(newList.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Member variables manipulators                                  //
// ************************************************************** //

template <class T>
void TList<T>::Append(const T& t)
{
	T* item = new T(t);
	m_List.AddTail(item);
}

// -------------------------------------------------------------------------- //

template <class T>
void TList<T>::Append(T* t)
{
	m_List.AddTail(t);
}

// -------------------------------------------------------------------------- //

template <class T>
T* TList<T>::GetAt(int index)
{
	return m_List.GetAt(m_List.FindIndex(index));
}

// -------------------------------------------------------------------------- //

template <class T>
void TList<T>::SetAt(int index, T* t)
{
	POSITION pos = m_List.FindIndex(index);
	T* oldT = m_List.GetAt(pos);
	m_List.SetAt(pos, new T(t));
	delete t;
	delete oldT;
}

// -------------------------------------------------------------------------- //

template <class T>
void TList<T>::RemoveAt(int index)
{
	CTypedPtrList<CObList, T*> tempList;

	// sets the copy of the list
	// discarding the item to remove
	POSITION pos = m_List.GetHeadPosition();
	int counter = 0;
	while(pos)
	{
		T* item = m_List.GetNext(pos);
		if(counter != index)
		{
			T* copyItem = new T(item);
			tempList.AddTail(copyItem);
		}
		counter++;
	}

	// sets the list to be the temp list
	SetList(tempList);

	// clears the temp list
	pos = tempList.GetHeadPosition();
	while(pos)
	{
		delete tempList.GetNext(pos);
	}
	tempList.RemoveAll();
}

// -------------------------------------------------------------------------- //

template <class T>
void TList<T>::RemoveAll()
{
	// removes all items from the list
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		delete m_List.GetNext(pos);
	}
	m_List.RemoveAll();
}

// ************************************************************** //
// Interface                                                      //
// ************************************************************** //

template <class T>
INT_PTR TList<T>::GetCount()
{
	return m_List.GetCount();
}

// -------------------------------------------------------------------------- //

template <class T>
BOOL TList<T>::IsEmpty()
{
	return m_List.IsEmpty();
}

// ************************************************************** //
// Serialization                                                  //
// ************************************************************** //

template <class T>
void TList<T>::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);
	m_List.Serialize(ar);
}

// -------------------------------------------------------------------------- //
