//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

// ************************ //
// Enum for parameter types //
// ************************ //
typedef enum
{
  LBPT_STRING,
  LBPT_INTEGER,
  LBPT_FLOAT,
  LBPT_BOOL
} LBParameterTypes;

//-----------------------------------------------------------------------------

class LBParameter : public CObject
{
  DECLARE_SERIAL( LBParameter )

protected:
  // the version
  int m_version;
  // the parameter name
  string m_name;
  // the parameter value
  string m_value;
  // the description
  string m_description;
  // the parameter type
  LBParameterTypes m_type;
  // true if this parameter has a default value
  bool m_hasDefault;
  // true if this parameter can be moved between global and dbf
  bool m_isMovable;
  // true if this parameter belongs to the module's extension
  bool m_isExtension;

public:
  // Default constructor
  LBParameter();

  // Constructs this parameter item with the given parameters
  LBParameter( const string& name, const string& value, const string& description,
               LBParameterTypes type, bool hasDefault, bool isMovable, bool isExtension );

  // until i don't find nothing better this is to avoid memory leaks
  LBParameter( LBParameter* other );

  // copy constructor
  LBParameter( const LBParameter& other );

  // Assignement 
  LBParameter& operator = ( const LBParameter& other );

  // returns the name of this parameter
  string GetName() const;

  // returns the value of this parameter
  string GetValue() const;

  // returns the description of this parameter
  string GetDescription() const;

  // returns the type of this parameter
  LBParameterTypes GetType() const;

  // returns true if this parameter has a default value
  bool GetHasDefault() const;

  // returns true if this parameter can be moved between global and dbf
  bool IsMovable() const;

  // returns true if this parameter belongs to the module's extension
  bool IsExtension() const;

  // sets the name of this parameter with the given one
  void SetName( const string& name );

  // sets the name of this parameter with the given one
  void SetName( const CString& name );

  // SetValue()
  // sets the value of this parameter with the given one
  void SetValue( const string& value );

  // sets the value of this parameter with the given one
  void SetValue( const CString& value );

  // sets the description of this parameter with the given one
  void SetDescription( const string& description );

  // sets the description of this parameter with the given one
  void SetDescription( const CString& description );

  // sets the type of this parameter with the given one
  void SetType( LBParameterTypes type );

  // sets the has default of this parameter with the given one
  void SetHasDefault( bool hasDefault );

  // sets the is movable of this parameter with the given one
  void SetIsMovable( bool isMovable );

  // sets the is extension of this parameter with the given one
  void SetIsExtension( bool isExtension );

  // clears the value of this parameter 
  void ClearValue();

  virtual void Serialize( CArchive& ar );
};

//-----------------------------------------------------------------------------
