// COptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VisualLandBuilder.h"
#include "COptionsDlg.h"

// COptionsDlg dialog

IMPLEMENT_DYNAMIC(COptionsDlg, CDialog)

// -------------------------------------------------------------------------- //

COptionsDlg::COptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDlg::IDD, pParent)
{
}

// -------------------------------------------------------------------------- //

COptionsDlg::~COptionsDlg()
{
}

// -------------------------------------------------------------------------- //

void COptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OPTIONSTAB, m_OptionsTabCtrl);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(COptionsDlg, CDialog)
	ON_BN_CLICKED(IDOK, &COptionsDlg::OnBnClickedOk)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL COptionsDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	// adds items to the param tabs
	m_OptionsTabCtrl.InsertItem(TCIF_TEXT, IDD_DIRECTORIES, "Directories", 0, 0);

	m_OptionsTabCtrl.Init();

	// connect the module to the dialogs in the param tab
	m_DirDlg = (CTabDirectoriesDlg*)m_OptionsTabCtrl.GetDialog(IDD_DIRECTORIES);

	m_DirDlg->SetDirectoryList(&m_Options.GetDirectoryList());

	// update the dialogs in the param tab
	UpdateDirectoriesTabDialog();

	return TRUE;
}

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

void COptionsDlg::UpdateDirectoriesTabDialog()
{
	m_DirDlg->UpdateDirectoryListCtrl();
}

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //

void COptionsDlg::SetOptions(LBOptions* options)
{
	// sets the working copy of the data
	m_Options = *options;

	// sets the pointer to the original data
	m_pOldOptions = options;
}

// -------------------------------------------------------------------------- //

void COptionsDlg::SetSelectedOption(LBOptTabItems selOption)
{
	m_OptionsTabCtrl.SetCurrentTab(selOption);
}

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //

void COptionsDlg::OnBnClickedOk()
{
	if(true)
	{
		// updates data
		*m_pOldOptions = m_Options;
	}
	OnOK();
}

