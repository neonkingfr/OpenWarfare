#include "stdafx.h"

#include ".\LBError.h"

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBError::LBError()
{
}

// -------------------------------------------------------------------------- //

LBError::LBError(LBErrorTypes type, int moduleIndex, const string& description) 
: m_Type(type)
, m_ModuleIndex(moduleIndex)
, m_Description(description)
{
}

// -------------------------------------------------------------------------- //

LBError::LBError(const LBError& other) 
: m_Type(other.m_Type)
, m_ModuleIndex(other.m_ModuleIndex)
, m_Description(other.m_Description)
{
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //

LBError& LBError::operator = (const LBError& other)
{
	m_Type        = other.m_Type;
	m_ModuleIndex = other.m_ModuleIndex; 
	m_Description = other.m_Description;

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //

LBErrorTypes LBError::GetType() const
{
	return m_Type;
}

// -------------------------------------------------------------------------- //

int LBError::GetModuleIndex() const
{
	return m_ModuleIndex;
}

// -------------------------------------------------------------------------- //

string LBError::GetDescription()
{
	switch(m_Type)
	{
	case LBET_NOERROR:
		m_Description = "No errors";
		break;
	case LBET_SHAPEFILEFILENOTFOUND:
		m_Description = "Shapefile file not found";
		break;
	case LBET_DBFFILENOTFOUND:
		m_Description = "Dbf file not found";
		break;
	case LBET_DEMFILENOTFOUND:
		m_Description = "Dem file not found";
		break;
	case LBET_MISSINGOBJECTNAME:
		m_Description = "Missing object name";
		break;
	case LBET_MISSINGMAPPARAMETER:
		m_Description = "Missing map parameter";
		break;
	case LBET_MISSINGOBJECTPARAMETER:
		m_Description = "Missing object parameter";
		break;
	case LBET_MISSINGDEMPARAMETER:
		m_Description = "Missing dem parameter";
		break;
	case LBET_MISSINGGLOBALPARAMETER:
		m_Description = "Missing global parameter";
		break;
	case LBET_MISSINGDBFPARAMETER:
		m_Description = "Missing dbf parameter";
		break;
	case LBET_NOACTIVEMODULES:
		m_Description = "No active modules";
		break;
	case LBET_MISSINGDIRECTORY:
		m_Description = "Missing data in Options/Directory";
		break;
	default:
		m_Description = "Unknown error";
		break;
	}

	return m_Description;
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBError::SetType(LBErrorTypes newType)
{
	m_Type = newType;
}

// -------------------------------------------------------------------------- //

void LBError::SetModuleIndex(int newModuleIndex)
{
	m_ModuleIndex = newModuleIndex; 
}

// -------------------------------------------------------------------------- //

void LBError::SetDescription(const string& newDescription)
{
	m_Description = newDescription;
}
