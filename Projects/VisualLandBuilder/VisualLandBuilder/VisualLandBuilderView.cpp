//-----------------------------------------------------------------------------

#include "stdafx.h"
#include ".\VisualLandBuilder.h"

#include ".\VisualLandBuilderDoc.h"
#include ".\VisualLandBuilderView.h"

//-----------------------------------------------------------------------------

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//-----------------------------------------------------------------------------

IMPLEMENT_DYNCREATE( CVisualLandBuilderView, CListView )

//-----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP( CVisualLandBuilderView, CListView )
	ON_UPDATE_COMMAND_UI( ID_MODULE_EDITMODULE,           &CVisualLandBuilderView::OnUpdateModuleEditModule )
	ON_UPDATE_COMMAND_UI( ID_MODULE_REMOVEMODULE,         &CVisualLandBuilderView::OnUpdateModuleRemoveModule )
	ON_UPDATE_COMMAND_UI( ID_MODULE_MOVEDOWNMODULE,       &CVisualLandBuilderView::OnUpdateModuleMoveDownModule )
	ON_UPDATE_COMMAND_UI( ID_MODULE_MOVEUPMODULE,         &CVisualLandBuilderView::OnUpdateModuleMoveUpModule )
	ON_UPDATE_COMMAND_UI( ID_MODULE_REMOVEALLMODULES,     &CVisualLandBuilderView::OnUpdateModuleRemoveAllModules )
	ON_UPDATE_COMMAND_UI( ID_MODULE_CHECKMODULE,          &CVisualLandBuilderView::OnUpdateModuleCheckModule )
	ON_UPDATE_COMMAND_UI( ID_MODULE_CHECKALLMODULES,      &CVisualLandBuilderView::OnUpdateModuleCheckAllModules )
	ON_UPDATE_COMMAND_UI( ID_MODULE_UNCHECKALLMODULES,    &CVisualLandBuilderView::OnUpdateModuleUncheckAllModules )
	ON_UPDATE_COMMAND_UI( ID_LANDBUILDER_PREVIEW,         &CVisualLandBuilderView::OnUpdateLandBuilderPreview )
	ON_UPDATE_COMMAND_UI( ID_LANDBUILDER_RUNLANDBUILDER2, &CVisualLandBuilderView::OnUpdateLandBuilderRunLandBuilder2 )
	ON_UPDATE_COMMAND_UI( ID_MODULE_DUPLICATEMODULE,      &CVisualLandBuilderView::OnUpdateModuleDuplicateModule )
	ON_NOTIFY_REFLECT( NM_CLICK,         &CVisualLandBuilderView::OnNMClick )
	ON_NOTIFY_REFLECT( LVN_ITEMACTIVATE, &CVisualLandBuilderView::OnLvnItemActivate )
	ON_NOTIFY_REFLECT( LVN_KEYDOWN,      &CVisualLandBuilderView::OnLvnKeydown )
	ON_NOTIFY_REFLECT( LVN_ITEMCHANGED,  &CVisualLandBuilderView::OnLvnItemChanged )
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------

CVisualLandBuilderView::CVisualLandBuilderView() 
: m_updateFromCheck( true )
{
}

//-----------------------------------------------------------------------------

CVisualLandBuilderView::~CVisualLandBuilderView()
{
}

//-----------------------------------------------------------------------------

BOOL CVisualLandBuilderView::PreCreateWindow( CREATESTRUCT& cs )
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.style &= ~LVS_TYPEMASK;
	cs.style |= LVS_REPORT;         // report style
	cs.style |= LVS_SHOWSELALWAYS;  // to see selection

	return (CListView::PreCreateWindow( cs ));
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();

	// TODO: You may populate your ListView with items by directly accessing
	//  its list control through a call to GetListCtrl().
	
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	LVCOLUMN col;
	col.mask = LVCF_WIDTH; // without THIS mask the following GetColumn doesn't work

	// checks if the list is already initialized
	// if not, sets the columns and styles
	BOOL b = listCtrl.GetColumn( 0, &col );
	if ( listCtrl.GetColumn( 0, &col ) == FALSE )
	{
		// adds columns to the module list
		listCtrl.InsertColumn( 0, _T( "Module name" ), LVCFMT_LEFT );
		listCtrl.InsertColumn( 1, _T( "Parameters" ), LVCFMT_LEFT );
		listCtrl.InsertColumn( 2, _T( "Notes" ), LVCFMT_LEFT );
		listCtrl.SetColumnWidth( 0, 200 );
		listCtrl.SetColumnWidth( 1, 500 );
		listCtrl.SetColumnWidth( 2, 400 );

		// updates module list style
		LONG extendedListStyle = 0;	
		extendedListStyle |= LVS_EX_GRIDLINES;      // adds grid lines to the module list
		extendedListStyle |= LVS_EX_CHECKBOXES;     // adds checkboxes to the module list
		extendedListStyle |= LVS_EX_FULLROWSELECT;  // adds full row selection to the module list
		listCtrl.SetExtendedStyle( extendedListStyle );
	}
}

//-----------------------------------------------------------------------------

#ifdef _DEBUG
void CVisualLandBuilderView::AssertValid() const
{
	CListView::AssertValid();
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::Dump( CDumpContext& dc ) const
{
	CListView::Dump( dc );
}

//-----------------------------------------------------------------------------

CVisualLandBuilderDoc* CVisualLandBuilderView::GetDocument() const // non-debug version is inline
{
	ASSERT( m_pDocument->IsKindOf( RUNTIME_CLASS( CVisualLandBuilderDoc ) ) );
	return ((CVisualLandBuilderDoc*)m_pDocument);
}
#endif //_DEBUG

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdate( CView* /*pSender*/, LPARAM lHint, CObject* /*pHint*/ )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();
	listCtrl.DeleteAllItems();

	LBModuleList& moduleList = GetDocument()->GetModuleList();

	INT_PTR modCount = moduleList.GetCount();
	for ( int i = 0; i < modCount; ++i )
	{
		LBModule* item = moduleList.GetAt( i );
		// sets first column
		m_updateFromCheck = false;
		listCtrl.InsertItem( i, item->GetVLBName().c_str() );
		m_updateFromCheck = true;
		if ( item->IsActive() )
		{
			listCtrl.SetCheck( i, TRUE );
		}
		else
		{
			listCtrl.SetCheck( i, FALSE );
		}
		listCtrl.SetItem( i, 1, LVIF_TEXT, item->ToString().c_str(), 0, 0, 0, 0 );
		listCtrl.SetItem( i, 2, LVIF_TEXT, item->GetNotes().c_str(), 0, 0, 0, 0 );
	}

	if ( lHint != 0 )
	{
		listCtrl.SetItemState( static_cast< int >( lHint - 1000 ), LVIS_SELECTED, LVIS_SELECTED | LVIS_FOCUSED );
		listCtrl.EnsureVisible( static_cast< int >( lHint - 1000 ), FALSE );
		GetDocument()->SetCurSelModuleIndex( static_cast< int >( lHint - 1000 ) );
	}

	GetDocument()->UpdateVisitorMapParameter();
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateLandBuilderPreview( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	if ( listCtrl.GetItemCount() == 0 )
	{
		cmdUI->Enable( FALSE );
		return;
	}

	for ( int i = 0; i < listCtrl.GetItemCount(); ++i )
	{
		if ( listCtrl.GetCheck( i ) )
		{
			cmdUI->Enable( TRUE );
			return;
		}
	}

	cmdUI->Enable( FALSE );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateLandBuilderRunLandBuilder2( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	// is list empty ?
	if ( listCtrl.GetItemCount() == 0 )
	{
		cmdUI->Enable( FALSE );
		return;
	}

	// there are active modules ?
	int activeCount = 0;

	for ( int i = 0; i < listCtrl.GetItemCount(); ++i )
	{
		if ( listCtrl.GetCheck( i ) )
		{
			activeCount++;
		}
	}

	if ( activeCount == 0 )
	{
		cmdUI->Enable( FALSE );
		return;
	}

	cmdUI->Enable( TRUE );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateModuleEditModule( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	if ( listCtrl.GetNextItem( -1, LVNI_SELECTED ) != -1 )
	{
		cmdUI->Enable( TRUE );
	}
	else
	{
		cmdUI->Enable( FALSE );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateModuleRemoveModule( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	if ( listCtrl.GetNextItem( -1, LVNI_SELECTED ) != -1 )
	{
		cmdUI->Enable( TRUE );
	}
	else
	{
		cmdUI->Enable( FALSE );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateModuleMoveDownModule( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	int selected = listCtrl.GetNextItem( -1, LVNI_SELECTED );

	if ( selected != -1 && selected != listCtrl.GetItemCount() - 1 )
	{
		cmdUI->Enable( TRUE );
	}
	else
	{
		cmdUI->Enable( FALSE );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateModuleMoveUpModule( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	int selected = listCtrl.GetNextItem( -1, LVNI_SELECTED );

	if ( selected != -1 && selected > 0 )
	{
		cmdUI->Enable( TRUE );
	}
	else
	{
		cmdUI->Enable( FALSE );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateModuleRemoveAllModules( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	if ( listCtrl.GetItemCount() != 0 )
	{
		cmdUI->Enable( TRUE );
	}
	else
	{
		cmdUI->Enable( FALSE );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateModuleCheckModule( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	int selected = listCtrl.GetNextItem( -1, LVNI_SELECTED );

	if ( selected != -1 )
	{
		if ( listCtrl.GetCheck( selected ) )
		{
			cmdUI->SetText( "Un&check module" );
		}
		else
		{
			cmdUI->SetText( "&Check module" );
		}
		cmdUI->Enable( TRUE );
	}
	else
	{
		cmdUI->SetText( "&Check module" );
		cmdUI->Enable( FALSE );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateModuleCheckAllModules( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	for ( int i = 0; i < listCtrl.GetItemCount(); ++i )
	{
		if ( !listCtrl.GetCheck( i ) )
		{
			cmdUI->Enable( TRUE );
			return;
		}
	}
	cmdUI->Enable( FALSE );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateModuleUncheckAllModules( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	for ( int i = 0; i < listCtrl.GetItemCount(); ++i )
	{
		if ( listCtrl.GetCheck( i ) )
		{
			cmdUI->Enable( TRUE );
			return;
		}
	}
	cmdUI->Enable( FALSE );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnUpdateModuleDuplicateModule( CCmdUI* cmdUI )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	int selected = listCtrl.GetNextItem( -1, LVNI_SELECTED );

	if ( selected != -1 )
	{
		cmdUI->Enable( TRUE );
	}
	else
	{
		cmdUI->Enable( FALSE );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnNMClick( NMHDR* nmhdr, LRESULT* result )
{
	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	// gets the index of the selected item in the list
	int selItem = listCtrl.GetNextItem( -1, LVNI_SELECTED );

	GetDocument()->SetCurSelModuleIndex( selItem );

	*result = 0;
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnLvnItemActivate( NMHDR* nmhdr, LRESULT* result )
{
	LPNMITEMACTIVATE nmia = reinterpret_cast< LPNMITEMACTIVATE >( nmhdr );

	PostMessage( WM_COMMAND, ID_MODULE_EDITMODULE, 0 );

	*result = 0;
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnLvnKeydown( NMHDR* nmhdr, LRESULT* result )
{
	LPNMLVKEYDOWN lvKeyDow = reinterpret_cast< LPNMLVKEYDOWN >( nmhdr );

	OnNMClick( nmhdr, result );

	*result = 0;
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderView::OnLvnItemChanged( NMHDR* nhmdr, LRESULT* result )
{
	LPNMLISTVIEW nmlv = reinterpret_cast< LPNMLISTVIEW >( nhmdr );

	// gets the module list control
	CListCtrl& listCtrl = GetListCtrl();

	// updates item active/inactive
	if ( m_updateFromCheck )
	{
		for ( int i = 0; i < listCtrl.GetItemCount(); ++i )
		{
			LBModule* item = GetDocument()->GetModuleList().GetAt( i );
			item->SetActive( listCtrl.GetCheck( i ) );
		}
		GetDocument()->UpdateVisitorMapParameter();
	}
	*result = 0;
}

//-----------------------------------------------------------------------------

