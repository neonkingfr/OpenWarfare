//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include ".\LBGlobalVariables.h"
#include ".\LBParameterList.h"
#include ".\LBObjectList.h"
#include ".\LBDemList.h"
#include ".\LBError.h"
#include ".\LBDirectoryList.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

// *********************************** //
// Definitions for allowed shape types //
// *********************************** //
#define AST_NONE        0x00000001
#define AST_POINT       0x00000002
#define AST_MULTIPOINT  0x00000004
#define AST_POLYGON     0x00000008
#define AST_POLYLINE    0x00000016

//-----------------------------------------------------------------------------

// ********************* //
// Enum for module types //
// ********************* //
typedef enum
{
  LBMT_TERRAIN_MODELER,
  LBMT_SHAPE_MODELER,
  LBMT_PLACER_FROM_SHAPEFILE,
  LBMT_DELETER_FROM_SHAPEFILE
  // scripts are not implemented at the moment
  // LBMT_SCRIPT
} LBModuleTypes;

//-----------------------------------------------------------------------------

// ************************* //
// Enum for use object types //
// ************************* //
typedef enum
{
  LBMUO_NONE,
  LBMUO_SINGLE,
  LBMUO_MULTIPLE
} LBModuleUseObjectTypes;

//-----------------------------------------------------------------------------

// ********************** //
// Enum for use dem types //
// ********************** //
typedef enum
{
  LBMUD_NONE,
  LBMUD_SINGLE,
  LBMUD_MULTIPLE
} LBModuleUseDemTypes;

//-----------------------------------------------------------------------------

// *********************************************** //
// Definitions for LandBuilder compatibility types //
// *********************************************** //
#define LBCT_1        0x00000001
#define LBCT_2        0x00000002

//-----------------------------------------------------------------------------

// *************************** //
// Enum for predefined modules //
// *************************** //
typedef enum
{
  LBPM_START_PREDEFINED_MODULES,
  LBPM_ADVANCED_ON_PLACE_PLACER,
  LBPM_ADVANCED_RANDOM_PLACER,
  LBPM_ADVANCED_RANDOM_PLACER_SLOPE,
  LBPM_BEST_FIT_PLACER,
  LBPM_BIASED_RANDOM_PLACER,
  LBPM_BIASED_RANDOM_PLACER_SLOPE,
  LBPM_BOUNDARY_SUB_SQUARES_FRAME_PLACER,
  LBPM_BUILDING_CREATOR,
  LBPM_CROSSROAD_PLACER,
  LBPM_CS_SMOOTH_TERRAIN_ALONG_PATH,
  LBPM_DELETE_ALONG_PATH,
  LBPM_DELETE_IN_POLYGON,
//  LBPM_DUMMY_BUILDING_PLACER,
  LBPM_EDIFICI,
  LBPM_FA_SMOOTH_TERRAIN_ALONG_PATH,
  LBPM_FR_SMOOTH_TERRAIN_ALONG_PATH,
  LBPM_FRAMED_FOREST_PLACER,
//  LBPM_GEO_PROJECTION,
//  LBPM_HIGHMAP_LOADER,
//  LBPM_HIGHMAP_TEST,
  LBPM_INNER_SUB_SQUARES_FRAME_PLACER,
  LBPM_M_SMOOTH_TERRAIN_ALONG_PATH,
  LBPM_MA_SMOOTH_TERRAIN_ALONG_PATH,
  LBPM_MS_SMOOTH_TERRAIN_ALONG_PATH,
  LBPM_NY_DUMMY_BUILDING_PLACER,
  LBPM_ON_PLACE_PLACER,
  LBPM_RANDOM_ON_PATH_PLACER,
  LBPM_REGULAR_ON_PATH_PLACER,
  LBPM_RIVERS,
  LBPM_ROADS_GENERATOR,
  LBPM_ROAD_PLACER,
  // for the moment scripts are not implemented
  // LBPM_SCRIPT_MODULE,
//  LBPM_NONE, // SimpleMapper
  LBPM_SIMPLE_RANDOM_PLACER,
  LBPM_SIMPLE_RANDOM_PLACER_SLOPE,
//	LBPM_SMOOTH_TERRAIN_ALONG_PATH, // no more needed, replaced by specialized modules
  LBPM_STRADE,
  LBPM_SUB_SQUARES_RANDOM_PLACER,
  LBPM_TRANSFORM_2D,
  LBPM_END_PREDEFINED_MODULES
} LBPredefinedModules;

//-----------------------------------------------------------------------------

class LBModule : public CObject
{
  DECLARE_SERIAL( LBModule )

  // ************************************************************** //
  // Member variables                                               //
  // ************************************************************** //
protected:
  // the version
  int m_version;

  // the module name inside LB
  string m_lbName;

  // the module name inside VLB
  string m_vlbName;

  // the module type
  LBModuleTypes m_type;

  // the module description
  string m_description;

  // the use objects
  LBModuleUseObjectTypes m_useObjects;

  // true if this module uses shapefile
  bool m_useShapefile;

  // the flag containing the allowed shape types
  DWORD m_allowedShapeTypes;

  // true if this module can work with multipart shapes
  bool m_acceptMultipartShapes;

  // the shapefile filename
  string m_shapeFileName;

  // the module object list of parameters
  LBParameterList m_objParametersList;

  // the object list 
  LBObjectList m_objectsList;

  // the list of global parameters
  LBParameterList m_globalParametersList;

  // the list of hidden global parameters
  LBParameterList m_hiddenGlobalParametersList;

  // the list of dbf parameters
  LBParameterList m_dbfParametersList;

  // the use dems
  LBModuleUseDemTypes m_useDems;

  // true if this module exports terrain
  bool m_exportsTerrain;

  // the dems list 
  LBDemList m_demsList;

  // true if this module is activated in the list
  bool m_active;

  // true if this module can be previewed
  bool m_hasPreview;

  // contains the version of LandBuilder which
  // are compatible with this module (ored)
  // obsolete still here for backward compatibility (serialization)
  int m_landBuilderCompatibility;

  // the objects to draw in the preview
  ModuleObjectOutputArray m_drawObjects;

  // true if the module is compound
  bool m_hasExtension;

  // the extension name inside LB
  string m_lbExtensionName;

  // the notes
  string m_notes;

public:
  // Default constructor
  LBModule();

  // Constructs this module with the given parameters
  LBModule( const string& lbName, const string& vlbName,
            int lbCompatibility,
            LBModuleTypes type, const string& description,
            LBModuleUseObjectTypes useObjects, 
            const LBParameterList& objParametersList,
            const LBParameterList& globalParametersList,
            const LBParameterList& hiddenGlobalParametersList,
            const LBParameterList& dbfParametersList,
            bool useShapefile, DWORD allowedShapeTypes, bool acceptMultipartShapes,
            const string& shapeFileName,
            LBModuleUseDemTypes useDems, bool exportsTerrain, bool hasPreview, bool hasExtension,
            const string& lbExtensionName, const string& notes );

  // until i don't find nothing better this is to avoid memory leak
  LBModule( LBModule* other );

  // copy constructor
  LBModule( const LBModule& other );

  virtual ~LBModule();

  LBModule& operator = ( const LBModule& other );

  // returns the LB name of this module
  const string& GetLBName() const;

  // returns the VLB name of this module
  const string& GetVLBName() const;

  // returns the type of this module
  LBModuleTypes GetType() const;

  // returns the description of this module
  string GetDescription() const;

  // returns the use objects of this module
  LBModuleUseObjectTypes GetUseObjects() const;

  // returns true if this module uses shapefile
  bool GetUseShapefile() const;

  // returns the flag containing the allowed shape types for this module 
  DWORD GetAllowedShapeTypes() const;

  // returns true if this module accepts multipart shapes
  bool GetAcceptMultipartShapes() const;

  // returns the file name of the shapefile of this module
  const string& GetShapeFileName() const;

  // returns the parameters list of the objects of this module
  LBParameterList& GetObjParametersList();

  // returns the objects list of the objects of this module
  LBObjectList& GetObjectsList();

  // returns the global parameters list of this module
  LBParameterList& GetGlobalParametersList();

  // returns the hidden global parameters list of this module
  LBParameterList& GetHiddenGlobalParametersList();

  // returns the dbf parameters list of this module
  LBParameterList& GetDbfParametersList();

  // returns the use dems of this module
  LBModuleUseDemTypes GetUseDems() const;

  // returns true if this module exports terrain
  bool GetExportsTerrain() const;

  // returns the dems list of this module
  LBDemList& GetDemsList();

  // returns true if the module is active in the list 
  bool IsActive() const;

  // returns true if the module can be previewed
  bool GetHasPreview() const;

  // returns true if the module is compound
  bool GetHasExtension() const;

  // returns the LB name of the extension of this module
  const string& GetLBExtensionName() const;

  // returns the notes of this module
  const string& GetNotes() const;

  // sets the LB name of this module with the given one
  void SetLBName( const string& lbName );

  // sets the VLB name of this module with the given one
  void SetVLBName( const string& vlbName );

  // sets the type of this module with the given one
  void SetType( LBModuleTypes type );

  // sets the description of this module with the given one
  void SetDescription( const string& description );

  // sets the use objects of this module with the given one
  void SetUseObjects( LBModuleUseObjectTypes useObjects );

  // sets the use shapefile of this module with the given one
  void SetUseShapefile( bool useShapefile );

  // sets the flag containing the allowed shape types for this module 
  void GetAllowedShapeTypes( DWORD allowedShapeTypes );

  // returns true if this module accepts multipart shapes
  void SetAcceptMultipartShapes( bool acceptMultipartShapes );

  // sets the file name of the shapefile of this module with the given one
  void SetShapeFileName( const string& shapeFileName );

  // sets the file name of the shapefile of this module with the given one
  void SetShapeFileName( const CString& shapeFileName );

  // sets the parameter list of the objects of this module with the given one
  void SetObjParametersList( const LBParameterList& objParametersList );

  // sets the objects list of this module with the given one
  void SetObjectsList( const LBObjectList& objectsList );

  // sets the global parameter list of this module with the given one
  void SetGlobalParametersList( const LBParameterList& globalParametersList );

  // sets the hidden global parameter list of this module with the given one
  void SetHiddenGlobalParametersList( const LBParameterList& hiddenGlobalParametersList );

  // sets the dbf parameter list of this module with the given one
  void SetDbfParametersList( const LBParameterList& dbfParametersList );

  // sets the use dem of this module with the given one
  void SetUseDems( LBModuleUseDemTypes useDems );

  // sets the exports terrain of this module with the given one
  void SetExportsTerrain( bool exportsTerrain );

  // sets the dems list of this module with the given one
  void SetDemsList( const LBDemList& demsList );

  // sets the module as active/inactive with the given value
  void SetActive( bool active );

  // sets the module as being or not previewable
  void SetHasPreview( bool hasPreview );

  // sets the module as being or not compound
  void SetHasExtension( bool hasExtension );

  // sets the LB name of the extension of this module with the given one
  void SetLBExtensionName( const string& lbName );

  // sets the notes of this module with the given one
  void SetNotes( const string& notes );

  // adds the given object to the objects list of this module 
  void AddObject( LBObject* object );

  // sets the object of the object list at the given index with 
  // the given object 
  void SetObject( int index, LBObject* object );

  // removes the object of the object list at the given index 
  void RemoveObject( int index );

  // removes all the object of the object list of this module
  void RemoveAllObjects();

  // sets the global parameter of this module at the given index with
  // the given parameter 
  void SetGlobalParameter( int index, LBParameter* parameter );

  // removes all the global parameters of this module
  void RemoveAllGlobalParameters();

  // clear all the data in the global parameters of this module
  void ClearAllGlobalParameters();

  // sets the hidden global parameter of this module at the given index with
  // the given parameter 
  void SetHiddenGlobalParameter( int index, LBParameter* parameter );

  // removes all the hidden global parameters of this module
  void RemoveAllHiddenGlobalParameters();

  // clear all the data in the hidden global parameters of this module
  void ClearAllHiddenGlobalParameters();

  // sets the dbf parameter of this module at the given index with
  // the given parameter 
  void SetDbfParameter( int index, LBParameter* parameter );

  // removes all the dbf parameters of this module
  void RemoveAllDbfParameters();

  // clear all the data in the dbf parameters of this module
  void ClearAllDbfParameters();

  // adds the given dem to the dems list of this module 
  void AddDem( LBDem* dem );

  // sets the dem of the dems list at the given index with 
  // the given dem
  void SetDem( int index, LBDem* dem );

  // removes the dem of the dems list at the given index 
  void RemoveDem( int index );

  // removes all the dems of the dems list of this module
  void RemoveAllDems();

  // returns the result from module validation
  // LBET_NOERROR if the module can be exported to LandBuilder
  LBErrorTypes Validate( LBDirectoryList& directoryList );

  // save the task config file for this module
  // returns true if successfull
  bool SaveTaskConfig( const string& fileName, const string& workingFolder );

  // save the task config file for the extension of this module
  // returns true if successfull
  bool SaveExtensionTaskConfig( const string& fileName, const string& workingFolder );

  // draw a preview of this module
  void Preview( Shapes::ShapeList& shapeList, Shapes::ShapeDatabase& shDB, CPaintDC* dc, 
                CRect& rect, DrawParams drawParams, bool calculate );

  // returns true if the given shapelist contains a shape type
  // allowed by this module
  bool IsAllowedShapeList( Shapes::ShapeList& shapeList );

private:
  // CreatePreview()
  // calculates the data needed for the preview
  void CreatePreview( Shapes::ShapeList& shapeList, Shapes::ShapeDatabase& shDB );

public:
  string ToString();

  virtual void Serialize( CArchive& ar );

  // returns the predefined module with the given code
  static LBModule GetPredefinedModule( LBPredefinedModules moduleCode );
};

//-----------------------------------------------------------------------------
