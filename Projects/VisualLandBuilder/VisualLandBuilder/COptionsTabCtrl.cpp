#include "stdafx.h"

#include "COptionsTabCtrl.h"

BEGIN_MESSAGE_MAP(COptionsTabCtrl, CTabCtrl)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

COptionsTabCtrl::COptionsTabCtrl()
{
	m_TabPages[0] = new CTabDirectoriesDlg();

	m_NumberOfPages = 1;
}

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

COptionsTabCtrl::~COptionsTabCtrl()
{
	for(int i = 0; i < m_NumberOfPages; ++i)
	{
		delete m_TabPages[i];
	}
}

// ************************************************************** //
// Helper functions                                               //
// ************************************************************** //

void COptionsTabCtrl::Init()
{
	m_CurrentTab = 0;

	m_TabPages[0]->Create(IDD_DIRECTORIES, this);

	m_TabPages[0]->ShowWindow(SW_SHOW);

	SetRectangle();
}

// -------------------------------------------------------------------------- //

void COptionsTabCtrl::SetRectangle()
{
	CRect tabRect, itemRect;

	GetClientRect(&tabRect);

	int nX = tabRect.left + 2;
	int nY = tabRect.top + 23;
	int nXc = tabRect.Width() - 6;
	int nYc = tabRect.Height() - 27;

	for(int i = 0; i < m_NumberOfPages; ++i)
	{
		if(i == m_CurrentTab)
		{
			m_TabPages[i]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW);
		}
		else
		{
			m_TabPages[i]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);
		}
	}
}

// -------------------------------------------------------------------------- //

CDialog* COptionsTabCtrl::GetDialog(UINT index) const
{
	switch(index)
	{
	case IDD_DIRECTORIES:
		return m_TabPages[0];
	}
}

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //

void COptionsTabCtrl::SetCurrentTab(LBOptTabItems newCurTab)
{
	m_CurrentTab = newCurTab;
}

// ************************************************************** //
// Message handlers                                               //
// ************************************************************** //

void COptionsTabCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CTabCtrl::OnLButtonDown(nFlags, point);

	if(m_CurrentTab != GetCurFocus())
	{
		m_TabPages[m_CurrentTab]->ShowWindow(SW_HIDE);
		m_CurrentTab = GetCurFocus();
		m_TabPages[m_CurrentTab]->ShowWindow(SW_SHOW);
		m_TabPages[m_CurrentTab]->SetFocus();
	}
}

// -------------------------------------------------------------------------- //

BOOL COptionsTabCtrl::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_LEFT || pMsg->wParam == VK_RIGHT)
		{

			m_TabPages[m_CurrentTab]->ShowWindow(SW_HIDE);

			if(pMsg->wParam == VK_LEFT)
			{
				if(m_CurrentTab > 0)
				{
					m_CurrentTab--;
				}
			}
			if(pMsg->wParam == VK_RIGHT)
			{
				if(m_CurrentTab < m_NumberOfPages - 1)
				{
					m_CurrentTab++;
				}
			}
			m_TabPages[m_CurrentTab]->ShowWindow(SW_SHOW);
		}
	}

	return CTabCtrl::PreTranslateMessage(pMsg);
}
