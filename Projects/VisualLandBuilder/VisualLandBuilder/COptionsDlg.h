#pragma once

#include ".\LBOptions.h"
#include ".\COptionsTabCtrl.h"

#include "afxcmn.h"

// COptionsDlg dialog

class COptionsDlg : public CDialog
{
	DECLARE_DYNAMIC(COptionsDlg)

public:
	COptionsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionsDlg();

// Dialog Data
	enum { IDD = IDD_OPTIONS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	// the param tab control
	COptionsTabCtrl m_OptionsTabCtrl;

	// the child dialogs
	CTabDirectoriesDlg* m_DirDlg;

	// DATA
	// the pointer to the original data
	LBOptions* m_pOldOptions;
	// the working copy of the data
	LBOptions m_Options;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //
public:
	// SetOptions()
	// sets the working options and the pointer to the original one
	//  !! must be called before the DoModal function of this dialog !!
	void SetOptions(LBOptions* options);
	// SetSelectedOption()
	// sets the given options as selected
	//  !! must be called before the DoModal function of this dialog !!
	void SetSelectedOption(LBOptTabItems selOption);

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	void UpdateDirectoriesTabDialog();

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnBnClickedOk();
};
