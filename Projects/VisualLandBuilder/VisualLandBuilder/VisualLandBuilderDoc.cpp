//-----------------------------------------------------------------------------

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\VisualLandBuilderDoc.h"

#include ".\LBHelperFunctions.h"

//-----------------------------------------------------------------------------

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//-----------------------------------------------------------------------------

IMPLEMENT_DYNCREATE( CVisualLandBuilderDoc, CDocument )

//-----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP( CVisualLandBuilderDoc, CDocument )
	ON_COMMAND( ID_MODULE_ADDAMODULE,           &CVisualLandBuilderDoc::OnModuleAddModule )
	ON_COMMAND( ID_TOOLS_DIRECTORIES,           &CVisualLandBuilderDoc::OnToolsDirectories )
	ON_COMMAND( ID_MODULE_EDITMODULE,           &CVisualLandBuilderDoc::OnModuleEditModule )
	ON_COMMAND( ID_MODULE_REMOVEMODULE,         &CVisualLandBuilderDoc::OnModuleRemoveModule )
	ON_COMMAND( ID_MODULE_REMOVEALLMODULES,     &CVisualLandBuilderDoc::OnModuleRemoveAllModules )
	ON_COMMAND( ID_MODULE_CHECKALLMODULES,      &CVisualLandBuilderDoc::OnModuleCheckAllModules )
	ON_COMMAND( ID_MODULE_CHECKMODULE,          &CVisualLandBuilderDoc::OnModuleCheckModule )
	ON_COMMAND( ID_MODULE_UNCHECKALLMODULES,    &CVisualLandBuilderDoc::OnModuleUncheckAllModules )
	ON_COMMAND( ID_MODULE_MOVEDOWNMODULE,       &CVisualLandBuilderDoc::OnModuleMoveDownModule )
	ON_COMMAND( ID_MODULE_MOVEUPMODULE,         &CVisualLandBuilderDoc::OnModuleMoveUpModule )
	ON_COMMAND( ID_MAP_VISITORMAP,              &CVisualLandBuilderDoc::OnMapVisitorMap )
	ON_COMMAND( ID_TOOLS_OPTIONS,               &CVisualLandBuilderDoc::OnToolsOptions )
	ON_COMMAND( ID_LANDBUILDER_PREVIEW,         &CVisualLandBuilderDoc::OnLandBuilderPreview )
	ON_COMMAND( ID_LANDBUILDER_RUNLANDBUILDER2, &CVisualLandBuilderDoc::OnLandBuilderRunLandBuilder2 )
	ON_COMMAND( ID_MODULE_DUPLICATEMODULE,      &CVisualLandBuilderDoc::OnModuleDuplicateModule )
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------

CVisualLandBuilderDoc::CVisualLandBuilderDoc()
{
}

//-----------------------------------------------------------------------------

CVisualLandBuilderDoc::~CVisualLandBuilderDoc()
{
}

//-----------------------------------------------------------------------------

BOOL CVisualLandBuilderDoc::OnNewDocument()
{
  if ( !CDocument::OnNewDocument() ) { return (FALSE); }

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	
	SetDefaultVisitorMap();
	SetDefaultDirectoryList();

	return (TRUE);
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::Serialize( CArchive& ar )
{
	CObject::Serialize( ar );
	m_project.Serialize( ar );
}

//-----------------------------------------------------------------------------

#ifdef _DEBUG
void CVisualLandBuilderDoc::AssertValid() const
{
	CDocument::AssertValid();
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::Dump( CDumpContext& dc ) const
{
	CDocument::Dump( dc );
}
#endif //_DEBUG

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::SetDefaultVisitorMap()
{
	m_project.SetDefaultVisitorMap();
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::SetDefaultDirectoryList()
{
	// the default specification for the directory list
	// is coded in the CDirectoriesDlg
	m_project.SetDefaultDirectoryList( m_directoriesDlg );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::RemoveModule( int index )
{
	m_project.RemoveModule( index );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::RemoveAllModules()
{
	m_project.RemoveAllModules();
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::SetModuleList( const LBModuleList& moduleList )
{
	m_project.SetModuleList( moduleList );
}

//-----------------------------------------------------------------------------

bool CVisualLandBuilderDoc::ValidateProject( const string& lbVersion )
{
	// validates landbuilder2 path
	string landBuilder2Path = m_project.GetDirectoryList().GetLandBuilder2Path();

	if ( landBuilder2Path == "" )
	{
		AfxGetMainWnd()->MessageBox( string( MSG_LANDBUILDER2MISSING ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
		return (false);
	}

	// checks if working folder exists
	if ( GetFileAttributes( landBuilder2Path.c_str() ) == INVALID_FILE_ATTRIBUTES )
	{
		AfxGetMainWnd()->MessageBox( string( MSG_LANDBUILDER2NOTFOUND ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
		return (false);
	}

	// validates visitor map data
	LBErrorTypes errType = m_project.GetVisitorMap().Validate();
	if ( errType != LBET_NOERROR )
	{
		AfxGetMainWnd()->MessageBox( string( MSG_MISSINGVISITORMAPDATA ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
		return (false);
	}

	// validates modules
	LBError err = m_project.ValidateModules();

	if ( err.GetType() != LBET_NOERROR )
	{
		if ( err.GetType() == LBET_NOACTIVEMODULES )
		{
			string msg = err.GetDescription();		
			AfxGetMainWnd()->MessageBox( msg.c_str(), string( MSG_ERRORINMODULES ).c_str(), MB_OK | MB_ICONSTOP );
			return (false);
		}
		else
		{
			char msg1[81];
			sprintf_s( msg1, 80, "Found error in module number %d", err.GetModuleIndex() ); 
			string msg = string( msg1 ) + ": " + err.GetDescription();		
			AfxGetMainWnd()->MessageBox( msg.c_str(), string(MSG_ERRORINMODULES).c_str(), MB_OK | MB_ICONSTOP );
			return (false);
		}
	}

	bool usesShapefiles = m_project.GetModuleList().UsesShapefiles();

  // checks for Transform2D modules
  int activeTransform2DCount = m_project.GetModuleList().GetActiveModuleCount( MODULE_VLBNAME_TRANSFORM2D );

	if ( usesShapefiles )
	{
		// searches first active module
		LBModule* firstActiveItem = m_project.GetModuleList().GetActiveModule( 1 );
		// searches second active module
		LBModule* secondActiveItem = m_project.GetModuleList().GetActiveModule( 2 );

		// verifies that there is only one Transform2D module
		if ( activeTransform2DCount > 1 )
		{
			AfxGetMainWnd()->MessageBox( string( MSG_ONLYONETRANSFORM2D ).c_str(), string( MSG_ERRORINMODULES ).c_str(), MB_OK | MB_ICONSTOP );
			return (false);
		}

		// if used Transform2D must be the first module in the list
		if ( activeTransform2DCount > 0 /*&& activeGeoProjectionCount == 0*/ )
		{
			if ( firstActiveItem->GetVLBName() != MODULE_VLBNAME_TRANSFORM2D )
			{
				AfxGetMainWnd()->MessageBox( string( MSG_WHENUSINGTRANFORM2D ).c_str(), string( MSG_ERRORINMODULES ).c_str(), MB_OK | MB_ICONSTOP );
				return (false);
			}
		}

		int activeUsingShapefileCount = m_project.GetModuleList().GetActiveModuleUsingShapefileCount();

		// if the number of modules using shapefiles is equal to one
		// it means that we have only Transform2D 
		if ( activeUsingShapefileCount == 1 )
		{
			if ( activeTransform2DCount == 1 )
			{
				AfxGetMainWnd()->MessageBox( string( MSG_TRANSFORM2DALONE ).c_str(), string( MSG_ERRORINMODULES ).c_str(), MB_OK | MB_ICONSTOP );
				return (false);
			}
		}
	}

	return (true);
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::UpdateVisitorMapParameter()
{
	bool usesDems = m_project.GetModuleList().UsesDems();

	INT_PTR visParamCount = m_project.GetVisitorMap().GetParametersList().GetCount();
	for ( int i = 0; i < visParamCount; ++i )
	{
		LBParameter* item = m_project.GetVisitorMap().GetParametersList().GetAt( i );
		if ( (item->GetName() == HIGHMAPGRID_X) || (item->GetName() == HIGHMAPGRID_Y) )
		{
			item->SetHasDefault( !usesDems );
		}		
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::DeleteContents()
{
	m_project.Reset();
	CDocument::DeleteContents();
}

//-----------------------------------------------------------------------------

LBModuleList& CVisualLandBuilderDoc::GetModuleList() 
{
	return (m_project.GetModuleList());
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::SetCurSelModuleIndex( int index )
{
	m_curSelModuleIndex = index;
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnMapVisitorMap()
{
	// passes the map data to the dialog
	m_editVisitorMapDlg.SetVisitorMap( &m_project.GetVisitorMap() );

	// the dialog is responsible to change the data
	// in the data, so we just call it
	if ( m_editVisitorMapDlg.DoModal() == IDOK )
	{
		SetModifiedFlag();
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleAddModule()
{
	LBModule* module = new LBModule();

	CAddEditModuleDlg addEditModuleDlg( &m_project.GetVisitorMap() );

	addEditModuleDlg.SetModule( module );
	if ( addEditModuleDlg.DoModal() == IDOK )
	{
		// adds the new object to the list
		m_project.AddModule( module );
		SetModifiedFlag();
		UpdateAllViews( NULL );
	}
	else
	{
		delete module;
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleEditModule()
{
  if ( m_curSelModuleIndex == -1 ) { return; }

	LBModule* module = m_project.GetModuleList().GetAt( m_curSelModuleIndex );

	CAddEditModuleDlg addEditModuleDlg( &m_project.GetVisitorMap() );

	addEditModuleDlg.SetModule( module );
	if ( addEditModuleDlg.DoModal() == IDOK )
	{
		m_project.SetModule( m_curSelModuleIndex, module );
		SetModifiedFlag();
		UpdateAllViews( NULL );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleRemoveModule()
{
  if ( m_curSelModuleIndex == -1 ) { return; }

	string text = "Confirm that you want to remove the selected module ?";

	POSITION pos = GetFirstViewPosition();
	CView* firstView = GetNextView( pos );
	HWND view = firstView->GetSafeHwnd();

	if ( MessageBox( view, text.c_str(), "", MB_YESNO | MB_ICONQUESTION ) == IDYES )
	{
		RemoveModule( m_curSelModuleIndex );
		SetModifiedFlag();
		UpdateAllViews( NULL );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleRemoveAllModules()
{
	string text = "Confirm that you want to remove all the modules ?";

	POSITION pos = GetFirstViewPosition();
	CView* firstView = GetNextView( pos );
	HWND view = firstView->GetSafeHwnd();

	if ( MessageBox( view, text.c_str(), "", MB_YESNO | MB_ICONQUESTION ) == IDYES )
	{
		RemoveAllModules();
		SetModifiedFlag();
		UpdateAllViews( NULL );
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleMoveDownModule()
{
  if ( m_curSelModuleIndex == -1 ) { return; }

	LBModule* module1 = m_project.GetModuleList().GetAt( m_curSelModuleIndex );
	LBModule* copyModule1 = new LBModule( module1 );
	LBModule* module2 = m_project.GetModuleList().GetAt( m_curSelModuleIndex + 1 );

	*module1 = *module2;
	*module2 = *copyModule1;

	delete copyModule1;

	SetModifiedFlag();
	UpdateAllViews( NULL, 1000 + m_curSelModuleIndex + 1 );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleMoveUpModule()
{
  if ( m_curSelModuleIndex == -1 ) { return; }

	LBModule* module1 = m_project.GetModuleList().GetAt( m_curSelModuleIndex - 1 );
	LBModule* copyModule1 = new LBModule( module1 );
	LBModule* module2 = m_project.GetModuleList().GetAt( m_curSelModuleIndex );

	*module1 = *module2;
	*module2 = *copyModule1;

	delete copyModule1;

	SetModifiedFlag();
	UpdateAllViews( NULL, 1000 + m_curSelModuleIndex - 1 );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleCheckModule()
{
  if ( m_curSelModuleIndex == -1 ) { return; }

	LBModule* module = m_project.GetModuleList().GetAt( m_curSelModuleIndex );
	
	if ( module->IsActive() )
	{
		module->SetActive( false );
	}
	else
	{
		module->SetActive( true );
	}

	m_project.SetModule( m_curSelModuleIndex, module );
	SetModifiedFlag();
	UpdateAllViews( NULL );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleCheckAllModules()
{
	INT_PTR modCount = m_project.GetModuleList().GetCount();
	for ( int i = 0; i < modCount; ++i )
	{
		LBModule* module = m_project.GetModuleList().GetAt( i );
		module->SetActive( true );
	}

	SetModifiedFlag();
	UpdateAllViews( NULL );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleUncheckAllModules()
{
	INT_PTR modCount = m_project.GetModuleList().GetCount();
	for ( int i = 0; i < modCount; ++i )
	{
		LBModule* module = m_project.GetModuleList().GetAt( i );
		module->SetActive( false );
	}

	SetModifiedFlag();
	UpdateAllViews( NULL );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnModuleDuplicateModule()
{
  if ( m_curSelModuleIndex == -1 ) { return; }

	LBModule* selectedModule = m_project.GetModuleList().GetAt( m_curSelModuleIndex );

	LBModule* module = new LBModule( selectedModule );

	// adds the new object to the list
	m_project.AddModule( module );
	SetModifiedFlag();
	UpdateAllViews( NULL );
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnToolsOptions()
{
	// passes the project to the dialog
	m_optionsDlg.SetOptions( &m_project.GetOptions() );
	m_optionsDlg.SetSelectedOption( OPT_DLG_NONE );
	if ( m_optionsDlg.DoModal() == IDOK )
	{
		SetModifiedFlag();
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnToolsDirectories()
{
	// passes the project to the dialog
	m_optionsDlg.SetOptions( &m_project.GetOptions() );
	m_optionsDlg.SetSelectedOption( OPT_DLG_DIRECTORIES );
	if ( m_optionsDlg.DoModal() == IDOK )
	{
		SetModifiedFlag();
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnLandBuilderRunLandBuilder2()
{
  if ( !ValidateProject( "LB2" ) ) { return; }

	string tempName = m_project.GetOutputName();
	m_runLandBuilder2Dlg.SetName( &tempName );
	if ( m_runLandBuilder2Dlg.DoModal() == IDOK )
	{
		BeginWaitCursor();

		m_project.SetOutputName( tempName );

		// retriews working folder
		string workingFolderPath = m_project.GetDirectoryList().GetWorkingFolder();

    if ( workingFolderPath[workingFolderPath.length() - 1] != '\\' )
    {
      workingFolderPath = workingFolderPath + "\\";
    }

		// checks if working folder exists
		if ( GetFileAttributes( workingFolderPath.c_str() ) == INVALID_FILE_ATTRIBUTES )
		{
			// if doesn't exists, creates it
			if ( SHCreateDirectoryEx( NULL, workingFolderPath.c_str(), NULL ) != ERROR_SUCCESS )
			{
				AfxGetMainWnd()->MessageBox( string( MSG_ERRORWORKINGFOLDER ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
				return;
			}
		}

		// saves LandBuilderConfig file
		string mainConfigName = m_project.GetOutputName() + ".cfg";
		mainConfigName = workingFolderPath + mainConfigName;
		if ( !m_project.SaveMainConfig( mainConfigName.c_str(), "LB2" ) )
		{
			AfxGetMainWnd()->MessageBox( string( MSG_ERRORMAINCONFIG ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
			return;
		}

		// saves TaskConfig files
		if ( !m_project.SaveTaskConfigs() )
		{
			AfxGetMainWnd()->MessageBox( string( MSG_ERRORTASKCONFIG ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
			return;
		}

		// copies resource files
		INT_PTR modCount = m_project.GetModuleList().GetCount();
		for ( int i = 0; i < modCount; ++i )
		{
			LBModule* item = m_project.GetModuleList().GetAt( i );
			if ( item->IsActive() )
			{
				if ( item->GetUseShapefile() )
				{
					string sourceName = item->GetShapeFileName();
					string destName   = workingFolderPath + GetFileNameFromPath( item->GetShapeFileName() );
					
					BOOL res = CopyFile( sourceName.c_str(), destName.c_str(), false );

					if ( !res )
					{
						AfxGetMainWnd()->MessageBox( string( MSG_ERRORCOPYINGSHAPEFILE ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
						return;
					}

					sourceName = sourceName.substr( 0, sourceName.size() - 3 ) + "dbf";
					destName   = destName.substr( 0, destName.size() - 3 ) + "dbf";

					res = CopyFile( sourceName.c_str(), destName.c_str(), false );

					if ( !res )
					{
						AfxGetMainWnd()->MessageBox( string( MSG_ERRORCOPYINGDBF ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
						return;
					}
				}

				if ( item->GetUseDems() != LBMUD_NONE )
				{
					INT_PTR demCount = item->GetDemsList().GetCount();
					for ( int j = 0; j < demCount; ++j )
					{
						LBDem* demItem = item->GetDemsList().GetAt( j );
					
						string sourceName = demItem->GetFileName();
						string destName   = workingFolderPath + GetFileNameFromPath( demItem->GetFileName() );

						BOOL res = CopyFile( sourceName.c_str(), destName.c_str(), false );

						if ( !res )
						{
							AfxGetMainWnd()->MessageBox( string( MSG_ERRORCOPYINGDEMFILE ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
							return;
						}

            if ( demItem->GetType() == LBDT_ARCINFOASCII )
            {
              string prjSourceName = sourceName.substr( 0, sourceName.length() - 4 ) + ".prj";
          		if ( GetFileAttributes( prjSourceName.c_str() ) != INVALID_FILE_ATTRIBUTES )
              {
                string prjDestName = destName.substr( 0, destName.length() - 4 ) + ".prj";

						    BOOL res = CopyFile( prjSourceName.c_str(), prjDestName.c_str(), false );

						    if ( !res )
						    {
							    AfxGetMainWnd()->MessageBox( string( MSG_ERRORCOPYINGDEMFILE ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONSTOP );
							    return;
						    }
              }
            }
					}
				}
			}
		}

		EndWaitCursor();

		string landBuilder2Path = m_project.GetDirectoryList().GetLandBuilder2Path();

		// launch landbuilder
		STARTUPINFO siStartupInfo;
		PROCESS_INFORMATION piProcessInfo;
		memset( &siStartupInfo, 0, sizeof( siStartupInfo ) );
		memset( &piProcessInfo, 0, sizeof( piProcessInfo ) );
		siStartupInfo.cb = sizeof( siStartupInfo );

		string cmdLine = landBuilder2Path + " " + mainConfigName;
		char* pszParam = new char[cmdLine.size() + 1];
		const char* pchrTemp = cmdLine.c_str();
		strcpy( pszParam, pchrTemp );

		if ( CreateProcess( NULL, pszParam, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &siStartupInfo, &piProcessInfo ) != false )
		{
			DWORD dwExitCode;
			GetExitCodeProcess( piProcessInfo.hProcess, &dwExitCode );

			mainConfigName = mainConfigName.substr( 0, mainConfigName.size() - 3 ) + "lbt";
			string msg = MSG_LANDBUILD2WILLCREATE + mainConfigName + "\n";
			msg += MSG_WHENTHISOPERATION;
			AfxGetMainWnd()->MessageBox( msg.c_str(), string( MSG_LANDBUILDER2ISRUNNING ).c_str(), MB_OK | MB_ICONINFORMATION );
		}
		else
		{
			AfxGetMainWnd()->MessageBox( string( MSG_ERRORRUNLANDBUILDER2 ).c_str(), string( MSG_ERROR ).c_str(), MB_OK | MB_ICONEXCLAMATION );
		}
		CloseHandle( piProcessInfo.hProcess );
		CloseHandle( piProcessInfo.hThread );
		delete[]pszParam;
		pszParam = 0;
	}
}

//-----------------------------------------------------------------------------

void CVisualLandBuilderDoc::OnLandBuilderPreview()
{
  if ( !ValidateProject( "PW" ) ) { return; }

	m_previewDlg.SetModuleList( &m_project.GetModuleList(), false, true );
	m_previewDlg.DoModal();
}

//-----------------------------------------------------------------------------
