// CSplashDlg.cpp : implementation file
//

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CSplashDlg.h"

CSplashDlg* CSplashDlg::m_SplashDlg;

// CSplashDlg dialog

IMPLEMENT_DYNAMIC(CSplashDlg, CDialog)

// -------------------------------------------------------------------------- //

CSplashDlg::CSplashDlg(CWnd* pParent /*=NULL*/)
: CDialog(CSplashDlg::IDD, pParent)
{
}

// -------------------------------------------------------------------------- //

CSplashDlg::~CSplashDlg()
{
}

// -------------------------------------------------------------------------- //

void CSplashDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_VERSION, m_VersionStaticCtrl);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CSplashDlg, CDialog)
	ON_WM_TIMER()
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

void CSplashDlg::ShowSplashScreen(CWnd* pParentWnd /*= NULL*/)
{
	// Allocate a new splash screen, and create the window.
	m_SplashDlg = new CSplashDlg;

	if (!m_SplashDlg->Create(CSplashDlg::IDD, pParentWnd))
	{
		delete m_SplashDlg;
	}
	else
	{
		m_SplashDlg->ShowWindow(SW_SHOW);
	}

	m_SplashDlg->UpdateWindow();
	m_SplashDlg->SetTimer(1, 3000, NULL);
}

// -------------------------------------------------------------------------- //

void CSplashDlg::HideSplashScreen()
{
	// Destroy the window, and update the mainframe.
	m_SplashDlg->KillTimer(1);
	DestroyWindow();
	AfxGetMainWnd()->UpdateWindow();
	delete m_SplashDlg;
	m_SplashDlg = NULL;
}

// -------------------------------------------------------------------------- //

void CSplashDlg::OnTimer(UINT_PTR nIDEvent)
{
	HideSplashScreen();
}

// -------------------------------------------------------------------------- //

BOOL CSplashDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CenterWindow();	
	SetWindowPos(&CWnd::wndTopMost, 0, 0, 0, 0,  SWP_NOMOVE|SWP_NOSIZE);

	return TRUE;  
}

