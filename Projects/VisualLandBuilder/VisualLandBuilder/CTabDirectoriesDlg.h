//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "afxcmn.h"
#include "resource.h"

#include ".\LBGlobalVariables.h"
#include ".\LBDirectoryList.h"

//-----------------------------------------------------------------------------

class CTabDirectoriesDlg : public CDialog
{
	DECLARE_DYNAMIC( CTabDirectoriesDlg )

public:
	CTabDirectoriesDlg( CWnd* parent = NULL );   // standard constructor
	virtual ~CTabDirectoriesDlg();

	enum 
  { 
    IDD = IDD_DIRECTORIES 
  };

protected:
	virtual void DoDataExchange( CDataExchange* dx );    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	// the list control
	CListCtrl m_directoryListCtrl;
	// will be set to true if the data are modified
	bool      m_modified;

	// DATA
	// the pointer to the data
	LBDirectoryList* m_directoryList;

private:
	virtual BOOL OnInitDialog();

public:
	// updates the list control and sets m_Modified if data have been changed
	void UpdateDirectoryListCtrl();

public:
	// sets the working directory list and the pointer to the original one
	//  !! must be called before the DoModal function of this dialog !!
	void SetDirectoryList( LBDirectoryList* inputList );

public:
	// resets the given directory list to the default values
	static void SetDefaultDirectoryList( LBDirectoryList* list );

private:
	afx_msg void OnNMClickDirectoryList( NMHDR* nmhdr, LRESULT* result);
	afx_msg void OnLvnItemActivateDirectoryList( NMHDR* nmhdr, LRESULT* result );
	afx_msg void OnLvnKeydownDirectoryList( NMHDR* nmhdr, LRESULT* result );

private:
	afx_msg void OnBnClickedChangeBtn();
	afx_msg void OnBnClickedDefaultBtn();
	afx_msg void OnBnClickedSaveDefaultBtn();

private:
	virtual BOOL PreTranslateMessage( MSG* msg );
};

//-----------------------------------------------------------------------------
