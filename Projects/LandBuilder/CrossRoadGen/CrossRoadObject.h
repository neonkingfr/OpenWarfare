#pragma once

#include "RoadEndDesc.h"

using namespace ObjektivLib; 

class CrossRoadObject
{
	AutoArray<RoadEndDesc> _roads;
	HBITMAP _texMask;
	HDC _texDC;
public:
	float bezierTeselationFactor;
	int bezierMaxRecursions;
	int _curLodLevel;
	RString _textureFolder;
	RString _textureName;
	int _textureSize;

	CrossRoadObject(const Array<RoadEndDesc>& _roads);
	~CrossRoadObject();

	void SetTextureParams(RString textureName, RString texturePathname, int textureSize);

	void GenerateCrossRoad(ObjectData& object, int level = 0);
	void GenerateCrossRoad(LODObject& object, int levels);

	void GenerateMemory(ObjectData& object);

	void GenerateTextureBitmap(ObjectData& object, int level);
  
	bool SaveTexture();
};
