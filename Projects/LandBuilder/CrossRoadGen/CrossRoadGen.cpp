// CrossRoadGen.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CrossRoadObject.h"

static Vector3 ReadVector(const IParamArrayValue& val)
{
	if (val.GetItemCount() != 3) throw new std::exception("Excepting vector (3 items in array)");

	return Vector3(val[0], val[1], val[2]);
} 

static void CenterAndPrintResult(const char* name, LODObject& obj)
{
	Vector3 res = obj.CenterAll();
	std::cout << name << std::endl << "Center: " << res[0] << " " << res[1] << " " << res[2] << std::endl;
}

static void DoGenScript(const ParamEntry& pfile)
{
	ParamEntryPtr pentry = pfile.FindEntry("RoadEnds");
	if (pentry.IsNull()) throw new std::exception("Missing 'RoadEnds' array");
    
	AutoArray<RoadEndDesc> roads;

	if (pentry->IsArray())
	{
		int cnt = pentry->GetSize();
		roads.Reserve(cnt);
		for (int i = 0; i < cnt; i++)
		{
			const IParamArrayValue& val = pentry->operator[](i);
			if (val.IsArrayValue())
			{
				int cc1 = val.GetItemCount();
				if (cc1 == 5)
				{
					Vector3 v1 = ReadVector(val[0]);
					Vector3 v2 = ReadVector(val[1]);
					Vector3 v3 = ReadVector(val[2]);
					Vector3 v4 = ReadVector(val[3]);
					float strength = val[4];
					roads.Add(RoadEndDesc(v1, v2, v3, v4, strength));    
				}
				else
				{
				throw new std::exception("For one road end there must be exactly 5 items");
				}
			}
			else
			{
				throw new std::exception("Each road end must be an array");
			}
		}
	}
	else
	{
		throw new std::exception("Entry 'RoadEnds' must be an array");
	}
  
	pentry = pfile.FindEntry("output");
	if (pentry.IsNull()) throw new std::exception("Missing 'output = <p3d>' field");
	RString objectFile = pentry->GetValue();

	pentry = pfile.FindEntry("texture");
	if (pentry.IsNull()) throw new std::exception("Missing 'texture = <tga>' field");
	RString textureFile = pentry->GetValue();

    bool textureExists = false;
	pentry = pfile.FindEntry("textureExists");
	if (!pentry.IsNull()) textureExists = pentry->operator int() != 0;

    pentry = pfile.FindEntry("textureBase");
	if (pentry.IsNull()) throw new std::exception("Missing 'textureBase = <path>' field");
	RString texPrefix = pentry->GetValue();

	pentry = pfile.FindEntry("levels");
	if (pentry.IsNull()) throw new std::exception("Missing 'levels = <int>' field");
	int levels = pentry->operator int();

	pentry = pfile.FindEntry("textureSize");
	if (pentry.IsNull()) throw new std::exception("Missing 'textureSize = <int>' field");
	int textureSize = pentry->operator int();

	bool autocenter = false;
	pentry = pfile.FindEntry("autocenter");
	if (!pentry.IsNull() && pentry->operator bool()) autocenter = true;

	CrossRoadObject cross(roads);
	LODObject lodobj;

	Pathname outp3d = objectFile;  
	Pathname outtga = textureFile;
	RString texname = outtga.FullToRelativeProjectRoot(textureFile, texPrefix);
	cross.SetTextureParams(texname, textureFile, textureSize);
	outtga.CreateFolder();
	cross.GenerateCrossRoad(lodobj, levels);
	if (autocenter) CenterAndPrintResult(outp3d, lodobj);
    if (!textureExists)
	{
	    if (!cross.SaveTexture()) 
		{
            throw new std::exception("Unable to save output file (tga)");
		}
	}
	std::cout << "Texture: " << outtga.GetFullPath() << std::endl;
	if (lodobj.Save(outp3d, OBJDATA_LATESTVERSION, false, 0, 0) != 0) throw new std::exception("Unable to save output file (p3d)");
	std::cout << "Done" << std::endl;
}

class QIStdStream : public QIStream
{
	std::istream& in;
public:
	QIStdStream(istream& in)
	: in(in) 
	{
	}

	virtual PosQIStreamBuffer ReadBuffer(PosQIStreamBuffer& buf, QFileSize pos) 
	{
		buf = PosQIStreamBuffer(new QIStreamBuffer(256), pos, 256);
		in.read((char*)buf->DataLock(0, buf->GetSize()), buf->GetSize());
		buf->DataUnlock(0, buf->GetSize());
		return PosQIStreamBuffer(buf, pos, in.gcount());
	}

	virtual bool PreloadBuffer(RequestableObject* obj, RequestableObject::RequestContext* ctx,
							   FileRequestPriority priority, QFileSize pos, QFileSize size) const 
	{
		return true;
	}

	virtual QFileSize GetDataSize() const 
	{
		return 0xFFFFFFFF;
	};

	~QIStdStream()
	{
		_buf.Free();
	}
};

class BatchFunctor
{
public:
	bool operator()(const ParamEntryVal& val) const
	{
		if (val.IsClass() && val.FindEntry("RoadEnds").NotNull())
		{
			DoGenScript(*val.GetPointer());
		}
		return false;
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	std::istream* definput = &std::cin;
	std::ifstream fin;
	if (argc == 2)
	{    
		fin.open(argv[1]);
		if (!fin)
		{
			fprintf(stderr, "Unable to open: %s\n", argv);
			return 2;
		}
		definput = &fin;
	}

    try
    {
		QIStdStream in(*definput);
		ParamFile pf;
		pf.Parse(in);
		pf.ForEachEntry(BatchFunctor());
      
		return 0;      
    }
    catch (std::exception* exp)
    {
		fprintf(stderr, "Error: %s\n", exp->what());
		delete exp;
		return -1;
    }
}
