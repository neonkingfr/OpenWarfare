// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#include <stdio.h>
#include <tchar.h>

#include <utility>
#include <el/Math/math3d.hpp>
#include <es/containers/array.hpp>
#include <es/algorithms/qsort.hpp>
#include <projects/ObjektivLib/ObjectData.h>
#include <projects/ObjektivLib/LODObject.h>
#include <projects/ObjektivLib/ObjToolTopology.h>
#include <el/Pathname/Pathname.h>

#include <el/ParamFile/ParamFile.hpp>
#include <iostream>
#include <El/PreprocC/preprocC.hpp>

#include <picture.hpp>


#include <windows.h>

// TODO: reference additional headers your program requires here
