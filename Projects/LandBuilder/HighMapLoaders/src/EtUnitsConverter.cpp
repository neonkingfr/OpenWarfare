#include "..\include\StdAfx.h"
#include "..\include\EtUnitsConverter.h"
#include "..\include\EtMath.h"

// --------------------------------------------------------------------------------
// FLOAT
// --------------------------------------------------------------------------------
// ANGLES
// --------------------------------------------------------------------------------
template<> const float EtUnitsConverter<float>::ANG_DEG_TO_SEC = static_cast<float>(3600.0);
template<> const float EtUnitsConverter<float>::ANG_DEG_TO_RAD = static_cast<float>(EtMathF::PI / 180.0);
template<> const float EtUnitsConverter<float>::ANG_MIN_TO_SEC = static_cast<float>(60.0);
template<> const float EtUnitsConverter<float>::ANG_RAD_TO_DEG = static_cast<float>(180.0 / EtMathF::PI);
template<> const float EtUnitsConverter<float>::ANG_RAD_TO_SEC = static_cast<float>(180.0 * 3600.0 / EtMathF::PI);
template<> const float EtUnitsConverter<float>::ANG_SEC_TO_RAD = static_cast<float>(EtMathF::PI / (180.0 * 3600.0));
// --------------------------------------------------------------------------------
// LENGHTS
// --------------------------------------------------------------------------------
template<> const float EtUnitsConverter<float>::LEN_FEET_TO_METERS = static_cast<float>(0.3048006096012);
template<> const float EtUnitsConverter<float>::LEN_METERS_TO_FEET = static_cast<float>(3.2808333332173);
// --------------------------------------------------------------------------------


// --------------------------------------------------------------------------------
// DOUBLE
// --------------------------------------------------------------------------------
// ANGLES
// --------------------------------------------------------------------------------
template<> const double EtUnitsConverter<double>::ANG_DEG_TO_SEC = 3600.0;
template<> const double EtUnitsConverter<double>::ANG_DEG_TO_RAD = EtMathD::PI / 180.0;
template<> const double EtUnitsConverter<double>::ANG_MIN_TO_SEC = 60.0;
template<> const double EtUnitsConverter<double>::ANG_RAD_TO_DEG = 180.0 / EtMathD::PI;
template<> const double EtUnitsConverter<double>::ANG_RAD_TO_SEC = 180.0 * 3600.0 / EtMathD::PI;
template<> const double EtUnitsConverter<double>::ANG_SEC_TO_RAD = EtMathD::PI / (180.0 * 3600.0);
// --------------------------------------------------------------------------------
// LENGHTS
// --------------------------------------------------------------------------------
template<> const double EtUnitsConverter<double>::LEN_FEET_TO_METERS = 0.3048006096012;
template<> const double EtUnitsConverter<double>::LEN_METERS_TO_FEET = 3.2808333332173;
// --------------------------------------------------------------------------------

