// ************************************************************************** //
// file          : ETLibs\Math\EtVector3.inl                                  //
// ************************************************************************** //
// Author        : Enrico Turri                                               //
//                                                                            //
// since         : 13.01.2007                                                 //
// last revision : 21.01.2007                                                 //
// ************************************************************************** //

// ************************************************************************** //
// Default constructor                                                        //
// ************************************************************************** //

template <class Real>
EtVector3<Real>::EtVector3()
{
}

// ************************************************************************** //
// Other constructors                                                         //
// ************************************************************************** //

template <class Real>
EtVector3<Real>::EtVector3(Real x, Real y, Real z) :
m_X(x), m_Y(y), m_Z(z)
{
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real>::EtVector3(const Real xyz[3])
{
	memcpy(m_XYZ, xyz, 3 * sizeof(Real));
}

// ************************************************************************** //
// Copy constructor                                                           //
// ************************************************************************** //

template <class Real>
EtVector3<Real>::EtVector3(const EtVector3<Real>& other) :
m_X(other.m_X), m_Y(other.m_Y), m_Z(other.m_Z)
{
}

// ************************************************************************** //
// Assignement                                                                //
// ************************************************************************** //

template <class Real>
EtVector3<Real>& EtVector3<Real>::operator = (const EtVector3<Real>& other)
{
	memcpy(m_XYZ, other.m_XYZ, 3 * sizeof(Real));
	return *this;
}

// ************************************************************************** //
// Comparison                                                                 //
// ************************************************************************** //

template <class Real>
bool EtVector3<Real>::operator == (const EtVector3<Real>& other) const
{
	if(EtMath<Real>::Fabs(m_X - other.m_X) > EtMath<Real>::ZERO_TOLERANCE) return false;
	if(EtMath<Real>::Fabs(m_Y - other.m_Y) > EtMath<Real>::ZERO_TOLERANCE) return false;
	if(EtMath<Real>::Fabs(m_Z - other.m_Z) > EtMath<Real>::ZERO_TOLERANCE) return false;

	return true;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool EtVector3<Real>::operator != (const EtVector3<Real>& other) const
{
	return !(operator ==);
}

// ************************************************************************** //
// Arithmetic operations                                                      //
// ************************************************************************** //

template <class Real>
EtVector3<Real> EtVector3<Real>::operator + (const EtVector3<Real>& other) const 
{
	return EtVector3<Real>(m_X + other.m_X, m_Y + other.m_Y, m_Z + other.m_Z);
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real> EtVector3<Real>::operator - (const EtVector3<Real>& other) const
{
	return EtVector3<Real>(m_X - other.m_X, m_Y - other.m_Y, m_Z - other.m_Z);
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real> EtVector3<Real>::operator * (const EtMatrix3x3<Real>& matrix) const
{
	return EtVector3<Real>(m_X * matrix(0, 0) + m_Y * matrix(1, 0) + m_Z * matrix(2, 0),
		                   m_X * matrix(0, 1) + m_Y * matrix(1, 1) + m_Z * matrix(2, 1),
		                   m_X * matrix(0, 2) + m_Y * matrix(1, 2) + m_Z * matrix(2, 2));
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real> EtVector3<Real>::operator * (Real scalar) const
{
	return EtVector3<Real>(m_X * scalar, m_Y * scalar, m_Z * scalar);
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::operator * (const EtVector3<Real>& other) const
{
	return Dot(other);
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real> EtVector3<Real>::operator / (Real scalar) const
{
    if (scalar != static_cast<Real>(0.0))
    {
        Real invScalar = static_cast<Real>(1.0) / scalar;
		return EtVector3<Real>(m_X * invScalar, m_Y * invScalar, m_Z * invScalar);
    }
    else
    {
		return EtVector3<Real>(EtMath<Real>::MAX_REAL, EtMath<Real>::MAX_REAL, EtMath<Real>::MAX_REAL);
    }
}

// ************************************************************************** //
// Arithmetic updates                                                         //
// ************************************************************************** //

template <class Real>
EtVector3<Real>& EtVector3<Real>::operator += (const EtVector3<Real>& other)
{
	m_X += other.m_X;
	m_Y += other.m_Y;
	m_Z += other.m_Z;

	return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real>& EtVector3<Real>::operator -= (const EtVector3<Real>& other)
{
	m_X -= other.m_X;
	m_Y -= other.m_Y;
	m_Z -= other.m_Z;

	return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real>& EtVector3<Real>::operator *= (Real scalar)
{
	m_X *= scalar;
	m_Y *= scalar;
	m_Z *= scalar;

	return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real>& EtVector3<Real>::operator /= (Real scalar)
{
    if (scalar != static_cast<Real>(0.0))
    {
        Real invScalar = static_cast<Real>(1.0) / scalar;
		m_X *= invScalar;
		m_Y *= invScalar;
		m_Z *= invScalar;
    }
    else
    {
		m_X = EtMath<Real>::MAX_REAL;
		m_Y = EtMath<Real>::MAX_REAL;
		m_Z = EtMath<Real>::MAX_REAL;
    }

	return *this;
}

// ************************************************************************** //
// Negation                                                                   //
// ************************************************************************** //

template <class Real>
EtVector3<Real> EtVector3<Real>::operator - () const
{
	return EtVector3(-m_X, -m_Y, -m_Z);
}

// ************************************************************************** //
// Vector properties                                                          //
// ************************************************************************** //

template <class Real>
Real EtVector3<Real>::Length() const
{
	return EtMath<Real>::Sqrt(SquaredLength());
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::Size() const
{
	return Length();
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::Magnitude() const
{
	return Length();
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::SquaredLength() const
{
	return (m_X * m_X + m_Y * m_Y + m_Z * m_Z);
}

// ************************************************************************** //
// Vector operations                                                          //
// ************************************************************************** //

template <class Real>
void EtVector3<Real>::Normalize()
{
    Real length = Length();

    if (length > EtMath<Real>::ZERO_TOLERANCE)
    {
        Real invLength = (static_cast<Real>(1.0)) / length;
        m_X *= invLength;
        m_Y *= invLength;
        m_Z *= invLength;
    }
    else
    {
        m_X = static_cast<Real>(0.0);
        m_Y = static_cast<Real>(0.0);
        m_Z = static_cast<Real>(0.0);
    }
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::Dot(const EtVector3<Real>& other) const
{
    return m_X * other.m_X + m_Y * other.m_Y + m_Z * other.m_Z;
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real> EtVector3<Real>::Cross(const EtVector3<Real>& other) const
{
    return EtVector3(m_Y * other.m_Z - m_Z * other.m_Y,
			  	     m_Z * other.m_X - m_X * other.m_Z,
				     m_X * other.m_Y - m_Y * other.m_X);
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real> EtVector3<Real>::ProjectInto(const EtVector3<Real>& other) const
{
	// copy of the given vector
	EtVector3<Real> temp = other;
	// normalizes the copy
	temp.Normalize();
	// dot product to obtain the projection of this vector along
	// the given vector
	Real dot = Dot(temp);
	// multiply the temp vector times the projection and return the result
	return (temp * dot);
}

// -------------------------------------------------------------------------- //

template <class Real>
EtVector3<Real> EtVector3<Real>::ProjectIntoPerpTo(const EtVector3<Real>& other) const
{
	// gets the component of this vector along the given vector
	EtVector3<Real> temp = ProjectInto(other);
	// returns the vector difference between this and the temp vectors
	return EtVector3<Real>(m_X - temp.m_X, m_Y - temp.m_Y, m_Z - temp.m_Z);
}

// ************************************************************************** //
// Member variables getters                                                   //
// ************************************************************************** //

template <class Real>
Real EtVector3<Real>::GetX() const
{
	return m_X;
}	

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::X() const	
{
	return GetX();
}	

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::GetY() const
{
	return m_Y;
}	

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::Y() const
{
	return GetY();
}	

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::GetZ() const
{
	return m_Z;
}	

// -------------------------------------------------------------------------- //

template <class Real>
Real EtVector3<Real>::Z() const
{
	return GetZ();
}	

// ************************************************************************** //
// Member variables setters                                                   //
// ************************************************************************** //

template <class Real>
void EtVector3<Real>::Set(Real newX, Real newY, Real newZ)
{
	m_X = newX;
	m_Y = newY;
	m_Z = newZ;
}

// -------------------------------------------------------------------------- //

template <class Real>
void EtVector3<Real>::SetX(Real newX)
{
	m_X = newX;
}

// -------------------------------------------------------------------------- //

template <class Real>
void EtVector3<Real>::X(Real newX)		
{
	SetX(newX);
}

// -------------------------------------------------------------------------- //

template <class Real>
void EtVector3<Real>::SetY(Real newY)
{
	m_Y = newY;
}

// -------------------------------------------------------------------------- //

template <class Real>
void EtVector3<Real>::Y(Real newY)		
{
	SetY(newY);
}

// -------------------------------------------------------------------------- //

template <class Real>
void EtVector3<Real>::SetZ(Real newZ)
{
	m_Z = newZ;
}

// -------------------------------------------------------------------------- //

template <class Real>
void EtVector3<Real>::Z(Real newZ)		
{
	SetZ(newZ);
}

// ************************************************************************** //
// Non-member functions                                                       //
// ************************************************************************** //

// ************************************************************************** //
// Overloaded arithmetic operations                                           //
// ************************************************************************** //

template <class Real>
EtVector3<Real> operator * (Real scalar, const EtVector3<Real>& v)
{
	return EtVector3<Real>(v.X() * scalar, v.Y() * scalar, v.Z() * scalar);
}

// ************************************************************************** //
// Serialization and debug                                                    //
// ************************************************************************** //

template <class Real>
ostream& operator << (ostream& stream, const EtVector3<Real>& v)
{
	return stream << "Vector3 ("<< v.X() << ", " << v.Y() << ", " << v.Z() << ")";
}

// ************************************************************************** //
