#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <set>

#include "..\include\EtMath.h"
#include "..\include\EtUnitsConverter.h"
#include "..\include\EtString.h"
#include "..\include\EtVector3.h"
#include "..\include\EtHelperFunctions.h"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::ios;
using std::set;
using std::setprecision;
using std::setw;
using std::setfill;

// --------------------------------------------------------------------------------
// CONSTRUCTORS
// --------------------------------------------------------------------------------

// constructs an empty grid
template < class Real, class Integer >
EtRectElevationGrid< Real, Integer >::EtRectElevationGrid()
{
	// ensures the pointer to the array of data doesn't point anywhere
	m_W = 0;

	// default units are meters for all coordinates
	setDefaultWorldUnits();
	
	// clears all data
	clear();
}

// --------------------------------------------------------------------------------

// constructs an empty grid with the specified u and v sizes
template < class Real, class Integer >
EtRectElevationGrid< Real, Integer >::EtRectElevationGrid( size_t sizeU, size_t sizeV )
{
	// ensures the pointer to the array of data doesn't point anywhere
	m_W = 0;

	// default units are meters for all coordinates
	setDefaultWorldUnits();
	
	// clears all data
	clear();

	// sets sizes and initializes elevation array
	setUVSize( sizeU, sizeV, true );
}

// --------------------------------------------------------------------------------
// COPY CONSTRUCTOR
// --------------------------------------------------------------------------------

template < class Real, class Integer >
EtRectElevationGrid< Real, Integer >::EtRectElevationGrid( const EtRectElevationGrid< Real, Integer >& other )
{
	// ensures the pointer to the array of data doesn't point anywhere
	m_W = 0;

	// copies sizes and initializes elevation array
	setUVSize( other.m_SizeU, other.m_SizeV, false );
	m_SizeW = other.m_SizeW;

	// copies elevation array
	for ( size_t i = 0; i < m_SizeU * m_SizeV; ++i )
	{
		m_W[i] = other.m_W[i];
	}

	// copies resolutions
	m_ResolutionU = other.m_ResolutionU;
	m_ResolutionV = other.m_ResolutionV;
	m_ResolutionW = other.m_ResolutionW;

	// copies World units
	m_WorldUnitsU = other.m_WorldUnitsU;
	m_WorldUnitsV = other.m_WorldUnitsV;
	m_WorldUnitsW = other.m_WorldUnitsW;

	// copies origin
	m_OriginWorldU = other.m_OriginWorldU;
	m_OriginWorldV = other.m_OriginWorldV;
	m_OriginWorldW = other.m_OriginWorldW;
}

// --------------------------------------------------------------------------------
// DESTRUCTOR
// --------------------------------------------------------------------------------

template <class Real, class Integer>
EtRectElevationGrid< Real, Integer >::~EtRectElevationGrid()
{
	// clears all data
	clear();
}

// --------------------------------------------------------------------------------
// ASSIGNEMENT
// --------------------------------------------------------------------------------

template < class Real, class Integer >
EtRectElevationGrid< Real, Integer >& EtRectElevationGrid< Real, Integer >::operator = ( const EtRectElevationGrid< Real, Integer >& other )
{
	if ( this != &other )
	{
		if ( !empty() )
			// clears all data
			clear();

		// copies sizes and initializes elevation array
		setUVSize( other.m_SizeU, other.m_SizeV, false );
		m_SizeW = other.m_SizeW;

		// copies elevation array
		for ( size_t i = 0; i < m_SizeU * m_SizeV; ++i )
		{
			m_W[i] = other.m_W[i];
		}

		// copies resolutions
		m_ResolutionU = other.m_ResolutionU;
		m_ResolutionV = other.m_ResolutionV;
		m_ResolutionW = other.m_ResolutionW;

		// copies World units
		m_WorldUnitsU = other.m_WorldUnitsU;
		m_WorldUnitsV = other.m_WorldUnitsV;
		m_WorldUnitsW = other.m_WorldUnitsW;

		// copies origin
		m_OriginWorldU = other.m_OriginWorldU;
		m_OriginWorldV = other.m_OriginWorldV;
		m_OriginWorldW = other.m_OriginWorldW;
	}

	return (*this);
}

// --------------------------------------------------------------------------------
// INTERNAL DATA METHODS
// --------------------------------------------------------------------------------

// sets the u and v grid sizes
// if the grid is not empty deletes all old data
// return false if the memory allocation fails
// if fillW is true the elevation array is filled with the NO_ELEVATION value
template < class Real, class Integer >
bool EtRectElevationGrid< Real, Integer >::setUVSize( const size_t extU, const size_t extV, bool fillW )
{
	// if the grid is not empty -> resets it
	if(!empty())
	{
		// clears all data
		clear();
		// default units are meters for all coordinates
		setDefaultWorldUnits();
	}

	// assign new values for sizes
	m_SizeU = extU;
	m_SizeV = extV;

	// alloc memory for elevation data
	// if fillW is true the elevation array is filled with the NO_ELEVATION value
	return allocW(fillW);
}

// --------------------------------------------------------------------------------

// swaps u and v coordinates of this grid
// if worldToo is true swaps also World origin, units and resolutions u and v
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::swapUV(bool worldToo)
{
	// creates a temp grid
	EtRectElevationGrid tempGrid;

	// sets temp grid sizes and initializes w array
	tempGrid.setUVSize(m_SizeV, m_SizeU, false);

	// copies w swapping u and v
	for(unsigned int i = 0; i < m_SizeU; ++i)
	{
		for(unsigned int j = 0; j < m_SizeV; ++j)
		{
			tempGrid.setWAt(j, i, this->getWAt(i, j));
		}
	}

	// normalizes w
	tempGrid.normalizeW();

	// sets world quantities
	// if worldToo is true swap also world quantities
	if(worldToo)
	{
		// sets World units u and v
		tempGrid.setWorldUnitsU(this->m_WorldUnitsV);
		tempGrid.setWorldUnitsV(this->m_WorldUnitsU);

		// sets resolutions u and v
		tempGrid.setResolutionU(this->m_ResolutionV);
		tempGrid.setResolutionV(this->m_ResolutionU);

		// sets origin u and v
		tempGrid.setOriginWorldU(this->m_OriginWorldV);
		tempGrid.setOriginWorldV(this->m_OriginWorldU);
	}
	else
	{
		// sets World units u and v
		tempGrid.setWorldUnitsU(this->m_WorldUnitsU);
		tempGrid.setWorldUnitsV(this->m_WorldUnitsV);

		// sets resolutions u and v
		tempGrid.setResolutionU(this->m_ResolutionU);
		tempGrid.setResolutionV(this->m_ResolutionV);

		// sets origin u and v
		tempGrid.setOriginWorldU(this->m_OriginWorldU);
		tempGrid.setOriginWorldV(this->m_OriginWorldV);
	}

	// sets World units, resolutions  and origin w
	tempGrid.setWorldUnitsW(this->m_WorldUnitsW);
	tempGrid.setResolutionW(this->m_ResolutionW);
	tempGrid.setOriginWorldW(this->m_OriginWorldW);

	*this = tempGrid;
}

// --------------------------------------------------------------------------------

// mirrors the ws on u coordinate
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::mirrorU()
{
	// creates a temp grid
	EtRectElevationGrid tempGrid;

	// sets temp grid sizes and initializes w array
	tempGrid.setUVSize(m_SizeU, m_SizeV, false);

	// copies w mirroring u
	for(unsigned int i = 0; i < m_SizeU; ++i)
	{
		for(unsigned int j = 0; j < m_SizeV; ++j)
		{
			tempGrid.setWAt(i, j, this->getWAt((m_SizeU - 1) - i, j));
		}
	}

	// normalizes w
	tempGrid.normalizeW();

	// sets World units
	tempGrid.setWorldUnitsU(this->m_WorldUnitsU);
	tempGrid.setWorldUnitsV(this->m_WorldUnitsV);
	tempGrid.setWorldUnitsW(this->m_WorldUnitsW);

	// sets resolutions
	tempGrid.setResolutionU(this->m_ResolutionU);
	tempGrid.setResolutionV(this->m_ResolutionV);
	tempGrid.setResolutionW(this->m_ResolutionW);
	
	// sets origin
	tempGrid.setOriginWorldU(this->m_OriginWorldU);
	tempGrid.setOriginWorldV(this->m_OriginWorldV);
	tempGrid.setOriginWorldW(this->m_OriginWorldW);

	*this = tempGrid;
}

// --------------------------------------------------------------------------------

// mirrors the ws on v coordinate
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::mirrorV()
{
	// creates a temp grid
	EtRectElevationGrid tempGrid;

	// sets temp grid sizes and initializes w array
	tempGrid.setUVSize(m_SizeU, m_SizeV, false);

	// copies w mirroring v
	for(unsigned int i = 0; i < m_SizeU; ++i)
	{
		for(unsigned int j = 0; j < m_SizeV; ++j)
		{
			tempGrid.setWAt(i, j, this->getWAt(i, (m_SizeV - 1) - j));
		}
	}

	// normalizes w
	tempGrid.normalizeW();

	// sets World units
	tempGrid.setWorldUnitsU(this->m_WorldUnitsU);
	tempGrid.setWorldUnitsV(this->m_WorldUnitsV);
	tempGrid.setWorldUnitsW(this->m_WorldUnitsW);

	// sets resolutions
	tempGrid.setResolutionU(this->m_ResolutionU);
	tempGrid.setResolutionV(this->m_ResolutionV);
	tempGrid.setResolutionW(this->m_ResolutionW);

	// sets origin
	tempGrid.setOriginWorldU(this->m_OriginWorldU);
	tempGrid.setOriginWorldV(this->m_OriginWorldV);
	tempGrid.setOriginWorldW(this->m_OriginWorldW);

	*this = tempGrid;
}

// --------------------------------------------------------------------------------

template < class Real, class Integer >
size_t EtRectElevationGrid< Real, Integer >::getSizeU() const 
{
	return (m_SizeU);
}

// --------------------------------------------------------------------------------

template < class Real, class Integer >
size_t EtRectElevationGrid< Real, Integer >::getSizeV() const 
{
	return (m_SizeV);
}

// --------------------------------------------------------------------------------

template < class Real, class Integer >
size_t EtRectElevationGrid< Real, Integer >::getSizeW() const 
{
	return (m_SizeW);
}

// --------------------------------------------------------------------------------

template < class Real, class Integer >
Integer EtRectElevationGrid< Real, Integer >::getWAt( size_t u, size_t v ) const 
{
	// checks u and v
  if ( (u >= m_SizeU) || (v >= m_SizeV) ) { return (NO_ELEVATION); }

	return (m_W[w_Index( u, v )]);
}

// --------------------------------------------------------------------------------

// sets the w at node (u, v) of the grid
// no resampling of the elevation array is done in this method
// so the method normalizeW() should be called after the call to 
// this method
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setWAt( size_t u, size_t v, Integer newElevation )
{
	// checks u and v
  if ( (u >= m_SizeU) || (v >= m_SizeV) ) { return; }

	m_W[w_Index( u, v )] = newElevation;
}

// --------------------------------------------------------------------------------

// sets the w at node (u, v) of the grid 
// no resampling of the elevation array is done in this method
// so the method normalizeW() should be called after the call to 
// this method
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setWorldWAt( size_t u, size_t v, Real worldElevation )
{
	// checks u and v
  if ( (u >= m_SizeU) || (v >= m_SizeV) ) { return; }

	// computes grid W
	Real newW = (worldElevation - m_OriginWorldW) / m_ResolutionW;

	m_W[w_Index( u, v )] = static_cast< Integer >( newW );
}

// --------------------------------------------------------------------------------

// sets all the ws to the same value
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setAllWToSingleValue( Integer value )
{
	for ( size_t i = 0; i < m_SizeU; ++i )
	{		
		for ( size_t j = 0; j < m_SizeV; ++j )
		{		
			setWAt( i, j, value );
		}
	}
}

// --------------------------------------------------------------------------------

template <class Real, class Integer>
void EtRectElevationGrid< Real, Integer >::setAllWorldWToSingleValue( Real value )
{
	for ( size_t i = 0; i < m_SizeU; ++i )
	{		
		for ( size_t j = 0; j < m_SizeV; ++j )
		{		
			setWorldWAt( i, j, value );
		}
	}
}

// --------------------------------------------------------------------------------

// returns the first non NO_ELEVATION w
// assumes that the ws are normalized
// returns NO_ELEVATION only if all the w are NO_ELEVATION
template <class Real, class Integer>
Integer EtRectElevationGrid<Real, Integer>::getFirstValidW() const 
{
	for(unsigned int i = 0; i < m_SizeU; ++i)
	{
		for(unsigned int j = 0; j < m_SizeV; ++j)
		{
			Integer elev = getWAt(i, j);
			if(elev != NO_ELEVATION)
			{
				return elev;
			}
		}
	}
	return NO_ELEVATION;
}

// --------------------------------------------------------------------------------

// returns the min valid w (always equal to 0)
// assumes that the ws are normalized
// returns NO_ELEVATION only if all the w are NO_ELEVATION
template <class Real, class Integer>
Integer EtRectElevationGrid<Real, Integer>::getMinValidW() const
{
	// no valid elevations
	if(m_SizeW == 0) return NO_ELEVATION;

	// the min valid W is always 0
	return 0;
}

// --------------------------------------------------------------------------------

// returns the max valid w (always equal to (m_SizeW - 1))
// assumes that the ws are normalized
// returns NO_ELEVATION only if all the w are NO_ELEVATION
template <class Real, class Integer>
Integer EtRectElevationGrid<Real, Integer>::getMaxValidW() const 
{
	// no valid elevations
	if(m_SizeW == 0) return NO_ELEVATION;

	return (m_SizeW - 1);
}

// --------------------------------------------------------------------------------

// normalize all the ws to be in the range [0..(m_SizeW -1)]
// updates also origin World w
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::normalizeW()
{
	Integer minW = NO_ELEVATION;
	Integer maxW = NO_ELEVATION;

	// searches for minW and maxW
	for ( size_t i = 0; i < m_SizeU; ++i )
	{
		for ( size_t j = 0; j < m_SizeV; ++j )
		{
			Integer elev = getWAt( i, j );
			if ( elev != NO_ELEVATION )
			{
				if ( minW == NO_ELEVATION )
				{
					minW = elev;
				}
				else
				{
					if ( minW > elev )
					{
						minW = elev;
					}
				}

				if ( maxW == NO_ELEVATION )
				{
					maxW = elev;
				}
				else
				{
					if ( maxW < elev )
					{
						maxW = elev;
					}
				}
			}
		}
	}

	// no valid elevations
	if ( minW == NO_ELEVATION )
	{
		m_SizeW = 0;
		return;
	}

	// already normalized
	if ( minW == 0 )
	{
		m_SizeW = maxW + 1;
		return;
	}

	// normalizes
	m_SizeW = maxW - minW + 1;

	for ( size_t i = 0; i < m_SizeU; ++i )
	{
		for ( size_t j = 0; j < m_SizeV; ++j )
		{
			Integer elev = getWAt( i, j );
			setWAt( i, j, elev - minW );
		}
	}

	// updates origin World w
	m_OriginWorldW += minW * m_ResolutionW;
}

// --------------------------------------------------------------------------------

// offsets the w of the sub grid of this grid whose
// SW and NE corners have the specified coordinates
// by the specified value
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::offsetW(const unsigned int swCornerU, const unsigned int swCornerV, const unsigned int neCornerU, const unsigned int neCornerV, const Integer value) const
{
	// updates w
	for(unsigned int i = swCornerU; i < neCornerU; ++i)
	{
		for(unsigned int j = swCornerV; j < neCornerV; ++j)
		{
			Integer elev = getWAt(i, j);
			subGrid.setWAt(i, j, elev + value);
		}
	}

	// normalizes W
	normalizeW();
}

// --------------------------------------------------------------------------------

// adds the ws of the specified grid to the ones of this grid
// in the area where the 2 grids overlap
// the values offsetU and offsetV allow to place the origin
// of the specified grid in the desired position with respect to the
// origin of this grid
// no addition take places where there are NO_ELEVATION values in either grid
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::addW(const int offsetU, const int offsetV, const EtRectElevationGrid& other)
{
	// adds w
	for(unsigned int i = 0; i < other.m_SizeU; ++i)
	{
		for(unsigned int j = 0; j < other.m_SizeV; ++j)
		{
			// if overlap
			if(((i + offsetU) >= 0) && ((i + offsetU) < m_SizeU) && 
			   ((j + offsetV) >= 0) && ((j + offsetV) < m_SizeV))
			{
				Integer elev = getWAt(i + offsetU, j + offsetV);
				if(elev != NO_ELEVATION)
				{
					Integer otherElev = other.getWAt(i, j);
					if(otherElev != NO_ELEVATION)
					{
						setWAt(i + offsetU, j + offsetV, elev + otherElev);
					}
				}
			}
		}
	}

	// normalizes W
	normalizeW();
}

// --------------------------------------------------------------------------------
// WORLD DATA METHODS
// --------------------------------------------------------------------------------

// returns the World units of u coordinate
template <class Real, class Integer>
int EtRectElevationGrid<Real, Integer>::getWorldUnitsU() const 
{
	return m_WorldUnitsU;
}

// --------------------------------------------------------------------------------

// returns the World units of v coordinate
template <class Real, class Integer>
int EtRectElevationGrid<Real, Integer>::getWorldUnitsV() const 
{
	return m_WorldUnitsV;
}

// --------------------------------------------------------------------------------

// returns the World units of w coordinate
template <class Real, class Integer>
int EtRectElevationGrid<Real, Integer>::getWorldUnitsW() const 
{
	return m_WorldUnitsW;
}

// --------------------------------------------------------------------------------

// returns the World units of u coordinate as string
template <class Real, class Integer>
string EtRectElevationGrid<Real, Integer>::getWorldUnitsUAsString() const 
{
	return surfaceUnitsAsString(m_WorldUnitsU);
}

// --------------------------------------------------------------------------------

// returns the World units of v coordinate as string
template <class Real, class Integer>
string EtRectElevationGrid<Real, Integer>::getWorldUnitsVAsString() const 
{
	return surfaceUnitsAsString(m_WorldUnitsU);
}

// --------------------------------------------------------------------------------

// returns the World units of w coordinate as string
template <class Real, class Integer>
string EtRectElevationGrid<Real, Integer>::getWorldUnitsWAsString() const 
{
	return elevationUnitsAsString(m_WorldUnitsW);
}

// --------------------------------------------------------------------------------

// sets the World units of u coordinate
// only setting (no conversion)
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::setWorldUnitsU(const SurfaceUnits newWorldUnitsU)
{
	m_WorldUnitsU = newWorldUnitsU;
}

// --------------------------------------------------------------------------------

// sets the World units of v coordinate
// only setting (no conversion)
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::setWorldUnitsV(const SurfaceUnits newWorldUnitsV)
{
	m_WorldUnitsV = newWorldUnitsV;
}

// --------------------------------------------------------------------------------

// sets the World units of w coordinate
// only setting (no conversion)
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::setWorldUnitsW(const ElevationUnits newWorldUnitsW)
{
	m_WorldUnitsW = newWorldUnitsW;
}

// --------------------------------------------------------------------------------

// converts the World units of u coordinate
// to the specified units
// suMETERS <-> suFEET
// suRADIANS <-> suSECONDS
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::convertWorldUnitsU(const SurfaceUnits newWorldUnitsU)
{
	// no change in units
	if(m_WorldUnitsU == newWorldUnitsU) return;

	// converts
	if((m_WorldUnitsU == suMETERS) && (newWorldUnitsU == suFEET))
	{
		m_ResolutionU  *= EtUnitsConverter<Real>::LEN_METERS_TO_FEET;
		m_WorldOriginU *= EtUnitsConverter<Real>::LEN_METERS_TO_FEET;
		m_WorldUnitsU = suFEET;
		return;
	}
		
	if((m_WorldUnitsU == suFEET) && (newWorldUnitsU == suMETERS))
	{
		m_ResolutionU  *= EtUnitsConverter<Real>::LEN_FEET_TO_METERS;
		m_WorldOriginU *= EtUnitsConverter<Real>::LEN_FEET_TO_METERS;
		m_WorldUnitsU = suMETERS;
		return;
	}

	if((m_WorldUnitsU == suRADIANS) && (newWorldUnitsU == suSECONDS))
	{
		m_ResolutionU  *= EtUnitsConverter<Real>::ANG_RAD_TO_SEC;
		m_WorldOriginU *= EtUnitsConverter<Real>::ANG_RAD_TO_SEC;
		m_WorldUnitsU = suSECONDS;
		return;
	}

	if((m_WorldUnitsU == suSECONDS) && (newWorldUnitsU == suRADIANS))
	{
		m_ResolutionU  *= EtUnitsConverter<Real>::ANG_SEC_TO_RAD;
		m_WorldOriginU *= EtUnitsConverter<Real>::ANG_SEC_TO_RAD;
		m_WorldUnitsU = suRADIANS;
		return;
	}
}

// --------------------------------------------------------------------------------

// converts the World units of v coordinate
// to the specified units
// suMETERS <-> suFEET
// suRADIANS <-> suSECONDS
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::convertWorldUnitsV(const SurfaceUnits newWorldUnitsV)
{
	// no change in units
	if(m_WorldUnitsV == newWorldUnitsV) return;

	// converts
	if((m_WorldUnitsV == suMETERS) && (newWorldUnitsV == suFEET))
	{
		m_ResolutionV  *= EtUnitsConverter<Real>::LEN_METERS_TO_FEET;
		m_WorldOriginV *= EtUnitsConverter<Real>::LEN_METERS_TO_FEET;
		m_WorldUnitsV = suFEET;
		return;
	}
		
	if((m_WorldUnitsV == suFEET) && (newWorldUnitsV == suMETERS))
	{
		m_ResolutionV  *= EtUnitsConverter<Real>::LEN_FEET_TO_METERS;
		m_WorldOriginV *= EtUnitsConverter<Real>::LEN_FEET_TO_METERS;
		m_WorldUnitsV = suMETERS;
		return;
	}

	if((m_WorldUnitsV == suRADIANS) && (newWorldUnitsV == suSECONDS))
	{
		Real 
		m_ResolutionV  *= EtUnitsConverter<Real>::ANG_RAD_TO_SEC;
		m_WorldOriginV *= EtUnitsConverter<Real>::ANG_RAD_TO_SEC;
		m_WorldUnitsV = suSECONDS;
		return;
	}

	if((m_WorldUnitsV == suSECONDS) && (newWorldUnitsV == suRADIANS))
	{
		m_ResolutionV  *= EtUnitsConverter<Real>::ANG_SEC_TO_RAD;
		m_WorldOriginV *= EtUnitsConverter<Real>::ANG_SEC_TO_RAD;
		m_WorldUnitsV = suRADIANS;
		return;
	}
}

// --------------------------------------------------------------------------------

// converts the World units of w coordinate
// to the specified units
// suMETERS <-> suFEET
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::convertWorldUnitsW( const ElevationUnits newWorldUnitsW )
{
	// no change in units
  if ( m_WorldUnitsW == newWorldUnitsW ) { return; }

	// converts
	if ( (m_WorldUnitsW == euMETERS) && (newWorldUnitsW == euFEET) )
	{
		m_ResolutionW  *= EtUnitsConverter<Real>::LEN_METERS_TO_FEET;
		m_OriginWorldW *= EtUnitsConverter<Real>::LEN_METERS_TO_FEET;
		m_WorldUnitsW = euFEET;
		return;
	}
		
	if( (m_WorldUnitsW == euFEET) && (newWorldUnitsW == euMETERS) )
	{
		m_ResolutionW  *= EtUnitsConverter<Real>::LEN_FEET_TO_METERS;
		m_OriginWorldW *= EtUnitsConverter<Real>::LEN_FEET_TO_METERS;
		m_WorldUnitsW = euMETERS;
		return;
	}
}

// --------------------------------------------------------------------------------

// returns the resolution of u coordinate
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getResolutionU() const 
{
	return (m_ResolutionU);
}

// --------------------------------------------------------------------------------

// returns the resolution of v coordinate
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getResolutionV() const 
{
	return (m_ResolutionV);
}

// --------------------------------------------------------------------------------

// returns the resolution of w coordinate
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getResolutionW() const 
{
	return (m_ResolutionW);
}

// --------------------------------------------------------------------------------

// sets the resolution of u coordinate
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setResolutionU( const Real newResolutionU )
{
	m_ResolutionU = newResolutionU;
}

// --------------------------------------------------------------------------------

// sets the resolution of v coordinate
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setResolutionV (const Real newResolutionV )
{
	m_ResolutionV = newResolutionV;
}

// --------------------------------------------------------------------------------

// sets the resolution of w coordinate
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setResolutionW( const Real newResolutionW )
{
	m_ResolutionW = newResolutionW;
}

// --------------------------------------------------------------------------------

// returns the World u coordinate of origin
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getOriginWorldU() const 
{
	return (m_OriginWorldU);
}

// --------------------------------------------------------------------------------

// returns the World v coordinate of origin
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getOriginWorldV() const 
{
	return (m_OriginWorldV);
}

// --------------------------------------------------------------------------------

// returns the World w coordinate of origin
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getOriginWorldW() const 
{
	return (m_OriginWorldW);
}

// --------------------------------------------------------------------------------

// sets the World u coordinate of origin
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setOriginWorldU( const Real newU )
{
	m_OriginWorldU = newU;
}

// --------------------------------------------------------------------------------

// sets the World v coordinate of origin
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setOriginWorldV( const Real newV )
{
	m_OriginWorldV = newV;
}

// --------------------------------------------------------------------------------

// sets the World w coordinate of origin
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setOriginWorldW( const Real newW )
{
	m_OriginWorldW = newW;
}

// --------------------------------------------------------------------------------

// sets the World origin
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::setOriginWorldUVW( const Real newU, const Real newV, const Real newW )
{
	m_OriginWorldU = newU;
	m_OriginWorldV = newV;
	m_OriginWorldW = newW;
}

// --------------------------------------------------------------------------------

// offsets the World u coordinate of origin with the specified value
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::offsetOriginWorldU( const Real offsetU )
{
	m_OriginWorldU += offsetU;
}

// --------------------------------------------------------------------------------

// offsets the World v coordinate of origin with the specified value
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::offsetOriginWorldV( const Real offsetV )
{
	m_OriginWorldV += offsetV;
}

// --------------------------------------------------------------------------------

// offsets the World w coordinate of origin with the specified value
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::offsetOriginWorldW( const Real offsetW )
{
	m_OriginWorldW += offsetW;
}

// --------------------------------------------------------------------------------

// offsets the World origin with the specified values
template < class Real, class Integer >
void EtRectElevationGrid< Real, Integer >::offsetOriginWorldUVW( const Real offsetU, const Real offsetV, const Real offsetW )
{
	m_OriginWorldU += offsetU;
	m_OriginWorldV += offsetV;
	m_OriginWorldW += offsetW;
}

// --------------------------------------------------------------------------------

// returns the World u at the specified u on grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getWorldUAt( size_t u ) const
{
	// converts u to world units
	Real worldU = u * m_ResolutionU;

	// adds world u of origin
	worldU += m_OriginWorldU;

	return (worldU);
}

// --------------------------------------------------------------------------------

// returns the World v at the specified v on grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getWorldVAt( size_t v ) const
{
	// converts v to world units
	Real worldV = v * m_ResolutionV;

	// adds world v of origin
	worldV += m_OriginWorldV;

	return (worldV);
}

// --------------------------------------------------------------------------------

// returns the World w at the specified w on grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getWorldWAt( size_t w ) const
{
	// converts w to world units
	Real worldW = w * m_ResolutionW;

	// adds world w of origin
	worldW += m_OriginWorldW;

	return (worldW);
}

// --------------------------------------------------------------------------------

// returns the World w at node (u, v) of the grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getWorldWAt( size_t u, size_t v ) const 
{
	Real w = getWAt( u, v );

	// if it is a valid w
	if ( w != NO_ELEVATION )
	{
		// converts w to world units
		w *= m_ResolutionW;

		// adds world w of origin
		w += m_OriginWorldW;
	}

	return (w);
}

// --------------------------------------------------------------------------------

// returns the World w at the point (worldU, worldV) of the grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getWorldWAt( Real worldU, Real worldV, InterpolationMethods method, bool clamp ) const
{
	switch ( method )
	{
	case imBILINEAR:
		{
			Real u = (worldU - m_OriginWorldU) / m_ResolutionU;

      if ( u < EtMath< Real >::ZERO ) 
      { 
        if ( clamp )
        {
          u = EtMath< Real >::ZERO;
        }
        else
        {
          return (NO_ELEVATION); 
        }
      }
      
      size_t floorU = static_cast< size_t >( floor( u ) );

      if ( floorU > m_SizeU - 1 ) 
      { 
        if ( clamp )
        {
          floorU = m_SizeU - 2;
        }
        else
        {
          return (NO_ELEVATION); 
        }
      }
      else if ( floorU == m_SizeU - 1 ) 
      {
        --floorU;
      }

      Real x = u - floorU;

			Real v = (worldV - m_OriginWorldV) / m_ResolutionV;

      if ( v < EtMath< Real >::ZERO ) 
      { 
        if ( clamp )
        {
          v = EtMath< Real >::ZERO;
        }
        else
        {
          return (NO_ELEVATION); 
        }
      }

      size_t floorV = static_cast< size_t >( floor( v ) );

      if ( floorV > m_SizeV - 1 ) 
      { 
        if ( clamp )
        {
          floorV = m_SizeV - 2;
        }
        else
        {
          return (NO_ELEVATION); 
        }
      }
      else if ( floorV == m_SizeV - 1 ) 
      {
        --floorV;
      }
			Real y = v - floorV;

			Real f00 = getWorldWAt( floorU, floorV );
			Real f10 = getWorldWAt( floorU + 1, floorV );
			Real f01 = getWorldWAt( floorU, floorV + 1 );
			Real f11 = getWorldWAt( floorU + 1, floorV + 1 );

      return ( EtMath< Real >::InterpolateBilinear( f00, f10, f01, f11, x, y ) );
		}
	default:
    {
  		return (NO_ELEVATION);
    }
	}
}

// --------------------------------------------------------------------------------

// returns the World slope at the point (worldU, worldV) of the grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getWorldSlopeAt( const Real worldU, const Real worldV, const InterpolationMethods method ) const
{
	switch(method)
	{
	case imARMA:
		{
			Real u = (worldU - m_OriginWorldU) / m_ResolutionU;
			unsigned int floorU = static_cast< size_t >( floor( u ) );
			Real x = u - floorU;

			Real v = (worldV - m_OriginWorldV) / m_ResolutionV;
			unsigned int floorV = static_cast< size_t >( floor( v ) );
			Real y = v - floorV;

			EtVector3< Real > v1;
			EtVector3< Real > v2;
			if ( y > (1 - x) )
			{
				Real dx1 = getWorldUAt( floorU ) - getWorldUAt( floorU + 1 );
				Real dy1 = static_cast< Real >( 0 );
				Real dz1 = getWorldWAt( floorU, floorV + 1 ) - getWorldWAt( floorU + 1, floorV + 1 );
				v1.Set( dx1, dy1, dz1 );
				Real dx2 = static_cast< Real >( 0 );
				Real dy2 = getWorldVAt( floorV ) - getWorldVAt( floorV + 1 );
				Real dz2 = getWorldWAt( floorU + 1, floorV ) - getWorldWAt( floorU + 1, floorV + 1 );
				v2.Set( dx2, dy2, dz2 );
			}
			else
			{
				Real dx1 = getWorldUAt( floorU + 1 ) - getWorldUAt( floorU );
				Real dy1 = static_cast< Real >( 0 );
				Real dz1 = getWorldWAt( floorU + 1, floorV ) - getWorldWAt( floorU, floorV );
				v1.Set( dx1, dy1, dz1 );
				Real dx2 = static_cast< Real >( 0 );
				Real dy2 = getWorldVAt( floorV + 1 ) - getWorldVAt( floorV );
				Real dz2 = getWorldWAt( floorU, floorV + 1 ) - getWorldWAt( floorU, floorV );
				v2.Set( dx2, dy2, dz2 );
			}

			EtVector3< Real > norm = v1.Cross( v2 );
			norm.Normalize();

      if ( (norm.X() * norm.X() + norm.Y() * norm.Y()) == static_cast< Real >( 0 ) ) { return (static_cast< Real >( 0 )); }

			return (EtMath< Real >::Sqrt( norm.X() * norm.X() + norm.Y() * norm.Y() ) / norm.Z());
		}
	default:
		return (static_cast< Real >( 0 ));
	}
}

// --------------------------------------------------------------------------------

// returns the minimum World u on this grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getMinWorldU() const
{
	// here grid u is always equal to 0.0

	// adds world u of origin
	Real minU = m_OriginWorldU;

	return (minU);
}

// --------------------------------------------------------------------------------

// returns the minimum World v on this grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getMinWorldV() const
{
	// here grid v is always equal to 0.0

	// adds world v of origin
	Real minV = m_OriginWorldV;

	return (minV);
}

// --------------------------------------------------------------------------------

// returns the minimum World w on this grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getMinWorldW() const
{
	Real minElev = static_cast< Real >( getMinValidW() );
	
	// if it is a valid elevation
	if ( minElev != NO_ELEVATION )
	{
		// here minElev should be equal to 0.0

		// adds world w of origin
		minElev += m_OriginWorldW;
	}

	return (minElev);
}

// --------------------------------------------------------------------------------

// returns the maximum World u on this grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getMaxWorldU() const
{
	// here grid u is always equal to (m_SizeU - 1)

	// converts u to world units
	Real maxU = (m_SizeU - 1) * m_ResolutionU;

	// adds world u of origin
	maxU += m_OriginWorldU;

	return (maxU);
}

// --------------------------------------------------------------------------------

// returns the maximum World v on this grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getMaxWorldV() const
{
	// here grid v is always equal to (m_SizeV - 1)

	// converts v to world units
	Real maxV = (m_SizeV - 1) * m_ResolutionV;

	// adds world v of origin
	maxV += m_OriginWorldV;

	return (maxV);
}

// --------------------------------------------------------------------------------

// returns the maximum World w on this grid
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getMaxWorldW() const
{
	Real maxElev = static_cast< Real >( getMaxValidW() );
	
	// if it is a valid elevation
	if ( maxElev != NO_ELEVATION )
	{
		// here maxElev should be equal to (m_SizeW - 1)

		// converts w to world units
		maxElev *= m_ResolutionW;

		// adds world w of origin
		maxElev += m_OriginWorldW;
	}
	return (maxElev);
}

// --------------------------------------------------------------------------------

// returns the u size of this grid in World units
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getWorldSizeU() const
{
	return (getMaxWorldU() - getMinWorldU());
}

// --------------------------------------------------------------------------------

// returns the v size of this grid in World units
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getWorldSizeV() const
{
	return (getMaxWorldV() - getMinWorldV());
}

// --------------------------------------------------------------------------------

// returns the w size of this grid in World units
template < class Real, class Integer >
Real EtRectElevationGrid< Real, Integer >::getWorldSizeW() const
{
	return (getMaxWorldW() - getMinWorldW());
}

// --------------------------------------------------------------------------------
// OPERATIONS ON GRID
// --------------------------------------------------------------------------------

template < class Real, class Integer >
EtRectElevationGrid< Real, Integer > EtRectElevationGrid< Real, Integer >::subGrid( size_t swCornerU, size_t swCornerV,
                                                                                    size_t neCornerU, size_t neCornerV ) const
{
	// new grid to store the sub grid
	EtRectElevationGrid< Real, Integer > subGrid;

	// calculates sub grid sizes
	size_t sizeU = neCornerU - swCornerU + 1;
	size_t sizeV = neCornerV - swCornerV + 1;

	// sets sub grid UV sizes
	subGrid.setUVSize( sizeU, sizeV, false );

	// copies resolutions
	subGrid.setResolutionU( m_ResolutionU );
	subGrid.setResolutionV( m_ResolutionV );
	subGrid.setResolutionW( m_ResolutionW );

	// copies units
	subGrid.setWorldUnitsU( m_WorldUnitsU );
	subGrid.setWorldUnitsV( m_WorldUnitsV );
	subGrid.setWorldUnitsW( m_WorldUnitsW );

	// sets origin
	subGrid.setOriginWorldU( m_OriginWorldU + swCornerU * m_ResolutionU );
	subGrid.setOriginWorldV( m_OriginWorldV + swCornerV * m_ResolutionV );
	subGrid.setOriginWorldW( m_OriginWorldW );

	// copies data
	for ( size_t i = 0; i < sizeU; ++i )
	{
		for ( size_t j = 0; j < sizeV; ++j )
		{
			subGrid.setWAt( i, j, getWAt( swCornerU + i, swCornerV + j ) );
		}
	}

	// normalizes W
	subGrid.normalizeW();

	return (subGrid);
}

// --------------------------------------------------------------------------------

template < class Real, class Integer >
EtRectElevationGrid< Real, Integer > EtRectElevationGrid< Real, Integer >::subGrid( Real swCornerWorldU, Real swCornerWorldV, 
                                                                                    Real neCornerWorldU, Real neCornerWorldV ) const
{
	// converts to grid units
	int swU = static_cast< int >( floor( (swCornerWorldU - m_OriginWorldU) / m_ResolutionU ) );
	int swV = static_cast< int >( floor( (swCornerWorldV - m_OriginWorldV) / m_ResolutionV ) );
	int neU = static_cast< int >( ceil( (neCornerWorldU - m_OriginWorldU) / m_ResolutionU ) );
	int neV = static_cast< int >( ceil( (neCornerWorldV - m_OriginWorldV) / m_ResolutionV ) );

  if ( swU < 0 ) { swU = 0; }
  if ( swV < 0 ) { swV = 0; }
  if ( neU < 0 ) { neU = 0; }
  if ( neV < 0 ) { neV = 0; }
  if ( swU > static_cast< int >( m_SizeU ) - 1 ) { swU = static_cast< int >( m_SizeU ) - 1; }
  if ( swV > static_cast< int >( m_SizeV ) - 1 ) { swV = static_cast< int >( m_SizeV ) - 1; }
  if ( neU > static_cast< int >( m_SizeU ) - 1 ) { neU = static_cast< int >( m_SizeU ) - 1; }
  if ( neV > static_cast< int >( m_SizeV ) - 1 ) { neV = static_cast< int >( m_SizeV ) - 1; }

	return (subGrid( static_cast< size_t >( swU ), static_cast< size_t >( swV ), 
                   static_cast< size_t >( neU ), static_cast< size_t >( neV ) ));
}

// --------------------------------------------------------------------------------

// returns the grid obtained by merging this grid to the specified one
// placing the second to the right of the first
// all world quantities of the new grid are inherited from this grid
// even if the second has different ones
// if offsetV is specified the specified grid will be shifted in V direction
template < class Real, class Integer >
EtRectElevationGrid< Real, Integer > EtRectElevationGrid< Real, Integer >::mergeRight( const EtRectElevationGrid< Real, Integer >& other, 
                                                                                       const int offsetV ) const
{
	// the new grid 
	EtRectElevationGrid< Real, Integer > newGrid;

	// calculates new grid sizes
	size_t sizeU = this->m_SizeU + other.m_SizeU;
	int minV = (offsetV < 0) ? offsetV : 0;
	int maxV;
	if(offsetV > 0)
	{
		maxV = (this->m_SizeV > (other.m_SizeV + offsetV)) ? this->m_SizeV : (other.m_SizeV + offsetV);
	}
	else if(offsetV < 0)
	{
		maxV = ((this->m_SizeV - offsetV) > other.m_SizeV) ? (this->m_SizeV - offsetV) : other.m_SizeV;
	}
	else 
	{
		maxV = (this->m_SizeV > other.m_SizeV) ? this->m_SizeV : other.m_SizeV;
	}
	size_t sizeV = maxV - minV;

	// sets new grid UV sizes
	newGrid.setUVSize( sizeU, sizeV, true );

	// copies resolutions
	newGrid.setResolutionU( this->m_ResolutionU );
	newGrid.setResolutionV( this->m_ResolutionV );
	newGrid.setResolutionW( this->m_ResolutionW );

	// copies units
	newGrid.setWorldUnitsU( this->m_WorldUnitsU );
	newGrid.setWorldUnitsV( this->m_WorldUnitsV );
	newGrid.setWorldUnitsW( this->m_WorldUnitsW );

	// sets origin
	newGrid.setOriginWorldU( this->m_OriginWorldU );
	if ( offsetV < 0 )
	{
		newGrid.setOriginWorldV( this->m_OriginWorldV + offsetV * this->m_ResolutionV );
	}
	else
	{
		newGrid.setOriginWorldV( this->m_OriginWorldV );
	}
	newGrid.setOriginWorldW( this->m_OriginWorldW );

	// copies data
	for ( size_t i = 0; i < this->m_SizeU; ++i )
	{
		for ( size_t j = 0; j < this->m_SizeV; ++j )
		{
			if ( offsetV < 0 )
			{
				newGrid.setWAt( i, j - offsetV, this->getWAt( i, j ) );
			}
			else
			{
				newGrid.setWAt( i, j, this->getWAt( i, j ) );
			}
		}
	}

#if (_MSC_VER > 1200)
	for ( size_t i = 0; i < other.m_SizeU; ++i )
#else
	for ( i = 0; i < other.m_SizeU; ++i )
#endif
	{
		for ( size_t j = 0; j < other.m_SizeV; ++j )
		{
			if ( offsetV < 0 )
			{
				newGrid.setWAt( this->m_SizeU + i, j, other.getWAt( i, j ) );
			}
			else
			{
				newGrid.setWAt( this->m_SizeU + i, j + offsetV, other.getWAt( i, j ) );
			}
		}
	}

	// normalizes W
	newGrid.normalizeW();

	return (newGrid);
}

// --------------------------------------------------------------------------------

// returns the grid obtained by merging this grid to the specified one
// placing the second to the left of the first
// all world quantities of the new grid are inherited from this grid
// even if the second has different ones
// if offsetV is specified the specified grid will be shifted in V direction
template < class Real, class Integer >
EtRectElevationGrid< Real, Integer > EtRectElevationGrid< Real, Integer >::mergeLeft( const EtRectElevationGrid< Real, Integer >& other, 
                                                                                      const int offsetV ) const
{
	// the new grid 
	EtRectElevationGrid< Real, Integer > newGrid;

	// calculates new grid sizes
	size_t sizeU = this->m_SizeU + other.m_SizeU;
	int minV = (offsetV < 0) ? offsetV : 0;
	int maxV;
	if ( offsetV > 0 )
	{
		maxV = (this->m_SizeV > (other.m_SizeV + offsetV)) ? this->m_SizeV : (other.m_SizeV + offsetV);
	}
	else if ( offsetV < 0 )
	{
		maxV = ((this->m_SizeV - offsetV) > other.m_SizeV) ? (this->m_SizeV - offsetV) : other.m_SizeV;
	}
	else 
	{
		maxV = (this->m_SizeV > other.m_SizeV) ? this->m_SizeV : other.m_SizeV;
	}
	size_t sizeV = maxV - minV;

	// sets new grid UV sizes
	newGrid.setUVSize( sizeU, sizeV, true );

	// copies resolutions
	newGrid.setResolutionU( this->m_ResolutionU );
	newGrid.setResolutionV( this->m_ResolutionV );
	newGrid.setResolutionW( this->m_ResolutionW );

	// copies units
	newGrid.setWorldUnitsU( this->m_WorldUnitsU );
	newGrid.setWorldUnitsV( this->m_WorldUnitsV );
	newGrid.setWorldUnitsW( this->m_WorldUnitsW );

	// sets origin
	newGrid.setWorldOriginU( this->m_WorldOriginU );
	if ( offsetV < 0 )
	{
		newGrid.setWorldOriginV( this->m_WorldOriginV + offsetV * this->m_ResolutionV );
	}
	else
	{
		newGrid.setWorldOriginV( this->m_WorldOriginV );
	}
	newGrid.setWorldOriginW( this->m_WorldOriginW );

	// copies data
	for ( size_t i = 0; i < this->m_SizeU; ++i )
	{
		for ( size_t j = 0; j < this->m_SizeV; ++j )
		{
			if ( offsetV < 0 )
			{
				newGrid.setWAt( other.m_SizeU + i, j - offsetV, this->getWAt( i, j ) );
			}
			else
			{
				newGrid.setWAt( other.m_SizeU + i, j, this->getWAt( i, j ) );
			}
		}
	}

#if (_MSC_VER > 1200)
	for ( size_t i = 0; i < other.m_SizeU; ++i )
#else
	for ( i = 0; i < other.m_SizeU; ++i )
#endif
	{
		for ( size_t j = 0; j < other.m_SizeV; ++j )
		{
			if ( offsetV < 0 )
			{
				newGrid.setWAt( i, j, other.getWAt( i, j ) );
			}
			else
			{
				newGrid.setWAt( i, j + offsetV, other.getWAt( i, j ) );
			}
		}
	}

	// normalizes W
	newGrid.normalizeW();

	return (newGrid);
}

// --------------------------------------------------------------------------------

// returns the grid obtained by merging this grid to the specified one
// placing the second to the top of the first
// all world quantities of the new grid are inherited from this grid
// even if the second has different ones
// if offsetU is specified the specified grid will be shifted in U direction
template <class Real, class Integer>
EtRectElevationGrid<Real, Integer> EtRectElevationGrid<Real, Integer>::mergeTop(const EtRectElevationGrid<Real, Integer>& other, const int offsetU) const
{
	// the new grid 
	EtRectElevationGrid<Real, Integer> newGrid;

	// calculates new grid sizes
	int minU = (offsetU < 0) ? offsetU : 0;
	int maxU;
	if(offsetU > 0)
	{
		maxU = (this->m_SizeU > (other.m_SizeU + offsetU)) ? this->m_SizeU : (other.m_SizeU + offsetU);
	}
	else if(offsetU < 0)
	{
		maxU = ((this->m_SizeU - offsetU) > other.m_SizeU) ? (this->m_SizeU - offsetU) : other.m_SizeU;
	}
	else 
	{
		maxU = (this->m_SizeU > other.m_SizeU) ? this->m_SizeU : other.m_SizeU;
	}
	unsigned int sizeU = maxU - minU;
	unsigned int sizeV = this->m_SizeV + other.m_SizeV;

	// sets new grid UV sizes
	newGrid.setUVSize(sizeU, sizeV, true);

	// copies resolutions
	newGrid.setResolutionU(this->m_ResolutionU);
	newGrid.setResolutionV(this->m_ResolutionV);
	newGrid.setResolutionW(this->m_ResolutionW);

	// copies units
	newGrid.setWorldUnitsU(this->m_WorldUnitsU);
	newGrid.setWorldUnitsV(this->m_WorldUnitsV);
	newGrid.setWorldUnitsW(this->m_WorldUnitsW);

	// sets origin
	if(offsetU < 0)
	{
		newGrid.setWorldOriginU(this->m_WorldOriginU + offsetU * this->m_ResolutionU);
	}
	else
	{
		newGrid.setWorldOriginU(this->m_WorldOriginU);
	}
	newGrid.setWorldOriginV(this->m_WorldOriginV);
	newGrid.setWorldOriginW(this->m_WorldOriginW);

	// copies data
	for(unsigned int i = 0; i < this->m_SizeU; ++i)
	{
		for(unsigned int j = 0; j < this->m_SizeV; ++j)
		{
			if(offsetU < 0)
			{
				newGrid.setWAt(i - offsetU, j, this->getWAt(i, j));
			}
			else
			{
				newGrid.setWAt(i, j, this->getWAt(i, j));
			}
		}
	}

#if (_MSC_VER > 1200)
	for(unsigned int i = 0; i < other.m_SizeU; ++i)
#else
	for(i = 0; i < other.m_SizeU; ++i)
#endif
	{
		for(unsigned int j = 0; j < other.m_SizeV; ++j)
		{
			if(offsetU < 0)
			{
				newGrid.setWAt(i, this->m_SizeV + j, other.getWAt(i, j));
			}
			else
			{
				newGrid.setWAt(i + offsetU, this->m_SizeV + j, other.getWAt(i, j));
			}
		}
	}

	// normalizes W
	newGrid.normalizeW();

	return newGrid;
}

// --------------------------------------------------------------------------------

// returns the grid obtained by merging this grid to the specified one
// placing the second to the bottom of the first
// all world quantities of the new grid are inherited from this grid
// even if the second has different ones
// if offsetU is specified the specified grid will be shifted in U direction
template <class Real, class Integer>
EtRectElevationGrid<Real, Integer> EtRectElevationGrid<Real, Integer>::mergeBottom(const EtRectElevationGrid<Real, Integer>& other, const int offsetU) const
{
	// the new grid 
	EtRectElevationGrid<Real, Integer> newGrid;

	// calculates new grid sizes
	int minU = (offsetU < 0) ? offsetU : 0;
	int maxU;
	if(offsetU > 0)
	{
		maxU = (this->m_SizeU > (other.m_SizeU + offsetU)) ? this->m_SizeU : (other.m_SizeU + offsetU);
	}
	else if(offsetU < 0)
	{
		maxU = ((this->m_SizeU - offsetU) > other.m_SizeU) ? (this->m_SizeU - offsetU) : other.m_SizeU;
	}
	else 
	{
		maxU = (this->m_SizeU > other.m_SizeU) ? this->m_SizeU : other.m_SizeU;
	}
	unsigned int sizeU = maxU - minU;
	unsigned int sizeV = this->m_SizeV + other.m_SizeV;

	// sets new grid UV sizes
	newGrid.setUVSize(sizeU, sizeV, true);

	// copies resolutions
	newGrid.setResolutionU(this->m_ResolutionU);
	newGrid.setResolutionV(this->m_ResolutionV);
	newGrid.setResolutionW(this->m_ResolutionW);

	// copies units
	newGrid.setWorldUnitsU(this->m_WorldUnitsU);
	newGrid.setWorldUnitsV(this->m_WorldUnitsV);
	newGrid.setWorldUnitsW(this->m_WorldUnitsW);

	// sets origin
	if(offsetU < 0)
	{
		newGrid.setOriginWorldU(this->m_OriginWorldU + offsetU * this->m_ResolutionU);
	}
	else
	{
		newGrid.setOriginWorldU(this->m_OriginWorldU);
	}
	newGrid.setOriginWorldV(this->m_OriginWorldV);
	newGrid.setOriginWorldW(this->m_OriginWorldW);

	// copies data
	for(unsigned int i = 0; i < this->m_SizeU; ++i)
	{
		for(unsigned int j = 0; j < this->m_SizeV; ++j)
		{
			if(offsetU < 0)
			{
				newGrid.setWAt(i - offsetU, other.m_SizeV + j, this->getWAt(i, j));
			}
			else
			{
				newGrid.setWAt(i, other.m_SizeV + j, this->getWAt(i, j));
			}
		}
	}

#if (_MSC_VER > 1200)
	for(unsigned int i = 0; i < other.m_SizeU; ++i)
#else
	for(i = 0; i < other.m_SizeU; ++i)
#endif
	{
		for(unsigned int j = 0; j < other.m_SizeV; ++j)
		{
			if(offsetU < 0)
			{
				newGrid.setWAt(i, j, other.getWAt(i, j));
			}
			else
			{
				newGrid.setWAt(i + offsetU, j, other.getWAt(i, j));
			}
		}
	}

	// normalizes W
	newGrid.normalizeW();

	return newGrid;
}

// --------------------------------------------------------------------------------

// returns a grid obtained by resampling the u of this grid
template <class Real, class Integer>
EtRectElevationGrid<Real, Integer> EtRectElevationGrid<Real, Integer>::resampleU(const ResamplingMethods method, const Real newResolutionU)
{
	EtRectElevationGrid<Real, Integer> newGrid;

	switch(method)
	{
	case rmLINEAR:
		return (this->linearResampleU(newResolutionU));
	default:
		return newGrid;		
	}
}

// --------------------------------------------------------------------------------

// returns a grid obtained by resampling the v of this grid
template <class Real, class Integer>
EtRectElevationGrid<Real, Integer> EtRectElevationGrid<Real, Integer>::resampleV(const ResamplingMethods method, const Real newResolutionV)
{
	EtRectElevationGrid<Real, Integer> newGrid;

	switch(method)
	{
	case rmLINEAR:
		return (this->linearResampleV(newResolutionV));
	default:
		return newGrid;		
	}
}

// --------------------------------------------------------------------------------

// returns a grid obtained by resampling the u and v of this grid
template <class Real, class Integer>
EtRectElevationGrid<Real, Integer> EtRectElevationGrid<Real, Integer>::resampleUV(const ResamplingMethods methodU, const ResamplingMethods methodV, const Real newResolutionU, const Real newResolutionV)
{
	EtRectElevationGrid<Real, Integer> newGridU;

	switch(methodU)
	{
	case rmLINEAR:
		switch(methodV)
		{
		case rmLINEAR:
			return (this->linearResampleUV(newResolutionU, newResolutionV));
		default:
			return newGridU;
		}
	default:
		return newGridU;
	}
}

// --------------------------------------------------------------------------------

// returns a grid obtained by linear resampling the u of this grid
template <class Real, class Integer>
EtRectElevationGrid<Real, Integer> EtRectElevationGrid<Real, Integer>::linearResampleU(const Real newResolutionU)
{
	EtRectElevationGrid<Real, Integer> newGrid;

	unsigned int sizeU = 1 + static_cast<unsigned int>(floor(this->getWorldSizeU() / newResolutionU));
	unsigned int sizeV = this->m_SizeV;

	// sets new grid UV sizes
	newGrid.setUVSize(sizeU, sizeV, false);

	// copies/sets resolutions
	newGrid.setResolutionU(newResolutionU);
	newGrid.setResolutionV(this->m_ResolutionV);
	newGrid.setResolutionW(this->m_ResolutionW);

	// copies units
	newGrid.setWorldUnitsU(this->m_WorldUnitsU);
	newGrid.setWorldUnitsV(this->m_WorldUnitsV);
	newGrid.setWorldUnitsW(this->m_WorldUnitsW);

	// copies origin
	newGrid.setOriginWorldU(this->m_OriginWorldU);
	newGrid.setOriginWorldV(this->m_OriginWorldV);
	newGrid.setOriginWorldW(static_cast<Real>(0.0));

	// resamples u
	for(unsigned int i = 0; i < sizeU; ++i)
	{
		Real worldU = newGrid.getWorldUAt(i);
		for(unsigned int j = 0; j < sizeV; ++j)
		{
			Real worldV = newGrid.getWorldVAt(j);
			Real worldW = this->getWorldWAt(worldU, worldV, imBILINEAR);
			Integer newW = static_cast<Integer>(worldW);
			newGrid.setWAt(i, j, newW);
		}
	}

	// normalizes W
	newGrid.normalizeW();

	return newGrid;		
}

// --------------------------------------------------------------------------------

// returns a grid obtained by linear resampling the v of this grid
template <class Real, class Integer>
EtRectElevationGrid<Real, Integer> EtRectElevationGrid<Real, Integer>::linearResampleV(const Real newResolutionV)
{
	EtRectElevationGrid<Real, Integer> newGrid;

	unsigned int sizeU = this->m_SizeU;
	unsigned int sizeV = 1 + static_cast<unsigned int>(floor(this->getWorldSizeV() / newResolutionV));

	// sets new grid UV sizes
	newGrid.setUVSize(sizeU, sizeV, false);

	// copies/sets resolutions
	newGrid.setResolutionU(this->m_ResolutionU);
	newGrid.setResolutionV(newResolutionV);
	newGrid.setResolutionW(this->m_ResolutionW);

	// copies units
	newGrid.setWorldUnitsU(this->m_WorldUnitsU);
	newGrid.setWorldUnitsV(this->m_WorldUnitsV);
	newGrid.setWorldUnitsW(this->m_WorldUnitsW);

	// copies origin
	newGrid.setOriginWorldU(this->m_OriginWorldU);
	newGrid.setOriginWorldV(this->m_OriginWorldV);
	newGrid.setOriginWorldW(static_cast<Real>(0.0));

	// resamples v
	for(unsigned int i = 0; i < sizeU; ++i)
	{
		Real worldU = newGrid.getWorldUAt(i);
		for(unsigned int j = 0; j < sizeV; ++j)
		{
			Real worldV = newGrid.getWorldVAt(j);
			Real worldW = this->getWorldWAt(worldU, worldV, imBILINEAR);
			Integer newW = static_cast<Integer>(worldW);
			newGrid.setWAt(i, j, newW);
		}
	}

	// normalizes W
	newGrid.normalizeW();

	return newGrid;		
}

// --------------------------------------------------------------------------------

// returns a grid obtained by linear resampling the u and v of this grid
template <class Real, class Integer>
EtRectElevationGrid<Real, Integer> EtRectElevationGrid<Real, Integer>::linearResampleUV(const Real newResolutionU, const Real newResolutionV)
{
	EtRectElevationGrid<Real, Integer> newGrid;

	unsigned int sizeU = 1 + static_cast<unsigned int>(floor(this->getWorldSizeU() / newResolutionU));
	unsigned int sizeV = 1 + static_cast<unsigned int>(floor(this->getWorldSizeV() / newResolutionV));

	// sets new grid UV sizes
	newGrid.setUVSize(sizeU, sizeV, false);

	// copies/sets resolutions
	newGrid.setResolutionU(newResolutionU);
	newGrid.setResolutionV(newResolutionV);
	newGrid.setResolutionW(this->m_ResolutionW);

	// copies units
	newGrid.setWorldUnitsU(this->m_WorldUnitsU);
	newGrid.setWorldUnitsV(this->m_WorldUnitsV);
	newGrid.setWorldUnitsW(this->m_WorldUnitsW);

	// copies origin
	newGrid.setOriginWorldU(this->m_OriginWorldU);
	newGrid.setOriginWorldV(this->m_OriginWorldV);
	newGrid.setOriginWorldW(static_cast<Real>(0.0));

	// resamples u and v
	for(unsigned int i = 0; i < sizeU; ++i)
	{
		Real worldU = newGrid.getWorldUAt(i);
		for(unsigned int j = 0; j < sizeV; ++j)
		{
			Real worldV = newGrid.getWorldVAt(j);
			Real worldW = this->getWorldWAt(worldU, worldV, imBILINEAR);
			newGrid.setWAt(i, j, static_cast<Integer>(worldW));
		}
	}

	// normalizes W
	newGrid.normalizeW();

	return newGrid;		
}

// --------------------------------------------------------------------------------
// LOADERS
// --------------------------------------------------------------------------------

// loads data from DTED *.dt2 files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::loadFromDT2(char* fileName)
{
	return loadFromDT2(string(fileName));
}

// --------------------------------------------------------------------------------

// loads data from DTED *.dt2 files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::loadFromDT2(string fileName)
{
	// if the grid is not empty -> resets it
	if(!empty())
	{
		// clears all data
		clear();
		// default units are meters for all coordinates
		setDefaultWorldUnits();
	}

	ifstream iFile;
	iFile.open(fileName.c_str(), ios::in | ios::binary);

	cout << "Opened file: " << fileName << endl;

	// error in opening the file
	if(!iFile.is_open()) 
	{
		return false;
	}

	// sets units and resolutions
	// in DTED2 are:
	// 1 arc-second for u and v
	// 1 meter for w
	m_WorldUnitsU = suSECONDS;
	m_WorldUnitsV = suSECONDS;
	m_WorldUnitsW = euMETERS;

	cout << "Reading data from file: " << fileName << endl;

#ifdef _DEBUG
	cout << "Reading UHL " << endl;
#endif

	char uhl[80];
	iFile.read(uhl, 80);

#ifdef _DEBUG
	cout << "Parsing UHL " << endl;
#endif
	
	char signature[5];
	for(int i = 0; i < 4; ++i)
	{
		signature[i] = uhl[i];
	}
	signature[4] = '\0';

	// not a valid DT2 file
	if (strcmp(signature, "UHL1") != 0)
	{
		iFile.close();
		return false;
	}

	// longitude of origin
	char angle[9];
#if (_MSC_VER > 1200)
	for(int i = 0; i < 8; ++i)
#else
	for(i = 0; i < 8; ++i)
#endif
	{
		angle[i] = uhl[4 + i];
	}
	angle[8] = '\0';
	m_OriginWorldU = DDDMMSSH_ToSeconds<Real>(angle);
	
	// latitude of origin
#if (_MSC_VER > 1200)
	for(int i = 0; i < 8; ++i)
#else
	for(i = 0; i < 8; ++i)
#endif
	{
		angle[i] = uhl[12 + i];
	}
	angle[8] = '\0';
	m_OriginWorldV = DDDMMSSH_ToSeconds<Real>(angle);
	
	// longitude resolution
	char resolution[5];
#if (_MSC_VER > 1200)
	for(int i = 0; i < 4; ++i)
#else
	for(i = 0; i < 4; ++i)
#endif
	{
		resolution[i] = uhl[20 + i];
	}
	resolution[4] = '\0';
	m_ResolutionU = static_cast<Real>(atof(resolution) / 10);
	
	// latitude resolution
#if (_MSC_VER > 1200)
	for(int i = 0; i < 4; ++i)
#else
	for(i = 0; i < 4; ++i)
#endif
	{
		resolution[i] = uhl[24 + i];
	}
	resolution[4] = '\0';
	m_ResolutionV = static_cast<Real>(atof(resolution) / 10);
	
	// elevation resolution
	m_ResolutionW = 1.0;

	// longitude size
	char size[5];
#if (_MSC_VER > 1200)
	for(int i = 0; i < 4; ++i)
#else
	for(i = 0; i < 4; ++i)
#endif
	{
		size[i] = uhl[47 + i];
	}
	size[4] = '\0';
	unsigned int sizeU = atoi(size);

	// latitude size
#if (_MSC_VER > 1200)
	for(int i = 0; i < 4; ++i)
#else
	for(i = 0; i < 4; ++i)
#endif
	{
		size[i] = uhl[51 + i];
	}
	size[4] = '\0';
	unsigned int sizeV = atoi(size);

	// prepares the grid
	setUVSize(sizeU, sizeV, false);

	// ----- end of UHL section

#ifdef _DEBUG
	cout << "Reading DSI " << endl;
#endif

	char dsi[648];
	iFile.read(dsi, 648);

	// ----- end of DSI section

#ifdef _DEBUG
	cout << "Reading ACC " << endl;
#endif

	char acc[2700];
	iFile.read(acc, 2700);

	// ----- end of ACC section

#ifdef _DEBUG
	cout << "Reading Data " << endl;
#endif

	int sizeBuf = 8 + 2 * m_SizeV + 4;
	char* record = new char[sizeBuf];

#if (_MSC_VER > 1200)
	for(unsigned int i = 0; i < m_SizeU; ++i)
#else
	for(i = 0; i < m_SizeU; ++i)
#endif
	{
		iFile.read(record, sizeBuf);

		for(unsigned int j = 0; j < m_SizeV; ++j)
		{
			union
			{
				short s;
				unsigned char c[2];
			} elev;

			// big endian short
			elev.c[1] = *(record + 8 + j * 2);
			elev.c[0] = *(record + 8 + j * 2 + 1);

			// put elevation in the grid
			setWAt(i, j, static_cast<Integer>(elev.s));			
		}
	}

	// normalizes W
	normalizeW();

	delete [] record;
	record = 0;

	iFile.close();

	cout << "Closed file: " << fileName << endl;
	
	return true;
}

// --------------------------------------------------------------------------------

// loads data from USGS DEM *.dem files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::loadFromUSGSDEM(char* fileName)
{
	return loadFromUSGSDEM(string(fileName));
}

// --------------------------------------------------------------------------------

// loads data from USGS DEM *.dem files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::loadFromUSGSDEM(string fileName)
{
	// if the grid is not empty -> resets it
	if(!empty())
	{
		// clears all data
		clear();
		// default units are meters for all coordinates
		setDefaultWorldUnits();
	}

	ifstream iFile;
	iFile.open(fileName.c_str(), ios::in | ios::binary);

	cout << "Opened file: " << fileName << endl;

	// error in opening the file
	if(!iFile.is_open()) 
	{
		return false;
	}

	cout << "Reading data from file: " << fileName << endl;

#ifdef _DEBUG
	cout << "Reading Record A " << endl;
#endif

	char recordA[1024];
	iFile.read(recordA, 1024);

#ifdef _DEBUG
	cout << "Parsing Record A " << endl;
#endif
	
	// u v units
	char units[7];
	for(int i = 0; i < 6; ++i)
	{
		units[i] = recordA[528 + i];
	}
	units[6] = '\0';

	switch(atoi(units))
	{
	case 0: // radians
		m_WorldUnitsU = suRADIANS;
		m_WorldUnitsV = suRADIANS;
		break;
	case 1: // feet
		m_WorldUnitsU = suFEET;
		m_WorldUnitsV = suFEET;
		break;
	case 2: // meters
		m_WorldUnitsU = suMETERS;
		m_WorldUnitsV = suMETERS;
		break;
	case 3: // meters
		m_WorldUnitsU = suSECONDS;
		m_WorldUnitsV = suSECONDS;
		break;
	default: // not supported
		return false;
	}

	// w units
#if (_MSC_VER > 1200)
	for(int i = 0; i < 6; ++i)
#else
	for(i = 0; i < 6; ++i)
#endif
	{
		units[i] = recordA[534 + i];
	}
	units[6] = '\0';

	switch(atoi(units))
	{
	case 1: // feet
		m_WorldUnitsW = euFEET;
		break;
	case 2: // meters
		m_WorldUnitsW = euMETERS;
		break;
	default: // not supported
		return false;
	}

	// u resolution
	char resolution[13];
#if (_MSC_VER > 1200)
	for(int i = 0; i < 12; ++i)
#else
	for(i = 0; i < 12; ++i)
#endif
	{
		resolution[i] = recordA[816 + i];
	}
	resolution[12] = '\0';
	m_ResolutionU = static_cast<Real>(atof(resolution));

	// v resolution
#if (_MSC_VER > 1200)
	for(int i = 0; i < 12; ++i)
#else
	for(i = 0; i < 12; ++i)
#endif
	{
		resolution[i] = recordA[828 + i];
	}
	resolution[12] = '\0';
	m_ResolutionV = static_cast<Real>(atof(resolution));

	// w resolution
#if (_MSC_VER > 1200)
	for(int i = 0; i < 12; ++i)
#else
	for(i = 0; i < 12; ++i)
#endif
	{
		resolution[i] = recordA[840 + i];
	}
	resolution[12] = '\0';
	m_ResolutionW = static_cast<Real>(atof(resolution));

	// u size
	char size[7];
#if (_MSC_VER > 1200)
	for(int i = 0; i < 6; ++i)
#else
	for(i = 0; i < 6; ++i)
#endif
	{
		size[i] = recordA[858 + i];
	}
	size[6] = '\0';
	unsigned int sizeU = atoi(size);

	// saves the present position on disk
	ios::pos_type rewindPos = iFile.tellg();

	Real maxU;
	Real minU;
	Real maxV;
	Real minV;

	// for determining the v size we have to scan all the data records
#if (_MSC_VER > 1200)
	for(unsigned int i = 0; i < sizeU; ++i)
#else
	for(i = 0; i < sizeU; ++i)
#endif
	{
		char recordB[1024];
		iFile.read(recordB, 1024);

		// number of point in this profile
		char numElev[7];
		for(int j = 0; j < 6; ++j)
		{
			numElev[j] = recordB[12 + j];
		}
		numElev[6] = '\0';
		unsigned int curSizeV = atoi(numElev);

		// u of first point in this profile
		char coord[25];
#if (_MSC_VER > 1200)
		for(int j = 0; j < 24; ++j)
#else
		for(j = 0; j < 24; ++j)
#endif
		{
			coord[j] = recordB[24 + j];
		}
		coord[24] = '\0';
		Real curU0 = static_cast<Real>(atof(coord));

		// v of first point in this profile
#if (_MSC_VER > 1200)
		for(int j = 0; j < 24; ++j)
#else
		for(j = 0; j < 24; ++j)
#endif
		{
			coord[j] = recordB[48 + j];
		}
		coord[24] = '\0';
		Real curV0 = static_cast<Real>(atof(coord));

		Real curV1 = curV0 + (curSizeV - 1) * m_ResolutionV;

		// determination min/max coords
		if(i == 0)
		{
			minU = curU0;
			minV = curV0;
			maxU = curU0;
			maxV = curV1;
		}
		else
		{
			if(minU > curU0) minU = curU0;
			if(minV > curV0) minV = curV0;
			if(maxU < curU0) maxU = curU0;
			if(maxV < curV1) maxV = curV1;
		}

		// read extra lines if needed
		if(curSizeV > 146)
		{
			iFile.read(recordB, 1024);
			curSizeV -= 146;
			while(curSizeV > 170)
			{
				iFile.read(recordB, 1024);
				curSizeV -= 170;
			}
		}
	}

	// new origin
	m_OriginWorldU = minU;
	m_OriginWorldV = minV;

	// new size
	sizeU = static_cast<unsigned int>(1 + (maxU - minU) / m_ResolutionU);
	unsigned int sizeV = static_cast<unsigned int>(1 + (maxV - minV) / m_ResolutionV);

	// resize grid to contain data
	// force filling with NO_ELEVATION
	setUVSize(sizeU, sizeV);

	// restore position in file
	iFile.seekg(rewindPos);

#ifdef _DEBUG
	cout << "Reading Data " << endl;
#endif

	// now reads data
#if (_MSC_VER > 1200)
	for(unsigned int i = 0; i < sizeU; ++i)
#else
	for(i = 0; i < sizeU; ++i)
#endif
	{
		char recordB[1024];
		iFile.read(recordB, 1024);

		// number of point in this profile
		char numElev[7];
		for(int j = 0; j < 6; ++j)
		{
			numElev[j] = recordB[12 + j];
		}
		numElev[6] = '\0';
		unsigned int curSizeV = atoi(numElev);

		// v of first point in this profile
		char coord[25];
#if (_MSC_VER > 1200)
		for(int j = 0; j < 24; ++j)
#else
		for(j = 0; j < 24; ++j)
#endif
		{
			coord[j] = recordB[48 + j];
		}
		coord[24] = '\0';
		Real curV0 = static_cast<Real>(atof(coord));

		// coordinates on grid of first point
		unsigned int absCurU0 = i;
		unsigned int absCurV0 = static_cast<unsigned int>((curV0 - minV) / m_ResolutionV);

		int offset = 144;
		// gets elevations
#if (_MSC_VER > 1200)
		for(unsigned int j = 0; j < curSizeV; ++j)
#else
		for(j = 0; j < curSizeV; ++j)
#endif
		{
			// read extra lines if needed
			if(offset > 1014)
			{
				iFile.read(recordB, 1024);
				offset = 0;
			}

			char elev[7];
			for(int k = 0; k < 6; ++k)
			{
				elev[k] = recordB[offset + k];
			}
			elev[6] = '\0';
			// if not null string
			if(strcmp(elev, "      ") != 0)
			{
				setWAt(absCurU0, absCurV0 + j, static_cast<Integer>(atoi(elev)));
			}
			else
			{
				break;
			}
			offset += 6;
		}
	}

	// normalizes W
	normalizeW();

	iFile.close();

	cout << "Closed file: " << fileName << endl;

	return true;
}

// --------------------------------------------------------------------------------

// loads data from Arc/Info ASCII Grid *.asc or *.grd files
// returns true if successfull
template < class Real, class Integer >
bool EtRectElevationGrid< Real, Integer >::loadFromARCINFOASCIIGrid( char* fileName )
{
	return (loadFromARCINFOASCIIGrid( string( fileName ) ));
}

// --------------------------------------------------------------------------------

// loads data from Arc/Info ASCII Grid *.asc or *.grd files
// returns true if successfull
template < class Real, class Integer >
bool EtRectElevationGrid< Real, Integer >::loadFromARCINFOASCIIGrid( const string& fileName )
{
	// if the grid is not empty -> resets it
	if ( !empty() )
	{
		// clears all data
		clear();
		// default units are meters for all coordinates
		setDefaultWorldUnits();
	}

	ifstream iFile( fileName.c_str(), ios::in | ios::binary );

	// error in opening the file
	if ( !iFile ) { return (false); }

	cout << "Opened file: " << fileName << endl;

	cout << "Reading data from file: " << fileName << endl;

	// sets units
	m_WorldUnitsU = suMETERS;
	m_WorldUnitsV = suMETERS;
	m_WorldUnitsW = euMETERS;

	string strNCols;
	string strSizeU;
	iFile >> strNCols;
	iFile >> strSizeU;
	
  if ( strNCols != "ncols" ) { return (false); }

	if ( !IsInteger( strSizeU, false ) ) { return (false); }

	size_t sizeU = atoi( strSizeU.c_str() );

	string strNRows;
	string strSizeV;
	iFile >> strNRows;
	iFile >> strSizeV;

  if ( strNRows != "nrows" ) { return (false); }

	if ( !IsInteger( strSizeV, false ) ) { return (false); }

	size_t sizeV = atoi( strSizeV.c_str() );

	string strXllCorner;
	string strOriginU;
	iFile >> strXllCorner;
	iFile >> strOriginU;

	if ( strXllCorner != "xllcorner" ) { return (false); }

	if ( !IsFloatingPoint( strOriginU, false ) ) { return (false); }

	Real originU = static_cast< Real >( atof( strOriginU.c_str() ) );

	string strYllCorner;
	string strOriginV;
	iFile >> strYllCorner;
	iFile >> strOriginV;
	if ( strYllCorner != "yllcorner" ) { return (false); }

	if ( !IsFloatingPoint( strOriginV, false ) ) { return (false); }

	Real originV = static_cast< Real >( atof( strOriginV.c_str() ) );

	// new origin
	m_OriginWorldU = originU;
	m_OriginWorldV = originV;
	m_OriginWorldW = EtMath< Real >::ZERO;

	string strCellSize;
	string strResolutionUV;
	iFile >> strCellSize;
	iFile >> strResolutionUV;
	if ( strCellSize != "cellsize" ) { return (false); }

	if ( !IsFloatingPoint( strResolutionUV, false ) ) { return (false); }

	Real resolutionUV = static_cast< Real >( atof( strResolutionUV.c_str() ) );

	// sets resolutions UV
	m_ResolutionU = resolutionUV;
	m_ResolutionV = resolutionUV;
	m_ResolutionW = 0.01; 

	// saves the present position on disk
	ios::pos_type rewindPos = iFile.tellg();

	string strNoData;
	iFile >> strNoData;
	bool bNoData;
	Real noData = EtMath< Real >::MAX_REAL;
	transform( strNoData.begin(), strNoData.end(), strNoData.begin(), toupper );
	if ( strNoData == "NODATA_VALUE" ) 
  {
		bNoData = true;
		string strNoDataValue;
		iFile >> strNoDataValue;
		if ( !IsFloatingPoint( strNoDataValue, false ) ) { return (false); }

		noData = static_cast< Real >( atof( strNoDataValue.c_str() ) );
	}
	else
	{
		bNoData = false;
		// restore position in file
		iFile.seekg( rewindPos );
	}

	// resize grid to contain data
	// force filling with NO_ELEVATION
	setUVSize( sizeU, sizeV );

	for ( size_t v = 0; v < sizeV; ++v )
	{
		for ( size_t u = 0; u < sizeU; ++u )
		{
			string strTempW;
			iFile >> strTempW;

			// replaces comma with point (if there are decimal)
			for ( size_t i = 0; i < strTempW.length(); ++i )
			{
        if ( strTempW[i] == ',' ) { strTempW[i] = '.'; }
			}

			if ( !IsFloatingPoint( strTempW, false ) ) 
      { 
        return (false); 
      }

			Real tempW = static_cast< Real >( atof( strTempW.c_str() ) );

			if ( bNoData )
			{
				if ( tempW == noData ) { tempW = NO_ELEVATION; }
			}

			setWorldWAt( u, v, tempW );
		}
	}

	// normalizes W
	normalizeW();

	// mirrors W vertically
	mirrorV();

	cout << "Closed file: " << fileName << endl;

	return (true);
}

// --------------------------------------------------------------------------------

// loads data from *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::loadFromXYZ(char* fileName)
{
	return loadFromXYZ(string(fileName));
}

// --------------------------------------------------------------------------------

// loads data from *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::loadFromXYZ(string fileName)
{
	// if the grid is not empty -> resets it
	if (!empty())
	{
		// clears all data
		clear();
		// default units are meters for all coordinates
		setDefaultWorldUnits();
	}

	ifstream iFile(fileName.c_str(), ios::in | ios::binary);

	// error in opening the file
	if (!iFile) 
	{
		return false;
	}

	cout << "Opened file: " << fileName << endl;

	cout << "Reading data from file: " << fileName << endl;

	// sets units
	m_WorldUnitsU = suMETERS;
	m_WorldUnitsV = suMETERS;
	m_WorldUnitsW = euMETERS;

	// read file for the first time to calculates all map parameters
	set<Real> tempX;
	set<Real> tempY;

	Real x;
	Real y;
	Real z;
	
	while (!iFile.eof())
	{
		iFile >> x >> y >> z;
		if (!iFile.fail()) 
		{
			tempX.insert(x);
			tempY.insert(y);
		}
	}

	unsigned int sizeU = static_cast<unsigned int>(tempX.size());
	unsigned int sizeV = static_cast<unsigned int>(tempY.size());

	Real originU = *tempX.begin(); 
	Real originV = *tempY.begin(); 

	// new origin
	m_OriginWorldU = originU;
	m_OriginWorldV = originV;
	m_OriginWorldW = EtMath<Real>::ZERO;

	Real originUplusOne = *(++tempX.begin()); 
	Real originVplusOne = *(++tempY.begin()); 

	// sets resolutions UV
	m_ResolutionU = originUplusOne - originU;
	m_ResolutionV = originVplusOne - originV;
	// elevation resolution is truncated to 0.01 meters
	m_ResolutionW = 0.01;

	// resize grid to contain data
	// force filling with NO_ELEVATION
	setUVSize(sizeU, sizeV);

	// rewind the file
	iFile.clear();
	iFile.seekg(0, iFile.beg);

	// and re-read to get elevations
	while (!iFile.eof())
	{
		iFile >> x >> y >> z;
		if (!iFile.fail()) 
		{
			unsigned int u = static_cast<unsigned int>((x - originU) / m_ResolutionU);
			unsigned int v = static_cast<unsigned int>((y - originV) / m_ResolutionV);
//			setWAt(u, v, static_cast<Integer>(z * 10));
			setWorldWAt(u, v, z);
		}
	}

	// normalizes W
	normalizeW();

	cout << "Closed file: " << fileName << endl;

	return true;
}

// --------------------------------------------------------------------------------
// SAVERS
// --------------------------------------------------------------------------------

// saves data to *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::saveToXYZ(char* fileName)
{
	return saveToXYZ(string(fileName));
}

// --------------------------------------------------------------------------------

// saves data to *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::saveToXYZ(string fileName)
{
	// if the grid is empty -> no data to save
	if (empty())
	{
		return false;
	}

	ofstream oFile;
	oFile.open(fileName.c_str(), ios::out);

#ifdef _DEBUG
	cout << "Opened file: " << fileName << endl;
#endif

	// error in opening the file
	if (!oFile.is_open()) 
	{
		return false;
	}

	for (unsigned int i = 0; i < m_SizeU; ++i)
	{
		for (unsigned int j = 0; j < m_SizeV; ++j)
		{
			oFile << setw(8) << setfill(' ') << i;
			oFile << setw(10) << setfill(' ') << j;
			oFile << setw(10) << setfill(' ') << getWAt(i, j) << endl;
		}
	}

	oFile.close();

#ifdef _DEBUG
	cout << "Closed file: " << fileName << endl;
#endif

	return true;
}

// --------------------------------------------------------------------------------

// saves data of the subGrid of this grid with the specified
// SW and NE corners to *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::saveSubGridToXYZ(char* fileName, const unsigned int swCornerU, const unsigned int swCornerV, const unsigned int neCornerU, const unsigned int neCornerV)
{
	return saveSubGridToXYZ(string(fileName), swCornerU, swCornerV, neCornerU, neCornerV);
}

// --------------------------------------------------------------------------------

// saves data of the subGrid of this grid with the specified
// SW and NE corners to *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::saveSubGridToXYZ(string fileName, const unsigned int swCornerU, const unsigned int swCornerV, const unsigned int neCornerU, const unsigned int neCornerV)
{
	// if the grid is empty -> no data to save
	if(empty())
	{
		return false;
	}

	// if corners are not valid -> return
	if((swCornerU > neCornerU) || (swCornerV > neCornerV))
	{
		return false;
	}

	if((neCornerU > m_SizeU) || (neCornerV > m_SizeV))
	{
		return false;
	}

	ofstream oFile;
	oFile.open(fileName.c_str(), ios::out);

#ifdef _DEBUG
	cout << "Opened file: " << fileName << endl;
#endif

	// error in opening the file
	if(!oFile.is_open()) 
	{
		return false;
	}

	for(unsigned int i = swCornerU; i < neCornerU; ++i)
	{
		for(unsigned int j = swCornerV; j < neCornerV; ++j)
		{
			oFile << setw(8) << setfill(' ') << (i - swCornerU);
			oFile << setw(10) << setfill(' ') << (j - swCornerV);
			oFile << setw(10) << setfill(' ') << getWAt(i, j) << endl;
		}
	}

	oFile.close();

#ifdef _DEBUG
	cout << "Closed file: " << fileName << endl;
#endif

	return true;
}

// --------------------------------------------------------------------------------

// saves World data to *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::saveAsWorldToXYZ(char* fileName)
{
	return saveAsWorldToXYZ(string(fileName));
}

// --------------------------------------------------------------------------------

// saves World data to *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::saveAsWorldToXYZ(string fileName)
{
	// if the grid is empty -> no data to save
	if (empty())
	{
		return false;
	}

	ofstream oFile;
	oFile.open(fileName.c_str(), ios::out);

#ifdef _DEBUG
	cout << "Opened file: " << fileName << endl;
#endif

	// error in opening the file
	if (!oFile.is_open()) 
	{
		return false;
	}

	for (unsigned int i = 0; i < m_SizeU; ++i)
	{
		for (unsigned int j = 0; j < m_SizeV; ++j)
		{
			oFile << setiosflags(ios::fixed) << setprecision(2);
			oFile << setw(8) << setfill(' ') << getWorldUAt(i);
			oFile << setw(10) << setfill(' ') << getWorldVAt(j);
			oFile << setw(10) << setfill(' ') << getWorldWAt(i, j) << endl;
		}
	}

	oFile.close();

#ifdef _DEBUG
	cout << "Closed file: " << fileName << endl;
#endif

	return true;
}

// --------------------------------------------------------------------------------

template < class Real, class Integer >
bool EtRectElevationGrid< Real, Integer >::saveAsWorldToARCINFOASCIIGrid( char* filename )
{
	return (saveAsWorldToARCINFOASCIIGrid( string( filename ) ));
}

// --------------------------------------------------------------------------------

template < class Real, class Integer >
bool EtRectElevationGrid< Real, Integer >::saveAsWorldToARCINFOASCIIGrid( const string& filename )
{
	// if the grid is empty -> no data to save
	if ( empty() ) { return (false); }

	ofstream oFile;
	oFile.open( filename.c_str(), ios::out );

	cout << "Saving file: " << filename << endl;

	// error in opening the file
	if ( !oFile.is_open() ) { return (false); }

	oFile << "ncols         " << m_SizeU << endl;
	oFile << "nrows         " << m_SizeV << endl;
	oFile << setiosflags( ios::fixed ) << setprecision( 1 );
	oFile << "xllcorner     " << m_OriginWorldU << endl;
	oFile << "yllcorner     " << m_OriginWorldV << endl;
  oFile << "cellsize      " << m_ResolutionU << endl;
	oFile << "NODATA_value  -9999" << endl;

	oFile << setiosflags( ios::fixed ) << setprecision( 2 );

	for ( int i = (m_SizeV - 1); i >= 0; --i )
	{
		for ( size_t j = 0; j < m_SizeU; ++j )
		{
			oFile << getWorldWAt( j, i ) << " ";
		}
		oFile << endl;
	}

	oFile.close();

	cout << "Closed file: " << filename << endl;

	return (true);
}

// --------------------------------------------------------------------------------

// saves World data of the subGrid of this grid with the specified
// SW and NE corners to *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::saveSubGridAsWorldToXYZ(char* fileName, const unsigned int swCornerU, const unsigned int swCornerV, const unsigned int neCornerU, const unsigned int neCornerV)
{
	return saveSubGridAsWorldToXYZ(string(fileName), swCornerU, swCornerV, neCornerU, neCornerV);
}

// --------------------------------------------------------------------------------

// saves World data of the subGrid of this grid with the specified
// SW and NE corners to *.xyz files
// returns true if successfull
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::saveSubGridAsWorldToXYZ(string fileName, const unsigned int swCornerU, const unsigned int swCornerV, const unsigned int neCornerU, const unsigned int neCornerV)
{
	// if the grid is empty -> no data to save
	if(empty())
	{
		return false;
	}

	// if corners are not valid -> return
	if((swCornerU > neCornerU) || (swCornerV > neCornerV))
	{
		return false;
	}

	if((neCornerU > m_SizeU) || (neCornerV > m_SizeV))
	{
		return false;
	}

	ofstream oFile;
	oFile.open(fileName.c_str(), ios::out);

#ifdef _DEBUG
	cout << "Saving file: " << fileName << endl;
#endif

	// error in opening the file
	if(!oFile.is_open()) 
	{
		return false;
	}

	for(unsigned int i = swCornerU; i < neCornerU; ++i)
	{
		for(unsigned int j = swCornerV; j < neCornerV; ++j)
		{
			oFile << setiosflags(ios::fixed) << setprecision(2);
			oFile << setw(8) << setfill(' ') << getWorldUAt(i);
			oFile << setw(10) << setfill(' ') << getWorldVAt(j);
			oFile << setw(10) << setfill(' ') << getWorldWAt(i, j) << endl;
		}
	}

	oFile.close();

#ifdef _DEBUG
	cout << "Closed file: " << fileName << endl;
#endif

	return true;
}

// --------------------------------------------------------------------------------
// HELPERS
// --------------------------------------------------------------------------------

// set all World units to meters
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::setDefaultWorldUnits()
{
	m_WorldUnitsU = suMETERS;
	m_WorldUnitsV = suMETERS;
	m_WorldUnitsW = euMETERS;
}

// --------------------------------------------------------------------------------

// clears all data
template <class Real, class Integer>
void EtRectElevationGrid<Real, Integer>::clear()
{
	m_SizeU = 0;
	m_SizeV = 0;
	m_SizeW = 0;
	
	if (m_W) free(m_W);
	m_W = 0;

	m_WorldUnitsU = suNULL;
	m_WorldUnitsV = suNULL;
	m_WorldUnitsW = euNULL;

	m_ResolutionU = 0;
	m_ResolutionV = 0;
	m_ResolutionW = 0;

	m_OriginWorldU = 0;
	m_OriginWorldV = 0;
	m_OriginWorldW = 0;
}

// --------------------------------------------------------------------------------

// return true if the grid is empty (cleared)
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::empty() const 
{
	if((m_SizeU == 0) || (m_SizeV == 0)) return true;
	return false;
}

// --------------------------------------------------------------------------------

// allocs memory for the elevation array 
// return true if the memory allocation is successfull
// if fillW is true the grid is filled with the NO_ELEVATION value
template <class Real, class Integer>
bool EtRectElevationGrid<Real, Integer>::allocW(bool fillW)
{
	// doesn't alloc if the grid is empty
	if(empty()) return false;

	// alloc memory to contain the elevation data
	m_W = static_cast<Integer *>(malloc(m_SizeU * m_SizeV * sizeof(Integer)));

	// if no successfull
	if (!m_W)
	{
		return false;
	}

	// if fillW is true fills the grid with the NO_ELEVATION value
	if(fillW)
	{
		setAllWToSingleValue(NO_ELEVATION);
	}

	return true;
}

// --------------------------------------------------------------------------------

// returns the index in the array of the node at (u, v)
template < class Real, class Integer >
size_t EtRectElevationGrid< Real, Integer >::w_Index( size_t u, size_t v) const 
{
	return (v * m_SizeU + u);
}

// --------------------------------------------------------------------------------

// converts Surface Units to string for more meaningful output
template <class Real, class Integer>
string EtRectElevationGrid<Real, Integer>::surfaceUnitsAsString(const SurfaceUnits units) const 
{
	switch(units)
	{ 
	case suRADIANS:
		return "radians";
	case suFEET:
		return "feet";
	case suMETERS:
		return "meters";
	case suSECONDS:
		return "arc-seconds";
	case suNULL:
		return "not defined";
	default:
		return "not defined";
	}
}

// --------------------------------------------------------------------------------

// converts Elevation Units to string for more meaningful output
template <class Real, class Integer>
string EtRectElevationGrid<Real, Integer>::elevationUnitsAsString(const ElevationUnits units) const
{
	switch(units)
	{ 
	case euFEET:
		return "feet";
	case euMETERS:
		return "meters";
	case euNULL:
		return "not defined";
	default:
		return "not defined";
	}
}

// --------------------------------------------------------------------------------
// HELPERS FOR LOADER
// --------------------------------------------------------------------------------
// DT2
// --------------------------------------------------------------------------------
// Helper for DT2 loader: 
// converts from DDDMMSSH (degrees-minute-seconds-hemisphere) to Real
// the returned value is in arc-seconds
template <class Real>
Real DDDMMSSH_ToSeconds(char angle[9])
{
	char hem = angle[7];
	angle[7] = '\0';
	Real seconds = static_cast<Real>(atof(angle + 5));
	angle[5] = '\0';
	Real minutes = static_cast<Real>(atof(angle + 3));
	angle[3] = '\0';
	Real degrees = static_cast<Real>(atof(angle));

	Real secs = degrees * EtUnitsConverter<Real>::ANG_DEG_TO_SEC + minutes * EtUnitsConverter<Real>::ANG_MIN_TO_SEC + seconds;

	if ((hem == 'W') || (hem == 'S')) 
	{
		secs *= -1;
	}

	return secs;
}

// --------------------------------------------------------------------------------
