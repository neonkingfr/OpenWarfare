#ifndef ETSTRING_H
#define ETSTRING_H

#include <string>

using std::string;

class EtString
{
public:
	// returns the string obtained from the specified string
	// deleting all the leading spaces (' ') and tabs ('\t')
	static string LTrim(const string& s);
	// returns the string obtained from the specified string
	// deleting all the trailing spaces (' ') and tabs ('\t')
	static string RTrim(const string& s);
	// returns the string obtained from the specified string
	// by removing the specified string to be removed if present
	static string RemoveFromStart(const string& s, const string& toBeRemoved);
};

#endif