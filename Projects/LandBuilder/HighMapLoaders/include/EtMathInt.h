#ifndef ETMATHINT_H
#define ETMATHINT_H

template <class Integer>
class EtMathInt
{
public:
	// returns the factorial of the given integer number
	// works only with positive integers
	// returns 1 if value is negative
	static Integer Factorial(Integer value);

	// returns the double factorial of the given integer number
	// works only with positive integers
	// returns 1 if value is negative
	static Integer DoubleFactorial(Integer value);

	// returns the power of 2 with the given integer exponent
	// works only with positive integers
	// returns 1 if value is negative
	static Integer PowerOfTwo(Integer value);
};

#include "..\src\EtMathInt.inl"


#endif