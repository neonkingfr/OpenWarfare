//-----------------------------------------------------------------------------
// File: DXF2007Importer.h
//
// Desc: Exporter of shape data
//
// Auth: phoboss
//
// Copyright (c) 2000-2008 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#ifndef __INC_DXF2007IMPORTER_H
#define __INC_DXF2007IMPORTER_H

//-----------------------------------------------------------------------------

#include <fstream>
#include <string>
#include <vector>

//-----------------------------------------------------------------------------

#include "CG_Polygon2.h"

//-----------------------------------------------------------------------------

using std::ifstream;
using std::string;
using std::vector;
using std::streamoff;
using std::ios_base;

//-----------------------------------------------------------------------------

/*!
Exporter of shape data in .dxf format
*/
class CDXF2007Importer
{
	ifstream m_in;

	vector< CG_Polygon2< double > > m_polygonList;

public:
	CDXF2007Importer();
	CDXF2007Importer( const string& filename );
	~CDXF2007Importer();

	void Filename( const string& filename );

	bool Read();

	const vector< CG_Polygon2< double > >& GetPolygons() const;

private:
	bool ReadLwPolyline();
	bool ReadLwPolylineAsPolygon( streamoff start, size_t verticesCount );
	bool ReadLwPolylineAsPolyline( streamoff start, size_t verticesCount );
	bool ReadLine( string& line );
};

//-----------------------------------------------------------------------------

#endif