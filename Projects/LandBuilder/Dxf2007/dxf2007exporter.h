//-----------------------------------------------------------------------------
// File: DXFExporter.h
//
// Desc: Exporter of shape data
//
// Auth: phoboss
//
// Copyright (c) 2000-2008 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#ifndef __INC_DXFEXPORTER_H
#define __INC_DXFEXPORTER_H

//-----------------------------------------------------------------------------

#include <fstream>
#include <string>

//-----------------------------------------------------------------------------

#include "CG_Polygon2.h"
#include "CG_Polyline2.h"

//-----------------------------------------------------------------------------

//class CPolylineArea;

//-----------------------------------------------------------------------------

using std::ofstream;
using std::string;
using std::endl;

//-----------------------------------------------------------------------------

/*!
Exporter of shape data in .dxf format
*/
class CDXF2007Exporter
{
	typedef double Float;
	typedef CG_Polygon2< Float >  Polygon2;
	typedef CG_Polyline2< Float > Polyline2;
	typedef CG_Point2< Float >    Point2;

	ofstream m_out;

	void WriteEntitiesHead();
	void WriteEntitiesTail();
	void WriteFileTail();

public:
	CDXF2007Exporter();
	CDXF2007Exporter( const string& filename );
	~CDXF2007Exporter();

	void Filename( const string& filename );
	void Filename( const char* filename );

//	void WritePolyline( CPolylineArea* pPolyline, const wxString& layer,
//						bool bClose, double dStartingWidth = 0.0, 
//						double dEndingWidth = 0.0 );

	void WritePolygon( const Polygon2& poly, const string& layer,
                     Float startingWidth = (Float)0, Float endingWidth = (Float)0 );

	void WritePolyline( const Polyline2& poly, const string& layer,
                      Float startingWidth = (Float)0, Float endingWidth = (Float)0 );

	void WriteCircle( const Point2& center, Float radius, const string& layer );
};

//-----------------------------------------------------------------------------

#endif