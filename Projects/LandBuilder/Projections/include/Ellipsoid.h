#ifndef ELLIPSOID_H
#define ELLIPSOID_H

template <class Real>
class Ellipsoid
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:

	// ------------------- //
	// the semi-major axis //
	// ------------------- //
	Real m_SemiMajorAxis;

	// ------------------- //
	// the semi-minor axis //
	// ------------------- //
	Real m_SemiMinorAxis;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ------------ //
	// Constructors //
	// ------------ //

	// Default
	Ellipsoid();

	// Constructs this ellipsoid with the given parameters
	Ellipsoid(Real semiMajorAxis, Real semiMinorAxis);

	// Copy constructor
	Ellipsoid(const Ellipsoid<Real>& other);

	// ----------- //
	// Assignement //
	// ----------- //
	Ellipsoid<Real>& operator = (const Ellipsoid<Real>& other);

	// ------------------------ //
	// Member variables getters //
	// ------------------------ //

	// GetSemiMajorAxis()
	// Returns the semi major axis of this ellipsoid.
	Real GetSemiMajorAxis() const;

	// GetSemiMinorAxis()
	// Returns the semi minor axis of this ellipsoid.
	Real GetSemiMinorAxis() const;

	// -------------------- //
	// Ellipsoid properties //
	// -------------------- //

	// Eccentricity()
	// Returns the eccentricity of this ellipsoid. 
	Real Eccentricity() const;

	// Flattening()
	// Returns the flattening of this ellipsoid. 
	Real Flattening() const;

	// MeridianRadius()
	// Returns the radius of the meridian of this ellipsoid at the given latitude (in radians). 
	Real MeridianRadius(Real latitude) const;

	// PrimeVerticalRadius()
	// Returns the radius of curvature of the prime vertical of this ellipsoid at the 
	// given latitude (in radians).
	// ------------------------------------------------------------------------------
	// prime vertical = circle perpendicular to the meridian at the given latitude                         
	Real PrimeVerticalRadius(Real latitude) const;

	// GetDistanceFromEquatorAlongMeridian()                  
	// Returns the distance (in meters) from the point at the 
	// given latitude (in radians) to the equator, along the  
	// meridian passing from the point.                       
	Real GetDistanceFromEquatorAlongMeridian(Real latitude) const;

	// ---------------- //
	// Helper functions //
	// ---------------- //
private:

	// ValidateSemiAxes()
	// Validates the semiaxes.
	void ValidateSemiAxes();
};

#include "..\src\Ellipsoid.inl"

#endif