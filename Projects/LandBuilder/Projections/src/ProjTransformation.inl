// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
ProjTransformation<Real>::ProjTransformation()
{
	m_Code = ProjTransformationCode_UNDEFINED;
}

// -------------------------------------------------------------------------- //

template <class Real>
ProjTransformation<Real>::ProjTransformation(PredefinedProjTransformationCodes code) 
{
	m_Code = code;
}

// -------------------------------------------------------------------------- //

template <class Real>
ProjTransformation<Real>::ProjTransformation(const ProjTransformation<Real>& other)
{
	m_Code      = other.m_Code;
	m_Ellipsoid = other.m_Ellipsoid;

	size_t paramCount = other.m_ParametersIn.size();
	for(unsigned int i = 0; i < paramCount; ++i)
	{
		m_ParametersIn.push_back(other.m_ParametersIn[i]);
	}
}

// ************************************************************************** //
// Assignement                                                                //
// ************************************************************************** //

template <class Real>
ProjTransformation<Real>& ProjTransformation<Real>::operator = (const ProjTransformation<Real>& other)
{
	m_Code      = other.m_Code;
	m_Ellipsoid = other.m_Ellipsoid;

	m_ParametersIn.clear();
	size_t paramCount = other.m_ParametersIn.size();
	for(unsigned int i = 0; i < paramCount; ++i)
	{
		m_ParametersIn.push_back(other.m_ParametersIn[i]);
	}
	return *this;
}

// ************************************************************************** //
// Member variables getters                                                   //
// ************************************************************************** //

template <class Real>
const GeoEllipsoid<Real>& ProjTransformation<Real>::GetEllipsoid() const
{
	return m_Ellipsoid;
}

// ************************************************************************** //
// Transformation functions                                                   //
// ************************************************************************** //

template <class Real>
ProjTransformationReturnValues ProjTransformation<Real>::Wgs84ToUtm(Real latitude, Real longitude, ProjTransformationReturnData<Real>& returnData)
{
	if(m_Ellipsoid.Eccentricity() == EtMath<Real>::MAX_REAL) return PjTrRet_INVALID_ELLIPSOID;
	if(m_ParametersIn.size() != 3) return PjTrRet_INVALID_PARAM_NUMBER;

	Real degLatitude  = EtMath<Real>::RadToDeg(latitude);
	Real degLongitude = EtMath<Real>::RadToDeg(longitude);

	// verifies limit
	if(degLongitude < static_cast<Real>(-180.0) || degLongitude > static_cast<Real>(180.0)) return PjTrRet_OUTOFBOUND_LONGITUDE;
	if(degLatitude < static_cast<Real>(-80.0) || degLatitude > static_cast<Real>(84.0)) return PjTrRet_OUTOFBOUND_LATITUDE;

	returnData.utmLonZone = static_cast<unsigned int>((degLongitude + static_cast<Real>(180.0)) / static_cast<Real>(6.0)) + 1; 
	Real longitudeCentralMeridian = EtMath<Real>::DegToRad((returnData.utmLonZone - 1) * static_cast<Real>(6.0) - static_cast<Real>(180.0) + static_cast<Real>(3.0));

	// Special zones
	if(degLatitude >= static_cast<Real>(56.0) && degLatitude < static_cast<Real>(64.0))
	{
		if(degLongitude >= static_cast<Real>(0.0) && degLongitude < static_cast<Real>(3.0))
		{
			returnData.utmLonZone = 31;
			longitudeCentralMeridian = EtMath<Real>::DegToRad(static_cast<Real>(1.5));
		}
		else if(degLongitude >= static_cast<Real>(3.0) && degLongitude < static_cast<Real>(12.0))
		{
			returnData.utmLonZone = 32;
			longitudeCentralMeridian = EtMath<Real>::DegToRad(static_cast<Real>(7.5));
		}
	}

	if(degLatitude >= static_cast<Real>(72.0) && degLatitude < static_cast<Real>(84.0)) 
	{
		if(degLongitude >= static_cast<Real>(0.0) && degLongitude < static_cast<Real>(9.0)) 
		{
			returnData.utmLonZone = 31;
			longitudeCentralMeridian = EtMath<Real>::DegToRad(static_cast<Real>(4.5));
		}
		else if(degLongitude >= static_cast<Real>(9.0) && degLongitude < static_cast<Real>(21.0)) 
		{
			returnData.utmLonZone = 33;
			longitudeCentralMeridian = EtMath<Real>::DegToRad(static_cast<Real>(15.0));
		}
		else if(degLongitude >= static_cast<Real>(21.0) && degLongitude < static_cast<Real>(33.0)) 
		{
			returnData.utmLonZone = 35;
			longitudeCentralMeridian = EtMath<Real>::DegToRad(static_cast<Real>(27.0));
		}
		else if(degLongitude >= static_cast<Real>(33.0) && degLongitude < static_cast<Real>(42.0)) 
		{
			returnData.utmLonZone = 37;
			longitudeCentralMeridian = EtMath<Real>::DegToRad(static_cast<Real>(37.5));
		}
	}

	Real n_0 = static_cast<Real>(0.0);
	Real n_1 = static_cast<Real>(1.0);
	Real n_2 = static_cast<Real>(2.0);
	Real n_4 = static_cast<Real>(4.0);
	Real n_5 = static_cast<Real>(5.0);
	Real n_6 = static_cast<Real>(6.0);
	Real n_9 = static_cast<Real>(9.0);
	Real n_12 = static_cast<Real>(12.0);
	Real n_14 = static_cast<Real>(14.0);
	Real n_18 = static_cast<Real>(18.0);
	Real n_20 = static_cast<Real>(20.0);
	Real n_30 = static_cast<Real>(30.0);
	Real n_42 = static_cast<Real>(42.0);
	Real n_56 = static_cast<Real>(56.0);
	Real n_58 = static_cast<Real>(58.0);
	Real n_61 = static_cast<Real>(61.0);
	Real n_179 = static_cast<Real>(179.0);
	Real n_270 = static_cast<Real>(270.0);
	Real n_330 = static_cast<Real>(330.0);
	Real n_479 = static_cast<Real>(479.0);
	Real n_543 = static_cast<Real>(543.0);
	Real n_1385 = static_cast<Real>(1385.0);
	Real n_3111 = static_cast<Real>(3111.0);

	Real N = m_Ellipsoid.PrimeVerticalRadius(latitude);
	Real cosPhi = EtMath<Real>::Cos(latitude);

	// this should never happen. this condition is true only at poles
	// and we have already clamped latitude
	if(cosPhi == n_0) return PjTrRet_OUTOFBOUND_LATITUDE;

	Real sinPhi = EtMath<Real>::Sin(latitude);
	Real t = sinPhi / cosPhi;
	Real t2 = t * t;
	Real t4 = t2 * t2;
	Real t6 = t4 * t2;

	Real e2 = m_Ellipsoid.Eccentricity();
	Real mu2 = e2 / (n_1 - e2) * cosPhi * cosPhi;
	Real mu4 = mu2 * mu2;

	Real lam = longitude - longitudeCentralMeridian;

	Real lam_cosPhi = lam * cosPhi;
	Real lam2_cosPhi2 = lam_cosPhi * lam_cosPhi;

	Real M = m_Ellipsoid.GetDistanceFromEquatorAlongMeridian(latitude);

	Real lam2_sinPhi_cosPhi = lam * lam * sinPhi * cosPhi;
	Real lam2_sinPhi_cosPhi_2 = lam2_sinPhi_cosPhi / n_2;

	Real x_Par_1 = n_1 - t2 + mu2;
	Real x_Par_2 = n_5 - n_18 * t2 + t4 + n_14 * mu2 - n_58 * t2 * mu2;
	Real x_Par_3 = n_61 - n_479 * t2 + n_179 * t4 - t6;


	Real y_Par_1 = n_5 - t2 + n_9 * mu2 + n_4 * mu4;
	Real y_Par_2 = n_61 - n_58 * t2 + t4 + n_270 * mu2 - n_330 * t2 * mu2;
	Real y_Par_3 = n_1385 - n_3111 * t2 + n_543 * t4 - t6;

	// easting determination
	Real x = lam_cosPhi * (n_1 + lam2_cosPhi2 / n_6 * (x_Par_1 + 
								 lam2_cosPhi2 / n_20 * (x_Par_2 +
								 lam2_cosPhi2 / n_42 * x_Par_3)));

	returnData.easting = x * N * m_ParametersIn[0] + m_ParametersIn[1];

	// northing determination	
	Real y = lam2_sinPhi_cosPhi_2 * (n_1 + lam2_cosPhi2 / n_12 * (y_Par_1 +
										   lam2_cosPhi2 / n_30 * (y_Par_2 +
										   lam2_cosPhi2 / n_56 * y_Par_3)));

	Real N0 = n_0;
	if(latitude < n_0)
	{
		N0 = m_ParametersIn[2];
	}

	returnData.northing = (M + y * N) * m_ParametersIn[0] + N0;

	if((static_cast<Real>(84.0) >= degLatitude) && (degLatitude >= static_cast<Real>(72.0))) 
	{
		returnData.utmLatZone = 'X';
	}
	else if((static_cast<Real>(72.0) > degLatitude) && (degLatitude >= static_cast<Real>(64.0))) 
	{
		returnData.utmLatZone = 'W';
	}
	else if((static_cast<Real>(64.0) > degLatitude) && (degLatitude >= static_cast<Real>(56.0))) 
	{
		returnData.utmLatZone = 'V';
	}
	else if((static_cast<Real>(56.0) > degLatitude) && (degLatitude >= static_cast<Real>(48.0))) 
	{
		returnData.utmLatZone = 'U';
	}
	else if((static_cast<Real>(48.0) > degLatitude) && (degLatitude >= static_cast<Real>(40.0))) 
	{
		returnData.utmLatZone = 'T';
	}
	else if((static_cast<Real>(40.0) > degLatitude) && (degLatitude >= static_cast<Real>(32.0))) 
	{
		returnData.utmLatZone = 'S';
	}
	else if((static_cast<Real>(32.0) > degLatitude) && (degLatitude >= static_cast<Real>(24.0))) 
	{
		returnData.utmLatZone = 'R';
	}
	else if((static_cast<Real>(24.0) > degLatitude) && (degLatitude >= static_cast<Real>(16.0))) 
	{
		returnData.utmLatZone = 'Q';
	}
	else if((static_cast<Real>(16.0) > degLatitude) && (degLatitude >= static_cast<Real>(8.0))) 
	{
		returnData.utmLatZone = 'P';
	}
	else if((static_cast<Real>(8.0) > degLatitude) && (degLatitude >= static_cast<Real>(0.0))) 
	{
		returnData.utmLatZone = 'N';
	}
	else if((static_cast<Real>(0.0) > degLatitude) && (degLatitude >= static_cast<Real>(-8.0))) 
	{
		returnData.utmLatZone = 'M';
	}
	else if((static_cast<Real>(-8.0) > degLatitude) && (degLatitude >= static_cast<Real>(-16.0))) 
	{
		returnData.utmLatZone = 'L';
	}
	else if((static_cast<Real>(-16.0) > degLatitude) && (degLatitude >= static_cast<Real>(-24.0))) 
	{
		returnData.utmLatZone = 'K';
	}
	else if((static_cast<Real>(-24.0) > degLatitude) && (degLatitude >= static_cast<Real>(-32.0))) 
	{
		returnData.utmLatZone = 'J';
	}
	else if((static_cast<Real>(-32.0) > degLatitude) && (degLatitude >= static_cast<Real>(-40.0))) 
	{
		returnData.utmLatZone = 'H';
	}
	else if((static_cast<Real>(-40.0) > degLatitude) && (degLatitude >= static_cast<Real>(-48.0))) 
	{
		returnData.utmLatZone = 'G';
	}
	else if((static_cast<Real>(-48.0) > degLatitude) && (degLatitude >= static_cast<Real>(-56.0))) 
	{
		returnData.utmLatZone = 'F';
	}
	else if((static_cast<Real>(-56.0) > degLatitude) && (degLatitude >= static_cast<Real>(-64.0))) 
	{
		returnData.utmLatZone = 'E';
	}
	else if((static_cast<Real>(-64.0) > degLatitude) && (degLatitude >= static_cast<Real>(-72.0))) 
	{
		returnData.utmLatZone = 'D';
	}
	else if((static_cast<Real>(-72.0) > degLatitude) && (degLatitude >= static_cast<Real>(-80.0))) 
	{
		returnData.utmLatZone = 'C';
	}
	else 
	{
		returnData.utmLatZone = 'Z'; //This is here as an error flag to show that the Latitude is outside the UTM limits
	}
	return PjTrRet_SUCCESS;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
ProjTransformationReturnValues ProjTransformation<Real>::TransformDirect(Real latitude, Real longitude, ProjTransformationReturnData<Real>& returnData)
{
	m_ParametersIn.clear();

	switch(m_Code)
	{
	case ProjTransformationCode_UNDEFINED:
		{
			m_Ellipsoid.SetPredefined(GeoEllipsoidCode_UNDEFINED);
			return PjTrRet_INVALID_ELLIPSOID;
		}
		break;
	case ProjTransformationCode_WGS84TOUTM:
		{
			m_Ellipsoid.SetPredefined(GeoEllipsoidCode_WGS84);
			SetWgs84ToUtmParametersIn();
			return Wgs84ToUtm(latitude, longitude, returnData);
		}
		break;
	default:
		{
			m_Ellipsoid.SetPredefined(GeoEllipsoidCode_NOTIMPLEMENTED);
			return PjTrRet_INVALID_ELLIPSOID;
		}
	}
}

// ************************************************************************** //
// Helper functions                                                           //
// ************************************************************************** //

template <class Real>
bool ProjTransformation<Real>::SetWgs84ToUtmParametersIn()
{
	m_ParametersIn.clear();

	Real k0 = static_cast<Real>(0.9996);
	m_ParametersIn.push_back(k0);

	Real E0 = static_cast<Real>(500000.0);
	m_ParametersIn.push_back(E0);

	Real N0 = static_cast<Real>(10000000.0);
	m_ParametersIn.push_back(N0);

	return true;
}
