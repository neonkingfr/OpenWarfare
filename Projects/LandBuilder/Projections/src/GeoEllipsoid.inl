// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
GeoEllipsoid<Real>::GeoEllipsoid() : Ellipsoid()
{
}

// -------------------------------------------------------------------------- //

template <class Real>
GeoEllipsoid<Real>::GeoEllipsoid(string name, Real semiMajorAxis, Real semiMinorAxis) 
: Ellipsoid(semiMajorAxis, semiMinorAxis) 
, m_Name(name)
{
}

// -------------------------------------------------------------------------- //

template <class Real>
GeoEllipsoid<Real>::GeoEllipsoid(PredefinedGeoEllipsoidCodes code)
{
	SetPredefined(code);
}

// -------------------------------------------------------------------------- //

template <class Real>
GeoEllipsoid<Real>::GeoEllipsoid(const GeoEllipsoid<Real>& other)
: Ellipsoid(other.m_SemiMajorAxis, other.m_SemiMinorAxis)
, m_Name(other.m_Name)
{
}

// ************************************************************************** //
// Assignement                                                                //
// ************************************************************************** //

template <class Real>
GeoEllipsoid<Real>& GeoEllipsoid<Real>::operator = (const GeoEllipsoid<Real>& other)
{
	m_Name          = other.m_Name;
	m_SemiMajorAxis = other.m_SemiMajorAxis;
	m_SemiMinorAxis = other.m_SemiMinorAxis;
	return *this;
}

// ************************************************************************** //
// Member variables getters                                                   //
// ************************************************************************** //

template <class Real>
const string& GeoEllipsoid<Real>::GetName() const
{
	return m_Name;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
void GeoEllipsoid<Real>::SetPredefined(PredefinedGeoEllipsoidCodes code)
{
	string name;
	Real semiMajorAxis;
	Real semiMinorAxis;
	Real invFlattening;

	Real ZERO = static_cast<Real>(0.0);
	Real MINUS_ONE = static_cast<Real>(-1.0);

	switch(code)
	{
	case GeoEllipsoidCode_UNDEFINED:
		{
			name = "Undefined";
			semiMajorAxis = ZERO;
			semiMinorAxis = ZERO;
		}
		break;
	case GeoEllipsoidCode_WGS84:
		{
			name = "WGS 84";
			semiMajorAxis = static_cast<Real>(6378137.0);
			semiMinorAxis = MINUS_ONE;
			invFlattening = static_cast<Real>(298.257223563);
		}
		break;
	default:
		{
			name = "Not implemented";
			semiMajorAxis = ZERO;
			semiMinorAxis = ZERO;
		}
	}

	if(semiMinorAxis == MINUS_ONE)
	{
		Real ONE = static_cast<Real>(1.0);
		semiMinorAxis = semiMajorAxis * (ONE - ONE / invFlattening);
	}

	m_Name          = name;
	m_SemiMajorAxis = semiMajorAxis;
	m_SemiMinorAxis = semiMinorAxis;
}