#include "..\..\HighMapLoaders\include\EtMath.h"
#include "..\..\HighMapLoaders\include\EtMathInt.h"

// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
Ellipsoid<Real>::Ellipsoid()
{
}

// -------------------------------------------------------------------------- //

template <class Real>
Ellipsoid<Real>::Ellipsoid(Real semiMajorAxis, Real semiMinorAxis) 
{
	// semi axes must be positive
	Real ZERO = static_cast<Real>(0.0);
	m_SemiMajorAxis = (semiMajorAxis >= ZERO) ? semiMajorAxis : -semiMajorAxis;
	m_SemiMinorAxis = (semiMinorAxis >= ZERO) ? semiMinorAxis : -semiMinorAxis;
	ValidateSemiAxes();
}

// -------------------------------------------------------------------------- //

template <class Real>
Ellipsoid<Real>::Ellipsoid(const Ellipsoid<Real>& other)
: m_SemiMajorAxis(other.m_SemiMajorAxis)
, m_SemiMinorAxis(other.m_SemiMinorAxis)
{
}

// ************************************************************************** //
// Assignement                                                                //
// ************************************************************************** //

template <class Real>
Ellipsoid<Real>& Ellipsoid<Real>::operator = (const Ellipsoid<Real>& other)
{
	m_SemiMajorAxis = other.m_SemiMajorAxis;
	m_SemiMinorAxis = other.m_SemiMinorAxis;
	return *this;
}

// ************************************************************************** //
// Member variables getters                                                   //
// ************************************************************************** //

template <class Real>
Real Ellipsoid<Real>::GetSemiMajorAxis() const
{
	return m_SemiMajorAxis;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real Ellipsoid<Real>::GetSemiMinorAxis() const
{
	return m_SemiMinorAxis;
}

// ************************************************************************** //
// Ellipsoid properties                                                       //
// ************************************************************************** //

template <class Real>
Real Ellipsoid<Real>::Eccentricity() const
{
	// ************************************ //
	// eccentricity e^2 = (a^2 - b^2) / a^2 // 
	// ************************************ //

	if(m_SemiMajorAxis > EtMath<Real>::ZERO_TOLERANCE)
	{
		Real a2 = m_SemiMajorAxis * m_SemiMajorAxis;
		Real b2 = m_SemiMinorAxis * m_SemiMinorAxis;
		return ((a2 - b2) / a2);
	}
	else
	{
		return EtMath<Real>::MAX_REAL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
Real Ellipsoid<Real>::Flattening() const
{
	// ************************** //
	// flattening f = (a - b) / a // 
	// ************************** //
	if(m_SemiMajorAxis > EtMath<Real>::ZERO_TOLERANCE)
	{
		return ((m_SemiMajorAxis - m_SemiMinorAxis) / m_SemiMajorAxis);
	}
	else
	{
		return EtMath<Real>::MAX_REAL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
Real Ellipsoid<Real>::MeridianRadius(Real latitude) const
{
	// **************************************************************** //
	// meridian radius = a * (1 - e^2) / (1 - e^2 * (sin(phi))^2)^(3/2) // 
	// **************************************************************** //

	Real ZERO = static_cast<Real>(0.0);
	Real ONE  = static_cast<Real>(1.0);

	Real e2     = Eccentricity();
	Real sinPhi = EtMath<Real>::Sin(latitude);

	Real sinPhi2 = sinPhi * sinPhi;

	Real t_den  = ONE - e2 * sinPhi2;
	Real t_den3 = t_den * t_den * t_den;

	// square root or ratio undefined 
	if(t_den3 <= ZERO) return EtMath<Real>::MAX_REAL;

	Real num = m_SemiMajorAxis * (ONE - e2);
	Real den = EtMath<Real>::Sqrt(t_den3);

	return (num / den);
}

// -------------------------------------------------------------------------- //

template <class Real>
Real Ellipsoid<Real>::PrimeVerticalRadius(Real latitude) const
{
	// ********************************************************** //
	// prive vertical radius = a / (1 - e^2 * (sin(phi))^2)^(1/2) // 
	// ********************************************************** //

	Real ZERO = static_cast<Real>(0.0);
	Real ONE  = static_cast<Real>(1.0);

	Real e2     = Eccentricity();
	Real sinPhi = EtMath<Real>::Sin(latitude);

	Real sinPhi2 = sinPhi * sinPhi;

	Real t_den = ONE - e2 * sinPhi2;

	// square root or ratio undefined 
	if(t_den <= ZERO) return EtMath<Real>::MAX_REAL;

	Real num = m_SemiMajorAxis;
	Real den = EtMath<Real>::Sqrt(t_den);

	return (num / den);
}

// -------------------------------------------------------------------------- //

template <class Real>
Real Ellipsoid<Real>::GetDistanceFromEquatorAlongMeridian(Real latitude) const
{
	Real ZERO = static_cast<Real>(0.0);
	Real ONE  = static_cast<Real>(1.0);

	Real e2     = Eccentricity();
	Real sinPhi = EtMath<Real>::Sin(latitude);
	Real cosPhi = EtMath<Real>::Cos(latitude);

	Real sinPhi2   = sinPhi * sinPhi; 
	Real sincosPhi = sinPhi * cosPhi;

	// determination of E
	Real e2n = ONE;
	Real prevE;
	Real currE = ONE;

	int iter = 1;
	do
	{
		prevE = currE;

		int t_a = 2 * iter - 1;

		Real t_b = static_cast<Real>(EtMathInt<int>::DoubleFactorial(t_a));
		Real t_c = static_cast<Real>(EtMathInt<int>::PowerOfTwo(iter));
		Real t_d = static_cast<Real>(EtMathInt<int>::Factorial(iter));
		Real t_e  = t_b / (t_c * t_d);
		Real t_e2 = t_e * t_e;
		
		e2n *= e2;

		Real iterE = t_e2 * e2n / static_cast<Real>(t_a);

		currE = prevE - iterE;

		iter++;
	} while(prevE - currE > EtMath<Real>::ZERO_TOLERANCE);

	Real E = currE;

	// determination of b serie
	e2n = ONE;
	Real sinPhi2N = ONE;
	Real prevB;
	Real currB = ONE - E;
	Real prevBsc;
	Real currBsc = currB;

	iter = 1;
	do
	{
		prevB   = currB;
		prevBsc = currBsc;

		int t_a = 2 * iter - 1;

		Real t_b = static_cast<Real>(EtMathInt<int>::DoubleFactorial(t_a));
		Real t_c = static_cast<Real>(EtMathInt<int>::PowerOfTwo(iter));
		Real t_d = static_cast<Real>(EtMathInt<int>::Factorial(iter));
		Real t_e  = t_b / (t_c * t_d);
		Real t_e2  = t_e * t_e;
		
		e2n *= e2;

		Real iterB = t_e2 * e2n / static_cast<Real>(t_a);

		currB = prevB - iterB;

		int t_f = 2 * iter;
		int t_g = 2 * iter + 1;

		Real t_h = static_cast<Real>(EtMathInt<int>::DoubleFactorial(t_f));
		Real t_i = static_cast<Real>(EtMathInt<int>::DoubleFactorial(t_g));

		sinPhi2N *= sinPhi2;

		Real iterBsc = currB * sinPhi2N * t_h / t_i;

		currBsc = prevBsc + iterBsc;

		iter++;
	} while (currBsc - prevBsc > EtMath<Real>::ZERO_TOLERANCE);

	Real Bsc = currBsc;

	Real Ephi = E * latitude + sincosPhi * Bsc;

	Real num = e2 * sincosPhi;
	Real den = ONE - e2 * sinPhi2;

	if(den <= ZERO) return(EtMath<Real>::MAX_REAL);

	den = EtMath<Real>::Sqrt(den);


	return (m_SemiMajorAxis * (Ephi - num / den));
}

// ************************************************************************** //
// Helper functions                                                           //
// ************************************************************************** //

template <class Real>
void Ellipsoid<Real>::ValidateSemiAxes()
{
	// minor semi axis must be smaller than major semi axis
	if(m_SemiMinorAxis > m_SemiMajorAxis)
	{
		Real t = m_SemiMinorAxis;
		m_SemiMinorAxis = m_SemiMajorAxis;
		m_SemiMajorAxis = t;
	}
}
