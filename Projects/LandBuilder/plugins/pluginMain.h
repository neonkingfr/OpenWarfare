// -------------------------------------------------------------------------- //

#pragma once

// -------------------------------------------------------------------------- //

#include <float.h>
#include <iostream>
#include <fstream>
#include <strstream>
#include <tchar.h>
#include <es/strings/rstring.hpp>
#include <el/BTree/Btree.h>
#include <el/Interfaces/IMultiInterfaceBase.hpp>
#include <el/MultiThread/ExceptionRegister.h>
#include <el/Pathname/pathname.h>
#include <el/Math/math3d.hpp>
#include <el/progressBar/ProgressBar.h>
#include <ProgressLog.h>
#include <ModuleRegistrator.h>
#include <IBuildModule.h>
#include <ILandBuildCmds.h>
#include "IPlugin.h"

// -------------------------------------------------------------------------- //

///Default implementation of IPlugin for current plugin
/**
    If you want to override this implementation, derive PluginMainClass and create one
    static instance. 
*/
class PluginMainClass : public IPlugin
{
protected:
    ///Pointer to LBGlobalPointers class. 
    /**
    Pointer is filled during initialization. Before initialization it contains NULL
    */
	LBGlobalPointers* _globalPointers;
public:
    
    ///Pointer to current only one instance
    /**
    This pointer can be NULL. In this case, default implementation will be used. Pointer is
    changed automatically after first instance is created
    */
    static PluginMainClass* _currentInstance;
    
    PluginMainClass();
    ~PluginMainClass();

    ///Called during initialization phase
    /**
     Contains standard initialization. You can override this function, but remember,
     you have to call base function before any other action is taken.
     
     @param globalPointers pointer to instance that contains global pointers.
     */
    virtual void InitPlugin(LBGlobalPointers* globalPointers);

    ///Get module list from the DLL
    /**
    @return pointer to registrator structure, that contains list of modules
    */
    LandBuilder2::ModuleRegistrator* GetModules();

    ///Caled before plugin is unloaded
    /**
     This function is best place for cleanup before plugin is unloaded. You can also
     use destructor, but using destructor has several limitations. You cannot for example
     call blocking operations in destructor, but this is no problem for Release()
     */
    virtual void Release();

    ///Sets the CWD
    /**
    Function used to set Pathname::SetCWD(). Because each module has own CWD Pathname object,
    this function synchronizes DLL's CWD with App's CWD
    */
	virtual void SetCWD(const char *cwd);
};
