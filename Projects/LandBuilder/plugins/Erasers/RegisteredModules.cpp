#include "StdAfx.h"
#include "ModuleRegistrator.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{
	using namespace LandBuildInt;
}

//-----------------------------------------------------------------------------

//source contains list of registered modules. 
/**Don't add new modules here. Instead, register module using the registrator 
at place of module's definition
*/
#include "DeleteAlongPath.h"
#include "DeleteInPolygon.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{
	RegisterModule< Modules::DeleteAlongPath > _register_E_001( "DeleteAlongPath" );
	RegisterModule< Modules::DeleteInPolygon > _register_E_002( "DeleteInPolygon" );
}

//-----------------------------------------------------------------------------
