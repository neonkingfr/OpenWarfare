//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\LandBuilder2\ObjectContainer.h"

#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"
#include "..\..\HighMapLoaders\include\EtMath.h"

#include ".\DeleteInPolygon.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

    DeleteInPolygon::DeleteInPolygon()
    : m_version ( "1.1.1" )
    , m_shapeCounter( 0 )
    {
    }

//-----------------------------------------------------------------------------

		void DeleteInPolygon::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();    

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "DeleteInPolygon " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";
      LOGF( Info ) << "None";

			m_deletionCounter = 0;

			// delete the objects
			DeleteObjects( cmdIfc );

			// writes the report
			WriteReport();
		}

//-----------------------------------------------------------------------------

		void DeleteInPolygon::DeleteObjects( IMainCommands* cmdIfc )
		{
			int objCount = 0;
			int objID = -1;
			while ( cmdIfc->GetObjectMap()->EnumObjects( objID ) )
			{
				objCount++;
				break;
			}

			if ( objCount > 0 )
			{
				int objID = -1;
				while ( cmdIfc->GetObjectMap()->EnumObjects( objID ) )
				{
					const Matrix4D& matr = cmdIfc->GetObjectMap()->GetObjectLocation( objID );
					const Vector3D objPos = matr.Position();

					Shapes::DVector tempV( objPos.X(), objPos.Z(), objPos.Y() );
					if ( m_currentShape->PtInside( tempV ) )
					{
						cmdIfc->GetObjectMap()->DeleteObject( objID );
						m_deletionCounter++;
					}
				}
			}
		}

//-----------------------------------------------------------------------------

		void DeleteInPolygon::WriteReport()
		{
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Deleted " << m_deletionCounter << " objects.     ";
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt

