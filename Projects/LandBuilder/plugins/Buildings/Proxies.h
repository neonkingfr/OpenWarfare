//-----------------------------------------------------------------------------
// File: Proxies.h
//
// Desc: classes related to proxies
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "TList.h"

//-----------------------------------------------------------------------------

class CProxy
{
	// The filename (fullpath) of the p3d file containing this proxy
	String m_srcFilename;

	// The folder (fullpath) of the destination file of this proxy
	String m_dstFolder;

	// Whether or not this proxy has already been exported to file
	mutable bool m_exported;

public:
	CProxy();
	CProxy( const String& filename, const String& dstFolder );
	CProxy( const CProxy& other );
	~CProxy();

	// Comparison
	// {
	bool operator == ( const CProxy& other ) const;
	bool operator != ( const CProxy& other ) const;
	// }

	// Returns the absolute destination filename of this proxy
	String GetDstAbsFilename() const;

	// Returns the destination filename of this proxy relative to the root folder
	String GetDstRelFilename() const;

	// Exports this proxy to file
	bool Export() const;
};

//-----------------------------------------------------------------------------

typedef TKeyList< CProxy > ProxiesList;

//-----------------------------------------------------------------------------

class CProxyHinstance
{
	// The proxy used by this proxy hinstance
	const CProxy* m_proxy;

	// The position of this proxy hinstance
	CGPoint3 m_position;

	// The unit vector oriented as the axes of this proxy hinstance
	CGVector3 m_axisX;
	CGVector3 m_axisY;
	CGVector3 m_axisZ;

public:
	CProxyHinstance();
	CProxyHinstance( const CProxy* proxy, const CGPoint3& position, 
                   const CGVector3& axisX, const CGVector3& axisY, const CGVector3& axisZ );
	CProxyHinstance( const CProxyHinstance& other );
	~CProxyHinstance();

	// Getters
	// {
	const CProxy* GetProxy() const;
	const CGPoint3& GetPosition() const;
	const CGVector3& GetAxisX() const;
	const CGVector3& GetAxisY() const;
	const CGVector3& GetAxisZ() const;
	// }

	// Proxy hinstance manipulators
	// {
	// Translates this proxy hinstance using the given displacements
	void Translate( Float dx, Float dy, Float dz );

	// Rotates CCW this proxy hinstance around the given axis passing through the given
	// center by the given angle in radians
	void Rotate( const CGVector3& axis, Float angle, const CGPoint3& center = CGPoint3() );
	// }
};

//-----------------------------------------------------------------------------

typedef TList< CProxyHinstance > ProxyHinstancesList;

//-----------------------------------------------------------------------------
