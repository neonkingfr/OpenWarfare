//-----------------------------------------------------------------------------
// File: StairTypes.h
//
// Desc: classes related to stair types 
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "TBuildingLists.h"
#include "Texture.h"

//-----------------------------------------------------------------------------

const Float DEFAULT_STAIR_WIDTH = 1.0;

//-----------------------------------------------------------------------------

const CColor DEFAULT_STAIR_RAMP_COLOR = CColor( 127, 127, 127 );
const CColor DEFAULT_STAIR_WALL_COLOR = CColor( 255, 255, 255 );

//-----------------------------------------------------------------------------

#define STAIR_NAME  "name"
#define STAIR_WIDTH "width"

//-----------------------------------------------------------------------------

#define STAIR_RAMP_COLOR "rampColor"
#define STAIR_WALL_COLOR "wallColor"

//-----------------------------------------------------------------------------

#define STAIR_RAMP_TEX "rampTexture"
#define STAIR_WALL_TEX "wallTexture"

//-----------------------------------------------------------------------------

class CStairType
{
	// The name of this stair type
	String m_name;

	// The width of this stair type
	Float m_width;

	// The textures used by this stair type
	TexturesList m_textures;

public:
	CStairType( const String& name, Float width );

	CStairType( const CStairType& other );

	// Getters
	// {
	const String& GetName() const;
	Float GetWidth() const;
	const CTexture* GetTexture( const String& key ) const;
	// }

	// Stair type manipulators
	// {
	void AddTexture( const String& key, const CTexture& texture );
	// }

	// Comparison
	// {
	bool operator == ( const CStairType& other ) const;
	bool operator != ( const CStairType& other ) const;
	// }
};

//-----------------------------------------------------------------------------

class CStairTypesLibrary : public TCfgList< CStairType >
{
	String m_modelsPath;

public:
	CStairTypesLibrary( const String& modelsPath = "" );
	virtual ~CStairTypesLibrary();

	void SetModelsPath( const String& modelsPath );

	virtual bool LoadFromCfgFile( const String& filename, ofstream& logFile );
};

//-----------------------------------------------------------------------------
