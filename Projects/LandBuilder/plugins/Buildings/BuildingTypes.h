//-----------------------------------------------------------------------------
// File: BuildingTypes.h
//
// Desc: classes related to building types 
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Color.h"
#include "TBuildingLists.h"
#include "Texture.h"
#include "Proxies.h"
#include "StairTypes.h"

//-----------------------------------------------------------------------------

const int DEFAULT_BUILDING_FLOORS_MAXNUM = 10;

//-----------------------------------------------------------------------------

const Float DEFAULT_BUILDING_FLOOR_HEIGHT      = 2.7;
const Float DEFAULT_BUILDING_INTERFLOOR_HEIGHT = 0.3;
const Float DEFAULT_BUILDING_ROOF_HEIGHT       = 0.5;
const Float DEFAULT_BUILDING_WALLS_WIDTH       = 0.5;
const Float DEFAULT_BUILDING_DOORS_WIDTH       = 1.0;
const Float DEFAULT_BUILDING_DOORS_HEIGHT      = 2.5;
const Float DEFAULT_BUILDING_WINDOWS_WIDTH     = 1.0;
const Float DEFAULT_BUILDING_WINDOWS_HEIGHT    = 1.5;

//-----------------------------------------------------------------------------

const CColor DEFAULT_BUILDING_FLOOR_EXWALL_COLOR    = CColor( 127, 127, 127 );
const CColor DEFAULT_BUILDING_FLOOR_INWALL_COLOR    = CColor( 255, 255, 255 );
const CColor DEFAULT_BUILDING_INTERFLOOR_WALL_COLOR = CColor( 127, 127, 127 );
const CColor DEFAULT_BUILDING_ROOF_TOP_COLOR        = CColor(  50,  50,  50 );
const CColor DEFAULT_BUILDING_ROOF_SLOPE_COLOR      = CColor( 127, 127, 127 );
const CColor DEFAULT_BUILDING_WINDOW_COLOR          = CColor( 127, 127, 127 );
const CColor DEFAULT_BUILDING_DOOR_COLOR            = CColor( 127, 127, 127 );
const CColor DEFAULT_BUILDING_CEILING_COLOR         = CColor( 255, 255, 255 );
const CColor DEFAULT_BUILDING_PAVEMENT_COLOR        = CColor( 127, 127, 127 );
const CColor DEFAULT_BUILDING_BASEMENT_COLOR        = CColor( 127, 127, 127 );

//-----------------------------------------------------------------------------

#define DEFAULT_BUILDING_TYPE_NAME "DefaultBuilding"

//-----------------------------------------------------------------------------

#define BUILDING_NAME              "name"
#define BUILDING_USESINTERFLOORS   "usesInterFloors"
#define BUILDING_USESWINDOWS       "usesWindows"
#define BUILDING_USESDOORS         "usesDoors"
#define BUILDING_USESSTAIRS        "usesStairs"
#define BUILDING_FLOOR_HEIGHT      "floorHeight"
#define BUILDING_INTERFLOOR_HEIGHT "interFloorHeight"
#define BUILDING_ROOF_HEIGHT       "roofHeight"
#define BUILDING_WALLS_WIDTH       "wallsWidth"
#define BUILDING_DOORS_WIDTH       "doorsWidth"
#define BUILDING_DOORS_HEIGHT      "doorsHeight"
#define BUILDING_WINDOWS_WIDTH     "windowsWidth"
#define BUILDING_WINDOWS_HEIGHT    "windowsHeight"
#define BUILDING_ROOF_TYPE         "roofType"
#define BUILDING_STAIR_TYPE        "stairType"
#define BUILDING_FLOORS_MAXNUM     "maxNumFloors"

//-----------------------------------------------------------------------------

#define BUILDING_FLOOR_EXWALL_COLOR    "floorExWallColor"
#define BUILDING_FLOOR_INWALL_COLOR    "floorInWallColor"
#define BUILDING_INTERFLOOR_WALL_COLOR "interFloorWallColor"
#define BUILDING_ROOF_TOP_COLOR        "roofTopColor"
#define BUILDING_ROOF_SLOPE_COLOR      "roofSlopeColor"
#define BUILDING_WINDOW_COLOR          "windowColor"
#define BUILDING_DOOR_COLOR            "doorColor"
#define BUILDING_CEILING_COLOR         "ceilingColor"
#define BUILDING_PAVEMENT_COLOR        "pavementColor"
#define BUILDING_BASEMENT_COLOR        "basementColor"

//-----------------------------------------------------------------------------

#define BUILDING_FLOOR_EXWALL_TEX    "floorExWallTexture"
#define BUILDING_FLOOR_INWALL_TEX    "floorInWallTexture"
#define BUILDING_INTERFLOOR_WALL_TEX "interFloorWallTexture"
#define BUILDING_ROOF_TOP_TEX        "roofTopTexture"
#define BUILDING_ROOF_SLOPE_TEX      "roofSlopeTexture"
#define BUILDING_WINDOW_TEX          "windowTexture"
#define BUILDING_DOOR_TEX            "doorTexture"
#define BUILDING_CEILING_TEX         "ceilingTexture"
#define BUILDING_PAVEMENT_TEX        "pavementTexture"
#define BUILDING_BASEMENT_TEX        "basementTexture"

//-----------------------------------------------------------------------------

#define BUILDING_WINDOW_PROXY "windowProxy"
#define BUILDING_DOOR_PROXY   "doorProxy"

//-----------------------------------------------------------------------------

class CBuildingType
{
	// The name of this building type
	String m_name;

	// Whether or not to place an inter-floor level between
	// two floors of this building type
	bool m_usesInterFloors;

	// Whether or not this buildings type uses windows
	bool m_usesWindows;

	// Whether or not this buildings type uses doors
	bool m_usesDoors;

	// Whether or not this buildings type uses stairs
	bool m_usesStairs;

	// The height of each floor of this building type
	Float m_floorHeight;

	// The height of each inter-floor of this building type
	Float m_interFloorHeight;

	// The height of the roof of this building type
	Float m_roofHeight;

	// The width of the walls of this building type
	Float m_wallsWidth;

	// The width of the doors of this building type
	Float m_doorsWidth;

	// The height of the doors of this building type
	Float m_doorsHeight;

	// The width of the windows of this building type
	Float m_windowsWidth;

	// The height of the windows of this building type
	Float m_windowsHeight;

	// The roof type used by this building type
	EBuildingRoofsType m_roofType;

	// A pointer to the stair type used by this building type
	const CStairType* m_pStairType;

	// The maximum number of floor created when using random
	// number of floor for this building type
	size_t m_maxNumFloors;

	// The textures used by this building type
	TexturesList m_textures;

	// The proxies used by this building type
	ProxiesList m_proxies;

public:
	CBuildingType( const String& name, bool usesInterFloors = true,
				   bool usesWindows = true, bool usesDoors = true,
				   bool usesStairs = true,
				   Float floorHeight = DEFAULT_BUILDING_FLOOR_HEIGHT,
				   Float interFloorHeight = DEFAULT_BUILDING_INTERFLOOR_HEIGHT,
				   Float roofHeight = DEFAULT_BUILDING_ROOF_HEIGHT,
				   Float wallsWidth = DEFAULT_BUILDING_WALLS_WIDTH,
				   Float doorsWidth = DEFAULT_BUILDING_DOORS_WIDTH,
				   Float doorsHeight = DEFAULT_BUILDING_DOORS_HEIGHT,
				   Float windowsWidth = DEFAULT_BUILDING_WINDOWS_WIDTH,
				   Float windowsHeight = DEFAULT_BUILDING_WINDOWS_HEIGHT,
				   EBuildingRoofsType roofType = BRT_FLAT,
				   const CStairType* pStairType = NULL,
				   size_t maxNumFloor = DEFAULT_BUILDING_FLOORS_MAXNUM );

	CBuildingType( const CBuildingType& other );

	// Getters
	// {
	const String& GetName() const;
	bool UsesInterFloors() const;
	bool UsesWindows() const;
	bool UsesDoors() const;
	bool UsesStairs() const;
	Float GetFloorHeight() const;
	Float GetInterFloorHeight() const;
	Float GetRoofHeight() const;
	Float GetWallsWidth() const;
	Float GetDoorsWidth() const;
	Float GetDoorsHeight() const;
	Float GetWindowsWidth() const;
	Float GetWindowsHeight() const;
	const EBuildingRoofsType& GetRoofType() const;
	const CStairType* GetStairType() const;
	size_t GetMaxNumFloors() const;
	const CTexture* GetTexture( const String& key ) const;
	const CProxy* GetProxy( const String& key ) const;
	// }

	// Building type manipulators
	// {
	void AddTexture( const String& key, const CTexture& texture );
	void AddProxy( const String& key, const CProxy& proxy );
	// }

	// Comparison
	// {
	bool operator == ( const CBuildingType& other ) const;
	bool operator != ( const CBuildingType& other ) const;
	// }
};

//-----------------------------------------------------------------------------

class CBuildingTypesLibrary : public TCfgList< CBuildingType >
{
	String m_modelsPath;
	const CStairTypesLibrary* m_pStairTypesLibrary;

public:
	CBuildingTypesLibrary( const String& modelsPath = "",
						   const CStairTypesLibrary* pStairTypesLibrary = NULL );
	virtual ~CBuildingTypesLibrary();

	void SetModelsPath( const String& modelsPath );
	void SetStairTypeLibrary( const CStairTypesLibrary* pStairTypesLibrary );

	virtual bool LoadFromCfgFile( const String& filename, ofstream& logFile );

private:
	void SetDefault( const String& texturesPath );
};

//-----------------------------------------------------------------------------
