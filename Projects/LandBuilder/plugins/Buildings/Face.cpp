//-----------------------------------------------------------------------------
// File: Face.cpp
//
// Desc: class for geometry faces
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Face.h"

//-----------------------------------------------------------------------------

CFace::CFace()
: m_texture( NULL )
, m_isRoadWay( false )
, m_visible( true )
, m_shadow( true )
{
}

//-----------------------------------------------------------------------------

CFace::CFace( size_t index1, size_t index2, size_t index3, 
              const CGPoint2& uv1, const CGPoint2& uv2, const CGPoint2& uv3,
              const CTexture* texture, bool isRoadWay, bool visible, bool shadow )
: m_isRoadWay( isRoadWay )
, m_visible( visible )
, m_shadow( shadow )
, m_texture( texture )
{
	m_indices[0] = index1;
	m_indices[1] = index2;
	m_indices[2] = index3;
	m_uvs[0]     = uv1;
	m_uvs[1]     = uv2;
	m_uvs[2]     = uv3;
}

//-----------------------------------------------------------------------------

CFace::CFace( const CFace& other )
: m_isRoadWay( other.m_isRoadWay )
, m_visible( other.m_visible )
, m_shadow( other.m_shadow )
, m_texture( other.m_texture )
{
	m_indices[0] = other.m_indices[0];
	m_indices[1] = other.m_indices[1];
	m_indices[2] = other.m_indices[2];
	m_uvs[0]     = other.m_uvs[0];
	m_uvs[1]     = other.m_uvs[1];
	m_uvs[2]     = other.m_uvs[2];
}

//-----------------------------------------------------------------------------

CFace::~CFace()
{
}

//-----------------------------------------------------------------------------

CFace& CFace::operator = ( const CFace& other )
{
	m_indices[0] = other.m_indices[0];
	m_indices[1] = other.m_indices[1];
	m_indices[2] = other.m_indices[2];
	m_uvs[0]     = other.m_uvs[0];
	m_uvs[1]     = other.m_uvs[1];
	m_uvs[2]     = other.m_uvs[2];
	m_isRoadWay  = other.m_isRoadWay;
	m_visible    = other.m_visible;
	m_shadow     = other.m_shadow;
	m_texture    = other.m_texture;
	return (*this);
}

//-----------------------------------------------------------------------------

size_t CFace::GetIndex( size_t vertex ) const
{
	assert( vertex < 3 );
	return (m_indices[vertex]);
}

//-----------------------------------------------------------------------------

const CGPoint2& CFace::GetUV( size_t vertex ) const
{
	assert( vertex < 3 );
	return (m_uvs[vertex]);
}

//-----------------------------------------------------------------------------

bool CFace::IsRoadWay() const
{
	return (m_isRoadWay);
}

//-----------------------------------------------------------------------------

bool CFace::IsVisible() const
{
	return (m_visible);
}

//-----------------------------------------------------------------------------

bool CFace::IsShadow() const
{
	return (m_shadow);
}

//-----------------------------------------------------------------------------

const CTexture* CFace::GetTexture() const
{
	return (m_texture);
}

//-----------------------------------------------------------------------------

void CFace::SetIndex( size_t vertex, size_t index )
{
	assert( vertex < 3 );
	m_indices[vertex] = index;
}

//-----------------------------------------------------------------------------

void CFace::SetUV( size_t vertex, const CGPoint2& uv )
{
	assert( vertex < 3 );
	m_uvs[vertex] = uv;
}

//-----------------------------------------------------------------------------

void CFace::SetRoadWay( bool isRoadWay )
{
	m_isRoadWay = isRoadWay;
}

//-----------------------------------------------------------------------------

void CFace::SetVisible( bool visible )
{
	m_visible = visible;
}

//-----------------------------------------------------------------------------

void CFace::SetShadow( bool shadow )
{
	m_shadow = shadow;
}

//-----------------------------------------------------------------------------

void CFace::SetTexture( const CTexture* texture )
{
	m_texture = texture;
}

//-----------------------------------------------------------------------------

bool CFace::Matches( const CFace& other ) const
{
	bool test1 = false;
	bool test2 = false;
	bool test3 = false;

	// test1
	if ( (m_indices[0] == other.m_indices[0]) ||
		   (m_indices[0] == other.m_indices[1]) ||
		   (m_indices[0] == other.m_indices[2]) )
	{
		test1 = true;
	}

	// test2
	if ( (m_indices[1] == other.m_indices[0]) ||
		   (m_indices[1] == other.m_indices[1]) ||
		   (m_indices[1] == other.m_indices[2]) )
	{
		test2 = true;
	}

	// test3
	if ( (m_indices[2] == other.m_indices[0]) ||
		   (m_indices[2] == other.m_indices[1]) ||
		   (m_indices[2] == other.m_indices[2]) )
	{
		test3 = true;
	}

	return ( test1 && test2 && test3 ) ? true : false;
}

//-----------------------------------------------------------------------------

void CFace::ReverseIndicesOrder()
{
	size_t tempIndex = m_indices[2];
	m_indices[2] = m_indices[1];
	m_indices[1] = tempIndex;

	CGPoint2 tempUV = m_uvs[2];
	m_uvs[2] = m_uvs[1];
	m_uvs[1] = tempUV;
}

//-----------------------------------------------------------------------------
