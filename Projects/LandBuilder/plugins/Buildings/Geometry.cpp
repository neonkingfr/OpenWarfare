//-----------------------------------------------------------------------------
// File: Geometry.cpp
//
// Desc: class for templates geometry
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Geometry.h"

//-----------------------------------------------------------------------------

CGeometry::CGeometry()
{
}

//-----------------------------------------------------------------------------

CGeometry::CGeometry( const CGeometry& other )
: m_vertices( other.m_vertices )
, m_faces( other.m_faces )
{
}

//-----------------------------------------------------------------------------

CGeometry::~CGeometry()
{
}

//-----------------------------------------------------------------------------

CGeometry& CGeometry::operator = ( const CGeometry& other )
{
	m_vertices = other.m_vertices;
	m_faces    = other.m_faces;
	return (*this);
}

//-----------------------------------------------------------------------------

const Point3List& CGeometry::GetVertices() const
{
	return (m_vertices);
}

//-----------------------------------------------------------------------------

const FacesList& CGeometry::GetFaces() const
{
	return (m_faces);
}

//-----------------------------------------------------------------------------

void CGeometry::SetFaces( const FacesList& faces )
{
	m_faces = faces;
}

//-----------------------------------------------------------------------------

void CGeometry::Clear()
{
	m_vertices.Clear();
	m_faces.Clear();
}

//-----------------------------------------------------------------------------

size_t CGeometry::AddVertex( const CGPoint3& vertex, Float tolerance )
{
	int index = FindVertexIndex( vertex, tolerance );
	if ( index == -1 )
	{
		m_vertices.Append( vertex );
		index = m_vertices.Size() - 1;
	}

	return (index);
}

//-----------------------------------------------------------------------------

size_t CGeometry::AddFace( const CFace& face )
{
	m_faces.Append( face );
	return (m_faces.Size() - 1);
}

//-----------------------------------------------------------------------------

void CGeometry::RemoveFace( size_t index )
{
	assert( index < m_faces.Size() );
	m_faces.RemoveAt( index );
}

//-----------------------------------------------------------------------------

void CGeometry::MergeWith( const CGeometry& other, bool removeDuplicatedFaces, Float tolerance )
{
	// first adds to this geometry the vertices of the other geometry
	// which are not already listed and prepares for them the map of the indices
	map< size_t, size_t > indicesMap;
	for ( size_t i = 0, cntI = other.VerticesCount(); i < cntI; ++i )
	{
		const CGPoint3& point = other.Vertex( i );
		int index = FindVertexIndex( point, tolerance );
		if ( index == -1 )
		{
			AddVertex( point, tolerance );
			index = VerticesCount() - 1;
		}
		indicesMap.insert( pair< size_t, size_t >( i, static_cast< size_t >( index ) ) );
	}

	// then uses the map of vertex indices to update the faces
	// and add the faces of the level to the geometry
	for ( size_t i = 0, cntI = other.FacesCount(); i < cntI; ++i )
	{
		CFace face = other.Face( i );
		for ( size_t j = 0; j < 3; ++j )
		{
			face.SetIndex( j, indicesMap[face.GetIndex( j )] );
		}
		AddFace( face );
	}

	if ( removeDuplicatedFaces )
	{
		size_t i = 0;
		while ( i < FacesCount() - 1 )
		{
			size_t j = i + 1;
			while ( j < FacesCount() )
			{
				if ( Face( i ).Matches( Face( j ) ) )
				{
					RemoveFace( j );
					RemoveFace( i );
					--i;
					break;
				}
				++j;
			}
			++i;
		}
	}
}

//-----------------------------------------------------------------------------

void CGeometry::Translate( Float dx, Float dy, Float dz )
{
	for ( size_t i = 0, cntI = m_vertices.Size(); i < cntI; ++i )
	{
		m_vertices.Get( i )->Translate( dx, dy, dz );
	}
}

//-----------------------------------------------------------------------------

void CGeometry::Rotate( const CGVector3& axis, Float angle, const CGPoint3& center )
{
	for ( size_t i = 0, cntI = m_vertices.Size(); i < cntI; ++i )
	{
		m_vertices.Get( i )->Rotate( axis, angle, center );
	}
}

//-----------------------------------------------------------------------------

size_t CGeometry::VerticesCount() const
{
	return (m_vertices.Size());
}

//-----------------------------------------------------------------------------

size_t CGeometry::FacesCount() const
{
	return (m_faces.Size());
}

//-----------------------------------------------------------------------------

const CGPoint3& CGeometry::Vertex( size_t index ) const
{
	assert( index < m_vertices.Size() );
	return (*m_vertices.Get( index ));
}

//-----------------------------------------------------------------------------

CGPoint3& CGeometry::Vertex( size_t index )
{
	assert( index < m_vertices.Size() );
	return (*m_vertices.Get( index ));
}

//-----------------------------------------------------------------------------

const CFace& CGeometry::Face( size_t index ) const
{
	assert( index < m_faces.Size() );
	return (*m_faces.Get( index ));
}

//-----------------------------------------------------------------------------

CFace& CGeometry::Face( size_t index )
{
	assert( index < m_faces.Size() );
	return (*m_faces.Get( index ));
}

//-----------------------------------------------------------------------------

int CGeometry::FindVertexIndex( const CGPoint3& point, Float tolerance ) const
{
	for ( size_t i = 0, cntI = m_vertices.Size(); i < cntI; ++i )
	{
		if ( m_vertices.Get( i )->Equals( point, tolerance ) ) { return (i); }
	}
	return (-1);
}

//-----------------------------------------------------------------------------

int CGeometry::FindVertexIndex( const CGPoint2& point, size_t startIndex, 
                                Float tolerance ) const
{
	for ( size_t i = startIndex, cntI = m_vertices.Size(); i < cntI; ++i )
	{
		const CGPoint3& point3 = *m_vertices.Get( i );
		CGPoint2 point2( point3.GetX(), point3.GetY() );
		if ( point2.Equals( point, tolerance ) ) { return (i); }
	}
	return (-1);
}

//-----------------------------------------------------------------------------
