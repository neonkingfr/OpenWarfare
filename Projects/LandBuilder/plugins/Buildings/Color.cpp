//-----------------------------------------------------------------------------
// File: Color.cpp
//
// Desc: an RGB color class
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Color.h"

//-----------------------------------------------------------------------------

CColor::CColor()
: m_red( 127 )
, m_green( 127 )
, m_blue( 127 )
{
}

// ------------------------------------------------------------------------------ //

CColor::CColor( size_t red, size_t green, size_t blue )
: m_red( red )
, m_green( green )
, m_blue( blue )
{
}

// ------------------------------------------------------------------------------ //

CColor::CColor( const CColor& other )
: m_red( other.m_red )
, m_green( other.m_green )
, m_blue( other.m_blue )
{
}

// ------------------------------------------------------------------------------ //

size_t CColor::GetRed() const
{
	return (m_red);
}

// ------------------------------------------------------------------------------ //

size_t CColor::GetGreen() const
{
	return (m_green);
}

// ------------------------------------------------------------------------------ //

size_t CColor::GetBlue() const
{
	return (m_blue);
}

// ------------------------------------------------------------------------------ //

void CColor::SetRed( size_t red )
{
	m_red = red;
}

// ------------------------------------------------------------------------------ //

void CColor::SetGreen( size_t green )
{
	m_green = green;
}

// ------------------------------------------------------------------------------ //

void CColor::SetBlue( size_t blue )
{
	m_blue = blue;
}

// ------------------------------------------------------------------------------ //

bool CColor::operator == ( const CColor& other ) const
{
	if ( this == &other ) { return (true); }

	if ( m_red   != other.m_red )   { return (false); }
	if ( m_green != other.m_green ) { return (false); }
	if ( m_blue  != other.m_blue )  { return (false); }
	return (true);
}

// ------------------------------------------------------------------------------ //

bool CColor::operator != ( const CColor& other ) const
{
	return (!(this->operator==( other )));
}

// ------------------------------------------------------------------------------ //

String CColor::ToString() const
{
	char buffer[80];
	sprintf_s( buffer, "[%d, %d, %d]", m_red, m_green, m_blue );
	return (String( buffer ));
}

// ------------------------------------------------------------------------------ //

String CColor::ToTexture() const
{
	char buffer[80];
	sprintf_s( buffer, "#(argb,8,8,3)color(%.2f,%.2f,%.2f,1.0,CO)", m_red / 255.0f, m_green / 255.0f, m_blue / 255.0f );
	return (String( buffer ));
}

// ------------------------------------------------------------------------------ //
