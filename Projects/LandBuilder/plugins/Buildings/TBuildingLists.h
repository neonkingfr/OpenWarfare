//-----------------------------------------------------------------------------
// File: TBuildingLists.h
//
// Desc: templated lists for buildings
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "TList.h"
#include "Color.h"

//-----------------------------------------------------------------------------

template < class T >
class TCfgList : public TList< T >
{
public:
	TCfgList();
	virtual ~TCfgList();

	virtual bool LoadFromCfgFile( const String& filename, ofstream& logFile ) = 0;

	// searches the item with the given name and returns its index if found
	// or -1 if not found
	int FindByName( const String& name ) const;

protected:
	CColor LoadColorFromCfgFile( const String& buff, ParamClassPtr& item, 
								 const CColor& defaultColor, 
								 const String& strColor, ofstream& logFile );

	String LoadTextureFromCfgFile( const String& buff, ParamClassPtr& item, 
								   const String& strTex, const CColor& color, 
								   ofstream& logFile );

	bool LoadBoolFromCfgFile( const String& buff, ParamClassPtr& item, 
							  const String& strBool, bool defaultValue, 
							  ofstream& logFile );

	Float LoadDoubleFromCfgFile( const String& buff, ParamClassPtr& item,
								 const String& strFloat, Float defaultValue,
								 Float minValidValue, Float maxValidValue,
								 ofstream& logFile );

	String LoadProxyNameFromCfgFile( const String& buff, ParamClassPtr& item, 
									 const String& strProxy, ofstream& logFile );
};

// -------------------------------------------------------------------------- //

#include "TBuildingLists.inl"

//-----------------------------------------------------------------------------
