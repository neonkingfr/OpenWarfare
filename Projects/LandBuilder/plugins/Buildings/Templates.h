//-----------------------------------------------------------------------------
// File: Templates.h
//
// Desc: classes related to building templates
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "BuildingTypes.h"
#include "BuildingLevels.h"
#include "BuildingComponents.h"

//-----------------------------------------------------------------------------

typedef enum
{
	TT_Building
} ETemplateType;

//-----------------------------------------------------------------------------

static const size_t BUILDING_FLOORS_ENTERABLE_MAXNUM = 2;

//-----------------------------------------------------------------------------

class CTemplate
{
	// Used to automatically calculate IDs
	static size_t s_counter;

protected:
	// The id of this template
	size_t m_id;

	// Counter of how many hinstances (models) uses this template.
	int m_referencesCount;

	// logger to file
	ofstream* m_logFile;

public:
	CTemplate( ofstream* logFile );
	CTemplate( const CTemplate& other );
	virtual ~CTemplate();

	// Reference count manipulators
	// {
	void IncreaseReferencesCount();
	void DecreaseReferencesCount();
	void ResetReferencesCount();
	// }

	// Getters
	// {
	int GetReferencesCount() const;
	// }

	// Returns the filename of this template (extension = p3d)
	String Filename() const;

	// Returns the type of this template
	virtual ETemplateType Type() const = 0;

	// Returns the name of this template
	virtual String Name() const = 0;

private:
	// Assignement
	CTemplate& operator = ( const CTemplate& other );
};

//-----------------------------------------------------------------------------

class CProceduralTemplate : public CTemplate
{
public:
	CProceduralTemplate( ofstream* logFile );
	CProceduralTemplate( const CProceduralTemplate& other );
	virtual ~CProceduralTemplate();

	// Exports this template into the given folder (must contain fullpath)
	// and returns true if successfull
	virtual bool ExportToP3D( const String& modelsPath ) = 0;

protected:
	// Exports the texture used by this procedural template
	virtual bool ExportTextures() = 0;

	// Exports the proxies used by this procedural template
	virtual bool ExportProxies() = 0;

	// Builds the geometry of this procedural template
	virtual bool BuildGeometry() = 0;

	// Generate the LODs of this procedural template
	virtual void GenerateLOD( LODObject& lodObj, float resolution ) = 0;
	virtual void GenerateComponentsLOD( LODObject& lodObj, float resolution ) = 0;
	virtual void GenerateRoadWayLOD( LODObject& lodObj ) = 0;
	virtual void GenerateShadowLOD( LODObject& lodObj ) = 0;
};

//-----------------------------------------------------------------------------

class CBuildingTemplate : public CProceduralTemplate
{
	// A pointer to the building type of this building template
	const CBuildingType* m_buildingType;

	// The footprint of this building template
	CGPolygon2 m_footprint;

	// The number of floors of this building template
	size_t m_numFloors;

	// Whether or not this building template can be entered
	bool m_enterable;

	// Whether or not to rectify the corners of this building template
	bool m_rectifyCorners;

	// The factor of similarity to apply when comparing two templates' footprints
	Float m_similarityFactor;

	// The list of levels composing this building template
	CBuildingLevelsList m_levels;

	// The list of components composing this building template
	BuildingComponentList m_components;

	// the prefix to add into the filename of the exported models
	String m_namePrefix;

  // Whether or not to use shadow volume LOD
  bool m_shadowVolume;

public:
	CBuildingTemplate( const CBuildingType* buildingType, const CGPolygon2& footprint,
                     int numFloors, bool enterable, bool rectifyCorners,
                     Float similarityFactor, const String& namePrefix,
                     bool shadowVolume, ofstream* logFile );
	CBuildingTemplate( const CBuildingTemplate& other );
	virtual ~CBuildingTemplate();

	// Getters
	// {
	const CBuildingType* GetBuildingType() const;
	const CGPolygon2& GetFootprint() const;
	size_t GetNumFloors() const;
	bool IsEnterable() const;
	bool GetRectifyCorners() const;
  bool UsesShadowVolume() const;
	// }

	// Comparison
	// {
	bool Equals( const CBuildingTemplate& other );
	// }

	// CTemplate implementation
	// {
	virtual ETemplateType Type() const;
	virtual String Name() const;
	// }

	// CProceduralTemplate implementation
	// {
	virtual bool ExportToP3D( const String& modelsPath );

protected:
	virtual bool ExportTextures();
	virtual bool ExportProxies();
	virtual bool BuildGeometry();
	virtual void GenerateLOD( LODObject& lodObj, float resolution );
	virtual void GenerateComponentsLOD( LODObject& lodObj, float resolution );
	virtual void GenerateRoadWayLOD( LODObject& lodObj );
	virtual void GenerateShadowLOD( LODObject& lodObj );
	// }

private:
  void PreprocessFootprint();
	void Init();
};

//-----------------------------------------------------------------------------

class CBuildingTemplatesLibrary : public TList< CBuildingTemplate >
{
public:
	CBuildingTemplatesLibrary();
	virtual ~CBuildingTemplatesLibrary();
};

// -------------------------------------------------------------------------- //
