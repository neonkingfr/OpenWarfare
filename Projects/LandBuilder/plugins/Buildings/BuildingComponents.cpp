//-----------------------------------------------------------------------------
// File: BuildingComponents.cpp
//
// Desc: classes related to building components
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "BuildingComponents.h"

//-----------------------------------------------------------------------------

CBuildingComponent::CBuildingComponent( CBuildingLevel* level,
                                        const CGPoint3& position, Float orientation )
: m_level( level )
, m_position( position )
, m_orientation( orientation )
{
}

//-----------------------------------------------------------------------------

const CBuildingLevel* CBuildingComponent::GetLevel() const
{
	return (m_level);
}

//-----------------------------------------------------------------------------

const GeometriesList& CBuildingComponent::GetGeometries() const
{
	return (m_geometries);
}

//-----------------------------------------------------------------------------

const ProxyHinstancesList& CBuildingComponent::GetProxies() const
{
	return (m_proxies);
}

//-----------------------------------------------------------------------------

bool CBuildingComponent::TransformGeometries()
{
	m_geometries = m_level->GetGeometries();
	m_proxies    = m_level->GetProxies();

	Translate( m_position.GetX(), m_position.GetY(), m_position.GetZ() );
	Rotate( CGVector3( CGConsts::ZERO, CGConsts::ZERO, CGConsts::ONE ), m_orientation, m_position );

	return (true);
}

//-----------------------------------------------------------------------------

void CBuildingComponent::Translate( Float dx, Float dy, Float dz )
{
	for ( size_t i = 0, cntI = m_geometries.Size(); i < cntI; ++i )
	{
		m_geometries.Get( i )->Translate( dx, dy, dz );
	}
	for ( size_t i = 0, cntI = m_proxies.Size(); i < cntI; ++i )
	{
		m_proxies.Get( i )->Translate( dx, dy, dz );
	}
}

//-----------------------------------------------------------------------------

void CBuildingComponent::Rotate( const CGVector3& axis, Float angle, const CGPoint3& center )
{
	if ( angle == CGConsts::ZERO ) { return; }

	for ( size_t i = 0, cntI = m_geometries.Size(); i < cntI; ++i )
	{
		m_geometries.Get( i )->Rotate( axis, angle, center );
	}
	for ( size_t i = 0, cntI = m_proxies.Size(); i < cntI; ++i )
	{
		m_proxies.Get( i )->Rotate( axis, angle, center );
	}
}

//-----------------------------------------------------------------------------
