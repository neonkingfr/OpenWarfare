//-----------------------------------------------------------------------------
// File: Face.h
//
// Desc: class for geometry faces
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "Texture.h"

//-----------------------------------------------------------------------------

class CFace
{
	// The indices of the vertices of this face.
	size_t m_indices[3];

	// The uv coordinates of the vertices of this face.
	CGPoint2 m_uvs[3];

	// Whether or not this face is part of the road way LOD
	bool m_isRoadWay;

	// Whether or not this face is part of the 1.000 LOD
	bool m_visible;

	// Whether or not this face is part of the shadow LOD
	bool m_shadow;

	// A pointer to the texture used by this face.
	const CTexture* m_texture;

public:
	CFace();
	CFace( size_t index1, size_t index2, size_t index3,
         const CGPoint2& uv1, const CGPoint2& uv2, const CGPoint2& uv3,
         const CTexture* texture = NULL, bool isRoadWay = false, 
         bool visible = true, bool shadow = true );
	CFace( const CFace& other );
	~CFace();

	// assignement
	CFace& operator = ( const CFace& other );

	// Getters
	// {
	size_t GetIndex( size_t vertex ) const;
	const CGPoint2& GetUV( size_t vertex ) const;
	bool IsRoadWay() const;
	bool IsVisible() const;
	bool IsShadow() const;
	const CTexture* GetTexture() const;
	// }

	// Setters
	// {
	// Sets the index of the given vertex with the given value 
	void SetIndex( size_t vertex, size_t index );

	// Sets the uv of the given vertex with the given value 
	void SetUV( size_t vertex, const CGPoint2& uv );

	// Sets the roadWay field of this face
	void SetRoadWay( bool isRoadWay );

	// Sets the visible field of this face
	void SetVisible( bool visible );

	// Sets the shadow field of this face
	void SetShadow( bool shadow );

	// Sets the pointer to the texture with the given value 
	void SetTexture( const CTexture* texture );
	// }

	// Comparison
	// {
	// Returns true if this face matches the given one 
	// ( both have the same indices no matter in which order )
	bool Matches( const CFace& other ) const;
	// }

	// Manipulators
	// {
	// Reverse the order of the indices of this face
	void ReverseIndicesOrder();
	// }
};

//-----------------------------------------------------------------------------
