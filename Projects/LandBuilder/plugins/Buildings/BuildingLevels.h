//-----------------------------------------------------------------------------
// File: BuildingLevels.h
//
// Desc: classes related to building levels
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "TList.h"
#include "BuildingTypes.h"
#include "GeometriesBuilder.h"
#include "Proxies.h"
#include "Stair.h"

//-----------------------------------------------------------------------------

#define DEFAULT_ROOF_WALLS_HEIGHT 1.0
#define DEFAULT_ROOF_SLOPE_INSET  1.0
#define DEFAULT_ROOF_OFFSET       1.0

//-----------------------------------------------------------------------------

class CBuildingLevel
{
protected:
  // A pointer to the building type of this building level
  const CBuildingType* m_buildingType;

  // The type of this building level
  EBuildingLevelType m_type;

  // The footprint to apply to this building level
  CGPolygon2* m_footprint;

  // The height (used in extrusion of the footprint) of this building level
  Float m_height;

  // The reference length used to scale textures
  Float m_texReferenceLength;

  // The geometries of this building level
  GeometriesList m_geometries;

  // The proxies of this building level
  ProxyHinstancesList m_proxies;

  // Whether or not the bottom faces of this building level are part of the 1.000 LOD
  bool m_bottomFacesVisible;

  // Whether or not the top faces of this building level are part of the 1.000 LOD
  bool m_topFacesVisible;

  // True if the level was an enterable one downgraded to solid
  bool m_solidified;

  // logger to file
  ofstream* m_logFile;

public:
  CBuildingLevel( const CBuildingType* buildingType, const EBuildingLevelType& type,
                  const CGPolygon2* footprint, Float height, Float texReferenceLength,
                  bool bottomFacesVisible, bool topFacesVisible, ofstream* logFile );
  CBuildingLevel( const CBuildingLevel& other );

  virtual ~CBuildingLevel();

  // Assignement
  CBuildingLevel& operator = ( const CBuildingLevel& other );

  // Getters
  // {
  const CBuildingType* GetBuildingType() const;
  const EBuildingLevelType& GetType() const;
  const CGPolygon2& GetFootprint() const;
  Float GetHeight() const;
  bool IsGroundLevel() const;
  Float GetTexReferenceLength() const;
  const GeometriesList& GetGeometries() const;
  const ProxyHinstancesList& GetProxies() const;
  bool IsSolidified() const;
  // }

  // Geometry manipulators
  // {
  virtual bool BuildGeometry() = 0;
  // }

protected:
  bool BuildSolidGeometry();
  bool BuildEnterableGeometry();
};

//-----------------------------------------------------------------------------

class CBuildingFloor : public CBuildingLevel
{
  EBuildingFloorsType m_floorType;
  CStair* m_stair;

public:
  CBuildingFloor( const CBuildingType* buildingType,
                  const EBuildingFloorsType& floorType,
                  const CGPolygon2* footprint, 
                  Float height, Float texReferenceLength, 
                  bool bottomFacesVisible, 
                  bool topFacesVisible,
                  ofstream* logFile,
                  const CStair* stair );
  CBuildingFloor( const CBuildingFloor& other );

  virtual ~CBuildingFloor();

  // Getters
  // {
  const EBuildingFloorsType& GetFloorType() const;
  const CStair* GetStair() const;
  // }

  // Assignement
  CBuildingFloor& operator = ( const CBuildingFloor& other );

  // CBuildingLevel implementation
  // {
  virtual bool BuildGeometry();
  // }

private:
  size_t AddWindowsAndDoorsSections( Float margin = CGConsts::ZERO );
  size_t AddDoorsSectionsOnly( Float margin = CGConsts::ZERO );
};

//-----------------------------------------------------------------------------

class CBuildingInterFloor : public CBuildingLevel
{
  EBuildingInterFloorsType m_interFloorType;

public:
  CBuildingInterFloor( const CBuildingType* buildingType, const CGPolygon2* footprint,
                       Float height, Float texReferenceLength,
                       const EBuildingInterFloorsType& interFloorType,
                       bool bottomFacesVisible, bool topFacesVisible,
                       ofstream* logFile );
  CBuildingInterFloor( const CBuildingInterFloor& other );

  virtual ~CBuildingInterFloor();

  // Getters
  // {
  const EBuildingInterFloorsType& GetInterFloorType() const;
  // }

  // Assignement
  CBuildingInterFloor& operator = ( const CBuildingInterFloor& other );

  // CBuildingLevel implementation
  // {
  virtual bool BuildGeometry();
  // }
};

//-----------------------------------------------------------------------------

class CBuildingRoof : public CBuildingLevel
{
  EBuildingRoofsType m_roofType;

  // True if this roof level was downgraded to flat
  bool m_flatted;

public:
  CBuildingRoof( const CBuildingType* buildingType,
                 const CGPolygon2* footprint,
                 Float height, Float texReferenceLength, 
                 bool bottomFacesVisible,
                 ofstream* logFile );
  CBuildingRoof( const CBuildingRoof& other );

  virtual ~CBuildingRoof();

  // Getters
  // {
  const EBuildingRoofsType& GetRoofType() const;
  bool IsFlatted() const;
  // }

  // Assignement
  CBuildingRoof& operator = ( const CBuildingRoof& other );

  // CBuildingLevel implementation
  // {
  virtual bool BuildGeometry();
  // }

private:
  bool BuildFlat();
  bool BuildFlatWalled();
  bool BuildSloped();
  bool BuildHip( Float offset );
};

//-----------------------------------------------------------------------------

// unfortunately we cannot derive the following class from TList< CBuildingLevel >
// because CBuildingLevel is abstract
class CBuildingLevelsList
{
  vector< CBuildingLevel* > m_levels;

public:
  CBuildingLevelsList();
  virtual ~CBuildingLevelsList();

  size_t Size() const;

  const CBuildingLevel* Get( size_t index ) const;
  CBuildingLevel* Get( size_t index );

  void Clear();

  CBuildingLevel* AddFloor( const CBuildingFloor& floor );
  CBuildingLevel* AddInterFloor( const CBuildingInterFloor& interFloor );
  CBuildingLevel* AddRoof( const CBuildingRoof& roof );
  CBuildingLevel* AddLevel( const CBuildingLevel& level );

private:
  // we don't want to use the Append method to add levels
  CBuildingLevel* Append( const CBuildingLevel& level );
};

//-----------------------------------------------------------------------------
