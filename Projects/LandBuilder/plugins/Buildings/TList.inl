//-----------------------------------------------------------------------------
// File: TList.inl
//
// Desc: a templated list
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < class T >
TList< T >::TList()
{
}

//-----------------------------------------------------------------------------

template < class T >
TList< T >::TList( const TList< T >& other )
{
	CopyFrom( other );
}

//-----------------------------------------------------------------------------

template < class T >
TList< T >::~TList()
{
	Clear();
}

//-----------------------------------------------------------------------------

template < class T >
TList< T >& TList< T >::operator = ( const TList< T >& other )
{
	CopyFrom( other );
	return (*this);
}

//-----------------------------------------------------------------------------

template < class T >
void TList< T >::Clear()
{
	for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
	{
		delete m_elements[i];
	}
	m_elements.clear();
}

//-----------------------------------------------------------------------------

template < class T >
T* TList< T >::Append( const T& element )
{
	T* pT = new T( element );
	m_elements.push_back( pT );
	return (m_elements[m_elements.size() - 1]);
}

//-----------------------------------------------------------------------------

template < class T >
size_t TList< T >::Size() const
{
	return (m_elements.size());
}

//-----------------------------------------------------------------------------

template < class T >
void TList< T >::Remove( const T* pElement )
{
	if ( pElement )
	{
		for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
		{
			if ( m_elements[i] == pElement )
			{
				RemoveAt( i );
				return;
			}
		}
	}
}

//-----------------------------------------------------------------------------

template < class T >
void TList< T >::RemoveAt( size_t index )
{
	assert( index < m_elements.size() );
	delete m_elements[index];
	m_elements.erase( m_elements.begin() + index );
}

//-----------------------------------------------------------------------------

template < class T >
T* TList< T >::Find( const T& t ) const
{
	for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
	{
		if ( m_elements[i]->Equals( t ) ) { return (m_elements[i]); }
	}

	return (NULL);
}

//-----------------------------------------------------------------------------

template < class T >
int TList< T >::FindIndex( const T& t ) const
{
	for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
	{
		if ( m_elements[i]->Equals( t ) ) { return (i); }
	}

	return (-1);
}

//-----------------------------------------------------------------------------

template < class T >
const T* TList< T >::Get( size_t index ) const
{
	assert( index < m_elements.size() );
	return (m_elements[index]);
}

//-----------------------------------------------------------------------------

template < class T >
T* TList< T >::Get( size_t index )
{
	assert( index < m_elements.size() );
	return (m_elements[index]);
}

//-----------------------------------------------------------------------------

template < class T >
void TList< T >::CopyFrom( const TList< T >& other )
{
	Clear();
	for ( size_t i = 0, cntI = other.Size(); i < cntI; ++i )
	{
		Append( *other.Get( i ) );
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template < class T >
TKeyList< T >::TKeyList()
{
}

//-----------------------------------------------------------------------------

template < class T >
TKeyList< T >::TKeyList( const TKeyList< T >& other )
{
	CopyFrom( other );
}

//-----------------------------------------------------------------------------

template < class T >
TKeyList< T >::~TKeyList()
{
	Clear();
}

//-----------------------------------------------------------------------------

template < class T >
TKeyList< T >& TKeyList< T >::operator = ( const TKeyList< T >& other )
{
	CopyFrom( other );
	return (*this);
}

//-----------------------------------------------------------------------------

template < class T >
void TKeyList< T >::Clear()
{
	map< string, T* >::iterator it;
	for ( it = m_elements.begin(); it != m_elements.end(); ++it )
	{
		delete it->second;
	}
	m_elements.clear();
}

//-----------------------------------------------------------------------------

template < class T >
T* TKeyList< T >::Append( const String& key, const T& element )
{
	T* pT = Get( key );	

	if ( !pT )
	{
		pT = new T( element );
		m_elements.insert( pair< string, T* >( string( key.Data() ), pT ) );
		pT = Get( key );	
		assert( pT );
	}

	return (pT);
}

//-----------------------------------------------------------------------------

template < class T >
size_t TKeyList< T >::Size() const
{
	return (m_elements.size());
}

//-----------------------------------------------------------------------------

template < class T >
const T* TKeyList< T >::Get( const String& key ) const
{
	map< string, T* >::const_iterator it = m_elements.find( string( key.Data() ) );
	if ( it != m_elements.end() )
	{
		return (it->second);
	}
	else
	{
		return (NULL);
	}
}

//-----------------------------------------------------------------------------

template < class T >
T* TKeyList< T >::Get( const String& key )
{
	map< string, T* >::const_iterator it = m_elements.find( string( key.Data() ) );
	if ( it != m_elements.end() )
	{
		return (it->second);
	}
	else
	{
		return (NULL);
	}
}

//-----------------------------------------------------------------------------

template < class T >
bool TKeyList< T >::operator == ( const TKeyList< T >& other ) const
{
	if ( this == &other ) { return (true); }

	if ( m_elements.size() != other.m_elements.size() ) { return (false); }

	map< string, T* >::const_iterator itThis;
	map< string, T* >::const_iterator itOther;
	for ( itThis = m_elements.begin(), itOther = other.m_elements.begin(); 
		  itThis != m_elements.end(); 
		  ++itThis, ++itOther )
	{
		if ( (*(itThis->second)) != (*(itOther->second)) ) { return (false); }
	}

	return (true);
}

//-----------------------------------------------------------------------------

template < class T >
bool TKeyList< T >::operator != ( const TKeyList< T >& other ) const
{
	return (!(this->operator==( other )));
}

//-----------------------------------------------------------------------------

template < class T >
void TKeyList< T >::CopyFrom( const TKeyList< T >& other )
{
	Clear();
	map< string, T* >::const_iterator it;
	for ( it = other.m_elements.begin(); it != other.m_elements.end(); ++it )
	{
		Append( String( it->first.c_str() ), *(it->second) );
	}
}

//-----------------------------------------------------------------------------
