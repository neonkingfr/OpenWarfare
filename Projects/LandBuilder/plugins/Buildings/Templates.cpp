//-----------------------------------------------------------------------------
// File: Templates.cpp
//
// Desc: classes related to building templates
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Templates.h"
#include "Stair.h"

#include <projects/ObjektivLib/ObjToolProxy.h>

//-----------------------------------------------------------------------------

size_t CTemplate::s_counter = 1;

//-----------------------------------------------------------------------------

CTemplate::CTemplate( ofstream* logFile )
: m_referencesCount( 0 )
, m_id( s_counter )
, m_logFile( logFile )
{
  ++s_counter;
}

//-----------------------------------------------------------------------------

CTemplate::CTemplate( const CTemplate& other )
: m_referencesCount( other.m_referencesCount )
, m_id( other.m_id )
, m_logFile( other.m_logFile )
{
}

//-----------------------------------------------------------------------------

CTemplate::~CTemplate()
{
  assert( m_referencesCount == 0 );
}

//-----------------------------------------------------------------------------

void CTemplate::IncreaseReferencesCount()
{
  ++m_referencesCount;
}

//-----------------------------------------------------------------------------

void CTemplate::DecreaseReferencesCount()
{
  --m_referencesCount;
  assert( m_referencesCount >= 0 );
}

//-----------------------------------------------------------------------------

void CTemplate::ResetReferencesCount()
{
  m_referencesCount = 0;
}

//-----------------------------------------------------------------------------

String CTemplate::Filename() const
{
  return (Name() + ".p3d");
}

//-----------------------------------------------------------------------------

int CTemplate::GetReferencesCount() const
{
  return (m_referencesCount);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CProceduralTemplate::CProceduralTemplate( ofstream* logFile )
: CTemplate( logFile )
{
}

//-----------------------------------------------------------------------------

CProceduralTemplate::CProceduralTemplate( const CProceduralTemplate& other )
: CTemplate( other )
{
}

//-----------------------------------------------------------------------------

CProceduralTemplate::~CProceduralTemplate()
{
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CBuildingTemplate::CBuildingTemplate( const CBuildingType* buildingType, 
                                      const CGPolygon2& footprint, int numFloors, 
                                      bool enterable, bool rectifyCorners,
                                      Float similarityFactor, const String& namePrefix,
                                      bool shadowVolume, ofstream* logFile )
: CProceduralTemplate( logFile )
, m_buildingType( buildingType )
, m_footprint( footprint )
, m_enterable( enterable )
, m_rectifyCorners( rectifyCorners )
, m_similarityFactor( similarityFactor )
, m_namePrefix( namePrefix )
, m_shadowVolume( shadowVolume )
{
  if ( numFloors == -1 )
  {
    // numFloors == -1 means randomization
    m_numFloors = static_cast< int >( RandomInRange( CGConsts::ONE, static_cast< Float >( m_buildingType->GetMaxNumFloors() ) ) );
  }
  else
  {
    m_numFloors = numFloors;
  }

  PreprocessFootprint();
  Init();
}

//-----------------------------------------------------------------------------

CBuildingTemplate::CBuildingTemplate( const CBuildingTemplate& other )
: CProceduralTemplate( other )
, m_buildingType( other.m_buildingType )
, m_footprint( other.m_footprint )
, m_numFloors( other.m_numFloors )
, m_enterable( other.m_enterable )
, m_levels( other.m_levels )
, m_components( other.m_components )
, m_rectifyCorners( other.m_rectifyCorners )
, m_similarityFactor( other.m_similarityFactor )
, m_namePrefix( other.m_namePrefix )
, m_shadowVolume( other.m_shadowVolume )
{
}

//-----------------------------------------------------------------------------

CBuildingTemplate::~CBuildingTemplate()
{
}

//-----------------------------------------------------------------------------

const CBuildingType* CBuildingTemplate::GetBuildingType() const
{
  return (m_buildingType);
}

//-----------------------------------------------------------------------------

const CGPolygon2& CBuildingTemplate::GetFootprint() const
{
  return (m_footprint);
}

//-----------------------------------------------------------------------------

size_t CBuildingTemplate::GetNumFloors() const
{
  return (m_numFloors);
}

//-----------------------------------------------------------------------------

bool CBuildingTemplate::IsEnterable() const
{
  return (m_enterable);
}

//-----------------------------------------------------------------------------

bool CBuildingTemplate::GetRectifyCorners() const
{
  return (m_rectifyCorners);
}

//-----------------------------------------------------------------------------

bool CBuildingTemplate::UsesShadowVolume() const
{
  return (m_shadowVolume);
}

//-----------------------------------------------------------------------------

bool CBuildingTemplate::Equals( const CBuildingTemplate& other ) 
{
  // general data checks
  if ( m_buildingType   != other.m_buildingType )   { return (false); }
  if ( m_numFloors      != other.m_numFloors )      { return (false); }
  if ( m_enterable      != other.m_enterable )      { return (false); }
  if ( m_rectifyCorners != other.m_rectifyCorners ) { return (false); }
  if ( m_shadowVolume   != other.m_shadowVolume )   { return (false); }

  // footprint checks
  if ( m_footprint.VerticesCount() != other.m_footprint.VerticesCount() ) { return (false); }

  Float thisArea  = m_footprint.Area();
  Float otherArea = other.m_footprint.Area();
  Float areaDiff  = abs( thisArea - otherArea );
  if ( areaDiff / thisArea > static_cast< Float >( 0.05 ) ) { return (false); }

  Float thisRatio  = m_footprint.MinimumBoundingBox().AspectRatio();
  Float otherRatio = other.m_footprint.MinimumBoundingBox().AspectRatio();
  Float ratioDiff  = abs( thisRatio - otherRatio );
  if ( ratioDiff / thisRatio > static_cast< Float >( 0.05 ) ) { return (false); }

  if ( !m_footprint.EqualsTurningSequence( other.m_footprint ) ) { return (false); }

  if ( !m_footprint.Equals( other.m_footprint, static_cast< Float >( 0.05 ) * m_similarityFactor ) ) 
  { 
    CGPolygon2 otherCopy( other.m_footprint );
    otherCopy.RotateAroundBarycenter( CGConsts::PI );
    if ( !m_footprint.Equals( otherCopy, static_cast< Float >( 0.05 ) * m_similarityFactor ) ) { return (false); }
  }

  return (true);
}

//-----------------------------------------------------------------------------

ETemplateType CBuildingTemplate::Type() const
{
  return (TT_Building);
}

//-----------------------------------------------------------------------------

String CBuildingTemplate::Name() const
{
  char buff[40];
  sprintf( buff, "building_%07d", m_id );
  String name = m_namePrefix + String( buff );
  return (name);
}

//-----------------------------------------------------------------------------

bool CBuildingTemplate::ExportToP3D( const String& modelsPath )
{
  BuildGeometry();

  LODObject lodObj;

  // LOD 1.0000
  GenerateLOD( lodObj, 1.0f );
  lodObj.DeleteLevel( 0 );

  // LOD Geometry
  GenerateLOD( lodObj, LOD_GEOMETRY );

  // LOD View Geometry
//	GenerateLOD( lodObj, LOD_VIEWGEO );

  // LOD Fire Geometry
//	GenerateLOD( lodObj, LOD_FIREGEO );

  // LOD Shadow Volume 0.000
  if ( m_shadowVolume ) { GenerateLOD( lodObj, LOD_GET_SHADOW( 0.0 ) ); }

  // LOD RoadWay
  GenerateLOD( lodObj, LOD_ROADWAY );
  
  String path = modelsPath;
  if ( path[path.GetLength() - 1] == '\\' )
  {
    path = path.Substring( 0, path.GetLength() - 1 );
  }

  Pathname pathname = path + "\\" + Filename();
  if ( lodObj.Save( pathname, OBJDATA_LATESTVERSION, false, 0, 0 ) != 0 ) { return (false); }

  if ( !ExportTextures() ) { return (false); }
  if ( !ExportProxies() )  { return (false); }

  return (true);
}

//-----------------------------------------------------------------------------

bool CBuildingTemplate::ExportTextures()
{
  for ( size_t i = 0, cntI = m_levels.Size(); i < cntI; ++i )
  {
    const GeometriesList& geometries = m_levels.Get( i )->GetGeometries();
    for ( size_t j = 0, cntJ = geometries.Size(); j < cntJ; ++j )
    {
      const FacesList& faces = geometries.Get( j )->GetFaces();
      for ( size_t k = 0, cntK = faces.Size(); k < cntK; ++k )
      {
        const CTexture* texture = faces.Get( k )->GetTexture();
        if ( texture )
        {
          if ( !texture->Export() ) { return (false); }
        }
      }
    }
  }

  return (true);
}

//-----------------------------------------------------------------------------

bool CBuildingTemplate::ExportProxies()
{
  for ( size_t i = 0, cntI = m_levels.Size(); i < cntI; ++i )
  {
    const ProxyHinstancesList& proxies = m_levels.Get( i )->GetProxies();
    for ( size_t j = 0, cntJ = proxies.Size(); j < cntJ; ++j )
    {
      const CProxy* proxy = proxies.Get( j )->GetProxy();
      if ( proxy )
      {
        if ( !proxy->Export() ) { return (false); }
      }
    }
  }

  return (true);
}

//-----------------------------------------------------------------------------

bool CBuildingTemplate::BuildGeometry()
{
  // first builds the levels' geometries
  // solidifies this template if it can not build enterable geometry
  int i = 0;
  int size = static_cast< int >( m_levels.Size() );
  while ( i < size )
  {
    CBuildingLevel* level = m_levels.Get( i ); 
    assert( level );
    if ( !level->BuildGeometry() ) { return (false); }

    if ( level->IsSolidified() )
    {
      m_enterable = false;
      Init();
      size = static_cast< int >( m_levels.Size() );
      i = -1;
      if ( m_logFile ) { (*m_logFile) << "Rebuilding: " << Name() << " with solid geometry." << endl; }
    }
    ++i;
  }

  // then adapts each level to the components
  for ( size_t i = 0, cntI = m_components.Size(); i < cntI; ++i )
  {
    CBuildingComponent* component = m_components.Get( i ); 
    assert( component );
    if ( !component->TransformGeometries() ) { return (false); }
    // moves up 10 cm to avoid interference with terrain
    component->Translate( CGConsts::ZERO, CGConsts::ZERO, static_cast< Float >( 0.1 ) );
  }

  return (true);
}

//-----------------------------------------------------------------------------

void CBuildingTemplate::GenerateLOD( LODObject& lodObj, float resolution )
{
  if ( resolution == LOD_ROADWAY )
  { 
    GenerateRoadWayLOD( lodObj );
  }
  else if ( resolution == LOD_GET_SHADOW( 0.0 ) )
  { 
    GenerateShadowLOD( lodObj );
  }
  else if ( resolution == LOD_GEOMETRY ||
            resolution == LOD_VIEWGEO  ||
            resolution == LOD_FIREGEO )
  { 
    GenerateComponentsLOD( lodObj, resolution );
  }
  else
  {
    ObjectData* obj = new ObjectData;
    CGeometry lodGeometry;

    // extracts the lod geometry from the template geometry
    for ( size_t i = 0, cntI = m_components.Size(); i < cntI; ++i )
    {
      const GeometriesList& geometries = m_components.Get( i )->GetGeometries();
      for ( size_t j = 0, cntJ = geometries.Size(); j < cntJ; ++j )
      {
        const CGeometry* geometry = geometries.Get( j );
        for ( size_t k = 0, cntK = geometry->FacesCount(); k < cntK; ++k )
        {
          CFace face = geometry->Face( k );
          for ( size_t m = 0; m < 3; ++m )
          {
            const CGPoint3& point = geometry->Vertex( face.GetIndex( m ) );
            int index = lodGeometry.FindVertexIndex( point );
            if ( index == -1 )
            {
              lodGeometry.AddVertex( point );
              index = lodGeometry.VerticesCount() - 1;
            }
            face.SetIndex( m, index );
          }
          lodGeometry.AddFace( face );
        }
      }
    }

    // vertices
    for ( size_t i = 0, cntI = lodGeometry.VerticesCount(); i < cntI; ++i )
    {
      const CGPoint3& point = lodGeometry.Vertex( i );
      obj->NewPoint()->SetPoint( Vector3( static_cast< Coord >( point.GetX() ),
                                          static_cast< Coord >( point.GetZ() ), 
                                          static_cast< Coord >( point.GetY() ) ) );
    }

    // faces
    for ( size_t i = 0, cntI = lodGeometry.FacesCount(); i < cntI; ++i )
    {
      const CFace& face = lodGeometry.Face( i );
      if ( face.IsVisible() )
      {
        FaceT objFace;
        objFace.CreateIn( obj );
        objFace.SetN( 3 );

        for ( size_t j = 0; j < 3; ++j )
        {
          objFace.SetPoint( j, face.GetIndex( j ) );
          const CGPoint2& point = face.GetUV( j );
          objFace.SetUV( j, static_cast< float >( point.GetX() ), static_cast< float >( point.GetY() ) );
        }

        objFace.SetTexture( face.GetTexture()->GetDstRelFilename() );
      }
    }

    // sharp edges
    for ( size_t i = 0, cntI = lodGeometry.FacesCount(); i < cntI; ++i )
    {
      const CFace& face = lodGeometry.Face( i );
      if ( face.IsVisible() )
      {
        obj->AddSharpEdge( face.GetIndex( 0 ), face.GetIndex( 1 ) );
        obj->AddSharpEdge( face.GetIndex( 1 ), face.GetIndex( 2 ) );
        obj->AddSharpEdge( face.GetIndex( 2 ), face.GetIndex( 0 ) );
      }
    }

    // masses
    for ( size_t i = 0, cntI = lodGeometry.VerticesCount(); i < cntI; ++i )
    {
      obj->SetPointMass( i, 1250.0f );
    }

    obj->RecalcNormals();

    // named selection
    Selection namedSelection( obj );

    for ( size_t i = 0, cntI = lodGeometry.VerticesCount(); i < cntI; ++i )
    {
      namedSelection.PointSelect( i );
    }

    size_t facesCounter = 0;
    for ( size_t i = 0, cntI = lodGeometry.FacesCount(); i < cntI; ++i )
    {
      const CFace& face = lodGeometry.Face( i );
      if ( face.IsVisible() )
      {
        namedSelection.FaceSelect( facesCounter++ );
      }
    }
    obj->SaveNamedSel( "base", &namedSelection );

    // proxies
    for ( size_t i = 0, cntI = m_components.Size(); i < cntI; ++i )
    {
      const ProxyHinstancesList& proxies = m_components.Get( i )->GetProxies();

      for ( size_t j = 0, cntJ = proxies.Size(); j < cntJ; ++j )
      {
        const CProxyHinstance* proxyHinstance = proxies.Get( j );
        const CGPoint3& proxyPosition = proxyHinstance->GetPosition();
        float position[3];
        position[0] = static_cast< float >( proxyPosition.GetX() );
        position[1] = static_cast< float >( proxyPosition.GetZ() );
        position[2] = static_cast< float >( proxyPosition.GetY() );
        const CGVector3& axisX = proxyHinstance->GetAxisX();
        const CGVector3& axisY = proxyHinstance->GetAxisY();
        const CGVector3& axisZ = proxyHinstance->GetAxisZ();
        Matrix3P rotation( static_cast< float >( axisX.GetX() ), static_cast< float >( axisZ.GetX() ), static_cast< float >( axisY.GetX() ),
                           static_cast< float >( axisX.GetZ() ), static_cast< float >( axisZ.GetZ() ), static_cast< float >( axisY.GetZ() ),
                           static_cast< float >( axisX.GetY() ), static_cast< float >( axisZ.GetY() ), static_cast< float >( axisY.GetY() ) );
        obj->GetTool< ObjToolProxy >().CreateProxy( proxyHinstance->GetProxy()->GetDstRelFilename(), position, &rotation );
      }
    }

    lodObj.InsertLevel( obj, resolution );
  }
}

//-----------------------------------------------------------------------------

void CBuildingTemplate::GenerateComponentsLOD( LODObject& lodObj, float resolution )
{
  ObjectData* obj = new ObjectData;

  size_t verticesCount = 0;
  size_t facesCount    = 0;
  int    compCounter   = -1;

  for ( size_t i = 0, cntI = m_components.Size(); i < cntI; ++i )
  {
    const GeometriesList& geometries = m_components.Get( i )->GetGeometries();
    for ( size_t j = 0, cntJ = geometries.Size(); j < cntJ; ++j )
    {
      ++compCounter;
      const CGeometry* geometry = geometries.Get( j );

      // vertices
      size_t geometryVerticesCount = geometry->VerticesCount();
      for ( size_t k = 0; k < geometryVerticesCount; ++k )
      {
        const CGPoint3& point = geometry->Vertex( k );
        obj->NewPoint()->SetPoint( Vector3( static_cast< Coord >( point.GetX() ),
                                            static_cast< Coord >( point.GetZ() ), 
                                            static_cast< Coord >( point.GetY() ) ) );
      }

      // faces
      size_t geometryFacesCount = geometry->FacesCount();
      for ( size_t k = 0; k < geometryFacesCount; ++k )
      {
        FaceT objFace;
        objFace.CreateIn( obj );
        objFace.SetN( 3 );

        const CFace& face = geometry->Face( k );
        for ( size_t m = 0; m < 3; ++m )
        {
          objFace.SetPoint( m, face.GetIndex( m ) + verticesCount );
          const CGPoint2& point = face.GetUV( m );
          objFace.SetUV( m, static_cast< float >( point.GetX() ), static_cast< float >( point.GetY() ) );
        }
      }

      // sharp edges
      for ( size_t k = 0; k < geometryFacesCount; ++k )
      {
        const CFace& face = geometry->Face( k );
        obj->AddSharpEdge( face.GetIndex( 0 ), face.GetIndex( 1 ) );
        obj->AddSharpEdge( face.GetIndex( 1 ), face.GetIndex( 2 ) );
        obj->AddSharpEdge( face.GetIndex( 2 ), face.GetIndex( 0 ) );
      }

      // masses
      for ( size_t k = 0; k < geometryVerticesCount; ++k )
      {
        obj->SetPointMass( verticesCount + k, 1250.0f );
      }

      // named selection
      Selection namedSelection( obj );

      for ( size_t k = 0; k < geometryVerticesCount; ++k )
      {
        namedSelection.PointSelect( verticesCount + k );
      }
      for ( size_t k = 0; k < geometryFacesCount; ++k )
      {
        namedSelection.FaceSelect( facesCount + k );
      }

      char buff[20];
      sprintf( buff, "%d", compCounter + 1 );
      String strCompCounter( buff );
      if ( strCompCounter.GetLength() < 2 ) { strCompCounter = "0" + strCompCounter; }
      strCompCounter = "Component" + strCompCounter;
      obj->SaveNamedSel( strCompCounter, &namedSelection );

      verticesCount += geometryVerticesCount;
      facesCount    += geometryFacesCount;
    }
  }

  // proxies
  for ( size_t i = 0, cntI = m_components.Size(); i < cntI; ++i )
  {
    const ProxyHinstancesList& proxies = m_components.Get( i )->GetProxies();

    for ( size_t j = 0, cntJ = proxies.Size(); j < cntJ; ++j )
    {
      const CProxyHinstance* proxyHinstance = proxies.Get( j );
      const CGPoint3& proxyPosition = proxyHinstance->GetPosition();
      float position[3];
      position[0] = static_cast< float >( proxyPosition.GetX() );
      position[1] = static_cast< float >( proxyPosition.GetZ() );
      position[2] = static_cast< float >( proxyPosition.GetY() );
      const CGVector3& axisX = proxyHinstance->GetAxisX();
      const CGVector3& axisY = proxyHinstance->GetAxisY();
      const CGVector3& axisZ = proxyHinstance->GetAxisZ();
      Matrix3P rotation( static_cast< float >( axisX.GetX() ), static_cast< float >( axisZ.GetX() ), static_cast< float >( axisY.GetX() ),
                         static_cast< float >( axisX.GetZ() ), static_cast< float >( axisZ.GetZ() ), static_cast< float >( axisY.GetZ() ),
                         static_cast< float >( axisX.GetY() ), static_cast< float >( axisZ.GetY() ), static_cast< float >( axisY.GetY() ) );
      obj->GetTool< ObjToolProxy >().CreateProxy( proxyHinstance->GetProxy()->GetDstRelFilename(), position, &rotation );
    }
  }

  if ( resolution == LOD_GEOMETRY )
  {
    obj->SetNamedProp( "class", "house" );
    obj->SetNamedProp( "map", "house" );
    obj->SetNamedProp( "dammage", "building" );
    obj->SetNamedProp( "autocenter", "0" );
    if ( m_shadowVolume )
    {
      obj->SetNamedProp( "sbsource", "shadowvolume" );
      obj->SetNamedProp( "prefershadowvolume", "0" );
    }
    else
    {
      obj->SetNamedProp( "sbsource", "visual" );
      obj->SetNamedProp( "prefershadowvolume", "0" );
    }
  }

  lodObj.InsertLevel( obj, resolution );
}

//-----------------------------------------------------------------------------

void CBuildingTemplate::GenerateRoadWayLOD( LODObject& lodObj )
{
  ObjectData* obj = new ObjectData;

  CGeometry roadWayGeometry;

  // extracts the roadway geometry from the template geometry
  for ( size_t i = 0, cntI = m_components.Size(); i < cntI; ++i )
  {
    const GeometriesList& geometries = m_components.Get( i )->GetGeometries();
    for ( size_t j = 0, cntJ = geometries.Size(); j < cntJ; ++j )
    {
      const CGeometry* geometry = geometries.Get( j );
      for ( size_t k = 0, cntK = geometry->FacesCount(); k < cntK; ++k )
      {
        CFace face = geometry->Face( k );
        if ( face.IsRoadWay() )
        {
          for ( size_t m = 0; m < 3; ++m )
          {
            const CGPoint3& point = geometry->Vertex( face.GetIndex( m ) );
            int index = roadWayGeometry.FindVertexIndex( point );
            if ( index == -1 )
            {
              roadWayGeometry.AddVertex( point );
              index = roadWayGeometry.VerticesCount() - 1;
            }
            face.SetIndex( m, index );
          }
          roadWayGeometry.AddFace( face );
        }
      }
    }
  }

  // vertices
  for ( size_t i = 0, cntI = roadWayGeometry.VerticesCount(); i < cntI; ++i )
  {
    const CGPoint3& point = roadWayGeometry.Vertex( i );
    obj->NewPoint()->SetPoint( Vector3( static_cast< Coord >( point.GetX() ),
                                        static_cast< Coord >( point.GetZ() ), 
                                        static_cast< Coord >( point.GetY() ) ) );
  }

  // faces
  for ( size_t i = 0, cntI = roadWayGeometry.FacesCount(); i < cntI; ++i )
  {
    FaceT face;
    face.CreateIn( obj );
    face.SetN( 3 );

    const CFace& gFace = roadWayGeometry.Face( i );
    for ( size_t j = 0; j < 3; ++j )
    {
      face.SetPoint( j, gFace.GetIndex( j ) );
    }

    obj->FaceSelect( face.GetFaceIndex() );
  }

  obj->RecalcNormals();
  lodObj.InsertLevel( obj, LOD_ROADWAY );
}

//-----------------------------------------------------------------------------

void CBuildingTemplate::GenerateShadowLOD( LODObject& lodObj )
{
  ObjectData* obj = new ObjectData;

  CGeometry lodGeometry;

  // extracts the lod geometry from the template geometry
  for ( size_t i = 0, cntI = m_components.Size(); i < cntI; ++i )
  {
    const GeometriesList& geometries = m_components.Get( i )->GetGeometries();
    for ( size_t j = 0, cntJ = geometries.Size(); j < cntJ; ++j )
    {
      const CGeometry* geometry = geometries.Get( j );
      for ( size_t k = 0, cntK = geometry->FacesCount(); k < cntK; ++k )
      {
        CFace face = geometry->Face( k );
        for ( size_t m = 0; m < 3; ++m )
        {
          const CGPoint3& point = geometry->Vertex( face.GetIndex( m ) );
          // the following 0.0001 is needed or sometimes it happens to find duplicated points
          size_t index = lodGeometry.AddVertex( point, static_cast< Float >( 0.0001 ) ); 
          face.SetIndex( m, index );
        }
        lodGeometry.AddFace( face );
      }
    }
  }

  // vertices
  for ( size_t i = 0, cntI = lodGeometry.VerticesCount(); i < cntI; ++i )
  {
    const CGPoint3& point = lodGeometry.Vertex( i );
    obj->NewPoint()->SetPoint( Vector3( static_cast< Coord >( point.GetX() ),
                                        static_cast< Coord >( point.GetZ() ),
                                        static_cast< Coord >( point.GetY() ) ) );
  }

  // faces
  for ( size_t i = 0, cntI = lodGeometry.FacesCount(); i < cntI; ++i )
  {
    const CFace& face = lodGeometry.Face( i );
    if ( face.IsShadow() )
    {
      FaceT objFace;
      objFace.CreateIn( obj );
      objFace.SetN( 3 );

      for ( size_t j = 0; j < 3; ++j )
      {
        objFace.SetPoint( j, face.GetIndex( j ) );
        const CGPoint2& point = face.GetUV( j );
        objFace.SetUV( j, static_cast< float >( point.GetX() ), static_cast< float >( point.GetY() ) );
      }

      obj->FaceSelect( objFace.GetFaceIndex() );
    }
  }

  // sharp edges
  for ( size_t i = 0, cntI = lodGeometry.FacesCount(); i < cntI; ++i )
  {
    const CFace& face = lodGeometry.Face( i );
    if ( face.IsShadow() )
    {
      obj->AddSharpEdge( face.GetIndex( 0 ), face.GetIndex( 1 ) );
      obj->AddSharpEdge( face.GetIndex( 1 ), face.GetIndex( 2 ) );
      obj->AddSharpEdge( face.GetIndex( 2 ), face.GetIndex( 0 ) );
    }
  }

  // masses
  for ( size_t i = 0, cntI = lodGeometry.VerticesCount(); i < cntI; ++i )
  {
    obj->SetPointMass( i, 1250.0f );
  }

  obj->RecalcNormals();
  lodObj.InsertLevel( obj, LOD_GET_SHADOW( 0.0 ) );
}

//-----------------------------------------------------------------------------

void CBuildingTemplate::PreprocessFootprint()
{
  // pre-processing of footprint
  // {
  // ensures that the footprint vertices are ordered CCW
  m_footprint.SetVerticesOrderToCCW();

  // TODO: here there should be the corner rectification call
//	if ( m_rectifyCorners ) { m_footprint.Rectify(); }

  // translate the footprint to its barycentric system
  const CGPoint2& barycenter = m_footprint.Barycenter();
  m_footprint.Translate( -barycenter.GetX(), -barycenter.GetY() );

  // rotates the footprint to match its minimum bounding box orientation
  Float orientation = m_footprint.MinimumBoundingBox().GetOrientation();
  m_footprint.RotateAroundBarycenter( -orientation );
  // }
}

//-----------------------------------------------------------------------------

void CBuildingTemplate::Init()
{
  // in this implementation the building is constructed as a stack
  // of CBuildingLevel all sharing the same footprint
  // other implementations could use several CBuildingLevel using different
  // footprints, also at the same elevation level, to construct more complex shaped
  // buildings

  m_levels.Clear();
  m_components.Clear();

  // initializes levels and components
  // {
  Float floorsHeight      = m_buildingType->GetFloorHeight();
  Float interFloorsHeight = m_buildingType->GetInterFloorHeight();
  Float roofHeight        = m_buildingType->GetRoofHeight();
  bool  usesInterFloors   = m_buildingType->UsesInterFloors();

  const EBuildingRoofsType roofType = m_buildingType->GetRoofType();
  bool usesStairs = m_enterable &&
                    m_buildingType->UsesStairs() && m_buildingType->GetStairType() &&
                    m_numFloors <= BUILDING_FLOORS_ENTERABLE_MAXNUM &&
                    ((m_numFloors == 1 && (roofType == BRT_FLAT || roofType == BRT_FLAT_WALLED)) ||
                    (m_numFloors > 1 && usesInterFloors));

  CStair* stair = NULL;
  if ( usesStairs )
  {
    Float groundHeight;
    if ( m_numFloors == 1 )
    {
      groundHeight = floorsHeight + roofHeight;
    }
    else
    {
      groundHeight = floorsHeight + interFloorsHeight;
    }

    // searches a suitable position for the stair
    usesStairs = false;
    CStair tempStair;
    for ( size_t i = 0, cntI = m_footprint.VerticesCount(); i < cntI; ++i )
    {
      CGSegment2 edge = m_footprint.Edge( i );
      CGVector2 vecEdge( edge.GetTo(), edge.GetFrom() );
      CGVector2 normEdge = vecEdge.PerpCCW();
      normEdge.Normalize();
      normEdge *= (DEFAULT_STAIR_MARGIN + m_buildingType->GetWallsWidth() + CGConsts::HALF * m_buildingType->GetStairType()->GetWidth());
      CGPoint2 stairCenter      = normEdge.Head( edge.MidPoint() );
      Float    stairOrientation = edge.Direction();
  
      tempStair = CStair( m_buildingType->GetStairType(), groundHeight,
                          stairCenter, stairOrientation );
      CGPolygon2 stairFootprint = tempStair.Footprint().Offset( DEFAULT_STAIR_MARGIN );

      if ( !m_footprint.Contains( stairFootprint, true ) )
      {
        stairFootprint.Rotate( stairCenter, CGConsts::HALF_PI );
        if ( m_footprint.Contains( stairFootprint, true ) )
        {
          tempStair.SetOrientation( stairOrientation + CGConsts::HALF_PI );
          usesStairs = true;
          break;
        }
      }
      else
      {
        usesStairs = true;
        break;
      }
    }

    if ( usesStairs ) { stair = new CStair( tempStair ); }
  }

  // adds the basement level and component
  Float elevation = -floorsHeight;
  bool bottomFacesVisible = true;
  bool topFacesVisible    = m_enterable ? true : false;
  CBuildingLevel* basement = m_levels.AddFloor( CBuildingFloor( m_buildingType, 
                                                                BFT_SOLID_UNDERGROUND, 
                                                                &m_footprint, floorsHeight, 
                                                                floorsHeight, 
                                                                bottomFacesVisible, 
                                                                topFacesVisible,
                                                                m_logFile, NULL ) );
  m_components.Append( CBuildingComponent( basement,
                                           CGPoint3( CGConsts::ZERO, CGConsts::ZERO, elevation ), 
                                           CGConsts::ZERO ) );
  elevation += basement->GetHeight();

  // adds the floor levels
  CGPolygon2 floorFootprint( m_footprint );

  CBuildingLevel* floorGround  = NULL;
  CBuildingLevel* floorGeneric = NULL;

  bottomFacesVisible = false;
  topFacesVisible    = false;

  if ( m_enterable )
  {
    CStair* floorGroundStair  = NULL;
    CStair* floorGenericStair = NULL;

    CGPoint2 floorGroundPosition     = CGPoint2();
    CGPoint2 floorGenericPosition    = CGPoint2();
    Float    floorGroundOrientation  = CGConsts::ZERO;
    Float    floorGenericOrientation = CGConsts::ZERO;

    Float floorGroundHeight;
    if ( m_numFloors == 1 )
    {
      floorGroundHeight = floorsHeight + roofHeight;
    }
    else
    {
      floorGroundHeight = floorsHeight + interFloorsHeight;
    }

    Float floorGenericHeight = floorsHeight + roofHeight;

    if ( stair )
    {
      floorGroundStair = new CStair( *stair );
      CGPoint2& stairCenter = floorGroundStair ->GetCenter();
      Float dx = stairCenter.GetX() - floorGroundPosition.GetX();
      Float dy = stairCenter.GetY() - floorGroundPosition.GetY();
      stairCenter.Set( CGPoint2( dx, dy ) );
      floorGroundStair->SetOrientation( floorGroundStair->GetOrientation() + floorGroundOrientation );
      floorGroundStair->SetHeight( floorGroundHeight );

      if ( roofType == BRT_FLAT || roofType == BRT_FLAT_WALLED )
      {
        floorGenericStair = new CStair( *stair );
        stairCenter = floorGenericStair->GetCenter();
        dx = stairCenter.GetX() - floorGroundPosition.GetX();
        dy = stairCenter.GetY() - floorGroundPosition.GetY();
        stairCenter.Set( CGPoint2( dx, dy ) );
        floorGenericStair->SetOrientation( floorGenericStair->GetOrientation() + floorGroundOrientation );
        floorGenericStair->SetHeight( floorGenericHeight );
      }
    }

    floorGround = m_levels.AddFloor( CBuildingFloor( m_buildingType, BFT_ENTERABLE_GROUND, &floorFootprint, floorsHeight, floorsHeight, bottomFacesVisible, topFacesVisible, m_logFile, floorGroundStair ) );
    if ( m_numFloors > BUILDING_FLOORS_ENTERABLE_MAXNUM )
    {
      floorGeneric = m_levels.AddFloor( CBuildingFloor( m_buildingType, BFT_SOLID, &floorFootprint, floorsHeight, floorsHeight, bottomFacesVisible, topFacesVisible, m_logFile, NULL ) );
    }
    else
    {
      floorGeneric = m_levels.AddFloor( CBuildingFloor( m_buildingType, BFT_ENTERABLE, &floorFootprint, floorsHeight, floorsHeight, bottomFacesVisible, topFacesVisible, m_logFile, floorGenericStair ) );
    }

    if ( floorGroundStair )  { delete floorGroundStair; }
    if ( floorGenericStair ) { delete floorGenericStair; }
  }
  else
  {
    floorGround  = m_levels.AddFloor( CBuildingFloor( m_buildingType, BFT_SOLID_GROUND, &floorFootprint, floorsHeight, floorsHeight, bottomFacesVisible, topFacesVisible, m_logFile, NULL ) );
    floorGeneric = m_levels.AddFloor( CBuildingFloor( m_buildingType, BFT_SOLID, &floorFootprint, floorsHeight, floorsHeight, bottomFacesVisible, topFacesVisible, m_logFile, NULL ) );
  }

  // adds the interfloor levels
  CBuildingLevel* interFloorGround = NULL;
  if ( usesInterFloors )
  {
    if ( m_enterable && (m_numFloors > BUILDING_FLOORS_ENTERABLE_MAXNUM) )
    {
      bottomFacesVisible = true;
      topFacesVisible    = false;
      interFloorGround = m_levels.AddInterFloor( CBuildingInterFloor( m_buildingType,
                                                                      &m_footprint,
                                                                      interFloorsHeight, 
                                                                      floorsHeight, 
                                                                      BIFT_SOLID, 
                                                                      bottomFacesVisible, 
                                                                      topFacesVisible,
                                                                      m_logFile ) );
    }
  }

  CBuildingLevel* interFloorGeneric = NULL;
  if ( usesInterFloors )
  {
    CGPolygon2* interFloorFootprint = NULL;

    if ( m_enterable && (m_numFloors <= BUILDING_FLOORS_ENTERABLE_MAXNUM) )
    {
      interFloorFootprint = new CGHoledPolygon2( m_footprint );
      bottomFacesVisible = true;
      topFacesVisible    = true;

      if ( stair )
      {
        CGPoint2 interFloorGroundPosition    = CGPoint2();
        Float    interFloorGroundOrientation = CGConsts::ZERO;
        stair->SetHeight( floorsHeight + interFloorsHeight );

        CGPolygon2 stairFootprint = stair->Footprint();
        stairFootprint.Translate( -interFloorGroundPosition.GetX(), -interFloorGroundPosition.GetY() );
        stairFootprint.Rotate( interFloorGroundPosition, -interFloorGroundOrientation );
        reinterpret_cast< CGHoledPolygon2* >( interFloorFootprint )->AddHole( stairFootprint );
      }
    }
    else
    {
      interFloorFootprint = new CGPolygon2( m_footprint );
      bottomFacesVisible = false;
      topFacesVisible    = false;
    }

    interFloorGeneric = m_levels.AddInterFloor( CBuildingInterFloor( m_buildingType,
                                                                     interFloorFootprint,
                                                                     interFloorsHeight, 
                                                                     floorsHeight, 
                                                                     BIFT_SOLID, 
                                                                     bottomFacesVisible, 
                                                                     topFacesVisible, 
                                                                     m_logFile ) );
    delete interFloorFootprint;
  }

  // adds floor and interfloor components
  for ( size_t i = 0, cntI = m_numFloors; i < cntI; ++i )
  {
    // adds floor components
    if ( i == 0 )
    {
      m_components.Append( CBuildingComponent( floorGround, 
                                               CGPoint3( CGConsts::ZERO, CGConsts::ZERO, elevation ), 
                                               CGConsts::ZERO ) );
      elevation += floorGround->GetHeight();
    }
    else
    {
      m_components.Append( CBuildingComponent( floorGeneric, 
                                               CGPoint3( CGConsts::ZERO, CGConsts::ZERO, elevation ), 
                                               CGConsts::ZERO ) );
      elevation += floorGeneric->GetHeight();
    }

    // adds interfloor components
    if ( usesInterFloors && (i < cntI - 1) )
    {
      if ( i == 0 && m_enterable && (m_numFloors > BUILDING_FLOORS_ENTERABLE_MAXNUM) )
      {
        m_components.Append( CBuildingComponent( interFloorGround, 
                                                 CGPoint3( CGConsts::ZERO, CGConsts::ZERO, elevation ), 
                                                 CGConsts::ZERO ) );
        elevation += interFloorGround->GetHeight();
      }
      else
      {
        m_components.Append( CBuildingComponent( interFloorGeneric, 
                                                 CGPoint3( CGConsts::ZERO, CGConsts::ZERO, elevation ), 
                                                 CGConsts::ZERO ) );
        elevation += interFloorGeneric->GetHeight();
      }
    }
  }

  // adds the roof level and component
  CGPolygon2* roofFootprint = NULL;

  if ( m_enterable && (m_numFloors <= BUILDING_FLOORS_ENTERABLE_MAXNUM) )
  {
    bottomFacesVisible = true;

    if ( (roofType == BRT_FLAT || roofType == BRT_FLAT_WALLED) && stair )
    {
      roofFootprint = new CGHoledPolygon2( m_footprint );
      CGPoint2 roofPosition    = CGPoint2();
      Float    roofOrientation = CGConsts::ZERO;
      stair->SetHeight( floorsHeight + roofHeight );

      CGPolygon2 stairFootprint = stair->Footprint();
      stairFootprint.Translate( -roofPosition.GetX(), -roofPosition.GetY() );
      stairFootprint.Rotate( roofPosition, -roofOrientation );
      reinterpret_cast< CGHoledPolygon2* >( roofFootprint )->AddHole( stairFootprint );
    }
    else
    {
      roofFootprint = new CGPolygon2( m_footprint );
    }
  }
  else if ( !m_enterable && (roofType == BRT_HIP_OFFSET) )
  {
    roofFootprint = new CGPolygon2( m_footprint );
    bottomFacesVisible = true;
  }
  else
  {
    roofFootprint = new CGPolygon2( m_footprint );
    bottomFacesVisible = false;
  }

  CBuildingLevel* roof = m_levels.AddRoof( CBuildingRoof( m_buildingType, 
                                                          roofFootprint, 
                                                          roofHeight, floorsHeight,
                                                          bottomFacesVisible, 
                                                          m_logFile ) );
  m_components.Append( CBuildingComponent( roof, 
                                           CGPoint3( CGConsts::ZERO, CGConsts::ZERO, elevation ), 
                                           CGConsts::ZERO ) );
  delete roofFootprint;
  // }

  if ( stair ) { delete stair; }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CBuildingTemplatesLibrary::CBuildingTemplatesLibrary()
{
}

//-----------------------------------------------------------------------------

CBuildingTemplatesLibrary::~CBuildingTemplatesLibrary()
{
}

//-----------------------------------------------------------------------------
