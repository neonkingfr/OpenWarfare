//-----------------------------------------------------------------------------
// File: Edifici.cpp
//
// Desc: module to generate procedural buildings
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Edifici.h"
#include "Templates.h"

//-----------------------------------------------------------------------------

static LandBuilder2::RegisterModule< CEdifici > edifici( "Edifici" );

//-----------------------------------------------------------------------------

CEdifici::CEdifici()
: m_shapesCount( 0 )
, m_aborted( false )
{
}

//-----------------------------------------------------------------------------

CEdifici::~CEdifici()
{
  // closes log file
  if ( !m_logFile.fail() ) { m_logFile.close(); } 
}

//-----------------------------------------------------------------------------

void CEdifici::Run( IMainCommands* cmdIfc )
{
  if ( m_aborted ) { return; }

  RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
  if ( taskName != m_currTaskName )
  {
    m_shapesCount  = 0;
    m_currTaskName = taskName;
  }

  ++m_shapesCount;
  LandBuilder2::ParamParser params( cmdIfc );

  // this part contains all the "global" variables and it is run only the
  // first time we enter this module
  if ( m_shapesCount == 1 )
  {
    m_startingTime = GetTickCount();

    // sets the working path
    m_workingPath = cmdIfc->GetCurrentTask()->GetWorkFolder();

    // opens log file
    m_logFile.open( m_workingPath + "\\Edifici.log", std::ios::out | std::ios::trunc );
    if ( m_logFile.fail() )
    {
      LOGF( Error ) << "Unable to open the log file.";
    }

    m_logFile << "=============" << endl;
    m_logFile << "Edifici 1.1.2" << endl;
    m_logFile << "=============" << endl << endl;

    // sets the models path
    try 
    {
      m_modelsPath = params.GetTextValue( MODELS_PATH );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_modelsPath = "";
    }

    if ( m_modelsPath.IsEmpty() )
    {
      m_logFile << "WARNING: Missing \"" << MODELS_PATH << "\" field in class Parameters." << endl;
      m_modelsPath = "c:\\ca\\edifici";
      m_logFile << "         Assuming \"" << m_modelsPath << "\" as default." << endl << endl;
    }
    else if ( m_modelsPath.Find( ':' ) == -1 )
    {
      m_logFile << "WARNING: Wrong \"" << MODELS_PATH << "\" field in class Parameters." << endl;
      if ( m_modelsPath[0] == '\\' )
      {
        m_modelsPath = "c:" + m_modelsPath;
      }
      else
      {
        m_modelsPath = "c:\\" + m_modelsPath;
      }
      m_logFile << "         Assuming \"" << m_modelsPath << "\" as default." << endl << endl;
    }

    // removes eventual trailing slashes
    while ( m_modelsPath[m_modelsPath.GetLength() - 1] == '\\' )
    {
      m_modelsPath = m_modelsPath.Substring( 0, m_modelsPath.GetLength() - 1 );
    }

    // sets the texture path
    m_texturesPath = m_modelsPath + "\\data";

    // sets the texture path
    m_proxiesPath = m_modelsPath + "\\data\\proxy";

    // Creates the target folders
    Pathname::CreateFolder( m_texturesPath );
    Pathname::CreateFolder( m_proxiesPath );

    // *************************************
    // TODO:
    //
    // Clean the folders if already existing
    // *************************************

    // sets the models name prefix
    try 
    {
      m_namesPrefix = params.GetTextValue( NAME_PREFIX );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_namesPrefix = "";
    }

    // try to load the stair types library
    try 
    {
      m_libraryFilename = params.GetTextValue( LIBRARY_FILE );
      m_libraryFilename = m_libraryFilename;
      m_stairTypesLibrary.SetModelsPath( m_modelsPath );
      if ( !m_stairTypesLibrary.LoadFromCfgFile( m_libraryFilename, m_logFile ) ) 
      {
        LOGF( Error ) << "Error loading library config file. Please, verify the data.";
        m_logFile << "WARNING: Unable to load data from \"" << m_libraryFilename << "\"." << endl;
        m_logFile << "         No stair type defined." << endl << endl;
      }
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      LOGF( Error ) << "Missing Library config file name.";
      m_logFile << "WARNING: Missing \"" << LIBRARY_FILE << "\" field in class Parameters." << endl;
      m_logFile << "         No stair type defined." << endl << endl;
    }

    if ( m_stairTypesLibrary.Size() == 0 )
    {
      LOGF( Error ) << "No stair type defined.";
      m_logFile << "WARNING: No stair type defined into \"" << m_libraryFilename << "\"." << endl << endl;
    }
    else
    {
      m_logFile << "-------------------------------------------------------------" << endl;
      m_logFile << "Loaded stair types:" << endl;
      m_logFile << "-------------------------------------------------------------" << endl;
      for ( size_t i = 0, cntI = m_stairTypesLibrary.Size(); i < cntI; ++i )
      {
        m_logFile << m_stairTypesLibrary.Get( i )->GetName() << endl;
      }
      m_logFile << "-------------------------------------------------------------" << endl << endl;
    }

    // try to load the building types library
    try 
    {
      m_libraryFilename = params.GetTextValue( LIBRARY_FILE );
      m_buildingTypesLibrary.SetModelsPath( m_modelsPath );
      m_buildingTypesLibrary.SetStairTypeLibrary( &m_stairTypesLibrary );
      if ( !m_buildingTypesLibrary.LoadFromCfgFile( m_libraryFilename, m_logFile ) ) 
      {
        m_logFile << "Module aborted." << endl;
        m_aborted = true;
        return;
      }
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      LOGF( Error ) << "Missing Library config file name.";
      m_logFile << "ERROR: Missing or wrong \"" << LIBRARY_FILE << "\" field in class Parameters." << endl ;
      m_logFile << "Module aborted." << endl;
      m_aborted = true;
    }

    m_logFile << "Models will be saved in:   \"" << m_modelsPath << "\"" << endl;
    m_logFile << "Textures will be saved in: \"" << m_texturesPath << "\"" << endl;
    m_logFile << "Proxies will be saved in:  \"" << m_proxiesPath << "\"" << endl << endl;

    m_logFile << "-------------------------------------------------------------" << endl;
    m_logFile << "Loaded building types:" << endl;
    m_logFile << "-------------------------------------------------------------" << endl;
    for ( size_t i = 0, cntI = m_buildingTypesLibrary.Size(); i < cntI; ++i )
    {
      m_logFile << m_buildingTypesLibrary.Get( i )->GetName() << endl;
    }
    m_logFile << "-------------------------------------------------------------" << endl << endl;

    // gets the random seed
    try
    {
      m_randomSeed = params.GetIntValue( RANDOM_SEED );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_logFile << "WARNING: Missing \"" << RANDOM_SEED << "\" field in class Parameters." << endl;
      m_randomSeed = 1;
      m_logFile << "         Assuming \"" << m_randomSeed << "\" as default." << endl << endl;
    }

    // gets the similarity factor
    try
    {
      m_similarityFactor = static_cast< Float >( params.GetFloatValue( SIMILARITY_FACTOR ) );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_logFile << "WARNING: Missing \"" << SIMILARITY_FACTOR << "\" field in class Parameters." << endl;
      m_similarityFactor = CGConsts::ONE;
      m_logFile << "         Assuming \"" << m_similarityFactor << "\" as default." << endl << endl;
    }

    m_logFile << "Using randomSeed       = " << m_randomSeed << endl;
    m_logFile << "Using similarityFactor = " << m_similarityFactor << endl << endl;
    srand( m_randomSeed );
  } // end of "global" settings

  // this part apply to all the shapes coming from the shapefile
  LOGF( Info ) << "Working on shape: " << m_shapesCount;

  // gets shape and its properties
  Shapes::IShape* curShape = const_cast< Shapes::IShape* >( cmdIfc->GetActiveShape() );

  // shape validation
  // checks for multipart shapes
  if ( curShape->GetPartCount() > 1 )
  {
    LOGF( Error ) << "Edifici module accepts only shapefiles containing non-multipart polygons.";
    m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
    m_logFile << "Edifici module accepts only shapefiles containing non-multipart polygons." << endl;
    return;
  }

  // checks for known shapes
  const IShapeExtra* extra = curShape->GetInterface< IShapeExtra >();
  if ( !extra ) 
  {
    LOGF( Error ) << "Found unknown shape in shapefile. Cannot get type of that shape.";
    m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
    m_logFile << "Cannot get type of that shape." << endl;
    return;
  }

  // checks for polygons 
  String shapeType = extra->GetShapeTypeName();
  if ( shapeType != String( SHAPE_NAME_POLYGON ) ) 
  {
    LOGF( Error ) << "Edifici module accepts only shapefiles containing non-multipart polygons.";
    m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
    m_logFile << "Edifici module accepts only shapefiles containing non-multipart polygons." << endl;
    return;
  }

  // checks for well-formed polygons 
  if ( curShape->GetVertexCount() < 3 )
  {
    LOGF( Error ) << "Found polygon with less then 3 vertices.";
    m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
    m_logFile << "Polygon with less than 3 vertices." << endl;
    return;
  }

  // gets building type from dbf
  String type = "";
  try 
  {
    type = params.GetTextValue( TYPE );
    if ( type == "Random" )
    {
      size_t librarySize = m_buildingTypesLibrary.Size();
      size_t sel = static_cast< size_t >( floor( static_cast< double >(rand()) / (RAND_MAX + 1) * librarySize) );
      if ( sel == 0 && librarySize > 1 ) { ++sel; }
      type = m_buildingTypesLibrary.Get( sel )->GetName();
    }
  }
  catch ( LandBuilder2::ParamParserException& e ) 
  {
    type = m_buildingTypesLibrary.Get( 0 )->GetName();
    LOGF( Error ) << "Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue();
    m_logFile << "WARNING: Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue() << endl;
    m_logFile << "         Shape n: " << m_shapesCount << " misses the \"" << TYPE << "\" field in the dbf." << endl;
    m_logFile << "         \"" << type << "\" assumed as type for this shape." << endl << endl;
  }

  // gets number of floors from dbf
  int numFloors = -1;
  try 
  {
    numFloors = params.GetIntValue( NUMFLOORS );
    if ( numFloors == 0 )
    {
      numFloors = -1;
      LOGF( Error ) << "Wrong value in dbf field \"" << NUMFLOORS << "\"";
      m_logFile << "WARNING: Wrong value in dbf field \"" << NUMFLOORS << "\"" << endl;
      m_logFile << "         Shape n: " << m_shapesCount << " has a wrong value in the \"" << NUMFLOORS << "\" field in the dbf." << endl;
      m_logFile << "         This shape will have a random number of floors." << endl << endl;
    }
  }
  catch ( LandBuilder2::ParamParserException& e ) 
  {
    numFloors = -1;
    LOGF( Error ) << "Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue();
    m_logFile << "WARNING: Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue() << endl;
    m_logFile << "         Shape n: " << m_shapesCount << " misses the \"" << NUMFLOORS << "\" field in the dbf." << endl;
    m_logFile << "         This shape will have a random number of floors." << endl << endl;
  }

  // gets if enterable from dbf
  int intEnterable = 0;
  try 
  {
    intEnterable = params.GetIntValue( ENTERABLE );
    if ( intEnterable != 0 && intEnterable != 1 && intEnterable != -1 )
    {
      intEnterable = 0;
      LOGF( Error ) << "Wrong value in dbf field \"" << ENTERABLE << "\"";
      m_logFile << "WARNING: Wrong value in dbf field \"" << ENTERABLE << "\"" << endl;
      m_logFile << "         Shape n: " << m_shapesCount << " has a wrong value in the \"" << ENTERABLE << "\" field in the dbf." << endl;
      m_logFile << "         This shape will be non enterable." << endl << endl;
    }
  }
  catch ( LandBuilder2::ParamParserException& e ) 
  {
    intEnterable = 0;
    LOGF( Error ) << "Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue();
    m_logFile << "WARNING: Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue() << endl;
    m_logFile << "         Shape n: " << m_shapesCount << " misses the \"" << ENTERABLE << "\" field in the dbf." << endl;
    m_logFile << "         This shape will be non enterable." << endl << endl;
  }

  bool enterable;
  if ( intEnterable == - 1 ) // random
  {
    enterable = (rand() > RAND_MAX * CGConsts::HALF) ? true : false;
  }
  else
  {	
    enterable = (intEnterable == 1) ? true : false;
  }

  // gets if rectify corners from dbf
  int intRectifyCorners = 0;
  try 
  {
    intRectifyCorners = params.GetIntValue( RECTIFYCORNERS );
    if ( intRectifyCorners != 0 && intRectifyCorners != 1)
    {
      intRectifyCorners = 0;
      LOGF( Error ) << "Wrong value in dbf field \"" << RECTIFYCORNERS << "\"";
      m_logFile << "WARNING: Wrong value in dbf field \"" << RECTIFYCORNERS << "\"" << endl;
      m_logFile << "         Shape n: " << m_shapesCount << " has a wrong value in the \"" << RECTIFYCORNERS << "\" field in the dbf." << endl;
      m_logFile << "         The corners of this shape will be non rectified." << endl << endl;
    }
  }
  catch ( LandBuilder2::ParamParserException& e ) 
  {
    intRectifyCorners = 0;
    LOGF( Error ) << "Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue();
    m_logFile << "WARNING: Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue() << endl;
    m_logFile << "         Shape n: " << m_shapesCount << " misses the \"" << RECTIFYCORNERS << "\" field in the dbf." << endl;
    m_logFile << "         The corners of this shape will be non rectified." << endl << endl;
  }

  bool rectifyCorners = (intRectifyCorners == 1) ? true : false;

  // checks if type is in library
  int libIndex = m_buildingTypesLibrary.FindByName( type );
  if ( libIndex == -1 )
  {
    LOGF( Error ) << "Found invalid building type. Using default.";
    m_logFile << "WARNING: Shape n: " << m_shapesCount << " has in the dbf an unknown type." << endl;
    m_logFile << "         \"" << DEFAULT_BUILDING_TYPE_NAME << "\" assumed as type for this shape." << endl << endl;
    libIndex = 0;
  }

  // copies shape to polygon 
  CGPolygon2 footprint;
  for ( size_t i = 0, cntI = curShape->GetVertexCount(); i < cntI; ++i )
  {
    const Shapes::DVertex& v = curShape->GetVertex( i );
    footprint.AppendVertex( v.x, v.y );
  }

  // pre-processes footprint
  footprint.RemoveConsecutiveDuplicatedVertices( TOLERANCE_CM );

  if ( footprint.VerticesCount() < 3 )
  {
    LOGF( Error ) << "Skipped shape: found degenerate polygon (vertices count).";
    m_logFile << "WARNING: Skipped shape n: " << m_shapesCount << " because reduced to a degenerate polygon (vertices count)." << endl << endl;
    return;
  }

  Float area = footprint.Area();
  if ( area < TOLERANCE_DM )
  {
    LOGF( Error ) << "Skipped shape: found degenerate polygon (area).";
    m_logFile << "WARNING: Skipped shape n: " << m_shapesCount << " because reduced to a degenerate polygon (area)." << endl << endl;
    return;
  }

  if ( area > FOOTPRINT_AREA_LIMIT && enterable )
  {
    m_logFile << "WARNING: Shape n: " << m_shapesCount << " is too big to be enterable." << endl;
    m_logFile << "         It will be converted to non-enterable." << endl << endl;
    enterable = false;
  }

  CGRectangle2 bbox = footprint.MinimumBoundingBox();
  if ( bbox.GetWidth() > FOOTPRINT_DIMENSION_LIMIT || bbox.GetHeight() > FOOTPRINT_DIMENSION_LIMIT )
  {
    m_logFile << "WARNING: Shape n: " << m_shapesCount << " is too big to be enterable." << endl;
    m_logFile << "         It will be converted to non-enterable." << endl << endl;
    enterable = false;
  }

  // creates a temporary template
  CBuildingTemplate buildingTemplate( m_buildingTypesLibrary.Get( libIndex ),
                                      footprint, numFloors, enterable,
                                      rectifyCorners, m_similarityFactor, m_namesPrefix, true, &m_logFile ); 

  // searches if the template is already in library
  CBuildingTemplate* templ = m_buildingTemplatesLibrary.Find( buildingTemplate );
  if ( !templ )
  {
    // if not add it to the library
    templ = m_buildingTemplatesLibrary.Append( buildingTemplate );
  }
  assert( templ );

  footprint.SetVerticesOrderToCCW();
  const CGPoint2& barycenter = footprint.Barycenter();
  CGRectangle2 minBoundingBox = footprint.MinimumBoundingBox();
  Float orientation = minBoundingBox.GetOrientation();
  const CGPoint2& center = minBoundingBox.GetCenter();

  footprint.Translate( -barycenter.GetX(), -barycenter.GetY() );
  footprint.RotateAroundBarycenter( -orientation );

  if ( !footprint.Equals( templ->GetFootprint(), 0.05 ) ) 
  { 
    orientation += CGConsts::PI; 
  }

  const CGPoint2& position = center;

  CBuilding building( templ, position, orientation );
  templ->IncreaseReferencesCount();

  Vector3D outPosition = Vector3D( position.GetX(),
                                               0.0,
                                   position.GetY() );

  Matrix4D mTranslation = Matrix4D( MTranslation, outPosition );
  Matrix4D mRotation    = Matrix4D( MRotationY, orientation );

  Matrix4D transform = mTranslation * mRotation;
  cmdIfc->GetObjectMap()->PlaceObject( transform, templ->Name(), 0 );
}

//-----------------------------------------------------------------------------

void CEdifici::OnEndPass( IMainCommands* cmdIfc )
{
  if ( m_aborted ) { return; }

  LOGF( Error ) << "Created " << m_buildingTemplatesLibrary.Size() <<  " building templates.";
  LOGF( Error ) << "------------------------------------------";
  m_logFile << "Processed " << m_shapesCount <<  " shapes." << endl;
  m_logFile << "Created " << m_buildingTemplatesLibrary.Size() <<  " building templates." << endl;
  m_logFile << "-------------------------------------------------------------" << endl;
  size_t totalRefCount = 0;

  DWORD generationTime = GetTickCount();

  for ( size_t i = 0, cntI = m_buildingTemplatesLibrary.Size(); i < cntI; ++i )
  {
    CBuildingTemplate* templ = m_buildingTemplatesLibrary.Get( i );

    LOGF( Error ) << "Exporting template: " << templ->Name() << " (" << templ->GetBuildingType()->GetName() << ")";
    if ( templ )
    {
      if ( !templ->ExportToP3D( m_modelsPath ) )
      {
        m_logFile << "ERROR: Unable to export the template: " << templ->Name() << endl << endl;
      }
      else
      {
        m_logFile << templ->Name() << " [" << templ->GetBuildingType()->GetName() << "] (" << templ->GetReferencesCount() << ")" << endl;
        templ->ResetReferencesCount();
      }
      m_logFile << "-------------------------------------------------------------" << endl;
    }
  }

  m_logFile << endl;

  DWORD exportTime = GetTickCount();

  m_logFile << "-------------------------------------------------------------" << endl;
  m_logFile << "Time statistics" << endl;
  m_logFile << "-------------------------------------------------------------" << endl;
  m_logFile << "Building generation: " << generationTime - m_startingTime << " ms" << endl;
  m_logFile << "Building export:     " << exportTime - generationTime << " ms" << endl;
  m_logFile << "-------------------------------------------------------------" << endl << endl;
}

//-----------------------------------------------------------------------------
