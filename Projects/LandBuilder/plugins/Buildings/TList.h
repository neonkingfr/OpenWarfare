//-----------------------------------------------------------------------------
// File: TList.h
//
// Desc: a templated list
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"

//-----------------------------------------------------------------------------

template < class T >
class TList
{
protected:
	vector< T* > m_elements;

public:
	TList();
	TList( const TList< T >& other );
	virtual ~TList();

	TList< T >& operator = ( const TList< T >& other );

	void Clear();
	T* Append( const T& element );
	size_t Size() const;
	void Remove( const T* pElement );
	void RemoveAt( size_t index );
	T* Find( const T& t ) const;
	int FindIndex( const T& t ) const;

	const T* Get( size_t index ) const;
	T* Get( size_t index );

private:
	void CopyFrom( const TList< T >& other );
};

//-----------------------------------------------------------------------------

template < class T >
class TKeyList
{
	// here using plain std::string because of problem between RString and std::map
	map< string, T* > m_elements;

public:
	TKeyList();
	TKeyList( const TKeyList< T >& other );
	virtual ~TKeyList();

	TKeyList< T >& operator = ( const TKeyList< T >& other );

	void Clear();
	T* Append( const String& key, const T& element );
	size_t Size() const;

	const T* Get( const String& key ) const;
	T* Get( const String& key );

	// Comparison
	// {
	bool operator == ( const TKeyList< T >& other ) const;
	bool operator != ( const TKeyList< T >& other ) const;
	// }

private:
	void CopyFrom( const TKeyList< T >& other );
};

//-----------------------------------------------------------------------------

#include "TList.inl"

//-----------------------------------------------------------------------------
