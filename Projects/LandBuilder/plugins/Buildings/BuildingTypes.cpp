//-----------------------------------------------------------------------------
// File: BuildingTypes.cpp
//
// Desc: classes related to building types 
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "BuildingTypes.h"

//-----------------------------------------------------------------------------

CBuildingType::CBuildingType( const String& name, bool usesInterFloors,
							  bool usesWindows, bool usesDoors,
							  bool usesStairs,
							  Float floorHeight, Float interFloorHeight,
							  Float roofHeight, Float wallsWidth, Float doorsWidth,
							  Float doorsHeight, Float windowsWidth,
							  Float windowsHeight, EBuildingRoofsType roofType,
							  const CStairType* pStairType, size_t maxNumFloor )
: m_name( name )
, m_usesInterFloors( usesInterFloors )
, m_usesWindows( usesWindows )
, m_usesDoors( usesDoors )
, m_usesStairs( usesStairs )
, m_floorHeight( floorHeight )
, m_interFloorHeight( interFloorHeight )
, m_roofHeight( roofHeight )
, m_wallsWidth( wallsWidth )
, m_doorsWidth( doorsWidth )
, m_doorsHeight( doorsHeight )
, m_windowsWidth( windowsWidth )
, m_windowsHeight( windowsHeight )
, m_roofType( roofType )
, m_pStairType( pStairType )
, m_maxNumFloors( maxNumFloor )
{
}

//-----------------------------------------------------------------------------

CBuildingType::CBuildingType( const CBuildingType& other )
: m_name( other.m_name )
, m_usesInterFloors( other.m_usesInterFloors )
, m_usesWindows( other.m_usesWindows )
, m_usesDoors( other.m_usesDoors )
, m_usesStairs( other.m_usesStairs )
, m_floorHeight( other.m_floorHeight )
, m_interFloorHeight( other.m_interFloorHeight )
, m_roofHeight( other.m_roofHeight )
, m_wallsWidth( other.m_wallsWidth )
, m_doorsWidth( other.m_doorsWidth )
, m_doorsHeight( other.m_doorsHeight )
, m_windowsWidth( other.m_windowsWidth )
, m_windowsHeight( other.m_windowsHeight )
, m_roofType( other.m_roofType )
, m_pStairType( other.m_pStairType )
, m_maxNumFloors( other.m_maxNumFloors )
, m_textures( other.m_textures )
, m_proxies( other.m_proxies )
{
}

//-----------------------------------------------------------------------------

const String& CBuildingType::GetName() const
{
	return (m_name);
}

//-----------------------------------------------------------------------------

bool CBuildingType::UsesInterFloors() const
{
	return (m_usesInterFloors);
}

//-----------------------------------------------------------------------------

bool CBuildingType::UsesWindows() const
{
	return (m_usesWindows);
}

//-----------------------------------------------------------------------------

bool CBuildingType::UsesDoors() const
{
	return (m_usesDoors);
}

//-----------------------------------------------------------------------------

bool CBuildingType::UsesStairs() const
{
	return (m_usesStairs);
}

//-----------------------------------------------------------------------------

Float CBuildingType::GetFloorHeight() const
{
	return (m_floorHeight);
}

//-----------------------------------------------------------------------------

Float CBuildingType::GetInterFloorHeight() const
{
	return (m_interFloorHeight);
}

//-----------------------------------------------------------------------------

Float CBuildingType::GetRoofHeight() const
{
	return (m_roofHeight);
}

//-----------------------------------------------------------------------------

Float CBuildingType::GetWallsWidth() const
{
	return (m_wallsWidth);
}

//-----------------------------------------------------------------------------

Float CBuildingType::GetDoorsWidth() const
{
	return (m_doorsWidth);
}

//-----------------------------------------------------------------------------

Float CBuildingType::GetDoorsHeight() const
{
	return (m_doorsHeight);
}

//-----------------------------------------------------------------------------

Float CBuildingType::GetWindowsWidth() const
{
	return (m_windowsWidth);
}

//-----------------------------------------------------------------------------

Float CBuildingType::GetWindowsHeight() const
{
	return (m_windowsHeight);
}

//-----------------------------------------------------------------------------

const EBuildingRoofsType& CBuildingType::GetRoofType() const
{
	return (m_roofType);
}

//-----------------------------------------------------------------------------

const CStairType* CBuildingType::GetStairType() const
{
	return (m_pStairType);
}

//-----------------------------------------------------------------------------

size_t CBuildingType::GetMaxNumFloors() const
{
	return (m_maxNumFloors);
}

//-----------------------------------------------------------------------------

const CTexture* CBuildingType::GetTexture( const String& key ) const
{
	return (m_textures.Get( key ));
}

//-----------------------------------------------------------------------------

const CProxy* CBuildingType::GetProxy( const String& key ) const
{
	return (m_proxies.Get( key ));
}

//-----------------------------------------------------------------------------

void CBuildingType::AddTexture( const String& key, const CTexture& texture )
{
	m_textures.Append( key, texture );
}

//-----------------------------------------------------------------------------

void CBuildingType::AddProxy( const String& key, const CProxy& proxy )
{
	m_proxies.Append( key, proxy );
}

//-----------------------------------------------------------------------------

bool CBuildingType::operator == ( const CBuildingType& other ) const
{
	if ( this == &other ) { return (true); }

	if ( m_name             != other.m_name )             { return (false); }
	if ( m_usesInterFloors  != other.m_usesInterFloors )  { return (false); }
	if ( m_usesWindows      != other.m_usesWindows )      { return (false); }
	if ( m_usesDoors        != other.m_usesDoors )        { return (false); }
	if ( m_usesStairs       != other.m_usesStairs )       { return (false); }
	if ( m_floorHeight      != other.m_floorHeight )      { return (false); }
	if ( m_interFloorHeight != other.m_interFloorHeight ) { return (false); }
	if ( m_roofHeight       != other.m_roofHeight )       { return (false); }
	if ( m_wallsWidth       != other.m_wallsWidth )       { return (false); }
	if ( m_doorsWidth       != other.m_doorsWidth )       { return (false); }
	if ( m_doorsHeight      != other.m_doorsHeight )      { return (false); }
	if ( m_windowsWidth     != other.m_windowsWidth )     { return (false); }
	if ( m_windowsHeight    != other.m_windowsHeight )    { return (false); }
	if ( m_roofType         != other.m_roofType )         { return (false); }
	if ( m_pStairType       != other.m_pStairType )       { return (false); }
	if ( m_maxNumFloors     != other.m_maxNumFloors )     { return (false); }
	if ( m_textures         != other.m_textures )         { return (false); }
	if ( m_proxies          != other.m_proxies )          { return (false); }

	return (true);
}

//-----------------------------------------------------------------------------

bool CBuildingType::operator != ( const CBuildingType& other ) const
{
	return (!(this->operator==( other )));
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CBuildingTypesLibrary::CBuildingTypesLibrary( const String& modelsPath,
											  const CStairTypesLibrary* pStairTypesLibrary )
: m_modelsPath( modelsPath )
, m_pStairTypesLibrary( pStairTypesLibrary )
{
}

//-----------------------------------------------------------------------------

CBuildingTypesLibrary::~CBuildingTypesLibrary()
{
}

//-----------------------------------------------------------------------------

void CBuildingTypesLibrary::SetModelsPath( const String& modelsPath )
{
	m_modelsPath = modelsPath;
}

//-----------------------------------------------------------------------------

void CBuildingTypesLibrary::SetStairTypeLibrary( const CStairTypesLibrary* pStairTypesLibrary )
{
	m_pStairTypesLibrary = pStairTypesLibrary;
}

//-----------------------------------------------------------------------------

bool CBuildingTypesLibrary::LoadFromCfgFile( const String& filename, ofstream& logFile )
{
	// clears the list
	Clear();

	String path = m_modelsPath;
	if ( path[path.GetLength() - 1] == '\\' )
	{
		path = path.Substring( 0, path.GetLength() - 1 );
	}
	String texturesPath = path + "\\Data\\";
	String proxyPath    = path + "\\Data\\Proxy\\";

	// adds the default building type
	SetDefault( texturesPath );

	// adds the buildings types from the config file
	ifstream in( filename, std::ios::in );
	if ( in.fail() )
	{
    LOGF( Error ) << "Failed to open config file.";
    logFile << "ERROR: Failed to open the file \"" << filename << "\"." << endl;
		return (false);
	}
	else
	{
		in.close();
	}

	ParamFile parFile;
	LSError err = parFile.Parse( filename );

	if ( err != LSOK ) { return (false); }

	ParamEntryPtr entry = parFile.FindEntry( "BuildingTypesLibrary" );
	if ( entry.IsNull() )
	{
		LOGF( Error ) << "Cannot find class 'BuildingTypesLibrary'";
		logFile << "ERROR: Unable to find class \"BuildingTypesLibrary\" inside the file \"" << filename << "\"." << endl;
		return (false);
	}
	if ( !entry->IsClass() ) 
	{
		LOGF( Error ) << "'BuildingTypesLibrary' must be a class";
		logFile << "ERROR: entry \"BuildingTypesLibrary\" inside the file \"" << filename << "\" must be a class." << endl;
		return (false);
	}

	ParamClassPtr buildingClass = entry->GetClassInterface();
	size_t i = 1;
	char buff[20];

  do
	{
		sprintf( buff, "Building%04d", i );
		entry = buildingClass->FindEntry( buff );

		if ( entry && entry->IsClass() )
		{
			ParamClassPtr item = entry->GetClassInterface();

			// name
			String name = buff;
			entry = item->FindEntry( BUILDING_NAME );            
			if ( entry.IsNull() ) 
			{
				LOGF( Error ) << "Missing " << buff << "::" << BUILDING_NAME << " field. Using default (" << name << ").";
				logFile << "WARNING: Missing " << buff << "::" << BUILDING_NAME << " field. Using default (" << name << ")." << endl << endl;
			}
			else
			{
				name = entry->GetValue();
			}

			// uses inter-floors
			bool usesInterFloors = LoadBoolFromCfgFile( buff, item, BUILDING_USESINTERFLOORS, true, logFile );

			// uses windows
			bool usesWindows = LoadBoolFromCfgFile( buff, item, BUILDING_USESWINDOWS, true, logFile );

			// uses doors
			bool usesDoors = LoadBoolFromCfgFile( buff, item, BUILDING_USESDOORS, true, logFile );

			// uses stairs
			bool usesStairs = LoadBoolFromCfgFile( buff, item, BUILDING_USESSTAIRS, true, logFile );

			// roof type
			EBuildingRoofsType roofType = BRT_FLAT;
			entry = item->FindEntry( BUILDING_ROOF_TYPE );            
			if ( entry.IsNull() ) 
			{
				LOGF( Error ) << "Missing " << buff << "::" << BUILDING_ROOF_TYPE << " field. Using default (" << roofType << ").";
				logFile << "WARNING: Missing " << buff << "::" << BUILDING_ROOF_TYPE << " field. Using default (" << roofType << ")." << endl << endl;
			}
			else
			{
				int intRoofType = *entry;
				if ( intRoofType < BRT_COUNT )
				{
					roofType = static_cast< EBuildingRoofsType >( intRoofType );
				}
				else
				{
					roofType = BRT_FLAT;
					LOGF( Error ) << "Wrong value in " << buff << "::" << BUILDING_ROOF_TYPE << " field. Using default (" << roofType << ").";
					logFile << "WARNING: Wrong value in " << buff << "::" << BUILDING_ROOF_TYPE << " field. Using default (" << roofType << ")." << endl << endl;
				}
			}

			// stairType
			const CStairType* pStairType;
			if ( m_pStairTypesLibrary )
			{
				String strStairType = "";
				entry = item->FindEntry( BUILDING_STAIR_TYPE );            
				if ( entry.IsNull() ) 
				{
					LOGF( Error ) << "Missing " << buff << "::" << BUILDING_STAIR_TYPE << " field. No stair type will be associated to this building type.";
					logFile << "WARNING: Missing " << buff << "::" << BUILDING_STAIR_TYPE << " field. No stair type will be associated to this building type." << endl << endl;
				}
				else
				{
					strStairType = entry->GetValue();
				}
				int index = m_pStairTypesLibrary->FindByName( strStairType );
				if ( index == -1 )
				{
					pStairType = NULL;
					logFile << "WARNING: No match found for the specified " << buff << "::" << BUILDING_STAIR_TYPE << " field. (" << strStairType << ")" << endl;
					logFile << "         No stair type will be associated to this building type." << endl << endl;
				}
				else
				{
					pStairType = m_pStairTypesLibrary->Get( index );
				}
			}
			else
			{
				pStairType = NULL;
			}

			// floor height
			Float floorHeight = LoadDoubleFromCfgFile( buff, item, BUILDING_FLOOR_HEIGHT, DEFAULT_BUILDING_FLOOR_HEIGHT, CGConsts::ZERO, CGConsts::MAX_REAL, logFile );

			// inter floor height
			Float interFloorHeight;
			if ( usesInterFloors )
			{
				interFloorHeight = LoadDoubleFromCfgFile( buff, item, BUILDING_INTERFLOOR_HEIGHT, DEFAULT_BUILDING_INTERFLOOR_HEIGHT, CGConsts::ZERO, CGConsts::MAX_REAL, logFile );
			}
			else
			{
				interFloorHeight = DEFAULT_BUILDING_INTERFLOOR_HEIGHT;
			}

			// roof height
			Float roofHeight = LoadDoubleFromCfgFile( buff, item, BUILDING_ROOF_HEIGHT, DEFAULT_BUILDING_ROOF_HEIGHT, CGConsts::ZERO, CGConsts::MAX_REAL, logFile );

			// walls width
			Float wallsWidth = LoadDoubleFromCfgFile( buff, item, BUILDING_WALLS_WIDTH, DEFAULT_BUILDING_WALLS_WIDTH, CGConsts::ZERO, CGConsts::MAX_REAL, logFile );

			// doors width
			Float doorsWidth = LoadDoubleFromCfgFile( buff, item, BUILDING_DOORS_WIDTH, DEFAULT_BUILDING_DOORS_WIDTH, CGConsts::ZERO, CGConsts::MAX_REAL, logFile );

			// doors height
			Float doorsHeight = LoadDoubleFromCfgFile( buff, item, BUILDING_DOORS_HEIGHT, DEFAULT_BUILDING_DOORS_HEIGHT, CGConsts::ZERO, CGConsts::MAX_REAL, logFile );

			// windows width
			Float windowsWidth = LoadDoubleFromCfgFile( buff, item, BUILDING_WINDOWS_WIDTH, DEFAULT_BUILDING_WINDOWS_WIDTH, CGConsts::ZERO, CGConsts::MAX_REAL, logFile );

			// windows height
			Float windowsHeight = LoadDoubleFromCfgFile( buff, item, BUILDING_WINDOWS_HEIGHT, DEFAULT_BUILDING_WINDOWS_HEIGHT, CGConsts::ZERO, CGConsts::MAX_REAL, logFile );

			// max number of floors
			size_t maxNumFloors = DEFAULT_BUILDING_FLOORS_MAXNUM;
			entry = item->FindEntry( BUILDING_FLOORS_MAXNUM );            
			if ( entry.IsNull() ) 
			{
				LOGF( Error ) << "Missing " << buff << "::" << BUILDING_FLOORS_MAXNUM << " field. Using default (" << maxNumFloors << ").";
				logFile << "WARNING: Missing " << buff << "::" << BUILDING_FLOORS_MAXNUM << " field. Using default (" << maxNumFloors << ")." << endl << endl;
			}
			else
			{
				int intMaxNumFloors = *entry;
				if ( (0 < intMaxNumFloors) && (intMaxNumFloors <= DEFAULT_BUILDING_FLOORS_MAXNUM) )
				{
					maxNumFloors = static_cast< size_t >( intMaxNumFloors );
				}
				else
				{
					maxNumFloors = DEFAULT_BUILDING_FLOORS_MAXNUM;
					LOGF( Error ) << "Wrong value in " << buff << "::" << BUILDING_FLOORS_MAXNUM << " field. Using default (" << maxNumFloors << ").";
					logFile << "WARNING: Wrong value in " << buff << "::" << BUILDING_FLOORS_MAXNUM << " field. Using default (" << maxNumFloors << ")." << endl << endl;
				}
			}

			// floor external walls color 
			CColor floorExWallColor = LoadColorFromCfgFile( buff, item, 
															DEFAULT_BUILDING_FLOOR_EXWALL_COLOR, 
															BUILDING_FLOOR_EXWALL_COLOR,
															logFile );

			// floor external walls texture
			String floorExWallTex = LoadTextureFromCfgFile( buff, item, 
															BUILDING_FLOOR_EXWALL_TEX, 
															floorExWallColor, logFile );

			// floor internal walls color 
			CColor floorInWallColor = LoadColorFromCfgFile( buff, item, 
															DEFAULT_BUILDING_FLOOR_INWALL_COLOR, 
															BUILDING_FLOOR_INWALL_COLOR,
															logFile );

			// floor internal walls texture
			String floorInWallTex = LoadTextureFromCfgFile( buff, item, 
															BUILDING_FLOOR_INWALL_TEX, 
															floorInWallColor, logFile );

			// interfloor walls color 
			CColor interFloorWallColor = LoadColorFromCfgFile( buff, item,
															   DEFAULT_BUILDING_INTERFLOOR_WALL_COLOR,
															   BUILDING_INTERFLOOR_WALL_COLOR,
															   logFile );

			// interfloor walls texture
			String interFloorWallTex = LoadTextureFromCfgFile( buff, item, 
															   BUILDING_INTERFLOOR_WALL_TEX, 
															   interFloorWallColor, logFile );

			// roof top color 
			CColor roofTopColor = LoadColorFromCfgFile( buff, item, 
														DEFAULT_BUILDING_ROOF_TOP_COLOR,
													    BUILDING_ROOF_TOP_COLOR, 
														logFile );

			// roof top texture
			String roofTopTex = LoadTextureFromCfgFile( buff, item, BUILDING_ROOF_TOP_TEX, 
														roofTopColor, logFile );

			// roof slope color 
			CColor roofSlopeColor = LoadColorFromCfgFile( buff, item, 
														  DEFAULT_BUILDING_ROOF_SLOPE_COLOR,
														  BUILDING_ROOF_SLOPE_COLOR, 
														  logFile );

			// roof slope texture
			String roofSlopeTex = LoadTextureFromCfgFile( buff, item, BUILDING_ROOF_SLOPE_TEX, 
														  roofSlopeColor, logFile );

			// window color 
			CColor windowColor = LoadColorFromCfgFile( buff, item, 
													   DEFAULT_BUILDING_WINDOW_COLOR,
													   BUILDING_WINDOW_COLOR, 
													   logFile );

			// window texture
			String windowTex = LoadTextureFromCfgFile( buff, item, BUILDING_WINDOW_TEX, 
													   windowColor, logFile );

			// door color 
			CColor doorColor = LoadColorFromCfgFile( buff, item, 
													 DEFAULT_BUILDING_DOOR_COLOR,
													 BUILDING_DOOR_COLOR, 
													 logFile );

			// door texture
			String doorTex = LoadTextureFromCfgFile( buff, item, BUILDING_DOOR_TEX, 
													 doorColor, logFile );

			// ceiling color 
			CColor ceilColor = LoadColorFromCfgFile( buff, item, 
													 DEFAULT_BUILDING_CEILING_COLOR,
													 BUILDING_CEILING_COLOR, 
													 logFile );

			// ceiling texture
			String ceilTex = LoadTextureFromCfgFile( buff, item, BUILDING_CEILING_TEX, 
													 ceilColor, logFile );

			// pavement color 
			CColor pavColor = LoadColorFromCfgFile( buff, item, 
													DEFAULT_BUILDING_PAVEMENT_COLOR,
													BUILDING_PAVEMENT_COLOR, 
													logFile );

			// pavement texture
			String pavTex = LoadTextureFromCfgFile( buff, item, BUILDING_PAVEMENT_TEX, 
													pavColor, logFile );

			// basement color 
			CColor baseColor = LoadColorFromCfgFile( buff, item, 
													 DEFAULT_BUILDING_BASEMENT_COLOR,
													 BUILDING_BASEMENT_COLOR, 
													 logFile );

			// basement texture
			String baseTex = LoadTextureFromCfgFile( buff, item, BUILDING_BASEMENT_TEX, 
													 baseColor, logFile );

			CBuildingType* pType = new CBuildingType( name, usesInterFloors,
													  usesWindows, usesDoors,
													  usesStairs,
													  floorHeight, interFloorHeight,
													  roofHeight, wallsWidth, doorsWidth,
													  doorsHeight, windowsWidth,
													  windowsHeight, roofType, 
													  pStairType, maxNumFloors );

			// window proxy
			String windowProxy = LoadProxyNameFromCfgFile( buff, item, BUILDING_WINDOW_PROXY, logFile );

			// door proxy
			String doorProxy = LoadProxyNameFromCfgFile( buff, item, BUILDING_DOOR_PROXY, logFile );

			pType->AddTexture( BUILDING_FLOOR_EXWALL_TEX,    CTexture( floorExWallTex,    texturesPath, floorExWallColor ) );
			pType->AddTexture( BUILDING_FLOOR_INWALL_TEX,    CTexture( floorInWallTex,    texturesPath, floorInWallColor ) );
			pType->AddTexture( BUILDING_INTERFLOOR_WALL_TEX, CTexture( interFloorWallTex, texturesPath, interFloorWallColor ) );
			pType->AddTexture( BUILDING_ROOF_TOP_TEX,        CTexture( roofTopTex,        texturesPath, roofTopColor ) );
			pType->AddTexture( BUILDING_ROOF_SLOPE_TEX,      CTexture( roofSlopeTex,      texturesPath, roofSlopeColor ) );
			pType->AddTexture( BUILDING_WINDOW_TEX,          CTexture( windowTex,         texturesPath, windowColor ) );
			pType->AddTexture( BUILDING_DOOR_TEX,            CTexture( doorTex,           texturesPath, doorColor ) );
			pType->AddTexture( BUILDING_CEILING_TEX,         CTexture( ceilTex,           texturesPath, ceilColor ) );
			pType->AddTexture( BUILDING_PAVEMENT_TEX,        CTexture( pavTex,            texturesPath, pavColor ) );
			pType->AddTexture( BUILDING_BASEMENT_TEX,        CTexture( baseTex,           texturesPath, baseColor ) );

			// using operator == because there are compilation problems with operator !=
			if ( !(windowProxy == "") ) { pType->AddProxy( BUILDING_WINDOW_PROXY, CProxy( windowProxy, proxyPath ) ); }
			if ( !(doorProxy == "") )   { pType->AddProxy( BUILDING_DOOR_PROXY,   CProxy( doorProxy,   proxyPath ) ); }

			m_elements.push_back( pType );
		}

		++i;
		if ( i > 9999 ) { break; }
	}
	while ( true );

	return (true);
}

//-----------------------------------------------------------------------------

void CBuildingTypesLibrary::SetDefault( const String& texturesPath )
{
	CBuildingType* pType = new CBuildingType( DEFAULT_BUILDING_TYPE_NAME ); 
	pType->AddTexture( BUILDING_FLOOR_EXWALL_TEX,    CTexture( "", texturesPath, DEFAULT_BUILDING_FLOOR_EXWALL_COLOR ) );
	pType->AddTexture( BUILDING_FLOOR_INWALL_TEX,    CTexture( "", texturesPath, DEFAULT_BUILDING_FLOOR_INWALL_COLOR ) );
	pType->AddTexture( BUILDING_INTERFLOOR_WALL_TEX, CTexture( "", texturesPath, DEFAULT_BUILDING_INTERFLOOR_WALL_COLOR ) );
	pType->AddTexture( BUILDING_ROOF_TOP_TEX,        CTexture( "", texturesPath, DEFAULT_BUILDING_ROOF_TOP_COLOR ) );
	pType->AddTexture( BUILDING_ROOF_SLOPE_TEX,      CTexture( "", texturesPath, DEFAULT_BUILDING_ROOF_SLOPE_COLOR ) );
	pType->AddTexture( BUILDING_WINDOW_TEX,          CTexture( "", texturesPath, DEFAULT_BUILDING_WINDOW_COLOR ) );
	pType->AddTexture( BUILDING_DOOR_TEX,            CTexture( "", texturesPath, DEFAULT_BUILDING_DOOR_COLOR ) );
	pType->AddTexture( BUILDING_CEILING_TEX,         CTexture( "", texturesPath, DEFAULT_BUILDING_CEILING_COLOR ) );
	pType->AddTexture( BUILDING_PAVEMENT_TEX,        CTexture( "", texturesPath, DEFAULT_BUILDING_PAVEMENT_COLOR ) );
	pType->AddTexture( BUILDING_BASEMENT_TEX,        CTexture( "", texturesPath, DEFAULT_BUILDING_BASEMENT_COLOR ) );

	m_elements.push_back( pType );
}

//-----------------------------------------------------------------------------
