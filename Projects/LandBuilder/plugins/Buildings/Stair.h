//-----------------------------------------------------------------------------
// File: Stair.h
//
// Desc: classes related to building stairs
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "StairTypes.h"
#include "GeometriesBuilder.h"

//-----------------------------------------------------------------------------

const Float DEFAULT_STAIR_STEP_RISER = 0.2;
const Float DEFAULT_STAIR_MARGIN     = 2.0;

//-----------------------------------------------------------------------------

class CStair
{
  const CStairType* m_type;

  Float m_height;

  CGPoint2 m_center;
  Float    m_orientation;

  size_t m_riserCount;
  Float  m_stepRiser;
  Float  m_stepTread;

public:
  CStair();
  CStair( const CStairType* type, Float height,
          const CGPoint2& center = CGPoint2(),
          Float orientation = CGConsts::ZERO );
  CStair( const CStair& other );

  // Getters
  // {
  const CStairType* GetType() const;
  Float GetHeight() const;
  const CGPoint2& GetCenter() const;
  CGPoint2& GetCenter();
  Float GetOrientation() const;
  // }

  // Setters
  // {
  void SetType( const CStairType* type );
  void SetHeight( Float height );
  void SetCenter( const CGPoint2& center );
  void SetOrientation( Float orientation );
  // }

  // Stair properties
  // {
  size_t RiserCount() const;
  Float StepRiser() const;
  Float StepTread() const;
  CGPolygon2 Footprint() const;
  GeometriesList GroundFloorGeometries() const;
  GeometriesList GenericFloorGeometries() const;
  // }

private:
  void Init();
};

//-----------------------------------------------------------------------------
