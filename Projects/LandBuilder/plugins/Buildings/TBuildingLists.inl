//-----------------------------------------------------------------------------
// File: TBuildingLists.inl
//
// Desc: templated lists for buildings
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < class T >
TCfgList< T >::TCfgList()
: TList< T >()
{
}

//-----------------------------------------------------------------------------

template < class T >
TCfgList< T >::~TCfgList()
{
}

//-----------------------------------------------------------------------------

template < class T >
int TCfgList< T >::FindByName( const String& name ) const
{
	for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
	{
		if ( m_elements[i]->GetName() == name ) { return ( static_cast< int >( i ) ); }
	}

	return (-1);
}

//-----------------------------------------------------------------------------

template < class T >
CColor TCfgList< T >::LoadColorFromCfgFile( const String& buff, 
										    ParamClassPtr& item, 
											const CColor& defaultColor, 
											const String& strColor, 
											ofstream& logFile )
{
	int defaultRed   = static_cast< int >( defaultColor.GetRed() );
	int defaultGreen = static_cast< int >( defaultColor.GetGreen() );
	int defaultBlue  = static_cast< int >( defaultColor.GetBlue() );

	int red   = defaultRed;
	int green = defaultGreen;
	int blue  = defaultBlue;

	ParamEntryPtr entry = item->FindEntry( strColor );

	if ( entry.IsNull() ) 
	{
		LOGF( Error ) << "Missing " << buff << "::" << strColor << " field. Using default " << defaultColor.ToString() << ".";
		logFile << "WARNING: Missing " << buff << "::" << strColor << " field. Using default " << defaultColor.ToString() << "." << endl << endl;
	}
	else if ( !entry->IsArray() )
	{
		LOGF( Error ) << "Bad data into " << buff << "::" << strColor << " field. Using default " << defaultColor.ToString() << ".";
		logFile << "WARNING: Bad data into " << buff << "::" << strColor << " field. Using default " << defaultColor.ToString() << "." << endl << endl;
	}
	else
	{
		// red component
		red = (*entry)[0];
		if ( red < 0 || 255 < red )
		{
			red = defaultRed;
			LOGF( Error ) << "Wrong value in " << buff << "::" << strColor << " ::Red field. Using default (" << red << ").";
			logFile << "WARNING: Wrong value in " << buff << "::" << strColor << " ::Red field. Using default (" << red << ")." << endl << endl;
		}

		// green component
		green = (*entry)[1];
		if ( green < 0 || 255 < green )
		{
			green = defaultGreen;
			LOGF( Error ) << "Wrong value in " << buff << "::" << strColor << " ::Green field. Using default (" << green << ").";
			logFile << "WARNING: Wrong value in " << buff << "::" << strColor << " ::Green field. Using default (" << green << ")." << endl << endl;
		}

		// blue component
		blue = (*entry)[2];
		if ( blue < 0 || 255 < blue )
		{
			blue = defaultBlue;
			LOGF( Error ) << "Wrong value in " << buff << "::" << strColor << " ::Blue field. Using default (" << blue << ").";
			logFile << "WARNING: Wrong value in " << buff << "::" << strColor << " ::Blue field. Using default (" << blue << ")." << endl << endl;
		}
	}

	return (CColor( static_cast< size_t >( red ), 
					static_cast< size_t >( green ),
					static_cast< size_t >( blue ) )); 
}

//-----------------------------------------------------------------------------

template < class T >
String TCfgList< T >::LoadTextureFromCfgFile( const String& buff, 
											  ParamClassPtr& item,
											  const String& strTex, 
											  const CColor& color, 
											  ofstream& logFile )
{
	String texture = "";
	ParamEntryPtr entry = item->FindEntry( strTex );            
	if ( entry.IsNull() ) 
	{
		LOGF( Error ) << "Missing " << buff << "::" << strTex << " field. Using uniform color " << color.ToString() << ".";
		logFile << "WARNING: Missing " << buff << "::" << strTex << " field. Using uniform color " << color.ToString() << "." << endl << endl;
	}
	else
	{
		texture = entry->GetValue();
		ifstream tex;
		tex.open( texture, std::ios::in );
		if ( tex.fail() )
		{
			LOGF( Error ) << "Warning " << buff << "::" << strTex << " field. File not found: using uniform color " << color.ToString() << ".";
			logFile << "WARNING: " << buff << "::" << strTex << " field. File not found: using uniform color " << color.ToString() << "." << endl << endl;
			texture = "";
		}
		else
		{
			tex.close();
		}
	}

	return (texture);
}

//-----------------------------------------------------------------------------

template < class T >
bool TCfgList< T >::LoadBoolFromCfgFile( const String& buff, 
										 ParamClassPtr& item, 
										 const String& strBool, 
										 bool defaultValue, 
										 ofstream& logFile )
{
	bool value = defaultValue;
	ParamEntryPtr entry = item->FindEntry( strBool );            
	if ( entry.IsNull() ) 
	{
		String def = defaultValue ? "1" : "0";
		LOGF( Error ) << "Missing " << buff << "::" << strBool << " field. Using default (" << def << ").";
		logFile << "WARNING: Missing " << buff << "::" << strBool << " field. Using default (" << def << ")." << endl << endl;
	}
	else
	{
		int intValue = *entry;
		if ( intValue == -1 ) // random
		{
			value = (rand() > RAND_MAX * CGConsts::HALF) ? true : false;
		}
		else
		{
			value = (intValue == 0) ? false : true;
		}
	}

	return (value);
}

//-----------------------------------------------------------------------------

template < class T >
Float TCfgList< T >::LoadDoubleFromCfgFile( const String& buff, 
										    ParamClassPtr& item,
											const String& strFloat, 
											Float defaultValue,
											Float minValidValue, 
											Float maxValidValue,
											ofstream& logFile )
{
	Float value = defaultValue;
	ParamEntryPtr entry = item->FindEntry( strFloat );            
	if ( entry.IsNull() ) 
	{
		LOGF( Error ) << "Missing " << buff << "::" << strFloat << " field. Using default (" << value << ").";
		logFile << "WARNING: Missing " << buff << "::" << strFloat << " field. Using default (" << value << ")." << endl << endl;
	}
	else
	{
		double doubleValue = *entry;
		if ( (static_cast< double >( minValidValue ) < doubleValue) && 
			 (doubleValue < static_cast< double >( maxValidValue )) )
		{
			value = static_cast< Float >( doubleValue );
		}
		else
		{
			value = defaultValue;
			LOGF( Error ) << "Wrong value in " << buff << "::" << strFloat << " field. Using default (" << value << ").";
			logFile << "WARNING: Wrong value in " << buff << "::" << strFloat << " field. Using default (" << value << ")." << endl << endl;
		}
	}

	return (value);
}

//-----------------------------------------------------------------------------

template < class T >
String TCfgList< T >::LoadProxyNameFromCfgFile( const String& buff, 
											   ParamClassPtr& item, 
											   const String& strProxy, 
											   ofstream& logFile )
{
	String proxyName = "";
	ParamEntryPtr entry = item->FindEntry( strProxy );            
	if ( !entry.IsNull() ) 
	{
		proxyName = entry->GetValue();
		ifstream proxy;
		proxy.open( proxyName, std::ios::in );
		if ( proxy.fail() )
		{
			LOGF( Error ) << "Warning " << buff << "::" << strProxy << " field. File not found.";
			logFile << "WARNING: " << buff << "::" << strProxy << " field. File not found." << endl << endl;
			proxyName = "";
		}
		else
		{
			proxy.close();
		}
	}

	return (proxyName);
}

//-----------------------------------------------------------------------------
