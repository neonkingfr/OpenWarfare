//-----------------------------------------------------------------------------
// File: Texture.cpp
//
// Desc: classes related to textures
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Texture.h"

//-----------------------------------------------------------------------------

CTexture::CTexture()
: m_srcFilename( "" )
, m_dstFolder( "" )
, m_color( CColor() )
, m_exported( false )
{
}

//-----------------------------------------------------------------------------

CTexture::CTexture( const String& srcFilename, const String& dstFolder, 
				    const CColor& color )
: m_srcFilename( srcFilename )
, m_dstFolder( dstFolder )
, m_color( color )
, m_exported( false )
{
	if ( !(m_dstFolder[m_dstFolder.GetLength() - 1] == '\\') )
	{
		m_dstFolder = m_dstFolder + "\\";
	}
}

//-----------------------------------------------------------------------------

CTexture::CTexture( const CTexture& other )
: m_srcFilename( other.m_srcFilename )
, m_dstFolder( other.m_dstFolder )
, m_color( other.m_color )
, m_exported( other.m_exported )
{
}

//-----------------------------------------------------------------------------

CTexture::~CTexture()
{
}

//-----------------------------------------------------------------------------

bool CTexture::operator == ( const CTexture& other ) const
{
	if ( this == &other ) { return (true); }

	if ( m_srcFilename != other.m_srcFilename ) { return (false); }
	if ( m_dstFolder   != other.m_dstFolder )   { return (false); }
	if ( m_color       != other.m_color )       { return (false); }

	return (true);
}

//-----------------------------------------------------------------------------

bool CTexture::operator != ( const CTexture& other ) const
{
	return (!(this->operator==( other )));
}

//-----------------------------------------------------------------------------

String CTexture::GetDstAbsFilename() const
{
	if ( m_srcFilename.IsEmpty() ) { return (""); }

	int pos = m_srcFilename.ReverseFind( '\\' );
	return (m_dstFolder + m_srcFilename.Substring( pos + 1, m_srcFilename.GetLength() ));
}

//-----------------------------------------------------------------------------

String CTexture::GetDstRelFilename() const
{
	if ( m_srcFilename.IsEmpty() ) { return (m_color.ToTexture()); }
	
	String root = m_dstFolder;
  int pos = root.Find( ':' ) + 2;
  root = root.Substring( pos, root.GetLength() );

	pos = m_srcFilename.ReverseFind( '\\' );
	return (root + m_srcFilename.Substring( pos + 1, m_srcFilename.GetLength() ));
}

//-----------------------------------------------------------------------------

bool CTexture::Export() const
{
	if ( m_exported ) { return (true); }

	String dstFilename = GetDstAbsFilename();

	if ( !dstFilename.IsEmpty() )
	{
		// copy the texture on disk.
		// first tests if the texture has been already saved 
		// (it maybe be shared with other models or may already exist)
		// source textures can be everywhere, destination textures
		// will be saved in a \data subfolder of the folder containing
		// the models
		ifstream test( dstFilename, std::ios::binary );
		if ( test.fail() )
		{
			ifstream ifs( m_srcFilename, std::ios::binary );
			ofstream ofs( dstFilename, std::ios::binary );

			const int BUFFER_SIZE = 4096;
			char buffer[BUFFER_SIZE];

			while ( !ifs.eof() ) 
			{
				ifs._Read_s( buffer, BUFFER_SIZE, BUFFER_SIZE );
				if ( !ifs.bad() ) 
				{
					ofs.write( buffer, ifs.gcount() );
				}
			}

			ifs.close();
			ofs.close();
		}
		else
		{
			test.close();
		}		

		m_exported = true;
	}

	return (true);
}

//-----------------------------------------------------------------------------
