//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\landbuilder2\MapInput.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
  namespace Modules
  {

//-----------------------------------------------------------------------------

    class AdvancedRandomPlacerSlope : public IBuildModule
    {
    public:
      struct ShapeInput
      {
        size_t m_randomSeed;
        double m_hectareDensity;
      };

      struct ObjectInput
      {
        RString m_name;
        double  m_prob;
        double  m_normProb;
        double  m_minHeight;
        double  m_maxHeight;
        double  m_minDistance;
        double  m_minSlope;
        double  m_maxSlope;
        int     m_counter;

        ObjectInput() 
        {
        }

        ObjectInput( RString name, double prob, double minHeight, double maxHeight, 
                     double minDistance, double minSlope, double maxSlope )
        : m_name( name )
        , m_prob( prob )
        , m_minHeight( minHeight )
        , m_maxHeight( maxHeight )
        , m_minDistance( minDistance )
        , m_minSlope( minSlope )
        , m_maxSlope( maxSlope )
        , m_counter( 0 )
        {
        }

        ClassIsMovable( ObjectInput );
      };

    private:
      struct ShapeGeo
      {
        double m_minX;
        double m_maxX;
        double m_minY;
        double m_maxY;

        double WidthBB() const
        {
			    return (m_maxX - m_minX);
        }

        double HeightBB() const
        {
			    return (m_maxY - m_minY);
        }

        double AreaBB() const
        {
          return (WidthBB() * HeightBB());
        }

        double AreaHectaresBB() const
        {
          return (AreaBB() / 10000.0);
        }
      };

    public:
      AdvancedRandomPlacerSlope();

      virtual ~AdvancedRandomPlacerSlope()
      {
      }

      virtual void Run( IMainCommands* cmdIfc );
      ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, MapInput& mapIn,
                                             AutoArray< AdvancedRandomPlacerSlope::ObjectInput >& objectsIn );

      DECLAREMODULEEXCEPTION( "AdvancedRandomPlacerSlope" )

    private:
      string m_version;
      bool   m_mapLoaded;
      size_t m_shapeCounter;

      const Shapes::IShape* m_currentShape;      

      ShapeGeo m_shpGeo;
      int      m_numberOfIndividuals;

      AutoArray< ObjectInput > m_objectsIn;
      ModuleObjectOutputArray  m_objectsOut;

      ShapeInput  m_shapeIn;
      MapInput    m_mapIn;

      EtRectElevationGrid< double, int > m_mapData;

      bool GetShapeGeoData();
      bool GetShapeParameters( IMainCommands* cmdIfc );
      bool GetObjectsParameters( IMainCommands* cmdIfc );
      bool GetMapParameters( IMainCommands* cmdIfc );
      bool LoadHighMap();

      bool NormalizeProbs();
      bool PtCloseToObject( const Shapes::DVector& v ) const;

      void CreateObjects( bool preview );
      int  GetRandomType();
      void ExportCreatedObjects( IMainCommands* cmdIfc );
      void WriteReport();
    };

//-----------------------------------------------------------------------------

  } // namespace Modules
}  // namespace LandBuildInt
