//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class BiasedRandomPlacer : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				bool   m_smallAtBoundary;
				size_t m_randomSeed;
				double m_hectareDensity;
				double m_clusterCoeff;
				int    m_parentCount;
			};

			struct ObjectInput
			{
				RString m_name;
				double  m_minHeight;
				double  m_maxHeight;
				double  m_prob;
				int     m_counter;
				double  m_minDistance;
				double  m_maxDistance;
				double  m_normProb;

				ObjectInput() 
				{
				}

				ObjectInput( RString name, double minHeight, double maxHeight, double prob, double minDistance )
				: m_name( name )
				, m_minHeight( minHeight )
				, m_maxHeight( maxHeight )
				, m_prob( prob )
				, m_counter( 0 )
				, m_minDistance( minDistance )
				{
				}

				ClassIsMovable( ObjectInput ); 
			};

		private:
			struct ShapeGeo
			{
				double m_minX;
				double m_maxX;
				double m_minY;
				double m_maxY;

        double WidthBB() const
        {
			    return (m_maxX - m_minX);
        }

        double HeightBB() const
        {
			    return (m_maxY - m_minY);
        }

        double AreaBB() const
        {
          return (WidthBB() * HeightBB());
        }

        double AreaHectaresBB() const
        {
          return (AreaBB() / 10000.0);
        }
      };

			struct ObjectOutputExtra
			{
				ModuleObjectOutput m_data;
				bool               m_atBoundary;
				int	               m_childIndex;
				double             m_minDistance;
				double             m_maxDistance;
				ClassIsMovable( ObjectOutputExtra ); 
			};

		public:
      BiasedRandomPlacer();

      virtual ~BiasedRandomPlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                             AutoArray< BiasedRandomPlacer::ObjectInput >& objectsIn );

      DECLAREMODULEEXCEPTION( "BiasedRandomPlacer" )

		private:
      string m_version;
      size_t m_shapeCounter;

			AutoArray< ObjectInput >       m_objectsIn;
			AutoArray< ObjectOutputExtra > m_objectsOutExtra;

			ModuleObjectOutputArray m_objectsOut;

			ShapeInput            m_shapeIn;
			const Shapes::IShape* m_currentShape;      
			ShapeGeo              m_shpGeo;
			int                   m_numberOfIndividuals;

			bool GetShapeGeoData();
			bool GetShapeParameters( IMainCommands* cmdIfc );
			bool GetObjectsParameters( IMainCommands* cmdIfc );

			bool NormalizeProbs();
			void SetParents();
			void CreateObjects( bool preview );
			void AddObjectOutExtra( const Shapes::DVector& v, int type, ObjectOutputExtra* parent );
			int  GetRandomType();
			bool PtCloseToObject( const Shapes::DVector& v ) const;
			void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();

			ObjectOutputExtra* GetRandomParent( int type );
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt