//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include <algorithm>

#include ".\BiasedRandomPlacerSlope.h"

#include "..\..\LandBuilder2\IShapeExtra.h"
#include "..\..\HighMapLoaders\include\EtMath.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		BiasedRandomPlacerSlope::BiasedRandomPlacerSlope()
    : m_version( "1.1.1" )
    , m_mapLoaded( false )
    , m_shapeCounter( 0 )
		{
		}

//-----------------------------------------------------------------------------

		void BiasedRandomPlacerSlope::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_mapLoaded    = false;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();      
			
      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "BiasedRandomPlacerSlope " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";

			// gets shape geometric data
			if ( GetShapeGeoData() )
			{
        // gets maps parameters from calling cfg file
        if ( GetMapParameters( cmdIfc ) ) 
        {
          // load maps
          if ( LoadHighMap() )
          {
            LOGF( Info ) << "Parameters:";

				    // gets shape parameters from calling cfg file
				    if ( GetShapeParameters( cmdIfc ) )
				    {
                // gets objects parameters from calling cfg file
					    if ( GetObjectsParameters( cmdIfc ) )
					    {
                // normalizes probabilities
						    if ( NormalizeProbs() )
						    {
							    // initializes random seed
							    srand( m_shapeIn.m_randomSeed );

							    // for each object type put at least one parent
							    // who will be the start for all the other objects
							    // of the same type
							    SetParents();

							    // calculates the number of individual to be generated
							    // (this value is referred to the bounding box of the shape
							    //  and assume a uniform distribution - random generated values
							    //  will be filtered to obtain the same density inside the shape,
							    //  discarding the ones which will fall out of the shape itself)
							    m_numberOfIndividuals = static_cast< int >( m_shpGeo.AreaHectaresBB() * m_shapeIn.m_hectareDensity );

							    // sets the population
							    CreateObjects( false );

							    // export objects
							    ExportCreatedObjects( cmdIfc );

							    // writes the report
							    WriteReport();
                }
              }
						}
					}
				}
			}
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* BiasedRandomPlacerSlope::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, MapInput& mapIn,
                                                                    AutoArray< BiasedRandomPlacerSlope::ObjectInput >& objectsIn )
		{
			m_currentShape  = shape;
			m_shapeIn       = shapeIn;
			m_mapIn         = mapIn;
			m_objectsIn     = objectsIn;

      if ( !m_currentShape ) { return (NULL); }

			int objInCount = m_objectsIn.Size();
			for ( int i = 0; i < objInCount; ++i )
			{
				m_objectsIn[i].m_maxDistance = m_objectsIn[i].m_minDistance * 9.0 * m_shapeIn.m_clusterCoeff; 
			}

      if ( m_objectsOutExtra.Size() != 0 ) { m_objectsOutExtra.Clear(); }

			// gets shape geometric data
			if ( GetShapeGeoData() )
			{
        // load maps
        if ( LoadHighMap() )
        {
				  // normalizes probabilities
				  if ( NormalizeProbs() )
				  {
					  // initializes random seed
					  srand( m_shapeIn.m_randomSeed );

					  // for each object type put at least one parent
					  // who will be the start for all the other objects
					  // of the same type
					  SetParents();

					  // calculates the number of individual to be generated
					  // (this value is referred to the bounding box of the shape
					  //  and assume a uniform distribution - random generated values
					  //  will be filtered to obtain the same density inside the shape,
					  //  discarding the ones which will fall out of the shape itself)
					  m_numberOfIndividuals = static_cast< int >( m_shpGeo.AreaHectaresBB() * m_shapeIn.m_hectareDensity );

					  // sets the population
					  CreateObjects( true );

					  m_objectsOut.Clear();

					  // extract the data
					  int objOutExCount = m_objectsOutExtra.Size();
					  for ( int i = 0; i < objOutExCount; ++i )
					  {
						  m_objectsOut.Add( m_objectsOutExtra[i].m_data );
					  }
					  return (&m_objectsOut);
				  }
        }
			  else
			  {
				  return (NULL);
			  }
			}
			else
			{
				return (NULL);
			}
		}

//-----------------------------------------------------------------------------

		bool BiasedRandomPlacerSlope::GetShapeGeoData()
		{
			Shapes::DBox bBox = m_currentShape->CalcBoundingBox();

			m_shpGeo.m_minX = bBox.lo.x;
			m_shpGeo.m_minY = bBox.lo.y;
			m_shpGeo.m_maxX = bBox.hi.x;
			m_shpGeo.m_maxY = bBox.hi.y;

			return (true);
		}

//-----------------------------------------------------------------------------

		bool BiasedRandomPlacerSlope::GetShapeParameters( IMainCommands* cmdIfc )
		{
      // smallAtBoundary (isMovable && hasDefault)
			m_shapeIn.m_smallAtBoundary = true; // default value

			const char* x = cmdIfc->QueryValue( "smallAtBoundary", true );
			if ( !x ) { x = cmdIfc->QueryValue( "smallAtBoundary", false ); }

			if ( x ) 
			{
				if ( IsBool( string( x ), false ) )
				{
					int i = static_cast< int >( atoi( x ) );
					if ( i == 0 )
					{
						m_shapeIn.m_smallAtBoundary = false;
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
          LOGF( Warn ) << ">>> Bad data for smallAtBoundary - using default value (1) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ----------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for smallAtBoundary - using default value (1) <<<";
        LOGF( Warn ) << ">>> ----------------------------------------------------------- <<<";
			}

      LOGF( Info ) << "smallAtBoundary: " << m_shapeIn.m_smallAtBoundary;

      // randomSeed (isMovable)
			x = cmdIfc->QueryValue( "randomSeed", true );
			if ( !x ) { x = cmdIfc->QueryValue( "randomSeed", false ); }

      if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_randomSeed = static_cast< size_t >( atoi( x ) );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for randomSeed <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing value for randomSeed <<<" ) );
				return (false);
			}

      LOGF( Info ) << "randomSeed: " << m_shapeIn.m_randomSeed;

			// hectareDensity (isMovable && hasDefault)
			m_shapeIn.m_hectareDensity = 100.0; // default value

			x = cmdIfc->QueryValue( "hectareDensity", true );
			if ( !x ) { x = cmdIfc->QueryValue( "hectareDensity", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_hectareDensity = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for hectareDensity - using default value (100) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------------ <<<";
        LOGF( Warn ) << ">>> Missing value for hectareDensity - using default value (100) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------------ <<<";
			}
			if ( m_shapeIn.m_hectareDensity < 0.01 )
			{
				ExceptReg::RegExcpt( Exception( 21, "Density is too low (less then 0.01)" ) );
				return (false);
			}

      LOGF( Info ) << "hectareDensity: " << m_shapeIn.m_hectareDensity;

      // aggregationCoeff (isMovable && hasDefault) 
			m_shapeIn.m_clusterCoeff = 1.0; // default value

			x = cmdIfc->QueryValue( "aggregationCoeff", true );
			if ( !x ) { x = cmdIfc->QueryValue( "aggregationCoeff", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_clusterCoeff = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for clusterCoeff - using default value (1.0) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ---------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for clusterCoeff - using default value (1.0) <<<";
        LOGF( Warn ) << ">>> ---------------------------------------------------------- <<<";
			}
			if ( m_shapeIn.m_clusterCoeff < 0.0 )
			{
				ExceptReg::RegExcpt( Exception( 22, ">>> Coefficient must be positive <<<" ) );
				return (false);
			}

      LOGF( Info ) << "clusterCoeff: " << m_shapeIn.m_clusterCoeff;

      // parentCount (isMovable && hasDefault)
			m_shapeIn.m_parentCount = 1; // default value

			x = cmdIfc->QueryValue( "parentCount", true );
			if ( !x ) { x = cmdIfc->QueryValue( "parentCount", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_parentCount = atoi( x );
          if ( m_shapeIn.m_parentCount < 1 ) { m_shapeIn.m_parentCount = 1; }
				}
				else
				{
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for parentCount - using default value (1) <<<";
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for parentCount - using default value (1) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
			}

      LOGF( Info ) << "parentCount: " << m_shapeIn.m_parentCount;

			return (true);
		};

//-----------------------------------------------------------------------------

		bool BiasedRandomPlacerSlope::GetObjectsParameters( IMainCommands* cmdIfc )
		{
			// resets arrays
			if ( m_objectsIn.Size() != 0 )
			{
				m_objectsIn.Clear();
				m_objectsOutExtra.Clear();
			}

			// gets objects' data from calling cfg file
			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], prob[50], mindist[50],minslope[50], maxslope[50];
				sprintf_s( object     , "object%d"     , counter );
				sprintf_s( minheight  , "minheight%d"  , counter );
				sprintf_s( maxheight  , "maxheight%d"  , counter );
				sprintf_s( prob       , "prob%d"       , counter );
				sprintf_s( mindist    , "mindist%d"    , counter );
				sprintf_s( minslope   , "minslope%d"   , counter );
				sprintf_s( maxslope   , "maxslope%d"   , counter );

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue( object, false );
        if ( !x ) { break; }
				nfo.m_name = x;

        LOGF( Info ) << "object " << counter << ": " << nfo.m_name;

				// object min height
				x = cmdIfc->QueryValue( minheight, false );
				nfo.m_minHeight = 100.0; // default value
				if ( x ) 
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_minHeight = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for minheight - using default value (100) <<<";
            LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing value for minheight - using default value (100) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
				}

        LOGF( Info ) << "minHeight: " << nfo.m_minHeight;

				// object max height
				x = cmdIfc->QueryValue( maxheight, false );
				nfo.m_maxHeight = 100.0; // default value
				if ( x ) 
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_maxHeight = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for maxheight - using default value (100) <<<";
            LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing value for maxheight - using default value (100) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
				}

        LOGF( Info ) << "maxHeight: " << nfo.m_maxHeight;

				// object min slope
				x = cmdIfc->QueryValue( minslope, false );
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_minSlope = atof( x );
					}
					else
					{
						ExceptReg::RegExcpt( Exception( 31, ">>> Bad data for minslope <<<" ) );
						return (false);
					}
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 32, ">>> Missing minslope value <<<" ) );
					return (false);
				}
				
        LOGF( Info ) << "minSlope: " << nfo.m_minSlope;

				// object max slope
				x = cmdIfc->QueryValue( maxslope, false );
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_maxSlope = atof( x );
					}
					else
					{
						ExceptReg::RegExcpt( Exception( 33, ">>> Bad data for maxslope <<<" ) );
						return (false);
					}
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 34, ">>> Missing maxslope value <<<" ) );
					return (false);
				}

        LOGF( Info ) << "maxSlope: " << nfo.m_maxSlope;

				// object probability
				nfo.m_prob = 100.0; // default value
				x = cmdIfc->QueryValue( prob, false );
				if ( x ) 
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_prob = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> --------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for prob - using default value (100) <<<";
            LOGF( Warn ) << ">>> --------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing value for prob - using default value (100) <<<";
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
				}

        LOGF( Info ) << "prob: " << nfo.m_prob;

				// object min distance
				x = cmdIfc->QueryValue( mindist, false );
				nfo.m_minDistance = 5.0; // default value
				if ( x ) 
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_minDistance = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
            LOGF( Warn ) << ">>> Bad data for mindist - using default value (5.0) <<<";
            LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing value for mindist - using default value (5.0) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
				}

        LOGF( Info ) << "minDistance: " << nfo.m_minDistance;

				// object counter
				nfo.m_counter = 0;

				// object max distance
				nfo.m_maxDistance = nfo.m_minDistance * 9.0 * m_shapeIn.m_clusterCoeff; 

				m_objectsIn.Add( nfo );
				counter++;
			} 
			while ( true );

			return (true);
		}

//-----------------------------------------------------------------------------

		bool BiasedRandomPlacerSlope::GetMapParameters( IMainCommands* cmdIfc )
		{
      if ( m_mapLoaded ) { return (true); }
      return (m_mapIn.LoadMapParameters( cmdIfc ));
		}

//-----------------------------------------------------------------------------

		bool BiasedRandomPlacerSlope::LoadHighMap()
		{
      if ( m_mapLoaded ) 
      { 
        m_mapIn.SetHmData( m_mapData );
      }
      else
      {
        if ( !m_mapIn.LoadHighMap() ) { return (false); }

			  m_mapData = m_mapIn.GetHmData();
			  m_mapLoaded = true;
      }

			return (true);
		}

//-----------------------------------------------------------------------------

		bool BiasedRandomPlacerSlope::NormalizeProbs()
		{
			double total = 0.0;

			int objInCount = m_objectsIn.Size();
			for ( int i = 0; i < objInCount; ++i )
			{
				total += m_objectsIn[i].m_prob;
			}

			if ( total == 0.0 ) 
			{
				ExceptReg::RegExcpt( Exception( 91, ">>> Total probability equal to zero <<<" ) );
				return (false);
			}

			double invTotal = 1 / total;
			for ( int i = 0; i < objInCount; ++i )
			{
				m_objectsIn[i].m_normProb = m_objectsIn[i].m_prob * invTotal;
			}
			return (true);
		}

//-----------------------------------------------------------------------------

		void BiasedRandomPlacerSlope::SetParents()
		{
			// add one object for every species in the config file
			int objInCount = m_objectsIn.Size();
			for ( int i = 0; i < objInCount; ++i )
			{
				Shapes::DVector v0;

				bool valid = false;

				while ( !valid )
				{
					// random position
					double x0 = RandomInRangeF( 0.0, m_shpGeo.WidthBB() );
					double y0 = RandomInRangeF( 0.0, m_shpGeo.HeightBB() );
					v0 = Shapes::DVector( x0 + m_shpGeo.m_minX, y0 + m_shpGeo.m_minY );

					// the random point is inside the current shape ?
					if ( m_currentShape->PtInside( v0 ) )
					{
						// is close to already created objects ?
						valid = !(PtCloseToObject( v0 ));
					}
				}

				// if all ok, add to array
				AddObjectOutExtra( v0, i, NULL );
			}

			// adds more parents for every species if specified
			for ( int i = 0; i < objInCount; ++i )
			{
				// set the parent to first created object of this species
				ObjectOutputExtra* parent = &m_objectsOutExtra[i];

				int estimatedNumberOfParents = (int)(m_shapeIn.m_parentCount * m_shpGeo.AreaHectaresBB());

				while ( m_objectsIn[i].m_counter < estimatedNumberOfParents )
				{
					Shapes::DVector v0;

					bool valid = false;

					while ( !valid )
					{
						// random position
						double x0 = RandomInRangeF( 0.0, m_shpGeo.WidthBB() );
						double y0 = RandomInRangeF( 0.0, m_shpGeo.HeightBB() );
						v0 = Shapes::DVector( x0 + m_shpGeo.m_minX, y0 + m_shpGeo.m_minY );

						// the random point is inside the current shape ?
						if ( m_currentShape->PtInside( v0 ) )
						{
							// is close to already created objects ?
							valid = !(PtCloseToObject( v0 ));
						}
					}

					// if all ok, add to array
					AddObjectOutExtra( v0, i, parent );

					// update parent
					parent = &m_objectsOutExtra[m_objectsOutExtra.Size() - 1];
				}
			}
		}

//-----------------------------------------------------------------------------

		bool BiasedRandomPlacerSlope::PtCloseToObject( const Shapes::DVector& v ) const
		{
			int objOutExCount = m_objectsOutExtra.Size();
			for ( int j = 0; j < objOutExCount; ++j )
			{
				double distSq = v.DistanceXY2( m_objectsOutExtra[j].m_data.position );

				// is close to already existing objects ?
				double minDistanceSq = m_objectsOutExtra[j].m_minDistance * m_objectsOutExtra[j].m_minDistance; 
				if ( distSq < minDistanceSq )
				{
					return (true);
				}
			}
			return (false);
		}

//-----------------------------------------------------------------------------

		void BiasedRandomPlacerSlope::AddObjectOutExtra( const Shapes::DVector& v, int type, ObjectOutputExtra* parent )
		{
			ObjectOutputExtra out;

			// sets data
			out.m_data.position   = v;
			out.m_data.type       = type;
			out.m_data.rotation   = RandomInRangeF( 0.0, EtMathD::TWO_PI );

			Shapes::DVector n = NearestToEdge( m_currentShape, v );

			out.m_minDistance = m_objectsIn[type].m_minDistance;
			out.m_maxDistance = m_objectsIn[type].m_maxDistance;

			double distSq = v.DistanceXY2( n );
			if ( distSq < 4.0 * (out.m_minDistance * out.m_minDistance) )
			{
				out.m_atBoundary = true;
			}
			else
			{
				out.m_atBoundary = false;
			}

			double minScale = m_objectsIn[type].m_minHeight;
			double maxScale = m_objectsIn[type].m_maxHeight;

			if ( m_shapeIn.m_smallAtBoundary && out.m_atBoundary )
			{
				out.m_data.scaleX = 
				out.m_data.scaleY = 
				out.m_data.scaleZ = minScale / 100.0;
			}
			else
			{
				out.m_data.scaleX = 
				out.m_data.scaleY = 
				out.m_data.scaleZ = RandomInRangeF( minScale, maxScale ) / 100.0;
			}

			// update parenthood
			if ( parent )
			{
				out.m_childIndex = parent->m_childIndex;
				parent->m_childIndex = m_objectsOutExtra.Size();
			}
			else
			{
				out.m_childIndex = m_objectsOutExtra.Size();
			}

			// used only by VLB for preview
			out.m_data.length = 10.0;
			out.m_data.width  = 10.0;
			out.m_data.height = 10.0;

			// adds to array
			m_objectsOutExtra.Add( out );

			// update type counter
			m_objectsIn[type].m_counter++;
		}

//-----------------------------------------------------------------------------

		int BiasedRandomPlacerSlope::GetRandomType()
		{
			double p = (double)rand() / ((double)RAND_MAX + 1);

			bool found = false;
			int index  = 0;
			double curProb = m_objectsIn[index].m_normProb;
			while ( !found )
			{
				if ( p < curProb )
				{
					found = true;
				}
				else
				{
					index++;
					curProb += m_objectsIn[index].m_normProb;
				}
			}
			return (index);
		}

//-----------------------------------------------------------------------------

		BiasedRandomPlacerSlope::ObjectOutputExtra* BiasedRandomPlacerSlope::GetRandomParent( int type )
		{
			int randomPosIndex = static_cast< int >( RandomInRangeF( 0.0, static_cast< double >( m_objectsIn[type].m_counter ) ) );

			int parentIndex = type;

			// searches for parent
			for ( int i = 0; i < randomPosIndex; ++i )
			{
				parentIndex = m_objectsOutExtra[parentIndex].m_childIndex;
			}

			return (&m_objectsOutExtra[parentIndex]);
		}

//-----------------------------------------------------------------------------

		void BiasedRandomPlacerSlope::CreateObjects( bool preview )
		{
			ProgressBar< int > pb( m_numberOfIndividuals );
			
			for ( int i = 0; i < m_numberOfIndividuals; ++i )
			{
        if ( !preview ) { pb.AdvanceNext( 1 ); }

				// random position
				double x0 = RandomInRangeF( 0.0, m_shpGeo.WidthBB() );
				double y0 = RandomInRangeF( 0.0, m_shpGeo.HeightBB() );
				Shapes::DVector v0 = Shapes::DVector( x0 + m_shpGeo.m_minX, y0 + m_shpGeo.m_minY );

				// the random point is inside the current shape ?
				if ( m_currentShape->PtInside( v0 ) )
				{
					// random type
					int type = GetRandomType();

					int iterNum = 0;
					int iterMax = 2 * m_objectsIn[type].m_counter;
					bool valid = false;
					while ( !valid )
					{
						iterNum++;

						if ( iterNum < iterMax )
						{
							// gets the parent
							ObjectOutputExtra* parent = GetRandomParent( type );

							// random polar coordinates from parent
 							double dist  = RandomInRangeF( parent->m_minDistance, parent->m_maxDistance );
							double angle = RandomInRangeF( 0.0, EtMathD::TWO_PI );

							// calculates new random position
							x0 = parent->m_data.position.x + dist * cos( angle );
              y0 = parent->m_data.position.y + dist * sin( angle );
							v0 = Shapes::DVector( x0, y0 );

							// the new random point is inside the current shape ?
							if ( m_currentShape->PtInside( v0 ) )
							{
								valid = !(PtCloseToObject( v0 ));

								if ( valid )
								{
									double slope = m_mapIn.GetHmData().getWorldSlopeAt( v0.x, v0.y, EtRectElevationGrid< double, int >::imARMA ) * 100.0;

									if ( (slope >= m_objectsIn[type].m_minSlope) && (slope <= m_objectsIn[type].m_maxSlope) )
									{
										// if all ok, add to array											
										AddObjectOutExtra( v0, type, parent );
									}
								}
							}
						}
						else
						{
							valid = true;
						}
					}
				}
			}
		}

//-----------------------------------------------------------------------------

		void BiasedRandomPlacerSlope::ExportCreatedObjects( IMainCommands* cmdIfc )
		{
			int objOutExCount = m_objectsOutExtra.Size();
			for ( int i = 0; i < objOutExCount; ++i )
			{
				RString name       = m_objectsIn[m_objectsOutExtra[i].m_data.type].m_name;
				Vector3D position  = Vector3D( m_objectsOutExtra[i].m_data.position.x, 
                                                                          0.0,
                                       m_objectsOutExtra[i].m_data.position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOutExtra[i].m_data.rotation );
				Matrix4D mScaling     = Matrix4D( MScale, m_objectsOutExtra[i].m_data.scaleX, 
                                                  m_objectsOutExtra[i].m_data.scaleZ, 
                                                  m_objectsOutExtra[i].m_data.scaleY );

				Matrix4D transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

		void BiasedRandomPlacerSlope::WriteReport()
		{
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Created                  ";
			int objInCount = m_objectsIn.Size();
			for ( int i = 0; i < objInCount; ++i )
			{
        LOGF( Info ) << m_objectsIn[i].m_counter << " : " << m_objectsIn[i].m_name << "                           ";
			}
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
