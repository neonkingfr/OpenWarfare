#include "StdAfx.h"
#include "ModuleRegistrator.h"

namespace LandBuilder2
{
	using namespace LandBuildInt;
}

//source contains list of registered modules. 
/**Don't add new modules here. Instead, register module using the registrator 
at place of module's definition
*/
#include "AdvancedRandomPlacer.h"
#include "AdvancedRandomPlacerSlope.h"
#include "BiasedRandomPlacer.h"
#include "BiasedRandomPlacerSlope.h"
#include "BoundarySubSquaresFramePlacer.h"
#include "FramedForestPlacer.h"
#include "InnerSubSquaresFramePlacer.h"
#include "RandomOnPathPlacer.h"
#include "SimpleRandomPlacer.h"
#include "SimpleRandomPlacerSlope.h"
#include "SubSquaresRandomPlacer.h"

namespace LandBuilder2
{
	RegisterModule<Modules::SimpleRandomPlacer>            _register_RP_001("SimpleRandomPlacer");
	RegisterModule<Modules::SimpleRandomPlacerSlope>       _register_RP_002("SimpleRandomPlacerSlope");
	RegisterModule<Modules::AdvancedRandomPlacer>          _register_RP_003("AdvancedRandomPlacer");
	RegisterModule<Modules::AdvancedRandomPlacerSlope>     _register_RP_004("AdvancedRandomPlacerSlope");
	RegisterModule<Modules::BiasedRandomPlacer>            _register_RP_005("BiasedRandomPlacer");
	RegisterModule<Modules::BiasedRandomPlacerSlope>       _register_RP_006("BiasedRandomPlacerSlope");
	RegisterModule<Modules::BoundarySubSquaresFramePlacer> _register_RP_007("BoundarySubSquaresFramePlacer");
	RegisterModule<Modules::InnerSubSquaresFramePlacer>    _register_RP_008("InnerSubSquaresFramePlacer");
	RegisterModule<Modules::FramedForestPlacer>            _register_RP_009("FramedForestPlacer");
	RegisterModule<Modules::RandomOnPathPlacer>            _register_RP_010("RandomOnPathPlacer");
	RegisterModule<Modules::SubSquaresRandomPlacer>        _register_RP_011("SubSquaresRandomPlacer");
}