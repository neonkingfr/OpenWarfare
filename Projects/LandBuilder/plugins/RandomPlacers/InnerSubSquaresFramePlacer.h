//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class InnerSubSquaresFramePlacer : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				size_t m_randomSeed;
				double m_subSquaresSize;
				double m_maxNoise;
			};

			struct ObjectInput
			{
				RString m_name;
				double  m_prob;
				double  m_normProb;
				double  m_minHeight;
				double  m_maxHeight;
				int     m_counter;

				ObjectInput() 
        {
        }

				ObjectInput( RString name, double prob, double minHeight, double maxHeight )
        : m_name( name )
        , m_prob( prob )
        , m_minHeight( minHeight )
        , m_maxHeight( maxHeight )
        , m_counter( 0 )
				{
				}

				ClassIsMovable( ObjectInput );
			};

		public:
      InnerSubSquaresFramePlacer();

      virtual ~InnerSubSquaresFramePlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                             AutoArray< InnerSubSquaresFramePlacer::ObjectInput >& objectsIn );

      DECLAREMODULEEXCEPTION( "InnerSubSquaresFramePlacer" )

		private:
      string m_version;
      size_t m_shapeCounter;

			const Shapes::IShape* m_currentShape;      

			AutoArray< ObjectInput > m_objectsIn;
			ModuleObjectOutputArray  m_objectsOut;

			ShapeInput m_shapeIn;

			bool GetShapeParameters( IMainCommands* cmdIfc );
			bool GetObjectsParameters( IMainCommands* cmdIfc );

			bool NormalizeProbs();

			void CreateObjects( bool preview );
			int  GetRandomType();
			void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
