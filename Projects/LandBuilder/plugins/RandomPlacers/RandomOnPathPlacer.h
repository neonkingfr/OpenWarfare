//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{
		class RandomOnPathPlacer : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				double m_maxDistance;
				size_t m_randomSeed;
				double m_linearDensity;
			};

			struct ObjectInput
			{
				RString m_name;
				double  m_minHeight;
				double  m_maxHeight;
				double  m_prob;
				double  m_normProb;
				int     m_counter;

				ObjectInput() 
        {
        }
				
        ObjectInput( RString name, double minHeight, double maxHeight, double prob )
        : m_name( name )
        , m_minHeight( minHeight )
        , m_maxHeight( maxHeight )
        , m_prob( prob )
        , m_counter( 0 )
				{
				}

				ClassIsMovable( ObjectInput ); 
			};

		private:
			struct ShapeGeo
			{
				double m_perimeter;
			};

		public:
      RandomOnPathPlacer();

      virtual ~RandomOnPathPlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                             AutoArray< RandomOnPathPlacer::ObjectInput >& objectsIn );

      DECLAREMODULEEXCEPTION( "RandomOnPathPlacer" )

		private:
      string m_version;
      size_t m_shapeCounter;

			AutoArray< ObjectInput > m_objectsIn;
			ModuleObjectOutputArray  m_objectsOut;

			ShapeInput            m_shapeIn;
			const Shapes::IShape* m_currentShape;      
			ShapeGeo              m_shpGeo;
			int                   m_numberOfIndividuals;

			bool GetShapeGeoData();
			bool GetShapeParameters( IMainCommands* cmdIfc );
			bool GetObjectsParameters( IMainCommands* cmdIfc );

			bool NormalizeProbs();
			int  GetRandomType();
			void CreateObjects( bool preview );
			void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();

			Shapes::DVertex ConvertToXY( double s );
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt