//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\HighMapLoaders\include\EtMath.h"
#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"

#include ".\InnerSubSquaresFramePlacer.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

    InnerSubSquaresFramePlacer::InnerSubSquaresFramePlacer()
    : m_version( "1.1.1" )
    , m_shapeCounter( 0 )
    {
    }

//-----------------------------------------------------------------------------

		void InnerSubSquaresFramePlacer::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();      

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "InnerSubSquaresFramePlacer " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

			// gets shape parameters from calling cfg file
			if ( GetShapeParameters( cmdIfc ) )
			{
				// gets objects parameters from calling cfg file
				if ( GetObjectsParameters( cmdIfc ) )
				{
					// normalizes probabilities
					if ( NormalizeProbs() )
					{
						// initializes random seed
						srand( m_shapeIn.m_randomSeed );

						// sets the population
						CreateObjects( false );

						// export objects
						ExportCreatedObjects( cmdIfc );

						// writes the report
						WriteReport();
					}													
				}
			}       
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* InnerSubSquaresFramePlacer::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                                                       AutoArray< InnerSubSquaresFramePlacer::ObjectInput >& objectsIn )
		{
			m_currentShape = shape;
			m_shapeIn     = shapeIn;
			m_objectsIn    = objectsIn;

      if ( !m_currentShape ) { return (NULL); }

      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			// normalizes probabilities
			if ( NormalizeProbs() )
			{
				// initializes random seed
				srand( m_shapeIn.m_randomSeed );

				// sets the population
				CreateObjects( true );

				return (&m_objectsOut);
			}
			else
			{
				return (NULL);
			}
		}

//-----------------------------------------------------------------------------

		bool InnerSubSquaresFramePlacer::GetShapeParameters( IMainCommands* cmdIfc )
		{
      // randomSeed (isMovable)
			const char* x = cmdIfc->QueryValue( "randomSeed", true );
			if ( !x ) { x = cmdIfc->QueryValue( "randomSeed", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_randomSeed = static_cast< size_t >( atoi( x ) );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for randomSeed <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing randomSeed value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "randomSeed: " << m_shapeIn.m_randomSeed;

      // subSquaresSize (isMovable && hasDefault)
			m_shapeIn.m_subSquaresSize = 10.0; // default value

      x = cmdIfc->QueryValue( "subSquaresSize", true );
			if ( !x ) { x = cmdIfc->QueryValue( "subSquaresSize", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_subSquaresSize = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> -------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for subSquaresSize - using default value (10.0) <<<";
          LOGF( Warn ) << ">>> -------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> --------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing subSquaresSize value - using default value (10.0) <<<";
        LOGF( Warn ) << ">>> --------------------------------------------------------- <<<";
			}
			if ( m_shapeIn.m_subSquaresSize < 1.0 )
			{
				ExceptReg::RegExcpt( Exception( 21, ">>> subSquaresSize is too low (less then 1.0) <<<" ) );
				return (false);
			}

      LOGF( Info ) << "subSquaresSize: " << m_shapeIn.m_subSquaresSize;

      // maxNoise (isMovable && hasDefault)
			m_shapeIn.m_maxNoise = 5.0; // default value

			x = cmdIfc->QueryValue( "maxNoise", true );
			if ( !x ) { x = cmdIfc->QueryValue( "maxNoise", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_maxNoise = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for maxNoise - using default value (5.0) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing maxNoise value - using default value (5.0) <<<";
        LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
			}

      LOGF( Info ) << "maxNoise: " << m_shapeIn.m_maxNoise;

			return (true);
		}

//-----------------------------------------------------------------------------

		bool InnerSubSquaresFramePlacer::GetObjectsParameters( IMainCommands* cmdIfc )
		{
			// resets arrays
			if (m_objectsIn.Size() != 0 )
			{
				m_objectsIn.Clear();
				m_objectsOut.Clear();
			}

			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], prob[50];
				sprintf_s( object,    "object%d",    counter );
				sprintf_s( minheight, "minheight%d", counter );
				sprintf_s( maxheight, "maxheight%d", counter );
				sprintf_s( prob,      "prob%d",      counter );

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue( object, false );
        if ( !x ) { break; }
				nfo.m_name = x;
        
        LOGF( Info ) << "object " << counter << ": " << nfo.m_name;

				// object min height
				nfo.m_minHeight = 100.0; // default value
				x = cmdIfc->QueryValue( minheight, false );
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_minHeight = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> ---------------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for minheight - using default value (100.0) <<<";
            LOGF( Warn ) << ">>> ---------------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing minheight value - using default value (100.0) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
				}

        LOGF( Info ) << "minHeight: " << nfo.m_minHeight;

				// object max height
				nfo.m_maxHeight = 100.0; // default value
				x = cmdIfc->QueryValue( maxheight, false );
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_maxHeight = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> ---------------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for maxheight - using default value (100.0) <<<";
            LOGF( Warn ) << ">>> ---------------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing maxheight value - using default value (100.0) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
				}
        
        LOGF( Info ) << "maxHeight: " << nfo.m_maxHeight;

				// object prob
				nfo.m_prob = 100.0; // default value
				x = cmdIfc->QueryValue( prob, false );
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_prob = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> ----------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for prob - using default value (100.0) <<<";
            LOGF( Warn ) << ">>> ----------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
          LOGF( Warn ) << ">>> Missing prob value - using default value (100.0) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
				}

        LOGF( Info ) << "prob: " << nfo.m_prob;

				nfo.m_counter = 0;

				m_objectsIn.Add( nfo );
				counter++;
			} 
			while ( true );

			return (true);
		}

//-----------------------------------------------------------------------------

		bool InnerSubSquaresFramePlacer::NormalizeProbs()
		{
			double total = 0.0;

			int objInCount = m_objectsIn.Size();
			for ( int i = 0; i < objInCount; ++i )
			{
				total += m_objectsIn[i].m_prob;
			}

			if ( total == 0.0 ) 
			{
				ExceptReg::RegExcpt( Exception( 41, ">>> Total probability equal to zero <<<" ) );
				return (false);
			}

			double invTotal = 1 / total;
			for ( int i = 0; i < objInCount; ++i )
			{
				m_objectsIn[i].m_normProb = m_objectsIn[i].m_prob * invTotal;
			}

			return (true);
		}

//-----------------------------------------------------------------------------

		void InnerSubSquaresFramePlacer::CreateObjects( bool preview )
		{
			Shapes::DBox bounding = m_currentShape->CalcBoundingBox();
			ProgressBar< int > pb( toLargeInt( (float)((bounding.hi.y - bounding.lo.y) / m_shapeIn.m_subSquaresSize) ) );

			for ( double y = bounding.lo.y + m_shapeIn.m_subSquaresSize * 0.5; y < bounding.hi.y; y += m_shapeIn.m_subSquaresSize ) 
			{
        if ( !preview ) { pb.AdvanceNext( 1 ); }

				Shapes::IShape* tmp = m_currentShape->Clip( 0.0, 1.0, -(y - m_shapeIn.m_subSquaresSize * 0.5) );
        if ( !tmp ) { continue; }
				Shapes::IShape* clp = tmp->Clip( 0.0, -1.0, (y + m_shapeIn.m_subSquaresSize * 0.5) );
				delete tmp;
        if ( !clp ) { continue; }

				Shapes::DBox leftright = clp->CalcBoundingBox();

				double nextTest = bounding.lo.x;
				for ( double x = floor( leftright.lo.x / m_shapeIn.m_subSquaresSize ) * m_shapeIn.m_subSquaresSize + m_shapeIn.m_subSquaresSize * 0.5; x < leftright.hi.x; x += m_shapeIn.m_subSquaresSize )
				{
					// square corners
					double xLB = x - m_shapeIn.m_subSquaresSize * 0.5;
					double yLB = y - m_shapeIn.m_subSquaresSize * 0.5;
					Shapes::DVector vLB = Shapes::DVector( xLB, yLB );
					double xRB = x + m_shapeIn.m_subSquaresSize * 0.5;
					double yRB = y - m_shapeIn.m_subSquaresSize * 0.5;
					Shapes::DVector vRB = Shapes::DVector( xRB, yRB );
					double xLT = x - m_shapeIn.m_subSquaresSize * 0.5;
					double yLT = y + m_shapeIn.m_subSquaresSize * 0.5;
					Shapes::DVector vLT = Shapes::DVector( xLT, yLT );
					double xRT = x + m_shapeIn.m_subSquaresSize * 0.5;
					double yRT = y + m_shapeIn.m_subSquaresSize * 0.5;
					Shapes::DVector vRT = Shapes::DVector( xRT, yRT );

					// accept only squares completely inside the polygon
					if ( m_currentShape->PtInside( vLB ) && m_currentShape->PtInside( vRB ) &&
					     m_currentShape->PtInside( vLT ) && m_currentShape->PtInside( vRT ) )
					{
						double randDist = RandomInRangeF( 0.0, m_shapeIn.m_maxNoise );
						double angle    = RandomInRangeF( 0.0, EtMathD::TWO_PI );

						double x0 = x + randDist * EtMathD::Cos( angle );
						double y0 = y + randDist * EtMathD::Sin( angle );
						Shapes::DVector v0 = Shapes::DVector( x0, y0 );

						if ( clp->PtInside( v0 ) )
						{
							// random type
							int type = GetRandomType();

							ModuleObjectOutput out;

							// sets data
							out.position = v0;
							out.type     = type;
							out.rotation = RandomInRangeF( 0.0, EtMathD::TWO_PI );

							double minScale = m_objectsIn[type].m_minHeight;
							double maxScale = m_objectsIn[type].m_maxHeight;

							out.scaleX = out.scaleY = out.scaleZ = RandomInRangeF( minScale, maxScale ) / 100.0;

							// used only by VLB for preview
							out.length = 10.0;
							out.width  = 10.0;
							out.height = 10.0;

							// adds to array
							m_objectsOut.Add( out );

							// update type counter
							m_objectsIn[type].m_counter++;
						}
					}
				}
				delete clp;
			}
		}

//-----------------------------------------------------------------------------

		int InnerSubSquaresFramePlacer::GetRandomType()
		{
			double p = (double)rand() / ((double)RAND_MAX + 1);

			bool found = false;
			int index  = 0;
			double curProb = m_objectsIn[index].m_normProb;
			while ( !found )
			{
				if ( p < curProb )
				{
					found = true;
				}
				else
				{
					index++;
					curProb += m_objectsIn[index].m_normProb;
				}
			}
			return (index);
		}

//-----------------------------------------------------------------------------

		void InnerSubSquaresFramePlacer::ExportCreatedObjects( IMainCommands* cmdIfc )
		{
			int objOutCount = m_objectsOut.Size();
			for ( int i = 0; i < objOutCount; ++i )
			{
				RString name      = m_objectsIn[m_objectsOut[i].type].m_name;
				Vector3D position  = Vector3D( m_objectsOut[i].position.x, 
                                       0.0, 
                                       m_objectsOut[i].position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[i].rotation );
				Matrix4D mScaling     = Matrix4D( MScale, m_objectsOut[i].scaleX, 
                                                  m_objectsOut[i].scaleZ, 
                                                  m_objectsOut[i].scaleY );

				Matrix4D transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

		void InnerSubSquaresFramePlacer::WriteReport()
		{
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Created                  ";
			int objInCount = m_objectsIn.Size();
			for ( int i = 0; i < objInCount; ++i )
			{
        LOGF( Info ) << m_objectsIn[i].m_counter << " : " << m_objectsIn[i].m_name << "                           ";
			}
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt