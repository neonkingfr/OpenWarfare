//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class SimpleRandomPlacer : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				size_t m_randomSeed;
				double m_hectareDensity;
			};

			struct ObjectInput
			{
				RString m_name;
				int     m_counter;

				ObjectInput() 
        {
        }

				ObjectInput( const RString& name )
        : m_name( name )
        , m_counter( 0 )
				{
				}

				ClassIsMovable( ObjectInput );
			};

		private:
			struct ShapeGeo
			{
				double m_minX;
				double m_maxX;
				double m_minY;
				double m_maxY;

        double WidthBB() const
        {
			    return (m_maxX - m_minX);
        }

        double HeightBB() const
        {
			    return (m_maxY - m_minY);
        }

        double AreaBB() const
        {
          return (WidthBB() * HeightBB());
        }

        double AreaHectaresBB() const
        {
          return (AreaBB() / 10000.0);
        }
      };

		public:
      SimpleRandomPlacer();

      virtual ~SimpleRandomPlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                             SimpleRandomPlacer::ObjectInput& objectsIn );

      DECLAREMODULEEXCEPTION( "SimpleRandomPlacer" )

		private:
      string m_version;
      size_t m_shapeCounter;

			const Shapes::IShape* m_currentShape;      

			ShapeGeo m_shpGeo;
			int      m_numberOfIndividuals;

			ObjectInput             m_objectsIn;
			ModuleObjectOutputArray m_objectsOut;

			ShapeInput m_shapeIn;

			bool GetShapeGeoData();
			bool GetShapeParameters( IMainCommands* cmdIfc );
			bool GetObjectsParameters( IMainCommands* cmdIfc );

			void CreateObjects( bool preview );
			void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
