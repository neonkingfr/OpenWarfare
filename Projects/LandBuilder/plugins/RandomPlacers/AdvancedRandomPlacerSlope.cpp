//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include ".\AdvancedRandomPlacerSlope.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		AdvancedRandomPlacerSlope::AdvancedRandomPlacerSlope()
    : m_version( "1.1.1" )
    , m_mapLoaded( false )
    , m_shapeCounter( 0 )
		{
		}

//-----------------------------------------------------------------------------

		void AdvancedRandomPlacerSlope::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_mapLoaded    = false;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();  

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "AdvancedRandomPlacerSlope " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";

			// gets shape geometric data
			if ( GetShapeGeoData() )
			{
        // gets maps parameters from calling cfg file
        if ( GetMapParameters( cmdIfc ) ) 
        {
          // load maps
          if ( LoadHighMap() )
          {
            LOGF( Info ) << "Parameters:";

				    // gets shape parameters from calling cfg or dbf file
				    if ( GetShapeParameters( cmdIfc ) )
				    {
              // gets objects parameters from calling cfg file
              if ( GetObjectsParameters( cmdIfc ) )
              {
                // normalizes probabilities
                if ( NormalizeProbs() )
                {
                  // initializes random seed
                  srand( m_shapeIn.m_randomSeed );

							    // calculates the number of individual to be generated
							    // (this value is referred to the bounding box of the shape
							    //  and assume a uniform distribution - random generated values
							    //  will be filtered to obtain the same density inside the shape,
							    //  discarding the ones which will fall out of the shape itself)
							    m_numberOfIndividuals = static_cast< int >( m_shpGeo.AreaHectaresBB() * m_shapeIn.m_hectareDensity );

							    // sets the population
							    CreateObjects( false );

							    // export objects
							    ExportCreatedObjects( cmdIfc );

							    // writes the report
							    WriteReport();
                }
              }
						}
					}
				}
			}       
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* AdvancedRandomPlacerSlope::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, MapInput& mapIn,
                                                                      AutoArray< AdvancedRandomPlacerSlope::ObjectInput >& objectsIn )
		{
			m_currentShape  = shape;
			m_shapeIn       = shapeIn;
			m_mapIn         = mapIn;
			m_objectsIn     = objectsIn;

      if ( !m_currentShape ) { return (NULL); }

      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			// gets shape geometric data
			if ( GetShapeGeoData() )
			{
        // load maps
        if ( LoadHighMap() )
        {
          // normalizes probabilities
          if ( NormalizeProbs() )
          {
					  // initializes random seed
					  srand( m_shapeIn.m_randomSeed );

					  // calculates the number of individual to be generated
					  // (this value is referred to the bounding box of the shape
					  //  and assume a uniform distribution - random generated values
					  //  will be filtered to obtain the same density inside the shape,
					  //  discarding the ones which will fall out of the shape itself)
					  m_numberOfIndividuals = static_cast< int >( m_shpGeo.AreaHectaresBB() * m_shapeIn.m_hectareDensity );

					  // sets the population
					  CreateObjects( true );

					  return (&m_objectsOut);
          }
				}
				else
				{
					return (NULL);
				}
			}
			else
			{
				return (NULL);
			}
		}

//-----------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::GetShapeGeoData()
		{
			Shapes::DBox bBox = m_currentShape->CalcBoundingBox();

			m_shpGeo.m_minX = bBox.lo.x;
			m_shpGeo.m_minY = bBox.lo.y;
			m_shpGeo.m_maxX = bBox.hi.x;
			m_shpGeo.m_maxY = bBox.hi.y;

			return (true);
		}

//-----------------------------------------------------------------------------

    bool AdvancedRandomPlacerSlope::GetShapeParameters( IMainCommands* cmdIfc )
		{
      // randomSeed (isMovable)
			const char* x = cmdIfc->QueryValue( "randomSeed", true );
      if ( !x ) { x = cmdIfc->QueryValue( "randomSeed", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_randomSeed = static_cast< size_t >( atoi( x ) );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for randomSeed <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing randomSeed value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "randomSeed: " << m_shapeIn.m_randomSeed;

      // hectareDensity (isMovable && hasDefault)
			m_shapeIn.m_hectareDensity = 100.0; // default value

			x = cmdIfc->QueryValue( "hectareDensity", true );
			if ( !x ) { x = cmdIfc->QueryValue( "hectareDensity", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_hectareDensity = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for hectareDensity - using default value (100) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> -------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing hectareDensity value - using default value (100) <<<";
        LOGF( Warn ) << ">>> -------------------------------------------------------- <<<";
			}
			if ( m_shapeIn.m_hectareDensity < 0.01 )
			{
				ExceptReg::RegExcpt( Exception( 21, ">>> hectareDensity is too low (less then 0.01) <<<" ) );
				return (false);
			}

      LOGF( Info ) << "hectareDensity: " << m_shapeIn.m_hectareDensity;

			return (true);
		}

//-----------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::GetObjectsParameters( IMainCommands* cmdIfc )
		{
			// resets arrays
			if ( m_objectsIn.Size() != 0 )
			{
				m_objectsIn.Clear();
				m_objectsOut.Clear();
			}

			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], mindist[50], minslope[50], maxslope[50], prob[50];
				sprintf_s( object,    "object%d",    counter );
				sprintf_s( minheight, "minheight%d", counter );
				sprintf_s( maxheight, "maxheight%d", counter );
				sprintf_s( mindist,   "mindist%d",   counter );
				sprintf_s( minslope,  "minslope%d",  counter );
				sprintf_s( maxslope,  "maxslope%d",  counter );
				sprintf_s( prob,      "prob%d",      counter );

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue( object, false );
        if ( !x ) { break; }
				nfo.m_name = x;
        
        LOGF( Info ) << "object " << counter << ": " << nfo.m_name;

				// object min height
				x = cmdIfc->QueryValue( minheight, false );
				nfo.m_minHeight = 100.0; // default value
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_minHeight = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for minheight - using default value (100) <<<";
            LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> --------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing minheight value - using default value (100) <<<";
          LOGF( Warn ) << ">>> --------------------------------------------------- <<<";
				}

        LOGF( Info ) << "minHeight: " << nfo.m_minHeight;

				// object max height
				x = cmdIfc->QueryValue( maxheight, false );
				nfo.m_maxHeight = 100.0; // default value
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_maxHeight = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for maxheight - using default value (100) <<<";
            LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> --------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing maxheight value - using default value (100) <<<";
          LOGF( Warn ) << ">>> --------------------------------------------------- <<<";
				}
        
        LOGF( Info ) << "maxHeight: " << nfo.m_maxHeight;

				// object min distance
				x = cmdIfc->QueryValue( mindist, false );
				nfo.m_minDistance = 5.0; // default value
				if ( x ) 
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_minDistance = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
            LOGF( Warn ) << ">>> Bad data for mindist - using default value (5.0) <<<";
            LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing value for mindist - using default value (5.0) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
				}

        LOGF( Info ) << "minDistance: " << nfo.m_minDistance;

				// object min slope
				x = cmdIfc->QueryValue( minslope, false );
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_minSlope = atof( x );
					}
					else
					{
						ExceptReg::RegExcpt( Exception( 35, ">>> Bad data for minslope value <<<" ) );
						return (false);
					}
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 36, ">>> Missing minslope value <<<" ) );
					return (false);
				}
				
        LOGF( Info ) << "minSlope: " << nfo.m_minSlope;

				// object max slope
				x = cmdIfc->QueryValue( maxslope, false );
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_maxSlope = atof( x );
					}
					else
					{
						ExceptReg::RegExcpt( Exception( 37, ">>> Bad data for maxslope value <<<" ) );
						return (false);
					}
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 38, ">>> Missing maxslope value <<<" ) );
					return (false);
				}

        LOGF( Info ) << "maxSlope: " << nfo.m_maxSlope;

				// object prob
				nfo.m_prob = 100.0; // default value
				x = cmdIfc->QueryValue( prob, false );
				if ( x )
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_prob = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> --------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for prob - using default value (100) <<<";
            LOGF( Warn ) << ">>> --------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing value for prob - using default value (100) <<<";
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
				}

        LOGF( Info ) << "prob: " << nfo.m_prob;

				nfo.m_counter = 0;

				m_objectsIn.Add( nfo );
				counter++;
			} 
			while ( true ); 

			return (true);
		}

//-----------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::GetMapParameters( IMainCommands* cmdIfc )
		{
      if ( m_mapLoaded ) { return (true); }
      return (m_mapIn.LoadMapParameters( cmdIfc ));
		}

//-----------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::LoadHighMap()
		{
      if ( m_mapLoaded ) 
      { 
        m_mapIn.SetHmData( m_mapData );
      }
      else
      {
        if ( !m_mapIn.LoadHighMap() ) { return (false); }

			  m_mapData = m_mapIn.GetHmData();
			  m_mapLoaded = true;
      }

			return (true);
		}

//-----------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::NormalizeProbs()
		{
			double total = 0.0;

			int objInCount = m_objectsIn.Size();
			for ( int i = 0; i < objInCount; ++i )
			{
				total += m_objectsIn[i].m_prob;
			}

			if ( total == 0.0 ) 
			{
				ExceptReg::RegExcpt( Exception( 91, ">>> Total probability equal to zero <<<" ) );
				return (false);
			}

			double invTotal = 1 / total;
			for ( int i = 0; i < objInCount; ++i )
			{
				m_objectsIn[i].m_normProb = m_objectsIn[i].m_prob * invTotal;
			}
			return (true);
		}

//-----------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::PtCloseToObject( const Shapes::DVector& v ) const
		{
			int objOutCount = m_objectsOut.Size();
			for ( int j = 0; j < objOutCount; ++j )
			{
				double distSq = v.DistanceXY2( m_objectsOut[j].position );

				// is close to already existing objects ?
				double minDistance = m_objectsIn[m_objectsOut[j].type].m_minDistance;
				double minDistanceSq = minDistance * minDistance; 
				if ( distSq < minDistanceSq )
				{
					return (true);
				}
			}
			return (false);
		}

//-----------------------------------------------------------------------------

		void AdvancedRandomPlacerSlope::CreateObjects( bool preview )
		{
			ProgressBar< int > pb( m_numberOfIndividuals );
			
			for ( int i = 0; i < m_numberOfIndividuals; ++i )
			{
        if ( !preview ) { pb.AdvanceNext( 1 ); }

				// random position
				double x0 = RandomInRangeF( 0, m_shpGeo.WidthBB() );
				double y0 = RandomInRangeF( 0, m_shpGeo.HeightBB() );
				Shapes::DVector v0 = Shapes::DVector( x0 + m_shpGeo.m_minX, y0 + m_shpGeo.m_minY );

				// the random point is inside the current shape ?
				if ( m_currentShape->PtInside( v0 ) )
				{
					if ( !PtCloseToObject( v0 ) )
					{
						// random type
						int type = GetRandomType();

						double slope = m_mapIn.GetHmData().getWorldSlopeAt( v0.x, v0.y, EtRectElevationGrid< double, int >::imARMA ) * 100.0;

						if ( (slope >= m_objectsIn[type].m_minSlope) && (slope <= m_objectsIn[type].m_maxSlope) )
						{
							ModuleObjectOutput out;

							// sets data
							out.position = v0;
							out.type     = type;
							out.rotation = RandomInRangeF( 0, EtMathD::TWO_PI );

							double minScale = m_objectsIn[type].m_minHeight;
							double maxScale = m_objectsIn[type].m_maxHeight;

							out.scaleX = out.scaleY = out.scaleZ = RandomInRangeF( minScale, maxScale ) / 100.0;

							// used only by VLB for preview
							out.length   = 10.0;
							out.width    = 10.0;
							out.height   = 10.0;

							// adds to array
							m_objectsOut.Add( out );

							// update type counter
							m_objectsIn[type].m_counter++;
						}
					}
				}
			}
		}

//-----------------------------------------------------------------------------

		int AdvancedRandomPlacerSlope::GetRandomType()
		{
			double p = (double)rand() / ((double)RAND_MAX + 1);

			bool found = false;
			int index  = 0;
			double curProb = m_objectsIn[index].m_normProb;
			while ( !found )
			{
				if ( p < curProb )
				{
					found = true;
				}
				else
				{
					index++;
					curProb += m_objectsIn[index].m_normProb;
				}
			}
			return (index);
		}

//-----------------------------------------------------------------------------

		void AdvancedRandomPlacerSlope::ExportCreatedObjects( IMainCommands* cmdIfc )
		{
			int objOutCount = m_objectsOut.Size();
			for ( int i = 0; i < objOutCount; ++i )
			{
				RString name      = m_objectsIn[m_objectsOut[i].type].m_name;
				Vector3D position  = Vector3D( m_objectsOut[i].position.x, 
                                       0.0, 
                                       m_objectsOut[i].position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[i].rotation );
				Matrix4D mScaling     = Matrix4D( MScale, m_objectsOut[i].scaleX, 
                                                  m_objectsOut[i].scaleZ, 
                                                  m_objectsOut[i].scaleY );

				Matrix4D transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

		void AdvancedRandomPlacerSlope::WriteReport()
		{
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Created                  ";
			int objInCount = m_objectsIn.Size();
			for ( int i = 0; i < objInCount; ++i )
			{
        LOGF( Info ) << m_objectsIn[i].m_counter << " : " << m_objectsIn[i].m_name << "                           ";
			}
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt