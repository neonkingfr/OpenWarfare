//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"

#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class FramedForestPlacer : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				size_t m_randomSeed;
				double m_mainDirection;
				double m_subSquaresSize;
				double m_maxNoise;
				double m_minDistance;
				size_t m_numBoundaryObjects;
			};

			struct ObjectInput
			{
				RString m_name;
				double  m_prob;
				double  m_normProb;
				double  m_minHeight;
				double  m_maxHeight;
				bool    m_inner;
				int     m_counter;

				ObjectInput() 
        {
        }

				ObjectInput( RString name, double prob, double minHeight, double maxHeight, bool inner )
        : m_name( name )
        , m_prob( prob )
        , m_minHeight( minHeight )
        , m_maxHeight( maxHeight )
        , m_inner( inner )
        , m_counter( 0 )
				{
				}

				ClassIsMovable( ObjectInput );
			};

		public:
      FramedForestPlacer();

      virtual ~FramedForestPlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                             AutoArray< FramedForestPlacer::ObjectInput >& objectsIn );

      DECLAREMODULEEXCEPTION( "FramedForestPlacer" )

		private:
      string m_version;
      size_t m_shapeCounter;

			int m_innerCounter;
			int m_outerCounter;

			Shapes::IShape* m_currentShape;      

			AutoArray< ObjectInput > m_objectsIn;
			ModuleObjectOutputArray  m_objectsOut;

			ShapeInput m_shapeIn;

			bool GetShapeParameters( IMainCommands* cmdIfc );
			bool GetObjectsParameters( IMainCommands* cmdIfc );

			bool NormalizeProbs( bool inner );

			void   CreateObjects( bool preview );
			void   CreateInnerObjects( double x, double y, Shapes::IShape* clp );
			void   CreateOuterObjects( double x, double y );
			int    GetRandomType( bool inner );
			double SquareDistanceFromPolygon( const Shapes::DVector& vLB, const Shapes::DVector& vRB, 
                                        const Shapes::DVector& vRT, const Shapes::DVector& vLT );
			void   RotateCreatedObjects( double angle );
			void   ExportCreatedObjects( IMainCommands* cmdIfc );
			void   WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
