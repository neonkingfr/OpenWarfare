//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include <algorithm>

#include ".\simplerandomplacerslope.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		SimpleRandomPlacerSlope::SimpleRandomPlacerSlope()
    : m_version( "1.1.1" )
    , m_mapLoaded( false )
    , m_shapeCounter( 0 )
		{
		}

//-----------------------------------------------------------------------------

		void SimpleRandomPlacerSlope::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_mapLoaded    = false;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();      

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "SimpleRandomPlacerSlope " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";

			// gets shape geometric data
			if ( GetShapeGeoData() )
			{
        // gets maps parameters from calling cfg file
        if ( GetMapParameters( cmdIfc ) ) 
        {
          // load maps
          if ( LoadHighMap() )
          {
            LOGF( Info ) << "Parameters:";

				    // gets shape parameters from calling cfg file
				    if ( GetShapeParameters( cmdIfc ) )
				    {
						  // gets objects parameters from calling cfg file
						  if ( GetObjectsParameters( cmdIfc ) )
						  {
							  // initializes random seed
							  srand( m_shapeIn.m_randomSeed );

							  // calculates the number of individual to be generated
							  // (this value is referred to the bounding box of the shape
							  //  and assume a uniform distribution - random generated values
							  //  will be filtered to obtain the same density inside the shape,
							  //  discarding the ones which will fall out of the shape itself)
							  m_numberOfIndividuals = static_cast< int >( m_shpGeo.AreaHectaresBB() * m_shapeIn.m_hectareDensity );

							  // sets the population
							  CreateObjects( false );

							  // export objects
							  ExportCreatedObjects( cmdIfc );

							  // writes the report
							  WriteReport();
              }
						}
					}
				}
			}       
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* SimpleRandomPlacerSlope::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, MapInput& mapIn,
                                                                    SimpleRandomPlacerSlope::ObjectInput& objectsIn )
		{
			m_currentShape = shape;
			m_shapeIn      = shapeIn;
			m_mapIn        = mapIn;
			m_objectsIn    = objectsIn;

      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

      if ( !m_currentShape ) { return (NULL); }

			// gets shape geometric data
			if ( GetShapeGeoData() )
			{
        // load maps
        if ( LoadHighMap() )
        {
				  // initializes random seed
				  srand( m_shapeIn.m_randomSeed );

				  // calculates the number of individual to be generated
				  // (this value is referred to the bounding box of the shape
				  //  and assume a uniform distribution - random generated values
				  //  will be filtered to obtain the same density inside the shape,
				  //  discarding the ones which will fall out of the shape itself)
				  m_numberOfIndividuals = static_cast< int >( m_shpGeo.AreaHectaresBB() * m_shapeIn.m_hectareDensity );

				  // sets the population
				  CreateObjects( true );

				  return (&m_objectsOut);
        }
			}
			else
			{
				return (NULL);
			}
		}

//-----------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::GetShapeGeoData()
		{
			Shapes::DBox bBox = m_currentShape->CalcBoundingBox();

			m_shpGeo.m_minX = bBox.lo.x;
			m_shpGeo.m_minY = bBox.lo.y;
			m_shpGeo.m_maxX = bBox.hi.x;
			m_shpGeo.m_maxY = bBox.hi.y;

			return (true);
		}

//-----------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::GetShapeParameters( IMainCommands* cmdIfc )
		{
      // randomSeed (isMovable)
			const char* x = cmdIfc->QueryValue( "randomSeed", true );
			if ( !x ) { x = cmdIfc->QueryValue( "randomSeed", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_randomSeed = static_cast< size_t >( atoi( x ) );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for randomSeed <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing value for randomSeed <<<" ) );
				return (false);
			}

      LOGF( Info ) << "randomSeed: " << m_shapeIn.m_randomSeed;

			// hectareDensity (isMovable && hasDefault)
			m_shapeIn.m_hectareDensity = 100.0; // default value

			x = cmdIfc->QueryValue( "hectareDensity", true );
			if ( !x ) { x = cmdIfc->QueryValue( "hectareDensity", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_hectareDensity = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for hectareDensity - using default value (100) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------------ <<<";
        LOGF( Warn ) << ">>> Missing value for hectareDensity - using default value (100) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------------ <<<";
			}
			if ( m_shapeIn.m_hectareDensity < 0.01 )
			{
				ExceptReg::RegExcpt( Exception( 21, ">>> Density is too low (less then 0.01) <<<" ) );
				return (false);
			}

      LOGF( Info ) << "hectareDensity: " << m_shapeIn.m_hectareDensity;

			return (true);
		}

//-----------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::GetObjectsParameters( IMainCommands* cmdIfc )
		{
			// resets arrays
      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			char object[50]   = "object";
			char minslope[50] = "minslope";
			char maxslope[50] = "maxslope";

			// object type
			const char* x = cmdIfc->QueryValue( object, false );
      if ( !x ) { return (false); }
			m_objectsIn.m_name = x;

      LOGF( Info ) << "object: " << m_objectsIn.m_name;

			// object min slope
			x = cmdIfc->QueryValue( minslope, false );
			if ( x )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_objectsIn.m_minSlope = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 31, ">>> Bad data for minslope <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 32, ">>> Missing minslope value <<<" ) );
				return (false);
			}
			
      LOGF( Info ) << "minslope: " << m_objectsIn.m_minSlope;

			// object max slope
			x = cmdIfc->QueryValue( maxslope, false );
			if ( x )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_objectsIn.m_maxSlope = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 33, ">>> Bad data for maxslope <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 34, ">>> Missing maxslope value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "maxslope: " << m_objectsIn.m_maxSlope;

			m_objectsIn.m_counter = 0;

			return (true);
		}

//-----------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::GetMapParameters( IMainCommands* cmdIfc )
		{
      if ( m_mapLoaded ) { return (true); }
      return (m_mapIn.LoadMapParameters( cmdIfc ));
		}

//-----------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::LoadHighMap()
		{
      if ( m_mapLoaded ) 
      { 
        m_mapIn.SetHmData( m_mapData );
      }
      else
      {
        if ( !m_mapIn.LoadHighMap() ) { return (false); }

			  m_mapData = m_mapIn.GetHmData();
			  m_mapLoaded = true;
      }

			return (true);
		}

//-----------------------------------------------------------------------------

		void SimpleRandomPlacerSlope::CreateObjects( bool preview )
		{
			ProgressBar< int > pb( m_numberOfIndividuals );
			
			for ( int i = 0; i < m_numberOfIndividuals; ++i )
			{
        if ( !preview ) { pb.AdvanceNext( 1 ); }

				// random position
				double x0 = RandomInRangeF( 0.0, m_shpGeo.WidthBB() );
				double y0 = RandomInRangeF( 0.0, m_shpGeo.HeightBB() );
				Shapes::DVector v0 = Shapes::DVector( x0 + m_shpGeo.m_minX, y0 + m_shpGeo.m_minY );

				// the random point is inside the current shape ?
				if ( m_currentShape->PtInside( v0 ) )
				{
					double slope = m_mapIn.GetHmData().getWorldSlopeAt( v0.x, v0.y, EtRectElevationGrid< double, int >::imARMA ) * 100.0;

					if ( (slope >= m_objectsIn.m_minSlope) && (slope <= m_objectsIn.m_maxSlope) )
					{
						ModuleObjectOutput out;

						// sets data
						out.position = v0;
						out.type     = 0;
						out.rotation = RandomInRangeF( 0.0, EtMathD::TWO_PI );
						out.scaleX = out.scaleY = out.scaleZ = 1.0;

						// used only by VLB for preview
						out.length = 10.0;
						out.width  = 10.0;
						out.height = 10.0;

						// adds to array
						m_objectsOut.Add( out );

						// update type counter
						m_objectsIn.m_counter++;
					}
				}
			}
		}

//-----------------------------------------------------------------------------

		void SimpleRandomPlacerSlope::ExportCreatedObjects( IMainCommands* cmdIfc )
		{
			int objOutCount = m_objectsOut.Size();
			for ( int i = 0; i < objOutCount; ++i )
			{
				RString name = m_objectsIn.m_name;
				Vector3D position = Vector3D( m_objectsOut[i].position.x, 
                                                             0.0, 
                                      m_objectsOut[i].position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[i].rotation );
				Matrix4D mScaling     = Matrix4D( MScale, m_objectsOut[i].scaleX, 
                                                  m_objectsOut[i].scaleZ, 
                                                  m_objectsOut[i].scaleY );

				Matrix4D transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

		void SimpleRandomPlacerSlope::WriteReport()
		{
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Created                  ";
      LOGF( Info ) << m_objectsIn.m_counter << " : " << m_objectsIn.m_name << "                           ";
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt