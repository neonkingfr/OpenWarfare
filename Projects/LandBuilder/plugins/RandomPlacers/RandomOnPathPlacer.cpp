//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\Shapes\IShape.h"
#include "..\..\Shapes\ShapeHelpers.h"

#include "..\..\HighMapLoaders\include\EtMath.h"
#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"

#include ".\RandomOnPathPlacer.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

    RandomOnPathPlacer::RandomOnPathPlacer()
    : m_version( "1.1.1" )
    , m_shapeCounter( 0 )
    {
    }

//-----------------------------------------------------------------------------

		void RandomOnPathPlacer::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();      

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "RandomOnPathPlacer " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

			// gets shape geometric data
			if ( GetShapeGeoData() )
			{
				// gets shape parameters from calling cfg file
				if ( GetShapeParameters( cmdIfc ) )
				{
					// gets objects parameters from calling cfg file
					if ( GetObjectsParameters( cmdIfc ) )
					{
						// normalizes probabilities
						if ( NormalizeProbs() )
						{
							// initializes random seed
							srand( m_shapeIn.m_randomSeed );

							// calculates the number of individual to be generated
							m_numberOfIndividuals = static_cast< int >( m_shpGeo.m_perimeter * m_shapeIn.m_linearDensity / 100.0 );

							// sets the population
							CreateObjects( FALSE);

							// export objects
							ExportCreatedObjects( cmdIfc );

							// writes the report
							WriteReport();
						}
					}
				}
			}
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* RandomOnPathPlacer::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                                               AutoArray< RandomOnPathPlacer::ObjectInput >& objectsIn )
		{
			m_currentShape = shape;
			m_shapeIn      = shapeIn;
			m_objectsIn    = objectsIn;

      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

      if ( !m_currentShape ) { return (NULL); }

			// gets shape geometric data
			if ( GetShapeGeoData() )
			{
				// normalizes probabilities
				if ( NormalizeProbs() )
				{
					// initializes random seed
					srand( m_shapeIn.m_randomSeed );

					// calculates the number of individual to be generated
					m_numberOfIndividuals = static_cast< int >( m_shpGeo.m_perimeter * m_shapeIn.m_linearDensity / 100.0 );

					// sets the population
					CreateObjects( true );

					return (&m_objectsOut);
				}
				else
				{
					return (NULL);
				}
			}
			else
			{
				return (NULL);
			}
		}

//-----------------------------------------------------------------------------

		bool RandomOnPathPlacer::GetShapeGeoData()
		{
			m_shpGeo.m_perimeter = GetShapePerimeter( m_currentShape );
			return (true);
		}

//-----------------------------------------------------------------------------

		bool RandomOnPathPlacer::GetShapeParameters( IMainCommands* cmdIfc )
		{
      // maxDistance (isMovable && hasDefault)
			m_shapeIn.m_maxDistance = 10.0; // default value

			const char* x = cmdIfc->QueryValue( "maxDistance", true );
			if ( !x ) { x = cmdIfc->QueryValue( "maxDistance", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_maxDistance = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> --------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for maxDistance - using default value (10) <<<";
          LOGF( Warn ) << ">>> --------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> -------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for maxDistance - using default value (10) <<<";
        LOGF( Warn ) << ">>> -------------------------------------------------------- <<<";
			}

      LOGF( Info ) << "maxDistance: " << m_shapeIn.m_maxDistance;


      // randomSeed (isMovable)
      x = cmdIfc->QueryValue( "randomSeed", true );
			if ( !x ) { x = cmdIfc->QueryValue( "randomSeed", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_randomSeed = static_cast< size_t >( atoi( x ) );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for randomSeed <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing value for randomSeed <<<" ) );
				return (false);
			}

      LOGF( Info ) << "randomSeed: " << m_shapeIn.m_randomSeed;

      // linearDensity (isMovable && hasDefault)
			m_shapeIn.m_linearDensity = 10.0; // default value

      x = cmdIfc->QueryValue( "linearDensity", true );
			if ( !x ) { x = cmdIfc->QueryValue( "linearDensity", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_linearDensity = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for linearDensity - using default value (10) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ---------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for linearDensity - using default value (10) <<<";
        LOGF( Warn ) << ">>> ---------------------------------------------------------- <<<";
			}
			if ( m_shapeIn.m_linearDensity < 0.01 )
			{
				ExceptReg::RegExcpt( Exception( 21, ">>> Density is too low (less then 0.01) <<<" ) );
				return (false);
			}

      LOGF( Info ) << "linearDensity: " << m_shapeIn.m_linearDensity;

			return (true);
		}

//-----------------------------------------------------------------------------

		bool RandomOnPathPlacer::GetObjectsParameters( IMainCommands* cmdIfc )
		{
			// resets arrays
			if( m_objectsIn.Size() != 0 )
			{
				m_objectsIn.Clear();
				m_objectsOut.Clear();
			}

			// gets objects' data from calling cfg file
			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], prob[50];
				sprintf_s( object   , "object%d"   , counter );
				sprintf_s( minheight, "minheight%d", counter );
				sprintf_s( maxheight, "maxheight%d", counter );
				sprintf_s( prob     , "prob%d"     , counter );

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue( object, false );
        if ( !x ) { break; }
				nfo.m_name = x;

        LOGF( Info ) << "object " << counter << ": " << nfo.m_name;

				// object min height
				x = cmdIfc->QueryValue( minheight, false );
				nfo.m_minHeight = 100.0; // default value
				if ( x ) 
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_minHeight = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Missing value for minheight - using default value (100) <<<";
            LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing value for minheight - using default value (100) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
				}

        LOGF( Info ) << "minHeight: " << nfo.m_minHeight;

				// object max height
				x = cmdIfc->QueryValue( maxheight, false );
				nfo.m_maxHeight = 100.0; // default value
				if ( x ) 
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_maxHeight = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Missing value for maxheight - using default value (100) <<<";
            LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing value for maxheight - using default value (100) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
				}

        LOGF( Info ) << "maxHeight: " << nfo.m_maxHeight;

				// object probability
				x = cmdIfc->QueryValue( prob, false );
				nfo.m_prob = 100.0; // default value
				if ( x ) 
				{
					if ( IsFloatingPoint( string( x ), false ) )
					{
						nfo.m_prob = atof( x );
					}
					else
					{
            LOGF( Warn ) << ">>> --------------------------------------------- <<<";
            LOGF( Warn ) << ">>> Bad data for prob - using default value (100) <<<";
            LOGF( Warn ) << ">>> --------------------------------------------- <<<";
					}
				}
				else
				{
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Missing value for prob - using default value (100) <<<";
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
				}

        LOGF( Info ) << "prob: " << nfo.m_prob;

				// object counter
				nfo.m_counter = 0;

				m_objectsIn.Add( nfo );
				counter++;
			} 
			while ( true );   

			return (true);
		}

//-----------------------------------------------------------------------------

		bool RandomOnPathPlacer::NormalizeProbs()
		{
			double total = 0.0;

			for ( int i = 0; i < m_objectsIn.Size(); ++i )
			{
				total += m_objectsIn[i].m_prob;
			}

			if ( total == 0.0 ) 
			{
				ExceptReg::RegExcpt( Exception( 31, ">>> Total probability equal to zero <<<" ) );
				return (false);
			}

			double invTotal = 1 / total;
			for ( int i = 0; i < m_objectsIn.Size(); ++i )
			{
				m_objectsIn[i].m_normProb = m_objectsIn[i].m_prob * invTotal;
			}
			return (true);
		}

//-----------------------------------------------------------------------------

		int RandomOnPathPlacer::GetRandomType()
		{
			double p = static_cast< double >( rand() ) / (static_cast< double >( RAND_MAX ) + 1);

			bool found = false;
			int  index = 0;
			double curProb = m_objectsIn[index].m_normProb;
			while ( !found )
			{
				if ( p < curProb )
				{
					found = true;
				}
				else
				{
					index++;
					curProb += m_objectsIn[index].m_normProb;
				}
			}
			return (index);
		}

//-----------------------------------------------------------------------------

		void RandomOnPathPlacer::CreateObjects( bool preview )
		{
			ProgressBar< int > pb( m_numberOfIndividuals );
			
			for ( int i = 0; i < m_numberOfIndividuals; i++ )
			{
        if ( !preview ) { pb.AdvanceNext( 1 ); }

				// random type
				int type = GetRandomType();

				// random coordinate on perimeter
				double s = RandomInRangeF( 0.0, m_shpGeo.m_perimeter );

				// convert to XY
				Shapes::DVertex v = ConvertToXY( s );

				// random polar coordinate from point on perimeter
				double dist  = RandomInRangeF( 0.0, m_shapeIn.m_maxDistance );
				double angle = RandomInRangeF( 0.0, EtMathD::TWO_PI );
				double x0 = v.x + dist * cos( angle );
				double y0 = v.y + dist * sin( angle );

				// new object
				ModuleObjectOutput out;

				// sets objects out data
				// type
				out.type     = type;
				// position
				out.position = Shapes::DVertex( x0, y0 );
				// rotation
				out.rotation = RandomInRangeF( 0.0, EtMathD::TWO_PI );
				// scale
				double minScale = m_objectsIn[type].m_minHeight;
				double maxScale = m_objectsIn[type].m_maxHeight;
				double scale = RandomInRangeF( minScale, maxScale );
				out.scaleX = out.scaleY = out.scaleZ = scale / 100.0;

				// used only by VLB for preview
				out.length = 10.0;
				out.width  = 10.0;
				out.height = 10.0;

				// adds to array
				m_objectsOut.Add( out );

				// update type counter
				m_objectsIn[type].m_counter++;
			}
		}

//-----------------------------------------------------------------------------

		Shapes::DVertex RandomOnPathPlacer::ConvertToXY( double s )
		{
			Shapes::DVertex v1;
			Shapes::DVertex v2;
			double dist;

			int vCount = m_currentShape->GetVertexCount();

			int index = -1;
			double tempPos = 0.0;

			for ( int i = 0; i < (vCount - 1); ++i )
			{
				v1 = m_currentShape->GetVertex( i );
				v2 = m_currentShape->GetVertex( i + 1 );
				dist = v1.DistanceXY( v2 );
				tempPos += dist;
				if ( s < tempPos )
				{
					index = i;
					break;
				}
			}

			if ( index == -1 )
			{
				v1 = m_currentShape->GetVertex( vCount - 1 );
				v2 = m_currentShape->GetVertex( 0 );
				dist = v1.DistanceXY( v2 );
				tempPos += dist;
			}
			else
			{
				v1 = m_currentShape->GetVertex( index );
				v2 = m_currentShape->GetVertex( index + 1 );
			}

			dist = v1.DistanceXY( v2 );			
			Shapes::DVector diff = v2 - v1;

			double localS = tempPos - s;
			double delta = 0.0;

			if ( dist != 0.0 )
			{
				delta = localS / dist;
			}

			double newX = v1.x + diff.x * delta;
			double newY = v1.y + diff.y * delta;

			return (Shapes::DVertex( newX, newY )); 
		}

//-----------------------------------------------------------------------------

		void RandomOnPathPlacer::ExportCreatedObjects( IMainCommands* cmdIfc )
		{
			int objOutCount = m_objectsOut.Size();
			for ( int i = 0; i < objOutCount; ++i )
			{
				RString name = m_objectsIn[m_objectsOut[i].type].m_name;
				Vector3D position = Vector3D( m_objectsOut[i].position.x, 
                                                             0.0, 
                                      m_objectsOut[i].position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[i].rotation );
				Matrix4D mScaling     = Matrix4D( MScale, m_objectsOut[i].scaleX, 
                                                  m_objectsOut[i].scaleZ, 
                                                  m_objectsOut[i].scaleY );

				Matrix4D transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

		void RandomOnPathPlacer::WriteReport()
		{
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Created                  ";
			for ( int i = 0; i < m_objectsIn.Size(); ++i )
			{
        LOGF( Info ) << m_objectsIn[i].m_counter << " : " << m_objectsIn[i].m_name << "                           ";
			}
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt