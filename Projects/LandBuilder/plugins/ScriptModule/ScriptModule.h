//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <ibuildmodule.h>
#include <el/Evaluator/express.hpp>

//-----------------------------------------------------------------------------

class O2ScriptClass;

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
  namespace Modules
  {

//-----------------------------------------------------------------------------

    class ScriptModule : public IBuildModule, public GameState
    {
/*
#if USE_PRECOMPILATION
      GameCodeType _code;      
#else
      RString _code;
#endif
*/    
      RString        m_code;
      IMainCommands* m_cmdIfc;

    public:
      bool LoadScript( const char* script );
      virtual void Run( IMainCommands* cmdIfc );

      DECLAREMODULEEXCEPTION( "ScriptModule" )

      static ScriptModule& Instance( GameState& gs ) 
      {
        return (static_cast< ScriptModule& >( gs ));
      }

      static const ScriptModule& Instance( const GameState& gs ) 
      {
        return (static_cast< const ScriptModule& >( gs ));
      }

      static IMainCommands* CmdIfc( GameState& gs ) 
      {
        return (Instance( gs ).m_cmdIfc);
      }

      static IMainCommands* CmdIfc( const GameState& gs) 
      {
        return (Instance( const_cast< GameState& >( gs ) ).m_cmdIfc);
      }
      
      ProgressBar< float >* m_progress;
    };

//-----------------------------------------------------------------------------

  } // namespace Modules
} // namespace LandBuildInt
