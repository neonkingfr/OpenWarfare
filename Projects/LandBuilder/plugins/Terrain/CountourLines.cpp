//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include ".\countourlines.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{

//-----------------------------------------------------------------------------

  using namespace Shapes;

//-----------------------------------------------------------------------------

  void CountourLines::AddShape( const ShapePolygon& shape, double height, int segment )
  {
    if ( segment == -1 )
    {
      m_heights.Add( height );
      m_clines.Add( static_cast< ShapePolygon* >( shape.Copy( &m_vxArr ) ) );
    }
    else
    {
      if ( segment < 0 || segment >= (signed)shape.GetPartCount()) { return; }

      class RetrieveData : public ShapePolygon
      {
      public:
        size_t* GetVertices( size_t part ) const
        {
          return (const_cast< size_t* >( m_points.Data() + GetPartIndex( part ) ));
        }
      };

      Array< size_t > vex( static_cast< const RetrieveData& >( shape ).GetVertices( segment ), shape.GetPartSize( segment ) );
      const VertexArray* src = shape.GetVertexArray();
      for ( int i = 0, cnt = vex.Size(); i < cnt; ++i ) 
      {
        vex[i] = m_vxArr.AddVertex( src->GetVertex( vex[i] ) );
      }

      size_t zr = 0;
      m_clines.Add( new ShapePolygon( vex, Array< size_t >( &zr, 0 ), &m_vxArr ) );
    }
  }

//-----------------------------------------------------------------------------

  double CountourLines::GetHeight( const HeightMapLocInfo& heightInfo ) const
  {
    double bestInsideDist  = FLT_MAX;
    double bestOutsideDist = FLT_MAX;
    int bestInsideId  = -1;
    int bestOutsideId = -1;

    DVector vec( heightInfo.GetX(), heightInfo.GetY() );

    for ( int i = 0, cnt = m_clines.Size(); i < cnt; ++i )
    {
      const ShapePolygon* p = m_clines[i];
      bool inside = p->PtInside( vec );
      DVector nr = p->NearestToEdge( vec, true );
      double d = nr.DistanceXY2( vec );
      if ( inside )
      {
        if ( d < bestInsideDist )
        {
          bestInsideDist = d;
          bestInsideId   = i;
        }
      }
      else
      {
        if ( d < bestOutsideDist )
        {
          bestOutsideDist = d;
          bestOutsideId   = i;
        }
      }
    }

    double finHeight;
    if ( bestInsideId == -1 )
    {
      if ( bestOutsideId == -1 ) { return (heightInfo.GetAlt()); }
      finHeight = m_heights[bestOutsideId];
    }
    else if ( bestOutsideId == -1 )
    {
      finHeight = m_heights[bestInsideId];
    }
    else
    {
      double d = sqrt( bestOutsideDist ) + sqrt( bestInsideDist );
      double f = sqrt( bestInsideDist ) / d;
      double h1 = m_heights[bestInsideId];
      double h2 = m_heights[bestOutsideId];
      finHeight = (1 - f) * h1 + f * h2;
    }
    return (CombineHeights( heightInfo.GetAlt(), finHeight ));
  }

//-----------------------------------------------------------------------------

  void CountourLines::AddShapeAuto( const Shapes::ShapePolygon& shape, double baseHeight, double step, 
                                    int segment )
  {
    if ( segment == -1 ) 
    {
      for ( size_t i = 0, cnt = shape.GetPartCount(); i < cnt; ++i ) 
      {
        AddShapeAuto( shape, baseHeight, step, i );
      }
    }
    else
    {
      const DVertex& vx = shape.GetVertex( shape.GetPartIndex( (size_t)segment ) );
      double optimalHeight = baseHeight;
      for ( int i = m_clines.Size() - 1; i >= 0; --i )
      {
        if ( m_heights[i] > optimalHeight && m_clines[i]->PtInside( vx ) ) 
        {
          optimalHeight = m_heights[i];
        }
      }
      optimalHeight += step;
      AddShape( shape, optimalHeight, segment );
    }
  }

//-----------------------------------------------------------------------------

  namespace Modules
  {

//-----------------------------------------------------------------------------

    void CountourLines::Run( IMainCommands* cmds )
    {
      double baseHeight;
      double step;
      const char* tmp;
      int mode;
      double refHeight;

      if ( (tmp = cmds->QueryValue( "baseHeight" ) ) == 0 || 
           (sscanf( tmp, "%f", &baseHeight) != 1) )
      {
        ExceptReg::RegExcpt( Exception( 1, "Missing baseHeight parameter" ) );
        return;
      }

      if ( (tmp = cmds->QueryValue( "step" ) ) == 0 || 
           (sscanf( tmp, "%f", &step ) != 1) )
      {
        ExceptReg::RegExcpt( Exception( 2, "Missing step parameter" ) );
        return;
      }

      tmp = cmds->QueryValue( "mode" );
      if ( tmp == 0 )
      {
        ExceptReg::RegExcpt( Exception( 3, "Missing mode parameter" ) );
        return;
      }

      if ( _strcmpi( tmp, "absolute" ) == 0 )            { mode = 0; }
      else if ( _strcmpi( tmp, "relative" ) == 0 )       { mode = 1; }
      else if ( _strcmpi( tmp, "weight" ) == 0 )         { mode = 2; }
      else if ( _strcmpi( tmp, "weightrelative" ) == 0 ) { mode = 3; }

      if ( mode >= 2 )
      {
        if ( (tmp = cmds->QueryValue( "refHeight" ) ) == 0 || 
             (sscanf( tmp, "%f", &refHeight) != 1) )
        {
          ExceptReg::RegExcpt( Exception( 4, "Need refHeight parameter for given mode" ) );
          return;
        }
      }
      else
      {
        ExceptReg::RegExcpt( Exception( 5, "Unknown mode" ) );
        return ;
      }

      const IShape* shp = cmds->GetActiveShape();

      if ( !shp ) { return; }

      DBox bx = shp->CalcBoundingBox();  
      LandBuildInt::CountourLines* ls = NULL;
      switch ( mode )
      {
      case 0: ls = new LandBuildInt::CountourLines; break;
      case 1: ls = new LandBuildInt::CountourLinesRelative; break;
      case 2: ls = new LandBuildInt::CountourLinesWeight( refHeight ); break;
      case 3: ls = new LandBuildInt::CountourLinesWeightRelative( refHeight ); break;
      default: return;
      }

      cmds->GetHeightMap()->AddSamplerModule( ls, 1, &DoubleRect( bx.lo.x, bx.lo.y, bx.hi.x, bx.hi.y ) );
    }

//-----------------------------------------------------------------------------

  } // namespace Modules
} // namespace LandBuildInt
