//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\highmaptest.h"

//-----------------------------------------------------------------------------

#define PI 3.1415926535f

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		void HeightMapTest::Run( IMainCommands* cmdIfc )
		{
			double ampx, perix, ampy, periy;

			const char* tmp = cmdIfc->QueryValue( "amplitudex" );
			if ( tmp == 0 || sscanf( tmp, "%f", &ampx ) != 1 )
			{
				ExceptReg::RegExcpt( Exception( 1, "missing 'amplitudex'" ) );
				return;
			}

			tmp = cmdIfc->QueryValue( "amplitudey" );
			if ( tmp == 0 || sscanf( tmp, "%f", &ampy ) != 1 )
			{
				ExceptReg::RegExcpt( Exception( 1, "missing 'amplitudey'" ) );
				return;
			}
			tmp = cmdIfc->QueryValue( "periodx" );
			if ( tmp == 0 || sscanf( tmp, "%f", &perix ) != 1 )
			{
				ExceptReg::RegExcpt( Exception( 1, "missing 'periodx'" ) );
				return;
			}
			tmp = cmdIfc->QueryValue( "periody" );
			if ( tmp == 0 || sscanf( tmp, "%f", &periy ) != 1 )
			{
				ExceptReg::RegExcpt( Exception( 1, "missing 'periody'" ) );
				return;
			}

			class Sampler : public IHeightMapSampler
			{
				double m_ampx;
        double m_perix;
        double m_ampy;
        double m_periy;

			public:
				Sampler( double ampx, double perix, double ampy, double periy )
        : m_ampx( ampx )
        , m_ampy( ampy )
        , m_perix( perix )
        , m_periy( periy ) 
				{
				}

				virtual double GetHeight( const HeightMapLocInfo& heightInfo ) const
				{
					return (sin( heightInfo.GetX() / m_perix * 2 * PI ) * m_ampx + sin( heightInfo.GetY() / m_periy * 2 * PI ) * m_ampy + (m_ampx + m_ampy));
				}
			};

			cmdIfc->GetHeightMap()->AddSamplerModule( new Sampler( ampx, perix, ampy, periy ) );
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt