//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include <algorithm>
#include ".\highmaploader.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		bool HeightMapLoader::GetSourcesParameters( IMainCommands* cmdIfc )
		{
			int counter = 1;

			do 
			{
        MapInput src;

        // needed to stop the while loop
        // {
        char buffer[50];
			  sprintf( buffer, "demType%d", counter );
        const char* tmp = cmdIfc->QueryValue( buffer, false );
        if ( !tmp ) { break; }
        // }

        if ( !src.LoadMapParameters( cmdIfc, counter ) ) { return (false); }

				m_sources.push_back( src );
				counter++;
			}
			while( true );   

      return (true);
		}

//-----------------------------------------------------------------------------

		bool HeightMapLoader::LoadSourcesHighMaps()
		{
			for ( int i = 0, cnt = m_sources.size(); i < cnt; ++i )
			{
        if ( !m_sources[i].LoadHighMap() ) { return (false); }
			}

			return (true);
		}

//-----------------------------------------------------------------------------

		void HeightMapLoader::Run( IMainCommands* cmdIfc )
		{
			// gets parameters from the calling cfg file
			if ( GetSourcesParameters( cmdIfc ) )
      {
			  // gets sources High Maps
			  if ( LoadSourcesHighMaps() )
        {
  			  cmdIfc->GetHeightMap()->AddSamplerModule( new DemSampler( m_sources ) );
        }
      }
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt