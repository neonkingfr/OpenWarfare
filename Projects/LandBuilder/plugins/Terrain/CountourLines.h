//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "..\LandBuilder2\shared\ilandbuildcmds.h"
#include "IBuildModule.h"
#include "..\LandBuilder2\shared\IShape.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{

//-----------------------------------------------------------------------------

  class CountourLines : public IHeightMapSampler
  {
    Shapes::VertexArray                       m_vxArr;
    AutoArray< SRef< Shapes::ShapePolygon > > m_clines;
    AutoArray< double >                       m_heights;

  public:

    virtual ~CountourLines()
    {
    }

    void AddShape( const Shapes::ShapePolygon& shape, double height, int segment = -1 );
    void AddShapeAuto( const Shapes::ShapePolygon& shape, double baseHeight, double step, int segment = -1 );

    virtual double GetHeight( const HeightMapLocInfo& heightInfo ) const;

    virtual double CombineHeights( double original, double newheight ) const 
    {
      return (newheight);
    }
  };

//-----------------------------------------------------------------------------

  class CountourLinesRelative : public CountourLines
  {
  public:
    virtual ~CountourLinesRelative()
    {
    }

    virtual double CombineHeights( double original, double newheight ) const 
    {
      return (original + newheight);
    }
  };

//-----------------------------------------------------------------------------

  class CountourLinesWeight : public CountourLines
  {
  protected:
    double m_absHeight;

  public:
    CountourLinesWeight( double absHeight )
    : m_absHeight( absHeight ) 
    {
    }

    virtual ~CountourLinesWeight()
    {
    }

    virtual double CombineHeights( double original, double newheight ) const 
    {
      return (original + (m_absHeight - original) * newheight);
    }
  };

//-----------------------------------------------------------------------------

  class CountourLinesWeightRelative : public CountourLinesWeight
  {
  public:
    CountourLinesWeightRelative( double relHeight )
    : CountourLinesWeight( relHeight ) 
    {
    }

    virtual ~CountourLinesWeightRelative()
    {
    }

    virtual double CombineHeights( double original, double newheight ) const 
    {
      return (original + m_absHeight * newheight);
    }
  };

//-----------------------------------------------------------------------------

  namespace Modules
  {
    class CountourLines : public IBuildModule
    {
    public:
      virtual void Run( IMainCommands* cmdIfc );

      DECLAREMODULEEXCEPTION( "CountourLines" )
    };

//-----------------------------------------------------------------------------

  } // namespace Modules
} // namespace LandBuildInt