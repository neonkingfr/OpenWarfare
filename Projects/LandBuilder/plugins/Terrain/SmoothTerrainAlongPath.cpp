//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include <algorithm>
#include ".\SmoothTerrainAlongPath.h"

#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
  namespace Modules
  {

//-----------------------------------------------------------------------------

    SmoothTerrainAlongPath::SmoothTerrainAlongPath()
    : m_firstRun( true )
    , m_canProceed( false )
    , m_shapeCounter( 0 )
    , m_filePrefix( "" )
    , m_longSmoothPar( 0 )
    , m_version( "1.1.1" )
    {
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::Run( IMainCommands* cmdIfc )
    {
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter   = 0;
        m_firstRun       = true;
        m_canProceed     = false;
        m_filePrefix     = "";
        m_longSmoothPar  = 0;
        m_currTaskName   = taskName;
      }

      ++m_shapeCounter;

      // gets shape and its properties
      m_currentShape = cmdIfc->GetActiveShape();      

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "SmoothTerrainAlongPath " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

      // this only the first time
      if ( m_firstRun )
      {
        // gets parameters from calling cfg file
        if ( !GetGlobalParameters( cmdIfc ) ) { return; }

        // gets map's parameters from calling cfg file
        if ( !GetMapParameters( cmdIfc ) ) { return; }

        // load map
        LoadHighMap();

        m_firstRun   = false;
        m_canProceed = true;
      }

      if ( m_canProceed )
      {
        // this for every shape
        // gets shape parameters from calling cfg file
        if ( GetShapeParameters( cmdIfc ) )
        {
          // Smooths the shape profile
          SmoothProfile();

          // Smooths the terrain under the path
          SmoothTerrainUnderPath();
        }
      }
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::OnEndPass( IMainCommands* cmdIfc )
    {
      // Smooths the surrounding terrain
      SmoothSurroundingTerrain();

      vector< MapInput > sources;
      sources.push_back( m_mapIn );
      cmdIfc->GetHeightMap()->AddSamplerModule( new DemSampler( sources ) );
      SaveTerrain();
    }

//-----------------------------------------------------------------------------

    bool SmoothTerrainAlongPath::GetGlobalParameters( IMainCommands* cmdIfc )
    {
      // namePrefix (hasDefault)
      m_filePrefix = "";
      const char* x = cmdIfc->QueryValue( "namePrefix", true );
      if ( x ) 
      {
        m_filePrefix = x;
      }
      else
      {
        LOGF( Warn ) << ">>> ---------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for namePrefix - using no prefix <<<";
        LOGF( Warn ) << ">>> ---------------------------------------------- <<<";
      }

      // longSmoothType (hasDefault)
      m_longSmoothType = "None";
      x = cmdIfc->QueryValue( "longSmoothType", true );
      if ( x ) 
      {
        m_longSmoothType = x;
      }
      else
      {
        LOGF( Warn ) << ">>> --------------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for longSmoothType - using default value ('None') <<<";
        LOGF( Warn ) << ">>> --------------------------------------------------------------- <<<";
      }
      if ( m_longSmoothType != "None" &&
           m_longSmoothType != "MovingAverage" &&
           m_longSmoothType != "MaxSlope" &&
           m_longSmoothType != "Monotone" &&
           m_longSmoothType != "ConstantSlope" &&
           m_longSmoothType != "FixedAbsolute" &&
           m_longSmoothType != "FixedRelative" )
      {
        ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for longSmoothType <<<" ) );
        return (false);
      }

      LOGF( Info ) << "longSmoothType: " << m_longSmoothType;

      // longSmoothPar
      x = cmdIfc->QueryValue( "longSmoothPar", true );
      if ( x ) 
      {
        if ( IsFloatingPoint( string( x ), false ) )
        {
          m_longSmoothPar = atof( x );
        }
        else
        {
          if ( m_longSmoothType != "None" &&
               m_longSmoothType != "ConstantSlope" )
          {
            ExceptReg::RegExcpt( Exception( 12, ">>> Bad data for longSmoothPar <<<" ) );
            return (false);
          }
        }
      }
      else
      {
        if ( m_longSmoothType != "None" &&
             m_longSmoothType != "ConstantSlope" )
        {
          ExceptReg::RegExcpt( Exception( 13, ">>> Missing longSmoothPar value <<<" ) );
          return (false);
        }
      }

      if ( m_longSmoothType == "MovingAverage" && m_longSmoothPar < 10 )
      {
        ExceptReg::RegExcpt( Exception( 14, ">>> longSmoothPar must be greater than 10.0 for MovingAverage <<<" ) );
        return (false);
      }

      LOGF( Info ) << "longSmoothPar: " << m_longSmoothPar;

      return (true);
    }

//-----------------------------------------------------------------------------

    bool SmoothTerrainAlongPath::GetShapeParameters( IMainCommands* cmdIfc )
    {
      // width (isMovable)
			const char* x = cmdIfc->QueryValue( "width", true );
			if ( !x ) { x = cmdIfc->QueryValue( "width", false ); }

      if ( x ) 
      {
        if ( IsFloatingPoint( string( x ), false ) )
        {
          m_shapeIn.m_width = atof( x );
        }
        else
        {
          ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for width <<<" ) );
          return (false);
        }
      }
      else
      {
        ExceptReg::RegExcpt( Exception( 12, ">>> Missing width value <<<" ) );
        return (false);
      }

      LOGF( Info ) << "width: " << m_shapeIn.m_width;

      return (true);
    }

//-----------------------------------------------------------------------------

    bool SmoothTerrainAlongPath::GetMapParameters( IMainCommands* cmdIfc )
    {
      return (m_mapIn.LoadMapParameters( cmdIfc ));
    }

//-----------------------------------------------------------------------------

    bool SmoothTerrainAlongPath::LoadHighMap()
    {
      if ( !m_mapIn.LoadHighMap() ) { return (false); }

			m_mapData = m_mapIn.GetHmData();

			return (true);
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SmoothProfile()
    {
      size_t verticesCount = m_currentShape->GetVertexCount();

      if ( verticesCount > 1 )
      // Smooths only if there is more than one vertex
      {
        if ( m_longSmoothType == "None" ) 
        {
          ProfileAsShape();
        }
        else if ( m_longSmoothType == "MovingAverage" )
        {
          ProfilePointAtSteps( 1.0 );
          SmoothWithMovingAverage();
        }
        else if ( m_longSmoothType == "MaxSlope" )
        {
          ProfileAsShape();
          SmoothWithMaxSlope();
        }
        else if ( m_longSmoothType == "Monotone" )
        {
          ProfileAsShape();
          SmoothWithMonotone();
        }
        else if ( m_longSmoothType == "ConstantSlope" )
        {
          ProfileAsShape();
          SmoothWithConstantSlope();
        }
        else if ( m_longSmoothType == "FixedAbsolute" )
        {
          ProfileAsShape();
          SmoothWithFixedAbsolute();
        }
        else if ( m_longSmoothType == "FixedRelative" )
        {
          ProfileAsShape();
          SmoothWithFixedRelative();
        }
      }
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::ProfileAsShape()
    {
      m_shapeProfile.Clear();
      size_t verticesCount = m_currentShape->GetVertexCount();
      
      for ( size_t i = 0; i < verticesCount; ++i )
      {
        m_shapeProfile.Add( Shapes::DVector( m_currentShape->GetVertex( i ) ) );
      }

      GetProfileElevations();
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::ProfilePointAtSteps( double step )
    {
      m_shapeProfile.Clear();

      double shapeLength = GetShapePerimeter( m_currentShape );

      for ( double i = 0.0; i <= shapeLength; i += step )
      {
        Shapes::DVector v = GetPointOnShapeAt( m_currentShape, i );
        m_shapeProfile.Add( v );
      }

      GetProfileElevations();
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::GetProfileElevations()
    {
      size_t verticesCount = m_shapeProfile.Size();

      for ( size_t i = 0; i < verticesCount; ++i )
      {
        Shapes::DVector v = m_shapeProfile[i];
        m_shapeProfile[i].z = m_mapIn.GetHmData().getWorldWAt( v.x, v.y, EtRectElevationGrid< double, int >::imBILINEAR, true );
      }
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SmoothWithMovingAverage()
    {
      LOGF( Info ) << "Applying algorithm: \"Moving Average\" to shape's profile";

      size_t verticesCount = m_shapeProfile.Size();

      // take the first odd number less than the given one
      size_t range = static_cast< size_t >( floor( EtMathD::Fabs( m_longSmoothPar ) ) );
      if ( range % 2 == 0 ) { range--; }

      if ( verticesCount > range + 1 )
      {
        // calculates moving average
        vector< double > movingAverage;
        for ( size_t i = 0; i < verticesCount; ++i )
        {
          // the first (range - 1) points cannot be averaged
          if ( i < range - 1 )
          {
            movingAverage.push_back( 0.0 );
          }
          else
          {
            double z = 0.0;
            for ( size_t j = i - range + 1; j <= i; ++j )
            {
              z += m_shapeProfile[j].z;
            }
            movingAverage.push_back( z / range );
          }
        }

        // translates the moving average on the profile
        size_t halfRange = static_cast< size_t >( floor( range * 0.5 ) );

        // slope between first point of the profile
        // and the first valid point in the moving average
        double slopeStart = (movingAverage[range - 1] - m_shapeProfile[0].z) / halfRange;

        // slope between the last point of the profile
        // and the last valid point in the moving average
        double slopeEnd = (m_shapeProfile[verticesCount - 1].z - movingAverage[verticesCount - 1])  / halfRange;

        for ( size_t i = 1; i < verticesCount - 1; ++i )
        {
          if ( i < halfRange )
          {
            // we are at the beginning of the profile
            // no data -> we need to interpolate between
            // first z in the profile and first valid z in the
            // moving average
            m_shapeProfile[i].z = m_shapeProfile[0].z + i * slopeStart;
          }
          else if ( i > verticesCount - 1 - halfRange )
          {
            // we are at the end of the profile
            // no data -> we need to interpolate between
            // last z in the profile and last valid z in the
            // moving average
            m_shapeProfile[i].z = movingAverage[verticesCount - 1] + (i - (verticesCount - 1 - halfRange)) * slopeEnd;
          }
          else
          {
            // we are in the moving average range
            // substitute moving average to profile elevations
            m_shapeProfile[i].z = movingAverage[i + halfRange];
          }
        }
      }
    }
    
//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SmoothWithMaxSlope()
    {
      LOGF( Info ) << "Applying algorithm: \"Max Slope\" to shape's profile";

      size_t verticesCount = m_shapeProfile.Size();
      for ( size_t i = 1; i < verticesCount - 1; ++i )
      {
        const Shapes::DVector& v0 = m_shapeProfile[i - 1];
        const Shapes::DVector& v1 = m_shapeProfile[i];
        Shapes::DVector v01( v1, v0 );

        double len = v01.SizeXY();
        if ( len == 0.0 ) { continue; }

        double dz = v1.z - v0.z;
        double slope = dz / len * 100.0;

        if ( slope < -m_longSmoothPar || m_longSmoothPar < slope )
        {
          double newSlope = m_longSmoothPar / 100.0;
          if ( dz < 0.0 )
          {
            newSlope *= -1.0;
          }
          m_shapeProfile[i].z = v0.z + newSlope * len;
        }
      }
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SmoothWithMonotone()
    {
      LOGF( Info ) << "Applying algorithm: \"Monotone\" to shape's profile";

      size_t verticesCount = m_shapeProfile.Size();

      const Shapes::DVector& vF = m_shapeProfile[0];
      const Shapes::DVector& vL = m_shapeProfile[verticesCount - 1];
      double dzLF = vL.z - vF.z;
      double sign = dzLF > 0.0 ? 1.0 : -1.0;

      for ( size_t i = 1; i < verticesCount - 1; ++i )
      {
        const Shapes::DVector& v0 = m_shapeProfile[i - 1];
        const Shapes::DVector& v1 = m_shapeProfile[i];
        Shapes::DVector v01( v1, v0 );

        double len = v01.SizeXY();
        if ( len == 0.0 ) { continue; }

        double dz = v1.z - v0.z;

        double slope = dz / len * 100.0;

        if ( sign * slope < 0.0 )
        {
          m_shapeProfile[i].z = v0.z;
        }
        else
        {
          if ( slope < -m_longSmoothPar || m_longSmoothPar < slope )
          {
            double newSlope = m_longSmoothPar / 100.0;
            if ( dz < 0.0 )
            {
              newSlope *= -1.0;
            }
            m_shapeProfile[i].z = v0.z + newSlope * len;
          }
        }
      }
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SmoothWithConstantSlope()
    {
      LOGF( Info ) << "Applying algorithm: \"Constant Slope\" to shape's profile";

      size_t verticesCount = m_shapeProfile.Size();
      const Shapes::DVector& vF = m_shapeProfile[0];
      const Shapes::DVector& vL = m_shapeProfile[verticesCount - 1];

      double diff = vL.z - vF.z;
      if ( diff == 0.0 ) { return; }

      double shapeLength = GetShapePerimeter( m_currentShape );
      if ( shapeLength == 0.0 ) { return; }

      double deltaZ = diff / shapeLength;

      double longCoordinate = 0.0;

      for ( size_t i = 1; i < verticesCount; ++i )
      {
        const Shapes::DVector& v0 = m_shapeProfile[i - 1];
        const Shapes::DVector& v1 = m_shapeProfile[i];

        Shapes::DVector v01( v1, v0 );
        longCoordinate += v01.SizeXY();
        m_shapeProfile[i].z = vF.z + longCoordinate * deltaZ; 
      }
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SmoothWithFixedAbsolute()
    {
      LOGF( Info ) << "Applying algorithm: \"Fixed Absolute\" to shape's profile";

      size_t verticesCount = m_shapeProfile.Size();

      for ( size_t i = 0; i < verticesCount; ++i )
      {
        m_shapeProfile[i].z = m_longSmoothPar;
      }
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SmoothWithFixedRelative()
    {
      LOGF( Info ) << "Applying algorithm: \"Fixed Relative\" to shape's profile";

      size_t verticesCount = m_shapeProfile.Size();

      for ( size_t i = 0; i < verticesCount; ++i )
      {
        m_shapeProfile[i].z += m_longSmoothPar;
      }
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SmoothTerrainUnderPath()
    {
      // here we smooth only the terrain which is under the path
      // or in the immediate neighborhood (all the triangle which
      // have some area under the strip having the polyline as 
      // centerline and the specified width)

      size_t verticesCount = m_shapeProfile.Size();

      if ( verticesCount > 1 )
      // Smooths only if there are is than one vertex
      {
        LOGF( Info ) << "Smoothing terrain under shape...";

        // first we smooth the closest neighborhood of the path
        for ( size_t i = 0; i < verticesCount - 1; ++i )
        {
          const Shapes::DVector& v0 = m_shapeProfile[i];
          const Shapes::DVector& v1 = m_shapeProfile[i + 1];

          // segment vector (for the moment only 2D)
          Shapes::DVector segment( v1, v0 );

          // the length of the current segment
          double segLength = segment.SizeXY();

          if ( segLength > 0.0 )
          // goes on only if the segment is a real segment
          {
            Shapes::DVertex newV0( v0 );
            Shapes::DVertex newV1( v1 );

            // searches for previous segment
            // if this segment and the previous are not aligned
            // extend this segment
            if ( i > 0 )
            {
              const Shapes::DVector& vP0 = m_shapeProfile[i - 1];
              const Shapes::DVector& vP1 = m_shapeProfile[i];

              // previous segment vector 
              Shapes::DVector segmentPrev( vP1, vP0 );

              if ( EtMathD::Fabs( EtMathD::Fabs( segmentPrev.DotXY( segment ) ) - 1.0 ) != 0.000001 )
              {
                // extends this segment on the first vertex side
                double stretchFactor = (segLength + m_shapeIn.m_width * 0.5) / segLength;
                Shapes::DVector tempV( v0, v1 );
                double dz = tempV.z / segLength;
                tempV *= stretchFactor;
                newV0 = tempV.GetSecondEndXY( v1 );
                newV0.z = v1.z + dz * tempV.SizeXY();
              }
            }

            // searches for next segment
            // if this segment and the next are not aligned
            // extend this segment
            if ( i < verticesCount - 2 )
            {
              const Shapes::DVector& vN0 = m_shapeProfile[i + 1];
              const Shapes::DVector& vN1 = m_shapeProfile[i + 2];

              // previous segment vector 
              Shapes::DVector segmentNext( vN1, vN0 );

              if ( EtMathD::Fabs( EtMathD::Fabs( segmentNext.DotXY( segment ) ) - 1.0 ) != 0.000001 )
              {
                // extends this segment on the second vertex side
                double stretchFactor = (segLength + m_shapeIn.m_width * 0.5) / segLength;
                Shapes::DVector tempV( v1, v0 );
                double dz = tempV.z / segLength;
                tempV *= stretchFactor;
                newV1 = tempV.GetSecondEndXY( v0 );
                newV1.z = v0.z + dz * tempV.SizeXY();
              }
            }

            Shapes::DVector newSegment( newV1, newV0 );
            segment = newSegment;
            segLength = segment.SizeXY();

            // for speed searches only the vertex that are close to the current segment
            // the localGrid variable will contain the vertices inside the rectangle 
            // containing the current segment
            EtRectangle< double > boundingRect = SegmentBoundingBox( newV0, newV1 );
            EtRectElevationGrid< double, int > localGrid = m_mapIn.GetHmData().subGrid( boundingRect.GetLeft(), boundingRect.GetBottom(), 
                                                                                        boundingRect.GetRight(), boundingRect.GetTop() );

            double sizeU = localGrid.getSizeU();
            double sizeV = localGrid.getSizeV();

            // calculates the offset of the localGrid with respect to the global grid
            size_t offsetU = static_cast< size_t >( (localGrid.getWorldUAt( 0 ) - m_mapIn.GetHmData().getWorldUAt( 0 )) / m_mapIn.GetHmData().getResolutionU() );
            size_t offsetV = static_cast< size_t >( (localGrid.getWorldVAt( 0 ) - m_mapIn.GetHmData().getWorldVAt( 0 )) / m_mapIn.GetHmData().getResolutionV() );

            double sqrt2 = EtMathD::Sqrt(2.0);
            for ( int iU = 0; iU < sizeU; ++iU )
            {
              for ( int iV = 0; iV < sizeV; ++iV )
              {
                // the unitary vector along the segment
                Shapes::DVector unitSegment( newV1, newV0 );
                unitSegment.NormalizeXYInPlace();

                // the vector from the first end of the segment to the
                // vertex of the grid (for the moment only 2D)
                Shapes::DVector tempNode( localGrid.getWorldUAt( iU ), localGrid.getWorldVAt( iV ) );
                Shapes::DVector node( tempNode, newV0 );

                // the length of the component of the vector node along the segment
                double proj = node.DotXY( unitSegment );

                // if 0 <= proj <= segLength the projection of the node on
                // the segment is really ON the segment
                // we go on with calculation only in this case
                if ( 0.0 <= proj && proj <= segLength )
                {
                  // measures the distance of the node from the segment
                  double nodeLength = node.SizeXY();
                  double dist = EtMathD::Sqrt( nodeLength * nodeLength - proj * proj );

                  // modify only vertices that are closer than (width / 2 + terrain resolution) * sqrt(2) to the path
                  if ( dist <= (m_shapeIn.m_width * 0.5 + m_mapIn.GetHmData().getResolutionU()) * sqrt2 )
                  {
                    // goes on only if both ends of the segment are inside the map
                    if ( newV0.z != NO_ELEVATION && newV1.z != NO_ELEVATION )
                    {
                      // the elevation on the projection of the node on the segment
                      double elevationAtP = newV0.z + (newV1.z - newV0.z) * proj / segLength;
                      // the node takes the same height value of this projection
                      FixedVertex fV( offsetU + iU, offsetV + iV, elevationAtP );
                      m_underFixedList.AddVertex( fV );
                    }
                  }
                }
              }
            }
          }
        }
        LOGF( Info ) << "Done.";
      }
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SmoothSurroundingTerrain()
    {
      LOGF( Info ) << "--------------------------------";
      LOGF( Info ) << "All shapes loaded               ";
      LOGF( Info ) << "--------------------------------";
      LOGF( Info ) << "Smoothing surrounding terrain...";

      size_t underFixedCount = m_underFixedList.Size(); 

      if ( underFixedCount > 0 )
      {
        // first averages multiple fixed points and updates the terrain
        for ( size_t i = 0; i < underFixedCount; ++i )
        {
          FixedVertex& v = m_underFixedList.GetVertex( i );
          if ( v.GetCount() > 1 )
          {
            v.AverageZ();
          }
          m_mapIn.GetHmData().setWorldWAt( v.GetX(), v.GetY(), v.GetZ() );
        }

        size_t mapSizeU = m_mapIn.GetHmData().getSizeU();
        size_t mapSizeV = m_mapIn.GetHmData().getSizeV();

        double z1, z2, z3;
        double zM12, dz, z;

        ProgressBar< int > pb( underFixedCount );

        // now we smooth the surrounding
        for ( size_t i = 0; i < underFixedCount; ++i )
        {
          pb.AdvanceNext( 1 );

          const FixedVertex& fixed = m_underFixedList.GetVertex( i );

          if ( m_underFixedList.Contains( fixed.GetX() - 1, fixed.GetY() ) != -1 )
          {
            if ( m_underFixedList.Contains( fixed.GetX(), fixed.GetY() + 1 ) != -1 )
            {
              if ( m_underFixedList.Contains( fixed.GetX() - 1, fixed.GetY() + 1 ) == -1 )
              {
                if ( ((fixed.GetX() - 1) >= 0) && ((fixed.GetY() + 1) < (signed)mapSizeV) )
                {
                  z1 = m_underFixedList.GetZAt( fixed.GetX() - 1, fixed.GetY() );
                  z2 = m_underFixedList.GetZAt( fixed.GetX(), fixed.GetY() + 1 );
                  z3 = m_underFixedList.GetZAt( fixed.GetX(), fixed.GetY() );

                  if ( z1 != cNODATA && z2 != cNODATA && z3 != cNODATA )
                  {
                    zM12 = (z1 + z2) * 0.5;
                    dz   = zM12 - z3;
                    z    = z3 + 2.0 * dz;

                    m_mapIn.GetHmData().setWorldWAt( fixed.GetX() - 1, fixed.GetY() + 1, z );
                  }
                }
              }
            }
          }

          if ( m_underFixedList.Contains( fixed.GetX() + 1, fixed.GetY() ) != -1 )
          {
            if ( m_underFixedList.Contains( fixed.GetX(), fixed.GetY() - 1 ) != -1 )
            {
              if ( m_underFixedList.Contains( fixed.GetX() + 1, fixed.GetY() - 1 ) == -1 )
              {
                if ( ((fixed.GetX() + 1) < (signed)mapSizeU) && ((fixed.GetY() - 1) >= 0) )
                {
                  z1 = m_underFixedList.GetZAt( fixed.GetX() + 1, fixed.GetY() );
                  z2 = m_underFixedList.GetZAt( fixed.GetX(), fixed.GetY() - 1 );
                  z3 = m_underFixedList.GetZAt( fixed.GetX(), fixed.GetY() );

                  if ( z1 != cNODATA && z2 != cNODATA && z3 != cNODATA )
                  {
                    zM12 = (z1 + z2) * 0.5;
                    dz   = zM12 - z3;
                    z    = z3 + 2.0 * dz;

                    m_mapIn.GetHmData().setWorldWAt( fixed.GetX() + 1, fixed.GetY() - 1, z );
                  }
                }
              }
            }
          }
        }
      }
      LOGF( Info ) << "Smoothing completed.   ";
    }

//-----------------------------------------------------------------------------

    void SmoothTerrainAlongPath::SaveTerrain()
    {
      LOGF( Info ) << "Saving modified terrain...";

      RString filename  = RString( m_mapIn.GetFileName().substr( 0, m_mapIn.GetFileName().length() - 4 ).c_str() );
      RString extension = RString( m_mapIn.GetFileName().substr( m_mapIn.GetFileName().length() - 4, 4 ).c_str() );

      int lastSlashPos    = filename.ReverseFind( '\\' );
      RString path        = filename.Substring( 0, lastSlashPos + 1 );
      RString oldFilename = filename.Substring( lastSlashPos + 1, filename.GetLength() );

      filename = path + m_filePrefix + oldFilename + "Mod" + extension; 

      if ( m_mapIn.GetDemType() == S_DTED2 )
      {
        // not implemented yet
      }
      else if ( m_mapIn.GetDemType() == S_USGSDEM )
      {
        // not implemented yet
      }
      else if ( m_mapIn.GetDemType() == S_ARCINFOASCII )
      {
        m_mapIn.GetHmData().saveAsWorldToARCINFOASCIIGrid( filename.Data() );
      }
      else if ( m_mapIn.GetDemType() == S_XYZ )
      {
        m_mapIn.GetHmData().saveAsWorldToXYZ( filename.Data() );
      }

      // copies the prj file if present
      RString srcPrjFilename = path + oldFilename + ".prj";
      RString dstPrjFilename = path + m_filePrefix + oldFilename + "Mod" + ".prj";
	    ifstream test( srcPrjFilename, std::ios::binary );
	    if ( !test.fail() )
	    {
        test.close();

		    const int BUFFER_SIZE = 4096;
		    char buffer[BUFFER_SIZE];

        ifstream ifs( srcPrjFilename, std::ios::binary );
        ofstream ofs( dstPrjFilename, std::ios::binary );

		    while ( !ifs.eof() ) 
		    {
			    ifs._Read_s( buffer, BUFFER_SIZE, BUFFER_SIZE );
			    if ( !ifs.bad() ) 
			    {
				    ofs.write( buffer, ifs.gcount() );
			    }
		    }

		    ifs.close();
		    ofs.close();
      }

      LOGF( Info ) << "Done.";
    }

//-----------------------------------------------------------------------------

    EtRectangle< double > SmoothTerrainAlongPath::SegmentBoundingBox( const Shapes::DVertex& v0, const Shapes::DVertex& v1 )
    {
      double segDirection = EtMathD::SegmentDirection_XY_CCW_X( v0.x, v0.y, v1.x, v1.y );

      double width = m_shapeIn.m_width * 0.5 + m_mapIn.GetHmData().getResolutionU() * EtMathD::Sqrt( 2.0 );
      double widthSin = width * EtMathD::Sin( segDirection );
      double widthCos = width * EtMathD::Sin( segDirection );

      // right point at 0
      double x0R = v0.x + widthSin;
      double y0R = v0.y - widthCos;

      double left   = x0R;
      double right  = x0R;
      double top    = y0R;
      double bottom = y0R;

      // left point at 0
      double x0L = v0.x - widthSin;
      double y0L = v0.y + widthCos;
      
      if ( left   > x0L ) { left   = x0L; }
      if ( right  < x0L ) { right  = x0L; }
      if ( bottom > y0L ) { bottom = y0L; }
      if ( top    < y0L ) { top    = y0L; }

      // right point at 1
      double x1R = v1.x + widthSin;
      double y1R = v1.y - widthCos;

      if ( left   > x1R ) { left   = x1R; }
      if ( right  < x1R ) { right  = x1R; }
      if ( bottom > y1R ) { bottom = y1R; }
      if ( top    < y1R ) { top    = y1R; }

      // left point at 0
      double x1L = v1.x - widthSin;
      double y1L = v1.y + widthCos;

      if ( left   > x1L ) { left   = x1L; }
      if ( right  < x1L ) { right  = x1L; }
      if ( bottom > y1L ) { bottom = y1L; }
      if ( top    < y1L ) { top    = y1L; }

      return (EtRectangle< double >( left, top, right, bottom ));
    }

//-----------------------------------------------------------------------------

  } // namespace Modules
} // namespace LandBuildInt
