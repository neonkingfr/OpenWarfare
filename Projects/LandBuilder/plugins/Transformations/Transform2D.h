//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Transform2D\include\Transform2Manager.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class Transform2D : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				double m_x;
				double m_y;
				bool   m_used; 
				bool   m_last;
			};

		public:
      Transform2D();

      virtual ~Transform2D()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );

      DECLAREMODULEEXCEPTION( "Transform2D" )

		private:
      string m_version;

      bool   m_firstRun;
      bool   m_canProceed;

			HomologousPoint2List< double > m_homoPoints;

      Transformations2 m_type;

		private:
			const Shapes::IShape* m_currentShape;   
			ShapeInput            m_shapeIn;

			bool GetGlobalParameters( IMainCommands* cmdIfc );
			bool GetShapeParameters( IMainCommands* cmdIfc );
			void UpdateHomoPoints();
			bool Solve();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
