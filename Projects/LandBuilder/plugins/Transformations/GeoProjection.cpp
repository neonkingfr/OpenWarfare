//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\Shapes\IShape.h"

#include ".\GeoProjection.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		void GeoProjection::Run( IMainCommands* cmdIfc )
		{
			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape(); 

			// gets parameters from calling cfg file
			if ( GetGlobalParameters( cmdIfc ) )
			{
				Solve( cmdIfc );
			}
		}

//-----------------------------------------------------------------------------

		bool GeoProjection::GetGlobalParameters( IMainCommands* cmdIfc )
		{
			// gets parameters from calling cfg file
			const char* x = cmdIfc->QueryValue( "transfType", false );
			if ( x )
			{
				if ( strcmp( x, "WGS84TOUTM" ) == 0 )
				{
					m_globalIn.m_type = ProjTransformationCode_WGS84TOUTM;
				}
				else 
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Unknown transfType <<<" ) );
					return (false);
				}
			}
			else
			{        
				ExceptReg::RegExcpt( Exception( 12, ">>> No transfType specified <<<" ) );
				return (false);
			}
			return (true);
		}

//-----------------------------------------------------------------------------

		bool GeoProjection::Solve( IMainCommands* cmdIfc )
		{
			ProjTransformation< double > trans( m_globalIn.m_type );
			ProjTransformationReturnData< double > returnData;
			
			Shapes::VertexArray* vxarray = const_cast< Shapes::VertexArray* >( cmdIfc->GetInterface< IGlobVertexArray >()->GetVertexArray() );

			int counter = 0;
			for ( Shapes::VertexArray::Iter iter = vxarray->First(); iter; ++iter )
			{
				const Shapes::DVertex& vx = iter.GetVertex();
				ProjTransformationReturnValues res = trans.TransformDirect( EtMathD::DegToRad( vx.y ), EtMathD::DegToRad( vx.x ), returnData );
				if ( res == PjTrRet_SUCCESS )
				{
					Shapes::DVertex nx( returnData.easting, returnData.northing );
					iter.SetVertex( nx );
					counter++;
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------- <<<";
          LOGF( Warn ) << ">>> Error (" << res << " while applying projection <<<";
          LOGF( Warn ) << ">>> ----------------------------------- <<<";
					return (false);
				}
			}
			return (true);
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
