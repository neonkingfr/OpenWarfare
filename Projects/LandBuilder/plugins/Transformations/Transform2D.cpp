//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\Shapes\IShape.h"
#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"

#include ".\Transform2D.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

    Transform2D::Transform2D()
    : m_firstRun( true )
    , m_canProceed( false )
    , m_version( "1.1.0" )
    {
    }

//-----------------------------------------------------------------------------

		void Transform2D::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_firstRun     = true;
        m_canProceed   = false;
        m_currTaskName = taskName;
      }

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape(); 

      // this only the first time
      if ( m_firstRun )
      {
        if ( !GetGlobalParameters( cmdIfc ) ) { return; }

        m_firstRun   = false;
        m_canProceed = true;
      }

      if ( m_canProceed )
      {
			  // gets shape parameters from calling cfg file
			  if ( GetShapeParameters( cmdIfc ) )
			  {
				  // updates the homologous point list
				  UpdateHomoPoints();

				  // if all points have been loaded
				  if ( m_shapeIn.m_last ) { Solve(); }
			  }
      }
		}

//-----------------------------------------------------------------------------

		bool Transform2D::GetGlobalParameters( IMainCommands* cmdIfc )
		{
			// transfType
			const char* x = cmdIfc->QueryValue( "transfType", true );

			if ( x )
			{
				if ( strcmp( x, "RIGID" ) == 0 )
				{
					m_type = T2_RIGID;
				}
				else if ( strcmp( x, "CONFORM" ) == 0 )
				{
					m_type = T2_CONFORM;
				}
				else if ( strcmp( x, "AFFINE" ) == 0 )
				{
					m_type = T2_AFFINE;
				}
				else if ( strcmp( x, "AFFINEEX" ) == 0 )
				{
					m_type = T2_AFFINEEX;
				}
				else if ( strcmp( x, "HOMOGRAPHIC" ) == 0 )
				{
					m_type = T2_HOMOGRAPHIC;
				}
				else if ( strcmp( x, "BILINEAR" ) == 0 )
				{
					m_type = T2_BILINEAR;
				}
				else if ( strcmp( x, "POLYNOMIAL2" ) == 0 )
				{
					m_type = T2_POLYNOMIAL2;
				}
				else if ( strcmp( x, "BESTFIT" ) == 0 )
				{
					m_type = T2_BESTFIT;
				}
				else 
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Unknown transfType <<<" ) );
					return (false);
				}
			}
			else
			{        
				ExceptReg::RegExcpt( Exception( 12, ">>> No transfType specified <<<" ) );
				return (false);
			}

			return (true);
		}

//-----------------------------------------------------------------------------

		bool Transform2D::GetShapeParameters( IMainCommands* cmdIfc )
		{
			// x 
			const char* x = cmdIfc->QueryValue( "x", false );

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_x = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 21, ">>> Bad data for x <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 22, ">>> Missing x value <<<" ) );
				return (false);
			}

			// y 
			x = cmdIfc->QueryValue( "y", false );

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_y = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 23, ">>> Bad data for y <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 24, ">>> Missing y value <<<" ) );
				return (false);
			}

			// used 
			x = cmdIfc->QueryValue( "used", false );

			if ( x ) 
			{
				if ( IsBool( string( x ), false ) )
				{
					int i = static_cast< int >( atoi( x ) );
					if ( i == 0 )
					{
						m_shapeIn.m_used = false;
					}
					else
					{
						m_shapeIn.m_used = true;
					}
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 25, ">>> Bad data for used <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 26, ">>> Missing used value <<<" ) );
				return (false);
			}

			// last 
			x = cmdIfc->QueryValue( "last", false );

			if ( x ) 
			{
				if ( IsBool( string( x ), false ) )
				{
					int i = static_cast< int >( atoi( x ) );
					if ( i == 0 )
					{
						m_shapeIn.m_last = false;
					}
					else
					{
						m_shapeIn.m_last = true;
					}
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 27, ">>> Bad data for last <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 28, ">>> Missing last value <<<" ) );
				return (false);
			}

			return (true);
		}

//-----------------------------------------------------------------------------

		void Transform2D::UpdateHomoPoints()
		{
			if ( m_shapeIn.m_used )
			{
				Shapes::DVector p = m_currentShape->GetVertex( 0 );

				Point2< double > p1( p.x, p.y );
				Point2< double > p2( m_shapeIn.m_x, m_shapeIn.m_y );
				HomologousPoint2< double > hp( p1, p2 );
				m_homoPoints.AddPoint( hp );
			}
		}

//-----------------------------------------------------------------------------

		bool Transform2D::Solve()
		{
			Transform2Manager< double > trans;
			trans.SetHomoPointsList( m_homoPoints );

			if ( trans.ChooseTransformation( m_type ) )
			{
        LOGF( Info ) << ">>> ------------------------------------------------------ <<<";
        LOGF( Info ) << ">>> Applying transformation: " << trans.GetTransformationName() << " <<<";
        LOGF( Info ) << ">>> ------------------------------------------------------ <<<";

        Shapes::VertexArray* vxarray = const_cast< Shapes::VertexArray* >( m_currentShape->GetVertexArray() );

				vector< Point2< double > > pointsIn;

				for ( Shapes::VertexArray::Iter iter = vxarray->First(); iter; ++iter )
				{
					const Shapes::DVertex& vx = iter.GetVertex();
					Point2< double > p( vx.x, vx.y );
					pointsIn.push_back( p );
				}

				vector< Point2< double > > pointsOut = trans.ApplyTransformation( pointsIn );

				int counter = 0;
				for ( Shapes::VertexArray::Iter iter = vxarray->First(); iter; ++iter )
				{
					Shapes::DVertex nx( pointsOut[counter].X(), pointsOut[counter].Y() );
					iter.SetVertex( nx );
					counter++;
				}

				return (true);
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
        LOGF( Warn ) << ">>> Error while applying transformation: " << trans.GetTransformationName() << " <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
				return (false);
			}
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt

