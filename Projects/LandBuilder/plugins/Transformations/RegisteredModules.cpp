#include "StdAfx.h"
#include "ModuleRegistrator.h"

namespace LandBuilder2
{
	using namespace LandBuildInt;
}

//source contains list of registered modules. 
/**Don't add new modules here. Instead, register module using the registrator 
at place of module's definition
*/
#include "Transform2D.h"
#include "GeoProjection.h"

namespace LandBuilder2
{
	RegisterModule<Modules::GeoProjection> _register_TR_001("GeoProjection");
	RegisterModule<Modules::Transform2D>   _register_TR_002("Transform2D");
}