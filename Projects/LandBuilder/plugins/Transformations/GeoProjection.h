//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Projections\include\ProjTransformation.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class GeoProjection : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				PredefinedProjTransformationCodes m_type;
			};

		public:
      virtual ~GeoProjection()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );

      DECLAREMODULEEXCEPTION( "GeoProjection" )

		private:
			const Shapes::IShape* m_currentShape;   
			GlobalInput           m_globalIn;

			bool GetGlobalParameters( IMainCommands* cmdIfc );
			bool Solve( IMainCommands* cmdIfc );
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
