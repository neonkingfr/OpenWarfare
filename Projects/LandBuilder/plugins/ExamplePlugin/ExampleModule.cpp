//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "ExampleModule.h"

//-----------------------------------------------------------------------------

///called on begin of pass during multipass processing        
void ExampleModule::OnBeginPass( IMainCommands* cmdIfc )
{
  LOGF( Progress ) << "Example module in begin of pass";
}

//-----------------------------------------------------------------------------

///called on begin of group
void ExampleModule::OnBeginGroup( IMainCommands* cmdIfc )
{
  LOGF( Progress ) << "Example module in begin of group";
}

//-----------------------------------------------------------------------------

void ExampleModule::OnEndGroup( IMainCommands* cmdIfc )
{
  LOGF( Progress ) << "Example module at the end of group";
}

//-----------------------------------------------------------------------------

void ExampleModule::OnEndPass( IMainCommands* cmdIfc )
{
  LOGF( Progress ) << "Example module at the end of pass";
}

//-----------------------------------------------------------------------------

void ExampleModule::Run( IMainCommands* cmdIfc )
{
  LOGF( Progress )<<"Example module in run";
}

//-----------------------------------------------------------------------------

void ExampleModule::Release() 
{
  LOGF( Progress ) << "Example module in release";
  LandBuilder2::IBuildModule::Release();
}

//-----------------------------------------------------------------------------

LandBuilder2::RegisterModule< ExampleModule > _register( "ExampleModule" );

//-----------------------------------------------------------------------------
