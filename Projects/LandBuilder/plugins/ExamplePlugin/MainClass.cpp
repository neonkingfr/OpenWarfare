#include "StdAfx.h"
#include "MainClass.h"

void MainClass::InitPlugin(LBGlobalPointers* globalPointers)
{
    PluginMainClass::InitPlugin(globalPointers);
    //demonstration of the allocation user memory in app's heap;
    usermemory = _globalPointers->mallocFunct(1000);
}

void MainClass::Release(void)
{
    //demonstration of the deallocation user memory in app's heap;
    _globalPointers->freeFunct(usermemory,1000);
    PluginMainClass::Release();
}

void MainClass::SetCWD(const char *cwd)
{
    LOGF(Info)<<"Plugin example, settings current directory"<<cwd;
    PluginMainClass::SetCWD(cwd);
}

//only one instance of mainclass
MainClass mainClass;