//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\HighMapLoaders\include\EtMath.h"
#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"

#include ".\BestFitPlacer.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
  namespace Modules
  {

//-----------------------------------------------------------------------------

    BestFitPlacer::BestFitPlacer()
    : m_version( "1.1.1" )
    , m_shapeCounter( 0 )
    {
    }

//-----------------------------------------------------------------------------

    void BestFitPlacer::Run( IMainCommands* cmdIfc )
    {
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

      // gets shape and its properties
      m_currentShape = cmdIfc->GetActiveShape();    

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "BestFitPlacer " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

      // resets arrays
      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

      // gets parameters from calling cfg file
      if ( GetShapeParameters( cmdIfc ) )
      {
        // gets objects parameters from calling cfg file
        if ( GetObjectsParameters( cmdIfc ) )
        {
          SetGeometry();

          // sets the output objects
          CreateObjects();

          // export objects
          ExportCreatedObjects( cmdIfc );

          // writes the report
          WriteReport();
        }
      }
    }

//-----------------------------------------------------------------------------

    ModuleObjectOutputArray* BestFitPlacer::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn,
                                                          AutoArray< BestFitPlacer::ObjectInput >& objectsIn )
    {
      m_currentShape  = shape;
      m_shapeIn       = shapeIn;
      m_objectsIn     = objectsIn;

      if ( !m_currentShape ) { return (NULL); }

      // resets arrays
      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

      SetGeometry();

      // sets the output objects
      CreateObjects();

      return (&m_objectsOut);
    }

//-----------------------------------------------------------------------------

    bool BestFitPlacer::GetShapeParameters( IMainCommands* cmdIfc )
    {
      // dirCorrection (isMovable && hasDefault)
      m_shapeIn.m_dirCorrection = 0.0; // default value
      const char* x = cmdIfc->QueryValue( "dirCorrection", true );
      if ( !x ) { x = cmdIfc->QueryValue( "dirCorrection", false ); }

      if ( x ) 
      {
        if ( IsFloatingPoint( string( x ), false ) )
        {
          m_shapeIn.m_dirCorrection = atof( x );
        }
        else
        {
          LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
          LOGF( Warn ) << ">>> Bad data for dirCorrection - using default value (0.0) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
        }
      }
      else
      {
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing dirCorrection value - using default value (0.0) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
      }

      LOGF( Info ) << "dirCorrection: " << m_shapeIn.m_dirCorrection;

      // desiredHeight (isMovable)
      x = cmdIfc->QueryValue( "desiredHeight", true );
      if ( !x ) { x = cmdIfc->QueryValue( "desiredHeight", false ); }

      if ( x ) 
      {
        if ( IsFloatingPoint( string( x ), false ) )
        {
          m_shapeIn.m_desiredHeight = atof( x );
        }
        else
        {
          ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for desiredHeight <<<" ) );
          return (false);
        }
      }
      else
      {
        ExceptReg::RegExcpt( Exception( 12, ">>> Missing desiredHeight value <<<" ) );
        return (false);
      }

      LOGF( Info ) << "desiredHeight: " << m_shapeIn.m_desiredHeight;

      return (true);
    }

//-----------------------------------------------------------------------------

    bool BestFitPlacer::GetObjectsParameters( IMainCommands* cmdIfc )
    {
      // resets arrays
      if ( m_objectsIn.Size() != 0 )
      {
        m_objectsIn.Clear();
        m_objectsOut.Clear();
      }

      int counter = 1;
      do 
      {
        char object[50], length[50], width[50], height[50];
        sprintf_s( object, "object%d", counter );
        sprintf_s( length, "length%d", counter );
        sprintf_s( width , "width%d" , counter );
        sprintf_s( height, "height%d", counter );

        ObjectInput nfo;

        // object type
        const char* x = cmdIfc->QueryValue( object, false );
        if ( !x ) { break; }
        nfo.m_name = x;
        
        LOGF( Info ) << "object " << counter << ": " << nfo.m_name;

        // object lenght
        x = cmdIfc->QueryValue( length, false );
        if ( x )
        {
          if ( IsFloatingPoint( string( x ), false ) )
          {
            nfo.m_length = atof( x );
          }
          else
          {
            ExceptReg::RegExcpt( Exception( 21, ">>> Bad data for length <<<" ) );
            return (false);
          }
        }
        else
        {
          ExceptReg::RegExcpt( Exception( 22, ">>> Missing length value <<<" ) );
          return (false);
        }

        LOGF( Info ) << "length: " << nfo.m_length;

        // object width
        x = cmdIfc->QueryValue( width, false );
        if ( x )
        {
          if ( IsFloatingPoint( string( x ), false ) )
          {
            nfo.m_width = atof( x );
          }
          else
          {
            ExceptReg::RegExcpt( Exception( 23, ">>> Bad data for width <<<" ) );
            return (false);
          }
        }
        else
        {
          ExceptReg::RegExcpt( Exception( 24, ">>> Missing width value <<<" ) );
          return (false);
        }
        
        LOGF( Info ) << "width: " << nfo.m_width;

        // object height
        x = cmdIfc->QueryValue( height, false );
        if ( x )
        {
          if ( IsFloatingPoint( string( x ), false ) )
          {
            nfo.m_height = atof( x );
          }
          else
          {
            ExceptReg::RegExcpt( Exception( 25, ">>> Bad data for height <<<" ) );
            return (false);
          }
        }
        else
        {
          ExceptReg::RegExcpt( Exception( 26, ">>> Missing height value <<<" ) );
          return (false);
        }

        LOGF( Info ) << "height: " << nfo.m_height;

        m_objectsIn.Add( nfo );
        counter++;
      } 
      while ( true ); 

      return (true);
    }

//-----------------------------------------------------------------------------

    void BestFitPlacer::SetGeometry()
    {
      // use the algorithm from: 
      // http://www.geometrictools.com/Documentation/MinimumAreaRectangle.pdf

      size_t vCount = m_currentShape->GetVertexCount();
      double minArea = EtMathD::MAX_REAL;

      const Shapes::DVertex& p0 = m_currentShape->GetVertex( 0 );

      for ( size_t i = 0, j = (vCount - 1); i < vCount; j = i++ )
      {
        const Shapes::DVertex& vJ = m_currentShape->GetVertex( j );
        const Shapes::DVertex& vI = m_currentShape->GetVertex( i );

        Shapes::DVector vJI( vI, vJ );
        vJI.NormalizeXYInPlace();

        Shapes::DVector vPerp = vJI.PerpXYCCW();

        double l0 = 0.0;
        double l1 = 0.0;
        double w0 = 0.0;
        double w1 = 0.0;

        for ( size_t k = 1; k < vCount; ++k )
        {
          const Shapes::DVertex& vK = m_currentShape->GetVertex( k );
          Shapes::DVector vK0( vK, p0 );

          double testL = vJI.DotXY( vK0 );
          if ( testL < l0 )
          {
            l0 = testL;
          }
          else if ( testL > l1 )
          {
            l1 = testL;
          }

          double testW = vPerp.DotXY( vK0 );
          if ( testW < w0 )
          {
            w0 = testW;
          }
          else if ( testW > w1 )
          {
            w1 = testW;
          }
        }

        double tempLength = l1 - l0;
        double tempWidth  = w1 - w0; 
        double area = tempLength * tempWidth;
        if ( area < minArea )
        {
          minArea           = area;
          m_calcLength      = tempLength;
          m_calcWidth       = tempWidth;
          m_calcOrientation = vJI.DirectionXY(); 
        }		
      }
    }

//-----------------------------------------------------------------------------

    void BestFitPlacer::CreateObjects()
    {
      ModuleObjectOutput out;

      int objInCount = m_objectsIn.Size();

      double minDiff = EtMathD::MAX_REAL;
      size_t choice = 0;

      for ( int i = 0; i < objInCount; ++i )
      {
        double dx = EtMathD::Fabs( m_calcLength - m_objectsIn[i].m_length );
        double dy = EtMathD::Fabs( m_calcWidth - m_objectsIn[i].m_width );
        double dz = EtMathD::Fabs( m_shapeIn.m_desiredHeight - m_objectsIn[i].m_height );
        double totDiff = dx + dy + dz;
        if ( totDiff < minDiff )
        {
          minDiff = totDiff;
          choice = i;
        }
      }

      // sets object out data
      out.type     = choice;
      out.position = m_currentShape->Barycenter();
      out.rotation = m_calcOrientation + EtMathD::DegToRad( m_shapeIn.m_dirCorrection );
      out.scaleX   = out.scaleY = out.scaleZ = 1.0;

      // used only by VLB for preview
      out.length = m_objectsIn[choice].m_length;
      out.width  = m_objectsIn[choice].m_width;
      out.height = m_objectsIn[choice].m_height;

      // adds to array
      m_objectsOut.Add( out );
    }

//-----------------------------------------------------------------------------

    void BestFitPlacer::ExportCreatedObjects( IMainCommands* cmdIfc )
    {
      if ( m_objectsOut.Size() > 0 )
      {
        RString name = m_objectsIn[m_objectsOut[0].type].m_name;
        Vector3D position = Vector3D( m_objectsOut[0].position.x, 
                                                             0.0, 
                                      m_objectsOut[0].position.y );

        Matrix4D mTranslation = Matrix4D( MTranslation, position );
        Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[0].rotation );

        Matrix4D transform = mTranslation * mRotation;
        cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
      }
    }

//-----------------------------------------------------------------------------

    void BestFitPlacer::WriteReport()
    {
      int objOutCount = m_objectsOut.Size();
      LOGF( Info ) << "-----------------------------------------------------------";
      if ( objOutCount > 0 )
      {
        LOGF( Info ) << "Created 1 new object (" << m_objectsIn[m_objectsOut[0].type].m_name << ")";
      }
      else
      {
        LOGF( Info ) << "Created 0 new objects.";
      }

      LOGF( Info ) << "-----------------------------------------------------------";
    }

//-----------------------------------------------------------------------------

  } // namespace Modules
} // namespace LandBuildInt
