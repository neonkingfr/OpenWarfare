//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"
#include "..\..\HighMapLoaders\include\EtMath.h"

#include ".\RegularOnPathPlacer.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		RegularOnPathPlacer::RegularOnPathPlacer()
    : m_version( "1.1.1" )
    , m_shapeCounter( 0 )
		{
		}

//-----------------------------------------------------------------------------

		void RegularOnPathPlacer::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "RegularOnPathPlacer " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

			// gets shape parameters from calling cfg file
			if ( GetShapeParameters( cmdIfc ) )
			{
				// gets objects parameters from calling cfg file
				if ( GetObjectsParameters( cmdIfc ) )
				{
					// initializes random seed
					srand( m_shapeIn.m_randomSeed );

					// creates the objects
					CreateObjects( false );

					// export objects
					ExportCreatedObjects( cmdIfc );

					// writes the report
					WriteReport();
				}
			}
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* RegularOnPathPlacer::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                                                AutoArray< RegularOnPathPlacer::ObjectInput >& objectsIn )
		{
			m_currentShape = shape;
			m_shapeIn      = shapeIn;
			m_objectsIn    = objectsIn;

      if ( !m_currentShape ) { return (NULL); }

      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			// initializes random seed
			srand( m_shapeIn.m_randomSeed );

			// creates the objects
			CreateObjects( true );

			return (&m_objectsOut);
		}

//-----------------------------------------------------------------------------

		bool RegularOnPathPlacer::GetShapeParameters( IMainCommands* cmdIfc )
		{
			// randomSeed (isMovable)
			const char* x = cmdIfc->QueryValue( "randomSeed", true );
			if ( !x ) { x = cmdIfc->QueryValue( "randomSeed", false ); }

      if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_randomSeed = static_cast< size_t >( atoi( x ) );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for randomSeed <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing value for randomSeed <<<" ) );
				return (false);
			}

      LOGF( Info ) << "randomSeed: " << m_shapeIn.m_randomSeed;

      // offset (isMovable)
			x = cmdIfc->QueryValue( "offset", true );
			if ( !x ) { x = cmdIfc->QueryValue( "offset", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_offset = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 21, ">>> Bad data for offset <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 22, ">>> Missing value for offset <<<" ) );
				return (false);
			}

      LOGF( Info ) << "offset: " << m_shapeIn.m_offset;

      // offsetMaxNoise (isMovable)
			x = cmdIfc->QueryValue( "offsetMaxNoise", true );
			if ( !x ) { x = cmdIfc->QueryValue( "offsetMaxNoise", false ); }

      if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_offsetMaxNoise = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 23, ">>> Bad data for offsetMaxNoise <<<" ) );
					return (false); 
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 24, ">>> Missing value for offsetMaxNoise <<<" ) );
				return (false);
			}

      LOGF( Info ) << "offsetMaxNoise: " << m_shapeIn.m_offsetMaxNoise;

      // step (isMovable)
			x = cmdIfc->QueryValue( "step", true );
			if ( !x ) { x = cmdIfc->QueryValue( "step", false ); }

      if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_step = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 25, ">>> Bad data for step <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 26, ">>> Missing value for step <<<" ) );
				return (false);
			}

      LOGF( Info ) << "step: " << m_shapeIn.m_step;

      // stepMaxNoise (isMovable)
			x = cmdIfc->QueryValue( "stepMaxNoise", true );
			if ( !x ) { x = cmdIfc->QueryValue( "stepMaxNoise", false ); }

      if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_stepMaxNoise = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 27, ">>> Bad data for stepMaxNoise <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 28, ">>> Missing value for stepMaxNoise <<<" ) );
				return (false);
			}

      LOGF( Info ) << "stepMaxNoise: " << m_shapeIn.m_stepMaxNoise;

      // firstStep (isMovable)
			x = cmdIfc->QueryValue( "firstStep", true );
			if ( !x ) { x = cmdIfc->QueryValue( "firstStep", false ); }

      if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_firstStep = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 29, ">>> Bad data for firstStep <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 30, ">>> Missing value for firstStep <<<" ) );
				return (false);
			}

      LOGF( Info ) << "firstStep: " << m_shapeIn.m_firstStep;

			return (true);
		}

//-----------------------------------------------------------------------------

		bool RegularOnPathPlacer::GetObjectsParameters( IMainCommands* cmdIfc )
		{
			// resets arrays
			if ( m_objectsIn.Size() != 0 )
			{
				m_objectsIn.Clear();
				m_objectsOut.Clear();
			}

			// gets objects' data from calling cfg file
			string object        = "object";
			string minheight     = "minheight";
			string maxheight     = "maxheight";
			string matchorient   = "matchOrient";
			string dircorrection = "dirCorrection";
			string placeright    = "placeRight";
			string placeleft     = "placeLeft";

			ObjectInput nfo;

			// object type
			const char* x = cmdIfc->QueryValue( object.c_str(), false );
      if ( !x ) { return (false); }
			nfo.m_name = x;

      LOGF( Info ) << "object: " << nfo.m_name;

			// object min height
			nfo.m_minHeight = 100.0; // default value
			x = cmdIfc->QueryValue( minheight.c_str(), false );
			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					nfo.m_minHeight = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for minheight - using default value (100) <<<";
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for minheight - using default value (100) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
			}

      LOGF( Info ) << "minheight: " << nfo.m_minHeight;

			// object max height
			nfo.m_maxHeight = 100.0; // default value
			x = cmdIfc->QueryValue( maxheight.c_str(), false );
			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					nfo.m_maxHeight = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for maxheight - using default value (100) <<<";
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for maxheight - using default value (100) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
			}

      LOGF( Info ) << "maxheight: " << nfo.m_maxHeight;

			// object match orientation
			nfo.m_matchOrient = true; // default value
			x = cmdIfc->QueryValue( matchorient.c_str(), false );
			if ( x ) 
			{
				if ( IsBool( string( x ), false ) )
				{
					int i = static_cast< int >( atoi( x ) );
					if ( i == 0 )
					{
						nfo.m_matchOrient = false;
					}
				}
				else
				{
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for matchOrient - using default value (1) <<<";
          LOGF( Warn ) << ">>> -------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for matchOrient - using default value (1) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
			}

      LOGF( Info ) << "matchOrient: " << nfo.m_matchOrient;

			// object direction correction
			nfo.m_dirCorrection = true; // default value
			x = cmdIfc->QueryValue( dircorrection.c_str(), false );
			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					nfo.m_dirCorrection = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
          LOGF( Warn ) << ">>> Bad data for dirCorrection - using default value (0.0) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ----------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for dirCorrection - using default value (0.0) <<<";
        LOGF( Warn ) << ">>> ----------------------------------------------------------- <<<";
			}

      LOGF( Info ) << "dirCorrection: " << nfo.m_dirCorrection;

			// object place right
			x = cmdIfc->QueryValue( placeright.c_str(), false );
			nfo.m_placeRight = true; // default value
			if ( x ) 
			{
				if ( IsBool( string( x ), false ) )
				{
					int i = static_cast< int >( atoi( x ) );
					if ( i == 0 )
					{
						nfo.m_placeRight = false;
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for placeRight - using default value (1) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
        LOGF( Warn ) << ">>> Missing value for placeRight - using default value (1) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
			}

      LOGF( Info ) << "placeRight: " << nfo.m_placeRight;

			// object place left
			x = cmdIfc->QueryValue( placeleft.c_str(), false );
			nfo.m_placeLeft = true; // default value
			if ( x )  
			{
				if ( IsBool( string( x ), false ) )
				{
					int i = static_cast< int >( atoi( x ) );
					if ( i == 0 )
					{
						nfo.m_placeLeft = false;
					}
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
          LOGF( Warn ) << ">>> Bad data for placeLeft - using default value (1) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing value for placeLeft - using default value (1) <<<";
        LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
			}

      LOGF( Info ) << "placeLeft: " << nfo.m_placeLeft;

			m_objectsIn.Add( nfo );

			return (true);
		}

//-----------------------------------------------------------------------------

		void RegularOnPathPlacer::CreateObjects( bool preview )
		{
			double perimeter = GetShapePerimeter( m_currentShape );
			for ( double s = m_shapeIn.m_firstStep; s <= perimeter; s += m_shapeIn.m_step )
			{
				// random noise along step
				double sNoiseR = RandomInRangeF( -m_shapeIn.m_stepMaxNoise, m_shapeIn.m_stepMaxNoise );
				double sNoiseL = RandomInRangeF( -m_shapeIn.m_stepMaxNoise, m_shapeIn.m_stepMaxNoise );

				double correctedSR = s + sNoiseR;
				double correctedSL = s + sNoiseL;

        if ( correctedSR < 0.0 )       { correctedSR = 0.0; }
        if ( correctedSR > perimeter ) { correctedSR = perimeter; }
        if ( correctedSL < 0.0 )       { correctedSL = 0.0; }
        if ( correctedSL > perimeter ) { correctedSL = perimeter; }

				// random noise along offset
				double oNoiseR = RandomInRangeF( -m_shapeIn.m_offsetMaxNoise, m_shapeIn.m_offsetMaxNoise );
				double oNoiseL = RandomInRangeF( -m_shapeIn.m_offsetMaxNoise, m_shapeIn.m_offsetMaxNoise );

				Shapes::DVertex curV1;
				Shapes::DVertex curV2;

				if ( m_objectsIn[0].m_placeRight )
				{
					Shapes::DVertex curPosR = ConvertToXY( correctedSR, curV1, curV2 );
					Shapes::DVector curV12( curV2, curV1 );
					curV12.NormalizeXYInPlace();
					Shapes::DVector curR = curV12.PerpXYCW();
					curR *= (m_shapeIn.m_offset + oNoiseR);
					Shapes::DVertex posR = curR.GetSecondEndXY( curPosR );

					// new object
					ModuleObjectOutput out;

					// sets objects out data
					// type
					out.type     = 0;
					// position
					out.position = Shapes::DVertex( posR.x, posR.y );
					// rotation
					if ( m_objectsIn[0].m_matchOrient )
					{
						out.rotation = curV12.DirectionXY() + m_objectsIn[0].m_dirCorrection * EtMathD::PI / 180.0;
					}
					else
					{
						out.rotation = RandomInRangeF( 0.0, EtMathD::TWO_PI );
					}

					// scale
					double minScale = m_objectsIn[0].m_minHeight;
					double maxScale = m_objectsIn[0].m_maxHeight;
					double scale = RandomInRangeF( minScale, maxScale );
					out.scaleX = out.scaleY = out.scaleZ = scale / 100.0;

					// used only by VLB for preview
					out.length = 10.0;
					out.width  = 10.0;
					out.height = 10.0;

					// adds to array
					m_objectsOut.Add( out );
				}

				if ( m_objectsIn[0].m_placeLeft )
				{
					Shapes::DVertex curPosL = ConvertToXY( correctedSL, curV1, curV2 );
					Shapes::DVector curV12( curV2, curV1 );
					curV12.NormalizeXYInPlace();
					Shapes::DVector curL = curV12.PerpXYCCW();
					curL *= (m_shapeIn.m_offset + oNoiseL);
					Shapes::DVertex posL = curL.GetSecondEndXY( curPosL );

					// new object
					ModuleObjectOutput out;

					// sets objects out data
					// type
					out.type     = 0;
					// position
					out.position = Shapes::DVertex( posL.x, posL.y );
					// rotation
					if ( m_objectsIn[0].m_matchOrient )
					{
						out.rotation = curV12.DirectionXY() + m_objectsIn[0].m_dirCorrection * EtMathD::PI / 180.0;
					}
					else
					{
						out.rotation = RandomInRangeF( 0.0, EtMathD::TWO_PI );
					}
					// scale
					double minScale = m_objectsIn[0].m_minHeight;
					double maxScale = m_objectsIn[0].m_maxHeight;
					double scale = RandomInRangeF( minScale, maxScale );
					out.scaleX = out.scaleY = out.scaleZ = scale / 100.0;

					// used only by VLB for preview
					out.length = 10.0;
					out.width  = 10.0;
					out.height = 10.0;

					// adds to array
					m_objectsOut.Add( out );
				}
			}
		}

//-----------------------------------------------------------------------------

		void RegularOnPathPlacer::ExportCreatedObjects( IMainCommands* cmdIfc )
		{
			int objOutCount = m_objectsOut.Size();
			for ( int i = 0; i < objOutCount; ++i )
			{
				RString name      = m_objectsIn[m_objectsOut[i].type].m_name;
				Vector3D position = Vector3D( m_objectsOut[i].position.x, 
                                                             0.0, 
                                      m_objectsOut[i].position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[i].rotation );
				Matrix4D mScaling     = Matrix4D( MScale, m_objectsOut[i].scaleX, 
                                                  m_objectsOut[i].scaleZ, 
                                                  m_objectsOut[i].scaleY );

				Matrix4D transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

		void RegularOnPathPlacer::WriteReport()
		{
      int objOutCount = m_objectsOut.Size();
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Created " << objOutCount << " new objects";
      LOGF( Info ) << "-----------------------------------------------------------";
    }

//-----------------------------------------------------------------------------

		Shapes::DVertex RegularOnPathPlacer::ConvertToXY( double s, Shapes::DVertex& v1, Shapes::DVertex& v2 )
		{
			double dist;

			int vCount = m_currentShape->GetVertexCount();

			int index = -1;
			double tempPos = 0.0;

			for ( int i = 0; i < (vCount - 1); i++ )
			{
				v1 = m_currentShape->GetVertex( i );
				v2 = m_currentShape->GetVertex( i + 1 );
				dist = v1.DistanceXY( v2 );
				tempPos += dist;
				if ( s < tempPos )
				{
					index = i;
					break;
				}
			}

			if ( index == -1 )
			{
				v1 = m_currentShape->GetVertex( vCount - 1 );
				v2 = m_currentShape->GetVertex( 0 );
				dist = v1.DistanceXY( v2 );
				tempPos += dist;
			}
			else
			{
				v1 = m_currentShape->GetVertex( index );
				v2 = m_currentShape->GetVertex( index + 1 );
			}

			dist = v1.DistanceXY( v2 );			
			Shapes::DVector diff = v1 - v2;

			double localS = tempPos - s;
			double delta = 0.0;

			if ( dist != 0.0 )
			{
				delta = localS / dist;
			}

			double newX = v2.x + diff.x * delta;
			double newY = v2.y + diff.y * delta;

			return (Shapes::DVertex( newX, newY )); 
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt

