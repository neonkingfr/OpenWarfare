//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "XPalette.h"

//-----------------------------------------------------------------------------

typedef std::pair< int,int > RoadLoc;

//-----------------------------------------------------------------------------

TypeIsSimpleZeroed( RoadLoc );

//-----------------------------------------------------------------------------

struct XPaletteItemEx : public XPaletteItem
{
  AutoArray< RoadLoc > m_exitIds;
  AutoArray< RoadLoc > m_typeCount;

  ClassIsMovable( XPaletteItemEx );
};

//-----------------------------------------------------------------------------

class RoadBuilder : public LandBuilder2::IBuildModule
{
  bool   m_endPhaseProcessed;
  string m_version;

protected:
  static double Vector2Azimut( double x, double y );
  static double Vector2Azimut( const Shapes::DVector& vx );
  static double Vector2Azimut( const Vector3& vx );

  static Vector3D Azimut2VectorVec( double azimut );
  static Shapes::DVector Azimut2VectorDVec( double azimut );
  static std::pair< double, double > Azimut2VectorXY( double azimut );

  struct Crossroad
  {
    ///position of crossroad
    Shapes::DVertex m_location;

    ///size of crossroad (radius)
    double m_size;

    ///space around crossroad, that must not contain any other crossroad
    /**
        mostly it is 3x bigger then _size
    */
    double m_padding;

    ///shape represents xroad
    Shapes::IShape* m_xshape;
    
    ///index do vertex array - center of crossroad
    size_t m_xpoint;

    RStringB m_resultShapefile;

    RStringB m_xpaletteFile;        

    RStringB m_forcedTexture;

    Crossroad( const Shapes::DVertex& location, double size, double padding, const char* xpaletteFile,
               const RString& resultShapefile = RString() )
    : m_location( location )
    , m_size( size )
    , m_padding( padding < size ? m_size * 3 : padding )
    , m_xshape( 0 )
    , m_xpoint( -1 )
    , m_resultShapefile( resultShapefile )
    , m_xpaletteFile( xpaletteFile ) 
    {
    }

    Crossroad() 
    {
    }

    ClassIsMovable( Crossroad );
  };

  struct RoadProp
  {
    int     m_partCount;
    double  m_width;
    double  m_step;
    RString m_roadType;
    RString m_resultShapefile;
    RString m_forcedTexture;
  };

  struct Road
  {
    Shapes::PShape m_shape;
    int            m_startCross;
    int            m_endCross;
    double         m_startAzimut;
    double         m_endAzimut;
    RoadProp       m_properties;

    Road() 
    {
    }

    Road( Shapes::IShape* shape, int startCross, int endCross, const RoadProp& properties )
    : m_shape( shape )
    , m_endCross( endCross )
    , m_startCross( startCross )
    , m_startAzimut( RoadBuilder::Vector2Azimut( m_shape->GetVertex( 1 ) - m_shape->GetVertex( 0 ) ) )
    , m_endAzimut( RoadBuilder::Vector2Azimut( m_shape->GetVertex( m_shape->GetVertexCount() - 2 ) - m_shape->GetVertex( m_shape->GetVertexCount() - 1 ) ) )
    , m_properties( properties )
    {
    }

    ClassIsMovable( Road );
  };

  AutoArray< Crossroad > m_crossroadList;
  AutoArray< Road >      m_roadList;

  void AddCrossPoint( IMainCommands* cmdIfc );
  void AddRoadLine( IMainCommands* cmdIfc );
  void AddRoadLine( IMainCommands* cmdIfc, Shapes::ShapePolyLine* polyline );

  int FindNearestXRoad( const Shapes::DVector& point, int from = 0 );

  void OptimizeXRoads();
  void JoinTwoXRoads( int leave, int remove );

  void OptimizeRoadShapes( IMainCommands* cmdIfc );
  void OptimizeRoadShape( Road& road );

  void SelectBestCrossroadModels( IMainCommands* cmdIfc );
  void BuildXPaletteItems( Array< XPaletteItemEx >& xitems );

  void GenerateRoadShapes( IMainCommands* cmdIfc );
  void GenerateRoadShape( IMainCommands* cmdIfc, Road& road ); 

public:
  RoadBuilder();
  virtual ~RoadBuilder();

  virtual void ProcessShape( IMainCommands* cmdIfc );

  virtual void OnEndPass( IMainCommands* cmdIfc );

  virtual void Run( IMainCommands* cmdIfc ) 
  {
    ProcessShape( cmdIfc );
  }
};

//-----------------------------------------------------------------------------
