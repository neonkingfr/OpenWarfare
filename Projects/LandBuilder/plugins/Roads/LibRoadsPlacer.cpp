#include "StdAfx.h"
#include "LibRoadsPlacer.h"
#include <IProjectTask.h>

// -------------------------------------------------------------------------- //

static LandBuilder2::RegisterModule< LibRoadsPlacer > libRoadsPlacer( "LibRoadsPlacer" );

// -------------------------------------------------------------------------- //

LibRoadsPlacer::LibRoadsPlacer()
: m_firstRun( true )
, m_canProceed( true )
, m_allRoadsLoaded( false )
{
}

// -------------------------------------------------------------------------- //

LibRoadsPlacer::~LibRoadsPlacer()
{
}

// -------------------------------------------------------------------------- //

void LibRoadsPlacer::Run( IMainCommands* pCmdIfc )
{
  if ( m_allRoadsLoaded ) return;

  LandBuilder2::ParamParser params( pCmdIfc );

  // here we get the global data
  if ( m_firstRun )
  {
    const LandBuilder2::IProjectTask* curTask = pCmdIfc->GetCurrentTask();

    try 
    {
      m_libraryFile = params.GetTextValue( "libraryFile" );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      LOGF( Error ) << "Missing Library config file name.";
      m_canProceed = false;
    }

    m_libraryFile = curTask->GetWorkFolder() + m_libraryFile;

    if ( m_canProceed )
    {
      if ( !m_library.LoadFromCfgFile( m_libraryFile ) ) 
      {
        LOGF( Error ) << "Error loading library config file. Please, verify data.";
        m_canProceed = false;
      }
    }

    if ( m_canProceed )
    {
      string dxfFilename = m_libraryFile.Substring( 0, m_libraryFile.ReverseFind( '\\' ) + 1 ).Data();
      dxfFilename += "NewPoly.dxf";
      m_dxfWriter.Filename( dxfFilename );
    }

    m_firstRun = false;
  }

  if ( m_canProceed )
  {
    Shapes::IShape* curShape = const_cast< Shapes::IShape* >( pCmdIfc->GetActiveShape() );

    RString errorMsg = "LibRoadsPlacer module accepts only shapefiles containing non-multipart polylines.";

    // checks for multipart polylines
    if ( curShape->GetPartCount() > 1 )
    {
      LOGF( Error ) << errorMsg;
      return;
    }

    // checks for known shapes
    const IShapeExtra* extra = curShape->GetInterface< IShapeExtra >();
    if ( extra == 0 ) 
    {
      LOGF( Error ) << "Found unknown shape in shapefile. Cannot get type of that shape.";
      return;
    }

    // checks for polylines only
    RString shapeType = extra->GetShapeTypeName();
    if ( shapeType != RString( SHAPE_NAME_POLYLINE ) ) 
    {
      LOGF( Error ) << errorMsg;
      return;
    }

    // these are mandatory data
    float width = 0.0;
    string type = "";

    try 
    {
      width = params.GetFloatValue( "width" );
      type  = params.GetTextValue( "type" );
    }
    catch ( LandBuilder2::ParamParserException& e ) 
    {
      LOGF( Error ) << "Cannot parse parameters: " << e.Message() << " " << e.GetName() << "=" << e.GetValue();
      return;
    }

    if ( width == 0.0 )
    {
      LOGF( Error ) << "Found road with null width.";
      return;
    }

    if ( type == "" )
    {
      LOGF( Error ) << "Found road with null type.";
      return;
    }

    Road road( m_library, width, type );
    for ( size_t i = 0, cnt = curShape->GetVertexCount(); i < cnt; ++i )
    {
      road.AddShapeVertex( curShape->GetVertex( i ) );
    }

    road.StartPosition( curShape->GetVertex( 0 ) );

    Shapes::DVector v01( curShape->GetVertex( 1 ), curShape->GetVertex( 0 ) );
    road.StartOrientation( v01.DirectionYX() );

    m_roadsList.push_back( road );
  }
}

// -------------------------------------------------------------------------- //

void LibRoadsPlacer::OnEndPass( IMainCommands* pCmdIfc ) 
{
  if ( m_allRoadsLoaded ) { return; }
  m_allRoadsLoaded = true;

  if ( m_canProceed )
  {
    CreateNodeList();
    PreprocessNodes();

    for ( size_t i = 0, cnt = m_roadsList.size(); i < cnt; ++i )
    {
      if ( m_roadsList[i].InUse() )
      {
        m_roadsList[i].Build();
      }
    }

    // correction of roads start position due to visitor3 way of work
    for ( size_t i = 0, cnt = m_roadsList.size(); i < cnt; ++i )
    {
      if ( m_roadsList[i].InUse() )
      {
        m_roadsList[i].V3Correction();
      }
    }

//		PostprocessNodes();

    for ( size_t i = 0, cnt = m_roadsList.size(); i < cnt; ++i )
    {
      if ( m_roadsList[i].InUse()) { ExportDxf( m_roadsList[i] ); }
    }

    ExportModels( pCmdIfc );				
  }
}

// -------------------------------------------------------------------------- //

void LibRoadsPlacer::CreateNodeList()
{
  for ( size_t i = 0, cntR = m_roadsList.size(); i < cntR; ++i )
  {
    // the crossroads canditates are all the roads ending points
    Road& road = m_roadsList[i];

    int foundS = -1;
    int foundE = -1;
    for ( size_t j = 0, cntN = m_nodesList.size(); j < cntN; ++j )
    {
      if ( m_nodesList[j].EqualPos( road.ShapeStartVertex() ) ) 
      {
        foundS = j;
        break;
      }
    }
    for ( size_t j = 0, cntN = m_nodesList.size(); j < cntN; ++j )
    {
      if ( m_nodesList[j].EqualPos( road.ShapeEndVertex() ) )
      {
        foundE = j;
        break;
      }
    }
    if ( foundS == -1 )
    {
      m_nodesList.push_back( Node( road.ShapeStartVertex() ) );
      foundS = m_nodesList.size() - 1;
    }
    if ( foundE == -1 )
    {
      m_nodesList.push_back( Node( road.ShapeEndVertex() ) );
      foundE = m_nodesList.size() - 1;
    }

    m_nodesList[foundS].AddRoad( RoadEx( &road, etStarting ) );
    m_nodesList[foundE].AddRoad( RoadEx( &road, etEnding ) );
   }
}

// -------------------------------------------------------------------------- //

void LibRoadsPlacer::PreprocessNodes()
{
  size_t i = 0;
  while ( i < m_nodesList.size() )
  {
    Node& node = m_nodesList[i];
    size_t count = node.InitCandidatesToMerge();

    size_t roadsCount = node.RoadsCount();

    if ( roadsCount == 2 && count == 2 )
    {
      node.MergeRoads2_2( m_nodesList );
      // we must now delete the node from the node list
      m_nodesList.erase( m_nodesList.begin() + i );
      --i;
    }
    else if ( roadsCount == 3 && count == 2 )
    {
      node.MergeRoads2_3( m_nodesList );
    }
    else if (roadsCount == 3 && count == 3)
    {
      node.MergeRoads2_3E( m_nodesList );
    }
    else if ( roadsCount == 4 && count == 2 )
    {
      node.MergeRoads2_4( m_nodesList );
    }
    else if ( roadsCount == 4 && count == 3 )
    {
      node.MergeRoads2_4E( m_nodesList );
    }
    else
    {
    }
    ++i;
  }
}

// -------------------------------------------------------------------------- //

void LibRoadsPlacer::PostprocessNodes()
{
  size_t i = 0;
  while ( i < m_nodesList.size() )
  {
    Node& node = m_nodesList[i];

    if ( node.IsTransit() )
    {
      node.RebuildNonTransitRoads();
    }

    ++i;
  }

  for ( size_t i = 0, cntI = m_roadsList.size(); i < cntI; ++i )
  {
    if ( m_roadsList[i].InUse() )
    {
      if ( !m_roadsList[i].Built() )
      {
        m_roadsList[i].Build();
      }
    }
  }
}

// -------------------------------------------------------------------------- //

void LibRoadsPlacer::ExportDxf( const Road& road )
{
  vector< Shapes::DVertex > polyline;

  for ( size_t i = 0, cnt = road.ShapeVerticesCount(); i < cnt; ++i )
  {
    polyline.push_back( road.ShapeVertex( i ) );
  }
  m_dxfWriter.WritePolyline( polyline, "Shapes", false );

  polyline.clear();
  for ( size_t i = 0, cnt = road.SegmentShapeVerticesCount(); i < cnt; ++i )
  {
    polyline.push_back( road.SegmentShapeVertex( i ) );
  }
  m_dxfWriter.WritePolyline( polyline, "SegmentShapes", false );

  const vector< RoadSegmentBase* >& roadSegments = road.Segments();
  if ( roadSegments.size() > 0 )
  {
    for ( size_t i = 0, cnt = roadSegments.size(); i < cnt; ++i )
    {
      vector< Shapes::DVertex > contour = roadSegments[i]->Contour( road.Width() );
      string modelName = roadSegments[i]->ModelName( road.Type() );
      m_dxfWriter.WritePolyline( contour, modelName, true );
    }
  }
}

// ---------------------------------------------------------------------- //

void LibRoadsPlacer::ExportModels( IMainCommands* pCmdIfc )
{
  // first creates roads definitions list
  m_roadDefinitions.clear();
  for ( size_t i = 0, cntI = m_roadsList.size(); i < cntI; ++i )
  {
    if ( m_roadsList[i].InUse() )
    {
      string name = "LBRoad_" + m_roadsList[i].Type();
      LandBuilder2::LBRoadDefinition roadDef( name );

      bool found = false;
      int index = -1;
      for ( size_t j = 0, cntJ = m_roadDefinitions.size(); j < cntJ; ++j )
      {
        if ( m_roadDefinitions[j].GetName() == roadDef.GetName() )
        {
          found = true;
          index = j;
          break;
        }
      }
      if ( !found )
      {
        for ( size_t j = 0, cntJ = m_roadsList[i].Library()->SegmentsCount(); j < cntJ; ++j )
        {
          LandBuilder2::LBRoadPartModel roadPart;
          const RoadSegmentBase* pSeg = m_roadsList[i].Library()->GetSegment( j );
          roadPart.SetName( pSeg->ModelName( m_roadsList[i].Type() ) );
          switch ( pSeg->Type() )
          {
          case RS_Straight:   roadPart.SetType( LandBuilder2::RPMT_Straight );   break;
          case RS_Corner:     roadPart.SetType( LandBuilder2::RPMT_Corner );     break;
          case RS_Terminator: roadPart.SetType( LandBuilder2::RPMT_Terminator ); break;
          default:            roadPart.SetType( LandBuilder2::RPMT_Straight );
          }
          roadDef.AddModel( roadPart );
        }
        m_roadDefinitions.push_back( roadDef );
      }
      else
      {
        LandBuilder2::LBRoadDefinition& tempDef = m_roadDefinitions[index];

        for ( size_t j = 0, cntJ = m_roadsList[i].Library()->SegmentsCount(); j < cntJ; ++j )
        {
          LandBuilder2::LBRoadPartModel roadPart;
          const RoadSegmentBase* pSeg = m_roadsList[i].Library()->GetSegment( j );
          roadPart.SetName( pSeg->ModelName( m_roadsList[i].Type() ) );
          switch ( pSeg->Type() )
          {
          case RS_Straight:   roadPart.SetType( LandBuilder2::RPMT_Straight );   break;
          case RS_Corner:     roadPart.SetType( LandBuilder2::RPMT_Corner );     break;
          case RS_Terminator: roadPart.SetType( LandBuilder2::RPMT_Terminator ); break;
          default:            roadPart.SetType( LandBuilder2::RPMT_Straight );
          }
          roadDef.AddModel( roadPart );
        }
      }
    }
  }

  // exports roads definitions list
  if ( m_roadDefinitions.size() > 0 )
  {
    for ( size_t i = 0, cntI = m_roadDefinitions.size(); i < cntI; ++i )
    {
      pCmdIfc->GetRoadMap()->AddRoadDefinition( m_roadDefinitions[i] );
    }
  }

  // now exports roads paths
  m_roadPaths.clear();
  for ( size_t i = 0, cntI = m_roadsList.size(); i < cntI; ++i )
  {
    if ( m_roadsList[i].InUse() )
    {
      const vector< RoadSegmentBase* >& roadSegments = m_roadsList[i].Segments();

      if ( roadSegments.size() > 0 )
      {
        string name = "LBRoad_" + m_roadsList[i].Type();
        LandBuilder2::LBRoadPath roadPath( name );
        roadPath.SetStartPosition( m_roadsList[i].StartPosition() );
        roadPath.SetStartOrientation( m_roadsList[i].StartOrientation() );

        for ( size_t j = 0, cntJ = roadSegments.size(); j < cntJ; ++j )
        {
          LandBuilder2::LBRoadPartModel roadPart;
          roadPart.SetName( roadSegments[j]->ModelName( m_roadsList[i].Type() ) );
          switch ( roadSegments[j]->Type() )
          {
          case RS_Straight: 
            {
              roadPart.SetType( LandBuilder2::RPMT_Straight );
            }
            break;
          case RS_Corner:
            {
              roadPart.SetType( LandBuilder2::RPMT_Corner );
              const RoadSegmentCorner* pPtr = reinterpret_cast< const RoadSegmentCorner* >( roadSegments[j] );
              if ( pPtr->Inverted() )
              {
                roadPart.SetRightOriented( false );
              }
            }
            break;
          case RS_Terminator:
            {
              roadPart.SetType( LandBuilder2::RPMT_Terminator );
            }
            break;
          default:
            {
              roadPart.SetType( LandBuilder2::RPMT_Straight );
            }
          }
          roadPath.AddRoadPart( roadPart );
        }

        m_roadPaths.push_back( roadPath );
      }		
    }
  }

  // exports roads definitions list
  if ( m_roadPaths.size() > 0 )
  {
    for ( size_t i = 0, cntI = m_roadPaths.size(); i < cntI; ++i )
    {
      pCmdIfc->GetRoadMap()->AddRoadPath( m_roadPaths[i] );
    }
  }	
}

// ---------------------------------------------------------------------- //
