#include "StdAfx.h"

#include <algorithm>

#include "LibRoadsPlacerHelpers.h"
#include "BSpline.h"

// -------------------------------------------------------------------------- //

namespace LRP_Help
{
	// -------------------------------------------------------------------------- //
	// Dxf2006Writer
	// -------------------------------------------------------------------------- //

	Dxf2006Writer::Dxf2006Writer()
	{
	}

	// -------------------------------------------------------------------------- //

	Dxf2006Writer::Dxf2006Writer(const string& filename)
	{
		m_File.open(filename.c_str(), std::ios::out | std::ios::trunc);
		if (!m_File.fail()) 
		{
			WriteEntitiesHead();
		}
		else
		{
			LOGF(Error) << "Failed to open dxf file.";
		}
	}

	// -------------------------------------------------------------------------- //

	Dxf2006Writer::~Dxf2006Writer()
	{
		if (!m_File.fail()) 
		{
			WriteEntitiesTail();
			WriteFileTail();
			m_File.close();
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::Filename(const string& filename)
	{
		if (m_File.is_open())
		{
			m_File.close();
		}

		m_File.open(filename.c_str(), std::ios::out | std::ios::trunc);
		if (!m_File.fail()) 
		{
			WriteEntitiesHead();
		}
		else
		{
			LOGF(Error) << "Failed to open dxf file.";
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WriteEntitiesHead()
	{
		if (!m_File.fail()) 
		{
			string out = "  0\nSECTION\n  2\nENTITIES";
			m_File << out << endl;
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WriteEntitiesTail()
	{
		if (!m_File.fail()) 
		{
			string out = "  0\nENDSEC";
			m_File << out << endl;
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WriteFileTail()
	{
		if (!m_File.fail()) 
		{
			string out = "  0\nEOF";
			m_File << out << endl;
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WriteCircle(const Shapes::DVertex& center, double radius, 
									const string& layer)
	{
		if (!m_File.fail()) 
		{
			char buffer[80];
			string out = "  0\nCIRCLE\n100\nAcDbEntity\n  8\n";
			out += (layer + "\n100\nAcDbCircle\n 10\n");
			_gcvt_s(buffer, 80, center.x, 8);
			out += (string(buffer) + "\n 20\n");
			_gcvt_s(buffer, 80, center.y, 8);
			out += (string(buffer) + "\n 40\n");
			_gcvt_s(buffer, 80, radius, 8);
			out += string(buffer);
			m_File << out << endl;
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WritePolylineAsLines(const vector<Shapes::DVertex>& polyline, 
											 const string& layer, bool close)
	{
		if (!m_File.fail()) 
		{
			unsigned int size = polyline.size();

			if (size > 1)
			{
				char buffer[80];
				for (unsigned int i = 0; i < size - 1; ++i)
				{
					string out = "  0\nLINE\n100\nAcDbEntity\n  8\n";
					out += (layer + "\n100\nAcDbLine\n 10\n");
					_gcvt_s(buffer, 80, polyline[i].x, 8);
					out += (string(buffer) + "\n 20\n");
					_gcvt_s(buffer, 80, polyline[i].y, 8);
					out += (string(buffer) + "\n 11\n");
					_gcvt_s(buffer, 80, polyline[i + 1].x, 8);
					out += (string(buffer) + "\n 21\n");
					_gcvt_s(buffer, 80, polyline[i + 1].y, 8);
					out += string(buffer);
					m_File << out << endl;
				}
				if (close)
				{
					string out = "  0\nLINE\n100\nAcDbEntity\n  8\n";
					out += (layer + "\n100\nAcDbLine\n 10\n");
					_gcvt_s(buffer, 80, polyline[polyline.size() - 1].x, 8);
					out += (string(buffer) + "\n 20\n");
					_gcvt_s(buffer, 80, polyline[polyline.size() - 1].y, 8);
					out += (string(buffer) + "\n 11\n");
					_gcvt_s(buffer, 80, polyline[0].x, 8);
					out += (string(buffer) + "\n 21\n");
					_gcvt_s(buffer, 80, polyline[0].y, 8);
					out += string(buffer);
					m_File << out << endl;
				}
			}
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WritePolyline(const vector<Shapes::DVertex>& polyline, 
									  const string& layer, bool close, 
									  double startingWidth, 
									  double endingWidth)
	{
		if (!m_File.fail()) 
		{
			unsigned int size = polyline.size();

			if (size > 1)
			{
				char buffer[80];
				string out = "  0\nPOLYLINE\n100\nAcDbEntity\n  8\n";
				out += (layer + "\n 66\n     1\n100\nAcDb2dPolyline\n 10\n0.0\n 20\n0.0\n 30\n0.0\n");
				if (close)
				{
					out += " 70\n1\n";
				}
				else
				{
					out += " 70\n0\n";
				}
				_gcvt_s(buffer, 80, startingWidth, 8);
				out += (" 40\n" + string(buffer) +"\n");
				_gcvt_s(buffer, 80, endingWidth, 8);
				out += (" 41\n" + string(buffer) +"\n");
				for (unsigned int i = 0; i < size; ++i)
				{
					out += "  0\nVERTEX\n100\nAcDbVertex\n  8\n";
					out += (layer + "\n100\nAcDb2dVertex\n 10\n");
					_gcvt_s(buffer, 80, polyline[i].x, 8);
					out += (string(buffer) + "\n 20\n");
					_gcvt_s(buffer, 80, polyline[i].y, 8);
					out += (string(buffer) + "\n 30\n0.0\n");
				}
				out += "  0\nSEQEND\n  8\n";
				out += layer;
				m_File << out << endl;
			}
		}
	}

	// ---------------------------------------------------------------------- //
	// RoadSegmentBase
	// ---------------------------------------------------------------------- //

	RoadSegmentBase::RoadSegmentBase()
	: m_Type(RS_Unknown)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentBase::RoadSegmentBase(const RoadSegTypes& type)
	: m_Type(type)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentBase::~RoadSegmentBase()
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegTypes RoadSegmentBase::Type() const
	{
		return m_Type;
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentBase::Orientation() const
	{
		return m_Orientation;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentBase::Type(const RoadSegTypes& type)
	{
		m_Type = type;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentBase::Orientation(double orientation)
	{
		m_Orientation = orientation;
	}

	// ---------------------------------------------------------------------- //
	// RoadSegmentStraight
	// ---------------------------------------------------------------------- //

	RoadSegmentStraight::RoadSegmentStraight()
	: RoadSegmentBase(RS_Straight)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentStraight::RoadSegmentStraight(double length)
	: RoadSegmentBase(RS_Straight)
	, m_Length(length)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentStraight::RoadSegmentStraight(const RoadSegmentStraight& other)
	: RoadSegmentBase(RS_Straight)
	, m_Length(other.m_Length)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentStraight::~RoadSegmentStraight()
	{
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentStraight::operator == (const RoadSegmentStraight& other) const
	{
		if (m_Length != other.m_Length) return false;

		return true;
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex RoadSegmentStraight::Position() const
	{
		return m_Position;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentStraight::Position(const Shapes::DVertex& position)
	{
		m_Position = position;
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentStraight::Length() const
	{
		return m_Length;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentStraight::Length(double length)
	{
		m_Length = length;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentStraight::Equals(const RoadSegmentBase& other) const
	{
		if (other.Type() != m_Type) return false;

		const RoadSegmentStraight* ptrOther = reinterpret_cast<const RoadSegmentStraight*>(&other);

		if (m_Length != ptrOther->m_Length) return false;

		return true;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentStraight::IsMultiple(const RoadSegmentBase& other, unsigned int count) const
	{
		if (other.Type() != m_Type) return false;
		if (count < 2) return false;

		const RoadSegmentStraight* ptrOther = reinterpret_cast<const RoadSegmentStraight*>(&other);

		double length = count * ptrOther->m_Length;

		if (fabs(m_Length - length) < 0.0001) return true;

		return false;
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex RoadSegmentStraight::NextPosition() const
	{
		// relative position
		double dx = 0.0;
		double dy = m_Length;

		// absolute position
		double x = m_Position.x + dx * cos(m_Orientation) - dy * sin(m_Orientation);
		double y = m_Position.y + dx * sin(m_Orientation) + dy * cos(m_Orientation);

		return Shapes::DVertex(x, y);
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentStraight::NextOrientation() const
	{
		return m_Orientation;
	}

	// ---------------------------------------------------------------------- //

	vector<Shapes::DVertex> RoadSegmentStraight::ShapeVertices() const
	{
		vector<Shapes::DVertex> shapeVertices;
		shapeVertices.push_back(NextPosition());
		return shapeVertices;
	}

	// ---------------------------------------------------------------------- //

	vector<Shapes::DVertex> RoadSegmentStraight::Contour(double width) const
	{
		vector<Shapes::DVertex> contour;
		double halfWidth = 0.5 * width;

		// local system
		contour.push_back(Shapes::DVertex(-halfWidth, 0.0));
		contour.push_back(Shapes::DVertex(halfWidth, 0.0));
		contour.push_back(Shapes::DVertex(halfWidth, m_Length));
		contour.push_back(Shapes::DVertex(-halfWidth, m_Length));

		// world system
		for (size_t i = 0, cnt = contour.size(); i < cnt; ++i)
		{
			double dx = contour[i].x;
			double dy = contour[i].y;
			contour[i].x = m_Position.x + dx * cos(m_Orientation) - dy * sin(m_Orientation);
			contour[i].y = m_Position.y + dx * sin(m_Orientation) + dy * cos(m_Orientation);
		}

		return contour;
	}

	// ---------------------------------------------------------------------- //

	string RoadSegmentStraight::ModelName(const string& suffix) const
	{
		double lfloor = floor(m_Length);
		double lceil  = ceil(m_Length);
		int val = (lceil - m_Length) < (m_Length - lfloor) ? static_cast<int>(lceil) : static_cast<int>(lfloor); 
		char bufLen[50];
		_itoa_s(val, bufLen, 50, 10);
		string length = bufLen;
		return (suffix + length);
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex RoadSegmentStraight::ModelPosition(double width) const
	{
		// local system
		Shapes::DVertex modelPosition(0.0, 0.5 * m_Length);

		// world system
		double dx = modelPosition.x;
		double dy = modelPosition.y;
		modelPosition.x = m_Position.x + dx * cos(m_Orientation) - dy * sin(m_Orientation);
		modelPosition.y = m_Position.y + dx * sin(m_Orientation) + dy * cos(m_Orientation);

		return modelPosition;
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentStraight::ModelOrientation() const
	{
		return m_Orientation;
	}

	// ---------------------------------------------------------------------- //
	// RoadSegmentCorner
	// ---------------------------------------------------------------------- //

	RoadSegmentCorner::RoadSegmentCorner()
	: RoadSegmentBase(RS_Corner)
	, m_Inverted(false)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentCorner::RoadSegmentCorner(double angle, double radius)
	: RoadSegmentBase(RS_Corner)
	, m_Angle(angle)
	, m_Radius(radius)
	, m_Inverted(false)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentCorner::RoadSegmentCorner(const RoadSegmentCorner& other)
	: RoadSegmentBase(RS_Corner)
	, m_Angle(other.m_Angle)
	, m_Radius(other.m_Radius)
	, m_Inverted(other.m_Inverted)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentCorner::~RoadSegmentCorner()
	{
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentCorner::operator == (const RoadSegmentCorner& other) const
	{
		if (m_Angle  != other.m_Angle)  return false;
		if (m_Radius != other.m_Radius) return false;

		return true;
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex RoadSegmentCorner::Position() const
	{
		return m_Position;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentCorner::Position(const Shapes::DVertex& position)
	{
		if (m_Inverted)
		{
			// relative position
			double dx = -(m_Radius * (1 - cos(m_Angle)));
			double dy = m_Radius * sin(m_Angle);

			// absolute position
			m_Position.x = position.x + dx * cos(m_Orientation) - dy * sin(m_Orientation);
			m_Position.y = position.y + dx * sin(m_Orientation) + dy * cos(m_Orientation);
		}
		else
		{
			m_Position = position;
		}
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentCorner::Angle() const
	{
		return m_Angle;
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentCorner::Radius() const
	{
		return m_Radius;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentCorner::Inverted() const
	{
		return m_Inverted;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentCorner::Angle(double angle)
	{
		m_Angle = angle;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentCorner::Radius(double radius)
	{
		m_Radius = radius;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentCorner::Inverted(bool inverted)
	{
		m_Inverted = inverted;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentCorner::Equals(const RoadSegmentBase& other) const
	{
		if (other.Type() != m_Type) return false;

		const RoadSegmentCorner* ptrOther = reinterpret_cast<const RoadSegmentCorner*>(&other);

		if (m_Angle    != ptrOther->m_Angle)    return false;
		if (m_Radius   != ptrOther->m_Radius)   return false;
		if (m_Inverted != ptrOther->m_Inverted) return false;

		return true;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentCorner::IsMultiple(const RoadSegmentBase& other, unsigned int count) const
	{
		if (other.Type() != m_Type) return false;
		if (count < 2) return false;

		const RoadSegmentCorner* ptrOther = reinterpret_cast<const RoadSegmentCorner*>(&other);

		if (m_Radius != ptrOther->m_Radius) return false;

		double angle = count * ptrOther->m_Angle;

		if (fabs(m_Angle - angle) < 0.0001) return true;

		return false;
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex RoadSegmentCorner::NextPosition() const
	{
		if (m_Inverted)
		{
			return m_Position;
		}
		else
		{
			// relative position
			double dx = m_Radius * (1 - cos(m_Angle));
			double dy = m_Radius * sin(m_Angle);

			// absolute position
			double x = m_Position.x + dx * cos(m_Orientation) - dy * sin(m_Orientation);
			double y = m_Position.y + dx * sin(m_Orientation) + dy * cos(m_Orientation);

			return Shapes::DVertex(x, y);
		}
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentCorner::NextOrientation() const
	{
		if (!m_Inverted)
		{
			double newOr = m_Orientation - m_Angle;
			if (newOr < 0.0) newOr += TWO_PI;
			return newOr;
		}
		else
		{
			double newOr = m_Orientation + m_Angle;
			if (newOr > TWO_PI) newOr -= TWO_PI;
			return newOr;
		}
	}

	// ---------------------------------------------------------------------- //

	vector<Shapes::DVertex> RoadSegmentCorner::ShapeVertices() const
	{
		vector<Shapes::DVertex> shapeVertices;

		double angleStep = m_Angle / 16.0;
		double x = m_Radius - m_Radius * cos(8 * angleStep);
		double y = m_Radius * sin(8 * angleStep);

		Shapes::DVertex midVertex(x, y);

		double corrOrientation;

		if (!m_Inverted)
		{
			corrOrientation = m_Orientation;
		}
		else
		{
			corrOrientation = m_Orientation + m_Angle + PI;
		}

		// world system
		double dx = midVertex.x;
		double dy = midVertex.y;
		midVertex.x = m_Position.x + dx * cos(corrOrientation) - dy * sin(corrOrientation);
		midVertex.y = m_Position.y + dx * sin(corrOrientation) + dy * cos(corrOrientation);
		shapeVertices.push_back(midVertex);

		shapeVertices.push_back(NextPosition());
		return shapeVertices;
	}

	// ---------------------------------------------------------------------- //

	vector<Shapes::DVertex> RoadSegmentCorner::Contour(double width) const
	{
		vector<Shapes::DVertex> contour;
		double halfWidth = 0.5 * width;
		double angleStep = m_Angle / 16.0;

		for (int i = 0; i <= 16; ++i)
		{
			double x = m_Radius - (m_Radius - halfWidth) * cos(i * angleStep);
			double y = (m_Radius - halfWidth) * sin(i * angleStep);
			contour.push_back(Shapes::DVertex(x, y));
		}
		for (int i = 16; i >= 0; --i)
		{
			double x = m_Radius - (m_Radius + halfWidth) * cos(i * angleStep);
			double y = (m_Radius + halfWidth) * sin(i * angleStep);
			contour.push_back(Shapes::DVertex(x, y));
		}

		double corrOrientation;

		if (!m_Inverted)
		{
			corrOrientation = m_Orientation;
		}
		else
		{
			corrOrientation = m_Orientation + m_Angle + PI;
		}

		// world system
		for (size_t i = 0, cnt = contour.size(); i < cnt; ++i)
		{
			double dx = contour[i].x;
			double dy = contour[i].y;
			contour[i].x = m_Position.x + dx * cos(corrOrientation) - dy * sin(corrOrientation);
			contour[i].y = m_Position.y + dx * sin(corrOrientation) + dy * cos(corrOrientation);
		}

		return contour;
	}

	// ---------------------------------------------------------------------- //

	string RoadSegmentCorner::ModelName(const string& suffix) const
	{
		double angle = m_Angle * 180 / PI;
		double afloor = floor(angle);
		double aceil  = ceil(angle);
		int val = (aceil - angle) < (angle - afloor) ? static_cast<int>(aceil) : static_cast<int>(afloor); 
		char bufAng[50];
		_itoa_s(val, bufAng, 50, 10);
		string angleStr = bufAng;

		double rfloor = floor(m_Radius);
		double rceil  = ceil(m_Radius);
		val = (rceil - m_Radius) < (m_Radius - rfloor) ? static_cast<int>(rceil) : static_cast<int>(rfloor); 
		char bufRad[50];
		_itoa_s(val, bufRad, 50, 10);
		string radiusStr = bufRad;
		return (suffix + angleStr + " " + radiusStr);
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex RoadSegmentCorner::ModelPosition(double width) const
	{
		double halfWidth = 0.5 * width;

		// local system
		double minX = m_Radius - (m_Radius + halfWidth);
		double minY = 0.0;
		double maxX = m_Radius - (m_Radius - halfWidth) * cos(m_Angle);
		double maxY = (m_Radius + halfWidth) * sin(m_Angle);

		Shapes::DVertex modelPosition(0.5 * (minX + maxX), 0.5 * (minY + maxY));

		double corrOrientation;

		if (!m_Inverted)
		{
			corrOrientation = m_Orientation;
		}
		else
		{
			corrOrientation = m_Orientation + m_Angle + PI;
		}

		// world system
		double dx = modelPosition.x;
		double dy = modelPosition.y;
		modelPosition.x = m_Position.x + dx * cos(corrOrientation) - dy * sin(corrOrientation);
		modelPosition.y = m_Position.y + dx * sin(corrOrientation) + dy * cos(corrOrientation);

		return modelPosition;
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentCorner::ModelOrientation() const
	{
		if (!m_Inverted)
		{
			return m_Orientation;
		}
		else
		{
			return (m_Orientation + m_Angle + PI);
		}
	}

	// ---------------------------------------------------------------------- //
	// RoadSegmentTerminator
	// ---------------------------------------------------------------------- //

	RoadSegmentTerminator::RoadSegmentTerminator()
	: RoadSegmentBase(RS_Terminator)
	, m_Inverted(false)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentTerminator::RoadSegmentTerminator(double length)
	: RoadSegmentBase(RS_Terminator)
	, m_Length(length)
	, m_Inverted(false)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentTerminator::RoadSegmentTerminator(const RoadSegmentTerminator& other)
	: RoadSegmentBase(RS_Terminator)
	, m_Length(other.m_Length)
	, m_Inverted(other.m_Inverted)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentTerminator::~RoadSegmentTerminator()
	{
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentTerminator::operator == (const RoadSegmentTerminator& other) const
	{
		if (m_Length != other.m_Length) return false;

		return true;
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex RoadSegmentTerminator::Position() const
	{
		if (m_Inverted)
		{
			// relative position
			double dx = 0.0;
			double dy = m_Length;
			double angle = m_Orientation + PI;

			// absolute position
			Shapes::DVertex pos;
			pos.x = m_Position.x + dx * cos(angle) - dy * sin(angle);
			pos.y = m_Position.y + dx * sin(angle) + dy * cos(angle);

			return pos;
		}
		else
		{
			return m_Position;
		}
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentTerminator::Position(const Shapes::DVertex& position)
	{
		if (m_Inverted)
		{
			// relative position
			double dx = 0.0;
			double dy = m_Length;

			// absolute position
			m_Position.x = position.x + dx * cos(m_Orientation) - dy * sin(m_Orientation);
			m_Position.y = position.y + dx * sin(m_Orientation) + dy * cos(m_Orientation);
		}
		else
		{
			m_Position = position;
		}
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentTerminator::Length() const
	{
		return m_Length;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentTerminator::Length(double length)
	{
		m_Length = length;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentTerminator::Inverted() const
	{
		return m_Inverted;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentTerminator::Inverted(bool inverted)
	{
		m_Inverted = inverted;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentTerminator::Equals(const RoadSegmentBase& other) const
	{
		if (other.Type() != m_Type) return false;

		const RoadSegmentTerminator* ptrOther = reinterpret_cast<const RoadSegmentTerminator*>(&other);

		if (m_Length   != ptrOther->m_Length)   return false;
		if (m_Inverted != ptrOther->m_Inverted) return false;

		return true;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentTerminator::IsMultiple(const RoadSegmentBase& other, unsigned int count) const
	{
		return false;
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex RoadSegmentTerminator::NextPosition() const
	{
		if (m_Inverted)
		{
			return m_Position;
		}
		else
		{
			// relative position
			double dx = 0.0;
			double dy = m_Length;

			// absolute position
			double x = m_Position.x + dx * cos(m_Orientation) - dy * sin(m_Orientation);
			double y = m_Position.y + dx * sin(m_Orientation) + dy * cos(m_Orientation);

			return Shapes::DVertex(x, y);
		}
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentTerminator::NextOrientation() const
	{
		return m_Orientation;
	}

	// ---------------------------------------------------------------------- //

	vector<Shapes::DVertex> RoadSegmentTerminator::ShapeVertices() const
	{
		vector<Shapes::DVertex> shapeVertices;
		shapeVertices.push_back(NextPosition());
		return shapeVertices;
	}

	// ---------------------------------------------------------------------- //

	vector<Shapes::DVertex> RoadSegmentTerminator::Contour(double width) const
	{
		vector<Shapes::DVertex> contour;
		double halfWidth = 0.5 * width;

		// local system
		contour.push_back(Shapes::DVertex(-halfWidth, 0.0));
		contour.push_back(Shapes::DVertex(halfWidth, 0.0));
		contour.push_back(Shapes::DVertex(halfWidth, m_Length));
		contour.push_back(Shapes::DVertex(-halfWidth, m_Length));

		double corrOrientation;

		if (!m_Inverted)
		{
			corrOrientation = m_Orientation;
		}
		else
		{
			corrOrientation = m_Orientation + PI;
		}

		// world system
		for (size_t i = 0, cnt = contour.size(); i < cnt; ++i)
		{
			double dx = contour[i].x;
			double dy = contour[i].y;
			contour[i].x = m_Position.x + dx * cos(corrOrientation) - dy * sin(corrOrientation);
			contour[i].y = m_Position.y + dx * sin(corrOrientation) + dy * cos(corrOrientation);
		}

		return contour;
	}

	// ---------------------------------------------------------------------- //

	string RoadSegmentTerminator::ModelName(const string& suffix) const
	{
		double lfloor = floor(m_Length);
		double lceil  = ceil(m_Length);
		int val = (lceil - m_Length) < (m_Length - lfloor) ? static_cast<int>(lceil) : static_cast<int>(lfloor); 
		char bufLen[50];
		_itoa_s(val, bufLen, 50, 10);
		string length = bufLen;
		return (suffix + length + "konec");
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex RoadSegmentTerminator::ModelPosition(double width) const
	{
		// local system
		Shapes::DVertex modelPosition(0.0, 0.5 * m_Length);

		double corrOrientation;

		if (!m_Inverted)
		{
			corrOrientation = m_Orientation;
		}
		else
		{
			corrOrientation = m_Orientation + PI;
		}

		// world system
		double dx = modelPosition.x;
		double dy = modelPosition.y;
		modelPosition.x = m_Position.x + dx * cos(corrOrientation) - dy * sin(corrOrientation);
		modelPosition.y = m_Position.y + dx * sin(corrOrientation) + dy * cos(corrOrientation);

		return modelPosition;
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentTerminator::ModelOrientation() const
	{
		if (!m_Inverted)
		{
			return m_Orientation;
		}
		else
		{
			return (m_Orientation + PI);
		}
	}

	// ---------------------------------------------------------------------- //
	// RoadSegmentsLibrary
	// ---------------------------------------------------------------------- //

	RoadSegmentsLibrary::RoadSegmentsLibrary()
	: m_AcceptUpdate(true)
	, m_TerminatorAlreadyDefined(false)
	{
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentsLibrary::~RoadSegmentsLibrary()
	{
		Clear();
	}

	// ---------------------------------------------------------------------- //

	const RoadSegmentBase* RoadSegmentsLibrary::operator [] (unsigned int index) const
	{
		if (index < m_Segments.size())
		{
			return m_Segments[index];
		}
		else
		{
			return NULL;
		}
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentBase* RoadSegmentsLibrary::operator [] (unsigned int index)
	{
		if (index < m_Segments.size())
		{
			return m_Segments[index];
		}
		else
		{
			return NULL;
		}
	}

	// ---------------------------------------------------------------------- //

	const RoadSegmentBase* RoadSegmentsLibrary::GetSegment(unsigned int index) const
	{
		if (index < m_Segments.size())
		{
			return m_Segments[index];
		}
		else
		{
			return NULL;
		}
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentBase* RoadSegmentsLibrary::GetSegment(unsigned int index)
	{
		if (index < m_Segments.size())
		{
			return m_Segments[index];
		}
		else
		{
			return NULL;
		}
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentsLibrary::LengthStep() const
	{
		return m_LengthStep;
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentsLibrary::AngleStep() const
	{
		return m_AngleStep;
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentsLibrary::RadiusStep() const
	{
		return m_RadiusStep;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentsLibrary::LengthStep(double lengthStep)
	{
		m_LengthStep = lengthStep;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentsLibrary::AngleStep(double angleStep)
	{
		m_AngleStep = angleStep;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentsLibrary::RadiusStep(double radiusStep)
	{
		m_RadiusStep = radiusStep;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentsLibrary::HasTerminator() const
	{
		return m_TerminatorAlreadyDefined;
	}

	// ---------------------------------------------------------------------- //

	double RoadSegmentsLibrary::TerminatorLength() const
	{
		if (!m_TerminatorAlreadyDefined) return 0.0;

		for (size_t i = 0, cntI = m_Segments.size(); i < cntI; ++i)
		{
			const RoadSegmentBase* segment = m_Segments[i];
			if (segment->Type() == RS_Terminator)
			{
				const RoadSegmentTerminator* temp = reinterpret_cast<const RoadSegmentTerminator*>(segment);
				return temp->Length();
			}
		}

		return 0.0;
	}

	// ---------------------------------------------------------------------- //

	const RoadSegmentBase* RoadSegmentsLibrary::Terminator() const
	{
		if (!m_TerminatorAlreadyDefined) return NULL;

		for (size_t i = 0, cntI = m_Segments.size(); i < cntI; ++i)
		{
			const RoadSegmentBase* segment = m_Segments[i];
			if (segment->Type() == RS_Terminator)
			{
				return segment;
			}
		}

		return NULL;
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentBase* RoadSegmentsLibrary::Terminator()
	{
		if (!m_TerminatorAlreadyDefined) return NULL;

		for (size_t i = 0, cntI = m_Segments.size(); i < cntI; ++i)
		{
			RoadSegmentBase* segment = m_Segments[i];
			if (segment->Type() == RS_Terminator)
			{
				return segment;
			}
		}

		return NULL;
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentsLibrary::Add(const RoadSegmentBase& segment)
	{
		if (m_AcceptUpdate)
		{
			RoadSegmentBase* newSegment = 0;

			switch (segment.Type())
			{
			case RS_Straight:
				{
					RoadSegmentStraight temp = *(reinterpret_cast<const RoadSegmentStraight*>(&segment));
					newSegment = new RoadSegmentStraight(temp);
				}
				break;
			case RS_Corner:
				{
					RoadSegmentCorner temp = *(reinterpret_cast<const RoadSegmentCorner*>(&segment));
					newSegment = new RoadSegmentCorner(temp);
				}
				break;
			case RS_Terminator:
				{
					if (!m_TerminatorAlreadyDefined)
					{
						RoadSegmentTerminator temp = *(reinterpret_cast<const RoadSegmentTerminator*>(&segment));
						newSegment = new RoadSegmentTerminator(temp);
						m_TerminatorAlreadyDefined = true;
					}
				}
				break;
			default:
				{
				}
			}

			if (newSegment)
			{
				if (AlreadyInList(*newSegment))
				{
					delete newSegment;
				}
				else
				{
					m_Segments.push_back(newSegment);
				}
			}
		}
	}

	// ---------------------------------------------------------------------- //

	void RoadSegmentsLibrary::Clear()
	{
		if (m_Segments.size() > 0)
		{
			for (size_t i = 0, cnt = m_Segments.size(); i < cnt; ++i)
			{
				delete m_Segments[i];
			}
		}
		m_Segments.clear();
	}

	// ---------------------------------------------------------------------- //

	size_t RoadSegmentsLibrary::SegmentsCount() const
	{
		return m_Segments.size();
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentsLibrary::LoadFromCfgFile(const char* filename)
	{
		Clear();
		ParamFile pFile;
		LSError err = pFile.Parse(filename);
		if (err) 
		{
			LOGF(Error) << "Failed to open config file.";
			return false;
		}

		ParamEntryPtr entry = pFile.FindEntry("RoadModelsLibrary");
		if (entry.IsNull())
		{
			LOGF(Error) << "Cannot find 'RoadModelsLibrary' a class";
			return false;
		}
		if (!entry->IsClass()) 
		{
			LOGF(Error) << "'RoadModelsLibrary' must be a class";
			return false;
		}

		ParamClassPtr roadModels = entry->GetClassInterface();
		int i = 0;
		char buff[20];

	    do
		{
			i++;
			sprintf(buff, "Item%04d", i);
			entry = roadModels->FindEntry(buff);
			if (entry.IsNull()) break;
			if (entry->IsClass())
			{
	            ParamClassPtr item = entry->GetClassInterface();
				entry = item->FindEntry("type");            
				if (entry.IsNull()) 
				{
					LOGF(Error) << "Missing " << buff << "::type field";
					return false;
				}
				RString type = entry->GetValue();

				if (type == "straight")
				{
					double length = 0.0;
					entry = item->FindEntry("length");            
					if (entry.IsNull()) 
					{
						LOGF(Error) << "Missing " << buff << "::length field";
						return false;
					}

					length = *entry;
					if (length <= 0.0)
					{
						LOGF(Error) << "Invalid value for " << buff << "::length";
						return false;
					}

					Add(RoadSegmentStraight(length));
				}
				else if (type == "corner")
				{
					double angle = 0.0;
					entry = item->FindEntry("angle");            
					if (entry.IsNull()) 
					{
						LOGF(Error) << "Missing " << buff << "::angle field";
						return false;
					}

					angle = *entry;
					if (angle <= 0.0)
					{
						LOGF(Error) << "Invalid value for " << buff << "::angle";
						return false;
					}
					angle = angle * PI / 180.0;

					double radius = 0.0;
					entry = item->FindEntry("radius");            
					if (entry.IsNull()) 
					{
						LOGF(Error) << "Missing " << buff << "::radius field";
						return false;
					}

					radius = *entry;
					if (radius <= 0.0)
					{
						LOGF(Error) << "Invalid value for " << buff << "::radius";
						return false;
					}

					Add(RoadSegmentCorner(angle, radius));
				}
				else if (type == "terminator")
				{
					double length = 0.0;
					entry = item->FindEntry("length");            
					if (entry.IsNull()) 
					{
						LOGF(Error) << "Missing " << buff << "::length field";
						return false;
					}

					length = *entry;
					if (length <= 0.0)
					{
						LOGF(Error) << "Invalid value for " << buff << "::length";
						return false;
					}

					Add(RoadSegmentTerminator(length));
				}
				else
				{
					LOGF(Error) << "Unknown value for " << buff << "::type";
					return false;
				}
			}
		}
		while (true);

		entry = roadModels->FindEntry("Parameters");
		if (entry.IsNull()) 
		{
			m_AcceptUpdate = false;
			m_LengthStep = 0.0;
			m_AngleStep = 0.0;
			m_RadiusStep = 0.0;
		}
		else
		{
			if (!entry->IsClass()) 
			{
				LOGF(Error) << "'Parameters' must be a class";
				return false;
			}
			ParamClassPtr xparams = entry->GetClassInterface();

			m_LengthStep = 0.0;
			entry = xparams->FindEntry("lengthStep");            
			if (entry.IsNull()) 
			{
				LOGF(Error) << "Missing parameter lengthStep";
				return false;
			}

			m_LengthStep = *entry;
			if (m_LengthStep <= 0.0)
			{
				LOGF(Error) << "Invalid value for lengthStep";
				return false;
			}

			m_AngleStep = 0.0;
			entry = xparams->FindEntry("angleStep");            
			if (entry.IsNull()) 
			{
				LOGF(Error) << "Missing parameter angleStep";
				return false;
			}

			m_AngleStep = *entry;
			if (m_AngleStep <= 0.0)
			{
				LOGF(Error) << "Invalid value for angleStep";
				return false;
			}

			m_RadiusStep = 0.0;
			entry = xparams->FindEntry("radiusStep");            
			if (entry.IsNull()) 
			{
				LOGF(Error) << "Missing parameter radiusStep";
				return false;
			}

			m_RadiusStep = *entry;
			if (m_RadiusStep <= 0.0)
			{
				LOGF(Error) << "Invalid value for radiusStep";
				return false;
			}
		}

		return true;
	}

	// ---------------------------------------------------------------------- //

	bool RoadSegmentsLibrary::AlreadyInList(const RoadSegmentBase& segment) const
	{
		for (size_t i = 0, cnt = m_Segments.size(); i < cnt; ++i)
		{
			if (m_Segments[i]->Type() == segment.Type())
			{
				switch (segment.Type())
				{
				case RS_Straight:
					{
						RoadSegmentStraight curr = *(reinterpret_cast<const RoadSegmentStraight*>(m_Segments[i]));
						RoadSegmentStraight seg  = *(reinterpret_cast<const RoadSegmentStraight*>(&segment));

						if (curr == seg) return true;
					}
					break;
				case RS_Corner:
					{
						RoadSegmentCorner curr = *(reinterpret_cast<const RoadSegmentCorner*>(m_Segments[i]));
						RoadSegmentCorner seg  = *(reinterpret_cast<const RoadSegmentCorner*>(&segment));

						if (curr == seg) return true;
					}
					break;
				case RS_Terminator:
					{
						RoadSegmentTerminator curr = *(reinterpret_cast<const RoadSegmentTerminator*>(m_Segments[i]));
						RoadSegmentTerminator seg  = *(reinterpret_cast<const RoadSegmentTerminator*>(&segment));

						if (curr == seg) return true;
					}
					break;
				default:
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	// ---------------------------------------------------------------------- //
	// Road
	// ---------------------------------------------------------------------- //

	Road::Road()
	: m_Library(0)
	, m_Width(0.0)
	, m_Type("")
	, m_InUse(true)
	, m_Built(false)
	{
		ResetSegments();
	}

	// ---------------------------------------------------------------------- //

	Road::Road(RoadSegmentsLibrary& library, double width, const string& type)
	: m_Library(&library)
	, m_Width(width)
	, m_Type(type)
	, m_InUse(true)
	, m_Built(false)
	{
		ResetSegments();
	}

	// ---------------------------------------------------------------------- //

	Road::~Road()
	{
		ResetSegments();
	}

	// ---------------------------------------------------------------------- //

	const RoadSegmentsLibrary* Road::Library() const
	{
		return m_Library;
	}

	// ---------------------------------------------------------------------- //

	const vector<RoadSegmentBase*>& Road::Segments() const
	{
		return m_Segments;
	}

	// ---------------------------------------------------------------------- //

	double Road::Width() const
	{
		return m_Width;
	}

	// ---------------------------------------------------------------------- //

	const string& Road::Type() const
	{
		return m_Type;
	}

	// ---------------------------------------------------------------------- //

	const Shapes::DVertex& Road::StartPosition() const
	{
		return m_StartPosition;
	}

	// ---------------------------------------------------------------------- //

	void Road::StartPosition(const Shapes::DVertex& startPosition)
	{
		m_StartPosition = startPosition;
	}

	// ---------------------------------------------------------------------- //

	double Road::StartOrientation() const
	{
		return m_StartOrientation;
	}

	// ---------------------------------------------------------------------- //

	void Road::StartOrientation(double startOrientation)
	{
		m_StartOrientation = startOrientation;
	}

	// ---------------------------------------------------------------------- //

	bool Road::InUse() const
	{
		return m_InUse;
	}

	// ---------------------------------------------------------------------- //

	void Road::InUse(bool inUse)
	{
		m_InUse = inUse;
	}

	// ---------------------------------------------------------------------- //

	bool Road::Built() const
	{
		return m_Built;
	}

	// ---------------------------------------------------------------------- //

	void Road::Built(bool built)
	{
		m_Built = built;
	}

	// ---------------------------------------------------------------------- //

	bool Road::Equal(const Road& other) const
	{
		if (m_Width != other.m_Width) return false;
		if (m_Type  != other.m_Type)  return false;

		return true;
	}

	// ---------------------------------------------------------------------- //

	void Road::Build()
	{
		// clear road if already built
		ResetSegments();

		double shapeLength = ShapeLength();
		double termLength  = m_Library->TerminatorLength();
		double limitLength = shapeLength - TERMLENGTHMULTIPLIER * termLength;

		// we try to start with a terminator
		if (m_Library->HasTerminator())
		{
			RoadSegmentBase* curSegment = m_Library->Terminator();
			if (curSegment)
			{
				RoadSegmentTerminator* newSegment = new RoadSegmentTerminator(*(reinterpret_cast<const RoadSegmentTerminator*>(curSegment)));
				newSegment->Inverted(true);
				newSegment->Orientation(m_CurrInsOrientation);						
				newSegment->Position(m_CurrInsPosition);
				AddSegment(newSegment);
			}
		}

		RoadSegmentBase* curSeg = 0;
		while (m_CurrShapeCoordinate < limitLength)
		{
//			curSeg = ClosestSegmentOnlyDist();
			curSeg = ClosestSegmentDistAndDir();

				double old = m_CurrShapeCoordinate;

			if (curSeg) AddSegment(curSeg);

				if (old >= m_CurrShapeCoordinate)
				{
					break;
				}

				LOGF(Error) << m_CurrShapeCoordinate;
		}

		if (m_Segments.size() > 0)
		{
			unsigned int removed = 0;
			do
			{
				removed = CollectSimilar();
			}
			while (removed > 0);
		}

		// we try to end with a terminator
		if (m_Library->HasTerminator())
		{
			RoadSegmentBase* curSegment = m_Library->Terminator();
			if (curSegment)
			{
				RoadSegmentBase* newSegment = new RoadSegmentTerminator(*(reinterpret_cast<const RoadSegmentTerminator*>(curSegment)));
				newSegment->Orientation(m_CurrInsOrientation);						
				newSegment->Position(m_CurrInsPosition);
				AddSegment(newSegment);
			}
		}

		RecalculateSegmentShape();
		m_Built = true;
	}

	// ---------------------------------------------------------------------- //

	void Road::V3Correction()
	{
		if (m_Segments.size() > 0)
		{
			RoadSegmentBase* firstSegment = m_Segments[0];
			if (firstSegment->Type() == RS_Terminator)
			{
				RoadSegmentTerminator* segTerm = reinterpret_cast<RoadSegmentTerminator*>(firstSegment);
				double len = segTerm->Length();
				double dx = len * sin(m_StartOrientation);
				double dy = len * cos(m_StartOrientation);
				Shapes::DVector corr(dx, dy);
				m_StartPosition += corr;
			}
		}
	}

	// ---------------------------------------------------------------------- //

	size_t Road::ShapeVerticesCount() const
	{
		return m_Shape.size();
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex& Road::ShapeStartVertex()
	{
		return m_Shape[0];
	}

	// ---------------------------------------------------------------------- //

	const Shapes::DVertex& Road::ShapeStartVertex() const
	{
		return m_Shape[0];
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex& Road::ShapeEndVertex()
	{
		return m_Shape[m_Shape.size() - 1];
	}

	// ---------------------------------------------------------------------- //

	const Shapes::DVertex& Road::ShapeEndVertex() const
	{
		return m_Shape[m_Shape.size() - 1];
	}

	// ---------------------------------------------------------------------- //

	void Road::AddShapeVertex(const Shapes::DVertex& vertex)
	{
		m_Shape.push_back(vertex);
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex& Road::ShapeVertex(unsigned int index)
	{
		return m_Shape[index];
	}

	// -------------------------------------------------------------------------- //

	const Shapes::DVertex& Road::ShapeVertex(unsigned int index) const
	{
		return m_Shape[index];
	}

	// -------------------------------------------------------------------------- //

	double Road::ShapeLength() const
	{
		double length = 0.0;

		size_t cnt = m_Shape.size();

		if (cnt < 2) return length;

		for (size_t i = 0; i < cnt - 1; i++)
		{
			const Shapes::DVertex& p0 = m_Shape[i];
			const Shapes::DVertex& p1 = m_Shape[i + 1];

			length += p0.DistanceXY(p1);
		}
		return length;
	}

	// -------------------------------------------------------------------------- //

	void Road::ReverseVertices()
	{
		vector<Shapes::DVertex> tempShape;

		for (int i = m_Shape.size() - 1; i >= 0; --i)
		{
			tempShape.push_back(m_Shape[i]);
		}

		m_Shape = tempShape;
	}

	// -------------------------------------------------------------------------- //

	void Road::MergeShapeWith(const Road& other, RoadEndTypes thisEndType, RoadEndTypes otherEndType)
	{
		vector<Shapes::DVertex> tempShape;
		bool thisReversed = false;
		bool otherReversed = false;

		if (thisEndType == etStarting)
		{
			thisReversed = true;
		}
		if (otherEndType == etEnding)
		{
			otherReversed = true;
		}

		if (thisReversed)
		{
			for (int i = m_Shape.size() - 1; i >= 0; --i)
			{
				tempShape.push_back(m_Shape[i]);
			}
			m_StartPosition    = m_Shape[m_Shape.size() - 1];
			Shapes::DVector v01(m_Shape[m_Shape.size() - 2], m_Shape[m_Shape.size() - 1]);
			m_StartOrientation = v01.DirectionYX();
		}
		else
		{
			for (size_t i = 0, cnt = m_Shape.size(); i < cnt; ++i)
			{
				tempShape.push_back(m_Shape[i]);
			}
		}

		if (otherReversed)
		{
			for (int i = other.m_Shape.size() - 2; i >= 0; --i)
			{
				tempShape.push_back(other.m_Shape[i]);
			}
		}
		else
		{
			for (size_t i = 1, cnt = other.m_Shape.size(); i < cnt; ++i)
			{
				tempShape.push_back(other.m_Shape[i]);
			}
		}

		m_Shape = tempShape;
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex Road::ClosestPointOnShapeTo(const Shapes::DVertex& point) const
	{
		double minDist = DBL_MAX;
		Shapes::DVertex nearestPt;

		Shapes::DVertex p0 = m_Shape[0];

		double dx = point.x - p0.x;
		double dy = point.y - p0.y;
		double d = dx * dx + dy * dy;
		if (d < minDist)
		{
			minDist   = d;
			nearestPt = p0;
		}

		for (size_t j = 1, cnt = m_Shape.size(); j < cnt; ++j)
		{
			const Shapes::DVertex& p1 = m_Shape[j];
			double dx, dy, d;
			Shapes::DVertex resV;

			if (PrumetNaUsecku(point, p0, p1, resV, d))
			{
				if (d < minDist)
				{
					minDist = d;
					nearestPt = resV;
				}
			}
			else
			{
				dx = point.x - p1.x;
				dy = point.y - p1.y;
				d = dx * dx + dy * dy;
				if (d < minDist)
				{
					minDist   = d;
					nearestPt = p1;
				}
			}
			p0 = p1;
		}
		return nearestPt;      
	}

	// -------------------------------------------------------------------------- //

	void Road::ReplaceShape(const vector<Shapes::DVertex>& newshape)
	{
		m_Shape = newshape;
	}

	// -------------------------------------------------------------------------- //

	size_t Road::SegmentShapeVerticesCount() const
	{
		return m_SegmentShape.size();
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex& Road::SegmentShapeVertex(unsigned int index)
	{
		return m_SegmentShape[index];
	}

	// -------------------------------------------------------------------------- //

	const Shapes::DVertex& Road::SegmentShapeVertex(unsigned int index) const
	{
		return m_SegmentShape[index];
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex Road::ClosestPointOnSegmentShapeTo(const Shapes::DVertex& point) const
	{
		double minDist = DBL_MAX;
		Shapes::DVertex nearestPt;

		Shapes::DVertex p0 = m_SegmentShape[0];

		double dx = point.x - p0.x;
		double dy = point.y - p0.y;
		double d = dx * dx + dy * dy;
		if (d < minDist)
		{
			minDist   = d;
			nearestPt = p0;
		}

		for (size_t j = 1, cnt = m_SegmentShape.size(); j < cnt; ++j)
		{
			const Shapes::DVertex& p1 = m_SegmentShape[j];
			double dx, dy, d;
			Shapes::DVertex resV;

			if (PrumetNaUsecku(point, p0, p1, resV, d))
			{
				if (d < minDist)
				{
					minDist = d;
					nearestPt = resV;
				}
			}
			else
			{
				dx = point.x - p1.x;
				dy = point.y - p1.y;
				d = dx * dx + dy * dy;
				if (d < minDist)
				{
					minDist   = d;
					nearestPt = p1;
				}
			}
			p0 = p1;
		}
		return nearestPt;      
	}

	// -------------------------------------------------------------------------- //

	void Road::ResetSegments()
	{
		if (m_Segments.size() > 0)
		{
			for (size_t i = 0, cnt = m_Segments.size(); i < cnt; ++i)
			{
				delete m_Segments[i];
			}
		}
		m_Segments.clear();

		if (m_Shape.size() > 1)
		{
			const Shapes::DVertex& p0 = m_Shape[0];
			const Shapes::DVertex& p1 = m_Shape[1];
			m_CurrInsPosition = Shapes::DVertex(p0.x, p0.y);
			Shapes::DVector v01(p1, p0);
			m_CurrInsOrientation = v01.DirectionXY() - HALF_PI;

			if (m_CurrInsOrientation < 0.0)
			{
				m_CurrInsOrientation += TWO_PI;
			}
		}
		else
		{
			m_CurrInsPosition = Shapes::DVertex(0.0, 0.0);
			m_CurrInsOrientation = 0.0;
		}

		m_CurrShapeCoordinate = 0.0;
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentBase* Road::ClosestSegmentOnlyDist()
	{
		RoadSegmentBase* curSegment = 0;
		RoadSegmentBase* newSegment = 0;

		double minDist  = DBL_MAX;
		//--------------------------------
		double minRatio = DBL_MAX;
		//--------------------------------

		for (size_t i = 0, cnt = m_Library->SegmentsCount(); i < cnt; ++i)
		{
			const RoadSegmentBase* libSegment = (*m_Library)[i];

			switch (libSegment->Type())
			{
			case RS_Straight:
				{
					if (curSegment) delete curSegment;
					curSegment = new RoadSegmentStraight(*(reinterpret_cast<const RoadSegmentStraight*>(libSegment)));
				}
				break;
			case RS_Corner:
				{
					if (curSegment) delete curSegment;
					curSegment = new RoadSegmentCorner(*(reinterpret_cast<const RoadSegmentCorner*>(libSegment)));
				}
				break;
			};

			// this is for straight and non inverted corners (right)
			curSegment->Orientation(m_CurrInsOrientation);
			curSegment->Position(m_CurrInsPosition);

			Shapes::DVertex curSegmentEnd  = curSegment->NextPosition();
			Shapes::DVertex closestOnShape = ClosestPointOnShapeTo(curSegmentEnd);
			
			double dist = curSegmentEnd.DistanceXY(closestOnShape);

			//--------------------------------
			double longDist = closestOnShape.DistanceXY(m_CurrInsPosition);
			double ratio;
			if (longDist == 0.0)
			{
				ratio = DBL_MAX;
			}
			else
			{
				ratio = dist / longDist;
			}

			if (ratio < minRatio)
			//--------------------------------
//			if (dist < minDist)
			{
				switch (curSegment->Type())
				{
				case RS_Straight:
					{
						if (newSegment) delete newSegment;
						newSegment = new RoadSegmentStraight(*(reinterpret_cast<const RoadSegmentStraight*>(curSegment)));
						newSegment->Orientation(m_CurrInsOrientation);						
						newSegment->Position(m_CurrInsPosition);
					}
					break;
				case RS_Corner:
					{
						if (newSegment) delete newSegment;
						newSegment = new RoadSegmentCorner(*(reinterpret_cast<const RoadSegmentCorner*>(curSegment)));
						newSegment->Orientation(m_CurrInsOrientation);						
						newSegment->Position(m_CurrInsPosition);
					}
					break;
				};

				//--------------------------------
				minRatio = ratio;
				//--------------------------------
//				minDist  = dist;
			}

			// this is for inverted corners (left)
			if (curSegment->Type() == RS_Corner)
			{
				// obtain the inverted corner by inverting the current one
				(reinterpret_cast<RoadSegmentCorner*>(curSegment))->Inverted(true);
				curSegment->Orientation(m_CurrInsOrientation);
				curSegment->Position(m_CurrInsPosition);

				curSegmentEnd  = curSegment->NextPosition();
				closestOnShape = ClosestPointOnShapeTo(curSegmentEnd);

				dist = curSegmentEnd.DistanceXY(closestOnShape);

				//--------------------------------
				double longDist = closestOnShape.DistanceXY(m_CurrInsPosition);
				double ratio;
				if (longDist == 0.0)
				{
					ratio = DBL_MAX;
				}
				else
				{
					ratio = dist / longDist;
				}

				if (ratio < minRatio)
				//--------------------------------
//				if (dist < minDist)
				{
					switch (curSegment->Type())
					{
					case RS_Straight:
						{
							if (newSegment) delete newSegment;
							newSegment = new RoadSegmentStraight(*(reinterpret_cast<const RoadSegmentStraight*>(curSegment)));
							newSegment->Orientation(m_CurrInsOrientation);						
							newSegment->Position(m_CurrInsPosition);
						}
						break;
					case RS_Corner:
						{
							if (newSegment) delete newSegment;
							newSegment = new RoadSegmentCorner(*(reinterpret_cast<const RoadSegmentCorner*>(curSegment)));
							newSegment->Orientation(m_CurrInsOrientation);						
							newSegment->Position(m_CurrInsPosition);
						}
						break;
					};

					//--------------------------------
					minRatio = ratio;
					//--------------------------------

//					minDist  = dist;
				}
			}
		}

		if (curSegment) delete curSegment;
		return newSegment;
	}

	// ---------------------------------------------------------------------- //

	RoadSegmentBase* Road::ClosestSegmentDistAndDir()
	{
		RoadSegmentBase* curSegment = 0;
		RoadSegmentBase* newSegment = 0;

		double minDist    = DBL_MAX;
		double minDirDiff = DBL_MAX;

		double shapeLength = ShapeLength();
		double termLength  = m_Library->TerminatorLength();
		double limitLength = shapeLength - TERMLENGTHMULTIPLIER * termLength;

		for (size_t i = 0, cnt = m_Library->SegmentsCount(); i < cnt; ++i)
		{
			const RoadSegmentBase* libSegment = (*m_Library)[i];

			switch (libSegment->Type())
			{
			case RS_Straight:
				{
					if (curSegment) delete curSegment;
					curSegment = new RoadSegmentStraight(*(reinterpret_cast<const RoadSegmentStraight*>(libSegment)));
				}
				break;
			case RS_Corner:
				{
					if (curSegment) delete curSegment;
					curSegment = new RoadSegmentCorner(*(reinterpret_cast<const RoadSegmentCorner*>(libSegment)));
				}
				break;
			};

			// this is for straight and non inverted corners (right)
			curSegment->Orientation(m_CurrInsOrientation);
			curSegment->Position(m_CurrInsPosition);

			Shapes::DVertex curSegmentEnd  = curSegment->NextPosition();
			Shapes::DVertex closestOnShape = ClosestPointOnShapeTo(curSegmentEnd);
			
			double dist     = curSegmentEnd.DistanceXY(closestOnShape);
			double segDir   = curSegment->NextOrientation();
			double shapeDir = OrientationShapeEdgeOwnerOf(closestOnShape) - HALF_PI;
			if (shapeDir < 0.0) shapeDir += TWO_PI;

			Shapes::DVector segVec(cos(segDir), sin(segDir));
			Shapes::DVector shapeVec(cos(shapeDir), sin(shapeDir));
			double cosBetween = segVec.DotXY(shapeVec);

			double dirDiff  = 1.0 - cosBetween;

			double oldShapeCoord = ShapeCoordinate(m_CurrInsPosition);
			double segShapeCoord = ShapeCoordinate(closestOnShape);

			if (oldShapeCoord < segShapeCoord)
			{
				if (segShapeCoord < limitLength)
				{
					if (dist < minDist)
					{
						if (dirDiff < minDirDiff)
						{
							switch (curSegment->Type())
							{
							case RS_Straight:
								{
									if (newSegment) delete newSegment;
									newSegment = new RoadSegmentStraight(*(reinterpret_cast<const RoadSegmentStraight*>(curSegment)));
									newSegment->Orientation(m_CurrInsOrientation);						
									newSegment->Position(m_CurrInsPosition);
								}
								break;
							case RS_Corner:
								{
									if (newSegment) delete newSegment;
									newSegment = new RoadSegmentCorner(*(reinterpret_cast<const RoadSegmentCorner*>(curSegment)));
									newSegment->Orientation(m_CurrInsOrientation);						
									newSegment->Position(m_CurrInsPosition);
								}
								break;
							};
							minDirDiff = dirDiff;
							minDist = dist;
						}
					}
				}
			}

			// this is for inverted corners (left)
			if (curSegment->Type() == RS_Corner)
			{
				// obtain the inverted corner by inverting the current one
				(reinterpret_cast<RoadSegmentCorner*>(curSegment))->Inverted(true);
				curSegment->Orientation(m_CurrInsOrientation);
				curSegment->Position(m_CurrInsPosition);

				curSegmentEnd  = curSegment->NextPosition();
				closestOnShape = ClosestPointOnShapeTo(curSegmentEnd);

				dist = curSegmentEnd.DistanceXY(closestOnShape);
				segDir   = curSegment->NextOrientation();
				shapeDir = OrientationShapeEdgeOwnerOf(closestOnShape) - HALF_PI;
				if (shapeDir < 0.0) shapeDir += TWO_PI;

				Shapes::DVector segVec(cos(segDir), sin(segDir));
				Shapes::DVector shapeVec(cos(shapeDir), sin(shapeDir));
				cosBetween = segVec.DotXY(shapeVec);

				dirDiff  = 1.0 - cosBetween;

				oldShapeCoord = ShapeCoordinate(m_CurrInsPosition);
				segShapeCoord = ShapeCoordinate(closestOnShape);

				if (oldShapeCoord < segShapeCoord)
				{
					if (segShapeCoord < limitLength)
					{
						if (dist < minDist)
						{
							if (dirDiff < minDirDiff)
							{
								switch (curSegment->Type())
								{
								case RS_Straight:
									{
										if (newSegment) delete newSegment;
										newSegment = new RoadSegmentStraight(*(reinterpret_cast<const RoadSegmentStraight*>(curSegment)));
										newSegment->Orientation(m_CurrInsOrientation);						
										newSegment->Position(m_CurrInsPosition);
									}
									break;
								case RS_Corner:
									{
										if (newSegment) delete newSegment;
										newSegment = new RoadSegmentCorner(*(reinterpret_cast<const RoadSegmentCorner*>(curSegment)));
										newSegment->Orientation(m_CurrInsOrientation);						
										newSegment->Position(m_CurrInsPosition);
									}
									break;
								};
								minDirDiff = dirDiff;
								minDist = dist;
							}
						}
					}
				}
			}
		}

		if (curSegment) delete curSegment;
		return newSegment;
	}

	// ---------------------------------------------------------------------- //

	double Road::ShapeCoordinate(const Shapes::DVertex& vertex) const
	{
		Shapes::DVertex posOnShape = ClosestPointOnShapeTo(vertex);

		double s = 0.0;
		bool found = false;
		double dp0; 
		double dp1;
		double d01;

		for (unsigned int i = 0, cnt = m_Shape.size(); i < (cnt - 1); ++i)
		{
			const Shapes::DVertex& v0 = m_Shape[i];
			const Shapes::DVertex& v1 = m_Shape[i + 1];

			dp0 = posOnShape.DistanceXY(v0); 
			dp1 = posOnShape.DistanceXY(v1);
			d01 = v0.DistanceXY(v1);

			if (fabs(dp0 + dp1 - d01) < 0.0001)
			{
				s += dp0;
				found = true;
				break;
			}
			else
			{
				s += d01;
			}
		}

		// we are past the end of the polyline
		if (!found) s += dp1;

		return s;
	}

	// ---------------------------------------------------------------------- //

	void Road::RecalculateCurrShapeCoordinate()
	{
		m_CurrShapeCoordinate = ShapeCoordinate(m_CurrInsPosition);
	}

	// ---------------------------------------------------------------------- //

	void Road::AddSegment(RoadSegmentBase* segment)
	{
		m_Segments.push_back(segment);
		m_CurrInsPosition    = segment->NextPosition();
		m_CurrInsOrientation = segment->NextOrientation();
		RecalculateCurrShapeCoordinate();
	}

	// ---------------------------------------------------------------------- //

	double Road::OrientationShapeEdgeOwnerOf(const Shapes::DVertex& vertex)
	{
		double dp0; 
		double dp1;
		double d01;

		for (unsigned int i = 0, cnt = m_Shape.size(); i < (cnt - 1); ++i)
		{
			const Shapes::DVertex& v0 = m_Shape[i];
			const Shapes::DVertex& v1 = m_Shape[i + 1];

			dp0 = vertex.DistanceXY(v0); 
			dp1 = vertex.DistanceXY(v1);
			d01 = v0.DistanceXY(v1);

			if (fabs(dp0 + dp1 - d01) < 0.0001)
			{
				Shapes::DVector v(v1, v0);
				return v.DirectionXY();
			}
		}

		// we are past the end of the polyline
		// we use last edge's orientation
		unsigned int cnt = m_Shape.size();
		const Shapes::DVertex& v0 = m_Shape[cnt - 2];
		const Shapes::DVertex& v1 = m_Shape[cnt - 1];
		Shapes::DVector v(v1, v0);
		return v.DirectionXY();
	}

	// ---------------------------------------------------------------------- //

	unsigned int Road::CollectSimilar()
	{
		if (m_Segments.size() < 2) return 0;

		RoadSegmentBase* curSelSegment = m_Segments[0];
		switch (curSelSegment->Type())
		{
		case RS_Straight:
			{
				curSelSegment = reinterpret_cast<RoadSegmentStraight*>(curSelSegment);
			}
			break;
		case RS_Corner:
			{
				curSelSegment = reinterpret_cast<RoadSegmentCorner*>(curSelSegment);
			}
			break;
		};

		int curSelCount = 1;
		size_t curSelIndex = 0;
		unsigned int removed = 0;

		for (size_t i = 1; i < m_Segments.size() - 1; ++i)
		{
			RoadSegmentBase* curSegment = m_Segments[i];

			if (curSegment->Type() == curSelSegment->Type())
			{
				switch (curSegment->Type())
				{
				case RS_Straight:
					{
						curSegment = reinterpret_cast<RoadSegmentStraight*>(curSegment);
					}
					break;
				case RS_Corner:
					{
						curSegment = reinterpret_cast<RoadSegmentCorner*>(curSegment);
					}
					break;
				};

				if (curSegment->Equals(*curSelSegment))
				{
					curSelCount++;
					unsigned int eqRemoved = ReplaceWithBiggerSegment(curSelIndex, curSelSegment, curSelCount);
					removed += eqRemoved;
					if (eqRemoved > 0)
					{
						curSelCount = 1;
					}
				}
				else
				{
					removed += ReplaceWithBiggerSegment(curSelIndex, curSelSegment, curSelCount);
					curSelSegment = curSegment;
					curSelCount = 1;
					curSelIndex = i;
				}
			}
			else
			{
				removed += ReplaceWithBiggerSegment(curSelIndex, curSelSegment, curSelCount);
				curSelSegment = curSegment;
				curSelCount = 1;
				curSelIndex = i;
			}
		}

		return removed;
	}

	// ---------------------------------------------------------------------- //

	unsigned int Road::ReplaceWithBiggerSegment(size_t index, const RoadSegmentBase* segment, int count)
	{
		if (m_Library->SegmentsCount() < 2) return 0;

		for (size_t i = 0, cnt = m_Library->SegmentsCount(); i < cnt; ++i)
		{
			const RoadSegmentBase* libSegment = (*m_Library)[i];

			if (libSegment->Type() == segment->Type())
			{
				double orientation;
				Shapes::DVertex position;
				bool inverted = false;

				if (libSegment->IsMultiple(*segment, count))
				{
					switch (segment->Type())
					{
					case RS_Straight:
						{
							orientation = segment->Orientation();						
							position    = segment->Position();
						}
						break;
					case RS_Corner:
						{
							orientation = segment->Orientation();						
							position    = segment->Position();
							const RoadSegmentCorner* temp = reinterpret_cast<const RoadSegmentCorner*>(segment);
							if (temp->Inverted())
							{
								if (index > 0)
								{
									position = m_Segments[index - 1]->NextPosition();
								}
								else
								{
									position = m_Shape[0];
								}
								inverted = true;
							}
						}
						break;
					};

					for (int j = 0; j < count; ++j)
					{
						delete m_Segments[index + j];
					}
					m_Segments.erase(m_Segments.begin() + index + 1, m_Segments.begin() + index + count);

					switch (libSegment->Type())
					{
					case RS_Straight:
						{
							m_Segments[index] = new RoadSegmentStraight(*(reinterpret_cast<const RoadSegmentStraight*>(libSegment)));
						}
						break;
					case RS_Corner:
						{
							m_Segments[index] = new RoadSegmentCorner(*(reinterpret_cast<const RoadSegmentCorner*>(libSegment)));
						}
						break;
					};

					if (inverted)
					{
						RoadSegmentCorner* temp = reinterpret_cast<RoadSegmentCorner*>(m_Segments[index]);
						temp->Inverted(true);
					}

					m_Segments[index]->Orientation(orientation);						
					m_Segments[index]->Position(position);

					segment = m_Segments[index];

					return (count - 1);
				}
			}
		}
		return 0;
	}

	// ---------------------------------------------------------------------- //

	bool Road::PrumetNaUsecku(const Shapes::DVertex& pt, const Shapes::DVertex& a, const Shapes::DVertex& b, Shapes::DVertex& res, double& d2) const
	{
		// the vector on the segment
		Shapes::DVector segment(b.x - a.x, b.y - a.y, 0.0);
		double segLength = segment.SizeXY();
		if (segLength > 0.0)
		// goes on only if the segment is not a null segment
		{
			// the unitary vector along the segment
			Shapes::DVector unitSegment(segment);
			unitSegment.NormalizeXYInPlace();
			// the vector from the first end of the segment to the extern point 
			Shapes::DVector v(pt.x - a.x, pt.y - a.y, 0.0);
			// the length of the component of the vector v along the segment
			double proj = v.DotXY(unitSegment);
			// if 0 <= proj <= segLength the projection of the node on
			// the segment is really ON the segment
			// we go on with calculation only in this case
			if (0.0 <= proj && proj <= segLength)
			{
				double angle = unitSegment.DirectionXY();
				// the projection on the segment
				res.x = a.x + proj * cos(angle);
				res.y = a.y + proj * sin(angle);
				// squared distance between extern point and its projection on the segment
				d2 = pt.DistanceXY2(res);
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	// ---------------------------------------------------------------------- //

	void Road::RecalculateSegmentShape()
	{
		m_SegmentShape.clear();

		if (m_Segments.size() == 0) return;

		m_SegmentShape.push_back(m_Segments[0]->Position());
		for (size_t i = 0, cntI = m_Segments.size(); i < cntI; ++i)
		{
			vector<Shapes::DVertex> shapeVertices = m_Segments[i]->ShapeVertices();
			for (size_t j = 0, cntJ = shapeVertices.size(); j < cntJ; ++j)
			{
				m_SegmentShape.push_back(shapeVertices[j]);
			}
		}
	}

	// ---------------------------------------------------------------------- //
	// RoadEx
	// ---------------------------------------------------------------------- //

	RoadEx::RoadEx(Road* road, RoadEndTypes roadEndType)
	: m_Road(road)
	, m_RoadEndType(roadEndType)
	{
	}

	// ---------------------------------------------------------------------- //

	Road* RoadEx::GetRoad()
	{
		return m_Road;
	}

	// ---------------------------------------------------------------------- //

	const Road* RoadEx::GetRoad() const
	{
		return m_Road;
	}

	// ---------------------------------------------------------------------- //

	RoadEndTypes RoadEx::GetRoadEndType() const
	{
		return m_RoadEndType;
	}

	// ---------------------------------------------------------------------- //

	void RoadEx::SetRoad(Road* road)
	{
		m_Road = road;
	}

	// ---------------------------------------------------------------------- //

	void RoadEx::SetRoadEndType(RoadEndTypes roadEndType)
	{
		m_RoadEndType = roadEndType;
	}

	// ---------------------------------------------------------------------- //

	void RoadEx::SwitchEndType()
	{
		if (m_RoadEndType == etStarting)
		{
			m_RoadEndType = etEnding;
		}
		else if (m_RoadEndType == etEnding)
		{
			m_RoadEndType = etStarting;
		}
	}

	// ---------------------------------------------------------------------- //
	// CandidatesItem
	// ---------------------------------------------------------------------- //

	CandidatesItem::CandidatesItem(double width, string type)
	: m_Width(width)
	, m_Type(type)
	{
	}

	// ---------------------------------------------------------------------- //

	bool CandidatesItem::operator == (const CandidatesItem& other) const
	{
		if (m_Width != other.m_Width) return false;
		if (m_Type  != other.m_Type)  return false;

		return true;
	}

	// ---------------------------------------------------------------------- //

	double CandidatesItem::Width() const
	{
		return m_Width;
	}

	// ---------------------------------------------------------------------- //

	void CandidatesItem::AddIndex(size_t index)
	{
		m_Indices.push_back(index);
	}

	// ---------------------------------------------------------------------- //

	size_t CandidatesItem::Index(size_t index) const
	{
		return m_Indices[index];
	}

	// ---------------------------------------------------------------------- //

	size_t CandidatesItem::IndicesCount() const
	{
		return m_Indices.size();
	}

	// ---------------------------------------------------------------------- //
	// ---------------------------------------------------------------------- //

	bool CandidatesItem_Smaller(const CandidatesItem& c1, const CandidatesItem& c2)
	{
		if (c1.IndicesCount() < c2.IndicesCount()) return true;
		return false;
	}

	// ---------------------------------------------------------------------- //

	bool CandidatesItem_Greater(const CandidatesItem& c1, const CandidatesItem& c2)
	{
		if (c1.IndicesCount() > c2.IndicesCount()) return true;
		return false;
	}

	// ---------------------------------------------------------------------- //
	// Node
	// ---------------------------------------------------------------------- //

	Node::Node(const Shapes::DVertex& position)
	: m_Position(position)
	, m_Transit(false)
	{
	}

	// ---------------------------------------------------------------------- //

	Node::~Node()
	{
	}

	// ---------------------------------------------------------------------- //

	void Node::AddRoad(const RoadEx& road)
	{
		m_RoadsList.push_back(road);
	}

	// ---------------------------------------------------------------------- //

	Shapes::DVertex& Node::Position()
	{
		return m_Position;
	}

	// ---------------------------------------------------------------------- //

	const Shapes::DVertex& Node::Position() const
	{
		return m_Position;
	}

	// ---------------------------------------------------------------------- //

	RoadEx& Node::GetRoad(unsigned int index)
	{
		assert(index < m_RoadsList.size());
		return m_RoadsList[index];
	}

	// ---------------------------------------------------------------------- //

	const RoadEx& Node::GetRoad(unsigned int index) const
	{
		assert(index < m_RoadsList.size());
		return m_RoadsList[index];
	}

	// ---------------------------------------------------------------------- //

	size_t Node::RoadsCount() const
	{
		return m_RoadsList.size();
	}

	// ---------------------------------------------------------------------- //

	bool Node::IsTransit() const
	{
		return m_Transit;
	}

	// ---------------------------------------------------------------------- //

	bool Node::EqualPos(const Shapes::DVertex& point)
	{
		double dx = fabs(m_Position.x - point.x);
		double dy = fabs(m_Position.y - point.y);

		if (dx > 0.001) return false;
		if (dy > 0.001) return false;

		return true;
	}

	// ---------------------------------------------------------------------- //

	size_t Node::InitCandidatesToMerge()
	{
		m_CandidatesToMerge.clear();

		if (m_RoadsList.size() == 0) return 0;

		RoadEx& roadEx0 = m_RoadsList[0];
		Road* road0 = roadEx0.GetRoad();
		CandidatesItem item(road0->Width(), road0->Type());
		item.AddIndex(0);
		m_CandidatesToMerge.push_back(item);

		for (size_t i = 1, cntI = m_RoadsList.size(); i < cntI; ++i)
		{
			RoadEx& roadExI = m_RoadsList[i];
			Road* roadI = roadExI.GetRoad();

			CandidatesItem item(roadI->Width(), roadI->Type());

			bool found = false;
			for (size_t j = 0, cntJ = m_CandidatesToMerge.size(); j < cntJ; ++j)
			{
				CandidatesItem& itemJ = m_CandidatesToMerge[j];
				if (item == itemJ)
				{
					itemJ.AddIndex(i);
					found = true;
					break;
				}				
			}
			if (!found)
			{
				item.AddIndex(i);
				m_CandidatesToMerge.push_back(item);
			}
		}

		sort(m_CandidatesToMerge.begin(), m_CandidatesToMerge.end(), CandidatesItem_Greater);

		return m_CandidatesToMerge[0].IndicesCount();
	}

	// ---------------------------------------------------------------------- //

	void Node::MergeRoads2_2(vector<Node>& nodesList)
	{
		RoadEx& roadEx1 = m_RoadsList[0];
		RoadEx& roadEx2 = m_RoadsList[1];
		Road* road1 = roadEx1.GetRoad();
		Road* road2 = roadEx2.GetRoad();
		
		road1->MergeShapeWith(*road2, roadEx1.GetRoadEndType(), roadEx2.GetRoadEndType());
		road2->InUse(false);

		for (size_t i = 0, cntI = nodesList.size(); i < cntI; ++i)
		{
			Node& nodeI = nodesList[i];

			if (!nodeI.EqualPos(m_Position))
			{
				for (size_t j = 0, cntJ = nodeI.RoadsCount(); j < cntJ; ++j)
				{
					RoadEx& roadExJ = nodeI.GetRoad(j);
					Road* roadJ = roadExJ.GetRoad();
					if (roadJ == road1)
					{
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etStarting);
						}
					}
					if (roadJ == road2)
					{
						roadExJ.SetRoad(road1);
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etEnding);
						}
					}
				}
			}
		}
	}

	// ---------------------------------------------------------------------- //

	void Node::MergeRoads2_3(vector<Node>& nodesList)
	{
		const CandidatesItem& item = m_CandidatesToMerge[GetBestCandidateItemIndex()];

		RoadEx& roadEx1 = m_RoadsList[item.Index(0)];
		RoadEx& roadEx2 = m_RoadsList[item.Index(1)];
		Road* road1 = roadEx1.GetRoad();
		Road* road2 = roadEx2.GetRoad();
		
		road1->MergeShapeWith(*road2, roadEx1.GetRoadEndType(), roadEx2.GetRoadEndType());
		road2->InUse(false);

		for (size_t i = 0, cntI = nodesList.size(); i < cntI; ++i)
		{
			Node& nodeI = nodesList[i];

			if (!nodeI.EqualPos(m_Position))
			{
				for (size_t j = 0, cntJ = nodeI.RoadsCount(); j < cntJ; ++j)
				{
					RoadEx& roadExJ = nodeI.GetRoad(j);
					Road* roadJ = roadExJ.GetRoad();
					if (roadJ == road1)
					{
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etStarting);
						}
					}
					if (roadJ == road2)
					{
						roadExJ.SetRoad(road1);
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etEnding);
						}
					}
				}
			}
		}

		roadEx1.SetRoadEndType(etTransit);
		roadEx2.SetRoadEndType(etTransit);
		roadEx2.SetRoad(road1);

		m_Transit = true;
	}

	// ---------------------------------------------------------------------- //

	void Node::MergeRoads2_3E(vector<Node>& nodesList)
	{
		RoadEx& roadEx1 = m_RoadsList[0];
		RoadEx& roadEx2 = m_RoadsList[1];
		RoadEx& roadEx3 = m_RoadsList[2];
		Road* road1 = roadEx1.GetRoad();
		Road* road2 = roadEx2.GetRoad();
		Road* road3 = roadEx3.GetRoad();

		Shapes::DVector vec1;
		if (roadEx1.GetRoadEndType() == etStarting)
		{
			vec1 = Shapes::DVector(road1->ShapeVertex(1), road1->ShapeVertex(0)); 
		}
		else
		{
			size_t cnt1 = road1->ShapeVerticesCount();
			vec1 = Shapes::DVector(road1->ShapeVertex(cnt1 - 2), road1->ShapeVertex(cnt1 - 1)); 
		}
		vec1.NormalizeXYInPlace();

		Shapes::DVector vec2;
		if (roadEx2.GetRoadEndType() == etStarting)
		{
			vec2 = Shapes::DVector(road2->ShapeVertex(1), road2->ShapeVertex(0)); 
		}
		else
		{
			size_t cnt2 = road2->ShapeVerticesCount();
			vec2 = Shapes::DVector(road2->ShapeVertex(cnt2 - 2), road2->ShapeVertex(cnt2 - 1)); 
		}
		vec2.NormalizeXYInPlace();

		Shapes::DVector vec3;
		if (roadEx3.GetRoadEndType() == etStarting)
		{
			vec3 = Shapes::DVector(road3->ShapeVertex(1), road3->ShapeVertex(0)); 
		}
		else
		{
			size_t cnt3 = road3->ShapeVerticesCount();
			vec3 = Shapes::DVector(road3->ShapeVertex(cnt3 - 2), road3->ShapeVertex(cnt3 - 1)); 
		}
		vec3.NormalizeXYInPlace();

		double minCos = vec1.DotXY(vec2);
		Road* mergeRoad1 = road1;
		Road* mergeRoad2 = road2;
		RoadEx* mergeRoadEx1 = &m_RoadsList[0];
		RoadEx* mergeRoadEx2 = &m_RoadsList[1];
		RoadEndTypes etRoad1 = roadEx1.GetRoadEndType();
		RoadEndTypes etRoad2 = roadEx2.GetRoadEndType();

		double tempCos = vec2.DotXY(vec3);
		if (tempCos < minCos)
		{
			mergeRoad1 = road2;
			mergeRoad2 = road3;
			mergeRoadEx1 = &m_RoadsList[1];
			mergeRoadEx2 = &m_RoadsList[2];
			etRoad1 = roadEx2.GetRoadEndType();
			etRoad2 = roadEx3.GetRoadEndType();
			minCos = tempCos;
		}

		tempCos = vec3.DotXY(vec1);
		if (tempCos < minCos)
		{
			mergeRoad1 = road3;
			mergeRoad2 = road1;
			mergeRoadEx1 = &m_RoadsList[2];
			mergeRoadEx2 = &m_RoadsList[0];
			etRoad1 = roadEx3.GetRoadEndType();
			etRoad2 = roadEx1.GetRoadEndType();
			minCos = tempCos;
		}

		mergeRoad1->MergeShapeWith(*mergeRoad2, etRoad1, etRoad2);
		mergeRoad2->InUse(false);

		for (size_t i = 0, cntI = nodesList.size(); i < cntI; ++i)
		{
			Node& nodeI = nodesList[i];

			if (!nodeI.EqualPos(m_Position))
			{
				for (size_t j = 0, cntJ = nodeI.RoadsCount(); j < cntJ; ++j)
				{
					RoadEx& roadExJ = nodeI.GetRoad(j);
					Road* roadJ = roadExJ.GetRoad();
					if (roadJ == mergeRoad1)
					{
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etStarting);
						}
					}
					if (roadJ == mergeRoad2)
					{
						roadExJ.SetRoad(mergeRoad1);
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etEnding);
						}
					}
				}
			}
		}

		mergeRoadEx1->SetRoadEndType(etTransit);
		mergeRoadEx2->SetRoadEndType(etTransit);
		mergeRoadEx2->SetRoad(mergeRoad1);

		m_Transit = true;
	}

	// ---------------------------------------------------------------------- //

	void Node::MergeRoads2_4(vector<Node>& nodesList)
	{
		const CandidatesItem& item = m_CandidatesToMerge[GetBestCandidateItemIndex()];

		RoadEx& roadEx1 = m_RoadsList[item.Index(0)];
		RoadEx& roadEx2 = m_RoadsList[item.Index(1)];
		Road* road1 = roadEx1.GetRoad();
		Road* road2 = roadEx2.GetRoad();
		
		road1->MergeShapeWith(*road2, roadEx1.GetRoadEndType(), roadEx2.GetRoadEndType());
		road2->InUse(false);

		for (size_t i = 0, cntI = nodesList.size(); i < cntI; ++i)
		{
			Node& nodeI = nodesList[i];

			if (!nodeI.EqualPos(m_Position))
			{
				for (size_t j = 0, cntJ = nodeI.RoadsCount(); j < cntJ; ++j)
				{
					RoadEx& roadExJ = nodeI.GetRoad(j);
					Road* roadJ = roadExJ.GetRoad();
					if (roadJ == road1)
					{
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etStarting);
						}
					}
					if (roadJ == road2)
					{
						roadExJ.SetRoad(road1);
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etEnding);
						}
					}
				}
			}
		}

		roadEx1.SetRoadEndType(etTransit);
		roadEx2.SetRoadEndType(etTransit);
		roadEx2.SetRoad(road1);

		m_Transit = true;
	}

	// ---------------------------------------------------------------------- //

	void Node::MergeRoads2_4E(vector<Node>& nodesList)
	{
		const CandidatesItem& item = m_CandidatesToMerge[GetBestCandidateItemIndex()];

		RoadEx& roadEx1 = m_RoadsList[item.Index(0)];
		RoadEx& roadEx2 = m_RoadsList[item.Index(1)];
		RoadEx& roadEx3 = m_RoadsList[item.Index(2)];
		Road* road1 = roadEx1.GetRoad();
		Road* road2 = roadEx2.GetRoad();
		Road* road3 = roadEx3.GetRoad();

		Shapes::DVector vec1;
		if (roadEx1.GetRoadEndType() == etStarting)
		{
			vec1 = Shapes::DVector(road1->ShapeVertex(1), road1->ShapeVertex(0)); 
		}
		else
		{
			size_t cnt1 = road1->ShapeVerticesCount();
			vec1 = Shapes::DVector(road1->ShapeVertex(cnt1 - 2), road1->ShapeVertex(cnt1 - 1)); 
		}
		vec1.NormalizeXYInPlace();

		Shapes::DVector vec2;
		if (roadEx2.GetRoadEndType() == etStarting)
		{
			vec2 = Shapes::DVector(road2->ShapeVertex(1), road2->ShapeVertex(0)); 
		}
		else
		{
			size_t cnt2 = road2->ShapeVerticesCount();
			vec2 = Shapes::DVector(road2->ShapeVertex(cnt2 - 2), road2->ShapeVertex(cnt2 - 1)); 
		}
		vec2.NormalizeXYInPlace();

		Shapes::DVector vec3;
		if (roadEx3.GetRoadEndType() == etStarting)
		{
			vec3 = Shapes::DVector(road3->ShapeVertex(1), road3->ShapeVertex(0)); 
		}
		else
		{
			size_t cnt3 = road3->ShapeVerticesCount();
			vec3 = Shapes::DVector(road3->ShapeVertex(cnt3 - 2), road3->ShapeVertex(cnt3 - 1)); 
		}
		vec3.NormalizeXYInPlace();

		double minCos = vec1.DotXY(vec2);
		Road* mergeRoad1 = road1;
		Road* mergeRoad2 = road2;
		RoadEx* mergeRoadEx1 = &m_RoadsList[0];
		RoadEx* mergeRoadEx2 = &m_RoadsList[1];
		RoadEndTypes etRoad1 = roadEx1.GetRoadEndType();
		RoadEndTypes etRoad2 = roadEx2.GetRoadEndType();

		double tempCos = vec2.DotXY(vec3);
		if (tempCos < minCos)
		{
			mergeRoad1 = road2;
			mergeRoad2 = road3;
			mergeRoadEx1 = &m_RoadsList[1];
			mergeRoadEx2 = &m_RoadsList[2];
			etRoad1 = roadEx2.GetRoadEndType();
			etRoad2 = roadEx3.GetRoadEndType();
			minCos = tempCos;
		}

		tempCos = vec3.DotXY(vec1);
		if (tempCos < minCos)
		{
			mergeRoad1 = road3;
			mergeRoad2 = road1;
			mergeRoadEx1 = &m_RoadsList[2];
			mergeRoadEx2 = &m_RoadsList[0];
			etRoad1 = roadEx3.GetRoadEndType();
			etRoad2 = roadEx1.GetRoadEndType();
			minCos = tempCos;
		}

		mergeRoad1->MergeShapeWith(*mergeRoad2, etRoad1, etRoad2);
		mergeRoad2->InUse(false);

		for (size_t i = 0, cntI = nodesList.size(); i < cntI; ++i)
		{
			Node& nodeI = nodesList[i];

			if (!nodeI.EqualPos(m_Position))
			{
				for (size_t j = 0, cntJ = nodeI.RoadsCount(); j < cntJ; ++j)
				{
					RoadEx& roadExJ = nodeI.GetRoad(j);
					Road* roadJ = roadExJ.GetRoad();
					if (roadJ == mergeRoad1)
					{
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etStarting);
						}
					}
					if (roadJ == mergeRoad2)
					{
						roadExJ.SetRoad(mergeRoad1);
						if (roadExJ.GetRoadEndType() != etTransit)
						{
							roadExJ.SetRoadEndType(etEnding);
						}
					}
				}
			}
		}

		mergeRoadEx1->SetRoadEndType(etTransit);
		mergeRoadEx2->SetRoadEndType(etTransit);
		mergeRoadEx2->SetRoad(mergeRoad1);

		m_Transit = true;
	}

	// ---------------------------------------------------------------------- //

	void Node::RebuildNonTransitRoads()
	{
		// first get the transit road
		Road* transitRoad;

		for (size_t i = 0, cntI = m_RoadsList.size(); i < cntI; ++i)
		{
			if (m_RoadsList[i].GetRoadEndType() == etTransit)
			{
				transitRoad = m_RoadsList[i].GetRoad();
			}
		}

		if (!transitRoad) return;

		vector<Shapes::DVertex> transitShape;
		for (size_t i = 0, cntI = transitRoad->SegmentShapeVerticesCount(); i < cntI; ++i)
		{
			transitShape.push_back(transitRoad->SegmentShapeVertex(i));
		}

		// now modify the original shape of the non transit roads
		for (size_t i = 0, cntI = m_RoadsList.size(); i < cntI; ++i)
		{
			if (m_RoadsList[i].GetRoadEndType() != etTransit)
			{
				Road* currRoad = m_RoadsList[i].GetRoad();

				if (currRoad != transitRoad)
				{
					vector<Shapes::DVertex> currShape;
					for (size_t j = 0, cntJ = currRoad->ShapeVerticesCount(); j < cntJ; ++j)
					{
						currShape.push_back(currRoad->ShapeVertex(j));
					}

					if (m_RoadsList[i].GetRoadEndType() == etEnding)
					{
						// reverse the shape
						vector<Shapes::DVertex> tempShape;
						for (int j = currShape.size() - 1; j >= 0; --j)
						{
							tempShape.push_back(currShape[j]);
						}
						currShape = tempShape;
					}

					PolylineInsersector polyInt(transitShape, currShape);
					const vector<Shapes::DVertex>& intPoints = polyInt.IntersectionPoints();

					if (intPoints.size() == 0)
					{
						Shapes::DVertex closestPt = transitRoad->ClosestPointOnSegmentShapeTo(currShape[0]);
						currShape[0] = closestPt;
					}
					else
					{
					}

					if (m_RoadsList[i].GetRoadEndType() == etEnding)
					{
						// reverse the shape again to restore old direction
						vector<Shapes::DVertex> tempShape;
						for (int j = currShape.size() - 1; j >= 0; --j)
						{
							tempShape.push_back(currShape[j]);
						}
						currShape = tempShape;
					}

					currRoad->ReplaceShape(currShape);
					currRoad->Built(false);
				}
			}
		}
	}

	// ---------------------------------------------------------------------- //

	size_t Node::GetBestCandidateItemIndex() const
	{
		size_t index = 0;

		for (size_t i = 1, cntI = m_CandidatesToMerge.size(); i < cntI; ++i)
		{
			if (m_CandidatesToMerge[i].IndicesCount() < m_CandidatesToMerge[index].IndicesCount())
			{
				return index;
			}
			if (m_CandidatesToMerge[i].Width() > m_CandidatesToMerge[index].Width())
			{
				index = i;
			}
		}

		return index;
	}

	// ---------------------------------------------------------------------- //
	// PolylineInsersector
	// ---------------------------------------------------------------------- //

	PolylineInsersector::PolylineInsersector(const vector<Shapes::DVertex>& poly1, const vector<Shapes::DVertex>& poly2)
	: m_Poly1(poly1)
	, m_Poly2(poly2)
	{
		CalculateIntersectionPoints();
	}

	// ---------------------------------------------------------------------- //

	const vector<Shapes::DVertex>& PolylineInsersector::IntersectionPoints() const
	{
		return m_IntPoints;
	}

	// ---------------------------------------------------------------------- //

	void PolylineInsersector::CalculateIntersectionPoints()
	{
	}

	// ---------------------------------------------------------------------- //
}

// -------------------------------------------------------------------------- //
