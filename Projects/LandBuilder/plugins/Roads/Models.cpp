#include "StdAfx.h"

#include "Models.h"

#include <projects/ObjektivLib/ObjToolTopology.h>

// -------------------------------------------------------------------------- //

#ifdef _VBS_ONLY_
  #define DEFAULT_RVMAT "vbs2\\data\\default.rvmat"
#endif

//-----------------------------------------------------------------------------

namespace RG_Help
{
	// -------------------------------------------------------------------------- //
	// Color
	// -------------------------------------------------------------------------- //

	Color::Color(unsigned int red, unsigned int green, unsigned int blue)
	: m_Red(red)
	, m_Green(green)
	, m_Blue(blue)
	{
	}

	// -------------------------------------------------------------------------- //

	Color::Color(unsigned int color)
	{
		m_Red = color / 1000000;
		color -= m_Red * 1000000;
		m_Green = color / 1000;
		color -= m_Green * 1000;
		m_Blue = color;
	}

	// -------------------------------------------------------------------------- //

	unsigned int Color::GetRed() const
	{
		return m_Red;
	}

	// -------------------------------------------------------------------------- //

	unsigned int Color::GetGreen() const
	{
		return m_Green;
	}

	// -------------------------------------------------------------------------- //

	unsigned int Color::GetBlue() const
	{
		return m_Blue;
	}

	// -------------------------------------------------------------------------- //
	// Line
	// -------------------------------------------------------------------------- //

	Line::Line(const Shapes::DVertex& v1, const Shapes::DVertex& v2)
	: m_V1(v1)
	, m_V2(v2)
	{
		CalculateCoefficients();
	}

	// -------------------------------------------------------------------------- //

	const Shapes::DVertex& Line::GetV1() const
	{
		return m_V1;
	}

	// -------------------------------------------------------------------------- //

	const Shapes::DVertex& Line::GetV2() const
	{
		return m_V2;
	}

	// -------------------------------------------------------------------------- //

	bool Line::Intersection(const Line& other, Shapes::DVertex& intPoint) const
	{
		double den = m_B * other.m_A - m_A * other.m_B;
		if (abs(den) < EPSILON) 
		{
			intPoint.x = DBL_MAX;
			intPoint.y = DBL_MAX;
			return false;
		}

		double numX = m_C * other.m_B - m_B * other.m_C;
		double numY = m_A * other.m_C - m_C * other.m_A;

		intPoint.x = numX / den;
		intPoint.y = numY / den;

		return true;
	}

	// -------------------------------------------------------------------------- //

	bool Line::ContainsBetweenEnds(const Shapes::DVertex& point) const
	{
		double dist1 = point.DistanceXY(m_V1);
		double dist2 = point.DistanceXY(m_V2);
		double dist12 = m_V1.DistanceXY(m_V2);

		if (abs(dist12 - (dist1 + dist2)) < EPSILON)
		{
			return true;
		}
		return false;
	}

	// -------------------------------------------------------------------------- //

	void Line::CalculateCoefficients()
	{
		double dx = m_V2.x - m_V1.x;
		double dy = m_V2.y - m_V1.y;

		if (abs(dx) < EPSILON)
		{
			if (abs(dy) < EPSILON)
			{
				m_A = 0.0;
				m_B = 0.0;
				m_C = 0.0;
			}
			else
			{
				m_A = 1.0;
				m_B = 0.0;
				m_C = -m_V1.x;
			}
		}
		else if (abs(dy) < EPSILON)
		{
			m_A = 0.0;
			m_B = 1.0;
			m_C = -m_V1.y;
		}
		else
		{
			m_A = 1.0 / dx;
			m_B = -1.0 / dy;
			m_C = - m_V1.x / dx + m_V1.y / dy;
		}
	}

	// -------------------------------------------------------------------------- //
	// BoundingBox
	// -------------------------------------------------------------------------- //

	BoundingBox::BoundingBox()
	: m_Width(DBL_MAX)
	, m_Height(DBL_MAX)
	, m_Orientation(0.0)
	{
	}

	// -------------------------------------------------------------------------- //

	double BoundingBox::GetWidth() const
	{
		return m_Width;
	}
	
	// -------------------------------------------------------------------------- //

	double BoundingBox::GetHeight() const
	{
		return m_Height;
	}

	// -------------------------------------------------------------------------- //

	double BoundingBox::GetOrientation() const
	{
		return m_Orientation;
	}

	// -------------------------------------------------------------------------- //

	void BoundingBox::Reset()
	{
		m_Width       = DBL_MAX;
		m_Height      = DBL_MAX;
		m_Orientation = 0.0;
	}

	// -------------------------------------------------------------------------- //

	void BoundingBox::CalculateFromVertices(const vector<Shapes::DVertex>& vertices)
	{
		unsigned int cnt = vertices.size();
		double minArea = DBL_MAX;

		Shapes::DVertex p0 = vertices[0];

		for (unsigned int i = 0, j = (cnt - 1); i < cnt; j = i++)
		{
			const Shapes::DVertex& pJ = vertices[j];
			const Shapes::DVertex& pI = vertices[i];

			Shapes::DVector vJI(pI, pJ);
			vJI.NormalizeXYInPlace();

			Shapes::DVector vPerp = vJI.PerpXYCCW();

			double w0 = 0.0;
			double w1 = 0.0;
			double h0 = 0.0;
			double h1 = 0.0;

			for (unsigned int k = 1; k < cnt; ++k)
			{
				const Shapes::DVertex& pK = vertices[k];
				Shapes::DVector vK0(pK, p0);

				double testW = vJI.DotXY(vK0);
				if (testW < w0)
				{
					w0 = testW;
				}
				else if (testW > w1)
				{
					w1 = testW;
				}

				double testH = vPerp.DotXY(vK0);
				if (testH < h0)
				{
					h0 = testH;
				}
				else if (testH > h1)
				{
					h1 = testH;
				}
			}

			double tempWidth = w1 - w0;
			double tempHeight  = h1 - h0; 
			double area = tempWidth * tempHeight;
			if (area < minArea)
			{
				minArea       = area;
				m_Width       = tempWidth;
				m_Height      = tempHeight;
				m_Orientation = vJI.DirectionXY(); 
			}		
		}

		if (m_Orientation != 0.0)
		{
			if (m_Width < m_Height)
			{
				m_Orientation += HALF_PI;
				double temp = m_Width;
				m_Width = m_Height;
				m_Height = temp;
			}
		}
	}

	// -------------------------------------------------------------------------- //
	// Contour
	// -------------------------------------------------------------------------- //

	Contour::Contour()
	{
	}

	// -------------------------------------------------------------------------- //

	Contour::Contour(const vector<Shapes::DVertex>& vertices)
	{
		AddVertices(vertices);
	}

	// -------------------------------------------------------------------------- //

	bool Contour::Equal(const Contour& other) const
	{
		if (other.GetVerticesCount() != m_Vertices.size())
		{
			return false;
		}

		vector<Shapes::DVertex> tempThis = m_Vertices; 
		double x0 = tempThis[0].x;
		double y0 = tempThis[0].y;
		for (unsigned int i = 0, cnt = tempThis.size(); i < cnt; ++i)
		{
			tempThis[i].x -= x0;
			tempThis[i].y -= y0;
		}

		vector<Shapes::DVertex> tempOther = other.GetVertices(); 
		x0 = tempOther[0].x;
		y0 = tempOther[0].y;
		for (unsigned int i = 0, cnt = tempOther.size(); i < cnt; ++i)
		{
			tempOther[i].x -= x0;
			tempOther[i].y -= y0;
		}
	
		Shapes::DVector v01This(tempThis[1], tempThis[0]);
		double dirThis = v01This.DirectionXY();
		Shapes::DVector v01Other(tempOther[1], tempOther[0]);
		double dirOther = v01Other.DirectionXY();

		double rotation = dirThis - dirOther;

		double x;
		double y;
		for (unsigned int i = 0, cnt = tempThis.size(); i < cnt; ++i)
		{
			x = tempOther[i].x;
			y = tempOther[i].y;
			tempOther[i].x = x * cos(rotation) - y * sin(rotation);
			tempOther[i].y = x * sin(rotation) + y * cos(rotation);
		}

		for (unsigned int i = 0, cnt = tempThis.size(); i < cnt; ++i)
		{
			Shapes::DVector v(tempOther[i], tempThis[i]);
			if (v.SizeXY() > 0.001) // millimeter
			{
				return false;
			}
		}

		return true;
	}

	// -------------------------------------------------------------------------- //

	const vector<Shapes::DVertex>& Contour::GetVertices() const
	{
		return m_Vertices;
	}

	// -------------------------------------------------------------------------- //

	vector<Shapes::DVertex>& Contour::GetVertices()
	{
		return m_Vertices;
	}

	// -------------------------------------------------------------------------- //

	void Contour::AddVertex(const Shapes::DVertex& vertex)
	{
		m_Vertices.push_back(vertex);
		m_BoundingBox.Reset();
	}

	// -------------------------------------------------------------------------- //

	void Contour::AddVertices(const vector<Shapes::DVertex>& vertices)
	{
		for (unsigned int i = 0, cnt = vertices.size(); i < cnt; ++i)
		{
			m_Vertices.push_back(vertices[i]);
		}
		m_BoundingBox.Reset();
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex& Contour::operator [] (unsigned int index)
	{
		assert(index < m_Vertices.size());
		return m_Vertices[index];
	}

	// -------------------------------------------------------------------------- //

	const Shapes::DVertex& Contour::operator [] (unsigned int index) const
	{
		assert(index < m_Vertices.size());
		return m_Vertices[index];
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex& Contour::GetVertex(unsigned int index)
	{
		assert(index < m_Vertices.size());
		return m_Vertices[index];
	}

	// -------------------------------------------------------------------------- //

	const Shapes::DVertex& Contour::GetVertex(unsigned int index) const
	{
		assert(index < m_Vertices.size());
		return m_Vertices[index];
	}

	// -------------------------------------------------------------------------- //

	void Contour::SetVertex(unsigned int index, const Shapes::DVertex& vertex)
	{
		assert(index < m_Vertices.size());
		m_Vertices[index] = vertex;
		m_BoundingBox.Reset();
	}

	// -------------------------------------------------------------------------- //

	void Contour::InsertVertex(unsigned int index, const Shapes::DVertex& vertex)
	{
		assert(index < m_Vertices.size());
		m_Vertices.insert(m_Vertices.begin() + index, vertex);
		m_BoundingBox.Reset();
	}

	// -------------------------------------------------------------------------- //

	void Contour::InsertVertices(unsigned int index, const vector<Shapes::DVertex>& vertices)
	{
		assert(index < m_Vertices.size());
		m_Vertices.insert(m_Vertices.begin() + index, vertices.begin(), vertices.end());
		m_BoundingBox.Reset();
	}

	// -------------------------------------------------------------------------- //

	unsigned int Contour::GetVerticesCount() const
	{
		return m_Vertices.size();
	}

	// -------------------------------------------------------------------------- //

	void Contour::SetVerticesOrderToCCW()
	{
		if (SignedArea() < 0.0)
		{
			ReverseVerticesOrder();
		}
	}

	// -------------------------------------------------------------------------- //

	double Contour::GetMinBoundingBoxWidth()
	{
		if (m_BoundingBox.GetWidth() == DBL_MAX)
		{
			m_BoundingBox.CalculateFromVertices(m_Vertices);
		}
		return m_BoundingBox.GetWidth();
	}

	// -------------------------------------------------------------------------- //

	double Contour::GetMinBoundingBoxOrientation()
	{
		if (m_BoundingBox.GetWidth() == DBL_MAX)
		{
			m_BoundingBox.CalculateFromVertices(m_Vertices);
		}
		return m_BoundingBox.GetOrientation();
	}

	// -------------------------------------------------------------------------- //

	void Contour::ResetBoundingBox()
	{
		m_BoundingBox.Reset();
	}

//-----------------------------------------------------------------------------

	void Contour::PaintOnMask( HDC dc, HBITMAP bmp, DoubleRect mapRect, size_t x, size_t y, size_t maskSize)
	{
		BITMAPINFOEX binfo;

		memset( &binfo, 0, sizeof( binfo ) );
		binfo.bmiHeader.biSize = sizeof( binfo.bmiHeader );

		GetDIBits( dc, bmp, 0, 1, 0, &binfo, DIB_RGB_COLORS );
		binfo.bmiHeader.biBitCount = 32;

		size_t cnt = m_Vertices.size();

		POINT* pts = new POINT[cnt];

		for ( size_t i = 0; i < cnt; ++i )
		{
			pts[i].x = (m_Vertices[i].x * maskSize / mapRect.Width()) - x;
			pts[i].y = maskSize - (m_Vertices[i].y * maskSize / mapRect.Height()) - y;
		}

		Polygon( dc, pts, cnt );
		delete [] pts;
	}

//-----------------------------------------------------------------------------

	double Contour::SignedArea() const
	{
		double area = 0.0;
		unsigned int cnt = m_Vertices.size();
		// calculates area (gauss algorithm)
		for (unsigned int i = 0, j = (cnt - 1); i < cnt; j = i++)
		{
			Shapes::DVertex v1 = m_Vertices[j];
			Shapes::DVertex v2 = m_Vertices[i];

			area += v1.x * v2.y - v1.y * v2.x;
		}
		return (0.5 * area);
	}

	// -------------------------------------------------------------------------- //

	void Contour::ReverseVerticesOrder()
	{
		vector<Shapes::DVertex> tempList;

		int size = m_Vertices.size();
		for (int i = size - 1; i >= 0; --i)
		{
			tempList.push_back(m_Vertices[i]);
		}

		m_Vertices = tempList;
		m_BoundingBox.Reset();
	}

	// -------------------------------------------------------------------------- //

	bool Contour::IntersectionWithSegment(const Line& line, Shapes::DVertex& intPoint, 
										  bool strictOnSegment) const
	{
		intPoint.x = intPoint.y = intPoint.z = DBL_MAX;
		Shapes::DVertex inters(DBL_MAX, DBL_MAX, DBL_MAX);
		double minDist = DBL_MAX;
		Shapes::DVertex tempMin(DBL_MAX, DBL_MAX, DBL_MAX);

		unsigned int cnt = m_Vertices.size();
		for (unsigned int i = 0, j = (cnt - 1); i < cnt; j = i++)
		{
			Shapes::DVertex vJ = m_Vertices[j];
			Shapes::DVertex vI = m_Vertices[i];

			Line lineVJI(vJ, vI);
			if (lineVJI.Intersection(line, inters))
			{
				if (lineVJI.ContainsBetweenEnds(inters))
				{
					if (strictOnSegment)
					{
						if (line.ContainsBetweenEnds(inters))
						{
							intPoint = inters;
							return true;
						}
					}
					else
					{
						double dist = line.GetV1().DistanceXY(inters);
						if (minDist > dist)
						{
							minDist = dist;
							tempMin = inters;
						}
					}
				}
			}
		}

		if (strictOnSegment)
		{
			return false;
		}
		else
		{
			if (minDist == DBL_MAX)
			{
				return false;
			}
			else
			{
				intPoint = tempMin;
				return true;
			}
		}
	}

	// -------------------------------------------------------------------------- //
	// Texture
	// -------------------------------------------------------------------------- //
		
	Texture::Texture(RString pathname, RString filename, unsigned int size)
	: m_Pathname(pathname)
	, m_Filename(filename)
	, m_Size(size)
	, m_Mask(NULL)
	, m_DC(NULL)
	{
	}

	// -------------------------------------------------------------------------- //

	Texture::~Texture()
	{
		Reset();
	}

	// -------------------------------------------------------------------------- //

	RString Texture::GetPathname() const
	{
		return m_Pathname;
	}

	// -------------------------------------------------------------------------- //

	RString Texture::GetFilename() const
	{
		return m_Filename;
	}

	// -------------------------------------------------------------------------- //

	unsigned int Texture::GetSize() const
	{
		return m_Size;
	}

	// -------------------------------------------------------------------------- //

	HBITMAP Texture::GetMask() const
	{
		return m_Mask;
	}

	// -------------------------------------------------------------------------- //

	HDC Texture::GetDC() const
	{
		return m_DC;
	}

	// -------------------------------------------------------------------------- //

	void Texture::SetMask(HBITMAP mask)
	{
		m_Mask = mask;
	}

	// -------------------------------------------------------------------------- //

	void Texture::SetDC(HDC dc)
	{
		m_DC = dc;
	}

	// -------------------------------------------------------------------------- //

	void Texture::Reset()
	{
		if (m_Mask) DeleteObject(m_Mask);
		if (m_DC)   DeleteObject(m_DC);
	}

	// -------------------------------------------------------------------------- //

	void Texture::GenerateFromModelFaces(const ObjectData& obj)
	{
		bool inittex = false;

		HDC display = ::CreateDC("DISPLAY", 0, 0, 0);
		if (m_DC == 0) m_DC = CreateCompatibleDC(display);

		HDC dc = m_DC;
		if (m_Mask == 0) 
		{
			m_Mask = CreateCompatibleBitmap(display, m_Size, m_Size);
			inittex = true;
		}

		HBITMAP bmp = m_Mask;
		DeleteDC(display);

		HBITMAP oldbmp = (HBITMAP)SelectObject(dc, bmp);
		SelectObject(dc, GetStockObject(WHITE_BRUSH));
	  
		RECT whole;
		whole.left = whole.top = 0;
		whole.right = whole.bottom = m_Size;

		if (inittex) FillRect(dc, &whole, (HBRUSH)GetStockObject(WHITE_BRUSH));

		SelectObject(dc, GetStockObject(DC_BRUSH));
		SetDCBrushColor(dc, 0x393b39);
		SelectObject(dc, GetStockObject(DC_PEN));
		SetDCPenColor(dc, 0x393b39);
		SetROP2(dc, R2_MASKPEN);

		for (int i = 0; i < obj.NFaces(); ++i)
		{
			FaceT fc(obj, i);
			POINT pt[4];
			for (int j = 0; j < fc.N(); ++j)
			{
				const ObjUVSet_Item uv = fc.GetUV(j);
				float u = uv.u * m_Size;
				float v = uv.v * m_Size;
				pt[j].x = toInt(floor(u - 0.5f));
				pt[j].y = toInt(floor(v - 0.5f));
			}
			Polygon(dc, pt, fc.N());
		}
		SelectObject(dc, oldbmp);
	}

	// -------------------------------------------------------------------------- //

	bool Texture::Save() const
	{
		if (m_DC == 0) return true;

		HDC     dc  = m_DC;
		HBITMAP bmp = m_Mask;
		BITMAPINFOEX binfo;

		memset(&binfo, 0, sizeof(binfo));
		binfo.bmiHeader.biSize = sizeof(binfo.bmiHeader);
  
		GetDIBits(dc, bmp, 0, 1, 0, &binfo, DIB_RGB_COLORS);
		binfo.bmiHeader.biBitCount = 32;

		LONG bitsize = binfo.bmiHeader.biWidth * binfo.bmiHeader.biHeight;
		unsigned long* bits = new unsigned long[bitsize];

		GetDIBits(dc, bmp, 0, binfo.bmiHeader.biHeight, bits, &binfo, DIB_RGB_COLORS);

		Picture respic;
		respic.Create(binfo.bmiHeader.biWidth, binfo.bmiHeader.biHeight);

		unsigned long* ln = bits;
		for (int y = binfo.bmiHeader.biHeight - 1 ; y >= 0; --y)
		{
			for (int x = 0; x < binfo.bmiHeader.biWidth; ++x)
			{
				PPixel px = ln[x];
				px |= 0xFF000000;
				respic.SetPixel(x, y, px);
			}
			ln += binfo.bmiHeader.biWidth;
		}

		delete [] bits;

		return respic.SaveTGA(m_Pathname + m_Filename + ".tga") == 0;
	}

	// -------------------------------------------------------------------------- //
	// ModelP3D
	// -------------------------------------------------------------------------- //

	ModelP3D::ModelP3D( RString filename, const Contour& contour, 
                      RString modelPath, RString texturesPath, 
                      const vector< Texture >& textures )
	: m_Filename( filename )
	, m_Contour( contour )
	, m_ModelPath( modelPath )
	, m_TexturesPath( texturesPath )
	, m_Textures( textures )
	{
	}

	// -------------------------------------------------------------------------- //

	ModelP3D::~ModelP3D()
	{
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex ModelP3D::GetPosition() const
	{
		return (m_Position);
	}

	// -------------------------------------------------------------------------- //

	double ModelP3D::GetOrientation() const
	{
		return (m_Orientation);
	}

	// -------------------------------------------------------------------------- //

	void ModelP3D::OrientateAsMinBoundingBox()
	{
		m_Orientation = m_Contour.GetMinBoundingBoxOrientation();

		if ( m_Orientation != 0.0 )
		{
			// we are rotating about the origin
			double x;
			double y;
			for ( unsigned int i = 0, cnt = m_Contour.GetVerticesCount(); i < cnt; ++i )
			{
				x = m_Contour[i].x;
				y = m_Contour[i].y;
				m_Contour[i].x = x * cos(m_Orientation) + y * sin( m_Orientation );
				m_Contour[i].y = - x * sin(m_Orientation) + y * cos( m_Orientation );
			}
		}
	}

	// -------------------------------------------------------------------------- //
	// CrossroadModelP3D
	// -------------------------------------------------------------------------- //

	CrossroadModelP3D::CrossroadModelP3D( RString filename, const Contour& contour, 
                                        RString modelPath, RString texturesPath, 
                                        const vector< Texture >& textures, 
                                        unsigned int textureSize,
                                        unsigned int crossroadResolution,
                                        const vector< RoadEndTypes >& roadsEndTypes,
                                        unsigned int roadsCount )
	: ModelP3D( filename, contour, modelPath, texturesPath, textures )
	, m_TextureSize( textureSize )
	, m_CrossroadResolution( crossroadResolution )
	, m_RoadsEndTypes( roadsEndTypes )
	, m_RoadsCount( roadsCount )
	{
		SetOrigin();
		OrientateAsMinBoundingBox();
	}

	// -------------------------------------------------------------------------- //

	CrossroadModelP3D::~CrossroadModelP3D()
	{
	}

	// -------------------------------------------------------------------------- //

	void CrossroadModelP3D::ExportToP3DFile()
	{
		LODObject lodObj;

		ObjectData* obj = new ObjectData;
		GenerateLOD( *obj, 1.0 );
		obj->SetNamedProp( "autocenter", "0" );
		obj->SetNamedProp( "class", "road" );
		obj->SetNamedProp( "map", "road" );
		lodObj.InsertLevel( obj, 1.0 );

		lodObj.DeleteLevel( 0 );

		obj = new ObjectData;
		GenerateLOD( *obj, LOD_ROADWAY );
		lodObj.InsertLevel( obj, LOD_ROADWAY );

		obj = new ObjectData;
		GenerateMemoryLOD( *obj );
		lodObj.InsertLevel( obj, LOD_MEMORY );

		Pathname path = m_ModelPath + m_Filename + ".p3d";
		if ( lodObj.Save( path, OBJDATA_LATESTVERSION, false, 0, 0 ) != 0 ) 
		{
			throw new std::exception( "Unable to save output file (p3d)" );
		}
	}

	// -------------------------------------------------------------------------- //

	void CrossroadModelP3D::GenerateTextureBitmap( ObjectData& obj )
	{
		Texture texture( m_TexturesPath, m_Filename, m_TextureSize ); 
		texture.GenerateFromModelFaces( obj );
		if ( !texture.Save() ) 
		{
			throw new std::exception( "Unable to save output file (tga)" );
		}
		texture.Reset();

		RString path = m_TexturesPath + m_Filename + ".tga";
		int posDrive = path.Find( ":" );
		path = path.Substring( posDrive + 1, path.GetLength() );
		if ( path[0] == '\\' )
		{
			path = path.Substring( 1, path.GetLength() );
		}

		for ( int i = 0; i < obj.NFaces(); ++i )
		{
			FaceT fc( obj, i );
			fc.SetTexture( path );

#ifdef _VBS_ONLY_
      fc.SetMaterial( DEFAULT_RVMAT );
#endif
    
    }

		m_Textures.push_back( texture );
	}

//-----------------------------------------------------------------------------

	void CrossroadModelP3D::GenerateLOD( ObjectData& obj, float resolution )
	{
		AutoArray< int > indices;

		for ( size_t i = 0, cnt = m_Contour.GetVerticesCount(); i < cnt; ++i )
		{
			obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[i].x, 
                                         (Coord)0.0, 
                                         (Coord)m_Contour[i].y ) );
			indices.Add( i );
		}

		ObjToolTopology& topology = obj.GetTool< ObjToolTopology >();

		topology.TessellatePolygon( indices.Size(), indices.Data() );

    if ( resolution != LOD_ROADWAY )
    {
		  GenerateMapping( obj );
		  if ( m_Textures.size() == 0 ) { GenerateTextureBitmap( obj ); }
    }

		obj.RecalcNormals();
	}

//-----------------------------------------------------------------------------

	void CrossroadModelP3D::GenerateMemoryLOD( ObjectData& obj )
	{
		unsigned int cnt = m_Contour.GetVerticesCount();

		obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[0].x,
                                       (Coord)0.0,
                                       (Coord)m_Contour[0].y ) );

		size_t i = 0;
		while ((i + 1) * m_CrossroadResolution + i < cnt - 1)
		{
			size_t index = (i + 1) * m_CrossroadResolution + i;
			obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[index].x,
                                         (Coord)0.0, 
                                         (Coord)m_Contour[index].y ) );
			obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[index + 1].x,
                                         (Coord)0.0, 
                                         (Coord)m_Contour[index + 1].y ) );
			++i;
		}

		obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[cnt - 1].x,
                                       (Coord)0.0, 
                                       (Coord)m_Contour[cnt - 1].y ) );

		for ( int i = 0, cnt = obj.NPoints(); i < cnt; ++i )
		{
			string netCode;
			string snapCode;
			switch ( i )
			{
			case 0:
				{
					netCode = "PB";
					snapCode = ".sp_crdm_A_1";
				}
				break;
			case 1:
				{
					if ( cnt < 8 ) 
					{
						netCode = "PE";
						snapCode = ".sp_crdm_V_1";
					}
					else
					{
						netCode = "PD";
						snapCode = ".sp_crds_V_0";
					}
				}
				break;
			case 2:
				{
					if ( cnt < 8 ) 
					{
						netCode = "LE";
						snapCode = ".sp_crdm_V_0";
					}
					else
					{
						netCode = "PH";
						snapCode = ".sp_crds_V_1";
					}
				}
				break;
			case 3:
				{
					if ( cnt < 6 ) 
					{
						netCode = "LB";
						snapCode = ".sp_crdm_A_0";
					}
					else if ( cnt < 8 ) 
					{
						netCode = "LH";
						snapCode = ".sp_crds_A_1";
					}
					else
					{
						netCode = "PE";
						snapCode = ".sp_crdm_V_1";
					}
				}
				break;
			case 4:
				{
					if ( cnt < 8 ) 
					{
						netCode = "LD";
						snapCode = ".sp_crds_A_0";
					}
					else
					{
						netCode = "LE";
						snapCode = ".sp_crdm_V_0";
					}
				}
				break;
			case 5:
				{
					if ( cnt < 8 ) 
					{
						netCode = "LB";
						snapCode = ".sp_crdm_A_0";
					}
					else
					{
						netCode = "LH";
						snapCode = ".sp_crds_A_1";
					}
				}
				break;
			case 6:
				{
					netCode = "LD";
					snapCode = ".sp_crds_A_0";
				}
				break;
			case 7:
				{
					netCode = "LB";
					snapCode = ".sp_crdm_A_0";
				}
				break;
			default:
				break;
			}

			Selection selNetCode( &obj );
			selNetCode.PointSelect( i );
			obj.SaveNamedSel( netCode.c_str(), &selNetCode );

			Selection selSnapCode( &obj );
			selSnapCode.PointSelect( i );
			obj.SaveNamedSel( snapCode.c_str(), &selSnapCode );
		}
	}

	// -------------------------------------------------------------------------- //

	void CrossroadModelP3D::SetOrigin()
	{
		if ( m_RoadsCount == 2 )
		{
			CalculatePositionWith2Roads();
		}
		else if ( m_RoadsCount == 3 )
		{
			CalculatePositionWith3Roads();
		}
		else
		{
			CalculatePositionWith4Roads();
		}

		for ( size_t i = 0, cnt = m_Contour.GetVerticesCount(); i < cnt; ++i )
		{
			m_Contour[i].x -= m_Position.x;
			m_Contour[i].y -= m_Position.y;
		}
		m_Contour.ResetBoundingBox();
	}

	// -------------------------------------------------------------------------- //

	void CrossroadModelP3D::CalculatePositionWith2Roads()
	{
		size_t cnt = m_Contour.GetVerticesCount();
		double dCnt1 = (double)cnt / 4.0 - 0.5;
		double dCnt2 = (double)cnt * 3.0 / 4.0 - 0.5;

		if ( abs( dCnt1 - floor( dCnt1 ) ) < EPSILON )
		{
			size_t i1 = static_cast< size_t >( dCnt1 );
			size_t i2 = static_cast< size_t >( dCnt2 );
			Shapes::DVector v12( m_Contour[i2], m_Contour[i1] );
			v12 *= 0.5;
			m_Position = v12.GetSecondEndXY( m_Contour[dCnt1] );
		}
		else
		{
			size_t iR1 = static_cast< size_t >( dCnt1 );
			size_t iR2 = iR1 + 1;
			size_t iL1 = static_cast< size_t >( dCnt2 );
			size_t iL2 = iL1 + 1;
			Shapes::DVector v12R( m_Contour[iR2], m_Contour[iR1] );
			Shapes::DVector v12L( m_Contour[iL2], m_Contour[iL1] );
			v12R *= 0.5;
			v12L *= 0.5;
			Shapes::DVector v1 = v12R.GetSecondEndXY( m_Contour[iR1] );
			Shapes::DVector v2 = v12L.GetSecondEndXY( m_Contour[iL1] );
			Shapes::DVector v12( v2, v1 );
			v12 *= 0.5;
			m_Position = v12.GetSecondEndXY( v1 );
		}
	}

	// -------------------------------------------------------------------------- //

	void CrossroadModelP3D::CalculatePositionWith3Roads()
	{
		size_t i11 = m_Contour.GetVerticesCount() - 1;
		size_t i12 = 0;
		size_t i21 = m_CrossroadResolution;
		size_t i22 = m_CrossroadResolution + 1;
		size_t i31 = 2 * m_CrossroadResolution + 1;
		size_t i32 = 2 * m_CrossroadResolution + 2;

		Shapes::DVector v1( m_Contour[i12], m_Contour[i11] );
		Shapes::DVector v2( m_Contour[i22], m_Contour[i21] );
		Shapes::DVector v3( m_Contour[i32], m_Contour[i31] );

		v1 *= 0.5;
		v2 *= 0.5;
		v3 *= 0.5;

		Shapes::DVertex midV1 = v1.GetSecondEndXY( m_Contour[i11] );
		Shapes::DVertex midV2 = v2.GetSecondEndXY( m_Contour[i21] );
		Shapes::DVertex midV3 = v3.GetSecondEndXY( m_Contour[i31] );

		Shapes::DVector l12( midV2, midV1 );
		Shapes::DVector l23(midV3, midV2);
		Shapes::DVector l31(midV1, midV3);

		double len1 = l12.SizeXY();
		double len2 = l23.SizeXY();
		double len3 = l31.SizeXY();

		double max = len1;
		Shapes::DVector v = l12;
		Shapes::DVertex vFirst = midV1;

		if ( len2 > max )
		{
			max = len2;
			v = l23;
			vFirst = midV2;
		}
		if ( len3 > max )
		{
			max = len3;
			v = l31;
			vFirst = midV3;
		}

		v *= 0.5;
		m_Position = v.GetSecondEndXY( vFirst );
	}

	// -------------------------------------------------------------------------- //

	void CrossroadModelP3D::CalculatePositionWith4Roads()
	{
		size_t i11 = m_Contour.GetVerticesCount() - 1;
		size_t i12 = 0;
		size_t i21 = m_CrossroadResolution;
		size_t i22 = m_CrossroadResolution + 1;
		size_t i31 = 2 * m_CrossroadResolution + 1;
		size_t i32 = 2 * m_CrossroadResolution + 2;
		size_t i41 = 3 * m_CrossroadResolution + 2;
		size_t i42 = 3 * m_CrossroadResolution + 3;

		Shapes::DVector v1( m_Contour[i12], m_Contour[i11] );
		Shapes::DVector v2( m_Contour[i22], m_Contour[i21] );
		Shapes::DVector v3( m_Contour[i32], m_Contour[i31] );
		Shapes::DVector v4( m_Contour[i42], m_Contour[i41] );

		v1 *= 0.5;
		v2 *= 0.5;
		v3 *= 0.5;
		v4 *= 0.5;

		Shapes::DVector midV1 = v1.GetSecondEndXY( m_Contour[i11] );
		Shapes::DVector midV2 = v2.GetSecondEndXY( m_Contour[i21] );
		Shapes::DVector midV3 = v3.GetSecondEndXY( m_Contour[i31] );
		Shapes::DVector midV4 = v4.GetSecondEndXY( m_Contour[i41] );

		Line l13 = Line( midV1, midV3 );
		Line l24 = Line( midV2, midV4 );

		Shapes::DVertex inters;
		l13.Intersection( l24, inters );

		m_Position = inters;
	}

	// -------------------------------------------------------------------------- //

	void CrossroadModelP3D::GenerateMapping( ObjectData& obj )
	{
		if ( m_Textures.size() == 2 )
		{
			RString path0 = m_TexturesPath + m_Textures[0].GetFilename() + ".tga";
			RString path1 = m_TexturesPath + m_Textures[1].GetFilename() + ".tga";
			int posDrive = path0.Find( ":" );
			path0 = path0.Substring( posDrive + 1, path0.GetLength() );
			if ( path0[0] == '\\' )
			{
				path0 = path0.Substring( 1, path0.GetLength() );
			}
			posDrive = path1.Find( ":" );
			path1 = path1.Substring( posDrive + 1, path1.GetLength() );
			if ( path1[0] == '\\' )
			{
				path1 = path1.Substring( 1, path1.GetLength() );
			}

			// here we redefine the faces to obtain a triangles' strip
			size_t counter = 0;
			for ( size_t i = 0, cnt = m_Contour.GetVerticesCount(); i < cnt - 1; ++i )
			{
				if ( i != (cnt / 2 - 1) )
				{	
					if ( i < cnt / 2 )
					{
						counter = i * 2;
					}
					else
					{
						counter = ((cnt - 2) - i) * 2 + 1;
					}

					FaceT fc( obj, counter );

					fc.SetPoint( 0, i );
					fc.SetPoint( 1, i + 1 );
					fc.SetPoint( 2, cnt - 1 - i );

					float u1;
					float u2;
					float v1;
					float v2;
					size_t halfRes = m_CrossroadResolution / 2;
					float invRes = 1.0f / m_CrossroadResolution;
					float twoInvRes = 2.0f * invRes;
					if ( i < halfRes )
					{
						u1 = i * twoInvRes;
						u2 = (i + 1) * twoInvRes;

						v1 = 1.0f;
						v2 = 0.0f;

						if ( m_RoadsEndTypes[0] == etStarting )
						{
							u1 = 1.0f - u1;
							u2 = 1.0f - u2;
							v1 = 0.0f;
							v2 = 1.0f;
						}

						fc.SetU( 0, u1 );
						fc.SetV( 0, v1 );
						fc.SetU( 1, u2 );
						fc.SetV( 1, v1 );
						fc.SetU( 2, u1 );
						fc.SetV( 2, v2 );

						fc.SetTexture( path0 );

#ifdef _VBS_ONLY_
            fc.SetMaterial( DEFAULT_RVMAT );
#endif

					}
					else if ( halfRes <= i && i < 2 * halfRes )
					{
						u1 = (i - halfRes) * twoInvRes;
						u2 = (i - halfRes + 1) * twoInvRes;

						v1 = 1.0f;
						v2 = 0.0f;

						if ( m_RoadsEndTypes[1] == etEnding )
						{
							u1 = 1.0f - u1;
							u2 = 1.0f - u2;
							v1 = 0.0f;
							v2 = 1.0f;
						}

						fc.SetU( 0, u1 );
						fc.SetV( 0, v1 );
						fc.SetU( 1, u2 );
						fc.SetV( 1, v1 );
						fc.SetU( 2, u1 );
						fc.SetV( 2, v2 );

						fc.SetTexture( path1 );

#ifdef _VBS_ONLY_
            fc.SetMaterial( DEFAULT_RVMAT );
#endif

					}
					else if ( 2 * halfRes < i && i <= 3 * halfRes )
					{
						u1 = 1.0f - (i - (2 * halfRes + 1)) * twoInvRes;
						u2 = 1.0f - (i - (2 * halfRes + 1) + 1) * twoInvRes;

						v1 = 0.0f;
						v2 = 1.0f;

						if ( m_RoadsEndTypes[1] == etEnding )
						{
							u1 = 1.0f - u1;
							u2 = 1.0f - u2;
							v1 = 1.0f;
							v2 = 0.0f;
						}

						fc.SetU( 0, u1 );
						fc.SetV( 0, v1 );
						fc.SetU( 1, u2 );
						fc.SetV( 1, v1 );
						fc.SetU( 2, u1 );
						fc.SetV( 2, v2 );

						fc.SetTexture( path1 );

#ifdef _VBS_ONLY_
            fc.SetMaterial( DEFAULT_RVMAT );
#endif

					}
					else
					{
						u1 = 1.0f - (i - (3 * halfRes + 1)) * twoInvRes;
						u2 = 1.0f - (i - (3 * halfRes + 1) + 1) * twoInvRes;

						v1 = 0.0f;
						v2 = 1.0f;

						if ( m_RoadsEndTypes[0] == etStarting )
						{
							u1 = 1.0f - u1;
							u2 = 1.0f - u2;
							v1 = 1.0f;
							v2 = 0.0f;
						}

						fc.SetU( 0, u1 );
						fc.SetV( 0, v1 );
						fc.SetU( 1, u2 );
						fc.SetV( 1, v1 );
						fc.SetU( 2, u1 );
						fc.SetV( 2, v2 );

						fc.SetTexture( path0 );

#ifdef _VBS_ONLY_
            fc.SetMaterial( DEFAULT_RVMAT );
#endif

					}

					counter++;
				}
			}
		}
		else
		{
			Selection sel( &obj );
			sel.SelectAll();
			std::pair< VecT, VecT > bbox = sel.GetBoundingBox();
			for ( int i = 0; i < obj.NFaces(); ++i )
			{
				FaceT fc( obj, i );
				for ( int j = 0; j < fc.N(); ++j )
				{
					const VecT& pos = fc.GetPointVector< VecT >( j );
					fc.SetUV( j, (pos[0] - bbox.first[0]) / (bbox.second[0] - bbox.first[0]),
                       (pos[2] - bbox.first[2]) / (bbox.second[2] - bbox.first[2]) );
				}
			}
		}
	}

	// -------------------------------------------------------------------------- //
	// RoadModelP3D
	// -------------------------------------------------------------------------- //

	RoadModelP3D::RoadModelP3D( RString filename, const Contour& contour, 
                              RString modelPath, RString texturesPath, 
                              const vector< Texture >& textures, 
                              TerminatorTypes terminator )
	: ModelP3D( filename, contour, modelPath, texturesPath, textures )
	, m_Terminator( terminator )
	{
		SetOrigin();
		OrientateAsMinBoundingBox();
	}

	// -------------------------------------------------------------------------- //

	RoadModelP3D::~RoadModelP3D()
	{
	}

	// -------------------------------------------------------------------------- //

	void RoadModelP3D::ExportToP3DFile()
	{
		LODObject lodObj;

		ObjectData* obj = new ObjectData;
		GenerateLOD( *obj, 1.0 );
		obj->SetNamedProp( "autocenter", "0" );
		obj->SetNamedProp( "class", "road" );
		obj->SetNamedProp( "map", "road" );
		lodObj.InsertLevel( obj, 1.0 );

		lodObj.DeleteLevel( 0 );

		obj = new ObjectData;
		GenerateLOD( *obj, LOD_ROADWAY );
		lodObj.InsertLevel( obj, LOD_ROADWAY );

		obj = new ObjectData;
		GenerateMemoryLOD( *obj );
		lodObj.InsertLevel( obj, LOD_MEMORY );

		Pathname path = m_ModelPath + m_Filename + ".p3d";
		if ( lodObj.Save( path, OBJDATA_LATESTVERSION, false, 0, 0 ) != 0 ) 
		{
			throw new std::exception( "Unable to save output file (p3d)" );
		}
	}

//-----------------------------------------------------------------------------

	void RoadModelP3D::GenerateLOD( ObjectData& obj, float resolution )
	{
		AutoArray< int > indices;
		AutoArray< float > uvList;

		for ( size_t i = 0, cnt = m_Contour.GetVerticesCount(); i < cnt; ++i )
		{
			obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[i].x,
                                         (Coord)0.0, 
                                         (Coord)m_Contour[i].y ) );
			indices.Add( i );
		}

		ObjToolTopology& topology = obj.GetTool< ObjToolTopology >();
		topology.TessellatePolygon( indices.Size(), indices.Data() );

    if ( resolution != LOD_ROADWAY ) { GenerateMapping( obj ); }

		obj.RecalcNormals();
	}

//-----------------------------------------------------------------------------

	void RoadModelP3D::GenerateMemoryLOD( ObjectData& obj )
	{
		size_t cnt = m_Contour.GetVerticesCount();
		obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[0].x,
                                       (Coord)0.0, 
                                       (Coord)m_Contour[0].y ) );
		obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[cnt / 2 - 1].x,
                                       (Coord)0.0, 
                                       (Coord)m_Contour[cnt / 2 - 1].y ) );
		obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[cnt / 2].x,
                                       (Coord)0.0,
                                       (Coord)m_Contour[cnt / 2].y ) );
		obj.NewPoint()->SetPoint( Vector3( (Coord)m_Contour[cnt - 1].x,
                                       (Coord)0.0, 
                                       (Coord)m_Contour[cnt - 1].y ) );

		for ( int i = 0; i < obj.NPoints(); ++i )
		{
			string netCode;
			string snapMCode;
			string snapSCode;
			switch ( i )
			{
			case 0:
				{
					netCode = "PB";
					snapMCode = ".sp_crdm_A_1";
					snapSCode = ".sp_crds_V_1";
				}
				break;
			case 1:
				{
					netCode = "PE";
					snapMCode = ".sp_crdm_V_1";
					snapSCode = ".sp_crds_A_1";
				}
				break;
			case 2:
				{
					netCode = "LE";
					snapMCode = ".sp_crdm_V_0";
					snapSCode = ".sp_crds_A_0";
				}
				break;
			case 3:
				{
					netCode = "LB";
					snapMCode = ".sp_crdm_A_0";
					snapSCode = ".sp_crds_V_0";
				}
				break;
			default:
				break;
			}

			Selection selNetCode( &obj );
			selNetCode.PointSelect( i );
			obj.SaveNamedSel( netCode.c_str(), &selNetCode );

			Selection selSnapMCode( &obj );
			selSnapMCode.PointSelect( i );
			obj.SaveNamedSel( snapMCode.c_str(), &selSnapMCode );

			Selection selSnapSCode( &obj );
			selSnapSCode.PointSelect( i );
			obj.SaveNamedSel( snapSCode.c_str(), &selSnapSCode );
		}
	}

	// -------------------------------------------------------------------------- //

	void RoadModelP3D::GenerateMapping( ObjectData& obj )
	{
		RString path;
		if ( m_Terminator == Terminator_NONE )
		{
			path = m_TexturesPath + m_Textures[0].GetFilename() + ".tga";
		}
		else
		{
			path = m_TexturesPath + m_Textures[1].GetFilename() + ".tga";
		}
		int posDrive = path.Find( ":" );
		path = path.Substring( posDrive + 1, path.GetLength() );
		if ( path[0] == '\\' )
		{
			path = path.Substring( 1, path.GetLength() );
		}

		// here we redefine the faces to obtain a triangles' strip

		Shapes::DVector vWidth( m_Contour[m_Contour.GetVerticesCount() - 1], m_Contour[0] );
		Shapes::DVector vLength( m_Contour[1], m_Contour[0] );

		if ( m_Contour.GetVerticesCount() == 4 && vWidth.SizeXY() > vLength.SizeXY() ) 
		{
			// special case:
			// only 4 vertices and the length is less than the width

			float len = (float)vLength.SizeXY() / vWidth.SizeXY();

			FaceT fc1( obj, 0 );
			fc1.SetPoint( 0, 0 );
			fc1.SetPoint( 1, 1 );
			fc1.SetPoint( 2, 3 );

			float u = 0.0f;
			float v = 1.0f;
			if ( m_Terminator == Terminator_START )
			{
				u = len - u;
				v = 0.0f;
			}
			fc1.SetU( 0, u );
			fc1.SetV( 0, v );

			u = len;
			v = 1.0f;
			if ( m_Terminator == Terminator_START )
			{
				u = len - u;
				v = 0.0f;
			}
			fc1.SetU( 1, u );
			fc1.SetV( 1, v );

			u = 0.0f;
			v = 0.0f;
			if ( m_Terminator == Terminator_START )
			{
				u = len - u;
				v = 1.0f;
			}
			fc1.SetU( 2, u );
			fc1.SetV( 2, v );

			fc1.SetTexture( path );

#ifdef _VBS_ONLY_
      fc1.SetMaterial( DEFAULT_RVMAT );
#endif

			FaceT fc2( obj, 1 );
			fc2.SetPoint( 0, 2 );
			fc2.SetPoint( 1, 3 );
			fc2.SetPoint( 2, 1 );

			u = len;
			v = 0.0f;
			if ( m_Terminator == Terminator_START )
			{
				u = len - u;
				v = 1.0f;
			}
			fc2.SetU( 0, u );
			fc2.SetV( 0, v );

			u = 0.0f;
			v = 0.0f;
			if ( m_Terminator == Terminator_START )
			{
				u = len - u;
				v = 1.0f;
			}
			fc2.SetU( 1, u );
			fc2.SetV( 1, v );

			u = len;
			v = 1.0f;
			if ( m_Terminator == Terminator_START )
			{
				u = len - u;
				v = 0.0f;
			}
			fc2.SetU( 2, u );
			fc2.SetV( 2, v );

			fc2.SetTexture( path );

#ifdef _VBS_ONLY_
      fc2.SetMaterial( DEFAULT_RVMAT );
#endif
    
    }
		else
		{
			// all other cases:

			size_t counter = 0;
			for ( size_t i = 0, cnt = m_Contour.GetVerticesCount(); i < cnt - 1; ++i )
			{
				if ( i != (cnt / 2 - 1) )
				{	
					if ( i < cnt / 2 )
					{
						counter = i * 2;
					}
					else
					{
						counter = ((cnt - 2) - i) * 2 + 1;
					}

					FaceT fc( obj, counter );

					float u = (float)m_Contour[i].m;
					float v = 0.0f;
					if ( i < cnt / 2 )
					{
						v = 1.0f;
					}
					if ( m_Terminator == Terminator_START )
					{
						u = 1.0f - u;
						if ( v == 0.0f )
						{
							v = 1.0f;
						}
						else
						{
							v = 0.0f;
						}
					}

					fc.SetPoint( 0, i );
					fc.SetU( 0, u );
					fc.SetV( 0, v );

					u = (float)m_Contour[i + 1].m;
					v = 0.0;
					if ( i < cnt / 2 )
					{
						v = 1.0;
					}
					if ( m_Terminator == Terminator_START )
					{
						u = 1.0f - u;
						if ( v == 0.0f )
						{
							v = 1.0f;
						}
						else
						{
							v = 0.0f;
						}
					}

					fc.SetPoint( 1, i + 1 );
					fc.SetU( 1, u );
					fc.SetV( 1, v );

					u = (float)m_Contour[cnt - 1 - i].m;
					v = 1.0;
					if ( i < cnt / 2 )
					{
						v = 0.0;
					}
					if ( m_Terminator == Terminator_START )
					{
						u = 1.0f - u;
						if ( v == 0.0f )
						{
							v = 1.0f;
						}
						else
						{
							v = 0.0f;
						}
					}

					fc.SetPoint( 2, cnt - 1 - i );
					fc.SetU( 2, u );
					fc.SetV( 2, v );

					fc.SetTexture( path );

#ifdef _VBS_ONLY_
          fc.SetMaterial( DEFAULT_RVMAT );
#endif

					counter++;
				}
			}
		}
	}

	// -------------------------------------------------------------------------- //

	void RoadModelP3D::SetOrigin()
	{
		size_t cnt = m_Contour.GetVerticesCount();
		double dCnt1 = (double)cnt / 4.0 - 0.5;
		double dCnt2 = (double)cnt * 3.0 / 4.0 - 0.5;

		if ( abs(dCnt1 - floor(dCnt1) ) < EPSILON )
		{
			size_t i1 = static_cast< size_t >( dCnt1 );
			size_t i2 = static_cast< size_t >( dCnt2 );
			Shapes::DVector v12( m_Contour[i2], m_Contour[i1] );
			v12 *= 0.5;
			m_Position = v12.GetSecondEndXY( m_Contour[dCnt1] );
		}
		else
		{
			size_t iR1 = static_cast< size_t >( dCnt1 );
			size_t iR2 = iR1 + 1;
			size_t iL1 = static_cast< size_t >( dCnt2 );
			size_t iL2 = iL1 + 1;
			Shapes::DVector v12R( m_Contour[iR2], m_Contour[iR1] );
			Shapes::DVector v12L( m_Contour[iL2], m_Contour[iL1] );
			v12R *= 0.5;
			v12L *= 0.5;
			Shapes::DVector v1 = v12R.GetSecondEndXY( m_Contour[iR1] );
			Shapes::DVector v2 = v12L.GetSecondEndXY( m_Contour[iL1] );
			Shapes::DVector v12( v2, v1 );
			v12 *= 0.5;
			m_Position = v12.GetSecondEndXY( v1 );
		}

		double firstM = m_Contour[0].m;
		for ( size_t i = 0, cnt = m_Contour.GetVerticesCount(); i < cnt; ++i )
		{
			m_Contour[i].x -= m_Position.x;
			m_Contour[i].y -= m_Position.y;
			m_Contour[i].m -= firstM;
		}
		m_Contour.ResetBoundingBox();
	}

	// -------------------------------------------------------------------------- //
}