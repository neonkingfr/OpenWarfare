//-----------------------------------------------------------------------------
// File: Templates.h
//
// Desc: classes and types related to road templates
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "TList.h"

//-----------------------------------------------------------------------------

#define SPEC_SUFFIX "_spec"
#define TERM_SUFFIX "_term"

//-----------------------------------------------------------------------------

typedef enum
{
  TT_Straight,
  TT_Corner,
  TT_StrTerminator,
  TT_T_RigAngCrossroad,
  TT_X_RigAngCrossroad,
  TT_T_GenAngCrossroad,
  TT_X_GenAngCrossroad
} ETemplateType;

//-----------------------------------------------------------------------------

class CTemplate
{
protected:
  // Counter of how many instances (models) uses this template.
  int m_referencesCount;

public:
  CTemplate();
  CTemplate( const CTemplate& other );
  virtual ~CTemplate();

  // Reference count manipulators
  // {
  void IncreaseReferencesCount();
  void DecreaseReferencesCount();
  // }

  // Getters
  // {
  int GetReferencesCount() const;
  // }

  // Returns the filename of this template (extension = p3d)
  String Filename() const;

  // Returns the type of this template
  virtual ETemplateType Type() const = 0;

  // Returns the name of this template
  virtual String Name() const = 0;

  // Exports this part template to a p3d file.
  // Returns true if successfull.
  virtual bool ExportP3D( const String& destPath ) const = 0;

  // Returns the geometric center (the center of the axis oriented
  // bounding box) of this template
  virtual CGPoint2 GeometricCenter() const = 0;

protected:
  // Generates a Lod to be saved in the p3d file.
  virtual void GenerateP3DLod( ObjectData& obj, float resolution, const String& destPath ) const = 0;

private:
  CTemplate& operator = ( const CTemplate& other );
};

//-----------------------------------------------------------------------------

class CRoadBaseTemplate : public CTemplate
{
protected:
  // The various part composing the footprint of this template.
  vector< CGPolygon2 > m_geometry;

  // The filenames of the textures associated with this template.
  vector< string > m_textures;

public:
  CRoadBaseTemplate();
  CRoadBaseTemplate( const CRoadBaseTemplate& other );
  virtual ~CRoadBaseTemplate();

  // Getters
  // {
  const vector< CGPolygon2 >& GetGeometry() const;
  const vector< string >& GetTextures() const;
  // }

  // Comparison
  virtual bool Equals( const CRoadBaseTemplate& other ) const;

  // CTemplate implementation
  // {
  virtual CGPoint2 GeometricCenter() const;
  // }

protected:
  // Initializes this template
  // This method must be called by children of this class in their constructor
  virtual void Initialize();

  // Calculates the geometry of this template
  virtual void CalcGeometry() = 0;

private:
  CRoadBaseTemplate& operator = ( const CRoadBaseTemplate& other );
};

//-----------------------------------------------------------------------------

class CRoadType;

//-----------------------------------------------------------------------------

class CRoadPartTemplate : public CRoadBaseTemplate
{
protected:
  // pointer to the road type of this road part template
  const CRoadType* m_roadType;

  // does this road part template use a special texture ?
  bool m_special;

  // the vector connecting the endpoints of the centerline of the
  // road part template
  CGVector2 m_chord;

  // the vector connecting the endpoints of the centerline of the
  // road part template when it used inverted
  CGVector2 m_invertedChord;

  // how much this road part template changes the road direction
  Float m_deltaDirection;

  // the resolution of this road part template
  size_t m_resolution;

public:
  CRoadPartTemplate( const CRoadType* roadType, const string& texture = "",
                     bool special = false );
  CRoadPartTemplate( const CRoadPartTemplate& other );
  virtual ~CRoadPartTemplate();

  // Getters
  // {
  const CRoadType* GetRoadType() const;
  const CGVector2& GetChord() const;
  const CGVector2& GetInvertedChord() const;
  Float GetDeltaDirection() const;
  bool IsSpecial() const;
  // }

  // CTemplate implementation
  // {
  virtual bool ExportP3D( const String& destPath ) const;
protected:
  virtual void GenerateP3DLod( ObjectData& obj, float resolution, const String& destPath ) const;
  // }

  // CRoadBaseTemplate override
  // {
public:
  virtual bool Equals( const CRoadBaseTemplate& other ) const;
  virtual void Initialize();
  // }

  // Returns the length of this road part template. 
  // For corner part templates it is the length of the arc.
  virtual Float Length() const = 0;

protected:
  // Calculates the chords of this road part template.
  virtual void CalcChords() = 0;

  // Calculates the variation of direction due to this roda part template.
  virtual void CalcDeltaDirection() = 0;

  // Calculates the resolution of the road part template.
  void CalcResolution();

private:
  CRoadPartTemplate& operator = ( const CRoadPartTemplate& other );
};

//-----------------------------------------------------------------------------

class CStraightTemplate : public CRoadPartTemplate
{
protected:
  // the length of this straight template
  Float m_length;

public:
  CStraightTemplate( const CRoadType* roadType, Float length,
                     const string& texture = "", bool special = false );
  CStraightTemplate( const CStraightTemplate& other );
  virtual ~CStraightTemplate();

  // CTemplate implementation
  // {
  virtual ETemplateType Type() const;
  virtual String Name() const;
  // }

  // CRoadBaseTemplate implementation
  // {
protected:
  virtual void CalcGeometry();
  // }

  // CRoadPartTemplate implementation
  // {
public:
  virtual Float Length() const;
protected:
  virtual void CalcChords();
  virtual void CalcDeltaDirection();
  // }

private:
  CStraightTemplate& operator = ( const CStraightTemplate& other );
};

//-----------------------------------------------------------------------------

class CCornerTemplate : public CRoadPartTemplate
{
protected:
  // the radius of this corner template
  Float m_radius;

  // the angle of this corner template
  Float m_angle;

public:
  CCornerTemplate( const CRoadType* roadType, Float radius, Float angle, 
                   const string& texture = "", bool special = false );
  CCornerTemplate( const CCornerTemplate& other );
  virtual ~CCornerTemplate();

  // Getters
  // {
  Float GetRadius() const;
  Float GetAngle() const;
  // }

  // CTemplate implementation
  // {
  virtual ETemplateType Type() const;
  virtual String Name() const;
  // }

  // CRoadBaseTemplate implementation
  // {
protected:
  virtual void CalcGeometry();
  // }

  // CRoadPartTemplate implementation
  // {
public:
  virtual Float Length() const;
protected:
  virtual void CalcChords();
  virtual void CalcDeltaDirection();
  // }

private:
  CCornerTemplate& operator = ( const CCornerTemplate& other );
};

//-----------------------------------------------------------------------------

class CStrTerminatorTemplate : public CStraightTemplate
{
public:
  CStrTerminatorTemplate( const CRoadType* roadType, Float length,
                          const string& texture = "", bool special = false );
  CStrTerminatorTemplate( const CStrTerminatorTemplate& other );
  virtual ~CStrTerminatorTemplate();

  // CTemplate implementation
  // {
  virtual ETemplateType Type() const;
  virtual String Name() const;
  // }

private:
  CStrTerminatorTemplate& operator = ( const CStrTerminatorTemplate& other );
};

//-----------------------------------------------------------------------------

class CRoadsSet;

//-----------------------------------------------------------------------------

class CCrossroadTemplate : public CRoadBaseTemplate
{
protected:
  // Pointer to the roads which arrive in the crossroad
  // represented by this template.
  CRoadsSet* m_roads;

  // the radius of this crossroad template
  Float m_radius;

public:
  CCrossroadTemplate( CRoadsSet& roads, Float radius );
  CCrossroadTemplate( const CCrossroadTemplate& other );
  virtual ~CCrossroadTemplate();

  // CRoadBaseTemplate override
  // {
  virtual bool Equals( const CRoadBaseTemplate& other ) const;
  // }

  // Getters
  // {
  const CRoadsSet* GetRoadSet() const;
  // }

private:
  CCrossroadTemplate& operator = ( const CCrossroadTemplate& other );
};

//-----------------------------------------------------------------------------

class CRightAnglesCrossroadTemplate : public CCrossroadTemplate
{
public:
  CRightAnglesCrossroadTemplate( CRoadsSet& roads, Float radius );
  CRightAnglesCrossroadTemplate( const CRightAnglesCrossroadTemplate& other );
  virtual ~CRightAnglesCrossroadTemplate();

  // CTemplate implementation
  // {
  virtual bool ExportP3D( const String& destPath ) const;
protected:
  virtual void GenerateP3DLod( ObjectData& obj, float resolution, const String& destPath ) const;
  // }

  // CRoadBaseTemplate override
  // {
  virtual bool Equals( const CRoadBaseTemplate& other ) const;
  // }

private:
  CRightAnglesCrossroadTemplate& operator = ( const CRightAnglesCrossroadTemplate& other );
};

//-----------------------------------------------------------------------------

class CRoad;

//-----------------------------------------------------------------------------

class CGenericAnglesCrossroadTemplate : public CCrossroadTemplate
{
protected:
  // The angles between the incoming roads
  vector< Float > m_angles;

  // the resolution of this road part template
  size_t m_resolution;

public:
  CGenericAnglesCrossroadTemplate( CRoadsSet& roads, Float radius );
  CGenericAnglesCrossroadTemplate( const CGenericAnglesCrossroadTemplate& other );
  virtual ~CGenericAnglesCrossroadTemplate();

  // CTemplate implementation
  // {
  virtual bool ExportP3D( const String& destPath ) const;
protected:
  virtual void GenerateP3DLod( ObjectData& obj, float resolution, const String& destPath ) const;
  // }

  // CRoadBaseTemplate override
  // {
public:
  virtual bool Equals( const CRoadBaseTemplate& other ) const;
  // }

protected:
  Float MaxAngle() const;
  int MaxAngleIndex() const;

  vector< CGPolygon2 > GenerateCorner( const CRoad* lowerRoad, const CRoad* upperRoad,
                                       Float angleBetweenRoads );

  void RotateAngles( size_t rotationsCount );

private:
  CGenericAnglesCrossroadTemplate& operator = ( const CGenericAnglesCrossroadTemplate& other );
};

//-----------------------------------------------------------------------------

//          N
//       |-----|
//       |     |
//   |---|     |
// W |         |
//   |---|     |
//       |     |
//       |-----|
//          S

class CTRightAnglesCrossroadTemplate : public CRightAnglesCrossroadTemplate
{
public:
  CTRightAnglesCrossroadTemplate( CRoadsSet& roads, Float radius );
  CTRightAnglesCrossroadTemplate( const CTRightAnglesCrossroadTemplate& other );
  virtual ~CTRightAnglesCrossroadTemplate();

  // CRoadBaseTemplate override
  // {
  virtual bool Equals( const CRoadBaseTemplate& other ) const;
  // }

  // CTemplate implementation
  // {
  virtual ETemplateType Type() const;
  virtual String Name() const;
  // }

  // CRoadBaseTemplate implementation
  // {
protected:
  virtual void CalcGeometry();
  // }

private:
  CTRightAnglesCrossroadTemplate& operator = ( const CTRightAnglesCrossroadTemplate& other );
};

//-----------------------------------------------------------------------------

//          N
//       |-----|
//       |     |
//   |---|     |---|
// W |             | E
//   |---|     |---|
//       |     |
//       |-----|
//          S

class CXRightAnglesCrossroadTemplate : public CRightAnglesCrossroadTemplate
{
public:
  CXRightAnglesCrossroadTemplate( CRoadsSet& roads, Float radius );
  CXRightAnglesCrossroadTemplate( const CXRightAnglesCrossroadTemplate& other );
  virtual ~CXRightAnglesCrossroadTemplate();

  // CRoadBaseTemplate override
  // {
  virtual bool Equals( const CRoadBaseTemplate& other ) const;
  // }

  // CTemplate implementation
  // {
  virtual ETemplateType Type() const;
  virtual String Name() const;
  // }

  // CRoadBaseTemplate implementation
  // {
protected:
  virtual void CalcGeometry();
  // }

private:
  CXRightAnglesCrossroadTemplate& operator = ( const CXRightAnglesCrossroadTemplate& other );
};

//-----------------------------------------------------------------------------

//          N
//       |-----|
//     / |     |
//  W / \|     |
//   /         |
//    \        |
//     \       |
//      \      |
//       |     |
//       |-----|
//          S

class CTGenericAnglesCrossroadTemplate : public CGenericAnglesCrossroadTemplate
{
public:
  CTGenericAnglesCrossroadTemplate( CRoadsSet& roads, Float radius );
  CTGenericAnglesCrossroadTemplate( const CTGenericAnglesCrossroadTemplate& other );
  virtual ~CTGenericAnglesCrossroadTemplate();

  // CRoadBaseTemplate override
  // {
  virtual bool Equals( const CRoadBaseTemplate& other ) const;
  // }

  // CTemplate implementation
  // {
  virtual ETemplateType Type() const;
  virtual String Name() const;
  // }

  // CRoadBaseTemplate implementation
  // {
protected:
  virtual void CalcGeometry();
  // }

private:
  CTGenericAnglesCrossroadTemplate& operator = ( const CTGenericAnglesCrossroadTemplate& other );
};

//-----------------------------------------------------------------------------

//          N
//       |-----|
//     / |     | \
//  W / \|     |/ \ E
//   /             \
//    \           /
//     \         /
//      \       /
//       |     |
//       |-----|
//          S

class CXGenericAnglesCrossroadTemplate : public CGenericAnglesCrossroadTemplate
{
public:
  CXGenericAnglesCrossroadTemplate( CRoadsSet& roads, Float radius );
  CXGenericAnglesCrossroadTemplate( const CXGenericAnglesCrossroadTemplate& other );
  virtual ~CXGenericAnglesCrossroadTemplate();

  // CRoadBaseTemplate override
  // {
  virtual bool Equals( const CRoadBaseTemplate& other ) const;
  // }

  // CTemplate implementation
  // {
  virtual ETemplateType Type() const;
  virtual String Name() const;
  // }

  // CRoadBaseTemplate implementation
  // {
protected:
  virtual void CalcGeometry();
  // }

private:
  CXGenericAnglesCrossroadTemplate& operator = ( const CXGenericAnglesCrossroadTemplate& other );
};

//-----------------------------------------------------------------------------

class CCrossroadTemplatesLibrary : public TList< CCrossroadTemplate >
{
public:
  // TList override
  // {
  CCrossroadTemplate* Append( const CCrossroadTemplate& crossroadTemplate );
  // }
};

//-----------------------------------------------------------------------------

class CRoadPartTemplatesLibrary : public TList< CRoadPartTemplate >
{
public:
  // TList override
  // {
  CRoadPartTemplate* Append( const CRoadPartTemplate& roadPartTemplate );
  // }

  CRoadPartTemplate* FindDoubled( const CRoadPartTemplate& roadPartTemplate );

  void RemoveUnused();

  const CRoadPartTemplate* GetTerminator() const;
  CRoadPartTemplate* GetTerminator();

  const CRoadPartTemplate* GetSquareStraight() const;
  CRoadPartTemplate* GetSquareStraight();

  vector< const CRoadPartTemplate* > GetAllStraights() const;
  vector< CRoadPartTemplate* > GetAllStraights();

  vector< const CRoadPartTemplate* > GetAllCornersWithRadius( Float radius ) const;
  vector< CRoadPartTemplate* > GetAllCornersWithRadius( Float radius );
};

//-----------------------------------------------------------------------------

