//-----------------------------------------------------------------------------
// File: Crossroads.cpp
//
// Desc: classes and types related to crossroads
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Crossroads.h"

//-----------------------------------------------------------------------------

CJoint::CJoint( const CGPoint2& position, CRoad* firstRoad )
: m_position( position )
{
	m_roads.Add( firstRoad );
}

//-----------------------------------------------------------------------------

CJoint::CJoint( const CJoint& other )
: m_position( other.m_position )
{
	for ( size_t i = 0, cntI = other.m_roads.Size(); i < cntI; ++i )
	{
		m_roads.Add( const_cast< CRoad* >( other.m_roads.Get( i ) ) );
	}
}

//-----------------------------------------------------------------------------

CJoint::~CJoint()
{
}

//-----------------------------------------------------------------------------

const CGPoint2& CJoint::GetPosition() const
{
	return (m_position);
}

//-----------------------------------------------------------------------------

const CRoadsSet& CJoint::GetRoads() const
{
	return (m_roads);
}

//-----------------------------------------------------------------------------

CRoadsSet& CJoint::GetRoads()
{
	return (m_roads);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CNullJoint::CNullJoint( const CGPoint2& position, CRoad* firstRoad )
: CJoint( position, firstRoad )
, m_matched( false )
{
}

//-----------------------------------------------------------------------------

CNullJoint::CNullJoint( const CNullJoint& other )
: CJoint( other )
, m_matched( other.m_matched )
{
}

//-----------------------------------------------------------------------------

CNullJoint::~CNullJoint()
{
}

//-----------------------------------------------------------------------------

bool CNullJoint::IsMatched() const
{
	return (m_matched);
}

//-----------------------------------------------------------------------------

void CNullJoint::BuildRoads()
{
	for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
	{
		CRoad* road = m_roads.Get( i );
    if ( !road->IsBuilt() && !road->IsDegenerated() )
		{
			if ( road->GetEndJoint() != this ) { road->ReverseVerticesOrder(); }
			road->BuildModels();
		}
	}

	if ( m_roads.Get( 0 ) == m_roads.Get( 1 ) )
	{
		// loop road
		m_matched = MatchLoopRoad();
	}
	else
	{
		m_matched = MatchRoads();
	}

	if ( m_matched ) { FillGaps(); }
}

//-----------------------------------------------------------------------------

EJointType CNullJoint::Type() const
{
	return (JT_NullJoint);
}

//-----------------------------------------------------------------------------

int indexInListLength( const vector< CRoadPartTemplate* >& list, Float length )
{
  for ( size_t i = 0, cntI = list.size(); i < cntI; ++i )
  {
    if ( list[i]->Length() == length ) { return (static_cast< int >( i )); }
  }

  return (-1);
}

//-----------------------------------------------------------------------------

int indexInListAngle( const vector< CRoadPartTemplate* >& list, Float angle )
{
  for ( size_t i = 0, cntI = list.size(); i < cntI; ++i )
  {
    if ( reinterpret_cast< CCornerTemplate* >( list[i] )->GetAngle() == angle ) { return (static_cast< int >( i )); }
  }

  return (-1);
}

//-----------------------------------------------------------------------------

Float replaceBetterStraight( CRoad* road, bool isFirstRoad, int partIndex, const CGVector2& dirOtherRoad, const CGPoint2& endOtherRoad, 
                             const CGVector2& normal, Float threshold )
{
  vector< CRoadPartModel >& models = road->GetModels();

  int size = static_cast< int >( models.size() ); 
  int last = size - 1;

  CGVector2 oldErrorVec = isFirstRoad ? CGVector2( endOtherRoad, models[last].GetNextPosition() ) : CGVector2( models[last].GetNextPosition(), endOtherRoad );
  Float oldErrorOnNorm = normal.Dot( oldErrorVec );

  if ( (partIndex < 0) || (last < partIndex) )
  {
    return (oldErrorOnNorm);
  }

  // gets the list of straight templates from the road type
  vector< CRoadPartTemplate* > straightTempls = road->GetType()->GetTemplates().GetAllStraights();

  // if there is only one template, there is nothing to replace
  if ( straightTempls.size() < 2 ) 
  {
    return (oldErrorOnNorm);
  }

  CGPoint2 startPos  = models[partIndex].GetCreationPosition();
  Float    startDir  = models[partIndex].GetCreationDirection();
  Float    oldLength = reinterpret_cast< CRoadPartTemplate* >( models[partIndex].GetTemplate() )->Length();
  int oldId = indexInListLength( straightTempls, oldLength );
  if ( oldId < 0 ) 
  { 
    return (oldErrorOnNorm); 
  }

  CGVector2 currErrorVec    = oldErrorVec;
  Float     currErrorOnNorm = oldErrorOnNorm;

  for ( size_t i = 0, cntI = straightTempls.size(); i < cntI; ++i )
  {
    if ( i != static_cast< size_t >( oldId ) )
    {
      // The temporary road starts with the new straight model
      CRoad tempRoad( road->GetType(), false );
      vector< CRoadPartModel >& tempModels = tempRoad.GetModels();
      CRoadPartModel tempModel( straightTempls[i], startPos, startDir );
      tempModels.push_back( tempModel );

      // Then continues with the remaining models on the original road
      size_t k = 0;
      for ( int j = partIndex + 1; j <= last; ++j )
      {
        CRoadPartModel newModel( models[j] );
        newModel.Redefine( tempModels[k].GetNextPosition(), tempModels[k].GetNextDirection() ); 
        tempModels.push_back( newModel );
        ++k;
      }

      // Now we have to check if the temporary roads gives a better solution than the current one
      const CRoadPartModel* lastModel = tempRoad.LastModel();

      CGVector2 newDir      = isFirstRoad ? CGVector2( lastModel->GetNextDirection() ) : -CGVector2( lastModel->GetNextDirection() );
      CGPoint2  newEnd      = lastModel->GetNextPosition();
      CGVector2 newErrorVec = isFirstRoad ? CGVector2( endOtherRoad, newEnd ) : CGVector2( newEnd, endOtherRoad );

      Float newErrorOnNorm = normal.Dot( newErrorVec );

      // proceed only if we are reducing the error
      if ( abs( newErrorOnNorm ) < abs( currErrorOnNorm ) )
      {
        // proceed only if there is no overlap
        if ( newDir.Dot( newErrorVec ) > ZERO )
        {
          road->ReplaceModel( partIndex, tempModel );
          if ( abs( newErrorOnNorm ) <= threshold ) 
          {
            return (newErrorOnNorm);
          }         
          currErrorVec    = newErrorVec;
          currErrorOnNorm = newErrorOnNorm;
        }
      }
    }
  }

  return (oldErrorOnNorm);
}

//-----------------------------------------------------------------------------

bool tryReplacingStraights( CRoad* road0, CRoad* road1, const CGVector2& normal, Float threshold, bool discardChanged, size_t algorithm )
{
  if ( !road0 || !road1 ) { return (false); }

  vector< CRoadPartModel >& models0 = road0->GetModels();
  vector< CRoadPartModel >& models1 = road1->GetModels();

  // backup copies of the models
  vector< CRoadPartModel > backModels0 = models0;
  vector< CRoadPartModel > backModels1 = models1;

  int size0 = static_cast< int >( models0.size() ); 
  int size1 = static_cast< int >( models1.size() ); 
  int last0 = size0 - 1;
  int last1 = size1 - 1;

  CGVector2 oldDir0 = CGVector2( models0[last0].GetNextDirection() );
  CGPoint2  oldEnd0 = models0[last0].GetNextPosition();
  CGVector2 oldDir1 = CGVector2( models1[last1].GetNextDirection() );
  CGPoint2  oldEnd1 = models1[last1].GetNextPosition();

  Float oldCompOnNorm = normal.Dot( CGVector2( oldEnd1, oldEnd0 ) );

  Float currCompOnNorm = oldCompOnNorm;

  switch ( algorithm )
  {
  default:
  case 0:
    {
      // we will move along the first road only, starting from the matching zone, searching
      // for its straight models, and, if any, we will try to replace them with straights with
      // different length
      int it0 = last0;
      do 
      {
        // goes to next straight model on the road, if any
        while ( (it0 >= 0) && (models0[it0].GetTemplate()->Type() != TT_Straight) )
        {
          --it0;
        }

        if ( (0 <= it0) && (it0 <= last0) )
        {
          // straight found, try to replace it with another
          Float newCompOnNorm = replaceBetterStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it0;
      }
      while ( it0 >= 0 );

      // at this point the first road could have been modified
      oldDir0 = CGVector2( models0[last0].GetNextDirection() );
      oldEnd0 = models0[last0].GetNextPosition();

      // we will now move along the 2nd road only, starting from the matching zone, searching
      // for its straight models, and, if any, we will try to replace them with straights with
      // different length
      int it1 = last1;
      do 
      {
        // goes to next straight model on the road, if any
        while ( (it1 >= 0) && (models1[it1].GetTemplate()->Type() != TT_Straight) )
        {
          --it1;
        }

        if ( (0 <= it1) && (it1 <= last1) )
        {
          // straight found, try to replace it with another
          Float newCompOnNorm = replaceBetterStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it1;
      }
      while ( it1 >= 0 );

      break;
    }
  case 1:
    {
      // we will move along the both roads, starting from the matching zone, searching
      // for their straight models, and, if any, we will try to replace them with straights with
      // different length
      int it0 = last0;
      int it1 = last1;
      do 
      {
        // goes to next straight model on the first road, if any
        while ( (it0 >= 0) && (models0[it0].GetTemplate()->Type() != TT_Straight) )
        {
          --it0;
        }

        // goes to next straight model on the 2nd road, if any
        while ( (it1 >= 0) && (models1[it1].GetTemplate()->Type() != TT_Straight) )
        {
          --it1;
        }

        // the 2nd road could have been changed in previous iteration
        oldDir1 = CGVector2( models1[last1].GetNextDirection() );
        oldEnd1 = models1[last1].GetNextPosition();

        // first road first
        if ( (0 <= it0) && (it0 <= last0) )
        {
          // straight found, try to replace it with another
          Float newCompOnNorm = replaceBetterStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        // the first road could have been changed 
        oldDir0 = CGVector2( models0[last0].GetNextDirection() );
        oldEnd0 = models0[last0].GetNextPosition();

        // then 2nd road 
        if ( (0 <= it1) && (it1 <= last1) )
        {
          // straight found, try to replace it with another
          Float newCompOnNorm = replaceBetterStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it0;
        --it1;
      }
      while ( (it0 >= 0) || (it1 >= 0) );

      break;
    }
  case 2:
    {
      // we will move along the both roads, starting from the matching zone, searching
      // for their straight models, and, if any, we will try to replace them with straights with
      // different length. At any changes we restart from the matching zone for both roads
      int it0 = last0;
      int it1 = last1;
      do 
      {
        // goes to next straight model on the first road, if any
        while ( (it0 >= 0) && (models0[it0].GetTemplate()->Type() != TT_Straight) )
        {
          --it0;
        }

        // goes to next straight model on the 2nd road, if any
        while ( (it1 >= 0) && (models1[it1].GetTemplate()->Type() != TT_Straight) )
        {
          --it1;
        }

        // the 2nd road could have been changed in previous iteration
        oldDir1 = CGVector2( models1[last1].GetNextDirection() );
        oldEnd1 = models1[last1].GetNextPosition();

        // first road first
        if ( (0 <= it0) && (it0 <= last0) )
        {
          // straight found, try to replace it with another
          Float newCompOnNorm = replaceBetterStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
            it0 = last0;
            it1 = last1;
            continue;
          }
        }

        // the first road could have been changed 
        oldDir0 = CGVector2( models0[last0].GetNextDirection() );
        oldEnd0 = models0[last0].GetNextPosition();

        // then 2nd road 
        if ( (0 <= it1) && (it1 <= last1) )
        {
          // straight found, try to replace it with another
          Float newCompOnNorm = replaceBetterStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
            it0 = last0;
            it1 = last1;
            continue;
          }
        }

        --it0;
        --it1;
      }
      while ( (it0 >= 0) || (it1 >= 0) );

      break;
    }
  }

  if ( discardChanged )
  {
    // restores old models
    models0 = backModels0;
    models1 = backModels1;
  }

  return (false);
}

//-----------------------------------------------------------------------------

Float replaceBetterCorner( CRoad* road, bool isFirstRoad, int partIndex, const CGVector2& dirOtherRoad, const CGPoint2& endOtherRoad, 
                           const CGVector2& normal, Float threshold )
{
  vector< CRoadPartModel >& models = road->GetModels();

  int size = static_cast< int >( models.size() ); 
  int last = size - 1;

  CGVector2 oldErrorVec = isFirstRoad ? CGVector2( endOtherRoad, models[last].GetNextPosition() ) : CGVector2( models[last].GetNextPosition(), endOtherRoad );
  Float oldErrorOnNorm = normal.Dot( oldErrorVec );

  Float radius     = reinterpret_cast< CCornerTemplate* >( models[partIndex].GetTemplate() )->GetRadius();
  bool  isInverted = models[partIndex].IsInverted();

  // gets the list of corner templates matching the radius from the road type
  vector< CRoadPartTemplate* > cornerTempls = road->GetType()->GetTemplates().GetAllCornersWithRadius( radius );

  // if there is only one template, there is nothing to replace
  if ( cornerTempls.size() < 2 ) 
  {
    return (oldErrorOnNorm);
  }

  CGPoint2 startPos = models[partIndex].GetCreationPosition();
  Float    startDir = models[partIndex].GetCreationDirection();
  Float    oldAngle = reinterpret_cast< CCornerTemplate* >( models[partIndex].GetTemplate() )->GetAngle();
  int oldId = indexInListAngle( cornerTempls, oldAngle );
  if ( oldId < 0 ) 
  { 
    return (oldErrorOnNorm); 
  }

  CGVector2 currErrorVec     = oldErrorVec;
  Float     currErrorOnNorm  = oldErrorOnNorm;
  Float     currCos          = dirOtherRoad.Dot( CGVector2( models[last].GetNextDirection() ) );

  for ( size_t i = 0, cntI = cornerTempls.size(); i < cntI; ++i )
  {
    if ( i != static_cast< size_t >( oldId ) )
    {
      // The temporary road starts with the new corner model
      CRoad tempRoad( road->GetType(), false );
      vector< CRoadPartModel >& tempModels = tempRoad.GetModels();
      CRoadPartModel tempModel( cornerTempls[i], startPos, startDir, isInverted );
      tempModels.push_back( tempModel );

      // Then continues with the remaining models on the original road
      size_t k = 0;
      for ( int j = partIndex + 1; j <= last; ++j )
      {
        CRoadPartModel newModel( models[j] );
        newModel.Redefine( tempModels[k].GetNextPosition(), tempModels[k].GetNextDirection() ); 
        tempModels.push_back( newModel );
        ++k;
      }

      // Now we have to check if the temporary roads gives a better solution than the current one
      const CRoadPartModel* lastModel = tempRoad.LastModel();

      CGVector2 newDir      = isFirstRoad ? CGVector2( lastModel->GetNextDirection() ) : -CGVector2( lastModel->GetNextDirection() );
      CGPoint2  newEnd      = lastModel->GetNextPosition();
      CGVector2 newErrorVec = isFirstRoad ? CGVector2( endOtherRoad, newEnd ) : CGVector2( newEnd, endOtherRoad );

      Float newErrorOnNorm = normal.Dot( newErrorVec );

      // proceed only if we are reducing the error
      if ( abs( newErrorOnNorm ) < abs( currErrorOnNorm ) )
      {
          // proceed only if we are reducing the difference of directions
          // (we should go toward cos = -1 which means aligned roads)
          if ( dirOtherRoad.Dot( newDir ) <= currCos )
          {
            // proceed only if there is no overlap
            if ( newDir.Dot( newErrorVec ) > ZERO )
            {
              road->ReplaceModel( partIndex, tempModel );
              if ( abs( newErrorOnNorm ) <= threshold ) 
              {
                return (newErrorOnNorm);
              }
              currErrorVec    = newErrorVec;
              currErrorOnNorm = newErrorOnNorm;
              currCos         = dirOtherRoad.Dot( CGVector2( models[last].GetNextDirection() ) );
            }
          }
      }
    }
  }

  return (oldErrorOnNorm);
}

//-----------------------------------------------------------------------------

bool tryReplacingCorners( CRoad* road0, CRoad* road1, const CGVector2& normal, Float threshold, bool discardChanged, size_t algorithm )
{
  if ( !road0 || !road1 ) { return (false); }

  vector< CRoadPartModel >& models0 = road0->GetModels();
  vector< CRoadPartModel >& models1 = road1->GetModels();

  // backup copies of the models
  vector< CRoadPartModel > backModels0 = models0;
  vector< CRoadPartModel > backModels1 = models1;

  int size0 = static_cast< int >( models0.size() ); 
  int size1 = static_cast< int >( models1.size() ); 
  int last0 = size0 - 1;
  int last1 = size1 - 1;

  CGVector2 oldDir0 = CGVector2( models0[last0].GetNextDirection() );
  CGPoint2  oldEnd0 = models0[last0].GetNextPosition();
  CGVector2 oldDir1 = CGVector2( models1[last1].GetNextDirection() );
  CGPoint2  oldEnd1 = models1[last1].GetNextPosition();

  Float oldCompOnNorm = normal.Dot( CGVector2( oldEnd1, oldEnd0 ) );

  Float currCompOnNorm = oldCompOnNorm;

  switch ( algorithm )
  {
  default:
  case 0:
    {
      // we will move along the first road only, starting from the matching zone, searching
      // for its corner models, and, if any, we will try to replace them with corner with
      // the same radius and different length
      int it0 = last0;
      do 
      {
        // goes to next corner model on the road, if any
        while ( (it0 >= 0) && (models0[it0].GetTemplate()->Type() != TT_Corner) )
        {
          --it0;
        }

        if ( (0 <= it0) && (it0 <= last0) )
        {
          // corner found, try to replace it with another
          Float newCompOnNorm = replaceBetterCorner( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it0;
      }
      while ( it0 >= 0 );

      // at this point the first road could have been modified
      oldDir0 = CGVector2( models0[last0].GetNextDirection() );
      oldEnd0 = models0[last0].GetNextPosition();

      // we will now move along the 2nd road only, starting from the matching zone, searching
      // for its corner models, and, if any, we will try to replace them with corners with
      // the same radius and different length
      int it1 = last1;
      do 
      {
        // goes to next corner model on the road, if any
        while ( (it1 >= 0) && (models1[it1].GetTemplate()->Type() != TT_Corner) )
        {
          --it1;
        }

        if ( (0 <= it1) && (it1 <= last1) )
        {
          // corner found, try to replace it with another
          Float newCompOnNorm = replaceBetterCorner( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it1;
      }
      while ( it1 >= 0 );

      break;
    }
  case 1:
    {
      // we will move along the both roads, starting from the matching zone, searching
      // for their corner models, and, if any, we will try to replace them with corners with
      // the same radius and different length
      int it0 = last0;
      int it1 = last1;
      do 
      {
        // goes to next straight model on the first road, if any
        while ( (it0 >= 0) && (models0[it0].GetTemplate()->Type() != TT_Corner) )
        {
          --it0;
        }

        // goes to next straight model on the 2nd road, if any
        while ( (it1 >= 0) && (models1[it1].GetTemplate()->Type() != TT_Corner) )
        {
          --it1;
        }

        // the 2nd road could have been changed in previous iteration
        oldDir1 = CGVector2( models1[last1].GetNextDirection() );
        oldEnd1 = models1[last1].GetNextPosition();

        // first road first
        if ( (0 <= it0) && (it0 <= last0) )
        {
          // corner found, try to replace it with another
          Float newCompOnNorm = replaceBetterCorner( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        // the first road could have been changed 
        oldDir0 = CGVector2( models0[last0].GetNextDirection() );
        oldEnd0 = models0[last0].GetNextPosition();

        // then 2nd road 
        if ( (0 <= it1) && (it1 <= last1) )
        {
          // corner found, try to replace it with another
          Float newCompOnNorm = replaceBetterCorner( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it0;
        --it1;
      }
      while ( (it0 >= 0) || (it1 >= 0) );

      break;
    }
  case 2:
    {
      // we will move along the both roads, starting from the matching zone, searching
      // for their corner models, and, if any, we will try to replace them with corners with
      // the same radius and different length.
      // At any changes we restart from the matching zone for both roads
      int it0 = last0;
      int it1 = last1;
      do 
      {
        // goes to next corner model on the first road, if any
        while ( (it0 >= 0) && (models0[it0].GetTemplate()->Type() != TT_Corner) )
        {
          --it0;
        }

        // goes to next corner model on the 2nd road, if any
        while ( (it1 >= 0) && (models1[it1].GetTemplate()->Type() != TT_Corner) )
        {
          --it1;
        }

        // the 2nd road could have been changed in previous iteration
        oldDir1 = CGVector2( models1[last1].GetNextDirection() );
        oldEnd1 = models1[last1].GetNextPosition();

        // first road first
        if ( (0 <= it0) && (it0 <= last0) )
        {
          // corner found, try to replace it with another
          Float newCompOnNorm = replaceBetterCorner( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
            it0 = last0;
            it1 = last1;
            continue;
          }
        }

        // the first road could have been changed 
        oldDir0 = CGVector2( models0[last0].GetNextDirection() );
        oldEnd0 = models0[last0].GetNextPosition();

        // then 2nd road 
        if ( (0 <= it1) && (it1 <= last1) )
        {
          // corner found, try to replace it with another
          Float newCompOnNorm = replaceBetterCorner( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
            it0 = last0;
            it1 = last1;
            continue;
          }
        }

        --it0;
        --it1;
      }
      while ( (it0 >= 0) || (it1 >= 0) );

      break;
    }
  }

  if ( discardChanged )
  {
    // restores old models
    models0 = backModels0;
    models1 = backModels1;
  }
  return (false);
}

//-----------------------------------------------------------------------------

bool tryReplacingAll( CRoad* road0, CRoad* road1, const CGVector2& normal, Float threshold, bool discardChanged, size_t algorithm )
{
  if ( !road0 || !road1 ) { return (false); }

  vector< CRoadPartModel >& models0 = road0->GetModels();
  vector< CRoadPartModel >& models1 = road1->GetModels();

  // backup copies of the models
  vector< CRoadPartModel > backModels0 = models0;
  vector< CRoadPartModel > backModels1 = models1;

  int size0 = static_cast< int >( models0.size() ); 
  int size1 = static_cast< int >( models1.size() ); 
  int last0 = size0 - 1;
  int last1 = size1 - 1;

  CGVector2 oldDir0 = CGVector2( models0[last0].GetNextDirection() );
  CGPoint2  oldEnd0 = models0[last0].GetNextPosition();
  CGVector2 oldDir1 = CGVector2( models1[last1].GetNextDirection() );
  CGPoint2  oldEnd1 = models1[last1].GetNextPosition();

  Float oldCompOnNorm = normal.Dot( CGVector2( oldEnd1, oldEnd0 ) );

  Float currCompOnNorm = oldCompOnNorm;

  switch ( algorithm )
  {
  default:
  case 0:
    {
      // we will move along the first road only, starting from the matching zone, and try to replace 
      // all models
      int it0 = last0;
      do 
      {
        if ( (0 <= it0) && (it0 <= last0) )
        {
          ETemplateType type0 = models0[it0].GetTemplate()->Type();

          if ( type0 == TT_Straight )
          {
            Float newCompOnNorm = replaceBetterStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
            }
          }
          else if ( type0 == TT_Corner )
          {
            Float newCompOnNorm = replaceBetterCorner( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
            }
          }
        }

        --it0;
      }
      while ( it0 >= 0 );

      // at this point the first road could have been modified
      oldDir0 = CGVector2( models0[last0].GetNextDirection() );
      oldEnd0 = models0[last0].GetNextPosition();

      // we will now move along the 2nd road only, starting from the matching zone, and try to replace 
      // all models
      int it1 = last1;
      do 
      {
        if ( (0 <= it1) && (it1 <= last1) )
        {
          ETemplateType type1 = models1[it1].GetTemplate()->Type();

          if ( type1 == TT_Straight )
          {
            Float newCompOnNorm = replaceBetterStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
            }
          }
          else if ( type1 == TT_Corner )
          {
            Float newCompOnNorm = replaceBetterCorner( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
            }
          }
        }

        --it1;
      }
      while ( it1 >= 0 );

      break;
    }
  case 1:
    {
      // we will move along the both roads, starting from the matching zone, and try to replace 
      // all models
      int it0 = last0;
      int it1 = last1;
      do 
      {
        // the 2nd road could have been changed in previous iteration
        oldDir1 = CGVector2( models1[last1].GetNextDirection() );
        oldEnd1 = models1[last1].GetNextPosition();

        // first road first
        if ( (0 <= it0) && (it0 <= last0) )
        {
          ETemplateType type0 = models0[it0].GetTemplate()->Type();

          if ( type0 == TT_Straight )
          {
            Float newCompOnNorm = replaceBetterStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
            }
          }
          else if ( type0 == TT_Corner )
          {
            Float newCompOnNorm = replaceBetterCorner( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
            }
          }
        }

        // the first road could have been changed 
        oldDir0 = CGVector2( models0[last0].GetNextDirection() );
        oldEnd0 = models0[last0].GetNextPosition();

        // then 2nd road 
        if ( (0 <= it1) && (it1 <= last1) )
        {
          ETemplateType type1 = models1[it1].GetTemplate()->Type();

          if ( type1 == TT_Straight )
          {
            Float newCompOnNorm = replaceBetterStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
            }
          }
          else if ( type1 == TT_Corner )
          {
            Float newCompOnNorm = replaceBetterCorner( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
            }
          }
        }

        --it0;
        --it1;
      }
      while ( (it0 >= 0) || (it1 >= 0) );

      break;
    }
  case 2:
    {
      // we will move along the both roads, starting from the matching zone, and try to replace 
      // all models
      // At any changes we restart from the matching zone for both roads
      int it0 = last0;
      int it1 = last1;
      do 
      {
        // the 2nd road could have been changed in previous iteration
        oldDir1 = CGVector2( models1[last1].GetNextDirection() );
        oldEnd1 = models1[last1].GetNextPosition();

        // first road first
        if ( (0 <= it0) && (it0 <= last0) )
        {
          ETemplateType type0 = models0[it0].GetTemplate()->Type();

          if ( type0 == TT_Straight )
          {
            Float newCompOnNorm = replaceBetterStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
              it0 = last0;
              it1 = last1;
              continue;
            }
          }
          else if ( type0 == TT_Corner )
          {
            Float newCompOnNorm = replaceBetterCorner( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
              it0 = last0;
              it1 = last1;
              continue;
            }
          }
        }

        // the first road could have been changed 
        oldDir0 = CGVector2( models0[last0].GetNextDirection() );
        oldEnd0 = models0[last0].GetNextPosition();

        // then 2nd road 
        if ( (0 <= it1) && (it1 <= last1) )
        {
          ETemplateType type1 = models1[it1].GetTemplate()->Type();

          if ( type1 == TT_Straight )
          {
            Float newCompOnNorm = replaceBetterStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
              it0 = last0;
              it1 = last1;
              continue;
            }
          }
          else if ( type1 == TT_Corner )
          {
            Float newCompOnNorm = replaceBetterCorner( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
            if ( newCompOnNorm < currCompOnNorm )
            {
              if ( abs( newCompOnNorm ) <= threshold ) 
              {
                return (true);
              }         
              currCompOnNorm = newCompOnNorm;
              it0 = last0;
              it1 = last1;
              continue;
            }
          }
        }

        --it0;
        --it1;
      }
      while ( (it0 >= 0) || (it1 >= 0) );

      break;
    }
  }

  if ( discardChanged )
  {
    // restores old models
    models0 = backModels0;
    models1 = backModels1;
  }

  return (false);
}

//-----------------------------------------------------------------------------

Float addBetterStraight( CRoad* road, bool isFirstRoad, int partIndex, const CGVector2& dirOtherRoad, const CGPoint2& endOtherRoad, 
                         const CGVector2& normal, Float threshold )
{
  vector< CRoadPartModel >& models = road->GetModels();
  vector< CRoadPartModel > backModels = models;

  int size = static_cast< int >( models.size() ); 
  int last = size - 1;

  CGVector2 oldErrorVec = isFirstRoad ? CGVector2( endOtherRoad, models[last].GetNextPosition() ) : CGVector2( models[last].GetNextPosition(), endOtherRoad );
  Float oldErrorOnNorm = normal.Dot( oldErrorVec );

  // gets the list of straight templates from the road type
  vector< CRoadPartTemplate* > straightTempls = road->GetType()->GetTemplates().GetAllStraights();

  CGPoint2 startPos = models[partIndex].GetNextPosition();
  Float    startDir = models[partIndex].GetNextDirection();

  CGVector2 currErrorVec   = oldErrorVec;
  Float     currErrorOnNorm = oldErrorOnNorm;

  bool changed = false;
  for ( size_t i = 0, cntI = straightTempls.size(); i < cntI; ++i )
  {
    // The temporary road starts with the new straight model
    CRoad tempRoad( road->GetType(), false );
    vector< CRoadPartModel >& tempModels = tempRoad.GetModels();
    CRoadPartModel tempModel( straightTempls[i], startPos, startDir );
    tempModels.push_back( tempModel );

    // Then continues with the remaining models on the original road
    size_t k = 0;
    for ( int j = partIndex + 1; j <= last; ++j )
    {
      CRoadPartModel newModel( backModels[j] );
      newModel.Redefine( tempModels[k].GetNextPosition(), tempModels[k].GetNextDirection() ); 
      tempModels.push_back( newModel );
      ++k;
    }

    // Now we have to check if the temporary roads gives a better solution than the current one
    const CRoadPartModel* lastModel = tempRoad.LastModel();

    CGVector2 newDir      = isFirstRoad ? CGVector2( lastModel->GetNextDirection() ) : -CGVector2( lastModel->GetNextDirection() );
    CGPoint2  newEnd      = lastModel->GetNextPosition();
    CGVector2 newErrorVec = isFirstRoad ? CGVector2( endOtherRoad, newEnd ) : CGVector2( newEnd, endOtherRoad );

    Float newErrorOnNorm = normal.Dot( newErrorVec );

    // proceed only if we are reducing the error
    if ( abs( newErrorOnNorm ) < abs( currErrorOnNorm ) )
    {
      // proceed only if there is no overlap
      if ( newDir.Dot( newErrorVec ) > ZERO )
      {
        if ( changed )
        {
          road->ReplaceModel( partIndex + 1, tempModel );
        }
        else
        {
          road->InsertModel( partIndex, tempModel );
//          if ( road->LastModel()->GetTemplate() == tempModel.GetTemplate() )
//          {
//            road->RemoveModel( models.size() - 1 );
//          }
          changed = true;
        }
        if ( abs( newErrorOnNorm ) <= threshold ) 
        {
          return (newErrorOnNorm);
        }         
        currErrorVec    = newErrorVec;
        currErrorOnNorm = newErrorOnNorm;
      }
    }
  }

  return (oldErrorOnNorm);
}

//-----------------------------------------------------------------------------

bool tryAddingStraights( CRoad* road0, CRoad* road1, const CGVector2& normal, Float threshold, bool discardChanged, size_t algorithm )
{
  if ( !road0 || !road1 ) { return (false); }

  vector< CRoadPartModel >& models0 = road0->GetModels();
  vector< CRoadPartModel >& models1 = road1->GetModels();

  // backup copies of the models
  vector< CRoadPartModel > backModels0 = models0;
  vector< CRoadPartModel > backModels1 = models1;

  int size0 = static_cast< int >( models0.size() ); 
  int size1 = static_cast< int >( models1.size() ); 
  int last0 = size0 - 1;
  int last1 = size1 - 1;

  CGVector2 oldDir0 = CGVector2( models0[last0].GetNextDirection() );
  CGPoint2  oldEnd0 = models0[last0].GetNextPosition();
  CGVector2 oldDir1 = CGVector2( models1[last1].GetNextDirection() );
  CGPoint2  oldEnd1 = models1[last1].GetNextPosition();

  Float oldCompOnNorm = normal.Dot( CGVector2( oldEnd1, oldEnd0 ) );

  Float currCompOnNorm = oldCompOnNorm;

  switch ( algorithm )
  {
  default:
  case 0:
    {
      // we will move along the first road only, starting from the matching zone, adding a straight in each position
      int it0 = last0;
      do 
      {
        if ( (0 <= it0) && (it0 <= static_cast< int >( models0.size() ) - 1) )
        {
          Float newCompOnNorm = addBetterStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it0;
      }
      while ( it0 >= 0 );

      // at this point the first road could have been modified
      oldDir0 = CGVector2( models0[models0.size() - 1].GetNextDirection() );
      oldEnd0 = models0[models0.size() - 1].GetNextPosition();

      // we will move along the 2nd road only, starting from the matching zone, adding a straight in each position
      int it1 = last1;
      do 
      {
        if ( (0 <= it1) && (it1 <= static_cast< int >( models1.size() ) - 1) )
        {
          Float newCompOnNorm = addBetterStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it1;
      }
      while ( it1 >= 0 );

      break;
    }
  case 1:
    {
      // we will move along both roads, starting from the matching zone, adding a straight in each position
      int it0 = last0;
      int it1 = last1;
      do 
      {
        // the 2nd road could have been changed in previous iteration
        oldDir1 = CGVector2( models1[models1.size() - 1].GetNextDirection() );
        oldEnd1 = models1[models1.size() - 1].GetNextPosition();

        if ( (0 <= it0) && (it0 <= static_cast< int >( models0.size() ) - 1) )
        {
          Float newCompOnNorm = addBetterStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        // at this point the first road could have been modified
        oldDir0 = CGVector2( models0[models0.size() - 1].GetNextDirection() );
        oldEnd0 = models0[models0.size() - 1].GetNextPosition();

        if ( (0 <= it1) && (it1 <= static_cast< int >( models1.size() ) - 1) )
        {
          Float newCompOnNorm = addBetterStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it1;
        --it0;
      }
      while ( (it0 >= 0) || (it1 >= 0) );

      break;
    }
  case 2:
    {
      // we will move along both roads, starting from the matching zone, adding a straight in each position
      int it0 = last0;
      int it1 = last1;
      do 
      {
        // the 2nd road could have been changed in previous iteration
        oldDir1 = CGVector2( models1[models1.size() - 1].GetNextDirection() );
        oldEnd1 = models1[models1.size() - 1].GetNextPosition();

        if ( (0 <= it0) && (it0 <= static_cast< int >( models0.size() ) - 1) )
        {
          Float newCompOnNorm = addBetterStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
            it0 = models0.size() - 1;
            it1 = models1.size() - 1;
            continue;
          }
        }

        // at this point the first road could have been modified
        oldDir0 = CGVector2( models0[models0.size() - 1].GetNextDirection() );
        oldEnd0 = models0[models0.size() - 1].GetNextPosition();

        if ( (0 <= it1) && (it1 <= static_cast< int >( models1.size() ) - 1) )
        {
          Float newCompOnNorm = addBetterStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
            it0 = models0.size() - 1;
            it1 = models1.size() - 1;
            continue;
          }
        }

        --it1;
        --it0;
      }
      while ( (it0 >= 0) || (it1 >= 0) );

      break;
    }
  }

  if ( discardChanged )
  {
    // restores old models
    models0 = backModels0;
    models1 = backModels1;
  }

  return (false);
}

//-----------------------------------------------------------------------------

Float removeStraight( CRoad* road, bool isFirstRoad, int partIndex, const CGVector2& dirOtherRoad, const CGPoint2& endOtherRoad, 
                      const CGVector2& normal, Float threshold )
{
  vector< CRoadPartModel >& models = road->GetModels();

  int size = static_cast< int >( models.size() ); 
  int last = size - 1;

  CGVector2 oldErrorVec = isFirstRoad ? CGVector2( endOtherRoad, models[last].GetNextPosition() ) : CGVector2( models[last].GetNextPosition(), endOtherRoad );
  Float oldErrorOnNorm = normal.Dot( oldErrorVec );

  if ( partIndex < 1 ) 
  {
    return (oldErrorOnNorm);
  }

  if ( models[partIndex].GetTemplate()->Type() != TT_Straight ) 
  {
    return (oldErrorOnNorm);
  }

  CGVector2 currErrorVec    = oldErrorVec;
  Float     currErrorOnNorm = oldErrorOnNorm;

  // The temporary road starts with the model previous to the current one
  CRoad tempRoad( road->GetType(), false );
  vector< CRoadPartModel >& tempModels = tempRoad.GetModels();
  CRoadPartModel tempModel( models[partIndex - 1] );
  tempModels.push_back( tempModel );

  // Then continues with the models on the original road after the current one
  size_t k = 0;
  for ( int j = partIndex + 1; j <= last; ++j )
  {
    CRoadPartModel newModel( models[j] );
    newModel.Redefine( tempModels[k].GetNextPosition(), tempModels[k].GetNextDirection() ); 
    tempModels.push_back( newModel );
    ++k;
  }

  // Finally, we add the removed model as final one
  CRoadPartModel finalModel( models[partIndex] );
  finalModel.Redefine( tempModels[k].GetNextPosition(), tempModels[k].GetNextDirection() ); 
  tempModels.push_back( finalModel );

  // Now we have to check if the temporary roads gives a better solution than the current one
  const CRoadPartModel* lastModel = tempRoad.LastModel();

  CGVector2 newDir      = isFirstRoad ? CGVector2( lastModel->GetNextDirection() ) : -CGVector2( lastModel->GetNextDirection() );
  CGPoint2  newEnd      = lastModel->GetNextPosition();
  CGVector2 newErrorVec = isFirstRoad ? CGVector2( endOtherRoad, newEnd ) : CGVector2( newEnd, endOtherRoad );

  Float newErrorOnNorm = normal.Dot( newErrorVec );

  // proceed only if we are reducing the error
  if ( abs( newErrorOnNorm ) < abs( currErrorOnNorm ) )
  {
    // proceed only if there is no overlap
    if ( newDir.Dot( newErrorVec ) > ZERO )
    {
      road->RemoveModel( partIndex );
      road->InsertModel( models.size() - 1, finalModel );

      if ( abs( newErrorOnNorm ) <= threshold ) 
      {
        return (newErrorOnNorm);
      }         
      currErrorVec    = newErrorVec;
      currErrorOnNorm = newErrorOnNorm;
    }
  }

  return (oldErrorOnNorm);
}

//-----------------------------------------------------------------------------

bool tryRemovingStraights( CRoad* road0, CRoad* road1, const CGVector2& normal, Float threshold, bool discardChanged, size_t algorithm )
{
  if ( !road0 || !road1 ) { return (false); }

  vector< CRoadPartModel >& models0 = road0->GetModels();
  vector< CRoadPartModel >& models1 = road1->GetModels();

  // backup copies of the models
  vector< CRoadPartModel > backModels0 = models0;
  vector< CRoadPartModel > backModels1 = models1;

  int size0 = static_cast< int >( models0.size() ); 
  int size1 = static_cast< int >( models1.size() ); 
  int last0 = size0 - 1;
  int last1 = size1 - 1;

  CGVector2 oldDir0 = CGVector2( models0[last0].GetNextDirection() );
  CGPoint2  oldEnd0 = models0[last0].GetNextPosition();
  CGVector2 oldDir1 = CGVector2( models1[last1].GetNextDirection() );
  CGPoint2  oldEnd1 = models1[last1].GetNextPosition();

  Float oldCompOnNorm = normal.Dot( CGVector2( oldEnd1, oldEnd0 ) );

  Float currCompOnNorm = oldCompOnNorm;

  switch ( algorithm )
  {
  default:
  case 0:
    {
      // we will move along the first road only, starting from the matching zone, removing the straights 
      int it0 = last0;
      do 
      {
        // goes to next straight model on the first road, if any
        while ( (it0 >= 0) && (models0[it0].GetTemplate()->Type() != TT_Straight) )
        {
          --it0;
        }

        if ( (0 <= it0) && (it0 <= static_cast< int >( models0.size() ) - 1) )
        {
          Float newCompOnNorm = removeStraight( road0, true, it0, oldDir1, oldEnd1, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it0;
      }
      while ( it0 >= 0 );

      // at this point the first road could have been modified
      oldDir0 = CGVector2( models0[models0.size() - 1].GetNextDirection() );
      oldEnd0 = models0[models0.size() - 1].GetNextPosition();

      // we will move along the 2nd road only, starting from the matching zone, adding a straight in each position
      int it1 = last1;
      do 
      {
        // goes to next straight model on the road, if any
        while ( (it1 >= 0) && (models1[it1].GetTemplate()->Type() != TT_Straight) )
        {
          --it1;
        }

        if ( (0 <= it1) && (it1 <= static_cast< int >( models1.size() ) - 1) )
        {
          Float newCompOnNorm = removeStraight( road1, false, it1, oldDir0, oldEnd0, normal, threshold );
          if ( newCompOnNorm < currCompOnNorm )
          {
            if ( abs( newCompOnNorm ) <= threshold ) 
            {
              return (true);
            }         
            currCompOnNorm = newCompOnNorm;
          }
        }

        --it1;
      }
      while ( it1 >= 0 );

      break;
    }
  }

  if ( discardChanged )
  {
    // restores old models
    models0 = backModels0;
    models1 = backModels1;
  }

  return (false);
}

//-----------------------------------------------------------------------------

bool CNullJoint::MatchRoads()
{
	CRoad* road0 = m_roads.Get( 0 );
	CRoad* road1 = m_roads.Get( 1 );

  // the roads should have the same type
  if ( road0->GetType() != road1->GetType() )
  {
    return (false);
  }

  vector< CRoadPartModel >& models0 = road0->GetModels();
  vector< CRoadPartModel >& models1 = road1->GetModels();

	if ( !road0->IsBuilt() || !road1->IsBuilt() ) { return (false); }
	if ( (models0.size() == 0) || (models1.size() == 0) ) { return (false); }

  // backup copies of the models
  vector< CRoadPartModel > backModels0 = models0;
  vector< CRoadPartModel > backModels1 = models1;

	Float threshold = road0->GetType()->GetMatchThreshold();

	// average normal to paths. we use it as a reference direction
	// we cannot bet that the ending segments of the two roads will be
	// aligned, so we make an average (...actually if the road paths are 
  // coming from a split they should be aligned...)
	CGVector2 norm0 = road0->GetPath().LastSegment().UnitNormalVector();
	CGVector2 norm1 = -road1->GetPath().LastSegment().UnitNormalVector();
	CGVector2 normal = HALF * ( norm0 + norm1 );
	normal.Normalize();

  Float currCompDiffOnNorm = normal.Dot( CGVector2( road1->LastModel()->GetNextPosition(), road0->LastModel()->GetNextPosition() ) );

  // if the threshold is already matched, return
  if ( abs( currCompDiffOnNorm ) <= threshold ) 
  { 
    if ( CGVector2( road0->LastModel()->GetNextDirection() ).Dot( CGVector2( road1->LastModel()->GetNextDirection() ) ) < -0.999 )
    {
      return (true); 
    }
  }

  // tries to replace straight parts
  if ( tryReplacingStraights( road0, road1, normal, threshold, true, 0 ) ) { return (true); }
  if ( tryReplacingStraights( road0, road1, normal, threshold, true, 1 ) ) { return (true); }
  if ( tryReplacingStraights( road0, road1, normal, threshold, true, 2 ) ) { return (true); }

  // tries to replace corner parts
  if ( tryReplacingCorners( road0, road1, normal, threshold, true, 0 ) )  { return (true); }
  if ( tryReplacingCorners( road0, road1, normal, threshold, true, 1 ) )  { return (true); }
  if ( tryReplacingCorners( road0, road1, normal, threshold, true, 2 ) )  { return (true); }

  // tries to replace all parts
  if ( tryReplacingAll( road0, road1, normal, threshold, true, 0 ) )  { return (true); }
  if ( tryReplacingAll( road0, road1, normal, threshold, true, 1 ) )  { return (true); }
  if ( tryReplacingAll( road0, road1, normal, threshold, true, 2 ) )  { return (true); }

  if ( tryAddingStraights( road0, road1, normal, threshold, true, 0 ) ) { return (true); }
  if ( tryAddingStraights( road0, road1, normal, threshold, true, 1 ) ) { return (true); }
  if ( tryAddingStraights( road0, road1, normal, threshold, true, 2 ) ) { return (true); }

  if ( tryRemovingStraights( road0, road1, normal, threshold, true, 0 ) )  { return (true); }

  Float newError = normal.Dot( CGVector2( road1->LastModel()->GetNextPosition(), road0->LastModel()->GetNextPosition() ) );
  Float oldError;
  do
  {
    oldError = newError;

    if ( tryReplacingStraights( road0, road1, normal, threshold, false, 0 ) ) { return (true); }
    if ( tryReplacingStraights( road0, road1, normal, threshold, false, 1 ) ) { return (true); }
    if ( tryReplacingStraights( road0, road1, normal, threshold, false, 2 ) ) { return (true); }

    if ( tryReplacingCorners( road0, road1, normal, threshold, false, 0 ) )  { return (true); }
    if ( tryReplacingCorners( road0, road1, normal, threshold, false, 1 ) )  { return (true); }
    if ( tryReplacingCorners( road0, road1, normal, threshold, false, 2 ) )  { return (true); }

    if ( tryReplacingAll( road0, road1, normal, threshold, false, 0 ) )  { return (true); }
    if ( tryReplacingAll( road0, road1, normal, threshold, false, 1 ) )  { return (true); }
    if ( tryReplacingAll( road0, road1, normal, threshold, false, 2 ) )  { return (true); }

    if ( tryAddingStraights( road0, road1, normal, threshold, false, 0 ) ) { return (true); }
    if ( tryAddingStraights( road0, road1, normal, threshold, false, 1 ) ) { return (true); }
    if ( tryAddingStraights( road0, road1, normal, threshold, false, 2 ) ) { return (true); }

    if ( tryRemovingStraights( road0, road1, normal, threshold, false, 0 ) )  { return (true); }

    newError = normal.Dot( CGVector2( road1->LastModel()->GetNextPosition(), road0->LastModel()->GetNextPosition() ) );
  }
  while ( newError < oldError );

  // restore initial models
  models0 = backModels0;
  models1 = backModels1;
  return (false);
}

//-----------------------------------------------------------------------------

bool CNullJoint::MatchLoopRoad()
{
	// non implemented yet
	return (false);
}

//-----------------------------------------------------------------------------

void CNullJoint::FillGaps()
{
	CRoad* road0 = m_roads.Get( 0 );
	CRoad* road1 = m_roads.Get( 1 );

  // the roads should have the same type
  if ( road0->GetType() != road1->GetType() )
  {
    return;
  }

  bool done = false;
  do
  {
	  const CRoadPartModel* lastModel0 = road0->LastModel();
	  const CRoadPartModel* lastModel1 = road1->LastModel();

	  const CGPoint2& currPosition0  = lastModel0->GetNextPosition();
	  const CGPoint2& currPosition1  = lastModel1->GetNextPosition();
	  const Float     currDirection0 = lastModel0->GetNextDirection();
	  const Float     currDirection1 = lastModel1->GetNextDirection();

    ETemplateType type0 = lastModel0->GetTemplate()->Type();
    ETemplateType type1 = lastModel1->GetTemplate()->Type();

	  CGVector2 diffVec( currPosition1, currPosition0 );
	  CGVector2 dir0Vec( currDirection0 );
	  Float comp = diffVec.Dot( dir0Vec );

	  // if the two vectors point in the same direction, there is a gap
	  // so, we add a straight road part to fill it if any of the 2 parts
	  if ( comp > ZERO )
	  {
      CRoadPartTemplate* templ = road0->GetType()->GetTemplates().GetSquareStraight();
      if ( templ )
      {
        if ( (type0 == TT_Straight) || (type1 == TT_Straight) )
        {
          // one of the two roads already has a straight as last model
          if ( type0 == TT_Straight )
          {
		        road0->GetModels().push_back( CRoadPartModel( templ, currPosition0, currDirection0 ) );
          }
          else 
          {
		        road1->GetModels().push_back( CRoadPartModel( templ, currPosition1, currDirection1 ) );
          }
        }
        else
        {
          // none of the two roads already has a straight as last model
          road0->GetModels().push_back( CRoadPartModel( templ, currPosition0, currDirection0 ) );
        }
      }
	  }
    else
    {
      done = true;
    }
  }
  while ( !done );
}

//-----------------------------------------------------------------------------

std::ostream& operator << ( std::ostream& o, const CNullJoint& joint )
{
	o << " joint " << joint.GetPosition();

	return (o);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CCrossroad::CCrossroad( const CGPoint2& position, ECrossroadType crossType, CRoad* firstRoad )
: CJoint( position, firstRoad )
, m_crossType( crossType )
{
}

//-----------------------------------------------------------------------------

CCrossroad::CCrossroad( const CCrossroad& other )
: CJoint( other )
, m_crossType( other.m_crossType )
{
}

//-----------------------------------------------------------------------------

CCrossroad::~CCrossroad()
{
}

//-----------------------------------------------------------------------------

const CCrossroadModel& CCrossroad::GetModel() const
{
	return (m_model);
}

//-----------------------------------------------------------------------------

void CCrossroad::BuildRoads()
{
	for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
	{
		CRoad* road = m_roads.Get( i );
    if ( !road->IsBuilt() && !road->GetEndJoint() && !road->IsDegenerated() ) 
		{
			road->BuildModels();
		}
	}
}

//-----------------------------------------------------------------------------

EJointType CCrossroad::Type() const
{
	return (JT_Crossroad);
}

//-----------------------------------------------------------------------------

Float CCrossroad::Radius() const
{
	switch ( m_crossType )
	{
	case CT_GenericAngles: { return (m_roads.MaxWidth() + HALF); }
	case CT_RightAngles:
	default:               { return (HALF * m_roads.MaxWidth() + HALF); }
	}

	assert( false );
	return ( ZERO );
}

//-----------------------------------------------------------------------------

void CCrossroad::BuildModel( CCrossroadTemplatesLibrary& crossroadsLibrary )
{
	Float radius = Radius();

	// selects or creates the crossroad template for this crossroad
	size_t roadsCount = m_roads.Size();
	CCrossroadTemplate* templ = NULL;
	Float direction; 
  
  PreBuildModel();

	switch ( m_crossType )
	{
	case CT_GenericAngles:
		{
			if      ( roadsCount == 3 ) { templ = new CTGenericAnglesCrossroadTemplate( m_roads, radius ); }
			else if ( roadsCount == 4 ) { templ = new CXGenericAnglesCrossroadTemplate( m_roads, radius ); }
			else
			{
				assert( false );
			}
  		break;
		}
	case CT_RightAngles:
	default:
		{
			if      ( roadsCount == 3 ) { templ = new CTRightAnglesCrossroadTemplate( m_roads, radius ); }
			else if ( roadsCount == 4 ) { templ = new CXRightAnglesCrossroadTemplate( m_roads, radius ); }
			else
			{
				assert( false );
			}
  		break;
		}
	}

  if ( m_roads.Get( 0 )->IsDegenerated() )
  {
    direction = CGConsts::PI + m_roads.Get( 0 )->GetPath().FirstSegment().Direction();
  }
  else
  {
	  const CGPoint2& south = m_roads.Get( 0 )->GetPath().FirstVertex();
	  CGVector2 southVec( m_position, south );
	  direction = southVec.Direction();
  }

	// appends the template to the library and reselect it
	templ = crossroadsLibrary.Append( *templ );
	assert( templ );

	m_model = CCrossroadModel( templ, m_position, direction );
}

//-----------------------------------------------------------------------------

void CCrossroad::PreBuildModel()
{
	Float radius = Radius();
	assert( radius != ZERO );

	// lets all roads to start in this crossroad
	for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
	{
		if ( m_roads.Get( i )->GetStartJoint() != this )
		{
			m_roads.Get( i )->ReverseVerticesOrder();
		}

		assert( m_roads.Get( i )->GetStartJoint() == this );
	}

	// reshapes roads in the crossroad's neighborhood
	// removes from the road paths all the vertices contained inside the circle
	// having as radius the crossroad radius
	// adds to all roads the intersection between the circle and the original path.
	CGCircle2 circle( m_position, radius );
	CGPoint2 intPoint;
	int segIndex;

	for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
	{
    CRoad* road = m_roads.Get( i );
		CGPolyline2& path = road->GetPath();
		if ( !FirstIntersect( circle, path, intPoint, segIndex, 0.001 ) )
		{
			// the road is completely contained in the crossroad circle
			// we don't touch it
      road->SetDegenerated();
		}
		else
		{
			if ( segIndex > 0 )
			{
				for ( size_t j = 1; j <= static_cast< size_t >( segIndex ); ++j )
				{
					path.RemoveVertex( 1 );
				}
			}
			path.InsertVertex( 1, intPoint );
		}
	}

	// sorts the roads by the direction of the first segment
	m_roads.SortByDirection();
}

//-----------------------------------------------------------------------------
