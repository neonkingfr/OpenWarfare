//-----------------------------------------------------------------------------
// File: TList.h
//
// Desc: a templated list
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 / 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"

//-----------------------------------------------------------------------------

template < class T >
class TList
{
protected:
	vector< T* > m_elements;

public:
	TList();
	TList( const TList< T >& other );
	virtual ~TList();

	TList< T >& operator = ( const TList< T >& other );

	void Clear();
	T* Append( const T& element );
	T* Append( T* element );
	size_t Size() const;
	void Remove( const T* element );
	void Remove( size_t index );
	T* Find( const T& t ) const;

	const T* Get( size_t index ) const;
	T* Get( size_t index );

private:
	void CopyFrom( const TList< T >& other );
};

//-----------------------------------------------------------------------------

#include "TList.inl"

//-----------------------------------------------------------------------------
