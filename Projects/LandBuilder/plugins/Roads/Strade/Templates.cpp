//-----------------------------------------------------------------------------
// File: Templates.cpp
//
// Desc: classes and types related to road templates
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Templates.h"
#include "Roads.h"

//-----------------------------------------------------------------------------

#ifdef _VBS_ONLY_
  #define DEFAULT_RVMAT "vbs2\\data\\default.rvmat"
#endif

//-----------------------------------------------------------------------------

CTemplate::CTemplate()
: m_referencesCount( 0 )
{
}

//-----------------------------------------------------------------------------

CTemplate::CTemplate( const CTemplate& other )
: m_referencesCount( other.m_referencesCount )
{
}

//-----------------------------------------------------------------------------

CTemplate::~CTemplate()
{
}

//-----------------------------------------------------------------------------

void CTemplate::IncreaseReferencesCount()
{
  ++m_referencesCount;
}

//-----------------------------------------------------------------------------

void CTemplate::DecreaseReferencesCount()
{
  --m_referencesCount;
  assert( m_referencesCount >= 0 );
}

//-----------------------------------------------------------------------------

String CTemplate::Filename() const
{
  return (Name() + ".p3d");
}

//-----------------------------------------------------------------------------

int CTemplate::GetReferencesCount() const
{
  return (m_referencesCount);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CRoadBaseTemplate::CRoadBaseTemplate()
: CTemplate()
{
}

//-----------------------------------------------------------------------------

CRoadBaseTemplate::CRoadBaseTemplate( const CRoadBaseTemplate& other )
: CTemplate( other )
, m_geometry( other.m_geometry )
, m_textures( other.m_textures )
{
}

//-----------------------------------------------------------------------------

CRoadBaseTemplate::~CRoadBaseTemplate()
{
}

//-----------------------------------------------------------------------------

const vector< CGPolygon2 >& CRoadBaseTemplate::GetGeometry() const
{
  return (m_geometry);
}

//-----------------------------------------------------------------------------

const vector< string >& CRoadBaseTemplate::GetTextures() const
{
  return (m_textures);
}

//-----------------------------------------------------------------------------

bool CRoadBaseTemplate::Equals( const CRoadBaseTemplate& other ) const
{
  if ( this == &other ) { return (true); }

  // checks textures
  size_t thisTexCount  = m_textures.size();
  size_t otherTexCount = other.m_textures.size();
  
  if ( thisTexCount != otherTexCount ) { return (false); }

  for ( size_t i = 0; i < thisTexCount; ++i )
  {
    if ( m_textures[i] != other.m_textures[i] ) { return (false); }
  }

  // checks geometry
  size_t thisGeomCount  = m_geometry.size();
  size_t otherGeomCount = other.m_geometry.size();

  if ( thisGeomCount != otherGeomCount ) { return (false); }

  for ( size_t i = 0; i < thisGeomCount; ++i )
  {
    if ( !m_geometry[i].Equals( other.m_geometry[i], TOLERANCE ) ) { return (false); }
  }

  return (true);
}

//-----------------------------------------------------------------------------

CGPoint2 CRoadBaseTemplate::GeometricCenter() const
{
  Float minX = std::numeric_limits< Float >::infinity();
  Float minY = std::numeric_limits< Float >::infinity();
  Float maxX = -std::numeric_limits< Float >::infinity();
  Float maxY = -std::numeric_limits< Float >::infinity();

  for ( size_t g = 0, cntG = m_geometry.size(); g < cntG; ++g )
  {
    minX = min( minX, m_geometry[g].MinX() );
    minY = min( minY, m_geometry[g].MinY() );
    maxX = max( maxX, m_geometry[g].MaxX() );
    maxY = max( maxY, m_geometry[g].MaxY() );
  }

  Float midX = HALF * ( minX + maxX );
  Float midY = HALF * ( minY + maxY );

  return (CGPoint2( midX, midY ));
}

//-----------------------------------------------------------------------------

void CRoadBaseTemplate::Initialize()
{
  CalcGeometry();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CRoadPartTemplate::CRoadPartTemplate( const CRoadType* roadType,
                                      const string& texture, bool special )
: CRoadBaseTemplate()
, m_roadType( roadType )
, m_special( special )
, m_resolution( 1 )
{
  // add the input texture to the list only if not empty
  if ( texture.length() > 0 ) { m_textures.push_back( texture ); }
}

//-----------------------------------------------------------------------------

CRoadPartTemplate::CRoadPartTemplate( const CRoadPartTemplate& other )
: CRoadBaseTemplate( other )
, m_roadType( other.m_roadType )
, m_special( other.m_special )
, m_chord( other.m_chord )
, m_invertedChord( other.m_invertedChord )
, m_deltaDirection( other.m_deltaDirection )
, m_resolution( other.m_resolution )
{
}

//-----------------------------------------------------------------------------

CRoadPartTemplate::~CRoadPartTemplate()
{
}

//-----------------------------------------------------------------------------

const CRoadType* CRoadPartTemplate::GetRoadType() const
{
  return (m_roadType);
}

//-----------------------------------------------------------------------------

const CGVector2& CRoadPartTemplate::GetChord() const
{
  return (m_chord);
}

//-----------------------------------------------------------------------------

const CGVector2& CRoadPartTemplate::GetInvertedChord() const
{
  return (m_invertedChord);
}

//-----------------------------------------------------------------------------

Float CRoadPartTemplate::GetDeltaDirection() const
{
  return (m_deltaDirection);
}

//-----------------------------------------------------------------------------

bool CRoadPartTemplate::IsSpecial() const
{
  return (m_special);
}

//-----------------------------------------------------------------------------

bool CRoadPartTemplate::ExportP3D( const String& destPath ) const
{
  LODObject lodObj;

  // Geometry LOD
  ObjectData* obj = new ObjectData();
  GenerateP3DLod( *obj, 1.0, destPath );

  // properties
  obj->SetNamedProp( "class", "road" );	
  const String& mapP3D = m_roadType->GetMapP3D();
  if ( !mapP3D.IsEmpty() ) 
  { 
    obj->SetNamedProp( "map", mapP3D );
  }

  lodObj.InsertLevel( obj, 1.0 );
  lodObj.DeleteLevel( 0 );

  // RoadWay LOD
  obj = new ObjectData();
  GenerateP3DLod( *obj, LOD_ROADWAY, destPath );
  lodObj.InsertLevel( obj, LOD_ROADWAY );

  // Memory LOD
  obj = new ObjectData();
  GenerateP3DLod( *obj, LOD_MEMORY, destPath );
  lodObj.InsertLevel( obj, LOD_MEMORY );

  Pathname path = destPath + "\\" + Filename();
  if ( lodObj.Save( path, OBJDATA_LATESTVERSION, false, 0, 0 ) != 0 ) 
  {
    return (false);
  }
  else
  {
    return (true);
  }
}

//-----------------------------------------------------------------------------

void CRoadPartTemplate::GenerateP3DLod( ObjectData& obj, float resolution,
                                        const String& destPath ) const
{
  AutoArray< int > indices;

  if ( resolution != LOD_MEMORY )
  {
    // geometry
    // vertices are ordered in CCW order. first vertex is the one
    // in the bottom-right position.
    size_t verticesCount = m_geometry[0].VerticesCount();
    for ( size_t v = 0; v < verticesCount; ++v )
    {
      const CGPoint2& point = m_geometry[0].Vertex( v );
      obj.NewPoint()->SetPoint( Vector3( (Coord)point.GetX(),
                                         (Coord)ZERO, 
                                         (Coord)point.GetY() ) );

      if ( m_textures.size() > 0 && m_textures[0] != "" ) { indices.Add( v ); }
    }

    // texture
    if ( m_textures.size() > 0 && m_textures[0] != "" )
    {
      // forces the automatic generation of faces
      ObjToolTopology& topology = obj.GetTool< ObjToolTopology >();
      topology.TessellatePolygon( indices.Size(), indices.Data() );

      // paths
      String modelPath = destPath;

      // texturePath is absolute, it is used as filename during
      // save operation
      String texturePath = modelPath + "\\data";

      int pos = modelPath.Find( ':' ) + 2;
      modelPath = modelPath.Substring( pos, modelPath.GetLength() );

      // texture is relative, it will be saved inside the p3d file
      String texture = String( m_textures[0].c_str() );
      int slashPos = 0;
      while ( slashPos != -1 )
      {
        slashPos = texture.Find( "\\" );
        if ( slashPos != -1 )
        {
          texture = texture.Substring( slashPos + 1, texture.GetLength() );
        }
      }
      texturePath = texturePath + "\\" + texture;

      if ( texture[0] == '#' )
      {
        // if procedural texture we don't need any path
      }
      else
      {
        texture = modelPath + "\\data\\" + texture;
        
        // copy the textures on disk.
        // first tests if the texture has been already saved 
        // (it maybe be shared with other models)
        // source textures can be everywhere, destination textures
        // will be saved in a \data subfolder of the folder containing
        // the models
        ifstream test( texturePath, std::ios::binary );
        if ( test.fail() )
        {
          ifstream ifs( m_textures[0].c_str(), std::ios::binary );
          if ( !ifs.fail() )
          {
            ofstream ofs( texturePath, std::ios::binary );

            const int BUFFER_SIZE = 4096;
            char buffer[BUFFER_SIZE];

            while ( !ifs.eof() ) 
            {
              ifs.read( buffer, BUFFER_SIZE );
              if ( !ifs.bad() ) 
              {
                ofs.write( buffer, ifs.gcount() );
              }
            }

            ifs.close();
            ofs.close();
          }
        }
        else
        {
          test.close();
        }
      }

      // texture mapping
      // we consider the model as represented by n = m_resolution 
      // rectangles, each one composed by two triangles.
      Float normLength = Length() / m_roadType->GetWidth();
      Float step = normLength / m_resolution;
      for ( size_t i = 0; i < m_resolution; ++i )
      {
        // first triangle
        FaceT face1( obj, i * 2 );
        face1.SetPoint( 0, i );
        face1.SetPoint( 1, i + 1 );
        face1.SetPoint( 2, verticesCount - 1 - i );

        // second triangle
        FaceT face2( obj, i * 2 + 1 );
        face2.SetPoint( 0, i + 1 );
        face2.SetPoint( 1, verticesCount - 2 - i );
        face2.SetPoint( 2, verticesCount - 1 - i );

        Float uLeft  = ZERO;
        Float uRight = ONE;

        // 0 is at the top of the texture, 1 at the bottom
        Float vBottom = (i + 1) * step - normLength;
        Float vTop    = i * step - normLength;

        face1.SetU( 0, (float)uRight );
        face1.SetV( 0, (float)vBottom );

        face1.SetU( 1, (float)uRight );
        face1.SetV( 1, (float)vTop );

        face1.SetU( 2, (float)uLeft );
        face1.SetV( 2, (float)vBottom );

        face2.SetU( 0, (float)uRight );
        face2.SetV( 0, (float)vTop );

        face2.SetU( 1, (float)uLeft );
        face2.SetV( 1, (float)vTop );

        face2.SetU( 2, (float)uLeft );
        face2.SetV( 2, (float)vBottom );

        if ( resolution != LOD_ROADWAY )
        {
          face1.SetTexture( texture );
          face2.SetTexture( texture );

#ifdef _VBS_ONLY_
          face1.SetMaterial( DEFAULT_RVMAT );
          face2.SetMaterial( DEFAULT_RVMAT );
#endif

        }
      }
    }

    obj.RecalcNormals();
  }
  else
  {
    // memory LOD
    size_t cnt = m_geometry[0].VerticesCount();
    RString netCodes[4]   = { "PB", "PE", "LE", "LB" };
    size_t  netIndices[4] = { 0, cnt / 2 - 1, cnt / 2, cnt - 1 };

    // geometry
    for ( size_t i = 0; i < 4; ++i )
    {
      const CGPoint2& point = m_geometry[0].Vertex( netIndices[i] );
      obj.NewPoint()->SetPoint( Vector3( (Coord)point.GetX(), 
                                         (Coord)ZERO,
                                         (Coord)point.GetY() ) );
    }

    // selections
    for ( size_t i = 0; i < 4; ++i )
    {
      Selection selNetCode( &obj );
      selNetCode.PointSelect( i );
      obj.SaveNamedSel( netCodes[i], &selNetCode );
    }
  }
}

//-----------------------------------------------------------------------------

bool CRoadPartTemplate::Equals( const CRoadBaseTemplate& other ) const
{
  if ( this == &other ) { return (true); }

  const CRoadPartTemplate* _other = reinterpret_cast< const CRoadPartTemplate* >( &other );
  if ( Type()     != _other->Type() )     { return (false); }
  if ( m_roadType != _other->m_roadType ) { return (false); }
  if ( m_special  != _other->m_special )  { return (false); }

  return (CRoadBaseTemplate::Equals( other ));
}

//-----------------------------------------------------------------------------

void CRoadPartTemplate::Initialize()
{
  CalcResolution();
  CRoadBaseTemplate::Initialize();
  CalcChords();
  CalcDeltaDirection();
}

// -------------------------------------------------------------------------- //

void CRoadPartTemplate::CalcResolution()
{
  if ( Type() == TT_Corner ) 
  { 
    Float len = Length();
    Float res = m_roadType->GetCornerResolution();
    while ( len / m_resolution > res ) { m_resolution *= 2; }
  }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CStraightTemplate::CStraightTemplate( const CRoadType* roadType, Float length, 
                                      const string& texture, bool special )
: CRoadPartTemplate( roadType, texture, special )
, m_length( length )
{
  assert( m_length > ZERO );
  Initialize();
}

//-----------------------------------------------------------------------------

CStraightTemplate::CStraightTemplate( const CStraightTemplate& other )
: CRoadPartTemplate( other )
, m_length( other.m_length )
{
}

//-----------------------------------------------------------------------------

CStraightTemplate::~CStraightTemplate()
{
}

//-----------------------------------------------------------------------------

ETemplateType CStraightTemplate::Type() const
{
  return (TT_Straight);
}

//-----------------------------------------------------------------------------

String CStraightTemplate::Name() const
{
  String name = m_roadType->ExportPrefix() + "_L" + String( FormatNumber( m_length ).c_str() );
  if ( m_special ) { name = name + SPEC_SUFFIX; }

  return (name);
}

//-----------------------------------------------------------------------------

void CStraightTemplate::CalcGeometry()
{
  m_geometry.clear();

  Float halfWidth = HALF * m_roadType->GetWidth();

  CGPolygon2 footprint;
  footprint.AppendVertex(  halfWidth, ZERO );
  footprint.AppendVertex(  halfWidth, m_length );
  footprint.AppendVertex( -halfWidth, m_length );
  footprint.AppendVertex( -halfWidth, ZERO );

  m_geometry.push_back( footprint );
}

//-----------------------------------------------------------------------------

Float CStraightTemplate::Length() const
{
  return (m_length);
}

//-----------------------------------------------------------------------------

void CStraightTemplate::CalcChords()
{
  m_chord         = CGVector2( ZERO, m_length );
  m_invertedChord = CGVector2( ZERO, -m_length );
}

//-----------------------------------------------------------------------------

void CStraightTemplate::CalcDeltaDirection()
{
  m_deltaDirection = ZERO;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CCornerTemplate::CCornerTemplate( const CRoadType* roadType, Float radius, Float angle, 
                                  const string& texture, bool special )
: CRoadPartTemplate( roadType, texture, special )
, m_radius( radius )
, m_angle( angle )
{
  assert( m_radius > ZERO );
  assert( m_angle  > ZERO );
  Initialize();
}

//-----------------------------------------------------------------------------

CCornerTemplate::CCornerTemplate( const CCornerTemplate& other )
: CRoadPartTemplate( other )
, m_radius( other.m_radius )
, m_angle( other.m_angle )
{
}

//-----------------------------------------------------------------------------

CCornerTemplate::~CCornerTemplate()
{
}

//-----------------------------------------------------------------------------

Float CCornerTemplate::GetRadius() const
{
  return (m_radius);
}

//-----------------------------------------------------------------------------

Float CCornerTemplate::GetAngle() const
{
  return (m_angle);
}

//-----------------------------------------------------------------------------

ETemplateType CCornerTemplate::Type() const
{
  return (TT_Corner);
}

//-----------------------------------------------------------------------------

String CCornerTemplate::Name() const
{
  Float angDeg = m_angle * CGConsts::RAD_TO_DEG;
  String name = m_roadType->ExportPrefix() + 
                "_A" + String( FormatNumber( angDeg ).c_str() ) + 
                "_R" + String( FormatNumber( m_radius ).c_str() );
  if ( m_special ) { name = name + SPEC_SUFFIX; }

  return (name);
}

//-----------------------------------------------------------------------------

void CCornerTemplate::CalcGeometry()
{
  m_geometry.clear();

  Float halfWidth = HALF * m_roadType->GetWidth();
  Float angle     = m_angle / m_resolution;

  CGPoint2 center( m_radius, ZERO );

  CGPolygon2 footprint;

  // right side
  for ( size_t i = 0; i <= m_resolution; ++i )
  {
    CGPoint2 point( halfWidth, ZERO );
    if ( i )
    {
      point.Rotate( center, -(i * angle) );
    }
    footprint.AppendVertex( point.GetX(), point.GetY() );
  }

  // left side
  for ( size_t i = 0; i <= m_resolution; ++i )
  {
    CGPoint2 point( -halfWidth, ZERO );
    if ( i != m_resolution )
    {
      point.Rotate( center, -(m_angle - i * angle) );
    }
    footprint.AppendVertex( point.GetX(), point.GetY() );
  }

  m_geometry.push_back( footprint );
}

//-----------------------------------------------------------------------------

Float CCornerTemplate::Length() const
{
  return (m_radius * m_angle);
}

//-----------------------------------------------------------------------------

void CCornerTemplate::CalcChords()
{
  Float halfAngle   = HALF * m_angle;
  Float chordLength = TWO * m_radius * sin( halfAngle );
  Float dx = chordLength * sin( halfAngle );
  Float dy = chordLength * cos( halfAngle );

  m_chord         = CGVector2( dx, dy );
  m_invertedChord = CGVector2( dx, -dy ); 
}

//-----------------------------------------------------------------------------

void CCornerTemplate::CalcDeltaDirection()
{
  // CW, so negative
  m_deltaDirection = -m_angle;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CStrTerminatorTemplate::CStrTerminatorTemplate( const CRoadType* roadType, Float length, 
                                                const string& texture, bool special )
: CStraightTemplate( roadType, length, texture, special )
{
}

//-----------------------------------------------------------------------------

CStrTerminatorTemplate::CStrTerminatorTemplate( const CStrTerminatorTemplate& other )
: CStraightTemplate( other )
{
}

//-----------------------------------------------------------------------------

CStrTerminatorTemplate::~CStrTerminatorTemplate()
{
}

//-----------------------------------------------------------------------------

ETemplateType CStrTerminatorTemplate::Type() const
{
  return (TT_StrTerminator);
}

//-----------------------------------------------------------------------------

String CStrTerminatorTemplate::Name() const
{
  String name = m_roadType->ExportPrefix() + "_L" + String( FormatNumber( m_length ).c_str() ) + TERM_SUFFIX;
  if ( m_special ) { name = name + SPEC_SUFFIX; }

  return (name);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CCrossroadTemplate::CCrossroadTemplate( CRoadsSet& roads, Float radius )
: CRoadBaseTemplate()
, m_roads( &roads )
, m_radius( radius )
{
  assert( m_radius != ZERO );
  assert( (2 < m_roads->Size()) && (m_roads->Size() < 5) );
}

//-----------------------------------------------------------------------------

CCrossroadTemplate::CCrossroadTemplate( const CCrossroadTemplate& other )
: CRoadBaseTemplate( other )
, m_roads( other.m_roads )
, m_radius( other.m_radius )
{
}

//-----------------------------------------------------------------------------

CCrossroadTemplate::~CCrossroadTemplate()
{
}

//-----------------------------------------------------------------------------

bool CCrossroadTemplate::Equals( const CRoadBaseTemplate& other ) const
{
  if ( this == &other ) { return (true); }

  // checks textures
  const CCrossroadTemplate* _other = reinterpret_cast< const CCrossroadTemplate* >( &other );
  if ( m_radius != _other->m_radius )           { return (false); }
  if ( !m_roads->Equals( *(_other->m_roads) ) ) { return (false); }

  return (CRoadBaseTemplate::Equals( other ));
}

//-----------------------------------------------------------------------------

const CRoadsSet* CCrossroadTemplate::GetRoadSet() const
{
  return (m_roads);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CRightAnglesCrossroadTemplate::CRightAnglesCrossroadTemplate( CRoadsSet& roads, Float radius )
: CCrossroadTemplate( roads, radius )
{
  assert( m_roads->Size() == 3 || m_roads->Size() == 4 );
}

//-----------------------------------------------------------------------------

CRightAnglesCrossroadTemplate::CRightAnglesCrossroadTemplate( const CRightAnglesCrossroadTemplate& other )
: CCrossroadTemplate( other )
{
}

//-----------------------------------------------------------------------------

CRightAnglesCrossroadTemplate::~CRightAnglesCrossroadTemplate()
{
}

//-----------------------------------------------------------------------------

bool CRightAnglesCrossroadTemplate::ExportP3D( const String& destPath ) const
{
  LODObject lodObj;

  // Geometry LOD
  ObjectData* obj = new ObjectData();
  GenerateP3DLod( *obj, 1.0, destPath );

  // properties
  obj->SetNamedProp( "class", "road" );
  const String& mapP3D = m_roads->Get( 0 )->GetType()->GetMapP3D();
  if ( !mapP3D.IsEmpty() ) 
  { 
    obj->SetNamedProp( "map", mapP3D );
  }

  lodObj.InsertLevel( obj, 1.0 );
  lodObj.DeleteLevel( 0 );

  // RoadWay LOD
  obj = new ObjectData();
  GenerateP3DLod( *obj, LOD_ROADWAY, destPath );
  lodObj.InsertLevel( obj, LOD_ROADWAY );

  // Memory LOD
  obj = new ObjectData();
  GenerateP3DLod( *obj, LOD_MEMORY, destPath );
  lodObj.InsertLevel( obj, LOD_MEMORY );

  Pathname path = destPath + "\\" + Filename();
  if ( lodObj.Save( path, OBJDATA_LATESTVERSION, false, 0, 0 ) != 0 ) 
  {
    return (false);
  }
  else
  {
    return (true);
  }
}

//-----------------------------------------------------------------------------

void CRightAnglesCrossroadTemplate::GenerateP3DLod( ObjectData& obj, float resolution,
                                                    const String& destPath ) const
{
  AutoArray< int > indices;

  if ( resolution != LOD_MEMORY )
  {
    // geometry
    // we force the generation of S and N parts as first
    vector< size_t > insOrder;
    if ( m_geometry.size() == 3 )
    {
      insOrder.push_back( 0 ); // S
      insOrder.push_back( 1 ); // N
      insOrder.push_back( 2 ); // W
    }
    else
    {
      insOrder.push_back( 0 ); // S
      insOrder.push_back( 2 ); // N
      insOrder.push_back( 1 ); // E
      insOrder.push_back( 3 ); // W
    }

    for ( size_t g = 0, cntG = m_geometry.size(); g < cntG; ++g )
    {
      // all the polygons in the geometry have 4 vertices CCW ordered
      // and they are stored in m_geometry in the order:
      // S-N-W   for T crossroads
      // S-E-N-W for X crossroads
      for ( size_t v = 0; v < 4; ++v )
      {
        const CGPoint2& point = m_geometry[insOrder[g]].Vertex( v );
        obj.NewPoint()->SetPoint( Vector3( (Coord)point.GetX(),
                                           (Coord)ZERO, 
                                           (Coord)point.GetY() ) );
      }
    }

    // textures
    for ( size_t t = 0, cntT = m_textures.size(); t < cntT; ++t )
    {
      size_t i = insOrder[t];
      if ( m_textures[i] != "" )
      {
        // forces automatic faces creation
        indices.Clear();
        for ( size_t v = 0; v < 4; ++v )
        {
          indices.Add( (i * 4) + v );
        }

        ObjToolTopology& topology = obj.GetTool< ObjToolTopology >();
        topology.TessellatePolygon( indices.Size(), indices.Data() );

        // saves the textures on disk
        String modelPath = destPath;

        // texturePath is absolute, it is used as filename during
        // save operation
        String texturePath = modelPath + "\\data";

        int pos = modelPath.Find( ':' ) + 2;
        modelPath = modelPath.Substring( pos, modelPath.GetLength() );

        // texture is relative, it will be saved inside the p3d file
        String texture = String( m_textures[i].c_str() );
        int slashPos = 0;
        while ( slashPos != -1 )
        {
          slashPos = texture.Find( "\\" );
          if ( slashPos != -1 )
          {
            texture = texture.Substring( slashPos + 1, texture.GetLength() );
          }
        }
        texturePath = texturePath + "\\" + texture;

        if ( texture[0] == '#' )
        {
          // if procedural texture we don't need any path
        }
        else
        {
          texture = modelPath + "\\data\\" + texture;

          // copy the textures on disk.
          // first tests if the texture has been already saved 
          // (it maybe be shared with other models)
          // source textures can be everywhere, destination textures
          // will be saved in a \data subfolder of the folder containing
          // the models
          ifstream test( texturePath, std::ios::binary );
          if ( test.fail() )
          {
            ifstream ifs( m_textures[i].c_str(), std::ios::binary );
            if ( !ifs.fail() )
            {
              ofstream ofs( texturePath, std::ios::binary );

              const int BUFFER_SIZE = 4096;
              char buffer[BUFFER_SIZE];

              while ( !ifs.eof() ) 
              {
                ifs.read( buffer, BUFFER_SIZE );
                if ( !ifs.bad() ) 
                {
                  ofs.write( buffer, ifs.gcount() );
                }
              }

              ifs.close();
              ofs.close();
            }
          }
          else
          {
            test.close();
          }
        }

        // texture mapping
        Float rightLength = m_geometry[i].Vertex( 0 ).Distance( m_geometry[i].Vertex( 1 ) );
        Float leftLength  = m_geometry[i].Vertex( 3 ).Distance( m_geometry[i].Vertex( 2 ) );
        Float maxLength = max( rightLength, leftLength );

        // first, redefines the faces
        // first triangle
        FaceT face1( obj, (t * 2) + 0 );
        face1.SetPoint( 0, (t * 4) + 0 );
        face1.SetPoint( 1, (t * 4) + 2 );
        face1.SetPoint( 2, (t * 4) + 3 );

        // second triangle
        FaceT face2( obj, (t * 2) + 1 );
        face2.SetPoint( 0, (t * 4) + 0 );
        face2.SetPoint( 1, (t * 4) + 1 );
        face2.SetPoint( 2, (t * 4) + 2 );

        // texture coords
        Float uLeft    = ZERO;
        Float uRight   = ONE;
        Float vTopRight = ONE - rightLength / maxLength;
        Float vTopLeft  = ONE - leftLength / maxLength;
        Float vBottom   = ONE;

        // now, remaps the faces
        face1.SetU( 0, (float)uRight );
        face1.SetV( 0, (float)vTopRight );

        face1.SetU( 1, (float)uLeft );
        face1.SetV( 1, (float)vBottom );

        face1.SetU( 2, (float)uLeft );
        face1.SetV( 2, (float)vTopLeft );

        face2.SetU( 0, (float)uRight );
        face2.SetV( 0, (float)vTopRight );

        face2.SetU( 1, (float)uRight );
        face2.SetV( 1, (float)vBottom );

        face2.SetU( 2, (float)uLeft );
        face2.SetV( 2, (float)vBottom );

        if ( resolution != LOD_ROADWAY )
        {
          face1.SetTexture( texture );
          face2.SetTexture( texture );

#ifdef _VBS_ONLY_
          face1.SetMaterial( DEFAULT_RVMAT );
          face2.SetMaterial( DEFAULT_RVMAT );
#endif

        }
      }
    }

    obj.RecalcNormals();
  }
  else // memory LOD
  {
    // geometry
    vector< CGPoint2 > points;
    for ( size_t g = 0, cntG = m_geometry.size(); g < cntG; ++g )
    {
      points.push_back( m_geometry[g].Vertex( 0 ) );
      points.push_back( m_geometry[g].Vertex( 3 ) );
    }
    for ( size_t i = 0, cntI = points.size(); i < cntI; ++i )
    {
      obj.NewPoint()->SetPoint( Vector3( (Coord)points[i].GetX(), 
                                         (Coord)ZERO, 
                                         (Coord)points[i].GetY() ) );
    }

    // selections
    RString netCodes[8] = { "PB", "LB", "LE", "PE", "LD", "LH", "PH", "PD" };

    vector< size_t > netIndices;
    if ( m_roads->Size() == 3 )
    {
      for ( size_t i = 0; i < 6; ++i )
      {
        netIndices.push_back( i );
      }
    }
    else
    {
      for ( size_t i = 0; i < 2; ++i )
      {
        netIndices.push_back( i );
      }
      for ( size_t i = 6; i < 8; ++i )
      {
        netIndices.push_back( i );
      }
      for ( size_t i = 2; i < 4; ++i )
      {
        netIndices.push_back( i );
      }
      for ( size_t i = 4; i < 6; ++i )
      {
        netIndices.push_back( i );
      }
    }

    for ( size_t i = 0, cntI = m_roads->Size() * 2; i < cntI; ++i )
    {
      Selection selNetCode( &obj );
      selNetCode.PointSelect( i );
      obj.SaveNamedSel( netCodes[netIndices[i]], &selNetCode );
    }
  }
}

//-----------------------------------------------------------------------------

bool CRightAnglesCrossroadTemplate::Equals( const CRoadBaseTemplate& other ) const
{
  return (CCrossroadTemplate::Equals( other ));
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CGenericAnglesCrossroadTemplate::CGenericAnglesCrossroadTemplate( CRoadsSet& roads, Float radius )
: CCrossroadTemplate( roads, radius )
, m_resolution( 1 )
{
  assert( (m_roads->Size() == 3) || (m_roads->Size() == 4) );
}

//-----------------------------------------------------------------------------

CGenericAnglesCrossroadTemplate::CGenericAnglesCrossroadTemplate( const CGenericAnglesCrossroadTemplate& other )
: CCrossroadTemplate( other )
, m_angles( other.m_angles )
, m_resolution( other.m_resolution )
{
}

//-----------------------------------------------------------------------------

CGenericAnglesCrossroadTemplate::~CGenericAnglesCrossroadTemplate()
{
}

//-----------------------------------------------------------------------------

bool CGenericAnglesCrossroadTemplate::ExportP3D( const String& destPath ) const
{
  LODObject lodObj;

  // Geometry LOD
  ObjectData* obj = new ObjectData();
  GenerateP3DLod( *obj, 1.0, destPath );

  // properties
  obj->SetNamedProp( "class", "road" );
  const String& mapP3D = m_roads->Get( 0 )->GetType()->GetMapP3D();
  if ( !mapP3D.IsEmpty() ) 
  { 
    obj->SetNamedProp( "map", mapP3D );
  }

  lodObj.InsertLevel( obj, 1.0 );
  lodObj.DeleteLevel( 0 );

  // RoadWay LOD
  obj = new ObjectData();
  GenerateP3DLod( *obj, LOD_ROADWAY, destPath );
  lodObj.InsertLevel( obj, LOD_ROADWAY );

  // Memory LOD
  obj = new ObjectData();
  GenerateP3DLod( *obj, LOD_MEMORY, destPath );
  lodObj.InsertLevel( obj, LOD_MEMORY );

  Pathname path = destPath + "\\" + Filename();
  if ( lodObj.Save( path, OBJDATA_LATESTVERSION, false, 0, 0 ) != 0 ) 
  {
    return (false);
  }
  else
  {
    return (true);
  }
}

//-----------------------------------------------------------------------------

void CGenericAnglesCrossroadTemplate::GenerateP3DLod( ObjectData& obj, float resolution,
                                                      const String& destPath ) const
{
  AutoArray< int > indices;

  if ( resolution != LOD_MEMORY )
  {
    // geometry
    // we force the generation of corner parts as first
    vector< size_t > insOrder;
    if ( m_geometry.size() == 3 )
    {
      insOrder.push_back( 0 ); // S
      insOrder.push_back( 1 ); // N
      insOrder.push_back( 2 ); // W
    }
    else if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 2 )->GetType() )
    {
      insOrder.push_back( 0 ); // S
      insOrder.push_back( 2 ); // N
      insOrder.push_back( 1 ); // E
      insOrder.push_back( 3 ); // W
    }
    else 
    {
      insOrder.push_back( 0 ); // S
      insOrder.push_back( 1 ); // E
      insOrder.push_back( 2 ); // N
      insOrder.push_back( 3 ); // W
    }

    for ( size_t g = 0, cntG = m_geometry.size(); g < cntG; ++g )
    {
      for ( size_t v = 0, cntV = m_geometry[insOrder[g]].VerticesCount(); v < cntV; ++v )
      {
        const CGPoint2& point = m_geometry[insOrder[g]].Vertex( v );
        obj.NewPoint()->SetPoint( Vector3( (Coord)point.GetX(),
                                           (Coord)ZERO, 
                                           (Coord)point.GetY() ) );
      }
    }

    // textures
    size_t verticesCounter = 0;
    size_t facesCounter = 0;
    for ( size_t t = 0, cntT = m_textures.size(); t < cntT; ++t )
    {
      size_t i = insOrder[t];
      if ( m_textures[i] != "" )
      {
        // forces automatic faces creation
        indices.Clear();
        for ( size_t v = 0, cntV = m_geometry[i].VerticesCount(); v < cntV; ++v )
        {
          indices.Add( verticesCounter );
          ++verticesCounter;
        }

        ObjToolTopology& topology = obj.GetTool< ObjToolTopology >();
        topology.TessellatePolygon( indices.Size(), indices.Data() );

        // saves the textures on disk
        String modelPath = destPath;

        // texturePath is absolute, it is used as filename during
        // save operation
        String texturePath = modelPath + "\\data";

        int pos = modelPath.Find( ':' ) + 2;
        modelPath = modelPath.Substring( pos, modelPath.GetLength() );

        // texture is relative, it will be saved inside the p3d file
        String texture = String( m_textures[i].c_str() );
        int slashPos = 0;
        while ( slashPos != -1 )
        {
          slashPos = texture.Find( "\\" );
          if ( slashPos != -1 )
          {
            texture = texture.Substring( slashPos + 1, texture.GetLength() );
          }
        }
        texturePath = texturePath + "\\" + texture;

        if ( texture[0] == '#' )
        {
          // if procedural texture we don't need any path
        }
        else
        {
          texture = modelPath + "\\data\\" + texture;

          // copy the textures on disk.
          // first tests if the texture has been already saved 
          // (it maybe be shared with other models)
          // source textures can be everywhere, destination textures
          // will be saved in a \data subfolder of the folder containing
          // the models
          ifstream test( texturePath, std::ios::binary );
          if ( test.fail() )
          {
            ifstream ifs( m_textures[i].c_str(), std::ios::binary );
            if ( !ifs.fail() )
            {
              ofstream ofs( texturePath, std::ios::binary );

              const int BUFFER_SIZE = 4096;
              char buffer[BUFFER_SIZE];

              while ( !ifs.eof() ) 
              {
                ifs.read( buffer, BUFFER_SIZE );
                if ( !ifs.bad() ) 
                {
                  ofs.write( buffer, ifs.gcount() );
                }
              }

              ifs.close();
              ofs.close();
            }
          }
          else
          {
            test.close();
          }
        }

        // texture mapping
        // we consider the model as represented by n = vCount / 2 - 1 
        // rectangles, each one composed by two triangles.
        size_t vCount = m_geometry[i].VerticesCount();
        size_t res = vCount / 2 - 1;
        Float step = ONE / res;
        for ( size_t f = 0; f < res; ++f )
        {
          // first triangle
          FaceT face1( obj, facesCounter );
          face1.SetPoint( 0, indices[f] );
          face1.SetPoint( 1, indices[f + 1] );
          face1.SetPoint( 2, indices[vCount - 1 - f] );
          ++facesCounter;

          // second triangle
          FaceT face2( obj, facesCounter );
          face2.SetPoint( 0, indices[f + 1] );
          face2.SetPoint( 1, indices[vCount - 2 - f] );
          face2.SetPoint( 2, indices[vCount - 1 - f] );
          ++facesCounter;

          Float uLeft  = ZERO;
          Float uRight = ONE;

          Float vBottom = (f + 1) * step - ONE;
          Float vTop    = f * step - ONE;

          face1.SetU( 0, (float)uRight );
          face1.SetV( 0, (float)vTop );

          face1.SetU( 1, (float)uRight );
          face1.SetV( 1, (float)vBottom );

          face1.SetU( 2, (float)uLeft );
          face1.SetV( 2, (float)vTop );

          face2.SetU( 0, (float)uRight );
          face2.SetV( 0, (float)vBottom );

          face2.SetU( 1, (float)uLeft );
          face2.SetV( 1, (float)vBottom );

          face2.SetU( 2, (float)uLeft );
          face2.SetV( 2, (float)vTop );

          if ( resolution != LOD_ROADWAY )
          {
            face1.SetTexture( texture );
            face2.SetTexture( texture );

#ifdef _VBS_ONLY_
            face1.SetMaterial( DEFAULT_RVMAT );
            face2.SetMaterial( DEFAULT_RVMAT );
#endif

          }
        }
      }
    }

    obj.RecalcNormals();
  }
  else // memory LOD
  {
    // geometry
    vector< CGPoint2 > points;
    for ( size_t g = 0, cntG = m_geometry.size(); g < cntG; ++g )
    {
      points.push_back( m_geometry[g].Vertex( 0 ) );
      points.push_back( m_geometry[g].Vertex( m_geometry[g].VerticesCount() - 1 ) );
    }
    for ( size_t i = 0, cntI = points.size(); i < cntI; ++i )
    {
      obj.NewPoint()->SetPoint( Vector3( (Coord)points[i].GetX(),
                                         (Coord)ZERO, 
                                         (Coord)points[i].GetY() ) );
    }

    // selections
    RString netCodes[8] = { "PB", "LB", "LE", "PE", "LD", "LH", "PH", "PD" };

    vector< size_t > netIndices;
    if ( m_roads->Size() == 3 )
    {
      for ( size_t i = 0; i < 6; ++i )
      {
        netIndices.push_back( i );
      }
    }
    else
    {
      for ( size_t i = 0; i < 2; ++i )
      {
        netIndices.push_back( i );
      }
      for ( size_t i = 6; i < 8; ++i )
      {
        netIndices.push_back( i );
      }
      for ( size_t i = 2; i < 4; ++i )
      {
        netIndices.push_back( i );
      }
      for ( size_t i = 4; i < 6; ++i )
      {
        netIndices.push_back( i );
      }
    }

    for ( size_t i = 0, cntI = m_roads->Size() * 2; i < cntI; ++i )
    {
      Selection selNetCode( &obj );
      selNetCode.PointSelect( i );
      obj.SaveNamedSel( netCodes[netIndices[i]], &selNetCode );
    }
  }
}

//-----------------------------------------------------------------------------

bool CGenericAnglesCrossroadTemplate::Equals( const CRoadBaseTemplate& other ) const
{
  const CGenericAnglesCrossroadTemplate* _other = reinterpret_cast< const CGenericAnglesCrossroadTemplate* >( &other );
  if ( m_angles.size() != _other->m_angles.size() ) { return (false); }
  for ( size_t i = 0, cntI = m_angles.size(); i < cntI; ++i )
  {
    if ( m_angles[i] != _other->m_angles[i] ) { return (false); }
  }
  return (CCrossroadTemplate::Equals( other ));
}

//-----------------------------------------------------------------------------

Float CGenericAnglesCrossroadTemplate::MaxAngle() const
{
  Float maxAngle = ZERO;

  for ( size_t i = 0, cntI = m_angles.size(); i < cntI; ++i )
  {
    if ( maxAngle < m_angles[i] )
    {
      maxAngle = m_angles[i];
    }
  }

  return (maxAngle);
}

//-----------------------------------------------------------------------------

int CGenericAnglesCrossroadTemplate::MaxAngleIndex() const
{
  int maxAngleIndex = -1;
  Float maxAngle = ZERO;

  for ( size_t i = 0, cntI = m_angles.size(); i < cntI; ++i )
  {
    if ( maxAngle < m_angles[i] )
    {
      maxAngle = m_angles[i];
      maxAngleIndex = i;
    }
  }

  return (maxAngleIndex);
}

//-----------------------------------------------------------------------------

vector< CGPolygon2 > CGenericAnglesCrossroadTemplate::GenerateCorner( const CRoad* lowerRoad, 
                                                                      const CRoad* upperRoad,
                                                                      Float angleBetweenRoads )
{
  // the width is unique because the two roads should be of the same type
  Float halfWidth = HALF * lowerRoad->GetType()->GetWidth();

  // lower road properties
  CGSegment2 seg1L       = lowerRoad->GetPath().FirstSegment();
  CGVector2  tangVecL    = -CGVector2( seg1L.Direction() );
  CGVector2  normCWVecL  = tangVecL.PerpCW();
  CGVector2  normCCWVecL = tangVecL.PerpCCW();
  normCWVecL  *= halfWidth;
  normCCWVecL *= halfWidth;

/*
  CGPoint2 midL;
  CGCircle2 circL( seg1L.GetFrom(), m_radius );
  if ( circL.Contains( seg1L.GetTo(), false, 0.01 ) )
  {
    CGVector2 v = -tangVecL;
    v *= m_radius;
    midL = v.Head( seg1L.GetFrom() );
  }
  else
  {
    midL = seg1L.GetTo();
  }
*/
  CGVector2 vL = -tangVecL;
  vL *= m_radius;
  CGPoint2 midL = vL.Head( seg1L.GetFrom() );

  CGPoint2 rightL = normCWVecL.Head( midL );
  CGPoint2 leftL  = normCCWVecL.Head( midL );
  CGLine2 perpL   = CGLine2( midL, rightL );

  // upper road properties
  CGSegment2 seg1U       = upperRoad->GetPath().FirstSegment();
  CGVector2  tangVecU    = -CGVector2( seg1U.Direction() );
  CGVector2  normCCWVecU = tangVecU.PerpCCW();
  normCCWVecU *= halfWidth;

/*
  CGPoint2 midU;
  CGCircle2 circU( seg1U.GetFrom(), m_radius );
  if ( circU.Contains( seg1U.GetTo(), false, 0.01 ) )
  {
    CGVector2 v = -tangVecU;
    v *= m_radius;
    midU = v.Head( seg1U.GetFrom() );
  }
  else
  {
    midU = seg1U.GetTo();
  }
*/

  CGVector2 vU = -tangVecU;
  vU *= m_radius;
  CGPoint2 midU = vU.Head( seg1U.GetFrom() );

  CGPoint2 leftU  = normCCWVecU.Head( midU );
  CGLine2 perpU = CGLine2( midU, leftU );

  // corner properties
  CGPoint2 centerInWorldSpace;
  Intersect( perpL, perpU, centerInWorldSpace );

  Float leftRadiusL  = centerInWorldSpace.Distance( leftL );
  Float rightRadiusL = centerInWorldSpace.Distance( rightL );

  Float angle  = abs( CGConsts::PI - angleBetweenRoads );
  Float radius = HALF * ( leftRadiusL + rightRadiusL );

  Float len = angle * radius;
  Float res = lowerRoad->GetType()->GetCornerResolution();
  m_resolution = 1;
  while ( len / m_resolution > res ) { m_resolution *= 2; }

  Float angleStep = angle / m_resolution;

  CGPoint2 center;
  if ( rightRadiusL < leftRadiusL ) 
  { 
    center = CGPoint2( radius, -m_radius );
    angle     = -angle; 
    angleStep = -angleStep; 
  }
  else
  {
    center = CGPoint2( -radius, -m_radius );
  }

  CGPolygon2 lowerPoly;
  CGPolygon2 upperPoly;

  // lower road
  // right side
  for ( size_t i = 0, cntI = m_resolution / 2; i <= cntI; ++i )
  {
    CGPoint2 point( halfWidth, -m_radius );
    point.Rotate( center, i * angleStep );
    lowerPoly.AppendVertex( point.GetX(), point.GetY() );
  }
  // left side
  for ( size_t i = 0, cntI = m_resolution / 2; i <= cntI; ++i )
  {
    CGPoint2 point( -halfWidth, -m_radius );
    point.Rotate( center, HALF * angle - i * angleStep );
    lowerPoly.AppendVertex( point.GetX(), point.GetY() );
  }

  // upper road
  // right side
  for ( size_t i = 0, cntI = m_resolution / 2; i <= cntI; ++i )
  {
    CGPoint2 point( -halfWidth, -m_radius );
    point.Rotate( center, angle - i * angleStep );
    upperPoly.AppendVertex( point.GetX(), point.GetY() );
  }
  // left side
  for ( size_t i = 0, cntI = m_resolution / 2; i <= cntI; ++i )
  {
    CGPoint2 point( halfWidth, -m_radius );
    point.Rotate( center, HALF * angle + i * angleStep );
    upperPoly.AppendVertex( point.GetX(), point.GetY() );
  }

  vector< CGPolygon2 > ret;
  ret.push_back( lowerPoly );
  ret.push_back( upperPoly );

  return (ret);
}

//-----------------------------------------------------------------------------

void CGenericAnglesCrossroadTemplate::RotateAngles( size_t rotationsCount )
{
  if ( m_angles.size() > 0 )
  {
    for ( size_t i = 0; i < rotationsCount; ++i )
    {
      Float temp = m_angles[0];
      for ( size_t a = 0, cntA = m_angles.size() - 1; a < cntA; ++a )
      {
        m_angles[a] = m_angles[a + 1];
      }
      m_angles[m_angles.size() - 1] = temp;
    }
  }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CTRightAnglesCrossroadTemplate::CTRightAnglesCrossroadTemplate( CRoadsSet& roads, Float radius )
: CRightAnglesCrossroadTemplate( roads, radius )
{
  Initialize();
}

//-----------------------------------------------------------------------------

CTRightAnglesCrossroadTemplate::CTRightAnglesCrossroadTemplate( const CTRightAnglesCrossroadTemplate& other )
: CRightAnglesCrossroadTemplate( other )
{
}

//-----------------------------------------------------------------------------

CTRightAnglesCrossroadTemplate::~CTRightAnglesCrossroadTemplate()
{
}

//-----------------------------------------------------------------------------

bool CTRightAnglesCrossroadTemplate::Equals( const CRoadBaseTemplate& other ) const
{
  if ( other.Type() != TT_T_RigAngCrossroad ) { return (false); }
  return (CRightAnglesCrossroadTemplate::Equals( other ));
}

//-----------------------------------------------------------------------------

ETemplateType CTRightAnglesCrossroadTemplate::Type() const
{
  return (TT_T_RigAngCrossroad);
}

//-----------------------------------------------------------------------------

String CTRightAnglesCrossroadTemplate::Name() const
{
  // assumes that the roads are already sorted in the order S-N-W
  String ret = "kr_t";

  for ( size_t i = 0; i < 3; ++i )
  {
    ret = ret + "_" + m_roads->Get( i )->GetType()->ExportPrefix();
  }

  return (ret);
}

//-----------------------------------------------------------------------------

void CTRightAnglesCrossroadTemplate::CalcGeometry()
{
  Float directions[3];
  for ( size_t i = 0; i < 3; ++i )
  {
    directions[i] = m_roads->Get( i )->GetPath().FirstSegment().Direction();
  }

  Float angles[3];
  Float sum = ZERO;
  for ( size_t i = 0; i < 2; ++i )
  {
    angles[i] = directions[i + 1] - directions[i];
    sum += angles[i];
  }
  angles[2] = CGConsts::TWO_PI - sum;

  Float maxAngle = ZERO;
  int maxAngleIdx = -1;
  if ( m_roads->TypesCount() == 1 )
  {
    // all the three roads are of the same type
    // we consider as main road the alignement of the two roads having the
    // greatest comprised angle 

    // searches for the biggest angle
    for ( size_t i = 0; i < 3; ++i )
    {
      if ( maxAngle < angles[i] )
      {
        maxAngle    = angles[i];
        maxAngleIdx = i;
      }
    }
  }
  else
  {
    // two roads are of the same type, the third is of a different type
    // we consider as main road the alignement of the two roads having the same type
    if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 1 )->GetType() )
    {
      maxAngle    = angles[0];
      maxAngleIdx = 0;
    }
    else if ( m_roads->Get( 1 )->GetType() == m_roads->Get( 2 )->GetType() )
    {
      maxAngle    = angles[1];
      maxAngleIdx = 1;
    }
    else if ( m_roads->Get( 2 )->GetType() == m_roads->Get( 0 )->GetType() )
    {
      maxAngle    = angles[2];
      maxAngleIdx = 2;
    }
  }

  CRoad* roadN = NULL;
  CRoad* roadS = NULL;
  CRoad* roadW = NULL;
  switch ( maxAngleIdx )
  {
  case 0:
    {
      roadS = m_roads->Get( 0 );
      roadN = m_roads->Get( 1 );
      roadW = m_roads->Get( 2 );
    }
    break;
  case 1:
    {
      roadS = m_roads->Get( 1 );
      roadN = m_roads->Get( 2 );
      roadW = m_roads->Get( 0 );
    }
    break;
  case 2:
    {
      roadS = m_roads->Get( 2 );
      roadN = m_roads->Get( 0 );
      roadW = m_roads->Get( 1 );
    }
    break;
  }
  assert( roadS && roadN && roadW );

  // alignes the two roads forming the main road
  Float correctionNS = HALF * (CGConsts::PI - maxAngle);
  roadN->GetPath().Vertex( 1 ).Rotate( roadN->GetPath().FirstVertex(), correctionNS );
  roadS->GetPath().Vertex( 1 ).Rotate( roadS->GetPath().FirstVertex(), -correctionNS );

  // perpendicularizes the other road
  int nextAngle = maxAngleIdx + 1;
  if ( nextAngle > 2 ) { nextAngle = 0; }
  Float correctionW = CGConsts::HALF_PI - (angles[nextAngle] - correctionNS);
  roadW->GetPath().Vertex( 1 ).Rotate( roadW->GetPath().FirstVertex(), correctionW );

  // updates roads' constraints
  roadN->SetStartDirectionConstraint( roadN->GetPath().FirstSegment().Direction() );
  roadS->SetStartDirectionConstraint( roadS->GetPath().FirstSegment().Direction() );
  roadW->SetStartDirectionConstraint( roadW->GetPath().FirstSegment().Direction() );

  // removes endpoints from roads (crossroad position)
  if ( roadN->GetPath().VerticesCount() > 2 ) { roadN->GetPath().RemoveVertex( 0 ); }
  if ( roadS->GetPath().VerticesCount() > 2 ) { roadS->GetPath().RemoveVertex( 0 ); }
  if ( roadW->GetPath().VerticesCount() > 2 ) { roadW->GetPath().RemoveVertex( 0 ); }

  // re-sort the roads list to fit template's calculations
  m_roads->Set( 0, roadS );
  m_roads->Set( 1, roadN );
  m_roads->Set( 2, roadW );

  m_geometry.clear();
  m_textures.clear();

  Float halfWidthS = HALF * roadS->GetType()->GetWidth();
  Float halfWidthN = HALF * roadN->GetType()->GetWidth();
  Float halfWidthW = HALF * roadW->GetType()->GetWidth();

  string strTextureS = roadS->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureS = roadS->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureN = roadN->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureN = roadN->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureW = roadW->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureW = roadW->GetType()->GetTexture( MAIN_TER_TEX );

  // generates geometry
  CGPolygon2 polyS;
  CGPolygon2 polyN;
  CGPolygon2 polyW;

  if ( roadS->GetType() == roadN->GetType() )
  {
    // south
    polyS.AppendVertex( CGPoint2( halfWidthS, -m_radius ) ); 
    polyS.AppendVertex( CGPoint2( halfWidthS, ZERO ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, ZERO ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, -m_radius ) ); 
    m_geometry.push_back( polyS );
    m_textures.push_back( strTextureS );

    // north
    polyN.AppendVertex( CGPoint2( -halfWidthN, m_radius ) ); 
    polyN.AppendVertex( CGPoint2( -halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2( halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2( halfWidthN, m_radius ) ); 
    m_geometry.push_back( polyN );
    m_textures.push_back( strTextureN );

    // west
    polyW.AppendVertex( CGPoint2( -m_radius, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2( ZERO, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2( ZERO, halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2( -m_radius, halfWidthW ) ); 
    m_geometry.push_back( polyW );
    m_textures.push_back( terTextureW );
  }
  else if ( roadS->GetType() == roadW->GetType() )
  {
    // south
    polyS.AppendVertex( CGPoint2(  halfWidthS, -m_radius ) ); 
    polyS.AppendVertex( CGPoint2(  halfWidthS, halfWidthW ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, -halfWidthW ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, -m_radius ) ); 
    m_geometry.push_back( polyS );
    m_textures.push_back( strTextureS );

    // north
    polyN.AppendVertex( CGPoint2( -halfWidthN, m_radius ) ); 
    polyN.AppendVertex( CGPoint2( -halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2(  halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2(  halfWidthN, m_radius ) ); 
    m_geometry.push_back( polyN );
    m_textures.push_back( terTextureN );

    // west
    polyW.AppendVertex( CGPoint2(   -m_radius, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2( -halfWidthS, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2(  halfWidthS, halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2(   -m_radius, halfWidthW ) ); 
    m_geometry.push_back( polyW );
    m_textures.push_back( strTextureW );
  }
  else // roadN->GetType() == roadW->GetType()
  {
    // south
    polyS.AppendVertex( CGPoint2(  halfWidthS, -m_radius ) ); 
    polyS.AppendVertex( CGPoint2(  halfWidthS, ZERO ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, ZERO ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, -m_radius ) ); 
    m_geometry.push_back( polyS );
    m_textures.push_back( terTextureS );

    // north
    polyN.AppendVertex( CGPoint2( -halfWidthN, m_radius ) ); 
    polyN.AppendVertex( CGPoint2( -halfWidthN, halfWidthW ) ); 
    polyN.AppendVertex( CGPoint2(  halfWidthN, -halfWidthW ) ); 
    polyN.AppendVertex( CGPoint2(  halfWidthN, m_radius ) ); 
    m_geometry.push_back( polyN );
    m_textures.push_back( strTextureN );

    // west
    polyW.AppendVertex( CGPoint2(   -m_radius, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2(  halfWidthN, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2( -halfWidthN, halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2(   -m_radius, halfWidthW ) ); 
    m_geometry.push_back( polyW );
    m_textures.push_back( strTextureW );
  }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CXRightAnglesCrossroadTemplate::CXRightAnglesCrossroadTemplate( CRoadsSet& roads, Float radius )
: CRightAnglesCrossroadTemplate( roads, radius )
{
  Initialize();
}

//-----------------------------------------------------------------------------

CXRightAnglesCrossroadTemplate::CXRightAnglesCrossroadTemplate( const CXRightAnglesCrossroadTemplate& other )
: CRightAnglesCrossroadTemplate( other )
{
}

//-----------------------------------------------------------------------------

CXRightAnglesCrossroadTemplate::~CXRightAnglesCrossroadTemplate()
{
}

//-----------------------------------------------------------------------------

bool CXRightAnglesCrossroadTemplate::Equals( const CRoadBaseTemplate& other ) const
{
  if ( other.Type() != TT_X_RigAngCrossroad ) { return (false); }
  return (CRightAnglesCrossroadTemplate::Equals( other ));
}

//-----------------------------------------------------------------------------

ETemplateType CXRightAnglesCrossroadTemplate::Type() const
{
  return (TT_X_RigAngCrossroad);
}

//-----------------------------------------------------------------------------

String CXRightAnglesCrossroadTemplate::Name() const
{
  // assumes that the roads are already sorted in the order S-E-N-W
  String ret = "kr_x";

  for ( size_t i = 0; i < 4; ++i )
  {
    ret = ret + "_" + m_roads->Get( i )->GetType()->ExportPrefix();
  }

  return (ret);
}

//-----------------------------------------------------------------------------

void CXRightAnglesCrossroadTemplate::CalcGeometry()
{
  Float directions[4];
  for ( size_t i = 0; i < 4; ++i )
  {
    directions[i] = m_roads->Get( i )->GetPath().FirstSegment().Direction();
  }

  Float angle02 = directions[2] - directions[0];
  Float angle13 = directions[3] - directions[1];

  // alignes m_roads[0] and m_roads[2]
  Float correction02 = HALF * (CGConsts::PI - angle02);
  m_roads->Get( 2 )->GetPath().Vertex( 1 ).Rotate( m_roads->Get( 2 )->GetPath().FirstVertex(), correction02 );
  m_roads->Get( 0 )->GetPath().Vertex( 1 ).Rotate( m_roads->Get( 0 )->GetPath().FirstVertex(), -correction02 );

  // alignes m_roads[1] and m_roads[3]
  Float correction13 = HALF * (CGConsts::PI - angle13);
  m_roads->Get( 3 )->GetPath().Vertex( 1 ).Rotate( m_roads->Get( 3 )->GetPath().FirstVertex(), correction13 );
  m_roads->Get( 1 )->GetPath().Vertex( 1 ).Rotate( m_roads->Get( 1 )->GetPath().FirstVertex(), -correction13 );

  // perpendicularizes roads
  Float dir02 = m_roads->Get( 2 )->GetPath().FirstSegment().Direction();
  Float dir13 = m_roads->Get( 3 )->GetPath().FirstSegment().Direction();
  Float angle = dir13 - dir02;
  if ( angle < ZERO ) { angle += CGConsts::TWO_PI; }
  Float correction = HALF * (CGConsts::HALF_PI - angle);
  m_roads->Get( 2 )->GetPath().Vertex( 1 ).Rotate( m_roads->Get( 2 )->GetPath().FirstVertex(), -correction );
  m_roads->Get( 0 )->GetPath().Vertex( 1 ).Rotate( m_roads->Get( 0 )->GetPath().FirstVertex(), -correction );
  m_roads->Get( 3 )->GetPath().Vertex( 1 ).Rotate( m_roads->Get( 3 )->GetPath().FirstVertex(), correction );
  m_roads->Get( 1 )->GetPath().Vertex( 1 ).Rotate( m_roads->Get( 1 )->GetPath().FirstVertex(), correction );

  // re-sort roads in dependence of their width/type to fit template's calculations
  size_t count = m_roads->TypesCount();

  bool done = false;
  while ( !done )
  {
    // rotates the roads until we found a match
    if ( count == 1 )
    {
      if ( correction02 <= correction13 )
      {
        done = true;
      }
    }
    else if ( count == 2 )
    {
      if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 2 )->GetType() )
      {
        if ( m_roads->Get( 1 )->GetType() == m_roads->Get( 3 )->GetType() )
        {
          if ( m_roads->Get( 0 )->GetType()->GetWidth() >= m_roads->Get( 1 )->GetType()->GetWidth() )
          {
            done = true;
          }
        }
        else if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 3 )->GetType() )
        {
          done = true;
        }
      }
      else if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 3 )->GetType() &&
                m_roads->Get( 1 )->GetType() == m_roads->Get( 2 )->GetType() )
      {
        if ( m_roads->Get( 0 )->GetType()->GetWidth() >= m_roads->Get( 1 )->GetType()->GetWidth() )
        {
          done = true;
        }
      }
    }
    else if ( count == 3 )
    {
      if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 2 )->GetType() )
      {
        if ( m_roads->Get( 3 )->GetType()->GetWidth() >= m_roads->Get( 1 )->GetType()->GetWidth() )
        {
          done = true;
        }
      }
      else if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 3 )->GetType() )
      {
        done = true;
      }
    }

    if ( !done )
    {
      CRoad* tempR = m_roads->Get( 0 );
      m_roads->Set( 0, m_roads->Get( 1 ) );
      m_roads->Set( 1, m_roads->Get( 2 ) );
      m_roads->Set( 2, m_roads->Get( 3 ) );
      m_roads->Set( 3, tempR );

      Float temp = correction02;
      correction02 = correction13;
      correction13 = temp;
    }
  }

  // updates roads' constraints and enpoints
  for ( size_t i = 0; i < 4; ++i )
  {
    CRoad* road = m_roads->Get( i );
    road->SetStartDirectionConstraint( road->GetPath().FirstSegment().Direction() );
    if ( road->GetPath().VerticesCount() > 2 ) { road->GetPath().RemoveVertex( 0 ); }
  }

  m_geometry.clear();
  m_textures.clear();

  CRoad* roadS = m_roads->Get( 0 );
  CRoad* roadE = m_roads->Get( 1 );
  CRoad* roadN = m_roads->Get( 2 );
  CRoad* roadW = m_roads->Get( 3 );

  Float halfWidthS = HALF * roadS->GetType()->GetWidth();
  Float halfWidthE = HALF * roadE->GetType()->GetWidth();
  Float halfWidthN = HALF * roadN->GetType()->GetWidth();
  Float halfWidthW = HALF * roadW->GetType()->GetWidth();

  string strTextureS = roadS->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureS = roadS->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureE = roadE->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureE = roadE->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureN = roadN->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureN = roadN->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureW = roadW->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureW = roadW->GetType()->GetTexture( MAIN_TER_TEX );

  // generates geometry
  CGPolygon2 polyS;
  CGPolygon2 polyE;
  CGPolygon2 polyN;
  CGPolygon2 polyW;

  if ( roadS->GetType() == roadN->GetType() )
  {
    // south
    polyS.AppendVertex( CGPoint2(  halfWidthS, -m_radius ) ); 
    polyS.AppendVertex( CGPoint2(  halfWidthS, ZERO ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, ZERO ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, -m_radius ) ); 
    m_geometry.push_back( polyS );
    m_textures.push_back( strTextureS );

    // east
    polyE.AppendVertex( CGPoint2( m_radius, halfWidthE ) ); 
    polyE.AppendVertex( CGPoint2(     ZERO, halfWidthE ) ); 
    polyE.AppendVertex( CGPoint2(     ZERO, -halfWidthE ) ); 
    polyE.AppendVertex( CGPoint2( m_radius, -halfWidthE ) ); 
    m_geometry.push_back( polyE );
    m_textures.push_back( terTextureE );

    // north
    polyN.AppendVertex( CGPoint2( -halfWidthN, m_radius ) ); 
    polyN.AppendVertex( CGPoint2( -halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2(  halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2(  halfWidthN, m_radius ) ); 
    m_geometry.push_back( polyN );
    m_textures.push_back( strTextureN );

    // west
    polyW.AppendVertex( CGPoint2( -m_radius, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2(      ZERO, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2(      ZERO, halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2( -m_radius, halfWidthW ) ); 
    m_geometry.push_back( polyW );
    m_textures.push_back( terTextureW );
  }
  else if ( roadS->GetType() == roadW->GetType() )
  {
    // south
    polyS.AppendVertex( CGPoint2( halfWidthS, -m_radius ) ); 
    polyS.AppendVertex( CGPoint2( halfWidthS,  halfWidthW ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, -halfWidthW ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, -m_radius ) ); 
    m_geometry.push_back( polyS );
    m_textures.push_back( strTextureS );

    // east
    polyE.AppendVertex( CGPoint2( m_radius, halfWidthE ) ); 
    polyE.AppendVertex( CGPoint2(     ZERO, halfWidthE ) ); 
    polyE.AppendVertex( CGPoint2(     ZERO, -halfWidthE ) ); 
    polyE.AppendVertex( CGPoint2( m_radius, -halfWidthE ) ); 
    m_geometry.push_back( polyE );
    m_textures.push_back( terTextureE );

    // north
    polyN.AppendVertex( CGPoint2( -halfWidthN, m_radius ) ); 
    polyN.AppendVertex( CGPoint2( -halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2(  halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2(  halfWidthN, m_radius ) ); 
    m_geometry.push_back( polyN );
    m_textures.push_back( terTextureN );

    // west
    polyW.AppendVertex( CGPoint2(   -m_radius, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2( -halfWidthS, -halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2(  halfWidthS, halfWidthW ) ); 
    polyW.AppendVertex( CGPoint2(   -m_radius, halfWidthW ) ); 
    m_geometry.push_back( polyW );
    m_textures.push_back( strTextureW );
  }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CTGenericAnglesCrossroadTemplate::CTGenericAnglesCrossroadTemplate( CRoadsSet& roads, Float radius )
: CGenericAnglesCrossroadTemplate( roads, radius )
{
  Initialize();
}

//-----------------------------------------------------------------------------

CTGenericAnglesCrossroadTemplate::CTGenericAnglesCrossroadTemplate( const CTGenericAnglesCrossroadTemplate& other )
: CGenericAnglesCrossroadTemplate( other )
{
}

//-----------------------------------------------------------------------------

CTGenericAnglesCrossroadTemplate::~CTGenericAnglesCrossroadTemplate()
{
}

//-----------------------------------------------------------------------------

bool CTGenericAnglesCrossroadTemplate::Equals( const CRoadBaseTemplate& other ) const
{
  if ( other.Type() != TT_T_GenAngCrossroad ) { return (false); }
  return (CGenericAnglesCrossroadTemplate::Equals( other ));
}

//-----------------------------------------------------------------------------

ETemplateType CTGenericAnglesCrossroadTemplate::Type() const
{
  return (TT_T_GenAngCrossroad);
}

//-----------------------------------------------------------------------------

String CTGenericAnglesCrossroadTemplate::Name() const
{
  // assumes that the roads are already sorted in the order S-N-W
  String ret = "kr_gt";

  for ( size_t i = 0; i < 3; ++i )
  {
    ret = ret + "_" + m_roads->Get( i )->GetType()->ExportPrefix();
  }

  for ( size_t i = 0; i < 3; ++i )
  {
    Float angDeg = m_angles[i] * CGConsts::RAD_TO_DEG;
    ret = ret + "_A" + String( FormatNumber( angDeg ).c_str() );
  }

  return (ret);
}

//-----------------------------------------------------------------------------

void CTGenericAnglesCrossroadTemplate::CalcGeometry()
{
  Float directions[3];
  for ( size_t i = 0; i < 3; ++i )
  {
    directions[i] = m_roads->Get( i )->GetPath().FirstSegment().Direction();
  }

  Float sum = ZERO;
  m_angles.clear();
  for ( size_t i = 0; i < 2; ++i )
  {
    m_angles.push_back( directions[i + 1] - directions[i] );
    sum += m_angles[m_angles.size() - 1];
  }
  m_angles.push_back( CGConsts::TWO_PI - sum );

  int southIdx = -1;
  if ( m_roads->TypesCount() == 1 )
  {
    // all the three roads are of the same type
    // we consider as main road the alignement of the two roads having the
    // greatest comprised angle 

    southIdx = MaxAngleIndex();
  }
  else
  {
    // two roads are of the same type, the third is of a different type
    // we consider as main road the alignement of the two roads having the same type
    if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 1 )->GetType() )
    {
      southIdx = 0;
    }
    else if ( m_roads->Get( 1 )->GetType() == m_roads->Get( 2 )->GetType() )
    {
      southIdx = 1;
    }
    else if ( m_roads->Get( 2 )->GetType() == m_roads->Get( 0 )->GetType() )
    {
      southIdx = 2;
    }
  }

  assert( southIdx != -1 );

  CRoad* roadN = NULL;
  CRoad* roadS = NULL;
  CRoad* roadW = NULL;

  switch ( southIdx )
  {
  case 0:
    {
      roadS = m_roads->Get( 0 );
      roadN = m_roads->Get( 1 );
      roadW = m_roads->Get( 2 );
    }
    break;
  case 1:
    {
      roadS = m_roads->Get( 1 );
      roadN = m_roads->Get( 2 );
      roadW = m_roads->Get( 0 );
    }
    break;
  case 2:
    {
      roadS = m_roads->Get( 2 );
      roadN = m_roads->Get( 0 );
      roadW = m_roads->Get( 1 );
    }
    break;
  }
  RotateAngles( southIdx );
  assert( roadS && roadN && roadW );

  m_geometry.clear();
  m_textures.clear();

  Float halfWidthS = HALF * roadS->GetType()->GetWidth();
  Float halfWidthN = HALF * roadN->GetType()->GetWidth();
  Float halfWidthW = HALF * roadW->GetType()->GetWidth();

  string strTextureS = roadS->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureS = roadS->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureN = roadN->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureN = roadN->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureW = roadW->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureW = roadW->GetType()->GetTexture( MAIN_TER_TEX );

  // generates geometry
  CGPolygon2 polyS;
  CGPolygon2 polyN;
  CGPolygon2 polyW;

  // we want to connect S and N roads with a corner so we search for the
  // center of the corner if any (if the two roads are not already aligned)
  if ( abs( m_angles[0] - CGConsts::PI ) > TOLERANCE )
  {
    vector< CGPolygon2 > polygs = GenerateCorner( roadS, roadN, m_angles[0] );

    polyS = polygs[0];
    m_geometry.push_back( polyS );
    m_textures.push_back( strTextureS );

    polyN = polygs[1];
    m_geometry.push_back( polyN );
    m_textures.push_back( strTextureN );
  }
  else
  {
    // roads are aligned
    // south
    polyS.AppendVertex( CGPoint2( halfWidthS, -m_radius ) ); 
    polyS.AppendVertex( CGPoint2( halfWidthS, ZERO ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, ZERO ) ); 
    polyS.AppendVertex( CGPoint2( -halfWidthS, -m_radius ) ); 
    m_geometry.push_back( polyS );
    m_textures.push_back( strTextureS );

    // north
    polyN.AppendVertex( CGPoint2( -halfWidthN, m_radius ) ); 
    polyN.AppendVertex( CGPoint2( -halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2( halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2( halfWidthN, m_radius ) ); 
    m_geometry.push_back( polyN );
    m_textures.push_back( strTextureN );
  }

  // west
  polyW.AppendVertex( CGPoint2( halfWidthW, -m_radius ) ); 
  polyW.AppendVertex( CGPoint2( halfWidthW, ZERO ) ); 
  polyW.AppendVertex( CGPoint2( -halfWidthW, ZERO ) ); 
  polyW.AppendVertex( CGPoint2( -halfWidthW, -m_radius ) ); 
  polyW.Rotate( CGPoint2( ZERO, ZERO ), -m_angles[2] );
  m_geometry.push_back( polyW );
  m_textures.push_back( terTextureW );

  // updates roads' constraints
  roadN->SetStartDirectionConstraint( roadN->GetPath().FirstSegment().Direction() );
  roadS->SetStartDirectionConstraint( roadS->GetPath().FirstSegment().Direction() );
  roadW->SetStartDirectionConstraint( roadW->GetPath().FirstSegment().Direction() );

  // removes endpoints from roads (crossroad position)
  if ( roadN->GetPath().VerticesCount() > 2 ) { roadN->GetPath().RemoveVertex( 0 ); }
  if ( roadS->GetPath().VerticesCount() > 2 ) { roadS->GetPath().RemoveVertex( 0 ); }
  if ( roadW->GetPath().VerticesCount() > 2 ) { roadW->GetPath().RemoveVertex( 0 ); }

  // re-sort the roads list to fit template's calculations
  m_roads->Set( 0, roadS );
  m_roads->Set( 1, roadN );
  m_roads->Set( 2, roadW );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CXGenericAnglesCrossroadTemplate::CXGenericAnglesCrossroadTemplate( CRoadsSet& roads, Float radius )
: CGenericAnglesCrossroadTemplate( roads, radius )
{
  Initialize();
}

//-----------------------------------------------------------------------------

CXGenericAnglesCrossroadTemplate::CXGenericAnglesCrossroadTemplate( const CXGenericAnglesCrossroadTemplate& other )
: CGenericAnglesCrossroadTemplate( other )
{
}

//-----------------------------------------------------------------------------

CXGenericAnglesCrossroadTemplate::~CXGenericAnglesCrossroadTemplate()
{
}

//-----------------------------------------------------------------------------

bool CXGenericAnglesCrossroadTemplate::Equals( const CRoadBaseTemplate& other ) const
{
  if ( other.Type() != TT_X_GenAngCrossroad ) { return (false); }
  return (CGenericAnglesCrossroadTemplate::Equals( other ));
}

//-----------------------------------------------------------------------------

ETemplateType CXGenericAnglesCrossroadTemplate::Type() const
{
  return (TT_X_GenAngCrossroad);
}

//-----------------------------------------------------------------------------

String CXGenericAnglesCrossroadTemplate::Name() const
{
  // assumes that the roads are already sorted in the order S-E-N-W
  String ret = "kr_gx";

  for ( size_t i = 0; i < 4; ++i )
  {
    ret = ret + "_" + m_roads->Get( i )->GetType()->ExportPrefix();
  }

  for ( size_t i = 0; i < 4; ++i )
  {
    Float angDeg = m_angles[i] * CGConsts::RAD_TO_DEG;
    ret = ret + "_A" + String( FormatNumber( angDeg ).c_str() );
  }

  return (ret);
}

//-----------------------------------------------------------------------------

void CXGenericAnglesCrossroadTemplate::CalcGeometry()
{
  Float directions[4];
  for ( size_t i = 0; i < 4; ++i )
  {
    directions[i] = m_roads->Get( i )->GetPath().FirstSegment().Direction();
  }

  Float sum = ZERO;
  m_angles.clear();
  for ( size_t i = 0; i < 3; ++i )
  {
    m_angles.push_back( directions[i + 1] - directions[i] );
    sum += m_angles[m_angles.size() - 1];
  }
  m_angles.push_back( CGConsts::TWO_PI - sum );

  Float correction02 = abs( CGConsts::PI - (directions[2] - directions[0]) );
  Float correction13 = abs( CGConsts::PI - (directions[3] - directions[1]) );

  // re-sort roads in dependence of their width/type to fit template's calculations
  size_t count = m_roads->TypesCount();

  bool done = false;
  while ( !done )
  {
    // rotates the roads until we found a match
    if ( count == 1 )
    {
      if ( correction02 <= correction13 )
      {
        done = true;
      }
    }
    else if ( count == 2 )
    {
      if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 2 )->GetType() )
      {
        if ( m_roads->Get( 1 )->GetType() == m_roads->Get( 3 )->GetType() )
        {
          if ( m_roads->Get( 0 )->GetType()->GetWidth() >= m_roads->Get( 1 )->GetType()->GetWidth() )
          {
            done = true;
          }
        }
        else if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 1 )->GetType() )
        {
          done = true;
        }
      }
      else if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 1 )->GetType() &&
                m_roads->Get( 2 )->GetType() == m_roads->Get( 3 )->GetType() )
      {
        if ( m_roads->Get( 0 )->GetType()->GetWidth() >= m_roads->Get( 2 )->GetType()->GetWidth() )
        {
          done = true;
        }
      }
    }
    else if ( count == 3 )
    {
      if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 2 )->GetType() )
      {
        if ( m_roads->Get( 3 )->GetType()->GetWidth() >= m_roads->Get( 1 )->GetType()->GetWidth() )
        {
          done = true;
        }
      }
      else if ( m_roads->Get( 0 )->GetType() == m_roads->Get( 1 )->GetType() )
      {
        done = true;
      }
    }

    if ( !done )
    {
      CRoad* tempR = m_roads->Get( 0 );
      m_roads->Set( 0, m_roads->Get( 1 ) );
      m_roads->Set( 1, m_roads->Get( 2 ) );
      m_roads->Set( 2, m_roads->Get( 3 ) );
      m_roads->Set( 3, tempR );

      Float temp   = correction02;
      correction02 = correction13;
      correction13 = temp;

      RotateAngles( 1 );
    }
  }

  m_geometry.clear();
  m_textures.clear();

  CRoad* roadS = m_roads->Get( 0 );
  CRoad* roadE = m_roads->Get( 1 );
  CRoad* roadN = m_roads->Get( 2 );
  CRoad* roadW = m_roads->Get( 3 );

  Float halfWidthS = HALF * roadS->GetType()->GetWidth();
  Float halfWidthE = HALF * roadE->GetType()->GetWidth();
  Float halfWidthN = HALF * roadN->GetType()->GetWidth();
  Float halfWidthW = HALF * roadW->GetType()->GetWidth();

  string strTextureS = roadS->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureS = roadS->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureE = roadE->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureE = roadE->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureN = roadN->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureN = roadN->GetType()->GetTexture( MAIN_TER_TEX );
  string strTextureW = roadW->GetType()->GetTexture( MAIN_STR_TEX );
  string terTextureW = roadW->GetType()->GetTexture( MAIN_TER_TEX );

  // generates geometry
  CGPolygon2 polyS;
  CGPolygon2 polyE;
  CGPolygon2 polyN;
  CGPolygon2 polyW;

  if ( roadS->GetType() == roadN->GetType() )
  {
    // we want to connect S and N roads with a corner so we search for the
    // center of the corner if any (if the two roads are not already aligned)
    Float totAngle = m_angles[0] + m_angles[1];
    if ( abs( totAngle - CGConsts::PI ) > TOLERANCE )
    {
      vector< CGPolygon2 > polygs = GenerateCorner( roadS, roadN, totAngle );

      // south
      polyS = polygs[0];
      m_geometry.push_back( polyS );
      m_textures.push_back( strTextureS );

      // east
      polyE.AppendVertex( CGPoint2( halfWidthE, -m_radius ) ); 
      polyE.AppendVertex( CGPoint2( halfWidthE, ZERO ) ); 
      polyE.AppendVertex( CGPoint2( -halfWidthE, ZERO ) ); 
      polyE.AppendVertex( CGPoint2( -halfWidthE, -m_radius ) ); 
      polyE.Rotate( CGPoint2( ZERO, ZERO ), m_angles[0] );
      m_geometry.push_back( polyE );
      m_textures.push_back( terTextureE );

      // north
      polyN = polygs[1];
      m_geometry.push_back( polyN );
      m_textures.push_back( strTextureN );

      // west
      polyW.AppendVertex( CGPoint2( halfWidthW, -m_radius ) ); 
      polyW.AppendVertex( CGPoint2( halfWidthW, ZERO ) ); 
      polyW.AppendVertex( CGPoint2( -halfWidthW, ZERO ) ); 
      polyW.AppendVertex( CGPoint2( -halfWidthW, -m_radius ) ); 
      polyW.Rotate( CGPoint2( ZERO, ZERO ), -m_angles[3] );
      m_geometry.push_back( polyW );
      m_textures.push_back( terTextureW );
    }
    else
    {
      // roads are aligned
      // south
      polyS.AppendVertex( CGPoint2( halfWidthS, -m_radius ) ); 
      polyS.AppendVertex( CGPoint2( halfWidthS, ZERO ) ); 
      polyS.AppendVertex( CGPoint2( -halfWidthS, ZERO ) ); 
      polyS.AppendVertex( CGPoint2( -halfWidthS, -m_radius ) ); 
      m_geometry.push_back( polyS );
      m_textures.push_back( strTextureS );

      // east
      polyE.AppendVertex( CGPoint2( halfWidthE, -m_radius ) ); 
      polyE.AppendVertex( CGPoint2( halfWidthE, ZERO ) ); 
      polyE.AppendVertex( CGPoint2( -halfWidthE, ZERO ) ); 
      polyE.AppendVertex( CGPoint2( -halfWidthE, -m_radius ) ); 
      polyE.Rotate( CGPoint2( ZERO, ZERO ), m_angles[0] );
      m_geometry.push_back( polyE );
      m_textures.push_back( terTextureE );

      // north
      polyN.AppendVertex( CGPoint2( -halfWidthN, m_radius ) ); 
      polyN.AppendVertex( CGPoint2( -halfWidthN, ZERO ) ); 
      polyN.AppendVertex( CGPoint2( halfWidthN, ZERO ) ); 
      polyN.AppendVertex( CGPoint2( halfWidthN, m_radius ) ); 
      m_geometry.push_back( polyN );
      m_textures.push_back( strTextureN );

      // west
      polyW.AppendVertex( CGPoint2( halfWidthW, -m_radius ) ); 
      polyW.AppendVertex( CGPoint2( halfWidthW, ZERO ) ); 
      polyW.AppendVertex( CGPoint2( -halfWidthW, ZERO ) ); 
      polyW.AppendVertex( CGPoint2( -halfWidthW, -m_radius ) ); 
      polyW.Rotate( CGPoint2( ZERO, ZERO ), -m_angles[3] );
      m_geometry.push_back( polyW );
      m_textures.push_back( terTextureW );
    }
  }
  else if ( roadS->GetType() == roadE->GetType() )
  {
    // we want to connect S and E roads with a corner so we search for the
    // center of the corner if any (if the two roads are not already aligned)
    if ( abs( m_angles[0] - CGConsts::PI ) > TOLERANCE )
    {
      vector< CGPolygon2 > polygs = GenerateCorner( roadS, roadE, m_angles[0] );

      // south
      polyS = polygs[0];
      m_geometry.push_back( polyS );
      m_textures.push_back( strTextureS );

      // east
      polyE = polygs[1];
      m_geometry.push_back( polyE );
      m_textures.push_back( strTextureE );
    }
    else
    {
      // roads are aligned
      // south
      polyS.AppendVertex( CGPoint2( halfWidthS, -m_radius ) ); 
      polyS.AppendVertex( CGPoint2( halfWidthS, ZERO ) ); 
      polyS.AppendVertex( CGPoint2( -halfWidthS, ZERO ) ); 
      polyS.AppendVertex( CGPoint2( -halfWidthS, -m_radius ) ); 
      m_geometry.push_back( polyS );
      m_textures.push_back( strTextureS );

      // east
      polyE.AppendVertex( CGPoint2( -halfWidthE, m_radius ) ); 
      polyE.AppendVertex( CGPoint2( -halfWidthE, ZERO ) ); 
      polyE.AppendVertex( CGPoint2( halfWidthE, ZERO ) ); 
      polyE.AppendVertex( CGPoint2( halfWidthE, m_radius ) ); 
      m_geometry.push_back( polyE );
      m_textures.push_back( strTextureE );
    }

    // north
    polyN.AppendVertex( CGPoint2( halfWidthN, -m_radius ) ); 
    polyN.AppendVertex( CGPoint2( halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2( -halfWidthN, ZERO ) ); 
    polyN.AppendVertex( CGPoint2( -halfWidthN, -m_radius ) ); 
    polyN.Rotate( CGPoint2( ZERO, ZERO ), m_angles[0] + m_angles[1] );
    m_geometry.push_back( polyN );
    m_textures.push_back( terTextureN );

    // west
    polyW.AppendVertex( CGPoint2( halfWidthW, -m_radius ) ); 
    polyW.AppendVertex( CGPoint2( halfWidthW, ZERO ) ); 
    polyW.AppendVertex( CGPoint2( -halfWidthW, ZERO ) ); 
    polyW.AppendVertex( CGPoint2( -halfWidthW, -m_radius ) ); 
    polyW.Rotate( CGPoint2( ZERO, ZERO ), -m_angles[3] );
    m_geometry.push_back( polyW );
    m_textures.push_back( terTextureW );
  }

  // updates roads' constraints and enpoints
  for ( size_t i = 0; i < 4; ++i )
  {
    CRoad* road = m_roads->Get( i );
    road->SetStartDirectionConstraint( road->GetPath().FirstSegment().Direction() );
    if ( road->GetPath().VerticesCount() > 2 ) { road->GetPath().RemoveVertex( 0 ); }
  }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CCrossroadTemplate* CCrossroadTemplatesLibrary::Append( const CCrossroadTemplate& crossroadTemplate )
{
  // first checks if already in the library
  CCrossroadTemplate* pLibTempl = Find( crossroadTemplate );
  if ( pLibTempl ) { return (pLibTempl); }

  // if not, add it to the library
  CCrossroadTemplate* pTempl = NULL;
  if ( crossroadTemplate.Type() == TT_T_RigAngCrossroad )
  {
    pTempl = new CTRightAnglesCrossroadTemplate( *(reinterpret_cast< const CTRightAnglesCrossroadTemplate* >( &crossroadTemplate )) );
  }
  else if ( crossroadTemplate.Type() == TT_X_RigAngCrossroad )
  {
    pTempl = new CXRightAnglesCrossroadTemplate( *(reinterpret_cast< const CXRightAnglesCrossroadTemplate* >( &crossroadTemplate )) );
  }
  else if ( crossroadTemplate.Type() == TT_T_GenAngCrossroad )
  {
    pTempl = new CTGenericAnglesCrossroadTemplate( *(reinterpret_cast< const CTGenericAnglesCrossroadTemplate* >( &crossroadTemplate )) );
  }
  else if ( crossroadTemplate.Type() == TT_X_GenAngCrossroad )
  {
    pTempl = new CXGenericAnglesCrossroadTemplate( *(reinterpret_cast< const CXGenericAnglesCrossroadTemplate* >( &crossroadTemplate )) );
  }

  assert( pTempl );

  if ( pTempl ) 
  { 
    m_elements.push_back( pTempl ); 
    return (m_elements[m_elements.size() - 1]);
  }

  // if we arrive here something went wrong
  return (NULL);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CRoadPartTemplate* CRoadPartTemplatesLibrary::Append( const CRoadPartTemplate& roadPartTemplate )
{
  // first checks if already in the library
  CRoadPartTemplate* pLibTempl = Find( roadPartTemplate );
  if ( pLibTempl ) { return (pLibTempl); }

  // if not, add it to the library
  CRoadPartTemplate* pTempl = NULL;
  if ( roadPartTemplate.Type() == TT_Straight )
  {
    pTempl = new CStraightTemplate( *(reinterpret_cast< const CStraightTemplate* >( &roadPartTemplate )) );
  }
  else if ( roadPartTemplate.Type() == TT_Corner )
  {
    pTempl = new CCornerTemplate( *(reinterpret_cast< const CCornerTemplate* >( &roadPartTemplate )) );
  }
  else if ( roadPartTemplate.Type() == TT_StrTerminator )
  {
    pTempl = new CStrTerminatorTemplate( *(reinterpret_cast< const CStrTerminatorTemplate* >( &roadPartTemplate )) );
  }

  assert( pTempl );

  if ( pTempl ) 
  { 
    m_elements.push_back( pTempl ); 
    return (m_elements[m_elements.size() - 1]);
  }

  // if we arrive here something went wrong
  return (NULL);
}

//-----------------------------------------------------------------------------

CRoadPartTemplate* CRoadPartTemplatesLibrary::FindDoubled( const CRoadPartTemplate& roadPartTemplate )
{
  const CRoadType* pRoadType = roadPartTemplate.GetRoadType();
  string tex = "";
  const vector< string >& textures = roadPartTemplate.GetTextures();
  if ( textures.size() > 0 ) { tex = textures[0]; }
  bool special = roadPartTemplate.IsSpecial();

  if ( roadPartTemplate.Type() == TT_Straight )
  {
    Float length = TWO * roadPartTemplate.Length();
    return (Find( CStraightTemplate( pRoadType, length, tex, special ) ));
  }
  else if ( roadPartTemplate.Type() == TT_Corner )
  {
    const CCornerTemplate& corner = reinterpret_cast< const CCornerTemplate& >( roadPartTemplate );
    Float radius = corner.GetRadius();
    Float angle  = TWO * corner.GetAngle();
    return (Find( CCornerTemplate( pRoadType, radius, angle, tex, special ) ));
  }

  return (NULL);
}

//-----------------------------------------------------------------------------

void CRoadPartTemplatesLibrary::RemoveUnused()
{
  size_t i = 0;
  while ( i < m_elements.size() )
  {
    if ( m_elements[i]->GetReferencesCount() == 0 )
    {
      m_elements.erase( m_elements.begin() + i );
      --i;
    }
    ++i;
  }
}

//-----------------------------------------------------------------------------

const CRoadPartTemplate* CRoadPartTemplatesLibrary::GetTerminator() const
{
  for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
  {
    if ( m_elements[i]->Type() == TT_StrTerminator )
    {
      return (m_elements[i]);
    }
  }

  return (NULL);
}

//-----------------------------------------------------------------------------

CRoadPartTemplate* CRoadPartTemplatesLibrary::GetTerminator()
{
  for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
  {
    if ( m_elements[i]->Type() == TT_StrTerminator )
    {
      return (m_elements[i]);
    }
  }

  return (NULL);
}

//-----------------------------------------------------------------------------

const CRoadPartTemplate* CRoadPartTemplatesLibrary::GetSquareStraight() const
{
  for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
  {
    if ( m_elements[i]->Type() == TT_Straight )
    {
      if ( m_elements[i]->GetChord().Length() == m_elements[i]->GetRoadType()->GetWidth() )
      {
        return (m_elements[i]);
      }
    }
  }

  return (NULL);
}

//-----------------------------------------------------------------------------

CRoadPartTemplate* CRoadPartTemplatesLibrary::GetSquareStraight()
{
  for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
  {
    if ( m_elements[i]->Type() == TT_Straight )
    {
      if ( m_elements[i]->GetChord().Length() == m_elements[i]->GetRoadType()->GetWidth() )
      {
        return (m_elements[i]);
      }
    }
  }

  return (NULL);
}

//-----------------------------------------------------------------------------

vector< const CRoadPartTemplate* > CRoadPartTemplatesLibrary::GetAllStraights() const
{
  vector< const CRoadPartTemplate* > ret;

  for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
  {
    if ( m_elements[i]->Type() == TT_Straight )
    {
      ret.push_back( m_elements[i] );
    }
  }

  return (ret);
}

//-----------------------------------------------------------------------------

vector< CRoadPartTemplate* > CRoadPartTemplatesLibrary::GetAllStraights()
{
  vector< CRoadPartTemplate* > ret;

  for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
  {
    if ( m_elements[i]->Type() == TT_Straight )
    {
      ret.push_back( m_elements[i] );
    }
  }

  return (ret);
}

//-----------------------------------------------------------------------------

vector< const CRoadPartTemplate* > CRoadPartTemplatesLibrary::GetAllCornersWithRadius( Float radius ) const
{
  vector< const CRoadPartTemplate* > ret;

  for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
  {
    if ( m_elements[i]->Type() == TT_Corner )
    {
      if ( reinterpret_cast< const CCornerTemplate* >( m_elements[i] )->GetRadius() == radius )
      {
        ret.push_back( m_elements[i] );
      }
    }
  }

  return (ret);
}

//-----------------------------------------------------------------------------

vector< CRoadPartTemplate* > CRoadPartTemplatesLibrary::GetAllCornersWithRadius( Float radius )
{
  vector< CRoadPartTemplate* > ret;

  for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
  {
    if ( m_elements[i]->Type() == TT_Corner )
    {
      if ( reinterpret_cast< const CCornerTemplate* >( m_elements[i] )->GetRadius() == radius )
      {
        ret.push_back( m_elements[i] );
      }
    }
  }

  return (ret);
}

//-----------------------------------------------------------------------------
