//-----------------------------------------------------------------------------
// File: Commons.h
//
// Desc: common data, definitions and functions
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008-2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

// -------------------------------------------------------------------------- //

#include <fstream>
#include <map>
#include <string>
#include <vector>

// -------------------------------------------------------------------------- //

#include <projects/ObjektivLib/LODObject.h>
#include <projects/ObjektivLib/ObjToolTopology.h>

// -------------------------------------------------------------------------- //

#include "CG_Circle2.h"
#include "CG_Line2.h"
#include "CG_Polygon2.h"
#include "CG_Polyline2.h"

// -------------------------------------------------------------------------- //

using std::endl;
using std::ifstream;
using std::map;
using std::pair;
using std::ofstream;
using std::string;
using std::vector;

// -------------------------------------------------------------------------- //

using namespace ObjektivLib; 

//-----------------------------------------------------------------------------

typedef RString String;
typedef double Float;

typedef CG_Circle2< Float >   CGCircle2;
typedef	CG_Consts< Float >    CGConsts;
typedef CG_Line2< Float >     CGLine2;
typedef CG_Point2< Float >    CGPoint2;
typedef CG_Polygon2< Float >  CGPolygon2;
typedef CG_Polyline2< Float > CGPolyline2;
typedef CG_Segment2< Float >  CGSegment2;
typedef CG_Vector2< Float >   CGVector2;

// -------------------------------------------------------------------------- //

#define HALF static_cast< Float >( 0.5 )
#define ZERO static_cast< Float >( 0 )
#define ONE  static_cast< Float >( 1 )
#define TWO  static_cast< Float >( 2 )

// -------------------------------------------------------------------------- //

const Float TOLERANCE      = static_cast< Float >( 0.001 );
const Float MAX_ROAD_WIDTH = static_cast< Float >( 24 );
const Float DEF_ROAD_WIDTH = static_cast< Float >( 8 );

// -------------------------------------------------------------------------- //

string FormatNumber( Float number );

// -------------------------------------------------------------------------- //
