//-----------------------------------------------------------------------------
// File: Commons.cpp
//
// Desc: common data, definitions and functions
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Commons.h"

//-----------------------------------------------------------------------------

string FormatNumber( Float number )
{
  string ret;

  char buff[20];
  sprintf( buff, "%.3f", number );
  ret = string( buff );

  while ( ret[ret.length() - 1] == '0' )
  {
    ret = ret.substr( 0, ret.length() - 1 );
  }

  if ( ret[ret.length() - 1] == '.' )
  {
    ret = ret.substr( 0, ret.length() - 1 );
  }

  for ( size_t i = 0, cntI = ret.length(); i < cntI; ++i )
  {
    if ( ret[i] == '.' ) { ret[i] = '_'; }
  }

  return (ret);
}

//-----------------------------------------------------------------------------
