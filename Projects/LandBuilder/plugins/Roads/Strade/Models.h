//-----------------------------------------------------------------------------
// File: Models.h
//
// Desc: classes and types related to road models
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

// -------------------------------------------------------------------------- //

#include "Commons.h"
#include "Templates.h"

//-----------------------------------------------------------------------------

class CRoadBaseTemplate;

//-----------------------------------------------------------------------------

class CModel
{
protected:
	// the template used by this model
	CRoadBaseTemplate* m_template;

	// the position of this model at creation time
	CGPoint2 m_creationPosition;

	// the direction of this model at creation time
	Float m_creationDirection;

	// the "real" position of this model in world space
	CGPoint2 m_position;

	// the "real" direction of this model in world space
	Float m_direction;

	// the position of this model used by Visitor to place it in world space
	CGPoint2 m_visitorPosition;

	// the direction of this model used by Visitor to place it in world space
	Float m_visitorDirection;

public:
	CModel();
	CModel( CRoadBaseTemplate* templ, const CGPoint2& creationPosition,
          Float creationDirection );
	CModel( const CModel& other );
	virtual ~CModel();

	// Assignement
	CModel& operator = ( const CModel& other );

	// Getters
	// {
	const CRoadBaseTemplate* GetTemplate() const;
	CRoadBaseTemplate* GetTemplate();
	const CGPoint2& GetCreationPosition() const;
	const CGPoint2& GetPosition() const;
	const CGPoint2& GetVisitorPosition() const;
	Float GetDirection() const;
	Float GetCreationDirection() const;
	Float GetVisitorDirection() const;
	// }

	// Model properties
	// {
	vector< CGPolygon2 > Geometry() const;
	// }

protected:
	// Initializes this model
	// This method must be called by children of this class in their constructor
	void Initialize();
	virtual void CalcPosition() = 0;
	virtual void CalcDirection() = 0;
	virtual void CalcVisitorDirection() = 0;
	void CalcVisitorPosition();
};

//-----------------------------------------------------------------------------

class CCrossroadModel : public CModel
{
public:
	CCrossroadModel();
	CCrossroadModel( CRoadBaseTemplate* templ, const CGPoint2& creationPosition,
                   Float creationDirection );
	CCrossroadModel( const CCrossroadModel& other );

	// CModel implementation
	// {
protected:
	virtual void CalcPosition();
	virtual void CalcDirection();
	virtual void CalcVisitorDirection();
	// }

  void CalcKeypartPosition();
};

//-----------------------------------------------------------------------------

class CRoadPartModel : public CModel
{
	// the creation position for the next model in the road
	CGPoint2 m_nextPosition;

	// the creation direction for the next model in the road
	Float m_nextDirection;

	// Set to true for left corners and starting terminator
	bool m_inverted;

public:
	CRoadPartModel();
	CRoadPartModel( CRoadBaseTemplate* templ, const CGPoint2& creationPosition, 
                  Float creationDirection, bool inverted = false );
	CRoadPartModel( const CRoadPartModel& other );
	virtual ~CRoadPartModel();

	// Assignement
	CRoadPartModel& operator = ( const CRoadPartModel& other );

	// Restrict comparison
	bool SameAs( const CRoadPartModel& other ) const;

	// Getters
	// {
	const CGPoint2& GetNextPosition() const;
	Float GetNextDirection() const;
	bool IsInverted() const;
	// }

	// Changes the creation attributes of this road part models and recalculates all
	// the other attributes
	void Redefine( const CGPoint2& newCreationPosition, Float newCreationDirection );

	// CRoadPartModel properties
	// {
	Float Length() const;
	// }

	// CModel implementation
	// {
protected:
	virtual void CalcPosition();
	virtual void CalcDirection();
	virtual void CalcVisitorDirection();
	// }

	void CalcNextPosition();
	void CalcNextDirection();
};

//-----------------------------------------------------------------------------
