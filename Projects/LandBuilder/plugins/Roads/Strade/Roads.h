//-----------------------------------------------------------------------------
// File: Roads.h
//
// Desc: classes and types related to roads
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "TList.h"
#include "Templates.h"
#include "Models.h"

//-----------------------------------------------------------------------------

#define DEFAULT_ROAD "DefaultRoad"

#define TYPENAME "typeName"
#define PREFIX   "prefix"
#define MAPP3D   "map"

#define MAIN_STR_TEX "mainStrTex"
#define SPEC_STR_TEX "specStrTex"
#define MAIN_COR_TEX "mainCorTex"
#define SPEC_COR_TEX "specCorTex"
#define MAIN_TER_TEX "mainTerTex"
#define SPEC_TER_TEX "specTerTex"

#define PROB_SPEC_STR_TEX "probSpecStrTex"
#define PROB_SPEC_COR_TEX "probSpecCorTex"
#define PROB_SPEC_TER_TEX "probSpecTerTex"

#define MIN_SPEC_DIST   "minSpecDist"
#define CORNER_RES      "cornerRes"
#define PROX_FACTOR     "proximityFactor"
#define MATCH_THRESHOLD "matchThreshold"

//-----------------------------------------------------------------------------

const Float DEF_MATCH_THRESHOLD = static_cast< Float >( 0.1 );
const Float MIN_CORNER_ANGLE    = static_cast< Float >( 0.0015 ); // PI / 2056
const Float MAX_CORNER_ANGLE    = static_cast< Float >( 0.7854 ); // PI / 4

//-----------------------------------------------------------------------------

class CRoadPartTemplatesLibrary;

//-----------------------------------------------------------------------------

class CRoadType
{
  // the name of this road type
  String m_name;

  // the prefix to use in the filenames of the templates of this road type
  String m_prefix;

  // the width of this road type
  Float m_width;

  // the map property to be saved in the p3d files of the templates of this road type
  String m_mapP3D;

  // the list of textures used by the templates of this road type
  // here we use std::string because RString does not work properly with std::map
  // the map.find() method does not work at all.
  map< string, string > m_textures;

  // the probabilities to use special textures (Str=straight, Cor=corner, Ter=terminator)
  Float m_probSpecStrTex;
  Float m_probSpecCorTex;
  Float m_probSpecTerTex;

  // the minimum distance between two consecutive special models (in number of models)
  size_t m_minSpecDist;

  // the max distance allowed between the vertices of the footprint in corner templates
  Float m_cornerResolution;

  // the library of the road part templates of this road type
  CRoadPartTemplatesLibrary m_templates;

  // the factor which multiplies the width of this road type when calculating
  // the distance from the road path during model calculation
  Float m_proximityFactor;

  // the threshold used during road matching in joints 
  Float m_matchThreshold;

public:
  CRoadType( const String& name, const String& prefix, const String& mapP3D, 
             const map< string, string >& textures, Float probSpecStrTex, Float probSpecCorTex, 
             Float probSpecTerTex, size_t minSpecDist, Float cornerResolution, Float proximityFactor,
             Float matchThreshold );

  // Getters
  // {
  const String& GetName() const;
  const String& GetPrefix() const;
  Float GetWidth() const;
  string GetTexture( const string& type ) const;
  const map< string, string >& GetTextures() const;
  const String& GetMapP3D() const;
  const CRoadPartTemplatesLibrary& GetTemplates() const;
  CRoadPartTemplatesLibrary& GetTemplates();
  Float GetCornerResolution() const;
  Float GetProbSpecStrTex() const;
  Float GetProbSpecCorTex() const;
  Float GetProbSpecTerTex() const;
  size_t GetMinSpecDist() const;
  Float GetMatchThreshold() const;
  Float GetProximityFactor() const;
  // }

  // Setters
  // {
  // This method forces also the creation of the default templates
  void SetWidth( Float width );
  // }

  String ExportName() const;
  String ExportPrefix() const;

  // Returns the template in the library which is closer to the given path
  // if applied at the given position and with the given direction
  // If the parameter fit is true, thresholds for distance and direction
  // will be applied
  // The parameter inverted will be true on return if the template has to be
  // applied inverted (left corners)
  // The parameter distance will be contain the distance from the path of the
  // model using the returned template
  CRoadPartTemplate* ClosestTemplateToPath( const CGPoint2& position, Float direction, 
                                            const CGPolyline2& path, bool fit, bool& inverted );

  CRoadPartTemplate* ClosestTemplateToPathAccurate( const CGPoint2& position, Float direction,
                                                    const CGPolyline2& path, bool fit, bool& inverted );

  // Tries to create a new corner template, add it to the list and return it if successfull
  CRoadPartTemplate* CreateNewCornerTemplate( const CGPoint2& position, Float direction, 
                                              const CGPolyline2& path );

private:
  // Adds to this road type the default templates
  void AddDefaultTemplates();
};

//-----------------------------------------------------------------------------

class CRoadTypesLibrary : public TList< CRoadType >
{
  String m_defaultProceduralTexture;

public:
  CRoadTypesLibrary();

  bool LoadFromCfgFile( const String& filename, ofstream& logFile );

  // searches the road type with the given name and width, and returns its index if found
  // or -1 if not found
  int FindByNameAndWidth( const String& name, Float width ) const;

private:
  void SetDefault();
};

//-----------------------------------------------------------------------------

class CJoint;
class CRoadPartModel;

//-----------------------------------------------------------------------------

class CRoad
{
  // The type of this road
  CRoadType* m_type;

  // The road path
  CGPolyline2 m_path;

  // Pointers to the crossroads or null joints in the endpoints, if any
  CJoint* m_startJoint;
  CJoint* m_endJoint;

  // The constraints to the directions (CCW angle between the world's X axis 
  // and the tangent to the road, in radians) in the endpoints of this road.
  // The builder will match exactly the start constraint and try to match
  // the end constraint.
  Float m_startDirectionConstraint;
  Float m_endDirectionConstraint;

  // Set to true if the models of this road has been already built
  bool m_built;

  // Set to true if the models of this road have been already exported
  bool m_exported;

  // the models associated to this road
  vector< CRoadPartModel > m_models;

	// whether or not to use splines in place
  // of the original polylines
	bool m_useSplines;

  // whether or not the road is completely contained into a crossroad circle
  bool m_degenerated;

public:
  CRoad( CRoadType* type, bool useSplines );
  CRoad( const CRoad& other );

  // Getters
  // {
  const CRoadType* GetType() const;
  CRoadType* GetType();
  const CGPolyline2& GetPath() const;
  CGPolyline2& GetPath();
  const CJoint* GetStartJoint() const;
  CJoint* GetStartJoint();
  const CJoint* GetEndJoint() const;	
  CJoint* GetEndJoint();	
  Float GetStartDirectionConstraint() const;
  Float GetEndDirectionConstraint() const;
  bool IsBuilt() const;
  bool IsExported() const;
  const vector< CRoadPartModel >& GetModels() const;
  vector< CRoadPartModel >& GetModels();
  bool UseSplines() const;
  bool IsDegenerated() const;
  // }

  // Setters
  // {
  void SetStartJoint( CJoint* joint );
  void SetEndJoint( CJoint* joint );
  void SetStartDirectionConstraint( Float direction );
  void SetEndDirectionConstraint( Float direction );
  void SetExported( bool exported );
  void SetDegenerated( bool degenerated = true );
  // }

  // Comparison
  bool Equals( const CRoad& other ) const;
  bool EqualsReverse( const CRoad& other ) const;

  // Path vertices manipulators
  // {
  void ReverseVerticesOrder();
  // }

  // Road properties
  // {
  // returns true if this road can be passed to the model generator algorithm
  bool IsValid( Float tolerance ) const;
  // }

  // Operations with road
  // {
  // Substitute the original polyline path of this road with its spline
  void ConvertToSpline();

  // Splits the path of this road in two part. 
  // At the end, this road will contain the first part, while the returned road
  // will contain the second part
  // if method = 0 the path will be splitted in two equal length parts
  // if method = 1 the path will be splitted in the middle of its longest segment
  CRoad Split( size_t method );
  // }

  // builds the models associated with this road
  // if the parameter allowCreation is true, the method will try to build new
  // road part templates when needed
  void BuildModels();

  // removes all the models associated with this road
  void ClearModels();

  // add models using special textures, if any
  void AddSpecialModels();

  // reduces the number of models applying model doubling
  void ReduceModels();

  // replaces the model at the given index with the given one
  void ReplaceModel( size_t index, const CRoadPartModel& model );

  // inserts the given model at the given index 
  void InsertModel( size_t index, const CRoadPartModel& model );

  // removes the model at the given index 
  void RemoveModel( size_t index );

  // returns a pointer to the last model of this road or NULL if there are no models
  const CRoadPartModel* LastModel() const;
};

//-----------------------------------------------------------------------------

std::ostream& operator << ( std::ostream& o, const CRoad& road );

//-----------------------------------------------------------------------------

class CRoadsSet
{
  class RoadDirectionLess
  {
  public:
    bool operator() ( const CRoad* r1, const CRoad* r2 )
    {			
      return (r1->GetPath().FirstSegment().Direction() < r2->GetPath().FirstSegment().Direction());
    }
  };

  class RoadWidthLess
  {
  public:
    bool operator() ( const CRoad* r1, const CRoad* r2 )
    {			
      return (r1->GetType()->GetWidth() < r2->GetType()->GetWidth());
    }
  };

  // the list of roads of this set
  vector< CRoad* > m_roads;

public:
  CRoadsSet();
  CRoadsSet( const CRoadsSet& other );

  // Comparison
  bool Equals( const CRoadsSet& other ) const;

  // Getters
  // {
  const CRoad* Get( size_t index ) const;
  CRoad* Get( size_t index );
  // }

  // Setters
  // {
  void Set( size_t index, CRoad* road );
  // }

  // Roads manipulators
  // {
  void Add( CRoad* pRoad );
  void Remove( size_t index );
  void Remove( CRoad* road );
  void Replace( CRoad* oldRoad, CRoad* newRoad );
  void SortByWidth();
  void SortByDirection();
  void Clear();
  // }

  // Set properties
  // {
  size_t Size() const;
  size_t TypesCount() const;
  Float MaxWidth() const;
  // }

private:
  CRoadsSet& operator = ( const CRoadsSet& other );
};

//-----------------------------------------------------------------------------

typedef TList< CRoad > RoadsList;

//-----------------------------------------------------------------------------
