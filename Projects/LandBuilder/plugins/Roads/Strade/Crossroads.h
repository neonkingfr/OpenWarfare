//-----------------------------------------------------------------------------
// File: Crossroads.h
//
// Desc: classes and types related to crossroads
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "Roads.h"

//-----------------------------------------------------------------------------

typedef enum 
{
	CT_RightAngles,
	CT_GenericAngles,
	CT_NoCrossroads,
	CT_COUNT
} ECrossroadType;

//-----------------------------------------------------------------------------

typedef enum 
{
	JT_NullJoint,
	JT_Crossroad
} EJointType;

//-----------------------------------------------------------------------------

class CJoint
{
protected:
	// the position of this joint
	CGPoint2 m_position;

	// the roads arriving in this joint
	CRoadsSet m_roads;

public:
	CJoint( const CGPoint2& position, CRoad* firstRoad );
	CJoint( const CJoint& other );
	virtual ~CJoint();

	// Getters
	// {
	const CGPoint2& GetPosition() const;
	const CRoadsSet& GetRoads() const;
	CRoadsSet& GetRoads();
	// }

	// builds all the roads starting from this joint
	virtual void BuildRoads() = 0;

	// Joint properties
	// {
	virtual EJointType Type() const = 0;
	// }
};

//-----------------------------------------------------------------------------

class CNullJoint : public CJoint
{
	// true if the roads match in this joint
	bool m_matched;

public:
	CNullJoint( const CGPoint2& position, CRoad* firstRoad );
	CNullJoint( const CNullJoint& other );
	virtual ~CNullJoint();

	// Getters
	// {
	bool IsMatched() const;
	// }

	// CJoint implementation
	// {
	virtual void BuildRoads();
	virtual EJointType Type() const;
	// }

private:
	// tries to match the roads in this joint
	// returns true if successfull
	bool MatchRoads();

	// tries to match the loop road starting and ending in this joint
	// returns true if successfull
	bool MatchLoopRoad();

	// tries to fill eventual gaps between roads
	void FillGaps();
};

//-----------------------------------------------------------------------------

std::ostream& operator << ( std::ostream& o, const CNullJoint& joint );

//-----------------------------------------------------------------------------

class CCrossroad : public CJoint
{
	// the type of this crossroad
	ECrossroadType m_crossType;

	// the model of this crossroad
	CCrossroadModel m_model;

public:
	CCrossroad( const CGPoint2& position, ECrossroadType crossType, CRoad* firstRoad );
	CCrossroad( const CCrossroad& other );
	virtual ~CCrossroad();

	// Getters
	// {
	const CCrossroadModel& GetModel() const;
	// }

	// CJoint implementation
	// {
	// builds all the roads starting from this crossroad and free in the other endpoint
	virtual void BuildRoads();
	virtual EJointType Type() const;
	// }

	// Crossroad properties
	// {
	Float Radius() const;
	// }

	// builds the model associated with this crossroad
	void BuildModel( CCrossroadTemplatesLibrary& crossroadsLibrary );

private:
	void PreBuildModel();
};

//-----------------------------------------------------------------------------

typedef TList< CNullJoint > JointsList;
typedef TList< CCrossroad > CrossroadsList;

//-----------------------------------------------------------------------------
