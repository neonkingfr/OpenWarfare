//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

class RoadBasicPlacer: public LandBuilder2::IBuildModule
{
  string m_version;

public:
  RoadBasicPlacer();
  virtual ~RoadBasicPlacer();

  virtual void Run( IMainCommands* cmdIfc );
  void PlaceRoad( IMainCommands* cmdIfc );
  void PlaceCrossRoad( IMainCommands* cmdIfc );
};

//-----------------------------------------------------------------------------
