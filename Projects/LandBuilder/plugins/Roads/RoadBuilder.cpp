//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include <es/algorithms/qsort.hpp>
#include <IProjectTask.h>
#include "RoadBuilder.h"
#include "ShapeCross.h"
#include <IShapeDbAccess.h>
#include "BSpline.h"
#include "ShapePolyLineWithStartEndDirection.h"

//-----------------------------------------------------------------------------

using namespace Shapes;

//-----------------------------------------------------------------------------

static LandBuilder2::RegisterModule< RoadBuilder > roadBuilder( "RoadBuilder" );

//-----------------------------------------------------------------------------

double RoadBuilder::Vector2Azimut( double x, double y )
{
  double res = atan2( x, y ) * 180.0 / 3.1415956535;
  if ( res < 0.0 ) { res = res + 360.0; }
  return (res);
}

//-----------------------------------------------------------------------------

double RoadBuilder::Vector2Azimut( const Shapes::DVector& vx )
{
  return (Vector2Azimut( vx.x, vx.y ));
}

//-----------------------------------------------------------------------------

double RoadBuilder::Vector2Azimut( const Vector3& vx )
{
  return (Vector2Azimut( vx.X(), vx.Z() ));
}

//-----------------------------------------------------------------------------

Vector3D RoadBuilder::Azimut2VectorVec( double azimut )
{
  std::pair< double, double > xy = Azimut2VectorXY( azimut );
  return (Vector3D( xy.first, 0, xy.second ));
}

//-----------------------------------------------------------------------------

Shapes::DVector RoadBuilder::Azimut2VectorDVec( double azimut )
{
  std::pair< double, double > xy = Azimut2VectorXY( azimut );
  return (Shapes::DVector( xy.first, xy.second ));
}

//-----------------------------------------------------------------------------

std::pair< double, double > RoadBuilder::Azimut2VectorXY( double azimut )
{
  azimut = azimut * 3.1415926535 / 180.0;
  return (std::pair< double, double >( sin( azimut ), cos( azimut ) ));
}

//-----------------------------------------------------------------------------

RoadBuilder::RoadBuilder()
: m_version( "1.1.0" )
{
  m_endPhaseProcessed = false;
}

//-----------------------------------------------------------------------------

RoadBuilder::~RoadBuilder()
{
}

//-----------------------------------------------------------------------------

void RoadBuilder::ProcessShape( IMainCommands* cmdIfc )
{
  if ( m_endPhaseProcessed ) { return; }
  const IShape* curShape = cmdIfc->GetActiveShape();
  const IShapeExtra* extra = curShape->GetInterface< IShapeExtra >();
  if ( !extra )
  {
    LOGF( Error ) << "Found unknown shape in shapefile. Cannot get type of that shape";
    return;
  }
  RString shapeType = extra->GetShapeTypeName();
  if ( shapeType == SHAPE_NAME_POINT )
  {
    AddCrossPoint( cmdIfc );
  }
  else if ( shapeType == SHAPE_NAME_POLYLINE )
  {
    AddRoadLine( cmdIfc );
  }
  else
  {
    LOGF( Error ) << "Can't operate on shape: " << shapeType.Data();
    return;
  }
}

//-----------------------------------------------------------------------------

void RoadBuilder::AddCrossPoint( IMainCommands* cmdIfc )
{
  const ShapePoint* curshape = static_cast< const ShapePoint* >( cmdIfc->GetActiveShape() );
  const Shapes::DVertex& vx = curshape->GetVertex( 0 );
  LandBuilder2::ParamParser params( cmdIfc );

  try
  {
    const LandBuilder2::IProjectTask* curTask = cmdIfc->GetCurrentTask();

// this line is commented out in mark's code
    RStringB xpalette = RString( curTask->GetWorkFolder(), params.GetTextValue( "XPaletteFile" ) );
/*
// Mark's code
// with this line LB2 is unable to find the palette file
        RStringB xpalette = RString(params.GetTextValue("XPaletteFile"));
*/
    Crossroad crossroad( vx,
                         params.GetFloatValue( "XSize" ),
                         params.GetFloatValue( "XPadding" ),
                         xpalette,
                         params.GetTextValue( "OutShapefile" ) ); 

// this code was deleted in mark's solution
    try
    {
      crossroad.m_forcedTexture = params.GetTextValue( "ForcedTexture" );
    } 
    catch ( LandBuilder2::ParamParserException& ) 
    {
    }

    m_crossroadList.Add( crossroad );
  }
  catch ( LandBuilder2::ParamParserException& e )
  {
    LOGF( Error ) << e.Message() << ": " << e.GetName().Data() << "=\"" << e.GetValue() << "\"";
  }
}

//-----------------------------------------------------------------------------

void RoadBuilder::AddRoadLine( IMainCommands* cmdIfc )
{
  const ShapePolyLine* curshape = static_cast< const ShapePolyLine* >( cmdIfc->GetActiveShape() );

  for ( int i = 0, cnt = curshape->GetPartCount(); i < cnt; ++i )
  {
    ShapePolyLine* onepoly = static_cast< ShapePolyLine* >( curshape->ExtractPart( i ) );
    AddRoadLine( cmdIfc, onepoly );
  }
}

//-----------------------------------------------------------------------------

int RoadBuilder::FindNearestXRoad( const DVector& point, int from )
{
  if ( from == m_crossroadList.Size() ) { return (-1); }
  double mdist = DBL_MAX;
  int choosen = -1;
  for ( int i = from, cnt = m_crossroadList.Size(); i < cnt; ++i )
  {
    double d = m_crossroadList[i].m_location.DistanceXY2( point );
    if ( d < mdist )
    {
      choosen = i;
      mdist = d;
    }
  }

  double limit = m_crossroadList[choosen].m_padding;
  if ( mdist <=  limit * limit )
  {
    return (choosen);
  }
  else 
  {
    return (-1);
  }
}

//-----------------------------------------------------------------------------

void RoadBuilder::AddRoadLine( IMainCommands* cmdIfc, ShapePolyLine* polyline )
{
  int begX = FindNearestXRoad( polyline->GetVertex( 0 ) );
  int endX = FindNearestXRoad( polyline->GetVertex( polyline->GetVertexCount() - 1 ) );
  LandBuilder2::ParamParser params( cmdIfc );

  try
  {
    RoadProp prop;

    prop.m_partCount       = params.GetIntValue( "partCount" );
    prop.m_step            = params.GetFloatValue( "step" );
    prop.m_width           = params.GetFloatValue( "width" );
    prop.m_roadType        = params.GetTextValue( "roadType" );
    prop.m_resultShapefile = params.GetTextValue( "OutShapefile" );

/*
// mark's code
    try 
    {
      prop.m_forcedTexture = params.GetTextValue( "XForcedTexture" );
      LOGF( Debug ) << "texture: " << prop.m_forcedTexture;
    } 
    catch ( LandBuilder2::ParamParserException& ) 
    {
      throw LandBuilder2::ParamParserInvalidValue( "XForcedTexture", ">0" );
    }
*/

    if ( prop.m_partCount & 0x1 ) { throw LandBuilder2::ParamParserInvalidValue( "partCount", "divided by 2" ); }
    if ( prop.m_width <= 0 )      { throw LandBuilder2::ParamParserInvalidValue( "width", ">0" ); }
    if ( prop.m_step <= 0 )       { throw LandBuilder2::ParamParserInvalidValue( "step", ">0" ); }

/*        for ( int i = 0, cnt = prop.m_straightParts.Size(); i < cnt; ++i )
          {
            if ( prop.m_straightParts[i] <= 0 )
            {
              throw LandBuilder2::ParamParserInvalidValue( "straightParts", ">0" );
            }
            if ( prop.m_straightTolerance < 0 )
            {
              throw LandBuilder2::ParamParserInvalidValue( "straightTolerance", ">=0" );
            }
          }
*/

    IGlobVertexArray* vertexArray = cmdIfc->GetInterface< IGlobVertexArray >();

    for ( int i = 0, cnt = polyline->GetVertexCount(); i < cnt; ++i )
    {
      DVertex vx = polyline->GetVertex( i );
      int idx = polyline->GetIndex( i );
      vx.m = prop.m_width;
      vertexArray->SetVertex( idx, vx );
    }

    m_roadList.Add( Road( polyline, begX, endX, prop ) );
  }
  catch ( LandBuilder2::ParamParserException& e )
  {
    LOGF( Error ) << e.Message() << ": " << e.GetName().Data() << "=\"" << e.GetValue() << "\"";
  }
}

//-----------------------------------------------------------------------------

void RoadBuilder::OnEndPass( IMainCommands* cmdIfc ) 
{
  if ( m_endPhaseProcessed ) { return; }

  m_endPhaseProcessed = true;
  LOGF( Progress ) << "RoadBuilder is processing collected data";

  LOGF( Progress ) << "(RoadBuilder) optimizing crossroads";
  OptimizeXRoads();

  LOGF( Progress ) << "(RoadBuilder) optimizing road shapes";
  OptimizeRoadShapes( cmdIfc );

  LOGF( Progress ) << "(RoadBuilder) generating crossroad shapes";
  SelectBestCrossroadModels( cmdIfc );

  LOGF( Progress ) << "(RoadBuilder) generating road shapes";
  GenerateRoadShapes( cmdIfc );

  LOGF( Progress ) << "(RoadBuilder) Done";
}

//-----------------------------------------------------------------------------

void RoadBuilder::OptimizeXRoads() 
{
  bool opakuj = true;
  while ( opakuj )
  {
    opakuj = false;
    int cnt = m_crossroadList.Size();
    ProgressBar< int > pb( cnt );
    for ( int i = 0; i < cnt; ++i )
    {
      pb.AdvanceNext( 1 );
      int nearest = FindNearestXRoad( m_crossroadList[i].m_location, i + 1 );
      if ( nearest != -1 )
      {
        JoinTwoXRoads( i, nearest );
        cnt--;
        i--;
        opakuj = true;
      }
    }
  }
}

//-----------------------------------------------------------------------------

void RoadBuilder::JoinTwoXRoads( int leave, int remove )
{
  for ( int i = 0, cnt = m_roadList.Size(); i < cnt; ++i )
  {
    if ( m_roadList[i].m_startCross == remove) { m_roadList[i].m_startCross = leave; }
    if ( m_roadList[i].m_endCross   == remove) { m_roadList[i].m_endCross = leave; }
    if ( m_roadList[i].m_startCross >= remove) { m_roadList[i].m_startCross--; }
    if ( m_roadList[i].m_endCross   >= remove) { m_roadList[i].m_endCross--; }
  }

  m_crossroadList[leave].m_location = DVertex( (m_crossroadList[leave].m_location + m_crossroadList[remove].m_location) * 0.5, 
                                                m_crossroadList[leave].m_location.m );

  m_crossroadList.Delete( remove );
}

//-----------------------------------------------------------------------------

void RoadBuilder::OptimizeRoadShapes( IMainCommands* cmdIfc )
{
  LandBuildInt::IGlobVertexArray* systVxArr = cmdIfc->GetInterface< LandBuildInt::IGlobVertexArray >();


  //create centers of crossroads
  for ( int i = 0, cnt = m_crossroadList.Size(); i < cnt; ++i )
  {
    m_crossroadList[i].m_xpoint = systVxArr->AllocVertex( m_crossroadList[i].m_location );
  }

  for ( int i = 0, cnt = m_roadList.Size(); i < cnt; ++i )
  {
    OptimizeRoadShape( m_roadList[i] );
  }
}

//-----------------------------------------------------------------------------

void RoadBuilder::OptimizeRoadShape( Road& road )
{
  ShapePolyLine* shape = static_cast< ShapePolyLine* >( road.m_shape.GetRef() );

  AutoArray< size_t > newVertices;

  int nx = 0;
  int sz = shape->GetVertexCount();

  if ( road.m_startCross != -1 )
  {
    const Crossroad& xrs = m_crossroadList[road.m_startCross];
    newVertices.Add( xrs.m_xpoint );
    while ( nx < sz &&  shape->GetVertex( nx ).DistanceXY2( xrs.m_location ) < xrs.m_padding * xrs.m_padding )
    {
      ++nx;
    }
  }
  
  int ls = sz - 1;
  if ( road.m_endCross != -1 )
  {
    const Crossroad& xrs = m_crossroadList[road.m_endCross];
    while ( ls>=0 && shape->GetVertex( ls ).DistanceXY2( xrs.m_location ) < xrs.m_padding * xrs.m_padding )
    {
      --ls;
    }
  }

  while ( nx <= ls ) 
  {
    newVertices.Add( shape->GetIndex( nx++ ) );
  }

  if ( road.m_endCross != -1 )
  {
    const Crossroad& xrs = m_crossroadList[road.m_endCross];
    newVertices.Add( xrs.m_xpoint );
  }

  size_t parts = 0;
  road = Road( shape->NewInstance( ShapePolyLine( newVertices, Array< size_t >( &parts, 1 ), shape->GetVertexArray() ) ),
               road.m_startCross, road.m_endCross, road.m_properties);

  IShape* shp = road.m_shape;
  VertexArray* vxarr = shp->GetVertexArray();
  for ( int i = 0, cnt = road.m_shape->GetVertexCount(); i < cnt; ++i )
  {
    int idx = shp->GetIndex( i );

// this is buggy
//        DVertex vx = vxarr->GetVertex( i );
//        vx.m = -1.0f;
//        vxarr->SetVertex( i, vx );

    DVertex vx = vxarr->GetVertex( idx );
    vx.m = road.m_properties.m_width;
    vxarr->SetVertex( idx, vx );
  }
}

//-----------------------------------------------------------------------------

void RoadBuilder::BuildXPaletteItems( Array< XPaletteItemEx >& xitems )
{
  for ( int i = 0, cnt = m_roadList.Size(); i < cnt; ++i )
  {
    const Road& rd = m_roadList[i];

    if ( rd.m_startCross != -1 )
    {
      xitems[rd.m_startCross].m_exits.Add( XPaletteItemExit( rd.m_properties.m_roadType,
                                                             rd.m_properties.m_width, 
                                                             rd.m_startAzimut ) );
      xitems[rd.m_startCross].m_exitIds.Add( RoadLoc( i, 0 ) );

/*
// mark's code

            //log the number of used roads per type
            bool found = false;
            for ( int j = 0; j < xitems[rd.m_startCross].m_typeCount.Size(); ++j )
            {
              if ( xitems[rd.m_startCross].m_typeCount[j].first == i )
              {
                xitems[rd.m_startCross].m_typeCount[j].second++;
                found = true;
              }
            }
            if ( !found ) { xitems[rd.m_startCross].m_typeCount.Add( RoadLoc( i, 0 ) ); }
*/
    }

    if ( rd.m_endCross != -1 ) 
    {
      xitems[rd.m_endCross].m_exits.Add( XPaletteItemExit( rd.m_properties.m_roadType, 
                                                           rd.m_properties.m_width, 
                                                           rd.m_endAzimut ) );
      xitems[rd.m_endCross].m_exitIds.Add( RoadLoc( i, 1 ) );

/*
// mark's code

            //log the number of used roads per type
            bool found = false;
            for ( int j = 0; j < xitems[rd.m_endCross].m_typeCount.Size(); ++j )
            {
              if ( xitems[rd.m_endCross].m_typeCount[j].first == i )
              {
                xitems[rd.m_endCross].m_typeCount[j].second++;
                found = true;
              }
            }
            if ( !found ) { xitems[rd.m_endCross].m_typeCount.Add( RoadLoc( i, 0 ) ); }
*/
    }
  }

  for ( int i = 0, cnt = m_crossroadList.Size(); i < cnt; ++i )
  {
    xitems[i].m_radius = m_crossroadList[i].m_size;

// this line is deleted in mark's code
    xitems[i].m_forcedTexture = m_crossroadList[i].m_forcedTexture;

/*
// mark's code

        //use the texture of the highest type count, if equal use the wider road
        RoadLoc* highestCount = NULL;
        for ( int j = 0; j < xitems[i].m_typeCount.Size(); ++j )
        {
          if ( !highestCount || xitems[i].m_typeCount[j].second > highestCount->second || 
               (xitems[i].m_typeCount[j].second == highestCount->second && 
                m_roadList[highestCount->first].m_properties.m_width <
                m_roadList[xitems[i].m_typeCount[j].first].m_properties.m_width) )
          {
            highestCount = & xitems[i].m_typeCount[j];
          }
        }
        if ( highestCount )
        {
          xitems[i].m_forcedTexture = m_roadList[highestCount->first].m_properties.m_forcedTexture;
        }
*/
  }
}

//-----------------------------------------------------------------------------

static void SortExitsInXRoad( XPaletteItemEx& item )
{
  bool opak;
  do
  {
    opak = false;
    for ( int i = 1; i < item.m_exits.Size(); ++i )
    {
      if ( item.m_exits[i - 1].m_azimut > item.m_exits[i].m_azimut )
      {
        std::swap( item.m_exits[i - 1], item.m_exits[i] );
        std::swap( item.m_exitIds[i - 1], item.m_exitIds[i] );
        opak = true;
      }
    }
  }
  while ( opak );        
}

//-----------------------------------------------------------------------------

typedef std::pair< int, int > RoadEnds;

//-----------------------------------------------------------------------------

static void SortXPalItems( Array< XPaletteItemEx >& xitems )
{
  for ( int i = 0, cnt = xitems.Size(); i < cnt; ++i )
  {
    SortExitsInXRoad( xitems[i] );
  }    
}

//-----------------------------------------------------------------------------

void RoadBuilder::SelectBestCrossroadModels( IMainCommands* cmdIfc )
{
  AutoArray< XPaletteItemEx > xitems;
  xitems.Resize( m_crossroadList.Size() );
  BuildXPaletteItems( xitems );
  SortXPalItems( xitems );
  XPalette xpal;
  RString prevPal;
  bool saveIt = false;
  
  for ( int i = 0, cnt = xitems.Size(); i < cnt; ++i )
  {
    try
    {
      RString newfl = m_crossroadList[i].m_xpaletteFile;
      if ( _stricmp( prevPal, newfl ) )
      {
        if ( saveIt ) { xpal.SavePalette( prevPal ); }
        saveIt = false;
        xpal.LoadPalette( newfl );
      }
      prevPal = newfl;
    }
    catch ( std::exception& e )
    {
      LOGF( Error ) << "Cannot load xpalette file: " << m_crossroadList[i].m_xpaletteFile << " - " << e.what();
      continue;
    }
     
    //ignore xings with no exits
    if ( xitems[i].m_exits.Size() == 0 ) { continue; }

    XPalBestResult best = xpal.FindBest( xitems[i], m_crossroadList[i].m_padding, true );

    if ( best.m_created ) { saveIt = true; }

    XPaletteItemEx& curxitem = xitems[i];
    const XPaletteItem& bestxitem = *best.m_xroad;

    AutoArray< size_t > points;
    AutoArray< size_t > roadPoints;
    IGlobVertexArray* vertexArrIfc = cmdIfc->GetInterface< IGlobVertexArray >();

    for ( int j = 0, cnt = bestxitem.m_exits.Size(); j < cnt; j++ )
    {
      double azimut = bestxitem.m_exits[j].m_azimut + best.m_azimutRot;
      DVertex dvx = m_crossroadList[i].m_location + (Azimut2VectorDVec( azimut ) * bestxitem.m_radius );
      dvx.m = bestxitem.m_exits[j].m_width;

      //index vertexu noveho zacatku cest
      //(nelze hybat tim puvodnim, co kdyz je k necemu jeste pouzit??)
      int vx = vertexArrIfc->AllocVertex( dvx );

      //vloz vertex do seznamu vertexu pro tuto krizovatku
      points.Add( vx );

      //spocitej normalu ve smeru vyjezdu
      DVertex dvx1 = dvx + Azimut2VectorDVec( azimut ) * 3 * dvx.m;
      
      //vyzvedni ktera cesta bude na tomto vyjezdu
      RoadLoc& rloc = curxitem.m_exitIds[(cnt + j - best.m_exitRot) % cnt];
      Road& road = m_roadList[rloc.first];
      dvx1.m = road.m_properties.m_width; //nastav sirku;

      roadPoints.Clear();
      for ( int k = 0, cnt = road.m_shape->GetVertexCount(); k < cnt; ++k )
      {
        int idx = road.m_shape->GetIndex( k );
        roadPoints.Add( idx );
      }
      
      double sidelen = rloc.second == 0 ? road.m_shape->GetVertex( 0 ).DistanceXY( road.m_shape->GetVertex( 1 ) ) 
                                        : road.m_shape->GetVertex( road.m_shape->GetVertexCount() - 1 ).DistanceXY( road.m_shape->GetVertex( road.m_shape->GetVertexCount() - 2 ) );

      if ( sidelen / 3.0 < 3.0 * dvx.m )
      {
        dvx1 = dvx + Azimut2VectorDVec( azimut ) * sidelen / 3.0;
        dvx1.m = road.m_properties.m_width; //nastav sirku;
      }

      int vx2 = vertexArrIfc->AllocVertex( dvx1 );            

      if ( rloc.second == 0 )
      {
        roadPoints[0] = vx;
        roadPoints.Insert( 1, vx2 );
      }
      else
      {
        roadPoints[roadPoints.Size() - 1] = vx;
        roadPoints.Insert( roadPoints.Size() - 1, vx2 );
      }
      
      size_t parts = 0;
      road.m_shape = static_cast< ShapePolyLine* >( road.m_shape.GetRef() )->NewInstance( ShapePolyLine( roadPoints, Array< size_t >( &parts, 1 ), road.m_shape->GetVertexArray() ) );
    }
    
    points.Add( m_crossroadList[i].m_xpoint );

    ShapeCross* xshape = new ShapeCross( best.m_xroad->m_model,
                                         best.m_scale,
                                         best.m_azimutRot,
                                         points,
                                         const_cast< Shapes::VertexArray* >( cmdIfc->GetInterface< IGlobVertexArray >()->GetVertexArray() ) );

    m_crossroadList[i].m_xshape = xshape;
    cmdIfc->GetInterface< LandBuilder2::IShapeDbAccess>()->AddToShapeGroup( m_crossroadList[i].m_resultShapefile, xshape );

    xpal.GenerateP3DWithLog( bestxitem );
  }
  
  if ( saveIt ) { xpal.SavePalette( prevPal ); }
}

//-----------------------------------------------------------------------------

class DVertexEx : public DVertex
{
public:
  DVertexEx( double x = 0.0, double y = 0.0, double z = DBL_MIN, double m = DBL_MIN ) 
  : DVertex( x, y, z, m )
  {
  }

  DVertexEx( const DVector& vx, double m = DBL_MIN ) 
  : DVertex( vx, m ) 
  {
  }

  DVertexEx( const DVertex& vx )
  : DVertex( vx ) 
  {
  }

  double& operator [] ( int i )
  {
    switch ( i )
    {
    case 0: return (x);
    case 1: return (y);
    case 2: return (z);
    case 3: return (m);
    }
    throw std::runtime_error( "Array is out of bounds" );
  }

  ClassIsSimpleZeroed( DVertexEx );
};

//-----------------------------------------------------------------------------

template < class A, class D >
static bool CreateWorkPoints( A& spline, int partCount, double step, D& workpoints )
{
  LOGF( Debug ) << "Spline start:----------------- (step: " << step << ")";
  do 
  {
    workpoints.Clear();

    double curT = 0.0;
    double maxT = spline.MaxT();
    double lastStep = 0.0;
    while ( fabs(curT - maxT) > 0.0000001 ) 
    {            
      double pos[4];
      curT = spline.GetNextPoint( curT, step, &lastStep, pos );
      workpoints.Add( DVertex( pos[0], pos[1], pos[2], pos[3] ) );
    }

    int cnt = workpoints.Size();
    int cluster = cnt / partCount;
    if ( cluster < 1 )
    {
      double dist = workpoints[workpoints.Size() - 1].DistanceXY( workpoints[0] );
      dist /= partCount;
      LOGF( Debug ) << "Spline res: cluster: " << cluster << " next step " << step << " dist " << dist;

      if ( dist < 0.001 ) { return (false); }

      step = dist * 0.9;
    }
    else
    {
      int fullparts = cluster * partCount;
      int extraparts = workpoints.Size() - fullparts;
      double errorf = 0.0;
      double lastDist = workpoints[workpoints.Size() - 1].DistanceXY( workpoints[workpoints.Size() - 2] );

      if ( extraparts == 1 )
      {
        workpoints.Delete( workpoints.Size() - 2 );
        --extraparts;
      }

      if ( extraparts == 0 )
      {
        errorf = lastDist - step;
      }
      else if ( extraparts * 2 <= partCount )
      {
        errorf = (step - lastDist) + (extraparts - 1) * step;
      }
      else
      {
        errorf = (extraparts - partCount) * step + (lastDist - step);
      }

      if ( fabs( errorf ) < step * 0.25 )
      {
        LOGF( Debug ) << "Spline finished:  cluster: " << cluster << " next step " << step << " errorf " << errorf;
        break;
      }

      double stepmod = errorf / (double)fullparts * 0.25;
      step += stepmod;
      LOGF( Debug ) << "Spline res: cluster: " << cluster << " next step " << step << " errorf " << errorf;
    }
  } while ( true );
  
  return (true);
}

//-----------------------------------------------------------------------------

void RoadBuilder::GenerateRoadShape( IMainCommands* cmdIfc, Road& road )
{
  IGlobVertexArray* vxarray = cmdIfc->GetInterface< IGlobVertexArray >();

  const IShape* shape = road.m_shape;

  int cnt = shape->GetVertexCount();

  AutoArray< DVertexEx > points;
  points.Reserve( shape->GetVertexCount() + 4 );
  points.Add( shape->GetVertex( 0 ) );
  points.Add( shape->GetVertex( 0 ) );

  for ( int i = 0 ; i < cnt; ++i )
  {
    points.Add( shape->GetVertex( i ) );
  }

  points.Add( shape->GetVertex( cnt - 1) );
  points.Add( shape->GetVertex( cnt - 1) );
    
  BSpline< 4, double, Array< DVertexEx > > spline( points, points.Size() );

  AutoArray< DVertex > workpoints;

  if ( CreateWorkPoints( spline, road.m_properties.m_partCount, road.m_properties.m_step, workpoints ) == false )
  {
    LOGF( Error ) << "Road cannot be created - spline generator failed. Probably too short line";
    return;
  }

  AutoArray< size_t > outpoints;
  outpoints.Reserve( workpoints.Size() + 1 );

  outpoints.Add( shape->GetIndex( 0 ) );

  for ( int i = 0, cnt = workpoints.Size(); i < cnt; ++i )
  {
    outpoints.Add( vxarray->AllocVertex( workpoints[i] ) );
  }

  Shapes::ShapePolyLineWithStartEndDirection* newshape = new ShapePolyLineWithStartEndDirection( outpoints, const_cast< VertexArray* >( vxarray->GetVertexArray() ),
                                                                                                 shape->GetIndex( 1 ), shape->GetIndex( cnt - 2 ) );
  cmdIfc->GetInterface< LandBuilder2::IShapeDbAccess >()->AddToShapeGroup( road.m_properties.m_resultShapefile, newshape );    
}

//-----------------------------------------------------------------------------

void RoadBuilder::GenerateRoadShapes( IMainCommands* cmdIfc )
{
  ProgressBar< int > pb( m_roadList.Size() );
  for ( int i = 0; i < m_roadList.Size(); ++i )  
  {
    pb.AdvanceNext( 1 );
    GenerateRoadShape( cmdIfc, m_roadList[i] );
  }
}

//-----------------------------------------------------------------------------
