//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "XPalette.h"

//-----------------------------------------------------------------------------

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
  if ( fdwReason == DLL_PROCESS_ATTACH )
  {
    Pathname curPath = Pathname::GetExePath( &hinstDLL );
    curPath.SetFilename( GENERATORAPP );
    XPalette::s_generatorExe = curPath.GetFullPath();
  }
  
  return (TRUE);
}

//-----------------------------------------------------------------------------

