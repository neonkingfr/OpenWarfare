//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

///Simple BSpline interpolator
/**
 * @param dimensions count of dimensions for the interpolator
 * @param Type type of results, can be float, double or int
 * @param PointContrainer type for point container. Default value enables to use
 *  single array as container
 */
template< int dimensions = 3, class Type = double, class PointContainer = double*[dimensions] >
class BSpline
{
	PointContainer m_points;
	int            m_count;

public:
	///constructs BSpline interpolator
	/**
	* @param points container with points
	* @param count of points from the beginning of the array should be used. This
	*  number have to be abode 3  
	*/
	BSpline( const PointContainer& points, int count )
	: m_points( points )
	, m_count( count ) 
	{
	}

private:
	///Gets result for given dimension
	template < int dimIndex >
	Type GetValueAt( double t )
	{    
    if ( t < 0.0 ) { t = 0.0; }
		double tf = floor( t );
		int idx = toInt( tf ); 
		if ( idx > m_count - 4 ) 
		{
			t = 1;
			idx = m_count - 4;
		}
		else
		{
			t -= tf;
		}
		return ((Type)((m_points[idx + 0][dimIndex] * (((-t + 3) * t - 3) * t + 1) / 6) +
					         (m_points[idx + 1][dimIndex] * (((3 * t - 6) * t) * t + 4) / 6) +
					         (m_points[idx + 2][dimIndex] * (((-3 * t + 3) * t + 3) * t + 1) / 6) +
					         (m_points[idx + 3][dimIndex] * (t * t * t) / 6)));
	}

	///Helper function for GetPoint
	template < int dimIndex >
	void GetPointDim( double t, Type* result )
	{
		*result = GetValueAt< dimIndex >( t );
		return (GetPointDim< dimIndex - 1 >( t, result - 1 ));
	}

	///Helper function for GetPoint
	template<>
  void GetPointDim< 0 >( double t, Type* result )
	{
		*result = GetValueAt< 0 >( t );
	}

public:
	///Retrieves a at spline. Location is defined
	/**
	* by t parameter.
	*
	* @param t position on spline. It is divided into two parts
	* Integer part defines segment that is defined by four points. Segment
	* 0 is defined by first four points, segment 1 is defined by second,third,fourth,fifth point,
	* segment 2 is defined by third fourth, fifth, sixth point, etc. Seconds part is fraction defines
	* position in segment.
	* @param result pointer to vector that will receive a result;
	*/
	void GetPoint( double t, Type* result )
	{
		GetPointDim< dimensions - 1 >( t, result + dimensions - 1 );
	}

	Type MinT() const 
	{
		return ((Type)0);
	}

	Type MaxT() const 
	{
		return ((Type)(m_count - 3));
	}

	///Calculates point that lies on curve but has defined distance from another point on curve
	/**
	* @param curpoint position of reference point
	* @param distance distance of the new point
	* @param lastStep optional, points to variable that receives step between reference and
	*  the result point. Function will use this value as hint to faster search next point.
	*  Before first usage, set this variable to 0.
	* @param result pointer to vector that will receive coordinates of result. If NULL passed
	*  no result will be stored. 
	* @return position of next step.
	*/
	double GetNextPoint( double curPoint, double distance, double* lastStep = NULL, Type* result = NULL )
	{
		Type tmp[dimensions];
    if ( !result ) { result = tmp; }
		Type cur[dimensions];
		GetPoint( curPoint, cur );
		double curStep = lastStep ? *lastStep : 0.1;    
    if ( curStep == 0.0 ) { curStep = 1.0; }
		double curHalf = curStep;
		bool expand = true;
		double t;
		do 
		{
			t = curPoint + curStep;   

			GetPoint( t, result );
			double dist = 0.0;
			for ( int i = 0; i < dimensions; ++i ) 
			{
				dist += (result[i] - cur[i]) * (result[i] - cur[i]);
			}
			dist = sqrt( dist );
			if ( t >= MaxT() )
			{      
				curStep = MaxT() - curPoint;        
				t = MaxT();
        if ( dist <= distance ) { break; }
			}
			{
        if ( fabs( dist - distance ) < 0.0001 ) { break; }
				if ( dist > distance )
				{
					curHalf *= 0.5;
					curStep -= curHalf;
          if ( curStep < 0.0 ) { curStep = 0.0; }
					expand = false;
				}
				else
				{
          if ( !expand ) { curHalf *= 0.5; }
					curStep += curHalf;
          if ( expand ) { curHalf *= 1.5; }
				}
/*
        double diff = distance / dist;
        if ( curStep * diff > 10.0 + curStep ) { diff = 10.0 + curStep / curStep; }
        if ( diff < 1.00001 && diff > 0.99999 ) { break; }
        double estStep = diff * curStep;
        curStep = (estStep + curStep) / 2.0;
*/
			}
		} 
		while ( true );
    if ( lastStep ) { *lastStep = curStep; }
		return (t);
	}
};

//-----------------------------------------------------------------------------
