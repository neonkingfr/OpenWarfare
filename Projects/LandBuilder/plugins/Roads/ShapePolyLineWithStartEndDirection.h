//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

namespace Shapes 
{
	class ShapePolyLineWithStartEndDirection : public Shapes::ShapePolyLine, public IShapeExtra
	{
		int m_startDir;
		int m_endDir;

	public:
		ShapePolyLineWithStartEndDirection( const Array< size_t >& points, VertexArray* vx,
                                        int startDir, int endDir )
		: ShapePolyLine( points, Array< size_t >( 0 ), vx )
		, m_startDir( startDir )
		, m_endDir( endDir )
		{
		}

		int GetStartDirIndex() const 
		{
			return (m_startDir);
		}

		int GetEndDirIndex() const 
		{
			return (m_endDir);
		}

		DVertex GetStartDir() const 
		{
			return (GetVertexArray()->GetVertex( m_startDir ) - GetVertex( 0 ));
		}
	    
		DVertex GetEndDir() const
		{
			return (GetVertexArray()->GetVertex( m_endDir ) - GetVertex( GetVertexCount() - 1 ));
		}

		DVertex GetStartDirPt() const 
		{
			return (GetVertexArray()->GetVertex( m_startDir ));
		}

		DVertex GetEndDirPt() const
		{
			return (GetVertexArray()->GetVertex( m_endDir ));
		}

		virtual void* GetInterfacePtr( size_t ifcuid );
	    
		virtual ShapePolyLineWithStartEndDirection* NewInstance( const ShapePolyLineWithStartEndDirection& other ) const
		{
			return (new ShapePolyLineWithStartEndDirection( other ));
		}

		virtual Shapes::DVector NearestEdge( const Shapes::DVector& vx ) const;
		virtual const char* GetShapeTypeName() const;

		static const char* s_name;
	};

//-----------------------------------------------------------------------------

} // namespace Shapes