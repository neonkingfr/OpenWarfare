//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "ShapePolyLineWithStartEndDirection.h"

//-----------------------------------------------------------------------------

using namespace Shapes;

//-----------------------------------------------------------------------------

Shapes::DVector ShapePolyLineWithStartEndDirection::NearestEdge( const Shapes::DVector& vx ) const 
{
  return (IShape::NearestToEdge( vx ));
}

//-----------------------------------------------------------------------------

const char* ShapePolyLineWithStartEndDirection::GetShapeTypeName() const 
{
	return (s_name);
}

//-----------------------------------------------------------------------------

const char* ShapePolyLineWithStartEndDirection::s_name = "ShapePolyLineWithStartEndDirection";

//-----------------------------------------------------------------------------

void* ShapePolyLineWithStartEndDirection::GetInterfacePtr( size_t ifcuid ) 
{
  if ( ifcuid == IShapeExtra::IFCUID )
	{
		return (static_cast< IShapeExtra* >( this ));
	}
  else
	{
		return (ShapePolyLine::GetInterfacePtr( ifcuid ));
	}
}

//-----------------------------------------------------------------------------
