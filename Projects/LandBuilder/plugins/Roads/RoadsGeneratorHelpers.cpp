//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include <assert.h>
#include <algorithm>

#include "RoadsGeneratorHelpers.h"

//-----------------------------------------------------------------------------

namespace RG_Help
{

//-----------------------------------------------------------------------------

  static const double MAX_ANGLE_BETWEEN_SPLINE_SEGMENTS = PI / 32.0;
  static const size_t CROSSROADS_TEXTURE_SIZE           = 1024;

//-----------------------------------------------------------------------------

  Dxf2006Writer::Dxf2006Writer( const string& filename )
  {
    m_file.open( filename.c_str(), std::ios::out | std::ios::trunc );
    if ( !m_file.fail() ) 
    {
      WriteEntitiesHead();
    }
    else
    {
      LOGF( Error ) << "Failed to open dxf file.";
    }
  }

//-----------------------------------------------------------------------------

  Dxf2006Writer::~Dxf2006Writer()
  {
    if ( !m_file.fail() ) 
    {
      WriteEntitiesTail();
      WriteFileTail();
      m_file.close();
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WriteEntitiesHead()
  {
    if ( !m_file.fail() ) 
    {
      string out = "  0\nSECTION\n  2\nENTITIES";
      m_file << out << endl;
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WriteEntitiesTail()
  {
    if ( !m_file.fail() ) 
    {
      string out = "  0\nENDSEC";
      m_file << out << endl;
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WriteFileTail()
  {
    if ( !m_file.fail() ) 
    {
      string out = "  0\nEOF";
      m_file << out << endl;
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WriteCircle( const Shapes::DVertex& center, double radius, 
                                   const string& layer )
  {
    if ( !m_file.fail() ) 
    {
      char buffer[80];
      string out = "  0\nCIRCLE\n100\nAcDbEntity\n  8\n";
      out += (layer + "\n100\nAcDbCircle\n 10\n");
      _gcvt_s( buffer, 80, center.x, 8 );
      out += (string( buffer ) + "\n 20\n");
      _gcvt_s( buffer, 80, center.y, 8 );
      out += (string( buffer ) + "\n 40\n");
      _gcvt_s( buffer, 80, radius, 8 );
      out += string( buffer );
      m_file << out << endl;
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WritePolylineAsLines( const vector< Shapes::DVertex >& polyline, 
                                            const string& layer, bool close )
  {
    if ( !m_file.fail() ) 
    {
      size_t size = polyline.size();

      if ( size > 1 )
      {
        char buffer[80];
        for ( size_t i = 0; i < size - 1; ++i )
        {
          string out = "  0\nLINE\n100\nAcDbEntity\n  8\n";
          out += (layer + "\n100\nAcDbLine\n 10\n");
          _gcvt_s( buffer, 80, polyline[i].x, 8 );
          out += (string( buffer ) + "\n 20\n");
          _gcvt_s( buffer, 80, polyline[i].y, 8 );
          out += (string( buffer ) + "\n 11\n");
          _gcvt_s( buffer, 80, polyline[i + 1].x, 8 );
          out += (string( buffer ) + "\n 21\n");
          _gcvt_s( buffer, 80, polyline[i + 1].y, 8 );
          out += string( buffer );
          m_file << out << endl;
        }

        if ( close )
        {
          string out = "  0\nLINE\n100\nAcDbEntity\n  8\n";
          out += (layer + "\n100\nAcDbLine\n 10\n");
          _gcvt_s( buffer, 80, polyline[polyline.size() - 1].x, 8 );
          out += (string( buffer ) + "\n 20\n");
          _gcvt_s( buffer, 80, polyline[polyline.size() - 1].y, 8 );
          out += (string( buffer ) + "\n 11\n");
          _gcvt_s( buffer, 80, polyline[0].x, 8 );
          out += (string( buffer ) + "\n 21\n");
          _gcvt_s( buffer, 80, polyline[0].y, 8 );
          out += string( buffer );
          m_file << out << endl;
        }
      }
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WritePolyline( const vector< Shapes::DVertex >& polyline,
                                     const string& layer, bool close,
                                     double startingWidth, 
                                     double endingWidth )
  {
    if ( !m_file.fail() ) 
    {
      size_t size = polyline.size();

      if ( size > 1 )
      {
        char buffer[80];
        string out = "  0\nPOLYLINE\n100\nAcDbEntity\n  8\n";
        out += (layer + "\n 66\n     1\n100\nAcDb2dPolyline\n 10\n0.0\n 20\n0.0\n 30\n0.0\n");
        if (close)
        {
          out += " 70\n1\n";
        }
        else
        {
          out += " 70\n0\n";
        }
        _gcvt_s( buffer, 80, startingWidth, 8 );
        out += (" 40\n" + string(buffer) + "\n");
        _gcvt_s( buffer, 80, endingWidth, 8 );
        out += (" 41\n" + string(buffer) + "\n");
        for ( size_t i = 0; i < size; ++i )
        {
          out += "  0\nVERTEX\n100\nAcDbVertex\n  8\n";
          out += (layer + "\n100\nAcDb2dVertex\n 10\n");
          _gcvt_s( buffer, 80, polyline[i].x, 8 );
          out += (string( buffer ) + "\n 20\n");
          _gcvt_s( buffer, 80, polyline[i].y, 8 );
          out += (string( buffer ) + "\n 30\n0.0\n");
        }
        out += "  0\nSEQEND\n  8\n";
        out += layer;
        m_file << out << endl;
      }
    }
  }

//-----------------------------------------------------------------------------

  Road::Road( double width, int level, RString mainTextureFilename, 
              size_t maskColor)
  : m_width( width )
  , m_level( level )
  , m_mainTextureFilename( mainTextureFilename )
  , m_maskColor( maskColor )
  , m_startTerminator( true )
  , m_endTerminator( true )
  {
    GenerateEndTextureFilenameFromMainTextureFilename();
  }

//-----------------------------------------------------------------------------

  double Road::GetWidth() const
  {
    return (m_width);
  }

//-----------------------------------------------------------------------------

  size_t Road::GetLevel() const
  {
    return (m_level);
  }

//-----------------------------------------------------------------------------

  RString Road::GetMainTextureFilename() const
  {
    return (m_mainTextureFilename);
  }

//-----------------------------------------------------------------------------

  size_t Road::GetMaskColor() const
  {
    return (m_maskColor);
  }

//-----------------------------------------------------------------------------

  bool Road::HasStartTerminator() const
  {
    return (m_startTerminator);
  }

//-----------------------------------------------------------------------------

  bool Road::HasEndTerminator() const
  {
    return (m_endTerminator);
  }

//-----------------------------------------------------------------------------

  RString Road::GetEndTextureFilename() const
  {
    return (m_endTextureFilename);
  }

//-----------------------------------------------------------------------------

  void Road::SetWidth( double width )
  {
    m_width = width;
  }

//-----------------------------------------------------------------------------

  void Road::SetLevel( size_t level )
  {
    m_level = level;
  }

//-----------------------------------------------------------------------------

  void Road::SetMainTextureFilename( const RString& mainTextureFilename )
  {
    m_mainTextureFilename = mainTextureFilename;
    GenerateEndTextureFilenameFromMainTextureFilename();
  }

//-----------------------------------------------------------------------------

  void Road::SetMaskColor( size_t maskColor )
  {
    m_maskColor = maskColor;
  }

//-----------------------------------------------------------------------------

  void Road::SetStartTerminator( bool startTerminator )
  {
    m_startTerminator = startTerminator;
  }
  
//-----------------------------------------------------------------------------
  
  void Road::SetEndTerminator( bool endTerminator )
  {
    m_endTerminator = endTerminator;
  }

//-----------------------------------------------------------------------------

  vector< Shapes::DVertex >& Road::GetShape() 
  {
    return (m_shape);
  }

//-----------------------------------------------------------------------------

  const vector< Shapes::DVertex >& Road::GetShape() const
  {
    return (m_shape);
  }

//-----------------------------------------------------------------------------

  void Road::SetShape( const vector< Shapes::DVertex >& shape ) 
  {
    m_shape = shape;
  }

//-----------------------------------------------------------------------------

  void Road::AddShapeVertex( const Shapes::DVertex& vertex )
  {
    m_shape.push_back( vertex );
  }

//-----------------------------------------------------------------------------

  void Road::RemoveShapeVertex( size_t index )
  {
    assert( index < m_shape.size() );
    m_shape.erase( m_shape.begin() + index );
  }

//-----------------------------------------------------------------------------

  size_t Road::GetShapeVerticesCount() const
  {
    return (m_shape.size());
  }

//-----------------------------------------------------------------------------

  const Shapes::DVertex& Road::GetShapeVertex( size_t index ) const
  {
    assert( index < m_shape.size() );
    return (m_shape[index]);
  }

//-----------------------------------------------------------------------------

  Shapes::DVertex& Road::GetShapeVertex( size_t index )
  {
    assert( index < m_shape.size() );
    return (m_shape[index]);
  }

//-----------------------------------------------------------------------------

  void Road::SetShapeVertex( size_t index, const Shapes::DVertex& vertex )
  {
    assert( index < m_shape.size() );
    m_shape[index] = vertex;
  }

//-----------------------------------------------------------------------------

  const Shapes::DVertex& Road::GetShapeStartingVertex() const
  {
    return (m_shape[0]);
  }

//-----------------------------------------------------------------------------

  const Shapes::DVertex& Road::GetShapeEndingVertex() const
  {
    return (m_shape[m_shape.size() - 1]);
  }

//-----------------------------------------------------------------------------

  Shapes::DVector Road::GetShapeStartingVector() const
  {
    return (Shapes::DVector( m_shape[1], m_shape[0] ));
  }

//-----------------------------------------------------------------------------

  Shapes::DVector Road::GetShapeEndingVector() const
  {
    size_t size = m_shape.size();
    return (Shapes::DVector( m_shape[size - 1], m_shape[size - 2] ));
  }

//-----------------------------------------------------------------------------

  bool Road::GenerateSpline()
  {
    AutoArray< DVertexEx > points;
    size_t cnt = m_shape.size();
    points.Reserve( cnt + 4 );

    points.Add( m_shape[0] );
    points.Add( m_shape[0] );

    for ( size_t j = 0 ; j < cnt; ++j )
    {
      points.Add( m_shape[j] );
    }

    points.Add( m_shape[cnt - 1] );
    points.Add( m_shape[cnt - 1] );

    BSpline< 4, double, Array< DVertexEx > > bspline( points, points.Size() );
        
    // first spline attemp
    FirstSplineAttempt( m_width, bspline );

    // then refines the splines adapting the resolution to the pattern
    return (RefineSpline( bspline ));
  }

//-----------------------------------------------------------------------------

  vector< Shapes::DVertex >& Road::GetSpline() 
  {
    return (m_spline);
  }

//-----------------------------------------------------------------------------

  const vector< Shapes::DVertex >& Road::GetSpline() const
  {
    return (m_spline);
  }

//-----------------------------------------------------------------------------

  void Road::AddSplineVertex( const Shapes::DVertex& vertex )
  {
    m_spline.push_back( vertex );
  }

//-----------------------------------------------------------------------------

  void Road::InsertSplineVertex( size_t index, const Shapes::DVertex& vertex )
  {
    assert( index < m_spline.size() );
    m_spline.insert( m_spline.begin() + index, vertex );
  }

//-----------------------------------------------------------------------------

  void Road::GenerateModelsContours( double maxDimension )
  {
    GenerateSegmentModelsContours();
    MergeSegmentModelsContours();
    EnhancedMergeModelsContours( maxDimension );
  }

//-----------------------------------------------------------------------------

  size_t Road::GetModelsContoursCount()
  {
    return (m_modelsContours.size());
  }

//-----------------------------------------------------------------------------

  vector< Contour >& Road::GetModelsContours()
  {
    return (m_modelsContours);
  }

//-----------------------------------------------------------------------------

  const vector< Contour >& Road::GetModelsContours() const
  {
    return (m_modelsContours);
  }

//-----------------------------------------------------------------------------

  Contour& Road::GetModelContour( size_t index )
  {
    assert( index < m_modelsContours.size() );
    return (m_modelsContours[index]);
  }

//-----------------------------------------------------------------------------

  const Contour& Road::GetModelContour( size_t index ) const
  {
    assert( index < m_modelsContours.size() );
    return (m_modelsContours[index]);
  }

//-----------------------------------------------------------------------------

  void Road::GenerateP3DModels( IMainCommands* cmdIfc, size_t roadCounter,
                                RString modelsPath, RString texturesPath )
  {
    for ( size_t i = 0, cnt = m_modelsContours.size(); i < cnt; ++i )
    {
      vector< Texture > textures;
      textures.push_back( Texture( texturesPath, m_mainTextureFilename, 0 ) );

      TerminatorTypes terminator = Terminator_NONE;
      if ( i == 0 && m_startTerminator )
      {
        terminator = Terminator_START;
      }
      if ( i == (cnt - 1) && m_endTerminator )
      {
        terminator = Terminator_END;
      }

      if ( terminator != Terminator_NONE )
      {
        textures.push_back( Texture( texturesPath, m_endTextureFilename, 0 ) );
      }

      char buffer[80];
      sprintf( buffer, "road_%d_%d", roadCounter, i );
      RString modelName( buffer );
      ModelP3D* modelP3D = new RoadModelP3D( modelName, m_modelsContours[i],
                                             modelsPath, texturesPath, 
                                             textures, terminator );
      modelP3D->ExportToP3DFile();

      Shapes::DVertex mPosition = modelP3D->GetPosition();
      Vector3D position  = Vector3D( mPosition.x, 
                                             0.0, 
                                     mPosition.y );

      Matrix4D mTranslation = Matrix4D( MTranslation, position );
      Matrix4D mRotation    = Matrix4D( MRotationY, modelP3D->GetOrientation() );

      Matrix4D transform = mTranslation * mRotation;
      cmdIfc->GetObjectMap()->PlaceObject( transform, modelName, 0 );			

      delete modelP3D;
      m_modelsPositions.push_back( mPosition );
    }
  }

//-----------------------------------------------------------------------------

  const vector< Shapes::DVertex >& Road::GetModelsPositions() const
  {
    return (m_modelsPositions);
  }

//-----------------------------------------------------------------------------

  void Road::AddModelPosition( const Shapes::DVertex& position )
  {
    m_modelsPositions.push_back( position );
  }

//-----------------------------------------------------------------------------

  void Road::InsertModelPosition( size_t index, const Shapes::DVertex& position )
  {
    assert( index < m_modelsPositions.size() );
    m_modelsPositions.insert( m_modelsPositions.begin() + index, position );
  }

//-----------------------------------------------------------------------------

  void Road::PaintOnMask( HDC dc, HBITMAP bmp, DoubleRect mapRect, size_t x, size_t y, size_t maskSize )
  {
    HBRUSH oldBrush = (HBRUSH)SelectObject( dc, GetStockObject( DC_BRUSH ) );
    Color c( m_maskColor );
    COLORREF color = RGB( c.GetRed(), c.GetGreen(), c.GetBlue() );
    SetDCBrushColor( dc, color );
    HPEN oldPen = (HPEN)SelectObject( dc, GetStockObject( DC_PEN ) );
    SetDCPenColor( dc, color );
    SetROP2(dc, R2_COPYPEN);

    for ( size_t i = 0, cnt = m_modelsContours.size(); i < cnt; ++i )
    {
      m_modelsContours[i].PaintOnMask( dc, bmp, mapRect, x, y, maskSize );
    }

    SelectObject( dc, oldBrush );
    SelectObject( dc, oldPen );
  }

//-----------------------------------------------------------------------------

  void Road::GenerateEndTextureFilenameFromMainTextureFilename()
  {
    if ( m_mainTextureFilename == "" )
    {
      m_endTextureFilename = "";
      return;
    }

    // assumes that the main texture filename ends with "_ca"
    // all checks are to be made before setting it by the caller

    int pos = m_mainTextureFilename.GetLength() - 3;
    m_endTextureFilename = m_mainTextureFilename.Substring( 0, pos ) + "_end_ca";
  }

//-----------------------------------------------------------------------------

  void Road::FirstSplineAttempt( double step, BSpline< 4, double, Array< DVertexEx > >& bspline )
  {
    ClearSpline();

    double curT = 0.0;
    double maxT = bspline.MaxT();

    m_shape[0].m = 1.0;
    m_shape[m_shape.size() - 1].m = 1.0;

    m_spline.push_back( m_shape[0] );

    while ( fabs( curT - maxT ) > EPSILON ) 
    {          
      double pos[4];
      curT = bspline.GetNextPoint( curT, step, NULL, pos );
      m_spline.push_back( Shapes::DVertex( pos[0], pos[1], 0.0, 1.0 ) );
    }		

    size_t cnt = m_spline.size();
    double last = m_spline[cnt - 2].DistanceXY( m_spline[cnt - 1] );

    if ( last < 0.1 )
    {
      m_spline.erase( m_spline.begin() + cnt - 1 );
//      m_spline[cnt - 2] = m_shape[m_shape.size() - 1];
    }

    m_spline[m_spline.size() - 1] = m_shape[m_shape.size() - 1];
  }

//-----------------------------------------------------------------------------

  bool Road::RefineSpline( BSpline< 4, double, Array< DVertexEx > >& bspline )
  {
    if ( m_spline.size() < 3 ) { return (false); }

    for ( size_t i = 0, cnt = m_spline.size(); i < cnt - 2; ++i )
    {
      Shapes::DVector v01( m_spline[i + 1], m_spline[i] );
      Shapes::DVector v12( m_spline[i + 2], m_spline[i + 1] );
      v01.NormalizeXYInPlace();
      v12.NormalizeXYInPlace();
      double teta = acos( v01.DotXY( v12 ) );
      double f = 1.0;
      while ( teta / f > MAX_ANGLE_BETWEEN_SPLINE_SEGMENTS )
      {
        f *= 2.0;
      }
      if ( m_spline[i].m < f ) { m_spline[i].m = f; }
      m_spline[i + 1].m = f;
    }

    vector< Shapes::DVertex > tempSpline;

    tempSpline.push_back( m_spline[0] );
    tempSpline[0].m = 0.0;

    double curT = 0.0;
    double lastStep = 0.0;
    double averageStep = SplineAverageSegmentLength();

    for ( size_t i = 0, cnt = m_spline.size(); i < cnt - 1; ++i )
    {
      size_t segResol = static_cast< size_t >( m_spline[i].m );
      double step = averageStep / segResol;
      for ( size_t j = 1; j <= segResol; ++j )
      {
        double pos[4];
        curT = bspline.GetNextPoint( curT, step, &lastStep, pos );
        tempSpline.push_back( Shapes::DVertex( pos[0], pos[1], 0.0, (double)i + (double)j / (double)segResol ) );
      }
    }

    size_t cnt = tempSpline.size();
    double last = tempSpline[cnt - 2].DistanceXY( tempSpline[cnt - 1] );

    if ( last < 0.1 )
    {
      tempSpline.erase( tempSpline.begin() + cnt - 1 );
      tempSpline[cnt - 2]   = m_spline[m_spline.size() - 1];
      tempSpline[cnt - 2].m = m_spline.size() - 1;
    }

    tempSpline[tempSpline.size() - 1]   = m_spline[m_spline.size() - 1];
    tempSpline[tempSpline.size() - 1].m = m_spline.size() - 1;
    m_spline = tempSpline;

    // this condition try to get rid of particular case
    // when a spline long more or less as the width is divided
    // into multiple pieces
    if ( m_spline.size() > 2 )
    {
      if ( averageStep < 0.51 * m_width )
      {
        size_t cnt = m_spline.size();
        double m = m_spline[cnt - 1].m;
        for ( size_t i = 0; i < cnt; ++i )
        {
          m_spline[i].m /= m;
        }
      }
    }

    return (true);
  }

//-----------------------------------------------------------------------------

  void Road::ClearSpline()
  {
    m_spline.clear();
  }

//-----------------------------------------------------------------------------

  double Road::SplineLength()
  {
    double len = 0.0;
    Shapes::DVertex v0;
    Shapes::DVertex v1;
    for ( size_t i = 0, cnt = m_spline.size() - 1; i < cnt; ++i )
    {
      v0 = m_spline[i];
      v1 = m_spline[i + 1];
      len += v0.DistanceXY(v1);
    }
    return (len);
  }

//-----------------------------------------------------------------------------

  double Road::SplineAverageSegmentLength()
  {
    if ( m_spline.size() < 2 ) { return (0.0); }
    return (SplineLength() / (m_spline.size() - 1));
  }

//-----------------------------------------------------------------------------

  void Road::GenerateSegmentModelsContours()
  {
    double width = m_width * 0.5;
    m_modelsContours.clear();

    // current segment
    Shapes::DVertex v0;
    Shapes::DVertex v1;
    Shapes::DVector v01;
    Shapes::DVector perpR;
    Shapes::DVector perpL;

    // previous segment
    Shapes::DVertex vP0;
    Shapes::DVertex vP1;
    Shapes::DVector vP01;
    Shapes::DVector perpPR;
    Shapes::DVector perpPL;

    // next segment
    Shapes::DVertex vN0;
    Shapes::DVertex vN1;
    Shapes::DVector vN01;
    Shapes::DVector perpNR;
    Shapes::DVector perpNL;

    // mean vectors
    Shapes::DVector meanPerpR;
    Shapes::DVector meanPerpL;

    // the current contour
    vector< Shapes::DVertex > p( 4 );

    // first generates a contour for every spline's segment
    for ( size_t i = 0, cnt = m_spline.size(); i < cnt - 1; ++i )
    {
      v0 = m_spline[i];
      v1 = m_spline[i + 1];
      v01 = Shapes::DVector( v1, v0 );
      v01.NormalizeXYInPlace();
      perpR = v01.PerpXYCW();
      perpR *= width;
      perpL = v01.PerpXYCCW();
      perpL *= width;

      if ( i > 0 )
      {
        // previous segment
        vP0 = m_spline[i - 1];
        vP1 = v0;
        vP01 = Shapes::DVector( vP1, vP0 );
        vP01.NormalizeXYInPlace();
        perpPR = vP01.PerpXYCW();
        perpPR *= width;
        perpPL = vP01.PerpXYCCW();
        perpPL *= width;
      }

      if ( i < (cnt - 2) )
      {
        // next segment
        vN0 = v1;
        vN1 = m_spline[i + 2];
        vN01 = Shapes::DVector( vN1, vN0 );
        vN01.NormalizeXYInPlace();
        perpNR = vN01.PerpXYCW();
        perpNR *= width;
        perpNL = vN01.PerpXYCCW();
        perpNL *= width;
      }

      if ( i == 0 )
      {
        p[0] = perpR.GetSecondEndXY( v0 );
        p[3] = perpL.GetSecondEndXY( v0 );

        meanPerpR = perpR + perpNR;
        meanPerpL = perpL + perpNL;
        meanPerpR.NormalizeXYInPlace();
        meanPerpR *= width;
        meanPerpL.NormalizeXYInPlace();
        meanPerpL *= width;
        p[1] = meanPerpR.GetSecondEndXY( v1 );
        p[2] = meanPerpL.GetSecondEndXY( v1 );
      }
      else if ( i == (cnt - 2) )
      {
        meanPerpR = perpR + perpPR;
        meanPerpL = perpL + perpPL;
        meanPerpR.NormalizeXYInPlace();
        meanPerpR *= width;
        meanPerpL.NormalizeXYInPlace();
        meanPerpL *= width;
        p[0] = meanPerpR.GetSecondEndXY( v0 );
        p[3] = meanPerpL.GetSecondEndXY( v0 );

        p[1] = perpR.GetSecondEndXY( v1 );
        p[2] = perpL.GetSecondEndXY( v1 );
      }
      else
      {
        meanPerpR = perpR + perpPR;
        meanPerpL = perpL + perpPL;
        meanPerpR.NormalizeXYInPlace();
        meanPerpR *= width;
        meanPerpL.NormalizeXYInPlace();
        meanPerpL *= width;
        p[0] = meanPerpR.GetSecondEndXY( v0 );
        p[3] = meanPerpL.GetSecondEndXY( v0 );

        meanPerpR = perpR + perpNR;
        meanPerpL = perpL + perpNL;
        meanPerpR.NormalizeXYInPlace();
        meanPerpR *= width;
        meanPerpL.NormalizeXYInPlace();
        meanPerpL *= width;
        p[1] = meanPerpR.GetSecondEndXY( v1 );
        p[2] = meanPerpL.GetSecondEndXY( v1 );
      }

      p[0].m = p[3].m = v0.m;
      p[1].m = p[2].m = v1.m;
      m_modelsContours.push_back( Contour( p ) );
    }
  }

//-----------------------------------------------------------------------------

  void Road::MergeSegmentModelsContours()
  {
    vector< size_t > toBeDeleted;
    size_t lastModel = 0;
    for ( size_t i = 0, cnt = m_spline.size(); i < cnt - 1; ++i )
    {
      if ( abs( m_spline[i].m - floor( m_spline[i].m ) ) > EPSILON )
      {
        if ( i > 0 )
        {
          size_t pos = m_modelsContours[lastModel].GetVerticesCount() / 2;
          m_modelsContours[lastModel].InsertVertex( pos, m_modelsContours[i].GetVertex( 1 ) );
          m_modelsContours[lastModel].InsertVertex( pos + 1, m_modelsContours[i].GetVertex( 2 ) );
          toBeDeleted.push_back( i );
        }
      }
      else
      {
        lastModel = i;
      }
    }

    for ( int i = toBeDeleted.size() - 1; i >= 0; --i )
    {
      m_modelsContours.erase( m_modelsContours.begin() + toBeDeleted[i] );
    }
  }

//-----------------------------------------------------------------------------

  void Road::EnhancedMergeModelsContours( double maxDimension )
  {
    // see void Road::GenerateMultiModels() in RoadsGenerator.h
    size_t cnt = m_modelsContours.size();
    int start = 0;     // must be signed to prevent underflow
    int end = cnt - 1; // must be signed to prevent underflow
    size_t lastModel = 0;
    vector< size_t > toBeDeleted;

    if ( m_startTerminator ) 
    {
      ++start;
      ++lastModel;
    }

    if ( m_endTerminator )   
    {
      --end;
    }

    if ( end > start )
    {
      for ( int i = start; i <= end; ++i )
      {
        if ( i != lastModel )
        {
          Contour tempContour = m_modelsContours[lastModel];
          size_t pos = tempContour.GetVerticesCount() / 2;
          vector< Shapes::DVertex > tempList;
          for ( size_t j = 1, vCnt = m_modelsContours[i].GetVerticesCount(); j < vCnt - 1; ++j )
          {
            tempList.push_back( m_modelsContours[i].GetVertex( j ) );
          }
          tempContour.InsertVertices( pos, tempList );
          if ( tempContour.GetMinBoundingBoxWidth() < maxDimension )
          {
            m_modelsContours[lastModel] = tempContour;
            toBeDeleted.push_back( i );
          }
          else
          {
            lastModel = i;
          }
        }
      }
    }

    for ( int i = toBeDeleted.size() - 1; i >= 0; --i )
    {
      m_modelsContours.erase( m_modelsContours.begin() + toBeDeleted[i] );
    }
  }

//-----------------------------------------------------------------------------
    
  XRoad::XRoad( Road* road, RoadEndTypes roadEndType )
  : m_road( road )
  , m_roadEndType( roadEndType )
  {
  }

//-----------------------------------------------------------------------------

  Road* XRoad::GetRoad()
  {
    return (m_road);
  }

//-----------------------------------------------------------------------------

  const Road* XRoad::GetRoad() const
  {
    return (m_road);
  }

//-----------------------------------------------------------------------------

  RoadEndTypes XRoad::GetRoadEndType() const
  {
    return (m_roadEndType);
  }

//-----------------------------------------------------------------------------

  Shapes::DVector XRoad::EndVectorInsideCrossroad() const
  {
    if ( m_roadEndType == etStarting )
    {
      return (m_road->GetShapeStartingVector());
    }
    else
    {
      return (m_road->GetShapeEndingVector() * (-1.0));
    }
  }

//-----------------------------------------------------------------------------

  double XRoad::AzimuthEndVectorInsideCrossroad() const
  {
    Shapes::DVector v = EndVectorInsideCrossroad();
    return (v.DirectionXY());
  }

//-----------------------------------------------------------------------------

  RoadsTransitionBuilder::RoadsTransitionBuilder( const Shapes::DVertex& crossroadCenter, 
                                                  double crossroadRadius, 
                                                  XRoad* road1, XRoad* road2, 
                                                  size_t resolution )
  : m_crossroadCenter( crossroadCenter )
  , m_crossroadRadius( crossroadRadius )
  , m_road1( road1 )
  , m_road2( road2 )
  , m_resolution( resolution )
  {
    CalculateGeometry();
    CalculateTransitionPoints();
  }

//-----------------------------------------------------------------------------

  vector< Shapes::DVertex >& RoadsTransitionBuilder::GetTransitionPoints()
  {
    return (m_transitionPoints);
  }

//-----------------------------------------------------------------------------

  const vector< Shapes::DVertex >& RoadsTransitionBuilder::GetTransitionPoints() const
  {
    return (m_transitionPoints);
  }

//-----------------------------------------------------------------------------

  void RoadsTransitionBuilder::CalculateGeometry()
  {
    // assumes that roads have been already ordered by the azimut
    // of their segment inside the crossroad

    if ( m_road1->GetRoadEndType() == etStarting )
    {
      m_vecRoad1 = m_road1->GetRoad()->GetShapeStartingVector();
    }
    else
    {
      m_vecRoad1 = m_road1->GetRoad()->GetShapeEndingVector() * (-1.0);
    }
    m_vecRoad1.NormalizeXYInPlace();
    m_perpVecRoad1 = m_vecRoad1.PerpXYCCW();
    m_vecRoad1 *= m_crossroadRadius;
    m_perpVecRoad1 *= (m_road1->GetRoad()->GetWidth() * 0.5);

    if ( m_road2->GetRoadEndType() == etStarting )
    {
      m_vecRoad2 = m_road2->GetRoad()->GetShapeStartingVector();
    }
    else
    {
      m_vecRoad2 = m_road2->GetRoad()->GetShapeEndingVector() * (-1.0);
    }
    m_vecRoad2.NormalizeXYInPlace();
    m_perpVecRoad2 = m_vecRoad2.PerpXYCW();
    m_vecRoad2 *= m_crossroadRadius;
    m_perpVecRoad2 *= (m_road2->GetRoad()->GetWidth() * 0.5);

    m_angle = m_vecRoad2.DirectionXY() - m_vecRoad1.DirectionXY();
    // the angle is always negative for the last builder in every crossroad
    if ( m_angle < 0.0 ) 
    {
      m_angle += TWO_PI;
      m_last = true;
    }
    else
    {
      m_last = false;
    }

    // Old end vertex for both roads is the crossroad center
    m_newEndVertexRoad1 = m_vecRoad1.GetSecondEndXY( m_crossroadCenter );
    m_newEndVertexRoad2 = m_vecRoad2.GetSecondEndXY( m_crossroadCenter );

    // left and right are as seen from the crossroad center
    m_road1EndOnLBorder = m_perpVecRoad1.GetSecondEndXY( m_newEndVertexRoad1 );
    m_road2EndOnRBorder = m_perpVecRoad2.GetSecondEndXY( m_newEndVertexRoad2 );

    // calculates the center of the transition
    // there is no center if the two roads are aligned
    if ( abs( m_angle - PI ) > EPSILON )
    {
      Line line1( m_newEndVertexRoad1, m_road1EndOnLBorder );
      Line line2( m_newEndVertexRoad2, m_road2EndOnRBorder );
      line1.Intersection( line2, m_center );
    }

    // if there is a center, calculates the two radii vectors
    if ( m_center.x != DBL_MAX )
    {
      m_vecRadius1 = Shapes::DVector( m_road1EndOnLBorder, m_center );
      m_vecRadius2 = Shapes::DVector( m_road2EndOnRBorder, m_center );
    }
  }

//-----------------------------------------------------------------------------

  void RoadsTransitionBuilder::CalculateTransitionPoints()
  {
    if ( abs( m_angle - PI ) <= EPSILON )
    {
      // rectilinear transition
      Shapes::DVector line12( m_road2EndOnRBorder, m_road1EndOnLBorder );
      double step = line12.SizeXY() / m_resolution;
      line12.NormalizeXYInPlace();

      m_transitionPoints.push_back( m_road1EndOnLBorder );
      Shapes::DVector tempLine;
      for ( size_t i = 1; i <= m_resolution; ++i )
      {
        tempLine = line12 * (i * step);
        m_transitionPoints.push_back( tempLine.GetSecondEndXY( m_road1EndOnLBorder ) );
      }
    }
    else
    {
      // rounded transition
      double startAngle  = m_vecRadius1.DirectionXY();
      double startRadius = m_vecRadius1.SizeXY();
      double endAngle    = m_vecRadius2.DirectionXY();
      double endRadius   = m_vecRadius2.SizeXY();

      if ( m_last && startAngle > endAngle && m_angle > PI )
      {
        endAngle += TWO_PI;
      }

      if ( startAngle < endAngle && m_angle < PI )
      {
        startAngle += TWO_PI;
      }

      double angleStep     = (endAngle - startAngle) / m_resolution;
      double radiusDiff    = endRadius - startRadius;
      double absRadiusDiff = abs( radiusDiff );
      double minRadius     = min( startRadius, endRadius );

      double d, t, r, x, y;
      double invResolution = 1.0 / m_resolution;
      for ( size_t i = 0; i <= m_resolution; ++i )
      {
        d = startAngle + i * angleStep;
        t = i * invResolution;
        if ( radiusDiff < 0 )
        {
          t = 1.0 - t;
        }
//				r = minRadius + t * absRadiusDiff;     // linear -> spiral
        r = minRadius + t * t * absRadiusDiff; // quadratic
        x = m_center.x + r * cos( d );
        y = m_center.y + r * sin( d );
        m_transitionPoints.push_back( Shapes::DVertex( x, y, 0.0 ) );
      }
    }
  }

//-----------------------------------------------------------------------------

  Crossroad::Crossroad( const Shapes::DVertex& position, size_t resolution )
  : m_position( position )
  , m_resolution( resolution )
  {
  }

//-----------------------------------------------------------------------------

  Shapes::DVertex Crossroad::GetPosition() const
  {
    return (m_position);
  }

//-----------------------------------------------------------------------------

  double Crossroad::GetRadius() const
  {
    return (m_radius);
  }

//-----------------------------------------------------------------------------

  const Contour& Crossroad::GetContour() const
  {
    return (m_contour);
  }

//-----------------------------------------------------------------------------

  size_t Crossroad::GetLevel() const
  {
    return (m_levelToBuild);
  }

//-----------------------------------------------------------------------------

  void Crossroad::SetPosition( const Shapes::DVertex& position )
  {
    m_position = position;
  }

//-----------------------------------------------------------------------------

  bool Crossroad::AddXRoad( XRoad& road )
  {
    // we cannot have more than 4 roads of the same level in each crossroad
    vector< int > levels;
    size_t levelsCount = MaxLevel() + 1;

    for ( size_t i = 0; i < levelsCount; ++i )
    {
      levels.push_back( 0 );
    }

    for ( size_t i = 0, cnt = m_xRoadsList.size(); i < cnt; ++i )
    {
      levels[m_xRoadsList[i].GetRoad()->GetLevel()] += 1;
    }

    int max = levels[0];

    for ( size_t i = 1; i < levelsCount; ++i )
    {
      if ( levels[i] > max )
      {
        max = levels[i];
      }
    }

    if ( max == 4 ) { return (false); }

    m_xRoadsList.push_back( road );

    // sets road's ends as no terminator
    if ( road.GetRoadEndType() == etStarting )
    {
      road.GetRoad()->SetStartTerminator( false );
    }
    else
    {
      road.GetRoad()->SetEndTerminator( false );
    }

    return (true);
  }

//-----------------------------------------------------------------------------

  size_t Crossroad::XRoadsCount() const
  {
    return (m_xRoadsList.size());
  }

//-----------------------------------------------------------------------------

  XRoad& Crossroad::GetXRoad( size_t index )
  {
    assert( index < m_xRoadsList.size() );
    return (m_xRoadsList[index]);
  }

//-----------------------------------------------------------------------------

  const XRoad& Crossroad::GetXRoad( size_t index ) const
  {
    assert( index < m_xRoadsList.size() );
    return (m_xRoadsList[index]);
  }

//-----------------------------------------------------------------------------

  void Crossroad::MatchXRoadsWithContour( double overlap )
  {
    size_t builderCount = 0;
    size_t vCount = 0;

    for ( size_t i = 0, roadCnt = m_xRoadsList.size(); i < roadCnt; ++i )
    {
      XRoad& curXRoad = m_xRoadsList[i];
      Road*  curRoad  = curXRoad.GetRoad();

      if ( curRoad->GetLevel() == m_levelToBuild )
      {
        Shapes::DVertex crossRoadR;
        Shapes::DVertex crossRoadL;

        if ( builderCount == 0 )
        {
          crossRoadR = m_contour.GetVertex( m_contour.GetVerticesCount() - 1 );
          crossRoadL = m_contour.GetVertex( 0 );
        }
        else
        {
          crossRoadR = m_contour.GetVertex( vCount - 1 );
          crossRoadL = m_contour.GetVertex( vCount );
        }

        vCount += (m_resolution + 1);
        ++builderCount;

        size_t roadModelsCount = curRoad->GetModelsContoursCount();
        // warning may happen that there are no models if the road is 
        // completely inside the crossroad circle
        if ( roadModelsCount > 0 )
        {
          if ( curXRoad.GetRoadEndType() == etStarting )
          {
            Contour& modelContour = const_cast< Contour& >( curRoad->GetModelContour( 0 ) );
            size_t nVertices = modelContour.GetVerticesCount();
            crossRoadR.m = modelContour.GetVertex( 0 ).m;
            crossRoadL.m = modelContour.GetVertex( nVertices - 1 ).m;
            modelContour.SetVertex( 0, crossRoadR );
            modelContour.SetVertex( nVertices - 1, crossRoadL );
          }
          else
          {
            Contour& modelContour = const_cast< Contour& >( curRoad->GetModelContour( roadModelsCount - 1 ) );
            size_t nVertices = modelContour.GetVerticesCount();
            crossRoadR.m = modelContour.GetVertex( nVertices / 2 ).m;
            crossRoadL.m = modelContour.GetVertex( nVertices / 2 - 1 ).m;
            modelContour.SetVertex( nVertices / 2, crossRoadR );
            modelContour.SetVertex( nVertices / 2 - 1, crossRoadL );
          }
        }
      }
      else
      {
        size_t roadModelsCount = curRoad->GetModelsContoursCount();
        // warning may happen that there are no models if the road is 
        // completely inside the crossroad circle
        if ( roadModelsCount > 0 )
        {
          if ( curXRoad.GetRoadEndType() == etStarting )
          {
//						Contour& modelContour = const_cast<Contour&>(curRoad->GetModelContour(0));
            Contour& modelContour = curRoad->GetModelContour( 0 );
            size_t nVertices = modelContour.GetVerticesCount();
            const Shapes::DVertex& v0L = modelContour.GetVertex( nVertices - 1 ); 
            const Shapes::DVertex& v1L = modelContour.GetVertex( nVertices - 2 ); 
            const Shapes::DVertex& v0R = modelContour.GetVertex( 0 ); 
            const Shapes::DVertex& v1R = modelContour.GetVertex( 1 ); 

            Shapes::DVertex intersL;
            if ( m_contour.IntersectionWithSegment( Line( v0L, v1L ), intersL, false ) )
            {
              Shapes::DVector v( intersL, v1L );
              if ( overlap > 0.0 )
              {
                double initLen = v.SizeXY();
                v.NormalizeXYInPlace();
                v *= (initLen + overlap);
              }
              Shapes::DVertex pL = v.GetSecondEndXY( v1L );
              pL.m = v0L.m;
              modelContour.SetVertex( nVertices - 1, pL );
            }

            Shapes::DVertex intersR;
            if ( m_contour.IntersectionWithSegment( Line( v0R, v1R ), intersR, false ) )
            {
              Shapes::DVector v( intersR, v1R );
              if ( overlap > 0.0 )
              {
                double initLen = v.SizeXY();
                v.NormalizeXYInPlace();
                v *= (initLen + overlap);
              }
              Shapes::DVertex pR = v.GetSecondEndXY( v1R );
              pR.m = v0R.m;
              modelContour.SetVertex( 0, pR );
            }
          }
          else
          {
//						Contour& modelContour = const_cast<Contour&>(curRoad->GetModelContour(roadModelsCount - 1));
            Contour& modelContour = curRoad->GetModelContour( roadModelsCount - 1 );
            size_t nVertices = modelContour.GetVerticesCount();
            const Shapes::DVertex& v0L = modelContour.GetVertex( nVertices / 2 - 1 ); 
            const Shapes::DVertex& v1L = modelContour.GetVertex( nVertices / 2 - 2 ); 
            const Shapes::DVertex& v0R = modelContour.GetVertex( nVertices / 2 ); 
            const Shapes::DVertex& v1R = modelContour.GetVertex( nVertices / 2 + 1 ); 

            Shapes::DVertex intersL;
            if ( m_contour.IntersectionWithSegment( Line( v0L, v1L ), intersL, false ) )
            {
              Shapes::DVector v( intersL, v1L );
              if ( overlap > 0.0 )
              {
                double initLen = v.SizeXY();
                v.NormalizeXYInPlace();
                v *= (initLen + overlap);
              }
              Shapes::DVertex pL = v.GetSecondEndXY( v1L );
              pL.m = v0L.m;
              modelContour.SetVertex( nVertices / 2 - 1, pL );
            }

            Shapes::DVertex intersR;
            if ( m_contour.IntersectionWithSegment( Line( v0R, v1R ), intersR, false ) )
            {
              Shapes::DVector v( intersR, v1R );
              if ( overlap > 0.0 )
              {
                double initLen = v.SizeXY();
                v.NormalizeXYInPlace();
                v *= (initLen + overlap);
              }
              Shapes::DVertex pR = v.GetSecondEndXY( v1R );
              pR.m = v0R.m;
              modelContour.SetVertex( nVertices / 2, pR );
            }
          }
        }
      }
    }
  }

//-----------------------------------------------------------------------------

  void Crossroad::ExtendXRoadSplinesToModelPosition()
  {
    for ( size_t i = 0, cnt = m_xRoadsList.size(); i < cnt; ++i )
    {
      XRoad& curXRoad = m_xRoadsList[i];
      Road*  curRoad  = curXRoad.GetRoad();

      if ( curXRoad.GetRoadEndType() == etStarting )
      {
        curRoad->InsertSplineVertex( 0, m_modelPosition );
        curRoad->InsertModelPosition( 0, m_modelPosition );
      }
      else
      {
        curRoad->AddSplineVertex( m_modelPosition );
        curRoad->AddModelPosition( m_modelPosition );
      }
    }
  }

//-----------------------------------------------------------------------------

  void Crossroad::GenerateP3DModels( IMainCommands* cmdIfc, RString modelsPath,
                                     RString texturesPath)
  {
    vector< Texture > textures;
    vector< RoadEndTypes > roadEndTypes;
    size_t roadCnt = MergedRoadsCount();
    if ( roadCnt == 2 )
    {
      for ( size_t i = 0, cnt = m_xRoadsList.size(); i < cnt; ++i )
      {
        if ( m_xRoadsList[i].GetRoad()->GetLevel() == m_levelToBuild )
        {
          textures.push_back( Texture( texturesPath, m_xRoadsList[i].GetRoad()->GetMainTextureFilename(), 0 ) );
          roadEndTypes.push_back( m_xRoadsList[i].GetRoadEndType() );
        }
      }
    }

    char buffer[80];
    sprintf( buffer, "crossroad_%d_%d", (int)m_position.x, (int)m_position.y );
    RString modelName( buffer );
    ModelP3D* modelP3D = new CrossroadModelP3D( modelName, m_contour, modelsPath, 
                                                texturesPath, textures, 
                                                CROSSROADS_TEXTURE_SIZE,
                                                m_resolution, roadEndTypes,
                                                roadCnt );
    modelP3D->ExportToP3DFile();

    Shapes::DVertex mPosition = modelP3D->GetPosition();
    Vector3D position  = Vector3D( mPosition.x, 
                                           0.0, 
                                   mPosition.y );

    Matrix4D mTranslation = Matrix4D( MTranslation, position );
    Matrix4D mRotation    = Matrix4D( MRotationY, modelP3D->GetOrientation() );

    Matrix4D transform = mTranslation * mRotation;
    cmdIfc->GetObjectMap()->PlaceObject( transform, modelName, 0 );			

    delete modelP3D;
    m_modelPosition = mPosition;
  }

//-----------------------------------------------------------------------------

  Shapes::DVertex Crossroad::GetModelPosition() const
  {
    return (m_modelPosition);
  }

//-----------------------------------------------------------------------------

  void Crossroad::PaintOnMask( HDC dc, HBITMAP bmp, DoubleRect mapRect, size_t x,
                               size_t y, size_t maskSize)
  {
    size_t red   = 0;
    size_t green = 0;
    size_t blue  = 0;

    size_t cnt = m_xRoadsList.size();
    if ( cnt > 0 )
    {
      for ( size_t i = 0; i < cnt; ++i )
      {
        Color rC( m_xRoadsList[i].GetRoad()->GetMaskColor() );
        red   += rC.GetRed();
        green += rC.GetGreen();
        blue  += rC.GetBlue();
      }
      red   /= cnt;
      green /= cnt;
      blue  /= cnt;
    }

    HBRUSH oldBrush = (HBRUSH)SelectObject( dc, GetStockObject( DC_BRUSH ) );
    Color c( red * 1000000 + green * 1000 + blue );
    COLORREF color = RGB( c.GetRed(), c.GetGreen(), c.GetBlue() );
    SetDCBrushColor( dc, color );
    HPEN oldPen = (HPEN)SelectObject( dc, GetStockObject( DC_PEN ) );
    SetDCPenColor( dc, color );
    SetROP2( dc, R2_COPYPEN );

    m_contour.PaintOnMask( dc, bmp, mapRect, x, y, maskSize );

    SelectObject( dc, oldBrush );
    SelectObject( dc, oldPen );
  }

//-----------------------------------------------------------------------------

  size_t Crossroad::LevelsCount() const
  {
    vector< int > levels;
    size_t  level;
    bool found;
    for ( size_t i = 0, roadCnt = m_xRoadsList.size(); i < roadCnt; ++i )
    {
      level = m_xRoadsList[i].GetRoad()->GetLevel();
      found = false;
      for ( size_t j = 0, levelCnt = levels.size(); j < levelCnt; ++j )
      {
        if ( levels[j] == level )
        {
          found = true;
          break;
        }
      }
      if ( !found ) { levels.push_back( level ); }
    }

    return (levels.size());
  }

//-----------------------------------------------------------------------------

  void Crossroad::CalculateLevelToBuild()
  {
    // we will build the level having the max number of roads
    vector< int > levels;
    size_t levelsCount = MaxLevel() + 1;

    for ( size_t i = 0; i < levelsCount; ++i )
    {
      levels.push_back( 0 );
    }

    for ( size_t i = 0, cnt = m_xRoadsList.size(); i < cnt; ++i )
    {
      levels[m_xRoadsList[i].GetRoad()->GetLevel()] += 1;
    }

    int max = levels[0];
    m_levelToBuild = 0;

    for ( size_t i = 1; i < levelsCount; ++i )
    {
      if ( levels[i] > max )
      {
        max = levels[i];
        m_levelToBuild = i;
      }
    }
  }

//-----------------------------------------------------------------------------

  void Crossroad::CalculateRadius()
  {
    bool done = false;
    bool modified = false;

    // starting radius
    // minimum possible value
    m_radius = MaxRoadsWidth();        

    do
    {
      ReshapeRoadsInsideCrossroad();
      OrderRoadsByAzimut();
      if ( !modified )
      {
        double minAngle = MinMergingAngle(); // considers only merged roads

// the only test i made with the following line was bad
//				double minAngle = MinAngle();        // considers all roads

        if ( minAngle >= HALF_PI )
        {
          done = true;
        }
        else 
        {
          // updated radius
          // the radius increases when the minimum angle
          // between roads decreases
          m_radius *= (4.0 - minAngle * 6.0 * INV_PI); 
          modified = true;
        }
      }
      else
      {
        done = true;
      }
    }
    while (!done);
  }

//-----------------------------------------------------------------------------

  void Crossroad::CreateBuilders()
  {
    int first = -1;
    int last  = -1;
  
    for ( size_t i = 0, cnt = m_xRoadsList.size(); i < (cnt - 1); ++i )
    {
      if ( m_xRoadsList[i].GetRoad()->GetLevel() == m_levelToBuild )
      {
        if ( first == -1 ) { first = i; }
        for ( size_t j = i + 1, cnt = m_xRoadsList.size(); j < cnt; ++j )
        {
          if ( m_xRoadsList[j].GetRoad()->GetLevel() == m_levelToBuild )
          {
            m_builders.push_back( RoadsTransitionBuilder( m_position, m_radius, &m_xRoadsList[i],
                                                          &m_xRoadsList[j], m_resolution ) );
            last = j;
            break;
          }
        }
      }
    }
    m_builders.push_back( RoadsTransitionBuilder( m_position, m_radius, &m_xRoadsList[last],
                                                  &m_xRoadsList[first], m_resolution ) );
  }

//-----------------------------------------------------------------------------

  void Crossroad::GenerateCrossroadContour()
  {
    for ( size_t i = 0, cnt = m_builders.size(); i < cnt; ++i )
    {
      m_contour.AddVertices( m_builders[i].GetTransitionPoints() );
    }
  }

//-----------------------------------------------------------------------------

  void Crossroad::AdjustRoadsEnds()
  {
    for ( size_t i = 0, roadsCnt = m_xRoadsList.size(); i < roadsCnt; ++i )
    {
      XRoad& curXRoad = m_xRoadsList[i];
      Road*  curRoad  = curXRoad.GetRoad();

      Shapes::DVector v = curXRoad.EndVectorInsideCrossroad();
      double vLen = v.SizeXY();
      v.NormalizeXYInPlace();

      Shapes::DVertex v1;
      if ( curXRoad.GetRoadEndType() == etStarting )
      {
        v1 = curRoad->GetShapeVertex(1);
      }
      else
      {
        v1 = curRoad->GetShapeVertex( curRoad->GetShapeVerticesCount() - 2 );
      }

      Shapes::DVertex newV0;
      if ( curRoad->GetLevel() == m_levelToBuild )
      {
        v *= m_radius;
        newV0 = v.GetSecondEndXY( m_position );
      }
      else
      {
        Shapes::DVertex inters;
        Line line( m_position, v1 );
        if ( !m_contour.IntersectionWithSegment( line, inters, true ) )
        {
          m_contour.IntersectionWithSegment( line, inters, false );
          v *= -(vLen + m_position.DistanceXY( inters ));						
          newV0 = v.GetSecondEndXY( v1 );
        }
        else
        {
          v *= m_position.DistanceXY( inters );
          newV0 = v.GetSecondEndXY( m_position );
        }
      }

      if ( curXRoad.GetRoadEndType() == etStarting )
      {
        curRoad->SetShapeVertex( 0, newV0 );
      }
      else
      {
        curRoad->SetShapeVertex( curRoad->GetShapeVerticesCount() - 1, newV0 );
      }
    }
  }

//-----------------------------------------------------------------------------

  size_t Crossroad::MaxLevel() const
  {
    size_t max = 0;
    size_t level;
    for ( size_t i = 0, roadCnt = m_xRoadsList.size(); i < roadCnt; ++i )
    {
      level = m_xRoadsList[i].GetRoad()->GetLevel();
      if ( level > max )
      {
        max = level;
      }
    }
    return (max);
  }

//-----------------------------------------------------------------------------

  double Crossroad::MaxRoadsWidth() const
  {
    double max = 0.0;
    double width;
    for ( size_t i = 0, cnt = m_xRoadsList.size(); i < cnt; ++i )
    {
      width = m_xRoadsList[i].GetRoad()->GetWidth();
      if ( width > max ) { max = width; }
    }
    return (max);
  }

//-----------------------------------------------------------------------------

  void Crossroad::ReshapeRoadsInsideCrossroad()
  {
    // removes from all the roads the vertices which are inside 
    // the crossroad's circle, but not the first/last 
    for ( size_t i = 0, roadCnt = m_xRoadsList.size(); i < roadCnt; ++i )
    {
      Road* curRoad = m_xRoadsList[i].GetRoad();

      if ( m_xRoadsList[i].GetRoadEndType() == etStarting )
      {
        size_t curVertexIndex = 1; 
        size_t verCnt = curRoad->GetShapeVerticesCount();
        while ( curVertexIndex < verCnt - 1 )
        {
          Shapes::DVertex curVertex = curRoad->GetShapeVertex( curVertexIndex );
          if ( m_position.DistanceXY( curVertex ) <= m_radius )
          {
            curRoad->RemoveShapeVertex( curVertexIndex );
            curVertexIndex--;
            verCnt--;
          }
          else
          {
            break;
          }
          curVertexIndex++;
        }
      }
      else
      {
        size_t curVertexIndex = curRoad->GetShapeVerticesCount() - 2; 
        while ( curVertexIndex > 0 )
        {
          Shapes::DVertex curVertex = curRoad->GetShapeVertex( curVertexIndex );
          if ( m_position.DistanceXY( curVertex ) <= m_radius )
          {
            curRoad->RemoveShapeVertex( curVertexIndex );
          }
          else
          {
            break;
          }
          curVertexIndex--;
        }
      }
    }
  }

//-----------------------------------------------------------------------------

  void Crossroad::OrderRoadsByAzimut()
  {
    sort( m_xRoadsList.begin(), m_xRoadsList.end(), AzimuthLess() );
  }

//-----------------------------------------------------------------------------

  double Crossroad::MinAngle()
  {
    double minAngle = DBL_MAX;
    double totAngle = 0.0;
    for ( size_t i = 0, cnt = m_xRoadsList.size(); i < (cnt - 1); ++i )
    {
      double angle1 = m_xRoadsList[i].AzimuthEndVectorInsideCrossroad();
      double angle2 = m_xRoadsList[i + 1].AzimuthEndVectorInsideCrossroad();
      double curAngle = angle2 - angle1;
      if ( minAngle > curAngle ) { minAngle = curAngle; }
      totAngle += curAngle;
    }
    double curAngle = TWO_PI - totAngle;
    if ( minAngle > curAngle ) { minAngle = curAngle; }

    return (minAngle);
  }

//-----------------------------------------------------------------------------

  double Crossroad::MinMergingAngle() 
  {
    double minAngle = DBL_MAX;
    double totAngle = 0.0;
    for ( size_t i = 0, cnt = m_xRoadsList.size(); i < (cnt - 1); ++i )
    {
      if ( m_xRoadsList[i].GetRoad()->GetLevel() == m_levelToBuild )
      {
        double angle1 = m_xRoadsList[i].AzimuthEndVectorInsideCrossroad();
        for ( size_t j = i + 1, cnt = m_xRoadsList.size(); j < cnt; ++j )
        {
          if ( m_xRoadsList[j].GetRoad()->GetLevel() == m_levelToBuild )
          {
            double angle2 = m_xRoadsList[j].AzimuthEndVectorInsideCrossroad();
            double curAngle = angle2 - angle1;
            if ( minAngle > curAngle ) { minAngle = curAngle; }
            totAngle += curAngle;
            break;
          }
        }
      }
    }
    double curAngle = TWO_PI - totAngle;
    if ( minAngle > curAngle ) { minAngle = curAngle; }

    return (minAngle);
  }

//-----------------------------------------------------------------------------

  size_t Crossroad::MergedRoadsCount() const
  {
    size_t counter = 0;
    for ( size_t i = 0, cnt = m_xRoadsList.size(); i < cnt; ++i )
    {
      if ( m_xRoadsList[i].GetRoad()->GetLevel() == m_levelToBuild )
      {
        counter++;
      }
    }
    return (counter);
  }

//-----------------------------------------------------------------------------

  template <>
  int TList< Crossroad >::Find( const Crossroad& crossroad ) const
  {
    if ( m_list.size() == 0 ) { return (-1); }

    for ( size_t i = 0, cnt = m_list.size(); i < cnt; ++i )
    {
      if ( abs( m_list[i].GetPosition().x - crossroad.GetPosition().x ) < EPSILON )
      {
        if ( abs( m_list[i].GetPosition().y - crossroad.GetPosition().y ) < EPSILON )
        {
          return (i);
        }
      }
    }

    return (-1);
  }

//-----------------------------------------------------------------------------

} // namespace RG_Help