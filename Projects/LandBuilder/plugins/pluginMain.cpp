#include "stdafx.h"
#include "IPlugin.h"
#include "../LandBuilder2/ModuleRegistrator.h"
#include "pluginMain.h"

using namespace LandBuilder2;

IProgressLogSupport* ProgressLog::logSupport = 0;

#ifdef _DEBUG
#define PlugInit LandBuilderPlugInit_Debug
#else
#define PlugInit LandBuilderPlugInit
#endif

class ExpProxy : public ExceptReg::ExceptionRegister
{
    ExceptReg::ExceptionRegister* foreign;

public:
    ExpProxy() 
	: foreign(0) 
    {
    }

    void SetForeignPtr(ExceptReg::ExceptionRegister* other) 
    {
        foreign = other;
    }

    virtual int Catch(const ExceptReg::IException& exception)
    {
        if (foreign) return foreign->Catch(exception);
        else return CatchNext(exception);
    }
};

static ExpProxy expProxy;

PluginMainClass* PluginMainClass::_currentInstance;

PluginMainClass::PluginMainClass() 
{
    _currentInstance = this;
}

ModuleRegistrator* PluginMainClass::GetModules() 
{
    return ModuleRegistrator::GetInstance();
}

PluginMainClass::~PluginMainClass()
{
    _currentInstance = 0;
}

void PluginMainClass::Release() 
{
}

void PluginMainClass::SetCWD(const char* cwd)
{
    Pathname tmp;
    tmp.SetDirectory(cwd);
    Pathname::SetCWD(tmp);
};

void PluginMainClass::InitPlugin(LBGlobalPointers* globalPointers)
{
    _globalPointers = globalPointers;
    ProgressLog::logSupport = _globalPointers->logObject;
    ProgressBarFunctions::SelectGlobalProgressBarHandler(_globalPointers->progressBarHandler);
    expProxy.SetForeignPtr(_globalPointers->exceptionHandler);
}

extern "C" 
{
    _declspec(dllexport) IPlugin* PlugInit(LBGlobalPointers* globPointers)
    {
        if (PluginMainClass::_currentInstance == 0)
        {
            class HotInstance : public PluginMainClass
            {
            public:
                HotInstance(LBGlobalPointers* globalPointers) 
				{
                    InitPlugin(globalPointers);
                }
                
				void Release() 
				{
					delete this;
				}
            };
            return new HotInstance(globPointers);
        }
        else
        {
            PluginMainClass::_currentInstance->InitPlugin(globPointers);
            return PluginMainClass::_currentInstance;
        }
    } 
}