//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <windows.h>

#include "..\..\..\projects\ObjektivLib\LODObject.h"

#include "..\..\LandBuilder2\ILandBuildCmds.h"

#include "..\..\Shapes\Vertex.h"
#include "..\..\CG\CG_Polygon2.h"

//-----------------------------------------------------------------------------

using namespace ObjektivLib;

//-----------------------------------------------------------------------------

using std::vector;
using std::string;
using std::ofstream;
using std::endl;

//-----------------------------------------------------------------------------

using LandBuildInt::IMainCommands;

//-----------------------------------------------------------------------------

namespace BC_Help
{
//-----------------------------------------------------------------------------

	static const double PI      = 4.0 * atan(1.0);
	static const double INV_PI  = 1.0 / PI;
	static const double TWO_PI  = 2.0 * PI;
	static const double HALF_PI = PI / 2.0;
	static const double EPSILON = 1e-08;

//-----------------------------------------------------------------------------

	class Dxf2006Writer
	{
		ofstream m_File;

	public:

		Dxf2006Writer();
		~Dxf2006Writer();

		void Set( const string& filename );

		void WritePolyline( const vector< Shapes::DVertex >& polyline,
                        const string& layer, bool close, 
                        double startingWidth = 0.0, 
                        double endingWidth = 0.0 );

	private:

		void WriteEntitiesHead();
		void WriteEntitiesTail();
		void WriteFileTail();
	};

//-----------------------------------------------------------------------------

	class Line
	{
		const Shapes::DVertex& m_V1;
		const Shapes::DVertex& m_V2;

		double m_A;
		double m_B;
		double m_C;

	public:
		Line( const Shapes::DVertex& v1, const Shapes::DVertex& v2 );

		// calculates the intersection point between this line and the given one
		// the intersection point, if any, will be returned in the intPoint variable
		// returns false if the two lines are parallel
		bool Intersection( const Line& other, Shapes::DVertex& intPoint ) const;

	private:
		void CalculateCoefficients();
	};

//-----------------------------------------------------------------------------

	class Triangle
	{
		Shapes::DVertex m_vertices[3];

	public:
		Triangle( const Shapes::DVertex& v1, const Shapes::DVertex& v2,
				      const Shapes::DVertex& v3 );

		double SignedArea();
	};

//-----------------------------------------------------------------------------

	class BoundingBox
	{
		double m_Width;
		double m_Height;
		double m_Orientation;
		int    m_BaseSegmentIndex;

	public:
		BoundingBox();

		double GetWidth() const;
		double GetHeight() const;
		double GetOrientation() const;
		int    GetBaseSegmentIndex() const;

		void Reset();

		void CalculateFromVertices(const vector<Shapes::DVertex>& vertices);
	};

//-----------------------------------------------------------------------------

	class Footprint
	{
		vector< Shapes::DVertex > m_vertices;

		BoundingBox      m_boundingBox;
		vector< double > m_turningFunction;
		string           m_turningSequence;
		double           m_delVertexTol;
		double           m_rectCornerTol;

	public:
		Footprint();
		Footprint( const vector< Shapes::DVertex >& vertices, double delVertexTol, 
				   double rectCornerTol, bool rectify );

		const vector< Shapes::DVertex >& GetVertices() const;
		vector< Shapes::DVertex >& GetVertices();

		Shapes::DVertex& GetVertex( size_t index );
		const Shapes::DVertex& GetVertex( size_t index ) const;

		Shapes::DVertex& operator [] ( size_t index );
		const Shapes::DVertex& operator [] ( size_t index ) const;

		size_t           GetVerticesCount() const;
		vector< double > GetTurningFunction();
		string           GetTurningSequence();

		// return the angle of rotation
		double GetMinBoundingBoxOrientation();

		Shapes::DVertex GetBarycenter() const;

		Shapes::DVertex GetMinPoint() const;

		double GetMinBoundingBoxWidth();
		double GetMinBoundingBoxHeight();

		// resets bounding box, to be called when modifying vertex using operator []
		void ResetBoundingBox();

		double GetArea() const;

		void SlideVertices( size_t count );

		// returns the index of the longest edge
		size_t LongestEdgeIndex() const;

	protected:
		void   RemoveDuplicatedPoints();
		void   SetVerticesOrderToCCW();
		void   DeleteMidSegmentVertices();
		void   RectifyCorners();
		double SignedArea() const;
		void   ReverseVerticesOrder();
		void   SetLongestEdgeAsFirst();
		void   CalculateTurningFunction();
		void   CalculateTurningSequence();
		void   RotateAboutVertex( const Shapes::DVertex& v, double angle );
	};

//-----------------------------------------------------------------------------

	// Returns the internal angle in the given vertex of the given polygon
	double InternalAngleAt(const vector<Shapes::DVertex>& polygon, unsigned int index, bool left = true);

//-----------------------------------------------------------------------------

	// Calculates the offsetted polygon of the given polygon
	// Returns false if there where problems as vertices reduction or non valid angles
	// Returns true if successful.
	// The polygon will be modified, so pass to this function a copy if you want
	// to preserve the original
	bool Offset( vector< Shapes::DVertex >& polygon, double offset, bool left = true );

//-----------------------------------------------------------------------------

	class Texture
	{
		RString m_pathname;
		RString m_filename;
		size_t  m_size;

	public:
		Texture( RString pathname, RString filename, size_t size );
		~Texture();

		RString GetPathname() const;
		RString GetFilename() const;
		size_t  GetSize() const;

		bool Equals( const Texture& other ) const;
	};

//-----------------------------------------------------------------------------

	class BuildingModelP3D
	{
    typedef CG_Point2< double >   CGPoint2;
    typedef CG_Polygon2< double > CGPolygon2;

	protected:
		int PREDEF_PROC_TEXT_COUNT;
		vector< string > m_procTextures;
		string           m_baseProcTexture;
		string           m_roofProcTexture;
		string           m_interiorProcTexture;

		RString          m_filename;

		mutable Footprint m_footprint;
		size_t            m_numOfFloors;
		double            m_floorHeight;
		size_t            m_roofType;

		Shapes::DVertex   m_position;
		double            m_orientation;

		RString           m_modelPath;
		RString           m_texturesPath;
		vector< Texture > m_textures;

		vector< CGPolygon2 > m_partitions;

		double m_roofRed;
		double m_roofGreen;
		double m_roofBlue;

	public:
		BuildingModelP3D();

		BuildingModelP3D( RString filename, const Footprint& footprint, 
                      size_t numOfFloors, double floorHeight,
                      size_t foorType, RString modelPath, 
                      RString texturesPath, const vector< Texture >& textures,
                      double roofRed, double roofGreen, double roofBlue );

		BuildingModelP3D( const BuildingModelP3D& other );

		void Create( RString filename, const Footprint& footprint,
                 size_t numOfFloors, double floorHeight,
                 size_t foorType, RString modelPath, 
                 RString texturesPath, const vector< Texture >& textures,
                 double roofRed, double roofGreen, double roofBlue );

		Shapes::DVertex GetPosition() const;
		double          GetOrientation() const;

		BuildingModelP3D& operator = ( const BuildingModelP3D& other );

		void ExportToP3DFile();

		const vector< CGPolygon2 >& GetPartitions() const;

	protected:
		void GenerateLOD( ObjectData& obj, bool textureLOD = false, bool genProceduralTexture = false );
		void GenerateGeometryLOD( ObjectData* obj );
		void GenerateViewGeometryLOD( ObjectData* obj );
		void GenerateFireGeometryLOD( ObjectData* obj );
		void GenerateRoadwayLOD( ObjectData& obj );
		void GenerateShadowLOD( ObjectData& obj );
		void GenerateMapping( ObjectData& obj );
		void GenerateComponents( ObjectData& obj );
		void GenerateComponentsLOD( ObjectData* obj );

		void SetOrigin();
		void OrientateAsMinBoundingBox();
		void CorrectOrigin();

	public:
		Footprint&       GetFootprint();
		const Footprint& GetFootprint() const;

		size_t GetNumOfFloors() const;
		double GetFloorHeight() const;
		size_t GetRoofType() const;
		const vector< Texture >& GetTextures() const;

		RString GetFilename() const;
	};

//-----------------------------------------------------------------------------

	class Building
	{
		Footprint m_footprint;
		size_t    m_numOfFloors;
		size_t    m_maxNumFloors;
		double    m_floorHeight;
		size_t    m_roofType;

		Shapes::DVertex  m_position;
		double           m_orientation;

		vector< double > m_fpTurningFunction;
		string           m_fpTurningSequence;

		BuildingModelP3D m_modelP3D;

		double m_roofRed;
		double m_roofGreen;
		double m_roofBlue;

	public:
		Building();

		Building( const Footprint& footprint, size_t numOfFloors,
              size_t maxNumFloors, double floorHeight, size_t roofType,
              double roofRed, double roofGreen, double roofBlue );

		Footprint&       GetFootprint();
		const Footprint& GetFootprint() const;

		Shapes::DVertex GetPosition() const;
		double          GetOrientation() const;

		const BuildingModelP3D& GetModelP3D() const;

		void GenerateP3DModel( IMainCommands* cmdIfc, RString modelsPath,
                           RString texturesPath, const vector< Texture >& textures,
                           vector< BuildingModelP3D >& modelsLibrary );

	private:
		int GetModelLibIndex( const vector< Texture >& textures, const vector< BuildingModelP3D >& modelsLibrary );
	};

//-----------------------------------------------------------------------------

} // namespace BC_Help