//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\BuildingCreatorHelpers.h"

#include "..\..\Shapes\ShapeHelpers.h"

#include <projects/ObjektivLib/ObjToolTopology.h>

//-----------------------------------------------------------------------------

namespace BC_Help
{
//-----------------------------------------------------------------------------

  Dxf2006Writer::Dxf2006Writer()
  {
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::Set( const string& filename )
  {
    m_File.open( filename.c_str(), std::ios::out | std::ios::trunc );
    if ( !m_File.fail() ) 
    {
      WriteEntitiesHead();
    }
    else
    {
      // TODO: some error message
    }
  }

//-----------------------------------------------------------------------------

  Dxf2006Writer::~Dxf2006Writer()
  {
    if ( !m_File.fail() ) 
    {
      WriteEntitiesTail();
      WriteFileTail();
      m_File.close();
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WriteEntitiesHead()
  {
    if ( !m_File.fail() ) 
    {
      string out = "  0\nSECTION\n  2\nENTITIES";
      m_File << out << endl;
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WriteEntitiesTail()
  {
    if ( !m_File.fail() ) 
    {
      string out = "  0\nENDSEC";
      m_File << out << endl;
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WriteFileTail()
  {
    if ( !m_File.fail() ) 
    {
      string out = "  0\nEOF";
      m_File << out << endl;
    }
  }

//-----------------------------------------------------------------------------

  void Dxf2006Writer::WritePolyline( const vector<Shapes::DVertex>& polyline,
                                     const string& layer, bool close, 
                                     double startingWidth, 
                                     double endingWidth )
  {
    if ( !m_File.fail() ) 
    {
      size_t size = polyline.size();

      if ( size > 1 )
      {
        char buffer[80];
        string out = "  0\nPOLYLINE\n100\nAcDbEntity\n  8\n";
        out += (layer + "\n 66\n     1\n100\nAcDb2dPolyline\n 10\n0.0\n 20\n0.0\n 30\n0.0\n");
        if ( close )
        {
          out += " 70\n1\n";
        }
        else
        {
          out += " 70\n0\n";
        }
        _gcvt_s(buffer, 80, startingWidth, 8);
        out += (" 40\n" + string(buffer) +"\n");
        _gcvt_s(buffer, 80, endingWidth, 8);
        out += (" 41\n" + string(buffer) +"\n");
        for (unsigned int i = 0; i < size; ++i)
        {
          out += "  0\nVERTEX\n100\nAcDbVertex\n  8\n";
          out += (layer + "\n100\nAcDb2dVertex\n 10\n");
          _gcvt_s(buffer, 80, polyline[i].x, 8);
          out += (string(buffer) + "\n 20\n");
          _gcvt_s(buffer, 80, polyline[i].y, 8);
          out += (string(buffer) + "\n 30\n0.0\n");
        }
        out += "  0\nSEQEND\n  8\n";
        out += layer;
        m_File << out << endl;
      }
    }
  }

//-----------------------------------------------------------------------------

  Line::Line( const Shapes::DVertex& v1, const Shapes::DVertex& v2 )
  : m_V1( v1 )
  , m_V2( v2 )
  {
    CalculateCoefficients();
  }

//-----------------------------------------------------------------------------

  bool Line::Intersection( const Line& other, Shapes::DVertex& intPoint ) const
  {
    double den = m_B * other.m_A - m_A * other.m_B;
    if (abs(den) < EPSILON) 
    {
      intPoint.x = DBL_MAX;
      intPoint.y = DBL_MAX;
      return false;
    }

    double numX = m_C * other.m_B - m_B * other.m_C;
    double numY = m_A * other.m_C - m_C * other.m_A;

    intPoint.x = numX / den;
    intPoint.y = numY / den;

    return true;
  }

//-----------------------------------------------------------------------------

  void Line::CalculateCoefficients()
  {
    double dx = m_V2.x - m_V1.x;
    double dy = m_V2.y - m_V1.y;

    if (abs(dx) < EPSILON)
    {
      if (abs(dy) < EPSILON)
      {
        m_A = 0.0;
        m_B = 0.0;
        m_C = 0.0;
      }
      else
      {
        m_A = 1.0;
        m_B = 0.0;
        m_C = -m_V1.x;
      }
    }
    else if (abs(dy) < EPSILON)
    {
      m_A = 0.0;
      m_B = 1.0;
      m_C = -m_V1.y;
    }
    else
    {
      m_A = 1.0 / dx;
      m_B = -1.0 / dy;
      m_C = - m_V1.x / dx + m_V1.y / dy;
    }
  }

//-----------------------------------------------------------------------------

  Triangle::Triangle( const Shapes::DVertex& v1, const Shapes::DVertex& v2,
            const Shapes::DVertex& v3 )
  {
    m_vertices[0] = v1;
    m_vertices[1] = v2;
    m_vertices[2] = v3;
  }

//-----------------------------------------------------------------------------

  double Triangle::SignedArea()
  {
    return (( m_vertices[0].x * m_vertices[1].y - m_vertices[0].y * m_vertices[1].x +
          m_vertices[1].x * m_vertices[2].y - m_vertices[1].y * m_vertices[2].x +
          m_vertices[2].x * m_vertices[0].y - m_vertices[2].y * m_vertices[0].x ) / 2.0 );
  }

//-----------------------------------------------------------------------------

  BoundingBox::BoundingBox()
  : m_Width(DBL_MAX)
  , m_Height(DBL_MAX)
  , m_Orientation(0.0)
  , m_BaseSegmentIndex(-1)
  {
  }

//-----------------------------------------------------------------------------

  double BoundingBox::GetWidth() const
  {
    return m_Width;
  }
  
//-----------------------------------------------------------------------------

  double BoundingBox::GetHeight() const
  {
    return m_Height;
  }

//-----------------------------------------------------------------------------

  double BoundingBox::GetOrientation() const
  {
    return m_Orientation;
  }

//-----------------------------------------------------------------------------

  int BoundingBox::GetBaseSegmentIndex() const
  {
    return m_BaseSegmentIndex;
  }

//-----------------------------------------------------------------------------

  void BoundingBox::Reset()
  {
    m_Width            = DBL_MAX;
    m_Height           = DBL_MAX;
    m_Orientation      = 0.0;
    m_BaseSegmentIndex = -1;
  }

//-----------------------------------------------------------------------------

  void BoundingBox::CalculateFromVertices( const vector< Shapes::DVertex >& vertices )
  {
    size_t cnt = vertices.size();
    double minArea = DBL_MAX;

    Shapes::DVertex p0 = vertices[0];

    for ( size_t i = 0, j = (cnt - 1); i < cnt; j = i++ )
    {
      const Shapes::DVertex& pJ = vertices[j];
      const Shapes::DVertex& pI = vertices[i];

      // the following algorithm works only for convex polygons, so we
      // apply it only to segments whose endpoints are both convex vertices
      // it is assumed that the vertices are already in CCW order
      // ------------------------------------------------------------------
      const Shapes::DVertex& currI = vertices[i];
      Shapes::DVertex prevI;
      Shapes::DVertex nextI;
      if ( i == 0 )
      {
        prevI = vertices[cnt - 1];
      }
      else
      {
        prevI = vertices[i - 1];
      }
      if ( i == cnt - 1 )
      {
        nextI = vertices[0];
      }
      else
      {
        nextI = vertices[i + 1];
      }
      
      Triangle triI( prevI, currI, nextI );
      if ( triI.SignedArea() < 0.0 ) { continue; }

      const Shapes::DVertex& currJ = vertices[j];
      Shapes::DVertex prevJ;
      Shapes::DVertex nextJ;
      if ( j == 0 )
      {
        prevJ = vertices[cnt - 1];
      }
      else
      {
        prevJ = vertices[j - 1];
      }
      if ( j == cnt - 1 )
      {
        nextJ = vertices[0];
      }
      else
      {
        nextJ = vertices[j + 1];
      }

      Triangle triJ( prevJ, currJ, nextJ );
      if ( triJ.SignedArea() < 0.0 ) { continue; }

      // ------------------------------------------------------------------

      Shapes::DVector vJI( pI, pJ );
      vJI.NormalizeXYInPlace();

      Shapes::DVector vPerp = vJI.PerpXYCCW();

      double w0 = 0.0;
      double w1 = 0.0;
      double h0 = 0.0;
      double h1 = 0.0;

      for ( size_t k = 1; k < cnt; ++k )
      {
        const Shapes::DVertex& pK = vertices[k];
        Shapes::DVector vK0( pK, p0 );

        double testW = vJI.DotXY( vK0 );
        if ( testW < w0 )
        {
          w0 = testW;
        }
        else if ( testW > w1 )
        {
          w1 = testW;
        }

        double testH = vPerp.DotXY( vK0 );
        if ( testH < h0 )
        {
          h0 = testH;
        }
        else if ( testH > h1 )
        {
          h1 = testH;
        }
      }

      double tempWidth  = w1 - w0;
      double tempHeight = h1 - h0; 
      double area = tempWidth * tempHeight;
      if ( area < minArea )
      {
        minArea            = area;
        m_Width            = tempWidth;
        m_Height           = tempHeight;
        m_Orientation      = vJI.DirectionXY(); 
        m_BaseSegmentIndex = j;
      }		
    }

    if ( m_Width < m_Height ) 
    { 
      m_Orientation += HALF_PI;
      if ( m_Orientation > TWO_PI ) { m_Orientation -= TWO_PI; }

      double temp = m_Width; 
      m_Width  = m_Height;
      m_Height = temp;
    }
  }

//-----------------------------------------------------------------------------

  Footprint::Footprint()
  : m_turningSequence( "" )
  {
  }

//-----------------------------------------------------------------------------

  Footprint::Footprint( const vector< Shapes::DVertex >& vertices, 
              double delVertexTol, double rectCornerTol, bool rectify )
  : m_vertices( vertices )
  , m_turningSequence( "" )
  , m_delVertexTol( delVertexTol / 180 * PI )
  , m_rectCornerTol( rectCornerTol / 180 * PI )
  {
    RemoveDuplicatedPoints();
    SetVerticesOrderToCCW();
    SetLongestEdgeAsFirst();
    DeleteMidSegmentVertices();
    if ( rectify )
    {
      RectifyCorners();
    }
  }

//-----------------------------------------------------------------------------

  const vector< Shapes::DVertex >& Footprint::GetVertices() const
  {
    return (m_vertices);
  }

//-----------------------------------------------------------------------------

  vector< Shapes::DVertex >& Footprint::GetVertices()
  {
    return (m_vertices);
  }

//-----------------------------------------------------------------------------

  Shapes::DVertex& Footprint::GetVertex( size_t index )
  {
    assert( index < m_vertices.size() );
    return (m_vertices[index]);
  }

//-----------------------------------------------------------------------------

  const Shapes::DVertex& Footprint::GetVertex( size_t index ) const
  {
    assert( index < m_vertices.size() );
    return (m_vertices[index]);
  }

//-----------------------------------------------------------------------------

  Shapes::DVertex& Footprint::operator [] ( size_t index )
  {
    assert( index < m_vertices.size() );
    return (m_vertices[index]);
  }

//-----------------------------------------------------------------------------

  const Shapes::DVertex& Footprint::operator [] ( size_t index ) const
  {
    assert( index < m_vertices.size() );
    return (m_vertices[index]);
  }

//-----------------------------------------------------------------------------

  size_t Footprint::GetVerticesCount() const
  {
    return (m_vertices.size());
  }

//-----------------------------------------------------------------------------

  vector< double > Footprint::GetTurningFunction()
  {
    if ( m_turningFunction.size() == 0 )
    {
      CalculateTurningFunction();
    }

    return (m_turningFunction);
  }

//-----------------------------------------------------------------------------

  string Footprint::GetTurningSequence()
  {
    if ( m_turningSequence == "" )
    {
      CalculateTurningSequence();
    }

    return (m_turningSequence);
  }

//-----------------------------------------------------------------------------

  void Footprint::RemoveDuplicatedPoints()
  {
    double proximityTol = 0.5; // half meter

    vector< size_t > toBeDeleted;

    size_t cnt = m_vertices.size();
    for ( size_t i = 0, j = (cnt - 1); i < cnt; j = i++)
    {
      const Shapes::DVertex& prevV = m_vertices[j];
      const Shapes::DVertex& curV  = m_vertices[i];

      if ( prevV.DistanceXY( curV ) < proximityTol )
      {
        toBeDeleted.push_back(i);
      }
    }

    if ( toBeDeleted.size() > 0 )
    {
      for ( int i = toBeDeleted.size() - 1; i >= 0; --i )
      {
        m_vertices.erase( m_vertices.begin() + toBeDeleted[i] );
      }

      SetLongestEdgeAsFirst();
      m_boundingBox.Reset();
      m_turningFunction.clear();
      m_turningSequence = "";
    }
  }

//-----------------------------------------------------------------------------

  void Footprint::SetVerticesOrderToCCW()
  {
    if ( SignedArea() < 0.0 )
    {
      ReverseVerticesOrder();
    }
  }

//-----------------------------------------------------------------------------

  void Footprint::DeleteMidSegmentVertices()
  {
    if ( m_turningFunction.size() == 0 )
    {
      CalculateTurningFunction();
    }

    vector< size_t > toBeDeleted;

    double diff; 
    for ( size_t i = 0, cnt = m_vertices.size(); i < cnt; ++i )
    {
      if ( i == 0 )
      {
        diff = m_turningFunction[i] - m_turningFunction[cnt - 1]; 
      }
      else
      {
        diff = m_turningFunction[i] - m_turningFunction[i - 1]; 
      }

      if ( abs( diff ) < m_delVertexTol )
      {
        toBeDeleted.push_back(i);
      }
    }

    if ( toBeDeleted.size() > 0 )
    {
      for ( int i = toBeDeleted.size() - 1; i >= 0; --i )
      {
        m_vertices.erase( m_vertices.begin() + toBeDeleted[i] );
      }

      SetLongestEdgeAsFirst();
      m_boundingBox.Reset();
      m_turningFunction.clear();
      m_turningSequence = "";
    }
  }

//-----------------------------------------------------------------------------

  void Footprint::RectifyCorners()
  {
    size_t cnt = m_vertices.size();

    // we cannot rectify a triangle
    if ( cnt < 4 ) { return; }

    double min90  = HALF_PI - m_rectCornerTol;
    double max90  = HALF_PI + m_rectCornerTol;
    double min270 = 3.0 * HALF_PI - m_rectCornerTol;
    double max270 = 3.0 * HALF_PI + m_rectCornerTol;

    size_t iPrev;
    size_t iCur;
    size_t iNext;
    size_t iNextNext;

    bool modified = false;

    for ( size_t i = 0; i < cnt; ++i )
    {
      if ( i == 0 )
      {
        iPrev     = cnt - 1;
        iCur      = i;
        iNext     = i + 1;
        iNextNext = i + 2;
      }
      else if ( i == (cnt - 1) )
      {
        iPrev     = i - 1;
        iCur      = i;
        iNext     = 0;
        iNextNext = 1;
      }
      else if ( i == (cnt - 2) )
      {
        iPrev     = i - 1;
        iCur      = i;
        iNext     = i + 1;
        iNextNext = 0;
      }
      else
      {
        iPrev     = i - 1;
        iCur      = i;
        iNext     = i + 1;
        iNextNext = i + 2;
      }

      Shapes::DVertex vPrev( m_vertices[iPrev] );
      Shapes::DVertex vCur( m_vertices[iCur] );
      Shapes::DVertex vNext( m_vertices[iNext] );
      Shapes::DVertex vNextNext( m_vertices[iNextNext] );

      Shapes::DVector vecPrev = Shapes::DVector( vCur, vPrev );
      Shapes::DVector vecCur  = Shapes::DVector( vNext, vCur );

      double dirP = vecPrev.DirectionXY();
      double dirC = vecCur.DirectionXY();

      double diffCP = dirC - dirP;

      if ( diffCP < 0.0 ) { diffCP += TWO_PI; }

      double diff = 0.0;

      if ( min90 < diffCP && diffCP < max90 )
      {
        diff = diffCP - HALF_PI;
      }
      else if ( min270 < diffCP && diffCP < max270 )
      {
        diff = diffCP - 3.0 * HALF_PI;
      }

      if ( diff != 0.0 )
      {
        Shapes::DVertex midPoint(0.5 * (vCur.x + vNext.x), 0.5 * (vCur.y + vNext.y));
        vecCur.RotateXY( -diff );

        Line linePrev( vPrev, vCur );
        Line lineCur( midPoint, vecCur.GetSecondEndXY( midPoint ) );
        Line lineNext( vNext, vNextNext );

        Shapes::DVertex newCur;
        Shapes::DVertex newNext;

        if ( lineCur.Intersection( linePrev, newCur ) )
        {
          if ( lineCur.Intersection( lineNext, newNext ) )
          {
            if ( newCur.DistanceXY( vCur ) < newCur.DistanceXY( vPrev ) )
            {
              if ( newNext.DistanceXY( vNext ) < newNext.DistanceXY( vNextNext ) )
              {
                m_vertices[iCur]  = newCur;
                m_vertices[iNext] = newNext;
                modified = true;
              }
            }
          }
        }
      }
    }

    if ( modified )
    {
      m_boundingBox.Reset();
      m_turningFunction.clear();
      m_turningSequence = "";
    }
  }

//-----------------------------------------------------------------------------

  double Footprint::GetMinBoundingBoxOrientation()
  {
    if ( m_boundingBox.GetWidth() == DBL_MAX )
    {
      m_boundingBox.CalculateFromVertices( m_vertices );
    }
    return (m_boundingBox.GetOrientation());
  }

//-----------------------------------------------------------------------------

  void Footprint::ResetBoundingBox()
  {
    m_boundingBox.Reset();
  }

//-----------------------------------------------------------------------------

  double Footprint::GetArea() const
  {
    return (abs( SignedArea() ));
  }

//-----------------------------------------------------------------------------

  size_t Footprint::LongestEdgeIndex() const
  {
    double maxLen = 0.0;
    size_t index  = 0;

    size_t cnt = m_vertices.size();

    for ( size_t i = 0, j = (cnt - 1); i < cnt; j = i++ )
    {
      Shapes::DVertex v1 = m_vertices[j];
      Shapes::DVertex v2 = m_vertices[i];

      double len = v1.DistanceXY( v2 );
      if ( len > maxLen )
      {
        maxLen = len;
        index = j;
      }
    }

    return (index);
  }

//-----------------------------------------------------------------------------

  void Footprint::SlideVertices( size_t count )
  {
    if ( count > 0 )
    {
      // TODO: we can find a much faster algorithm !! :P
      for ( size_t i = 0; i < count; ++i )
      {
        Shapes::DVertex temp = m_vertices[0];
        for ( size_t v = 0, cntV = m_vertices.size(); v < cntV - 1; ++v )
        {
          m_vertices[v] = m_vertices[v + 1];
        }
        m_vertices[m_vertices.size() - 1] = temp;
      }

      m_boundingBox.Reset();
      m_turningFunction.clear();
      m_turningSequence = "";
    }
  }

//-----------------------------------------------------------------------------

  double Footprint::SignedArea() const
  {
    double area = 0.0;
    size_t cnt = m_vertices.size();
    // calculates area (gauss algorithm)
    for ( size_t i = 0, j = (cnt - 1); i < cnt; j = i++ )
    {
      Shapes::DVertex v1 = m_vertices[j];
      Shapes::DVertex v2 = m_vertices[i];

      area += v1.x * v2.y - v1.y * v2.x;
    }
    return (0.5 * area);
  }

//-----------------------------------------------------------------------------

  void Footprint::ReverseVerticesOrder()
  {
    vector< Shapes::DVertex > tempList;

    int size = m_vertices.size();
    for ( int i = size - 1; i >= 0; --i )
    {
      tempList.push_back( m_vertices[i] );
    }

    m_vertices = tempList;
    m_boundingBox.Reset();
    m_turningFunction.clear();
    m_turningSequence = "";
  }

//-----------------------------------------------------------------------------

  void Footprint::SetLongestEdgeAsFirst()
  {
    size_t index = LongestEdgeIndex();
    SlideVertices( index );
  }

//-----------------------------------------------------------------------------

  void Footprint::CalculateTurningFunction()
  {
    m_turningFunction.clear();

    if ( m_vertices.size() == 0) { return; }

    size_t cnt = m_vertices.size();

    for ( size_t i = 0; i < (cnt - 1); ++i )
    {
      const Shapes::DVertex& v0 = m_vertices[i];
      const Shapes::DVertex& v1 = m_vertices[i + 1];
      Shapes::DVector v( v1, v0 );
      m_turningFunction.push_back( v.DirectionXY() );
    }
    const Shapes::DVertex& v0 = m_vertices[cnt - 1];
    const Shapes::DVertex& v1 = m_vertices[0];
    Shapes::DVector v( v1, v0 );
    m_turningFunction.push_back( v.DirectionXY() );
  }

//-----------------------------------------------------------------------------

  void Footprint::CalculateTurningSequence()
  {
    m_turningSequence = "";
    if ( m_vertices.size() < 3 ) { return; }

    for ( size_t i = 0, cnt = m_vertices.size(); i < cnt; ++i )
    {
      Shapes::DVector v0;
      Shapes::DVector v1;

      if ( i == 0 )
      {
        v0 = Shapes::DVector( m_vertices[0], m_vertices[cnt - 1] );
        v1 = Shapes::DVector( m_vertices[1], m_vertices[0] );
      }
      else if ( i == cnt - 1 )
      {
        v0 = Shapes::DVector( m_vertices[cnt - 1], m_vertices[cnt - 2] );
        v1 = Shapes::DVector( m_vertices[0], m_vertices[cnt - 1] );
      }
      else
      {
        v0 = Shapes::DVector( m_vertices[i], m_vertices[i - 1] );
        v1 = Shapes::DVector( m_vertices[i + 1], m_vertices[i] );
      }

      Shapes::DVector cross = v0.Cross( v1 );

      if ( cross.z == 0.0 )
      {
        m_turningSequence += "S"; // straight
      }
      else if ( cross.z > 0.0 )
      {
        m_turningSequence += "L"; // left turn
      }
      else
      {
        m_turningSequence += "R"; // right turn
      }
    }
  }

//-----------------------------------------------------------------------------

  void Footprint::RotateAboutVertex( const Shapes::DVertex& v, double angle )
  {
    if ( angle != 0.0 )
    {
      double x;
      double y;
      for ( size_t i = 0, cnt = m_vertices.size(); i < cnt; ++i )
      {
        x = m_vertices[i].x - v.x;
        y = m_vertices[i].y - v.y;
        m_vertices[i].x = v.x + x * cos( angle ) + y * sin( angle );
        m_vertices[i].y = v.y - x * sin( angle ) + y * cos( angle );
      }
    }
  }

//-----------------------------------------------------------------------------

  Shapes::DVertex Footprint::GetBarycenter() const
  {
    Shapes::DVertex barycenter( 0.0, 0.0, 0.0 );

    for ( size_t i = 0, cnt = m_vertices.size(); i < cnt; ++i )
    {
      barycenter += m_vertices[i];
    }
    barycenter /= m_vertices.size();
    return (barycenter);
  }

//-----------------------------------------------------------------------------
  
  Shapes::DVertex Footprint::GetMinPoint() const
  {
    Shapes::DVertex minV = m_vertices[0];

    for ( size_t i = 1, cnt = m_vertices.size(); i < cnt; ++i )
    {
      if ( minV.x > m_vertices[i].x )
      {
        minV.x = m_vertices[i].x;
      }
      if ( minV.y > m_vertices[i].y )
      {
        minV.y = m_vertices[i].y;
      }
      if ( minV.z > m_vertices[i].z )
      {
        minV.z = m_vertices[i].z;
      }
    }

    return (minV);
  }

//-----------------------------------------------------------------------------

  double Footprint::GetMinBoundingBoxWidth()
  {
    if ( m_boundingBox.GetWidth() == DBL_MAX )
    {
      m_boundingBox.CalculateFromVertices( m_vertices );
    }
    return (m_boundingBox.GetWidth());
  }

//-----------------------------------------------------------------------------

  double Footprint::GetMinBoundingBoxHeight()
  {
    if ( m_boundingBox.GetWidth() == DBL_MAX )
    {
      m_boundingBox.CalculateFromVertices( m_vertices );
    }
    return (m_boundingBox.GetHeight());
  }

//-----------------------------------------------------------------------------

  double InternalAngleAt(const vector<Shapes::DVertex>& polygon, unsigned int index, bool left)
  {
    unsigned int cnt = polygon.size();

    Shapes::DVector vecPrev;
    Shapes::DVector vecNext;

    if (index == 0)
    {
      vecPrev = Shapes::DVector(polygon[cnt - 1], polygon[0]);
      vecNext = Shapes::DVector(polygon[1], polygon[0]);
    }
    else if (index == cnt - 1)
    {
      vecPrev = Shapes::DVector(polygon[cnt - 2], polygon[cnt - 1]);
      vecNext = Shapes::DVector(polygon[0], polygon[cnt - 1]);
    }
    else
    {
      vecPrev = Shapes::DVector(polygon[index - 1], polygon[index]);
      vecNext = Shapes::DVector(polygon[index + 1], polygon[index]);
    }

    double angle = vecNext.DirectionXY() - vecPrev.DirectionXY();

    if (angle < 0.0) angle += TWO_PI;

    if (left)
    {
      angle = TWO_PI - angle;
    }

    return angle;
  }

//-----------------------------------------------------------------------------

  bool Offset( vector<Shapes::DVertex>& polygon, double offset, bool left )
  {
    unsigned int cnt = polygon.size();

    vector< Shapes::DVertex > newPolygon( polygon );

    for ( unsigned int i = 0; i < cnt; ++i )
    {
      double angle = InternalAngleAt( polygon, i, left );

      if ( angle == 0.0 || angle == TWO_PI )
      {
        polygon.clear();
        return (false);
      }

      // TODO: we miss the check of the offset value to see if it is
      //       valid (if it does not give a point deletion)

      double sin_HalfA = sin( angle / 2.0 );

      Shapes::DVector vecPrev;
      Shapes::DVector vecNext;

      if ( i == 0 )
      {
        vecPrev = Shapes::DVector( polygon[0], polygon[cnt - 1] );
        vecNext = Shapes::DVector( polygon[1], polygon[0] );
      }
      else if (i == cnt - 1)
      {
        vecPrev = Shapes::DVector( polygon[cnt - 1], polygon[cnt - 2] );
        vecNext = Shapes::DVector( polygon[0], polygon[cnt - 1] );
      }
      else
      {
        vecPrev = Shapes::DVector( polygon[i], polygon[i - 1] );
        vecNext = Shapes::DVector( polygon[i + 1], polygon[i] );
      }

      Shapes::DVector vecPerpPrev;
      Shapes::DVector vecPerpNext;

      if ( left )
      {
        vecPerpPrev = vecPrev.PerpXYCCW();
        vecPerpNext = vecNext.PerpXYCCW();
      }
      else
      {
        vecPerpPrev = vecPrev.PerpXYCW();
        vecPerpNext = vecNext.PerpXYCW();
      }

      vecPerpPrev.NormalizeXYInPlace();
      vecPerpNext.NormalizeXYInPlace();
      Shapes::DVector vecMean = (vecPerpPrev + vecPerpNext) / 2.0;
      vecMean.NormalizeXYInPlace();
      vecMean *= (offset * 1.0 / sin_HalfA);

      newPolygon[i] = vecMean.GetSecondEndXY( polygon[i] );
    }

    polygon = newPolygon;

    return (true);
  }

//-----------------------------------------------------------------------------
    
  Texture::Texture( RString pathname, RString filename, size_t size )
  : m_pathname( pathname )
  , m_filename( filename )
  , m_size( size )
  {
  }

//-----------------------------------------------------------------------------

  Texture::~Texture()
  {
  }

//-----------------------------------------------------------------------------

  RString Texture::GetPathname() const
  {
    return (m_pathname);
  }

//-----------------------------------------------------------------------------

  RString Texture::GetFilename() const
  {
    RString filename = m_filename.Substring( m_filename.ReverseFind( '\\' ) + 1, m_filename.GetLength() );
    return (filename);
  }

//-----------------------------------------------------------------------------

  size_t Texture::GetSize() const
  {
    return (m_size);
  }

//-----------------------------------------------------------------------------

  bool Texture::Equals( const Texture& other ) const
  {
    if ( !(m_pathname == other.m_pathname) ) { return (false); }
    if ( !(m_filename == other.m_filename) ) { return (false); }
    if ( !(m_size     == other.m_size ) )    { return (false); }
    return (true);
  }

//-----------------------------------------------------------------------------

  BuildingModelP3D::BuildingModelP3D()
  {
  }

//-----------------------------------------------------------------------------

  BuildingModelP3D::BuildingModelP3D( RString filename, const Footprint& footprint,
                                      size_t numOfFloors, double floorHeight,
                                      size_t roofType, 
                                      RString modelPath, RString texturesPath, 
                                      const vector< Texture >& textures,
                                      double roofRed, double roofGreen, double roofBlue )
  {
    Create( filename, footprint, numOfFloors, floorHeight, roofType,
            modelPath, texturesPath, textures, roofRed, roofGreen, roofBlue );
  }

//-----------------------------------------------------------------------------

  BuildingModelP3D::BuildingModelP3D( const BuildingModelP3D& other )
  : PREDEF_PROC_TEXT_COUNT( other.PREDEF_PROC_TEXT_COUNT)
  , m_procTextures( other.m_procTextures )
  , m_baseProcTexture( other.m_baseProcTexture )
  , m_roofProcTexture( other.m_roofProcTexture )
  , m_interiorProcTexture( other.m_interiorProcTexture )
  , m_filename( other.m_filename )
  , m_footprint( other.m_footprint )
  , m_numOfFloors( other.m_numOfFloors )
  , m_floorHeight( other.m_floorHeight ) 
  , m_roofType( other.m_roofType )
  , m_position( other.m_position )
  , m_orientation( other.m_orientation )
  , m_modelPath( other.m_modelPath )
  , m_texturesPath( other.m_texturesPath )
  , m_textures( other.m_textures )
  , m_partitions( other.m_partitions )
  , m_roofRed( 0.65 )
  , m_roofGreen( 0.39 )
  , m_roofBlue( 0.31 )
  {
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::Create( RString filename, const Footprint& footprint,
                                 size_t numOfFloors, double floorHeight,
                                 size_t roofType, 
                                 RString modelPath, RString texturesPath, 
                                 const vector< Texture >& textures,
                                 double roofRed, double roofGreen, double roofBlue )
  {
    m_filename     = filename;
    m_footprint    = footprint;
    m_numOfFloors  = numOfFloors;
    m_floorHeight  = floorHeight;
    m_roofType     = roofType;
    m_modelPath    = modelPath;
    m_texturesPath = texturesPath;
    m_textures     = textures;
    m_roofRed      = roofRed;
    m_roofGreen    = roofGreen;
    m_roofBlue     = roofBlue;

    PREDEF_PROC_TEXT_COUNT = 6;
    m_procTextures.push_back( "#(argb,8,8,3)color(0.45,0.45,0.45,1.0,CO)" );
    m_procTextures.push_back( "#(argb,8,8,3)color(0.55,0.55,0.55,1.0,CO)" ); 
    m_procTextures.push_back( "#(argb,8,8,3)color(0.62,0.58,0.49,1.0,CO)" );
    m_procTextures.push_back( "#(argb,8,8,3)color(0.87,0.81,0.65,1.0,CO)" ); 
    m_procTextures.push_back( "#(argb,8,8,3)color(0.66,0.60,0.39,1.0,CO)" );
    m_procTextures.push_back( "#(argb,8,8,3)color(0.65,0.69,0.73,1.0,CO)" );
    m_procTextures.push_back( "#(argb,8,8,3)color(0.49,0.45,0.40,1.0,CO)" );
    m_procTextures.push_back( "#(argb,8,8,3)color(0.44,0.30,0.25,1.0,CO)" ); 
    m_procTextures.push_back( "#(argb,8,8,3)color(0.59,0.60,0.74,1.0,CO)" );
    m_procTextures.push_back( "#(argb,8,8,3)color(0.16,0.20,0.56,1.0,CO)" ); 
    m_procTextures.push_back( "#(argb,8,8,3)color(0.38,0.51,0.45,1.0,CO)" );
    m_procTextures.push_back( "#(argb,8,8,3)color(0.39,0.42,0.37,1.0,CO)" );

    m_baseProcTexture = "#(argb,8,8,3)color(1.0,1.0,1.0,1.0,CO)";

    m_interiorProcTexture = "#(argb,8,8,3)color(0.45,0.45,0.45,1.0,CO)";

    char buffer[80];
    sprintf_s( buffer, "#(argb,8,8,3)color(%.2f,%.2f,%.2f,1.0,CO)", m_roofRed, m_roofGreen, m_roofBlue );
    m_roofProcTexture = string( buffer );

    if ( m_numOfFloors < 1 )   { m_numOfFloors = 1; }
    if ( m_floorHeight < 2.0 ) { m_floorHeight = 2.0; }

    SetOrigin();
    OrientateAsMinBoundingBox();
    CorrectOrigin();
  }

//-----------------------------------------------------------------------------

  Shapes::DVertex BuildingModelP3D::GetPosition() const
  {
    return (m_position);
  }

//-----------------------------------------------------------------------------

  double BuildingModelP3D::GetOrientation() const
  {
    return (m_orientation);
  }

//-----------------------------------------------------------------------------

  BuildingModelP3D& BuildingModelP3D::operator = ( const BuildingModelP3D& other )
  {
    PREDEF_PROC_TEXT_COUNT = other.PREDEF_PROC_TEXT_COUNT;
    m_procTextures         = other.m_procTextures;
    m_baseProcTexture      = other.m_baseProcTexture;
    m_roofProcTexture      = other.m_roofProcTexture;
    m_interiorProcTexture  = other.m_interiorProcTexture;

    m_filename             = other.m_filename;
    m_footprint            = other.m_footprint;
    m_numOfFloors          = other.m_numOfFloors;
    m_floorHeight          = other.m_floorHeight;
    m_roofType             = other.m_roofType;

    m_position             = other.m_position;
    m_orientation          = other.m_orientation;

    m_modelPath            = other.m_modelPath;
    m_texturesPath         = other.m_texturesPath;
    m_textures             = other.m_textures;

    m_partitions           = other.m_partitions;

    return (*this);
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::ExportToP3DFile()
  {
    LODObject lodObj;

    ObjectData* obj = new ObjectData;
    if ( m_textures.size() > 0 )
    {
      GenerateLOD( *obj, true );
    }
    else
    {
      GenerateLOD( *obj, true, true );
    }

    lodObj.InsertLevel( obj, 1.0 );

    lodObj.DeleteLevel( 0 );

    obj = new ObjectData;
    GenerateGeometryLOD( obj );
    obj->SetNamedProp( "class", "house" );
    obj->SetNamedProp( "map", "house" );
    obj->SetNamedProp( "dammage", "building" );
    obj->SetNamedProp( "sbsource", "shadowvolume" );
    obj->SetNamedProp( "prefershadowvolume", "0" );
    lodObj.InsertLevel( obj, LOD_GEOMETRY );

    obj = new ObjectData;
    GenerateViewGeometryLOD( obj );
    lodObj.InsertLevel( obj, LOD_VIEWGEO );

    obj = new ObjectData;
    GenerateFireGeometryLOD( obj );
    lodObj.InsertLevel( obj, LOD_FIREGEO );

    obj = new ObjectData;
    GenerateRoadwayLOD(*obj);
    lodObj.InsertLevel(obj, LOD_ROADWAY);

    obj = new ObjectData;
    GenerateShadowLOD( *obj );
    lodObj.InsertLevel( obj, LOD_GET_SHADOW( 0.0 ) );

    Pathname path = m_modelPath + m_filename + ".p3d";
    if ( lodObj.Save( path, OBJDATA_LATESTVERSION, false, 0, 0 ) != 0 ) 
    {
      throw new std::exception( "Unable to save output file (p3d)" );
    }
  }

//-----------------------------------------------------------------------------

  const vector< BuildingModelP3D::CGPolygon2 >& BuildingModelP3D::GetPartitions() const
  {
    return (m_partitions);
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::GenerateLOD( ObjectData& obj, bool textureLOD,
                                      bool genProceduralTexture )
  {
    int counter = -1;
    size_t cnt = m_footprint.GetVerticesCount();
    for ( size_t i = 0; i <= m_numOfFloors + 1; ++i )
    {
      for ( size_t j = 0; j < cnt; ++j )
      {
        obj.NewPoint()->SetPoint( Vector3( (Coord)m_footprint[j].x,
                                           (Coord)(counter * m_floorHeight),
                                           (Coord)m_footprint[j].y ) );
      }
      counter++;
    }

    vector< Shapes::DVertex > polygonRoof1( m_footprint.GetVertices() );
    if ( m_roofType == 1 )
    {
      if ( Offset( polygonRoof1, 1.0, true ) )
      {
        for ( size_t j = 0; j < cnt; ++j )
        {
          obj.NewPoint()->SetPoint( Vector3( (Coord)polygonRoof1[j].x,
                                             (Coord)((m_numOfFloors + 1) * m_floorHeight), 
                                             (Coord)polygonRoof1[j].y ) );
        }
      }
      else
      {
        m_roofType = 0;
      }
    }

    AutoArray< int > indices;

    ObjToolTopology& topology = obj.GetTool< ObjToolTopology >();

    // bottom (reversed order)
    for ( int j = cnt - 1; j >= 0; --j )
    {
      indices.Add( j );
    }
    topology.TessellatePolygon( indices.Size(), indices.Data() );

    // facades and inclined roofs
    size_t shift = (m_roofType == 1) ? 1 : 0;

    for ( size_t i = 0; i <= m_numOfFloors + shift; ++i )
    {
      for ( size_t j = 0; j < cnt; ++j )
      {
        FaceT fc1;
        fc1.CreateIn( &obj );
        fc1.SetN( 3 );

        FaceT fc2;
        fc2.CreateIn( &obj );
        fc2.SetN( 3 );

        if ( j != cnt - 1 )
        {
          fc1.SetPoint( 0, i * cnt + j );
          fc1.SetPoint( 1, i * cnt + j + 1 );
          fc1.SetPoint( 2, (i + 1) * cnt + j );
          obj.FaceSelect( fc1.GetFaceIndex() );

          fc2.SetPoint( 0, i * cnt + j + 1 );
          fc2.SetPoint( 1, (i + 1) * cnt + j + 1 );
          fc2.SetPoint( 2, (i + 1) * cnt + j );
          obj.FaceSelect( fc2.GetFaceIndex() );
        }
        else
        {
          fc1.SetPoint( 0, i * cnt + j );
          fc1.SetPoint( 1, i * cnt );
          fc1.SetPoint( 2, (i + 1) * cnt + j );
          obj.FaceSelect( fc1.GetFaceIndex() );

          fc2.SetPoint( 0, i * cnt );
          fc2.SetPoint( 1, (i + 1) * cnt );
          fc2.SetPoint( 2, (i + 1) * cnt + j );
          obj.FaceSelect( fc2.GetFaceIndex() );
        }
      }
    }

    // top
    switch ( m_roofType )
    {
    case 1:
      {
        indices.Clear();
        for ( size_t j = 0; j < cnt; ++j )
        {
          indices.Add( (m_numOfFloors + 2) * cnt + j );
        }
        topology.TessellatePolygon( indices.Size(), indices.Data() );
      }
      break;
    case 0:
    default:
      {
        indices.Clear();
        for ( size_t j = 0; j < cnt; ++j )
        {
          indices.Add( (m_numOfFloors + 1) * cnt + j );
        }
        topology.TessellatePolygon( indices.Size(), indices.Data() );
      }
    }

    int randTexture = (int)RandomInRangeF( 1, PREDEF_PROC_TEXT_COUNT ) - 1;

    // masses
    for ( size_t i = 0, pCnt = obj.NPoints(); i < pCnt; ++i )
    {
      obj.SetPointMass( i, 1250.0f );
    }

    // sharp edges and textures
    size_t floorCounter = 0;
    size_t faceCounter = 0;
    for ( size_t i = 0, fCnt = obj.NFaces(); i < fCnt; ++i )
    {
      FaceT fc( obj, i );
      size_t vCnt = fc.GetVxCount();
      for ( size_t j = 0, k = vCnt - 1; j < vCnt; k = j++ )
      {
        int v1 = fc.GetPoint( k );
        int v2 = fc.GetPoint( j );
        obj.AddSharpEdge( v1, v2 );
      }

      if ( textureLOD )
      {
        if ( genProceduralTexture )
        {
          if ( i < cnt - 2 )
          {
            // bottom 
            fc.SetTexture( m_baseProcTexture.c_str() );
          }
          else if ( cnt - 2 <= i && i < cnt - 2 + 2 * (m_numOfFloors + 1) * cnt )
          {
            // facades 
            faceCounter++;
            if ( faceCounter > 2 * cnt )
            {
              faceCounter = 1;
              floorCounter++;
            }

            RString procText;
            if ( floorCounter % 2 == 0 )
            {
              procText = m_procTextures[randTexture * 2].c_str();
            }
            else
            {
              procText = m_procTextures[randTexture * 2 + 1].c_str();
            }
            fc.SetTexture( procText );
          }
          else
          {
            // top 
            fc.SetTexture( m_roofProcTexture.c_str() );
          }
        }
        else
        {
          if ( i < cnt - 2 )
          {
            // bottom 
            fc.SetTexture( m_baseProcTexture.c_str() );
          }
          else if ( cnt - 2 <= i && i < cnt - 2 + 2 * (m_numOfFloors + 1) * cnt )
          {
            RString path;
            path = m_texturesPath + m_textures[0].GetFilename();
            int posDrive = path.Find( ":" );
            path = path.Substring( posDrive + 1, path.GetLength() );
            if ( path[0] == '\\' )
            {
              path = path.Substring( 1, path.GetLength() );
            }

            const VecT& vec0 = fc.GetPointVector< VecT >( 0 );
            const VecT& vec1 = fc.GetPointVector< VecT >( 1 );
            const VecT& vec2 = fc.GetPointVector< VecT >( 2 );
            int i0 = fc.GetPoint( 0 );
            int i1 = fc.GetPoint( 1 );
            int i2 = fc.GetPoint( 2 );

            float u0, v0;
            float u1, v1;
            float u2, v2;
            float ratio;

            if ( vec0.Y() == vec1.Y() )
            {
              ratio = 1.0f + floor( vec0.Distance( vec1 ) / fabs( vec0.Y() - vec2.Y() ) );

              int iDiff = i0 - i1;
              if ( abs( iDiff ) == 1 )
              {
                if ( iDiff > 0 )
                {
                  u0 = 1.0f;
                  u1 = 0.0f;
                }
                else
                {
                  u0 = 0.0f;
                  u1 = 1.0f;
                }
              }
              else
              {
                if ( iDiff > 0 )
                {
                  u0 = 0.0f;
                  u1 = 1.0f;
                }
                else
                {
                  u0 = 1.0f;
                  u1 = 0.0f;
                }
              }

              if ( vec2.Distance( vec0 ) < vec2.Distance( vec1 ) )
              {
                u2 = u0;
              }
              else
              {
                u2 = u1;
              }

              if ( vec0.Y() < vec2.Y() )
              {
                v0 = 1.0f;
                v1 = 1.0f;
                v2 = 0.0f;
              }
              else
              {
                v0 = 0.0f;
                v1 = 0.0f;
                v2 = 1.0f;
              }
            }
            else if ( vec0.Y() == vec2.Y() )
            {
              ratio = 1.0f + floor( vec0.Distance( vec2 ) / fabs( vec0.Y() - vec1.Y() ) );

              int iDiff = i0 - i2;
              if ( abs( iDiff ) == 1 )
              {
                if ( iDiff > 0 )
                {
                  u0 = 1.0f;
                  u2 = 0.0f;
                }
                else
                {
                  u0 = 0.0f;
                  u2 = 1.0f;
                }
              }
              else
              {
                if ( iDiff > 0 )
                {
                  u0 = 0.0f;
                  u2 = 1.0f;
                }
                else
                {
                  u0 = 1.0f;
                  u2 = 0.0f;
                }
              }

              if ( vec1.Distance( vec0 ) < vec1.Distance( vec2 ) )
              {
                u1 = u0;
              }
              else
              {
                u1 = u2;
              }

              if ( vec0.Y() < vec1.Y() )
              {
                v0 = 1.0f;
                v2 = 1.0f;
                v1 = 0.0f;
              }
              else
              {
                v0 = 0.0f;
                v2 = 0.0f;
                v1 = 1.0f;
              }
            }
            else
            {
              ratio = 1.0f + floor( vec1.Distance( vec2 ) / fabs( vec1.Y() - vec0.Y() ) );

              int iDiff = i1 - i2;
              if ( abs( iDiff ) == 1 )
              {
                if ( iDiff > 0 )
                {
                  u1 = 1.0f;
                  u2 = 0.0f;
                }
                else
                {
                  u1 = 0.0f;
                  u2 = 1.0f;
                }
              }
              else
              {
                if ( iDiff > 0 )
                {
                  u1 = 0.0f;
                  u2 = 1.0f;
                }
                else
                {
                  u1 = 1.0f;
                  u2 = 0.0f;
                }
              }

              if ( vec0.Distance( vec1 ) < vec0.Distance( vec2 ) )
              {
                u0 = u1;
              }
              else
              {
                u0 = u2;
              }

              if ( vec1.Y() < vec0.Y() )
              {
                v1 = 1.0f;
                v2 = 1.0f;
                v0 = 0.0f;
              }
              else
              {
                v1 = 0.0f;
                v2 = 0.0f;
                v0 = 1.0f;
              }
            }

            if ( u0 == 1.0f ) { u0 *= ratio; }
            if ( u1 == 1.0f ) { u1 *= ratio; }
            if ( u2 == 1.0f ) { u2 *= ratio; }

            fc.SetUV( 0, u0, v0 );
            fc.SetUV( 1, u1, v1 );
            fc.SetUV( 2, u2, v2 );

            fc.SetTexture( path );
          }
          else
          {
            // top 
            fc.SetTexture( m_roofProcTexture.c_str() );
          }
        }
      }
    }

    GenerateMapping( obj );

    obj.RecalcNormals();
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::GenerateGeometryLOD( ObjectData* obj )
  {
    GenerateComponentsLOD( obj );
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::GenerateViewGeometryLOD( ObjectData* obj )
  {
    GenerateComponentsLOD( obj );
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::GenerateFireGeometryLOD( ObjectData* obj )
  {
    GenerateComponentsLOD( obj );
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::GenerateRoadwayLOD( ObjectData& obj )
  {
    AutoArray< int > indices;

    ObjToolTopology& topology = obj.GetTool< ObjToolTopology >();

    // top
    switch ( m_roofType )
    {
    case 0:
      {
        for ( size_t j = 0, cnt = m_footprint.GetVerticesCount(); j < cnt; ++j )
        {
          obj.NewPoint()->SetPoint( Vector3( (Coord)m_footprint[j].x,
                                             (Coord)(m_numOfFloors * m_floorHeight),
                                             (Coord)m_footprint[j].y ) );
          indices.Add( j );
        }
        topology.TessellatePolygon( indices.Size(), indices.Data() );
      }
      break;
    case 1:
      {
        size_t counter = 0;
        for ( size_t j = 0, cnt = m_footprint.GetVerticesCount(); j < cnt; ++j )
        {
          obj.NewPoint()->SetPoint( Vector3( (Coord)m_footprint[j].x,
                                             (Coord)(m_numOfFloors * m_floorHeight),
                                             (Coord)m_footprint[j].y ) );
          ++counter;
        }

        vector< Shapes::DVertex > polygon = m_footprint.GetVertices();
        if ( Offset( polygon, 1.0, true ) )
        {
          for ( size_t j = 0, cnt = polygon.size(); j < cnt; ++j )
          {
            obj.NewPoint()->SetPoint( Vector3( (Coord)polygon[j].x,
                                               (Coord)((m_numOfFloors + 1) * m_floorHeight),
                                               (Coord)polygon[j].y ) );
            indices.Add( counter + j );
          }
        }

        // facade
        for ( size_t j = 0, cnt = polygon.size(); j < cnt; ++j )
        {
          FaceT fc1;
          fc1.CreateIn( &obj );
          fc1.SetN( 3 );

          if ( j != cnt - 1 )
          {
            fc1.SetPoint( 0, j );
            fc1.SetPoint( 1, j + 1 );
            fc1.SetPoint( 2, cnt + j );

            obj.FaceSelect( fc1.GetFaceIndex() );

            FaceT fc2;
            fc2.CreateIn( &obj );
            fc2.SetN( 3 );

            fc2.SetPoint( 0, j + 1 );
            fc2.SetPoint( 1, cnt + j + 1 );
            fc2.SetPoint( 2, cnt + j );

            obj.FaceSelect( fc2.GetFaceIndex() );
          }
          else
          {
            fc1.SetPoint( 0, j );
            fc1.SetPoint( 1, 0 );
            fc1.SetPoint( 2, cnt + j );

            obj.FaceSelect( fc1.GetFaceIndex() );

            FaceT fc2;
            fc2.CreateIn( &obj );
            fc2.SetN( 3 );

            fc2.SetPoint( 0, 0 );
            fc2.SetPoint( 1, cnt );
            fc2.SetPoint( 2, cnt + j );

            obj.FaceSelect( fc2.GetFaceIndex() );
          }
        }

        // top
        topology.TessellatePolygon( indices.Size(), indices.Data() );
      }
      break;
    default:
      {
        for ( size_t j = 0, cnt = m_footprint.GetVerticesCount(); j < cnt; ++j )
        {
          obj.NewPoint()->SetPoint( Vector3( (Coord)m_footprint[j].x,
                                             (Coord)(m_numOfFloors * m_floorHeight), 
                                             (Coord)m_footprint[j].y ) );
          indices.Add( j );

        }
        topology.TessellatePolygon( indices.Size(), indices.Data() );
      }
    }

    obj.RecalcNormals();
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::GenerateShadowLOD( ObjectData& obj )
  {
    GenerateLOD( obj );
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::GenerateMapping( ObjectData& obj )
  {
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::SetOrigin()
  {
    m_position = m_footprint.GetBarycenter();

    for ( size_t i = 0, cnt = m_footprint.GetVerticesCount(); i < cnt; ++i )
    {
      m_footprint[i].x -= m_position.x;
      m_footprint[i].y -= m_position.y;
    }
    m_footprint.ResetBoundingBox();
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::OrientateAsMinBoundingBox()
  {
    m_orientation = m_footprint.GetMinBoundingBoxOrientation();

    if ( m_orientation != 0.0 )
    {
      // we are rotating about the origin
      double x;
      double y;
      for ( size_t i = 0, cnt = m_footprint.GetVerticesCount(); i < cnt; ++i )
      {
        x = m_footprint[i].x;
        y = m_footprint[i].y;
        m_footprint[i].x = x * cos( m_orientation ) + y * sin( m_orientation );
        m_footprint[i].y = - x * sin( m_orientation ) + y * cos( m_orientation );
      }
    }
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::CorrectOrigin()
  {
    Shapes::DVertex minP = m_footprint.GetMinPoint();

    double halfBBWidth  = 0.5 * m_footprint.GetMinBoundingBoxWidth();
    double halfBBHeight = 0.5 * m_footprint.GetMinBoundingBoxHeight();

    double dx = halfBBWidth + minP.x;
    double dy = halfBBHeight + minP.y;

    for ( size_t i = 0, cnt = m_footprint.GetVerticesCount(); i < cnt; ++i )
    {
      m_footprint[i].x -= dx;
      m_footprint[i].y -= dy;
    }

    double rotDx = dx * cos( m_orientation ) - dy * sin( m_orientation );
    double rotDy = dx * sin( m_orientation ) + dy * cos( m_orientation );

    m_position.x += rotDx;
    m_position.y += rotDy;
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::GenerateComponents( ObjectData& obj )
  {
/*
    // old code
    {
      Selection sel(&obj);
      for (int i = 0; i < obj.NPoints(); ++i)
      {
        sel.PointSelect(i);
      }

      for (int i = 0; i < obj.NFaces(); ++i)
      {
        sel.FaceSelect(i);
      }

      obj.SaveNamedSel("Component01", &sel);
    }
*/
    // new code
    {
      CGPolygon2 poly;
      for ( size_t i = 0, cnt = m_footprint.GetVerticesCount(); i < cnt; ++i )
      {
        poly.AppendVertex( m_footprint[i].x, m_footprint[i].y );
      }

      poly.Partition();
      m_partitions = poly.GetPartitions();

      for ( size_t i = 0, cnt1 = m_partitions.size(); i < cnt1; ++i )
      {
        Selection sel( &obj );

        for ( size_t j = 0, cnt2 = m_partitions[i].VerticesCount(); j < cnt2; ++j )
        {
          for ( int k = 0, cnt3 = m_footprint.GetVerticesCount(); k < cnt3; ++k )
          {
            CGPoint2 point( m_footprint[k].x, m_footprint[k].y );

            if ( m_partitions[i].FindVertex( point ) )
            {
              size_t shift = 0;
              if ( m_roofType == 1 ) { shift = 1; }
              for ( size_t m = 0; m <= m_numOfFloors + 1 + shift; ++m )
              {
                sel.PointSelect( k + m * cnt3 );
              }
            }
          }
        }

        char chCounter[4];
        sprintf_s( chCounter, "%d", i + 1 );
        string strCounter = chCounter;
        
        while ( strCounter.length() < 2 )
        {
          strCounter = "0" + strCounter;
        }

        string component = "Component" + strCounter;

        obj.SaveNamedSel( component.c_str(), &sel );
      }
    }
  }

//-----------------------------------------------------------------------------

  void BuildingModelP3D::GenerateComponentsLOD( ObjectData* obj )
  {
    CGPolygon2 poly;
    for ( size_t i = 0, cnt = m_footprint.GetVerticesCount(); i < cnt; ++i )
    {
      poly.AppendVertex( m_footprint[i].x, m_footprint[i].y );
    }

    poly.Partition();
    m_partitions = poly.GetPartitions();

    size_t globalVerticesCounter = 0;
    size_t oldFacesCount = 0;

    for ( size_t p = 0, cntP = m_partitions.size(); p < cntP; ++p )
    {
      int counter = -1;
      size_t tempVerticesCounter = 0;
      size_t cnt = m_partitions[p].VerticesCount();
      for ( size_t f = 0; f <= m_numOfFloors + 1; ++f )
      {
        for ( size_t j = 0; j < cnt; ++j )
        {
          const CGPoint2& v = m_partitions[p].Vertex( j );
          obj->NewPoint()->SetPoint( Vector3( (Coord)v.GetX(),
                                              (Coord)(counter * m_floorHeight), 
                                              (Coord)v.GetY() ) );
          ++tempVerticesCounter;
        }
        counter++;
      }

      if ( m_roofType == 1 )
      {
        vector< Shapes::DVertex > polygonRoof1;
        for ( size_t j = 0; j < cnt; ++j )
        {
          const CGPoint2& point = m_partitions[p].Vertex( j );
          polygonRoof1.push_back( Shapes::DVertex( point.GetX(), point.GetY() ) );
        }
        if ( Offset( polygonRoof1, 1.0, true ) )
        {
          for ( size_t j = 0; j < cnt; ++j )
          {
            obj->NewPoint()->SetPoint( Vector3( (Coord)polygonRoof1[j].x,
                                                (Coord)((m_numOfFloors + 1) * m_floorHeight), 
                                                (Coord)polygonRoof1[j].y ) );
            ++tempVerticesCounter;
          }
        }
        else
        {
          m_roofType = 0;
        }
      }

      AutoArray< int > indices;

      ObjToolTopology& topology = obj->GetTool< ObjToolTopology >();

      // bottom (reversed order)
      for ( int j = cnt - 1; j >= 0; --j )
      {
        indices.Add( globalVerticesCounter + j );
      }
      topology.TessellatePolygon( indices.Size(), indices.Data() );

      // facades and inclined roofs
      size_t shift = 0;
      if ( m_roofType == 1 ) { shift = 1; }

      for ( size_t i = 0; i <= m_numOfFloors + shift; ++i )
      {
        for ( size_t j = 0; j < cnt; ++j )
        {
          FaceT fc1;
          fc1.CreateIn( obj );
          fc1.SetN( 3 );

          if ( j != cnt - 1 )
          {
            fc1.SetPoint( 0, i * cnt + j + globalVerticesCounter );
            fc1.SetPoint( 1, i * cnt + j + 1 + globalVerticesCounter );
            fc1.SetPoint( 2, (i + 1) * cnt + j + globalVerticesCounter );

            obj->FaceSelect( fc1.GetFaceIndex() );

            FaceT fc2;
            fc2.CreateIn( obj );
            fc2.SetN( 3 );

            fc2.SetPoint( 0, i * cnt + j + 1 + globalVerticesCounter );
            fc2.SetPoint( 1, (i + 1) * cnt + j + 1 + globalVerticesCounter  );
            fc2.SetPoint( 2, (i + 1) * cnt + j + globalVerticesCounter );

            obj->FaceSelect( fc2.GetFaceIndex() );
          }
          else
          {
            fc1.SetPoint( 0, i * cnt + j + globalVerticesCounter );
            fc1.SetPoint( 1, i * cnt + globalVerticesCounter );
            fc1.SetPoint( 2, (i + 1) * cnt + j + globalVerticesCounter );

            obj->FaceSelect( fc1.GetFaceIndex() );

            FaceT fc2;
            fc2.CreateIn( obj );
            fc2.SetN( 3 );

            fc2.SetPoint( 0, i * cnt + globalVerticesCounter );
            fc2.SetPoint( 1, (i + 1) * cnt + globalVerticesCounter );
            fc2.SetPoint( 2, (i + 1) * cnt + j + globalVerticesCounter );

            obj->FaceSelect( fc2.GetFaceIndex() );
          }
        }
      }

      // top
      switch ( m_roofType )
      {
      case 0:
        {
          indices.Clear();
          for ( size_t j = 0; j < cnt; ++j )
          {
            indices.Add( (m_numOfFloors + 1) * cnt + j + globalVerticesCounter);
          }
          topology.TessellatePolygon( indices.Size(), indices.Data() );
        }
        break;
      case 1:
        {
          indices.Clear();
          for ( size_t j = 0; j < cnt; ++j )
          {
            indices.Add( (m_numOfFloors + 2) * cnt + j + globalVerticesCounter);
          }
          topology.TessellatePolygon( indices.Size(), indices.Data() );
        }
        break;
      default:
        {
          indices.Clear();
          for ( size_t j = 0; j < cnt; ++j )
          {
            indices.Add( (m_numOfFloors + 1) * cnt + j + globalVerticesCounter);
          }
          topology.TessellatePolygon( indices.Size(), indices.Data() );
        }
      }

      // masses
      for ( size_t j = 0; j < tempVerticesCounter; ++j )
      {
        obj->SetPointMass( j + globalVerticesCounter, 1250.0f );
      }

      // selections
      size_t newFacesCount = obj->NFaces();
      size_t currFacesCount = newFacesCount - oldFacesCount;
      Selection sel( obj );

      for ( size_t j = 0; j < tempVerticesCounter; ++j )
      {
        sel.PointSelect( j + globalVerticesCounter );
      }
      for ( size_t j = 0; j < currFacesCount; ++j )
      {
        sel.FaceSelect( j + oldFacesCount );
      }
      oldFacesCount = newFacesCount;

      char chCounter[4];
      sprintf_s( chCounter, "%d", p + 1 );
      string strCounter = chCounter;
        
      while ( strCounter.length() < 2 )
      {
        strCounter = "0" + strCounter;
      }

      string component = "Component" + strCounter;

      obj->SaveNamedSel( component.c_str(), &sel );

      globalVerticesCounter += tempVerticesCounter;
    }
  }

//-----------------------------------------------------------------------------

  Footprint& BuildingModelP3D::GetFootprint()
  {
    return (m_footprint);
  }

//-----------------------------------------------------------------------------

  const Footprint& BuildingModelP3D::GetFootprint() const
  {
    return (m_footprint);
  }

//-----------------------------------------------------------------------------

  size_t BuildingModelP3D::GetNumOfFloors() const
  {
    return (m_numOfFloors);
  }

//-----------------------------------------------------------------------------

  double BuildingModelP3D::GetFloorHeight() const
  {
    return (m_floorHeight);
  }

//-----------------------------------------------------------------------------

  size_t BuildingModelP3D::GetRoofType() const
  {
    return (m_roofType);
  }

//-----------------------------------------------------------------------------

  const vector< Texture >& BuildingModelP3D::GetTextures() const
  {
    return (m_textures);
  }

//-----------------------------------------------------------------------------

  RString BuildingModelP3D::GetFilename() const
  {
    return (m_filename);
  }

//-----------------------------------------------------------------------------

  Building::Building()
  {
  }

//-----------------------------------------------------------------------------

  Building::Building( const Footprint& footprint, size_t numOfFloors, 
                      size_t maxNumFloors, double floorHeight, size_t roofType, 
                      double roofRed, double roofGreen, double roofBlue )
  : m_footprint( footprint )
  , m_numOfFloors( numOfFloors )
  , m_maxNumFloors( maxNumFloors )
  , m_floorHeight( floorHeight )
  , m_roofType( roofType )
  , m_roofRed( roofRed )
  , m_roofGreen( roofGreen )
  , m_roofBlue( roofBlue )
  {
    m_fpTurningFunction = m_footprint.GetTurningFunction();
    m_fpTurningSequence = m_footprint.GetTurningSequence();

    if ( m_numOfFloors == 0 ) { m_numOfFloors = RandomInRangeI( 1, m_maxNumFloors ); }
  }

//-----------------------------------------------------------------------------

  Footprint& Building::GetFootprint()
  {
    return (m_footprint);
  }

//-----------------------------------------------------------------------------

  const Footprint& Building::GetFootprint() const
  {
    return (m_footprint);
  }

//-----------------------------------------------------------------------------

  Shapes::DVertex Building::GetPosition() const
  {
    return (m_position);
  }

//-----------------------------------------------------------------------------

  double Building::GetOrientation() const
  {
    return (m_orientation);
  }

//-----------------------------------------------------------------------------

  const BuildingModelP3D& Building::GetModelP3D() const
  {
    return (m_modelP3D);
  }

//-----------------------------------------------------------------------------

  void Building::GenerateP3DModel( IMainCommands* cmdIfc, RString modelsPath,
                                   RString texturesPath, const vector< Texture >& textures,
                                   vector< BuildingModelP3D >& modelsLibrary )
  {
    Shapes::DVertex position = m_footprint.GetBarycenter();

    int libIndex = GetModelLibIndex( textures, modelsLibrary );
    if ( libIndex == -1 )
    {			
      char buffer[80];
      sprintf( buffer, "building_%d_%d", (int)position.x, (int)position.y );
      RString modelName( buffer );

      m_modelP3D.Create( modelName, m_footprint, m_numOfFloors, m_floorHeight,
                         m_roofType, modelsPath, texturesPath,
                         textures, m_roofRed, m_roofGreen, m_roofBlue );
      m_modelP3D.ExportToP3DFile();

      modelsLibrary.push_back( m_modelP3D );
    }
    else
    {
      m_modelP3D = modelsLibrary[libIndex];
    }

    for ( size_t i = 0, cntI = m_footprint.GetVerticesCount(); i < cntI; ++i )
    {
      m_footprint[i].x -= position.x;
      m_footprint[i].y -= position.y;
    }
    m_footprint.ResetBoundingBox();

    double orientation = m_footprint.GetMinBoundingBoxOrientation();
    if ( orientation != 0.0 )
    {
      // we are rotating about the origin
      double x;
      double y;
      for ( size_t i = 0, cntI = m_footprint.GetVerticesCount(); i < cntI; ++i )
      {
        x = m_footprint[i].x;
        y = m_footprint[i].y;
        m_footprint[i].x = x * cos( orientation ) + y * sin( orientation );
        m_footprint[i].y = - x * sin( orientation ) + y * cos( orientation );
      }
    }

    Shapes::DVertex minP = m_footprint.GetMinPoint();

    double halfBBWidth  = 0.5 * m_footprint.GetMinBoundingBoxWidth();
    double halfBBHeight = 0.5 * m_footprint.GetMinBoundingBoxHeight();

    double dx = halfBBWidth + minP.x;
    double dy = halfBBHeight + minP.y;

    for ( size_t i = 0, cntI = m_footprint.GetVerticesCount(); i < cntI; ++i )
    {
      m_footprint[i].x -= dx;
      m_footprint[i].y -= dy;
    }

    double rotDx = dx * cos( orientation ) - dy * sin( orientation );
    double rotDy = dx * sin( orientation ) + dy * cos( orientation );

    position.x += rotDx;
    position.y += rotDy;

    Vector3D vPosition = Vector3D( position.x, 0.0, position.y );

    Matrix4D mTranslation = Matrix4D( MTranslation, vPosition );
    Matrix4D mRotation    = Matrix4D( MRotationY, orientation );

    Matrix4D transform = mTranslation * mRotation;
    cmdIfc->GetObjectMap()->PlaceObject( transform, m_modelP3D.GetFilename(), 0 );
  }

//-----------------------------------------------------------------------------

  int Building::GetModelLibIndex( const vector< Texture >& textures, const vector< BuildingModelP3D >& modelsLibrary ) 
  {
    for ( size_t i = 0, cntI = modelsLibrary.size(); i < cntI; ++i )
    {
      if ( m_footprint.GetVerticesCount() != modelsLibrary[i].GetFootprint().GetVerticesCount() ) { continue; }
      if ( m_numOfFloors != modelsLibrary[i].GetNumOfFloors() ) { continue; }
      if ( m_floorHeight != modelsLibrary[i].GetFloorHeight() ) { continue; }
      if ( m_roofType    != modelsLibrary[i].GetRoofType() ) { continue; }

      const vector< Texture >& modelTextures = modelsLibrary[i].GetTextures();
      if ( textures.size() != modelTextures.size() ) { continue; }
      bool match = true;
      for ( size_t t = 0, cntT = textures.size(); t < cntT; ++t )
      {
        if ( !textures[t].Equals( modelTextures[t] ) )
        {
          match = false;
          break;
        }
      }
      if ( !match ) { continue; }

      double thisArea  = m_footprint.GetArea();
      if ( thisArea == 0.0 ) { continue; }
      double otherArea = modelsLibrary[i].GetFootprint().GetArea();
      double areaDiff = abs( thisArea - otherArea );
      if ( areaDiff / thisArea > 0.05 ) { continue; }

      double thisRatio = m_footprint.GetMinBoundingBoxWidth() / m_footprint.GetMinBoundingBoxHeight();
      Footprint otherF = modelsLibrary[i].GetFootprint();
      double otherRatio = otherF.GetMinBoundingBoxWidth() / otherF.GetMinBoundingBoxHeight();
      double ratioDiff = abs( thisRatio - otherRatio );
      if ( ratioDiff / thisRatio > 0.05 ) { continue; }

      string thisTS = m_footprint.GetTurningSequence();
      thisTS += thisTS;

      string otherTS = otherF.GetTurningSequence();

      size_t pos = thisTS.find( otherTS );
      if ( pos == string::npos ) { continue; }

      m_footprint.SlideVertices( pos );

      match = true;
      for ( size_t v = 0, cntV = m_footprint.GetVerticesCount(); v < cntV - 1; ++v )
      {
        double thisLen  = m_footprint.GetVertex( v ).DistanceXY( m_footprint.GetVertex( v + 1 ) );
        double otherLen = otherF.GetVertex( v ).DistanceXY( otherF.GetVertex( v + 1 ) );
        double lenDiff = abs( thisLen - otherLen );
        if ( lenDiff / thisLen > 0.25 ) 
        { 
          match = false;
          break;
        }
      }
      if ( !match ) { continue; }

      return (i);
    }
    return (-1);
  }

//-----------------------------------------------------------------------------

} // namespace BC_Help