//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"
#include "..\..\HighMapLoaders\include\EtMath.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class DummyBuildingPlacer : public IBuildModule
		{
		public:
			// enumerator for building type
			enum BuildingType 
      { 
        btNULL, 
        btCIVIL, 
        btINDUSTRIAL, 
        btMILITARY, 
        btNUM 
      };

		public:
			// enumerator for building shape
			enum BuildingShape 
      { 
        bsRECTANGLE, 
        bsLSHAPE, 
        bsNUM 
      };

			// L-Shape can be vectorialized in several ways, depending from
			// the choice of the first vertex and of the direction of rotation (CW or CCW)
			// To let the module recognize which is the configuration of the current
			// L-Shape we have to take in account of all possible variants.
			// The method choiced is to start from the first edge (determined by the first
			// two vertices) and then follow the perimeter of the shape. When we arrive 
			// to a corner, we change edge and we can turn or to the right (R) or to the left (L).
			// writing the sequences of turns we obtain an unique string of Ls and Rs
			// identifying the current configuration.
			// known the configuration, we will be able to determine the direction of every
			// edge, and to know which edge we have to be cutted to obtain the 2 rectangles
			// forming the L-Shape

			// enum defining the possible L-Shape configuration
			enum LShapeConfiguration 
      { 
        lscRLLLLL, 
        lscLRLLLL, 
        lscLLRLLL, 
        lscLLLRLL, 
        lscLLLLRL, 
        lscLLLLLR, 
        lscLRRRRR, 
        lscRLRRRR, 
        lscRRLRRR, 
        lscRRRLRR, 
        lscRRRRLR, 
        lscRRRRRL,
        lscNUM
      };

			// enum defining the direction of rotation of vertices
			enum ShapeVerticesRotationDir 
      { 
        vrdCW, 
        vrdCCW 
      };

		public:
			struct GlobalInput
			{
				RString m_modelCivil;
				RString m_modelIndustrial;
				RString m_modelMilitary;
				double  m_heightFloorsCivil;
				double  m_heightFloorsIndustrial;
				double  m_heightFloorsMilitary;
				int     m_maxNumFloorsCivil;
				int     m_maxNumFloorsIndustrial;
				int     m_maxNumFloorsMilitary;
			};

			struct DbfInput
			{
				BuildingType m_buildingType;
				int          m_numOfFloors;
			};

		private:
			class Edge
			{
				Shapes::DVertex* m_v1;
				Shapes::DVertex* m_v2;

			public:
				// constructors
				Edge() 
        : m_v1( NULL )
				, m_v2( NULL )
				{
				}

				Edge( Shapes::DVertex* v1, Shapes::DVertex* v2 )
        : m_v1( v1 )
        , m_v2( v2 ) 
        {
        }

				// copy constructor
				Edge( const Edge& other )
				{
					m_v1 = other.m_v1;
					m_v2 = other.m_v2;
				}

				// sets the given vertex with the given coordinates
				// vertex must be 1 or 2
				void SetVertex( int vertex, double x, double y )
				{
					switch ( vertex )
					{
          default:
					case 1:
						m_v1->x = x;
						m_v1->y = y;
						break;
					case 2:
						m_v2->x = x;
						m_v2->y = y;
						break;
					}
				}

				// return the x coordinate of the given vertex
				// vertex must be 1 or 2
				double GetVertexX( int vertex )
				{
					switch ( vertex )
					{
          default:
					case 1:
						return (m_v1->x);
					case 2:
						return (m_v2->x);
					}
				}

				// return the y coordinate of the given vertex
				// vertex must be 1 or 2
				double GetVertexY( int vertex )
				{
					switch ( vertex )
					{
          default:
					case 1:
						return (m_v1->y);
					case 2:
						return (m_v2->y);
					}
				}

				// return the mid point of the edge
				Shapes::DVertex MidPoint()
				{
					return (Shapes::DVertex( (m_v1->x + m_v2->x) * 0.5, (m_v1->y + m_v2->y) * 0.5 ));
				}

				// return the angle of direction of this edge in the plane XY
				// from the x axis (in radians)
				double DirectionXY()
				{
					return (EtMathD::SegmentDirection_XY_CCW_X( m_v1->x, m_v1->y, m_v2->x, m_v2->y ));
				}

				// return the length of this edge
				double Length()
				{
					return (m_v1->DistanceXY(*m_v2));
				}

				ClassIsMovable( Edge ); 
			};

		public:
			DummyBuildingPlacer();

      virtual ~DummyBuildingPlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, GlobalInput& globalIn, 
                                             DbfInput& dbfIn );

      DECLAREMODULEEXCEPTION( "DummyBuildingPlacer" )

		private:
      string m_version;

			AutoArray< Shapes::DVertex > m_vertices;
			AutoArray< Edge >            m_edges;

			ModuleObjectOutputArray m_objectsOut;

			string m_lShapeOddEdgeSigns[12];
			string m_lShapeEvenEdgeSigns[12];
			string m_strLShapeConfiguration[12];

			const Shapes::IShape* m_currentShape; 
			int                   m_csVertexCount;
			GlobalInput           m_globalIn;
			DbfInput              m_dbfIn;
			double                m_meanDirectionU;
			double                m_meanDirectionV;
			LShapeConfiguration   m_lShapeConfiguration;
			BuildingShape         m_buildingShape;

			bool GetDbfParameters( IMainCommands* cmdIfc );
			bool GetGlobalParameters( IMainCommands* cmdIfc );
			bool SetGeometry();
			void CalculateMeanUVDirections();
			void OrthogonalizeUV();
			void ReshapeVertices();
			void RotateAboutPoint( const Shapes::DVertex point, double angle );
			void OrthogonalizeShape();
			void CreateObjects();
			void ExportCreatedObjects( IMainCommands* cmdIfc );
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt