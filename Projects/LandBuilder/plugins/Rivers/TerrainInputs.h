//-----------------------------------------------------------------------------
// File: TerrainInputs.h
//
// Desc: class related to terrain data import
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "..\..\landbuilder2\MapInput.h"

//-----------------------------------------------------------------------------

class CTerrainInputs : public MapInput
{
  double m_noWaterElevation;
  double m_waterDepth;
  double m_terrainDepth;
  bool   m_applySmooth;

public:
  CTerrainInputs();

  CTerrainInputs( string demType, string filename, size_t srcLeft, size_t srcTop,
                  size_t srcRight, size_t srcBottom, double dstLeft, double dstTop,
                  double dstRight, double dstBottom, double noWaterElevation,
                  double waterDepth, double terrainDepth, bool applySmooth );

  double GetNoWaterElevation() const
  {
    return (m_noWaterElevation);
  }

  void SetNoWaterElevation( double noWaterElevation )
  {
    m_noWaterElevation = noWaterElevation;
  }

  double GetWaterDepth() const
  {
    return (m_waterDepth);
  }

  void SetWaterDepth( double waterDepth )
  {
    m_waterDepth = waterDepth;
  }

  double GetTerrainDepth() const
  {
    return (m_terrainDepth);
  }

  void SetTerrainDepth( double terrainDepth )
  {
    m_terrainDepth = terrainDepth;
  }

  bool GetApplySmooth() const
  {
    return (m_applySmooth);
  }

  void SetApplySmooth( bool applySmooth )
  {
    m_applySmooth = applySmooth;
  }
};

//-----------------------------------------------------------------------------
