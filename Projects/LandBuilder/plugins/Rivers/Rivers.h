//-----------------------------------------------------------------------------
// File: Rivers.h
//
// Desc: module to generate procedural rivers
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Windows.h"
#include "Commons.h"
#include "TerrainInputs.h"
#include "GaussianFilter.h"
#include "TRectQuadTree.h"

//-----------------------------------------------------------------------------

#define DEM_TYPE      "demType"
#define FILENAME      "filename"
#define SRC_LEFT      "srcLeft"
#define SRC_RIGHT     "srcRight"
#define SRC_TOP       "srcTop"
#define SRC_BOTTOM    "srcBottom"
#define DST_LEFT      "dstLeft"
#define DST_RIGHT     "dstRight"
#define DST_TOP       "dstTop"
#define DST_BOTTOM    "dstBottom"
#define NO_WATER_ELEV "noWaterElev"
#define WATER_DEPTH   "waterDepth"
#define TERRAIN_DEPTH "terrainDepth"
#define APPLY_SMOOTH  "applySmooth"
#define MAX_WIDTH   "maxWidth"
#define FILE_PREFIX "namePrefix"

//-----------------------------------------------------------------------------

class CRivers : public LandBuilder2::IBuildModule
{
	// internal use flags
	// {
	size_t m_shapesCount;
	// }

	// the path containing LandBuilder
	String m_workingPath;

#ifdef _DEBUG
  String m_dxfPath;
#endif

	CTerrainInputs m_terrainIn;
  Elevations     m_riverBed;
  Elevations     m_waterSurface;

  Float m_noWaterElevation;
  Float m_waterDepth;
  Float m_terrainDepth;
  Float m_maxTriSize;

  bool* m_containmentMatrix;

  size_t m_filterSize;

  // logger to file
	ofstream m_logFile;

  String m_filePrefix;

public:
	CRivers();
	virtual ~CRivers();

	virtual void Run( IMainCommands* cmdIfc );
  virtual void OnEndPass( IMainCommands* cmdIfc );

private:
  bool GetTerrainParameters( IMainCommands* cmdIfc, LandBuilder2::ParamParser& params );
  bool LoadHighMap();
  void UpdateContainmentMatrix( const CGPolygon2& riverContour );
  void CalculateRiverBed();
  void CalculateWaterSurface();
};

//-----------------------------------------------------------------------------
