//-----------------------------------------------------------------------------
// File: Rivers.cpp
//
// Desc: module to generate procedural rivers
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Rivers.h"

//-----------------------------------------------------------------------------

static LandBuilder2::RegisterModule< CRivers > rivers( "Rivers" );

//-----------------------------------------------------------------------------

CRivers::CRivers()
: m_shapesCount( 0 )
, m_containmentMatrix( NULL )
, m_noWaterElevation( CGConsts::ZERO )
, m_waterDepth( CGConsts::ZERO )
, m_terrainDepth( CGConsts::ZERO )
, m_maxTriSize( CGConsts::ZERO )
, m_filterSize( 3 )
, m_filePrefix( "" )
{
  // do nothing
}

//-----------------------------------------------------------------------------

CRivers::~CRivers()
{
  if ( m_containmentMatrix ) 
    delete m_containmentMatrix; 

  // closes log file
	if ( !m_logFile.fail() ) 
    m_logFile.close();  
}

//-----------------------------------------------------------------------------

void CRivers::Run( IMainCommands* cmdIfc )
{
  RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
  if ( taskName != m_currTaskName )
  {
    m_shapesCount  = 0;
    m_currTaskName = taskName;
  }

	++m_shapesCount;
	LandBuilder2::ParamParser params( cmdIfc );

	// this part contains all the "global" variables and it is run only the
	// first time we enter this module
	if ( m_shapesCount == 1 )
	{
		// sets the working path
		m_workingPath = cmdIfc->GetCurrentTask()->GetWorkFolder();

#ifdef _DEBUG
    m_dxfPath = m_workingPath + "\\dxf\\";
		Pathname::CreateFolder( m_dxfPath );
#endif

    // opens log file
    m_logFile.open( m_workingPath + "\\Rivers.log", std::ios::out | std::ios::trunc );
    if ( m_logFile.fail() )
      LOGF( Error ) << "Unable to open the log file.";

    LOGF( Info ) << "===========================================================";
    LOGF( Info ) << "Rivers 1.1.8                                               ";
    LOGF( Info ) << "===========================================================";
    m_logFile << "============" << endl;
    m_logFile << "Rivers 1.1.8" << endl;
    m_logFile << "============" << endl << endl;

		// gets maps parameters from calling cfg file
    if ( !GetTerrainParameters( cmdIfc, params ) ) 
      return; 

    // gets the file prefix
    m_filePrefix = "";
	  try 
	  {
		  m_filePrefix = params.GetTextValue( FILE_PREFIX );
	  }
	  catch ( LandBuilder2::ParamParserException& ) 
	  {
		  LOGF( Error ) << "Warning: Missing \"" << FILE_PREFIX << "\" field in class Parameters. Using no prefix.";
		  m_logFile << "Warning: Missing \"" << FILE_PREFIX << "\" field in class Parameters. Using no prefix." << endl;
      m_filePrefix = "";
	  }

		// loads terrain
		LoadHighMap();

    m_noWaterElevation = m_terrainIn.GetNoWaterElevation();
    m_waterDepth       = m_terrainIn.GetWaterDepth();
    m_terrainDepth     = m_terrainIn.GetTerrainDepth();

    m_logFile << "Applied no water elevation (m): " << m_noWaterElevation << endl;
    m_logFile << "Applied terrain depth (m): " << m_terrainDepth << endl;
    m_logFile << "Applied water depth (m): " << m_waterDepth << endl << endl;

    const Elevations& hmData = m_terrainIn.GetHmData();
    int sizeU = (int)hmData.getSizeU();
    int sizeV = (int)hmData.getSizeV();
    m_containmentMatrix = new bool[sizeU * sizeV];
    assert( m_containmentMatrix );

    for ( int z = 0; z < sizeV; ++z )
    {
      for ( int x = 0; x < sizeU; ++x )
      {
        m_containmentMatrix[z * sizeU + x] = false;
      }
    }

    m_riverBed     = hmData;
    m_waterSurface = hmData; 
    m_waterSurface.setAllWorldWToSingleValue( m_noWaterElevation );
  }

	// this part apply to all the shapes coming from the shapefile
	LOGF( Info ) << "Working on shape: " << m_shapesCount << "                        ";

	// gets shape and its properties
	const Shapes::IShape* curShape = cmdIfc->GetActiveShape();

	// shape validation
	// checks for multipart shapes
	if ( curShape->GetPartCount() > 1 )
	{
		LOGF( Error ) << "Rivers module accepts only shapefiles containing non-multipart polygons.";
		m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
		m_logFile << "Rivers module accepts only shapefiles containing non-multipart polygons." << endl;
		return;
	}

	// checks for known shapes
	const IShapeExtra* extra = curShape->GetInterface< IShapeExtra >();
	if ( !extra ) 
	{
		LOGF( Error ) << "Found unknown shape in shapefile. Cannot get type of that shape.";
		m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
		m_logFile << "Cannot get type of that shape." << endl;
		return;
	}

	// checks for polygons 
	String shapeType = extra->GetShapeTypeName();
	if ( shapeType != String( SHAPE_NAME_POLYGON ) ) 
	{
		LOGF( Error ) << "Rivers module accepts only shapefiles containing non-multipart polygons.";
		m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
		m_logFile << "Rivers module accepts only shapefiles containing non-multipart polygons." << endl;
		return;
	}

  // copies shape to polygon 
	CGPolygon2 contour;
	for ( size_t i = 0, cntI = curShape->GetVertexCount(); i < cntI; ++i )
	{
		const Shapes::DVertex& v = curShape->GetVertex( i );
		contour.AppendVertex( v.x, v.y );
	}

	// pre-processes contour
	contour.RemoveConsecutiveDuplicatedVertices( TOLERANCE_CM );

	if ( contour.VerticesCount() < 3 )
	{
		LOGF( Error ) << "Skipped shape: found degenerate polygon (vertices count).";
		m_logFile << "WARNING: Skipped shape n: " << m_shapesCount << " because reduced to a degenerate polygon (vertices count)." << endl << endl;
		return;
	}

	Float area = contour.Area();
	if ( area < TOLERANCE_DM )
	{
		LOGF( Error ) << "Skipped shape: found degenerate polygon (area).";
		m_logFile << "WARNING: Skipped shape n: " << m_shapesCount << " because reduced to a degenerate polygon (area)." << endl << endl;
		return;
	}

  contour.Triangulate();

  DWORD startingTime = GetTickCount();
  UpdateContainmentMatrix( contour );
	DWORD updateContainmentMatrixTime = GetTickCount();

	m_logFile << "-------------------------------------------------------------" << endl;
	m_logFile << "Time statistics - Shape n. " << m_shapesCount << endl;
	m_logFile << "-------------------------------------------------------------" << endl;
	m_logFile << "Containment Matrix update: " << updateContainmentMatrixTime - startingTime << " ms" << endl;
	m_logFile << "-------------------------------------------------------------" << endl << endl;
}

//-----------------------------------------------------------------------------

void CRivers::OnEndPass( IMainCommands* cmdIfc )
{
  DWORD startingTime = GetTickCount();
  CalculateRiverBed();
	DWORD calculateRiverBedTime = GetTickCount();
  CalculateWaterSurface();
	DWORD calculateWaterSurfaceTime = GetTickCount();

	m_logFile << "-------------------------------------------------------------" << endl;
	m_logFile << "Time statistics" << endl;
	m_logFile << "-------------------------------------------------------------" << endl;
	m_logFile << "River Bed calculation:     " << calculateRiverBedTime - startingTime << " ms" << endl;
	m_logFile << "Water Surface calculation: " << calculateWaterSurfaceTime - calculateRiverBedTime << " ms" << endl;
	m_logFile << "-------------------------------------------------------------" << endl << endl;

  const Elevations& hmData = m_terrainIn.GetHmData();
  Elevations terrainAndWaterLayer = hmData;

  int sizeU = (int)m_waterSurface.getSizeU();
  int sizeV = (int)m_waterSurface.getSizeV();

  for ( int v = 0; v < sizeV; ++v )
  {
    for ( int u = 0; u < sizeU; ++u )
    {
      if ( m_containmentMatrix[v * sizeU + u] )
        terrainAndWaterLayer.setWorldWAt( u, v, m_waterSurface.getWorldWAt( u, v ) );
    }
  }

  String filename = String( m_terrainIn.GetFileName().c_str() );
  filename = filename.Substring( 0, filename.GetLength() - 4 );

  int lastSlashPos   = filename.ReverseFind( '\\' );
  String path        = filename.Substring( 0, lastSlashPos + 1 );
  String oldFilename = filename.Substring( lastSlashPos + 1, filename.GetLength() );

  // currently the module exports to asc file only
  String terrainFilename         = path + m_filePrefix + oldFilename + "_terrain"; 
  String waterFilename           = path + m_filePrefix + oldFilename + "_water"; 
  String terrainAndWaterFilename = path + m_filePrefix + oldFilename + "_terrain_water"; 

  m_riverBed.saveAsWorldToARCINFOASCIIGrid( (terrainFilename + ".asc").Data() );
  m_waterSurface.saveAsWorldToARCINFOASCIIGrid( (waterFilename + ".asc").Data() );
  terrainAndWaterLayer.saveAsWorldToARCINFOASCIIGrid( (terrainAndWaterFilename + ".asc").Data() );

  // copies the prj file if present
  String prjFilename = filename + ".prj";
	ifstream test( prjFilename, std::ios::binary );
	if ( !test.fail() )
	{
    test.close();

		const int BUFFER_SIZE = 4096;
		char buffer[BUFFER_SIZE];

    ifstream ifs( prjFilename, std::ios::binary );
    ofstream ofs_terrain( terrainFilename + ".prj", std::ios::binary );
    ofstream ofs_water( waterFilename + ".prj", std::ios::binary );
    ofstream ofs_both( terrainAndWaterFilename + ".prj", std::ios::binary );

		while ( !ifs.eof() ) 
		{
			ifs._Read_s( buffer, BUFFER_SIZE, BUFFER_SIZE );
			if ( !ifs.bad() ) 
			{
				ofs_terrain.write( buffer, ifs.gcount() );
				ofs_water.write( buffer, ifs.gcount() );
				ofs_both.write( buffer, ifs.gcount() );
			}
		}

		ifs.close();
		ofs_terrain.close();
		ofs_water.close();
		ofs_both.close();
  }
}

//-----------------------------------------------------------------------------

bool CRivers::GetTerrainParameters( IMainCommands* cmdIfc, LandBuilder2::ParamParser& params )
{
  // gets demType from cfg file
  string demType = "";
	try 
	{
		demType = params.GetTextValue( DEM_TYPE );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
			LOGF( Error ) << "ERROR: Missing \"" << DEM_TYPE << "\" field in class Parameters.";
			m_logFile << "ERROR: Missing \"" << DEM_TYPE << "\" field in class Parameters." << endl;
      return (false);
	}

  m_terrainIn.SetDemType( demType );

  // checks demType
  if ( (demType != S_DTED2) && 
       (demType != S_USGSDEM) && 
       (demType != S_ARCINFOASCII) && 
       (demType != S_XYZ) )
  {
		LOGF( Error ) << "ERROR: Unknown dem type specified in \"" << DEM_TYPE << "\" field in class Parameters.";
		m_logFile << "ERROR: Unknown dem type specified in \"" << DEM_TYPE << "\" field in class Parameters." << endl;
	  return (false);
  }

  // gets filename from cfg file
  string filename = "";
	try 
	{
		filename = params.GetTextValue( FILENAME );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
			LOGF( Error ) << "ERROR: Missing \"" << FILENAME << "\" field in class Parameters.";
			m_logFile << "ERROR: Missing \"" << FILENAME << "\" field in class Parameters." << endl;
      return (false);
	}

  // checks file extension
  if ( demType == S_DTED2 ) 
  {
    String tmpFileName = filename.c_str();
	  tmpFileName.Upper();

	  if ( !(tmpFileName.Find( ".DT2" ) == tmpFileName.GetLength() - 4) )
	  {
			LOGF( Error ) << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .dt2";
			m_logFile << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .dt2" << endl;
		  return (false);
	  }
  }
  else if ( demType == S_USGSDEM ) 
  {
	  String tmpFileName = filename.c_str();
	  tmpFileName.Upper();

	  if ( !(tmpFileName.Find( ".DEM" ) == tmpFileName.GetLength() - 4) )
	  {
			LOGF( Error ) << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .dem";
			m_logFile << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .dem" << endl;
		  return (false);
	  }
  }
  else if ( demType == S_ARCINFOASCII ) 
  {
	  String tmpFileName = filename.c_str();
	  tmpFileName.Upper();

	  if ( (!(tmpFileName.Find( ".GRD" ) == tmpFileName.GetLength() - 4)) && 
         (!(tmpFileName.Find( ".ASC" ) == tmpFileName.GetLength() - 4)) )
	  {
			LOGF( Error ) << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .asc or .grd";
			m_logFile << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .asc or .grd" << endl;
		  return (false);
	  }
  }
  else if ( demType == S_XYZ ) 
  {
	  String tmpFileName = filename.c_str();
	  tmpFileName.Upper();

	  if ( !(tmpFileName.Find( ".XYZ" ) == tmpFileName.GetLength() - 4) )
	  {
			LOGF( Error ) << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .xyz";
			m_logFile << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .xyz" << endl;
		  return (false);
	  }
  }

	ifstream in( filename.c_str(), std::ios::in );
	if ( in.fail() )
	{
		LOGF( Error ) << "ERROR: unable to open the file \"" << filename;
    m_logFile << "ERROR: unable to open the  \"" << filename << endl;
		return (false);
	}
	else
		in.close();

  m_terrainIn.SetFileName( filename );

  // gets srcLeft from cfg file
  size_t srcLeft = 0;
	try
	{
		srcLeft = params.GetIntValue( SRC_LEFT );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << SRC_LEFT << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << SRC_LEFT << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets srcRight from cfg file
  size_t srcRight = 0;
	try
	{
		srcRight = params.GetIntValue( SRC_RIGHT );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << SRC_RIGHT << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << SRC_RIGHT << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets srcTop from cfg file
  size_t srcTop = 0;
	try
	{
		srcTop = params.GetIntValue( SRC_TOP );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << SRC_TOP << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << SRC_TOP << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets srcBottom from cfg file
  size_t srcBottom = 0;
	try
	{
		srcBottom = params.GetIntValue( SRC_BOTTOM );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << SRC_BOTTOM << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << SRC_BOTTOM << "\" field in class Parameters." << endl;
    return (false);
	}

  m_terrainIn.SetSrcLeft( srcLeft );
  m_terrainIn.SetSrcRight( srcRight );
  m_terrainIn.SetSrcTop( srcTop );
  m_terrainIn.SetSrcBottom( srcBottom );

  // gets dstLeft from cfg file
  double dstLeft = CGConsts::ZERO;
	try
	{
		dstLeft = static_cast< double >( params.GetFloatValue( DST_LEFT ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << DST_LEFT << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << DST_LEFT << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets dstRight from cfg file
  double dstRight = CGConsts::ZERO;
	try
	{
		dstRight = static_cast< double >( params.GetFloatValue( DST_RIGHT ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << DST_RIGHT << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << DST_RIGHT << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets dstTop from cfg file
  double dstTop = CGConsts::ZERO;
	try
	{
		dstTop = static_cast< double >( params.GetFloatValue( DST_TOP ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << DST_TOP << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << DST_TOP << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets dstBottom from cfg file
  double dstBottom = CGConsts::ZERO;
	try
	{
		dstBottom = static_cast< double >( params.GetFloatValue( DST_BOTTOM ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << DST_BOTTOM << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << DST_BOTTOM << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets noWaterElev from cfg file
  double noWaterElevation = CGConsts::ZERO;
	try
	{
    noWaterElevation = static_cast< double >( params.GetFloatValue( NO_WATER_ELEV ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << NO_WATER_ELEV << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << NO_WATER_ELEV << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets waterDepth from cfg file
  double waterDepth = CGConsts::ZERO;
	try
	{
    waterDepth = abs( static_cast< double >( params.GetFloatValue( WATER_DEPTH ) ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << WATER_DEPTH << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << WATER_DEPTH << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets terrainDepth from cfg file
  double terrainDepth = CGConsts::ZERO;
// <<<<<<<<<<<< 1.1.8 <<<<<<<<<<<<<<<<<<<<<<<<<<
  bool foundTerrainDepth = false;
// <<<<<<<<<<<< 1.1.8 <<<<<<<<<<<<<<<<<<<<<<<<<<
	try
	{
    terrainDepth = abs( static_cast< double >( params.GetFloatValue( TERRAIN_DEPTH ) ) );
    foundTerrainDepth = true;
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
// <<<<<<<<<<<< 1.1.8 <<<<<<<<<<<<<<<<<<<<<<<<<<
//		LOGF( Error ) << "ERROR: Missing \"" << TERRAIN_DEPTH << "\" field in class Parameters.";
//		m_logFile << "ERROR: Missing \"" << TERRAIN_DEPTH << "\" field in class Parameters." << endl;
//    return (false);
		m_logFile << "ERROR: Missing \"" << TERRAIN_DEPTH << "\" field in class Parameters." << endl;
		m_logFile << "       Applying backward compatibility." << endl;
    terrainDepth = waterDepth;
    waterDepth = terrainDepth - 1.0;
// <<<<<<<<<<<< 1.1.8 <<<<<<<<<<<<<<<<<<<<<<<<<<
	}

  // gets applySmooth from cfg file
  bool applySmooth = true;
  try 
  {
    String apply = params.GetTextValue( APPLY_SMOOTH );
    if ( apply == "0" ) 
      applySmooth = false; 
  }
  catch ( LandBuilder2::ParamParserException& ) 
  {
    m_logFile << "WARNING: Missing \"" << APPLY_SMOOTH << "\" field in class Parameters." << endl;
    m_logFile << "         Assuming \"" << applySmooth << "\" as default." << endl;
  }

  m_terrainIn.SetDstLeft( dstLeft );
  m_terrainIn.SetDstRight( dstRight );
  m_terrainIn.SetDstTop( dstTop );
  m_terrainIn.SetDstBottom( dstBottom );
  m_terrainIn.SetNoWaterElevation( noWaterElevation );
  m_terrainIn.SetWaterDepth( waterDepth );
  m_terrainIn.SetTerrainDepth( terrainDepth );
  m_terrainIn.SetApplySmooth( applySmooth );

  return (true);
}

//-----------------------------------------------------------------------------

bool CRivers::LoadHighMap()
{
	Elevations hmData;

  string demType = m_terrainIn.GetDemType();

	if ( demType == S_DTED2 )
	{
		if ( !(hmData.loadFromDT2( m_terrainIn.GetFileName() )) )
		{
      LOGF( Error ) << ">>> Error while loading DTED2 file <<<";
		  m_logFile << ">>> Error while loading DTED2 file <<<" << endl;
			return (false);
		}
	}
	else if ( demType == S_USGSDEM )
	{
		if ( !(hmData.loadFromUSGSDEM( m_terrainIn.GetFileName() )) )
		{
      LOGF( Error ) << ">>> Error while loading USGS DEM file <<<";
		  m_logFile << ">>> Error while loading USGS DEM file <<<" << endl;
			return (false);
		}
		else
		{
			// if the elevation units in the DEM are feet, converts to METERS
			if ( hmData.getWorldUnitsW() == Elevations::euFEET )
				hmData.convertWorldUnitsW( Elevations::euMETERS );
		}
	}
	else if ( demType == S_ARCINFOASCII )
	{
		if ( !(hmData.loadFromARCINFOASCIIGrid( m_terrainIn.GetFileName() )) )
		{
      LOGF( Error ) << ">>> Error while loading Arc/Info Ascii Grid file <<<";
		  m_logFile << ">>> Error while loading Arc/Info Ascii Grid file <<<" << endl;
			return (false);
		}
	}
  else if ( demType == S_XYZ )
	{
		if ( !(hmData.loadFromXYZ( m_terrainIn.GetFileName() )) )
		{
      LOGF( Error ) << ">>> Error while loading xyz file <<<";
		  m_logFile << ">>> Error while loading xyz file <<<" << endl;
			return (false);
		}
	}

	m_terrainIn.SetHmData( hmData );

	return (true);
}

//-----------------------------------------------------------------------------

void CRivers::UpdateContainmentMatrix( const CGPolygon2& riverContour )
{
  size_t sizeU = m_riverBed.getSizeU();
  size_t sizeV = m_riverBed.getSizeV();
  Float resolutionU = m_riverBed.getResolutionU();
  Float resolutionV = m_riverBed.getResolutionV();

  const vector< CGTriangle2 >& triangulation = riverContour.GetTriangulation().GetTrianglesList();
  for ( size_t i = 0, cntI = triangulation.size(); i < cntI; ++i )
  {
    const CGTriangle2& triangle = triangulation[i];
    Float left   = triangle.MinX();
    Float right  = triangle.MaxX();
    Float top    = triangle.MaxY();
    Float bottom = triangle.MinY();

    m_maxTriSize = std::max( m_maxTriSize, (right - left) / resolutionU );
    m_maxTriSize = std::max( m_maxTriSize, (top - bottom) / resolutionV );

    Elevations subGrid = m_riverBed.subGrid( left, bottom, right, top ); 
    size_t triSizeU = subGrid.getSizeU();
    size_t triSizeV = subGrid.getSizeV();
    size_t offsetU = static_cast< size_t >( (subGrid.getOriginWorldU() - m_riverBed.getOriginWorldU()) / resolutionU );
    size_t offsetV = static_cast< size_t >( (subGrid.getOriginWorldV() - m_riverBed.getOriginWorldV()) / resolutionV );

    if ( (triSizeU > 0) && (triSizeV > 0) )
    {
      for ( size_t u = 0; u < triSizeU; ++u )
      {
        Float x = subGrid.getWorldUAt( u );
        for ( size_t v = 0; v < triSizeV; ++v )
        {
          int index = (u + offsetU) + (v + offsetV) * sizeU;
          if ( !m_containmentMatrix[index] )
          {
            Float y = subGrid.getWorldVAt( v );
            if ( triangle.Contains( x, y, true ) )
              m_containmentMatrix[(u + offsetU) + (v + offsetV) * sizeU] = true;
          }
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------

void CRivers::CalculateRiverBed()
{
  // the river bed is generated by pushing down the terrain vertices
  // contained into the polygons by a fixed amount and applying a smoothing filter

  if ( !m_containmentMatrix ) 
    return; 

  size_t sizeU = m_riverBed.getSizeU();
  size_t sizeV = m_riverBed.getSizeV();

  const Elevations& hmData = m_terrainIn.GetHmData();

  for ( size_t v = 0; v < sizeV; ++v )
  {
    for ( size_t u = 0; u < sizeU; ++u )
    {
      if ( m_containmentMatrix[v * sizeU + u] )
        m_riverBed.setWorldWAt( u, v, hmData.getWorldWAt( u, v ) - m_terrainDepth );
    }
  }
}

//-----------------------------------------------------------------------------

void CRivers::CalculateWaterSurface()
{
  // sets water surface inside the polygons
  if ( !m_containmentMatrix ) 
    return; 

  int sizeU = (int)m_waterSurface.getSizeU();
  int sizeV = (int)m_waterSurface.getSizeV();

  const Elevations& hmData = m_terrainIn.GetHmData();

  for ( int v = 0; v < sizeV; ++v )
  {
    for ( int u = 0; u < sizeU; ++u )
    {
      if ( m_containmentMatrix[v * sizeU + u] )
        m_waterSurface.setWorldWAt( u, v, hmData.getWorldWAt( u, v ) - m_terrainDepth + m_waterDepth );
    }
  }

  // sets water surface at the polygons' boundaries
  for ( int v = 0; v < sizeV; ++v )
  {
    for ( int u = 0; u < sizeU; ++u )
    {
      Float correction = CGConsts::ZERO;
      size_t correctionsCount = 0;
      if ( !m_containmentMatrix[v * sizeU + u] )
      {
        if ( u > 0 )
        {
          if ( (v > 0) && (m_containmentMatrix[(v - 1) * sizeU + (u - 1)]) )
          {
            correction += m_waterSurface.getWorldWAt( u - 1, v - 1 );
            ++correctionsCount;
          }

          if ( m_containmentMatrix[v * sizeU + (u - 1)] )
          {
            correction += m_waterSurface.getWorldWAt( u - 1, v );
            ++correctionsCount;
          }

          if ( (v < sizeV - 1) && (m_containmentMatrix[(v + 1) * sizeU + (u - 1)]) )
          {
            correction += m_waterSurface.getWorldWAt( u - 1, v + 1 );
            ++correctionsCount;
          }
        }

        if ( (v > 0) && (m_containmentMatrix[(v - 1) * sizeU + u]) )
        {
          correction += m_waterSurface.getWorldWAt( u, v - 1 );
          ++correctionsCount;
        }

        if ( (v < sizeV - 1) && (m_containmentMatrix[(v + 1) * sizeU + u]) )
        {
          correction += m_waterSurface.getWorldWAt( u, v + 1 );
          ++correctionsCount;
        }

        if ( u < sizeU - 1 )
        {
          if ( (v > 0) && (m_containmentMatrix[(v - 1) * sizeU + (u + 1)]) )
          {
            correction += m_waterSurface.getWorldWAt( u + 1, v - 1 );
            ++correctionsCount;
          }

          if ( m_containmentMatrix[v * sizeU + (u + 1)] )
          {
            correction += m_waterSurface.getWorldWAt( u + 1, v );
            ++correctionsCount;
          }

          if ( (v < sizeV - 1) && (m_containmentMatrix[(v + 1) * sizeU + (u + 1)]) )
          {
            correction += m_waterSurface.getWorldWAt( u + 1, v + 1 );
            ++correctionsCount;
          }
        }
      }

      if ( correctionsCount > 0 )
        m_waterSurface.setWorldWAt( u, v, correction / (Float)correctionsCount );
    }
  }

  // smoothes if requested
  if ( m_terrainIn.GetApplySmooth() )
  {
//    if ( m_maxTriSize > 7 )
//      m_maxTriSize = 7;

    for ( size_t i = 3; i < (size_t)m_maxTriSize; i += 2 )
    {
      CGaussianFilter filter( i );
      for ( int v = 0; v < sizeV; ++v )
      {
        for ( int u = 0; u < sizeU; ++u )
        {        
          Float newElevation = CGConsts::ZERO;

          if ( abs( m_waterSurface.getWorldWAt( u, v ) - m_noWaterElevation ) > 0.001 )
          {
            Float weight = CGConsts::ZERO;

            int halfFilterSize = (int)filter.GetSize() / 2;

            for ( int x = -halfFilterSize; x <= halfFilterSize; ++x )
            {
              for ( int y = -halfFilterSize; y <= halfFilterSize; ++y )
              {
                int uu = u + x;
                int vv = v + y;
                if ( (0 <= uu) && (uu < sizeU) )
                {
                  if ( (0 <= vv) && (vv < sizeV) )
                  {
                    Float tempElevation = m_waterSurface.getWorldWAt( uu, vv );
                    if ( abs( tempElevation - m_noWaterElevation ) > 0.001 ) 
                    {
                      Float currWeight = filter.ValueAt( x, y );
                      weight += currWeight;
                      newElevation += currWeight * tempElevation;
                    }
                  }
                }
              }
            }

            if ( weight > CGConsts::ZERO )
            {
              newElevation /= weight;
              m_waterSurface.setWorldWAt( u, v, newElevation );
            }
          }
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------
