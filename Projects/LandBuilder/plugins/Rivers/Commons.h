//-----------------------------------------------------------------------------
// File: Commons.h
//
// Desc: common data, definitions and functions
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <fstream>
#include <vector>

//-----------------------------------------------------------------------------

#include "EtRectElevationGrid.h"
#include "DXF2007Exporter.h"

#include "CG_Polyline2.h"
#include "CG_Polygon2.h"

//-----------------------------------------------------------------------------

using std::endl;
using std::ofstream;
using std::ifstream;
using std::vector;

//-----------------------------------------------------------------------------

typedef RString String;
typedef double Float;

//-----------------------------------------------------------------------------

typedef EtRectElevationGrid< Float, int > Elevations;

//-----------------------------------------------------------------------------

typedef CG_Consts< Float >     CGConsts;
typedef CG_Line2< Float >      CGLine2;
typedef CG_Point2< Float >     CGPoint2;
typedef CG_Polyline2< Float >  CGPolyline2;
typedef CG_Polygon2< Float >   CGPolygon2;
typedef CG_Rectangle2< Float > CGRectangle2;
typedef CG_Triangle2< Float >  CGTriangle2;
typedef CG_Vector2< Float >    CGVector2;

//-----------------------------------------------------------------------------

#define TOLERANCE_MM 0.001
#define TOLERANCE_CM 0.01
#define TOLERANCE_DM 0.1

//-----------------------------------------------------------------------------
