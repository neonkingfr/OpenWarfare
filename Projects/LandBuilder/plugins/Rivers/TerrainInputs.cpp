//-----------------------------------------------------------------------------
// File: TerrainInputs.cpp
//
// Desc: class related to terrain data import
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "TerrainInputs.h"

//-----------------------------------------------------------------------------

CTerrainInputs::CTerrainInputs() 
{
  // do nothing
}

//-----------------------------------------------------------------------------

CTerrainInputs::CTerrainInputs( string demType, string filename, size_t srcLeft, size_t srcTop,
                                size_t srcRight, size_t srcBottom, double dstLeft, double dstTop,
                                double dstRight, double dstBottom, double noWaterElevation,
                                double waterDepth, double terrainDepth, bool applySmooth )
: MapInput( demType, filename, srcLeft, srcTop, srcRight, srcBottom, dstLeft, dstTop,
            dstRight, dstBottom )
, m_noWaterElevation( noWaterElevation )
, m_waterDepth( waterDepth )
, m_terrainDepth( terrainDepth )
, m_applySmooth( applySmooth )
{
  // do nothing
}

//-----------------------------------------------------------------------------
