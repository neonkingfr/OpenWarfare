//-----------------------------------------------------------------------------
// File: GaussianFilter.h
//
// Desc: class for smooth with gaussian filter
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "GaussianFilter.h"

//-----------------------------------------------------------------------------

CGaussianFilter::CGaussianFilter( size_t size )
: m_size( size )
, m_filter( NULL )
{
  // we use odd sizes only
  if ( m_size % 2 == 0 ) { ++m_size; }
  if ( m_size < 3 )      { m_size = 3; }
  BuildFilter();
}

//-----------------------------------------------------------------------------

CGaussianFilter::~CGaussianFilter()
{
  if ( m_filter ) { delete m_filter; }
}

//-----------------------------------------------------------------------------

Float CGaussianFilter::ValueAt( int x, int y ) const
{
  int halfSize = m_size / 2;

  if ( x < -halfSize ) { return (CGConsts::ZERO); }
  if ( y < -halfSize ) { return (CGConsts::ZERO); }
  if ( x > halfSize )  { return (CGConsts::ZERO); }
  if ( y > halfSize )  { return (CGConsts::ZERO); }

  size_t index = (halfSize - y) * m_size + (halfSize + x);

  assert( m_filter );
  return (m_filter[index]);
}

//-----------------------------------------------------------------------------

size_t CGaussianFilter::GetSize() const
{
  return (m_size);
}

//-----------------------------------------------------------------------------

void CGaussianFilter::BuildFilter()
{
  m_filter = new Float[m_size * m_size];

  Float fSize      = static_cast< Float >( m_size );
  Float sigma      = CGConsts::HALF * fSize / static_cast< Float >( 3 );
  Float twoSqSigma = CGConsts::TWO * sigma * sigma;
  Float expDen     = twoSqSigma;
  Float den        = twoSqSigma * CGConsts::PI;
  Float term1      = CGConsts::ONE / den;

  int halfSize = m_size / 2;

  Float x, y, expNum, term2;
  for ( size_t i = 0; i < m_size; ++i )
  {
    for ( size_t j = 0; j < m_size; ++j )
    {
      x = static_cast< Float >( i - halfSize );
      y = static_cast< Float >( halfSize - j );
      expNum = x * x + y * y;
      term2 = exp( -expNum / expDen );
      m_filter[j * m_size + i] = term1 * term2;
    }
  }
}

//-----------------------------------------------------------------------------
