//-----------------------------------------------------------------------------
// File: GaussianFilter.h
//
// Desc: class for smooth with gaussian filter
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"

//-----------------------------------------------------------------------------

class CGaussianFilter
{
  size_t m_size;
  Float* m_filter;

public:
  CGaussianFilter( size_t size = 3 );
  ~CGaussianFilter();

  Float ValueAt( int x, int y ) const;  

  size_t GetSize() const;

private:
  void BuildFilter();
};

//-----------------------------------------------------------------------------
