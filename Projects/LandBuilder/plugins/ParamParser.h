//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "ILandBuildCmds.h"

//-----------------------------------------------------------------------------

#pragma warning( disable : 4290 )

//-----------------------------------------------------------------------------

namespace LandBuilder2
{
  class ParamParserException
  {
    RString _paramName;
    RString _paramValue;

  public:
    ParamParserException( RString paramName, RString paramValue )
    : _paramName( paramName )
    , _paramValue( paramValue ) 
    {
    }

    const RString& GetName() const 
    {
      return (_paramName);
    }

    const RString& GetValue() const 
    {
      return (_paramValue);
    }

    virtual const char* Message() const = 0;
  };
    
//-----------------------------------------------------------------------------

  class ParamParserFloatExcept : public ParamParserException 
  {
  public:
    ParamParserFloatExcept( RString paramName, RString paramValue )
    : ParamParserException( paramName, paramValue ) 
    {
    }

    virtual const char* Message() const 
    {
      return ("Excepted floated point number");
    }
  };

//-----------------------------------------------------------------------------

  class ParamParserIntExcept : public ParamParserException
  {
  public:
    ParamParserIntExcept( RString paramName, RString paramValue )
    : ParamParserException( paramName, paramValue ) 
    {
    }

    virtual const char* Message() const 
    {
      return ("Excepted int number");
    }
  };

//-----------------------------------------------------------------------------

  class ParamParserMissingValue : public ParamParserException 
  {
  public:
    ParamParserMissingValue( RString paramName, RString paramValue )
    : ParamParserException( paramName, paramValue ) 
    {
    }

    virtual const char* Message() const 
    {
      return ("Missing value");
    }
  };

//-----------------------------------------------------------------------------

  class ParamParserOutOfRange : public ParamParserException 
  {
    int index;
  
  public:
    ParamParserOutOfRange( RString paramName, RString paramValue, int index )
    : ParamParserException( paramName, paramValue )
    , index( index ) 
    {
    }

    virtual const char* Message() const 
    {
      return ("Index is out of range");
    }

    int GetIndex() const 
    {
      return (index);
    }
  };

//-----------------------------------------------------------------------------

  class ParamParserInvalidValue : public ParamParserException 
  {
    RString text;
  
  public:
    ParamParserInvalidValue( RString paramName, const char* desc )
    : ParamParserException( paramName, desc ) 
    {
    }
        
    virtual const char* Message() const 
    {
      return ("Invalid value. Required");
    }
  };

//-----------------------------------------------------------------------------
  
  class ParamParser
  {
    IMainCommands* data;
  
  public:
    ParamParser( IMainCommands* data );

    const char* GetTextValue( const char* name ) throw (ParamParserException);
    int GetIntValue( const char* name ) throw (ParamParserException);
    float GetFloatValue( const char* name ) throw (ParamParserException);

    RString ListGetTextValue( const char* name, int index, char sep = ',' ) throw (ParamParserException);
    int ListGetIntValue( const char* name, int index, char sep = ',' ) throw (ParamParserException);
    float ListGetFloatValue( const char* name, int index, char sep = ',' ) throw (ParamParserException);

    AutoArray< RString > GetTextArr( const char* name, char sep = ',' ) throw (ParamParserException);
    AutoArray< int > GetIntArr( const char* name, char sep = ',' ) throw (ParamParserException);
    AutoArray< float > GetFloatArr( const char* name, char sep = ',' ) throw (ParamParserException);
  };

//-----------------------------------------------------------------------------

} // namespace LandBuilder2
