#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string>
#include <conio.h>
#include <es/common/fltopts.hpp>
#include <windows.h>
#include "VisViewerPipe.h"
#include "ViewerDiag.h"
#include <el/pathname/pathname.h>
#include <fstream>
#include <el/ParamFile/ParamFile.hpp>
#include <es/framework/appframe.hpp>
#include <Commdlg.h>

#include "LBTData.h"

#define WM_INITIATEMESSAGE (WM_APP+1)
#define WM_EXITWAIT (WM_APP+2)


class MyFrame: public AppFrameFunctions {
public:
    virtual void LogF(const char *format, va_list argptr)
    {
        static DWORD initTime = GetTickCount();
        DWORD curTime = GetTickCount();
        printf("[%10.3f]: ", (float)(curTime-initTime)/1000.0f);
        vprintf(format,argptr);
        puts("");
    }
};


MyFrame myframe;



static ViewerDiag viewerview;

LRESULT WINAPI Connector(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_INITIATEMESSAGE:
        {
            if (wParam == 0 && lParam == 0) //request for parameters
            {
                return 256*1024;
            }

            VisViewerPipe *ex=new VisViewerPipe(viewerview);
            bool res=ex->Create(wParam,lParam);
            if (res) 
            {
                IVisViewerExchange *iface = ex;
                ReplyMessage(TRUE);     //Reply to viewer - connection accepted
                PSPosMessage msg;
                int waitMsg=COM_VERSION; //wait for message protocol number
                LogF("Found viewer, waiting for initiation packet...");
                VisViewerExchange::Status wt=ex->WaitMessage(Array<int>(waitMsg),msg,10000);
                if (wt!=ex->statOk) 
                {
                    LogF("Cannot receive initiation packet - disconnecting and waiting for retry");
                    delete ex;
                    return FALSE;
                }
                ex->DispatchMessage(*msg);
                LogF("Viewer connected");
                PostMessage(hWnd,WM_EXITWAIT,0,(LPARAM)iface);
                return TRUE;
            } 
            else 
            {
                delete ex; 
                return FALSE;
            }          
        }
        break;
    case WM_CREATE:        
        SetTimer(hWnd,1,120000,0);
        return 1;
    case WM_TIMER:
        PostMessage(hWnd,WM_EXITWAIT,0,0);
        return DefWindowProc(hWnd,message,wParam,lParam);
    default:
        return DefWindowProc(hWnd,message,wParam,lParam);
    }
    return 0;
}


IVisViewerExchange *ConnectBuldozer() 
{
    WNDCLASS cls;
    ZeroMemory(&cls,sizeof(cls));
    cls.hInstance = GetModuleHandle(0);
    cls.lpfnWndProc = &Connector;
    cls.lpszClassName = "Connector";
    RegisterClass(&cls);    

    HWND hWnd = CreateWindow("Connector","Connect Buldozer",0,0,0,0,0,HWND_MESSAGE,0,GetModuleHandle(0),0);

    if (hWnd == NULL) {        
        throw std::string("Unable to create message window to accept connections");
    }

    Pathname thispath = Pathname::GetExePath(0);

    thispath.SetFilename("runviewer.bat");

    char cmdline[500];
    sprintf(cmdline,"\"-connect=pipe\\%X,%X\"",hWnd,WM_INITIATEMESSAGE);

    LogF("Starting viewer...");
    const char *dirname  = thispath.GetDirectoryWithDriveWLBS();
    int code = (int)ShellExecute(hWnd,NULL,thispath,cmdline,dirname,SW_SHOWNORMAL);
    if (code && code<32) {
        LogF("ShellExecute error %d",code);
        throw std::string("Cannot start viewer");
    }
    LogF("Waiting for viewer... (2 minutes) - Ctrl+C to exit");

    MSG msg;
    while (GetMessage(&msg,0,0,0)) {
        if (msg.message == WM_EXITWAIT) {

            if (msg.lParam != 0) {
                DestroyWindow(hWnd);
                return (IVisViewerExchange *)msg.lParam;
            } else {
                break;
            }
        } else {
            TranslateMessage(&msg);
            DispatchMessage(&msg);        
        }
    }

    DestroyWindow(hWnd);
    throw std::string("No valid response in time (timeout) ");    
}


static bool AskFilename(char *buff, int size) {

    OPENFILENAME ofn;
    ZeroMemory(&ofn,sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.lpstrFile = buff;
    ofn.nMaxFile = size;
    ofn.hwndOwner = GetForegroundWindow();
    ofn.lpstrTitle = "Enter or choose export file";
    ofn.lpstrFilter = "Land file\0*.wrp\0All files\0*.*\0";
    ofn.lpstrDefExt = "wrp";
    ofn.Flags = OFN_PATHMUSTEXIST|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT|OFN_ENABLESIZING|
        OFN_NOREADONLYRETURN;

    return  (GetSaveFileName(&ofn) != FALSE);
}

int run(bool interactive, const char *configfile, const char *lbtfile, const char *exportFile) 
{

    IVisViewerExchange *ifc;
    {

        LogF("Land Config: %s", configfile);
        LogF("Source LBT: %s", lbtfile);

        LogF("Loading the land configuration");

        ParamFile pfile;
        LSError err = pfile.Parse(configfile);
        if (err != 0) throw err;

        ParamEntryPtr entry = pfile.FindEntry("LandInfo");
        if (entry.IsNull())
            throw std::string("Missing LandInfo section in config");
        if (!entry->IsClass())
            throw std::string("LandInfo must be class type");

        ParamClassPtr landInfo = entry->GetClassInterface();

        entry = landInfo->FindEntry("SystemInit");
        if (entry.IsNull())
            throw std::string("Missing SystemInit section in config");
        if (!entry->IsClass())
            throw std::string("SystemInit must be class type");

        ParamClassPtr systemInit = entry->GetClassInterface();

        SLandscapePosMessData landScapeInfo;

        entry = systemInit->FindEntry("textureRange");
        if (entry.IsNull() || !entry->IsArray() || entry->GetSize()!=2) 
            throw std::string("textureRange must be array with two fields");

        landScapeInfo.textureRangeX = entry->operator [](0).GetInt();
        landScapeInfo.textureRangeY = entry->operator [](1).GetInt();

        entry = systemInit->FindEntry("landRange");
        if (entry.IsNull() || !entry->IsArray() || entry->GetSize()!=2) 
            throw std::string("landRange must be array with two fields");

        landScapeInfo.landRangeX= entry->operator [](0).GetInt();
        landScapeInfo.landRangeY = entry->operator [](1).GetInt();

        entry = systemInit->FindEntry("landGrid");
        if (entry.IsNull() || !entry->IsArray() || entry->GetSize()!=2) 
            throw std::string("landGrid must be array with two fields");

        landScapeInfo.landGridX= entry->operator [](0).GetFloat();
        landScapeInfo.landGridY = entry->operator [](1).GetFloat();

        RString configName;
        entry = landInfo->FindEntry("configName");            
        if (entry.NotNull()) configName = entry->GetValue();

        entry = landInfo->FindEntry("Textures");
        ParamClassPtr textureList = (entry.IsNull() || !entry->IsClass())?0:entry->GetClassInterface();
        entry = landInfo->FindEntry("textureMap");
        ParamEntryPtr textureMap = (entry.IsNull() || !entry->IsArray())?0:entry;
        entry = landInfo->FindEntry("heightMap");
        ParamEntryPtr terrian = (entry.IsNull() || !entry->IsArray())?0:entry;


        LogF("Loading LBT data");


        LBTData lbdata(textureMap,textureList,landScapeInfo,terrian);
        {
            std::ifstream input(lbtfile,std::ios::in);
            if (!input)
                throw std::string("Unable to open LBT file: ") + lbtfile;

            lbdata.Load(input);
        }

        ifc = ConnectBuldozer();   

        ifc -> SystemInit(landScapeInfo, configName);


        LogF("Transfering ... (progress screen should appear)");
        lbdata.SendToViewer(ifc);
        LogF("Data has been transfered");


        class ViewerClose {
            IVisViewerExchange *ifc;
        public:
            ViewerClose (IVisViewerExchange *ifc):ifc(ifc) {}
            ~ViewerClose ()
            {
                LogF("Closing viewer");
                ifc->SystemQuit();
                delete ifc;
            }
        };

        ViewerClose _(ifc);
        if (!interactive && exportFile) {
            LogF("Exporting to %s", exportFile);
            ifc->FileExport(exportFile);
        }
        else {

            LogF("Entering to interactive mode");
            puts("Esc - quit interactive mode");
            puts("Enter - Export land");

            bool rep;
            do {
                while (_kbhit() == 0) {
                    VisViewerPipe::Status st = ifc->CheckInterfaceStatus();
                    if (st == VisViewerPipe::statLost)
                        throw std::string("Connection has been lost");
                    else if (st == VisViewerPipe::statError)
                        throw std::string("Connection error");
                    Sleep(250);
                }
                int c = _getch();
                rep = c != 27;
                if (c == 13) {
                    char buff[MAX_PATH];
                    if (exportFile) strcpy_s(buff,MAX_PATH,exportFile); else buff[0]=0;
                    if (AskFilename(buff,MAX_PATH)==true) {
                        LogF("Exporting to %s", buff);
                        ifc->FileExport(buff);
                    }
                }

            } while (rep);

        }
        LogF("Releasing memory...");
    }
    return 0;
}  

int main(int argc, char **argv) 
{

    CurrentAppFrameFunctions = &myframe;;


    if (argc < 2) {
        puts("Usage:");
        puts("LBTBuldoView [-x] <lbt file>");
        puts("LBTBuldoView [-x] <lbt file> <landconfig>");
        puts("LBTBuldoView [-x] <lbt file> <landconfig> <output>");
        puts("\n"
            "<lbt file>     name of LBT file to import\n\n"
            "<landconfig>   name of config file captured from Visitor\n"
            "               Use CaptureLandInfo as viewer to create config\n"
            "               If parameter skipped, lbt pathname with \n"
            "               .cfg extension will be used\n\n"
            "<output>       specifies export file\n"
            "               If parameter skipped, lbt pathname with \n"
            "               .wrp extension will be used\n\n"
            "-x             if specified, program will not run in \n"
            "               interactive mode");
        return -1;
    }

    try {

        bool interactive;

        if (_stricmp(argv[1], "-x")==0) {
            interactive = false;
            argc--;
            argv++;
        }
        else
            interactive = true;

        if (argc == 2) {

            Pathname cfg = argv[1];
            cfg.SetExtension(".cfg");
            Pathname out = argv[1];
            out.SetExtension(".wrp");
            run(interactive,cfg,argv[1],out);

        } else if (argc == 3) {

            Pathname out = argv[1];
            out.SetExtension(".wrp");
            run(interactive,argv[2],argv[1],out);

        } else if (argc == 4) {

            run(interactive,argv[2],argv[1],argv[3]);
        } else
            throw std::string("Incorrect parameter count");

    } catch (std::string &e) {
        fprintf(stderr,"Error: %s \n", e.c_str());
    } catch (LSError &e) {
        fprintf(stderr,"Param File Error: %d \n", e);
    };

}    