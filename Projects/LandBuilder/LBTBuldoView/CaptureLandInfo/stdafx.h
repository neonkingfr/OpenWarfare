// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <Es/containers/array.hpp>
#include "messagedll.h"
#include <es/types/pointers.hpp>
#include <VisViewerPipe.h>
#include <fstream>
#include <Commdlg.h>



// TODO: reference additional headers your program requires here
