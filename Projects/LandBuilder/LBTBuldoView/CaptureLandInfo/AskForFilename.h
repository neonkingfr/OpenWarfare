#pragma once

static bool AskFilename(char *buff, int size) {

    OPENFILENAME ofn;
    ZeroMemory(&ofn,sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    buff[0]=0;
    ofn.lpstrFile = buff;
    ofn.nMaxFile = size;
    ofn.hwndOwner = GetForegroundWindow();
    ofn.lpstrTitle = "Enter or choose capture file";
    ofn.lpstrFilter = "Land-Info captured data file\0*.*\0";
    ofn.lpstrDefExt = "cfg";
    ofn.Flags = OFN_PATHMUSTEXIST|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT|OFN_ENABLESIZING|
                OFN_NOREADONLYRETURN;

    return  (GetSaveFileName(&ofn) != FALSE);
}
