#pragma once
#include <vector>

class IVisViewerExchange;

struct LBTObject{
    float mx[4][4];
    std::string name;
};

typedef std::vector<LBTObject> LBTObjectList;

struct LBTerrain {

    int width;
    int height;
    float *data;

    LBTerrain(int width, int height):
            width(width),
            height(height),
            data(new float[width*height]) 
            {
                for (int i = 0, cnt = height*width; i<cnt; i++)
                    data[i] = 10.0f;
            }
    ~LBTerrain() {delete [] data;}

    float CalculateHeight(float x, float z);


    float *operator[](int line) {return data + width * line;}
};

class LBTData
{
    LBTObjectList objects;
    LBTerrain terrain; 
    ParamEntryPtr textureMap;
    ParamClassPtr textureList;
    SLandscapePosMessData landscape;    
public:
    LBTData(ParamEntryPtr textureMap,ParamClassPtr textureList,
            const SLandscapePosMessData &landscape, ParamEntryPtr terrain);
    ~LBTData(void);

    void Load(std::istream &infile);
    void SendToViewer(IVisViewerExchange *ifc);
protected:
    void RegisterExplicitBitmaps(IVisViewerExchange *ifc);
    void SendHeightMap(IVisViewerExchange *ifc);
    void SendExplicitBitmaps(IVisViewerExchange *ifc);
    void RegisterObjects(IVisViewerExchange *ifc);
    void SendObjects(IVisViewerExchange *ifc);
    int GetCountOfRegisteredObjects();

};
