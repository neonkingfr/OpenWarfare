
// ViewerSim.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


void LogF(char const *str,...) 
{
    static DWORD initTime = GetTickCount();
    DWORD curTime = GetTickCount();
    va_list args;
    va_start(args,str);
    printf("[%10.3f]: ", (float)(curTime-initTime)/1000.0f);
    vprintf(str,args);
    puts("");
}

int _tmain(int argc, _TCHAR* argv[])
{
    printf("%s",GetCommandLine());


    if (argc < 2 || _strnicmp(argv[1],"-connect=pipe\\",14)!=0) {        
        puts("Needs one parameter - buldozer connection string");
        return -1;
    }
        
    ViewerDiag diag;
    VisViewerPipe pipe(diag);
    bool res = pipe.Create(argv[1]+14);

    if (res == false) {
        puts("Pipe creation failed");
    }

    LogF("Connection estabilished");

    pipe.ComVersion(MSG_VERSION);

    while (!diag.quitMessage) {
        pipe.CheckInterfaceStatus();
        Sleep(100);
    }

	return 0;
}

