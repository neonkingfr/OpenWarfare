///-----------------------------------------------------------------------------
// File: CG_Polygon2.inl
//
// Desc: A two dimensional polygon
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "CG_Triangle2.h"
#include "CG_Vector2.h"
#include "CG_Triangle16Wrapper.h"
#include "CG_Polyline2.h"

#include "TPriorityQueue.h"

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real >::PVertex::PVertex()
: CG_Point2< Real >()
, m_isConvex( true )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real >::PVertex::PVertex( Real x, Real y )
: CG_Point2< Real >( x, y )
, m_isConvex( true )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real >::PVertex::PVertex( const typename CG_Polygon2< Real >::PVertex& other )
: CG_Point2< Real >( other )
, m_isConvex( other.m_isConvex )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real >::PVertex::~PVertex()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
typename CG_Polygon2< Real >::PVertex& CG_Polygon2< Real >::PVertex::operator = ( const PVertex& other )
{
  CG_Point2< Real >::operator = ( other );
  m_isConvex = other.m_isConvex;
  return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::PVertex::IsConvex() const
{
  return (m_isConvex);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::PVertex::SetConvex( bool convex )
{
  m_isConvex = convex;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real >::CG_Polygon2()
: m_type( PolyType_NORMAL )
, m_triangulation( NULL )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real >::CG_Polygon2( const CG_Polygon2< Real >& other )
: m_type( other.m_type )
, m_vertices( other.m_vertices )
, m_triangulation( NULL )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real >::~CG_Polygon2()
{
  if (m_triangulation) delete m_triangulation;
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real >& CG_Polygon2< Real >::operator = ( const CG_Polygon2< Real >& other )
{
  m_type     = other.m_type;
  m_vertices = other.m_vertices;

  return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::Equals( const CG_Polygon2< Real >& other, Real tolerance ) const
{
  if ( this == &other ) { return (true); }

  if ( m_type != other.m_type ) { return (false); }

  size_t thisSize  = m_vertices.Size();
  size_t otherSize = other.m_vertices.Size();
  if ( thisSize != otherSize ) { return (false); }

  if ( thisSize == 0 ) { return (true); }

  // for the next check we need to copy the two polygons because
  // we will try to use the longest edge as first for both
  CG_Polygon2< Real > thisCopy( *this );
  CG_Polygon2< Real > otherCopy( other );

  thisCopy.SetLongestEdgeAsFirst();
  otherCopy.SetLongestEdgeAsFirst();

  // now we have some more chance that the two polygons are equally oriented
  // and we can compare the edges
  for ( size_t i = 0; i < thisSize; ++i )
  {
    if ( !m_vertices[i].Equals( other.m_vertices[i], tolerance ) ) { return (false); }
  }

  return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::EqualsTurningSequence( const CG_Polygon2< Real >& other ) const
{
  if ( m_type            != other.m_type            ) { return (false); }
  if ( m_vertices.Size() != other.m_vertices.Size() ) { return (false); }

  // assumes that both polygons have the same vertices order
  string thisTurningSequence = TurningSequence();
  thisTurningSequence += thisTurningSequence;
  string otherTurningSequence = other.TurningSequence();

  size_t pos = thisTurningSequence.find( otherTurningSequence );
  return ( pos != string::npos );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::Translate( Real dx, Real dy )
{
  for ( int i = 0, cntI = static_cast< int >( m_vertices.Size() ); i < cntI; ++i )
  {
    m_vertices[i].Translate( dx, dy );
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::Translate( const CG_Vector2< Real >& displacement )
{
  Translate( displacement.GetX(), displacement.GetY() );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::Rotate( const CG_Point2< Real >& center, Real angle )
{
  for ( int i = 0, cntI = static_cast< int >( m_vertices.Size() ); i < cntI; ++i )
  {
    m_vertices[i].Rotate( center, angle );
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::RotateAroundBarycenter( Real angle )
{
  CG_Point2< Real > barycenter = Barycenter();
  Rotate( barycenter, angle );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::Triangulate( const ETriangulationMethod& method )
{
  switch ( method ) 
  {
  case PolyTri_TRIANGLE16:
  default:
    {
      if ( m_triangulation ) { delete m_triangulation; }
      m_triangulation = new CG_Triangle16Wrapper< Real >( this );
    }
  }

  m_triangulation->Clear();

  size_t size = m_vertices.Size();
  if ( size == 3 )
  {
    const PVertex& v1 = m_vertices[0];
    const PVertex& v2 = m_vertices[1];
    const PVertex& v3 = m_vertices[2];
    m_triangulation->AppendTriangle( CG_Triangle2< Real >( v1, v2, v3 ) );
  }
  else if ( size > 3 )
  {
    switch ( method )
    {
    case PolyTri_TRIANGLE16:
    default:
      {
        Triangle16Triangulate();
      }
      break;
    }
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::Partition( const ETriangulationMethod& method )
{
  m_partitions.clear();

  // first triangulates this polygon
  Triangulate( method );

  if ( m_triangulation->GetTrianglesList().size() > 0 )
  {
    // we begin filling the polygons list with all the triangles
    // of the triangulation
    for ( size_t i = 0, cntI = m_triangulation->GetTrianglesList().size(); i < cntI; ++i )
    {
      CG_Triangle2< Real > t = m_triangulation->GetTrianglesList()[i];
      CG_Polygon2< Real > poly;
      for ( size_t j = 0; j < 3; ++j )
      {
        poly.AppendVertex( t.Vertex( j ) );
      }
      m_partitions.push_back( poly );
    }

    // then we merge all the triangles which form convex polygons
    size_t currPolyIndex = 0;
    while ( currPolyIndex < m_partitions.size() - 1 )
    {
      CG_Polygon2< Real > currPoly( m_partitions[currPolyIndex] );

      for ( int v = 0, cntV = static_cast< int >( currPoly.m_vertices.Size() ); v < cntV; ++v )
      {
        CG_Segment2< Real > seg( currPoly.m_vertices[v], currPoly.m_vertices[v + 1] );

        bool modified = false;
        for ( size_t p = currPolyIndex + 1; p < m_partitions.size(); ++p )
        {
          CG_Polygon2< Real > nextPoly( m_partitions[p] );

          if ( nextPoly.HasAsReversedEdge( seg ) )
          {
            CG_Polygon2< Real > copyPoly( currPoly );					
            CG_Polygon2< Real > mergedPoly = copyPoly.MergeWith( nextPoly );
            if ( mergedPoly.IsConvex() )
            {
              m_partitions[currPolyIndex] = mergedPoly;
              m_partitions.erase( m_partitions.begin() + p );
              modified = true;
              break;
            }
          }
        }

        if ( modified ) 
        { 
          --currPolyIndex;
          break; 
        }
      }

      ++currPolyIndex;
    }
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::Rectify()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::SetVerticesConvexity()
{
  size_t size = m_vertices.Size();
  if ( size > 2 )
  {
    // we use the property that once you are sure that the 
    // vertices are orderer in CCW, if you walk along the
    // perimeter, the polygon will be always on your left.
    // so, a convex vertex will always be on the right of
    // the segment connecting the previous and the next
    // vertices, which is equal to say that the triangle
    // formed by the previous, current and next vertices
    // is CCW (area >= 0)

    // sets to CCW if not already
    bool reversed = false;
    if ( !IsVerticesOrderCCW() ) 
    { 
      SetVerticesOrderToCCW(); 
      reversed = true;
    }

    for ( int i = 0, cntI = static_cast< int >( size ); i < cntI; ++i )
    {
      PVertex& currV = m_vertices[i];
      CG_Triangle2< Real > tri( m_vertices[i - 1], currV, m_vertices[i + 1] );
      if ( tri.SignedArea() >= CG_Consts< Real >::ZERO )
      {
        currV.SetConvex( true );
      }
      else
      {
        currV.SetConvex( false );
      }
    }

    // restore to CW if reversed
    if ( reversed ) { SetVerticesOrderToCW(); }
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::AppendVertex( Real x, Real y )
{
  m_vertices.Append( PVertex( x, y ) );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::AppendVertex( const CG_Point2< Real >& vertex )
{
  AppendVertex( vertex.GetX(), vertex.GetY() );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::RemoveAllVertices()
{
  m_vertices.RemoveAll();
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::InsertVertex( size_t index,
                                        const CG_Point2< Real >& vertex )
{
  m_vertices.Insert( index, PVertex( vertex.GetX(), vertex.GetY() ) );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::InsertVertexOnEdge( size_t edgeIndex,
                                              Real distanceFromFirstVertexOnEdge )
{
  if ( m_vertices.Size() > 1 )
  {
    const PVertex& currV = m_vertices[edgeIndex];
    CG_Vector2< Real > edgeVec( m_vertices[edgeIndex + 1], currV );
    edgeVec.Normalize();
    edgeVec *= distanceFromFirstVertexOnEdge;
    InsertVertex( edgeIndex, edgeVec.Head( currV ) );
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::InsertVertexOnEdge( const CG_Point2< Real >& vertex, 
                                              Real distance )
{
  int index = FindVertexIndex( vertex );
  if ( index != -1 ) { InsertVertexOnEdge( index, distance ); }
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_Polygon2< Real >::RemoveConsecutiveDuplicatedVertices( Real tolerance )
{
  return (m_vertices.RemoveConsecutiveDuplicated( tolerance ));
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_Polygon2< Real >::RemoveConsecutiveAlignedVertices( Real tolerance )
{
  size_t counter = 0;

  // this algorithm stops when three vertices are left
  // so that at least a degenerate polygon exit this method
  if ( m_vertices.Size() > 2 )
  {
    int i = 0;

    do
    {
      if ( m_vertices.Size() > 3 )
      {
        const PVertex& prevV = m_vertices[i - 1];
        PVertex currV = m_vertices[i];
        PVertex nextV = m_vertices[i + 1];

        // using a while statement because there could be more
        // than three consecutive vertices aligned. At each step
        // we remove the central one
        while ( Collinear( prevV, currV, nextV, tolerance ) )
        {
          if ( m_vertices.Size() <= 3 ) { break; }

          m_vertices.Remove( i );
          currV = m_vertices[i];
          nextV = m_vertices[i + 1];
          ++counter;
        }
      }

      ++i;
    }
    while ( i < m_vertices.Size() && m_vertices.Size() > 3 );
  }

  return (counter);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::ReverseVerticesOrder()
{
  m_vertices.Reverse();
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::SetVerticesOrderToCW()
{
  if ( IsVerticesOrderCCW() ) { ReverseVerticesOrder(); }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::SetVerticesOrderToCCW()
{
  if ( !IsVerticesOrderCCW() ) { ReverseVerticesOrder(); }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::SetLongestEdgeAsFirst()
{
  int index = LongestEdgeIndex();
  if ( index > 0 ) { SlideVertices( index ); }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::SlideVertices( int count )
{
  m_vertices.Slide( count );
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_Polygon2< Real >::InsertConstantLengthSections( int count, Real length, 
                                                          Real margin )
{
  if ( count == 0 ) { return (0); }
  if ( length == CG_Consts< Real >::ZERO ) { return (0); }

  size_t inserted = 0;
  if ( m_vertices.Size() > 1 )
  {
    Real dist;
    Real wall;
    size_t totCount;
    Real nextPos;

    size_t i = 0;
    do
    {
      PVertex& currV = m_vertices[i];

      dist = currV.Distance( m_vertices[i + 1] ) - CG_Consts< Real >::TWO * margin;
      if ( dist >= abs( count ) * length )
      {
        if ( count == - 1 )
        {
          totCount = static_cast< size_t >( floor( dist / length ) );
        }
        else
        {
          totCount = static_cast< size_t >( count );
        }

        wall = margin + (dist - totCount * length) * CG_Consts< Real >::HALF;
        nextPos = wall;
        for ( size_t j = 0, cntJ = totCount + 1; j < cntJ; ++j )
        {
          PVertex& insV = m_vertices[i];
          InsertVertexOnEdge( insV, nextPos );
          ++i;
          nextPos = length;
        }
        inserted += totCount;
      }
      ++i;
    }
    while ( i < m_vertices.Size() );
  }

  return (inserted);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::SetAsStartingEdge( const CG_Segment2< Real >& segment )
{
  int index = FindEdgeIndex( segment );
  if ( index != -1 )
  {
    SlideVertices( index );
    return (true);
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
typename CG_Polygon2< Real >::EPolygonType CG_Polygon2< Real >::GetType() const
{
  return (m_type);
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_Polygon2< Real >::VerticesCount() const
{
  return (m_vertices.Size());
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::IsVerticesOrderCCW() const
{
  return (SignedArea() > CG_Consts< Real >::ZERO);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::IsDegenerated() const
{
  // degenerate polygon
  return ( m_vertices.Size() < 3 );
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::HasDuplicatedVertices() const
{
  size_t size = m_vertices.Size();

  // duplicated vertices (not only consecutive)
  for ( size_t i = 0, cntI = size - 1; i < cntI; ++i )
  {
    for ( size_t j = i + 1; j < size; ++j )
    {
      if ( m_vertices[i].Equals( m_vertices[j] ) ) { return (true); }
    }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::HasSelfIntersections() const
{
  size_t size = m_vertices.Size();

  // self intersections
  for ( size_t i = 0, cntI = size - 1; i < cntI; ++i )
  {
    for ( size_t j = i + 2; j < size; ++j )
    {
      if ( !((i == 0) && (j == size - 1)) )
      {
        if ( Edge( i ).IntersectsProperly( Edge( j ) ) ) { return (true); }
      }
    }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::HasVerticesOnEdges() const
{
  size_t size = m_vertices.Size();

  // vertices on edges
  for ( size_t i = 0; i < size; ++i )
  {
    for ( size_t j = 0; j < size; ++j )
    {
      if ( Edge( i ).Contains( m_vertices[j], false ) ) { return (true); }
    }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::IsSimple() const
{
  return (!IsDegenerated() && !HasDucplicatedVertices() && !HasSelfIntersections() && !HasVerticesOnEdges());
}

//-----------------------------------------------------------------------------

template < typename Real >
const typename CG_Polygon2< Real >::PVertex& CG_Polygon2< Real >::Vertex( size_t index ) const
{
  return (m_vertices[index]);
}

//-----------------------------------------------------------------------------

template < typename Real >
typename CG_Polygon2< Real >::PVertex& CG_Polygon2< Real >::Vertex( size_t index ) 
{
  return (m_vertices[index]);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real > CG_Polygon2< Real >::Edge( size_t index ) const
{
  return (CG_Segment2< Real >( m_vertices[index], m_vertices[index + 1] ));
}

//-----------------------------------------------------------------------------

template < typename Real >
const typename CG_Polygon2< Real >::PVertex* CG_Polygon2< Real >::FindVertex( const CG_Point2< Real >& point ) const
{
  return (m_vertices.Find( PVertex( point.GetX(), point.GetY() ) ));
}

//-----------------------------------------------------------------------------

template < typename Real >
int CG_Polygon2< Real >::FindVertexIndex( const CG_Point2< Real >& point ) const
{
  return (m_vertices.FindIndex( PVertex( point.GetX(), point.GetY() ) ));
}

//-----------------------------------------------------------------------------

template < typename Real >
int CG_Polygon2< Real >::FindEdgeIndex( const CG_Segment2< Real >& segment, 
                                        Real tolerance ) const
{
  size_t size = m_vertices.Size();
  if ( size > 1 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      if ( Edge( i ).Equals( segment, tolerance ) ) { return static_cast< int >( i ); }
    }
  }

  return (-1);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polygon2< Real >::SignedArea() const
{
  Real signedArea = CG_Consts< Real >::ZERO;

  size_t size = m_vertices.Size();
  if ( size > 2 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      const PVertex& currV = m_vertices[i];
      const PVertex& nextV = m_vertices[i + 1];
      signedArea += (currV.GetX() * nextV.GetY() - currV.GetY() * nextV.GetX());
    }
    signedArea *= CG_Consts< Real >::HALF;
  }

  return (signedArea);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polygon2< Real >::Area() const
{
  return (abs( SignedArea() ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polygon2< Real >::Perimeter() const
{
  Real perimeter = CG_Consts< Real >::ZERO;

  size_t size = m_vertices.Size();
  if ( size > 1 )
  {
    for ( int i = 0; i < size; ++i )
    {
      perimeter += m_vertices[i].Distance( m_vertices[i + 1] );
    }
  }

  return (perimeter);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Rectangle2< Real > CG_Polygon2< Real >::MinimumBoundingBox() const
{
  CG_Rectangle2< Real > rect;

  // first: calculates the bounding rectangle
  size_t size = m_vertices.Size();
  if ( size > 0 )
  {
    Real width       = CG_Consts< Real >::ZERO;
    Real height      = CG_Consts< Real >::ZERO;
    Real orientation = CG_Consts< Real >::ZERO;

    if ( size > 2 )
    {
      // to let this method be const we need a copy of this polygon
      // because the following call to SetVerticesConvexity()
      // will alter the polygon member data
      CG_Polygon2< Real > tempThis( *this );

      // the following algorithm works only for convex polygons
      // so we adapt it and use it only on the edges of the polygon
      // whose endpoints are both convex 
      tempThis.SetVerticesConvexity();

      // it does not matter if vertices are listed CW or CCW
      Real minArea = CG_Consts< Real >::MAX_REAL;

      Real s0, s1, t0, t1, test;
      for ( size_t i = 0; i < size; ++i )
      {
        const PVertex& currV = tempThis.m_vertices[i];
        const PVertex& nextV = tempThis.m_vertices[i + 1];

        if ( currV.IsConvex() && nextV.IsConvex() )
        {
          // only if both endpoints are convex
          CG_Vector2< Real > currVec( nextV, currV );
          currVec.Normalize();
          CG_Vector2< Real > perpVec = currVec.PerpCCW();

          s0 = CG_Consts< Real >::ZERO;
          s1 = CG_Consts< Real >::ZERO;
          t0 = CG_Consts< Real >::ZERO;
          t1 = CG_Consts< Real >::ZERO;

          for ( size_t j = 1; j < size; ++j )
          {
            const PVertex& itV = tempThis.m_vertices[j];
            CG_Vector2< Real > itVec( itV, tempThis.m_vertices[0] );
            test = currVec.Dot( itVec );
            s0 = min( s0, test );
            s1 = max( s1, test );

            test = perpVec.Dot( itVec );
            t0 = min( t0, test );
            t1 = max( t1, test );
          }

          Real ds = (s1 - s0);
          Real dt = (t1 - t0);
          Real area = ds * dt;

          if ( minArea > area )
          {
            minArea = area;
            width  = ds;
            height = dt;
            orientation = currVec.Direction();
          }
        }
      }
    }

    // we want the width to be the longer edge and parallel to the x axis 
    if ( width < height ) 
    { 
      orientation += CG_Consts< Real >::HALF_PI;
      if ( orientation > CG_Consts< Real >::TWO_PI ) { orientation -= CG_Consts< Real >::TWO_PI; }

      Real temp = width; 
      width  = height;
      height = temp;
    }

    rect.SetWidth( width );
    rect.SetHeight( height );
    rect.SetOrientation( orientation );
  }

  // then: calculates the center of the bounding rectangle
  if ( size > 2 )
  {
    // at the moment i did not found a faster way to calculate the
    // bounding rectangle center...

    // first we calculate the center of the axis aligned bounding box
    CG_Point2< Real > barycenter = Barycenter();

    Real minX = CG_Consts< Real >::MAX_REAL;
    Real minY = CG_Consts< Real >::MAX_REAL;

    for ( size_t i = 0; i < size; ++i )
    {
      const PVertex& currV = m_vertices[i];
      CG_Point2< Real > p = currV.Rotated( barycenter, -rect.GetOrientation() );
      minX = min( minX, p.GetX() );
      minY = min( minY, p.GetY() );
    }

    Real bbcXaa = minX + CG_Consts< Real >::HALF * rect.GetWidth();
    Real bbcYaa = minY + CG_Consts< Real >::HALF * rect.GetHeight();
    CG_Point2< Real > bbC( bbcXaa, bbcYaa );

    // then we move to the real bounding box orientation
    bbC.Rotate( barycenter, rect.GetOrientation() );
    rect.SetCenter( bbC );
  }
  else
  {
    rect.SetCenter( CG_Point2< Real >() );
  }

  return (rect);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Rectangle2< Real > CG_Polygon2< Real >::AxisAlignedBoundingBox() const
{
  Real minX = MinX();
  Real minY = MinY();
  Real maxX = MaxX();
  Real maxY = MaxY();
  Real midX = CG_Consts< Real >::HALF * (maxX + minX);
  Real midY = CG_Consts< Real >::HALF * (maxY + minY);

  CG_Rectangle2< Real > rect;
  rect.SetWidth( maxX - minX );
  rect.SetHeight( maxY - minY );
  rect.SetOrientation( CG_Consts< Real >::ZERO );
  rect.SetCenter( CG_Point2< Real >( midX, midY ) );

  return (rect);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Polygon2< Real >::Barycenter() const
{
  Real x = CG_Consts< Real >::ZERO;
  Real y = CG_Consts< Real >::ZERO;

  size_t size = m_vertices.Size();
  if ( size > 0 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      const PVertex& currV = m_vertices[i];
      x += currV.GetX();
      y += currV.GetY();
    }

    Real invSize = CG_Consts< Real >::ONE / size;
    x *= invSize;
    y *= invSize;
  }

  return (CG_Point2< Real >( x, y ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polygon2< Real >::MinX() const
{
  Real minX = CG_Consts< Real >::MAX_REAL;

  size_t size = m_vertices.Size();
  if ( size > 0 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      minX = std::min( minX, m_vertices[i].GetX() );
    }
  }

  return (minX);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polygon2< Real >::MaxX() const
{
  Real maxX = -CG_Consts< Real >::MAX_REAL;

  size_t size = m_vertices.Size();
  if ( size > 0 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      maxX = std::max( maxX, m_vertices[i].GetX() );
    }
  }

  return (maxX);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polygon2< Real >::MinY() const
{
  Real minY = CG_Consts< Real >::MAX_REAL;

  size_t size = m_vertices.Size();
  if ( size > 0 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      minY = std::min( minY, m_vertices[i].GetY() );
    }
  }

  return (minY);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polygon2< Real >::MaxY() const
{
  Real maxY = -CG_Consts< Real >::MAX_REAL;

  size_t size = m_vertices.Size();
  if ( size > 0 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      maxY = std::max( maxY, m_vertices[i].GetY() );
    }
  }

  return (maxY);
}

//-----------------------------------------------------------------------------

template < typename Real >
string CG_Polygon2< Real >::TurningSequence() const
{
  string seq;

  size_t size = m_vertices.Size();
  if ( size > 2 )
  {
    for ( int i = 0, cntI = static_cast< int >( size ); i < cntI; ++i )
    {
      const PVertex& prevV = m_vertices[i - 1];
      const PVertex& currV = m_vertices[i];
      const PVertex& nextV = m_vertices[i + 1];

      CG_Vector2< Real > v0( currV, prevV );
      CG_Vector2< Real > v1( nextV, currV );

      Float cross = v0.Cross( v1 );
        
      if ( cross == CG_Consts< Real >::ZERO )
      {
        seq += "S";
      }
      else if ( cross > CG_Consts< Real >::ZERO )
      {
        seq += "L";
      }
      else
      {
        seq += "R";
      }
    }
  }

  return (seq);
}

//-----------------------------------------------------------------------------

template < typename Real >
int CG_Polygon2< Real >::LongestEdgeIndex() const
{
  int index = -1;

  size_t size = m_vertices.Size();
  if ( size > 1 )
  {
    Real maxSqLen = CG_Consts< Real >::ZERO;

    for ( size_t i = 0; i < size; ++i )
    {
      const PVertex& currV = m_vertices[i];
      const PVertex& nextV = m_vertices[i + 1];

      Real sqLen = currV.SquaredDistance( nextV );
      if ( maxSqLen < sqLen )
      {
        maxSqLen = sqLen;
        index    = i;
      }
    }
  }

  return (index);
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Triangulation< Real >& CG_Polygon2< Real >::GetTriangulation() const
{
  assert( m_triangulation );
  return (*m_triangulation);
}

//-----------------------------------------------------------------------------

template < typename Real >
const vector< CG_Polygon2< Real > >& CG_Polygon2< Real >::GetPartitions() const
{
  return (m_partitions);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Polygon2< Real > >& CG_Polygon2< Real >::GetPartitions()
{
  return (m_partitions);
}

//-----------------------------------------------------------------------------

template < typename Real >
const TCircularList< typename CG_Polygon2< Real >::PVertex >& CG_Polygon2< Real >::GetVertices() const
{
  return (m_vertices);
}

//-----------------------------------------------------------------------------

template < typename Real >
TCircularList< typename CG_Polygon2< Real >::PVertex >& CG_Polygon2< Real >::GetVertices()
{
  return (m_vertices);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real > CG_Polygon2< Real >::GetLongestInternalDiagonal() const
{
  CG_Segment2< Real > diagonal;

  size_t size = m_vertices.Size();
  if ( size > 1 )
  {
    CG_Segment2< Real > testSeg;

    Real maxLen = CG_Consts< Real >::ZERO;
    Real testLen;

    for ( size_t i = 0; i < size; ++i )
    {
      const PVertex& currV = m_vertices[i];

      for ( size_t j = i + 1; j <= size; ++j )
      {
        const PVertex& nextV = m_vertices[j];

        testSeg = CG_Segment2< Real >( currV, nextV );
        if ( IsInternalDiagonal( testSeg ) )
        {
          testLen = testSeg.Length();
          if ( maxLen < testLen )
          {
            maxLen   = testLen;
            diagonal = testSeg;
          }
        }
      }
    }
  }

  return (diagonal);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::IsDiagonal( const CG_Segment2< Real >& segment, Real tolerance ) const
{
  size_t size = m_vertices.Size();
  if ( size > 1 )
  {
    const PVertex* pFrom = FindVertex( segment.GetFrom() );
    const PVertex* pTo   = FindVertex( segment.GetTo() );

    if ( !pFrom || !pTo ) { return (false); }
    if ( pFrom == pTo )   { return (false); }

    if ( HasAsEdge( segment, tolerance ) )         { return (false); }
    if ( HasAsReversedEdge( segment, tolerance ) ) { return (false); }

    for ( size_t i = 0; i < size; ++i )
    {
      const PVertex& currV = m_vertices[i];
      const PVertex& nextV = m_vertices[i + 1];

      if ( !pFrom->Equals( currV, tolerance ) &&
           !pFrom->Equals( nextV, tolerance ) &&
           !pTo->Equals( currV, tolerance )   &&
           !pTo->Equals( nextV, tolerance ) )
      {
        CG_Segment2< Real > edge( currV, nextV );
        if ( segment.Intersects( edge, true, tolerance ) )
        {
          return (false);
        }
      }
    }

    return (true);
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::IsInternalDiagonal( const CG_Segment2< Real >& segment, 
                                              Real tolerance ) const
{
  const PVertex* pFromV = FindVertex( segment.GetFrom() );
  const PVertex* pToV   = FindVertex( segment.GetTo() );
  if ( !pFromV || !pToV ) { return (false); }
  if ( pFromV == pToV )   { return (false); }

  CG_Segment2< Real > invSegment = segment.Inverted();

  return (InCone( *pFromV, segment ) && InCone( *pToV, invSegment ) && IsDiagonal( segment ));
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::HasAsEdge( const CG_Segment2< Real >& segment,
                                     Real tolerance ) const
{
  size_t size = m_vertices.Size();
  if ( size > 1 )
  {
    const CG_Point2< Real >& from = segment.GetFrom();
    const CG_Point2< Real >& to   = segment.GetTo();

    for ( size_t i = 0; i < size; ++i )
    {
      const PVertex& currV = m_vertices[i];
      const PVertex& nextV = m_vertices[i + 1];

      if ( currV.Equals( from, tolerance ) && nextV.Equals( to, tolerance ) ) 
      {
        return (true);
      }
    }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::HasAsReversedEdge( const CG_Segment2< Real >& segment,
                                             Real tolerance ) const
{
  return (HasAsEdge( segment.Inverted(), tolerance ));
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::IsConvex()
{
  SetVerticesConvexity();

  size_t size = m_vertices.Size();
  if ( size > 0 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      if (!m_vertices[i].IsConvex() ) { return (false); }
    }
  }

  return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_StraightSkeleton2< Real > CG_Polygon2< Real >::StraightSkeleton()
{
  CG_StraightSkeleton2< Real > skeleton( &CG_Polygon2< Real >( *this ) );
  return (skeleton);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::Contains( Real x, Real y, bool boundaries )
{
  bool contains = false;

	size_t size = m_vertices.Size();

  if ( boundaries )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      CG_Segment2< Real > edge = Edge( i );
      if ( edge.Contains( x, y, true ) ) { return (true); }
    }
  }

  if ( size > 2 )
	{
    Real rhs, lhs;
		Real xC, yC, xN, yN;

		for ( size_t i = 0; i < size; ++i )
		{
			const PVertex& currV = m_vertices[i];
			const PVertex& nextV = m_vertices[i + 1];

			xC = currV.GetX();
			yC = currV.GetY();
			xN = nextV.GetX();
			yN = nextV.GetY();

      if ( y < yC )
			{
				if ( yN <= y )
				{
					lhs = (y - yN) * (xC - xN);
					rhs = (x - xN) * (yC - yN);
					if ( lhs > rhs ) { contains = !contains; }
				}
			}
			else if ( y < yN )
			{
				lhs = (y - yN) * (xC - xN);
				rhs = (x - xN) * (yC - yN);
				if ( lhs < rhs ) { contains = !contains; }
			}
		}
	}

	return (contains);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::Contains( const CG_Point2< Real >& point, bool boundaries )
{
  return (Contains( point.GetX(), point.GetY(), boundaries ));
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::Contains( const CG_Polygon2< Real >& other, bool boundaries )
{
  for ( int i = 0, cntI = static_cast< int >( other.m_vertices.Size() ); i < cntI; ++i )
  {
    if ( !Contains( other.m_vertices[i], boundaries ) ) { return (false); }
    if ( IntersectsProperly( other.Edge( i ) ) ) { return (false); }
  }

  return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::Contains( const CG_Polyline2< Real >& polyline, bool boundaries )
{
  int size = static_cast< int >( polyline.VerticesCount() );
  for ( int i = 0; i < size; ++i )
  {
    if ( !Contains( polyline.Vertex( i ), boundaries ) ) { return (false); }
  }

  for ( int i = 0; i < size - 1; ++i )
  {
    if ( IntersectsProperly( polyline.Segment( i ) ) ) { return (false); }
  }

  return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::Intersects( const CG_Segment2< Real >& segment, bool boundaries )
{
  size_t size = m_vertices.Size();
  if ( size > 1 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      if ( Edge( i ).Intersects( segment, boundaries ) ) { return (true); }
    }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::Intersects( const CG_Polygon2< Real >& other, bool boundaries )
{
  size_t size = other.m_vertices.Size();
  for ( size_t i = 0; i < size; ++i )
  {
    if ( Intersects( other.Edge( i ), boundaries ) ) { return (true); }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::IntersectsProperly( const CG_Segment2< Real >& segment )
{
  size_t size = m_vertices.Size();
  if ( size > 1 )
  {
    for ( size_t i = 0; i < size; ++i )
    {
      if ( Edge( i ).IntersectsProperly( segment ) ) { return (true); }
    }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::IntersectsProperly( const CG_Polygon2< Real >& other )
{
  size_t size = other.m_vertices.Size();
  for ( size_t i = 0; i < size; ++i )
  {
    if ( IntersectsProperly( other.Edge( i ) ) ) { return (true); }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::IntersectsProperly( const CG_Polyline2< Real >& polyline )
{
  size_t size = polyline.VerticesCount();
  for ( size_t i = 0; i < size - 1; ++i )
  {
    if ( IntersectsProperly( polyline.Segment( i ) ) ) { return (true); }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real > CG_Polygon2< Real >::Offset( Real offset, Real tolerance ) const
{
  CG_Polygon2< Real > newPoly;

  if ( offset == CG_Consts< Real >::ZERO )
  {
    return (*this);
  }
  else
  {
    size_t size = m_vertices.Size();
    if ( size > 1 )
    {
      // first we offset all the polygon edges
      vector< CG_Segment2< Real > > edges;
      for ( size_t i = 0; i < size; ++i )
      {
        CG_Segment2< Real > edge = Edge( i );
        CG_Segment2< Real > offsetEdge = edge.Offset( offset );
        edges.push_back( offsetEdge );
      }

      // then we calculate the intersections between the offset edges
      vector< CG_Point2< Real > > points;

      for ( size_t i = 0, cntI = edges.size(), j = cntI - 1; i < cntI; j = i++ )
      {
        if ( edges[j].GetTo().Equals( edges[i].GetFrom(), tolerance ) )
        {
          // the two consecutive edges are aligned
          points.push_back( edges[j].GetTo() );
        }
        else
        {
          // the lines containing the two consecutive edges 
          // cross each other somewhere
          const CG_Point2< Real >& fromJ = edges[j].GetFrom();
          const CG_Point2< Real >& toJ   = edges[j].GetTo();
          const CG_Point2< Real >& fromI = edges[i].GetFrom();
          const CG_Point2< Real >& toI   = edges[i].GetTo();

          CG_Line2< Real > lineJ( fromJ, toJ );
          CG_Line2< Real > lineI( fromI, toI );

          CG_Point2< Real > point;
          bool res = Intersect( lineJ, lineI, point );
          assert( res );

          points.push_back( point );
        }
      }

      // now we can generate the offset polygon
      for ( size_t i = 0, cntI = points.size(); i < cntI; ++i )
      {
        newPoly.AppendVertex( points[i].GetX(), points[i].GetY() );
      }
    }
  }

  return (newPoly);
}

//-----------------------------------------------------------------------------

namespace CGPOLYGON2PRIVATE
{
  typedef enum
  {
    INSIDE,
    OUTSIDE,
    INTERSECTION_IN,
    INTERSECTION_OUT,
    BORDER,
    BORDER_IN,
    BORDER_OUT
  } ClipVertexType;

  template < typename Real >
  class ClipVertex : public CG_Point2< Real >
  {
    string         m_name;
    ClipVertexType m_type;
    bool           m_visited;

  public:
		ClipVertex( Real x, Real y, const string& name, ClipVertexType type = POLYGON )
    : CG_Point2< Real >( x, y )
    , m_name( name )
    , m_type( type )
    , m_visited( false )
    {
    }

		ClipVertex( const ClipVertex& other )
    : CG_Point2< Real >( other )
    , m_name( other.m_name )
    , m_type( other.m_type )
    , m_visited( other.m_visited )
    {
    }

		virtual ~ClipVertex()
    {
    }

    const string& GetName() const
    {
      return (m_name);
    }

    ClipVertexType GetType() const
    {
      return (m_type);
    }

    bool IsVisited() const
    {
      return (m_visited);
    }

    void SetType( ClipVertexType type ) 
    {
      m_type = type;
    }

    void SetVisited( bool visited )
    {
      m_visited = visited;
    }
  };

  template < typename Real >
  class ClipVertexList
  {
    char                         m_prefix;
    vector< ClipVertex< Real > > m_list;
    size_t                       m_current;

  public:
    ClipVertexList( const char& prefix )
    : m_prefix( prefix )
    , m_current( 0 )
    {
    }

    size_t Size() const
    {
      return (m_list.size());
    }

    void Append( const ClipVertex< Real >& vertex )
    {
      string name = m_prefix + vertex.GetName();
      m_list.push_back( ClipVertex< Real >( vertex.GetX(), vertex.GetY(), name, vertex.GetType() ) );
    }

    void InsertAfter( const string& vertexName, const ClipVertex< Real >& vertex )
    {
      int index = FindByName( vertexName );
      if ( index != -1 ) 
      {
        if ( index < (int)(m_list.size() - 1) )
        {
          const ClipVertex< Real >& pointAtIndex = m_list[index];
          Real dist1 = pointAtIndex.Distance( vertex );
          while ( index < (int)(m_list.size() - 1) && m_list[index + 1].GetName()[0] != m_prefix )
          {
            Real dist2 = pointAtIndex.Distance( m_list[index + 1] );
            if ( dist2 < dist1 ) 
            { 
              ++index; 
            }
            else
            {
              break;
            }
          }
          m_list.insert( m_list.begin() + index + 1, vertex ); 
        }
        else
        {
          m_list.push_back( vertex );
        }
      }
    }

    void Replace( const string& vertexName, const ClipVertex< Real >& vertex )
    {
      int index = FindByName( vertexName );
      if ( index != -1 ) 
      {
        m_list[index] = vertex;
      }
    }

    int FindByName( const string& vertexName ) const
    {
      for ( size_t i = 0, cntI = m_list.size(); i < cntI; ++i )
      {
        if ( vertexName == m_list[i].GetName() ) { return ((int)i); }
      }

      return (-1);
    }

    char GetPrefix() const
    {
      return (m_prefix);
    }

    void AdvanceCurrent()
    {
      ++m_current;
      if ( m_current == m_list.size() ) { m_current = 0; }
    }

    void ResetCurrent()
    {
      m_current = 0;
    }

    void SetAsCurrent( const string& vertexName )
    {
      int index = FindByName( vertexName );
      if ( index != -1 ) { m_current = (size_t)index; }
    }

    void SetAsCurrent( size_t index )
    {
      if ( index < m_list.size() ) { m_current = index; }
    }

    size_t GetCurrent() const
    {
      return (m_current);
    }

    CG_Point2< Real > GetCurrentPosition() const
    {
      return (m_list[m_current]);
    }

    ClipVertexType GetCurrentType() const
    {
      return (m_list[m_current].GetType());
    }

    const string& GetCurrentName() const
    {
      return (m_list[m_current].GetName());
    }

    bool IsCurrentIntersectionIn() const
    {
      return (m_list[m_current].GetType() == INTERSECTION_IN);
    }

    bool IsCurrentIntersectionOut() const
    {
      return (m_list[m_current].GetType() == INTERSECTION_OUT);
    }

    bool IsCurrentBorderIn() const
    {
      return (m_list[m_current].GetType() == BORDER_IN);
    }

    bool IsCurrentBorderOut() const
    {
      return (m_list[m_current].GetType() == BORDER_OUT);
    }

    bool IsCurrentVisited() const
    {
      return (m_list[m_current].IsVisited());
    }

    void SetAsVisited( const string& vertexName )
    {
      int index = FindByName( vertexName );
      if ( index != -1 ) { m_list[index].SetVisited( true ); }
    }

    void SetType( const string& vertexName, ClipVertexType type )
    {
      int index = FindByName( vertexName );
      if ( index != -1 ) { m_list[index].SetType( type ); }
    }

    size_t UnvisitedIntersectionCount() const
    {
      size_t count = 0;
      for ( size_t i = 0, cntI = m_list.size(); i < cntI; ++i )
      {
        if ( !m_list[i].IsVisited() )
        {
          ClipVertexType type = m_list[i].GetType();
          if ( type == INTERSECTION_IN || type == INTERSECTION_OUT )
          {
            ++count;
          }
        }
      }
      return (count);
    }
  };
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Polygon2< Real > > CG_Polygon2< Real >::Clip( const CG_Polygon2< Real >& clipPolygon ) const
{
  // first we make a working copy of the two polygons 
  CG_Polygon2< Real > tempThis( *this );
  CG_Polygon2< Real > tempClip( clipPolygon );

  vector< CG_Polygon2< Real > > polygons;

  size_t sizeThis = tempThis.m_vertices.Size();
  size_t sizeClip = tempClip.m_vertices.Size();

  if ( sizeThis < 3 || sizeClip < 3 ) { return (polygons); }

  // this polygon completely contains the clipping one
  if ( tempThis.Contains( tempClip, true ) )
  {
    polygons.push_back( tempClip );
    return (polygons); 
  }

  // the clipping polygon completely contains this one
  if ( tempClip.Contains( tempThis, true ) )
  {
    polygons.push_back( tempThis );
    return (polygons); 
  }

  // the two polygons don't intersect
  if ( !tempClip.IntersectsProperly( tempThis ) ) { return (polygons); }

  // if we are here, this polygon is partly contained into the clipping polygon so has to be clipped
  // we use the weiler - atherton algorithm
  // http://www.cs.drexel.edu/~david/Classes/CS430/HWs/p214-weiler.pdf

  // then we ensure that both polygons' vertices are ordered clockwise
  tempThis.SetVerticesOrderToCW(); 
  tempClip.SetVerticesOrderToCW(); 

  // we create two lists, one for each polygon, containing the polygons' vertices
  CGPOLYGON2PRIVATE::ClipVertexList< Real > listThis( 'T' );
  for ( size_t i = 0; i < sizeThis; ++i )
  {
    const CG_Point2< Real >& point = tempThis.m_vertices[i];
    char buff[20];
    sprintf_s( buff, "%d", i );
    string name( buff );
    CGPOLYGON2PRIVATE::ClipVertexType type;
    if ( tempClip.Contains( point, true ) )
    {
      type = CGPOLYGON2PRIVATE::INSIDE; 
    }
    else
    {
      type = CGPOLYGON2PRIVATE::OUTSIDE; 
    }

    listThis.Append( CGPOLYGON2PRIVATE::ClipVertex< Real >( point.GetX(), point.GetY(), name, type ) ); 
  }

  CGPOLYGON2PRIVATE::ClipVertexList< Real > listClip( 'C' );
  for ( size_t i = 0; i < sizeClip; ++i )
  {
    const CG_Point2< Real >& point = tempClip.m_vertices[i];
    char buff[20];
    sprintf_s( buff, "%d", i );
    string name( buff );
    CGPOLYGON2PRIVATE::ClipVertexType type = CGPOLYGON2PRIVATE::INSIDE;
    listClip.Append( CGPOLYGON2PRIVATE::ClipVertex< Real >( point.GetX(), point.GetY(), name, type ) ); 
  }

  // step one is to identify all of the intersection points between the two polygons
  // and add them to the lists

  size_t intCounter = 0;
  size_t borCounter = 0;
  for ( size_t i = 0; i < sizeThis; ++i )
  {
    CG_Segment2< Real > edgeThis = tempThis.Edge( i );
    CG_Vector2< Real >  vecThis  = edgeThis.ToVector();
    vecThis.Normalize();

    for ( size_t j = 0; j < sizeClip; ++j )
    {
      CG_Segment2< Real > edgeClip = tempClip.Edge( j );

      if ( edgeClip.Contains( edgeThis.GetTo(), true ) )
      // if a vertex of this polygon lies on one of the clip polygon edges
      // it needs a special treatment
      {
        const CG_Point2< Real >& borPoint = edgeThis.GetTo();

        CGPOLYGON2PRIVATE::ClipVertexType borType = CGPOLYGON2PRIVATE::BORDER;

        // sets the point name
        char buffThis[20];
        size_t index = i + 1;
        if ( index == sizeThis ) { index = 0; }
        sprintf_s( buffThis, "%d", index );
        string borName = listThis.GetPrefix() + string( buffThis );
        listThis.SetType( borName, borType );

  		  CGPOLYGON2PRIVATE::ClipVertex< Real > borVertex( borPoint.GetX(), borPoint.GetY(), borName, borType );

        char buffClip[20];
        sprintf_s( buffClip, "%d", j );
        string nameClip = listClip.GetPrefix() + string( buffClip );
        listClip.InsertAfter( nameClip, borVertex );

        ++borCounter;
      }
      else if ( edgeThis.IntersectsProperly( edgeClip ) )
      {
        // calculates intersection point
        CG_Point2< Real > intPoint = edgeThis.Intersection( edgeClip, false );

        if ( intPoint.GetX() != CG_Consts< Real >::MAX_REAL )
        {
          // determines the point type
          CG_Vector2< Real > vecClip = edgeClip.ToVector();
          CG_Vector2< Real > vecNormClip = vecClip.PerpCCW();
          vecNormClip.Normalize();
          Real dot = vecThis.Dot( vecNormClip );

          CGPOLYGON2PRIVATE::ClipVertexType type;
          if ( dot < CG_Consts< Real >::ZERO )
          {
            type = CGPOLYGON2PRIVATE::INTERSECTION_IN;
          }
          else
          {
            type = CGPOLYGON2PRIVATE::INTERSECTION_OUT;
          }

          // sets the point name
          char buffCounter[20];
          sprintf_s( buffCounter, "I%d", (++intCounter) );
          string intName( buffCounter );
    		  CGPOLYGON2PRIVATE::ClipVertex< Real > intVertex( intPoint.GetX(), intPoint.GetY(), intName, type );

          // adds the point to the lists
          char buffThis[20];
          sprintf_s( buffThis, "%d", i );
          string nameThis = listThis.GetPrefix() + string( buffThis );
          char buffClip[20];
          sprintf_s( buffClip, "%d", j );
          string nameClip = listClip.GetPrefix() + string( buffClip );
          listThis.InsertAfter( nameThis, intVertex );
          listClip.InsertAfter( nameClip, intVertex );
        }
      }
    }
  }

  // now we identify the type of each border point
  if ( borCounter > 0 )
  {
    listThis.ResetCurrent();
    CGPOLYGON2PRIVATE::ClipVertexType typeCurr;
    string name;
    for ( size_t i = 0, cntI = listThis.Size(); i < cntI; ++i )
    {
      typeCurr = listThis.GetCurrentType();
      name     = listThis.GetCurrentName();
      if ( typeCurr == CGPOLYGON2PRIVATE::BORDER )
      {
        listThis.AdvanceCurrent();
        CGPOLYGON2PRIVATE::ClipVertexType typeNext = listThis.GetCurrentType();
        if ( typeNext == CGPOLYGON2PRIVATE::OUTSIDE )
        {
          listThis.SetType( name, CGPOLYGON2PRIVATE::BORDER_OUT );
          listClip.SetType( name, CGPOLYGON2PRIVATE::BORDER_OUT );
        }
        else
        {
          listThis.SetType( name, CGPOLYGON2PRIVATE::BORDER_IN );
          listClip.SetType( name, CGPOLYGON2PRIVATE::BORDER_IN );
        }
      }
      else
      { 
        listThis.AdvanceCurrent();
      }
    }
  }

  // now we use the lists to identify the resulting polygons
  listThis.ResetCurrent();
  bool done = false;
  while ( !done )
  {
    if ( !listThis.IsCurrentIntersectionIn() /*&& !listThis.IsCurrentBorderIn()*/ ) 
    { 
      listThis.AdvanceCurrent();
    }
    else
    {
      CG_Polygon2< Real > polygon;

      size_t currentThis = listThis.GetCurrent();
      CGPOLYGON2PRIVATE::ClipVertexList< Real >* curList = &listThis;
      string curListName = "This";

      while ( !curList->IsCurrentVisited() )
      {
        polygon.AppendVertex( curList->GetCurrentPosition() );
        string name = curList->GetCurrentName();
        curList->SetAsVisited( name );
        if ( curList->IsCurrentIntersectionOut() || 
             curList->IsCurrentIntersectionIn()  ||
             curList->IsCurrentBorderIn()        ||
             curList->IsCurrentBorderOut() )
        {
          if ( curListName == "This" )
          {
            listClip.SetAsVisited( name );
          }
          else
          {
            listThis.SetAsVisited( name );
          }
        }

        if ( curListName == "This" && 
             (curList->IsCurrentIntersectionOut() || curList->IsCurrentBorderOut() ) )
        {
          listClip.SetAsCurrent( name );
          curList = &listClip;
          curListName = "Clip";
        }
        else if ( curListName == "Clip" && 
                  (curList->IsCurrentIntersectionIn() || curList->IsCurrentBorderIn() ) )
        {
          listThis.SetAsCurrent( name );
          curList = &listThis;
          curListName = "This";
        }

        curList->AdvanceCurrent();
      }

      if ( polygon.VerticesCount() > 0 ) { polygons.push_back( polygon ); }

      listThis.SetAsCurrent( currentThis + 1 );
      if ( listThis.UnvisitedIntersectionCount() == 0 ) { done = true; }
    }
  }

  return (polygons);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polygon2< Real > CG_Polygon2< Real >::MergeWith( const CG_Polygon2< Real >& other ) const
{	
  CG_Polygon2< Real > newPoly;

  size_t size = m_vertices.Size();
  if ( size > 1 )
  {
    // copy required to preserve the const of the input polygons
    CG_Polygon2< Real > tempThis( *this );
    CG_Polygon2< Real > tempOther( other );

    for ( size_t i = 0; i < size; ++i )
    {
      CG_Segment2< Real > edge = tempThis.Edge( i );
      if ( tempOther.HasAsReversedEdge( edge ) )
      {
        if ( tempThis.SetAsStartingEdge( edge ) )
        {
          CG_Segment2< Real > invEdge = edge.Inverted();
          if ( tempOther.SetAsStartingEdge( invEdge ) )
          {
            for ( size_t j = 1, cntJ = tempThis.m_vertices.Size(); j < cntJ ; ++j )
            {
              newPoly.AppendVertex( tempThis.m_vertices[j] );
            }
            newPoly.AppendVertex( tempThis.m_vertices[0] );
            for ( size_t j = 2, cntJ = tempOther.m_vertices.Size(); j < cntJ ; ++j )
            {
              newPoly.AppendVertex( tempOther.m_vertices[j] );
            }
          }
        }
        break;
      }
    }
  }

  return (newPoly);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polygon2< Real >::InCone( const CG_Point2< Real >& point, 
                                  const CG_Segment2< Real >& segment,
                                  Real tolerance ) const
{
  size_t size = m_vertices.Size();
  if ( size > 2 )
  {
    int index = FindVertexIndex( point );
    if ( index == -1 ) { return (false); }

    const PVertex& prevV = m_vertices[index - 1];	
    const PVertex& currV = m_vertices[index];	
    const PVertex& nextV = m_vertices[index + 1];	

    CG_Segment2< Real > edge( currV, nextV );
    CG_Segment2< Real > invSegment = segment.Inverted();

    // if vertex is convex
    if ( edge.HasAtLeftOrOn( prevV, tolerance ) )
    {
      return (segment.HasAtLeft( prevV, tolerance ) &&
              invSegment.HasAtLeft( nextV, tolerance ));
    }

    // vertex is reflex
    return !(segment.HasAtLeftOrOn( nextV, tolerance ) &&
             invSegment.HasAtLeftOrOn( prevV, tolerance ));
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polygon2< Real >::Triangle16Triangulate()
{
  string switches = "p";

  static_cast< CG_Triangle16Wrapper< Real > *>( m_triangulation )->Triangulate( switches );
  static_cast< CG_Triangle16Wrapper< Real > *>( m_triangulation )->MakeTrianglesList();
}

//-----------------------------------------------------------------------------
