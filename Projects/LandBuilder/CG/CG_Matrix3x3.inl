//-----------------------------------------------------------------------------
// File: CG_Matrix3x3.inl
//
// Desc: A 3x3 matrix
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Matrix3x3< Real >::CG_Matrix3x3()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Matrix3x3< Real >::CG_Matrix3x3( const CG_Matrix3x3< Real >& other )
{
	for ( size_t i = 0; i < 9; ++i )
	{
		m_elements[i] = other.m_elements[i];
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Matrix3x3< Real >::~CG_Matrix3x3()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Matrix3x3< Real >& CG_Matrix3x3< Real >::operator = ( const CG_Matrix3x3< Real >& other )
{
	for ( size_t i = 0; i < 9; ++i )
	{
		m_elements[i] = other.m_elements[i];
	}
	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Matrix3x3< Real > CG_Matrix3x3< Real >::operator - () const
{
	CG_Matrix3x3< Real > mat( *this );
	for ( size_t i = 0; i < 9; ++i )
	{
		mat.m_elements[i] *= static_cast< Real >( -1 );
	}
	return (mat);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point3< Real > CG_Matrix3x3< Real >::operator * ( const CG_Point3< Real >& point ) const
{
    return (CG_Point3< Real >( m_elements[0] * point.GetX() + m_elements[1] * point.GetY() + m_elements[2] * point.GetZ(),
							   m_elements[3] * point.GetX() + m_elements[4] * point.GetY() + m_elements[5] * point.GetZ(),
							   m_elements[6] * point.GetX() + m_elements[7] * point.GetY() + m_elements[8] * point.GetZ() ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Matrix3x3< Real >::operator () ( size_t row, size_t col ) const
{
	assert( row < 3 );
	assert( col < 3 );
	return (m_elements[3 * row + col]);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real& CG_Matrix3x3< Real >::operator () ( size_t row, size_t col )
{
	assert( row < 3 );
	assert( col < 3 );
	return (m_elements[3 * row + col]);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Matrix3x3< Real >::SetFromAxisAngle( const CG_Vector3< Real >& axis, 
											 Real angle )
{
	// ensures that we use a unit vector
	CG_Vector3< Real > axisCopy( axis );
	axisCopy.Normalize();

    Real cosA = cos( angle );
    Real sinA = sin( angle );
    Real oneMinusCosA = static_cast< Real >( 1 ) - cosA;
	Real aX = axisCopy.GetX();
	Real aY = axisCopy.GetY();
	Real aZ = axisCopy.GetZ();
	Real x2 = aX * aX;
    Real y2 = aY * aY;
    Real z2 = aZ * aZ;
    Real xyM = aX * aY * oneMinusCosA;
    Real xzM = aX * aZ * oneMinusCosA;
    Real yzM = aY * aZ * oneMinusCosA;
    Real xSinA = aX * sinA;
    Real ySinA = aY * sinA;
    Real zSinA = aZ * sinA;
    
    m_elements[0] = x2 * oneMinusCosA + cosA;
    m_elements[1] = xyM - zSinA;
    m_elements[2] = xzM + ySinA;
    m_elements[3] = xyM + zSinA;
    m_elements[4] = y2 * oneMinusCosA + cosA;
    m_elements[5] = yzM - xSinA;
    m_elements[6] = xzM - ySinA;
    m_elements[7] = yzM + xSinA;
    m_elements[8] = z2 * oneMinusCosA + cosA;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Matrix3x3< Real >::SetFromBasis( const CG_Vector3< Real >& axisX, 
										 const CG_Vector3< Real >& axisY,
										 const CG_Vector3< Real >& axisZ )
{
	// ensures that we use unit vectors
	CG_Vector3< Real > axisXCopy( axisX );
	CG_Vector3< Real > axisYCopy( axisY );
	CG_Vector3< Real > axisZCopy( axisZ );
	axisXCopy.Normalize();
	axisYCopy.Normalize();
	axisZCopy.Normalize();

	m_elements[0] = axisXCopy.GetX();
    m_elements[1] = axisXCopy.GetY();
    m_elements[2] = axisXCopy.GetZ();
    m_elements[3] = axisYCopy.GetX();
    m_elements[4] = axisYCopy.GetY();
    m_elements[5] = axisYCopy.GetZ();
    m_elements[6] = axisZCopy.GetX();
    m_elements[7] = axisZCopy.GetY();
    m_elements[8] = axisZCopy.GetZ();	
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Matrix3x3< Real >::Transpose()
{
	for ( size_t i = 0; i < 2; ++i )
	{
		for ( size_t j = i + 1; j < 3; ++j )
		{
			Real temp = m_elements[i * 3 + j];
			m_elements[i * 3 + j] = m_elements[j * 3 + i];
			m_elements[j * 3 + i] = temp;
		}
	}
}

//-----------------------------------------------------------------------------
