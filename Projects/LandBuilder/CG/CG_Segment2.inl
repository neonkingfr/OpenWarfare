//-----------------------------------------------------------------------------
// File: CG_Segment2.inl
//
// Desc: A two dimensional segment
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real >::CG_Segment2()
: m_from( CG_Point2< Real >() )
, m_to( CG_Point2< Real >() )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real >::CG_Segment2( const CG_Point2< Real >& from, 
								  const CG_Point2< Real >& to )
: m_from( from )
, m_to( to )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real >::CG_Segment2( const CG_Segment2< Real >& other )
: m_from( other.m_from )
, m_to( other.m_to )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real >::~CG_Segment2()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point2< Real >& CG_Segment2< Real >::GetFrom() const
{
	return (m_from);
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point2< Real >& CG_Segment2< Real >::GetTo() const
{
	return (m_to);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Segment2< Real >::SetFrom( const CG_Point2< Real >& from )
{
	m_from = from;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Segment2< Real >::SetTo( const CG_Point2< Real >& to )
{
	m_to = to;
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::Equals( const CG_Segment2< Real >& other, Real tolerance ) const
{
	if ( !m_from.Equals( other.m_from, tolerance ) ) { return (false); }
	if ( !m_to.Equals( other.m_to, tolerance ) )     { return (false); }
	return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::IsInverseOf( const CG_Segment2< Real >& other, Real tolerance ) const
{
	if ( !m_from.Equals( other.m_to, tolerance ) ) { return (false); }
	if ( !m_to.Equals( other.m_from, tolerance ) ) { return (false); }
	return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::EqualsOrInverseOf( const CG_Segment2< Real >& other, Real tolerance ) const
{
	return (Equals( other, tolerance ) || IsInverseOf( other, tolerance ));
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Segment2< Real >::Rotate( const CG_Point2< Real >& center, Real angle )
{
	m_from.Rotate( center, angle );
	m_to.Rotate( center, angle );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Segment2< Real >::RotateAroundMidpoint( Real angle )
{
	Rotate( MidPoint(), angle );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Segment2< Real >::Translate( Real dx, Real dy )
{
  m_from.Translate( dx, dy );
  m_to.Translate( dx, dy );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Segment2< Real >::Translate( const CG_Vector2< Real >& displacement )
{
  Translate( displacement.GetX(), displacement.GetY() );
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Segment2< Real >::SquaredLength() const
{
	return (m_from.SquaredDistance( m_to ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Segment2< Real >::Length() const
{
	return (sqrt( SquaredLength() ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Segment2< Real >::Direction() const
{
	CG_Vector2< Real > v( m_to, m_from );

	return ( v.Direction() );
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Segment2< Real >::ClosestPointTo( const CG_Point2< Real >& point,
													   Real tolerance ) const
{
	CG_Point2< Real > ret;
	Float minDist = std::numeric_limits< Float >::infinity();

	// for speed, we use squared distances

	// first we check the endpoints
	Float dist = point.SquaredDistance( m_from );
	if ( minDist > dist )
	{
		minDist = dist;
		ret = m_from;
	}
	dist = point.SquaredDistance( m_to );
	if ( minDist > dist )
	{
		minDist = dist;
		ret = m_to;
	}

	// then we check the projection of the point on the segment
	// we use it only if it is ON the segment
	{
		CG_Vector2< Real > unitSegVec = UnitTangentVector();
		CG_Vector2< Real > pointVec( point, m_from );

		// component of pVec on this segment
		Float pointCompOnSeg = pointVec.Dot( unitSegVec );

		// if and only if the projected point is ON this segment (excluding endpoints)...
		if ( (tolerance <= pointCompOnSeg) && (pointCompOnSeg <= (Length() - tolerance)) )
		{
			CG_Vector2< Real > vecOnSeg = unitSegVec * pointCompOnSeg;
			CG_Point2< Real > projPoint = vecOnSeg.Head( m_from );
			dist = point.SquaredDistance( projPoint );
			if ( minDist > dist )
			{
				minDist = dist;
				ret = projPoint;
			}
		}
	}

	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real> CG_Segment2< Real >::UnitTangentVector() const
{
	CG_Vector2< Real > ret( m_to, m_from );
	ret.Normalize();
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real> CG_Segment2< Real >::UnitNormalVector() const
{
	return (UnitTangentVector().PerpCCW());
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Segment2< Real >::MidPoint() const
{
	Real x = static_cast< Real >( 0.5 ) * (m_from.GetX() + m_to.GetX());
	Real y = static_cast< Real >( 0.5 ) * (m_from.GetY() + m_to.GetY());

	return (CG_Point2< Real >( x, y ));
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::HasAtLeft( const CG_Point2< Real >& point, Real tolerance ) const
{
	CG_Triangle2< Real > tri( m_from, m_to, point );

	return ( tri.SignedArea() > tolerance );
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::HasAtLeftOrOn( const CG_Point2< Real >& point, Real tolerance ) const
{
	return (HasAtLeft( point, tolerance) || IsCollinear( point, tolerance ));
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::IsCollinear( const CG_Point2< Real >& point, Real tolerance ) const
{
	CG_Triangle2< Real > tri( m_from, m_to, point );

	return (fabs( tri.SignedArea() ) <= tolerance);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Segment2< Real >::DistanceFromContainingLine( const CG_Point2< Real >& point ) const
{
  CG_Line2< Real > line( m_from, m_to );
  return (line.Distance( point ));
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::Contains( Real x, Real y, bool boundary, Real tolerance ) const
{
  return (Contains( CG_Point2< Real >( x, y ), boundary, tolerance ));
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::Contains( const CG_Point2< Real >& point, bool boundary, 
                                    Real tolerance ) const
{
	if ( boundary )
	{
		if ( point.Equals( m_from, tolerance ) ) { return (true); }
		if ( point.Equals( m_to, tolerance ) )   { return (true); }
	}
  else
  {
		if ( point.Equals( m_from, tolerance ) ) { return (false); }
		if ( point.Equals( m_to, tolerance ) )   { return (false); }
  }

	Real sum = point.Distance( m_from ) + point.Distance( m_to );
  Real len = Length();
	Real diff = abs( len - sum );

	return ( diff <= tolerance );
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::Contains( const CG_Segment2< Real >& other, bool boundary, Real tolerance ) const
{
  return (Contains( other.GetFrom(), boundary, tolerance ) && Contains( other.GetTo(), boundary, tolerance ) );
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::IntersectsProperly( const CG_Segment2< Real >& other, Real tolerance ) const
{
	if ( IsCollinear( other.m_from, tolerance ) ||
       IsCollinear( other.m_to, tolerance )   ||
       other.IsCollinear( m_from, tolerance ) ||
       other.IsCollinear( m_to, tolerance ) )
	{
		return (false);
	}

	return ((HasAtLeft( other.m_from, tolerance ) ^ HasAtLeft( other.m_to, tolerance )) &&
          (other.HasAtLeft( m_from, tolerance ) ^ other.HasAtLeft( m_to, tolerance )));
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Segment2< Real >::Intersects( const CG_Segment2< Real >& other, 
                                      bool boundaries, Real tolerance ) const
{
	if ( IntersectsProperly( other, tolerance ) )
	{
		return (true);
	}
	else if ( Contains( other.m_from, boundaries, tolerance ) ||
            Contains( other.m_to, boundaries, tolerance )   ||
            other.Contains( m_from, boundaries, tolerance ) ||
            other.Contains( m_to, boundaries, tolerance ) )
	{
		return (true);
	}
	else
	{
		return (false);
	}
}

// -------------------------------------------------------------------------- //

template < typename Real >
CG_Point2< Real > CG_Segment2< Real >::Intersection( const CG_Segment2< Real >& other,
                                                     bool boundary, Real tolerance ) const
{
	CG_Line2< Real > line1( m_from, m_to );
	CG_Line2< Real > line2( other.m_from, other.m_to );

	CG_Point2< Real > point;

	if ( Intersect( line1, line2, point ) )
	{
		if ( Contains( point, boundary, tolerance ) && other.Contains( point, boundary, tolerance ) ) { return (point); }
	}

	return (CG_Point2< Real >( CG_Consts< Real >::MAX_REAL, CG_Consts< Real >::MAX_REAL ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real > CG_Segment2< Real >::Inverted() const
{
	return (CG_Segment2< Real >( m_to, m_from ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real > CG_Segment2< Real >::Offset( Real offset ) const
{
	if ( offset == static_cast< Real >( 0 ) ) 
	{ 
		return (*this); 
	}
	else
	{
		CG_Vector2< Real > vec = ToVector();
		CG_Vector2< Real > perpVec;
		if ( offset < static_cast< Real >( 0 ) ) 
		{ 
			perpVec = vec.PerpCCW();
		}
		else
		{
			perpVec = vec.PerpCW();
		}
		perpVec.Normalize();
		perpVec *= abs( offset );

		return (CG_Segment2< Real >( perpVec.Head( m_from ), perpVec.Head( m_to ) ) );

	}
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CG_Segment2< Real >::ToVector() const
{
	return (CG_Vector2< Real >( m_to, m_from ));
}

//-----------------------------------------------------------------------------
