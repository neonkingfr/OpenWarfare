//-----------------------------------------------------------------------------
// File: CG_Triangle3.inl
//
// Desc: A two dimensional triangle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangle3< Real >::CG_Triangle3()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangle3< Real >::CG_Triangle3( const CG_Point3< Real >& v1, const CG_Point3< Real >& v2,
									const CG_Point3< Real >& v3 )
{
	m_vertices[0] = v1;
	m_vertices[1] = v2;
	m_vertices[2] = v3;
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangle3< Real >::CG_Triangle3( const CG_Triangle3< Real >& other )
{
	for ( size_t i = 0; i < 3; ++i )
	{
		m_vertices[i] = other.m_vertices[i];
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangle3< Real >::~CG_Triangle3()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle3< Real >::Translate( Real dx, Real dy, Real dz )
{
	for ( size_t i = 0; i < 3; ++i )
	{
		m_vertices[i].Translate( dx, dy, dz );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle3< Real >::Translate( const CG_Vector3< Real >& displacement )
{
	Translate( displacement.GetX(), displacement.GetY(), displacement.GetZ() ); 
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle3< Real >::Rotate( const CG_Vector3< Real >& axis, Real angle,
								   const CG_Point3< Real >& center )
{
	for ( size_t i = 0; i < 3; ++i )
	{
		m_vertices[i].Rotate( axis, angle, center );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle3< Real >::Rotate( const CG_Matrix3x3< Real >& matrix )
{
	for ( size_t i = 0; i < 3; ++i )
	{
		m_vertices[i].Rotate( matrix );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point3< Real >& CG_Triangle3< Real >::Vertex( size_t index ) const
{
	assert( index < 3 );
	return (m_vertices[index]);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real > CG_Triangle3< Real >::Normal() const
{
	CG_Vector3< Real > vec01( m_vertices[1], m_vertices[0] );
	CG_Vector3< Real > vec02( m_vertices[2], m_vertices[0] );
	CG_Vector3< Real > cross = vec01.Cross( vec02 );
	return (cross.Normalized());
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Triangle3< Real >::Perimeter()  const
{
	return (m_vertices[0].Distance( m_vertices[1] ) +
			m_vertices[1].Distance( m_vertices[2] ) +
			m_vertices[2].Distance( m_vertices[0] ));
}

//-----------------------------------------------------------------------------
