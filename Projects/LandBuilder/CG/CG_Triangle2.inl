//-----------------------------------------------------------------------------
// File: CG_Triangle2.inl
//
// Desc: A two dimensional triangle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangle2< Real >::CG_Triangle2()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangle2< Real >::CG_Triangle2( const CG_Point2< Real >& v1, const CG_Point2< Real >& v2,
                                    const CG_Point2< Real >& v3 )
{
	m_vertices[0] = v1;
	m_vertices[1] = v2;
	m_vertices[2] = v3;
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangle2< Real >::~CG_Triangle2()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Triangle2< Real >::SignedArea() const
{
	Real x0 = m_vertices[0].GetX();
	Real y0 = m_vertices[0].GetY();
	Real x1 = m_vertices[1].GetX();
	Real y1 = m_vertices[1].GetY();
	Real x2 = m_vertices[2].GetX();
	Real y2 = m_vertices[2].GetY();
	return ((x0 * y1 - y0 * x1 + x1 * y2 - y1 * x2 + x2 * y0 - y2 * x0 ) / static_cast< Real >( 2 ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Triangle2< Real >::Area() const
{
	return (abs( SignedArea() ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Triangle2< Real >::Perimeter() const
{
	return (m_vertices[0].Distance( m_vertices[1] ) +
			m_vertices[1].Distance( m_vertices[2] ) +
			m_vertices[2].Distance( m_vertices[0] ));
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point2< Real >& CG_Triangle2< Real >::Vertex( size_t index ) const
{
	assert( index < 3 );
	return (m_vertices[index]);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real >& CG_Triangle2< Real >::Vertex( size_t index )
{
	assert( index < 3 );
	return (m_vertices[index]);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Triangle2< Real >::InCenter() const
{
	Real perimeter = Perimeter();
	assert( perimeter != CG_Consts< Real >::ZERO );

	Real numX = CG_Consts< Real >::ZERO;
	Real numY = CG_Consts< Real >::ZERO;
	for ( size_t i = 0; i < 3; ++i )
	{
		const CG_Point2< Real > p = m_vertices[i];
		numX += p.GetX() * EdgeOppositeTo( i ).Length();
		numY += p.GetY() * EdgeOppositeTo( i ).Length();
	}

	return (CG_Point2< Real >( numX / perimeter, numY / perimeter ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real > CG_Triangle2< Real >::EdgeOppositeTo( size_t index ) const
{
	assert( index < 3 );

	return (CG_Segment2< Real >( m_vertices[(index + 1) % 3], 
								 m_vertices[(index + 2) % 3] ));
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_Triangle2< Real >::VerticesCount() const
{
  return (3);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Rectangle2< Real > CG_Triangle2< Real >::AxisAlignedBoundingBox() const
{
	Real minX = MinX();
	Real minY = MinY();
	Real maxX = MaxX();
	Real maxY = MaxY();
	Real midX = CG_Consts< Real >::HALF * (maxX + minX);
	Real midY = CG_Consts< Real >::HALF * (maxY + minY);

	CG_Rectangle2< Real > rect;
	rect.SetWidth( maxX - minX );
	rect.SetHeight( maxY - minY );
	rect.SetOrientation( CG_Consts< Real >::ZERO );
	rect.SetCenter( CG_Point2< Real >( midX, midY ) );

	return (rect);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Triangle2< Real >::MinX() const
{
	Real minX = CG_Consts< Real >::MAX_REAL;

	for ( size_t i = 0; i < 3; ++i )
	{
		minX = std::min( minX, m_vertices[i].GetX() );
	}

	return (minX);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Triangle2< Real >::MaxX() const
{
	Real maxX = -CG_Consts< Real >::MAX_REAL;

	for ( size_t i = 0; i < 3; ++i )
	{
		maxX = std::max( maxX, m_vertices[i].GetX() );
	}

	return (maxX);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Triangle2< Real >::MinY() const
{
	Real minY = CG_Consts< Real >::MAX_REAL;

	for ( size_t i = 0; i < 3; ++i )
	{
		minY = std::min( minY, m_vertices[i].GetY() );
	}

	return (minY);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Triangle2< Real >::MaxY() const
{
	Real maxY = -CG_Consts< Real >::MAX_REAL;

	for ( size_t i = 0; i < 3; ++i )
	{
		maxY = std::max( maxY, m_vertices[i].GetY() );
	}

	return (maxY);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle2< Real >::SetVertex( size_t index, const CG_Point2< Real >& vertex )
{
	assert( index < 3 );
	m_vertices[index] = vertex;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle2< Real >::Translate( Real dx, Real dy )
{
	for ( size_t i = 0; i < 3; ++i )
	{
		m_vertices[i].Translate( dx, dy );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle2< Real >::Rotate( const CG_Point2< Real >& center, Real angle )
{
	for ( size_t i = 0; i < 3; ++i )
	{
		m_vertices[i].Rotate( center, angle );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle2< Real >::Scale( const CG_Point2< Real >& center, Real scaleFactor )
{
	for ( size_t i = 0; i < 3; ++i )
	{
		CG_Vector2< Real > vec( m_vertices[i], center );
		vec *= scaleFactor;
		m_vertices[i] = vec.Head( center );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Triangle2< Real >::Contains( Real x, Real y, bool boundaries ) const
{
	// uses the same algorithm used by CG_Polygon2
  bool contains = false;

  if ( boundaries )
  {
    for ( size_t i = 0; i < 3; ++i )
    {
      CG_Segment2< Real > edge = EdgeOppositeTo( i );
      if ( edge.Contains( x, y, true ) ) { return (true); }
    }
  }

  Real rhs, lhs;
	Real xC, yC, xN, yN;

	for ( size_t i = 0, j = 2; i < 3; j = i++ )
	{
		const CG_Point2< Real >& currV = m_vertices[j];
		const CG_Point2< Real >& nextV = m_vertices[i];

		xC = currV.GetX();
		yC = currV.GetY();
		xN = nextV.GetX();
		yN = nextV.GetY();

		if ( y < yC )
		{
			if ( yN <= y )
			{
				lhs = (y - yN) * (xC - xN);
				rhs = (x - xN) * (yC - yN);
				if ( lhs > rhs ) { contains = !contains; }
			}
		}
		else if ( y < yN )
		{
			lhs = (y - yN) * (xC - xN);
			rhs = (x - xN) * (yC - yN);
			if ( lhs < rhs ) { contains = !contains; }
		}
	}

	return (contains);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Triangle2< Real >::Contains( const CG_Point2< Real >& point, bool boundaries ) const
{
  return (Contains( point.GetX(), point.GetY(), boundaries ));
}

//-----------------------------------------------------------------------------

template < typename Real >
std::ostream& operator << ( std::ostream& o, const CG_Triangle2< Real >& t )
{
	o << "[";
	for ( size_t i = 0; i < 3; ++i )
	{
		o << "(" << t.Vertex( i ).GetX() << ", " << t.Vertex( i ).GetY() << ")";
		if ( i < 2 )
		{
			o << "-";
		}
	}
	o << "]";
    return (o);
}

//-----------------------------------------------------------------------------
