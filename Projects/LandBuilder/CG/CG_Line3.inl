//-----------------------------------------------------------------------------
// File: CG_Line3.inl
//
// Desc: A three dimensional line
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Line3< Real >::CG_Line3()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Line3< Real >::CG_Line3( const CG_Point3< Real >& base, 
						    const CG_Vector3< Real >& direction )
: m_base( base )
, m_direction( direction )
{
	// to be sure it is a unit vector
	m_direction.Normalize();
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Line3< Real >::CG_Line3( const CG_Line3< Real >& other )
: m_base( other.m_base )
, m_direction( other.m_direction )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Line3< Real >::~CG_Line3()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point3< Real > CG_Line3< Real >::PointAt( Real distanceFromBase ) const
{
	CG_Vector3< Real > displacement = distanceFromBase * m_direction;
	CG_Point3< Real > point( m_base );
	return (point.Translated( displacement.GetX(), displacement.GetY(), displacement.GetZ() ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Line3< Real >::Distance( const CG_Point3< Real >& point ) const
{
	assert( m_direction.Length() > static_cast< Real >( 0 ) );

	CG_Vector3< Real > vec( point, m_base );
	Real num = m_direction.Dot( vec );
	Real den = m_direction.Dot( m_direction );
	Real t0 = num / den;
	CG_Point3< Real > projection = PointAt( t0 );
	return (point.Distance( projection ));
}

//-----------------------------------------------------------------------------
