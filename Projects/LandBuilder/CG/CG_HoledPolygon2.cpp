//-----------------------------------------------------------------------------
// File: CG_HoledPolygon2.cpp
//
// Desc: A two dimensional polygon with holes
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "CG_HoledPolygon2.h"

//-----------------------------------------------------------------------------
