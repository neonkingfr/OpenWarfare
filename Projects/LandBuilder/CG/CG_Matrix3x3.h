//-----------------------------------------------------------------------------
// File: CG_Matrix3x3.h
//
// Desc: A 3x3 matrix
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#ifndef CG_MATRIX3X3_H
#define CG_MATRIX3X3_H

//-----------------------------------------------------------------------------

#include "CG_Vector3.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Matrix3x3 
{
	Real m_elements[9];

public:
	CG_Matrix3x3();
	CG_Matrix3x3( const CG_Matrix3x3< Real >& other );
	~CG_Matrix3x3();

	// Assignement
	CG_Matrix3x3< Real >& operator = ( const CG_Matrix3x3< Real >& other );

    // Arithmetic operations
	// {
    CG_Matrix3x3< Real > operator - () const;
    CG_Point3< Real > operator * ( const CG_Point3< Real >& point ) const;
	// }

	// Matrix elements access
	// {
    Real operator () ( size_t row, size_t col ) const;
    Real& operator () ( size_t row, size_t col );
	// }

	// Matrix manipulators
	// {
	void SetFromAxisAngle( const CG_Vector3< Real >& axis, Real angle );
	void SetFromBasis( const CG_Vector3< Real >& axisX, const CG_Vector3< Real >& axisY,
					   const CG_Vector3< Real >& axisZ );

	void Transpose();
	// }
};

//-----------------------------------------------------------------------------

#include "CG_Matrix3x3.inl"

//-----------------------------------------------------------------------------

#endif