//-----------------------------------------------------------------------------
// File: TRectQuadTree.h
//
// Desc: a quad tree using rectangles
//
// The template class must have the following methods:
// 
// - size_t VerticesCount() const; 
// - T.Vertex().GetX();
// - T.Vertex().GetY();
// - T.Contains( Real x, Real y ) const;
// 
// 
// 
// - T( const T& other );
// - bool Equals( const T& other, Real tolerance ) const;
//
// The current implementation allows for duplicated items.
// 
// 
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#ifndef TRECTQUADTREE_H
#define TRECTQUADTREE_H

//-----------------------------------------------------------------------------

#include <vector>

//-----------------------------------------------------------------------------

#include "CG_Point2.h"
#include "CG_Segment2.h"

//-----------------------------------------------------------------------------

using std::vector;

//-----------------------------------------------------------------------------

template < class T, typename Real >
class TRectQuadTree
{
  class Node
  {
    Real m_left;
    Real m_top;
    Real m_right;
    Real m_bottom;

    Real m_midX;
    Real m_midY;

    Node* m_childNW;
    Node* m_childNE;
    Node* m_childSW;
    Node* m_childSE;

    size_t m_depth;

    vector< const T* > m_items;

  public:
	  /**
	  \name Constructors
	  */
	  //@{
    Node( Real left, Real top, Real right, Real bottom, size_t depth );
  	//@}

	  /**
	  \name Destructor
	  */
	  //@{
	  ~Node();
	  //@}

	  /**
	  \name Getters
	  */
	  //@{
	  //@}

	  /**
	  \name Node Manipulators
	  */
	  //@{
    void Add( const T& item, size_t maxDepth );
	  //@}

	  /**
	  \name Containment
	  */
	  //@{
    bool Contains( Real x, Real y, size_t maxDepth ) const;
    bool Contains( const CG_Point2< Real >& point, size_t maxDepth ) const;
	  //@}

  private:
    bool RectContains( Real x, Real y ) const;
    bool RectIntersect( const CG_Segment2< Real >& edge ) const;
  };

  Real m_left;
  Real m_top;
  Real m_right;
  Real m_bottom;

  size_t m_maxDepth;
  Node* m_root;

public:
	/**
	\name Constructors
	*/
	//@{
  TRectQuadTree( Real left, Real top, Real right, Real bottom, size_t maxDepth = 3 );
	//@}

	/**
	\name Destructor
	*/
	//@{
	virtual ~TRectQuadTree();
	//@}

	/**
	\name Quad tree Manipulators
	*/
	//@{
  void Add( const T& item );
	//@}

  bool Contains( Real x, Real y ) const;
};

//-----------------------------------------------------------------------------

#include "TRectQuadTree.inl"

//-----------------------------------------------------------------------------

#endif
