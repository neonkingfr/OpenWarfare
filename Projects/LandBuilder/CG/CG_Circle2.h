//-----------------------------------------------------------------------------
// File: CG_Circle2.h
//
// Desc: A two dimensional circle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a circle in two dimensions (euclidean plane XY).
//-----------------------------------------------------------------------------

#ifndef CG_CIRCLE2_H
#define CG_CIRCLE2_H

//-----------------------------------------------------------------------------

#include "CG_Polyline2.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Circle2
{
	CG_Point2< Real > m_center;
	Real              m_radius;

public:
	CG_Circle2();
	CG_Circle2( const CG_Point2< Real >& center, Real radius );
	CG_Circle2( Real centerX, Real centerY, Real radius );
	CG_Circle2( const CG_Circle2< Real >& other );

	// Getters
	// {
	const CG_Point2< Real >& GetCenter() const;
	Real GetRadius() const;
	// }

	// Setters
	// {
	void SetCenter( const CG_Point2< Real >& center );
	void SetCenter( Real centerX, Real centerY );
	void SetRadius( Real radius );
	// }

	// Circles properties
	// {
	// Returns true if this circle contains the given point.
	// If boundary is true the circle's boundary is tested for containment too.
	bool Contains( const CG_Point2< Real >& point, bool boundary, 
				   Real tolerance = CG_Consts< Real >::EPSILON ) const;

	Real Area() const;
	Real Perimeter() const;
	// }
};

//-----------------------------------------------------------------------------

// Returns true if the given circle and segment intersect.
// The intersection point is returned in the parameter intersection.
// WARNING: this implementation is unable to detect intersections between the circle and 
// the segment if both endpoints of the segment are not contained in the circle
template < typename Real >
bool Intersect( const CG_Circle2< Real >& circle, const CG_Segment2< Real >& segment, 
			    CG_Point2< Real >& intersection, Real tolerance = CG_Consts< Real >::EPSILON );

//-----------------------------------------------------------------------------

// Returns true if the given circle and polyline have at least one intersection.
// The first encountered intersection point (moving along the polyline) is returned 
// in the parameter intersection and the correspondent polyline's segment index is 
// returned in the parameter segIndex.
template < typename Real >
bool FirstIntersect( const CG_Circle2< Real >& circle, const CG_Polyline2< Real >& polyline, 
					 CG_Point2< Real >& intersection, int& segIndex, 
					 Real tolerance = CG_Consts< Real >::EPSILON );

//-----------------------------------------------------------------------------

#include "CG_Circle2.inl"

//-----------------------------------------------------------------------------

#endif
