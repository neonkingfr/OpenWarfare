//-----------------------------------------------------------------------------
// File: TRectQuadTree.inl
//
// Desc: a quad tree using rectangles
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < class T, typename Real >
TRectQuadTree< T, Real >::Node::Node( Real left, Real top, Real right, Real bottom, 
                                      size_t depth )
: m_left( left )
, m_top( top )
, m_right( right )
, m_bottom( bottom )
, m_childNW( NULL )
, m_childNE( NULL )
, m_childSW( NULL )
, m_childSE( NULL )
, m_depth( depth )
{
  m_midX = CG_Consts< Real >::HALF * (m_left + m_right);
  m_midY = CG_Consts< Real >::HALF * (m_top + m_bottom);
}

//-----------------------------------------------------------------------------

template < class T, typename Real >
TRectQuadTree< T, Real >::Node::~Node()
{
  if ( m_childNW ) { delete m_childNW; }
  if ( m_childNE ) { delete m_childNE; }
  if ( m_childSW ) { delete m_childSW; }
  if ( m_childSE ) { delete m_childSE; }
}

//-----------------------------------------------------------------------------

template < class T, typename Real >
void TRectQuadTree< T, Real >::Node::Add( const T& item, size_t maxDepth )
{
  bool contains = false;

  // checks vertices
  for ( size_t i = 0, cntI = item.VerticesCount(); i < cntI; ++i )
  {
    const CG_Point2< Real >& v = item.Vertex( i );
    if ( RectContains( v.GetX(), v.GetY() ) )
    {
      contains = true;
      break;
    }
  }

  // checks edges
  if ( !contains )
  {
    for ( size_t i = 0, cntI = item.VerticesCount() - 1; i < cntI; ++i )
    {
      if ( RectIntersect( CG_Segment2< Real >( item.Vertex( i ), item.Vertex( i + 1 ) ) ) )
      {
        contains = true;
        break;
      }
    }
  }

  if ( contains )
  {
    if ( m_depth == maxDepth )
    {
      m_items.push_back( &item );
    }
    else
    {
        if ( !m_childNW ) { m_childNW = new Node( m_left, m_top, m_midX, m_midY, m_depth + 1 ); }
        m_childNW->Add( item, maxDepth );

        if ( !m_childNE ) { m_childNE = new Node( m_midX, m_top, m_right, m_midY, m_depth + 1 ); }
        m_childNE->Add( item, maxDepth );

        if ( !m_childSE ) { m_childSE = new Node( m_midX, m_midY, m_right, m_bottom, m_depth + 1 ); }
        m_childSE->Add( item, maxDepth );

        if ( !m_childSW ) { m_childSW = new Node( m_left, m_midY, m_midX, m_bottom, m_depth + 1 ); }
        m_childSW->Add( item, maxDepth );
    }
  }
}

//-----------------------------------------------------------------------------

template < class T, typename Real >
bool TRectQuadTree< T, Real >::Node::Contains( Real x, Real y, size_t maxDepth ) const
{
  if ( m_depth == maxDepth )
  {
    for ( size_t i = 0, cntI = m_items.size(); i < cntI; ++i )
    {
      if ( m_items[i]->Contains( x, y, true ) ) { return (true); }
    }
  }
  else
  {
    if ( m_childNW && m_childNW->Contains( x, y, maxDepth ) ) { return (true); }
    if ( m_childNE && m_childNE->Contains( x, y, maxDepth ) ) { return (true); }
    if ( m_childSE && m_childSE->Contains( x, y, maxDepth ) ) { return (true); }
    if ( m_childSW && m_childSW->Contains( x, y, maxDepth ) ) { return (true); }
  }

  return (false);
}

//-----------------------------------------------------------------------------

template < class T, typename Real >
bool TRectQuadTree< T, Real >::Node::Contains( const CG_Point2< Real >& point, size_t maxDepth ) const
{
  return (Contains( point.GetX(), point.GetY() ));
}

//-----------------------------------------------------------------------------

template < class T, typename Real >
bool TRectQuadTree< T, Real >::Node::RectContains( Real x, Real y ) const
{
  if ( x < m_left )   { return (false); }
  if ( x > m_right )  { return (false); }
  if ( y < m_bottom ) { return (false); }
  if ( y > m_top )    { return (false); }

  return (true);
}

//-----------------------------------------------------------------------------

template < class T, typename Real >
bool TRectQuadTree< T, Real >::Node::RectIntersect( const CG_Segment2< Real >& edge ) const
{
  CG_Segment2< Real > leftEdge( CG_Point2< Real >( m_left, m_bottom ), CG_Point2< Real >( m_left, m_top ) );
  if ( leftEdge.IntersectsProperly( edge ) ) { return (true); }

  CG_Segment2< Real > topEdge( CG_Point2< Real >( m_left, m_top ), CG_Point2< Real >( m_right, m_top ) );
  if ( topEdge.IntersectsProperly( edge ) ) { return (true); }

  CG_Segment2< Real > rightEdge( CG_Point2< Real >( m_right, m_bottom ), CG_Point2< Real >( m_right, m_top ) );
  if ( rightEdge.IntersectsProperly( edge ) ) { return (true); }

  CG_Segment2< Real > bottomEdge( CG_Point2< Real >( m_left, m_bottom ), CG_Point2< Real >( m_right, m_bottom ) );
  if ( bottomEdge.IntersectsProperly( edge ) ) { return (true); }

  return (false);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template < class T, typename Real >
TRectQuadTree< T, Real >::TRectQuadTree( Real left, Real top, Real right, Real bottom, 
                                         size_t maxDepth )
: m_left( left )
, m_top( top )
, m_right( right )
, m_bottom( bottom )
, m_maxDepth( maxDepth )
, m_root( NULL )
{
}

//-----------------------------------------------------------------------------

template < class T, typename Real >
TRectQuadTree< T, Real >::~TRectQuadTree()
{
  if ( m_root ) { delete m_root; }
} 

//-----------------------------------------------------------------------------

template < class T, typename Real >
void TRectQuadTree< T, Real >::Add( const T& item )
{
  if ( !m_root ) { m_root = new Node( m_left, m_top, m_right, m_bottom, 0 ); }
  m_root->Add( item, m_maxDepth );
}

//-----------------------------------------------------------------------------

template < class T, typename Real >
bool TRectQuadTree< T, Real >::Contains( Real x, Real y ) const
{
  if ( !m_root ) { return (false); }
  return (m_root->Contains( x, y, m_maxDepth ));
}

//-----------------------------------------------------------------------------
