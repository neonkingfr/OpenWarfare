//-----------------------------------------------------------------------------
// File: CG_Triangle16Wrapper.inl
//
// Desc: A wrapper for triangle 1.6
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangle16Wrapper< Real >::IO::IO()
{
	Init();
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle16Wrapper< Real >::IO::Init()
{
	pointlist                  = NULL;
	pointattributelist         = NULL;
	pointmarkerlist            = NULL;
	numberofpoints             = 0;
	numberofpointattributes    = 0;
	trianglelist               = NULL;
	triangleattributelist      = NULL;
	trianglearealist           = NULL;
	neighborlist               = NULL;
	numberoftriangles          = 0;
	numberofcorners            = 0;
	numberoftriangleattributes = 0;
	segmentlist                = NULL;
	segmentmarkerlist          = NULL;
	numberofsegments           = 0;
	holelist                   = NULL;
	numberofholes              = 0;
	regionlist                 = NULL;
	numberofregions            = 0;
	edgelist                   = NULL;
	edgemarkerlist             = NULL;
	normlist                   = NULL;
	numberofedges              = 0;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangle16Wrapper< Real >::CG_Triangle16Wrapper( CG_Polygon2< Real >* pPolygon )
: m_pPolygon( pPolygon )
, CG_Triangulation()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle16Wrapper< Real >::Triangulate( const string& switches )
{
	size_t holesCount = 0;

	if ( m_pPolygon->GetType() == CG_Polygon2< Real >::PolyType_HOLED )
	{
		holesCount = reinterpret_cast< CG_HoledPolygon2< Real >* >( m_pPolygon )->HolesCount();
	}

	size_t verticesCount = m_pPolygon->VerticesCount();

	// ensures that all polygons are CCW ordered
	m_pPolygon->SetVerticesOrderToCCW();

	if ( holesCount > 0 )
	{
		vector< CG_Polygon2< Real > >& holes = reinterpret_cast< CG_HoledPolygon2< Real >* >( m_pPolygon )->GetHoles();

		for ( size_t i = 0; i < holesCount; ++i )
		{
			verticesCount += holes[i].VerticesCount();

			// ensures that all polygons are CCW ordered
			holes[i].SetVerticesOrderToCCW();
		}		
	}

	// SETUP POINTLIST
	// ------------------------------------------------------
	double* pPointList = new double[verticesCount * 2];
	size_t pointsCounter = 0;

	for ( size_t i = 0, cntI = m_pPolygon->VerticesCount(); i < cntI; ++i )
	{
		const CG_Point2< Real >& p = m_pPolygon->Vertex( i );
		pPointList[pointsCounter++] = static_cast< double >( p.GetX() );
		pPointList[pointsCounter++] = static_cast< double >( p.GetY() );
	}

	if ( holesCount > 0 )
	{
		vector< CG_Polygon2< Real > >& holes = reinterpret_cast< CG_HoledPolygon2< Real >* >( m_pPolygon )->GetHoles();

		for ( size_t i = 0; i < holesCount; ++i )
		{
			const CG_Polygon2< Real >& hole = holes[i];
			for ( size_t j = 0, cntJ = hole.VerticesCount(); j < cntJ; ++j )
			{
				const CG_Point2< Real >& p = hole.Vertex( j );
				pPointList[pointsCounter++] = static_cast< double >( p.GetX() );
				pPointList[pointsCounter++] = static_cast< double >( p.GetY() );
			}
		}
	}
	// ------------------------------------------------------

	// SETUP POINTMARKERLIST
	// ------------------------------------------------------
	int* pPointMarkerList = new int[verticesCount];
	size_t pointMarkersCounter = 0;

	for ( size_t i = 0, cntI = m_pPolygon->VerticesCount(); i < cntI; ++i )
	{
		pPointMarkerList[pointMarkersCounter++] = 1;
	}

	if ( holesCount > 0 )
	{
		vector< CG_Polygon2< Real > >& holes = reinterpret_cast< CG_HoledPolygon2< Real >* >( m_pPolygon )->GetHoles();

		for ( size_t i = 0; i < holesCount; ++i )
		{
			const CG_Polygon2< Real >& hole = holes[i];
			for ( size_t j = 0, cntJ = hole.VerticesCount(); j < cntJ; ++j )
			{
				pPointMarkerList[pointMarkersCounter++] = static_cast< int >( i + 1 );
			}
		}
	}
	// ------------------------------------------------------

	// SETUP SEGMENTLIST
	// ------------------------------------------------------
	int* pSegmentList = new int[verticesCount * 2];
	size_t segmentsCounter = 0;
	size_t verticesCounter = 0;

	for ( size_t i = 0, cntI = m_pPolygon->VerticesCount(); i < cntI; ++i )
	{
		pSegmentList[segmentsCounter++] = static_cast< int >( i + 1 );
		if ( i < cntI - 1 )
		{
			pSegmentList[segmentsCounter++] = static_cast< int >( i + 2 );
		}
		else
		{
			pSegmentList[segmentsCounter++] = 1;
		}
	}

	verticesCounter += m_pPolygon->VerticesCount();

	if ( holesCount > 0 )
	{
		vector< CG_Polygon2< Real > >& holes = reinterpret_cast< CG_HoledPolygon2< Real >* >( m_pPolygon )->GetHoles();

		for ( size_t i = 0; i < holesCount; ++i )
		{
			const CG_Polygon2< Real >& hole = holes[i];
			for ( size_t j = 0, cntJ = hole.VerticesCount(); j < cntJ; ++j )
			{
				pSegmentList[segmentsCounter++] = static_cast< int >( verticesCounter + j + 1 );
				if ( j < cntJ - 1 )
				{
					pSegmentList[segmentsCounter++] = static_cast< int >( verticesCounter + j + 2 );
				}
				else
				{
					pSegmentList[segmentsCounter++] = static_cast< int >( verticesCounter + 1 );
				}
			}
			verticesCounter += hole.VerticesCount();
		}
	}
	// ------------------------------------------------------

	// SETUP SEGMARKERLIST
	// ------------------------------------------------------
	int* pSegMarkerList = new int[verticesCount];
	size_t segMarkersCounter = 0;

	for ( size_t i = 0, cntI = m_pPolygon->VerticesCount(); i < cntI; ++i )
	{
		pSegMarkerList[segMarkersCounter++] = 1;
	}

	if ( holesCount > 0 )
	{
		vector< CG_Polygon2< Real > >& holes = reinterpret_cast< CG_HoledPolygon2< Real >* >( m_pPolygon )->GetHoles();

		for ( size_t i = 0; i < holesCount; ++i )
		{
			const CG_Polygon2< Real >& hole = holes[i];
			for ( size_t j = 0, cntJ = hole.VerticesCount(); j < cntJ; ++j )
			{
				pSegMarkerList[segMarkersCounter++] = static_cast< int >( i + 1 );
			}
		}
	}
	// ------------------------------------------------------

	// SETUP HOLELIST
	// ------------------------------------------------------
	double* pHoleList = NULL;
	if ( holesCount > 0 )
	{
		vector< CG_Polygon2< Real > >& holes = reinterpret_cast< CG_HoledPolygon2< Real >* >( m_pPolygon )->GetHoles();

		pHoleList = new double[holesCount * 2];
		size_t holesCounter = 0;

		for ( size_t i = 0; i < holesCount; ++i )
		{
			const CG_Polygon2< Real >& hole = holes[i];
			CG_Point2< Real > p = hole.GetLongestInternalDiagonal().MidPoint();
			pHoleList[holesCounter++] = static_cast< double >( p.GetX() );
			pHoleList[holesCounter++] = static_cast< double >( p.GetY() );
		}
	}
	// ------------------------------------------------------

	// SETUP INPUT STRUCTURE
	// ------------------------------------------------------
	m_triIN.pointlist         = pPointList;
	m_triIN.pointmarkerlist   = pPointMarkerList;
	m_triIN.numberofpoints    = static_cast< int >( verticesCount );
	m_triIN.segmentlist       = pSegmentList;
	m_triIN.segmentmarkerlist = pSegMarkerList;
	m_triIN.numberofsegments  = static_cast< int >( verticesCount );
	m_triIN.holelist          = pHoleList;
	m_triIN.numberofholes     = static_cast< int >( holesCount );
 	// ------------------------------------------------------

	// CALL TRIANGLE 1.6
	// ------------------------------------------------------
	triangulate( const_cast< char* >( switches.c_str() ), &m_triIN, &m_triOUT, &m_vorOUT );
	// ------------------------------------------------------

	// CLEANUP 
	// ------------------------------------------------------
	if ( pPointList )       { delete [] pPointList; }
	if ( pPointMarkerList ) { delete [] pPointMarkerList; }
	if ( pSegmentList )     { delete [] pSegmentList; }
	if ( pSegMarkerList )   { delete [] pSegMarkerList; }
	if ( pHoleList )        { delete [] pHoleList; }
// ------------------------------------------------------
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangle16Wrapper< Real >::MakeTrianglesList()
{
	CG_Point2< Real > points[3];

	m_triangles.clear();

	size_t triCount = static_cast< size_t >( m_triOUT.numberoftriangles );
	int index;
	for ( size_t i = 0; i < triCount; ++i )
	{
		for ( size_t j = 0; j < 3; ++j )
		{
			index = m_triOUT.trianglelist[i * 3 + j] - 1;
			points[j] = CG_Point2< Real >( m_triOUT.pointlist[index * 2], 
										   m_triOUT.pointlist[index * 2 + 1] );
		}

		m_triangles.push_back( CG_Triangle2< Real >( points[0], points[1], points[2] ) );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Point2< Real > > CG_Triangle16Wrapper< Real >::GetPointsList() const
{
	vector< CG_Point2< Real > > pointsList;

	size_t pointsCount = static_cast< size_t >( m_triOUT.numberofpoints );
	for ( size_t i = 0; i < pointsCount; ++i )
	{
		pointsList.push_back( CG_Point2< Real >( m_triOUT.pointlist[i * 2], m_triOUT.pointlist[i * 2 + 1] ) );
	}

	return (pointsList);
}

//-----------------------------------------------------------------------------

template < typename Real >
long CG_Triangle16Wrapper< Real >::GetNumberOfSegments() const
{
	if (m_triangles.size() < 1) return 0;
	if (m_triangles.size() == 1) return 3;
	return ( static_cast< long >( m_triOUT.numberofsegments ) );
}

//-----------------------------------------------------------------------------
