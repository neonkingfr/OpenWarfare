//-----------------------------------------------------------------------------
// File: CG_Triangle16Wrapper.h
//
// Desc: A wrapper for triangle 1.6
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#ifndef CG_TRIANGLE16WRAPPER_H
#define CG_TRIANGLE16WRAPPER_H

//-----------------------------------------------------------------------------

#include <vector>
#include <string>

//-----------------------------------------------------------------------------

#include "CG_HoledPolygon2.h"
#include "triangle_1_6\triangle.h"

//-----------------------------------------------------------------------------

using std::vector;
using std::string;

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Triangle16Wrapper : public CG_Triangulation< Real >
{
	class IO : public triangulateio
	{
	public:
		IO();

	private:
		void Init();
	};

	CG_Polygon2< Real >* m_pPolygon;

	IO m_triIN;
	IO m_triOUT;
	IO m_vorOUT;

public:
	CG_Triangle16Wrapper( CG_Polygon2< Real >* pPolygon );

	virtual void Triangulate( const string& switches );

	void MakeTrianglesList();
	virtual vector< CG_Point2< Real > > GetPointsList() const;
	virtual long GetNumberOfSegments() const;
};

//-----------------------------------------------------------------------------

#include "CG_Triangle16Wrapper.inl"

//-----------------------------------------------------------------------------

#endif
