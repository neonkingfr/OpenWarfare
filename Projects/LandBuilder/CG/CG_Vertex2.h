//-----------------------------------------------------------------------------
// File: CG_Vertex2.h
//
// Desc: A two dimensional vertex
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a vertex in two dimensions (euclidean plane XY).
// Is vertex is just a point with some non-geometrical attribute.
//-----------------------------------------------------------------------------

#ifndef CG_VERTEX2_H
#define CG_VERTEX2_H

//-----------------------------------------------------------------------------

#include "CG_Point2.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Vertex2 : public CG_Point2< Real >
{
	int m_id;

public:
	CG_Vertex2();
	CG_Vertex2( Real x, Real y );
	CG_Vertex2( Real x, Real y, int id );
	CG_Vertex2( const CG_Vertex2< Real >& other );

	virtual ~CG_Vertex2();

	// Getters
	// {
	int GetID() const;
	// }

	// Setters
	// {
	void SetID( int id );
	// }
};

//-----------------------------------------------------------------------------

#include "CG_Vertex2.inl"

//-----------------------------------------------------------------------------

#endif