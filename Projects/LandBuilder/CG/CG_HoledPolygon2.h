//-----------------------------------------------------------------------------
// File: CG_HoledPolygon2.h
//
// Desc: A two dimensional polygon with holes
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a polygon with internal holes in two dimensions 
// (euclidean plane XY).
//-----------------------------------------------------------------------------

#ifndef CG_HOLEDPOLYGON2_H
#define CG_HOLEDPOLYGON2_H

//-----------------------------------------------------------------------------

#include "CG_Polygon2.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_HoledPolygon2 : public CG_Polygon2< Real >
{
	// The contours of the holes in this polygon
	vector< CG_Polygon2< Real > > m_holes;

public:
	CG_HoledPolygon2();
	CG_HoledPolygon2( const CG_Polygon2< Real >& polygon );
	CG_HoledPolygon2( const CG_HoledPolygon2< Real >& other );
	virtual ~CG_HoledPolygon2();

	// Assignement
	CG_HoledPolygon2< Real >& operator = ( const CG_HoledPolygon2< Real >& other );

	// Getters
	// {
	const vector< CG_Polygon2< Real > >& GetHoles() const;
	vector< CG_Polygon2< Real > >& GetHoles();
	// }

	// Holed Polygon manipulators
	// {
	// Translates this polygon using the given displacements.
	virtual void Translate( Real dx, Real dy );
	virtual void Translate( const CG_Vector2< Real >& displacement );

	// Rotates this polygon around the given center by the 
	// given angle in radians (positive CCW).
	virtual void Rotate( const CG_Point2< Real >& center, Real angle );

	// Rotates this polygon around the its barycenter by the 
	// given angle in radians (positive CCW).
	virtual void RotateAroundBarycenter( Real angle );

	// Generates the triangulation associated with this polygon using
	// the specified algorithm 
	virtual void Triangulate( const ETriangulationMethod& method = PolyTri_TRIANGLE16 );

	// Generates the sub parts associated with this polygon using
	// the specified algorithm for the triangulation
	virtual void Partition( const ETriangulationMethod& method = PolyTri_TRIANGLE16 );

	// Tries to orient the edges of this polygon as its boundary rectangle's edges
	virtual void Rectify();
	// }

	// Holes manipulators
	// {
	// Adds a hole to this polygon
	void AddHole( const CG_Polygon2< Real >& hole );
	// }

	// Holed Polygon properties
	// {
	// Returns the number of holes of this polygon
	size_t HolesCount() const;
	// }

	// Containment
	// {
	// Returns true if this polygon contains the given point
	virtual bool Contains( const CG_Point2< Real >& point, bool boundaries );

	// Returns true if this polygon contains the given polygon
	virtual bool Contains( const CG_Polygon2< Real >& polygon, bool boundaries );
	// }
};

//-----------------------------------------------------------------------------

#include "CG_HoledPolygon2.inl"

//-----------------------------------------------------------------------------

#endif
