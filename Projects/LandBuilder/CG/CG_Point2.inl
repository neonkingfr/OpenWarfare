//-----------------------------------------------------------------------------
// File: CG_Point2.inl
//
// Desc: A two dimensional point
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real >::CG_Point2( Real x, Real y )
: m_x( x )
, m_y( y )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real >::CG_Point2( const CG_Point2< Real >& other )
: m_x( other.m_x )
, m_y( other.m_y )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real >::~CG_Point2()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real >& CG_Point2< Real >::operator = ( const CG_Point2< Real >& other )
{
	m_x = other.m_x;
	m_y = other.m_y;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Point2< Real >::Equals( const CG_Point2< Real >& other, Real tolerance ) const
{
	if ( this == &other ) { return (true); }

	if ( abs( other.m_x - m_x ) > tolerance ) { return (false); }
	if ( abs( other.m_y - m_y ) > tolerance ) { return (false); }

	return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Point2< Real >::GetX() const
{
	return (m_x);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Point2< Real >::GetY() const
{
	return (m_y);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point2< Real >::SetX( Real x )
{
	m_x = x;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point2< Real >::SetY( Real y )
{
	m_y = y;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point2< Real >::Set( const CG_Point2< Real >& other )
{
	m_x = other.m_x;
	m_y = other.m_y;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point2< Real >::Translate( Real dx, Real dy )
{
	m_x += dx;
	m_y += dy;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point2< Real >::Rotate( const CG_Point2< Real >& center, Real angle )
{
	Real cX = center.GetX();
	Real cY = center.GetY();
	Real dx = m_x - cX;
	Real dy = m_y - cY;
	Real c = cos( angle );
	Real s = sin( angle );

	m_x = cX + dx * c - dy * s;
	m_y = cY + dx * s + dy * c;
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Point2< Real >::SquaredDistance( const CG_Point2< Real >& other ) const
{
	Real dx = other.m_x - m_x;
	Real dy = other.m_y - m_y;
	return (dx * dx + dy * dy);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Point2< Real >::Distance( const CG_Point2< Real >& other ) const
{
	return (sqrt( SquaredDistance( other ) ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Point2< Real >::Translated( Real dx, Real dy ) const
{
	CG_Point2< Real > retP( *this );
	retP.Translate( dx, dy );
	return (retP);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Point2< Real >::Rotated( const CG_Point2< Real >& center, Real angle ) const
{
	CG_Point2< Real > retP( *this );
	retP.Rotate( center, angle );
	return (retP);
}

//-----------------------------------------------------------------------------

template < typename Real >
std::ostream& operator << ( std::ostream& o, const CG_Point2< Real >& p )
{
  char buff[80];
  sprintf_s( buff, "(%.3f, %.3f)", p.GetX(), p.GetY() );
  string msg( buff );

	o << msg;
  return (o);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool Collinear( const CG_Point2< Real >& p1, const CG_Point2< Real >& p2,
                const CG_Point2< Real >& p3, Real tolerance )
{
	// we use the triangle formed with the three points to test collinearity
	// we don't use just the triangle's area, because for triangles with a 
	// long base, this could lead to areas greater then the tolerance even
	// if the points are quasi-aligned
	// instead we check the height of the triangle (the base being the longest
	// triangle's edge)
	Real sqDist12 = p1.SquaredDistance( p2 );
	Real sqDist23 = p2.SquaredDistance( p3 );
	Real sqDist31 = p3.SquaredDistance( p1 );

	Real maxSqDist = std::max( sqDist12, std::max( sqDist23, sqDist31 ) );
	if ( maxSqDist == static_cast< Real >( 0 ) ) { return (true); }

	Real triArea = abs( p1.GetX() * p2.GetY() - p1.GetY() * p2.GetX() +
                      p2.GetX() * p3.GetY() - p2.GetY() * p3.GetX() +
                      p3.GetX() * p1.GetY() - p3.GetY() * p1.GetX() ) / static_cast< Real >( 2 );

	Real triHeight = triArea / sqrt( maxSqDist );
	if ( triHeight <= tolerance )
	{
		return (true);
	}
	else
	{
		return (false);
	}
}

//-----------------------------------------------------------------------------
