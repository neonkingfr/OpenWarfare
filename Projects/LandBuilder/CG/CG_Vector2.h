//-----------------------------------------------------------------------------
// File: CG_Vector2.h
//
// Desc: A two dimensional vector
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a vector in two dimensions (euclidean plane XY).
//-----------------------------------------------------------------------------

#ifndef CG_VECTOR2_H
#define CG_VECTOR2_H

//-----------------------------------------------------------------------------

#include "CG_Point2.h"
#include "CG_Consts.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Vector2 
{
	Real m_x;
	Real m_y;

public:
	CG_Vector2();
	CG_Vector2( Real x, Real y );
	// Construct the vector (head - tail)
	CG_Vector2( const CG_Point2< Real >& head, 
				const CG_Point2< Real >& tail = CG_Point2< Real >( static_cast< Real >( 0 ), static_cast< Real >( 0 ) ));
	// Construct the unit vector having the given direction
	CG_Vector2( Real direction );
	CG_Vector2( const CG_Vector2< Real >& other );
	~CG_Vector2();

	// Assignement
	CG_Vector2< Real >& operator = ( const CG_Vector2< Real >& other );

	// Arithmetic updates
	CG_Vector2< Real >& operator += ( const CG_Vector2< Real >& other );
	CG_Vector2< Real >& operator -= ( const CG_Vector2< Real >& other );
	CG_Vector2< Real >& operator *= ( const Real scalar );

	// Arithmetic operations
	CG_Vector2< Real > operator + ( const CG_Vector2< Real >& other ) const;
	CG_Vector2< Real > operator - ( const CG_Vector2< Real >& other ) const;
	CG_Vector2< Real > operator * ( const Real scalar ) const;

	// Getters
	// {
	Real GetX() const;
	Real GetY() const;
	// }

	// Setters
	// {
	void SetX( Real x );
	void SetY( Real y );
	// }

	// Vector properties
	// {
	Real SquaredLength() const;
	Real Length() const;

	// returns the direction (angle between the world's X axis and this 
	// vector) in radians (positive CCW)
	Real Direction() const;
	// }

	// Vector manipulators
	// {
	// Rotates this vector by the given angle (in radians and positive CCW)
	void Rotate( Real angle );

	// Normalizes this vector
	void Normalize();
	// }

	// Operations with vectors
	// {
	CG_Vector2< Real > operator - () const;

	// Returns the dot product between this vector and the given one.
	Real Dot( const CG_Vector2< Real >& other ) const;

	// Returns the pseudo-cross (the signed module of the cross-product vector) 
	// product between this vector and the given one.
	Real Cross( const CG_Vector2< Real >& other ) const;

	// Returns a normalized copy of this vector 
	CG_Vector2< Real > Normalized() const;

	// Returns a copy of this vector rotated by the given angle
	// (in radians and positive CCW)
	CG_Vector2< Real > Rotated( Real angle ) const;

	// returns the vector perpendicular to this and directed to its right
	CG_Vector2< Real > PerpCW() const;

	// returns the vector perpendicular to this and directed to its left
	CG_Vector2< Real > PerpCCW() const;

	// returns the head of this vector when applied to the given point
	CG_Point2< Real > Head( const CG_Point2< Real >& tail = CG_Point2< Real >() ) const;

	// returns the tail (the application point) of this vector when 
	// the head is in the given point
	CG_Point2< Real > Tail( const CG_Point2< Real >& head ) const;
	// }
};

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > operator * ( const Real scalar, const CG_Vector2< Real >& v );

//-----------------------------------------------------------------------------

#include "CG_Vector2.inl"

//-----------------------------------------------------------------------------

#endif