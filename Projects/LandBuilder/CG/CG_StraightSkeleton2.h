//-----------------------------------------------------------------------------
// File: CG_StraightSkeleton2.h
//
// Desc: the straight skeleton of a two dimensional polygon
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#ifndef CG_STRAIGHTSKELETON2_H
#define CG_STRAIGHTSKELETON2_H

//-----------------------------------------------------------------------------

#include <vector>

//-----------------------------------------------------------------------------

#include "CG_Segment2.h"
#include "TCircularList.h"

//-----------------------------------------------------------------------------

using std::vector;

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Polygon2;

//-----------------------------------------------------------------------------

template < typename Real >
class SSVertex : public CG_Polygon2< Real >::PVertex
{
  size_t             m_prevEdgeIndex;
  size_t             m_nextEdgeIndex;
  CG_Vector2< Real > m_bisector;
  size_t             m_va;
  size_t             m_vb;
  Real               m_distance;

public:
  SSVertex();
  SSVertex( Real x, Real y );
  SSVertex( const SSVertex& other );
  virtual ~SSVertex();

  // Assignement
  SSVertex< Real >& operator = ( const SSVertex< Real >& other );

  // Getters
  // {
  size_t GetPrevEdgeIndex() const;
  size_t GetNextEdgeIndex() const;
  const CG_Vector2< Real >& GetBisector() const;
  size_t GetVa() const;
  size_t GetVb() const;
  Real GetDistance() const;
  // }

  // Setters
  // {
  void SetPrevEdgeIndex( size_t index );
  void SetNextEdgeIndex( size_t index );
  void SetBisector( const CG_Vector2< Real >& bisector );
  void SetVa( size_t va );
  void SetVb( size_t vb );
  void SetDistance( Real distance );
  // }
};

//-----------------------------------------------------------------------------

template < typename Real >
class CG_StraightSkeleton2
{
public:
  typedef enum
  {
    ENRICO
  } EAlgorithms;

  class Node
  {
    Node*             m_parent;
    CG_Point2< Real > m_data;
    vector< Node* >   m_children;

  public:
    /**
    \name Constructor
    */
    //@{
    Node( Node* parent, const CG_Point2< Real >& data );
    //@}

    /**
    \name Destructor
    */
    //@{
    ~Node();
    //@}

    /**
    \name Getters
    */
    //@{
    CG_Point2< Real >& GetData();
    const CG_Point2< Real >& GetData() const;
    vector< Node* >& GetChildren();
    const vector< Node* >& GetChildren() const;
    //@}

    /**
    \name Manipulators
    */
    //@{
    void AddChild( const CG_Point2< Real >& point, Real tolerance = static_cast< Real >( 0.00001 )  );
    void RemoveAllChildren();
    void RemoveOverlappingBones( Real tolerance );
    //@}

    /**
    \name Queries
    */
    //@{
    Node* Find( const CG_Point2< Real >& point, Real tolerance );
    //@}

    /**
    \name Properties
    */
    //@{
    size_t DescendantsCount() const;
    vector< CG_Segment2< Real > > AttachedBones() const;
    vector< CG_Point2< Real > > Leaves() const;
    bool IsLeaf() const;
    bool IsRoot() const;
    vector< CG_Point2< Real > > PathTo( const Node* caller,
                                        const CG_Point2< Real >& point,
                                        Real tolerance = static_cast< Real >( 0.00001 ) ) const;
    //@}

  private:
    void _ChangeParent( Node* newParent );
    void _AddChild( Node* child );
    void _ExtractChild( Node* child );
  };

  CG_Polygon2< Real >* m_polygon;
  EAlgorithms m_algorithm;
  Node* m_root;

public:
  /**
  \name Constructor
  */
  //@{
  CG_StraightSkeleton2( CG_Polygon2< Real >* polygon, EAlgorithms algorithm = ENRICO );
  CG_StraightSkeleton2( const CG_StraightSkeleton2< Real >& other );
  //@}

  /**
  \name Destructor
  */
  //@{
  ~CG_StraightSkeleton2();
  //@}

  /**
  \name Manipulators
  */
  //@{
  bool AddBone( const CG_Segment2< Real >& bone, Real tolerance = static_cast< Real >( 0.00001 ) );
  //@}

  /**
  \name Properties
  */
  //@{
  size_t BonesCount() const;
  vector< CG_Segment2< Real > > Bones() const;
  vector< CG_Point2< Real > > Leaves() const;
  vector< CG_Point2< Real > > PathConnecting( const CG_Point2< Real >& p1,
                                              const CG_Point2< Real >& p2,
                                              Real tolerance = static_cast< Real >( 0.00001 ) ) const;
  //@}

private:
  void _CalculateEnrico();
  void _CopyFrom( const CG_StraightSkeleton2< Real >& other );
  void _ClearNodes();
  bool _CoreEnrico();
  void _RemoveOverlappingBones( Real tolerance = static_cast< Real >( 0.00001 ) );
  SSVertex< Real > _ClosestNodeCandidate( const TCircularList< SSVertex< Real > >& lav,
                                          int vertexIndex );
};

//-----------------------------------------------------------------------------

#include "CG_StraightSkeleton2.inl"

//-----------------------------------------------------------------------------

#endif
