//-----------------------------------------------------------------------------
// File: CG_Point3.h
//
// Desc: A three dimensional point
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a point in three dimensions (euclidean space XYZ).
//-----------------------------------------------------------------------------

#ifndef CG_POINT3_H
#define CG_POINT3_H

//-----------------------------------------------------------------------------

#include <complex>
#include "CG_Matrix3x3.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Vector3;

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Point3
{
protected:
	Real m_x;
	Real m_y;
	Real m_z;

public:
	CG_Point3( Real x = CG_Consts< Real >::ZERO, Real y = CG_Consts< Real >::ZERO,
             Real z = CG_Consts< Real >::ZERO );
	CG_Point3( const CG_Point3< Real >& other );
	virtual ~CG_Point3();

	// Assignement
	CG_Point3< Real >& operator = ( const CG_Point3< Real >& other );

	// Comparison
	bool Equals( const CG_Point3< Real >& other,
               Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

	// Getters
	// {
	Real GetX() const;
	Real GetY() const;
	Real GetZ() const;
	// }

	// Setters
	// {
	void SetX( Real x );
	void SetY( Real y );
	void SetZ( Real z );
	// }

	// Point manipulators
	// {
	// Translates this point using the given displacements
	void Translate( Real dx, Real dy, Real dz );

	// Rotates CCW this point around the given axis passing through the given
	// center by the given angle in radians
	void Rotate( const CG_Vector3< Real >& axis, Real angle,
               const CG_Point3< Real >& center = CG_Point3< Real >() );

	// Rotates this point using the given rotation matrix
	void Rotate( const CG_Matrix3x3< Real >& matrix );
	// }

	// Operations with points
	// {
	Real SquaredDistance( const CG_Point3< Real >& other ) const;
	Real Distance( const CG_Point3< Real >& other ) const;

	// returns a copy of this point translated by the given displacements
	CG_Point3< Real > Translated( Real dx, Real dy, Real dz );
	// }
};

//-----------------------------------------------------------------------------

template < typename Real >
std::ostream& operator << ( std::ostream& o, const CG_Point3< Real >& p );

//-----------------------------------------------------------------------------

#include "CG_Point3.inl"

//-----------------------------------------------------------------------------

#endif