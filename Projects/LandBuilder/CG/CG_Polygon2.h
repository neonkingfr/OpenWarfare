//-----------------------------------------------------------------------------
// File: CG_Polygon2.h
//
// Desc: A two dimensional polygon
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a polygon in two dimensions (euclidean plane XY)
// The vertices as stored in a circular list. A pointer to the list's head 
// is stored in the member variable m_pVertices
//-----------------------------------------------------------------------------

#ifndef CG_POLYGON2_H
#define CG_POLYGON2_H

//-----------------------------------------------------------------------------

#include <assert.h>
#include <limits>
#include <string>
#include <vector>

//-----------------------------------------------------------------------------

using std::string;
using std::vector;

//-----------------------------------------------------------------------------

#include "TCircularList.h"
#include "CG_Vertex2.h"
#include "CG_Vector2.h"
#include "CG_Rectangle2.h"
#include "CG_Segment2.h"
#include "CG_Triangle2.h"
#include "CG_StraightSkeleton2.h"
#include "CG_Triangulation.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Triangle16Wrapper;

template < typename Real >
class CG_Polyline2;

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Polygon2
{
public:
	class PVertex : public CG_Point2< Real >
	{
		bool m_isConvex;

	public:
		PVertex();
		PVertex( Real x, Real y );
		PVertex( const PVertex& other );
		virtual ~PVertex();

		// Assignement
		PVertex& operator = ( const PVertex& other );

		// Getters
		// {
		bool IsConvex() const;
		// }

		// Setters
		// {
		void SetConvex( bool convex );
		// }
	};

public:
	typedef enum
	{
		PolyType_NORMAL,
		PolyType_HOLED
	} EPolygonType;

protected:
	// The type of this polygon
	EPolygonType m_type;

	// The vertices of this polygon
	TCircularList< PVertex > m_vertices;

	// The triangulation associated to this polygon
	CG_Triangulation< Real >* m_triangulation;

	// The sub parts in which this polygon can be divided
	vector< CG_Polygon2< Real > > m_partitions;

public:
	typedef enum
	{
		PolyTri_TRIANGLE16
	} ETriangulationMethod;

	CG_Polygon2();
	CG_Polygon2( const CG_Polygon2< Real >& other );
	virtual ~CG_Polygon2();

	// Assignement
	CG_Polygon2< Real >& operator = ( const CG_Polygon2< Real >& other );

	// Comparison
	bool Equals( const CG_Polygon2< Real >& other,
               Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;
	bool EqualsTurningSequence( const CG_Polygon2< Real >& other ) const;

	// Polygon manipulators
	// {
	// Translates this polygon using the given displacements.
	virtual void Translate( Real dx, Real dy );
	virtual void Translate( const CG_Vector2< Real >& displacement );

	// Rotates this polygon around the given center by the 
	// given angle in radians (positive CCW).
	virtual void Rotate( const CG_Point2< Real >& center, Real angle );

	// Rotates this polygon around the its barycenter by the 
	// given angle in radians (positive CCW).
	virtual void RotateAroundBarycenter( Real angle );

	// Generates the triangulation associated with this polygon using
	// the specified algorithm 
	virtual void Triangulate( const ETriangulationMethod& method = PolyTri_TRIANGLE16 );

	// Returns the triangulation associated with this polygon
	virtual const CG_Triangulation< Real > &CG_Polygon2< Real >::GetTriangulation() const;

	// Generates the sub parts associated with this polygon using
	// the specified algorithm for the triangulation
	virtual void Partition( const ETriangulationMethod& method = PolyTri_TRIANGLE16 );

	// Tries to orient the edges of this polygon as its boundary rectangle's edges
	virtual void Rectify();

	// Calculates and sets the convex field of every vertex of this polygon
	void SetVerticesConvexity();
	// }

	// Vertices manipulators
	// {
	// Appends the given vertex.
	void AppendVertex( Real x, Real y );
	void AppendVertex( const CG_Point2< Real >& vertex );

	// Removes all vertices.
	void RemoveAllVertices();

	// Inserts the given vertex in the given position
	void InsertVertex( size_t index, const CG_Point2< Real >& vertex );

	// Inserts a vertex on the edge of this polygon with the given index
	// at the given distance from the first vertex of the edge
	// The vertices' order DOES matter.
	void InsertVertexOnEdge( size_t edgeIndex, Real distanceFromFirstVertexOnEdge );

	// Inserts a vertex on the edge of this polygon having the given point as first vertex
	// at the given distance from that point
	// The vertices' order DOES matter.
	void InsertVertexOnEdge( const CG_Point2< Real >& vertex, Real distance );

	// Removes consecutive duplicated vertices (only the first of 
	// them will be left).
	// Returns the number of removed vertices.
	// The vertices' order does not matter.
	size_t RemoveConsecutiveDuplicatedVertices( Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE );

	// Tries to remove consecutive aligned vertices.
	// The check is done using three consecutive vertices at a time
	// and removing the central one.
	// Returns the number of removed vertices.
	// The vertices' order does not matter.
	size_t RemoveConsecutiveAlignedVertices( Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE );

	// Reverses the current vertices' order.
	void ReverseVerticesOrder();

	// Sets the vertices to be in clockwise order.
	void SetVerticesOrderToCW();

	// Sets the vertices to be in counterclockwise order.
	void SetVerticesOrderToCCW();

	// Sets the longest edge of this polygon as first one.
	// (slides the head of the circular list until it become the first
	// vertex of the longest edge)
	void SetLongestEdgeAsFirst();

	// Slides the head of the circular list count times
	void SlideVertices( int count );

	// Inserts in every edge of this polygon the specified count of sections
	// of the given length
	// If count == -1 the number of section will be automatically calculated
	// in dependence of the edges' lengths
	// The parameter margin is the length close to edges' endpoints we must
	// be left untouched
	size_t InsertConstantLengthSections( int count, Real length, Real margin );

	// Modifies the head of the circular list of vertices to point
	// to the first endpoint of the given segment
	// Returns true if successfull
	// The vertices' order DOES matter.
	bool SetAsStartingEdge( const CG_Segment2< Real >& segment );
	// }

	// Polygon properties
	// {
	// Returns the type of this polygon
	EPolygonType GetType() const;

	// Returns the number of vertices of this polygon.
	size_t VerticesCount() const;

	// Returns true if the vertices of this polygon are CCW ordered
	bool IsVerticesOrderCCW() const;

	// Returns true if the number of vertices is lesser than 3
	bool IsDegenerated() const;

	// Returns true if there are duplicated vertices (even the non-consecutive ones)
	bool HasDuplicatedVertices() const;

	// Returns true if there is any self intersection
   bool HasSelfIntersections() const;

	// Return true if there is any vertex on an edge
   bool HasVerticesOnEdges() const;

	// Returns true if this polygon is simple
   bool IsSimple() const;

	// Returns the vertex of this polyline at the given index.
	const PVertex& Vertex( size_t index ) const;
	PVertex& Vertex( size_t index );

	// Returns the edge of this polyline at the given index.
	CG_Segment2< Real > Edge( size_t index ) const;

	// Returns a pointer to the vertex of this polygon which coincide with
	// the given point or NULL if no match is found
	const PVertex* FindVertex( const CG_Point2< Real >& point ) const;

	// Returns the index of the vertex of this polygon which coincide with
	// the given point or -1 if no match is found
	int FindVertexIndex( const CG_Point2< Real >& point ) const;

	// Returns the index of the edge of this polygon matching the given segment
	// or -1 if no match is found
	int FindEdgeIndex( const CG_Segment2< Real >& segment,
                     Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

	// Returns the signed area of this polygon.
	Real SignedArea() const;

	// Returns the area of this polygon.
	Real Area() const;

	// Returns the perimeter of this polygon.
	Real Perimeter() const;

	// Returns the minimum rectangle containing this polygon.
	CG_Rectangle2< Real > MinimumBoundingBox() const;

	// Returns the axis aligned rectangle containing this polygon.
	CG_Rectangle2< Real > AxisAlignedBoundingBox() const;

	// Returns the barycenter of the vertices of this polygon.
	CG_Point2< Real > Barycenter() const;

	// Returns the extreme values of the coordinates of the vertices
	// of this polygon
	Real MinX() const;
	Real MaxX() const;
	Real MinY() const;
	Real MaxY() const;

	// Returns the turning sequence (a string of L, R and S - where L = left,
	// R = right and S = straight) of the corners of this polygon
	// The vertices' order DOES matter.
	string TurningSequence() const;

	// Returns the index of the longest edge or -1 if the polygon
	// is degenerate
	int LongestEdgeIndex() const;

	// Returns the partitions associated with this polygon
	const vector< CG_Polygon2< Real > >& GetPartitions() const;
	vector< CG_Polygon2< Real > >& GetPartitions();

	// Returns the vertices of this polygon
	const TCircularList< PVertex >& GetVertices() const;
	TCircularList< PVertex >& GetVertices();

	// Returns the longest internal diagonal of this polygon
	CG_Segment2< Real > GetLongestInternalDiagonal() const;

	// Returns true if the given segment is a proper internal or external
	// diagonal of this polygon
	bool IsDiagonal( const CG_Segment2< Real >& segment,
                   Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

	// Returns true if the given segment is a proper internal diagonal 
	// of this polygon
	bool IsInternalDiagonal( const CG_Segment2< Real >& segment,
                           Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

	// Returns true if the given segment coincides with an edge of this polygon
	// The vertices' order DOES matter.
	bool HasAsEdge( const CG_Segment2< Real >& segment,
                  Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;
	
	// Returns true if the given segment coincides with a reversed edge of this polygon
	// The vertices' order DOES matter.
	bool HasAsReversedEdge( const CG_Segment2< Real >& segment,
                          Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

	// Returns true if this polygon is convex
	bool IsConvex();

	// Returns the straight skeleton of this polygon
	CG_StraightSkeleton2< Real > StraightSkeleton();
	// }

	// Containment
	// {
	// Returns true if this polygon contains the given point.
  // If boundaries is set to true the edges of this polygon are checked too.
	virtual bool Contains( Real x, Real y, bool boundaries );

  // Returns true if this polygon contains the given point.
  // If boundaries is set to true the edges of this polygon are checked too.
	virtual bool Contains( const CG_Point2< Real >& point, bool boundaries );

	// Returns true if this polygon contains the given polygon.
  // If boundaries is set to true the edges of this polygon are checked too.
	virtual bool Contains( const CG_Polygon2< Real >& other, bool boundaries );

	// Returns true if this polygon contains the given polyline.
  // If boundaries is set to true the edges of this polygon are checked too.
	virtual bool Contains( const CG_Polyline2< Real >& polyline, bool boundaries );
  // }

	// Intersection
	// {
	// Returns true if this polygon intersects the given segment.
	bool Intersects( const CG_Segment2< Real >& segment, bool boundaries );

 	// Returns true if this polygon intersects the given one.
	bool Intersects( const CG_Polygon2< Real >& other, bool boundaries );

	// Returns true if this polygon intersects the given segment.
  // See CG_Segment2::IntersectsProperly() for the meaning of "properly"
	bool IntersectsProperly( const CG_Segment2< Real >& segment );

 	// Returns true if this polygon intersects the given one.
  // See CG_Segment2::IntersectsProperly() for the meaning of "properly"
	bool IntersectsProperly( const CG_Polygon2< Real >& other );

 	// Returns true if this polygon intersects with the given polyline.
  // See CG_Segment2::IntersectsProperly() for the meaning of "properly"
	bool IntersectsProperly( const CG_Polyline2< Real >& polyline );
  // }

	// Operations with polygons
	// {
	// Merges this polygon and the given one and returns the resulting polygon
	// The two polygons MUST share an edge and must have the same vertices
	// orientation
	CG_Polygon2< Real > MergeWith( const CG_Polygon2< Real >& other ) const;

	// Returns a polygon obtained from this polygon offset by the given quantity
	// Negative value of the offset are inward if the vertices order is CCW
	CG_Polygon2< Real > Offset( Real offset, Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

  // Returns the list of polygons obtained by clipping this polygon with the given one.
  // The returned polygons are completely contained into the clipping polygon.
  // The list will be empty if this polygon is completely outside the clipping polygon.
  // The list will contain only one polygon (a copy of this) if this polygon is completely
  // inside the clipping polygon.
  // The list will contain one or more polygons if this polygon is partly contained into the
  // clipping polygon.
  vector< CG_Polygon2< Real > > Clip( const CG_Polygon2< Real >& clipPolygon ) const;
  // }

private:
	// Returns true if the given segment is strictly internal to this polygon
	// in the neighborhood of the given vertex
	// WARNING: point must coincide with segment.m_From
	bool InCone( const CG_Point2< Real >& point, const CG_Segment2< Real >& segment,
               Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

	// Triangulates this polygon using the triangle 1.6 algorithm.
	// Reference: http://www.cs.cmu.edu/~quake/triangle.html
	void Triangle16Triangulate();
};

//-----------------------------------------------------------------------------

#include "CG_Polygon2.inl"

//-----------------------------------------------------------------------------

#endif
