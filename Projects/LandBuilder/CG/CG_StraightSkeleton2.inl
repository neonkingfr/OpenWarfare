//-----------------------------------------------------------------------------
// File: CG_StraightSkeleton2.inl
//
// Desc: the straight skeleton of a two dimensional polygon
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
SSVertex< Real >::SSVertex()
: CG_Polygon2< Real >::PVertex()
, m_prevEdgeIndex( 0 )
, m_nextEdgeIndex( 0 )
, m_bisector( CG_Vector2< Real >() )
, m_va( 0 )
, m_vb( 0 )
, m_distance( CG_Consts< Real >::MAX_REAL )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
SSVertex< Real >::SSVertex( Real x, Real y )
: CG_Polygon2< Real >::PVertex( x, y )
, m_prevEdgeIndex( 0 )
, m_nextEdgeIndex( 0 )
, m_bisector( CG_Vector2< Real >() )
, m_va( 0 )
, m_vb( 0 )
, m_distance( CG_Consts< Real >::MAX_REAL )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
SSVertex< Real >::SSVertex( const SSVertex& other )
: CG_Polygon2< Real >::PVertex( other )
, m_prevEdgeIndex( other.m_prevEdgeIndex )
, m_nextEdgeIndex( other.m_nextEdgeIndex )
, m_bisector( other.m_bisector )
, m_va( other.m_va )
, m_vb( other.m_vb )
, m_distance( other.m_distance )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
SSVertex< Real >::~SSVertex()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
SSVertex< Real >& SSVertex< Real >::operator = ( const SSVertex< Real >& other )
{
  CG_Polygon2< Real >::PVertex::operator = ( other );
  m_prevEdgeIndex  = other.m_prevEdgeIndex;
  m_nextEdgeIndex  = other.m_nextEdgeIndex;
  m_bisector       = other.m_bisector;
  m_va             = other.m_va;
  m_vb             = other.m_vb;
  m_distance       = other.m_distance;
  return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t SSVertex< Real >::GetPrevEdgeIndex() const
{
  return (m_prevEdgeIndex);
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t SSVertex< Real >::GetNextEdgeIndex() const
{
  return (m_nextEdgeIndex);
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Vector2< Real >& SSVertex< Real >::GetBisector() const
{
  return (m_bisector);
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t SSVertex< Real >::GetVa() const
{
  return (m_va);
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t SSVertex< Real >::GetVb() const
{
  return (m_vb);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real SSVertex< Real >::GetDistance() const
{
  return (m_distance);
}

//-----------------------------------------------------------------------------

template < typename Real >
void SSVertex< Real >::SetPrevEdgeIndex( size_t index )
{
  m_prevEdgeIndex = index;
}

//-----------------------------------------------------------------------------

template < typename Real >
void SSVertex< Real >::SetNextEdgeIndex( size_t index )
{
  m_nextEdgeIndex = index;
}

//-----------------------------------------------------------------------------

template < typename Real >
void SSVertex< Real >::SetBisector( const CG_Vector2< Real >& bisector )
{
  m_bisector = bisector;
}

//-----------------------------------------------------------------------------

template < typename Real >
void SSVertex< Real >::SetVa( size_t va )
{
  m_va = va;
}

//-----------------------------------------------------------------------------

template < typename Real >
void SSVertex< Real >::SetVb( size_t vb )
{
  m_vb = vb;
}

//-----------------------------------------------------------------------------

template < typename Real >
void SSVertex< Real >::SetDistance( Real distance )
{
  m_distance = distance;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template < typename Real >
CG_StraightSkeleton2< Real >::Node::Node( Node* parent, const CG_Point2< Real >& data )
: m_parent( parent )
, m_data( data )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_StraightSkeleton2< Real >::Node::~Node()
{
  RemoveAllChildren();
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real >& CG_StraightSkeleton2< Real >::Node::GetData()
{
  return (m_data);
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point2< Real >& CG_StraightSkeleton2< Real >::Node::GetData() const
{
  return (m_data);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< typename CG_StraightSkeleton2< Real >::Node* >& CG_StraightSkeleton2< Real >::Node::GetChildren()
{
  return (m_children);
}

//-----------------------------------------------------------------------------

template < typename Real >
const vector< typename CG_StraightSkeleton2< Real >::Node* >& CG_StraightSkeleton2< Real >::Node::GetChildren() const
{
  return (m_children);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::Node::AddChild( const CG_Point2< Real >& point,
                                                   Real tolerance )
{
  bool found = false;

  // checks if the point is already the parent of this node
  if ( m_parent && m_parent->GetData().Equals( point, tolerance ) ) { found = true; }

  if ( !found )
  {
    // checks if the point is already a child of this node
    for ( size_t i = 0, cntI = m_children.size(); i < cntI; ++i )
    {
      if ( m_children[i]->GetData().Equals( point, tolerance ) )
      {
        found = true;
        break;
      }
    }
  }

  if ( !found )
  {
    Node* node = new Node( this, point );
    m_children.push_back( node );
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::Node::RemoveAllChildren()
{
  for ( size_t i = 0, cntI = m_children.size(); i < cntI; ++i )
  {
    if ( m_children[i] ) { delete m_children[i]; }
  }
  m_children.clear();
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::Node::RemoveOverlappingBones( Real tolerance )
{
  // this method has been written only to postprocess data coming from
  // cgal which is buggy (overlapping bones in certain edge cases), 
  // so it considers only bones starting from the leaves (original polygon vertices)

  if ( !IsLeaf() )
  // internal nodes
  {
    size_t i = 0;
    while ( i < m_children.size() )
    {
      // using a while statement because the children size could vary
      // deeper into the tree
      size_t oldSize = m_children.size();
      m_children[i]->RemoveOverlappingBones( tolerance );
      if ( oldSize == m_children.size() ) { ++i; }
    }
  }
  else if ( IsRoot() )
  // root
  {
    // the root should have only one child
    assert( m_children.size() == 1 );

    Node* child = m_children[0];
    CG_Segment2< Real > boneToChild( m_data, child->m_data );

    // checks child's children
    for ( size_t i = 0, cntI = child->m_children.size(); i < cntI; ++i )
    {
      if ( boneToChild.Contains( child->m_children[i]->m_data, false, tolerance ) )
      {
        child->m_children[i]->_ChangeParent( this );
        break;
      }
    }
  
    // transverse tree
    child->RemoveOverlappingBones( tolerance ); 
  }
  else
  // non root leaves
  {
    CG_Segment2< Real > boneToParent( m_data, m_parent->m_data );

    bool done = false;

    // checks parent's parent
    Node* grandFather = m_parent->m_parent;
    if ( grandFather )
    {
      if ( boneToParent.Contains( grandFather->m_data, false, tolerance ) )
      {
        _ChangeParent( grandFather );
        done = true;
      }
    }

    // checks siblings
    if ( !done )
    {
      for ( size_t i = 0, cntI = m_parent->m_children.size(); i < cntI; ++i )
      {
        if ( m_parent->m_children[i] != this )
        {
          if ( boneToParent.Contains( m_parent->m_children[i]->m_data, false, tolerance ) )
          {
            _ChangeParent( m_parent->m_children[i] );
            break;
          }
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
typename CG_StraightSkeleton2< Real >::Node* CG_StraightSkeleton2< Real >::Node::Find( const CG_Point2< Real >& point, 
                                                                                       Real tolerance )
{
  if ( m_data.Equals( point, tolerance ) ) { return (this); }
  for ( size_t i = 0, cntI = m_children.size(); i < cntI; ++i )
  {
    Node* res = m_children[i]->Find( point, tolerance );
    if ( res ) { return (res); }
  }
  return (NULL);
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_StraightSkeleton2< Real >::Node::DescendantsCount() const
{
  size_t size = m_children.size();
  size_t descendantsCount = size;
  for ( size_t i = 0; i < size; ++i )
  {
    descendantsCount += m_children[i]->DescendantsCount();
  }
  return (descendantsCount);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Segment2< Real > > CG_StraightSkeleton2< Real >::Node::AttachedBones() const
{
  vector< CG_Segment2< Real > > bones;

  size_t size = m_children.size();
  for ( size_t i = 0; i < size; ++i )
  {
    bones.push_back( CG_Segment2< Real >( m_data, m_children[i]->m_data ) );
  }

  vector< CG_Segment2< Real > > childBones;
  for ( size_t i = 0; i < size; ++i )
  {
    childBones = m_children[i]->AttachedBones();
    for ( size_t j = 0, cntJ = childBones.size(); j < cntJ; ++j )
    {
      bones.push_back( childBones[j] );
    }
  }

  return (bones);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Point2< Real > > CG_StraightSkeleton2< Real >::Node::Leaves() const
{
  vector< CG_Point2< Real > > leaves;

  vector< CG_Point2< Real > > childLeaves;
  for ( size_t i = 0, cntI = m_children.size(); i < cntI; ++i )
  {
    childLeaves = m_children[i]->Leaves();
    for ( size_t j = 0, cntJ = childLeaves.size(); j < cntJ; ++j )
    {
      leaves.push_back( childLeaves[j] );
    }
  }

  if ( IsLeaf() ) { leaves.push_back( m_data ); }

  return (leaves);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_StraightSkeleton2< Real >::Node::IsLeaf() const
{
  if ( IsRoot() ) { return (m_children.size() == 1); }
  return (m_children.size() == 0);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_StraightSkeleton2< Real >::Node::IsRoot() const
{
  return (m_parent == NULL);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Point2< Real > > CG_StraightSkeleton2< Real >::Node::PathTo( const Node* caller, 
                                                                        const CG_Point2< Real >& point,
                                                                        Real tolerance ) const
{
  vector< CG_Point2< Real > > path;

  if ( m_data.Equals( point, tolerance ) )
  {
    path.push_back( m_data );
  }
  else
  {
    for ( size_t i = 0, cntI = m_children.size(); i < cntI; ++i )
    {
      if ( m_children[i] != caller )
      {
        path = m_children[i]->PathTo( this, point, tolerance );
        if ( path.size() > 0 ) { break; } 
      }
    }

    if ( path.size() == 0 )
    {
      if ( m_parent && m_parent != caller )
      {
        path = m_parent->PathTo( this, point, tolerance );
      }
    }

    if ( path.size() > 0 ) { path.insert( path.begin(), m_data ); }
  }

  return (path);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::Node::_ChangeParent( Node* newParent )
{
  m_parent->_ExtractChild( this );
  m_parent = newParent;
  newParent->_AddChild( this );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::Node::_AddChild( Node* child )
{
 // no creation, we move the ownership to this node
  m_children.push_back( child );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::Node::_ExtractChild( Node* child )
{
  size_t i = 0;
  while ( i < m_children.size() )
  {
    if ( m_children[i] == child )
    {
      // no deletion, we move the ownership to another node
      m_children.erase( m_children.begin() + i );
      return;
    }
    ++i;
  }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template < typename Real >
CG_StraightSkeleton2< Real >::CG_StraightSkeleton2( CG_Polygon2< Real >* polygon, EAlgorithms algorithm )
: m_root( NULL )
, m_polygon( polygon )
, m_algorithm( algorithm )
{
  switch( m_algorithm )
  {
  case ENRICO:
  default:
    {
      _CalculateEnrico();
    }
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_StraightSkeleton2< Real >::CG_StraightSkeleton2( const CG_StraightSkeleton2< Real >& other )
: m_root( NULL )
{
  _CopyFrom( other );
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_StraightSkeleton2< Real >::~CG_StraightSkeleton2()
{
  _ClearNodes();
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_StraightSkeleton2< Real >::AddBone( const CG_Segment2< Real >& bone,
                                            Real tolerance )
{
  const CG_Point2< Real >& from = bone.GetFrom();
  const CG_Point2< Real >& to   = bone.GetTo();

  if ( bone.Length() < tolerance ) { return (true); }

  if ( !m_root )
  {
    m_root = new Node( NULL, from );
    m_root->AddChild( to );
  }
  else
  {
    Node* fromNode = m_root->Find( from, tolerance );
    Node* toNode   = m_root->Find( to, tolerance );
    if ( fromNode )
    {
      fromNode->AddChild( to );
    }
    else if ( toNode )
    {
      toNode->AddChild( from );
    }
    else
    {
      return (false);
    }
  }

  return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_StraightSkeleton2< Real >::BonesCount() const
{
  size_t bonesCount = 0;

  if ( m_root ) { bonesCount = m_root->DescendantsCount(); }

  return (bonesCount);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Segment2< Real > > CG_StraightSkeleton2< Real >::Bones() const
{
  vector< CG_Segment2< Real > > bones;

  if ( m_root ) { bones = m_root->AttachedBones(); }

  return (bones);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Point2< Real > > CG_StraightSkeleton2< Real >::Leaves() const
{
  vector< CG_Point2< Real > > leaves;

  if ( m_root ) { leaves = m_root->Leaves(); }

  return (leaves);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Point2< Real > > CG_StraightSkeleton2< Real >::PathConnecting( const CG_Point2< Real >& p1,
                                                                          const CG_Point2< Real >& p2,
                                                                          Real tolerance ) const
{
  vector< CG_Point2< Real > > path;

  Node* from = m_root->Find( p1, tolerance );
  Node* to   = m_root->Find( p2, tolerance );

  if ( from && to )
  {
    path = from->PathTo( NULL, p2, tolerance );
  }

  return (path);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::_CalculateEnrico()
{
  // unfortunately at the moment this algorithm works only for convex polygons and for some
  // non-convex polygon, we still don't have 100 percent success
  // so at the moment it is restricted to convex polygons only
  if ( m_polygon )
  {
    size_t size = m_polygon->VerticesCount();
    if ( size > 2 )
    {
      if ( m_polygon->GetType() != CG_Polygon2< Real >::PolyType_HOLED /*&& m_polygon->IsConvex()*/ )
      {
        bool reversed = false;

        // sets to CCW if not already
        if ( !m_polygon->IsVerticesOrderCCW() ) 
        { 
          m_polygon->SetVerticesOrderToCCW(); 
          reversed = true;
        }

        bool res = _CoreEnrico();

        // reverts to CW if reverted
        if ( reversed ) { m_polygon->SetVerticesOrderToCW(); }
      }
    }
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::_CopyFrom( const CG_StraightSkeleton2< Real >& other )
{
  m_polygon   = other.m_polygon;
  m_algorithm = other.m_algorithm;

  _ClearNodes();
  vector< CG_Segment2< Real > > bones = other.Bones();
  for ( size_t i = 0, cntI = bones.size(); i < cntI; ++i )
  {
    AddBone( bones[i] );
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::_ClearNodes()
{
  if ( m_root ) 
  { 
    delete m_root; 
    m_root = NULL;
  }
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CalcBisector( const CG_Polygon2< Real >& polygon, const SSVertex< Real >& vertex )
{
  CG_Segment2< Real > prevEdge = polygon.Edge( vertex.GetPrevEdgeIndex() );
  CG_Segment2< Real > nextEdge = polygon.Edge( vertex.GetNextEdgeIndex() );
  CG_Vector2< Real > prevEdgeNorm = prevEdge.UnitNormalVector();
  CG_Vector2< Real > nextEdgeNorm = nextEdge.UnitNormalVector();
  CG_Vector2< Real > bisector     = prevEdgeNorm + nextEdgeNorm;

  // we have to take in account that the two edges could be parallel
  if ( bisector.Length() < CG_Consts< Real >::ZERO_TOLERANCE ) { bisector = prevEdgeNorm.PerpCCW(); }

  return (bisector.Normalized());
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_StraightSkeleton2< Real >::_CoreEnrico()
{
  _ClearNodes();

  m_polygon->SetVerticesConvexity();

  // This implementation comes from a revision of the paper:
  // Straight Skeleton Implementation
  // by Petr Felkel and Stepan Obdralek
  vector< CG_Segment2< Real > > tempBones;

  // 1) Initialization: 
  // {
  // 1a + 1b
  // we put the polygon vertices into a circular list
  TCircularList< SSVertex< Real > > lav;
  for ( size_t i = 0, cntI = m_polygon->VerticesCount(); i < cntI; ++i )
  {
    const CG_Polygon2< Real >::PVertex& p = m_polygon->Vertex( i );
    SSVertex< Real > v( p.GetX(), p.GetY() );
    v.SetConvex( p.IsConvex() );
    lav.Append( v );
  }

  // we calculate the bisectors in every vertex
  for ( int i = 0, cntI = static_cast< int >( lav.Size() ); i < cntI; ++i )
  {
    if ( i == 0 )
    {
      lav[i].SetPrevEdgeIndex( cntI - 1 );
    }
    else
    {
      lav[i].SetPrevEdgeIndex( i - 1 );
    }

    lav[i].SetNextEdgeIndex( i );
    lav[i].SetBisector( CalcBisector( *m_polygon, lav[i] ) );
  }

  // 1c
  // we calculates the skeleton node candidates
  // and we store them into a priority queue
  TPriorityQueue< SSVertex< Real >, Real > nodes;
  // 1st candidate type: intersections of consecutive bisectors
  for ( int i = 0, cntI = static_cast< int >( lav.Size() ); i < cntI; ++i )
  {
    SSVertex< Real > candidate = _ClosestNodeCandidate( lav, i );
    Real minDist = candidate.GetDistance();

    if ( minDist != CG_Consts< Real >::MAX_REAL && minDist > CG_Consts< Real >::ZERO_TOLERANCE )
    {
      if ( m_polygon->Contains( candidate, true ) )
      {
        nodes.Push( candidate, CG_Consts< Real >::ONE / minDist );
      }
    }
  }

  // 2) Bones calculation: 
  // {
  while ( !nodes.Empty() )
  {
    SSVertex< Real > currN = nodes.Top();
    nodes.Pop();

    size_t lavSize = lav.Size();

    int vaIndex = static_cast< int >( currN.GetVa() );
    int vbIndex = static_cast< int >( currN.GetVb() );
    SSVertex< Real >& va = lav[vaIndex];
    const SSVertex< Real >& vb = lav[vbIndex];

    if ( lav[vaIndex - 2].Equals( vb ) )
    {
      int vcIndex;
      if ( vaIndex == 0 )
      {
        vcIndex = static_cast< int >( lav.Size() ) - 1;
      }
      else
      {
        vcIndex = vaIndex - 1;
      }
      SSVertex< Real >& vc = lav[vcIndex];

      tempBones.push_back( CG_Segment2< Real >( currN, va ) );
      tempBones.push_back( CG_Segment2< Real >( currN, vb ) );
      tempBones.push_back( CG_Segment2< Real >( currN, vc ) );

      // removes nodes pointing to va and vb and vc
      int i = 0;
      while ( i < static_cast< int >( nodes.Size() ) )
      {
        int nVa = static_cast< int >( nodes[i].GetVa() );
        int nVb = static_cast< int >( nodes[i].GetVb() );

        if ( nVa == vaIndex || nVa == vbIndex || nVa == vcIndex ||
             nVb == vaIndex || nVb == vbIndex || nVb == vcIndex )
        {
          nodes.Remove( i );
          --i;
        }
        ++i;
      }

      // at this point the generation of the bones should be completed
      nodes.Clear();
    }
    else
    {
      bool done = false;

      CG_Segment2< Real > segCurrVa( currN, va );
      const SSVertex< Real >& prevVa = lav[vaIndex - 1];

      if ( segCurrVa.Contains( prevVa, true ) )
      {
        tempBones.push_back( CG_Segment2< Real >( currN, prevVa ) );
        tempBones.push_back( CG_Segment2< Real >( currN, vb ) );
        tempBones.push_back( CG_Segment2< Real >( prevVa, va ) );

        if ( lavSize == 4 )
        {
          const SSVertex< Real >& nextVb = lav[vbIndex + 1];
          tempBones.push_back( CG_Segment2< Real >( currN, nextVb ) );

          // at this point the generation of the bones should be completed
          nodes.Clear();
          done = true;
        }

        lav.Remove( vaIndex ); // removes va from lav
        --vaIndex;
        lavSize = lav.Size();
        if ( vaIndex < 0 ) { vaIndex = lavSize - 1; }

        currN.SetPrevEdgeIndex( prevVa.GetPrevEdgeIndex() );
      }
      else
      {
        tempBones.push_back( segCurrVa );
        tempBones.push_back( CG_Segment2< Real >( currN, vb ) );

        currN.SetPrevEdgeIndex( va.GetPrevEdgeIndex() );
      }

      if ( !done )
      {
        currN.SetNextEdgeIndex( vb.GetNextEdgeIndex() );
        currN.SetBisector( CalcBisector( *m_polygon, currN ) );

        // removes nodes pointing to va and vb
        int i = 0;
        while ( i < static_cast< int >( nodes.Size() ) )
        {
          int nVa = static_cast< int >( nodes[i].GetVa() );
          int nVb = static_cast< int >( nodes[i].GetVb() );

          if ( nVa == vaIndex || nVa == vbIndex ||
               nVb == vaIndex || nVb == vbIndex )
          {
            nodes.Remove( i );
            --i;
          }
          ++i;
        }

        // updates the indices saved into the nodes
        if ( vbIndex > 0 )
        {
          for ( size_t i = 0, cntI = nodes.Size(); i < cntI; ++i )
          {
            int nVa = static_cast< int >( nodes[i].GetVa() );
            int nVb = static_cast< int >( nodes[i].GetVb() );
            if ( nVa > vbIndex )
            {
              nodes[i].SetVa( nVa - 1 );
            }
            if ( nVb > vbIndex )
            {
              nodes[i].SetVb( nVb - 1 );
            }
          }
        }

        lav.Insert( vaIndex - 1, currN );

        if ( vaIndex == 0 )
        {
          lav.Remove( 0 ); // removes va from lav
          lav.Remove( 1 ); // removes vb from lav
        }
        else if ( vaIndex == static_cast< int >( lavSize ) - 1 )
        {
          lav.Remove( static_cast< int >( lavSize ) ); // removes va from lav
          lav.Remove( 0 );                             // removes vb from lav
          vaIndex = 0;                                 // corrects vaIndex
        }
        else
        {
          lav.Remove( vaIndex + 1 ); // removes va from lav
          lav.Remove( vaIndex + 1 ); // removes vb from lav
        }

        bool searchNewCandidate = true;
        CG_Segment2< Real > segPrev( lav[vaIndex - 1], lav[vaIndex] );
        CG_Segment2< Real > segNext( lav[vaIndex], lav[vaIndex + 1] );
        for ( size_t b = 0, cntB = tempBones.size(); b < cntB; ++b )
        {
          if ( /*segPrev.Contains( tempBones[b], true ) || */segNext.Contains( tempBones[b], true ) )
          {
            searchNewCandidate = false;
            break;
          }
        }

        SSVertex< Real > candidate = _ClosestNodeCandidate( lav, vaIndex );
        Real minDist = candidate.GetDistance();

        if ( minDist != CG_Consts< Real >::MAX_REAL && minDist > CG_Consts< Real >::ZERO_TOLERANCE )
        {
          if ( m_polygon->Contains( candidate, true ) )
          {
            if ( !searchNewCandidate ) 
            { 
              nodes.Push( candidate, CG_Consts< Real >::ZERO );
            }
            else
            {
              nodes.Push( candidate, CG_Consts< Real >::ONE / minDist );
            }
          }
        }
        else
        {
          int a = 0;
        }
      }
    }
  }
  // }

  size_t escape = 0;
  size_t size = tempBones.size();
  size_t escapeLimit = size * size;
  while ( (tempBones.size() > 0) && (escape < escapeLimit) )
  {
    // if unable to add the first bone, duplicate it at the bottom of the list
    // for a later attempt
    if ( !AddBone( tempBones[0] ) ) { tempBones.push_back( tempBones[0] ); }
    tempBones.erase( tempBones.begin() );
    ++escape;
  }

  // failed to create the skeleton
  if ( tempBones.size() > 0 ) 
  { 
    _ClearNodes();
    return (false);
  }

  _RemoveOverlappingBones();

  return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_StraightSkeleton2< Real >::_RemoveOverlappingBones( Real tolerance )
{
  if ( m_root ) { m_root->RemoveOverlappingBones( tolerance ); }
}

//-----------------------------------------------------------------------------

template < typename Real >
SSVertex< Real > CG_StraightSkeleton2< Real >::_ClosestNodeCandidate( const TCircularList< SSVertex< Real > >& lav,
                                                                      int vertexIndex )
{
  size_t size = lav.Size();

  const SSVertex< Real >& prevV = lav[vertexIndex - 1];
  const SSVertex< Real >& currV = lav[vertexIndex];
  const SSVertex< Real >& nextV = lav[vertexIndex + 1];

  const CG_Vector2< Real >& prevBisector = prevV.GetBisector();
  const CG_Vector2< Real >& currBisector = currV.GetBisector();
  const CG_Vector2< Real >& nextBisector = nextV.GetBisector();

  CG_Line2< Real > prevBisLine( prevV, prevBisector.Head( prevV ) );
  CG_Line2< Real > currBisLine( currV, currBisector.Head( currV ) );
  CG_Line2< Real > nextBisLine( nextV, nextBisector.Head( nextV ) );

  CG_Segment2< Real > prevEdge;
  CG_Segment2< Real > nextEdge;
  bool hasPrevEdgeLine = false;
  bool hasNextEdgeLine = false;

  if ( !prevV.Equals( currV ) ) 
  { 
    prevEdge = m_polygon->Edge( currV.GetPrevEdgeIndex() );
    hasPrevEdgeLine = true;
  }
  if ( !nextV.Equals( currV ) ) 
  { 
    nextEdge = m_polygon->Edge( currV.GetNextEdgeIndex() );
    hasNextEdgeLine = true;
  }

  TPriorityQueue< SSVertex< Real >, Real > candidates;

  CG_Point2< Real > intersection;

  if ( hasPrevEdgeLine && Intersect( prevBisLine, currBisLine, intersection ) )
  {
    SSVertex< Real > newCandidate( intersection.GetX(), intersection.GetY() );
    newCandidate.SetVa( (vertexIndex == 0) ? (size - 1) : (vertexIndex - 1) );
    newCandidate.SetVb( vertexIndex );
    newCandidate.SetDistance( prevEdge.DistanceFromContainingLine( intersection ) );
    Real dist = currV.Distance( intersection );

    if ( dist > CG_Consts< Real >::ZERO_TOLERANCE ) { candidates.Push( newCandidate, CG_Consts< Real >::ONE / dist ); }
  }

  if ( hasNextEdgeLine && Intersect( nextBisLine, currBisLine, intersection ) )
  {
    SSVertex< Real > newCandidate( intersection.GetX(), intersection.GetY() );
    newCandidate.SetVa( vertexIndex );
    newCandidate.SetVb( (vertexIndex == (size - 1)) ? 0 : (vertexIndex + 1) );
    newCandidate.SetDistance( nextEdge.DistanceFromContainingLine( intersection ) );
    Real dist = currV.Distance( intersection );

    if ( dist > CG_Consts< Real >::ZERO_TOLERANCE ) { candidates.Push( newCandidate, CG_Consts< Real >::ONE / dist ); }
  }

  SSVertex< Real > candidate;
  if ( candidates.Size() > 0 ) { candidate = candidates.Top(); }

  return (candidate);
}

//-----------------------------------------------------------------------------

