//-----------------------------------------------------------------------------
// File: CG_Vector2.inl
//
// Desc: A two dimensional vector
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real >::CG_Vector2()
: m_x( static_cast< Real >( 0 ) )
, m_y( static_cast< Real >( 0 ) )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real >::CG_Vector2( Real x, Real y )
: m_x( x )
, m_y( y )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real >::CG_Vector2( const CG_Point2< Real >& head, const CG_Point2< Real >& tail )
{
	m_x = head.GetX() - tail.GetX();
	m_y = head.GetY() - tail.GetY();
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real >::CG_Vector2( Real direction )
{
	m_x = cos( direction );
	m_y = sin( direction );
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real >::CG_Vector2( const CG_Vector2< Real >& other )
: m_x( other.m_x )
, m_y( other.m_y )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real >::~CG_Vector2()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real >& CG_Vector2< Real >::operator = ( const CG_Vector2< Real >& other )
{
	m_x = other.m_x;
	m_y = other.m_y;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real >& CG_Vector2< Real >::operator += ( const CG_Vector2< Real >& other )
{
	m_x += other.m_x;
	m_y += other.m_y;

	return (*this);
}

//-----------------------------------------------------------------------------
template < typename Real >
CG_Vector2< Real >& CG_Vector2< Real >::operator -= ( const CG_Vector2< Real >& other )
{
	m_x -= other.m_x;
	m_y -= other.m_y;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real >& CG_Vector2< Real >::operator *= ( const Real scalar )
{
	m_x *= scalar;
	m_y *= scalar;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CG_Vector2< Real >::operator + ( const CG_Vector2< Real >& other ) const
{
	CG_Vector2< Real > ret( *this );
	ret += other;
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CG_Vector2< Real >::operator - ( const CG_Vector2< Real >& other ) const
{
	CG_Vector2< Real > ret( *this );
	ret -= other;
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CG_Vector2< Real >::operator * ( const Real scalar ) const
{
	CG_Vector2< Real > ret( *this );
	ret *= scalar;
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector2< Real >::GetX() const
{
	return (m_x);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector2< Real >::GetY() const
{
	return (m_y);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vector2< Real >::SetX( Real x )
{
	m_x = x;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vector2< Real >::SetY( Real y )
{
	m_y = y;
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector2< Real >::SquaredLength() const
{
	return (m_x * m_x + m_y * m_y);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector2< Real >::Length() const
{
	return (sqrt( SquaredLength() ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector2< Real >::Direction() const
{
	Real ret = atan2( m_y, m_x );
	if ( ret < static_cast< Real >( 0 ) )
	{
		ret += CG_Consts< Real >::TWO_PI;
	}
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vector2< Real >::Rotate( Real angle )
{
	Real c = cos( angle );
	Real s = sin( angle );
	Real x = m_x * c - m_y * s;
	Real y = m_x * s + m_y * c;
	m_x = x;
	m_y = y;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vector2< Real >::Normalize()
{
	Real len = Length();
	if ( len != static_cast< Real >( 0 ) )
	{
		Real inv = static_cast< Real >( 1 ) / len;
		m_x *= inv;
		m_y *= inv;
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CG_Vector2< Real >::operator - () const
{
	return (CG_Vector2< Real >( -m_x, -m_y ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector2< Real >::Dot( const CG_Vector2< Real >& other ) const
{
	return (m_x * other.m_x + m_y * other.m_y);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector2< Real >::Cross( const CG_Vector2< Real >& other ) const
{
	return (m_x * other.m_y - m_y * other.m_x);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CG_Vector2< Real >::Rotated( Real angle ) const
{
	CG_Vector2< Real > retVec( *this );
	retVec.Rotate( angle );
	return (retVec);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CG_Vector2< Real >::Normalized() const
{
	CG_Vector2< Real > retVec( *this );
	retVec.Normalize();
	return (retVec);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CG_Vector2< Real >::PerpCW() const
{
	return (CG_Vector2< Real >( m_y, -m_x ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > CG_Vector2< Real >::PerpCCW() const
{
	return (CG_Vector2< Real >( -m_y, m_x ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Vector2< Real >::Head( const CG_Point2< Real >& tail ) const
{
	CG_Point2< Real > ret( tail );
	ret.Translate( m_x, m_y );
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Vector2< Real >::Tail( const CG_Point2< Real >& head ) const
{
	CG_Point2< Real > ret( head );
	ret.Translate( -m_x, -m_y );
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector2< Real > operator * ( const Real scalar, const CG_Vector2< Real >& v )
{
	CG_Vector2< Real > ret( v );
	ret *= scalar;
	return (ret);
}

//-----------------------------------------------------------------------------
