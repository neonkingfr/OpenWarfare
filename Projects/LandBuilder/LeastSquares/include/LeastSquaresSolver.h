// *********************************************************************** //
// LeastSquaresSolver                                                      //
// *********************************************************************** //
// Class for solving linear systems using least squares method.            //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
// Usage:                                                                  //
// ------                                                                  //
//                                                                         //
// Given a matrix (rA, cA):                                                //
// ** MatrixMxN matA;                                                      //
// filled with the coefficients of the linear system;                      //
// a vector (rA):                                                          //
// ** VectorN vecL;                                                        //
// filled with the known terms of the linear system;                       //
// and, optionally, a square matrix (rA, rA):                              //
// ** MatrixMxN matW;                                                      //
// filled with the weights of the equations of the linear system;          //
//                                                                         //
//                                                                         //
// 1) declare a LeastSquareSolver object:                                  //
// ** LeastSquaresSolver lss;                                              //
// 2) set the matrices                                                     //
// ** lss.SetCoefficientsMatrix(matA);   // mandatory                      //
// ** lss.SetWeightsMatrix(matW);        // optional                       //
// ** lss.SetKnownsVector(vecL);         // mandatory                      //
// [matW is optional, if not specified will default to the unitary matrix] //
// 3) call Solve() method                                                  //
// ** if(lss.Solve())                                                      //
// ** {                                                                    //
// **   ...                                                                //
// 4) retriew desired results                                              //
// **   MatrixMxN<Real>& matC   = GetCovariancesMatrix();                  //
// **   VectorN<Real>&   vecX   = GetSolutionsVector();                    //
// **   VectorN<Real>&   vecV   = GetResidualsVector();                    //
// **   Real             sigma2 = GetResidualsSquaredSigma();              //
// **   VectorN<Real>&   vecS   = GetSigmasVector();                       //
// **   ...                                                                //
// ** }                                                                    //
// ** else                                                                 //
// ** {                                                                    //
// **    // error while solving                                            //
// ** }                                                                    //
//                                                                         //
// *********************************************************************** //

#ifndef LEASTSQUARESSOLVER_H
#define LEASTSQUARESSOLVER_H

#include ".\MatrixMxN.h"

template <class Real>
class LeastSquaresSolver
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// ---------------------------------- //
	// the matrix of coefficients (input) //
	// ---------------------------------- //
    MatrixMxN<Real> m_MatA;

	// ----------------------------- //
	// the matrix of weights (input) //
	// ----------------------------- //
    MatrixMxN<Real> m_MatW;

	// ---------------------------- //
	// the vector of knowns (input) //
	// ---------------------------- //
    VectorN<Real> m_VecL;

	// ---------------------------------- //
	// the matrix of covariances (output) //
	// ---------------------------------- //
    MatrixMxN<Real> m_MatH;

	// -------------------------------- //
	// the vector of solutions (output) //
	// -------------------------------- //
    VectorN<Real> m_VecX;

	// -------------------------------- //
	// the vector of residuals (output) //
	// -------------------------------- //
    VectorN<Real> m_VecV;

	// --------------------------------------- //
	// the squared sigma of residuals (output) //
	// --------------------------------------- //
    Real m_SqSigma;

	// ------------------------------------------ //
	// the vector of sigmas of residuals (output) //
	// ------------------------------------------ //
    VectorN<Real> m_VecSigmas;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	LeastSquaresSolver();

	// ********* //
	// Interface //
	// ********* //

	// return equal to true if the matrix is valid
	// rows' number must be greater then or equal to columns' number 
	// calling this function changes also the matrix of weights, 
	// which is cleared and resized
	bool SetCoefficientsMatrix(const MatrixMxN<Real>& matA);

	// return equal to true if the matrix is valid
	// must be square and with size equal to the number of rows of
	// the matrix of coefficients
	bool SetWeightsMatrix(const MatrixMxN<Real>& matW);

	// return equal to true if the vector is valid
	// must have size equal to the number of rows of
	// the matrix of coefficients
	bool SetKnownsVector(const VectorN<Real>& vecL);

	MatrixMxN<Real> GetCoefficientsMatrix() const;
	MatrixMxN<Real> GetWeigthsMatrix() const;
	VectorN<Real>   GetKnownsVector() const;
	MatrixMxN<Real> GetCovariancesMatrix() const;
	VectorN<Real>   GetSolutionsVector() const;
	VectorN<Real>   GetResidualsVector() const;
	Real            GetResidualsSquaredSigma() const;
	VectorN<Real>   GetSigmasVector() const;

	// returns true if the solving process is successfull
	bool Solve();
};

#include "..\src\LeastSquaresSolver.inl"

#endif