#ifndef MATRIXMXN_H
#define MATRIXMXN_H

#include ".\VectorN.h"
#include "..\..\HighMapLoaders\include\EtMath.h"

template <class Real>
class MatrixMxN
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// --------------------- //
	// the matrix dimensions //
	// --------------------- //
	unsigned int m_RowsCount;
	unsigned int m_ColumnsCount;

	// ----------------------------------- //
	// the total number of matrix elements //
	// ----------------------------------- //
	unsigned int m_ElementsCount;

	// ------------------- //
	// the matrix elements //
	// -------------------------------------------------------------- //
	// elements are stored in a 1 dimensional array in row major form //
	// -------------------------------------------------------------- //
	Real* m_Elements;

	// ----------------------------- //
	// array of pointers to the rows //
	// ----------------------------- //
	Real** m_Rows;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //

	MatrixMxN(unsigned int rowsCount = 0, unsigned int columnsCount = 0);
    MatrixMxN(unsigned int rowsCount, unsigned int columnsCount, const Real* elements);
    MatrixMxN(unsigned int rowsCount, unsigned int columnsCount, const Real** rows);

	MatrixMxN(const MatrixMxN<Real>& other);

	// ********** //
	// Destructor //
	// ********** //
    ~MatrixMxN();

	// ********* //
	// Interface //
	// ********* //
	
    // assignement
	MatrixMxN<Real>& operator = (const MatrixMxN<Real>& other);

    // comparison
    bool operator == (const MatrixMxN<Real>& other) const;
    bool operator != (const MatrixMxN<Real>& other) const;

    // arithmetic operations
    MatrixMxN<Real> operator + (const MatrixMxN<Real>& other) const;
    MatrixMxN<Real> operator - (const MatrixMxN<Real>& other) const;
    MatrixMxN<Real> operator * (const MatrixMxN<Real>& other) const;
    VectorN<Real>   operator * (const VectorN<Real>& v) const;
    MatrixMxN<Real> operator * (Real scalar) const;
    MatrixMxN<Real> operator / (Real scalar) const;

	MatrixMxN<Real> operator - () const;

    // arithmetic updates
    MatrixMxN<Real>& operator += (const MatrixMxN<Real>& other);
    MatrixMxN<Real>& operator -= (const MatrixMxN<Real>& other);
    MatrixMxN<Real>& operator *= (Real scalar);
    MatrixMxN<Real>& operator /= (Real scalar);

	unsigned int GetRowsCount() const;
	unsigned int GetColumnsCount() const;
	unsigned int GetElementsCount() const;

    VectorN<Real> GetRow(unsigned int row) const;
    VectorN<Real> GetColumn(unsigned int column) const;

    operator const Real* () const;
    operator Real* ();
    const Real* operator [] (unsigned int row) const;
    Real* operator [] (unsigned int row);
    Real operator () (unsigned int row, unsigned int column) const;
//    Real& operator() (unsigned int row, unsigned int column);

	void SetSize(unsigned int rowsCount, unsigned int columnsCount);
    void SetMatrix(unsigned int rowsCount, unsigned int columnsCount, const Real* elements);
    void SetMatrix(unsigned int rowsCount, unsigned int columnsCount, const Real** rows);

	Real GetAt(unsigned int row, unsigned int column) const;
	void SetAt(unsigned int row, unsigned int column, Real value);

    void SetRow(unsigned int row, const VectorN<Real>& v);
    void SetColumn(unsigned int column, const VectorN<Real>& v);

    void SwapRows(unsigned int row1, unsigned int row2);

	bool IsSquare() const;
    MatrixMxN<Real> Transpose() const;

	// ONLY FOR SQUARED MATRICES
		bool Inverse(MatrixMxN<Real>& out) const;
		bool SetUnitary();
	//

	// ************************************************************** //
	// Helper functions                                               //
	// ************************************************************** //
protected:	
    // Support for allocation and deallocation.  The allocation call requires
    // m_Rows, m_Columns, and m_ElementsCount to have already been correctly
    // initialized.
    void Allocate(bool setToZero);
    void Deallocate();

    // support for comparisons
    int CompareElements(const MatrixMxN<Real>& other) const;
};

template <class Real>
MatrixMxN<Real> operator * (Real scalar, const MatrixMxN<Real>& m);

template <class Real>
VectorN<Real> operator * (const VectorN<Real>& v, const MatrixMxN<Real>& m);

#include "..\src\MatrixMxN.inl"

#endif