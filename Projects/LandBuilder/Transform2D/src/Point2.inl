#include "..\..\HighMapLoaders\include\EtMath.h"

// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
Point2<Real>::Point2(Real x, Real y)
: m_X(x), m_Y(y)
{
}

// ************************ //
// Member variables getters //
// ************************ //

template <class Real>
Real Point2<Real>::X() const
{
	return m_X;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real Point2<Real>::Y() const
{
	return m_Y;
}

// ************************************************************************** //
// Member variables setters                                                   //
// ************************************************************************** //

template <class Real>
void Point2<Real>::X(Real x)
{
	m_X = x;
}

// -------------------------------------------------------------------------- //

template <class Real>
void Point2<Real>::Y(Real y)
{
	m_Y = y;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
void Point2<Real>::TranslateX(Real valueX)
{
	m_X += valueX;
}

// -------------------------------------------------------------------------- //

template <class Real>
void Point2<Real>::TranslateY(Real valueY)
{
	m_Y += valueY;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real Point2<Real>::SquaredDistance(const Point2<Real>& other) const
{
	Real dx = other.m_X - m_X;
	Real dy = other.m_Y - m_Y;
	return (dx * dx + dy * dy);
}

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //

template <class Real>
Real DistancePoint2Line2(const Point2<Real>& p1, const Point2<Real>& p2, const Point2<Real>& pE)
{
	Real a = p2.Y() - p1.Y();
	Real b = p1.X() - p2.X();
	Real c = p1.Y() * (p2.X() - p1.X()) - p1.X() * (p2.Y() - p1.Y());

	Real d = a * a + b * b;

	if(d == static_cast<Real>(0.0)) return static_cast<Real>(0.0);

	return EtMath<Real>::Fabs(a * pE.X() + b* pE.Y() + c) / EtMath<Real>::Sqrt(d);
}
