// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
Transform2Manager<Real>::Transform2Manager()
{
	m_Transformation = NULL;
}

// ************************************************************************** //
// Destructor                                                                 //
// ************************************************************************** //

template <class Real>
Transform2Manager<Real>::~Transform2Manager()
{
	Clear();
}

// ************************************************************************** //
// Member variables setters                                                   //
// ************************************************************************** //

template <class Real>
void Transform2Manager<Real>::SetHomoPointsList(const HomologousPoint2List<Real>& homoPointList)
{
	m_HomoPointsList = homoPointList;
}

// -------------------------------------------------------------------------- //

template <class Real>
void Transform2Manager<Real>::SetCtrlPointsList(const HomologousPoint2List<Real>& ctrlPointList)
{
	m_CtrlPointsList = ctrlPointList;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
string Transform2Manager<Real>::GetTransformationName() const
{
	if(m_Transformation)
	{
		return m_Transformation->GetName();
	}
	else
	{
		return "";
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
const vector<Real>& Transform2Manager<Real>::GetParameters() const
{
	if(m_Transformation) return m_Transformation->GetParameters();
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Manager<Real>::ChooseTransformation(Transformations2 transformation)
{
	Clear();

	switch(transformation)
	{
	case T2_RIGID:
		{
			m_Transformation = new Transform2Rigid<Real>();
		}
		break;
	case T2_CONFORM:
		{
			m_Transformation = new Transform2Conform<Real>();
		}
		break;
	case T2_AFFINE:
		{
			m_Transformation = new Transform2Affine<Real>();
		}
		break;
	case T2_AFFINEEX:
		{
			m_Transformation = new Transform2AffineEx<Real>();
		}
		break;
	case T2_HOMOGRAPHIC:
		{
			m_Transformation = new Transform2Homographic<Real>();
		}
		break;
	case T2_BILINEAR:
		{
			m_Transformation = new Transform2Bilinear<Real>();
		}
		break;
	case T2_POLYNOMIAL2:
		{
			m_Transformation = new Transform2Polynomial2<Real>();
		}
		break;
	case T2_BESTFIT:
		{
			BestFit();
		}
		break;
	case T2_WIDEST:
		{
			Widest();
		}
		break;
	default:
		{
			m_Transformation = NULL;
		}
		break;
	};

	if(m_Transformation)
	{
		m_Transformation->SetHomoPointsList(m_HomoPointsList);
		if(m_Transformation->CanProceed() && m_Transformation->DoParametersCalculation())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
vector<Point2<Real> > Transform2Manager<Real>::ApplyTransformation(vector<Point2<Real> > pointsIn)
{
	vector<Point2<Real> > pointsOut;

	size_t pointsInCount = pointsIn.size();
	for(unsigned int i = 0; i < pointsInCount; ++i)
	{
		Point2<Real> p = m_Transformation->TransformPoint(pointsIn[i]);
		pointsOut.push_back(p);
	}
	return pointsOut;
}

// ************************************************************************** //
// Helper functions                                                           //
// ************************************************************************** //

template <class Real>
void Transform2Manager<Real>::Clear()
{
	if(m_Transformation) 
	{
		delete m_Transformation;
		m_Transformation = NULL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void Transform2Manager<Real>::BestFit()
{
	Real best = EtMath<Real>::MAX_REAL;
	Transformations2 transf = T2_NONE;

	m_Transformation = new Transform2Polynomial2<Real>();
	Real tempDist = VerifyPoints();
	if(tempDist < best)
	{
		best = tempDist;
		transf = T2_POLYNOMIAL2;
	}
	delete m_Transformation;

	m_Transformation = new Transform2Bilinear<Real>();
	tempDist = VerifyPoints();
	if(tempDist < best)
	{
		best = tempDist;
		transf = T2_BILINEAR;
	}
	delete m_Transformation;

	m_Transformation = new Transform2Homographic<Real>();
	tempDist = VerifyPoints();
	if(tempDist < best)
	{
		best = tempDist;
		transf = T2_HOMOGRAPHIC;
	}
	delete m_Transformation;

	m_Transformation = new Transform2AffineEx<Real>();
	tempDist = VerifyPoints();
	if(tempDist < best)
	{
		best = tempDist;
		transf = T2_AFFINEEX;
	}
	delete m_Transformation;

	m_Transformation = new Transform2Affine<Real>();
	tempDist = VerifyPoints();
	if(tempDist < best)
	{
		best = tempDist;
		transf = T2_AFFINE;
	}
	delete m_Transformation;

	m_Transformation = new Transform2Conform<Real>();
	tempDist = VerifyPoints();
	if(tempDist < best)
	{
		best = tempDist;
		transf = T2_CONFORM;
	}
	delete m_Transformation;

	m_Transformation = new Transform2Rigid<Real>();
	tempDist = VerifyPoints();
	if(tempDist < best)
	{
		best = tempDist;
		transf = T2_RIGID;
	}
	delete m_Transformation;

	m_Transformation = NULL;

	switch(transf)
	{
	case T2_RIGID:
		{
			m_Transformation = new Transform2Rigid<Real>();
		}
		break;
	case T2_CONFORM:
		{
			m_Transformation = new Transform2Conform<Real>();
		}
		break;
	case T2_AFFINE:
		{
			m_Transformation = new Transform2Affine<Real>();
		}
		break;
	case T2_AFFINEEX:
		{
			m_Transformation = new Transform2AffineEx<Real>();
		}
		break;
	case T2_HOMOGRAPHIC:
		{
			m_Transformation = new Transform2Homographic<Real>();
		}
		break;
	case T2_BILINEAR:
		{
			m_Transformation = new Transform2Bilinear<Real>();
		}
		break;
	case T2_POLYNOMIAL2:
		{
			m_Transformation = new Transform2Polynomial2<Real>();
		}
		break;
	default:
		{
			m_Transformation = NULL;
		}
		break;
	};
}

// -------------------------------------------------------------------------- //

template <class Real>
void Transform2Manager<Real>::Widest()
{
	m_Transformation = new Transform2Polynomial2<Real>();
	if(m_Transformation->CanProceed()) return;

	m_Transformation = new Transform2Bilinear<Real>();
	if(m_Transformation->CanProceed()) return;

	m_Transformation = new Transform2Homographic<Real>();
	if(m_Transformation->CanProceed()) return;

	delete m_Transformation;
	m_Transformation = new Transform2AffineEx<Real>();
	if(m_Transformation->CanProceed()) return;

	delete m_Transformation;
	m_Transformation = new Transform2Affine<Real>();
	if(m_Transformation->CanProceed()) return;

	delete m_Transformation;
	m_Transformation = new Transform2Conform<Real>();
	if(m_Transformation->CanProceed()) return;

	delete m_Transformation;
	m_Transformation = new Transform2Rigid<Real>();
	if(m_Transformation->CanProceed()) return;

	delete m_Transformation;
	m_Transformation = NULL;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real Transform2Manager<Real>::VerifyPoints()
{
	m_Transformation->SetHomoPointsList(m_HomoPointsList);
	if(m_Transformation->CanProceed() && m_Transformation->DoParametersCalculation())
	{
		Real tempDist = static_cast<Real>(0.0);
		unsigned int hPointsCount = m_HomoPointsList.Count();
		for(unsigned int i = 0; i < hPointsCount; ++i)
		{
			Point2<Real> pIn      = m_HomoPointsList.GetPoint(i)->GetXYSystem1();
			Point2<Real> pControl = m_HomoPointsList.GetPoint(i)->GetXYSystem2();
			Point2<Real> pOut     = m_Transformation->TransformPoint(pIn);
			
			tempDist += pOut.SquaredDistance(pControl);
		}
		unsigned int cPointsCount = m_CtrlPointsList.Count();
		for(unsigned int i = 0; i < cPointsCount; ++i)
		{
			Point2<Real> pIn      = m_CtrlPointsList.GetPoint(i)->GetXYSystem1();
			Point2<Real> pControl = m_CtrlPointsList.GetPoint(i)->GetXYSystem2();
			Point2<Real> pOut     = m_Transformation->TransformPoint(pIn);
			
			tempDist += pOut.SquaredDistance(pControl);
		}
		return tempDist;
	}
	return EtMath<Real>::MAX_REAL;
}
