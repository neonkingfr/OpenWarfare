// *********************************************************************** //
// Transform2Rigid                                                         //
// *********************************************************************** //
// Class who defines a two dimensional rigid transformation                //
// (translation + counterclockwise rotation).                              //
// ----------------------------------------------------------------------- //
// x' = x0 + x * cos(a) - y * sin(a)                                       //
// y' = y0 + x * sin(a) + y * cos(a)                                       //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
// For parameters calculations are needed at least two distinct points     //
// ----------------------------------------------------------------------- //
// Parameters count = 3                                                    //
// Parameters[0] = x0 = translation in X                                   //
// Parameters[1] = y0 = translation in Y                                   //
// Parameters[2] = a  = counterclockwise rotation (in radians)             //
// *********************************************************************** //

#ifndef TRANSFORM2RIGID_H
#define TRANSFORM2RIGID_H

#include ".\Transform2.h"

template <class Real>
class Transform2Rigid : public Transform2<Real>
{
	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Transform2Rigid();

	// ********* //
	// Interface //
	// ********* //
	virtual Point2<Real> TransformPoint(Point2<Real> p);

	// **************** //
	// Helper functions //
	// **************** //
protected:
	virtual bool SetCoefficientsMatrix();
	virtual bool SetKnownsVector();
	virtual bool SetWeightsMatrix();
	virtual bool ConvertParameters();
};

#include "..\src\Transform2Rigid.inl"

#endif
