// *********************************************************************** //
// HomologousPoint2List                                                    //
// *********************************************************************** //
// Class definying a list of homologous points with two dimensions.        //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
//                                                                         //
//                                                                         //
//                                                                         //
//                                                                         //
//                                                                         //
//                                                                         //
// *********************************************************************** //

#ifndef HOMOLOGOUSPOINT2LIST_H
#define HOMOLOGOUSPOINT2LIST_H

#include <vector>

#include ".\HomologousPoint2.h"

using std::vector;

template <class Real>
class HomologousPoint2List
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// ----------------------------- //
	// The list of homologous points //
	// ----------------------------- //
    vector<HomologousPoint2<Real> > m_Points;

	// ----------------------------------- //
	// The list of active and valid points //
	// ----------------------------------- //
    vector<HomologousPoint2<Real> > m_ActiveAndValidPoints;

	// ---------------------------- //
	// The counter of active points //
	// ---------------------------- //
	unsigned int* m_ActiveCount;

	// ---------------------------- //
	// The counter of valid points //
	// ---------------------------- //
	unsigned int* m_ValidCount;

	// -------------------------------------- //
	// The counter of active and valid points //
	// -------------------------------------- //
	unsigned int* m_ActiveAndValidCount;

	// -------------------------------------- //
	// Variable used to know wheter the check //
	// on the points alignement has been done //
	// -------------------------------------- //
	bool m_AlignementChecked;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	HomologousPoint2List();

	// ********** //
	// Destructor //
	// ********** //
	~HomologousPoint2List();

	// *********** //
	// Assignement //
	// *********** //
	HomologousPoint2List<Real>& operator = (const HomologousPoint2List<Real>& other);

	// ************************ //
	// Member variables getters //
	// ************************ //

	// GetPoints()
	// Returns the points of this list.
	vector<HomologousPoint2<Real> > GetPoints() const;

	// GetActiveAndValidPoints()
	// Returns the active and valid points of this list.
	vector<HomologousPoint2<Real> > GetActiveAndValidPoints();

	// ********* //
	// Interface //
	// ********* //

	// AddPoint()
	// Appends the given point to this list.
	void AddPoint(HomologousPoint2<Real> point);

	// RemovePoint()
	// Removes from this list the point at the given index.
	// Returns true if successfull.
	bool RemovePoint(unsigned int index);

	// GetPoint()
	// Returns a pointer to the point of this list with the specified index.
	// NULL if out of bounds.
	HomologousPoint2<Real>* GetPoint(unsigned int index);

	// Clear()
	// Clears this list.
	void Clear();

	// Count()
	// Returns the number of points in this list.
	unsigned int Count() const;

	// ActiveCount()
	// Returns the number of active points in this list.
	unsigned int ActiveCount();

	// ValidCount()
	// Returns the number of valid points in this list.
	unsigned int ValidCount();

	// ActiveAndValidCount()
	// Returns the number of active and valid points in this list.
	unsigned int ActiveAndValidCount();

	// **************** //
	// Helper functions //
	// **************** //
protected:
	// CalculateActiveAndValidPoints()
	// Calculates the active and valid points.
	void CalculateActiveAndValidPoints();

	// CalculateActiveCount()
	// Calculates the number of active points of this list.
	void CalculateActiveCount();

	// CalculateValidCount()
	// Calculates the number of valid points of this list.
	void CalculateValidCount();

	// CalculateActiveAndValidCount()
	// Calculates the number of active and valid points of this list.
	void CalculateActiveAndValidCount();

	// ClearActiveCount()
	// Clears the active points count variable of this list.
	void ClearActiveCount();

	// ClearValidCount()
	// Clears the valid points count variable of this list.
	void ClearValidCount();

	// ClearActiveAndValidCount()
	// Clears the active and valid points count variable of this list.
	void ClearActiveAndValidCount();

	// CheckAlignement()
	// Checks whether the points are aligned. In this case only the
	// two points more far are setted as valid, while all the others
	// are invalidated.
	void CheckAlignement();
};

#include "..\src\HomologousPoint2List.inl"

#endif