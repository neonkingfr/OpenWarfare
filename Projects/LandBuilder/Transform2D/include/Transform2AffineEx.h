// *********************************************************************** //
// Transform2AffineEx                                                      //
// *********************************************************************** //
// Class who defines a two dimensional extended affine transformation      //
// (translation + counterclockwise rotation + non-homogeneous scaling +    //
//  + counterclockwise shearing).                                          //
// ----------------------------------------------------------------------- //
// x' = x0 + sx * x * cos(a) - sy * y * sin(a + b)                         //
// y' = y0 + sx * x * sin(a) + sy * y * cos(a + b)                         //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
// For parameters calculations are needed at least three distinct and not  //
// aligned points.                                                         //
// ----------------------------------------------------------------------- //
// Parameters count = 6                                                    //
// Parameters[0] = x0 = translation in X                                   //
// Parameters[1] = y0 = translation in Y                                   //
// Parameters[2] = a  = counterclockwise rotation (in radians)             //
// Parameters[3] = sx = scaling in X                                       //
// Parameters[4] = sy = scaling in T                                       //
// Parameters[5] = b  = counterclockwise shearing (in radians)             //
// *********************************************************************** //

#ifndef TRANSFORM2AFFINEEX_H
#define TRANSFORM2AFFINEEX_H

#include ".\Transform2.h"

template <class Real>
class Transform2AffineEx : public Transform2<Real>
{
	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Transform2AffineEx();

	// ********* //
	// Interface //
	// ********* //
	virtual Point2<Real> TransformPoint(Point2<Real> p);

	// **************** //
	// Helper functions //
	// **************** //
protected:
	virtual bool SetCoefficientsMatrix();
	virtual bool SetKnownsVector();
	virtual bool SetWeightsMatrix();
	virtual bool ConvertParameters();
};

#include "..\src\Transform2AffineEx.inl"

#endif
