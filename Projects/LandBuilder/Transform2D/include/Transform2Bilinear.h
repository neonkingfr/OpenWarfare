// *********************************************************************** //
// Transform2Bilinear                                                      //
// *********************************************************************** //
// Class who defines a two dimensional bilinear transformation             //
// ----------------------------------------------------------------------- //
// x' = a0 + a1 * x + a2 * y + a3 * x * y                                  //
// y' = b0 + b1 * x + b2 * y + b3 * x * y                                  //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
// For parameters calculations are needed at least four distinct and not   //
// aligned points.                                                         //
// ----------------------------------------------------------------------- //
// Parameters count = 8                                                    //
// Parameters[0] = a0                                                      //
// Parameters[1] = a1                                                      //
// Parameters[2] = a2                                                      //
// Parameters[3] = a3                                                      //
// Parameters[4] = b0                                                      //
// Parameters[5] = b1                                                      //
// Parameters[6] = b2                                                      //
// Parameters[7] = b3                                                      //
// *********************************************************************** //

#ifndef TRANSFORM2BILINEAR_H
#define TRANSFORM2BILINEAR_H

#include ".\Transform2.h"

template <class Real>
class Transform2Bilinear : public Transform2<Real>
{
	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Transform2Bilinear();

	// ********* //
	// Interface //
	// ********* //
	virtual Point2<Real> TransformPoint(Point2<Real> p);

	// **************** //
	// Helper functions //
	// **************** //
protected:
	virtual bool SetCoefficientsMatrix();
	virtual bool SetKnownsVector();
	virtual bool SetWeightsMatrix();
	virtual bool ConvertParameters();
};

#include "..\src\Transform2Bilinear.inl"

#endif
