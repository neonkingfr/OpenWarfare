// *********************************************************************** //
// Transform2Conform                                                       //
// *********************************************************************** //
// Class who defines a two dimensional conform transformation              //
// (translation + counterclockwise rotation + homogeneous scaling).        //
// ----------------------------------------------------------------------- //
// x' = x0 + s * (x * cos(a) - y * sin(a))                                 //
// y' = y0 + s * (x * sin(a) + y * cos(a))                                 //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
// For parameters calculations are needed at least two distinct points     //
// ----------------------------------------------------------------------- //
// Parameters count = 4                                                    //
// Parameters[0] = x0 = translation in X                                   //
// Parameters[1] = y0 = translation in Y                                   //
// Parameters[2] = a  = counterclockwise rotation (in radians)             //
// Parameters[3] = s  = homogeneous scaling                                //
// *********************************************************************** //

#ifndef TRANSFORM2CONFORM_H
#define TRANSFORM2CONFORM_H

#include ".\Transform2.h"

template <class Real>
class Transform2Conform : public Transform2<Real>
{
	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Transform2Conform();

	// ********* //
	// Interface //
	// ********* //
	virtual Point2<Real> TransformPoint(Point2<Real> p);

	// **************** //
	// Helper functions //
	// **************** //
protected:
	virtual bool SetCoefficientsMatrix();
	virtual bool SetKnownsVector();
	virtual bool SetWeightsMatrix();
	virtual bool ConvertParameters();
};

#include "..\src\Transform2Conform.inl"

#endif
