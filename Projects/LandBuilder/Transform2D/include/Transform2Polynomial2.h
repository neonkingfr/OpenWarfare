// *********************************************************************** //
// Transform2Bilinear                                                      //
// *********************************************************************** //
// Class who defines a two dimensional polynomial of 2nd order             //
// transformation                                                          //
// ----------------------------------------------------------------------- //
// x' = a0 + a1 * x + a2 * y + a3 * x * y + a4 * x^2 + a5 * y^2            //
// y' = b0 + b1 * x + b2 * y + b3 * x * y + b4 * x^2 + b5 * y^2            //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
// For parameters calculations are needed at least six distinct and not    //
// aligned points.                                                         //
// ----------------------------------------------------------------------- //
// Parameters count = 12                                                   //
// Parameters[ 0] = a0                                                     //
// Parameters[ 1] = a1                                                     //
// Parameters[ 2] = a2                                                     //
// Parameters[ 3] = a3                                                     //
// Parameters[ 4] = a4                                                     //
// Parameters[ 5] = a5                                                     //
// Parameters[ 6] = b0                                                     //
// Parameters[ 7] = b1                                                     //
// Parameters[ 8] = b2                                                     //
// Parameters[ 9] = b3                                                     //
// Parameters[10] = b4                                                     //
// Parameters[11] = b5                                                     //
// *********************************************************************** //

#ifndef TRANSFORM2POLYNOMIAL2_H
#define TRANSFORM2POLYNOMIAL2_H

#include ".\Transform2.h"

template <class Real>
class Transform2Polynomial2 : public Transform2<Real>
{
	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Transform2Polynomial2();

	// ********* //
	// Interface //
	// ********* //
	virtual Point2<Real> TransformPoint(Point2<Real> p);

	// **************** //
	// Helper functions //
	// **************** //
protected:
	virtual bool SetCoefficientsMatrix();
	virtual bool SetKnownsVector();
	virtual bool SetWeightsMatrix();
	virtual bool ConvertParameters();
};

#include "..\src\Transform2Polynomial2.inl"

#endif
