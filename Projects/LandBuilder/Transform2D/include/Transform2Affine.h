// *********************************************************************** //
// Transform2Affine                                                        //
// *********************************************************************** //
// Class who defines a two dimensional affine transformation               //
// (translation + rotation + non-homogeneous scaling).                     //
// ----------------------------------------------------------------------- //
// x' = x0 + sx * x * cos(a) - sy * y * sin(a)                             //
// y' = y0 + sx * x * sin(a) + sy * y * cos(a)                             //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
// For parameters calculations are needed at least three distinct and not  //
// aligned points.                                                         //
// ----------------------------------------------------------------------- //
// Parameters count = 5                                                    //
// Parameters[0] = x0 = translation in X                                   //
// Parameters[1] = y0 = translation in Y                                   //
// Parameters[2] = a  = counterclockwise rotation (in radians)             //
// Parameters[3] = sx = scaling in X                                       //
// Parameters[4] = sy = scaling in Y                                       //
// *********************************************************************** //

#ifndef TRANSFORM2AFFINE_H
#define TRANSFORM2AFFINE_H

#include ".\Transform2.h"

template <class Real>
class Transform2Affine : public Transform2<Real>
{
	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Transform2Affine();

	// ********* //
	// Interface //
	// ********* //
	virtual Point2<Real> TransformPoint(Point2<Real> p);

	// **************** //
	// Helper functions //
	// **************** //
protected:
	virtual bool SetCoefficientsMatrix();
	virtual bool SetKnownsVector();
	virtual bool SetWeightsMatrix();
	virtual bool ConvertParameters();
};

#include "..\src\Transform2Affine.inl"

#endif
