// *********************************************************************** //
// Transform2Homographic                                                   //
// *********************************************************************** //
// Class who defines a two dimensional homographic transformation          //
// ----------------------------------------------------------------------- //
// x' = (a * x + b * y + c) / (g * x + h * y + 1)                          //
// x' = (d * x + e * y + f) / (g * x + h * y + 1)                          //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
// For parameters calculations are needed at least four distinct and not   //
// aligned points.                                                         //
// ----------------------------------------------------------------------- //
// Parameters count = 8                                                    //
// Parameters[0] = a                                                       //
// Parameters[1] = b                                                       //
// Parameters[2] = c                                                       //
// Parameters[3] = d                                                       //
// Parameters[4] = e                                                       //
// Parameters[5] = f                                                       //
// Parameters[6] = g                                                       //
// Parameters[7] = h                                                       //
// *********************************************************************** //

#ifndef TRANSFORM2HOMOGRAPHIC_H
#define TRANSFORM2HOMOGRAPHIC_H

#include ".\Transform2.h"

template <class Real>
class Transform2Homographic : public Transform2<Real>
{
	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Transform2Homographic();

	// ********* //
	// Interface //
	// ********* //
	virtual Point2<Real> TransformPoint(Point2<Real> p);

	// **************** //
	// Helper functions //
	// **************** //
protected:
	virtual bool SetCoefficientsMatrix();
	virtual bool SetKnownsVector();
	virtual bool SetWeightsMatrix();
	virtual bool ConvertParameters();
};

#include "..\src\Transform2Homographic.inl"

#endif
