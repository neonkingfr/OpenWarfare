#include "StdAfx.h"
#include ".\roadplacer.h"
#include "BSpline.h"
#include <es/algorithms/qsort.hpp>

/*

partCount - count of parts for road excluding begin and end part, excluding straight parts and excluding special parts
partNameBase - base of part name used for all parts.
width - width of the road in meters
step - step between parts in meters
straightParts - list of straightParts, each item representing aspect ratio for part. 
        For example: "1,2,5" defines three straight parts with aspect ration 1, 2 and 5.
        For road width=6 means straight length 6,12,30 meters.
        
straightTolerance - tolerance of straight road to real road in meters. If total difference
        between straight road to calculated road is in this tolerance, 
        engine will continue to generate straight parts. If difference leaves
        this tolerance, engine starts to generate curve.
alternativeParts - list of alternative parts and its weight. alternativeParts are
        indexed by position in array. At the position there is number representing probability
        of this item (0-1). Sum of probabilities should be below 1. 1-sum is probability 
        for standard road.
        

naming conventions:
-------------------
name of road part is divided to two parts:
prefix+_+partName

partName is calculated by engine. There is following variants:
Lx,Rx where x is number of part. 1L,1R,2L,2R,3L,3R, etc. L means Left, R means Right. Right is
 always first
0B is begin of road. This is always Left
0E is end of road. This is always Right
xS marks straight parts, x is index. Index is position of part in straightParts array.
yAxL,yAxR - alternative parts. y is index of alternative. Count of parts for one alternative
 must be the same as count of standard parts. There is no begin and end part, so alternative
 parts cannot be used at the begin or at the end.

*/

#pragma warning (disable : 4244)

namespace LandBuildInt
{
	namespace Modules
	{
		using namespace Shapes;

		RoadPlacer::RoadPlacer(void) {}

		RoadPlacer::~RoadPlacer(void) {}

		template<class Arr>
		void ReadDoubleArray(const char* data, Arr& result)
		{
			char* x;
			double val = strtod(data, &x);
			while (x != data)
			{
				if (val > 0) result.Add((typename Arr::DataType)val);
				data = x;
				while (*data && isspace(*data)) data++;
				if (*data == ',') data++;
				val = strtod(data, &x);
			}
		}

		struct MyPoint
		{
			double x, y;      
			ClassIsMovable(MyPoint);

			MyPoint(double x, double y) : x(x), y(y) {}
			MyPoint() {}

			double& operator [] (int i)
			{
				if (i == 0) 
					return x;
				else 
					return y;
			}

			double operator [] (int i) const
			{
				if (i == 0) 
					return x;
				else 
					return y;
			}

			operator double * () { return &x; }

			double Distance(const MyPoint &other) const
			{
				register double dx = other.x - x;
				register double dy = other.y - y;
				return sqrt(dx * dx + dy * dy);
			}

			double Size() const
			{
				return sqrt(x * x + y * y);
			}

			MyPoint operator / (double f) const 
			{
				return MyPoint(x / f, y / f);
			}

			MyPoint operator * (double f) const 
			{
				return MyPoint(x * f, y * f);
			}

			MyPoint operator + (const MyPoint &p) const 
			{
				return MyPoint(x + p.x, y + p.y);
			}

			MyPoint operator - (const MyPoint &p) const 
			{
				return MyPoint(x - p.x, y - p.y);
			}
		};

		AutoArray<MyPoint> CalculateBSpline(const AutoArray<MyPoint>& src, double step)
		{
			BSpline<2, double, const MyPoint*> spline(src.Data(), src.Size());
			double t = 0, last = -1;
			MyPoint tmp;
			spline.GetPoint(t, tmp);
			AutoArray<MyPoint> trg;
			double lastStep = 0;
			do 
			{
				trg.Add(tmp);
				t = spline.GetNextPoint(last = t, step, &lastStep, tmp);
			} 
			while(t != last);
			return trg;
		}

		void RoadPlacer::Run(IMainCommands* land)
		{
			const IShape* shp = land->GetActiveShape();
      
			int partCount;
			const char* tmp;
			if ((tmp = land->QueryValue("partCount")) == 0 || 
				 sscanf(tmp, "%d", &partCount) != 1 || 
				 partCount < 2 || (partCount & 1) == 1)
			{
				ExceptReg::RegExcpt(Exception(1, "partCount must be a number above 0 and dividable by 2"));
				return;
			}
      
			const char* partNameBase = land->QueryValue("partNameBase");
			if (partNameBase == 0)
			{
				ExceptReg::RegExcpt(Exception(2, "partNameBase is not defined"));
				return;
			}
      
			float width;
			float step;
			if ((tmp = land->QueryValue("width")) == 0 || 
				 sscanf(tmp, "%f", &width) != 1 || width <= 0)
			{
				ExceptReg::RegExcpt(Exception(3, "Width of the road cannot be zero or negative"));
				return;
			}

			if ((tmp = land->QueryValue("step")) == 0 || 
				 sscanf(tmp, "%f", &step) != 1 || step <= 0)
			{
				ExceptReg::RegExcpt(Exception(4, "Step of the road cannot be zero or negative"));
				return;
			}

			AutoArray<float, MemAllocStack<float, 16> > straight;
			tmp = land->QueryValue("straightParts");
			if (tmp != 0) ReadDoubleArray(tmp, straight);

			float straightTolerance = width / 10;

			tmp = land->QueryValue("straightTolerance");
			if (tmp)
			{
				if (sscanf(tmp, "%f", &straightTolerance) != 1 ||
					straightTolerance <= 0)
				{
					ExceptReg::RegExcpt(Exception(5, "StraightTolerance cannot be negative"));
					return;
				}
			}

			AutoArray<float, MemAllocStack<float, 16> > alternativeParts;
			tmp = land->QueryValue("alternativeParts ");
			if (tmp != 0) ReadDoubleArray(tmp, alternativeParts);
			float x = 0;
			for (int i = 0; i < alternativeParts.Size(); i++)
			{
				x += alternativeParts[i];
				alternativeParts[i] = x;
			}
			if (x > 1)
			{
				ExceptReg::RegExcpt(Exception(6, "Sum of probabilities for alternativeParts is above 1. It is not allowed"));
				return;
			}

			for (unsigned int i = 0, cnt = shp->GetPartCount(); i < cnt; i++)
			{
				bool opak = false;
				float curStep = step;
				float endPointTolerance = 0.000001f; //tolerance koncoveho bodu (meni se v kazdem cyklu)
				AutoArray<MyPoint> inputPoints; //nacti ridici body
				unsigned int offst = shp->GetPartIndex(i);
				for (unsigned int j = 0, cnt = shp->GetPartSize(j); j < cnt; j++)
				{
					const DVertex &vx = shp->GetVertex(offst + j);
					MyPoint pt;
					pt.x = vx.x;
					pt.y = vx.y;
					inputPoints.Add(pt);
				}
				MyPoint k = inputPoints[0];
				inputPoints.Insert(0, k); //2x duplikace body na zacatku a na konci
				inputPoints.Insert(0, k); //zajisti, ze krivka bude zacinat v techto 
				k = inputPoints[inputPoints.Size() - 1];
				inputPoints.Add(k); //BSpline normalne zacina nekde na puli cesty
				inputPoints.Add(k); //mezi prvnim a tretim bodem
				AutoArray<MyPoint> outputPoints;      //vystupni body
				do 
				{                   
					opak = false;
					///rozteseluj krivku na casti o zadany krok
					outputPoints = CalculateBSpline(inputPoints, curStep);
					//pts=pocet segmentu v ceste (bez prvniho a posledniho bodu)
					int pts = outputPoints.Size(); 
					pts -= 2;
					if (pts < 0)
					{
						outputPoints.Clear();
						outputPoints.Add(inputPoints[0]);
						outputPoints.Add(inputPoints[inputPoints.Size() - 1]);
						pts = 0;
					}
					//clustr=pocet klusteru
					int clustr = pts / partCount;
					//error=celkova chyba konce silnice
					float error = 0;
					//nekompletni cluster pricti k chybe
					if (clustr * partCount < pts) error = curStep * (partCount - (pts - clustr * partCount));
					//velikost deformace posledniho segmentu pricti k chybe
					if (clustr)
					{
						int q = outputPoints.Size() - 1;
						error += (float)(curStep - outputPoints[q].Distance(outputPoints[q - 1]));

						//maximalni mozna chyba je delka celeho klusteru
						float maxError = partCount * curStep;
						//pokud je chyba skoro stejne velka jako cely kluster (o toleranci)
						if (maxError - error < endPointTolerance)
						{
							//odeber vsechny body navic
							while (clustr * partCount + 2 < outputPoints.Size())
								outputPoints.Delete(outputPoints.Size() - 2);              
						}
						//pokud je chyba mensi nez nastavena tolerance
						else if (error < endPointTolerance)
						{
							//prijatelna cesta, nedelej nic, cyklus skonci
						}
						else
						{        
							//urci novy krok
							float newStep;
							//pokud mensi cast dalsiho klusteru precuhuje
							if (error < maxError / 2)
							{
								//staci zmensit krok
								newStep = curStep - error / ((float)(pts + 1)); //rozmelni chybu mezi dilky
								//pokud vetsi cast dalsiho klusteru precuhuje
								curStep = (newStep + curStep) / 2.0f; //zvysuj postupne (snizeni pravdepodobnosti rozkmitani
							}
							else
							{
								//je potreba zvetsit krok aby precuhoval cely segment
								newStep = curStep + (maxError - error)/((float)pts); //rozmelni chybu mezi dilky
								curStep = newStep; //zvysuj postupne (snizeni pravdepodobnosti rozkmitani
							}
							//kvuli stabilite zvysuj toleranci. Cim dele bezi vypocet, tim je benevoletnejsi.
							endPointTolerance = endPointTolerance + endPointTolerance / 2;
							opak = true;
						}
					}
                    else
                    {
                        curStep *= 0.9f;
                        if (curStep<0.0000001) 
                            goto skip;
                        opak = true;
                    }
				}  
				while (opak);

				PlaceRoad(land, outputPoints, partCount, partNameBase, width, straight, straightTolerance, alternativeParts);
skip:;
			}
		}
    
		enum RoadType{roadUnknown, roadCurve, roadStraight};

		class RDTypeArray: public Array<RoadType>
		{
		public:
			RDTypeArray(RoadType* rd, int count) : Array<RoadType>(rd, count) {}
			RDTypeArray(const RDTypeArray &other) : Array<RoadType>(other)
			{
				*const_cast<Array<RoadType>*>((const Array<RoadType>*)&other) = Array<RoadType>(0);
			}
			~RDTypeArray() 
			{
				delete [] Data();
			}      
		};

		RDTypeArray AnalyzeRoad(const Array<MyPoint>& points, int cluster, double straightTolerance, float minStraight)
		{
			//pole useku, kazdy usek nese informaci o tom,
			//v jakem vztahu je s predchozim usekem
			//zde se muze objevit 
			// roadStraight - tento usek pokracuje rovne vuci predchozimu useku
			// roadCurve - tento usek pokracuje zatackou vuci predchozimu useku
			RDTypeArray res(new RoadType[points.Size() - 1], points.Size() - 1);
			int useku = points.Size() - 1;
			//na zacatku predpokladame, ze vsechno jsou zatacky
			for (int i = 0; i < useku; i++) res[i] = roadCurve;

			//vedeme primku predchozim usekem
			MyPoint primka(0, 0);
			double d = straightTolerance * 2;
			for (int i = 1; i < points.Size(); i++)
			{        
				//novy bod lezi od primky vzdalen o toleranci?
				double dd = primka.x * points[i].x + primka.y * points[i].y + d;
				//pokud ne
				if (abs(dd) > straightTolerance)
				{
					//vypocti novou primku pro nasledujici bod  
					primka.x = points[i].y - points[i - 1].y;
					primka.y = points[i - 1].x - points[i].x; 
					primka = primka / primka.Size();
					d = -(primka.x * points[i].x + primka.y * points[i].y);
				}
				else
				{
					//pokud ano, pak to je rovny usek
					res[i - 1] = roadStraight;
				}        
			}

			//prvni usek je vzdy zatacka, coz neni dobry napad, pokud pokracuje rovinkou
			//takze nastavime tento usek rovny useku druhemu
			res[0] = res[1];

			//curvBeg index prvni zatacky
			int curvBeg = 0;
			for (int i = 0; i < useku; i++)
			{
				//pokud jsme narazili na rovinku
				if (res[i] == roadStraight) 
				{
					//spocitej delku predchozi zatacky
					int len = i - curvBeg;
					//kazda zatacka se musi skladat s 'cluster' kousku 
					//dilek (pocatecni dilek je soucasti prniho useku).
					//delka zatacky tedy musi byt po odecteni jednicky delitelny hodnotou 'cluster'.
					//delka 1 neni pripustna
					//pokud je zatacka kratsi nez povoelno, je treba zatacku protahnout do rovne casti
					if (len != 0 && (len == 1 || ((len - 1) % cluster)))
						res[i] = roadCurve;
					else
					{
						//vse je OK, zaznamenej ze dalsi dilek muze byt zatacka
						curvBeg = i + 1;
					}
				}
			}
			bool opak;
			do 
			{
				opak = false;
				//jestlize poslednim usekem je zatacka
				//je treba zajistit, aby tato zatacka byla spravne dlouha, protoze zatacku
				//je nutne ukoncit presne na konci cesty
				if (res[useku - 1] == roadCurve)
				{
					//tento algoritmus je iterativni, dokud neni nalezeno reseni        
					bool opak = false;
					do
					{        
						//zatim nemam podezreni, ze je nutne opakovani
						opak = false; 
						//zacni na poslednim useku
						int i = useku - 1;
						//hledej konec zatacky
						while (i >= 0 && res[i] == roadCurve) i--;
						//konec zatacky nalezen? (jsou zde rovne useky)
						if (i >= 0)
						{
							//spocitej delku zatacky
							int len = useku - i;
							//spocitej kolik useku zbyva do dokonceni zatacky
							int remain = len <= 1 ? cluster : (cluster - (len - 1) % cluster);
							//pokud neco zbyva
							if (remain)
							{            
								//prepis urceny pocet rovnych useku na zatacku
								while (i >= 0 && remain--) res[i] = roadCurve;
								//mohlo dojit ke spojeni dvou zatacek do jedne
								//pak je treba znovu zkontrolovat, zda pocet useku zatacky odpovida pozadovanemu poctu
								opak = true;
							}
						}
					}
					while (opak);
				}
				//nakonec zkontrolujeme rovinky, zda se zde nevyskytuji rovinky kratsi nez zadana hodnota.
				//tak kratke rovinky se nevyplati vytvaret, protoze by v 
				//konecnem dusledku vzniklo vice objektu, nez pouzitim zatacek.
				int i  = 0;
				int cv = 0;
				//pro vsechny useky
				while (i < useku)
				{        
					//pokud je to rovinka
					if (res[i] == roadStraight)
					{
						if (cv % cluster != 1 || cv == 1)
						{
							res[i] = roadCurve;
							i++;
							cv++;
							opak = true;
						}
						else
						{          
							//poznamenej si zacatek
							int j = i + 1;
							//ziskej pocatecni bod;
							MyPoint p1 = points[i];
							//hledej konec rovinky
							while (j < useku && res[j] == roadStraight) j++;
							//koncovy bod
							MyPoint p2 = points[j];
							//vzdalenost je mensi nez minimalni
							if (p2.Distance(p1) < minStraight)
							{
								//v tomto useku nastav tvorbu zatacek
								for (int k = i; k < j; k++) 
								{ 
									res[k] = roadCurve;
									cv++;
								}
								opak = true;
							}
							else
							{
								cv = 0;
							}
							//synchronizuj indexy
							i = j;
						}
					}
					else
					{
						//pokracuj
						i++;
						cv++;
					}
				}
			} 
			while(opak);
			return res;
		}

		struct StraightInfo
		{
			int index;
			float length;
			ClassIsSimpleZeroed(StraightInfo);

			struct Compare
			{
				int operator()(const StraightInfo *a, const StraightInfo *b) const
				{
					if (a->length > b->length) return 1;
					if (a->length < b->length) return -1;
					return 0;
				}
			};
		};

		static MyPoint CalculateSide(const MyPoint &p1, const MyPoint &p2, double side, int pos)
		{
			MyPoint dir = p2 - p1;
			dir = dir / dir.Size();
			MyPoint pdir(dir.y, -dir.x);
			return (pos == 1 ? p2 : p1) + pdir * side;
		}

		static void PlaceStraightObject(IMainCommands *land, const char *partNameBase, int idx, const MyPoint &p1, const MyPoint &dir, double width, double flen)
		{
			char* fullname = (char*)alloca(strlen(partNameBase) + 20);
			sprintf(fullname, "%s%ds", partNameBase, idx);
			Matrix4 pos(flen * dir.x, 0, -width * dir.y, p1.x,
						           0, 1,              0, 0,
						flen * dir.y, 0, width * dir.x, p1.y);            
      
			land->GetObjectMap()->PlaceObject(pos, fullname, 100);
		}

		static void PlaceCurveObject(IMainCommands* land, const char* partNameBase, const char *partId, const MyPoint &p1, const MyPoint &p2, const MyPoint &p3)
		{
			char* fullname = (char*)alloca(strlen(partNameBase) + strlen(partId) + 1);
			sprintf(fullname, "%s%s", partNameBase, partId);
      
			MyPoint dir1 = p1 - p2;
			MyPoint dir3 = p3 - p2;

			Matrix4 pos(dir1.x, 0, dir3.x, p2.x,
						     0, 1,      0, 0,
						dir1.y, 0, dir3.y, p2.y);
			pos = pos * Matrix4(MTranslation,Vector3(0.25,0.25,0.25));
			land->GetObjectMap()->PlaceObject(pos, fullname, 100);      
		}

		static void PlaceCurveObject(IMainCommands* land, const char* partNameBase, char type, int idx, const MyPoint &p1, const MyPoint &p2, const MyPoint &p3)
		{
			char buff[50];
			sprintf(buff, "%d%c", idx, type);
			PlaceCurveObject(land, partNameBase, buff, p1, p2, p3);
		}

		void RoadPlacer::PlaceRoad(IMainCommands *land,
								   const Array<MyPoint> &points, 
								   int cluster, 
								   const char *partNameBase, 
								   double width, 
								   const Array<float> &straight,
								   double straightTolerance,
								   const Array<float> &alternative)
		{
			AutoArray<StraightInfo, MemAllocStack<StraightInfo, 32> > straightList;
			for (int i = 0; i < straight.Size(); i++)
			{
				StraightInfo &a = straightList.Append();
				a.index = i;
				a.length = (float)(straight[i] * width);
			}
			float sidew = (float)(width / 2);
			QSort(straightList, StraightInfo::Compare());
			float minStraight;
			if (straightList.Size())
			{
				minStraight = straightList[0].length * 2;
			}
			else
			{
				minStraight = FLT_MAX;
			}
			RDTypeArray roadInfo = AnalyzeRoad(points, cluster, straightTolerance, minStraight);

			int i = 0;
			RoadType curType = roadStraight;
			MyPoint lastCurvePt2 = CalculateSide(points[0], points[1], -sidew, 1);
			MyPoint lastCurvePt = CalculateSide(points[0], points[1], sidew, 1);
			float swap = -1;
			int idx;

			while (i < roadInfo.Size())
			{
				if (roadInfo[i] == roadStraight)
				{
					MyPoint p1 = points[i];                    
					int j = i + 1;
					while (j < roadInfo.Size() && roadInfo[j] == roadStraight) j++;
					MyPoint p2 = points[j];          
					MyPoint dir = p2 - p1;
					MyPoint cntr;
					swap = -swap;
					p1 = CalculateSide(lastCurvePt2, lastCurvePt2 + dir, swap * sidew, 0);

					if (curType == roadCurve)
					{
						MyPoint p = CalculateSide(lastCurvePt2, lastCurvePt2 + dir, swap * sidew * 2, 0);
						PlaceCurveObject(land, partNameBase, "0e", lastCurvePt, lastCurvePt2, p);
						curType = roadStraight;            
					}
          
					float len = (float)p1.Distance(p2); 
					float minlen = 0;
					float maxlen = 0;
					float correction = 1;
					int x = straightList.Size() - 1;
					while (maxlen < len)
					{
						while (straightList[x].length > (len - maxlen) && x > 0) x--; 
						minlen = maxlen;
						maxlen += straightList[x].length;            
					}
					if (maxlen - len > straightList[0].length / 2)          
						correction = len / maxlen;
					else
						correction = len / minlen; 
					dir = dir / dir.Size();
					x = straightList.Size() - 1;
					lastCurvePt2 = CalculateSide(p1, p2, -sidew, 1);
					lastCurvePt = CalculateSide(p1, p2, sidew, 1);
					while (len > straightList[0].length * correction / 2)
					{
						float flen;
						while ((flen = straightList[x].length * correction) > len && x > 0) x--;           
						PlaceStraightObject(land, partNameBase, x, p1, dir, width, flen);
						p1 = p1 + dir * flen;
						len -= flen;
					}
					i = j + 1;
				}
				else
				{
					if (curType == roadStraight)
					{
						MyPoint p1 = CalculateSide(points[i], points[i + 1], sidew, 1);
						PlaceCurveObject(land, partNameBase, "0b", lastCurvePt, lastCurvePt2, p1);
						lastCurvePt = lastCurvePt2;
						lastCurvePt2 = p1;            
						curType = roadCurve;
						swap = 1;
						idx = 0;
					}
					else
					{
						MyPoint p1 = CalculateSide(points[i], points[i + 1], -sidew * swap, 1);
						//MyPoint p2=CalculateSide(points[i-1],points[i],sidew*swap,1);
						PlaceCurveObject(land, partNameBase, swap > 0 ? 'r' : 'l', idx / 2 + 1, lastCurvePt, lastCurvePt2, p1);
						lastCurvePt = lastCurvePt2;
						lastCurvePt2 = p1;
						swap = -swap;
						idx = (idx + 1) % cluster;
					}
					i++;
				}
			}
		}
	}
}