//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\IBuildModule.h"
#include "..\Shapes\IShape.h"
#include <el/MultiThread/ExceptionRegister.h>

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{
		class DeleteInPolygon : public IBuildModule
		{
		public:
			virtual void Run( IMainCommands* );

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception( unsigned int code, const char* text ) 
        : code( code )
        , text( text ) 
        {
        }

				virtual unsigned int GetCode() const 
        { 
          return (code); 
        }

				virtual const _TCHAR* GetDesc() const 
        { 
          return (text); 
        }

				virtual const _TCHAR* GetModule() const 
        { 
          return ("DeleteInPolygon"); 
        }

				virtual const _TCHAR* GetType() const 
        { 
          return ("DeleteInPolygonException"); 
        }
			};

		private:
			const Shapes::IShape* m_pCurrentShape;   
			int                   m_deletionCounter;

			void DeleteObjects( IMainCommands* pCmdIfc );
			void WriteReport();
		};
	}
}

//-----------------------------------------------------------------------------
