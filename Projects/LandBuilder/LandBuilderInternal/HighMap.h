#pragma once

#include <es/containers/array2D.hpp>
#include "../Shapes/Vertex.h"

namespace LandBuildInt
{
	class HighMap : protected Array2D<float>
	{
		Shapes::DBox _area;
		typedef Array2D<float> Super;
	public:
		HighMap() 
		{
		}

		void Dim(int x, int y, const Shapes::DBox &area)
		{
			_area = area;
			Super::Dim(x, y);
		}

		HighMap(int dimx, int dimy, const Shapes::DBox &area)
		{
			Dim(dimx, dimy, area);
		}

		void Set(int x, int y, float v)
		{
			Super::Set(x, y) = v;
		}

		int PosX(float x, float *factor) const
		{
			if (x < _area.lo.x) x = (float)_area.lo.x;
			if (x > _area.hi.x) x = (float)_area.hi.x;

			float f = (float)((x - _area.lo.x) / (_area.hi.x - _area.lo.x) * (GetXRange() - 1));
			int xx = toLargeInt(floor(f));

			if (xx == GetXRange() - 1)
			{
				if (factor) 
				{
					*factor = 1;
					return xx - 1;
				}
				else 
				{
					return xx;
				}
			}
			else
			{
				if (factor) 
				{
					*factor= f - xx;
				}
				return xx;
			}    
		}

		int PosY(float y, float *factor) const
		{
			if (y < _area.lo.y) y = (float)_area.lo.y;
			if (y > _area.hi.y) y = (float)_area.hi.y;

			float f = (float)((y - _area.lo.y) / (_area.hi.y - _area.lo.y) * (GetYRange() - 1));
			int yy = toLargeInt(floor(f));

			if (yy == GetYRange() - 1)
			{
				if (factor) 
				{
					*factor = 1;
					return yy - 1;
				}
				else 
				{
					return yy;
				}
			}
			else
			{
				if (factor) 
				{
					*factor = f - yy;
				}
				return yy;
			}    
		}

		float PosX(int x) const
		{
			return (float)(x * (_area.hi.x - _area.lo.x) / (GetXRange() - 1) + _area.lo.x);
		}

		float PosY(int y) const
		{
			return (float)(y * (_area.hi.y - _area.lo.y) / (GetYRange() - 1) + _area.lo.y);
		}

		static float Interpolate(float a, float b, float f)
		{
			return a + (b - a) * f;
		}

		float Get(float x, float y) const
		{
			int ix, iy;
			float fx, fy;
			ix = PosX(x, &fx);
			iy = PosY(y, &fy);
			float v1 = Interpolate(Super::Get(ix, iy), Super::Get(ix + 1, iy), fx);
			float v2 = Interpolate(Super::Get(ix, iy + 1), Super::Get(ix + 1, iy + 1), fx);
			return Interpolate(v1, v2, fy);
		}

		///Set the high and the slope
		/**
		* @param x x coord.
		* @param y y coord.
		* @param h new height
		* @param nx coord. x of normal at position 
		* @param ny coord. y of normal at position
		* @note you don't passing  coord. z, it is calculated from xn and yn
		*/
		void Set(float x, float y, float h, float nx, float ny)
		{
			Vector3 n(nx, ny, 1);
			n.Normalize();

			float d = -n.X() * x + n.Y() * y + n.Z() * h;

			int ix, iy;
			float fx, fy;
			ix = PosX(x, &fx);
			iy = PosY(y, &fy);

			float v1 = -(PosX(ix) * n.X() + PosY(iy) * n.Y() + d) / n.Z();
			float v2 = -(PosX(ix + 1) * n.X() + PosY(iy) * n.Y() + d) / n.Z();
			float v3 = -(PosX(ix + 1) * n.X() + PosY(iy + 1) * n.Y() + d) / n.Z();
			float v4 = -(PosX(ix) * n.X() + PosY(iy + 1) * n.Y() + d) / n.Z();
			Set(ix, iy, v1);
			Set(ix + 1, iy, v2);
			Set(ix + 1, iy + 1, v3);
			Set(ix, iy + 1, v4);
		}

		float GetNearestX(float x) const
		{
			int ix;
			float fx;
			ix = PosX(x, &fx);
			if (fx >= 0.5f) 
			{
				return PosX(ix + 1);
			}
			else 
			{
				return PosX(ix);
			}
		}

		float GetNearestY(float y) const
		{
			int iy;
			float fy;
			iy = PosY(y, &fy);
			if (fy >= 0.5f) 
			{
				return PosY(iy + 1);
			}
			else 
			{
				return PosY(iy);
			}
		}

		float GetXStep() const
		{
			return (float)(_area.hi.x - _area.lo.x) / (GetXRange() - 1);
		}

		float GetYStep() const
		{
			return (float)(_area.hi.y - _area.lo.y) / (GetYRange() - 1);
		}

		const Shapes::DBox &GetArea() const 
		{
			return _area;
		}

		Vector3 GetNormal(float x, float y)
		{
			int ix, iy;
			float fx, fy;
			ix = PosX(x, &fx);
			iy = PosY(y, &fy);
			Vector3 v1(PosX(ix), PosY(iy), Super::Get(ix, iy)),
					v2(PosX(ix + 1), PosY(iy), Super::Get(ix + 1, iy)),
					v3(PosX(ix + 1), PosY(iy + 1), Super::Get(ix + 1, iy + 1)),
					v4(PosX(ix), PosY(iy + 1), Super::Get(ix, iy + 1));
			return (((v4 - v1).CrossProduct(v2 - v1)) +
					((v1 - v2).CrossProduct(v2 - v3)) +
					((v2 - v3).CrossProduct(v3 - v4)) +
					((v3 - v4).CrossProduct(v4 - v1))).Normalized();
		}
	};
}
