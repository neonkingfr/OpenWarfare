#include "StdAfx.h"
#include <algorithm>
#include ".\SmoothTerrainAlongPath.h"

#include "..\Shapes\ShapeHelpers.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------------------

		SmoothTerrainAlongPath::SmoothTerrainAlongPath()
		: m_FirstRun(true)
		, m_AllRoadsLoaded(false)
		, m_ShapesCounter(0)
		{
			// initializes const variables
			sDTED2        = "DTED2";
			sUSGSDEM      = "USGSDEM";
			sARCINFOASCII = "ARCINFOASCII";
			sXYZ          = "XYZ";
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::Run(IMainCommands* cmdIfc)
		{
		    if (m_AllRoadsLoaded) return;

			++m_ShapesCounter;

			printf("--------------------------------------\n");
			printf("Started a new shape (%d)\n", m_ShapesCounter);
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();      

			// this only the first time
			if (m_FirstRun)
			{
				// gets parameters from calling cfg file
				if (!GetGlobalParameters(cmdIfc)) return;
				// gets map's parameters from calling cfg file
				if (!GetMapParameters(cmdIfc)) return;
				// load map
				LoadHighMap();

				m_FirstRun = false;
			}

			// this for every shape
			// gets dbf parameters from calling cfg file
			if (GetDbfParameters(cmdIfc))
			{
				// Smooths the shape profile
				SmoothProfile();

				// Smooths the terrain under the path
				SmoothTerrainUnderPath();
			}
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::OnEndPass(IMainCommands* cmdIfc)
		{
			if (m_AllRoadsLoaded) return;
			m_AllRoadsLoaded = true;

			// Smooths the surrounding terrain
			SmoothSurroundingTerrain();

			cmdIfc->GetHeightMap()->AddSamplerModule(new DemSampler(m_MapIn));
			SaveTerrain();
		}

		// --------------------------------------------------------------------------------

		bool SmoothTerrainAlongPath::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			m_GlobalIn.longSmoothType = "None";
			const char* x = cmdIfc->QueryValue("longSmoothType", false);
			if (x != 0) 
			{
				m_GlobalIn.longSmoothType = x;
			}
			else
			{
				printf(">>> --------------------------------------------------------------- <<<\n");
				printf(">>> Missing value for longSmoothType - using default value ('None') <<<\n");
				printf(">>> --------------------------------------------------------------- <<<\n");
			}

			if (m_GlobalIn.longSmoothType != "None" &&
				m_GlobalIn.longSmoothType != "MovingAverage" &&
				m_GlobalIn.longSmoothType != "MaxSlope" &&
				m_GlobalIn.longSmoothType != "Monotone" &&
				m_GlobalIn.longSmoothType != "ConstantSlope" &&
				m_GlobalIn.longSmoothType != "FixedAbsolute" &&
				m_GlobalIn.longSmoothType != "FixedRelative")
			{
				ExceptReg::RegExcpt(Exception(11, ">>> Bad data for longSmoothType <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("longSmoothPar", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.longSmoothPar = atof(x);
				}
				else
				{
					if (m_GlobalIn.longSmoothType != "None" &&
						m_GlobalIn.longSmoothType != "ConstantSlope")
					{
						ExceptReg::RegExcpt(Exception(12, ">>> Bad data for longSmoothPar <<<"));
						return false;
					}
				}
			}
			else
			{
				if (m_GlobalIn.longSmoothType != "None" &&
					m_GlobalIn.longSmoothType != "ConstantSlope")
				{
					ExceptReg::RegExcpt(Exception(13, ">>> Missing longSmoothPar value <<<"));
					return false;
				}
			}

			if (m_GlobalIn.longSmoothType != "MovingAverage" && m_GlobalIn.longSmoothPar < 10)
			{
				ExceptReg::RegExcpt(Exception(14, ">>> longSmoothPar must be greater than 10.0 for MovingAverage <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SmoothTerrainAlongPath::GetDbfParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("width", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.width = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for width <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing width value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SmoothTerrainAlongPath::GetMapParameters(IMainCommands* cmdIfc)
		{
			// gets demType from cfg file
			const char* tmp = cmdIfc->QueryValue("demType", false);
			if (tmp == 0) 
			{
				ExceptReg::RegExcpt(Exception(51, ">>> Missing Dem\'s type <<<"));
				return false;
			}

			m_MapIn.m_DemType = tmp;

			// checks demType
			if((m_MapIn.m_DemType != sDTED2) && (m_MapIn.m_DemType != sUSGSDEM) && (m_MapIn.m_DemType != sARCINFOASCII) && (m_MapIn.m_DemType != sXYZ))
			{
				ExceptReg::RegExcpt(Exception(52, ">>> Dem type not implemented <<<"));
				return false;
			}

			// gets filename from cfg file
			tmp = cmdIfc->QueryValue("filename", false);
			if (tmp == 0)
			{
				ExceptReg::RegExcpt(Exception(53, ">>> Missing Dem\'s filename <<<"));
				return false;
			}

			m_MapIn.m_FileName = tmp;

			// checks file extension
			if(m_MapIn.m_DemType == sDTED2) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if(!(tmpFileName.find(".DT2") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(54, ">>> The file is not a DTED2 <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sUSGSDEM) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if(!(tmpFileName.find(".DEM") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(55, ">>> The file is not a USGS DEM <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sARCINFOASCII) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if((!(tmpFileName.find(".GRD") == tmpFileName.length() - 4)) && (!(tmpFileName.find(".ASC") == tmpFileName.length() - 4)))
				{
					ExceptReg::RegExcpt(Exception(56, ">>> The file is not a Arc/Info ASCII Grid <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sXYZ) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if(!(tmpFileName.find(".XYZ") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(56, ">>> The file is not a XYZ <<<"));
					return false;
				}
			}

			// gets srcLeft from cfg file
			tmp = cmdIfc->QueryValue("srcLeft", false);
			if (tmp != 0)
			{
				if(IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcLeft = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(61, ">>> Bad data for srcLeft <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(62, ">>> Missing srcLeft value <<<"));
				return false;
			}

			// gets srcRight from cfg file
			tmp = cmdIfc->QueryValue("srcRight", false);
			if (tmp != 0)
			{
				if(IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcRight = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(63, ">>> Bad data for srcRight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(64, ">>> Missing srcRight value <<<"));
				return false;
			}

			// gets srcTop from cfg file
			tmp = cmdIfc->QueryValue("srcTop", false);
			if (tmp != 0)
			{
				if(IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcTop = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(65, ">>> Bad data for srcTop <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(66, ">>> Missing srcTop value <<<"));
				return false;
			}

			// gets srcBottom from cfg file
			tmp = cmdIfc->QueryValue("srcBottom", false);
			if (tmp != 0)
			{
				if(IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcBottom = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(67, ">>> Bad data for srcBottom <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(68, ">>> Missing srcBottom value <<<"));
				return false;
			}

			// gets dstLeft from cfg file
			tmp = cmdIfc->QueryValue("dstLeft", false);
			if (tmp != 0)
			{
				if(IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstLeft = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(69, ">>> Bad data for dstLeft <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(70, ">>> Missing dstLeft value <<<"));
				return false;
			}

			// gets dstRight from cfg file
			tmp = cmdIfc->QueryValue("dstRight", false);
			if (tmp != 0)
			{
				if(IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstRight = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(71, ">>> Bad data for dstRight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(72, ">>> Missing dstRight value <<<"));
				return false;
			}

			// gets dstTop from cfg file
			tmp = cmdIfc->QueryValue("dstTop", false);
			if (tmp != 0)
			{
				if(IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstTop = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(73, ">>> Bad data for dstTop <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(74, ">>> Missing dstTop value <<<"));
				return false;
			}

			// gets dstBottom from cfg file
			tmp = cmdIfc->QueryValue("dstBottom", false);
			if (tmp != 0)
			{
				if(IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstBottom = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(75, ">>> Bad data for dstBottom <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(76, ">>> Missing dstBottom value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SmoothTerrainAlongPath::LoadHighMap()
		{
			if(m_MapIn.m_DemType == sDTED2)
			{
				if(!(m_MapData.loadFromDT2(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(81, ">>> Error while loading DTED2 file <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sUSGSDEM)
			{
				if(!(m_MapData.loadFromUSGSDEM(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(82, ">>> Error while loading USGS DEM file <<<"));
					return false;
				}
				else
				{
					// if the elevation units in the DEM are feet, converts to METERS
					if(m_MapData.getWorldUnitsW() == EtRectElevationGrid<double, int>::euFEET)
					{
						m_MapData.convertWorldUnitsW(EtRectElevationGrid<double, int>::euMETERS);
					}
				}
			}
			if(m_MapIn.m_DemType == sARCINFOASCII)
			{
				if(!(m_MapData.loadFromARCINFOASCIIGrid(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(83, ">>> Error while loading Arc/Info Ascii Grid file <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sXYZ)
			{
				if(!(m_MapData.loadFromXYZ(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(84, ">>> Error while loading xyz file <<<"));
					return false;
				}
			}
			m_MapIn.hmData = m_MapData;

			m_MapOriginWorldU = m_MapIn.hmData.getOriginWorldU();
			m_MapOriginWorldV = m_MapIn.hmData.getOriginWorldV();

			m_MapIn.hmData.offsetOriginWorldU(-m_MapOriginWorldU);
			m_MapIn.hmData.offsetOriginWorldV(-m_MapOriginWorldV);

			return true;
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SmoothProfile()
		{
			unsigned int verticesCount = m_pCurrentShape->GetVertexCount();

			if (verticesCount > 1)
			// Smooths only if there is more than one vertex
			{
				if (m_GlobalIn.longSmoothType == "None") 
				{
					ProfileAsShape();
				}
				else if (m_GlobalIn.longSmoothType == "MovingAverage")
				{
					ProfilePointAtSteps(1.0);
					SmoothWithMovingAverage();
				}
				else if (m_GlobalIn.longSmoothType == "MaxSlope")
				{
					ProfileAsShape();
					SmoothWithMaxSlope();
				}
				else if (m_GlobalIn.longSmoothType == "Monotone")
				{
					ProfileAsShape();
					SmoothWithMonotone();
				}
				else if (m_GlobalIn.longSmoothType == "ConstantSlope")
				{
					ProfileAsShape();
					SmoothWithConstantSlope();
				}
				else if (m_GlobalIn.longSmoothType == "FixedAbsolute")
				{
					ProfileAsShape();
					SmoothWithFixedAbsolute();
				}
				else if (m_GlobalIn.longSmoothType == "FixedRelative")
				{
					ProfileAsShape();
					SmoothWithFixedRelative();
				}
			}
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::ProfileAsShape()
		{
			m_ShapeProfile.Clear();
			unsigned int verticesCount = m_pCurrentShape->GetVertexCount();
			
			for (unsigned int i = 0; i < verticesCount; ++i)
			{
				m_ShapeProfile.Add(Shapes::DVector(m_pCurrentShape->GetVertex(i)));
			}

			GetProfileElevations();
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::ProfilePointAtSteps(double step)
		{
			m_ShapeProfile.Clear();

			double shapeLength = GetShapePerimeter(m_pCurrentShape);

			for (double i = 0.0; i <= shapeLength; i += step)
			{
				Shapes::DVector v = GetPointOnShapeAt(m_pCurrentShape, i);
				m_ShapeProfile.Add(v);
			}

			GetProfileElevations();
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::GetProfileElevations()
		{
			unsigned int verticesCount = m_ShapeProfile.Size();

			for (unsigned int i = 0; i < verticesCount; ++i)
			{
				Shapes::DVector v = m_ShapeProfile[i];
				m_ShapeProfile[i].z = m_MapIn.hmData.getWorldWAt(v.x, v.y, EtRectElevationGrid<double, int>::imBILINEAR);
			}
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SmoothWithMovingAverage()
		{
			printf("Applying *Moving Average* to shape's profile\n");

			unsigned int verticesCount = m_ShapeProfile.Size();
			// take the first odd number less than the given one
			int range = static_cast<int>(floor(EtMathD::Fabs(m_GlobalIn.longSmoothPar)));
			if (range % 2 == 0) range--;

			if (verticesCount > range + 1)
			{
				// calculates moving average
				vector<double> movingAverage;
				for (unsigned int i = 0; i < verticesCount; ++i)
				{
					// the first (range - 1) points cannot be averaged
					if (i < range - 1)
					{
						movingAverage.push_back(0.0);
					}
					else
					{
						double z = 0.0;
						for (unsigned int j = i - range + 1; j <= i; ++j)
						{
							z += m_ShapeProfile[j].z;
						}
						movingAverage.push_back(z / range);
					}
				}

				// translates the moving average on the profile
				int halfRange = static_cast<int>(floor(range / 2.0));

				// slope between first point of the profile
				// and the first valid point in the moving average
				double slopeStart = (movingAverage[range - 1] - m_ShapeProfile[0].z) / halfRange;

				// slope between the last point of the profile
				// and the last valid point in the moving average
				double slopeEnd = (m_ShapeProfile[verticesCount - 1].z - movingAverage[verticesCount - 1])  / halfRange;

				for (unsigned int i = 1; i < verticesCount - 1; ++i)
				{
					if (i < halfRange)
					{
						// we are at the beginning of the profile
						// no data -> we need to interpolate between
						// first z in the profile and first valid z in the
						// moving average
						m_ShapeProfile[i].z = m_ShapeProfile[0].z + i * slopeStart;
					}
					else if (i > verticesCount - 1 - halfRange)
					{
						// we are at the end of the profile
						// no data -> we need to interpolate between
						// last z in the profile and last valid z in the
						// moving average
						m_ShapeProfile[i].z = movingAverage[verticesCount - 1] + (i - (verticesCount - 1 - halfRange)) * slopeEnd;
					}
					else
					{
						// we are in the moving average range
						// substitute moving average to profile elevations
						m_ShapeProfile[i].z = movingAverage[i + halfRange];
					}
				}
			}
		}
		
		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SmoothWithMaxSlope()
		{
			printf("Applying *Max Slope* to shape's profile\n");

			unsigned int verticesCount = m_ShapeProfile.Size();
			for (unsigned int i = 1; i < verticesCount - 1; ++i)
			{
				Shapes::DVector v0 = m_ShapeProfile[i - 1];
				Shapes::DVector v1 = m_ShapeProfile[i];
				Shapes::DVector v01(v1, v0);

				double len = v01.SizeXY();
				if (len == 0.0) continue;

				double dz = v1.z - v0.z;
				double slope = dz / len * 100.0;

				if (slope < -m_GlobalIn.longSmoothPar || m_GlobalIn.longSmoothPar < slope)
				{
					double newSlope = m_GlobalIn.longSmoothPar / 100.0;
					if (dz < 0.0)
					{
						newSlope *= -1.0;
					}
					m_ShapeProfile[i].z = v0.z + newSlope * len;
				}
			}
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SmoothWithMonotone()
		{
			printf("Applying *Monotone* to shape's profile\n");

			unsigned int verticesCount = m_ShapeProfile.Size();

			Shapes::DVector vF = m_ShapeProfile[0];
			Shapes::DVector vL = m_ShapeProfile[verticesCount - 1];
			double dzLF = vL.z - vF.z;
			double sign = dzLF > 0.0 ? 1.0 : -1.0;

			for (unsigned int i = 1; i < verticesCount - 1; ++i)
			{
				Shapes::DVector v0 = m_ShapeProfile[i - 1];
				Shapes::DVector v1 = m_ShapeProfile[i];
				Shapes::DVector v01(v1, v0);

				double len = v01.SizeXY();
				if (len == 0.0) continue;

				double dz = v1.z - v0.z;

				double slope = dz / len * 100.0;

				if (sign * slope < 0.0)
				{
					m_ShapeProfile[i].z = v0.z;
				}
				else
				{
					if (slope < -m_GlobalIn.longSmoothPar || m_GlobalIn.longSmoothPar < slope)
					{
						double newSlope = m_GlobalIn.longSmoothPar / 100.0;
						if (dz < 0.0)
						{
							newSlope *= -1.0;
						}
						m_ShapeProfile[i].z = v0.z + newSlope * len;
					}
				}
			}
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SmoothWithConstantSlope()
		{
			printf("Applying *Constant Slope* to shape's profile\n");

			unsigned int verticesCount = m_ShapeProfile.Size();
			Shapes::DVector vF = m_ShapeProfile[0];
			Shapes::DVector vL = m_ShapeProfile[verticesCount - 1];

			double diff = vL.z - vF.z;
			if (diff == 0.0) return;

			double shapeLength = GetShapePerimeter(m_pCurrentShape);
			if (shapeLength == 0.0) return;

			double deltaZ = diff / shapeLength;

			double longCoordinate = 0.0;

			for (unsigned int i = 1; i < verticesCount; ++i)
			{
				Shapes::DVector v0 = m_ShapeProfile[i - 1];
				Shapes::DVector v1 = m_ShapeProfile[i];

				Shapes::DVector v01(v1, v0);
				longCoordinate += v01.SizeXY();
				m_ShapeProfile[i].z = vF.z + longCoordinate * deltaZ; 
			}
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SmoothWithFixedAbsolute()
		{
			printf("Applying *Fixed Absolute* to shape's profile\n");

			unsigned int verticesCount = m_ShapeProfile.Size();

			for (unsigned int i = 0; i < verticesCount; ++i)
			{
				m_ShapeProfile[i].z = m_GlobalIn.longSmoothPar;
			}
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SmoothWithFixedRelative()
		{
			printf("Applying *Fixed Relative* to shape's profile\n");

			unsigned int verticesCount = m_ShapeProfile.Size();

			for (unsigned int i = 0; i < verticesCount; ++i)
			{
				m_ShapeProfile[i].z += m_GlobalIn.longSmoothPar;
			}
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SmoothTerrainUnderPath()
		{
			// here we smooth only the terrain which is under the path
			// or in the immediate neighborhood (all the triangle which
			// have some area under the strip having the polyline as 
			// centerline and the specified width)

			unsigned int verticesCount = m_ShapeProfile.Size();

			if (verticesCount > 1)
			// Smooths only if there are is than one vertex
			{
				printf("Smoothing terrain under shape...\n");

				// first we smooth the closest neighborhood of the path
				for (unsigned int i = 0; i < verticesCount - 1; ++i)
				{
					Shapes::DVector v0 = m_ShapeProfile[i];
					Shapes::DVector v1 = m_ShapeProfile[i + 1];

					// segment vector (for the moment only 2D)
					Shapes::DVector segment(v1, v0);

					// the length of the current segment
					double segLength = segment.SizeXY();

					if (segLength > 0.0)
					// goes on only if the segment is a real segment
					{
						Shapes::DVertex newV0(v0);
						Shapes::DVertex newV1(v1);

						// searches for previous segment
						// if this segment and the previous are not aligned
						// extend this segment
						if (i > 0)
						{
							Shapes::DVector vP0 = m_ShapeProfile[i - 1];
							Shapes::DVector vP1 = m_ShapeProfile[i];

							// previous segment vector 
							Shapes::DVector segmentPrev(vP1, vP0);

							if (EtMathD::Fabs(EtMathD::Fabs(segmentPrev.DotXY(segment)) - 1.0) != 0.000001)
							{
								// extends this segment on the first vertex side
								double stretchFactor = (segLength + m_DbfIn.width * 0.5) / segLength;
								Shapes::DVector tempV(v0, v1);
								double dz = tempV.z / segLength;
								tempV *= stretchFactor;
								newV0 = tempV.GetSecondEndXY(v1);
								newV0.z = v1.z + dz * tempV.SizeXY();
							}
						}

						// searches for next segment
						// if this segment and the next are not aligned
						// extend this segment
						if (i < verticesCount - 2)
						{
							Shapes::DVector vN0 = m_ShapeProfile[i + 1];
							Shapes::DVector vN1 = m_ShapeProfile[i + 2];

							// previous segment vector 
							Shapes::DVector segmentNext(vN1, vN0);

							if (EtMathD::Fabs(EtMathD::Fabs(segmentNext.DotXY(segment)) - 1.0) != 0.000001)
							{
								// extends this segment on the second vertex side
								double stretchFactor = (segLength + m_DbfIn.width * 0.5) / segLength;
								Shapes::DVector tempV(v1, v0);
								double dz = tempV.z / segLength;
								tempV *= stretchFactor;
								newV1 = tempV.GetSecondEndXY(v0);
								newV1.z = v0.z + dz * tempV.SizeXY();
							}
						}

						Shapes::DVector newSegment(newV1, newV0);
						segment = newSegment;
						segLength = segment.SizeXY();

						// for speed searches only the vertex that are close to the current segment
						// the localGrid variable will contain the vertices inside the rectangle 
						// containing the current segment
						EtRectangle<double> boundingRect = SegmentBoundingBox(newV0, newV1);
						EtRectElevationGrid<double, int> localGrid = m_MapIn.hmData.subGrid(boundingRect.GetLeft(), boundingRect.GetBottom(), boundingRect.GetRight(), boundingRect.GetTop());

						double sizeU = localGrid.getSizeU();
						double sizeV = localGrid.getSizeV();

						// calculates the offset of the localGrid with respect to the global grid
						unsigned int offsetU = static_cast<unsigned int>((localGrid.getWorldUAt(0) - m_MapIn.hmData.getWorldUAt(0)) / m_MapIn.hmData.getResolutionU());
						unsigned int offsetV = static_cast<unsigned int>((localGrid.getWorldVAt(0) - m_MapIn.hmData.getWorldVAt(0)) / m_MapIn.hmData.getResolutionV());

						double sqrt2 = EtMathD::Sqrt(2.0);
						for (int iU = 0; iU < sizeU; ++iU)
						{
							for (int iV = 0; iV < sizeV; ++iV)
							{
								// the unitary vector along the segment
								Shapes::DVector unitSegment(newV1, newV0);
								unitSegment.NormalizeXYInPlace();

								// the vector from the first end of the segment to the
								// vertex of the grid (for the moment only 2D)
								Shapes::DVector tempNode(localGrid.getWorldUAt(iU), localGrid.getWorldVAt(iV));
								Shapes::DVector node(tempNode, newV0);

								// the length of the component of the vector node along the segment
								double proj = node.DotXY(unitSegment);

								// if 0 <= proj <= segLength the projection of the node on
								// the segment is really ON the segment
								// we go on with calculation only in this case
								if (0.0 <= proj && proj <= segLength)
								{
									// measures the distance of the node from the segment
									double nodeLength = node.SizeXY();
									double dist = EtMathD::Sqrt(nodeLength * nodeLength - proj * proj);

									// modify only vertices that are closer than (width / 2 + terrain resolution) * sqrt(2) to the path
									if (dist <= (m_DbfIn.width * 0.5 + m_MapIn.hmData.getResolutionU()) * sqrt2)
									{
										// goes on only if both ends of the segment are inside the map
										if (newV0.z != NO_ELEVATION && newV1.z != NO_ELEVATION)
										{
											// the elevation on the projection of the node on the segment
											double elevationAtP = newV0.z + (newV1.z - newV0.z) * proj / segLength;
											// the node takes the same height value of this projection
											FixedVertex fV(offsetU + iU, offsetV + iV, elevationAtP);
											m_UnderFixedList.AddVertex(fV);
										}
									}
								}
							}
						}
					}
				}
				printf("Done.\n");
			}
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SmoothSurroundingTerrain()
		{
			printf("--------------------------------\n");
			printf("All shapes loaded               \n");
			printf("--------------------------------\n");
			printf("Smoothing surrounding terrain...\n");

			size_t underFixedCount = m_UnderFixedList.Size(); 

			if (underFixedCount > 0)
			{
				// first averages multiple fixed points and updates the terrain
				for (unsigned int i = 0; i < underFixedCount; ++i)
				{
					FixedVertex& v = m_UnderFixedList.GetVertex(i);
					if (v.Count() > 1)
					{
						v.AverageZ();
					}
					m_MapIn.hmData.setWorldWAt(v.X(), v.Y(), v.Z());
				}

				unsigned int mapSizeU = m_MapIn.hmData.getSizeU();
				unsigned int mapSizeV = m_MapIn.hmData.getSizeV();

				double z1, z2, z3;
				double zM12, dz, z;

				ProgressBar<int> pb(underFixedCount);

				// now we smooth the surrounding
				for (unsigned int i = 0; i < underFixedCount; ++i)
				{
					pb.AdvanceNext(1);

					const FixedVertex& fixed = m_UnderFixedList.GetVertex(i);

					if (m_UnderFixedList.Contains(fixed.X() - 1, fixed.Y()) != -1)
					{
						if (m_UnderFixedList.Contains(fixed.X(), fixed.Y() + 1) != -1)
						{
							if (m_UnderFixedList.Contains(fixed.X() - 1, fixed.Y() + 1) == -1)
							{
								if (((fixed.X() - 1) >= 0) && ((fixed.Y() + 1) < mapSizeV))
								{
									z1 = m_UnderFixedList.GetZAt(fixed.X() - 1, fixed.Y());
									z2 = m_UnderFixedList.GetZAt(fixed.X(), fixed.Y() + 1);
									z3 = m_UnderFixedList.GetZAt(fixed.X(), fixed.Y());

									if (z1 != cNODATA && z2 != cNODATA && z3 != cNODATA)
									{
										zM12 = (z1 + z2) * 0.5;
										dz   = zM12 - z3;
										z    = z3 + 2.0 * dz;

										m_MapIn.hmData.setWorldWAt(fixed.X() - 1, fixed.Y() + 1, z);
									}
								}
							}
						}
					}

					if (m_UnderFixedList.Contains(fixed.X() + 1, fixed.Y()) != -1)
					{
						if (m_UnderFixedList.Contains(fixed.X(), fixed.Y() - 1) != -1)
						{
							if (m_UnderFixedList.Contains(fixed.X() + 1, fixed.Y() - 1) == -1)
							{
								if (((fixed.X() + 1) < mapSizeU) && ((fixed.Y() - 1) >= 0))
								{
									z1 = m_UnderFixedList.GetZAt(fixed.X() + 1, fixed.Y());
									z2 = m_UnderFixedList.GetZAt(fixed.X(), fixed.Y() - 1);
									z3 = m_UnderFixedList.GetZAt(fixed.X(), fixed.Y());

									if (z1 != cNODATA && z2 != cNODATA && z3 != cNODATA)
									{
										zM12 = (z1 + z2) * 0.5;
										dz   = zM12 - z3;
										z    = z3 + 2.0 * dz;

										m_MapIn.hmData.setWorldWAt(fixed.X() + 1, fixed.Y() - 1, z);
									}
								}
							}
						}
					}
				}
			}
			printf("Smoothing completed.   \n");
		}

		// --------------------------------------------------------------------------------

		void SmoothTerrainAlongPath::SaveTerrain()
		{
			printf("Saving modified terrain...\n");

			string filename  = m_MapIn.m_FileName.substr(0, m_MapIn.m_FileName.length() - 4);
			string extension = m_MapIn.m_FileName.substr(m_MapIn.m_FileName.length() - 4, 4);

			filename = filename + "Mod" + extension;

			m_MapIn.hmData.offsetOriginWorldU(m_MapOriginWorldU);
			m_MapIn.hmData.offsetOriginWorldV(m_MapOriginWorldV);

			if (m_MapIn.m_DemType == sDTED2)
			{
				// not implemented yet
			}
			else if (m_MapIn.m_DemType == sUSGSDEM)
			{
				// not implemented yet
			}
			else if (m_MapIn.m_DemType == sARCINFOASCII)
			{
				m_MapIn.hmData.saveAsWorldToARCINFOASCIIGrid(filename);
			}
			else if (m_MapIn.m_DemType == sXYZ)
			{
				m_MapIn.hmData.saveAsWorldToXYZ(filename);
			}

			printf("Done.\n");
		}

		// --------------------------------------------------------------------------------

		EtRectangle<double> SmoothTerrainAlongPath::SegmentBoundingBox(const Shapes::DVertex& v0, const Shapes::DVertex& v1)
		{
			double segDirection = EtMathD::SegmentDirection_XY_CCW_X(v0.x, v0.y, v1.x, v1.y);

			double width = m_DbfIn.width * 0.5 + m_MapIn.hmData.getResolutionU() * EtMathD::Sqrt(2.0);
			double widthSin = width * EtMathD::Sin(segDirection);
			double widthCos = width * EtMathD::Sin(segDirection);

			// right point at 0
			double x0R = v0.x + widthSin;
			double y0R = v0.y - widthCos;

			double left   = x0R;
			double right  = x0R;
			double top    = y0R;
			double bottom = y0R;

			// left point at 0
			double x0L = v0.x - widthSin;
			double y0L = v0.y + widthCos;
			
			if (left   > x0L) left   = x0L;
			if (right  < x0L) right  = x0L;
			if (bottom > y0L) bottom = y0L;
			if (top    < y0L) top    = y0L;

			// right point at 1
			double x1R = v1.x + widthSin;
			double y1R = v1.y - widthCos;

			if (left   > x1R) left   = x1R;
			if (right  < x1R) right  = x1R;
			if (bottom > y1R) bottom = y1R;
			if (top    < y1R) top    = y1R;

			// left point at 0
			double x1L = v1.x - widthSin;
			double y1L = v1.y + widthCos;

			if (left   > x1L) left   = x1L;
			if (right  < x1L) right  = x1L;
			if (bottom > y1L) bottom = y1L;
			if (top    < y1L) top    = y1L;

			return EtRectangle<double>(left, top, right, bottom);
		}
	}
}
