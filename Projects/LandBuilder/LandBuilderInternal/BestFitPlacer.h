#pragma once

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>
#include "..\Shapes\ShapeHelpers.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class BestFitPlacer : public IBuildModule
		{
		public:
			struct DbfInput
			{
				double desiredHeight;
				double dirCorrection;
			};

			struct ObjectInput
			{
				RString m_Name;
				double  m_Length;
				double  m_Width;
				double  m_Height;

				ObjectInput() 
				{
				}

				ObjectInput(RString name, double length, double width, double height)
				: m_Name(name)
				, m_Length(length)
				, m_Width(width)
				, m_Height(height)
				{
				}

				ClassIsMovable(ObjectInput);
			};

		public:
			virtual void Run(IMainCommands*);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, DbfInput& dbfIn,
												  AutoArray<BestFitPlacer::ObjectInput, MemAllocStack<BestFitPlacer::ObjectInput, 32> >& objectsIn);

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) 
				: code(code)
				, text(text) 
				{
				}

				virtual unsigned int GetCode() const 
				{ 
					return code; 
				}

				virtual const _TCHAR* GetDesc() const 
				{ 
					return text; 
				}

				virtual const _TCHAR* GetModule() const 
				{ 
					return "BestFitPlacer"; 
				}

				virtual const _TCHAR* GetType() const 
				{ 
					return "BestFitPlacerException"; 
				}
			};

		private:
			const Shapes::IShape* m_pCurrentShape;   
			DbfInput              m_DbfIn;

			double m_CalcLength;
			double m_CalcWidth;
			double m_CalcHeight;
			double m_CalcOrientation;

			AutoArray<ObjectInput, MemAllocStack<ObjectInput, 32> > m_ObjectsIn;
			ModuleObjectOutputArray                                 m_ObjectsOut;

			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetObjectsParameters(IMainCommands* cmdIfc);
			void SetGeometry();
			void CreateObjects();
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();
		};
	}
}