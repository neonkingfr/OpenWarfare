#pragma once

#include "IBuildModule.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class HeightMapTest : public IBuildModule
		{
		public:
			DECLAREMODULEEXCEPTION("HeightMapTest");

			virtual void Run(IMainCommands *cmdIfc);
		};
	}
}

