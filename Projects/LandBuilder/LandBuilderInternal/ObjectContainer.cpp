#include "StdAfx.h"
#include ".\objectcontainer.h"

namespace LandBuildInt
{
	RStringI ObjectContainer::ShareString(const RStringI& str)
	{
		RStringI* fnd = _stringTable.Find(str);
		if(fnd == 0) 
		{
			_stringTable.Add(str);
			return str;
		}
		else
		{
			return *fnd;
		}
	}

	// -------------------------------------------------------------------------- //

	RStringI ObjectContainer::ShareString(const RStringI& str) const
	{
		RStringI* fnd = _stringTable.Find(str);
		if(fnd == 0) 
		{
			return str;
		}
		else
		{
			return *fnd;
		}
	}

	// -------------------------------------------------------------------------- //

	ObjectContainer::ObjectContainer(void) : _idxXPos(16), _idxYPos(16), _idxName(16), _stringTable(16)
	{
		_idxName.SetOwner(&_objects);
		_idxXPos.SetOwner(&_objects);
		_idxYPos.SetOwner(&_objects);
	}

	// -------------------------------------------------------------------------- //

	ObjectContainer::~ObjectContainer(void)
	{
	}

	// -------------------------------------------------------------------------- //

	void ObjectContainer::IndexItem(int id)
	{    
		id = id << 1;
		_idxName.Add(id);
		_idxXPos.Add(id);
		_idxYPos.Add(id);
	}

	// -------------------------------------------------------------------------- //

	int ObjectContainer::Add(const ObjectDesc& object)
	{
		int id = _objects.Add(ObjectDesc(ShareString(object.GetName()), object.GetPositionMatrix(), object.GetTag()));
		IndexItem(id);
		return id;
	}

	// -------------------------------------------------------------------------- //

	void ObjectContainer::ReIndex()
	{
		_idxName.Clear();
		_idxXPos.Clear();
		_idxYPos.Clear();
		for (int i = 0; i < _objects.Size(); ++i)
		{
			if(_objects.IsIdValid(i)) IndexItem(i);
		}
	}

	// -------------------------------------------------------------------------- //

	bool ObjectContainer::Delete(int id)
	{
//        printf("Delete object: %d\n",id);
		int arrid = id << 1;
		_idxName.Remove(arrid);
		_idxXPos.Remove(arrid);
		_idxYPos.Remove(arrid);
		return _objects.Delete(id);
	}

	// -------------------------------------------------------------------------- //

	static int TempID(ObjectDesc* obj)
	{
		int id = (int)obj;
		id |= 1;
		return id;
	}

	// -------------------------------------------------------------------------- //

	const ObjectDesc* ObjectContainer::NearestOnX(float x, float y, unsigned int tag, const Array<unsigned int>& ignore) const
	{
		SearchObj refPos(x, y);
		double best = DBL_MAX;
		const ObjectDesc* bestObj = 0;
		BTreeIterator<int, IndexByXPos> fwd(_idxXPos);
		fwd.BeginFrom(TempID(&refPos));
		BTreeIterator<int, IndexByXPos> bkw = fwd;
		int* fnd;
		while ((fnd = fwd.Next()) != 0)
		{
			const ObjectDesc* obj = _idxXPos.ConvIdToObject(*fnd);
			if (tag == 0 || obj->GetTag() == tag)
			{
				double cx = obj->GetPositionVector()[0];
				double cy = obj->GetPositionVector()[1];
				double dx = cx - x;
				double dy = cy - y;
				if (fabs(dx) > best) break;
				double sz = sqrt(dx * dx + dy * dy);
				if (sz < best) 
				{          
					for (int i = 0; i < ignore.Size(); ++i)
					{
						if (ignore[i] == fnd[0] >> 1) goto skip;
					}
					best = sz;
					bestObj = obj;
skip:;
				}
			}
		}

		bkw.Next();
		while ((fnd = bkw.Previous()) != 0)
		{
			const ObjectDesc* obj = _idxXPos.ConvIdToObject(*fnd);
			if (tag == 0 || obj->GetTag() == tag)
			{
				double cx = obj->GetPositionVector()[0];
				double cy = obj->GetPositionVector()[1];
				double dx = cx - x;
				double dy = cy - y;
				if (fabs(dx) > best) break;
				double sz = sqrt(dx * dx + dy * dy);
				if (sz < best) 
				{
					for (int i = 0; i < ignore.Size(); ++i)
					{
						if (ignore[i]==fnd[0]>>1) goto skip2;
					}
					best = sz;
					bestObj = obj;
skip2:;
				}
			}
		}
		return bestObj;
	}

	// -------------------------------------------------------------------------- //

	const ObjectDesc* ObjectContainer::NearestOnY(float x, float y, unsigned int tag, const Array<unsigned int>& ignore) const
	{
		SearchObj refPos(x, y);
		double best = DBL_MAX;
		const ObjectDesc* bestObj = 0;
		BTreeIterator<int, IndexByYPos> fwd(_idxYPos);
		fwd.BeginFrom(TempID(&refPos));
		BTreeIterator<int, IndexByYPos> bkw = fwd;
		int* fnd;
		while ((fnd = fwd.Next()) != 0)
		{
			const ObjectDesc* obj = _idxYPos.ConvIdToObject(*fnd);
			double cx = obj->GetPositionVector()[0];
			double cy = obj->GetPositionVector()[1];
			double dx = cx - x;
			double dy = cy - y;
			if (fabs(dy) > best) break;
			double sz = sqrt(dx * dx + dy * dy);
			if (sz < best) 
			{
				best = sz;
				bestObj = obj;
			}
		}
		bkw.Next();
		while ((fnd = bkw.Previous()) != 0)
		{
			const ObjectDesc* obj = _idxYPos.ConvIdToObject(*fnd);
			double cx = obj->GetPositionVector()[0];
			double cy = obj->GetPositionVector()[1];
			double dx = cx - x;
			double dy = cy - y;
			if(fabs(dy) > best) break;
			double sz = sqrt(dx * dx + dy * dy);
			if (sz < best) 
			{
				best = sz;
				bestObj = obj;
			}
		}
		return bestObj;
	}

	// -------------------------------------------------------------------------- //

	int ObjectContainer::GetObjectID(const ObjectDesc* obj) const
	{
		int k = _objects.GetObjectIndex(obj);
		if (!_objects.IsIdValid(k)) return -1;
		return k;
	}

	// -------------------------------------------------------------------------- //

	bool ObjectContainer::SetObjectName(int ID, const RStringI& name)
	{
		if (_objects.IsIdValid(ID))
		{
			_objects[ID] = ObjectDesc(ShareString(name), _objects[ID].GetPositionMatrix(), _objects[ID].GetTag());
			return true;
		}
		else
		{
			return false;
		}
	}

	// -------------------------------------------------------------------------- //

	SRef<AutoArray<const ObjectDesc*> > ObjectContainer::ObjectsInBox(const Shapes::DBox& box, unsigned int tag) const
	{
		int left, right, top, bottom;
		left   = _idxXPos.RankOf(TempID(&SearchObj(box.lo.x, 0)));
		right  = _idxXPos.RankOf(TempID(&SearchObj(box.hi.x, 0)));
		top    = _idxYPos.RankOf(TempID(&SearchObj(0, box.lo.y)));
		bottom = _idxYPos.RankOf(TempID(&SearchObj(0, box.hi.y)));

		AutoArray<const ObjectDesc*>* ret = new AutoArray<const ObjectDesc*>;

		if (right - left < bottom - top)
		{
			BTreeIterator<int, IndexByXPos> iter(_idxXPos);
			iter.BeginFrom(*_idxXPos.ItemWithRank(left));
			int *fnd;
			while ((fnd = iter.Next()) != 0)        
			{
				const ObjectDesc* obj = _idxXPos.ConvIdToObject(*fnd);
				if (tag == 0 || obj->GetTag() == tag)
				{
					const Vector3& vx = obj->GetPositionVector();
					if(vx[0] >= box.hi.x) break;
					if(vx[1] >= box.lo.y && vx[1] < box.hi.y)
					ret->Add(obj);
				}
			}
		}
		else
		{
			BTreeIterator<int, IndexByYPos> iter(_idxYPos);
			iter.BeginFrom(*_idxYPos.ItemWithRank(top));
			int *fnd;
			while ((fnd = iter.Next()) != 0)        
			{
				const ObjectDesc* obj = _idxYPos.ConvIdToObject(*fnd);
				if (tag == 0 || obj->GetTag() == tag)
				{
					const Vector3& vx = obj->GetPositionVector();
					if (vx[1] >= box.hi.y) break;
					if (vx[0] >= box.lo.x && vx[0] < box.hi.x)
					ret->Add(obj);
				}
			}
		}
		return ret;
	}

	// -------------------------------------------------------------------------- //

	const ObjectDesc* ObjectContainer::Nearest(float x, float y, unsigned int tag, const Array<unsigned int>& ignore) const
	{
		if (_objects.Size() == 0) return 0;
		int rankx = _idxXPos.RankOf(TempID(&SearchObj(x, y, tag)));
		int ranky = _idxYPos.RankOf(TempID(&SearchObj(x, y, tag)));
		if (rankx >= _idxXPos.Size()) rankx = _idxXPos.Size() - 1;
		if (ranky >= _idxYPos.Size()) ranky = _idxYPos.Size() - 1;

		const ObjectDesc* obj1 = _idxXPos.ConvIdToObject(*_idxXPos.ItemWithRank(rankx));
		const ObjectDesc* obj2 = _idxYPos.ConvIdToObject(*_idxYPos.ItemWithRank(ranky));
		Vector3 vx(x, y, 0);
		if (obj1->GetPositionVector().Distance2(vx) < obj2->GetPositionVector().Distance2(vx))
		{
			return NearestOnX(x, y, tag, ignore);
		}
		else
		{
			return NearestOnY(x, y, tag, ignore);
		}
	}

	// -------------------------------------------------------------------------- //

	bool ObjectContainer::Replace(int id, const ObjectDesc& object)
	{
		if (!IsValidObjectID(id)) return false;
		int arrid = TempID(&_objects[id]);
		_idxName.Remove(arrid);
		_idxXPos.Remove(arrid);
		_idxYPos.Remove(arrid);
		_objects[id] = object;
		IndexItem(id);
		return true;
	}
}