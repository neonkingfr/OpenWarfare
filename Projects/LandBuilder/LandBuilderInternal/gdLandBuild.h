#pragma once

#include <el/Evaluator/express.hpp>
#include "../Shapes/IShape.h"

#define TYPES_LANDBUILDER(XX, Category) \
  XX("Shape", GdShapeTypeId, CreateShapeType, "@Shape", "Shape", "One shape from shapefile", Category) \

TYPES_LANDBUILDER(DECLARE_TYPE, LandBuilder)

class IGdShape: public GameData
{
public:
  virtual const Shapes::IShape *GetShapeRef() const=0;
  virtual const GameType &GetType() const  {return GdShapeTypeId;}
  virtual RString GetText() const;
  virtual bool IsEqualTo(const GameData *data) const ;
  virtual const char *GetTypeName() const {return "Shape";}

  static IGdShape *Instance(GameData *gd) {return static_cast<IGdShape *>(gd);}
  static const IGdShape *Instance(const GameData *gd) {return static_cast<const IGdShape *>(gd);}

  static const Shapes::IShape *Shape(const GameData *gd) {return Instance(gd)->GetShapeRef();}
};

class GdConstShape: public IGdShape
{
  const Shapes::IShape *_shape;
public:
  GdConstShape(const Shapes::IShape *shape):_shape(shape) {}

  virtual const Shapes::IShape *GetShapeRef() const {return _shape;}
  virtual GameData *Clone() const {return new GdConstShape(*this);}

};

class GdTempShape: public IGdShape
{
  Shapes::IShape *_shape;
  Shapes::VertexArray _vxArray;
public:
  GdTempShape():_shape(0) {}
  GdTempShape(const GdTempShape &other):_shape(other._shape->Copy(&_vxArray)) {}

  Shapes::VertexArray *GetVertexArray() {return &_vxArray;}

  void SetShape(Shapes::IShape *shp)
  {
    delete _shape;
    _shape=shp;
  }
  ~GdTempShape() {delete _shape;}

  virtual GameData *Clone() const {return new GdTempShape(*this);}

  virtual const Shapes::IShape *GetShapeRef() const {return _shape;}
};


class GdLandBuild
{
public:
  static void RegisterToGameState(GameState *gState);
};
