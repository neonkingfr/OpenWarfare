#include "StdAfx.h"
#include ".\nydummybuildingplacer.h"

#include "..\HighMapLoaders\include\EtMath.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------------------

		NYDummyBuildingPlacer::NYDummyBuildingPlacer()
		{
		}

		// --------------------------------------------------------------------------------

		void NYDummyBuildingPlacer::Run(IMainCommands* cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = const_cast<Shapes::IShape*>(cmdIfc->GetActiveShape()); 

			// gets parameters from calling cfg file
			if(GetGlobalParameters(cmdIfc))
			{
				// gets dbf parameters from calling cfg file
				if(GetDbfParameters(cmdIfc))
				{
					// initializes random seed
					srand(m_GlobalIn.randomSeed);

					CalculateOrientedBBox();

					CalculateRoadNetwork();

					CalculateBlocks();

					// creates objects to export
					CreateObjects();

					RotateObjectsAboutShapeBarycenter();

					DeleteOutsideObjects();

					// export the objects
					ExportCreatedObjects(cmdIfc);

					// writes the report
					WriteReport();
				}
			}
		}

		// --------------------------------------------------------------------------------

		ModuleObjectOutputArray* NYDummyBuildingPlacer::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn)
		{
			m_pCurrentShape = const_cast<Shapes::IShape*>(shape);
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;

			if(m_TempObjectsOut.Size() != 0) m_TempObjectsOut.Clear();
			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// initializes random seed
			srand(m_GlobalIn.randomSeed);

			CalculateOrientedBBox();

			CalculateRoadNetwork();

			CalculateBlocks();

			// creates objects to export
			CreateObjects();

			RotateObjectsAboutShapeBarycenter();

			DeleteOutsideObjects();

			return &m_ObjectsOut;
		}

		// --------------------------------------------------------------------------------

		bool NYDummyBuildingPlacer::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing randomSeed value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("buildingMinHeight", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.buildingMinHeight = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(13, ">>> Bad data for buildingMinHeight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(14, ">>> Missing buildingMinHeight value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("buildingMaxHeight", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.buildingMaxHeight = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(15, ">>> Bad data for buildingMaxHeight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(16, ">>> Missing buildingMaxSize value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("modelNameBase", false);
			if (x != 0) 
			{
				m_GlobalIn.modelNameBase = x;
			}
			else
			{
				ExceptReg::RegExcpt(Exception(17, ">>> Missing modelNameBase value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("modelVariants", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.modelVariants = atoi(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(18, ">>> Bad data for modelVariants <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(19, ">>> Missing modelVariants value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool NYDummyBuildingPlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("mainDirection", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.mainDirection = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(21, ">>> Bad data for mainDirection <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(22, ">>> Missing mainDirection value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("roadMinWidth", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.roadMinWidth = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(23, ">>> Bad data for roadMinWidth <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(24, ">>> Missing roadMinWidth value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("roadMaxWidth", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.roadMaxWidth = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(25, ">>> Bad data for roadMaxWidth <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(26, ">>> Missing roadMaxWidth value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("blockMinSize", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.blockMinSize = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(27, ">>> Bad data for blockMinSize <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(28, ">>> Missing blockMinSize value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("blockMaxSize", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.blockMaxSize = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(29, ">>> Bad data for blockMaxSize <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(30, ">>> Missing blockMaxSize value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("blockMinSubdivisions", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_DbfIn.blockMinSubdivisions = atoi(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(31, ">>> Bad data for blockMinSubdivisions <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(32, ">>> Missing blockMinSubdivisions value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("blockMaxSubdivisions", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_DbfIn.blockMaxSubdivisions = atoi(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(33, ">>> Bad data for blockMaxSubdivisions <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(34, ">>> Missing blockMaxSubdivisions value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		void NYDummyBuildingPlacer::CalculateOrientedBBox()
		{
//			double angle = m_DbfIn.mainDirection / 180.0 * EtMathD::PI;
//			m_pCurrentShape->RotateAboutBarycenter(-angle);
			m_OrientedBBox = m_pCurrentShape->CalcBoundingBox();
//			m_pCurrentShape->RotateAboutBarycenter(angle);
		}

		// --------------------------------------------------------------------------------

		void NYDummyBuildingPlacer::CalculateRoadNetwork()
		{
			m_VRoads.Clear();
			m_HRoads.Clear();

			double minX = m_OrientedBBox.lo.x;
			double minY = m_OrientedBBox.lo.y;
			double maxX = m_OrientedBBox.hi.x;
			double maxY = m_OrientedBBox.hi.y;

			// vertical roads
			double currentX = minX - (maxX - minX) * 0.5;
			double width = RandomInRangeF(m_DbfIn.roadMinWidth, m_DbfIn.roadMaxWidth);
			m_VRoads.Add(Road(currentX, width));
	
			int counter = 0;
			while(currentX < maxX + (maxX - minX) * 0.5)
			{
				double width = RandomInRangeF(m_DbfIn.roadMinWidth, m_DbfIn.roadMaxWidth);
				double block = RandomInRangeF(m_DbfIn.blockMinSize, m_DbfIn.blockMaxSize);

				currentX += m_VRoads[counter].m_Width * 0.5 + block + width * 0.5;

//				if(currentX < maxX)
//				{
					m_VRoads.Add(Road(currentX, width));
					counter++;
//				}
			}

			// horizontal roads
			double currentY = minY - (maxY - minY) * 0.5;
			width = RandomInRangeF(m_DbfIn.roadMinWidth, m_DbfIn.roadMaxWidth);
			m_HRoads.Add(Road(currentY, width));

			counter = 0;
			while(currentY < maxY + (maxY - minY) * 0.5)
			{
				double width = RandomInRangeF(m_DbfIn.roadMinWidth, m_DbfIn.roadMaxWidth);
				double block = RandomInRangeF(m_DbfIn.blockMinSize, m_DbfIn.blockMaxSize);

				currentY += m_HRoads[counter].m_Width * 0.5 + block + width * 0.5;

//				if(currentY < maxY)
//				{
					m_HRoads.Add(Road(currentY, width));
					counter++;
//				}
			}
		}

		// --------------------------------------------------------------------------------

		void NYDummyBuildingPlacer::CalculateBlocks()
		{
			m_Blocks.Clear();

			int hRoadsCount = m_HRoads.Size();
			int vRoadsCount = m_VRoads.Size();

			for(int i = 0; i < hRoadsCount - 1; ++i)
			{
				for(int j = 0; j < vRoadsCount - 1; ++j)
				{
					double minX = m_VRoads[j].m_Coord + m_VRoads[j].m_Width * 0.5;
					double maxX = m_VRoads[j + 1].m_Coord - m_VRoads[j + 1].m_Width * 0.5;
					double minY = m_HRoads[i].m_Coord + m_HRoads[i].m_Width * 0.5;
					double maxY = m_HRoads[i + 1].m_Coord - m_HRoads[i + 1].m_Width * 0.5;
					Shapes::DVertex vLo(minX, minY);
					Shapes::DVertex vHi(maxX, maxY);

					Block block(vLo, vHi);

					m_Blocks.Add(block);
				}
			}
		}

		// --------------------------------------------------------------------------------

		void NYDummyBuildingPlacer::CreateObjects()
		{
			if(m_TempObjectsOut.Size() != 0) m_TempObjectsOut.Clear();
			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			int blocksCount = m_Blocks.Size();
			for(int i = 0; i < blocksCount; ++i)
			{
				Block block = m_Blocks[i];
				double blockWSize = block.hi.x - block.lo.x;
				double blockHSize = block.hi.y - block.lo.y;

				int cellsW = floor(RandomInRangeF(m_DbfIn.blockMinSubdivisions, m_DbfIn.blockMaxSubdivisions + 1));
				int cellsH = floor(RandomInRangeF(m_DbfIn.blockMinSubdivisions, m_DbfIn.blockMaxSubdivisions + 1));

				double cellWSize = blockWSize / cellsW;
				double cellHSize = blockHSize / cellsH;
					
				for(int j = 0; j < cellsW; ++j)
				{
					for(int k = 0; k < cellsH; ++k)
					{
						if(j == 0 || j == cellsW - 1 || k == 0 || k == cellsH - 1)
						{
							double midW = block.lo.x + (j + 0.5) * cellWSize;
							double midH = block.lo.y + (k + 0.5) * cellHSize;

							// new object
							ModuleObjectOutput out;

							Shapes::DVertex v(midW, midH);

							out.position = v;
							out.rotation = 0.0;
							out.scaleX = cellWSize;
							out.scaleY = cellHSize;
							out.scaleZ = RandomInRangeF(m_GlobalIn.buildingMinHeight, m_GlobalIn.buildingMaxHeight);

							// used only by VLB for preview
							out.length = 1.0;
							out.width  = 1.0;
							out.height = 1.0;

							// adds to array
							m_TempObjectsOut.Add(out);
						}
					}
				}
			}
		}

		// --------------------------------------------------------------------------------

		void NYDummyBuildingPlacer::RotateObjectsAboutShapeBarycenter()
		{
			Shapes::DVertex barycenter = m_pCurrentShape->GetBarycenter();
			double angle = m_DbfIn.mainDirection / 180.0 * EtMathD::PI;

			int objectsCount = m_TempObjectsOut.Size();
			for(int i = 0; i < objectsCount; ++i)
			{
				double dx = m_TempObjectsOut[i].position.x - barycenter.x;
				double dy = m_TempObjectsOut[i].position.y - barycenter.y;

				m_TempObjectsOut[i].position.x = barycenter.x + (dx * EtMathD::Cos(angle) - dy * EtMathD::Sin(angle));
				m_TempObjectsOut[i].position.y = barycenter.y + (dx * EtMathD::Sin(angle) + dy * EtMathD::Cos(angle));
				m_TempObjectsOut[i].rotation = angle;
			}
		}

		// --------------------------------------------------------------------------------

		void NYDummyBuildingPlacer::DeleteOutsideObjects()
		{
			int objectsCount = m_TempObjectsOut.Size();
			int counter = 0;
			while(counter < objectsCount)
			{
				ModuleObjectOutput out = m_TempObjectsOut[counter];
				double angle = out.rotation;
				double x0    = out.position.x;
				double y0    = out.position.y;

				double dimX = out.scaleX;
				double dimY = out.scaleY;

				Shapes::DVertex v[4];

				v[0].x = x0 + (-dimX * 0.5) * EtMathD::Cos(angle) - (dimY * 0.5)  * EtMathD::Sin(angle);
				v[0].y = y0 + (-dimX * 0.5) * EtMathD::Sin(angle) + (dimY * 0.5)  * EtMathD::Cos(angle);
				v[1].x = x0 + (-dimX * 0.5) * EtMathD::Cos(angle) - (-dimY * 0.5) * EtMathD::Sin(angle);
				v[1].y = y0 + (-dimX * 0.5) * EtMathD::Sin(angle) + (-dimY * 0.5) * EtMathD::Cos(angle);
				v[2].x = x0 + (dimX * 0.5)  * EtMathD::Cos(angle) - (-dimY * 0.5) * EtMathD::Sin(angle);
				v[2].y = y0 + (dimX * 0.5)  * EtMathD::Sin(angle) + (-dimY * 0.5) * EtMathD::Cos(angle);
				v[3].x = x0 + (dimX * 0.5)  * EtMathD::Cos(angle) - (dimY * 0.5)  * EtMathD::Sin(angle);
				v[3].y = y0 + (dimX * 0.5)  * EtMathD::Sin(angle) + (dimY * 0.5)  * EtMathD::Cos(angle);

				bool inside = true;
				for(int i = 0; i < 4; ++i)
				{
					if(!m_pCurrentShape->PtInside(v[i]))
					{
						inside = false;
						break;
					}
				}
				if(inside)
				{
					m_ObjectsOut.Add(out);
				}

				counter++;
			}
		}

		// --------------------------------------------------------------------------------

		void NYDummyBuildingPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			for(int i = 0; i < m_ObjectsOut.Size(); ++i)
			{
				char cNumber[50];

				int n = m_GlobalIn.modelVariants + 1;
				while (n > m_GlobalIn.modelVariants)
				{
					n = floor(RandomInRangeF(1, m_GlobalIn.modelVariants + 1));
				}

				sprintf_s(cNumber, "%d", n);
				RString name = m_GlobalIn.modelNameBase + cNumber;

				Vector3 position = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void NYDummyBuildingPlacer::WriteReport()
		{
			int objOutCount = m_ObjectsOut.Size();
			printf("Created %d new objects\n", objOutCount);
			printf("--------------------------------------\n");
		}
	}
}
