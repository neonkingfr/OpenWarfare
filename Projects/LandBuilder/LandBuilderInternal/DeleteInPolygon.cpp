#include "StdAfx.h"
#include ".\DeleteInPolygon.h"

#include "..\HighMapLoaders\include\EtHelperFunctions.h"
#include "..\HighMapLoaders\include\EtMath.h"
#include ".\ObjectContainer.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------//

		void DeleteInPolygon::Run( IMainCommands* pCmdIfc )
		{
			m_deletionCounter = 0;

			// gets shape and its properties
			m_pCurrentShape = pCmdIfc->GetActiveShape();    

			// delete the objects
			DeleteObjects( pCmdIfc );

			// writes the report
			WriteReport();
		}

		// --------------------------------------------------------------------//

		void DeleteInPolygon::DeleteObjects( IMainCommands* pCmdIfc )
		{
			int objCount = 0;
			int objID = -1;
			while ( pCmdIfc->GetObjectMap()->EnumObjects( objID ) )
			{
				objCount++;
				break;
			}

			if ( objCount > 0 )
			{
				int objID = -1;
				while ( pCmdIfc->GetObjectMap()->EnumObjects( objID ) )
				{
					const Matrix4& matr = pCmdIfc->GetObjectMap()->GetObjectLocation( objID );
					const Vector3 objPos = matr.Position();

					Shapes::DVector tempV( objPos.X(), objPos.Z(), objPos.Y() );
					if ( m_pCurrentShape->PtInside( tempV ) )
					{
						pCmdIfc->GetObjectMap()->DeleteObject( objID );
						m_deletionCounter++;
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		void DeleteInPolygon::WriteReport()
		{
			printf( "Deleted %d objects.     \n",  m_deletionCounter );
			printf( "--------------------------------------\n" );
		}
	}
}

//-----------------------------------------------------------------------------
