#pragma once

#include <string>
#include "IBuildModule.h"

#include "..\HighMapLoaders\include\EtRectElevationGrid.h"

using std::string;

namespace LandBuildInt
{
	namespace Modules
	{
		class HeightMapLoader : public IBuildModule
		{
		private:
			struct SourceInfo
			{
				string       demType;
				string       fileName;
				unsigned int srcLeft;
				unsigned int srcTop;
				unsigned int srcRight;
				unsigned int srcBottom;
				float        dstLeft;
				float        dstTop;
				float        dstRight;
				float        dstBottom;
				EtRectElevationGrid<double, int> hmData;
				ClassIsMovable(SourceInfo); 
			};

			class DemSampler : public IHeightMapSampler
			{
				AutoArray<SourceInfo>& m_Sources;

			public:
				DemSampler(AutoArray<SourceInfo> &sources) : m_Sources(sources)
				{
				}

				virtual float GetHeight(const HeightMapLocInfo& heightInfo) const
				{
					for(int i = 0; i < m_Sources.Size(); ++i)
					{
						// select the source owner of the heightInfo point
						// and get the height from it
						if((heightInfo.x >= m_Sources[i].dstLeft) &&
						   (heightInfo.x <= m_Sources[i].dstRight) && 
						   (heightInfo.y >= m_Sources[i].dstBottom) && 
						   (heightInfo.y <= m_Sources[i].dstTop))
						{
							float srcWLeft   = m_Sources[i].hmData.getWorldUAt(m_Sources[i].srcLeft);
							float srcWBottom = m_Sources[i].hmData.getWorldVAt(m_Sources[i].srcBottom);
							float srcWRight  = m_Sources[i].hmData.getWorldUAt(m_Sources[i].srcRight);
							float srcWTop    = m_Sources[i].hmData.getWorldVAt(m_Sources[i].srcTop);

							float srcSizeX = srcWRight - srcWLeft;
							float srcSizeY = srcWTop - srcWBottom;
							float dstSizeX = m_Sources[i].dstRight - m_Sources[i].dstLeft;
							float dstSizeY = m_Sources[i].dstTop - m_Sources[i].dstBottom;
							
							float newX = srcWLeft + (heightInfo.x - m_Sources[i].dstLeft)* srcSizeX / dstSizeX;
							float newY = srcWBottom + (heightInfo.y - m_Sources[i].dstBottom) * srcSizeY / dstSizeY;

							return m_Sources[i].hmData.getWorldWAt(newX, newY, EtRectElevationGrid<double, int>::imBILINEAR);
						}
					}
					// if we arrive here something went wrong
					return 0;
				}
			};

		public:
			DECLAREMODULEEXCEPTION("HeightMapLoader");

			virtual void Run(IMainCommands *cmdIfc);

		private:
			string sDTED2;
			string sUSGSDEM;
			string sARCINFOASCII;
			string sXYZ;

		private:
			AutoArray<SourceInfo> m_Sources;

			void GetSourcesParameters(IMainCommands *cmdIfc);
			bool LoadSourcesHighMaps();
		};
	}
}