// LandBuilderInternal.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ParseProject.h"
#include <Windows.h>
#include "LBShapes.h"
#include "ParseProject.h"
#include <El/Multithread/ExceptionRegister.h>
#include "SimpleProgressBar.h"
#include "ModuleRegistrator.h"
#include "PluginManager.h"
#include <set>

// -------------------------------------------------------------------------- //

struct Arguments
{
	RString inputFile;
	RString outputFile;
	int level;
	std::set<RStringI> paths;
};

// -------------------------------------------------------------------------- //

class ShowModuleDesc
{
public:
	bool operator()(const RStringI& name, const LandBuilder2::ICreateModule* ifc = 0) const
	{
		printf("%s\n", name.Data());
		return false;
	}
};

// -------------------------------------------------------------------------- //

void ShowHelp()
{
	LandBuilder2::PluginManager manag;
	manag.LoadList();
	manag.MakeScanning();
	puts("\n"
		 "\n"
		 "LandBuilder2\n"
		 "Copyright 2007 Bohemia Interactive. All rights reserved.\n"
		 "--------------------------------------------------------\n"
		 "\n"
		 "\n"
		 "Usage: LandBuilder2 [switches] <project pathname>\n"
		 "\n"
		 "Switches:\n"
		 "-o <name>     Specifies output name. By default, output\n"
		 "              name is given from input name with different extension\n"
		 "\n"
		 "-i <path>     Installs modules from given path\n"
		 "              Use this switch to enforce LB2 rescan given folder\n"
		 "              Switch can repeat on command line. When path is added\n"
		 "              it will be rescaned anytime when it is needed, till\n"
		 "              there is at least one DLL inside\n"
		 "\n"
		 "-l <level>    defines loging level, 0 - 89 [default 10]\n");

	puts("Build-in modules:");
	LandBuilder2::ModuleRegistrator* modreg = LandBuilder2::ModuleRegistrator::GetInstance();
	modreg->ForEachModule(ShowModuleDesc());
	puts("");
	puts("External modules:");
	manag.EnumModules(ShowModuleDesc());
	puts("");
}

// -------------------------------------------------------------------------- //

bool ParseArguments(int argc, char** argv, Arguments& result)
{
	argc--;
	argv++;
	while (argc)
	{
		argc--;
		if ((*argv)[0] == '-')
		{
			bool extraParam = false;
			const char* extra = argc ? (argv[1]) : 0;
			switch ((*argv)[1])
			{
				case 'l':
					if (extra == 0) return false;
					extraParam = true;
					result.level = atoi(extra);
					break;
				case 'o':
					if (extra == 0) return false;
					extraParam = true;
					result.outputFile = extra;
					break;
				case 'h':
					ShowHelp();
					exit(0);
					break;
				case 'i':
					if (extra == 0) return false;
					extraParam = true;
					Pathname pp;
					pp.SetDirectory(extra);
					result.paths.insert(pp.GetDirectoryWithDrive());
					break;
			}
			if (extraParam) 
			{
				argc--;
				argv++;
			}
		}
		else
		{
			result.inputFile = argv[0];
		}
		argv++;
	}
	return true;
}

// -------------------------------------------------------------------------- //

class LBException : public ExceptReg::ExceptionRegister
{
public:
	virtual int Catch(const ExceptReg::IException& exception)
	{
		using namespace ExceptReg;
		char paramStr[1024];
		va_list parms = ExceptionRegister::CreateListOfParams(exception, alloca(exception.GetParamCount() * sizeof(double)));
		_vsntprintf(paramStr, sizeof(paramStr) / sizeof(_TCHAR), exception.GetDesc(), parms);
		paramStr[sizeof(paramStr) / sizeof(_TCHAR) - 1] = 0;
		LOGF(Error) << exception.GetModule() << ":" << exception.GetCode() << " " << paramStr;
		return ExceptReg::ExceptionRegister::fbAbort;
	}
};

// -------------------------------------------------------------------------- //

int main(int argc, char* argv[])
{
	using namespace LandBuildInt;
    using namespace LandBuilder2;

    SimpleProgress simpleProgress;
    simpleProgress.SelectGlobalProgressBarHandler(&simpleProgress);

    LBException exp;

	if (argc < 2)
	{
        ShowHelp();
		return -1;
	}

	puts("\n"
		 "\n"
		 "LandBuilder2\n"
		 "Copyright 2007 Bohemia Interactive. All rights reserved.\n"
		 "--------------------------------------------------------\n"
		 "\n"
		 "\n");

	Arguments objargs;
    objargs.level = ProgressLog::GetLevel();
    if (!ParseArguments(argc, argv, objargs))
    {
		LOGF(Fatal) << "Invalid parameters. See help (-h)";
		return -1;
    }

    if (objargs.inputFile.GetLength() == 0)
    {
        if (objargs.paths.size() != 0)
        {
	        ParseProject procs;
            procs.AddModulePaths(objargs.paths);
        }
        else
		{
		    LOGF(Fatal) << "Invalid parameters. See help (-h)";
		}
		return -1;
    }

    ProgressLog::SetLevel(objargs.level);
    LOGF(Note) << "Log level Level changed to " << ProgressLog::GetLevel();

	ParseProject procs;
    procs.AddModulePaths(objargs.paths);

//	SimpleProgress progressBar;
//	ProgressBarFunctions::SelectGlobalProgressBarHandler(&progressBar);

    if (procs.LoadProject(objargs.inputFile) == false)
	{
		return -2;
	}

	Pathname resultName;
    if (objargs.outputFile.GetLength()) 
	{
		resultName = objargs.outputFile;
	}
	else
	{
		resultName = objargs.inputFile;
		if (_stricmp(resultName.GetExtension(), ".lbt") == 0)
		{
			resultName.SetExtension(".ltx");
		}
		else
		{
			resultName.SetExtension(".lbt");
		}
	}

    procs.RunProject();
	procs.SaveResult(resultName);
}

// -------------------------------------------------------------------------- //
