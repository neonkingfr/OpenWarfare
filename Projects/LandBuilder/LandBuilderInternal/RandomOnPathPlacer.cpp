#include "StdAfx.h"
#include ".\RandomOnPathPlacer.h"

#include "..\Shapes\IShape.h"
#include "..\Shapes\ShapeHelpers.h"

#include "..\HighMapLoaders\include\EtMath.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

// -------------------------------------------------------------------------- //

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------//

		void RandomOnPathPlacer::Run(IMainCommands* cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();      

			// gets shape geometric data
			if(GetShapeGeoData())
			{
				// gets global parameters from calling cfg file
				if(GetGlobalParameters(cmdIfc))
				{
					// gets dbf parameters from calling cfg file
					if(GetDbfParameters(cmdIfc))
					{
						// gets objects parameters from calling cfg file
						if(GetObjectsParameters(cmdIfc))
						{
							// normalizes probabilities
							if(NormalizeProbs())
							{
								// initializes random seed
								srand(m_GlobalIn.randomSeed);

								// calculates the number of individual to be generated
								m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.perimeter * m_DbfIn.linearDensity / 100.0);

								// sets the population
								CreateObjects(false);

								// export objects
								ExportCreatedObjects(cmdIfc);

								// writes the report
								WriteReport();
							}
						}
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* RandomOnPathPlacer::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn,
																  AutoArray<RandomOnPathPlacer::ObjectInput, MemAllocStack<RandomOnPathPlacer::ObjectInput, 32> >& objectsIn)
		{
			m_pCurrentShape = shape;
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;
			m_ObjectsIn     = objectsIn;

			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// gets shape geometric data
			if(GetShapeGeoData())
			{
				// normalizes probabilities
				if(NormalizeProbs())
				{
					// initializes random seed
					srand(m_GlobalIn.randomSeed);

					// calculates the number of individual to be generated
					m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.perimeter * m_DbfIn.linearDensity / 100.0);

					// sets the population
					CreateObjects(true);

					return &m_ObjectsOut;
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return NULL;
			}
		}

		// --------------------------------------------------------------------//

		bool RandomOnPathPlacer::GetShapeGeoData()
		{
			m_ShpGeo.perimeter = GetShapePerimeter(m_pCurrentShape);
			return true;
		}

		// --------------------------------------------------------------------//

		bool RandomOnPathPlacer::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			m_GlobalIn.maxDistance = 10.0; // default value
			const char* x = cmdIfc->QueryValue("maxDistance", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.maxDistance = atof(x);
				}
				else
				{
					printf(">>> --------------------------------------------------- <<<\n");
					printf(">>> Bad data for maxDistance - using default value (10) <<<\n");
					printf(">>> --------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> -------------------------------------------------------- <<<\n");
				printf(">>> Missing value for maxDistance - using default value (10) <<<\n");
				printf(">>> -------------------------------------------------------- <<<\n");
			}
			x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing value for randomSeed <<<"));
				return false;
			}
			return true;
		};

		// --------------------------------------------------------------------//

		bool RandomOnPathPlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			// gets parameters from calling cfg file
			m_DbfIn.linearDensity = 10.0; // default value
			const char* x = cmdIfc->QueryValue("linearDensity", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.linearDensity = atof(x);
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Bad data for linearDensity - using default value (10) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ---------------------------------------------------------- <<<\n");
				printf(">>> Missing value for linearDensity - using default value (10) <<<\n");
				printf(">>> ---------------------------------------------------------- <<<\n");
			}
			if (m_DbfIn.linearDensity < 0.01)
			{
				ExceptReg::RegExcpt(Exception(21, ">>> Density is too low (less then 0.01) <<<"));
				return false;
			}
			return true;
		};

		// --------------------------------------------------------------------//

		bool RandomOnPathPlacer::GetObjectsParameters(IMainCommands* cmdIfc)
		{
			// resets arrays
			if(m_ObjectsIn.Size() != 0)
			{
				m_ObjectsIn.Clear();
				m_ObjectsOut.Clear();
			}

			// gets objects' data from calling cfg file
			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], prob[50];
				sprintf_s(object   , "object%d"   , counter);
				sprintf_s(minheight, "minheight%d", counter);
				sprintf_s(maxheight, "maxheight%d", counter);
				sprintf_s(prob     , "prob%d"     , counter);

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue(object, false);
				if (x == 0) break;
				nfo.m_Name = x;

				// object min height
				x = cmdIfc->QueryValue(minheight, false);
				nfo.m_MinHeight = 100.0; // default value
				if (x != 0) 
				{
					if(IsFloatingPoint(string(x), false))
					{
						nfo.m_MinHeight = atof(x);
					}
					else
					{
						printf(">>> ------------------------------------------------------- <<<\n");
						printf(">>> Missing value for minheight - using default value (100) <<<\n");
						printf(">>> ------------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ------------------------------------------------------- <<<\n");
					printf(">>> Missing value for minheight - using default value (100) <<<\n");
					printf(">>> ------------------------------------------------------- <<<\n");
				}

				// object max height
				x = cmdIfc->QueryValue(maxheight, false);
				nfo.m_MaxHeight = 100.0; // default value
				if (x != 0) 
				{
					if(IsFloatingPoint(string(x), false))
					{
						nfo.m_MaxHeight = atof(x);
					}
					else
					{
						printf(">>> ------------------------------------------------------- <<<\n");
						printf(">>> Missing value for maxheight - using default value (100) <<<\n");
						printf(">>> ------------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ------------------------------------------------------- <<<\n");
					printf(">>> Missing value for maxheight - using default value (100) <<<\n");
					printf(">>> ------------------------------------------------------- <<<\n");
				}

				// object probability
				x = cmdIfc->QueryValue(prob, false);
				nfo.m_Prob = 100.0; // default value
				if (x != 0) 
				{
					if(IsFloatingPoint(string(x), false))
					{
						nfo.m_Prob = atof(x);
					}
					else
					{
						printf(">>> --------------------------------------------- <<<\n");
						printf(">>> Bad data for prob - using default value (100) <<<\n");
						printf(">>> --------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> -------------------------------------------------- <<<\n");
					printf(">>> Missing value for prob - using default value (100) <<<\n");
					printf(">>> -------------------------------------------------- <<<\n");
				}

				// object counter
				nfo.m_Counter = 0;

				m_ObjectsIn.Add(nfo);
				counter++;
			} 
			while(true);   
			return true;
		}

		// --------------------------------------------------------------------//

		bool RandomOnPathPlacer::NormalizeProbs()
		{
			double total = 0.0;

			for(int i = 0; i < m_ObjectsIn.Size(); ++i)
			{
				total += m_ObjectsIn[i].m_Prob;
			}

			if(total == 0.0) 
			{
				ExceptReg::RegExcpt(Exception(31, ">>> Total probability equal to zero <<<"));
				return false;
			}

			double invTotal = 1 / total;
			for(int i = 0; i < m_ObjectsIn.Size(); ++i)
			{
				m_ObjectsIn[i].m_NormProb = m_ObjectsIn[i].m_Prob * invTotal;
			}
			return true;
		}

		// --------------------------------------------------------------------//

		int RandomOnPathPlacer::GetRandomType()
		{
			double p = static_cast<double>(rand()) / (static_cast<double>(RAND_MAX) + 1);

			bool found = false;
			int  index = 0;
			double curProb = m_ObjectsIn[index].m_NormProb;
			while(!found)
			{
				if (p < curProb)
				{
					found = true;
				}
				else
				{
					index++;
					curProb += m_ObjectsIn[index].m_NormProb;
				}
			}
			return index;
		}

		// --------------------------------------------------------------------//

		void RandomOnPathPlacer::CreateObjects(bool preview)
		{
			ProgressBar<int> pb(m_NumberOfIndividuals);
			
			for(int i = 0; i < m_NumberOfIndividuals; i++)
			{
				if(!preview) pb.AdvanceNext(1);

				// random type
				int type = GetRandomType();

				// random coordinate on perimeter
				double s = RandomInRangeF(0, m_ShpGeo.perimeter);

				// convert to XY
				Shapes::DVertex v = ConvertToXY(s);

				// random polar coordinate from point on perimeter
				double dist  = RandomInRangeF(0, m_GlobalIn.maxDistance);
				double angle = RandomInRangeF(0, EtMathD::TWO_PI);
				double x0 = v.x + dist * cos(angle);
				double y0 = v.y + dist * sin(angle);

				// new object
				ModuleObjectOutput out;

				// sets objects out data
				// type
				out.type     = type;
				// position
				out.position = Shapes::DVertex(x0, y0);
				// rotation
				out.rotation = RandomInRangeF(0, EtMathD::TWO_PI);
				// scale
				double minScale = m_ObjectsIn[type].m_MinHeight;
				double maxScale = m_ObjectsIn[type].m_MaxHeight;
				double scale = RandomInRangeF(minScale, maxScale);
				out.scaleX = out.scaleY = out.scaleZ = scale / 100.0;

				// used only by VLB for preview
				out.length = 10.0;
				out.width  = 10.0;
				out.height = 10.0;

				// adds to array
				m_ObjectsOut.Add(out);

				// update type counter
				m_ObjectsIn[type].m_Counter++;
			}
		}

		// --------------------------------------------------------------------//

		Shapes::DVertex RandomOnPathPlacer::ConvertToXY(double s)
		{
			Shapes::DVertex v1;
			Shapes::DVertex v2;
			double dist;

			int vCount = m_pCurrentShape->GetVertexCount();

			int index = -1;
			double tempPos = 0.0;

			for (int i = 0; i < (vCount - 1); i++)
			{
				v1 = m_pCurrentShape->GetVertex(i);
				v2 = m_pCurrentShape->GetVertex(i + 1);
				dist = v1.DistanceXY(v2);
				tempPos += dist;
				if (s < tempPos)
				{
					index = i;
					break;
				}
			}

			if (index == -1)
			{
				v1 = m_pCurrentShape->GetVertex(vCount - 1);
				v2 = m_pCurrentShape->GetVertex(0);
				dist = v1.DistanceXY(v2);
				tempPos += dist;
			}
			else
			{
				v1 = m_pCurrentShape->GetVertex(index);
				v2 = m_pCurrentShape->GetVertex(index + 1);
			}

			dist = v1.DistanceXY(v2);			
			Shapes::DVector diff = v2 - v1;

			double localS = tempPos - s;
			double delta = 0.0;

			if (dist != 0.0)
			{
				delta = localS / dist;
			}

			double newX = v1.x + diff.x * delta;
			double newY = v1.y + diff.y * delta;

			return Shapes::DVertex(newX, newY); 
		}

		// --------------------------------------------------------------------//

		void RandomOnPathPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for(int i = 0; i < objOutCount; ++i)
			{
				RString name      = m_ObjectsIn[m_ObjectsOut[i].type].m_Name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void RandomOnPathPlacer::WriteReport()
		{
			printf("Created                  \n");
			for(int i = 0; i < m_ObjectsIn.Size(); i++)
			{
				printf("%d", m_ObjectsIn[i].m_Counter);
				printf(" : ");
				printf(m_ObjectsIn[i].m_Name);
				printf("                           \n");
			}
			printf("--------------------------------------\n");
		}
	}
}