#pragma once

#include "IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

#include "..\Shapes\ShapeHelpers.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class Road
		{
		public:
			double m_Coord;
			double m_Width;

			Road() 
			{
			}

			Road(double coord, double width) 
			: m_Coord(coord)
			, m_Width(width) 
			{
			}

			ClassIsMovable(Road);
		};

		class Block : public Shapes::DBox
		{
		public:
			Block() 
			: Shapes::DBox() 
			{
			}

			Block(const Shapes::DVertex lo, const Shapes::DVertex hi) 
			: Shapes::DBox(lo, hi) 
			{
			}

			ClassIsMovable(Block);
		};

		class NYDummyBuildingPlacer : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				unsigned int randomSeed;
				double buildingMinHeight;
				double buildingMaxHeight;
				RString modelNameBase;
				int modelVariants;
			};

			struct DbfInput
			{
				double mainDirection;
				double roadMinWidth;
				double roadMaxWidth;
				double blockMinSize;
				double blockMaxSize;
				int    blockMinSubdivisions;
				int    blockMaxSubdivisions;
			};


		public:
			NYDummyBuildingPlacer();

			virtual void Run(IMainCommands* cmdIfc);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn);

			class Exception : public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) 
				: code(code)
				, text(text) 
				{
				}

				virtual unsigned int GetCode() const 
				{ 
					return code; 
				}

				virtual const _TCHAR* GetDesc() const 
				{ 
					return text; 
				}

				virtual const _TCHAR* GetModule() const 
				{ 
					return "NYDummyBuildingPlacer"; 
				}

				virtual const _TCHAR* GetType() const 
				{ 
					return "NYDummyBuildingPlacerException"; 
				}
			};

		private:
			Shapes::IShape* m_pCurrentShape; 
			GlobalInput     m_GlobalIn;
			DbfInput        m_DbfIn;

			Shapes::DBox m_OrientedBBox;
			AutoArray<Road, MemAllocStack<Road, 32> > m_HRoads;
			AutoArray<Road, MemAllocStack<Road, 32> > m_VRoads;
			AutoArray<Block, MemAllocStack<Block, 32> > m_Blocks;

			ModuleObjectOutputArray m_ObjectsOut;
			ModuleObjectOutputArray m_TempObjectsOut;

			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			void CalculateOrientedBBox();
			void CalculateRoadNetwork();
			void CalculateBlocks();
			void CreateObjects();
			void RotateObjectsAboutShapeBarycenter();
			void DeleteOutsideObjects();
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();
		};
	}
}