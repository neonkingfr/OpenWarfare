#include "StdAfx.h"
#include ".\countourlines.h"
namespace LandBuildInt
{

  using namespace Shapes;
  void CountourLines ::AddShape(const ShapePolygon &shape, float height, int segment)
  {
    if (segment==-1)
    {
      _heights.Add(height)  ;
      _clines.Add(static_cast<ShapePolygon *>(shape.Copy(&vxArr)));
    }
    else
    {
      if (segment<0 || segment>=(signed)shape.GetPartCount()) return;

      class RetrieveData: public ShapePolygon
      {
      public:
        unsigned int *GetVertices(int part) const
        {
          return const_cast<unsigned int *>(_points.Data()+GetPartIndex(part));
        }
      };

      Array<unsigned int> vex(static_cast<const RetrieveData &>(shape).GetVertices(segment),shape.GetPartSize(segment));
      const VertexArray *src=shape.GetVertexArray();
      for (int i=0;i<vex.Size();i++) vex[i]=vxArr.AddVertex(src->GetVertex(vex[i]));
      unsigned int zr=0;
      _clines.Add(new ShapePolygon(vex,Array<unsigned int>(&zr,0),&vxArr));
    }
  }

  float CountourLines ::GetHeight(const HeightMapLocInfo &heightInfo) const
  {
    double bestInsideDist=FLT_MAX;
    double bestOutsideDist=FLT_MAX;
    int bestInsideId=-1;
    int bestOutsideId=-1;

    DVector vec(heightInfo.x,heightInfo.y);

    for (int i=0;i<_clines.Size();i++)
    {
      const ShapePolygon *p=_clines[i];
      bool inside=p->PtInside(vec);
      DVector nr=p->NearestToEdge(vec,true);
      double d=nr.DistanceXY2(vec);
      if (inside)
      {
        if (d<bestInsideDist)
        {
          bestInsideDist=d;
          bestInsideId=i;
        }
      }
      else
      {
        if (d<bestOutsideDist)
        {
          bestOutsideDist=d;
          bestOutsideId=i;
        }
      }
    }
    float finHeight;
    if (bestInsideId==-1)
    {
      if (bestOutsideId==-1)
        return heightInfo.alt_m;
      finHeight=_heights[bestOutsideId];
    }
    else if (bestOutsideId==-1)
    {
      finHeight=_heights[bestInsideId];
    }
    else
    {
      double d=sqrt(bestOutsideDist)+sqrt(bestInsideDist);
      double f=sqrt(bestInsideDist)/d;
      double h1=_heights[bestInsideId];
      double h2=_heights[bestOutsideId];
      finHeight=(float)((1-f)*h1+f*h2);
    }
    return CombineHeights(heightInfo.alt_m,finHeight);
  }

  void CountourLines::AddShapeAuto(const Shapes::ShapePolygon &shape, float baseHeight, float step, int segment)
  {
    if (segment==-1) for (unsigned int i=0;i<shape.GetPartCount();i++) AddShapeAuto(shape,baseHeight, step,i);
    else
    {
      const DVertex &vx=shape.GetVertex(shape.GetPartIndex(segment));
      float optimalHeight=baseHeight;
      for (int i=_clines.Size()-1;i>=0;i++)
      {
        if (_heights[i]>optimalHeight && _clines[i]->PtInside(vx)) optimalHeight=_heights[i];        
      }
      optimalHeight+=step;
      AddShape(shape,optimalHeight,segment);
    }
  }

  namespace Modules
  {

    void CountourLines::Run(IMainCommands *cmds)
    {
      float baseHeight;
      float step;
      const char *tmp;
      int mode;
      float refHeight;

      if ((tmp = cmds->QueryValue("baseHeight")) == 0 || 
        sscanf(tmp, "%f", &baseHeight) != 1 )
      {
        ExceptReg::RegExcpt(Exception(1, "Missing baseHeight parameter"));
        return;
      }

      if ((tmp = cmds->QueryValue("step")) == 0 || 
        sscanf(tmp, "%f", &step) != 1 )
      {
        ExceptReg::RegExcpt(Exception(2, "Missing step parameter"));
        return;
      }

      tmp=cmds->QueryValue("mode");
      if (tmp==0)
      {
        ExceptReg::RegExcpt(Exception(3, "Missing mode parameter"));
        return;
      }

      if (stricmp(tmp,"absolute")==0) mode=0;
      else if (stricmp(tmp,"relative")==0) mode=1;
      else if (stricmp(tmp,"weight")==0) mode=2;
      else if (stricmp(tmp,"weightrelative")==0) mode=3;

      if (mode>=2)
      {
        if ((tmp = cmds->QueryValue("refHeight")) == 0 || sscanf(tmp, "%f", &refHeight) != 1 )
        {
          ExceptReg::RegExcpt(Exception(4, "Need refHeight parameter for given mode"));
          return;
        }
      }
      else
      {
        ExceptReg::RegExcpt(Exception(5, "Unknown mode"));
        return ;
      }

      const IShape *shp=cmds->GetActiveShape();
      DBox bx=shp->CalcBoundingBox();  
      LandBuildInt::CountourLines *ls;
      switch (mode)
      {
      case 0: ls=new LandBuildInt::CountourLines;break;
      case 1: ls=new LandBuildInt::CountourLinesRelative;break;
      case 2: ls=new LandBuildInt::CountourLinesWeight(refHeight);break;
      case 3: ls=new LandBuildInt::CountourLinesWeightRelative(refHeight);break;
      default: return;
      }

      cmds->GetHeightMap()->AddSamplerModule(ls,1,&FloatRect((float)bx.lo.x,(float)bx.lo.y,(float)bx.hi.x,(float)bx.hi.y));
    }
  }
}
