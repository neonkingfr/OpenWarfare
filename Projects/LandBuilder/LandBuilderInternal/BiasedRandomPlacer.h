#pragma once

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>
#include "..\Shapes\ShapeHelpers.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class BiasedRandomPlacer : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				bool         smallAtBoundary;
				unsigned int randomSeed;
			};

			struct DbfInput
			{
				double hectareDensity;
				double clusterCoeff;
				int    parentCount;
			};

			struct ObjectInput
			{
				RString m_Name;
				double  m_MinHeight;
				double  m_MaxHeight;
				double  m_Prob;
				int     m_Counter;
				double  m_MinDistance;
				double  m_MaxDistance;
				double  m_NormProb;

				ObjectInput() 
				{
				}

				ObjectInput(RString name, double minHeight, double maxHeight, double prob, double minDistance)
				: m_Name(name)
				, m_MinHeight(minHeight)
				, m_MaxHeight(maxHeight)
				, m_Prob(prob)
				, m_Counter(0)
				, m_MinDistance(minDistance)
				{
				}

				ClassIsMovable(ObjectInput); 
			};

		private:
			struct ShapeGeo
			{
				double minX;
				double maxX;
				double minY;
				double maxY;
				double widthBB;        // bounding box width
				double heightBB;       // bounding box height
				double area;           // area of the shape (polygon)
				double areaHectares;   // area of the shape in hectares
				double areaBB;         // bounding box area
				double areaBBHectares; // bounding box area in hectares
			};

			struct ObjectOutputExtra
			{
				ModuleObjectOutput data;
				bool               atBoundary;
				int	               childIndex;
				double             minDistance;
				double             maxDistance;
				ClassIsMovable(ObjectOutputExtra); 
			};

		public:
			virtual void Run(IMainCommands*);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn,
												  AutoArray<BiasedRandomPlacer::ObjectInput, MemAllocStack<BiasedRandomPlacer::ObjectInput, 32> >& objectsIn);

			class Exception : public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code,const char* text) : code(code), text(text) {}
				virtual unsigned int GetCode() const { return code; }
				virtual const _TCHAR* GetDesc() const { return text; }
				virtual const _TCHAR* GetModule() const { return "BiasedRandomPlacer"; }
				virtual const _TCHAR* GetType() const { return "BiasedRandomPlacerException"; }
			};
		private:
			AutoArray<ObjectInput, MemAllocStack<ObjectInput, 32> >             m_ObjectsIn;
			AutoArray<ObjectOutputExtra, MemAllocStack<ObjectOutputExtra, 32> > m_ObjectsOutExtra;

			ModuleObjectOutputArray m_ObjectsOut;

			GlobalInput           m_GlobalIn;
			DbfInput              m_DbfIn;
			const Shapes::IShape* m_pCurrentShape;      
			ShapeGeo              m_ShpGeo;
			int                   m_NumberOfIndividuals;

			bool GetShapeGeoData();
			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetObjectsParameters(IMainCommands* cmdIfc);

			bool NormalizeProbs();
			void SetParents();
			void CreateObjects(bool preview);
			void AddObjectOutExtra(const Shapes::DVector& v, int type, ObjectOutputExtra* parent);
			int  GetRandomType();
			bool PtCloseToObject(const Shapes::DVector& v) const;
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();

			ObjectOutputExtra* GetRandomParent(int type);
		};
	}
}