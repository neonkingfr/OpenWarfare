#include "StdAfx.h"
#include ".\modulefactory.h"

#include "AdvancedOnPlacePlacer.h"
#include "AdvancedRandomPlacer.h"
#include "AdvancedRandomPlacerSlope.h"
#include "BestFitPlacer.h"
#include "BiasedRandomPlacer.h"
#include "BiasedRandomPlacerSlope.h"
#include "BoundarySubSquaresFramePlacer.h"
#include "BuildingCreator.h"
#include "DeleteAlongPath.h"
#include "DeleteInPolygon.h"
#include "DummyBuildingPlacer.h"
#include "GeoProjection.h"
#include "HighMapLoader.h"
#include "HighMapTest.h"
#include "FramedForestPlacer.h"
#include "InnerSubSquaresFramePlacer.h"
#include "NoneModule.h"
#include "OnPlacePlacer.h"
#include "NYDummyBuildingPlacer.h"
#include "RandomOnPathPlacer.h"
#include "RegularOnPathPlacer.h"
#include "RoadPlacer.h"
#include "ScriptModule.h"
#include "SimpleRandomPlacer.h"
#include "SimpleRandomPlacerSlope.h"
#include "SmoothTerrainAlongPath.h"
#include "SubSquaresRandomPlacer.h"
#include "Transform2D.h"

#include "CountourLines.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{    
    IBuildModule* ModuleFactory::GetModule(const char* name)
	{
		printf("Initializing module: %s\n", name);
		// empty module
		if (_stricmp(name, "none") == 0)                          return new Modules::NoneModule();
		// placers
		if (_stricmp(name, "SimpleRandomPlacer") == 0)            return new Modules::SimpleRandomPlacer();
		if (_stricmp(name, "SimpleRandomPlacerSlope") == 0)       return new Modules::SimpleRandomPlacerSlope();
		if (_stricmp(name, "AdvancedRandomPlacer") == 0)          return new Modules::AdvancedRandomPlacer();
		if (_stricmp(name, "AdvancedRandomPlacerSlope") == 0)     return new Modules::AdvancedRandomPlacerSlope();
		if (_stricmp(name, "RoadPlacer") == 0)                    return new Modules::RoadPlacer();
		if (_stricmp(name, "BiasedRandomPlacer") == 0)            return new Modules::BiasedRandomPlacer();
		if (_stricmp(name, "BiasedRandomPlacerSlope") == 0)       return new Modules::BiasedRandomPlacerSlope();
		if (_stricmp(name, "RandomOnPathPlacer") == 0)            return new Modules::RandomOnPathPlacer();
		if (_stricmp(name, "RegularOnPathPlacer") == 0)           return new Modules::RegularOnPathPlacer();
		if (_stricmp(name, "AdvancedOnPlacePlacer") == 0)         return new Modules::AdvancedOnPlacePlacer();
		if (_stricmp(name, "OnPlacePlacer") == 0)                 return new Modules::OnPlacePlacer();
		if (_stricmp(name, "DummyBuildingPlacer") == 0)           return new Modules::DummyBuildingPlacer();
		if (_stricmp(name, "NYDummyBuildingPlacer") == 0)         return new Modules::NYDummyBuildingPlacer();
		if (_stricmp(name, "SubSquaresRandomPlacer") == 0)        return new Modules::SubSquaresRandomPlacer();
		if (_stricmp(name, "InnerSubSquaresFramePlacer") == 0)    return new Modules::InnerSubSquaresFramePlacer();
		if (_stricmp(name, "BoundarySubSquaresFramePlacer") == 0) return new Modules::BoundarySubSquaresFramePlacer();
		if (_stricmp(name, "FramedForestPlacer") == 0)            return new Modules::FramedForestPlacer();
		if (_stricmp(name, "BestFitPlacer") == 0)                 return new Modules::BestFitPlacer();
		if (_stricmp(name, "BuildingCreator") == 0)               return new Modules::BuildingCreator();
		// deleters
		if (_stricmp(name, "DeleteAlongPath") == 0)               return new Modules::DeleteAlongPath();
		if (_stricmp(name, "DeleteInPolygon") == 0)               return new Modules::DeleteInPolygon();
		// terrain
		if (_stricmp(name, "CountourLines") == 0)                 return new Modules::CountourLines();
		if (_stricmp(name, "HighMapTest") == 0)                   return new Modules::HeightMapTest();
		if (_stricmp(name, "HighMapLoader") == 0)                 return new Modules::HeightMapLoader();
		if (_stricmp(name, "SmoothTerrainAlongPath") == 0)        return new Modules::SmoothTerrainAlongPath();
		// transformation
		if (_stricmp(name, "GeoProjection") == 0)                 return new Modules::GeoProjection();
		if (_stricmp(name, "Transform2D") == 0)                   return new Modules::Transform2D();
		// scripts
		if (_strnicmp(name, "Script:", 7) == 0)
		{
			Modules::ScriptModule* pMod = new Modules::ScriptModule();
			if ( pMod->LoadScript( name + 7 ) )
			{
				return (pMod);
			}
			else
			{
				delete pMod;        
				return (0);
			}
		}

		return (0);
	}
}

//-----------------------------------------------------------------------------
