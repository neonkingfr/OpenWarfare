#include "StdAfx.h"
#include ".\dummybuildingplacer.h"

#include "..\HighMapLoaders\include\EtHelperFunctions.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------------------

		DummyBuildingPlacer::DummyBuildingPlacer()
		{
			// defining the possible L-Shape configuration in string form
			m_StrLShapeConfiguration[0]  = "RLLLLL";
			m_StrLShapeConfiguration[1]  = "LRLLLL";
			m_StrLShapeConfiguration[2]  = "LLRLLL";
			m_StrLShapeConfiguration[3]  = "LLLRLL";
			m_StrLShapeConfiguration[4]  = "LLLLRL";
			m_StrLShapeConfiguration[5]  = "LLLLLR";
			m_StrLShapeConfiguration[6]  = "LRRRRR";
			m_StrLShapeConfiguration[7]  = "RLRRRR";
			m_StrLShapeConfiguration[8]  = "RRLRRR";
			m_StrLShapeConfiguration[9]  = "RRRLRR";
			m_StrLShapeConfiguration[10] = "RRRRLR";
			m_StrLShapeConfiguration[11] = "RRRRRL";

			// array defining the sign of the direction of odd edges for every possible 
			// L-Shape configuration 
			m_LShapeOddEdgeSigns[0]  = "++-";
			m_LShapeOddEdgeSigns[1]  = "++-";
			m_LShapeOddEdgeSigns[2]  = "+--";
			m_LShapeOddEdgeSigns[3]  = "+--";
			m_LShapeOddEdgeSigns[4]  = "+-+";
			m_LShapeOddEdgeSigns[5]  = "+-+";
			m_LShapeOddEdgeSigns[6]  = "++-";
			m_LShapeOddEdgeSigns[7]  = "++-";
			m_LShapeOddEdgeSigns[8]  = "+--";
			m_LShapeOddEdgeSigns[9]  = "+--";
			m_LShapeOddEdgeSigns[10] = "+-+";
			m_LShapeOddEdgeSigns[11] = "+-+";

			// array defining the sign of the direction of even edges for every possible 
			// L-Shape configuration 
			m_LShapeEvenEdgeSigns[0]  = "+-+";
			m_LShapeEvenEdgeSigns[1]  = "++-";
			m_LShapeEvenEdgeSigns[2]  = "++-";
			m_LShapeEvenEdgeSigns[3]  = "+--";
			m_LShapeEvenEdgeSigns[4]  = "+--";
			m_LShapeEvenEdgeSigns[5]  = "+-+";
			m_LShapeEvenEdgeSigns[6]  = "+-+";
			m_LShapeEvenEdgeSigns[7]  = "++-";
			m_LShapeEvenEdgeSigns[8]  = "++-";
			m_LShapeEvenEdgeSigns[9]  = "+--";
			m_LShapeEvenEdgeSigns[10] = "+--";
			m_LShapeEvenEdgeSigns[11] = "+-+";
		}

		// --------------------------------------------------------------------------------

		void DummyBuildingPlacer::Run(IMainCommands* cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape(); 

			// gets parameters from calling cfg file
			if(GetGlobalParameters(cmdIfc))
			{
				// gets dbf parameters from calling cfg file
				if(GetDbfParameters(cmdIfc))
				{
					// the following operations are needed to correct digitalization errors
					// (we want real rectangle and L-shapes frames)

					// sets the geometry of the shape
					if(SetGeometry())
					{
						// calculates mean UV directions
						CalculateMeanUVDirections();

						// orthogonalizes the UV directions
						OrthogonalizeUV();

						// reshape the shape to be a regular polygon
						// (removes digitalization errors)
						ReshapeVertices();

						// creates objects to export
						CreateObjects();

						// export the shape
						ExportCreatedObjects(cmdIfc);
					}
					else
					// skips the shape if SetGeometry returns false
					{
						printf("Shape skipped                  \n");
					}
				}
			}
		}

		// --------------------------------------------------------------------------------

		ModuleObjectOutputArray* DummyBuildingPlacer::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn)
		{
			m_pCurrentShape = shape;
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;

			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// the following operations are needed to correct digitalization errors
			// (we want real rectangle and L-shapes frames)

			// sets the geometry of the shape
			if(SetGeometry())
			{
				// calculates mean UV directions
				CalculateMeanUVDirections();

				// orthogonalizes the UV directions
				OrthogonalizeUV();

				// reshape the shape to be a regular polygon
				// (removes digitalization errors)
				ReshapeVertices();

				// creates objects to export
				CreateObjects();

				return &m_ObjectsOut;
			}
			else
			// skips the shape if SetGeometry returns false
			{
				return NULL;
			}
		}

		// --------------------------------------------------------------------------------

		bool DummyBuildingPlacer::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			// gets parameters from calling cfg file
			const char* x = cmdIfc->QueryValue("modelCivil", false);
			if (x != 0) 
			{
				m_GlobalIn.modelCivil = x;
			}
			else
			{
				ExceptReg::RegExcpt(Exception(11, ">>> Missing modelCivil value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("modelIndustrial", false);
			if (x != 0) 
			{
				m_GlobalIn.modelIndustrial = x;
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing modelIndustrial value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("modelMilitary", false);
			if (x != 0) 
			{
				m_GlobalIn.modelMilitary = x;
			}
			else
			{
				ExceptReg::RegExcpt(Exception(13, ">>> Missing modelMilitary value <<<"));
				return false;
			}

			m_GlobalIn.maxNumFloorsCivil = 3; // default value
			x = cmdIfc->QueryValue("maxNumFloorsCivil", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.maxNumFloorsCivil = atoi(x);
				}
				else
				{
					printf(">>> -------------------------------------------------------- <<<\n");
					printf(">>> Bad data for maxNumFloorsCivil - using default value (3) <<<\n");
					printf(">>> -------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> --------------------------------------------------------- <<<\n");
				printf(">>> Missing maxNumFloorsCivil value - using default value (3) <<<\n");
				printf(">>> --------------------------------------------------------- <<<\n");
			}
			if (m_GlobalIn.maxNumFloorsCivil < 1)
			{
				ExceptReg::RegExcpt(Exception(14, " >>> maxNumFloorsCivil MUST be equal or greater than 1 <<<"));
				return false;
			}

			m_GlobalIn.maxNumFloorsIndustrial = 2; // default value
			x = cmdIfc->QueryValue("maxNumFloorsIndustrial", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.maxNumFloorsIndustrial = atoi(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------------- <<<\n");
					printf(">>> Bad data for maxNumFloorsIndustrial - using default value (2) <<<\n");
					printf(">>> ------------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> -------------------------------------------------------------- <<<\n");
				printf(">>> Missing maxNumFloorsIndustrial value - using default value (2) <<<\n");
				printf(">>> -------------------------------------------------------------- <<<\n");
			}
			if (m_GlobalIn.maxNumFloorsIndustrial < 1)
			{
				ExceptReg::RegExcpt(Exception(15, ">>> maxNumFloorsIndustrial MUST be equal or greater than 1 <<<"));
				return false;
			}

			m_GlobalIn.maxNumFloorsMilitary = 1; // default value
			x = cmdIfc->QueryValue("maxNumFloorsMilitary", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.maxNumFloorsMilitary = atoi(x);
				}
				else
				{
					printf(">>> ----------------------------------------------------------- <<<\n");
					printf(">>> Bad data for maxNumFloorsMilitary - using default value (1) <<<\n");
					printf(">>> ----------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------------ <<<\n");
				printf(">>> Missing maxNumFloorsMilitary value - using default value (1) <<<\n");
				printf(">>> ------------------------------------------------------------ <<<\n");
			}
			if (m_GlobalIn.maxNumFloorsMilitary < 1)
			{
				ExceptReg::RegExcpt(Exception(16, ">>> maxNumFloorsMilitary MUST be equal or greater than 1 <<<"));
				return false;
			}

			m_GlobalIn.heightFloorsCivil = 3.0; // default value
			x = cmdIfc->QueryValue("heightFloorsCivil", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.heightFloorsCivil = atof(x);
				}
				else
				{
					printf(">>> ---------------------------------------------------------- <<<\n");
					printf(">>> Bad data for heightFloorsCivil - using default value (3.0) <<<\n");
					printf(">>> ---------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ----------------------------------------------------------- <<<\n");
				printf(">>> Missing heightFloorsCivil value - using default value (3.0) <<<\n");
				printf(">>> ----------------------------------------------------------- <<<\n");
			}
			if (m_GlobalIn.heightFloorsCivil < 3.0)
			{
				printf(">>> --------------------------------------------------------------- <<<\n");
				printf(">>> Value for heightFloorsCivil too low - using default value (3.0) <<<\n");
				printf(">>> --------------------------------------------------------------- <<<\n");
				m_GlobalIn.heightFloorsCivil = 3.0;
			}

			m_GlobalIn.heightFloorsIndustrial = 3.0; // default value
			x = cmdIfc->QueryValue("heightFloorsIndustrial", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.heightFloorsIndustrial = atof(x);
				}
				else
				{
					printf(">>> --------------------------------------------------------------- <<<\n");
					printf(">>> Bad data for heightFloorsIndustrial - using default value (3.0) <<<\n");
					printf(">>> --------------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ---------------------------------------------------------------- <<<\n");
				printf(">>> Missing heightFloorsIndustrial value - using default value (3.0) <<<\n");
				printf(">>> ---------------------------------------------------------------- <<<\n");
			}
			if (m_GlobalIn.heightFloorsIndustrial < 3.0)
			{
				printf(">>> -------------------------------------------------------------------- <<<\n");
				printf(">>> Value for heightFloorsIndustrial too low - using default value (3.0) <<<\n");
				printf(">>> -------------------------------------------------------------------- <<<\n");
				m_GlobalIn.heightFloorsIndustrial = 3.0;
			}

			m_GlobalIn.heightFloorsMilitary = 3.0; // default value
			x = cmdIfc->QueryValue("heightFloorsMilitary", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.heightFloorsMilitary = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------------- <<<\n");
					printf(">>> Bad data for heightFloorsMilitary - using default value (3.0) <<<\n");
					printf(">>> ------------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> -------------------------------------------------------------- <<<\n");
				printf(">>> Missing heightFloorsMilitary value - using default value (3.0) <<<\n");
				printf(">>> -------------------------------------------------------------- <<<\n");
			}
			if (m_GlobalIn.heightFloorsMilitary < 3.0)
			{
				printf(">>> ----------------------------------------------------------------- <<<\n");
				printf(">>> Value for heightFloorsMilitary too low - using default value (3.0)<<<\n");
				printf(">>> ----------------------------------------------------------------- <<<\n");
				m_GlobalIn.heightFloorsMilitary = 3.0;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool DummyBuildingPlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			// gets shape type
			m_DbfIn.buildingType = btNULL;
			const char* x = cmdIfc->QueryValue("buildingType", false);
			if (x != 0) 
			{
				if(string(x) == "CIVIL")      m_DbfIn.buildingType = btCIVIL;
				if(string(x) == "INDUSTRIAL") m_DbfIn.buildingType = btINDUSTRIAL;
				if(string(x) == "MILITARY")   m_DbfIn.buildingType = btMILITARY;
			}
			else
			{
				ExceptReg::RegExcpt(Exception(21, ">>> Missing buildingType value <<<"));
				return false;
			}
			
			// checks if existing shape type
			if(m_DbfIn.buildingType == btNULL)
			{
				ExceptReg::RegExcpt(Exception(22, ">>> buildingType not implemented <<<"));
				return false;
			}

			// gets number of floor
			// 0 means random
			x = cmdIfc->QueryValue("numOfFloor", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_DbfIn.numOfFloors = atoi(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(23, ">>> Bad data for numOfFloor <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(24, ">>> Missing numOfFloor value <<<"));
				return false;
			}

			// checks if positive
			if(m_DbfIn.numOfFloors < 0)
			{
				ExceptReg::RegExcpt(Exception(25, ">>> numOfFloor MUST be a positive value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------
		// returns false if the shape has no 4 or 6 vertices or if it has 6 vertices
		// but has not a recognizable L-shape
		bool DummyBuildingPlacer::SetGeometry()
		{
			// gets the number of vertices
			m_csVertexCount = m_pCurrentShape->GetVertexCount();

			// checks vertex count
			if((m_csVertexCount != 4) && (m_csVertexCount != 6))
			{
				return false;
			}

			// sets the shape of the shape
			if(m_csVertexCount == 4)
			{
				m_BuildingShape = bsRECTANGLE;
			}
			else
			{
				m_BuildingShape = bsLSHAPE;
			}

			// sets the vertices array
			if(m_Vertices.Size() != 0)
			{
				m_Vertices.Clear();
			}

			for(int i = 0; i < m_csVertexCount; ++i)
			{
				Shapes::DVertex v = m_pCurrentShape->GetVertex(i);
				m_Vertices.Add(v);
			}

			// sets the edges array
			if(m_Edges.Size() != 0)
			{
				m_Edges.Clear();
			}

			for(int i = 0; i < (m_csVertexCount - 1); ++i)
			{
				Edge e(&m_Vertices[i], &m_Vertices[i + 1]);
				m_Edges.Add(e);
			}
			Edge e(&m_Vertices[m_csVertexCount - 1], &m_Vertices[0]);
			m_Edges.Add(e);

			// determines the configuration if L-Shape
			if(m_BuildingShape == bsLSHAPE)
			{
				string tmpConfiguration = "";

				// to determinate if the sequence of corners
				for(int i = 0; i < m_csVertexCount; ++i)
				{
					int i1 = i;
					int i2 = i + 1;
					int i3 = i + 2;
					if(i2 >= m_csVertexCount) i2 -= m_csVertexCount;
					if(i3 >= m_csVertexCount) i3 -= m_csVertexCount;

					Shapes::DVector v1(m_Vertices[i2].x - m_Vertices[i1].x, m_Vertices[i2].y - m_Vertices[i1].y);
					Shapes::DVector v2(m_Vertices[i3].x - m_Vertices[i1].x, m_Vertices[i3].y - m_Vertices[i1].y);

					Shapes::DVector cross = v1.Cross(v2);

					if(cross.z > 0)
					{
						tmpConfiguration += "L";
					}
					else
					{
						tmpConfiguration += "R";
					}
				}

				// assigns configuration
				m_LShapeConfiguration = lscNUM;
				for(int i = 0; i < lscNUM; ++i)
				{
					if(tmpConfiguration == m_StrLShapeConfiguration[i])
					{
						m_LShapeConfiguration = static_cast<LShapeConfiguration>(i);
					}
				}

				// checks configuration
				if(m_LShapeConfiguration == lscNUM)
				{
					return false;
				}
			}
			return true;
		}

		// --------------------------------------------------------------------------------

		void DummyBuildingPlacer::CalculateMeanUVDirections()
		{
			double tempU = 0.0;
			double tempV = 0.0;

			// U direction is determined by the odd edges (starting at 0)
			// V direction is determined by the even edges (starting at 1)
			// in case of rectangle we have two couples of edges, in each couple
			// the two edges point in opposite directions whatever if the order of
			// vertices is CW or CCW
			// in case of L-shape the orientation of the edges depends on the order
			// in which the vertices were inputted during vectorialization, if CW
			// or CCW, and on the choice of the first vertex, so we have to check
			// all possible case

			// odd edges
			int counter = 0;
			double firstDir;
			for(int i = 0; i < m_csVertexCount; i += 2)
			{
				double tempDir = m_Edges[i].DirectionXY();

				// if L-Shape
				if(m_BuildingShape == bsLSHAPE)
				{
					if(m_LShapeOddEdgeSigns[m_LShapeConfiguration][counter] == '-')
					{
						tempDir += EtMathD::PI;
						if(tempDir > EtMathD::TWO_PI)
						{
							tempDir -= EtMathD::TWO_PI;
						}
					}
				}
				// if rectangle
				else
				{
					if(counter == 1)
					{
						tempDir += EtMathD::PI;
						if(tempDir > EtMathD::TWO_PI)
						{
							tempDir -= EtMathD::TWO_PI;
						}
					}
				}

				// store first direction to check for problems around the 0.0
				// when adding directions
				if(counter == 0)
				{
					firstDir = tempDir;
				}
				// checks for problems around the 0.0 (if the product of sins is negative
				// then we have a problem - reasoning in degrees instead of radians:
				// this means that we are tring to add something like 1 + 359, while the
				// correct should be 1 + (-1) if then we want the average direction (in the
				// first case we will obtain 180 which is not correct))
				//
				// this implementation doesn't intercept and correct the case:
				// 1st dir = 0.0
				// 2nd * 3rd < 0.0
				else
				{
					double c1 = EtMathD::Cos(firstDir);
					double s1 = EtMathD::Sin(firstDir);
					double s2 = EtMathD::Sin(tempDir);
					if((c1 > 0) && ((s1 * s2) < 0.0))
					{
						if(s1 >= 0.0)
						{
							tempDir -= EtMathD::TWO_PI;
						}
						else
						{
							tempDir += EtMathD::TWO_PI;
						}
					}
				}

				tempU += tempDir;
				counter++;
			}
			m_MeanDirectionU = tempU / counter;
			if(m_MeanDirectionU > EtMathD::TWO_PI)
			{
				m_MeanDirectionU -= EtMathD::TWO_PI;
			}
			if(m_MeanDirectionU < 0.0)
			{
				m_MeanDirectionU += EtMathD::TWO_PI;
			}

			// even edges
			counter = 0;
			for(int i = 1; i < m_csVertexCount; i += 2)
			{
				double tempDir = m_Edges[i].DirectionXY();

				// if L-Shape
				if (m_BuildingShape == bsLSHAPE)
				{
					if (m_LShapeEvenEdgeSigns[m_LShapeConfiguration][counter] == '-')
					{
						tempDir += EtMathD::PI;
						if (tempDir > EtMathD::TWO_PI)
						{
							tempDir -= EtMathD::TWO_PI;
						}
					}
				}
				// if rectangle
				else
				{
					if (counter == 1)
					{
						tempDir += EtMathD::PI;
						if (tempDir > EtMathD::TWO_PI)
						{
							tempDir -= EtMathD::TWO_PI;
						}
					}
				}

				// store first direction to check for problems around the 0.0
				// when adding directions
				if(counter == 0)
				{
					firstDir = tempDir;
				}
				// checks for problems around the 0.0 (if the product of sins is negative
				// then we have a problem - reasoning in degrees instead of radians:
				// this means that we are tring to add something like 1 + 359, while the
				// correct should be 1 + (-1) if then we want the average direction (in the
				// first case we will obtain 180 which is not correct))
				//
				// this implementation doesn't intercept and correct the case:
				// 1st dir = 0.0
				// 2nd * 3rd < 0.0
				// that can happen with L-Shape
				else
				{
					double c1 = EtMathD::Cos(firstDir);
					double s1 = EtMathD::Sin(firstDir);
					double s2 = EtMathD::Sin(tempDir);
					if((c1 > 0) && ((s1 * s2) < 0.0))
					{
						if(s1 >= 0.0)
						{
							tempDir -= EtMathD::TWO_PI;
						}
						else
						{
							tempDir += EtMathD::TWO_PI;
						}
					}
				}

				tempV += tempDir;
				counter++;
			}
			m_MeanDirectionV = tempV / counter;
			if(m_MeanDirectionV > EtMathD::TWO_PI)
			{
				m_MeanDirectionV -= EtMathD::TWO_PI;
			}
			if(m_MeanDirectionV < 0.0)
			{
				m_MeanDirectionV += EtMathD::TWO_PI;
			}
		}

		// --------------------------------------------------------------------------------

		void DummyBuildingPlacer::OrthogonalizeUV()
		{
			// unitary vectors on the UV directions
			Shapes::DVector vectorU(EtMathD::Cos(m_MeanDirectionU), EtMathD::Sin(m_MeanDirectionU));
			Shapes::DVector vectorV(EtMathD::Cos(m_MeanDirectionV), EtMathD::Sin(m_MeanDirectionV));

			// dot product between vectors
			double dot = vectorU.DotXY(vectorV);
			// cross product between vectors
			Shapes::DVector cross = vectorU.Cross(vectorV);
			double angle = EtMathD::ACos(dot);

			if (angle > EtMathD::PI) angle = EtMathD::TWO_PI - angle;
			double correctAngle = EtMathD::Fabs((EtMathD::PI / 2.0 - angle) / 2.0);

			// angle smaller than 90 degrees
			if (dot > EtMathD::ZERO_TOLERANCE)
			{
				if(cross.z > 0)
				{
					m_MeanDirectionU -= correctAngle; 
					m_MeanDirectionV += correctAngle;
				}
				else
				{
					m_MeanDirectionU += correctAngle; 
					m_MeanDirectionV -= correctAngle;
				}
			}

			// angle greater than 90 degrees
			if(dot < EtMathD::ZERO_TOLERANCE)
			{
				if(cross.z > 0)
				{
					m_MeanDirectionU += correctAngle; 
					m_MeanDirectionV -= correctAngle;
				}
				else
				{
					m_MeanDirectionU -= correctAngle; 
					m_MeanDirectionV += correctAngle;
				}
			}
		}

		// --------------------------------------------------------------------------------

		void DummyBuildingPlacer::ReshapeVertices()
		{
			// gets barycenter of shape
			Shapes::DVertex barycenter = GetShapeBarycenter(m_pCurrentShape);

			// aligns U axis to the X axis
			RotateAboutPoint(barycenter, -m_MeanDirectionU);

			// Replace vertices to remove digitalizatin errors
			OrthogonalizeShape();

			// restores previous alignement
			RotateAboutPoint(barycenter, m_MeanDirectionU);
		}

		// --------------------------------------------------------------------------------

		void DummyBuildingPlacer::RotateAboutPoint(const Shapes::DVertex point, double angle)
		{
			for(int i = 0; i < m_Vertices.Size(); ++i)
			{
				double dx = m_Vertices[i].x - point.x;
				double dy = m_Vertices[i].y - point.y;

				m_Vertices[i].x = point.x + (dx * EtMathD::Cos(angle) - dy * EtMathD::Sin(angle));
				m_Vertices[i].y = point.y + (dx * EtMathD::Sin(angle) + dy * EtMathD::Cos(angle));
			}
		}

		// --------------------------------------------------------------------------------

		void DummyBuildingPlacer::OrthogonalizeShape()
		{
			AutoArray<Shapes::DVertex, MemAllocStack<Shapes::DVertex, 32> > midPoints;

			// stores edges midPoints
			for(int i = 0; i < m_Edges.Size(); ++i)
			{
				midPoints.Add(m_Edges[i].MidPoint());
			}

			// odd edges
			for(int i = 0; i < m_Edges.Size(); i += 2)
			{
				m_Edges[i].SetVertex(1, m_Edges[i].GetVertexX(1), midPoints[i].y);
				m_Edges[i].SetVertex(2, m_Edges[i].GetVertexX(2), midPoints[i].y);
			}

			// even edges
			for(int i = 1; i < m_Edges.Size(); i += 2)
			{
				m_Edges[i].SetVertex(1, midPoints[i].x, m_Edges[i].GetVertexY(1));
				m_Edges[i].SetVertex(2, midPoints[i].x, m_Edges[i].GetVertexY(2));
			}
		}

		// --------------------------------------------------------------------------------

		void DummyBuildingPlacer::CreateObjects()
		{
			// clears array
			m_ObjectsOut.Clear();

			// calculate building height
			double floorHeight;
			switch(m_DbfIn.buildingType)
			{
			case btCIVIL:
				floorHeight = m_GlobalIn.heightFloorsCivil;
			case btINDUSTRIAL:
				floorHeight = m_GlobalIn.heightFloorsIndustrial;
			case btMILITARY:
				floorHeight = m_GlobalIn.heightFloorsMilitary;
			}

			int numFloor;
			if(m_DbfIn.numOfFloors == 0)
			{
				// random num floor
				int max;
				switch(m_DbfIn.buildingType)
				{
				case btCIVIL:
					max = m_GlobalIn.maxNumFloorsCivil;
				case btINDUSTRIAL:
					max = m_GlobalIn.maxNumFloorsIndustrial;
				case btMILITARY:
					max = m_GlobalIn.maxNumFloorsMilitary;
				}

				numFloor = 1 + static_cast<int>((rand() / RAND_MAX) * max);
			}
			else
			{
				numFloor = m_DbfIn.numOfFloors;
			}

			double height = floorHeight * numFloor;

			// if L-Shape
			if(m_BuildingShape == bsLSHAPE)
			{
				// the subdivision in 2 rectangles depends on the vertices
				// order

				// new objects
				ModuleObjectOutput out1;
				ModuleObjectOutput out2;

				int edge_1_1_1, edge_1_1_2, edge_1_2_1, edge_1_2_2;
				int edge_2_1_1, edge_2_1_2, edge_2_2_1, edge_2_2_2;
				int diag_1_1, diag_1_2;
				int diag_2_1, diag_2_2;

				switch(m_LShapeConfiguration)
				{
				case lscRLLLLL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 0;
						edge_1_1_2 = 1;
						// edge 2 rectangle 1
						edge_1_2_1 = 5;
						edge_1_2_2 = 0;
						// diagonal rectangle 1
						diag_1_1 = 5;
						diag_1_2 = 1;
						// edge 1 rectangle 2
						edge_2_1_1 = 2;
						edge_2_1_2 = 3;
						// edge 2 rectangle 2
						edge_2_2_1 = 4;
						edge_2_2_2 = 3;
						// diagonal rectangle 2
						diag_2_1 = 2;
						diag_2_2 = 4;
					}
					break;
				case lscLRLLLL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 0;
						edge_1_1_2 = 1;
						// edge 2 rectangle 1
						edge_1_2_1 = 1;
						edge_1_2_2 = 2;
						// diagonal rectangle 1
						diag_1_1 = 0;
						diag_1_2 = 2;
						// edge 1 rectangle 2
						edge_2_1_1 = 5;
						edge_2_1_2 = 4;
						// edge 2 rectangle 2
						edge_2_2_1 = 3;
						edge_2_2_2 = 4;
						// diagonal rectangle 2
						diag_2_1 = 3;
						diag_2_2 = 5;
					}
					break;
				case lscLLRLLL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 3;
						edge_1_1_2 = 2;
						// edge 2 rectangle 1
						edge_1_2_1 = 1;
						edge_1_2_2 = 2;
						// diagonal rectangle 1
						diag_1_1 = 1;
						diag_1_2 = 3;
						// edge 1 rectangle 2
						edge_2_1_1 = 5;
						edge_2_1_2 = 4;
						// edge 2 rectangle 2
						edge_2_2_1 = 0;
						edge_2_2_2 = 5;
						// diagonal rectangle 2
						diag_2_1 = 0;
						diag_2_2 = 4;
					}
					break;
				case lscLLLRLL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 3;
						edge_1_1_2 = 2;
						// edge 2 rectangle 1
						edge_1_2_1 = 4;
						edge_1_2_2 = 3;
						// diagonal rectangle 1
						diag_1_1 = 2;
						diag_1_2 = 4;
						// edge 1 rectangle 2
						edge_2_1_1 = 0;
						edge_2_1_2 = 1;
						// edge 2 rectangle 2
						edge_2_2_1 = 0;
						edge_2_2_2 = 5;
						// diagonal rectangle 2
						diag_2_1 = 1;
						diag_2_2 = 5;
					}
					break;
				case lscLLLLRL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 4;
						edge_1_1_2 = 5;
						// edge 2 rectangle 1
						edge_1_2_1 = 4;
						edge_1_2_2 = 3;
						// diagonal rectangle 1
						diag_1_1 = 3;
						diag_1_2 = 5;
						// edge 1 rectangle 2
						edge_2_1_1 = 0;
						edge_2_1_2 = 1;
						// edge 2 rectangle 2
						edge_2_2_1 = 1;
						edge_2_2_2 = 2;
						// diagonal rectangle 2
						diag_2_1 = 0;
						diag_2_2 = 2;
					}
					break;
				case lscLLLLLR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 4;
						edge_1_1_2 = 5;
						// edge 2 rectangle 1
						edge_1_2_1 = 5;
						edge_1_2_2 = 0;
						// diagonal rectangle 1
						diag_1_1 = 4;
						diag_1_2 = 0;
						// edge 1 rectangle 2
						edge_2_1_1 = 3;
						edge_2_1_2 = 2;
						// edge 2 rectangle 2
						edge_2_2_1 = 1;
						edge_2_2_2 = 2;
						// diagonal rectangle 2
						diag_2_1 = 1;
						diag_2_2 = 3;
					}
					break;
				case lscLRRRRR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 2;
						edge_1_1_2 = 3;
						// edge 2 rectangle 1
						edge_1_2_1 = 1;
						edge_1_2_2 = 2;
						// diagonal rectangle 1
						diag_1_1 = 1;
						diag_1_2 = 3;
						// edge 1 rectangle 2
						edge_2_1_1 = 5;
						edge_2_1_2 = 4;
						// edge 2 rectangle 2
						edge_2_2_1 = 5;
						edge_2_2_2 = 0;
						// diagonal rectangle 2
						diag_2_1 = 0;
						diag_2_2 = 4;
					}
					break;
				case lscRLRRRR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 2;
						edge_1_1_2 = 3;
						// edge 2 rectangle 1
						edge_1_2_1 = 3;
						edge_1_2_2 = 4;
						// diagonal rectangle 1
						diag_1_1 = 2;
						diag_1_2 = 4;
						// edge 1 rectangle 2
						edge_2_1_1 = 0;
						edge_2_1_2 = 1;
						// edge 2 rectangle 2
						edge_2_2_1 = 0;
						edge_2_2_2 = 5;
						// diagonal rectangle 2
						diag_2_1 = 1;
						diag_2_2 = 5;
					}
					break;
				case lscRRLRRR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 5;
						edge_1_1_2 = 4;
						// edge 2 rectangle 1
						edge_1_2_1 = 3;
						edge_1_2_2 = 4;
						// diagonal rectangle 1
						diag_1_1 = 3;
						diag_1_2 = 5;
						// edge 1 rectangle 2
						edge_2_1_1 = 0;
						edge_2_1_2 = 1;
						// edge 2 rectangle 2
						edge_2_2_1 = 1;
						edge_2_2_2 = 2;
						// diagonal rectangle 2
						diag_2_1 = 0;
						diag_2_2 = 2;
					}
					break;
				case lscRRRLRR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 5;
						edge_1_1_2 = 4;
						// edge 2 rectangle 1
						edge_1_2_1 = 0;
						edge_1_2_2 = 5;
						// diagonal rectangle 1
						diag_1_1 = 0;
						diag_1_2 = 4;
						// edge 1 rectangle 2
						edge_2_1_1 = 3;
						edge_2_1_2 = 2;
						// edge 2 rectangle 2
						edge_2_2_1 = 1;
						edge_2_2_2 = 2;
						// diagonal rectangle 2
						diag_2_1 = 1;
						diag_2_2 = 3;
					}
					break;
				case lscRRRRLR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 0;
						edge_1_1_2 = 1;
						// edge 2 rectangle 1
						edge_1_2_1 = 0;
						edge_1_2_2 = 5;
						// diagonal rectangle 1
						diag_1_1 = 1;
						diag_1_2 = 5;
						// edge 1 rectangle 2
						edge_2_1_1 = 3;
						edge_2_1_2 = 2;
						// edge 2 rectangle 2
						edge_2_2_1 = 4;
						edge_2_2_2 = 3;
						// diagonal rectangle 2
						diag_2_1 = 2;
						diag_2_2 = 4;
					}
					break;
				case lscRRRRRL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 0;
						edge_1_1_2 = 1;
						// edge 2 rectangle 1
						edge_1_2_1 = 1;
						edge_1_2_2 = 2;
						// diagonal rectangle 1
						diag_1_1 = 0;
						diag_1_2 = 2;
						// edge 1 rectangle 2
						edge_2_1_1 = 4;
						edge_2_1_2 = 5;
						// edge 2 rectangle 2
						edge_2_2_1 = 4;
						edge_2_2_2 = 3;
						// diagonal rectangle 2
						diag_2_1 = 3;
						diag_2_2 = 5;
					}
					break;
				}
				Edge edge11(&m_Vertices[edge_1_1_1], &m_Vertices[edge_1_1_2]);
				Edge edge12(&m_Vertices[edge_1_2_1], &m_Vertices[edge_1_2_2]);
				Edge edge21(&m_Vertices[edge_2_1_1], &m_Vertices[edge_2_1_2]);
				Edge edge22(&m_Vertices[edge_2_2_1], &m_Vertices[edge_2_2_2]);
				Edge diag1(&m_Vertices[diag_1_1], &m_Vertices[diag_1_2]);
				Edge diag2(&m_Vertices[diag_2_1], &m_Vertices[diag_2_2]);

				out1.position = diag1.MidPoint();
				out1.rotation = m_MeanDirectionU;
				out1.scaleX = edge11.Length();
				out1.scaleY = edge12.Length();
				out1.scaleZ = height;

				out2.position = diag2.MidPoint();
				out2.rotation = m_MeanDirectionU;
				out2.scaleX = edge21.Length();
				out2.scaleY = edge22.Length();
				out2.scaleZ = height;

				// used only by VLB for preview
				out1.length = 1.0;
				out1.width  = 1.0;
				out1.height = 1.0;
				out2.length = 1.0;
				out2.width  = 1.0;
				out2.height = 1.0;

				// adds to array
				m_ObjectsOut.Add(out1);
				m_ObjectsOut.Add(out2);

			}
			// if rectangle
			else
			{
				// new object
				ModuleObjectOutput out;

				out.position = GetShapeBarycenter(m_pCurrentShape);
				out.rotation = m_MeanDirectionU;
				out.scaleX = m_Vertices[0].DistanceXY(m_Vertices[1]);
				out.scaleY = m_Vertices[1].DistanceXY(m_Vertices[2]);
				out.scaleZ = height;

				// used only by VLB for preview
				out.length = 1.0;
				out.width  = 1.0;
				out.height = 1.0;

				// adds to array
				m_ObjectsOut.Add(out);
			}
		}

		// --------------------------------------------------------------------------------

		void DummyBuildingPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			for(int i = 0; i < m_ObjectsOut.Size(); i++)
			{
				RString name;
				switch(m_DbfIn.buildingType)
				{
				case btCIVIL:
					name = m_GlobalIn.modelCivil;
				case btINDUSTRIAL:
					name = m_GlobalIn.modelIndustrial;
				case btMILITARY:
					name = m_GlobalIn.modelMilitary;
				}

				Vector3 position = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------------------
	}
}