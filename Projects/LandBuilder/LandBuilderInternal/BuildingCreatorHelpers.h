#pragma once

// -------------------------------------------------------------------------- //

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <windows.h>
#include "..\Shapes\Vertex.h"
#include "..\..\..\projects\ObjektivLib\LODObject.h"
#include ".\ILandBuildCmds.h"

#include "..\ComputationalGeometry\CG_Polygon2.h"

// -------------------------------------------------------------------------- //

using namespace ObjektivLib;

// -------------------------------------------------------------------------- //

using std::vector;
using std::string;
using std::ofstream;
using std::endl;

// -------------------------------------------------------------------------- //

using LandBuildInt::IMainCommands;

// -------------------------------------------------------------------------- //

namespace BC_Help
{
	// -------------------------------------------------------------------------- //

	static const double PI      = 4.0 * atan(1.0);
	static const double INV_PI  = 1.0 / PI;
	static const double TWO_PI  = 2.0 * PI;
	static const double HALF_PI = PI / 2.0;
	static const double EPSILON = 1e-08;

	// -------------------------------------------------------------------------- //

	class Dxf2006Writer
	{
		ofstream m_File;

	public:

		Dxf2006Writer();
		~Dxf2006Writer();

		void Set(const string& filename);

		void WritePolyline(const vector<Shapes::DVertex>& polyline, 
						   const string& layer, bool close, 
						   double startingWidth = 0, 
						   double endingWidth = 0);

	private:

		void WriteEntitiesHead();
		void WriteEntitiesTail();
		void WriteFileTail();
	};

	// -------------------------------------------------------------------------- //

	class Line
	{
		const Shapes::DVertex& m_V1;
		const Shapes::DVertex& m_V2;

		double m_A;
		double m_B;
		double m_C;

	public:
		Line(const Shapes::DVertex& v1, const Shapes::DVertex& v2);

		// calculates the intersection point between this line and the given one
		// the intersection point, if any, will be returned in the intPoint variable
		// returns false if the two lines are parallel
		bool Intersection(const Line& other, Shapes::DVertex& intPoint) const;

	private:
		void CalculateCoefficients();
	};

	// -------------------------------------------------------------------------- //

	class BoundingBox
	{
		double m_Width;
		double m_Height;
		double m_Orientation;
		int    m_BaseSegmentIndex;

	public:
		BoundingBox();

		double GetWidth() const;
		double GetHeight() const;
		double GetOrientation() const;
		int    GetBaseSegmentIndex() const;

		void Reset();

		void CalculateFromVertices(const vector<Shapes::DVertex>& vertices);
	};

	// -------------------------------------------------------------------------- //

	class Footprint
	{
		vector<Shapes::DVertex> m_Vertices;

		BoundingBox     m_BoundingBox;
		vector<double>  m_TurningFunction;
		string          m_TurningSequence;
		double          m_DelVertexTol;
		double          m_RectCornerTol;

	public:
		Footprint();
		Footprint(const vector<Shapes::DVertex>& vertices, double delVertexTol, 
				  double rectCornerTol, bool rectify);

		const vector<Shapes::DVertex>& GetVertices() const;
		vector<Shapes::DVertex>& GetVertices();

		Shapes::DVertex& GetVertex(unsigned int index);
		const Shapes::DVertex& GetVertex(unsigned int index) const;

		Shapes::DVertex& operator [] (unsigned int index);
		const Shapes::DVertex& operator [] (unsigned int index) const;

		unsigned int   GetVerticesCount() const;
		vector<double> GetTurningFunction();
		string         GetTurningSequence();

		// return the angle of rotation
		double GetMinBoundingBoxOrientation();

		Shapes::DVertex GetBarycenter() const;

		Shapes::DVertex GetMinPoint() const;

		double GetMinBoundingBoxWidth();
		double GetMinBoundingBoxHeight();

		// resets bounding box, to be called when modifying vertex using operator []
		void ResetBoundingBox();

	protected:
		void   RemoveDuplicatedPoints();
		void   SetVerticesOrderToCCW();
		void   DeleteMidSegmentVertices();
		void   RectifyCorners();
		double SignedArea() const;
		void   ReverseVerticesOrder();
		void   SetLongestEdgeAsFirst();
		void   CalculateTurningFunction();
		void   CalculateTurningSequence();
		void   RotateAboutVertex(const Shapes::DVertex& v, double angle);
	};

	// -------------------------------------------------------------------------- //
	// Helper functions
	// -------------------------------------------------------------------------- //

	// Returns the internal angle in the given vertex of the given polygon
	double InternalAngleAt(const vector<Shapes::DVertex>& polygon, unsigned int index, bool left = true);

	// -------------------------------------------------------------------------- //

	// Calculates the offsetted polygon of the given polygon
	// Returns false if there where problems as vertices reduction or non valid angles
	// Returns true if successful.
	// The polygon will be modified, so pass to this function a copy if you want
	// to preserve the original
	bool Offset(vector<Shapes::DVertex>& polygon, double offset, bool left = true);

	// -------------------------------------------------------------------------- //

	class Texture
	{
		RString      m_Pathname;
		RString      m_Filename;
		unsigned int m_Size;

	public:
		Texture(RString pathname, RString filename, unsigned int size);
		~Texture();

		// -------------------------------------------------------------------------- //
		// Getters
		// -------------------------------------------------------------------------- //

		RString      GetPathname() const;
		RString      GetFilename() const;
		unsigned int GetSize() const;
	};

	// -------------------------------------------------------------------------- //

	class BuildingModelP3D
	{
	protected:
		int PREDEF_PROC_TEXT_COUNT;
		vector<string> ProcTextures;
		string         BaseProcTexture;
		string         RoofProcTexture;

		RString         m_Filename;

		mutable Footprint m_Footprint;
		unsigned int      m_NumOfFloors;
		double            m_FloorHeight;
		unsigned int      m_RoofType;

		Shapes::DVertex   m_Position;
		double            m_Orientation;

		RString         m_ModelPath;
		RString         m_TexturesPath;
		vector<Texture> m_Textures;

		vector<CG_Polygon2> m_Partitions;

	public:
		BuildingModelP3D();

		BuildingModelP3D(RString filename, const Footprint& footprint, 
						 unsigned int numOfFloors, double floorHeight,
						 unsigned int foorType, RString modelPath, 
						 RString texturesPath, const vector<Texture>& textures);

		void Create(RString filename, const Footprint& footprint, 
					unsigned int numOfFloors, double floorHeight,
					unsigned int foorType, RString modelPath, 
					RString texturesPath, const vector<Texture>& textures);

		// -------------------------------------------------------------------------- //
		// Getters
		// -------------------------------------------------------------------------- //

		Shapes::DVertex GetPosition() const;
		double          GetOrientation() const;

		void ExportToP3DFile();

		const vector<CG_Polygon2>& GetPartitions() const;

	protected:
		void GenerateLOD(ObjectData& obj, bool textureLOD = false, bool genProceduralTexture = false);
		void GenerateGeometryLOD(ObjectData& obj);
		void GenerateViewGeometryLOD(ObjectData& obj);
		void GenerateFireGeometryLOD(ObjectData& obj);
		void GenerateRoadwayLOD(ObjectData& obj);
		void GenerateShadowLOD(ObjectData& obj);
		void GenerateMapping(ObjectData& obj);
		void GenerateComponents(ObjectData& obj);

		void SetOrigin();
		void OrientateAsMinBoundingBox();
		void CorrectOrigin();
	};

	// -------------------------------------------------------------------------- //

	class Building
	{
		Footprint       m_Footprint;
		unsigned int    m_NumOfFloors;
		double          m_FloorHeight;
		unsigned int    m_RoofType;

		Shapes::DVertex  m_Position;
		double           m_Orientation;

		vector<double> m_FPTurningFunction;
		string         m_FPTurningSequence;

		BuildingModelP3D m_ModelP3D;

	public:
		Building();

		Building(const Footprint& footprint, unsigned int numOfFloors, 
				 double floorHeight, unsigned int roofType);

		Footprint&       GetFootprint();
		const Footprint& GetFootprint() const;

		Shapes::DVertex GetPosition() const;
		double          GetOrientation() const;

		const BuildingModelP3D& GetModelP3D() const;

		void GenerateP3DModels(IMainCommands* cmdIfc, RString modelsPath, RString texturesPath, const vector<Texture>& textures);
	};
}