#pragma once

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

#include "..\Shapes\ShapeHelpers.h"
#include "..\HighMapLoaders\include\EtRectElevationGrid.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class BiasedRandomPlacerSlope : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				bool         smallAtBoundary;
				unsigned int randomSeed;
			};

			struct DbfInput
			{
				double hectareDensity;
				double clusterCoeff;
				int    parentCount;
			};

			struct ObjectInput
			{
				RString m_Name;
				double  m_MinHeight;
				double  m_MaxHeight;
				double  m_Prob;
				int     m_Counter;
				double  m_MinDistance;
				double  m_MinSlope;
				double  m_MaxSlope;
				double  m_MaxDistance;
				double  m_NormProb;

				ObjectInput() 
				{
				}

				ObjectInput(RString name, double minHeight, double maxHeight, double prob, double minDistance, double minSlope, double maxSlope)
				: m_Name(name)
				, m_MinHeight(minHeight)
				, m_MaxHeight(maxHeight)
				, m_Prob(prob)
				, m_Counter(0)
				, m_MinDistance(minDistance)
				, m_MinSlope(minSlope)
				, m_MaxSlope(maxSlope)
				{
				}

				ClassIsMovable(ObjectInput); 
			};

			struct MapInput
			{
				string       m_DemType;
				string       m_FileName;
				unsigned int m_SrcLeft;
				unsigned int m_SrcTop;
				unsigned int m_SrcRight;
				unsigned int m_SrcBottom;
				double       m_DstLeft;
				double       m_DstTop;
				double       m_DstRight;
				double       m_DstBottom;

				EtRectElevationGrid<double, short> hmData;

				MapInput() {}
				MapInput(string demType, string fileName, unsigned int srcLeft, unsigned int srcTop, 
					     unsigned int srcRight, unsigned int srcBottom, double dstLeft, double dstTop,
						 double dstRight, double dstBottom)
				{
					m_DemType   = demType;
					m_FileName  = fileName;
					m_SrcLeft   = srcLeft;
					m_SrcTop    = srcTop;
					m_SrcRight  = srcRight;
					m_SrcBottom = srcBottom;
					m_DstLeft   = dstLeft;
					m_DstTop    = dstTop;
					m_DstRight  = dstRight;
					m_DstBottom = dstBottom;
				}

				ClassIsMovable(MapInput); 
			};

		private:
			struct ShapeGeo
			{
				double minX;
				double maxX;
				double minY;
				double maxY;
				double widthBB;        // bounding box width
				double heightBB;       // bounding box height
				double area;           // area of the shape (polygon)
				double areaHectares;   // area of the shape in hectares
				double areaBB;         // bounding box area
				double areaBBHectares; // bounding box area in hectares
			};

			struct ObjectOutputExtra
			{
				ModuleObjectOutput data;
				bool               atBoundary;
				int	               childIndex;
				double             minDistance;
				double             maxDistance;
				ClassIsMovable(ObjectOutputExtra); 
			};

		public:
			BiasedRandomPlacerSlope();

			virtual void Run(IMainCommands*);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, MapInput& mapIn,
												  AutoArray<BiasedRandomPlacerSlope::ObjectInput, MemAllocStack<BiasedRandomPlacerSlope::ObjectInput, 32> >& objectsIn);

			class Exception : public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code,const char* text) : code(code), text(text) {}
				virtual unsigned int GetCode() const { return code; }
				virtual const _TCHAR* GetDesc() const { return text; }
				virtual const _TCHAR* GetModule() const { return "BiasedRandomPlacer"; }
				virtual const _TCHAR* GetType() const { return "BiasedRandomPlacerException"; }
			};

		private:
			string sDTED2;
			string sUSGSDEM;
			string sARCINFOASCII;
			string sXYZ;

			bool   m_MapLoaded;

			AutoArray<ObjectInput, MemAllocStack<ObjectInput, 32> >             m_ObjectsIn;
			AutoArray<ObjectOutputExtra, MemAllocStack<ObjectOutputExtra, 32> > m_ObjectsOutExtra;

			ModuleObjectOutputArray m_ObjectsOut;

			GlobalInput           m_GlobalIn;
			DbfInput              m_DbfIn;
			const Shapes::IShape* m_pCurrentShape;      
			ShapeGeo              m_ShpGeo;
			int                   m_NumberOfIndividuals;
			MapInput    m_MapIn;

			EtRectElevationGrid<double, short> m_MapData;

			bool GetShapeGeoData();
			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetObjectsParameters(IMainCommands* cmdIfc);
			bool GetMapParameters(IMainCommands* cmdIfc);
			bool LoadHighMap();

			bool NormalizeProbs();
			void SetParents();
			void CreateObjects(bool preview);
			void AddObjectOutExtra(const Shapes::DVector& v, int type, ObjectOutputExtra* parent);
			int  GetRandomType();
			bool PtCloseToObject(const Shapes::DVector& v) const;
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();

			ObjectOutputExtra* GetRandomParent(int type);
		};
	}
}