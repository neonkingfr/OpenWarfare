#include "StdAfx.h"
#include ".\BestFitPlacer.h"

#include "..\HighMapLoaders\include\EtMath.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------//

		void BestFitPlacer::Run(IMainCommands* cmdIfc)
		{
			// resets arrays
			if (m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();    

			// gets parameters from calling cfg file
			if (GetDbfParameters(cmdIfc))
			{
				// gets objects parameters from calling cfg file
				if (GetObjectsParameters(cmdIfc))
				{
					SetGeometry();

					// sets the output objects
					CreateObjects();

					// export objects
					ExportCreatedObjects(cmdIfc);

					// writes the report
					WriteReport();
				}
			}
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* BestFitPlacer::RunAsPreview(const Shapes::IShape* shape, DbfInput& dbfIn,
															 AutoArray<BestFitPlacer::ObjectInput, MemAllocStack<BestFitPlacer::ObjectInput, 32> >& objectsIn)
		{
			m_pCurrentShape = shape;
			m_DbfIn         = dbfIn;
			m_ObjectsIn     = objectsIn;

			// resets arrays
			if (m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			SetGeometry();

			// sets the output objects
			CreateObjects();

			return &m_ObjectsOut;
		}

		// --------------------------------------------------------------------//

		bool BestFitPlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			// gets parameters from calling cfg file
			m_DbfIn.dirCorrection = 0.0; // default value
			const char* x = cmdIfc->QueryValue("dirCorrection", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.dirCorrection = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------ <<<\n");
					printf(">>> Bad data for dirCorrection - using default value (0.0) <<<\n");
					printf(">>> ------------------------------------------------------ <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------- <<<\n");
				printf(">>> Missing dirCorrection value - using default value (0.0) <<<\n");
				printf(">>> ------------------------------------------------------- <<<\n");
			}

			x = cmdIfc->QueryValue("desiredHeight", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.desiredHeight = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for desired height <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing desired height value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool BestFitPlacer::GetObjectsParameters(IMainCommands* cmdIfc)
		{
			// resets arrays
			if (m_ObjectsIn.Size() != 0)
			{
				m_ObjectsIn.Clear();
				m_ObjectsOut.Clear();
			}

			int counter = 1;
			do 
			{
				char object[50], length[50], width[50], height[50];
				sprintf_s(object, "object%d", counter);
				sprintf_s(length, "length%d", counter);
				sprintf_s(width , "width%d" , counter);
				sprintf_s(height, "height%d", counter);

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue(object, false);
				if (x == 0) break;
				nfo.m_Name = x;
        
				// object lenght
				x = cmdIfc->QueryValue(length, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_Length = atof(x);
					}
					else
					{
						ExceptReg::RegExcpt(Exception(21, ">>> Bad data for length <<<"));
						return false;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(22, ">>> Missing length value <<<"));
					return false;
				}

				// object width
				x = cmdIfc->QueryValue(width, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_Width = atof(x);
					}
					else
					{
						ExceptReg::RegExcpt(Exception(23, ">>> Bad data for width <<<"));
						return false;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(24, ">>> Missing width value <<<"));
					return false;
				}
        
				// object height
				x = cmdIfc->QueryValue(height, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_Height = atof(x);
					}
					else
					{
						ExceptReg::RegExcpt(Exception(25, ">>> Bad data for height <<<"));
						return false;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(26, ">>> Missing height value <<<"));
					return false;
				}

				m_ObjectsIn.Add(nfo);
				counter++;
			} 
			while(true); 

			return true;
		}

		// --------------------------------------------------------------------//

		void BestFitPlacer::SetGeometry()
		{
			// use the algorithm from: 
			// http://www.geometrictools.com/Documentation/MinimumAreaRectangle.pdf

			unsigned int vCount = m_pCurrentShape->GetVertexCount();
			double minArea = EtMathD::MAX_REAL;

			Shapes::DVertex p0 = m_pCurrentShape->GetVertex(0);

			for (unsigned int i = 0, j = (vCount - 1); i < vCount; j = i++)
			{
				Shapes::DVertex pJ = m_pCurrentShape->GetVertex(j);
				Shapes::DVertex pI = m_pCurrentShape->GetVertex(i);

				Shapes::DVector vJI(pI, pJ);
				vJI.NormalizeXYInPlace();

				Shapes::DVector vPerp = vJI.PerpXYCCW();

				double l0 = 0.0;
				double l1 = 0.0;
				double w0 = 0.0;
				double w1 = 0.0;

				for (unsigned int k = 1; k < vCount; ++k)
				{
					Shapes::DVertex pK = m_pCurrentShape->GetVertex(k);
					Shapes::DVector vK0(pK, p0);

					double testL = vJI.DotXY(vK0);
					if (testL < l0)
					{
						l0 = testL;
					}
					else if (testL > l1)
					{
						l1 = testL;
					}

					double testW = vPerp.DotXY(vK0);
					if (testW < w0)
					{
						w0 = testW;
					}
					else if (testW > w1)
					{
						w1 = testW;
					}
				}

				double tempLength = l1 - l0;
				double tempWidth  = w1 - w0; 
				double area = tempLength * tempWidth;
				if (area < minArea)
				{
					minArea           = area;
					m_CalcLength      = tempLength;
					m_CalcWidth       = tempWidth;
					m_CalcOrientation = vJI.DirectionXY(); 
				}		
			}
		}

		// --------------------------------------------------------------------//

		void BestFitPlacer::CreateObjects()
		{
			ModuleObjectOutput out;

			unsigned int objInCount = m_ObjectsIn.Size();

			double minDiff = EtMathD::MAX_REAL;
			unsigned int choice = 0;

			for (unsigned int i = 0; i < objInCount; ++i)
			{
				double dx = EtMathD::Fabs(m_CalcLength - m_ObjectsIn[i].m_Length);
				double dy = EtMathD::Fabs(m_CalcWidth - m_ObjectsIn[i].m_Width);
				double dz = EtMathD::Fabs(m_DbfIn.desiredHeight - m_ObjectsIn[i].m_Height);
				double totDiff = dx + dy + dz;
				if (totDiff < minDiff)
				{
					minDiff = totDiff;
					choice = i;
				}
			}

			// sets object out data
			out.type     = choice;
			out.position = GetShapeBarycenter(m_pCurrentShape);
			out.rotation = m_CalcOrientation + EtMathD::DegToRad(m_DbfIn.dirCorrection);
			out.scaleX   = out.scaleY = out.scaleZ = 1.0;

			// used only by VLB for preview
			out.length = m_ObjectsIn[choice].m_Length;
			out.width  = m_ObjectsIn[choice].m_Width;
			out.height = m_ObjectsIn[choice].m_Height;

			// adds to array
			m_ObjectsOut.Add(out);
		}

		// --------------------------------------------------------------------//

		void BestFitPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for (int i = 0; i < objOutCount; ++i)
			{
				RString name      = m_ObjectsIn[m_ObjectsOut[i].type].m_Name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));

				Matrix4 transform = mTranslation * mRotation;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void BestFitPlacer::WriteReport()
		{
			int objOutCount = m_ObjectsOut.Size();
			printf("Created %d new objects\n", objOutCount);
			printf("--------------------------------------\n");
		}
	}
}
