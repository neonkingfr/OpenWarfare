#pragma once

#include "ObjectDesc.h"
#include "../Shapes/AllocIDArray.h"
#include "../Shapes/Vertex.h"

namespace LandBuildInt
{
	class ObjectContainer
	{
		AllocIDArray<ObjectDesc, 32768 / sizeof(ObjectDesc)> _objects;

		// -------------------------------------------------------------------------- //

		class IndexBase : public BTreeDefaultCompare
		{
		protected:
			AllocIDArray<ObjectDesc, 32768 / sizeof(ObjectDesc)>* _objects;
		public:
			void SetOwner(AllocIDArray<ObjectDesc, 32768 / sizeof(ObjectDesc)>* objects)
			{
				_objects = objects;
			}

			// -------------------------------------------------------------------------- //

			const ObjectDesc* ConvIdToObject(int id) const
			{
				if(id & 0x1) 
				{
					return reinterpret_cast<ObjectDesc*>(id & ~0x1);
				}
				else 
				{
					return &(*_objects)[id>>1];
				}
			}

			// -------------------------------------------------------------------------- //

            int IndexById(int cmpres, int a, int b) 
            {                
                if (cmpres == 0) 
                {
                    if ((a & 0x1) && (~b & 0x1)) return -1;
                    if ((~a & 0x1) && (b & 0x1)) return 1;
                    return (a > b) - (a < b);
                }
                else
				{
                    return cmpres;
				}
            }
		};

		// -------------------------------------------------------------------------- //

		class IndexByName : public IndexBase
		{
		public:
			int Cmp(const int& a, const int& b)
			{
				return IndexById(ConvIdToObject(a)->IndexByName(*ConvIdToObject(b)), a, b);
			}
		};

		// -------------------------------------------------------------------------- //

		class IndexByXPos : public IndexBase
		{
		public:
			int Cmp(const int& a, const int& b)
			{
				return IndexById(ConvIdToObject(a)->IndexByXPos(*ConvIdToObject(b)), a, b);
			}
		};

		// -------------------------------------------------------------------------- //

		class IndexByYPos : public IndexBase
		{
		public:
			int Cmp(const int& a, const int& b)
			{
				return IndexById(ConvIdToObject(a)->IndexByYPos(*ConvIdToObject(b)), a, b);
			}
		};

		// -------------------------------------------------------------------------- //

		class IndexByTag : public IndexBase
		{
		public:
			int Cmp(const int& a, const int& b)
			{
				return IndexById(ConvIdToObject(a)->IndexByTag(*ConvIdToObject(b)), a, b);
			}
		};

		// -------------------------------------------------------------------------- //

		class SearchObj : public ObjectDesc
		{
		public:
			SearchObj(double x, double y, unsigned long tag = 0) 
			: ObjectDesc("", Matrix4(MTranslation, Vector3((Coord)x, (Coord)y, 0)), tag) 
			{
			}
		};

		// -------------------------------------------------------------------------- //

//    int ObjToIntID(const ObjectDesc *obj) const {return reinterpret_cast<const char *>(obj)-reinterpret_cast<const char *>(_objects.Data());}    

		BTree<int, IndexByName> _idxName;
		BTree<int, IndexByXPos> _idxXPos;
		BTree<int, IndexByYPos> _idxYPos;
		BTree<int, IndexByTag>  _idxTag;
		BTree<RStringI>         _stringTable;

		void IndexItem(int id);

		const ObjectDesc* NearestOnX(float x, float y, unsigned int tag, const Array<unsigned int>& ignore) const;
		const ObjectDesc* NearestOnY(float x, float y, unsigned int tag, const Array<unsigned int>& ignore) const;

	  public:
		ObjectContainer(void);
		~ObjectContainer(void);

		RStringI ShareString(const RStringI& str);
		RStringI ShareString(const RStringI& str) const;

		int Add(const ObjectDesc& object);
		bool Delete(int id);
		bool Replace(int id, const ObjectDesc& object);

		bool SetObjectName(int ID, const RStringI& name);
		int GetObjectID(const ObjectDesc* obj) const;

		// -------------------------------------------------------------------------- //

		template<class Functor>
		bool ForEachF(const Functor& f) const
		{
			return _objects.ForEachF(f);
		}

		// -------------------------------------------------------------------------- //

		SRef<AutoArray<const ObjectDesc*> > ObjectsInBox(const Shapes::DBox& box, unsigned int tag = 0) const;

		const ObjectDesc* Nearest(float x, float y, unsigned int tag = 0, const Array<unsigned int>& ignore = Array<unsigned int>(0)) const;

		// -------------------------------------------------------------------------- //

		const ObjectDesc& operator[](int pos) const 
		{
			return _objects[pos];
		}

		// -------------------------------------------------------------------------- //

		void ReIndex();

		// -------------------------------------------------------------------------- //

		bool IsValidObjectID(int ID) const
		{
			return _objects.IsIdValid(ID);
		}

		// -------------------------------------------------------------------------- //

		int Size() const 
		{
			return _objects.Size();
		}
	};
}

TypeIsSimpleZeroed(const LandBuildInt::ObjectDesc*)
