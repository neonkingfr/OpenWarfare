#include "StdAfx.h"
#include <algorithm>
#include ".\highmaploader.h"

namespace LandBuildInt
{
	namespace Modules
	{
		void HeightMapLoader::GetSourcesParameters(IMainCommands* cmdIfc)
		{
			int counter = 1;
			do 
			{
				char demType[50], fileName[50];
				char srcLeft[50], srcTop[50], srcRight[50], srcBottom[50];
				char dstLeft[50], dstTop[50], dstRight[50], dstBottom[50];
				sprintf(demType  , "demType%d"  , counter);
				sprintf(fileName , "filename%d" , counter);
				sprintf(srcLeft  , "srcLeft%d"  , counter);
				sprintf(srcTop   , "srcTop%d"   , counter);
				sprintf(srcRight , "srcRight%d" , counter);
				sprintf(srcBottom, "srcBottom%d", counter);
				sprintf(dstLeft  , "dstLeft%d"  , counter);
				sprintf(dstTop   , "dstTop%d"   , counter);
				sprintf(dstRight , "dstRight%d" , counter);
				sprintf(dstBottom, "dstBottom%d", counter);

				SourceInfo src;

				// gets demType from cfg file
				const char* tmp = cmdIfc->QueryValue(demType, false);
				if (tmp == 0) break;
				src.demType = tmp;

				// checks demType
				if((src.demType != sDTED2) && (src.demType != sUSGSDEM) && (src.demType != sARCINFOASCII)  && (src.demType != sXYZ))
				{
					ExceptReg::RegExcpt(Exception(1, "Dem type not implemented."));
					return;
				}

				// gets filename from cfg file
				tmp = cmdIfc->QueryValue(fileName, false);
				if (tmp == 0) break;
				src.fileName = tmp;

				// checks file extension
				if(src.demType == sDTED2) 
				{
					string tmpFileName = src.fileName;
					transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

					if(!(tmpFileName.find(".DT2") == tmpFileName.length() - 4))
					{
						ExceptReg::RegExcpt(Exception(1, "The file is not a DTED2."));
						return;
					}
				}
				if(src.demType == sUSGSDEM) 
				{
					string tmpFileName = src.fileName;
					transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

					if(!(tmpFileName.find(".DEM") == tmpFileName.length() - 4))
					{
						ExceptReg::RegExcpt(Exception(2, "The file is not a USGS DEM."));
						return;
					}
				}
				if(src.demType == sARCINFOASCII) 
				{
					string tmpFileName = src.fileName;
					transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

					if((!(tmpFileName.find(".GRD") == tmpFileName.length() - 4)) && (!(tmpFileName.find(".ASC") == tmpFileName.length() - 4)))
					{
						ExceptReg::RegExcpt(Exception(2, "The file is not a Arc/Info ASCII Grid."));
						return;
					}
				}
				if(src.demType == sXYZ) 
				{
					string tmpFileName = src.fileName;
					transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

					if(!(tmpFileName.find(".XYZ") == tmpFileName.length() - 4))
					{
						ExceptReg::RegExcpt(Exception(56, ">>> The file is not a XYZ <<<"));
						return;
					}
				}

				// gets srcLeft from cfg file
				tmp = cmdIfc->QueryValue(srcLeft, false);
				if (tmp == 0 || sscanf(tmp, "%d", &src.srcLeft) != 1)
				{
					ExceptReg::RegExcpt(Exception(3, "missing 'srcLeft'"));
					return;
				}

				// gets srcRight from cfg file
				tmp = cmdIfc->QueryValue(srcRight, false);
				if (tmp == 0 || sscanf(tmp, "%d", &src.srcRight) != 1)
				{
					ExceptReg::RegExcpt(Exception(4, "missing 'srcRight'"));
					return;
				}

				// gets srcTop from cfg file
				tmp = cmdIfc->QueryValue(srcTop, false);
				if (tmp == 0 || sscanf(tmp, "%d", &src.srcTop) != 1)
				{
					ExceptReg::RegExcpt(Exception(5, "missing 'srcTop'"));
					return;
				}

				// gets srcBottom from cfg file
				tmp = cmdIfc->QueryValue(srcBottom, false);
				if (tmp == 0 || sscanf(tmp, "%d", &src.srcBottom) != 1)
				{
					ExceptReg::RegExcpt(Exception(6, "missing 'srcBottom'"));
					return;
				}

				// gets dstLeft from cfg file
				tmp = cmdIfc->QueryValue(dstLeft, false);
				if (tmp == 0 || sscanf(tmp, "%f", &src.dstLeft) != 1)
				{
					ExceptReg::RegExcpt(Exception(7, "missing 'dstLeft'"));
					return;
				}

				// gets dstRight from cfg file
				tmp = cmdIfc->QueryValue(dstRight, false);
				if (tmp == 0 || sscanf(tmp, "%f", &src.dstRight) != 1)
				{
					ExceptReg::RegExcpt(Exception(8, "missing 'dstRight'"));
					return;
				}

				// gets dstTop from cfg file
				tmp = cmdIfc->QueryValue(dstTop, false);
				if (tmp == 0 || sscanf(tmp, "%f", &src.dstTop) != 1)
				{
					ExceptReg::RegExcpt(Exception(9, "missing 'dstTop'"));
					return;
				}

				// gets dstBottom from cfg file
				tmp = cmdIfc->QueryValue(dstBottom, false);
				if (tmp == 0 || sscanf(tmp, "%f", &src.dstBottom) != 1)
				{
					ExceptReg::RegExcpt(Exception(10, "missing 'dstBottom'"));
					return;
				}

				m_Sources.Add(src);
				counter++;
			}
			while(true);   
		}

		bool HeightMapLoader::LoadSourcesHighMaps()
		{
			for(int i = 0; i < m_Sources.Size(); ++i)
			{
				EtRectElevationGrid<double, int> newHM;
				if(m_Sources[i].demType == sDTED2)
				{
					if(!(newHM.loadFromDT2(m_Sources[i].fileName)))
					{
						ExceptReg::RegExcpt(Exception(101, "error while loading DTED2 file"));
						return false;
					}
				}
				if(m_Sources[i].demType == sUSGSDEM)
				{
					if(!(newHM.loadFromUSGSDEM(m_Sources[i].fileName)))
					{
						ExceptReg::RegExcpt(Exception(101, "error while loading USGS DEM file"));
						return false;
					}
					else
					{
						// if the elevation units in the DEM are feet, converts to METERS
						if(newHM.getWorldUnitsW() == EtRectElevationGrid<double, int>::euFEET)
						{
							newHM.convertWorldUnitsW(EtRectElevationGrid<double, int>::euMETERS);
						}
					}
				}
				if(m_Sources[i].demType == sARCINFOASCII)
				{
					if(!(newHM.loadFromARCINFOASCIIGrid(m_Sources[i].fileName)))
					{
						ExceptReg::RegExcpt(Exception(102, "error while loading Arc/Info Ascii Grid file"));
						return false;
					}
				}
				if(m_Sources[i].demType == sXYZ)
				{
					if(!(newHM.loadFromXYZ(m_Sources[i].fileName)))
					{
						ExceptReg::RegExcpt(Exception(103, "error while loading xyz file"));
						return false;
					}
				}
				m_Sources[i].hmData = newHM;
			}
			return true;
		}

		void HeightMapLoader::Run(IMainCommands* cmdIfc)
		{
			// initializes const variables
			sDTED2        = "DTED2";
			sUSGSDEM      = "USGSDEM";
			sARCINFOASCII = "ARCINFOASCII";
			sXYZ          = "XYZ";

			// gets parameters from the calling cfg file
			GetSourcesParameters(cmdIfc);
			// gets sources High Maps
			LoadSourcesHighMaps();

			cmdIfc->GetHeightMap()->AddSamplerModule(new DemSampler(m_Sources));
		}
	}
}