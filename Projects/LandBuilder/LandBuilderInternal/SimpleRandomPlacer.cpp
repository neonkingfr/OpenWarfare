#include "StdAfx.h"
#include ".\simplerandomplacer.h"

#include "..\HighMapLoaders\include\EtMath.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------------------

		void SimpleRandomPlacer::Run(IMainCommands *cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();      

			// gets shape geometric data
			if(GetShapeGeoData())
			{
				// gets parameters from calling cfg file
				if(GetGlobalParameters(cmdIfc))
				{
					// gets dbf parameters from calling cfg file
					if(GetDbfParameters(cmdIfc))
					{
						// gets objects parameters from calling cfg file
						if(GetObjectsParameters(cmdIfc))
						{
							// initializes random seed
							srand(m_GlobalIn.randomSeed);

							// calculates the number of individual to be generated
							// (this value is referred to the bounding box of the shape
							//  and assume a uniform distribution - random generated values
							//  will be filtered to obtain the same density inside the shape,
							//  discarding the ones which will fall out of the shape itself)
							m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

							// sets the population
							CreateObjects(false);

							// export objects
							ExportCreatedObjects(cmdIfc);

							// writes the report
							WriteReport();
						}
					}
				}
			}       
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* SimpleRandomPlacer::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, 
																  SimpleRandomPlacer::ObjectInput& objectsIn)
		{
			m_pCurrentShape = shape;
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;
			m_ObjectsIn     = objectsIn;

			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// gets shape geometric data
			if(GetShapeGeoData())
			{
				// initializes random seed
				srand(m_GlobalIn.randomSeed);

				// calculates the number of individual to be generated
				// (this value is referred to the bounding box of the shape
				//  and assume a uniform distribution - random generated values
				//  will be filtered to obtain the same density inside the shape,
				//  discarding the ones which will fall out of the shape itself)
				m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

				// sets the population
				CreateObjects(true);

				return &m_ObjectsOut;
			}
			else
			{
				return NULL;
			}
		}

		// --------------------------------------------------------------------//

		bool SimpleRandomPlacer::GetShapeGeoData()
		{
			Shapes::DBox bBox = m_pCurrentShape->CalcBoundingBox();

			m_ShpGeo.minX = bBox.lo.x;
			m_ShpGeo.minY = bBox.lo.y;
			m_ShpGeo.maxX = bBox.hi.x;
			m_ShpGeo.maxY = bBox.hi.y;

			m_ShpGeo.area         = GetShapeArea(m_pCurrentShape);
			m_ShpGeo.areaHectares = m_ShpGeo.area / 10000.0;

			m_ShpGeo.widthBB        = m_ShpGeo.maxX - m_ShpGeo.minX;
			m_ShpGeo.heightBB       = m_ShpGeo.maxY - m_ShpGeo.minY;
			m_ShpGeo.areaBB         = m_ShpGeo.widthBB * m_ShpGeo.heightBB;
			m_ShpGeo.areaBBHectares = m_ShpGeo.areaBB / 10000.0;

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SimpleRandomPlacer::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing value for randomSeed <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SimpleRandomPlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			m_DbfIn.hectareDensity = 100.0; // default value
			const char* x = cmdIfc->QueryValue("hectareDensity", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.hectareDensity = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------- <<<\n");
					printf(">>> Bad data for hectareDensity - using default value (100) <<<\n");
					printf(">>> ------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------------ <<<\n");
				printf(">>> Missing value for hectareDensity - using default value (100) <<<\n");
				printf(">>> ------------------------------------------------------------ <<<\n");
			}
			if (m_DbfIn.hectareDensity < 0.01)
			{
				ExceptReg::RegExcpt(Exception(21, ">>> Density is too low (less then 0.01) <<<"));
				return false;
			}
			return true;
		}

		// --------------------------------------------------------------------------------

		bool SimpleRandomPlacer::GetObjectsParameters(IMainCommands* cmdIfc)
		{
			// resets arrays
			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			char object[50] = "object";

			// object type
			const char* x = cmdIfc->QueryValue(object, false);
			if (x == 0) return false;
			m_ObjectsIn.m_Name = x;
        
			m_ObjectsIn.m_Counter = 0;

			return true;
		}

		// --------------------------------------------------------------------//

		void SimpleRandomPlacer::CreateObjects(bool preview)
		{
			ProgressBar<int> pb(m_NumberOfIndividuals);
			
			for(int i = 0; i < m_NumberOfIndividuals; ++i)
			{
				if(!preview) pb.AdvanceNext(1);

				// random position
				double x0 = RandomInRangeF(0, m_ShpGeo.widthBB);
				double y0 = RandomInRangeF(0, m_ShpGeo.heightBB);
				Shapes::DVector v0 = Shapes::DVector(x0 + m_ShpGeo.minX, y0 + m_ShpGeo.minY);

				// the random point is inside the current shape ?
				if (m_pCurrentShape->PtInside(v0))
				{
					ModuleObjectOutput out;

					// sets data
					out.position = v0;
					out.type     = 0;
					out.rotation = RandomInRangeF(0, EtMathD::TWO_PI);
					out.scaleX = out.scaleY = out.scaleZ = 1.0;

					// used only by VLB for preview
					out.length = 10.0;
					out.width  = 10.0;
					out.height = 10.0;

					// adds to array
					m_ObjectsOut.Add(out);

					// update type counter
					m_ObjectsIn.m_Counter++;
				}
			}
		}

		// --------------------------------------------------------------------//

		void SimpleRandomPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for(int i = 0; i < objOutCount; ++i)
			{
				RString name      = m_ObjectsIn.m_Name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void SimpleRandomPlacer::WriteReport()
		{
			printf("Created                  \n");
			printf("%d", m_ObjectsIn.m_Counter);
			printf(" : ");
			printf(m_ObjectsIn.m_Name);
			printf("                           \n");
			printf("--------------------------------------\n");
		}
	}
}