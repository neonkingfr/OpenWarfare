#pragma once

namespace LandBuildInt
{
	class ObjectDesc
	{
		RStringI      _name;
		Matrix4       _position;
		unsigned long _tag;

	public:
		ObjectDesc(void) 
		{
		}

		// -------------------------------------------------------------------------- //

		ObjectDesc(const RStringI& name, const Matrix4& position, unsigned long tag)
		: _name(name)
		, _position(position)
		, _tag(tag) 
		{
		}

		// -------------------------------------------------------------------------- //

		const RStringI& GetName() const 
		{
			return _name;
		}

		// -------------------------------------------------------------------------- //

		const Matrix4& GetPositionMatrix() const 
		{
			return _position;
		}

		// -------------------------------------------------------------------------- //

	    const Vector3& GetPositionVector() const 
		{
			return _position.Position();
		}

		// -------------------------------------------------------------------------- //

		int IndexByName(const ObjectDesc& other) const
		{
			return _stricmp(_name, other._name);
		}

		// -------------------------------------------------------------------------- //

		int IndexByXPos(const ObjectDesc& other) const
		{
			return (_position.Position().X() > other._position.Position().X()) - (_position.Position().X() < other._position.Position().X());
		}

		// -------------------------------------------------------------------------- //

		int IndexByYPos(const ObjectDesc& other) const
		{
			return (_position.Position().Y() > other._position.Position().Y()) - (_position.Position().Y() < other._position.Position().Y());
		}

		// -------------------------------------------------------------------------- //

		int IndexByTag(const ObjectDesc& other) const
		{
			return (_tag > other._tag) - (_tag < other._tag);
		}

		// -------------------------------------------------------------------------- //
    
		unsigned long GetTag() const 
		{
			return _tag;
		}

		ClassIsMovable(ObjectDesc);
	};
}
