#pragma once

#include "ILandBuildCmds.h"
#include "IBuildModule.h"
#include "ObjectContainer.h"
#include "../shapes/ShapeOrganizer.h"
#include "../shapes/DBFReader.h"
#include "HighMapBase.h"

namespace LandBuildInt
{
	class IModuleFactory
	{
	public:
		virtual IBuildModule* GetModule(const char* name) = 0; 
	};
  
	class Core : public IMainCommands, public IObjectMap, public Shapes::IDBColumnNameConvert
	{
		Shapes::ShapeOrganizer  _shapes;
		HeightMapBase           _highMap;
		ObjectContainer         _objects;
		Shapes::ShapeList::Iter _curShape;
		FloatRect               _mapArea;
  
		struct ModuleItem
		{
			SRef<IBuildModule> _module;
			RString            _name;

			ModuleItem() 
			{
			}

			ModuleItem(const RString& name, const SRef<IBuildModule> module) 
			: _module(module)
			, _name(name) 
			{
			}

			int Compare(const ModuleItem& other) const 
			{
				return _stricmp(_name, other._name);
			}
		};

		struct RscItem
		{
			RString _origin;
			RString _replace;
			int     _type;

			RscItem(int type, const RString& origin, const RString& replace) 
			: _origin(origin)
			, _replace(replace)
			, _type(type) 
			{
			}

			RscItem() 
			{
			}

			int Compare(const RscItem& other) const 
			{
				if (_type == other._type)
				{
					return _stricmp(_origin, other._origin);
				}
				else 
				{
					return (_type > other._type) - (_type < other._type);
				}
			}
		};

		class BTreeCompare : public BTreeStaticCompare
		{
		public:
			template <class T>
			void UnsetItem(T& item)
			{
				item = T();
			}
		};

		BTree<ModuleItem, BTreeCompare> _modList;
		BTree<RscItem, BTreeCompare>    _rscList;

		IModuleFactory* _modFactory;

		float _ofpMapSegmentSizeX;
		float _ofpMapSegmentSizeY;

	public:
		Core(IModuleFactory* factory);
		~Core(void);

		void SetMapArea(const FloatRect& area) 
		{
			_mapArea = area;
			_highMap.SetMapArea(area);
		}

		virtual const char* QueryValue(const char* name, bool forceConstant) const;
		virtual const Shapes::IShape* GetActiveShape() const;

        virtual void* GetInterfacePtr(unsigned int ifcuid);
        
/*      
		virtual IHeightMap* GetHeightMap() 
		{
			return &_highMap;
		}

		virtual const IHeightMap* GetHeightMap() const 
		{
			return &_highMap;
		}

		virtual IObjectMap* GetObjectMap() 
		{
			return this;
		}

		virtual const IObjectMap* GetObjectMap() const 
		{
			return this;
		}
*/

		ObjectContainer* GetObjectContainer()
		{
			return &_objects;
		}

		const ObjectContainer* GetObjectContainer() const
		{
			return &_objects;
		}

		virtual const char* ConvertName(const char* alias) const;
		virtual const Shapes::IShape* GetObjectShape(const char* objectName) const;
		virtual Shapes::IShape* CreateObjectShapeTransformed(const char* objectName, const Matrix4& trns) const;
		virtual void DestroyShape(Shapes::IShape* shape);

		virtual FloatRect GetMapArea() const 
		{
			return _mapArea;
		}

		virtual unsigned int PlaceObject(const Matrix4& location, const char* objectName, unsigned int tag);
		virtual const Matrix4& GetObjectLocation(unsigned int objId);
		virtual const char* GetObjectType(unsigned int objId);
		virtual unsigned int GetObjectTag(unsigned int objId);
		virtual unsigned int FindNearestObject(float x, float y, unsigned int tag, const Array<unsigned int>& ignoreIDs);

		virtual Shapes::IShape *CreateObjectShape(unsigned int objId) 
		{
			return 0;
		}

		virtual bool EnumObjects(int& objId);
		virtual int ObjectsInArea(const FloatRect& area, unsigned int tag, const IEnumObjectCallback& callback);
		virtual int ObjectsInArea(const Shapes::IShape& shape, unsigned int tag, const IEnumObjectCallback& callback);
		virtual bool DeleteObject(unsigned int objId);
		virtual bool ReplaceObject(unsigned int objId, const Matrix4& location, const char* objectName, unsigned int tag);

		virtual const char* TranslateColumnName(const char* name) const;

		void FlushColumnTranslateTable();

		Shapes::ShapeOrganizer& GetShapeOrganizer() 
		{
			return _shapes;
		}

		const Shapes::ShapeOrganizer& GetShapeOrganizer() const 
		{
			return _shapes;
		}

		static const int typeUser = 0;
		static const int typeColumnName = 1;
		void AddResource(int type, RString origin, RString replacement);
		IBuildModule* GetModule(const char* name);

		void Run();
		bool ExportHeightMap(std::ostream& out);

		bool SaveResult(const char* target);
		bool SaveResult(std::ostream& out);

		void SetShapeFactory(Shapes::IShapeFactory* factory)
		{
			_shapes.SetShapeFactory(factory);
		}

		void SetOFPSegmentSize(float x, float y)
		{
			_ofpMapSegmentSizeX = x;
			_ofpMapSegmentSizeY = y;
		}
	};
}
