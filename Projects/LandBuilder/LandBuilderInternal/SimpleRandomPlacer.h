#pragma once

#include "IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

#include "..\Shapes\ShapeHelpers.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class SimpleRandomPlacer : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				unsigned int randomSeed;
			};

			struct DbfInput
			{
				double hectareDensity;
			};

			struct ObjectInput
			{
				RString m_Name;
				int     m_Counter;

				ObjectInput() {}
				ObjectInput(RString name)
				{
					m_Name        = name;
					m_Counter     = 0;
				}

				ClassIsMovable(ObjectInput);
			};

		private:
			struct ShapeGeo
			{
				double minX;
				double maxX;
				double minY;
				double maxY;
				double widthBB;        // bounding box width
				double heightBB;       // bounding box height
				double area;           // area of the shape (polygon)
				double areaHectares;   // area of the shape in hectares
				double areaBB;         // bounding box area
				double areaBBHectares; // bounding box area in hectares
			};

		public:
			virtual void Run(IMainCommands*);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, 
												  SimpleRandomPlacer::ObjectInput& objectsIn);

		public:
			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) : code(code), text(text) {}
				virtual unsigned int GetCode() const { return code; }
				virtual const _TCHAR* GetDesc() const { return text; }
				virtual const _TCHAR* GetModule() const { return "SimpleRandomPlacer"; }
				virtual const _TCHAR* GetType() const { return "SimpleRandomPlacerException"; }
			};

			const Shapes::IShape* m_pCurrentShape;      

			ShapeGeo m_ShpGeo;
			int      m_NumberOfIndividuals;

			ObjectInput             m_ObjectsIn;
			ModuleObjectOutputArray m_ObjectsOut;

			GlobalInput m_GlobalIn;
			DbfInput    m_DbfIn;

			bool GetShapeGeoData();
			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetObjectsParameters(IMainCommands* cmdIfc);

			void CreateObjects(bool preview);
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();
		};
	}
}

