#include "StdAfx.h"
#include ".\DeleteAlongPath.h"

#include "..\HighMapLoaders\include\EtHelperFunctions.h"
#include "..\HighMapLoaders\include\EtMath.h"
#include ".\ObjectContainer.h"

#include "..\Shapes\ShapePolygon.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------//

		void DeleteAlongPath::Run( IMainCommands* pCmdIfc )
		{
			m_deletionCounter = 0;

			// gets shape and its properties
			m_pCurrentShape = pCmdIfc->GetActiveShape();    

			// gets parameters from calling cfg file
			if ( GetDbfParameters( pCmdIfc ) )
			{
				// delete the objects
				DeleteObjects( pCmdIfc );

				// writes the report
				WriteReport();
			}
		}

		// --------------------------------------------------------------------//

		bool DeleteAlongPath::GetDbfParameters( IMainCommands* pCmdIfc )
		{
			// gets parameters from calling cfg file
			const char* x = pCmdIfc->QueryValue( "width", false );
			if ( x != 0 )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_dbfIn.width = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for width <<<" ) );
					return (false);
				}
			}
			else
			{        
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing width value <<<" ) );
				return (false);
			}

			return (true);
		}

		// --------------------------------------------------------------------//

		void DeleteAlongPath::DeleteObjects( IMainCommands* pCmdIfc )
		{
			int objCount = 0;
			int objID = -1;
			while ( pCmdIfc->GetObjectMap()->EnumObjects( objID ) )
			{
				objCount++;
				break;
			}

			if ( objCount > 0 )
			{
				int vCount = m_pCurrentShape->GetVertexCount();
				for ( int i = 0; i < vCount - 1; ++i )
				{
					const Shapes::DVertex v1 = m_pCurrentShape->GetVertex( i );
					const Shapes::DVertex v2 = m_pCurrentShape->GetVertex( i + 1 );

					Shapes::DVector v12( v2, v1 );
					double v12Length = v12.SizeXY();

					// goes on only if the segment is not a null segment
					if ( v12Length > 0.0 )
					{
						// extends the segment to avoid the formation of zones of non overlapping
						// between adjacents segments. otherwise objects in non-overlapping zones
						// will be not detected and deleted.
						double stretchFactor = (v12Length + m_dbfIn.width * 0.5) / v12Length;
						Shapes::DVector temp1( v2, v1 );
						temp1 *= stretchFactor;
						Shapes::DVertex newV2 = temp1.GetSecondEndXY( v1 );
						Shapes::DVector temp2( v1, v2 );
						temp2 *= stretchFactor;
						Shapes::DVertex newV1 = temp2.GetSecondEndXY( v2 );

						// now starts with the algorithm
						// first creates a rectangle around the segment
						Shapes::DVector v( newV2, newV1 );
						v.NormalizeXYInPlace();

						Shapes::DVector vPerpCW  = v.PerpXYCW();
						Shapes::DVector vPerpCCW = v.PerpXYCCW();

						vPerpCW.NormalizeXYInPlace();
						vPerpCCW.NormalizeXYInPlace();

						vPerpCW *= (m_dbfIn.width * 0.5);
						vPerpCCW *= (m_dbfIn.width * 0.5);

						Shapes::DVertex v1R = vPerpCW.GetSecondEndXY( newV1 );
						Shapes::DVertex v1L = vPerpCCW.GetSecondEndXY( newV1 );
						Shapes::DVertex v2R = vPerpCW.GetSecondEndXY( newV2 );
						Shapes::DVertex v2L = vPerpCCW.GetSecondEndXY( newV2 );

						unsigned int points[] = { 0, 1, 2, 3 };
						Array< unsigned int > arrPoints( points, 4 );
						unsigned int parts[] = { 0 };
						Array< unsigned int > arrParts( parts, 1 );

						Shapes::VertexArray vertices;
						vertices.AddVertex( v1R );
						vertices.AddVertex( v2R );
						vertices.AddVertex( v2L );
						vertices.AddVertex( v1L );
					
						Shapes::ShapePolygon p( arrPoints, arrParts, &vertices );

						// then, searches for objects inside the rectangle
						int objID = -1;
						while ( pCmdIfc->GetObjectMap()->EnumObjects( objID ) )
						{
							const Matrix4& matr = pCmdIfc->GetObjectMap()->GetObjectLocation( objID );
							const Vector3 objPos = matr.Position();

							Shapes::DVector tempV( objPos.X(), objPos.Z(), objPos.Y() );
							if ( p.PtInside( tempV ) )
							{
								pCmdIfc->GetObjectMap()->DeleteObject( objID );
								m_deletionCounter++;
							}
						}
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		void DeleteAlongPath::WriteReport()
		{
			printf( "Deleted %d objects.     \n",  m_deletionCounter );
			printf( "--------------------------------------\n" );
		}
	}
}

//-----------------------------------------------------------------------------
