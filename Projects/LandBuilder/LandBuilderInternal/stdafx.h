// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#include <float.h>
#include <iostream>
#include <fstream>
#include <strstream>
#include <tchar.h>
#include <es/strings/rstring.hpp>
#include <el/BTree/Btree.h>
#include <el/Interfaces/IMultiInterfaceBase.hpp>
#include <el/MultiThread/ExceptionRegister.h>
#include <el/Pathname/pathname.h>
#include <el/Math/math3d.hpp>
#include <el/progressBar/ProgressBar.h>
#include <es/Containers/rStringArray.hpp>
#include "../LandBuilder2/ProgressLog.h"
#include "../Shapes/IShape.h"


// TODO: reference additional headers your program requires here







