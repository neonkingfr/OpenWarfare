#include "StdAfx.h"
#include ".\GeoProjection.h"

#include "..\Shapes\IShape.h"

namespace LandBuildInt
{
	namespace Modules
	{
		void GeoProjection::Run(IMainCommands* cmdIfc)
		{
			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape(); 

			// gets parameters from calling cfg file
			if(GetGlobalParameters(cmdIfc))
			{
				Solve(cmdIfc);
			}
		}

		// --------------------------------------------------------------------//

		bool GeoProjection::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			// gets parameters from calling cfg file
			const char* x = cmdIfc->QueryValue("transfType", false);
			if (x != 0)
			{
				if(strcmp(x, "WGS84TOUTM") == 0)
				{
					m_GlobalIn.type = ProjTransformationCode_WGS84TOUTM;
				}
				else 
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Unknown transfType <<<"));
					return false;
				}
			}
			else
			{        
				ExceptReg::RegExcpt(Exception(12, ">>> No transfType specified <<<"));
				return false;
			}
			return true;
		}

		// --------------------------------------------------------------------//

		bool GeoProjection::Solve(IMainCommands* cmdIfc)
		{
			ProjTransformation<double> trans(m_GlobalIn.type);
			ProjTransformationReturnData<double> returnData;
			
			Shapes::VertexArray* vxarray = const_cast<Shapes::VertexArray*>(cmdIfc->GetInterface<IGlobVertexArray>()->GetVertexArray());

			int counter = 0;
			for(Shapes::VertexArray::Iter iter = vxarray->First(); iter; ++iter)
			{
				const Shapes::DVertex& vx = iter.GetVertex();
				ProjTransformationReturnValues res = trans.TransformDirect(EtMathD::DegToRad(vx.y), EtMathD::DegToRad(vx.x), returnData);
				if(res == PjTrRet_SUCCESS)
				{
					Shapes::DVertex nx(returnData.easting, returnData.northing);
					iter.SetVertex(nx);
					counter++;
				}
				else
				{
					printf(">>> --------------------------------------- <<<\n");
					printf(">>> Error (%d) while applying projection <<<\n", res);
					printf(">>> --------------------------------------- <<<\n");
					return false;
				}
			}
			return true;
		}
	}
}
