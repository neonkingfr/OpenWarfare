#pragma once
#include "ilandbuildcmds.h"
#include "../Shapes/ShapePolygon.h"
#include "IBuildModule.h"

namespace LandBuildInt
{
  class CountourLines : public IHeightMapSampler
  {
    Shapes::VertexArray vxArr;
    AutoArray<SRef<Shapes::ShapePolygon> > _clines;
    AutoArray<float> _heights;
  public:
    void AddShape(const Shapes::ShapePolygon &shape, float height, int segment=-1);
    void AddShapeAuto(const Shapes::ShapePolygon &shape,float baseHeight, float step, int segment=-1);

    virtual float GetHeight(const HeightMapLocInfo &heightInfo) const;

    virtual float CombineHeights(float original, float newheight) const {return newheight;}
  };

  class CountourLinesRelative: public CountourLines
  {
  public:
    virtual float CombineHeights(float original, float newheight) const {return original+newheight;}
  };

  class CountourLinesWeight: public CountourLines
  {
  protected:
    float _absHeight;
  public:
    CountourLinesWeight(float absHeight):_absHeight(absHeight) {}
    virtual float CombineHeights(float original, float newheight) const {return original+(_absHeight-original)*newheight;}
  };

  class CountourLinesWeightRelative: public CountourLinesWeight
  {
  public:
    CountourLinesWeightRelative(float relHeight):CountourLinesWeight(relHeight) {}
    virtual float CombineHeights(float original, float newheight) const {return original+_absHeight*newheight;}
  };


  namespace Modules
  {
    class CountourLines: public IBuildModule
    {
      virtual void Run(IMainCommands *);

      class Exception: public ExceptReg::IException
      {
        unsigned int code;
        const char *text;

      public:
        Exception(unsigned int code,const char *text):code(code),text(text) {}
        virtual unsigned int GetCode() const {return code;}
        virtual const _TCHAR *GetDesc() const {return text;}
        virtual const _TCHAR *GetModule() const {return "CountourLines";}
        virtual const _TCHAR *GetType() const {return "CountourLinesException";}
      };

    };
  }
}