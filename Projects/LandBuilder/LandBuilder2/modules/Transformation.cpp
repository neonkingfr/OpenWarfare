#include "StdAfx.h"
#include "Transformation.h"
#include "determinant.h"
#include "../ModuleRegistrator.h"
#include "../IShapeDBAccess.h"
#include "../IProjectTask.h"


using namespace ObjektivLib;

namespace LandBuilder2
{
	namespace Modules
	{

      double Transformation3pt::getNumber(const ShapeFile &shapeFile, const char *name)
      {
        const char *item = shapeFile(0,name);
        if (item == 0) throw Exception(-2,RString("Missing variable: ",name));
        float v;
        if (sscanf(item,"%f",&v) != 1) 
              throw Exception(-2,RString("Value must be a number: ",name));
        return v;
      }

      static bool Solve(double *vstupy, double *matice, double *vystupy)
      {
        double v[6];        
        double dc = Determinant(matice,6);
        if (fabs(dc)<1e-20) return false;
        for (int i = 0; i < 6; i++)
        {
          for (int j = 0;j<6;j++)
          {
            v[j] = matice[j*6+i];
            matice[j*6+i] = vstupy[j];
          }

          vystupy[i] = Determinant(matice,6) / dc;

          for (int j = 0;j<6;j++)
          {
            matice[j*6+i] = v[j];
          }
        }
        
        return true;
      }

      void Transformation3pt::OnBeginPass(IMainCommands* cmdIfc)
      {
        const IProjectTask *curtask = cmdIfc->GetCurrentTask();
        if (_processedTasks.find(curtask) !=  _processedTasks.end())
        {
            LOGF(Warn)<<"Transform: Skipping already processed task";
          return;
        }
        _processedTasks.insert(curtask);
        
        ShapeFile shapeFile(cmdIfc->GetInterface<IShapeDbAccess>());
        shapeFile.Open(curtask->GetShapeGroupName());
        if (shapeFile.Size()==0){
            LOGF(Error)<<"Transorm: No shapes in task, skipping";
            return;
        }
        const Shapes::IShape *curShape = shapeFile[0];

        Shapes::VertexArray *vxarray = const_cast<Shapes::VertexArray *>(curShape->GetVertexArray());

        Matrix4 transform;

/*        const char *mode = cmdIfc->QueryValue("mode");
        if (_stricmp(mode,"bbox") == 0)
        {
          Shapes::DBox box = curShape->CalcBoundingBox();
          FloatRect rect = cmdIfc->GetObjectMap()->GetMapArea();
          FloatRect shapeArea(box.lo.x,box.lo.y,box.hi.x,box.hi.y);
          transform = Matrix4P(MTranslation, Vector3(rect.left, rect.top, 0)) *
							 Matrix4P(MScale, (rect.right - rect.left) / (shapeArea.right - shapeArea.left), (rect.bottom - rect.top) / (shapeArea.bottom - shapeArea.top), 1) *
							 Matrix4P(MTranslation, Vector3(-shapeArea.left, -shapeArea.top, 0));    
        }
        else if (stricmp(mode,"3points") == 0)
        {*/
          if (curShape->GetVertexCount() != 3)
          {
			  ExceptReg::RegExcpt(Exception(1, "Source shape must have exactly 3 vertices"));
			  return;
          }

          double vstupy[6];
          double vystupy[6];
          double x1,y1,x2,y2,x3,y3;

          try
          {
            vstupy[0] = getNumber(shapeFile,"point1x");
            vstupy[1] = getNumber(shapeFile,"point1y");
            vstupy[2] = getNumber(shapeFile,"point2x");
            vstupy[3] = getNumber(shapeFile,"point2y");
            vstupy[4] = getNumber(shapeFile,"point3x");
            vstupy[5] = getNumber(shapeFile,"point3y");
            x1 = curShape->GetVertex(0).x;
            y1 = curShape->GetVertex(0).y;
            x2 = curShape->GetVertex(1).x;
            y2 = curShape->GetVertex(1).y;
            x3 = curShape->GetVertex(2).x;
            y3 = curShape->GetVertex(2).y;
          }
          catch(Exception e)
          {
            ExceptReg::RegExcpt(e);
            return;
          }
          double matice[36];
          /**
           x1' = x1*a11 + y1 * a12 + a13
           y1' = x1*a21 + y1 * a22 + a23
           x2' = x2*a11 + y2 * a12 + a13
           y2' = x2*a21 + y2 * a22 + a23
           x3' = x3*a11 + y3 * a12 + a13
           y3' = x3*a21 + y3 * a22 + a23

           */
          memset(matice,0,sizeof(matice));
          matice[0] = x1;
          matice[1] = y1;
          matice[2] = 1;
          matice[1*6+3] = x1;
          matice[1*6+4] = y1;
          matice[1*6+5] = 1;
          matice[2*6+0] = x2;
          matice[2*6+1] = y2;
          matice[2*6+2] = 1;
          matice[3*6+3] = x2;
          matice[3*6+4] = y2;
          matice[3*6+5] = 1;
          matice[4*6+0] = x3;
          matice[4*6+1] = y3;
          matice[4*6+2] = 1;
          matice[5*6+3] = x3;
          matice[5*6+4] = y3;
          matice[5*6+5] = 1;

          LOGF(Debug)<<"Solving transform equation";
          if (Solve(vstupy,matice, vystupy)==false)
          {
            ExceptReg::RegExcpt(Exception(2, "Cannot solve equation - more then one result found. "
              "Transformation cannot be performed."));
            return ;
          }


          transform = Matrix4P((float)vystupy[0],(float)vystupy[1],0,(float)vystupy[2],
                              (float)vystupy[3],(float)vystupy[4],0,(float)vystupy[5],
                              0,0,1,0);
          

 //       }
        LOGF(Debug)<<"Transforming vertices";
        TransformVertices(vxarray,transform);
      
      }

	  void Transformation3pt::TransformVertices(Shapes::VertexArray *vxarray, const Matrix4& a)
	  {
        for (Shapes::VertexArray::Iter iter = vxarray->First(); iter; ++iter)
		  {
			  const Shapes::DVertex& vx = iter.GetVertex();
			  Shapes::DVertex nx;
			  nx.x = a(0, 0) * vx.x + a(0,1) * vx.y + a(0, 2) * vx.z + a(0, 3);
			  nx.y = a(1, 0) * vx.x + a(1,1) * vx.y + a(1, 2) * vx.z + a(1, 3);
			  nx.z = a(2, 0) * vx.x + a(2,1) * vx.y + a(2, 2) * vx.z + a(2, 3);
			  iter.SetVertex(nx);
		  }
	  }


    RegisterModule<Modules::Transformation3pt> _register2("Transform3pt");

    }


}

