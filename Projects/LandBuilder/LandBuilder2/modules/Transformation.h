#pragma once

#include "../IBuildModule.h"
#include "../ILandBuildCmds.h"
#include <set>


namespace Shapes
{
  class VertexArray;
}

namespace LandBuilder2
{
    class ShapeFile;

    namespace Modules
	{

      class Transformation3pt :public IBuildModule
      {        

        std::set<const IProjectTask *> _processedTasks;

        double getNumber(const ShapeFile &shapeFile, const char *name);
      public:
        DECLAREMODULEEXCEPTION("Transformation");

        void Run(IMainCommands* cmdIfc) {}

        void OnBeginPass(IMainCommands* cmdIfc);
            

        static void TransformVertices(Shapes::VertexArray *vxarray, const Matrix4& a);

      };

      

    }
}