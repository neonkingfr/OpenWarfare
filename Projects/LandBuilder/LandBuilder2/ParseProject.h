//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "LBProject.h"
#include "ModuleManager.h"
#include <el/ParamFile/paramFile.hpp>
#include "LBWorkspace.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

  using namespace LandBuildInt;
  
//-----------------------------------------------------------------------------

  class ParseProject
  {
    friend class FillResourceConvertTable;

    ParamFile         m_config;
    ModuleManager     m_moduleManager;
    LBProject         m_project;
    SRef<LBWorkspace> m_workspace;

  protected:
    DoubleRect LoadRectangle( ParamClassPtr entry );

    void LoadTask( const Pathname& configName, const Pathname& shapeName, 
                   const Pathname& dbfName = Pathname( PathNull ) );

  public:
    ParseProject();
    ~ParseProject();

    bool LoadProject( const char* configName );      
    void RunProject();
    bool SaveResult( const char* targetName );

    void AddModulePaths( const std::set< RStringI >& path ) 
    {
      m_moduleManager.AddModulePaths( path );
    }       
  };

//-----------------------------------------------------------------------------

} // namespace LandBuilder2