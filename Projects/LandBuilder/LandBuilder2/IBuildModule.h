//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\ILandBuildCmds.h"
#include ".\IProjectTask.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{

//-----------------------------------------------------------------------------

  class ModuleException : public ExceptReg::IException
  {
    size_t      m_code;
    const char* m_text;    

  public:
    ModuleException( size_t code, const char* text ) 
    : m_code( code )
    , m_text( text ) 
    {
    }

    virtual ~ModuleException()
    {
    }

    virtual size_t GetCode() const 
    { 
      return (m_code); 
    }

    virtual const _TCHAR* GetDesc() const 
    { 
      return (m_text); 
    }

    virtual const _TCHAR* GetModule() const 
    { 
      return ("Module"); 
    }

    virtual const _TCHAR* GetType() const 
    { 
      return ("ModuleException"); 
    }
  };

//-----------------------------------------------------------------------------

#define DECLAREMODULEEXCEPTION( ModuleName ) class Exception : public ModuleException {\
public:\
  Exception( size_t code, const char* text ) : ModuleException( code, text ) {}\
  virtual const _TCHAR* GetModule() const { return (ModuleName); }\
  virtual const _TCHAR* GetModuleType() const { return (ModuleName "Exception"); }\
};

//-----------------------------------------------------------------------------

  class IBuildModule
  {
    // _ENRICO_
    // *******************************
  protected:
    RString m_currTaskName;
    // *******************************

  public:

    // _ENRICO_
    // *******************************
    IBuildModule() 
    : m_currTaskName( "" )
    {
    }
    // *******************************

    ///called on begin of pass during multipass processing
    virtual void OnBeginPass( IMainCommands* cmdIfc ) 
    {
    }

    ///called on begin of group
    virtual void OnBeginGroup( IMainCommands* cmdIfc ) 
    {
    }

    ///called on end of group
    virtual void OnEndGroup( IMainCommands* cmdIfc ) 
    {
    }

    ///called on end of pass
    virtual void OnEndPass( IMainCommands* cmdIfc ) 
    {
    }

    ///called for every shape
    virtual void Run( IMainCommands* cmdIfc ) = 0;

    ///called for destruction
    virtual void Release() 
    {
      delete this;
    }

    virtual ~IBuildModule() 
    {
    }
  };

//-----------------------------------------------------------------------------

} // namespace LandBuildInt
