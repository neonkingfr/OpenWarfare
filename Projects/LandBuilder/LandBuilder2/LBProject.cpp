//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "LBProject.h"
#include "IShapeDbAccess.h"
#include "IShapeExtra.h"

#include "..\Shapes\ShapePolyline.h"
#include "..\Shapes\ShapePolygon.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

  using LandBuildInt::IMainCommands;

//-----------------------------------------------------------------------------

  class LBProjectBuildHelp : public IMainCommands, 
                             public IShapeDbAccess,
                             public LBTaskQueue::ICombiner,
                             public IGlobVertexArray
  {
    LBProject*    m_outer;
    IProjectTask* m_curTask;
    int           m_curShapeId;
    LBWorkspace*  m_workspace;

    mutable AutoArray< size_t > m_allShapes;

  public:
    LBProjectBuildHelp( LBProject* outer, LBWorkspace* workspace )
    : m_outer( outer )
    , m_curTask( NULL )
    , m_curShapeId( -1 )
    , m_workspace( workspace ) 
    {
    }

    virtual IMainCommands* getCommandInterface( IProjectTask* task )
    {
      m_curTask = task;
      return (this);
    }

    virtual const char* QueryValue( const char* name, bool forceConstant = false ) const
    {
      const char* dbres;
      if ( (forceConstant || m_curShapeId == -1) || 
           (dbres = m_outer->m_shapes.GetShapeParameter( m_curShapeId, m_curTask->TranslateColumnName( name ) ) ) == 0 ) 
      {
        return (m_curTask->QueryValue( name ));
      }
      else
      {
        // if a parameter has the same name as a dbf field give precedence to it
        const char* data = m_curTask->QueryValue( name );
        if ( data )
        {
          return ( data );
        }
        else
        {
          return (dbres);
        }
      }
//      if ( (forceConstant || m_curShapeId == -1) || 
//           (dbres = m_outer->m_shapes.GetShapeParameter( m_curShapeId, m_curTask->TranslateColumnName( name ) ) ) == 0 ) 
//      {
//        return (m_curTask->QueryValue( name ));
//      }
//      else
//      {
//        return (dbres);
//      }
    }

    virtual const Shapes::IShape* GetActiveShape() const 
    {
      if ( m_curShapeId == -1 ) 
      {
        return (NULL);
      }
      else
      {
        return (m_outer->m_shapes.GetShapeList().GetShape( m_curShapeId ));
      }
    }

    virtual void* GetInterfacePtr( size_t ifcuid )
    {
      switch ( ifcuid )
      {
      case IRoadMap::IFCUID:         return (static_cast< IRoadMap* >( &m_workspace->m_roads ));
      case IObjectMap::IFCUID:       return (static_cast< IObjectMap* >( &m_workspace->m_objects ));
      case IHeightMap::IFCUID:       return (static_cast< IHeightMap* >( &m_workspace->m_highMap ));
      case IShapeDbAccess::IFCUID:   return (static_cast< IShapeDbAccess* >( this ));
      case IMainCommands::IFCUID:    return (static_cast< IMainCommands* >( this ));
      case IGlobVertexArray::IFCUID: return (static_cast< IGlobVertexArray* >( this ));
      default:                       return (IMainCommands::GetInterfacePtr( ifcuid ));
      }            
    }

    virtual const char* ConvertName( const char* alias ) const 
    {
      std::map< RStringI, RString >::const_iterator it = m_outer->m_resourceTranslate.find( RStringI( alias ) );

      if ( it == m_outer->m_resourceTranslate.end() )
      {
        return (alias);
      }
      else 
      {
        return (it->second);
      }
    }
    
    virtual bool AddToShapeGroup( const char* shapeGroupName, Shapes::IShape* shape,
                                  const Array< std::pair< RStringI, RString > >& parameters )
    {      
      Shapes::ShapeDatabase& db = m_outer->m_shapes.GetShapeDatabase();
      Shapes::ShapeList& slist  = m_outer->m_shapes.GetShapeList();
      Shapes::IShape* ref = slist.First().GetShape();

      if ( ref && ref->GetVertexArray() != shape->GetVertexArray() ) { return (false); }

      int shapeId = slist.AddShape( Shapes::PShape( shape ) );
      for ( int i = 0, cnt = parameters.Size(); i < cnt; ++i ) 
      {
        db.SetField( shapeId, parameters[i].first, parameters[i].second );
      }

      ShapefileDesc* desc = m_outer->CreateShapeGroup( shapeGroupName );
      desc->m_shapeIDs.Add( shapeId );

      return (true);
    }

    const IProjectTask* GetCurrentTask() const 
    {
      return (m_curTask);
    }

    bool EnqueueTask( LandBuilder2::IProjectTask* task, bool highpriority ) const
    {
      return (m_outer->EnqueueTask( task, highpriority ));
    }

    void SetActiveShapeId( int id ) 
    {
      m_curShapeId = id;
    }

    const Shapes::IShape* GetObjectShape( const char* objectName ) const 
    {
      return (NULL);
    }

    Shapes::IShape* CreateObjectShapeTransformed( const char* objectName, const Matrix4& trns ) const 
    {
      return (NULL);
    }

    AutoArray< size_t > GetShapefileIndexes( const char* name = NULL ) const
    {
      if ( !name )
      {
        name = m_curTask->GetShapeGroupName();
      }
      ShapefileDesc* desc = m_outer->GetShapeGroup( name );
      if ( !desc ) 
      {
        return (AutoArray< size_t >( 0 ));
      }
      else 
      {
        return (desc->m_shapeIDs);
      }
    }

    virtual const Shapes::IShape* GetShape( int index ) const
    {
      Shapes::ShapeOrganizer& shplist = m_outer->m_shapes;
      return (shplist.GetShapeList().GetShape( index ));
    }

    virtual int EnumShapefiles( const IShapefileEnumerator& enumerator, const char* prefix = NULL ) const
    {
      return (m_outer->EnumShapefiles( enumerator, prefix ));
    }

    const char* QueryValue( int shapeIndex, const char* name, bool directDb )
    {
      if ( directDb ) 
      { 
        return (m_outer->m_shapes.GetShapeParameter( shapeIndex, name ));
      }
      else
      {
        const char* dbres;
        if ( (dbres = m_outer->m_shapes.GetShapeParameter( m_curShapeId, m_curTask->TranslateColumnName( name ) ) ) == 0 )
        {
          return (m_curTask->QueryValue( name ));
        }
        else
        {
          return (dbres);
        }
      }
    }

    virtual LoadResult LoadShapeFile( const char* name, Shapes::IDBColumnNameConvert* dbconvert,
                                      const Pathname& DBFName )
    {
      Pathname fullname( name, Pathname::GetCWD() );
      if ( fullname.TestFile() == false ) { return (resShapeFileNotExists); }

      const char* groupname = fullname.GetFilename();

      AutoArray< size_t > shapeIndexes = GetShapefileIndexes( groupname );
      if ( shapeIndexes.Size() != 0 ) { return (resOk); }

      if ( m_outer->m_shapes.ReadShapeFile( fullname, 0, dbconvert, DBFName, &shapeIndexes ) == false ) 
      {
        return (resShapeLoadError);
      }

      ShapefileDesc* desc = m_outer->CreateShapeGroup( groupname );
      desc->m_shapeIDs.Append( shapeIndexes );
      return (resOk);
    }

    virtual unsigned int AllocVertex( const Shapes::DVertex& vertex )
    {
      return (m_outer->m_shapes.GetVertexArray().AddVertex( vertex ));
    }
        
    virtual bool FreeVertex( size_t index ) 
    {
      return (m_outer->m_shapes.GetVertexArray().DeleteVertex( index ));
    }
        
    virtual const Shapes::VertexArray* GetVertexArray() const
    {
      return (&m_outer->m_shapes.GetVertexArray());
    }

    virtual bool SetVertex( size_t index, const Shapes::DVertex& vertex )
    {
      return (m_outer->m_shapes.GetVertexArray().SetVertex( index, vertex ));
    }
  };

//-----------------------------------------------------------------------------

  bool LBProject::ProcessTask( IProjectTask* task, LBProjectBuildHelp& helper )
  {
    IBuildModule* taskModule = task->GetModule();
    const char* groupName = task->GetShapeGroupName();
    if ( !groupName || *groupName == 0 )
    {
      if ( helper.GetActiveShape()->IsEnabled() )
      {
        taskModule->OnBeginGroup( helper.getCommandInterface( task ) );
        taskModule->Run( &helper );
        taskModule->OnEndGroup( &helper );
      }
      return (true);
    } 
    else
    {
      ShapefileDesc findGroup( groupName );
      ShapefileDesc* foundGroup = m_groupSet.Find( findGroup );

      if ( foundGroup ) 
      {
        taskModule->OnBeginGroup( helper.getCommandInterface( task ) );
        ProgressBar< int > pb( foundGroup->m_shapeIDs.Size() );
        for ( int i = 0, cnt = foundGroup->m_shapeIDs.Size(); i < cnt; ++i )
        {
          pb.AdvanceNext( 1 );
          helper.SetActiveShapeId( foundGroup->m_shapeIDs[i] );
          if ( helper.GetActiveShape()->IsEnabled() )
          {
            taskModule->Run( &helper );
          }
        }
        helper.SetActiveShapeId( -1 );
        taskModule->OnEndGroup( &helper );

        return (true);
      }
      else
      {
        LOGF( Error ) << "Task " << task->GetTaskName() <<" requests operation on unknown shapefile: " 
                      << groupName ;
      }
      return (false);
    }    
  }

//-----------------------------------------------------------------------------

  void LBProject::RunProject( LBWorkspace* workspace )   
  {
    LBProjectBuildHelp helper( this, workspace );
    IProjectTask* lastInQueue = 0;
    int pass = 0;
    std::queue< IProjectTask* > transferedTasks;
    
    while ( m_queue.AnyTask() || !transferedTasks.empty() ) 
    {
      pass++;
      LOGF( Progress ) << "Preparing pass " << pass;
      m_queue.ForAllTasks( &IBuildModule::OnBeginPass, helper );      
      LOGF( Progress ) << "Starting pass " << pass;

      while ( !transferedTasks.empty() )
      {
        IProjectTask* task = transferedTasks.front();
        transferedTasks.pop();
        if ( CanRunThisTask( task, true ) )
        {
          LOGF( Progress ) << " ";
          LOGF( Progress ) << "Processing task " << task->GetTaskName();
          ProcessTask( task, helper );
        }
        else
        {
          LOGF( Error ) << "Can't run task '" << task->GetTaskName() << "'.";
        }
      }

      while ( m_queue.AnyTask() ) 
      {
        IProjectTask* task = m_queue.GetNextTask();
        if ( CanRunThisTask( task ) )
        {
          LOGF( Progress ) << " ";
          LOGF( Progress ) << "Processing task " << task->GetTaskName();
          ProcessTask( task, helper );
          lastInQueue = 0;
        }
        else
        {
          if ( lastInQueue != 0 && lastInQueue == task )
          {
            do
            {
              transferedTasks.push( task );
            }
            while ( (task = m_queue.GetNextTask()) );
          }
          else
          {
            if ( !lastInQueue ) { lastInQueue = task; }
            m_queue.EnqueueTask( task );
          }
        }
      }

      LOGF( Progress ) << " ";
      LOGF( Progress ) << "Finishing pass " << pass;
      m_queue.ForAllTasks( &IBuildModule::OnEndPass, helper );
    }
  }

//-----------------------------------------------------------------------------

  bool LBProject::EnqueueTask( LandBuilder2::IProjectTask* task, bool highpriority ) 
  {
    m_queue.AddTask( task );
    m_queue.EnqueueTask( task );
    return (true);
  }

//-----------------------------------------------------------------------------

  ShapefileDesc* LBProject::CreateShapeGroup( const char* groupName ) 
  {
    ShapefileDesc* desc = m_groupSet.Find( ShapefileDesc( groupName ) );
    if ( !desc ) 
    {
      m_groupSet.Add( ShapefileDesc( groupName ) );
      desc = m_groupSet.Find( ShapefileDesc( groupName ) );
    }
    return (desc);
  }

//-----------------------------------------------------------------------------

  ShapefileDesc* LBProject::GetShapeGroup( const char* groupName ) const
  {
    ShapefileDesc* desc = m_groupSet.Find( ShapefileDesc( groupName ) );
    return (desc);
  }

//-----------------------------------------------------------------------------

  LBProject::Result LBProject::AddShapeFile( const Pathname& name, const char* groupName,
                                             Shapes::IDBColumnNameConvert* dbconvert, 
                                             const Pathname& DBFName ) 
  {
    if ( name.TestFile() == false ) { return (resShapeFileNotExists); }

    AutoArray< size_t > shapeIndexes;

    if ( m_shapes.ReadShapeFile( name, 0, dbconvert, DBFName, &shapeIndexes ) == false ) 
    {
      return (resShapeLoadError);
    }

    Shapes::ShapeList& shapeList = m_shapes.GetShapeList();
    for ( int i = 0, cnt = shapeIndexes.Size(); i < cnt; ++i )
    {
		  Shapes::IShape* shape = shapeList.GetShape( shapeIndexes[i] );
      shape->SetGroupName( groupName );
    }

    ShapefileDesc* desc = CreateShapeGroup( groupName );    
    desc->m_shapeIDs.Append( shapeIndexes );
    return (resOk);
  }

//-----------------------------------------------------------------------------

  DoubleRect LBProject::GetShapeBoundingBox() const
  {
    Shapes::DBox bbox = m_shapes.CalcBoundingBox();
    return (DoubleRect( bbox.lo.x, bbox.lo.y, bbox.hi.x, bbox.hi.y ));
  }

//-----------------------------------------------------------------------------

	void LBProject::CutShapesIntoFrame( const DoubleRect& rect )
  {
    Shapes::ShapeList&     shapeList = m_shapes.GetShapeList();
    Shapes::VertexArray&   vxList    = m_shapes.GetVertexArray(); 
    Shapes::ShapeDatabase& database  = m_shapes.GetShapeDatabase();
		Shapes::ShapeDatabase& cp        = m_shapes.GetConstantParamDB();
    Shapes::IShapeFactory* factory   = m_shapes.GetShapeFactory();

    for ( size_t i = 0, cntI = shapeList.Size(); i < cntI; ++i )
    {
		  Shapes::IShape* currShape = shapeList.GetShape( i );
      RString shapeGroupName    = currShape->GetGroupName();
      vector< Shapes::IShape* > res = currShape->CutIntoFrame( vxList, rect.GetLeft(), rect.GetTop(), 
                                                               rect.GetRight(), rect.GetBottom() );

    	const IShapeExtra* extra = currShape->GetInterface< IShapeExtra >();
    	RString shapeType = extra->GetShapeTypeName();

      for ( size_t j = 0, cntJ = res.size(); j < cntJ; ++j )
      {
        size_t newID;
        
        if ( shapeType == SHAPE_NAME_POINT )
        {
          newID = shapeList.AddShape( Shapes::PShape( factory->CreatePoint( *(reinterpret_cast< Shapes::ShapePoint* >( res[j] )) ) ) );
        }
        else if ( shapeType == SHAPE_NAME_MULTIPOINT )
        {
          newID = shapeList.AddShape( Shapes::PShape( factory->CreateMultipoint( *(reinterpret_cast< Shapes::ShapeMultiPoint* >( res[j] )) ) ) );
        }
        else if ( shapeType == SHAPE_NAME_POLYLINE )
        {
          newID = shapeList.AddShape( Shapes::PShape( factory->CreatePolyLine( *(reinterpret_cast< Shapes::ShapePolyLine* >( res[j] )) ) ) );
        }        
        else if ( shapeType == SHAPE_NAME_POLYGON )
        {
          newID = shapeList.AddShape( Shapes::PShape( factory->CreatePolygon( *(reinterpret_cast< Shapes::ShapePolygon* >( res[j] )) ) ) );
        }

        BTreeIterator< Shapes::ShapeDatabase::DBItem > iter( database.m_database );
        iter.BeginFrom( Shapes::ShapeDatabase::DBItem( i ) );
        Shapes::ShapeDatabase::DBItem* found = NULL;
	  		while ( ((found = iter.Next()) != NULL) && (i == found->m_shapeId ) )
        {
   				database.SetField( newID, found->m_paramName, found->m_value );
        }

        BTreeIterator< Shapes::ShapeDatabase::DBItem > iterCp( cp.m_database );
        iterCp.BeginFrom( Shapes::ShapeDatabase::DBItem( i ) );
        found = NULL;
	  		while ( ((found = iterCp.Next()) != NULL) && (i == found->m_shapeId ) )
        {
  				cp.SetField( newID, found->m_paramName, found->m_value );
        }

        ShapefileDesc* desc = GetShapeGroup( shapeGroupName );
        desc->m_shapeIDs.Add( newID );
      }

      currShape->Disable();
    }
  }

//-----------------------------------------------------------------------------

  void LBProject::SetFlag( const RString& flag )
  {
    m_flags.insert( flag );
  }
  
//-----------------------------------------------------------------------------

  bool LBProject::CanRunThisTask( IProjectTask* task, bool report ) const
  {
    const char* groupName = task->GetShapeGroupName();
    Array< RString >* requestedFlags = task->GetFlagsList();
    if ( groupName && *groupName )
    {
      ShapefileDesc* foundGroup = this->GetShapeGroup( groupName );    
      if ( !foundGroup ) 
      {
        if ( report ) 
        {
          LOGF( Error ) << "Shapefile doesn't exists (task: '"
                        << task->GetTaskName() << "' shapefile: '" << groupName << "')";
        }
        return (false);
      }
    }

    if ( requestedFlags )
    {
      for ( int i = 0, cnt = requestedFlags->Size(); i < cnt; ++i )
      {
        const char* flagName = requestedFlags->operator[]( i );
        std::set< RString >::const_iterator it = m_flags.find( flagName );
        if ( it == m_flags.end() ) 
        {
          if ( report ) 
          {
            LOGF( Error ) << "Dependency error. Waiting on flag (task: '"
                          << task->GetTaskName() << "' flag: '" << flagName << "')";        
          }
          return (false);
        }
      }
    }
    return (true);
  }

//-----------------------------------------------------------------------------

  int LBProject::EnumShapefiles( const IShapefileEnumerator& enumerator, const char* prefix ) const
  {
    if ( !prefix ) { prefix = ""; }
    int prefixsz = strlen( prefix );
    ShapeGroupSetIter iter( m_groupSet );
    ShapefileDesc search( prefix );
    iter.BeginFrom( search );
    ShapefileDesc* x = NULL;
    while ( (x = iter.Next()) != 0 && _strnicmp( x->m_shapeFileName, prefix, prefixsz ) == 0 )
    {
      int res = enumerator( x->m_shapeFileName );
      if ( res ) { return (res); }
    }
    return (0);
  }

//-----------------------------------------------------------------------------

} // namespace LandBuilder2
