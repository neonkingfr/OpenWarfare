//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "ILandBuildCmds.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{

//-----------------------------------------------------------------------------

	class HeightMapBase : public IHeightMap
	{
	public:
		struct SamplerModule
		{
			const IHeightMapSampler* m_sampler;
			double                   m_resolution;
			DoubleRect               m_area;

			SamplerModule() 
			: m_sampler( NULL )
			, m_resolution( FLT_MAX ) 
			{
			}
      
			SamplerModule( const IHeightMapSampler* sampler, double resolution, const DoubleRect& area ) 
			: m_sampler( sampler )
			, m_resolution( resolution )
			, m_area( area ) 
			{
			}

			SamplerModule( const SamplerModule& other ) 
      : m_sampler( other.m_sampler )
			, m_resolution( other.m_resolution )
			, m_area( other.m_area )
			{
				const_cast< SamplerModule& >( other ).m_sampler = NULL;
			}

			SamplerModule& operator = ( const SamplerModule& other )
			{
				this->~SamplerModule();
				new (this) SamplerModule( other );
				return (*this);
			}

			~SamplerModule()
			{
        if ( m_sampler ) { m_sampler->Release(); }
			}

			ClassIsMovable( SamplerModule )
		};

	protected:
		AutoArray< SamplerModule > m_samplers;
		DoubleRect                 m_mapArea;
		double                     m_minRes;

	public:
		HeightMapBase();
		virtual ~HeightMapBase();
  
		virtual DoubleRect GetMapArea();
		void SetMapArea( const DoubleRect& mapSize );
		virtual void AddSamplerModule( const IHeightMapSampler* sampler, double resolution = 1.0, 
                                   const DoubleRect* area = NULL );
		virtual double GetHeight( double x, double y ) const;

		double GetRecomendedResolution() const 
		{
			return (m_minRes);
		}
	};

//-----------------------------------------------------------------------------

} // namespace LandBuildInt