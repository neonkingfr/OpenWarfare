//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include <windows.h>
#include "ProgressLog.h"
#include <fstream>
#include <string>

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

class LocalProgressSupport : public IProgressLogSupport
{
  int           m_level;
  std::ofstream m_logFile;

public:
  LocalProgressSupport()
  : m_level( 10 ) 
  {
	  TCHAR szEXEPath[2048];
	  GetModuleFileName ( NULL, szEXEPath, 2048 );
    string filename = string( szEXEPath );
    string::size_type pos = filename.find_last_of( '\\' );
    filename = filename.substr( 0, pos + 1 ) + "LB2.log";
    m_logFile.open( filename.c_str(), std::ios::out | std::ios::trunc );
  }

  virtual ~LocalProgressSupport()
  {
    if ( !m_logFile.fail() ) { m_logFile.close(); } 
  }

  virtual void ReportLineBeg( int level );
  virtual std::ostream& GetStream();
	virtual std::ostream& GetFileStream();
  virtual void ReportLineEnd( const char* function );
  virtual int GetCurLevel() const;
  virtual void SetCurLevel( int lev );
};

//-----------------------------------------------------------------------------

static LocalProgressSupport localLog;

//-----------------------------------------------------------------------------

IProgressLogSupport* ProgressLog::logSupport = &localLog;

//-----------------------------------------------------------------------------

static char* levelNames[] =
{
  "debug", 
  "info",
  "note",
  "progress",
  "warn",
  "atten",
  "error",
  "critical",
  "fatal"
};

//-----------------------------------------------------------------------------

static char levelShorts[] =
{
  'D', 'I', 'N', 'P', 'W', 'A', 'E', 'C', 'F'
};

//-----------------------------------------------------------------------------

const char* LevelText( ProgressLog::LogLevel lev )
{
  int lvid = lev / 10;
  return (levelNames[lev % 9]);
}

//-----------------------------------------------------------------------------

RString LevelShortText( int lev )
{
  RString res;
  char* buff = res.CreateBuffer(2);
  buff[0] = levelShorts[lev / 10];
  buff[1] = '0' + lev % 10;
  return (res);
}

//-----------------------------------------------------------------------------

void LocalProgressSupport::ReportLineBeg( int level )
{
  SYSTEMTIME tm;
  GetLocalTime( &tm );
  char asctm[32];
  sprintf( asctm, "%02d:%02d:%02d.%02d",
//        tm.wDay,tm.wMonth,tm.wYear%100,
           tm.wHour, tm.wMinute, tm.wSecond,tm.wMilliseconds / 10 );
  std::cerr << asctm << " " << LevelShortText( level ).Data() << " " << std::endl;    
}

//-----------------------------------------------------------------------------

void LocalProgressSupport::ReportLineEnd( const char* function )
{
//  std::cerr << " (" << function << ")" << std::endl;
  std::cerr << std::endl;
  m_logFile << std::endl;
}

//-----------------------------------------------------------------------------

std::ostream& LocalProgressSupport::GetStream()
{
  return (std::cerr);
}

//-----------------------------------------------------------------------------

std::ostream& LocalProgressSupport::GetFileStream()
{
  return (m_logFile);
}

//-----------------------------------------------------------------------------

int LocalProgressSupport::GetCurLevel() const
{
  return (m_level);
}

//-----------------------------------------------------------------------------

void LocalProgressSupport::SetCurLevel( int lev )
{
  m_level = lev;
}

//-----------------------------------------------------------------------------
