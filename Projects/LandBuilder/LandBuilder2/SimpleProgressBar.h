#pragma once

#define MAXREMAINTIME (999*3600)

class SimpleProgress: public ProgressBarFunctions
{
  DWORD lastUpdate;
  int cnt;
  DWORD sumEndTime;
  float lastMarkup;
  DWORD lastMarkupTime;
  DWORD startTime;
public:
  SimpleProgress() {startTime=lastUpdate=GetTickCount();cnt=0;lastMarkupTime=lastUpdate;lastMarkup=0;sumEndTime=300000;}  
  virtual bool ManualUpdate() {return true;}
  virtual void Update()
  {
    char rot[]="\\|/-";
    DWORD curTime=GetTickCount();
    if (curTime-lastUpdate>100)
    {
      lastUpdate=curTime;
      float result=ReadResult();
      if (result<lastMarkup && result<0.01) 
      {
        lastMarkup=0;
        lastMarkupTime=startTime=GetTickCount();        
        sumEndTime=1;
      }
      cnt=(cnt+1)&0x3;
      

      DWORD curRelTime=curTime-lastMarkupTime; 
      DWORD relMarkup=lastMarkupTime-startTime;
      float estimateProcessed=lastMarkup+(float)curRelTime/(sumEndTime/2-relMarkup)*(1-lastMarkup);
      if (estimateProcessed>=1.0f) estimateProcessed=0.99999f;
      float diff=(1-result)/(1-estimateProcessed);

      if (diff<0.8f || diff>1.2f)
      {
        DWORD processed=curTime-lastMarkupTime;
        float estimation=processed/(result-lastMarkup)*(1-result);
        float endTime=curTime+estimation-startTime;
      /*  float endTime2=(curTime-startTime)/result;
        float fact=result*2.0f-1.0f;
        fact=fabs(fact);
        DWORD endTimeF=toLargeInt(endTime*(fact)+endTime2*(1-fact));*/
        DWORD endTimeF=toLargeInt(endTime);
        lastMarkup=result;
        lastMarkupTime=curTime;
        sumEndTime=(sumEndTime-sumEndTime/2)+endTimeF;

      }      
      DWORD remainTime=(sumEndTime/2-curTime+startTime)/1000;
      if (remainTime>MAXREMAINTIME) remainTime=0;
      
      printf("%5.2f%%, %c  (%02d:%02d:%02d)  \r",result*100.0f,rot[cnt],remainTime/3600,(remainTime/60)%60,(remainTime%60));

    }
  }
};