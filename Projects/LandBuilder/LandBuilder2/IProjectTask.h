//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

namespace LandBuildInt 
{
  class IBuildModule;
};

//-----------------------------------------------------------------------------

namespace LandBuilder2 
{
//-----------------------------------------------------------------------------

	using namespace LandBuildInt;

//-----------------------------------------------------------------------------

	///SRef that calls release for deleting
	template < class Type >   
	struct SRefReleaseTraits  
	{
		static void Delete( Type* ptr ) 
		{
			ptr->Release();
		}
	};

//-----------------------------------------------------------------------------

    ///custom task build.
    /**
    Instance is associated with the task, but is opaque for LandBuilder2.
    Class and instance of this class can be used by build modules to store partial
    results.

    To use this class, declare any class that uses it. Create instance of that class
    and store pointer to this instance by IProjectTask::SetProjectBuildData.

    If you want to retrieve instance anytime later, use IProjectTask::GetProjectBuildData 
    with operator static_cast or dynamic_cast.

    You don't need to care about destruction of instance. When LandBuilder
    decides, that instance is no longer needed, it calls Release function, that
    if mostly defined as deletion of instance
    */
	class ITaskBuildData
	{
	public:
        ///releases the instance of this class
        /**
        Redefine it, when you need different method of destruction
        */
		virtual void Release() 
		{
			delete this;
		}
      
		virtual ~ITaskBuildData() 
		{
		}
	};

//-----------------------------------------------------------------------------

	///Represents one enqueued task
	class IProjectTask
	{
		mutable SRef< ITaskBuildData, SRefReleaseTraits<ITaskBuildData > > m_buildData;

	public:
		///retrieves build module
		virtual IBuildModule* GetModule() const = 0;

		///retrieves parameter from the task
		virtual const char* QueryValue( const char* name ) const = 0;

		///retrieves shape group name associated with this shape
		virtual const char* GetShapeGroupName() const = 0;

		///releases instance of this class
		virtual void Release() 
		{
			delete this;
		}

		virtual const char* TranslateColumnName( const char* colName ) const
		{
			return (colName);
		}
    
		///retrieves list of flags that must be set to start this task
		virtual Array< RString >* GetFlagsList() const 
		{
			return (NULL);
		}

		///retrieves custom build data assigned to the project
		ITaskBuildData* GetProjectBuildData() const 
		{
			return (m_buildData);
		}

		///Assignes custom build data to the project
		/**
		* @param data pointer to instance of the data class. Pointer must be
		* valid during lifetime of project instance, or until it is  reset to another
		* value. Everytime the pointer is changed or class is destroyed, function 
		* IProjectBuildData::Release() is called. Default behaviour of this function
		* is calling Delete on data instance
		*
		* Build Data can be used to store partial results with the project by the
		* build module.
        *
        * @note Method is marked as const, because settings the build data is
        *  not treat as change state of the object.
		*/
		void SetProjectBuildData( ITaskBuildData* data ) const
		{
			m_buildData = data;
		}

		///retrieves task name.
		/**
		* @retval text Human readable task name
		* @retval NULL Unnamed task
		*/
		virtual const char* GetTaskName() const = 0;
 
    ///Retrieves working folder
    /**
        Retrieves working folder in absolute form. Working folder
        is retrived during initialization and stored with the task. 
        Build module can use it to translate relative path that are specified
        in config or in database of shapefile into absolute form.             
    */
    virtual const char* GetWorkFolder() const = 0;

		virtual ~IProjectTask() 
		{
		}
	};

//-----------------------------------------------------------------------------

} // LandBuilder2