//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/Interfaces/IMultiInterfaceBase.hpp>

#include ".\math3dD.h"

//-----------------------------------------------------------------------------

namespace Shapes
{
  class IShape;
  struct DVertex;
  class VertexArray;
}

//-----------------------------------------------------------------------------

namespace LandBuilder2
{
  class IProjectTask;
  class LBCrossroadDefinition;
  class LBRoadDefinition;
  class LBRoadKeypart;
  class LBRoadPath;
}

//-----------------------------------------------------------------------------

namespace LandBuildInt
{

//-----------------------------------------------------------------------------

  class IHeightMap;
  class IObjectMap;

//-----------------------------------------------------------------------------

  class DoubleRect
  {
    double m_left;
    double m_top;
    double m_right;
    double m_bottom;

  public:
    DoubleRect() 
    {
    }
    
    DoubleRect( double left, double top, double right, double bottom )
    : m_left( left )
    , m_top( top )
    , m_right( right )
    , m_bottom( bottom ) 
    {
      CheckValues();
    }

    void SetLeft( double left )
    {
      m_left = left;
    }

    void SetRight( double right )
    {
      m_right = right;
    }

    void SetTop( double top )
    {
      m_top = top;
    }

    void SetBottom( double bottom )
    {
      m_bottom = bottom;
    }

    double GetLeft() const
    {
      return (m_left);
    }

    double GetRight() const
    {
      return (m_right);
    }

    double GetTop() const
    {
      return (m_top);
    }

    double GetBottom() const
    {
      return (m_bottom);
    }

    double Width() const
    {
      return (m_right - m_left);
    }

    double Height() const
    {
      return (m_top - m_bottom);
    }

  private:
    void CheckValues()
    {
      if ( m_left > m_right )
      {
        double temp = m_left;
        m_left      = m_right;
        m_right     = temp;
      }

      if ( m_bottom > m_top )
      {
        double temp = m_top;
        m_top       = m_bottom;
        m_bottom    = temp;
      }
    }
  };

//-----------------------------------------------------------------------------

  class HeightMapLocInfo
  {
    double m_x;   ///<x coordinate
    double m_y;   ///<y coordinate
    double m_alt; ///<altitude at coordinate
    double m_nx;  ///<x coord of the normal at coordinate
    double m_ny;  ///<y coord of the normal at coordinate
    double m_nz;  ///<z coord of the normal at coordinate

  public:
    HeightMapLocInfo( double x, double y, double alt, double nx = 0.0, double ny = 0.0, double nz = 0.0 )
    : m_x( x )
    , m_y( y )
    , m_alt( alt )
    , m_nx( nx )
    , m_ny( ny )
    , m_nz( nz )
    {
    }

    double GetX() const
    {
      return (m_x);
    }

    double GetY() const
    {
      return (m_y);
    }

    double GetAlt() const
    {
      return (m_alt);
    }

    double GetNx() const
    {
      return (m_nx);
    }

    double GetNy() const 
    {
      return (m_ny);
    }

    double GetNz() const
    {
      return (m_nz);
    }

    void SetAlt( double alt )
    {
      m_alt = alt;
    }
  };

//-----------------------------------------------------------------------------

  ///Sampler is the basic part of high map interface
  /**
  * The high map is generated through samplers. Sampler is module, that should calculate height
  * in given coordinates. When high map is generated, sampler is called for each position in
  * map and ask modules for high on given coordinates.
  *
  * To define high map, you have to register sampler into the IHeightMap interface. Samplers
  * are registered in order. Later samplers can modify terrain generaded by previous samplers. 
  * 
  */
  class IHeightMapSampler : public IMultiInterfaceBase
  {
  public:
    ///Starts sampler for given position
    /**
    * Function called for given position. Read position from the structure.
    * Function should return new height. 
    * @param heightInfo current height at position. 
    * @return new height
    * @note sampler doesn't need to support normal vectors. In this case, xn..zn contains zeroes
    */
    virtual double GetHeight( const HeightMapLocInfo& heightInfo ) const = 0;
    
    ///Called by HeightMap class in destructor
    virtual void Release() const 
    {
      delete this;
    }
    
    ///Virtual destructor
    virtual ~IHeightMapSampler() 
    {
    }
  };

//-----------------------------------------------------------------------------

  class IHeightMap : public IMultiInterfaceBase
  {
  public:
    DECLARE_IFCUIDA( 'H','g','t','M','a','p' );

    ///Returns active map area in meters
    virtual DoubleRect GetMapArea() = 0;

    ///Starts land sampler
    /**
    * Function calls IHigMapSampler for each position at land. Sampler can choose positions in order or
    * random. It depends on high map representation and optimalization
    * @param sampler instance of client sampler
    * @param resolution recommended resolution in meters. This parameter is hint for sampler. High values
    * can cause that sampler will not detect small details in the high map. Low values causes longer sampling.
    * Advanced samplers can dynamically change this value during sampling to optimize its work and find out 
    * maximum detail.
    * @param area defines area, where sampler should run. If 0 passed, sampler will use entire map.
    */
    virtual void AddSamplerModule( const IHeightMapSampler* sampler, double resolution = 1.0, 
                                   const DoubleRect* area = NULL ) = 0;

    ///Retrieves height on given point
    /**
    * @param x x-position  
    * @param y y-position
    * @return height in meters
    *
    * @note This is not single lookup function. Function must process call samplers that operates at
    * given point and recalculate the height
    */
    virtual double GetHeight( double x, double y ) const = 0;
        
    virtual ~IHeightMap() 
    {
    }
  };
    
//-----------------------------------------------------------------------------

  class IEnumObjectCallback
  {
  public:
    virtual int operator () ( size_t objId ) const = 0;
  };

//-----------------------------------------------------------------------------

  class IObjectMap : public IMultiInterfaceBase
  {
  public:
    DECLARE_IFCUIDA( 'O','b','j','M','a','p' );

    ///Places object into object map
    /**
      @param location Matrix specifies object location, orientation, etc. Hight coordinates are 
       relative to terain height

      @param objectName name of object at position

      @param tag user defined tag. It useful to create groups of object. You can use tag when searching an
      object or objects. Use tag equal zero means "no group", this object is not in group, and can be find
      only with no filtering.
    */
    virtual size_t PlaceObject( const Matrix4D& location, const char* objectName, size_t tag ) = 0;

    virtual const Matrix4D& GetObjectLocation( size_t objId ) = 0;

    virtual const char* GetObjectType( size_t objId ) = 0;
    
    virtual size_t GetObjectTag( size_t objId ) = 0;

    virtual size_t FindNearestObject( double x, double y, size_t tag, 
                                      const Array< size_t >& ignoreIDs ) = 0;

    virtual Shapes::IShape* CreateObjectShape( size_t objId ) = 0;

    ///Enumerates objects
    /**
    On the start, set objId to -1. Function returns true, if objId has been changed to next object id.
    When function returns false, all the objects has been enumerated.
    */
    virtual bool EnumObjects( int& objId ) = 0;

    ///
    virtual int ObjectsInArea( const DoubleRect& area, size_t tag, const IEnumObjectCallback& callback ) = 0;

    virtual int ObjectsInArea( const Shapes::IShape& shape, size_t tag, const IEnumObjectCallback& callback ) = 0;

    virtual bool DeleteObject( size_t objId ) = 0;

    virtual bool ReplaceObject( size_t objId, const Matrix4D& location, const char* objectName, 
                                size_t tag ) = 0;

    ///Returns active map area in meters
    virtual DoubleRect GetMapArea() const = 0;
  };

//-----------------------------------------------------------------------------

  class IRoadMap : public IMultiInterfaceBase
  {
  public:
    DECLARE_IFCUIDA( 'R','o','a','M','a','p' );

    virtual ~IRoadMap()
    {
    }

    virtual void AddCrossroadDefinition( const LandBuilder2::LBCrossroadDefinition& crossroadDefinition ) = 0;
    virtual void AddRoadDefinition( const LandBuilder2::LBRoadDefinition& roadDefinition ) = 0;
    virtual void AddRoadKeypart( const LandBuilder2::LBRoadKeypart& roadKeypart ) = 0;
    virtual void AddRoadPath( const LandBuilder2::LBRoadPath& roadPath ) = 0;
  };

//-----------------------------------------------------------------------------

  class IGlobVertexArray : public IMultiInterfaceBase
  {
  public:
    DECLARE_IFCUIDA( 'G','V','x','A','r','r' );

    virtual ~IGlobVertexArray()
    {
    }

    virtual size_t AllocVertex( const Shapes::DVertex& vertex ) = 0;
    virtual bool FreeVertex( size_t index) = 0;
    virtual const Shapes::VertexArray* GetVertexArray() const = 0;
    virtual bool SetVertex( size_t index, const Shapes::DVertex& vertex ) = 0;
  };

//-----------------------------------------------------------------------------

  class IMainCommands : public IMultiInterfaceBase
  {
  public:
    DECLARE_IFCUIDA( 'M','a','i','n','C','m' );

    virtual ~IMainCommands()
    {
    }

    ///queries value of database.
    /**
    @param name variable name
    @param forceConstant true to force get value from constant params. Normally if variable exists in database
    it taken from it. Otherwise, it takes from constant params. Set this to true to take variable from constant
    params.
    @retval pointer Pointer to value text
    @retval NULL Variable was not found (forward exception is raised)
    */
    virtual const char* QueryValue( const char* name, bool forceConstant = false ) const = 0;

    ///retrieves pointer to IShape interface of active shape
    /**
      Module can use active shape to find borders of active area.
      @return function should always return a valid pointer. 
    */
    virtual const Shapes::IShape* GetActiveShape() const = 0;

    ///retrieves pointer to IHeightMap interface
    IHeightMap* GetHeightMap() 
    {
      return (GetInterface< IHeightMap >());
    }

    ///retrieves pointer to IHeightMap interface
    const IHeightMap* GetHeightMap() const  
    {
      return (GetInterface< IHeightMap >());
    }

    ///retrieves pointer to IObjectMap interface
    IObjectMap* GetObjectMap() 
    {
      return (GetInterface< IObjectMap >());
    }

    ///retrieves pointer to IObjectMap interface
    const IObjectMap* GetObjectMap() const 
    {
      return (GetInterface< IObjectMap >());
    }

    ///retrieves pointer to IRoadMap interface
    IRoadMap* GetRoadMap() 
    {
      return (GetInterface< IRoadMap >());
    }

    ///retrieves pointer to IRoadMap interface
    const IRoadMap* GetRoadMap() const 
    {
      return (GetInterface< IRoadMap >());
    }

    ///Converts alias name to real object name
    /**
      Typical module doesn't work with real objects, but with aliases. Real object names are
      passed using the config.
      @param alias name of object alias
      @retval pointer real object name
      @retval 0 unknown alias (forward exception is raised)
    */
    virtual const char* ConvertName( const char* alias ) const = 0;

    virtual const Shapes::IShape* GetObjectShape( const char* objectName ) const = 0;

    ///Creates shape object that represents transformed object
    /**
      @return pointer to created shape object. When you don't need shape anymore, destroy it by DestroyShape function.
    */
    virtual Shapes::IShape* CreateObjectShapeTransformed( const char* objectName, const Matrix4& trns ) const = 0;

    ///Retrieves informations about active task
    /**
      Because one module can be used with multiple tasks, this function returns
      pointer to instance, that requests the module. You can use this pointer to
      optain additional informations about the task
    */
    virtual const LandBuilder2::IProjectTask* GetCurrentTask() const 
    {
      return (NULL);
    }

    ///Enqueues task into queue
    /**
      Any module can create the task and enqueue the task for seconds pass.
      @param task newly created instance of task. Instance will be destroyed during
        processing. Don't enqueu
      @param highpriority when true, task is enqueued at the top of queue. Othewise it
        is enqueued at the bottom of queue. 
      @retval true success
      @retval false failed, not supported
      @note Enqueueing task during OnBeginPass with highpriority causes, that
        tasks will be processed before any other tasks. With lowpriority causes, that
        task will be processed on after any other task. Enqueuing task during OnEndPass
        will cause start new pass.          
     */
    virtual bool EnqueueTask( LandBuilder2::IProjectTask* task, bool highpriority ) const 
    {
      return (false);
    }
  };

//-----------------------------------------------------------------------------

} // namespace LandBuildInt

//-----------------------------------------------------------------------------

namespace LandBuilder2
{
  using namespace LandBuildInt;
}

//-----------------------------------------------------------------------------
