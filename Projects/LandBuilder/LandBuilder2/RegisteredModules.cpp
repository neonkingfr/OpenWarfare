#include "StdAfx.h"
#include "ModuleRegistrator.h"

namespace LandBuilder2
{
	using namespace LandBuildInt;
}

//source contains list of registered modules. 
/**Don't add new modules here. Instead, register module using the registrator 
at place of module's definition
*/
#include "modules/NoneModule.h"
#include "modules/RoadPlacer.h"

namespace LandBuilder2
{
	RegisterModule<Modules::NoneModule>          _register_BI_001("none");
	RegisterModule<Modules::RoadPlacer>          _register_BI_002("RoadPlacer");
}