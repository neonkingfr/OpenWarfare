//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/BTree/BTree.h>

//-----------------------------------------------------------------------------

///assign name to group of shapes
struct ShapefileDesc
{
	///group shape name
	RStringI m_shapeFileName;

  ///shape ids
	AutoArray< size_t > m_shapeIDs;

	ShapefileDesc( const RStringI& shapeFileName, const AutoArray< size_t >& shapeIDs )
  : m_shapeFileName( shapeFileName )
	, m_shapeIDs( shapeIDs )
	{
	}

	ShapefileDesc( const RStringI& shapeFileName )
  : m_shapeFileName( shapeFileName )
  { 
	}

	ShapefileDesc() 
	{
	}

	int Compare( const ShapefileDesc& other ) const 
	{
		return (_stricmp( m_shapeFileName, other.m_shapeFileName ));
	}
};

//-----------------------------------------------------------------------------

typedef BTree< ShapefileDesc, BTreeStaticCompareDefUnset >         ShapeGroupSet;
typedef BTreeIterator< ShapefileDesc, BTreeStaticCompareDefUnset > ShapeGroupSetIter;

//-----------------------------------------------------------------------------
