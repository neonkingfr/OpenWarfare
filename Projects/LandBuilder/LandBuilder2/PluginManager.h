//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <map>
#include <set>
#include "IBuildModule.h"
#include "../plugins/IPlugin.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	typedef std::pair< IPlugin*, void* > PluginInfo;

//-----------------------------------------------------------------------------

	class PluginManager
	{
		std::map< RStringI, RStringI >   m_modules;
		std::map< RStringI, PluginInfo > m_loadedDLLs;
    std::set< RStringI >             m_paths;
		bool                            m_scanned;

		LBGlobalPointers m_globParams;

		static void free2( void* ptr, std::size_t sz ) 
		{
			free( ptr );
		}

	public:
		PluginManager() 
    : m_scanned( false ) 
		{
			m_globParams.exceptionHandler   = ExceptReg::ExceptionRegister::GetCurrent();
			m_globParams.progressBarHandler = ProgressBarFunctions::GetGlobalProgressBarInstance();
			m_globParams.logObject          = ProgressLog::logSupport;
			m_globParams.mallocFunct        = &malloc;
			m_globParams.freeFunct          = &free2;
		}

		~PluginManager();

		PluginInfo LoadDll( const char* dllName );
    
		static void UnloadDll( PluginInfo& plugin );

		void ScanDll( const char* dllName );
		void RegisterModule( RStringI module, RString dll );

    IBuildModule* CreateModule( const RStringI& name, const char* params, const char* cwd );

		void MakeScanning();
		void SaveList() const;
		void ScanFolder( const char* folder );
		void LoadList();
    void AddModulePaths( const std::set< RStringI >& path );
    
		template< class Functor >
		void EnumModules( const Functor& fn ) const
		{
			for ( std::map< RStringI, RStringI >::const_iterator iter = m_modules.begin(); iter != m_modules.end(); ++iter )
			{
				fn( iter->first );
			}
		}        
	};
}

//-----------------------------------------------------------------------------
