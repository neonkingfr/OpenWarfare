//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "LBObjectMap.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

  size_t LBObjectMap::PlaceObject( const Matrix4D& location, const char* objectName, size_t tag )
  {
    if ( !location.IsFinite() ) 
    {
      LOGF( Warn ) << "Object '" << objectName << "' tag " << tag 
                   << " was not placed, because location is not finite";
      return (-1);
    }
    else
    {
      return (m_objects.Add( LandBuildInt::ObjectDesc( objectName, location, 0 ) ));
    }
  }

//-----------------------------------------------------------------------------

  const Matrix4D& LBObjectMap::GetObjectLocation( size_t objId )
  {
    return (m_objects[objId].GetPositionMatrix());
  }

//-----------------------------------------------------------------------------

  const char* LBObjectMap::GetObjectType( size_t objId )
  {
    return (m_objects[objId].GetName());
  }

//-----------------------------------------------------------------------------

  size_t LBObjectMap::GetObjectTag( size_t objId )
  {
    return (m_objects[objId].GetTag());
  }

//-----------------------------------------------------------------------------

  size_t LBObjectMap::FindNearestObject( double x, double y, size_t tag, const Array< size_t >& ignoreIDs )
  {
    const ObjectDesc* obj = m_objects.Nearest( x, y, tag, ignoreIDs );
    return (m_objects.GetObjectID( obj ));
  }

//-----------------------------------------------------------------------------

  bool LBObjectMap::EnumObjects( int& objId )
  {
    do
    {
      objId++;
      if ( objId < 0 || objId >= m_objects.Size() ) 
      {
        objId = -1;
        return (false);
      }
    }
    while ( !m_objects.IsValidObjectID( objId ) );
    return (true);
  }

//-----------------------------------------------------------------------------

  int LBObjectMap::ObjectsInArea( const DoubleRect& area, size_t tag, const IEnumObjectCallback& callback )
  {
    SRef< AutoArray< const ObjectDesc* > > objs = m_objects.ObjectsInBox( Shapes::DBox( Shapes::DVertex( area.GetLeft(), area.GetTop() ), Shapes::DVertex( area.GetRight(), area.GetBottom() ) ) );
    for ( int i = 0, cnt = objs->Size(); i < cnt; ++i ) 
    {
      int res = callback( m_objects.GetObjectID( objs->operator[]( i ) ) );
      if ( res != 0 ) { return (res); }
    }
    return (0);
  }

//-----------------------------------------------------------------------------

  int LBObjectMap::ObjectsInArea( const Shapes::IShape& shape, size_t tag, const IEnumObjectCallback& callback )
  {
    SRef< AutoArray< const ObjectDesc* > > objs = m_objects.ObjectsInBox( shape.CalcBoundingBox() );
    for ( int i = 0, cnt = objs->Size(); i < cnt; ++i ) 
    {
      const Vector3D& vx = objs->operator []( i )->GetPositionVector();
      if ( shape.PtInside( Shapes::DVector( vx.X(), vx.Y() ) ) )
      {
        int res = callback( m_objects.GetObjectID( objs->operator[]( i ) ) );
        if ( res != 0 ) { return (res); }
      }
    }
    return 0;
  }

//-----------------------------------------------------------------------------

  bool LBObjectMap::DeleteObject( size_t objId )
  {
    return (m_objects.Delete( objId ));
  }

//-----------------------------------------------------------------------------

  bool LBObjectMap::ReplaceObject( size_t objId, const Matrix4D& location, const char* objectName, size_t tag )
  {
    return (m_objects.Replace( objId, ObjectDesc( objectName, location, tag ) ));
  }

//-----------------------------------------------------------------------------

} // namespace LandBuilder2
