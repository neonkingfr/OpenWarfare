//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/Interfaces/IMultiInterfaceBase.hpp>

//-----------------------------------------------------------------------------

class IShapeExtra
{
public:
	DECLARE_IFCUIDA( 'S', 'h', 'p', 'E', 'x', ' ' );
	virtual Shapes::DVector NearestEdge( const Shapes::DVector& vx ) const = 0;
	virtual const char* GetShapeTypeName() const = 0;
};

//-----------------------------------------------------------------------------

#define SHAPE_NAME_POINT      "point"
#define SHAPE_NAME_MULTIPOINT "multipoint"
#define SHAPE_NAME_POLYLINE   "polyline"
#define SHAPE_NAME_POLYGON    "polygon"

//-----------------------------------------------------------------------------
