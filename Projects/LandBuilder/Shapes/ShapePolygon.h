//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\shapepoly.h"

#include "..\CG\CG_Polygon2.h"

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

  class IShapePolygonTest
  {
  public:
    DECLARE_IFCUIDA( 'S', 'P', 'o', 'l', 'g', 'T' );

    virtual bool IntersectionPoints( double yPos, Array< double >& sortedX ) = 0;
  };

//-----------------------------------------------------------------------------

  class ShapePolygon : public ShapePoly, public IShapePolygonTest
  {    
    typedef CG_Point2< double >   CGPoint2;
    typedef CG_Polygon2< double > CGPolygon2;

  public:
    ShapePolygon( const Array< size_t >& points = Array< size_t >( 0, 0 ),
                  const Array< size_t >& parts  = Array< size_t >( 0 ),
                  VertexArray* vx = NULL ) 
    : ShapePoly( points, parts, vx ) 
    {
    }

    ShapePolygon( const ShapePoly& other ) 
    : ShapePoly( other )
    {
    }

    virtual ~ShapePolygon()
    {
    }

  protected:
    static int AboveBelow( const DVector& b, double yline )
    {
      if ( b.y > yline ) { return (1); }
      if ( b.y < yline ) { return (-1); }
      return (0);
    }

  public:
    virtual bool PtInside( const DVector& vx ) const
    {
      bool inside = false;
      for ( size_t i = 0, cnt = GetPartCount(); i < cnt; ++i )
      {
        size_t pindex = GetPartIndex( i );
        size_t psize  = GetPartSize( i );
        DVector a = GetVertex( pindex + psize - 1 );
        int state = AboveBelow( a, vx.y );
        int counter = 0;
        for ( size_t j = 0; j < psize; ++j )
        {
          const DVector& b = GetVertex( j + pindex );
          int st1 = AboveBelow( b, vx.y );
          if ( state == 0 ) 
          {
            st1 = state;
          }
          else if ( st1 != 0 )
          {
            if ( state != st1 && fabs( b.y - a.y ) > 0.00000000001 )
            {
              double xp = (vx.y - a.y) / (b.y - a.y) * (b.x - a.x) + a.x;
              if ( xp > vx.x ) { counter++; }
              state = st1;
            }
          }
          a = b;
        }
        if ( counter & 1 ) { inside = !inside; }
      }
      return (inside);
    }

    virtual DVector NearestInside( const DVector& vx ) const
    {
      if ( PtInside( vx ) ) 
      {
        return (vx);
      }
      else
      {
        return (NearestToEdge( vx, true ));
      }
    }

    virtual void* GetInterfacePtr( size_t ifcuid )
    {
      if ( ifcuid == IShapePolygonTest::IFCUID ) 
      {
        return (static_cast< IShapePolygonTest* >( this ));
      }
      else 
      {
        return (ShapePoly::GetInterfacePtr( ifcuid ));
      }
    }

    virtual bool IntersectionPoints( double yPos, Array< double >& sortedX )
    {
      double* lst = sortedX.Data();
      int maxlst = sortedX.Size();
      int curlst = 0;

      for ( size_t i = 0, cnt = GetPartCount(); i < cnt; ++i )
      {
        size_t pindex = GetPartIndex( i );
        size_t psize  = GetPartSize( i );
        DVector a = GetVertex( pindex + psize - 1 );
        int state = AboveBelow( a, yPos );
        for ( size_t j = 0; j < psize; ++j )
        {
          const DVector& b = GetVertex( j + pindex );
          int st1 = AboveBelow( b, yPos );
          if ( state == 0 ) 
          {
            st1 = state;
          }
          else if ( st1 != 0 )
          {
            double xp = (yPos - a.y) / (b.y - a.y) * (b.x - a.x) + a.x;
            if ( curlst == maxlst ) { return (false); }

            int i = curlst;
            while ( i > 0 && lst[i - 1] > xp )
            {
              lst[i] = lst[i - 1];
              i--;
            }
            lst[i] = xp;
            curlst++;

            state = st1;
          }
          a = b;
        }
      }
      sortedX = Array< double >( lst, curlst );
      return (true);
    }

    virtual ShapePolygon* NewInstance( const ShapePoly& other ) const
    {
      return (NewInstance( ShapePolygon( other ) ));
    }

    ///Creates copy of the shape
    /**
    * @copydoc ShapeMultiPoint::NewInstance 
    */
    virtual ShapePolygon* NewInstance( const ShapePolygon& other ) const
    {
      return (new ShapePolygon( other ));
    }

    ///Function helps to calculating clipping of the shape
    /**
    * @copydoc ShapePoly::ClipHelp     
    */
    void ClipHelp( double xn, double yn, double pos, VertexArray* vxArr, 
                   AutoArray< size_t >& newParts, 
                   AutoArray< size_t >& newPoints ) const
    {
      ShapePoly::ClipHelp( xn, yn, pos, vxArr, newParts, newPoints );
      const DVertex& v1 = GetVertex( 0 );
      int sid1 = Side( xn, yn, pos, v1.x, v1.y );
      const DVertex& v2 = GetVertex( GetVertexCount() - 1 );
      int sid2 = Side( xn, yn, pos, v2.x, v2.y );
      if ( sid1 != 0 && sid2 != 0 && sid1 != sid2 )
      {
        DVertex cp = this->ClipPoint( v2, v1, xn, yn, pos );
        if ( sid2 < 0 )
        {
          newParts.Add( newPoints.Size() );
        }
        newPoints.Add( vxArr->AddVertex( cp ) );
        sid2 = 0;
      }
      if ( sid1 >= 0 && sid2 >= 0 && newParts.Size() > 1 )
      {
        for ( size_t i = 0; i < newParts[1]; ++i )
        {
          size_t p = newPoints[i];
          newPoints.Add( p );
        }
        newPoints.Delete( 0, newParts[1] );
        newParts.Delete( 0, 1 );
        size_t offset = newParts[0];
        for ( int i = 0, cnt = newParts.Size(); i < cnt; ++i )
        {
          newParts[i] -= offset;
        }
      }
    }

    virtual IShape* Clip( double xn, double yn, double pos, VertexArray* vxArr = NULL ) const
    {
      AutoArray< size_t > newParts;
      AutoArray< size_t > newPoints;
      if ( !vxArr ) { vxArr = m_vertices; }

      //if there is more then one part
      if ( GetPartCount() > 1 )
      {
        AutoArray< size_t > curParts;
        AutoArray< size_t > curPoints;
        for ( size_t i = 0, cntI = GetPartCount(); i < cntI; ++i )
        {
          curParts.Clear();
          curPoints.Clear();
          size_t onepart = 0;
          ShapePolygon simplePartPoly( Array< size_t >( const_cast< size_t* >( m_points.Data()) + GetPartIndex( i ), GetPartSize( i ) ),
                                       Array< size_t >( &onepart, 1 ), m_vertices );
          simplePartPoly.ClipHelp( xn, yn, pos, vxArr, curParts, curPoints );   
          size_t offset = newPoints.Size();
          newPoints.Append( curPoints );
          for ( int j = 0, cntJ = curParts.Size(); j < cntJ; ++j ) 
          {
            newParts.Add( curParts[j] + offset ); 
          }
        }
      }
      else
      {
        ClipHelp( xn, yn, pos, vxArr, newParts, newPoints );
      }

      if ( newPoints.Size() == 0 ) { return (NULL); }

      return ( NewInstance( ShapePolygon( newPoints, newParts, vxArr ) ));
    } 

    virtual vector< IShape* > CutIntoFrame( VertexArray& vxArray, double left, double top, 
                                            double right, double bottom ) const
    {
      // WARNING
      // this method currently works for non multipart polygon shapes only

      vector< IShape* > ret;
  
      CGPolygon2 clipPoly;
      clipPoly.AppendVertex( left, bottom );
      clipPoly.AppendVertex( left, top );
      clipPoly.AppendVertex( right, top );
      clipPoly.AppendVertex( right, bottom );

      for ( size_t i = 0, cntI = GetPartCount(); i < cntI; ++i )
      {
        CGPolygon2 poly;

        size_t pindex = GetPartIndex( i );
        size_t psize  = GetPartSize( i );

        for ( size_t j = 0; j < psize; ++j )
        {
          const DVector& v = GetVertex( j + pindex );
          poly.AppendVertex( v.x, v.y );
        }

        vector< CGPolygon2 > resPolies = poly.Clip( clipPoly );

        for ( size_t j = 0, cntJ = resPolies.size(); j < cntJ; ++j )
        {
          const CGPolygon2& jPoly = resPolies[j];
          size_t vCount = jPoly.VerticesCount();

          if ( vCount > 2 )
          {
            AutoArray< size_t > vxlist;
            AutoArray< size_t > partlist;
            vxlist.Resize( vCount );
            partlist.Resize( 1 );
            partlist[0] = 0;

            for ( size_t k = 0; k < vCount; ++k )
            {
              const CGPoint2& v = jPoly.Vertex( k );

              vxlist[k] = vxArray.AddVertex( DVertex( v.GetX(), v.GetY() ) );
            }

            ret.push_back( new ShapePolygon( vxlist, partlist, &vxArray ) );
          }
        }
      }

      return (ret);
    }

    ///retrieves shape perimeter
    /**
    @return shape perimeter
    */
    virtual double Perimeter() const 
    {
      return (ShapePoly::Perimeter( true ));
    }

    ///retrieves shape's area
    /**
    @return shape area
    */
    virtual double Area() const
    {
      double area = 0.0;

      for ( size_t i = 0, cntI = GetPartCount(); i < cntI; ++i )
      {
        size_t start = GetPartIndex( i );
        size_t count = GetPartSize( i );
        for ( size_t j = 0, cntJ = count - 1; j < cntJ; ++j )
        {
          Shapes::DVertex v1 = GetVertex( j + start );
          Shapes::DVertex v2 = GetVertex( i + start + 1 );          

          // calculates area (gauss algorithm)
          area += v1.x * v2.y - v1.y * v2.x;
        }
      }
      return (fabs( area ) * 0.5);
    }

    virtual IShape* RemoveVertices( const Array< size_t >& pointsToRemove, VertexArray* vxArr ) const
    {
      if ( !vxArr ) { vxArr = m_vertices; }

      AutoArray< size_t > newParts;
      AutoArray< size_t > newPoints;
      newParts = m_parts;

      for ( size_t i = 0, srcCnt = m_points.Size(); i < srcCnt; ++i )
      {
        bool found = false;
        for ( size_t j = 0, remCnt = pointsToRemove.Size(); j < remCnt; ++j )
        {
          if ( pointsToRemove[j] == i )
          {
            found = true;
            for ( size_t k = 0, partCnt = newParts.Size(); k < partCnt; ++k )
            {
              if ( newParts[k] >= i )
              {
                newParts[k]--;
              }
            }
            break;
          }
        }
        if ( !found )
        {
          newPoints.Add( vxArr->AddVertex( GetVertex( i ) ) );
        }
      }
      return (NewInstance( ShapePolygon( newPoints, newParts, vxArr ) ));
    }
  };

//-----------------------------------------------------------------------------

} // namespace Shapes
