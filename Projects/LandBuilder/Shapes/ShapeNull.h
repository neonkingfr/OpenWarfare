//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "ishape.h"

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	class ShapeNull : public IShape
	{
	public:
		ShapeNull( VertexArray* v )
    : IShape( v ) 
    {
    }

		virtual size_t GetVertexCount() const 
    {
      return (0);
    }

		virtual size_t GetPartCount() const 
    {
      return (0);
    }

		virtual size_t GetIndex( size_t pos ) const 
    {
      return (0);
    }

		const DVertex& GetVertex( size_t pos ) const 
    {
      return (DVertex::GetInvalidVertex());
    }

		virtual size_t GetPartIndex( size_t partId ) const 
    {
      return (0);
    }

		virtual size_t GetPartSize( size_t partId ) const 
    {
      return (0);
    }

		virtual bool PtInside( const DVector& vx) const 
    {
      return (false);
    }

		virtual DVector NearestInside( const DVector& vx ) const 
    {
      return (vx);
    }

		virtual IShape* Clip( double xn, double yn, double pos, VertexArray* vxArr = NULL ) const 
    {
      return (NULL);
    }

		virtual IShape* Copy( VertexArray* vxArr = NULL ) const 
    {
      return (NULL);
    }

		IShape* Crop( double left, double top, double right, double bottom, VertexArray* vxArr = NULL ) const 
    {
      return (NULL);
    }

    virtual vector< IShape* > CutIntoFrame( VertexArray& vxArray, double left, double top, double right, double bottom ) const
    {
      return (vector< IShape* >());
    }

		virtual double Perimeter() const 
    {
      return (0.0);
    }

		virtual double Area() const 
    {
      return (0.0);
    }

		Shapes::IShape* ExtractPart( size_t p ) const 
    {
      return (NULL);
    }
	};

//-----------------------------------------------------------------------------

} // namespace Shapes
