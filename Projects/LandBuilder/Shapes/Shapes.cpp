// Shapes.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ShapeOrganizer.h"
#include "ShapeOrganizerHTMLDump.h"

using namespace Shapes;


int _tmain(int argc, _TCHAR* argv[])
{
  ShapeOrganizerHTMLDump dmp;
  ShapeOrganizer shapeOrg(&dmp);
  unsigned int cpID=shapeOrg.AllocateNewConstantParamsID("null");
  shapeOrg.ReadShapeFile(Pathname("C:\\VCProjects\\BIS\\Projects\\LandBuilder\\test\\bredy_les.shp"),cpID);
  dmp.DumpToHTML(shapeOrg,Pathname("t:\\dump.html"));
	return 0;
}

