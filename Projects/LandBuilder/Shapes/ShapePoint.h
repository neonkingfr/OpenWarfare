//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "ShapeBase.h"

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	///Shape that represents one point
	/**
	* This is the simplest shape. It represents one point. The shape has always one vertex 
	* position and always has one part.
	*/
	class ShapePoint : public ShapeBase
	{
		size_t m_index;

	public:
		ShapePoint( size_t index = 0, VertexArray* vertices = NULL )
    : ShapeBase( vertices ) 
		, m_index( index )
		{
		}
    
		virtual size_t GetVertexCount() const 
		{
			return (1);
		}

		virtual size_t GetPartCount() const 
		{
			return (1);
		}

		virtual size_t GetIndex( size_t pos ) const 
		{
			return (m_index);
		}

		virtual size_t GetPartIndex( size_t partId ) const 
		{
			return (0);
		}

		virtual size_t GetPartSize( size_t partId ) const 
		{
			return (1);
		}

		///Function tests, whether the specified point is inside of shape
		/**
		* @copydoc IShape::PtInside
		* @note Function returns true, when vx is exactly the same as point returned by GetVertex(0) with epsilon difference.
		*/
		virtual bool PtInside( const DVector& v ) const
		{
			return (GetVertex( 0 ).EqualXY( v ));
		}

		virtual DVector NearestInside( const DVector& v ) const
		{
			return (GetVertex( 0 ));
		}

		virtual IShape* Clip( double xn, double yn, double pos, VertexArray* vxArr = NULL ) const
		{
			const DVertex& v = GetVertex( 0 );      
			if ( Side( xn, yn, pos, v.x, v.y ) < 0 ) 
			{
				return (NULL);
			}
			else 
			{
				return (Copy( vxArr ));
			}
		}

		///Creates copy of the point
		/**
		* Derived class can reimplement this function to 
		* handle allocation by itself.
		* @return pointer to new instance. 
		*/
		virtual ShapePoint* NewInstance( const ShapePoint& orig ) const
		{
			return (new ShapePoint( orig ));
		}

		virtual ShapePoint* Copy( VertexArray* vxArr = NULL ) const
		{
			if ( !vxArr ) 
			{
				return (NewInstance( ShapePoint( m_index, m_vertices ) ));
			}
			else 
			{
				return (NewInstance( ShapePoint( vxArr->AddVertex( GetVertex( 0 ) ), vxArr ) ));
			}
		}

    virtual vector< IShape* > CutIntoFrame( VertexArray& vxArray, double left, double top, 
                                            double right, double bottom ) const
    {
      vector< IShape* > ret;

			const DVertex& v = GetVertex( 0 ); 

      if ( (left <= v.x && v.x <= right) &&
           (bottom <= v.y && v.y <= top) )
      {
        ret.push_back( new ShapePoint( m_index, m_vertices ) );
      }

      return (ret);
    }

		///retrieves shape perimeter
		/**
		@return shape perimeter
		*/
		virtual double Perimeter() const 
		{
			return (0.0);
		}

		///retrieves shape's area
		/**
		@return shape area
		*/
		virtual double Area() const 
		{
			return (0.0);
		}

		virtual ShapePoint* ExtractPart( size_t part ) const 
		{
      if ( part > 0 ) { return (NULL); }
			return (Copy());
		}
	};

//-----------------------------------------------------------------------------

} // namespace Shapes