//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "shapepoly.h"

#include "..\CG\CG_Polygon2.h"
#include "..\CG\CG_Polyline2.h"

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

  static const double ShapePolyLine_thinLineWidth = 0.0000000001;

//-----------------------------------------------------------------------------

  class ShapePolyLine : public ShapePoly
  {
    typedef CG_Point2< double >    CGPoint2;
    typedef CG_Polygon2< double >  CGPolygon2;
    typedef CG_Polyline2< double > CGPolyline2;

  public:
    ShapePolyLine( const Array< size_t >& points = Array< size_t>( 0, 0 ),
                   const Array< size_t >& parts  = Array< size_t >( 0 ), VertexArray* vx = NULL ) 
    : ShapePoly( points, parts, vx ) 
    {
    }

    ShapePolyLine( const ShapePoly& other ) 
    : ShapePoly( other )
    {
    }

    virtual ~ShapePolyLine()
    {
    }

    virtual DVector NearestInside( const DVector& vx ) const
    {
      return (NearestToEdge( vx, false ));
    }

    virtual bool PtInside( const DVector& vx ) const
    {
      DVector res = NearestInside( vx );
      return ((vx.x - res.x) * (vx.x - res.x) + (vx.x - res.y) * (vx.x - res.y) < (ShapePolyLine_thinLineWidth * ShapePolyLine_thinLineWidth));
    }

    virtual IShape* Clip( double xn, double yn, double pos, VertexArray* vxArr = NULL ) const
    {
      if ( !vxArr ) { vxArr = m_vertices; }
      AutoArray< size_t > newParts;
      AutoArray< size_t > newPoints;
      ClipHelp( xn, yn, pos, vxArr, newParts, newPoints );
      if ( newPoints.Size() == 0 ) 
      {
        return (NULL);
      }
      else 
      {
        return (NewInstance( ShapePolyLine( newPoints, newParts, vxArr ) ));
      }
    }

    virtual ShapePolyLine* NewInstance( const ShapePoly& other ) const
    {
      return (NewInstance( ShapePolyLine( other ) ));
    }

    ///Creates copy of the shape
    /**
    * @copydoc ShapeMultiPoint::NewInstance 
    */
    virtual ShapePolyLine* NewInstance( const ShapePolyLine& other ) const
    {
      return (new ShapePolyLine( other ));
    }

    virtual vector< IShape* > CutIntoFrame( VertexArray& vxArray, double left, double top, 
                                            double right, double bottom ) const
    {
      vector< IShape* > ret;

      CGPolygon2 clipPoly;
      clipPoly.AppendVertex( left, bottom );
      clipPoly.AppendVertex( left, top );
      clipPoly.AppendVertex( right, top );
      clipPoly.AppendVertex( right, bottom );

      for ( size_t i = 0, cntI = GetPartCount(); i < cntI; ++i )
      {
        CGPolyline2 poly;

        size_t pindex = GetPartIndex( i );
        size_t psize  = GetPartSize( i );

        for ( size_t j = 0; j < psize; ++j )
        {
          const DVector& v = GetVertex( j + pindex );
          poly.AppendVertex( v.x, v.y );
        }

        vector< CGPolyline2 > resPolies = poly.Clip( clipPoly );

        for ( size_t j = 0, cntJ = resPolies.size(); j < cntJ; ++j )
        {
          const CGPolyline2& jPoly = resPolies[j];
          size_t vCount = jPoly.VerticesCount();

          if ( vCount > 1 )
          {
            AutoArray< size_t > vxlist;
            AutoArray< size_t > partlist;
            vxlist.Resize( vCount );
            partlist.Resize( 1 );
            partlist[0] = 0;

            for ( size_t k = 0; k < vCount; ++k )
            {
              const CGPoint2& v = jPoly.Vertex( k );
              vxlist[k] = vxArray.AddVertex( DVertex( v.GetX(), v.GetY() ) );
            }

            ret.push_back( new ShapePolyLine( vxlist, partlist, &vxArray ) );
          }
        }
      }

      return (ret);
    }

    ///retrieves shape perimeter
    /**
    @return shape perimeter
    */
    virtual double Perimeter() const 
    {
      return (ShapePoly::Perimeter( false ));
    }
  };

//-----------------------------------------------------------------------------

} // namespace Shapes
