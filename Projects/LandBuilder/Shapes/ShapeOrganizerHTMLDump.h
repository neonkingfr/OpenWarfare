#pragma once
#include "shapeorganizer.h"

namespace Shapes
{

  class ShapeOrganizerHTMLDump: public IShapeFactory
  {
  public:
    void DumpToHTML(const ShapeOrganizer &db, const Pathname &filename);

    virtual IShape *CreatePoint(const ShapePoint &origShape);
    virtual IShape *CreateMultipoint(const ShapeMultiPoint &origShape);
    virtual IShape *CreatePolyLine(const ShapePoly &origShape);
    virtual IShape *CreatePolygon(const ShapePoly &origShape);

  };
}
