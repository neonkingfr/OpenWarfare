//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\vertex.h"

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	size_t VertexArray::GetNextFreeAtPos( size_t pos ) const
	{
		Assert( pos < Size() && (m_flags[pos] & fUsed) == 0 );
		const double& p = m_v[pos].x;
		const size_t& n = reinterpret_cast< const size_t& >( p );
		return (n);
	}

//-----------------------------------------------------------------------------

	size_t VertexArray::GetPrevFreeAtPos( size_t pos ) const
	{
		Assert( pos < Size() && (m_flags[pos] & fUsed) == 0 );
		const double& p = m_v[pos].x;
		const size_t& n = reinterpret_cast< const size_t& >( p );
		return (n);
  }

//-----------------------------------------------------------------------------

	void VertexArray::SetNextFreeAtPos( size_t pos, size_t nextFree )
	{
		Assert( pos < Size() && (m_flags[pos] & fUsed) == 0 );

		{    
			double& p = m_v[pos].x;
			size_t& n = reinterpret_cast< size_t& >( p );
			n = nextFree;    
		}

		{
			double& p = m_v[pos].y;
			size_t& n = reinterpret_cast< size_t& >( p );
			n = -1;
		}

		if ( nextFree < Size() )
		{
			double& p = m_v[nextFree].y;
			size_t& n = reinterpret_cast< size_t& >( p );
			n = pos;
		}
	}

//-----------------------------------------------------------------------------

	size_t VertexArray::AllocVertex()
	{
		if ( m_nextFree < Size() )
		{
			size_t nn = m_nextFree;
			m_nextFree = GetNextFreeAtPos( m_nextFree );
      if ( m_nextFree > Size() ) { m_nextFree = Size(); }
			m_flags[nn] = fUsed;
			return (nn);
		}
		else
		{
      if ( m_nextFree > Size() ) { m_nextFree = Size(); }
			m_v.Access( m_nextFree );
			m_flags.Access( m_nextFree );
			m_flags[m_nextFree] = fUsed;
			return (m_nextFree++);
		}
	}

//-----------------------------------------------------------------------------

	void VertexArray::FreeVertex( size_t pos )
	{
		Assert( pos < Size() );
		if ( pos + 1 == Size() )
		{
			m_v.Resize( pos );
			m_flags.Resize( pos );
      if ( m_nextFree > Size() ) { m_nextFree = Size(); }
			while ( Size() > 0 && m_flags[Size() - 1] == 0 )
			{
				size_t k = Size() - 1;
				size_t prev = GetPrevFreeAtPos( k );
				size_t next = GetNextFreeAtPos( k );
        if ( prev != -1 /*?????*/) { SetNextFreeAtPos( prev, next ); }
        if ( m_nextFree == k ) { m_nextFree = next; }
        if ( m_nextFree > k )  { m_nextFree = k; }
				m_v.Resize( k );
				m_flags.Resize( k );
			}
		}
		else
		{
			Assert( (m_flags[pos] & fUsed) != 0 );
			m_flags[pos] = 0;
			SetNextFreeAtPos( pos, m_nextFree );
			m_nextFree = pos;
		}
	}

//-----------------------------------------------------------------------------

	size_t VertexArray::AddVertex( const DVertex& vx )
	{
		size_t vxp = AllocVertex();
		SetVertex( vxp, vx );
		return (vxp);
	}

//-----------------------------------------------------------------------------

	bool VertexArray::DeleteVertex( size_t id )
	{
		if ( id < Size() && (m_flags[id] & fUsed) != 0 )
		{
			FreeVertex( id );
			return (true);
		}
		else
		{
			return (false);
		}
	}

//-----------------------------------------------------------------------------

	static DVertex SNullVertex;

//-----------------------------------------------------------------------------

	bool DVertex::IsInvalidVertex() const
	{
		return (&SNullVertex == this);
	}

//-----------------------------------------------------------------------------

	const DVertex& DVertex::GetInvalidVertex()
	{
		return (SNullVertex);
	}

//-----------------------------------------------------------------------------

	const DVertex& VertexArray::GetVertex( size_t pos ) const
	{
		if ( this && pos < Size() && (m_flags[pos] & fUsed) != 0 )
		{
			return (m_v[pos]);
		}
		else
		{
			return (SNullVertex);
		}
	}

//-----------------------------------------------------------------------------

	bool VertexArray::SetVertex( size_t pos, const DVertex& vx )
	{
		if ( this && pos < Size() && (m_flags[pos] & fUsed) != 0 )
		{
			m_v[pos] = vx;
			return (true);
		}
		else
		{
			return (false);
		}
	}

//-----------------------------------------------------------------------------

  VertexArray::Iter VertexArray::First()
  {
    for ( size_t i = 0, cnt = Size(); i < cnt; ++i )
    {
      if ( m_flags[i] & fUsed ) { return (Iter( this, i )); }
    }
    return (Iter( this, -1 ));
  }

//-----------------------------------------------------------------------------

  VertexArray::IterC VertexArray::First() const
  {
    for ( size_t i = 0, cnt = Size(); i < cnt; ++i )
    {
      if ( m_flags[i] & fUsed ) { return (IterC( this, i )); }
    }
    return (IterC( this, -1 ));
  }

//-----------------------------------------------------------------------------

  VertexArray::Iter VertexArray::Last()
  {
    for ( int i = Size() - 1; i >= 0; --i )
    {
      if ( m_flags[i] & fUsed ) { return (Iter( this, i )); }
    }
    return (Iter( this, -1 ));
  }

//-----------------------------------------------------------------------------

  VertexArray::IterC VertexArray::Last() const
  {
    for ( int i = Size() - 1; i >= 0; --i )
    {
      if ( m_flags[i] & fUsed ) { return (IterC( this, i )); }
    }
    return (IterC( this, -1 ));
  }

//-----------------------------------------------------------------------------

  void VertexArray::NextVertex( size_t& vxPos ) const
  {
    if ( vxPos == -1 ) /*?????*/ { return; }
    for ( size_t i = vxPos + 1, cnt = Size(); i < cnt; ++i )
    {
      if ( m_flags[i] & fUsed ) 
      {
        vxPos = i;
        return;
      }
    }
    vxPos = -1; /*?????*/
  }

//-----------------------------------------------------------------------------

  void VertexArray::PrevVertex( size_t& vxPos ) const
  {
    if ( vxPos == -1 ) /*?????*/ { return; }
    if ( vxPos >= Size() ) { vxPos = Size(); }
    for ( int i = vxPos - 1; i >= 0; --i )
    {
      if ( m_flags[i] & fUsed ) 
      {
        vxPos = i;
        return;
      }
    }
    vxPos = -1; /*?????*/
  }

//-----------------------------------------------------------------------------

} // namespace Shapes
