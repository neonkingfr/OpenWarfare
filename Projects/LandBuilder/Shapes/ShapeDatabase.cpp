//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\shapedatabase.h"

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	RString ShapeDatabase::ShareString( const RString& text )
	{
		RString* nw = m_stringTable.Find( text );

    if ( nw )
		{
			return (*nw);
		}
		else
		{
			m_stringTable.Add( text );
			return (text);
		}
	}

//-----------------------------------------------------------------------------

	RString ShapeDatabase::ShareString( const RString& text ) const
	{
		RString* nw = m_stringTable.Find( text );

    if ( nw ) 
		{
			return (*nw);
		}
		else 
		{
			return (text);
		}
	}

//-----------------------------------------------------------------------------

  bool ShapeDatabase::SetField( size_t shapeId, const char* fieldName, const char* value )
	{
		RString field = fieldName;
		field.Upper();
		field = ShareString( field );
		RString v = value;
		v = ShareString( v );

		DBItem* i = m_database.Find( DBItem( shapeId, field ) );

		if ( i ) 
		{
			i->m_value = v;
			return (true);
		}
		else
		{
			return (m_database.Add( DBItem( shapeId, field, v ) ));
		}
	}

//-----------------------------------------------------------------------------

	const char* ShapeDatabase::GetField( size_t shapeId, const char* fieldName ) const
	{
		RString field = fieldName;
		field.Upper();
		field = ShareString( field );

		const DBItem* found = m_database.Find( DBItem( shapeId, field ) );

		if ( found ) 
		{
			return (found->m_value);
		}
		else 
		{
			return (NULL);
		}
	}

//-----------------------------------------------------------------------------

	bool ShapeDatabase::DeleteShape( size_t shapeId )
	{
		bool res = false;
		BTreeIterator< DBItem > iter( m_database );
		DBItem* found;
		iter.BeginFrom( DBItem( shapeId ) );
		while ( (found = iter.Next()) != 0 && found->m_shapeId == shapeId )
		{
			m_database.Remove( *found );
			iter.BeginFrom( DBItem( shapeId ) );
			res = true;
		}
		return (res);
	}

//-----------------------------------------------------------------------------

	bool ShapeDatabase::DeleteField( size_t shapeId, const char* fieldName )
	{
		RString field = fieldName;
		field.Upper();
		field = const_cast< const ShapeDatabase* >( this )->ShareString( field );

		m_database.Remove( DBItem( shapeId, field ) );
		return (true);
	}

//-----------------------------------------------------------------------------

	void ShapeDatabase::Clear()
	{
		m_database.Clear();
		m_stringTable.Clear();
	}

//-----------------------------------------------------------------------------

	size_t ShapeDatabase::GetNextUsedShapeID( int currentID ) const
	{
		currentID++;
		BTreeIterator< DBItem > iter( m_database );
		DBItem* found;
		iter.BeginFrom( DBItem( currentID ) );
		found = iter.Next();
    if ( !found ) { return (-1) /*??????*/; }
		return (found->m_shapeId);
	}

//-----------------------------------------------------------------------------

} // namespace Shapes