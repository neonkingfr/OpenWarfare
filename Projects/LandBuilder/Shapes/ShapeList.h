//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	class ShapeList
	{
		AutoArray< PShape > m_shapeList;
		size_t              m_nextFree;

	public:
		ShapeList() 
    : m_nextFree( -1 ) /*??????*/
		{
		}

		size_t AddShape( const PShape& shape )
		{
			if ( m_nextFree != -1 /*??????*/ )
			{
				size_t ret = m_nextFree;
				m_shapeList[m_nextFree] = shape;
				for ( size_t i = m_nextFree + 1, cnt = m_shapeList.Size(); i < cnt; ++i )
				{
					if ( m_shapeList[i].IsNull() ) 
					{
						m_nextFree = i;
						return (ret);
					}
				}
				m_nextFree = -1; /*??????*/
				return (ret);
			}
			else
			{
				size_t ret = m_shapeList.Size();
				m_shapeList.Add( shape );
				return (ret);
			}
		}

		void DeleteShape( size_t id )
		{
			m_shapeList[id] = 0;
			m_nextFree = __min( m_nextFree, id );
			if ( id == (unsigned)(m_shapeList.Size() - 1 ) )
			{
				m_shapeList.Resize( id - 1 );
        if ( m_nextFree == id ) { m_nextFree = -1; }
			}
		}

		void SetShape( size_t id, const PShape& shape )
		{
			if ( id > (unsigned)m_shapeList.Size() )
			{
				m_nextFree = __min( m_nextFree, (unsigned)m_shapeList.Size() );
			}
			m_shapeList.Access( id );
			m_shapeList[id] = shape;
		}

		const IShape* GetShape( size_t id ) const
		{
			return (m_shapeList[id]);
		}

		IShape* GetShape( size_t id )
		{
			return (m_shapeList[id]);
		}

		template < class Functor >
		int EnumShapes( const Functor& funct ) 
		{
			for ( int i = 0; i < m_shapeList.Size(); ++i )
			{
				if ( m_shapeList[i].NotNull() )
				{
					int ret = funct( i, m_shapeList[i] );
          if ( ret ) { return (ret); }
				}
			}
			return (0);
		}

		template < class ShapeListType >
		class IterT
		{
			friend class ShapeList;

			size_t         m_pos;
			ShapeListType* m_outer;

			IterT( ShapeListType* arr, size_t pos) 
      : m_pos( pos )
      , m_outer( arr ) 
      {
      }

		public:
			operator bool () 
			{
				return (m_pos != -1 /*??????*/);
			}

			IterT& operator ++ () 			
			{
				m_outer->Next( m_pos );
				return (*this);
			}

			IterT& operator -- () 
			{
				m_outer->Prev( m_pos );
				return (*this);
			}

			IterT operator ++ ( int ) 
			{
				int saveVx = m_pos;
				m_outer->Next( m_pos );
				return (IterT< ShapeListType >( m_outer, saveVx ));
			}

			IterT operator -- ( int ) 
			{
				int saveVx = m_pos;
				m_outer->Prev( m_pos );
				return (IterT< ShapeListType >( m_outer, saveVx ));
			}

			const IShape* GetShape() const 
			{
				return (m_pos != -1 /*??????*/ ? m_outer->GetShape( m_pos ) : NULL);
			}

			IShape* GetShape() 
			{
				return (m_pos != -1 /*??????*/ ? const_cast< IShape* >( m_outer->GetShape( m_pos ) ) : NULL);
			}

			void SetShape( const SRef< PShape >& shape ) 
			{ 
        if ( m_pos != -1 /*??????*/ ) { m_outer->SetShape( m_pos, shape ); }
			}

			void DeleteShape() 
			{
				m_outer->DeleteShape( m_pos );
			}      

			size_t GetShapeId() const 
			{
				return (m_pos);
			}
		};

		typedef IterT< ShapeList > Iter;
		typedef IterT< const ShapeList > IterC;

		friend class IterT< ShapeList >;
		friend class IterT< const ShapeList >;

		Iter First()
		{
			for ( int i = 0, cnt = m_shapeList.Size(); i < cnt; ++i ) 
			{
        if ( m_shapeList[i].NotNull() ) { return (Iter( this, i )); }
			}
			return (Iter( this, -1 ));
		}

		IterC First() const
		{
			for ( int i = 0, cnt = m_shapeList.Size(); i < cnt; ++i ) 
			{
        if ( m_shapeList[i].NotNull() ) { return (IterC( this, i )); }
			}
			return (IterC( this, -1 ));
		}

		Iter Last()
		{
			for ( int i = m_shapeList.Size() - 1; i >= 0; --i ) 
			{
        if ( m_shapeList[i].NotNull() ) { return (Iter( this, i )); }
			}
			return (Iter( this, -1 ));
		}

		IterC Last() const
		{
			for ( int i = m_shapeList.Size() - 1; i >= 0; --i ) 
			{
        if ( m_shapeList[i].NotNull() ) { return (IterC( this, i )); }
			}
			return (IterC( this, -1 ));
		}

	protected:
		void Next( size_t& pos ) const
		{
      if ( pos == -1 /*??????*/ ) { return; }
			while ( ++pos < (unsigned)m_shapeList.Size() )
			{
        if ( m_shapeList[pos].NotNull() ) { return; }
			}
			pos = -1 /*??????*/ ;
		}

		void Prev( size_t& pos ) const
		{
      if ( pos == -1 /*??????*/) { return; }
			while ( --pos != -1 /*??????*/ )
			{
        if ( m_shapeList[pos].NotNull() ) { return; }
			}
		}

  public:
		size_t Size() const
		{
			return (m_shapeList.Size());
		}
	};

//-----------------------------------------------------------------------------

} // namespace Shapes