//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "ShapeBase.h"

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	///Shape that represents one point
	/**
	* Shape consist on list of points. Each point is reported as part with one point. 
	* The shape has the same part count as vertex count and each part has one vertex.
	*/
	class ShapeMultiPoint : public ShapeBase
	{
	protected:
		AutoArray< size_t > m_points;

	public:

		ShapeMultiPoint( const Array< size_t >& points = Array< size_t >( 0, 0 ), VertexArray* vx = NULL )
		: ShapeBase( vx ) 
		{
			m_points = points;
		}

		virtual size_t GetVertexCount() const 
		{
			return (m_points.Size());
		}

		virtual size_t GetPartCount() const 
		{
			return (m_points.Size());
		}

		virtual size_t GetIndex( size_t pos ) const 
		{
			return (m_points[(int)pos]);
		}

		virtual size_t GetPartIndex( size_t partId ) const 
		{
			return (partId);
		}

		virtual size_t GetPartSize( size_t partId ) const 
		{
			return (1);
		}

		///Function tests, whether the specified point is inside of shape
		/**
		* @copydoc IShape::PtInside
		* @note Function returns true, when vx is exactly the same as one of the points in this shape(with zero epsilon difference).
		*/
		virtual bool PtInside( const DVector& vx ) const
		{
			for ( size_t i = 0, cnt = GetVertexCount(); i < cnt; ++i )
			{
        if ( GetVertex( i ).EqualXY( vx ) ) { return (true); }
			}

			return (false);
		}

		virtual DVector NearestInside( const DVector& vx ) const
		{
			double minDist = DBL_MAX;
			int best = -1;
			for ( size_t i = 0, cnt = GetVertexCount(); i < cnt; ++i )
			{
				double dist = GetVertex( i ).DistanceXY2( vx );
				if ( dist < minDist )
				{
					best    = (int)i;
					minDist = dist;
				}
			}
			return (best != -1 ? GetVertex( best ) : DVector());
		}

		virtual IShape* Clip( double xn, double yn, double pos, VertexArray* vxArr = NULL ) const
		{
			Array< size_t > accepted( (size_t*)alloca( sizeof( size_t ) * m_points.Size() ), m_points.Size() );
			int used = 0;

			for ( int i = 0, cnt = m_points.Size(); i < cnt; ++i )
			{      
				const DVertex& vx = GetVertex( i );
        if ( Side( xn, yn, pos, vx.x, vx.y ) >= 0 ) { accepted[used++] = m_points[i]; }
			}

      if ( used == 0 ) { return (NULL); }

			accepted = Array< size_t >( accepted.Data(), used );
			if ( vxArr )
			{
				for ( int i = 0, cnt = accepted.Size(); i < cnt; ++i ) 
				{
					accepted[i] = vxArr->AddVertex( GetVertex( accepted[i] ) );
				}
				return (NewInstance( ShapeMultiPoint( accepted, vxArr ) ));
			}
			else
			{
				return (NewInstance( ShapeMultiPoint( accepted, m_vertices ) ));
			}
		}

		///Creates copy of the shape
		/**
		* Derived class can reimplement this function to 
		* handle allocation by itself.
		* @return pointer to new instance. 
		*/
		virtual ShapeMultiPoint* NewInstance( const ShapeMultiPoint& other ) const
		{
			return (new ShapeMultiPoint( other ));
		}

		virtual IShape* Copy( VertexArray* vxArr = NULL ) const
		{
			if ( vxArr )
			{      
				AutoArray< size_t > accepted = m_points;
				for ( int i = 0, cnt = accepted.Size(); i < cnt; ++i ) 
				{
					accepted[i] = vxArr->AddVertex( GetVertex( accepted[i] ) );
				}
				return (NewInstance( ShapeMultiPoint( accepted, vxArr ) ));
			}
			else
			{
				return (NewInstance( ShapeMultiPoint( m_points, m_vertices ) ));
			}
		} 

    virtual vector< IShape* > CutIntoFrame( VertexArray& vxArray, double left, double top, double right, double bottom ) const
    {
      vector< IShape* > ret;

			Array< size_t > accepted( (size_t*)alloca( sizeof( size_t ) * m_points.Size() ), m_points.Size() );
			int used = 0;

			for ( int i = 0, cnt = m_points.Size(); i < cnt; ++i )
			{      
				const DVertex& v = GetVertex( i );
        if ( (left <= v.x && v.x <= right) &&
             (bottom <= v.y && v.y <= top) )
        {
          accepted[used++] = m_points[i];
        }
			}

      if ( used > 0 ) 
      { 
			  accepted = Array< size_t >( accepted.Data(), used );
        ret.push_back( new ShapeMultiPoint( accepted, m_vertices ) );
      }

      return (ret);
    }

		///retrieves shape perimeter
		/**
		@return shape perimeter
		*/
		virtual double Perimeter() const 
		{
			return (0.0);
		}

		///retrieves shape's area
		/**
		@return shape area
		*/
		virtual double Area() const 
		{
			return (0.0);
		}

		virtual ShapeMultiPoint* ExtractPart( size_t part ) const 
		{
      if ( part >= (unsigned)m_points.Size()) { return (NULL); }
			return (NewInstance( ShapeMultiPoint( Array< size_t >( const_cast< size_t* >( m_points.Data() ) + part, 1 ), m_vertices ) ));    
		}
	};

//-----------------------------------------------------------------------------

} // namespace Shapes
  