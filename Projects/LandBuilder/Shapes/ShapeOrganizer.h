//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\IShape.h"
#include ".\IShapeFactory.h"
#include ".\ShapeDatabase.h"
#include ".\ShapeList.h"
#include ".\DBFReader.h"

//-----------------------------------------------------------------------------

class Matrix4;

//-----------------------------------------------------------------------------

namespace Shapes
{
	class ShapeOrganizer
	{
		ShapeList      m_listOfShapes;
		ShapeDatabase  m_shapeData;
		size_t         m_nextcpcountervalue; //next ID of constant parameters
		ShapeDatabase  m_cp; //constant parameters;
		IShapeFactory* m_factory;
		VertexArray    m_vxList;

  public:
		ShapeOrganizer( IShapeFactory* factory = NULL );
		~ShapeOrganizer();

		static const char* s_algorithmFieldName;
		static const char* s_cpFieldName;

		unsigned int AllocateNewConstantParamsID( const char* algorithm );
		void DeleteConstantParams( size_t ID );

		void SetConstantParam( size_t cpID, const char* name, const char* value );
		const char* GetConstantParam( size_t cpID, const char* name ) const;  

		const ShapeDatabase& GetConstantParamDB() const
		{
			return (m_cp);
		}

		ShapeDatabase& GetConstantParamDB()
		{
			return (m_cp);
		}

		///Reads shape file and database
		/**
		*  @param name name of shape file. Extension should be .shp. Different extension
		* is allowed, bud database name is always assumed with .dbf extension;
		*  @param constantParamsID ID of allocated constant params. Call AllocNewConstantParamsID 
		* to allocate new ID, or use an existing ID (depends on usage). For "nocp" use -1
		*  @param dbconvert pointer to class that provides column name conversion. Use NULL for no conversion
		*  @retval true, successfully loaded
		*  @retval false, error occurred. Error is reported through exception register.
		*/
		bool ReadShapeFile( const Pathname& name, size_t constantParamsID, 
                        IDBColumnNameConvert* dbconvert = NULL, 
                        const Pathname& forceDBChange = Pathname( PathNull ),
                        AutoArray< size_t >* shapeIndexes = NULL );

		///Changes CP of specified shape.
		bool SetShapeCP( size_t ShapeID, size_t constantParamsID );

		ShapeDatabase& GetShapeDatabase()
		{
			return (m_shapeData);
		}
	
		const ShapeDatabase& GetShapeDatabase() const
		{
			return (m_shapeData);
		}

		const char* GetShapeParameter( size_t ShapeID, const char* fieldname ) const;
		const char* GetShapeConstParameter( size_t ShapeID, const char* fieldname ) const;

		const char* operator () ( size_t ShapeID, const char* fieldname ) const
		{
			const char* res = GetShapeParameter( ShapeID, fieldname );
      if ( !res ) { res = GetShapeConstParameter( ShapeID, fieldname ); }
			return (res);
		}

		size_t GetShapeCP( size_t ShapeID ) const;

		template < class Functor >
		int ForEachShape( const Functor& funct )
		{
			m_listOfShapes.EnumShapes( funct )
		}

		ShapeList::Iter FirstShape() 
		{
			return (m_listOfShapes.First());
		}

		ShapeList::IterC FirstShape() const 
		{
			return (m_listOfShapes.First());
		}

		ShapeList::Iter LastShape() 
		{
			return (m_listOfShapes.Last());
		}

		ShapeList::IterC LastShape() const 
		{
			return (m_listOfShapes.Last());
		}

		ShapeList& GetShapeList() 
		{
			return (m_listOfShapes);
		}

		const ShapeList& GetShapeList() const 
		{
			return (m_listOfShapes);
		}

		size_t AddShape( IShape* shape, size_t constantParamsID );
		bool DeleteShape( size_t ID );

		size_t Size() const 
		{
			return (m_listOfShapes.Size());
		}    

		///Function removes unused constant params
		/**
		* After some shapes deleted, there can be unused CP ID's in database
		* This CP records can be deleted
		* @param report - if true, deleted CP are reported through exception register. Registered
		*  catcher can selective choose, which CP are important. Use fbIgnore to mark CP important
		*  preventing deletion
		*
		*/
		void DeleteUnusedCP( bool report );

		DBox CalcBoundingBox() const;
		void TransformVertices( const Matrix4& mx );

		void SetShapeFactory( IShapeFactory* f ) 
		{
			m_factory = f;
		}

		IShapeFactory* GetShapeFactory() 
		{
			return (m_factory);
		}

	public: ///Exception register classes
		class IBase : public ExceptReg::IException
		{
		public:
			virtual const _TCHAR* GetModule() const 
			{
				return ("ShapeOrganizer");
			}    
		};

		class TwoParamsSpecificBase : public IBase
		{
		protected:
			size_t      m_error;
			const char* m_filename;

		public:
			TwoParamsSpecificBase( size_t error, const char* filename ) 
			: m_error( error )
			, m_filename( filename ) 
			{
			}

			virtual size_t GetParamCount() const 
			{
				return (2);
			}

			virtual bool GetParam( size_t pos, const char*& param ) const
			{
				if ( pos == 1 ) 
				{
					param = m_filename;
					return (true);
				}
				else
				{
					return (IBase::GetParam( pos, param ));
				}
			}

			virtual bool GetParam( size_t pos, size_t& param ) const
			{
				if ( pos == 0 ) 
				{
					param = m_error;
					return (true);
				}
				else
				{
					return (IBase::GetParam( pos, param ));
				}
			}
		};

		class ShapeLoaderError : public TwoParamsSpecificBase
		{
			size_t      m_error;
			const char* m_filename;

		public:
			ShapeLoaderError( size_t error, const char* filename ) 
			: TwoParamsSpecificBase( error, filename ) 
			{
			}

			virtual size_t GetCode() const 
			{
				return (1);
			}

			virtual const _TCHAR* GetType() const 
			{
				return ("ShapeLoaderError");
			}

			virtual const _TCHAR* GetDesc() const 
			{
				return ("Shape loader error %d at file %s");
			}
		};

		class DatabaseLoaderError : public TwoParamsSpecificBase
		{
			size_t      m_error;
			const char* m_filename;

		public:
			DatabaseLoaderError( size_t error, const char* filename ) 
			: TwoParamsSpecificBase( error, filename ) 
			{
			}

			virtual size_t GetCode() const 
			{
				return (2);
			}

			virtual const _TCHAR* GetType() const 
			{
				return ("DatabaseLoaderError");
			}

			virtual const _TCHAR* GetDesc() const 
			{
				return ("DBF loader error %d at file %s");
			}
		};

		class UnusedCP : public IBase
		{
			size_t          m_unusedCP;
			ShapeOrganizer* m_owner;

    public:
			UnusedCP( size_t unusedCP, ShapeOrganizer* owner ) 
			: m_unusedCP( unusedCP )
			, m_owner( owner ) 
			{
			}

			virtual const _TCHAR* GetType() const 
			{
				return ("DeleteUnusedCP");
			}

			virtual const _TCHAR* GetDesc() const 
			{
				return ("Request for deletion of CP %d");
			}

			virtual size_t GetCode() const 
			{
				return (3);
			}

			virtual size_t GetParamCount() const 
			{
				return (2);
			}

			virtual bool GetParam( size_t pos, void*& param ) const
			{
				if ( pos == 1 ) 
				{
					param = m_owner;
					return (true);
				}
				else
				{
					return (IBase::GetParam( pos, param ));
				}
			}

			virtual bool GetParam( size_t pos, size_t& param ) const
			{
				if ( pos == 0 ) 
				{
					param = m_unusedCP;
					return (true);
				}
				else
				{
					return (IBase::GetParam( pos, param ));
				}
			}
		};
	
    VertexArray& GetVertexArray() 
		{
			return (m_vxList);
		}

		const VertexArray& GetVertexArray() const 
		{
			return (m_vxList);
		}
  };

//-----------------------------------------------------------------------------

} // namespace Shapes
