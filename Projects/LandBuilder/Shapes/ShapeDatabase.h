//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	class ShapeDatabase
	{
  public:
		struct DBItem
		{
			size_t   m_shapeId;
			RStringI m_paramName;
			RStringI m_value;

			DBItem( size_t shapeId = 0, const RStringI& paramName = RStringI(), const RStringI& value = RStringI())
      : m_shapeId( shapeId )
      , m_paramName( paramName )
      , m_value( value )
			{
			}

			int CompareWith( const DBItem& other ) const
			{
				if ( m_shapeId == other.m_shapeId )
				{
					return (_stricmp( m_paramName, other.m_paramName ));
				}
				return ((m_shapeId > other.m_shapeId) - (m_shapeId < other.m_shapeId));
			}

			bool operator == ( const DBItem& other ) const 
			{
				return (CompareWith( other ) == 0);
			}

			bool operator >= ( const DBItem& other ) const 
			{
				return (CompareWith( other ) >= 0);
			}

			bool operator <= ( const DBItem& other ) const 
			{
				return (CompareWith( other ) <= 0);
			}

			bool operator != ( const DBItem& other ) const 
			{
				return (CompareWith( other ) != 0);
			}

			bool operator > ( const DBItem& other ) const 
			{
				return (CompareWith( other ) > 0);
			}

			bool operator < ( const DBItem& other ) const 
			{
				return (CompareWith( other ) < 0);
			}
		};

		BTree< DBItem >   m_database;
		BTree< RStringS > m_stringTable;

		RString ShareString( const RString& text );
		RString ShareString( const RString& text ) const;

	public:
		bool SetField( size_t shapeId, const char* fieldName, const char* value );
		const char* GetField( size_t shapeId, const char* fieldName ) const;

		template < class T >
		bool ForEachField( const T& functor, size_t shapeId = -1 /*?????*/ ) const
		{
			BTreeIterator< DBItem > iter( m_database );
			iter.BeginFrom( DBItem( shapeId == -1 /*??????*/ ? 0 : shapeId ) );
			DBItem* found;
			while ( ((found = iter.Next()) != NULL) && (shapeId == -1 /*??????*/ || shapeId == found->m_shapeId ) )
			{
        if ( functor( found->m_shapeId, found->m_paramName, found->m_value ) ) { return (true); }
			}
			return (false);
		}

		bool DeleteShape( size_t shapeId );
		bool DeleteField( size_t shapeId, const char* fieldName );
		void Clear();
		size_t GetNextUsedShapeID( int currentID ) const;
	};

//-----------------------------------------------------------------------------

} // namespace Shapes
