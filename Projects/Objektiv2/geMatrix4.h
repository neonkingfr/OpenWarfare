#ifndef __GEMATRIX__H__46484654646846416031016089790_ONDREJ_NOVAK_
#define __GEMATRIX__H__46484654646846416031016089790_ONDREJ_NOVAK_

#include "geVector4.h"

class geMatrixLine4
  {
  public:
    float *line;
    geMatrixLine4(const float *line) 
      {this->line=const_cast<float *>(line);}
    float& operator[] (int pos) 
      {return line[pos & 3];}
    const float& operator[] (int pos) const 
      {return line[pos & 3];}
  };

//--------------------------------------------------

#define MATRIX_SWAPF(a,b,f) f=a,a=b,b=f
class geMatrix4
  {
  public:
    union
      {
      struct
        {
        geVector4 x;
        geVector4 y;
        geVector4 z;
        geVector4 pos;
        };
      struct
        {
        float a11,a12,a13,a14;
        float a21,a22,a23,a24;
        float a31,a32,a33,a34;
        float a41,a42,a43,a44;
        };
      float mx[4][4];
      };
  public:
    void Identity()
      {
      mx[0][0]=1;mx[0][1]=0;mx[0][2]=0;mx[0][3]=0;
      mx[1][0]=0;mx[1][1]=1;mx[1][2]=0;mx[1][3]=0;
      mx[2][0]=0;mx[2][1]=0;mx[2][2]=1;mx[2][3]=0;
      mx[3][0]=0;mx[3][1]=0;mx[3][2]=0;mx[3][3]=1;
      }
    
    geMatrix4(bool init=false)	
      {if (init) Identity();}
    
    geMatrixLine4 operator[] (int row) 
      {return geMatrixLine4(mx[row & 3]);}
    const geMatrixLine4 operator[] (int row) const 
      {return geMatrixLine4(mx[row & 3]);}
    
    geMatrix4& Translate(float x, float y, float z); //Create translation matrix
    geMatrix4& TranslateC(float x, float y, float z); //Translate current matrix
    geMatrix4& Scale(float x, float y, float z);	//Create scale matrix
    geMatrix4& ScaleC(float x, float y, float z);	//Scale current matrix
    geMatrix4& Rotate(geAxis axis, float angle);	//Create rotation matrix (about x, y or z)
    geMatrix4& RotateC(geAxis axis, float angle);	//Rotate current matrix (about x, y or z)
    geMatrix4& Rotate(float x, float y, float z, float angle);	//Create rotation matrix (general but slow)
    geMatrix4& RotateC(float x, float y, float z, float angle);  //rotate current matrix (general but slow)
    
    geMatrix4& MultC(const geMatrix4& other);	//Multiply current matrix with another one
    geMatrix4& Mult(const geMatrix4& left,const geMatrix4& right); //Create matrix as multiply
    
    geMatrix4 Invert() const; //returns inverted matrix (slow)
    geMatrix4& Invert(const geMatrix4& mm); //Create inverted version of matrix mm (faster)
    geMatrix4& InvertC(const geMatrix4& mm); //Mupltiply with inverted version of matrix mm 
    
    geMatrix4& Translate(const float *v) 
      {return Translate(v[0],v[1],v[2]);} //Create translation matrix
    geMatrix4& TranslateC(const float *v) 
      {return TranslateC(v[0],v[1],v[2]);} //Translate current matrix
    geMatrix4& Scale(const float *v) 
      {return Scale(v[0],v[1],v[2]);}	//Create scale matrix
    geMatrix4& ScaleC(const float *v) 
      {return ScaleC(v[0],v[1],v[2]);}	//Scale current matrix
    geMatrix4& Rotate(const float *v, float angle) 
      {return Rotate(v[0],v[1],v[2],angle);}	//Create rotation matrix (general but slow)
    geMatrix4& RotateC(const float *v, float angle) 
      {return RotateC(v[0],v[1],v[2],angle);}	  //rotate current matrix (general but slow)
    
    void Transform4(const float *in, float *out) const;
    float Transform3w(const float *in, float *out) const;
    void Transform3T(const float *in, float *out) const;
    void Transform3(const float *in, float *out) const;
    void Transform2T(const float *in, float *out) const;
    void Transform2(const float *in, float *out) const;
    void Transform1T(const float *in, float *out) const;
    
    geMatrix4& Projection(float front, float back, float fov, float aspec=1); //front plane, back plane, field of view
    geMatrix4& ProjectionC(float front, float back, float fov, float aspec=1);  //front plane, back plane, field of view
    
    geMatrix4 Transpose()
      {
      geMatrix4 out;
      out.a11=a11;
      out.a21=a12;
      out.a31=a13;
      out.a41=a14;
      out.a12=a21;
      out.a22=a22;
      out.a32=a23;
      out.a42=a24;
      out.a13=a31;
      out.a23=a32;
      out.a33=a33;
      out.a43=a34;
      out.a14=a41;
      out.a24=a42;
      out.a34=a43;
      out.a44=a44;
      return out;
      }
    geMatrix4& Transpose(const geMatrix4 &other)
      {
      a11=other.a11;
      a21=other.a12;
      a31=other.a13;
      a41=other.a14;
      a12=other.a21;
      a22=other.a22;
      a32=other.a23;
      a42=other.a24;
      a13=other.a31;
      a23=other.a32;
      a33=other.a33;
      a44=other.a34;
      a14=other.a41;
      a24=other.a42;
      a34=other.a43;
      a44=other.a44;
      return *this;
      }
    
    geMatrix4&  TransposeC()
      {
      float f;
      MATRIX_SWAPF (mx[1][0],mx[0][1],f);
      MATRIX_SWAPF (mx[2][0],mx[0][2],f);
      MATRIX_SWAPF (mx[2][1],mx[1][2],f);
      MATRIX_SWAPF (mx[3][0],mx[0][3],f);
      MATRIX_SWAPF (mx[3][1],mx[1][3],f);
      MATRIX_SWAPF (mx[3][2],mx[2][3],f);
      return *this;
      }
    
    
    
    //looking from
    //looking to (at)
    //top vector is direction of my top of head
    //if correct is true, then top vector is corrected to be plumb at plane of view
    // (WARINING: replaces "top" value directly it means that it destroys its original value)
    geMatrix4& LookAt(geVector4& from, geVector4& to, geVector4& top, bool correct=true); 
    
    //interpolates two matrices by factor
    //it is linear interpolation, so rotation matrices can distort object.
    //set normscale to value differet from zero, to normalize rotation vectors and prevent to distortion
    // .. but any scalling will be removed. Use normscale value to set uniform scaling of 
    //rotation vectors (set one to noscale ~ only normalize)
    //You will need use key-track interpolation or quaternion interpolation to interpolate
    //rotations. However, for small angles, linear interpolation returns useable results.		
    geMatrix4& Interpolate3T(geMatrix4 &left, geMatrix4 &right, float factor, float normscale=0.0f);
    
  };

//--------------------------------------------------

#endif