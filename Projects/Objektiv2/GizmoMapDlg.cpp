// GizmoMapDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "objektiv2doc.h"
#include "GizmoMapDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGizmoMapDlg dialog


int CGizmoMapDlg::gizmotype=0;

CGizmoMapDlg::CGizmoMapDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CGizmoMapDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CGizmoMapDlg)
    SwapUV = FALSE;
    TexName = _T("");
    wrapu = TRUE;
    wrapv = TRUE;
    MapMode = 0;
    //}}AFX_DATA_INIT	
    }










void CGizmoMapDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CGizmoMapDlg)
  DDX_Control(pDX, IDC_MAPTYPE, wMapMode);
  DDX_Control(pDX, IDC_MAPPREVIEW, wMapPrv);
  DDX_Check(pDX, IDC_SWAPTUTV, SwapUV);
  DDX_Text(pDX, IDC_TEXNAME, TexName);
  DDX_Check(pDX, IDC_WRAPU, wrapu);
  DDX_Check(pDX, IDC_WRAPV, wrapv);
  DDX_CBIndex(pDX, IDC_MAPTYPE, MapMode);
  //}}AFX_DATA_MAP
  }










BEGIN_MESSAGE_MAP(CGizmoMapDlg, CDialog)
  //{{AFX_MSG_MAP(CGizmoMapDlg)
  ON_COMMAND(IDC_MAPPREVIEW, OnMappreview)
    ON_EN_KILLFOCUS(IDC_TU1, OnKillfocusTexCoords)
      ON_BN_CLICKED(IDC_MIRRORTU, OnMirrortu)
        ON_BN_CLICKED(IDC_MIRRORTV, OnMirrortv)
          ON_BN_CLICKED(IDC_BROWSE, OnSetTexture)
            ON_BN_CLICKED(IDC_PREVIEW, OnPreview)
              ON_COMMAND(ID_GIZMO_RESETMAP, OnGizmoResetmap)
                ON_COMMAND(ID_SURFACES_RESETGISMO, OnSurfacesResetgismo)
                  ON_EN_KILLFOCUS(IDC_TEXNAME, OnKillfocusTexname)
                    ON_WM_GETMINMAXINFO()
                      ON_WM_SIZE()
                        ON_EN_KILLFOCUS(IDC_TU2, OnKillfocusTexCoords)
                          ON_EN_KILLFOCUS(IDC_TV1, OnKillfocusTexCoords)
                            ON_EN_KILLFOCUS(IDC_TV2, OnKillfocusTexCoords)
                              ON_BN_CLICKED(IDC_SETFROMLIST, OnSetTexture)
                                ON_BN_CLICKED(IDC_SETFROMVIEW, OnSetTexture)
                                  ON_COMMAND(ID_SURFACES_UNDOGIZMO, OnSurfacesResetgismo)
                                    ON_COMMAND(ID_TRANSF_MOVE, OnSurfacesResetgismo)
                                      ON_COMMAND(ID_TRANSF_ROTATE, OnSurfacesResetgismo)
                                        ON_COMMAND(ID_TRANSF_SCALE, OnSurfacesResetgismo)
                                          ON_COMMAND(ID_TRANS2_SCALE, OnSurfacesResetgismo)
                                            ON_COMMAND(ID_TRANS2_ROTATE, OnSurfacesResetgismo)
                                              ON_COMMAND(ID_TRANS2_MIRRORX, OnSurfacesResetgismo)
                                                ON_COMMAND(ID_TRANS2_MIRRORY, OnSurfacesResetgismo)
                                                  ON_CBN_SELCHANGE(IDC_MAPTYPE, OnSelchangeMaptype)
                                                    //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CGizmoMapDlg message handlers
    
    WINDOWPLACEMENT CGizmoMapDlg::wp=
      {0};










CGizmoMapDlg::GizmoMapInfo CGizmoMapDlg::nfo;

BOOL CGizmoMapDlg::OnInitDialog() 
  {
  MapMode=gizmotype;
  CDialog::OnInitDialog();
  CRect rc;
  GetClientRect(&rc);
  cursz=minsz=rc.Size();
  
  ((CButton *)GetDlgItem(IDC_BROWSE))->SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_BROWSE)));
  ((CButton *)GetDlgItem(IDC_SETFROMVIEW))->SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_FROMBGR)));
  ((CButton *)GetDlgItem(IDC_SETFROMLIST))->SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_FROMLIST)));
  POSITION pos=doc->GetFirstViewPosition();
  if (wp.length!=0) SetWindowPlacement(&wp);
  CWnd *wnd=GetOwner();
  wnd->EnableWindow(true);
  wrapu=nfo.wrapu;
  wrapv=nfo.wrapv;
  SwapUV=nfo.swapuv;
  MapMode=nfo.mode;
  nfo.nula=0;
  TexName=nfo.texname;
  wMapPrv.LoadBitmap(TexName);
  UpdateData(FALSE);
  OnMappreview() ;
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }










void CGizmoMapDlg::OnMappreview() 
  {
  SetDlgItemFloat(IDC_TU1,wMapPrv.fleft,3);
  SetDlgItemFloat(IDC_TU2,wMapPrv.fright,3);
  SetDlgItemFloat(IDC_TV1,wMapPrv.ftop,3);
  SetDlgItemFloat(IDC_TV2,wMapPrv.fbottom,3);
  }










void CGizmoMapDlg::SetDlgItemFloat(int id, float value, int decimal)
  {
  char form[20];
  char text[20];
  sprintf(text,"%%0.%df",decimal);
  sprintf(form,text,value);
  SetDlgItemText(id,form);
  }










float CGizmoMapDlg::GetDlgItemFloat(int id)
  {
  CString p;
  float z=0;
  GetDlgItemText(id,p);
  sscanf(p,"%f",&z);
  return z;
  }










void CGizmoMapDlg::OnKillfocusTexCoords() 
  {
  wMapPrv.fleft=GetDlgItemFloat(IDC_TU1);	
  wMapPrv.fright=GetDlgItemFloat(IDC_TU2);	
  wMapPrv.ftop=GetDlgItemFloat(IDC_TV1);	
  wMapPrv.fbottom=GetDlgItemFloat(IDC_TV2);	
  wMapPrv.Redraw();
  OnMappreview() ;
  }










void CGizmoMapDlg::OnMirrortu() 
  {
  float z;
  z=wMapPrv.fright;
  wMapPrv.fright=wMapPrv.fleft;
  wMapPrv.fleft=z;
  wMapPrv.Redraw();
  OnMappreview() ;
  }










void CGizmoMapDlg::OnMirrortv() 
  {
  float z;
  z=wMapPrv.ftop;
  wMapPrv.ftop=wMapPrv.fbottom;
  wMapPrv.fbottom=z;
  wMapPrv.Redraw();
  OnMappreview() ;
  }










CString GetTextureFrom(int idc, const char *OldTexName);
void CGizmoMapDlg::OnSetTexture() 
  {
  UpdateData();
  int id=GetFocus()->GetDlgCtrlID();
  TexName=GetTextureFrom(id,TexName);
  UpdateData(FALSE);
  wMapPrv.LoadBitmap(TexName);
  }










CIGizmoMap::CIGizmoMap(LPHMATRIX gt,CGizmoMapDlg ::GizmoMapInfo &gi)
  {
  InverzeMatice(gt,gizmo);
  nfo=gi;
  }










CIGizmoMap::CIGizmoMap(istream &str)
  {
  datard(str,gizmo);
  datard(str,nfo);
  }










void CIGizmoMap::Save(ostream &str)
  {
  datawr(str,gizmo);
  datawr(str,nfo);
  }










CICommand *CIGizmoMap_Create(istream &str)
  {
  return new CIGizmoMap(str);
  }










int CIGizmoMap::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();
  int np=obj->NPoints();
  int nf=obj->NFaces();
  PreCalcInfo *vlist=new PreCalcInfo[np];
  memset(vlist,0,sizeof(*vlist)*np);
  int i;
  for (i=0;i<nf;i++)
    if (obj->FaceSelected(i))
      {
      FaceT fc(obj,i);	
      for (int i=0;i<fc.N();i++)
        {
        int pt=fc.GetPoint(i);
        PreCalcInfo &pnfo=vlist[pt];
        if (!pnfo.used)
          {
          HVECTOR pp;
          PosT &ps=obj->Point(pt);
          TransformVector(gizmo,mxVector3(ps[0],ps[1],ps[2]),pp);
          float &u=pnfo.u,&v=pnfo.v;
          pnfo.used=true;
          switch (nfo.mode)
            {
            case 0: /* Cylindric mapping */
              {
              v=(-pp[ZVAL]+1.0f)*0.5f;
              u=-(atan2(pp[XVAL],-pp[YVAL]))/(2*FPI)+0.5f;
              }
            break;
            case 1: /* Cylindric mapping, (X, angle) */
              {
              v=(-pp[ZVAL]+1.0f)*0.5f;
              u=(float)sqrt(pp[XVAL]*pp[XVAL]+pp[YVAL]*pp[YVAL]);
              }
            break;
            
            case 2: /* Spheric mapping - one singularity */
              {
              float az=(float)atan2(pp[XVAL],pp[YVAL]);
              float zen=(float)atan2(-pp[ZVAL],sqrt(pp[XVAL]*pp[XVAL]+pp[YVAL]*pp[YVAL]));
              float d=(1.0f-((zen/FPI)+0.5f))*0.5f;
              u=0.5f+(float)cos(az)*d;
              v=0.5f+(float)sin(az)*d;
              }
            break;
            case 3: /* Spheric mapping - two singularity */
              {
              float az=(float)atan2(pp[XVAL],pp[YVAL]);
              float zen=(float)atan2(-pp[ZVAL],sqrt(pp[XVAL]*pp[XVAL]+pp[YVAL]*pp[YVAL]));
              u=az/(2*FPI);
              v=(zen/FPI)+0.5f;
              }
            break;
            }	  
          }
        fc.SetU(i,pnfo.u);
        fc.SetV(i,pnfo.v);
        }
      fc.SetTexture(nfo.texname);
      if(nfo.wrapu )
        { // perform cyclical correction of texture coordinate
        double uMax=-1e10;
        int j;
        for( j=0; j<fc.N(); j++ )
          {
          float u=fc.GetU(j);
          u=u-floor(u); // clamp into interval <0,1)
          if( uMax<u ) uMax=u;
          fc.SetU(j,u);
          }
        for( j=0; j<fc.N(); j++ )
          {
          float u=fc.GetU(j);
          if( uMax-u>0.5 )
            {
            // texture should wrap through point 1.0
            u=u+1.0;
            }
          fc.SetU(j,u);
          }
        }
      if(  nfo.wrapv)
        { // perform cyclical correction of texture coordinate
        double vMax=-1e10;
        int j;
        for( j=0; j<fc.N(); j++ )
          {
          float v=fc.GetV(j);
          v=v-floor(v); // clamp into interval <0,1)
          if( vMax<v ) vMax=v;
          fc.SetV(j,v);
          }
        for( j=0; j<fc.N(); j++ )
          {
          float v=fc.GetV(j);
          if( vMax-v>0.5 )
            {
            // texture should wrap through point 1.0
            v=v+1.0;
            }
          fc.SetV(j,v);
          }
        }
      for (int j=0;j<fc.N();j++)
        {
        if (nfo.swapuv)
          {
          float x=fc.GetU(j);fc.SetU(j,fc.GetV(j));fc.SetV(j,x);
          }		
        fc.SetUV(j,nfo.tu1+(nfo.tu2-nfo.tu1)*fc.GetU(j),nfo.tv1+(nfo.tv2-nfo.tv1)*fc.GetV(j));
        }
      }
  delete vlist;
  comint.sectchanged=true;
  return 0;
  }










void CGizmoMapDlg::SaveValues()
  {
  UpdateData();
  nfo.mode=MapMode;
  nfo.swapuv=SwapUV!=FALSE;
  strncpy(nfo.texname,TexName,sizeof(nfo.texname));
  nfo.tu1=wMapPrv.fleft;
  nfo.tu2=wMapPrv.fright;
  nfo.tv1=wMapPrv.ftop;
  nfo.tv2=wMapPrv.fbottom;
  nfo.wrapu=wrapu!=FALSE;
  nfo.wrapv=wrapv!=FALSE;
  nfo.nula=0;
  GetWindowPlacement(&wp);
  }










void CGizmoMapDlg::OnOK() 
  {
  SaveValues();	
  CDialog::OnOK();
  }










void CGizmoMapDlg::OnPreview() 
  {
  SaveValues();
  CIGizmoMap map(doc->gismo,nfo);
  map.Execute(doc->LodData);
  doc->UpdateAllViews(NULL,1);
  comint.SendNotify();
  }










void CGizmoMapDlg::OnGizmoResetmap() 
  {
  wMapPrv.fleft=0;
  wMapPrv.fright=1;
  wMapPrv.ftop=0;
  wMapPrv.fbottom=1;
  wMapPrv.Redraw();
  OnMappreview() ;
  }










void CGizmoMapDlg::OnSurfacesResetgismo() 
  {
  const MSG *msg=GetCurrentMessage();
  theApp.m_pMainWnd->PostMessage(msg->message,msg->wParam,msg->lParam);
  }










void CGizmoMapDlg::OnKillfocusTexname() 
  {
  CString z;
  GetDlgItemText(IDC_TEXNAME,z);
  wMapPrv.LoadBitmap(z);
  }










void CGizmoMapDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
  {
  CDialog::OnGetMinMaxInfo(lpMMI);
  if (wMapPrv.m_hWnd!=NULL)
    {
    lpMMI->ptMinTrackSize.x=minsz.cx;	
    lpMMI->ptMinTrackSize.y=minsz.cy;	
    }
  }










void CGizmoMapDlg::OnSize(UINT nType, int cx, int cy) 
  {
  CDialog::OnSize(nType, cx, cy);
  if (wMapPrv.m_hWnd==NULL) return;
  cx-=cursz.cx;
  cy-=cursz.cy;
  HDWP hdwp=BeginDeferWindowPos(20);
  MoveWindowRel(wMapPrv,0,0,cx,cy,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_MIRRORTV),cx,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_MIRRORTU),cx,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_SWAPTUTV),cx,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDOK),cx,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDCANCEL),cx,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_PREVIEW),cx,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_BROWSE),cx,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_SETFROMLIST),cx,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_SETFROMVIEW),cx,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_TEXNAME),0,cy,cx,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_WRAPU),0,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_WRAPV),0,cy,0,0,hdwp);
  MoveWindowRel(*GetDlgItem(IDC_MAPTYPE),0,cy,cx,0,hdwp);
  EndDeferWindowPos(hdwp);
  Invalidate();
  UpdateWindow();
  wMapPrv.Redraw();
  cursz.cx+=cx;
  cursz.cy+=cy;
  // TODO: Add your message handler code here
  
  }










void CGizmoMapDlg::OnSelchangeMaptype() 
  {
  gizmotype=wMapMode.GetCurSel();
  doc->UpdateAllViews(NULL,1);
  }










