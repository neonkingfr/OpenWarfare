#pragma once


// DlgRunScript dialog
class ScriptDebuggerBase ;
class GameState ;
class CMainFrame ;

class DlgRunScript : public CDialog
{
	DECLARE_DYNAMIC(DlgRunScript)

public:
	DlgRunScript(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgRunScript();

// Dialog Data
	enum { IDD = IDD_RUNSCRIPTDLG };

    HANDLE waitHandle;
    ScriptDebuggerBase *debugger;
    GameState *gameState;
    CMainFrame *mainfrm;
    CWnd *unlock;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  virtual BOOL OnInitDialog();
  afx_msg void OnTimer(UINT nIDEvent);
protected:
  virtual void OnCancel();
  virtual void OnOK();
public:
  afx_msg void OnBnClickedTerminate();
//  virtual BOOL PreTranslateMessage(MSG* pMsg);
  afx_msg void OnDestroy();
  void OnUnlockConsole();
  afx_msg void OnBnClickedUnlock();
};
