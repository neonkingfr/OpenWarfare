#pragma once
#include <afxwin.h>


// DlgUVManualTransform dialog

class DlgUVManualTransform;

class IDlgUVManualTransform
{
public:
    virtual void ManualTransformPerformed(const DlgUVManualTransform &dlg, bool preview)=0;
};

class DlgUVManualTransform : public CDialog
{
	DECLARE_DYNAMIC(DlgUVManualTransform)

  IDlgUVManualTransform *_notify;

public:
	DlgUVManualTransform(IDlgUVManualTransform *notify, CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgUVManualTransform();

// Dialog Data
	enum { IDD = IDD_UVMANUALTRANSFORM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  int vOperation;
  CEdit wUAxe;
  float vUAxe;
  CEdit wVAxe;
  float vVAxe;
  CEdit wRadius;
  float vRadius;
  CComboBox wCenter;
  int vCenter;
  CEdit wCenterU;
  float vCenterU;
  CEdit wCenterV;
  float vCenterV;
  int vUnits;
  CButton wRotation;
protected:
  afx_msg void OnBnClickedApply();
  afx_msg void OnBnClickedShowpreview();
  virtual void OnOK();
  afx_msg void OnBnClickedOperoffset();
  void DialogRules();


public:
  afx_msg void OnBnClickedOperrotation();
  afx_msg void OnBnClickedOperscale();
  afx_msg void OnCbnSelchangeCenter();
  virtual BOOL OnInitDialog();
protected:
  virtual void OnCancel();
};
