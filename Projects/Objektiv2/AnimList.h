#if !defined(AFX_ANIMLIST_H__839CF5C2_733B_4EFB_A171_D34EE32E7795__INCLUDED_)
#define AFX_ANIMLIST_H__839CF5C2_733B_4EFB_A171_D34EE32E7795__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AnimList.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAnimList dialog

class CAnimList : public CDialog
  {
  // Construction
  public:
    CAnimList(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CAnimList)
    enum 
      { IDD = IDD_ANIMLIST };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CAnimList)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CAnimList)
    // NOTE: the ClassWizard will add member functions here
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ANIMLIST_H__839CF5C2_733B_4EFB_A171_D34EE32E7795__INCLUDED_)
