

extern long prot_componumber;  //computer number
extern long prot_magicnumber;  //magic number
extern long prot_unlockcode;  //unlock code
extern long prot_requestcode;  //request code
extern long prot_handle;		 //protection handle
extern long prot_result;	   //result of unlock operation
extern long prot_encryptcompo;  //computer number
extern long prot_dummy;  //computer number


//#ifdef FULLVER
//definition for ful version


inline bool protCheckKeyLib1()
  {
  return true;
  }

//--------------------------------------------------

inline bool protCheckKeyLib2()
  {
  return true;
  }

//--------------------------------------------------

inline bool protCheckKeyLib3()
  {
  return true;
  }

//--------------------------------------------------

inline bool protCheckValidity()
  {
  return true;
  }

//--------------------------------------------------

inline bool protInit()
  {
  return true;
  }

//--------------------------------------------------

#define protCheckValid() 

inline void protClose()
  {
  }

//--------------------------------------------------

//#else
#if 0
inline bool protCheckKeyLib1()
  {
  return (pp_libtest(2054976245) == 2301186);
  }

//--------------------------------------------------

inline bool protCheckKeyLib2()
  {
  return (pp_libtest(1530278581) == 1177923);
  }

//--------------------------------------------------

inline bool protCheckKeyLib3()
  {
  return (pp_libtest(1396112293) == 5226407);
  }

//--------------------------------------------------

inline bool protCheckValidity()
  {
  if (!protCheckKeyLib1()) return false;
  DWORD retval = pp_copycheckth(prot_handle, ACTION_MANUAL ,prot_componumber, 100);
  if (retval==PP_TRUE || pp_ndecrypt(prot_encryptcompo,prot_result)==(prot_componumber & 16383))
    {
    if (pp_getvarnum(prot_handle, VAR_UDEF_NUM_1, &prot_magicnumber)!=PP_SUCCESS) return false;
    if (pp_getvarnum(prot_handle, VAR_UDEF_NUM_3, &prot_unlockcode)!=PP_SUCCESS) return false;
    if (pp_getvarnum(prot_handle, VAR_UDEF_NUM_2, &prot_requestcode)!=PP_SUCCESS) return false;
    return true;
    }
  return false;
  }

//--------------------------------------------------

inline bool protCheckExpiration()
  {
  if (!protCheckKeyLib1()) return false;
  return pp_expired(prot_handle)!=FALSE;
  }

//--------------------------------------------------

#define protCheckValid()  \
  if (((pp_copycheckth(prot_handle, ACTION_MANUAL ,prot_componumber, 100))!=PP_TRUE || __LINE__==prot_dummy) &&\
  pp_ndecrypt(prot_encryptcompo,prot_result)!=(prot_componumber & 16383) || !protCheckKeyLib2() || pp_valdate(prot_handle)==PP_FALSE || pp_expired(prot_handle)!=PP_FALSE)\
	__asm lea edi,[esp+__LINE__] \
	__asm mov ecx,0xcfa		\
	__asm xor eax,eax	  \
	__asm cld			  \
	__asm rep stosd


inline void protClose()
  {
  DWORD retval = pp_copycheckth(prot_handle, ACTION_MANUAL ,prot_componumber, 100);
  if (retval!=PP_TRUE)
    {
    LONG prot=pp_ndecrypt(prot_encryptcompo,prot_result) | (prot_componumber & ~16383);
    pp_copyadd(prot_handle, COPYADD_ERASEALL, prot);
    pp_setvarnum(prot_handle,VAR_UDEF_NUM_5,0);		//vynuluj registracni cislo
    }
  pp_upddate(prot_handle,UPDDATE_NOTIME);
  pp_lfclose(prot_handle);
  }

//--------------------------------------------------

void protInitProtection();


#endif