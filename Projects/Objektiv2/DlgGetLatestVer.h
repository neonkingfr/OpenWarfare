#pragma once


// DlgGetLatestVer dialog

class DlgGetLatestVer : public CDialog
{
	DECLARE_DYNAMIC(DlgGetLatestVer)
  CString _pretext;
  int _timeout;  
  CWnd *_wndMsg;
  UINT _cmdID;

public:
	DlgGetLatestVer(CWnd *wndMsg, UINT cmdID, CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgGetLatestVer();

  

// Dialog Data
	enum { IDD = IDD_GETLATESTVERSIONDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  virtual BOOL OnInitDialog();
protected:
  virtual void OnOK();
  virtual void OnCancel();
public:
  afx_msg void OnTimer(UINT nIDEvent);
protected:
  virtual void PostNcDestroy();
};
