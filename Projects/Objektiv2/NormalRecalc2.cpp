#include "stdafx.h"
#include "NormalRecalc.h"
#include "Edges.h"

void ReadEdges(ObjectData *obj, CEdges& egs)
  {
  egs.SetVertices(obj->NPoints());
  int from, to;
  for (int enm=0;obj->EnumEdges(enm,from,to);)
    {
    egs.SetEdge(from,to);
    }
  }

//--------------------------------------------------

void RecalcNormals2(ObjectData *obj)
  {
  //  obj->RecalcNormals();
  //obj->CleanNormals();
  //  }
  CEdges egs;
  ReadEdges(obj, egs);
  obj->ResetNormals();
  PrepareNormals(obj,egs);
  CalcNormals(obj);
  }

//--------------------------------------------------

