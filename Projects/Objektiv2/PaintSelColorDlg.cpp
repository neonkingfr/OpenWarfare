// PaintSelColorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "PaintSelColorDlg.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPaintSelColorDlg dialog


CPaintSelColorDlg::CPaintSelColorDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CPaintSelColorDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CPaintSelColorDlg)
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CPaintSelColorDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CPaintSelColorDlg)
  DDX_Control(pDX, IDC_SLIDER4, wStrength);
  DDX_Control(pDX, IDC_COLORSLIDER, wColorSlider);
  DDX_Control(pDX, IDC_SLIDER3, wMaxSize);
  DDX_Control(pDX, IDC_SLIDER2, wMinSize);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CPaintSelColorDlg, CDialog)
  //{{AFX_MSG_MAP(CPaintSelColorDlg)
  ON_WM_HSCROLL()
    ON_WM_TIMER()
      //}}AFX_MSG_MAP
      END_MESSAGE_MAP()
        
        /////////////////////////////////////////////////////////////////////////////
        // CPaintSelColorDlg message handlers
        
        void CPaintSelColorDlg::OnOK()
          {
          
          }

//--------------------------------------------------

void CPaintSelColorDlg::OnCancel()
  {
  DestroyWindow();
  }

//--------------------------------------------------

BOOL CPaintSelColorDlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  wMaxSize.SetRange(0,100);
  wMinSize.SetRange(0,100);
  wStrength.SetRange(0,20);
  wMaxSize.SetPos((int)(frame->paintradiusmax-frame->paintradiusmin));
  wMinSize.SetPos((int)(frame->paintradiusmin));
  wStrength.SetPos((int)(frame->paintstrength*20));
  wColorSlider.val=frame->paintweight;
  SetTimer(1000,1000,0);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CPaintSelColorDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
  {
  CWnd *wnd=(CWnd *)pScrollBar;
  if (wnd==&wMaxSize || wnd==&wMinSize || wnd==&wStrength)
    {
    int pos1=wMaxSize.GetPos();
    int pos2=wMinSize.GetPos();
    int pos3=wStrength.GetPos();
    frame->paintradiusmax=pos1+pos2;
    frame->paintradiusmin=pos2;
    frame->paintstrength=(float)pos3/20.0f;
    }
  if (wnd==&wColorSlider)
    {
    frame->paintweight=wColorSlider.val;
    }
  CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
  }

//--------------------------------------------------

void CPaintSelColorDlg::OnTimer(UINT nIDEvent) 
  {
  if (GetActiveWindow()==this)
    {
    CWnd *w=WindowFromPoint(CPoint(GetMessagePos()));
    bool a=(w==this) || (w!=NULL && w->GetParent()==this);
    if (!a)	frame->SetActiveWindow();
    }
  }

//--------------------------------------------------

