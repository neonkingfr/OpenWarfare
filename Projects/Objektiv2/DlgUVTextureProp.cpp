// DlgUVTextureProp.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgUVTextureProp.h"
#include ".\dlguvtextureprop.h"


// DlgUVTextureProp dialog

IMPLEMENT_DYNAMIC(DlgUVTextureProp, CDialog)
DlgUVTextureProp::DlgUVTextureProp(IDlgUVTexturePropNotify *notify, CWnd* pParent /*=NULL*/)
: CDialog(DlgUVTextureProp::IDD, pParent),_notify(notify)
{
}

DlgUVTextureProp::~DlgUVTextureProp()
{
}

void DlgUVTextureProp::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_SLIDER3, wContrast);
  DDX_Control(pDX, IDC_SLIDER2, wBrightness);
}


BEGIN_MESSAGE_MAP(DlgUVTextureProp, CDialog)
  ON_WM_VSCROLL()
  ON_BN_CLICKED(IDC_RESET, OnBnClickedReset)
  ON_BN_CLICKED(IDC_BLACKONWHTE, OnBnClickedBlackonwhte)
END_MESSAGE_MAP()



BOOL DlgUVTextureProp::OnInitDialog()
{
  CDialog::OnInitDialog();

  wContrast.SetRange(0,100);
  wBrightness.SetRange(0,100);
  wContrast.SetTicFreq(50);
  wBrightness.SetTicFreq(50);

  wBrightness.SetPos(100-_notify->GetBrightness());
  wContrast.SetPos(100-_notify->GetContrast());
  CheckDlgButton(IDC_BLACKONWHTE,_notify->GetBlackOnWhite()?1:0);
  CenterWindow();

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}


void DlgUVTextureProp::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  if (nSBCode==SB_ENDSCROLL)
  {
    CSliderCtrl *what=reinterpret_cast<CSliderCtrl *>(pScrollBar);
    if (what==&wContrast) _notify->ContrastChanged(100-wContrast.GetPos());
    else if (what==&wBrightness) _notify->BrightnessChanged(100-wBrightness.GetPos());
  }
  
  CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}


void DlgUVTextureProp::OnOK()
{
}

void DlgUVTextureProp::OnCancel()
{
  DestroyWindow();
}
 

void DlgUVTextureProp::OnBnClickedReset()
{
  _notify->BrightnessContrastReset();
  wBrightness.SetPos(100-_notify->GetBrightness());
  wContrast.SetPos(100-_notify->GetContrast());
}

void DlgUVTextureProp::OnBnClickedBlackonwhte()
{
  _notify->BlackOnWhiteChanged(IsDlgButtonChecked(IDC_BLACKONWHTE)!=0);
}
