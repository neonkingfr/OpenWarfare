#if !defined(AFX_MENUBAR_H__E67DBEC0_54D7_11D5_B39E_00C0DFAE7D0A__INCLUDED_)
#define AFX_MENUBAR_H__E67DBEC0_54D7_11D5_B39E_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MenuBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMenuBar dialog

class CMenuBar : public CDialogBar
  {
  // Construction
  CWnd *notify;
  CMenu mnu;
  bool droped;
  HWND last;
  public:
    CMenuBar();   // standard constructor
    CDIALOGBAR_RESIZEABLE
      void Create(CWnd *parent);
    
    // Dialog Data
    //{{AFX_DATA(CMenuBar)
    enum 
      { IDD = IDD_MENUBAR };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMenuBar)
  protected:
    
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CMenuBar)
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg UINT OnNcHitTest(CPoint point);
    afx_msg void OnEnterIdle(UINT nWhy, CWnd* pWho);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MENUBAR_H__E67DBEC0_54D7_11D5_B39E_00C0DFAE7D0A__INCLUDED_)
