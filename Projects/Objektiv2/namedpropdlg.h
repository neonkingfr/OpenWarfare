#if !defined(AFX_NAMEDPROPDLG_H__A81DADC4_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_NAMEDPROPDLG_H__A81DADC4_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// NamedPropDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNamedPropDlg dialog

#include "ObjectData.h"

class CNamedPropDlg : public CDialogBar
  {
  CWnd *notify;
  CListCtrl list;
  LODObject *lod;
  public:
    CDIALOGBAR_RESIZEABLE
      void LoadProperties(LODObject *LodObject);
    CNamedPropDlg();   // standard constructor
    void Create(CWnd *parent, int menuid);
    // Dialog Data
    //{{AFX_DATA(CNamedPropDlg)
    enum 
      { IDD = IDD_NAMEDPROPERTY };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CNamedPropDlg)
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CNamedPropDlg)
    afx_msg void OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnRclickList1(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnNampropNew();
    afx_msg void OnDestroy();
    afx_msg void OnNampropEdit();
    afx_msg void OnNampropDelete();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnPluginPopupCommand(UINT cmd);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NAMEDPROPDLG_H__A81DADC4_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
