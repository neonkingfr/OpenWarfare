// TexPreviewData.cpp: implementation of the CTexPreviewData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "TexPreviewData.h"
#include <g3dlib.h>
#include <malloc.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexPreviewData::CTexPreviewData()
  {
  for (int i=0;i<CTEX_CACHESIZE;i++) cache[i].texindex=-1;
  refpath=NULL;
  rlu_counter=0;
  }

//--------------------------------------------------

CTexPreviewData::~CTexPreviewData()
  {
  free(refpath);
  for (int i=0;i<CTEX_CACHESIZE;i++)
    if (cache[i].texindex>=0) cache[i].DestroyBitmap();
  }

//--------------------------------------------------

void STexPreviewData::LoadBitmap(const char * pathname, CDC *pdc)
  {
  TUNIPICTURE *uni=cmLoadUni((char *)pathname);
  if (uni!=NULL)
    {
    xsize=cmGetXSize(uni);
    ysize=cmGetYSize(uni);
    format=_strdup(cmGetFormat(uni));
    BITMAPINFO *buni=(BITMAPINFO *)cmUni2BitMap(uni, 1, 48, 48);
    bitmap=new CBitmap();
    bitmap->CreateCompatibleBitmap(pdc,128,128);
    context=new CDC();
    context->CreateCompatibleDC(pdc);
    old=(HBITMAP)*context->SelectObject(bitmap);	
    StretchDIBits( context->m_hDC,
    0,0,48,48,
    0,0,buni->bmiHeader.biWidth,buni->bmiHeader.biHeight,
    (void *)((char *)buni+sizeof(buni->bmiHeader)),
    buni,0,SRCCOPY);
    free(uni);
    free(buni);
    }
  else
    {
    xsize=0;
    ysize=0;
    format=_strdup("???");
    bitmap=new CBitmap();
    bitmap->LoadBitmap(IDB_UNKNOWNTEX);
    context=new CDC();
    context->CreateCompatibleDC(pdc);
    old=(HBITMAP)*context->SelectObject(bitmap);
    }
  }

//--------------------------------------------------

void STexPreviewData::DestroyBitmap()
  {
  if (texindex==-1) return;
  if (context!=NULL)  context->SelectObject(CBitmap::FromHandle(old));
  delete bitmap;
  context->DeleteDC();
  delete context;
  free(format);
  texindex=-1;
  }

//--------------------------------------------------

void CTexPreviewData::SetRefPath(char * path)
  {
  free(refpath);
  refpath=_strdup(path);
  Flush();
  }

//--------------------------------------------------

void CTexPreviewData::Flush()
  {
  for (int i=0;i<CTEX_CACHESIZE;i++)
    if (cache[i].texindex>=0) 
      {
      cache[i].DestroyBitmap();
      }
  }

//--------------------------------------------------

CDC * CTexPreviewData::GetBitmap(int index, const char * filename, CDC *pDC,  char *desc,int descsize, bool load)
  {
  unsigned int minrlu=(unsigned int)-1;
  int inxrlu=0;
  for (int i=0;i<CTEX_CACHESIZE;i++)
    {
    if (cache[i].texindex==index) 
      {
      cache[i].priority=rlu_counter++;
      _snprintf(desc,descsize,"%s %dx%d %s",filename,cache[i].xsize,cache[i].ysize,cache[i].format);
      return cache[i].context;
      }
    if (cache[i].texindex==-1)
      {
      minrlu=0;
      inxrlu=i;
      }
    else if (cache[i].priority<minrlu)
      {
      minrlu=cache[i].priority;
      inxrlu=i;
      }
    }  
  if (!load) 
    {
    _snprintf(desc,descsize,"%s",filename);
    return NULL;
    }
  cache[inxrlu].DestroyBitmap();
  char *c=(char *)alloca(GetFullNameSize(filename));
  GetFullName(c,filename);  
  cache[inxrlu].LoadBitmap(c,pDC);
  cache[inxrlu].priority=rlu_counter++;
  cache[inxrlu].texindex=index;
  _snprintf(desc,descsize,"%s %dx%d %s",filename,cache[inxrlu].xsize,cache[inxrlu].ysize,cache[inxrlu].format);
  return cache[inxrlu].context;
  }

//--------------------------------------------------

int CTexPreviewData::GetFullNameSize(const char * name)
  {
  if (refpath) return strlen(refpath)+2+strlen(name);
  else return strlen(name)+1;
  }

//--------------------------------------------------

void CTexPreviewData::GetFullName(char *src, const char * filename)
  {
  if (refpath)
    {
    if (refpath[strlen(refpath)-1]!='\\') sprintf(src,"%s\\%s",refpath,filename);
    else sprintf(src,"%s%s",refpath,filename);
    }
  else
    strcpy(src,filename);
  }

//--------------------------------------------------

