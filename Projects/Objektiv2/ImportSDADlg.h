#if !defined(AFX_IMPORTSDADLG_H__5F30FBE6_72A9_4573_A2C3_743929A9CF4F__INCLUDED_)
#define AFX_IMPORTSDADLG_H__5F30FBE6_72A9_4573_A2C3_743929A9CF4F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImportSDADlg.h : header file
//

#include <strstream>
using namespace std;
#include "CreatePrimDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CImportSDADlg dialog

class CImportSDADlg : public CDialog
  {
  // Construction
  ifstream instr;
  strstream *pllx;
  CCreatePrimDlg prim;
  float fscale;
  int delimiter;
  int fsize;
  char primcntr;
  public:
    CImportSDADlg(CWnd* pParent = NULL);   // standard constructor
    ObjectData *obj;
    const char *filename;
    
    // Dialog Data
    //{{AFX_DATA(CImportSDADlg)
    enum 
      { IDD = IDD_SURFACEDATA };
    CButton	wnd_select;
    CEdit	wnd_bxsize;
    CListCtrl	list;
    float	bxsize;
    UINT	max;
    UINT	min;
    int		vxtype;
    CString	scale;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CImportSDADlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void CreatePrims(ObjectData *obj);
    void OnDialogRules();
    
    // Generated message map functions
    //{{AFX_MSG(CImportSDADlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnVxtype();
    virtual void OnOK();
    afx_msg void OnSelect();
    afx_msg void OnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnDestroy();
    afx_msg void OnCreatprim();
    afx_msg void OnDeleteprim();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORTSDADLG_H__5F30FBE6_72A9_4573_A2C3_743929A9CF4F__INCLUDED_)
