// PointAttacher.cpp: implementation of the CPointAttacher class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "PointAttacher.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPointAttacher::CPointAttacher()
  {
  mspoints=NULL;
  indexes=NULL;
  Reset();
  }

//--------------------------------------------------

CPointAttacher::~CPointAttacher()
  {
  Reset();
  }

//--------------------------------------------------

void CPointAttacher::Reset()
  {
  delete [] mspoints;
  delete [] indexes;
  count=0;
  mspoints=NULL;
  indexes=NULL;
  }

//--------------------------------------------------

void CPointAttacher::Create(ObjectData *odata, HMATRIX mm)
  {
  const Selection *sel=odata->GetSelection();
  const Selection *hide=odata->GetHidden();
  const Selection *testsel;
  bool testval;
  int i,cnt;
  Reset();
  cnt=odata->NPoints();
  if (sel->IsEmpty()) 
    {testsel=hide;testval=false;}
  else 
    {testsel=sel;testval=true;}  
  for (int j=0;j<2;j++)
    {
    int pcnt=0;
    for (i=0;i<cnt;i++) 
      if (testsel->PointSelected(i)==testval)
        {
        HVECTOR tv;
        PosT& pos=odata->Point(i);
        TransformVector(mm,mxVector3(pos[0],pos[1],pos[2]),tv);
        XYZW2XYZQ(tv);
        if (tv[ZVAL]>0.0f && tv[WVAL]>0.0f)
          if (mspoints==NULL) pcnt++;
        else
          {
          CPoint *p=mspoints+pcnt;
          p->x=(int)tv[XVAL];
          p->y=(int)tv[YVAL];
          indexes[pcnt]=i;
          pcnt++;
          }
        }
    if (mspoints==NULL) 
      {
      if (pcnt==0) return;
      else 
        {
        mspoints=new CPoint[pcnt];
        indexes=new int[pcnt];
        }
      }
    count=pcnt;
    }	
  }

//--------------------------------------------------

int CPointAttacher::AttachMouse(CPoint &pt)
  {
  float minw;
  int idx;
  if (mspoints==NULL) return -1;
  idx=0;
  minw=GetDistance2(pt,mspoints[0]);
  for (int i=1;i<count;i++)
    {
    float d=GetDistance2(pt,mspoints[i]);
    if (d<minw) 
      {minw=d;idx=i;}
    }
  pt=mspoints[idx];
  return indexes[idx];
  }

//--------------------------------------------------

float CPointAttacher::GetDistance2(CPoint &pt1, CPoint &pt2)
  {
  float xd,yd;
  xd=(float)(pt1.x-pt2.x);
  yd=(float)(pt1.y-pt2.y);
  return xd*xd+yd*yd;
  }

//--------------------------------------------------

