#pragma once
#include "afxcmn.h"
#include <projects/ObjektivLib/ObjToolSections.h>


// CMatLibTreeToolbar dialog

/*struct SMatLibItemInfo
  {
  enum ItemMode {
  Pathname itempath;

  }*/

class CMatLibTreeToolbar : public CDialogBar
{	
HTREEITEM _curlod;
HTREEITEM _curmodel;
HTREEITEM _modelfolder;
HTREEITEM _libfull;
HTREEITEM _models;
HTREEITEM _sections;
HTREEITEM _menuitem;
CTreeCtrl tree;
CImageList ilist;
CImageList iStatus;
Pathname _modelfldr;
bool _mdlprefixed;
enum SortSectionsEnum {SectByTex,SectByMat, SectByFlags, SectByCount};
enum SearchDiskEnum {SearchTextures=1, SearchMaterials=2, SearchModels=4, SearchImages=8, SearchNoModels=11, SearchAll=15};
SortSectionsEnum _sectsort;

ObjectData *_curLOD;
LODObject *_curModel;
const char *_draggedFile;
CImageList *_draggedImage;
COleDataSource _COleDataSource;

  void UpdateCurModelLOD(HTREEITEM expand, LODObject *lod=NULL, ObjectData *obj=NULL);

public:
	CMatLibTreeToolbar(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMatLibTreeToolbar();
   CDIALOGBAR_RESIZEABLE

// Dialog Data
	enum { IDD = IDD_MATLIBTREETOOLBAR };

protected:

	DECLARE_MESSAGE_MAP()
public:
  BOOL Create(CWnd * parent, int menuid);
  void InitContent(void);
  HTREEITEM AddItemToTree(const char * pathname, int icon, HTREEITEM parent, bool folder=false, bool isspec=false, int id=0, HTREEITEM insertAfter=TVI_LAST);
  HTREEITEM UpdateItemInTree(HTREEITEM &walker, const char * pathname, int icon, HTREEITEM parent, bool folder=false, bool isspec=false, int id=0);
  void BeginUpdateInTree(HTREEITEM parent, HTREEITEM &walker);
  void EndUpdateInTree(HTREEITEM &walker);
  void DeleteTreeItem(HTREEITEM item);
  void DeleteChildItems(HTREEITEM parent);
  void TreeResetContent(void);

  void UpdateCurLOD(ObjectData *obj, HTREEITEM expand=NULL);  
  void UpdateCurModel(LODObject *lod, HTREEITEM expand=NULL);
  void SetModelPath(const Pathname& path); //nastavi vychozi cestu k modelu. Pokud je NULL, pouzije se prefix
  const char *GetSelected();
protected:
  void LoadDirectoryToTree(HTREEITEM parent, Pathname path, bool topexpand, int SearchEnum);
  bool UseLoadModeDefault(const char *filename);

  afx_msg void OnTvnItemexpandingTree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnTvnItemexpandedTree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDestroy();
  afx_msg void OnSize(UINT nType, int cx, int cy);  

  friend VOID CALLBACK DelayedUpdate(HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime);
  
public:
  afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
  afx_msg void OnMatlibOpen();
  afx_msg void OnMatlibExplore();
  afx_msg void OnMatlibLoad();
  afx_msg void OnMatlibSelect();
  afx_msg void OnMatlibCreateproxy();
  afx_msg void OnTvnBegindragTree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnNMDblclkTree(NMHDR *pNMHDR, LRESULT *pResult);
  void OnTreeActivateItem(void);
  void UpdateSections(BTree<ObjToolSections::ObjToolSectionInfo> & sections);
  afx_msg BOOL OnMatlibsort(UINT cmd);
  afx_msg void OnMatlibRename();
  afx_msg void OnTvnBeginlabeleditTree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnMatlibProperties();
  afx_msg void OnMatlibHistory();
  afx_msg void OnMatlibGetlatestversion();
  afx_msg void OnMatLibCreateMaterial();

  afx_msg void OnTimer(UINT nIDEvent);
  afx_msg void OnMatLibCheckOut();
  afx_msg void OnMatLibCheckIn();
  afx_msg void OnMatLibUndoCheckOut();

  bool UpdateSSStatusOneItem(HTREEITEM root);
};
