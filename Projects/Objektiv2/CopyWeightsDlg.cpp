// CopyWeightsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "objektiv2doc.h"
#include "CopyWeightsDlg.h"
#include "ComInt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCopyWeightsDlg dialog


CCopyWeightsDlg::CCopyWeightsDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CCopyWeightsDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CCopyWeightsDlg)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CCopyWeightsDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CCopyWeightsDlg)
  DDX_Control(pDX, IDC_SELLIST, wSelList);
  DDX_Control(pDX, IDC_REFLOD, wRefLod);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CCopyWeightsDlg, CDialog)
  //{{AFX_MSG_MAP(CCopyWeightsDlg)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_REFLOD, OnItemchangedReflod)
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CCopyWeightsDlg message handlers
      
      static void LoadSelections(CListCtrl& list, ObjectData *obj)
        {
        list.DeleteAllItems();
        int i,cnt;
        for (i=0,cnt=MAX_NAMED_SEL;i<cnt;i++)
          {
          const NamedSelection *p=obj->GetNamedSel(i);
          if (p) 
            {
            int q=list.InsertItem(0,p->Name(),ILST_SELECTION);
            list.SetItemData(q,i);
            }
          }
        }

//--------------------------------------------------

BOOL CCopyWeightsDlg::OnInitDialog() 
  { 
  CDialog::OnInitDialog();
  
  wSelList.SetImageList(&shrilist,LVSIL_SMALL);
  wSelList.InsertColumn(0,"",LVCFMT_LEFT,100,0);
  wRefLod.SetImageList(&shrilist,LVSIL_SMALL);
  wRefLod.InsertColumn(0,"",LVCFMT_LEFT,100,0);
  for (int i=0;i<lod->NLevels();i++)
    {
    int p=wRefLod.InsertItem(i,LODResolToName(lod->Resolution(i)),ILST_LODDEFINITION);
    wRefLod.SetItemData(p,i);
    }	
  
  
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CCopyWeightsDlg::OnItemchangedReflod(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NMLISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
  if (pNMListView->uNewState & LVIS_FOCUSED )
    {
    int ld=pNMListView->lParam;
    ObjectData *obj=lod->Level(ld);
    LoadSelections(wSelList,obj);
    }
  *pResult = 0;
  }

//--------------------------------------------------

class CICopyWeights: public CICommand
  {
  ObjectData *srcobj;
  CString selname;
  public:
    CICopyWeights(ObjectData *srcobj, const char *selnam):
    srcobj(srcobj),selname(selnam) 
      {recordable=false;}
    int Execute(LODObject *obj);
  };

//--------------------------------------------------

int CICopyWeights::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();
  const Selection *src=srcobj->GetNamedSel(selname);
  if (src==NULL) return -1;
  obj->SaveNamedSel(selname);
  Selection *trg=const_cast<NamedSelection *>(obj->GetNamedSel(selname));
  trg->Clear();
  int srcpt=srcobj->NPoints();
  int trgpt=obj->NPoints();
  for (int i=0;i<trgpt;i++)
    {
    float maxdistance=0;
    float wsuma=0;	
    float weight=0;
    int j;
    PosT &ps=obj->Point(i);
    for (j=0;j<srcpt;j++)
      {
      PosT &rps=srcobj->Point(j);
      float d=ps.Distance2(rps);
      maxdistance+=1/d;
      }	
    for (j=0;j<srcpt;j++)
      {
      PosT &rps=srcobj->Point(j);
      float d=ps.Distance2(rps);
      weight+=src->PointWeight(j)/d;
      }
    weight/=maxdistance;
    trg->SetPointWeight(i,weight);
    }
  return 0;
  }

//--------------------------------------------------

void CCopyWeightsDlg::OnOK() 
  {
  POSITION pos=wSelList.GetFirstSelectedItemPosition();
  ObjectData *obj=lod->Level(wRefLod.GetItemData(wRefLod.GetNextItem(-1,LVNI_FOCUSED )));
  comint.Begin(WString("CopyWeights"));	
  while (pos)
    {
    int i=wSelList.GetNextSelectedItem(pos);
    CString p=wSelList.GetItemText(i,0);
    comint.Run(new CICopyWeights(obj,p));
    }
  CDialog::OnOK();
  }

//--------------------------------------------------

