#if !defined(AFX_SELECTIONLIST_H__D16EEEAF_D25E_4277_A3B6_075A38CC7456__INCLUDED_)
#define AFX_SELECTIONLIST_H__D16EEEAF_D25E_4277_A3B6_075A38CC7456__INCLUDED_

#include "SelectionGroup.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectionList.h : header file
//

#define WM_KICKIDLE         0x036A

/////////////////////////////////////////////////////////////////////////////
// CSelectionList dialog

class CSelectionList : public CDialogBar
  {
  CWnd *notify;
  public:
    CSelectionGroup selgrp;
    bool selscan;
    WString FindAprNewName();
    int GetNamedSelectionIndex();
    CDIALOGBAR_RESIZEABLE
      CSelectionList();   // standard constructor
      CListCtrl list;
    LODObject *lod;
    bool labelediting;
    void UpdateListOfSel(LODObject *lodobject);
    void Create(CWnd *parent, int menuid);
    
    
    // Dialog Data
    //{{AFX_DATA(CSelectionList)
    enum 
      { IDD = IDD_SELLIST };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CSelectionList)
  public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CSelectionList)
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnDestroy();
    afx_msg void OnSlctRename();
    afx_msg void OnRclickList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnBeginlabeleditList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnSlctNew();
    afx_msg void OnSlctDelete();
    afx_msg void OnSlctRedefine();
    afx_msg void OnSlctWeights();
    afx_msg void OnDefineproxy();
    afx_msg void OnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnReleaseList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnSlctSetgroup();
    afx_msg void OnSlctRedefnorm();
    afx_msg void OnPluginPopupCommand(UINT cmd);
    //}}AFX_MSG
    
    DECLARE_MESSAGE_MAP()
  public:
    afx_msg BOOL OnSlctMoveselectiontoobject(UINT command);
    afx_msg void OnSlctExtractproxy();
    afx_msg void OnSlctExtractproxyAl();
    afx_msg void OnSlctDelempty();
  };

//--------------------------------------------------

//bool DefineProxy(ObjectData *obj, const char *oldname);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTIONLIST_H__D16EEEAF_D25E_4277_A3B6_075A38CC7456__INCLUDED_)
