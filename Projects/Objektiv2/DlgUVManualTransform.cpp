// DlgUVManualTransform.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgUVManualTransform.h"
#include ".\dlguvmanualtransform.h"


// DlgUVManualTransform dialog

IMPLEMENT_DYNAMIC(DlgUVManualTransform, CDialog)
DlgUVManualTransform::DlgUVManualTransform(IDlgUVManualTransform *notify,CWnd* pParent /*=NULL*/)
	: CDialog(DlgUVManualTransform::IDD, pParent),_notify(notify)
  , vOperation(0)
  , vUAxe(0)
  , vVAxe(0)
  , vRadius(0)
  , vCenter(0)
  , vCenterU(0)
  , vCenterV(0)
  , vUnits(0)
{
}

DlgUVManualTransform::~DlgUVManualTransform()
{
}

void DlgUVManualTransform::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Radio(pDX, IDC_OPEROFFSET, vOperation);
  DDX_Control(pDX, IDC_UAXE, wUAxe);
  DDX_Text(pDX, IDC_UAXE, vUAxe);
  DDX_Control(pDX, IDC_VAXE, wVAxe);
  DDX_Text(pDX, IDC_VAXE, vVAxe);
  DDX_Control(pDX, IDC_RADIUS, wRadius);
  DDX_Text(pDX, IDC_RADIUS, vRadius);
  DDX_Control(pDX, IDC_CENTER, wCenter);
  DDX_CBIndex(pDX, IDC_CENTER, vCenter);
  DDX_Control(pDX, IDC_CENTER_U, wCenterU);
  DDX_Text(pDX, IDC_CENTER_U, vCenterU);
  DDX_Control(pDX, IDC_CENTER_V, wCenterV);
  DDX_Text(pDX, IDC_CENTER_V, vCenterV);
  DDX_Radio(pDX, IDC_UNITS_UV, vUnits);
  DDX_Control(pDX,IDC_OPERROTATION,wRotation);
}


BEGIN_MESSAGE_MAP(DlgUVManualTransform, CDialog)
  ON_BN_CLICKED(IDC_APPLY, OnBnClickedApply)
  ON_BN_CLICKED(IDC_SHOWPREVIEW, OnBnClickedShowpreview)
  ON_BN_CLICKED(IDC_OPEROFFSET, OnBnClickedOperoffset)
  ON_BN_CLICKED(IDC_OPERROTATION, OnBnClickedOperrotation)
  ON_BN_CLICKED(IDC_OPERSCALE, OnBnClickedOperscale)
  ON_CBN_SELCHANGE(IDC_CENTER, OnCbnSelchangeCenter)
END_MESSAGE_MAP()


// DlgUVManualTransform message handlers

void DlgUVManualTransform::OnBnClickedApply()
{
  UpdateData(TRUE);
  _notify->ManualTransformPerformed(*this,false);
}

void DlgUVManualTransform::OnBnClickedShowpreview()
{
  UpdateData(TRUE);
  _notify->ManualTransformPerformed(*this,true);
}

void DlgUVManualTransform::OnOK()
{
  UpdateData(TRUE);
  _notify->ManualTransformPerformed(*this,false);

  OnCancel();
}

void DlgUVManualTransform::DialogRules()
{
  BOOL radius=wRotation.GetCheck()!=0;
  wUAxe.EnableWindow(!radius);
  wVAxe.EnableWindow(!radius);
  wRadius.EnableWindow(radius);
  int c=wCenter.GetCurSel();
  int m=wCenter.GetCount();
  wCenterU.EnableWindow(c+1==m);
  wCenterV.EnableWindow(c+1==m);
}

void DlgUVManualTransform::OnBnClickedOperoffset()
{
  DialogRules();
}

void DlgUVManualTransform::OnBnClickedOperrotation()
{
  DialogRules();
}

void DlgUVManualTransform::OnBnClickedOperscale()
{
  DialogRules();
}

void DlgUVManualTransform::OnCbnSelchangeCenter()
{
  DialogRules();
}

BOOL DlgUVManualTransform::OnInitDialog()
{
  CDialog::OnInitDialog();

  CenterWindow(GetParent());

  DialogRules();

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgUVManualTransform::OnCancel()
{
  DestroyWindow();
}
