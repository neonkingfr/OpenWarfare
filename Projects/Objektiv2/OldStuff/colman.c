#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ddraw.h>
#include <wingdi.h>
#include "colman.h"
#include "lzwc.h"

static char _stpalformat[30]="BGR888";


#ifndef int8
#define int8 char
#endif

#ifndef int16
#define int16 short
#endif

#ifndef int32
#define int32 long
#endif


int cmCreateColorModel(char *format, TCOLMODEL *mdl)
  {
  char *c=format,*d;
  int i=0;
  int bitcount=0;
  memset(mdl,0,sizeof(*mdl));
  while (*c>='A' && *c<='Z' || *c=='?') *c++;
  if (c==format) return CME_FORMATERROR;
  d=c;
  c--;
  while (*d>'0' && *d<'9') *d++;
  if (c==d) return CME_FORMATERROR;
  d--;
  if (c-(format-1)!=d-c) return CME_FORMATERROR;
  while (c>=format && i<8)
	if (*c!='?')
	  {
	  TCOLINFO *inf=mdl->part+i;
	  inf->name=*c;	
	  inf->bits=*d-'0';
	  inf->baseshift=bitcount;
	  inf->orgmask=(1<<inf->bits)-1;
	  inf->srcmask=(inf->orgmask)<<bitcount;
	  bitcount+=inf->bits;
	  if (bitcount>32) return CME_FORMATERROR;
	  c--;d--;i++;
	  if (inf->name=='P')
		if (mdl->palflags==0) mdl->palflags=1;
		else return CME_FORMATERROR;
	  }	
	else
	  {
	  bitcount+=*d-'0';
	  d--;
	  c--;
	  }	
  mdl->templat|=0xFFFFFFFF;
  mdl->srcpixelsize=(bitcount+7)>>3;
  mdl->colcount=i;
  return CME_OK;
  }

static TCOLINFO *FindColor(TCOLMODEL *mdl,char name)
  {
  int i;

  for (i=0;i<mdl->colcount;i++) if (mdl->part[i].name==name) return mdl->part+i;
  return NULL;
  }

static void CalculateConvert(TCOLINFO *src1, TCOLINFO *src2, TCOLMODEL *res, int colindex, int pal)
  {
  res->templat&=~src2->srcmask;
  if (src1->bits<src2->bits)
	{
	COLULONG mask1,mask2;
	res->part[colindex].srcmask=src1->srcmask;
	res->part[colindex].baseshift=(src1->baseshift-(src2->baseshift+(src2->bits-src1->bits))) & 0x1F;
	mask1=(src1->orgmask>>1)<<(src2->baseshift+(src2->bits-src1->bits)+1);
	mask2=src2->orgmask<<(src2->baseshift);
	res->part[colindex].orgmask=mask1^mask2;
	}
  else
	{
	res->part[colindex].srcmask=src2->orgmask<<(src1->baseshift+(src1->bits-src2->bits));
	res->part[colindex].baseshift=((src1->baseshift+(src1->bits-src2->bits))-src2->baseshift) & 0x1F;
	res->part[colindex].orgmask=0;
	}
  res->palflags>>=1;
  if (pal) res->palflags|=0x80;
  }

static int Calc(TCOLMODEL *mdl1, TCOLMODEL *mdl2, TCOLMODEL *palm, TCOLMODEL *diff, int c, int i, int pal)
  {
  TCOLINFO *fnd;

  fnd=FindColor(mdl2,mdl1->part[i].name);
  if (fnd!=NULL) CalculateConvert(mdl1->part+i,fnd,diff,c++,pal);
  else
    switch (mdl1->part[i].name)
	  {
	  case 'A':   if (FindColor(mdl1,'R') || FindColor(mdl1,'G') || FindColor(mdl1,'B')) break;
	  case 'I':   if (FindColor(mdl1,'P')) break;
				  fnd=FindColor(mdl2,'R');
				  if (fnd!=NULL && FindColor(mdl1,'R')==NULL) CalculateConvert(mdl1->part+i,fnd,diff,c++,pal);
				  fnd=FindColor(mdl2,'G');
				  if (fnd!=NULL && FindColor(mdl1,'G')==NULL) CalculateConvert(mdl1->part+i,fnd,diff,c++,pal);
				  fnd=FindColor(mdl2,'B');
				  if (fnd!=NULL && FindColor(mdl1,'B')==NULL) CalculateConvert(mdl1->part+i,fnd,diff,c++,pal);
				  fnd=FindColor(mdl2,'A');
				  if (fnd!=NULL && FindColor(mdl1,'A')==NULL) CalculateConvert(mdl1->part+i,fnd,diff,c++,pal);
				  fnd=FindColor(mdl2,'I');
				  if (fnd!=NULL && FindColor(mdl1,'I')==NULL) CalculateConvert(mdl1->part+i,fnd,diff,c++,pal);
				  break;
	  case 'R':
	  case 'G':
	  case 'B': fnd=FindColor(mdl2,'I');
				  if (fnd!=NULL && FindColor(mdl1,'I')==NULL) CalculateConvert(mdl1->part+i,fnd,diff,c++,pal);
				  else
					{
					fnd=FindColor(mdl2,'A');
					if (fnd!=NULL && FindColor(mdl1,'A')==NULL) CalculateConvert(mdl1->part+i,fnd,diff,c++,pal);
					}
				  break;
	  case 'P': 
		  {
		  int j;
		  if (palm==NULL) break;
		  diff->index=mdl1->part[i];
		  for (j=0;j<palm->colcount;j++) 
			if (FindColor(mdl1,palm->part[j].name)==NULL)
			  {
			  c=Calc(palm,mdl2,NULL,diff,c,j,1);
			  }
		  break;
		  }
	} 
  return c;
  }  

int cmCreateDiffModel(TCOLMODEL *mdl1, TCOLMODEL *mdl2,TCOLMODEL *pal, TCOLMODEL *diff)
  {
  int i,c;
  memset(diff,0,sizeof(*diff));
  diff->srcpixelsize=mdl1->srcpixelsize;
  diff->trgpixelsize=mdl2->srcpixelsize;
  if (pal!=NULL && mdl1->palflags) diff->palitem=pal->srcpixelsize;
  diff->templat=mdl2->templat;
  for (i=0,c=0;i<mdl1->colcount;i++) c=Calc(mdl1,mdl2,pal,diff,c,i,0);
  diff->colcount=c;
  diff->palflags>>=(8-c);
  return CME_OK;
  }
  

int cmCreateConvModel(char *srcformat,char *trgformat, char *palformat, TCOLMODEL *conv)
  {
  TCOLMODEL mdl1,mdl2,pal;
  int res;

  res=cmCreateColorModel(srcformat,&mdl1);
  if (res) return res;
  res=cmCreateColorModel(trgformat,&mdl2);
  if (res) return res;
  if (palformat!=NULL)
	{
	res=cmCreateColorModel(palformat,&pal);
	if (res) return res;
	}
  else
	{
	res=cmCreateColorModel(_stpalformat,&pal);
	if (res) return res;
	}
  res=cmCreateDiffModel(&mdl1,&mdl2,&pal,conv);
  return res;
  }
  
int cmSetPaletteFormat(char *format)
  {
  strncpy(_stpalformat,format,sizeof(_stpalformat));
  return CME_OK;
  }

typedef void (*TREAD)(unsigned int32 *, void *, int, int);
typedef void (*TWRITE)(unsigned int32 *, void *, int);
typedef void (*TCONV)(TCOLMODEL *,unsigned int32 *, unsigned int32 *, int);


static void readLine8(unsigned int32 *line, void *src, int size, int skip)
  {
  unsigned int8 *s=(unsigned int8 *)src;
  do
	{
	*line++=*s;
	s+=skip;
	}
  while (--size);
  }

static void readLine16(unsigned int32 *line, void *src, int size, int skip)
  {
  unsigned int16 *s=(unsigned int16 *)src;
  do
	{
	*line++=*s;
	s+=skip;
	}
  while (--size);
  }

static void readLine24(unsigned int32 *line, void *src, int size, int skip)
  {
  unsigned int32 *s=(unsigned int32 *)src;
  do
	{
	*line++=*s;
	s=(unsigned int32 *)((char *)s+3*skip);
	}
  while (--size);
  }

static void readLine32(unsigned int32 *line, void *src, int size, int skip)
  {
  unsigned int32 *s=(unsigned int32 *)src;
  do
	{
	*line++=*s;
	s+=skip;
	}
  while (--size);
  }

static void storeLine8(unsigned int32 *line, void *src, int size)
  {
  unsigned int8 *s=(unsigned int8 *)src;
  do		
	*s++=(unsigned int8)(*line++);				
  while (--size);
  }

static void storeLine16(unsigned int32 *line, void *src, int size)
  {
  unsigned int16 *s=(unsigned int16 *)src;
  do
	*s++=(unsigned int16)(*line++);	
  while (--size);
  }

static void storeLine24(unsigned int32 *line, void *src, int size)
  {
  unsigned int8 *s=(unsigned int8 *)src;
  do
	{
	*s++=(unsigned int8)(line[0]);
	*s++=(unsigned int8)(line[0]>>8);
	*s++=(unsigned int8)(line[0]>>16);
	line++;
	}
  while (--size);
  }

static void storeLine32(unsigned int32 *line, void *src, int size)
  {
  unsigned int32 *s=(unsigned int32 *)src;
  do
	*s++=*line++;
  while (--size);
  }


#define convBegin1 \
  unsigned int32 indexval,dirval;	\
  do					\
	{									\
	register unsigned int32 acc=0;			\
	register unsigned int8 rotor=mdl->palflags;	\
	register unsigned int32 index;		\
	TCOLINFO *info=&(mdl->index);	\
	if (mdl->palflags) \
	  {		 \
	  index=*line & mdl->index.srcmask;	\
	  index=_rotr(index & info->srcmask,info->baseshift); \
	  indexval=pal[index];\
	  }	\
	info=mdl->part;	\
	dirval=*line;
	
#define convLoop(i)	\
	  if (rotor & (1<<i)) index=indexval; else index=dirval;  \
	  index=_rotr(index & info->srcmask,info->baseshift); \
	  if (index & info->orgmask) index|=info->orgmask;	\
	  acc|=index; \
	  info++
		
#define convEnd	\
	acc|=mdl->templat;\
	*line++=acc;\
	} \
  while (--size)

static void convLine1(TCOLMODEL *mdl,unsigned int32 *line, unsigned int32 *pal, int size)
	{
	convBegin1;
	convLoop(0);
	convEnd;
	}	

static void convLine2(TCOLMODEL *mdl,unsigned int32 *line, unsigned int32 *pal, int size)
	{
	convBegin1;
	convLoop(0);
	convLoop(1);
	convEnd;
	}	
	  
static void convLine3(TCOLMODEL *mdl,unsigned int32 *line, unsigned int32 *pal, int size)
	{
	convBegin1;
	convLoop(0);
	convLoop(1);
	convLoop(2);
	convEnd;
	}	

static void convLine4(TCOLMODEL *mdl,unsigned int32 *line, unsigned int32 *pal, int size)
	{
	convBegin1;
	convLoop(0);
	convLoop(1);
	convLoop(2);
	convLoop(3);
	convEnd;
	}	

static void convLine5(TCOLMODEL *mdl,unsigned int32 *line, unsigned int32 *pal, int size)
	{
	convBegin1;
	convLoop(0);
	convLoop(1);
	convLoop(2);
	convLoop(3);
	convLoop(4);
	convEnd;
	}	

static void convLine6(TCOLMODEL *mdl,unsigned int32 *line, unsigned int32 *pal, int size)
	{
	convBegin1;
	convLoop(0);
	convLoop(1);
	convLoop(2);
	convLoop(3);
	convLoop(4);
	convLoop(5);
	convEnd;
	}	

static void convLine7(TCOLMODEL *mdl,unsigned int32 *line, unsigned int32 *pal, int size)
	{
	convBegin1;
	convLoop(0);
	convLoop(1);
	convLoop(2);
	convLoop(3);
	convLoop(4);
	convLoop(5);
	convLoop(6);
	convEnd;
	}	

static void convLine8(TCOLMODEL *mdl,unsigned int32 *line, unsigned int32 *pal, int size)
	{
	convBegin1;
	convLoop(0);
	convLoop(1);
	convLoop(2);
	convLoop(3);
	convLoop(4);
	convLoop(5);
	convLoop(6);
	convLoop(7);
	convEnd;
	}	

/*
static void convLine(TCOLMODEL *mdl,unsigned int32 *line, unsigned int32 *pal, int size)
  {
  int i;
  do
	{
	register unsigned int32 acc=0;
	register unsigned int8 rotor=mdl->palflags;
	register unsigned int32 index;
	TCOLINFO *info;
	for (i=0,info=mdl->part;i<mdl->colcount;i++,info++)
	  {
	  if (rotor & 1) 
		{
		index=_rotr(*line & mdl->index.srcmask,mdl->index.baseshift);
		index=pal[index];		
		}
	  else
		index=*line;
	  index=_rotr(index & info->srcmask,info->baseshift);
	  if (index & info->orgmask) index|=info->orgmask;
	  acc|=index;
	  rotor>>=1;
	  }
	acc|=mdl->templat;
	*line++=acc;
	}
  while (--size);
  }
*/
static TREAD readProcs[]=
  {readLine8,readLine16,readLine24,readLine32};
static TWRITE storeProcs[]=
  {storeLine8,storeLine16,storeLine24,storeLine32};
static TCONV convProcs[]=
  {convLine1,convLine2,convLine3,convLine4,convLine5,convLine6,convLine7,convLine8};


#define ASSIGNPROCS(conv,readLine,storeLine,convLine)\
  if(conv->srcpixelsize>0 && conv->srcpixelsize<5) readLine=readProcs[conv->srcpixelsize-1];\
  if(conv->trgpixelsize>0 && conv->trgpixelsize<5) storeLine=storeProcs[conv->trgpixelsize-1];\
  if(conv->colcount>0 && conv->colcount<9) convLine=convProcs[conv->colcount-1];  \

int cmDoConvert(TPICINFO *info,TCOLMODEL *conv)
  {
  TREAD readLine;
  TWRITE storeLine;
  TCONV convLine;
  unsigned int32 palette[256];
  unsigned int32 *line;
  char freeit;
  int y,x;  
  int skip=info->reduce<1?1:info->reduce;
  char *src,*trg;

  ASSIGNPROCS(conv,readLine,storeLine,convLine);
  if (info->xsize<1 || info->ysize<1) return CME_PICTUREFORMATERROR;
  src=info->src;
  trg=info->trg;
  if (info->srcpal!=NULL && conv->palitem) 
	{
	int entries=info->palentries==0?256:info->palentries;
	switch(conv->palitem)
	  {
	  case 1:readLine8(palette,info->srcpal,entries,1);break;
	  case 2:readLine16(palette,info->srcpal,entries,1);break;
	  case 3:readLine24(palette,info->srcpal,entries,1);break;
	  case 4:readLine32(palette,info->srcpal,entries,1);break;
	  default: return CME_FORMATERROR;
	  }
	}
  if (src==NULL || trg==NULL) return CME_FORMATERROR;
  x=info->xsize/skip;
  line=(unsigned int32 *)alloca(((info->xsize+skip-1)/skip)*4);
  if (line==NULL)
	{
	line=(unsigned int32 *)malloc(((info->xsize+skip-1)/skip)*4);
	if (line==NULL) return CME_NOMEMORY;
	freeit=1;
	}
  else
	freeit=0;  
  for (y=0;y<info->ysize;y+=skip)
	{
	readLine(line,src,x,skip);
	convLine(conv,line,palette,x);
	storeLine(line,trg,x);
	src+=info->srclinelen*skip;
	trg+=info->trglinelen;
	}
  if (freeit) free(line);
  return CME_OK;
  }

int cmDoConvertRaw(TCOLMODEL *conv,void *src, void *trg, void *pal, int len,int palentries)
  {
  unsigned int32 *line;
  TREAD readLine;
  TWRITE storeLine;
  TCONV convLine;
  unsigned int32 palette[256];

  
  ASSIGNPROCS(conv,readLine,storeLine,convLine);
  line=(unsigned int32 *)alloca(len*4);
  if (conv->palflags) 
	{
	switch(conv->palitem)
	  {
	  case 1:readLine8(palette,pal,palentries,1);break;
	  case 2:readLine16(palette,pal,palentries,1);break;
	  case 3:readLine24(palette,pal,palentries,1);break;
	  case 4:readLine32(palette,pal,palentries,1);break;
	  default: return CME_FORMATERROR;
	  }
	}
  readLine(line,src,len,1);
  convLine(conv,line,palette,len);
  storeLine(line,trg,len);  
  return 0;
  }


unsigned long cmCalcSpace(TPICINFO *info, TCOLMODEL *conv,int flags)
  {
  int skip=info->reduce<1?1:info->reduce;
  long xsize=(info->xsize+skip-1)/skip*conv->trgpixelsize;
  long mask=flags-1;
  mask=~mask;
  if (flags==CMC_DONTCARE)
	xsize=info->trglinelen;
  else
	xsize=(xsize+flags-1)&mask;
  info->trglinelen=xsize;
  return (info->ysize+skip-1)/skip*info->trglinelen;
  }

TCOLMODEL *cmAddFormatToList(TCOLMODEL *mdl, char *format)
  {
  TCOLMODEL *nmdl=malloc(sizeof(TCOLMODEL));
  if (nmdl==NULL) return mdl;
  if (cmCreateColorModel(format, nmdl)) 
	{
	free(nmdl);
	return mdl;
	}
  nmdl->next=mdl;
  return nmdl;
  }

void cmPurgeFormatList(TCOLMODEL *mdl)
  {
  TCOLMODEL *c;

  while (mdl!=NULL)
	{
	c=mdl;
	mdl=c->next;
	free(c);
	}
  }
TCOLMODEL *cmCreateBestConvModel(TCOLMODEL *list, char *format, char *palformat, TCOLMODEL *conv)
  {
  return cmCreateBestConvModelEx(list, format,palformat, conv,0);
  }

TCOLMODEL *cmCreateBestConvModelEx(TCOLMODEL *list,char *format, char *palformat,  TCOLMODEL *conv, int hint)
  {
  TCOLMODEL mdl1,pal,*best=NULL;
  int res;
  int points=-999999;

  res=cmCreateColorModel(format,&mdl1);
  if (res) return NULL;
  if (palformat!=NULL)
	{
	res=cmCreateColorModel(palformat,&pal);
	if (res) return NULL;
	}
  else
	{
	res=cmCreateColorModel(_stpalformat,&pal);
	if (res) return NULL;
	}
  while(list!=NULL)
	{
	int pt=0;
	char palet=0;
	int i,j;	
	TCOLINFO *info;
	TCOLINFO *fnd;

	for (i=0;i<mdl1.colcount;i++)
	  {
	  if (mdl1.part[i].name!='P')					  
		{
		int bits;
		info=mdl1.part+i;
		fnd=FindColor(list,info->name);
		if (fnd==NULL && (info->name=='I' || info->name=='A'))
		  {
		  fnd=FindColor(list,'R');
		  if (fnd==NULL) fnd=FindColor(list,'G');
		  if (fnd==NULL) fnd=FindColor(list,'B');
		  if (fnd==NULL) fnd=FindColor(list,'I');
		  if (fnd==NULL) fnd=FindColor(list,'A');
		  pt-=((info->name=='A')+1)*100;
		  }
		if (fnd!=NULL)
		  if (hint)
			{
			int c=list->colcount;
			bits=hint*info->bits/c;
			if (bits>info->bits) bits=info->bits;
			}
		  else bits=info->bits;
		if (fnd==NULL) pt-=400;
		else if (fnd->bits<bits) pt-=(1<<(bits-fnd->bits))+10;
		else if (fnd->bits>bits) pt-=fnd->bits-bits;
		}
	  else
		{
		info=mdl1.part+i;
		fnd=FindColor(list,info->name);
		if (fnd==NULL || fnd->bits<mdl1.part[i].bits)
		  for (j=0;j<pal.colcount;j++)
			{
			info=pal.part+j;
  			fnd=FindColor(list,info->name);
			if (fnd==NULL) pt-=200;
			else if (fnd->bits<info->bits) pt-=(1<<(info->bits-fnd->bits))+10;
			else if (fnd->bits>info->bits) pt-=fnd->bits-info->bits;
			}	
		else palet=1;
		}
	  }	
	if (!palet) for (i=0;i<list->colcount;i++) if (list->part[i].name=='P') 
	  {pt=points+pt;break;}
	if (list->colcount>mdl1.colcount) pt-=20*(list->colcount-mdl1.colcount);
	if (pt>points) {best=list;points=pt;}
	list=list->next;
	}
  if (best==NULL) return NULL;
  res=cmCreateDiffModel(&mdl1,best,&pal,conv);
  if (res) return NULL;
  return best;
  }
#if 0
int cmFormat2DDX2(TCOLMODEL *mdl, LPDDPIXELFORMAT lpddpx)
  {
  int i;
  int bits;
  memset(lpddpx,0,sizeof(DDPIXELFORMAT));
  lpddpx->dwSize=sizeof(DDPIXELFORMAT);
  lpddpx->dwFlags=0;
  for (i=0;i<mdl->colcount;i++)
	{
	switch (mdl->part[i].name)
	  {
	  case 'I': lpddpx->dwFlags |=DDPF_LUMINANCE ;
				lpddpx->dwLuminanceBitMask=mdl->part[i].srcmask;
				bits+=lpddpx->dwLuminanceBitCount=mdl->part[i].bits;
				break;
	  case 'A': if (mdl->colcount==1)
				  {
				  lpddpx->dwFlags |=DDPF_ALPHA;
				  bits+=lpddpx->dwAlphaBitDepth=mdl->part[i].bits;
				  lpddpx->dwRGBAlphaBitMask=mdl->part[i].srcmask;
				  }
				else
				  {
				  lpddpx->dwFlags |=DDPF_ALPHAPIXELS;
				  lpddpx->dwRGBAlphaBitMask=mdl->part[i].srcmask;
				  }
				break;
	  case 'P': switch (mdl->part[i].bits)
				  { 
				  case 1: lpddpx->dwFlags|=DDPF_PALETTEINDEXED1;break;
				  case 2: lpddpx->dwFlags|=DDPF_PALETTEINDEXED2;break;
				  case 4: lpddpx->dwFlags|=DDPF_PALETTEINDEXED4;break;
				  case 8: lpddpx->dwFlags|=DDPF_PALETTEINDEXED8;break;
				  default: return CME_INVALIDPIXELFORMAT;
				  }
				  lpddpx->dwFlags |=DDPF_RGB;  
				  break;
	  case 'R':	lpddpx->dwRBitMask=mdl->part[i].srcmask;
				lpddpx->dwFlags |=DDPF_RGB;
				bits+=mdl->part[i].bits;
				break;
	  case 'B': lpddpx->dwBBitMask=mdl->part[i].srcmask;
				lpddpx->dwFlags |=DDPF_RGB;
				bits+=mdl->part[i].bits;
				break;
	  case 'G': lpddpx->dwGBitMask=mdl->part[i].srcmask;
				lpddpx->dwFlags |=DDPF_RGB;
				bits+=mdl->part[i].bits;
				break;	  
/*	  case 'D': if (mdl->colcount==1)
				  {
				  lpddpx->dwFlags |=DDPF_ZBUFFER;
				  bits+=lpddpx->dwZBufferBitDepth=mdl->part[i].bits;
				  lpddpx->dwZBitMask=mdl->part[i].srcmask;
				  }
				else
				  {
				  lpddpx->dwFlags |=DDPF_ZPIXELS;
				  lpddpx->dwZBitMask=mdl->part[i].srcmask;
				  }
				break;
*/
	  default:	return CME_INVALIDPIXELFORMAT;				  
	  }	
	}
  lpddpx->dwRGBBitCount=mdl->srcpixelsize*8;
  return CME_OK ;
  }
int cmFormat2DDX(char *format, LPDDPIXELFORMAT lpddpx)
  {
  int res;
  int bits=0;
  TCOLMODEL mdl;
  res=cmCreateColorModel(format,&mdl);
  if (res) return res;
  return cmFormat2DDX2(&mdl,lpddpx);
  }

static void MakeMask(char *to, unsigned long bits, char znak)
  {
  int i;
  for (i=0;i<32;i++) if (bits & (1<<i)) to[31-i]=znak;
  }

int cmDDX2Format(LPDDPIXELFORMAT lpddpx, char *format)
  {
  char mask[32];
  char last;
  char cisla[32]="";
  int i,p;
  memset(mask,'?',32);
  if (lpddpx->dwSize<sizeof(DDPIXELFORMAT)) return CME_INVALIDPIXELFORMAT;
  if (lpddpx->dwFlags & DDPF_RGB)
	{
	MakeMask(mask,lpddpx->dwRBitMask,'R');
	MakeMask(mask,lpddpx->dwGBitMask,'G');
	MakeMask(mask,lpddpx->dwBBitMask,'B');
	}
/*  if (lpddpx->dwFlags & DDPF_ZBUFFER)
	{
	MakeMask(mask,(1<<lpddpx->dwZBufferBitDepth)-1,"D");
	}
  if (lpddpx->dwFlags & DDPF_ZPIXELS)
	{
	MakeMask(mask,lpddpx->dwZBitMask,"D");
	}*/
  if (lpddpx->dwFlags & DDPF_ALPHA)
	{
	MakeMask(mask,(1<<lpddpx->dwAlphaBitDepth)-1,'A');
	}
  if (lpddpx->dwFlags & DDPF_ALPHAPIXELS)
	{
	MakeMask(mask,lpddpx->dwRGBAlphaBitMask,'A');
	}
  if (lpddpx->dwFlags & DDPF_LUMINANCE)
	{
	MakeMask(mask,lpddpx->dwLuminanceBitMask,'I');
	}
  if (lpddpx->dwFlags & DDPF_PALETTEINDEXED1) MakeMask(mask,0x1,'P');
  if (lpddpx->dwFlags & DDPF_PALETTEINDEXED2) MakeMask(mask,0x3,'P');
  if (lpddpx->dwFlags & DDPF_PALETTEINDEXED4) MakeMask(mask,0xF,'P');
  if (lpddpx->dwFlags & DDPF_PALETTEINDEXED8) MakeMask(mask,0xFF,'P');	
  last='.';
  for (i=lpddpx->dwRGBBitCount;i<32;i++) mask[31-i]=last;
  for (i=0,p=-1;i<32;i++)
 	if (mask[i]==last) if (p>=0) cisla[p]++; else;
	else 
	  {	  
	  p++;
	  last=mask[i];
	  format[p]=mask[i];
	  cisla[p]='1';
	  }
  format[p+1]=0;
  cisla[p+1]=0;
  strcat(format,cisla);
  return CME_OK;
  }
#endif
#include "g3dfiles.c"

//------------------- TGA ---------------------------
#pragma pack (1)
typedef struct _tgaheader
  {
  unsigned short sign1,sign2;
  unsigned long unknw1,unknw2;
  unsigned short xsize,ysize;
  char format;
  char flags;
  }TGAHEADER;

#pragma pack ()


static TUNIPICTURE *cmLoadTGAf(HANDLE f)
  {
  TGAHEADER thd;
  TUNIPICTURE *pic;
  char palette[768];
  unsigned long datasize;
  unsigned long hdsize;
  unsigned long blocksize;
  char pal;
  int pixel;
  char *format;
  void *data;
  char *c;
  int i;
  unsigned long fsize;

  if (!gread(&thd,sizeof(thd),1,f)) return NULL;
  pal=thd.format==8 && thd.sign2==1;
  if (pal)
	if (!gread(palette,768,1,f)) return NULL;
  switch (thd.format)
	{
	case 8: if (pal) format="P8|BGR888";else format="I8"; break;
	case 15: format="RGB555";break;
	case 16: format="RGB555";break;
	case 24: format="RGB888";break;
	case 32: format="ARGB8888";break;
	default: return NULL;
	}
  pixel=(thd.format+7)/8;
  datasize=pixel*thd.xsize*thd.ysize;
  fsize=GetFileSize(f,NULL);
  if (datasize>fsize) return NULL;
  if (datasize<1) return NULL;
  hdsize=sizeof(TUNIPICTURE)+strlen(format);
  blocksize=datasize+hdsize;
  if (pal) blocksize+=sizeof(palette);
  pic=malloc(blocksize);
  if (pic==NULL) return NULL;
  pic->xsize=thd.xsize;
  pic->ysize=thd.ysize;
  pic->xlinelen=thd.xsize*pixel;
  pic->dataofset=hdsize;
  pic->totaldata=datasize;
  pic->palettelen=(pal)?sizeof(palette):0;
  pic->transparent=0xffffffff;
  pic->mipmapcount=0;  
  strcpy(pic->format,format);
  data=cmGetData(pic);
  if (thd.flags & 0x20)
  {
  c=(char *)data;
  for (i=0;i<pic->ysize;i++,c+=pic->xlinelen)	
	if (!gread(c,pic->xlinelen,1,f)) {free(pic);return NULL;}
  }
  else
  {
  c=(char *)data+(pic->ysize-1)*pic->xlinelen;
  for (i=0;i<pic->ysize;i++,c-=pic->xlinelen)	
	if (!gread(c,pic->xlinelen,1,f)) {free(pic);return NULL;}
  }
  if (pal)
	{
	data=cmGetPalette(pic);
	memcpy(data,palette,sizeof(palette));
	}
  return pic;
  }

TUNIPICTURE *cmLoadTGA(char *name)
  {
  HANDLE f;
  TUNIPICTURE *pic;

  f=fopen(name,"rb");
  if (f==NULL) return NULL;
  pic=cmLoadTGAf(f);
  fclose(f);
  return pic;
  }


static void decomprimate_line_256(unsigned char *src,unsigned char *trg,int linelen,int *srcstep)
  {
  unsigned char *srcsave;

  srcsave=src;
  while (linelen--)
     {
     if (*src>=0xc0)
        {
        int i;
        i=*src++ & 0x3f;memset(trg,*src++,i);
        trg+=i;linelen-=i-1;
        }
     else
        *trg++=*src++;
     }
  *srcstep=src-srcsave;
  }


//------------------- PCX ---------------------------


typedef struct pcxrecord
     {
     unsigned short id;
     unsigned char encoding;
     unsigned char bitperpixel;
     unsigned short xmin,ymin,xmax,ymax;
     unsigned short hdpi,vdpi;
     unsigned char colormap[48];
     unsigned char reserved;
     unsigned char mplanes;
     unsigned short bytesperline;
     unsigned short paleteinfo;
     unsigned short hscreen,vscreen;
     unsigned char filler[54];
     }PCXHEADER;

static TUNIPICTURE *LoadPcxf(HANDLE f)
    {
  unsigned char *ptr1;
  unsigned char *ptr3;  
  PCXHEADER pcxdata;
  int xsize,ysize;
  TUNIPICTURE *pic;
  long pos,size,psize;
  char palette[768];
  char *format;
  unsigned char *buff;
  
  if (!gread(&pcxdata,sizeof(pcxdata),1,f) || pcxdata.id!=0x50A)
	{
	gseek(f,0,SEEK_SET);return cmLoadTGAf(f); //NOT a PCX - Try TGA
	}
  pos=gtell(f);
  gseek(f,0,SEEK_END);
  gseek(f,-768,SEEK_CUR);
  size=gtell(f)-pos;
  if (!gread(palette,768,1,f)) return NULL;  
  gseek(f,pos,SEEK_SET);
  format="P8|BGR888";
  buff=malloc(size);
  if (buff==NULL) return NULL;
  if (!gread(buff,size,1,f)) {free(buff);return NULL;}
  xsize=pcxdata.xmax-pcxdata.xmin+1;
  ysize=pcxdata.ymax-pcxdata.ymin+1;
  psize=sizeof(*pic)+strlen(format)+pcxdata.bytesperline*ysize+768;  
  pic=malloc(psize);
  if (pic==NULL) {free(buff);return NULL;}
  ptr3=buff;
  pic->dataofset=sizeof(*pic)+strlen(format);
  pic->totaldata=pcxdata.bytesperline*ysize;
  pic->xsize=xsize;
  pic->ysize=ysize;
  pic->xlinelen=pcxdata.bytesperline;
  pic->transparent=0;
  pic->palettelen=768;
  pic->mipmapcount=0;  
  memcpy(cmGetPalette(pic),palette,768);
  strcpy(pic->format,format);
  ptr1=cmGetData(pic);
  ysize++;
  while (--ysize)
     {
     int step;
     decomprimate_line_256(ptr3,ptr1,pcxdata.bytesperline,&step);
     ptr1+=pic->xlinelen;
     ptr3+=step;
     }
  free(buff);
  return pic;
  }

//------------------- BMP --------------------------


static TUNIPICTURE *LoadBMPf(HANDLE f)
  {
  BITMAPFILEHEADER header;
  BITMAPINFOHEADER info;
  TUNIPICTURE uni,*pic;
  char *format;
  RGBQUAD palette[256];
  long masks[3];
  char pal;
  unsigned char *bmpbot;

  if (!gread(&header,sizeof(header),1,f) || (header.bfType!=('B'+'M'*256)))
	{gseek(f,0,SEEK_SET); return LoadPcxf(f);}	//NOT a BMP - try PCX
  if (!gread(&info,sizeof(BITMAPINFOHEADER),1,f)) return NULL;
  uni.xsize=info.biWidth;
  uni.ysize=info.biHeight;
  switch (info.biBitCount)
	{
	case 8:format="P8|?RGB8888";uni.xlinelen=1;
	  	   if (!gread(palette,sizeof(palette),1,f)) return NULL;
		   pal=1;
		   break;
	case 16:uni.xlinelen=2;
			if (info.biCompression==BI_BITFIELDS)
			  {
			  if (!gread(masks,sizeof(masks),1,f)) return NULL;
			  if (masks[1]==0x03E0) format="RGB555";
			  else if (masks[1]==0x07E0) format="RGB565";
			  else return NULL;
			  }	
			else format="RGB555";
			pal=0;
			break;
	case 24:uni.xlinelen=3;format="RGB888";pal=0;break;
	case 32:uni.xlinelen=4;format="?RGB8888";
			if (info.biCompression==BI_BITFIELDS)			  
			  if (!gread(masks,sizeof(masks),1,f)) return NULL;
			  else;
			else;	
			pal=0;
			break;
	default: return NULL;
	}
  uni.xlinelen=(uni.xsize*uni.xlinelen+3) & ~3;
  uni.totaldata=uni.xlinelen*uni.ysize;
  uni.palettelen=pal?sizeof(palette):0;
  uni.dataofset=sizeof(uni)+strlen(format);
  uni.transparent=0xffffffff;
  uni.mipmapcount=0;
  pic=(TUNIPICTURE *)malloc(cmGetSize(&uni));
  *pic=uni;
  strcpy(pic->format,format);
  if (pic==NULL) return NULL;
  if (pal) memcpy(cmGetPalette(pic),palette,sizeof(palette));
  bmpbot=(unsigned char *)cmGetData(pic)+pic->xlinelen*(uni.ysize-1);
  if (info.biCompression==BI_RLE8)
	{
	int znak;
	int pixel;
	int i;
	unsigned char *ptr=bmpbot;
	znak=ggetc(f);
	while (znak!=EOF)
	  {
	  if (znak==0)
		{
		znak=ggetc(f);
		switch (znak)
		  {
		  case 0: bmpbot-=pic->xlinelen;ptr=bmpbot;break;
		  case 1: goto endbmp;
		  case 2: i=ggetc(f); ptr+=i;
				  i=ggetc(f); ptr+=pic->xlinelen*i;bmpbot+=pic->xlinelen*i;break;
		  case EOF: goto endbmp;
		  default:znak;
				  for(i=0;i<znak;i++) 
					{pixel=ggetc(f);*ptr++=pixel;}
				  if (znak & 1) ggetc(f);
				  break;
		  }				  				  
		}
	  else
		{
  		pixel=ggetc(f);
		for (i=0;i<znak;i++) *ptr++=pixel;
		}
  	  znak=ggetc(f);
	  }
endbmp:;
	}
  else if (info.biCompression==BI_RGB || info.biCompression==BI_BITFIELDS)
	{
	unsigned int i;
	for (i=0;i<pic->ysize;i++)
	  {
	  if (!gread(bmpbot,pic->xlinelen,1,f)) {free(pic);return NULL;}
	  bmpbot-=pic->xlinelen;
	  }
	}
  else {free(pic);return NULL;}
  return pic;
  }



//------------------- UNI ---------------------------


#define UNIMAGIC "UNIPIC"
#define UNIMAGICLEN 7
#define UNIVER 0x100

TUNIPICTURE *(*LoadUniFCallBack)(HANDLE f)=LoadBMPf;

#pragma pack(1)
typedef struct _tagUniHeader
  {
  char magic[UNIMAGICLEN];
  char compressed;
  short version;
  short padding;
  unsigned long needspace;
  unsigned long size;  
  }UNIHEADER;
#pragma pack()

  
TUNIPICTURE *LoadUnif(HANDLE f)
  {
  UNIHEADER hd;
  TUNIPICTURE *pic;
  if (!gread(&hd,sizeof(hd),1,f) || memcmp(hd.magic,UNIMAGIC,UNIMAGICLEN)!=0)
    {gseek(f,0,SEEK_SET);return LoadUniFCallBack(f);}	//not unipicture - try BMP
  if (hd.version>UNIVER) return NULL;
  pic=(TUNIPICTURE *)malloc(hd.needspace);
  if (hd.compressed)
	{
	char freeit;
	void *p;
	
	p=(void *)alloca(hd.size);
	if (p==NULL) {p=malloc(hd.size);freeit=1;}else freeit=0;
	if (p==NULL) return NULL;
	if (!gread(p,hd.size,1,f)) {if (freeit) free(p);return NULL;}
	init_lzw_compressor(8);
	lzw_decode(p,(unsigned char *)pic);
	if (freeit) free(p);
	}
  else
	if (!gread(pic,hd.needspace,1,f)) return NULL;
  return pic;
  }

static char *importfilename;
char *cmGetImportName()
  {
  return importfilename;
  }

TUNIPICTURE *cmLoadUni(char *filename)
  {
  TUNIPICTURE *pic;
  HANDLE f;

  importfilename=filename;
  f=gopenread(filename);
  if (f==NULL) return NULL;
  pic=LoadUnif(f);
  gclose(f);
  return pic;
  }

int SaveUnif(FILE *f,TUNIPICTURE *pic)
  {
  UNIHEADER hd;
  void *p;
  char freeit=1;
  long size=cmGetSize(pic);
  long comp;
  int res;
  p=malloc(size+16);
  if (p==NULL) return -1;
  init_lzw_compressor(8);
  memset(p,0,sizeof(size));
  comp=lzw_encode((unsigned char *)pic,p,size);
  if (comp==-1)
	{
	hd.needspace=size;
	hd.size=size;
	hd.compressed=0;
	if (freeit) {free(p);freeit=0;}
	p=(void *)pic;
	}
  else
	{
	hd.needspace=size;
	hd.size=comp;
	hd.compressed=1;
	size=comp;
	}
  memcpy(hd.magic,UNIMAGIC,UNIMAGICLEN);
  hd.version=UNIVER;
  res=-(int)(!fwrite(&hd,sizeof(hd),1,f) || !fwrite(p,size,1,f));
  if (freeit) free(p);
  return res;
  }

int cmSaveUni(char *filename, TUNIPICTURE *pic)
  {
  FILE *f;
  int res;

  f=fopen(filename,"wb");
  if (f==NULL) return -1;
  res=SaveUnif(f,pic);
  fclose(f);  
  return res;
  }

TUNIPICTURE *cmCreateUni(int xsize, int ysize, char *format, int align, int pallen)
  {
  TUNIPICTURE uni,*pic;
  TCOLMODEL form;

  if (cmCreateColorModel(format,&form)) return NULL;
  uni.xsize=xsize;
  uni.ysize=ysize;
  if (align<1) align=1;
  uni.xlinelen=((xsize*form.srcpixelsize+(align-1))/align)*align;
  uni.dataofset=sizeof(uni)+strlen(format);
  uni.totaldata=ysize*uni.xlinelen;
  uni.palettelen=pallen;
  uni.transparent=0xffffffff;
  uni.mipmapcount=0;
  pic=malloc(cmGetSize(&uni));
  if (pic==NULL) return NULL;
  *pic=uni;
  strcpy(pic->format,format);
  return pic;
  }

int cmGetMaxMipMapCount(int xsize, int ysize)
  {
  int i=1;
  while ((xsize & ~1)==xsize && (ysize & ~1)==ysize) 
	{
	i++;
	xsize>>=1;
	ysize>>=1;
	}
  return i;
  }


TUNIPICTURE *cmCreateMipMap(int xsize, int ysize, char *format, int count, int pallen)
  {
  TUNIPICTURE uni,*pic;
  TCOLMODEL form;
  int maxcount=cmGetMaxMipMapCount(xsize,ysize);
  
  count=max(maxcount,count);
  if (cmCreateColorModel(format,&form)) return NULL;
  uni.xsize=xsize;
  uni.ysize=ysize;
  uni.xlinelen=xsize*form.srcpixelsize;
  uni.dataofset=sizeof(uni)+strlen(format);
  uni.totaldata=cmCalcMipMapLen(&uni,count);
  uni.palettelen=pallen;
  uni.transparent=0xffffffff;
  uni.mipmapcount=count;
  pic=malloc(cmGetSize(&uni));
  if (pic==NULL) return NULL;
  *pic=uni;
  strcpy(pic->format,format);
  return pic;
  }

void *cmUni2BitMap(TUNIPICTURE *pic, char checker, int xmax, int ymax)
  {
  TPICINFO info;
  TCOLMODEL conv;
  BITMAPINFOHEADER *hd;
  BITMAPINFO *bitmap;
  int redx,redy,red;
  char *tg;
  unsigned char *alpha=NULL;
  long tln;
  int size;
  int hds;
  int xsr,ysr;
    

  if (pic==NULL) return NULL;
  if (strchr(pic->format,'A')==NULL) checker=0;
  if (checker)
    {if (cmCreateConvModel(cmGetFormat(pic), "ARGB8888", cmGetPalFormat(pic), &conv)) 
	return NULL;}
  else
	{if (cmCreateConvModel(cmGetFormat(pic), "RGB888", cmGetPalFormat(pic), &conv)) 
	return NULL;}
  if (ymax>0)
	{
	redx=(pic->xsize-1)/xmax+1;
	redy=(pic->ysize-1)/ymax+1;
	red=max(redx,redy);
	}
  else
	red=xmax;
  if (checker) hds=sizeof(BITMAPINFO);else hds=sizeof(BITMAPINFOHEADER);
  xsr=(pic->xsize+red-1)/red;
  ysr=(pic->ysize+red-1)/red;
  info.xsize=pic->xsize;
  info.ysize=pic->ysize;
  info.srcpal=cmGetPalette(pic);
  info.src=cmGetData(pic);
  info.palentries=(unsigned char)(pic->palettelen/3);
  info.srclinelen=pic->xlinelen;
  info.reduce=red;  
  size=cmCalcSpace(&info,&conv,CMC_ALIGNDWORD)+hds;
  bitmap=malloc(size);
  if (bitmap==NULL) return NULL;
  tg=(char *)bitmap+hds;
  tg+=info.trglinelen*(ysr-1);
  info.trglinelen=-(tln=info.trglinelen);
  info.trg=(void *)tg;
  if (cmDoConvert(&info,&conv))
	{
	free(bitmap);
	return NULL;
	}  
  hd=&(bitmap->bmiHeader);
  memset(hd,0,sizeof(BITMAPINFOHEADER));
  hd->biSize=sizeof(BITMAPINFOHEADER);
  hd->biWidth=xsr;
  hd->biHeight=ysr;
  hd->biPlanes=1;
  hd->biBitCount=checker?32:24;
  hd->biCompression=BI_RGB;
  hd->biSizeImage=0;
  if (checker)
	{
	int size;
	int x,y;
	unsigned char a,*c,*d;
	int ln=-info.trglinelen;
	size=hd->biWidth*ln;
	c=(unsigned char *)hd+hds;
	x=0;y=0;
	for (y=0;y<hd->biHeight;y++,c+=ln)
	  for (d=c,x=0;x<hd->biWidth;x++,d+=4)
		{
		int col;
		if ((x>>3)+(y>>3)&1) col=0x80;else col=0x40;
		a=d[3];
		d[0]=((255-a)*col+a*d[0])>>8;
  		d[1]=((255-a)*col+a*d[1])>>8;
		d[2]=((255-a)*col+a*d[2])>>8;	  
		}
	}
  return bitmap;
  }

TUNIPICTURE *cmConvertUni(TUNIPICTURE *pic, const char *newformat, int reduce)
  {
  TCOLMODEL conv,pconv;
  TUNIPICTURE *nw;
  TPICINFO info;   
  char srcpalette=strchr(pic->format,'P')!=NULL;
  char trgpalette=strchr(newformat,'P')!=NULL;
  if (!srcpalette && trgpalette) return NULL;
  if (cmCreateConvModel(cmGetFormat(pic),(char *)newformat,cmGetPalFormat(pic),&conv))
	return NULL;
  if (trgpalette) 
	{
	char *c=strchr(newformat,'|');
	if (c==NULL) return NULL;
	c++;
	cmCreateConvModel(cmGetPalFormat(pic),c,NULL,&pconv);
	}
  else
	{pconv.srcpixelsize=pconv.trgpixelsize=1;}
  nw=cmCreateUni(cmGetXSize(pic)/reduce,cmGetYSize(pic)/reduce,(char *)newformat,1,pic->palettelen*pconv.trgpixelsize/pconv.srcpixelsize);
  if (nw==NULL) return NULL;
  info.xsize=cmGetXSize(pic);
  info.ysize=cmGetYSize(pic);
  info.srclinelen=cmGetXlen(pic);
  info.trglinelen=cmGetXlen(nw);
  info.reduce=reduce;
  info.srcpal=cmGetPalette(pic);
  info.src=cmGetData(pic);
  info.trg=cmGetData(nw);
  if (cmGetPalette(pic)) info.palentries=pic->palettelen/conv.palitem;
  if (cmDoConvert(&info,&conv))
	{
	free(nw);
	return NULL;
	}
  if (trgpalette)
	{
	if (cmDoConvertRaw(&pconv,cmGetPalette(pic),cmGetPalette(nw), NULL, pic->palettelen/pconv.srcpixelsize,0))
	  {
	  free(nw);
	  return NULL;
	  }
	}
  nw->transparent=pic->transparent;  
  return nw;
  }

typedef void (*LinearMipMapLine)(void *src, void *trg, long xsize, long stride, long mask);

static void LinearMipMapLine8(void *src, void *trg, long xsize, long stride, long mask)
  {
  _asm
	{
	mov	  esi,src	  ;esi src
	mov	  edi,trg	  ;edi trg
	mov	  edx,esi	  ;edx pracuje na dalsi radku
	add	  edx,stride  ;k edx je prictena sirka
	mov	  ecx,xsize	  ;k ecx je nactena velikost x
	mov	  bh,byte ptr mask	;a jeste precti masku
lp1:mov	  al,[esi]	  ;al - prvni bod (x,y)
	mov	  bl,[edx]	  ;bl - bod (x,y+1)
	mov	  ah,[esi+1]  ;ah - bod (x+1,y)
	and	  al,bh		  ;odmaskuj bity
	and	  bl,bh
	and	  ah,bh
	add	  al,ah		  ;proved (al+ah)/2
	mov	  ah,[edx+1]  ;ah - bod (x+1,y+1)
	rcr	  al,1		  
	and	  ah,bh		  ;odmaskuj bity
	add	  bl,ah		  ;proved (bl+ah)/2
	rcr	  bl,1		  
	and	  al,bh		  ;odnaskuj bity
	and	  bl,bh
	add	  al,bl		  ;proved (al+bl)/2
	rcr	  al,1	
	stosb			  ;uloz vysledek
	add	  esi,2		  ;posun se na dalsi ctverici
	add	  edx,2
	dec	  ecx
	jnz	  lp1		  ;dokud je vetsi nez 0 delej
	}
  }

static void LinearMipMapLine16(void *src, void *trg, long xsize, long stride, long mask)
  {
  _asm
	{
	push  ebp
	mov	  esi,src	  ;esi src
	mov	  edi,trg	  ;edi trg
	mov	  edx,esi	  ;edx pracuje na dalsi radku
	add	  edx,stride  ;k edx je prictena sirka
	mov	  ecx,xsize	  ;k ecx je nactena velikost x
	mov	  eax,mask
	shl	  eax,16
	mov	  ax,word ptr mask;	zopakuj masku dvakrat
	and	  eax,0FFFEFFFFh  ;odmasku pripadny druhy low bit v masce
	mov	  ebp,eax	  ;pro pripad preteceni
lp1:mov	  eax,[esi]	  ;eax - prvni dva body (x,y)
	mov	  ebx,[edx]	  ;ebx - druhe dva body (x,y+1)
	and	  eax,ebp	  ;odmaskuj bity
	and	  ebx,ebp	  ;
	add	  eax,ebx	  ;proved operaci (eax+ebx)/2
	rcr	  eax,1		
	mov	  ebx,eax	  ;uloz do ebx horni pulku eax
	shr	  ebx,16	  ;
	and	  eax,ebp
	and	  ebx,ebp
	add	  eax,ebx
	rcr	  eax,1
	stosw
	add	  esi,4		  ;posun se na dalsi ctverici
	add	  edx,4
	dec	  ecx
	jnz	  lp1		  ;dokud je vetsi nez 0 delej
	pop	  ebp
	}
  }

static void LinearMipMapLine24(void *src, void *trg, long xsize, long stride, long mask)
  {
  _asm
	{
	push  ebp
	mov	  esi,src	  ;esi src
	mov	  edi,trg	  ;edi trg
	mov	  edx,esi	  ;edx pracuje na dalsi radku
	add	  edx,stride  ;k edx je prictena sirka
	mov	  ecx,xsize	  ;k ecx je nactena velikost x
	mov	  ebp,mask
	and	  ebp,0FFFFFFh
	push  edx
lp1:mov	  edx,[esp]	  ;vyzvedni edx
	mov	  eax,[esi]	  ;eax - prvni dva body (x,y)
	mov	  ebx,[edx]	  ;ebx - druhe dva body (x,y+1)
	and	  eax,ebp	  ;odmaskuj bity
	and	  ebx,ebp	  ;
	add	  eax,ebx	  ;proved operaci (eax+ebx)/2
	mov	  ebx,[esi+3]
	rcr	  eax,1		
	mov	  edx,[edx+3]
	and	  ebx,ebp	  ;odmaskuj bity
	and	  edx,ebp	  ;
	add	  edx,ebx	  ;proved operaci (eax+ebx)/2
	and	  eax,ebp
	rcr	  edx,1		
	and	  edx,ebp
	add	  eax,edx
	add	  esi,6		  ;posun se na dalsi ctverici
	rcr	  eax,1
	add	  dword ptr [esp],6
	stosw
	shr	  eax,16
	stosb
	dec	  ecx
	jnz	  lp1		  ;dokud je vetsi nez 0 delej
	pop	  edx
	pop	  ebp
	}
  }

static void LinearMipMapLine32(void *src, void *trg, long xsize, long stride, long mask)
  {
  _asm
	{
	push  ebp
	mov	  esi,src	  ;esi src
	mov	  edi,trg	  ;edi trg
	mov	  edx,esi	  ;edx pracuje na dalsi radku
	add	  edx,stride  ;k edx je prictena sirka
	mov	  ecx,xsize	  ;k ecx je nactena velikost x
	mov	  ebp,mask
	push  edx
lp1:mov	  edx,[esp]	  ;vyzvedni edx
	mov	  eax,[esi]	  ;eax - prvni dva body (x,y)
	mov	  ebx,[edx]	  ;ebx - druhe dva body (x,y+1)
	and	  eax,ebp	  ;odmaskuj bity
	and	  ebx,ebp	  ;
	add	  eax,ebx	  ;proved operaci (eax+ebx)/2
	mov	  ebx,[esi+4]
	rcr	  eax,1		
	mov	  edx,[edx+4]
	and	  ebx,ebp	  ;odmaskuj bity
	and	  edx,ebp	  ;
	add	  edx,ebx	  ;proved operaci (eax+ebx)/2
	rcr	  edx,1		
	and	  eax,ebp
	add	  esi,8		  ;posun se na dalsi ctverici
	and	  edx,ebp
	add	  eax,edx
	rcr	  eax,1
	add	  dword ptr[esp],8
	stosd
	dec	  ecx
	jnz	  lp1		  ;dokud je vetsi nez 0 delej
	pop	  edx
	pop	  ebp
	}
  }

static void NearestMipMapLine8(void *src, void *trg, long xsize, long stride, long mask)
  {
  while (xsize--)
	{
	*(char *)trg=*(char *)src;
	trg=(void *)((char *)trg+1);
	src=(void *)((char *)src+2);
	}
  }

static void NearestMipMapLine16(void *src, void *trg, long xsize, long stride, long mask)
  {
  while (xsize--)
	{
	*(short *)trg=*(short *)src;
	trg=(void *)((char *)trg+2);
	src=(void *)((char *)src+4);
	}
  }

static void NearestMipMapLine24(void *src, void *trg, long xsize, long stride, long mask)
  {
  while (xsize--)
	{
	*(short *)trg=*(short *)src;
	trg=(void *)((char *)trg+2);
	src=(void *)((char *)src+2);
	*(char *)trg=*(char *)src;
	trg=(void *)((char *)trg+1);
	src=(void *)((char *)src+4);
	}
  }

static void NearestMipMapLine32(void *src, void *trg, long xsize, long stride, long mask)
  {
  while (xsize--)
	{
	*(long *)trg=*(long *)src;
	trg=(void *)((char *)trg+4);
	src=(void *)((char *)src+8);
	}
  }


char cmMipMapGen(TMIPMAPGENSTRUCT *mmgs)
  {
  LinearMipMapLine lmml;
  int i;
  int xs;
  void *src,*trg;
  if (mmgs->flags & CMMG_LINEAR)
	switch (mmgs->bits)
	  {
	  case 1: lmml=LinearMipMapLine8;break;
	  case 2: lmml=LinearMipMapLine16;break;
	  case 3: lmml=LinearMipMapLine24;break;
	  case 4: lmml=LinearMipMapLine32;break;
	  default: return -1;
	  }
  else
	switch (mmgs->bits)
	  {
	  case 1: lmml=NearestMipMapLine8;break;
	  case 2: lmml=NearestMipMapLine16;break;
	  case 3: lmml=NearestMipMapLine24;break;
	  case 4: lmml=NearestMipMapLine32;break;
	  default: return -1;
	  }
  src=mmgs->source;
  trg=mmgs->target;
  xs=mmgs->xsize>>1;
  for (i=0;i<mmgs->ysize;i+=2)
	{
	lmml(src,trg,xs,mmgs->sourcestride,mmgs->bitmask);
	src=(void *)((char *)src+mmgs->sourcestride*2);
	trg=(void *)((char *)trg+mmgs->targetstride);
	}
  return 0;
  }

char cmGetMipMapMask(TCOLMODEL *mdl, TMIPMAPGENSTRUCT *mmgs)
  {
  COLULONG mask;
  COLULONG over=0;
  int i;
  mmgs->bits=mdl->srcpixelsize;
  if (mdl->palflags) 
	{
	mmgs->flags=CMMG_NEAREST;
	mask=mdl->index.srcmask;
	}
  else
	mask=0;
  for (i=0;i<mdl->colcount;i++) if ((mdl->palflags & (1<<i))==0)
	{
	COLULONG overflow=mdl->part[i].srcmask+mdl->part[i].srcmask;
	over|=overflow & ~mdl->part[i].srcmask;
	mask|=mdl->part[i].srcmask;
	}
  mmgs->bitmask=mask & ~over; 
  return 0;
  }



/*
void main()
  {
  TCOLMODEL *list=NULL;
  TCOLMODEL mdl;
  TUNIPICTURE *pct;
  int i;
  char *formats[]=
	{
	"RGB332","RGB565","BGR888","ARGB4444","P8"
	};
  for (i=0;i<sizeof(formats)/sizeof(char *);i++)
	list=cmAddFormatToList(list,formats[i]);
  cmCreateBestConvModel(list,"I4",NULL,&mdl);
  pct=cmLoadTGA("copy.tga");
  }




/*void main()
  {
  TCOLMODEL mdl;
  unsigned int32 picture[128][128];
  unsigned long needspace;
  TPICINFO info;
  int res;

  memset(&info,0,sizeof(info));
  info.xsize=128;
  info.ysize=128;
  info.srclinelen=128*4;
  info.reduce=1;
  info.src=picture;
  res=cmCreateConvModel("?RGB8888","RGB565",NULL,&mdl);
  if (res) {printf("CreateConvModelError %04X\n",res);return;}
  needspace=cmCalcSpace(&info,&mdl,CMC_ALIGNDWORD);
  info.trg=malloc(needspace);
  res=cmDoConvert(&info,&mdl);
  if (res) {printf("DoConvertError %04X\n",res);return;}
  
	}*/