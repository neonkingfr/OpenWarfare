// GBase.h: interface for the CGLight class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GBASE_H__097E6B88_FCB0_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_GBASE_H__097E6B88_FCB0_11D3_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "G3DObject.h"
#include <string.h>
#include <stdlib.h>

class CGLight : public CG3DObject  
  {
	TLIGHTDEF light;
  public:
	void SetPosition(HVECTOR v) {CopyVektor(light.position,v);}
	void SetDirection(HVECTOR v) {CopyVektor(light.direction,v);}
	void SetColor(float r, float g, float b)
	  {light.col_r=r;light.col_g=g;light.col_b=b;}
	void SetAmplitude(float r, float g, float b)
	  {light.ampl_r=r;light.ampl_g=g;light.ampl_b=b;}
	void SetOffset(float r, float g, float b)
	  {light.offs_r=r;light.offs_g=g;light.offs_b=b;}
	void SetFrequence(float f)
	  {light.freq=f;}
	void SetRange(float range)
	  {light.range=range;}
	void SetCosRadius(float min, float max)
	  {light.minradius=min;light.maxradius=max;}
	void SetRadius(float min, float max)
	  {light.maxradius=(float)cos(min);light.minradius=(float)cos(max);}
	void TurnOn() {light.light_flags|=LGH_ACTIVE;}
	void TurnOff() {light.light_flags&=~LGH_ACTIVE;}
	void LightType(int type) {light.light_flags=(light.light_flags & LGH_ACTIVE) | (type);}
	operator HLIGHTDEF() {return &light;}
	void SetLight(int pos) {g3dSetLight(pos,&light);}
	void GetLight(int pos) {g3dGetLight(pos,&light);}
	CGLight(TLIGHTDEF& l) {light=l;}
	CGLight(bool init = true) 
	  {if (init) memset(&light,0,sizeof(light));}	
  };


class CGVertexCol : public CG3DObject  
	{ 
	TVERTEXCOLOR col;
public:
	void SetColor(TLIGHTDEF& l);
	CGVertexCol(CGLight& l);
	CGVertexCol(TLIGHTDEF& l);
	CGVertexCol(bool init=true)
	  {if (init) memset(&col,0,sizeof(init));}
	void SetDiffuse(float r,float g, float b)
	  {col.pohlc_r=r;col.pohlc_g=g;col.pohlc_b=b;}
	void SetEmit(float r,float g, float b)
	  {col.emit_r=r;col.emit_g=g;col.emit_b=b;}
	void SetAmplitude(float r, float g, float b)
	  {col.ampl_r=r;col.ampl_g=g;col.ampl_b=b;}
	void SetOffset(float r, float g, float b)
	  {col.offs_r=r;col.offs_g=g;col.offs_b=b;}
	void SetFrequence(float f)
	  {col.freq=f;}
	void SetAlpha(float alpha)
	  {col.alpha=alpha;col.flags|=TVCOL_ALPHA;}
	void UnsetAlpha()
	  {col.flags &= ~TVCOL_ALPHA;}
	void SetFlags(long flags)
	  {col.flags=flags;}
	long GetFlags() {return col.flags;}
	};


class CGMatrixStack : public CG3DObject  
	{
	TMATRIXSTACK mxs;
public:
	void Flush()
	  {mxsFreeStack(&mxs);mxsInitStack(&mxs);}
	LPHMATRIX PushL()
	  {return mxsPushL(&mxs);}
	LPHMATRIX PushR()
	  {return mxsPushR(&mxs);}
	void PushL(HMATRIX mm)
	  {CopyMatice(mxsPushL(&mxs),mm);}
	void PushR(HMATRIX mm)
	  {CopyMatice(mxsPushR(&mxs),mm);}
	LPHMATRIX Top()
	  {return mxsTop(&mxs);}
	LPHMATRIX Pop(HMATRIX mm)
	  {return mxsPop(&mxs,mm);}
	CGMatrixStack()
	  {mxsInitStack(&mxs);}
	virtual ~CGMatrixStack()
	  {mxsFreeStack(&mxs);}
	operator LPHMATRIX() 
	  {return mxsTop(&mxs);}
	};


class CGScene : public CG3DDispObject  
  {
  TSCENEDEF def;	
  public:
	CGScene() {g3dCreateScene(&def);}
	CGScene(TSCENEDEF& dd) {memcpy(&def,&dd,sizeof(TSCENEDEF));memset(&dd,0,sizeof(TSCENEDEF));}	
	CGScene(CGScene& other) 
	  {CG3DObject::CG3DObject(other);g3dCreateScene(&def);g3dDuplicate(&def,&(other.def));}
	virtual ~CGScene() {g3dDestroy(&def);}
	static void BeginScene() {g3dBeginScene(NULL);}
	static void BeginObject(HMATRIX mm) {g3dBeginObject(mm);}
	static int AddPosition(HVECTOR v) {return g3dAddPosition(v);}
	static int AddPosition(float x, float y, float z) {return g3dAddPosition(Vector3(x,y,z));}
	static int AddTPosition(HVECTOR v,HVECTOR t) {return g3dAddTPosition(v,t);}
	static int AddNormal(HVECTOR v) {return g3dAddNormal(v);}
	static int AddNormal(float x, float y, float z) {return g3dAddNormal(Vector3(x,y,z));}
	static int AddVertex(int vi,int ni) {return g3dAddVertex(vi,ni);}
	static int AddFace(int vertices,int flags,long handle)
	  {return g3dAddFace(vertices,flags,handle);}
	static int AddFaceVertex(int vertex,int color,float tu, float tv)
	  {return g3dAddFaceVertex(vertex,color,tu,tv);}
	static void EndScene(CGScene & scene) 
	  {g3dEndScene(&(scene.def));}
	static void EndScene() 
	  {g3dEndScene(NULL);}
	virtual bool Load(FILE *f)
	  {return g3dioLoad(&def,f)==G3D_OK;}
	virtual bool Save(FILE *f)
	  {return g3dioSave(&def,f)==G3D_OK;}	
	virtual int LoadToScene(HMATRIX mm,HTXITABLE txi=NULL) {return g3dLoadEx(&def,mm,txi);}
  };



class CGCamera : public CG3DObject  
  {
  protected:
	HMATRIX curview;
	HMATRIX curproj;
  public:
	void ObjectMatrix(HMATRIX matr);
	CGCamera()
	  {
	  JednotkovaMatice(curproj);
	  JednotkovaMatice(curview);
	  }
	void SetCamera(float front, float back, float fow)
	  {
	  CreateProjection2(curproj,front,back,fow);
	  }
	void SetCamera(float scale)
	  {
	  Zoom(curproj,scale,scale,scale);
//	  curproj[3][2]=0.5f;
	  }

	void LookAt(HVECTOR from, HVECTOR to, HVECTOR top)
	  {
	  PohledovaMatice(from,to,top,curview);
	  }
	void LookAt(HVECTOR pos, float rotz, float rotx, float distance);
	void DrawScene(int flags)
	  {
	  g3dDrawScene(curview, curproj,flags);
	  }
	void CalcFinalMatrix()
	  {
	  g3dCalcFinalMatrix(curview,curproj);
	  }
	void SetViewMatrix(HMATRIX mm)
	  {
	  CopyMatice(curview,mm);
	  }
	void SetProjMatrix(HMATRIX mm)
	  {
	  CopyMatice(curproj,mm);
	  }
	LPHMATRIX GetView() {return curview;}
	LPHMATRIX GetProjection() {return curproj;}
  };




class CGMatrix : public CG3DObject  
{
	HMATRIX mm;
public:
	operator LPHMATRIX() {return mm;}
	LPHMATRIX operator= (float z) {Zero();mm[0][0]=mm[1][1]=mm[2][2]=mm[3][3]=z;return mm;}
	LPHMATRIX operator*=(HMATRIX m1) {SoucinMatic(mm,m1,mm);return mm;}
	LPHMATRIX operator=(HMATRIX m1) {CopyMatice(mm,m1);return m1;}
	void Identity() {JednotkovaMatice(mm);}
	void Zero() {NulovaMatice(mm);}
	void Translate(float x,float y, float z) {Translace(mm,x,y,z);}
	void RotateX(float r) {RotaceX(mm,r);}
	void RotateY(float r) {RotaceY(mm,r);}
	void RotateZ(float r) {RotaceZ(mm,r);}
	void Zoom(float xs, float ys, float zs) {::Zoom(mm,xs,ys,zs);}
	void CreateProjection(float front, float back, float fow) {CreateProjection2(mm,front,back,fow);}
	float *operator[](int line) {return mm[line];}
	LPHMATRIX Mul(HMATRIX m1, HMATRIX m2) {SoucinMatic(m1,m2,mm);return mm;}
	CGMatrix(bool init=true) {if (init) Identity();}
	CGMatrix(HMATRIX m1) {CopyMatice(mm,m1);}	
	CGMatrix(HMATRIX m1, HMATRIX m2) {Mul(m1,m2);}
};

class CGCamera2: public CGCamera
  {
  protected:
	HMATRIX canvas;
	CGMatrixStack all;
	CGMatrixStack light_inv; //opacna pro svetla
	void DrawScene(int flags) {}
	void CalcFinalMatrix() {}	
  public:
	void FlushStack();
    void SetCanvas(float xmid, float ymid, float scalex, float scaley);
	LPHMATRIX GetTransform() {return all.Top();}
	LPHMATRIX GetLightTransform() {return light_inv;}
	LPHMATRIX GetCanvas() {return canvas;}
	char PushMatrix(LPHMATRIX mm, bool pushright=false)	;
	void PopMatrix() {all.Pop(NULL);light_inv.Pop(NULL);}
  };


#endif // !defined(AFX_GBASE_H__097E6B88_FCB0_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
