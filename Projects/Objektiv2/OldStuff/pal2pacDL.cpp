#include <windows.h>
#include <malloc.h>
#include "pal2pac.hpp"
#include "../pacload/pachead.h"

// define all veriables required to dynamic linking of pal2pac

UpdateTextureT *UpdateTextureF=NULL;
CreateTextureDataT *CreateTextureDataF=NULL;
DestroyTextureDataT *DestroyTextureDataF=NULL;



bool InitPal2Pac(const char *dllpath)
  {
  char *c=(char *)alloca(strlen(dllpath)+20);
  strcpy(c,dllpath);
  if (c[0] && c[strlen(c)-1]!='\\') strcat(c,"\\");
  strcat(c,"pal2pac.dll");
  if (UpdateTextureF) return false;
  dbgprintf("LoadLib %s",c);
  HINSTANCE lib = ::LoadLibrary(c);
  if (!lib)
  {
    dbgprintf("{error} Library loading failed");
    return false;
  }
  dbgprintf("LoadLib End");
  UpdateTextureF = (UpdateTextureT *)::GetProcAddress(lib,"_UpdateTexture@12");
  CreateTextureDataF = (CreateTextureDataT *)::GetProcAddress(lib,"_CreateTextureData@8");
  DestroyTextureDataF = (DestroyTextureDataT *)::GetProcAddress(lib,"_DestroyTextureData@4");
  return true;
  }

//--------------------------------------------------

