#include "HASH.H"
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <conio.h>


#define ZeroAlloc(b) memset(malloc(b),0,b)

HHASHITEM find_uzel(THASHTABLE *table,void *lastfind);

static unsigned long ModMulti(unsigned char *number, unsigned long size,unsigned long div)
  {
  unsigned long res=0;
  while (size--)
	{
	res<<=8;
	res+=*number++;
	if (res>div) res=res%div;
	else if (res==div) res=0;	
	}
  return res;
  }

static unsigned long MapKey(THASHTABLE *table,void *key)
  {
  unsigned long l=0;int size=table->keylen;

  l=ModMulti((unsigned char *)key,size,table->entries);
  return l;
  }

static unsigned long MapKeyByData(THASHTABLE *table,void *data)
  {
  unsigned long l=0;int size=table->keylen;
  void *key=(void *)((char *)data+table->keyoffset);

  l=ModMulti((unsigned char *)key,size,table->entries);
  return l;
  }

char Hash_Init(THASHTABLE *table)
  {
  table->table=(HHASHITEM *)malloc(sizeof(void **)*table->entries);
  if (table->table!=NULL)
    {
    memset(table->table,0,sizeof(void **)*table->entries);
    table->destructor=NULL;
    table->freelist=NULL;
    table->dataoffs=0;
    }
  return table->table==NULL?-1:0;
  }

static THASHITEM *AllocItem(THASHTABLE *table)
  {
  if (table->freelist!=NULL)
    {
    THASHITEM *it;

    it=table->freelist;table->freelist=it->next;
    return it;
    }
  else
    if (table->flags & HASHF_POINTERS)
      return (THASHITEM *)malloc(sizeof(THASHITEM));
    else
      return (THASHITEM *)malloc(sizeof(THASHITEM)+table->datasize);
  }

static void FreeItem(THASHTABLE *table, THASHITEM *item)
  {
  if (table->flags & HASHF_SPEEDADD)
    {
    item->next=table->freelist;
    table->freelist=item;
    }
  else
    free(item);
  }

static void *Hash_AddShared(THASHTABLE *table, HHASHITEM hi)
  {
  unsigned long l;
  HHASHITEM  *modf;

  l=MapKeyByData(table,hi->data);
  modf=&table->table[l];
  if (table->flags & HASHF_ADDEND)
    while (*modf!=NULL) modf=&(*modf)->next;
  hi->next=*modf;
  *modf=hi;
  return hi->data;
  }

void *Hash_Add(THASHTABLE *table,void *data) //pridava do tabulky
  {
  HHASHITEM hi;

  if (table->flags & HASHF_EXISTERROR)   //Exist Error
    {
    void *p;
    p=Hash_Find(table,data);
    if (p!=NULL) return NULL;
    }
  else if (table->flags & HASHF_UPDATE)        //Update mode?
    {
    void *p;
    p=Hash_Find(table,data);
    if (p!=NULL)
      {
      if (~table->flags & HASHF_POINTERS)
        memcpy(p,data,table->datasize);    //pokud polozka existuje, pouze
      else
        {
        HHASHITEM x;
        x=find_uzel(table,p);
        x->data=data;
        p=data;
        }
      return p;                         //zmen jeji obsah
      }
    }
  hi=AllocItem(table);
  if (hi==NULL) return NULL;
  if (table->flags & HASHF_POINTERS)
    hi->data=data;
  else
    {
    hi->data=(void *)((char *)hi+sizeof(THASHITEM));
    memcpy(hi->data,data,table->datasize);
    }
  return Hash_AddShared(table,hi);
  }

void *Hash_AddEx(THASHTABLE *table,HHASHITEM hi) //pridava do tabulky
  {
  if (table->flags & HASHF_POINTERS) return NULL;
  hi->data=(void *)(hi+1);
  if (table->flags & HASHF_EXISTERROR)   //Exist Error
    {
    void *p;
    p=Hash_Find(table,hi->data);
    if (p!=NULL) return NULL;
    }
  else if (table->flags & HASHF_UPDATE)        //Update mode?
    {
    void *p;
    p=Hash_Find(table,hi->data);
    if (p!=NULL)
        memcpy(p,hi->data,table->datasize);    //pokud polozka existuje, pouze
    }
  return Hash_AddShared(table,hi);
  }


static char CompareKeys(THASHTABLE *table,void *data,void *key)
  {
  data=(void *)((char *)data+table->keyoffset);
  return !memcmp(data,key,table->keylen);
  }

static char CompareDataKeys(THASHTABLE *table,void *data1,void *data2)
  {
  data1=(void *)((char *)data1+table->keyoffset);
  data2=(void *)((char *)data2+table->keyoffset);
  return !memcmp(data1,data2,table->keylen);
  }

void *Hash_Find(THASHTABLE *table,void *data) //hleda duplicitni data
  {
  return Hash_FindKey(table,(void *)((char *)data+table->keyoffset));
  }

void *Hash_FindKey(THASHTABLE *table,void *key)
  {
  unsigned long l;
  HHASHITEM hi;

  l=MapKey(table,key);
  hi=table->table[l];
  while (hi!=NULL)
    {
    if (CompareKeys(table,hi->data,key)) return hi->data;
    hi=hi->next;
    }
  return NULL;
  }

static HHASHITEM find_uzel(THASHTABLE *table,void *lastfind)
  {
  HHASHITEM hi;
  if (table->flags & HASHF_POINTERS)
    {
    unsigned long l;
    l=MapKeyByData(table,lastfind);
    hi=table->table[l];
    while (hi!=NULL && hi->data!=lastfind) hi=hi->next;
    }
  else
    {
    hi=(HHASHITEM)((char *)lastfind-sizeof(THASHITEM));
    if (hi->data!=lastfind) return NULL;
    }
  return hi;
  }

void *Hash_FindNext(THASHTABLE *table, void *lastfind)
  {
  HHASHITEM hi;
  void *key=Hash_GetKey(table,lastfind);

  hi=find_uzel(table,lastfind);
  if (hi==NULL) return NULL;
  hi=hi->next;
  while (hi!=NULL)
    {
    if (CompareKeys(table,hi->data,key)) return hi->data;
    hi=hi->next;
    }
  return NULL;
  }

void *Hash_EnumData(THASHTABLE *table, void *lastfind)
  {
  long l;
  HHASHITEM hi;

  if (lastfind!=NULL)
    {
    hi=find_uzel(table,lastfind);
    hi=hi->next;
    if (hi!=NULL) return hi->data;
    l=MapKeyByData(table,lastfind);
    }
  else l=-1;
  do
    {
    l++;
    if (l>=table->entries) return NULL;
    hi=table->table[l];
    }
  while (hi==NULL) ;
  return hi->data;
  }

#define Destroy(table,hi) ((table)->destructor((void *)((char *)hi->data+(table)->dataoffs)))

char Hash_Delete(THASHTABLE *table,void *key)
  {
  unsigned long l;
  char res=-1;
  HHASHITEM hi,*modf,*kill;


  l=MapKey(table,key);
  modf=&table->table[l];
  hi=*modf;kill=NULL;
  while (hi!=NULL)
    {
    if (CompareKeys(table,hi->data,key))
      {
      if (table->flags & HASHF_DELLAST)
        {
        kill=modf;
        res=0;
        }
      else
        {
        if (table->destructor!=NULL)
          if (Destroy(table,hi)) return -1;
        *modf=hi->next;
        FreeItem(table,hi);
        res=0;
        }
      if (!(table->flags & HASHF_DELALL)) return 0;
      }
    modf=&hi->next;
    hi=*modf;
    }
  if (kill!=NULL)
    {
    hi=*kill;
    if (table->destructor!=NULL)
      if (Destroy(table,hi)) return -1;
    *kill=hi->next;
    FreeItem(table,hi);
    res=0;
    }
  return res;
  }

char Hash_DeleteExact(THASHTABLE *table, void *found, char flags)
  {
  HHASHITEM it,*modf;
  unsigned long l;

  if (found==NULL) return -1;
  l=MapKeyByData(table,found);
  modf=table->table+l;
  it=*modf;
  while (it!=NULL && it->data!=found)
    {
    modf=&it->next;it=*modf;
    }
  if (it==NULL) return -1;
  if (!(flags & HASHD_DESTROYED) && table->destructor!=NULL)
      if (Destroy(table,it)) return -1;
  *modf=it->next;
  FreeItem(table,it);
  return 0;
  }

void *Hash_Fill(THASHTABLE *table,void *data) //vyplni data hodnotou z tabulky
  {
  void *fnd;

  fnd=Hash_Find(table,data);
  if (fnd!=NULL)
    memcpy(data,fnd,table->datasize);
  return fnd;
  }


void Hash_Flush(THASHTABLE *table) //likviduje tabulku
  {
  long l;

  for (l=0;l<table->entries;l++)
    while (table->table[l]!=NULL)
      {
      HHASHITEM hi;

      hi=table->table[l];
      table->table[l]=hi->next;
      if (table->destructor!=NULL) Destroy(table,hi);
      FreeItem(table,hi);
      }
  }

void Hash_Done(THASHTABLE *table) //likviduje tabulku
  {
  Hash_Flush(table);
  Hash_Clean(table);
  free(table->table);
  table->table=NULL;
  }

char Hash_Init2(THASHTABLE *table, int entries, int datasize, int keyoffset, int keysize, int flags)
  {
  table->entries=entries;
  table->datasize=datasize;
  table->keyoffset=keyoffset;
  table->keylen=keysize;
  table->flags=flags;
  return Hash_Init(table);
  }

void Hash_GetState(THASHTABLE *table,THASHSTATE *state)//zjistuje ruzne informace
  {
  void *find=NULL;
  unsigned long l=0,lp;
  int total=0,curl=0,maxl=0,used=0,minl=-1;

  while ((find=Hash_EnumData(table,find))!=NULL)
    {
    lp=MapKeyByData(table,find);
    if (lp!=l)
      {
      l=lp;
      if (minl==-1) minl=curl;else minl=min(minl,curl);
      maxl=max(maxl,curl);
      curl=0;
      }
    if (curl==0) used++;
    total++;curl++;
    }
  if (minl==-1) minl=curl;else minl=min(minl,curl);
  maxl=max(maxl,curl);
  state->total_data=total;
  state->largest_string=maxl;
  state->shortest_string=minl;
  state->unused_entries=table->entries-used;
  state->avg_entry=total/table->entries;
  state->avg_entry2=total/used;
  state->percent=used*100/table->entries;
  }

void Hash_Clean(THASHTABLE *table)
  {
  THASHITEM *it,*fr;

  it=table->freelist;
  table->freelist=NULL;
  while (it!=NULL)
    {
    fr=it;it=it->next;
    free(fr);
    }
  }



char Cache_Init(THASHCACHE *cache,int entries,int cachelen,int datasize, int keyoffs,int keylen)
  {
  int offset=THCHESIZE();
  int res;

  if (!cachelen) return -1;
  keyoffs+=offset;
  datasize+=offset;
  res=Hash_Init2(&cache->hash,entries,datasize,keyoffs,keylen,0);
  if (res) return res;
  cache->cachemax=cachelen;
  cache->cachelen=0;
  cache->begin=cache->end=NULL;
  cache->hash.dataoffs=THCHESIZE();
  return 0;
  }


char Cache_Done(THASHCACHE *cache)
  {
  Hash_Done(&cache->hash);
  return 0;
  }

char Cache_Flush(THASHCACHE *cache)
  {
  Hash_Flush(&cache->hash);
  cache->cachelen=0;
  return 0;
  }

char Cache_Invalidate(THASHCACHE *cache,void *key)
  {
  THCHEITEM *it,*up,*down;
  char res;

  it=Hash_FindKey(&cache->hash,key);
  if (it==NULL) return -1;
  up=it->up;
  down=it->down;
  if (up!=NULL) up->down=down;else cache->begin=down;
  if (down!=NULL) down->up=up;else cache->end=up;
  res=Hash_Delete(&cache->hash,key);
  if (!res) cache->cachelen--;
  return res;
  }

void *Cache_Find(THASHCACHE *cache,void *key)
  {
  THCHEITEM *it,*up,*down;

  it=Hash_FindKey(&cache->hash,key);
  if (it==NULL) return NULL;
  up=it->up;
  down=it->down;
  if (down!=NULL)
    {
    if (up!=NULL) up->down=down;else cache->begin=down;
    down->up=up;
    up=cache->end;
    up->down=it;
    it->down=NULL;
    it->up=up;
    cache->end=it;
    //if (cache->begin==NULL) cache->begin=it;
    }
  return (void *)(it->item);
  }

void *Cache_Add(THASHCACHE *cache,void *data)
  {
  THCHEITEM *it,*up,*it2;
  char freeit=0;
  HHASHITEM hit;

  it=(THCHEITEM *)((char *)data-sizeof(THCHEITEM)+1);
  it=Cache_Find(cache,Hash_GetKey(&cache->hash,it));
  if (it!=NULL) return it;
  hit=AllocItem(&cache->hash);
  if (hit==NULL) return NULL;
  it=(THCHEITEM *)(hit+1);
  memcpy(it->item,data,cache->hash.datasize-THCHESIZE());
  it2=Hash_AddEx(&cache->hash,hit);
  if (it2==NULL) {FreeItem(&cache->hash,hit);return NULL;}
  up=cache->end;
  it2->up=up;
  it2->down=NULL;
  if (up==NULL) cache->begin=it2; else up->down=it2;
  cache->end=it2;
  cache->cachelen++;
  if (cache->cachelen>=cache->cachemax)
    {
    Cache_Invalidate(cache,Hash_GetKey(&cache->hash,cache->begin));
    cache->cachelen--;
    }
  return it2->item;
  }


char Timer_Init(THASHTIMER *timer, int entries, int datasize, int flags)
  {
  char res;

  flags&=HASHF_SPEEDADD;
  res=Hash_Init2(&timer->hash,entries,sizeof(TTIMERITEM)+datasize,
    GetOffset(TTIMERITEM,time),GetKeySize(TTIMERITEM,time),flags);
  if (res) return res;
  timer->lastcheck=0;
  timer->hash.dataoffs=sizeof(TTIMERITEM);
  return 0;
  }


HTIMER Timer_Add(THASHTIMER *timer, unsigned long time, void *data)
  {
  TTIMERITEM it;

  it.time=time;
  it.nextcall=0;
  it.counter=1;
  it.data=data;
  return Timer_AddEx(timer,&it);
  }

HTIMER Timer_AddEx(THASHTIMER *timer, TTIMERITEM *it)
  {
  TTIMERITEM *it2,*it3;
  HTIMER timerid;
  HHASHITEM hit;
  unsigned long l;

  if (!it->counter) return 0;
  l=MapKey(&timer->hash,&it->time);
  hit=timer->hash.table[l];
  if (hit!=NULL)
    {
    it3=hit->data;
    timerid=it3->timerid+timer->hash.entries;
    }
  else
    timerid=l+1;
  hit=AllocItem(&timer->hash);
  if (hit==NULL) return 0;
  memcpy(hit+1,it,sizeof(TTIMERITEM));
  it2=Hash_AddEx(&timer->hash,hit);
  if (it2==NULL) {FreeItem(&timer->hash,hit);return 0;}
  it2->timerid=timerid;
  it2->data=it2+1;
  memcpy(it2->data,it->data,timer->hash.datasize-sizeof(TTIMERITEM));
  return timerid;
  }

char Timer_Kill(THASHTIMER *timer, HTIMER timerid)
  {
  unsigned long l;
  TTIMERITEM *it2;
  HHASHITEM hit,*modf;

  l=(timerid-1)%timer->hash.entries;
  modf=timer->hash.table+l;
  hit=*modf;
  if (hit==NULL) return -1;
  while (hit!=NULL)
    {
    it2=hit->data;
    if (it2->timerid==timerid) break;
    modf=&hit->next;
    hit=*modf;
    }
  if (hit==NULL) return -1;
  if (timer->hash.destructor!=NULL)
    if (Destroy(&timer->hash,hit)) return -1;
  *modf=hit->next;
  FreeItem(&timer->hash,hit);
  return 0;
  }

char Timer_KillAll(THASHTIMER *timer)
  {
  Hash_Flush(&timer->hash);
  return 0;
  }

void Timer_Done(THASHTIMER *timer)
  {
  Hash_Done(&timer->hash);
  }

void Timer_SetTime(THASHTIMER *timer, unsigned long time)
  {
  timer->lastcheck=time;
  }

TTIMERITEM *Timer_Run(THASHTIMER *timer, unsigned long curtime, TTIMERITEM *last)
  {
  long timedif,td;

  timedif=curtime-timer->lastcheck;
  if (timedif>=0)
    {
    if (timedif>=timer->hash.entries)
      {
      TTIMERITEM *it=last;

      it=Hash_EnumData(&timer->hash,it);
      while (it!=NULL)
        {
        td=curtime-it->time;
        if (td>0) break;
        it=Hash_EnumData(&timer->hash,it);
        }
      return it;
      }
    else
      {
      TTIMERITEM *it=last;
      unsigned long tim=timer->lastcheck;

      while (tim<=curtime)
        {
        if (it==NULL) it=Hash_FindKey(&timer->hash,&tim);
        else it=Hash_FindNext(&timer->hash,it);
        if (it!=NULL) break;
        tim++;
        }
      timer->lastcheck=tim;
      return it;
      }
    }
  if (last!=NULL)
    {
    TTIMERITEM it2;

    memcpy(&it2,last,sizeof(TTIMERITEM));
    it2.counter--;
    if (it2.counter>0)
      {
      it2.time+=it2.nextcall;
      Timer_AddEx(timer,&it2);
      Hash_DeleteExact(&timer->hash,last,HASHD_DESTROYED);
      }
    else
      Hash_DeleteExact(&timer->hash,last,0);
    }
  return 0;
  }
//---------- Test --------------------
/*
typedef struct _item
  {
  char key[10];
  }TITEM;

void main()
  {
  THASHTABLE tt;
  THASHSTATE st;
  TITEM item, *it;
  int i,k;

  Hash_InitEx(&tt,51,TITEM,key,0);
  for (k=0;k<500;k++)
    {
    for (i=0;i<9;i++) item.key[i]=(rand()*26)/(RAND_MAX+1)+65;
    Hash_Add(&tt,&item);
    }
  it=Hash_FindKey(&tt,&item);
  while (it!=NULL)
    {
    cprintf("%s\r\n",it->key);
    it=Hash_FindNext(&tt,it);
    }
  cputs("\n\r");
  //Hash_DefragData(&tt);
  Hash_GetState(&tt,&st);
  cprintf("Total data:     %8d \n\r"
          "Largest string: %8d \n\r"
          "Shortest string:%8d \n\r"
          "Unused entries: %8d/%d \n\r"
          "Avrg entry:     %8d/%d \n\r"
          "Efficiency:     %8d%%\n\r",
          st.total_data,
          st.largest_string,
          st.shortest_string,
          st.unused_entries,tt.entries,
          st.avg_entry2,st.avg_entry,
          st.percent);
  Hash_Done(&tt);
  printf("TimerTest\n");
    {
    THASHTIMER tt;
    TTIMERITEM *it;
    HTIMER endid;
    time_t time;

    Timer_Init(&tt,101,0,HASHF_SPEEDADD);
    printf("Timer 1: %d\n",Timer_Add(&tt,CLOCKS_PER_SEC,NULL));
    printf("Timer 2: %d\n",Timer_Add(&tt,CLOCKS_PER_SEC*3,NULL));
    printf("Timer 3: %d\n",Timer_Add(&tt,CLOCKS_PER_SEC*2,NULL));
    printf("Timer 4: %d\n",Timer_Add(&tt,10,NULL));
    printf("Timer 5: %d\n",Timer_Add(&tt,100,NULL));
    printf("Timer 6: %d\n",endid=Timer_Add(&tt,CLOCKS_PER_SEC*10,NULL));
    for (;;)
      {
      time=clock();
      it=Timer_Run(&tt,time,NULL);
      while (it!=NULL)
        {
        printf("Timer %d\n",it->timerid);
        if (it->timerid==endid) goto exit;
        it=Timer_Run(&tt,time,it);
        }
      }
   exit:;
    }
  }


*/