#include "../stdafx.h"
#include <g3dlib.h>
#include "../resource.h"
#include "../config.h"
extern CConfig *config;

void g3dInitPlugins()
{
  {
    CString s;
    s=config->GetString(IDS_CFGOBJEKTIVDLLS);
    if (s.GetLength()!=0) 
      if (s[s.GetLength()-1]!='\\') s+='\\';
    //Register different format loaders
    g3dRegisterModule(s+"jpgload.dll");
    g3dRegisterModule(s+"gifload.dll");
    g3dRegisterModule(s+"pacload.dll");
    //Init G3D modules;
    g3dInitModules();    
  }
}
