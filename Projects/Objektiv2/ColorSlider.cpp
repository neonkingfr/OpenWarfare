// ColorSlider.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "ColorSlider.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorSlider

CColorSlider::CColorSlider()
  {
  value=0;
  }

//--------------------------------------------------

CColorSlider::~CColorSlider()
  {
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CColorSlider, CWnd)
  //{{AFX_MSG_MAP(CColorSlider)
  ON_WM_PAINT()
    ON_WM_LBUTTONDOWN()
      ON_WM_MOUSEMOVE()
        //}}AFX_MSG_MAP
        END_MESSAGE_MAP()
          
          
          /////////////////////////////////////////////////////////////////////////////
          // CColorSlider message handlers
          
          #define STEP 10
          
          void CColorSlider::OnPaint() 
            {
            CPaintDC dc(this); // device context for painting
            CRect rc;
            GetClientRect(&rc);
            int xm=rc.right-STEP;
            int xval=(int)(value*(rc.right-10))+5;  
            bool draw=true;
            for (int x=0;x<xm;x+=STEP)
              {
              int r=255*x/xm;
              int b=255-r;
              dc.FillSolidRect(x,0,STEP,rc.bottom,RGB(r,0,b));
              if (draw && x>xval) 
                {DrawPos(dc,xval,rc.bottom);draw=false;}
              }
            if (draw) DrawPos(dc,xval,rc.bottom);
            }

//--------------------------------------------------

void CColorSlider::DrawPos(CDC &dc, int pos,int size)
  {
  CPen pen(PS_SOLID,3,RGB(0,0,0)),*old;
  old=dc.SelectObject(&pen);
  dc.MoveTo(pos,1);
  dc.LineTo(pos,size-2);
  dc.SelectObject(old);
  }

//--------------------------------------------------

void CColorSlider::OnLButtonDown(UINT nFlags, CPoint point) 
  {
  CRect rc;
  GetClientRect(&rc);
  value=(float)(point.x-5)/(rc.right-10);	  
  if (value<0.0f) value=0.0f;
  if (value>1.0f) value=1.0f;
  Invalidate(FALSE);
  GetParent()->PostMessage(WM_HSCROLL,0,(LPARAM)(this->m_hWnd));
  }

//--------------------------------------------------

void CColorSlider::OnMouseMove(UINT nFlags, CPoint point) 
  {
  if (nFlags & MK_LBUTTON)
    {
    CRect rc;
    GetClientRect(&rc);
    value=(float)(point.x-5)/(rc.right-10);	  
    if (value<0.0f) value=0.0f;
    if (value>1.0f) value=1.0f;
    Invalidate(FALSE);
    GetParent()->PostMessage(WM_HSCROLL,0,(LPARAM)(this->m_hWnd));
    }
  }

//--------------------------------------------------

