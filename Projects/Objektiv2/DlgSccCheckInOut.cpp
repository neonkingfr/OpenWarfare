// DlgSccCheckInOut.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgSccCheckInOut.h"
#include ".\dlgscccheckinout.h"


// CDlgSccCheckInOut dialog

IMPLEMENT_DYNAMIC(CDlgSccCheckInOut, CDialog)
CDlgSccCheckInOut::CDlgSccCheckInOut(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSccCheckInOut::IDD, pParent)
    , vComments(_T(""))
    , vKeepCheck(FALSE)
    , vCheckOther(FALSE)
  {
}

CDlgSccCheckInOut::~CDlgSccCheckInOut()
{
}

void CDlgSccCheckInOut::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Text(pDX, IDC_COMMENTS, vComments);
DDX_Check(pDX, IDC_KEEPCHECK, vKeepCheck);
DDX_Check(pDX, IDC_CHECKOTHER, vCheckOther);
}


BEGIN_MESSAGE_MAP(CDlgSccCheckInOut, CDialog)
END_MESSAGE_MAP()


// CDlgSccCheckInOut message handlers

BOOL CDlgSccCheckInOut::OnInitDialog()
  {
  CDialog::OnInitDialog();
  GetDlgItem(IDC_KEEPCHECK)->EnableWindow(enableKeepCheck);
  GetDlgItem(IDC_CHECKOTHER)->EnableWindow(enableKeepCheck);
  CheckDlgButton(IDC_CHECKOTHER,1);
  return TRUE;
  }
