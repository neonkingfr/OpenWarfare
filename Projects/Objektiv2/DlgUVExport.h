#pragma once


// DlgUVExport dialog

class DlgUVExport : public CFileDialogEx
{
	DECLARE_DYNAMIC(DlgUVExport)

public:
	DlgUVExport(BOOL bOpenFileDialog,  LPCTSTR lpszDefExt = NULL , LPCTSTR lpszFileName  = NULL , DWORD dwFlags  = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT , LPCTSTR lpszFilter  = NULL , CWnd* pParentWnd  = NULL , DWORD dwSize  = 0 );
	virtual ~DlgUVExport();

// Dialog Data
	enum { IDD = IDD_UVEXPORTUV };

protected:
  CButton _wKeepAspect;
  CButton _wAddTextureOutline;
  CButton _wSpecifyDim;
  CEdit _wDimWidth;
  CEdit _wDimHeight;

  void DialogRules();

public:

  bool _keepAspect;
  bool _addTextureOutline;
  bool _specifyDim;
  int _dimWidth;
  int _dimHeight;

  static bool Save_keepAspect;
  static bool Save_addTextureOutline;
  static bool Save_specifyDim;
  static int Save_dimWidth;
  static int Save_dimHeight;

  float _aspect;




	DECLARE_MESSAGE_MAP()
  virtual BOOL OnInitDialog();
  afx_msg void OnBnClickedCheck3();
protected:
  virtual BOOL OnFileNameOK();
public:
  afx_msg void OnEnChangeEdit1();
  afx_msg void OnEnChangeEdit5();
  afx_msg void OnBnClickedCheck1();
};
