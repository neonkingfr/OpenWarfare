// expressxx.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdio.h"
#include "strstrea.h"
#include "LexAnl.h"
#include <string.h>
#include "LexTree.h"


int main(int argc, char* argv[])
  {
  char text[1024];
  
  strcpy(text,"A=1;N=10;i=1;while(i<=N,A=A*i;i=i+1);A");
  istrstream s(text,strlen(text));
  CLexAnl lxanl(s);
  CLexTree lxtree;
  if (lxtree.Parse(lxanl)==false)
    {
    printf("Error before %d character\n",s.tellg()+1);	
    }
  else
    {
    float p;
    lxtree.Calculate(p);
    printf("%f\n",p);
    }
  return 0;
  }

//--------------------------------------------------

