// StringRes.cpp: implementation of the CStringRes class.
//
//////////////////////////////////////////////////////////////////////

#include <TCHAR.h>
#include <windows.h>
#include "StringRes.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

template<>
void BTreeUnsetItem<SStringResInfo>(SStringResInfo &item)
  {
  item.idc=0;  
  }


CStringRes::CStringRes()
{
  
}

CStringRes::~CStringRes()
{
SStringResInfo *first=strlist.Smallest(),*next;
if (first)
  {
  BTreeIterator<SStringResInfo> iter(strlist);
  iter.BeginFrom(*first);
  while ((next=iter.Next())!=NULL) delete [] next->text;
  }
}

const _TCHAR *CStringRes::operator[](int idc)
{
  SStringResInfo find;
  find.idc=idc;
  SStringResInfo *item=strlist.Find(find);
  if (item) return item->text;
 
  _TCHAR fx[256];
  _TCHAR *buff=fx;
  int size=256;
  int s=LoadString(hInst,idc,buff,size);
  while (size-	1==s)
  {
    if (buff!=fx) delete [] buff;
    size*=2;
    buff=new _TCHAR[size];	  	
    s=LoadString(hInst,idc,buff,size);
  }
  size_t nwsz=_tcslen(buff)+1;
  find.text=new _TCHAR[nwsz];
  _tcscpy(find.text,buff);
  find.idc=idc;  
  strlist.Add(find);
  if (buff!=fx) delete [] buff;  
  return find.text;
}

