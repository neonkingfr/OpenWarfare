#include "..\stdafx.h"



/*----------------------------------------------------------------------------*\
This is a lib which reads 3d-studio binary files from version 3.0
and higher
(v1.05)
author: Martin van Velsen
( and some great help by Gert van der Spoel )
email:  vvelsen@ronix.ptf.hro.nl

	If you happen to come across some variables with strange names, then
	that will possible be Dutch names, sorry for that :)
	
\*----------------------------------------------------------------------------*/
#include "wpch.hpp"
#include "i3ds.h"

#include <stdarg.h>
#include <windows.h>

#include "objobje.hpp"

#ifdef _DEBUG
#define __DEBUG__ 1
#endif

static void LLogF( const char *format, ... )
  {
  static char buf[1024];
  
  va_list arglist;
  va_start( arglist, format );
  
  vsprintf(buf+strlen(buf),format,arglist);
  
  va_end( arglist );
  
  if( strchr(buf,'\n') )
    {
    OutputDebugString(buf);
    *buf=0;
    }
  }

//--------------------------------------------------

inline void NoF( const char *f, ... )
  {}

//--------------------------------------------------

#if _DEBUG
#define DebugF LLogF
#define OutF LLogF
#else
#define DebugF NoF
#define OutF NoF
#endif



ObjectData *import=NULL;


class ImportObject
  {
  public:
    virtual void BeginSelection( const char *name ); // start named selection
    virtual void EndSelection();
    
    virtual void SetTransform( float matrix[4][4] );
    
    virtual void BeginPoints( int count );
    virtual void AddPoint( float x, float y, float z);
    virtual void EndPoints();
    
    virtual void BeginTris( int count );
    virtual void AddTri( int a, int b, int c, bool ab, bool bc, bool ca );
    virtual void EndTris();
    
    
    // switch to some selection
    virtual void Select( const char *name );
    virtual void Unselect();
    
    virtual void CreateKeyframes( int count );
    
    virtual void StartKeyTrans( int frames );
    virtual void NextKeyTrans( int num, float x, float y, float z );
    virtual void EndKeyTrans();
    
    virtual void StartKeyRot( int frames );
    virtual void NextKeyRot( int num, float angle, float x, float y, float z );
    virtual void EndKeyRot();
    
    virtual void StartKeyScale( int frames );
    virtual void NextKeyScale( int num, float x, float y, float z );
    virtual void EndKeyScale();
  };

//--------------------------------------------------

ImportObject importer;

WString lastSel;

void ImportObject::BeginSelection( const char *name )
  {
  if( !import ) return;
  DebugF("Begin selection %s\n",name);
  import->ClearSelection();
  lastSel=name;
  }

//--------------------------------------------------

void ImportObject::EndSelection()
  {
  if( !import ) return;
  DebugF("End selection\n");
  import->SaveNamedSel(lastSel);
  
  }

//--------------------------------------------------

struct TransformObject
  {
  Matrix4 transform;
  WString name;
  };

//--------------------------------------------------

TypeIsGeneric(TransformObject);

static AutoArray<TransformObject> selBase;

const Matrix4 &FindBase( const char *name )
  {
  for( int i=0; i<selBase.Size(); i++ )
    {
    if( !strcmpi(selBase[i].name,name) ) return selBase[i].transform;
    }
  return MIdentity;
  }

//--------------------------------------------------

Matrix4 cumulate;

/*
static void FlipRows( int r1, int r2 )
{
    
}
*/

void ImportObject::SetTransform( float matrix[4][4] )
  {
  // aplly to all points in current selection
  DebugF ("  Apply transform:\n");
  int i;
  for (i=0;i<4;i++)
    {
    DebugF
      (
        "   | %6.2f %6.2f %6.2f %6.2f |\n",
    matrix [i][0],
    matrix [i][1],
    matrix [i][2],
    matrix [i][3]
      );
    }
  TransformObject to;
  to.transform=Matrix4
    (
      matrix[0][0], matrix[1][0], matrix[2][0], matrix[3][0],
  matrix[0][1], matrix[1][1], matrix[2][1], matrix[3][1],
  matrix[0][2], matrix[1][2], matrix[2][2], matrix[3][2]
    );
  to.name=lastSel;
  selBase.Add(to);
  }

//--------------------------------------------------

static int faceOffset;

void ImportObject::BeginPoints( int count )
  {
  //OutF("  Begin points %d\n",count);
  if( !import ) return;
  faceOffset=import->NPoints();
  }

//--------------------------------------------------

void ImportObject::AddPoint( float x, float y, float z )
  {
  DebugF("    (%8.2f,%8.2f,%8.2f)\n",x,y,z);
  if( !import ) return;
  PosT *p=import->NewPoint();
  (*p)[0]=x;
  (*p)[1]=y;
  (*p)[2]=z;
  int index=p-&import->Point(0);
  import->PointSelect(index);
  }

//--------------------------------------------------

void ImportObject::EndPoints()
  {
  //OutF("  End points\n");
  }

//--------------------------------------------------

void ImportObject::BeginTris( int count )
  {
  //OutF("  Begin tris %d\n",count);
  
  }

//--------------------------------------------------

void ImportObject::AddTri( int a, int b, int c, bool ab, bool bc, bool ca )
  {
  //OutF("    Add tri %d,%d,%d,%d,%d,%d\n",a,b,c,ab,bc,ca);
  if( !import ) return;
  FaceT face(import,import->NewFace());
  face.SetN(3);
  
  a+=faceOffset;
  b+=faceOffset;
  c+=faceOffset;
  
  face.SetPoint(0,a);
  face.SetPoint(1,b);
  face.SetPoint(2,c);
  face.SetNormal(0,0);
  face.SetNormal(1,0);
  face.SetNormal(2,0);
  // edges
  if( ab )  import->AddSharpEdge(a,b);
  if( bc )  import->AddSharpEdge(b,c);
  if( ca )  import->AddSharpEdge(c,a);
  
  import->FaceSelect(face.GetFaceIndex());
  }

//--------------------------------------------------

void ImportObject::EndTris()
  {
  //OutF("  End tris\n");
  }

//--------------------------------------------------

static AutoArray<Matrix4> transform;

static Matrix4 baseTrans;
static Vector3 pivot;

void ImportObject::Select( const char *name )
  {
  OutF("  Select %s\n",name);
  if( !import ) return;
  import->UseNamedSel(name);
  
  baseTrans=FindBase(name);
  // create appropriate number of keyframes
  for( int i=0; i<transform.Size(); i++ )
    {
    transform[i]=MIdentity;
    }
  OutF("  Initialized %d keyframes\n",transform.Size());
  }

//--------------------------------------------------

void ImportObject::Unselect()
  {
  OutF("  Unselect\n");
  if( !import ) return;
  // apply defined transformation
  
  Matrix4 baseInv=baseTrans.InverseScaled();
  
  for( int i=0; i<transform.Size(); i++ )
    {
    
    AnimationPhase *anim=NULL;
    if( i>=import->NAnimations() )
      {
      float time=float(i)/transform.Size();
      AnimationPhase an(import);
      an.Redefine(import);
      an.SetTime(time);
      import->AddAnimation(an);
      
      //OutF("add anim %d\n",import->NAnimations());
      
      int index=import->NAnimations()-1;
      anim=import->GetAnimation(index);
      //OutF("select anim %d\n",index);
      }
    else
      {
      anim=import->GetAnimation(i);
      //OutF("select anim %d\n",i);
      
      }
    //anim->SetTime(time);
    Matrix4Val tI=transform[i];
    // apply pivot 
    Matrix4 mPivot(MTranslation,-pivot);
    //Matrix4 mIPivot(MTranslation,pivot);
    
    //Matrix4 at=mPivot*tI*mPivot*baseInv;
    Matrix4 at=tI*mPivot*baseInv;
    
    for( int p=0; p<import->NPoints(); p++ )
      {
      if( !import->PointSelected(p) ) continue;
      Vector3 ap=at.FastTransform(import->Point(p));
      (*anim)[p]=Vector3(ap.X(),ap.Z(),ap.Y());
      }
    }
  import->ClearSelection();
  
  }

//--------------------------------------------------

void ImportObject::CreateKeyframes( int count )
  {
  if( !import ) return;
  OutF("  Total %d keyframes\n",count+1);
  transform.Resize(count+1);
  /*
    AnimationPhase anim(import);
    anim.Redefine(import);
    for( int i=0; i<=count; i++ )
    {
        float time=float(i)/count;
        anim.SetTime(time);
        import->AddAnimation(anim);
    }
    anim.SetTime(-1);
    import->AddAnimation(anim);
    */
  }

//--------------------------------------------------

static int lastKeyframe;
static Vector3 lastPosition;

void ImportObject::StartKeyTrans( int frames )
  {
  OutF("Start trans\n");
  lastKeyframe=-1;
  lastPosition=VZero;
  }

//--------------------------------------------------

void ImportObject::NextKeyTrans( int num, float x, float y, float z )
  {
  if( num<0 ) return;
  
  if( lastKeyframe<0 ) lastKeyframe=num-1;
  
  Vector3 off(x,y,z);
  for( int f=lastKeyframe+1; f<=num; f++ )
    {
    float factor=1;
    if( f<num )
      {
      // interpolation from last frame necessary
      factor=float(f-lastKeyframe)/(num-lastKeyframe);
      }
    //AnimationPhase *anim=import->GetAnimation(f);
    // apply transformation to all points selected
    DebugF("key %d translation factor %.3f\n",f,factor);
    transform[f].SetPosition(off*factor+lastPosition*(1-factor));
    transform[f].SetOrientation(M3Identity);
    }    
  lastKeyframe=num;
  lastPosition=off;
  }

//--------------------------------------------------

void ImportObject::EndKeyTrans() 
  {}

//--------------------------------------------------

void ImportObject::StartKeyRot( int frames )
  {
  OutF("Start rotation\n");
  lastKeyframe=-1;
  cumulate = MIdentity;
  }

//--------------------------------------------------

void ImportObject::NextKeyRot( int num, float angle, float x, float y, float z )
  {
  if( num<0 ) return;
  //angle = angle * (H_PI/180);
  
  // rotation around given axis
  Vector3 axis(x,y,z); //--
  
  // affects only orientation part of matrix
  if( axis.SquareSize()<1e-3 ) axis=VForward,angle=0;
  // rotate around given axis
  // align axis with z-axis or y-axis
  // find corresponding transformation
  /*
    const Matrix4 &baseTrans=FindBase(lastSel);
    
    Vector3 center=baseTrans.Position();
    */
  
  Matrix4 align(MIdentity);
  if( fabs(axis*VForward)>0.9 )
    {
    align.SetDirectionAndUp(axis,VUp);
    }
  else
    {
    align.SetDirectionAndUp(axis,VForward);
    }
  Matrix4 invAlign(MInverseRotation,align);
  //
  if( lastKeyframe<0 ) lastKeyframe=num-1;
  
  for( int f=lastKeyframe+1; f<=num; f++ )
    {
    float factor=1;
    if( f<num )
      {
      // interpolation from last frame necessary
      factor=float(f-lastKeyframe)/(num-lastKeyframe);
      }
    //AnimationPhase *anim=import->GetAnimation(f);
    // apply transformation to all points selected
    
    Matrix4 orient
      (
        align
          *Matrix4(MRotationZ,-angle)
            *invAlign
              );
    
    transform[f]=transform[f]* orient * cumulate;
    //transform[f]=orient*transform[f];
    DebugF("key %d rotation factor %.3f\n",f,factor);
    }    
  
  Matrix4 orient
    (
      align
        *Matrix4(MRotationZ,-angle)
          *invAlign
            );
  
  cumulate = orient * cumulate;
  
  lastKeyframe=num;
  
  }

//--------------------------------------------------

void ImportObject::EndKeyRot() 
  {}

//--------------------------------------------------

void ImportObject::StartKeyScale( int frames )
  {
  OutF("Start scale\n");
  lastKeyframe=-1;
  }

//--------------------------------------------------

void ImportObject::NextKeyScale( int num, float x, float y, float z )
  {
  // scale is ignored
  }

//--------------------------------------------------

void ImportObject::EndKeyScale() 
  {}

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned char ReadChar (void)
  {
  return (fgetc (bin3ds));
  
  //>------ if you want to add some code to create a progress bar, then
  //>------ I suggest you do it here. This is the only function which
  //>------ reads from disk
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned int ReadInt (void)
  {
  unsigned int temp = ReadChar();
  return ( temp | (ReadChar () << 8));
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadLong (void)
  {
  unsigned long temp1,temp2;
  //unsigned long temp3,temp4;
  
  temp1=ReadInt ();
  temp2=ReadInt ();
  
  //return (temp3+(temp4*0x10000L));
  return (temp1+(temp2*0x10000L));
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadChunkPointer (void)
  {
  return (ReadLong ());
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long GetChunkPointer (void)
  {
  return (ftell (bin3ds)-2); // compensate for the already read Marker
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
void ChangeChunkPointer (unsigned long temp_pointer)
  {
  int dif=temp_pointer - ftell (bin3ds);
  if( dif )
    {
    DebugF( " skip %d\n",dif );
    }
  fseek (bin3ds,temp_pointer,SEEK_SET);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
int ReadName (void)
  {
  unsigned int teller=0;
  unsigned char letter;
  
  strcpy (temp_name,"Default name");
  
  letter=ReadChar ();
  if (letter==0) return (-1); // dummy object
  temp_name [teller]=letter;
  teller++;
  
  do
    {
    letter=ReadChar ();
    temp_name [teller]=letter;
    teller++;
    }
  while ((letter!=0) && (teller<12));
  
  temp_name [teller-1]=0;
  
  #ifdef __DEBUG__
  DebugF ("     Found name : %s\n",temp_name);
  #endif
  return (0);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
int ReadLongName (void)
  {
  unsigned int teller=0;
  unsigned char letter;
  
  strcpy (temp_name,"Default name");
  
  letter=ReadChar ();
  if (letter==0) return (-1); // dummy object
  temp_name [teller]=letter;
  teller++;
  
  do
    {
    letter=ReadChar ();
    temp_name [teller]=letter;
    teller++;
    }
  while (letter!=0);
  
  temp_name [teller-1]=0;
  
  #ifdef __DEBUG__
  DebugF ("Found name : %s\n",temp_name);
  #endif
  return (0);
  }

//--------------------------------------------------

unsigned long ReadUnknownChunk (unsigned int chunk_id)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  
  chunk_id=chunk_id;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  /**/
  int len=temp_pointer-6;
  //DebugF("        Unknown chunk %0X len %d\n",chunk_id, len);
  OutF("        Unknown chunk %0X len %d\n",chunk_id, len);
  if( len<=40 )
    {
    char *data=new char[len];
    fread(data,len,1,bin3ds);
    OutF("        ");
    int i;
    for( i=0; i<len; i++ )
      {
      OutF("%02X ",(unsigned char)data[i]);
      }
    OutF("\n");
    
    OutF("        ");
    for( i=0; i<len; i++ )
      {
      int c=(unsigned char)data[i];
      OutF(" %c ",c>=' ' ? c : '#');
      }
    OutF("\n");
    delete[] data;
    }
  /**/
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

unsigned long SkipUnknownChunk (unsigned int chunk_id)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  
  chunk_id=chunk_id;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();	
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/

static int begFrame;
static int endFrame;

unsigned long ReadFramesChunk (unsigned int chunk_id)
  {
  unsigned long current_pointer=GetChunkPointer ();
  unsigned long temp_pointer   =ReadChunkPointer ();
  
  // some number (zero)
  unsigned long start=ReadLong();
  unsigned long end=ReadLong();
  
  OutF ("    Found frames %d to %d\n", start, end );
  
  importer.CreateKeyframes(end);
  
  begFrame=start;
  endFrame=end;
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

unsigned long ReadObjPivotChunk (unsigned int chunk_id)
  {
  //return ReadUnknownChunk(chunk_id);
  unsigned long current_pointer=GetChunkPointer ();
  unsigned long temp_pointer   =ReadChunkPointer ();
  
  //float pivot[3];
  
  fread (&pivot, sizeof (pivot),1,bin3ds);
  
  OutF ("    pivot %.2f,%.2f,%.2f\n", pivot[0],pivot[1],pivot[2]);
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

unsigned long ReadObjTransChunk (unsigned int chunk_id)
  {
  //return ReadUnknownChunk(chunk_id);
  #if 1
  unsigned long current_pointer=GetChunkPointer ();
  unsigned long temp_pointer   =ReadChunkPointer ();
  
  // skip 6B
  ReadLong();
  ReadInt();
  // read DWORD (number of frames)
  
  int maxFrames = ReadLong();
  int nFrames = ReadLong();
  
  DebugF ("    Frame count %d compressed to %d\n", maxFrames, nFrames);
  float array[3];
  int frameID=-1;
  importer.StartKeyTrans(endFrame-begFrame);
  for( int i=0; i<nFrames; i++ )
    {
    // for each frame:
    frameID=ReadInt();
    // skip 16B (4 floats?)
    
    int flags = ReadLong();
    
    fread (array,sizeof (array),1,bin3ds);
    
    DebugF ("    %d: Read move %d: ", i, frameID);
    DebugF ("%x %8.4f,%8.4f,%8.4f\n",flags,array[0],array[1],array[2]);
    
    importer.NextKeyTrans(frameID-begFrame,array[0],array[1],array[2]);
    frameID++;
    
    }
  
  if( frameID>=0 ) for( ; frameID<=endFrame; frameID++ )
    {
    importer.NextKeyTrans(frameID-begFrame,array[0],array[1],array[2]);
    }
  importer.EndKeyTrans();
  
  /*
	
	// some number (zero)
#ifdef __DEBUG__
	DebugF ("    Found frames %d to %d\n", start, end );
#endif
	
	*/
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  #endif
  }

//--------------------------------------------------

unsigned long ReadKeyNameChunk (unsigned int chunk_id)
  {
  unsigned long current_pointer=GetChunkPointer ();
  unsigned long temp_pointer   =ReadChunkPointer ();
  
  unsigned long tpointer;
  
  tpointer=ReadName ();
  
  importer.Select(temp_name);
  #ifdef __DEBUG__
  if (tpointer==-1)
    DebugF (">>>>* No section name found\n");
  #endif
  
  int hierarchy = ReadInt();
  ReadInt(); // dummy
  ReadInt(); // dummy
  
  tpointer+=8;
  
  
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

unsigned long ReadObjRotChunk (unsigned int chunk_id)
  {
  unsigned long current_pointer=GetChunkPointer ();
  unsigned long temp_pointer   =ReadChunkPointer ();
  
  // skip 6B
  ReadLong();
  ReadInt();
  // read DWORD (number of frames)
  
  int maxFrames = ReadLong();
  int nFrames = ReadLong();
  
  DebugF ("    Frame count %d compressed to %d\n", maxFrames, nFrames);
  
  //int rest=temp_pointer - 6 - 6 - 8;
  
  importer.StartKeyRot(endFrame-begFrame);
  
  int frameID=-1;
  float array[4];
  
  for( int i=0; i<nFrames; i++ )
    {
    // for each frame:
    frameID=ReadInt();
    
    int flags = ReadLong();
    
    fread (array,sizeof (array),1,bin3ds);
    
    DebugF ("    %d: Read rot %d: ", i, frameID);
    DebugF ("%x %8.4f,%8.4f,%8.4f,%8.4f\n",flags,array[0],array[1],array[2],array[3]);
    
    importer.NextKeyRot(frameID-begFrame,array[0],array[1],array[2],array[3]);
    frameID++;
    }
  
  if( frameID>=0 ) for( ; frameID<=endFrame; frameID++ )
    {
    importer.NextKeyRot(frameID-begFrame,0,0,0,0);
    }
  importer.EndKeyRot();
  
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

unsigned long ReadObjScaleChunk (unsigned int chunk_id)
  {
  unsigned long current_pointer=GetChunkPointer ();
  unsigned long temp_pointer   =ReadChunkPointer ();
  
  // skip 6B
  ReadLong();
  ReadInt();
  // read DWORD (number of frames)
  
  int maxFrames = ReadLong();
  int nFrames = ReadLong();
  
  DebugF ("    Frame count %d compressed to %d\n", maxFrames, nFrames);
  
  int rest=temp_pointer - 6 - 6 - 8;
  
  for( int i=0; i<nFrames; i++ )
    {
    // for each frame:
    int frameID=ReadInt();
    // skip 16B (4 floats?)
    float array[4];
    
    fread (array,sizeof (array),1,bin3ds);
    
    //DebugF ("    %d: Read scale %d ", i, frameID);
    //DebugF ("%8.4f,%8.4f,%8.4f,%8.4f\n",array[0],array[1],array[2],array[3]);
    
    }
  
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

unsigned long ReadObjDefChunk (unsigned int chunk_id)
  {
  unsigned char end_found=FALSE;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  while (end_found==FALSE)
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case KEYF_OBJUNKNWN04: // B021 unknown (rotation)
      #ifdef __DEBUG__
      DebugF ("      Found rotation chunk id of %0X\n",temp_int );
      #endif
      //tellertje+=ReadUnknownChunk (temp_int);
      tellertje+=ReadObjRotChunk (temp_int);
      break;
      case KEYF_OBJPIVOT: // translation
      #ifdef __DEBUG__
      DebugF ("      Found translation chunk id of %0X\n",temp_int );
      #endif
      //tellertje+=ReadUnknownChunk (temp_int);
      tellertje+=ReadObjTransChunk (temp_int);
      break;
      case KEYF_OBJUNKNWN05: // B022 unknown 32 B (scale ?)
      #ifdef __DEBUG__
      DebugF ("      Found scale chunk id of %0X\n",temp_int );
      #endif
      //tellertje+=ReadUnknownChunk (temp_int);
      tellertje+=ReadObjScaleChunk (temp_int);
      break;
      
      case KEYF_OBJHIERARCH: // B010: name, WORD, WORD WORD hierarchy
      #ifdef __DEBUG__
      DebugF ("      Found name chunk id of %0X\n",temp_int );
      #endif
      
      tellertje+=ReadKeyNameChunk (temp_int);
      //tellertje+=ReadUnknownChunk (temp_int);
      break;
      
      case KEYF_OBJUNKNWN01: // B013  3 floats
      DebugF ("      Found pivot? id of %0X\n",temp_int );
      tellertje+=ReadObjPivotChunk (temp_int);
      break;
      
      case KEYF_OBJUNKNWN06: // hierarchy ID WORD
      tellertje+=SkipUnknownChunk (temp_int);
      break;
      
      case KEYF_OBJDUMMYNAME:
      case KEYF_OBJUNKNWN02:  
      case KEYF_OBJUNKNWN03:   
      
      #ifdef __DEBUG__
      DebugF ("      Found objdef chunk id of %0X\n",temp_int );
      #endif
      tellertje+=ReadUnknownChunk (temp_int);
      break;
      
      default:
      #ifdef __DEBUG__
      DebugF ("      Found objdef unknown id of %0X\n",temp_int );
      #endif
      tellertje+=ReadUnknownChunk (temp_int);
      
      break;
      
      }
    tellertje+=2;
    if (tellertje>=temp_pointer)
      end_found=TRUE;
    }
  
  importer.Unselect();
  
  return tellertje;
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadRGBColor (void)
  {
  float rgb_val [3];
  
  for (int i=0;i<3;i++)
    fread (&(rgb_val [i]),sizeof (float),1,bin3ds);
  
  #ifdef __DEBUG__
  DebugF ("     Found Color (RGB) def of: R:%5.2f,G:%5.2f,B:%5.2f\n",
  rgb_val [0],
  rgb_val [1],
  rgb_val [2]);
  #endif
  
  return (12L);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadTrueColor (void)
  {
  unsigned char true_c_val [3];
  
  for (int i=0;i<3;i++)
    true_c_val [i]=ReadChar ();
  
  #ifdef __DEBUG__
  DebugF ("     Found Color (24bit) def of: R:%d,G:%d,B:%d\n",
  true_c_val [0],
  true_c_val [1],
  true_c_val [2]);
  #endif
  
  return (3L);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadBooleanChunk (unsigned char *boolean)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  *boolean=ReadChar ();
  
  ChangeChunkPointer (current_pointer+temp_pointer); // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadSpotChunk (void)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  float target [4];
  float hotspot,falloff;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  fread (&(target [0]),sizeof (float),1,bin3ds);
  fread (&(target [1]),sizeof (float),1,bin3ds);
  fread (&(target [2]),sizeof (float),1,bin3ds);
  fread (&hotspot,sizeof (float),1,bin3ds);
  fread (&falloff,sizeof (float),1,bin3ds);
  
  #ifdef __DEBUG__
  DebugF ("      The target of the spot is at: X:%5.2f Y:%5.2f Y:%5.2f\n",
  target [0],
  target [1],
  target [2]);
  DebugF ("      The hotspot of this light is : %5.2f\n",hotspot);
  DebugF ("      The falloff of this light is : %5.2f\n",falloff);
  #endif
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadLightChunk (void)
  {
  unsigned char end_found=FALSE,boolean;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L; // 2 id + 4 pointer
  float light_coors [3];
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  fread (&(light_coors [0]),sizeof (float),1,bin3ds);
  fread (&(light_coors [1]),sizeof (float),1,bin3ds);
  fread (&(light_coors [2]),sizeof (float),1,bin3ds);
  
  #ifdef __DEBUG__
  DebugF ("     Found light at coordinates: X: %5.2f, Y: %5.2f,Z: %5.2f\n",
  light_coors [0],
  light_coors [1],
  light_coors [2]);
  #endif
  
  while (end_found==FALSE)
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case LIT_UNKNWN01 :
      #ifdef __DEBUG__
      DebugF (">>>>> Found Light unknown chunk id of %0X\n",LIT_UNKNWN01);
      #endif
      tellertje+=ReadUnknownChunk (LIT_UNKNWN01);
      break;
      case LIT_OFF      :
      #ifdef __DEBUG__
      DebugF (">>>>> Light is (on/off) chunk: %0X\n",LIT_OFF);
      #endif
      tellertje+=ReadBooleanChunk (&boolean);
      #ifdef __DEBUG__
      if (boolean==TRUE)
        DebugF ("      Light is on\n");
      else
        DebugF ("      Light is off\n");
      #endif
      break;
      case LIT_SPOT     :
      #ifdef __DEBUG__
      DebugF (">>>>> Light is SpotLight: %0X\n",TRI_VERTEXL);
      #endif
      tellertje+=ReadSpotChunk ();
      break;
      case COL_RGB      :
      #ifdef __DEBUG__
      DebugF (">>>>> Found Color def (RGB) chunk id of %0X\n",temp_int);
      #endif
      tellertje+=ReadRGBColor ();
      break;
      case COL_TRU      :
      #ifdef __DEBUG__
      DebugF (">>>>> Found Color def (24bit) chunk id of %0X\n",temp_int);
      #endif
      tellertje+=ReadTrueColor ();
      break;
      default:
      DebugF("Unknown light chunk\n");
      break;
      }
    
    tellertje+=2;
    if (tellertje>=temp_pointer)
      end_found=TRUE;
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadCameraChunk (void)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  float camera_eye [3];
  float camera_focus [3];
  float rotation,lens;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  fread (&(camera_eye [0]),sizeof (float),1,bin3ds);
  fread (&(camera_eye [1]),sizeof (float),1,bin3ds);
  fread (&(camera_eye [2]),sizeof (float),1,bin3ds);
  
  #ifdef __DEBUG__
  DebugF ("     Found Camera viewpoint at coordinates: X: %5.2f, Y: %5.2f,Z: %5.2f\n",
  camera_eye [0],
  camera_eye [1],
  camera_eye [2]);
  #endif
  
  fread (&(camera_focus [0]),sizeof (float),1,bin3ds);
  fread (&(camera_focus [1]),sizeof (float),1,bin3ds);
  fread (&(camera_focus [2]),sizeof (float),1,bin3ds);
  
  #ifdef __DEBUG__
  DebugF ("     Found Camera focus coors at coordinates: X: %5.2f, Y: %5.2f,Z: %5.2f\n",
  camera_focus [0],
  camera_focus [1],
  camera_focus [2]);
  #endif
  
  fread (&rotation,sizeof (float),1,bin3ds);
  fread (&lens,sizeof (float),1,bin3ds);
  #ifdef __DEBUG__
  DebugF ("     Rotation of camera is:  %5.4f\n",rotation);
  DebugF ("     Lens in used camera is: %5.4fmm\n",lens);
  #endif
  
  if ((temp_pointer-38)>0) // this means more chunks are to follow
    {
    #ifdef __DEBUG__
    DebugF ("     **** found extra cam chunks ****\n");
    #endif
    if (ReadInt ()==CAM_UNKNWN01)
      {
      #ifdef __DEBUG__
      DebugF ("     **** Found cam 1 type ch ****\n");
      #endif
      ReadUnknownChunk (CAM_UNKNWN01);
      }
    if (ReadInt ()==CAM_UNKNWN02)
      {
      #ifdef __DEBUG__
      DebugF ("     **** Found cam 2 type ch ****\n");
      #endif
      ReadUnknownChunk (CAM_UNKNWN02);
      }
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadVerticesChunk (void)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  float vertices [3]; // x,y,z
  //unsigned int numb_v;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  numb_vertices  =ReadInt ();
  
  #ifdef __DEBUG__
  DebugF ("      Found (%d) number of vertices\n",numb_vertices);
  #endif
  
  importer.BeginPoints(numb_vertices);
  
  for (int i=0;i<numb_vertices;i++)
    {
    fread (&(vertices [0]),sizeof (float),1,bin3ds);
    fread (&(vertices [1]),sizeof (float),1,bin3ds);
    fread (&(vertices [2]),sizeof (float),1,bin3ds);
    
    importer.AddPoint(vertices[0],vertices[1],vertices[2]);
    }
  
  importer.EndPoints();
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/ 
unsigned long ReadSmoothingChunk ()
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long smoothing;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  for (int i=0;i<numb_faces;i++)
    {
    smoothing=ReadLong();
    smoothing=smoothing; // compiler warnig depressor *>:)
    //		DebugF ("      The smoothing group for face [%5d] is %d\n",i,smoothing);
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadFacesChunk (void)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned int temp_diff;
  unsigned int faces [6]; // a,b,c,Diff (Diff= AB: BC: CA: )
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  numb_faces     =ReadInt ();
  DebugF ("      Found (%d) number of faces\n",numb_faces);
  
  importer.BeginTris(numb_faces);
  
  for (int i=0;i<numb_faces;i++)
    {
    faces [0]=ReadInt ();
    faces [1]=ReadInt ();
    faces [2]=ReadInt ();
    temp_diff=ReadInt () & 0x000F;
    faces [3]=(temp_diff & 0x0004) >> 2;
    faces [4]=(temp_diff & 0x0002) >> 1;
    faces [5]=(temp_diff & 0x0001);
    
    importer.AddTri
      (
        faces [0],faces [1],faces [2],
    faces [3]!=0,faces [4]!=0,faces [5]!=0
      );
    }
  
  importer.EndTris();
  
  if (ReadInt ()==TRI_SMOOTH)
    ReadSmoothingChunk ();
  else
    DebugF ("      No smoothing groups found, assuming autosmooth\n");
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadTranslationChunk (void)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  for (int j=0;j<4;j++)
    {
    for (int i=0;i<3;i++)
      fread (&(trans_mat [j][i]),sizeof (float),1,bin3ds);
    }
  
  trans_mat [0][3]=0;
  trans_mat [1][3]=0;
  trans_mat [2][3]=0;
  trans_mat [3][3]=1;
  
  importer.SetTransform(trans_mat);
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/

unsigned long ReadObjectChunk (void);

unsigned long ReadObjChunk (void)
  {
  // temp name contains object name
  unsigned char end_found=FALSE,boolean=TRUE;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L; // 2 id + 4 pointer
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  DebugF ("Obj chunk len %d",temp_pointer);
  
  while ( tellertje<temp_pointer )
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case TRI_VERTEXL :
      #ifdef __DEBUG__
      DebugF (">>>>> Found Object vertices chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadVerticesChunk ();
      break;
      case TRI_FACEL1  :
      #ifdef __DEBUG__
      DebugF (">>>>> Found Object faces (1) chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadFacesChunk ();
      break;
      case TRI_FACEL2  :
      #ifdef __DEBUG__
      DebugF (">>>>> Found Object faces (2) chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadUnknownChunk (temp_int);
      break;
      case TRI_LOCAL  :
      #ifdef __DEBUG__
      DebugF (">>>>> Found Object translation chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadTranslationChunk ();
      break;
      case TRI_VISIBLE :
      #ifdef __DEBUG__
      DebugF (">>>>> Found Object vis/invis chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadBooleanChunk (&boolean);
      
      #ifdef __DEBUG__
      if (boolean==TRUE)
        DebugF ("      Object is (visible)\n");
      else
        DebugF ("      Object is (not visible)\n");
      #endif
      break;
      case EDIT_OBJECT:
      DebugF ("      CAUTION: Object chunk found\n");
      // proper recursion handling
      tellertje+=ReadObjectChunk ();
      break;
      default:
      DebugF ("      Unknown obj chunk\n");
      tellertje+=ReadUnknownChunk (temp_int);
      break;
      }
    
    tellertje+=2;
    /*
			 if (tellertje>=temp_pointer)
				 end_found=TRUE;
             */
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadObjectChunk (void)
  {
  unsigned char end_found=FALSE;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L; // 2 id + 4 pointer
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  DebugF ("Object chunk len %d",temp_pointer);
  
  if (ReadName ()==-1)
    {
    #ifdef __DEBUG__
    DebugF (">>>>* Dummy Object found\n");
    #endif
    }
  
  tellertje = ftell(bin3ds)-current_pointer;
  
  importer.BeginSelection(temp_name);
  
  while (tellertje<temp_pointer)
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case OBJ_UNKNWN01:tellertje+=ReadUnknownChunk (OBJ_UNKNWN01);break;
      case OBJ_UNKNWN02:tellertje+=ReadUnknownChunk (OBJ_UNKNWN02);break;
      case OBJ_TRIMESH :
      #ifdef __DEBUG__
      DebugF (">>>> Found Obj/Mesh chunk id of %0X\n",
      OBJ_TRIMESH);
      #endif
      tellertje+=ReadObjChunk ();
      break;
      case OBJ_LIGHT   :
      #ifdef __DEBUG__
      DebugF (">>>> Found Light chunk id of %0X\n",
      OBJ_LIGHT);
      #endif
      tellertje+=ReadLightChunk ();
      break;
      case OBJ_CAMERA  :
      #ifdef __DEBUG__
      DebugF (">>>> Found Camera chunk id of %0X\n",
      OBJ_CAMERA);
      #endif
      tellertje+=ReadCameraChunk ();
      break;
      default:
        {
        DebugF("      Unknown object chunk %x\n",temp_int);
        tellertje+=ReadUnknownChunk (temp_int);
        }
      break;
      }
    
    tellertje+=2;
    }
  
  importer.EndSelection();
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadBackgrChunk (void)
  {
  unsigned char end_found=FALSE;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L; // 2 id + 4 pointer
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  while (end_found==FALSE)
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case COL_RGB :
      #ifdef __DEBUG__
      DebugF (">> Found Color def (RGB) chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadRGBColor ();
      break;
      case COL_TRU :
      #ifdef __DEBUG__
      DebugF (">> Found Color def (24bit) chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadTrueColor ();
      break;
      default:
      DebugF("Unknown background chunk\n");
      break;
      }
    
    tellertje+=2;
    if (tellertje>=temp_pointer)
      end_found=TRUE;
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadAmbientChunk (void)
  {
  unsigned char end_found=FALSE;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L; // 2 id + 4 pointer
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  while (end_found==FALSE)
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case COL_RGB :
      #ifdef __DEBUG__
      DebugF (">>>> Found Color def (RGB) chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadRGBColor ();
      break;
      case COL_TRU :
      #ifdef __DEBUG__
      DebugF (">>>> Found Color def (24bit) chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadTrueColor ();
      break;
      default:
      DebugF("Unknown background chunk\n");
      break;
      }
    
    tellertje+=2;
    if (tellertje>=temp_pointer)
      end_found=TRUE;
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long FindCameraChunk (void)
  {
  long temp_pointer=0L;
  
  for (int i=0;i<12;i++)
    ReadInt ();
  
  temp_pointer=11L;
  temp_pointer=ReadName ();
  
  #ifdef __DEBUG__
  if (temp_pointer==-1)
    DebugF (">>>>* No Camera name found\n");
  #endif
  
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadViewPortChunk (void)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned int port,attribs;
  
  views_read++;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  attribs=ReadInt ();
  if (attribs==3)
    {
    #ifdef __DEBUG__
    DebugF ("<Snap> active in viewport\n");
    #endif
    }
  if (attribs==5)
    {
    #ifdef __DEBUG__
    DebugF ("<Grid> active in viewport\n");
    #endif
    }
  
  for (int i=1;i<6;i++) ReadInt (); // read 5 ints to get to the viewport
  
  port=ReadInt ();
  if ((port==0xFFFF) || (port==0))
    {
    FindCameraChunk ();
    port=CAMERA;
    }
  
  #ifdef __DEBUG__
  DebugF ("Reading [%s] information with id:%d\n",viewports [port],port);
  #endif
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadViewChunk (void)
  {
  unsigned char end_found=FALSE;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  while (end_found==FALSE)
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case EDIT_VIEW_P1 :
      #ifdef __DEBUG__
      DebugF (">>>> Found Viewport1 chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadViewPortChunk ();
      break;
      case EDIT_VIEW_P2 :
      #ifdef __DEBUG__
      DebugF (">>>> Found Viewport2 (bogus) chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadUnknownChunk (EDIT_VIEW_P2);
      break;
      case EDIT_VIEW_P3 :
      #ifdef __DEBUG__
      DebugF (">>>> Found Viewport chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadViewPortChunk ();
      break;
      default:
      DebugF("Unknown view chunk\n");
      break;
      }
    
    tellertje+=2;
    if (tellertje>=temp_pointer)
      end_found=TRUE;
    
    if (views_read>3)
      end_found=TRUE;
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadMatDefChunk (void)
  {
  unsigned long current_pointer;
  unsigned long temp_pointer;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  if (ReadLongName ()==-1)
    {
    #ifdef __DEBUG__
    DebugF (">>>>* No Material name found\n");
    #endif
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadMaterialChunk (void)
  {
  unsigned char end_found=FALSE;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  while (end_found==FALSE)
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case MAT_NAME01  :
      #ifdef __DEBUG__
      DebugF (">>>> Found Material def chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadMatDefChunk ();
      break;
      default:
      //DebugF("Unknown material chunk\n");
      break;
      }
    
    tellertje+=2;
    if (tellertje>=temp_pointer)
      end_found=TRUE;
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadEditChunk (void)
  {
  unsigned char end_found=FALSE;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  while (end_found==FALSE)
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case EDIT_UNKNW01:tellertje+=ReadUnknownChunk (EDIT_UNKNW01);break;
      case EDIT_UNKNW02:tellertje+=ReadUnknownChunk (EDIT_UNKNW02);break;
      case EDIT_UNKNW03:tellertje+=ReadUnknownChunk (EDIT_UNKNW03);break;
      case EDIT_UNKNW04:tellertje+=ReadUnknownChunk (EDIT_UNKNW04);break;
      case EDIT_UNKNW05:tellertje+=ReadUnknownChunk (EDIT_UNKNW05);break;
      case EDIT_UNKNW06:tellertje+=ReadUnknownChunk (EDIT_UNKNW06);break;
      case EDIT_UNKNW07:tellertje+=ReadUnknownChunk (EDIT_UNKNW07);break;
      case EDIT_UNKNW08:tellertje+=ReadUnknownChunk (EDIT_UNKNW08);break;
      case EDIT_UNKNW09:tellertje+=ReadUnknownChunk (EDIT_UNKNW09);break;
      case EDIT_UNKNW10:tellertje+=ReadUnknownChunk (EDIT_UNKNW10);break;
      case EDIT_UNKNW11:tellertje+=ReadUnknownChunk (EDIT_UNKNW11);break;
      case EDIT_UNKNW12:tellertje+=ReadUnknownChunk (EDIT_UNKNW12);break;
      case EDIT_UNKNW13:tellertje+=ReadUnknownChunk (EDIT_UNKNW13);break;
      
      case EDIT_MATERIAL :
      #ifdef __DEBUG__
      DebugF (">>> Found Materials chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadMaterialChunk ();
      break;
      case EDIT_VIEW1    :
      #ifdef __DEBUG__
      DebugF (">>> Found View main def chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadViewChunk ();
      break;
      case EDIT_BACKGR   :
      #ifdef __DEBUG__
      DebugF (">>> Found Backgr chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadBackgrChunk ();
      break;
      case EDIT_AMBIENT  :
      #ifdef __DEBUG__
      DebugF (">>> Found Ambient chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadAmbientChunk ();
      break;
      case EDIT_OBJECT   :
      #ifdef __DEBUG__
      DebugF (">>> Found Object chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadObjectChunk ();
      break;
      default:
      //DebugF("    Unknown edit chunk %x\n",temp_int);
      break;
      }
    
    tellertje+=2;
    if (tellertje>=temp_pointer)
      end_found=TRUE;
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadKeyfChunk (void)
  {
  unsigned char end_found=FALSE;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  
  #if 0
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  #else
  
  while (end_found==FALSE)
    {
    unsigned int temp_int;
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case KEYF_UNKNWN01 :
      tellertje+=ReadUnknownChunk (temp_int);
      // some DWORD (not always zero - it was 0x320 )
      break;
      case KEYF_UNKNWN02 :
      // len 15
      // some kind of description?
      //  0   1  2 WORD small number (5)
      //  2   9  8 "MAXSCENE" // file name
      //  10 10  1 zero
      //  11 12  2 WORD // total number of frames
      //  13 14  2 WORD zero
      
      //tellertje+=ReadUnknownChunk (temp_int);
      tellertje+=ReadUnknownChunk (temp_int);
      break;
      case KEYF_FRAMES   :
      #ifdef __DEBUG__
      DebugF (">>> Found Keyframer frames chunk id of %0X\n",
      temp_int);
      #endif
      //ReadFramesChunk ReadUnknownChunk
      tellertje+=ReadFramesChunk (temp_int);
      break;
      case KEYF_OBJDES   :
      #ifdef __DEBUG__
      DebugF (">>> Found Keyframer object description chunk id of %0X\n",
      temp_int);
      #endif
      // composed of many chunks
      
      tellertje+=ReadObjDefChunk (temp_int);
      break;
      case EDIT_VIEW1    :
      #ifdef __DEBUG__
      DebugF (">>> Found View main def chunk id of %0X\n",
      temp_int);
      #endif
      tellertje+=ReadViewChunk ();
      break;
      default:
      DebugF("Unknown keyframe chunk\n");
      break;
      }
    
    tellertje+=2;
    if (tellertje>=temp_pointer)
      end_found=TRUE;
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  #endif
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
unsigned long ReadMainChunk (void)
  {
  unsigned char end_found=FALSE;
  unsigned int temp_int;
  unsigned long current_pointer;
  unsigned long temp_pointer;
  unsigned long tellertje=6L;
  
  current_pointer=GetChunkPointer ();
  temp_pointer   =ReadChunkPointer ();
  
  while (end_found==FALSE)
    {
    temp_int=ReadInt ();
    
    switch (temp_int)
      {
      case KEYF3DS :
      #ifdef __DEBUG__
      DebugF (">> Found *Keyframer* chunk id of %0X\n",KEYF3DS);
      #endif
      tellertje+=ReadKeyfChunk ();
      break;
      case EDIT3DS :
      #ifdef __DEBUG__
      DebugF (">> Found *Editor* chunk id of %0X\n",EDIT3DS);
      #endif
      tellertje+=ReadEditChunk ();
      break;
      default:
      //DebugF("  Unknown main chunk %x\n",temp_int);
      break;
      }
    
    tellertje+=2;
    if (tellertje>=temp_pointer)
      end_found=TRUE;
    }
  
  ChangeChunkPointer (current_pointer+temp_pointer); 
  // move to the new chunk position
  return (temp_pointer);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
int ReadPrimaryChunk (void)
  {
  unsigned char version;
  
  if (ReadInt ()==MAIN3DS)
    {
    #ifdef __DEBUG__
    DebugF ("> Found Main chunk id of %0X\n",MAIN3DS);
    #endif
    //>---------- find version number
    fseek (bin3ds,28L,SEEK_SET);
    version=ReadChar ();
    if (version<3)
      {
      #ifdef __DEBUG__
      DebugF ("Sorry this lib can only read 3ds files of version 3.0 and higher\n");
      DebugF ("The version of the file you want to read is: %d\n",version);
      #endif
      return (1);
      }
    fseek (bin3ds,2,SEEK_SET);
    ReadMainChunk ();
    }
  else
    return (1);
  
  return (0);
  }

//--------------------------------------------------

/*----------------------------------------------------------------------------*/
/*                      Test Main for the 3ds-bin lib                         */
/*----------------------------------------------------------------------------*/



int ObjectData::Load3DS( const char *name )
  {
  
  bin3ds=fopen (name,"rb");
  if (bin3ds==NULL)
    {
    return (-1);
    }
  
  #ifdef __DEBUG__
  DebugF ("\nLoading 3ds binary file : %s\n",name);
  #endif
  
  import=this;
  selBase.Clear();
  
  cumulate=MIdentity;
  
  while (ReadPrimaryChunk ()==0) 
    {}
  fclose(bin3ds);
  bin3ds=NULL;
  
  
  ClearSelection();
  
  DoRecalcNormals();
  
  SortAnimations();
  UseAnimation(0);
  
  import=NULL;
  
  return 0;
  }

//--------------------------------------------------

/*
int main (int argc,char **argv)
{
	argc=argc;
	
	bin3ds=fopen (argv [1],"rb");
	if (bin3ds==NULL)
		return (-1);
	
#ifdef __DEBUG__
  DebugF ("\nLoading 3ds binary file : %s\n",argv [1]);
#endif
	while (ReadPrimaryChunk ()==0);
	
	return (0);
}
*/

