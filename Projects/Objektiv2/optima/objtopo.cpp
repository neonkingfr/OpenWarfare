#include "..\stdafx.h"
// 3D model - topology operations
// (C) Ondrej Spanel, Suma, 6/1996

#include "wpch.hpp"
#include "global.hpp"

#include "objobje.hpp"

void ObjectData::CheckClosedTopology()
  {
  // check all edges for topology closiness
  int i;
  
  ClearSelection();
  for( i=0; i<NFaces(); i++ )
    {
    FaceT face(this,i);
    for( int j=0,last=face.N()-1; j<face.N(); last=j,j++ )
      {
      int iCurr=face.GetPoint(j);
      int iLast=face.GetPoint(last);
      int neighbourghs=0;
      for( int vs=0; vs<NFaces(); vs++ )
        {
        FaceT other(this,vs);
        if( other.ContainsEdge(iCurr,iLast) ) neighbourghs++;
        if( other.ContainsEdge(iLast,iCurr) ) neighbourghs--;
        }
      if( neighbourghs )
        {
        PointSelect(iCurr);   
        PointSelect(iLast);   
        }
      }
    //FaceSelect(i,open);
    }
  //SelectPointsFromFaces();
  }










struct Edge
  {
  int a,b;
  Edge( int v1, int v2 )
    {a=v1,b=v2;}
  Edge()
    {}
  int Compare( const Edge &with ) const
    {
    if( a!=with.a ) return a-with.a;
    if( b!=with.b ) return b-with.b;
    return 0;
    }
  bool operator ==  ( const Edge &with ) const 
    {return Compare(with)==0;}
  };










TypeIsSimple(Edge);

class Edges: public FindArray<Edge>
  {
  public:
    double HullVolume( const ObjectData *obj ) const; //  calculate volume of convex hull
    int HullNPoints( const ObjectData *obj ) const; //  calculate volume of convex hull
    
  };










/*
static void CheckMinMax( Point3D &min, Point3D &max, const Point3D &val )
{
	if( min.x>val.x ) min.x=val.x;
	if( min.y>val.y ) min.y=val.y;
	if( min.z>val.z ) min.z=val.z;
	if( max.x<val.x ) max.x=val.x;
	if( max.y<val.y ) max.y=val.y;
	if( max.z<val.z ) max.z=val.z;
}
*/

int Edges::HullNPoints( const ObjectData *obj ) const
  {
  FindArray<int> points;
  for( int i=0; i<Size(); i++ )
    {
    const Edge &edge=(*this)[i];
    points.AddUnique(edge.a);
    points.AddUnique(edge.b);
    }
  return points.Size();
  }










double Edges::HullVolume( const ObjectData *obj ) const
  {
  //  calculate volume of convex hull
  Point3D mmin(+1e10,+1e10,+1e10),mmax(-1e10,-1e10,-1e10);
  for( int i=0; i<Size(); i++ )
    {
    const Edge &edge=(*this)[i];
    const PosT &pA=obj->Point(edge.a);
    //Point3D pA=obj->Point(edge.a);
    const PosT &pB=obj->Point(edge.b);
    CheckMinMax(mmin,mmax,pA);
    CheckMinMax(mmin,mmax,pB);
    }
  //return (max.x-min.x)*(max.y-min.y)*(max.z-min.z);
  return (mmax-mmin).SquareSize();
  }










void ObjectData::CloseTopology()
  {
  // check all edges for topology closiness
  int i;
  
  ClearSelection();
  
  #define DO_REPEAT 0
  #if DO_REPEAT
  Repeat:
  #endif
  Edges openEdges;
  for( i=0; i<NFaces(); i++ )
    {
    FaceT face(this,i);
    bool open=false;
    for( int j=0,last=face.N()-1; j<face.N(); last=j,j++ )
      {
      int iCurr=face.GetPoint(j);
      int iLast=face.GetPoint(last);
      int neighbourghs=0;
      for( int vs=0; vs<NFaces(); vs++ )
        {
        FaceT other(this,vs);
        if( other.ContainsEdge(iCurr,iLast) ) neighbourghs++;
        if( other.ContainsEdge(iLast,iCurr) ) neighbourghs--;
        }
      if( neighbourghs )
        {
        PointSelect(iCurr);
        PointSelect(iLast);
        }
      while( neighbourghs>0 )
        {
        openEdges.Add(Edge(iLast,iCurr));
        neighbourghs--;
        }
      while( neighbourghs<0 )
        {
        openEdges.Add(Edge(iCurr,iLast));
        neighbourghs++;
        }
      }
    }
  // use list of all open edges
  // scan for connected components
  // and triangulate them
  for(;;)
    {
    bool someFace=false;
    int sV1,sV2,sV3;
    double minArea2=1e30;
    for( int s=0; s<openEdges.Size(); s++ )
      {
      // there must be at least two edges to produce face
      int v1=openEdges[s].a;
      int v2=openEdges[s].b;
      // we have to create a face with edge (v1,v2)
      for( int e=s+1; e<openEdges.Size(); e++ )
        {
        bool case1=false;
        bool case2=false;
        if( openEdges[e].a==v2 && openEdges[e].b!=v1 ) case1=true;
        if( openEdges[e].a!=v2 && openEdges[e].b==v1 ) case2=true;
        if( case1 || case2 )
          {
          int v3=case1 ? openEdges[e].b : openEdges[e].a;
          // avoid co-linear faces
          // we will try to create a new edge f1,f2
          //if( FindEdge(f1,f2) ) continue; // no edge
          Point3D p1=(Point3D)Point(v1)-(Point3D)Point(v3);
          Point3D p2=(Point3D)Point(v2)-(Point3D)Point(v3);
          Point3D p3=(Point3D)Point(v2)-(Point3D)Point(v1);
          float cosFi=p1*p2/(p1.Size()*p2.Size());
          if( fabs(cosFi)>=0.999 ) continue; // co-linear points
          //double area2=p1.CrossProduct(p2).SquareSize();
          
          // this face will change the number of close edges:
          bool sureYes=!someFace;
          bool sureNo=false;
          #if 1
          FaceT face(this);
          face.SetN(3);
          face.SetPoint(0,v1);
          face.SetPoint(1,v2);
          face.SetPoint(2,v3);
          double area2=face.CalculateArea();
          //area2=face.CalculatePerimeter(this);
          if( !sureYes && !sureNo )
            {
            if( minArea2>area2 ) sureYes=true;
            else if( minArea2<area2 ) sureNo=true;
            }
          #endif
          if( sureYes )
            {
            sV1=v1;
            sV2=v2;
            sV3=v3;
            minArea2=area2;
            someFace=true;
            }
          } // if( case )
        } // for(e)
      } // for(s)
    if( !someFace ) break; // no face found
    int index=NFaces();
    FaceT face(this,NewFace());
    face.SetN(3);
    face.SetPoint(0,sV1);
    face.SetPoint(1,sV2);
    face.SetPoint(2,sV3);
    for (int tmpi=0;tmpi<3;tmpi++) face.SetNormal(tmpi,0);
    FaceSelect(index);
    // (sV1,sV2), (sV2,sV3), (sV3,sV1) are added negNeighbourghs
    // this can be done as: either remove original pair or add reversed pair
    int f;
    if( (f=openEdges.Find(Edge(sV1,sV2)))<0 ) openEdges.Add(Edge(sV2,sV1));
    else openEdges.Delete(f);
    if( (f=openEdges.Find(Edge(sV2,sV3)))<0 ) openEdges.Add(Edge(sV3,sV2));
    else openEdges.Delete(f);
    if( (f=openEdges.Find(Edge(sV3,sV1)))<0 ) openEdges.Add(Edge(sV1,sV3));
    else openEdges.Delete(f);
    #if DO_REPEAT
    goto Repeat;
    #endif
    }
  RecalcNormals();
  Squarize(false);
  RecalcNormals();
  }










void ObjectData::RemoveComponents()
  {
  // to do: remove any named selection formated like Component<number>
  int skip=50;
  for( int i=1; skip>0; i++ )
    {
    WString name;
    name.Sprintf("Component%02d",i);
    if( !DeleteNamedSel(name) ) skip--;
    }
  }










void ObjectData::CreateComponents()
  {
  Selection save=_sel;
  // select components as used when usign 'O' selections
  bool *visited=new bool[NPoints()];
  if( !visited ) return;
  int i;
  for( i=0; i<NPoints(); i++ ) visited[i]=false;
  EdgeMatrix *edges=new EdgeMatrix(NPoints());
  if( !edges ) 
    {delete[] visited;return;}
  for( i=0; i<NFaces(); i++ )
    {
    const FaceT face(this,i);
    int j;
    for( j=0; j<face.N(); j++ )
      {
      int i0=face.GetPoint(j);
      int i1=face.GetPoint((j+1)%face.N());
      edges->AddEdge(i0,i1);
      }
    }
  // remove any components
  RemoveComponents();
  int component=0;
  for( i=0; i<NPoints(); i++ )
    {
    if( !visited[i] )
      {
      ClearSelection();
      PointSelect(i,true);
      visited[i]=true;
      PromoteComponent(visited,*edges,i,true);
      SelectFacesFromPoints(-1,SelSet);
      WString name;
      name.Sprintf("Component%02d",++component);
      SaveNamedSel(name);
      }
    }
  delete edges;
  delete[] visited;
  _sel=save;
  }










int ObjectData::CalculateInsideFaces
  (
  int faceIndex, bool onlySelected
  ) const
        {
        const FaceT face(this,faceIndex);
        Point3D normal=face.CalculateNormal();
        Point3D p0=Point(face.GetPoint(0));
        float d=-(normal*p0);
        int count=0;
        for( int i=0; i<NFaces(); i++ ) if( i!=faceIndex )
          {
          const FaceT check(this,i);
          for( int v=0; v<check.N(); v++ )
            {
            Point3D p=Point(check.GetPoint(v));
            float inside=p*normal+d;
            if( inside<=0 ) count++;
            }
          }
        return count;
        }












void ObjectData::SelectHalfSpace( const FaceT &face )
  {
  int i;
  for( i=0; i<NPoints(); i++ )
    {
    Point3D pt=Point(i);
    PointSelect(i,face.PointInHalfSpace(pt));
    }
  for( i=0; i<NFaces(); i++ )
    {
    const FaceT iFace(this,i);
    FaceSelect(i,face.FaceInHalfSpace(iFace));
    }
  }



// Changed by BREDY - Better implementation
bool ObjectData::SelectionSplit()
  {
  int i;
  int pcnt=0;
  int nmax=NPoints();
  int ncur=nmax;
  int *parray=new int[nmax];    //Alloc temporary array
  for (i=0;i<nmax;i++) parray[i]=i;
  for (i=0;i<NFaces();i++)
    {
    FaceT fc(this,i);
    if (!FaceSelected(i))
      for (int j=0;j<fc.N();j++)
        {        
        int pt=fc.GetPoint(j);
        if (PointSelected(pt) && parray[pt]==pt)
                parray[pt]=nmax++;      //Prepare array for adding new points
        }
    }    
  if (nmax==NPoints()) return false;
  ReservePoints(nmax-NPoints());    //Reserve new points
  for (i=0;i<ncur;i++) if (parray[i]!=i)    //Split points
    {
    PosT &ps=Point(parray[i]);
    ps=Point(i);
    for (int j=0;j<NAnimations();j++)       //Split points in animations
      {
      AnimationPhase *phs=GetAnimation(j);      
      (*phs)[parray[i]]=(*phs)[i];
      }
    PointSelect(i,false);
    PointSelect(parray[i],true);
    }
  for (i=0;i<NFaces();i++) if (FaceSelected(i)) //Rebuild faces
    {
    FaceT fc(this,i);
    for (int j=0;j<fc.N();j++)
      fc.SetPoint(j,parray[fc.GetPoint(j)]);
    } 
  delete [] parray;      //delete temporary array;
  return true;
  }


/*

bool ObjectData::SelectionSplit()
  {
  // unselect any points that are not used in any selected face
    {
    Temp<bool> selectedByFace(NPoints());
    int i;
    for( i=0; i<NPoints(); i++ ) selectedByFace[i]=false;
    for( i=0; i<NFaces(); i++ ) if( FaceSelected(i) )
      {
      const FaceT &face=Face(i);
      for( int v=0; v<face.N(); v++ ) selectedByFace[face.GetPoint(v)]=true;
      }
    for( i=0; i<NPoints(); i++ ) if( !selectedByFace[i] ) PointSelect(i,false);
    }
  
  // save current selection to a temporary object
  SRef<ObjectData> temp=new ObjectData(*this);
  temp->ExtractSelection();
  
  Temp<bool> wasUsed(NPoints());
  Temp<bool> isUsed(NPoints());
  int i;
  for( i=0; i<NPoints(); i++ ) wasUsed[i]=isUsed[i]=false;
  // scan which points were used
  for( i=0; i<NFaces(); i++ )
    {
    const FaceT &face=Face(i);
    for( int v=0; v<face.N(); v++ ) wasUsed[face.GetPoint(v)]=true;
    }
  // remove all selected faces
  for( i=NFaces(); --i>=0; ) if( FaceSelected(i) )
    {
    // scanned and convexFaces array ned be changed to reflect deletion
    DeleteFace(i);
    }
  // scan which points are now used
  for( i=0; i<NFaces(); i++ )
    {
    const FaceT &face=Face(i);
    for( int v=0; v<face.N(); v++ ) isUsed[face.GetPoint(v)]=true;
    }
  // remove all points that are no longer used (but was before)
  for( i=NPoints(); --i>=0; ) if( wasUsed[i] && !isUsed[i] )
    {
    // convexPoints array need be changed to reflect deletion
    DeletePoint(i);
    }
  // paste in the saved selection
  Merge(*temp);
  // recalculate normals
  RecalcNormals();
  return true;
  }



*/






void ObjectData::SelectionToConvexComponents()
  {
  // merge in single components
  SRef<ObjectData> result=new ObjectData();
  Temp<bool> scanned(NFaces());
  Temp<bool> inSelection(NFaces());
  Temp<bool> convexFaces(NFaces());
  Temp<bool> convexPoints(NPoints());
  int i;
  int nConvexPoints=0;
  // scan only for selected faces
  for( i=0; i<NFaces(); i++ ) inSelection[i]=FaceSelected(i);
  for( i=0; i<NFaces(); i++ ) scanned[i]=!FaceSelected(i);
  for( i=0; i<NPoints(); i++ ) convexPoints[i]=false;
  for( i=0; i<NFaces(); i++ ) convexFaces[i]=false;
  //bool convex=false;
  bool allScanned;
  int nParts=0;
  for(;;)
    {
    allScanned=true;
    // find last non-scanned face
    for(;;)
      { // scan all faces to see if we can add something
      bool noFace=true;
      for( i=NFaces(); --i>=0; ) if( !scanned[i] )
        {
        allScanned=false;
        // check if the component will remain convex
        // after add face 'i'
        // i.e. check if all faces until now are inside 
        FaceT faceI(this,i);
        bool neighbourgh=false;
        bool convex=true;
        if( nConvexPoints>=2 )
          {
          if( faceI.ContainsPoints(convexPoints)<2 ) continue;
          }
        else
          {
          neighbourgh=true;
          }
        // check if there is come common egde between faceI and convex faces
        int f;
        for( f=0; f<NFaces(); f++ ) if( convexFaces[f] )
          {
          FaceT faceF(this,f);
          if( faceI.IsNeighbourgh(faceF) ) neighbourgh=true;
          if
            (
              !faceI.FaceInHalfSpace(faceF) ||
                !faceF.FaceInHalfSpace(faceI)
                  )
                    {
                    convex=false;
                    break;
                    }
          }
        if( convex && neighbourgh )
          {
          scanned[i]=true;
          convexFaces[i]=true;
          int v;
          for( v=0; v<faceI.N(); v++ )
            {
            convexPoints[faceI.GetPoint(v)]=true;
            nConvexPoints++;
            }
          noFace=false;
          }
        }
      if( noFace ) break; // all faces scanned - nothing to add
      }
    // copy component we have
    if( allScanned ) break; // no more components
    nParts++;
    ClearSelection();
    int f;
    for( f=0; f<NFaces(); f++ )
      {
      FaceSelect(f,convexFaces[f]);
      }
    SelectPointsFromSelectedFaces(-1,SelSet);
    #if 1
    // save current selection to a temporary object
    SRef<ObjectData> temp=new ObjectData(*this);
    temp->ExtractSelection();
    
    // paste in the saved selection
    result->Merge(*temp);
    
    #else
    static int convex=0;
    WString name;
    name.Sprintf("Convex%02d",++convex);
    SaveNamedSel(name);
    #endif
    
    for( i=0; i<NFaces(); i++ ) convexFaces[i]=false;
    for( i=0; i<NPoints(); i++ ) convexPoints[i]=false;
    nConvexPoints=0;
    }
  
  if( nParts>1 )
    {
    // it the result is more than one component
    // remove all faces that belonged to the component
    
    Temp<bool> wasUsed(NPoints());
    Temp<bool> isUsed(NPoints());
    Temp<bool> normalUsed(NNormals());
    for( i=0; i<NPoints(); i++ ) wasUsed[i]=isUsed[i]=false;
    for( i=0; i<NNormals(); i++ ) normalUsed[i]=false;
    // scan which points were used
    for( i=0; i<NFaces(); i++ )
      {
      const FaceT face(this,i);
      for( int v=0; v<face.N(); v++ ) wasUsed[face.GetPoint(v)]=true;
      }
    // remove all faces of the component
    for( i=NFaces(); --i>=0; ) if( inSelection[i] )
      {
      DeleteFace(i);
      }
    // scan which points and normals are now used
    for( i=0; i<NFaces(); i++ )
      {
      const FaceT face(this,i);
      for( int v=0; v<face.N(); v++ )
        {
        isUsed[face.GetPoint(v)]=true;
        normalUsed[face.GetNormal(v)]=true;
        }
      }
    // remove all points that are no longer used (but was before)
    for( i=NPoints(); --i>=0; ) if( wasUsed[i] && !isUsed[i] )
      {
      DeletePoint(i);
      }
    // remove all normals that are no longer used
    for( i=NNormals(); --i>=0; ) if( !normalUsed[i] )
      {
      DeleteNormal(i);
      }
    
    // merge convex components
    Merge(*result);
    ClearSelection();
    }
  }










#if 0
void ObjectData::CreateConvexComponents()
  {
  // scan for components
  CreateComponents();
  int i;
  for( i=1; ; i++ )
    {
    WString name;
    name.Sprintf("Component%02d",i);
    if( !UseNamedSel(name) ) break; // all components done
    // split selection to convex components
    SelectionToConvexComponents();
    }
  // components changed - rescan
  CreateComponents();
  }










#endif

void ObjectData::CheckConvexComponent()
  {
  // check a component for convexity
  // check all pairs of vertices
  // check if all component vertices are inside of all component faces
  // 
  Temp<bool> badPoints(NPoints());
  int i,f;
  for( i=0; i<NPoints(); i++ ) badPoints[i]=false;
  for( f=0; f<NFaces(); f++ ) if( FaceSelected(f) )
    {
    FaceT face(this,f);
    Point3D normal=face.CalculateRawNormal();
    Point3D p0=Point(face.GetPoint(0));
    float d=-(normal*p0);
    for( i=0; i<NPoints(); i++ ) if( PointSelected(i) )
      {
      Point3D pI=Point(i);
      if( !FaceT::PointInHalfSpace(normal,d,pI) )
        {
        badPoints[i]=true;
        }
      }
    }
  ClearSelection();
  for( i=0; i<NPoints(); i++ ) PointSelect(i,badPoints[i]);
  }










void ObjectData::CheckConvexComponents()
  {
  // scan for components
  Selection result(this);
  result.Clear();
  CreateComponents();
  int i;
  for( i=1; ; i++ )
    {
    WString name;
    name.Sprintf("Component%02d",i);
    if( !UseNamedSel(name) ) break; // all components done
    // split selection to convex components
    CheckConvexComponent();
    result+=*GetSelection();
    }
  _sel=result;
  }










bool ObjectData::FaceCanBeInConvexHull
  (
  int i, int j, int k, bool allPoints
  )
        {
        if( j==i || k==i || j==k ) return false;
        FaceT face(this);
        face.SetN(3);
        face.SetPoint(0,i);
        face.SetPoint(1,j);
        face.SetPoint(2,k);
        Point3D normal=face.CalculateRawNormal();
        Point3D p0=Point(face.GetPoint(0));
        float d=-(normal*p0);
        int l;
        // check if all points are inside the new face
        for( l=0; l<NPoints(); l++ ) if( allPoints || PointSelected(l) )
          {
          Point3D pL=Point(l);
          if( !FaceT::PointInHalfSpace(normal,d,pL) ) return false;
          }
        
        // avoid co-linear faces
        Point3D p1=(Point3D)Point(j)-(Point3D)Point(i);
        Point3D p2=(Point3D)Point(k)-(Point3D)Point(i);
        float cosFi=p1*p2/(p1.Size()*p2.Size());
        if( fabs(cosFi)>=0.999 ) return false; // co-linear points
        
        return true;
        }










void ObjectData::CreateConvexHull( bool allPoints )
  {
  int npts=NPoints();
  for(;;)
    {
    // first of all remove any co-linear points
    bool noChange=true;
    int i,j,k;
    for( i=0; i<NPoints(); i++ ) if( allPoints || PointSelected(i) )
      for( j=0; j<NPoints(); j++ ) if( allPoints || PointSelected(j) )
        for( k=0; k<NPoints(); k++ ) if( allPoints || PointSelected(k) )
          {
          // avoid co-linear triplets
          if( i==k || i==j || j==k ) continue;
          Point3D p1=(Point3D)Point(j)-(Point3D)Point(i);
          Point3D p2=(Point3D)Point(k)-(Point3D)Point(i);
          float cosFi=p1*p2/(p1.Size()*p2.Size());
          if( cosFi<=-0.999 )
            {
            // i is middle point of the triplet
            noChange=false;
            DeletePoint(i);
            SetProgress(i*100/npts);
            goto NextPoint;
            }
          }
    NextPoint:
    if( noChange ) break;
    }
  Temp<bool> wasUsed(NPoints());
  Temp<bool> isUsed(NPoints());
  int i;
  for( i=0; i<NPoints(); i++ ) wasUsed[i]=isUsed[i]=false;
  // scan which points were used
  for( i=0; i<NFaces(); i++ )
    {
    const FaceT face(this,i);
    for( int v=0; v<face.N(); v++ ) wasUsed[face.GetPoint(v)]=true;
    }
  
  Edges openEdges; // list of edges that we need to close
  Edges closedEdges; // list of edges that we need to close
  // all open edges need to be closed
  // no edge can be opened twice
  // no edge can be closed twice
  
  // scan all possible triangles
  // maintain a list of "open" edges
    {
    // start by finding the smallest face
    // that could belong to the convex hull
    int minI=-1,minJ=-1,minK=-1;
    double minArea=1e50;
    // if there are some open edges, close them
    int i,j,k;
    for( i=0; i<NPoints(); i++ ) if( allPoints || PointSelected(i) )
      for( j=0; j<NPoints(); j++ ) if( allPoints || PointSelected(j) )
        for( k=0; k<NPoints(); k++ ) if( allPoints || PointSelected(k) )
          {
          if( !FaceCanBeInConvexHull(i,j,k,allPoints) ) continue;
          FaceT face(this);
          face.SetN(3);
          face.SetPoint(0,i);
          face.SetPoint(1,j);
          face.SetPoint(2,k);
          face.SetNormal(0,0);
          face.SetNormal(1,0);
          face.SetNormal(2,0);
          double area=face.CalculateArea();
          double perimeter=face.CalculatePerimeter();
          area=perimeter/area;
          if( minArea>area ) minArea=area,minI=i,minJ=j,minK=k;
          }
    
    if( minI<0 ) return; // no face
    // create a new face
    int index=NFaces();
    FaceT newFace(this,NewFace());
    newFace.SetN(3);
    newFace.SetPoint(0,minI);
    newFace.SetPoint(1,minJ);
    newFace.SetPoint(2,minK);
    newFace.SetNormal(0,0);
    newFace.SetNormal(1,0);
    newFace.SetNormal(2,0);
    FaceSelect(index);
    
    // open all new edges
    openEdges.Add(Edge(minI,minJ));
    openEdges.Add(Edge(minJ,minK));
    openEdges.Add(Edge(minK,minI));
    }
  
  
  while( openEdges.Size()>0 )
    {
    // while there are some open edges, close them
    int minI=-1,minJ=-1,minK=-1;
    int maxClose=-1; // select the one that will close most edges
    double minArea=1e50; // select the one wtih the smallest area
    int ei;
    for( ei=0; ei<openEdges.Size(); ei++ )
      {
      // try to create some triangle with edge (edge.b,edge.a)
      // this would close some edge
      Edge &edge=openEdges[ei];
      int i=edge.b;
      int j=edge.a;
      int k;
      
      // check if this edge had already been closed
      if( closedEdges.Find(Edge(i,j))>=0 )
        {
        LogF("Some closed edge is opened.");
        continue;
        }
      
      // try to create triangle i,j,k
      for( k=0; k<NPoints(); k++ ) if( allPoints || PointSelected(k) )
        {
        if( !FaceCanBeInConvexHull(i,j,k,allPoints) ) continue;
        
        // no edge can be closed twice
        if( closedEdges.Find(Edge(j,k))>=0 ) continue;
        if( closedEdges.Find(Edge(k,i))>=0 ) continue;
        FaceT face(this);
        face.SetN(3);
        face.SetPoint(0,i);
        face.SetPoint(1,j);
        face.SetPoint(2,k);
        // calculate perimeter/area (small means nice triangle)
        double area=face.CalculateArea();
        double perimeter=face.CalculatePerimeter();
        area=perimeter/area;
        // check how many edges we will close
        int nClose=0;
        if( openEdges.Find(Edge(j,i))>=0 ) nClose++;
        if( openEdges.Find(Edge(k,j))>=0 ) nClose++;
        if( openEdges.Find(Edge(i,k))>=0 ) nClose++;
        bool sureYes=false,sureNo=false;
        if( maxClose<nClose ) sureYes=true;
        else if( maxClose>nClose ) sureNo=true;
        if( !sureYes && !sureNo )
          {
          if( minArea>area ) sureYes=true;
          else if( minArea<area ) sureNo=true;
          }
        if( sureYes ) minArea=area,maxClose=nClose,minI=i,minJ=j,minK=k;
        }		
      }
    // create a new face
    if( minI<0 )
      { // no more faces - some open edges - that is strange
      break;
      }
    int index=NFaces();
    FaceT newFace(this,NewFace());
    newFace.SetN(3);
    newFace.SetPoint(0,minI);
    newFace.SetPoint(1,minJ);
    newFace.SetPoint(2,minK);
    newFace.SetNormal(0,0);
    newFace.SetNormal(1,0);
    newFace.SetNormal(2,0);
    FaceSelect(index);
    // close edges (minJ,minI), (minK,minJ), (minI,minK)
    // or open edges (minI,minJ), (minJ,minK), (minK,minI)
    int v1,v2,eIndex;
    v1=minI,v2=minJ;
    if( (eIndex=openEdges.Find(Edge(v2,v1)))>=0 )
      {
      closedEdges.Add(Edge(v1,v2));
      closedEdges.Add(Edge(v2,v1));
      openEdges.Delete(eIndex);
      }
    else
      {
      if( closedEdges.Find(Edge(v2,v1))<0 ) openEdges.Add(Edge(v1,v2));
      }
    
    v1=minJ,v2=minK;
    if( (eIndex=openEdges.Find(Edge(v2,v1)))>=0 )
      {
      closedEdges.Add(Edge(v1,v2));
      closedEdges.Add(Edge(v2,v1));
      openEdges.Delete(eIndex);
      }
    else
      {
      if( closedEdges.Find(Edge(v2,v1))<0 ) openEdges.Add(Edge(v1,v2));
      }
    
    v1=minK,v2=minI;
    if( (eIndex=openEdges.Find(Edge(v2,v1)))>=0 )
      {
      closedEdges.Add(Edge(v1,v2));
      closedEdges.Add(Edge(v2,v1));
      openEdges.Delete(eIndex);
      }
    else
      {
      if( closedEdges.Find(Edge(v2,v1))<0 ) openEdges.Add(Edge(v1,v2));
      }
    }
  // scan which points are used
  for( i=0; i<NFaces(); i++ )
    {
    const FaceT face(this,i);
    for( int v=0; v<face.N(); v++ ) isUsed[face.GetPoint(v)]=true;
    }
  // delete any unused points
  for( i=NPoints(); --i>=0; ) if( allPoints || PointSelected(i) )
    {
    //if( !isUsed[i] && wasUsed[i] ) DeletePoint(i);
    if( !isUsed[i] ) DeletePoint(i);
    }
  Squarize();
  //RecalcNormals(); // will be done is squarize
  }










void ObjectData::ComponentConvexHull()
  {
  // scan for components
  CreateComponents();
  int i;
  for( i=1; ; i++ )
    {
    WString name;
    name.Sprintf("Component%02d",i);
    if( !UseNamedSel(name) ) break; // all components done
    // split selection to convex components
    // delete any faces
    SelectionDeleteFaces();
    CreateConvexHull();
    }
  CreateComponents();
  ClearSelection();
  }










void ObjectData::BackfaceCull( const Vector3 &pin )
  {
  // check all faces and mark backfaced
  ClearSelection();
  // select all faces
  for( int f=0; f<NFaces(); f++ )
    {
    bool culled=true;
    const FaceT face(this,f);
    // calculate face normal
    Vector3 normal=face.CalculateNormal();
    // check backface for all vertices
    Vector3 direction=((Vector3)Point(face.GetPoint(0))-pin).Normalized();
    if( normal*direction>-0.05 ) culled=false;
    /**/
    for( int p=0; p<NAnimations() && culled; p++ )
      {
      AnimationPhase &phase=*GetAnimation(p);
      Vector3 normal=face.CalculateNormal(phase);
      Vector3 direction=(phase[face.GetPoint(0)]-pin).Normalized();
      if( normal*direction>-0.05 ) culled=false;
      }
    /**/
    FaceSelect(f,culled);
    }
  SelectPointsFromFaces();
  }










