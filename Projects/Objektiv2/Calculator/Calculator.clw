; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCalcInitDataDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Calculator.h"

ClassCount=4
Class1=CCalculatorApp
Class2=CCalculatorDlg

ResourceCount=5
Resource2=IDD_CALCULATOR_DIALOG
Resource3=IDD_CALCULATOR_DIALOG (English (U.S.))
Resource1=IDR_MAINFRAME
Class3=CCalcModifyDlg
Resource4=IDD_CALCULATOR_MODIFY
Class4=CCalcInitDataDlg
Resource5=IDD_CALCULATOR_INITVARS

[CLS:CCalculatorApp]
Type=0
HeaderFile=Calculator.h
ImplementationFile=Calculator.cpp
Filter=N

[CLS:CCalculatorDlg]
Type=0
HeaderFile=CalculatorDlg.h
ImplementationFile=CalculatorDlg.cpp
Filter=D
LastObject=CCalculatorDlg
BaseClass=CDialog
VirtualFilter=dWC



[DLG:IDD_CALCULATOR_DIALOG]
Type=1
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Class=CCalculatorDlg

[DLG:IDD_CALCULATOR_DIALOG (English (U.S.))]
Type=1
Class=CCalculatorDlg
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_PROGLIST,listbox,1352728835
Control4=IDC_STATIC,static,1342308352
Control5=IDC_NEW,button,1342242816
Control6=IDC_MODIFY,button,1342242816
Control7=IDC_DELETE,button,1342242816

[DLG:IDD_CALCULATOR_MODIFY]
Type=1
Class=CCalcModifyDlg
ControlCount=19
Control1=IDC_STATIC,static,1342308352
Control2=IDC_INPUTVARS,edit,1352732996
Control3=IDC_RELTOPIN,button,1342242819
Control4=IDC_WARPU,button,1342242819
Control5=IDC_WARPV,button,1342242819
Control6=IDC_STATIC,static,1342308352
Control7=IDC_INITPROGRAM,edit,1352732996
Control8=IDC_STATIC,static,1342308352
Control9=IDC_POINTPROGRAM,edit,1352732996
Control10=IDC_STATIC,static,1342308352
Control11=IDC_FACEPROGRAM,edit,1352732996
Control12=IDC_STATIC,static,1342308352
Control13=IDC_NAME,edit,1350631552
Control14=IDOK,button,1342242817
Control15=IDCANCEL,button,1342242816
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_SETVARS,button,1342242816
Control19=IDC_HELP,button,1342242816

[CLS:CCalcModifyDlg]
Type=0
HeaderFile=CalcModifyDlg.h
ImplementationFile=CalcModifyDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_BUTTON1
VirtualFilter=dWC

[DLG:IDD_CALCULATOR_INITVARS]
Type=1
Class=CCalcInitDataDlg
ControlCount=23
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342177287
Control4=IDC_STATIC,static,1342177287
Control5=IDC_STATIC,static,1342308864
Control6=IDC_STATIC,static,1342177287
Control7=IDC_SCROLLBAR,scrollbar,1342177281
Control8=IDC_CALCTEXT1,static,1073873408
Control9=IDC_CALCDATA1,edit,1082195969
Control10=IDC_CALCTEXT2,static,1073873408
Control11=IDC_CALCDATA2,edit,1082195969
Control12=IDC_CALCTEXT3,static,1073873408
Control13=IDC_CALCDATA3,edit,1082195969
Control14=IDC_CALCTEXT4,static,1073873408
Control15=IDC_CALCDATA4,edit,1082195969
Control16=IDC_CALCTEXT5,static,1073873408
Control17=IDC_CALCDATA5,edit,1082195969
Control18=IDC_CALCTEXT6,static,1073873408
Control19=IDC_CALCDATA6,edit,1082195969
Control20=IDC_CALCTEXT7,static,1073873408
Control21=IDC_CALCDATA7,edit,1082195969
Control22=IDC_CALCTEXT8,static,1073873408
Control23=IDC_CALCDATA8,edit,1082195969

[CLS:CCalcInitDataDlg]
Type=0
HeaderFile=CalcInitDataDlg.h
ImplementationFile=CalcInitDataDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CCalcInitDataDlg
VirtualFilter=dWC

