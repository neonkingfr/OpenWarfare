// CalculatorDlg.h : header file
//

#if !defined(AFX_CALCULATORDLG_H__9DA93C46_C069_11D4_B399_00C0DFAE7D0A__INCLUDED_)
#define AFX_CALCULATORDLG_H__9DA93C46_C069_11D4_B399_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "../comint.h"
#include "../LexTree.h"
class CICalcUse:public CICommand
  {
  char name[25];
  CString pgmname;
  bool selonly;
  bool alllods;
  bool allanims;
  HMATRIX transform;
  HMATRIX back;
  HVECTOR pin;
  char konec[25];
  public:	
    virtual int Execute(LODObject *lod);
    CICalcUse(const char *pgm, bool sel, bool lods, bool anims,HMATRIX t,HVECTOR pin);
  };

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// CCalculatorDlg dialog



class CCalculatorDlg : public CDialog
  {
  // Construction
  public:
    void ReadPrograms();
    CString outname;
    CCalculatorDlg(CWnd* pParent = NULL);	// standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CCalculatorDlg)
    enum 
      { IDD = IDD_CALCULATOR_DIALOG };
    CListBox	List;
    CString	list;
    BOOL	allanims;
    BOOL	alllods;
    BOOL	selonly;
    BOOL	camtransf;
    //}}AFX_DATA
    
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CCalculatorDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    HICON m_hIcon;
    
    // Generated message map functions
    //{{AFX_MSG(CCalculatorDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnNew();
    afx_msg void OnModify();
    afx_msg void OnDelete();
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALCULATORDLG_H__9DA93C46_C069_11D4_B399_00C0DFAE7D0A__INCLUDED_)
