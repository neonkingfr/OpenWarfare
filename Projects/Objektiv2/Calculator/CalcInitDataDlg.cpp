// CalcInitDataDlg.cpp : implementation file
//

#include "../stdafx.h"
#include "../resource.h"
#include "CalcInitDataDlg.h"


/////////////////////////////////////////////////////////////////////////////
// CCalcInitDataDlg dialog


CCalcInitDataDlg::CCalcInitDataDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CCalcInitDataDlg::IDD, pParent)
    {
    initials=NULL;
    //{{AFX_DATA_INIT(CCalcInitDataDlg)
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CCalcInitDataDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  for (int i=0;i<8;i++)
    {
    DDX_Control(pDX, IDC_CALCTEXT1+i*2, TextBox[i]);
    DDX_Control(pDX, IDC_CALCDATA1+i*2, InputBox[i]);
    }
  //{{AFX_DATA_MAP(CCalcInitDataDlg)
  DDX_Control(pDX, IDC_SCROLLBAR, scroll);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CCalcInitDataDlg, CDialog)
  //{{AFX_MSG_MAP(CCalcInitDataDlg)
  ON_WM_VSCROLL()
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CCalcInitDataDlg message handlers
      
      #include <strstream>
      using namespace std;
      
      void CCalcInitDataDlg::OnOK() 
        {
        ScanList();
        if (initials)
          {
          for (int i='A';i<='Z';i++) initials[i-'A']=tempvarspace.GetVarible(i);
          }
        CDialog::OnOK();
        }

//--------------------------------------------------

static int ReadVarible(istrstream& f)
  {
  int v=f.get();
  while (v<33 && v!=EOF) v=f.get();  
  v=toupper(v);
  if (v<'A' || v>'Z') return -1;
  if (f.get()!=':') return -1;
  return v;
  }

//--------------------------------------------------

int CCalcInitDataDlg::ListOfVars(int offset)
  {
  istrstream f((char *)varlist,strlen(varlist));
  int k=-offset;
  int lines=0;
  while (!f.eof())
    {
    if (k<0 || k>=8) f.ignore(99999,'\n');
    else
      {
      int var=ReadVarible(f);
      if (f.eof()) break;
      if (var==-1) 
        {		
        TextBox[k].ShowWindow(SW_SHOW);		
        TextBox[k].SetWindowText("---------------------------------------------------------------------");
        InputBox[k].ShowWindow(SW_HIDE);		
        f.ignore(99999,'\n');
        }
      else
        {
        CString text;
        int c=f.get();
        while (c!='\n' && c!=EOF) 
          {
          if (c<33) c=32;
          text+=(char)c;		  
          c=f.get();
          }
        TextBox[k].SetWindowText(text);
        TextBox[k].ShowWindow(SW_SHOW);
        InputBox[k].SetLong(var);
        InputBox[k].SetFloat(tempvarspace.GetVarible(var),3);
        InputBox[k].ShowWindow(SW_SHOW);
        }
      }
    k++;
    lines++;
    }
  if (k<0) k=0;
  while (k<8)
    {	
    TextBox[k].ShowWindow(SW_HIDE);
    InputBox[k].ShowWindow(SW_HIDE);
    InputBox[k].SetLong(-1);
    k++;
    }
  return lines;
  }

//--------------------------------------------------

float CCalcInitDataDlg::CEditEx::GetFloat()
  {
  CString s;
  GetWindowText(s);
  float f=0.0f;
  sscanf(s,"%f",&f);
  return f;
  }

//--------------------------------------------------

void CCalcInitDataDlg::CEditEx::SetFloat(float f, int precision)
  {
  CString s;
  CString form;
  form.Format("%%.%df",precision);
  s.Format(form,f);
  SetWindowText(s);
  };

//--------------------------------------------------

void CCalcInitDataDlg::ScanList()
  {
  for (int i=0;i<8;i++) if (InputBox[i].GetLong()!=-1)
    {
    int var=InputBox[i].GetLong();
    tempvarspace.SetVarible(var,InputBox[i].GetFloat());
    }
  }

//--------------------------------------------------

void CCalcInitDataDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
  {
  if (pScrollBar->GetDlgCtrlID()==IDC_SCROLLBAR)
    {
    int pos=pScrollBar->GetScrollPos();
    switch (nSBCode)
      {
      case SB_LINEDOWN: pos++;break;
      case SB_LINEUP: pos--;break;
      case SB_PAGEDOWN: pos+=6;break;
      case SB_PAGEUP: pos-=6;break;
      case SB_THUMBPOSITION:
      case SB_THUMBTRACK: pos=(int)nPos;break;
      }
    pScrollBar->SetScrollPos(pos,nSBCode!=SB_THUMBTRACK);
    pos=pScrollBar->GetScrollPos();
    ScanList();
    ListOfVars(pos);
    }
  else	
    CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
  }

//--------------------------------------------------

BOOL CCalcInitDataDlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  if (initials==NULL)
    {
    for (int i='A';i<='Z';i++) tempvarspace.SetVarible(i,0);
    }
  else
    {
    for (int i='A';i<='Z';i++) tempvarspace.SetVarible(i,initials[i-'A']);
    }
  int p=ListOfVars(0);	
  if (p==0) 
    {
    EndDialog(IDOK);
    return TRUE;
    }
  SCROLLINFO info;
  info.cbSize=sizeof(info);
  info.fMask=SIF_PAGE|SIF_POS|SIF_RANGE;
  info.nMax=p-1;
  info.nMin=0;
  info.nPage=8;
  info.nPos=0;
  scroll.SetScrollInfo(&info,FALSE);	
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

