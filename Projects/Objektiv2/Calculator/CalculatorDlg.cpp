// CalculatorDlg.cpp : implementation file
//

#include "../stdafx.h"
#include "../resource.h"
#include "CalculatorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCalculatorDlg dialog


CCalculatorDlg::CCalculatorDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CCalculatorDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CCalculatorDlg)
    list = _T("");
    allanims = FALSE;
    alllods = FALSE;
    selonly = TRUE;
    camtransf = FALSE;
    //}}AFX_DATA_INIT
    // Note that LoadIcon does not require a subsequent DestroyIcon in Win32
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    }

//--------------------------------------------------

void CCalculatorDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CCalculatorDlg)
  DDX_Control(pDX, IDC_PROGLIST, List);
  DDX_LBString(pDX, IDC_PROGLIST, list);
  DDX_Check(pDX, IDC_APPLYTOANIMS, allanims);
  DDX_Check(pDX, IDC_APPLYTOLODS, alllods);
  DDX_Check(pDX, IDC_SELONLY, selonly);
  DDX_Check(pDX, IDC_CAMTRANSF, camtransf);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CCalculatorDlg, CDialog)
  //{{AFX_MSG_MAP(CCalculatorDlg)
  ON_BN_CLICKED(IDC_NEW, OnNew)
    ON_BN_CLICKED(IDC_MODIFY, OnModify)
      ON_BN_CLICKED(IDC_DELETE, OnDelete)
        //}}AFX_MSG_MAP
        END_MESSAGE_MAP()
          
          /////////////////////////////////////////////////////////////////////////////
          // CCalculatorDlg message handlers
          
          BOOL CCalculatorDlg::OnInitDialog()
            {
            CDialog::OnInitDialog();
            
            // Set the icon for this dialog.  The framework does this automatically
            //  when the application's main window is not a dialog
            SetIcon(m_hIcon, TRUE);			// Set big icon
            SetIcon(m_hIcon, FALSE);		// Set small icon
            
            // TODO: Add extra initialization here
            
            ReadPrograms();
            
            return TRUE;  // return TRUE  unless you set the focus to a control
            }

//--------------------------------------------------

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

//DEL void CCalculatorDlg::OnPaint() 
//DEL {
//DEL 	if (IsIconic())
//DEL 	{
//DEL 		CPaintDC dc(this); // device context for painting
//DEL 
//DEL 		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
//DEL 
//DEL 		// Center icon in client rectangle
//DEL 		int cxIcon = GetSystemMetrics(SM_CXICON);
//DEL 		int cyIcon = GetSystemMetrics(SM_CYICON);
//DEL 		CRect rect;
//DEL 		GetClientRect(&rect);
//DEL 		int x = (rect.Width() - cxIcon + 1) / 2;
//DEL 		int y = (rect.Height() - cyIcon + 1) / 2;
//DEL 
//DEL 		// Draw the icon
//DEL 		dc.DrawIcon(x, y, m_hIcon);
//DEL 	}
//DEL 	else
//DEL 	{
//DEL 		CDialog::OnPaint();
//DEL 	}
//DEL }

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
//DEL HCURSOR CCalculatorDlg::OnQueryDragIcon()
//DEL {
//DEL 	return (HCURSOR) m_hIcon;
//DEL }

#include "CalcModifyDlg.h"

void CCalculatorDlg::ReadPrograms()
  {
  CString s,fnd;
  int cur=List.GetCurSel();
  if (cur>=0) List.GetText(cur,s);
  List.ResetContent();
  CFileFind ff;
  BOOL noend;
  if (ff.FindFile(CCalcModifyDlg::GetProgramFileName("*.clc")))  
    do
      {
      noend=ff.FindNextFile();	  
      fnd=ff.GetFileTitle();
      //	  fnd.Delete(fnd.GetLength()-4,4);
      List.AddString(fnd);
      }
  while(noend);  
  List.SelectString(-1,s);
  }

//--------------------------------------------------

void CCalculatorDlg::OnNew() 
  {
  CCalcModifyDlg dlg;
  dlg.clcname="";
  dlg.DoModal();
  ReadPrograms();
  }

//--------------------------------------------------

void CCalculatorDlg::OnModify() 
  {
  UpdateData();
  CCalcModifyDlg dlg;
  dlg.clcname=CCalcModifyDlg::GetProgramFileName(list);
  dlg.DoModal();
  ReadPrograms();  
  }

//--------------------------------------------------

void CCalculatorDlg::OnDelete() 
  {
  UpdateData();
  CString prompt;
  AfxFormatString1(prompt,IDS_PGMDELETE,list);
  if (AfxMessageBox(prompt,MB_YESNO)==IDNO) return;
  CString name=CCalcModifyDlg::GetProgramFileName(list);
  remove(name);
  ReadPrograms();
  }

//--------------------------------------------------

#include "CalcInitDataDlg.h"

void CCalculatorDlg::OnOK() 
  {
  UpdateData();
  CString name=CCalcModifyDlg::GetProgramFileName(list);
  CCalcInfo info;
  info.Load(name);
  CCalcInitDataDlg dlg;
  dlg.varlist=info.varlist;
  dlg.initials=info.initials;
  if (dlg.DoModal()==IDCANCEL) return;
  info.Save(name);
  CDialog::OnOK();
  outname=name;
  }

//--------------------------------------------------

CICalcUse::CICalcUse(const char *pgm, bool sel, bool lods, bool anims,HMATRIX t,HVECTOR pin):
pgmname(pgm),selonly(sel),alllods(lods),allanims(anims)
  {
  CopyMatice(back, t);
  recordable=false;
  CopyVektor(this->pin,pin);
  CopyVektor(back[3],mxVector3(0,0,0));
  strcpy(name,"ZDE JE CICALCUSE");
  strcpy(konec,"KONEC CICALCUSE");
  }

//--------------------------------------------------

int CICalcUse::Execute(LODObject *lod)
  {
  CCalcInfo info;
  if (info.Load(pgmname)==false) 
    {
    AfxMessageBox(IDS_LOADUNSUCCESFUL);
    return -1;
    }
  if (!info.reltopin) 
    {
    pin[0]=pin[1]=pin[2]=0;
    }
  Translace(transform,-pin[0],-pin[1],-pin[2]);
  SoucinMatic(transform,back,transform);
  InverzeMatice(transform,back);
  CLexTree tree;
  int i;
  float dummy;
  for (i='A';i<='Z';i++) tree.SetVarible(i,info.initials[i-'A']);
  if (info.initprog!="") 
    {
    if (tree.Parse(info.initprog)!=0 || tree.Calculate(dummy)==false)
      {
      AfxMessageBox(IDS_SYNTAXERROR);
      return -1;
      }	
    }
  for (int k=0;k<2;k++)
    {
    const char *s;
    if (k==0) s=info.pointprog;else s=info.faceprog;
    if (s[0])
      {
      if (tree.Parse(s)!=0)
        {
        AfxMessageBox(IDS_SYNTAXERROR);
        return -1;
        }	
      int from=0;int to=lod->NLevels();
      if (!alllods) to=(from=lod->ActiveLevel())+1;
      for (int ld=from;ld<to;ld++)
        {
        HVECTOR v;
        ObjectData *obj=lod->Level(ld);
        int nanims=(allanims && k==0)?obj->NAnimations():0;
        for (int an=-1;an<nanims;an++)
          {
          AnimationPhase *phs=(an==-1)?NULL:obj->GetAnimation(an);
          Selection *sel=(Selection *)(obj->GetSelection());
          if (k==0)
            {
            for (i=0;i<obj->NPoints();i++) if (!selonly || obj->PointSelected(i))
              {
              if (phs)
                {
                Vector3P& pos=(*phs)[i];
                TransformVector(transform,mxVector3(pos[0],pos[1],pos[2]),v);
                tree.xval=tree.lxval=v[0];
                tree.yval=tree.lyval=v[1];
                tree.zval=tree.lzval=v[2];
                tree.weightval=tree.lweightval=sel->PointWeight(i);
                if (tree.Calculate(dummy)==false) return -1;
                TransformVector(back,mxVector3(tree.lxval,tree.lyval,tree.lzval),v);
                pos[0]=v[0];
                pos[1]=v[1];
                pos[2]=v[2];
                sel->SetPointWeight(i,tree.lweightval);
                }
              else
                {
                PosT& pos=obj->Point(i);
                TransformVector(transform,mxVector3(pos[0],pos[1],pos[2]),v);
                tree.xval=tree.lxval=v[0];
                tree.yval=tree.lyval=v[1];
                tree.zval=tree.lzval=v[2];
                tree.weightval=tree.lweightval=sel->PointWeight(i);
                if (tree.Calculate(dummy)==false) return -1;
                TransformVector(back,mxVector3(tree.lxval,tree.lyval,tree.lzval),v);
                pos[0]=v[0];
                pos[1]=v[1];
                pos[2]=v[2];
                sel->SetPointWeight(i,tree.lweightval);
                }
              }
            }
          else
            {
            for (i=0;i<obj->NFaces();i++) if (!selonly || obj->FaceSelected(i))
              {
              FaceT fc(obj,i);
              for (int j=0;j<fc.N();j++)
                {
                PosT& pos=obj->Point(fc.GetPoint(j));
                TransformVector(transform,mxVector3(pos[0],pos[1],pos[2]),v);
                tree.tuval=tree.ltuval=fc.GetU(j);
                tree.tvval=tree.ltvval=fc.GetV(j);
                tree.xval=v[0];
                tree.yval=v[1];
                tree.zval=v[2];
                tree.weightval=tree.lweightval=sel->PointWeight(fc.GetPoint(j));
                if (tree.Calculate(dummy)==false) return -1;
                fc.SetU(j,tree.ltuval);
                fc.SetV(j,tree.ltvval);				
                }
              if(info.wrapu )
                { // perform cyclical correction of texture coordinate
                double uMax=-1e10;
                int j;
                for( j=0; j<fc.N(); j++ )
                  {
                  float u=fc.GetU(j);
                  u=u-floor(u); // clamp into interval <0,1)
                  if( uMax<u ) uMax=u;
                  fc.SetU(j,u);
                  }
                for( j=0; j<fc.N(); j++ )
                  {
                  float u=fc.GetU(j);
                  if( uMax-u>0.5 )
                    {
                    // texture should wrap through point 1.0
                    u=u+1.0;
                    }
                  fc.SetU(j,u);
                  }
                }
              if(  info.wrapv)
                { // perform cyclical correction of texture coordinate
                double vMax=-1e10;
                int j;
                for( j=0; j<fc.N(); j++ )
                  {
                  float v=fc.GetV(j);
                  v=v-floor(v); // clamp into interval <0,1)
                  if( vMax<v ) vMax=v;
                  fc.SetV(j,v);
                  }
                for( j=0; j<fc.N(); j++ )
                  {
                  float v=fc.GetV(j);
                  if( vMax-v>0.5 )
                    {
                    // texture should wrap through point 1.0
                    v=v+1.0;
                    }
                  fc.SetV(j,v);
                  }
                }
              }
            }
          }
        }
      }
    }
  return 0;
  }

//--------------------------------------------------

