#undef IDC_HELP
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Calculator.rc
//
#define IDS_PGMDELETE                   1
#define IDS_MISSINGNAME                 2
#define IDS_SYNTAXERROR                 3
#define IDS_UNABLETOSAVEPROGRAM         4
#define IDD_CALCULATOR_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDD_CALCULATOR_MODIFY           129
#define IDD_CALCULATOR_INITVARS         130
#define IDC_PROGLIST                    1000
#define IDC_NEW                         1001
#define IDC_INPUTVARS                   1002
#define IDC_MODIFY                      1002
#define IDC_RELTOPIN                    1003
#define IDC_DELETE                      1003
#define IDC_WARPU                       1004
#define IDC_WARPV                       1005
#define IDC_POINTPROGRAM                1006
#define IDC_FACEPROGRAM                 1007
#define IDC_INITPROGRAM                 1008
#define IDC_NAME                        1009
#define IDC_SCROLLBAR                   1011
#define IDC_CALCTEXT1                   1012
#define IDC_CALCDATA1                   1013
#define IDC_CALCTEXT2                   1014
#define IDC_SETVARS                     1014
#define IDC_CALCDATA2                   1015
#define IDC_HELP                        1015
#define IDC_CALCTEXT3                   1016
#define IDC_CALCDATA3                   1017
#define IDC_CALCTEXT4                   1018
#define IDC_CALCDATA4                   1019
#define IDC_CALCTEXT5                   1020
#define IDC_CALCDATA5                   1021
#define IDC_CALCTEXT6                   1022
#define IDC_CALCDATA6                   1023
#define IDC_CALCTEXT7                   1024
#define IDC_CALCDATA7                   1025
#define IDC_CALCTEXT8                   1026
#define IDC_CALCDATA8                   1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
