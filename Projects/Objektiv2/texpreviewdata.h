// TexPreviewData.h: interface for the CTexPreviewData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXPREVIEWDATA_H__A81DADC8_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_TEXPREVIEWDATA_H__A81DADC8_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

struct STexPreviewData
  {
  int texindex;
  unsigned int priority;
  CBitmap *bitmap;
  HBITMAP old;
  CDC *context;
  int xsize;
  int ysize;
  char *format;
  public:
    void DestroyBitmap();
    void LoadBitmap(const char *pathname,  CDC *pdc);
  };

//--------------------------------------------------

#define CTEX_CACHESIZE 32

class CTexPreviewData  
  {
  STexPreviewData cache[CTEX_CACHESIZE];
  char *refpath;
  unsigned int rlu_counter;
  public:
    void GetFullName(char *src, const char *filename);
    int GetFullNameSize(const char *name);
    CDC * GetBitmap(int index,const char *filename, CDC *pDC, char *desc,int descsize,bool load=true);
    void Flush();
    void SetRefPath(char *path);
    CTexPreviewData();
    virtual ~CTexPreviewData();
  };

//--------------------------------------------------

#endif // !defined(AFX_TEXPREVIEWDATA_H__A81DADC8_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
