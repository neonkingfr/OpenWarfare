// TexMapList.h: interface for the CTexMapList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXMAPLIST_H__7C4D0BB8_7B9F_470D_B1C0_B320D9816F64__INCLUDED_)
#define AFX_TEXMAPLIST_H__7C4D0BB8_7B9F_470D_B1C0_B320D9816F64__INCLUDED_

#include "TextureMapping.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define TEXMAXLOAD 5

class CTexMapList  
  {
  CTextureMapping *list[TEXMAXLOAD];
  int lru[TEXMAXLOAD];
  int curlru;
  public:
    void ForceFastTex(bool force);
    bool CheckHitTest(CPoint &pt);
    CTextureMapping * EnumTex(CTextureMapping *last);
    int GetCount();
    void Draw(CDC *dc, CTextureMapping *start, bool select);
    CTextureMapping * HitTest(CPoint &pt);
    void Delete(CTextureMapping *mp);
    CTextureMapping * Alloc();
    CTexMapList();
    virtual ~CTexMapList();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_TEXMAPLIST_H__7C4D0BB8_7B9F_470D_B1C0_B320D9816F64__INCLUDED_)
