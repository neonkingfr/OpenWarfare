#ifndef __GEPLANE__H__57454515030316541244554135464_ONDREJ_NOVAK_
#define __GEPLANE__H__57454515030316541244554135464_ONDREJ_NOVAK_


#include "geVector4.h"
#include "geMatrix4.h"

class geLine
  {
  public:
    geVector4 p; //is starting point
    geVector4 r; //is relative direction;
    
  public:  
    geLine(const geVector4 A,const geVector4 B, bool dir=false):p(A),r(dir?B:B-A) 
      {}
    geLine() 
      {}
    
    geVector4 GetPoint(float k) const
      {
      return p+r*k;
      }
    
    geVector4 operator() (float k) 
      {return p+r*k;}
    
    float Distance(const geVector4& v) const
      {
      float l2=~r;
      float t1=(r*(v-p))/l2;	  
      return v.Distance(GetPoint(t1));
      }
    
    float Distance(const geLine& v, geVector4 *pt1=NULL, geVector4 *pt2=NULL) const
      {
      geVector4 w(p-v.p);
      float a=~r;
      float b=r*v.r;
      float c=~v.r;
      float d=r*w;
      float e=v.r*w;
      float D=a*c-b*b;
      float sc,tc;
      if (D<0.000001f)
        {
        sc=0.0f;
        tc=(b>c?d/b:e/c);
        }
      else
        {
        sc=(b*e-c*d)/D;
        tc=(a*e-b*d)/D;
        }
      if (pt1) *pt1=GetPoint(sc);
      if (pt2) *pt2=v.GetPoint(tc);
      w=w+(r*sc)-(v.r*tc);
      return w.Module();
      }
    
    geLine& TransformC(const geMatrix4& mx)
      {
      mx.Transform3T(p,p);
      mx.Transform3(r,r);
      return *this;
      }
    geLine Transform(const geMatrix4& mx)const 
      {
      geLine z=*this;
      return z.TransformC(mx);
      }
    bool IsFinite() const//Returns true, if line is valid (not infinite)
      {return p.IsFinite() && r.IsFinite();}
    
    float MapPoint(const geVector4& pt) const 
      {
      float l2=~r;
      float t1=(r*(pt-p))/l2;	  
      return t1;
      }
  };










class gePlane2V
  {
  public:
    geVector4 p;  //is starting point
    geVector4 r1; //is relative direction 1
    geVector4 r2; //is relative direction 2
    
    gePlane2V() 
      {}
    gePlane2V(const geVector4 &A, const geVector4 &B, const geVector4 &C, bool dir=false):
    p(A),r1(dir?B:B-A),r2(dir?C:C-A) 
      {}
    gePlane2V(const geMatrix4& mm):
    p(mm.pos),r1(mm.x),r2(mm.y) 
      {}
    geVector4 operator() (float r,float s) 
      {return p+r1*r+r2*s;}
    
    //Calculatin intersection line with plane
    //result is a 3D vector in this order (r,s,k)
    //where r,s is coordinates at plane 
    //k is coordinate at line
    //if it is NAN or INFINITY, line and plane has no intersection
    //A + k * u = B + r * p + s * q
    //A - B     =     r * p + s * q - k * u
    //A - B     =    [r,s,k] * [p,q,-k]^T
    //(A - B)*[p,q,-k]^-1 = [r,s,k]
    geVector4 Intersection(const geLine &line) const
      {
      geMatrix4 m1;  
      m1.x=r1; m1.y=r2; m1.z=-line.r; //create 3D matrix
      m1=m1.Invert();				  //calculate inverse
      geVector4 v=(line.p-p);		  //calculate left side	
      m1.Transform3(v,v);			  //multiply with matrix to optian [r,s,k] vector
      return v;
      }
    operator geMatrix4() const
      {
      geMatrix4 m1;
      m1.x=r1;
      m1.y=r2;
      m1.z=r1^r2;
      m1.pos=p;
      return m1;
      }
    gePlane2V& TransformC(const geMatrix4 &me)
      {
      me.Transform3T(p,p);
      me.Transform3(r1,r1);
      me.Transform3(r2,r2);
      return *this;
      }
    gePlane2V Transform(const geMatrix4 &me)
      {
      geVector4 a/*=p*/;
      geVector4 b/*=a+r1*/;
      geVector4 c/*=a+r2*/;
      me.Transform3T(p,a);
      me.Transform3T(p+r1,b);
      me.Transform3T(p+r2,c);
      return gePlane2V(a,b,c,false);
      }
    bool IsFinite() const //Returns true, if plane is valid (not infinite)
      {return p.IsFinite() && r1.IsFinite() && r2.IsFinite();}
    
  };










class gePlaneGen
  {
  public:
    float a,b,c,d;
    
    //constructs undefined plane
    gePlaneGen() 
      {}
    //constructs plane defined by tree points
    gePlaneGen(const geVector4 &A,const geVector4 &B,const geVector4 &C)
      {
      geVector4 tmp=(B-A)^(C-A);
      a=tmp.x;
      b=tmp.y;
      c=tmp.z;
      d=-(a*A.x+b*A.y+c*A.z);
      }
    //constructs plane defined by normal and point
    gePlaneGen(const geVector4 &pt,const geVector4 &norm)
      {
      a=norm.x;b=norm.y;c=norm.z;
      d=-(a*pt.x+b*pt.y+c*pt.z);
      }
    //converts gePlane2V to gePlaneGen
    gePlaneGen(const gePlane2V &plane)
      {
      geVector4 tmp=plane.r1^plane.r2;
      a=tmp.x;
      b=tmp.y;
      c=tmp.z;
      d=-(a*plane.p.x+b*plane.p.y+c*plane.p.z);
      }
    //constructs plane from matrix (z is plane normal)
    gePlaneGen(const geMatrix4 &mx)
      {
      a=mx.z.x;
      b=mx.z.y;
      c=mx.z.z;
      d=-(a*mx.z.x+b*mx.z.y+c*mx.z.z);
      }
    //constructs plane upright to two planes (oriented)
    gePlaneGen(const gePlaneGen& p1, const gePlaneGen& p2)
      {
      a=p1.b*p2.c-p1.c*p2.b;
      b=p1.c*p2.a-p1.a*p2.c;
      c=p1.a*p2.b-p1.b*p2.a;
      d=0;
      }
    //constructs plane upright to two planes (oriented), point A must lay on plane
    gePlaneGen(const gePlaneGen& p1, const gePlaneGen& p2, const geVector4 &A)
      {
      a=p1.b*p2.c-p1.c*p2.b;
      b=p1.c*p2.a-p1.a*p2.c;
      c=p1.a*p2.b-p1.b*p2.a;
      d=-(a*A.x+b*A.y+c*A.z);
      }

    //construct plane upright to another, in direction of line (must lay on plane)
    gePlaneGen(const gePlaneGen& p1, const geLine &line)
      {
      a=p1.b*line.r.z-p1.c*line.r.y;
      b=p1.c*line.r.x-p1.a*line.r.z;
      c=p1.a*line.r.y-p1.b*line.r.x;
      d=-(a*line.p.x+b*line.p.y+c*line.p.z);      
      }
    
    //calculates distance of point from oriented plane
    //distance can be negative, if point lies in other half of space
    float Distance(const geVector4& other) const
      {
      return (d+a*other.x+b*other.y+c*other.z)/(float)sqrt(a*a+b*b+c*c);
      }
    //returns true, if point lies on right side of plane (where plane is oriented)
    //returns false, if point lies on opposite side
    bool Side(const geVector4& other) const
      {
      return (d+a*other.x+b*other.y+c*other.z)>=0;
      }
    
    float SideF(const geVector4& other) const
      {
      return (d+a*other.x+b*other.y+c*other.z);
      }

    //calculates intersection of line with plane, and result stores in k
    //returns nonzero, if line has intersection with the plane 
    //returns sign of cosinus alpha of line and normal vector of plane
    float Intersection(const geLine& other,float &k) const	 
      {      
      float t=a*other.r.x+b*other.r.y+c*other.r.z;
      float jm=a*other.p.x+b*other.p.y+c*other.p.z+d;
      if (jm-t==jm) return 0.0f;	  //test, zda je cislo moc male pro deleni
      k=-jm/t;
      return t;
      }
    //calculate intersection of 3 planes. Returns point (3D vector). If point is invalid, point does not exists
    geVector4 Intersection(const gePlaneGen& p1, const gePlaneGen& p2) const
      {
      geMatrix4 mx(true); //load planes to the matrix
      mx.a11=a;mx.a21=b;mx.a31=c;		   //a1*x+b1*y+c1*z=d1
      mx.a12=p1.a;mx.a22=p1.b;mx.a32=p1.c; //a2*x+b2*y+c2*z=d2
      mx.a13=p2.a;mx.a23=p2.b;mx.a33=p2.c; //a3*x+b3*y+c3*z=d3
      mx=mx.Invert();						
      geVector4 vx(-d,-p1.d,-p2.d);  //load right side
      mx.Transform3(vx,vx);		  //calculate X = D * M^-1
      return vx;
      }
    //calculate intersection of 2 planes. Returns line.
    geLine Intersection(const gePlaneGen& p1) const
      {
      gePlaneGen p2(*this,p1); //create upright plane
      geVector4 pt=Intersection(p1,p2); //find point intersects three planes
      return geLine(pt,geVector4(p2.a,p2.b,p2.c),true); //create line from point and vector
      }
    bool IsFinite() const //Returns true, if plane is valid (not infinite)
      {return _finite(a) && _finite(b) && _finite(c) && _finite(d);}
    //Returns point at the plane that construct upright line to plane with input point 
    //Maps point into plane
    geVector4 MapPoint(const geVector4 &pt) const
      {
      float t=a*a+b*b+c*c;
      float k=-(a*pt.x+b*pt.y+c*pt.z+d)/t;	  
      return geVector4(pt.x+a*k,pt.y+b*k,pt.z+c*k);
      }
    void MoveToPoint(const geVector4 &A)
      {
      d=-(a*A.x+b*A.y+c*A.z);
      }

    void Normalize()
      {
      float norm=1.0f/sqrt(a*a+b*b+c*c);
      a*=norm;
      b*=norm;
      c*=norm;
      d*=norm;
      }
  };










#if defined(AFX_CTARCHIVE_H__F66DAB8E_9620_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
inline ctArchive& operator>>=(ctArchive &arch, geLine& data)
  {
  arch.serial(&data,sizeof(data));
  return arch;
  }










inline ctArchive& operator>>=(ctArchive &arch, gePlaneGen& data)
  {
  arch.serial(&data,sizeof(data));
  return arch;
  }










inline ctArchive& operator>>=(ctArchive &arch, gePlane2V& data)
  {
  arch.serial(&data,sizeof(data));
  return arch;
  }










#endif
#endif

