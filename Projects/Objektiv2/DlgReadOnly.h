#if !defined(AFX_DLGREADONLY_H__A839F8BF_93E1_4924_9099_C9B73EC0E410__INCLUDED_)
#define AFX_DLGREADONLY_H__A839F8BF_93E1_4924_9099_C9B73EC0E410__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgReadOnly.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgReadOnly dialog

class CDlgReadOnly : public CDialog
{
// Construction
  const char *_fname;
  bool _saveAs;
public:
	CDlgReadOnly(CWnd* pParent = NULL);   // standard constructor

    static int CheckFileRO(const char *filename, bool enableSaveAs=false);

// Dialog Data
	//{{AFX_DATA(CDlgReadOnly)
	enum { IDD = IDD_READONLY };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgReadOnly)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgReadOnly)
	virtual BOOL OnInitDialog();
	afx_msg void OnRunexplorer();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGREADONLY_H__A839F8BF_93E1_4924_9099_C9B73EC0E410__INCLUDED_)
