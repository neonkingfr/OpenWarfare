#if !defined(AFX_MULTPINCULLDLG_H__0B432912_4AB0_4958_84C0_2D2D52D4C3F9__INCLUDED_)
#define AFX_MULTPINCULLDLG_H__0B432912_4AB0_4958_84C0_2D2D52D4C3F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MultPinCullDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMultPinCullDlg dialog

class CMultPinCullDlg : public CDialog
  {
  // Construction
  public:
    CMultPinCullDlg(CWnd* pParent = NULL);   // standard constructor
    ObjectData *obj;
    
    // Dialog Data
    //{{AFX_DATA(CMultPinCullDlg)
    enum 
      { IDD = IDD_MULTPINCULL };
    CComboBox	List;
    CString	selection;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMultPinCullDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CMultPinCullDlg)
    virtual BOOL OnInitDialog();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MULTPINCULLDLG_H__0B432912_4AB0_4958_84C0_2D2D52D4C3F9__INCLUDED_)
