// WinAnimator.h: interface for the CWinAnimator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WINANIMATOR_H__1D6BE6C0_4789_11D5_B39C_00C0DFAE7D0A__INCLUDED_)
#define AFX_WINANIMATOR_H__1D6BE6C0_4789_11D5_B39C_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define WINANIMTIM 12134

class CWinAnimator  
  {
  static float animtime;
  DWORD systime;
  int timid;
  CWnd &wnd;
  CRect defrect;
  CRect currect;
  public:
    void Run(DWORD tim);
    void RunOpen();
    CWinAnimator(CWnd &w):wnd(w) 
      {}	
    virtual ~CWinAnimator();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_WINANIMATOR_H__1D6BE6C0_4789_11D5_B39C_00C0DFAE7D0A__INCLUDED_)
