#if !defined(AFX_GIZMOMAPDLG_H__27EFD124_AFD4_4625_8E9F_EEE97265DBA7__INCLUDED_)
#define AFX_GIZMOMAPDLG_H__27EFD124_AFD4_4625_8E9F_EEE97265DBA7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GizmoMapDlg.h : header file
//

#include "GizmoTexWnd.h"
#include "comint.h"

/////////////////////////////////////////////////////////////////////////////
// CGizmoMapDlg dialog
class CGizmoMapDlg : public CDialog
  {
  // Construction
  static WINDOWPLACEMENT wp;
  CSize minsz;
  CSize cursz;
  public:
    static int gizmotype;
    void SaveValues();
    float GetDlgItemFloat(int id);
    void SetDlgItemFloat(int id, float value, int decimal);
    CGizmoMapDlg(CWnd* pParent = NULL);   // standard constructor
    
    struct GizmoMapInfo
      {
      float tu1, tu2,tv1,tv2;
      int mode;
      bool wrapu;
      bool wrapv;
      bool swapuv;
      char texname[256]; //texture name is limited
      char nula;
      };
    static GizmoMapInfo nfo;	
    // Dialog Data
    //{{AFX_DATA(CGizmoMapDlg)
    enum 
      { IDD = IDD_GIZMOMAPPING };
    CComboBox	wMapMode;
    CGizmoTexWnd wMapPrv;
    BOOL	SwapUV;
    CString	TexName;
    BOOL	wrapu;
    BOOL	wrapv;
    int		MapMode;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CGizmoMapDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CGizmoMapDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnMappreview();
    afx_msg void OnKillfocusTexCoords();
    afx_msg void OnMirrortu();
    afx_msg void OnMirrortv();
    afx_msg void OnSetTexture();
    virtual void OnOK();
    afx_msg void OnPreview();
    afx_msg void OnGizmoResetmap();
    afx_msg void OnSurfacesResetgismo();
    afx_msg void OnKillfocusTexname();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnSelchangeMaptype();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

class CIGizmoMap: public CICommand
  {
  struct PreCalcInfo
    {
    bool used;
    float u;
    float v;
    };
  HMATRIX gizmo;
  CGizmoMapDlg::GizmoMapInfo nfo;
  public:
    CIGizmoMap(istream &str);
    CIGizmoMap(LPHMATRIX gt, CGizmoMapDlg ::GizmoMapInfo &gi);
    int Execute(LODObject *lod);
    void Save(ostream &str);
    int Tag() 
      {return CITAG_GIZMOMAP;}
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GIZMOMAPDLG_H__27EFD124_AFD4_4625_8E9F_EEE97265DBA7__INCLUDED_)
