// VyberLodDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "VyberLodDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVyberLodDlg dialog


CVyberLodDlg::CVyberLodDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CVyberLodDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CVyberLodDlg)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CVyberLodDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CVyberLodDlg)
  DDX_Control(pDX, IDC_LOD, wLod);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CVyberLodDlg, CDialog)
  //{{AFX_MSG_MAP(CVyberLodDlg)
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CVyberLodDlg message handlers
    
    BOOL CVyberLodDlg::OnInitDialog() 
      {
      CDialog::OnInitDialog();
      
      int nlod=lodobj->NLevels();
      for (int i=0;i<nlod;i++)
        {
        float p=lodobj->Resolution(i);
        int pos=wLod.AddString(LODResolToName(p));
        wLod.SetItemData(pos,i);
        }  	
      return TRUE;  // return TRUE unless you set the focus to a control
      // EXCEPTION: OCX Property Pages should return FALSE
      }

//--------------------------------------------------

void CVyberLodDlg::OnOK() 
  {
  int p=wLod.GetCurSel();
  if (p<0) return;
  p=wLod.GetItemData(p);
  this->sellod=this->lodobj->Resolution(p);	
  CDialog::OnOK();
  }

//--------------------------------------------------

