#pragma once


#include <projects/ObjektivLib/ObjToolMapping.h>

// UVEditorView


struct UVEditorViewObject
{
  int faceIndex; ///<index of face that this object belongs to
  ObjUVSet_Item uv[MAX_DATA_POLY]; ///<UV coordinates
  int n;  ///<count of vertices (3 or 4)
  unsigned long selectedVertices; ///< Each vertice has a single BIT
  COLORREF color; ///<Color of object, use 0 as default color

  void SelectVertex(int idx, bool select)
  {
    if (select)
      selectedVertices|=1<<idx;
    else
      selectedVertices&=~(1<<idx);
  }

  bool IsSelectedVertex(int idx) const
  {
    return (selectedVertices & (1<<idx))!=0;
  }

  bool IsAllVerticesSelected() const
  {
    return (selectedVertices) == ((1<<n)-1);
  }

  void SelectAllVertices()
  {
    selectedVertices = ((1<<n)-1);
  }

  bool IsUnmapped()
  {
    if (uv[0].u==0 && uv[0].v==0 &&
      uv[1].u==0 && uv[1].v==0 &&
      uv[2].u==0 && uv[2].v==0
      )
      if (n==3) return true;
      else return uv[3].u==0 && uv[3].v==0;
    return false;
  }


  UVEditorViewObject():selectedVertices(0) {}
  UVEditorViewObject(const FaceT &fc, COLORREF color=0):selectedVertices(0)
  {
    faceIndex=fc.GetFaceIndex();
    n=fc.N();
    for (int i=0;i<n;i++)
      uv[i]=fc.GetUV(i);
    this->color=color;
  }
};

class IUVEditorViewEvent
{
public:
  virtual ~IUVEditorViewEvent()=0 {}
  virtual void SelectionChanged(const Array<UVEditorViewObject> &newSelection)=0;
  virtual void TransformedSelection(const Array<UVEditorViewObject> &data)=0;
  virtual void MovedPoints(const Array<UVEditorViewObject> &data, const Array<int> movedPts)=0;
};



TypeIsSimpleZeroed(UVEditorViewObject);

class UVEditorView : public CWnd
{
	DECLARE_DYNAMIC(UVEditorView)

  AutoArray<UVEditorViewObject> _uvObjects;
  AutoArray<UVEditorViewObject> _backgroundUV;

  CBitmap _backbmpbitmap;    
  void *_previewCache;  

  void ClearCache();
  void DrawPreviewPolygon(CDC *dc, const POINT *pts, int count);

public:

  struct FSnapPoint: public FPoint2D
  {
    FSnapPoint() {}
    FSnapPoint(float x, float y):FPoint2D(x,y) {}
    FSnapPoint(int x, int y=0):FPoint2D((float)x,(float)y) {}

    int CompareWith(const FSnapPoint &other) const
    {
      int res=(x>other.x)-(x<other.x);
      if (res==0)
      {
        return (y>other.y)-(y<other.y);
      }
      return res;
    }
    
    bool operator==(const FSnapPoint &other) const {return CompareWith(other)==0;}
    bool operator>=(const FSnapPoint &other) const {return CompareWith(other)>=0;}
    bool operator<=(const FSnapPoint &other) const {return CompareWith(other)<=0;}
    bool operator!=(const FSnapPoint &other) const {return CompareWith(other)!=0;}
    bool operator>(const FSnapPoint &other) const {return CompareWith(other)>0;}
    bool operator<(const FSnapPoint &other) const {return CompareWith(other)<0;}
  
  };

 private:
  FRect _uvbounding;
  CSize _bmpsize;
  FPoint2D _origin;
  CPoint _scrollPage;
  float _zoom;
  float _scrtrackbeg;
  bool _zoomMode;
  bool _whiteMode;
  
  FRect _selArea;
  FTransform _transformArea;
  CRect _dragArea;
  CRect _clearArea;
  AutoArray<int,MemAllocLocal<int,5> > _draggedPts;
  BTree<FSnapPoint> _snapPoints;
  FPoint2D _snapPt;
  bool _autoSnap;
    

  COLORREF _linecolor;
  COLORREF _selectcolor;
  COLORREF _backUvColor;

  void DrawUVObject(CDC &dc,UVEditorViewObject &obj, const FTransform &trns, bool colorize,const CRect &dirty);

  enum DragMode
  {
    DragNone, DragRect,DragTopLeft,DragTop,DragRightTop,DragRight,DragRightBottom,DragBottom,DragBottomLeft,DragLeft,DragMove,DragRotate,
    DragVertex, DragPan, DragZoom, DragO2Zoom, DragMoveSnap, DragVertexSnap
  };

  DragMode _dragMode;

  IUVEditorViewEvent *_owner;
public:
	UVEditorView();
	virtual ~UVEditorView();

  bool Create(CWnd *parent, IUVEditorViewEvent *notify, unsigned long id, unsigned long style, unsigned long exstyle);

  void SetNullTexture();

  bool LoadTexture(BITMAPINFO *bitmap, const char *bitmapdata);
  bool LoadDefaultTexture();

  void LoadUVs(const Array<UVEditorViewObject> &uvs, bool keepsel=true);
  void LoadBgrnUVs(const Array<UVEditorViewObject> &uvs)
  {
    _backgroundUV=uvs;
    Invalidate();
    _snapPoints.Clear();
  }


  FRect CalculateUVBounding(bool selected);
  void SelectRectangle(const CRect &rc, bool clearSel, bool deselect);
  void BreakSelection();
  void SetZoom(float zoom, CPoint staticPt);
  void FitTextureToWindow();
  void FitUVSToWindow(bool selected);

  void DrawToDC(CDC &dc, const CRect &clipRect);
  void SetZoomMode(bool zoomMode);
  bool IsZoomMode() {return _zoomMode;}
  const AutoArray<UVEditorViewObject> &GetUVObjects() const {return _uvObjects;}
  const CSize &GetCurTextureDimensions() const { return _bmpsize;}
  void TransformXYToUV(Array<UVEditorViewObject> objects);
  void Snap();
  void CalculateUnwrap(ObjectData &byobject, int mode);
  void SelectAll();  
  void SetWhiteMode(bool enable);
  bool IsWhiteMode() const {return _whiteMode;}
  void TransformSelection(const FTransform &transform, bool centerSel, bool preview);

  void SelectionMirrorHorz();
  void SelectionMirrorVert();

protected:
  
  FTransform CalcFloatToPixelTransf();
  FTransform CalcPixelToFloatTransf();
  static FTransform CalculateDifferenceMatrix(const FPoint2D &src1, const FPoint2D &src2, const FPoint2D &src3, const FPoint2D &res1, const FPoint2D &res2, const FPoint2D &res3);
  static CPoint TransformToInt(const FTransform &trns, const FPoint2D &pt);
  static CRect TransformToInt(const FTransform &trns, const FRect &pt);

  DragMode DragHitTest(const CPoint &mouse, CRect *area);
  template<class Allocator>
  bool HitTestToPoint(const CPoint &mouse, AutoArray<int,Allocator> &indexList);
  void DrawUVPreview(CDC *dc);

  void EndEdit(UINT nFlags, CPoint point);
  void ProcessTransform();

  int HitTestToPoint(const CPoint &mouse);
  void ProcessPtTransform();

  void DrawPtDragPreview(CDC *dc);
  void ScrollOriginTo(FPoint2D oldpos, FPoint2D newpos);

  void CenterRect(FRect rc);

  void CalculateZoomRect(FRect rc);

  void CorrectSelArea();

  void UpdateSnapPoints();
  FPoint2D FindNearestSnapPoint(const FPoint2D &pt, float distance, bool *found=0);

	DECLARE_MESSAGE_MAP()
public:


  void EnableAutoSnap(bool enable) {_autoSnap=enable;}
  bool IsAutoSnapEnabled() const {return _autoSnap;}
protected:
  afx_msg void OnPaint();
  afx_msg void OnDestroy();
  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
  afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
  afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
};


