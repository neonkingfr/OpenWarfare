// AnimListBar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "AnimListBar.h"
#include "ComInt.h"
#include "Objektiv2Doc.h"
#include "MainFrm.h"
#include "SoftenDlg.h"
//#include "DlgReadOnly.h"
#include "ROCheck.h"
#include <malloc.h>
#include <projects/ObjektivLib/ObjToolAnimation.h>
#include <projects/ObjektivLib/ObjToolClipboard.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAnimListBar dialog


CAnimListBar::CAnimListBar()
: CDialogBar()
  {
  //{{AFX_DATA_INIT(CAnimListBar)
  // NOTE: the ClassWizard will add member initialization here
  //}}AFX_DATA_INIT
  //{{AFX_DATA_MAP(CAnimListBar)
  // NOTE: the ClassWizard will add DDX and DDV calls here
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CAnimListBar, CDialogBar)
  //{{AFX_MSG_MAP(CAnimListBar)
  ON_WM_SIZE()
  ON_WM_DESTROY()
  ON_WM_CONTEXTMENU()
  ON_COMMAND(ID_ANIM_NEW, OnAnimNew)
  ON_COMMAND(ID_ANIM_DELETE, OnAnimDelete)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST, OnItemchangedList)
  ON_NOTIFY(NM_KILLFOCUS, IDC_LIST, OnKillfocusList)
  ON_COMMAND(ID_ANIM_RENAME, OnAnimRename)
  ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST, OnEndlabeleditList)
  ON_COMMAND_EX(ID_ANIM_REDEFINE, OnAnimRedefine)
  ON_COMMAND(ID_ANIM_COPY, OnAnimCopy)
  ON_COMMAND_EX(ID_ANIM_PASTE, OnAnimPaste)
  ON_COMMAND(ID_ANIM_IMPORTMULTIPLE, OnAnimImport)
  ON_COMMAND_EX(ID_ANIM_AUTOTIME, OnAnimAutotime)
  ON_COMMAND_EX(ID_ANIM_HALFRATE, OnAnimHalfrate)
  ON_COMMAND(ID_ANIM_FROMMATRICES, OnAnimFrommatrices)
  ON_COMMAND(ID_ANIM_EXPORTMATRICES, OnAnimExportmatrices)
  ON_COMMAND(ID_ANIM_SOFTSELECT, OnAnimSoftselect)
  ON_COMMAND_EX(ID_ANIM_NEUTRALIZESTEP, OnAnimNeutralizestep)
  ON_COMMAND_EX(ID_ANIM_XNORMCENTER, OnAnimXnormcenter)
  ON_COMMAND_EX(ID_ANIM_ZNORMCENTER, OnAnimZnormcenter)
  ON_COMMAND_EX(ID_ANIM_MIRROR, OnAnimMirror)
  ON_COMMAND_EX(ID_ANIM_INSERTSTEP, OnAnimInsertstep)
  ON_COMMAND(ID_ANIM_FROMMATRICESLOD, OnAnimFrommatriceslod)
  ON_COMMAND(ID_ANIM_LOCKSELECTED, OnAnimLockselected)
  ON_COMMAND(ID_ANIM_UNLOCKALL, OnAnimUnlockall)
  ON_COMMAND_EX(ID_ANIM_PASTEOVERSEL, OnAnimPaste)
  ON_COMMAND_EX(ID_ANIM_AUTOTIME10, OnAnimAutotime)
  ON_COMMAND_EX(ID_ANIM_NEUTRALIZEXSTEP, OnAnimNeutralizestep)
  ON_COMMAND(ID_ANIM_TIMERANGE, OnAnimTimerange)
  ON_COMMAND_RANGE(100,1000,OnPluginPopupCommand)
  //}}AFX_MSG_MAP
  ON_COMMAND(ID_ANIM_DELETEALLANIMATIONS, OnAnimDeleteallanimations)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAnimListBar message handlers
#ifdef FULLVER
void CAnimListBar::Create(CWnd *parent, int menuid)
  {
  CDialogBar::Create(parent,IDD,CBRS_RIGHT,menuid);
  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  EnableDocking(CBRS_ALIGN_ANY);
  CString top;top.LoadString(IDS_ANIMLISTTITLE);
  SetWindowText(top);
  notify=parent;
  list.Attach(::GetDlgItem(m_hWnd,IDC_LIST));
  list.SetImageList(&shrilist,LVSIL_SMALL);
  list.InsertColumn(0,WString(IDS_TIMETITLE),LVCFMT_LEFT,100,0);
  list.InsertColumn(1,WString(IDS_PHASETITLE),LVCFMT_RIGHT,50,1);  
  LoadBarSize(*this,160,0);
  defpath.SetDirectory(config->GetString(IDS_CFGANIMFOLDER));
  //defpath.SetFilename(".");
  }

//--------------------------------------------------

#else
void CAnimListBar::Create(CWnd *parent, int menuid)
  {
  }

//--------------------------------------------------

#endif

void CAnimListBar::OnSize(UINT nType, int cx, int cy) 
  {
  CDialogBar::OnSize(nType, cx, cy);
  if ((HWND)list) list.MoveWindow(5,5,cx-10,cy-10);
  }

//--------------------------------------------------

void CAnimListBar::OnDestroy() 
  {
  list.Detach();
  SaveBarSize(*this);
  CDialogBar::OnDestroy();
  }

//--------------------------------------------------

void CAnimListBar::UpdateListOfAnims(ObjectData *odata, float curlod)
  {
  obj=odata;
  int cnt=obj->NAnimations();
  list.DeleteAllItems();
  WString str;
  lock.CheckNewAnims(curlod,cnt);
  int handle=lock.FindLod(curlod);
  for (int i=0;i<cnt;i++)
    {
    LVITEM it;
    str.Format("%.6f",odata->GetAnimation(i)->GetTime());
    it.mask=LVIF_TEXT |LVIF_IMAGE |LVIF_PARAM;
    it.iItem=i;
    it.iSubItem=0;
    it.iImage=lock.GetWeight(handle,i)?ILST_ANIMATIONLOCK:ILST_ANIMATION;
    it.lParam=i;
    it.pszText=(char *)((const char *)str);
    int idx=list.InsertItem(&it);
    str.Format(i==doc->frozenanim?"[%d]":"%d",i);
    list.SetItemText(idx,1,str);	
    }
  this->curlod=curlod;
  }

//--------------------------------------------------

int CAnimListBar::GetActiveIndex(int search)
  {
  int i=list.GetNextItem(-1,LVNI_FOCUSED);    
  if (i==-1) return -1;
  LVITEM it;
  it.mask=LVIF_PARAM;
  it.iItem=i;
  it.iSubItem=0;
  list.GetItem(&it);
  return it.lParam;
  }

//--------------------------------------------------

class CIUseAnimation:public CICommand
  {
  int animindex;
  public:
    CIUseAnimation(int idx): animindex(idx) 
      {recordable=false;marksave=false;};
    virtual int Execute(LODObject *obj)
      {
      ObjectData *odata=obj->Active();
      Assert(odata);
      if (odata)
        odata->UseAnimation(animindex);
      return 0;
      }
    virtual int Tag() const 
      {return CITAG_USEANIM;}
    virtual bool CanRemove(const CICommand *prev) 
      {
      return prev->Tag()==CITAG_USEANIM;
      }

  };

//--------------------------------------------------

void CAnimListBar::OnContextMenu(CWnd* pWnd, CPoint point) 
  {
  int idx=GetActiveIndex(LVNI_SELECTED);
  bool itemclick=((HWND)*pWnd==(HWND)list && idx!=-1);
  CMenu popup;
  ::LoadPopupMenu(IDR_ANIMMENU_POPUP,popup);
  if (!itemclick)
    {
    static int idgray[]=
      {ID_ANIM_DELETE,ID_ANIM_RENAME,ID_ANIM_REDEFINE,ID_ANIM_IMPORTMULTIPLE,
      ID_ANIM_COPY,ID_ANIM_PASTE,ID_ANIM_PASTEOVERSEL,ID_ANIM_LOCKSELECTED,ID_ANIM_TIMERANGE,0};
    for (int i=0;idgray[i];i++)
      popup.EnableMenuItem(idgray[i],MF_BYCOMMAND|MF_GRAYED);
    }
  if (obj->NAnimations()<2)
    {
    static int idgray[]=
      {ID_ANIM_SOFTSELECT,ID_ANIM_NEUTRALIZESTEP,ID_ANIM_NEUTRALIZEXSTEP,
      ID_ANIM_INSERTSTEP,ID_ANIM_ZNORMCENTER,
      ID_ANIM_XNORMCENTER,ID_ANIM_MIRROR,
      ID_ANIM_AUTOTIME,ID_ANIM_AUTOTIME10,ID_ANIM_HALFRATE,
      0};
    for (int i=0;idgray[i];i++)
      popup.EnableMenuItem(idgray[i],MF_BYCOMMAND|MF_GRAYED);
    }
  if (obj->NAnimations()==0)
  {
    popup.EnableMenuItem(ID_ANIM_EXPORTMATRICES,MF_BYCOMMAND|MF_GRAYED);
    popup.EnableMenuItem(ID_ANIM_DELETEALLANIMATIONS,MF_BYCOMMAND|MF_GRAYED);
  }
  if (!IsClipboardFormatAvailable(theApp.ClipFormat1))
    {
    popup.EnableMenuItem(ID_ANIM_PASTE,MF_BYCOMMAND|MF_GRAYED);
    popup.EnableMenuItem(ID_ANIM_PASTEOVERSEL,MF_BYCOMMAND|MF_GRAYED);
    }
  theApp.PluginPopupHook(IPluginGeneralPluginSide::PMAnimationList,popup,*this);
  popup.TrackPopupMenu(TPM_LEFTALIGN,point.x,point.y,this,NULL);	
  SetActiveIndex(lastindex,false);
  }

//--------------------------------------------------

int CIAnimSimple::Execute(LODObject *lod)
  {
  WString text;
  text.Format("command: %d value %.3f index %d, obj %X",idcommand,value,index,lod->Active());
  frame->SetStatus(text);
  frame->UpdateWindow();
  ObjectData *obj=lod->Active();
  switch (idcommand)
    {
    case ID_ANIM_NEW:
      {
      AnimationPhase anim(obj);
      float maxTime=-0.1;
      for( int p=0; p<obj->NAnimations(); p++ )
        {
        AnimationPhase *anim=obj->GetAnimation(p);
        if( maxTime<anim->GetTime() ) maxTime=anim->GetTime();
        }
      maxTime+=0.1;
      if (maxTime<0) maxTime=0;
      anim.SetTime(maxTime);
      text.Sprintf("%.6f",anim.GetTime());
      obj->RedefineAnimation(anim);
      return !obj->AddAnimation(anim);
      }
    case ID_ANIM_DELETE:
      ASSERT(obj!=NULL);
      ASSERT(obj->GetAnimation(index));
      return !obj->DeleteAnimation(obj->GetAnimation(index)->GetTime());		   
    case ID_ANIM_RENAME:
      {
      int ret=!obj->RenameAnimation(obj->GetAnimation(index)->GetTime(),value);
      obj->SortAnimations();
      return ret;
      }
    case ID_ANIM_AUTOTIME:
      obj->GetTool<ObjToolAnimation>().AutoTimeAnimations(false);return 0;
    case ID_ANIM_AUTOTIME10:
      obj->GetTool<ObjToolAnimation>().AutoTimeAnimations(true);return 0;
    case ID_ANIM_HALFRATE:
      obj->UseAnimation(-1);
      obj->GetTool<ObjToolAnimation>().HalfRateAnimation();return 0;
    case ID_ANIM_DELETEALLANIMATIONS:
      obj->UseAnimation(0);
      obj->UseAnimation(-1);
      obj->DeleteAllAnimations();
      doc->UpdateAllViews(NULL);
      return 0;
    case ID_ANIM_NEUTRALIZESTEP:
      {
      float length=obj->GetTool<ObjToolAnimation>().StepLength(CAnimListBar::GetStepSelection(obj));
      obj->GetTool<ObjToolAnimation>().RemoveZStep(length);
      WString step;
      step.Sprintf("%f",length);
      obj->GetTool<ObjToolAnimation>().SetNamedProp("Step",step);		
      return 0;
      }
    case ID_ANIM_NEUTRALIZEXSTEP:
      {
      float length=obj->GetTool<ObjToolAnimation>().XStepLength(CAnimListBar::GetStepSelection(obj));
      obj->GetTool<ObjToolAnimation>().RemoveXStep(length);
      WString step;
      step.Sprintf("%f",length);
      obj->SetNamedProp("XStep",step);
      return 0;
      }
    case ID_ANIM_INSERTSTEP:
      {
      const char *step=obj->GetNamedProp("step");
      const char *xStep=obj->GetNamedProp("xStep");
      float length=0;
      float xLength=0;
      if( step ) length=atof(step);
      if( xStep ) xLength=atof(xStep);
      obj->GetTool<ObjToolAnimation>().RemoveZStep(-length);
      obj->GetTool<ObjToolAnimation>().RemoveXStep(-xLength);
      obj->DeleteNamedProp("Step");
      obj->DeleteNamedProp("xStep");
      return 0;
      }
    case ID_ANIM_ZNORMCENTER:
      obj->GetTool<ObjToolAnimation>().NormalizeCenter(false,true,CAnimListBar::GetStepSelection(obj));
      return 0;
    case ID_ANIM_XNORMCENTER:
      obj->GetTool<ObjToolAnimation>().NormalizeCenter(true,false,CAnimListBar::GetStepSelection(obj));
      return 0;
    case ID_ANIM_MIRROR:
      obj->GetTool<ObjToolAnimation>().MirrorAnimation();
      obj->SortAnimations();
      return 0;
    }
  return -1;
  }


//--------------------------------------------------

class CISoftenSelected:public CICommand
  {
  float xval,yval,zval,timerange;
  bool actframe,interpolate,loop;
  public:
    CISoftenSelected(CSoftenDlg& dlg):
        xval(dlg.xval),yval(dlg.yval),zval(dlg.zval),
          timerange(dlg.timerange),actframe(dlg.actframe==TRUE),
          interpolate(dlg.interpolate==TRUE),loop(dlg.loop==TRUE) 
          {}
        CISoftenSelected(istream& str)
          {
          datard(str,xval);
          datard(str,yval);
          datard(str,zval);
          datard(str,timerange);
          datard(str,actframe);
          datard(str,interpolate);
          datard(str,loop);
          }
        virtual void Save(ostream &str)
          {
          datawr(str,xval);
          datawr(str,yval);
          datawr(str,zval);
          datawr(str,timerange);
          datawr(str,actframe);
          datawr(str,interpolate);
          datawr(str,loop);
          }
        virtual int Tag() const 
          {return CITAG_SOFTENSELCTED;}
        virtual int Execute(LODObject *obj)
          {
          obj->Active()->GetTool<ObjToolAnimation>().SoftenAnimation(xval,yval,zval,actframe,timerange,interpolate,loop);
          return 0;	  
          }  
  };

//--------------------------------------------------

CICommand *CISoftenSelected_Create(istream &str)
  {
  return new CISoftenSelected(str);
  }

//--------------------------------------------------

void CAnimListBar::OnAnimNew() 
  {
  comint.Begin(WString(IDS_NEWANIMATION));
  comint.Run(new CIAnimSimple(ID_ANIM_NEW));  
  this->UpdateListOfAnims(obj);
  }

//--------------------------------------------------

void CAnimListBar::OnAnimDelete() 
  {
  comint.Begin(WString(IDS_DELANIMATION));    
  int idx=list.GetNextItem(-1,LVNI_SELECTED);  
  int aa=0;
  while (idx!=-1)
    {
    LVITEM it;
    it.mask=LVIF_PARAM;
    it.iItem=idx;
    it.iSubItem=0;
    list.GetItem(&it);    
    comint.Run(new CIAnimSimple(ID_ANIM_DELETE,it.lParam-aa));
    lock.DeleteFrame(curlod,idx-aa);
    aa++;
    idx=list.GetNextItem(idx,LVNI_SELECTED);	
    }
  UpdateListOfAnims(obj);
  }

//--------------------------------------------------

void CAnimListBar::SetActiveIndex(int id, bool redraw)
  {
  int cnt=list.GetItemCount();
  int idx;
  for (idx=0;idx<cnt;idx++)
    {
    int j=TranslateIndex(idx);
    if (j==id) 
      {
      list.SetItemState(idx,LVIS_FOCUSED,LVIS_FOCUSED);
      break;
      }
    }
  if (idx!=-1)
    if (redraw)
      {
      comint.Begin(WString(IDS_USEANIMATION,id));
      comint.Run(new CIUseAnimation(id));
      doc->UpdateAllViews(NULL);
      }
  }

//--------------------------------------------------

void CAnimListBar::OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NMLISTVIEW* pNMListView = (NMLISTVIEW*)pNMHDR;

  if (pNMListView->uNewState  & LVIS_FOCUSED)
    {
    if (GetKeyState(VK_RBUTTON) & 0x80) return;  	  
    int i=GetActiveIndex();
    if (i==-1) return;
    comint.Begin(WString(IDS_USEANIMATION,i));
    comint.Run(new CIUseAnimation(i));
    doc->UpdateAllViews(NULL);
    doc->curanim=i;
    int disp=config->GetInt(IDS_CFGPREVANIM);
    if (disp==0) doc->prevanim=-1;
    else 
      {
      doc->prevanim=i+disp;
      int na=obj->NAnimations();
      while (doc->prevanim<0) doc->prevanim+=na;
      while (doc->prevanim>=na) doc->prevanim-=na;
      }
    lastindex=i;
    }	  
  *pResult = 0;
  }

//--------------------------------------------------

void CAnimListBar::OnKillfocusList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  for (int i=-1;(i=list.GetNextItem(-1,LVIS_SELECTED))!=-1;)
    list.SetItemState(i,0,LVIS_SELECTED);	
  int idx=list.GetNextItem(-1,LVIS_FOCUSED);
  if (idx!=-1)	
    list.SetItemState(idx,LVIS_SELECTED,LVIS_SELECTED);	
  *pResult = 0;
  }

//--------------------------------------------------

void CAnimListBar::OnAnimRename() 
  {
  int idx=list.GetNextItem(-1,LVIS_SELECTED);
  if (idx!=-1) list.EditLabel(idx);
  }

//--------------------------------------------------

void CAnimListBar::OnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult) 
  { 
  NMLVDISPINFO* pDispInfo = (NMLVDISPINFO*)pNMHDR;
  LVITEM *it=&(pDispInfo->item);
  *pResult = 0;
  long lParam=it->lParam;
  float val;
  if (it->pszText==NULL || sscanf(it->pszText,"%f",&val)==-1) return;
  comint.Begin(WString(IDS_CHANGEANIMTIME));
  comint.Run(new CIAnimSimple(ID_ANIM_RENAME,lParam,val));
  UpdateListOfAnims(obj);
  }

//--------------------------------------------------

BOOL CAnimListBar::OnAnimRedefine(UINT cmd) 
  {
  comint.Begin(WString(IDS_REDEFANIMATION));
  for (int idx=-1;(idx=list.GetNextItem(idx,LVNI_SELECTED))!=-1;)
    comint.Run(new CIAnimSimple(cmd,TranslateIndex(idx)));
  UpdateListOfAnims(obj);
  return TRUE;
  }

//--------------------------------------------------

int CAnimListBar::TranslateIndex(int idx)
  {
  LVITEM it;
  it.mask=LVIF_PARAM;
  it.iItem=idx;
  it.iSubItem=0;
  list.GetItem(&it);
  return it.lParam;
  }

//--------------------------------------------------

void CAnimListBar::UpdatePhase()
  {
  OnAnimRedefine(ID_ANIM_REDEFINE);
  }

//--------------------------------------------------

void CAnimListBar::OnAnimCopy() 
  {
  int idx=GetActiveIndex(LVNI_SELECTED);
  if (idx==-1) return;
  SRef<ObjectData> source=new ObjectData(*obj);
  SetCursor(theApp.LoadStandardCursor(IDC_WAIT));
  if( source )
    {
    source->UseAnimation(idx);
    source->DeleteAllAnimations();
    source->GetTool<ObjToolClipboard>().SaveClipboard(theApp.ClipFormat1);
    }
  }

//--------------------------------------------------

class CIPasteAnimation: public CICommand
  {
  protected:
    ObjectData *source;
    int phase;
  public:
    CIPasteAnimation(ObjectData *src,int p):source(src),phase(p) 
      {recordable=false;}
    virtual ~CIPasteAnimation() 
      {delete source;}
    virtual int Execute(LODObject *obj)
      {
      ObjectData *objData=obj->Active();
      objData->SetDirty();
      objData->UseAnimation(-1);
      AnimationPhase *nPhase=objData->GetAnimation(phase);
      nPhase->Redefine(source);
      objData->UseAnimation(phase);
      return 0;
      }
  };

//--------------------------------------------------

class CIPasteAnimationOverSel:public CIPasteAnimation
  {
  public:
    CIPasteAnimationOverSel(ObjectData *src,int p):
        CIPasteAnimation(src,p) 
          {};
        virtual int Execute(LODObject *obj)
          {
          ObjectData *objData=obj->Active();
          objData->SetDirty();
          objData->UseAnimation(-1);
          AnimationPhase *nPhase=objData->GetAnimation(phase);
          const Selection &sel=*objData->GetSelection();
          int i;
          for( i=0; i<objData->NPoints(); i++ ) if( sel.PointSelected(i) )
            {
            float weight=sel.PointWeight(i);
            (*nPhase)[i]=(*nPhase)[i]*(1-weight)+source->Point(i)*weight;
            }
          objData->UseAnimation(phase);	  
          return 0;
          }
  };  

//--------------------------------------------------

BOOL CAnimListBar::OnAnimPaste(UINT cmd) 
  {
  int idx=GetActiveIndex(LVNI_SELECTED);
  if (idx==-1) return TRUE;
  ObjectData *source=new ObjectData();
  if( source && source->GetTool<ObjToolClipboard>().LoadClipboard(theApp.ClipFormat1) )
    {
    if( source->NPoints()!=obj->NPoints() )
      {
      AfxMessageBox(IDS_BADIMPORT,MB_OK|MB_ICONSTOP);
      delete source;
      return TRUE;
      }	
    comint.Begin(WString(IDS_ANIMPASTE));
    if (cmd==ID_ANIM_PASTE)
      comint.Run(new CIPasteAnimation(source,idx));
    else
      comint.Run(new CIPasteAnimationOverSel(source,idx));
    doc->UpdateAllViews(NULL,1);
    }
  else
    delete source;
  return TRUE;
  }

//--------------------------------------------------

void CAnimListBar::OnAnimImport() 
  {  
  int idx=GetActiveIndex(LVNI_SELECTED);
  if (idx==-1) return;
  CFileDialogEx fdlg(TRUE, WString(IDS_FILEDEFEXT), NULL,OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, WString(IDS_FILEOPENFILTERS));
  if (fdlg.DoModal()==IDCANCEL) return;
  Pathname name=fdlg.GetPathName();
  SRef<LODObject> source=new LODObject();
  if( source )
    {
    if( source->Load(name,NULL,NULL)>=0 )
      {
      // import into new animation phases
      float lodResol=doc->LodData->Resolution(doc->LodData->ActiveLevel());
      int srcLevel=source->FindLevel(lodResol);
      ObjectData *srcData=source->Level(srcLevel);
      if( srcData->NPoints()!=obj->NPoints() )
        {
        AfxMessageBox(IDS_BADIMPORT,MB_OK|MB_ICONSTOP);
        return;
        }
      source->DetachLevel(srcLevel);
      comint.Begin(WString(IDS_ANIMIMPORT));
      comint.Run(new CIPasteAnimation(srcData,idx));
      doc->UpdateAllViews(NULL,1);
      }
    }
  }

//--------------------------------------------------

BOOL CAnimListBar::OnAnimAutotime(UINT cmd) 
  {
  comint.Begin(WString(IDS_ANIMAUTOTIME));
  comint.Run(new CIAnimSimple(cmd));
  UpdateListOfAnims(obj);
  return TRUE;
  }

//--------------------------------------------------

BOOL CAnimListBar::OnAnimHalfrate(UINT cmd) 
  {
  comint.Begin(WString(IDS_HALFRATE));
  comint.Run(new CIAnimSimple(cmd));
  UpdateListOfAnims(obj);
  return TRUE;
  }

//--------------------------------------------------

class CIFromMatrices:public CICommand
  {
  Pathname matrixpath;
  bool alllods;
  public:
    CIFromMatrices(const char *path,bool all=false):matrixpath(path),alllods(all) 
      {}
    CIFromMatrices(istream& str)
      {
      char path[MAX_PATH];
      datard(str,alllods);
      FLoadString(str,path,sizeof(path));
      matrixpath=path;
      }
    virtual int Tag() const 
      {return CITAG_FROMMATRICES;}
    virtual void Save(ostream& str)
      {
      datawr(str,alllods);
      FSaveString(str,matrixpath);
      }
    virtual int Execute(LODObject *obj)
      {
      if (alllods)
        {
        for (int i=0;i<obj->NLevels();i++)
          obj->Level(i)->GetTool<ObjToolAnimation>().AutoAnimation(matrixpath);
        }
      else
        obj->Active()->GetTool<ObjToolAnimation>().AutoAnimation(matrixpath);
      return 0;
      }
  };

//--------------------------------------------------

CICommand *CIFromMatrices_Create(istream &str)
  {
  return new CIFromMatrices(str);
  }

//--------------------------------------------------

void CAnimListBar::OnAnimFrommatrices() 
  {
  CFileDialogEx fdlg(TRUE, WString(IDS_RTMDEFEXT), defpath,OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, WString(IDS_RTMFILTER));
  if (fdlg.DoModal()==IDCANCEL) return;
  comint.Begin(WString(IDS_ANIMFROMMATRIX));
  comint.Run(new CIFromMatrices(fdlg.GetPathName()));
  comint.SetLongOp();
  frame->PostMessage(WM_COMMAND,ID_VIEW_REFRESH,0);
  defpath=fdlg.GetPathName();
  }

//--------------------------------------------------

void CAnimListBar::OnAnimExportmatrices() 
  {
  CFileDialogEx fdlg(FALSE, WString(IDS_RTMDEFEXT), defpath,OFN_HIDEREADONLY | OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT, WString(IDS_RTMFILTER));
  if (fdlg.DoModal()==IDCANCEL) return;
  theApp.rocheck.TestFileRO(fdlg.GetPathName(),ROCHF_DisableSaveAs);
  obj->GetTool<ObjToolAnimation>().ExportAnimationLooped(fdlg.GetPathName(),GetStepSelection(obj));
  defpath=fdlg.GetPathName();
  }

//--------------------------------------------------

void CAnimListBar::OnAnimSoftselect() 
  {
  static CSoftenDlg dlg;
  if (dlg.DoModal()==IDCANCEL) return;
  comint.Begin(WString(IDS_SOFTENSELECTED));
  comint.Run(new CISoftenSelected(dlg));
  }

//--------------------------------------------------

BOOL CAnimListBar::OnAnimNeutralizestep(UINT cmd) 
  {
  if (GetStepSelection(obj)=="") return TRUE;
  comint.Begin(WString(cmd==ID_ANIM_NEUTRALIZESTEP?IDS_NEUTRALIZESTEP:IDS_NEUTRALIZEXSTEP));
  comint.Run(new CIAnimSimple(cmd));    
  doc->UpdateAllViews(NULL);
  doc->UpdateAllToolbars(1);
  return TRUE;
  }

//--------------------------------------------------

BOOL CAnimListBar::OnAnimXnormcenter(UINT cmd) 
  {  
  if (GetStepSelection(obj)=="") return TRUE;
  comint.Begin(WString(IDS_XNORMCENTER));
  comint.Run(new CIAnimSimple(cmd));    
  doc->UpdateAllViews(NULL);
  doc->UpdateAllToolbars(1);
  return TRUE;
  }

//--------------------------------------------------

BOOL CAnimListBar::OnAnimZnormcenter(UINT cmd) 
  {
  if (GetStepSelection(obj)=="") return TRUE;
  comint.Begin(WString(IDS_ZNORMCENTER));
  comint.Run(new CIAnimSimple(cmd));    
  doc->UpdateAllViews(NULL);
  doc->UpdateAllToolbars(1);
  return TRUE;
  }

//--------------------------------------------------

BOOL CAnimListBar::OnAnimMirror(UINT cmd) 
  {
  comint.Begin(WString(IDS_ANIMMIRROR));
  comint.Run(new CIAnimSimple(cmd));    
  UpdateListOfAnims(obj);  
  return TRUE;
  }

//--------------------------------------------------

BOOL CAnimListBar::OnAnimInsertstep(UINT cmd) 
  {
  comint.Begin(WString(IDS_INSERTSTEP));
  comint.Run(new CIAnimSimple(cmd));    
  doc->UpdateAllViews(NULL);
  doc->UpdateAllToolbars(1);
  return TRUE;
  }

//--------------------------------------------------

void CAnimListBar::OnAnimFrommatriceslod() 
  {
  CFileDialogEx fdlg(TRUE, WString(IDS_RTMDEFEXT), defpath,OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, WString(IDS_RTMFILTER));
  if (fdlg.DoModal()==IDCANCEL) return;
  comint.Begin(WString(IDS_ANIMFROMMATRIX));
  comint.Run(new CIFromMatrices(fdlg.GetPathName(),true));
  comint.SetLongOp();
  UpdateListOfAnims(obj);
  frame->PostMessage(WM_COMMAND,ID_VIEW_REFRESH,0);
  defpath=fdlg.GetPathName();
  }

//--------------------------------------------------

void CAnimListBar::OnAnimLockselected() 
  {
  int handle=lock.FindLod(curlod);
  if (handle>=0) lock.DeleteLod(handle);
  handle=lock.CreateLod(curlod,obj->NAnimations());
  int idx=list.GetNextItem(-1,LVIS_SELECTED);
  while (idx!=-1)
    {
    lock.SetWeight(handle,idx,1.0f);
    idx=list.GetNextItem(idx,LVIS_SELECTED);
    }
  UpdateListOfAnims(obj);  
  }

//--------------------------------------------------

void CAnimListBar::OnAnimUnlockall() 
  {
  int handle=lock.FindLod(curlod);
  if (handle>=0) lock.DeleteLod(handle);
  UpdateListOfAnims(obj);  
  }

//--------------------------------------------------

float* CAnimListBar::GetWeightList(int &size)
  {
  int handle=lock.FindLod(curlod);
  if (handle>=0)
    {
    size=lock.GetSize(handle);
    return lock.GetWeights(handle);
    }
  else
    {
    size=0;
    return NULL;
    }
  }

//--------------------------------------------------

#include "TimeRangeDlg.h"

void CAnimListBar::OnAnimTimerange() 
  {
  static CTimeRangeDlg dlg;
  int handle=lock.FindLod(curlod);
  int idx=GetActiveIndex(LVNI_SELECTED);
  if (idx==-1) return;
  float curtime=obj->GetAnimation(idx)->GetTime();
  float mintime,maxtime;
  bool set=true;
  for (int i=0;i<obj->NAnimations();i++)
    {
    float tm=obj->GetAnimation(i)->GetTime();
    if (tm<0.0) continue;
    if (set) mintime=maxtime=tm;	
    else if (mintime>tm) mintime=tm;
    else if (maxtime<tm) maxtime=tm;
    set=false;
    }
  if (dlg.DoModal()==IDOK)
    {
    if (handle>=0) lock.DeleteLod(handle);
    handle=lock.CreateLod(curlod,obj->NAnimations());
    for (int i=0;i<obj->NAnimations();i++)
      {
      const AnimationPhase *phs=obj->GetAnimation(i);
      float f=phs->GetTime();
      float weight;
      if (f<0.0f) continue;
      if ((weight=fabs(f-curtime))>dlg.timerange)
        {
        float r=maxtime-mintime;		
        if ((weight=fabs(f-(curtime+r)))<=dlg.timerange && dlg.loop)
          weight=1.0f-(weight/dlg.timerange);
        else if ((weight=fabs(f-(curtime-r)))<=dlg.timerange && dlg.loop)
          weight=1.0f-(weight/dlg.timerange);
        else weight=0.0f;
        }
      else weight=1.0f-(weight/dlg.timerange);
      if (!dlg.interpolate && weight!=0.0f) weight=1.0f;
      lock.SetWeight(handle,i,weight);
      }
    UpdateListOfAnims(obj);  
    }
  }

//--------------------------------------------------

void CAnimListBar::OnDocumentReset()
  {
  lock.Reset();
  }

//--------------------------------------------------

#include "SelectSelectionDlg.h"
#include ".\animlistbar.h"

CString CAnimListBar::GetStepSelection(ObjectData *obj)
  {
  CString stepsel=config->GetString(IDS_STEPANIMNAME);
  if (stepsel=="" || obj->GetNamedSel(stepsel)==NULL)
    {
    CSelectSelectionDlg dlg;
    dlg.obj=obj;
    if (dlg.DoModal()==IDNO) return CString("");
    config->SetString(IDS_STEPANIMNAME,dlg.vList,true);
    return dlg.vList;
    }
  return stepsel;
  }

void CAnimListBar::OnAnimDeleteallanimations()
  {
  comint.Begin(WString(IDS_DELANIMATION));
  comint.Run(new CIAnimSimple(ID_ANIM_DELETEALLANIMATIONS));    
  UpdateListOfAnims(obj);  
  }

int CIAnimAutoTime::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();
  obj->GetTool<ObjToolAnimation>().AutoTimeAnimations(false);
  return 0;
  }

void CAnimListBar::OnPluginPopupCommand(UINT cmd)
{
  theApp.HookMenuCommand(cmd);
}
BOOL CAnimListBar::PreTranslateMessage(MSG* pMsg)
{
  if (list.GetEditControl()!=0 && IsDialogMessage(pMsg)) return TRUE;
  else return CDialogBar::PreTranslateMessage(pMsg);  
}