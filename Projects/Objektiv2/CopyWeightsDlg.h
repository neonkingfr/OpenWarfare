#if !defined(AFX_COPYWEIGHTSDLG_H__F87A6C69_EE89_4BDE_B479_A70C443F837F__INCLUDED_)
#define AFX_COPYWEIGHTSDLG_H__F87A6C69_EE89_4BDE_B479_A70C443F837F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CopyWeightsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCopyWeightsDlg dialog

class CCopyWeightsDlg : public CDialog
  {
  // Construction
  public:
    CCopyWeightsDlg(CWnd* pParent = NULL);   // standard constructor
    
    LODObject *lod;
    
    
    // Dialog Data
    //{{AFX_DATA(CCopyWeightsDlg)
    enum 
      { IDD = IDD_COPYWEIGHTS };
    CListCtrl	wSelList;
    CListCtrl	wRefLod;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CCopyWeightsDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CCopyWeightsDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnItemchangedReflod(NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COPYWEIGHTSDLG_H__F87A6C69_EE89_4BDE_B479_A70C443F837F__INCLUDED_)
