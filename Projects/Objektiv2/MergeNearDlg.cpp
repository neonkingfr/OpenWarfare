// MergeNearDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "MergeNearDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMergeNearDlg dialog


CMergeNearDlg::CMergeNearDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CMergeNearDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CMergeNearDlg)
    distance = 0.0001f;
    //}}AFX_DATA_INIT
    obj=NULL;
    }

//--------------------------------------------------

void CMergeNearDlg::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_DISTANCE,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CMergeNearDlg)
  DDX_Text(pDX, IDC_DISTANCE, distance);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CMergeNearDlg, CDialog)
  //{{AFX_MSG_MAP(CMergeNearDlg)
  ON_BN_CLICKED(IDC_DETECT, OnDetect)
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CMergeNearDlg message handlers
      
      #define GROUPS 7
      
      static float distr[GROUPS+1]=
        {
        0.01f,0.02f,0.04f,0.08f,0.16f,0.32f,0.50f,1.0f
        };

//--------------------------------------------------

void CMergeNearDlg::OnDetect() 
  {
  if (obj)
    {
    float *lens=new float[obj->NPoints()];
    int n=0;
    float max=0;
    float min=1e12;
    SetCursor(LoadCursor(NULL,IDC_WAIT));
    int i;
    ProgressBar<int> pp(obj->NPoints());
    for (i=0;i<obj->NPoints();i++) if (obj->PointSelected(i))
      {
      pp.AdvanceNext(1);        
      float mind=1e12;
      PosT &ps1=obj->Point(i);
      for (int j=0;j<obj->NPoints();j++) if (i!=j && obj->PointSelected(j))
        {
        PosT &ps2=obj->Point(j);
        float d2=ps1.Distance2(ps2);
        if (mind>d2) mind=d2;
        }
      float d=(float)sqrt(mind);
      lens[n++]=d;
      if (d>max) max=d;
      if (d<min) min=d;
      }	
    int groups[GROUPS];
    float dif=max-min;
    memset(groups,0,sizeof(groups));
    for (i=0;i<n;i++) 
      for (int j=0;j<GROUPS;j++)
        {
        if (lens[i]-min>=distr[j]*dif && lens[i]-min<distr[j+1]*dif)
          {
          groups[j]++;
          break;
          }
        }
    int pmin=0;
    int grp=0;
    for (i=0;i<GROUPS;i++) 
      if (groups[i]>pmin) 
        {pmin=groups[i];grp=i;}
    distance=distr[grp+1]*dif+min+0.00001;
    UpdateData(FALSE);
    delete [] lens;
    }
  }

//--------------------------------------------------

