// IUserMapping.cpp: implementation of the CIUserMapping class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "Objektiv2Doc.h"
#include "IUserMapping.h"
#include "ComInt.h"
#include "optima\express.hpp"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIUserMapping::CIUserMapping(bool wrapu, bool wrapv, bool onlyfaces, bool onlypoints, CString *vals, const char *texname)
  {
  this->wrapu=wrapu;
  this->wrapv=wrapv;
  this->onlyfaces=onlyfaces;
  this->onlypoints=onlypoints;
  for (int i=0;i<6;i++) values[i]=vals[i];
  texture=texname;
  }

//--------------------------------------------------

CIUserMapping::CIUserMapping(istream &str)
  {
  char text[1024];
  for (int i=0;i<6;i++)
    {
    FLoadString(str,text,sizeof(text));
    values[i]=text;
    }
  FLoadString(str,text,sizeof(text));
  texture=text;
  datard(str,wrapu);
  datard(str,wrapv);
  datard(str,onlyfaces);
  datard(str,onlypoints);
  }

//--------------------------------------------------

CICommand *CIUserMapping_Create(istream& str)
  {
  return new CIUserMapping(str);
  }

//--------------------------------------------------

CIUserMapping::~CIUserMapping()
  {
  }

//--------------------------------------------------

void CIUserMapping::Save(ostream &str)
  {
  int i;
  for (i=0;i<6;i++)
    {
    FSaveString(str,values[i]);	
    }
  FSaveString(str,values[i]);
  datawr(str,wrapu);
  datawr(str,wrapv);  
  datawr(str,onlyfaces);
  datawr(str,onlypoints);
  }

//--------------------------------------------------

static EvalError EvalConstExpr(CString *vals )
  {
  EvalError error=EvalOK;
  Evaluate(vals[0],error,0);
  if( error!=EvalOK ) return error;
  Evaluate(vals[1],error,1);
  if( error!=EvalOK ) return error;
  Evaluate(vals[2],error,2);
  if( error!=EvalOK ) return error;
  Evaluate(vals[3],error,3);
  if( error!=EvalOK ) return error;
  return EvalOK;
  }

//--------------------------------------------------

static EvalError EvalUV( CString *vals, float &u, float &v, const Vector3 &pos )
  {
  EvalError error=EvalOK;
  SetVariable('x'-'a',pos[0]);
  SetVariable('y'-'a',pos[1]);
  SetVariable('z'-'a',pos[2]);
  u=atof(Evaluate(vals[4],error));
  if( error!=EvalOK ) return error;
  v=atof(Evaluate(vals[5],error));
  if( error!=EvalOK ) return error;
  return EvalOK;
  }

//--------------------------------------------------

int CIUserMapping::Execute(LODObject *lod)
  {
  Vector3 refPoint;
  refPoint[0]=doc->pin[XVAL];
  refPoint[1]=doc->pin[YVAL];
  refPoint[2]=doc->pin[ZVAL];
  ObjectData *obj=lod->Active();
  EvalError error;
  int i;
  
  //	if (texture.GetLength()>26) 
  //		AfxMessageBox(IDS_TEXNAMETOOLONG,MB_OK|MB_ICONEXCLAMATION);
  
  error=EvalConstExpr(values);
  if( error!=EvalOK ) goto Error;
  for( i=0; i<obj->NFaces(); i++ )
    {
    if(onlyfaces && !obj->FaceSelected(i) ) continue;
    FaceT face(obj,i);
    for( int j=0; j<face.N(); j++ )
      {
      int pIndex=face.GetPoint(j);
      if( onlypoints && !obj->PointSelected(pIndex) ) continue;
      Vector3 pos=(Vector3)obj->Point(pIndex)-refPoint;
      float u,v;
      error=EvalUV(values,u,v,pos);
      if( error!=EvalOK ) goto Error;
      face.SetUV(j,u,v);
      if( error!=EvalOK ) goto Error;
      if( texture.GetLength()>0 )
        { 
        face.SetTexture((LPCTSTR)texture);
        }
      obj->SetDirty();
      }
    if( !onlypoints && wrapu )
      { // perform cyclical correction of texture coordinate
      double uMax=-1e10;
      int j;
      for( j=0; j<face.N(); j++ )
        {
        float u=face.GetU(j);
        u=u-floor(u); // clamp into interval <0,1)
        if( uMax<u ) uMax=u;
        face.SetU(j,u);
        }
      for( j=0; j<face.N(); j++ )
        {
        float u=face.GetU(j);
        if( uMax-u>0.5 )
          {
          // texture should wrap through point 1.0
          u=u+1.0;
          }
        face.SetU(j,u);
        }
      }
    if( !onlypoints && wrapv)
      { // perform cyclical correction of texture coordinate
      double vMax=-1e10;
      int j;
      for( j=0; j<face.N(); j++ )
        {
        float v=face.GetV(j);
        v=v-floor(v); // clamp into interval <0,1)
        if( vMax<v ) vMax=v;
        face.SetV(j,v);
        }
      for( j=0; j<face.N(); j++ )
        {
        float v=face.GetV(j);
        if( vMax-v>0.5 )
          {
          // texture should wrap through point 1.0
          v=v+1.0;
          }
        face.SetV(j,v);
        }
      }
    }
  
  Error:
  if( error!=EvalOK )
    {
    AfxMessageBox(IDS_CALCERROR);
    return -1;
    }
  return 0;
  }

//--------------------------------------------------

