// TexPrewWin.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "TexPrewWin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTexPrewWin

CTexPrewWin::CTexPrewWin()
  {
  bitmap=NULL;
  winclass=NULL;
  }

//--------------------------------------------------

CTexPrewWin::~CTexPrewWin()
  {
  free(bitmap);
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CTexPrewWin, CWnd)
  //{{AFX_MSG_MAP(CTexPrewWin)
  ON_WM_PAINT()
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      
      /////////////////////////////////////////////////////////////////////////////
      // CTexPrewWin message handlers
      
      void CTexPrewWin::Create(char * fullfilename)
        {
        RECT rc,rc2;
        DWORD wflags;
        if (m_hWnd) DestroyWindow();
        TUNIPICTURE *uni=cmLoadUni(fullfilename);
        if (uni==NULL) return;
        ::GetClientRect(::GetDesktopWindow(),&rc);
        rc2=rc;
        rc.right=rc.right*2/3;
        rc.bottom=rc.bottom*2/3;
        free(bitmap);
        bitmap=(BITMAPINFO *)cmUni2BitMap(uni, 1, rc.right, rc.bottom);
        rc.right=bitmap->bmiHeader.biWidth;
        rc.bottom=bitmap->bmiHeader.biHeight;
        rc.left=rc2.right/2-rc.right/2;
        rc.top=rc2.bottom/2-rc.bottom/2;
        rc.right+=rc.left;
        rc.bottom+=rc.top;
        wflags=WS_BORDER |WS_SYSMENU|WS_VISIBLE|WS_OVERLAPPED|WS_CAPTION;
        AdjustWindowRect(&rc, wflags,FALSE);  
        if (winclass==NULL) winclass=AfxRegisterWndClass(0,::LoadCursor(NULL,IDC_ARROW),0,NULL);
        CString preview;
        AfxFormatString1(preview,IDS_PREVIEW,fullfilename);
        CreateEx(WS_EX_TOOLWINDOW   |WS_EX_WINDOWEDGE, winclass,preview, wflags, rc, AfxGetMainWnd(), 0,NULL);
        }

//--------------------------------------------------

void CTexPrewWin::OnPaint() 
  {
  CPaintDC dc(this); // device context for painting
  int shift;
  if (bitmap->bmiHeader.biBitCount==24) shift=sizeof(bitmap->bmiHeader);
  else shift=sizeof(bitmap->bmiHeader)+4;
  SetDIBitsToDevice( dc,0,0,bitmap->bmiHeader.biWidth,bitmap->bmiHeader.biHeight,
  0,0,0,bitmap->bmiHeader.biHeight,
  (void *)((char *)bitmap+shift),
  bitmap,0);
  }

//--------------------------------------------------

