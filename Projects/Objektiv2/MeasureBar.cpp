// MeasureBar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MeasureBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMeasureBar dialog


CMeasureBar::CMeasureBar()
  : CDialogBar()
    {
    //{{AFX_DATA_INIT(CMeasureBar)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    //{{AFX_DATA_MAP(CMeasureBar)
    // NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CMeasureBar, CDialogBar)
  //{{AFX_MSG_MAP(CMeasureBar)
  ON_WM_DESTROY()
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CMeasureBar message handlers
      
      void CMeasureBar::Create(CWnd *parent, int menuid)
        {
        CDialogBar::Create(parent,IDD,CBRS_LEFT,menuid);
        SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
        EnableDocking(CBRS_ALIGN_ANY);
        CString top;top.LoadString(IDS_MEASUREINFOTITLE);
        SetWindowText(top);
        main=parent;
        check1.Attach(*GetDlgItem(IDC_PINTOCUR));
        check2.Attach(*GetDlgItem(IDC_PINTOCENTER));
        check1.SetCheck(1);
        }

//--------------------------------------------------

void CMeasureBar::UpdateValues(HVECTOR pin, HVECTOR cur)
  {
  HVECTOR vx;
  if (check2.GetCheck())	
    GetObjectCenter(true,vx,NULL);	
  else
    CopyVektor(vx,cur);
  RozdilVektoru(vx,pin);
  float d=ModulVektoru(vx);
  UpdateFloat(IDC_DESTATION,d);
  NormalizeVector(vx);
  UpdateFloat(IDC_XDIR,vx[XVAL]);
  UpdateFloat(IDC_YDIR,vx[YVAL]);
  UpdateFloat(IDC_ZDIR,vx[ZVAL]);
  float azimut=atan2(vx[ZVAL],vx[XVAL]);
  float zenit=atan2(vx[YVAL],sqrt(vx[XVAL]*vx[XVAL]+vx[ZVAL]*vx[ZVAL]));
  UpdateFloat(IDC_AZIMUT,todec(azimut));
  UpdateFloat(IDC_ZENIT,todec(zenit));
  }

//--------------------------------------------------

void CMeasureBar::UpdateFloat(int idc, float f)
  {
  char text[32];
  sprintf(text,"%.3f",f);
  SetDlgItemText(idc,text);
  }

//--------------------------------------------------

bool CMeasureBar::IsVisible()
  {
  return (GetStyle() & WS_VISIBLE)!=0;
  }

//--------------------------------------------------

void CMeasureBar::OnDestroy() 
  {
  check1.Detach();
  check2.Detach();
  CDialogBar::OnDestroy();
  }

//--------------------------------------------------

