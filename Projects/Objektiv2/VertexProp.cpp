// VertexProp.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MainFrm.h"
#include "VertexProp.h"
#include "ComInt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVertexProp dialog


CVertexProp::CVertexProp(CWnd* pParent /*=NULL*/)
  : CDialog(CVertexProp::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CVertexProp)
    Decal = -1;
    Fog = -1;
    Surface = -1;
    User = _T("");
    YVal = _T("");
    XVal = _T("");
    ZVal = _T("");
    Light = -1;
    nVert = _T("");
    initialized = false;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CVertexProp::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_XVALUE,IDC_YVALUE,IDC_ZVALUE,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CVertexProp)
  DDX_Control(pDX, IDC_HIDDEN_VERTEX, HiddenVertex);
  DDX_Control(pDX, IDC_LIST, List);
  DDX_Radio(pDX, IDC_DECAL_NORMAL, Decal);
  DDX_Radio(pDX, IDC_FOG_NORMAL, Fog);
  DDX_Radio(pDX, IDC_SURFACE_NORMAL, Surface);
  DDX_Radio(pDX, IDC_NORMALCALC, Normal);
  DDX_Text(pDX, IDC_USERVALUE, User);
  DDX_Text(pDX, IDC_YVALUE, YVal);
  DDX_Text(pDX, IDC_XVALUE, XVal);
  DDX_Text(pDX, IDC_ZVALUE, ZVal);
  DDX_Radio(pDX, IDC_LIGHTING_NORMAL, Light);
  DDX_Text(pDX, IDC_NUMVERTICES, nVert);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CVertexProp, CDialog)
  //{{AFX_MSG_MAP(CVertexProp)
  ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
    ON_BN_CLICKED(IDC_SELECTALL, OnSelectall)
      ON_BN_CLICKED(IDC_FILTER, OnFilter)
        ON_BN_CLICKED(IDC_APPLY, OnApply)
          ON_BN_CLICKED(IDC_CREATESEL, OnCreatesel)
            //}}AFX_MSG_MAP
            ON_WM_SIZE()
            ON_WM_SIZING()
END_MESSAGE_MAP()
              
              /////////////////////////////////////////////////////////////////////////////
              // CVertexProp message handlers
              
              #define CASEFLAG(flag,mask) if ((flag) & (mask))
              
              void CVertexProp::ReadFlags(ULONG flags, bool first)
                {
                int fog,decal,light,surface,normal;
                bool hidden;                
                fog=0;decal=0;light=0;surface=0;hidden=false;normal=0;
                CASEFLAG(flags,POINT_ONLAND) surface=1;
                CASEFLAG(flags,POINT_UNDERLAND) surface=2;
                CASEFLAG(flags,POINT_ABOVELAND) surface=3;
                CASEFLAG(flags,POINT_KEEPLAND) surface=4;
                
                CASEFLAG(flags,POINT_DECAL) decal=1;
                CASEFLAG(flags,POINT_VDECAL) decal=2;
                
                CASEFLAG(flags,POINT_NOFOG) fog=2;
                CASEFLAG(flags,POINT_SKYFOG) fog=1;
                
                CASEFLAG(flags,POINT_SPECIAL_HIDDEN) hidden=true;
                
                CASEFLAG(flags,POINT_NOLIGHT) light=1;
                CASEFLAG(flags,POINT_AMBIENT) light=2;
                CASEFLAG(flags,POINT_FULLLIGHT) light=4;
                CASEFLAG(flags,POINT_HALFLIGHT) light=3;

                CASEFLAG(flags,POINT_SPECIAL_LOCKNORMAL) normal=2;
                CASEFLAG(flags,POINT_SPECIAL_ANGLENORMAL) normal=1;
                
                if (first)
                  {
                  Fog=fog;
                  Decal=decal;
                  HiddenVertex.SetCheck(hidden==true);
                  Light=light;
                  Surface=surface;
                  Normal=normal;
                  }
                else
                  {
                  if (Fog!=fog) Fog=-1;
                  if (Decal!=decal) Decal=-1;
                  if (Surface!=surface) Surface=-1;
                  if (Light!=light) Light=-1;
                  if (Normal!=normal) Normal=-1;
                  int x= HiddenVertex.GetCheck();
                  if (x==0 && hidden || x!=0 && !hidden) HiddenVertex.SetCheck(2);
                  
                  }  
                UpdateData(FALSE);
                }

//--------------------------------------------------

ULONG CVertexProp::GetFlags(ULONG src)
  {
  static ULONG FogTable[]=
    {0,POINT_SKYFOG,POINT_NOFOG};
  static ULONG DecalTable[]=
    {0,POINT_DECAL,POINT_VDECAL};
  static ULONG SurfaceTable[]=
    {0,POINT_ONLAND,POINT_UNDERLAND,POINT_ABOVELAND,POINT_KEEPLAND};
  static ULONG LightTable[]=
    {0,POINT_NOLIGHT,POINT_AMBIENT,POINT_HALFLIGHT,POINT_FULLLIGHT};
  static ULONG NormalTable[]=
    {0,POINT_SPECIAL_ANGLENORMAL,POINT_SPECIAL_LOCKNORMAL};
  UpdateData(TRUE);
  if (HiddenVertex.GetCheck()==0) src&=~POINT_SPECIAL_HIDDEN;
  if (HiddenVertex.GetCheck()==1) src|=POINT_SPECIAL_HIDDEN;
  if (Fog!=-1) src=(src & ~POINT_FOG_MASK) | FogTable[Fog];
  if (Decal!=-1) src=(src & ~POINT_DECAL_MASK) | DecalTable[Decal];
  if (Surface!=-1) src=(src & ~POINT_LAND_MASK) | SurfaceTable[Surface];
  if (Light!=-1) src=(src & ~POINT_LIGHT_MASK) | LightTable[Light];
  if (Normal!=-1) src=(src & ~(POINT_SPECIAL_ANGLENORMAL|POINT_SPECIAL_LOCKNORMAL) | NormalTable[Normal]);
  return src;
  }

//--------------------------------------------------

void CVertexProp::ReadSelection()
  {
  int cnt=obj->NPoints();
  List.ResetContent();
  for (int i=0;i<cnt;i++) if (obj->PointSelected(i))
    FormatToList(i,obj->Point(i));
  OnSelectall();
  }

//--------------------------------------------------

void CVertexProp::FormatToList(int idx,PosT &pt)
  {
  char text[512];  
  sprintf(text,"%5d\t%8.3f\t%8.3f\t%8.3f\t%d\t%08x",
    idx,pt[0],pt[1],pt[2],(pt.flags & POINT_USER_MASK)/POINT_USER_STEP,pt.flags);
  int i=List.AddString(text);
  List.SetItemData(i,idx);
  }

//--------------------------------------------------

void CVertexProp::ForAllSelectedLoad()
  {
  SetCursor(theApp.LoadStandardCursor(IDC_WAIT));
  int cnt=List.GetCount();
  nVert.Format("%d",List.GetSelCount());
  bool first=true;
  for (int i=0;i<cnt;i++) if (List.GetSel(i))
    {
    int idx=List.GetItemData(i);
    PosT& pos=obj->Point(idx);
    WString xval,yval,zval,user;
    xval.Format("%.3f",pos[0]);
    yval.Format("%.3f",pos[1]);
    zval.Format("%.3f",pos[2]);
    user.Format("%d", (pos.flags & POINT_USER_MASK)/POINT_USER_STEP);
    if (first)
      {
      XVal=xval;YVal=yval;ZVal=zval;User=user;
      }
    else
      {
      if (XVal!=xval) XVal="";
      if (YVal!=yval) YVal="";
      if (ZVal!=zval) ZVal="";
      if (User!=user) User="";
      }
    ReadFlags(pos.flags,first);
    first=false;
    }
  UpdateData(FALSE);
  }

//--------------------------------------------------

void CVertexProp::OnSelchangeList() 
  {
  ForAllSelectedLoad();	
  }

//--------------------------------------------------

BOOL CVertexProp::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  ReadSelection();

  initialized =true;
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CVertexProp::OnSelectall() 
  {
  if (List.GetCount()<2)
    List.SetSel(0,TRUE);
  else
    List.SelItemRange(true,0,List.GetCount()-1);
  ForAllSelectedLoad();
  }

//--------------------------------------------------

class CISetVertexProp:public CICommand
  {
  PosT value;
  PosT mask;
  int *idxlist;
  int index;
  public:
    CISetVertexProp(int cnt, PosT& value, PosT& mask);
    CISetVertexProp(istream& str);
    virtual ~CISetVertexProp()
      {delete [] idxlist;}
    void AddIdx(int idx)
      {idxlist[index++]=idx;}
    virtual int Execute(LODObject *obj);
    virtual void Save(ostream &str);
    virtual int Tag() const 
      {return CITAG_SETVERTPROP;}
  };

//--------------------------------------------------

CICommand *CISetVertexProp_Create(istream &str)
  {
  return new CISetVertexProp(str);
  }

//--------------------------------------------------

CISetVertexProp::CISetVertexProp(int cnt, PosT& val,PosT& mk):
mask(mk),value(val)
  {
  idxlist=new int[cnt];
  index=0;
  }

//--------------------------------------------------

CISetVertexProp::CISetVertexProp(istream &str)
  {
  datard(str,value[0]);
  datard(str,value[1]);
  datard(str,value[2]);
  datard(str,value.flags);
  datard(str,mask[0]);
  datard(str,mask[1]);
  datard(str,mask[2]);
  datard(str,mask.flags);  
  idxlist=NULL;
  }

//--------------------------------------------------

void CISetVertexProp::Save(ostream &str)
  {
  datawr(str,value[0]);
  datawr(str,value[1]);
  datawr(str,value[2]);
  datawr(str,value.flags);
  datawr(str,mask[0]);
  datawr(str,mask[1]);
  datawr(str,mask[2]);
  datawr(str,mask.flags);  
  }

//--------------------------------------------------

int CISetVertexProp::Execute(LODObject *obj)
  {
  ObjectData *odata=obj->Active();
  int cnt=odata->NPoints();
  int idx=0,sel=0;
  for (int i=0;i<cnt;i++)
    if (odata->PointSelected(i))
      {
      if (idxlist==NULL || idxlist[idx]==sel)
        {
        PosT &ps=odata->Point(i);
        if (mask[0]!=0.0f) ps[0]=value[0];
        if (mask[1]!=0.0f) ps[1]=value[1];
        if (mask[2]!=0.0f) ps[2]=value[2];
        ps.flags=(ps.flags & ~mask.flags) | (value.flags & mask.flags);
        idx++;
        }
      sel++;
      }
  return 0;
  }

//--------------------------------------------------

void CVertexProp::OnFilter() 
  {
  UpdateData();
  bool xvalb,yvalb,zvalb,userb;
  float xval,yval,zval;
  int user;
  ULONG mask1,mask2;
  xvalb=sscanf(XVal,"%f",&xval)==1;
  yvalb=sscanf(YVal,"%f",&yval)==1;
  zvalb=sscanf(ZVal,"%f",&zval)==1;
  userb=sscanf(User,"%d",&user)==1;
  mask1=GetFlags(0);
  mask2=GetFlags(0xFFFFFFFF);
  mask2^=mask1;
  mask1|=mask2;
  int cnt=List.GetCount();
  for (int i=0;i<cnt;i++)
    {
    int idx=List.GetItemData(i);
    PosT& pos=obj->Point(idx);
    ULONG flg=pos.flags|mask2;
    bool mark=(mask1 ^ flg)==0;
    int u=(pos.flags & POINT_USER_MASK)/POINT_USER_STEP;
    if (xvalb) mark=mark && (fabs(xval-pos[0])<0.005);
    if (yvalb) mark=mark && (fabs(yval-pos[1])<0.005);
    if (zvalb) mark=mark && (fabs(zval-pos[2])<0.005);
    if (userb) mark=mark && (user==u);
    List.SetSel(i,mark==true);
    }  
  ForAllSelectedLoad();
  }

//--------------------------------------------------

void CVertexProp::OnApply() 
  {
  UpdateData();
  bool xvalb,yvalb,zvalb,userb;
  float xval,yval,zval;
  int user;
  ULONG mask1,mask2;
  xvalb=sscanf(XVal,"%f",&xval)==1;
  yvalb=sscanf(YVal,"%f",&yval)==1;
  zvalb=sscanf(ZVal,"%f",&zval)==1;
  userb=sscanf(User,"%d",&user)==1;
  mask1=GetFlags(0);
  mask2=GetFlags(POINT_LAND_MASK|POINT_DECAL_MASK|POINT_LIGHT_MASK|POINT_FOG_MASK|POINT_USER_MASK|POINT_SPECIAL_HIDDEN|POINT_SPECIAL_LOCKNORMAL|POINT_SPECIAL_ANGLENORMAL);
  mask2^=mask1;
  PosT mask,pos;
  mask.flags=~mask2;
  mask[0]=(int)xvalb*1.0f;
  mask[1]=(int)yvalb*1.0f;
  mask[2]=(int)zvalb*1.0f;
  pos[0]=xval;
  pos[1]=yval;
  pos[2]=zval;
  if (userb) mask.flags|=POINT_USER_MASK;
  pos.flags=mask1;
  pos.flags&=~POINT_USER_MASK;
  pos.flags|=user*POINT_USER_STEP;
  int cn=List.GetCount();
  CISetVertexProp *pp=new CISetVertexProp(List.GetCount(),pos,mask);
  for (int i=0;i<cn;i++) if (List.GetSel(i)) pp->AddIdx(i);
  comint.Begin(WString(IDS_SETVERTEXPROP));
  comint.Run(pp);
  frame->SendMessage(WM_COMMAND,ID_VIEW_REFRESH,0);
  ReadSelection();
  }

//--------------------------------------------------

void CVertexProp::OnOK() 
  {
  OnApply();	
  CDialog::OnOK();
  }

//--------------------------------------------------

void CVertexProp::OnCreatesel() 
  {
  Selection *sel=new Selection(obj);
  int cn=List.GetCount();
  for (int i=0;i<cn;i++) if (List.GetSel(i)) 
    sel->PointSelect(List.GetItemData(i));  
  comint.Begin(WString(IDS_MAKESELECTION));
  comint.Run(new CISelect(sel));
  EndDialog(IDOK);
  
  }

//--------------------------------------------------



void CVertexProp::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType,cx,cy);
  static int ocx;
  static int ocy;
  if (initialized && (List.GetSafeHwnd()!=NULL)) 
  {
    RECT oldwindowRect;
    GetWindowRect(&oldwindowRect);
    int dx=cx-ocx;
    int dy=cy-ocy;
    if ((ocx!=0)&&((dx!=0)||(dy!=0)))
    {
      HDWP hdwp=BeginDeferWindowPos(60);

      MoveWindowRel(List,0,0,dx,dy,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_YVALUE),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_XVALUE),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_ZVALUE),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_USERVALUE),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_STATIC_SURFACE),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_DECAL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_FOG),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_LIGHNING),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_LIGHNING),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_NORMALS),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_SELECTALL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_FILTER),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_CREATESEL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDOK),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDCANCEL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_APPLY),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_HIDDEN_VERTEX),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_STATIC_USER),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_X),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_Z),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_Y),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_DECAL_NORMAL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_RADIO11),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_SURFACE_NORMAL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_SURFACE_ONSURFACE),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_SURFACE_ABOVE),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_SURFACE_UNDER),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_SURFACE_KEEPHEIGHT),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_FOG_NORMAL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_FOG_SKY),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_FOG_NONE),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_NORMAL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_SHINING),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_SHADOW),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_HALF),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_FULL),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_NORMALCALC),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_NORMALCALC2),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_NORMALCALC3),0,dy,0,0,hdwp);

      EndDeferWindowPos(hdwp);
      List.Invalidate(FALSE);
    }
  }
  ocx=cx;
  ocy=cy;
}

void CVertexProp::OnSizing(UINT fwSide, LPRECT pRect)
{
  static int minx;
  static int miny;
  CRect oldwindowRect;
  GetWindowRect(&oldwindowRect);
  if (minx==0) minx=pRect->right-pRect->left;
  if (miny==0) miny=pRect->bottom-pRect->top;
  if (((fwSide==1)||(fwSide==7)||(fwSide==4))&&(pRect->right-pRect->left<minx)) 
  {
    pRect->left=pRect->right-minx;
    pRect->right=pRect->left+minx;
  }
  if (((fwSide==2)||(fwSide==8)||(fwSide==5))&&(pRect->right-pRect->left<minx)) 
  {
    pRect->right=pRect->left+minx;
    pRect->left=pRect->right-minx;
  }

  if (((fwSide==3)||(fwSide==5)||(fwSide==4))&&(pRect->bottom-pRect->top<miny)) 
  {
    pRect->top=pRect->bottom-miny;
    pRect->bottom=pRect->top+miny;
  }
  if (((fwSide==6)||(fwSide==8)||(fwSide==7))&&(pRect->bottom-pRect->top<miny)) 
  {
    pRect->bottom=pRect->top+miny;
    pRect->top=pRect->bottom-miny;
  }
  CDialog::OnSizing(fwSide, pRect);
}
