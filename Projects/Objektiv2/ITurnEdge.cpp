#include "StdAfx.h"
#include ".\iturnedge.h"

CITurnEdge::CITurnEdge(geLine &atLine,bool top):_atLine(atLine),_top(top)
  {
  recordable=false;
  }

CITurnEdge::~CITurnEdge(void)
  {
  }

int CITurnEdge::Execute(LODObject *lod)
  {
  ObjectData &obj=*(lod->Active());
  gePlaneGen clipPlane(_atLine.p,_atLine.r);
  int na,nb,i;
  float mind=FLT_MAX;
  int fca=-1,fcb=-1;
  int aa,ab,ba,bb;
  for (i=0;i<obj.NFaces();i++) if (obj.FaceSelected(i))
    {
    FaceT fc(obj,i);
    int a,b;
    a=fc.N()-1;
    for (b=0;b<fc.N();a=b++)
      {
      geVector4 va=obj.Point(fc.GetPoint(a));
      geVector4 vb=obj.Point(fc.GetPoint(b));
      geVector4 np;
      geLine usecka(va,vb);
      float d=usecka.Distance(_atLine,&np,NULL);
      float t=usecka.MapPoint(np);
      if (t>=0.0f && t<=1.0f)
        {
        if (_top) d=clipPlane.Distance(np);
        if (d<mind)
            {
            aa=a;
            ab=b;
            na=fc.GetPoint(a);
            nb=fc.GetPoint(b);
            mind=d;
            fca=i;
            }          
        }
      }
    }
  for (fcb=0;fcb<obj.NFaces();fcb++) if (obj.FaceSelected(fcb))
    {
    FaceT fc(obj,fcb);
    int a,b;
    a=fc.N()-1;
    for (b=0;b<fc.N();a=b++)
      if (fc.GetPoint(a)==nb && fc.GetPoint(b)==na) 
        {
        ba=b;
        bb=a;
        goto jumpout;
        }
    }
  MessageBeep(MB_ICONEXCLAMATION);
  return -1;
jumpout:
  FaceT faceA(obj,fca);
  FaceT faceB(obj,fcb);
  FaceT hlpA(obj);
  FaceT hlpB(obj);
  hlpA=faceA;
  hlpB=faceB;
  int pra=faceB.N()==4?bb-2:bb-1;
  int prb=faceA.N()==4?aa-2:aa-1;
  if (pra<0) pra+=faceB.N();
  if (prb<0) prb+=faceA.N();
  int nxa=faceB.GetPoint(pra);
  int nxb=faceA.GetPoint(prb);


  for (i=0;i<faceA.N();i++) if (faceA.GetPoint(i)==nxa)
    {MessageBeep(MB_ICONEXCLAMATION);return -1;}
  for (i=0;i<faceB.N();i++) if (faceB.GetPoint(i)==nxb)
    {MessageBeep(MB_ICONEXCLAMATION);return -1;}
  faceA.CopyPointInfo(ab,hlpB,pra);
  faceB.CopyPointInfo(ba,hlpA,prb);

  faceA.SetPoint(ab,nxa);
  faceB.SetPoint(ba,nxb);


  return 0;
  }