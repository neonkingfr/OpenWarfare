// PtInfoBar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "PtInfoBar.h"
#include "Objektiv2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPtInfoBar dialog


CPtInfoBar::CPtInfoBar()
  : CDialogBar()
    {
    //{{AFX_DATA_INIT(CPtInfoBar)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    //{{AFX_DATA_MAP(CPtInfoBar)
    // NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CPtInfoBar, CDialogBar)
  //{{AFX_MSG_MAP(CPtInfoBar)
  ON_WM_DESTROY()
    ON_EN_KILLFOCUS(IDC_XINFO, OnKillfocusXinfo)
      ON_EN_KILLFOCUS(IDC_YINFO, OnKillfocusYinfo)
        ON_EN_KILLFOCUS(IDC_ZINFO, OnKillfocusZinfo)
          //}}AFX_MSG_MAP
          END_MESSAGE_MAP()
            
            /////////////////////////////////////////////////////////////////////////////
            // CPtInfoBar message handlers
            void CPtInfoBar::Create(CWnd *parent, int menuid, CObjektiv2Doc *parentdoc)
              {
              CDialogBar::Create(parent,IDD,CBRS_TOP,menuid);
              SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
              EnableDocking(CBRS_ALIGN_ANY);
              CString top;top.LoadString(IDS_PTINFOTITLE);
              SetWindowText(top);
              doc=parentdoc;
              xinfo.Attach(*GetDlgItem(IDC_XINFO));
              yinfo.Attach(*GetDlgItem(IDC_YINFO));
              zinfo.Attach(*GetDlgItem(IDC_ZINFO));
              LoadBarSize(*this);
              chg=false;
              }

//--------------------------------------------------

void CPtInfoBar::OnDestroy() 
  {
  xinfo.Detach();
  yinfo.Detach();
  zinfo.Detach();
  SaveBarSize(*this);
  CDialogBar::OnDestroy();	
  }

//--------------------------------------------------

BOOL CPtInfoBar::PreTranslateMessage(MSG* pMsg) 
  {
  if (IsDialogMessage(pMsg))
    {
    if (pMsg->message==WM_KEYDOWN && pMsg->wParam!=VK_TAB && pMsg->wParam!=VK_SHIFT) 
      chg=true;
    return TRUE;
    }
  else
    return CDialogBar::PreTranslateMessage(pMsg);
  }

//--------------------------------------------------

void CPtInfoBar::OnKillfocusXinfo() 
  {
  CString s;
  xinfo.GetWindowText(s);
  if (chg) SetNewPt(s,XVAL);
  chg=false;
  }

//--------------------------------------------------

void CPtInfoBar::OnKillfocusYinfo() 
  {
  CString s;
  yinfo.GetWindowText(s);
  if (chg) SetNewPt(s,YVAL);
  chg=false;
  }

//--------------------------------------------------

void CPtInfoBar::OnKillfocusZinfo() 
  {
  CString s;
  zinfo.GetWindowText(s);
  if (chg) SetNewPt(s,ZVAL);
  chg=false;
  CObjektiv2View::GetCurrentView()->SetFocus();
  }

//--------------------------------------------------

void CPtInfoBar::SetNewPt(const char *text, int value)
  {
  float f;
  if (sscanf(text,"%f",&f)!=1)
    {
    MessageBeep(MB_ICONASTERISK);return;
    }
  if (strchr(text,'*'))
    {
    HVECTOR v;
    v[XVAL]=v[YVAL]=v[ZVAL]=0.0f;
    v[value]=f;
    v[WVAL]=1.0f;
    SoucetVektoru(doc->pin,v);
    }
  else
    {
    doc->pin[value]=f;
    }
  doc->UpdateAllViews(NULL);
  }

//--------------------------------------------------

