#if !defined(AFX_SELECTIONNAMEDLG_H__58344911_A400_46E2_8158_8E3D930E0C62__INCLUDED_)
#define AFX_SELECTIONNAMEDLG_H__58344911_A400_46E2_8158_8E3D930E0C62__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectionNameDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectionNameDlg dialog

class CSelectionNameDlg : public CDialog
  {
  // Construction
  bool exitdlg;
  public:
    void OnCancel();
    void OnOK();
    CSelectionNameDlg(CWnd* pParent = NULL);   // standard constructor
    CPoint showpoint;
    
    // Dialog Data
    //{{AFX_DATA(CSelectionNameDlg)
    enum 
      { IDD = IDD_SELECTIONNAME };
    CComboBox	wList;
    CString	vList;
    //}}AFX_DATA
    
    LODObject *lod;
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CSelectionNameDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void LoadSelections();
    
    // Generated message map functions
    //{{AFX_MSG(CSelectionNameDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
    afx_msg void OnDblclkSellist();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTIONNAMEDLG_H__58344911_A400_46E2_8158_8E3D930E0C62__INCLUDED_)
