#pragma once

#include <el/BTree/BTree.h>

class ProxyCacheItem: public RStringI
{
public:
  ProxyCacheItem():RStringI() {}
  ProxyCacheItem(const char *name):RStringI(name) {}
  SRef<LODObject> _object;
};

class ProxyCache
{
  RString _root;
  BTree<ProxyCacheItem> _cache;
public:
  ProxyCache(void);
  ~ProxyCache(void);

  void Init(RString root) {_root=root;}
  void Reset() {_cache.Clear();}

  ObjectData *GetBestProxyObject(const char *proxyName, float curResolution);
  ObjectData *GetBestProxyObject(const NamedSelection *proxySel, float curResolution);
  virtual RString ProxyNotFound(RString name) {return RString();};
};

/*
class ProxyVisInfo: public RStringI
{  
public:
  bool visState;

  ProxyVisInfo():RStringI() {}
  ProxyVisInfo(const char *name):RStringI(name) {}

};

class ProxyVisibility
{
  BTree<ProxyVisInfo> _visinfo;
public:
  bool IsProxyVisible(const char *name);
  void Reset() {_visinfo.Clear();}
  void SetProxyVisible(const RString &name, bool state);
};

class ProxyFaceInfo
{
  RString proxyName;
  bool visible;
  Matrix4 proxyMatrix;
public:
  ProxyFaceInfo(RString proxyName, bool visible, const Matrix4 proxyMatrix):
      proxyName(proxyName),visible(visible),proxyMatrix(proxyMatrix) {}

      RString GetProxtName() {return proxyName;}
      bool IsVisible() {return visible;}
      void SetVisible(bool visible) {this->visible=visible;}
      const Matrix4 &GetProxyMatrix();
      ObjectData *GetObject(ProxyCache &cache, float resolution) 
        {return cache.GetbestProxyObject(proxyName,resolution);
}

TypeIsMovable(SRef<ProxyFaceInfo>);

class ProxyFaceFastSearch
{
  AutoArray<SRef<ProxyFaceInfo> > _proxyFaceList;

  void *syncid1;
  int syncid2;
public:
  void UpdateDependency(const ObjectData &fromObj);
}*/