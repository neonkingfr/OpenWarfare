#if !defined(AFX_TEXPREWWIN_H__15B8E202_56BE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_TEXPREWWIN_H__15B8E202_56BE_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TexPrewWin.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTexPrewWin window

class CTexPrewWin : public CWnd
  {
  // Construction
  LPCTSTR winclass;
  BITMAPINFO *bitmap;
  public:
    CTexPrewWin();
    
    // Attributes
  public:
    
    // Operations
  public:
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTexPrewWin)
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
    void Create(char *fullfilename);
    virtual ~CTexPrewWin();
    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CTexPrewWin)
    afx_msg void OnPaint();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXPREWWIN_H__15B8E202_56BE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
