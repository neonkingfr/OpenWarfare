#pragma once

#include "NewMAT/NewMatInterface.hpp"
#include "IPluginGeneral.h"
class CMainFrame;

#include ".\plugingeneralappside.h"


class CMATEditorPlugin: public IMatAppSide
  {
  IMatPluginSide *_plugin;
  public:
    CMATEditorPlugin(void *matService);
    ~CMATEditorPlugin(void);

    IMatPluginSide &CallMat() {return *_plugin;}

    bool IsLoaded() {return  _plugin!=0;}

  protected:
    virtual void UseMaterial(const char *filename);
    virtual bool CanUseMaterial();
    virtual ObjectData *CreatePrimitive(const char *filename, const char *texture, const char *material);
    virtual ObjectData *CreatePrimitive(ObjPrimitiveType type, int segments,float scale, const char *texture, const char *material);
    virtual void DestroyPrimitive(ObjectData *obj);
  };
