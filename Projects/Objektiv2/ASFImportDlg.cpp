// ASFImportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "ASFImportDlg.h"
#include "ASFHierarchy.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CASFImportDlg dialog


CASFImportDlg::CASFImportDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CASFImportDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CASFImportDlg)
    scale = 0.1f;
    //}}AFX_DATA_INIT
    }










void CASFImportDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CASFImportDlg)
  DDX_Control(pDX, IDC_LIST1, list);
  DDX_Text(pDX, IDC_SCALE, scale);
  //}}AFX_DATA_MAP
  }










#ifndef ListView_SetCheckState
#define ListView_SetCheckState(hwndLV, i, fCheck) \
      ListView_SetItemState(hwndLV, i, \
      INDEXTOSTATEIMAGEMASK((fCheck)+1), LVIS_STATEIMAGEMASK)
#endif

BEGIN_MESSAGE_MAP(CASFImportDlg, CDialog)
  //{{AFX_MSG_MAP(CASFImportDlg)
  ON_WM_SIZE()
    ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST1, OnEndlabeleditList1)
      //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CASFImportDlg message handlers
    
    BOOL CASFImportDlg::OnInitDialog() 
      {
      CDialog::OnInitDialog();
      
      CRect rc;
      GetClientRect(&rc);
      sz=rc.Size();
      
      list.InsertColumn(0,WString(IDS_BONELENGTH),LVCFMT_RIGHT,80,0);
      list.InsertColumn(1,WString(IDS_BONENAME),LVCFMT_LEFT,200,1);
      ListView_SetExtendedListViewStyleEx(list,LVS_EX_FULLROWSELECT |LVS_EX_GRIDLINES |LVS_EX_CHECKBOXES,LVS_EX_FULLROWSELECT |LVS_EX_GRIDLINES |LVS_EX_CHECKBOXES );
      int cnt=asf->GetBoneCount();
      int i;
      for (i=0;i<cnt;i++)
        {
        SASFHierarchyItem &itm=asf->GetBone(i);
        list.InsertItem(i,WString("%.3f",itm.length),0);
        list.SetItemText(i,1,itm.name);
        ListView_SetCheckState(list,i,1);
        }
      return TRUE;  // return TRUE unless you set the focus to a control
      // EXCEPTION: OCX Property Pages should return FALSE
      }










void CASFImportDlg::OnSize(UINT nType, int cx, int cy) 
  {
  
  if (list.GetSafeHwnd())
    {
    if (cx<200) cx=200;
    if (cy<100) cy=100;
    int rx=cx-sz.cx;
    int ry=cy-sz.cy;
    HDWP hdwp=BeginDeferWindowPos(3);
    MoveWindowRel(list,0,0,rx,ry,hdwp);
    MoveWindowRel(*GetDlgItem(IDOK),rx,0,0,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDCANCEL),rx,0,0,0,hdwp);
    EndDeferWindowPos(hdwp);
    sz.cx=cx;
    sz.cy=cy;
    }
  CDialog::OnSize(nType, cx, cy);
  }










void CASFImportDlg::OnOK() 
  {
  int cnt=asf->GetBoneCount();
  int i;
  for (i=0;i<cnt;i++)
    {
    SASFHierarchyItem &itm=asf->GetBone(i);
    itm.process=ListView_GetCheckState(list,i)!=0;
    }  
  CDialog::OnOK();
  asf->SetMasterScale(scale);
  }










void CASFImportDlg::OnEndlabeleditList1(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
  
  float val;	
  if (pDispInfo->item.pszText==NULL || sscanf(pDispInfo->item.pszText,"%f",&val)!=1)
    *pResult=0;
  else
    {
    SASFHierarchyItem &itm=asf->GetBone(pDispInfo->item.iItem);
    itm.length=val;
    *pResult =1;
    }	  
  }










