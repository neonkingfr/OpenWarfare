// SelectSelectionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "SelectSelectionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectSelectionDlg dialog


CSelectSelectionDlg::CSelectSelectionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectSelectionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectSelectionDlg)
	vList = _T("");
	//}}AFX_DATA_INIT
}


void CSelectSelectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectSelectionDlg)
	DDX_Control(pDX, IDC_LIST, wList);
	DDX_LBString(pDX, IDC_LIST, vList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectSelectionDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectSelectionDlg)
	ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectSelectionDlg message handlers

BOOL CSelectSelectionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

    for (int i=0;i<MAX_NAMED_SEL;i++) 
      {
      const NamedSelection *sel=obj->GetNamedSel(i);
      if (sel) wList.AddString(sel->Name());
      }
    GetDlgItem(IDOK)->EnableWindow(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelectSelectionDlg::OnSelchangeList() 
{
GetDlgItem(IDOK)->EnableWindow(TRUE);
}
