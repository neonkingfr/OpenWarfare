// Interpolator.cpp: implementation of the CInterpolator class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "Interpolator.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInterpolator::CInterpolator(int b,int e,int s)
  {
  begin=b;
  end=e!=b?e-1:e;
  steps=s;
  Reset();
  }

//--------------------------------------------------

int CInterpolator::GetNextValue()
  {
  int ret=pos;
  akumulator+=diff;
  while (akumulator>diff)
    {
    akumulator-=steps;
    pos+=direction;
    }
  return ret;
  }

//--------------------------------------------------

void CInterpolator::Reset()
  {  
  diff=abs(end-begin);
  direction=(end>begin)-(end<begin);
  akumulator=0;
  pos=begin;
  }

//--------------------------------------------------

