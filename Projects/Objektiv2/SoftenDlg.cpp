// SoftenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "SoftenDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSoftenDlg dialog


CSoftenDlg::CSoftenDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CSoftenDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CSoftenDlg)
    actframe = TRUE;
    interpolate = TRUE;
    loop = FALSE;
    timerange = 1.0f;
    xval = 0.0f;
    yval = 0.0f;
    zval = 0.0f;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CSoftenDlg::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_TIMERANGE,IDC_XVALUE,IDC_YVALUE,IDC_ZVALUE,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CSoftenDlg)
  DDX_Check(pDX, IDC_ACTUALFRAME, actframe);
  DDX_Check(pDX, IDC_INTERPOLATE, interpolate);
  DDX_Check(pDX, IDC_LOOP, loop);
  DDX_Text(pDX, IDC_TIMERANGE, timerange);
  DDX_Text(pDX, IDC_XVALUE, xval);
  DDX_Text(pDX, IDC_YVALUE, yval);
  DDX_Text(pDX, IDC_ZVALUE, zval);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CSoftenDlg, CDialog)
  //{{AFX_MSG_MAP(CSoftenDlg)
  // NOTE: the ClassWizard will add message map macros here
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CSoftenDlg message handlers
    