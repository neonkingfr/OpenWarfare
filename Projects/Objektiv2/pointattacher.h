// PointAttacher.h: interface for the CPointAttacher class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_POINTATTACHER_H__4B48D45A_D64F_484C_8715_ACB4D557C466__INCLUDED_)
#define AFX_POINTATTACHER_H__4B48D45A_D64F_484C_8715_ACB4D557C466__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CPointAttacher  
  {
  CPoint *mspoints;
  int *indexes;
  int count;
  public:
    static float GetDistance2(CPoint &pt1, CPoint &pt2);
    int AttachMouse(CPoint &pt);
    void Create(ObjectData *odata, HMATRIX mm);
    void Reset();
    CPointAttacher();	
    virtual ~CPointAttacher();
  };

//--------------------------------------------------

#endif // !defined(AFX_POINTATTACHER_H__4B48D45A_D64F_484C_8715_ACB4D557C466__INCLUDED_)
