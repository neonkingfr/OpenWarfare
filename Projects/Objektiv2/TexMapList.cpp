// TexMapList.cpp: implementation of the CTexMapList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "TexMapList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexMapList::CTexMapList()
  {
  memset(list,0,sizeof(list));
  memset(lru,0,sizeof(lru));
  curlru=0;
  }

//--------------------------------------------------

CTexMapList::~CTexMapList()
  {
  for (int i=0;i<TEXMAXLOAD;i++) delete list[i];
  }

//--------------------------------------------------

CTextureMapping * CTexMapList::Alloc()
  {
  int last=0;
  int llru=lru[0];
  for (int i=1;i<TEXMAXLOAD;i++)	
    if (lru[i]<llru) 
      {llru=lru[i];last=i;}
  curlru++;
  delete list[last];
  list[last]=new CTextureMapping();
  lru[last]=curlru;
  return list[last];
  }

//--------------------------------------------------

void CTexMapList::Delete(CTextureMapping *mp)
  {
  int i;
  for (i=0;i<TEXMAXLOAD;i++)
    if (list[i]==mp) break;
  delete mp;
  if (i<TEXMAXLOAD)
    {
    list[i]=NULL;
    lru[i]=0;
    }
  }

//--------------------------------------------------

CTextureMapping * CTexMapList::HitTest(CPoint &pt)
  {
  static int ptt=0;
  int i=ptt;
  
  do
    {
    i++;
    if (i>=TEXMAXLOAD) i=0;
    if (list[i] && list[i]->TestPointInside(pt)) return list[ptt=i];
    }
  while (i!=ptt);
  return NULL;
  }

//--------------------------------------------------

void CTexMapList::Draw(CDC *dc,  CTextureMapping *start, bool select)
  {  
  bool draw=false;
  for (int i=0;i<2;i++)
    for (int j=0;j<TEXMAXLOAD;j++)
      if (list[j])
        {
        if (draw)
          if (!list[j]->hidden && list[j]->textureName!=NULL)list[j]->DrawTexture(dc);
        else if (select) list[j]->DrawBouding(dc);
        if (list[j]==start) draw=!draw;
        }
  }

//--------------------------------------------------

int CTexMapList::GetCount()
  {
  int suma=0;
  for (int i=0;i<TEXMAXLOAD;i++) if (list[i]) suma++;
  return suma;
  }

//--------------------------------------------------

//DEL void CTexMapList::Recalc(CGCamera2 camera)
//DEL   {
//DEL   
//DEL   }

CTextureMapping * CTexMapList::EnumTex(CTextureMapping *last)
  {
  int p;
  if (last==NULL) p=-1;
  else
    for (p=0;p<TEXMAXLOAD;p++) if (list[p]==last) break;
  for (p++;p<TEXMAXLOAD;p++) if (list[p]) return list[p];
  return NULL;
  }

//--------------------------------------------------

bool CTexMapList::CheckHitTest(CPoint &pt)
  {
  for (int i=0;i<TEXMAXLOAD;i++)
    if (list[i] && list[i]->TestPointInside(pt)) return true;
  return false;
  }

//--------------------------------------------------

void CTexMapList::ForceFastTex(bool force)
  {
  for (int i=0;i<TEXMAXLOAD;i++)
    if (list[i]) list[i]->forcefast=force;
  }

//--------------------------------------------------

