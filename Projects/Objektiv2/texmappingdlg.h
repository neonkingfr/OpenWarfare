#if !defined(AFX_TEXMAPPINGDLG_H__DE36631B_0550_4358_BA22_605D32761557__INCLUDED_)
#define AFX_TEXMAPPINGDLG_H__DE36631B_0550_4358_BA22_605D32761557__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TexMappingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTexMappingDlg dialog

class CTexMappingDlg : public CDialog
  {
  // Construction	
  public:
    CTexMappingDlg(const CTexMappingDlg &other);
    void CalcCylindricMappingToObject(ObjectData *odata);
    void CalcCylindricMapping(HMATRIX mat, HVECTOR vect, float& tu, float &tv, float& lastrad);
    void CalcSpehereMappingToObject(ObjectData *odata);
    void RemapTuTv(float &tu, float &tv);
    void CalcSpehereMapping(HVECTOR pos, HVECTOR pin, float& tu, float& tv,float &lastaz);
    CTexMappingDlg(CWnd* pParent = NULL);   // standard constructor
    ObjectData *zaloha;
    
    
    // Dialog Data
    //{{AFX_DATA(CTexMappingDlg)
    enum 
      { IDD = IDD_CYLINDRICMAPPING };
    CEdit	TVScaleVal;
    CEdit	TVOffsetVal;
    CEdit	TUScaleVal;
    CEdit	TUOffsetVal;
    CSliderCtrl	TVScale;
    CSliderCtrl	TVOffset;
    CSliderCtrl	TUScale;
    CSliderCtrl	TUOffset;
    BOOL	MirrorTu;
    BOOL	MirrorTv;
    BOOL	OneSing;
    BOOL	SwapTuTv;
    float	azimut;
    float	zenit;
    CString	TexName;
    //}}AFX_DATA
    float tuscale,tuoffset,tvscale,tvoffset;
    bool spehere;
    ObjectData *previewobject;
    HVECTOR pos;
    HVECTOR dir;
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTexMappingDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void UpdateFloatValues();
    static void SetFloatValueToStatic(CEdit& st, float value);
    static void SetSliderFloatValue(CSliderCtrl&slider, float value, bool log);
    static float GetSliderFloatValue(CSliderCtrl& slider, bool logval);
    
    // Generated message map functions
    //{{AFX_MSG(CTexMappingDlg)
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    virtual BOOL OnInitDialog();
    afx_msg void OnPreview();
    afx_msg void OnDestroy();
    afx_msg void OnSetTexture();
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXMAPPINGDLG_H__DE36631B_0550_4358_BA22_605D32761557__INCLUDED_)
