#include "stdafx.h"
#include ".\proxycache.h"
#include "..\ObjektivLib\ObjToolProxy.h"

ProxyCache::ProxyCache(void)
{
}

ProxyCache::~ProxyCache(void)
{
}


ObjectData *ProxyCache::GetBestProxyObject(const char *proxyName, float curResolution)
{
  Pathname pth(proxyName,Pathname(_root));
  pth.SetExtension(".p3d");
  ProxyCacheItem item(pth),*fnd;
  LODObject *lod;
  fnd=_cache.Find(item);
  if (fnd==0)
  {
    item._object=lod=new LODObject;    
    while (item._object->Load(pth,0,0)!=0) 
    {
      RString res=ProxyNotFound(RString(pth));      
      if (res=="" || res==pth) return 0;
      pth=res;
    }
    _cache.Add(item);
  }
  else
    lod=fnd->_object;
  int lev=lod->FindLevel(curResolution);
  if (lev==-1) lev=lod->ActiveLevel();
  if (lev==-1) return 0;
  return lod->Level(lev);
}

ObjectData *ProxyCache::GetBestProxyObject(const NamedSelection *proxySel, float curResolution)
{
  if (proxySel)
  {
    int offst=strlen(ObjToolProxy::ProxyPrefix);
    return GetBestProxyObject(proxySel->Name()+offst,curResolution);
  }
  return 0;
}


/*
bool ProxyVisibility::IsProxyVisible(const char *name)
{
  ProxyVisInfo item(name),*fnd;
  fnd=_visinfo.Find(item);
  if (fnd==0)
  {
    item.visState=false;
    _visinfo.Add(item);
    return false;
  }
  return fnd->visState;
}

void ProxyVisibility::SetProxyVisible(const RString &name, bool state)
{
  ProxyVisInfo item(name),*fnd;
  fnd=_visinfo.Find(item);
  if (fnd==0)
  {
    item.visState=state;
    _visinfo.Add(item);    
  }
  fnd->visState=state;
}

class ObjectDataSyncTester: public ObjectData
{
public:
  bool TestSync(void *sync1, int sync2) const
  {
    return (&_faces.operator [](0)==sync1 && _faces.Size()==sync2);
  }
  void *GetSync1() const {return (&_faces.operator [](0);}
  int GetSync2() const {return (&_faces.Size();}
};

void ProxyFaceFastSearch::UpdateDependency(const ObjectData &fromObj)
{
  const ObjectDataSyncTester &ss=fromObj.GetTool<const ObjectDataSyncTester>();
  if (s.TestSync(syncid1,syncid2)) return;
  _proxyFaceList.Clear();
  _proxyFaceList.Access(ss.NFaces()-1);
  
  ObjToolProxy &proxtool=odata->GetTool<ObjToolProxy>();
  int proxCount=proxtool.EnumProxies();
  int proxyOffset=strlen(proxtool.ProxyPrefix);
  if (proxCount)
  {
    NamedSelection **proxyList=reinterpret_cast<NamedSelection **>(alloca(sizeof(NamedSelection *)*proxCount));
    proxtool.EnumProxies(proxyList);

    for (int i=0;i<proxCount;i++) 
    {
      int face=proxtool.FindProxyFace(proxyList[i]);
      Matrix4 trans;      
      FaceT fc(proxtool,face);
      proxtool.GetProxyTransform(fc,trans);
      _proxyFaceList[face]=new ProxyFaceInfo(proxyList[i]->Name(),false
  
    }
}*/