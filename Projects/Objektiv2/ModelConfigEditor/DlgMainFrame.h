// DlgMainFrame.h : header file
//

#pragma once

#include "ModelConfig.h"
#include "afxwin.h"
#include "ListBoxMCE.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "SafeEntryRef.h"
#include "afxwin.h"
#include "TimeLineControl.h"
#include "controlList.h"
#include "ControlerListWnd.h"


#define VIEW_CONTROLERS 0
#define VIEW_SELECTIONS 1
#define VIEW_SELECTIONHIERARCHY 2
#define VIEW_CLASSES 3
#define VIEW_CLASSHIERARCHY 4


// DlgMainFrame dialog
class DlgMainFrame : public CDialog, public IPropShEvent, public TimeLineControlEvent,public ControlerListWndEvent
{
// Construction
public:
	DlgMainFrame(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MODELCONFIGEDITOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

    ModelConfig _modelCfg;
    ParamFile _config;
    CString _modelCfgFname;
    CString _curModelName;
    PropSheetWindow _propWindow;
    int _lastSel;
    bool _bigLogFile;
    int _logFileSz;
    DWORD _commandId; //used to store last command id for UNDO
    bool _disabled;
    bool _allowMofidyParent;
    ControlList _cntrList;
    CCriticalSection _cntrListLock;
    bool _animated;
    bool _tmlineanimated;
    
    CWinThread *_animThread;

// Implementation
protected:
	HICON m_hIcon;
    AutoArray<ContentInfo> _selectionList;
    AutoArray<ContentInfo> _selectionListH;
    AutoArray<ContentInfo> _classList;
    AutoArray<ContentInfo> _classListH;
    AutoArray<ContentInfo> _controlList;
    AutoArray<ContentInfo> *_currentList;
    unsigned long _listDirtyFlags;
    SafeEntryRef _currentClass;
    RString _currentSelection;
    RString _currentControlerFilter;
    bool _autodetectFilter;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()


    void ListAllClasses(AutoArray<ContentInfo> &content, bool hieararchy);
    void ListAllSelections(AutoArray<ContentInfo> &content, bool hierarchy, bool selonly=false);

    void UpdateListModels();
    static void CorrectClassName(CString &name);
    
public:
    bool OpenModelConfig(CString fname);
  CListBoxMCE wBrowseList;
  void UpdateBrowser(bool setdirty=false);
  CTabCtrl wBrowserTab;
  afx_msg void OnTcnSelchangeBrowser(NMHDR *pNMHDR, LRESULT *pResult);
  CComboBox wModelList;
  TimeLineControl wTimeLineControl;
  ControlerListWnd wAllAnimations;

  afx_msg void OnCbnSelchangeModellist();
  afx_msg void OnCbnKillfocusModellist();
  afx_msg void OnButtonAdd();

  void UpdateWholeDialog()
  {
    _listDirtyFlags=0;
    UpdateBrowser();
  }
protected:
  virtual void OnOK();
public:
  void ListControlers(AutoArray<ContentInfo> & context);
  void EditClass(ParamClassPtr pclass);
  afx_msg void OnLbnSelchangeBrowselist();
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  void SetActiveView(int view);

  void Action(PropShItem *item,const ActionData &action);
  void OnGotoClass();
  void OnApply();
  static int FindInContainer(AutoArray<ContentInfo> content, const tchar *name, int type, int after=-1);

  void CleanClassForSaving(ParamClassPtr pclass, ParamClassPtr searchIn);
  void SavePanelData();
  void SaveConfigFile(ParamFile *pfile);
  void CreateSelectionList(AutoArray<SkeletonInfo> &list);
  
  void EditSelection(const CString & selName);
  void SavePanelSelection();
  void ShowControlerDesc(const CString &controler);

  void OnGotoSelection(const char *name=0);
  void OnDelete();
  void OnAddClassFromSel();
  void OnApplyAndNew();
  
  CString DefaultClassName(CString base);
  CString DefaultSelectionName(CString base);

  void FillComboByDerivedClasses(ParamClassPtr cls, ParamClassPtr der, AutoArray<RString> &list);
  afx_msg void OnAddmenuClass();
  void RecalcLayout(int cx, int cy);
  afx_msg void OnSize(UINT nType, int cx, int cy);
  void DlgMainFrame::OnGotoDerived(const tchar *name);
  afx_msg void OnFileExit();
  afx_msg void OnFileOpen();
  afx_msg void OnInsertModeldefinition32795();
  afx_msg void OnTexteditorEditall();
  afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
  afx_msg void OnTexteditorEditSpecific(UINT id);
  afx_msg void OnFiltercontrolersShowall();
  afx_msg void OnFiltercontrolersShowSpecific(UINT id);
  afx_msg void OnModeltypeAutodetect();
  afx_msg LRESULT OnLogMessage(WPARAM wParam, LPARAM lParam);

  void AutodetectModelType();
  afx_msg void OnDeleteModeldefinition32800();
  void OnReportEntry();
  CEdit wLogFile;

  afx_msg void OnEditUndo32793();
  afx_msg void OnFileReload();
  void AddNewModelDefinition(CString &name);

  afx_msg void OnAddmenuSelection();
  afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
  afx_msg void OnGotoListCmd(UINT cmd);
  afx_msg void OnDeleteItem();
  void Reset();

  bool LoadModelConfig(CString name);
  void DisableMCE(bool disable);
  bool IsMCEDisabled() {return _disabled;}
protected:
  virtual void OnCancel();
public:
  afx_msg void OnDestroy();
  void SwitchToModel(CString name);
  void AddBones(AutoArray<RString> &boneList);
  bool CanHandleRequestNow();
  void CleanUp();

  afx_msg void OnOptionsAllowmodifyparentconfigs();
  afx_msg void OnOptionsDefinetexteditor();

private:
  template<class Array>
  bool EnumSelectedControlers(Array &controlers) const;

  static UINT CALLBACK AnimationThread(LPVOID data);
  void ThreadAnimation();

public:
  virtual void SetPosition(float pos);
  virtual void SetPingpong(bool enabled);
  virtual void SetAutoAnim(bool enabled);
  void UpdateTimeLine(bool askMinMax=false);
  virtual void StopAllAnimations();
  virtual void ChangeSpeed(float factor);
  virtual void ResetAllAnimations();

  virtual void StartAnimThread();
  virtual void UpdateController(ControlInfoItem *cntr);


  afx_msg void OnTimer(UINT nIDEvent);
  void AfterViewerUpdated();
  afx_msg void OnPopupSlower();
  afx_msg void OnPopupFaster();
  afx_msg void OnPopupResetall();
  afx_msg void OnPopupResetspeed();
  afx_msg void OnPopupStopall();
  afx_msg void OnPopupResetselected();
  afx_msg void OnPopupAllanimations();
};


#define ICON_SELECTION 3
#define ICON_CLASS 4
#define ICON_CONTROLER 5
#define ICON_UNKCONTROLER 6