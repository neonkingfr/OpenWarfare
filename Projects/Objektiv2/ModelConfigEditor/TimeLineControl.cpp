// TimeLineControl.cpp : implementation file
//

#include "stdafx.h"
#include "ModelConfigEditor.h"
#include "TimeLineControl.h"
#include ".\timelinecontrol.h"


// TimeLineControl dialog

IMPLEMENT_DYNAMIC(TimeLineControl, CDialog)
TimeLineControl::TimeLineControl(TimeLineControlEvent *event, CWnd* pParent /*=NULL*/)
	: CDialog(TimeLineControl::IDD, pParent),_event(event)
{
  ASSERT(event!=0);
}

TimeLineControl::~TimeLineControl()
{
  if (GetSafeHwnd()) DestroyWindow();
}

void TimeLineControl::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_PINGPONGMODE, wPingPingMode);
  DDX_Control(pDX, IDC_PLAYBUTTON, wPlayMode);
  DDX_Control(pDX, IDC_SLIDER1, wSliderPos);
}


BEGIN_MESSAGE_MAP(TimeLineControl, CDialog)
  ON_WM_SIZE()
  ON_BN_CLICKED(IDC_PINGPONGMODE, OnBnClickedPingpongmode)
  ON_BN_CLICKED(IDC_PLAYBUTTON, OnBnClickedPlaybutton)
  ON_WM_HSCROLL()
END_MESSAGE_MAP()


// TimeLineControl message handlers

void TimeLineControl::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType, cx, cy);

  if (wPlayMode.GetSafeHwnd()==0)
  {
    lastSz.cx=cx;
    lastSz.cy=cy;
    return;
  }

  CRect bt1,bt2,sld;;
  wPlayMode.GetWindowRect(&bt1);
  wPingPingMode.GetWindowRect(&bt2);
  wSliderPos.GetWindowRect(&sld);
  ScreenToClient(&bt1);
  ScreenToClient(&bt2);
  ScreenToClient(&sld);
  if (lastSz.cx-sld.right+5>=cx) return;

  HDWP dwp=BeginDeferWindowPos(5);
  DeferWindowPos(dwp,wPlayMode,0,cx-(lastSz.cx-bt1.left),bt1.top,0,0,SWP_NOZORDER|SWP_NOSIZE);
  DeferWindowPos(dwp,wPingPingMode,0,cx-(lastSz.cx-bt2.left),bt2.top,0,0,SWP_NOZORDER|SWP_NOSIZE);
  DeferWindowPos(dwp,wSliderPos,0,sld.left,sld.top,(cx-(lastSz.cx-sld.right))-sld.left,sld.Size().cy,SWP_NOZORDER);
  EndDeferWindowPos(dwp);
  lastSz.cx=cx;
  lastSz.cy=cy;
  wSliderPos.SetRange(0,1000,TRUE);
  wSliderPos.SetTicFreq(250);
}

BOOL TimeLineControl::OnInitDialog()
{
  CDialog::OnInitDialog();


  HICON icon1=(HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_PLAYICON),IMAGE_ICON,16,16,LR_SHARED);
  HICON icon2=(HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_PINGPONGICON),IMAGE_ICON,16,16,LR_SHARED);
  wPlayMode.SetIcon(icon1);
  wPingPingMode.SetIcon(icon2);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void TimeLineControl::OnBnClickedPingpongmode()
{
  bool state=wPingPingMode.GetCheck()!=0;
  _event->SetPingpong(state);
}

void TimeLineControl::OnBnClickedPlaybutton()
{
  bool state=wPlayMode.GetCheck()!=0;
  _event->SetAutoAnim(state);
}


void TimeLineControl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  CSliderCtrl *what=(CSliderCtrl *)pScrollBar;
  if (what==&wSliderPos && nSBCode!=SB_ENDSCROLL)
  {
    nPos=wSliderPos.GetPos();
    _event->SetPosition((float)nPos/1000.0f*(_max-_min)+_min);
  }

  CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}


void TimeLineControl::SetMinMax(float min, float max)
{
  if (_max-_min<0.000001) {_max-=0.5;_min+=0.5;}
  _min=min;
  _max=max;
  _nolimit=(_max-_min)>200;
  if (_nolimit) 
  {
    float center=_max/2+_min/2;
    _min=center-200;
    _max=center+200;
  }
}

void TimeLineControl::SetPosition(float pos)
{
  int x=toLargeInt((pos-_min)/(_max-_min)*1000.0f);
  wSliderPos.SetPos(x);
  
}

void TimeLineControl::SetMarkRange(float min, float max)
{
  int xl=toLargeInt((min-_min)/(_max-_min)*1000.0f);
  int xr=toLargeInt((max-_min)/(_max-_min)*1000.0f);
  wSliderPos.SetSelection(xl,xr);
  wSliderPos.Invalidate();
}

void TimeLineControl::Enable(bool enable)
{
  wPingPingMode.EnableWindow(enable);
  wPlayMode.EnableWindow(enable);
  wSliderPos.EnableWindow(enable);
}

bool TimeLineControl::IsPingPongActive()
{
  return wPingPingMode.GetCheck()!=0;
}

void TimeLineControl::SetButtonAnimate(bool animate)
{
  wPlayMode.SetCheck(animate);
}

void TimeLineControl::SetButtonPingPong(bool pingpong)
{
  wPingPingMode.SetCheck(pingpong);
}

BOOL TimeLineControl::PreTranslateMessage(MSG* pMsg)
{
  if (pMsg->message==WM_MOUSEWHEEL && wPlayMode.GetCheck())
  {
    CRect rc;
    GetWindowRect(rc);
    CPoint pt(GetMessagePos());
    if (rc.PtInRect(pt)) 
    {
      float ff=(float)((signed short)HIWORD(pMsg->wParam))/(float)WHEEL_DELTA;
      float fff=pow(1.1f,fabs(ff));
      if (ff>0) fff=1.0f/fff;
      _event->ChangeSpeed(fff);
      return TRUE;
    }
    return FALSE;
  }
  if (IsDialogMessage(pMsg)) return TRUE;
  
  return CDialog::PreTranslateMessage(pMsg);
}
