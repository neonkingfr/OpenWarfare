#include "StdAfx.h"
#include ".\safeentryref.h"
#include "ParamEntry_DirectAccess.h"

SafeEntryRef::SafeEntryRef(void)
{
}

SafeEntryRef::~SafeEntryRef(void)
{
}

void SafeEntryRef::SetRef(ParamEntryPtr entry)
{
  if (entry.IsNull()) {_path=RString();return;}
  ParamEntryPtr stack[256];
  int sz=0;
  if (entry.GetPointer()!=entry.GetClass()) stack[sz++]=entry;
  while (entry.GetClass()->GetClassInterface()->GetParent())
  {
    stack[sz++]=entry.GetClass();
    entry=ParamEntryPtr(entry.GetClass()->GetParent(),entry.GetClass());
  }
  int needsz=0;
  for (int i=0;i<sz;i++) needsz+=stack[i]->GetName().GetLength()+1;
  char *p=_path.CreateBuffer(needsz);
  
  for (int i=sz-1;i>=0;i--)
  {
    strcpy(p,stack[i]->GetName());
    p=strchr(p,0);
    if (i!=0) *p++='\n';
  }    
}


ParamEntryPtr SafeEntryRef::GetRef(const ParamFile &relativeTo) const
{
  if (_path[0]==0) return ParamEntryPtr();
  char *temp=(char *)alloca(_path.GetLength()+1);
  const char *p=_path;  
  ParamClassPtr parent=&relativeTo;
  while (parent)
  {
    char *n=const_cast<char *>(strchr(p,'\n'));
    if (n==0) 
    {
      strcpy(temp,p);
      if (temp[0]==0) return ParamEntryPtr();
      ParamEntryPtr entry=parent->FindEntry(temp);
      return entry;
    }
    else 
    {
      strncpy(temp,p,n-p);
      temp[n-p]=0;
      p=n+1;
      if (temp[0])
      {
        ParamEntryPtr entry=parent->FindEntry(temp);
        parent=entry->GetClassInterface();
      }
    }
  }
  return ParamEntryPtr();
}

void SafeEntryRef::EntryRenamed(const RString &newname)
{
  const char *n=strrchr(_path,'\n');
  if (n!=0) n++;else n=_path;
  int offset=n-_path;
  _path=RString(_path.Mid(0,offset),newname);
}

static unsigned int wrapId=0;
bool SafeEntryRef::ImportEntry(ParamFile &relativeTo) const
{
  if (_path[0]==0) return false;
  char *temp=(char *)alloca(_path.GetLength()+1);
  const char *p=_path;  
  ParamClassPtr parent=&relativeTo;
  while (parent)
  {
    char *n=const_cast<char *>(strchr(p,'\n'));
    if (n==0) 
    {
      strcpy(temp,p);
      if (temp[0]==0) return ParamEntryPtr();
      ParamEntryPtr entry=parent->FindEntry(temp);
      if (entry.IsNull()) {parent->AddClass(temp,false,true);entry=parent->FindEntry(temp);}
      return true;
    }
    else 
    {      
      strncpy(temp,p,n-p);
      temp[n-p]=0;
      p=n+1;
      if (temp[0])
      {
        ParamEntryPtr entry=parent->FindEntry(temp);
        if (entry.IsNull()) {parent->AddClass(temp);entry=parent->FindEntry(temp);}
        else if (!entry->IsClass()) return false;
        else if (!entry->IsDefined())
        {
          char buff[50];
          sprintf(buff,"$$$%d_",wrapId++);
          static_cast<ParamClass_DirectAccess *>(entry.GetPointer())->SetName(RString(buff,temp));
          parent->AddClass(temp);
          entry->GetClassInterface()->SetBaseEx(temp);
          entry=parent->FindEntry(temp);
        }
        parent=entry->GetClassInterface();
      }
    }
  }
  return ParamEntryPtr();

}

static void CleanUpAfterImport2(ParamClassPtr root, bool &changed)
{
  AutoArray<RString>delClasses;
  for (int i=0;i<root->GetEntryCount();i++)
  {
    ParamEntryVal val=root->GetEntry(i);
    if (val.IsClass())
    {
      ParamClassPtr valclass=val.GetClassInterface();
      ParamEntryPtr e=valclass->FindBase();
      while (e.NotNull() && !e->IsDefined() && (e->GetClassInterface()->FindBase()).NotNull())
      {
        ParamClassPtr base=e->GetClassInterface();
        e=base->FindBase();
        changed=true;
        const_cast<ParamClass *>(val.GetClassInterface())->SetBaseEx(e->GetName());
        if (base->GetLockCount()==1)
        {
          ParamClassPtr owner=base->GetParent();
          RString basename=base->GetName();
          base=0;
          owner->Delete(basename);          
        }
      }
      if (valclass->IsDefined())
        CleanUpAfterImport2(val.GetClassInterface(),changed);
      else
        if (valclass->GetLockCount()<2)                  
          delClasses.Append(valclass->GetName());        
    }
  }
  for (int i=0,cnt=delClasses.Size();i<cnt;i++) root->Delete(delClasses[i]);
}

void SafeEntryRef::CleanUpAfterImport(ParamClassPtr visibleFor)
{
  bool changed;
  do
  {
    changed=false;
    ::CleanUpAfterImport2(visibleFor->GetRoot(),changed);
  }
  while (changed);
}