// DlgMainFrame.cpp : implementation file
//

#include "stdafx.h"

#include <el/interfaces/irocheck.hpp>
#include <el/interfaces/iScc.hpp>

#include "ModelConfigEditor.h"
#include "DlgMainFrame.h"
#include ".\dlgmainframe.h"
#include "DlgAskClassName.h"
#include "DlgAddModelDefinition.h"
#include <io.h>
#include "ParamEntry_DirectAccess.h"
#include <projects/ObjektivLib/IExternalViewer.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define BUTT_GOTOCLASS _T("gotoclass")
#define BUTT_GOTOSELECTION _T("gotogotoselection")
#define BUTT_APPLY _T("apply")
#define IL_DERIVE _T("derive")
#define IL_CLASSNAME _T("classname")
#define IL_DERIVEDCLS _T("derivedcls")
#define TEMPTY _T("")
#define GRP_CLASSCONTENT _T("classContent")
#define GRP_CLASSCONTENT_PTH _T(".classContent")

#define BUTT_DELETE _T("delete")

#define BUTT_ADDCLASSFROMSEL _T("addclassfromsel")
#define BUTT_APPLYANDNEW _T("applyandnew")
#define BUTT_GOTODERCLASS _T("gotoderived")
#define BUTT_REPORTENTRY _T("reportentry")

#define ID_TEXTEDIT_FIRSTFILE 100
#define ID_FILTERCONTROLERS_FIRST 200

#define UPDATE_ANIM_TIMER 1122

DlgMainFrame::DlgMainFrame(CWnd* pParent /*=NULL*/)
	: CDialog(DlgMainFrame::IDD, pParent),wTimeLineControl(this),wAllAnimations(_cntrList,this)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    _autodetectFilter=true;
    _bigLogFile=false;
    _logFileSz=-1;
    _disabled=false;
    _animThread=0;
    _animated=false;
}

void DlgMainFrame::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_BROWSELIST, wBrowseList);
  DDX_Control(pDX, IDC_BROWSER, wBrowserTab);
  DDX_Control(pDX, IDC_MODELLIST, wModelList);
  DDX_Control(pDX, IDC_LOGFILE, wLogFile);
}

BEGIN_MESSAGE_MAP(DlgMainFrame, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_NOTIFY(TCN_SELCHANGE, IDC_BROWSER, OnTcnSelchangeBrowser)
    ON_CBN_SELCHANGE(IDC_MODELLIST, OnCbnSelchangeModellist)
    ON_CBN_KILLFOCUS(IDC_MODELLIST, OnCbnKillfocusModellist)
    ON_LBN_SELCHANGE(IDC_BROWSELIST, OnLbnSelchangeBrowselist)
    ON_COMMAND(IDC_ADD, OnButtonAdd)
    ON_COMMAND(ID_ADDMENU_CLASS, OnAddmenuClass)
    ON_WM_SIZE()
    ON_COMMAND(ID_FILE_EXIT, OnFileExit)
    ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
    ON_COMMAND(ID_INSERT_MODELDEFINITION32795, OnInsertModeldefinition32795)
    ON_COMMAND(ID_TEXTEDITOR_EDITALL, OnTexteditorEditall)
    ON_COMMAND_RANGE(ID_TEXTEDIT_FIRSTFILE,ID_TEXTEDIT_FIRSTFILE+100,OnTexteditorEditSpecific)
    ON_COMMAND_RANGE(ID_FILTERCONTROLERS_FIRST ,ID_FILTERCONTROLERS_FIRST +100,OnFiltercontrolersShowSpecific)
    ON_WM_INITMENUPOPUP()
    ON_COMMAND(ID_FILTERCONTROLERS_SHOWALL, OnFiltercontrolersShowall)
    ON_COMMAND(ID_MODELTYPE_AUTODETECT, OnModeltypeAutodetect)
    ON_COMMAND(ID_DELETE_MODELDEFINITION32800, OnDeleteModeldefinition32800)
    ON_MESSAGE(WM_LOGMESSAGE, OnLogMessage)
    ON_COMMAND(ID_EDIT_UNDO32793, OnEditUndo32793)
    ON_COMMAND(ID_FILE_RELOAD, OnFileReload)
    ON_COMMAND(ID_ADDMENU_SELECTION, OnAddmenuSelection)
    ON_COMMAND_RANGE(ID_POPUP_GOTOSELECTIONLIST,ID_POPUP_GOTOCONTROLERS,OnGotoListCmd)
    ON_WM_CONTEXTMENU()
    ON_COMMAND(ID_DELETE_ITEM, OnDeleteItem)
    ON_WM_DESTROY()
//    ON_WM_CLOSE()
ON_COMMAND(ID_OPTIONS_ALLOWMODIFYPARENTCONFIGS, OnOptionsAllowmodifyparentconfigs)
ON_COMMAND(ID_OPTIONS_DEFINETEXTEDITOR, OnOptionsDefinetexteditor)
ON_WM_TIMER()
ON_COMMAND(ID_POPUP_SLOWER, OnPopupSlower)
ON_COMMAND(ID_POPUP_FASTER, OnPopupFaster)
ON_COMMAND(ID_POPUP_RESETALL, OnPopupResetall)
ON_COMMAND(ID_POPUP_RESETSPEED, OnPopupResetspeed)
ON_COMMAND(ID_POPUP_STOPALL, OnPopupStopall)
ON_COMMAND(ID_POPUP_RESETSELECTED, OnPopupResetselected)
ON_COMMAND(ID_POPUP_ALLANIMATIONS, OnPopupAllanimations)
END_MESSAGE_MAP()


// DlgMainFrame message handlers


BOOL DlgMainFrame::OnInitDialog()
{
	CDialog::OnInitDialog();



    Pathname pth;    
    HINSTANCE hInst=::AfxGetInstanceHandle();
    pth=pth.GetExePath(&hInst);
    pth.SetFilename("configModelEditor.cfg");
    _config.Parse(pth);


	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon



	// TODO: Add extra initialization here
//    OpenModelConfig(_T("P:\\ofp2\\Vehicles\\Land\\Wheeled\\MTVR\\model.cfg"));
    SRef<CImageList> imgList=new CImageList;
    imgList->Create(IDB_LISTICONS,16,0,RGB(255,0,255));
    wBrowseList.SetImageList(imgList);    
    CString title;
    title.LoadString(IDS_SORTBYSELECTIONS);
    wBrowserTab.InsertItem(0,title,0);
    title.LoadString(IDS_SORTBYSELECTIONHIERARCHY);
    wBrowserTab.InsertItem(1,title,0);
    title.LoadString(IDS_SORTBYCLASSES);
    wBrowserTab.InsertItem(2,title,0);
    title.LoadString(IDS_SORTBYCLASSESHIERARCHY);
    wBrowserTab.InsertItem(3,title,0);
    title.LoadString(IDS_CONTROLERLIST);
    wBrowserTab.InsertItem(4,title,0);
    wBrowserTab.SetCurSel(0);
    UpdateListModels();
    OnCbnKillfocusModellist();

    wTimeLineControl.Create(wTimeLineControl.IDD,this);
    wTimeLineControl.ShowWindow(SW_SHOW);

    RECT rc;
    GetDlgItem(IDC_CONFIGPANEL)->GetWindowRect(&rc);
    ScreenToClient(&rc);
    title.LoadString(IDS_PROPERTYWINDOWTITLE);
    _propWindow.Create(title,WS_CHILD|WS_VISIBLE,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,*this,WS_EX_CONTROLPARENT);
    _propWindow.SetCanvasSize(rc.right-rc.left,rc.bottom-rc.top);
    _propWindow.SetEventCatcher(*this);

    WINDOWPLACEMENT *wp;
    UINT wpsz;
    theApp.GetProfileBinary("Window","Placement",(LPBYTE *)(&wp),&wpsz);
    if (wp)
    {
      SetWindowPlacement(wp);
      delete [] (char *)wp;
    }

  _allowMofidyParent=false;

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void DlgMainFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
		CDialog::OnSysCommand(nID, lParam);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void DlgMainFrame::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR DlgMainFrame::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

  
bool DlgMainFrame::OpenModelConfig(CString fname)
{
  _modelCfgFname=fname;
  bool res=_modelCfg.Load(Pathname(fname));
  UpdateListModels();
  if (_autodetectFilter) AutodetectModelType();
  UpdateBrowser(true);
  return res;
}

class ClassesAddToListFunctor
{
  AutoArray<ContentInfo> &contentLB;
  bool hierarchy;
  ParamClassPtr ownerClass;
  bool isroot;
public:
  ClassesAddToListFunctor(AutoArray<ContentInfo> &contentLB,bool hierarchy, ParamClassPtr ownerClass, bool isroot):contentLB(contentLB),hierarchy(hierarchy),ownerClass(ownerClass),isroot(isroot) {}
  bool operator () (ParamEntryVal &entry) const
  {
    bool derived=ModelConfig::IsNotDefinedInClassFile(ownerClass,ParamEntryPtr(entry.GetClass(),const_cast<ParamEntry *>(entry.GetPointer())),false);
    if (entry.IsClass())
    {
      const ParamClass *cls=entry.GetClassInterface();
      if (isroot && (_stricmp(cls->GetName(),"CfgModels")==0 || _stricmp(cls->GetName(),"CfgSkeletons")==0)) return false;
        int insert;
        if (hierarchy)
        {
          ConstParamEntryPtr base=cls->FindBase();
          SafeEntryRef baseref=base?base->GetClassInterface():0;
          if (base.NotNull()) for (int i=0;i<contentLB.Size();i++)
            if (contentLB[i].entryInfo==baseref)
            {
              int j;
              for (j=i+1;j<contentLB.Size() && contentLB[j].ident<contentLB[i].ident;j++);
              contentLB.Insert(insert=j);
              ContentInfo &inserted=contentLB[j];
              inserted.ident=contentLB[i].ident+1;
              inserted.title=cls->GetName();
              inserted.entryInfo=cls;
              inserted.icon=ICON_CLASS;       
              inserted.palcolor=derived?1:0;
              goto end;
            }
        }
        insert=contentLB.Size();
        ContentInfo &newone=contentLB.Append();
        newone.entryInfo=entry.GetClassInterface();;
        newone.title=entry.GetName();          
        newone.icon=ICON_CLASS;   
        newone.ident=0;
        newone.palcolor=derived?1:0;
end:;
        ParamEntryPtr ptrentry=entry.FindEntry("selection");
        if (ptrentry.NotNull())
        {
          RString value=ptrentry->GetValue();
          if (value!=RString())
          {
            ContentInfo &insel=contentLB.Insert(insert+1);
            insel.ident=contentLB[insert].ident+1;
            insel.icon=ICON_SELECTION;
            insel.title=CharToTCHAR(value);
            insel.bold=1;
            insel.palcolor=derived?1:0;
          }
        }
      
    }
    return false;
  }

};

void DlgMainFrame::ListAllClasses(AutoArray<ContentInfo> &content,bool hierarchy)
{  
  content.Clear();  
  ParamClassPtr ptr=_modelCfg.FindModelDefinition(&RString(TCHARToChar(_curModelName)));
  if (ptr.IsNull()) return;
  ParamEntryPtr entry=ptr->FindEntry("Animations");
  if (entry.NotNull() && entry->IsClass())
  {
    entry->GetClassInterface()->ForEachEntry(ClassesAddToListFunctor(content,hierarchy,entry->GetClassInterface(),false));    
    entry->GetClassInterface()->GetRoot()->ForEachEntry(ClassesAddToListFunctor(content,hierarchy,entry->GetClassInterface(),true));
  }
  
}

__declspec(noinline)
static void RStringToCString(const RString &rstring, CString &str)
{
  str=CharToTCHAR(rstring);
}

static int FindSelectionParent(const AutoArray<ContentInfo>  &content, const RString &parent)
{
  if (parent.GetLength()==0) return -1;
  CString cparent;
  RStringToCString(parent,cparent);
  for (int i=0,cnt=content.Size();i<cnt;++i)
  {
    if (content[i].icon==3 && _tcsicmp(cparent,content[i].title)==0) 
      return i+1;
  }
  return -1;
}

static int SortByHierarchy(AutoArray<SkeletonInfo *> *helparr)
{
  int cycles=0;
  for (int i=0,cnt=helparr->Size();i<cnt;++i)
  {
    SkeletonInfo *nfo=helparr->operator[] (i);
    if (nfo->parent[0]==0) continue;
    for (int j=i+1;j<cnt;++j)
    {
      if (_stricmp(helparr->operator[](j)->selName,nfo->parent)==0)
      {
        helparr->Append()=nfo;
        helparr->Delete(i);
        i--;
        cycles++;
        if (cycles>helparr->Size()) 
          return helparr->Size();
        break;
      }
    }
  }
  return 0;
}

TypeIsSimpleZeroed(SkeletonInfo *);

void DlgMainFrame::ListAllSelections(AutoArray<ContentInfo>  &content, bool hierarchy, bool selonly)
{
  content.Clear();
  BTree<SkeletonInfo> list;  
  RString modelName(TCHARToChar(_curModelName));
  _modelCfg.CreateSelectionList(list,&modelName);
  BTreeIterator<SkeletonInfo> iter(list);
  iter.Reset();
  AutoArray<ParamClassPtr> classes;    
  AutoArray<SkeletonInfo *> helparr; 
  helparr.Reserve(list.Size()+1,list.Size()+1);
  SkeletonInfo *item;  
  while (item=iter.Next())  helparr.Append()=item;
  int err;
  if (hierarchy)  
  {
    err=SortByHierarchy(&helparr);
    if (err)
    {
      err--;
      CString s;
      AfxFormatString1(s,IDS_UNABLETOBUILDHIERARCHY,helparr[err]->selName);
      AfxMessageBox(s,MB_ICONEXCLAMATION);
    }
  }
  for (int i=0,cnt=helparr.Size();i<cnt;++i)  
  {    
    item=helparr[i];
    int insertPos=hierarchy?FindSelectionParent(content,item->parent):-1;
    int ident=insertPos<0?0:content[insertPos-1].ident+1;
    if (insertPos<0) insertPos=content.Size();
    
    ContentInfo &cnfo=content.Insert(insertPos++);
    RStringToCString(item->selName,cnfo.title);   
    cnfo.ident=ident;
    cnfo.icon=ICON_SELECTION;
    cnfo.bold=true;
    cnfo.palcolor=item->derived?1:0;
    if (!selonly)
    {
      classes.Clear();
      _modelCfg.EnumerateClassesForSelection(item->selName, classes,&modelName);
      for (int i=0;i<classes.Size();i++)
      {      
        ContentInfo &cnfo=content.Insert(insertPos++);
        ParamClassPtr &cls=classes[i];
        cnfo.entryInfo=cls.GetPointer();
        cnfo.ident=ident+1;
        cnfo.icon=ICON_CLASS;      
        cnfo.title=classes[i]->GetName();
        ConstParamEntryPtr base=cls->FindBase();
        while (!base.IsNull())
        {
          ContentInfo &cnfo=content.Insert(insertPos++);
          ParamClassPtr &cls=classes[i];
          cnfo.entryInfo=base;
          cnfo.ident=ident+2;
          cnfo.icon=ICON_CLASS;
          cnfo.title=base->GetName();
          base=base->FindBase();
        }
      }
    }
  }
}

static void UpdateChangesInList(AutoArray<ContentInfo> &list, AutoArray<ContentInfo> &by)
{  
  int ll=0,bb=0;
  while (ll<list.Size() || bb<by.Size())
  {
    if (ll==list.Size()) {list.Append()=by[bb++];ll++;}
    else if (bb==by.Size()) list.Delete(ll);
    else if (list[ll].icon==by[bb].icon && list[ll].title==by[bb].title && list[ll].entryInfo==by[bb].entryInfo && list[ll].ident==by[bb].ident)
    {
      list[ll].bold=by[bb].bold;
      list[ll].palcolor=by[bb].palcolor;
      ll++;
      bb++;
    }
    else 
    {
      int i;
      bool found=false;
      for (i=ll;i<list.Size();i++)      
      {
        if (list[i].icon==by[bb].icon && list[i].title==by[bb].title && list[i].entryInfo==by[bb].entryInfo)
        {found=true;break;}
        if (list[i].ident<by[bb].ident)
        {found=false;break;}
      }
      if (!found)
      {
        list.Insert(ll,by[bb]);
        ll++;
        bb++;
      }
      else
      {
        while (i!=ll)
        {list.Delete(ll);i--;}

        list[ll].bold=by[bb].bold;
        list[ll].ident=by[bb].ident;
        list[ll].palcolor=by[bb].palcolor;

        bb++;
        ll++;
      }

    }
  }
}

void DlgMainFrame::UpdateBrowser(bool setdirty)
{
  if (setdirty) _listDirtyFlags =0;
  int i=wBrowserTab.GetCurSel();
  AutoArray<ContentInfo> *content;
  AutoArray<ContentInfo> newlist;
  switch (i)
  {
    case 4:
      if (~_listDirtyFlags & 0x10) {ListControlers(newlist);_listDirtyFlags|=0x10;UpdateChangesInList(_controlList,newlist);}      
      content=&_controlList;
      break;
    case 0: 
      if (~_listDirtyFlags & 0x1) {ListAllSelections(newlist,false);_listDirtyFlags|=0x1;UpdateChangesInList(_selectionList,newlist);}      
      content=&_selectionList;
      break;
    case 1: 
      if (~_listDirtyFlags & 0x2) {ListAllSelections(newlist,true,true);_listDirtyFlags|=0x2;      UpdateChangesInList(_selectionListH,newlist);}      
      content=&_selectionListH;
      break;
    case 2: 
      if (~_listDirtyFlags & 0x4) {ListAllClasses(newlist,false);_listDirtyFlags|=0x4; UpdateChangesInList(_classList,newlist);}                  
      content=&_classList;
      break;    
    case 3: 
      if (~_listDirtyFlags & 0x8) {ListAllClasses(newlist,true);_listDirtyFlags|=0x8;UpdateChangesInList(_classListH,newlist);}      
      content=&_classListH;
      break;
    default: return;
  }

  wBrowseList.SetContentInfo(content);
  wBrowseList.Update();
  CRect rc;
  wBrowserTab.GetWindowRect(&rc);
  ScreenToClient(&rc);
  wBrowserTab.AdjustRect(FALSE,&rc);
  wBrowseList.MoveWindow(&rc,TRUE);
  _currentList=content;
}

void DlgMainFrame::OnTcnSelchangeBrowser(NMHDR *pNMHDR, LRESULT *pResult)
{
  UpdateBrowser();
  UpdateTimeLine(false);
  *pResult = 0;
}

void DlgMainFrame::UpdateListModels()
{
  AutoArray<RString> list;
  _modelCfg.ListAllModels(list);
  CString name;
  CString curname;
  wModelList.GetWindowText(curname);
  wModelList.ResetContent();
  for (int i=0,cnt=list.Size();i<cnt;++i)
  {
    RStringToCString(list[i],name);
    wModelList.AddString(name);
  }
  if (curname.GetLength()!=0)
  {
    if (wModelList.FindStringExact(-1,curname)==-1)
      curname=_T("");
  }
  wModelList.SetWindowText(curname);
  if (wModelList.GetWindowTextLength()==0)
  {
    wModelList.SetWindowText(name);
    _curModelName=name;
  }
}

void DlgMainFrame::OnCbnSelchangeModellist()
{
    wModelList.GetLBText(wModelList.GetCurSel(),_curModelName);
    if (_autodetectFilter) AutodetectModelType();
    UpdateWholeDialog();
}

void DlgMainFrame::OnCbnKillfocusModellist()
{
    CString text;
    wModelList.GetWindowText(text);
    if (text!=_curModelName && text!="")
    {
      ParamClassPtr mdl=_modelCfg.FindModelDefinition(&RString(TCHARToChar(text)));
      if (mdl.IsNull())
      {
        if (AfxMessageBox(IDS_MODELNOTFOUND,MB_YESNO|MB_ICONQUESTION)==IDNO)
        {
          wModelList.SetWindowText(_curModelName);
          return;
        }
        else
        {
          AddNewModelDefinition(text);
          return;
        }
      }
      _curModelName=text;
      if (_autodetectFilter) AutodetectModelType();
       UpdateWholeDialog();    
    }

}

void DlgMainFrame::OnOK()
{
  // TODO: Add your specialized code here and/or call the base class
}

class EnumControlersFunctor
{
  RString filter;
  AutoArray<ContentInfo> &controlerList;  
public:
  EnumControlersFunctor(AutoArray<ContentInfo> &controlerList,RString filter):
    controlerList(controlerList),filter(filter) {}
  bool operator() (ParamEntryVal value) const
  {
    if (filter[0] && value.IsClass())
    {
      ParamEntryPtr e=value.GetClassInterface()->FindEntry("subjects");
      if (e.NotNull())
      {
        int i,cnt;
        for (i=0,cnt=e->GetSize();i<cnt;i++)
          if (e->operator [](i).GetValue()==filter) break;
        if (i==cnt) return false;
      }
    }
    ContentInfo &nfo=controlerList.Append();
    nfo.title=value.GetName();
    nfo.icon=ICON_CONTROLER;
    nfo.bold=true;
    return false;
  }


};

static RString GetBoneSource(const ParamEntry *boneDef)
{
  RString res;
  if (boneDef && boneDef->IsClass())
  {
    ParamClassPtr pp=boneDef->GetClassInterface();
    ParamEntryPtr entry=pp->FindEntry("source");
    if (entry.NotNull())
      res=entry->GetValue();
    if (res[0]==0)
    {
      entry=pp->FindEntry("selection");
      if (entry.NotNull())
        res=entry->GetValue();
    }
  }
  return res;
}

class EnumControlerAssigment
{
  AutoArray<ContentInfo> &controlerList;  
  ParamClassPtr owner;
public:
  EnumControlerAssigment(AutoArray<ContentInfo> &controlerList,ParamClassPtr owner):
    controlerList(controlerList),owner(owner) {}
  bool operator() (ParamEntryVal value) const
  {
    bool derived=ModelConfig::IsNotDefinedInClassFile(owner,ParamEntryPtr(value.GetClass(),const_cast<ParamEntry *>(value.GetPointer())),false);
    RString svalue=GetBoneSource(value.GetPointer());
      if (svalue!=RString())
      {
        int i,cnt;
        for (i=0,cnt=controlerList.Size();i<cnt;i++)
        {
         if (controlerList[i].ident==0 && _stricmp(controlerList[i].title,svalue)==0)
           break;
        }
       if (i==cnt)
       {
            ContentInfo &newitem=controlerList.Insert(i);
            newitem.icon=ICON_UNKCONTROLER;
            newitem.bold=true;
            newitem.ident=0;
            newitem.title=svalue;
            cnt++;
       }
       if (i!=cnt)
       {
            int insertPos=1;
            ContentInfo &newitem=controlerList.Insert(i+insertPos++);
            newitem.icon=ICON_CLASS;
            newitem.ident=1;
            newitem.title=value.GetName();
            newitem.entryInfo=value.GetClassInterface();
            newitem.palcolor=derived?1:0;
            ConstParamEntryPtr base=value.GetClassInterface()->FindBase();
            while (!base.IsNull())
            {
              derived=ModelConfig::IsNotDefinedInClassFile(owner,base->GetClassInterface()->FindParent(),false);
              ContentInfo &cnfo=controlerList.Insert(i+insertPos++);
              cnfo.entryInfo=base;
              cnfo.ident=2;
              cnfo.icon=ICON_CLASS;
              cnfo.title=base->GetName();
              cnfo.palcolor=derived?1:0;
              base=base->FindBase();
            }
      
       }
      }      
  return false;
  }


};

void DlgMainFrame::ListControlers(AutoArray<ContentInfo> & context)
{
  context.Clear();
  ParamEntryPtr entry=_config.FindEntry("Controllers");
  if (entry.NotNull() && entry->IsClass() && entry->GetClassInterface()->IsDefined())
  {
    ParamClassPtr pclass=entry->GetClassInterface();
    pclass->ForEachEntry(EnumControlersFunctor(context,_currentControlerFilter));    
  }
  ParamClassPtr anim=_modelCfg.FindAnimationDefintion(_modelCfg.FindModelDefinition(&RString(TCHARToChar(_curModelName))));
  if (!anim.IsNull())
  {
    anim->ForEachEntry(EnumControlerAssigment(context,anim));
  }
}

class FillClassesCombobox
{
  RString filter;
  PropShComboEdit *_combo;
public:
  FillClassesCombobox(PropShComboEdit *combo,RString filter):_combo(combo),filter(filter) {}
  bool operator() (ParamEntryVal val) const
  {
    if (val.IsClass())
    {
      if (filter[0] )
      {
        ParamEntryPtr e=val.GetClassInterface()->FindEntry("subjects");
        if (e.NotNull())
        {
          int i,cnt;
          for (i=0,cnt=e->GetSize();i<cnt;i++)
            if (e->operator [](i).GetValue()==filter) break;
          if (i==cnt) return false;
        }
      }
    _combo->AddItem(CharToTCHAR(val.GetName()));
    }
    return false;
  }
};

class CreateFormUsingConfig
{
  PropShGroupWithSwitches  *group;  
  ParamClassPtr valueClass;
  mutable AutoArray<bool> deriveMarks;
  AutoArray<SkeletonInfo> &sellist;
  RString filter;
  
public:
  CreateFormUsingConfig(PropShGroupWithSwitches  *grp,ParamClassPtr valueClass, AutoArray<SkeletonInfo> &sellist,RString filter):group(grp),valueClass(valueClass),sellist(sellist),filter(filter) {}
  bool operator() (ParamEntryVal val) const;
  void UpdateCheckboxes(int offset);
};

static PropShSliderLine::Mode SliderMode(const char *name)
{
  if (_stricmp(name,"Default")==0) return PropShSliderLine::Default;
  else if (_stricmp(name,"CheckRange")==0) return PropShSliderLine::CheckRange;
  else if (_stricmp(name,"Relative")==0) return PropShSliderLine::Relative;
  else if (_stricmp(name,"Wrap")==0) return PropShSliderLine::Wrap;
  else return PropShSliderLine::Default;
}

bool CreateFormUsingConfig::operator() (ParamEntryVal val) const
{
  if (val.IsClass())
  {
    ParamClassPtr pclass=val.GetClassInterface();
    if (pclass->IsDefined())
    {
      ParamEntryPtr valentry=valueClass->FindEntry(pclass->GetName());
      bool autoinit=true;

      RString type;
      RString title;      
      ParamEntryPtr entry=pclass->FindEntry("type");
      if (entry.NotNull())
      {
        type=entry->GetValue();
        entry=pclass->FindEntry("title");
        if (entry.NotNull())
        {
          title=entry->GetValue();
          ParamEntryPtr display=pclass->FindEntry("display");          
          PropShItem *itm;
          if (type==RString("select")) 
          {
            PropShComboEdit *cbx=new PropShComboEdit;
            itm=cbx;
            group->AddItem(cbx,CharToTCHAR(val.GetName()),CharToTCHAR(title));
            entry=pclass->FindEntry("source");
            if (entry->IsArray())
            {
              for (int i=0,cnt=entry->GetSize();i<cnt;i++)
              {
                RString value=entry->operator [](i);
                if (value==RString("!!none")) value=RString();
                cbx->AddItem(CharToTCHAR(value));
              }
            }
            else
            {
              RString val=entry->GetValue();
              if (val[0]=='*') //special source
              {
                if (_stricmp(val,"*selections")==0)
                {
                  CString tmp;
                  for (int i=0,cnt=sellist.Size();i<cnt;++i)
                  {
                    RStringToCString(sellist[i].selName,tmp);
                    cbx->AddItem(tmp);
                  }
                }
              }
              else
              {
                entry=pclass->GetRoot()->FindEntry(val);
                if (entry.NotNull() && entry->IsClass())
                {
                  entry->GetClassInterface()->ForEachEntry(FillClassesCombobox(cbx,filter));
                }
              }
            }                        
          }          
          else if (type==RString("checkbox")) 
          {
            itm=new PropShCheckBox(PropShCheckBox::Flat);
            group->AddItem(itm,CharToTCHAR(val.GetName()),CharToTCHAR(title));
            if (valentry.NotNull())
            {
              bool res=*valentry;
              itm->SetBinaryData(&res,sizeof(res));
            }
            autoinit=false;
          }
          else if (type==RString("float") || type==RString("angle")) 
          {             
            itm=0;
            if (display.NotNull())
            {
              ParamEntry &dispInfo=*display;
              if (dispInfo[0].GetValue()==RString("slider") && dispInfo.GetSize()>=6)            
                itm=new PropShSliderLine(dispInfo[1],dispInfo[2],dispInfo[3],dispInfo[4],dispInfo[5],dispInfo.GetSize()>6?SliderMode(dispInfo[6].GetValue()):PropShSliderLine::Default);
            }
            if (itm==0)
              itm=new PropShTextLine();
            group->AddItem(itm,CharToTCHAR(val.GetName()),CharToTCHAR(title));
            if (valentry.NotNull())
            {
              float val=*valentry;
              if (type==RString("angle")) val=val*180.0f/3.1415926535f;
              tchar *buff=(tchar *)alloca(100);
              _stprintf(buff,"%g",val);
              itm->SetStringData(buff);
            }
            autoinit=false;
          }
          if (autoinit)
          {
            if (valentry.NotNull())
              itm->SetStringData(CharToTCHAR(valentry->GetValue()));
          }
          bool isinherit=valueClass->FindEntryNoInheritance(pclass->GetName()).IsNull();
          deriveMarks.Append()=!isinherit;
/*          PropShCheckBox *chkbox=new PropShCheckBox();
          group->AddItem(chkbox,CharToTCHAR(RString("?",val.GetName())),_T(""));*/
/*          chkbox->SetBinaryData(&isinherit,sizeof(isinherit));*/
        }
      }
    }
  }
  return false;
}

void CreateFormUsingConfig::UpdateCheckboxes(int offset)
{
  for (int i=0,cnt=deriveMarks.Size();i<cnt;++i)
  {
    group->EnableItem(i+offset,deriveMarks[i]);
  }
}

static ParamClassPtr GetFormDef(ParamFile &config, ParamClassPtr pclass, ParamClassPtr searchIn, const tchar *formtype=0)
{
  ParamClassPtr formDef;
  RString type;
  ParamEntryPtr entry=searchIn->FindEntry("type");
  if (formtype) type=TCHARToChar(formtype);
  else if (entry.NotNull()) type=entry->GetValue();

  entry=config.FindEntry("AnimationTypes");
  if (entry.NotNull() && entry->IsClass()) 
  {
    ParamClassPtr atypes=entry->GetClassInterface();
    if (type!=RString()) entry=atypes->FindEntry(type);else entry=ParamEntryPtr();
    if (entry.IsNull() || !entry->IsClass())
      entry=config.FindEntry("MinimalForm");
    if (entry.IsNull() || !entry->IsClass())
    {
      AfxMessageBox(IDS_INVALIDCONFIG_NOMINIMALFORM);
      return formDef;
    }
    formDef=entry->GetClassInterface();    
  }
  else
    {
      AfxMessageBox(IDS_INVALIDCONFIG_ANIMTYPES);
      return formDef;
    }
  return formDef;
}

void DlgMainFrame::CreateSelectionList(AutoArray<SkeletonInfo> &skellist)
{
  BTree<SkeletonInfo> list;  
  RString modelName(TCHARToChar(_curModelName));
  _modelCfg.CreateSelectionList(list,&modelName);
  BTreeIterator<SkeletonInfo> iter(list);
  skellist.Reserve(list.Size(),list.Size());
  SkeletonInfo *item;  
  while (item=iter.Next())  skellist.Append()=*item;
}

static void SetPropWindowFont(CWnd *wnd, PropSheetWindow &propWindow)
{
    LOGFONT lg;
    wnd->GetFont()->GetLogFont(&lg);
    if (lg.lfHeight<0) lg.lfHeight-=3;else lg.lfHeight+=3;
    propWindow.SetFont(lg);
}

void DlgMainFrame::EditClass(ParamClassPtr pclass)
{
  _currentSelection=RString();
  _propWindow.DeleteAllItems();
  CString title;
  _propWindow.SetItemProperty(PROPSH_TITLESIZE,150.0f);
  title.LoadString(IDS_PROPITEM_CLASSNAME);    
  PropShTextLine *txt;
  _propWindow.AddItem(txt=new PropShTextLine(),IL_CLASSNAME,title);
  txt->SetStringData(CharToTCHAR(pclass->GetName()));
  title.LoadString(IDS_PROPITEM_DERIVEDFROM);
  PropShComboEdit *combo=new PropShComboEdit;
  _propWindow.AddItem(combo,IL_DERIVE,title);
  combo->SetStringData(CharToTCHAR(pclass->GetBaseName()));
  ParamClassPtr anim=_modelCfg.FindAnimationDefintion(_modelCfg.FindModelDefinition(&RString(TCHARToChar(_curModelName))));
  while (!anim.IsNull())  
  {
    anim->ForEachEntry(FillClassesCombobox(combo,""));
    anim=anim->GetParent();
  }
    
  title.LoadString(IDS_GOTOCLASS);
  _propWindow.AddItem(new PropShButton(PropShButton::AlignRight,100),BUTT_GOTOCLASS,title);

  PropShFrameGroup *vgroup=new PropShFrameGroup;
  _propWindow.AddItem(vgroup,TEMPTY,TEMPTY);

  PropShGroupWithSwitches *grid=new PropShGroupWithSwitches ;
  vgroup->AddItem(grid,GRP_CLASSCONTENT,TEMPTY);
  
  ParamClassPtr formDef=GetFormDef(_config,pclass,pclass);
  if (formDef.IsNull()) return;

  AutoArray<SkeletonInfo> selectionList;
  CreateSelectionList(selectionList);
  CreateFormUsingConfig hlp(grid,pclass,selectionList,_currentControlerFilter);    

    formDef->ForEachEntry(hlp);
/*    grid->HideCheckbox(0,true);
    grid->HideCheckbox(1,true);
    grid->HideCheckbox(2,true);*/
    hlp.UpdateCheckboxes(0);

  PropShGridGroup *grid1=new PropShGridGroup(5,0,true);
  _propWindow.AddItem(grid1,TEMPTY ,TEMPTY );
  title.LoadString(IDS_APPLYBUTTON);
  grid1->AddItem(new PropShButton(PropShButton::AlignRight,0.999f),BUTT_APPLY,title);
  title.LoadString(IDS_DELETEBUTTON);
  grid1->AddItem(new PropShButton(PropShButton::AlignRight,0.999f),BUTT_DELETE,title);  
  title.LoadString(IDS_REPORTENTRY);
  grid1->AddItem(new PropShButton(PropShButton::AlignRight,0.999f),BUTT_REPORTENTRY,title);  
    
  title.LoadString(IDS_CLASSESDERIVEDFROM);
  PropShPanelGroup *panel=new PropShPanelGroup(true);
  _propWindow.AddItem(panel,TEMPTY,title);
  PropShGridGroup *gg=new PropShGridGroup(3,0,true);
  panel->AddItem(gg,TEMPTY,TEMPTY);
  BTreeIterator<ClassRelationItem> bit(_modelCfg.GetRelationDb());
  bit.BeginFrom(ClassRelationItem(pclass,ParamClassPtr(),ClassRelationItem::Derived));
  ClassRelationItem *itm;
  CString name;  
  while ((itm=bit.Next()) && itm->_mainClass==pclass && itm->_type==itm->Derived)
  {
    RStringToCString(itm->_relatedClass->GetName(),name);
    gg->AddItem(new PropShButton(PropShButton::AlignRight,0.999f),BUTT_GOTODERCLASS+name,name);
  }
  if (name.GetLength()==0)
    _propWindow.DeleteItem(panel);
    


  SetPropWindowFont(this,_propWindow);

  CRect rc;
  GetClientRect(rc);
  RecalcLayout(rc.right,rc.bottom);
  _propWindow.RecalcLayout();
  _currentClass=pclass->GetClassInterface();
  wTimeLineControl.Enable(true);
  UpdateTimeLine(true);
}

void DlgMainFrame::OnLbnSelchangeBrowselist()
{
  int sel=wBrowseList.GetCaretIndex();
  bool cleanDlg=true;
  sel=wBrowseList.MapIndex(sel);
  if (sel!=_lastSel)
  {
    _lastSel=sel;
    ParamClassPtr selClass;
    if (sel!=-1) 
      {
        AutoArray<ContentInfo> *nfo=wBrowseList.GetContentInfo();
        if (nfo && sel<nfo->Size())
        {
          ParamEntryPtr entry=nfo->operator [] (sel).entryInfo(_modelCfg);
          if (entry.NotNull())
          {
            selClass=entry->GetClassInterface();
            EditClass(selClass);
            cleanDlg=false;
          }
        }
        if (selClass.IsNull())
        {
          ContentInfo &cfo=nfo->operator [] (sel);
          if (cfo.icon==ICON_SELECTION)
          {
            EditSelection(cfo.title);
            cleanDlg=false;
          }
          if (cfo.icon==ICON_CONTROLER || cfo.icon==ICON_UNKCONTROLER)
          {
            ShowControlerDesc(cfo.title);
            cleanDlg=false;
          }
        }
      }
    if (cleanDlg)
      {_propWindow.DeleteAllItems();_propWindow.RecalcLayout();}    
      
  }
}

BOOL DlgMainFrame::PreTranslateMessage(MSG* pMsg)
{  
  if (_propWindow.IsMyMessage(pMsg)==TRUE) return TRUE;
  if (pMsg->message==WM_LBUTTONDBLCLK && pMsg->hwnd==wLogFile)
  {
    _bigLogFile=!_bigLogFile;
    CRect rc;
    GetClientRect(rc);
    RecalcLayout(rc.right,rc.bottom);
    return TRUE;
  }
  if (wTimeLineControl.PreTranslateMessage(pMsg)) return TRUE;
  if (wAllAnimations.GetSafeHwnd() && wAllAnimations.PreTranslateMessage(pMsg)) return TRUE;
  return CDialog::PreTranslateMessage(pMsg);
}


void DlgMainFrame::Action(PropShItem *item, const IPropShEvent::ActionData &action)
{
  const tchar *name=item->GetName();
  if (_tcscmp(name,BUTT_GOTOCLASS)==0) OnGotoClass();
  else if (_tcscmp(name,BUTT_APPLY)==0) OnApply();
  else if (_tcscmp(name,BUTT_GOTOSELECTION)==0) OnGotoSelection();
  else if (_tcscmp(name,BUTT_DELETE )==0) OnDelete();
  else if (_tcscmp(name,BUTT_ADDCLASSFROMSEL)==0) OnAddClassFromSel();
  else if (_tcscmp(name,BUTT_APPLYANDNEW )==0) OnApplyAndNew();
  else if (_tcscmp(name,BUTT_REPORTENTRY)==0) OnReportEntry();
  else if (_tcsncmp(name,BUTT_GOTODERCLASS,_tcslen(BUTT_GOTODERCLASS))==0)
    OnGotoDerived(name+_tcslen(BUTT_GOTODERCLASS));
  else ThrowEvent(item,this).Action(item,action);
}

void DlgMainFrame::OnGotoClass()
{
  PropShBase *itm=_propWindow.Find(IL_DERIVE);
  ASSERT(itm);
  
  int datasize=itm->GetDataSize()+1;
  tchar *buff=(tchar *)alloca(sizeof(tchar)*datasize);
  itm->GetStringData(buff,datasize);
  int f=FindInContainer(*_currentList,buff,ICON_CLASS, wBrowseList.MapIndex(wBrowseList.GetAnchorIndex()));
    if (f!=-1)
    {
      wBrowseList.SetActiveItem(f);
      _lastSel=f;
      EditClass(_currentList->operator[](f).entryInfo(_modelCfg)->GetClassInterface());
      return;
    }
  AfxMessageBox(IDS_CLASSNOTFOUND,MB_OK|MB_ICONEXCLAMATION);
}

void DlgMainFrame::SetActiveView(int view)
{
  wBrowserTab.SetCurSel(view);
  UpdateBrowser();
}

int DlgMainFrame::FindInContainer(AutoArray<ContentInfo> content, const tchar *name, int type, int after)
{  
  for (int i=after+1,cnt=content.Size();i<cnt;i++)
    if ( content[i].icon==type
      && _tcscmp(content[i].title,name)==0) return i;
  if (after>=0) return FindInContainer(content,name,type);
  return -1;
}

class CleanClassUsing
{
  ParamClassPtr pclass;
public:
  CleanClassUsing(ParamClassPtr pclass):pclass(pclass) {}
  bool operator() (ParamEntryVal val) const
  {
    pclass->Delete(val.GetName());
    return false;
  }
};

void DlgMainFrame::CleanClassForSaving(ParamClassPtr pclass, ParamClassPtr searchIn)
{
  ParamClassPtr formDef=GetFormDef(_config,pclass,searchIn);  
  if (formDef.IsNull()) return; 
  {
    formDef->ForEachEntry(CleanClassUsing(pclass));
  }
}

static CString GetStringDataFromPropItem(PropShBase *item)
{
  CString buff;
  int datasize=item->GetDataSize()+1;
  tchar *data=buff.GetBuffer(datasize);
  if (item->GetStringData(data,datasize)==false) {buff.ReleaseBuffer();return CString();}
  buff.ReleaseBuffer();
  return buff;
}

static CString GetStringDataFromPropItem(PropShBase *item, const tchar *name)
{
  PropShBase *fnd=item->Find(name);
  if (fnd==0) return CString();
  return GetStringDataFromPropItem(fnd);
}

class SaveClassFormValues
{
  PropShGroupWithSwitches *group;
  ParamClassPtr saveclass;
  ParamClassPtr baseclass;
  mutable AutoArray<RString> regSkeletons;
public:
  SaveClassFormValues(PropShGroupWithSwitches *group,  ParamClassPtr pclass, ParamClassPtr baseclass):
      group(group),saveclass(pclass),baseclass(baseclass) {}
  bool operator() (ParamEntryVal val) const;
  AutoArray<RString> &GetRegSelection() {return regSkeletons;}
};

bool SaveClassFormValues::operator() (ParamEntryVal val) const
{
  bool error=false;
  if (val.IsClass())
  {
    ParamClassPtr pclass=val.GetClassInterface();
    if (pclass->IsDefined())
    {
      RString name=pclass->GetName();
      ParamEntryPtr entry=pclass->FindEntry("type");
      if (entry.NotNull())
      {
        PropShBase *item=group->Find(CharToTCHAR(name));
        if (item && group->IsItemEnabled(item))
        {
          RString type=entry->GetValue();      
          ParamEntryPtr prevState=baseclass.IsNull()?0:baseclass->FindEntry(name);
          if (type==RString("checkbox"))
          {
            bool state;
            if (item->GetBinaryData(&state,1))          
            {
              int istate=state?1:0;
              if (prevState.IsNull() || prevState->GetInt()!=istate)
                saveclass->Add(name,state?1:0);
            }
            else
              error=true;
          }
          else if (type==RString("float") || type==RString("angle"))
          {
            CString data=GetStringDataFromPropItem(item);
            tchar *endptr;
            double val=_tcstod(data,&endptr);
            if (*endptr!=0)
            {
              RString thisState;
              if (type==RString("angle"))
                thisState=RString("rad (",TCHARToChar(data))+")";
              else
                thisState=TCHARToChar(data);
              if (prevState.IsNull() || prevState->GetValue()!=thisState)
                saveclass->Add(name,thisState);
            }
            else
            {
              float thisState;
              if (type==RString("angle"))
                thisState=(float)(val*3.1415926535/180.0);
              else
                thisState=(float)val;
              if (prevState.IsNull() || fabs((float)(*prevState)-thisState)>0.00001)
                saveclass->Add(name,thisState);
            }
          }
          else
          {
            CString data=GetStringDataFromPropItem(item);
            RString rData=TCHARToChar(data);
            if (prevState.IsNull() || prevState->GetValue()!=rData)
              saveclass->Add(name,rData);
            entry=pclass->FindEntry("registerAs");
            if (entry.NotNull())
            {
              RString val=entry->GetValue();
              if (_stricmp(val,"skeletonBone")==0)
                regSkeletons.Append()=rData;            
            }
          }
        }
      }
      else error=true;
    }
    else error=true;
  }
  else error;
  if (error)
  {
    CString error;
    AfxFormatString1(error,IDS_UNABLETOSAVEVALUE,CharToTCHAR(val.GetName()));
    AfxMessageBox(error,MB_OK|MB_ICONEXCLAMATION);
  }
  
  return false; //we are not done, get next item
}


void DlgMainFrame::SavePanelData()
{
  bool update=false;
  ParamEntryPtr curEntry=_currentClass(_modelCfg);
  if (curEntry.NotNull() && curEntry->IsClass())
  {
    ParamClassPtr curClass=curEntry->GetClassInterface();    
    PropShBase *datagroup=_propWindow.Find(GRP_CLASSCONTENT);
    if (datagroup)
    {
      PropShBase *type=datagroup->Find(_T("type"));
      if (type)
      {
        ParamClassPtr baseclass=0;
        ParamClassPtr cursaved=curClass;
        ParamClassPtr entry=_modelCfg.Transform(curClass);
        ParamClassPtr newentry=_modelCfg.CreateAnimClassNw(TCHARToChar(_curModelName),entry->GetName(),entry->GetName(),RString(),RString());
        bool newcreated=!newentry.IsNull();
        if (newcreated) {entry=newentry;_currentClass=newentry->GetClassInterface();}
        if (!entry.IsNull() && entry->IsClass())
        {      
          ParamClassPtr pclass=entry->GetClassInterface();
          CleanClassForSaving(pclass,curClass);        
        
          CString typen=GetStringDataFromPropItem(type);  
          ParamClassPtr formDef=GetFormDef(_config,curClass,curClass,typen);

          CString cname=GetStringDataFromPropItem(&_propWindow,IL_CLASSNAME);
          CorrectClassName(cname);
          const char *rname=TCHARToChar(cname);
          if (static_cast<ParamClass_DirectAccess *>(pclass.GetPointer())->SetName(rname)==false)          
            AfxMessageBox(IDS_UNABLERENAMECLASSEXISTS);                      
          else
            _currentClass.EntryRenamed(rname);

          CString derived=GetStringDataFromPropItem(&_propWindow,IL_DERIVE);
          RString rderived=TCHARToChar(derived);
          if (newcreated) {rderived=rname;baseclass=cursaved;}
          if (!pclass->SetBaseEx(rderived))
          {
            _modelCfg.ImportBaseClass(curClass->GetParent()->GetClassInterface(),pclass,rderived);
            if (!pclass->SetBaseEx(rderived))
              AfxMessageBox(IDS_UNABLETOSETBASECLASS);
            _modelCfg.CleanUpAfterImport(pclass);
          }          
          if (!formDef.IsNull())          
          {
            SaveClassFormValues f(static_cast<PropShGroupWithSwitches *>(datagroup),pclass,baseclass);
            formDef->ForEachEntry(f);          
            AutoArray<RString> &skeletons=f.GetRegSelection();
            for (int i=0,cnt=skeletons.Size();i<cnt;++i) 
              if (!_modelCfg.IsSelectionExists(skeletons[i],TCHARToChar(_curModelName)))
              _modelCfg.RegisterSelection(TCHARToChar(_curModelName),skeletons[i],skeletons[i],0);
          }
          _modelCfg.CleanUpAfterImport(pclass);
          SaveConfigFile(static_cast<ParamFile *>(const_cast<ParamClass *>(pclass->GetRoot()->GetClassInterface())));
          update=true;

        }
        else
          AfxMessageBox(IDS_CANNOTMODIFYCLASS_NOTRANSFORM);
      }
    }
  }
  if (update)
  {
      curEntry=0;
      _modelCfg.Update();
      UpdateBrowser(true);
  }
  
}

void DlgMainFrame::OnApply()
{
  if (!_currentClass.IsNull())
  {
    SavePanelData();
    if (_currentClass(_modelCfg).NotNull())
      EditClass(_currentClass(_modelCfg)->GetClassInterface());
  }
  if (_currentSelection[0])
  {
    SavePanelSelection();
    EditSelection(TCHARToChar(_currentSelection));
  }
  theApp.App()->ReloadViewer();
}

class DeleteAllBakFilesFunctor
{
public:
  bool operator()(SRef<ParamFile> &stage) const
  {
    RString name=stage->GetName();
    name=name+".$$$";
    DeleteFile(CharToTCHAR(name));
    return false;
  }
};

class ConfigIndexFunctor
{
  int &index;
  ParamFile *pf;
public:
  ConfigIndexFunctor(int &index,ParamFile *pf):index(index),pf(pf) {index=0;}
  bool operator()(SRef<ParamFile> &stage) const
  {
    if (stage.GetRef()==pf) return true;
    index++;
    return false;
  }

};

void DlgMainFrame::SaveConfigFile(ParamFile *pfile)
{
  if (GetCurrentMessage()->time!=_commandId)
  {
    _commandId=GetCurrentMessage()->time;
    _modelCfg.EnumConfigFiles(DeleteAllBakFilesFunctor());
  }
  int cfgindex;
  _modelCfg.EnumConfigFiles(ConfigIndexFunctor(cfgindex,pfile));
  if (cfgindex!=_modelCfg.CountConfigs()-1 && !_allowMofidyParent)
  {
    CString text;
    AfxFormatString1(text,IDS_UNABLETOSAVECONFIG_NOTALLOWED,CharToTCHAR(pfile->GetName()));
    AfxMessageBox(text);
    PostMessage(WM_COMMAND,ID_FILE_RELOAD,0);
    return;
  }
  RString name=pfile->GetName();
  ROCheckResult res=theApp.App()->TestFileRO(CharToTCHAR(name),ROCHF_DisableSaveAs|ROCHF_TrackFileUpdate,*this);
  
  if (res==ROCHK_FileRO || res==ROCHK_FileOKUpdated) 
  {
     PostMessage(WM_COMMAND,ID_FILE_RELOAD,0);
     return;
  }

  RString bakname=name+".$$$";
  LSError err;
  if (_access(name,02)==0)
  {
    MoveFile(CharToTCHAR(name),CharToTCHAR(bakname));
  
    CWaitCursor wt;
    for (int i=0;i<80;i++)
    {    
      err=pfile->Save();
      if (err!=LSAccessDenied) break;
      Sleep(100);
    }
  }
  else
    err=(LSError)1;
  if (err!=0)
  {
    CString msg;
    AfxFormatString1(msg,IDS_UNABLETOSAVEFILE,pfile->GetName());
    AfxMessageBox(msg,MB_ICONSTOP);    
    PostMessage(WM_COMMAND,ID_FILE_RELOAD,0);
  }
}

void DlgMainFrame::OnButtonAdd()
{
  CMenu mnu;
  mnu.LoadMenu(IDR_ADDMENU);
  CMenu *popup=mnu.GetSubMenu(0);
  CRect rc;
  GetDlgItem(IDC_ADD)->GetWindowRect(&rc);
  popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON,rc.left,rc.bottom,this,0);
}

/*
static void InvalidateMCEDataHelper(AutoArray<ContentInfo> &mcedata)
{
  for (int i=0,cnt=mcedata.Size();i<cnt;++i)
  {
    mcedata[i].entryInfo=0;
  }
}

void DlgMainFrame::InvalidateMCEData()
{
  InvalidateMCEDataHelper(_selectionList);
  InvalidateMCEDataHelper(_selectionListH);
  InvalidateMCEDataHelper(_classList);
  InvalidateMCEDataHelper(_classListH);
  InvalidateMCEDataHelper(_controlList);
}
*/
void DlgMainFrame::EditSelection(const CString & selName)
{
  _currentClass=0;
  
  _propWindow.DeleteAllItems();
  CString title;
  RString rSelName=TCHARToChar(selName);
  _currentSelection=rSelName;

  title.LoadString(IDS_SELECTIONNAME);
  PropShItem *itm;
  _propWindow.AddItem(itm=new PropShTextLine(),_T("selName"),title);
  title.LoadString(IDS_SELECTIONNAMEPARENT);
  PropShComboEdit *cbx;
  _propWindow.AddItem(cbx=new PropShComboEdit(),_T("selParent"),title);

  AutoArray<SkeletonInfo> selectionList;
  CreateSelectionList(selectionList);

  RString parentName;

  for (int i=0,cnt=selectionList.Size();i<cnt;++i)
  {
    cbx->AddItem(selectionList[i].selName);
    if (_stricmp(selectionList[i].selName,rSelName)==0) parentName=selectionList[i].parent;
  }
  
  itm->SetStringData(selName);  
  cbx->SetStringData(CharToTCHAR(parentName));

  title.LoadString(IDS_GOTOSELECTION);
  _propWindow.AddItem(new PropShButton(PropShButton::AlignRight,0.25f),BUTT_GOTOSELECTION,title);
  _propWindow.AddItem(new PropShTitleOnly(PropShTitleOnly::HorzLine),TEMPTY,TEMPTY);

  PropShGridGroup *grid1=new PropShGridGroup(5,0,true);
  _propWindow.AddItem(grid1,TEMPTY ,TEMPTY );
  title.LoadString(IDS_APPLYANDNEWBUTTON);
  grid1->AddItem(new PropShButton(PropShButton::AlignRight,0.999f),BUTT_APPLYANDNEW,title);
  title.LoadString(IDS_ADDCLASS);
  grid1->AddItem(new PropShButton(PropShButton::AlignRight,0.999f),BUTT_ADDCLASSFROMSEL ,title);
  title.LoadString(IDS_DELETEBUTTON);
  grid1->AddItem(new PropShButton(PropShButton::AlignRight,0.999f),BUTT_DELETE,title);  
  title.LoadString(IDS_APPLYBUTTON);
  grid1->AddItem(new PropShButton(PropShButton::AlignRight,0.999f),BUTT_APPLY,title);
  
  SetPropWindowFont(this,_propWindow);
  _propWindow.RecalcLayout();  
  wTimeLineControl.Enable(false);

}

void DlgMainFrame::SavePanelSelection()
{
  CString selName=GetStringDataFromPropItem(&_propWindow,_T("selName"));
  CString parent=GetStringDataFromPropItem(&_propWindow,_T("selParent"));
  
  AutoArray<SkeletonInfo> selectionList;
  CreateSelectionList(selectionList);

  RString rSelName=TCHARToChar(selName);
  RString rParent=TCHARToChar(parent);

  if (_stricmp(rSelName,_currentSelection))
  {
    for (int i=0;i<selectionList.Size();i++) if (!selectionList[i].derived)
      if (_stricmp(selectionList[i].selName,rSelName)==0)
      {
        AfxMessageBox(IDS_SELECTIONALREADYEXISTS);
          return;
      }
  }
  if (rParent[0])
  {
    int i;
    for (i=0;i<selectionList.Size();i++)
      if (_stricmp(selectionList[i].selName,rParent)==0) break;
    if (i==selectionList.Size()) 
    {
      AfxMessageBox(IDS_PARENTSELECTIONHASNOTBEENFOUND);
    }
  }
  ParamFile *pfile=_modelCfg.RegisterSelection(_curModelName,_currentSelection,rSelName,rParent);
  if (pfile==0) AfxMessageBox(IDS_CANNOTMODIFYSELECTION);
  else
  {
    _currentSelection=rSelName;
   _modelCfg.Update();
    UpdateBrowser(true);
   SaveConfigFile(pfile);
  }
}

void DlgMainFrame ::ShowControlerDesc(const CString &controler)
{
  _currentClass=0;
  _currentSelection=RString();
  _propWindow.DeleteAllItems();
  ParamEntryPtr entry=_config.FindEntry("Controllers");
  if (entry.NotNull() && entry->IsClass() && entry->IsDefined())
  {
    RString desc;
    AutoArray<RString> subjects;
    entry=entry->GetClassInterface()->FindEntry(TCHARToChar(controler));
    if (entry.NotNull())
    {
      ParamEntryPtr val=entry->FindEntry("description");
      desc=val->GetValue();
      val=entry->FindEntry("subjects");
      if (val->IsArray())
      {
        for (int i=0;i<val->GetSize();i++) subjects.Append()=val->operator[](i).GetValue();
      }
    }
    else
    {
      CString hlp;
      hlp.LoadString(IDS_UNKNOWCONTROLERDESC);
      desc=TCHARToChar(hlp);
      subjects.Append(desc);
    }
    CString title;
    title.LoadString(IDS_CONTROLERDESC);
    PropShFrameGroup *grp;
    _propWindow.AddItem(grp=new PropShFrameGroup(),TEMPTY,title);
    grp->AddItem(new PropShTitleOnly(PropShTitleOnly::AlignLeft),TEMPTY,CharToTCHAR(desc));
    title.LoadString(IDS_CONTROLERSUBJECTS);
    _propWindow.AddItem(grp=new PropShFrameGroup(),TEMPTY,title);
    for (int i=0,cnt=subjects.Size();i<cnt;++i)
      grp->AddItem(new PropShTitleOnly(PropShTitleOnly::AlignLeft),TEMPTY,CharToTCHAR(subjects[i]));
  }
   SetPropWindowFont(this,_propWindow);
 _propWindow.RecalcLayout();
 wTimeLineControl.Enable(true);
 UpdateTimeLine(true);


}

void DlgMainFrame::OnGotoSelection(const char *name)
{
  CString parent=name?name:GetStringDataFromPropItem(&_propWindow,_T("selParent"));
  if (parent.GetLength())
  {
    int f=FindInContainer(*_currentList,parent,ICON_SELECTION, wBrowseList.MapIndex(wBrowseList.GetAnchorIndex()));
    if (f!=-1)
    {
      wBrowseList.SetActiveItem(f);
      EditSelection(parent);
      _lastSel=f;
    }
    else
      AfxMessageBox(IDS_SELECTIONNOTFOUND);
  }
}

void DlgMainFrame::OnDelete()
{
  if (!_currentClass.IsNull())
  {    
    ParamFile *pfile=_modelCfg.DeleteAnimClass(_currentClass(_modelCfg));
    if (pfile==0) AfxMessageBox(IDS_CANNOTDELETECLASS);
    else
    {
      _currentSelection=RString();
      _modelCfg.Update();
      UpdateBrowser(true);
      SaveConfigFile(pfile);
    }
  } 
  if (_currentSelection[0])
  {
    ParamFile *pfile=_modelCfg.RegisterSelection(_curModelName,_currentSelection,0,0);
    if (pfile==0) AfxMessageBox(IDS_CANNOTMODIFYSELECTION);
    else
    {
      _currentSelection=RString();
      _modelCfg.Update();
      UpdateBrowser(true);
      SaveConfigFile(pfile);
    }
  } 
  _lastSel=-1;
  OnLbnSelchangeBrowselist();
}

void DlgMainFrame::OnAddClassFromSel()
{
  CString selName=GetStringDataFromPropItem(&_propWindow,_T("selName"));
  CString name=DefaultClassName(selName);
  CorrectClassName(name);
  ParamFile *p=_modelCfg.CreateAnimClass(TCHARToChar(_curModelName),
      TCHARToChar(name),"","",TCHARToChar(selName));
  if (p)
  {
      _modelCfg.Update();
      UpdateBrowser(true);
      SaveConfigFile(p);
      ParamClassPtr pclass=_modelCfg.FindAnimClass(TCHARToChar(_curModelName),TCHARToChar(name));
      EditClass(pclass);
  }
  else
    AfxMessageBox(IDS_UNABLETOCREATECLASS);

}

void DlgMainFrame::OnApplyAndNew()
{
      OnApply();      
      EditSelection(DefaultSelectionName(TEMPTY));
}

CString DlgMainFrame::DefaultClassName(CString base)
{
  AutoArray<ContentInfo> clslist;
  ListAllClasses(clslist, false);

  if (base.GetLength()==0) 
  {
    base.LoadString(IDS_UNNAMEDCLASS);  
    if (FindInContainer(clslist,base,ICON_CLASS,-1)==-1) return base;
  }
  int index=1;

  do
  {
    CString name;
    name.Format(_T("%s_%d"),base,index);
    if (FindInContainer(clslist,name,ICON_CLASS,-1)==-1) return name;
    index++;
  }
  while (true);
}

CString DlgMainFrame::DefaultSelectionName(CString base)
{
  AutoArray<ContentInfo> clslist;
  ListAllSelections(clslist, false, true);
  if (base.GetLength()==0) 
  {
    base.LoadString(IDS_UNNAMEDSELECTION);  
    if (FindInContainer(clslist,base,ICON_SELECTION,-1)==-1) return base;
  }

  int index=1;
  do
  {
    CString name;
    name.Format(_T("%s_%d"),base,index);    
    if (FindInContainer(clslist,name,ICON_SELECTION,-1)==-1) return name;
    index++;
  }
  while (true);
}

static void CalculateCommonName(CString &thsname, CString newname)
{
  const char *a=thsname, *b=newname;
  while (toupper(*a)==toupper(*b)) {a++;b++;}
  while (a>thsname.GetString() && a[-1]=='_') a--;
  thsname=thsname.Mid(0,a-thsname);  
}

void DlgMainFrame::OnAddmenuClass()
{
  CString derived;
  CString sel;
  CString controler;
  CString name;
  CString bname;
  ParamFile *pfile;

  ParamClassPtr model=_modelCfg.FindModelDefinition(&RString(TCHARToChar(_curModelName)));
  if (model.IsNull()) return;
  ParamClassPtr anim=_modelCfg.FindAnimationDefintion(model);
  if (anim.IsNull()) return;

  if (wBrowseList.GetSelCount()==0) return;
  if (wBrowseList.GetSelCount()==1)
  {
    int i=wBrowseList.GetAnchorIndex();
    i=wBrowseList.MapIndex(i);
    if (i!=-1)
      if ((*_currentList)[i].icon==ICON_CLASS)
        {bname=derived=(*_currentList)[i].title;}
      else if ((*_currentList)[i].icon==ICON_CONTROLER || (*_currentList)[i].icon==ICON_UNKCONTROLER)
        bname=controler=(*_currentList)[i].title;
      else if ((*_currentList)[i].icon==ICON_SELECTION)
        bname=sel=(*_currentList)[i].title;
    if (anim->FindEntryNoInheritance(TCHARToChar(bname)).NotNull())
      bname=DefaultClassName(bname);
  
    CorrectClassName(bname);
    pfile=_modelCfg.CreateAnimClass(TCHARToChar(_curModelName),
      TCHARToChar(bname),TCHARToChar(derived),TCHARToChar(controler),TCHARToChar(sel));        
  }
  else
  {
    bool first=true;
    for (int i=0,cnt=wBrowseList.GetCount();i<cnt;i++) if (wBrowseList.GetSel(i))      
    {
      int map=wBrowseList.MapIndex(i);
      if ((*_currentList)[map].icon!=ICON_CLASS)
      {
        if (first) {name=(*_currentList)[i].title;first=false;}
        else 
          {
            CalculateCommonName(name,(*_currentList)[i].title);
          }
      }
    }
    if (!first)
    {
      DlgAskClassName dlg;
      dlg.vClassName=name;
      if (dlg.DoModal()==IDCANCEL) return;
      name=dlg.vClassName;
      CorrectClassName(name);
      pfile=_modelCfg.CreateAnimClass(TCHARToChar(_curModelName),
        TCHARToChar(name),"","","");
      bname=name;      
    }
    for (int i=0,cnt=wBrowseList.GetCount();i<cnt;i++) if (wBrowseList.GetSel(i))      
    {
      int map=wBrowseList.MapIndex(i);
      if ((*_currentList)[map].icon!=ICON_CLASS)
      {        
        if ((*_currentList)[map].icon==ICON_CONTROLER || (*_currentList)[i].icon==ICON_UNKCONTROLER)
          name=controler=(*_currentList)[i].title;
        else if ((*_currentList)[map].icon==ICON_SELECTION)
          name=sel=(*_currentList)[map].title;
        if (anim->FindEntryNoInheritance(TCHARToChar(name)).NotNull())
          name=DefaultClassName(name);
        pfile=_modelCfg.CreateAnimClass(TCHARToChar(_curModelName),
          TCHARToChar(name),TCHARToChar(bname),TCHARToChar(controler),TCHARToChar(sel));        
      }
      else
      {
        name=(*_currentList)[map].title;
        if (bname.GetLength()==0) bname=name;
        CString bname=name;
        if (anim->FindEntryNoInheritance(TCHARToChar(name)).NotNull())
          name=DefaultClassName(bname);
      pfile=_modelCfg.CreateAnimClass(TCHARToChar(_curModelName),
        TCHARToChar(name),TCHARToChar(bname),"","");              
      }
    }
  }

  if (pfile)
  {
      model=0;
      anim=0;
      _modelCfg.Update();
      UpdateBrowser(true);
      SaveConfigFile(pfile);
      ParamClassPtr pclass=_modelCfg.FindAnimClass(TCHARToChar(_curModelName),TCHARToChar(bname));
      EditClass(pclass);
  }
  else
    AfxMessageBox(IDS_UNABLETOCREATECLASS);
}


void DlgMainFrame::RecalcLayout(int cx, int cy)
{
  CRect rc,rc2;
  if (_propWindow.GetWindowHandle()==0) return;
  ::GetWindowRect(_propWindow.GetWindowHandle(),&rc);
  ScreenToClient(&rc);
  CRect logwn;
  CRect timeln;
  wTimeLineControl.GetClientRect(&timeln);
  wLogFile.GetWindowRect(&logwn);
  ScreenToClient(&logwn);
  if (_logFileSz<0) _logFileSz=logwn.Size().cy;
  if (_bigLogFile) 
  {
    if (cy>500) logwn.top=250-cy+logwn.bottom;
    else logwn.top=cy/2-cy+logwn.bottom;
    if (wTimeLineControl.IsWindowVisible())
    {
      wTimeLineControl.ShowWindow(SW_HIDE);
    }
    timeln.bottom=0;
  }
  else
  {
    logwn.top=logwn.bottom-_logFileSz;
    if (!wTimeLineControl.IsWindowVisible())
    {
      wTimeLineControl.ShowWindow(SW_SHOW);
    }
  }
  int bottoffst=logwn.Size().cy+10;
  int xs=cx-rc.left-GetSystemMetrics(SM_CXHSCROLL);
  int ys=cy-bottoffst-rc.top-GetSystemMetrics(SM_CYVSCROLL)-timeln.bottom;
  _propWindow.LockResize(true);
  //::SetWindowPos(_propWindow.GetWindowHandle(),0,0,0,xs,ys,SWP_NOZORDER|SWP_NOMOVE);
  _propWindow.SetCanvasSize(xs,ys);
  _propWindow.RecalcLayout();    
  _propWindow.LockResize(false);
  PropShHDWP dwp(10);
  wBrowserTab.GetWindowRect(&rc);
  ScreenToClient(&rc);
  rc.bottom=cy-rc.left;
  dwp.SetWindowRect(wBrowserTab,rc);
  rc2=rc;
  wBrowserTab.AdjustRect(FALSE,&rc2);
  dwp.SetWindowRect(wBrowseList,rc2);
  if (!_bigLogFile) dwp.SetWindowPos(wTimeLineControl,logwn.left,cy-bottoffst+5-timeln.bottom,cx-logwn.left,timeln.bottom);
  dwp.SetWindowPos(wLogFile,logwn.left,cy-bottoffst+5,cx-logwn.left,logwn.Size().cy);
}

void DlgMainFrame::OnSize(UINT nType, int cx, int cy)
{
  __super::OnSize(nType, cx, cy);
  RecalcLayout(cx,cy);

  // TODO: Add your message handler code here
}

void DlgMainFrame::OnGotoDerived(const tchar *name)
{
  int f=FindInContainer(*_currentList,name,ICON_CLASS, wBrowseList.MapIndex(wBrowseList.GetAnchorIndex()));
    if (f!=-1)
    {
      wBrowseList.SetActiveItem(f);
      EditClass(_currentList->operator[](f).entryInfo(_modelCfg)->GetClassInterface());
      _lastSel=f;
      return;
    }
  AfxMessageBox(IDS_CLASSNOTFOUND,MB_OK|MB_ICONEXCLAMATION);
}
void DlgMainFrame::OnFileExit()
{
  OnCancel();
}

void DlgMainFrame::OnFileOpen()
{
  CString filter;
  filter.LoadString(IDS_MODELCFGFILTERS);
  CFileDialog fdlg(TRUE,0,_T("model.cfg"),OFN_HIDEREADONLY,filter);
  if (fdlg.DoModal()==IDOK)
  {
    if (LoadModelConfig(fdlg.GetPathName())==true)
      DisableMCE(false);
  }
}

static void DisplayMCFGError(ModelConfig::Error err, const tchar *param1=0, const tchar *param2=0)
{
  int msgid;
  switch(err)
  {
  case ModelConfig::errOk: return;
  case ModelConfig::errAlreadyExists: msgid=IDS_MCERR_ALREADYEXISTS;break;
  case ModelConfig::errCfgModelsNotFound: msgid=IDS_MCERR_CFGMODELSNOTFOUND;break;
  case ModelConfig::errClassNotFound: msgid=IDS_MCERR_CLASSNOTFOUND;break;
  case ModelConfig::errInUse: msgid=IDS_MCERR_CLASSINUSE;break;
  case ModelConfig::errNoTransform: msgid=IDS_MCERR_NOTRANSFORM;break;
  case ModelConfig::errUnableToImportBase: msgid=IDS_MCERR_UNABLETOIMPORTBASE;break;
  case ModelConfig::errUnableToSetBase: msgid=IDS_MCERR_UNABLETOSETBASE;break;
  case ModelConfig::errCfgSkeletonsNotFound: msgid=IDS_MCERR_CFGSKELETONNOTFOUND;break;
  default: msgid=IDS_MCERR_UNKNOWNERROR;break;
  }
  CString msg1;
  if (param1==0) msg1.LoadString(msgid);
  else if (param2==0) AfxFormatString1(msg1,msgid,param1);
  else  AfxFormatString2(msg1,msgid,param1,param2);
  CString msg2;
  AfxFormatString1(msg2,IDS_MCERR_REPORT,msg1);
  AfxMessageBox(msg2,MB_OK|MB_ICONEXCLAMATION);
}

  
void DlgMainFrame::OnInsertModeldefinition32795()
{
  AddNewModelDefinition(CString());
}

class EnumConfigs_StartEditor
{
public:
  bool operator() (SRef<ParamFile> &item) const
  {
    RString name=item->GetName();
    const tchar *tname=CharToTCHAR(name);
    SHELLEXECUTEINFO nfo;
    ZeroMemory(&nfo,sizeof(nfo));
    nfo.cbSize=sizeof(nfo);
    nfo.fMask=SEE_MASK_NOCLOSEPROCESS;
    nfo.lpFile=theApp.GetTxtEditor();
    nfo.lpParameters=tname;
    nfo.nShow=SW_SHOWNORMAL;    
    if (ShellExecuteEx(&nfo)==TRUE)
    {
      WaitForInputIdle(nfo.hProcess,INFINITE);
    }
    return false;
  }
};

class EnumConfigs_StartEditor2:public EnumConfigs_StartEditor
{
  int _index;
public:
  EnumConfigs_StartEditor2(int index):_index(index) {}
  int GetIndex() const {return _index;}
};

template<>
template<>
bool AutoArray<SRef<ParamFile>,MemAllocD>::ForEachF(const EnumConfigs_StartEditor2 &func)
{
  int index=func.GetIndex();
  if (index<0 || index>=Size()) return false;
  func(Set(Size()-index-1));
  return true;
}



class EnumConfigsForMenu
{
protected:
  mutable CMenu *_mnu;
  mutable int cntr;
public:
  EnumConfigsForMenu(CMenu *mnu):_mnu(mnu),cntr(1) {}
  bool operator() (SRef<ParamFile> &item) const
  {
    RString name=item->GetName();
    const tchar *tname=CharToTCHAR(name);
    CString text;
    if (cntr<10) text.Format(_T("&%d. %s"),cntr,tname);
    else if (cntr==10) text.Format(_T("&0. %s"),cntr,tname);
    else if (cntr>10 && cntr<=('Z'-'A'+11)) text.Format(_T("&%c. %s"),cntr+'A'-11,tname);
    else text.Format(_T("%s"),tname);
    _mnu->AppendMenu(MF_STRING, ID_TEXTEDIT_FIRSTFILE+cntr-1,text);
    cntr++;
    return false;
  }
};

template<>
template<>
bool AutoArray<SRef<ParamFile>,MemAllocD>::ForEachF(const EnumConfigsForMenu &func)
{
  for (int i=Size()-1; i>=0; i--)
  {
    if (func(Set(i)))
    {
      return true;
    }
  }
  return false;
}


class EnumFiltersForMenu: public EnumConfigsForMenu
{
  mutable RString added_filters;
  RString curFilter;
public:
  EnumFiltersForMenu(CMenu *mnu, RString curFilter):EnumConfigsForMenu(mnu),curFilter(curFilter) {}
  bool operator() (ParamEntryVal &item) const
  {
    if (item.IsClass() && item.GetClassInterface()->IsDefined())
    {
      ParamEntryPtr e=item.GetClassInterface()->FindEntry("subjects");
      if (e.NotNull() && e->IsArray())
      {
        for (int i=0;i<e->GetSize();i++)
        {
          RString name=e->operator [](i).GetValue();
          RString addName=RString(RString("~",name),"~");
          if (strstr(added_filters,addName)==0)
          {
            added_filters=RString(added_filters,addName);
            _mnu->AppendMenu(MF_STRING|(name==curFilter?MF_CHECKED:0),ID_FILTERCONTROLERS_FIRST+cntr,CharToTCHAR(name));
            cntr++;
          }
        }
      }
    }
    return false;
  }
};

void DlgMainFrame::OnTexteditorEditall()
{
  _modelCfg.EnumConfigFiles(EnumConfigs_StartEditor());
}

void DlgMainFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
  __super::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);

  if (pPopupMenu->GetMenuItemID(0)==ID_TEXTEDITOR_EDITALL)
  {
    while (pPopupMenu->GetMenuItemCount()>2) pPopupMenu->DeleteMenu(2,MF_BYPOSITION);
    _modelCfg.EnumConfigFiles(EnumConfigsForMenu(pPopupMenu));
  }
  if (pPopupMenu->GetMenuItemID(2)==ID_FILTERCONTROLERS_SHOWALL)
  {
    while (pPopupMenu->GetMenuItemCount()>3) pPopupMenu->DeleteMenu(3,MF_BYPOSITION);
    ParamEntryPtr e=_config.FindEntry("Controllers");
    if (e.NotNull() && e->IsClass() && e->IsDefined())
    {
      e->GetClassInterface()->ForEachEntry(EnumFiltersForMenu(pPopupMenu,_currentControlerFilter));
    }
    if (_currentControlerFilter[0]==0) pPopupMenu->CheckMenuItem(ID_FILTERCONTROLERS_SHOWALL,MF_CHECKED);
    else pPopupMenu->CheckMenuItem(ID_FILTERCONTROLERS_SHOWALL,MF_UNCHECKED);
    if (_autodetectFilter) pPopupMenu->CheckMenuItem(ID_MODELTYPE_AUTODETECT,MF_CHECKED);
    else pPopupMenu->CheckMenuItem(ID_MODELTYPE_AUTODETECT,MF_UNCHECKED);
  }
  for (int i=0,cnt=pPopupMenu->GetMenuItemCount();i<cnt;i++)
  {
    switch (pPopupMenu->GetMenuItemID(i))
    {
    case ID_OPTIONS_ALLOWMODIFYPARENTCONFIGS:
       pPopupMenu->CheckMenuItem(i,MF_BYPOSITION|(_allowMofidyParent?MF_CHECKED:MF_UNCHECKED));
      break;

    }
  }
}

void DlgMainFrame::OnTexteditorEditSpecific(UINT id)
{
  _modelCfg.EnumConfigFiles(EnumConfigs_StartEditor2(id-ID_TEXTEDIT_FIRSTFILE)); 
}
void DlgMainFrame::OnFiltercontrolersShowall()
{
  _currentControlerFilter="";
  UpdateBrowser(true);
}

static CMenu *FindIdMenu(CMenu *m, int id)
{
  for (int i=0,cnt=m->GetMenuItemCount();i<cnt;i++)
  {
    if (m->GetMenuItemID(i)==id) return m;
    CMenu *sub=m->GetSubMenu(i);
    if (sub) 
    {
      sub=FindIdMenu(sub,id);
      if (sub) return sub;
    }
  }
  return 0;
}

void DlgMainFrame::OnFiltercontrolersShowSpecific(UINT id)
{
  CMenu *p=GetMenu();
  p=FindIdMenu(p,id);
  if (p)
  {
    CString name;
    p->GetMenuString(id,name,MF_BYCOMMAND);
    _currentControlerFilter=TCHARToChar(name);
    UpdateBrowser(true);
  }
}
void DlgMainFrame::OnModeltypeAutodetect()
{
  _autodetectFilter=!_autodetectFilter;
}

void DlgMainFrame::AutodetectModelType()
{
  ParamClassPtr ptr=_modelCfg.FindModelDefinition(&RString(TCHARToChar(_curModelName)));
  if (!ptr.IsNull())
  {
    ParamEntryPtr e=_config.FindEntry("Autodetect");
    if (e.NotNull() && e->IsClass() && e->IsDefined())
    {
      ParamClassPtr autodetect=e->GetClassInterface();
      e=autodetect->FindEntry(ptr->GetName());
      while (e.IsNull() && !ptr.IsNull())
      {
        e=ptr->FindBase();
        if (e.IsNull()) 
        {
          ptr=ParamClassPtr();
          e=ParamEntryPtr();
        }
        else 
        {
          ptr=e->GetClassInterface();
          e=autodetect->FindEntry(ptr->GetName());
        }
      }
      if (e.NotNull()) 
        _currentControlerFilter=e->GetValue();
      else
        _currentControlerFilter="";
    }
  }
}
void DlgMainFrame::OnDeleteModeldefinition32800()
{
  if (AfxMessageBox(IDS_CONFIRMDELETEMODEL,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDNO) return;
  ParamFile *tosave=0;
  ModelConfig::Error err=_modelCfg.DeleteModelDef(TCHARToChar(_curModelName),&tosave);
  DisplayMCFGError(err,_curModelName);
  UpdateListModels();
  UpdateBrowser(true);
  if (tosave) SaveConfigFile(tosave);
}

LRESULT DlgMainFrame::OnLogMessage(WPARAM wParam, LPARAM lParam)
{
  bool *p=(bool *)wParam;
  const tchar *text=(const tchar *)lParam;
  int len=wLogFile.GetWindowTextLength();
  if (len+_tcslen(text)>32000)
  {
    wLogFile.SetSel(0,8100,TRUE);
    wLogFile.ReplaceSel(_T(""));
  }
  len=wLogFile.GetWindowTextLength();
  wLogFile.SetSel(len,len,TRUE);
  wLogFile.ReplaceSel(text);
  len=wLogFile.GetWindowTextLength();
  wLogFile.SetSel(len,len,FALSE);
  *p=true;
  return 0;
}

void DlgMainFrame::OnReportEntry()
{
  ParamClassPtr curClass=_currentClass(_modelCfg)->GetClassInterface();
  if (!curClass.IsNull())
  {
    LogF("-----------------------------");
    _modelCfg.ReportEntry(ParamEntryPtr(curClass.GetPointer(),curClass.GetPointer()));
    
  }
}



class UndoFilesFunctor
{
public:
  bool operator()(SRef<ParamFile> &stage) const
  {
    RString name=stage->GetName();
    RString bakname=name+".$$$";
    if (_access(CharToTCHAR(bakname),0)==0)
    {
      DeleteFile(CharToTCHAR(name));
      MoveFile(CharToTCHAR(bakname),CharToTCHAR(name));
    }
    return false;
  }
};


void DlgMainFrame::OnEditUndo32793()
{
  _modelCfg.EnumConfigFiles(UndoFilesFunctor());
  _modelCfg.Reload();
  UpdateListModels();
  UpdateBrowser(true);
}


void DlgMainFrame::OnFileReload()
{
  _modelCfg.Reload();
  UpdateListModels();
  UpdateBrowser(true);  
}

void DlgMainFrame::AddNewModelDefinition(CString &name)
{
  DlgAddModelDefinition dlg;
  dlg._cfgModels=_modelCfg.FindModelDefinitionClass();
  dlg.vDefName=name;
  dlg.vDerived=_curModelName;
  if (dlg.DoModal()==IDOK)
  {
    dlg._cfgModels=0;
    CorrectClassName(dlg.vDefName);
    ParamFile *toSave=0;
    ModelConfig::Error err;
    if (dlg.vClone)      
      err=_modelCfg.CreateCloneModelDef(TCHARToChar(dlg.vDefName),TCHARToChar(dlg.vDerived),&toSave);
    else
      err=_modelCfg.CreateNewModelDef(TCHARToChar(dlg.vDefName),TCHARToChar(dlg.vDerived),TCHARToChar(dlg.vSuffix),&toSave);
    DisplayMCFGError(err);
    if (err==ModelConfig::errOk && toSave) SaveConfigFile(toSave);
    wModelList.SetWindowText(_T(""));
    UpdateListModels();
    UpdateBrowser(true);    
  }
}
void DlgMainFrame::OnAddmenuSelection()
{
  EditSelection(DefaultSelectionName(TEMPTY));
}

void DlgMainFrame::OnContextMenu(CWnd* pWnd, CPoint point)
{
  if (pWnd==&wBrowseList)
  {
    CMenu mnu;
    mnu.LoadMenu(IDR_LISTMENU);
    CMenu *popup=mnu.GetSubMenu(0);
    CPoint ls=point;
    wBrowseList.ScreenToClient(&ls);
    BOOL outside;
    UINT item=wBrowseList.ItemFromPoint(ls,outside);
    if (!outside)
    {
      wBrowseList.SetSel(-1,FALSE);
      wBrowseList.SetSel(item,TRUE);
      wBrowseList.SetCaretIndex(item);
      wBrowseList.SetAnchorIndex(item);
      popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,this);
    }    
  }
  if (pWnd==&wTimeLineControl)
  {
    CMenu mnu;
    mnu.LoadMenu(IDR_ANIMMENU);
    CMenu *popup=mnu.GetSubMenu(0);
    popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,this);

  }
}

void DlgMainFrame::OnGotoListCmd(UINT cmd)
{
  int item=wBrowseList.GetCaretIndex();
  item=wBrowseList.MapIndex(item);
  if (item>=0)
  {
    ContentInfo nfo=_currentList->operator[](item);
    int mode;
    switch (cmd)
    {
      case ID_POPUP_GOTOSELECTIONLIST: mode=0;break;
      case ID_POPUP_GOTOSELECTIONHIERARCHY: mode=1;break;
      case ID_POPUP_GOTOCLASSLIST: mode=2;break;
      case ID_POPUP_GOTOCLASSHIERARCHY: mode=3;break;
      case ID_POPUP_GOTOCONTROLERS: mode=4;break;
    }
    wBrowserTab.SetCurSel(mode);
    UpdateBrowser();
    if (nfo.icon==ICON_SELECTION)
      this->OnGotoSelection(nfo.title);
    else if (nfo.icon==ICON_CLASS)
      this->OnGotoDerived(nfo.title);   
  }
}  

void DlgMainFrame::OnDeleteItem()
{
  OnDelete();
}


bool DlgMainFrame::LoadModelConfig(CString name)
{
    bool res=OpenModelConfig(name);
    if (res==false)
    {
      SetActiveWindow();
      if (AfxMessageBox(IDS_NEWCONFIG,MB_YESNO|MB_ICONQUESTION)==IDYES)
      {
        _modelCfg.CreateNewConfig(TCHARToChar(name));
        UpdateListModels();
        UpdateBrowser(true);        
        return true;
      }
      else return false;
    }
    return true;
}

void DlgMainFrame::DisableMCE(bool disable)
{
  _disabled=disable;
  if (GetSafeHwnd()==0) return;
  GetDlgItem(IDC_ADD)->EnableWindow(!disable);
  wBrowseList.EnableWindow(!disable);
  if (disable) _propWindow.DeleteAllItems();
  CMenu *mnu=GetMenu();
  mnu->EnableMenuItem(1,(disable?MF_GRAYED:MF_ENABLED)|MF_BYPOSITION);
  mnu->EnableMenuItem(1,(disable?MF_GRAYED:MF_ENABLED)|MF_BYPOSITION);
}
void DlgMainFrame::OnCancel()
{
 DestroyWindow();
 
}

void DlgMainFrame::OnDestroy()
{
  CleanUp();
  WINDOWPLACEMENT wpos;
  wpos.length=sizeof(wpos);
  GetWindowPlacement(&wpos);
  theApp.WriteProfileBinary("Window","Placement",(LPBYTE)&wpos,sizeof(wpos));
  __super::OnDestroy();
  _modelCfg.Reset();

  // TODO: Add your message handler code here
}

void DlgMainFrame::SwitchToModel(CString name)
{
  const char *cname=TCHARToChar(name);
  AutoArray<RString> list;
  _modelCfg.ListAllModels(list);
  for (int i=0,cnt=list.Size();i<cnt;i++)
  {
    if (_stricmp(list[i],cname)==0) 
    {
      name=CharToTCHAR(list[i]);
      _curModelName=name;
      wModelList.SetWindowText(name);
      UpdateBrowser(true);
    }
  }  
}

void DlgMainFrame::Reset()
{
  CleanUp();
  _modelCfg.Reset();
  UpdateBrowser(true);
  wModelList.SetWindowText("");
}
  
void DlgMainFrame::AddBones(AutoArray<RString> &boneList)
{
  if (boneList.Size()>0)
  {
    CString name=DefaultClassName(CharToTCHAR(boneList[0]));
    if (boneList.Size()>1)
    {
      for (int i=1;i<boneList.Size();i++)
      {
        CalculateCommonName(name,CharToTCHAR(boneList[i]));
      }
      DlgAskClassName dlg;
      dlg.vClassName=name;
      if (dlg.DoModal()==IDCANCEL) return;
      name=dlg.vClassName;
    }
  CorrectClassName(name);
  ParamFile *p=_modelCfg.CreateAnimClass(TCHARToChar(_curModelName),
    TCHARToChar(name),"","",boneList.Size()>1?"":boneList[0]);
  _modelCfg.Update();
  if (boneList.Size()>1 && p)
  for (int i=0;i<boneList.Size();i++)
  {
    const TCHAR *bonName=CharToTCHAR(boneList[i]);
    CString subname=DefaultClassName(bonName);
    CorrectClassName(subname);
    _modelCfg.CreateAnimClass(TCHARToChar(_curModelName),
        TCHARToChar(subname),TCHARToChar(name),"",bonName);    
  }
  if (p)
  {
      _modelCfg.Update();
      UpdateBrowser(true);
      SaveConfigFile(p);
      ParamClassPtr pclass=_modelCfg.FindAnimClass(TCHARToChar(_curModelName),TCHARToChar(name));
      EditClass(pclass);
  }
  else
    AfxMessageBox(IDS_UNABLETOCREATECLASS);
}
}

void DlgMainFrame::CorrectClassName(CString& name)
{
  for (int i=0;i<name.GetLength();i++)
  {
    if (!isalpha(name[i]) && (!isdigit(name[i]) || i==0) && name[i]!='_')
      name.SetAt(i,' ');
  }
  name.Trim();
  for (int i=0;i<name.GetLength();i++)
  {
    if (name[i]==' ') name.SetAt(i,'_');
  }
  if (name.GetLength()==0)
  {
    name="Error";
  }

}

bool DlgMainFrame::CanHandleRequestNow()
{
  if (GetSafeHwnd()==0) return true;
  if (!::IsWindowEnabled(*this))
 {      
   SetForegroundWindow();
   BringWindowToTop();
   CString str,topic;
   str.LoadString(IDS_UNABLEHANDLEREQUEST_BUSY);
   topic.LoadString(IDS_APPNAME);
   AfxMessageBox(str,MB_ICONSTOP|MB_OK|MB_TASKMODAL);
   return false;
 }
 return true;
}

class FunctorDeleteUndo
{
public: bool operator () (SRef<ParamFile> &item) const
        {
          RString name=item->GetName();
          CString undoname=CharToTCHAR(name);
          undoname=undoname+_T(".$$$");
          DeleteFile(undoname);
          return false;
        }
};


class FunctorTestCheckedOut
{
  bool *ask;
  SccFunctions *scc;
  
public: 
  FunctorTestCheckedOut(bool *ask,SccFunctions *scc):ask(ask),scc(scc) {}  
  bool operator () (SRef<ParamFile> &item) const
        {
          RString name=item->GetName();
          if (scc)
            if (scc->UnderSSControlNotDeleted(name) && scc->CheckedOut(name)) *ask=true;
          return *ask;
        }
};


#include "DlgCheckIn.h"


void DlgMainFrame::CleanUp()
{
  StopAllAnimations();
  if (_animThread) {WaitForSingleObject(_animThread->m_hThread,INFINITE);delete _animThread;_animThread=0;}
  _modelCfg.EnumConfigFiles(FunctorDeleteUndo());
  bool ask=false;
  _modelCfg.EnumConfigFiles(FunctorTestCheckedOut(&ask,theApp.App()->GetSccInterface()));
  if (ask)
  {
    DlgCheckIn dlg(IsWindowVisible()?this:0);
    int id=dlg.DoModal();
    if (id!=IDCANCEL)
    {
      SccFunctions *scc=theApp.App()->GetSccInterface();
      int sz=theApp.GetAllDependencies(theApp.GetCurrentDocument(),0,0);
      const char **list=(const char **)alloca(sz);
      sz=theApp.GetAllDependencies(theApp.GetCurrentDocument(),0,const_cast<char **>(list));
      const char **addlist=(const char **)alloca(sizeof(char *)*sz);
      int newsz=0;
      for (int i=0;i<sz;i++) 
      {
        bool remove=false;
        if (_access(list[i],0)!=0) remove=true;
        else
          if (!scc->UnderSSControlNotDeleted(list[i]))
          {
            addlist[newsz++]=list[i]; remove=true;
          }
       else
         if (!scc->CheckedOut(list[i])) remove=true;
       if (remove) {memcpy(list+i,list+i+1,(sz-i-1)*sizeof(char *));sz--;i--;}
      }
      if (id!=IDNO)
      {        
        if (newsz) scc->Add(addlist,newsz,dlg.vComments);
        scc->CheckIn(list,sz,dlg.vComments);
      }
      else
        scc->UndoCheckOut(list,sz);
    }
  }
}

void DlgMainFrame::OnOptionsAllowmodifyparentconfigs()
{
  _allowMofidyParent=!_allowMofidyParent;
}


void DlgMainFrame::OnOptionsDefinetexteditor()
{
  CFileDialog fdlg(TRUE,0,theApp.GetTxtEditor(),OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,
    CString(MAKEINTRESOURCE(IDS_APPLICATIONSFILTER)));
  if (fdlg.DoModal()==IDOK)
    theApp.SetNewEditor(fdlg.GetPathName());
}


template<class Array>
bool DlgMainFrame::EnumSelectedControlers(Array &controlers) const
{
  ParamClassPtr modelDef=_modelCfg.FindModelDefinition(&RString(TCHARToChar(_curModelName)));
  if (modelDef.IsNull()) return false;
  ParamClassPtr animDef=_modelCfg.FindAnimationDefintion(modelDef);
  if (modelDef.IsNull()) return false;

  RStringI controler;
  for (int i=0,cnt=wBrowseList.GetCount();i<cnt;i++) if (wBrowseList.GetSel(i))      
  {
    int map=wBrowseList.MapIndex(i);
    if ((*_currentList)[map].icon==ICON_CLASS)
    {
      CString name=(*_currentList)[map].title;
      ParamEntryPtr entry=animDef->FindEntry(RString(TCHARToChar(name)));
      if (entry.NotNull() && entry->IsClass())
      {
        controler=GetBoneSource(entry.GetPointer());
      }
    }
    else if ((*_currentList)[map].icon==ICON_CONTROLER || (*_currentList)[map].icon==ICON_UNKCONTROLER)
    {
      controler=CharToTCHAR((*_currentList)[map].title);
    }
    else continue;

    int k;
    for (k=0;k<controlers.Size();k++)    
      if (controler==controlers[k]) break;      
    if (k==controlers.Size())
      controlers.Append(controler);
  }
  return true;
}


void DlgMainFrame::SetPosition(float pos)
{
  IExternalViewer *viewer=theApp.App()->GetViewerInterface();
  if (viewer==0) return;
  
  AutoArray<RStringI,MemAllocLocal<RStringI,16> > controlerList;

  if (!EnumSelectedControlers(controlerList)) return;
  
  _cntrListLock.Lock();
  DWORD ctime=GetTickCount();
  for (int i=0;i<controlerList.Size();i++)
  {
    viewer->SetViewerControler(controlerList[i],pos,0,0);
    ControlInfoItem *ccntr=_cntrList.FindControler(controlerList[i]);
    if (ccntr)
    {
      ccntr->RestartAnim(ctime);
      ccntr->position=pos;
    }
  }
  if (wAllAnimations.GetSafeHwnd()) wAllAnimations.Update();
  _cntrListLock.Unlock();
  
}
void DlgMainFrame::SetPingpong(bool enabled)
{
  AutoArray<RStringI,MemAllocLocal<RStringI,16> > controlerList;

  if (!EnumSelectedControlers(controlerList)) return;
  _cntrListLock.Lock();

  DWORD ctime=GetTickCount();
  for (int i=0;i<controlerList.Size();i++)
  {
    ControlInfoItem *ccntr=_cntrList.FindControler(controlerList[i]);
    if (ccntr)
    {
      if (ccntr->IsAnimated())
      {
        ccntr->animMode=enabled?ccntr->AnimatedPingpong:ccntr->AnimatedLoop;
      }
    }
  }
  _cntrListLock.Unlock();
}
void DlgMainFrame::SetAutoAnim(bool enabled)
{
  AutoArray<RStringI,MemAllocLocal<RStringI,16> > controlerList;

  if (!EnumSelectedControlers(controlerList)) return;
  _cntrListLock.Lock();

  DWORD ctime=GetTickCount();
  for (int i=0;i<controlerList.Size();i++)
  {
    ControlInfoItem *ccntr=_cntrList.FindControler(controlerList[i]);
    if (ccntr)
    {
      ccntr->RestartAnim(ctime);
      if (ccntr->speed<0.00001) ccntr->SetDefaultSpeed();
      ccntr->animMode=enabled?       
        (wTimeLineControl.IsPingPongActive()?ccntr->AnimatedPingpong:ccntr->AnimatedLoop):
        (ccntr->NotAnimated);      
    }
  }


  if (enabled) StartAnimThread();

  AfterViewerUpdated();
  _cntrListLock.Unlock();  
}

void DlgMainFrame::UpdateTimeLine(bool askMinMax)
{
  if (wAllAnimations.GetSafeHwnd())
  {
    _cntrListLock.Lock();
    wAllAnimations.Update();
    _cntrListLock.Unlock();
  }
  AutoArray<RStringI,MemAllocLocal<RStringI,16> > controlerList;
  if (!EnumSelectedControlers(controlerList)) return;
  if (controlerList.Size()==0) return;
  
  _cntrListLock.Lock();
  IExternalViewer *viewer=theApp.App()->GetViewerInterface();
  DWORD time=GetTickCount();

  float minval=+FLT_MAX;
  float maxval=-FLT_MAX;
  float sumpos=0; 
  bool pingpong=false;
  bool anim=false;

  for (int i=0;i<controlerList.Size();i++)
  {
    RString cname=controlerList[i];    
    ControlInfoItem *cc=_cntrList.FindControler(cname);
    if (cc==0)
    {
       ControlInfoItem nw;
       nw=cname;
       _cntrList.SaveControler(nw);
       cc=_cntrList.FindControler(cname);
    }
    float pos=cc->CalculatePosition(time);
    sumpos+=pos;
    if (askMinMax)
      viewer->SetViewerControler(cname,pos,&(cc->vmin),&(cc->vmax));
    if (cc->vmin<minval) minval=cc->vmin;
    if (cc->vmax>maxval) maxval=cc->vmax;
    if (cc->animMode!=cc->NotAnimated) anim=true;
    if (cc->animMode==cc->AnimatedPingpong) pingpong=true;
  }
  wTimeLineControl.SetMinMax(minval,maxval);
  wTimeLineControl.SetPosition(sumpos/controlerList.Size());
  wTimeLineControl.SetButtonAnimate(anim);
  if (anim)
    wTimeLineControl.SetButtonPingPong(pingpong);
  _tmlineanimated=anim;
  if (askMinMax)
  {  
    if (_currentClass.NotNull())
    {
      ParamEntryPtr eanim=_currentClass(_modelCfg);
      if (eanim.NotNull() && eanim->IsClass())
      {
        ParamClassPtr anim=eanim->GetClassInterface();
        float parmin=1,parmax=0;
        ParamEntryPtr emin,emax;
        emin=anim->FindEntry("minValue");
        emax=anim->FindEntry("maxValue");
        if (emin.NotNull() && emax.NotNull())
        {
          parmin=(*emin);
          parmax=(*emax);
          if (parmin<parmax)
          {
            wTimeLineControl.SetMarkRange(parmin,parmax);
          }
        }
      }
    }
    else
      wTimeLineControl.SetMarkRange(minval,maxval);
  }
  _cntrListLock.Unlock();
}

UINT CALLBACK DlgMainFrame::AnimationThread(LPVOID data)
{
  DlgMainFrame *self=(DlgMainFrame *)data;
  self->ThreadAnimation();
  return 0;
}
class FunctorAnimateControler
{
  IExternalViewer *viewer;
  bool &anim;  
  DWORD time;
  mutable bool first;
public:
  FunctorAnimateControler(IExternalViewer *x,bool &anim):viewer(x),anim(anim) {time=GetTickCount();first=true;}
  bool operator()(const ControlInfoItem &item) const
  {

    if (item.IsAnimated())
    {
      float m,*mm=first?&m:0;
      first=false;
      if (viewer->SetViewerControler(item.controlName,item.CalculatePosition(time),mm,mm)!=viewer->ErrOK)      
        const_cast<ControlInfoItem &>(item).animMode=ControlInfoItem::NotAnimated;
      else
        anim=true;      
    }
    return false;
  }
};

void DlgMainFrame::ThreadAnimation()
{
  IExternalViewer *viewer;
  bool anim;
  do 
  {    
    anim=false;
    viewer=theApp.App()->GetViewerInterface();
    _cntrListLock.Lock();
    _cntrList.ForEach(FunctorAnimateControler(viewer,anim));
    _cntrListLock.Unlock();
    Sleep(0);
  } 
  while(anim);
  _animated=false;
}

class FunctorStopAllAnimations
{
public:
  bool operator()(const ControlInfoItem &item) const
  {
    const_cast<ControlInfoItem &>(item).RestartAnim(GetTickCount());
    const_cast<ControlInfoItem &>(item).animMode=ControlInfoItem::NotAnimated;
    return false;
  }

};

void DlgMainFrame::StopAllAnimations()
{
  _cntrListLock.Lock();
  _cntrList.ForEach(FunctorStopAllAnimations());
  _cntrListLock.Unlock();
}
void DlgMainFrame::OnTimer(UINT nIDEvent)
{
  if (nIDEvent==UPDATE_ANIM_TIMER)
  {
    UpdateTimeLine();
    if (!_animated) KillTimer(nIDEvent);
  }

  __super::OnTimer(nIDEvent);
}

class FunctorUpdateNotAnimated
{
  IExternalViewer *viewer;
  DWORD time;
public:

  FunctorUpdateNotAnimated(IExternalViewer *x):viewer(x) {time=GetTickCount();}
  bool operator()(const ControlInfoItem &item) const
  {
    if (!item.IsAnimated())
    {
      viewer->SetViewerControler(item.controlName,item.CalculatePosition(time));
    }
    return false;
  }
};

void DlgMainFrame::AfterViewerUpdated()
{
  IExternalViewer *ww=theApp.App()->GetViewerInterface();
  if (ww)
  {
    IExternalViewer::ErrorEnum state=ww->IsReady();
    if (state==ww->ErrOK || state==ww->ErrBusy)
    {    
      _cntrListLock.Lock();
      _cntrList.ForEach(FunctorUpdateNotAnimated(ww));
      _cntrListLock.Unlock();
    }
  }
  UpdateTimeLine(true);
}

void DlgMainFrame::ChangeSpeed(float factor)
{
  IExternalViewer *viewer=theApp.App()->GetViewerInterface();
  if (viewer==0) return;

  AutoArray<RStringI,MemAllocLocal<RStringI,16> > controlerList;

  if (!EnumSelectedControlers(controlerList)) return;


  _cntrListLock.Lock();
  DWORD time=GetTickCount();
  for (int i=0;i<controlerList.Size();i++)
  {
    ControlInfoItem *cc=_cntrList.FindControler(controlerList[i]);
    if (cc)
    {
      cc->RestartAnim(time);
      cc->speed*=factor;
    }
  }
  _cntrListLock.Unlock();
}
void DlgMainFrame::OnPopupSlower()
{
  ChangeSpeed(0.5);
}

void DlgMainFrame::OnPopupFaster()
{
  ChangeSpeed(2);
}

void DlgMainFrame::OnPopupResetall()
{
  ResetAllAnimations();
}

void DlgMainFrame::OnPopupResetspeed()
{
  

  AutoArray<RStringI,MemAllocLocal<RStringI,16> > controlerList;
  if (!EnumSelectedControlers(controlerList)) return;

  _cntrListLock.Lock();
  DWORD time=GetTickCount();
  for (int i=0;i<controlerList.Size();i++)
  {
    ControlInfoItem *cc=_cntrList.FindControler(controlerList[i]);
    if (cc)
    {
      cc->RestartAnim(time);
      cc->SetDefaultSpeed();
    }
  }

  _cntrListLock.Unlock();

}

void DlgMainFrame::OnPopupStopall()
{
  StopAllAnimations();
  AfterViewerUpdated();
}

void DlgMainFrame::OnPopupResetselected()
{
  AutoArray<RStringI,MemAllocLocal<RStringI,16> > controlerList;
  if (!EnumSelectedControlers(controlerList)) return;

  _cntrListLock.Lock();
  DWORD time=GetTickCount();
  for (int i=0;i<controlerList.Size();i++)
  {
    ControlInfoItem *cc=_cntrList.FindControler(controlerList[i]);
    if (cc)
    {
      ControlInfoItem nw;
      nw=cc->controlName;
      *cc=nw;
    }
  }

  _cntrListLock.Unlock();
  AfterViewerUpdated();
}

void DlgMainFrame::OnPopupAllanimations()
{
  AutoArray<ContentInfo> content;
  ParamClassPtr anim=_modelCfg.FindAnimationDefintion(_modelCfg.FindModelDefinition(&RString(TCHARToChar(_curModelName))));
  if (!anim.IsNull())
  {
    anim->ForEachEntry(EnumControlerAssigment(content,anim));
  }
  _cntrListLock.Lock();
  for (int i=0;i<content.Size();i++)
    if (content[i].ident==0)
    {
      ControlInfoItem *controler=_cntrList.FindControler(TCHARToChar(content[i].title));
      if (controler==0)
      {
        ControlInfoItem nw;
        nw=content[i].title;
        _cntrList.SaveControler(nw);
        controler=_cntrList.FindControler(TCHARToChar(content[i].title));
      }
      if (theApp.App()->GetViewerInterface())
        theApp.App()->GetViewerInterface()->
          SetViewerControler(controler->controlName,
                  0,&controler->vmin,&controler->vmax);
    }
  _cntrListLock.Unlock();
 
 if (wAllAnimations.GetSafeHwnd())
    wAllAnimations.BringWindowToTop();
  else
    wAllAnimations.Create(this);
 wAllAnimations.Update();
}

void DlgMainFrame::StartAnimThread()
{
 if (!_animated)
  {
    _animated=true;
    delete _animThread;
    _animThread=AfxBeginThread((AFX_THREADPROC)AnimationThread,this);
    _animThread->m_bAutoDelete=FALSE;
    SetTimer(UPDATE_ANIM_TIMER,120,0);
  }
}
void DlgMainFrame::UpdateController(ControlInfoItem *cntr)
{
  if (theApp.App()->GetViewerInterface())
    theApp.App()->GetViewerInterface()->SetViewerControler(cntr->controlName,cntr->CalculatePosition(GetTickCount()),&(cntr->vmin),&(cntr->vmax));
  UpdateTimeLine(false);
}

void DlgMainFrame::ResetAllAnimations()
{
  _cntrListLock.Lock();
  _cntrList.Clear();  
  _cntrListLock.Unlock();
  theApp.App()->ReloadViewer();
  UpdateTimeLine(true);

}