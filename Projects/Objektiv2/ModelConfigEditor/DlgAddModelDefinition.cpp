// DlgAddModelDefinition.cpp : implementation file
//

#include "stdafx.h"
#include "ModelConfigEditor.h"
#include "DlgAddModelDefinition.h"
#include ".\dlgaddmodeldefinition.h"


// DlgAddModelDefinition dialog

IMPLEMENT_DYNAMIC(DlgAddModelDefinition, CDialog)
DlgAddModelDefinition::DlgAddModelDefinition(CWnd* pParent /*=NULL*/)
	: CDialog(DlgAddModelDefinition::IDD, pParent)
    , vDefName(_T("")),vSuffix(_T("Skeleton"))
    , vClone(FALSE)
{
}

DlgAddModelDefinition::~DlgAddModelDefinition()
{
}

void DlgAddModelDefinition::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_NAME, vDefName);
  DDX_Control(pDX, IDC_NAME, wDefName);
  DDX_Control(pDX, IDC_DERIVED, wDerived);
  DDX_Text(pDX, IDC_SURFIX, vSuffix);
  DDX_Control(pDX, IDC_CHECK2, wClone);
  DDX_Check(pDX, IDC_CHECK2, vClone);
}


BEGIN_MESSAGE_MAP(DlgAddModelDefinition, CDialog)
  ON_EN_CHANGE(IDC_NAME, OnEnChangeName)
  ON_BN_CLICKED(IDC_CHECK2, OnBnClickedCheck2)
  ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
END_MESSAGE_MAP()


// DlgAddModelDefinition message handlers

class EnumBaseClasses
{
  CComboBox &_combo;
public:
  EnumBaseClasses(CComboBox &combo):_combo(combo) {}
  bool operator() (ParamEntryVal &entry) const
  {
    if (entry.IsClass()) _combo.AddString(CharToTCHAR(entry.GetName()));
    return false;
  }
};

void DlgAddModelDefinition::OnEnChangeName()
{
DialogRules();
}

BOOL DlgAddModelDefinition::OnInitDialog()
{
  CDialog::OnInitDialog();
  
  DialogRules();

  if (!_cfgModels.IsNull()) _cfgModels->ForEachEntry(EnumBaseClasses(wDerived));
  wDerived.AddString("");
  wDerived.SelectString(-1,vDerived);
  
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgAddModelDefinition::DialogRules()
{
  GetDlgItem(IDOK)->EnableWindow(wDefName.GetWindowTextLength()!=0);
  GetDlgItem(IDC_SURFIX)->EnableWindow(wClone.GetCheck()==0);
}
void DlgAddModelDefinition::OnOK()
{
  UpdateData();
  wDerived.GetLBText(wDerived.GetCurSel(),vDerived);  
  CString msg;
  AfxFormatString2(msg,IDS_ASKCREATENEWCLASS,vDefName,vDerived);
  if (AfxMessageBox(msg,MB_OKCANCEL|MB_ICONEXCLAMATION)==IDCANCEL) return;
  CDialog::OnOK();
}

void DlgAddModelDefinition::OnBnClickedCheck2()
{
  DialogRules();
}

void DlgAddModelDefinition::OnBnClickedButton1()
{
  AfxMessageBox(IDS_CLONEHELP);
}
