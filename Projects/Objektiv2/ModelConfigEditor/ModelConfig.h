#pragma once

#include <el/ParamFile/ParamFile.hpp>
#include <el/Pathname/Pathname.h>
#include <el\Btree\Btree.h>

#define DEFAULT_AXIS_TEXT "_axis"


struct SkeletonInfo
{
  RString selName;
  RString parent;
  bool derived; //selection is derived for this model

  bool operator==(const SkeletonInfo &other) const {return _stricmp(selName,other.selName)==0;}
  bool operator>=(const SkeletonInfo &other) const {return _stricmp(selName,other.selName)>=0;}
  bool operator<=(const SkeletonInfo &other) const {return _stricmp(selName,other.selName)<=0;}
  bool operator!=(const SkeletonInfo &other) const {return _stricmp(selName,other.selName)!=0;}
  bool operator>(const SkeletonInfo &other) const {return _stricmp(selName,other.selName)>0;}
  bool operator<(const SkeletonInfo &other) const {return _stricmp(selName,other.selName)<0;}
  SkeletonInfo &operator=(int n)
  {
    selName="";
    parent="";
    return *this;
  }
};

struct ClassRelationItem
{
  ParamClassPtr _mainClass;
  ParamClassPtr _relatedClass;
  enum RelationType {Derived,Child};
  RelationType _type;
  int Compare(const ClassRelationItem &other) const
  {
    if (_type==other._type)
    {
      if (_mainClass.GetPointer()>other._mainClass.GetPointer()) return 1;
      if (_mainClass.GetPointer()<other._mainClass.GetPointer()) return -1;
      if (_relatedClass.GetPointer()>other._relatedClass.GetPointer()) return 1;
      if (_relatedClass.GetPointer()<other._relatedClass.GetPointer()) return -1;
      return 0;
    }
    if (_type>other._type) return 1;
    else return -1;
  }
  bool operator==(const ClassRelationItem &other) const {return Compare(other)==0;}
  bool operator>=(const ClassRelationItem &other) const {return Compare(other)>=0;}
  bool operator<=(const ClassRelationItem &other) const {return Compare(other)<=0;}
  bool operator!=(const ClassRelationItem &other) const {return Compare(other)!=0;}
  bool operator>(const ClassRelationItem &other) const {return Compare(other)>0;}
  bool operator<(const ClassRelationItem &other) const {return Compare(other)<0;}
  ClassRelationItem(const ParamClassPtr &main, const ParamClassPtr other, RelationType relationType):
  _mainClass(main),_relatedClass(other),_type(relationType) {}
  ClassRelationItem() {}
  int operator=(int n) {return n;}
};

TypeIsMovable(SkeletonInfo);

/*
class ModelConfig :public ParamFile
{  
  AutoArray<SRef<ParamFile> >_inherited;
public:
  ModelConfig(void);
  ~ModelConfig(void);

  bool AddInherited(const Pathname &fname);
  bool Load(const _TCHAR *fname);
  bool CreateSelectionList(BTree<SkeletonInfo> &list, const RString *modelName=0);
  ConstParamClassPtr GetBaseClass(ConstParamClassPtr pclass) const;
  ParamEntryPtr FindEntryInherited(ConstParamClassPtr pclass, const char *name) const;

  ParamClassPtr FindModelDefinition(const RString *modelName=0) const;
  ParamClassPtr FindSkeletonDefinition(ConstParamClassPtr foundAnimations);
  
  void EnumerateClassesForSelection(const RString &selectionName, AutoArray<ParamClassPtr> &classes, const RString *modelName=0);
protected:
  void GetSelections(ConstParamClassPtr skeleton, BTree<SkeletonInfo> &selContainer);
  void EnumerateClassesForSelection(const RString &selectionName, AutoArray<ParamClassPtr> &classes, ConstParamClassPtr foundAnimations);
};

*/


class ModelConfig :public ParamFile
{  
  struct PFEntryItem
  {
    ParamEntryPtr _merged;
    ParamEntryPtr _origin;

    bool operator==(const PFEntryItem &other) const {return _merged.GetPointer()==other._merged.GetPointer();}
    bool operator>=(const PFEntryItem &other) const {return _merged.GetPointer()>=other._merged.GetPointer();}
    bool operator<=(const PFEntryItem &other) const {return _merged.GetPointer()<=other._merged.GetPointer();}
    bool operator!=(const PFEntryItem &other) const {return _merged.GetPointer()!=other._merged.GetPointer();}
    bool operator>(const PFEntryItem &other) const {return _merged.GetPointer()>other._merged.GetPointer();}
    bool operator<(const PFEntryItem &other) const {return _merged.GetPointer()<other._merged.GetPointer();}
    PFEntryItem &operator=(int n)
    {
      _merged=0;
      _origin=0;
      return *this;
    }
  };
  
  AutoArray<SRef<ParamFile> >_stages;  
  BTree<PFEntryItem> _contextTransform;
  BTree<ClassRelationItem> _relations;
  
  bool FillUpDatabase(ParamClassPtr pclass);

  ModelConfig(const ModelConfig &other);
  ModelConfig& operator=(const ModelConfig &other);
public:
  ModelConfig() {};

  void *operator new(size_t sz, void *pos) {return pos;}
  void operator delete(void *, void *pos) {}

  ///Adds new config into stage. First stage is top most config.
  bool Add(const Pathname &fname);
  ///Loads entire config, including all parent configs.
  bool Load(const Pathname &fname);
  ///Updates this class from stages and rebuilds tranform
  bool Update();  

  void ReportEntry(ConstParamEntryPtr pentry) const;

  void Reset();

  ParamEntryPtr Transform(ParamEntryPtr fromMerged) const;
  ParamClassPtr Transform(ParamClassPtr fromMerged) const
  {
    ParamEntryPtr res=Transform(ParamEntryPtr(fromMerged->GetClassInterface(),fromMerged.GetPointer()));
    if (res.IsNull() || !res->IsClass()) return ParamClassPtr();
    else return ParamClassPtr(res->GetClassInterface());
  }

  void GetSelections(ConstParamClassPtr skeleton, BTree<SkeletonInfo> &selContainer, bool isderived);
  void EnumerateClassesForSelection(const RString &selectionName, AutoArray<ParamClassPtr> &classes, ConstParamClassPtr foundAnimations);
  bool CreateSelectionList(BTree<SkeletonInfo> &list, const RString *modelName=0);
  ParamClassPtr FindModelDefinition(const RString *modelName=0) const;
  ParamClassPtr FindModelDefinitionClass() const;
  ParamClassPtr FindSkeletonDefinition(ConstParamClassPtr foundAnimations,bool excludeUpper=false); 
  ParamClassPtr FindSkeletonDefinitionClass() const;
  ParamClassPtr FindAnimationDefintion(ConstParamClassPtr modelDefinition) const;
  ParamClassPtr FindInheritedSkeleton(ConstParamClassPtr skeleton); 
  void EnumerateClassesForSelection(const RString &selectionName, AutoArray<ParamClassPtr> &classes, const RString *modelName=0);

  void ListAllModels(AutoArray<RString> &list);

  class EnumerateClassesForSelectionFunctor
  {
    const RString &selectionName;
    AutoArray<ParamClassPtr> &classes;
  public:
    EnumerateClassesForSelectionFunctor(const RString &selectionName, AutoArray<ParamClassPtr> &classes):
        selectionName(selectionName),classes(classes) {}
    bool operator ()(ParamEntryVal entry) const;
  };

    enum Error
    {
      errOk=0,
      errCfgModelsNotFound,
      errAlreadyExists,
      errNoTransform,
      errUnableToImportBase,
      errUnableToSetBase,
      errCfgSkeletonsNotFound,
      errInUse,
      errClassNotFound
    };

   ParamFile *RegisterSelection(const char *modelName, const char *oldName, const char *newName, const char *newParent);
   ParamFile * CreateAnimClass(const RString &model, const RString & name, const RString & derived, const RString & controler, const RString & selection);
   ParamClassPtr CreateAnimClassNw(const RString &model, const RString & name, const RString & derived, const RString & controler, const RString & selection);
   
   ParamClassPtr ModelConfig::FindAnimClass(const RString &model, const RString &name);
   ParamFile * DeleteAnimClass(ParamEntryPtr pclass);

   void RebuildRelationDB();
   BTree<ClassRelationItem> &GetRelationDb() {return _relations;}

/*  static bool EnsureBaseImported(ParamClassPtr motherClass, const char *baseName, ParamClassPtr visibleFor, bool lastUndefined);*/
   static bool ImportBaseClass(ParamClassPtr parentClass, ParamClassPtr thisClass, const char *name);
   static void CleanUpAfterImport(ParamClassPtr visibleFor);

   bool CreateNewConfig(const char *fname);
   Error CreateNewModelDef(const char *name, const char *derivedFrom, const char *skeletonSufix="Skeleton", ParamFile **toSave=0);
   Error CreateCloneModelDef(const char *name, const char *derivedFrom,  ParamFile **toSave=0);
   Error DeleteModelDef(const char *name, ParamFile **tosave);

   void Reload();

   template <class T>
   void EnumConfigFiles(T &functor)
   {
     _stages.ForEachF(functor);
   }
   int CountConfigs() {return _stages.Size();}   


   static bool IsNotDefinedInClassFile(ParamClassPtr owningClass, ParamEntryPtr entry, bool file);      
   bool IsSelectionExists(const char *name, const char *model);
};

TypeIsMovable(ParamClassPtr);

