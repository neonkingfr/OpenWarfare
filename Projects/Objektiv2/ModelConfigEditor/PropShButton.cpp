#include "StdAfx.h"
#include ".\propshbutton.h"
#include <projects\bredy.libs\extendedpropertysheet\WindowPos.h>
#include <projects\bredy.libs\extendedpropertysheet\PropShCommon.h>

PropShButton::PropShButton(Align align, float width, int height, bool singleline)
{
  SetAlign(align);
  SetWidth(width);
  SetHeight(height);
  _hButton=0;
  _singleline=singleline;
}

PropShButton::~PropShButton(void)
{
  DestroyWindow(_hButton);
}


bool PropShButton::Create(const tchar *name, const tchar *title, PropShItem *parent)
{
  if (PropShItem::Create(name,title,parent))
  {
    _hButton=CreateWindow("BUTTON",title,BS_PUSHBUTTON|BS_TEXT|(_singleline?0:BS_MULTILINE)|BS_CENTER|BS_VCENTER|WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_GROUP,0,0,0,0,parent->GetWindowHandle(),0,parent->GetInstance(),0);
    return _hButton!=0;
  }
  return 0;
}

void PropShButton::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  SendMessage(_hButton,message,wParam,lParam);
}

void PropShButton::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{
  tchar *s=(tchar *)alloca(sizeof(*s)*(GetWindowTextLength(_hButton)+1));
  GetWindowText(_hButton,s,GetWindowTextLength(_hButton)+1);

  int h=_height;
  int w=_width;
  if (w<0)
  {
    w=(int)(wspace*_fwidth);
  }
  if (w<0)
  {
    float prop;
    GetParentItem()->GetItemProperty(PROPSH_TITLESIZE,prop,100.0f);
    if (prop<1) w=(int)(wspace*prop);else w=(int)prop;
  }
  if (h<0 && !_singleline)
  {    
    h=GetWindowsTextSizeFixedWith(_hButton,w-8,s)+8;
  }
  if (_singleline)
  {
    SIZE sz=GetWindowsTextSize(_hButton,s);
    sz.cx+=8;
    sz.cy+=8;
    if (sz.cx>w) w=sz.cx;
    if (h<0) h=sz.cy;
  }
  int x;
  switch(_align)
  {
  case AlignLeft: x=0;break;
  case AlignCenter: x=(wspace-w)>>1;break;
  case AlignRight: x=wspace-w;break;
  };
  if (x<0) x=0;
  hdwp.SetWindowPos(_hButton,left+x,top,w,h);
  height=h;
  width=x+w;
}

void PropShButton::SetWidth(float width)
{
  if (width==0.0) _width=0;
  else if (width>0.0f && width<1.0f) {_fwidth=width;_width=-1;}
  else if (width<0.0f) {_fwidth=width;_width=-1;}
  else _width=(int)width;
}

bool PropShButton::ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result)
{
  if (msg==WM_COMMAND && hWnd==_hButton)
  {
    ThrowEvent().Action(this,ButtonAction(PROPSHBUTTON_BUTTONACTION,HIWORD(wParam)));
    return true;
  }
  return false;
}