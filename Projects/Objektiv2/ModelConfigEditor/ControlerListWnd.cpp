// ontrolerListWnd.cpp : implementation file
//

#include "stdafx.h"
#include "ModelConfigEditor.h"
#include "ControlerListWnd.h"
#include "ControlList.h"
#include ".\controlerlistwnd.h"


// ControlerListWnd

IMPLEMENT_DYNAMIC(ControlerListWnd, CWnd)
ControlerListWnd::ControlerListWnd(ControlList &cntrList,ControlerListWndEvent *event):_cntrList(cntrList),_event(event)
{
}

ControlerListWnd::~ControlerListWnd()
{
}


BEGIN_MESSAGE_MAP(ControlerListWnd, CWnd)
  ON_WM_SIZE()
  ON_WM_DESTROY()
  ON_WM_VSCROLL()
  ON_WM_PAINT()
  ON_MESSAGE(WM_EXITSIZEMOVE, AfterResize)
  ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()



// ControlerListWnd message handlers

BOOL ControlerListWnd::Create(CWnd *owner)
{
  CString ss;
  ss.Format(_T("%d,%d,%d,%d"),CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT);
  ss=theApp.GetProfileString(_T("ControlerListWnd"),_T("Position"),ss);
  int x,y,xs,ys;
  sscanf(ss,"%d,%d,%d,%d",&x,&y,&xs,&ys);  

  BOOL res=CreateEx(WS_EX_TOOLWINDOW,AfxRegisterWndClass(0,LoadCursor(0,IDC_ARROW),0,0),
    CString(MAKEINTRESOURCE(IDS_CONTROLLERLIST)),WS_VISIBLE|WS_OVERLAPPEDWINDOW,CRect(x,y,xs+x,ys+y),owner,0,0);
  if (res) Rebuild();
  return res;
}

bool ControlerListWnd::FillNamesControls::operator()(const ControlInfoItem &item) const
{
  CStatic *title=new CStatic();
  TimeLineControlEventEx *event=new TimeLineControlEventEx(outer,item.controlName);
  TimeLineControl *tmcntr=new TimeLineControl(event);


  tmcntr->Create(tmcntr->IDD,outer);  
  title->Create(item.controlName,SS_CENTERIMAGE|SS_LEFT|WS_CHILD,CRect(0,0,0,0),outer);

  CFont *f=tmcntr->GetFont();
  title->SetFont(f);

  outer->_names.Append()=item.controlName;
  outer->_titles.Append()=title;
  outer->_controls.Append()=tmcntr;
  outer->_events.Append()=event;
  return false;
}

void ControlerListWnd::Rebuild()
{
  _controls.Clear();
  _names.Clear();
  _titles.Clear();
  _events.Clear();

  _cntrList.ForEach(FillNamesControls(this));
  SetScrollRange(SB_VERT,0,_controls.Size()-1,TRUE);
  CRect dim;
  GetClientRect(&dim);
  RecalcLayout(dim.right,dim.bottom);
}

void ControlerListWnd::RecalcLayout(int cx,int cy)
{
  SetScrollRange(SB_VERT,0,_controls.Size()-1,TRUE);
  int offst=GetScrollPos(SB_VERT);
  int idx=0;
  int y=0;
  HDWP dwp=BeginDeferWindowPos(_controls.Size()*2+5);
  while (idx<offst && idx<_controls.Size()) 
  {
    _controls[idx]->ShowWindow(SW_HIDE);
    _titles[idx]->ShowWindow(SW_HIDE);
    idx++;
  }
  while (idx<_controls.Size() && y<cy)
  {
    CRect dim;
    int splt=cx/4;
    if (splt<80) 
      if (cx/3>80) splt=80;
      else splt=cx/3;
    _controls[idx]->GetWindowRect(&dim);
    _controls[idx]->ShowWindow(SW_SHOW);
    _titles[idx]->ShowWindow(SW_SHOW);
    DeferWindowPos(dwp,*_titles[idx],0,0,y,splt,dim.Size().cy,SWP_NOZORDER);
    DeferWindowPos(dwp,*_controls[idx],0,splt,y,cx-splt,dim.Size().cy,SWP_NOZORDER);
    idx++;
    y+=dim.Size().cy;
  }
  if (idx+1<_controls.Size())
  {  
    SCROLLINFO nfo;
    nfo.cbSize=sizeof(nfo);
    nfo.fMask=SIF_PAGE;
    nfo.nPage=idx-offst-1;
    SetScrollInfo(SB_VERT,&nfo,FALSE);
  }
  while (idx<_controls.Size())
  {
    _controls[idx]->ShowWindow(SW_HIDE);
    _titles[idx]->ShowWindow(SW_HIDE);
    idx++;    
  }
  _latestY=y;
  InvalidateRect(CRect(0,_latestY,cy,cx),TRUE);
  EndDeferWindowPos(dwp);
}

void ControlerListWnd::SetPosition(RString controler, float pos)
{
  ControlInfoItem *item=_cntrList.FindControler(controler);
  if (item)
  {
    item->RestartAnim(GetTickCount());
    item->position=pos;
    _event->UpdateController(item);
  }
  else
    Rebuild();
}
void ControlerListWnd::SetPingpong(RString controler, bool enabled)
{
  ControlInfoItem *item=_cntrList.FindControler(controler);
  if (item)
  {
    if (item->IsAnimated())
    {
      item->RestartAnim(GetTickCount());
      item->animMode=enabled?item->AnimatedPingpong:item->AnimatedLoop;
    }
  }
  else
    Rebuild();

}
void ControlerListWnd::SetAutoAnim(RString controler, bool enabled)
{
  ControlInfoItem *item=_cntrList.FindControler(controler);
  if (item)
  {
    bool enpingpong;
    item->RestartAnim(GetTickCount());    
      for (int i=0;i<_names.Size();i++)
        if (controler==_names[i])        
        {enpingpong=_controls[i]->IsPingPongActive();break;}
    item->animMode=enabled?(enpingpong?item->AnimatedPingpong:item->AnimatedLoop):item->NotAnimated;
    if (item->speed<0.0001) item->SetDefaultSpeed();
    if (enabled) _event->StartAnimThread();
  }
  else
    Rebuild();

}
void ControlerListWnd::ChangeSpeed(RString controler, float factor)
{
  ControlInfoItem *item=_cntrList.FindControler(controler);
  if (item)
  {
    item->RestartAnim(GetTickCount());
    item->speed*=factor;
  }
  else
    Rebuild();

}

void ControlerListWnd::OnSize(UINT nType, int cx, int cy)
{
  CWnd::OnSize(nType, cx, cy);
}

void ControlerListWnd::OnDestroy()
{
  CRect rc;
  GetWindowRect(&rc);
  CString saverect;saverect.Format(_T("%d,%d,%d,%d"),rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top);
  theApp.WriteProfileString(_T("ControlerListWnd"),_T("Position"),saverect);

  CWnd::OnDestroy();

  _controls.Clear();
  _names.Clear();
  _titles.Clear();
  _events.Clear();
}

void ControlerListWnd::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  if (nSBCode!=SB_THUMBPOSITION && nSBCode!=SB_THUMBTRACK) 
  {
    if (nSBCode==SB_ENDSCROLL) return;
    nPos=GetScrollPos(SB_VERT);
    switch(nSBCode) {
    case SB_BOTTOM: nPos=_controls.Size()-1;break;
    case SB_TOP: nPos=0;break;
    case SB_LINEDOWN: nPos++;break;
    case SB_LINEUP: nPos--;break;
    case SB_PAGEDOWN: nPos+=5;break;
    case SB_PAGEUP: nPos-=5;break;   
    }
    SetScrollPos(SB_VERT,nPos,TRUE);
  }
  else  
    SetScrollPos(SB_VERT,nPos,TRUE);

  CRect dim;
  GetClientRect(&dim);
  RecalcLayout(dim.right,dim.bottom);
}

void ControlerListWnd::OnPaint()
{
  CPaintDC dc(this); 
  CRect dim;
  GetClientRect(&dim);
  dc.FillSolidRect(0,_latestY,dim.right,dim.bottom-_latestY,GetSysColor(COLOR_BTNFACE));
}


void ControlerListWnd::Update()
{  
  if (_cntrList.Size()!=_controls.Size()) Rebuild();
  for (int i=0;i<_controls.Size();i++)
  {
    RString name=_names[i];
    ControlInfoItem *itm=_cntrList.FindControler(name);
    if (itm==0)
    {
      Rebuild();
      Update();
      return;
    }
    TimeLineControl *tmln=_controls[i];
    tmln->SetButtonAnimate(itm->IsAnimated());
    if (itm->IsAnimated())
    {
      tmln->SetButtonPingPong(itm->animMode==itm->AnimatedPingpong);
    }
    tmln->SetMinMax(itm->vmin,itm->vmax);
    tmln->SetPosition(itm->CalculatePosition(GetTickCount()));
  }
}

LRESULT ControlerListWnd::AfterResize(WPARAM wParam,LPARAM lParam)
{
  CRect dim;
  GetClientRect(&dim);
  RecalcLayout(dim.right,dim.bottom);
  return 0;
}
BOOL ControlerListWnd::PreTranslateMessage(MSG* pMsg)
{
  for (int i=0;i<_controls.Size();i++)
    if (_controls[i]->PreTranslateMessage(pMsg)) return TRUE;

  return FALSE;
}

void ControlerListWnd::OnContextMenu(CWnd* pWnd, CPoint point)
{
  for (int i=0;i<_controls.Size();i++)
  {
    if (pWnd==_controls[i]) 
    {
      ControlInfoItem *itm=_cntrList.FindControler(_names[i]);
      if (itm)
      {      
        CMenu mnu;
        mnu.LoadMenu(IDR_ANIMMENU);
        CMenu *popup=mnu.GetSubMenu(0);
        popup->EnableMenuItem(ID_POPUP_ALLANIMATIONS,MF_GRAYED);
        popup->EnableMenuItem(ID_POPUP_RESETALL,MF_GRAYED);
        int cmd=popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RETURNCMD|TPM_NONOTIFY,point.x,point.y,this);
        DWORD ctime=GetTickCount();
        switch (cmd)
        {
        case ID_POPUP_FASTER: itm->RestartAnim(ctime);itm->speed*=2.0f;break;
        case ID_POPUP_SLOWER: itm->RestartAnim(ctime);itm->speed/=2.0f;break;
        case ID_POPUP_RESETSPEED: itm->RestartAnim(ctime);itm->SetDefaultSpeed();break;
        case ID_POPUP_RESETSELECTED: itm->RestartAnim(ctime);itm->animMode=itm->NotAnimated;itm->position=0;_event->UpdateController(itm);break;
        case ID_POPUP_STOPALL: _event->StopAllAnimations();break;
        }
      }
      return;
    }
  }
}
