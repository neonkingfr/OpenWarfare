#ifndef IFCModelConfig_H
#define IFCModelConfig_H

#include "..\IPluginGeneral.h"

class SccFunctions;

class IMCEAppSide;
class IMCEPluginSide
{
public:
  virtual void Init(IMCEAppSide *appSide)=0;
  ///Opens model config editor for given filename
  /**
  Function returns true, if there was no error handling this request.
  If there is some editor openned, function closes editor, and opens editor again.
  */
  virtual bool OpenModelConfig(const char *p3dname)=0;
  ///Retrieves list of skeletons
  /**
  @param array pointer to allocated space for array. Pass 0 to get required count of bytes to allocate
  @return if array is 0, function returns required bytes to allocate. If array points to some 
  space, function returns number of records stored in array
  @note Do not move data to another location. All pointers in array points into allocated block. If 
  you want move data, you need relocate all pointers in array
  */
  virtual int GetListOfSkeletons(char **array)=0;


  ///Adds bones froms selections
  /**
  Function creates classes for bones in model config.
  Model config must be opened.
  @param boneList array of bones to add
  @param count count of bones to add
  @return Function returns true, when operation was successful. Function returns false, if there was an error.
  */
  virtual bool AddBones(const char **boneList, int count)=0;

  ///Retrieves filename used with OpenModelConfig;
  /** Function returns 0, if window is closed. */
  const char *GetOpenedConfig();
};

class IMCEAppSide
{
public:
  virtual SccFunctions *GetScc()=0;
  virtual void ConfigChanged()=0;

  ///Retrieves list of selection used in current model - it used to fill comboboxes
  /*
  @param array pointer to allocated space for array, Pass 0 to get required count of bytes to allocate
  @return if array is 0, function returns count of required bytes to allocate. If array points to 
  some memory, function fills memory and returns number of records stored in array  
  @note Do not move data to another location. All pointers in array points into allocated block. If 
  you want move data, you need relocate all pointers in array
  */
  virtual int GetAllSelections(char **array)=0;
};


#endif