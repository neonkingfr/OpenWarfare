#pragma once


// DlgCheckIn dialog

class DlgCheckIn : public CDialog
{
	DECLARE_DYNAMIC(DlgCheckIn)

public:
	DlgCheckIn(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgCheckIn();

// Dialog Data
	enum { IDD = IDD_SSCOMMENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedUndocheckin();
    CString vComments;
};
