// DlgAskClassName.cpp : implementation file
//

#include "stdafx.h"
#include "ModelConfigEditor.h"
#include "DlgAskClassName.h"
#include ".\dlgaskclassname.h"


// DlgAskClassName dialog

IMPLEMENT_DYNAMIC(DlgAskClassName, CDialog)
DlgAskClassName::DlgAskClassName(CWnd* pParent /*=NULL*/)
	: CDialog(DlgAskClassName::IDD, pParent)
    , vClassName(_T(""))
{
}

DlgAskClassName::~DlgAskClassName()
{
}

void DlgAskClassName::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_EDIT1, wClassName);
  DDX_Text(pDX, IDC_EDIT1, vClassName);
}


BEGIN_MESSAGE_MAP(DlgAskClassName, CDialog)
  ON_EN_CHANGE(IDC_EDIT1, OnEnChangeEdit1)
END_MESSAGE_MAP()


// DlgAskClassName message handlers

void DlgAskClassName::OnEnChangeEdit1()
{
  DialogRules();
}

BOOL DlgAskClassName::OnInitDialog()
{
  CDialog::OnInitDialog();
  DialogRules();
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgAskClassName::DialogRules(void)
{
  GetDlgItem(IDOK)->EnableWindow(wClassName.GetWindowTextLength());
}
