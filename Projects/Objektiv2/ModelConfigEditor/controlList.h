#pragma once

struct ControlInfoItem
{
  enum AnimMode
  {
    NotAnimated,
    AnimatedLoop,
    AnimatedPingpong
  };
  RString controlName;
  float vmin;
  float vmax;
  float position;
  DWORD startAnimTime;
  float speed;  //per second
  AnimMode animMode;

  ControlInfoItem():vmin(0),vmax(0),position(0),startAnimTime(0),speed(0),animMode(NotAnimated) {}
  bool IsAnimated() const {return animMode!=NotAnimated;}

  float CalculatePosition(DWORD time) const
  {
    float xmin=vmin;
    float xmax=vmax;
    if (xmax>FLT_MAX/2) {xmin=0;xmax=1000*speed;}
    if (animMode==NotAnimated) return position;
    float newpos=position+(float)(time-startAnimTime)/1000.0f*speed;
    float ds=(xmax-xmin);
    float ds2=ds*2;
    newpos-=xmin;
    if (animMode==AnimatedPingpong)
    {
      if (newpos<0) newpos=-newpos;
      if (newpos>ds2)
      {
        newpos=newpos-(float)floor(newpos/ds2)*ds2;
      }
      if (newpos>ds) newpos=ds2-newpos;
      return newpos+xmin;
    }
    else
    {
      if (newpos>ds)      
        newpos=newpos-(float)floor(newpos/ds)*ds;
      else if (newpos<-ds)
        newpos=newpos+(float)ceil(newpos/ds)*ds;
      if (newpos<0) newpos=ds-newpos;
      return newpos+xmin;
    }
  }
  int CompareWith(const ControlInfoItem &other) const
  {
    return _stricmp(controlName,other.controlName);
  }
  void SetDefaultSpeed()
  {
    float diff=vmax/2-vmin/2;
    if (diff>FLT_MAX/2) speed=1.0f;
    else speed=diff/2.0f;
  }

  void RestartAnim(DWORD time)
  {
    position=CalculatePosition(time);
    startAnimTime=time;
  }
  
  bool operator==(const ControlInfoItem &other) const {return CompareWith(other)==0;}
  bool operator>=(const ControlInfoItem &other) const {return CompareWith(other)>=0;}
  bool operator<=(const ControlInfoItem &other) const {return CompareWith(other)<=0;}
  bool operator!=(const ControlInfoItem &other) const {return CompareWith(other)!=0;}
  bool operator>(const ControlInfoItem &other) const {return CompareWith(other)>0;}
  bool operator<(const ControlInfoItem &other) const {return CompareWith(other)<0;}
  void operator=(const char *c) {controlName=c;}

};


class ControlList
{
  BTree<ControlInfoItem> _list;
public:

  ControlInfoItem *FindControler(RString name)
  {
    ControlInfoItem key,*found;
    key=name;
    found=_list.Find(key);
    return found;
  }
  template<class F>
  bool ForEach(const F &funct)
  {
    BTreeIterator<ControlInfoItem> _iter(_list);
    ControlInfoItem *item;
    while (item=_iter.Next())
    {
      if (funct(*item)) return true;
    }
    return false;
  }

  void Clear()
  {
    _list.Clear();
  }
  
  int Size() {return _list.Size();}

  void SaveControler(const ControlInfoItem &data)
  {
    ControlInfoItem *found=_list.Find(data);
    if (!found)    
      _list.Add(data);
    else
      *found=data;
  }
};
