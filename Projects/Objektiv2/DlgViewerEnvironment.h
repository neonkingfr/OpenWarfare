#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "..\ObjektivLib\IExternalViewer.h"
#include <afxdtctl.h>
#include <afxwin.h>


// CDlgViewerEnvironment dialog


#define ENVSTARTUPPROFILE "Startup"

class CDlgViewerEnvironment : public CDialog
{
public:
  struct EnvInfoStruct: public IExternalViewer::EnvInfoStruct
  {
    template<class Archive>
    void Serialize(Archive &arch)
    {
      arch(overcast);
      arch(fog);
      arch(eyeAccom);
      arch(time);
    }
  };
protected:  
  DECLARE_DYNAMIC(CDlgViewerEnvironment)
  EnvInfoStruct _lastState;
public:
	CDlgViewerEnvironment(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgViewerEnvironment();

// Dialog Data
	enum { IDD = IDD_VIEWERENVIRONMENT };

protected:
    DWORD lastKnowTime;
    COLORREF ambient;
    COLORREF diffuse;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CSliderCtrl wOvercast;
  CSliderCtrl wFog;
  CSliderCtrl wHdr;
  CButton wHdrEnable;
  CEdit wOvercastVal;
  CEdit wFogVal;
  CEdit wHdrVal;
  CDateTimeCtrl wTimeOfDay;
  CScrollBar wTimeOfDayScr;
  CComboBox wPresets;
  
  afx_msg void OnTimer(UINT nIDEvent);
  virtual BOOL OnInitDialog();
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
protected:
  virtual void OnOK();
  virtual void OnCancel();
  void ReadViewerState();
  static void SetFloatValue(CEdit &edt, float val);
public:
  afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);

  void LoadPresets();
  afx_msg void OnBnClickedSavePreset();
  afx_msg void OnBnClickedDelPreset();
  afx_msg void OnCbnEditchangePresets();

  static bool LoadProfile(const char *name, EnvInfoStruct &envInfo);
  afx_msg void OnCbnSelendokPresets();
  afx_msg void OnBnClickedHdrenable();
  afx_msg void OnEnChangeOFH();
  afx_msg void OnDtnDatetimechangeTimeofdaynum(NMHDR *pNMHDR, LRESULT *pResult);
};
