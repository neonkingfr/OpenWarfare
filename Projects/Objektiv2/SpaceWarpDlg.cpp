// SpaceWarpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "SpaceWarpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSpaceWarpDlg dialog


CSpaceWarpDlg::CSpaceWarpDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CSpaceWarpDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CSpaceWarpDlg)
    SaveToSel = FALSE;
    Distance = 0.0f;
    End = 0;
    Begin = 0;
    //}}AFX_DATA_INIT
    odata=NULL;
    }

//--------------------------------------------------

void CSpaceWarpDlg::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_EDIT4,IDC_EDIT2,IDC_EDIT1,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CSpaceWarpDlg)
  DDX_Control(pDX, IDC_FUNCTION, Funct);
  DDX_Control(pDX, IDC_SELECTSEL, SelList);
  DDX_Control(pDX, IDC_SPIN3, Spin3);
  DDX_Control(pDX, IDC_SPIN2, Spin2);
  DDX_Control(pDX, IDC_SPIN1, Spin1);
  DDX_Check(pDX, IDC_SAVETOSEL, SaveToSel);
  DDX_Text(pDX, IDC_EDIT4, Distance);
  DDV_MinMaxFloat(pDX, Distance, 0.f, 1.e+038f);
  DDX_Text(pDX, IDC_EDIT2, End);
  DDV_MinMaxUInt(pDX, End, 0, 100);
  DDX_Text(pDX, IDC_EDIT1, Begin);
  DDV_MinMaxUInt(pDX, Begin, 0, 100);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CSpaceWarpDlg, CDialog)
  //{{AFX_MSG_MAP(CSpaceWarpDlg)
  ON_WM_DRAWITEM()
    ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN1, OnDeltaposSpin1)
      ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN2, OnDeltaposSpin2)
        ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN3, OnDeltaposSpin3)
          ON_EN_CHANGE(IDC_EDIT1, OnChangeEdit)
            ON_EN_CHANGE(IDC_EDIT2, OnChangeEdit)
              ON_EN_CHANGE(IDC_EDIT4, OnChangeEdit)
                //}}AFX_MSG_MAP
                END_MESSAGE_MAP()
                  
                  /////////////////////////////////////////////////////////////////////////////
                  // CSpaceWarpDlg message handlers
                  
                  BOOL CSpaceWarpDlg::OnInitDialog() 
                    {
                    CDialog::OnInitDialog();
                    
                    // TODO: Add extra initialization here
                    if (odata==NULL) 
                      {PostMessage(WM_COMMAND,IDCANCEL,0);return FALSE;}
                    int i;
                    for (i=0;i<MAX_NAMED_SEL;i++)
                      {
                      NamedSelection *nm=odata->GetNamedSel(i);
                      if (nm!=NULL)		
                        SelList.SetItemData(SelList.AddString(nm->Name()),i);		
                      }
                    i=SelList.AddString(WString(IDS_ALLPOINTS));
                    SelList.SetItemData(i,-1);
                    SelList.SetCurSel(i);
                    return TRUE;  // return TRUE unless you set the focus to a control
                    // EXCEPTION: OCX Property Pages should return FALSE
                    }

//--------------------------------------------------

float CSpaceWarpDlg::CalculateWeight(float dist, bool norm)
  {
  float f;
  if (!norm)f=dist/Distance;else f=dist;
  float a=(float)Begin+1.0f;
  float b=(float)End+1.0f;
  float w1=(float)exp(a*log(f));
  float w2=1.0f-(float)exp(b*log(1.0f-f));
  return 1.0-(w1+w2)*0.5f;
  }

//--------------------------------------------------

void CSpaceWarpDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
  {
  if (nIDCtl==IDC_FUNCTION)
    {
    RECT &rc=lpDrawItemStruct->rcItem;
    int x=rc.left;
    int y=rc.top;
    int xs=rc.right-rc.left-1;
    int ys=rc.bottom-rc.top-1;
    CDC *pDC=CDC::FromHandle(lpDrawItemStruct->hDC);
    for (int i=0;i<xs;i++)
      {
      float f=(float)i/(float)xs;
      float z=CalculateWeight(f,true);
      int y=(int)((1.0-z)*ys+0.5f);
      pDC->SetPixel(i,y,0);
      }
    }
  else
    CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
  }

//--------------------------------------------------

void CSpaceWarpDlg::OnDeltaposSpin1(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
  // TODO: Add your control notification handler code here
  
  if ((int)Begin-pNMUpDown->iDelta<0) Begin=0;
  else if ((int)Begin-pNMUpDown->iDelta>100) Begin=100;
  else Begin-=pNMUpDown->iDelta;
  UpdateData(FALSE);
  Funct.Invalidate();
  *pResult = 0;
  }

//--------------------------------------------------

void CSpaceWarpDlg::OnDeltaposSpin2(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
  // TODO: Add your control notification handler code here
  if ((int)End<pNMUpDown->iDelta) End=0;
  else if ((int)End-pNMUpDown->iDelta>100) End=100;
  else End-=pNMUpDown->iDelta;
  UpdateData(FALSE);
  Funct.Invalidate();
  *pResult = 0;
  }

//--------------------------------------------------

void CSpaceWarpDlg::OnDeltaposSpin3(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
  // TODO: Add your control notification handler code here
  Distance-=pNMUpDown->iDelta*0.01f;
  if (Distance<0.0f) Distance=0.0f;
  UpdateData(FALSE);
  
  *pResult = 0;
  }

//--------------------------------------------------

void CSpaceWarpDlg::OnChangeEdit() 
  {
  UpdateData(TRUE);
  Funct.Invalidate();
  
  // TODO: If this is a RICHEDIT control, the control will not
  // send this notification unless you override the CDialog::OnInitDialog()
  // function and call CRichEditCtrl().SetEventMask()
  // with the ENM_CHANGE flag ORed into the mask.
  
  // TODO: Add your control notification handler code here
  
  }

//--------------------------------------------------

CSpaceWarpDlg * CSpaceWarpDlg::Clone()
  {
  CSpaceWarpDlg *dlg=new CSpaceWarpDlg();
  dlg->Begin=Begin;
  dlg->End=End;
  dlg->Distance=Distance;
  dlg->selsel=selsel;
  dlg->SaveToSel=SaveToSel;
  return dlg;
  }

//--------------------------------------------------

void CSpaceWarpDlg::OnOK() 
  {
  int i=SelList.GetCurSel();
  int p=SelList.GetItemData(i);
  if (p==-1) selsel="";
  else selsel=odata->NamedSel(i);
  CDialog::OnOK();
  }

//--------------------------------------------------

int CSpaceWarpDlg::Run(ObjectData *obj)
  {
  int pcnts=obj->GetSelection()->NPoints();
  int pcnt=obj->NPoints();
  Selection *insel=NULL;
  Selection newsl(obj);
  if (selsel!="") insel=obj->GetNamedSel(selsel);
  PosT **pts=new PosT *[pcnts];
  int i,j;
  for (i=0,j=0;i<pcnt;i++) if (obj->PointSelected(i))
    {pts[j++]=&obj->Point(i);newsl.SetPointWeight(i,obj->GetSelection()->PointWeight(i));}
  for (i=0;i<pcnt;i++) if (insel==NULL || insel->PointSelected(i))
    if (!obj->PointSelected(i))
      {
      HVECTOR v1;
      float mdist=0.0f;
      bool set=true;
      PosT &ps=obj->Point(i);
      CopyVektor(v1,mxVector3(ps[0],ps[1],ps[2]));
      for (j=0;j<pcnts;j++)
        {
        LPHVECTOR v2=mxVector3((*pts[j])[0],(*pts[j])[1],(*pts[j])[2]);
        RozdilVektoru(v2,v1);
        float m=ModulVektoru2(v2);
        if (set || mdist>m) mdist=m;
        set=false;
        }
      mdist=sqrt(mdist);
      if (mdist<Distance)	newsl.SetPointWeight(i,CalculateWeight(mdist,false));
      }
  insel=(Selection *)obj->GetSelection();
  *insel=newsl;
  obj->SelectFacesFromPoints();
  if (SaveToSel) 
    {
    odata->DeleteNamedSel(selsel);
    odata->SaveNamedSel(selsel);
    }
  delete [] pts;
  return 0;
  }

//--------------------------------------------------

