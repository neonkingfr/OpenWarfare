#if !defined(AFX_MATCHDIRECTION_H__798B3E92_680A_4A2B_B56B_9E14ACFA9D6F__INCLUDED_)
#define AFX_MATCHDIRECTION_H__798B3E92_680A_4A2B_B56B_9E14ACFA9D6F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MatchDirection.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMatchDirection dialog

class CMatchDirection : public CDialog
  {
  // Construction
  public:
    void DlgControl();
    void LoadParams();
    void SaveParams();
    CMatchDirection(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CMatchDirection)
    enum 
      { IDD = IDD_MATCHDIRECTION };
    CButton	Browse;
    BOOL	make_backup;
    CString	proxy;
    CString	src_file;
    float	xdir;
    CString	xrot_center;
    CString	xrot_name;
    float	ydir;
    float	zdir;
    CString	zrot_center;
    CString	zrot_name;
    BOOL	preview;
    float	xdir2;
    float	ydir2;
    float	zdir2;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMatchDirection)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CMatchDirection)
    virtual BOOL OnInitDialog();
    virtual void OnOK();
    afx_msg void OnBrowse();
    afx_msg void OnPrew();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MATCHDIRECTION_H__798B3E92_680A_4A2B_B56B_9E14ACFA9D6F__INCLUDED_)
