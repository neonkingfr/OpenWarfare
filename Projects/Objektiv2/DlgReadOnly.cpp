// DlgReadOnly.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "DlgReadOnly.h"
#include <io.h>
#include <malloc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgReadOnly dialog


CDlgReadOnly::CDlgReadOnly(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgReadOnly::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgReadOnly)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgReadOnly::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgReadOnly)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgReadOnly, CDialog)
	//{{AFX_MSG_MAP(CDlgReadOnly)
	ON_BN_CLICKED(IDC_RUNEXPLORER, OnRunexplorer)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgReadOnly message handlers

int CDlgReadOnly::CheckFileRO(const char *filename,bool enableSaveAs)
  {
  CDlgReadOnly self;
  self._fname=filename;
  self._saveAs=enableSaveAs;
  if (_access(self._fname,0)==-1) return 1;
  if (_access(self._fname,06)==0) return 1;
  int ok=self.DoModal();
  if (ok==IDOK) return 0;
  else return -1;
  }

BOOL CDlgReadOnly::OnInitDialog() 
{
	CDialog::OnInitDialog();
    SetDlgItemText(IDC_FILENAME,_fname);
    GetDlgItem(IDOK)->EnableWindow(_saveAs);
    MessageBeep(MB_ICONEXCLAMATION);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgReadOnly::OnRunexplorer() 
  {
  char *s=(char *)alloca(strlen(_fname)+50);
  sprintf(s,"/select,\"%s\"",_fname);
  HINSTANCE hInst=ShellExecute(*this,"open","explorer",s,NULL,SW_SHOWDEFAULT);
  if ((int)hInst<=32) 
    {
    AfxMessageBox(IDS_UNKNOWNERROR);    
    }
  }
