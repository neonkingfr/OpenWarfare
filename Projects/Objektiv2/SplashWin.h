#if !defined(AFX_SPLASHWIN_H__F54D42C9_7DC1_4E17_893E_FDE6969A71CA__INCLUDED_)
#define AFX_SPLASHWIN_H__F54D42C9_7DC1_4E17_893E_FDE6969A71CA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SplashWin.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSplashWin window

class CSplashWin : public CWnd
  {
  // Construction
  public:
    CSplashWin();
    
    // Attributes
  public:
    CBitmap pic;
    
    // Operations
  public:
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CSplashWin)
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
    void Create();
    virtual ~CSplashWin();
    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CSplashWin)
    afx_msg void OnPaint();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPLASHWIN_H__F54D42C9_7DC1_4E17_893E_FDE6969A71CA__INCLUDED_)
