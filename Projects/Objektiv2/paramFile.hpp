#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PARAM_FILE_HPP
#define _PARAM_FILE_HPP

#include <Es/Strings/rstring.hpp>
#include <Es/Containers/array.hpp>
#include "qstream.hpp"
//#include "colors.hpp"

class ParamFile;

class SerializeBinStream;

/*
struct SoundPars : public SerializeClass
{
	RString name;
	float vol,freq;
	float volRnd,freqRnd;

	#ifndef ACCESS_ONLY
	LSError Serialize(ParamArchive &ar);
	#else
	LSError Serialize(ParamArchive &ar){return LSOK;}
	#endif
};

TypeIsMovableZeroed(SoundPars);

  */
class IParamArrayValue;

#include <Es/Memory/normalNew.hpp>

class ParamRawValue;

class ParamEntry: public RefCount
  {
  friend class ParamClass;
  
  protected:
    RStringB _name;
    //bool _overload;
    
    void SetName( const RStringB &name ) 
      {_name=name;}
    //void SetOverload( bool overload ) {_overload=overload;}
    
    Ref<ParamEntry> Merge( const ParamEntry *ovld ) const; // merge in base class/member
    
  public:
    explicit ParamEntry( const RStringB &name );
    const RStringB &GetName() const 
      {return _name;}
    RString GetContext( const char *member=NULL ) const; // fully qualified name
    
    virtual bool IsClass() const 
      {return false;}
    virtual bool IsArray() const 
      {return false;}
    
    //bool GetOverload() const {return _overload;}
    
    // value conversions
    virtual operator RStringB() const;
    virtual operator float() const;
    virtual operator int() const;
    virtual operator bool() const;
    virtual int GetInt() const;
    
    virtual void SetValue( const RStringB &string );
    virtual void SetValue( float val );
    virtual void SetValue( int val );
    virtual void SetFile(ParamFile *file) = NULL;
    
    virtual void Add( const RStringB &name, const RStringB &val );
    virtual void Add( const RStringB &name, float val );
    virtual ParamEntry *AddClass( const RStringB &name);
    virtual ParamEntry *AddArray( const RStringB &name);
    virtual void Clear();
    
    virtual void AddValue(float val);
    virtual void AddValue(int val);
    virtual void AddValue(bool val);
    virtual void AddValue(const RStringB &val);
    virtual void AddValue(const char *val);
    
    virtual int GetEntryCount() const;
    virtual const ParamEntry &GetEntry( int i ) const;
    
    //	virtual PackedColor GetPackedColor() const;
    //	virtual Color GetColor() const;
    //	virtual operator SoundPars() const;
    
    virtual const IParamArrayValue &operator [] ( int index ) const;
    virtual void SetValue( int index, const RStringB &string );
    virtual void SetValue( int index, float val );
    virtual void SetValue( int index, int val );
    virtual int GetSize() const;
    
    virtual ParamEntry *FindEntry( const char *name ) const;
    virtual const ParamEntry &operator >> ( const char *name ) const;
    //virtual const ParamEntry &operator >> ( RString name ) const;
    
    virtual void Save( QOStream &f, int indent ) const 
      {}
    
    //	virtual void SerializeBin(SerializeBinStream &f);
  };

//--------------------------------------------------

class IParamArrayValue: public RefCount //,public ParamRawValue
  {
  public:
    // param array value interface
    
    virtual RStringB GetValue() const = NULL;
    virtual float GetFloat() const = NULL;
    virtual int GetInt() const = NULL;
    
    virtual void SetValue(const RStringB &val )= NULL;
    virtual void SetValue(float val)= NULL;
    virtual void SetValue(int val)= NULL;
    virtual void SetFile(ParamFile *file)= NULL;
    
    virtual void Save(QOStream &f, int indent) const = NULL;
    //	virtual void SerializeBin(SerializeBinStream &f) = NULL;
    
    //	virtual PackedColor GetPackedColor() const = NULL;
    //	virtual SoundPars GetSoundPars() const = NULL;
    
    // some handy conversions using virtual functions
    operator float() const 
      {return GetFloat();}
    operator int() const 
      {return GetInt();}
    operator RStringB() const 
      {return GetValue();}
    operator bool() const 
      {return GetInt()!=0;}
    //	operator PackedColor() const {return GetPackedColor();}
    //	operator SoundPars() const {return GetSoundPars();}
    
    // may be array of values
    virtual const IParamArrayValue *GetItem(int i) const = NULL;
    virtual int GetItemCount() const = NULL;
    
    const IParamArrayValue &operator [] (int i) const 
      {return *GetItem(i);}
  };

//--------------------------------------------------

class ParamClass: public ParamEntry
  {
  friend class ParamEntry;
  friend class ParamFile;
  
  RefArray<ParamEntry> _entries;
  InitPtr<ParamClass> _base;
  
  InitPtr<ParamClass> _parent;
  
  public:
    bool IsClass() const 
      {return true;}
    
    const ParamClass *GetClass( const char *name ) const;
    RStringB GetValue( const char *name ) const;
    RStringB GetValue( const char *name, int i ) const;
    
    void Parse( QIStream &f, ParamFile *file); // read until close bracket
    void Save( QOStream &f, int indent ) const; // read until close bracket
    void SerializeBin(SerializeBinStream &f);
    
    void Update(ParamClass &source, ParamFile *file);
    
    ParamEntry *FindEntry( const char *name ) const;
    const ParamEntry &operator >> ( const char *name ) const;
    //const ParamEntry &operator >> ( RString name ) const;
    
    void Add( const RStringB &name, const RStringB &val );
    void Add( const RStringB &name, float val );
    void Add( const RStringB &name, int val );
    
    ParamEntry *AddClass( const RStringB &name);
    ParamEntry *AddArray( const RStringB &name);
    
    void SetFile(ParamFile *file);
    
    int GetEntryCount() const 
      {return _entries.Size();}
    const ParamEntry &GetEntry( int i ) const 
      {return *_entries[i];}
    const char *GetBaseName() const 
      {return _base.NotNull() ? _base->GetName() : NULL;}
    
    bool IsDerivedFrom( const ParamClass &parent ) const;
    
  protected:
    ParamClass();
    ParamClass( const RStringB &name );
    ~ParamClass();
    
    void NewEntry( Ref<ParamEntry> entry );
    
    ParamEntry *Find( const char *name, bool parent, bool base ) const;
    int FindIndex( const char *name ) const;
    
    void Diagnostics( int indent );
    
    //	USE_FAST_ALLOCATOR
  };

//--------------------------------------------------

class GameVarSpace;

class ParamFile: public ParamClass
  {
  protected:
    //	SRef<GameVarSpace> _vars;
    
  public:
    ParamFile();
    ~ParamFile();
    
    //	GameVarSpace *GetVariables() {return _vars;}
    
    LSError Parse( const char *name);
    LSError Save( const char *name ) const;
    void Parse( QIStream &f);
    void Save( QOStream &f, int ident ) const;
    
    bool ParseBin( const char *name);
    bool SaveBin( const char *name );
    
    bool ParseBinOrTxt(const char *name);
    //	bool ParseBin(QFBank &bank, const char *name);
    
    void SerializeBin(SerializeBinStream &f);
    
    void Clear();
    
    void Reload();
    
    //	USE_FAST_ALLOCATOR
  };

//--------------------------------------------------

#include <Es/Memory/debugNew.hpp>

extern ParamFile Pars;
extern ParamFile ExtParsCampaign;
extern ParamFile ExtParsMission;
extern ParamFile Res;

RString GetDefaultName( RString baseName, const char *dDir, const char *dExt );
RString GetAnimationName( RString baseName );
RString GetShapeName( RString baseName );
RString GetPictureName( RString baseName );
RString GetSoundName( RString baseName );
RString GetFontName( RString baseName );

#endif
