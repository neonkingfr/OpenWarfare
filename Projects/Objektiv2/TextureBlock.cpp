// TextureBlock.cpp: implementation of the CTextureBlock class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "stdafx.h"
#include "Objektiv2.h"
#include "TextureBlock.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CTextureBlock::~CTextureBlock()
  {
  Reset();
  }

//--------------------------------------------------

static DrawUchyt(CDC *pDC, int x, int y)
  {
  CBrush *black=CBrush::FromHandle((HBRUSH)::GetStockObject(BLACK_BRUSH));
  CRect rc(x-5,y-5,x+5,y+5);
  pDC->FillRect(rc,black);
  }

//--------------------------------------------------

void CTextureBlock::Draw(CDC *pDC, bool uchytky)
  {
  if (invalid) return;
  else
    {	
    CSize size=rect.Size();
    pDC->StretchBlt(rect.left,rect.top,size.cx,size.cy,&texdc,0,0,sizex,sizey,SRCCOPY);	
    }
  if (uchytky)
    {
    DrawUchyt(pDC,rect.left,rect.top);
    DrawUchyt(pDC,rect.right,rect.top);
    DrawUchyt(pDC,rect.right,rect.bottom);
    DrawUchyt(pDC,rect.left,rect.bottom);
    }
  }

//--------------------------------------------------

void CTextureBlock::SetBitmap(CDC *compdc, const char *bitmapname)
  {
  TUNIPICTURE *pic;
  BITMAPINFO *bi;  
  
  pic=cmLoadUni((char *)bitmapname);
  if (pic==NULL) return;
  bi=(BITMAPINFO *)cmUni2BitMap(pic,true,16384,16384);
  free(pic);
  if (bi==NULL) return;
  texbitmap.CreateCompatibleBitmap(compdc,bi->bmiHeader.biWidth,bi->bmiHeader.biHeight);
  texdc.CreateCompatibleDC(compdc);
  old=(HBITMAP)texdc.SelectObject(texbitmap);
  int offset=sizeof(bi->bmiHeader);
  if (bi->bmiHeader.biBitCount!=24) offset+=4;
  ::SetDIBitsToDevice(texdc.m_hDC,0,0,bi->bmiHeader.biWidth,bi->bmiHeader.biHeight,0,0,0,bi->bmiHeader.biHeight,(void *)((char *)bi+offset),bi,0); 
  free(bi);
  invalid=false;
  }

//--------------------------------------------------

void CTextureBlock::SetRect(CRect &rc)
  {
  rect=rc;
  rect.NormalizeRect();
  }

//--------------------------------------------------

void CTextureBlock::Reset()
  {
  if (!invalid)
    {
    texdc.SelectObject(CBitmap::FromHandle(old));
    invalid=true;
    texdc.DeleteDC();
    texbitmap.DeleteObject();
    }
  }

//--------------------------------------------------

