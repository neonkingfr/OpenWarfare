// Hierarchy.h: interface for the CHierarchy class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HIERARCHY_H__DBDE00F2_770A_4DB2_A818_475A71A2D7A3__INCLUDED_)
#define AFX_HIERARCHY_H__DBDE00F2_770A_4DB2_A818_475A71A2D7A3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "comint.h"

class CHierarchy  
  {
  public:
    
    struct SHierarchyItem
      {
      char *filename;	//filename with object
      char *shortname;	//short name in list
      LODObject *object;  //object in item
      CComInt *comstack; //command interface
      float previewlod; //lod for preview. -1 sets current
      HMATRIX mm; //hiearchy matrix
      HMATRIX curmatrix; //
      int flag; //flag to distribute.
      public:
        ~SHierarchyItem();
        SHierarchyItem();
      };
    SHierarchyItem *itemlist;	//list of items extendable
    int count;	  //count of object in hierachy
    int alloc;	  //allocated array
    int *order;	  //order of items in hierarchy
    int *parents;	  //list of parents in hierarchy
    
    bool Expand(int items);
    
    CHierarchy();
    virtual ~CHierarchy();
    SHierarchyItem *AddItem();
    int GetCount() 
      {return count;}
    void DeleteItem(int idx);
    SHierarchyItem& operator[] (int p) 
      {ASSERT(p>=0 && p<count);return itemlist[p];}
    void SetHiearchyOrder(int order, int parent, SHierarchyItem *item)
      {
      ASSERT(order>=0 && order<count);
      ASSERT(parent>=-1 && parent<count);
      ASSERT(item!=NULL);
      int idx=item-itemlist;
      this->order[order]=idx;
      parents[order]=idx;
      }
    void RecalcHierarchy();  
    void DistributeFlag(SHierarchyItem *start, int flagset, int flagchild);
  };

//--------------------------------------------------

#endif // !defined(AFX_HIERARCHY_H__DBDE00F2_770A_4DB2_A818_475A71A2D7A3__INCLUDED_)
