// GizmoTexWnd.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "GizmoTexWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGizmoTexWnd

float CGizmoTexWnd::fleft=0;
float CGizmoTexWnd::fright=1;
float CGizmoTexWnd::ftop=0;
float CGizmoTexWnd::fbottom=1;

#define UCHYT 5
#define ATTACH 0.05

CGizmoTexWnd::CGizmoTexWnd()
  {
  xmid=(fleft+fright)*0.5f;
  ymid=(fbottom+ftop)*0.5f;
  scale=1/(float)fabs(fright-fleft)*1.25f;
  texback=NULL;
  }

//--------------------------------------------------

CGizmoTexWnd::~CGizmoTexWnd()
  {
  free(texback);
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CGizmoTexWnd, CStatic)
  //{{AFX_MSG_MAP(CGizmoTexWnd)
  ON_WM_PAINT()
    ON_WM_DESTROY()
      ON_WM_SETCURSOR()
        ON_WM_LBUTTONDOWN()
          ON_WM_RBUTTONDOWN()
            ON_WM_LBUTTONUP()
              ON_WM_RBUTTONUP()
                ON_WM_MOUSEMOVE()
                  //}}AFX_MSG_MAP
                  END_MESSAGE_MAP()
                    
                    /////////////////////////////////////////////////////////////////////////////
                    // CGizmoTexWnd message handlers
                    
                    void CGizmoTexWnd::OnPaint() 
                      {
                      CPaintDC dc(this); // device context for painting
                      CRect wnd;
                      GetClientRect(&wnd);
                      
                      if (membmp.m_hObject==NULL)	membmp.CreateCompatibleBitmap(&dc,wnd.right,wnd.bottom);
                      if (memdc.m_hDC==NULL)
                        {
                        memdc.CreateCompatibleDC(&dc);
                        dcbmp=(HBITMAP)(memdc.SelectObject(&membmp)->m_hObject);
                        OnDrawToBitmap(&memdc,wnd.right,wnd.bottom);
                        }
                      dc.BitBlt(0,0,wnd.right,wnd.bottom,&memdc,0,0,SRCCOPY);
                      }

//--------------------------------------------------

inline void DrawUchyt(CDC *pDC, int x, int y)
  { 
  pDC->FillSolidRect(CRect(x-UCHYT,y-UCHYT,x+UCHYT,y+UCHYT),RGB(255,120,120));
  }

//--------------------------------------------------

inline void AlfaRectangle(CDC &dc,CRect &rc)
  {
  rc.NormalizeRect();
  int p=dc.SaveDC();
  CSize sz=rc.Size();
  dc.IntersectClipRect(rc);
  for(int x=rc.left-sz.cy;x<rc.right;x+=2)
    {
    int y1=rc.bottom;
    int x2=x+sz.cy;
    dc.MoveTo(x,rc.top);dc.LineTo(x2,y1);
    }
  dc.RestoreDC(p);
  }

//--------------------------------------------------

void CGizmoTexWnd::OnDrawToBitmap(CDC *dc,int width, int height)
  {
  dc->FillSolidRect(0,0,width,height,GetSysColor(COLOR_WINDOW));
  LPHMATRIX out=disp;
    {
    HMATRIX m1,m2,mm,mn;
    Zoom(m2,scale,scale,1);
    Translace(m1,-xmid,-ymid,0);
    SoucinMatic(m1,m2,mm);
    Zoom(m1,width*0.5f,height*0.5f,1);
    Translace(m2,width*0.5f,height*0.5f,0);
    SoucinMatic(m1,m2,mn);
    SoucinMatic(mm,mn,out);
    }
  HVECTOR vmin,vmax;
    {
    LPHMATRIX inv=invdisp;
    HVECTOR v1;
    InverzeMatice(out,inv);
    CopyVektor(v1,mxVector3(0,0,0));
    TransformVector(inv,v1,vmin);
    CopyVektor(v1,mxVector3(width,height,0));
    TransformVector(inv,v1,vmax);
    }
  HVECTOR leftop,righbot;
    {
    HVECTOR v1;
    CopyVektor(v1,mxVector3(fleft,ftop,0));
    TransformVector(out,v1,leftop);
    CopyVektor(v1,mxVector3(fright,fbottom,0));
    TransformVector(out,v1,righbot);
    }
  vmin[XVAL]=(float)ceil(vmin[XVAL]);
  vmin[YVAL]=(float)ceil(vmin[YVAL]);
  vmax[XVAL]=(float)floor(vmax[XVAL]);
  vmax[YVAL]=(float)floor(vmax[YVAL]);
  float cykl;
  CPen gray(PS_DOT,1,GetSysColor(COLOR_GRAYTEXT));
  CPen marked(PS_DOT,1,GetSysColor(COLOR_HIGHLIGHT));
  CPen black(PS_SOLID,3,GetSysColor(COLOR_WINDOWTEXT));
  CPen green(PS_SOLID,3,RGB(0,0,200));
  CPen white(PS_SOLID,1,GetSysColor(COLOR_DESKTOP));
  CPen *pold;
  
  // CBrush *bold;
  pold=dc->SelectObject(&gray);
  //  bold=dc->SelectObject(&fill);
  int xl=(int)leftop[XVAL],yl=(int)leftop[YVAL],xr=(int)righbot[XVAL],yr=(int)righbot[YVAL];
  dc->SetBrushOrg(xl&7,yl&7);  
  if (texback)
    {
    HVECTOR vect;	
    HVECTOR tvmin,tvmax;
    if (fleft<fright)
      {
      tvmin[XVAL]=(float)floor(fleft);
      tvmax[XVAL]=(float)ceil(fright);
      }
    else
      {
      tvmax[XVAL]=(float)ceil(fleft);
      tvmin[XVAL]=(float)floor(fright);
      }
    if (ftop<fbottom)
      {
      tvmin[YVAL]=(float)floor(ftop);
      tvmax[YVAL]=(float)ceil(fbottom);	
      }
    else
      {
      tvmax[YVAL]=(float)ceil(ftop);
      tvmin[YVAL]=(float)floor(fbottom);	
      }
    
    vect[WVAL]=1.0f;
    vect[ZVAL]=0.0f;
    /*	dc->SelectStockObject(NULL_PEN);
	dc->SetTextColor(0x0);
	dc->SetBkColor(0xFFFFFF);
	int x1=xl,y1=yl,x2=xr,y2=yr;
	if (x1>x2) {x1+=x2;x2=x1-x2;x1=x1-x2;}
	if (y1>y2) {y1+=y2;y2=y1-y2;y1=y1-y2;}
	dc->Rectangle(0,0,width,y1);
	dc->Rectangle(0,y2,width,height);
	dc->Rectangle(0,y1,x1,y2);
	dc->Rectangle(x2,y1,width,y2);*/
    
    for (vect[XVAL]=tvmin[XVAL];vect[XVAL]<tvmax[XVAL];vect[XVAL]+=1.0f)
      for (vect[YVAL]=tvmin[YVAL];vect[YVAL]<tvmax[YVAL];vect[YVAL]+=1.0f)
        {
        HVECTOR v2;
        HVECTOR tv1,tv2;
        v2[WVAL]=1.0f;
        v2[ZVAL]=0.0f;
        v2[XVAL]=vect[XVAL]+1;
        v2[YVAL]=vect[YVAL]+1;
        TransformVector(out,vect,tv1);
        TransformVector(out,v2,tv2);
        RozdilVektoru(tv2,tv1);
        SetStretchBltMode(*dc,hq?STRETCH_HALFTONE:COLORONCOLOR);
        StretchDIBits(*dc,(int)tv1[XVAL],(int)tv1[YVAL],(int)tv2[XVAL],(int)tv2[YVAL],
        0,0,texback->bmiHeader.biWidth,texback->bmiHeader.biHeight,
        (void *)((char *)texback+sizeof(BITMAPINFO)),texback,DIB_RGB_COLORS,SRCCOPY);
        }
    }
    {
    for (cykl=vmin[XVAL];cykl<=vmax[XVAL];cykl+=1.0f)
      {
      HVECTOR v1,v2;
      v1[XVAL]=cykl,v1[YVAL]=0;v1[ZVAL]=0;v1[WVAL]=1.0f;
      TransformVector(out,v1,v2);
      if (cykl<0.1 && cykl>-0.1) dc->SelectObject(&marked);else dc->SelectObject(&gray);
      dc->MoveTo((int)v2[XVAL],0);
      dc->LineTo((int)v2[XVAL],height);
      }
    for (cykl=vmin[YVAL];cykl<=vmax[YVAL];cykl+=1.0f)
      {
      HVECTOR v1,v2;
      v1[XVAL]=0;v1[YVAL]=cykl;v1[ZVAL]=0;v1[WVAL]=1.0f;
      TransformVector(out,v1,v2);
      if (cykl<0.1 && cykl>-0.1) dc->SelectObject(&marked);else dc->SelectObject(&gray);
      dc->MoveTo(0,(int)v2[YVAL]);
      dc->LineTo(width,(int)v2[YVAL]);
      }
    }
  dc->SelectObject(&white);
  dc->SelectStockObject(HOLLOW_BRUSH);
  AlfaRectangle(*dc,CRect(xl,yl,xr,yr));
  dc->Rectangle(xl,yl,xr,yr);
  dc->SelectObject(&green);
  int modf=(yr>yl)?10:-10;
  dc->MoveTo(xl,yr);
  dc->LineTo(xl,yl);
  dc->MoveTo(xl+modf,yl+modf);dc->LineTo(xl,yl);dc->LineTo(xl-modf,yl+modf);
  DrawUchyt(dc,xl,yl);DrawUchyt(dc,xr,yr);
  DrawUchyt(dc,xr,yl);DrawUchyt(dc,xl,yr);
  dc->SelectObject(pold);
  //  dc->SelectObject(bold);  
  }

//--------------------------------------------------

void CGizmoTexWnd::Redraw()
  {
  if (memdc.m_hDC!=NULL)
    {
    SelectObject(memdc,dcbmp);
    membmp.DeleteObject();
    memdc.DeleteDC();
    }
  Invalidate(FALSE);
  }

//--------------------------------------------------

CGizmoTexWnd::MousePos CGizmoTexWnd::WhereMouse(CPoint &pt,LPHVECTOR pos)
  {
  HVECTOR v1;
  v1[XVAL]=pt.x;
  v1[YVAL]=pt.y;
  v1[ZVAL]=0;
  v1[WVAL]=1;
  TransformVector(invdisp,v1,pos);
  HVECTOR leftop,righbot;
    {
    HVECTOR v1;
    CopyVektor(v1,mxVector3(fleft,ftop,0));
    TransformVector(disp,v1,leftop);
    CopyVektor(v1,mxVector3(fright,fbottom,0));
    TransformVector(disp,v1,righbot);
    }
  if (fabs(leftop[XVAL]-pt.x)<=UCHYT)
    if (fabs(leftop[YVAL]-pt.y)<=UCHYT) return left_top;
  else if (fabs(righbot[YVAL]-pt.y)<=UCHYT) return left_bottom;
  else if ((pt.y>leftop[YVAL] && pt.y<righbot[YVAL])||
    (pt.y<leftop[YVAL] && pt.y>righbot[YVAL])) return left;
  else return outside;
  if (fabs(righbot[XVAL]-pt.x)<=UCHYT)
    if (fabs(leftop[YVAL]-pt.y)<=UCHYT) return right_top;
  else if (fabs(righbot[YVAL]-pt.y)<=UCHYT) return right_bottom;
  else if ((pt.y>leftop[YVAL] && pt.y<righbot[YVAL])||
    (pt.y<leftop[YVAL] && pt.y>righbot[YVAL])) return right;
  else return outside;
  if (fabs(leftop[YVAL]-pt.y)<=UCHYT && 
    ((pt.x>leftop[XVAL] && pt.x<righbot[XVAL])||
      (pt.x<leftop[XVAL] && pt.x>righbot[XVAL]))) return top;
  if (fabs(righbot[YVAL]-pt.y)<=UCHYT && 
    ((pt.x>leftop[XVAL] && pt.x<righbot[XVAL])||
      (pt.x<leftop[XVAL] && pt.x>righbot[XVAL]))) return bottom;
  if ((pt.x>leftop[XVAL] && pt.x<righbot[XVAL] ||
    pt.x<leftop[XVAL] && pt.x>righbot[XVAL]) &&
      (pt.y>leftop[YVAL] && pt.y<righbot[YVAL] ||	
        pt.y<leftop[YVAL] && pt.y>righbot[YVAL]) )	
          return inside;
  else 
    return outside;
  }

//--------------------------------------------------

void CGizmoTexWnd::OnDestroy() 
  {
  Redraw();
  CStatic::OnDestroy();
  }

//--------------------------------------------------

BOOL CGizmoTexWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
  {
  CPoint pt(GetMessagePos());
  ScreenToClient(&pt);
  HVECTOR tv;
  MousePos pos=WhereMouse(pt,tv);
  char *cursor;
  switch (pos)
    {
    case left:
    case right: cursor=IDC_SIZEWE;break;
    case top:
    case bottom: cursor=IDC_SIZENS;break;
    case left_top:
    case right_bottom: cursor=IDC_SIZENWSE;break;
    case right_top:
    case left_bottom: cursor=IDC_SIZENESW;break;
    case inside: cursor=IDC_SIZEALL;break;
    default:cursor=IDC_CROSS;break;
    }
  ::SetCursor(::LoadCursor(NULL,cursor));
  return FALSE;
  }

//--------------------------------------------------

void CGizmoTexWnd::OnLButtonDown(UINT nFlags, CPoint point) 
  {
  if (GetCapture()==this) return;
  SetCapture();
  altpress=(GetKeyState(VK_MENU) & 0x80)!=0;
  HVECTOR temp;
  lastpos=WhereMouse(point,temp);
  hq=false;
  last=point;
  }

//--------------------------------------------------

void CGizmoTexWnd::OnRButtonDown(UINT nFlags, CPoint point) 
  {
  if (GetCapture()==this) return;
  SetCapture();
  altpress=(GetKeyState(VK_MENU) & 0x80)!=0;
  HVECTOR temp;
  lastpos=WhereMouse(point,temp);
  hq=false;
  last=point;
  }

//--------------------------------------------------

void CGizmoTexWnd::OnLButtonUp(UINT nFlags, CPoint point) 
  {
  hq=true;
  if (GetCapture()==this)
    {
    ReleaseCapture();
    Redraw();
    }
  
  CStatic::OnLButtonUp(nFlags, point);
  }

//--------------------------------------------------

void CGizmoTexWnd::OnRButtonUp(UINT nFlags, CPoint point) 
  {
  hq=true;
  if (GetCapture()==this)
    {
    ReleaseCapture();
    Redraw();
    }
  
  CStatic::OnRButtonUp(nFlags, point);
  }

//--------------------------------------------------

void CGizmoTexWnd::OnMouseMove(UINT nFlags, CPoint point) 
  {  
  if (GetCapture()==this)
    {		
    if (altpress)
      {
      PointDiff(point);
      if ((nFlags & MK_LBUTTON) && (nFlags & MK_RBUTTON))		
        scale*=exp((-point.y)*0.01);
      else
        {
        xmid-=point.x/(scale*100);
        ymid-=point.y/(scale*100);
        }/*
	  if (point.x || point.y)
		{	
		CPoint p=last-point;
		ClientToScreen(&p);
		SetCursorPos(p.x,p.y);
		}*/
      }
    else
      {
      HVECTOR z;
      WhereMouse(point,z);
      if (lastpos!=outside && lastpos!=inside)
        {
        if (!(nFlags & MK_CONTROL) )
          {
          if (z[XVAL]-floor(z[XVAL])<ATTACH) z[XVAL]=floor(z[XVAL]);
          if (ceil(z[XVAL])-z[XVAL]<ATTACH) z[XVAL]=ceil(z[XVAL]);
          if (z[YVAL]-floor(z[YVAL])<ATTACH) z[YVAL]=floor(z[YVAL]);
          if (ceil(z[YVAL])-z[YVAL]<ATTACH) z[YVAL]=ceil(z[YVAL]);
          }
        switch (lastpos)
          {
          case left:
          case left_bottom:
          case left_top: fleft=z[XVAL];break;
          case right:
          case right_bottom:
          case right_top: fright=z[XVAL];break;
          }
        switch (lastpos)
          {
          case top:
          case left_top:
          case right_top: ftop=z[YVAL];break;
          case bottom:
          case right_bottom:
          case left_bottom: fbottom=z[YVAL];break;
          }
        }
      else if (lastpos==inside)
        {
        HVECTOR l;
        WhereMouse(last,l);
        last=point;
        RozdilVektoru(z,l);
        fleft+=z[XVAL];
        fright+=z[XVAL];
        ftop+=z[YVAL];
        fbottom+=z[YVAL];
        }
      else 
        {
        HVECTOR l;
        WhereMouse(last,l);
        fleft=l[XVAL];
        fright=z[XVAL];
        ftop=l[YVAL];
        fbottom=z[YVAL];		  		  
        }
      GetParent()->PostMessage(WM_COMMAND,GetWindowLong(*this,GWL_ID),(LPARAM)this->m_hWnd);
      }
    
    Redraw();
    UpdateWindow();
    }
  }

//--------------------------------------------------

void CGizmoTexWnd::LoadBitmap(const char *filename)
  { 
  free(texback);
  texback=NULL;
  if (filename==NULL || filename[0]==0) return;
  Pathname s;
  s.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
  s.SetFilename(filename);
  TUNIPICTURE *pic=cmLoadUni(const_cast<char *>(s.GetFullPath()));  
  if (pic==NULL) return;
  texback=(BITMAPINFO *)cmUni2BitMap(pic,true,512,512);
  free(pic);
  Redraw();
  }

//--------------------------------------------------

