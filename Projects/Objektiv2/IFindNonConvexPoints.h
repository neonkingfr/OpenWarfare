#pragma once
#include "comint.h"

class CIFindNonConvexPoints :
  public CICommand
  {
  public:
    CIFindNonConvexPoints(void);
    ~CIFindNonConvexPoints(void);
    virtual int Execute(LODObject * lodobj);
  };

class CIMakeConvex: public CICommand
  {
  public:
  CIMakeConvex(void);
  ~CIMakeConvex(void);
    virtual int Execute(LODObject * lodobj);
  };