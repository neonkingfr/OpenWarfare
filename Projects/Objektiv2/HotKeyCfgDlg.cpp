// HotKeyCfgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "HotKeyCfgDlg.h"
#include "MainFrm.h"
#include <malloc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHotKeyCfgDlg dialog


CHotKeyCfgDlg::CHotKeyCfgDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHotKeyCfgDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHotKeyCfgDlg)
	vAlt = FALSE;
	vControl = FALSE;
	vShift = FALSE;
	vHotkey = -1;
	//}}AFX_DATA_INIT
_wnd=NULL;
_mnu=NULL;
}


void CHotKeyCfgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHotKeyCfgDlg)
	DDX_Control(pDX, IDC_HOTKEY, wHotkey);
	DDX_Control(pDX, IDC_ITEMNAME, wItemName);
	DDX_Check(pDX, IDC_FALT, vAlt);
	DDX_Check(pDX, IDC_FCONTROL, vControl);
	DDX_Check(pDX, IDC_FSHIFT, vShift);
	DDX_CBIndex(pDX, IDC_HOTKEY, vHotkey);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHotKeyCfgDlg, CDialog)
	//{{AFX_MSG_MAP(CHotKeyCfgDlg)
	ON_BN_CLICKED(IDC_RESET, OnReset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHotKeyCfgDlg message handlers

static HMENU FindCommandInMenu(HMENU mnu, int command)
  {
  int cnt=GetMenuItemCount(mnu);
  for (int i=0;i<cnt;i++)
	{
	if (GetSubMenu(mnu,i)!=NULL) 
	  {
	  HMENU fnd=FindCommandInMenu(GetSubMenu(mnu,i),command);
	  if (fnd) return fnd;
	  }
	}
  MENUITEMINFO nfo;
  nfo.cbSize=sizeof(nfo);
  nfo.fMask=MIIM_ID;
  if (::GetMenuItemInfo(mnu,command,FALSE,&nfo)==TRUE) return mnu;
  return NULL;
  }

void CHotKeyCfgDlg::OnCommandIncomed(CFrameWnd *wnd, int command)
  {  
  if (command>=1010 && command<=1200) return;
  HACCEL haccel=wnd->GetDefaultAccelerator();
  if (haccel==NULL) return;
  _wnd=wnd;
  int accls=CopyAcceleratorTable(haccel,NULL,0);
  ACCEL *accel=(ACCEL *)alloca(sizeof(ACCEL)*accls);
  CopyAcceleratorTable(haccel,accel,accls);
  int i;
  for (i=0;i<accls;i++)
	if (accel[i].cmd==command)
	  {	  
	  vAlt=(accel[i].fVirt & FALT)!=0;
	  vShift=(accel[i].fVirt & FSHIFT)!=0;
	  vControl=(accel[i].fVirt & FCONTROL)!=0;
	  int cnt=wHotkey.GetCount();
	  int sel;
	  for (sel=0;sel<cnt;sel++) if (wHotkey.GetItemData(sel)==accel[i].key) break;
	  vHotkey=sel;
	  UpdateData(FALSE);
	  break;
	  }
  if (i==accls)
	{
	vAlt=vShift=vControl=FALSE;
	vHotkey=0;
    UpdateData(FALSE);
	}
  _mnu=*wnd->GetMenu();
  HMENU pmnu=FindCommandInMenu(_mnu,command);
  if (pmnu)
	{
	char buff[256];
	GetMenuString(pmnu,command,buff,254,MF_BYCOMMAND);
	if (buff[0]) wItemName.SetWindowText(buff);
	}
  else
	{
	CString unknown;
	unknown.Format(IDS_UNKNOWNMENUITEM,command);
	wItemName.SetWindowText(unknown);
	}
  _command=command;
  GetDlgItem(IDOK)->EnableWindow(TRUE);
  BringWindowToTop();
  }

void CHotKeyCfgDlg::OnCancel() 
{
	
	DestroyWindow();
}

void CHotKeyCfgDlg::OnOK() 
  {
  UpdateData(TRUE);
  HACCEL haccel=_wnd->GetDefaultAccelerator();
  if (haccel==NULL) return;
  int accls=CopyAcceleratorTable(haccel,NULL,0);
  ACCEL *accel=(ACCEL *)alloca(sizeof(ACCEL)*(accls+1));
  CopyAcceleratorTable(haccel,accel,accls);
  WORD key;
  key=wHotkey.GetItemData(vHotkey);
  int flags=FVIRTKEY;  
  if (vAlt) flags|=FALT;
  if (vControl) flags |=FCONTROL;
  if (vShift) flags|=FSHIFT;
  int i;
  for (i=0;i<accls;i++)
	{
	if ((accel[i].fVirt & (FVIRTKEY | FALT | FCONTROL | FSHIFT))==flags
	  && accel[i].key==key)
	  {
	  HMENU pmnu=FindCommandInMenu(_mnu,accel[i].cmd);
	  CString kass;	  
	  char *buff=(char *)alloca(256);
	  if (pmnu)		
		GetMenuString(pmnu,accel[i].cmd,buff,256,MF_BYCOMMAND);
	  else
		buff[0]=0;
	  kass.Format(IDS_HOTKEYEXISTS,buff);
	  if (AfxMessageBox(kass,MB_YESNO|MB_ICONQUESTION)==IDYES)
		{
		accel[i].cmd=_command;
		goto skip;
		}
	  else
		return;
	  }
	}
  for (i=0;i<accls;i++)
	if (accel[i].cmd==_command) break;
  if (key==0)
	{
	if (i!=accls)
	  {
	  memcpy(accel+i,accel+i+1,(accls-i-1)*sizeof(ACCEL));
	  accls--;
	  }
	}
  else
	{
	accel[i].fVirt=flags;
	accel[i].key=key;
	accel[i].cmd=_command;
	if (i==accls) accls++;
	}
  skip:;
  _wnd->SendMessage(HKK_ACCELCHANGED,accls,(LPARAM)accel);
  HMENU pmnu=FindCommandInMenu(_mnu,_command);
  if (pmnu)
	{
	char buff[256];
	GetMenuString(pmnu,_command,buff,254,MF_BYCOMMAND);
	if (buff[0]) wItemName.SetWindowText(buff);
	}
  }

void CHotKeyCfgDlg::OnReset() 
  {
  if (AfxMessageBox(IDS_HOTKEYRESET,MB_YESNO|MB_ICONEXCLAMATION)==IDNO) return;
  _wnd->SendMessage(HKK_ACCELCHANGED,NULL,NULL);  
  }


static char *vk_table1[]=
  {
  "<None>",
  "~Mouse Left",
  "~Mouse Right",
  "~Cancel",
  "~Mouse Middle",
  "~05 undefined",
  "~06 undefined",
  "~07 undefined",
  "Back",
  "Tab",
  "~0A undefined",
  "~0B undefined",
  "Clear",
  "Enter",
  "~0E undefined",
  "~0F undefined",
  "Shift",
  "Control", 
  "Alt", 
  "Pause", 
  "Caps Lock", 
  "~15 undefined",
  "~16 undefined",
  "~17 undefined",
  "~18 undefined",
  "~19 undefined",
  "~1A undefined",
  "Esc",
  "~1C undefined",
  "~1D undefined",
  "~1E undefined",
  "~1F undefined",
  "Space",
  "PageUp",
  "PageDown",
  "End",
  "Home", 
  "Left",
  "Up",
  "Right",
  "Down",
  "Select", 
  "~Print", 
  "~Execute", 
  "~SysRq",
  "Ins", 
  "Del",
  "Help"};

static char *vk_table2[]=
  {
  "Num 0",
  "Num 1",
  "Num 2",
  "Num 3",
  "Num 4",
  "Num 5",
  "Num 6",
  "Num 7",
  "Num 8",
  "Num 9",
  "Num *",
  "Num +",
  "~Separator",
  "Num -",
  "Num Del",
  "Num /",
  "F1",
  "F2",
  "F3",
  "F4",
  "F5",
  "F6",
  "F7",
  "F8",
  "F9",
  "F10",
  "F11",
  "F12",
  "F13",
  "F14",
  "F15",
  "F16",
  "F17",
  "F18",
  "F19",
  "F20",
  "F21",
  "F22",
  "F23",
  "F24",
  "~88h",
  "~89h",
  "~8ah",
  "~8bh",
  "~8ch",
  "~8dh",
  "~8eh",
  "~8fh",
  "Num Lock",
  "Scroll Lock",
  };
 

static void ReplaceMenuHotkeys(ACCEL *lpaccel, int acccnt, HMENU mnu)
  {
  int cnt=GetMenuItemCount(mnu);
  char *buff=NULL;
  for (int i=0;i<cnt;i++)
	{
	if (GetSubMenu(mnu,i)!=NULL) 
  	  {
	  ReplaceMenuHotkeys(lpaccel,acccnt,GetSubMenu(mnu,i));
	  }	
	else
	  {
	  int command=GetMenuItemID(mnu,i);
	  int j;
	  if (buff==NULL) buff=(char *)alloca(256);	
	  MENUITEMINFO nfo;
	  nfo.cbSize=sizeof(nfo);
	  nfo.fMask=MIIM_TYPE;
	  nfo.dwTypeData=buff;
	  nfo.cch=256;
	  GetMenuItemInfo(mnu,i,TRUE,&nfo);
	  char *c=strchr(buff,'\t');
	  if (c) *c=0;else c=strchr(buff,0);
	  for (j=0;j<acccnt;j++) if (lpaccel[j].cmd==command) break;
	  if (j<acccnt)
		{
		if (lpaccel[j].fVirt & FVIRTKEY)
		  {
		  strcpy(c,"\t");
		  if (lpaccel[j].fVirt & FCONTROL) strcat(c,"Ctrl+");
		  if (lpaccel[j].fVirt & FALT) strcat(c,"Alt+");
		  if (lpaccel[j].fVirt & FSHIFT) strcat(c,"Shift+");
		  if (lpaccel[j].key<=VK_HELP) strcat(c,vk_table1[lpaccel[j].key]);
		  else if (lpaccel[j].key>=VK_NUMPAD0 && lpaccel[j].key<=0x91) strcat(c,vk_table2[lpaccel[j].key-VK_NUMPAD0]);
		  else if (lpaccel[j].key>='0' && lpaccel[j].key<='Z')
			{
			char z[2];z[0]=lpaccel[j].key;z[1]=0;
			strcat(c,z);
			}
		  }
		}
	  SetMenuItemInfo(mnu,i,TRUE,&nfo);
	  }
	}
  }

void CHotKeyCfgDlg::RecursiveReplaceMenuHotkeys(HACCEL haccel, HMENU start)
  {
  int accls=CopyAcceleratorTable(haccel,NULL,0);
  ACCEL *accel=(ACCEL *)alloca(sizeof(ACCEL)*(accls+1));
  CopyAcceleratorTable(haccel,accel,accls);
  ReplaceMenuHotkeys(accel,accls,start);
  }

BOOL CHotKeyCfgDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	_wnd=(CFrameWnd *)theApp.m_pMainWnd;

	int i;

	for (i=0;i<=VK_HELP;i++)
	  {
	  if (vk_table1[i][0]!='~')
		{
		int p=wHotkey.AddString(vk_table1[i]);
		wHotkey.SetItemData(p,i);
		}
	  }

	for (i='0';i<='9';i++)
	  {
	  char buff[2];buff[0]=i;buff[1]=0;
	  int p=wHotkey.AddString(buff);
	  wHotkey.SetItemData(p,i);
	  }

	for (i='A';i<='Z';i++)
	  {
	  char buff[2];buff[0]=i;buff[1]=0;
	  int p=wHotkey.AddString(buff);
	  wHotkey.SetItemData(p,i);
	  }

	for (i=VK_NUMPAD0;i<=0x91;i++)
	  {
	  int pos=i-VK_NUMPAD0;
	  if (vk_table2[pos][0]!='~')
		{
		int p=wHotkey.AddString(vk_table2[pos]);
		wHotkey.SetItemData(p,i);
		}
	  }
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
