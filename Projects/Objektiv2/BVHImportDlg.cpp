// BVHImportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "BVHImportDlg.h"

#include "BVHImport.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CBVHImportDlg dialog


CBVHImportDlg::CBVHImportDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CBVHImportDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CBVHImportDlg)
    scale = 0.1f;
    ztop = FALSE;
    invertX = FALSE;
    invertY = FALSE;
    invertZ = FALSE;
    //}}AFX_DATA_INIT
    }










void CBVHImportDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CBVHImportDlg)
  DDX_Control(pDX, IDC_EXTERMS, extrms);
  DDX_Control(pDX, IDC_LIST1, list);
  DDX_Text(pDX, IDC_SCALE, scale);
  DDX_Check(pDX, IDC_ZTOP, ztop);
  DDX_Check(pDX, IDC_INVERTX, invertX);
  DDX_Check(pDX, IDC_INVERTY, invertY);
  DDX_Check(pDX, IDC_INVERTZ, invertZ);
  //}}AFX_DATA_MAP
  }










BEGIN_MESSAGE_MAP(CBVHImportDlg, CDialog)
  //{{AFX_MSG_MAP(CBVHImportDlg)
  ON_WM_SIZE()
    ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST1, OnEndlabeleditList1)
      ON_BN_CLICKED(IDC_EXTERMS, OnExterms)
        //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CBVHImportDlg message handlers
    
    BOOL CBVHImportDlg::OnInitDialog() 
      {
      CDialog::OnInitDialog();
      
      CRect rc;
      GetClientRect(&rc);
      sz=rc.Size();
      
      list.InsertColumn(0,WString(IDS_BONELENGTH),LVCFMT_RIGHT,80,0);
      list.InsertColumn(1,WString(IDS_BONENAME),LVCFMT_LEFT,200,1);
      ListView_SetExtendedListViewStyleEx(list,LVS_EX_FULLROWSELECT |LVS_EX_GRIDLINES |LVS_EX_CHECKBOXES,LVS_EX_FULLROWSELECT |LVS_EX_GRIDLINES |LVS_EX_CHECKBOXES );
      int i;
      for (i=0;i<itemcount;i++)
        {
        SBVHHierarchyItem &itm=itemlist[i];	
        list.InsertItem(i,"100",0);
        list.SetItemText(i,1,itm.name);
        ListView_SetCheckState(list,i,1);
        }
      return TRUE;  // return TRUE unless you set the focus to a control
      // EXCEPTION: OCX Property Pages should return FALSE
      }










void CBVHImportDlg::OnSize(UINT nType, int cx, int cy) 
  {
  if (list.GetSafeHwnd())
    {
    if (cx<200) cx=200;
    if (cy<100) cy=100;
    int rx=cx-sz.cx;
    int ry=cy-sz.cy;
    HDWP hdwp=BeginDeferWindowPos(10);
    MoveWindowRel(list,0,0,rx,ry,hdwp);
    MoveWindowRel(*GetDlgItem(IDOK),rx,0,0,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDCANCEL),rx,0,0,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDC_SCALE),rx,0,0,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDC_SCALE_STATIC),rx,0,0,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDC_ZTOP),rx,0,0,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDC_EXTERMS),rx,ry,0,0,hdwp);
    EndDeferWindowPos(hdwp);
    sz.cx=cx;
    sz.cy=cy;
    }
  CDialog::OnSize(nType, cx, cy);
  }










void CBVHImportDlg::OnOK() 
  {
  int i;
  if (!UpdateData()) return;
  for (i=0;i<itemcount;i++)
    {
    SBVHHierarchyItem &itm=itemlist[i];
    itm.ustate=ListView_GetCheckState(list,i)!=0;
    CString s=list.GetItemText(i,0);
    float scl;
    sscanf(s,"%f",&scl);
    scl/=100.0f;
    itm.scale*=scale;
    for (int j=0;j<itemcount;j++) if (itemlist[j].parent==i) itemlist[j].scale=scl;	
    }  
  CDialog::OnOK();  
  }










void CBVHImportDlg::OnEndlabeleditList1(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
  
  float val;	
  if (pDispInfo->item.pszText==NULL || sscanf(pDispInfo->item.pszText,"%f",&val)!=1)
    *pResult=0;
  else
    {
    *pResult =1;
    }	  
  }










void CBVHImportDlg::OnExterms() 
  {
  for (int i=0;i<itemcount;i++)
    {
    SBVHHierarchyItem &itm=itemlist[i];
    if (itm.term) ListView_SetCheckState(list,i,0);
    }  
  }










