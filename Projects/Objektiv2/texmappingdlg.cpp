// TexMappingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "Objektiv2Doc.h"
#include "MainFrm.h"
#include "TexMappingDlg.h"
#include "Objektiv2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTexMappingDlg dialog


CTexMappingDlg::CTexMappingDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CTexMappingDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CTexMappingDlg)
    MirrorTu = FALSE;
    MirrorTv = FALSE;
    OneSing = FALSE;
    SwapTuTv = FALSE;
    azimut = 0.0f;
    zenit = 0.0f;
    TexName = _T("");
    //}}AFX_DATA_INIT
    tuscale=1.0f;
    tvscale=1.0f;
    tuoffset=0.0f;
    tvoffset=0.0f;
    previewobject=NULL;
    zaloha=NULL;
    }

//--------------------------------------------------

void CTexMappingDlg::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_AZIMUT,IDC_ZENIT,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CTexMappingDlg)
  DDX_Control(pDX, IDC_TV_SCALE_VAL, TVScaleVal);
  DDX_Control(pDX, IDC_TV_OFFSET_VAL, TVOffsetVal);
  DDX_Control(pDX, IDC_TU_SCALE_VAL, TUScaleVal);
  DDX_Control(pDX, IDC_TU_OFFSET_VAL, TUOffsetVal);
  DDX_Control(pDX, IDC_TV_SCALE, TVScale);
  DDX_Control(pDX, IDC_TV_OFFSET, TVOffset);
  DDX_Control(pDX, IDC_TU_SCALE, TUScale);
  DDX_Control(pDX, IDC_TU_OFFSET, TUOffset);
  DDX_Check(pDX, IDC_MIRRORTU, MirrorTu);
  DDX_Check(pDX, IDC_MIRRORTV, MirrorTv);
  DDX_Check(pDX, IDC_SING, OneSing);
  DDX_Check(pDX, IDC_SWAPTUTV, SwapTuTv);
  DDX_Text(pDX, IDC_AZIMUT, azimut);
  DDV_MinMaxFloat(pDX, azimut, 0.f, 360.f);
  DDX_Text(pDX, IDC_ZENIT, zenit);
  DDV_MinMaxFloat(pDX, zenit, -180.f, 180.f);
  DDX_Text(pDX, IDC_TEXNAME, TexName);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CTexMappingDlg, CDialog)
  //{{AFX_MSG_MAP(CTexMappingDlg)
  ON_WM_HSCROLL()
    ON_BN_CLICKED(IDC_PREVIEW, OnPreview)
      ON_WM_DESTROY()
        ON_BN_CLICKED(IDC_BROWSE, OnSetTexture)
          ON_BN_CLICKED(IDC_SETFROMVIEW, OnSetTexture)
            ON_BN_CLICKED(IDC_SETFROMLIST, OnSetTexture)
              //}}AFX_MSG_MAP
              END_MESSAGE_MAP()
                
                /////////////////////////////////////////////////////////////////////////////
                // CTexMappingDlg message handlers
                
                void CTexMappingDlg::SetSliderFloatValue(CSliderCtrl &slider, float value, bool logg)
                  {
                  if (logg)
                    {
                    value=sqrt(value/4.0f);
                    }
                  slider.SetRangeMin(0);
                  slider.SetRangeMax(1000);
                  slider.SetLineSize(10);
                  slider.SetPageSize(100);
                  slider.SetPos((int)(value*1000));
                  }

//--------------------------------------------------

float CTexMappingDlg::GetSliderFloatValue(CSliderCtrl &slider,bool logval)
  {
  int i=slider.GetPos();
  float res=(float)i/1000.0f;
  if (logval)
    {
    res=res*res*4.0f;
    }
  return res;
  }

//--------------------------------------------------

void CTexMappingDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
  {
  CSliderCtrl *bar=(CSliderCtrl *)pScrollBar;
  int idc=bar->GetDlgCtrlID();
  CEdit *disp;
  bool logg;
  switch (idc)
    {
    case IDC_TU_OFFSET: disp=&TUOffsetVal;logg=false;break;
    case IDC_TV_OFFSET: disp=&TVOffsetVal;logg=false;break;
    case IDC_TU_SCALE: disp=&TUScaleVal;logg=true;break;
    case IDC_TV_SCALE: disp=&TVScaleVal;logg=true;break;
    }
  float p=GetSliderFloatValue(*bar,logg);
  SetFloatValueToStatic(*disp,p);
  }

//--------------------------------------------------

void CTexMappingDlg::SetFloatValueToStatic(CEdit &st, float value)
  {
  char s[200];
  sprintf(s,"%.3f",value);
  st.SetWindowText(s);
  }

//--------------------------------------------------

BOOL CTexMappingDlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  SetSliderFloatValue(TUOffset,tuoffset,false);
  SetSliderFloatValue(TVOffset,tvoffset,false);
  SetSliderFloatValue(TUScale,tuscale,true);
  SetSliderFloatValue(TVScale,tvscale,true);
  SetFloatValueToStatic(TUOffsetVal,tuoffset);
  SetFloatValueToStatic(TVOffsetVal,tvoffset);
  SetFloatValueToStatic(TUScaleVal,tuscale);
  SetFloatValueToStatic(TVScaleVal,tvscale);
  GetDlgItem(IDC_PREVIEW)->EnableWindow(previewobject!=NULL);
  GetDlgItem(IDC_AZIMUT)->EnableWindow(spehere==true);
  GetDlgItem(IDC_ZENIT)->EnableWindow(spehere==true);
  GetDlgItem(IDC_SING)->EnableWindow(spehere==true);
  ((CButton *)GetDlgItem(IDC_BROWSE))->SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_BROWSE)));
  ((CButton *)GetDlgItem(IDC_SETFROMVIEW))->SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_FROMBGR)));
  ((CButton *)GetDlgItem(IDC_SETFROMLIST))->SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_FROMLIST)));
  CWnd *wnd=GetOwner();
  wnd->EnableWindow(true);
  if (previewobject) zaloha=new ObjectData(*previewobject);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

#define sgndir(a,b) (((b)>(a))-((b)<(a)))

void CTexMappingDlg::CalcSpehereMapping(HVECTOR pos, HVECTOR pin, float &tu, float &tv, float &lastaz)
  {
  HVECTOR prac;
  CopyVektor(prac,pos);
  RozdilVektoru(prac,pin);
  float az=(float)atan2(prac[ZVAL],prac[XVAL]);
  float zen=(float)atan2(prac[YVAL],sqrt(prac[XVAL]*prac[XVAL]+prac[ZVAL]*prac[ZVAL]));
  if (fabs(az-lastaz)>FPI) 
    az+=2*FPI*sgndir(az,lastaz);
  lastaz=az;
  float setaz=torad(azimut);
  float setze=torad(zenit);
  az-=setaz;
  zen-=setze;
  if (OneSing)
    {
    float d=(1.0f-((zen/FPI)+0.5f))*0.5f;
    tu=0.5f+(float)cos(az)*d;
    tv=0.5f+(float)sin(az)*d;
    }
  else
    {
    tu=az/(2*FPI)+0.5f;
    tv=zen/FPI;
    }
  RemapTuTv(tu,tv);
  }

//--------------------------------------------------

void CTexMappingDlg::RemapTuTv(float &tu, float &tv)
  {
  if (SwapTuTv)
    {
    float x=tu;tu=tv;tv=x;
    }
  tu*=tuscale;
  tv*=tvscale;
  tu+=tuoffset;
  tv+=tvoffset;
  if (MirrorTu) tu=1.0f-tu;
  if (MirrorTv) tv=1.0f-tv;
  }

//--------------------------------------------------

void CTexMappingDlg::CalcSpehereMappingToObject(ObjectData *odata)
  {
  int cnt=odata->NFaces();
  for (int i=0;i<cnt;i++) 
    if (odata->FaceSelected(i))
      {
      float lastaz=0.0f;
      FaceT fc(odata,i);
      fc.SetTexture(TexName);
      for (int j=0;j<fc.N();j++)
        {
        PosT &ps=odata->Point(fc.GetPoint(j));
        float u=fc.GetU(j),v=fc.GetV(j);
        CalcSpehereMapping(mxVector3(ps[0],ps[1],ps[2]),pos,u,v,lastaz);
        fc.SetUV(j,u,v);
        }
      }
  }

//--------------------------------------------------

void CTexMappingDlg::OnPreview() 
  {
  //  if (TexName.GetLength()>26) 
  //			  	AfxMessageBox(IDS_TEXNAMETOOLONG,MB_OK|MB_ICONEXCLAMATION);
  if (previewobject && TexName)
    {
    UpdateFloatValues();
    if (spehere)
      CalcSpehereMappingToObject(previewobject);
    else
      CalcCylindricMappingToObject(previewobject);
    doc->UpdateAllViews(NULL,1);
    frame->SendMessage(WM_COMMAND,ID_UPDATE_MODEL,0);
    //*previewobject=zaloha;
    }
  }

//--------------------------------------------------

void CTexMappingDlg::UpdateFloatValues()
  {
  UpdateData();
  CString s; 
  TUOffsetVal.GetWindowText(s);
  sscanf(s,"%f",&tuoffset);
  TVOffsetVal.GetWindowText(s);
  sscanf(s,"%f",&tvoffset);
  TUScaleVal.GetWindowText(s);
  sscanf(s,"%f",&tuscale);
  TVScaleVal.GetWindowText(s);
  sscanf(s,"%f",&tvscale);
  SetSliderFloatValue(TUOffset,tuoffset,false);
  SetSliderFloatValue(TVOffset,tvoffset,false);
  SetSliderFloatValue(TUScale,tuscale,true);
  SetSliderFloatValue(TVScale,tvscale,true);
  }

//--------------------------------------------------

void CTexMappingDlg::CalcCylindricMapping(HMATRIX mat, HVECTOR vect,float &tu, float &tv, float &lastrad)
  {
  HVECTOR tvect;
  TransformVector(mat,vect,tvect);
  float az=(float)atan2(tvect[ZVAL],tvect[XVAL]);
  if (fabs(az-lastrad)>FPI) 
    az+=2*FPI*sgndir(az,lastrad);
  lastrad=az;
  tu=az/(2*FPI);
  tv=tvect[YVAL];
  RemapTuTv(tu,tv);
  }

//--------------------------------------------------

void CTexMappingDlg::CalcCylindricMappingToObject(ObjectData *odata)
  {
  HMATRIX mat,mat2;
  CopyVektor(mat[3],pos);
  HVECTOR top,left;
  CopyVektor(top,mxVector3(1,0,0));
  VektorSoucin(dir,top,left);
  if (ModulVektoru2(left)<0.001f) 
    {
    CopyVektor(top,mxVector3(0,1,0));
    VektorSoucin(dir,top,left);
    }
  NormalizeVector(left);
  top[WVAL]=0;left[WVAL]=0;dir[WVAL]=0;
  CopyVektor(mat[0],left);
  CopyVektor(mat[1],dir);
  CopyVektor(mat[2],top);
  InverzeMatice(mat,mat2);
  int cnt=odata->NFaces();
  for (int i=0;i<cnt;i++) 
    if (odata->FaceSelected(i))
      {
      float lastaz=0.0f;
      FaceT fc(odata,i);
      fc.SetTexture(TexName);
      for (int j=0;j<fc.N();j++)
        {
        PosT &ps=odata->Point(fc.GetPoint(j));
        float u=fc.GetU(j),v=fc.GetV(j);
        CalcCylindricMapping(mat2,mxVector3(ps[0],ps[1],ps[2]),u,v,lastaz);
        fc.SetUV(j,u,v);
        }
      }
  }

//--------------------------------------------------

//static LRESULT DummyDialogProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
//  {
//  return 0;
//  }

//DEL void CTexMappingDlg::Create()
//DEL   {  
//DEL   HWND wnd=CreateDialog(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDD),*frame,(DLGPROC)DummyDialogProc);
//DEL   if (wnd==NULL) return;
//DEL   SubclassWindow(wnd);
//DEL   OnInitDialog();
//DEL   }

void CTexMappingDlg::OnOK() 
  {
  UpdateData();
  /*     if (TexName.GetLength()>26) 
			  	AfxMessageBox(IDS_TEXNAMETOOLONG,MB_OK|MB_ICONEXCLAMATION);*/
  CDialog::OnOK();
  }

//--------------------------------------------------

//DEL void CTexMappingDlg::OnCancel() 
//DEL   {
//DEL 
//DEL   }

CTexMappingDlg::CTexMappingDlg(const CTexMappingDlg &other)
  {
  azimut=other.azimut;
  zenit=other.zenit;
  tuscale=other.tuscale;
  tuoffset=other.tuoffset;
  tvscale=other.tvscale;
  tvoffset=other.tvoffset;
  spehere=other.spehere;
  CopyVektor(pos,other.pos);
  CopyVektor(dir,other.dir);
  MirrorTu=other.MirrorTu;
  MirrorTv=other.MirrorTv;
  OneSing=other.OneSing;
  SwapTuTv=other.SwapTuTv;
  TexName=other.TexName;
  }

//--------------------------------------------------

void CTexMappingDlg::OnDestroy() 
  {
  CDialog::OnDestroy();
  if (previewobject) 
    {
    *previewobject=*zaloha;
    delete zaloha;
    }
  }

//--------------------------------------------------

void CTexMappingDlg::OnSetTexture() 
  {
  UpdateData();
  int id=GetFocus()->GetDlgCtrlID();
  TexName=GetTextureFrom(id,TexName);
  UpdateData(FALSE);
  }

//--------------------------------------------------

