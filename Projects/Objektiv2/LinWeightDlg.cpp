// LinWeightDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "LinWeightDlg.h"
#include "ComInt.h"
#include "Objektiv2Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLinWeightDlg dialog

CLinWeightDlg::CLinWeightDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CLinWeightDlg::IDD, pParent),saved(&(ObjectData()))
    {
    //{{AFX_DATA_INIT(CLinWeightDlg)
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CLinWeightDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CLinWeightDlg)
  DDX_Control(pDX, IDC_DRAWFRAME, wDrawFrame);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CLinWeightDlg, CDialog)
  //{{AFX_MSG_MAP(CLinWeightDlg)
  ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONUP()
      ON_WM_MOUSEMOVE()
        ON_BN_CLICKED(IDC_BOTTOM, OnBottom)
          ON_BN_CLICKED(IDC_LEFT, OnLeft)
            ON_BN_CLICKED(IDC_RIGHT, OnRight)
              ON_BN_CLICKED(IDC_TOP, OnTop)
                //}}AFX_MSG_MAP
                END_MESSAGE_MAP()
                  
                  /////////////////////////////////////////////////////////////////////////////
                  // CLinWeightDlg message handlers
                  
                  void CLinWeightDlg::OnLButtonDown(UINT nFlags, CPoint point) 
                    {
                    // TODO: Add your message handler code here and/or call default
                    if (ChildWindowFromPoint(point)==&wDrawFrame)
                      {
                      SetCapture();
                      last1=last2=point;
                      }
                    
                    CDialog::OnLButtonDown(nFlags, point);
                    }

//--------------------------------------------------

void CLinWeightDlg::OnLButtonUp(UINT nFlags, CPoint point) 
  {
  // TODO: Add your message handler code here and/or call default
  if (GetCapture()==this) 
    {
    ReleaseCapture();
    wDrawFrame.Invalidate();
    if (last1!=last2)
      {
      CheckDlgButton(IDC_LEFT,0);
      CheckDlgButton(IDC_TOP,0);
      CheckDlgButton(IDC_BOTTOM,0);
      CheckDlgButton(IDC_RIGHT,0);
      xa=last2.x-last1.x;
      ya=last2.y-last1.y;
      float m=1.0f/(float)sqrt((xa*xa)+(ya*ya));
      xa*=m;
      ya*=m;
      MakeSel();
      }
    }
  CDialog::OnLButtonUp(nFlags, point);
  }

//--------------------------------------------------

void CLinWeightDlg::OnMouseMove(UINT nFlags, CPoint point) 
  {
  // TODO: Add your message handler code here and/or call default
  if (GetCapture()==this && ChildWindowFromPoint(point)==&wDrawFrame)
    {
    CDC &dc=*GetDC();
    dc.SelectStockObject(WHITE_PEN);
    dc.SetROP2(R2_XORPEN);
    dc.MoveTo(last1);
    dc.LineTo(last2);
    dc.MoveTo(last1);
    dc.LineTo(point);
    last2=point;
    ReleaseDC(&dc);
    }
  CDialog::OnMouseMove(nFlags, point);
  }

//--------------------------------------------------

BOOL CLinWeightDlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  int cnt=curobj->NPoints();
  int i;
  bool first=true;
  saved=*curobj->GetSelection();
  for (i=0;i<cnt;i++)
    if (curobj->PointSelected(i))
      {
      PosT &ps=curobj->Point(i);
      HVECTOR tv;
      TransformVector(scrtrans,mxVector3(ps[0],ps[1],ps[2]),tv);
      CorrectVector(tv);
      if (tv[ZVAL]<0.0 || tv[WVAL]<0.0 || fabs(tv[XVAL])>10000.0 || fabs(tv[YVAL])>10000.0) continue;
      if (first)
        {
        left=right=tv[0];
        top=bottom=tv[1];
        first=false;
        }
      else
        {
        if (tv[0]<left) left=tv[0];
        if (tv[0]>right) right=tv[0];
        if (tv[1]<top) top=tv[1];
        if (tv[1]>bottom) bottom=tv[1];
        }		  
      }
  CRect rc((int)left,(int)top,(int)right,(int)bottom);
  extent=rc;
  CDC *dc=drwwnd->GetDC();
  dc->SelectStockObject(BLACK_PEN);
  dc->SelectStockObject(HOLLOW_BRUSH);
  dc->Rectangle(&rc);
  drwwnd->ReleaseDC(dc);
  xa=0;
  ya=1;
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CLinWeightDlg::OnBottom() 
  {
  xa=0;
  ya=-1;
  MakeSel();
  }

//--------------------------------------------------

void CLinWeightDlg::OnLeft() 
  {
  xa=1;
  ya=0;
  MakeSel();
  }

//--------------------------------------------------

void CLinWeightDlg::OnRight() 
  {
  xa=-1;
  ya=0;
  MakeSel();
  }

//--------------------------------------------------

void CLinWeightDlg::OnTop() 
  {
  xa=0;
  ya=1;
  MakeSel();
  }

//--------------------------------------------------

void CLinWeightDlg::MakeSel()
  {
  float mind,maxd;
  int cnt=curobj->NPoints();
  int i;
  bool first=true;
  for (i=0;i<cnt;i++)
    if (saved.PointSelected(i))
      {
      PosT &ps=curobj->Point(i);
      HVECTOR tv;
      TransformVector(scrtrans,mxVector3(ps[0],ps[1],ps[2]),tv);
      CorrectVector(tv);
      if (tv[ZVAL]<0.0 || tv[WVAL]<0.0 || fabs(tv[XVAL])>10000.0 || fabs(tv[YVAL])>10000.0) continue;
      float d=-(xa*tv[0]+ya*tv[1])/(xa*xa+ya*ya);
      if (first) 
        {mind=maxd=d;first=false;}
      else 
        {
        if (d<mind) mind=d;
        if (d>maxd) maxd=d;
        }	
      }
  Selection *q=new Selection(curobj);
  for (i=0;i<cnt;i++)
    if (saved.PointSelected(i))
      {
      PosT &ps=curobj->Point(i);
      HVECTOR tv;
      TransformVector(scrtrans,mxVector3(ps[0],ps[1],ps[2]),tv);
      CorrectVector(tv);
      if (tv[ZVAL]<0.0 || tv[WVAL]<0.0 || fabs(tv[XVAL])>10000.0 || fabs(tv[YVAL])>10000.0) continue;
      float d=-(xa*tv[0]+ya*tv[1])/(xa*xa+ya*ya);
      float tq=(d-mind)/(maxd-mind);
      q->SetPointWeight(i,tq);
      }
  cnt=curobj->NFaces();
  for (i=0;i<cnt;i++) q->FaceSelect(i,saved.FaceSelected(i));
  comint.Begin(WString(IDS_MAKESELECTION));
  comint.Run(new CISelect(q));
  doc->UpdateAllViews(NULL,1);
  CDC *dc=drwwnd->GetDC();
  dc->SelectStockObject(BLACK_PEN);
  dc->SelectStockObject(HOLLOW_BRUSH);
  dc->Rectangle(&extent);
  drwwnd->ReleaseDC(dc);
  }

//--------------------------------------------------

