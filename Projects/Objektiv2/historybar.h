#if !defined(AFX_HISTORYBAR_H__75C436E3_7C80_40C6_BC16_8D10E334CC7E__INCLUDED_)
#define AFX_HISTORYBAR_H__75C436E3_7C80_40C6_BC16_8D10E334CC7E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HistoryBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHistoryBar dialog

class CHistoryBar : public CDialogBar
  {
  // Construction
  CListBox lsb;
  CWnd *notify;
  public:
    CHistoryBar();   // standard constructor
    CDIALOGBAR_RESIZEABLE
      void Create(CWnd *parent,int menuid);
    
    // Dialog Data
    //{{AFX_DATA(CHistoryBar)
    enum 
      { IDD = IDD_HISTORY };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CHistoryBar)
  protected:
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CHistoryBar)
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnDestroy();
    afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg void OnSelchangeList();
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HISTORYBAR_H__75C436E3_7C80_40C6_BC16_8D10E334CC7E__INCLUDED_)
