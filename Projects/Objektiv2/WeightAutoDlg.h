#if !defined(AFX_WEIGHTAUTODLG_H__65FE31A0_5E86_4DFD_8BF9_3EC40BD8A741__INCLUDED_)
#define AFX_WEIGHTAUTODLG_H__65FE31A0_5E86_4DFD_8BF9_3EC40BD8A741__INCLUDED_

#include "ComInt.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WeightAutoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWeightAutoDlg dialog

class CWeightAutoDlg : public CDialog
  {
  // Construction
  public:
    CWeightAutoDlg(CWnd* pParent = NULL);   // standard constructor
    
    int level;
    
    // Dialog Data
    //{{AFX_DATA(CWeightAutoDlg)
    enum 
      { IDD = IDD_WEIGHTAUTOMATION };
    CComboBox	m_deftex;
    CComboBox	w_lod;
    CComboBox	w_file;
    CEdit	def;
    CString	filename;
    BOOL	findbones;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CWeightAutoDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CWeightAutoDlg)
    afx_msg void OnDropdownFile();
    virtual BOOL OnInitDialog();
    afx_msg void OnCreatefile();
    afx_msg void OnSavefile();
    afx_msg void OnLoadfile();
    virtual void OnOK();
    afx_msg void OnDropdownDeftexture();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

int WeightAuto(LODObject *lods, CString &name, int level, bool findbones);
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WEIGHTAUTODLG_H__65FE31A0_5E86_4DFD_8BF9_3EC40BD8A741__INCLUDED_)
