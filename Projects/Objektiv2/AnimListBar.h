#if !defined(AFX_ANIMLISTBAR_H__CFF98CD2_0ABE_474D_BA3E_63B2393835A2__INCLUDED_)
#define AFX_ANIMLISTBAR_H__CFF98CD2_0ABE_474D_BA3E_63B2393835A2__INCLUDED_

#include "LockAnims.h"	// Added by ClassView
#include "ComInt.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AnimListBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAnimListBar dialog
class CAnimListBar : public CDialogBar
  {
  // Construction
  CListCtrl list;
  CWnd *notify;
  ObjectData *obj;
  Pathname defpath;	
  CLockAnims lock;
  float curlod;
  protected:
    void UpdateListOfAnims(ObjectData *odata) 
      {UpdateListOfAnims(odata,curlod);}
    virtual BOOL PreTranslateMessage(MSG* pMsg);
  public:
	static CString GetStepSelection(ObjectData *obj);
    void OnDocumentReset();
    float* GetWeightList(int &size);
    void UpdatePhase();
    int TranslateIndex(int idx);
    void SetActiveIndex(int idx, bool redraw=true);
    int GetActiveIndex(int search=LVNI_FOCUSED);
    void UpdateListOfAnims(ObjectData *odata, float curlod);
    CAnimListBar();   // standard constructor
    CDIALOGBAR_RESIZEABLE
      void Create(CWnd *parent,int menuid);
    int lastindex;
    // Dialog Data
    //{{AFX_DATA(CAnimListBar)
    enum 
      { IDD = IDD_ANIMLIST };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CAnimListBar)
  protected:
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CAnimListBar)
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnDestroy();
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    afx_msg void OnAnimNew();
    afx_msg void OnAnimDelete();
    afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnKillfocusList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnAnimRename();
    afx_msg void OnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg BOOL OnAnimRedefine(UINT cmd);
    afx_msg void OnAnimCopy();
    afx_msg BOOL OnAnimPaste(UINT cmd);
    afx_msg void OnAnimPasteoversel();
    afx_msg void OnAnimImport();
    afx_msg BOOL OnAnimAutotime(UINT cmd);
    afx_msg BOOL OnAnimHalfrate(UINT cmd);
    afx_msg void OnAnimFrommatrices();
    afx_msg void OnAnimExportmatrices();
    afx_msg void OnAnimSoftselect();
    afx_msg BOOL OnAnimNeutralizestep(UINT cmd);
    afx_msg BOOL OnAnimXnormcenter(UINT cmd);
    afx_msg BOOL OnAnimZnormcenter(UINT cmd);
    afx_msg BOOL OnAnimMirror(UINT cmd);
    afx_msg BOOL OnAnimInsertstep(UINT cmd);
    afx_msg void OnAnimFrommatriceslod();
    afx_msg void OnAnimLockselected();
    afx_msg void OnAnimUnlockall();
    afx_msg void OnAnimTimerange();
    afx_msg void OnPluginPopupCommand(UINT cmd);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  public:
    afx_msg void OnAnimDeleteallanimations();
  };

//--------------------------------------------------

class CIAnimSimple:public CICommand
  {
  int idcommand;
  float value;
  long index;
  public:
    CIAnimSimple(long idc, float value=0.0f):
    idcommand(idc),value(value) 
      {recordable=false;}
    CIAnimSimple(long idc, int idx, float value=0.0f):
    idcommand(idc),index(idx),value(value) 
      {recordable=false;}
    virtual int Execute(LODObject *lod);
  };

class CIAnimLoadPhase: public CICommand
  {  
  AnimationPhase *_phs;
  public:
    CIAnimLoadPhase(AnimationPhase *phs): _phs(phs) {recordable=false;}
    ~CIAnimLoadPhase() {delete _phs;}
    virtual int Execute(LODObject *lod)
      {
      ObjectData *obj=lod->Active();
      int idx=obj->AnimationIndex(_phs->GetTime());
      if (idx!=-1) obj->DeleteAnimation(_phs->GetTime());
      obj->AddAnimation(*_phs);        
      return 0;
      }
  };

class CIAnimAutoTime: public CICommand
  {  
  public:
    CIAnimAutoTime() {recordable=false;}
    virtual int Execute(LODObject *lod);
  };

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ANIMLISTBAR_H__CFF98CD2_0ABE_474D_BA3E_63B2393835A2__INCLUDED_)
