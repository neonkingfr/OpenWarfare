#if !defined(AFX_DLGTEXELMEASURE_H__D907E421_29A7_4007_9784_46515E2763B7__INCLUDED_)
#define AFX_DLGTEXELMEASURE_H__D907E421_29A7_4007_9784_46515E2763B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgTexelMeasure.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgTexelMeasure dialog

#define HISTOGRAMPOSIT 64

class CDlgTexelMeasure : public CDialog
  {
  // Construction
  struct texelinfo
    {
    float realmin;
    float realmax;
    float realtotal;
    float texelmin;
    float texelmax;
    float texeltotal;
    float avgmin;
    float avgmax;
    float avgtotal;
    int count;
    const char *texturename;
    bool *markers;
    bool selonly;
    float mintexelsz;
    float maxtexelsz;
    int *histogram;
    public:
      void AddToHistogram(float pos);
    };
  
  bool selonly;		  
  float *avgs;		  //pole facu, kazdy ma pridelenou velikost
  //pouziva se po vypoctu pro vyber selekce
  
  static int sc_units;  //ulozena hodnota units
  
  
  texelinfo lastresult; //posledni vysledek vypoctu
  
  int histogram[HISTOGRAMPOSIT];
  
  public:
    float GetUnits();
    void CalcDlg(bool firstphase=false);
    CDlgTexelMeasure(CWnd* pParent = NULL);   // standard constructor
    
    ObjectData *source;
    Selection *selout;
    
    // Dialog Data
    //{{AFX_DATA(CDlgTexelMeasure)
    enum { IDD = IDD_TEXELMEASURE };
    CListCtrl	wTexelNfo;
    CButton	wHistogram;
    float	m_rangeMin;
    float	m_rangeMax;
    int		m_units;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CDlgTexelMeasure)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CDlgTexelMeasure)
    virtual BOOL OnInitDialog();
    afx_msg void OnSelonly();
    virtual void OnOK();
    virtual void OnCancel();
    afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg void OnRecalculaterange();
    afx_msg void OnUnit();
    afx_msg void OnResetrange();
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnSelectrange();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    private:
    void FormatTexelInfo(texelinfo &nfo);
    void CalculateTexelInfo(ObjectData *obj, texelinfo &nfo);
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGTEXELMEASURE_H__D907E421_29A7_4007_9784_46515E2763B7__INCLUDED_)
