// TimeRangeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "TimeRangeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTimeRangeDlg dialog


CTimeRangeDlg::CTimeRangeDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CTimeRangeDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CTimeRangeDlg)
    timerange = 0.0f;
    loop = TRUE;
    interpolate = TRUE;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CTimeRangeDlg::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_TIMERANGE,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CTimeRangeDlg)
  DDX_Text(pDX, IDC_TIMERANGE, timerange);
  DDX_Check(pDX, IDC_LOOP, loop);
  DDX_Check(pDX, IDC_INTERPOLATE, interpolate);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CTimeRangeDlg, CDialog)
  //{{AFX_MSG_MAP(CTimeRangeDlg)
  // NOTE: the ClassWizard will add message map macros here
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CTimeRangeDlg message handlers
    