// AllRenameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "objektiv2doc.h"
#include "AllRenameDlg.h"
#include <malloc.h>
#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAllRenameDlg dialog


CAllRenameDlg::CAllRenameDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAllRenameDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAllRenameDlg)
	//}}AFX_DATA_INIT
_minsize.cx=0;
_minsize.cy=0;
}


void CAllRenameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAllRenameDlg)
	DDX_Control(pDX, IDC_TEXTURELIST, wTextureList);
	DDX_Control(pDX, IDC_OLDNAMEMASK, wOldName);
	DDX_Control(pDX, IDC_NEWNAMEMASK, wNewName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAllRenameDlg, CDialog)
	//{{AFX_MSG_MAP(CAllRenameDlg)
	ON_WM_MEASUREITEM()
	ON_WM_DRAWITEM()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_LBN_SELCHANGE(IDC_TEXTURELIST, OnSelchangeTexturelist)
	ON_BN_CLICKED(IDC_UNDO, OnUndo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAllRenameDlg message handlers



void CAllRenameDlg::ScanTexturesAndMaterials(ObjectData *obj)
  {
  for (int i=0;i<obj->NFaces();i++)
    {    
    if (_signend) return;
    FaceT fc(obj,i);
    int id;
    if (fc.GetTexture().GetLength()!=0)
      {
      id=wTextureList.FindStringExact(-1,fc.GetTexture());
      if (id==LB_ERR)
        wTextureList.AddString(fc.GetTexture());
      }
    if (fc.GetMaterial().GetLength()!=0)
      {
      id=wTextureList.FindStringExact(-1,fc.GetMaterial());
      if (id==LB_ERR)
       wTextureList.AddString(fc.GetMaterial());
      }
    
    }
  }

void CAllRenameDlg::ScanWholeLod()
  {
  wTextureList.ResetContent();
  for (int i=0;i<iCurLod->NLevels();i++)
    {
    ScanTexturesAndMaterials(iCurLod->Level(i));
    }
  }



void CAllRenameDlg::StopBuildList()
  {
  SetCursor(::LoadCursor(NULL,IDC_WAIT));
  _signend=true;
  if (_build)
    {
    while (MsgWaitForMultipleObjects(1,&_build->m_hThread,FALSE,INFINITE,QS_SENDMESSAGE)==WAIT_OBJECT_0+1)
      {
      MSG msg;
      while (PeekMessage(&msg,NULL,0,0,PM_REMOVE | (QS_SENDMESSAGE<<16)))      
        DispatchMessage(&msg);
      }
    delete _build;
    }
  _build=NULL;
  }

void CAllRenameDlg::OnCancel() 
  {
  StopBuildList();	
  CDialog::OnCancel();
  }

static UINT _cdecl _StartBuildList_Thread(LPVOID object) 
  {((CAllRenameDlg *)object)->ScanWholeLod();return 0;}

void CAllRenameDlg::StartBuildList()
  {
  StopBuildList();
  _build=AfxBeginThread(_StartBuildList_Thread,(void *)this,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED);
  _build->m_bAutoDelete=false;
  _signend=false;
  _build->ResumeThread();
  }

BOOL CAllRenameDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

    CRect rc;
    CRect glob;
    GetWindowRect(&glob);
    wTextureList.GetWindowRect(&rc);
    rc-=CPoint(glob.left,glob.top);
    _minsize.cy=glob.bottom-rc.bottom;   
    GetDlgItem(IDOK)->GetWindowRect(&rc);
    rc-=CPoint(glob.left,glob.top);
    _minsize.cx=glob.right-rc.left;   
    GetClientRect(&rc);
    _lastsize=rc.Size();
	
    _build=NULL;
    _signend=false;
    StartBuildList();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAllRenameDlg::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
  {
  CDialog::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
  if (nIDCtl==IDC_TEXTURELIST) 
    {
    CDC *dc=GetDC();
    CSize sz=dc->GetTextExtent("W",1);
    lpMeasureItemStruct->itemHeight=sz.cy;
    ReleaseDC(dc);
    }
  }

void CAllRenameDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
  {
  if (nIDCtl==IDC_TEXTURELIST)
    {
    if (lpDrawItemStruct->itemID==-1) return;
    CDC dc;
    dc.Attach(lpDrawItemStruct->hDC);
    DWORD bg;
    int txtlen=wTextureList.GetTextLen(lpDrawItemStruct->itemID);
    char *p=(char *)alloca(txtlen+1);
    wTextureList.GetText(lpDrawItemStruct->itemID,p);
    if (lpDrawItemStruct->itemState & ODS_SELECTED)
      {
      bg=GetSysColor(COLOR_HIGHLIGHT);
      dc.SetTextColor(GetSysColor(COLOR_HIGHLIGHTTEXT));
      }
    else
      {
	  DWORD state=lpDrawItemStruct->itemData;
	  if (state==0)
		{
    if ((_access(CString(config->GetString(IDS_CFGPATHFORTEX))+p,0))||(p[strlen(p)-1]==' ')||(strchr(p,'/'))) 
		  state=1;
		else
		  state=2;
		wTextureList.SetItemData(lpDrawItemStruct->itemID,state);
		}
	  if (state==1)
		dc.SetTextColor(RGB(255,0,0));
	  else
        dc.SetTextColor(GetSysColor(COLOR_WINDOWTEXT));
      bg=GetSysColor(COLOR_WINDOW);
      }
    dc.SetBkMode(TRANSPARENT);
    CPen line(PS_SOLID,1,GetSysColor(COLOR_GRAYTEXT));
    dc.FillSolidRect(&lpDrawItemStruct->rcItem,bg);
    CRect rc1=lpDrawItemStruct->rcItem;
    CRect rc2=rc1;
    int xpos=45;
    rc1.right=rc1.right-xpos-2;
    rc2.left=rc2.right-xpos+2;
    rc1.left+=10;
    char *c=strrchr(p,'.');
    int prelen=c==NULL?txtlen:c-p;;
    dc.DrawTextA(p,prelen,&rc1,DT_SINGLELINE|DT_VCENTER|DT_LEFT|DT_PATH_ELLIPSIS|DT_NOPREFIX);
    if (prelen!=txtlen) dc.DrawTextA(c+1,txtlen-prelen-1,&rc2,DT_SINGLELINE|DT_VCENTER|DT_LEFT|DT_PATH_ELLIPSIS|DT_NOPREFIX);
    CPen *oldline=dc.SelectObject(&line);
    dc.MoveTo(rc2.right-xpos,rc1.top);
    dc.LineTo(rc2.right-xpos,rc1.bottom);
    dc.SelectObject(oldline);
    dc.Detach();
    }
  else
    CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
  }

void CAllRenameDlg::OnSize(UINT nType, int cx, int cy) 
  {
  if (wTextureList.GetSafeHwnd()!=NULL)
    {
    int rx=cx-_lastsize.cx;
    int ry=cy-_lastsize.cy;
    HDWP hdwp=BeginDeferWindowPos(10);
    MoveWindowRel(wTextureList,0,0,rx,ry,hdwp);
    MoveWindowRel(*GetDlgItem(IDC_OLDNAMEMASK_DESC),0,ry,rx,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDC_NEWNAMEMASK_DESC),0,ry,rx,0,hdwp);
    MoveWindowRel(wNewName,0,ry,rx,0,hdwp);
    MoveWindowRel(wOldName,0,ry,rx,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDOK),rx,ry,0,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDCANCEL),rx,ry,0,0,hdwp);
    MoveWindowRel(*GetDlgItem(IDC_UNDO),0,ry,0,0,hdwp);
    EndDeferWindowPos(hdwp);
    wTextureList.Invalidate(FALSE);
    _lastsize.cx=cx;
    _lastsize.cy=cy;
    }
  CDialog::OnSize(nType, cx, cy);
  }

void CAllRenameDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
  {  	
  CDialog::OnGetMinMaxInfo(lpMMI);
  lpMMI->ptMinTrackSize.x=_minsize.cx;
  lpMMI->ptMinTrackSize.y=_minsize.cy;
  }

void CAllRenameDlg::OnSelchangeTexturelist() 
  {
  int sel=wTextureList.GetCurSel();
  if (sel!=LB_ERR)
    {
    int txtlen=wTextureList.GetTextLen(sel);
    char *p=(char *)alloca(txtlen+1);
    wTextureList.GetText(sel,p);
    SetDlgItemText(IDC_OLDNAMEMASK,p);
    }
  }

CIMassRenameTextures::CIMassRenameTextures(const char *oldname, const char *newname,bool allLods):
  _oldname(oldname),_newname(newname),_allLods(allLods)
    {
    recordable=false;
    }

int CIMassRenameTextures::Execute(LODObject *lod)
  {
  if (_allLods)
    for (int i=0;i<lod->NLevels();i++)
      {
      RenameInOneLod(lod->Level(i));
      }
  else
    RenameInOneLod(lod->Active());
  return 0;
  }

static const char *FindInMask(const char *text, const char *mask)
  {
  if (*mask!='*' && *mask!='?')
    {
    while (*text!=*mask && *text) text++;
    if (*text)
      {
      const char *comp=mask;
      const char *tbeg=text;
      while (*tbeg && *tbeg==*comp && *comp!='*' && *comp!='?') {tbeg++;comp++;}
      if (*tbeg==*comp || *comp=='*' || *comp=='?') return text;
      text++;
      }
    if (*mask==0) return text;
    }
  return NULL;
  }

static RString TryToRename(const char *src, const char *fndmask, const char *newname)
  {
  char *outbuff=(char *)alloca(strlen(src)+strlen(newname)+10);
  char *srccnv=(char *)_strupr(strcpy((char *)alloca(strlen(src)+1),src));
  char *fndcnv=(char *)_strupr(strcpy((char *)alloca(strlen(fndmask)+1),fndmask));
  char *outptr=outbuff;
  const char *fndptr=fndcnv;
  const char *newptr=newname;
  const char *srcptr=srccnv;
  while (true)
    {
    if (*fndptr=='*')
      {      
      fndptr++;
      const char *begfnd=FindInMask(srcptr,fndptr);
      if (begfnd==NULL) return src;
      if (*newptr=='*') 
        {
        memcpy(outptr,src+(srcptr-srccnv),begfnd-srcptr);
        outptr+=begfnd-srcptr;
        newptr++;
        }      
      srcptr=begfnd;       // a ve zdroji
      }
    else if (*fndptr=='?') 
        {
        if (!*srcptr) return src;
        *outptr++=src[srcptr-srccnv];
        srcptr++;
        fndptr++;
        if (*newptr=='?') newptr++;
        else if (*newptr!='*') return src;
        else if (*fndptr!='?') newptr++;
        }
    else 
      {      
      const char *lastfnd=fndptr;
      if (FindInMask(srcptr,fndptr)!=srcptr) return src;
      while (*fndptr && *fndptr!='*' && *fndptr!='?') {fndptr++;srcptr++;}
      while (*newptr!='*' && *newptr!='?' && *newptr) *outptr++=*newptr++;
      if (*newptr==0 || *fndptr==0) 
        {
        *outptr++=0;
        return RString(outbuff);
        }
      }
    }  
  }

void CIMassRenameTextures::RenameInOneLod(ObjectData *obj)
  {
  for (int i=0;i<obj->NFaces();i++)
    {    
    FaceT fc(obj,i);
    fc.SetTexture(TryToRename(fc.GetTexture(),_oldname,_newname));
    fc.SetMaterial(TryToRename(fc.GetMaterial(),_oldname,_newname));
    }
  }

void CAllRenameDlg::OnOK() 
  {
  this->StopBuildList();
  CString from;
  CString to;
  wOldName.GetWindowText(from);
  wNewName.GetWindowText(to);
  comint.Begin(WString(IDS_MASSRENAME,from,to));
  comint.Run(new CIMassRenameTextures(from,to));
  this->StartBuildList();
  wOldName.SetFocus();
  }

void CAllRenameDlg::OnUndo() 
  {
  this->StopBuildList();
  theApp.m_pMainWnd->SendMessage(WM_COMMAND,ID_EDIT_UNDO,0);
  wOldName.SetFocus();
  this->StartBuildList();
  }

BOOL CAllRenameDlg::PreTranslateMessage(MSG* pMsg) 
  {
  if ((GetKeyState(VK_CONTROL) & 0x80) && 
    pMsg->message==WM_KEYDOWN &&
    (pMsg->wParam=='Z' || pMsg->wParam==VK_BACK))
    {
    OnUndo();return TRUE;
    }
  return CDialog::PreTranslateMessage(pMsg);
  }
