#pragma once
#include "afxwin.h"


// DlgInProgress dialog

class DlgInProgress : public CDialog
{
	DECLARE_DYNAMIC(DlgInProgress)
    float _progressVal;
    float _lastpvupdate;
    bool _stopped;
    bool _canstop;
    bool _noprogressmode;
    DWORD _estLastValue;
    DWORD _estLastTime;
    DWORD _estEnd;
    DWORD _estCnt;
    


    
public:
	DlgInProgress(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgInProgress();

// Dialog Data
#ifdef PUBLIC
	enum { IDD = IDD_INPROGRESS_PUBLIC };
#else
  enum { IDD = IDD_INPROGRESS };
#endif

    void SetProgress(float val);
    void SetDescription(const char *text);
    bool IsStopped();
    void EstimateTime(DWORD timeNow);
    void EnableProgress(bool Enable) {_noprogressmode=!Enable;}

    

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CButton wProgress;
  afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
  CStatic wText;
  afx_msg void OnBnClickedStop();
  virtual BOOL OnInitDialog();
  afx_msg void OnTimer(UINT nIDEvent);

  void WaitForNextUpdate();
protected:
  virtual void OnOK();
  virtual void OnCancel();
};
