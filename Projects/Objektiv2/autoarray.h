// CAutoArray.h: interface for the CAutoArray class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CAutoArray_H__FED10224_55E1_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_CAutoArray_H__FED10224_55E1_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <iostream>
using namespace std;

#define L1CACHEPAGE 2048

template <class Item> 
class CAutoArray  
  {
  protected:
    Item * AllocSlot();
    Item **array;	//points to itemarray
    int Slotcount;	//count of used slots
    int Slotsize;	//max count of items in slot
    int Slotalloc;	//total count of slots
    int Lastcount;	//count of items in last slot
    Item * FindItem(unsigned int index) const;
  public:
    void FreeExtra();
    void RemoveSlot(int slotindex);
    void InsertSlot(int slotindex);
    void Delete(int index, int count);
    void Reserve(int index, int count);
    unsigned int Insert(const Item& p);
    Item& operator[] (unsigned int index);
    const Item& operator[] (unsigned int index) const;
    int GetCount() const 
      {return Slotsize*(Slotcount-1)+Lastcount;}
    void Release(int toindex=-1);
    void Expand(unsigned int items);
    CAutoArray(int slotsize=(L1CACHEPAGE/sizeof(Item)));
    CAutoArray(const CAutoArray<Item>& other);
    CAutoArray<Item>& operator=(const CAutoArray<Item>& other);
    void Save(ostream& str);	
    void Load(istream& str);	
    virtual ~CAutoArray();
    Item *Flatten(bool move=false);
  };

//--------------------------------------------------

template <class Item>
ostream& operator<<(ostream& str,const CAutoArray<Item>& itm)
  {itm.Save(str);return str;}

//--------------------------------------------------

template <class Item>
istream& operator>>(istream& str,const CAutoArray<Item>& itm)
  {itm.Load(str);return str;}

//--------------------------------------------------

class CObkException
  {
  public:
    long errn;
    unsigned long line;
    char source[248];
    
    CObkException(long errn, unsigned long line, const char *src)
      {
      this->errn=errn;
      this->line=line;
      strncpy(source,src,sizeof(source));
      source[sizeof(source)-1]=0;
      }
  };

//--------------------------------------------------

#define RAISE(num) throw CObkException(num,__LINE__,__FILE__)

#include "AutoArray.tli"

#endif // !defined(AFX_CAutoArray_H__FED10224_55E1_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
