#if !defined(AFX_EXTRUDESTEPSBAR_H__5672C621_2B16_48B4_90A5_6F4E35471F2E__INCLUDED_)
#define AFX_EXTRUDESTEPSBAR_H__5672C621_2B16_48B4_90A5_6F4E35471F2E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExtrudeStepsBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CExtrudeStepsBar dialog

class CExtrudeStepsBar : public CDialogBar
  {
  // Construction
  CSpinButtonCtrl spin;
  CEdit edit;
  CSize cursize;
  
  public:
    int GetSegments();
    CExtrudeStepsBar();   // standard constructor
    CDIALOGBAR_RESIZEABLE
      void Create(CWnd *parent,int menuid);
    
    // Dialog Data
    //{{AFX_DATA(CExtrudeStepsBar)
    enum 
      { IDD = IDD_EXTRUDESTEPS };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CExtrudeStepsBar)
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CExtrudeStepsBar)
    afx_msg void OnDeltaposSegspin(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTRUDESTEPSBAR_H__5672C621_2B16_48B4_90A5_6F4E35471F2E__INCLUDED_)
