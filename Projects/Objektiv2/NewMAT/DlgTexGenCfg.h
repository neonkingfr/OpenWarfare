#pragma once

#include "../geMatrix4.h"
// CDlgTexGenCfg dialog

class CDlgTexGenCfg : public CDialog
{
	DECLARE_DYNAMIC(CDlgTexGenCfg)

  int _number;
  bool _matrixValid;
  bool _trnsUpdate;
  CString _initUVSource;

  geMatrix4 _initialMatrix;
  geMatrix4 _resultMatrix;

  CComboBox wUVSource;
  CComboBox wTransformStack;
  CComboBox wTransformOper;
  CEdit wTransformValue;


public:
  virtual BOOL OnInitDialog();
	CDlgTexGenCfg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgTexGenCfg();

  void SetNumber(int number);
  int GetNumber() {return _number;};
  const geMatrix4 & GetTransform(void);
  void ResetMatrix(void);
  void SaveTransform(void);
  void Serialize(IArchive &arch);
  void SetUVSource(const char *texture);
  void LoadTexGenPanel(ParamEntryPtr stageinfo);
   
  virtual void OnOK(){}; //overwriting default close OK/Cancem methods
  virtual void OnCancel(){};

  CString GetUVSource();
// Dialog Data
	enum { IDD = IDD_TEXGENCFG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedTexgenEditmatrix();
  afx_msg void OnDeltaposTexgenValuespin(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedDeletetransform();
  afx_msg void OnBnClickedTexgenInserttransform();
  afx_msg void OnCbnSelchangeTexgenStacklist();
  afx_msg void OnCbnSelchangeTexgenUvsource();
  afx_msg void OnCbnEditupdateTexgenUvsource();
  afx_msg void OnCbnSelchangeTexgenOperation();
  afx_msg void OnEnChangeTexgenValue();
//  afx_msg void OnCbnEditchangeTexgenUvsource();
//  afx_msg void OnCbnSelendokTexgenUvsource();
};
