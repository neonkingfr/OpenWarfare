#pragma once


// DlgUpdateMaterial dialog

class DlgUpdateMaterial : public CDialog
{
	DECLARE_DYNAMIC(DlgUpdateMaterial)

public:
	DlgUpdateMaterial(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgUpdateMaterial();

// Dialog Data
	enum { IDD = IDD_UPDATEMATERIAL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
    virtual void OnOK();
    virtual void OnCancel();
public:
  CString vFilename;
  virtual BOOL PreTranslateMessage(MSG* pMsg);
};
