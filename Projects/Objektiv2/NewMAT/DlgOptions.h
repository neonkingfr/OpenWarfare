#pragma once
#include "afxwin.h"
//#include "t:\h\atlmfc\include\afxwin.h"


// DlgOptions dialog

class DlgOptions : public CDialog
{
	DECLARE_DYNAMIC(DlgOptions)

public:
	DlgOptions(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgOptions();

// Dialog Data
	enum { IDD = IDD_CONFIGDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  void OnOK();


  void SaveConfig();
  void LoadConfig();
  CString vTemplatePath;

  Pathname GetTemplatePath();
  afx_msg void OnBnClickedO2options();
  CComboBox wDefColScheme;
  CString vDefColScheme;
  virtual BOOL OnInitDialog();
  afx_msg void OnBnClickedBrose();
  CEdit wTextEditor;
  CString vTextEditor;
  float vRangeMinVal;
  float vRangeMaxVal;
};
