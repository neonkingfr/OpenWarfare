// DlgMaterialEditor.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgMaterialEditor.h"

#include "DlgColorCfg.h"
#include "DlgSpecularCfg.h"
#include "DlgGeneralCfg.h"
#include "DlgStageCfg.h"
#include "DlgStage0Cfg.h"
#include ".\dlgmaterialeditor.h"
/*#include "..\TraditionalInterface\ObjectData.h"
#include "..\TraditionalInterface\FaceT.h"*/
#include <projects/ObjektivLib/IExternalViewer.h>
#include "DlgTypeManager.h"
#include <io.h>
#include <el/interfaces/iScc.hpp>
#include "DlgTemplateManager.h"
#include <projects\bredy.libs\Archive\Archive.h>

#include "DlgCheckInComments.h"
#include "DlgProceduralTex.h"
#include "DlgNextPassCfg.h"
#include "DlgAskCheckIn.h"


#include <strstream>
#include <fstream>

using namespace std;

#define     GetWindowInstance(hwnd) ((HINSTANCE)GetWindowLongPtr(hwnd, GWLP_HINSTANCE))

#define UPDATETIMEREVENT 100
#define ENABLEDIRTY 101
#define SETNOTOP 103
#define UNDOTIMER 104

#define DLGIDOFFSET 100
#define SCROLLTIME 100
// DlgMaterialEditor dialog



IMPLEMENT_DYNAMIC(DlgMaterialEditor, CDialog)
DlgMaterialEditor::DlgMaterialEditor(CWnd* pParent /*=NULL*/)
: CDialog(DlgMaterialEditor::IDD, pParent)
{
  memset(_panels,0,sizeof(_panels));
  _formWidth=0;
  _waitUpdate=false;
  _dirty=false;
  _texGenCount=-1;
  yellow.CreateSolidBrush(RGB(255,255,0));
}

inline bool IsMyWindow(HWND my, HWND win)
{
  while (win && win!=my) win=GetParent(win);
  return win==my;
}


DlgMaterialEditor::~DlgMaterialEditor()
{
}

void DlgMaterialEditor::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DlgMaterialEditor, CDialog)
  ON_WM_DESTROY()
  ON_COMMAND_RANGE(DLGIDOFFSET,DLGIDOFFSET+MAT_MAXIMUMWINDOWS,OnShowHidePanel)
  ON_WM_VSCROLL()
  ON_WM_SIZE()
  ON_WM_GETMINMAXINFO()  
  ON_WM_CTLCOLOR()
  ON_MESSAGE(MATMSG_COLORCHANGED,OnMsgColorChanged)
  ON_MESSAGE(MATMSG_DETECTCYCLES,OnMsgDetectCycles)
  ON_MESSAGE(MATMSG_GETMATERIALNAME, OnMsgGetMaterialName)
  ON_MESSAGE(MATMSG_STAGECHANGED , OnMsgStageChanged)
  ON_MESSAGE(MATMSG_CONFIG_CHANGES,OnMsgStageChanged)
  ON_COMMAND(IDC_ADDSTAGE,OnAddStage)
  ON_WM_TIMER()
  ON_WM_INITMENUPOPUP()
  ON_COMMAND(ID_FILE_EXIT, OnFileExit)
  ON_COMMAND(ID_FILE_NEW, OnFileNew)
  ON_COMMAND(ID_VIEW_NEWWINDOW, OnViewNewwindow)
  ON_WM_ACTIVATE()
  ON_COMMAND(ID_FILE_SAVE, OnFileSave)
  ON_COMMAND(ID_FILE_SAVEAS, OnFileSaveas)
  ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
  ON_COMMAND(ID_FILE_OPTIONS, OnFileOptions)
  ON_COMMAND(ID_FILE_MATERIALTYPEMANAGER, OnFileMaterialtypemanager)
  ON_COMMAND(ID_SOUCECONTROL_RELOADTEMPLATESTYPES, OnSoucecontrolReloadtemplatestypes)
  ON_MESSAGE(MATMSG_RESTRICTIONS_CHANGES,OnRestrictionsChanges)
  ON_COMMAND(ID_SAVE, OnSave)
  ON_COMMAND(ID_FILE_SAVEASTEMPLATE, OnFileSaveastemplate)
  ON_COMMAND(ID_FILE_SAVEANDCHECKIN, OnFileSaveandcheckin)
  ON_UPDATE_COMMAND_UI(ID_FILE_SAVEANDCHECKIN, OnUpdateFileSaveandcheckin)
  ON_COMMAND(ID_FILE_OPENFROMSOURCECONTROL, OnFileOpenfromsourcecontrol)
  ON_UPDATE_COMMAND_UI(ID_FILE_OPENFROMSOURCECONTROL, OnUpdateFileOpenfromsourcecontrol)
  ON_COMMAND(ID_SOUCECONTROL_CHECKOUT, OnSoucecontrolCheckout)
  ON_UPDATE_COMMAND_UI(ID_SOUCECONTROL_CHECKOUT, OnUpdateSoucecontrolCheckout)
  ON_COMMAND(ID_SOUCECONTROL_GETLATESTVERSION, OnSoucecontrolGetlatestversion)
  ON_UPDATE_COMMAND_UI(ID_SOUCECONTROL_GETLATESTVERSION, OnUpdateSoucecontrolGetlatestversion)
  ON_COMMAND(ID_SOUCECONTROL_HISTORY, OnSoucecontrolHistory)
  ON_UPDATE_COMMAND_UI(ID_SOUCECONTROL_HISTORY, OnUpdateSoucecontrolHistory)
  ON_COMMAND(ID_SOUCECONTROL_UNDOCHECKOUT, OnSoucecontrolUndocheckout)
  ON_UPDATE_COMMAND_UI(ID_SOUCECONTROL_UNDOCHECKOUT, OnUpdateSoucecontrolUndocheckout)
  ON_MESSAGE(MATMSG_DELSTAGE,OnDelStageCommand)
  ON_COMMAND(ID_INSERT_STAGE, OnInsertStage)
  ON_COMMAND(ID_PRIMITIVE_CREATE, OnPrimitiveCreate)
  ON_COMMAND(ID_PRIMITIVE_LOADFROMFILE, OnPrimitiveLoadfromfile)
  ON_COMMAND(ID_PRIMITIVE_SELECTTEXTURE, OnPrimitiveSelecttexture)
  ON_COMMAND(ID_VIEW_SHOWPRIMITIVE, OnViewShowprimitive)
  ON_COMMAND(ID_VIEW_SHOWMODEL, OnViewShowmodel)
  ON_COMMAND(ID_EDIT_TEXTEDITO, OnEditTextedito)
  ON_WM_SETCURSOR()
  ON_WM_LBUTTONDOWN()
  ON_WM_MOUSEMOVE()
  ON_WM_LBUTTONUP()
  ON_WM_RBUTTONUP()
  ON_WM_MOUSEWHEEL()
  ON_COMMAND(ID_PRIMITIVE_SETCOLOR, OnPrimitiveSetcolor)
  ON_COMMAND(ID_PRIMITIVE_SETWHITE, OnPrimitiveSetwhite)
  ON_COMMAND(ID_PRIMITIVE_CLEARTEXTURE, OnPrimitiveCleartexture)
  ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
  ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
  ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
  ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)
  ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
  ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
  ON_COMMAND(ID_VIEW_SELECTINO2, OnViewSelectino2)
  ON_COMMAND(ID_PRIMITIVE_SETCOLOR01, OnPrimitiveSetcolor01)
  ON_COMMAND(ID_PANEL_NEXTPASS, OnPanelNextpass)
  ON_COMMAND(ID_TAB_CLOSETAB, OnTabClosetab)
  ON_COMMAND(ID_FILE_RELOAD, OnFileReload)
  ON_COMMAND(ID_VIEW_TEXGEN, OnViewTexgen)
  ON_UPDATE_COMMAND_UI(ID_VIEW_TEXGEN, OnUpdateViewTexgen)
END_MESSAGE_MAP()

static bool IsDialogAtMouse(CWnd *parent,const CPoint &ms)
{
  CPoint pt;
  pt=ms;
  parent->ScreenToClient(&pt);
  CWnd *child=parent->ChildWindowFromPoint(pt,CWP_SKIPINVISIBLE|CWP_SKIPDISABLED|CWP_SKIPTRANSPARENT);
  CWnd *prev=NULL;
  while (child)
  {
    prev=child;
    pt=ms;
    child->ScreenToClient(&pt);
    child=child->ChildWindowFromPoint(pt,CWP_SKIPINVISIBLE|CWP_SKIPDISABLED|CWP_SKIPTRANSPARENT);
    if (child==prev) child=NULL;
  }
  if (prev==NULL) return FALSE;
  char buff[256];
  GetClassName(*prev,buff,256);
  return _stricmp(buff,"#32770");
}

static bool SccError(SCCRTN rtn)
{
  if (rtn)
  {
    CString msg;
    msg.Format(IDS_SOURCESAFEERROR,rtn);
    theApp.GetMyTopLevelWindow(NULL)->MessageBox(msg,StrRes[IDS_MATNAME],MB_ICONSTOP);
    return false;
  }
  return true;
}

void EditInTextEditor(CWnd *wnd,const char *filename);

class InitByRestriction: public MassProcess
{
public:
  DlgGeneralCfg *dialog;
  AutoArray<SMatRestricts> *matRestricts;
  virtual bool ProcessFile(const char *filename);    
};

bool InitByRestriction::ProcessFile(const char *filename)
{  
  SMatRestricts restricts;  
  if (restricts.LoadRestricts(filename)==true)
  {
    dialog->InitByRestricts(restricts);
    matRestricts->Append()=restricts;
  }
  return true;
}

// DlgMaterialEditor message handlers


int DlgMaterialEditor::CreatePanel(CDialog * panel)
{
  for (int i=0;i<MAT_MAXIMUMWINDOWS;i++)
    if (_panels[i]==NULL)
    {
      _panels[i]=panel;
      CRect rc;
      panel->GetWindowRect(&rc);
      _dlgsizes[i]=rc.Size();
      return i;
    } 
    return -1;
}

bool DlgMaterialEditor::DeletePanel(int index)
{
  _panels[index]->DestroyWindow();
  delete _panels[index];
  for (int i=index+1;i<MAT_MAXIMUMWINDOWS;i++)
  {    
    _panels[i-1]=_panels[i];
    _dlgsizes[i-1]=_dlgsizes[i];
    if (_panels[i]==NULL) return true;
  }
  _panels[MAT_MAXIMUMWINDOWS-1]=NULL;
  return true;
}

bool DlgMaterialEditor::DeletePanel(CDialog * panel)
{
  int index;
  for (index=0;index<MAT_MAXIMUMWINDOWS && _panels[index];index++)
    if (_panels[index]==panel) 
      return DeletePanel(index);
  return false;
}

void DlgMaterialEditor::RecalcLayout(bool update)
{
  HDWP defer=BeginDeferWindowPos(2*MAT_MAXIMUMWINDOWS);
  CRect dlgrc(0,0,12,10);
  MapDialogRect(&dlgrc);
  int ypos=0;
  int scrollpos=GetScrollPos(SB_VERT);
  _formWidth=0;
  for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
  {
    CRect rc;
    _panels[i]->GetWindowRect(&rc);
    DeferWindowPos(defer,*_panels[i],NULL,dlgrc.right,ypos-scrollpos,0,0,SWP_NOZORDER|SWP_NOSIZE|(update?0:SWP_NOREDRAW));
    CWnd *checker=GetDlgItem(i+DLGIDOFFSET);
    if (checker) 
      DeferWindowPos(defer,*checker,NULL,0,ypos-scrollpos,dlgrc.right,dlgrc.bottom,SWP_NOZORDER|(update?0:SWP_NOREDRAW));
    ypos+=rc.bottom-rc.top;
    _formWidth=dlgrc.right+rc.right-rc.left+GetSystemMetrics(SM_CXVSCROLL)+4*GetSystemMetrics(SM_CXEDGE);
  }
  _formLength=ypos;
  EndDeferWindowPos(defer);
}

BOOL DlgMaterialEditor::OnInitDialog()
{
  CDialog::OnInitDialog();

  _generalPanel=new DlgGeneralCfg(this);_generalPanel->Create(_generalPanel->IDD,this);
  _ambientPanel=new DlgColorCfg(this);_ambientPanel->Create(_ambientPanel->IDD,this);
  _diffusePanel=new DlgColorCfg(this);_diffusePanel->Create(_diffusePanel->IDD,this);
  _forcedDPanel=new DlgColorCfg(this);_forcedDPanel->Create(_forcedDPanel->IDD,this);
  _emmissvPanel=new DlgColorCfg(this);_emmissvPanel->Create(_emmissvPanel->IDD,this);
  _speculrPanel=new DlgColorCfg(this);_speculrPanel->Create(_speculrPanel->IDD,this);
  _specpowPanel=new DlgSpecularCfg(this);_specpowPanel->Create(_specpowPanel->IDD,this);
  _stage0Panel=new DlgStage0Cfg(this); _stage0Panel->Create(_stage0Panel->IDD,this);
  _nextPassPanel=NULL;

  _ambientPanel->SetColor(1,1,1,1);
  _diffusePanel->SetColor(1,1,1,1);
  _forcedDPanel->SetColor(0,0,0,0);
  _emmissvPanel->SetColor(0,0,0,1);
  _speculrPanel->SetColor(0,0,0,1);

  CreatePanel(_generalPanel);
  CreatePanel(_ambientPanel);
  CreatePanel(_diffusePanel);
  CreatePanel(_forcedDPanel);
  CreatePanel(_emmissvPanel);
  CreatePanel(_speculrPanel);
  CreatePanel(_specpowPanel);
  _stageIndex=CreatePanel(_stage0Panel)+1;
  RecreateOtherControls();  
  RecalcLayout();
  RecalcScrollbar();

  _ambientPanel->SetColorName(StrRes[IDS_AMBIENTCOLOR],IDS_AMBIENTCOLOR);
  _diffusePanel->SetColorName(StrRes[IDS_DIFFUSECOLOR],IDS_DIFFUSECOLOR);
  _forcedDPanel->SetColorName(StrRes[IDS_FORCEDDIFFUSE],IDS_FORCEDDIFFUSE);
  _emmissvPanel->SetColorName(StrRes[IDS_EMMISIVE],IDS_EMMISIVE);
  _speculrPanel->SetColorName(StrRes[IDS_SPECULAR],IDS_SPECULAR);

  InitByRestricts();
  SetWindowPos(NULL,0,0,_formWidth,500,SWP_NOZORDER|SWP_NOMOVE);
  //ShowWindow(SW_SHOW);
  //SetTimer(SETNOTOP,500,NULL);
  SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);

  SetIcon(theApp.LoadIcon(IDI_MAINICON),FALSE);
  SetIcon(theApp.LoadIcon(IDI_MAINICON),TRUE);
  _dirty=false;
  NewDocument();

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgMaterialEditor::RecreateOtherControls(void)
{
  int i;
  for (i=0;i<MAT_MAXIMUMWINDOWS;i++)
  {
    CWnd *wnd=GetDlgItem(i+DLGIDOFFSET);
    if (wnd) wnd->DestroyWindow();
  }
  for (i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
  {
    int id=i+DLGIDOFFSET;
    WINDOWPLACEMENT wp;
    wp.length=sizeof(wp);
    _panels[i]->GetWindowPlacement(&wp); 
    HWND hWnd=::CreateWindow("BUTTON","",WS_CHILD|WS_VISIBLE|BS_CHECKBOX|BS_NOTIFY|(i?0:WS_TABSTOP|WS_GROUP),
      wp.rcNormalPosition.right,wp.rcNormalPosition.top,20,20,*this,
      *(HMENU *)&id,AfxGetInstanceHandle(),NULL);
    if (wp.rcNormalPosition.bottom-wp.rcNormalPosition.top==_dlgsizes[i].cy)
      CheckDlgButton(id,BST_CHECKED);
    //    ::SetWindowPos(hWnd,i==0?HWND_TOP:*_panels[i-1],0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  }  
}

void DlgMaterialEditor::OnOK()
{
  //No reaction on OK
}

void DlgMaterialEditor::OnCancel()
{
  if (IsIconic()) OpenIcon();
  //if (this->AskExitMAT())  DestroyWindow(); // disabled MatEditor was crashing on ESC pressed
}

void DlgMaterialEditor::OnDestroy()
{
  CDialog::OnDestroy();
  for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++) 
  {
    _panels[i]->DestroyWindow();
    delete _panels[i];
  }
  memset(_panels,0,sizeof(_panels));
  theApp.CallGen().ReloadViewer();
  _rvmatName.SetNull();
  _dirty=false;
  _enabledirty=false;
  _waitUpdate=false;
}

void DlgMaterialEditor::OnShowHidePanel(UINT cmd)
{  
  int pos=cmd-DLGIDOFFSET;
  CRect rc(0,0,10,10);
  MapDialogRect(&rc);
  DWORD time=GetTickCount();
  DWORD curtime=time;
  if (!IsDlgButtonChecked(cmd))
  {
    CheckDlgButton(cmd,1);
    while (curtime-SCROLLTIME<time)
    {
      int ipos=rc.bottom+(_dlgsizes[pos].cy-rc.bottom)*(curtime-time)/SCROLLTIME;  
      _panels[pos]->SetWindowPos(NULL,0,0,_dlgsizes[pos].cx,ipos,SWP_NOMOVE|SWP_NOZORDER);
      RecalcLayout();
      UpdateWindow();
      Sleep(10);
      curtime=GetTickCount();
    }
    _panels[pos]->SetWindowPos(NULL,0,0,_dlgsizes[pos].cx,_dlgsizes[pos].cy,SWP_NOMOVE|SWP_NOZORDER);
    _panels[pos]->EnableWindow(TRUE);
  }
  else
  {
    CheckDlgButton(cmd,0);
    while (curtime-SCROLLTIME<time)
    {
      int ipos=_dlgsizes[pos].cy-(_dlgsizes[pos].cy-rc.bottom)*(curtime-time)/SCROLLTIME;  
      _panels[pos]->SetWindowPos(NULL,0,0,_dlgsizes[pos].cx,ipos,SWP_NOMOVE|SWP_NOZORDER);
      RecalcLayout();
      UpdateWindow();
      Sleep(10);
      curtime=GetTickCount();
    }
    _panels[pos]->SetWindowPos(NULL,0,0,_dlgsizes[pos].cx,rc.bottom,SWP_NOMOVE|SWP_NOZORDER);
    _panels[pos]->EnableWindow(FALSE);
  }
  RecalcLayout();
  RecalcScrollbar();
  EnsureVisible(*_panels[pos]);
}

void DlgMaterialEditor::RecalcScrollbar(void)
{
  CRect rc;
  GetClientRect(&rc);
  SCROLLINFO nfo;
  nfo.cbSize=sizeof(nfo);
  nfo.fMask=SIF_RANGE|SIF_PAGE;
  nfo.nPage=rc.bottom;
  nfo.nMin=0;
  nfo.nMax=_formLength;
  SetScrollInfo(SB_VERT,&nfo,TRUE);
}


void DlgMaterialEditor::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar *pScrollBar)
{
  if (pScrollBar==NULL)
  {
    int oldpos=GetScrollPos(SB_VERT);
    int newpos;
    CRect clientrc;
    GetClientRect(&clientrc);
    int formMax=_formLength-clientrc.bottom;
    switch (nSBCode)
    {
    case SB_BOTTOM: newpos=formMax;break;
    case SB_TOP: newpos=0;break;
    case SB_LINEDOWN: newpos=oldpos+1;break;
    case SB_LINEUP: newpos=oldpos-1;break;
    case SB_PAGEDOWN: newpos=oldpos+clientrc.bottom*3/4;break;
    case SB_PAGEUP: newpos=oldpos-clientrc.bottom*3/4;break;
    case SB_THUMBTRACK:
    case SB_THUMBPOSITION : newpos=nPos;break;
    case SB_ENDSCROLL: return;
    }
    if (newpos<0) newpos=0;
    if (newpos>formMax) newpos=formMax;
    SetScrollPos(SB_VERT,newpos,TRUE);
    RecalcLayout();
  }
}
void DlgMaterialEditor::OnSize(UINT nType, int cx, int cy)
{
  RecalcScrollbar();
}

void DlgMaterialEditor::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
  CDialog::OnGetMinMaxInfo(lpMMI);
  if (_formWidth)
  {
    lpMMI->ptMaxSize.x=_formWidth;
    lpMMI->ptMaxTrackSize.x=_formWidth;
    lpMMI->ptMinTrackSize.x=_formWidth;    
  }
}
void DlgMaterialEditor::EnsureVisible(HWND hWnd)
{
  CRect wrect;
  CRect wclient;
  if (!IsWindow(hWnd)) return;
  ::GetWindowRect(hWnd,&wrect);
  GetClientRect(&wclient);
  ScreenToClient(wrect);
  int posmin=GetScrollPos(SB_VERT);
  int posmax=posmin+wclient.bottom;
  CWnd *parent=CWnd::FromHandle(::GetParent(hWnd));
  if (parent)
  {
    for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
      if (_panels[i]==parent)
      {
        if (IsDlgButtonChecked(i+DLGIDOFFSET)==0)
          OnShowHidePanel(i+DLGIDOFFSET);
        break;
      }
  }
  if (wrect.top<0) 
  {
    posmin+=wrect.top;
    posmax=posmin+wclient.bottom;
  }
  else if (wrect.bottom>wclient.bottom)
  {
    posmax+=wrect.bottom-wclient.bottom;
    posmin=posmax-wclient.bottom;
  }
  else return;
  SetScrollPos(SB_VERT,posmin,TRUE);
  RecalcLayout();
}



BOOL DlgMaterialEditor::PreTranslateMessage(MSG* pMsg)
{
  bool myWin=IsMyWindow(*this,pMsg->hwnd);
  if (myWin)
    if (TranslateAccelerator(*this,theApp.hAccTable,pMsg)) return TRUE;
    else
    {
      CPoint ms=GetMessagePos();
      if (myWin &&(pMsg->message==WM_LBUTTONDOWN || pMsg->message==WM_MOUSEWHEEL) && !IsDialogAtMouse(this,ms) && GetCapture()==NULL)
      {
        SetCapture();
        BeginDragScroll(ms);
        return true;
      }
    }

    BOOL res=IsDialogMessage(pMsg);
    if (myWin && pMsg->message==WM_KEYDOWN)
    {
      HWND hwnd=::GetParent(pMsg->hwnd);
      while (hwnd!=*this && hwnd) hwnd=::GetParent(hwnd);
      if (hwnd)
      {
        HWND focus=::GetFocus();
        if (focus) EnsureVisible(focus);
      }
    }
    if (res) return res;
    return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH DlgMaterialEditor::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
  HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
  if (GetFocus()->GetSafeHwnd()==pWnd->GetSafeHwnd()) 
    return yellow;
  return hbr;
}


BOOL DlgMaterialEditor::OnCommand(WPARAM wParam, LPARAM lParam)
{
  if (LOWORD(wParam)>=DLGIDOFFSET && LOWORD(wParam)<DLGIDOFFSET+MAT_MAXIMUMWINDOWS)
  {
    GetDlgItem(LOWORD(wParam))->Invalidate(NULL);
  }
  return CDialog::OnCommand(wParam, lParam);
}

LRESULT DlgMaterialEditor::OnMsgColorChanged(WPARAM id,LPARAM lParam)
{
  DlgColorCfg *array[5];
  DlgColorCfg *sender=(DlgColorCfg *)lParam;
  array[0]=_ambientPanel;
  array[1]=_diffusePanel;
  array[2]=_forcedDPanel;
  array[3]=_emmissvPanel;
  array[4]=_speculrPanel;


  for (int i=0;i<5;i++)
  {
    if (array[i]->GetLinkedWith()==id)
    {
      array[i]->_r=sender->_r;
      array[i]->_g=sender->_g;
      array[i]->_b=sender->_b;
      array[i]->_a=sender->_a;
      array[i]->LoadColorValues();
    }
  }
  UpdateViewer(true);
  _needSaveUndo=true;
  _canSaveUndo=false;
  SetTimer(UNDOTIMER,250,NULL);
  return 0;
}

LRESULT DlgMaterialEditor::OnMsgDetectCycles(WPARAM id, LPARAM lParam)
{
  DlgColorCfg *array[5];
  DlgColorCfg *sender=(DlgColorCfg *)lParam;
  array[0]=_ambientPanel;
  array[1]=_diffusePanel;
  array[2]=_forcedDPanel;
  array[3]=_emmissvPanel;
  array[4]=_speculrPanel;
  WPARAM curid=sender->GetLinkedWith();  
  while (curid!=id)
  {
    int i;
    for (i=0;i<5;i++)
      if (curid==array[i]->GetControlID()) break;
    if (i==5) return 0;
    sender=array[i];
    curid=sender->GetLinkedWith();  
  }
  return 1;
}

int DlgMaterialEditor::AddStage(int number,int texGenCount)
{
  int end=0;
  if (number==-1)
  {  
    int i;
    number=1;
    for (i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
    {
      DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
      if (stage)
      {
        number=__max(number,stage->GetStageNumber()+1);
      }
    }
    end=i;
  }
  if (end>=MAT_MAXIMUMWINDOWS) return -1;
  DlgStageCfg *stage=new DlgStageCfg;
  stage->Create(stage->IDD,this);
  stage->SetTexGenCount(texGenCount);
  stage->SetStageNumber(number);
  return CreatePanel(stage);  
}


void DlgMaterialEditor::OnAddStage()
{
  int stage=AddStage(-1,_texGenCount);
  if (stage==-1) return;
  RecreateOtherControls();
  RecalcLayout();
  RecalcScrollbar();
  EnsureVisible(*_panels[stage]);
  CWnd *child=_panels[stage]->GetWindow(GW_CHILD);
  child=_panels[stage]->GetNextDlgTabItem(child);
  child->SetFocus();
}

static void LoadColorPanel(DlgColorCfg *panel,const ParamEntry *colorinfo)
{
  if (colorinfo && colorinfo->IsArray())
  {
    const ParamEntry &arr=*colorinfo;
    if (arr.GetSize()>3)
    {
      panel->_r=arr[0].GetFloat();
      panel->_g=arr[1].GetFloat();
      panel->_b=arr[2].GetFloat();
      panel->_a=arr[3].GetFloat();
      panel->LoadColorValues();
    }
  }
}

static geMatrix4 LoadTransform(const ParamEntry *matrix)
{
  geMatrix4 out(true);
  const char *names[]={"aside","up","dir","pos"};
  for (int i=0;i<4;i++)
  {
    geMatrixLine4 &values=out[i];
    ParamEntryPtr line=matrix->FindEntry(names[i]);
    if (line.NotNull() && line->IsArray() && line->GetSize()>2)
    {
      for (int j=0;j<3;j++)
        values[j]=(*(line.GetPointer()))[j].GetFloat();
    }
  }
  return out;
}

static void LoadStagePanel(DlgStageCfg *panel, const ParamEntry *stageinfo)
{
  if (stageinfo && stageinfo->IsClass())
  {
    ParamEntryPtr temp=stageinfo->FindEntry("texture");  
    if (temp.NotNull()) panel->SetTexture(temp->GetValue());
    temp=stageinfo->FindEntry("texGen");
    if (temp.NotNull()) 
    {
      panel->SetUVSource(temp->GetValue());
      panel->LockUVSource(true);
    }
    else
    {
    temp=stageinfo->FindEntry("uvSource");
    if (temp.NotNull()) panel->SetUVSource(temp->GetValue());  
    }
    temp=stageinfo->FindEntry("uvTransform");
    if (temp.NotNull() && temp->IsClass())
    {
      panel->_resultMatrix=LoadTransform(temp.GetPointer());
      panel->ResetMatrix();
    }
    temp=stageinfo->FindEntry("Filter");
    if (temp.NotNull()) panel->SetStageFilter(temp->GetValue());
  }
}

static void LoadSpecularPanel(DlgSpecularCfg *panel, const ParamEntry *specularinfo)
{
  if (specularinfo && (specularinfo->IsFloatValue() || specularinfo->IsIntValue()))
  {
    panel->_factor=*specularinfo;
    panel->LoadValues();
  }
}

static void LoadGeneralPanel(DlgGeneralCfg *panel, const ParamEntry *PixelShaderID, const ParamEntry *VertexShaderID)
{
  if (PixelShaderID && PixelShaderID->IsTextValue())
    panel->SetPixelShader(PixelShaderID->GetValue());
  else
    panel->SetPixelShader("Normal");
  if (VertexShaderID && VertexShaderID->IsTextValue())
    panel->SetVertexShader(VertexShaderID->GetValue());
  else
    panel->SetVertexShader("Basic");
}

static void LoadDefaultTexture(DlgStage0Cfg *panel, const ParamEntry *stage0)
{
  panel->SetTexture("");
  if (stage0 && stage0->IsClass())
  {
    ParamEntryPtr entry=stage0->FindEntry("texture");
    if (entry && entry->IsTextValue())      
      panel->SetTexture(entry->GetValue()); 
    entry=stage0->FindEntry("Filter");
    if (entry)
      panel->SetStageFilter(entry->GetValue());
  }
}

static void LoadRenderFlags(DlgGeneralCfg *panel, const ParamEntry *flags)
{
  panel->ResetAllFlags();
  if (flags && flags->IsArray())
  {
    int size=flags->GetSize();
    for (int i=0;i<size;i++)
    {
      RString val=(*flags)[i];
      panel->SetFlag(val,true);
    }
  }
}

static void LoadNextPassPanel(DlgMaterialEditor *editor, DlgNextPassCfg * &_nextPassPanel, const ParamEntry *data)
{
  if (data && data->IsTextValue())
  {
    if (_nextPassPanel==NULL)
    {
      _nextPassPanel=new DlgNextPassCfg;
      _nextPassPanel->Create(_nextPassPanel->IDD,editor);
      editor->CreatePanel(_nextPassPanel);
    }
    CString d=data->GetValue();
    if (d[1]!=':') d=theApp.CallGen().GetProjectRoot()+d;
    _nextPassPanel->wMatName.SetWindowText(d);
  }
}

int DlgMaterialEditor::LoadTexGens(ParamFile &material)
{
  texGenDlg.DeleteAllTexGens();
  int entrycnt=material.GetEntryCount();
  int c=0;
  for (int i=0;i<entrycnt;i++)
  {
    const ParamEntryPtr texGen=&material.GetEntry(i);
    RStringB name=texGen->GetName();
    if (texGen->IsClass() && strnicmp(name,"TexGen",6)==0)
    {
      int num=atoi(name.Data()+6);
      int texGenid=texGenDlg.AddTexGen(texGen,num);
      ++c;
    }
  } 
  if (c==0) return -1;
  return c;
};
int DlgMaterialEditor::LoadMaterial(ParamFile &material)
{
  ParamEntryPtr ambient=material.FindEntry("ambient");
  ParamEntryPtr diffuse=material.FindEntry("diffuse");
  ParamEntryPtr forcedDiffuse=material.FindEntry("forcedDiffuse");
  ParamEntryPtr emmisive=material.FindEntry("emmisive");
  ParamEntryPtr specular=material.FindEntry("specular");
  ParamEntryPtr specularPower=material.FindEntry("specularPower");
  ParamEntryPtr PixelShaderID=material.FindEntry("PixelShaderID");
  ParamEntryPtr VertexShaderID=material.FindEntry("VertexShaderID");
  ParamEntryPtr Stage0=material.FindEntry("Stage0");
  ParamEntryPtr renderFlags=material.FindEntry("renderFlags");
  ParamEntryPtr nextPass=material.FindEntry("nextPass");

  DeleteAllStages();
  int entrycnt=material.GetEntryCount();
  for (int i=0;i<entrycnt;i++)
  {
    const ParamEntry &stage=material.GetEntry(i);
    RStringB name=stage.GetName();
    if (stage.IsClass() && strnicmp(name,"Stage",5)==0)
    {
      int num=atoi(name.Data()+5);
      if (num)
      {
        int stageid=AddStage(num,_texGenCount);
        if (stageid!=-1)
        {
          DlgStageCfg *stagepanel=static_cast<DlgStageCfg *>(_panels[stageid]);
          LoadStagePanel(stagepanel,&stage);
        }
      }
    }
  }  
  LoadColorPanel(_ambientPanel,ambient.GetPointer());
  LoadColorPanel(_diffusePanel,diffuse.GetPointer());
  LoadColorPanel(_forcedDPanel,forcedDiffuse.GetPointer());
  LoadColorPanel(_emmissvPanel,emmisive.GetPointer());
  LoadColorPanel(_speculrPanel,specular.GetPointer());
  LoadSpecularPanel(_specpowPanel,specularPower.GetPointer());
  LoadGeneralPanel(_generalPanel,PixelShaderID.GetPointer(),VertexShaderID.GetPointer());
  LoadRenderFlags(_generalPanel,renderFlags.GetPointer());
  LoadNextPassPanel(this,_nextPassPanel,nextPass.GetPointer());
  LoadDefaultTexture(_stage0Panel,Stage0.GetPointer());
  RecreateOtherControls();
  RecalcLayout();
  RecalcScrollbar();
  QOStrStream tmp;
  material.Save(tmp,0);
  QIStrStream tmpin(tmp.str(),tmp.pcount());
  _pfCopy.Parse(tmpin);
  return 0;
}

int DlgMaterialEditor::LoadMaterial(const char * xfilename)
{    
  char *filename=strcpy((char *)alloca(strlen(xfilename)+1),xfilename);
  if (NewDocument()==false) return -1;
  _enabledirty=false;
  ParamFile material;
  LSError err=material.Parse(filename);
  if (err!=LSOK) 
  {
    OnTabClosetab();
    DisplayPFileError(err,filename);return err;
  }
  _texGenCount= LoadTexGens(material);
  LoadMaterial(material);
  if (_ambientPanel->_r==_diffusePanel->_r && 
    _ambientPanel->_g==_diffusePanel->_g && 
    _ambientPanel->_b==_diffusePanel->_b &&
    _ambientPanel->_a==_diffusePanel->_a)
  {
    _diffusePanel->SetLinkedWith(IDS_NOLINK);
    _ambientPanel->SetLinkedWith(IDS_DIFFUSECOLOR);
    ShowPanel(_ambientPanel,false);
  }
  else
  {
    _diffusePanel->SetLinkedWith(IDS_NOLINK);
    _ambientPanel->SetLinkedWith(IDS_NOLINK);
    ShowPanel(_ambientPanel,true);
  }

  ShowPanel(_forcedDPanel,!(_forcedDPanel->_r==0 && _forcedDPanel->_g==0 && _forcedDPanel->_b==0));    
  ShowPanel(_emmissvPanel,!(_emmissvPanel->_r==0 && _emmissvPanel->_g==0 && _emmissvPanel->_b==0));    
  if (_speculrPanel->_r==0 && _speculrPanel->_g==0 && _speculrPanel->_b==0)
  {
    ShowPanel(_speculrPanel,false);
    ShowPanel(_specpowPanel,false);
  }
  else
  {
    int i;
    bool irradiance=false;
    for (i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
    {
      DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
      if (stage)
      {
        CString texture=stage->GetTexture();
        if (texture.Find("irradiance",0)!=-1) {irradiance=true;break;}
      }
    }
    ShowPanel(_speculrPanel,true);
    ShowPanel(_specpowPanel,!irradiance);
  }
  ShowPanel(_stage0Panel,_stage0Panel->vTextureName.GetLength()!=0);
  EnsureVisible(*_generalPanel);
  CSingleLock lock(&csect,TRUE);
  if (!_rvmatTempName.IsNull()) DeleteFile(_rvmatTempName);
  Pathname tmpname(filename);
  _rvmatName=tmpname;
  char *buff=(char *)alloca(strlen(_rvmatName.GetExtension())+10);
  strcpy(buff,".tmp");
  strcat(buff,_rvmatName.GetExtension());
  _rvmatTempName=_rvmatName;
  _rvmatTempName.SetExtension(buff);
  theApp.CallGen().ReloadViewer();
  _generalPanel->SetMaterialName(filename);
  _dirty=false;
  UpdateTitle();
  SetMaterialTypeFromDialog();
  SetTimer(ENABLEDIRTY,500,NULL);
  UpdateSSStatus();
  GetFileTime(filename,_edit_file_time);
  return 0;
}

static void CALLBACK SetMsgEditButton(HWND hwnd,
                                      UINT uMsg,
                                      UINT_PTR idEvent,
                                      DWORD dwTime
                                      )
{
  CWnd *w=CWnd::GetActiveWindow();
  w->SetDlgItemText(IDRETRY,CString(MAKEINTRESOURCE(IDS_TEXTEDITORBUTTON)));
  w->SetDlgItemText(IDCANCEL,CString(MAKEINTRESOURCE(IDS_OKBUTTON)));
  KillTimer(hwnd,idEvent);
}

void DlgMaterialEditor::DisplayPFileError(LSError error, const char *filename)
{
  const char *message;
  switch (error)
  {
  case LSFileNotFound: message="LSFileNotFound";break;
  case LSBadFile: message="LSBadFile";break;
  case LSStructure: message="LSStructure";break;
  case LSUnsupportedFormat: message="LSUnsupportedFormat";break;
  case LSVersionTooNew: message="LSVersionTooNew";break;
  case LSVersionTooOld: message="LSVersionTooOld";break;
  case LSDiskFull: message="LSDiskFull";break;
  case LSAccessDenied: message="LSAccessDenied";break;
  case LSDiskError: message="LSDiskError";break;
  case LSNoEntry: message="LSNoEntry";break;
  case LSUnknownError: message="LSUnknownError";break;
  default: message="Unspecified error";break;
  }
  CString buff;
  buff.Format(IDS_RVMATERROR,error,message,filename);
  ::SetTimer(0,0,10,SetMsgEditButton);
  int id=theApp.GetMyTopLevelWindow(this)->MessageBox(buff,StrRes[IDS_MATNAME],MB_RETRYCANCEL|MB_ICONEXCLAMATION|MB_DEFBUTTON1);
  if (id==IDRETRY)
  {
    EditInTextEditor(this,filename);
  }
}

void DlgMaterialEditor::DeleteAllStages(BTree<RStringI> *scaveFiles)
{
  for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
  {
    DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
    if (stage)
    {
      if (scaveFiles) stage->ScaveFiles(*scaveFiles);
      DeletePanel(stage);
      i--;
    }
  }
  RecreateOtherControls();
  RecalcLayout();
  RecalcScrollbar();
}

bool DlgMaterialEditor::ShowPanel(CDialog * panel, bool show)
{
  for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
  {
    if (_panels[i]==panel)
    {
      if ((IsDlgButtonChecked(i+DLGIDOFFSET)==1)!=show)
      {
        OnShowHidePanel(i+DLGIDOFFSET);
        return true;        
      }
    }
  }
  return false;
}

static void AddColorToPF(ParamFile &pf, const char *name, DlgColorCfg *panel)
{
  ParamEntryPtr array;
  array=pf.AddArray(name);
  array->AddValue(panel->_r);
  array->AddValue(panel->_g);
  array->AddValue(panel->_b);
  array->AddValue(panel->_a);  
}

bool DlgMaterialEditor::SaveMaterial(ParamFile &pf, bool forpreview)
{
  CSingleLock lock(&csect,TRUE);
  pf.Delete("ambient");
  pf.Delete("diffuse");
  pf.Delete("forcedDiffuse");
  pf.Delete("emmisive");
  pf.Delete("specular");
  pf.Delete("specularPower");
  pf.Delete("renderFlags");
  pf.Delete("nextPass");
  pf.Delete("PixelShaderID");
  pf.Delete("VertexShaderID");
  pf.Delete("Stage0");
  {
    int i=1;
    int skipp=0;
    char name[50];
    sprintf(name,"Stage%d",i);    
    while (skipp++<10) 
    {
      if (pf.FindEntry(name).NotNull()) 
      {
        skipp=0;
        pf.Delete(name);
      }
      i++;
      sprintf(name,"Stage%d",i);
    }
  }
  // removing texgen records from paramfile; 
  for (int i=0;i<pf.GetEntryCount();++i)
  {
    ParamEntryPtr temp= &pf.GetEntry(i);
    if (temp.NotNull()&& temp->IsClass())
    {    
      RString name= temp->GetName();
      temp = NULL;
      if (strnicmp(name,"texgen",6)==0)
      {
        pf.Delete(name);
        --i;
      }
    }
  }

    
  CString renderFlags;
  bool value;
  AddColorToPF(pf,"ambient",_ambientPanel);
  AddColorToPF(pf,"diffuse",_diffusePanel);
  AddColorToPF(pf,"forcedDiffuse",_forcedDPanel);
  AddColorToPF(pf,"emmisive",_emmissvPanel);
  AddColorToPF(pf,"specular",_speculrPanel);
  pf.Add("specularPower",_specpowPanel->_factor);
  
  ParamEntryPtr array;
  array=pf.AddArray("renderFlags");
  for (int i=0;_generalPanel->GetFlag(i,&renderFlags,&value);i++)
    if (value) array->AddValue((LPCTSTR)renderFlags);
  if (array->GetSize()==0) pf.Delete("renderFlags");

  if (_nextPassPanel && _nextPassPanel->wMatName.GetWindowTextLength())
  {
    CString nextPass;
    _nextPassPanel->wMatName.GetWindowText(nextPass);
    pf.Add("nextPass",Pathname::FullToRelativeProjectRoot(nextPass,theApp.CallGen().GetProjectRoot()));
  }

  CString pxshader=_generalPanel->GetPixelShader();
  pf.Add("PixelShaderID",(LPCTSTR)pxshader);
  CString vxshader=_generalPanel->GetVertexShader();
  pf.Add("VertexShaderID",(LPCTSTR)vxshader);

  // saving texGens
  texGenDlg.SaveTexGens(pf);
  if (_stage0Panel->vTextureName.GetLength() || _stage0Panel->vStageFilter.GetLength())
  {
    ParamClassPtr cls=pf.AddClass("Stage0");    
    if (_stage0Panel->vTextureName.GetLength())
    {
      if (_stage0Panel->vTextureName.GetLength() && _stage0Panel->vTextureName[0]=='\\') _stage0Panel->vTextureName.Delete(0,1);
      cls->Add("texture",(LPCTSTR)_stage0Panel->vTextureName);
    }
    if (_texGenCount==-1)
      {
    cls->Add("uvSource","tex");
      }
      else
      {
        cls->Add("texGen","0");
      }

    if (_stage0Panel->vStageFilter.GetLength())    
      cls->Add("Filter",(LPCTSTR)_stage0Panel->vStageFilter);
  }

  for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
  {
    DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
    if (stage)
    {
      if ((!forpreview || stage->IsStageEnabled()) && stage->GetStageNumber()!=0)
      {        
        char buff[20];
        sprintf(buff,"Stage%d",stage->GetStageNumber());
        ParamClassPtr cls=pf.AddClass(buff);    
        
        CString uvsource=stage->GetUVSource();
        cls->Add("texture",(LPCTSTR)stage->GetTextureEx(forpreview));
        if (_texGenCount==-1)
        {
        cls->Add("uvSource",(LPCTSTR)uvsource);
        }
        else
        {
          cls->Add("texGen",(LPCTSTR)uvsource);
        }
        if (_strnicmp(uvsource,"tex",3)==0)
        {
          const geMatrix4 &m=stage->GetTransform();
          ParamClassPtr mxcls=cls->AddClass("uvTransform");              
          array=mxcls->AddArray("aside");
          array->AddValue(m.a11);
          array->AddValue(m.a12);
          array->AddValue(m.a13);
          array=mxcls->AddArray("up");
          array->AddValue(m.a21);
          array->AddValue(m.a22);
          array->AddValue(m.a23);
          array=mxcls->AddArray("dir");
          array->AddValue(m.a31);
          array->AddValue(m.a32);
          array->AddValue(m.a33);
          array=mxcls->AddArray("pos");
          array->AddValue(m.a41);
          array->AddValue(m.a42);
          array->AddValue(m.a43);
        }
        if (stage->GetStageFilter().GetLength())
          cls->Add("Filter",(LPCTSTR)stage->GetStageFilter());        
      }
    }
  }
  return true;
}
/*bool DlgMaterialEditor::SaveMaterial(ostream &matinfo, bool forpreview)
{
  CSingleLock lock(&csect,TRUE);
  CString renderFlags;
  _generalPanel->StringByFlags(renderFlags,true);
  char buff[512];
  sprintf(buff,"ambient[]={%g,%g,%g,%g};\n",_ambientPanel->_r,_ambientPanel->_g,_ambientPanel->_b,_ambientPanel->_a);
  matinfo<<buff;
  sprintf(buff,"diffuse[]={%g,%g,%g,%g};\n",_diffusePanel->_r,_diffusePanel->_g,_diffusePanel->_b,_diffusePanel->_a);
  matinfo<<buff;
  sprintf(buff,"forcedDiffuse[]={%g,%g,%g,%g};\n",_forcedDPanel->_r,_forcedDPanel->_g,_forcedDPanel->_b,_forcedDPanel->_a);
  matinfo<<buff;
  sprintf(buff,"emmisive[]={%g,%g,%g,%g};\n",_emmissvPanel->_r,_emmissvPanel->_g,_emmissvPanel->_b,_emmissvPanel->_a);
  matinfo<<buff;
  sprintf(buff,"specular[]={%g,%g,%g,%g};\n",_speculrPanel->_r,_speculrPanel->_g,_speculrPanel->_b,_speculrPanel->_a);
  matinfo<<buff;
  sprintf(buff,"\nspecularPower=%g;\n\n",_specpowPanel->_factor);
  matinfo<<buff;
  if (renderFlags.GetLength()>0)  
    matinfo<<"renderFlags[]={"<<(LPCTSTR)renderFlags<<"};\n";  
  if (_nextPassPanel && _nextPassPanel->wMatName.GetWindowTextLength())
  {
    CString nextPass;
    _nextPassPanel->wMatName.GetWindowText(nextPass);
    matinfo<<"nextPass=\""<<Pathname::FullToRelativeProjectRoot(nextPass,theApp._app->GetProjectRoot())<<"\";\n";
  }
  matinfo<<"\n";
  CString pxshader=_generalPanel->GetPixelShader();
  if (pxshader.GetLength()) matinfo<<"PixelShaderID=\""<<(LPCTSTR)pxshader<<"\";\n";
  CString vxshader=_generalPanel->GetVertexShader();
  if (vxshader.GetLength()) matinfo<<"VertexShaderID=\""<<(LPCTSTR)vxshader<<"\";\n\n";
  if (_stage0Panel->vTextureName.GetLength() || _stage0Panel->vStageFilter.GetLength())
  {
    matinfo<<"class Stage0\n{\n";
    if (_stage0Panel->vTextureName.GetLength())
    {
    if (_stage0Panel->vTextureName.GetLength() && _stage0Panel->vTextureName[0]=='\\') _stage0Panel->vTextureName.Delete(0,1);
    matinfo<<"\ttexture=\""<<(LPCTSTR)_stage0Panel->vTextureName<<"\";\n";
    }
    matinfo<<"\tuvSource=\"tex\";\n";
    if (_stage0Panel->vStageFilter.GetLength())    
      matinfo<<"\tFilter=\""<<(LPCTSTR)_stage0Panel->vStageFilter<<"\";\n";
    matinfo<<"};\n";   
  }
  for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
  {
    DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
    if (stage)
    {
      if (!forpreview || stage->IsStageEnabled() && stage->GetStageNumber()!=0)
      {        
        CString uvsource=stage->GetUVSource();
        matinfo<<"class Stage"<<stage->GetStageNumber()<<"\n{\n";
        matinfo<<"\ttexture=\""<<(LPCTSTR)stage->GetTextureEx(forpreview)<<"\";\n";
        matinfo<<"\tuvSource=\""<<(LPCTSTR)uvsource<<"\";\n";
        if (stricmp(uvsource,"tex")==0)
        {
          const geMatrix4 &m=stage->GetTransform();
          sprintf(buff,"\tclass uvTransform\n"
            "\t{\n"
            "\t\taside[]={%g,%g,%g};\n"
            "\t\tup[]={%g,%g,%g};\n"
            "\t\tdir[]={%g,%g,%g};\n"
            "\t\tpos[]={0,0,0};\n"
            "\t};\n",
            m.a11,m.a12,m.a13,
            m.a21,m.a22,m.a23,
            m.a41,m.a42,m.a43);
          matinfo<<buff;
        }
        if (stage->GetStageFilter().GetLength())
          matinfo<<"\tFilter=\""<<(LPCTSTR)stage->GetStageFilter()<<"\";\n";
        matinfo<<"};\n";
      }
    }
  }
  return true;
}
*/

void DlgMaterialEditor::AfterPrepareModelForViewer(const ObjectData &src, ObjectData &trg)
{
  /*  CSingleLock lock(&csect,TRUE);
  const char *matname=_rvmatName;  
  const char *root=theApp.CallGen().GetProjectRoot();
  int rootsz=strlen(root);
  if (strlen(matname)>rootsz) matname+=rootsz;
  for (int i=0;i<trg.NFaces();i++)
  {
  FaceT fc(trg,i);
  if (stricmp(fc.GetMaterial(),matname)==0) fc.SetMaterial(_rvmatTempName.GetFullPath()+rootsz);
  }*/
  UpdateViewer(true);
}
void DlgMaterialEditor::OnTimer(UINT nIDEvent)
{
  if (nIDEvent==UPDATETIMEREVENT)
  {
    KillTimer(nIDEvent);
    _waitUpdate=false;
    UpdateViewer(false);
  }
  if (nIDEvent==ENABLEDIRTY)
  {
    _enabledirty=true;
    KillTimer(nIDEvent);
  }
  if (nIDEvent==SETNOTOP)
  {
    if (IsWindowEnabled())  SetWindowPos(&wndNoTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
    KillTimer(nIDEvent);
  }
  if (nIDEvent==UNDOTIMER)
  {
    if (_canSaveUndo && !IsMyWindow(*this,*GetCapture()))
    {
      SaveUndo();
      KillTimer(nIDEvent);
      _needSaveUndo=false;
    }
    else
      _canSaveUndo=true;
  }
  CDialog::OnTimer(nIDEvent);
}

void DlgMaterialEditor::UpdateViewer(bool delayed)
{
  if (delayed) 
  {
    if (!_waitUpdate) 
    {
      _waitUpdate=true;
      SetTimer(UPDATETIMEREVENT,100,NULL);
    }
  }
  else 
  {
    COLORREF dif,specul,forcedif,sumdif;
    specul=_speculrPanel->GetColor();
    dif=_diffusePanel->GetColor();
    forcedif=_forcedDPanel->GetColor();
    sumdif=RGB((GetRValue(dif)*30+GetRValue(forcedif)*50)/100,
      (GetGValue(dif)*30+GetGValue(forcedif)*50)/100,
      (GetBValue(dif)*30+GetBValue(forcedif)*50)/100);
    _specpowPanel->SetDrawColors(sumdif,specul);
    if (_rvmatName.IsNull()) return;
    if (theApp.CallGen().IsViewerActive())
    { 
      IExternalViewer *viewer=theApp.CallGen().GetViewerInterface();
      SaveMaterial(_pfCopy,true);
      QOStrStream tempbuff;
      _pfCopy.Save(tempbuff,0);
      const char *prefix=theApp.CallGen().GetProjectRoot();
      const char *matName=_rvmatName.GetFullPath();  
      int offset=0;
      while (prefix[offset] && toupper(prefix[offset])==toupper(matName[offset])) offset++;
      if (prefix[offset]) offset=0;
      viewer->SetMaterial(matName+offset,(void *)tempbuff.str(),tempbuff.pcount());
      viewer->RedrawViewer();
    }
  }

  DWORD thread=GetWindowThreadProcessId(*this,NULL);
  if (_dirty!=_enabledirty && GetCurrentThreadId()==thread)
  {
    if (_enabledirty && _rvmatName.GetFullPath()!=NULL && _access(_rvmatName,02)!=0)
    {
      FILETIME ft1,ft2;
      _enabledirty=false;
      GetFileTime(_rvmatName,ft1);
      theApp.CallGen().TestFileRO(_rvmatName,ROCHF_DisableSaveAs,*this);
      GetFileTime(_rvmatName,ft2);
      if (memcmp(&ft1,&ft2,sizeof(ft1))) LoadMaterial(_rvmatName);
      _enabledirty=true;
    }
    _dirty=_enabledirty;
    UpdateTitle();
    UpdateSSStatus();
  }
}

bool DlgMaterialEditor::AskExitMAT()
{
  if (IsIconic()) OpenIcon();
  if (IsWindowEnabled()==false)
  {
    BringWindowToTop();
    SetForegroundWindow();
    return false;
  }
  return CloseDocument();
}

LRESULT DlgMaterialEditor::OnMsgGetMaterialName(WPARAM wParam,LPARAM lParam)
{
  return (LRESULT)&(_rvmatName);
}

LRESULT DlgMaterialEditor::OnMsgStageChanged(WPARAM wParam, LPARAM lParam)
{
  UpdateViewer(true);  
  _needSaveUndo=true;
  _canSaveUndo=false;
  SetTimer(UNDOTIMER,500,NULL);
  return 0;
}


void DlgMaterialEditor::OnInitMenuPopup(CMenu* pMenu, UINT nIndex, BOOL bSysMenu)
{

  if (bSysMenu)
    return;     // don't support system menu

  ASSERT(pMenu != NULL);

  // check the enabled state of various menu items

  CCmdUI state;
  state.m_pMenu = pMenu;
  ASSERT(state.m_pOther == NULL);
  ASSERT(state.m_pParentMenu == NULL);

  // determine if menu is popup in top-level menu and set m_pOther to
  //  it if so (m_pParentMenu == NULL indicates that it is secondary popup)
  HMENU hParentMenu;
  if (AfxGetThreadState()->m_hTrackingMenu == pMenu->m_hMenu)
    state.m_pParentMenu = pMenu;    // parent == child for tracking popup
  else if ((hParentMenu = ::GetMenu(m_hWnd)) != NULL)
  {
    CWnd* pParent = GetTopLevelParent();
    // child windows don't have menus -- need to go to the top!
    if (pParent != NULL &&
      (hParentMenu = ::GetMenu(pParent->m_hWnd)) != NULL)
    {
      int nIndexMax = ::GetMenuItemCount(hParentMenu);
      for (int nIndex = 0; nIndex < nIndexMax; nIndex++)
      {
        if (::GetSubMenu(hParentMenu, nIndex) == pMenu->m_hMenu)
        {
          // when popup is found, m_pParentMenu is containing menu
          state.m_pParentMenu = CMenu::FromHandle(hParentMenu);
          break;
        }
      }
    }
  }

  state.m_nIndexMax = pMenu->GetMenuItemCount();
  for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax;
    state.m_nIndex++)
  {
    state.m_nID = pMenu->GetMenuItemID(state.m_nIndex);
    if (state.m_nID == 0)
      continue; // menu separator or invalid cmd - ignore it

    ASSERT(state.m_pOther == NULL);
    ASSERT(state.m_pMenu != NULL);
    if (state.m_nID == (UINT)-1)
    {
      // possibly a popup menu, route to first item of that popup
      state.m_pSubMenu = pMenu->GetSubMenu(state.m_nIndex);
      if (state.m_pSubMenu == NULL ||
        (state.m_nID = state.m_pSubMenu->GetMenuItemID(0)) == 0 ||
        state.m_nID == (UINT)-1)
      {
        continue;       // first item of popup can't be routed to
      }
      state.DoUpdate(this, FALSE);    // popups are never auto disabled
    }
    else
    {
      // normal menu item
      // Auto enable/disable if frame window has 'm_bAutoMenuEnable'
      //    set and command is _not_ a system command.
      state.m_pSubMenu = NULL;
      state.DoUpdate(this, true);
    }

    // adjust for menu deletions and additions
    UINT nCount = pMenu->GetMenuItemCount();
    if (nCount < state.m_nIndexMax)
    {
      state.m_nIndex -= (state.m_nIndexMax - nCount);
      while (state.m_nIndex < nCount &&
        pMenu->GetMenuItemID(state.m_nIndex) == state.m_nID)
      {
        state.m_nIndex++;
      }
    }
    state.m_nIndexMax = nCount;
  }
}

void DlgMaterialEditor::OnFileExit()
{
  theApp.OnExitMatMenu();
}

void DlgMaterialEditor::OnFileNew()
{
  theApp.CreateMaterial(_rvmatName.GetDirectoryWithDrive());
}
bool DlgMaterialEditor::CloseDocument()
{
  UpdateSSStatus();
  if (_dirty)
  {
    CString msg;
    msg.Format(IDS_MATNOTSAVED,_rvmatName.GetFilename());
    int cmd=theApp.GetMyTopLevelWindow(this)->MessageBox(msg,StrRes[IDS_MATNAME],MB_YESNOCANCEL|MB_DEFBUTTON3|MB_ICONQUESTION);
    switch (cmd)
    {
    case IDYES:OnFileSave();if (_dirty) return false;
    case IDNO:break;
    case IDCANCEL: return false;
    }
  }
  if (_scc_status==1)
  {
    CString msg;
    if (_dirty)
    {
      DlgAskCheckIn dlg;
      dlg.vName=_rvmatName;
      int cmd=dlg.DoModal();
      switch (cmd)
      {
      case IDOK: break;
      case IDC_CHECKIN: 
        {
          DlgCheckInComments dlg(this);
          dlg.vFilename=_rvmatName.GetFullPath();
          if (dlg.DoModal()==IDOK)
            SccError(theApp.CallGen().GetSccInterface()->CheckIn(_rvmatName,dlg.vComments));break;
        }
        break;
      case IDC_UNDOCHECKOUT: SccError(theApp.CallGen().GetSccInterface()->UndoCheckOut(_rvmatName));break;
      case IDNO: break;
      case IDCANCEL: return false;
      }
    }
    else
    {
      DlgCheckInComments dlg(this);
      dlg.vFilename=_rvmatName.GetFullPath();
      if (dlg.DoModal()==IDOK)      
        SccError(theApp.CallGen().GetSccInterface()->CheckIn(_rvmatName.GetFilename(),dlg.vComments));      
    }
  }
  return true;
}
 
bool DlgMaterialEditor::NewDocument(void)
{
  _ambientPanel->SetColor(RGB(255,255,255));
  _diffusePanel->SetColor(RGB(255,255,255));
  _forcedDPanel->SetColor(RGB(0,0,0));
  _emmissvPanel->SetColor(RGB(0,0,0));
  _speculrPanel->SetColor(RGB(0,0,0));
  _specpowPanel->_factor=1.0f;
  _specpowPanel->LoadValues();
  _generalPanel->SetMaterialType("");
  _rvmatName.SetNull();
  UpdateTitle();
  _generalPanel->SetMaterialName("");
  this->DeleteAllStages();
  RecreateOtherControls();  
  RecalcLayout();
  RecalcScrollbar();
  _scc_status=-1;
  _needSaveUndo=false;
  _canSaveUndo=false;
  _enableSaveUndo=true;
  return true;
}

void DlgMaterialEditor::UpdateTitle(void)
{
  if (_rvmatName.IsNull())
    SetWindowText(StrRes[IDS_UNNAMEDMATERIAL]);
  else
    SetWindowText(_rvmatName.GetTitle());
  GetParent()->SendMessage(MATMSG_TITLEUPDATED,_dirty,(LPARAM)this);

}

void DlgMaterialEditor::OnViewNewwindow()
{
  if (theApp.CreateNewWindow()==false)
    theApp.GetMyTopLevelWindow(this)->MessageBox(StrRes[IDS_TOMANYWINDOWS],StrRes[IDS_MATNAME],MB_ICONINFORMATION);
}

void DlgMaterialEditor::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
  CDialog::OnActivate(nState, pWndOther, bMinimized);
  if (nState==WA_ACTIVE || nState==WA_CLICKACTIVE)
    theApp.SetActiveMatWindow(this);
  CheckAndUpdateChanges();
}

bool DlgMaterialEditor::SaveMaterial(const Pathname & filename)
{  
  SaveMaterial(_pfCopy,false);
  if (_pfCopy.Save(filename)!=LSOK) return false;
  GetFileTime(filename,_edit_file_time);
  return true;
}

void DlgMaterialEditor::OnFileSave()
{
  if (_rvmatName.IsNull()) {OnFileSaveas();return;}
  int res=theApp.CallGen().TestFileRO(_rvmatName,0,*this);
  if (res==ROCHK_FileSaveAs) {OnFileSaveas();return;}
  if (res==ROCHK_FileRO) return;
  if (SaveMaterial(_rvmatName)==false)
    theApp.GetMyTopLevelWindow(this)->MessageBox(StrRes[IDS_SAVEFAILED],StrRes[IDS_MATNAME],MB_ICONEXCLAMATION);
  else
  {
    _dirty=false;
    UpdateTitle();
    _generalPanel->SetMaterialName(_rvmatName);
    UpdateSSStatus();
  }
}

void DlgMaterialEditor::OnFileSaveas()
{
  int res;
  do
  {
    CFileDialog fdlg(FALSE,StrRes[IDS_RVMATDEFEXT],_rvmatName.IsNull()?"":_rvmatName.GetFullPath(),
      OFN_PATHMUSTEXIST|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,StrRes[IDS_RVMATSAVEFILTERS],this);
    res=fdlg.DoModal();
    if (res==IDOK) 
    {
      CString name=fdlg.GetPathName();
      res=theApp.CallGen().TestFileRO(name,0,*this);
      if (res==ROCHK_FileOK)
      {
        _rvmatName=name;
        SaveMaterial(_rvmatName);
        UpdateTitle();
        _generalPanel->SetMaterialName(_rvmatName);
        UpdateSSStatus();
      }
    }
    else
      res=ROCHK_FileRO;
  }
  while (res==ROCHK_FileSaveAs);
}

void DlgMaterialEditor::OnFileOpen()
{
  CFileDialog fdlg(TRUE,StrRes[IDS_RVMATDEFEXT],_rvmatName.IsNull()?"":_rvmatName.GetFullPath(),
    OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,StrRes[IDS_RVMATLOADFILTERS],this);
  fdlg.m_ofn.lpstrInitialDir=theApp.CallGen().GetProjectRoot();
  int res=fdlg.DoModal();
  if (res==IDOK)
  {
    theApp.EditMaterial(fdlg.GetPathName());
  }
}

void DlgMaterialEditor::OnFileOptions()
{
  theApp.config.DoModal();
}

void DlgMaterialEditor::OnFileMaterialtypemanager()
{
  DlgTypeManager dlg;
  dlg.vPixelShaderName=_generalPanel->GetPixelShader();
  dlg.vVertexShaderName=_generalPanel->GetVertexShader();  
  dlg.vDefaultFlags="";
  _generalPanel->StringByFlags(dlg.vPredefinedFlags,true);
  dlg.vOnlyFlags=false;
  dlg.vLockFlags=true;
opak:
  if (dlg.DoModal()==IDOK)
  {
    if (!dlg.vFullPath.IsPathValid())
    {
      theApp.GetMyTopLevelWindow(this)->MessageBox(StrRes[IDS_INVALID_FILENAME],StrRes[IDS_MATNAME],MB_ICONEXCLAMATION);
      goto opak;
    }
    SMatRestricts restricts;
    for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
    {
      DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
      if (stage)
      {
        int stageNum=stage->GetStageNumber();
        CString source;
        SStageRestricts &ss=restricts.stageRestricts.Append();
        source=stage->GetUVSource();
        ss.uvSource=(LPCTSTR)source;
        source=stage->GetStageName();
        ss.stageName=(LPCTSTR)source;
        source=stage->GetTexture();
        ss.irradiance=source.GetLength()>0 && source[0]=='#' && source.Find("irradiance")!=-1;
        if (dlg.vLockAll)
          ss.locked=true;
        else
        {
          int val;
          istrstream intst(const_cast<char *>((LPCTSTR)dlg.vLockedStages));
          ss.locked=false;
          while (!intst && !ss.locked)
          {
            intst>>val;
            if (val==stageNum) ss.locked=true;
          }
        }
        ss.stagenum=stageNum;        
      }
    }
    restricts.defaultFlags=(LPCTSTR)dlg.vDefaultFlags;
    restricts.predefinedFlags=(LPCTSTR)dlg.vPredefinedFlags;
    restricts.lockFlags=dlg.vLockFlags;
    restricts.onlyFlags=dlg.vOnlyFlags;
    restricts.lockAddStage=dlg.vLockAddButt!=FALSE;
    restricts.pixerShader=(LPCTSTR)dlg.vPixelShaderName;
    restricts.vertexShader=(LPCTSTR)dlg.vVertexShaderName;
    restricts.typeName=(LPCTSTR)dlg.vTemplateName;
    int res=theApp.CallGen().TestFileRO(dlg.vFullPath,0,*this);
    if (res==ROCHK_FileSaveAs) goto opak;
    if (res!=ROCHK_FileOK) return;
    char *dirWLBS=(char *)alloca(strlen(dlg.vFullPath.GetDirectoryWithDrive()));
    dlg.vFullPath.GetDirectoryWithDriveWLBS(dirWLBS,0x7FFFFFFF);
    FILETIME ft;    
    if (GetFileTime(dirWLBS,ft)==false)
      CreateDirectory(dirWLBS,NULL);
    if (GetFileTime(dirWLBS,ft)==false)
    {
      CString msg;
      msg.Format(IDS_UNABLETOCREATETYPEFOLDER,dirWLBS);
      theApp.GetMyTopLevelWindow(this)->MessageBox(msg,StrRes[IDS_MATNAME],MB_ICONSTOP);
      return;
    }
    if (restricts.SaveRestricts(dlg.vFullPath))
    {
      SccFunctions *scc=theApp.CallGen().GetSccInterface();
      if (scc && scc->UnderSSControlDir(dlg.vFullPath.GetDirectoryWithDrive()))
      {
        CString report;
        report.Format(IDS_TYPEREPORT,dlg.vPixelShaderName,dlg.vVertexShaderName,dlg.vLockAll,
          dlg.vLockAddButt,restricts.stageRestricts.Size(),dlg.vLockedStages);
        if (!scc->UnderSSControlNotDeleted(dlg.vFullPath)) scc->Add(dlg.vFullPath,report);
        scc->CheckIn(dlg.vFullPath,report);
      }
      matRestricts.Clear();
      InitByRestricts();
    }
    else
      theApp.GetMyTopLevelWindow(this)->MessageBox(StrRes[IDS_MATERIALTYPESAVEFAILED],StrRes[IDS_MATNAME],MB_ICONSTOP);
  }
}

void DlgMaterialEditor::InitByRestricts()
{
  _generalPanel->ResetAll();
  InitByRestriction genPanelInit;
  genPanelInit.dialog=_generalPanel;
  genPanelInit.matRestricts=&matRestricts;
  genPanelInit.ProcessFolders(theApp.config.vTemplatePath,"*"TYPEEXTENSION,false);
  _generalPanel->InitFreeType();
}
void DlgMaterialEditor::OnSoucecontrolReloadtemplatestypes()
{
  theApp.ReloadTemplatesFromSS();
   theApp.GetMyTopLevelWindow(this)->MessageBox(StrRes[IDS_TYPESRELOADRESTART],StrRes[IDS_MATNAME],MB_ICONINFORMATION);
}



LRESULT DlgMaterialEditor::OnRestrictionsChanges(WPARAM wParam, LPARAM lParam)
{
  if (wParam==0 && lParam==0)
  {
    for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
    {
      DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
      if (stage)
      {
        stage->LockStageSetup(false,false);   
        stage->LockDelButton(false);
      }
    }
    _generalPanel->LockAddButton(false);    
    _generalPanel->LockFlags(false);   
  }
  else
  {
    int sel=wParam;  
    CComboBox &wType=*(CComboBox *)lParam;
    CString name;
    wType.GetLBText(sel,name);
    int i;
    for (i=matRestricts.Size()-1;i>=0;i--)
      if (matRestricts[i].typeName==(LPCTSTR)name) break;
    if (i<0) return 0;
    SMatRestricts &restrictions=matRestricts[i];
    _generalPanel->SetPixelShader(restrictions.pixerShader);
    _generalPanel->SetVertexShader(restrictions.vertexShader);
    _generalPanel->LockAddButton(restrictions.lockAddStage);
    _generalPanel->LockFlags(restrictions.lockFlags);
    if (restrictions.onlyFlags) _generalPanel->ClearFlags();
    _generalPanel->FlagsByString(restrictions.defaultFlags,false);
    _generalPanel->FlagsByString(restrictions.predefinedFlags,true);
    _ambientPanel->SetColor(restrictions.ambient);
    _diffusePanel->SetColor(restrictions.diffuse);
    _forcedDPanel->SetColor(restrictions.forcedDiffuse);
    _emmissvPanel->SetColor(restrictions.emmisive);
    _speculrPanel->SetColor(restrictions.specular);
    _texGenCount=restrictions.texGenCount;

    texGenDlg.DeleteAllTexGens(); 
    for (int i=0;i<_texGenCount;++i)
      texGenDlg.AddTexGen();
       

    if (_specpowPanel->_factor>0)
      _specpowPanel->_factor = restrictions.specularPrower;
    if (_stage0Panel->vStageFilter==_T(""))
      _stage0Panel->SetTexture(restrictions.defaultTexture);    

    BTree<RStringI> scavenger;
    DeleteAllStages(&scavenger);
    for (int i=0;i<restrictions.stageRestricts.Size();i++)
    {
      int pos=AddStage(restrictions.stageRestricts[i].stagenum,_texGenCount);
      DlgStageCfg *stage=static_cast<DlgStageCfg *>(_panels[pos]);
      stage->RecorverFiles(scavenger);
      stage->SetTexGenCount(_texGenCount);
      stage->SetUVSource(restrictions.stageRestricts[i].uvSource);      
      stage->SetStageName(restrictions.stageRestricts[i].stageName);
      stage->LockStageSetup(restrictions.stageRestricts[i].locked,restrictions.stageRestricts[i].irradiance);    
      stage->LockDelButton(restrictions.lockAddStage);
      stage->LockUVSource(restrictions.stageRestricts[i].lockedTexGen && _texGenCount!=-1);
      if (stage->GetTexture()==_T("") && (!restrictions.stageRestricts[i].defaultValue.IsEmpty()))
        stage->SetTexture(restrictions.stageRestricts[i].defaultValue);
    }
    RecreateOtherControls();
    RecalcLayout();
    RecalcScrollbar();
  }
  return 0;
}

int DlgMaterialEditor::DetectMaterialType()
{
  RString pixelShader=_generalPanel->GetPixelShader();
  RString vertexShader=_generalPanel->GetVertexShader();  
  int spos=0;
  int stages=0;
  int i,j;
  for (i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
  {
    DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
    if (stage) stages++;
  }
  for (i=0;i<matRestricts.Size();i++)
  {
    SMatRestricts &restricts=matRestricts[i];
    if (restricts.vertexShader==vertexShader && restricts.pixerShader==pixelShader && restricts.stageRestricts.Size()==stages)
    {
      for (j=0;j<MAT_MAXIMUMWINDOWS && _panels[j];j++)
      {
        DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[j]);
        if (stage)
        {
          if (stage->GetStageNumber()!=restricts.stageRestricts[spos].stagenum) goto skip;
          spos++;
        }
      }
      return i;
    }
skip:;
  }
  return -1;
}

void DlgMaterialEditor::SetMaterialTypeFromDialog()
{
  int type=DetectMaterialType();
  if (type!=-1) 
  {
    _generalPanel->SetMaterialType(matRestricts[type].typeName);
    SMatRestricts &restricts=matRestricts[type];
    _generalPanel->LockAddButton(restricts.lockAddStage);    
    int spos=0;
    for (int j=0;j<MAT_MAXIMUMWINDOWS && _panels[j];j++)
    {
      DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[j]);
      if (stage && stage->GetStageNumber()==restricts.stageRestricts[spos].stagenum)
      {
        stage->LockStageSetup(restricts.stageRestricts[spos].locked,restricts.stageRestricts[spos].irradiance);        
        stage->LockDelButton(restricts.lockAddStage);   
        stage->SetStageName(restricts.stageRestricts[spos].stageName);
        spos++;
      }
    }
  }
  else  _generalPanel->SetMaterialType(NULL);
}

void DlgMaterialEditor::OnSave()
{
  OnFileSave();
}

void DlgMaterialEditor::OnFileSaveastemplate()
{
  DlgTemplateManager dlg;
  dlg.vTemplateName=_rvmatName.GetTitle();
  int result;
  while ((result=dlg.DoModal())==IDOK && theApp.CallGen().TestFileRO(dlg.vOutPath)!=ROCHK_FileOK);

  if (result==IDOK)
  {   
    SccFunctions *scc=theApp.CallGen().GetSccInterface();
    SaveMaterial(dlg.vOutPath);
    if (!scc->UnderSSControlNotDeleted(dlg.vOutPath))  scc->Add(dlg.vOutPath);
    scc->CheckIn(dlg.vOutPath);
  }
}


void DlgMaterialEditor::OnFileSaveandcheckin()
{
  SccFunctions *scc=theApp.CallGen().GetSccInterface();
  OnFileSave();
  if (!_dirty && scc)
  {
    DlgCheckInComments dlg(this);
    dlg.vFilename=_rvmatName.GetFullPath();
    if (dlg.DoModal()==IDOK)
    {
      SCCRTN rtn;
      if (!scc->UnderSSControlNotDeleted(_rvmatName))
        rtn=scc->Add(_rvmatName,dlg.vComments);
      else rtn=0;
      if (rtn==0)
        rtn=scc->CheckIn(_rvmatName,dlg.vComments);
      SccError(rtn);
      UpdateSSStatus();
    }
  }
}


void DlgMaterialEditor::UpdateSSStatus()
{
  SccFunctions *scc=theApp.CallGen().GetSccInterface();
  if (scc==NULL || _rvmatName.IsNull()) _scc_status=-1;
  else if (scc->UnderSSControlNotDeleted(_rvmatName))
  {
    if (scc->CheckedOut(_rvmatName)==true) _scc_status=1;
    else _scc_status=0;
  }
  else _scc_status=-1;
}

void DlgMaterialEditor::OnUpdateFileSaveandcheckin(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(theApp.CallGen().GetSccInterface()!=NULL);
}

void DlgMaterialEditor::OnFileOpenfromsourcecontrol()
{
  if (CloseDocument()==false) return;
  NewDocument();
  SccFunctions *scc=theApp.CallGen().GetSccInterface();
  SccFunctions *browse=scc->Clone();
  if (browse->ChooseProjectEx(browse->GetProjName(),browse->GetLocalPath(),browse->GetServerName(),browse->GetUserName())==0)
  {
    browse->GetLatestVersionDir(browse->GetLocalPath(),false);
    CFileDialog fdlg(TRUE,StrRes[IDS_RVMATDEFEXT],NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,StrRes[IDS_RVMATLOADFILTERS],
      this);
    fdlg.m_ofn.lpstrInitialDir=browse->GetLocalPath();
    fdlg.m_ofn.lpstrTitle=StrRes[IDS_OPENFROMSSTITLE];
    if (fdlg.DoModal()==IDOK)
    {
      if (LoadMaterial(fdlg.GetPathName())==0)
      {
        UpdateTitle();
        _generalPanel->SetMaterialName(_rvmatName);
      }
      else
        theApp.GetMyTopLevelWindow(this)->MessageBox(StrRes[IDS_LOADFAILED],StrRes[IDS_MATNAME],MB_ICONEXCLAMATION);
    }
  }
  delete browse;
}

void DlgMaterialEditor::OnUpdateFileOpenfromsourcecontrol(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(theApp.CallGen().GetSccInterface()!=NULL);
}

void DlgMaterialEditor::OnSoucecontrolCheckout()
{
  FILETIME ftime1,ftime2;
  GetFileTime(_rvmatName,ftime1);
  SCCRTN rtn=theApp.CallGen().GetSccInterface()->CheckOut(_rvmatName);
  GetFileTime(_rvmatName,ftime2);
  if (memcmp(&ftime1,&ftime2,sizeof(ftime2))!=0) 
  {
    LoadMaterial(_rvmatName);
  }
  SccError(rtn);
  UpdateSSStatus();
}

void DlgMaterialEditor::OnUpdateSoucecontrolCheckout(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_scc_status==0);
}

void DlgMaterialEditor::OnSoucecontrolGetlatestversion()
{
  theApp.CallGen().GetSccInterface()->GetLatestVersion(this->_rvmatName);
  for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
  {
    DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
    if (stage) 
    {
      CString tex=stage->GetTextureEx(false);
      if (tex.GetLength() && tex[0]!='#')
        theApp.CallGen().GetSccInterface()->GetLatestVersion(tex);
    }
  }
}

void DlgMaterialEditor::OnUpdateSoucecontrolGetlatestversion(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(theApp.CallGen().GetSccInterface()!=NULL);
}

void DlgMaterialEditor::OnSoucecontrolHistory()
{
  SCCRTN rtn=theApp.CallGen().GetSccInterface()->History(_rvmatName);
  if (rtn==5) LoadMaterial(_rvmatName);
  else SccError(rtn);
}

void DlgMaterialEditor::OnUpdateSoucecontrolHistory(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_scc_status!=-1);
}

void DlgMaterialEditor::OnSoucecontrolUndocheckout()
{
  FILETIME ftime1,ftime2;
  GetFileTime(_rvmatName,ftime1);
  SCCRTN rtn=theApp.CallGen().GetSccInterface()->UndoCheckOut(_rvmatName);
  GetFileTime(_rvmatName,ftime2);
  if (memcmp(&ftime1,&ftime2,sizeof(ftime2))!=0) 
  {
    LoadMaterial(_rvmatName);
  }
  SccError(rtn);
  UpdateSSStatus();
}


void DlgMaterialEditor::OnUpdateSoucecontrolUndocheckout(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_scc_status==1);
}


void DlgMaterialEditor::SetMaterialName(const char *name)
{
  _rvmatName=name;
  UpdateTitle();
  UpdateSSStatus();
  _generalPanel->SetMaterialName(name);
}

LRESULT DlgMaterialEditor::OnDelStageCommand(WPARAM wParam,LPARAM lParam)
{
  CDialog *dlg=(CDialog *)lParam;
  for (int i=0;i<MAT_MAXIMUMWINDOWS;i++)
    if (dlg==_panels[i] && IsDlgButtonChecked(i+DLGIDOFFSET))
      OnShowHidePanel(i+DLGIDOFFSET);
  DeletePanel(dlg);
  RecreateOtherControls();
  RecalcLayout();
  RecalcScrollbar();
  return 0;
}
void DlgMaterialEditor::OnInsertStage()
{
  _generalPanel->PostMessage(WM_COMMAND,IDC_ADDSTAGE,0);
}

void DlgMaterialEditor::OnPrimitiveCreate()
{
  theApp.ChangePrimitive();
  _generalPanel->OnBnClickedShowprimitive();
}

void DlgMaterialEditor::OnPrimitiveLoadfromfile()
{
  theApp.ChangePrimitiveFromFile();
  _generalPanel->OnBnClickedShowprimitive();
}

void DlgMaterialEditor::OnPrimitiveSelecttexture()
{
  theApp.SetPrimTexture();
  _generalPanel->OnBnClickedShowprimitive();
}

void DlgMaterialEditor::OnViewShowprimitive()
{
  _generalPanel->OnBnClickedShowprimitive();
}

void DlgMaterialEditor::OnViewShowmodel()
{
  _generalPanel->OnBnClickedShodmodel();
}

static BOOL CALLBACK SetThreadHwnd(HWND hwnd,LPARAM lParam)
{
  HWND *sethWnd=(HWND *)lParam;
  *sethWnd=hwnd;
  return FALSE;
}

static void EditInTextEditor(CWnd *wnd,const char *filename)
{
  CString pathtext;
  STARTUPINFO stinfo;
  memset(&stinfo,0,sizeof(stinfo));
  stinfo.cb=sizeof(stinfo);
  stinfo.dwFlags=0;
  PROCESS_INFORMATION pinfo;
  pathtext.Format("%s %s",theApp.config.vTextEditor.GetLength()==0?"notepad.exe":theApp.config.vTextEditor.GetString(), filename);
  BOOL ok=CreateProcess(NULL,pathtext.LockBuffer(),NULL,NULL,FALSE,0,NULL,NULL,&stinfo,&pinfo);
  pathtext.UnlockBuffer();
  if (!ok)
    theApp.GetMyTopLevelWindow(wnd)->MessageBox(StrRes[IDS_UNABLETOSTARTNOTEPAD],StrRes[IDS_MATNAME],MB_ICONEXCLAMATION);
  else
  {
    CloseHandle(pinfo.hThread);
    CloseHandle(pinfo.hProcess);
  }

}

void DlgMaterialEditor::OnEditTextedito()
{
  if (_dirty)
  {
    int res=theApp.GetMyTopLevelWindow(this)->MessageBox(StrRes[IDS_SAVEBEFORETEXTEDIT],StrRes[IDS_MATNAME],MB_YESNOCANCEL|MB_ICONQUESTION);
    if (res==IDYES) OnFileSave();
    else if (res==IDCANCEL) return;
  }
  EditInTextEditor(this,_rvmatName.GetFullPath());
}


BOOL DlgMaterialEditor::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
  CRect rc;
  GetClientRect(&rc);
  if (_formLength>rc.bottom && nHitTest==HTCLIENT && !IsDialogAtMouse(this,CPoint(GetMessagePos()))) 
  {    
    SetCursor(theApp.LoadCursor(IDC_SCROLLER));
    return TRUE;
  }
  else return CDialog::OnSetCursor(pWnd,nHitTest,message);  
}

void DlgMaterialEditor::BeginDragScroll(const CPoint & pt)
{
  _ypos_scroll=GetScrollPos(SB_VERT)+pt.y;
}

void DlgMaterialEditor::DragScroll(const CPoint & pt)
{
  int newpos=_ypos_scroll-pt.y;
  SetScrollPos(SB_VERT,newpos,TRUE);
  RecalcLayout();
}


void DlgMaterialEditor::OnMouseMove(UINT nFlags, CPoint point)
{
  if (GetCapture()==this)
  {
    CPoint ms=GetMessagePos();
    DragScroll(ms);
  }
  CDialog::OnMouseMove(nFlags, point);
}

void DlgMaterialEditor::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (GetCapture()==this) ReleaseCapture();
  CDialog::OnLButtonUp(nFlags, point);
}

void DlgMaterialEditor::OnRButtonUp(UINT nFlags, CPoint point)
{
  if (GetCapture()==this) ReleaseCapture();
  CDialog::OnRButtonUp(nFlags, point);
}

BOOL DlgMaterialEditor::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
  CPoint ms=GetMessagePos();
  if (!IsDialogAtMouse(this,ms) && GetCapture()==NULL)

  {
    int pos1=GetScrollPos(SB_VERT);
    if (zDelta<0) SetScrollPos(SB_VERT,pos1+50,TRUE);
    else SetScrollPos(SB_VERT,pos1-50,TRUE);
    RecalcLayout();
    int pos2=GetScrollPos(SB_VERT);
    GetCursorPos(&ms);
    if (pos1!=pos2)
    {
      ms.y=ms.y+pos1-pos2;
      CRect rc;
      GetWindowRect(&rc);
      if (rc.PtInRect(ms))  SetCursorPos(ms.x,ms.y);
    }
  }
  // TODO: Add your message handler code here and/or call default

  return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

void DlgMaterialEditor::OnPrimitiveSetcolor()
{
  CColorDialog dlg(0,CC_ANYCOLOR|CC_FULLOPEN|CC_RGBINIT);
  if (dlg.DoModal()==IDOK)
  {
    COLORREF col=dlg.GetColor();
    float r,g,b;
    r=GetRValue(col)/255.0f;
    g=GetGValue(col)/255.0f;
    b=GetBValue(col)/255.0f;
    theApp._prim_texture.Format("#(rgb,8,8,3)color(%g,%g,%g,1)",r,g,b);
    _generalPanel->OnBnClickedShowprimitive();
  }
}

void DlgMaterialEditor::OnPrimitiveSetwhite()
{
  theApp._prim_texture="#(rgb,8,8,3)color(1,1,1,1)";
  _generalPanel->OnBnClickedShowprimitive();
}

void DlgMaterialEditor::OnPrimitiveCleartexture()
{
  theApp._prim_texture="";
  _generalPanel->OnBnClickedShowprimitive();  
}

void DlgMaterialEditor::Serialize(IArchive &arch)
{
  ArchiveSection asect(arch);
  while (asect.Block("General"))  _generalPanel->Serialize(arch);
  while (asect.Block("Diffuse")) _diffusePanel->Serialize(arch);
  while (asect.Block("Ambient")) _ambientPanel->Serialize(arch);
  while (asect.Block("ForceDiffuse")) _forcedDPanel->Serialize(arch);
  while (asect.Block("Emmisive")) _emmissvPanel->Serialize(arch);
  while (asect.Block("Specular")) _speculrPanel->Serialize(arch);
  while (asect.Block("SpecularPower")) _specpowPanel->Serialize(arch);
  while (asect.Block("Stage0")) _stage0Panel->Serialize(arch);
  while (asect.Block("NextPass"))
  {
    if (arch.RValue("NextPassDlg",(_nextPassPanel!=NULL),false)==true)
    {
      if (-arch && _nextPassPanel==NULL) 
      {
        _nextPassPanel=new DlgNextPassCfg;
        _nextPassPanel->Create(_nextPassPanel->IDD,this);
        CreatePanel(_nextPassPanel);
      }
      _nextPassPanel->Serialize(arch);
    }
    else if (-arch && _nextPassPanel)
    {
      DeletePanel(_nextPassPanel);
      _nextPassPanel=NULL;
    }
  }
  while (asect.Block("stages"))
  {
    for (int i=0;i<MAT_MAXIMUMWINDOWS && _panels[i];i++)
    {    
      DlgStageCfg *stage=dynamic_cast<DlgStageCfg *>(_panels[i]);
      if (stage)
      {
        int q=1;
        arch(q);
        if (q==0)
        {
          while (_panels[i]) DeletePanel(static_cast<CDialog *>(_panels[i]));
          goto skip;
        }
        else
        {
          stage->Serialize(arch);          
          stage->SerializeLock(arch);
        }
      }
    }
    if (-arch)
    {
      int q=1;
      arch(q);
      while (q)
      {
        int p=AddStage(-1,_texGenCount);
        DlgStageCfg *stage=static_cast<DlgStageCfg *>(_panels[p]);
        stage->Serialize(arch);
        stage->SerializeLock(arch);
        arch(q);
      }
    }
    else 
    {
      int q=0;
      arch(q);
    }
skip:;
  }
  if (-arch)
  {
    RecreateOtherControls();
    RecalcLayout();
    RecalcScrollbar();
  }
}

void DlgMaterialEditor::SaveUndo()
{
  if (!_enableSaveUndo)
  {
    _enableSaveUndo=true;
    return;
  }
  ArchiveStreamMemoryOut memout;
  ArchiveSimpleBinary arch(memout);
  Serialize(arch);
  size_t bufsize=memout.GetBufferSize();
  _undo.SaveUndo(memout.DetachBuffer(),bufsize);
  TRACE0("Undo saved\n");
}
void DlgMaterialEditor::OnEditUndo()
{
  if (_needSaveUndo)
  {
    KillTimer(UNDOTIMER);
    _needSaveUndo=false;
    SaveUndo();
  }
  ArchiveStreamMemoryIn &buff=_undo.Undo();
  ArchiveSimpleBinary arch(buff);
  buff.Rewind();
  _enableSaveUndo=false;
  Serialize(arch);
}

void DlgMaterialEditor::OnEditRedo()
{
  ArchiveStreamMemoryIn &buff=_undo.Redo();
  ArchiveSimpleBinary arch(buff);
  buff.Rewind();
  _enableSaveUndo=false;
  Serialize(arch);
}

void DlgMaterialEditor::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_undo.CanUndo());
}

void DlgMaterialEditor::OnUpdateEditRedo(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_undo.CanRedo());
}

int DlgMaterialEditor::LoadMaterial(strstream &in)
{
  QIStrStream str(in.str(),in.rdbuf()->pcount());  
  ParamFile material;
  LSError ls=material.Parse(str);  
  LoadMaterial(material);
  in.rdbuf()->freeze(false);
  return 0;
}


CDialog *DlgMaterialEditor::GetFocusPanel()
{
  CWnd *focus=GetFocus();
  CWnd *prev=NULL;
  while (focus && focus!=this) 
  {
    prev=focus;
    focus=focus->GetParent();
  }
  if (focus==NULL) return NULL;
  if (prev==NULL) return NULL;
  return dynamic_cast<CDialog *>(prev);
}


void DlgMaterialEditor::OnEditCopy()
{
  CDialog *focus=GetFocusPanel();
  if (focus) focus->PostMessage(WM_COPY);
}

void DlgMaterialEditor::OnEditPaste()
{
  CDialog *focus=GetFocusPanel();
  if (focus) focus->PostMessage(WM_PASTE);
}


void DlgMaterialEditor::OnViewSelectino2()
{  
  Pathname root=theApp.CallGen().GetProjectRoot();
  Pathname rel=_rvmatName;
  rel.FullToRelative(root);
  theApp.CallGen().SelectInO2(rel,IPluginGeneralAppSide::resourceMaterial);
}

void DlgMaterialEditor::OnPrimitiveSetcolor01()
{
  DlgProceduralTex dlg;
  dlg.DoModal();
  theApp._prim_texture=dlg.vCode;
}

void DlgMaterialEditor::OnPanelNextpass()
{
  if (_nextPassPanel==NULL)
  {
    _nextPassPanel=new DlgNextPassCfg;
    _nextPassPanel->Create(_nextPassPanel->IDD,this);
    CreatePanel(_nextPassPanel);
    RecreateOtherControls();  
    RecalcLayout();
    RecalcScrollbar();
    SendMessage(MATMSG_CONFIG_CHANGES,0,0);
  }
}


void DlgMaterialEditor::OnTabClosetab()
{
  theApp.OnCloseTabMenu();
}

void DlgMaterialEditor::CheckAndUpdateChanges(void)
{
  if (GetSafeHwnd()==0) return;
  FILETIME ftm;
  if (_rvmatName.IsNull()) return;
  GetFileTime(_rvmatName,ftm);
  if (CompareFileTime(&_edit_file_time,&ftm)!=0)
  {
    if (_dlgUpdateMat.GetSafeHwnd()==0)
    {
      _dlgUpdateMat.vFilename=_rvmatName.GetFullPath();
      _dlgUpdateMat.Create(_dlgUpdateMat.IDD,this);
      _dlgUpdateMat.CenterWindow(this);
      _dlgUpdateMat.SetOwner(this);
      GetFileTime(_rvmatName,_edit_file_time);
    }
  }
}

void DlgMaterialEditor::OnFileReload()
{
  if (_dirty && AfxMessageBox(IDS_UPDATEASKDISCARD,MB_YESNO)==IDNO) return;
  _dirty=false;
  CString name=_rvmatName;
  LoadMaterial(name);
}

void DlgMaterialEditor::OnViewTexgen()
{
  if (8>-1)// TODO
  {
    texGenDlg.DoModal();
  }
}

void DlgMaterialEditor::OnUpdateViewTexgen(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(_texGenCount!=-1);
  // TODO: Add your command update UI handler code here
}
