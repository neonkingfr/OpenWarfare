// DlgStage0Cfg.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgStage0Cfg.h"
#include ".\dlgstage0cfg.h"
#include <projects/bredy.libs/archive/iarchive.h>
#include "ArchiveExt.h"
#include "DlgProceduralTex.h"


// DlgStage0Cfg dialog

IMPLEMENT_DYNAMIC(DlgStage0Cfg, CDialog)
DlgStage0Cfg::DlgStage0Cfg(CWnd* pParent /*=NULL*/)
	: CDialog(DlgStage0Cfg::IDD, pParent)
    , vTextureName(_T(""))
    , vStageFilter(_T(""))
{
  _edit=false;
}

DlgStage0Cfg::~DlgStage0Cfg()
{
}

void DlgStage0Cfg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_TEXTURENAME, wTextureName);
  DDX_Text(pDX, IDC_TEXTURENAME, vTextureName);
  DDX_CBString(pDX, IDC_STAGEFILTER, vStageFilter);
}


BEGIN_MESSAGE_MAP(DlgStage0Cfg, CDialog)
  ON_CBN_EDITCHANGE(IDC_TEXTURENAME, OnEnChangeTexturename)
  ON_CBN_SELCHANGE(IDC_TEXTURENAME, OnCbnSelchangeTexturename)
  ON_CBN_KILLFOCUS(IDC_TEXTURENAME, OnCbnKillfocusTexturename)
  ON_CBN_EDITUPDATE(IDC_STAGEFILTER, OnCbnEditupdateStagefilter)
  ON_CBN_SELENDOK(IDC_STAGEFILTER, OnCbnEditupdateStagefilter)
END_MESSAGE_MAP()


// DlgStage0Cfg message handlers

void DlgStage0Cfg::OnEnChangeTexturename()
  {
  CString s;
  wTextureName.GetWindowText(s);
  if (strcmp(s,StrRes[IDS_BROWSEFILE])==0)
    {
    Pathname root=theApp.CallGen().GetProjectRoot();
    Pathname cur=vTextureName;
    if (!cur.IsNull() && cur.RelativeToFull(root)==false) cur.SetNull();      
    CFileDialog fdlg(TRUE,0,cur,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST,StrRes[IDS_TEXTUREFILTES],this);
    fdlg.m_ofn.lpstrTitle=StrRes[IDS_SELECTTEXTURETITLE];
    if (fdlg.DoModal()==IDOK)
      {
      cur=fdlg.GetPathName();
      cur.FullToRelative(root);
      vTextureName=cur;
      }
    UpdateData(FALSE);
    wTextureName.SetEditSel(vTextureName.GetLength(),vTextureName.GetLength());
    }
  else if (strcmp(s,StrRes[IDS_PROCEDURALTEX])==0)
    {
    DlgProceduralTex dlg;
    dlg.vCode=vTextureName;
    if (dlg.DoModal()==IDOK)
      vTextureName=dlg.vCode;
    UpdateData(FALSE);
    wTextureName.SetEditSel(vTextureName.GetLength(),vTextureName.GetLength());
    }
  else
    {
    vTextureName=s;
    }
  GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
  _edit=true;
  }

void DlgStage0Cfg::OnCbnSelchangeTexturename()
  {
    PostMessage(WM_COMMAND,MAKEWPARAM(IDC_TEXTURENAME,CBN_EDITCHANGE),(LPARAM)(wTextureName.GetSafeHwnd()));
  }

void DlgStage0Cfg::OnCbnKillfocusTexturename()
  {
  if (_edit && vTextureName.GetLength())
    {
    int i=wTextureName.FindStringExact(-1,vTextureName);
    if (i>=0) wTextureName.DeleteString(i);
    wTextureName.InsertString(0,vTextureName);
    _edit=false;
    wTextureName.SetWindowText(vTextureName);
    wTextureName.SetEditSel(vTextureName.GetLength(),vTextureName.GetLength());
/*    if (vTextureName=="GPF") 
      {
        *(char *)i=1;
      }*/

    _edit=false;
    }
  }

BOOL DlgStage0Cfg::OnInitDialog()
  {
  CDialog::OnInitDialog();

  wTextureName.AddString(StrRes[IDS_BROWSEFILE]);
  wTextureName.AddString(StrRes[IDS_PROCEDURALTEX]);
  wTextureName.SetEditSel(vTextureName.GetLength(),vTextureName.GetLength());


  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

void DlgStage0Cfg::Serialize(IArchive &arch)
  {
  if (+arch) UpdateData();
  arch(vTextureName);
  arch(vStageFilter);
  if (-arch) UpdateData(FALSE);
  }

void DlgStage0Cfg::SetTexture(const char *string)
  {
  vTextureName=string;
  _edit=true;
  OnCbnKillfocusTexturename();
  }

void DlgStage0Cfg::SetStageFilter(const char *filter)
{
  SetDlgItemText(IDC_STAGEFILTER,filter);
}
void DlgStage0Cfg::OnCbnEditupdateStagefilter()
{
  GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
}
