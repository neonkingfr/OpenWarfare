// DlgColorCfg.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgColorCfg.h"
#include ".\dlgcolorcfg.h"
#include <math.h>
#include "XYZSet.h"

int DlgColorCfg::_clipboardFormat=-1;
// DlgColorCfg dialog

IMPLEMENT_DYNAMIC(DlgColorCfg, CDialog)
DlgColorCfg::DlgColorCfg(CWnd* pParent /*=NULL*/)
	: CDialog(DlgColorCfg::IDD, pParent)
{
_r=0.5f;
_g=0.7f;
_b=0.8f;
_a=1.0f;
_nosync=false;
if (_clipboardFormat==-1) _clipboardFormat=::RegisterClipboardFormat("O2 MAT Color Info");
}

DlgColorCfg::~DlgColorCfg()
{
}

int DlgColorCfg::MaxValue=255;

void DlgColorCfg::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Control(pDX, IDC_LOCKEDWITH, wLockedWith);
DDX_Control(pDX, IDC_LOCK1, wLock[0]);
DDX_Control(pDX, IDC_CHANNELNAME1, wChannelName[0]);
DDX_Control(pDX, IDC_CHANNELBAR1, wChannelBar[0]);
DDX_Control(pDX, IDC_CHANNELVALUE1, wChannelValue[0]);
DDX_Control(pDX, IDC_LOCK2, wLock[1]);
DDX_Control(pDX, IDC_CHANNELNAME2, wChannelName[1]);
DDX_Control(pDX, IDC_CHANNELBAR2, wChannelBar[1]);
DDX_Control(pDX, IDC_CHANNELVALUE2, wChannelValue[1]);
DDX_Control(pDX, IDC_LOCK3, wLock[2]);
DDX_Control(pDX, IDC_CHANNELNAME3, wChannelName[2]);
DDX_Control(pDX, IDC_CHANNELBAR3, wChannelBar[2]);
DDX_Control(pDX, IDC_CHANNELVALUE3, wChannelValue[2]);
DDX_Control(pDX, IDC_LOCK4, wLock[3]);
DDX_Control(pDX, IDC_CHANNELNAME4, wChannelName[3]);
DDX_Control(pDX, IDC_CHANNELBAR4, wChannelBar[3]);
DDX_Control(pDX, IDC_CHANNELVALUE4, wChannelValue[3]);
DDX_Control(pDX, IDC_COLORBUTTON, wColorButton);
DDX_Control(pDX, IDC_COLORSCHEME, wColourScheme);
}


BEGIN_MESSAGE_MAP(DlgColorCfg, CDialog)
  ON_CBN_SELCHANGE(IDC_LOCKEDWITH, OnCbnSelchangeLockedwith)
  ON_CBN_SELCHANGE(IDC_COLORSCHEME, OnCbnSelchangeColorscheme)
  ON_WM_HSCROLL()
  ON_EN_CHANGE(IDC_CHANNELVALUE1, OnEnChangeChannelvalue1)
  ON_EN_CHANGE(IDC_CHANNELVALUE2, OnEnChangeChannelvalue2)
  ON_EN_CHANGE(IDC_CHANNELVALUE3, OnEnChangeChannelvalue3)
  ON_EN_CHANGE(IDC_CHANNELVALUE4, OnEnChangeChannelvalue4)
  ON_STN_CLICKED(IDC_COLORPICKER, OnStnClickedColorpicker)
  ON_WM_LBUTTONUP()
  ON_WM_MOUSEMOVE()
  ON_BN_CLICKED(IDC_COLORBUTTON, OnBnClickedColorbutton)
  ON_WM_CAPTURECHANGED()
  ON_MESSAGE(WM_COPY, OnWmCopy)
  ON_MESSAGE(WM_PASTE, OnWmPaste)
END_MESSAGE_MAP()


// DlgColorCfg message handlers

void DlgColorCfg::SetColorName(const char * color, WPARAM id)
  {
  char *str=(char *)alloca(strlen(color)+3);
  sprintf(str," %s ",color);
  GetDlgItem(IDC_COLORNAME)->SetWindowText(str);
  control_id=id;
  }

BOOL DlgColorCfg::OnInitDialog()
  {
  CDialog::OnInitDialog();
  int def;

  wLockedWith.SetItemData(def=wLockedWith.AddString(StrRes[IDS_NOLINK]),IDS_NOLINK);
  wLockedWith.SetItemData(wLockedWith.AddString(StrRes[IDS_AMBIENTCOLOR]),IDS_AMBIENTCOLOR);
  wLockedWith.SetItemData(wLockedWith.AddString(StrRes[IDS_DIFFUSECOLOR]),IDS_DIFFUSECOLOR);
  wLockedWith.SetItemData(wLockedWith.AddString(StrRes[IDS_FORCEDDIFFUSE]),IDS_FORCEDDIFFUSE);
  wLockedWith.SetItemData(wLockedWith.AddString(StrRes[IDS_EMMISIVE]),IDS_EMMISIVE);
  wLockedWith.SetItemData(wLockedWith.AddString(StrRes[IDS_SPECULAR]),IDS_SPECULAR);

  for (int i=IDS_COLSCHEME_RGBA;i<=IDS_COLSCHEME_IRBA;i++)
    wColourScheme.SetItemData(wColourScheme.AddString(StrRes[i]),i);
  int p=wColourScheme.FindStringExact(-1,theApp.config.vDefColScheme);
  if (p==-1) p=wColourScheme.FindStringExact(-1,StrRes[IDS_COLSCHEME_RGBA10]);
  if (p!=-1) wColourScheme.SetCurSel(p);
  else wColourScheme.SetCurSel(0);
  wLockedWith.SelectString(-1,StrRes[IDS_NOLINK]);

  InitColorSpace();
  LoadColorValues();

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

void DlgColorCfg::OnCbnSelchangeLockedwith()
  {
  DWORD_PTR item=wLockedWith.GetItemData(wLockedWith.GetCurSel());
  if (GetParent()->SendMessage(MATMSG_DETECTCYCLES,control_id,(LPARAM)this))    
    {
    wLockedWith.SelectString(-1,StrRes[IDS_NOLINK]);
    item=IDS_NOLINK;
    }
  EnableControls(item==IDS_NOLINK);
  }

void DlgColorCfg::EnableControls(bool enable)
  {
  CWnd *traverse=GetWindow(GW_CHILD);
  while (traverse)
    {
    if (*traverse!=wLockedWith) traverse->EnableWindow(enable);
    traverse=traverse->GetWindow(GW_HWNDNEXT);
    }
  }


void DlgColorCfg::LoadColorValues()
  {
  DWORD_PTR scheme=wColourScheme.GetItemData(wColourScheme.GetCurSel());
  float v[4];
  switch(scheme)
    {
    case IDS_COLSCHEME_RGBA:
      v[0]=_r*255.0f;
      v[1]=_g*255.0f;
      v[2]=_b*255.0f;
      v[3]=_a;
      break;
    case IDS_COLSCHEME_HSI:
      RGBtoHSV(_r,_g,_b,v,v+1,v+2);
      v[3]=_a;
      break;
    case IDS_COLSCHEME_CMYK:
      {
      float c,m,y,k,ik;      
      c=1.0f-_r;
      m=1.0f-_g;
      y=1.0f-_b;
      k=v[3]=__min(c,__min(m,y));
      ik=1.0f-k;
      if (k<1.0f)
        {
        v[0]=(c-k)/ik;
        v[1]=(m-k)/ik;
        v[2]=(y-k)/ik;
        }
      else
        {
        v[0]=v[1]=v[2]=0.0f;
        }
      for (int i=0;i<4;i++) v[i]*=255.0f;
      }
      break;
    case IDS_COLSCHEME_LABA:
      {
      XYZSet xyzset(
        RGBCoord(
         toLargeInt(_r*255.0f),
         toLargeInt(_g*255.0f),
         toLargeInt(_b*255.0f)
         ));
      v[0]=xyzset.L;
      v[1]=xyzset.aa;
      v[2]=xyzset.bb;
      v[3]=_a;
      break;
      }
    case IDS_COLSCHEME_YCCA:
      {
      double rr,gg,bb;
      rr=_r/*>0.018?(pow(1.099*_r,0.45)-0.099):4.5*_r*/;
      gg=_g/*>0.018?(pow(1.099*_g,0.45)-0.099):4.5*_g*/;
      bb=_b/*>0.018?(pow(1.099*_b,0.45)-0.099):4.5*_b*/;
      v[0]=(float)(0.299*rr+0.587*gg+0.114*bb);
      v[1]=(float)(-0.299*rr-0.587*gg+0.886*bb);
      v[2]=(float)(0.701*rr-0.587*gg-0.114*bb);
      v[3]=_a;
      v[0]=(255.0f/1.402f)*v[0];
      v[1]=(114.4f*v[1])+156.0f;
      v[2]=(135.64f*v[2])+137.0f;
      }
      break;
    case IDS_COLSCHEME_PAL:
      v[0]= 0.299f*_r + 0.587f*_g + 0.114f*_b;
      v[1]= -0.147f*_r - 0.289f*_g + 0.436f*_b;
      v[2]= 0.615f*_r - 0.515f*_g - 0.100f*_b;
      v[3]=_a;
      break;
    case IDS_COLSCHEME_NTSC:
      v[0]= 0.299f*_r + 0.587f*_g + 0.114f*_b;
      v[1]= 0.596f*_r - 0.274f*_g + 0.322f*_b;
      v[2]= 0.212f*_r - 0.523f*_g - 0.311f*_b;
      v[3]=_a;
      break;
    case IDS_COLSCHEME_RGBA10:
      v[0]=_r;
      v[1]=_g;
      v[2]=_b;
      v[3]=_a;
      break;
    case IDS_COLSCHEME_IRBA:
      v[0]=_g;
      v[1]=_r-_g;
      v[2]=_b-_g;
      v[3]=_a;
      break;
    }
  const char *name=StrRes[(int)scheme];
  char namechar[2];
  char valuebuff[50];
  for (int i=0;i<4;i++)
    {
    float rmin,rmax;
    CalculateRange(i,rmin,rmax);
    namechar[0]=name[i];
    namechar[1]=0;
    wChannelName[i].SetWindowText(namechar);
    sprintf(valuebuff,_valFormats[i],v[i]);
    wChannelValue[i].SetWindowText(valuebuff);
    BOOL enabled=wChannelBar[i].IsWindowEnabled();
    wChannelBar[i].SetScrollPos(toLargeInt((v[i]-rmin)/(rmax-rmin)*1000.0f));
    wChannelBar[i].EnableWindow(enabled);
    _values[i]=v[i];
    }
  wColorButton.SetColor(GetColor());
  }
void DlgColorCfg::OnCbnSelchangeColorscheme()
  {
  InitColorSpace();
  LoadColorValues();    
  }

void DlgColorCfg::SyncBarsWithValues()
  {
  for (int i=0;i<4;i++)
    {    
    float rmin,rmax;
    CalculateRange(i,rmin,rmax);
    BOOL enabled=wChannelBar[i].IsWindowEnabled();
    wChannelBar[i].SetScrollPos(toLargeInt((_values[i]-rmin)/(rmax-rmin)*1000.0f));
    wChannelBar[i].EnableWindow(enabled);
    }
  }

static float rgbRanges[4][4]=
  {{0.0f,255.0f,1.0f,1.0f},
  {0.0f,255.0f,1.0f,1.0f},
  {0.0f,255.0f,1.0f,1.0f},
  {0.0f,1.0f,200.0f,0}};

static const char *rgbFormats[]={"%.0f","%.0f","%.0f","%.2f"};

static float cmykRanges[][4]=
  {{0.0f,255.0f,1.0f,1.0f},
  {0.0f,255.0f,1.0f,1.0f},
  {0.0f,255.0f,1.0f,1.0f},
  {0.0f,255.0f,1.0f,0}};

static const char *cmykFormats[]={"%.0f","%.0f","%.0f","%.0f"};

static float hsiRanges[][4]=
  {{0.0f,359.999f,0},
  {0.0f,1.0f,100.0f,0},
  {0.0f,1.0f,100.0f,1.0f},
  {0.0f,1.0f,200.0f,0}};

static const char *hsiFormats[]={"%.0f","%.2f","%.2f","%.2f"};

static float labaRanges[][4]=
  {{0.0f,100.0f,2.0f,0},
  {-120.0f,120.0f,1.0f,0},
  {-120.0f,120.0f,1.0f,0},
  {0.0f,1.0f,200.0f,0}};

static const char *labaFormats[]={"%.1f","%.1f","%.1f","%.2f"};

static float irbaRanges[][4]=
  {{0.0f,1.0f,100.0f,0},
  {-1.0f,1.0f,100.0f,0},
  {-1.0f,1.0f,100.0f,0},
  {0.0f,1.0f,100.0f,0}};

static const char *irbaFormats[]={"%.2f","%.2f","%.2f","%.2f"};

static float yccaRanges[][4]=
  {{0.0f,255.0f,1.0f,0},
  {0.0f,255.0f,1.0f,0},
  {0.0f,255.0f,1.0f,0},
  {0.0f,1.0f,200.0f,0}};

static const char *yccaFormats[]={"%.0f","%.0f","%.0f","%.2f"};

static float palRanges[][4]=
  {{0.0f,1.0f,200.0f,0},
  {-1.0f,1.0f,200.0f,0},
  {-1.0f,1.0f,200.0f,0},
  {-1.0f,1.0f,200.0f,0}};

static const char *palFormats[]={"%.2f","%.2f","%.2f","%.2f"};

static float normRanges[][4]=
  {{0.0f,1.0f,200.0f,1},
  {0.0f,1.0f,200.0f,1},
  {0.0f,1.0f,200.0f,1},
  {0.0f,1.0f,200.0f,0}};

static const char *normFormats[]={"%.2f","%.2f","%.2f","%.2f"};


void DlgColorCfg::InitColorSpace()
  {
  DWORD_PTR scheme=wColourScheme.GetItemData(wColourScheme.GetCurSel());
  switch (scheme)
    {
    case IDS_COLSCHEME_RGBA:
      _barRanges=rgbRanges;
      _valFormats=rgbFormats;
      break;
    case IDS_COLSCHEME_HSI:
      _barRanges=hsiRanges;
      _valFormats=hsiFormats;
      break;
    case IDS_COLSCHEME_CMYK:
      _barRanges=cmykRanges;
      _valFormats=cmykFormats;
      break;
    case IDS_COLSCHEME_LABA:
      _barRanges=labaRanges;
      _valFormats=labaFormats;
      break;
    case IDS_COLSCHEME_YCCA:
      _barRanges=yccaRanges;
      _valFormats=yccaFormats;
      break;
    case IDS_COLSCHEME_PAL:
      _barRanges=palRanges;
      _valFormats=palFormats;
      break;
    case IDS_COLSCHEME_NTSC:
      _barRanges=palRanges;
      _valFormats=palFormats;
      break;
    case IDS_COLSCHEME_RGBA10:
      _barRanges=normRanges;
      _valFormats=normFormats;
      break;
    case IDS_COLSCHEME_IRBA:
      _barRanges=irbaRanges;
      _valFormats=irbaFormats;
      break;
    };
  for (int i=0;i<4;i++)
    {
    wChannelBar[i].SetScrollRange(0,1000);
    }
  }
void DlgColorCfg::ScrollByCode(int bar, UINT code, int relpos)
  {
  CScrollBar &scrbar=wChannelBar[bar];
  float value=_values[bar];
  char buff[50];  

  float rmin,rmax;
  CalculateRange(bar,rmin,rmax);

  float step=1.0f/_barRanges[bar][2];
  float bigstep=(rmax-rmin)/8.0f;


  switch (code)
    {
    case SB_LEFT: value=rmin;break;
    case SB_RIGHT: value=rmax;break;
    case SB_LINELEFT: 
        value-=step;
        if (value<rmin) value=rmin;
        break;
    case SB_LINERIGHT: 
        value+=step;
        if (value>rmax) value=rmax;
        break;
    case SB_PAGELEFT: 
        value-=bigstep;
        if (value<rmin) value=rmin;
        break;
    case SB_PAGERIGHT: 
        value+=bigstep;
        if (value>rmax) value=rmax;
        break;
    case SB_THUMBPOSITION:
    case SB_THUMBTRACK:
        value+=(rmax-rmin)*(relpos)/1000.0f;
        if (value>rmax) value=rmax;
        if (value<rmin) value=rmin;
        break;
    }
  _values[bar]=value;
  sprintf(buff,_valFormats[bar],value);
  _nosync=true;
  wChannelValue[bar].SetWindowText(buff);
  _nosync=false;
  }

void DlgColorCfg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
  {
  for (int bar=0;bar<4;bar++)
    {
    if (pScrollBar==wChannelBar+bar)
      {
      int curPos=pScrollBar->GetScrollPos();
      if (wLock[bar].GetCheck())
        {
        for (int i=0;i<4;i++)
          if (wLock[i].GetCheck())
            ScrollByCode(i,nSBCode,(int)nPos-curPos);
        }
      else
        ScrollByCode(bar,nSBCode,(int)nPos-curPos);
      SaveColorValues();
      SyncBarsWithValues();
      return;
      }
    }
  CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
  }

void DlgColorCfg::SaveColorValues()
  {
  DWORD_PTR scheme=wColourScheme.GetItemData(wColourScheme.GetCurSel());
  float *v=_values;
  switch(scheme)
    {
    case IDS_COLSCHEME_RGBA:
      _r=v[0]/255.0f;
      _g=v[1]/255.0f;
      _b=v[2]/255.0f;
      _a=v[3];
      break;
    case IDS_COLSCHEME_HSI:
      HSVtoRGB(&_r,&_g,&_b,v[0],v[1],v[2]);
      _a=v[3];
      break;
    case IDS_COLSCHEME_CMYK:
      {
      float c,m,y,k;      
      c=v[0]/255.0f;
      m=v[1]/255.0f;
      y=v[2]/255.0f;
      k=v[3]/255.0f;
      c=__min(1,c*(1-k)+k);
      m=__min(1,m*(1-k)+k);
      y=__min(1,y*(1-k)+k);
      _r=1-c;
      _g=1-m;
      _b=1-y;
      }
      break;
    case IDS_COLSCHEME_LABA:
      {
      XYZSet xyzset;
      RGBCoord rgb;
      xyzset.L=v[0];
      xyzset.aa=v[1];
      xyzset.bb=v[2];
      _a=v[3];
      xyzset.LABtoXYZ();
      xyzset.XYZtoRGB(rgb);
      if (rgb.isValid())
        {
        _r=rgb.getRed()/255.0f;
        _g=rgb.getGreen()/255.0f;
        _b=rgb.getBlue()/255.0f;
        }
      break;
      }
    case IDS_COLSCHEME_YCCA:
      {
  	  float L = 1.3584f * v[0];
	  float Chroma1= 2.2179f * (v[1] - 156);
	  float Chroma2= 1.8215f * (v[2] - 137);

	  _r= (L + Chroma2)/255.0f;
	  _g= (L - 0.194f*Chroma1 - 0.509f*Chroma2)/255.0f;
	  _b= (L + Chroma1)/255.0f;
      _a=v[3];

      }
      break;
    case IDS_COLSCHEME_PAL:
      _r=v[0] + 0.000f*v[1] + 1.140f*v[2];
      _g=v[0] - 0.396f*v[1] - 0.581f*v[2];
      _b=v[0] + 2.029f*v[1] + 0.000f*v[2];
      _a=v[3];
      break;
    case IDS_COLSCHEME_NTSC:
      _r= v[0] + 0.956f*v[1] + 0.621f*v[2];
      _g= v[0] - 0.272f*v[1] - 0.647f*v[2];
      _b= v[0] - 1.105f*v[1] + 1.702f*v[2];
      v[3]=_a;
      break;
    case IDS_COLSCHEME_RGBA10:
      _r=v[0];
      _g=v[1];
      _b=v[2];
      _a=v[3];
      break;
    case IDS_COLSCHEME_IRBA:      
      {
      _r=_g=_b=v[0];
      _r+=v[1];
      _b+=v[2];
      _r=__max(__min(_r,1.0f),0.0f);
      _b=__max(__min(_b,1.0f),0.0f);
      _a=v[3];
      break;
      }
    }
  wColorButton.SetColor(GetColor());
  GetParent()->PostMessage(MATMSG_COLORCHANGED,control_id,(LPARAM)this);
  }

COLORREF DlgColorCfg::GetColor()
  {
  float r,g,b;
  r=__max(__min(_r,1.0f),0.0f);
  g=__max(__min(_g,1.0f),0.0f);
  b=__max(__min(_b,1.0f),0.0f);
  return RGB(toLargeInt(r*255.0f),toLargeInt(g*255.0f),toLargeInt(b*255.0f));
  }
void DlgColorCfg::OnEnChangeChannelvalue1()
  {
  if (_nosync) return;
  char buff[50];
  wChannelValue[0].GetWindowText(buff,50);
  _values[0]=(float)atof(buff);
  SaveColorValues();
  SyncBarsWithValues();
  }

void DlgColorCfg::OnEnChangeChannelvalue2()
  {
  if (_nosync) return;
  char buff[50];
  wChannelValue[1].GetWindowText(buff,50);
  _values[1]=(float)atof(buff);
  SaveColorValues();
  SyncBarsWithValues();
  }

void DlgColorCfg::OnEnChangeChannelvalue3()
  {
  if (_nosync) return;
  char buff[50];
  wChannelValue[2].GetWindowText(buff,50);
  _values[2]=(float)atof(buff);
  SaveColorValues();
  SyncBarsWithValues();
  }

void DlgColorCfg::OnEnChangeChannelvalue4()
  {
  if (_nosync) return;
  char buff[50];
  wChannelValue[3].GetWindowText(buff,50);
  _values[3]=(float)atof(buff);
  SaveColorValues();
  SyncBarsWithValues();
  }

void DlgColorCfg::OnStnClickedColorpicker()
  {
  GetDlgItem(IDC_COLORPICKER)->ShowWindow(SW_HIDE);
  SetCapture();
  SetCursor(theApp.LoadCursor(IDC_PICKER));
  }

void DlgColorCfg::OnLButtonUp(UINT nFlags, CPoint point)
  {
  if (GetCapture()==this) ReleaseCapture();
  GetDlgItem(IDC_COLORPICKER)->ShowWindow(SW_SHOW);  
  CDialog::OnLButtonUp(nFlags, point);
  }

void DlgColorCfg::OnMouseMove(UINT nFlags, CPoint point)
  {
  if (GetCapture()==this)
    {
    CDC dc;
    dc.CreateDC("DISPLAY",NULL,NULL,NULL);
    CPoint point(GetMessagePos());
    COLORREF ref=dc.GetPixel(point);
    _r=GetRValue(ref)/255.0f;
    _g=GetGValue(ref)/255.0f;
    _b=GetBValue(ref)/255.0f;
    LoadColorValues();
    dc.DeleteDC();
    }
  }

void DlgColorCfg::OnBnClickedColorbutton()
  {  
  CColorDialog dlg(GetColor(),CC_ANYCOLOR|CC_FULLOPEN|CC_RGBINIT);
  if (dlg.DoModal()==IDOK)
    {
    COLORREF ref=dlg.GetColor();
    SetColor(ref);
    }
  }

WPARAM DlgColorCfg::GetLinkedWith()
  {
  int cursel=wLockedWith.GetCurSel();
  return wLockedWith.GetItemData(cursel);
  }

void DlgColorCfg::SetColor(COLORREF color)
  {
  _r=GetRValue(color)/255.0f;
  _g=GetGValue(color)/255.0f;
  _b=GetBValue(color)/255.0f;
  LoadColorValues();
  }

void DlgColorCfg::SetColor(float r, float g, float b, float a)
{
  _r=r;
  _g=g;
  _b=b;
  _a=a;
  LoadColorValues();
}

void DlgColorCfg::SetLinkedWith(int link)
  {
  wLockedWith.SelectString(-1,StrRes[link]);
  OnCbnSelchangeLockedwith();
  }
void DlgColorCfg::OnCaptureChanged(CWnd *pWnd)
  {
  GetDlgItem(IDC_COLORPICKER)->ShowWindow(SW_SHOW);  
  }

LRESULT DlgColorCfg::OnWmCopy(WPARAM wParam, LPARAM lParam)
  {
  if (OpenClipboard()==FALSE) 
    {
    AfxMessageBox(IDS_UNABLETOOPENCLIPBOARD,MB_OK|MB_ICONEXCLAMATION);
    return 0;
    }
  EmptyClipboard();
  HGLOBAL glob=GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE,sizeof(float)*4);
  float *values=(float *)GlobalLock(glob);
  values[0]=_r;
  values[1]=_g;
  values[2]=_b;
  values[3]=_a;
  GlobalUnlock(glob);  
  SetClipboardData(_clipboardFormat,glob);
  glob=GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE,512);
  char *text=(char *)GlobalLock(glob);
  sprintf(text,"{%g,%g,%g,%g}",_r,_g,_b,_a);
  GlobalUnlock(glob);
  SetClipboardData(CF_TEXT,glob);
  CloseClipboard();
  return 1;
  }

LRESULT DlgColorCfg::OnWmPaste(WPARAM wParam, LPARAM lParam)
  {
  if (OpenClipboard()==FALSE)
    {
    AfxMessageBox(IDS_UNABLETOOPENCLIPBOARD,MB_OK|MB_ICONEXCLAMATION);
    return 1;
    }
  HGLOBAL glob=GetClipboardData(_clipboardFormat);
  if (glob!=NULL)
    {
    float *values=(float *)GlobalLock(glob);
    _r=values[0];
    _g=values[1];
    _b=values[2];
    _a=values[3];
    GlobalUnlock(glob);
    LoadColorValues();
    }
  CloseClipboard();
  return 1;
  }

void DlgColorCfg::Serialize(IArchive &arch)
  {
  arch("r",_r);
  arch("g",_g);
  arch("b",_b);
  arch("a",_a);
  if (-arch) LoadColorValues();
  }

void DlgColorCfg::CalculateRange(int bar, float &rmin, float &rmax)
{
  rmin=_barRanges[bar][0];
  rmax=_barRanges[bar][1];
  float scl=rmax-rmin;
  if (_barRanges[bar][3]>0)
  {
    rmin+=scl*theApp.config.vRangeMinVal;
    rmax=scl*theApp.config.vRangeMaxVal;
  }

}