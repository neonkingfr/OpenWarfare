#pragma once

class MassProcess
  {
	void *_fileInfo;
  public:    
    /// process all files in folder and call virtual function for each one
    /** 
    @param folder folder name without file or mask specification. Back-slash at end of path can be included
    @param mask file mask to process. To process all files, specify *.*
    @param recursive if true recursive processes all subfolders. 
    @return true, if all files has been processed. Return false, if any error has been detected, or 
      if processing was stoped, function returns false too */
    bool ProcessFolders(const char *folder, const char *mask, bool recursive);

    ///Callback function called for each file
    /**
    @param filename pointer to filename to process
    @return true to process next file, false to stop processing
    */
    virtual bool ProcessFile(const char *filename)=0;

	///Obtains current file informations
	/**
	@param infosize size of info structure, used for check, if application wants right info structre.
	@return pointer to the information structure
	*/
	const void *GetCurFileSpecificInfo(size_t infosize);

	virtual bool BeforeEnterFolder(const char *folder) {return true;}

	virtual void AfterExitFolder(const char *folder) {}
  };
