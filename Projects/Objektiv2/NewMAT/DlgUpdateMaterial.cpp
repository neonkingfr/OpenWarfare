// DlgUpdateMaterial.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgUpdateMaterial.h"
#include ".\dlgupdatematerial.h"


// DlgUpdateMaterial dialog

IMPLEMENT_DYNAMIC(DlgUpdateMaterial, CDialog)
DlgUpdateMaterial::DlgUpdateMaterial(CWnd* pParent /*=NULL*/)
	: CDialog(DlgUpdateMaterial::IDD, pParent)
    , vFilename(_T(""))
{
}

DlgUpdateMaterial::~DlgUpdateMaterial()
{
}

void DlgUpdateMaterial::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_FILENAME, vFilename);
}


BEGIN_MESSAGE_MAP(DlgUpdateMaterial, CDialog)
END_MESSAGE_MAP()


// DlgUpdateMaterial message handlers

void DlgUpdateMaterial::OnOK()
{
  GetOwner()->PostMessage(WM_COMMAND,ID_FILE_RELOAD,0);

  DestroyWindow();
}

void DlgUpdateMaterial::OnCancel()
{

  DestroyWindow();
}

BOOL DlgUpdateMaterial::PreTranslateMessage(MSG* pMsg)
{
  if (IsDialogMessage(pMsg)) return TRUE;

  return CDialog::PreTranslateMessage(pMsg);
}
