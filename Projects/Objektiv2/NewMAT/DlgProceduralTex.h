#pragma once


// DlgProceduralTex dialog

class DlgProceduralTex : public CDialog
{
	DECLARE_DYNAMIC(DlgProceduralTex)

public:
	DlgProceduralTex(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgProceduralTex();

// Dialog Data
	enum { IDD = IDD_PROCEDURALTEX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  float vRed;
  float vGreen;
  float vBlue;
  float vAlpha;

  CString vCode;
  CString vType;

  void DialogRules();
protected:
  virtual void OnOK();
public:
  BOOL vAlphamode;
  virtual BOOL OnInitDialog();
  afx_msg void OnBnClickedAlphamode();
  afx_msg void OnBnClickedRadio2();
  afx_msg void OnBnClickedRadio3();

  afx_msg void OnBnClickedPreset1();
  afx_msg void OnBnClickedPreset3();
  afx_msg void OnBnClickedPreset4();
  afx_msg void OnBnClickedPreset5();
  afx_msg void OnBnClickedPreset7();
  afx_msg void OnBnClickedPreset6();
  afx_msg void OnBnClickedPreset8();
  afx_msg void OnBnClickedPreset9();
};


