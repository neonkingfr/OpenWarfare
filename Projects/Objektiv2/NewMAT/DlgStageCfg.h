#pragma once
#include "afxwin.h"
#include "../geMatrix4.h"

#include "ConfigPanelsCommon.h"

// DlgStageCfg dialog

class DlgStageCfg : public CDialog
{
	DECLARE_DYNAMIC(DlgStageCfg)
    int _number;
    bool _trnsUpdate;
    bool _matrixValid;
    int _nTexGen;

public:
    geMatrix4 _initialMatrix;
    geMatrix4 _resultMatrix;

    static int _clipboardFormat;

    
	DlgStageCfg(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgStageCfg();

// Dialog Data
	enum { IDD = IDD_STAGECFG };

    void SetStageNumber(int number);
    int GetStageNumber() {return _number;}

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  

	DECLARE_MESSAGE_MAP()
public:
  virtual BOOL OnInitDialog();
  CComboBox wTransformStack;
  CComboBox wTransformOper;
  CComboBox wStageSource;
  CEdit wTransformValue;
  void ResetMatrix(void);
  void SaveTransform(void);
  afx_msg void OnEnChangeValue();
  afx_msg void OnCbnSelchangeOperation();
  afx_msg void OnDeltaposValuespin(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnCbnSelchangeStacklist();
  const geMatrix4 & GetTransform(void);
  afx_msg void OnBnClickedEditmatrix();
  afx_msg void OnBnClickedIrradiance();
  afx_msg void OnBnClickedFresnelFce();

  void SetTexture(const char *texture);
  void SetUVSource(const char *texture);
  void SetTexGenCount(int _ncount);
  int GetTexGenCount(){return _nTexGen;};
  CComboBox wUVSource;
  bool _lockUVSource;
  CString vStageSource;
  CEdit wStageName;
  CComboBox wStageFilter;
  CButton wInsertTransform,wDeleteTransform,wEditMatrix;
  CSpinButtonCtrl wTransformValueSpin;
  CStatic wTexSource;

  CString GetTexture();
  CString GetUVSource();  
  bool IsStageEnabled();
  afx_msg void OnCbnSelchangeUvsource();
  afx_msg void OnCbnSelchangeSourcename();
  afx_msg void OnBnClickedEnable();
  afx_msg void OnBnClickedBrowsetex();
  afx_msg void OnBnClickedInserttransform();
  afx_msg void OnBnClickedDeletetransform();
  void LockStageSetup(bool lock,bool irradiance);
  void LockDelButton(bool lock);
  void LockUVSource(bool lock);
  afx_msg void OnCbnSelectchangeUvsource();
  afx_msg void OnCbnSelectchangeSourcename();
  afx_msg void OnBnClickedDelstage();
  afx_msg void OnEnChangeStagenumber();
  CString GetTextureEx(bool preview);

  afx_msg LRESULT OnWmCopy(WPARAM wParam, LPARAM lParam);
  afx_msg LRESULT OnWmPaste(WPARAM wParam, LPARAM lParam);

  void Serialize(IArchive &arch);
  void SerializeLock(IArchive &arch);

  afx_msg void OnStnDblclickEditname();
  afx_msg void OnEnKillfocusStagenameedit();

  void ScaveFiles(BTree<RStringI> &scavenger);
  void RecorverFiles(const BTree<RStringI> &scavenger);

  void SetStageName(const char *name);
  CString GetStageName();

  void SetStageFilter(const char *name);
  CString GetStageFilter();
  afx_msg void OnCbnEditupdateStagefilter();
};
