// DlgTypeManager.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgTypeManager.h"
#include ".\dlgtypemanager.h"
#include <el/interfaces/iScc.hpp>



// DlgTypeManager dialog

IMPLEMENT_DYNAMIC(DlgTypeManager, CDialog)
DlgTypeManager::DlgTypeManager(CWnd* pParent /*=NULL*/)
	: CDialog(DlgTypeManager::IDD, pParent)
    , vLockedStages(_T(""))
    , vLockAll(FALSE)
    , vLockAddButt(FALSE)
    , vVertexShaderName(_T(""))
    , vPixelShaderName(_T(""))
    , vTemplateName(_T(""))
    , vDefaultFlags(_T(""))
    , vOnlyFlags(FALSE)
    , vPredefinedFlags(_T(""))
    , vLockFlags(FALSE)
{
}

DlgTypeManager::~DlgTypeManager()
{
}

void DlgTypeManager::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_LOCKEDSTAGES, vLockedStages);
  DDX_Control(pDX, IDC_LOCKEDSTAGES, wLockedStages);
  DDX_Check(pDX, IDC_LOCKALLSTAGES, vLockAll);
  DDX_Check(pDX, IDC_LOCKADDBUTTON, vLockAddButt);
  DDX_Text(pDX, IDC_VERTEXSHADERNAME, vVertexShaderName);
  DDX_Text(pDX, IDC_PIXELSHADERNAME, vPixelShaderName);
  DDX_CBString(pDX, IDC_CURRENTTEMPLATES, vTemplateName);
  DDX_Control(pDX, IDC_CURRENTTEMPLATES, wTemplateList);
  DDX_Text(pDX, IDC_DEFAULTFLAGS, vDefaultFlags);
  DDX_Check(pDX, IDC_ONLYFLAGS, vOnlyFlags);
  DDX_Text(pDX, IDC_PREDEFINEDFLAGS, vPredefinedFlags);
  DDX_Check(pDX, IDC_LOCKFLAGS, vLockFlags);
}


BEGIN_MESSAGE_MAP(DlgTypeManager, CDialog)
  ON_BN_CLICKED(IDC_LOCKALLSTAGES, OnBnClickedLockallstages)
  ON_CBN_EDITCHANGE(IDC_CURRENTTEMPLATES, OnCbnEditchangeCurrenttemplates)
  ON_BN_CLICKED(IDC_DELETE, OnBnClickedDelete)
  ON_CBN_SELCHANGE(IDC_CURRENTTEMPLATES, OnCbnSelchangeCurrenttemplates)
END_MESSAGE_MAP()


// DlgTypeManager message handlers

bool DlgTypeManager::ProcessFile(const char *filename)
  {
  filename=Pathname::GetNameFromPath(filename);
  char *z=strcpy((char *)alloca(strlen(filename)+1),filename);
  char *p=const_cast<char *>(Pathname::GetExtensionFromPath(z));
  *p=0;
  wTemplateList.AddString(Pathname::GetNameFromPath(z));
  return true;
  }

BOOL DlgTypeManager::OnInitDialog()
  {
  CDialog::OnInitDialog();

  this->ProcessFolders(theApp.config.vTemplatePath,"*"TYPEEXTENSION,false);
  CheckDlgButton(IDC_LOCKALLSTAGES,1);
  CheckDlgButton(IDC_LOCKADDBUTTON,1);
  DialogRules();

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

void DlgTypeManager::DialogRules()
  {
  BOOL enab=wTemplateList.GetWindowTextLength()>0;

  GetDlgItem(IDOK)->EnableWindow(enab);
  GetDlgItem(IDC_DELETE)->EnableWindow(enab);

  wLockedStages.EnableWindow(!IsDlgButtonChecked(IDC_LOCKALLSTAGES));  
  }
void DlgTypeManager::OnBnClickedLockallstages()
  {
  DialogRules();
  }

void DlgTypeManager::OnCbnEditchangeCurrenttemplates()
  {
  DialogRules();
  }

void DlgTypeManager::OnOK()
  {  
  CDialog::OnOK();
  vFullPath.SetDirectory(theApp.config.vTemplatePath);
  vFullPath.SetFiletitle(vTemplateName);
  vFullPath.SetExtension(TYPEEXTENSION);
  }

void DlgTypeManager::OnBnClickedDelete()
  {
  UpdateData();  
  CString msg;
  msg.Format(IDS_ASKDELETETYPE,vTemplateName);
  if (AfxMessageBox(msg,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDNO) return;

  vFullPath.SetDirectory(theApp.config.vTemplatePath);
  vFullPath.SetFiletitle(vTemplateName);
  vFullPath.SetExtension(TYPEEXTENSION);
  SccFunctions *scc=theApp.CallGen().GetSccInterface();
  if (scc && scc->UnderSSControlNotDeleted(vFullPath))    
    {
    SCCRTN res=scc->Remove(vFullPath);
    if (res!=0) 
      {
      msg.Format(IDS_SCCRESULT,res);
      AfxMessageBox(msg,MB_ICONEXCLAMATION);
      return;
      }
    SetFileAttributes(vFullPath,GetFileAttributes(vFullPath) & ~FILE_ATTRIBUTE_READONLY);
    }  
  DeleteFile(vFullPath);
  int sel=wTemplateList.FindStringExact(-1,vTemplateName);
  if (sel!=-1) wTemplateList.DeleteString(sel);
  wTemplateList.SetWindowText("");
  DialogRules();
  }

void DlgTypeManager::OnCbnSelchangeCurrenttemplates()
  {
  PostMessage(WM_COMMAND,MAKEWPARAM(IDC_CURRENTTEMPLATES,CBN_EDITCHANGE),(LPARAM)(GetDlgItem(IDC_CURRENTTEMPLATES)->GetSafeHwnd()));
  }
