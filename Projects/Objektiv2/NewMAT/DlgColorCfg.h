#pragma once
#include "afxwin.h"
#include "ColorButton.h"

#include "ConfigPanelsCommon.h"



class IArchive;
// DlgColorCfg dialog


class DlgColorCfg : public CDialog
{
	DECLARE_DYNAMIC(DlgColorCfg)    

    WPARAM control_id;

public:
    float _r,_g,_b,_a;
    float _values[4];
    static int MaxValue;    

    static int _clipboardFormat;

    DlgColorCfg(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgColorCfg();

// Dialog Data
	enum { IDD = IDD_COLORCFG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CComboBox wLockedWith;
  CButton wLock[4];
  CStatic wChannelName[4];
  CScrollBar wChannelBar[4];
  CEdit wChannelValue[4];
  CColorButton wColorButton;
  const float (*_barRanges)[4];
  const char **_valFormats;
  void SetColorName(const char * color, WPARAM id);
  virtual BOOL OnInitDialog();
  afx_msg void OnCbnSelchangeLockedwith();
  void EnableControls(bool enable);
  CComboBox wColourScheme;

  void LoadColorValues();
  void SyncBarsWithValues();
  void InitColorSpace();

  WPARAM GetLinkedWith();
  WPARAM GetControlID() {return control_id;}

  COLORREF GetColor();
  bool _nosync;
  
  afx_msg void OnCbnSelchangeColorscheme();
  void ScrollByCode(int bar, UINT code, int relpos);
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  void SaveColorValues();
  afx_msg void OnEnChangeChannelvalue1();
  afx_msg void OnEnChangeChannelvalue2();
  afx_msg void OnEnChangeChannelvalue3();
  afx_msg void OnEnChangeChannelvalue4();
  afx_msg void OnStnClickedColorpicker();
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnBnClickedColorbutton();
  void SetColor(COLORREF color);

  void SetColor(float r, float g, float b, float a);

  void SetLinkedWith(int link);
  afx_msg void OnCaptureChanged(CWnd *pWnd);

  afx_msg LRESULT OnWmCopy(WPARAM wParam, LPARAM lParam);
  afx_msg LRESULT OnWmPaste(WPARAM wParam, LPARAM lParam);

  void Serialize(IArchive &arch);
  void CalculateRange(int bar, float &rmin, float &rmax);


};
