#pragma once
#include "afxwin.h"


#include "ConfigPanelsCommon.h"
#include "afxcmn.h"


class SMatRestricts;
// DlgGeneralCfg dialog

class DlgGeneralCfg : public CDialog
{
	DECLARE_DYNAMIC(DlgGeneralCfg)

public:
	DlgGeneralCfg(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgGeneralCfg();

// Dialog Data
	enum { IDD = IDD_GENERALCFG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedAddstage();
  CComboBox wPixelShaderID;
  CComboBox wVertexShaderID;

  void SetPixelShader(const char *text);
  void SetVertexShader(const char *text);
  void AddType(const char *type);
  void InitByRestricts(const SMatRestricts &restricts);
  void InitFreeType();
  void ResetAll();
  CString GetPixelShader();
  CString GetVertexShader();
  void SetMaterialName(const char * name);
  CEdit wMaterialName;
  CComboBox wType;
  afx_msg void OnCbnSelchangeMaterialtype();
  void LockAddButton(bool lock);

  void SetMaterialType(const char *typeName);
  afx_msg void OnBnClickedShodmodel();
  afx_msg void OnBnClickedShowprimitive();
  afx_msg void OnBnClickedSelectprimitive();
  afx_msg void OnCbnSelchangePixershaderid();
  afx_msg void OnCbnSelchangeVertexshaderid();

  void Serialize(IArchive &arch);

  afx_msg void OnBnClickedCreateselection();
  CListCtrl wFlagList;
  virtual BOOL OnInitDialog();
  void SetFlag(const char *name, bool value);
  bool GetFlag(int index, CString *name=NULL, bool *value=NULL);
  void ClearFlags();
  void FlagsByString(const char *flags,bool value);
  void StringByFlags(CString &flgstring, bool value);
  void LockFlags(bool lock);
  afx_msg void OnLvnItemchangedFlags(NMHDR *pNMHDR, LRESULT *pResult);
  void ResetAllFlags();
};
