// DlgOptions.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgOptions.h"
#include ".\dlgoptions.h"


// DlgOptions dialog

IMPLEMENT_DYNAMIC(DlgOptions, CDialog)
DlgOptions::DlgOptions(CWnd* pParent /*=NULL*/)
: CDialog(DlgOptions::IDD, pParent)
, vTemplatePath(_T(""))
, vDefColScheme(_T(""))
, vTextEditor(_T(""))
, vRangeMinVal(0)
, vRangeMaxVal(1)
{
}

DlgOptions::~DlgOptions()
{
}

void DlgOptions::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_TEMPLATEPATH, vTemplatePath);
  DDX_Control(pDX, IDC_DEFCOLSCHEME, wDefColScheme);
  DDX_CBString(pDX, IDC_DEFCOLSCHEME, vDefColScheme);
  DDX_Control(pDX, IDC_EDIT1, wTextEditor);
  DDX_Text(pDX, IDC_EDIT1, vTextEditor);
  DDX_Text(pDX, IDC_MINVAL, vRangeMinVal);
  DDX_Text(pDX, IDC_MAXVAL, vRangeMaxVal);
}


#define CONFIGSECTION "Options"

BEGIN_MESSAGE_MAP(DlgOptions, CDialog)  
  ON_BN_CLICKED(IDC_O2OPTIONS, OnBnClickedO2options)
  ON_BN_CLICKED(IDC_BROSE, OnBnClickedBrose)
END_MESSAGE_MAP()




// DlgOptions message handlers

void DlgOptions::OnOK()
{
  CDialog::OnOK();
  SaveConfig();
}

void DlgOptions::SaveConfig()
{  
  WRITECONFIGSTR(CONFIGSECTION,vTemplatePath);
  WRITECONFIGSTR(CONFIGSECTION,vDefColScheme);
  WRITECONFIGSTR(CONFIGSECTION,vTextEditor);
  WRITECONFIGFLOAT(CONFIGSECTION,vRangeMinVal);
  WRITECONFIGFLOAT(CONFIGSECTION,vRangeMaxVal);
}

void DlgOptions::LoadConfig()
{
  GETCONFIGSTR(CONFIGSECTION,vTemplatePath,"p:\\doc\\MatTemplates");
  GETCONFIGSTR(CONFIGSECTION,vDefColScheme,"RGBA 1.0");
  GETCONFIGSTR(CONFIGSECTION,vTextEditor,"");
  GETCONFIGFLOAT(CONFIGSECTION,vRangeMinVal,0.0f);
  GETCONFIGFLOAT(CONFIGSECTION,vRangeMaxVal,1.0f);
}

Pathname DlgOptions::GetTemplatePath()
{
  Pathname out;
  out.SetDirectory(vTemplatePath);
  return out;
}
void DlgOptions::OnBnClickedO2options()
{
  EnableWindow(FALSE);
  theApp.CallGen().OpenOptionsDialog();
  EnableWindow(TRUE);
}

BOOL DlgOptions::OnInitDialog()
{
  CDialog::OnInitDialog();

  for (int i=IDS_COLSCHEME_RGBA;i<=IDS_COLSCHEME_IRBA;i++)
    wDefColScheme.SetItemData(wDefColScheme.AddString(StrRes[i]),i);  

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgOptions::OnBnClickedBrose()
{
  CString title;
  CString fname;
  CString filter;
  wTextEditor.GetWindowText(fname);
  title.LoadString(IDS_BROWSEFOREDITOR);
  filter.LoadString(IDS_APPLICATIONSFILTER);
  CFileDialog fdlg(TRUE,0,fname,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,filter);
  if (fdlg.DoModal()==IDOK)
  {
    wTextEditor.SetWindowText(fdlg.GetPathName());
  }
}
