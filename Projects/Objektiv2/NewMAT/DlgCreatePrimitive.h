#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// DlgCreatePrimitive dialog

class DlgCreatePrimitive : public CDialog
{
	DECLARE_DYNAMIC(DlgCreatePrimitive)

public:
	DlgCreatePrimitive(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgCreatePrimitive();

// Dialog Data
	enum { IDD = IDD_CREATEPRIMITIVE };

    IMatAppSide::ObjPrimitiveType primType;
    int quality;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support


	DECLARE_MESSAGE_MAP()
public:
  CComboBox wPrimitive;
  CSliderCtrl wQuality;
  virtual BOOL OnInitDialog();
protected:
  virtual void OnOK();
public:
  CSliderCtrl wSize;
  int vSize;
};
