#pragma once
#include "afxwin.h"
#include "DlgSpecularCfg.h"


// DlgIrradiance dialog

class DlgIrradiance : public CDialog
{
	DECLARE_DYNAMIC(DlgIrradiance)

public:
    CString _irrString;
    CString _format;
    int _width;
    int _height;
    int _levels;
    float _exponent;

    DlgSpecularCfg _specular;


	DlgIrradiance(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgIrradiance();

// Dialog Data
	enum { IDD = IDD_IRRADIANCEDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  virtual BOOL OnInitDialog();
  CComboBox wFormat;
  CComboBox wWidth;
  CComboBox wHeight;
  CComboBox wLevels;
  void ParseIrrString(void);
protected:
  virtual void OnOK();
};
