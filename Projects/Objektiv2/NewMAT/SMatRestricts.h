#pragma once

#include <Projects/Bredy.Libs/Archive/IArchive.h>

struct SStageRestricts
  {
  RString uvSource;
  RString texGenSource;
  bool lockedTexGen;
  RString defaultValue;
  RString stageName;
  bool irradiance;
  bool locked;
  int stagenum;  
  void Serialize(IArchive &arch);    
//  template<> 
    friend void ArchiveVariableExchange(IArchive &arch,SStageRestricts &data)
      {data.Serialize(arch);}
  };



TypeIsMovable(SStageRestricts);

class SMatRestricts
  {
  public:
    AutoArray<SStageRestricts> stageRestricts;
    RString vertexShader;
    RString pixerShader;
    RString typeName;  
    RString defaultFlags;
    RString predefinedFlags;
    COLORREF ambient;
    COLORREF diffuse;
    COLORREF forcedDiffuse;
    COLORREF emmisive;
    COLORREF specular;
    float specularPrower;
    RString defaultTexture;
    int texGenCount;

    bool onlyFlags;
    bool lockFlags;
    bool lockAddStage;
    SMatRestricts(void);

  public:
    void Serialize(IArchive &arch);

    bool LoadRestricts(const char *filename);
    bool SaveRestricts(const char *filename);
  };

TypeIsMovable(SMatRestricts);