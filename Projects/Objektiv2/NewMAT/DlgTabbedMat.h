#pragma once
#include <afxcmn.h>


class DlgMaterialEditor;
// DlgTabbedMat dialog

class DlgTabbedMat : public CDialog
{
	DECLARE_DYNAMIC(DlgTabbedMat)

    DlgMaterialEditor *_activePanel;
    CImageList _images;

    int _tabSize;
    int _panelX;
    int _panelY;

public:
	DlgTabbedMat(DlgMaterialEditor* pParent = NULL);   // standard constructor
	virtual ~DlgTabbedMat();

// Dialog Data
	enum { IDD = IDD_TABBEDMATDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  void AddPanel(DlgMaterialEditor * panel);
  CTabCtrl wTabs;
  void RecalcLayout(void);
  afx_msg void OnTcnSelchangeTabs(NMHDR *pNMHDR, LRESULT *pResult);
  DlgMaterialEditor *GetActive();
  void SetActive(DlgMaterialEditor *panel);
  DlgMaterialEditor *EnumPanels(unsigned int &position);
  void Activate();
  bool BeforeClose();
public:
  afx_msg void OnClose();
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
  afx_msg void OnSize(UINT nType, int cx, int cy);
  void CloseCurrentTab();
  afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
protected:
  virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
public:
  afx_msg void OnFileExit();
  afx_msg void OnFileNew();
  LRESULT OnTitleUpdated(WPARAM wParam,LPARAM lParam);
  virtual BOOL OnInitDialog();

  void LoadWindowPosition();
  void SaveWindowPosition();

  afx_msg void OnTimer(UINT nIDEvent);
  afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
  afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
};
