// DlgGeneralCfg.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgGeneralCfg.h"
#include ".\dlggeneralcfg.h"
#include "SMatRestricts.h"
#include "ArchiveExt.h"


// DlgGeneralCfg dialog

IMPLEMENT_DYNAMIC(DlgGeneralCfg, CDialog)
DlgGeneralCfg::DlgGeneralCfg(CWnd* pParent /*=NULL*/)
: CDialog(DlgGeneralCfg::IDD, pParent)
{
}

DlgGeneralCfg::~DlgGeneralCfg()
{
}

void DlgGeneralCfg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_PIXERSHADERID, wPixelShaderID);
  DDX_Control(pDX, IDC_VERTEXSHADERID, wVertexShaderID);
  DDX_Control(pDX, IDC_MATERIALNAME, wMaterialName);
  DDX_Control(pDX, IDC_MATERIALTYPE, wType);
  DDX_Control(pDX, IDC_FLAGS, wFlagList);
}


BEGIN_MESSAGE_MAP(DlgGeneralCfg, CDialog)
  ON_BN_CLICKED(IDC_ADDSTAGE, OnBnClickedAddstage)
  ON_CBN_SELCHANGE(IDC_MATERIALTYPE, OnCbnSelchangeMaterialtype)
  ON_BN_CLICKED(IDC_SHODMODEL, OnBnClickedShodmodel)
  ON_BN_CLICKED(IDC_SHOWPRIMITIVE, OnBnClickedShowprimitive)
  ON_BN_CLICKED(IDC_SELECTPRIMITIVE, OnBnClickedSelectprimitive)
  ON_CBN_SELCHANGE(IDC_PIXERSHADERID, OnCbnSelchangePixershaderid)
  ON_CBN_SELCHANGE(IDC_VERTEXSHADERID, OnCbnSelchangeVertexshaderid)
  ON_BN_CLICKED(IDC_CREATESELECTION, OnBnClickedCreateselection)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_FLAGS, OnLvnItemchangedFlags)
END_MESSAGE_MAP()


// DlgGeneralCfg message handlers

void DlgGeneralCfg::OnBnClickedAddstage()
{
  GetParent()->PostMessage(WM_COMMAND,IDC_ADDSTAGE,0);
}

static void SetComboboxText(CComboBox &box, const char *text)
{
  int i=box.FindStringExact(-1,text);
  if (i==CB_ERR) i=box.AddString(text);
  box.SetCurSel(i);  
}

void DlgGeneralCfg::SetPixelShader(const char *text)
{
  SetComboboxText(wPixelShaderID, text);
}

void DlgGeneralCfg::SetVertexShader(const char *text)
{
  SetComboboxText(wVertexShaderID, text);
}

CString DlgGeneralCfg::GetPixelShader()
{
  CString out;
  int cursel=wPixelShaderID.GetCurSel();
  if (cursel!=-1) wPixelShaderID.GetLBText(cursel,out);
  return out;
}
CString DlgGeneralCfg::GetVertexShader()
{
  CString out;
  int cursel=wVertexShaderID.GetCurSel();
  if (cursel!=-1) wVertexShaderID.GetLBText(cursel,out);
  return out;
}

void DlgGeneralCfg::SetMaterialName(const char * name)
{
  wMaterialName.SetWindowText(name);
  int len=strlen(name);
  wMaterialName.SetSel(len,len);
}

void DlgGeneralCfg::AddType(const char *type)
{
  if (wType.FindStringExact(-1,type)==-1) wType.AddString(type);
}

void DlgGeneralCfg::InitByRestricts(const SMatRestricts &restricts)
{
  SetPixelShader(restricts.pixerShader);
  SetVertexShader(restricts.vertexShader);
  AddType(restricts.typeName);
  FlagsByString(restricts.defaultFlags,false);
  FlagsByString(restricts.predefinedFlags,false);
}

void DlgGeneralCfg::InitFreeType()
{
  wType.AddString(StrRes[IDS_FREETYPE]);
  wType.SelectString(-1,StrRes[IDS_FREETYPE]);
}

void DlgGeneralCfg::ResetAll()
{
  wPixelShaderID.ResetContent();
  wVertexShaderID.ResetContent();
  wType.ResetContent();
  ClearFlags();
}
void DlgGeneralCfg::OnCbnSelchangeMaterialtype()
{
  int sel=wType.GetCurSel();
  int free=wType.FindStringExact(-1,StrRes[IDS_FREETYPE]);
  wPixelShaderID.EnableWindow(sel==free);
  wVertexShaderID.EnableWindow(sel==free);  
  if (sel!=free)
    GetParent()->SendMessage(MATMSG_RESTRICTIONS_CHANGES,sel,(LPARAM)&wType);
  else
    GetParent()->SendMessage(MATMSG_RESTRICTIONS_CHANGES,0,0);
  GetParent()->PostMessage(MATMSG_CONFIG_CHANGES,0,0);
}
void DlgGeneralCfg::LockAddButton(bool lock)
{
  GetDlgItem(IDC_ADDSTAGE)->EnableWindow(!lock);
}

void DlgGeneralCfg::SetMaterialType(const char *typeName)
{
  wType.SelectString(-1,StrRes[IDS_FREETYPE]);
  if (typeName!=NULL) wType.SelectString(-1,typeName);
  wPixelShaderID.EnableWindow(typeName==NULL);
  wVertexShaderID.EnableWindow(typeName==NULL);  
}
void DlgGeneralCfg::OnBnClickedShodmodel()
{
  theApp.CallGen().ReloadViewer();
}

void DlgGeneralCfg::OnBnClickedShowprimitive()
{
  CString name;
  wMaterialName.GetWindowText(name);
  const char *partname=name;
  const char *root=theApp.CallGen().GetProjectRoot();
  int sz=strlen(root);
  if (sz<strlen(partname)) partname+=sz;
  ObjectData *obj;
  if (theApp._prim_model.GetLength()==0)
    obj=theApp.CallMatClient().CreatePrimitive(theApp._prim_type,theApp._prim_segments,theApp._prim_scale,theApp._prim_texture,partname);
  else
    obj=theApp.CallMatClient().CreatePrimitive(theApp._prim_model,theApp._prim_texture,partname);
  if (obj==NULL)
    MessageBeep(MB_ICONEXCLAMATION);
  else
    theApp.CallGen().UpdateViewer(obj);
  theApp.CallMatClient().DestroyPrimitive(obj);
}

void DlgGeneralCfg::OnBnClickedSelectprimitive()
{
  CMenu menu;
  menu.LoadMenu(IDR_MAINMENU);
  CMenu *primmenu=menu.GetSubMenu(3)->GetSubMenu(0);
  CPoint mspt(GetMessagePos());
  primmenu->TrackPopupMenu(TPM_LEFTALIGN  |TPM_LEFTBUTTON,mspt.x,mspt.y,GetParent(),NULL);
}

void DlgGeneralCfg::OnCbnSelchangePixershaderid()
{
  GetParent()->PostMessage(MATMSG_CONFIG_CHANGES,0,0);
}

void DlgGeneralCfg::OnCbnSelchangeVertexshaderid()
{
  GetParent()->PostMessage(MATMSG_CONFIG_CHANGES,0,0);
}

void DlgGeneralCfg::Serialize(IArchive &arch)
{
  CString s;
  if (+arch) wType.GetLBText(wType.GetCurSel(),s);
  if (arch("MaterialType",s) && -arch) SetMaterialType(s);
  if (+arch) s=GetPixelShader();
  if (arch("PixelShader",s) && -arch) SetPixelShader(s);
  if (+arch) s=GetVertexShader();
  if (arch("VertexShader",s) && -arch) SetVertexShader(s);
  bool lock;
  if (+arch) lock=wPixelShaderID.IsWindowEnabled()!=FALSE;
  if (arch("PixelShdEnabled",lock) && -arch) wPixelShaderID.EnableWindow(lock);
  if (+arch) lock=wPixelShaderID.IsWindowEnabled()!=FALSE;
  if (arch("VertexShdEnabled",lock) && -arch) wVertexShaderID.EnableWindow(lock);
  if (+arch) lock=GetDlgItem(IDC_ADDSTAGE)->IsWindowEnabled()!=FALSE;
  if (arch("AddStageButt",lock) && -arch) GetDlgItem(IDC_ADDSTAGE)->EnableWindow(lock);
  if (-arch) ClearFlags();
  if (+arch) StringByFlags(s,true);
  if (arch("FlagsOn",s) && -arch) FlagsByString(s,true);
  if (+arch) StringByFlags(s,false);
  if (arch("FlagsOff",s) && -arch) FlagsByString(s,false);

}
void DlgGeneralCfg::OnBnClickedCreateselection()
{
  GetParent()->PostMessage(WM_COMMAND,MAKEWPARAM(ID_VIEW_SELECTINO2,0),NULL);
}

BOOL DlgGeneralCfg::OnInitDialog()
{
  CDialog::OnInitDialog();
  ListView_SetExtendedListViewStyleEx(wFlagList,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT);
  CRect rc;
  wFlagList.GetClientRect(&rc);
  wFlagList.InsertColumn(0,"",LVCFMT_LEFT,rc.right-GetSystemMetrics(SM_CYVSCROLL),0);
  return TRUE;
}

void DlgGeneralCfg::SetFlag(const char *name, bool value)
{
  CString curf;
  int cnt=wFlagList.GetItemCount();
  for (int i=0;i<cnt;i++)
  {
    curf=wFlagList.GetItemText(i,0);
    if (_stricmp(curf,name)==0)
    {
      ListView_SetCheckState(wFlagList,i,value);
      return;
    }
  }
  int p=wFlagList.InsertItem(cnt,name,0);
  ListView_SetCheckState(wFlagList,p,value);
}

void DlgGeneralCfg::ResetAllFlags()
{
  CString curf;
  int cnt=wFlagList.GetItemCount();
  for (int i=0;i<cnt;i++)
  {
      ListView_SetCheckState(wFlagList,i,false);
  }
}


bool DlgGeneralCfg::GetFlag(int i, CString *name, bool *value)
{
  int cnt=wFlagList.GetItemCount();
  if (i<0 || i>=cnt) return false;
  if (name) *name=wFlagList.GetItemText(i,0);
  if (value) *value=ListView_GetCheckState(wFlagList,i);
  return true;
}

void DlgGeneralCfg::ClearFlags()
{
  wFlagList.DeleteAllItems(); 
}

void DlgGeneralCfg::FlagsByString(const char *flags,bool value)
{
  char *p=strcpy((char *)alloca(strlen(flags)+2),flags);
  p[strlen(p)+1]=0;
  char *q=strchr(p,',');
  while (q) {*q=0;q=strchr(q+1,',');};
  while (*p)
  {
    SetFlag(p,value);
    p+=strlen(p)+1;
  };
}

void DlgGeneralCfg::LockFlags(bool lock)
{
  wFlagList.EnableWindow(!lock);
}

void DlgGeneralCfg::StringByFlags(CString &flgstring, bool value)
{
  ostrstream out;
  int cnt=wFlagList.GetItemCount();
  CString name;
  for (int i=0;i<cnt;i++) if ((ListView_GetCheckState(wFlagList,i)!=FALSE)==value)
  {
    name=wFlagList.GetItemText(i,0);
    if (out.tellp()>0) out<<",";
    out<<(LPCTSTR)name;
  }
  out.put(0);
  flgstring=out.str();
  out.freeze(0);  
}
void DlgGeneralCfg::OnLvnItemchangedFlags(NMHDR *pNMHDR, LRESULT *pResult)
{
  GetParent()->SendMessage(MATMSG_CONFIG_CHANGES,0,0);
}
