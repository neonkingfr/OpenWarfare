#pragma once


// DlgAbout dialog

class DlgAbout : public CDialog
{
	DECLARE_DYNAMIC(DlgAbout)

public:
	DlgAbout(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgAbout();

// Dialog Data
	enum { IDD = IDD_ABOUTDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedEmail();
};
