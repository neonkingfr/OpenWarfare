#include "StdAfx.h"
#include ".\smatrestricts.h"
#include <Projects/Bredy.Libs/Archive/IArchive.h>
#include <Projects/Bredy.Libs/Archive/ArchiveParamFile.h>
#include "ArchiveExt.h"
#include "resource.h"

SMatRestricts::SMatRestricts(void)
  {
    onlyFlags=false;
    lockFlags=false;
    ambient=RGB(0xff,0xff,0xff);
    diffuse=RGB(0xff,0xff,0xff);
    forcedDiffuse=0;
    emmisive=0;
    specular=0;
    specularPrower=0;
    texGenCount=-1;
  }




void SStageRestricts::Serialize(IArchive &arch)
  {
  arch("StageNum",stagenum);
  arch("DefaultValue",defaultValue);
  arch("Locked",locked);
  arch("Irradiance",irradiance);
  arch("uvSource",uvSource);
  arch("texGenSource",texGenSource);
  arch("lockedTexGen",lockedTexGen);
  arch("stageName",stageName,RString(StrRes[IDS_DEFSTAGECONFIGNAME]));
  }

void SMatRestricts::Serialize(IArchive &arch)
  {
  int size=stageRestricts.Size();
  arch("NStages",size);
  if (-arch) stageRestricts.Clear();
  for (int i=0;i<size;i++)
    {
    SStageRestricts &ss=+arch?stageRestricts[i]:stageRestricts.Append();
    arch.SetIndex(i);
    arch("StageRestricts",ss);
    }
  arch("vertexShader",vertexShader);
  arch("pixerShader",pixerShader);
  arch("typeName",typeName);
  arch("lockAddStage",lockAddStage);
  arch("defaultFlags",defaultFlags);
  arch("predefinedFlags",predefinedFlags);
  arch("onlyFlags",onlyFlags);
  arch("lockFlags",lockFlags);
  arch("ambient",ambient);
  arch("diffuse",diffuse);
  arch("forcedDiffuse",forcedDiffuse);
  arch("emmisive",emmisive);
  arch("specular",specular);
  arch("specularPrower",specularPrower);
  arch("defaultTexture",defaultTexture);
  arch("NTexGen",texGenCount);
  }

bool SMatRestricts::LoadRestricts(const char *filename)
  {
  ParamFile pfile;
  pfile.Parse(filename);
  ArchiveParamFile apf(&pfile,false);
  Serialize(apf);
  return !(!apf);
  }

bool SMatRestricts::SaveRestricts(const char *filename)
  {
  ParamFile pfile;
  ArchiveParamFile apf(&pfile,true);
  Serialize(apf);
  if (pfile.Save(filename)!=0) return false;
  return !(!apf);
  }
