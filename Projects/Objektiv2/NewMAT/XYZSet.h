/*************************************************
 * File: XYZSet.java
 * Description: Conversion between RGB, XYZ and CIELAB
 *  color spaces.
 * Author: Gene Vishnevsky  Oct. 15, 1997
*************************************************/

class RGBCoord {

	/**
	* RGB space coordinates (0...255)
	*/
protected: int red, green, blue;

	/**
	* Flag set to true if the coordinates are valid.
	*/
	 bool valid;			// 


	/**
	* Constructs RGBCoord given initial values. The values
	* must be 0 to 255.
	*/
public:
  RGBCoord( int red=0, int green=0, int blue=0 ) {
		this->red = red;
		this->green = green;
		this->blue = blue;
		valid = true;
	}


	/**
	* Copy new RGB values includin the valid flag.
	* @param rgb source for the new RGB values.
	*/
	void getFromRGB( const RGBCoord& rgb )  {
		red = rgb.red;
		green = rgb.green;
		blue = rgb.blue;
		valid = rgb.valid;
	}

	/**
	* Returns the red component.
	*/
	int getRed() const {
		return red;
	}

	/**
	* Returns the green component.
	*/
	int getGreen() const {
		return green;
	}

	/**
	* Returns the blue component.
	*/
	int getBlue() const {
		return blue;
	}

	/**
	* Returns true if the coordinates are valid.
	*/
	bool isValid() const {
		return valid;
	}

	/**
	* Copies the components into array.
	* @param rgb[] array to place red, green and blue
	* components, respectively.
	* @return the valid flag
	*/
	bool getRGB ( int *rgb ) const {
		rgb[0] = red;
		rgb[1] = green;
		rgb[2] = blue;
		return valid;
	}


	/**
	* Sets the red component.
	*/
	void setRed( int red ) {
		this->red = red;
	}

	/**
	* Sets the green component.
	*/
	void setGreen( int green ) {
		this->green = green;
	}

	/**
	* Sets the blue component.
	*/
	void setBlue( int blue ) {
		this->blue = blue;
	}

	/**
	* Sets the valid flag.
	*/
	void setValid( bool valid ) {
		this->valid = valid;
	}

	/**
	* Sets the RGB coordinates to specified values. The values
	* must be 0 to 255.
	*/
	void setRGB( int red, int green, int blue ) {
		this->red = red;
		this->green = green;
		this->blue = blue;
		this->valid = true;		// of course, it's true
	}


	/**
	* Compares RGB components. Use when verifying if components
	* must be updated from another source.
	* @param rgb set of components to compare with.
	* @return true if different in any respect; false otherwise
	*/
	bool differentFrom( const RGBCoord &rgb ) const {

		// don't update from invalid coordinates
		if( rgb.isValid() == false )
			return false;

		if( rgb.getRed() != this->getRed() ||
			rgb.getGreen() != this->getGreen() ||
			rgb.getBlue() != this->getBlue() )

			return true;

		return false;

	}
  };

/**
 * This class implements conversions between RGB, CIE XYZ and CIELAB color
 * spaces.
 */
class XYZSet {

public:
	float L, aa, bb;	// current CIE Lab coords
	float X, Y, Z;		// CIE XYZ coords
	int r, g, b;		// RGB coords
	float rf, gf, bf;	// [0,1] RGB

	/**
	* x color matching function tabulated at 20-nm intervals.
	* (10-degree 1964 CIE suppl. std. observer)
	*/
	static float Px[];

	/**
	* y color matching function tabulated at 20-nm intervals.
	* (10-degree 1964 CIE suppl. std. observer)
	*/
	static float Py[];

	/**
	* z color matching function tabulated at 20-nm intervals.
	* (10-degree 1964 CIE suppl. std. observer)
	*/
	static float Pz[];

	/**
	* Constructs a converter with no arguments.
	*/
public :
    XYZSet() {}


	/**
	* Constructs a converter with an argument.
	* @param rgb RGB components that are used to
	* compute initial XYZ and LAB values.
	* @see RGBCoord
	*/
	XYZSet( RGBCoord &rgb ) {
		convertFromRGB( rgb );
	}

	/**
	* Converts from RGB to XYZ and LAB.
	* @param rgb RGB components to convert from.
	* @see RGBCoord
	*/
	void convertFromRGB( RGBCoord &rgb ) {
		RGBtoXYZ( rgb );
		XYZtoLAB();
	}


	/**
	* Computes XYZ coordinates for a spectrum.
	* @param data[] an array of 16 spectrum data points.
	*/
	void spectrumToXYZ( float *data ) {

		X = 0.f;
		Y = 0.f;
		Z = 0.f;

		// integrate
		for( int wl = 0; wl < 16; wl++) {
			X += data[wl] * Px[wl];
			Y += data[wl] * Py[wl];
			Z += data[wl] * Pz[wl];
		}
	}


	/**
	* Converts from XYZ to RGB internally.
	* @return 24-bit RGB color if conversion successful; 0 otherwise
	*/
	int XYZtoRGB() {

		// using D65 white point
		r = (int)((3.063f*X - 1.393f*Y - 0.476f*Z) * 255);
		if( r < 0 ) return 0;
		if( r > 255 ) return 0;

		g = (int)((-0.969f*X + 1.876f*Y + 0.042f*Z) * 255);
		if( g < 0 ) return 0;
		if( g > 255 ) return 0;

		b = (int)((0.068f*X - 0.229f*Y + 1.069f*Z) * 255);
		if( b < 0 ) return 0;
		if( b > 255 ) return 0;

		return (255<<24) | (r<<16) | (g<<8) | b;
	}


	/**
	* Converts from XYZ to RGB.
	* @param rgb RGBCoord object to set with converted values.
	*/
	void XYZtoRGB( RGBCoord &rgb ) {

		if( XYZtoRGB() == 0 ) {
			rgb.setValid( false );
		}
		else {
			rgb.setRGB( r, g, b );
		}
	}


	/**
	* Converts from RGB to XYZ.
	* @param rgb input RGB values.
	*/
	void RGBtoXYZ( RGBCoord &rgb ) {

			// scale to [0,1] range
			rf = 0.0039215f * rgb.getRed();		// 0..1
			gf = 0.0039215f * rgb.getGreen();	// 0..1
			bf = 0.0039215f * rgb.getBlue();	// 0..1

			X = 0.431f*rf + 0.342f*gf + 0.178f*bf;
			Y = 0.222f*rf + 0.707f*gf + 0.071f*bf;
			Z = 0.020f*rf + 0.130f*gf + 0.939f*bf;

	}


	/**
	* Converts from LAB to XYZ internally.
	*/
	void LABtoXYZ() {
		LABtoXYZ( aa, bb );
	}

	/**
	* Converts from LAB to XYZ. Uses internal L-value.
	* @param a the a-value
	* @param b the b-value
	*/
	void LABtoXYZ( float a, float b ) {

		float frac = (L + 16) / 116;

		if( L < 7.9996f ) {
			Y = L / 903.3f;
			X = a / 3893.5f + Y;
			Z = Y - b / 1557.4f;
		}
		else {
			float tmp;
			tmp = frac + a / 500;
			X = tmp * tmp * tmp;
			Y = frac * frac * frac;
			tmp = frac - b / 200;
			Z = tmp * tmp * tmp;
		}
	}


	/**
	* Converts from XYZ to LAB internally.
	*/
	void XYZtoLAB() {

		if( Y > 0.008856 ) {
			L = (float)( 116.0f * pow( (double)Y, 0.3333333 ) - 16 ); 
		}
		else {
			L = 903.3f * Y;
		}

		aa = 500.0f * (float)( pow( (double)X, 0.3333333) - 
			pow( (double)Y, 0.3333333) );

		bb = 200.0f * (float)( pow( (double)Y, 0.3333333) - 
			pow( (double)Z, 0.3333333) );

	}



};

void RGBtoHSV( float r, float g, float b, float *h, float *s, float *v );
void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v );