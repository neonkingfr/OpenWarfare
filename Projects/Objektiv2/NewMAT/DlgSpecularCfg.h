#pragma once
#include "afxwin.h"


#include "ConfigPanelsCommon.h"

// DlgSpecularCfg dialog

class DlgSpecularCfg : public CDialog
{
	DECLARE_DYNAMIC(DlgSpecularCfg)
    COLORREF _diffuse;
    COLORREF _specular;
    float _factor;
    CBitmap _specView;
    CBitmap _specGraph;

public:
	DlgSpecularCfg(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgSpecularCfg();

    bool _stepMode;

// Dialog Data
	enum { IDD = IDD_SPECULARCFG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CEdit wSpecularValue;
  CScrollBar wSpecularScroll;
  virtual BOOL OnInitDialog();
  void LoadValues(void);
  CButton wSpecularView;

  void RecalculateView();
  void RecalculateGraph();
  void UpdateGraphs();
  afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
  afx_msg void OnEnChangeSpecularval();
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  CButton wSpecularGraph;

  void SetDrawColors(COLORREF diffuse, COLORREF specular)
    {
    _diffuse=diffuse;
    _specular=specular;
    if (_specView.GetSafeHandle()!=NULL) _specView.DeleteObject();
    wSpecularView.Invalidate(FALSE);
    }

  void Serialize(IArchive &arch);

};
