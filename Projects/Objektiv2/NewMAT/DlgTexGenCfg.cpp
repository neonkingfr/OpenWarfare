// DlgTexGenCfg.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgTexGenCfg.h"
#include "DlgEditMatrix.h"
#include <Projects/Bredy.Libs/Archive/Archive.h>
#include <Projects/Bredy.Libs/Archive/ArchiveStreamMemory.h>
#include <Projects/Bredy.Libs/Archive/ArchiveParamFile2.h>
#include "ArchiveExt.h"



// CDlgTexGenCfg dialog

IMPLEMENT_DYNAMIC(CDlgTexGenCfg, CDialog)

CDlgTexGenCfg::CDlgTexGenCfg(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgTexGenCfg::IDD, pParent),
_initialMatrix(true),
_resultMatrix(true)
{

}

CDlgTexGenCfg::~CDlgTexGenCfg()
{
}

void CDlgTexGenCfg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_TEXGEN_UVSOURCE, wUVSource);
  DDX_Control(pDX, IDC_TEXGEN_STACKLIST, wTransformStack);
  DDX_Control(pDX, IDC_TEXGEN_OPERATION, wTransformOper);
  DDX_Control(pDX, IDC_TEXGEN_VALUE, wTransformValue);
}


BEGIN_MESSAGE_MAP(CDlgTexGenCfg, CDialog)
  ON_BN_CLICKED(IDC_TEXGEN_EDITMATRIX, OnBnClickedTexgenEditmatrix)
  ON_NOTIFY(UDN_DELTAPOS, IDC_TEXGEN_VALUESPIN, OnDeltaposTexgenValuespin)
  ON_BN_CLICKED(IDC_DELETETRANSFORM, OnBnClickedDeletetransform)
  ON_BN_CLICKED(IDC_TEXGEN_INSERTTRANSFORM, OnBnClickedTexgenInserttransform)
  ON_CBN_SELCHANGE(IDC_TEXGEN_STACKLIST, OnCbnSelchangeTexgenStacklist)
  ON_CBN_SELCHANGE(IDC_TEXGEN_UVSOURCE, OnCbnSelchangeTexgenUvsource)
  ON_CBN_EDITUPDATE(IDC_TEXGEN_UVSOURCE, OnCbnEditupdateTexgenUvsource)
  ON_CBN_SELCHANGE(IDC_TEXGEN_OPERATION, OnCbnSelchangeTexgenOperation)
  ON_EN_CHANGE(IDC_TEXGEN_VALUE, OnEnChangeTexgenValue)
END_MESSAGE_MAP()

void CDlgTexGenCfg::SetNumber(int number)
  {
  _number=number;  
  }

BOOL CDlgTexGenCfg::OnInitDialog()
{
  CDialog::OnInitDialog();

  wTransformOper.AddString(StrRes[IDS_TRNS_POSX]);
  wTransformOper.AddString(StrRes[IDS_TRNS_POSY]);
  wTransformOper.AddString(StrRes[IDS_TRNS_SCALEX]);
  wTransformOper.AddString(StrRes[IDS_TRNS_SCALEY]);
  wTransformOper.AddString(StrRes[IDS_TRNS_ROTATEZ]);
  wTransformOper.AddString(StrRes[IDS_TRNS_SCALE]);  
  wTransformOper.AddString(StrRes[IDS_TRNS_SKEWX]);  
  wTransformOper.AddString(StrRes[IDS_TRNS_SKEWY]);  

  wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX]);
  wUVSource.AddString(StrRes[IDS_UVSOURCE_NONE]);
  wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX1]);
  wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX2]);
  wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX3]);
  wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX4]);
  wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX5]);
  wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX6]);
  wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX7]);
  wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX8]);
  wUVSource.SetWindowText(StrRes[IDS_UVSOURCE_NONE]); 

  SetDlgItemInt(IDC_TEXGEN_STAGENUMBER,_number,FALSE);
  SetUVSource(_initUVSource);
  ResetMatrix();

  return TRUE;
}



// CDlgTexGenCfg message handlers

void CDlgTexGenCfg::OnBnClickedTexgenEditmatrix()
{
  const geMatrix4 &mx=GetTransform();
  DlgEditMatrix dlg;
  dlg.vMatrix=mx;
  if (dlg.DoModal()==IDOK)
    {
    _resultMatrix=dlg.vMatrix;
    ResetMatrix();
    _matrixValid=true;
   //TODO GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
    }
}

static int DetectOperation(const char *buffer)
  {
  for (int i=IDS_TRNS_POSX;i<IDS_TRNS_SKEWZ;i++)
    {
    if (_stricmp(buffer,StrRes[i])==0) return i;
    }
  return 0;
  }

const geMatrix4 & CDlgTexGenCfg::GetTransform(void)
  {
  if (!_matrixValid)
    {
    geMatrix4 temp=_initialMatrix;
    char buff[50];
    int cnt=wTransformStack.GetCount();
    for (int i=0;i<cnt;i++)
      {
      wTransformStack.GetLBText(i,buff);
      DWORD_PTR data=wTransformStack.GetItemData(i);
      float value=*(float *)&data;
      int op=DetectOperation(buff);
      switch (op)
        {
        case IDS_TRNS_POSX: temp.TranslateC(value,0,0);break;
        case IDS_TRNS_POSY: temp.TranslateC(0,value,0);break;
        case IDS_TRNS_POSZ: temp.TranslateC(0,0,value);break;
        case IDS_TRNS_SCALE: temp.ScaleC(value,value,value);break;
        case IDS_TRNS_SCALEX: temp.ScaleC(value,1,1);break;
        case IDS_TRNS_SCALEY: temp.ScaleC(1,value,1);break;
        case IDS_TRNS_SCALEZ: temp.ScaleC(1,1,value);break;
        case IDS_TRNS_ROTATEX: temp.RotateC(geAX,value*3.14159265f/180.0f);break;
        case IDS_TRNS_ROTATEY: temp.RotateC(geAY,value*3.14159265f/180.0f);break;
        case IDS_TRNS_ROTATEZ: temp.RotateC(geAZ,value*3.14159265f/180.0f);break;
        case IDS_TRNS_SKEWX: 
        case IDS_TRNS_SKEWY: 
          {
          geMatrix4 mx(true);
          if (op==IDS_TRNS_SKEWX) mx.a12=value;
          else mx.a21=value;
          temp.MultC(mx);
          }
        break;
        default: //currently unsupported 
          break;
        }
      }
    _resultMatrix=temp;
    _matrixValid=true;
    }
  return _resultMatrix;
  }

void CDlgTexGenCfg::ResetMatrix(void)
  {
  _initialMatrix=_resultMatrix;
  wTransformOper.SetCurSel(-1);
  wTransformStack.ResetContent();
  wTransformStack.AddString(StrRes[IDS_TRNS_IDENTITY]);
  wTransformValue.SetWindowText("0");
  wTransformStack.SetCurSel(0);
  }


void CDlgTexGenCfg::OnDeltaposTexgenValuespin(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  char buff[50];
  wTransformOper.GetLBText(wTransformOper.GetCurSel(),buff);
  int oper=DetectOperation(buff);
  float offset;
  switch (oper)
    {
    case IDS_TRNS_POSX:
    case IDS_TRNS_POSY:
    case IDS_TRNS_POSZ:
    case IDS_TRNS_SKEWX:
    case IDS_TRNS_SKEWY:
    case IDS_TRNS_SKEWZ:
      offset=0.01f;
      break;
    case IDS_TRNS_SCALEX:
    case IDS_TRNS_SCALEY:
    case IDS_TRNS_SCALEZ:
    case IDS_TRNS_SCALE:
      offset=0.1f;
      break;
    case IDS_TRNS_ROTATEX:
    case IDS_TRNS_ROTATEY:
    case IDS_TRNS_ROTATEZ:
      offset=1.0f;
      break;
    default: offset=0.1f;
      break;
    }
  wTransformValue.GetWindowText(buff,50);
  float value=(float)atof(buff);
  value-=pNMUpDown->iDelta*offset;
  sprintf(buff,"%.2f",value);
  wTransformValue.SetWindowText(buff);
  *pResult = 0;
}

void CDlgTexGenCfg::OnBnClickedDeletetransform()
{
  int curSel=wTransformStack.GetCurSel();
  if (curSel+1>=wTransformStack.GetCount()) return;
  wTransformStack.DeleteString(curSel);
  if (wTransformStack.GetCount()==0)
    OnBnClickedTexgenInserttransform();
  wTransformStack.SetCurSel(curSel?curSel-1:curSel);
  OnCbnSelchangeTexgenStacklist();
}

void CDlgTexGenCfg::OnBnClickedTexgenInserttransform()
{
  int curSel=wTransformStack.GetCurSel();
  wTransformStack.InsertString(curSel,StrRes[IDS_TRNS_IDENTITY]);
  wTransformStack.SetCurSel(curSel);
  OnCbnSelchangeTexgenStacklist();
}

void CDlgTexGenCfg::OnCbnSelchangeTexgenStacklist()
{
  char buff[50];
  _trnsUpdate=false;
  int cursel=wTransformStack.GetCurSel();
  wTransformStack.GetLBText(cursel,buff);
  DWORD_PTR data=wTransformStack.GetItemData(cursel);
  float value=*(float *)&data;
  wTransformOper.SetCurSel(-1);
  wTransformOper.SelectString(-1,buff);
  sprintf(buff,"%.2f",value);
  wTransformValue.SetWindowText(buff);
  _trnsUpdate=true;
}

void CDlgTexGenCfg::OnCbnSelchangeTexgenUvsource()
{
  PostMessage(WM_COMMAND,MAKELPARAM(IDC_TEXGEN_UVSOURCE,CBN_EDITUPDATE),(LPARAM)wUVSource.GetSafeHwnd());
}

void CDlgTexGenCfg::OnCbnEditupdateTexgenUvsource()
{
  wUVSource.GetWindowText(_initUVSource);
  bool matrixenable=(_initUVSource!=StrRes[IDS_UVSOURCE_NONE]);
  GetDlgItem(IDC_TEXGEN_EDITMATRIX)->EnableWindow(matrixenable);
  wTransformValue.EnableWindow(matrixenable);
  wTransformStack.EnableWindow(matrixenable);
  wTransformOper.EnableWindow(matrixenable);

  //GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
}

void CDlgTexGenCfg::OnCbnSelchangeTexgenOperation()
{
  SaveTransform();
  //GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
}
void CDlgTexGenCfg::SaveTransform(void)
  {
  char buff[50];
  if (wTransformOper.GetCurSel()<0 || !_trnsUpdate) return;
  int cursel=wTransformStack.GetCurSel();
  if (cursel+1==wTransformStack.GetCount())
    {
    wTransformStack.AddString(StrRes[IDS_TRNS_IDENTITY]);
    }
  wTransformOper.GetLBText(wTransformOper.GetCurSel(),buff);
  wTransformStack.InsertString(cursel,buff);
  wTransformStack.SetCurSel(cursel);
  wTransformValue.GetWindowText(buff,50);
  float value=(float)atof(buff);
  wTransformStack.SetItemData(cursel,*(DWORD_PTR *)&value);
  wTransformStack.DeleteString(cursel+1);
  _matrixValid=false;
 // GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
  }

void CDlgTexGenCfg::OnEnChangeTexgenValue()
{
  SaveTransform();
  //GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
}

void CDlgTexGenCfg::Serialize(IArchive &arch)
  {
  CString s;
  if (+arch) s=GetUVSource();
  arch("tuvSource",s);
  if (-arch) SetUVSource(s);

  ArchiveSection asect(arch);
  while (asect.Block("transformStack"))
    {
    int cnt=wTransformStack.GetCount();
    arch("count",cnt);
    if (-arch) wTransformStack.ResetContent();
    for (int i=0;i<cnt;i++)
      {
      float val;
      if (+arch) 
        {
        wTransformStack.GetLBText(i,s);
        unsigned long uval=wTransformStack.GetItemData(i);
        val=*(float *)(&uval);
        }
      ArchiveSection asect2(arch);
      arch(s);
      arch(val);
      if (-arch)
        {
        int p=wTransformStack.AddString(s);
        wTransformStack.SetItemData(p,*(unsigned long *)(&val));
        }
      }
    }
  while (asect.Block("aside"))
    {for (int j=0;j<3;j++) arch(_initialMatrix[0][j]);}
  while (asect.Block("up"))
    {for (int j=0;j<3;j++) arch(_initialMatrix[1][j]);}
  while (asect.Block("dir"))
    {for (int j=0;j<3;j++) arch(_initialMatrix[2][j]);}
  while (asect.Block("pos"))
    {for (int j=0;j<3;j++) arch(_initialMatrix[3][j]);}
}
CString CDlgTexGenCfg::GetUVSource()
  {
    return _initUVSource;
  }
void CDlgTexGenCfg::SetUVSource(const char *source)
  {  
  int i=wUVSource.FindStringExact(-1,source);
  if (i==-1) wUVSource.AddString(source);
  wUVSource.SetWindowText(source);
  OnCbnEditupdateTexgenUvsource();
  }

static geMatrix4 LoadTransform(const ParamEntry *matrix)
{
  geMatrix4 out(true);
  const char *names[]={"aside","up","dir","pos"};
  for (int i=0;i<4;i++)
  {
    geMatrixLine4 &values=out[i];
    ParamEntryPtr line=matrix->FindEntry(names[i]);
    if (line.NotNull() && line->IsArray() && line->GetSize()>2)
    {
      for (int j=0;j<3;j++)
        values[j]=(*(line.GetPointer()))[j].GetFloat();
    }
  }
  return out;
}
void CDlgTexGenCfg::LoadTexGenPanel(ParamEntryPtr stageinfo)
{
  if (stageinfo && stageinfo->IsClass())
  {
    ParamEntryPtr temp=stageinfo->FindEntry("uvSource");
    if (temp.NotNull()) _initUVSource=temp->GetValue();  
    temp=stageinfo->FindEntry("uvTransform");
    if (temp.NotNull() && temp->IsClass())
    {
      _resultMatrix=LoadTransform(temp.GetPointer());
    }
  }
}


