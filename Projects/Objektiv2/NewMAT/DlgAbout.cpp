// DlgAbout.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgAbout.h"
#include ".\dlgabout.h"


// DlgAbout dialog

IMPLEMENT_DYNAMIC(DlgAbout, CDialog)
DlgAbout::DlgAbout(CWnd* pParent /*=NULL*/)
	: CDialog(DlgAbout::IDD, pParent)
{
}

DlgAbout::~DlgAbout()
{
}

void DlgAbout::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DlgAbout, CDialog)
  ON_BN_CLICKED(IDC_EMAIL, OnBnClickedEmail)
END_MESSAGE_MAP()


// DlgAbout message handlers

void DlgAbout::OnBnClickedEmail()
  {
  CString s;
  GetDlgItemText(IDC_EMAIL,s);
  s="mailto:"+s+"?subject=Material%20Editor%20bug%20report";
  ShellExecute(*this,NULL,s,NULL,NULL,SW_NORMAL);
  }
