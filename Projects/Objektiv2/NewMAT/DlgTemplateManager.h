#pragma once
#include "afxwin.h"


// DlgTemplateManager dialog

#include "MassProcess.h"

class DlgTemplateManager : public CDialog, public MassProcess
{
	DECLARE_DYNAMIC(DlgTemplateManager)

public:
	DlgTemplateManager(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgTemplateManager();

// Dialog Data
	enum { IDD = IDD_TEMPLATEMANAGER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CComboBox wTemplateName;
  CString vTemplateName;
  virtual BOOL OnInitDialog();
  Pathname vOutPath;

  virtual bool ProcessFile(const char *filename);
  afx_msg void OnBnClickedDeletetemplate();
protected:
  virtual void OnOK();
};
