// DlgTexelMeasure.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "DlgTexelMeasure.h"
#include <colman.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgTexelMeasure dialog

int CDlgTexelMeasure::sc_units=0;;


CDlgTexelMeasure::CDlgTexelMeasure(CWnd* pParent /*=NULL*/)
  : CDialog(CDlgTexelMeasure::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CDlgTexelMeasure)
    m_units = sc_units;
    m_rangeMin = sqrt(1e-8f*GetUnits());
    m_rangeMax = sqrt(10000.0f*GetUnits());
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CDlgTexelMeasure::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CDlgTexelMeasure)
  DDX_Control(pDX, IDC_TEXELINFO, wTexelNfo);
  DDX_Control(pDX, IDC_HISTOGRAM, wHistogram);
  DDX_Text(pDX, IDC_RANGEMIN, m_rangeMin);
  DDV_MinMaxFloat(pDX, m_rangeMin, 1.e-008f, 1.e+038f);
  DDX_Text(pDX, IDC_RANGEMAX, m_rangeMax);
  DDV_MinMaxFloat(pDX, m_rangeMax, 1.e-008f, 1.e+038f);
  DDX_Radio(pDX, IDC_UNITMM, m_units);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CDlgTexelMeasure, CDialog)
  //{{AFX_MSG_MAP(CDlgTexelMeasure)
  ON_BN_CLICKED(IDC_SELONLY, OnSelonly)
    ON_WM_DRAWITEM()
      ON_BN_CLICKED(IDC_RECALCULATERANGE, OnRecalculaterange)
        ON_BN_CLICKED(IDC_UNITMM, OnUnit)
          ON_BN_CLICKED(IDC_RESETRANGE, OnResetrange)
            ON_WM_MOUSEMOVE()
              ON_BN_CLICKED(IDC_UNITCM, OnUnit)
                ON_BN_CLICKED(IDC_UNITM, OnUnit)
                  ON_BN_CLICKED(IDC_SELECTRANGE, OnSelectrange)
                    //}}AFX_MSG_MAP
                    END_MESSAGE_MAP()
                      
                      /////////////////////////////////////////////////////////////////////////////
                      // CDlgTexelMeasure message handlers
                      
                      void CDlgTexelMeasure::CalculateTexelInfo(ObjectData *obj, texelinfo &nfo)
                        {
                        int texwidth,texheight;
                          {
                          Pathname path;
                          path.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
                          path.SetFilename(nfo.texturename); 
                          TUNIPICTURE *pic=cmLoadUni((char *)((LPCTSTR)path));
                          if (pic==NULL)
                            texwidth=texheight=256;
                          else
                            {
                            texwidth=cmGetXSize(pic);
                            texheight=cmGetYSize(pic);
                            }
                          free(pic);
                          }
                        bool first=true;
                        for (int i=0;i<obj->NFaces();i++) if (!nfo.selonly || obj->FaceSelected(i))
                          {
                          FaceT fc(obj,i);
                          ASSERT(fc.N()==3 || fc.N()==4);
                          if (_stricmp(nfo.texturename,fc.GetTexture())==0)
                            {
                            float xx=0;
                            int xxcnt=0;
                            for (int k=0;k<fc.N();k+=3)
                              {
                              PosT ps1,ps2,ps3;
                              VecT da,db,cross;
                              float realvolume;
                              float texvolume;	  
                              
                              ps1=obj->Point(fc.GetPoint(k));
                              ps2=obj->Point(fc.GetPoint(1));
                              ps3=obj->Point(fc.GetPoint(2));
                              da=ps2-ps1;
                              db=ps3-ps1;
                              cross=da.CrossProduct(db);
                              realvolume=cross.Size()*0.5f;
                              
                              ps1[0]=fc.GetU(k)*texwidth;ps1[1]=fc.GetV(k)*texheight;ps1[2]=0.0f;
                              ps2[0]=fc.GetU(1)*texwidth;ps2[1]=fc.GetV(1)*texheight;ps2[2]=0.0f;
                              ps3[0]=fc.GetU(2)*texwidth;ps3[1]=fc.GetV(2)*texheight;ps3[2]=0.0f;
                              da=ps2-ps1;
                              db=ps3-ps1;
                              cross=da.CrossProduct(db);
                              texvolume=cross.Size()*0.5f;
                              
                              float avg=realvolume/texvolume;
                              
                              if (_finite(avg) && avg>=nfo.mintexelsz && avg<=nfo.maxtexelsz)
                                {
                                nfo.count++;
                                nfo.realtotal+=realvolume;
                                if (first) nfo.realmax=nfo.realmin=realvolume;
                                else
                                  if (nfo.realmax<realvolume) nfo.realmax=realvolume;
                                else if (nfo.realmin>realvolume) nfo.realmin=realvolume;
                                
                                
                                nfo.texeltotal+=texvolume;
                                if (first) nfo.texelmax=nfo.texelmin=texvolume;
                                else
                                  if (nfo.texelmax<texvolume) nfo.texelmax=texvolume;
                                else if (nfo.texelmin>texvolume) nfo.texelmin=texvolume;
                                
                                nfo.avgtotal+=avg;
                                if (first) nfo.avgmax=nfo.avgmin=avg;
                                else
                                  if (nfo.avgmax<avg) nfo.avgmax=avg;
                                else if (nfo.avgmin>avg) nfo.avgmin=avg;
                                first=false;
                                xx+=avg;
                                xxcnt++;
                                
                                nfo.AddToHistogram(avg);
                                
                                }
                              else 
                                {xx=-1;xxcnt=1;}
                              nfo.markers[i]=true;
                              }
                            avgs[i]=xx/xxcnt;
                            }
                          }
                        }

//--------------------------------------------------

void CDlgTexelMeasure::FormatTexelInfo(texelinfo &nfo)
  {
  char buff[256];
  float value;
  float sqrvalue;
  
  sprintf(buff,"%.2f",nfo.realtotal*GetUnits());
  wTexelNfo.SetItemText(0,1,buff);
  
  sprintf(buff,"%.2f",nfo.texeltotal);
  wTexelNfo.SetItemText(1,1,buff);
  
  value=nfo.avgmin*GetUnits();sqrvalue=sqrt(value);
  sprintf(buff,"%.2f",value);
  wTexelNfo.SetItemText(3,1,buff);
  sprintf(buff,"(%.1f)",sqrvalue);
  wTexelNfo.SetItemText(4,1,buff);
  
  value=nfo.avgmax*GetUnits();sqrvalue=sqrt(value);
  sprintf(buff,"%.2f",value);
  wTexelNfo.SetItemText(6,1,buff);
  sprintf(buff,"(%.1f)",sqrvalue);
  wTexelNfo.SetItemText(7,1,buff);
  
  value=nfo.avgtotal*GetUnits()/nfo.count;sqrvalue=sqrt(value);
  sprintf(buff,"%.2f",value);
  wTexelNfo.SetItemText(9,1,buff);
  sprintf(buff,"(%.1f)",sqrvalue);
  wTexelNfo.SetItemText(10,1,buff);
  
  //  GetDlgItem(IDC_HISTOGRAM)->Invalidate();
  }

//--------------------------------------------------

void CDlgTexelMeasure::CalcDlg(bool firstphase)
  {
  bool *markers=new bool[source->NFaces()];
  memset(markers,0,source->NFaces()*sizeof(*markers));
  
  memset(histogram,0,sizeof(histogram));
  
  texelinfo sum;
  memset(&sum,0,sizeof(sum));
  memset(avgs,0,sizeof(*avgs)*source->NFaces());
  GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
  GetDlgItem(IDC_RESETRANGE)->EnableWindow(FALSE);
  GetDlgItem(IDC_RECALCULATERANGE)->EnableWindow(FALSE);
  GetDlgItem(IDC_SELECTRANGE)->EnableWindow(FALSE);
  GetDlgItem(IDC_SELONLY)->EnableWindow(FALSE);
  GetDlgItem(IDC_UNITMM)->EnableWindow(FALSE);
  GetDlgItem(IDC_UNITCM)->EnableWindow(FALSE);
  GetDlgItem(IDC_UNITM)->EnableWindow(FALSE);
  
  
  int i;
  
  for (i=0;i<source->NFaces();i++) avgs[i]=-1;
  
  ProgressBar<int> pb(source->NFaces());
  for (i=0;i<source->NFaces();i++) if (!markers[i] && (!selonly || source->FaceSelected(i)))
    {
    pb.SetPos(i);
    texelinfo nfo;
    memset(&nfo,0,sizeof(nfo));
    
    nfo.markers=markers;
    nfo.selonly=selonly;
    nfo.maxtexelsz=m_rangeMax*m_rangeMax/GetUnits();
    nfo.mintexelsz=m_rangeMin*m_rangeMin/GetUnits();
    nfo.histogram=histogram;
    
    FaceT fc(source,i);
    nfo.texturename=fc.GetTexture();
    
    if (nfo.texturename[0])
      {
      CalculateTexelInfo(source,nfo);
      if (sum.count==0) memcpy(&sum,&nfo,sizeof(nfo));
      else if (nfo.count)
        {
        sum.avgtotal+=nfo.avgtotal;
        sum.avgmin=__min(sum.avgmin,nfo.avgmin);
        sum.avgmax=__max(sum.avgmax,nfo.avgmax);
        sum.realtotal+=nfo.realtotal;
        sum.realmin=__min(sum.realmin,nfo.realmin);
        sum.realmax=__max(sum.realmax,nfo.realmax);
        sum.texeltotal+=nfo.texeltotal;
        sum.texelmin=__min(sum.texelmin,nfo.texelmin);
        sum.texelmax=__max(sum.texelmax,nfo.texelmax);
        sum.count+=nfo.count;
        }
      FormatTexelInfo(sum);
      }
    MSG msg;
    while (PeekMessage(&msg,0,0,0,PM_REMOVE)) DispatchMessage(&msg);
    }
  FormatTexelInfo(sum);
  delete [] markers;
  if (firstphase)
    {
    if (sum.avgmin>1e-12f) 
	  {
	  m_rangeMax=sqrt(sum.avgmax*GetUnits());
	  m_rangeMax+=m_rangeMax*.01f;
	  }
    if (sum.avgmax>1e-12f) 
	  {
	  m_rangeMin=sqrt(sum.avgmin*GetUnits());
	  m_rangeMin-=m_rangeMin*.01f;
	  }

    UpdateData(FALSE);
    }
  GetDlgItem(IDC_HISTOGRAM)->Invalidate();
  GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
  GetDlgItem(IDC_RESETRANGE)->EnableWindow(TRUE);
  GetDlgItem(IDC_RECALCULATERANGE)->EnableWindow(TRUE);
  GetDlgItem(IDC_SELECTRANGE)->EnableWindow(TRUE);
  GetDlgItem(IDC_SELONLY)->EnableWindow(TRUE);
  GetDlgItem(IDC_UNITMM)->EnableWindow(TRUE);
  GetDlgItem(IDC_UNITCM)->EnableWindow(TRUE);
  GetDlgItem(IDC_UNITM)->EnableWindow(TRUE);
  
  lastresult=sum;
  }

//--------------------------------------------------

BOOL CDlgTexelMeasure::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  wTexelNfo.InsertColumn(0,WString(IDS_TEXELCOLNAME),LVCFMT_LEFT,100,0);
  wTexelNfo.InsertColumn(1,WString(IDS_TEXELCOLVALUE),LVCFMT_RIGHT,100,0);
  wTexelNfo.InsertItem(0,WString(IDS_TEXELSURFACESIZE),0);
  wTexelNfo.InsertItem(1,WString(IDS_TEXELTOTAL),0);
  wTexelNfo.InsertItem(2,"",0);
  wTexelNfo.InsertItem(3,WString(IDS_TEXELMINTEXELSIZE),0);
  wTexelNfo.InsertItem(4,"",0);
  wTexelNfo.InsertItem(5,"",0);
  wTexelNfo.InsertItem(6,WString(IDS_TEXELMAXTEXELSIZE),0);
  wTexelNfo.InsertItem(7,"",0);
  wTexelNfo.InsertItem(8,"",0);
  wTexelNfo.InsertItem(9,WString(IDS_TEXELAVERAGE),0);
  wTexelNfo.InsertItem(10,"",0);
  
  
  
  selonly=!source->GetSelection()->IsEmpty();
  if (selonly) CheckDlgButton(IDC_SELONLY,1);
  avgs=new float[source->NFaces()];
  ShowWindow(SW_SHOW);
  CalcDlg(true);
  CalcDlg();
  
  selout=NULL;
  
  
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CDlgTexelMeasure::OnSelonly() 
  {
  selonly=!selonly;
  UpdateData(TRUE);
  CalcDlg();
  }

//--------------------------------------------------

void CDlgTexelMeasure::OnOK() 
  {
  OnCancel();  	
  }

//--------------------------------------------------

void CDlgTexelMeasure::OnCancel() 
  {
  delete [] avgs;
  sc_units=m_units;
  CDialog::OnCancel();
  }

//--------------------------------------------------

float CDlgTexelMeasure::GetUnits()
  {
  switch (m_units)
    {
    case 0: return 1000000.0f;
    case 1: return 10000.0f;
    case 2: return 1.0f;
    }
  return 1.0f;
  }

//--------------------------------------------------

static int CalculateStep(int value)
  {
  int ranges[]=
    {1,2,5};
  int mult=1;
  int idx=0;
  int step=1;
  while (value/step>=10)
    {
    idx++;
    if (idx>=3) 
      {idx=0;mult=mult*10;}
    step=ranges[idx]*mult;
    }
  return step;
  }

//--------------------------------------------------

void CDlgTexelMeasure::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
  {
  int maxvalue=0;
  int i;
  int step;
  for (i=0;i<HISTOGRAMPOSIT;i++) if (histogram[i]>maxvalue) maxvalue=histogram[i];
  step=CalculateStep(maxvalue);
  int hsz=lpDrawItemStruct->rcItem.right-lpDrawItemStruct->rcItem.left;
  int vsz=lpDrawItemStruct->rcItem.bottom-lpDrawItemStruct->rcItem.top;
  CDC dc;dc.Attach(lpDrawItemStruct->hDC);
  CPen hlines(PS_DOT,1,GetSysColor(COLOR_GRAYTEXT));
  CPen vlines(PS_SOLID,1,GetSysColor(COLOR_WINDOWTEXT));
  CPen *save=dc.SelectObject(&hlines);
  dc.FillSolidRect(&lpDrawItemStruct->rcItem,GetSysColor(COLOR_WINDOW));  
  dc.SetTextAlign(TA_BOTTOM|TA_RIGHT);
  for (i=0;i<maxvalue;i+=step)
    {
    char buff[32];
    int pos=i*vsz/maxvalue;
    dc.MoveTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.bottom-pos);
    dc.LineTo(lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.bottom-pos);
    _itoa(i,buff,10);
    dc.TextOut(lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.bottom-pos,buff,strlen(buff));
    }
  dc.SelectObject(&vlines);
  if (maxvalue) for (i=0;i<hsz;i++)
    {
    int pos=i*HISTOGRAMPOSIT/hsz;
    int endpoint=histogram[pos]*vsz/maxvalue;
    dc.MoveTo(lpDrawItemStruct->rcItem.left+i,lpDrawItemStruct->rcItem.bottom);
    dc.LineTo(lpDrawItemStruct->rcItem.left+i,lpDrawItemStruct->rcItem.bottom-endpoint);
    }
  dc.SelectObject(save);
  dc.Detach();
  }

//--------------------------------------------------

void CDlgTexelMeasure::OnRecalculaterange() 
  {
  if (UpdateData()==FALSE) return;
  UpdateData(TRUE);
  CalcDlg();
  }

//--------------------------------------------------

void CDlgTexelMeasure::OnUnit() 
  {
  float prev=GetUnits();
  if (UpdateData()==FALSE) return;
  float cur=sqrt(GetUnits()/prev);
  m_rangeMin=m_rangeMin*cur;
  m_rangeMax=m_rangeMax*cur;
  UpdateData(FALSE);
  FormatTexelInfo(lastresult);
  }

//--------------------------------------------------

void CDlgTexelMeasure::OnResetrange() 
  {
  m_rangeMin = sqrt(1e-8f*GetUnits());
  m_rangeMax = sqrt(10000.0f*GetUnits());
  UpdateData(FALSE);
  CalcDlg(true);
  CalcDlg();  
  }

//--------------------------------------------------

void CDlgTexelMeasure::OnMouseMove(UINT nFlags, CPoint point) 
  {
  CWnd *hist=GetDlgItem(IDC_HISTOGRAM);
  CRect rc;
  hist->GetClientRect(&rc);
  hist->MapWindowPoints(this,&rc);
  if (rc.PtInRect(point))
    {
    float f=(float)(point.x-rc.left)/(float)(rc.right-rc.left);
    float curspos=f*(m_rangeMax-m_rangeMin)+m_rangeMin;
    char buff[50];
    sprintf(buff,"%.1f",curspos);
    SetDlgItemText(IDC_CURPOS,buff);
    }
  CDialog::OnMouseMove(nFlags, point);
  }

//--------------------------------------------------

void CDlgTexelMeasure::texelinfo::AddToHistogram(float pos)
  {
  pos=sqrt(pos);
  float min=sqrt(this->mintexelsz);
  float max=sqrt(this->maxtexelsz);
  int hpos=floor(HISTOGRAMPOSIT*(pos-min)/(max-min)+0.5f);
  if (hpos>=0 && hpos<HISTOGRAMPOSIT)
    {
    histogram[hpos]++;
    }
  
  }

//--------------------------------------------------

void CDlgTexelMeasure::OnSelectrange() 
  {
  if (UpdateData()==FALSE) return;
  float rnmin=m_rangeMin*m_rangeMin/GetUnits();
  float rnmax=m_rangeMax*m_rangeMax/GetUnits();
  selout=new Selection(source);
  for (int i=0;i<source->NFaces();i++) 
    if (avgs[i]>=0 && avgs[i]>=rnmin && avgs[i]<=rnmax)
      {
      selout->FaceSelect(i);	
      }
  OnCancel();
  }

//--------------------------------------------------

