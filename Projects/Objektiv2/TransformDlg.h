#if !defined(AFX_TRANSFORMDLG_H__0DC9FEFA_9EE6_4D8D_84C1_CF5EFD78A435__INCLUDED_)
#define AFX_TRANSFORMDLG_H__0DC9FEFA_9EE6_4D8D_84C1_CF5EFD78A435__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TransformDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTransformDlg dialog

class CTransformDlg : public CDialog
  {
  // Construction
  public:
    void DoGismoTranform();
    HVECTOR pin;
    void Save(ostream &str);
    void Load(istream &str);
    void DoTransforms(LODObject *lod);
    int cmd;
    CTransformDlg(CWnd* pParent = NULL);   // standard constructor
    int prwfaze; //0 - no preview, 1 - draw preview, 2 - preview drawed
    int slot;
    
    static BOOL	sav_allAnims[3];
    static BOOL	sav_allLODs[3];
    static BOOL	sav_selOnly[3];
    static float	sav_xValue[3];
    static float	sav_yValue[3];
    static float	sav_zValue[3];
    
    
    // Dialog Data
    //{{AFX_DATA(CTransformDlg)
    enum 
      { IDD = IDD_TRANSFORM };
    BOOL	allAnims;
    BOOL	allLODs;
    BOOL	relToPin;
    BOOL	selOnly;
    float	xValue;
    float	yValue;
    float	zValue;
    BOOL	gismo;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTransformDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void MakeTransformForLod(ObjectData *odata,HMATRIX mm, bool allanims);
    void CalcTransfMatrix(HMATRIX mm,ObjectData *obj);
    
    // Generated message map functions
    //{{AFX_MSG(CTransformDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnPreview();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnChangeValue();
    afx_msg void OnApplytolods();
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRANSFORMDLG_H__0DC9FEFA_9EE6_4D8D_84C1_CF5EFD78A435__INCLUDED_)
