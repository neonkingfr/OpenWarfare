#if !defined(AFX_TEXLIST_H__A81DADC7_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_TEXLIST_H__A81DADC7_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#include "TexPreviewData.h"	// Added by ClassView
#include "ObjectData.h"
#include "TexPrewWin.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TexList.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CTexList dialog

class CTexList : public CDialogBar
  {
  // Construction
  struct texinfo
    {
    char name[128];
    char lib[32];
    void SetKey(const char *n)
      {memset(name,0,sizeof(name));strncpy(name,n,sizeof(name)-1);}
    };
  THASHTABLE hash;
  THASHTABLE model;
  bool needtosave;
  bool preview;
  ObjectData *needupdate;
  int texcnt;
  bool error;
  public:	
    bool scan; //scanning for new texture = DON'T DESTROY THE WINDOW!
    bool needredraw;
    static void ConvertTexture(const char *name, char *outname, int size);
    const char * EnumModelTextures(void **enumerator);
    void UpdateModelTextures(ObjectData *obj, bool delay=true, bool flush=true); //delay=true,flus false is invalid
    //	static void AddPrefix(const char *src, char *buff, int len);
    //	static const char * RemovePrefix(const char *name);
    void GetActiveTextureName(char *name, int size);
    CSize cursize;
    CDIALOGBAR_RESIZEABLE
      void Create(CWnd *parent, int menuid);
    void UpdateLibraryList();
    void UpdateList();
    void CheckForNewTextures();
    void SaveLibrary();
    void LoadLibraryList();
    CTexPreviewData prvtable;
    CTexPrewWin prew;
    char curpath[256];
    CTexList();   // standard constructor
    virtual ~CTexList();
    virtual void OnUpdateCmdUI( CFrameWnd* pTarget, BOOL bDisableIfNoHndler ) 
      {
      pTarget->GetWindowTextLength();  
      }
    
    // Dialog Data
    //{{AFX_DATA(CTexList)
    enum 
      { IDD = IDD_TEXLIST };
    CComboBox	m_LibList;
    CListBox	m_TexList;
    CProgressCtrl m_Progress;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTexList)
  public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CTexList)
    virtual BOOL OnInitDialog();
    afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg void OnPath();
    afx_msg void OnDestroy();
    afx_msg void OnDblclkTexlist();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnSelchangeLibrary();
    afx_msg void OnCut();
    afx_msg void OnPaste();
    afx_msg void OnEditchangeLibrary();
    afx_msg void OnPreview();
    afx_msg void OnTimer(UINT nIDEvent);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXLIST_H__A81DADC7_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
