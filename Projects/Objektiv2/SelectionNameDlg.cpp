// SelectionNameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "SelectionNameDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectionNameDlg dialog


CSelectionNameDlg::CSelectionNameDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CSelectionNameDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CSelectionNameDlg)
    vList = _T("");
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CSelectionNameDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CSelectionNameDlg)
  DDX_Control(pDX, IDC_SELLIST, wList);
  DDX_CBString(pDX, IDC_SELLIST, vList);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CSelectionNameDlg, CDialog)
  //{{AFX_MSG_MAP(CSelectionNameDlg)
  ON_WM_ACTIVATE()
    ON_CBN_DBLCLK(IDC_SELLIST, OnDblclkSellist)
      //}}AFX_MSG_MAP
      END_MESSAGE_MAP()
        
        /////////////////////////////////////////////////////////////////////////////
        // CSelectionNameDlg message handlers
        
        BOOL CSelectionNameDlg::OnInitDialog() 
          {
          CDialog::OnInitDialog();
          LoadSelections();
          exitdlg=false;
          AfxGetMainWnd()->EnableWindow(TRUE);
          SetWindowPos(NULL,showpoint.x,showpoint.y,0,0,SWP_NOZORDER|SWP_NOSIZE);
          
          CRect rc;
          GetWindowRect(&rc);
          HMONITOR mon=MonitorFromRect(&rc,MONITOR_DEFAULTTONULL);
          if (mon)
            {
            MONITORINFO nfo;
            nfo.cbSize=sizeof(MONITORINFO);
            GetMonitorInfo(mon,&nfo);
            if (rc.top<nfo.rcWork.top) rc.top=nfo.rcWork.top;
            if (rc.bottom>nfo.rcWork.bottom) rc.bottom=nfo.rcWork.bottom;
            if (rc.right>nfo.rcWork.right) rc.right=nfo.rcWork.right;
            if (rc.left<nfo.rcWork.left) rc.left=nfo.rcWork.left;
            MoveWindow(&rc,FALSE);
            ScreenToClient(&rc);
            wList.MoveWindow(&rc,FALSE);
            }
          
          return TRUE;  // return TRUE unless you set the focus to a control
          // EXCEPTION: OCX Property Pages should return FALSE
          }

//--------------------------------------------------

void CSelectionNameDlg::LoadSelections()
  {
  int lodcnt=lod->NLevels();
  int i;
  for (i=0;i<lodcnt;i++) if (lod->ActiveLevel()!=i)
    {
    float res=lod->Resolution(i);
    if (res<999.0f)
      {
      ObjectData *obj=lod->Level(i);
      for (int j=0;j<MAX_NAMED_SEL;j++)
        {
        NamedSelection *sel=obj->GetNamedSel(j);
        if (sel!=NULL) 
          {
          int pos=wList.FindStringExact(-1,sel->Name());
          if (pos==-1) wList.AddString(sel->Name());
          }
        }
      }
    }
  }

//--------------------------------------------------

void CSelectionNameDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
  {
  CDialog::OnActivate(nState, pWndOther, bMinimized);
  
  if (nState==WA_INACTIVE && !exitdlg) OnCancel();
  }

//--------------------------------------------------

void CSelectionNameDlg::OnOK()
  {
  if (UpdateData(TRUE)==FALSE) return;
  exitdlg=true;
  CDialog::OnOK();
  }

//--------------------------------------------------

void CSelectionNameDlg::OnCancel()
  {
  exitdlg=true;
  CDialog::OnCancel();
  }

//--------------------------------------------------

void CSelectionNameDlg::OnDblclkSellist() 
  {
  OnOK();	
  }

//--------------------------------------------------

