// LodConfig.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "LodConfig.h"
#include ".\lodconfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLodConfig dialog


CLodConfig::CLodConfig(CWnd* pParent /*=NULL*/)
  : CDialog(CLodConfig::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CLodConfig)
    res = 0.0f;
    //}}AFX_DATA_INIT
    }










void CLodConfig::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_RESOLUTION,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CLodConfig)
  DDX_Control(pDX, IDC_LIST, List);
  DDX_Text(pDX, IDC_RESOLUTION, res);
  //}}AFX_DATA_MAP
  }










BEGIN_MESSAGE_MAP(CLodConfig, CDialog)
  //{{AFX_MSG_MAP(CLodConfig)
  ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
    ON_LBN_DBLCLK(IDC_LIST, OnDblclkList)
      ON_WM_ACTIVATE()
        //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CLodConfig message handlers
    
    BOOL CLodConfig::OnInitDialog() 
      {
      CDialog::OnInitDialog();
      
      int i;
      for(i=IDS_LOD_RES;i<=IDS_LOD_WRECK;i++) if (i!=IDS_LOD_xxx)
        {
        RString w=GetStringRes(i);
        int item=List.AddString(w);
        float f=LODNameToResol(w);
        if (f==0.0) f=-9e10;
        List.SetItemData(item,*(DWORD *)&f);
        }
      if (lod>=LOD_SHADOW_MIN && lod<LOD_SHADOW_MAX)
        {
        List.SelectString(-1,WString(IDS_LOD_SHADOW));
        GetDlgItem(IDC_RESOLUTION)->EnableWindow(TRUE);        
        res=lod-LOD_SHADOW_MIN;
        }
      else if (lod>=LOD_EDIT_MIN && lod<LOD_EDIT_MAX)
        {
        List.SelectString(-1,WString(IDS_LOD_EDITTEMP));
        GetDlgItem(IDC_RESOLUTION)->EnableWindow(TRUE);        
        res=lod-LOD_EDIT_MIN;
        }
      else
        {
        int cnt=List.GetCount();
        for(i=0;i<cnt;i++)
          if (List.GetItemData(i)==*(DWORD *)&lod)
            {
            List.SetCurSel(i);
            break;
            }
        if (i==cnt)
          {
          List.SetCurSel(0);	  
          GetDlgItem(IDC_RESOLUTION)->EnableWindow(TRUE);
          res=lod;
          }
        else
          GetDlgItem(IDC_RESOLUTION)->EnableWindow(FALSE);
        }  
      AfxGetMainWnd()->EnableWindow(TRUE);
      exitdlg=false;
      UpdateData(FALSE);
      return TRUE;  // return TRUE unless you set the focus to a control
      // EXCEPTION: OCX Property Pages should return FALSE
      }










void CLodConfig::OnSelchangeList() 
  {
  int i=List.GetCurSel();
  DWORD fd=List.GetItemData(i);
  float f=*(reinterpret_cast<float *>(&fd));
  GetDlgItem(IDC_RESOLUTION)->EnableWindow(i==0 || f==LOD_SHADOW_MIN || f==LOD_EDIT_MIN);
  }










void CLodConfig::OnOK() 
  {
  int i=List.GetCurSel();
  UpdateData();
  if (i==0) lod=res;
  else 
    {
    DWORD w=List.GetItemData(i);
    lod=*(float *)&w;
    if (lod==10000.0f || lod==20000) lod+=res;    
    }
  exitdlg=true;
  CDialog::OnOK();
  }










void CLodConfig::OnDblclkList() 
  {
  OnOK();	
  }










void CLodConfig::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
  {
  CDialog::OnActivate(nState, pWndOther, bMinimized);
  
  if (nState==WA_INACTIVE && !exitdlg) OnCancel();
  }











  BOOL CLodConfig::PreTranslateMessage(MSG* pMsg)
  {
    if (pMsg->hwnd==List && pMsg->message==WM_LBUTTONDOWN)
    {
      List.SendMessage(pMsg->message,pMsg->wParam,pMsg->lParam);
      OnSelchangeList();
      GetDlgItem(IDC_RESOLUTION)->SetFocus();
      GetDlgItem(IDC_RESOLUTION)->SendMessage(EM_SETSEL,0,-1);
      return TRUE;
    }
 
    return CDialog::PreTranslateMessage(pMsg);
  }
