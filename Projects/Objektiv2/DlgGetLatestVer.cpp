// DlgGetLatestVer.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgGetLatestVer.h"
#include ".\dlggetlatestver.h"


// DlgGetLatestVer dialog

IMPLEMENT_DYNAMIC(DlgGetLatestVer, CDialog)
DlgGetLatestVer::DlgGetLatestVer(CWnd *wndMsg, UINT cmdID,CWnd* pParent /*=NULL*/)
	: CDialog(DlgGetLatestVer::IDD, pParent),
  _cmdID(cmdID),
  _wndMsg(wndMsg)
{
}

DlgGetLatestVer::~DlgGetLatestVer()
{
}

void DlgGetLatestVer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DlgGetLatestVer, CDialog)
  ON_WM_TIMER()
END_MESSAGE_MAP()


// DlgGetLatestVer message handlers

BOOL DlgGetLatestVer::OnInitDialog()
{
  CDialog::OnInitDialog();

  GetDlgItemText(IDOK,_pretext);

  _timeout=10;
  SetTimer(1000,1000,0);


  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgGetLatestVer::OnOK()
{
  // TODO: Add your specialized code here and/or call the base class
  _wndMsg->PostMessage(WM_COMMAND,_cmdID,0);
  OnCancel();
}

void DlgGetLatestVer::OnCancel()
{
  // TODO: Add your specialized code here and/or call the base class
    
  DestroyWindow();
}

void DlgGetLatestVer::OnTimer(UINT nIDEvent)
{
  _timeout--;
  CString tt;
  tt.Format((_pretext+" = %d s").GetString(),_timeout);
  SetDlgItemText(IDOK,tt);
  if (_timeout==0) OnOK();

}

void DlgGetLatestVer::PostNcDestroy()
{
  delete this;
}
