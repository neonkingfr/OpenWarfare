// Config.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "Config.h"
#include <El/Interfaces/iScc.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfig dialog


#define MARK_FOLDER '\\'
#define MARK_BOOLEAN '#'
#define MARK_VALUE '0'
#define MARK_STRING '$'
#define MARK_EXEFILE '%'
#define MARK_COLOR 'C'
#define MARK_VSSFILE 'S'

CConfig::CConfig(CWnd* pParent /*=NULL*/)
  : CDialog(CConfig::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CConfig)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    savept=-1;
    }

//--------------------------------------------------

void CConfig::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CConfig)
  DDX_Control(pDX, IDC_LIST1, List);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CConfig, CDialog)
  //{{AFX_MSG_MAP(CConfig)
  ON_WM_SIZE()
    ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnDblclkList1)
      ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, OnItemchangedList1)
        ON_WM_GETMINMAXINFO()
          ON_EN_CHANGE(IDC_VALUE, OnChangeValue)
            ON_EN_KILLFOCUS(IDC_VALUE, OnKillfocusValue)
              //}}AFX_MSG_MAP
              END_MESSAGE_MAP()
                
                /////////////////////////////////////////////////////////////////////////////
                // CConfig message handlers
                
                CConfig *config;

void CConfig::OnSize(UINT nType, int cx, int cy) 
  {
  CDialog::OnSize(nType, cx, cy);
  CSize size(cx,cy);
  CSize rsize=size-initsize;
  
  CWnd *wnd=GetTopWindow();
  while (wnd!=NULL)
    {
    CRect rc;
    wnd->GetWindowRect(&rc);
    ScreenToClient(&rc);
    if (wnd->GetDlgCtrlID()==IDC_LIST1)
      {
      rc.right+=rsize.cx;
      rc.bottom+=rsize.cy;
      }
    else if (wnd->GetDlgCtrlID()==IDC_VALUE || wnd->GetDlgCtrlID()==IDC_VALUESTATIC )
      {
      rc.top+=rsize.cy;
      rc.bottom+=rsize.cy;
      rc.right+=rsize.cx;
      }
    else
      {
      rc+=rsize;
      }
    wnd->MoveWindow(rc);
    wnd=wnd->GetNextWindow();
    }	
  initsize=size;
  }

//--------------------------------------------------

BOOL CConfig::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  // TODO: Add extra initialization here
  CRect rect;
  GetClientRect(&rect);
  initsize=rect.Size();
  List.InsertColumn(0,WString(IDS_CONFIGTITLE1),LVCFMT_LEFT,150,0);
  List.InsertColumn(1,WString(IDS_CONFIGTITLE2),LVCFMT_LEFT,300,1);
  List.SetImageList(&shrilist,LVSIL_SMALL);
  for (int i=0;i<CFG_VALUESCOUNT;i++)
    {
    #ifndef FULLVER
    if (i==2 || i==7 || i==22 || i==19) continue;
    #endif
    WString s(i+CFG_VALUENAMES);
    int p;
    const char *c=s;
    switch (*c)
      {
      case MARK_FOLDER: p=ILST_FOLDER;break;
      case MARK_BOOLEAN: if (values[i]!="") p=ILST_CHECK;else p=ILST_EMPTY;break;
      case MARK_VALUE: p=ILST_NUMBER;break;
      case MARK_STRING:p=ILST_TEXT;break;
      case MARK_EXEFILE: p=ILST_EXEFILES;break;
      case MARK_COLOR: p=ILST_COLOR;break;
      case MARK_VSSFILE: p=ILST_SSFILES;break;
      }
    LVITEM it;it.mask=LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM;
    it.iItem=i;
    it.iSubItem=0;
    it.pszText=(char *)c+1;
    it.iImage=p;
    it.lParam=i;
    p=List.InsertItem(&it);
    List.SetItemText(p,1,values[i]);	  	  
    }
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CConfig::AskSS(int pt)
  {
      EnableWindow(FALSE);
      theApp._scc.Free();
      theApp.InitScc(true);
      SccFunctions *scc=theApp._scc;
      if (scc)
        {
        const char *localPath=scc->GetLocalPath();
        const char *projectName=scc->GetProjName();
        const char *userName=scc->GetUserName();
        const char *serverName=scc->GetServerName();        
        values[pt].Sprintf("%s*%s*%s*%s",serverName,userName,projectName,localPath);
        }
      else
        values[pt]="";
      EnableWindow(TRUE);
  }

void CConfig::OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  int item=List.GetNextItem(-1,LVNI_FOCUSED);
  int pt;
  if (item==-1) return;
  LVITEM it;
  it.mask=LVIF_PARAM;
  it.iItem=item;
  it.iSubItem=0;
  List.GetItem(&it);
  pt=it.lParam;  
  WString s(CFG_VALUENAMES+pt);
  switch (s[0])
    {
    case MARK_BOOLEAN: 
      {
      WString w(IDS_ON);
      if (values[pt]==w) values[pt]="";else values[pt]=w;
      }
    break;
    case MARK_FOLDER:
      {
      const char *path=::AskForPath(m_hWnd);
      if (path) values[pt]=path;
      }
    break;
    case MARK_EXEFILE:
      {
      const char *c=AskForFile(values[pt]);
      if (c!=NULL) values[pt]=c;
      }
    break;
   case MARK_VSSFILE:
     AskSS(pt);
    break;
    case MARK_COLOR:
      {
      COLORREF col;sscanf(values[pt],"%X",&col);
      CColorDialog cdlg(col,CC_ANYCOLOR|CC_RGBINIT);
      if (cdlg.DoModal()==IDOK)
        {
        values[pt].Format("%08X",cdlg.GetColor());
        }
      }
    break;
    }
  List.SetItemText(item,1,values[pt]);
  if (s[0]==MARK_BOOLEAN)
    {
    it.mask=LVIF_IMAGE;
    it.iImage=values[pt]==""?ILST_EMPTY:ILST_CHECK;
    List.SetItem(&it);
    }
  *pResult = 0;
  }

//--------------------------------------------------

#define Section "CCONFIG"

void CConfig::SaveConfig()
  {
  for (int i=0;i<CFG_VALUESCOUNT;i++)
    theApp.WriteProfileString(Section,WString(CFG_VALUENAMES+i),values[i]);
  }

//--------------------------------------------------

void CConfig::LoadConfig()
  {
  for (int i=0;i<CFG_VALUESCOUNT;i++)
    values[i]=theApp.GetProfileString(Section,WString(CFG_VALUENAMES+i),NULL);  
  }

//--------------------------------------------------

void CConfig::OnOK() 
  {
  UpdateData();
  #ifndef FULLVER
  Pathname path(values[0]);
  values[2]=path.GetDirectoryWithDrive();
  values[IDS_CFGCONVERTGIFTGA-CFG_VALUENAMES]="On";
  #endif
  SaveConfig();
  CDialog::OnOK();
  }

//--------------------------------------------------

void CConfig::OnCancel() 
  {
  LoadConfig();	
  CDialog::OnCancel();
  }

//--------------------------------------------------

void CConfig::OnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  if (savept!=-1) SaveText();
  NMLISTVIEW* pNMListView = (NMLISTVIEW*)pNMHDR;
  if (pNMListView->uNewState & LVIS_FOCUSED)
    {
    int param;
    char type=GetType(pNMListView->iItem,&param);
    if (type==MARK_STRING || type==MARK_VALUE || type==MARK_EXEFILE || type==MARK_FOLDER || type==MARK_COLOR)		
      {
      GetDlgItem(IDC_VALUE)->EnableWindow(TRUE);
      SetDlgItemText(IDC_VALUE, values[param]);
      }
    else
      {
      SetDlgItemText(IDC_VALUE, "");
      GetDlgItem(IDC_VALUE)->EnableWindow(FALSE);
      }
    SendDlgItemMessage(IDC_VALUE,EM_SETSEL,0,-1);
    }
  savept=-1;
  *pResult = 0;
  }

//--------------------------------------------------

char CConfig::GetType(int i, int *param)
  {
  LVITEM it;
  it.mask=LVIF_PARAM;
  it.iItem=i;
  it.iSubItem=0;
  List.GetItem(&it);
  int pt=it.lParam;
  WString s(CFG_VALUENAMES+pt);	  
  if (param) *param=pt;
  return s[0];  
  }

//--------------------------------------------------

void CConfig::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
  {
  
  CDialog::OnGetMinMaxInfo(lpMMI);
  lpMMI->ptMinTrackSize.x=200;
  lpMMI->ptMinTrackSize.y=100;
  }

//--------------------------------------------------

void CConfig::OnChangeValue() 
  {
  savept=List.GetNextItem(-1,LVNI_FOCUSED);
  }

//--------------------------------------------------

void CConfig::SaveText()
  {
  if (savept==-1) return;
  CString s;
  bool error=false;
  GetDlgItemText(IDC_VALUE,s);
  int param;
  long val;
  char c=GetType(savept,&param);
  switch(c)
    {
    case MARK_VSSFILE: 
    case MARK_EXEFILE:
    case MARK_FOLDER: 
    case MARK_STRING: break;
    case MARK_VALUE: error=sscanf(s,"%d",&val)!=1;break;
    case MARK_COLOR: error=sscanf(s,"%x",&val)!=1;break;
    }
  if (error)
    {
    if (AfxMessageBox(IDS_INVALIDVALUE,MB_YESNO)==IDYES)
      {
      GetDlgItem(IDC_VALUE)->SetFocus();
      }
    else
      {
      SetDlgItemText(IDC_VALUE,values[param]);
      }	
    }
  else
    {
    values[param]=s;
    List.SetItemText(savept,1,s);
    }
  savept=-1;
  }

//--------------------------------------------------

void CConfig::OnKillfocusValue() 
  {
  SaveText();	
  }

//--------------------------------------------------

const char * CConfig::AskForFile(const char *prewname,const char *filter)
  {
  if (filter==NULL) filter=StrRes[IDS_EXEFILTER];
  CFileDialogEx fdlg(TRUE,NULL,prewname,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_LONGNAMES,filter,this);
  if (fdlg.DoModal()==IDCANCEL) return NULL;
  static char out[256];
  strncpy(out,fdlg.GetPathName(),sizeof(out));
  return out;
  }

//--------------------------------------------------

bool CConfig::GetBool(int idsname)
  {
  int i=idsname-CFG_VALUENAMES;
  return values[i]!="";
  }

//--------------------------------------------------

int CConfig::GetInt(int idsname, int defval)
  {
  int i=idsname-CFG_VALUENAMES;
  int j=defval;
  sscanf(values[i],"%d",&j);
  return j;
  }

//--------------------------------------------------

const char * CConfig::GetString(int idsname)
  {
  int i=idsname-CFG_VALUENAMES;
  return values[i];
  }

//--------------------------------------------------

unsigned long CConfig::GetHex(int idsname, unsigned long defval)
  {
  int i=idsname-CFG_VALUENAMES;
  unsigned long j=defval;
  sscanf(values[i],"%x",&j);
  return j;
  }

//--------------------------------------------------

// const char *CConfig::FillOpenDialogByPath(int pathid, OPENFILENAME &ofn, const char *deffilename)
//   {
//   int i=pathid-CFG_VALUENAMES;
//   const char *p=strrchr(values[i],'\\');
//   if (p==NULL) values[i] = WString(deffilename);
//   ofn.lpstrInitialDir=values[i];
//   return values[i];
//   }

const char *CConfig::FillOpenDialogByPath(int pathid, OPENFILENAME &ofn, const char *deffilename)
{
  int i=pathid-CFG_VALUENAMES;
  const char *p=strrchr(deffilename,'\\');
  if (p==NULL) p=(const char *)deffilename;
  ofn.lpstrInitialDir=values[i];
  return p;
}

//--------------------------------------------------

void CConfig::SaveCurrPath(int pathid, const char *fname)
  {
  int i=pathid-CFG_VALUENAMES;
  Pathname path(fname);
  values[i]=path.GetDirectoryWithDrive();
  SaveConfig();
  }

//--------------------------------------------------

void CConfig::SetString(int id, const char *str, bool regsave)
  {
  values[id-CFG_VALUENAMES]=str;
  if (regsave) SaveConfig();
  }

//--------------------------------------------------

