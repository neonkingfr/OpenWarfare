// SelectionGroup.cpp: implementation of the CSelectionGroup class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "SelectionGroup.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSelectionGroup::CSelectionGroup()
  {Reset();}

//--------------------------------------------------

CSelectionGroup::~CSelectionGroup()
  {
  
  }

//--------------------------------------------------

void CSelectionGroup::ClearGroup(float level)
  {
  SelGroup *grp=FindLevel(level);
  if (grp)
    {
    grp->level=-1;
    memset(grp->marked,0,sizeof(grp->marked));
    }
  }

//--------------------------------------------------

CSelectionGroup::SelGroup *CSelectionGroup::FindLevel(float level)
  {
  for (int i=0;i<SELGROUPMAX ;i++)
    {
    if (list[i].level==level) return list+i;
    }
  return NULL;
  }

//--------------------------------------------------

int CSelectionGroup::GetGroupHandle(float level)
  {
  SelGroup *grp=FindLevel(level);
  if (grp==NULL) return -1;
  else return grp-list;
  }

//--------------------------------------------------

int	CSelectionGroup::CreateGroupHandle(float level)
  {
  int i;
  for (i=0;i<SELGROUPMAX;i++)
    if (list[i].level<0.0) break;
  if (i==SELGROUPMAX) 
    {i=pos++;if (pos>=SELGROUPMAX) pos=0;}
  list[i].level=level;
  memset(list[i].marked,0,sizeof(list[i].marked));
  return i;
  }

//--------------------------------------------------

void CSelectionGroup::NormalizeSelections(ObjectData *obj, bool *marks, int selid)
  {
  const Selection *sel=obj->GetNamedSel(selid);
  if  (sel==NULL) return;
  int n=obj->NPoints();
  for (int p=0;p<n;p++)
    {
    float w=1.0f-sel->PointWeight(p);
    float suma=0.0f;
    int selcount=0;
    int s;
    for (s=0;s<MAX_NAMED_SEL;s++)
      {
      Selection *qs=const_cast<NamedSelection *>(obj->GetNamedSel(s));
      if (qs==sel) continue;
      if (qs==NULL) continue;
      if (marks[s]==false) continue;
      selcount++;
      suma+=qs->PointWeight(p);
      }
    for (s=0;s<MAX_NAMED_SEL;s++)
      {
      Selection *qs=const_cast<NamedSelection *>(obj->GetNamedSel(s));
      if (qs==sel) continue;
      if (qs==NULL) continue;
      if (marks[s]==false) continue;
      float ths=qs->PointWeight(p);
      float wd;
      if (suma==0.0)	
        wd=w/selcount;
      else		
        wd=(qs->PointWeight(p)/suma)*w;
      suma-=ths;
      selcount--;
      w-=wd;
      qs->SetPointWeight(p,wd);
      }
    }
  }

//--------------------------------------------------

void CSelectionGroup::Reset()
  {
  for (int i=0;i<SELGROUPMAX ;i++)
    {
    list[i].level=-1;
    memset(list[i].marked,0,sizeof(list[i].marked));
    }
  pos=0;
  }

//--------------------------------------------------

