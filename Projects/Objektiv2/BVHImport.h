// BVHImport.h: interface for the CBVHImport class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BVHIMPORT_H__3762D955_8F8F_4ABE_937E_7A8C80C15F02__INCLUDED_)
#define AFX_BVHIMPORT_H__3762D955_8F8F_4ABE_937E_7A8C80C15F02__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

struct SBVHHierarchyItem
  {
  CString name;
  HMATRIX curmx;
  HMATRIX globmx;
  enum t_op 
    {tx,ty,tz,rx,ry,rz};
  t_op tlist[10];
  float tvals[10];
  int tcount;
  HVECTOR offset;
  int parent;
  bool ustate;
  bool term;
  float scale;
  };

//--------------------------------------------------

#define BVHMAXNODES 256

#define BVHFLAGS_ZTOP 1
#define BVHFLAGS_INVERTX 2
#define BVHFLAGS_INVERTY 4
#define BVHFLAGS_INVERTZ 8

typedef int (*BVHMotionCallback)(SBVHHierarchyItem *hlist, int count, int frame, int total, float ftime, void *context, DWORD &flags);

class CBVHImport  
  {
  SBVHHierarchyItem nodes[BVHMAXNODES];
  int nodecnt;
  
  CString rootname;
  
  public:
    void RecalcHierarchy(bool static_phase=true);
    CBVHImport();
    virtual ~CBVHImport();
    
    
    enum LexSymb 
      {lx_HIERARCHY,lx_ROOT,lx_OFFSET,lx_CHANNELS,lx_JOINT,lx_Xposition,lx_Yposition,lx_Zposition,lx_Zrotation,
      lx_Xrotation,lx_Yrotation,lx_MOTION,lx_Frames,lx_Frame,lx_Time,lx_EndSite,lx_EndSiteC,lx_Begin,lx_End,lx_Text,lx_Number,lx_Eof};
    
    struct LexInfo
      {
      const char *text;
      LexSymb value;
      };
    
    char axis[3];		//poradi os "x","y","z" nebo "x","z","y"
    float axisdir[3];  //znaminka os "1" nebo "-1"
    bool ztop; //true znamena, ze osa Z vede nahoru
    
    bool invertx;
    bool inverty;
    bool invertz;
    
    
    void SetSource(istream *in) 
      {data=in;}
    void SetCallback(BVHMotionCallback ccb, void *context) 
      {callback=ccb;this->ccontext=context;}
    
    int Parse(int *errline);
    
    static char *GetError(int code);
    BVHMotionCallback callback;
    void *ccontext;
  private:
    
    int Frames;
    float FrameTime;
    
    struct SymbInfo
      {
      LexSymb type;
      CString text;
      float number;
      };
    
    SymbInfo symb;
    istream *data;
    
    
    
    void AddRotation(char osa, float value, HMATRIX mx);
    void AddTranslate(char osa, float value, HMATRIX mx);
    int ReadHierarchyItem(int parent,bool term=false);
    int ReadHierarchy();
    int ReadFile();
    void ReadNext();
    void ReadTillPar();
    int ReadOffset(SBVHHierarchyItem &itm);
    int ReadChannels(SBVHHierarchyItem &itm);
    
    int ReadMotion();
  };

//--------------------------------------------------

int BVHImportModel(SBVHHierarchyItem *hlist, int count, int frame, int total, float ftime, void *context, DWORD &flags);

#endif // !defined(AFX_BVHIMPORT_H__3762D955_8F8F_4ABE_937E_7A8C80C15F02__INCLUDED_)
