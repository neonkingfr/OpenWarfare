#if !defined(AFX_UNWRAPDLG_H__F66F0AC7_91A1_441C_B205_CA6244E2F2B2__INCLUDED_)
#define AFX_UNWRAPDLG_H__F66F0AC7_91A1_441C_B205_CA6244E2F2B2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UnWrapDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUnWrapDlg dialog

class CUnWrapDlg : public CDialog
  {
  // Construction
  int forcepreview;
  public:
    void Calc();
    void UpdateValues();
    CUnWrapDlg(CWnd* pParent = NULL);   // standard constructor
    ObjectData *obj;
    // Dialog Data
    //{{AFX_DATA(CUnWrapDlg)
    enum 
      { IDD = IDD_UNWRAP };
    CEdit	tvoffsetval;
    CEdit	tuoffsetval;
    CSliderCtrl	tvoffsetctrl;
    CSliderCtrl	tuoffsetctrl;
    int		tuoffset;
    int		tvoffset;
    BOOL	tvweight;
    BOOL	tuweight;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CUnWrapDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CUnWrapDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnTimer(UINT nIDEvent);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNWRAPDLG_H__F66F0AC7_91A1_441C_B205_CA6244E2F2B2__INCLUDED_)
