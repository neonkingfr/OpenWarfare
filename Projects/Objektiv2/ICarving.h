// ICarving.h: interface for the CICarving class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ICARVING_H__48DAF4C0_53D1_4FF6_A279_DB87D6379493__INCLUDED_)
#define AFX_ICARVING_H__48DAF4C0_53D1_4FF6_A279_DB87D6379493__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CICarving : public CICommand  
{
public:
	CICarving();
	virtual ~CICarving();
    int Tag() {return CITAG_CARVING;}
    void Save(ostream &str) {}
    int Execute(LODObject *obj);
    CICarving(istream &str) {}


};

#endif // !defined(AFX_ICARVING_H__48DAF4C0_53D1_4FF6_A279_DB87D6379493__INCLUDED_)
