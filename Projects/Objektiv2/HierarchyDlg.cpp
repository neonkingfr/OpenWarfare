// HierarchyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "HierarchyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHierarchyDlg dialog


CHierarchyDlg::CHierarchyDlg()
  : CDialogBar()
    {
    //{{AFX_DATA_INIT(CHierarchyDlg)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    //{{AFX_DATA_MAP(CHierarchyDlg)
    // NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CHierarchyDlg, CDialogBar)
  //{{AFX_MSG_MAP(CHierarchyDlg)
  ON_WM_SIZE()
    ON_WM_DESTROY()
      ON_WM_CONTEXTMENU()
        //}}AFX_MSG_MAP
        END_MESSAGE_MAP()
          
          /////////////////////////////////////////////////////////////////////////////
          // CHierarchyDlg message handlers
          
          void CHierarchyDlg::Create(CWnd *parent, int menuid)
            {
            CDialogBar::Create(parent,IDD_HIERARCHY,CBRS_LEFT,menuid);
            SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
            EnableDocking(CBRS_ALIGN_ANY);
            CString top;top.LoadString(IDS_HIERARCHY);
            SetWindowText(top);
            notify=parent;
            htree.Attach(::GetDlgItem(m_hWnd,IDC_HTREE));
            htree.SetImageList(&shrilist,LVSIL_SMALL);
            CRect rect;GetClientRect(&rect);
            LoadBarSize(*this,160,0);
            }

//--------------------------------------------------

void CHierarchyDlg::OnSize(UINT nType, int cx, int cy) 
  {
  if ((HWND)htree)htree.SetWindowPos(NULL,5,5,cx-10,cy-10,SWP_NOZORDER);
  }

//--------------------------------------------------

void CHierarchyDlg::OnDestroy() 
  {
  SaveBarSize(*this);	
  htree.Detach();
  CDialogBar::OnDestroy();
  }

//--------------------------------------------------

void CHierarchyDlg::OnContextMenu(CWnd* pWnd, CPoint point) 
  {
  CMenu popup;	
  
  LoadPopupMenu(IDR_HIERARCHY_POPUP,popup);
  HTREEITEM it=htree.GetSelectedItem();
  if (it==NULL)
    {
    popup.EnableMenuItem(ID_HIERARCHY_DELETE,MF_GRAYED);
    popup.EnableMenuItem(ID_HIERARCHY_COMBINE,MF_GRAYED);	
    }
  }

//--------------------------------------------------

