// 3DC.cpp: implementation of the C3DC class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "3DC.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

typedef BOOL (PASCAL *GradientFill_Type)(HDC hdc, CONST PTRIVERTEX pVertex, DWORD dwNumVertex, CONST PVOID pMesh, DWORD dwNumMesh, DWORD dwMode);

#define EMPTY_GRF (GradientFill_Type)1
static GradientFill_Type _GradientFill=EMPTY_GRF;



C3DC::C3DC()
{
  JednotkovaMatice(transform);
  oldbrush=NULL;oldpen=NULL;oldbitmap=NULL;oldfont=NULL;
  soft=NULL;
  sharp=NULL;
  softsharp=true;
}

C3DC::~C3DC()
{
  if (m_hDC!=NULL)
  {
    if (oldbrush) CDC::SelectObject(CBrush::FromHandle(oldbrush));
    if (oldpen) CDC::SelectObject(CPen::FromHandle(oldpen));
    if (oldfont) CDC::SelectObject(CFont::FromHandle(oldfont));
    if (oldbitmap) CDC::SelectObject(CBitmap::FromHandle(oldbitmap));
  }
}

void C3DC::SelectObject(CPen *pen)
{
  if (oldpen==NULL) oldpen=(HPEN)*CDC::SelectObject(pen);
  else CDC::SelectObject(pen);
}

void C3DC::SelectObject(CBrush *brush)
{
  if (oldbrush==NULL) oldbrush=(HBRUSH)*CDC::SelectObject(brush);
  else CDC::SelectObject(brush);
}

CFont* C3DC::SelectObject(CFont *font)
{
  CFont *ret=CDC::SelectObject(font);
  if (oldfont==NULL) oldfont=(HFONT)*ret;
  return ret;
}

void C3DC::SelectObject(CBitmap *bitmap)
{
  if (oldbitmap==NULL) oldbitmap=(HBITMAP)*CDC::SelectObject(bitmap);
  else CDC::SelectObject(bitmap);
}

void C3DC::ClipLine(HVECTOR tv1, HVECTOR tv2)
{
  float x1,y1,z1,w1,x2,y2,z2,w2;
  
  w1=1.0f/tv1[WVAL];
  x1=tv1[XVAL]*w1;
  y1=tv1[YVAL]*w1;
  z1=tv1[ZVAL]*w1;
  w2=1.0f/tv2[WVAL];
  x2=tv2[XVAL]*w2;
  y2=tv2[YVAL]*w2;
  z2=tv2[ZVAL]*w2;
  float f=(z2)/(z2-z1);
  tv1[XVAL]=x2+(x1-x2)*f;
  tv1[YVAL]=y2+(y1-y2)*f;
  tv1[ZVAL]=z2+(z1-z2)*f;
  tv1[WVAL]=w2+(w1-w2)*f;
  XYZW2XYZQ(tv1);
}

#define CLP_TOP 0x1
#define CLP_BOTTOM 0x2
#define CLP_LEFT 0x4
#define CLP_RIGHT 0x8

char C3DC::GetClipFlags(HVECTOR tv1, RECT& rc)
{
  char clip=0;
  if (tv1[XVAL]<(float)rc.left) clip|=CLP_LEFT;
  if (tv1[YVAL]<(float)rc.top) clip|=CLP_TOP;
  if (tv1[YVAL]>(float)rc.bottom) clip|=CLP_BOTTOM;
  if (tv1[XVAL]>(float)rc.right) clip|=CLP_RIGHT;
  return clip;
}

bool C3DC::Clip(HVECTOR tv1, HVECTOR tv2, RECT& rc)
{
  char flg1=GetClipFlags(tv1,rc);
  char flg2=GetClipFlags(tv2,rc);
  if (flg1 & flg2) return false;
  if (flg1==0 && flg2==0) return true;
  float f,g=1.0f;
  if (flg1 & CLP_LEFT) 
  {
    f=(tv2[XVAL]-(float)rc.left)/(tv2[XVAL]-tv1[XVAL]);
    g=__min(f,g);
  }
  if (flg1 & CLP_RIGHT) 
  {
    f=(tv2[XVAL]-(float)rc.right)/(tv2[XVAL]-tv1[XVAL]);
    g=__min(f,g);
  }
  if (flg1 & CLP_BOTTOM) 
  {
    f=(tv2[YVAL]-(float)rc.bottom)/(tv2[YVAL]-tv1[YVAL]);
    g=__min(f,g);
  }
  if (flg1 & CLP_TOP) 
  {
    f=(tv2[YVAL]-(float)rc.top)/(tv2[YVAL]-tv1[YVAL]);
    g=__min(f,g);
  }
  tv1[XVAL]=tv2[XVAL]+(tv1[XVAL]-tv2[XVAL])*g;
  tv1[YVAL]=tv2[YVAL]+(tv1[YVAL]-tv2[YVAL])*g;
  flg1=GetClipFlags(tv1,rc);
  if (flg1 & flg2) return false;
  g=1.0f;
  if (flg2 & CLP_LEFT) 
  {
    f=(tv1[XVAL]-(float)rc.left)/(tv1[XVAL]-tv2[XVAL]);
    g=__min(f,g);
  }
  if (flg2 & CLP_RIGHT) 
  {
    f=(tv1[XVAL]-(float)rc.right)/(tv1[XVAL]-tv2[XVAL]);
    g=__min(f,g);
  }
  if (flg2 & CLP_BOTTOM) 
  {
    f=(tv1[YVAL]-(float)rc.bottom)/(tv1[YVAL]-tv2[YVAL]);
    g=__min(f,g);
  }
  if (flg2 & CLP_TOP) 
  {
    f=(tv1[YVAL]-(float)rc.top)/(tv1[YVAL]-tv2[YVAL]);
    g=__min(f,g);
  }
  tv2[XVAL]=tv1[XVAL]+(tv2[XVAL]-tv1[XVAL])*g;
  tv2[YVAL]=tv1[YVAL]+(tv2[YVAL]-tv1[YVAL])*g;
  return true;
}

void C3DC::Line3D(HVECTOR v1, HVECTOR v2)
{
  HVECTOR tv1,tv2;
  TransformVector(transform,v1,tv1);
  TransformVector(transform,v2,tv2);
  XYZW2XYZQ(tv1);
  XYZW2XYZQ(tv2);
  Line3DClipped(tv1,tv2);
}

void C3DC::MoveTo3D(float x, float y, float z)
{
  HVECTOR v;
  v[XVAL]=x;
  v[YVAL]=y;
  v[ZVAL]=z;
  v[WVAL]=1.0f;
  TransformVector(transform,v,oldpos);
  XYZW2XYZQ(oldpos);
}

void C3DC::MoveTo3D(HVECTOR v)
{
  TransformVector(transform,v,oldpos);
  XYZW2XYZQ(oldpos);
}

void C3DC::Line3DClipped(HVECTOR tv1, HVECTOR tv2)
{
  if (softsharp) 
    if (flags & 1) SelectObject(soft);else SelectObject(sharp);
  bool cp1=(tv1[WVAL]<0.0f || tv1[ZVAL]<0.0f);
  bool cp2=(tv2[WVAL]<0.0f || tv2[ZVAL]<0.0f);
  if (!cp1 || !cp2)
  {
    HVECTOR ctv1,ctv2;
    CopyVektor(ctv1,tv1);
    CopyVektor(ctv2,tv2);
    if (cp1) ClipLine(ctv1,ctv2);
    if (cp2) ClipLine(ctv2,ctv1);
    if (Clip(ctv1,ctv2,cliprect))
    {
      MoveTo(ToInt(ctv1[XVAL]),ToInt(ctv1[YVAL])); 
      LineTo(ToInt(ctv2[XVAL]),ToInt(ctv2[YVAL])); 
    }
  }
}

void C3DC::LineTo3D(HVECTOR v)
{
  HVECTOR tv;
  TransformVector(transform,v,tv);
  XYZW2XYZQ(tv);
  Line3DClipped(oldpos,tv);
  MoveToT(tv);
  softsharp=false;
}

void C3DC::LineTo3D(float x, float y, float z) 
{
  HVECTOR v;
  HVECTOR tv;
  v[XVAL]=x;
  v[YVAL]=y;
  v[ZVAL]=z;
  v[WVAL]=1.0f;
  TransformVector(transform,v,tv);
  XYZW2XYZQ(tv);
  Line3DClipped(oldpos,tv);
  MoveToT(tv);
  softsharp=false;
}

void C3DC::BeginStripFan(HVECTOR v)
{
  TransformVector(transform,v,oldpos2);
  XYZW2XYZQ(oldpos2);  
  stripswapper=false;
}

int C3DC::GetCull(HVECTOR v1, HVECTOR v2, HVECTOR v3)
{
  float x1=v1[XVAL]-v2[XVAL];
  float y1=v1[YVAL]-v2[YVAL];
  float x2=v1[XVAL]-v3[XVAL];
  float y2=v1[YVAL]-v3[YVAL];
  float cull=(x1*y2)-(y1*x2);
  return (cull>0)-(cull<0);
}

void C3DC::WiredStrip(HVECTOR v)
{
  HVECTOR tv;
  TransformVector(transform,v,tv);
  XYZW2XYZQ(tv);
  Triangle(oldpos,oldpos2,v);
  if (stripswapper)
    CopyVektor(oldpos2,tv);
  else
    MoveToT(tv);
  stripswapper=!stripswapper;
}

void C3DC::Triangle(HVECTOR v1, HVECTOR v2, HVECTOR v3)
{
  if (curcull && GetCull(v1,v2,v3)==curcull) return;
  Line3DClipped(v1,v2); flags>>=1;
  Line3DClipped(v2,v3); flags>>=1;
  Line3DClipped(v3,v1); flags>>=1;
  softsharp=false;
}

void C3DC::WiredFan(HVECTOR v)
{
  HVECTOR tv;
  TransformVector(transform,v,tv);
  XYZW2XYZQ(tv);
  Triangle(oldpos,oldpos2,tv);
  CopyVektor(oldpos2,tv);
}

void C3DC::Quad3D(HVECTOR v3, HVECTOR v4)
{
  HVECTOR tv1,tv2;
  TransformVector(transform,v3,tv1);
  TransformVector(transform,v4,tv2);
  XYZW2XYZQ(tv1);
  XYZW2XYZQ(tv2);
  if (curcull && GetCull(oldpos,oldpos2,tv1)==curcull) return;
  Line3DClipped(oldpos,oldpos2);flags>>=1;
  Line3DClipped(oldpos2,tv1); flags>>=1;
  Line3DClipped(tv1,tv2); flags>>=1;
  Line3DClipped(tv2,oldpos); flags>>=1;
  softsharp=false;
}

void C3DC::Point3D(LPHVECTOR v,int type, unsigned long size)
{
  POINT pt;
  HVECTOR tv;
  if (v==NULL)
  {
    if (oldpos[ZVAL]<0||oldpos[WVAL]<0) return;
    pt.x=ToInt(oldpos[XVAL]);
    pt.y=ToInt(oldpos[YVAL]);
  }
  else
  {
    TransformVector(transform,v,tv);
    XYZW2XYZQ(tv);
    pt.x=ToInt(tv[XVAL]);
    pt.y=ToInt(tv[YVAL]);
    if (tv[ZVAL]<0||tv[WVAL]<0) return;
  }
  if (cliprect.PtInRect(pt))
  {
    if (type==C3DCPT_DEFAULT) 
    {
      MoveTo(pt);
      pt.x++;
      pt.y++;
      LineTo(pt);
    }
    else
    {
      RECT rc;
      rc.left=pt.x-size;
      rc.top=pt.y-size;
      rc.right=pt.x+size;
      rc.bottom=pt.y+size;
      switch (type)
      {
        case C3DCPT_RECTANGLE:Rectangle(&rc);break;
        case C3DCPT_CROSS: MoveTo(rc.left,pt.y); LineTo(rc.right,pt.y);
        MoveTo(pt.x,rc.top); LineTo(pt.x,rc.bottom);
        break;
        case C3DCPT_XCROSS:MoveTo(rc.left,rc.top); LineTo(rc.right,rc.bottom);
        MoveTo(rc.right,rc.top); LineTo(rc.left,rc.bottom);
        break;
        case C3DCPT_CIRCLE:Ellipse(&rc);break;
        case C3DCPT_CHARACTER:
        TextOut(pt.x,pt.y,(char *)&size,1);
        break;
        case C3DCPT_STRING:if (size)
          TextOut(pt.x,pt.y,(char *)size,1);
        break;
        
      }
    }
  }
}

bool C3DC::TestTriangle(HVECTOR pt1, HVECTOR pt2, HVECTOR pt3, HVECTOR pt)
{
  int a=TestLinePt(pt1,pt2,pt);
  int b=TestLinePt(pt2,pt3,pt);
  int c=TestLinePt(pt3,pt1,pt);
  return (a==b && b==c);
}

int C3DC::TestLinePt(HVECTOR pt1, HVECTOR pt2, HVECTOR pt)
{
  float a,b,c;
  HVECTOR pt3;
  CopyVektor(pt3,pt2);
  RozdilVektoru(pt3,pt1);
  a=pt3[YVAL];
  b=-pt3[XVAL];
  c=-(a*pt1[XVAL]+b*pt1[YVAL]);
  float e=a*pt[XVAL]+b*pt[YVAL]+c;
  return (e>0)-(e<0);
}

LPHVECTOR calcFacePlane(HVECTOR v1, HVECTOR v2, HVECTOR v3)
{
  HVECTOR va,vb;
  static HVECTOR vout;
  va[XVAL]=v2[XVAL]-v1[XVAL];
  va[YVAL]=v2[YVAL]-v1[YVAL];
  va[ZVAL]=v2[ZVAL]-v1[ZVAL];
  vb[XVAL]=v2[XVAL]-v3[XVAL];
  vb[YVAL]=v2[YVAL]-v3[YVAL];
  vb[ZVAL]=v2[ZVAL]-v3[ZVAL];
  VektorSoucin(va,vb,vout);
  vout[WVAL]=-(vout[XVAL]*v1[XVAL]+vout[YVAL]*v1[YVAL]+vout[ZVAL]*v1[ZVAL]);
  return vout;
}

int C3DC::FullTestTriangle(HVECTOR v1, HVECTOR v2, HVECTOR v3, HVECTOR pt, LPHVECTOR plane)
{  
  if (TestTriangle(v1,v2,v3,pt)==false) return 0;
  if (plane==C3DCPT_FASTTEST) return 1;
  if (plane==C3DCPT_CALCNORMAL) plane=calcFacePlane(v1,v2,v3);
  float e=-(plane[XVAL]*pt[XVAL]+plane[YVAL]*pt[YVAL]+plane[WVAL])/plane[ZVAL];
  lasttestdistance=e;
  if (fabs(e-pt[ZVAL])<0.00001) return 0;
  return 2*(e<pt[ZVAL])-1;
}

bool C3DC::PtTestStrip(HVECTOR v, HVECTOR pt, int cull)
{
  bool out;
  HVECTOR tv;
  TransformVector(transform,v,tv);
  XYZW2XYZQ(tv);
  int res=FullTestTriangle(oldpos,oldpos2,tv,pt,C3DCPT_CALCNORMAL);
  if (res==0) out=false;
  else
    if (res==cull || cull==0) out=true;
  if (stripswapper)
    CopyVektor(oldpos2,tv);
  else
    MoveToT(tv);
  stripswapper=!stripswapper;
  return out;
}

void C3DC::ClipRectFromBitmap(CBitmap & bmp)
{
  BITMAP bi;
  
  bmp.GetBitmap(&bi);
  cliprect.left=0;
  cliprect.top=0;
  cliprect.right=bi.bmWidth-1;
  cliprect.bottom=bi.bmHeight-1;  
}

void C3DC::ClearCanvas(COLORREF col)
{
  FillSolidRect(cliprect,col);
}

void C3DC::FilledQuad(int npt, HMATRIX mm, COLORREF ref)
{
  POINT pt[4];
  CDC::SelectStockObject(NULL_PEN);
  for (int i=0;i<npt;i++)
  {
    if (mm[i][WVAL]<=0.0f) return;	
    pt[i].x=ToInt(mm[i][XVAL]);
    pt[i].y=ToInt(mm[i][YVAL]);
    if (pt[i].x<-0x7FFF) pt[i].x=-0x7FFF;
    if (pt[i].x>0x7FFF) pt[i].x=0x7FFF;
    if (pt[i].y<-0x7FFF) pt[i].y=-0x7FFF;
    if (pt[i].y>0x7FFF) pt[i].y=0x7FFF;
  }
  CBrush brsh(ref);
  CBrush *old=CDC::SelectObject(&brsh);
  Polygon(pt,npt);
  SelectObject(old);
}

static void FilledTriangle(CDC& dc,TRIVERTEX vert [4] , int pt1, int pt2, int pt3)
{
  GRADIENT_TRIANGLE    gTri;
  
  gTri.Vertex1   = pt1;
  gTri.Vertex2   = pt2;
  gTri.Vertex3   = pt3;
  if (_GradientFill==EMPTY_GRF)
  {
    HMODULE mod=LoadLibrary("msimg32.dll");
    _GradientFill=(GradientFill_Type)GetProcAddress(mod,"GradientFill");
  }
  if (_GradientFill) _GradientFill(dc,vert,4,&gTri,1,GRADIENT_FILL_TRIANGLE);
}

void C3DC::FilledQuadGuard(int pt, HMATRIX mm, COLORREF *ref, bool wires)
{
  TRIVERTEX vert [4];
  for (int i=0;i<pt;i++)
  {
    if (mm[i][WVAL]<=0.0f) return;	
    if (mm[i][XVAL]<-32000.0f) mm[i][XVAL]=-32000.0f;
    if (mm[i][XVAL]>32000.0f) mm[i][XVAL]=32000.0f;
    if (mm[i][YVAL]<-32000.0f) mm[i][YVAL]=-32000.0f;
    if (mm[i][YVAL]>32000.0f) mm[i][YVAL]=32000.0f;
    vert [i] .x       =  ToInt(mm[i][XVAL]);
    vert [i] .y       =  ToInt(mm[i][YVAL]);
    vert [i] .Red     =  GetRValue(ref[i])<<8;
    vert [i] .Green   =  GetGValue(ref[i])<<8;
    vert [i] .Blue    =  GetBValue(ref[i])<<8;
    vert [i] .Alpha   =  0xFF50;
  }
  FilledTriangle(*this,vert,0,1,2);
  if (pt==4) FilledTriangle(*this,vert,0,2,3);
  if (wires)
    {
    POINT ptarr[4];
    ptarr[0].x=vert[0].x;ptarr[0].y=vert[0].y;
    ptarr[1].x=vert[1].x;ptarr[1].y=vert[1].y;
    ptarr[2].x=vert[2].x;ptarr[2].y=vert[2].y;
    ptarr[3].x=vert[3].x;ptarr[3].y=vert[3].y;
    Polyline(ptarr,pt);
    }
}

inline COLORREF CalculateLightColor(COLORREF refcolor, LPHVECTOR normal, LPHVECTOR dirlig, LPHVECTOR half)
{

  unsigned char *cc=(unsigned char *)&refcolor;
  float cosf=Scalar1(normal,dirlig);
  float specf=Scalar1(normal,half);
  if (cosf<0) cosf=0;
  cosf=cosf*0.5f+0.5f;
  if (specf<0) specf=0;
  specf*=specf;
  specf*=specf;
  specf*=specf;
  specf*=specf;
  specf*=0.8f;
  int col=ToInt(cosf*255.0f);
  int specular=ToInt(specf*255.0f);
  register unsigned char r=cc[0]*col>>8;
  register unsigned char g=cc[1]*col>>8;
  register unsigned char b=cc[2]*col>>8;
  r=r+(((255-r)*specular)>>8);
  g=g+(((255-g)*specular)>>8);
  b=b+(((255-b)*specular)>>8);  
  return RGB(r,g,b);
}

void CFaceSortList::AddFace(int npts, HMATRIX points, COLORREF refcol, LPHMATRIX normals)
{
  LPHVECTOR plane=calcFacePlane(points[0], points[1], points[2]);  
  LPHMATRIX mx=adc->GetMatrix();
  NormalizeVector(plane);
  unsigned char *cc=(unsigned char *)&refcol;
  float cosf=-Scalar1(plane,lightdir);
  if (cosf<0) cosf=0;
  cosf=cosf*0.5f+0.5f;
  int col=ToInt(cosf*255.0f);
  int cnt=Size();
  SFaceSortInfo &face=Append();
  for (int i=0;i<npts;i++)
  {
    TransformVector(mx,points[i],face.points[i]);
    XYZW2XYZQ(face.points[i]);
    float z=face.points[i][ZVAL];
    if (face.points[i][WVAL]<0.0f) z=0.0f;
    if (i==0)
    {
      face.minz=face.maxz=z;	  
    }
    else
    {
      if (z<face.minz) face.minz=z;
      else if (z>face.maxz) face.maxz=z;
    }
    if (normals) face.normrf[i]=CalculateLightColor(refcol,normals[i],lightdir,halfdir);
  }
  face.minz=(face.maxz+face.minz)*0.5f;
  face.color=RGB(cc[0]*col>>8,cc[1]*col>>8,cc[2]*col>>8);
  face.n=npts;
  face.dontchangeflag=-1;
  face.lasttested=-1;  
  CopyVektor(face.plane,calcFacePlane(face.points[0], face.points[1], face.points[2]));
  if (face.plane[ZVAL]<0.0) 	
    Delete(cnt);
}

static char TestSide(LPHVECTOR plane, LPHVECTOR point, char &side)
  {
  float res=plane[3]+point[0]*plane[0]+point[1]*plane[1]+point[2]*plane[2];  
  char crs=(res>0)-(res<0);
  if (crs==side) return 1;
  if (side==0) {side=crs;return 1;}
  return 0;
  }

static int _cdecl sortfaces1(const void *lp1, const void *lp2)
  {
  SFaceSortInfo *face1=*(SFaceSortInfo **)lp1;
  SFaceSortInfo *face2=*(SFaceSortInfo **)lp2;  
  return face1->minz>face2->minz?-1:1;
  }


static int sortfaces2(const void *lp1, const void *lp2)
{
  SFaceSortInfo *face1=*(SFaceSortInfo **)lp1;
  SFaceSortInfo *face2=*(SFaceSortInfo **)lp2;  
  char side=0;
  if (TestSide(face1->plane,face2->points[0],side) &&
    TestSide(face1->plane,face2->points[1],side)&&
    TestSide(face1->plane,face2->points[2],side)) return side;
  else
   side=0;
  if (TestSide(face2->plane,face1->points[0],side) &&
    TestSide(face2->plane,face1->points[1],side) &&
    TestSide(face2->plane,face1->points[2],side)) return -side;
  return (face1->dontchangeflag>face1->dontchangeflag)?1:-1;
}

bool CFaceSortList::Prepare()
{
  SFaceSortInfo **nw;
  int cnt=Size();
  nw=(SFaceSortInfo **)realloc(sortedptrs,sizeof(SFaceSortInfo *)*cnt);
  if (nw==NULL) return false;
  sortedptrs=nw;
  int i;
  for (i=0;i<cnt;i++) nw[i]=&(*this)[i];
  qsort(nw,cnt,sizeof(SFaceSortInfo *),sortfaces1);
  for (i=0;i<cnt;i++) nw[i]->dontchangeflag=0;
  for (i=0;i<cnt-1;i++)
    {
    int c=sortfaces2(nw+i,nw+i+1);
    if (c>0)
      {
      int j=i;
      while (c>0) 
        {
        SFaceSortInfo *tmp=nw[j];
        nw[j]=nw[j+1];
        nw[j+1]=tmp;
        j--; 
        if (j<0) break;
        c=sortfaces2(nw+j,nw+j+1);
        }
      }
    }
  //  ApplyFaceSort();
  return true;
}

void CFaceSortList::ApplyFaceSort()
{
  int cnt=Size();
  int down=0;
  while (down<cnt)
  {
    opakuj:
    SFaceSortInfo *df=sortedptrs[down];	
    int trc;
    if (df->lasttested<=down) trc=down+1;else trc=df->lasttested+1;
    while (trc<cnt && df->minz<sortedptrs[trc]->maxz)
    {	  
      SFaceSortInfo *tf=sortedptrs[trc];
      int tst=0;
      int p;
      ;	  float d;
      float max=0;
      bool chngflag=false;
      for (int i=0;i<tf->n;i++)
      {
        float z=tf->points[i][ZVAL];
        float e=-(df->plane[XVAL]*tf->points[i][XVAL]+
          df->plane[YVAL]*tf->points[i][YVAL]+
            df->plane[WVAL])/df->plane[ZVAL];	    
        
        p=(e>z)-(z<e);
        if (i) 
        {if (tst!=p) tst=0;}else tst=i;
      }
      if (tst<=0) 
      {
        tst=0;
        max=0;
        for (int i=0;i<df->n;i++)
        { 
          float z=df->points[i][ZVAL];
          float e=-(tf->plane[XVAL]*df->points[i][XVAL]+
            tf->plane[YVAL]*df->points[i][YVAL]+
              tf->plane[WVAL])/tf->plane[ZVAL];	    		  
          d=fabs(e-z);
          if (d>max) 
          {tst=(e>z)-(e<z);max=d;}
        }		
        if (tst>0) chngflag=true;
      }
      if (chngflag)
      {
        if (tf->dontchangeflag!=down)
        {
          memmove(sortedptrs+down,sortedptrs+down+1,sizeof(void *)*(trc-down));
          sortedptrs[trc]=df;
          df->dontchangeflag=down;
          df->lasttested=trc;
          goto opakuj;
        }
        else
          trc++;
      }
      else 
        trc++;
    }
    down++;
  }
}

void CFaceSortList::DrawFaces(bool wires)
{
  int cnt=Size();
  for (int i=0;i<cnt;i++)
  {
    adc->FilledQuadGuard(sortedptrs[i]->n,sortedptrs[i]->points,sortedptrs[i]->normrf,wires);
  }
}

bool C3DC::MoveToRect(CRect &rect, float x, float y, float z)
{
  HVECTOR tv;
  
  TransformVector(transform,mxVector3(x,y,z),tv);
  XYZW2XYZQ(tv);
  if (ToInt(tv[XVAL])<rect.left ||ToInt(tv[XVAL])>rect.right ||
    ToInt(tv[YVAL])<rect.top || ToInt(tv[YVAL])>rect.bottom || tv[ZVAL]<0 || tv[WVAL]<0)
      return false;
  MoveToT(tv);
  return true;
}

COLORREF C3DC::GetColorAt(float x, float y, float z, COLORREF def)
{
  HVECTOR tv;
  
  TransformVector(transform,mxVector3(x,y,z),tv);
  XYZW2XYZQ(tv);
  if (tv[ZVAL]<0.0f || tv[WVAL]<=0.0f) return def;
  return GetPixel(ToInt(tv[XVAL]),ToInt(tv[YVAL]));
}

void C3DC::MoveToT(HVECTOR pos)
{
  reldist[0]=pos[0]-oldpos[0];
  reldist[1]=pos[1]-oldpos[1];
  reldist[2]=pos[2]-oldpos[2];
  CopyVektor(oldpos,pos);
}

