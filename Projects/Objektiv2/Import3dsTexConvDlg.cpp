// Import3dsTexConvDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "Import3dsTexConvDlg.h"
#include "pal2pac.hpp"
#include "io.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define TEMPFILE "temp$$$.tga"

/////////////////////////////////////////////////////////////////////////////
// CImport3dsTexConvDlg dialog


CImport3dsTexConvDlg::CImport3dsTexConvDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CImport3dsTexConvDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CImport3dsTexConvDlg)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    texinfo=NULL;
    usematname=false;
    }

//--------------------------------------------------

void CImport3dsTexConvDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CImport3dsTexConvDlg)
  DDX_Control(pDX, IDC_BROWSEPATH, wBrowse);
  DDX_Control(pDX, IDC_FORCECONV, wForceConv);
  DDX_Control(pDX, IDC_TODOLIST, wToDo);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CImport3dsTexConvDlg, CDialog)
  //{{AFX_MSG_MAP(CImport3dsTexConvDlg)
  ON_WM_DESTROY()
    ON_BN_CLICKED(IDC_BROWSEPATH, OnBrowsepath)
      ON_BN_CLICKED(IDC_FORCECONV, OnForceconv)
        //}}AFX_MSG_MAP
        END_MESSAGE_MAP()
          
          /////////////////////////////////////////////////////////////////////////////
          // CImport3dsTexConvDlg message handlers
          
          BOOL CImport3dsTexConvDlg::OnInitDialog() 
            {
            CDialog::OnInitDialog();
            
            texcount=0;	
            delete [] texinfo;
            texinfo=NULL;
            wToDo.InsertColumn(0,WString(IDS_3DSTMATERIAL),LVCFMT_LEFT,150,0);
            wToDo.InsertColumn(1,WString(IDS_3DSTTARGET),LVCFMT_LEFT,150,0);
            wToDo.InsertColumn(2,WString(IDS_3DSTCOMMENT),LVCFMT_LEFT,200,0);
            wToDo.SetImageList(&shrilist,LVSIL_SMALL);
            for (int i=0;i<2;i++)
              {
              int pos=0;
              C3dsChunk *mat_entry=mdata->FindChunkSub(MAT_ENTRY);
              while (mat_entry)
                {
                C3dsNamedObject *mat_name=static_cast<C3dsNamedObject *>(mat_entry->FindChunkSub(MAT_NAME));
                if (mat_name==NULL) break;
                if (i==0) texcount++;
                else
                  {
                  TexInfo &nfo=texinfo[pos];	
                  nfo.matname=mat_name->GetName();
                  CHUNKID diffpath[]=
                    {MAT_ENTRY,MAT_DIFFUSE,COLOR_24};
                  C3dsRGBTriplet *mat_diffuse=static_cast<C3dsRGBTriplet *>(mat_entry->FindChunk(3,diffpath));
                  if (mat_diffuse)
                    {
                    S3DSRGB rgb=mat_diffuse->GetRGB();
                    nfo.rd=rgb.r;
                    nfo.gd=rgb.g;
                    nfo.bd=rgb.b;
                    }
                  else
                    {
                    nfo.rd=0;
                    nfo.gd=0;
                    nfo.bd=0;
                    }
                  CHUNKID transpath[]=
                    {MAT_ENTRY,MAT_TRANSPARENCY,INT_PERCENTAGE};
                  C3dsPercentage *mat_transparency=static_cast<C3dsPercentage *>(mat_entry->FindChunk(3,transpath));
                  if (mat_transparency)			
                    nfo.ad=255-(mat_transparency->GetValue()*255/100);
                  else 
                    nfo.ad=255;
                  C3dsChunk *mat_texmap=mat_entry->FindChunkSub(MAT_TEXMAP);
                  nfo.sourcemap.SetDirectory("");
                  nfo.sourcemap.SetFilename("");
                  nfo.sourcemap.SetExtension("");
                  nfo.sourcealpha.SetDirectory("");
                  nfo.sourcealpha.SetFilename("");
                  nfo.sourcealpha.SetExtension("");
                  if (mat_texmap)
                    {
                    C3dsNamedObject *mat_mapname=static_cast<C3dsNamedObject *>(mat_texmap->FindChunkSub(MAT_MAPNAME));
                    if (mat_mapname)
                      {
                      nfo.sourcemap=basepath;
                      nfo.sourcemap.SetFilename(mat_mapname->GetName());
                      C3dsPercentage *int_percentage=static_cast<C3dsPercentage *>(mat_texmap->FindChunkSub(INT_PERCENTAGE));
                      if (int_percentage) nfo.dmix=int_percentage->GetValue()*128/100;
                      else nfo.dmix=128;
                      }
                    else			  
                      nfo.dmix=0;			  
                    }
                  C3dsChunk *mat_opacmap=mat_entry->FindChunkSub(MAT_OPACMAP);
                  if (mat_opacmap)
                    {
                    C3dsNamedObject *mat_mapname=static_cast<C3dsNamedObject *>(mat_opacmap->FindChunkSub(MAT_MAPNAME));
                    if (mat_mapname)
                      {
                      nfo.sourcealpha=basepath;
                      nfo.sourcealpha.SetFilename(mat_mapname->GetName());
                      C3dsPercentage *int_percentage=static_cast<C3dsPercentage *>(mat_opacmap->FindChunkSub(INT_PERCENTAGE));
                      if (int_percentage) nfo.amix=int_percentage->GetValue()*128/100;
                      else nfo.amix=128;
                      }
                    else			  
                      nfo.amix=0;			  
                    }
                  nfo.targetmap=targetpath;
                  if (usematname || (nfo.sourcealpha=="\\" && nfo.sourcemap=="\\"))
                    nfo.targetmap.SetFilename(nfo.matname);
                  else
                    if (nfo.sourcemap!="\\")	 nfo.targetmap.SetFilename(nfo.sourcemap.GetFilename());
                  else nfo.targetmap.SetFilename(nfo.sourcealpha.GetFilename());
                  nfo.targetmap.SetExtension(nfo.sourcealpha=="\\"?".pac":".paa");
                  nfo.state=TexInfo::inQueue;
                  nfo.idscomment=0;
                  nfo.texname=prefix+nfo.targetmap.GetFilename();
                  int lpos=wToDo.InsertItem(pos,nfo.matname,ILST_EMPTY);
                  wToDo.SetItemText(lpos,1,nfo.texname);
                  }
                pos++;
                mat_entry=mat_entry->FindChunkNext(MAT_ENTRY);
                }
              if (i==0) texinfo=new TexInfo[texcount];	  
              }
            ShowWindow(SW_SHOW);
            UpdateWindow();
            ProcessAll();
            return TRUE;  // return TRUE unless you set the focus to a control
            // EXCEPTION: OCX Property Pages should return FALSE
            }

//--------------------------------------------------

void CImport3dsTexConvDlg::OnDestroy() 
  {
  CDialog::OnDestroy();
  }

//--------------------------------------------------

void CImport3dsTexConvDlg::SetState(int item, TexInfo::Texinfostate state, int comment)
  {
  int icon;
  switch (state)
    {
    case TexInfo::Done: icon=ILST_CHECKOK;break;
    case TexInfo::Error: icon=ILST_CHECKERROR; break;
    case TexInfo::Warning: icon=ILST_CHECKEXCLAMATION; break;
    case TexInfo::InProgress: icon=ILST_POSITION ;break;
    }
  LVITEM it;
  it.iItem=item;
  if (comment>=0)
    {
    CString s;
    s.LoadString(comment);
    texinfo[item].idscomment=comment;
    it.iSubItem=2;
    it.mask =LVIF_TEXT ;
    it.pszText=s.LockBuffer();
    wToDo.SetItem(&it);
    s.UnlockBuffer();
    }
  it.iImage=icon;
  it.mask=LVIF_IMAGE ;
  it.iSubItem=0;
  wToDo.SetItem(&it);
  wToDo.EnsureVisible(item,FALSE);
  wToDo.UpdateWindow();
  texinfo[item].state=state;
  }

//--------------------------------------------------

bool CImport3dsTexConvDlg::DoConvert(int item)
  {
  TexInfo &nfo=texinfo[item];
  TexInfo::Texinfostate basestate=nfo.state;
  SetState(item,TexInfo::InProgress,IDS_3DSTCONVERTING);
  int xsize=8;
  int ysize=8;
  TUNIPICTURE *rgbimg=NULL;
  TUNIPICTURE *alfaimg=NULL;  
  if (_access(nfo.targetmap,0)==0 && basestate==TexInfo::inQueue) 
    {
    SetState(item,TexInfo::Warning,IDS_3DSTTARGETEXISTS);
    return false;
    }
  if (nfo.sourcemap!="\\")
    {
    rgbimg=cmLoadUni((char *)((LPCTSTR)nfo.sourcemap));
    if (rgbimg==NULL)
      {
      SetState(item,TexInfo::Error,IDS_3DSTTEXTURENOTFOUND);
      return false;
      }
    }
  if (nfo.sourcealpha!="\\")
    {
    alfaimg=cmLoadUni((char *)((LPCTSTR)nfo.sourcealpha));
    if (alfaimg==NULL)
      {
      SetState(item,TexInfo::Error,IDS_3DSTTEXTURENOTFOUND);
      free(rgbimg);
      return false;
      }
    }
  if (rgbimg && alfaimg && (alfaimg->xsize!=rgbimg->xsize || alfaimg->ysize!=rgbimg->ysize)) 
    {
    SetState(item,TexInfo::Error,IDS_3DSTTEXDIMENSIONERROR);
    free(rgbimg);free(alfaimg);return false;
    }
  if (rgbimg) 
    {xsize=rgbimg->xsize;ysize=rgbimg->ysize;}
  else if (alfaimg) 
    {xsize=alfaimg->xsize;ysize=alfaimg->ysize;}
  TUNIPICTURE *target=::cmCreateUni(xsize,ysize,"ARGB8888",1,0);
  DWORD basecolor=RGB(nfo.rd,nfo.gd,nfo.bd);
  basecolor|=nfo.ad<<24;
  DWORD *data=(DWORD *)cmGetData(target);
  int rep=target->totaldata/4;
  int i;
  for (i=0;i<rep;i++) data[i]=basecolor;
  TUNIPICTURE *a;
  if (rgbimg)
    {
    a=::cmConvertUni(rgbimg,"RGB888",1);
    if (a==NULL)
      {
      SetState(item,TexInfo::Error,IDS_3DSTCANNOTCONVERTFORMAT);
      free(rgbimg);free(alfaimg);free(target);return false;
      }
    free(rgbimg);
    rgbimg=a;
    }
  if (alfaimg)
    {
    a=::cmConvertUni(alfaimg,"A8",1);
    if (a==NULL)
      {
      SetState(item,TexInfo::Error,IDS_3DSTCANNOTCONVERTFORMAT);
      free(rgbimg);free(alfaimg);free(target);return false;
      }
    free(alfaimg);
    alfaimg=a;
    } 
  /* now mix data toegether*/
  if (rgbimg)	
    {
    unsigned char *t=(unsigned char *)cmGetData(target);
    unsigned char *im=(unsigned char *)cmGetData(rgbimg);
    for (int y=0;y<target->ysize;y++)
      for (int x=0;x<target->xsize;x++)
        {
        *t=*t+(*im-*t)*nfo.dmix/128;t++;im++;
        *t=*t+(*im-*t)*nfo.dmix/128;t++;im++;
        *t=*t+(*im-*t)*nfo.dmix/128;t++;im++;
        t++;
        }	  
    }
  if (alfaimg)	
    {
    unsigned char *t=(unsigned char *)cmGetData(target);
    unsigned char *im=(unsigned char *)cmGetData(alfaimg);
    for (int y=0;y<target->ysize;y++)
      for (int x=0;x<target->xsize;x++)
        {
        t+=3;
        *t=*t+(*im-*t)*nfo.amix/128;t++;im++;
        }	  
    }
  free(rgbimg);free(alfaimg);
  ofstream tgaout;
  tgaout.open(TEMPFILE,ios::out|ios::trunc|ios::binary);
  if (!tgaout)
    {
    SetState(item,TexInfo::Error,IDS_3DSTCANNOTOPENTEMPFILE);
    free(target);return false;
    }
  OpenTGAFile(tgaout,target->xsize,target->ysize,32);
  for (int y=target->ysize-1;y>=0;y--)
    tgaout.write((char *)cmGetData(target)+y*target->xlinelen,target->xlinelen);
  CloseTGAFile(tgaout);
  free(target);
  tgaout.close();
  if (UpdateTextureF==NULL) 
    {
    SetState(item,TexInfo::Error,IDS_3DSTPACPALERROR);
    return false;
    }  
  CString tmp=CString(nfo.targetmap)+".$$$";
  UpdateTextureF(tmp,TEMPFILE,-1);
  if (_access(tmp,0)!=0) 
    {
    SetState(item,TexInfo::Error,IDS_3DSTCONVERTERERROR);
    remove(tmp);
    return false;
    }
  else
    {
    remove(nfo.targetmap);
    rename(tmp,nfo.targetmap);
    }
  remove(TEMPFILE);
  SetState(item,TexInfo::Done,IDS_3DSTOK);
  MSG msg;
  while (::PeekMessage(&msg,NULL,0,0,PM_REMOVE)) DispatchMessage(&msg);
  return true;
  }

//--------------------------------------------------

void CImport3dsTexConvDlg::OnOK() 
  {
  // TODO: Add extra validation here
  
  CDialog::OnOK();
  }

//--------------------------------------------------

void CImport3dsTexConvDlg::ProcessAll()
  {
  int i=wToDo.GetNextItem(-1,LVNI_SELECTED);
  EnableWindow(FALSE);
  bool quit=true;
  if (i==-1)
    {
    for (i=0;i<texcount;i++) if (texinfo[i].state!=TexInfo::Done) quit&=DoConvert(i);
    }
  else
    {
    quit=false;
    while (i!=-1)
      {
      DoConvert(i);
      i=wToDo.GetNextItem(i,LVNI_SELECTED);
      }
    }
  EnableWindow(TRUE);;
  if (quit) PostMessage(WM_COMMAND,IDOK,0);	
  }

//--------------------------------------------------

const char * CImport3dsTexConvDlg::GetTextureName(const char *matname)
  {
  for (int i=0;i<texcount;i++)
    {
    if (strcmp(texinfo[i].matname,matname)==0) return texinfo[i].texname;
    }
  return NULL;
  }

//--------------------------------------------------

CImport3dsTexConvDlg::~CImport3dsTexConvDlg()
  {
  delete [] texinfo;
  }

//--------------------------------------------------

void CImport3dsTexConvDlg::OnBrowsepath() 
  {
  const char *path=AskForPath(this->m_hWnd);
  if (path)
    {
    int i=wToDo.GetNextItem(-1,LVNI_SELECTED);
    if (i==-1)
      {
      for (i=0;i<texcount;i++) if (texinfo[i].state!=TexInfo::Done)
        {
        SetState(i,TexInfo::inQueue,-1);
        if (texinfo[i].sourcealpha!="\\")
          {
          texinfo[i].sourcealpha.SetDirectory(path);
          }
        if (texinfo[i].sourcemap!="\\")
          {
          texinfo[i].sourcemap.SetDirectory(path);
          }
        }
      }
    else
      while (i!=-1)
        {
        SetState(i,TexInfo::inQueue,-1);
        texinfo[i].sourcealpha.SetDirectory(path);
        texinfo[i].sourcemap.SetDirectory(path);
        i=wToDo.GetNextItem(i,LVNI_SELECTED);
        }
    }
  ProcessAll();
  }

//--------------------------------------------------

void CImport3dsTexConvDlg::OnForceconv() 
  {
  int i=wToDo.GetNextItem(-1,LVNI_SELECTED);
  if (i==-1)
    {
    for (i=0;i<texcount;i++) if (texinfo[i].state!=TexInfo::Done)
      {
      SetState(i,TexInfo::Force,-1);
      }
    }
  else
    while (i!=-1)
      {
      SetState(i,TexInfo::Force,-1);
      i=wToDo.GetNextItem(i,LVNI_SELECTED);
      }
  ProcessAll();	
  }

//--------------------------------------------------

