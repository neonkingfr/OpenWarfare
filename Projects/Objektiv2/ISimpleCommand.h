


#define CIS_RECALCNORM 1
#define CIS_OPTIMIZE 2
#define CIS_CHECKFACES 3
#define CIS_ISOLATEDPOINTS 4
#define CIS_TRIANGULATE 5
#define CIS_SQUARIZE 6
#define CIS_FINDCOMPONENTS 7
#define CIS_COMPONENTHULL 8
#define CIS_CONVEXHULL 9
#define CIS_FINDNONCONVEX 10
#define CIS_SELECTHALFSPACE 11
#define CIS_FINDNONCLOSED 12
#define CIS_CLOSE 13
#define CIS_SPLIT 14
#define CIS_DELETE 15
#define CIS_CUT 16
#define CIS_REVERSEFACE 17
#define CIS_UNCROSS 18
#define CIS_REMOVE 19
#define CIS_MERGESEL 20
#define CIS_SMOOTH 21
#define CIS_SHARP 22
#define CIS_CREATEFACE 23
#define CIS_PINCULL 24
#define CIS_TRIANGULATE2 25
#define CIS_CENTERALL 26
#define CIS_FACESFROMPOINTS 27
#define CIS_POINTSFROMFACES 28
#define CIS_REPAIRFACES 29
#define CIS_CHECKDEGENERATED 30
#define CIS_CREATEPOLYGON 31
#define CIS_SORTFACES 32
#define CIS_SHOWPROXIES 33
#define CIS_HIDEPROXIES 34
#define CIS_SORTALPHAFACES 35
#define CIS_NEIGHBOURMAPPING 36
#define CIS_BREAKWELDS 37
#define CIS_BREAKTOCONVEX 38
#define CIS_TRIANGULATE_CONVEX 39
#define CIS_TRIANGULATE_CONCAVE 40


class CISimpleCommand:public CICommand
  {
  int comm;
  public:
    CISimpleCommand(int c):comm(c) 
      {};
    CISimpleCommand(istream& str) 
      {datard(str,comm);}
    int Execute(LODObject *obj);
    virtual int Tag() const 
      {return CITAG_SIMPLECOMMAND;}
    virtual void Save(ostream &str) 
      {datawr(str,comm);}
  };




CICommand *CISimpleCommand_Create(istream &str);





