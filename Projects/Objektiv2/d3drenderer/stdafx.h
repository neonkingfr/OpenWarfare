// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <stdio.h>
#include <tchar.h>

#include <iostream>
#include <fstream>

#include <g3dlib.h>
#include <g3dpp\g3dpp.h>
#define mxVector3(x,y,z) mxInline_SetVector4(x,y,z,1.0f)
//#undef Vector3 //G3D Matrix namespace colision. Remove Vector3 Definition
#include <projects/ObjektivLib/LODObject.h> 
/*
#include "StringRes.h"
#include "FileDialogEx.h"
*/
using namespace ObjektivLib;

// TODO: reference additional headers your program requires here
