#include "d3drenderer.h"

#include <stdio.h>
#include <process.h>
using namespace d3dr;

d3dproxy::d3dproxy(char *path, char *pathdir) {
	ZeroMemory(&modelData, sizeof(d3dr::MODELBLOCK));
	strcpy(name, "");
	valid = loadModel(path, pathdir);
}

d3dproxy::~d3dproxy() {
	RptF("Releasing proxy <%s>", name);
	valid = false;
	modelData.release();
}

// Does proxy name (including .001) match this model name?
bool d3dproxy::MatchName(const char *tname) {
	char sname[256];
	strcpy(sname, tname);
	char *dot = strstr(sname, ".");
	if(dot) *dot = 0x00;
	return (!stricmp(sname, name));
}

bool d3dproxy::loadModel(char *path, char *pathdir) {
	RptF("Loading proxy model: <%s>", path);

	// Replace .001 with .p3d
	char p3dpath[256];
	strcpy(p3dpath, path);
	char *dot = strstr(p3dpath,".");
	if(dot)
		strcpy(dot, ".p3d");

	char fullPath[256];
	if(!strstr(p3dpath, ":"))
		snprintf(fullPath, 256, "%s\\%s", pathdir, p3dpath);	// Relative path
	else
		snprintf(fullPath, 256, "%s", p3dpath);	// Full path

	LODObject mdl;
	ifstream f;
	f.open(fullPath, ios::in|ios::binary);
	if(mdl.Load(f, 0, 0) == -1) {
		RptF("Proxy model <%s> not found", fullPath);
#ifdef PRINTF_ERRORS
		printf("WARNING: Cannot load proxy: <%s>\n", fullPath);
#endif
		return false;
	}
	RptF("Proxy model <%s> loaded", p3dpath);
	f.close();
	
	ZeroMemory(name, 256);
	strncpy(name, p3dpath, dot-p3dpath);	// TODO: strip .p3d
	ObjectData *obj = mdl.Level(0);

	// Build model data
	d3drenderer::createDataVL(*obj, modelData);
	d3drenderer::createDataTX(*obj, modelData, true);
	return true;
}
