//
// Simple class for loading raw PAA texture data
//

#include <stdio.h>
#include "El/QStream/QStream.hpp"
#include "img/Picture/lzcompr.hpp"

class paaFile {
public:
	enum paaFormat {
		PAA_88   = 0x8080,
		PAA_1555 = 0x1555,
		PAA_4444 = 0x4444,
		PAA_DXT1 = 0xff01,
		PAA_DXT5 = 0xff05,
	};

	paaFile();
	~paaFile();
	bool load(char *name, int level=0);

	void* DataPtr(void) {return data;};
	int DataSize(void) {return size;};
	int Width(void) {return w;};
	int Height(void) {return h;};
	paaFormat Format(void) {return fmt;};
	bool valid() {return (loadedLevel!=-1&&data);};

private:
	int loadedLevel;	// Currently loaded mipmap level
	int w, h;
	paaFormat fmt;
	void *data;			// Constains loaded image data
	int size;			// size of data

	int fgeti(QIStream &f, int len);
	void seekf(QIStream &f, int off) {f.seekg(off, QIOS::cur);};

};