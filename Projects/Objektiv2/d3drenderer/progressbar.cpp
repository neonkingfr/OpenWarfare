#include "progressbar.h"

static ProgressBarFunctions DefaultPB;
static ProgressBarFunctions *GProgressBarFunctions=0;

ProgressBarFunctions *ProgressBarFunctions::GetGlobalProgressBarInstance()
{
  if (GProgressBarFunctions==0) return &DefaultPB;
  else return GProgressBarFunctions;
}