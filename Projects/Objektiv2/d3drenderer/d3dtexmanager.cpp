
#include "d3dtexmanager.h"

//#define USE_PICTURE		// Use Picture class for texture loading support, D3DX+paafile otherwise

#ifdef USE_PICTURE
#include "picture/picture.hpp"
#else
#include "paafile.h"
#endif

d3dtexmanager::d3dtexmanager(LPDIRECT3DDEVICE9 dev) {
	valid = true;
	d3dd=dev;
	strcpy(texPath, "");

	// Create default texture
	D3DMTEX tex;
	strncpy(tex.name, "_TEXM_DEFAULT__", 256);	
	static int texSize = 8;
	d3dd->CreateTexture(texSize, texSize, 1, NULL, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &tex.tex, NULL);	
	D3DLOCKED_RECT lck;
	tex.tex->LockRect(0, &lck, NULL, 0);

#ifdef D3DRENDERER_BLANKTEX
	// Blank default texture for p3drender
	// TODO: should not need a preprocessor macro for this
	DWORD *d = (DWORD*)lck.pBits;
	DWORD ca = 0x80000000;
	for(int y=0;y<texSize;y++)
		for(int x=0;x<texSize;x++)
			d[y*(lck.Pitch/4)+x] = ca;
#else
	DWORD *d = (DWORD*)lck.pBits;
	DWORD ca = 0xFFeeeeee;
	DWORD cb = 0xFFFF00FF;
	for(int y=0;y<texSize;y++)
		for(int x=0;x<texSize;x++)
			d[y*(lck.Pitch/4)+x] = (x+y)%2==0?ca:cb;
#endif
	tex.tex->UnlockRect(0);
	tex.unused = false;
	textures.push_back(tex);
}

d3dtexmanager::~d3dtexmanager() {
	if(valid) {
		flushAll();
		if(textures.size() > 0 && textures[0].tex) {
			while(textures[0].tex->Release()) {};
			textures[0].tex = 0;
		}
		valid = false;
	}
}

bool d3dtexmanager::isPaa(char *s) {
	if(!s || strlen(s)<5)
		return false;
	if(!_strnicmp(".paa", s+strlen(s)-4, 4))
		return true;
	return false;
}

__int64 d3dtexmanager::getFileDate(char *path) {
	__int64 ret = 0;
	HANDLE h = CreateFile(path, 0, FILE_SHARE_WRITE|FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);
	if(!h) return ret;
	BY_HANDLE_FILE_INFORMATION hi;ZeroMemory(&hi, sizeof(BY_HANDLE_FILE_INFORMATION));
	GetFileInformationByHandle(h, &hi);
	CloseHandle(h);
	memcpy(&ret, &hi.ftLastWriteTime.dwLowDateTime, 4);
	memcpy(&ret+4, &hi.ftLastWriteTime.dwHighDateTime, 4);
	return ret;	
}

void d3dtexmanager::getFullPath(char *name, char *fullPath) {
	if(!strstr(name, ":")) snprintf(fullPath, 512, "%s/%s", texPath, name);
	else snprintf(fullPath, 512, "%s", name);
}

void d3dtexmanager::setTexturePath(char *path) {
	strncpy(texPath, path, 256);
}

bool d3dtexmanager::loadTexture(D3DMTEX *tex) {
	if(tex->tex)
		while(tex->tex->Release());
	tex->tex = NULL;

#ifdef USE_PICTURE
	Picture pic;
	pic.Load(tex->fullPath);
	if(!pic.Valid())
		return false;
	int w = pic.W();
	int h = pic.H();

#define FMAP(x,y) case x: fmt = y;break;
	D3DFORMAT fmt = D3DFMT_UNKNOWN;
	switch(pic.GetSourceFormat()) {
		FMAP(TexFormDXT1, D3DFMT_DXT1);
		FMAP(TexFormDXT5, D3DFMT_DXT5);
		default:
			RptF("d3dtexmanager::loadTexture: Unsupported texture format %d <%s>", pic.GetSourceFormat(), tex->fullPath);
			break;
	}
#undef FMAP

	//d3dd->CreateTexture(w, h, 1, NULL, fmt, D3DPOOL_MANAGED, &tex->tex, NULL);	
	d3dd->CreateTexture(w, h, 1, NULL, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &tex->tex, NULL);	
	D3DLOCKED_RECT lck;
	tex->tex->LockRect(0, &lck, NULL, 0);
	DWORD *d = (DWORD*)lck.pBits; 
	for(int x=0;x<w;x++)
		for(int y=0;y<h;y++)
			*d++ = pic.GetPixelARGB(x, y);
	tex->tex->UnlockRect(0);
	return true;

#else
	if(isPaa(tex->fullPath)) {
		d3dmdbg("d3dtexmanager::getTexture: Loading PAA: <%s>\n", tex->fullPath);
		paaFile paa;
		paa.load(tex->fullPath);
		if(!paa.valid()) {
#ifdef PRINTF_ERRORS
			printf("WARNING: Cannot load texture: <%s>\n", tex->fullPath);
#endif
			return false;
		}

		D3DFORMAT fmt = D3DFMT_UNKNOWN;
		DWORD w = paa.Width();
		DWORD h = paa.Height();
		DWORD wSize = 0;
		switch(paa.Format()) {
			case paaFile::PAA_88:   fmt=D3DFMT_A8L8; wSize=w*h*2; break;
			case paaFile::PAA_1555: fmt=D3DFMT_A1R5G5B5; wSize=w*h*2; break;
			case paaFile::PAA_4444: fmt=D3DFMT_A4R4G4B4; wSize=w*h*2; break;
			case paaFile::PAA_DXT1: fmt=D3DFMT_DXT1; wSize=w*h/2; break;
			case paaFile::PAA_DXT5: fmt=D3DFMT_DXT5; wSize=w*h*1; break;
			default:
				d3dmdbg("d3dtexmanager::getTexture: Unsupported texture format: 0x%04X\n", paa.Format());
				return false;
		}
		if(wSize != paa.DataSize()) {
			d3dmdbg("d3dtexmanager::getTexture: Invalid PAA data %d != %d\n", wSize, paa.DataSize());
			return false;
		}
		
		if(d3dd->CreateTexture(w, h, 1, NULL, fmt, D3DPOOL_MANAGED, &tex->tex, NULL) != D3D_OK) {
			d3dmdbg("d3dtexmanager::getTexture: CreateTexture failed (Unsupported texture format or out of video memory)\n");
			return false;
		}
		D3DLOCKED_RECT lck;
		if(tex->tex->LockRect(0, &lck, NULL, 0) != D3D_OK)
			return false;
		memcpy(lck.pBits, paa.DataPtr(), wSize);
		tex->tex->UnlockRect(0);
		return true;
	} else {
		// d3dx library - loads tga, png, bmp, dds, etc. (fast)
		D3DXIMAGE_INFO ii;
		if(D3DXCreateTextureFromFileEx(d3dd, tex->fullPath, D3DX_DEFAULT, D3DX_DEFAULT, generateMipmaps?0:1, 0, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, 
			D3DX_DEFAULT, D3DX_DEFAULT, 0x00000000, &ii, NULL, &tex->tex) != D3D_OK) {

#ifdef PRINTF_ERRORS
			printf("WARNING: Cannot load texture: <%s>\n", tex->fullPath);
#endif
			//d3dmdbg("d3dtexmanager::getTexture: Cannot load texture <%s>\n", tex->fullPath);
			return false;
		}
		//d3dmdbg("d3dtexmanager::getTexture: Added new texture <%s> (%dx%d)\n", tex->fullPath, ii.Width, ii.Height);
		return true;
	}
#endif
}

DWORD d3dtexmanager::addTexture(char *name) {
	if(!name || strlen(name) == 0)
		return defaultTexture;	// Blank = default texture

	// See if this texture is already loaded
	for(DWORD i=0;i < textures.size();i++) {
		if(!strncmp(name, textures[i].name, 256)) {
			textures[i].unused = false;
			return i;		
		}
	}

	// Create new texture
	D3DMTEX tex;ZeroMemory(&tex, sizeof(D3DMTEX));
	strncpy(tex.name, name, 256);	
	getFullPath(name, tex.fullPath);

	if(!loadTexture(&tex))
		return defaultTexture;
		
	DWORD id = (DWORD)textures.size();	
	tex.mdate = getFileDate(tex.fullPath);
	tex.unused = false;
	textures.push_back(tex);
	return id;
}

LPDIRECT3DTEXTURE9 d3dtexmanager::getTexture(DWORD id) {
	if(id < 0 || id > textures.size())
		return defaultTexture;

	// Check if texture has changed and reload it	
	__int64 dn = getFileDate(textures[id].fullPath);
	__int64 dp = textures[id].mdate;
	if(dn != dp && dn && dp) {
		loadTexture(&textures[id]);
		textures[id].mdate = dn;
		//RptF("FILE %d CHANGED", id);
	}

	return textures[id].tex;	
}

// Release all but the default texture
void d3dtexmanager::flushAll() {
	if(textures.size() < 2)
		return;
	for(DWORD i=1;i < textures.size();i++) {
		if(textures[i].tex)
			while(textures[i].tex->Release()) {};	
		textures[i].tex = NULL;
	}
	textures.resize(1);	
}

// Mark all currently loaded textures as unused
void d3dtexmanager::prepareFlush() {
	if(textures.size() < 2)
		return;

	vector<D3DMTEX>	validTex;
	validTex.push_back(textures[defaultTexture]);
	for(DWORD i=1;i < textures.size();i++) {
		if(textures[i].tex)
			validTex.push_back(textures[i]);
	}
	textures.swap(validTex);
	for(DWORD i=1;i < textures.size();i++)
		textures[i].unused = true;
}

// Release textures still marked unused
void d3dtexmanager::flushUnused() {
	if(textures.size() < 2)
		return;
	for(DWORD i=1;i < textures.size();i++) {
		if(textures[i].unused && textures[i].tex) {
			while(textures[i].tex->Release()) {};
			textures[i].tex = NULL;
			strcpy(textures[i].name, "__UNUSED__");
			//d3dmdbg("d3dtexmanager::flushUnused: Releasing unused texture id %d\n", i);
		}
	}
}