// d3drenderer.cpp : Defines the entry point for the console application.
// Debugging executable for d3drenderer

#include "stdafx.h"

#include <windows.h>
#include <stdio.h>
#include <process.h>

#include <projects/ObjektivLib/bitxtimport.h>
#include <projects/ObjektivLib/ObjectData.h>
#include <projects/ObjektivLib/ObjToolAnimation.h>
#include <projects/ObjektivLib/ObjToolClipboard.h>
#include <projects/ObjektivLib/ObjToolsMatLib.h>
#include <projects/ObjektivLib/ObjToolProxy.h>
#include <projects/ObjektivLib/ObjToolTopology.h>

#include "d3drenderer.h"

using namespace std;

static volatile bool running = true;
HWND mainWindow;	// Window for d3d
void __cdecl threadDummyWindow(void*);

float camx, camy, camz, camd;
int LODid = 0;
DWORD texFlags = D3DR_TEXTURE;
DWORD wireFlags = D3DR_WIREFRAME+D3DR_VERTICES;

LARGE_INTEGER tFreq, tStart;
void startTimer() {
	QueryPerformanceFrequency(&tFreq);
	QueryPerformanceCounter(&tStart);
}

double stopTimer() {
	LARGE_INTEGER tEnd;
    QueryPerformanceCounter(&tEnd);
    return (double(tEnd.QuadPart - tStart.QuadPart) / tFreq.QuadPart);
}

int _tmain(int argc, _TCHAR* argv[])
{
	camx = camy = camz = 5;
	camd = 5;

	LODObject obj;
	d3drenderer d3dr;

	ifstream mf;
	//mf.open("c:/bis/box2.p3d", ios::in|ios::binary);
	mf.open("C:/bis/samples/ARMA_SampleVehiclesWeapons/A10/a10.p3d", ios::in|ios::binary);
	int r = obj.Load(mf, 0, 0);
	if(r == -1) {
		printf("Load fail\n");
		return 0;
	}
	mf.close();

	int nlev = obj.NLevels();	// Number of levels in model
	for(int i=0;i<nlev;i++) {
		printf("LOD %d: resolution: %f\n", i, obj.Resolution(i));
	}

	// Create device window thread (Creates window & handles it in future)
	_beginthread(threadDummyWindow, 0, 0);
	while(!mainWindow)
		Sleep(100);

	if(d3dr.initD3D(mainWindow, 8) < 1) {
		printf("initD3D fail\n");
		running = false;
	}

	while(running) {
		static int curLODid = -1;
		if(LODid != curLODid) {
			if(LODid < 0) LODid = 0;
			if(LODid >= nlev) LODid = nlev-1;
			printf("Loading LOD %d: resolution: %f\n", LODid, obj.Resolution(LODid));
			startTimer();
			d3dr.createDataVL(*obj.Level(LODid));
			d3dr.createDataTX(*obj.Level(LODid));
			DOUBLE t = stopTimer();
			printf("LOD loaded in %fs\n", t);
			curLODid = LODid;
		}
		//d3dr.positionCameraPerspective(sin(camx/128.0f)*camd, (cos(camx/128.0f)*camd) + (cos(camy/128.0f)*camd), sin(camy/128.0f)*camd);
		d3dr.positionLight();
		d3dr.clear(0xEEEEEEEE);

		if(texFlags)
			d3dr.renderDataTX(texFlags);
		d3dr.renderGrid();
		if(wireFlags)
			d3dr.renderDataVL(wireFlags);
		

		//d3dr.renderObject(*obj.Level(2));
		d3dr.presentWin(NULL);
		//Sleep(500);
	}
	return 0;
}

// warning, Win32 crud below //

// Direct3D needs a window, even if invisible
LONG WINAPI WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
HWND createWindow(int x, int y, int w, int h, HWND rootWindow, HWND *retHandle) {
	HWND hWnd;
    static HINSTANCE hInstance = 0;
	WNDCLASS    wc;

    if (!hInstance) {
		hInstance = GetModuleHandle(NULL);
		wc.style         = CS_OWNDC;
		wc.lpfnWndProc   = (WNDPROC) WindowProc;
		wc.cbClsExtra    = 0;
		wc.cbWndExtra    = 0;
		wc.hInstance     = hInstance;
		wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO);
		wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);;
		wc.lpszMenuName  = NULL;
		wc.lpszClassName = "d3drenderer";

		if (!RegisterClass(&wc)) {
			MessageBox(NULL, "RegisterClass() failed:  "
			   "Cannot register window class.", "Error", MB_OK);
			return NULL;
		}
    }

    hWnd = CreateWindowEx(NULL, "d3drenderer", 
                               "d3drenderer temp window",
							   WS_SYSMENU,
                               //WS_VISIBLE|WS_SYSMENU,
							    //WS_POPUP,
                               x, y, w, h, NULL, NULL, hInstance, NULL);

    if(hWnd == NULL) {
		printf("CreateWindowEx FAILED\n");
        return NULL;
	}

    UpdateWindow(hWnd);	

	// Increase window size so client rect is of wanted size
	// Ugly, but in win32 what isn't?
	RECT rcClient, rcWindow;
	POINT ptDiff;
	GetClientRect(hWnd, &rcClient);
	GetWindowRect(hWnd, &rcWindow);
	ptDiff.x = (rcWindow.right - rcWindow.left) - rcClient.right;
	ptDiff.y = (rcWindow.bottom - rcWindow.top) - rcClient.bottom;
	MoveWindow(hWnd, rcWindow.left, rcWindow.top, w + ptDiff.x, h + ptDiff.y, TRUE);

	mainWindow = hWnd;
	if(retHandle)
		*retHandle = hWnd;

	ShowWindow(hWnd, true);

	// Message handling thread
	MSG msg;
	while(WaitMessage()) {
		PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE);
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	/* what happened to getmessage??
	while(GetMessage(&msg, hWnd, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	*/

	return hWnd;
}

void __cdecl threadDummyWindow(void*) {
	createWindow(-1300,0,1024,1024, NULL, NULL);
}


LONG WINAPI WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) { 
	switch(uMsg) {
		case WM_CLOSE:
			printf("WM_CLOSE\n");
			//exit(0);
			running = false;
			break;
		case WM_MOUSEMOVE: {
			int x = LOWORD(lParam);
			int y = HIWORD(lParam);
			static int lx=x, ly=y;

			if(wParam & MK_LBUTTON) {
				camx -= (lx-x);
				camy -= (ly-y);
			}
			if(wParam & MK_RBUTTON) {
				camd += (ly-y);
			}

			lx=x;ly=y;
			break;
		}

		case WM_KEYUP: {
			switch(wParam) {
				case '1':	LODid--; break;
				case '2':	LODid++; break;
				case VK_ESCAPE: running=false; break;
				case 'Q': texFlags=NULL; break;
				case 'W': texFlags=D3DR_TEXTURE; break;
				case 'E': texFlags=D3DR_SOLID; break;
				case 'A': wireFlags ^= D3DR_WIREFRAME; break;
				case 'S': wireFlags ^= D3DR_VERTICES; break;
			}
		}
	}
	return (LONG)DefWindowProc(hWnd, uMsg, wParam, lParam); 
}