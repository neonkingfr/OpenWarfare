#if !defined(AFX_SELECTSELECTIONDLG_H__A58F1A77_CC81_4F9E_8AA7_7098F33955CC__INCLUDED_)
#define AFX_SELECTSELECTIONDLG_H__A58F1A77_CC81_4F9E_8AA7_7098F33955CC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectSelectionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectSelectionDlg dialog

class CSelectSelectionDlg : public CDialog
{
// Construction
public:
	CSelectSelectionDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectSelectionDlg)
	enum { IDD = IDD_SELECTSELECTION };
	CListBox	wList;
	CString	vList;
	//}}AFX_DATA

    ObjectData *obj;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectSelectionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectSelectionDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTSELECTIONDLG_H__A58F1A77_CC81_4F9E_8AA7_7098F33955CC__INCLUDED_)
