// MatchDirection.cpp : implementation file
//

#include "stdafx.h"
#include <projects/ObjektivLib/common.hpp>
#include "Objektiv2.h"
#include "MatchDirection.h"
#include "Objektiv2Doc.h"
#include "AnimListBar.h"
#include <projects/ObjektivLib/ObjToolAnimation.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMatchDirection dialog


CMatchDirection::CMatchDirection(CWnd* pParent /*=NULL*/)
  : CDialog(CMatchDirection::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CMatchDirection)
    make_backup = TRUE;
    proxy = _T("");
    src_file = _T("");
    xdir = 0.0f;
    xrot_center = _T("");
    xrot_name = _T("");
    ydir = 0.0f;
    zdir = 0.0f;
    zrot_center = _T("");
    zrot_name = _T("");
    preview = TRUE;
    xdir2 = 1.0f;
    ydir2 = 0.0f;
    zdir2 = 0.0f;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CMatchDirection::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_XDIR,IDC_YDIR,IDC_ZDIR,IDC_XDIR2,IDC_YDIR2,IDC_ZDIR2,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CMatchDirection)
  DDX_Control(pDX, IDC_BROWSE, Browse);
  DDX_Check(pDX, IDC_MAKEBACKUP, make_backup);
  DDX_Text(pDX, IDC_PROXY, proxy);
  DDX_Text(pDX, IDC_SOURCEFILENAME, src_file);
  DDX_Text(pDX, IDC_XDIR, xdir);
  DDX_Text(pDX, IDC_XROTCENT, xrot_center);
  DDX_Text(pDX, IDC_XROTNAME, xrot_name);
  DDX_Text(pDX, IDC_YDIR, ydir);
  DDX_Text(pDX, IDC_ZDIR, zdir);
  DDX_Text(pDX, IDC_ZROTCENT, zrot_center);
  DDX_Text(pDX, IDC_ZROTNAME, zrot_name);
  DDX_Check(pDX, IDC_PREW, preview);
  DDX_Text(pDX, IDC_XDIR2, xdir2);
  DDX_Text(pDX, IDC_YDIR2, ydir2);
  DDX_Text(pDX, IDC_ZDIR2, zdir2);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CMatchDirection, CDialog)
  //{{AFX_MSG_MAP(CMatchDirection)
  ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
    ON_BN_CLICKED(IDC_PREW, OnPrew)
      //}}AFX_MSG_MAP
      END_MESSAGE_MAP()
        
        /////////////////////////////////////////////////////////////////////////////
        // CMatchDirection message handlers
        
        #define SECTION "MATCHDIRCFG"
        #define IDS(a) WString("%d",a)
        
        void CMatchDirection::SaveParams()
          {
          theApp.WriteProfileString(SECTION,IDS(IDC_ZROTNAME),zrot_name);
          theApp.WriteProfileString(SECTION,IDS(IDC_ZROTCENT),zrot_center);
          theApp.WriteProfileString(SECTION,IDS(IDC_XROTNAME),xrot_name);
          theApp.WriteProfileString(SECTION,IDS(IDC_XROTCENT),xrot_center);
          theApp.WriteProfileString(SECTION,IDS(IDC_PROXY),proxy);
          theApp.WriteProfileString(SECTION,IDS(IDC_XDIR),WString("%f",xdir));
          theApp.WriteProfileString(SECTION,IDS(IDC_YDIR),WString("%f",ydir));
          theApp.WriteProfileString(SECTION,IDS(IDC_ZDIR),WString("%f",zdir));
          }

//--------------------------------------------------

void CMatchDirection::LoadParams()
  {
  zrot_name=theApp.GetProfileString(SECTION,IDS(IDC_ZROTNAME),NULL);
  zrot_center=theApp.GetProfileString(SECTION,IDS(IDC_ZROTCENT),NULL);
  xrot_name=theApp.GetProfileString(SECTION,IDS(IDC_XROTNAME),NULL);
  xrot_center=theApp.GetProfileString(SECTION,IDS(IDC_XROTCENT),NULL);
  proxy=theApp.GetProfileString(SECTION,IDS(IDC_PROXY),NULL);
  sscanf(theApp.GetProfileString(SECTION,IDS(IDC_XDIR),NULL),"%f",&xdir);
  sscanf(theApp.GetProfileString(SECTION,IDS(IDC_YDIR),NULL),"%f",&ydir);
  sscanf(theApp.GetProfileString(SECTION,IDS(IDC_ZDIR),NULL),"%f",&zdir);
  }

//--------------------------------------------------

BOOL CMatchDirection::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  LoadParams();
  DlgControl();
  UpdateData(FALSE);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

static int FindSelFace(Selection *sel,ObjectData *obj)
  {
  for (int i=0;i<obj->NFaces();i++) if (sel->FaceSelected(i)) return i;
  return 0;
  }

//--------------------------------------------------

static PosT& FindSelPoint(Selection *sel,ObjectData *obj)
  {
  for (int i=0;i<obj->NPoints();i++) if (sel->PointSelected(i)) return obj->Point(i);
  return obj->Point(0);
  }

//--------------------------------------------------

//LPHVECTOR GetFaceNormal(FaceT& fc, ObjectData *obj, bool normalize=true);

void GetProxyTransform(FaceT& fc, ObjectData *obj, Matrix4 &trans)
  {
  
  // get proxy object coordinates
  // scan selection
  int pi0=fc.GetPoint(0),pi1=fc.GetPoint(1),pi2=fc.GetPoint(2);
  
  const Vector3 *p0=&obj->Point(pi0);
  const Vector3 *p1=&obj->Point(pi1);
  const Vector3 *p2=&obj->Point(pi2);
  
  float dist01=p0->Distance2(*p1);
  float dist02=p0->Distance2(*p2);
  float dist12=p1->Distance2(*p2);
  
  // p0,p1 should be the shortest distance
  if( dist01>dist02 ) __swap(p1,p2),__swap(dist01,dist02); // swap points 1,2
  if( dist01>dist12 ) __swap(p0,p2),__swap(dist01,dist12); // swap points 0,2
  // p0,p2 should be the second shortest distance
  if( dist02>dist12 ) __swap(p0,p1),__swap(dist02,dist12);
  //  verify results
  
  trans.SetPosition(*p0);
  trans.SetDirectionAndUp((*p1-*p0),(*p2-*p0));
  }

//--------------------------------------------------

void CMatchDirection::OnOK() 
  {
  Matrix4 trans;
  if (UpdateData(TRUE)==FALSE) return;
  SaveParams();	
  CDialog::OnOK();
  LODObject *LodData=doc->LodData;
  ObjectData *memory=LodData->Level(LodData->FindLevelExact(1e15));
  ObjectData *graph=LodData->Level(LodData->FindLevel(0.5f));
  if (memory==NULL || graph==NULL) 
    {AfxMessageBox(IDS_INVALIDOBJECT);return;}
  memory->GetTool<ObjToolAnimation>().AutoAnimation(src_file);
  graph->GetTool<ObjToolAnimation>().AutoAnimation(src_file);
  Selection *sel=memory->GetNamedSel(proxy);
  if (sel==NULL)
    {AfxMessageBox(IDS_SELECTIONNOTFOUND);return;}
  FaceT proxy(memory,FindSelFace(sel,memory));
  Vector3 vz(0,0,0);
  Vector3 vp(-xdir2,-ydir2,-zdir2);
  Vector3 vx;
  vp.Normalize();
  int i;
  for (i=1;i<memory->NAnimations();i++)
    {
    memory->UseAnimation(i);
    GetProxyTransform(proxy,memory,trans);
    vx=trans.Rotate(vp);
    vz+=vx;
    }
  vz.Normalize();
  float alpha=(float)atan2(vz[XVAL],vz[ZVAL]);
  float beta=(float)atan2(vz[YVAL],sqrt(vz[ZVAL]*vz[ZVAL]+vz[XVAL]*vz[XVAL]));
  float gama=(float)atan2(xdir,zdir);
  float delta=(float)atan2(ydir,sqrt(zdir*zdir+xdir*xdir));
  alpha=gama-alpha;
  beta-=delta;
  sel=memory->GetNamedSel(zrot_center);
  if (sel==NULL) 
    {AfxMessageBox(IDS_SELECTIONNOTFOUND);return;}
  PosT& osamir=FindSelPoint(sel,memory);
  sel=memory->GetNamedSel(xrot_center);
  if (sel==NULL) 
    {AfxMessageBox(IDS_SELECTIONNOTFOUND);return;}
  PosT& osamir2=FindSelPoint(sel,memory);
  Selection *aiming=graph->GetNamedSel(zrot_name);
  Selection *aiming2=graph->GetNamedSel(xrot_name);
  if (aiming==NULL || aiming2==NULL)
    {AfxMessageBox(IDS_SELECTIONNOTFOUND);return;}
  TMATRIXSTACK mx1,mx2;
  mxsInitStack(&mx1);
  mxsInitStack(&mx2);
  ObjectData *x;
  for (int k=0;k<2;k++)
    {
    if (k) x=graph;else x=memory;}
  for (i=1;i<x->NAnimations();i++)
    {
    x->UseAnimation(i);
    Translace(mxsPushR(&mx1),-osamir[0],-osamir[1],-osamir[2]);
    Translace(mxsPushR(&mx2),-osamir2[0],-osamir2[1],-osamir2[2]);
    RotaceY(mxsPushR(&mx1),-alpha);
    RotaceX(mxsPushR(&mx2),-beta);
    Translace(mxsPushR(&mx1),osamir[0],osamir[1],osamir[2]);
    Translace(mxsPushR(&mx2),osamir2[0],osamir2[1],osamir2[2]);
    for (int j=0;j<x->NPoints();j++) 
      {
      PosT& ps=x->Point(j);
      if (aiming->PointSelected(j))
        {
        float w=aiming->PointWeight(j);
        HVECTOR vv;
        TransformVector(mxsTop(&mx1),mxVector3(ps[0],ps[1],ps[2]),vv);
        CorrectVectorM(vv);
        ps[0]=ps[0]+(vv[0]-ps[0])*w;
        ps[1]=ps[1]+(vv[1]-ps[1])*w;
        ps[2]=ps[2]+(vv[2]-ps[2])*w;
        }
      if (aiming2->PointSelected(j))
        {
        float w=aiming2->PointWeight(j);
        HVECTOR vv;
        TransformVector(mxsTop(&mx2),mxVector3(ps[0],ps[1],ps[2]),vv);
        CorrectVectorM(vv);
        ps[0]=ps[0]+(vv[0]-ps[0])*w;
        ps[1]=ps[1]+(vv[1]-ps[1])*w;
        ps[2]=ps[2]+(vv[2]-ps[2])*w;
        }
      }
    mxsFreeStack(&mx1);
    mxsFreeStack(&mx2);
    }  
  if (!preview)
    {
    if (make_backup) rename(src_file,RString(src_file)+".bak");
    graph->GetTool<ObjToolAnimation>().ExportAnimationLooped(src_file,CAnimListBar::GetStepSelection(graph));
    memory->GetTool<ObjToolAnimation>().AutoAnimation(src_file);
    }
  else
    {
    RString pth(src_file);
    pth=pth+".$$$";
    graph->GetTool<ObjToolAnimation>().ExportAnimationLooped(pth,CAnimListBar::GetStepSelection(graph));
    memory->GetTool<ObjToolAnimation>().AutoAnimation(pth);
    remove(pth);
    }
  doc->UpdateAllViews(NULL);
  }

//--------------------------------------------------

void CMatchDirection::OnBrowse() 
  {
  if (UpdateData(TRUE)==FALSE) return;
  CFileDialogEx fdlg(TRUE, WString(IDS_RTMDEFEXT), NULL,OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, WString(IDS_RTMFILTER));
  if (fdlg.DoModal()==IDCANCEL) return;
  src_file=fdlg.GetPathName();
  UpdateData(FALSE);
  }

//--------------------------------------------------

void CMatchDirection::DlgControl()
  {
  GetDlgItem(IDC_MAKEBACKUP)->EnableWindow(IsDlgButtonChecked(IDC_PREW)==0);
  }

//--------------------------------------------------

void CMatchDirection::OnPrew() 
  {
  DlgControl();
  }

//--------------------------------------------------

