#if !defined(AFX_EDITPROPDLG_H__A81DADC6_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_EDITPROPDLG_H__A81DADC6_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// EditPropDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEditPropDlg dialog

class CEditPropDlg : public CDialog
  {
  // Construction
  public:
    CEditPropDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CEditPropDlg)
    enum 
      { IDD = IDD_EDITPROPERTY };
    CButton	m_Ok;
    CString	m_Name;
    CString	m_Value;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CEditPropDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CEditPropDlg)
    afx_msg void OnChangeName();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITPROPDLG_H__A81DADC6_5640_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
