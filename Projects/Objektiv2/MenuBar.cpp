// MenuBar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MenuBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMenuBar dialog


CMenuBar::CMenuBar()
  : CDialogBar()
    {
    //{{AFX_DATA_INIT(CMenuBar)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    
    
    //{{AFX_DATA_MAP(CMenuBar)
    // NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CMenuBar, CDialogBar)
  //{{AFX_MSG_MAP(CMenuBar)
  ON_WM_SIZE()
    ON_WM_LBUTTONDOWN()
      ON_WM_MOUSEMOVE()
        ON_WM_SETCURSOR()
          ON_WM_NCHITTEST()
            ON_WM_ENTERIDLE()
              //}}AFX_MSG_MAP
              END_MESSAGE_MAP()
                
                /////////////////////////////////////////////////////////////////////////////
                // CMenuBar message handlers
                
                void CMenuBar::Create(CWnd *parent)
                  {
                  CDialogBar::Create(parent,IDD,CBRS_BOTTOM,6543);
                  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
                  EnableDocking(CBRS_ALIGN_ANY);
                  notify=parent;
                  mnu.LoadMenu(IDR_MAINFRAME);
                  CDC dc;
                  dc.CreateDC("DISPLAY",NULL,NULL,NULL);
                  dc.SelectStockObject(DEFAULT_GUI_FONT);
                  for (unsigned int i=0;i<mnu.GetMenuItemCount();i++)
                    {
                    char buff[128];
                    mnu.GetMenuString(i,buff,sizeof(buff),MF_BYPOSITION);	
                    CSize sz=dc.GetTextExtent(buff,strlen(buff));
                    sz.cx+=5;
                    sz.cy+=5;
                    CWnd wnd;
                    wnd.Attach(::CreateWindow("STATIC",buff,WS_CHILD|SS_CENTER|SS_CENTERIMAGE|WS_VISIBLE,0,0,sz.cx,sz.cy,(*this),(HMENU)(i+100),AfxGetInstanceHandle(),NULL));
                    wnd.SetFont(dc.GetCurrentFont(),FALSE);
                    wnd.Detach();	
                    }
                  droped=false;
                  dc.DeleteDC();
                  last=NULL;
                  
                  }

//--------------------------------------------------

void CMenuBar::OnSize(UINT nType, int cx, int cy) 
  {
  CDialogBar::OnSize(nType, cx, cy);
  
  int x=5,y=5;
  if ((HMENU)mnu!=NULL)
    for (unsigned int i=0;i<mnu.GetMenuItemCount();i++)
      {
      CWnd *wnd=GetDlgItem(i+100);
      CRect rc;
      wnd->GetWindowRect(&rc);
      CSize sz=rc.Size();
      if (x+sz.cx>cx) 
        {x=5;y+=sz.cy;}
      wnd->SetWindowPos(NULL,x,y,0,0,SWP_NOZORDER|SWP_NOSIZE);	
      x+=sz.cx;
      }
  Invalidate(TRUE);
  
  }

//--------------------------------------------------

void CMenuBar::OnLButtonDown(UINT nFlags, CPoint point) 
  {
  // TODO: Add your message handler code here and/or call default
  CWnd *wnd=ChildWindowFromPoint(point);
  if (wnd->GetDlgCtrlID()<100 || wnd->GetDlgCtrlID()>200 )
    {
    CDialogBar::OnLButtonDown(nFlags, point);
    return;
    }
  droped=true;
  last=NULL;
  OnMouseMove(nFlags,point);
  }

//--------------------------------------------------

void CMenuBar::OnMouseMove(UINT nFlags, CPoint point) 
  {
  
  CWnd *wnd=ChildWindowFromPoint(point);	
  if (wnd && (wnd->GetDlgCtrlID()<100 || wnd->GetDlgCtrlID()>200 )) wnd=NULL;
  if ((HWND)*wnd==last) 
    {
    CDialogBar::OnMouseMove(nFlags, point);
    return;
    }
  if (last)
    {
    if (droped) 
      {SendMessage(WM_CANCELMODE);wnd=NULL;}
    ::InvalidateRect(last,NULL,FALSE);
    }
  if (wnd)
    {
    CDC *dc=wnd->GetDC();
    CRect rc;
    wnd->GetClientRect(&rc);
    if (droped)
      {
      dc->DrawEdge(&rc,BDR_SUNKENINNER,BF_RECT);
      wnd->GetWindowRect(&rc);
      TrackPopupMenu(mnu.GetSubMenu(wnd->GetDlgCtrlID()-100)->m_hMenu,TPM_LEFTALIGN,rc.left,rc.bottom,0,*this,NULL);
      }
    else
      {
      dc->DrawEdge(&rc,BDR_RAISEDINNER,BF_RECT);		
      }
    wnd->ReleaseDC(dc);
    }
  last=*wnd;
  if (wnd==NULL)
    CDialogBar::OnMouseMove(nFlags, point);
  
  }

//--------------------------------------------------

BOOL CMenuBar::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
  {
  droped=false;
  
  return CDialogBar::OnSetCursor(pWnd, nHitTest, message);
  }

//--------------------------------------------------

UINT CMenuBar::OnNcHitTest(CPoint point) 
  {
  if (droped)
    {
    CPoint pt=point;
    ScreenToClient(&pt);
    OnMouseMove(0,pt);
    }
  
  return CDialogBar::OnNcHitTest(point);
  }

//--------------------------------------------------

void CMenuBar::OnEnterIdle(UINT nWhy, CWnd* pWho) 
  {
  if (nWhy==MSGF_MENU)
    {
    CPoint pt;
    GetCursorPos(&pt);
    ScreenToClient(&pt);
    OnMouseMove(0,pt);
    }
  CDialogBar::OnEnterIdle(nWhy, pWho);
  
  }

//--------------------------------------------------

