// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#define APSTUDIO_INVOKED
#undef APSTUDIO_READONLY_SYMBOLS
#define MIN_COMMAND_VALUE _APS_NEXT_COMMAND_VALUE
#define MAX_COMMAND_VALUE 0xe000
#define MIN_SCRIPT_CMD_VAL 2000
#define MAX_SCRIPT_CMD_VAL 3000
#include "Objektiv2.h"

#include "MainFrm.h"

#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include "ObjectColorizer.h"
#include "UnlockSoftDlg.h"
#include "Console.h"
#include <malloc.h>
#include <projects/ObjektivLib/GlobalFunctions.h>
#include <projects/ObjektivLib/ObjToolAnimation.h>
#include <projects/ObjektivLib/ObjToolProxy.h>
#include "DlgInputFile.h"
#include <projects\objektivlib\o2scriptlib\o2scriptclass.h>
#include <projects\objektivlib\o2scriptlib\gdLODObject.h>
#include <projects\objektivlib\o2scriptlib\gdObjectData.h>
#include <projects\objektivlib\o2scriptlib\gdSourceSafe.h>
#include <projects\bredy.libs\ProgressBar\ProgressBar.h>
#include <El/Evaluator/Addons/Dialogs/dialogs.h>
#include "dlgrunscript.h"
//#include <shlwapi.h>
#include <io.h>

#include "ExternalRequests.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CMainFrame *frame;

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

#define HOTKEYSECTION "Hotkeys"


IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
//{{AFX_MSG_MAP(CMainFrame)
ON_WM_CREATE()
ON_WM_DESTROY()
ON_COMMAND(ID_VIEW_STANDARD, OnViewStandard)
ON_COMMAND_EX(ID_SURFACES_BACKGROUNDTEXTURE, OnSelectTempMode)
ON_COMMAND_EX(ID_EDIT_SELECTFACES, OnEditSelectMode)
ON_COMMAND_EX(ID_EDIT_SELECTPOLYS, OnEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_SURFACES_BACKGROUNDTEXTURE, OnUpdateEditSelectMode)
ON_COMMAND(ID_EDIT_LASO, OnEditLaso)
ON_UPDATE_COMMAND_UI(ID_EDIT_LASO, OnUpdateEditLaso)
ON_COMMAND(ID_FILE_OPTIONS, OnFileOptions)
ON_WM_INITMENUPOPUP()
ON_COMMAND(ID_STRUCTURE_UPDATEPHASE, OnStructureUpdatephase)
ON_WM_CLOSE()
ON_WM_TIMER()
ON_COMMAND(ID_STRUCTURE_MATCHDIRECTION, OnStructureMatchdirection)
ON_MESSAGE(WM_HOTKEY, OnHotKey)
ON_COMMAND(ID_TOOLS_CALCULATOR, OnToolsCalculator)
ON_WM_SIZE()
ON_WM_QUERYENDSESSION()
ON_WM_ENDSESSION()
ON_COMMAND(ID_UPDATE_TEXLIST, OnUpdateTexlist)
ON_COMMAND(ID_POINTS_UNIFORMSCALE, OnPointsUniformscale)
ON_UPDATE_COMMAND_UI(ID_POINTS_UNIFORMSCALE, OnUpdatePointsUniformscale)
ON_MESSAGE(WM_APP1000,OnWmApp1000)
ON_COMMAND(ID_FILE_EXTENDLICENCE, OnFileExtendlicence)
ON_UPDATE_COMMAND_UI(ID_FILE_EXTENDLICENCE, OnUpdateFileExtendlicence)
ON_COMMAND(ID_SHOW_NORMALS, OnShowNormals)
ON_UPDATE_COMMAND_UI(ID_SHOW_NORMALS, OnUpdateShowNormals)
ON_COMMAND(ID_POINTS_MOVEBYNORMALS, OnPointsMovebynormals)
ON_UPDATE_COMMAND_UI(ID_POINTS_MOVEBYNORMALS, OnUpdatePointsMovebynormals)
ON_COMMAND(ID_FILE_HOTKEYS, OnFileHotkeys)
ON_COMMAND_EX(ID_VIEW_STATUS_BAR, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_TOOLBAR, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_NAMEDPROPERTIES, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_TEXLIST, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_CONTROL1, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_EDITBAR, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_HISTORY, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_ANIMATIONS, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_LODLIST, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_SELLIST, OnBarCheck)
ON_COMMAND_EX(ID_VIEW_PTINFOBAR, OnBarCheck)
ON_COMMAND_EX(ID_WINDOW_MASS, OnBarCheck)
ON_COMMAND_EX(ID_WINDOW_EXTRUDE, OnBarCheck)
ON_COMMAND_EX(ID_WINDOW_MEASURE, OnBarCheck)
ON_COMMAND_EX(ID_WINDOW_SELECTIONOPERATORS,OnBarCheck)
ON_COMMAND_EX(ID_WINDOW_HIERARCHY,OnBarCheck)
ON_COMMAND_EX(ID_WINDOW_MATERIALLIB,OnBarCheck)
ON_COMMAND_EX(ID_WINDOW_SOURCECONTROL,OnBarCheck)
ON_COMMAND_EX(ID_WINDOW_SCRIPTCONSOLE,OnBarCheck)
ON_UPDATE_COMMAND_UI(ID_VIEW_STATUS_BAR, OnUpdateControlBarMenu)
ON_UPDATE_COMMAND_UI(ID_VIEW_TOOLBAR, OnUpdateControlBarMenu)
ON_UPDATE_COMMAND_UI(ID_VIEW_NAMEDPROPERTIES, OnUpdateControlBarMenu)
ON_UPDATE_COMMAND_UI(ID_VIEW_TEXLIST, OnUpdateControlBarMenu)
ON_UPDATE_COMMAND_UI(ID_VIEW_CONTROL1, OnUpdateControlBarMenu)
ON_UPDATE_COMMAND_UI(ID_VIEW_EDITBAR, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_VIEW_HISTORY, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_VIEW_ANIMATIONS, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_VIEW_LODLIST, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_VIEW_SELLIST, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_VIEW_PTINFOBAR, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_WINDOW_MASS, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_WINDOW_EXTRUDE, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_WINDOW_MEASURE, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_WINDOW_HIERARCHY, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_WINDOW_SELECTIONOPERATORS, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_WINDOW_MATERIALLIB, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_WINDOW_SOURCECONTROL, OnUpdateControlBarMenu)	
ON_UPDATE_COMMAND_UI(ID_WINDOW_SCRIPTCONSOLE, OnUpdateControlBarMenu)	
ON_COMMAND_EX(ID_CREATE_TRIANGLE, OnEditSelectMode)
ON_COMMAND_EX(ID_CREATE_STRIP, OnEditSelectMode)
ON_COMMAND_EX(ID_CREATE_FAN, OnEditSelectMode)
ON_COMMAND_EX(ID_CREATE_QUAD, OnEditSelectMode)
ON_COMMAND_EX(ID_DRAW_POLYGON, OnEditSelectMode)
ON_COMMAND_EX(ID_EDIT_SELECTOBJECTS, OnEditSelectMode)
ON_COMMAND_EX(ID_EDIT_SELECTVERTICES, OnEditSelectMode)
ON_COMMAND_EX(ID_EDIT_TOUCH, OnEditSelectMode)
ON_COMMAND_EX(ID_EDIT_EDITMODES_TOUCHEDGES, OnEditSelectMode)
ON_COMMAND_EX(ID_EDIT_PIN, OnEditSelectMode)
ON_COMMAND_EX(ID_EDIT_PAINTVERTICES, OnEditSelectMode)
ON_COMMAND_EX(ID_SURFACES_CYLMAP, OnSelectTempMode)
ON_COMMAND_EX(ID_VIEW_ZOOM, OnSelectTempMode)
ON_UPDATE_COMMAND_UI(ID_EDIT_PAINTVERTICES, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_CREATE_TRIANGLE, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_CREATE_STRIP, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_CREATE_FAN, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_CREATE_QUAD, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_DRAW_POLYGON, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_EDIT_SELECTFACES, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_EDIT_SELECTPOLYS, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_EDIT_SELECTOBJECTS, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_EDIT_TOUCH, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_EDIT_EDITMODES_TOUCHEDGES, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_EDIT_SELECTVERTICES, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_EDIT_PIN, OnUpdateEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM, OnUpdateEditSelectMode)
ON_MESSAGE(HKK_ACCELCHANGED,OnChangeAccelTable)
ON_COMMAND(ID_MACRO_RECORDMACRO,OnMacroRecordmacro)
ON_COMMAND(ID_MACRO_PLAYLASTMACRO,OnMacroPlaymacro)
ON_UPDATE_COMMAND_UI(ID_MACRO_RECORDMACRO,OnUpdateMacroRecordmacro)
ON_UPDATE_COMMAND_UI(ID_MACRO_PLAYLASTMACRO,OnUpdateMacroPlaymacro)
ON_MESSAGE(MSG_UPDATESTATUS,OnUpdateStatusMsg)
ON_MESSAGE(MSG_ENDSECTIONSCALC,OnEndCalculationSections)
ON_MESSAGE(MSG_CALCULATESECTIONS ,OnCalculateSections)
ON_WM_ACTIVATE()

//}}AFX_MSG_MAP
ON_COMMAND_EX(ID_EDITMODES_TURNEDGE, OnEditSelectMode)
ON_UPDATE_COMMAND_UI(ID_EDITMODES_TURNEDGE, OnUpdateEditSelectMode)
ON_COMMAND(ID_EXTVIEWER_START, OnExtviewerStart)
ON_COMMAND(ID_EXTRACOMMANDS_ATTACH, OnExtracommandsAttach)
ON_COMMAND(ID_EXTRACOMMANDS_DETACH, OnExtracommandsDetach)
ON_UPDATE_COMMAND_UI(ID_EXTRACOMMANDS_ATTACH, OnExtViewerNotRunning)
ON_UPDATE_COMMAND_UI(ID_EXTRACOMMANDS_DETACH, OnExtViewerRunning)
ON_UPDATE_COMMAND_UI(ID_OTHER_STARTNEWINSTANCE, OnExtViewerNotRunning)
ON_COMMAND(ID_EXTRACOMMANDS_STOP, OnExtracommandsStop)
ON_UPDATE_COMMAND_UI(ID_EXTRACOMMANDS_STOP, OnExtViewerRunning)
ON_COMMAND(ID_EXTRACOMMANDS_STARTSCRIPT, OnExtracommandsStartscript)
ON_UPDATE_COMMAND_UI(ID_EXTRACOMMANDS_STARTSCRIPT, OnExtViewerRunning)
ON_COMMAND(ID_EXTRACOMMANDS_REFRESH, OnExtracommandsRefresh)
ON_UPDATE_COMMAND_UI(ID_EXTRACOMMANDS_REFRESH, OnExtViewerRunning)
ON_COMMAND_RANGE(ID_EXTVIEWER_HIDETEXTURES,ID_EXTVIEWER_DISABLEMODELCFG,OnExtViewerDisplayFlags)
ON_UPDATE_COMMAND_UI_RANGE(ID_EXTVIEWER_HIDETEXTURES,ID_EXTVIEWER_DISABLEMODELCFG,OnUpdateExtViewerDisplayFlags)
ON_COMMAND_RANGE(ID_EXTVIEWER_MASKTEXTURE,ID_EXTVIEWER_MASKWFRAME,OnExtViewerSelectMask)
ON_UPDATE_COMMAND_UI_RANGE(ID_EXTVIEWER_MASKTEXTURE,ID_EXTVIEWER_MASKWFRAME,OnUpdateExtViewerSelectMask)
ON_COMMAND_RANGE(ID_EXTVIEWER_AUTOCENTER,ID_EXTVIEWER_CENTERSELECTION,OnExtViewerCenter)
ON_UPDATE_COMMAND_UI_RANGE(ID_EXTVIEWER_AUTOCENTER,ID_EXTVIEWER_CENTERSELECTION,OnUpdateExtViewerCenter)
ON_COMMAND_RANGE(ID_MENUSFORHOTKEYS_BULDOZERMENU,ID_MENUSFORHOTKEYS_OTHERMENU,OnExtViewerMenuFor)
ON_COMMAND_RANGE(ID_EXTVIEWEROTHER_SETMASKTEXTURE,ID_EXTVIEWEROTHER_SETMASKWFTEXTURE,OnExtViewerSetTexMat)
ON_COMMAND(ID_EXTVIEWER_SETWEATHER,OnExtViewerSetWeather)
ON_COMMAND_RANGE(MIN_COMMAND_VALUE,MAX_COMMAND_VALUE,OnPluginCommand)
ON_UPDATE_COMMAND_UI_RANGE(MIN_COMMAND_VALUE,MAX_COMMAND_VALUE,OnUpdatePluginCommand)
ON_COMMAND(ID_MANAGESCRIPTS, OnManagescripts)
ON_COMMAND(ID_SCRIPTS_EDITSCRIPT, OnScriptsEditscript)
ON_COMMAND(ID_SCRIPTS_RUNSCRIPT, OnScriptsRunscript)
ON_COMMAND_RANGE(MIN_SCRIPT_CMD_VAL,MAX_SCRIPT_CMD_VAL,OnScriptsRunMenuScript)
ON_COMMAND(ID_SCRIPTS_CONSOLEFONT, OnScriptsConsolefont)
ON_COMMAND(ID_EXVIEWER_GROUNDFILE, OnExviewerGroundfile)
ON_COMMAND_RANGE(ID_EXVIEWER_GROUNDDEFAULT,ID_EXVIEWER_GROUNDDISABLED, OnExviewerGrounddefault)
ON_UPDATE_COMMAND_UI_RANGE(ID_EXVIEWER_GROUNDDEFAULT, ID_EXVIEWER_GROUNDDISABLED,OnUpdateExviewerGrounddefault)
ON_MESSAGE(O2M_OPENRVMATFILE,OnOpenRVMATRequest)
ON_MESSAGE(MSG_EXVIEWERUPDATESTATUS,OnExViewerUpdateStatus)
ON_COMMAND(ID_EXTVIEWER_MARKERBROWSE, OnExtviewerMarkerbrowse)
ON_COMMAND(ID_OTHER_STARTNEWINSTANCE, OnOtherStartnewinstance)
//ON_COMMAND(ID_SOURCECONTROL_ENABLED, OnSourcecontrolEnabled)
//ON_UPDATE_COMMAND_UI(ID_SOURCECONTROL_ENABLED, OnUpdateSourcecontrolEnabled)
//ON_COMMAND(ID_SOURCECONTROL_ENABLED, OnSourcecontrolEnabled)
ON_COMMAND(ID_RTMPREVIEW_LOADRTM, OnExtViewerLoadRTM)
ON_COMMAND(ID_RTMPREVIEW_CLEARRTM, OnExtViewerClearRTM)
ON_UPDATE_COMMAND_UI(ID_RTMPREVIEW_LOADRTM, OnUpdateExtViewerLoadRTM)
ON_UPDATE_COMMAND_UI(ID_RTMPREVIEW_CLEARRTM, OnUpdateExtViewerClearRTM)
ON_MESSAGE(MSG_BGRYIELD,OnMessageBgrYield)

ON_COMMAND(ID_UVSETS_UVEDITOR, OnUvsetsUveditor)
ON_COMMAND(ID_MACRO_LOADMACRO, OnMacroLoadmacro)
ON_COMMAND(ID_MACRO_SAVELASTMACROAS, OnMacroSavelastmacroas)
ON_UPDATE_COMMAND_UI(ID_MACRO_SAVELASTMACROAS,OnUpdateMacroPlaymacro)
ON_COMMAND_RANGE(ID_PLAYMACRO_FOREACHLOD,ID_PLAYMACRO_FOREACHGEOMETRYLOD,OnPlaymacroForeachlod)
ON_UPDATE_COMMAND_UI_RANGE(ID_PLAYMACRO_FOREACHLOD,ID_PLAYMACRO_FOREACHGEOMETRYLOD,OnUpdateMacroPlaymacro)
ON_COMMAND(ID_PLAYMACRO_FOREACHANIMATION, OnPlaymacroForeachanimation)
ON_UPDATE_COMMAND_UI(ID_PLAYMACRO_FOREACHANIMATION,OnUpdateMacroPlaymacro)
ON_COMMAND(ID_SELECTIONMASKING_INVERTSELECTION, OnSelectionmaskingInvertselection)
ON_UPDATE_COMMAND_UI(ID_SELECTIONMASKING_INVERTSELECTION, OnUpdateSelectionmaskingInvertselection)
END_MESSAGE_MAP()

static UINT indicators[] =
  {
  ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
  };

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction


static void ObjectData_NoCoordinatePointsError(const char *name)
   {  
   CString s;
   AfxFormatString1(s,IDS_NOCOORDINATESPOINTSIN,name);
   AfxMessageBox(s,MB_OK|MB_ICONEXCLAMATION);
   }

   CMainFrame::CMainFrame():_externalViewer(_MAT),_MAT(theApp.FindPluginService(Service_MaterialEditor))
  {
  // TODO: add member initialization code here
  frame=this;	
  laso=false;
  PrevEditCommand=EditCommand=ID_EDIT_SELECTOBJECTS;
  initphase=true;
  uniformscale=true;
  movebynormals=false;
  paintweight=1.0f;
  paintradiusmin=10;
  paintradiusmax=20;
  paintstrength=0.2f;
  userhkeys=NULL;
  disableMenu=false;
  }

CMainFrame::~CMainFrame()
  {
  }

#define InstallDialogBar(name,IDD,menuid,align,dock)\
  name.Create(this,IDD,align,menuid);\
  name.SetBarStyle(name.GetBarStyle() |\
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);\
		name.EnableDocking(dock);\
		
#define InstallDialogBar2(name,IDD,menuid,align)\
InstallDialogBar(name,IDD,menuid,align,CBRS_ALIGN_ANY)

#define InstallToolBar(name,IDD,align,menuid)\
  if (name.Create(this,WS_CHILD | WS_VISIBLE | align, menuid ) && \
		name.LoadToolBar(IDD))\
  {\
  name.SetBarStyle(name.GetBarStyle() |\
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);\
		name.EnableDocking(CBRS_ALIGN_ANY);\
	}	 

class FunctorOnOpen
{
HWND wnd;
public:
  FunctorOnOpen(HWND wnd):wnd(wnd) {}
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    plugin->GetPlugin().AfterOpen(wnd);
    return false;
  }

};

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
  {  


  CreateWindow("STATIC",O2EXTERNALNOTIFIER,WS_OVERLAPPED,0,0,0,0,*this,NULL,AfxGetInstanceHandle(),NULL);
  ObjData_NoCoordinatePointsMessage=ObjectData_NoCoordinatePointsError;
  ObjToolProxy::ProxyPrefix=StrRes[IDS_PROXYNAME];


  LoadViewerConfig();
  _externalViewer.SetViewerSettings(&_externalCfg);

  if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
    return -1;
  
  if (config->GetBool(IDS_CFGDONTSTARTVIEWER)==false)
    PostMessage(WM_COMMAND,ID_EXTVIEWER_START,0);
  
  LoadWindowSize(this, FRAMENAME);
  if (!m_wndToolBar.Create(this) ||
    !m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
    {
	TRACE0("Failed to create toolbar\n");
	return -1;      // fail to create
    }
  
  if (!m_wndStatusBar.Create(this) ||
    !m_wndStatusBar.SetIndicators(indicators,
	sizeof(indicators)/sizeof(UINT)))
	{
    TRACE0("Failed to create status bar\n");
    return -1;      // fail to create
	}
  
  // TODO: Remove this if you don't want tool tips or a resizeable toolbar
  m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
    CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  
  // TODO: Delete these three lines if you don't want the toolbar to
  //  be dockable
  focusicon=LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_FOCUS));
  SetTimer(10,1000,NULL);
  m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
  m_wndNamedPropBar.Create(this,ID_VIEW_NAMEDPROPERTIES);
  m_wndTexList.Create(this,ID_VIEW_TEXLIST);
  m_wndLodList.Create(this,ID_VIEW_LODLIST);
  m_wndSelBar.Create(this,ID_VIEW_SELLIST);
  m_wndHistoryBar.Create(this,ID_VIEW_HISTORY);
  m_wndPtInfo.Create(this,ID_VIEW_PTINFOBAR,doc);
  m_wndMatLib.Create(this,ID_WINDOW_MATERIALLIB);

#ifdef FULLVER
  m_wndAnimListBar.Create(this,ID_VIEW_ANIMATIONS);
#endif
  m_wndMassBar.Create(this,ID_WINDOW_MASS);
#ifdef FULLVER
  m_wndExtrude.Create(this,ID_WINDOW_EXTRUDE);
  m_wndMeasureBar.Create(this,ID_WINDOW_MEASURE);
  m_wndHierarchy.Create(this,ID_WINDOW_HIERARCHY);
  _scriptConsole.Create(this,ID_WINDOW_SCRIPTCONSOLE);
#endif
  LoadBarSize(m_wndTexList,160,0);
  LoadBarSize(m_wndNamedPropBar,160,0);
  InstallToolBar(m_wndControlBar1,IDR_CONTROL1,CBRS_TOP,ID_VIEW_CONTROL1);
  InstallToolBar(m_wndEditBar,IDR_EDITBAR,CBRS_TOP,ID_VIEW_EDITBAR);
  InstallDialogBar2(m_wndLogoSmall,IDD_LOGOSMALL,10,CBRS_TOP);  
#ifdef FULLVER
  InstallToolBar(m_wndSelOp,IDR_SELOPERATIONS,CBRS_TOP,ID_WINDOW_SELECTIONOPERATORS);
  InstallToolBar(m_wndSourceControl,IDR_SOURCECONTROL,CBRS_TOP,ID_WINDOW_SOURCECONTROL);
#endif
  EnableDocking(CBRS_ALIGN_ANY);
  DockControlBar(m_wndNamedPropBar,1);
  DockControlBar(m_wndMassBar,1);
  DockControlBar(m_wndTexList,1);
#ifdef FULLVER
  DockControlBar(m_wndMeasureBar,1);
  DockControlBar(m_wndHierarchy,1);
#endif
  DockControlBar(m_wndLodList);
  DockControlBar(m_wndSelBar,m_wndLodList,0,1);
  DockControlBar(m_wndHistoryBar,m_wndSelBar,0,1);
#ifdef FULLVER
  DockControlBar(m_wndAnimListBar,m_wndHistoryBar,0,1,true);
#endif
  DockControlBar(m_wndToolBar);
  DockControlBar(m_wndEditBar,m_wndToolBar,1,0);
  DockControlBar(m_wndControlBar1,m_wndToolBar,0,1);
#ifdef FULLVER
  DockControlBar(m_wndSelOp,m_wndControlBar1,1,0,true);
  DockControlBar(m_wndSourceControl,m_wndControlBar1,1,0,true);
  DockControlBar(m_wndExtrude,m_wndSelOp,1,0);
  DockControlBar(m_wndMatLib,true);
  DockControlBar(_scriptConsole,1);
#endif
  DockBarRight(m_wndPtInfo,m_wndControlBar1);	
  m_wndLogoSmall.m_sizeDefault.cx=64;
  m_wndLogoSmall.m_sizeDefault.cy=32;
  m_wndLogoSmall.SetWindowPos(NULL,0,0,m_wndLogoSmall.m_sizeDefault.cx,m_wndLogoSmall.m_sizeDefault.cy,SWP_NOZORDER|SWP_NOMOVE);
  (m_wndLogoSmall.GetDlgItem(IDC_LOGO))->SetWindowPos(NULL,0,0,64,32,SWP_NOZORDER);
  (m_wndLogoSmall.GetDlgItem(IDC_LOGO2))->SetWindowPos(NULL,0,0,64,32,SWP_NOZORDER);
  DockBarRight(m_wndLogoSmall,m_wndToolBar,false);
  if ((GetKeyState(VK_SHIFT) & 0x80)==0) 	
    LoadBarState(FRAMENAME);
	/*	theApp.BarsOtherInfo(this,false,
	ID_VIEW_NAMEDPROPERTIES,ID_VIEW_TEXLIST,ID_VIEW_TEXLIST,
	ID_VIEW_LODLIST,ID_VIEW_SELLIST,ID_VIEW_HISTORY,
	ID_VIEW_PTINFOBAR,ID_VIEW_ANIMATIONS,ID_WINDOW_MASS,
  ID_WINDOW_EXTRUDE,ID_VIEW_CONTROL1,ID_VIEW_EDITBAR,-1);*/
  if (config->GetBool(IDS_CFGSTARTINFOURVIEWS))
    PostMessage(WM_COMMAND,ID_VIEW_STANDARD,0);
  comint.SetNotify(this,ID_UPDATE_MODEL);
  hotreg=false;
  PostMessage(WM_COMMAND,ID_UPDATE_TEXLIST);
  /*#ifndef FULLVER
  {
  long expday;
  pp_daysleft(prot_handle,&expday);
  if (expday<365)
  CMessageWnd::ShowMessage(WString(IDS_EXPIRATIONDAYS,expday),10000);
  }
#endif*/

  LOGFONT *fnt;
  UINT fntsize;
  theApp.GetProfileBinary("Settings","ConsoleFont",(LPBYTE *)&fnt,&fntsize);
  if (fntsize==sizeof(LOGFONT)) _scriptConsole.SetTextFont(*fnt);
  delete fnt;

  return 0;
}


BOOL CMainFrame::LoadFrame(UINT nIDResource,
				DWORD dwDefaultStyle,
				CWnd* pParentWnd ,
				CCreateContext* pContext)
	  {
	  BOOL ok=CFrameWnd::LoadFrame(nIDResource,dwDefaultStyle,pParentWnd,pContext);
      _dlgmanscr.LoadRegistry();
      _dlgmanscr.InitMenu(GetMenu()->GetSubMenu(IPluginGeneralAppSide::mnuAutomation),MIN_SCRIPT_CMD_VAL,0,3);
      if (ok) this->LoadHotkeys();
	  return ok;
	  }


BOOL CMainFrame::OnCreateClient( LPCREATESTRUCT /*lpcs*/,
								CCreateContext* pContext)
  {
  return m_wndSplitter.Create( this,
	2, 2,                 // TODO: adjust the number of rows, columns
	CSize( 10, 10 ),      // TODO: adjust the minimum pane size
	pContext );
  }

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
  {
  // TODO: Modify the Window class or styles here by modifying
  //  the CREATESTRUCT cs
  
  return CFrameWnd::PreCreateWindow(cs);
  }

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
  {
  CFrameWnd::AssertValid();
  }

void CMainFrame::Dump(CDumpContext& dc) const
  {
  CFrameWnd::Dump(dc);
  }

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnDestroy() 
  {
  CFrameWnd::OnDestroy();
  }

void CMainFrame::OnViewStandard() 
  {
  RECT rc;
  m_wndSplitter.GetClientRect(&rc);
  if (m_wndSplitter.GetRowCount()<2) m_wndSplitter.SplitRow(rc.bottom/2);
  if (m_wndSplitter.GetColumnCount()<2) m_wndSplitter.SplitColumn(rc.right/2);
  CObjektiv2View *pane;
  if (m_wndSplitter.GetRowCount()<2 || m_wndSplitter.GetColumnCount()<2) return;
  pane=(CObjektiv2View *)m_wndSplitter.GetPane(0,0);
  pane->SetView(ID_VIEW_FRONT);
  pane=(CObjektiv2View *)m_wndSplitter.GetPane(0,1);
  pane->SetView(ID_VIEW_LEFT);
  pane=(CObjektiv2View *)m_wndSplitter.GetPane(1,0);
  pane->SetView(ID_VIEW_TOP);
  pane=(CObjektiv2View *)m_wndSplitter.GetPane(1,1);
  pane->SetView(ID_VIEW_PROJECTION);
  
  }

BOOL CMainFrame::OnSelectTempMode(UINT id) 
  {
  if (GetEditCommand()!=id) EditCommand=id;else EditCommand=GetPrevEditCommand();
  if (id==ID_SURFACES_BACKGROUNDTEXTURE) doc->UpdateAllViews(NULL);
  if (id==ID_VIEW_ZOOM) 
	{laso=false;}
  return TRUE;
  }

BOOL CMainFrame::OnEditSelectMode(UINT id)
  {
  if (EditCommand==ID_EDIT_PAINTVERTICES) CObjektiv2View::lastcursview->InvalidateRect(NULL,FALSE);
  EditCommand=PrevEditCommand=id;
  if (id==ID_EDIT_PAINTVERTICES && paintdlg.m_hWnd==NULL)
    paintdlg.Create(paintdlg.IDD);
  return TRUE;
  }

void CMainFrame::OnUpdateEditSelectMode(CCmdUI* pCmdUI) 
  {
  pCmdUI->SetCheck(pCmdUI->m_nID==EditCommand);
  }

void CMainFrame::OnFileOpen() 
  {
  CString defext;defext.LoadString(IDS_FILEDEFEXT);
  CString filters;filters.LoadString(IDS_FILEOPENFILTERS);
  CFileDialogEx fdlg(TRUE,defext,NULL,OFN_CREATEPROMPT|OFN_LONGNAMES,
	filters,NULL);
  if (fdlg.DoModal()!=IDOK) return;
  if (doc->OnNewDocument()==false) return;
  doc->LoadDocument(Pathname(fdlg.GetPathName()));  
  }

void CMainFrame::UpdateXYZ(HVECTOR v)
  {
  char buff[50];
  sprintf(buff,"%.3f",v[XVAL]);m_wndPtInfo.SetDlgItemText(IDC_XINFO,buff);
  sprintf(buff,"%.3f",v[YVAL]);m_wndPtInfo.SetDlgItemText(IDC_YINFO,buff);
  sprintf(buff,"%.3f",v[ZVAL]);m_wndPtInfo.SetDlgItemText(IDC_ZINFO,buff);
  if (m_wndMeasureBar.IsVisible())
    m_wndMeasureBar.UpdateValues(doc->pin,v);
  }

void CMainFrame::OnEditLaso() 
  {
  laso=!laso;	
  }

void CMainFrame::OnUpdateEditLaso(CCmdUI* pCmdUI) 
  {
  pCmdUI->SetCheck(laso==true);
  pCmdUI->Enable(EditCommand==ID_EDIT_SELECTOBJECTS || 
    EditCommand==ID_EDIT_SELECTFACES|| 
	EditCommand==ID_EDIT_SELECTVERTICES);
  }

void CMainFrame::OnFileOptions() 
  {
  config->DoModal();
  LoadViewerConfig();
  doc->UpdateAll();
  doc->UpdateAllToolbars();
  doc->UpdateFileSSStatus();
  }

void CMainFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
  {
  CFrameWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
  if (disableMenu)
  {
    int cnt=pPopupMenu->GetMenuItemCount();
    for (int i=0;i<cnt;i++) if (pPopupMenu->GetSubMenu(i)==NULL)
      pPopupMenu->EnableMenuItem(i,MF_BYPOSITION|MF_GRAYED);
  }
  else
    doc->OnInitMenu(pPopupMenu);
  }

void CMainFrame::OnStructureUpdatephase() 
  {
  m_wndAnimListBar.UpdatePhase();
  }

class FunctorAfterClose
{
public:  
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    plugin->GetPlugin().AfterClose();
    return false;
  }
};


void CMainFrame::OnClose() 
  {
  if (comint.IsLocked()) return;
  if (m_wndTexList.scan) 
	{
    m_wndTexList.scan=false;
    PostMessage(WM_CLOSE);
    return ;
	}
  bool canClose=true;
  EnableWindow(FALSE);
  theApp.ForEachPlugin(FunctorCloseAllPlugins(canClose));
  EnableWindow(TRUE);
  if (!canClose) return;
  EnableWindow(FALSE);
  if (_dlguveditor.GetSafeHwnd()) _dlguveditor.DestroyWindow();
  if (doc->OnNewDocument()==FALSE)
  {
    EnableWindow(TRUE);
    return;
  }
  if (!IsIconic()) SaveBarState(FRAMENAME);
  theApp.ForEachPlugin(FunctorAfterClose());
  SaveWindowSize(this, FRAMENAME);
  ProgressBar<int> pb(1);
  pb.ReportState(IDS_EXITTING);


  UnregisterHotKey(*this,0x00C0);
  CFrameWnd::OnClose();
  }

void CMainFrame::RestartAutosaveTimer()
  {
  KillTimer(asavetimer);
  int interval=config->GetInt(IDS_CFGAUTOSAVEMIN);
  if (interval) asavetimer=SetTimer(1,interval*1000*60,NULL);
  }

void CMainFrame::OnTimer(UINT nIDEvent) 
  {
  theApp.idlecnt++;
  static bool swapper=false;
  if ((HWND)theApp.splash!=NULL) theApp.splash.DestroyWindow();
  CWnd *w=GetForegroundWindow();
  if (_externalViewer.HasFocus()==_externalViewer.ErrOK || w==this || (w!=NULL && w->GetOwner()==this))
    if (hotreg==false)
	  {
      RegisterHotKey(*this,0x00C0,0,0x00C0);
      hotreg=true;
	  }
	else;
	else if(hotreg==true)
	  {
	  UnregisterHotKey(*this,0x00C0);
	  hotreg=false;
	  }
	if (nIDEvent==10)
	  {
	  CWnd *status=GetMessageBar();
	  CString s1,s2;
	  status->GetWindowText(s1);
	  s2.LoadString(AFX_IDS_IDLEMESSAGE);
	  if (doc->LodData)
		{
        if (s1==s2 || doc->extupdate) doc->UpdateStatus();
		if (doc->extupdate)
          if (_externalViewer.IsReady()==_externalViewer.ErrOK) 
		  {
		  doc->UpdateModel();
		  if (!doc->filename.IsNull() && doc->GetPathName()!=doc->filename) doc->SetPathName(doc->filename);
		  }
          else
          {
            if (_externalViewer.IsReady()==_externalViewer.ErrNotInicialized)
            {
              ChangeViewerStatus(CStatusLight::statusNA);
              doc->extupdate=false;
            }
          }
		if (comint.sectchanged) 
		  {
          doc->numSections=-1;            
          doc->UpdateStatus();
          SendMessage(MSG_CALCULATESECTIONS);
		  }
		if (m_wndTexList.needredraw) 
		  {m_wndTexList.Invalidate(false);   m_wndTexList.needredraw=false;}	  
		CRect rc;
		CRect rc2;
		m_wndLogoSmall.GetWindowRect(&rc2);
		GetClientRect(&rc);	  
		ClientToScreen(&rc);	  	  	  
		rc.bottom=rc.top+(rc2.bottom-rc2.top);
		rc.left=rc.right-(rc2.right-rc2.left);
		if (abs(rc.left-rc2.left)>5)  CFrameWnd::DockControlBar(&m_wndLogoSmall,(UINT)0,&rc);
		if (!doc->LodData->Dirty())
		  {
		  (m_wndLogoSmall.GetDlgItem(IDC_LOGO))->ShowWindow(SW_SHOW);
		  (m_wndLogoSmall.GetDlgItem(IDC_LOGO2))->ShowWindow(SW_HIDE);
		  }
		else
		  {
		  (m_wndLogoSmall.GetDlgItem(IDC_LOGO2))->ShowWindow(SW_SHOW);
		  (m_wndLogoSmall.GetDlgItem(IDC_LOGO))->ShowWindow(SW_HIDE);
		  }
		}
		/*	CWnd *wnd=GetFocus();
		if (wnd!=NULL)
		{
		CRect rc;
		CDC *dc=wnd->GetDC();
		GetClientRect(&rc);
		dc->DrawIcon(rc.right-32,rc.top,focusicon);
		wnd->ReleaseDC(dc);
		if (lastfocus!=*wnd) ::InvalidateRect(lastfocus,NULL,TRUE);
		lastfocus=*wnd;
	  }*/
	  }
	if (nIDEvent==asavetimer) doc->Autosave();
	if (initphase)
	  {
	  initphase=false;
      theApp.ForEachPlugin(FunctorOnOpen(*this));
	  if (theApp.GetProfileInt("CConfig","FirstRun",0)==0)
		{
#ifdef PUBLIC
		AfxMessageBox(IDS_FORCECONFIG_PUBLIC,MB_OK);
#else
    AfxMessageBox(IDS_FORCECONFIG,MB_OK);
#endif
		config->DoModal();
        LoadViewerConfig();
		theApp.WriteProfileInt("CConfig","FirstRun",1);
		}

    // Correct the application menu based on configuration file
    theApp.CorrectApplicationMenuBasedOnConfigFile();

	  if (theApp.startuptoload!="") doc->LoadDocument(Pathname(theApp.startuptoload));
	  }
	else CFrameWnd::OnTimer(nIDEvent);  
  }

#include "MatchDirection.h"

void CMainFrame::OnStructureMatchdirection() 
  {
  static CMatchDirection dlg;
  dlg.DoModal();
  }

LRESULT CMainFrame::OnHotKey(WPARAM wParam,LPARAM lParam)
  {
  if (_externalViewer.HasFocus()==_externalViewer.ErrOK)
    SetForegroundWindow();  
  else
	{
    SetForegroundWindow();
    _externalViewer.SwitchToViewer();
	}
  return 0;
  }

#include "calculator\CalculatorDlg.h"
#include ".\mainfrm.h"

void CMainFrame::OnToolsCalculator() 
  {
  CCalculatorDlg dlg;
  if (dlg.DoModal()==IDCANCEL) return;
  HMATRIX mm;
  LPHMATRIX lp;
  if (dlg.camtransf)
	{
    CObjektiv2View *cur=CObjektiv2View::GetCurrentView();
    lp=cur->camera.GetTransform();
	}
  else
	{
    JednotkovaMatice(mm);
    lp=mm;
	}
  comint.Begin(WString(IDS_CALCULATOR));
  comint.Run(new CICalcUse(dlg.outname,dlg.selonly!=0,dlg.alllods!=0,dlg.allanims!=0,lp,doc->pin));
  doc->UpdateAllViews(NULL);
  }

void CMainFrame::DockControlBar(CControlBar &dock, CControlBar &relto, int xrel, int yrel,bool hide)
  {
  CRect rc;
  CRect p;
  CFrameWnd::DockControlBar(&dock);
  relto.GetWindowRect(&rc);
  dock.GetWindowRect(&p);
  rc+=CSize((rc.right-rc.left+10)*xrel,(rc.bottom-rc.top+10)*yrel);
  rc.right=rc.left+p.right-p.right;
  rc.bottom=rc.top+p.bottom-p.top;
  CFrameWnd::DockControlBar(&dock,(UINT)0,&rc);
  if (hide) 
    ShowControlBar(&dock,FALSE,FALSE);
  else
    ShowControlBar(&dock,TRUE,FALSE);
  }

void CMainFrame::DockControlBar(CControlBar &bar,bool hide)
  {
  CFrameWnd::DockControlBar(&bar);
  if (hide) 
    ShowControlBar(&bar,FALSE,FALSE);
  else
    ShowControlBar(&bar,TRUE,FALSE);
  }

void CMainFrame::DockBarRight(CControlBar &dock,CControlBar &relto,bool hide)
  {
  CRect rc;
  CRect p,w;
  CFrameWnd::DockControlBar(&dock);
  relto.GetWindowRect(&rc);
  dock.GetWindowRect(&p);
  GetWindowRect(&w);
  rc.right=w.right;
  rc.left=rc.right-10;
  CFrameWnd::DockControlBar(&dock,(UINT)0,&rc);
  if (hide) 
    ShowControlBar(&dock,FALSE,FALSE);
  else
    ShowControlBar(&dock,TRUE,FALSE);
  
  }

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
  {
  if (GetControlBar(10)!=NULL)
    if (nType==SIZE_MINIMIZED) 
      SaveBarState(FRAMENAME);
	else if (laststate==SIZE_MINIMIZED)
	  LoadBarState(FRAMENAME);
	CFrameWnd::OnSize(nType, cx, cy);
	laststate=nType;
  }

BOOL CMainFrame::OnQueryEndSession() 
  {
  if (!CFrameWnd::OnQueryEndSession())
    return FALSE;	
  return doc->OnNewDocument();
  }

void CMainFrame::OnEndSession(BOOL bEnding) 
  {
  if (bEnding)
	{
    CFrameWnd::OnEndSession(bEnding);
    if (!IsIconic()) SaveBarState(FRAMENAME);
    SaveWindowSize(this, FRAMENAME);
	}
  }

void CMainFrame::OnUpdateTexlist()
  {
  m_wndTexList.CheckForNewTextures();
  m_wndTexList.UpdateList();
  m_wndTexList.UpdateLibraryList();
  }

void CMainFrame::OnSelopChange(UINT id) 
  {
  seloperator=id;
  }

void CMainFrame::OnUpdateSelop(CCmdUI* pCmdUI)
  {
  pCmdUI->SetCheck(seloperator==pCmdUI->m_nID);
  }

/*Selection *CombineSelection(int seloperator, Selection *current, Selection *nwsel)
  {
  int i;
  const ObjectData *obj=current->GetObject();
  if (obj!=nwsel->GetObject()) return nwsel;
  int fcnt=obj->NFaces();
  int pcnt=obj->NPoints();
  switch (seloperator)
	{
    case ID_SELOP_NONE: return nwsel;
    case ID_SELOP_UNION: (*current)+=*nwsel;break;
    case ID_SELOP_COMPLEMENT:(*current)-=*nwsel;break;
    case ID_SELOP_INTERSECTION:
	  for (i=0;i<pcnt;i++)		
		current->SetPointWeight(i,current->PointWeight(i) * nwsel->PointWeight(i));
	  for (i=0;i<fcnt;i++)		
		current->FaceSelect(i,current->FaceSelected(i) & nwsel->FaceSelected(i));
	  break;
    case ID_SELOP_COMPINTER:
	  for (i=0;i<pcnt;i++)		
		current->SetPointWeight(i,(float)fabs(current->PointWeight(i)-nwsel->PointWeight(i)));
	  for (i=0;i<fcnt;i++)		
		current->FaceSelect(i,(current->FaceSelected(i) ^ nwsel->FaceSelected(i)));
	  break;
	}
  return current;
  }
*/

void CMainFrame::OnPointsUniformscale() 
  {
  uniformscale=!uniformscale;	
  }

void CMainFrame::OnUpdatePointsUniformscale(CCmdUI* pCmdUI) 
  {
  pCmdUI->SetCheck((int)uniformscale);
  }

LRESULT CMainFrame::OnWmApp1000(WPARAM wParam,LPARAM lParam)
  {
  PostThreadMessage(lParam,WM_APP+1000,0,0);
  return 0;
  }

void CMainFrame::OnFileExtendlicence() 
  {
  /*#ifndef FULLVER
  if (doc->LodData->Dirty())
  {	  
  int id=WMessageBox::Messagef(NULL,MB_YESNOCANCEL|MB_ICONQUESTION,::AfxGetAppName(),WString(IDS_SAVEQUESTION),doc->GetPathName());
  switch (id)
		{
		case IDCANCEL: return;
		case IDYES:if (doc->AskSave()==FALSE) return ;break;
		case IDNO: break;
		}
		}  
		CUnlockSoftDlg dlg;  	
		dlg.forceextend=true;
		dlg.timeexpand=true;
		dlg.DoModal();
		long expday;
		pp_daysleft(prot_handle,&expday);
		CMessageWnd::ShowMessage(WString(IDS_EXPIRATIONDAYS,expday),10000);
		#endif
  */
  AfxMessageBox("This version haven't any licence");
  }

void CMainFrame::OnUpdateFileExtendlicence(CCmdUI* pCmdUI) 
  {
  /*#ifndef FULLVER
  pCmdUI->Enable(PP_TRUE==pp_copycheckth(prot_handle, ACTION_MANUAL ,prot_componumber, 100));	
#endif*/
  pCmdUI->Enable(FALSE);
  }

void CMainFrame::OnShowNormals() 
  {
  if (config->GetBool(IDS_CFGVIEWNORMALS))  config->SetString(IDS_CFGVIEWNORMALS,"",true);
  else config->SetString(IDS_CFGVIEWNORMALS,"On",true);
  doc->UpdateAllViews(NULL);
  }

void CMainFrame::OnUpdateShowNormals(CCmdUI* pCmdUI) 
  {
  pCmdUI->SetCheck(config->GetBool(IDS_CFGVIEWNORMALS));
  }

void CMainFrame::OnPointsMovebynormals() 
  {
  movebynormals=!movebynormals;	
  }

void CMainFrame::OnUpdatePointsMovebynormals(CCmdUI* pCmdUI) 
  {
  pCmdUI->SetCheck(movebynormals==true);	
  }

class FunctorPretranslateMessages
{
  mutable bool &flag;
  MSG *msg;
public:
  FunctorPretranslateMessages(MSG *msg, bool &flag):flag(flag),msg(msg) {}
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    if (plugin->GetPlugin().OnPretranslateMessage(msg)==TRUE) {flag=true;return true;}
    return false;
  }  
};

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg) 
  { 
  //    ::LogWindowMessage(pMsg->message,pMsg->wParam,pMsg->lParam);
  bool premsg=false;
  theApp.ForEachPlugin(FunctorPretranslateMessages(pMsg,premsg));
  if (premsg) return TRUE;
  if (pMsg->message==WM_MOUSEWHEEL)
  {
    CWnd *target=WindowFromPoint(pMsg->pt);
    if (target) target->SendMessage(pMsg->message,pMsg->wParam,pMsg->lParam);
    return TRUE;
  }
  return CFrameWnd::PreTranslateMessage(pMsg);
  }

void CMainFrame::OnFileHotkeys() 
  {
  if (wHotKeyDlg.GetSafeHwnd()) return;
  wHotKeyDlg.Create(wHotKeyDlg.IDD);
  wHotKeyDlg.CenterWindow();
  }


LRESULT CMainFrame::OnUpdateStatusMsg(WPARAM ,LPARAM )
  {
  doc->UpdateStatus();  
  return 1;
  }


BOOL CMainFrame::OnCommand(WPARAM wParam, LPARAM lParam)
  {
  if (wHotKeyDlg.GetSafeHwnd())
	{
	wHotKeyDlg.OnCommandIncomed(this,LOWORD(wParam));
	return TRUE;
	}
  return CFrameWnd::OnCommand(wParam,lParam);
  }

HACCEL CMainFrame::GetDefaultAccelerator()
  {
  if (userhkeys) return userhkeys;
  else return CFrameWnd::GetDefaultAccelerator();
  }

LRESULT CMainFrame::OnChangeAccelTable(WPARAM wParam, LPARAM lParam)
  {
  if (userhkeys!=NULL)
	{
	::DestroyAcceleratorTable(userhkeys);
	}
  if (lParam!=0)
	{
	userhkeys=::CreateAcceleratorTable((LPACCEL)lParam,wParam);
	}
  else
	userhkeys=NULL;
  CHotKeyCfgDlg::RecursiveReplaceMenuHotkeys(GetDefaultAccelerator(),*GetMenu());
  SaveHotkeys();
  return 0;
  }

void CMainFrame::SaveHotkeys()
  {
  if (userhkeys==NULL)
	{
	theApp.WriteProfileString(HOTKEYSECTION,HOTKEYSECTION,NULL);
	}
  else
	{
	int accls=CopyAcceleratorTable(userhkeys,NULL,0);
	ACCEL *accel=(ACCEL *)alloca(sizeof(ACCEL)*(accls+1));
	CopyAcceleratorTable(userhkeys,accel,accls);
	theApp.WriteProfileBinary(HOTKEYSECTION,HOTKEYSECTION,(LPBYTE)accel,sizeof(ACCEL)*accls);
	}
  }

void CMainFrame::LoadHotkeys()
  {
  LPBYTE data;
  UINT size;
  if (theApp.GetProfileBinary(HOTKEYSECTION,HOTKEYSECTION,&data,&size)!=FALSE) 
	{
	userhkeys=CreateAcceleratorTable((LPACCEL)data,size/sizeof(ACCEL));
    delete [] data;
	}
  CHotKeyCfgDlg::RecursiveReplaceMenuHotkeys(GetDefaultAccelerator(),*GetMenu());
  }

void CMainFrame::ShowMessageWindow(const char *caption, const char *text)
  {
  CConsole dlg;
  dlg.consoleText=text;
  dlg.vCaption=caption;
  dlg.DoModal();
  }

void CMainFrame::ShowMessageWindow(int idtext, const char *text)
  {
  CString caption;
  caption.LoadString(idtext);
  ShowMessageWindow(caption,text);
  }


void CMainFrame::OnMacroRecordmacro()
  {
  if (recorder.IsRecording()) 
    recorder.StopRecord();
  else
    recorder.RecordMacro();
  }
void CMainFrame::OnMacroPlaymacro()
  {
  if (!recorder.IsPlaybacking()) 
    recorder.PlaybackRecord();
  }
void CMainFrame::OnUpdateMacroRecordmacro(CCmdUI* pCmdUI)
  {
  pCmdUI->SetCheck(recorder.IsRecording());  
  }
void CMainFrame::OnUpdateMacroPlaymacro(CCmdUI* pCmdUI)
  {
  pCmdUI->Enable(recorder.IsMacroRecorder() && !recorder.IsPlaybacking());
  }

void CMainFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
/*	if (nState==WA_ACTIVE)
      {
      INPUT input;
      memset(&input,0,sizeof(input));
      input.mi.dwFlags=0;
	  if (GetAsyncKeyState(VK_LBUTTON) & 0x8000) input.mi.dwFlags|=MOUSEEVENTF_LEFTUP;
	  if (GetAsyncKeyState(VK_RBUTTON) & 0x8000) input.mi.dwFlags|=MOUSEEVENTF_RIGHTUP;
      if (GetAsyncKeyState(VK_MBUTTON) & 0x8000) input.mi.dwFlags|=MOUSEEVENTF_MIDDLEUP;
	  if (input.mi.dwFlags)
		{
		input.mi.time=GetTickCount();
		input.type=INPUT_MOUSE;
		SendInput(1,&input,sizeof(input));
		}
      }
	
	// TODO: Add your message handler code here
	*/
}

LRESULT CMainFrame::OnEndCalculationSections(WPARAM ,LPARAM )
  {  
  return 1;
  }

LRESULT CMainFrame::OnCalculateSections(WPARAM, LPARAM)
  {
  ObjToolSections &sect=doc->LodData->Active()->GetTool<ObjToolSections>();
  BTree<ObjToolSections::ObjToolSectionInfo> list;
  sect.CalculateSections(list);
  doc->numSections=list.Size();
  this->m_wndMatLib.UpdateSections(list);
  doc->UpdateStatus();
  comint.sectchanged=false;
  return 1;
  }

#define ASSTRING(p) #p

bool CMainFrame::StoreViewerConfig(void)
  {
  theApp.WriteProfileString(EXTERNALCONFIG,ASSTRING(selectTex),_externalCfg.selectTex);
  theApp.WriteProfileString(EXTERNALCONFIG,ASSTRING(selectMat),_externalCfg.selectMat);
  theApp.WriteProfileString(EXTERNALCONFIG,ASSTRING(wframeTex),_externalCfg.wframeTex);
  theApp.WriteProfileInt(EXTERNALCONFIG,ASSTRING(selMode),_externalCfg.selMode);
  theApp.WriteProfileInt(EXTERNALCONFIG,ASSTRING(centerMode),_externalCfg.centerMode);
  theApp.WriteProfileInt(EXTERNALCONFIG,ASSTRING(displayFlags),_externalCfg.displayFlags);
  theApp.WriteProfileString(EXTERNALCONFIG,ASSTRING(groundObj),_externalCfg.groundObj);
  theApp.WriteProfileInt(EXTERNALCONFIG,ASSTRING(groundPos),_externalCfg.groundPos);
  theApp.WriteProfileString(EXTERNALCONFIG,ASSTRING(markerObj),_externalCfg.markerObj);
  return true;
  }

bool CMainFrame::LoadViewerConfig(void)
  {
  _externalCfg.selectTex=(LPCTSTR)theApp.GetProfileString(EXTERNALCONFIG,ASSTRING(selectTex),"#(argb,8,8,3)color(0.3,0,0,0.5)");
  _externalCfg.wframeTex=(LPCTSTR)theApp.GetProfileString(EXTERNALCONFIG,ASSTRING(wframeTex),"DATA\\WIREFRAME.PAA");
  _externalCfg.selectMat=(LPCTSTR)theApp.GetProfileString(EXTERNALCONFIG,ASSTRING(selectMat),"DATA\\SELECT.RVMAT");
  _externalCfg.groundObj=(LPCTSTR)theApp.GetProfileString(EXTERNALCONFIG,ASSTRING(groundObj),"");
  _externalCfg.markerObj=(LPCTSTR)theApp.GetProfileString(EXTERNALCONFIG,ASSTRING(markerObj),"");
  _externalCfg.selMode=(ExternalViewerConfig::SelEnum)theApp.GetProfileInt(EXTERNALCONFIG,ASSTRING(selMode),_externalCfg.SelectTexture);
  _externalCfg.centerMode=(ExternalViewerConfig::CenterEnum)theApp.GetProfileInt(EXTERNALCONFIG,ASSTRING(centerMode),_externalCfg.CenterToPin);
  _externalCfg.groundPos=(ExternalViewerConfig::GroundPosFlags)theApp.GetProfileInt(EXTERNALCONFIG,ASSTRING(groundPos),0);
  _externalCfg.displayFlags=theApp.GetProfileInt(EXTERNALCONFIG,ASSTRING(displayFlags),0);
  _externalCfg.lodBias=config->GetInt(IDS_CFGLODBIAS)/10.0f;
  _externalCfg.freezeAnim=-1;

  char cmdLine[_MAX_PATH];
  strcpy_s(cmdLine,_MAX_PATH,config->GetString(IDS_CFGEXTERNARVIEWER));
  _externalCfg.startupLine=cmdLine;

  char *start = strchr(cmdLine,'"');
  if (start)
  {
    ++start;
    while (*start==' ')++start;
    char *end=strchr(start+1,'"');
    if (end)
    {
      --end;
      while (*end==' ')--end;
      *(end+1)=0;
    }
    _externalCfg.buldozerName=start;
  }
  else
  {
    start=cmdLine;
    while (*start==' ')++start;
    char *end = strchr(start,'.');
    if (end)*end=0;
    _externalCfg.buldozerName=start;
  }
  _externalCfg.hNotifyStatus=*this;
  _externalCfg.msgNotifyStatus=MSG_EXVIEWERUPDATESTATUS;
  _externalCfg.startupFolder=config->GetString(IDS_CFGPATHFORTEX);
  return true;
  }

bool CMainFrame::ExternalError(ExternalViewer::ErrorEnum errornum)
  {
  const char *error=NULL;  
  switch (errornum)
    {
    case ExternalViewer::ErrCreateProcessFail: error=StrRes[IDS_EXTERNERRCPF];break;
    case ExternalViewer::ErrBusy: error=StrRes[IDS_EXTERNALBUSY];break;
    case ExternalViewer::ErrNotInicialized: error=StrRes[IDS_EXTERNALNOINIT];break;
    case ExternalViewer::ErrAttachFailed: error=StrRes[IDS_EXTERNALATTACHF];break;
    case ExternalViewer::ErrNoConfig: error=StrRes[IDS_EXTERNALNOCFG];break;
    case ExternalViewer::ErrTimeout: error=StrRes[IDS_EXTERNALTIMEOUT];break;
    case ExternalViewer::ErrFailed: error=StrRes[IDS_EXTERNALFAILED];break;
    case ExternalViewer::ErrInitFailed: error=StrRes[IDS_EXTERNALINITFAILED];break;
    case ExternalViewer::ErrLost: error=StrRes[IDS_EXTERNALLOST];break;
    case ExternalViewer::ErrOK: return true;
    default: 
      {
      char *buff=(char *)alloca(100);
      error=buff;
      sprintf(buff,StrRes[IDS_EXTERNALUNKNOWN],errornum);
      }
    }
  AfxMessageBox(error,MB_ICONEXCLAMATION);
  return false;
  }

void CMainFrame::OnExtviewerStart()
  {
  ProgressBar<> pb(1);
  pb.ReportState(PROGRESS_TIMEOUT,_externalCfg.init_timeout);
  pb.ReportState(IDS_STARTINGVIEWER);
  ExternalError(_externalViewer.Restart());
  OnExtracommandsRefresh();
  }

void CMainFrame::OnExtracommandsAttach()
  {
  ExternalError(_externalViewer.Attach());
  OnExtracommandsRefresh();
  }

void CMainFrame::OnExtViewerNotRunning(CCmdUI *pCmdUI)
  {
  pCmdUI->Enable(_externalViewer.IsReady()!=_externalViewer.ErrOK && _externalViewer.IsReady()!=_externalViewer.ErrBusy);
  }

void CMainFrame::OnExtracommandsDetach()
  {
  ExternalError(_externalViewer.Detach());
  }

void CMainFrame::OnExtViewerRunning(CCmdUI *pCmdUI)
  {
  pCmdUI->Enable(_externalViewer.IsReady()==_externalViewer.ErrOK || _externalViewer.IsReady()==_externalViewer.ErrBusy);
  }

void CMainFrame::OnExtracommandsStop()
  {
  ExternalError(_externalViewer.Stop());
  }

void CMainFrame::OnExtracommandsStartscript()
  {
  static CDlgInputFile dlg;
  dlg.filter.LoadString(IDS_STARTSCRIPT);
  dlg.title.LoadString(IDS_EXTVIEWER_RUNSCRIPT);
  if (dlg.DoModal()==IDOK)
    {
    ExternalError(_externalViewer.StartScript(dlg.vFilename));
    }
  }

void CMainFrame::OnExtracommandsRefresh()
  {
  doc->extupdate=true;
  }

void CMainFrame::OnUpdateExtViewerDisplayFlags(CCmdUI *pCmdUI)
  {
  switch (pCmdUI->m_nID) 
    {
    case ID_EXTVIEWER_HIDETEXTURES: pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.HideTextures)!=FALSE);break;
    case ID_EXTVIEWER_HIDEMATERIALS: pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.HideMaterials)!=FALSE);break;
    case ID_EXTVIEWER_HIDEPROXIES: pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.HideProxies)!=FALSE);break;
    case ID_EXTVIEWER_HIDEHIDDEN : pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.HideHidden)!=FALSE);break;
    case ID_EXTVIEWER_LODSWITCHING : pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.MultipleLODs)!=FALSE);break;
    case ID_EXTVIEWER_BEWAREUNSUPORTEDTEXTURES: pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.RemoveUnsupportedTexs)!=FALSE);break;
    case ID_EXTVIEWER_SHOWSHADOW: pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.ShowShadow)!=FALSE);break;
    case ID_EXTVIEWER_POPUPBULDOZER: pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.PopupViewer)!=FALSE);break;
    case ID_EXTVIEWER_MARKERENABLED: pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.ShowMarkers)!=FALSE);
                                    pCmdUI->Enable(_externalCfg.markerObj.GetLength());break;

    case ID_EXTVIEWER_SHOWWIREFRAME: pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.WireFrame)!=FALSE);break;
    case ID_EXTVIEWER_DISABLEMODELCFG: pCmdUI->SetCheck((_externalCfg.displayFlags & _externalCfg.NoModelConfig)!=FALSE);break;    
    }
  }

void CMainFrame::OnExtViewerDisplayFlags(UINT cmd)
  {
  switch (cmd)
    {
    case ID_EXTVIEWER_HIDETEXTURES: _externalCfg.displayFlags ^= _externalCfg.HideTextures;break;
    case ID_EXTVIEWER_HIDEMATERIALS: _externalCfg.displayFlags ^= _externalCfg.HideMaterials;break;
    case ID_EXTVIEWER_HIDEPROXIES: _externalCfg.displayFlags ^= _externalCfg.HideProxies;break;
    case ID_EXTVIEWER_HIDEHIDDEN : _externalCfg.displayFlags ^= _externalCfg.HideHidden;break;
    case ID_EXTVIEWER_LODSWITCHING : _externalCfg.displayFlags ^= _externalCfg.MultipleLODs;_externalCfg.displayFlags &=~_externalCfg.ShowShadow;break;
    case ID_EXTVIEWER_BEWAREUNSUPORTEDTEXTURES: _externalCfg.displayFlags ^= _externalCfg.RemoveUnsupportedTexs;break;
    case ID_EXTVIEWER_SHOWSHADOW: _externalCfg.displayFlags ^= _externalCfg.ShowShadow;_externalCfg.displayFlags &=~_externalCfg.MultipleLODs;break;
    case ID_EXTVIEWER_POPUPBULDOZER : _externalCfg.displayFlags ^= _externalCfg.PopupViewer;break;
    case ID_EXTVIEWER_MARKERENABLED : _externalCfg.displayFlags ^= _externalCfg.ShowMarkers;break;
    case ID_EXTVIEWER_SHOWWIREFRAME: _externalCfg.displayFlags ^= _externalCfg.WireFrame;break;
    case ID_EXTVIEWER_DISABLEMODELCFG: _externalCfg.displayFlags ^= _externalCfg.NoModelConfig;break;
    }
  OnExtracommandsRefresh();
  StoreViewerConfig();
  }

void CMainFrame::OnUpdateExtViewerSelectMask(CCmdUI *pCmdUI)
  {
  switch (pCmdUI->m_nID)
    {
    case ID_EXTVIEWER_MASKTEXTURE: pCmdUI->SetCheck(_externalCfg.selMode==_externalCfg.SelectTexture);break;
    case ID_EXTVIEWER_MASKMATERIAL:pCmdUI->SetCheck(_externalCfg.selMode==_externalCfg.SelectMaterial);break;
    case ID_EXTVIEWER_HIGHLIGHTSELECTION:pCmdUI->SetCheck(_externalCfg.selMode==_externalCfg.SelectLight);break;
    case ID_EXTVIEWER_DISABLESELECTION:pCmdUI->SetCheck(_externalCfg.selMode==_externalCfg.SelectNone);break;
    case ID_EXTVIEWER_HIDENOTSELECTED:pCmdUI->SetCheck(_externalCfg.selMode==_externalCfg.SelectDisplay);break;
    case ID_EXTVIEWER_MASKWFRAME:pCmdUI->SetCheck(_externalCfg.selMode==_externalCfg.SelectWFrame);break;
    }
  }

void CMainFrame::OnExtViewerSelectMask(UINT cmd)
  {
  switch (cmd)
    {
    case ID_EXTVIEWER_MASKTEXTURE: _externalCfg.selMode=_externalCfg.SelectTexture;break;
    case ID_EXTVIEWER_MASKMATERIAL:_externalCfg.selMode=_externalCfg.SelectMaterial;break;
    case ID_EXTVIEWER_HIGHLIGHTSELECTION:_externalCfg.selMode=_externalCfg.SelectLight;break;
    case ID_EXTVIEWER_DISABLESELECTION:_externalCfg.selMode=_externalCfg.SelectNone;break;
    case ID_EXTVIEWER_HIDENOTSELECTED:_externalCfg.selMode=_externalCfg.SelectDisplay;break;
    case ID_EXTVIEWER_MASKWFRAME:_externalCfg.selMode=_externalCfg.SelectWFrame;break;
    }
  OnExtracommandsRefresh();
  StoreViewerConfig();
  }

void CMainFrame::OnUpdateExtViewerCenter(CCmdUI *pCmdUI)
  {
  switch (pCmdUI->m_nID)
    {
    case ID_EXTVIEWER_AUTOCENTER: pCmdUI->SetCheck(_externalCfg.centerMode==_externalCfg.Autocenter);break;
    case ID_EXTVIEWER_CENTERTOPIN:pCmdUI->SetCheck(_externalCfg.centerMode==_externalCfg.CenterToPin);break;
    case ID_EXTVIEWER_CENTERSELECTION:pCmdUI->SetCheck(_externalCfg.centerMode==_externalCfg.CenterToSelection);break;
    }
  }

void CMainFrame::OnExtViewerCenter(UINT cmd)
  {
  switch (cmd)
    {
    case ID_EXTVIEWER_AUTOCENTER:_externalCfg.centerMode=_externalCfg.Autocenter;break;
    case ID_EXTVIEWER_CENTERTOPIN:_externalCfg.centerMode=_externalCfg.CenterToPin;break;
    case ID_EXTVIEWER_CENTERSELECTION:_externalCfg.centerMode=_externalCfg.CenterToSelection;break;
    }
  OnExtracommandsRefresh();
  StoreViewerConfig();
  }

void CMainFrame::OnExtViewerMenuFor(UINT cmd)
  {
  CMenu mnu;
#ifdef PUBLIC
  mnu.LoadMenu(IDR_MAINFRAME_PUBLIC);
#else
  mnu.LoadMenu(IDR_MAINFRAME);
#endif
  CMenu *sub;
  sub=mnu.GetSubMenu(3);
  if (sub) sub=sub->GetSubMenu(0);
  if (sub) sub=sub->GetSubMenu(0);
  if (sub)
    {
    switch (cmd)
      {
      case ID_MENUSFORHOTKEYS_BULDOZERMENU: break;
      case ID_MENUSFORHOTKEYS_UNSELECTEDPARTSMENU: sub=sub->GetSubMenu(11);break;
      case ID_MENUSFORHOTKEYS_OTHERMENU: sub=sub->GetSubMenu(1);break;
      }
    }
  if (sub)    
    {
    CPoint pt(GetMessagePos());
    sub->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON,pt.x,pt.y,this,NULL);
    }
  }

void CMainFrame::OnExtViewerSetTexMat(UINT cmd)
  {
  CDlgInputFile dlg;
  dlg.title.LoadString(IDS_EXTVIEWER_SELECTMASK);
  switch (cmd)
    {
    case ID_EXTVIEWEROTHER_SETMASKTEXTURE: 
      dlg.vFilename+=_externalCfg.selectTex;
      dlg.filter.LoadString(IDS_PACPAAFILTER);
      break;
    case ID_EXTVIEWEROTHER_SETMASKMATERIAL: 
      dlg.vFilename+=_externalCfg.selectMat;
      dlg.filter.LoadString(IDS_MATERIALFILTER);
      break;
    case ID_EXTVIEWEROTHER_SETMASKWFTEXTURE:
      dlg.vFilename+=_externalCfg.wframeTex;
      dlg.filter.LoadString(IDS_PACPAAFILTER);
      break;
    }
  if (strcmp(dlg.vFilename,config->GetString(IDS_CFGPATHFORTEX))==0) dlg.vFilename="";
  if (dlg.DoModal()==IDOK)
    {    
    switch (cmd)
      {
      case ID_EXTVIEWEROTHER_SETMASKTEXTURE: 
        _externalCfg.selectTex=dlg.vFilename.GetString();break;
      case ID_EXTVIEWEROTHER_SETMASKMATERIAL: 
        _externalCfg.selectMat=dlg.vFilename.GetString();break;
      case ID_EXTVIEWEROTHER_SETMASKWFTEXTURE:
        _externalCfg.wframeTex=dlg.vFilename.GetString();break;
      }
    OnExtracommandsRefresh();
    StoreViewerConfig();
    }
  }

void CMainFrame::OnExtViewerSetWeather()
  {
  if (_viewerEnvirom.GetSafeHwnd()==NULL)    
    _viewerEnvirom.Create(_viewerEnvirom.IDD);
  _viewerEnvirom.ShowWindow(SW_SHOW);
  _viewerEnvirom.BringWindowToTop();
  }

void CMainFrame::OnUpdateExtViewerLoadRTM(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(_lastTypedRTM.GetLength());
  pCmdUI->Enable(_externalViewer.IsReady()==_externalViewer.ErrOK || _externalViewer.IsReady()==_externalViewer.ErrBusy);
}

void CMainFrame::OnExtViewerLoadRTM()
{
  CDlgInputFile dlg;
  dlg.title.LoadString(IDS_EXVIEWERLOADRTM);
  dlg.vFilename=_lastTypedRTM;
  dlg.filter.LoadString(IDS_RTMFILTER);
  if (dlg.DoModal()==IDOK)
  {
    if (_access(dlg.vFilename,0)==0)
    {
      ExternalError(_externalViewer.SetRTM(dlg.vFilename));
      _lastTypedRTM=dlg.vFilename;
    }
    else
    {
      AfxMessageBox(IDS_RTMFILENOTEXIST,MB_ICONEXCLAMATION);
    }
  }
}
void CMainFrame::OnUpdateExtViewerClearRTM(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_externalViewer.IsReady()==_externalViewer.ErrOK || _externalViewer.IsReady()==_externalViewer.ErrBusy);
}

void CMainFrame::OnExtViewerClearRTM()
{
     _externalViewer.SetRTM(0);
     _lastTypedRTM="";
}


void CMainFrame::OnExtracommandsReloadall()
  {
  MessageBox("Function is out of order!");  
  }

bool ControlledExternalViewer::AfterModelPrepared(const ObjectData &source, ObjectData &target)
  {
    if (_MAT.IsLoaded()) _MAT.CallMat().AfterPrepareModelForViewer(source,target);
  return true;
  }

void CMainFrame::OpenOptions() 
 {
  if (config->GetSafeHwnd())
    {
    config->BringWindowToTop();
    config->SetForegroundWindow();
    }
  else    
    OnFileOptions();
 }

HMENU CMainFrame::AddSubMenu(int where, const char *menuName)
  {
  CMenu *mnu=this->GetMenu();
  CMenu newmenu;
  newmenu.CreateMenu();
  if (where!=255)  mnu=mnu->GetSubMenu(where);
  if (mnu)
    {
    mnu->AppendMenu(MF_STRING,(UINT_PTR)newmenu.GetSafeHmenu(),menuName);
    HMENU ret=newmenu.Detach();
    return ret;
    }
  else
    return NULL;    
  }

bool CMainFrame::AddMenuCommand(HMENU subMenu, const char *name, IPluginCommand *cmd)
{
  CMenu mnu;
  mnu.Attach(subMenu);
  if (cmd==0 || name==0)
  {
    mnu.AppendMenu(MF_SEPARATOR,0,(LPCTSTR)0);
  }
  else
  {
    int x=_menuCommands.Size();
    if (x>=(MAX_COMMAND_VALUE-MIN_COMMAND_VALUE)) {mnu.Detach();return false;}
    _menuCommands.Append(Ref<IPluginCommand>(cmd));    
    mnu.AppendMenu(MF_STRING,MIN_COMMAND_VALUE+x,name);
  }
  mnu.Detach();
  return true;
}

static CMenu *FindNthSubmenu(CMenu *what, int order) {
    int pos = what->GetMenuItemCount();
    for (int i = 0; i< pos;i++) {
        if (what->GetSubMenu(i) != NULL) {
            if (order) order--;
            else return what->GetSubMenu(i);
        }        
    }
    return 0;

}

bool CMainFrame::AddMenuCommand(int where, const char *name, IPluginCommand *cmd)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  CMenu *mnu=this->GetMenu();    
  if (where==128) mnu=FindNthSubmenu(mnu->GetSubMenu(0),1);
  else if (where==129) mnu=FindNthSubmenu(mnu->GetSubMenu(0),2);
  else if (where!=255)  mnu=mnu->GetSubMenu(where);
  if (mnu) return AddMenuCommand(*mnu,name,cmd);
  return false;
}


void CMainFrame::OnPluginCommand(UINT cmd)
  {
    int x=cmd-MIN_COMMAND_VALUE;
    Ref<IPluginCommand> cmdif=_menuCommands[x];
    cmdif->OnCommand();
  }

class CmdUpdate: public IPluginMenuCommandUpdate
{
  CCmdUI *pCmdUI;
public:
  CmdUpdate(CCmdUI *pCmdUI):pCmdUI(pCmdUI) {}
  virtual void Enable(bool enable) 
  {
    pCmdUI->Enable(enable);
  }
  virtual void SetCheck(bool check)
  {
    pCmdUI->SetCheck(check);
  }
  
};

void CMainFrame::OnUpdatePluginCommand(CCmdUI *pCmdUI)
  {
    int x=pCmdUI->m_nID-MIN_COMMAND_VALUE;
    Ref<IPluginCommand> cmdif=_menuCommands[x];
    cmdif->Update(&CmdUpdate(pCmdUI));
  }


struct RunScriptThreadInfo
{
  GameState *gState;
  O2ScriptClass *script;
};

static UINT RunScriptThread(LPVOID data)
{
RunScriptThreadInfo *nfo=(RunScriptThreadInfo *)data;
GameValue res=nfo->script->RunScript(*nfo->gState);
return toInt(res);
}

class CIUpdateObject: public CICommand
{
public:
  LODObject *object;
  CIUpdateObject(LODObject *o) {recordable=false;object=o;}
  virtual int Execute(LODObject *lod)
  {
    *lod=*object;
    return 0;
  };
  ~CIUpdateObject() {delete object;}

};


static HKEY GetConfigKey(const char *script)
{
  HKEY reg=::AfxGetApp()->GetAppRegistryKey();
  char *scrname=strcpy((char *)alloca(strlen(script)+1),script);
  char *c=strchr(scrname,'\\');
  while (c) {*c='?';c=strchr(c,'\\');}
  HKEY scrreg;
  HKEY result;
  if (::RegCreateKeyEx(reg,"ScriptConfig",0,0,0,KEY_ALL_ACCESS,NULL,&scrreg,NULL)==ERROR_SUCCESS)
  {
    if (::RegCreateKeyEx(scrreg,scrname,0,0,0,KEY_ALL_ACCESS,NULL,&result,NULL)==ERROR_SUCCESS)
    {
      RegCloseKey(scrreg);
      RegCloseKey(reg);
      return result;
    }
   RegCloseKey(scrreg);
  }
  RegCloseKey(reg);
  return NULL;
}

static void LoadConfig(const char *script, GameDataNamespace &space)
{
  HKEY cfgreg=GetConfigKey(script);
  DWORD vals, valMax, nameMax;
  RegQueryInfoKey(cfgreg,NULL,NULL,NULL,NULL,NULL,NULL,&vals,&nameMax,&valMax,NULL,NULL);
  valMax++;
  nameMax++;
  SRef<char>name=new char[nameMax];
  SRef<BYTE>value=new BYTE[valMax];
  for (unsigned int i=0;i<vals;i++)
  {
    DWORD namelen=nameMax;
    DWORD vallen=valMax;
    DWORD type;
    RegEnumValue(cfgreg,i,name.GetRef(),&namelen,NULL,&type,value.GetRef(),&vallen);
    if (type==REG_SZ) space.VarSet(name,RString(reinterpret_cast<char *>(value.GetRef())));
    else if (type==REG_DWORD) space.VarSet(name,(float)*reinterpret_cast<int *>(value.GetRef()));
    else if (type==REG_BINARY) space.VarSet(name,*reinterpret_cast<float *>(value.GetRef()));
  }
  RegCloseKey(cfgreg);
}

class ScriptSaveConfigFunctor
{
public:
  HKEY reg;
  bool operator()(GameVariable &var,const MapStringToClass<GameVariable,AutoArray<GameVariable> > *space) 
  { 
    if (strncmp((RString)var._name,"cfg_",4)==0)
    {
      if (var._value.GetType()==GameScalar)
      {
        float val=var._value;
        if ((float)floor(val)==val && val>=0) 
        {        
          DWORD dval=toLargeInt(val);
          ::RegSetValueEx(reg,var._name,0,REG_DWORD,(BYTE *)&dval,sizeof(dval));
        }
        else
          ::RegSetValueEx(reg,var._name,0,REG_BINARY,(BYTE *)&val,sizeof(val));
      }
      else if (var._value.GetType()==GameString)
      {
        RString text=var._value;
        ::RegSetValueEx(reg,var._name,0,REG_SZ,(BYTE *)(text.Data()),text.GetLength()+1);
      }
    }    
    return false;
  }
};

class ScriptCanSaveConfigFunctor
{
public:
  bool *canSave;
  bool operator()(GameVariable &var,const MapStringToClass<GameVariable,AutoArray<GameVariable> > *space) 
  {
    if (strncmp((RString)var._name,"cfg_",4)==0)
    {
      if (var._value.GetType()==GameScalar || var._value.GetType()==GameString) 
      {
        *canSave=true;
        return true;
      }
    }
    return false;
  }
};

static void SaveConfig(const char *script, GameDataNamespace &space)
{
  ScriptCanSaveConfigFunctor canSaveFunct;
  bool canSave;
  canSaveFunct.canSave=&canSave;
  space.GetVariables().ForEachF(canSaveFunct);
  if (canSave)
  {
    HKEY cfgreg=GetConfigKey(script);
    DWORD vals,valMax,nameMax;
    RegQueryInfoKey(cfgreg,NULL,NULL,NULL,NULL,NULL,NULL,&vals,&nameMax,&valMax,NULL,NULL);
    nameMax++;
    SRef<char>name=new char[nameMax];
    for (unsigned int i=0;i<vals;i++)
    {
      DWORD namelen=nameMax;
      GameValue test;
      ::RegEnumValue(cfgreg,i,name,&namelen,NULL,NULL,NULL,NULL);
      if (space.VarGet(name,test)==false  || test.GetNil()) 
      {
        RegDeleteValue(cfgreg,name);
        vals--;
        i--;
      }
    }
    ScriptSaveConfigFunctor save;
    save.reg=cfgreg;
    space.GetVariables().ForEachF(save);
    RegCloseKey(cfgreg);    
  }
}

void CMainFrame::ScriptRegMaintance()
{
  HKEY reg=::AfxGetApp()->GetAppRegistryKey();
  HKEY scrreg;
  if (::RegCreateKeyEx(reg,"ScriptConfig",0,0,0,KEY_ALL_ACCESS,NULL,&scrreg,NULL)==ERROR_SUCCESS)
  {
    char name[256];
    char file[256];
    int i=0;
    DWORD namelen=256;
    while (RegEnumKeyEx(scrreg,i,name,&namelen,NULL,NULL,NULL,NULL)==ERROR_SUCCESS)
    {
      
      strcpy(file,name);
      char *c=file;
      while ((c=strchr(c,'?'))!=NULL) *c='\\';
      if (_access(file,00)!=0) 
      {      
          RegDeleteKey(scrreg,name);
        i--;
      }
      else
      {
        HKEY sreg;
        if (RegOpenKey(scrreg,name,&sreg)==ERROR_SUCCESS)
        {
          DWORD values;
          RegQueryInfoKey(sreg,NULL,NULL,NULL,NULL,NULL,NULL,&values,NULL,NULL,NULL,NULL);
          RegCloseKey(sreg);
          if (values==0)
          {
             RegDeleteKey(scrreg,name);
             i--;
          }
        }
      }
      namelen=256;
      i++;
    }
  }
  RegCloseKey(reg);
  RegCloseKey(scrreg);
}

#include "O2ScriptCommands.h"

void CMainFrame::RunScript(const char *script, bool settings, HWND hWnd)
{
  if (!_dbgprovider.IsLoaded())
  {
    Pathname pth;
    pth.SetDirectory(config->GetString(IDS_CFGOBJEKTIVDLLS));
    pth.SetFilename("UIDebugger.dll");
    _dbgprovider.CreateProvider(pth.GetFilename());
    if (!_dbgprovider.IsLoaded())
    {
      _dbgprovider.CreateProvider(pth);
    }
  }
  DllDebuggingSession dbgsession(_dbgprovider);
  
    GameState gState;
    GameDataNamespace globals(NULL, RString());
    GameDataNamespace *oldNamespace = gState.SetGlobalVariables(&globals);

  O2ScriptClass scriptclass;
  scriptclass.SetLibraryPaths(config->GetString(IDS_CFGSCRIPTLIBS));
  if (scriptclass.LoadScript(script)==false)
  {
    AfxMessageBox(scriptclass.GetPreprocError(),MB_ICONEXCLAMATION);
    gState.SetGlobalVariables(oldNamespace);
    return;
  }
  O2ScriptCommands::RegisterToGameState(&gState);
  scriptclass.PrepareGameState(gState);
  gState.AttachDebugger(dbgsession);
  LoadConfig(script, globals);
  LODObject *workobj=NULL;
  if (!settings)
  {
    workobj=new LODObject (*doc->LodData);  
    GdLODObject *gd=new GdLODObject(new LODObjectRef(workobj));
    gd->_objname=doc->GetPathName();
    GameValue workobjval(gd);    
    gState.VarSet("this",workobjval);
  }
  else
    gState.VarSet("this",RString("options"));
  if (hWnd)
    GdDialogs::SetParentWindow(&gState,hWnd);
  else
      GdDialogs::SetParentWindow(&gState,AfxGetMainWnd()->GetSafeHwnd());
  GdSourceSafe::GScc=theApp._scc;
  GdSourceSafe::GSccActive=false;
  RunScriptThreadInfo thinfo;
  thinfo.gState=&gState;
  thinfo.script=&scriptclass;
  CWinThread *thread=AfxBeginThread((AFX_THREADPROC)RunScriptThread,&thinfo,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED,NULL);
  thread->m_bAutoDelete=FALSE;  
  DlgRunScript dlg(AfxGetMainWnd());
  dlg.gameState=&gState;
  dlg.waitHandle=thread->m_hThread;
  dlg.debugger=dbgsession;
  dlg.mainfrm=this;
  dlg.DoModal();  
  long result;
  GetExitCodeThread(thread->m_hThread,(LPDWORD)&result);
  delete thread;
  if (result>=0 && (dlg.debugger==0 || dlg.debugger->GetTraceMode()!=ScriptDebuggerBase::traceTerminate))
  {
    if (workobj)
      {
        for (int i=0;i<workobj->NLevels();i++) workobj->Level(i)->RecalcNormals();
        comint.Begin(WString(IDS_CMDRUNSCRIPT));
        comint.Run(new CIUpdateObject(workobj));
        doc->UpdateAllViews(NULL);
        doc->UpdateAllToolbars(0);
      }
  }
  else
  {
    delete workobj;
  }
  SaveConfig(script, globals);

  gState.SetGlobalVariables(oldNamespace);


  doc-> UpdateFileSSStatus();
  FastCAlloc::CleanUpAll();
}

void CMainFrame::OnScriptsEditscript()
{
  CString path=config->GetString(IDS_SCRIPTPATH);  
  if (path=="") {path=Pathname::GetExePath().GetDirectoryWithDrive();}
  CString edit=config->GetString(IDS_SCRIPTEDITOR);
  if (edit=="") {AfxMessageBox(IDS_NOSCRIPTEDITOR);return;}
  CFileDialogEx fdlg(FALSE,"bio2s",NULL,OFN_PATHMUSTEXIST,WString(IDS_SCRIPTFILTER),this);
  fdlg.m_ofn.lpstrInitialDir=path;
  if (fdlg.DoModal()==IDOK)
  {
    Pathname path=fdlg.GetPathName();
    theApp.rocheck.TestFileRO(path,ROCHF_DisableSaveAs);
    UINT res=(UINT)ShellExecute(*this,NULL,edit,path,path.GetDirectoryWithDrive(),SW_SHOWNORMAL);
    if (res<=32)
      AfxMessageBox(IDS_UNABLESTARTEDITOR);
    config->SaveCurrPath(IDS_SCRIPTPATH,fdlg.GetPathName());
  }
}

void CMainFrame::OnScriptsRunscript()
{
  CString path=config->GetString(IDS_SCRIPTPATH);  
  if (path=="") {path=Pathname::GetExePath().GetDirectoryWithDrive();}
  CFileDialogEx fdlg(FALSE,"bio2s",NULL,OFN_PATHMUSTEXIST,WString(IDS_SCRIPTFILTER),this);
  fdlg.m_ofn.lpstrInitialDir=path;
  if (fdlg.DoModal()==IDOK)
  {
    Pathname path=fdlg.GetPathName();
    RunScript(path);
    config->SaveCurrPath(IDS_SCRIPTPATH,fdlg.GetPathName());
  }
}

static UINT RunExternalScript(LPVOID ptr)
{
  const DlgManageScriptsItemInfo *nfo=*(const DlgManageScriptsItemInfo **)ptr;
  const char *thisname=*((const char **)ptr+1);
  HANDLE event=*((HANDLE *)ptr+2);
  CString cmdline=nfo->cmdline;
  if (cmdline.GetLength())
  {
    const char *sw=cmdline;
    do
    {
      while (*sw>=1 && *sw<33) sw++;
      if (*sw=='-') 
        while (*sw<0 || *sw>32) sw++;
    }
    while (*sw>=1 && *sw<33);
    cmdline.Insert(sw-cmdline,(CString(nfo->scriptPath)+" "));
    cmdline.Replace("%1",thisname);
  }
  else
  {
    cmdline=cmdline+nfo->scriptPath+"\""+thisname+"\"";
  }
  Pathname pth=Pathname::GetExePath();
  pth.SetFilename("O2Script.exe");

  HANDLE input,output;
  SECURITY_ATTRIBUTES sec;
  memset(&sec,0,sizeof(sec));
  sec.bInheritHandle=TRUE;
  if (nfo->inputFile)
  {
    do
      {
        CFileDialogEx inFile(TRUE,0,0,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,0);
      if (inFile.DoModal()!=IDOK) 
      {
        SetEvent(event);
        return 0;
      }
      input=CreateFile(inFile.GetPathName(),GENERIC_READ,FILE_SHARE_READ,&sec,OPEN_EXISTING,0,NULL);
      if (input==NULL) 
        AfxMessageBox(IDS_UNABLEOPENINPUTFILE);
      }
    while (input==NULL);
  }
  else
    input=NULL;
  Pathname logname;
  if (nfo->logOutput)
  {
    logname.SetTempDirectory();
    logname.SetTempFile();
    output=CreateFile(logname,GENERIC_WRITE,FILE_SHARE_READ,&sec,CREATE_ALWAYS,0,NULL);
    if (output==NULL)
    {
      AfxMessageBox(IDS_UNABLEOPENOUTPUTFILE);
      CloseHandle(input);
      SetEvent(event);
      return 0;
    }
  }
  else 
    output=NULL;

  cmdline=pth.GetFullPath()+(" "+cmdline);
  CString title;
  title.LoadString(
#ifdef PUBLIC
    IDS_SCRIPTEXTERNALTITLE_PUBLIC
#else 
    IDS_SCRIPTEXTERNALTITLE
#endif
    );
  STARTUPINFO stnfo;
  memset(&stnfo,0,sizeof(stnfo));
  stnfo.cb=sizeof(stnfo);
  stnfo.dwFlags=STARTF_USESTDHANDLES;
  AllocConsole();
  stnfo.hStdInput=input?input:GetStdHandle(STD_INPUT_HANDLE);
  stnfo.hStdOutput=output?output:GetStdHandle(STD_OUTPUT_HANDLE);;
  stnfo.hStdError=GetStdHandle(STD_ERROR_HANDLE);
  stnfo.lpTitle=title.LockBuffer();
  PROCESS_INFORMATION pi;
  if (CreateProcess(NULL,cmdline.LockBuffer(),NULL,NULL,TRUE,0,NULL,nfo->scriptPath.GetDirectoryWithDrive(),&stnfo,&pi)==FALSE)
  {
    int err=GetLastError();
    CloseHandle(input);
    CloseHandle(output);
    FreeConsole();
    AfxMessageBox(IDS_EXTERNALSCRIPTEXECUTEFAILED);
    SetEvent(event);
  }
  else
  {
    SetEvent(event);
    FreeConsole();
    WaitForSingleObject(pi.hProcess,INFINITE);
    CloseHandle(input);
    CloseHandle(output);
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
    int res;
    if (output)
    do
    {
      res=AfxMessageBox(IDS_SCRIPTLOGVIEW,MB_YESNOCANCEL|MB_ICONQUESTION);
      if (res==IDYES)
      {
        CString editor=config->GetString(IDS_SCRIPTEDITOR);
        if (editor.GetLength()==0) editor="notepad.exe";
        if ((UINT)ShellExecute(NULL,NULL,editor,logname,logname.GetDirectoryWithDrive(),SW_SHOWNORMAL)<32)
        {
          AfxMessageBox("Operation failed");
          res=IDRETRY;
        }
        else
        {
          Sleep(10000);
          DeleteFile(logname);
        }
      }
      else if (res==IDNO)
      {
        CFileDialogEx fdlg(FALSE,0,"output.log",OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,nfo->promptOutput);
        if (fdlg.DoModal()==IDYES)
        {
          if (MoveFileEx(logname,fdlg.GetPathName(),MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING)==FALSE)
          {
            AfxMessageBox("Operation failed");
            res=IDRETRY;
          }
        }
        else
          res=IDRETRY;
      }
      else if (res==IDCANCEL) DeleteFile(logname);
    }
    while (res==IDRETRY);
  }
  title.UnlockBuffer();
  cmdline.UnlockBuffer();
  return 0;
}

void CMainFrame::OnScriptsRunMenuScript(UINT cmd)
{
  const DlgManageScriptsItemInfo *info=_dlgmanscr.FromMenu(cmd,MIN_SCRIPT_CMD_VAL);
  if (info->saveFile) 
  {
    if (doc->AskSave()==FALSE)  return;
  };

  if (info->asExternal)
  {
    _scriptConsole.DetachIO();
    HANDLE h=CreateEvent(NULL,TRUE,FALSE,NULL);
    const void *args[3];
    args[0]=info;
    args[1]=doc->filename;
    args[2]=(void *)h;        
    AfxBeginThread((AFX_THREADPROC)RunExternalScript,&args);
    while (MsgWaitForMultipleObjects(1,&h,FALSE,INFINITE,QS_SENDMESSAGE)!=WAIT_OBJECT_0)
    {
      AfxPumpMessage();
    }
    CloseHandle(h);
    _scriptConsole.AttachIO();
  }
  else
  {
    _scriptConsole.EnableInput(true);
    RunScript(info->scriptPath);
    _scriptConsole.EnableInput(false);
  }
}

static void RunScriptSettings(const char *scriptName, void *context, HWND hWnd)
{
  CMainFrame *frm=(CMainFrame *)context;
  frm->RunScript(scriptName,true,hWnd);
}

void CMainFrame::OnManagescripts()
{
  _dlgmanscr.RunScriptSettings=RunScriptSettings;
  _dlgmanscr.RunScriptContext=this;
  if (_dlgmanscr.DoModal()==IDOK)
  {
    _dlgmanscr.InitMenu(GetMenu()->GetSubMenu(IPluginGeneralAppSide::mnuAutomation),MIN_SCRIPT_CMD_VAL,0,3);
    _dlgmanscr.SaveRegistry();
    ScriptRegMaintance();
  }
}

void CMainFrame::EnableWindowSpecial(CWnd *excluding, BOOL enable)
{
  disableMenu=enable==FALSE;
  CWnd *wnd=excluding;
  CWnd *parent=wnd->GetParent();
  while (parent && wnd!=this)
  {
    CWnd *sib=parent->GetNextWindow(GW_CHILD);
    while (sib) {sib->EnableWindow(enable || sib==wnd);sib=sib->GetNextWindow();}
    wnd=parent;
    parent=wnd->GetParent();
  }
}
void CMainFrame::OnScriptsConsolefont()
{  
  LOGFONT fnt=_scriptConsole.GetTextFont();
  CFontDialog fntdlg(&fnt);
  if (fntdlg.DoModal()==IDOK)
  {  
    fntdlg.GetCurrentFont(&fnt);
    theApp.WriteProfileBinary("Settings","ConsoleFont",(LPBYTE)&fnt,sizeof(fnt));
    _scriptConsole.SetTextFont(fnt);
  }
}


void CMainFrame::OnExviewerGroundfile()
{
  CDlgInputFile dlg;
  dlg.title.LoadString(IDS_EXTERNALSETGROUNDOBJECT);
  dlg.vFilename+=_externalCfg.groundObj;
  dlg.filter.LoadString(IDS_FILESAVEFILTERS);
  if (dlg.DoModal()==IDOK)
  {
        _externalCfg.groundObj=(LPCTSTR)dlg.vFilename;
        OnExtracommandsRefresh();
        StoreViewerConfig();
  }
}

void CMainFrame::OnExviewerGrounddefault(UINT cmd)
{
  _externalCfg.groundPos=(ExternalViewerConfig::GroundPosFlags)(cmd-ID_EXVIEWER_GROUNDDEFAULT);
  OnExtracommandsRefresh();
  StoreViewerConfig();

}

void CMainFrame::OnUpdateExviewerGrounddefault(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(pCmdUI->m_nID-ID_EXVIEWER_GROUNDDEFAULT==_externalCfg.groundPos);
  pCmdUI->Enable(_externalCfg.groundObj.GetLength());
}


LRESULT CMainFrame::OnOpenRVMATRequest( WPARAM wParam, LPARAM lParam)
{  
  PReceivedPackage package(&_externalRequests,lParam);
  if (!package.IsNull() && !package->IsStream())
  {
    const char *rvmatName=(const char *)package->Data();
    if (_MAT.IsLoaded())
    {
      ReplyMessage(1);
      _MAT.CallMat().EditMaterial(rvmatName);
      return 1;
    }
    else 
      return 0;
  }
  return 0;
}
void CMainFrame::OnExtviewerMarkerbrowse()
{
  CDlgInputFile dlg;
  dlg.title.LoadString(IDS_EXTERNALSETGROUNDOBJECT);
  dlg.vFilename+=_externalCfg.markerObj;
  dlg.filter.LoadString(IDS_FILESAVEFILTERS);
  if (dlg.DoModal()==IDOK)
  {
        _externalCfg.markerObj=(LPCTSTR)dlg.vFilename;
        OnExtracommandsRefresh();
        StoreViewerConfig();
  }
}

class FunctorAfterUpdateViewer
{
public:
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    plugin->GetPlugin().AfterViewerUpdated();
    return false;
  }

};

LRESULT CMainFrame::OnExViewerUpdateStatus(WPARAM status,LPARAM)
{
  ChangeViewerStatus(status);  
  if (status==0) theApp.ForEachPlugin(FunctorAfterUpdateViewer());
  return 0;
}
void CMainFrame::OnOtherStartnewinstance()
{
  _externalCfg.displayFlags|=_externalCfg.DontShareViewer;
  StartExternal();
  _externalCfg.displayFlags&=~_externalCfg.DontShareViewer;

}

//void CMainFrame::OnSourcecontrolEnabled()
//{
//  // TODO: Add your command handler code here
//}

//void CMainFrame::OnUpdateSourcecontrolEnabled(CCmdUI *pCmdUI)
//{
//  // TODO: Add your command update UI handler code here
//}

//void CMainFrame::OnSourcecontrolEnabled()
//{
//  // TODO: Add your command handler code here
//}


void CMainFrame::StartExternal()
{
  ProgressBar<> pb(1);
  pb.ReportState(PROGRESS_TIMEOUT,_externalCfg.init_timeout);
  pb.ReportState(IDS_STARTINGVIEWER);
  ExternalError(_externalViewer.Start());
}

LPARAM CMainFrame::OnMessageBgrYield(WPARAM wParam,LPARAM lParam)
{
  MSG msg;
  while (PeekMessage(&msg,0,0,0,PM_NOREMOVE)) 
  {
    if (msg.message==WM_QUIT) return 0;
    AfxPumpMessage();   
  }
  theApp.OnIdle(0);
  return 0;
}
void CMainFrame::OnUvsetsUveditor()
{
  _dlguveditor.SetMatPlugin(&_MAT.CallMat());
  if (_dlguveditor.GetSafeHwnd()==0)
  {
    _dlguveditor.Create(0);
    _dlguveditor.ShowWindow(SW_SHOW);
  }
  else
  {
    if (_dlguveditor.IsIconic()) _dlguveditor.OpenIcon();
    _dlguveditor.BringWindowToTop();
  }
_dlguveditor.UpdateFromDocument(static_cast<CObjektiv2Doc *>(GetActiveDocument()));
}

void CMainFrame::OnMacroLoadmacro()
{
#ifdef PUBLIC
    CFileDialogEx fdlg(TRUE,0,0,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST,CString(MAKEINTRESOURCE(IDS_MACRO_FILTER_PUBLIC)));
#else
    CFileDialogEx fdlg(TRUE,0,0,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST,CString(MAKEINTRESOURCE(IDS_MACRO_FILTER)));
#endif
    

  if (fdlg.DoModal())
  {
    if (recorder.LoadMacro(fdlg.GetPathName()))
    {
      AfxMessageBox(IDS_MACROLOADED,MB_ICONINFORMATION);
    }
    else
    {
      AfxMessageBox(IDS_MACROCANNOTLOAD,MB_ICONEXCLAMATION);
    }
  }
}

void CMainFrame::OnMacroSavelastmacroas()
{
#ifdef PUBLIC    
  CFileDialogEx fdlg(FALSE,"mo2",0,OFN_HIDEREADONLY|OFN_PATHMUSTEXIST,CString(MAKEINTRESOURCE(IDS_MACRO_FILTER_PUBLIC )));
#else
  CFileDialogEx fdlg(FALSE,"mo2",0,OFN_HIDEREADONLY|OFN_PATHMUSTEXIST,CString(MAKEINTRESOURCE(IDS_MACRO_FILTER )));
#endif

  if (fdlg.DoModal())
  {
    if (recorder.SaveMacro(fdlg.GetPathName()))
    {
      AfxMessageBox(IDS_MACROSAVED,MB_ICONINFORMATION);
    }
    else
    {
      AfxMessageBox(IDC_MACROCANNOTSAVE,MB_ICONEXCLAMATION);
    }
  }
}

void CMainFrame::OnPlaymacroForeachlod(UINT cmd)
{
  if (doc->LodData->Dirty())
  {
    if (AfxMessageBox(IDS_MACRO_STARTMULTIPLE,MB_YESNO)==IDNO)
      return;
  }
  int levels=doc->LodData->NLevels();
  for (int i=0;i<levels;i++)
  {
    float res=doc->LodData->Resolution(i);
    if (cmd==ID_PLAYMACRO_FOREACHLOD ||
      cmd==ID_PLAYMACRO_FOREACHRESOLUTIONLOD && (res>=0.0f && res<LOD_SHADOW_MIN || res==LOD_VIEW_COMMANDER) ||
      cmd==ID_PLAYMACRO_FOREACHEDITLOD && res>=LOD_EDIT_MIN && res<LOD_EDIT_MAX ||
      cmd==ID_PLAYMACRO_FOREACHGEOMETRYLOD && res>=LOD_MEMORY && res!=LOD_VIEW_COMMANDER)
    {    
      class CIChangeLod: public CICommand
      {
        int lid;
      public:
        CIChangeLod(int lid):lid(lid) {recordable=false;}
        int Execute(LODObject *obj)
        {
          obj->SelectLevel(lid);
          return 0;
        }
      };
      comint.Begin(WString(IDS_MACROCHANGELOD));
      comint.Run(new CIChangeLod(i));
      doc->UpdateAllToolbars();
      OnMacroPlaymacro();
      while (recorder.IsPlaybacking()) 
      {
        if (!AfxPumpMessage())
          theApp.OnIdle(0);
      }
      if (!recorder.WasFinished()) return;
    }
  }
}

void CMainFrame::OnPlaymacroForeachanimation()
{
  if (doc->LodData->Dirty())
  {
    if (AfxMessageBox(IDS_MACRO_STARTMULTIPLE,MB_YESNO)==IDNO)
      return;
  }
  ObjectData *obj=doc->LodData->Active();
  int nAnims=obj->NAnimations();
  for (int i=0;i<nAnims;i++)
  {
    class CIChangeAnim: public CICommand
    {
      int id;
    public:
      CIChangeAnim(int id):id(id) {recordable=false;}
      int Execute(LODObject *obj)
      {
        obj->Active()->UseAnimation(id);
        return 0;
      }
    };

    comint.Begin(WString(IDS_MACROCHANGEANIMATION));
    comint.Run(new CIChangeAnim(i));
    doc->UpdateAllToolbars();
    OnMacroPlaymacro();
    while (recorder.IsPlaybacking()) 
    {
      if (!AfxPumpMessage())  theApp.OnIdle(0);
    }
    if (!recorder.WasFinished()) return;
  }
}

void CMainFrame::OnSelectionmaskingInvertselection()
{
  _externalCfg.displayFlags^=ExternalViewerConfig::InvertedSelection;
  StoreViewerConfig();
  OnExtracommandsRefresh();
  
}

void CMainFrame::OnUpdateSelectionmaskingInvertselection(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(_externalCfg.displayFlags & ExternalViewerConfig::InvertedSelection);
}
