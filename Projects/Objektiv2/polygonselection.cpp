// PolygonSelection.cpp: implementation of the CPolygonSelection class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "PolygonSelection.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


bool CPolygonSelection::PointIsNeed(CPoint *from, CPoint *to, CPoint *test)
  {
  int a=-(from->y-to->y);
  int b=(from->x-to->x);
  int c=-(a*from->x+b*from->y);
  float d=(float)fabs((a*test->x+b*test->y+c)/(sqrt(a*a+b*b)));
  return d<2.0f;
  }

//--------------------------------------------------

bool CPolygonSelection::OptimizePointsOnePass()
  {
  CPoint *pt1, *pt2, *ptt, *movto; 
  int newcount=count;
  if (count<3) return false;
  bool res=false;
  pt1=points;
  movto=points;
  pt2=points+2;
  ptt=points+1;
  do
    {
    if (!PointIsNeed(pt1,pt2,ptt))
      {
      res=true;
      *movto++=*pt1;
      *movto++=*pt2;
      newcount--;
      }
    else
      {
      *movto++=*pt1;
      *movto++=*ptt;
      *movto++=*pt2;
      }
    pt1=pt2;
    pt2=pt1+2;
    ptt=pt1+1;
    }
  while (pt2-points<count && pt1-points<count);
  count=newcount;
  return res;
  }

//--------------------------------------------------

void CPolygonSelection::OptimizePoints()
  {
  while (OptimizePointsOnePass());
  }

//--------------------------------------------------

void CPolygonSelection::AddPoint(CPoint &pt, int index)
  {
  if (count>=MAXPOINTSINPOLYGON)
    {
    OptimizePoints();
    if (count>=MAXPOINTSINPOLYGON) return;
    }
  indexes[count]=index;
  points[count++]=pt;
  }

//--------------------------------------------------

extern CPen selpen;

void CPolygonSelection::Draw(CDC *pdc, bool preview)
  {
  if (count==0) return;
  if (preview)
    {
    pdc->SelectStockObject(HOLLOW_BRUSH);
    CPen *old=pdc->SelectObject(&selpen);
    pdc->Polyline(points,count);
    pdc->SelectObject(old);
    }
  else
    {
    pdc->SelectStockObject(NULL_PEN);
    pdc->SelectStockObject(BLACK_BRUSH);
    pdc->Polygon(points,count);
    }
  }

//--------------------------------------------------

void CPolygonSelection::DrawAsStrip(CDC *pdc)
  {
  if (count<3) Draw(pdc,true);
  CPoint pt[3];
  pt[0]=points[0];
  pt[1]=points[1];
  pdc->SelectStockObject(HOLLOW_BRUSH);
  CPen *old=pdc->SelectObject(&selpen);
  for (int i=2;i<count;i++)
    {
    pt[2]=points[i];
    pdc->Polygon(pt,3);
    if (i & 1) pt[1]=pt[2];else pt[0]=pt[2];
    }
  }

//--------------------------------------------------

void CPolygonSelection::DrawAsFan(CDC *pdc)
  {
  if (count<3) Draw(pdc,true);
  CPoint pt[3];
  pt[0]=points[0];
  pt[1]=points[1];
  pdc->SelectStockObject(HOLLOW_BRUSH);
  CPen *old=pdc->SelectObject(&selpen);
  for (int i=2;i<count;i++)
    {
    pt[2]=points[i];
    pdc->Polygon(pt,3);
    pt[1]=pt[2];
    }
  }

//--------------------------------------------------

