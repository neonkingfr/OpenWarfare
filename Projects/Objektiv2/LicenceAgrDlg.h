#if !defined(AFX_LICENCEAGRDLG_H__84C13119_B25E_467B_BD17_B1335E1369CE__INCLUDED_)
#define AFX_LICENCEAGRDLG_H__84C13119_B25E_467B_BD17_B1335E1369CE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LicenceAgrDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLicenceAgrDlg dialog

class CLicenceAgrDlg : public CDialog
  {
  // Construction
  bool readfull;
  public:
    CLicenceAgrDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CLicenceAgrDlg)
    enum 
      { IDD = IDD_LICENCEAGR };
    CButton	okbutt;
    CRichEditCtrl	richtext;
    //}}AFX_DATA
    
    static DWORD CALLBACK EditStreamCallback(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb);
    static void LoadLicenceToRichText(CRichEditCtrl &richtext);


    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CLicenceAgrDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CLicenceAgrDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnTimer(UINT nIDEvent);
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LICENCEAGRDLG_H__84C13119_B25E_467B_BD17_B1335E1369CE__INCLUDED_)
