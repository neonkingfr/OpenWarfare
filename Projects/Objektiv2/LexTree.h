// LexTree.h: interface for the CLexTree class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEXTREE_H__87865549_BFE9_11D4_B399_00C0DFAE7D0A__INCLUDED_)
#define AFX_LEXTREE_H__87865549_BFE9_11D4_B399_00C0DFAE7D0A__INCLUDED_

#include "LexAnl.h"

#include <string.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CLexTree  
  {
  class GeneralOp
    {
    protected:
      GeneralOp **child;
      int num;
      int priority;
    public:
      virtual float *Calculate(float *tostore, bool optimize)  = 0;
      //optimize true - nepocita kdyz nalezne promennou nebo funkci, jenz nelze optimizovat
      //pokud je tostore=NULL pak pozaduje lhodnotu
      //jinak funkce vraci pointer tostore
      //pokud vrati NULL nastala chyba /nebo nelze oprimalizovat
      void SetPriority(int p) 
        {priority=p;}
      GeneralOp **GetChildPtr(unsigned int id) 
        {id%=num;return child+id;}
      GeneralOp(unsigned int childs, int priority):priority(priority),num(childs)
        {child=new GeneralOp *[num];memset(child,0,sizeof(GeneralOp *)*num);}
      virtual ~GeneralOp() 
        {for (int i=0;i<num;i++) delete child[i];delete [] child;}
      bool AddToTree(GeneralOp **ptr);
    };
  
  class Varible:public GeneralOp
    {
    float *var;
    public:
      virtual float * Calculate(float *tostore,bool optimize);
      Varible(float *v):GeneralOp(0,OBJDATA_LATESTVERSION),var(v) 
        {}
    };
  
  class Number:public GeneralOp
    {
    float num;
    public:
      virtual float * Calculate(float *tostore,bool optimize);
      Number(float value):GeneralOp(0,OBJDATA_LATESTVERSION),num(value) 
        {}
    };
  
  class SpecialVarible:public GeneralOp
    {
    float *varptr;
    float *lvalue;
    public:
      virtual float * Calculate(float *tostore,bool optimize);
      SpecialVarible(float *value,float *lv):GeneralOp(0,OBJDATA_LATESTVERSION),varptr(value),lvalue(lv) 
        {}
    };
  
  class UnOp:public GeneralOp
    {
    LexSymbol symb;
    public:
      virtual float * Calculate(float *tostore, bool optimize);
      UnOp(LexSymbol s):GeneralOp(1,9000),symb(s) 
        {}
      
    };
  
  class BinOp:public GeneralOp
    {
    LexSymbol symb;
    public:
      virtual float * Calculate(float *tostore, bool optimize);
      BinOp(LexSymbol s,int priority):GeneralOp(2,priority),symb(s) 
        {}
    };
  
  class Assign:public GeneralOp
    {
    public:
      virtual float * Calculate(float *tostore, bool optimize);
      Assign():GeneralOp(2,1500) 
        {}
    };
  
  class Command:public GeneralOp
    {
    public:
      virtual float *Calculate(float *tostore, bool optimize);
      Command():GeneralOp(2,500) 
        {}
    };
  
  class Iff:public GeneralOp
    {
    public:
      virtual float *Calculate(float *tostore, bool optimize);
      Iff(GeneralOp *cond, GeneralOp *exp1,GeneralOp *exp2):GeneralOp(3,9000)
        {child[0]=cond;child[1]=exp1;child[2]=exp2;}
    };
  
  class While:public GeneralOp
    {
    public:
      virtual float *Calculate(float *tostore, bool optimize);
      While(GeneralOp *cond, GeneralOp *comm):GeneralOp(2,9000)
        {child[0]=cond;child[1]=comm;}
    };
  
  class Repeat:public GeneralOp
    {
    public:
      virtual float *Calculate(float *tostore, bool optimize);
      Repeat(GeneralOp *comm): GeneralOp(1,9000)
        {child[0]=comm;}
    };
  
  class For:public GeneralOp
    {
    public:
      virtual float *Calculate(float *tostore,bool optimize);
      For(GeneralOp *cond, GeneralOp *exp1,GeneralOp *exp2):GeneralOp(3,9000)
        {child[0]=cond;child[1]=exp1;child[2]=exp2;}
      
    };
  
  GeneralOp *tree;
  public:
    void *SwitchTree(void *other); //umoznuje ulozit strom do promenne
    bool Calculate(float &out);
    int Parse(const char *program);
    bool Parse(CLexAnl &lex);
    float xval,lxval,yval,lyval,zval,lzval,tuval,ltuval,tvval,ltvval,weightval,lweightval;
    float varspace['Z'-'A'+1];
    void SetVarible(char var, float val) 
      {if (var>='A' && var<='Z') varspace[var-'A']=val;}
    float GetVarible(char var) 
      {if (var>='A' && var<='Z') return varspace[var-'A'];else return 0.0f;}
    CLexTree();
    virtual ~CLexTree();
    
  private:
    CLexTree::GeneralOp *BuildTree(CLexAnl& lex,LexSymbol stop);
  };

//--------------------------------------------------

#endif // !defined(AFX_LEXTREE_H__87865549_BFE9_11D4_B399_00C0DFAE7D0A__INCLUDED_)
