// SplashWin.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "SplashWin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSplashWin

CSplashWin::CSplashWin()
  {
  }

//--------------------------------------------------

CSplashWin::~CSplashWin()
  {
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CSplashWin, CWnd)
  //{{AFX_MSG_MAP(CSplashWin)
  ON_WM_PAINT()
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      
      /////////////////////////////////////////////////////////////////////////////
      // CSplashWin message handlers
      
      void CSplashWin::Create()
        {
        #ifndef PUBLIC
        if ((GetTickCount()>>8) & 1) pic.LoadBitmap(IDB_SPLASH);
        else pic.LoadBitmap(IDB_SPLASH2);
        #else
        pic.LoadBitmap(IDB_SPLASH);
        #endif
        BITMAP bp;
        pic.GetBitmap(&bp);
        CRect scr;
        GetDesktopWindow()->GetWindowRect(&scr);
        CRect rc(0,0,bp.bmWidth+2,bp.bmHeight+2);
        rc+=CSize(scr.right/2-bp.bmWidth/2,scr.bottom/2-bp.bmHeight/2);
        this->CreateEx(0,AfxRegisterWndClass(0),"Objektiv2",
        WS_POPUPWINDOW|WS_VISIBLE,rc,NULL,0,NULL);  
        SetIcon(theApp.LoadIcon(IDR_MAINFRAME),TRUE);
        }

//--------------------------------------------------

void CSplashWin::OnPaint() 
  {
  CPaintDC dc(this); // device context for painting
  BITMAP bp;	
  pic.GetBitmap(&bp);
  
  CDC bitmapdc;
  bitmapdc.CreateCompatibleDC(&dc);
  CBitmap *old=bitmapdc.SelectObject(&pic);
  dc.BitBlt(0,0,bp.bmWidth,bp.bmHeight,&bitmapdc,0,0,SRCCOPY);
  bitmapdc.SelectObject(old);	
  // Do not call CWnd::OnPaint() for painting messages
  }

//--------------------------------------------------

