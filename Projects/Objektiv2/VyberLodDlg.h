#if !defined(AFX_VYBERLODDLG_H__BA6F4B4B_C2B2_4A28_A403_682C6EAF0156__INCLUDED_)
#define AFX_VYBERLODDLG_H__BA6F4B4B_C2B2_4A28_A403_682C6EAF0156__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// VyberLodDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CVyberLodDlg dialog

class CVyberLodDlg : public CDialog
  {
  // Construction
  public:
    LODObject *lodobj;
    float sellod;
    CVyberLodDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CVyberLodDlg)
    enum 
      { IDD = IDD_VYBERLOD };
    CComboBox	wLod;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CVyberLodDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CVyberLodDlg)
    virtual BOOL OnInitDialog();
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VYBERLODDLG_H__BA6F4B4B_C2B2_4A28_A403_682C6EAF0156__INCLUDED_)
