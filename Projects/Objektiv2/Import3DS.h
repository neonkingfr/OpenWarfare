#if !defined(AFX_IMPORT3DS_H__92F4B365_BB5D_4954_B720_2DF920C428C3__INCLUDED_)
#define AFX_IMPORT3DS_H__92F4B365_BB5D_4954_B720_2DF920C428C3__INCLUDED_

#include "Import3dsTexConvDlg.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Import3DS.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImport3DS dialog

#include "..\g3d\3ds_trc\3dsFile2.h"

class CImport3DSDlg : public CDialog
  {
  // Construction	
  struct SObjectInfo
    {
    static int defaultframecount; //static varible
    float objlod;
    C3dsNamedObject *objref;
    int frames;
    int parent;
    CGMatrix *mlist;
    SObjectInfo():frames(defaultframecount),objlod(0.0) 
      {mlist=new CGMatrix[defaultframecount];}
    ~SObjectInfo() 
      {delete [] mlist;}
    };
  bool _itemNoChange;
  int objects;
  int frames;
  SObjectInfo *objlist;
  C3dsMaster master;
  BOOL ImportError(int ids,...);
  UINT framebeg;
  UINT frameend;
  UINT progresbeg;
  UINT progressize;
  public:
    CImport3dsTexConvDlg texconvdlg;
    void Import3DSObject(SObjectInfo&nfo, ObjectData *obj);
    CImport3DSDlg(CWnd* pParent = NULL);   // standard constructor
    LODObject *lod;
    Pathname f3ds;
    
    // Dialog Data
    //{{AFX_DATA(CImport3DS)
	enum { IDD = IDD_IMPORT3DS };
    CStatic	wStaticPrefix;
    CStatic	wStaticPath;
    CEdit	wPath;
    CEdit	wPrefix;
    CListCtrl	wObjectList;
    UINT	maxFrame;
    UINT	minFrame;
    CString	framereport;
    BOOL	noanim;
    BOOL  mattexname;
    UINT	scalemag;
    UINT	scalemin;
    BOOL	convtex;
    CString	texpath;
	BOOL	nosquarize;
	BOOL	nomerge;
	//}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CImport3DS)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CImport3DS)
    virtual BOOL OnInitDialog();
    afx_msg void OnDestroy();
    afx_msg void OnItemchangedObjectlist(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnSetlod();
    virtual void OnOK();
    afx_msg void OnDblclkObjectlist(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnContex();
    afx_msg void OnChangePrefix();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORT3DS_H__92F4B365_BB5D_4954_B720_2DF920C428C3__INCLUDED_)
