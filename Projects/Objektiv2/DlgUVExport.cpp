// DlgUVExport.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgUVExport.h"
#include ".\dlguvexport.h"

bool DlgUVExport::Save_keepAspect=false;
bool DlgUVExport::Save_addTextureOutline=false;
bool DlgUVExport::Save_specifyDim=false;
int DlgUVExport::Save_dimWidth=2048;
int DlgUVExport::Save_dimHeight=2048;



IMPLEMENT_DYNAMIC(DlgUVExport, CFileDialogEx)
DlgUVExport::DlgUVExport(BOOL bOpenFileDialog, LPCTSTR lpszDefExt /* = NULL  */, LPCTSTR lpszFileName /* = NULL  */, DWORD dwFlags /* = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT  */, LPCTSTR lpszFilter /* = NULL  */, CWnd* pParentWnd /* = NULL  */, DWORD dwSize /* = 0  */)
: CFileDialogEx(bOpenFileDialog,lpszDefExt,lpszFileName,dwFlags|OFN_ENABLETEMPLATE,lpszFilter,pParentWnd)
{
  m_pOFN->Flags|=OFN_ENABLESIZING;
  _keepAspect=Save_keepAspect;
  _addTextureOutline=Save_addTextureOutline;
  _specifyDim=Save_specifyDim;
  _dimWidth=Save_dimWidth;
  _dimHeight=Save_dimHeight;
  _aspect=1;
  m_pOFN->hInstance=AfxGetInstanceHandle();
  m_pOFN->lpTemplateName=MAKEINTRESOURCE(IDD);
}

DlgUVExport::~DlgUVExport()
{
}


BEGIN_MESSAGE_MAP(DlgUVExport, CFileDialogEx)
  ON_BN_CLICKED(IDC_CHECK3, OnBnClickedCheck3)
  ON_EN_CHANGE(IDC_EDIT1, OnEnChangeEdit1)
  ON_EN_CHANGE(IDC_EDIT5, OnEnChangeEdit5)
  ON_BN_CLICKED(IDC_CHECK1, OnBnClickedCheck1)
END_MESSAGE_MAP()


// DlgUVExport message handlers

BOOL DlgUVExport::OnInitDialog()
{
  CFileDialogEx::OnInitDialog();

  _wKeepAspect.SubclassDlgItem(IDC_CHECK1,this);
  _wAddTextureOutline.SubclassDlgItem(IDC_CHECK2,this);
  _wSpecifyDim.SubclassDlgItem(IDC_CHECK3,this);
  _wDimWidth.SubclassDlgItem(IDC_EDIT1,this);
  _wDimHeight.SubclassDlgItem(IDC_EDIT5,this);

  char buff[50];

  _wKeepAspect.SetCheck(_keepAspect?1:0);
  _wAddTextureOutline.SetCheck(_addTextureOutline?1:0);
  _wSpecifyDim.SetCheck(_specifyDim?1:0);
  _wDimWidth.SetWindowText(_itoa(_dimWidth,buff,10));
  _wDimHeight.SetWindowText(_itoa(_dimHeight,buff,10));

  DialogRules();

  return CFileDialogEx::OnInitDialog();  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgUVExport::DialogRules()
{
  BOOL checked=_wSpecifyDim.GetCheck()==1;
  _wDimWidth.EnableWindow(checked);
  _wDimHeight.EnableWindow(checked);
}
void DlgUVExport::OnBnClickedCheck3()
{
  DialogRules();
}

BOOL DlgUVExport::OnFileNameOK()
{

  _keepAspect=_wKeepAspect.GetCheck()!=0; 
  _addTextureOutline=_wAddTextureOutline.GetCheck()!=0;
  _specifyDim=_wSpecifyDim.GetCheck()!=0;
  _dimWidth=GetDlgItemInt(_wDimWidth.GetDlgCtrlID(),0,FALSE);
  _dimHeight=GetDlgItemInt(_wDimHeight.GetDlgCtrlID(),0,FALSE);  

  Save_keepAspect=_keepAspect;
  Save_addTextureOutline=_addTextureOutline;
  Save_specifyDim=_specifyDim;
  Save_dimWidth=_dimWidth;
  Save_dimHeight=_dimHeight;

  if (CFileDialogEx::OnFileNameOK()==FALSE) return FALSE;
  return TRUE;
}

void DlgUVExport::OnEnChangeEdit1()
{
  if (GetFocus()==&_wDimWidth || GetFocus()==&_wKeepAspect)
  {  
    int x=GetDlgItemInt(_wDimWidth.GetDlgCtrlID(),0,FALSE);
    if (_wKeepAspect.GetCheck())
    {
      int y=toLargeInt((float)x/_aspect);
      SetDlgItemInt(_wDimHeight.GetDlgCtrlID(),y,FALSE);
    }
  }
}

void DlgUVExport::OnEnChangeEdit5()
{
  if (GetFocus()==&_wDimHeight)
  {  
    int y=GetDlgItemInt(_wDimHeight.GetDlgCtrlID(),0,FALSE);
    if (_wKeepAspect.GetCheck())
    {
      int x=toLargeInt((float)y*_aspect);
      SetDlgItemInt(_wDimWidth.GetDlgCtrlID(),x,FALSE);
    }
  }
}

void DlgUVExport::OnBnClickedCheck1()
{
  OnEnChangeEdit1();
}
