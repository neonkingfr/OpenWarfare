// TextureBlock.h: interface for the CTextureBlock class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXTUREBLOCK_H__C7016D48_FA8F_4056_AEDA_56B809AABF68__INCLUDED_)
#define AFX_TEXTUREBLOCK_H__C7016D48_FA8F_4056_AEDA_56B809AABF68__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CTextureBlock  
  {
  CString texname;
  CBitmap texbitmap;
  HBITMAP old;
  CDC texdc;
  bool invalid;
  int sizex,sizey;
  CRect rect;
  public:
    void Reset();
    bool IsInvalid() 
      {return invalid;}
    void SetRect(CRect& rc);
    void SetBitmap(CDC *compdc, const char *bitmapname);
    void Draw(CDC *pDC, bool uchytky);
    CTextureBlock() 
      {invalid=true;}
    virtual ~CTextureBlock();
  };

//--------------------------------------------------

#endif // !defined(AFX_TEXTUREBLOCK_H__C7016D48_FA8F_4056_AEDA_56B809AABF68__INCLUDED_)
