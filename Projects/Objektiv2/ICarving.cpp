// ICarving.cpp: implementation of the CICarving class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "ICarving.h"
#include "gePlane.h"
#include "geVector4.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static void SplitOptimizeFace(FaceT &fc)
  {
  bool opak=true;
  while (opak)
    {
    opak=false;
    for (int p=0;p<fc.N();p++)
      {
      int q=p+1;
      if (q>=fc.N()) q=0;
      if (fc.GetPoint(p)==fc.GetPoint(q))
        {
        if (q>p)
          for (int j=q;j<fc.N();j++) fc.CopyPointInfo(j-1,j);
        fc.SetN(fc.N()-1);
        opak=true;
        }
      }
    }
  }

static inline bool TestIntersection(const gePlane2V &plane, const geVector4 &p1, const geVector4 &p2)
  {
  geVector4 isectinfo=plane.Intersection(geLine(p1,p2));
  if (isectinfo.IsFinite())
    {
    if (isectinfo.x>=-0.001f && isectinfo.y>=-0.001f && isectinfo.y+isectinfo.x<=1.001f)
      {
      return true;
      }
    }
  return false;
  }

static bool SplitTwoTriangles(const geVector4 &t11, const geVector4 &t12, const geVector4 &t13,
                              const geVector4 &t21, const geVector4 &t22, const geVector4 &t23,
                              gePlaneGen *split1, gePlaneGen *split2)
  {
  gePlane2V p1(t11,t12,t13);
  gePlane2V p2(t21,t22,t23);
  if (split1) *split1=gePlaneGen(p1);
  if (split2) *split2=gePlaneGen(p2);
  return TestIntersection(p2,t11,t12) || TestIntersection(p2,t12,t13) || TestIntersection(p2,t13,t11) ||
         TestIntersection(p1,t21,t22) || TestIntersection(p1,t22,t23) || TestIntersection(p1,t23,t21);
  }

struct IntersectionCalculateInfo
  {
  geVector4 face1[4];
  geVector4 face2[4];
  int n1; //count points of face1
  int n2; //count points of face2
  geVector4 splitpts1[2]; //split points for face1
  geVector4 splitpts2[2]; //split points for face2
  int pos1[2];    //split positions for face1
  int pos2[2];    //split positions for face2
  };

//spocita intersectni body facu s plochou.
//Body jsou nalezene jen 2. Program predpoklada, ze facy jsou konvexni
static bool FindIntersectionPoints(const gePlaneGen &splitplane, const geVector4 *points, int ptcnt, geVector4 *splitpts, int *respos)
  {
  int i;
  int pos=0;
  for (i=0;i<ptcnt;i++)   //prochazej vsechny body
    {
    float k;
    int j=i+1;
    if (j>=ptcnt) j-=ptcnt;
    geLine line(points[i],points[j]); // z kazde sousedni dvojice vyrob usecku
    if (splitplane.Intersection(line,k)!=0.0f && k>=0.0f && k<=1.0f) //test zda existuje prusecick s useckou
      {
      geVector4 result=line.GetPoint(k);  //vypocitej prusecik
      if (pos==2) //pokud mame dva pruseciky
        {
        //pokud je prusecik dal nez dosavadni nalezene...
        //algoritmus predpoklada, ze byly nalezeny dva blizke body, ktery vlivem zaokrouhlovani
        //mel byt jen jeden. Pokud je dalsi bod daleji, jeden z blizkych bodu je neplatny (jedno ktery)
        if (result.Distance2(splitpts[0])<splitpts[1].Distance2(splitpts[0])) continue; 
        pos--; //prepisuj bod 1
        }
      splitpts[pos]=result; //zapis vysledek
      respos[pos]=i; //zapis pozici
      pos++; //zvyz zapisovou pozici 
      } //prohledava dalsi body prestoze ma jiz 2. To proto aby eliminoval falesne poplachy
    }
  return pos==2;
  }

static bool SplitTwoFaces(IntersectionCalculateInfo &info)
  {
  if (info.n1<3 || info.n1>4 || info.n2<3 || info.n2>4) return false;
  bool split;
  gePlaneGen splitplane1;
  gePlaneGen splitplane2;
  
  split=SplitTwoTriangles(info.face1[0],info.face1[1],info.face1[2],
                          info.face2[0],info.face2[1],info.face2[2],
                          &splitplane1,&splitplane2);
  if (!split && info.n1==4)    
    split=SplitTwoTriangles(info.face1[0],info.face1[2],info.face1[3],
                            info.face2[0],info.face2[1],info.face2[2],
                            NULL,NULL);

  if (!split && info.n2==4)
    split=SplitTwoTriangles(info.face1[0],info.face1[1],info.face1[2],
                            info.face2[0],info.face2[2],info.face2[3],
                            NULL,NULL);
  if (!split && info.n1==4 && info.n2==4)
    split=SplitTwoTriangles(info.face1[0],info.face1[2],info.face1[3],
                            info.face2[0],info.face2[2],info.face2[3],
                            NULL,NULL);
  if (!split) return false;
  if (FindIntersectionPoints(splitplane2,info.face1,info.n1,info.splitpts1,info.pos1)==false) return false;
  if (FindIntersectionPoints(splitplane1,info.face2,info.n2,info.splitpts2,info.pos2)==false) return false;
  return true;
  }

static inline PosT PosType(const geVector4 &vx, PosT &templ)
  {
  PosT ps=templ;
  ps[0]=vx.x;
  ps[1]=vx.y;
  ps[2]=vx.z;
  return ps;
  }

static bool SplitFace(ObjectData &obj, int fc, int *sptpos, geVector4 *splitvect)
  {
  FaceT fc1(obj,fc);  
  int e11=sptpos[0];
  int e12=(sptpos[0]+1==fc1.N())?0:sptpos[0]+1;
  int e21=sptpos[1];
  int e22=(sptpos[1]+1==fc1.N())?0:sptpos[1]+1;
  PosT vp1=PosType(splitvect[0],obj.Point(fc1.GetPoint(sptpos[0])));
  PosT vp2=PosType(splitvect[1],obj.Point(fc1.GetPoint(sptpos[1])));
  PosT vp1l=obj.Point(fc1.GetPoint(e11));
  PosT vp1r=obj.Point(fc1.GetPoint(e12));
  PosT vp2l=obj.Point(fc1.GetPoint(e21));
  PosT vp2r=obj.Point(fc1.GetPoint(e22));
  float f1=vp1.Distance(vp1l)/vp1r.Distance(vp1l);
  float f2=vp2.Distance(vp2l)/vp2r.Distance(vp2l);
  if (f1>0.95 || f1<0.05) return false;
  if (f2>0.95 || f2<0.05) return false;
  int nwp1=obj.ReservePoints(2);
  int nwp2=nwp1+1;
  obj.Point(nwp1)=vp1;
  obj.Point(nwp2)=vp2;
  float tu1=fc1.GetU(e11)*(1-f1)+fc1.GetU(e12)*f1;
  float tv1=fc1.GetV(e11)*(1-f1)+fc1.GetV(e12)*f1;
  float tu2=fc1.GetU(e21)*(1-f2)+fc1.GetU(e22)*f2;
  float tv2=fc1.GetV(e21)*(1-f2)+fc1.GetV(e22)*f2;
  FaceT ff1=fc1;
  FaceT ff2=fc1;
  FaceT ff3=fc1;

  ff1.SetN(e11==e22?3:4);  
  ff1.SetPoint(0,fc1.GetPoint(e11));
  ff1.SetPoint(1,nwp1);
  ff1.SetPoint(2,nwp2);
  ff1.SetPoint(3,fc1.GetPoint(e22));

  ff1.SetU(0,fc1.GetU(e11));
  ff1.SetV(0,fc1.GetV(e11));
  ff1.SetU(1,tu1);
  ff1.SetV(1,tv1);
  ff1.SetU(2,tu2);
  ff1.SetV(2,tv2);
  ff1.SetU(3,fc1.GetU(e22));
  ff1.SetV(3,fc1.GetV(e22));

  ff2.SetN(e21==e12?3:4);
  ff2.SetPoint(0,fc1.GetPoint(e21));
  ff2.SetPoint(1,nwp2);
  ff2.SetPoint(2,nwp1);
  ff2.SetPoint(3,fc1.GetPoint(e12));
 
  ff2.SetU(0,fc1.GetU(e21));
  ff2.SetV(0,fc1.GetV(e21));
  ff2.SetU(1,tu2);
  ff2.SetV(1,tv2);
  ff2.SetU(2,tu1);
  ff2.SetV(2,tv1);
  ff2.SetU(3,fc1.GetU(e12));
  ff2.SetV(3,fc1.GetV(e12));

  bool exists3=false;

  for (int np=0;np<fc1.N();np++)
    if (np!=e11 && np!=e12 && np!=e21 && np!=e22)
      {
      exists3=true;
      ff3.CopyPointInfo(0,fc1,np);
      ff3.CopyPointInfo(1,fc1,np+1>=fc1.N()?0:np+1);
      ff3.CopyPointInfo(2,fc1,np-1<0?fc1.N()-1:np-1);
      ff3.SetN(3);
      }
  SplitOptimizeFace(ff1);
  SplitOptimizeFace(ff2);
  if (exists3) SplitOptimizeFace(ff3);
  if (ff1.N()<3) 
    {
    if (exists3==false) return false;
    ff1=ff2;
    ff2=ff3;
    exists3=false;
    if (ff1.N()<3) return false;
    }
  if (ff2.N()<3)
    {
    if (exists3==false) return false;
    ff2=ff3;
    exists3=false;
    if (ff2.N()<3) return false;
    }
  if (exists3 && ff3.N()<3)
    exists3=false;  
  int fac1=fc;
  int fac2=obj.ReserveFaces(exists3?2:1);
  int fac3=fac2+1;
  ff1.CopyFaceTo(fac1);
  ff2.CopyFaceTo(fac2);
  obj.FaceSelect(fac2,obj.FaceSelected(fac1));
  if (exists3)
    {
    ff3.CopyFaceTo(fac3);
    obj.FaceSelect(fac3,obj.FaceSelected(fac1));
    }
  return true;
  }

static void ProcessTwoFaces(ObjectData &obj,int face1, int face2)
  {
  IntersectionCalculateInfo info;
  int i;
  FaceT fc1(obj,face1);
  FaceT fc2(obj,face2);
  for (i=0;i<fc1.N();i++)
    {
    PosT &pos=obj.Point(fc1.GetPoint(i));
    info.face1[i]=geVector4(pos[0],pos[1],pos[2]);
    }
  info.n1=fc1.N();
  for (i=0;i<fc2.N();i++)
    {
    PosT &pos=obj.Point(fc2.GetPoint(i));
    info.face2[i]=geVector4(pos[0],pos[1],pos[2]);
    }
  info.n2=fc2.N();
  if (SplitTwoFaces(info))
    {
    SplitFace(obj,face1,info.pos1,info.splitpts1);
    SplitFace(obj,face2,info.pos2,info.splitpts2);
    }
  }

struct IntersectCalculationInfo2
  {
  geVector4 face1[4];
  int nface1;
  geVector4 face2[4];
  int nface2;
  geLine result;
  };


//Funkce orezava primku useckama face. Primka musi lezet na rovine face
//Algoritmus prevadi usecky na roviny, kteryma se usecka orezava
static bool OrezavaniPrimky(geVector4 face[4],int n, geLine& line, gePlaneGen &plane, bool onlyinside)
  {
  float klist[2];  //dostatecna zasoba kcek
  int kcount=0;   //pocet kcek
  for (int i=0;i<n;i++) //pro vsechny vrcholy
    {
    float k;
    int j=i+1;if (j>=n) j-=n;
    geLine edge(face[i],face[j]); //vytvor z dvojice hranu
    gePlaneGen edgePlane(plane,edge); //vytvor hranicni rovinu
    if (edgePlane.Intersection(line,k)!=0.0)  //najdi prusecik s primky s rovinou
      {
      geVector4 pt=line.GetPoint(k);    //ziskej souradnice pruseciku 
      float ek=edge.MapPoint(pt);       //mapuj prusecik na hranu
      if (ek>=0.0 && ek<=1.0)           //lezi prusecik na v usecce?
        {
        if (kcount==2)    //nemame pruseciky?
          {
          if (fabs(k-klist[0])<fabs(klist[0]-klist[1])) //kontroluj vzdalenosti pruseciku
            continue; //prilis mala vzdalenost - falesny poplach
          kcount--;
          }
        klist[kcount++]=k; //zaloz prusecik ho jako novy
        }
      }
    }
  if (kcount<2) return false;
  if (klist[0]>klist[1]) 
    {
    klist[0]=klist[0]+klist[1]; //a=a+b  b=b;
    klist[1]=klist[0]-klist[1]; //a=a+b  b=a+b-b = a
    klist[0]=klist[0]-klist[1]; //a=a+b-a = b
    }    
  if (onlyinside)
    {
    if (klist[1]>1.0f) klist[1]=1.0f;
    if (klist[0]<0.0f) klist[0]=0.0f;
    if (klist[0]>klist[1]) return false; //nesmysl, spolecna usecka neexistuje
    }
  geVector4 pt1=line.GetPoint(klist[0]); //vytvor usecku z primky nebo z jine usecky
  geVector4 pt2=line.GetPoint(klist[1]);
  line=geLine(pt1,pt2);
  return true;
  }

static bool FindIntersectionLine(IntersectCalculationInfo2 &info)
  {
  gePlaneGen plane1(info.face1[0],info.face1[1],info.face1[2]);
  gePlaneGen plane2(info.face2[0],info.face2[1],info.face2[2]);
  geLine primka=plane1.Intersection(plane2);
  if (OrezavaniPrimky(info.face1,info.nface1,primka,plane1,false)==false) return false;
  if (OrezavaniPrimky(info.face2,info.nface2,primka,plane2,true)==false) return false;
  info.result=primka; 
  return true;
  }

static int FindEdgePoint(geVector4 face[4], int n, geVector4 &pt)
  {
  for (int i=0;i<n;i++)
    {
    int j=i+1;if (j>=n) j-=n;
    geLine edge(face[i],face[j]);
    float dist=edge.Distance(pt);
    if (dist<0.001f) 
      {
      float k=edge.MapPoint(pt);
      if (k<0.005 || k>0.995) return -2;
      return i;
      }
    }
  return -1;
  }

static void SplitFaceByPoint(ObjectData &obj, int findex, geVector4 &vx, int edgepoint)
  {
  if (edgepoint==-2)
    return;
  FaceT fc(obj);
  fc=FaceT(obj,findex);
  int nwpt=obj.ReservePoints(1);
    {
    obj.Point(nwpt)=PosType(vx,obj.Point(fc.GetPoint(0)));
    int nwfindex=obj.ReserveFaces(3);
    bool frst=true;
    for (int i=0;i<fc.N();i++) 
      {
      int j=(i+1)%fc.N();      
      if (i!=edgepoint)
        {        
        FaceT nwfc(obj,frst?findex:nwfindex+i-1);
        nwfc.SetN(3);
        nwfc.SetPoint(0,nwpt);
        nwfc.SetPoint(1,fc.GetPoint(i));
        nwfc.SetPoint(2,fc.GetPoint(j));
        if (!frst) obj.FaceSelect(nwfindex+i-1,obj.FaceSelected(findex));
        frst=false;
        }
      }
    }
  }

static void ProcessTwoFaces2(ObjectData &obj, int face1, int face2, bool ndline)
  {
  IntersectCalculationInfo2 info;
  int i;
  FaceT fc1(obj,face1);
  FaceT fc2(obj,face2);
  for (i=0;i<fc1.N();i++)
    {
    PosT &pos=obj.Point(fc1.GetPoint(i));
    info.face1[i]=geVector4(pos[0],pos[1],pos[2]);
    }
  info.nface1=fc1.N();
  for (i=0;i<fc2.N();i++)
    {
    PosT &pos=obj.Point(fc2.GetPoint(i));
    info.face2[i]=geVector4(pos[0],pos[1],pos[2]);
    }
  info.nface2=fc2.N();
  if (FindIntersectionLine(info)==false) return;
  geVector4 vx1=info.result.GetPoint(0.0f),vx2=info.result.GetPoint(1.0f);
  int edge;
  if (ndline)
    {edge=FindEdgePoint(info.face1,info.nface1,vx2);SplitFaceByPoint(obj,face1,vx2,edge);}
  else
    {edge=FindEdgePoint(info.face1,info.nface1,vx1);SplitFaceByPoint(obj,face1,vx1,edge);}
  }

CICarving::CICarving()
  { 
  }

CICarving::~CICarving()
  {

  }

CICommand *CICarving_Create(istream &str)
  {
  return new CICarving(str);
  }

int CICarving::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();  
  const Selection *hid=obj->GetHidden();
  ProgressBar<int> pb(obj->NFaces());
  for (int i=0;i<obj->NFaces();i++)
  {
    if (obj->FaceSelected(i))
      {      
      for (int j=0;j<obj->NFaces();j++)
        if (!hid->FaceSelected(j) && !obj->FaceSelected(j))
          {
          ProcessTwoFaces2(*obj,j,i,false);        
          ProcessTwoFaces2(*obj,j,i,true);        
          }
      }
      pb.AdvanceNext(1);
  }
  return 0;
  }