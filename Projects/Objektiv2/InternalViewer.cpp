// InternalViewer.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "InternalViewer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInternalViewer

IMPLEMENT_DYNCREATE(CInternalViewer, CView)
  
  CInternalViewer::CInternalViewer()
    {
    }

//--------------------------------------------------

CInternalViewer::~CInternalViewer()
  {
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CInternalViewer, CView)
  //{{AFX_MSG_MAP(CInternalViewer)
  ON_WM_DESTROY()
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CInternalViewer drawing
      
      void CInternalViewer::OnDraw(CDC* pDC)
        {
        CDocument* pDoc = GetDocument();
        // TODO: add draw code here
        }

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// CInternalViewer diagnostics

#ifdef _DEBUG
void CInternalViewer::AssertValid() const
  {
  CView::AssertValid();
  }

//--------------------------------------------------

void CInternalViewer::Dump(CDumpContext& dc) const
  {
  CView::Dump(dc);
  }

//--------------------------------------------------

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CInternalViewer message handlers
