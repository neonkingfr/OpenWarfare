#include "afxwin.h"
#if !defined(AFX_FACEPROP_H__D598A4EB_2442_470B_B7E0_3CCDC636EAA4__INCLUDED_)
#define AFX_FACEPROP_H__D598A4EB_2442_470B_B7E0_3CCDC636EAA4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FaceProp.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFaceProp dialog

class CFaceProp : public CDialog
  {
  // Construction
  unsigned int deftmr;
  public:
	  void Apply();
    void SetInt(CString& s, int f, bool first);
    void ForAllSelectedLoad();
    void LoadToList();
    void GetDialogData(FaceT &face, FaceT &mask);
    void SetDialogData(FaceT& face, bool first);
    CFaceProp(CWnd* pParent = NULL);   // standard constructor
    ObjectData *obj;
    // Dialog Data
    //{{AFX_DATA(CFaceProp)
    enum 
      { IDD = IDD_FACE_PROPERTIES };
    CButton	BBrowse2;
    CListCtrl	List;
    CButton	BSetFromView;
    CButton	BSetFromList;
    CButton	BBrowse;
    CButton BColor;
    CButton	EnTexMerge;
    CButton	EnShadow;    
    int		Bias;
    int		Lighting;
    CString	VertNum;
    CString	TU1;
    CString	TU2;
    CString	TU3;
    CString	TU4;
    CString	TV1;
    CString	TV2;
    CString	TV3;
    CString	TV4;
    CString	TexName;
    CString	userValue;
    BOOL	selcreat;
    CString	MatName;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CFaceProp)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CFaceProp)
    afx_msg void OnSelchangeList();
    virtual BOOL OnInitDialog();
    afx_msg void OnSelectall();
    afx_msg void OnFilter();
    afx_msg void OnApply();
    virtual void OnOK();
    afx_msg void OnSetTexture();
    afx_msg void OnCreatesel();
    afx_msg void OnBrowse2(); 
    afx_msg void OnItemchangedListex(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnSetColor();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  public:
    afx_msg void OnBnClickedSetfromlist2();
    CButton BFromList2;
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
  protected:
    bool initialized;
    void MoveItem(int dx,int dy,CWnd &item);
    void SizeItem(int dx,int dy,CWnd &item);
};

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FACEPROP_H__D598A4EB_2442_470B_B7E0_3CCDC636EAA4__INCLUDED_)
