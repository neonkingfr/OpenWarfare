#if !defined(AFX_OPENQDLG_H__C0B95A1B_A074_4412_A20D_EC6AC701A2B8__INCLUDED_)
#define AFX_OPENQDLG_H__C0B95A1B_A074_4412_A20D_EC6AC701A2B8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OpenQDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COpenQDlg dialog

class COpenQDlg : public CDialog
{
  // Construction
  public:
    COpenQDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(COpenQDlg)
    enum  { IDD = IDD_OPENQ };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(COpenQDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(COpenQDlg)
    // NOTE: the ClassWizard will add member functions here
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPENQDLG_H__C0B95A1B_A074_4412_A20D_EC6AC701A2B8__INCLUDED_)
