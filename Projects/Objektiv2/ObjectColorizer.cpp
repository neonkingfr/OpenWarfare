// ObjectColorizer.cpp: implementation of the CObjectColorizer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "ObjectColorizer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


#define frand() ((float)rand()/(float)RAND_MAX)

CObjectColorizer::CObjectColorizer()
  {
  for (int i=1;i<COC_COLORCOUNT;i++)
    {
    HVECTOR v1;
    CopyVektor(v1,mxVector3(frand(),frand(),frand()+0.0001f));	
    NormalizeVector(v1);
    float rgb[3];
    memcpy(rgb,v1,sizeof(float)*3);
    int ir,ig,ib;
    ir=(int)(rgb[0]*255.0);
    ig=(int)(rgb[1]*255.0);
    ib=(int)(rgb[2]*255.0);
    pens[i].CreatePen(PS_SOLID,1,RGB(ir/2,ig/2,ib/2));
    col[i]=RGB((ir+255)>>1,(ig+255)>>1,(ib+255)>>1);
    }
  pens[0].CreatePen(PS_SOLID,1,RGB(0,0,0));
  col[0]=RGB(192,192,192);
  facecols=NULL;
  faces=0;
  }

//--------------------------------------------------

CObjectColorizer::~CObjectColorizer()
  {
  delete [] facecols;
  }

//--------------------------------------------------

CPen & CObjectColorizer::GetObjectColor(int facenum)
  {
  if (facenum>=faces) return pens[0];
  ASSERT(facecols[facenum]<16);
  return pens[facecols[facenum]];
  }

//--------------------------------------------------

COLORREF CObjectColorizer::GetObjectRGB(int facenum)
  {
  if (facenum>=faces) return col[0];
  return col[facecols[facenum]];
  }

//--------------------------------------------------

void CObjectColorizer::ColorizeObject(ObjectData *obj)
  {
  delete [] facecols;
  facecols=new char[obj->NFaces()];
  faces=obj->NFaces();
  Selection sel(obj);
  int idx=0;
  sel.Clear();
  while (ColorizeOneObject(obj,&sel,idx)) 
    idx=(idx+1)&15;
  }

//--------------------------------------------------

bool CObjectColorizer::ColorizeOneObject(ObjectData *obj, Selection *sel, int index)
  {
  int cnt=obj->NPoints();
  int fcnt=obj->NFaces();
  int i;
  for (i=0;i<cnt;i++) 
    if (!sel->PointSelected(i)) break;
  if (i==cnt) return false;
  sel->PointSelect(i);
  bool rep;
  do
    {
    rep=false;
    for (int j=0;j<fcnt;j++) if (!sel->FaceSelected(j))
      {
      FaceT fc(obj,j);
      int k;
      for(k=0;k<fc.N();k++) if (sel->PointSelected(fc.GetPoint(k))) break;
      if (k!=fc.N())
        {
        facecols[j]=index;
        sel->FaceSelect(j);
        for(int l=0;l<fc.N();l++) sel->PointSelect(fc.GetPoint(l));
        rep=true;
        }
      }
    }
  while (rep);
  return true;
  }

//--------------------------------------------------

CPen CObjectColorizer::weightPens[COC_COLORCOUNT];
bool CObjectColorizer::inited=false;


CPen * CObjectColorizer::WeightColor(float weight)
  {
  if (!inited)
    {
    for (int i=0;i<COC_WEIGHTCOUNT;i++)
      {
      CPen& pen=weightPens[i];
      int ref=(i*256)/COC_WEIGHTCOUNT;
      pen.CreatePen(PS_SOLID,1,RGB(ref,0,255-ref));
      }
    inited=true;
    }
  int ref=(int)(weight*(COC_WEIGHTCOUNT-1)+0.5);
  if (ref>=COC_WEIGHTCOUNT) ref=COC_WEIGHTCOUNT;
  return weightPens+ref;
  }

//--------------------------------------------------

//DEL bool CObjectColorizer::IsColorized(ObjectData *obj)
//DEL   {
//DEL   for (int i=0;i<obj->NFaces();i++) 
//DEL 	{
//DEL 	FaceT &fc=obj->Face(i);
//DEL 	if (fc.flags & FACE_COLORIZE_MASK) return true;
//DEL 	}
//DEL   return false;
//DEL   }

//DEL void CObjectColorizer::UnColorize(ObjectData *obj)
//DEL   {
//DEL   for (int i=0;i<obj->NFaces();i++) 
//DEL 	{
//DEL 	FaceT &fc=obj->Face(i);
//DEL 	fc.flags &=~FACE_COLORIZE_MASK;
//DEL 	}
//DEL   }

void CObjectColorizer::Uncolorize()
  {
  delete [] facecols;
  facecols=0;
  faces=0;
  }

//--------------------------------------------------

bool CObjectColorizer::Validate(ObjectData *obj)
  {
  if (obj->NFaces()!=faces) 
    {ColorizeObject(obj);return true;}
  return false;
  }

//--------------------------------------------------

