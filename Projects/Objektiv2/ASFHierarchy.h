// ASFHierarchy.h: interface for the CASFHierarchy class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ASFHIERARCHY_H__D6F309CA_5718_4387_A1A9_5BA5C31615A7__INCLUDED_)
#define AFX_ASFHIERARCHY_H__D6F309CA_5718_4387_A1A9_5BA5C31615A7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



struct SASFHierarchyItem  
  {
  int parent;
  CString name;
  HVECTOR direction;
  char order[3];
  char dof[10];
  float length;
  HVECTOR axis;
  HMATRIX CB; //C*B matrix
  HMATRIX Cinv; //C inverted matrix
  HMATRIX selfmx; //spocitana matice teto bunky
  HMATRIX globmx;  //matice vuci globalnim souradnicim
  Selection *ns;
  bool process;
  public:
    void DoPrecalculate(bool deg,SASFHierarchyItem *parent,float mscale);
  };

//--------------------------------------------------

struct SASFInfo
  {
  float mass;
  float length;
  bool deg;
  bool localaxis;
  CString documentation;
  char axis[3];
  enum torderenum 
    {tx, ty, tz, rx, ry, rz};
  torderenum torder[6];
  HVECTOR position;
  HVECTOR orientation;
  float version;
  CString name;
  };

//--------------------------------------------------

struct SAMCInfo
  {
  CString fname;
  CString path;
  float fps;
  CString smpte;
  int samplecount;
  bool fullspec;
  };

//--------------------------------------------------

#define MAXBONES 300

class CASFHierarchy  
  {
  SASFHierarchyItem items[MAXBONES];
  int count;
  SASFInfo info;
  SAMCInfo amc;
  HMATRIX osy;
  
  
  float mscale;
  
  bool ASFHierarchy(istream &in, char *buffer, int size);
  bool ASFRoot(istream &in, char *buffer, int size);
  bool ASFBoneData(istream &in, char *buffer, int size);
  
  public:
    bool ImportToModel(ObjectData &obj, const char *acm, CString &err);
    int ReadAMCFrame(istream &in);
    bool OpenAMCFile(istream &in, CString &error);
    void RecalculateStatic();
    bool ReadASF(const char *filename,CString &errortext);
    int FindHierarchy(const char *name);
    CASFHierarchy();
    virtual ~CASFHierarchy();
    
    void SetMasterScale(float scl) 
      {mscale=scl;}
    
    int GetBoneCount() 
      {return count;}
    SASFHierarchyItem &GetBone(int i) 
      {return items[i];}
    
    bool ReadASF(istream &in);
    
    
  };

//--------------------------------------------------

#endif // !defined(AFX_ASFHIERARCHY_H__D6F309CA_5718_4387_A1A9_5BA5C31615A7__INCLUDED_)
