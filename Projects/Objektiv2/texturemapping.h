// TextureMapping.h: interface for the CTextureMapping class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXTUREMAPPING_H__C949AFCB_6EA7_46EE_B73D_6C31EC8FCC03__INCLUDED_)
#define AFX_TEXTUREMAPPING_H__C949AFCB_6EA7_46EE_B73D_6C31EC8FCC03__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define CTMCOM_NONE 0
#define CTMCOM_SCALE 1
#define CTMCOM_ROTATE 2

#include "SPoint.h"

class CTextureMapping  
{
  SPoint2DF pts[4];
  SPoint2DF undo[4];
  int tu[4];
  int tv[4];  
  TUNIPICTURE *pic;
  int format; //0 - unknown, 3 - 24BIT RGB, 4 - 32BITRGBA
  CPoint ref;
  int modifycom;
  int modifyparm;
  public:
    void KeyScale(int x, int y);
    void KeyMove(int x, int y);
    void Undo();
    void SaveUndo();
    CPoint GetCenter();
    void Rotate(float dec, CPoint &center);
    void Scale(int pt, CPoint &nw);
    void CorrectActpect();
    void RotateR();
    void RotateL();
    void MirrorY();
    void MirrorX();
    void MoveToPoint(CPoint pt);
    bool TestPointInside(CPoint &pt);
    void SetTextureName(const char *filename,const char *shortname);
    void CheckPoints(int moveidx);    
    void BackgroundFromTex(HVECTOR v1, float tu1, float tv1, HVECTOR v2, float tu2, float tv2, HVECTOR v3, float tu3, float tv3);
    char *textureName;
    bool GetTextureMapping(HVECTOR cpt, float& tu, float &tv);
    HMATRIX points3d;
    HMATRIX undo3d;
    int state;
    bool error;
    bool gray;
    bool hidden;
    bool transp100;
    void MarkRefPoint(CPoint pt) 
    {ref=pt;}
    void SetIndex(int idx, float x, float y);
    int GetIndex(CPoint &pt);
    SPoint2DF& GetPos(int idx) 
    {return pts[idx];}
    void DrawBouding(CDC *pDC);
    void DrawTexture(CDC *dc);
    void DetectTextureFormat();
    void DoCommand();
    int GetTexSizeX() 
    {return cmGetXSize(pic);}
    int GetTexSizeY() 
    {return cmGetYSize(pic);}
    const unsigned char * GetRGB(int x, int y);
    CTextureMapping();
    virtual ~CTextureMapping();
    void SetRect(LPRECT lprc)
    {
      pts[0].x()=lprc->left;pts[0].y()=lprc->top;
      pts[1].x()=lprc->right;pts[1].y()=lprc->top;
      pts[2].x()=lprc->right;pts[2].y()=lprc->bottom;
      pts[3].x()=lprc->left;pts[3].y()=lprc->bottom;
      SetTuTv(0,0,0);
      SetTuTv(1,0.999999f,0);
      SetTuTv(2,0.999999f,0.999999f);
      SetTuTv(3,0,0.999999f);
    }
    void SetTuTv(int pt, float u, float v)
    {
      if (pic)
      {
        tu[pt]=(int)(cmGetXSize(pic)*u+0.5f);
        tv[pt]=(int)(cmGetYSize(pic)*v+0.5f);
      }
    }
	void GetTuTv(int pt, float *u, float *v)
	{
		if (pic) {
			*u = (float)tu[pt]/(float)cmGetXSize(pic);
			*v = (float)tv[pt]/(float)cmGetYSize(pic);
		} else {
			*u = (float)tu[pt];
			*v = (float)tv[pt];
		}
	}
    void WrapXY(int& x, int& y)
    {
      x=x%cmGetXSize(pic);
      y=y%cmGetYSize(pic);
      if (x<0) x+=cmGetXSize(pic);
      if (y<0) y+=cmGetYSize(pic);
    }
    TUNIPICTURE *SetTexture(TUNIPICTURE *pc)
    {
      TUNIPICTURE *pp;
      pp=pic;
      pic=pc;
      if (pic) DetectTextureFormat();
      SetTuTv(0,0,0);
      SetTuTv(1,0.999999f,0);
      SetTuTv(2,0.999999f,0.999999f);
      SetTuTv(3,0,0.999999f);
      return pp;
    }
    
    bool forcefast;
    
  private:
    void RecalculateByCommand(CPoint *pts);
    void CalcRotate(CPoint &out, CPoint& center, int rotate);
    void CalcScale(CPoint &out, int pt, CPoint &nw);
};

#endif // !defined(AFX_TEXTUREMAPPING_H__C949AFCB_6EA7_46EE_B73D_6C31EC8FCC03__INCLUDED_)
