#if !defined(AFX_SPACEWARPDLG_H__BF2D98A6_57EF_47A0_A83E_78EB6CD50BDE__INCLUDED_)
#define AFX_SPACEWARPDLG_H__BF2D98A6_57EF_47A0_A83E_78EB6CD50BDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SpaceWarpDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSpaceWarpDlg dialog

class CSpaceWarpDlg : public CDialog
  {
  // Construction
  public:
    int Run(ObjectData *obj);
    CSpaceWarpDlg * Clone();
    CSpaceWarpDlg(CWnd* pParent = NULL);   // standard constructor
    ObjectData *odata;
    // Dialog Data
    //{{AFX_DATA(CSpaceWarpDlg)
    enum 
      { IDD = IDD_SPACEWARP };
    CButton	Funct;
    CComboBox	SelList;
    CSpinButtonCtrl	Spin3;
    CSpinButtonCtrl	Spin2;
    CSpinButtonCtrl	Spin1;
    BOOL	SaveToSel;
    float	Distance;
    UINT	End;
    UINT	Begin;
    //}}AFX_DATA
    
    CString selsel;
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CSpaceWarpDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    float CalculateWeight(float dist, bool norm);
    
    // Generated message map functions
    //{{AFX_MSG(CSpaceWarpDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg void OnDeltaposSpin1(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnDeltaposSpin2(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnDeltaposSpin3(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnChangeEdit();
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPACEWARPDLG_H__BF2D98A6_57EF_47A0_A83E_78EB6CD50BDE__INCLUDED_)
