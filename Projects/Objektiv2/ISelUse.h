#pragma once

class CIDeleteSelection: public CICommand
  {
  protected:
    WString name;
  public:
    CIDeleteSelection(const char *n): name(n) 
      {}
    virtual int Execute(LODObject *obj)
      {
      ObjectData *odata=obj->Active();
      return !odata->DeleteNamedSel(name);
      }
    virtual void Save(ostream &str)
      {
      FSaveString(str,name);
      }
    virtual int Tag() const 
      {return CITAG_DELETESELECTION;}
    
  };


class CIRenameNamedSel: public CICommand
  {
  protected:
    WString oldname;
    WString text;
  public:	
    CIRenameNamedSel(const char *old, const char *txt): text(txt),oldname(old) 
      {}
    virtual int Execute(LODObject *obj)
      {
      ObjectData *odata=obj->Active();
      const NamedSelection *namsel=odata->GetNamedSel(oldname);
      const NamedSelection *newsel=odata->GetNamedSel(text);
      if (namsel!=newsel && newsel!=NULL)
        {
        AfxMessageBox(WString(IDS_NAMEDSELEXISTS,(LPCTSTR)text),MB_OK);
        return -1;
        }
      return !odata->RenameNamedSel(oldname,text);	  
      }
    virtual void Save(ostream &str) 
      {FSaveString(str,oldname);FSaveString(str,text);}
    virtual int Tag() const 
      {return CITAG_RENAMENAMEDSEL;}
  };




class CISelUse: public CICommand
  {
  WString name;
  SelMode mode;
  public:
    CISelUse(istream &str)
      {
      char text[256];
      datard(str,mode);
      FLoadString(str,text,sizeof(text));
      }
    CISelUse(const char *m, SelMode selmode=SelSet): name(m),mode(selmode) 
      {marksave=false;}
    virtual int Execute(LODObject *lod)
      {
      ObjectData *odata=lod->Active();
      return !odata->UseNamedSel(name,mode);
      }
    virtual void Save(ostream &str)
      {
      datawr(str,mode);
      FSaveString(str,name);
      }
    virtual bool CanRemove(const CICommand *prev) 
      {
      int tg=prev->Tag();
      return mode==SelSet && (			 
        tg==CITAG_SELECT || 
          tg==CITAG_SELECTBYTEXTURE || 
            tg==CITAG_SELUSE);
      
      }
    virtual int Tag() const 
      {return CITAG_SELUSE;}
  };

//--------------------------------------------------

CICommand *CISelUse_Create(istream& str);

class CISetProxy: public CIRenameNamedSel
  {
  public:	
    CISetProxy(const char *old, const char *txt):
    CIRenameNamedSel(old,txt)
      {}
    virtual int Execute(LODObject *obj)
      {
      ObjectData *odata=obj->Active();
      const Selection *sel=odata->GetNamedSel(oldname);
      for (int i=0;i<odata->NPoints();i++) if (sel->PointSelected(i))
        {
        PosT& pos=odata->Point(i);
        pos.flags|=POINT_SPECIAL_HIDDEN;
        }
      return CIRenameNamedSel::Execute(obj);
      }
    
  };
