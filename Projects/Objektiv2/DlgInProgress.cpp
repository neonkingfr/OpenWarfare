// DlgInProgress.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgInProgress.h"
#include ".\dlginprogress.h"


// DlgInProgress dialog

IMPLEMENT_DYNAMIC(DlgInProgress, CDialog)
DlgInProgress::DlgInProgress(CWnd* pParent /*=NULL*/)
	: CDialog(DlgInProgress::IDD, pParent)
{
  _progressVal=0;
  _stopped=false;
  _canstop=false;
  _noprogressmode=false;
  _estLastValue=0;
  _estLastTime=0;
  _estEnd=0;
  _estCnt=0;
}

DlgInProgress::~DlgInProgress()
{
}

void DlgInProgress::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_PROGRESS, wProgress);
  DDX_Control(pDX, IDC_STATICTEXT, wText);
}


BEGIN_MESSAGE_MAP(DlgInProgress, CDialog)
  ON_WM_DRAWITEM()
  ON_BN_CLICKED(IDC_STOP, OnBnClickedStop)
  ON_WM_TIMER()
END_MESSAGE_MAP()


// DlgInProgress message handlers

static COLORREF ColorBlend(COLORREF col1, COLORREF col2, unsigned char factor)
{
    int r1 = GetRValue(col1);
    int g1 = GetGValue(col1);
    int b1 = GetBValue(col1);
    int r2 = GetRValue(col2);
    int g2 = GetGValue(col2);
    int b2 = GetBValue(col2);

    return RGB(
        r1 + (r2 - r1) * factor /255,
        g1 + (g2 - g1) * factor /255,
        b1 + (b2 - b1) * factor /255
        );
}

void DlgInProgress::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
  CDC dc;dc.Attach(lpDrawItemStruct->hDC);
  CRect rc=lpDrawItemStruct->rcItem;
  dc.Draw3dRect(&rc,GetSysColor(COLOR_3DSHADOW),GetSysColor(COLOR_3DHILIGHT));
  rc += CRect(-2,-2,-2,-2);
  CSize sz = rc.Size();
  int step = 8;
  int items = (sz.cx) / step;

  COLORREF col1,  col2;
  float val;


  if (_noprogressmode)
  {
      col1 = GetSysColor(COLOR_BTNFACE);
      col2 = GetSysColor(COLOR_HIGHLIGHT);
      val = 1.0f;
  }
  else
  {
     col1 = GetSysColor(COLOR_HIGHLIGHT);
     col2 = GetSysColor(COLOR_WINDOW);
     val = _progressVal;
  }

  int pos = toInt(sz.cx * val);
  int cur = pos / step;
  int par = pos % step;
  if (par > step - 3) par = step - 3;

  int total = sz.cx / step;
  float effpos = (float)GetTickCount() / 5000.0f * total;

  for (int i = cur ; i >= 0; i--) 
  {
      float spos = sin((effpos + i)/(float)total * 3.14159265 * 2);
      spos *= spos ;
      spos *= spos ;
      spos *= spos ;
      unsigned char factor = toInt(spos * 255);
      dc.FillSolidRect(i*step+rc.left,rc.top,par,sz.cy,
                            ColorBlend(col1,col2,factor));
      par = step - 3;      
  }

  dc.FillSolidRect(pos + rc.left,rc.top,sz.cx - pos, sz.cy, GetSysColor(COLOR_BTNFACE));


  dc.Detach();
}

void DlgInProgress::SetProgress(float val)
{
  if (fabs(_lastpvupdate-val)>0.01)
    {
      GetDlgItem(IDC_PROGRESS)->Invalidate(FALSE);
      _lastpvupdate=val;
    }
  _progressVal=val;
  _noprogressmode=false;
}

void DlgInProgress::SetDescription(const char *text)
{
  wText.SetWindowText(text);
}

bool DlgInProgress::IsStopped()
{
  if (!_canstop) 
  {
    _canstop=true;
    GetDlgItem(IDC_STOP)->EnableWindow(TRUE);
    return false;
  }
  return _stopped;
}
void DlgInProgress::OnBnClickedStop()
{
  _stopped=true;
    GetDlgItem(IDC_STOP)->EnableWindow(FALSE);

}

BOOL DlgInProgress::OnInitDialog()
{
  CDialog::OnInitDialog();

  SetTimer(1,100,0);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgInProgress::OnTimer(UINT nIDEvent)
{
  EstimateTime(GetMessageTime());
  GetDlgItem(IDC_PROGRESS)->Invalidate(FALSE);

}


void DlgInProgress::WaitForNextUpdate()
{
  MSG msg;
  while (PeekMessage(&msg,0,0,0,PM_REMOVE))
  {
    if (!IsDialogMessage(&msg))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
  }
  WaitMessage();
}

void DlgInProgress::OnOK()
{
}

void DlgInProgress::OnCancel()
{
  OnBnClickedStop();
}

void DlgInProgress::EstimateTime(DWORD timeNow)
{
  if (_noprogressmode) return;
  DWORD val=toInt(_progressVal*100);
  if (val<1) return;
  if (val>99) return;
  if (val<_estLastValue) 
  {
    _estLastValue=val;
    _estEnd=0;
    _estCnt=0;
     return;
  }
  if (val==_estLastValue) return;
  if (_estLastTime==0) {_estLastTime=timeNow;return;}
  DWORD timeElps=(timeNow-_estLastTime);
  DWORD speed=timeElps/(val-_estLastValue);
  DWORD end=speed*(100-val)+timeNow;
  _estCnt++;
  if (_estCnt>5) 
  {
    _estCnt=5;
    _estEnd=(_estEnd-_estEnd/_estCnt);
  }
  _estEnd+=end;
  end=_estEnd/_estCnt;
  int remain=end-timeNow;
  if (remain<0) remain=0;

  remain/=1000;
  CString text;
  if (remain>60)
    text.Format(IDS_PROGRESSREMAIN,(remain+30)/60);
  else
    text.Format(IDS_PROGRESSREMAINSEC,remain);			  
  SetDlgItemText(IDC_ODHAD,text);  
  _estLastValue=val;
  _estLastTime=timeNow;
}