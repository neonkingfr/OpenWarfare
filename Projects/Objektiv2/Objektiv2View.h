// Objektiv2View.h : interface of the CObjektiv2View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_OBJEKTIV2VIEW_H__6F94A43C_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_OBJEKTIV2VIEW_H__6F94A43C_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#include "TexMapList.h"	// Added by ClassView
#include "TextureMapping.h"	// Added by ClassView
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <g3dpp\g3dpp.h>
#include "GuideLines.h"
#include "PointAttacher.h"


#ifdef WITH_D3D
#include "d3drenderer/d3drenderer.h"
#endif

#define CMS_MAPX 1
#define CMS_MAPY 2
#define CMS_MAPZ 4

#define TEXLOADMAX 5


struct AttachPoint2DInfo
{
  int _dcX;
  int _dcY;
  float _Z;
  VecT vc;

  int Distance2(const AttachPoint2DInfo &other) {return (_dcX-other._dcX)*(_dcX-other._dcX)+(_dcY-other._dcY)*(_dcY-other._dcY);}
  AttachPoint2DInfo(int x, int y, float z, const VecT &vc):_dcX(x),_dcY(y),_Z(z),vc(vc) {};
  AttachPoint2DInfo() {}

  int Compare(const AttachPoint2DInfo &other) const
  {
    if (_dcX==other._dcY) 
    {
      if (_dcY>other._dcY) return 1;
      if (_dcY<other._dcY) return -1;
      return 0;
    }
    else
    {
      if (_dcX>other._dcX) return 1;
      return -1;      
    }
  }

  bool operator==(const AttachPoint2DInfo &other) const {return Compare(other)==0;}
  bool operator>=(const AttachPoint2DInfo &other) const {return Compare(other)>=0;}
  bool operator<=(const AttachPoint2DInfo &other) const {return Compare(other)<=0;}
  bool operator!=(const AttachPoint2DInfo &other) const {return Compare(other)!=0;}
  bool operator>(const AttachPoint2DInfo &other) const {return Compare(other)>0;}
  bool operator<(const AttachPoint2DInfo &other) const {return Compare(other)<0;}
  void operator=(int x) {_dcX=x;}
};

class CObjektiv2View : public CView
  {
  protected: // create from serialization only
    CObjektiv2View();
    DECLARE_DYNCREATE(CObjektiv2View)
      static CObjektiv2View *curview;
    
    // Attributes
  public:
    static CObjektiv2View *lastcursview;
    CObjektiv2Doc* GetDocument();
    CPointAttacher ptattach;
    CGCamera2 camera;
    float fov,rotz,rotx,distance,scale;
    HVECTOR pos;
    HVECTOR help; //helper pos
    CPoint curpos;
    bool relmode;
    bool facecull;
    CBitmap *drawbmp;
    CBitmap *backbmp;
    bool redraw;
    bool grid;
    bool solid;
    bool solidwires;
    CRect lastrect;
    bool projection,rotlock;
    int LockMode;
    int UnlockMode;
    CGuideLines glines;
    bool recalc_texview;
    CString framename;
    CTextureMapping *texview;	  
    CPoint contextmenupt;
    HWND extattach;
    VecT attpoint;
    bool attvalid;

#ifdef WITH_D3D
	static d3drenderer d3dr;	
#endif

	bool used3d;	// Use new direct3D renderer for this viewport
	bool d3dtextured;	// Textured instead of solid view

    BTree<AttachPoint2DInfo> _attPointCache;
    
    // Operations
  public:
    static CObjektiv2View *GetCurrentView() 
      {return curview;}
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CObjektiv2View)
  public:
    virtual void OnDraw(CDC* pDC);  // overridden to draw this view
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
  protected:
    virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
    virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
    virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
    virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
    void EndPaintVertices();
    void DoPaintVertices(CPoint &point);
    int AttachPoint(CPoint &ms);
    void DrawBackground(CDC *pDC);
    CTexMapList list;
    void CalcNetMatrix(HMATRIX mm);
    void CalcMirror(bool y, HMATRIX mm, bool pin);
    void MakeZoomRect(CRect &rc);
    void TexContextMenu();
    void CalcScaleOnPlane(float f, HMATRIX mm, bool pin,bool nogrid=false);
    void CalcNonuniformScaleOnPlane(float x,float y, HMATRIX mm, bool pin, bool nogrid=false);
    void CalcRotationOnPlane(float rad,HMATRIX mm,bool pin, bool nogrid=false);
    void MapMouseMovement(CPoint &pt1, CPoint &pt2, HMATRIX mm, bool nolock);
    void CancelPolygonialSelection();
    LPHVECTOR MousePtTo3D(CPoint& pt, LPHVECTOR ref, LPHVECTOR norm=NULL);
    LPHVECTOR MousePtTo3D(SPoint2DF& pt, LPHVECTOR ref,  LPHVECTOR norm=NULL);
    void MapMouse(CPoint& p, int iz, float& x, float& y, float& z);
    void MapMouse2(CPoint& p, float& x, float& y, float& z);
    void Invalidate();
    void InvalidateNoClearAttachDB();
    void DrawFocusFrame(bool focused, CDC* dc=NULL);
    void OnDrawToBitmap(CDC *pDC);
	void DrawToBitmapD3D(CBitmap *drawbmp, DWORD width, DWORD height);
    void SetView(UINT type) 
      {OnView(type);}
    virtual ~CObjektiv2View();
    #ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
    #endif

    void CalcRotationOnLocalAxis(float angle, HMATRIX mm);
    void CalcScaleOnLocalAxis(float f, HMATRIX mm, bool pin);
    void CalcMoveOnLocalAxis(float f, HMATRIX mm, bool pin);

  protected:
    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CObjektiv2View)
    afx_msg void OnSetFocus(CWnd* pOldWnd);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnView(UINT com);
    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg BOOL OnLockXYZ(UINT id);
    afx_msg void OnUpdateLockXYZ(CCmdUI* pCmdUI);
    afx_msg BOOL OnSetLockxyz(UINT id);
    afx_msg void OnLockRotation();
    afx_msg void OnUpdateLockRotation(CCmdUI* pCmdUI);
    afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnViewPincamer();
    afx_msg void OnViewFacecull();
    afx_msg void OnUpdateViewFacecull(CCmdUI* pCmdUI);
    afx_msg void OnViewGrid();
    afx_msg void OnUpdateViewGrid(CCmdUI* pCmdUI);
    afx_msg void OnUseD3D();
    afx_msg void OnUpdateUseD3D(CCmdUI* pCmdUI);
    afx_msg void OnD3DTextured();
    afx_msg void OnUpdateD3DTextured(CCmdUI* pCmdUI);
    afx_msg void OnViewSolidfill();
    afx_msg void OnUpdateViewSolidfill(CCmdUI* pCmdUI);
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg void OnKillFocus(CWnd* pNewWnd);
    afx_msg void OnInsertPoint();
    afx_msg BOOL OnCreatePrim(UINT com);
    afx_msg void OnSurfacesBackgroundmapping();
    afx_msg void OnSurfacesBgrfromface();
    afx_msg void OnSurfacesBgrfromsel();
    afx_msg void OnTexTransp100();
    afx_msg void OnTexMirrorX();
    afx_msg void OnTexMirrorY();
    afx_msg void OnTexRotatel();
    afx_msg void OnTexRotater();
    afx_msg void OnTexAspec();
    afx_msg void OnTexUnload();
    afx_msg BOOL OnViewPagezoom(UINT cmd);
    afx_msg void OnUpdateViewPagezoomsel(CCmdUI* pCmdUI);
    afx_msg BOOL OnViewZoomin(UINT cmd);
    afx_msg void OnTexUndo();
    afx_msg void OnTexGray();
    afx_msg void OnTexLoad();
    afx_msg void OnTexHide();
    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
    afx_msg void OnViewLookatface();
    afx_msg void OnTexAdd();
    afx_msg void OnTexSelect();
    afx_msg void OnViewExternalaschild();
    afx_msg void OnMove(int x, int y);
    afx_msg void OnEditLineardeform();
    afx_msg void OnPointsBevel();
	afx_msg void OnSurfacesSetautomapselection();
	afx_msg void OnUpdateSurfacesSetautomapselection(CCmdUI* pCmdUI);
    afx_msg LRESULT ReupdateView(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnViewWiresinsolid();
	afx_msg void OnUpdateViewWiresinsolid(CCmdUI* pCmdUI);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
public:
  void TurnEdgeSupp(CPoint & cursor);
  afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
  bool FindAttachPoint(const CPoint &ms, const LPHMATRIX camera, VecT &found);
  VecT CopyVectWithLock(const VecT &vect, const VecT &origin);
};










#ifndef _DEBUG  // debug version in Objektiv2View.cpp
inline CObjektiv2Doc* CObjektiv2View::GetDocument()
  { return (CObjektiv2Doc*)m_pDocument; }










#endif

CString GetTextureFrom(int idc, const char *OldTexName);

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OBJEKTIV2VIEW_H__6F94A43C_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)

