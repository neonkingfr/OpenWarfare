// MultPinCullDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "MultPinCullDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMultPinCullDlg dialog


CMultPinCullDlg::CMultPinCullDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CMultPinCullDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CMultPinCullDlg)
    selection = _T("");
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CMultPinCullDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CMultPinCullDlg)
  DDX_Control(pDX, IDC_SELLIST, List);
  DDX_CBString(pDX, IDC_SELLIST, selection);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CMultPinCullDlg, CDialog)
  //{{AFX_MSG_MAP(CMultPinCullDlg)
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CMultPinCullDlg message handlers
    
    BOOL CMultPinCullDlg::OnInitDialog() 
      {
      CDialog::OnInitDialog();
      
      int i;
      CString q;
      
      List.ResetContent();
      for (i=0;i<MAX_NAMED_SEL;i++)
        {
        if (obj->GetNamedSel(i)!=NULL)
          {
          List.AddString(obj->GetNamedSel(i)->Name());
          if (obj->GetNamedSel(i)->NFaces()==0) q=obj->GetNamedSel(i)->Name();
          }
        }
      List.SelectString(-1,q);	
      return TRUE;  // return TRUE unless you set the focus to a control
      // EXCEPTION: OCX Property Pages should return FALSE
      }

//--------------------------------------------------

