// EditPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "EditPropDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditPropDlg dialog


CEditPropDlg::CEditPropDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CEditPropDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CEditPropDlg)
    m_Name = _T("");
    m_Value = _T("");
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CEditPropDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CEditPropDlg)
  DDX_Control(pDX, IDOK, m_Ok);
  DDX_Text(pDX, IDC_NAME, m_Name);
  DDX_Text(pDX, IDC_VALUE, m_Value);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CEditPropDlg, CDialog)
  //{{AFX_MSG_MAP(CEditPropDlg)
  ON_EN_CHANGE(IDC_NAME, OnChangeName)
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CEditPropDlg message handlers
      
      void CEditPropDlg::OnChangeName() 
        {
        m_Ok.EnableWindow(GetDlgItem(IDC_NAME)->GetWindowTextLength()>0);
        }

//--------------------------------------------------

