// IExtrude.h: interface for the CIExtrude class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IEXTRUDE_H__56B3FD3E_D4D9_4824_98F2_3C82BF9DFAD2__INCLUDED_)
#define AFX_IEXTRUDE_H__56B3FD3E_D4D9_4824_98F2_3C82BF9DFAD2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CIExtrude : public CICommand  
  {
  int segments;
  public:
    virtual int Execute(LODObject *obj);
    CIExtrude(int sgm=1) 
      {segments=sgm;}
    CIExtrude(istream& str) 
      {datard(str,segments);}
    virtual ~CIExtrude() 
      {}
    virtual int Tag() 
      {return CITAG_EXTRUDE;}
    virtual void Save(ostream &str) 
      {datawr(str,segments);}
  };

//--------------------------------------------------

class CIShape: public CIExtrude
  {
  public:
    virtual int Execute(LODObject *obj);
    CIShape(int sgm=1):CIExtrude(sgm) 
      {}
    CIShape(istream& str):CIExtrude(str) 
      {}
    virtual int Tag() const 
      {return CITAG_SHAPE;}
    
  };

//--------------------------------------------------

#endif // !defined(AFX_IEXTRUDE_H__56B3FD3E_D4D9_4824_98F2_3C82BF9DFAD2__INCLUDED_)
