// MSNavigator.h: interface for the CMSNavigator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MSNAVIGATOR_H__79169A66_FA38_407F_B0E9_5DE9DC7CDFFF__INCLUDED_)
#define AFX_MSNAVIGATOR_H__79169A66_FA38_407F_B0E9_5DE9DC7CDFFF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Objektiv2View.h"

class CObjektiv2View;

class CMSNavigator  
  {
  CObjektiv2View &view;
  public:
    CMSNavigator(CObjektiv2View& vw): view(vw) 
      {};
    virtual void MsAction(CPoint &mspt, 
    virtual ~CMSNavigator();
  };

//--------------------------------------------------

#endif // !defined(AFX_MSNAVIGATOR_H__79169A66_FA38_407F_B0E9_5DE9DC7CDFFF__INCLUDED_)
