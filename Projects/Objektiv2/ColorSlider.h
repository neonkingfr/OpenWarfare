#if !defined(AFX_COLORSLIDER_H__E9CA7FA1_B348_4172_BEDA_788BAB39023C__INCLUDED_)
#define AFX_COLORSLIDER_H__E9CA7FA1_B348_4172_BEDA_788BAB39023C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorSlider.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CColorSlider window

class CColorSlider : public CWnd
  {
  // Construction
  float value;
  public:
    CColorSlider();
    
	__declspec(property(get=GetValue, put=SetValue)) float val;

    void SetValue(float val) 
      {value=val;Invalidate();}
    float GetValue() 
      {return value;}
    // Attributes
  public:
    
    // Operations
  public:
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CColorSlider)
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
    virtual ~CColorSlider();
    
    // Generated message map functions
  protected:
    void DrawPos(CDC &dc,int pos,int size);
    //{{AFX_MSG(CColorSlider)
    afx_msg void OnPaint();
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORSLIDER_H__E9CA7FA1_B348_4172_BEDA_788BAB39023C__INCLUDED_)
