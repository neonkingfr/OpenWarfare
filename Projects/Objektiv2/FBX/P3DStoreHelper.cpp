#include "StdAfx.h"
#include "P3DStoreHelper.h"
#include <projects/objektivlib/BiTXTImportDefault.h>

using namespace ObjektivLib;

bool P3DStoreHelper::Save(const Pathname &target)
{
    return p3d.Save(target,OBJDATA_LATESTVERSION,false,0,0) >= 0;
}


void P3DStoreHelper::WalkNode(KFbxNode* pNode, 
                              KTime& lTime, 
                              KFbxXMatrix& pParentGlobalPosition,
                              KFbxXMatrix& pGlobalPosition) {

    const char *name = pNode->GetName();
    if (name && name[0])
    {
      char * tempName = new char[strlen(name)+1];
      strcpy(tempName,name);
      if (tempName[0]=='_') tempName[0]='-';
      lastName = tempName;
      delete tempName;
    }
    const char *substr = strstr(lastName,"_LOD");
    if (substr != 0) {

        const char *lodname = substr + 4;
        CBiTXTImportDefault importHelper;
        float resol = importHelper.LODNameToResol(lodname);
        int idx = p3d.FindLevelExact(resol);
        if (idx == -1)
            p3d.SelectLevel(p3d.AddLevel(ObjectData(),resol));
        else
            p3d.SelectLevel(idx);
        lastName = lastName.Mid(0,substr - lastName.Data());
    }

    

    return SceneWalker::WalkNode(pNode,lTime,pParentGlobalPosition,pGlobalPosition);

}


void P3DStoreHelper::CleanupP3D() {

    for (int i = 0; i<p3d.NLevels() && p3d.NLevels()>1 ;i++) if (p3d.Level(i)->NPoints()==0) {
        p3d.DeleteLevel(i);
        --i;            
    }

}