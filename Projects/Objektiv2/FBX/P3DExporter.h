#pragma once

struct SubObjMap;

class UVEnumer;

class P3DExporter {

public:
    P3DExporter(FbxSdk::MManager &manager,
                FbxSdk::MScene &scene,
                const RVMATExplorer &rvmatExplorer,
                const char *vxcachePath,
                 IFBXExportControl *control);


    bool exportAllLods(const LODObject &p3d);
    bool exportLevel(const ObjectData &level);
    bool exportSelected(const ObjectData &level, const Selection &sel);
    bool exportObject(const ObjectData &level, const Selection &subObj);


protected:
        FbxSdk::MManager &manager;
        FbxSdk::MScene &scene;
        const RVMATExplorer &rvmatExplorer;
        const char *vxcachePath;
        IFBXExportControl *control;
        const ObjectData *curMesh;
        float curLodNum;


        class TextMatGroup: public std::pair<RStringI,RStringI>  {
            typedef std::pair<RStringI,RStringI>  Super;
        public:
            TextMatGroup(RStringI texture, RStringI material):Super(texture,material) {}
            TextMatGroup() {};
            RStringI getTexture() const {return first;}
            RStringI getMaterial() const {return second;}
        };

        typedef std::map<TextMatGroup,KFbxSurfaceMaterial *> MaterialMap;

        MaterialMap materialMap;

        typedef std::map<KFbxSurfaceMaterial *, int> MaterialToIndex;




protected:

    RString createObjectName(const ObjectData &level, const SubObjMap &sub);
    void createMesh(const ObjectData &level, const SubObjMap &sub, 
                    FbxSdk::FbxStdObject<KFbxMesh> &mesh);
    void createUVLayers(const ObjectData &level, const SubObjMap &sub, 
                    FbxSdk::FbxStdObject<KFbxMesh> &mesh,
                    FbxSdk::FbxStdObject<KFbxNode> &meshNode);
    friend class UVEnumer;

    KFbxSurfaceMaterial *createMaterial(const TextMatGroup &group,const char *materialType);
    void CreateDummyObject(FbxSdk::FbxStdObject<KFbxCluster> &cluster,
                       const char *objectName,
                       Vector3 position);
    void createCacheFile(const ObjectData &level, const SubObjMap &sobj, 
        FbxSdk::FbxStdObject<KFbxMesh> &mesh, RString objname) ;

};
