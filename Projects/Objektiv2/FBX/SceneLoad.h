#pragma once


namespace FbxSdk
{
	class SceneLoad
	{
	protected:

		int lFileMajor, lFileMinor, lFileRevision;	
		int lSDKMajor,  lSDKMinor,  lSDKRevision;
		int lFileFormat;

	public:

		bool LoadScene(MManager& pSdkManager, 
			MScene &pScene, 
			const char* pFilename);

		virtual void ImportError(MImporter &importer, const char* pFilename) {}
		virtual void InitializeError(MImporter &importer, const char* pFilename) {}
		virtual const char *AskPassword(char *buffer, std::size_t sz) {return 0;}
		virtual void ExtraFbxProcessing(KFbxImporter &importer) {}
	};
}