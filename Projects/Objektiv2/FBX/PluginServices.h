#pragma once
#include "ifbxpluginservices.h"

namespace ObjektivLib {
    class LODObject;
}


class PluginServices :
    public IFBXPluginServices
{
public:

    virtual void ImportFBX(const char *fbxfilename, IFBXImportControl *control);
    virtual void ExportFBX(const char *fbxfilename, IFBXExportControl *control);


protected:


};
