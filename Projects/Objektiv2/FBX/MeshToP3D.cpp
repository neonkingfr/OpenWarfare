#include "StdAfx.h"
#include "MeshToP3D.h"
#include <es/containers/array.hpp>




void MeshToP3D::Mesh(KFbxXMatrix& pGlobalPosition, KFbxMesh* pMesh,KFbxVector4* pVertexArray) 
{
    int vertexCount = pMesh->GetControlPointsCount();
    ObjectData object;

    object.ReservePoints(vertexCount);
    for (int i = 0; i< vertexCount ;i++)
    {
        KFbxVector4 transformed = pGlobalPosition.MultT(pVertexArray[i]);
        PosT pos;
        pos.SetPoint(Vector3(transformed[0],transformed[1],transformed[2]));
        pos.flags = 0;
        object.Point(i) = pos;
    }


    int polygons = pMesh->GetPolygonCount();
    AutoArray<int> indices;
    AllPolyInfo polygonInfo;
    PolygonInfo pol;   
    for (int i = 0; i< polygons; i++)
    {
        int ns = pMesh->GetPolygonSize(i);
        indices.Clear();
        indices.Resize(ns);
        for (int j = 0; j< ns; j++)             
            indices[ns-j-1] = (pMesh->GetPolygonVertex(i,j));

        pol.Resize(ns*3);
        Array<ObjToolTopology::STessellateInfo> nfo(pol.Data(),pol.Size());
        object.GetTool<ObjToolTopology>().TessellatePolygon(ns,indices.Data(),0,0,0,&nfo);
        polygonInfo.Add(nfo);
    }

    ScanWeights(pMesh,object);

    ReadUVs(pMesh,object,polygonInfo);

    const char *meshName = lastName;
    if (meshName[0]==0) meshName = 0;

    p3d.Active() -> Merge(object,meshName,true);
}


void MeshToP3D::ScanWeights(KFbxMesh* pMesh, ObjectData &object)
{

    int deformers = pMesh->GetDeformerCount(KFbxDeformer::eSKIN);
    for (int d = 0; d < deformers; d++) {
            
    	KFbxSkin* curDeformer = ((KFbxSkin*)pMesh->GetDeformer(0, KFbxDeformer::eSKIN));
        KFbxCluster::ELinkMode clusterMode = curDeformer->GetCluster(0)->GetLinkMode();
        int clusterCount = curDeformer->GetClusterCount();


        AutoArray<SRef<NamedSelection> > newSelections;
    
        for (int j = 0; j < clusterCount; j++) {

			KFbxCluster* lCluster =curDeformer->GetCluster(j);
			if (!lCluster->GetLink())
				continue;

            RString clusterName = lCluster->GetLink()->GetName();
            if (clusterName.GetLength() == 0)  {
                sprintf_s(clusterName.CreateBuffer(20),20,"%d",j);
            }

            
            SRef<NamedSelection> sel = new NamedSelection(&object,clusterName);            

 			int lVertexIndexCount = lCluster->GetControlPointIndicesCount();

			for (int k = 0; k < lVertexIndexCount; ++k) 
            {
				int lIndex = lCluster->GetControlPointIndices()[k];
				double lWeight = lCluster->GetControlPointWeights()[k];
                sel->SetPointWeight(lIndex,(float)lWeight);
            }

            newSelections.Add(sel);
        }

        if (clusterMode == KFbxCluster::eNORMALIZE) {

            for (int i = 0; i < object.NPoints(); i++) {

                float sum = 0;
                for (int j = 0; j < newSelections.Size(); j++) 
                    sum = sum + newSelections[j]->PointWeight(i);
                sum = 1/sum;
                if (_finite(sum)) {
                    for (int j = 0; j < newSelections.Size(); j++) {
                        newSelections[j]->SetPointWeight(i,
                            newSelections[j]->PointWeight(i) * sum);

                    }
                }
            }
        }

        for (int j = 0; j < newSelections.Size(); j++) {
            const Selection *sel = object.GetNamedSel(newSelections[j]->Name());
            if (sel) (*(newSelections[j])) += *sel;
            object.SaveNamedSel(newSelections[j]->Name(),newSelections[j].GetRef());        
        }
    }

}

void MeshToP3D::ReadUVs(KFbxMesh* pMesh,ObjectData &object, const AllPolyInfo &pointInfo) {

    KFbxLayerElement::ELayerElementType textures[] ={
        KFbxLayerElement::eDIFFUSE_TEXTURES,
        KFbxLayerElement::eEMISSIVE_TEXTURES,
        KFbxLayerElement::eEMISSIVE_FACTOR_TEXTURES,
        KFbxLayerElement::eAMBIENT_TEXTURES,
        KFbxLayerElement::eAMBIENT_FACTOR_TEXTURES,
        KFbxLayerElement::eDIFFUSE_FACTOR_TEXTURES,
        KFbxLayerElement::eSPECULAR_TEXTURES,
        KFbxLayerElement::eNORMALMAP_TEXTURES,
        KFbxLayerElement::eSPECULAR_FACTOR_TEXTURES,
        KFbxLayerElement::eSHININESS_TEXTURES,
        KFbxLayerElement::eBUMP_TEXTURES,
        KFbxLayerElement::eTRANSPARENT_TEXTURES,
        KFbxLayerElement::eTRANSPARENCY_FACTOR_TEXTURES,
        KFbxLayerElement::eREFLECTION_TEXTURES,
        KFbxLayerElement::eREFLECTION_FACTOR_TEXTURES,
    };
        
    for (int i = 0, uvsetidx = 0, cnt = pMesh->GetLayerCount(); i < cnt; i++) {
        KFbxLayer *layer = pMesh->GetLayer(i);
        if (layer) {

            for (int j = 0; j < lenof(textures); j++, uvsetidx++)
            {
                KFbxLayerElementUV *layerUv =  layer->GetUVs(textures[j]);
                if (layerUv != 0) {

                    KFbxLayerElement::EMappingMode mapMode = layerUv->GetMappingMode();
                    KArrayTemplate<KFbxVector2> &uvs = layerUv->GetDirectArray();
                    KArrayTemplate<int> &uvs_ids = layerUv->GetIndexArray();
                    KFbxLayerElement::EReferenceMode refMode = layerUv->GetReferenceMode();
                
                    if (mapMode != KFbxLayerElement::eNONE 
                        && mapMode != KFbxLayerElement::eALL_SAME 
                        && mapMode != KFbxLayerElement::eBY_EDGE
                        && mapMode != KFbxLayerElement::eBY_POLYGON) //empty or not supported
                    {                        
                        object.AddUVSet(uvsetidx);
                        object.SetActiveUVSet(uvsetidx);

                        int ns = 0;

                        for (int k = 0, offset = 0; k < pointInfo.Size(); 
                                k++, offset += ns) {

                            ns = pMesh->GetPolygonSize(k);
                            const PolygonInfo &polInfo = pointInfo[k];

                            for (int l = 0; l < polInfo.Size(); l++)
                            {
                                const ObjToolTopology::STessellateInfo &tesInfo = polInfo[l];
                                int uvsrc;
                                switch (mapMode) {
                                    
                                    case KFbxLayerElement::eBY_CONTROL_POINT:
                                        uvsrc = pMesh->GetPolygonVertex(k,ns - tesInfo.index - 1);
                                        break;
                                    case KFbxLayerElement::eBY_POLYGON_VERTEX:
                                        uvsrc = offset + (ns - tesInfo.index - 1);
                                        break;
                                }
                                
                                int idx = refMode==KFbxLayerElement::eDIRECT? uvsrc: uvs_ids[uvsrc];
                                KFbxVector2 uvinfo = uvs[ idx ];
                                

                                FaceT fc(object,tesInfo.face);
                                fc.SetUV(tesInfo.vx,uvinfo[0],uvinfo[1]);
//                                printf("%5d %5d %5d %5d %8.4f %8.4f\n",k,l,uvsrc, idx, uvinfo[0], uvinfo[1]);

                            }
                        }
                    }

                }
            }
        }                                                               
    }
}
