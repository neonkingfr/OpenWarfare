#pragma once
#include "scenewalker.h"

class P3DStoreHelper : public SceneWalker
{
protected:
    RString lastName;
    ObjektivLib::LODObject p3d;

    virtual void WalkNode(KFbxNode* pNode, 
              KTime& lTime, 
              KFbxXMatrix& pParentGlobalPosition,
              KFbxXMatrix& pGlobalPosition);

public:
    bool Save(const Pathname &target);

    const ObjektivLib::LODObject &GetP3D() {return p3d;}


    void CleanupP3D();

};
