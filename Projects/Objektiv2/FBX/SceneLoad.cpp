#include "StdAfx.h"
#include "SceneLoad.h"

namespace FbxSdk
{

	bool SceneLoad::LoadScene(MManager& manager, 
		MScene &scene, 
		const char* pFilename)
	{
		manager->GetIOSettings()->SetBoolProp(IMP_FBX_MATERIAL,        true);
		manager->GetIOSettings()->SetBoolProp(IMP_FBX_TEXTURE,         true);
		manager->GetIOSettings()->SetBoolProp(IMP_FBX_LINK,            true);
		manager->GetIOSettings()->SetBoolProp(IMP_FBX_SHAPE,           true);
		manager->GetIOSettings()->SetBoolProp(IMP_FBX_GOBO,            true);
		manager->GetIOSettings()->SetBoolProp(IMP_FBX_ANIMATION,       true);
		manager->GetIOSettings()->SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, true);

		// Create an importer.
		MImporter lImporter(manager);

		// Get the file version number generate by the FBX SDK.
		KFbxSdkManager::GetFileFormatVersion(lSDKMajor, lSDKMinor, lSDKRevision);

		if (manager->GetIOPluginRegistry()->DetectReaderFileFormat(pFilename, lFileFormat))
		{
			// Unrecognizable file format. Try to fall back to native format.
			lFileFormat = manager->GetIOPluginRegistry()->GetNativeReaderFormat();
		}

		// Initialize the importer by providing a filename.
		const bool lImportStatus = lImporter->Initialize(pFilename,lFileFormat);
		lImporter->GetFileVersion(lFileMajor, lFileMinor, lFileRevision);

		if( !lImportStatus )
		{
			ImportError(lImporter,pFilename);
			return false;
		}

		if (lImporter->IsFBX())
		{
			ExtraFbxProcessing(*lImporter);
		}

		// Import the scene.
		bool lStatus = lImporter->Import(scene);

		int retries = 3;

		while (lStatus == false 
			&& lImporter->GetLastErrorID() == KFbxIO::ePASSWORD_ERROR 
			&& retries > 0)
		{
			retries--;

			char buff[1024];
			const char *passwordSet = AskPassword(buff,lenof(buff));
			if (passwordSet == 0) 
				break;

			manager->GetIOSettings()->SetStringProp(IMP_FBX_PASSWORD,KString(buff));
			manager->GetIOSettings()->SetBoolProp(IMP_FBX_PASSWORD_ENABLE,true);

			lStatus = lImporter->Import(scene);
		}

		if (lStatus == false)
			ImportError(lImporter,pFilename);


		return lStatus;
	}
}