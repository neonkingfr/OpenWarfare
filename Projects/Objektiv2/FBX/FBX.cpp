// FBX.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "fbx.h"
#include "resource.h"
#include "PluginServices.h"
#include "FBXImportDlg.h"
#include <el/ProgressBar/ProgressBar.h>
#include "../FileDialogEx.h"
#include "FBXExportDlg.h"
#define FBXDLL_EXPORTS
#include "FBXDll.h"
#include <sstream>
#include <strstream>

// Chad's fix - own instance of app is required, otherwise we receive nasty assert in debug (MFC calls method of NULL pointer),  maybe something wrong in release
CWinApp fluff;

FBXPlugin theApp;

void FBXPlugin::Init(IPluginGeneralAppSide *appside) {

    this->appside = appside;

}



void FBXPlugin::AfterOpen(HWND O2Window) {
    
    appside->AddMenuItem(appside->mnuImport,"FBX",new FBXImportCommand());
    appside->AddMenuItem(appside->mnuExport,"FBX",new FBXExportCommand());
    this->o2Window = O2Window;
    ProgressBarFunctions::SelectGlobalProgressBarHandler(appside->GetProgressBarHandler());

    CFileDialogEx::SetHistoryInterface(appside->GetFileDialogExHandler());


}
void FBXPlugin::AfterClose() {

}

static PluginServices GPluginServices;


void FBXPlugin::FBXImportCommand::OnCommand() {
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    
    CFileDialogEx fdlg(TRUE,
        CString(MAKEINTRESOURCE(IDS_FBXDEFAULT)),0,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,
        CString(MAKEINTRESOURCE(IDS_FBXFILTER)),CWnd::FromHandle(theApp.o2Window));

    CString title(MAKEINTRESOURCE(IDS_FBXIMPORTTITLE));
    fdlg.m_ofn.lpstrTitle = title;

    int res = fdlg.DoModal();
    if (res == IDOK) {
        IFBXImportUI *ui = CreateImportDialog();
        ui->Open(theApp.o2Window,fdlg.GetPathName(),theApp.appside->GetProjectRoot(),false);
        theApp.appside->BeginUpdate(CString(MAKEINTRESOURCE(IDS_FBXIMPORT)));
        theApp.appside->UpdateActiveObject(ui->GetImportedP3D());
        theApp.appside->EndUpdate();
        delete ui;
    }    
}

void FBXPlugin::FBXExportCommand::OnCommand() {

    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    
    CFileDialogEx fdlg(FALSE,
        CString(MAKEINTRESOURCE(IDS_FBXDEFAULT)),0,OFN_PATHMUSTEXIST|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,
        CString(MAKEINTRESOURCE(IDS_FBXFILTER)),CWnd::FromHandle(theApp.o2Window));

    CString title(MAKEINTRESOURCE(IDS_FBXEXPORTTITLE));
    fdlg.m_ofn.lpstrTitle = title;

    int res = fdlg.DoModal();
    if (res == IDOK) {
        const LODObject *p3d = theApp.appside->GetActiveObject();
        IFBXExportUI *ui = CreateExportDialog(*p3d);
        ui->Open(theApp.o2Window, fdlg.GetPathName(), theApp.appside->GetProjectRoot(),false);
        delete ui;
    }

}

// Parses string to array of flags
class FBXflagParse {
protected:
	std::vector<std::string> flags;
	bool getFlags(char *strue, char *sfalse, bool def) const {
		bool r = def;
		for(unsigned int i=0;i<flags.size();i++) {
			if(!stricmp(strue, flags[i].c_str()))
				r = true;
			if(!stricmp(sfalse, flags[i].c_str()))
				r = false;
		}
		return r;
	}

	bool isFlagSet(char *flag) const {
		for(unsigned int i=0;i<flags.size();i++)
			if(!stricmp(flag, flags[i].c_str()))
				return true;
		return false;
	}

	void parseFlags(const char *str) {
		char *tok;
		tok = strtok((char*)str, " ,.");
		while(tok) {
			std::string str = tok;
			flags.push_back(str);
			tok = strtok(NULL, " ,.-");
		}
	};
};

// GUIless export control - export settings from string of flags
class FBXExportGUIless : public IFBXExportControl, FBXflagParse {
public:
	FBXExportGUIless(LODObject p3d, const char *newflags, float newscale, int newfps) {
		scale = newscale;
		animfps = newfps;
		subject = p3d;
		error = false;
		parseFlags(newflags);
	};

	~FBXExportGUIless() {
		flags.clear();
	};

	bool getError() {return error;};

private:
	ObjektivLib::LODObject subject;
	float scale;
	int animfps;
	
	bool error;
	virtual void ExporterFailed(const char *filename, const char *errorText) {
		error = true;
		RptF("FBX Export error %s", errorText);
	}

private:

	bool ExportTextures() const {return getFlags("textures", "noTextures", true);};
	bool ASCIIFormat() const {return getFlags("formatASCII", "formatBinary", false);};
	bool GetSkinSkipOrdSel() const {return getFlags("skinSkipOrdinary", "noSkinSkipOrdinary", false);};
	const char * GetProjectRoot() const {return "";};
	bool ExtractRVMats() const {return getFlags("extractRVMAT", "noExtractRVMAT", true);};
	float GetMasterScale() const {return scale;};
	const ObjektivLib::LODObject &GetSubject() const {return subject;};
	bool ExportNormals() const {return getFlags("exportNormals", "noExportNormals", true);};
	bool ExportEdges() const {return getFlags("exportEdges", "noExportEdges", true);};
	bool SplitEnabled() const {return getFlags("splitObjects", "noSplitObjects", true);};
	bool DetectObjectNames() const {return getFlags("reconstructNames", "noReconstructNames", true);};
	MeshExportMode GetCurMeshExportMode() const {
		if(isFlagSet("allLods")) return mAllLods;
		if(isFlagSet("currentLod")) return mCurLod;
		if(isFlagSet("currentSelection")) return mSel;
		return mAllLods;
	};
	SkinExportMode GetSkinningMode() const {
		if(isFlagSet("noSkinning")) return sNoSkinning;
		if(isFlagSet("liteSkinning")) return sLiteSkinning;
		if(isFlagSet("fullSkinning")) return sFullSkinning;
		return sNoSkinning;
	};
	bool ExportRVMatRef() const {return getFlags("extractRVMATref", "noExtractRVMATref", true);};
	int GetAssignedLayer(int uvSetId) const {return 0;};
	unsigned int ExportAnimationFPS() const {return animfps;};	
};

// GUIless import control
class FBXEimportGUIless : public IFBXImportControl, FBXflagParse {
public:
	FBXEimportGUIless(const char *newflags, float newscale, int newanimFPS) {
		error=false;
		scale=newscale;
		animFPS=newanimFPS;
		parseFlags(newflags);

		mask = 0;
		if(!isFlagSet("noMesh")) mask += importMesh;
		if(!isFlagSet("noTextures")) mask += importTextures;
		if(!isFlagSet("noUVSets")) mask += importUVSets;
		if(!isFlagSet("noSmoothing")) mask += importSmooths;
		if(!isFlagSet("noSkinning")) mask += importSkinning;
		if(!isFlagSet("noLODs")) mask += importLODs;
		if(!isFlagSet("noRVMATRefs")) mask += importRVMATRefs;
		if(!isFlagSet("noSkeleton")) mask += importSkeleton;
		if(!isFlagSet("noBonePivots")) mask += importBonePivots;
		if(!isFlagSet("noAnimation")) mask += importAnimation;
	};
	~FBXEimportGUIless() {flags.clear();};
	ObjektivLib::LODObject getResult() {return resultp3d;};
	bool getError() {return error;};
	int getErrorCode() {return errorCode;};
private:
	int mask;
	void ImportError(int code,const char *popis, const char* pFilename) {error=true;errorCode=code;}
	int errorCode;
	float scale;
	int animFPS;
	bool error;
	LODObject resultp3d;
	bool DontCreateObjectSelections() {return isFlagSet("DontCreateObjSel");};
	bool StoreResult(const ObjektivLib::LODObject &object) {
		resultp3d = object;
		return true;
	}
    int GetAnimationSpeed() {return animFPS;}
    float GetMasterScale() {return scale;}
	//int GetImportMask() {return importMesh|importUVSets|importSmooths;};
	int GetImportMask() {return mask;};
};


// Exports for O2script interface (no GUI)
extern "C" {
	FBXDLL_DECL bool __cdecl exportLODobjectToFBX(LODObject *p3d, const char* filename, const char *flags, float scale, int animFPS) {
		FBXExportGUIless *xprt = new FBXExportGUIless(*p3d, flags, scale, animFPS);
        PluginServices services;            
        GPluginServices.ExportFBX(filename, xprt);

		bool err = xprt->getError();
		delete xprt;
		return !err;
	}
	
	FBXDLL_DECL bool __cdecl importFBXtoLODobject(LODObject *p3d, const char* filename, const char *flags, float scale, int animFPS) {
		FBXEimportGUIless *imp = new FBXEimportGUIless(flags, scale, animFPS);
		PluginServices services; 
		services.ImportFBX(filename, imp);
		if(!imp->getError()) {
			*p3d = imp->getResult();
			delete imp;
			return true;
		} else {
			RptF("FBX Import error %d", imp->getErrorCode());
			delete imp;
			return false;
		}
	}
}


// Export init function
extern "C" {
    FBXDLL_DECL IPluginGeneralPluginSide * __cdecl  ObjektivPluginMain(IPluginGeneralAppSide *appside)
    {
      theApp.Init(appside);
      return &theApp;
    }

     FBXDLL_DECL IFBXPluginServices * __cdecl  GetPluginServices()
    {
        return &GPluginServices;
    }

    FBXDLL_DECL LODObject *LoadP3D(const void *data, size_t sz) {

        std::istrstream strm((const char *)data, sz);

        LODObject *lodobj = new LODObject();
        if (lodobj->Load(strm,0,0)) {
            delete lodobj;
            return 0;
        }
        return lodobj;
    }

    FBXDLL_DECL void FreeP3D(LODObject *p3d) {

        delete p3d;

    }

    FBXDLL_DECL const char *GetPluginVersionString() {
        return PACKAGE_VERSION VERSION_EXTRA;
    }

    FBXDLL_DECL IFBXImportUI * __cdecl CreateImportDialog() {

        class Import: public CFBXImportDlg, public IFBXImportUI {
            bool skip;
            CString objname;
            std::string data;

        public:
            virtual bool Open(HWND parentWnd, 
                              const char *filename,
                              const char *projectRoot,
                              bool skipImport) {
                
                AFX_MANAGE_STATE(AfxGetStaticModuleState());
                skip = skipImport;
                if (GetSafeHwnd()) DestroyWindow();
                Create(IDR_FBXIMPORT,CWnd::FromHandle(parentWnd));        
                ShowWindow(SW_SHOW);
                SetBasePath(projectRoot);
                ::SetCursor(::LoadCursor(0,IDC_WAIT));
                GPluginServices.ImportFBX(filename,static_cast<CFBXImportDlg *>(this));
                int importresult = GetImportResult();
                if (importresult == IDOK) {
                    ShowMessages();                    
                }
                return importresult == IDOK;
            }

            virtual bool OpenConfigurationDialog() {
                AFX_MANAGE_STATE(AfxGetStaticModuleState());
                bool res = CFBXImportDlg::OpenConfigurationDialog();
                if (skip) return false;
                return res;
            }
            
            const char *GetObjectName(unsigned long objId) {
                AFX_MANAGE_STATE(AfxGetStaticModuleState());
                TreeMap::const_iterator iter = treeMap.find(objId);
                if (iter == treeMap.end()) return 0;                
                objname=wObjectTree.GetItemText(iter->second);
                return objname.GetString();
            }

            const LODObject &GetImportedP3D() {
                AFX_MANAGE_STATE(AfxGetStaticModuleState());
                return CFBXImportDlg::GetImportedP3D();
            }

            const void *GetImportedP3D(size_t &sz) {
                AFX_MANAGE_STATE(AfxGetStaticModuleState());
                std::ostringstream buff;
                GetImportedP3D().Save(buff,0,0);
                data = buff.str();
                sz = data.size();
                return data.data();
            }

            virtual IFBXImportControl *GetControl() {return this;}


/*            using IFBXImportUI::operator new;
            using IFBXImportUI::operator delete;*/

        };

        return new Import();


    }

    FBXDLL_DECL IFBXExportUI * __cdecl CreateExportDialog(const LODObject &p3d) {

        class Export: public FBXExportDlg, public IFBXExportUI {

        public:
            Export(const LODObject &p3d):FBXExportDlg(p3d) {}

            virtual bool Open(HWND parentWnd, 
                              const char *filename, 
                              const char *projectRoot, 
                              bool skipExport) {


                AFX_MANAGE_STATE(AfxGetStaticModuleState());
                SetProjectRoot(projectRoot);
                Create(IDD_FBXEXPORT,CWnd::FromHandle(parentWnd));
                while (GetSafeHwnd()) {
                    if (!AfxPumpMessage()) break;
                    if (OkClicked()) {
                        if (skipExport) return true;
                        PluginServices services;
                        ::SetCursor(::LoadCursor(0,IDC_WAIT));                
                        GPluginServices.ExportFBX(filename,static_cast<FBXExportDlg *>(this));
                        ShowMessages();
                        return true;
                    }
                }
                return false;

            }

            virtual IFBXExportControl *GetControl() {return this;}

/*            using IFBXExportUI::operator new;
            using IFBXExportUI::operator delete;*/

        };

        return new Export(p3d);

    }


}

