#pragma once

class SceneWalker
{
public:
    ~SceneWalker(void) {}

    /// Walks the scene at a given time for the current take.
    virtual void WalkScene(KFbxScene* pScene, KTime& pTime);

protected:
    
    /// Walks recursively each node of the scene. 
    /** To avoid recomputing 
     * uselessly the global positions, the global position of each 
     * node is passed to it's children while browsing the node tree.
     */
    virtual void WalkNodeRecursive(KFbxNode* pNode, 
                           KTime& pTime, 
                           KFbxXMatrix& pParentGlobalPosition);
    virtual void WalkNode(KFbxNode* pNode, 
                  KTime& lTime, 
                  KFbxXMatrix& pParentGlobalPosition,
                  KFbxXMatrix& pGlobalPosition);
    virtual void WalkSkeleton(KFbxNode* pNode, 
                      KFbxXMatrix& pParentGlobalPosition, 
                      KFbxXMatrix& pGlobalPosition);
    virtual void WalkMesh(KFbxNode* pNode,
                  KTime& pTime,
                  KFbxXMatrix& pGlobalPosition);
    virtual void ComputeShapeDeformation(KFbxNode* pNode,
                                 KFbxMesh* pMesh, 
                                 KTime& pTime, 
                                 KFbxVector4* pVertexArray);
    virtual void ComputeClusterDeformation(KFbxXMatrix& pGlobalPosition, 
                                KFbxMesh* pMesh, 
                                KTime& pTime, 
                                KFbxVector4* pVertexArray);
    virtual void ReadVertexCacheData(KFbxMesh* pMesh, 
						     KTime& pTime, 
						     KFbxVector4* pVertexArray);
    virtual void WalkCamera(KFbxNode* pNode, 
                    KTime& pTime, 
                    KFbxXMatrix& pGlobalPosition);
    virtual void WalkLight(KFbxNode* pNode, 
                   KTime& pTime, 
                   KFbxXMatrix& pGlobalPosition);
    
    virtual void LimbNode(KFbxNode *node, KFbxXMatrix& pGlobalBasePosition, KFbxXMatrix& pGlobalEndPosition) {}
    virtual void WalkMarker(KFbxXMatrix& pGlobalPosition) {}
    virtual void Mesh(KFbxXMatrix& pGlobalPosition, KFbxMesh* pMesh, 
                 KFbxVector4* pVertexArray) {}
    virtual void Camera(KFbxXMatrix& pGlobalPosition, double pRoll) {}
    virtual void Light(KFbxXMatrix& pGlobalPosition, KFbxLight* pLight, 
                KFbxColor& pColor, double pConeAngle) {}

};
