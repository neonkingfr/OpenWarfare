#include "stdafx.h"

namespace FBXTextureTypes {
    
    const char *texturesMaterial[] = {
        KFbxSurfaceMaterial::sDiffuse,
        KFbxSurfaceMaterial::sEmissive,
        KFbxSurfaceMaterial::sAmbient,
        KFbxSurfaceMaterial::sSpecular,
        KFbxSurfaceMaterial::sNormalMap,
        KFbxSurfaceMaterial::sBump,
        KFbxSurfaceMaterial::sShininess,
        KFbxSurfaceMaterial::sReflection,
        KFbxSurfaceMaterial::sEmissiveFactor,
        KFbxSurfaceMaterial::sAmbientFactor,
        KFbxSurfaceMaterial::sDiffuseFactor,
        KFbxSurfaceMaterial::sSpecularFactor,
        KFbxSurfaceMaterial::sTransparentColor,
        KFbxSurfaceMaterial::sTransparencyFactor,
        KFbxSurfaceMaterial::sReflectionFactor
    };

    const char *getType(int index) {
        return texturesMaterial[index];
    }
    int getCountTypes() {
        return _countof(texturesMaterial);
    }
}