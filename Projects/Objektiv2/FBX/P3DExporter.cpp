#include "StdAfx.h"
#include "PluginServices.h"
#include "SceneLoad.h"
#include "P3DImporter.h"
#include "IFBXPluginServices.h"
#include <sstream>
#include "FBXFileFeaturesDetector.h"
#include <el/ProgressBar/ProgressBar.h>
#include <projects/objektivlib/ObjToolClipboard.h>
#include <projects/objektivlib/BiTXTImportDefault.h>
#include "RVMATExplorer.h"
#include "hlpfuncts.h"
#include "P3DExporter.h"
#include "textureTypes.h"

using namespace FbxSdk;

void SpacesToUnderscores(RString &objname) {
    int i = objname.Find(' ');                                    
    while (i != -1) {
        objname.MutableData()[i] = '_';
        i = objname.Find(' ');                                    
    }
}

P3DExporter::P3DExporter(MManager &manager,
                         MScene &scene,
                         const RVMATExplorer &rvmatExplorer,
                         const char *vxcachePath,
                         IFBXExportControl *control)
                         :manager(manager)
                         ,scene(scene)
                         ,rvmatExplorer(rvmatExplorer)
                         ,vxcachePath(vxcachePath) 
                         ,control(control)
{
}


bool P3DExporter::exportAllLods(const LODObject &p3d) {
    double cntfaces = 0;
    for (int i = 0; i < p3d.NLevels(); i++) 
        cntfaces += p3d.Level(i)->NPoints();

    ProgressBar<double> pb(cntfaces);
    for (int i = 0; i < p3d.NLevels(); i++)  {

        pb.AdvanceNext(p3d.Level(i)->NPoints());
        curLodNum = p3d.Resolution(i);
        if (!exportLevel(*p3d.Level(i))) return false;
        if (pb.StopSignaled()) return false;
    }
    return true;


}
bool P3DExporter::exportSelected(const ObjectData &level, const Selection &sel) {

    ObjectData selOnly = level;
    ObjToolClipboard &tool = selOnly.GetTool<ObjToolClipboard>();
    tool.ExtractSelection(&sel);
    return exportLevel(selOnly);
}

bool P3DExporter::exportLevel(const ObjectData &level) {

    if (!control->SplitEnabled()) {
        Selection worksel(&level);
        worksel.SelectAll();
        return exportObject(level,worksel);
    }
    else
    {
        ProgressBar<int> pb(level.NPoints());
        Selection worksel(&level),forbidsel(&level);
        int start = 0;
        while (start < level.NPoints()) {

            while (start < level.NPoints() && forbidsel.PointSelected(start)) start++;
            if (start < level.NPoints()) {

                pb.SetNextPos(start);
                worksel.PointSelect(start);
                worksel.SelectConnectedFaces();
                if (!exportObject(level,worksel)) 
                    return false;
                forbidsel += worksel;
                worksel.Clear();
                if (pb.StopSignaled())
                    return false;

            }

        }
        return true;
    }
}


struct SubObjMap {
    int *faces;
    int *points;
    int *remap;
    int nfaces;
    int npoints;

    SubObjMap(const Selection &worksel):faces(0),points(0),remap(0),nfaces(0),npoints(0) {
        nfaces = worksel.NFaces();
        npoints = worksel.NPoints();
        const ObjectData &level = *worksel.GetOwner();
        try {
            faces = new int[nfaces];
            points = new int[npoints];
            remap = new int[level.NPoints()];

            for (int i = 0, idx = 0, cnt= level.NPoints(); i < cnt; ++i) {
                if (worksel.PointSelected(i)) {
                    remap[i] = idx;
                    points[idx++] = i;
                }
            }
            for (int i = 0, idx = 0, cnt= level.NFaces(); i < cnt; ++i)
                if (worksel.FaceSelected(i))
                    faces[idx++] = i;

            nfaces = idx;

        } catch (...) {
            delete [] faces;
            delete [] points;
            delete [] remap;
            throw;
        }
    }

    ~SubObjMap() {
        delete [] faces;
        delete [] points;
        delete [] remap;
    }
};

static const char *DetectSelName(const ObjectData &level, const SubObjMap &sub) {

    for (int i = 0; i < MAX_NAMED_SEL; i++) {

        const NamedSelection *sel = level.GetNamedSel(i);
        if (sel) {

            if (sel->NPoints() == sub.npoints && sel->NFaces() == sub.nfaces) {

                for (int i = 0; i < sub.npoints; i++) 
                    if (!sel->PointSelected(sub.points[i])) goto dalsi;

                for (int i = 0; i < sub.nfaces; i++) 
                    if (!sel->FaceSelected(sub.faces[i])) goto dalsi;

                return sel->Name();

            }
dalsi:;

        }

    }
    return 0;

}


RString P3DExporter::createObjectName(const ObjectData &level, const SubObjMap &sub) {
    RString objname;
    if (control->DetectObjectNames()) 
        objname = DetectSelName(level, sub);        


    if (objname.GetLength() == 0) {
        objname = MakeUniqueName("object");
    }

    if (control->GetCurMeshExportMode() == control->mAllLods) {
        CBiTXTImportDefault helper;
        objname = objname + "_LOD" + helper.LODResolToName(curLodNum);   
        SpacesToUnderscores(objname);
    }
    return objname;
}

bool P3DExporter::exportObject(const ObjectData &level, const Selection &subObj) {
    SubObjMap sobj(subObj);

    RString objname = createObjectName(level, sobj);

    FbxStdObject<KFbxMesh> mesh(manager, objname);
    createMesh(level,sobj,mesh);
    FbxStdObject<KFbxNode> meshnode(manager,objname);
    createUVLayers(level,sobj,mesh,meshnode);
    createCacheFile(level,sobj,mesh,objname);
    meshnode->SetNodeAttribute(mesh.Unlink());
    KFbxNode* lRootNode = scene->GetRootNode();
    lRootNode->AddChild(meshnode.Unlink());
    


    return true;
}
void P3DExporter::createMesh(const ObjectData &level, const SubObjMap &sobj, 
                             FbxStdObject<KFbxMesh> &mesh) 
{

    float masterScale = control->GetMasterScale();

    mesh->InitControlPoints(sobj.npoints);
    for (int i = 0; i < sobj.npoints; i++) {        
        const VecT &pt = level.Point(sobj.points[i]);
        mesh->GetControlPoints()[i] = KFbxVector4(
            pt[0] * masterScale,
            pt[1] * masterScale,
            -pt[2] * masterScale);
    }

    CEdges egs(level.NPoints());    
    for (int i = 0; i < sobj.nfaces; i++) {

        FaceT fc(level,sobj.faces[i]);
        mesh->BeginPolygon();        
        for (int j = 0; j < fc.N(); j++) {
            int v2 = fc.GetPoint(j);
            mesh->AddPolygon(sobj.remap[v2]);
        }
        mesh->EndPolygon();
    } 
    mesh->BeginAddMeshEdgeIndex();
    for (int i = 0; i < sobj.nfaces; i++) {

        FaceT fc(level,sobj.faces[i]);
        for (int j = 0, k = fc.N() - 1; j < fc.N(); k = j, j++) {
            int v1 = fc.GetPoint(k);
            int v2 = fc.GetPoint(j);
            if (!egs.IsEdge(v1,v2)) {
                egs.SetEdge(v1,v2);
                mesh->AddMeshEdgeIndex(sobj.remap[v1],sobj.remap[v2],false);
            }
        }
    }
    mesh->EndAddMeshEdgeIndex();

	if(sobj.nfaces == 0) {
		// No faces? GetLayer will return null pointer unless this is done
		mesh->BeginPolygon();  
		mesh->EndPolygon();
	}

    //setup layer
    int layeridx = 0;
    KFbxLayer *layer = mesh->GetLayer(layeridx);

    //export normals
    if (control->ExportNormals() && level.NNormals()>0) {

        KFbxLayerElementNormal *normals = static_cast<KFbxLayerElementNormal *>(
            layer->CreateLayerElementOfType(KFbxLayerElement::eNORMAL,false));
        normals->SetMappingMode(normals->eBY_POLYGON_VERTEX);
        normals->SetReferenceMode(normals->eDIRECT);
        for (int i = 0; i < sobj.nfaces; i++) {
            FaceT fc(level,sobj.faces[i]);
            for (int j = 0; j < fc.N(); j++) {
                VecT n = fc.GetNormalVector<VecT>(j);
                normals->GetDirectArray().Add(KFbxVector4(n[0],n[1],n[2]));
            }
        }
        layer->SetNormals(normals);
    }

    //export edges
    if (control->ExportEdges()) {
        KFbxLayerElementSmoothing *edges = static_cast<KFbxLayerElementSmoothing *>(layer->CreateLayerElementOfType(KFbxLayerElement::eSMOOTHING));
        edges->SetMappingMode(edges->eBY_EDGE);
        edges->SetReferenceMode(edges->eDIRECT);
        CEdges levelEdges(level.NPoints());
        level.LoadSharpEdges(levelEdges);
        int cntedges = mesh->GetMeshEdgeCount();
        for (int i = 0; i < cntedges; i++) {
            int v1,v2;
            mesh->GetMeshEdgeVertices(i,v1,v2);
            bool isthere = levelEdges.IsEdge(sobj.points[v1],sobj.points[v2]);
            edges->GetDirectArray().Add(!isthere);
        }
        layer->SetSmoothing(edges);
    }

        //export skinning
        if (control->GetSkinningMode() != control->sNoSkinning) {
            
            FbxStdObject<KFbxSkin> skin(manager,MakeUniqueName());
            
            for (int i = 0; i < MAX_NAMED_SEL; i++) {
                const NamedSelection *ns = level.GetNamedSel(i);
                if (ns) {
                    if (control->GetSkinSkipOrdSel()) {
                        int j;
                        for (j = 0; j < level.NPoints(); j++) 
                            if (ns->PointWeight(j) > 0 && ns->PointWeight(j)<255.0) break;
                        if (j == level.NPoints())
                            continue;
                    }

                    RString nsname = MakeUniqueName(ns->Name());
                    
                    FbxStdObject<KFbxCluster> cluster(manager, nsname);
                    for (int j = 0; j < sobj.npoints; j++) 
                        if (ns->PointSelected(sobj.points[j]))
                            cluster->AddControlPointIndex(j,ns->PointWeight(sobj.points[j]));

                    cluster->SetLinkMode(cluster->eADDITIVE);                    
                    if (cluster->GetControlPointIndicesCount()) {

                        if (control->GetSkinningMode() == control->sFullSkinning)
                            CreateDummyObject(cluster,ns->Name(),ns->GetCenter());

                        skin->AddCluster(cluster.Unlink());
                    }
                }
            }
            if (skin->GetClusterCount())
                mesh->AddDeformer(skin.Unlink());

        }
}

void P3DExporter::createCacheFile(const ObjectData &level, const SubObjMap &sobj, 
                             FbxStdObject<KFbxMesh> &mesh, RString objname) 
{
    float masterScale = control->GetMasterScale();

    int fps = control->ExportAnimationFPS();
        if (level.NAnimations() && fps) {
            RString channelName("cache_",objname);
            RString filename(objname,".mc");
            KFbxCache *vxcache = KFbxCache::Create(manager.GetRef(),channelName);
            vxcache->SetCacheFileFormat(vxcache->eMC);
            vxcache->SetCacheFileName(filename,RString(vxcachePath,filename));

            if (!vxcache->OpenFileForWrite(KFbxCache::eMC_ONE_FILE, fps, channelName))
                control->UnableToOpenVertexCacheChannel(channelName);
            else {
                    int chanIndex = vxcache->GetChannelIndex(objname);
                    double *buff = new double[sobj.npoints * 3];
                    KTime::ETimeMode tmode = KTime::ConvertFrameRateToTimeMode((double)fps);
                    if (tmode == KTime::eDEFAULT_MODE) tmode = KTime::eFRAMES1000;
                    KTime::SetGlobalTimeMode(tmode);

                    for (int i = 0; i < level.NAnimations(); i++) {
                        AnimationPhase *a = level.GetAnimation(i);
                        for (int j = 0, p = 0; j < sobj.npoints; j++, p+=3) {
                            buff[p] = (*a)[sobj.points[j]][0] * masterScale;
                            buff[p + 1] = (*a)[sobj.points[j]][1] * masterScale;
                            buff[p + 2] = - (*a)[sobj.points[j]][2] * masterScale;
                        }

                        KTime tm;
                        tm.SetSecondDouble((double)i/(double)fps);
                        vxcache->Write(chanIndex,tm,buff,sobj.npoints);

                    }
                    delete [] buff;
                    vxcache->CloseFile();
                }
            KFbxVertexCacheDeformer *cachedef 
                = KFbxVertexCacheDeformer::Create(manager.GetRef(),MakeUniqueName("Deformer"));
            cachedef->SetCache(vxcache);
            cachedef->SetCacheChannel(channelName);
            cachedef->SetActive(true);
            mesh->AddDeformer(cachedef);
        }
}


class UVEnumer 
{
public:
    typedef std::map<int,int> UVSetToLayerMap;

    UVEnumer(P3DExporter& owner, FbxSdk::FbxStdObject<KFbxMesh> &mesh, 
            const SubObjMap &sub, const ObjectData &level)
        :owner(owner),mesh(mesh),sub(sub),level(level)
        ,firstLayer(true)
    {}

    bool operator()(const ObjUVSet *uvset) const;

    const UVSetToLayerMap &getLayerMap() const {return uvsetToLayerMap;}


protected:
    P3DExporter& owner;
    FbxSdk::FbxStdObject<KFbxMesh> &mesh;
    const SubObjMap &sub;
    const ObjectData &level;

    mutable bool firstLayer;
    mutable UVSetToLayerMap uvsetToLayerMap;
};


bool UVEnumer::operator()(const ObjUVSet *uvset) const {


    int index = uvset->GetIndex();
    int layerType = owner.control->GetAssignedLayer(index);
    if (layerType == -1) return false;


    int layerId = firstLayer?0:mesh->CreateLayer();
    firstLayer = false;

    KFbxLayer *layer = mesh->GetLayer(layerId);
    uvsetToLayerMap[index] = layerId;

	char name[32];
	snprintf(name, 32, "UVChannel_%d", index+1);

    FbxStdObject<KFbxLayerElementUV> uvmap = KFbxLayerElementUV::Create(mesh,"");
	uvmap->SetName(name);
    uvmap->SetMappingMode(uvmap->eBY_POLYGON_VERTEX);
    uvmap->SetReferenceMode(uvmap->eINDEX_TO_DIRECT);
    for (int i = 0; i < sub.nfaces; i++) {
        FaceT fc(level, sub.faces[i]);
        for (int j = 0; j < fc.N(); j++) {
            ObjUVSet_Item uv = uvset->GetUV(sub.faces[i],j);
            uvmap->GetIndexArray().Add(uvmap->GetDirectArray().GetCount());
            uvmap->GetDirectArray().Add(KFbxVector2(uv.u,1.0f - uv.v));
        }
    }
    layer->SetUVs(uvmap.Unlink(), KFbxLayerElement::eDIFFUSE_TEXTURES);
    return false;
}



void P3DExporter::createUVLayers(const ObjectData &level, const SubObjMap &sub, 
                                 FbxSdk::FbxStdObject<KFbxMesh> &mesh
                                 ,FbxSdk::FbxStdObject<KFbxNode> &meshNode) 
{    
    UVEnumer uvenum(*this,mesh,sub,level);
    level.EnumUVSets(uvenum);

    MaterialToIndex matToIndex;

    const char *projectRoot = control->GetProjectRoot();
    Pathname absPath;
    absPath.SetDirectory(projectRoot);

    if (control->ExportTextures()) {

        int textureTypeIndex = control->GetAssignedLayer(0);
        if (textureTypeIndex != -1) {
            const char *basicTexType = FBXTextureTypes::getType(textureTypeIndex);

            FbxStdObject<KFbxLayerElementMaterial> matmap = KFbxLayerElementMaterial::Create(mesh,"");
            matmap->SetMappingMode(matmap->eBY_POLYGON);
            matmap->SetReferenceMode(matmap->eINDEX);


            MaterialToIndex matMap;
            for (int i = 0; i < sub.nfaces; i++) {
                FaceT fc(level, sub.faces[i]);
                RString tex = fc.GetTexture();
                if (tex.GetLength() != 0 && tex[0] != '#') {
                    Pathname fullPath(tex,absPath);
					//tex = fullPath.GetFullPath();
                    tex = fullPath.GetFilename();
                }
                RString mat;
                if (control->ExportRVMatRef()) {
                    mat = fc.GetMaterial();
                    if (mat.GetLength() != 0) {
                        Pathname fullPath(mat,absPath);
                        //mat = fullPath.GetFullPath();
                        mat = fullPath.GetFilename();
                    }
                }
                TextMatGroup texInfo(tex,mat);
                MaterialMap::const_iterator fnd = materialMap.find(texInfo);
                KFbxSurfaceMaterial *fbxmat;
                if (fnd == materialMap.end()) {
                    fbxmat = createMaterial(texInfo,basicTexType);
                    materialMap[texInfo] = fbxmat;
                } else {
                    fbxmat = fnd->second;
                } 
                int index;
                if (fbxmat != 0)  {
                    MaterialToIndex::const_iterator fnd2 = matMap.find(fbxmat);
                    if (fnd2 == matMap.end()) {
                        index = meshNode->AddMaterial(fbxmat);
                        matMap[fbxmat] = index;
                    } else {
                        index = fnd2->second;
                    }
                } else {
                    index = -1;
                }
                matmap->GetIndexArray().Add(index);
            }
            KFbxLayer *layer = mesh->GetLayer(0);
            layer->SetMaterials(matmap.Unlink());
        }

        if (control->ExtractRVMats())
            for (UVEnumer::UVSetToLayerMap::const_iterator iter = uvenum.getLayerMap().begin();
            iter != uvenum.getLayerMap().end(); ++iter) 
        {
            int layerid = iter->second;
            int uvindex = iter->first;
            if (uvindex == 0) continue;            
            int textureTypeIndex = control->GetAssignedLayer(uvindex);
            if (textureTypeIndex == -1) continue;

            const char *basicTexType = FBXTextureTypes::getType(textureTypeIndex);

            FbxStdObject<KFbxLayerElementMaterial> matmap = KFbxLayerElementMaterial::Create(mesh,"");
            matmap->SetMappingMode(matmap->eBY_POLYGON);
            matmap->SetReferenceMode(matmap->eINDEX_TO_DIRECT);
            MaterialToIndex matMap;
            for (int i = 0; i < sub.nfaces; i++) {
                FaceT fc(level, sub.faces[i]);
                RString mat = fc.GetMaterial();
                int index = -1;
                if (mat.GetLength() != 0) {
                    Pathname fullPath(mat,absPath);                    
                    const char *texName = this->rvmatExplorer.GetTextureOfMaterial(
                                                    fullPath.GetFullPath(),uvindex);
                    if (texName) {                        
                        Pathname fullPath(texName,absPath);
                        TextMatGroup texInfo(fullPath.GetFullPath(),RStringI());
                        MaterialMap::const_iterator fnd = materialMap.find(texInfo);
                        KFbxSurfaceMaterial *fbxmat;
                        if (fnd == materialMap.end()) {
                            fbxmat = createMaterial(texInfo,basicTexType);
                            materialMap[texInfo] = fbxmat;
                        } else {
                            fbxmat = fnd->second;
                        }                         
                        if (fbxmat != 0)  {
                            MaterialToIndex::const_iterator fnd2 = matMap.find(fbxmat);
                            if (fnd2 == matMap.end()) {
                                index = meshNode->AddMaterial(fbxmat);
                                matMap[fbxmat] = index;
                            } else {
                                index = fnd2->second;
                            }
                        }
                    }
                }
                matmap->GetIndexArray().Add(index);
            }
            KFbxLayer *layer = mesh->GetLayer(layerid);
            layer->SetMaterials(matmap.Unlink());
        }
    }

}

KFbxSurfaceMaterial *P3DExporter::createMaterial(const TextMatGroup &group,const char *materialType) {
    
	char uvname[32];
	snprintf(uvname, 32, "UVChannel_%d", 1);	// Where to get the index? Why is this needed in the texture definition??

    RString name(MakeUniqueName(Pathname::GetNameFromPath(group.getTexture())));
    SpacesToUnderscores(name);
    FbxStdObject<KFbxSurfacePhong> newMat(manager,name);
    FbxStdObject<KFbxTexture> newTex(manager,name);
    //newTex->SetFileName(group.getTexture());
	newTex->SetRelativeFileName(group.getTexture());
	newTex->UVSet = uvname;
    if (group.getMaterial())
        newTex->SetMediaName(group.getMaterial());
    KFbxProperty lProperty = newMat->FindProperty(materialType);
    lProperty.ConnectSrcObject(newTex.Unlink());
    return newMat.Unlink();

}


void P3DExporter::CreateDummyObject(FbxStdObject<KFbxCluster> &cluster,
                       const char *objectName,
                       Vector3 position) 
{

    KFbxNode *xx = 0;
    KFbxNode *nd = (KFbxNode *)scene->FindRootMember(xx, const_cast<char *>(objectName));
    if (nd != 0)
        cluster->SetLink(nd);
    else 
    {
        FbxStdObject<KFbxSkeleton> skeletonObj(manager,objectName);
        skeletonObj->SetSkeletonType(skeletonObj->eEFFECTOR);
        FbxStdObject<KFbxNode> node(manager,objectName);
        node->SetNodeAttribute(skeletonObj.Unlink());
        node->LclTranslation.Set(KFbxVector4(position[0],position[1],position[2]));
        cluster->SetLink(node);
        scene->GetRootNode()->AddChild(node.Unlink());
    }
}
