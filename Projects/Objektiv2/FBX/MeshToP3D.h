#pragma once
#include <projects/objektivlib/ObjToolTopology.h>
#include "P3DStoreHelper.h"

class MeshToP3D :    public P3DStoreHelper 
{


public:

    virtual void Mesh(KFbxXMatrix& pGlobalPosition, KFbxMesh* pMesh, 
                 KFbxVector4* pVertexArray);


protected:

    typedef AutoArray<ObjToolTopology::STessellateInfo> PolygonInfo;
    class AllPolyInfo : public AutoArray<PolygonInfo> 
    {
    public:
        ClassIsMovable(AllPolyInfo);
    };

    void ScanWeights(KFbxMesh* pMesh, ObjectData &object);
    void ReadUVs(KFbxMesh* pMesh,ObjectData &object, const AllPolyInfo &pointInfo);
};
