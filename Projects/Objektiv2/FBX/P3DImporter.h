#pragma once
#include <projects/objektivlib/ObjToolTopology.h>
#include "P3DStoreHelper.h"
#include "IFBXPluginServices.h"

class P3DImporter :  public P3DStoreHelper
{


    IFBXImportControl *control;
public:

    P3DImporter(IFBXImportControl *control, FbxSdk::MManager &manager);

    virtual void Mesh(KFbxXMatrix& pGlobalPosition, KFbxMesh* pMesh, 
                 KFbxVector4* pVertexArray);
    virtual void LimbNode(KFbxNode *node, KFbxXMatrix& pGlobalBasePosition, 
                 KFbxXMatrix& pGlobalEndPosition);
    virtual void WalkNode(KFbxNode* pNode, 
              KTime& lTime, 
              KFbxXMatrix& pParentGlobalPosition,
              KFbxXMatrix& pGlobalPosition);


protected:

	FbxSdk::MManager &manager;
    typedef AutoArray<ObjToolTopology::STessellateInfo> PolygonInfo;
    class AllPolyInfo : public AutoArray<PolygonInfo> 
    {
    public:
        ClassIsMovable(AllPolyInfo);
    };

    void ScanWeights(KFbxMesh* pMesh, ObjectData &object);
    void ReadUVs(KFbxMesh* pMesh,ObjectData &object, const AllPolyInfo &pointInfo);
    void ImportSharpEdges(KFbxMesh* pMesh, ObjectData &object);
    void TryToRemoveHiddenEdges(KFbxMesh* pMesh, ObjectData &object);
};
