#include "StdAfx.h"
#include "RVMATExplorer.h"
#include <projects/objektivlib/LODObject.h>
#include <projects/objektivlib/ObjToolsMatLib.h>
#include <el/Pathname/Pathname.h>
#include <el/ParamFile/ParamFile.hpp>

using namespace ObjektivLib;

void RVMATExplorer::ExploreP3D(const LODObject &p3d, const char *projectRoot) {


    BTree<ObjMatLibItem> matlist;
    for (int i = 0; i < p3d.NLevels(); i++) {

        const ObjectData *obj = p3d.Level(i);
        
        const ObjToolMatLib &matlib = obj->GetTool<ObjToolMatLib>();
        matlib.ReadMatLib(matlist,ObjToolMatLib::ReadMaterials);
    }

    BTreeIterator<ObjMatLibItem> iter(matlist);
    ObjMatLibItem *itm;
    while ((itm = iter.Next()) != 0) {        
        ExploreMaterial(itm->name, projectRoot);
    }

}

void RVMATExplorer::ExploreMaterial(const char *matname, const char *projectRoot) {

    if (matname == 0 || matname[0] == 0) return;

    RString fullnamestr(projectRoot,matname);
    Pathname rvmatname(PathNull);
    rvmatname.SetPathName(fullnamestr);

    ParamFile rvmat;
    rvmat.Parse(rvmatname);

    for (int i = 0, cnt = rvmat.GetEntryCount(); i<cnt; i++) {
        
        ParamEntryVal entry = rvmat.GetEntry(i);

        if (entry.IsClass()) {
            int stagenum;
            if (sscanf(entry.GetName(),"Stage%d",&stagenum) == 1) {
                
                ParamClassPtr stage = entry.GetClassInterface();
                ParamEntryPtr entval = stage->FindEntry("uvSource");
                if (entval.NotNull() && entval->IsTextValue()) {

                    int uvset = 0;
                    RString uvassg = entval->GetValue();
                    if (uvassg == "tex" || sscanf(uvassg,"tex%d",&uvset) == 1) {
                        entval = stage->FindEntry("texture");
                        if (entval.NotNull() && entval->IsTextValue()) {
                            RString texname(entval->GetValue());
                            if (texname.GetLength() && texname[0] != '#') {
                                
                                //add pair uvset and texture
                                texname = RString(projectRoot, texname);

                                insert(Item(Key(matname, uvset), texname));

                            }
                        }
                    }

                }
            }
        }
    }
}

const char *RVMATExplorer::GetTextureOfMaterial(
            const RStringI &materialName, int uvset) const {

    const_iterator iter = find(Key(materialName,uvset));
    if (iter == end()) return 0;
    else return iter->second;

}