#include "StdAfx.h"
#include "P3DImporter.h"
#include <projects/objektivlib/bianmimport.h>
#include <map>
#include "hlpfuncts.h"
#include "textureTypes.h" 

P3DImporter::P3DImporter(IFBXImportControl *control, FbxSdk::MManager &manager):control(control),manager(manager) {}


void P3DImporter::LimbNode(KFbxNode *node, KFbxXMatrix& pGlobalBasePosition, 
                             KFbxXMatrix& pGlobalEndPosition) 
{
	if (control->GetImportMask() & IFBXImportControl::importSkeleton)
	{
    ObjectData &obj = *p3d.Active();

    float masterScale = control->GetMasterScale();

    
    KFbxVector4 pos =   pGlobalEndPosition.GetT();
    KFbxXMatrix parent = pGlobalBasePosition.Inverse();
    KFbxVector4 relpos = parent.MultT(pos);

    float sz = (float)relpos.Length();
    relpos.Normalize();

    int ptbeg = obj.NPoints();

    CBIANMImport::CreateBonePrimitive(obj,Vector3(relpos[0],relpos[1],relpos[2]),sz);
    int ptend = obj.NPoints();
    KFbxNode *parnode = node->GetParent();
    Selection *sel = obj.GetNamedSel(parnode ->GetName());    
    if (sel == NULL) {
        Selection empty(&obj);
        obj.SaveNamedSel(parnode->GetName(),&empty);
        sel = obj.GetNamedSel(parnode->GetName());    
    }
    
    if (sel == NULL)
        abort();
        

    for (int i = ptbeg; i <  ptend; i++)  {
        sel->PointSelect(i);
        PosT &ps = obj.Point(i);
        KFbxVector4 vx(ps[0],ps[1],ps[2]);
        vx = pGlobalBasePosition.MultT(vx);
        ps.SetPoint(Vector3(vx[0] * masterScale,
                            vx[1] * masterScale,
                            -vx[2] * masterScale));

    }
	}
	if (control->GetImportMask() & IFBXImportControl::importBonePivots)
	{
		ObjectData &obj = *p3d.Active();
		float masterScale = control->GetMasterScale();

		KFbxNode *parnode = node->GetParent();
		Selection *sel = obj.GetNamedSel(parnode ->GetName());    
		if (sel == NULL) {
			Selection empty(&obj);
			obj.SaveNamedSel(parnode->GetName(),&empty);
			sel = obj.GetNamedSel(parnode->GetName());    
		}

		if (sel == NULL)
			abort();

		int p=obj.NPoints();
		obj.ReservePoints(1);

		KFbxVector4 pos =   pGlobalBasePosition.GetT();
		PosT ps;
		ps[0]=pos[0]*masterScale; ps[1]=pos[1]*masterScale; ps[2]=-pos[2]*masterScale;

		ps.flags=0;
		obj.Point(p)=ps;
		sel->PointSelect(p);
	}

//    obj.SaveNamedSel(node->GetName(),&sel);
}
    




void P3DImporter::Mesh(KFbxXMatrix& pGlobalPosition, KFbxMesh* pMesh,KFbxVector4* pVertexArray) 
{
    if ((control->GetImportMask() & IFBXImportControl::importMesh) == 0)
        return;

    float masterScale = control->GetMasterScale();


    int vertexCount = pMesh->GetControlPointsCount();
    ObjectData object;

    object.ReservePoints(vertexCount);
    for (int i = 0; i< vertexCount ;i++)
    {
        KFbxVector4 transformed = pGlobalPosition.MultT(pVertexArray[i]);
        PosT pos;
        pos.SetPoint(Vector3(transformed[0] * masterScale,
                             transformed[1] * masterScale,
                             -transformed[2] * masterScale));
        pos.flags = 0;
        object.Point(i) = pos;
    }


    int polygons = pMesh->GetPolygonCount();
    AutoArray<int> indices;
    AllPolyInfo polygonInfo;
    PolygonInfo pol;   
    for (int i = 0; i< polygons; i++)
    {
        int ns = pMesh->GetPolygonSize(i);
        indices.Clear();
        indices.Resize(ns);
        for (int j = 0; j< ns; j++)             
            indices[j] = (pMesh->GetPolygonVertex(i,j));

        pol.Resize(ns*3);
        Array<ObjToolTopology::STessellateInfo> nfo(pol.Data(),pol.Size());
        object.GetTool<ObjToolTopology>().TessellatePolygon(ns,indices.Data(),0,0,0,&nfo);
        polygonInfo.Add(PolygonInfo(nfo));
    }

    ReadUVs(pMesh,object,polygonInfo);

    if (control->GetImportMask() & IFBXImportControl::importSkinning)
        ScanWeights(pMesh,object);

	if (control->GetImportMask() & IFBXImportControl::importSmooths)
        ImportSharpEdges(pMesh, object);

    const char *meshName = lastName;
    if (meshName[0]==0 || control->DontCreateObjectSelections()) meshName = 0;

    p3d.Active() -> Merge(object,meshName,true);
}


void P3DImporter::ScanWeights(KFbxMesh* pMesh, ObjectData &object)
{
   


    int deformers = pMesh->GetDeformerCount(KFbxDeformer::eSKIN);
    for (int d = 0; d < deformers; d++) {
            
    	KFbxSkin* curDeformer = ((KFbxSkin*)pMesh->GetDeformer(d, KFbxDeformer::eSKIN));
        int clusterCount = curDeformer->GetClusterCount();
        if (clusterCount == 0) continue;
        KFbxCluster::ELinkMode clusterMode = curDeformer->GetCluster(0)->GetLinkMode();


        AutoArray<SRef<NamedSelection> > newSelections;
    
        for (int j = 0; j < clusterCount; j++) {

			KFbxCluster* lCluster =curDeformer->GetCluster(j);
            RString clusterName;
			if (!lCluster->GetLink())
				clusterName = lCluster->GetName();
            else
                clusterName = lCluster->GetLink()->GetName();

            if (clusterName.GetLength() == 0)  {
                sprintf_s(clusterName.CreateBuffer(20),20,"%d",j);
            }

            
            SRef<NamedSelection> sel = new NamedSelection(&object,RemoveUniqueId(clusterName));            

 			int lVertexIndexCount = lCluster->GetControlPointIndicesCount();

			for (int k = 0; k < lVertexIndexCount; ++k) 
            {
				int lIndex = lCluster->GetControlPointIndices()[k];
				double lWeight = lCluster->GetControlPointWeights()[k];
                sel->SetPointWeight(lIndex,(float)lWeight);
            }

            newSelections.Add(sel);
        }

        if (clusterMode == KFbxCluster::eNORMALIZE) {

            for (int i = 0; i < object.NPoints(); i++) {

                float sum = 0;
                for (int j = 0; j < newSelections.Size(); j++) 
                    sum = sum + newSelections[j]->PointWeight(i);
                sum = 1/sum;
                if (_finite(sum)) {
                    for (int j = 0; j < newSelections.Size(); j++) {
                        newSelections[j]->SetPointWeight(i,
                            newSelections[j]->PointWeight(i) * sum);

                    }
                }
            }
        }

        for (int j = 0; j < newSelections.Size(); j++) {
            const Selection *sel = object.GetNamedSel(newSelections[j]->Name());
            if (sel) (*(newSelections[j])) += *sel;
            object.SaveNamedSel(newSelections[j]->Name(),newSelections[j].GetRef());        
        }
    }

}

static RString ExtractTextureName(IFBXImportControl *control, KFbxTexture* tex) {

    const char *basePath = control->GetTextureBasePath();
    //const char *fname = tex->GetFileName();
	const char *fname = tex->GetRelativeFileName();
    if (fname == 0) return fname;

    // convert slashs to backslashs 
    char * nName = _strdup(fname);
    for (char *i = nName;*i!='\0';++i) if (*i=='/') *i='\\'; 

    fname = control->ConvertTexture(nName);
    if (basePath == 0) return fname;    
    return Pathname::FullToRelativeProjectRoot(fname,basePath);
}

static RString ExtractMaterialName(IFBXImportControl *control, KFbxTexture* tex) {

    if (control->GetImportMask() & control->importRVMATRefs) {
        const char *basePath = control->GetTextureBasePath();
        const char *fname = tex->GetMediaName();
        if (fname == 0) return fname;

        // convert slashs to backslashs 
        char * nName = _strdup(fname);
        for (char *i = nName;*i!='\0';++i) if (*i=='/') *i='\\'; 
		    

        //only path with extension .rvmat is valid
        if (_stricmp(Pathname::GetExtensionFromPath(nName),".rvmat") != 0)
            return RString();
        fname = control->ConvertTexture(nName);
        if (basePath == 0) return fname;    
        return Pathname::FullToRelativeProjectRoot(fname,basePath);
    }
    else
        return RString();
}

static KFbxTexture *ExtractTextureFromMaterial(KFbxSurfaceMaterial *lMaterial) {
    for (int i = 0; i < FBXTextureTypes::getCountTypes(); i++) {
        const char *propName = FBXTextureTypes::getType(i);
        KFbxProperty lProperty = lMaterial->FindProperty(propName);
        int lNbTextures = lProperty.GetSrcObjectCount(KFbxTexture::ClassId);
        if (lNbTextures>0) {
            KFbxTexture *fbxtex = (KFbxTexture *)lProperty.GetSrcObject(KFbxTexture::ClassId,0);
            if (fbxtex) return fbxtex;
        }
    }
    return 0;
}

void P3DImporter::ReadUVs(KFbxMesh* pMesh,ObjectData &object, const AllPolyInfo &pointInfo) {

    if ((control->GetImportMask() & (IFBXImportControl::importUVSets | IFBXImportControl::importTextures) ) == 0)
        return;

    typedef std::map<std::pair<int,int>, KFbxTexture *> RVMatDataMap;
    RVMatDataMap rvmatdatamap;

    for (int i = 0,  cnt = pMesh->GetLayerCount(); i < cnt; i++) {
        KFbxLayer *layer = pMesh->GetLayer(i);
        if (layer) {


            //UV Sets

            KFbxLayerElementUV *layerUv =  layer->GetUVs();
            if ((layerUv != 0) && (control->GetImportMask() & (IFBXImportControl::importUVSets))) {

                KFbxLayerElement::EMappingMode mapMode = layerUv->GetMappingMode();
                KFbxLayerElement::EReferenceMode refMode = layerUv->GetReferenceMode();
                const KFbxLayerElementArrayTemplate<KFbxVector2> *uvs = &layerUv->GetDirectArray();
                const KFbxLayerElementArrayTemplate<int> *uvs_ids = refMode==KFbxLayerElement::eDIRECT?NULL:&layerUv->GetIndexArray();

                if (mapMode != KFbxLayerElement::eNONE 
                    && mapMode != KFbxLayerElement::eALL_SAME 
                    && mapMode != KFbxLayerElement::eBY_EDGE
                    && mapMode != KFbxLayerElement::eBY_POLYGON) //empty or not supported
                {                        
                    int uvsetidx = control->GetUVSetForTextureType(i);
                    object.AddUVSet(uvsetidx);
                    object.SetActiveUVSet(uvsetidx);

                    int ns = 0;

                    for (int k = 0, offset = 0; k < pointInfo.Size(); 
                            k++, offset += ns) {

                        ns = pMesh->GetPolygonSize(k);
                        const PolygonInfo &polInfo = pointInfo[k];

                        for (int l = 0; l < polInfo.Size(); l++)
                        {
                            const ObjToolTopology::STessellateInfo &tesInfo = polInfo[l];
                            int uvsrc = 0;
                            switch (mapMode) {
                                
                                case KFbxLayerElement::eBY_CONTROL_POINT:
                                    uvsrc = pMesh->GetPolygonVertex(k,tesInfo.index);
                                    break;
                                case KFbxLayerElement::eBY_POLYGON_VERTEX:
                                    uvsrc = offset + (tesInfo.index);
                                    break;
                            }
                            
                            int idx = uvs_ids? uvs_ids->GetAt(uvsrc):uvsrc;
                            KFbxVector2 uvinfo = uvs->GetAt(idx);

                            FaceT fc(object,tesInfo.face);
                            fc.SetUV(tesInfo.vx,uvinfo[0],1.0f - uvinfo[1]);

                        }
                    }
                }
                else
                    control->UnsupportedReferenceModeForUVCoords(lastName,mapMode);
            }
        
            //Materials
 
            KFbxLayerElementMaterial *layerMat = layer->GetMaterials();
            if (layerMat && (control->GetImportMask() & (IFBXImportControl::importTextures))) {                
                KFbxLayerElement::EMappingMode mapMode = layerMat->GetMappingMode();
                KFbxLayerElement::EReferenceMode refMode = layerMat->GetReferenceMode();
                int uvsetidx = control->GetUVSetForTextureType(i);
                if (mapMode == KFbxLayerElement::eALL_SAME) {
                    KFbxSurfaceMaterial* lMaterial = pMesh->GetNode()->GetMaterial(layerMat->GetIndexArray().GetAt(0));
                    if (lMaterial) {
                        KFbxTexture *fbxtex = ExtractTextureFromMaterial(lMaterial);
                        if (fbxtex) {
                            RString tex = ExtractTextureName(control,fbxtex );
                            RString mat = ExtractMaterialName(control,fbxtex );
                            for (int i = 0; i < object.NFaces(); i++) {
                                FaceT fc(object,i);
                                if (uvsetidx == 0) {              
                                    fc.SetTexture(tex);
                                    fc.SetMaterial(mat);
                                }
                                else
                                    rvmatdatamap.insert(
                                        std::make_pair(std::make_pair(i,uvsetidx),fbxtex));
                            }
                        }
                    }
                } else if (mapMode == KFbxLayerElement::eBY_POLYGON) {
                    for (int k = 0; k < pointInfo.Size(); k++) {
                        int texsrc = k;
                        if (refMode!=KFbxLayerElement::eDIRECT) {
                            texsrc = layerMat->GetIndexArray().GetAt(k);
                            if (texsrc < 0) {
                                control->InvalidIndexInArray(lastName,i);
                                continue;
                            }
                        }
                        KFbxSurfaceMaterial* lMaterial = pMesh->GetNode()->GetMaterial(texsrc);
                        if (lMaterial) {
                            KFbxTexture *fbxtex = ExtractTextureFromMaterial(lMaterial);
                            if (fbxtex) {
                                RString tex = ExtractTextureName(control,fbxtex );
                                RString mat = ExtractMaterialName(control,fbxtex );
                                for (int j = 0; j < pointInfo[k].Size(); j++) {
                                    FaceT fc(object,pointInfo[k][j].face);
                                    if (uvsetidx == 0) {
                                        fc.SetTexture(tex);
                                        fc.SetMaterial(mat);
                                    }
                                    else
                                        rvmatdatamap.insert(
                                            std::make_pair(std::make_pair(fc.GetFaceIndex(),uvsetidx),fbxtex ));
                                }
                            }
                        }
                    }
                }
                else
                    control->UnsupportedReferenceModeForTextures(lastName,mapMode);



            }
        
                
        }
   }
    if (!rvmatdatamap.empty()) {
        IFBXImportControl::DataForRVMat tmp;
        int lastlayer = -1, lastface = -1;
        for (RVMatDataMap::const_iterator iter = rvmatdatamap.begin();
            iter != rvmatdatamap.end(); ++iter) {

                if (lastface != iter->first.first) {
                    if (tmp.Size()) {
                        const char *name = control->CreateRVMATPrototype(tmp);
                        tmp.Clear();
                        if (name == 0) break;
                        FaceT fc(object,lastface);
                        fc.SetMaterial(Pathname::FullToRelativeProjectRoot(name,control->GetTextureBasePath()));
                    }                        
                    lastface = iter->first.first;
                    lastlayer = -1;
                }

                if (lastlayer != iter->first.second) {
                    lastlayer = iter->first.second;
                    tmp.Add(lastlayer,ExtractTextureName(control,iter->second).Data());
                }
        }
        if (tmp.Size()) {
            const char *name = control->CreateRVMATPrototype(tmp);
            if (name != 0) {
                FaceT fc(object,lastface);
                fc.SetMaterial(Pathname::FullToRelativeProjectRoot(name,control->GetTextureBasePath()));
            }
        }                        

    }
}

    /*

    KFbxLayerElement::ELayerElementType textures[] ={
        KFbxLayerElement::eDIFFUSE_TEXTURES,
        KFbxLayerElement::eEMISSIVE_TEXTURES,
        KFbxLayerElement::eEMISSIVE_FACTOR_TEXTURES,
        KFbxLayerElement::eAMBIENT_TEXTURES,
        KFbxLayerElement::eAMBIENT_FACTOR_TEXTURES,
        KFbxLayerElement::eDIFFUSE_FACTOR_TEXTURES,
        KFbxLayerElement::eSPECULAR_TEXTURES,
        KFbxLayerElement::eNORMALMAP_TEXTURES,
        KFbxLayerElement::eSPECULAR_FACTOR_TEXTURES,
        KFbxLayerElement::eSHININESS_TEXTURES,
        KFbxLayerElement::eBUMP_TEXTURES,
        KFbxLayerElement::eTRANSPARENT_TEXTURES,
        KFbxLayerElement::eTRANSPARENCY_FACTOR_TEXTURES,
        KFbxLayerElement::eREFLECTION_TEXTURES,
        KFbxLayerElement::eREFLECTION_FACTOR_TEXTURES,
    };

    const char *texturesMaterial[] = {
        KFbxSurfaceMaterial::sDiffuse,
        KFbxSurfaceMaterial::sEmissive,
        KFbxSurfaceMaterial::sEmissiveFactor,
        KFbxSurfaceMaterial::sAmbient,
        KFbxSurfaceMaterial::sAmbientFactor,
        KFbxSurfaceMaterial::sDiffuseFactor,
        KFbxSurfaceMaterial::sSpecular,
        KFbxSurfaceMaterial::sNormalMap,
        KFbxSurfaceMaterial::sSpecularFactor,
        KFbxSurfaceMaterial::sShininess,
        KFbxSurfaceMaterial::sBump,
        KFbxSurfaceMaterial::sTransparentColor,
        KFbxSurfaceMaterial::sTransparencyFactor,
        KFbxSurfaceMaterial::sReflection,
        KFbxSurfaceMaterial::sReflectionFactor
    };

    typedef std::map<std::pair<int,int>, KFbxTexture *> RVMatDataMap;
    RVMatDataMap rvmatdatamap;
        
    for (int i = 0,  cnt = pMesh->GetLayerCount(); i < cnt; i++) {
        KFbxLayer *layer = pMesh->GetLayer(i);
        if (layer) {

            for (int j = 0; j < lenof(textures); j++)
            {
                KFbxLayerElementUV *layerUv =  layer->GetUVs(textures[j]);
                if ((layerUv != 0) && (control->GetImportMask() & (IFBXImportControl::importUVSets))) {

                    KFbxLayerElement::EMappingMode mapMode = layerUv->GetMappingMode();
                    const KFbxLayerElementArrayTemplate<KFbxVector2> &uvs = layerUv->GetDirectArray();
                    const KFbxLayerElementArrayTemplate<int> &uvs_ids = layerUv->GetIndexArray();
                    KFbxLayerElement::EReferenceMode refMode = layerUv->GetReferenceMode();
                
                    if (mapMode != KFbxLayerElement::eNONE 
                        && mapMode != KFbxLayerElement::eALL_SAME 
                        && mapMode != KFbxLayerElement::eBY_EDGE
                        && mapMode != KFbxLayerElement::eBY_POLYGON) //empty or not supported
                    {                        
                        int uvsetidx = control->GetUVSetForTextureType(i,textures[j]);
                        object.AddUVSet(uvsetidx);
                        object.SetActiveUVSet(uvsetidx);

                        int ns = 0;

                        for (int k = 0, offset = 0; k < pointInfo.Size(); 
                                k++, offset += ns) {

                            ns = pMesh->GetPolygonSize(k);
                            const PolygonInfo &polInfo = pointInfo[k];

                            for (int l = 0; l < polInfo.Size(); l++)
                            {
                                const ObjToolTopology::STessellateInfo &tesInfo = polInfo[l];
                                int uvsrc = 0;
                                switch (mapMode) {
                                    
                                    case KFbxLayerElement::eBY_CONTROL_POINT:
                                        uvsrc = pMesh->GetPolygonVertex(k,tesInfo.index);
                                        break;
                                    case KFbxLayerElement::eBY_POLYGON_VERTEX:
                                        uvsrc = offset + (tesInfo.index);
                                        break;
                                }
                                
                                int idx = refMode==KFbxLayerElement::eDIRECT? uvsrc: uvs_ids[uvsrc];
                                KFbxVector2 uvinfo = uvs[ idx ];
                                

                                FaceT fc(object,tesInfo.face);
                                fc.SetUV(tesInfo.vx,uvinfo[0],1.0f - uvinfo[1]);
//                                printf("%5d %5d %5d %5d %8.4f %8.4f\n",k,l,uvsrc, idx, uvinfo[0], uvinfo[1]);

                            }
                        }
                    }
                    else
                        control->UnsupportedReferenceModeForUVCoords(lastName,mapMode);
                }
            }
            KFbxLayerElementMaterial *layerMat = layer->GetMaterials();
            if (layerMat && (control->GetImportMask() & (IFBXImportControl::importTextures))) {                
                KFbxLayerElement::EMappingMode mapMode = layerMat->GetMappingMode();
                KFbxLayerElement::EReferenceMode refMode = layerMat->GetReferenceMode();
                for (int j = 0; j < lenof(textures); j++) {
                    int uvsetidx = control->GetUVSetForTextureType(i,textures[j]);
                    if (mapMode == KFbxLayerElement::eALL_SAME) {
                        KFbxSurfaceMaterial* lMaterial = pMesh->GetNode()->GetMaterial(layerMat->GetIndexArray().GetAt(0));
                        if (lMaterial) {
                            KFbxProperty lProperty = lMaterial->FindProperty(texturesMaterial[j]);
                            int lNbTextures = lProperty.GetSrcObjectCount(KFbxTexture::ClassId);
                            if (lNbTextures>0) {
                                KFbxTexture *fbxtex = (KFbxTexture *)lProperty.GetSrcObject(KFbxTexture::ClassId,0);
                                RString tex = ExtractTextureName(control,fbxtex );
                                RString mat = ExtractMaterialName(control,fbxtex );
                                for (int i = 0; i < object.NFaces(); i++) {
                                    FaceT fc(object,i);
                                    if (uvsetidx == 0) {              
                                        fc.SetTexture(tex);
                                        fc.SetMaterial(mat);
                                    }
                                    else
                                        rvmatdatamap.insert(
                                            std::make_pair(std::make_pair(i,uvsetidx),fbxtex));
                                }
                            }
                        }
                    } else if (mapMode == KFbxLayerElement::eBY_POLYGON) {
                        for (int k = 0; k < pointInfo.Size(); k++) {
                            int texsrc = k;
                            if (refMode!=KFbxLayerElement::eDIRECT) {
                                texsrc = layerMat->GetIndexArray().GetAt(k);
                                if (texsrc < 0) {
                                    control->InvalidIndexInArray(lastName,textures[j]);
                                    continue;
                                }
                            }
                            KFbxSurfaceMaterial* lMaterial = pMesh->GetNode()->GetMaterial(texsrc);
                            if (lMaterial) {
                                KFbxProperty lProperty = lMaterial->FindProperty(texturesMaterial[j]);
                                int lNbTextures = lProperty.GetSrcObjectCount(KFbxTexture::ClassId);
                                if (lNbTextures>0) {
                                    KFbxTexture *fbxtex = (KFbxTexture *)lProperty.GetSrcObject(KFbxTexture::ClassId,0);
                                    RString tex = ExtractTextureName(control,fbxtex );
                                    RString mat = ExtractMaterialName(control,fbxtex );
                                    for (int j = 0; j < pointInfo[k].Size(); j++) {
                                        FaceT fc(object,pointInfo[k][j].face);
                                        if (uvsetidx == 0) {
                                            fc.SetTexture(tex);
                                            fc.SetMaterial(mat);
                                        }
                                        else
                                            rvmatdatamap.insert(
                                                std::make_pair(std::make_pair(fc.GetFaceIndex(),uvsetidx),fbxtex ));
                                    }
                                }
                            }                            
                        }
                    }
                    else
                        control->UnsupportedReferenceModeForTextures(lastName,mapMode);
                }
            }
            
        }                                                               
    }

}
    */

void P3DImporter::WalkNode(KFbxNode* pNode, 
              KTime& lTime, 
              KFbxXMatrix& pParentGlobalPosition,
              KFbxXMatrix& pGlobalPosition) {
    
    if (!control->CanImportObject((unsigned long)pNode))
        return;
    
    if ((control->GetImportMask() & IFBXImportControl::importLODs) == 0) {
        const char *name = pNode->GetName();
        if (name && name[0])
        lastName = name;
        SceneWalker::WalkNode(pNode,lTime,pParentGlobalPosition,pGlobalPosition);
    }
    else
        P3DStoreHelper::WalkNode(pNode,lTime,pParentGlobalPosition,pGlobalPosition);

}
typedef std::pair<int,int> SimpleEdge;
/*
typedef SimpleEdge FacePair;
typedef std::map<SimpleEdge,FacePair> EdgeMap;
typedef std::pair<SimpleEdge,FacePair> EdgeMapItem;

static void BuildEdgeMap(ObjectData &object, EdgeMap &egmap) {
    
    for (int i = 0; i <  object.NFaces(); i++) {
        FaceT fc(object,i);
        int b = fc.N()-1;
        for (int e = 0; e <fc.N(); b = e, e++) {
            int v1 = fc.GetPoint(b);
            int v2 = fc.GetPoint(e);
            if (v1 > v2) std::swap(v1, v2);
            EdgeMap::iterator iter = egmap.find(SimpleEdge(v1, v2));
            if (iter != egmap.end()) iter->second.second = i;
            else egmap.insert(EdgeMapItem(SimpleEdge(v1,v2),FacePair(i,-1)));
        }
    }
}

static void JoinFaces(ObjectData &object, EdgeMap &egmap) {
    Selection todel(&object);
    for (EdgeMap::iterator iter = egmap.begin(); iter != egmap.end(); ++iter) {
        if (iter->second.first < 0 || iter->second.second < 0) 
            continue;
        FaceT fc1(object,iter->second.first);
        FaceT fc2(object,iter->second.second);
        if (fc1.N() == 3 && fc2.N() == 3 && todel.FaceSelected(iter->second.first) == false && todel.FaceSelected(iter->second.second) == false) {


            int frthpt = -1;

            for (int j = 0; j < fc2.N(); j++) 
                if (fc2.GetPoint(j) != iter->first.first && fc2.GetPoint(j) != iter->first.second) 
                    frthpt = j;

            if (frthpt == -1) 
                continue;

            int b = fc1.N()-1;
            int bb = fc1.N()-2;
            for (int e = 0; e < fc1.N();bb = b, b = e,e++) {
                
                int v0 = fc1.GetPoint(bb);
                int v1 = fc1.GetPoint(b);
                int v2 = fc1.GetPoint(e);
                int v3 = fc2.GetPoint(frthpt);

                Vector3 vx1 = object.Point(v1);
                Vector3 vx0 = object.Point(v0) - vx1;
                Vector3 vx3 = object.Point(v3) - vx1;
                if (v1 > v2) std::swap(v1, v2);
                if (v1 == iter->first.first && v2 ==iter->first.second) {
                    float cosAngle = vx3.CosAngle(vx0);
                    if (cosAngle < 0.99 && cosAngle > -0.99) {

                        fc1.SetN(4);
                        for (int k = 2; k >= e; k--)
                            fc1.CopyPointInfo(k+1,k);
                        fc1.CopyPointInfo(e,fc2,frthpt);
                        todel.FaceSelect(fc2.GetFaceIndex());
                    }
                    break;
                }

            }
                
        }
    }
    object.SelectionDeleteFaces(&todel);
}
/*
            FaceT fc3;
            fc3.CreateIn(&object);
            fc1.CopyFaceTo(fc3.GetFaceIndex());
            fc3.SetN(4);
            int vs = 0;
            int j = 0;
            for (int i = 0; i < fc1.N(); i++) {
                fc3.CopyPointInfo(vs++,fc1,i);
                if (fc1.GetPoint(i) == iter->first.first || fc1.GetPoint(i) == iter->first.second) {
                    for (; j < fc2.N(); j++) {
                        if (fc2.GetPoint(j) != iter->first.first && fc2.GetPoint(j) != iter->first.second) {
                            fc3.CopyPointInfo(vs++,fc2,j);
                        }
                    }
                }            
            }*/

void P3DImporter::ImportSharpEdges(KFbxMesh* pMesh, ObjectData &object) {
    
    CEdges objectEdges(object.NPoints());

    KFbxLayer *layer = pMesh->GetLayer(0);
    if (layer) {
        KFbxLayerElementSmoothing *smoothing = layer->GetSmoothing();
        if (smoothing) {
            KFbxLayerElement::EMappingMode mapMode = smoothing->GetMappingMode();
			if (mapMode == KFbxLayerElement::eBY_POLYGON) {
				// Convert from polygon to edge smoothing
				KFbxGeometryConverter c(manager);
				c.ComputeEdgeSmoothingFromPolygonSmoothing(pMesh, 0);
				mapMode = smoothing->GetMappingMode();
			}

			if (mapMode == KFbxLayerElement::eBY_EDGE) {
/*                EdgeMap egmap;
                BuildEdgeMap(object,egmap);*/
                int edgeCnt = pMesh->GetMeshEdgeCount();
                KFbxLayerElement::EReferenceMode refMode = smoothing->GetReferenceMode();
                const KFbxLayerElementArrayTemplate<int> &edgeinfo = smoothing->GetDirectArray();
                const KFbxLayerElementArrayTemplate<int> *idxs 
                    = refMode ==  KFbxLayerElement::eDIRECT?0:&smoothing->GetIndexArray();                
                for (int i = 0; i < edgeCnt; i++) {
                    int start;
                    int end;
                    pMesh->GetMeshEdgeVertices (i,start,end);
                    int eidx = idxs == 0? i: idxs->GetAt(i);
                    if (start < end)
                        std::swap(start, end);
                    if (edgeinfo[eidx] == false)
                        objectEdges.SetEdge(start,end);
//                    egmap.erase(SimpleEdge(start,end));
                }
                object.SaveSharpEdges(objectEdges);
                object.Squarize();
				object.RecalcNormals(true);
/*                JoinFaces(object,egmap);*/

            }
            else if (mapMode == KFbxLayerElement::eBY_POLYGON) {
                /*EdgeMap egmap;*/
                typedef std::map<SimpleEdge, unsigned long> SmoothMap;
                SmoothMap smoothmap;
                /*BuildEdgeMap(object,egmap);*/
                KFbxLayerElement::EReferenceMode refMode = smoothing->GetReferenceMode();                
                const KFbxLayerElementArrayTemplate<int> &smoothinfo = smoothing->GetDirectArray();
                const KFbxLayerElementArrayTemplate<int> &idxs = smoothing->GetIndexArray();
                int count = pMesh->GetPolygonCount();
                for (int i = 0; i < count; i++) {
                    int eidx = refMode ==  KFbxLayerElement::eDIRECT? i: idxs[i];
                    unsigned long grpid = (unsigned long)(smoothinfo[eidx]);
                    int polysz = pMesh->GetPolygonSize(i);
                    int pt1 = pMesh->GetPolygonVertex(i, polysz-1);
                    for (int k = 0; k < polysz; k++) {
                        int pt2 = pMesh->GetPolygonVertex(i, k);
                        int start = pt1;
                        int end = pt2;
                        if (start > end) 
                            std::swap(start,end);
                        SimpleEdge se(start,end);
                        SmoothMap::iterator fnd = smoothmap.find(se);
                        if (fnd == smoothmap.end()) {
                            smoothmap[se] = grpid;
                        } else {
                            if ((fnd->second & grpid) == 0) {
                               objectEdges.SetEdge(start,end);
 /*                               egmap.erase(se);*/
                            } else {
                                fnd->second |= grpid;
                            }
                        }
                        pt1 = pt2;
                    }
                }
                object.SaveSharpEdges(objectEdges);
                object.Squarize();
/*               JoinFaces(object,egmap);*/
               

            } else
                control->UnsupportedReferenceModeForSmoothing(lastName,mapMode);
        }
    }

}