// ITexMerge.cpp: implementation of the CITexMerge class.
//
//////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "objektiv2.h"
#include "ITexMerge.h"
#include <El/paramfile/paramFile.hpp>
#include "TexList.h"
#include "Objektiv2Doc.h"
#include "../ObjektivLib/MergeTexturesTool.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//#ifdef textmerge

class ReplaceStruct
  {
  public:
    typedef float TRANS[3][2];
    char srcTex[256];
    char trgTex[256];
    TRANS transform;
    void Multiply(TRANS nw);
    void Transform(float& tu, float& tv);
    void Scale(float factor);
    void SetRect(float x, float y, float sizex, float sizey, int angle, float mainszx, float mainszy);
    void Rotate(int count);
    ReplaceStruct(const char *src, const char *trg)
      {
      memset(srcTex,0,sizeof(srcTex));
      strncpy(srcTex,src,sizeof(srcTex));strncpy(trgTex,trg,sizeof(trgTex));
      _strlwr(srcTex);
      trgTex[255]=0;
      memset(transform,0,sizeof(transform));
      transform[0][0]=1;
      transform[1][1]=1;
      }
  };










static THASHTABLE fhash;

void ReplaceStruct::Transform(float& tu, float& tv)
  {
  float savtu=tu,savtv=tv;  
  savtu-=floor(savtu);
  savtv-=floor(savtv);
  tu=savtu*transform[0][0]+savtv*transform[1][0]+transform[2][0];
  tv=savtu*transform[0][1]+savtv*transform[1][1]+transform[2][1];
  }










void ReplaceStruct::Multiply(TRANS nw)
  {
  TRANS save;
  memcpy(save,nw,sizeof(save));
  Transform(save[0][0],save[0][1]);
  Transform(save[1][0],save[1][1]);
  Transform(save[2][0],save[2][1]);
  memcpy(transform,save,sizeof(transform));
  }










void ReplaceStruct::Scale(float factor)	
  {
  transform[0][0]*=factor;
  transform[1][1]*=factor;  
  }










void ReplaceStruct::SetRect(float x, float y, float sizex, float sizey, int angle, float mainszx, float mainszy)
  {  
  float a,b,c,d;
  ASSERT(sizex!=0 && sizey!=0);
  switch (angle)
    {
    case 0:a=sizex;b=0;c=0;d=sizey;break;
    case 1:y+=sizex-1;a=0;b=-sizex;c=sizey;d=0;break;
    case 2:x+=sizex-1;y+=sizey-1;a=-sizex;d=-sizey;c=b=0;break;
    case 3:x+=sizey-1;a=0;b=sizex;c=-sizey;d=0;break;
    default:ASSERT(0);
    }
  float mx=1/mainszx;
  float my=1/mainszy;
  transform[0][0]=a*mx;
  transform[0][1]=b*my;
  transform[1][0]=c*mx;
  transform[1][1]=d*my;
  transform[2][0]=x*mx;
  transform[2][1]=y*my;
  }










void ReplaceStruct::Rotate(int count)
  {
  for (int i=0;i<count;i++)
    {
    float p=transform[0][0];
    transform[0][0]=transform[1][0];
    transform[1][0]=-transform[1][1];
    transform[1][1]=-transform[0][1];
    transform[0][1]=p;
    }
  }










CITexMerge::CITexMerge(const char *script,bool alllods,AutoArray<MatchInfo> *matchInfo):script(script),alllods(alllods),matchInfo(matchInfo)
  {
  recordable=false;
  }










static bool LoadScript(ParamFile& parfile)
  {
  Hash_InitEx(&fhash,101,ReplaceStruct,srcTex,HASHF_EXISTERROR);
  const ParamEntry* entry1=parfile.FindEntry("InfTexMerge").GetPointer();
  if (entry1==0) return false;
  int cnt=entry1->GetEntryCount();
  for (int i=0;i<cnt;i++)
    {
    const ParamEntry& entry2=entry1->GetEntry(i);	
    if (!entry2.IsClass()) continue;
    if (entry2.GetEntryCount()<1) return false;
    const ParamEntry& items=entry2.GetEntry(0);
    ParamEntry *temp=entry2.FindEntry("file").GetPointer();
    if (temp==0) return false;
    RStringB file=(*temp);
    if ((temp=entry2.FindEntry("w").GetPointer())==0) return false;	 
    float mainw=(*temp);
    if ((temp=entry2.FindEntry("h").GetPointer())==0) return false;	 
    float mainh=(*temp);
    int cnt2=items.GetEntryCount();
    for (int j=0;j<cnt2;j++)
      {
      const ParamEntry& item=items.GetEntry(j);
      if ((temp=item.FindEntry("x").GetPointer())==0) return false;	 
      float x=(*temp);
      if ((temp=item.FindEntry("y").GetPointer())==0) return false;	 
      float y=(*temp);
      if ((temp=item.FindEntry("w").GetPointer())==0) return false;	 
      float w=(*temp);
      if ((temp=item.FindEntry("h").GetPointer())==0) return false;	 
      float h=(*temp);
      if ((temp=item.FindEntry("scale").GetPointer())==0) return false;	 
      int scale=(*temp);
      if ((temp=item.FindEntry("angle").GetPointer())==0) return false;	 
      int angle=(*temp);
      if ((temp=item.FindEntry("name").GetPointer())==0) return false;	 
      RStringB name=(*temp);
      char buff[256];
      strcpy(buff,name);
      //	  CTexList::AddPrefix(name,buff,sizeof(buff));
      ReplaceStruct info(buff,file);	  
      if (scale>0) 
        {w=(float)(((int)w)<<scale);h=(float)(((int)h)<<scale);}
      if (scale<0) 
        {w=(float)(((int)w)>>(-scale));h=(float)(((int)h)>>(-scale));}
      info.SetRect((x+0.5f),(y+0.5f),w,h,angle,mainw,mainh);
      Hash_Add(&fhash,&info);
      }
    }
  return true;
  }










static bool RunScript(ObjectData *obj, bool all)
  {
  int cnt=obj->NFaces();
  bool foundsomething=false;
  for (int i=0;i<cnt;i++) if (all || obj->FaceSelected(i))
    {
    FaceT fc(obj,i);
    ReplaceStruct nfo(fc.GetTexture(),"");
    if (Hash_Fill(&fhash,&nfo)!=NULL)
      {
      fc.SetTexture(nfo.trgTex);
      foundsomething=true;
      float tu=0;
      float tv=0;	  
      int j;
      for (j=0;j<fc.N();j++)		
        {
        tu+=fc.GetU(j);
        tv+=fc.GetV(j);
        }
      tu/=fc.N();tu=floor(tu);
      tv/=fc.N();tv=floor(tv);
      for (j=0;j<fc.N();j++)		
        {
        float ntu,ntv;
        ntu=fc.GetU(j)-tu;
        ntv=fc.GetV(j)-tv;
        if (ntu<0.0f) ntu=0.0f;
        if (ntv<0.0f) ntv=0.0f;
        if (ntu>=1.0f) ntu=0.9999f;
        if (ntv>=1.0f) ntv=0.9999f;
        fc.SetUV(j,ntu,ntv);
        }
      
      for (j=0;j<fc.N();j++)		
        {
        float u=fc.GetU(j),v=fc.GetV(j);
        nfo.Transform(u,v);		
        fc.SetUV(j,u,v);
        }
      }
    
    }
  return foundsomething;
  }










static void DoneScript()
  {
  Hash_Done(&fhash);
  }




struct MatchMergeFunctor {

    mutable AutoArray<CITexMerge::MatchInfo> &matchInfo;

    MatchMergeFunctor(AutoArray<CITexMerge::MatchInfo> &matchInfo):matchInfo(matchInfo) {}

    void operator()(RString texture, bool face, bool rvmat) const {
        matchInfo.Add(CITexMerge::MatchInfo(texture,face,rvmat));
    }

};


int CITexMerge::Execute(LODObject *lod)
{
  MergeTexturesTool merge;
  if (merge.LoadPtm(script)!=LSOK) return -1;
  bool success=false;

  success|=merge.MergeTextures(lod);
  success|=merge.MergeMaterialsUsingTextures(lod,config->GetString(IDS_CFGPATHFORTEX));


  if (matchInfo) merge.MatchState(MatchMergeFunctor(*matchInfo));
  matchInfo = 0;

  return success?0:-2;
}

/*

int CITexMerge::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();
  int ret;
  ParamFile param;
  if (param.Parse(script)!=LSOK)
    {
    AfxMessageBox(IDS_TEXMERGESCRIPTERROR);ret=-1;
    }
  else
    {
    if (LoadScript(param))
      {
      bool ok=false;
      if (alllods)
        for (int i=0;i<lod->NLevels();i++)                              
          ok|=RunScript(lod->Level(i),true);
      else
        ok=RunScript(obj,false);
      if (!ok)
        {
        CString s;
        s.LoadString(IDS_TEXMERGENOFOUND);
        for (void *p=NULL;p=::Hash_EnumData(&fhash,p);)
          {
          ReplaceStruct *info=(ReplaceStruct *)p;
          s+=info->srcTex;
          s+=",   ";
          }
        AfxMessageBox(s,MB_OK|MB_ICONSTOP);
        ret=-1;
        }
      else
        ret=0;
      }
    else
      {AfxMessageBox(IDS_TEXMERGESCRIPTERROR);ret=-1;}
    DoneScript();
	comint.sectchanged=true;
    }
  return ret;
  }


*/







//#endif

