#include "stdafx.h"
#include "ciuvsetactions.h"
#include "../ObjektivLib/LODObject.h"

CIAddUVSet::CIAddUVSet(int id):_id(id)
{
  recordable=false;
}

int CIAddUVSet::Execute(LODObject *obj)
{
  ObjectData *active=obj->Active();
  int id=_id;
  ObjUVSet *s=active->AddUVSet(id);
  if (s && id==-1) id=s->GetIndex();  
  return active->SetActiveUVSet(id)?0:-1;
}


CIDeleteUVSet::CIDeleteUVSet(int id):_id(id)
{
  recordable=false;
}

int CIDeleteUVSet::Execute(LODObject *obj)
{
  ObjectData *active=obj->Active();
  return active->DeleteUvSet(active->GetActiveUVSet()->GetIndex())?0:-1;
}

CISetActiveUVSet::CISetActiveUVSet(int id):_id(id)
{
  recordable=false;
}

int CISetActiveUVSet::Execute(LODObject *obj)
{
  ObjectData *active=obj->Active();
  return active->SetActiveUVSet(_id)?0:-1;
}

CIChangeUVSetID::CIChangeUVSetID(int id):_id(id)
{
  recordable=false;
}

int CIChangeUVSetID::Execute(LODObject *obj)
{
  if (_id<0) return -1;
  ObjectData *active=obj->Active();
  ObjUVSet *nextuv=active->GetUVSet(_id);
  ObjUVSet *curuv=active->GetActiveUVSet();
  if (curuv==nextuv) return 0;
  if (nextuv==0)   
    curuv->SetIndex(_id);
  else
  {
    nextuv->SetIndex(curuv->GetIndex());
    curuv->SetIndex(_id);
  }
  return 0;
}