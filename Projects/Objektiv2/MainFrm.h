// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__6F94A438_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_MAINFRM_H__6F94A438_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#include "LodList.h"	// Added by ClassView
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "NamedPropDlg.h"
#include "TexList.h"
#include "SelectionList.h"	// Added by ClassView
#include "HistoryBar.h"
#include "PtInfoBar.h"
#include "AnimListBar.h"
#include "MassBar.h"
#include "ExtrudeStepsBar.h"
#include "HierarchyDlg.h"
#include "PaintSelColorDlg.h"
#include "MacroRecordPlayback.h"
#include "MatLibTreeToolbar.h"
#include <projects\objektivlib\uidebugger\dlldebuggingsession.h>
#include <projects/bredy.libs/InterProcComunicator/IpcSharedMem.h>
#include "DlgUVEditor.h"


#define FRAMENAME "MainFrame"

#define EXTERNALCONFIG "ExternalViewer"


#include "StatusLight.h"


class CObkSplitterWnd: public CSplitterWnd
{
  CStatusLight _light;
  public:
    virtual BOOL CreateScrollBarCtrl( DWORD dwStyle, UINT nID )
    {
      ASSERT_VALID(this);
      ASSERT(m_hWnd != NULL);

      if (dwStyle & SS_GRAYFRAME)
      {
        if (_light.GetSafeHwnd()==0)
        {
          _light.Create("",WS_VISIBLE|WS_CHILD,CRect(0,0,1,1),this,nID);
        }
        return TRUE;
      }      
      return (::CreateWindow(_T("STATIC"), "",
      dwStyle | WS_VISIBLE | WS_CHILD,
      0, 0, 1, 1, m_hWnd, (HMENU)nID,
      AfxGetInstanceHandle(), NULL) != NULL);
    }
    void ChangeStatus(int status) {_light.SetStatus(status);}    
};

//#include "Calculator.h"
#include "MeasureBar.h"
#include "HotKeyCfgDlg.h"	// Added by ClassView
#include <projects/ObjektivLib/IExternalViewer.h>
#include <projects/ObjektivLib/ExternalViewer.h>
#include "DlgViewerEnvironment.h"
#include "MATEditorPlugin.h"
#include "DlgManageScripts.h"
#include "ScriptConsoleBar.h"

class ControlledExternalViewer:public ExternalViewer
  {
  CMATEditorPlugin &_MAT;
  public:
    ControlledExternalViewer(CMATEditorPlugin &MAT):_MAT(MAT) {}
    bool AfterModelPrepared(const ObjectData &source, ObjectData &target);
  };


class CMainFrame : public CFrameWnd
{
  protected: // create from serialization only
    CMainFrame();
    DECLARE_DYNCREATE(CMainFrame)
      int laststate;
    HWND lastfocus;
    HICON focusicon;
	HACCEL userhkeys;    
    
    AutoArray<Ref<IPluginCommand> >_menuCommands;

    // Attributes
  protected:
    CObkSplitterWnd m_wndSplitter;
    ConnReceiverSharedMem _externalRequests;
  public:
    UINT EditCommand;
    UINT PrevEditCommand;
    void SetEditCommand(UINT com) 
    {PrevEditCommand=EditCommand;EditCommand=com;}
    UINT GetEditCommand() 
    {return EditCommand;}
    UINT GetPrevEditCommand() 
    {return PrevEditCommand;}
    
    UINT seloperator;
    float paintweight;
    float paintradiusmin;
    float paintradiusmax;
    float paintstrength;
    CPaintSelColorDlg paintdlg;
    // Operations
  public:
    CLodList m_wndLodList;
    CSelectionList m_wndSelBar;
    CNamedPropDlg m_wndNamedPropBar;
    CHistoryBar m_wndHistoryBar;
    CPtInfoBar  m_wndPtInfo;
    CTexList m_wndTexList;
    CAnimListBar m_wndAnimListBar;
    CMassBar m_wndMassBar;
    CExtrudeStepsBar m_wndExtrude;
    CMeasureBar m_wndMeasureBar;
    CDialogBar m_wndLogoSmall;
    CHierarchyDlg m_wndHierarchy;
    CMacroRecordPlayback recorder;
    CMatLibTreeToolbar m_wndMatLib;
    ControlledExternalViewer _externalViewer;
    ExternalViewerConfig _externalCfg;
    CDlgViewerEnvironment _viewerEnvirom;
    CMATEditorPlugin _MAT;
    DlgManageScripts _dlgmanscr;
    DllDebuggerProvider _dbgprovider;
    CScriptConsoleBar _scriptConsole;
    CString _lastTypedRTM;
    DlgUVEditor _dlguveditor;

    bool laso;
//    CCalculator calcdlg;
    UINT asavetimer;
    bool hotreg;
    bool initphase; //program is in initphase
    bool uniformscale;
    bool movebynormals; 
    bool disableMenu;

    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMainFrame)
  public:
    virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual BOOL PreTranslateMessage(MSG *pMsg);
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	virtual HACCEL GetDefaultAccelerator();
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
	  void ShowMessageWindow(const char *caption, const char *text);
	  void ShowMessageWindow(int idcaption, const char *text);
	  void LoadHotkeys();
	  void SaveHotkeys();
	CHotKeyCfgDlg wHotKeyDlg;
    void DockBarRight(CControlBar& bar,CControlBar &relto,bool hide=false);
    void DockControlBar(CControlBar& bar,bool hide=false);
    void DockControlBar(CControlBar &dock, CControlBar &relto, int xrel, int yrel,bool hide=false);
    void RestartAutosaveTimer();
    void EnableWindowSpecial(CWnd *excluding, BOOL enable);
    void SetStatus(const char *text)
    {SetMessageText(text);}
    void UpdateXYZ(HVECTOR v);
    virtual ~CMainFrame();
    #ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
    #endif

    
  protected:  // control bar embedded members
    CStatusBar  m_wndStatusBar;
    CToolBar    m_wndToolBar;
    CToolBar	m_wndControlBar1;
    CToolBar	m_wndEditBar;
    CToolBar	m_wndSelOp;
    CToolBar    m_wndSourceControl;
	virtual BOOL LoadFrame(UINT nIDResource,
				DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE,
				CWnd* pParentWnd = NULL,
				CCreateContext* pContext = NULL);

    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CMainFrame)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnDestroy();
    afx_msg void OnViewStandard();
    afx_msg BOOL OnSelectTempMode(UINT id);
    afx_msg BOOL OnEditSelectMode(UINT id);
    afx_msg void OnUpdateEditSelectMode(CCmdUI* pCmdUI);
    afx_msg void OnEditLaso();
    afx_msg void OnUpdateEditLaso(CCmdUI* pCmdUI);
    afx_msg void OnFileOptions();
    afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
    afx_msg void OnStructureUpdatephase();
    afx_msg void OnClose();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnStructureMatchdirection();
    afx_msg LRESULT OnHotKey(WPARAM wParam,LPARAM lParam);
    afx_msg void OnToolsCalculator();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnQueryEndSession();
    afx_msg void OnEndSession(BOOL bEnding);
    afx_msg void OnUpdateTexlist();
    afx_msg void OnSelopChange(UINT id);
    afx_msg void OnUpdateSelop(CCmdUI* pCmdUI);
    afx_msg void OnPointsUniformscale();
    afx_msg void OnUpdatePointsUniformscale(CCmdUI* pCmdUI);
    afx_msg void OnFileOpen() ;	
    afx_msg LRESULT OnWmApp1000(WPARAM wParam,LPARAM lParam);
    afx_msg void OnFileExtendlicence();
    afx_msg void OnUpdateFileExtendlicence(CCmdUI* pCmdUI);
    afx_msg void OnShowNormals();
    afx_msg void OnUpdateShowNormals(CCmdUI* pCmdUI);
	afx_msg void OnPointsMovebynormals();
	afx_msg void OnUpdatePointsMovebynormals(CCmdUI* pCmdUI);
    afx_msg void OnFileHotkeys();
	afx_msg LRESULT OnChangeAccelTable(WPARAM wParam, LPARAM lParam);
    afx_msg void OnMacroRecordmacro();
    afx_msg void OnMacroPlaymacro();
    afx_msg void OnUpdateMacroRecordmacro(CCmdUI* pCmdUI);
    afx_msg void OnUpdateMacroPlaymacro(CCmdUI* pCmdUI);
    afx_msg LRESULT OnUpdateStatusMsg(WPARAM ,LPARAM );
    afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) ;
    afx_msg LRESULT OnEndCalculationSections(WPARAM ,LPARAM );
    afx_msg LRESULT OnCalculateSections(WPARAM, LPARAM);
    afx_msg void OnExtviewerStart();
    afx_msg void OnExtracommandsAttach();
    afx_msg void OnExtracommandsDetach();
    afx_msg void OnExtViewerNotRunning(CCmdUI *pCmdUI);
    afx_msg void OnExtViewerRunning(CCmdUI *pCmdUI);
    afx_msg void OnExtracommandsStop();
    afx_msg void OnExtracommandsStartscript();
    afx_msg void OnExtracommandsRefresh();
    afx_msg void OnUpdateExtViewerDisplayFlags(CCmdUI *pCmdUI);
    afx_msg void OnExtViewerDisplayFlags(UINT cmd);
    afx_msg void OnUpdateExtViewerSelectMask(CCmdUI *pCmdUI);
    afx_msg void OnExtViewerSelectMask(UINT cmd);
    afx_msg void OnUpdateExtViewerCenter(CCmdUI *pCmdUI);
    afx_msg void OnExtViewerCenter(UINT cmd);
    afx_msg void OnExtViewerMenuFor(UINT cmd);
    afx_msg void OnExtViewerSetTexMat(UINT cmd);
    afx_msg void OnExtViewerSetWeather();
    afx_msg void OnExtracommandsReloadall();
    afx_msg void OnUpdatePluginCommand(CCmdUI *pCmdUI);
    afx_msg void OnPluginCommand(UINT cmd);
    afx_msg void OnScriptsRunMenuScript(UINT cmd);
    afx_msg LPARAM OnMessageBgrYield(WPARAM wParam,LPARAM lParam);


	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
public:
  bool StoreViewerConfig(void);
  bool LoadViewerConfig(void);
  bool ExternalError(ExternalViewer::ErrorEnum error);
  void OpenOptions();
  void RunScript(const char *script, bool settings=false, HWND hWnd=0);
  void ScriptRegMaintance();
  void StartExternal();

  HMENU  AddSubMenu(int where, const char *menuName);
  bool AddMenuCommand(HMENU subMenu, const char *name, IPluginCommand *cmd);
  bool AddMenuCommand(int where, const char *name, IPluginCommand *cmd);

  afx_msg void OnManagescripts();
  afx_msg void OnScriptsEditscript();
  afx_msg void OnScriptsRunscript();
  afx_msg void OnScriptsConsolefont();
  afx_msg void OnExviewerGroundfile();
  afx_msg void OnExviewerGrounddefault(UINT cmd);
  afx_msg void OnExtViewerLoadRTM();
  afx_msg void OnExtViewerClearRTM();
  afx_msg void OnUpdateExtViewerLoadRTM(CCmdUI *pCmdUI);
  afx_msg void OnUpdateExtViewerClearRTM(CCmdUI *pCmdUI);
  afx_msg void OnUpdateExviewerGrounddefault(CCmdUI *pCmdUI);
  afx_msg LRESULT OnOpenRVMATRequest(WPARAM wParam, LPARAM lParam);
  afx_msg void OnExtviewerMarkerbrowse();
  afx_msg LRESULT OnExViewerUpdateStatus(WPARAM,LPARAM);
  void ChangeViewerStatus(int status) {m_wndSplitter.ChangeStatus(status);}

  afx_msg void OnOtherStartnewinstance();
//  afx_msg void OnSourcecontrolEnabled();
//  afx_msg void OnUpdateSourcecontrolEnabled(CCmdUI *pCmdUI);
//  afx_msg void OnSourcecontrolEnabled();
  afx_msg void OnUvsetsUveditor();
  afx_msg void OnMacroLoadmacro();
  afx_msg void OnMacroSavelastmacroas();
  afx_msg void OnPlaymacroForeachlod(UINT cmd);
  afx_msg void OnPlaymacroForeachanimation();
  afx_msg void OnSelectionmaskingInvertselection();
  afx_msg void OnUpdateSelectionmaskingInvertselection(CCmdUI *pCmdUI);
};

extern CMainFrame *frame;


TypeIsMovable(Ref<IPluginCommand> )

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__6F94A438_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
