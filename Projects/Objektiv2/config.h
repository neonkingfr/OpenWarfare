#if !defined(AFX_CONFIG_H__64707317_B0BF_4B57_91BC_8C0F300C1D2A__INCLUDED_)
#define AFX_CONFIG_H__64707317_B0BF_4B57_91BC_8C0F300C1D2A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Config.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CConfig dialog

#define CFG_VALUESCOUNT 39
#define CFG_VALUENAMES IDS_CFGEXTERNARVIEWER

class CConfig : public CDialog
  {
  // Construction
  CSize initsize;
  WString values[CFG_VALUESCOUNT];
  int savept;


  public:
    void SaveCurrPath(int pathid, const char *fname);
    const char *FillOpenDialogByPath(int pathid,OPENFILENAME &ofn, const char *deffilename);
    unsigned long GetHex(int idc, unsigned long defval);
    const char * GetString(int idsname);
    int GetInt(int idsname,int defval=0);
    bool GetBool(int idsname);
    const char * AskForFile(const char *prewname, const char *filter=NULL);
    void LoadConfig();
    void SaveConfig();
    CConfig(CWnd* pParent = NULL);   // standard constructor
    void SetString(int id, const char *str, bool regsave=false);
    
    // Dialog Data
    //{{AFX_DATA(CConfig)
    enum 
      { IDD = IDD_CONFIG };
    CListCtrl	List;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CConfig)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void SaveText();
    char GetType(int i,int *param=NULL);
    void AskSS(int pt);
    
    // Generated message map functions
    //{{AFX_MSG(CConfig)
    afx_msg void OnSize(UINT nType, int cx, int cy);
    virtual BOOL OnInitDialog();
    afx_msg void OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnOK();
    virtual void OnCancel();
    afx_msg void OnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnChangeValue();
    afx_msg void OnKillfocusValue();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIG_H__64707317_B0BF_4B57_91BC_8C0F300C1D2A__INCLUDED_)
