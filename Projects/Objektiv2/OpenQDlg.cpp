// OpenQDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "OpenQDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COpenQDlg dialog


COpenQDlg::COpenQDlg(CWnd* pParent /*=NULL*/)
  : CDialog(COpenQDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(COpenQDlg)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void COpenQDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(COpenQDlg)
  // NOTE: the ClassWizard will add DDX and DDV calls here
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(COpenQDlg, CDialog)
  //{{AFX_MSG_MAP(COpenQDlg)
  // NOTE: the ClassWizard will add message map macros here
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // COpenQDlg message handlers
    
    BOOL COpenQDlg::OnCommand(WPARAM wParam, LPARAM lParam) 
      {
      EndDialog(wParam);
      return TRUE;
      }

//--------------------------------------------------

