; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMainFrame
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "objektiv2.h"
LastPage=0

ClassCount=75
Class1=C3dsExport
Class2=CAllRenameDlg
Class3=CAnimList
Class4=CAnimListBar
Class5=CASFImportDlg
Class6=CBevelDlg
Class7=CBIANMImportDlg
Class8=CBVHImportDlg
Class9=CCalculator
Class10=CColorizeDlg
Class11=CColorSlider
Class12=CConfig
Class13=CConsole
Class14=CCopyWeightsDlg
Class15=CCreatePrimDlg
Class16=CDlgNormalDirection
Class17=CDlgTexelMeasure
Class18=CEditPropDlg
Class19=CExtrudeStepsBar
Class20=CFaceProp
Class21=CFlattenPointsDlg
Class22=CGizmoMapDlg
Class23=CGizmoTexWnd
Class24=CHierarchyDlg
Class25=CHistoryBar
Class26=CHotKeyCfgDlg
Class27=CImport3DS
Class28=CImport3dsTexConvDlg
Class29=CImportSDADlg
Class30=CInternalView
Class31=CInternalViewer
Class32=CLicenceAgrDlg
Class33=CLinWeightDlg
Class34=CLodConfig
Class35=CLodList
Class36=CMainFrame
Class37=CMapDlg
Class38=CMassBar
Class39=CMatchDirection
Class40=CMeasureBar
Class41=CMenuBar
Class42=CMergeNearDlg
Class43=CMessageWnd
Class44=CMultPinCullDlg
Class45=CNamedPropDlg
Class46=CNormAnimDlg
Class47=CObjektiv2App
Class48=CAboutDlg
Class49=CObjektiv2Doc
Class50=CObjektiv2View
Class51=COpenQDlg
Class52=CPaintSelColorDlg
Class53=CProxyNameDlg
Class54=CPtInfoBar
Class55=CSelectionList
Class56=CSelectionNameDlg
Class57=CSoftenDlg
Class58=CSpaceWarpDlg
Class59=CSplashWin
Class60=CSplitHoleDlg
Class61=CTexList
Class62=CTexMappingDlg
Class63=CTexPrewWin
Class64=CTimeRangeDlg
Class65=CTransform2D
Class66=CTransformDlg
Class67=CUnlockSoftDlg
Class68=CUnWrapDlg
Class69=CVertexProp
Class70=CVyberLodDlg
Class71=CWeightAutoDlg
Class72=CWeights

ResourceCount=87
Resource1=IDD_UNWRAP (English (U.S.))
Resource2=IDD_CALCULATOR_INITVARS (English (U.S.) - FULLVER)
Resource3=IDD_BVHIMPORT
Resource4=IDD_EXPORT3DS (English (U.S.))
Resource5=IDD_EDITPROPERTY
Resource6=IDR_TEXTURE_POPUP (English (U.S.))
Resource7=IDD_CALCULATOR_DIALOG (English (U.S.) - FULLVER)
Resource8=IDD_CYLINDRICMAPPING (English (U.S.))
Resource9=IDD_CALCULATOR (English (U.S.) - FULLVER)
Resource10=IDD_SPLITHOLE (English (U.S.))
Resource11=IDD_MACROMANAGER
Resource12=IDD_VERTEX_PROPERTIES (English (U.S.))
Resource13=IDD_LOGOSMALL (English (U.S.))
Resource14=IDD_PAINTSELCOLOR (FULLVER)
Resource15=IDD_COLORS (English (U.S.))
Resource16=IDR_MAINFRAME (English (U.S.))
Resource17=IDD_NAMEDPROPERTY
Resource18=IDD_SOFTENANIM (English (U.S.))
Resource19=IDR_EDITBAR (English (U.S.))
Resource20=IDD_MESSAGEWND
Resource21=IDR_GIZMOMENU
Resource22=IDR_SELMENU_POPUP (English (U.S.))
Resource23=IDD_TRANSFORM (English (U.S.))
Resource24=IDR_INTVIEW_POPUP (English (U.S.) - FULLVER)
Resource25=IDR_ANIMMENU_POPUP (English (U.S.) - FULLVER)
Resource26=IDR_HIERARCHY_POPUP (English (U.S.) - FULLVER)
Resource27=IDD_CONSOLE
Resource28=IDD_CALCULATOR_MODIFY (English (U.S.) - FULLVER)
Resource29=IDD_BEVEL
Resource30=IDD_HOTKEYCONFIG
Resource31=IDR_CONTROL1 (English (U.S.))
Resource32=IDD_INPROGRESS (English (U.S.))
Resource33=IDD_LINWEIGHTSEL
Resource34=IDD_READONLY
Resource35=IDD_COPYWEIGHTS (FULLVER)
Resource36=IDD_UNLOCKSOFT
Resource37=IDR_NAMPROP_POPUP
Resource38=IDD_TEXLIST (FULLVER)
Resource39=IDD_IMPORT3DS (FULLVER)
Resource40=IDD_NORMANIM (FULLVER)
Resource41=IDD_GIZMOMAPPING
Resource42=IDD_SELECTSELECTION
Resource43=IDR_EDITBAR (English (U.S.) - FULLVER)
Resource44=IDD_LICENCEAGR
Resource45=IDD_PROXYNAME (English (U.S.))
Resource46=IDD_TIMERANGE (English (U.S.))
Resource47=IDD_VYBERLOD
Resource48=IDD_MAPDLG (English (U.S.))
Resource49=IDD_CREATE_PRIMITIVE (English (U.S.))
Resource50=IDD_TEXELMEASURE
Resource51=IDD_NORMALDIRECTION
Resource52=IDD_NORMALIZESTEP
Resource53=IDD_SELECTIONNAME
Resource54=IDD_ABOUTBOX (English (U.S.))
Resource55=IDD_ALLTEXTURES
Resource56=IDD_SURFACEDATA (FULLVER)
Resource57=IDD_FLATTENPOINTS
Resource58=IDD_LODLIST (English (U.S.))
Resource59=IDD_HIERARCHY (English (U.S.) - FULLVER)
Resource60=IDD_WEIGHTAUTOMATION
Resource61=IDD_SELLIST (English (U.S.))
Resource62=IDD_2DTRANSFORM (English (U.S.))
Resource63=IDD_PTINFO (English (U.S.))
Resource64=IDD_ASFAMCIMPORT
Resource65=IDR_SELOPERATIONS (English (U.S.) - FULLVER)
Resource66=IDD_MERGENEAR (English (U.S.))
Resource67=IDD_CONFIG (English (U.S.))
Resource68=IDR_HISTORY_POPUP (English (U.S.))
Resource69=IDD_BIANMIMPORTDLG
Resource70=IDD_WEIGHTS (English (U.S.))
Resource71=IDD_MULTPINCULL (English (U.S.))
Resource72=IDD_HISTORY (English (U.S.))
Resource73=IDD_SPACEWARP (English (U.S.) - FULLVER)
Resource74=IDD_FACE_PROPERTIES (English (U.S.))
Resource75=IDR_TEXTURE_POPUP (English (U.S.) - FULLVER)
Resource76=IDD_MATCHDIRECTION (English (U.S.) - FULLVER)
Resource77=IDD_MEASURING (English (U.S.) - FULLVER)
Resource78=IDR_LODMENU_POPUP (English (U.S.))
Resource79=IDD_EXTRUDESTEPS (English (U.S.) - FULLVER)
Resource80=IDD_IMPORT3DSTEXCONV
Resource81=IDD_TEXLIST
Resource82=IDD_LODCONFIG (English (U.S.))
Resource83=IDD_ANIMLIST (English (U.S.) - FULLVER)
Resource84=IDD_OPENQ (English (U.S.))
Class73=CDlgReadOnly
Resource85=IDD_IMPORT3DS
Resource86=IDD_MASSBAR (English (U.S.))
Class74=CNormalizeStepDlg
Class75=CSelectSelectionDlg
Resource87=IDR_MAINFRAME (English (U.S.) - FULLVER)

[CLS:C3dsExport]
Type=0
BaseClass=CDialog
HeaderFile=3dsExport.h
ImplementationFile=3dsExport.cpp
Filter=D
VirtualFilter=dWC
LastObject=C3dsExport

[CLS:CAllRenameDlg]
Type=0
BaseClass=CDialog
HeaderFile=AllRenameDlg.h
ImplementationFile=AllRenameDlg.cpp

[CLS:CAnimList]
Type=0
BaseClass=CDialog
HeaderFile=AnimList.h
ImplementationFile=AnimList.cpp

[CLS:CAnimListBar]
Type=0
BaseClass=CDialogBar
HeaderFile=AnimListBar.h
ImplementationFile=AnimListBar.cpp

[CLS:CASFImportDlg]
Type=0
BaseClass=CDialog
HeaderFile=ASFImportDlg.h
ImplementationFile=ASFImportDlg.cpp

[CLS:CBevelDlg]
Type=0
BaseClass=CDialog
HeaderFile=BevelDlg.h
ImplementationFile=BevelDlg.cpp

[CLS:CBIANMImportDlg]
Type=0
BaseClass=CDialog
HeaderFile=BIANMImportDlg.h
ImplementationFile=BIANMImportDlg.cpp

[CLS:CBVHImportDlg]
Type=0
BaseClass=CDialog
HeaderFile=BVHImportDlg.h
ImplementationFile=BVHImportDlg.cpp

[CLS:CCalculator]
Type=0
BaseClass=CDialog
HeaderFile=calculator.h
ImplementationFile=calculator.cpp

[CLS:CColorizeDlg]
Type=0
BaseClass=CDialog
HeaderFile=ColorizeDlg.h
ImplementationFile=ColorizeDlg.cpp

[CLS:CColorSlider]
Type=0
BaseClass=CWnd
HeaderFile=ColorSlider.h
ImplementationFile=ColorSlider.cpp

[CLS:CConfig]
Type=0
BaseClass=CDialog
HeaderFile=config.h
ImplementationFile=config.cpp

[CLS:CConsole]
Type=0
BaseClass=CDialog
HeaderFile=Console.h
ImplementationFile=Console.cpp
Filter=D
VirtualFilter=dWC

[CLS:CCopyWeightsDlg]
Type=0
BaseClass=CDialog
HeaderFile=CopyWeightsDlg.h
ImplementationFile=CopyWeightsDlg.cpp

[CLS:CCreatePrimDlg]
Type=0
BaseClass=CDialog
HeaderFile=CreatePrimDlg.h
ImplementationFile=CreatePrimDlg.cpp

[CLS:CDlgNormalDirection]
Type=0
BaseClass=CDialog
HeaderFile=DlgNormalDirection.h
ImplementationFile=DlgNormalDirection.cpp

[CLS:CDlgTexelMeasure]
Type=0
BaseClass=CDialog
HeaderFile=DlgTexelMeasure.h
ImplementationFile=DlgTexelMeasure.cpp

[CLS:CEditPropDlg]
Type=0
BaseClass=CDialog
HeaderFile=editpropdlg.h
ImplementationFile=editpropdlg.cpp

[CLS:CExtrudeStepsBar]
Type=0
BaseClass=CDialogBar
HeaderFile=ExtrudeStepsBar.h
ImplementationFile=ExtrudeStepsBar.cpp

[CLS:CFaceProp]
Type=0
BaseClass=CDialog
HeaderFile=FaceProp.h
ImplementationFile=FaceProp.cpp

[CLS:CFlattenPointsDlg]
Type=0
BaseClass=CDialog
HeaderFile=FlattenPointsDlg.h
ImplementationFile=FlattenPointsDlg.cpp

[CLS:CGizmoMapDlg]
Type=0
BaseClass=CDialog
HeaderFile=GizmoMapDlg.h
ImplementationFile=GizmoMapDlg.cpp

[CLS:CGizmoTexWnd]
Type=0
BaseClass=CStatic
HeaderFile=GizmoTexWnd.h
ImplementationFile=GizmoTexWnd.cpp

[CLS:CHierarchyDlg]
Type=0
BaseClass=CDialogBar
HeaderFile=HierarchyDlg.h
ImplementationFile=HierarchyDlg.cpp

[CLS:CHistoryBar]
Type=0
BaseClass=CDialogBar
HeaderFile=historybar.h
ImplementationFile=historybar.cpp

[CLS:CHotKeyCfgDlg]
Type=0
BaseClass=CDialog
HeaderFile=hotkeycfgdlg.h
ImplementationFile=hotkeycfgdlg.cpp

[CLS:CImport3DS]
Type=0
HeaderFile=Import3DS.h
ImplementationFile=Import3DS.cpp
LastObject=CImport3DS

[CLS:CImport3dsTexConvDlg]
Type=0
BaseClass=CDialog
HeaderFile=Import3dsTexConvDlg.h
ImplementationFile=Import3dsTexConvDlg.cpp

[CLS:CImportSDADlg]
Type=0
BaseClass=CDialog
HeaderFile=ImportSDADlg.h
ImplementationFile=ImportSDADlg.cpp

[CLS:CInternalView]
Type=0
BaseClass=CWnd
HeaderFile=internalview.h
ImplementationFile=internalview.cpp

[CLS:CInternalViewer]
Type=0
BaseClass=CView
HeaderFile=InternalViewer.h
ImplementationFile=InternalViewer.cpp

[CLS:CLicenceAgrDlg]
Type=0
BaseClass=CDialog
HeaderFile=LicenceAgrDlg.h
ImplementationFile=LicenceAgrDlg.cpp

[CLS:CLinWeightDlg]
Type=0
BaseClass=CDialog
HeaderFile=LinWeightDlg.h
ImplementationFile=LinWeightDlg.cpp

[CLS:CLodConfig]
Type=0
BaseClass=CDialog
HeaderFile=LodConfig.h
ImplementationFile=lodconfig.cpp

[CLS:CLodList]
Type=0
BaseClass=CDialogBar
HeaderFile=LodList.h
ImplementationFile=lodlist.cpp

[CLS:CMainFrame]
Type=0
BaseClass=CFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
LastObject=CMainFrame
Filter=T
VirtualFilter=fWC

[CLS:CMapDlg]
Type=0
BaseClass=CDialog
HeaderFile=MapDlg.h
ImplementationFile=MapDlg.cpp

[CLS:CMassBar]
Type=0
BaseClass=CDialogBar
HeaderFile=MassBar.h
ImplementationFile=MassBar.cpp
LastObject=CMassBar
Filter=D
VirtualFilter=dWC

[CLS:CMatchDirection]
Type=0
BaseClass=CDialog
HeaderFile=MatchDirection.h
ImplementationFile=MatchDirection.cpp

[CLS:CMeasureBar]
Type=0
BaseClass=CDialogBar
HeaderFile=MeasureBar.h
ImplementationFile=MeasureBar.cpp

[CLS:CMenuBar]
Type=0
BaseClass=CDialogBar
HeaderFile=MenuBar.h
ImplementationFile=MenuBar.cpp

[CLS:CMergeNearDlg]
Type=0
BaseClass=CDialog
HeaderFile=MergeNearDlg.h
ImplementationFile=MergeNearDlg.cpp

[CLS:CMessageWnd]
Type=0
BaseClass=CDialog
HeaderFile=MessageWnd.h
ImplementationFile=MessageWnd.cpp

[CLS:CMultPinCullDlg]
Type=0
BaseClass=CDialog
HeaderFile=MultPinCullDlg.h
ImplementationFile=MultPinCullDlg.cpp

[CLS:CNamedPropDlg]
Type=0
BaseClass=CDialogBar
HeaderFile=namedpropdlg.h
ImplementationFile=namedpropdlg.cpp

[CLS:CNormAnimDlg]
Type=0
BaseClass=CDialog
HeaderFile=NormAnimDlg.h
ImplementationFile=NormAnimDlg.cpp

[CLS:CObjektiv2App]
Type=0
BaseClass=CWinApp
HeaderFile=objektiv2.h
ImplementationFile=objektiv2.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=objektiv2.cpp
ImplementationFile=objektiv2.cpp

[CLS:CObjektiv2Doc]
Type=0
BaseClass=CDocument
HeaderFile=Objektiv2Doc.h
ImplementationFile=Objektiv2Doc.cpp
LastObject=CObjektiv2Doc

[CLS:CObjektiv2View]
Type=0
BaseClass=CView
HeaderFile=Objektiv2View.h
ImplementationFile=Objektiv2View.cpp
LastObject=ID_STRUCTURE_CHECKTEXTURES

[CLS:COpenQDlg]
Type=0
BaseClass=CDialog
HeaderFile=OpenQDlg.h
ImplementationFile=OpenQDlg.cpp

[CLS:CPaintSelColorDlg]
Type=0
BaseClass=CDialog
HeaderFile=PaintSelColorDlg.h
ImplementationFile=PaintSelColorDlg.cpp

[CLS:CProxyNameDlg]
Type=0
BaseClass=CDialog
HeaderFile=ProxyNameDlg.h
ImplementationFile=ProxyNameDlg.cpp

[CLS:CPtInfoBar]
Type=0
BaseClass=CDialogBar
HeaderFile=ptinfobar.h
ImplementationFile=ptinfobar.cpp

[CLS:CSelectionList]
Type=0
BaseClass=CDialogBar
HeaderFile=selectionlist.h
ImplementationFile=selectionlist.cpp

[CLS:CSelectionNameDlg]
Type=0
BaseClass=CDialog
HeaderFile=SelectionNameDlg.h
ImplementationFile=SelectionNameDlg.cpp

[CLS:CSoftenDlg]
Type=0
BaseClass=CDialog
HeaderFile=SoftenDlg.h
ImplementationFile=SoftenDlg.cpp

[CLS:CSpaceWarpDlg]
Type=0
BaseClass=CDialog
HeaderFile=SpaceWarpDlg.h
ImplementationFile=SpaceWarpDlg.cpp

[CLS:CSplashWin]
Type=0
BaseClass=CWnd
HeaderFile=SplashWin.h
ImplementationFile=SplashWin.cpp

[CLS:CSplitHoleDlg]
Type=0
BaseClass=CDialog
HeaderFile=SplitHoleDlg.h
ImplementationFile=SplitHoleDlg.cpp

[CLS:CTexList]
Type=0
BaseClass=CDialogBar
HeaderFile=texlist.h
ImplementationFile=texlist.cpp

[CLS:CTexMappingDlg]
Type=0
BaseClass=CDialog
HeaderFile=texmappingdlg.h
ImplementationFile=texmappingdlg.cpp

[CLS:CTexPrewWin]
Type=0
BaseClass=CWnd
HeaderFile=texprewwin.h
ImplementationFile=texprewwin.cpp

[CLS:CTimeRangeDlg]
Type=0
BaseClass=CDialog
HeaderFile=TimeRangeDlg.h
ImplementationFile=TimeRangeDlg.cpp

[CLS:CTransform2D]
Type=0
BaseClass=CDialog
HeaderFile=Transform2D.h
ImplementationFile=Transform2D.cpp

[CLS:CTransformDlg]
Type=0
BaseClass=CDialog
HeaderFile=TransformDlg.h
ImplementationFile=TransformDlg.cpp

[CLS:CUnlockSoftDlg]
Type=0
BaseClass=CDialog
HeaderFile=UnlockSoftDlg.h
ImplementationFile=UnlockSoftDlg.cpp

[CLS:CUnWrapDlg]
Type=0
BaseClass=CDialog
HeaderFile=UnWrapDlg.h
ImplementationFile=UnWrapDlg.cpp

[CLS:CVertexProp]
Type=0
BaseClass=CDialog
HeaderFile=VertexProp.h
ImplementationFile=VertexProp.cpp

[CLS:CVyberLodDlg]
Type=0
BaseClass=CDialog
HeaderFile=VyberLodDlg.h
ImplementationFile=VyberLodDlg.cpp

[CLS:CWeightAutoDlg]
Type=0
BaseClass=CDialog
HeaderFile=WeightAutoDlg.h
ImplementationFile=WeightAutoDlg.cpp

[CLS:CWeights]
Type=0
BaseClass=CDialog
HeaderFile=Weights.h
ImplementationFile=Weights.cpp

[DLG:IDD_ALLTEXTURES]
Type=1
Class=CAllRenameDlg
ControlCount=8
Control1=IDC_TEXTURELIST,listbox,1344340307
Control2=IDC_OLDNAMEMASK_DESC,static,1342308352
Control3=IDC_OLDNAMEMASK,edit,1350631552
Control4=IDC_NEWNAMEMASK_DESC,static,1342308352
Control5=IDC_NEWNAMEMASK,edit,1350631552
Control6=IDOK,button,1342242817
Control7=IDCANCEL,button,1342242816
Control8=IDC_UNDO,button,1342242816

[DLG:IDD_BIANMIMPORTDLG]
Type=1
Class=CBIANMImportDlg
ControlCount=7
Control1=IDC_SCALE_STATIC,static,1342308352
Control2=IDC_SCALE,edit,1350631552
Control3=IDC_INVERTX,button,1342242819
Control4=IDC_INVERTY,button,1342242819
Control5=IDC_INVERTZ,button,1342242819
Control6=IDOK,button,1342242817
Control7=IDCANCEL,button,1342242816

[DLG:IDD_HOTKEYCONFIG]
Type=1
Class=CHotKeyCfgDlg
ControlCount=12
Control1=IDC_STATIC,static,1342177294
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308864
Control4=IDC_STATIC,static,1342308864
Control5=IDC_HOTKEY,combobox,1344339971
Control6=IDC_FSHIFT,button,1342242819
Control7=IDC_FCONTROL,button,1342242819
Control8=IDC_FALT,button,1342242819
Control9=IDOK,button,1476460545
Control10=IDC_RESET,button,1342242816
Control11=IDCANCEL,button,1342242816
Control12=IDC_ITEMNAME,static,1342308864

[DLG:IDD_IMPORT3DS]
Type=1
Class=CImport3DS
ControlCount=16
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_OBJECTLIST,SysListView32,1350664201
Control4=IDC_STATIC,button,1342177287
Control5=IDC_SETLOD,button,1476460544
Control6=IDC_STATIC,static,1342308353
Control7=IDC_STATIC,static,1342308353
Control8=IDC_SCALEMAG,edit,1350639617
Control9=IDC_SCALEMIN,edit,1350639617
Control10=IDC_CONTEX,button,1342255875
Control11=IDC_STATIC,button,1342177287
Control12=IDC_STATICTEXPREFIX,static,1342308864
Control13=IDC_PREFIX,edit,1350631552
Control14=IDC_STATICTEXPATH,static,1342308864
Control15=IDC_PATH,edit,1350631552
Control16=IDC_MATNAME,button,1342242819

[DLG:IDD_OPENQ]
Type=1
Class=COpenQDlg

[MNU:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MERGE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_SEND
Command7=ID_FILE_IMPORT3DS
Command8=ID_FILE_OPTIONS
Command9=ID_FILE_EXTENDLICENCE
Command10=ID_FILE_MRU_FILE1
Command11=ID_APP_EXIT
Command12=ID_EDIT_UNDO
Command13=ID_EDIT_REDO
Command14=ID_EDIT_CUT
Command15=ID_EDIT_COPY
Command16=ID_EDIT_PASTE
Command17=ID_EDIT_DELETE
Command18=ID_EDIT_SELECT_ALL
Command19=ID_EDIT_DESELECT
Command20=ID_EDIT_INVERTSELECTION
Command21=ID_EDIT_SELECTOBJECTS
Command22=ID_EDIT_SELECTFACES
Command23=ID_EDIT_SELECTVERTICES
Command24=ID_EDIT_PIN
Command25=ID_SURFACES_BACKGROUNDTEXTURE
Command26=ID_EDIT_LASO
Command27=ID_EDIT_EDITMODES_TOUCHFACES
Command28=ID_VIEW_SELLOCKED
Command29=ID_EDIT_SELHIDDEN
Command30=ID_EDIT_SELECTONESIDE
Command31=ID_EDIT_LINEARDEFORM
Command32=ID_EDIT_CENTERPIN
Command33=ID_EDIT_LOCKPIN
Command34=ID_CREATE_PLANE
Command35=ID_CREATE_BOX
Command36=ID_CREATE_CIRCLE
Command37=ID_CREATE_SPEHERE
Command38=ID_CREATE_CYLINDER
Command39=ID_CREATE_FACE
Command40=ID_CREATE_PROXY
Command41=ID_VIEW_FRONT
Command42=ID_VIEW_TOP
Command43=ID_VIEW_LEFT
Command44=ID_VIEW_PROJECTION
Command45=ID_VIEW_RIGHT
Command46=ID_VIEW_BACK
Command47=ID_VIEW_BOTTOM
Command48=ID_VIEW_LOOKATFACE
Command49=ID_VIEW_ZOOM
Command50=ID_VIEW_ZOOMIN
Command51=ID_VIEW_ZOOMOUT
Command52=ID_VIEW_PAGEZOOM
Command53=ID_VIEW_PAGEZOOMSEL
Command54=ID_VIEW_CENTERZOOM_AUTO
Command55=ID_VIEW_SOLIDFILL
Command56=ID_VIEW_FACECULL
Command57=ID_VIEW_GRID
Command58=ID_VIEW_WIRESINSOLID
Command59=ID_VIEW_RESTARTEXTERNAL
Command60=ID_VIEW_VIEWERS_FOCUSSELECTIONS
Command61=ID_LOCK_XY
Command62=ID_LOCK_XZ
Command63=ID_LOCK_YZ
Command64=ID_LOCK_X
Command65=ID_LOCK_Y
Command66=ID_LOCK_Z
Command67=ID_LOCK_ROTATION
Command68=ID_VIEW_PINCAMER
Command69=ID_VIEW_HIDESEL
Command70=ID_VIEW_UNHIDESEL
Command71=ID_VIEW_LOCKSEL
Command72=ID_VIEW_UNLOCK
Command73=ID_VIEW_COLORIZEOBJECTS
Command74=ID_VIEW_REFRESH
Command75=ID_STRUCTURE_OPTIMIZE
Command76=ID_STRUCTURE_CHECKFACES
Command77=ID_STRUCTURE_CENTERALL
Command78=ID_STRUCTURE_TOPOLOGY_FINDNONCLOSED
Command79=ID_STRUCTURE_TOPOLOGY_CLOSE
Command80=ID_STRUCTURE_TOPOLOGY_FINDCOMPONENTS
Command81=ID_STRUCTURE_TOPOLOGY_SPLIT
Command82=ID_STRUCTURE_CONVEXITY_FINDNONCONVEXITIES
Command83=ID_STRUCTURE_CONVEXITY_CONVEXHULL
Command84=ID_STRUCTURE_CONVEXITY_COMPONENTCONVEXHULL
Command85=ID_STRUCTURE_TRIANGULATE
Command86=ID_STRUCTURE_TRIANGULATE2
Command87=ID_STRUCTURE_SQARIZE
Command88=ID_STRUCTURE_PINCULL
Command89=ID_STRUCTURE_MULTIPLEPINCULL
Command90=ID_POINTS_FIRST
Command91=ID_POINTS_NEXT
Command92=ID_POINTS_PREV
Command93=ID_STRUCTURE_FINDISOLATEDPOINTS
Command94=ID_TRANSF_MOVE
Command95=ID_TRANSF_ROTATE
Command96=ID_TRANSF_SCALE
Command97=ID_TRANS2_ROTATE
Command98=ID_TRANS2_SCALE
Command99=ID_TRANS2_MIRRORX
Command100=ID_TRANS2_MIRRORY
Command101=ID_POINTS_MERGE
Command102=ID_POINTS_MERGENEAR
Command103=ID_POINTS_FLATTEN
Command104=ID_POINTS_UNIFORMSCALE
Command105=ID_POINTS_PROPERTIES
Command106=ID_CREATE_TRIANGLE
Command107=ID_CREATE_QUAD
Command108=ID_CREATE_STRIP
Command109=ID_CREATE_FAN
Command110=ID_FACES_REVERSE
Command111=ID_FACES_UNCROSS
Command112=ID_FACES_REMOVE
Command113=ID_FACES_SPLITHOLE
Command114=ID_FACES_MOVETOP
Command115=ID_FACES_MOVEBOTTOM
Command116=ID_FACES_MOVETONEXTALPHA
Command117=ID_FACES_MOVETOPREVALPHA
Command118=ID_SURFACES_RECALCULATENORMALS
Command119=ID_FACE_PROPERTIES
Command120=ID_SURFACES_SHARPEDGE
Command121=ID_SURFACES_SMOOTHEDGE
Command122=ID_SURFACES_GIZMOMAPPING
Command123=ID_SURFACES_BACKGROUNDTEXTURE
Command124=ID_SURFACES_BACKGROUNDMAPPING
Command125=ID_SURFACES_UNWRAP
Command126=ID_SURFACES_BGRFROMFACE
Command127=ID_SURFACES_BGRFROMSEL
Command128=ID_SURFACES_SLUCOVANI
Command129=ID_VIEW_STANDARD
Command130=ID_WINDOW_SPLIT
Command131=ID_VIEW_STATUS_BAR
Command132=ID_VIEW_TOOLBAR
Command133=ID_VIEW_CONTROL1
Command134=ID_VIEW_EDITBAR
Command135=ID_VIEW_NAMEDPROPERTIES
Command136=ID_VIEW_SELLIST
Command137=ID_VIEW_TEXLIST
Command138=ID_VIEW_LODLIST
Command139=ID_VIEW_PTINFOBAR
Command140=ID_VIEW_HISTORY
Command141=ID_WINDOW_MASS
Command142=ID_APP_ABOUT
CommandCount=142

[DLG:IDD_NAMEDPROPERTY]
Type=1
Class=?
ControlCount=1
Control1=IDC_LIST1,SysListView32,1342242881

[DLG:IDD_EDITPROPERTY]
Type=1
Class=?
ControlCount=6
Control1=IDC_STATIC,static,1342308352
Control2=IDC_NAME,edit,1350631552
Control3=IDC_STATIC,static,1342308352
Control4=IDC_VALUE,edit,1350631552
Control5=IDOK,button,1342242817
Control6=IDCANCEL,button,1342242816

[DLG:IDD_TEXLIST]
Type=1
Class=?
ControlCount=7
Control1=IDC_PROGRESS,msctls_progress32,1073741825
Control2=IDC_TEXLIST,listbox,1352730963
Control3=IDC_PATH,button,1073807360
Control4=IDC_LIBRARY,combobox,1075904770
Control5=IDC_CUT,button,1073807360
Control6=IDC_PASTE,button,1073807360
Control7=IDC_PREVIEW,button,1342246915

[DLG:IDD_FLATTENPOINTS]
Type=1
Class=?
ControlCount=8
Control1=IDOK,button,1342373889
Control2=IDCANCEL,button,1342373888
Control3=IDC_RADIO1,button,1342308361
Control4=IDC_RADIO2,button,1342177289
Control5=IDC_RADIO3,button,1342177289
Control6=IDC_RADIO4,button,1342177289
Control7=IDC_RADIO5,button,1476395017
Control8=IDC_CHECK1,button,1342242819

[DLG:IDD_MESSAGEWND]
Type=1
Class=?
ControlCount=1
Control1=IDC_TEXT,static,1342308353

[DLG:IDD_SURFACEDATA (FULLVER)]
Type=1
Class=?
ControlCount=19
Control1=IDC_LIST,SysListView32,1350797825
Control2=IDC_STATIC,button,1342308359
Control3=IDC_STATIC,static,1342308864
Control4=IDC_MIN,edit,1350762624
Control5=IDC_STATIC,static,1342308864
Control6=IDC_MAX,edit,1350762624
Control7=IDC_VXTYPE,button,1342373897
Control8=IDC_BXTYPE,button,1342177289
Control9=IDC_OXTYPE,button,1342177289
Control10=IDC_STATIC,static,1342308352
Control11=IDC_BXSIZE,edit,1350762625
Control12=IDC_SELECT,button,1342242816
Control13=IDC_STATIC,button,1342308359
Control14=IDOK,button,1342373889
Control15=IDCANCEL,button,1342373888
Control16=IDC_STATIC,button,1342177287
Control17=IDC_SCALE,combobox,1344339970
Control18=IDC_CREATPRIM,button,1342242816
Control19=IDC_DELETEPRIM,button,1342242816

[DLG:IDD_LINWEIGHTSEL]
Type=1
Class=?
ControlCount=7
Control1=IDCANCEL,button,1342242816
Control2=IDC_TOP,button,1342320649
Control3=IDC_RIGHT,button,1342189577
Control4=IDC_BOTTOM,button,1342189577
Control5=IDC_LEFT,button,1342189577
Control6=IDC_DRAWFRAME,static,1342308358
Control7=IDC_STATIC,static,1342308353

[DLG:IDD_COPYWEIGHTS (FULLVER)]
Type=1
Class=?
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308864
Control4=IDC_STATIC,static,1342308864
Control5=IDC_REFLOD,SysListView32,1350649935
Control6=IDC_SELLIST,SysListView32,1350649947

[DLG:IDD_IMPORT3DSTEXCONV]
Type=1
Class=?
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TODOLIST,SysListView32,1350664257
Control4=IDC_BROWSEPATH,button,1342242816
Control5=IDC_FORCECONV,button,1342242816

[DLG:IDD_PAINTSELCOLOR (FULLVER)]
Type=1
Class=?
ControlCount=8
Control1=IDC_STATIC,static,1342308864
Control2=IDC_COLORSLIDER,static,1342177554
Control3=IDC_STATIC,static,1342308864
Control4=IDC_SLIDER2,msctls_trackbar32,1342242840
Control5=IDC_STATIC,static,1342308864
Control6=IDC_SLIDER3,msctls_trackbar32,1342242840
Control7=IDC_STATIC,static,1342308864
Control8=IDC_SLIDER4,msctls_trackbar32,1342242840

[DLG:IDD_TEXLIST (FULLVER)]
Type=1
Class=?
ControlCount=7
Control1=IDC_PROGRESS,msctls_progress32,1073741825
Control2=IDC_TEXLIST,listbox,1352730963
Control3=IDC_PATH,button,1342242816
Control4=IDC_LIBRARY,combobox,1344340226
Control5=IDC_CUT,button,1342242816
Control6=IDC_PASTE,button,1342242816
Control7=IDC_PREVIEW,button,1342246915

[DLG:IDD_IMPORT3DS (FULLVER)]
Type=1
Class=CImport3DS
ControlCount=25
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_OBJECTLIST,SysListView32,1350664201
Control4=IDC_STATIC,button,1342177287
Control5=IDC_SETLOD,button,1476460544
Control6=IDC_STATIC,static,1342308353
Control7=IDC_STATIC,static,1342308353
Control8=IDC_SCALEMAG,edit,1350639617
Control9=IDC_SCALEMIN,edit,1350639617
Control10=IDC_STATIC,static,1342308353
Control11=IDC_STATIC,static,1342308353
Control12=IDC_FRAMEMIN,edit,1350631553
Control13=IDC_FRAMEMAX,edit,1350639745
Control14=IDC_STATIC,static,1342308353
Control15=IDC_FRAMEREPORT,static,1342308353
Control16=IDC_NOANIM,button,1342258179
Control17=IDC_CONTEX,button,1342255875
Control18=IDC_STATIC,button,1342177287
Control19=IDC_STATICTEXPREFIX,static,1342308864
Control20=IDC_PREFIX,edit,1350631552
Control21=IDC_STATICTEXPATH,static,1342308864
Control22=IDC_PATH,edit,1350631552
Control23=IDC_MATNAME,button,1342242819
Control24=IDS_NOMERGE,button,1342242819
Control25=IDS_NOSQUARIZE,button,1342242819

[DLG:IDD_NORMANIM (FULLVER)]
Type=1
Class=?
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308864
Control4=IDC_STATIC,static,1342308864
Control5=IDC_REFLOD,SysListView32,1350649935
Control6=IDC_SELLIST,SysListView32,1350649951
Control7=IDC_SELONLY,button,1342251011

[DLG:IDD_GIZMOMAPPING]
Type=1
Class=?
ControlCount=23
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308865
Control3=IDC_TU1,edit,1350631552
Control4=IDC_STATIC,static,1342308865
Control5=IDC_TU2,edit,1350631552
Control6=IDC_STATIC,static,1342308865
Control7=IDC_TV1,edit,1350631552
Control8=IDC_STATIC,static,1342308865
Control9=IDC_TV2,edit,1350631552
Control10=IDC_MAPPREVIEW,static,1342177542
Control11=IDC_SWAPTUTV,button,1342246915
Control12=IDC_MIRRORTU,button,1342242816
Control13=IDC_MIRRORTV,button,1342242816
Control14=IDC_TEXNAME,edit,1350631552
Control15=IDC_SETFROMVIEW,button,1342242880
Control16=IDC_SETFROMLIST,button,1342242880
Control17=IDC_BROWSE,button,1342242880
Control18=IDOK,button,1342242816
Control19=IDC_PREVIEW,button,1342242817
Control20=IDCANCEL,button,1342242816
Control21=IDC_WRAPU,button,1342242819
Control22=IDC_WRAPV,button,1342242819
Control23=IDC_MAPTYPE,combobox,1344339971

[DLG:IDD_UNLOCKSOFT]
Type=1
Class=?
ControlCount=20
Control1=IDC_STATIC,static,1342308353
Control2=IDC_UNLOCKTXT,static,1073872897
Control3=IDC_EXPIRETXT,static,1073872897
Control4=IDC_EXTENDTXT,static,1073872897
Control5=IDC_VISITWEB,button,1342242816
Control6=IDC_ORDER,button,1342242816
Control7=IDC_UNLOCK,button,1342242817
Control8=IDCANCEL,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_CODEOUT,edit,1476462721
Control11=IDC_STATIC,static,1342308864
Control12=IDC_CODEIN1,edit,1484857473
Control13=IDC_STATIC,static,1342308865
Control14=IDC_CODEIN2,edit,1484857473
Control15=IDC_STATIC,static,1342308864
Control16=IDC_CODEIN3,edit,1484857473
Control17=IDC_CODEIN4,edit,1484857473
Control18=IDC_UNLOCKNOW,button,1476460544
Control19=IDC_SEPARATE,static,1342177296
Control20=IDC_STATIC,static,1342308865

[DLG:IDD_CONSOLE]
Type=1
Class=?
ControlCount=1
Control1=IDC_CONSOLE,edit,1352734788

[DLG:IDD_LICENCEAGR]
Type=1
Class=?
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_RICHTEXT,RICHEDIT,1352730692
Control4=IDC_STATIC,static,1342308352

[DLG:IDD_WEIGHTAUTOMATION]
Type=1
Class=?
ControlCount=17
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308864
Control4=IDC_LOD,combobox,1344340227
Control5=IDC_STATIC,static,1342308864
Control6=IDC_FILE,combobox,1344340226
Control7=IDC_DEFINITION,edit,1353781700
Control8=IDC_CREATEFILE,button,1342242816
Control9=IDC_LOADFILE,button,1342242816
Control10=IDC_SAVEFILE,button,1342242816
Control11=IDC_STATIC,static,1342308352
Control12=IDC_DEFSOLID,edit,1350631552
Control13=IDC_STATIC,static,1342308352
Control14=IDC_DEFGRAD,edit,1350631552
Control15=IDC_STATIC,static,1342308352
Control16=IDC_DEFTEXTURE,combobox,1344340226
Control17=IDC_FINDBONES,button,1342242819

[DLG:IDD_VYBERLOD]
Type=1
Class=?
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LOD,combobox,1344340227
Control4=IDC_STATIC,static,1342308352

[DLG:IDD_BVHIMPORT]
Type=1
Class=?
ControlCount=11
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST1,SysListView32,1350664717
Control4=IDC_STATIC,static,1342308352
Control5=IDC_SCALE_STATIC,static,1342308352
Control6=IDC_SCALE,edit,1350631552
Control7=IDC_ZTOP,button,1342242819
Control8=IDC_EXTERMS,button,1342251008
Control9=IDC_INVERTX,button,1342242819
Control10=IDC_INVERTY,button,1342242819
Control11=IDC_INVERTZ,button,1342242819

[DLG:IDD_ASFAMCIMPORT]
Type=1
Class=?
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST1,SysListView32,1350664717
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_SCALE,edit,1350631552

[DLG:IDD_BEVEL]
Type=1
Class=?
ControlCount=12
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STRENGHTSLD,msctls_trackbar32,1342242817
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STRENGHT,edit,1350631553
Control6=IDC_STATIC,static,1342308864
Control7=IDC_RNG,button,1342308361
Control8=IDC_RNG2,button,1342177289
Control9=IDC_RNG3,button,1342177289
Control10=IDC_RNG4,button,1342177289
Control11=IDC_LOCKS,button,1476591619
Control12=IDC_PREVIEW,button,1342242816

[DLG:IDD_TEXELMEASURE]
Type=1
Class=?
ControlCount=20
Control1=IDC_STATIC,static,1342308352
Control2=IDC_TEXELINFO,SysListView32,1350631429
Control3=IDC_STATIC,static,1342308352
Control4=IDC_HISTOGRAM,button,1476460555
Control5=IDCANCEL,button,1342242816
Control6=IDC_SELONLY,button,1342252035
Control7=IDC_STATIC,button,1342177287
Control8=IDC_UNITMM,button,1342308361
Control9=IDC_UNITCM,button,1342177289
Control10=IDC_UNITM,button,1342177289
Control11=IDC_STATIC,static,1342308864
Control12=IDC_RANGEMIN,edit,1350631552
Control13=IDC_STATIC,static,1342308865
Control14=IDC_RANGEMAX,edit,1350631552
Control15=IDC_RESETRANGE,button,1342242816
Control16=IDC_RECALCULATERANGE,button,1342242817
Control17=IDC_SELECTRANGE,button,1342242816
Control18=IDC_STATIC,static,1342308352
Control19=IDC_STATIC,static,1342308352
Control20=IDC_CURPOS,edit,1342244993

[DLG:IDD_NORMALDIRECTION]
Type=1
Class=?
ControlCount=12
Control1=IDC_STATIC,static,1342308352
Control2=IDC_XVALUE,edit,1350631425
Control3=IDC_STATIC,static,1342308352
Control4=IDC_YVALUE,edit,1350631425
Control5=IDC_STATIC,static,1342308352
Control6=IDC_ZVALUE,edit,1350631425
Control7=IDC_RELATIVETO,button,1342242819
Control8=IDC_NORMALLIST,combobox,1344340227
Control9=IDOK,button,1342242817
Control10=IDCANCEL,button,1342242816
Control11=IDC_UNLOCKNORMALS,button,1342242816
Control12=IDC_PREVIEW,button,1342242816

[DLG:IDD_SELECTIONNAME]
Type=1
Class=?
ControlCount=1
Control1=IDC_SELLIST,combobox,1344341313

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=?
ControlCount=2
Control1=IDOK,button,1342373889
Control2=IDC_SING,static,1342308353

[DLG:IDD_LODLIST (English (U.S.))]
Type=1
Class=?
ControlCount=1
Control1=IDC_LIST,SysListView32,1342276187

[DLG:IDD_LODCONFIG (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342308352
Control2=IDC_RESOLUTION,edit,1350631553
Control3=IDOK,button,1342242817
Control4=IDC_LIST,listbox,1344340227

[DLG:IDD_PTINFO (English (U.S.))]
Type=1
Class=?
ControlCount=6
Control1=IDC_XINFO,edit,1350631552
Control2=IDC_STATIC,static,1342308352
Control3=IDC_YINFO,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_ZINFO,edit,1350631552
Control6=IDC_STATIC,static,1342308352

[DLG:IDD_SELLIST (English (U.S.))]
Type=1
Class=?
ControlCount=1
Control1=IDC_LIST,SysListView32,1342278235

[DLG:IDD_HISTORY (English (U.S.))]
Type=1
Class=?
ControlCount=1
Control1=IDC_LIST,listbox,1352728913

[DLG:IDD_CREATE_PRIMITIVE (English (U.S.))]
Type=1
Class=?
ControlCount=22
Control1=IDC_STATIC,static,1342308352
Control2=IDC_SIZEX,edit,1350631561
Control3=IDC_STATIC,static,1342308352
Control4=IDC_SIZEY,edit,1350631561
Control5=IDC_STATIC,static,1342308352
Control6=IDC_SIZEZ,edit,1350631561
Control7=IDC_NUMX,edit,1350631561
Control8=IDC_STATIC,static,1342308352
Control9=IDC_NUMY,edit,1350631561
Control10=IDC_STATIC,static,1342308352
Control11=IDC_NUMZ,edit,1350631561
Control12=IDC_STATIC,static,1342308352
Control13=IDC_RADIUS,edit,1350631561
Control14=IDC_STATIC,button,1342177287
Control15=IDC_CENTER,button,1342373897
Control16=IDC_BOTTOM,button,1342177289
Control17=IDC_UP,button,1342177289
Control18=IDOK,button,1342242817
Control19=IDCANCEL,button,1342242816
Control20=IDC_ST_SEGX,static,1342308352
Control21=IDC_ST_SEGRD,static,1073872896
Control22=IDC_CIRCCENTER,button,1073815555

[DLG:IDD_TRANSFORM (English (U.S.))]
Type=1
Class=?
ControlCount=14
Control1=IDC_STATIC,static,1342308352
Control2=IDC_XVALUE,edit,1350631425
Control3=IDC_STATIC,static,1342308352
Control4=IDC_YVALUE,edit,1350631425
Control5=IDC_STATIC,static,1342308352
Control6=IDC_ZVALUE,edit,1350631425
Control7=IDC_SELONLY,button,1342242819
Control8=IDC_RELTOPIN,button,1342242819
Control9=IDC_APPLYTOANIMS,button,1342242819
Control10=IDC_APPLYTOLODS,button,1342242819
Control11=IDOK,button,1342242817
Control12=IDCANCEL,button,1342242816
Control13=IDC_PREVIEW,button,1342242816
Control14=IDC_GISMO,button,1342242819

[DLG:IDD_CALCULATOR (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=42
Control1=IDC_AVALUENAME,static,1342308864
Control2=IDC_AVALUE,edit,1350631425
Control3=IDC_BVALUENAME,static,1342308864
Control4=IDC_BVALUE,edit,1350631425
Control5=IDC_CVALUENAME,static,1342308864
Control6=IDC_CVALUE,edit,1350631425
Control7=IDC_DVALUENAME,static,1342308864
Control8=IDC_DVALUE,edit,1350631425
Control9=IDC_XVALUENAME,static,1342308864
Control10=IDC_XVALUE,edit,1350631425
Control11=IDC_YVALUENAME,static,1342308864
Control12=IDC_YVALUE,edit,1350631425
Control13=IDC_ZVALUENAME,static,1342308864
Control14=IDC_ZVALUE,edit,1350631425
Control15=IDOK,button,1342242817
Control16=IDCANCEL,button,1342242816
Control17=IDC_DEFINE,button,1342242816
Control18=IDC_STATIC,static,1342308864
Control19=IDC_ACALC,edit,1350631552
Control20=IDC_STATIC,static,1342308864
Control21=IDC_BCALC,edit,1350631552
Control22=IDC_STATIC,static,1342308864
Control23=IDC_CCALC,edit,1350631552
Control24=IDC_STATIC,static,1342308864
Control25=IDC_DCALC,edit,1350631552
Control26=IDC_STATIC,static,1342308864
Control27=IDC_XCALC,edit,1350631552
Control28=IDC_STATIC,static,1342308864
Control29=IDC_YCALC,edit,1350631552
Control30=IDC_STATIC,static,1342308864
Control31=IDC_ZCALC,edit,1350631552
Control32=IDC_STATIC,static,1342308864
Control33=IDC_SCHEMENAME,edit,1350631552
Control34=IDC_SAVE,button,1342242816
Control35=IDC_STATIC,static,1342308352
Control36=IDC_LIST,listbox,1352728835
Control37=IDC_SELONLY,button,1342242819
Control38=IDC_RELTOPIN,button,1342242819
Control39=IDC_APPLYTOANIMS,button,1342242819
Control40=IDC_APPLYTOLODS,button,1342242819
Control41=IDC_NEW,button,1342242816
Control42=IDC_REMOVE,button,1342242816

[DLG:IDD_CONFIG (English (U.S.))]
Type=1
Class=?
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST1,SysListView32,1350664265
Control4=IDC_VALUESTATIC,static,1342308352
Control5=IDC_VALUE,edit,1484849536

[DLG:IDD_CYLINDRICMAPPING (English (U.S.))]
Type=1
Class=?
ControlCount=27
Control1=IDC_STATIC,static,1342308352
Control2=IDC_TU_OFFSET,msctls_trackbar32,1342242840
Control3=IDC_STATIC,static,1342308352
Control4=IDC_TV_OFFSET,msctls_trackbar32,1342242840
Control5=IDC_STATIC,static,1342308352
Control6=IDC_TU_SCALE,msctls_trackbar32,1342242840
Control7=IDC_STATIC,static,1342308352
Control8=IDC_TV_SCALE,msctls_trackbar32,1342242840
Control9=IDC_STATIC,static,1342308864
Control10=IDC_AZIMUT,edit,1350631553
Control11=IDC_STATIC,static,1342308864
Control12=IDC_ZENIT,edit,1350631553
Control13=IDC_MIRRORTU,button,1342242819
Control14=IDC_MIRRORTV,button,1342242819
Control15=IDC_SWAPTUTV,button,1342242819
Control16=IDC_SING,button,1342242819
Control17=IDOK,button,1342242817
Control18=IDC_PREVIEW,button,1342242816
Control19=IDCANCEL,button,1342242816
Control20=IDC_TU_OFFSET_VAL,edit,1350631553
Control21=IDC_TV_OFFSET_VAL,edit,1350631553
Control22=IDC_TU_SCALE_VAL,edit,1350631553
Control23=IDC_TV_SCALE_VAL,edit,1350631553
Control24=IDC_TEXNAME,edit,1350631552
Control25=IDC_SETFROMVIEW,button,1342242880
Control26=IDC_SETFROMLIST,button,1342242880
Control27=IDC_BROWSE,button,1342242880

[DLG:IDD_VERTEX_PROPERTIES (English (U.S.))]
Type=1
Class=?
ControlCount=40
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308352
Control3=IDC_XVALUE,edit,1350631553
Control4=IDC_STATIC,static,1342308352
Control5=IDC_YVALUE,edit,1350631553
Control6=IDC_STATIC,static,1342308352
Control7=IDC_ZVALUE,edit,1350631553
Control8=IDC_STATIC,static,1342308352
Control9=IDC_USERVALUE,edit,1350631553
Control10=IDC_STATIC,button,1342177287
Control11=IDC_SURFACE_NORMAL,button,1342308361
Control12=IDC_SURFACE_ONSURFACE,button,1342177289
Control13=IDC_SURFACE_ABOVE,button,1342177289
Control14=IDC_SURFACE_UNDER,button,1342177289
Control15=IDC_SURFACE_KEEPHEIGHT,button,1342177289
Control16=IDC_STATIC,button,1342177287
Control17=IDC_FOG_NORMAL,button,1342308361
Control18=IDC_FOG_SKY,button,1342177289
Control19=IDC_FOG_NONE,button,1342177289
Control20=IDC_HIDDEN_VERTEX,button,1342373894
Control21=IDC_STATIC,button,1342177287
Control22=IDC_DECAL_NORMAL,button,1342308361
Control23=IDC_RADIO11,button,1342177289
Control24=IDC_RADIO12,button,1342177289
Control25=IDC_STATIC,button,1342177287
Control26=IDC_LIGHTING_NORMAL,button,1342308361
Control27=IDC_LIGHTING_SHINING,button,1342177289
Control28=IDC_LIGHTING_SHADOW,button,1342177289
Control29=IDC_LIGHTING_HALF,button,1342177289
Control30=IDC_LIGHTING_FULL,button,1342177289
Control31=IDC_APPLY,button,1342242816
Control32=IDCANCEL,button,1342242816
Control33=IDOK,button,1342254849
Control34=IDC_LIST,listbox,1352731011
Control35=IDC_SELECTALL,button,1342242816
Control36=IDC_FILTER,button,1342242816
Control37=IDC_STATIC,static,1342308864
Control38=IDC_NUMVERTICES,static,1342308866
Control39=IDC_CREATESEL,button,1342251008
Control40=IDC_LOCKEDNORMAL,button,1342242822

[DLG:IDD_FACE_PROPERTIES (English (U.S.))]
Type=1
Class=?
ControlCount=50
Control1=IDC_STATIC,static,1342308352
Control2=IDC_NUMVERTICES,static,1342308866
Control3=IDC_STATIC,static,1342308864
Control4=IDC_STATIC,button,1342177287
Control5=IDC_STATIC,static,1342308864
Control6=IDC_STATIC,static,1342308864
Control7=IDC_STATIC,static,1342308353
Control8=IDC_TU1,edit,1350631425
Control9=IDC_TV1,edit,1350631425
Control10=IDC_STATIC,static,1342308353
Control11=IDC_TU2,edit,1350631425
Control12=IDC_TV2,edit,1350631425
Control13=IDC_STATIC,static,1342308353
Control14=IDC_TU3,edit,1350631425
Control15=IDC_TV3,edit,1350631425
Control16=IDC_STATIC,static,1342308353
Control17=IDC_TU4,edit,1350631425
Control18=IDC_TV4,edit,1350631425
Control19=IDC_STATIC,button,1342177287
Control20=IDC_LIGHTING_NORMAL,button,1342374921
Control21=IDC_LIGHTING_BSIDES,button,1342178313
Control22=IDC_LIGHTING_POSITION,button,1342178313
Control23=IDC_LIGHTING_FLAT,button,1342178313
Control24=IDC_LIGHTING_REVERSED,button,1342186505
Control25=IDC_ENABLE_SHADOW,button,1342242822
Control26=IDC_STATIC,button,1342177287
Control27=IDC_BIAS_NORMAL,button,1342373897
Control28=IDC_BIAS_LOW,button,1342177289
Control29=IDC_BIAS_MIDDLE,button,1342177289
Control30=IDC_BIAS_HIGH,button,1342177289
Control31=IDC_STATIC,button,1342177287
Control32=IDC_STATIC,static,1342308352
Control33=IDC_EDIT1,edit,1350631552
Control34=IDC_SETFROMVIEW,button,1342242880
Control35=IDC_SETFROMLIST,button,1342242880
Control36=IDC_BROWSE,button,1342242880
Control37=IDC_ENABLE_TEXMERGE,button,1342242822
Control38=IDC_STATIC,static,1342308352
Control39=IDC_MATERIAL,edit,1350631552
Control40=IDC_BROWSE2,button,1342242880
Control41=IDC_STATIC,static,1342308352
Control42=IDC_USERVALUE,edit,1350631552
Control43=IDOK,button,1342254849
Control44=IDCANCEL,button,1342242816
Control45=IDC_APPLY,button,1342242816
Control46=IDC_SELECTALL,button,1342242816
Control47=IDC_FILTER,button,1342242816
Control48=IDC_CREATESEL,button,1342251776
Control49=IDC_STATIC,button,1342177287
Control50=IDC_LISTEX,SysListView32,1350664201

[DLG:IDD_ANIMLIST (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=1
Control1=IDC_LIST,SysListView32,1342276169

[DLG:IDD_SOFTENANIM (English (U.S.))]
Type=1
Class=?
ControlCount=14
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_XVALUE,edit,1350631553
Control4=IDC_YVALUE,edit,1350631553
Control5=IDC_ZVALUE,edit,1350631553
Control6=IDC_TIMERANGE,edit,1350631553
Control7=IDC_STATIC,static,1342308864
Control8=IDC_STATIC,static,1342308864
Control9=IDC_STATIC,static,1342308864
Control10=IDC_STATIC,static,1342308864
Control11=IDC_INTERPOLATE,button,1342242819
Control12=IDC_LOOP,button,1342242819
Control13=IDC_ACTUALFRAME,button,1342242819
Control14=IDC_STATIC,button,1342177287

[DLG:IDD_WEIGHTS (English (U.S.))]
Type=1
Class=?
ControlCount=3
Control1=IDOK,button,1342242816
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST,SysListView32,1350664781

[DLG:IDD_MASSBAR (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_MASS,edit,1350631552
Control2=IDC_CHECK,button,1342242819
Control3=IDC_STATIC,static,1342308864
Control4=IDC_APPLY,button,1342242817

[DLG:IDD_MAPDLG (English (U.S.))]
Type=1
Class=?
ControlCount=38
Control1=IDC_AVALUENAME,static,1342308864
Control2=IDC_AVALUE,edit,1350631425
Control3=IDC_BVALUENAME,static,1342308864
Control4=IDC_BVALUE,edit,1350631425
Control5=IDC_CVALUENAME,static,1342308864
Control6=IDC_CVALUE,edit,1350631425
Control7=IDC_DVALUENAME,static,1342308864
Control8=IDC_DVALUE,edit,1350631425
Control9=IDC_XVALUENAME,static,1342308864
Control10=IDC_XVALUE,edit,1350631425
Control11=IDC_YVALUENAME,static,1342308864
Control12=IDC_YVALUE,edit,1350631425
Control13=IDOK,button,1342242817
Control14=IDCANCEL,button,1342242816
Control15=IDC_DEFINE,button,1342242816
Control16=IDC_STATIC,static,1342308864
Control17=IDC_ACALC,edit,1350631552
Control18=IDC_STATIC,static,1342308864
Control19=IDC_BCALC,edit,1350631552
Control20=IDC_STATIC,static,1342308864
Control21=IDC_CCALC,edit,1350631552
Control22=IDC_STATIC,static,1342308864
Control23=IDC_DCALC,edit,1350631552
Control24=IDC_STATIC,static,1342308864
Control25=IDC_XCALC,edit,1350631552
Control26=IDC_STATIC,static,1342308864
Control27=IDC_YCALC,edit,1350631552
Control28=IDC_STATIC,static,1342308864
Control29=IDC_SCHEMENAME,edit,1350631552
Control30=IDC_SAVE,button,1342242816
Control31=IDC_STATIC,static,1342308352
Control32=IDC_LIST,listbox,1352728835
Control33=IDC_POIINTSONLY,button,1342242819
Control34=IDC_FACESONLY,button,1342242819
Control35=IDC_NEW,button,1342242816
Control36=IDC_REMOVE,button,1342242816
Control37=IDC_UWRAP,button,1342242819
Control38=IDC_VWRAP,button,1342242819

[DLG:IDD_EXTRUDESTEPS (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=3
Control1=IDC_STATIC,static,1342308352
Control2=IDC_SEGMENTS,edit,1350639618
Control3=IDC_SEGSPIN,msctls_updown32,1342177312

[DLG:IDD_INPROGRESS (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATICTEXT,static,1342308353
Control2=IDC_PROGRESS,msctls_progress32,1350565889
Control3=IDC_FLASHING,static,1342177285
Control4=IDC_ODHAD,static,1342308354

[DLG:IDD_INPROGRESS_PUBLIC (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATICTEXT,static,1342308353
Control2=IDC_PROGRESS,msctls_progress32,1350565889
Control3=IDC_FLASHING,static,1342177285
Control4=IDC_ODHAD,static,1342308354

[DLG:IDD_2DTRANSFORM (English (U.S.))]
Type=1
Class=?
ControlCount=5
Control1=IDC_STATIC,static,1342308864
Control2=IDC_VALUE,edit,1350631552
Control3=IDC_PIN,button,1342242819
Control4=IDOK,button,1342242817
Control5=IDC_PREVIEW,button,1342242816

[DLG:IDD_SPACEWARP (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=15
Control1=IDC_STATIC,static,1342308864
Control2=IDC_EDIT1,edit,1350631552
Control3=IDC_SPIN1,msctls_updown32,1342177312
Control4=IDC_STATIC,static,1342308864
Control5=IDC_EDIT2,edit,1350631552
Control6=IDC_SPIN2,msctls_updown32,1342177312
Control7=IDC_STATIC,static,1342308864
Control8=IDC_EDIT4,edit,1350631552
Control9=IDC_SPIN3,msctls_updown32,1342177312
Control10=IDC_STATIC,static,1342308352
Control11=IDC_SELECTSEL,combobox,1344340227
Control12=IDC_SAVETOSEL,button,1342242819
Control13=IDOK,button,1342242817
Control14=IDCANCEL,button,1342242816
Control15=IDC_FUNCTION,button,1476460555

[DLG:IDD_PROXYNAME (English (U.S.))]
Type=1
Class=?
ControlCount=3
Control1=IDC_COMBO1,combobox,1344340034
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816

[DLG:IDD_OPENQ (English (U.S.))]
Type=1
Class=?
ControlCount=6
Control1=IDC_IGNORE,button,1342242817
Control2=IDC_UPDATENONEXISTS,button,1342242816
Control3=IDC_UPDATENEWER,button,1342242816
Control4=IDC_UPDATEALL,button,1342242816
Control5=IDCANCEL,button,1342242816
Control6=IDC_STATIC,static,1342308352

[DLG:IDD_MATCHDIRECTION (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=32
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_XDIR,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_YDIR,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_ZDIR,edit,1350631552
Control9=IDC_STATIC,static,1342308352
Control10=IDC_SOURCEFILENAME,edit,1350631552
Control11=IDC_BROWSE,button,1342242816
Control12=IDC_MAKEBACKUP,button,1342242819
Control13=IDC_STATIC,button,1342177287
Control14=IDC_STATIC,static,1342308352
Control15=IDC_XROTNAME,edit,1350631552
Control16=IDC_STATIC,static,1342308352
Control17=IDC_XROTCENT,edit,1350631552
Control18=IDC_STATIC,static,1342308352
Control19=IDC_ZROTNAME,edit,1350631552
Control20=IDC_STATIC,static,1342308352
Control21=IDC_ZROTCENT,edit,1350631552
Control22=IDC_STATIC,static,1342308352
Control23=IDC_PROXY,edit,1350631552
Control24=IDC_PREW,button,1342242819
Control25=IDC_STATIC,button,1342177287
Control26=IDC_STATIC,static,1342308352
Control27=IDC_XDIR2,edit,1350631552
Control28=IDC_STATIC,static,1342308352
Control29=IDC_YDIR2,edit,1350631552
Control30=IDC_STATIC,static,1342308352
Control31=IDC_ZDIR2,edit,1350631552
Control32=IDC_STATIC,button,1342177287

[DLG:IDD_MEASURING (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=13
Control1=IDC_STATIC,static,1342308864
Control2=IDC_DESTATION,edit,1350633601
Control3=IDC_STATIC,static,1342308864
Control4=IDC_XDIR,edit,1350633601
Control5=IDC_YDIR,edit,1350633601
Control6=IDC_ZDIR,edit,1350633601
Control7=IDC_STATIC,static,1342308864
Control8=IDC_AZIMUT,edit,1350633600
Control9=IDC_STATIC,static,1342308864
Control10=IDC_ZENIT,edit,1350633600
Control11=IDC_STATIC,button,1342177287
Control12=IDC_PINTOCUR,button,1342177289
Control13=IDC_PINTOCENTER,button,1342177289

[DLG:IDD_UNWRAP (English (U.S.))]
Type=1
Class=?
ControlCount=10
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TU_OFFSET,msctls_trackbar32,1342242840
Control4=IDC_STATIC,static,1342308864
Control5=IDC_TV_OFFSET,msctls_trackbar32,1342242840
Control6=IDC_STATIC,static,1342308864
Control7=IDC_WEIGHTTU,button,1342242819
Control8=IDC_WEIGHTTV,button,1342242819
Control9=IDC_TU_OFFSET_VAL,edit,1350633601
Control10=IDC_TV_OFFSET_VAL,edit,1350633601

[DLG:IDD_TIMERANGE (English (U.S.))]
Type=1
Class=?
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308864
Control4=IDC_TIMERANGE,edit,1350631552
Control5=IDC_INTERPOLATE,button,1342242819
Control6=IDC_LOOP,button,1342242819

[DLG:IDD_EXPORT3DS (English (U.S.))]
Type=1
Class=C3dsExport
ControlCount=16
Control1=IDC_VERTOPT,button,1342242819
Control2=IDC_CREATEMATERIALS,button,1342242819
Control3=IDC_EXPORTTEX,button,1342242819
Control4=IDC_STATIC,button,1342177287
Control5=IDC_SAMEDIR,button,1342373897
Control6=IDC_SELECTPATH,button,1342177289
Control7=IDC_PATH,edit,1350762624
Control8=IDC_BROWSE,button,1342242880
Control9=IDC_SMOOTGROUPS,button,1342242819
Control10=IDOK,button,1342242817
Control11=IDCANCEL,button,1342242816
Control12=IDC_STATIC,static,1342308864
Control13=IDC_SCALE,edit,1350631425
Control14=IDC_NONALPHADETECTS,button,1342251011
Control15=IDC_EXPTOFOLDER,button,1342242819
Control16=IDC_BYTOPOLOGY,button,1342242819

[DLG:IDD_CALCULATOR_MODIFY (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=19
Control1=IDC_STATIC,static,1342308352
Control2=IDC_INPUTVARS,edit,1352732996
Control3=IDC_RELTOPIN,button,1342242819
Control4=IDC_WARPU,button,1342242819
Control5=IDC_WARPV,button,1342242819
Control6=IDC_STATIC,static,1342308352
Control7=IDC_INITPROGRAM,edit,1353781700
Control8=IDC_STATIC,static,1342308352
Control9=IDC_POINTPROGRAM,edit,1353781700
Control10=IDC_STATIC,static,1342308352
Control11=IDC_FACEPROGRAM,edit,1353781700
Control12=IDC_STATIC,static,1342308352
Control13=IDC_NAME,edit,1350631552
Control14=IDOK,button,1342242817
Control15=IDCANCEL,button,1342242816
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_SETVARS,button,1342242816
Control19=IDC_HELPBUTT,button,1342242816

[DLG:IDD_CALCULATOR_INITVARS (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=23
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342177287
Control4=IDC_STATIC,static,1342177287
Control5=IDC_STATIC,static,1342308864
Control6=IDC_STATIC,static,1342177287
Control7=IDC_SCROLLBAR,scrollbar,1342177281
Control8=IDC_CALCTEXT1,static,1073873408
Control9=IDC_CALCDATA1,edit,1082195969
Control10=IDC_CALCTEXT2,static,1073873408
Control11=IDC_CALCDATA2,edit,1082195969
Control12=IDC_CALCTEXT3,static,1073873408
Control13=IDC_CALCDATA3,edit,1082195969
Control14=IDC_CALCTEXT4,static,1073873408
Control15=IDC_CALCDATA4,edit,1082195969
Control16=IDC_CALCTEXT5,static,1073873408
Control17=IDC_CALCDATA5,edit,1082195969
Control18=IDC_CALCTEXT6,static,1073873408
Control19=IDC_CALCDATA6,edit,1082195969
Control20=IDC_CALCTEXT7,static,1073873408
Control21=IDC_CALCDATA7,edit,1082195969
Control22=IDC_CALCTEXT8,static,1073873408
Control23=IDC_CALCDATA8,edit,1082195969

[DLG:IDD_CALCULATOR_DIALOG (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=11
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_PROGLIST,listbox,1352728835
Control4=IDC_STATIC,static,1342308352
Control5=IDC_NEW,button,1342242816
Control6=IDC_MODIFY,button,1342242816
Control7=IDC_DELETE,button,1342242816
Control8=IDC_SELONLY,button,1342242819
Control9=IDC_APPLYTOANIMS,button,1342242819
Control10=IDC_APPLYTOLODS,button,1342242819
Control11=IDC_CAMTRANSF,button,1342242819

[DLG:IDD_LOGOSMALL (English (U.S.))]
Type=1
Class=?
ControlCount=2
Control1=IDC_LOGO,static,1342177806
Control2=IDC_LOGO2,static,1073742350

[DLG:IDD_MERGENEAR (English (U.S.))]
Type=1
Class=?
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308353
Control4=IDC_DISTANCE,edit,1350631553
Control5=IDC_DETECT,button,1342242816

[DLG:IDD_SPLITHOLE (English (U.S.))]
Type=1
Class=?
ControlCount=11
Control1=IDC_SPLIT,button,1342308361
Control2=IDC_HOLE,button,1342177289
Control3=IDC_STATIC,button,1342308359
Control4=IDC_RADIO1,button,1342308361
Control5=IDC_RADIO2,button,1342177289
Control6=IDC_RADIO3,button,1342177289
Control7=IDC_STATIC,button,1342308359
Control8=IDC_POINTS,edit,1350770817
Control9=IDC_SPIN,msctls_updown32,1342308384
Control10=IDOK,button,1342373889
Control11=IDCANCEL,button,1342373888

[DLG:IDD_MULTPINCULL (English (U.S.))]
Type=1
Class=?
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308864
Control3=IDC_SELLIST,combobox,1344340227

[DLG:IDD_COLORS (English (U.S.))]
Type=1
Class=?
ControlCount=18
Control1=IDC_COLOR1,button,1342242827
Control2=IDC_COLOR2,button,1342242827
Control3=IDC_COLOR3,button,1342242827
Control4=IDC_COLOR4,button,1342242827
Control5=IDC_COLOR5,button,1342242827
Control6=IDC_COLOR6,button,1342242827
Control7=IDC_COLOR7,button,1342242827
Control8=IDC_COLOR8,button,1342242827
Control9=IDC_COLOR9,button,1342242827
Control10=IDC_COLOR10,button,1342242827
Control11=IDC_COLOR11,button,1342242827
Control12=IDC_COLOR12,button,1342242827
Control13=IDC_COLOR13,button,1342242827
Control14=IDC_COLOR14,button,1342242827
Control15=IDC_COLOR15,button,1342242827
Control16=IDC_COLOR16,button,1342242827
Control17=IDCANCEL,button,1342242816
Control18=IDC_STATIC,static,1342308353

[DLG:IDD_HIERARCHY (English (U.S.) - FULLVER)]
Type=1
Class=?
ControlCount=1
Control1=IDC_HTREE,SysTreeView32,1350637059

[MNU:IDR_NAMPROP_POPUP]
Type=1
Class=?
Command1=ID_NAMPROP_NEW
Command2=ID_NAMPROP_DELETE
Command3=ID_NAMPROP_EDIT
CommandCount=3

[MNU:IDR_GIZMOMENU]
Type=1
Class=?
Command1=ID_GIZMO_RESETMAP
Command2=ID_SURFACES_RESETGISMO
Command3=ID_SURFACES_UNDOGIZMO
Command4=ID_TRANSF_MOVE
Command5=ID_TRANSF_ROTATE
Command6=ID_TRANSF_SCALE
Command7=ID_TRANS2_ROTATE
Command8=ID_TRANS2_SCALE
Command9=ID_TRANS2_MIRRORX
Command10=ID_TRANS2_MIRRORY
CommandCount=10

[MNU:IDR_LODMENU_POPUP (English (U.S.))]
Type=1
Class=?
Command1=ID_LOD_INSERT
Command2=ID_LOD_COPY
Command3=ID_LOD_DELETE
Command4=ID_LOD_SETASBACK
Command5=ID_LOD_DISPLAYSHORT
Command6=ID_LOD_DISPLAYDETAILS
Command7=ID_LOD_PROPERIES
CommandCount=7

[MNU:IDR_SELMENU_POPUP (English (U.S.))]
Type=1
Class=?
Command1=ID_SLCT_NEW
Command2=ID_SLCT_DELETE
Command3=ID_SLCT_RENAME
Command4=ID_SLCT_PROXY
Command5=ID_SLCT_REDEFINE
Command6=ID_SLCT_REDEFNORM
Command7=ID_SLCT_SETGROUP
Command8=ID_SLCT_WEIGHTS
CommandCount=8

[MNU:IDR_HISTORY_POPUP (English (U.S.))]
Type=1
Class=?
Command1=ID_NOTHING
Command2=ID_HISTORY_CLEARHISTORY
Command3=ID_HISTORY_RECORD
Command4=ID_HISTORY_STOP
Command5=ID_HISTORY_DELETE
Command6=ID_HISTORY_OPEN
CommandCount=6

[MNU:IDR_INTVIEW_POPUP (English (U.S.) - FULLVER)]
Type=1
Class=?
Command1=ID_INTVIEW_SHOWHIDDEN
Command2=ID_INTVIEW_SHOWPROXIES
Command3=ID_INTVIEW_RELOAD
Command4=ID_INTVIEW_SETWINDOW
CommandCount=4

[MNU:IDR_ANIMMENU_POPUP (English (U.S.) - FULLVER)]
Type=1
Class=?
Command1=ID_ANIM_NEW
Command2=ID_ANIM_DELETE
Command3=ID_ANIM_RENAME
Command4=ID_ANIM_REDEFINE
Command5=ID_ANIM_IMPORT
Command6=ID_ANIM_COPY
Command7=ID_ANIM_PASTE
Command8=ID_ANIM_PASTEOVERSEL
Command9=ID_ANIM_LOCKSELECTED
Command10=ID_ANIM_UNLOCKALL
Command11=ID_ANIM_TIMERANGE
Command12=ID_ANIM_IMPORTMULTIPLE
Command13=ID_ANIM_AUTOTIME
Command14=ID_ANIM_AUTOTIME10
Command15=ID_ANIM_HALFRATE
Command16=ID_ANIM_SOFTSELECT
Command17=ID_ANIM_NEUTRALIZESTEP
Command18=ID_ANIM_NEUTRALIZEXSTEP
Command19=ID_ANIM_INSERTSTEP
Command20=ID_ANIM_ZNORMCENTER
Command21=ID_ANIM_XNORMCENTER
Command22=ID_ANIM_MIRROR
Command23=ID_ANIM_FROMMATRICES
Command24=ID_ANIM_FROMMATRICESLOD
Command25=ID_ANIM_EXPORTMATRICES
CommandCount=25

[MNU:IDR_TEXTURE_POPUP (English (U.S.))]
Type=1
Class=?
Command1=ID_TEX_UNDO
Command2=ID_TEX_LOAD
Command3=ID_TEX_ASPEC
Command4=ID_TEX_MIRROR_X
Command5=ID_TEX_MIRROR_Y
Command6=ID_TEX_ROTATER
Command7=ID_TEX_ROTATEL
Command8=ID_TEX_GRAY
Command9=ID_TEX_HIDE
Command10=ID_SURFACES_BGRFROMFACE
Command11=ID_TEX_UNLOAD
CommandCount=11

[MNU:IDR_HIERARCHY_POPUP (English (U.S.) - FULLVER)]
Type=1
Class=?
Command1=ID_HIERARCHY_SETACTIVE
Command2=ID_HIERARCHY_NEW
Command3=ID_HIERARCHY_DELETE
Command4=ID_HIERARCHY_COMBINE
Command5=ID_HIERARCHY_LOD
Command6=ID_HIERARCHY_HIDE
Command7=ID_HIERARCHY_COLOR
Command8=ID_HIERARCHY_DISPLAYINVIEWER
CommandCount=8

[MNU:IDR_MAINFRAME (English (U.S.) - FULLVER)]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MERGE
Command4=ID_FILE_MERGEASSELECTION
Command5=ID_FILE_SAVE
Command6=ID_FILE_SAVE_AS
Command7=ID_FILE_SAVETOSERVER
Command8=ID_FILE_IMPORT3DS
Command9=ID_FILE_IMPORT_BIANMMAYAANIMATIONEXPORT
Command10=ID_FILE_IMPORT_OBJFILE
Command11=ID_FILE_IMPORT_ASFAMC
Command12=ID_FILE_IMPORT_BIOVISION
Command13=ID_FILE_IMPORT_MAYAWEIGHTSCRIPT
Command14=ID_FILE_SEND
Command15=ID_FILE_EXPORT_3DSTUDIO
Command16=ID_FILE_EXPORT_P3DOLDVERSION
Command17=ID_FILE_EXPORT_MDA
Command18=ID_FILE_OPTIONS
Command19=ID_FILE_HOTKEYS
Command20=ID_FILE_MRU_FILE1
Command21=ID_APP_EXIT
Command22=ID_EDIT_UNDO
Command23=ID_EDIT_REDO
Command24=ID_EDIT_CUT
Command25=ID_EDIT_COPY
Command26=ID_EDIT_PASTE
Command27=ID_EDIT_DELETE
Command28=ID_EDIT_SELECT_ALL
Command29=ID_EDIT_DESELECT
Command30=ID_EDIT_INVERTSELECTION
Command31=ID_EDIT_SELECTFACESFROMPOINTS
Command32=ID_EDIT_SELECTPOINTSFROMFACES
Command33=ID_VIEW_SELLOCKED
Command34=ID_EDIT_SELHIDDEN
Command35=ID_EDIT_SELECTONESIDE
Command36=ID_EDIT_UNSELECTTRIANGLES
Command37=ID_EDIT_ELASTICSEL
Command38=ID_EDIT_LINEARDEFORM
Command39=ID_SELOP_NONE
Command40=ID_SELOP_UNION
Command41=ID_SELOP_COMPLEMENT
Command42=ID_SELOP_INTERSECTION
Command43=ID_SELOP_COMPINTER
Command44=ID_SELOP_COMBINE
Command45=ID_SELOP_CLEAR
Command46=ID_EDIT_CENTERPIN
Command47=ID_EDIT_LOCKPIN
Command48=ID_EDIT_SELECTOBJECTS
Command49=ID_EDIT_SELECTFACES
Command50=ID_EDIT_SELECTVERTICES
Command51=ID_EDIT_PIN
Command52=ID_SURFACES_BACKGROUNDTEXTURE
Command53=ID_EDIT_PAINTVERTICES
Command54=ID_EDIT_LASO
Command55=ID_EDIT_EDITMODES_TOUCHFACES
Command56=ID_EDIT_EDITMODES_TOUCHEDGES
Command57=ID_EDIT_INSERTPOINT
Command58=ID_CREATE_PLANE
Command59=ID_CREATE_BOX
Command60=ID_CREATE_CIRCLE
Command61=ID_CREATE_SPEHERE
Command62=ID_CREATE_CYLINDER
Command63=ID_CREATE_FACE
Command64=ID_CREATE_PROXY
Command65=ID_FACES_EXTRUDE
Command66=ID_CREATE_SHAPE
Command67=ID_VIEW_FRONT
Command68=ID_VIEW_TOP
Command69=ID_VIEW_LEFT
Command70=ID_VIEW_PROJECTION
Command71=ID_VIEW_RIGHT
Command72=ID_VIEW_BACK
Command73=ID_VIEW_BOTTOM
Command74=ID_VIEW_LOOKATFACE
Command75=ID_VIEW_ZOOM
Command76=ID_VIEW_ZOOMIN
Command77=ID_VIEW_ZOOMOUT
Command78=ID_VIEW_PAGEZOOM
Command79=ID_VIEW_PAGEZOOMSEL
Command80=ID_VIEW_CENTERZOOM_AUTO
Command81=ID_VIEW_SOLIDFILL
Command82=ID_VIEW_FACECULL
Command83=ID_VIEW_GRID
Command84=ID_VIEW_RUNINTERNALVIEWER
Command85=ID_VIEW_RESTARTEXTERNAL
Command86=ID_VIEW_VIEWERS_FOCUSSELECTIONS
Command87=ID_VIEW_VIEWERS_FREEZEANIMATION
Command88=ID_LOCK_XY
Command89=ID_LOCK_XZ
Command90=ID_LOCK_YZ
Command91=ID_LOCK_X
Command92=ID_LOCK_Y
Command93=ID_LOCK_Z
Command94=ID_LOCK_ROTATION
Command95=ID_VIEW_PINCAMER
Command96=ID_VIEW_HIDESEL
Command97=ID_VIEW_UNHIDESEL
Command98=ID_VIEW_LOCKSEL
Command99=ID_VIEW_UNLOCK
Command100=ID_VIEW_COLORIZEOBJECTS
Command101=ID_VIEW_REFRESH
Command102=ID_STRUCTURE_OPTIMIZE
Command103=ID_STRUCTURE_CHECKFACES
Command104=ID_STRUCTURE_CHECKTEXTURES
Command105=ID_STRUCTURE_CENTERALL
Command106=ID_STRUCTURE_TOPOLOGY_FINDNONCLOSED
Command107=ID_STRUCTURE_TOPOLOGY_CLOSE
Command108=ID_STRUCTURE_TOPOLOGY_FINDCOMPONENTS
Command109=ID_STRUCTURE_TOPOLOGY_SPLIT
Command110=ID_STRUCTURE_CONVEXITY_FINDNONCONVEXITIES
Command111=ID_STRUCTURE_CONVEXITY_CONVEXHULL
Command112=ID_STRUCTURE_CONVEXITY_COMPONENTCONVEXHULL
Command113=ID_STRUCTURE_CONVEXITY_SELECTHALFSPACE
Command114=ID_STRUCTURE_CARVE
Command115=ID_STRUCTURE_TRIANGULATE
Command116=ID_STRUCTURE_TRIANGULATE2
Command117=ID_STRUCTURE_SQARIZE
Command118=ID_STRUCTURE_SQUARIZEALLLODS
Command119=ID_STRUCTURE_PINCULL
Command120=ID_STRUCTURE_MULTIPLEPINCULL
Command121=ID_POINTS_FIRST
Command122=ID_POINTS_NEXT
Command123=ID_POINTS_PREV
Command124=ID_STRUCTURE_FINDISOLATEDPOINTS
Command125=ID_TRANSF_MOVE
Command126=ID_TRANSF_ROTATE
Command127=ID_TRANSF_SCALE
Command128=ID_TRANS2_ROTATE
Command129=ID_TRANS2_SCALE
Command130=ID_TRANS2_MIRRORX
Command131=ID_TRANS2_MIRRORY
Command132=ID_POINTS_MERGE
Command133=ID_POINTS_MERGENEAR
Command134=ID_POINTS_REMOVE
Command135=ID_POINTS_FLATTEN
Command136=ID_POINTS_LOCKNORMALS
Command137=ID_POINTS_UNIFORMSCALE
Command138=ID_POINTS_MOVEBYNORMALS
Command139=ID_POINTS_PROPERTIES
Command140=ID_CREATE_TRIANGLE
Command141=ID_CREATE_QUAD
Command142=ID_CREATE_STRIP
Command143=ID_CREATE_FAN
Command144=ID_FACES_REVERSE
Command145=ID_FACES_UNCROSS
Command146=ID_FACES_REMOVE
Command147=ID_FACES_SPLITHOLE
Command148=ID_FACES_MOVETOP
Command149=ID_FACES_MOVEBOTTOM
Command150=ID_FACES_MOVETONEXTALPHA
Command151=ID_FACES_MOVETOPREVALPHA
Command152=ID_SURFACES_RECALCULATENORMALS
Command153=ID_FACE_PROPERTIES
Command154=ID_SURFACES_SHARPEDGE
Command155=ID_SURFACES_SMOOTHEDGE
Command156=ID_SURFACES_UNDOGIZMO
Command157=ID_SURFACES_RESETGISMO
Command158=ID_SURFACES_SHOWGISMO
Command159=ID_SURFACES_EDITGISMO
Command160=ID_SURFACES_GIZMOMAPPING
Command161=ID_SURFACES_CYLMAP
Command162=ID_SURFACES_BACKGROUNDTEXTURE
Command163=ID_SURFACES_BACKGROUNDMAPPING
Command164=ID_SURFACES_UNWRAP
Command165=ID_SURFACES_CHILLISKINNER
Command166=ID_SURFACES_BGRFROMFACE
Command167=ID_SURFACES_BGRFROMSEL
Command168=ID_SURFACES_SETAUTOMAPSELECTION
Command169=ID_SURFACES_SLUCOVANI
Command170=ID_SURFACES_MERGETEXTURESALLLODS
Command171=ID_SURFACES_TEXELCALCULATIONS
Command172=ID_STRUCTURE_MATCHDIRECTION
Command173=ID_TOOLS_CALCULATOR
Command174=IDS_TOOLS_ANIMNORM
Command175=ID_TOOLS_COPYWEIGHTS
Command176=ID_TOOLS_FIND3WEIGHTS
Command177=ID_TOOLS_WEIGHTAUTOMATION
Command178=ID_TOOLS_ADAPTOVANIANIMACE
Command179=ID_TOOLS_MASSTEXTUREMATERIALRENAMING
Command180=ID_VIEW_STANDARD
Command181=ID_WINDOW_SPLIT
Command182=ID_VIEW_STATUS_BAR
Command183=ID_VIEW_TOOLBAR
Command184=ID_VIEW_CONTROL1
Command185=ID_VIEW_EDITBAR
Command186=ID_VIEW_NAMEDPROPERTIES
Command187=ID_VIEW_SELLIST
Command188=ID_VIEW_TEXLIST
Command189=ID_VIEW_USERVIEW
Command190=ID_VIEW_LODLIST
Command191=ID_VIEW_PTINFOBAR
Command192=ID_VIEW_HISTORY
Command193=ID_VIEW_ANIMATIONS
Command194=ID_WINDOW_MASS
Command195=ID_WINDOW_EXTRUDE
Command196=ID_WINDOW_MEASURE
Command197=ID_WINDOW_SELECTIONOPERATORS
Command198=ID_MACRO_RECORDMACRO
Command199=ID_MACRO_PLAYLASTMACRO
Command200=ID_MACRO_SAVELASTMACROAS
Command201=ID_MACRO_MACROMANAGER
Command202=ID_MACRO_PREDEFINEDMACROS
Command203=ID_APP_ABOUT
CommandCount=203

[MNU:IDR_TEXTURE_POPUP (English (U.S.) - FULLVER)]
Type=1
Class=?
Command1=ID_TEX_UNDO
Command2=ID_TEX_LOAD
Command3=ID_TEX_ASPEC
Command4=ID_TEX_ADD
Command5=ID_TEX_SELECT
Command6=ID_TEX_MIRROR_X
Command7=ID_TEX_MIRROR_Y
Command8=ID_TEX_ROTATER
Command9=ID_TEX_ROTATEL
Command10=ID_TEX_GRAY
Command11=ID_TEX_HIDE
Command12=ID_TEX_TRANSP100
Command13=ID_SURFACES_BGRFROMFACE
Command14=ID_SURFACES_SETAUTOMAPSELECTION
Command15=ID_TEX_UNLOAD
CommandCount=15

[TB:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_EDIT_UNDO
Command8=ID_EDIT_REDO
Command9=ID_VIEW_RESTARTEXTERNAL
CommandCount=9

[TB:IDR_CONTROL1 (English (U.S.))]
Type=1
Class=CObjektiv2View
Command1=ID_BUTTON33255
Command2=ID_VIEW_TOP
Command3=ID_VIEW_LEFT
Command4=ID_VIEW_FRONT
Command5=ID_VIEW_PROJECTION
Command6=ID_VIEW_LOOKATFACE
Command7=ID_LOCK_XY
Command8=ID_LOCK_XZ
Command9=ID_LOCK_YZ
Command10=ID_LOCK_X
Command11=ID_LOCK_Y
Command12=ID_LOCK_Z
Command13=ID_LOCK_ROTATION
Command14=ID_VIEW_PINCAMER
Command15=ID_VIEW_SOLIDFILL
Command16=ID_VIEW_WIRESINSOLID
Command17=ID_VIEW_FACECULL
Command18=ID_VIEW_GRID
Command19=ID_SHOW_NORMALS
Command20=ID_VIEW_VIEWERS_FREEZEANIMATION
CommandCount=20

[TB:IDR_EDITBAR (English (U.S.))]
Type=1
Class=?
Command1=ID_EDIT_SELECTOBJECTS
Command2=ID_EDIT_SELECTVERTICES
Command3=ID_EDIT_TOUCH
Command4=ID_EDIT_PIN
Command5=ID_VIEW_ZOOM
Command6=ID_TRANS2_ROTATE
Command7=ID_TRANS2_SCALE
Command8=ID_TRANS2_MIRRORX
Command9=ID_TRANS2_MIRRORY
Command10=ID_FACE_PROPERTIES
Command11=ID_POINTS_PROPERTIES
Command12=ID_VIEW_COLORIZEOBJECTS
Command13=ID_SURFACES_BACKGROUNDTEXTURE
CommandCount=13

[TB:IDR_SELOPERATIONS (English (U.S.) - FULLVER)]
Type=1
Class=?
Command1=ID_SELOP_NONE
Command2=ID_SELOP_UNION
Command3=ID_SELOP_COMPLEMENT
Command4=ID_SELOP_INTERSECTION
Command5=ID_SELOP_COMPINTER
Command6=ID_SELOP_COMBINE
Command7=ID_SELOP_CLEAR
CommandCount=7

[TB:IDR_MAINFRAME (English (U.S.) - FULLVER)]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_EDIT_UNDO
Command8=ID_EDIT_REDO
Command9=ID_VIEW_RESTARTEXTERNAL
Command10=ID_VIEW_RUNINTERNALVIEWER
Command11=ID_MACRO_RECORDMACRO
Command12=ID_MACRO_PLAYLASTMACRO
CommandCount=12

[TB:IDR_EDITBAR (English (U.S.) - FULLVER)]
Type=1
Class=?
Command1=ID_EDIT_SELECTOBJECTS
Command2=ID_EDIT_SELECTVERTICES
Command3=ID_EDIT_TOUCH
Command4=ID_EDIT_EDITMODES_TOUCHEDGES
Command5=ID_EDIT_PIN
Command6=ID_VIEW_ZOOM
Command7=ID_EDIT_LASO
Command8=ID_POINTS_UNIFORMSCALE
Command9=ID_POINTS_MOVEBYNORMALS
Command10=ID_TRANS2_ROTATE
Command11=ID_TRANS2_SCALE
Command12=ID_TRANS2_MIRRORX
Command13=ID_TRANS2_MIRRORY
Command14=ID_TRANSF_MOVE
Command15=ID_TRANSF_ROTATE
Command16=ID_TRANSF_SCALE
Command17=ID_FACE_PROPERTIES
Command18=ID_POINTS_PROPERTIES
Command19=ID_VIEW_COLORIZEOBJECTS
Command20=ID_SURFACES_BACKGROUNDTEXTURE
CommandCount=20

[ACL:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_STRUCTURE_TRIANGULATE2
Command2=ID_VIEW_NAMEDPROPERTIES
Command3=ID_SURFACES_BACKGROUNDTEXTURE
Command4=ID_EDIT_SELECT_ALL
Command5=ID_SURFACES_BACKGROUNDMAPPING
Command6=ID_SURFACES_BGRFROMFACE
Command7=ID_EDIT_CENTERPIN
Command8=ID_EDIT_COPY
Command9=ID_VIEW_PTINFOBAR
Command10=ID_EDIT_LOCKPIN
Command11=ID_FACES_REMOVE
Command12=ID_EDIT_DESELECT
Command13=ID_POINTS_MERGE
Command14=ID_FACE_PROPERTIES
Command15=ID_VIEW_EDITBAR
Command16=ID_POINTS_PROPERTIES
Command17=ID_VIEW_FACECULL
Command18=ID_SURFACES_GIZMOMAPPING
Command19=ID_VIEW_HIDESEL
Command20=ID_VIEW_HISTORY
Command21=ID_VIEW_UNHIDESEL
Command22=ID_SURFACES_SMOOTHEDGE
Command23=ID_EDIT_INVERTSELECTION
Command24=ID_EDIT_EDITMODES_TOUCHEDGES
Command25=ID_EDIT_LASO
Command26=ID_VIEW_LOCKSEL
Command27=ID_VIEW_SELLIST
Command28=ID_VIEW_LODLIST
Command29=ID_VIEW_UNLOCK
Command30=ID_SURFACES_MAPPING
Command31=ID_WINDOW_MASS
Command32=ID_WINDOW_MEASURE
Command33=ID_POINTS_MERGE
Command34=ID_FILE_NEW
Command35=ID_EDIT_SELECTOBJECTS
Command36=ID_FILE_OPEN
Command37=ID_VIEW_COLORIZEOBJECTS
Command38=ID_POINTS_FLATTEN
Command39=ID_VIEW_NAMEDPROPERTIES
Command40=ID_FACES_UNCROSS
Command41=ID_EDIT_PIN
Command42=ID_VIEW_VIEWERS_FOCUSSELECTIONS
Command43=ID_FILE_SAVE
Command44=ID_WINDOW_SPLIT
Command45=ID_EDIT_TOUCH
Command46=ID_VIEW_TOOLBAR
Command47=ID_VIEW_TEXLIST
Command48=ID_POINTS_TRANSFORM
Command49=ID_SURFACES_SHARPEDGE
Command50=ID_SURFACES_UNWRAP
Command51=ID_EDIT_SELECTVERTICES
Command52=ID_EDIT_PASTE
Command53=ID_VIEW_ZOOMIN
Command54=ID_EDIT_UNDO
Command55=ID_EDIT_DELETE
Command56=ID_EDIT_CUT
Command57=ID_FACES_MOVEBOTTOM
Command58=ID_EDIT_SELECTOBJECTS
Command59=ID_EDIT_SELECTFACES
Command60=ID_EDIT_SELECTVERTICES
Command61=ID_SURFACES_RECALCULATENORMALS
Command62=ID_CREATE_FACE
Command63=ID_CREATE_BOX
Command64=ID_CREATE_CYLINDER
Command65=ID_VIEW_STANDARD
Command66=ID_POINTS_FIRST
Command67=ID_FACES_MOVETOP
Command68=ID_EDIT_INSERTPOINT
Command69=ID_EDIT_COPY
Command70=ID_POINTS_NEWPOINTATPIN
Command71=ID_EDIT_PASTE
Command72=ID_VIEW_PAGEZOOMSEL
Command73=ID_VIEW_PAGEZOOM
Command74=ID_VIEW_CENTERZOOM_AUTO
Command75=ID_POINTS_NEXT
Command76=ID_FACES_MOVETOPREVALPHA
Command77=ID_POINTS_PREV
Command78=ID_FACES_MOVETONEXTALPHA
Command79=ID_VIEW_PINCAMER
Command80=ID_VIEW_ZOOMOUT
Command81=ID_NEXT_PANE
Command82=ID_PREV_PANE
Command83=ID_FACES_REVERSE
Command84=ID_EDIT_SELECTONESIDE
Command85=ID_EDIT_LINEARDEFORM
Command86=ID_SET_LOCKX
Command87=ID_EDIT_CUT
Command88=ID_SET_LOCKY
Command89=ID_SET_LOCKZ
Command90=ID_EDIT_UNDO
Command91=ID_STRUCTURE_TRIANGULATE
CommandCount=91

[ACL:IDR_MAINFRAME (English (U.S.) - FULLVER)]
Type=1
Class=?
Command1=ID_STRUCTURE_TRIANGULATE
Command2=ID_VIEW_NAMEDPROPERTIES
Command3=ID_SURFACES_BACKGROUNDTEXTURE
Command4=ID_EDIT_SELECT_ALL
Command5=ID_SURFACES_BACKGROUNDMAPPING
Command6=ID_SURFACES_BGRFROMFACE
Command7=ID_POINTS_BEVEL
Command8=ID_EDIT_CENTERPIN
Command9=ID_EDIT_COPY
Command10=ID_EDIT_LOCKPIN
Command11=ID_FACES_REMOVE
Command12=ID_EDIT_DESELECT
Command13=ID_POINTS_MERGE
Command14=ID_FACE_PROPERTIES
Command15=ID_VIEW_EDITBAR
Command16=ID_POINTS_PROPERTIES
Command17=ID_VIEW_FACECULL
Command18=ID_FACES_EXTRUDE
Command19=ID_WINDOW_EXTRUDE
Command20=ID_VIEW_HIDESEL
Command21=ID_VIEW_UNHIDESEL
Command22=ID_SURFACES_SMOOTHEDGE
Command23=ID_EDIT_INVERTSELECTION
Command24=ID_EDIT_EDITMODES_TOUCHEDGES
Command25=ID_EDIT_LASO
Command26=ID_VIEW_LOCKSEL
Command27=ID_VIEW_UNLOCK
Command28=ID_SURFACES_GIZMOMAPPING
Command29=ID_SURFACES_EDITGISMO
Command30=ID_POINTS_MERGE
Command31=ID_SURFACES_RESETGISMO
Command32=ID_EDIT_PAINTVERTICES
Command33=ID_FILE_NEW
Command34=ID_POINTS_LOCKNORMALS
Command35=ID_EDIT_SELECTOBJECTS
Command36=ID_FILE_OPEN
Command37=ID_VIEW_COLORIZEOBJECTS
Command38=ID_POINTS_FLATTEN
Command39=ID_FILE_PRINT
Command40=ID_MACRO_PLAYLASTMACRO
Command41=ID_FACES_UNCROSS
Command42=ID_EDIT_PIN
Command43=ID_MACRO_RECORDMACRO
Command44=ID_VIEW_VIEWERS_FOCUSSELECTIONS
Command45=ID_FILE_SAVE
Command46=ID_WINDOW_SPLIT
Command47=ID_EDIT_TOUCH
Command48=ID_VIEW_TOOLBAR
Command49=ID_POINTS_TRANSFORM
Command50=ID_SURFACES_SHARPEDGE
Command51=ID_SURFACES_UNWRAP
Command52=ID_EDIT_SELECTVERTICES
Command53=ID_EDIT_PASTE
Command54=ID_VIEW_ZOOMIN
Command55=ID_EDIT_UNDO
Command56=ID_EDIT_DELETE
Command57=ID_POINTS_REMOVE
Command58=ID_EDIT_CUT
Command59=ID_FACES_MOVEBOTTOM
Command60=ID_VIEW_TEXLIST
Command61=ID_WINDOW_SELECTIONOPERATORS
Command62=ID_WINDOW_MEASURE
Command63=ID_EDIT_SELECTOBJECTS
Command64=ID_VIEW_ANIMATIONS
Command65=ID_EDIT_SELECTFACES
Command66=ID_VIEW_PTINFOBAR
Command67=ID_EDIT_SELECTVERTICES
Command68=ID_VIEW_HISTORY
Command69=ID_SURFACES_RECALCULATENORMALS
Command70=ID_VIEW_SELLIST
Command71=ID_CREATE_FACE
Command72=ID_VIEW_LODLIST
Command73=ID_CREATE_BOX
Command74=ID_WINDOW_MASS
Command75=ID_CREATE_CYLINDER
Command76=ID_VIEW_NAMEDPROPERTIES
Command77=ID_VIEW_STANDARD
Command78=ID_POINTS_FIRST
Command79=ID_FACES_MOVETOP
Command80=ID_EDIT_INSERTPOINT
Command81=ID_EDIT_COPY
Command82=ID_POINTS_NEWPOINTATPIN
Command83=ID_EDIT_PASTE
Command84=ID_VIEW_PAGEZOOMSEL
Command85=ID_VIEW_PAGEZOOM
Command86=ID_VIEW_CENTERZOOM_AUTO
Command87=ID_POINTS_NEXT
Command88=ID_FACES_MOVETOPREVALPHA
Command89=ID_STRUCTURE_TOPOLOGY_SPLIT
Command90=ID_POINTS_PREV
Command91=ID_FACES_MOVETONEXTALPHA
Command92=ID_VIEW_PINCAMER
Command93=ID_VIEW_ZOOMOUT
Command94=ID_NEXT_PANE
Command95=ID_PREV_PANE
Command96=ID_FACES_REVERSE
Command97=ID_EDIT_SELECTONESIDE
Command98=ID_EDIT_LINEARDEFORM
Command99=ID_SET_LOCKX
Command100=ID_EDIT_CUT
Command101=ID_SET_LOCKY
Command102=ID_SET_LOCKZ
Command103=ID_EDIT_UNDO
Command104=ID_STRUCTURE_TRIANGULATE2
CommandCount=104

[DLG:IDD_MACROMANAGER]
Type=1
Class=?
ControlCount=34
Control1=IDC_STATIC,static,1342308352
Control2=IDC_FOLDERBROWSE,button,1342243072
Control3=IDC_STATIC,static,1342308352
Control4=IDC_MACROLIST,listbox,1352728835
Control5=IDC_STATIC,button,1342177287
Control6=IDC_VISIBLE,button,1342242819
Control7=IDC_GEOMETRIC,button,1342242819
Control8=IDC_SPECIALS,button,1342242819
Control9=IDC_NONSPECIALS,button,1342242819
Control10=IDC_COCKPITS,button,1342242819
Control11=IDC_COCKPITS_GEOMETRY,button,1342242819
Control12=IDC_COCKPITS_FIREGEOMETRY,button,1342242819
Control13=IDC_LODRANGE,button,1342242819
Control14=IDC_RANGEMIN,edit,1350631552
Control15=IDC_STATIC,static,1342308353
Control16=IDC_RANGEMAX,edit,1350631552
Control17=IDC_SELECTLODLIST,button,1342242819
Control18=IDC_LODLIST,listbox,1352728835
Control19=IDC_STATIC,button,1342177287
Control20=IDC_STOPCOND,button,1342308361
Control21=IDC_RADIO3,button,1342177289
Control22=IDC_RADIO4,button,1342177289
Control23=IDC_RADIO5,button,1342177289
Control24=IDC_STATIC,static,1342308352
Control25=IDC_FILES,listbox,1352728835
Control26=IDC_ADDFILE,button,1342242816
Control27=IDC_DELETEFILE,button,1342242816
Control28=IDC_START,button,1342242816
Control29=IDC_STATIC,button,1342177287
Control30=IDC_STATIC,static,1342308352
Control31=IDC_POSITION,combobox,1344340226
Control32=IDC_ASSIGN,button,1342242816
Control33=IDC_DELETEPOSITION,button,1342242816
Control34=IDC_MACRODESCRIPTION,edit,1350633600

[DLG:IDD_READONLY]
Type=1
Class=CDlgReadOnly
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_FILENAME,edit,1342244864
Control5=IDC_STATIC,static,1342308352
Control6=IDC_RUNEXPLORER,button,1342242816
Control7=IDC_STATIC,static,1342177283

[CLS:CDlgReadOnly]
Type=0
HeaderFile=DlgReadOnly.h
ImplementationFile=DlgReadOnly.cpp
BaseClass=CDialog
Filter=D
LastObject=CDlgReadOnly
VirtualFilter=dWC

[DLG:IDD_NORMALIZESTEP]
Type=1
Class=CNormalizeStepDlg
ControlCount=6
Control1=IDOK,button,1342373889
Control2=IDCANCEL,button,1342177280
Control3=IDC_LIST1,listbox,1352859907
Control4=IDC_RADIO1,button,1342373897
Control5=IDC_RADIO2,button,1342177289
Control6=IDC_RADIO3,button,1342177289

[CLS:CNormalizeStepDlg]
Type=0
HeaderFile=NormalizeStepDlg.h
ImplementationFile=NormalizeStepDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CNormalizeStepDlg

[DLG:IDD_SELECTSELECTION]
Type=1
Class=CSelectSelectionDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST,listbox,1352728835

[CLS:CSelectSelectionDlg]
Type=0
HeaderFile=SelectSelectionDlg.h
ImplementationFile=SelectSelectionDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CSelectSelectionDlg

