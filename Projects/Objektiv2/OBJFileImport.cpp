// OBJFileImport.cpp: implementation of the COBJFileImport class.
//
//////////////////////////////////////////////////////////////////////

/*#include "stdafx.h"*/
/*#include "objektiv2.h"*/
#include <projects/ObjektivLib/common.hpp>
#include <projects/ObjektivLib/ObjectData.h>
#include <projects/ObjektivLib/GlobalFunctions.h>
#include <projects/ObjektivLib/ObjToolTopology.h>
#include "OBJFileImport.h"

using namespace ObjektivLib;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COBJFileImport::_LexInfo COBJFileImport::lxinfo[]=
  {
  {"v",COBJFileImport::lx_Vertex},
  {"vn",COBJFileImport::lx_VNormal},
  {"vt",COBJFileImport::lx_VTexture},
  {"f",COBJFileImport::lx_Face},
  {"g",COBJFileImport::lx_BeginSelection},
  {"usemtl",COBJFileImport::lx_UseMat},
  };

COBJFileImport::COBJFileImport()
  {
  _scale=1.0f;
  _angle=-1;
  _invertx=_inverty=_invertz=false;
  }

COBJFileImport::~COBJFileImport()
  {

  }

const char * COBJFileImport::GetError(int err)
  {
  switch(err)
    {
    case -2: return "Excepting integer number";
    case -3: return "File integrity error: point's reference is out of range";
    case -4: return "File integrity error: texture's reference is out of range";
    case -5: return "File integrity error: normal's reference is out of range";
    case -6: return "Extra characters on line (maybe more vertices on face, than supported - four)";
    case -7: return "Extra characters on line";
    case -8: return "General read error (maybe float values are invalid)";
    default: return "Undetermined error";
    }
  }

static void EatEndOfLine(istream &fIn)
  {
  int p=fIn.get();
  while (p!=EOF && p!='\n' && isspace(p)) p=fIn.get();
  if (p!=EOF) fIn.putback((char)p);

  }

COBJFileImport::_LexSymb COBJFileImport::GetCommand(std::istream &fIn)
  {
  char buff[50];
  int i=0;
  int z;
  z=fIn.get();
  while (z!=EOF && !isspace(z) && i<49) 
    {
    buff[i++]=(char)z;
    z=fIn.get();
    }
  if (z!=EOF) fIn.putback((char)z);
  buff[i]=0;
  if (buff[0]==0) 
    if (z==EOF)
      return lx_EOF;
    else
      return lx_NONE;
  for (i=0;i<sizeof(lxinfo)/sizeof(lxinfo[0]);i++)    
    if (strcmp(lxinfo[i].text,buff)==0) return  lxinfo[i].value;
  return lx_Unknown;
  }

static int ReadFaceInfo(istream &fIn, int count, int *point, int *normal, int *texture)
  {
  int i;
  for (i=0;i<count;i++)
    {
    EatEndOfLine(fIn);
    if (fIn.peek()=='\n') break; //end of line, this is the end
    int v,vn=0,vt=0;
    int peek;                 //  1/2/3
    fIn>>v;                    //  /2/3
    if (!fIn) return -2;      //
    peek=fIn.peek();          //  /2/3
    if (peek=='/')            
      {
      fIn.ignore();           //  2/3
      peek=fIn.peek();        
      if (!isspace(peek))
        {        
        if (peek!='/') fIn>>vt; else vt=0; //   2/3
        if (!fIn) return -2;        //   /3
        peek=fIn.peek();           
        if (peek=='/')
          {
          fIn.ignore();             //   3
          fIn>>vn;                  //   3
          if (!fIn) return -2;      
          }
        }
      }        
    point[i]=v-1;
    normal[i]=vn-1;
    texture[i]=vt-1;
    }
  return i;
  }


int COBJFileImport::LoadFile(std::istream &fIn, int *errline)
  {
  AutoArray<VecT> textures;
  AutoArray<int> merged;
  RString curselname;
  RString curtexture;
  int lastselp=0;
  int lastself=0;
  int ln;
  float ix=_scale*(_invertx?-1:1);
  float iy=_scale*(_inverty?-1:1);
  float iz=_scale*(_invertz?-1:1);
  if (errline==NULL) errline=&ln;
  *errline=0;
  EatEndOfLine(fIn);
  _LexSymb symb=GetCommand(fIn);
  while (symb!=lx_EOF)
    {

    switch (symb)
      {
      case lx_Vertex:
        {
        int i;
        float x,y,z;
        fIn>>x>>y>>z;
        x*=ix;
        y*=iy;
        z*=iz;
        for (i=object.NPoints()-1;i>=lastselp;i--)
          {
          PosT &ps=object.Point(i);
          if (fabs(x-ps[0])<0.00001f && fabs(y-ps[1])<0.00001f && fabs(z-ps[2])<0.00001f) break;
          }
        if (i<lastselp)
          {
          merged.Append()=object.NPoints();
          PosT &ps=*object.NewPoint();
          ps[0]=x;
          ps[1]=y;
          ps[2]=z;
          }
        else
          merged.Append()=i;
        }
        break;
      case lx_VNormal:
        {
        float x,y,z;
        fIn>>x>>y>>z;
        VecT &ps=*object.NewNormal();
        ps[0]=-x*ix;
        ps[1]=-y*iy;
        ps[2]=-z*iz;
        }
        break;
      case lx_VTexture:
        {
        float x,y;
        fIn>>x>>y;
        VecT &ps=textures.Append();
        ps[0]=x;
        ps[1]=y;      
        EatEndOfLine(fIn);
        if (fIn.peek()!='\n') fIn>>x;
        break;
        }
      case lx_Face:
        {
        int points[4];
        int normals[4];
        int texs[4];
        int res=ReadFaceInfo(fIn,4,points,normals,texs);
        if (res<0) return res;
        FaceT fc(object,object.NewFace());
        fc.SetN(res);
        for (int i=0;i<res;i++)
          {
          int vsp=i;
          if (points[i]<0 || points[i]>=merged.Size()) return -3;          
          if (normals[i]<-1 || normals[i]>=object.NNormals()) return -4;
          if (texs[i]<-1 || texs[i]>=textures.Size()) return -5;
          fc.SetPoint(vsp,merged[points[i]]);
          fc.SetNormal(vsp,normals[i]);
          fc.SetTexture(curtexture);
          if (texs[i]>=0)
            {
            fc.SetU(vsp,textures[texs[i]][0]);
            fc.SetV(vsp,1.0f-textures[texs[i]][1]);
            }
          }
        EatEndOfLine(fIn);
        if (fIn.peek()!='\n') return -6;
        }
        break;
      case lx_UseMat:
        {
          int i=fIn.get();
          ws(fIn);
          strstream tex;
          tex<<'-';
          while (i!=EOF && i!='\n') {tex<<(char)i;i=fIn.get();}
          fIn.putback('\n');
          tex<<(char)0;
          curtexture=tex.str();
          tex.freeze(false);
        }
      break;
      case lx_BeginSelection:
        {
          if (lastselp!=object.NPoints() && lastself!=object.NFaces())
          {
            object.ClearSelection();
            for (int pt=lastselp;pt<object.NPoints();pt++) object.PointSelect(pt);
            for (int fc=lastself;fc<object.NFaces();fc++) object.FaceSelect(fc);
            lastselp=object.NPoints();
            lastself=object.NFaces();
            object.SaveNamedSel("-"+curselname);
          }
          int i=fIn.get();
          ws(fIn);
          strstream tex;
          tex<<'-';
          while (i!=EOF && i!='\n') {tex<<(char)i;i=fIn.get();}
          fIn.putback('\n');
          tex<<(char)0;
          curselname=tex.str();
          tex.freeze(false);
        }
      break;
      case lx_Unknown:
      default:
        fIn.ignore(0x7FFFFFFF,'\n');
        fIn.putback('\n');
        break;
      }
    if (!fIn) return -8;
    EatEndOfLine(fIn);
    if (fIn.peek()!='\n') return -7;
    errline[0]++;
    fIn.ignore();
    symb=GetCommand(fIn);
    }
  ObjToolTopology &topo=object.GetTool<ObjToolTopology>();
  if (_angle>=0) 
  {
    for (int i=0;i<topo.NFaces();i++) topo.FaceSelect(i);
    topo.SelectPointsFromFaces();
    topo.AutoSharpEdges(_angle*3.14159265f/180.0f);
  }
  return 0;
  }

void COBJFileImport::SetScale(float s)
  {
  _scale=s;
  }

void COBJFileImport::SetInvert(bool ix,bool iy,bool iz)
  {
  _invertx=ix;
  _inverty=iy;
  _invertz=iz;
  }