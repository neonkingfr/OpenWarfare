#pragma once

// CFileDialogEx
class IFileDlgHistory
{
public:
  ///Stores history folder
  virtual void StoreHistoryFolder(const char *folderName)=0;
  ///Returns next history folder
  /**
   * To enumeration, set @b index to zero and repeat this function, until NULL is not returned 
   * @param index index of history folder
   * @retval string folder name at position
   * @retval 0 if there are no more folder
   * @note Returned pointer can be invalidated by next GetNextHistoryFolder call. Do not
   * store the pointer.
   */
  virtual const char *GetNextHistoryFolder(int index)=0;

  virtual HBITMAP GetHistoryBitmap()=0;
  virtual HBITMAP GetHistoryMenuBitmap()=0;
};
