// DlgProxyLevel.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgProxyLevel.h"
#include ".\dlgproxylevel.h"
#include "Objektiv2Doc.h"
#include <math.h>

DlgProxyLevel *DlgProxyLevel::oneInstance=0;


// DlgProxyLevel dialog

IMPLEMENT_DYNAMIC(DlgProxyLevel, CDialog)
DlgProxyLevel::DlgProxyLevel(CWnd* pParent /*=NULL*/)
	: CDialog(DlgProxyLevel::IDD, pParent)
{
}

DlgProxyLevel::~DlgProxyLevel()
{
}

void DlgProxyLevel::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_SLIDER1, wSlider);
}


BEGIN_MESSAGE_MAP(DlgProxyLevel, CDialog)
  ON_WM_HSCROLL()
END_MESSAGE_MAP()


// DlgProxyLevel message handlers

BOOL DlgProxyLevel::OnInitDialog()
{
  CDialog::OnInitDialog();

  if (oneInstance) oneInstance->DestroyWindow();
  oneInstance=this;

  wSlider.SetRange(-600,600,TRUE);
  wSlider.SetPos(-600);
  wSlider.SetPos(600);
  wSlider.SetPos(toInt(log(document->_proxyLevel)*100));

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgProxyLevel::PostNcDestroy()
{
  CDialog::PostNcDestroy();
  oneInstance=0;
  delete this;
}

void DlgProxyLevel::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  int pos=wSlider.GetPos();
  document->_proxyLevel=exp((float)pos/100);
  document->UpdateAllViews(0);

  CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void DlgProxyLevel::OnOK()
{
  DestroyWindow();
}

void DlgProxyLevel::OnCancel()
{
  DestroyWindow();
}
