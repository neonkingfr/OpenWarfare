// LexAnl.h: interface for the CLexAnl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEXANL_H__87865546_BFE9_11D4_B399_00C0DFAE7D0A__INCLUDED_)
#define AFX_LEXANL_H__87865546_BFE9_11D4_B399_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <iostream>
using namespace std;

enum LexSymbol
  {
  lexUnknown,lexPartOf, //unknown symbol && part of symbol
  lexEof,lexLP,lexRP,lexPlus,lexMinus,lexMult,lexDiv,lexAssign,
  lexEqual,lexGreat,lexLower,lexGEq,lexLEq,lexNoEq,lexAnd,lexOr,lexXor,lexRad,
  lexCommdiv,
  lexComma,
  lexNumber, //float number
  lexVarible, //A ... Z varible
  lexVertX,lexVertY,lexVertZ, //vertex source & target
  lexFaceTu,lexFaceTv,lexWeight, //face TU & TV & selection weight
  lexFSin,lexFCos,lexFTan,lexFExp,lexFLn,lexFSqr,lexFSqrt,lexFAtan,
  lexIff,lexRepeat,lexWhile,lexFor,
  };

//--------------------------------------------------

class CLexAnl  
  {
  istream &istr;
  LexSymbol top;
  float rnumb;
  char varible;
  LexSymbol ReadSymb();
  public:	
    CLexAnl(istream &str):istr(str)
      {top=ReadSymb();}
    void ReadNext() 
      {top=ReadSymb();}
    LexSymbol GetAct() 
      {return top;}
    float GetNumber() 
      {return rnumb;}
    char GetVarible() 
      {return varible;}
  };

//--------------------------------------------------

#endif // !defined(AFX_LEXANL_H__87865546_BFE9_11D4_B399_00C0DFAE7D0A__INCLUDED_)
