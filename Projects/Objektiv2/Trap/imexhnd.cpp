//-----------------------------------------------------------------------------
//
// FILE: IMEXHND.CPP
// 
// Matt Pietrek
// Microsoft Systems Journal, April 1997
//
// (c) Interactive Magic (1997)
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------
// Version tracking information
//-----------------------------------------------------------------------

#include "..\stdafx.h"

#include <tchar.h>
#include <time.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "imexhnd.h"
//#include "debugTrap.hpp"

#include "mapFile.hpp"

//============================== Global Variables =============================

//
// Declare the static variables of the DebugExceptionTrap class
//

#define BREAK() ASSERT(0)

bool IsDebugger()
  {
  static bool value=false;
  static bool first=true;
  if (first)
    {
    HMODULE kernel32 = LoadLibrary("kernel32.dll");
    if (kernel32)
      {
      typedef BOOL IsDebuggerPresentProc(VOID);
      IsDebuggerPresentProc *debPresent = (IsDebuggerPresentProc *)
        (
          GetProcAddress(kernel32,"IsDebuggerPresent")
            );
      if (debPresent)
        {
        value = debPresent()!=FALSE;
        if (value)
          {
          LogF("Starting debugger session");
          }
        }
      first=false;
      }
    }
  return value;
  }

//--------------------------------------------------

#if USE_IMAGE

DebugExceptionTrap::SYMINITIALIZEPROC DebugExceptionTrap::_SymInitialize = 0;
DebugExceptionTrap::SYMCLEANUPPROC DebugExceptionTrap::_SymCleanup = 0;
DebugExceptionTrap::STACKWALKPROC DebugExceptionTrap::_StackWalk = 0;

DebugExceptionTrap::SYMFUNCTIONTABLEACCESSPROC
  DebugExceptionTrap::_SymFunctionTableAccess = 0;

DebugExceptionTrap::SYMGETMODULEBASEPROC
  DebugExceptionTrap::_SymGetModuleBase = 0;

DebugExceptionTrap::SYMGETSYMFROMADDRPROC
  DebugExceptionTrap::_SymGetSymFromAddr = 0;

#endif

DebugExceptionTrap GDebugExceptionTrap;  // Declare global instance of class

//============================== Class Methods =============================

//=============
// Constructor 
//=============
DebugExceptionTrap :: DebugExceptionTrap( )
  {
  if (!IsDebugger())
    {
    m_header = false; // header printer
    // Install the unhandled exception filter function
    m_previousFilter = SetUnhandledExceptionFilter(ExceptionCallback);
    
    // Figure out what the report file will be named, and store it away
    GetModuleFileName( 0, m_szLogFileName, MAX_PATH );
    
    // Look for the '.' before the "EXE" extension.  Replace the extension
    // with "RPT"
    PTSTR pszDot = _tcsrchr( m_szLogFileName, _T('.') );
    if ( pszDot )
      {
      pszDot++;   // Advance past the '.'
      if ( _tcslen(pszDot) >= 3 )
        _tcscpy( pszDot, _T("RPT") );   // "RPT" -> "Report"
      }
    }
  }

//--------------------------------------------------

//============
// Destructor 
//============
DebugExceptionTrap :: ~DebugExceptionTrap( )
  {
  if (m_previousFilter)
    {
    SetUnhandledExceptionFilter( m_previousFilter );
    }
  }

//--------------------------------------------------

//==============================================================
// Lets user change the name of the report file to be generated 
//==============================================================
void DebugExceptionTrap :: SetLogFileName( PTSTR pszLogFileName )
  {
  _tcscpy( m_szLogFileName, pszLogFileName );
  }

//--------------------------------------------------

//extern void PrintMissionInfo( HANDLE file );
//extern void PrintNetworkInfo( HANDLE file );

// file operations
void DebugExceptionTrap::OpenLogFile()
  {
  if (!IsDebugger())
    {
    m_hReportFile = CreateFile
      (
        m_szLogFileName,
    GENERIC_WRITE,
    0,
    0,
    OPEN_ALWAYS, //CREATE_ALWAYS,
    FILE_FLAG_WRITE_THROUGH,
    0
      );
    }
  
  if ( m_hReportFile )
    {
    SetFilePointer( m_hReportFile, 0, 0, FILE_END );
    
    PrintHeader();
    }
  }

//--------------------------------------------------

void DebugExceptionTrap :: PrintHeader()
  {
  if( !m_header )
    {
    m_header=true;
    // scan session information
    
    TCHAR exeName[MAX_PATH];
    GetModuleFileName( 0, exeName, MAX_PATH );
    _tprintf(_T("Posli tenhle report na adresu xnovako1@volny.cz"));
    _tprintf(_T("                              ~~~~~~~~~~~~~~~~~"));
    _tprintf( _T("\r\n") );
    _tprintf( _T("\r\n") );
    _tprintf( _T("=====================================================================\r\n") );
    _tprintf( _T("== %s\r\n"), exeName );
    _tprintf( _T("=====================================================================\r\n") );
    
    struct _stat exeStat;
    if( _stat(exeName,&exeStat)>=0 )
      {
      _tprintf( _T("Exe version: %s\r\n"), ctime(&exeStat.st_mtime) );
      }
    
    //		if (m_hReportFile) PrintMissionInfo(m_hReportFile);
    }
  }

//--------------------------------------------------

void DebugExceptionTrap :: CloseLogFile()
  {
  if ( m_hReportFile )
    {
    CloseHandle( m_hReportFile );
    m_hReportFile = 0;
    }
  }

//--------------------------------------------------

static char TextReported[256][256];
static int NTextReported;

void DebugExceptionTrap::ReportContext( const char *text, CONTEXT *context )
  {
  OpenLogFile();
  
  if( text ) _tprintf( _T("===%s====>>>>>>BEG\r\n"), text );
  
  PrintContext((void *)context->Eip,context);
  IntelStackWalk(context);
  
  if( text ) _tprintf( _T("===%s====>>>>>>END\r\n"), text );
  
  CloseLogFile();
  }

//--------------------------------------------------

void DebugExceptionTrap :: LogFSP( void *eip, DWORD *esp, const char *text, bool stack )
  {
  OpenLogFile();
  _tprintf( _T("%s\r\n"), text);
  if( stack )
    {
    if (IsDebugger())
      {
      BREAK();
      }
    
    // check if this text was already reported
    int i;
    for( i=0; i<NTextReported; i++ )
      {
      if( !strcmp(TextReported[i],text) ) break;
      }
    
    if( i>=NTextReported )
      {
      strcpy(TextReported[NTextReported++],text);
      // generate stack report
      IntelStackWalk((DWORD)eip,esp,(DWORD *)m_stackBottom);
      }
    }
  
  CloseLogFile();
  }

//--------------------------------------------------

void DebugExceptionTrap :: LogF( const char *text, bool stack )
  {
  // get eip, esp
  GDebugExceptionTrap.LogFSP(LogF,(DWORD *)&text,text,stack);
  }

//--------------------------------------------------

void SaveCrash();


//===========================================================
// Entry point where control comes on an unhandled exception 
//===========================================================

LONG WINAPI DebugExceptionTrap::ExceptionCallback( PEXCEPTION_POINTERS pExceptionInfo )
  {
  return GDebugExceptionTrap.UnhandledExceptionFilter(pExceptionInfo);
  }

//--------------------------------------------------

LONG DebugExceptionTrap :: UnhandledExceptionFilter( PEXCEPTION_POINTERS pExceptionInfo )
  {
  // no alive will arive any more
  //	GDebugger.NextAliveExpected(INT_MAX);
  
  OutputDebugString(_T("Unhandled exception."));
  OpenLogFile();
  if ( m_hReportFile )
    {
    GenerateExceptionReport( pExceptionInfo );
    }
  CloseLogFile();
  
  //SaveCrash(); // allow game to save its internal state
  
  if ( m_previousFilter )
    return m_previousFilter( pExceptionInfo );
  else
    return EXCEPTION_CONTINUE_SEARCH;
  }

//--------------------------------------------------

void DebugExceptionTrap::PrintBanner()
  {
  if (!m_hReportFile) return;
  
  char tmpBuf[32];
  // Start out with a banner
  _tprintf( _T("=======================================================\r\n") );
  _strdate(tmpBuf);
  _tprintf( _T("Date: %s  "), tmpBuf);
  _strtime(tmpBuf);
  _tprintf( _T("Time: %s\r\n"), tmpBuf);
  _tprintf( _T("-------------------------------------------------------\r\n") );
  
  //	PrintMissionInfo(m_hReportFile);
  }

//--------------------------------------------------

void DebugExceptionTrap::PrintContext( void *ip, CONTEXT *ctx )
  {
  TCHAR szFaultingModule[MAX_PATH];
  DWORD section, offset;
  GetLogicalAddress(  ip,
  szFaultingModule,
  sizeof( szFaultingModule ),
  section, offset );
  
  _tprintf( _T("Fault address:  %08X %02X:%08X %s\r\n"),
  ip,
  section, offset, szFaultingModule );
  
  char code[1024];
  
  *code = 0;
  int i;
  for (i=0; i<16; i++)
    {
    unsigned char *p=((unsigned char *)ip-16+i);
    if (IsBadReadPtr(p,1)) strcat(code," ??");
    else sprintf(code+strlen(code)," %2X",*p);
    }
  _tprintf( _T("Prev. code bytes:%s\r\n"), code );
  
  *code = 0;
  for (i=0; i<16; i++)
    {
    unsigned char *p=((unsigned char *)ip+i);
    if (IsBadReadPtr(p,1)) strcat(code," ??");
    else sprintf(code+strlen(code)," %2X",*p);
    }
  _tprintf( _T("Fault code bytes:%s\r\n"), code );
  
  // Show the registers
  #ifdef _M_IX86  // Intel Only!
  _tprintf( _T("\r\nRegisters:\r\n") );
  
  _tprintf(_T("EAX:%08X EBX:%08X\r\nECX:%08X EDX:%08X\r\nESI:%08X EDI:%08X\r\n"),
  ctx->Eax, ctx->Ebx, ctx->Ecx, ctx->Edx, ctx->Esi, ctx->Edi );
  
  _tprintf( _T("CS:EIP:%04X:%08X\r\n"), ctx->SegCs, ctx->Eip );
  _tprintf( _T("SS:ESP:%04X:%08X  EBP:%08X\r\n"),
  ctx->SegSs, ctx->Esp, ctx->Ebp );
  _tprintf( _T("DS:%04X  ES:%04X  FS:%04X  GS:%04X\r\n"),
  ctx->SegDs, ctx->SegEs, ctx->SegFs, ctx->SegGs );
  _tprintf( _T("Flags:%08X\r\n"), ctx->EFlags );
  
  #endif
  }

//--------------------------------------------------

//===========================================================================
// Open the report file, and write the desired information to it.  Called by 
// UnhandledExceptionFilter                                               
//===========================================================================

void DebugExceptionTrap :: GenerateExceptionReport(
                                                   PEXCEPTION_POINTERS pExceptionInfo )
    {
    PrintBanner();
    
    //		PrintNetworkInfo(m_hReportFile);
    
    PEXCEPTION_RECORD pExceptionRecord = pExceptionInfo->ExceptionRecord;
    
    // First print information about the type of fault
    _tprintf(   _T("Exception code: %08X %s\r\n"),
    pExceptionRecord->ExceptionCode,
    GetExceptionString(pExceptionRecord->ExceptionCode) );
    
    // Now print information about where the fault occured
    TCHAR szFaultingModule[MAX_PATH];
    DWORD section, offset;
    GetLogicalAddress(  pExceptionRecord->ExceptionAddress,
    szFaultingModule,
    sizeof( szFaultingModule ),
    section, offset );
    
    _tprintf( _T("Fault address:  %08X %02X:%08X %s\r\n"),
    pExceptionRecord->ExceptionAddress,
    section, offset, szFaultingModule );
    
    PrintContext(pExceptionRecord->ExceptionAddress,pExceptionInfo->ContextRecord);
    _tprintf( _T("=======================================================\r\n") );
    
    // Walk the stack using x86 specific code
    IntelStackWalk( pExceptionInfo->ContextRecord );
    
    _tprintf( _T("\r\n") );
    }

//--------------------------------------------------

//======================================================================
// Given an exception code, returns a pointer to a static string with a 
// description of the exception                                         
//======================================================================
LPTSTR DebugExceptionTrap :: GetExceptionString( DWORD dwCode )
  {
  #define EXCEPTION( x ) case EXCEPTION_##x: return _T(#x);
  
  switch ( dwCode )
    {
    EXCEPTION( ACCESS_VIOLATION )
      EXCEPTION( DATATYPE_MISALIGNMENT )
        EXCEPTION( BREAKPOINT )
          EXCEPTION( SINGLE_STEP )
            EXCEPTION( ARRAY_BOUNDS_EXCEEDED )
              EXCEPTION( FLT_DENORMAL_OPERAND )
                EXCEPTION( FLT_DIVIDE_BY_ZERO )
                  EXCEPTION( FLT_INEXACT_RESULT )
                    EXCEPTION( FLT_INVALID_OPERATION )
                      EXCEPTION( FLT_OVERFLOW )
                        EXCEPTION( FLT_STACK_CHECK )
                          EXCEPTION( FLT_UNDERFLOW )
                            EXCEPTION( INT_DIVIDE_BY_ZERO )
                              EXCEPTION( INT_OVERFLOW )
                                EXCEPTION( PRIV_INSTRUCTION )
                                  EXCEPTION( IN_PAGE_ERROR )
                                    EXCEPTION( ILLEGAL_INSTRUCTION )
                                      EXCEPTION( NONCONTINUABLE_EXCEPTION )
                                        EXCEPTION( STACK_OVERFLOW )
                                          EXCEPTION( INVALID_DISPOSITION )
                                            EXCEPTION( GUARD_PAGE )
                                              EXCEPTION( INVALID_HANDLE )
    }
  
  // If not one of the "known" exceptions, try to get the string
  // from NTDLL.DLL's message table.
  
  static TCHAR szBuffer[512] = 
    { 0 };
  
  FormatMessage(  FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_HMODULE,
  GetModuleHandle( _T("NTDLL.DLL") ),
  dwCode, 0, szBuffer, sizeof( szBuffer ), 0 );
  
  return szBuffer;
  }

//--------------------------------------------------

//==============================================================================
// Given a linear address, locates the module, section, and offset containing  
// that address.                                                               
//                                                                             
// Note: the szModule paramater buffer is an output buffer of length specified 
// by the len parameter (in characters!)                                       
//==============================================================================
BOOL DebugExceptionTrap :: GetLogicalAddress(
                                             PVOID addr, PTSTR szModule, DWORD len, DWORD& section, DWORD& offset )
    {
    MEMORY_BASIC_INFORMATION mbi;
    
    if ( !VirtualQuery( addr, &mbi, sizeof(mbi) ) )
      return FALSE;
    
    DWORD hMod = (DWORD)mbi.AllocationBase;
    
    if ( !GetModuleFileName( (HMODULE)hMod, szModule, len ) )
      return FALSE;
    
    // Point to the DOS header in memory
    PIMAGE_DOS_HEADER pDosHdr = (PIMAGE_DOS_HEADER)hMod;
    
    // From the DOS header, find the NT (PE) header
    PIMAGE_NT_HEADERS pNtHdr = (PIMAGE_NT_HEADERS)(hMod + pDosHdr->e_lfanew);
    
    PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( pNtHdr );
    
    DWORD rva = (DWORD)addr - hMod; // RVA is offset from module load address
    
    // Iterate through the section table, looking for the one that encompasses
    // the linear address.
    for (   unsigned i = 0;
    i < pNtHdr->FileHeader.NumberOfSections;
    i++, pSection++ )
      {
      DWORD sectionStart = pSection->VirtualAddress;
      DWORD sectionEnd = sectionStart
        + max(pSection->SizeOfRawData, pSection->Misc.VirtualSize);
      
      // Is the address in this section???
      if ( (rva >= sectionStart) && (rva <= sectionEnd) )
        {
        // Yes, address is in the section.  Calculate section and offset,
        // and store in the "section" & "offset" params, which were
        // passed by reference.
        section = i+1;
        offset = rva - sectionStart;
        return TRUE;
        }
      }
    
    return FALSE;   // Should never get here!
    }

//--------------------------------------------------

//============================================================
// Walks the stack, and writes the results to the report file 
//============================================================
void DebugExceptionTrap :: IntelPCPrint( MapFile &map, int pc )
  {
  TCHAR szModule[MAX_PATH] = _T("");
  DWORD section = 0, offset = 0;
  
  GetLogicalAddress((PVOID)pc, szModule,sizeof(szModule),section,offset );
  
  int nameValue=0;
  const char *name=map.MapNameFromLogical(offset,&nameValue);
  int nameOffset=offset-nameValue;
  
  
  if( section==1 && nameValue!=0 )
    {
    _tprintf( _T("%8X %8X %8X + %s\r\n"),
    pc, nameValue, nameOffset, name );
    }
  else
    {
    _tprintf( _T("%08X %04X:%08X       %s\r\n"),
    pc, section, offset, szModule );
    }
  }

//--------------------------------------------------

void DebugExceptionTrap :: IntelStackWalk
  (
  DWORD eip, DWORD *pStackTop, DWORD *pStackBot
  )
        {
        if( pStackBot<pStackTop ) pStackBot=pStackTop+4*1024; // in DWORDS
        
        _tprintf( _T("\r\nCall stack:\r\n") );
        _tprintf( _T("\r\nStack %08X %08X\r\n"), pStackTop,pStackBot );
        
        _tprintf( _T("Address  Logical            Function\r\n") );
        
        DWORD pc = eip;
        
        static MapFile map;
        map.ParseMapFile();
        
        _tprintf( _T("mapfile: %s (empty %d)\r\n"), map.GetName(), map.Empty() );
        
        
        
        int minPc=map.MinLogicalAddress();
        int maxPc=map.MaxLogicalAddress();
        
        int offset=map.MinPhysicalAddress()-map.MinLogicalAddress();
        IntelPCPrint(map,pc);
        
        if( map.Empty() ) return;
        
        for( PDWORD stack=pStackTop; stack<pStackBot; stack++ )
          {
          if( IsBadReadPtr(stack,4) ) break;
          pc = *stack;
          
          int logpc=pc-offset;
          if( logpc<minPc || logpc>=maxPc ) continue;
          
          // Can two DWORDs be read from the supposed frame address?          
          if ( IsBadCodePtr((int (__stdcall*)())pc) ) continue;
          
          IntelPCPrint(map,pc);
          }
        }

//--------------------------------------------------

void DebugExceptionTrap :: IntelStackWalk( PCONTEXT pContext )
  {
  IntelStackWalk(pContext->Eip,(PDWORD)pContext->Esp,(PDWORD)m_stackBottom);
  }

//--------------------------------------------------

//============================================================
// Walks the stack, and writes the results to the report file 
//============================================================
#if USE_IMAGE

void DebugExceptionTrap :: ImagehlpStackWalk( PCONTEXT pContext )
  {
  _tprintf( _T("\r\nCall stack (IMAGEHLP.DLL):\r\n") );
  
  _tprintf( _T("Address   Frame\r\n") );
  
  // Could use SymSetOptions here to add the SYMOPT_DEFERRED_LOADS flag
  
  STACKFRAME sf;
  memset( &sf, 0, sizeof(sf) );
  
  // Initialize the STACKFRAME structure for the first call.  This is only
  // necessary for Intel CPUs, and isn't mentioned in the documentation.
  sf.AddrPC.Offset       = pContext->Eip;
  sf.AddrPC.Mode         = AddrModeFlat;
  sf.AddrStack.Offset    = pContext->Esp;
  sf.AddrStack.Mode      = AddrModeFlat;
  sf.AddrFrame.Offset    = pContext->Ebp;
  sf.AddrFrame.Mode      = AddrModeFlat;
  
  while ( 1 )
    {
    if ( ! _StackWalk(  IMAGE_FILE_MACHINE_I386,
    GetCurrentProcess(),
    GetCurrentThread(),
    &sf,
    pContext,
    0,
    _SymFunctionTableAccess,
    _SymGetModuleBase,
    0 ) )
      break;
    
    if ( 0 == sf.AddrFrame.Offset ) // Basic sanity check to make sure
    break;                      // the frame is OK.  Bail if not.
    
    _tprintf( _T("%08X  %08X  "), sf.AddrPC.Offset, sf.AddrFrame.Offset );
    
    // IMAGEHLP is wacky, and requires you to pass in a pointer to a
    // IMAGEHLP_SYMBOL structure.  The problem is that this structure is
    // variable length.  That is, you determine how big the structure is
    // at runtime.  This means that you can't use sizeof(struct).
    // So...make a buffer that's big enough, and make a pointer
    // to the buffer.  We also need to initialize not one, but TWO
    // members of the structure before it can be used.
    
    BYTE symbolBuffer[ sizeof(IMAGEHLP_SYMBOL) + 512 ];
    PIMAGEHLP_SYMBOL pSymbol = (PIMAGEHLP_SYMBOL)symbolBuffer;
    pSymbol->SizeOfStruct = sizeof(symbolBuffer);
    pSymbol->MaxNameLength = 512;
    
    DWORD symDisplacement = 0;  // Displacement of the input address,
    // relative to the start of the symbol
    
    if ( _SymGetSymFromAddr(GetCurrentProcess(), sf.AddrPC.Offset,
    &symDisplacement, pSymbol) )
      {
      _tprintf( _T("%hs+%X\r\n"), pSymbol->Name, symDisplacement );
      
      }
    else    // No symbol found.  Print out the logical address instead.
      {
      TCHAR szModule[MAX_PATH] = _T("");
      DWORD section = 0, offset = 0;
      
      GetLogicalAddress(  (PVOID)sf.AddrPC.Offset,
      szModule, sizeof(szModule), section, offset );
      
      _tprintf( _T("%04X:%08X %s\r\n"),
      section, offset, szModule );
      }
    }
  
  }

//--------------------------------------------------

#endif

//============================================================================
// Helper function that writes to the report file, and allows the user to use 
// printf style formating                                                     
//============================================================================
int __cdecl DebugExceptionTrap :: _tprintf(const TCHAR * format, ...)
  {
  int retValue=0;
  if (m_hReportFile)
    {
    TCHAR szBuff[1024];
    DWORD cbWritten;
    va_list argptr;
    
    va_start( argptr, format );
    retValue = wvsprintf( szBuff, format, argptr );
    va_end( argptr );
    
    WriteFile( m_hReportFile, szBuff, retValue * sizeof(TCHAR), &cbWritten, 0 );
    }
  return retValue;
  }

//--------------------------------------------------

#if USE_IMAGE

//=========================================================================
// Load IMAGEHLP.DLL and get the address of functions in it that we'll use 
//=========================================================================
BOOL DebugExceptionTrap :: InitImagehlpFunctions( void )
  {
  HMODULE hModImagehlp = LoadLibrary( _T("IMAGEHLP.DLL") );
  if ( !hModImagehlp )
    return FALSE;
  
  _SymInitialize = (SYMINITIALIZEPROC)GetProcAddress( hModImagehlp,
  "SymInitialize" );
  if ( !_SymInitialize )
    return FALSE;
  
  _SymCleanup = (SYMCLEANUPPROC)GetProcAddress( hModImagehlp, "SymCleanup" );
  if ( !_SymCleanup )
    return FALSE;
  
  _StackWalk = (STACKWALKPROC)GetProcAddress( hModImagehlp, "StackWalk" );
  if ( !_StackWalk )
    return FALSE;
  
  _SymFunctionTableAccess = (SYMFUNCTIONTABLEACCESSPROC)
    GetProcAddress( hModImagehlp, "SymFunctionTableAccess" );
  
  if ( !_SymFunctionTableAccess )
    return FALSE;
  
  _SymGetModuleBase = (SYMGETMODULEBASEPROC)GetProcAddress( hModImagehlp,
  "SymGetModuleBase");
  if ( !_SymGetModuleBase )
    return FALSE;
  
  _SymGetSymFromAddr = (SYMGETSYMFROMADDRPROC)GetProcAddress( hModImagehlp,
  "SymGetSymFromAddr" );
  if ( !_SymGetSymFromAddr )
    return FALSE;
  
  if ( !_SymInitialize( GetCurrentProcess(), 0, TRUE ) )
    return FALSE;
  
  return TRUE;        
  }

//--------------------------------------------------

#endif
