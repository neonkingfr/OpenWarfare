//#include "wpch.hpp"
//#include <windows.h>

#include "debugTrap.hpp"
//#include "multisync.hpp"
#include "imexhnd.h"
#if 0
class DebugThreadWatch
  {
  // check if given thread is working
  mutable CriticalSection _lock;
  int _timeOut;
  int _enable; // <0 disabled, >=0 enabled
  
  HANDLE _mainThreadHandle;
  HANDLE _myThreadHandle;
  DWORD _myThreadId;
  Event _terminate;
  Event _keepAlive;
  
  bool _debuggerAttached;
  
  public:
    DebugThreadWatch();
    ~DebugThreadWatch();
    
    
    DWORD Execute();
    
    static DWORD WINAPI ExecuteHelper( void *watch )
      {
      return ((DebugThreadWatch*)watch)->Execute();
      }
    
    // external access functions
    void Terminate() 
      {_terminate.Set();}
    void KeepAlive() 
      {_keepAlive.Set();}
    void AliveEnable (int count)
      {
      ScopeLockSection lock(_lock);
      _enable+=count;
      }
    int GetAliveEnabled() const
      {
      ScopeLockSection lock(_lock);
      return _enable;
      }
    
    void SetAliveTimeout(int timeMs)
      {
      ScopeLockSection lock(_lock);
      _timeOut=timeMs;
      }
    
  };

//--------------------------------------------------

DebugThreadWatch::DebugThreadWatch()
  {
  ScopeLockSection lock(_lock);
  _debuggerAttached = false;
  _timeOut=-1;
  _enable=0;
  
  ::DuplicateHandle
    (
      ::GetCurrentProcess(),::GetCurrentThread(),
  ::GetCurrentProcess(),&_mainThreadHandle,
  0,false,DUPLICATE_SAME_ACCESS
    );
  
  DWORD threadId;
  _myThreadId;
  _myThreadHandle = ::CreateThread
    (
      NULL,64*1024,ExecuteHelper,this,CREATE_SUSPENDED,&threadId
        );
  
  if (_myThreadHandle)
    {
    ::ResumeThread(_myThreadHandle);
    }
  }

//--------------------------------------------------

DWORD DebugThreadWatch::Execute()
  {
  DWORD lastAlive=::GetTickCount();
  for(;;)
    {
    SignaledObject *wait[]=
      {&_terminate,&_keepAlive};
    int waitTime,waitTime0;
      {
      ScopeLockSection lock(_lock);
      waitTime0 = waitTime = _timeOut;
      if (waitTime<0 || waitTime>10000) waitTime = 10000;
      }
    DWORD start = ::GetTickCount();
    int which = SignaledObject::WaitForMultiple
      (
        wait,sizeof(wait)/sizeof(*wait),waitTime
          );
    if (which==0)
      {
      return 0; // _terminate event
      }
    else if (which==1)	 // alive event
      {
      lastAlive=::GetTickCount();
      DWORD delay = lastAlive-start;
      if (delay>waitTime-2000 || delay>2500)
        {
        LogF("No alive in %d of %d ms",delay,waitTime);
        LogF("  check another threads -- add ProgressRefresh()");
        }
      // no more event expected until told by NextAlive
      
      
      }
    else
      {
      if (waitTime0>=0 && waitTime0<10000 && _enable>=0)
        {
        RptF("No alive in %d",waitTime);
        CONTEXT context;
        context.ContextFlags=CONTEXT_FULL;
        ::SuspendThread(_mainThreadHandle);
        if (::GetThreadContext(_mainThreadHandle,&context))
          {
          GDebugExceptionTrap.ReportContext("FROZEN",&context);
          }
        if (::GetTickCount()-lastAlive>=9000)
          {
          // fatal situation - terminate
          extern void DDTerm();
          DDTerm();
          exit(1);
          /*NOT REACHED*/
          }
        ::ResumeThread(_mainThreadHandle);
        }
      }
    }
  return 0;
  }

//--------------------------------------------------

DebugThreadWatch::~DebugThreadWatch()
  {
  }

//--------------------------------------------------

#endif

Debugger::Debugger()
  {
  // dynamic function import
  _isDebugger=false;
  
  HMODULE kernel32 = LoadLibrary("kernel32.dll");
  if (kernel32)
    {
    typedef BOOL IsDebuggerPresentProc(VOID);
    IsDebuggerPresentProc *debPresent = (IsDebuggerPresentProc *)
      (
        GetProcAddress(kernel32,"IsDebuggerPresent")
          );
    if (debPresent)
      {
      _isDebugger = debPresent()!=FALSE;
      if (_isDebugger)
        {
        LogF("Starting debugger session");
        }
      }
    }
  #if _RELEASE
  if (!_isDebugger)
    {
    // start light debugger thread
    _watch = new DebugThreadWatch();
    }
  #endif
  }

//--------------------------------------------------

void Debugger::ForceLogging()
  {
  _isDebugger = false;
  }

//--------------------------------------------------

void Debugger::NextAliveExpected( int timeout )
  {
  /*
	GlobalShowMessage
	(
		timeout,
		"Next alive in %d ms, enabled %d",
		timeout, CheckingAlivePaused()
	);
	*/
  if (_watch)
    {
    _watch->SetAliveTimeout(timeout);
    _watch->KeepAlive();
    }
  }

//--------------------------------------------------

bool Debugger::CheckingAlivePaused()
  {
  if (_watch)
    {
    return _watch->GetAliveEnabled()>=0;
    }
  return false;
  }

//--------------------------------------------------

void Debugger::PauseCheckingAlive()
  { // decrement counter - cheking enabled
  if (_watch)
    {
    _watch->AliveEnable(-1);
    }
  }

//--------------------------------------------------

void Debugger::ResumeCheckingAlive()
  { // increment counter - cheking enabled
  if (_watch)
    {
    _watch->AliveEnable(+1);
    }
  I_AM_ALIVE();
  }

//--------------------------------------------------

void Debugger::ProcessAlive()
  {
  static int lastAlive=GetTickCount();
  
  int time = GetTickCount();
  int delay = time-lastAlive; 
  lastAlive = time;
  
  if (delay>1000)
    {
    __asm nop;
    }
  if (_watch) _watch->KeepAlive();
  }

//--------------------------------------------------

Debugger::~Debugger()
  {
  if (_watch) _watch->Terminate();
  
  if (_isDebugger)
    {
    LogF("Closing debugger session");
    }
  }

//--------------------------------------------------

// initialization
Debugger GDebugger;
