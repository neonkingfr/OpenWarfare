// Objektiv2.h : main header file for the OBJEKTIV2 application
//

#if !defined(AFX_OBJEKTIV2_H__6F94A434_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_OBJEKTIV2_H__6F94A434_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Config.h"
#include "NormalRecalc.h"
#include "SplashWin.h"
#include "MessageWnd.h"
#include "ROCheck.h"
/////////////////////////////////////////////////////////////////////////////
// CObjektiv2App:
// See Objektiv2.cpp for the implementation of this class
//

#define WM_PROGRESSCONTROL (WM_USER+100)
#define WM_APP1000 (WM_APP+1000)

class SccFunctions;

#include ".\plugingeneralappside.h"

#include "IPluginGeneral.h"

class CObjektiv2App : public CWinApp,public IFileDlgHistory
  {
  public:
    CSplashWin splash;
    void BarsOtherInfo(CFrameWnd *frame, bool save,...);
    int ClipFormat1; //old clip format
    int ClipFormat2; //Objektiv 2 clip format
    CObjektiv2App();
    
    CString startuptoload;

    SRef<SccFunctions> _scc;
    AutoArray<SRef<PluginGeneralAppSide> >_pluginList;
    WinROCheck rocheck;

    AutoArray<Ref<IPluginCommand> > _popupMenus;

    int idlecnt;
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CObjektiv2App)
  public:
    virtual BOOL InitInstance();
    virtual int ExitInstance();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    //}}AFX_VIRTUAL
    
    // Implementation
    
    //{{AFX_MSG(CObjektiv2App)
    afx_msg void OnAppAbout();
    // NOTE - the ClassWizard will add and remove member functions here.
    //    DO NOT EDIT what you see in these blocks of generated code !
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    protected:
    void SaveBarInfo(int id, CControlBar *bar);
    void LoadBarInfo(int id,CControlBar *bar, CFrameWnd *frame);
    void LoadAllPlugins();
  public:
    void InitScc(bool ask=false);
    bool SccOpenProject(bool ask);
    void *FindPluginService(ServiceName serviceName);
    template<class Functor>
    bool ForEachPlugin(const Functor f)
    {
      return _pluginList.ForEachF(f);
    }
    

    void PluginPopupHook(IPluginGeneralPluginSide::PopupMenuName name,HMENU hMenu, HWND hWnd);
    bool HookMenuCommand(int cmd);
    bool AddToPopupMenu(HMENU mnu, const char *menuName, IPluginCommand *command);
    static void ConsolePrintf(const char *format,...);
    static void ConsolePrintf(const char *text);
    static void ConsolePrintf(int ids,...);

    AutoArray<RString> _historyFolders;

    virtual void StoreHistoryFolder(const char *folderName);
    virtual const char *GetNextHistoryFolder(int index);
    virtual HBITMAP GetHistoryBitmap();
    virtual HBITMAP GetHistoryMenuBitmap();

    void CorrectApplicationMenuBasedOnConfigFile();
  };

class FunctorCloseAllPlugins
{
  mutable bool &canClose;
public:
  FunctorCloseAllPlugins(bool &canClose):canClose(canClose) {}
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    if (plugin->GetPlugin().BeforeClose()==false) {canClose=false;return true;}
    return false;
  }  
};


//--------------------------------------------------

extern CImageList shrilist;
extern CObjektiv2App theApp;
extern CConfig *config;

#define ILST_PROPERTY 0
#define ILST_SELECTION 1
#define ILST_LODDEFINITION 2
#define ILST_ANIMATION 3
#define ILST_ANIMATIONLOCK 4
#define ILST_WEIGHT 5
#define ILST_EMPTY 6
#define ILST_CHECK 7
#define ILST_FOLDER 8
#define ILST_NUMBER 9
#define ILST_TEXT 10
#define ILST_EXEFILES 11
#define ILST_COLOR 12
#define ILST_HIERARCHY 13
#define ILST_POSITION 14
#define ILST_CHECKOK 15
#define ILST_CHECKERROR 16
#define ILST_CHECKEXCLAMATION 17
#define ILST_MARKEDSELECTION 18
#define ILST_SSFILES 19
#define ILST_SCRIPT 20
#define ILST_SEPARATOR 21

#define PROGRESS_TIMEOUT -1234
#define ENABLE_STOP -10

#define MSG_BGRYIELD (WM_APP+7017)

void LoadPopupMenu(int IDD, CMenu& menu);

int FastCreateFace(ObjectData *odata, int cnt, ...);
void GetObjectCenter(bool selection, HVECTOR center, ObjectData *obj);
const char *AskForPath(HWND hWnd);
void CloseProgressWindow();
//LPHVECTOR GetFaceNormal(FaceT& fc, ObjectData *obj, bool normalize=true);
void GetMatrixFromFace(ObjectData &obj, FaceT &fc,HMATRIX mm);

Selection *CombineSelection(int seloperator, Selection *current, Selection *nwsel);
void CorrectFloatValues(CWnd *dlg, ...); //IDC LIST terminated by ZERO
const char *DeriveTexPath(const char *texpath, const char *texname);
const char *RemovePathPrefix(const char *texname);

#define G3D_DLLPATH "..\\g3d\\dlls\\"
//#define G3D_DLLPATH "dlls\\"

void OpenTGAFile(ostream& out, int xsize, int ysize, int bitedepth);
void CloseTGAFile(ostream& out);

void SendOrderMessage(HWND hWnd,LONG code1, LONG code2,bool extendonly);


const char *ConvertTexture(Pathname &filename);
const char *ShortPath(const char *pathname);
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OBJEKTIV2_H__6F94A434_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
