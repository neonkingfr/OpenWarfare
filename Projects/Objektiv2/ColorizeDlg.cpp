// ColorizeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "ColorizeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorizeDlg dialog

#define frand() ((float)rand()/(float)RAND_MAX)


CColorizeDlg::CColorizeDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CColorizeDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CColorizeDlg)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    ending=false;
    for (int i=1;i<COC_COLORCOUNT;i++)
      {
      HVECTOR v1;
      CopyVektor(v1,mxVector3(frand(),frand(),frand()+0.0001f));	
      NormalizeVector(v1);
      float rgb[3];
      memcpy(rgb,v1,sizeof(float)*3);
      int ir,ig,ib;
      ir=(int)(rgb[0]*255.0);
      ig=(int)(rgb[1]*255.0);
      ib=(int)(rgb[2]*255.0);
      pens[i].CreatePen(PS_SOLID,1,RGB(ir/2,ig/2,ib/2));
      fillcolor[i]=RGB((ir+255)>>1,(ig+255)>>1,(ib+255)>>1);
      drawcolor[i]=RGB(ir/2,ig/2,ib/2);
      }
    pens[0].CreatePen(PS_SOLID,1,RGB(0,0,0));
    fillcolor[0]=RGB(192,192,192);
    drawcolor[0]=0;
    
    }

//--------------------------------------------------

void CColorizeDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CColorizeDlg)
  // NOTE: the ClassWizard will add DDX and DDV calls here
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CColorizeDlg, CDialog)
  //{{AFX_MSG_MAP(CColorizeDlg)
  ON_WM_DRAWITEM()
    ON_WM_ACTIVATE()
      ON_COMMAND_RANGE(IDC_COLOR1,IDC_COLOR16,OnColorClick)
        //}}AFX_MSG_MAP
        END_MESSAGE_MAP()
          
          /////////////////////////////////////////////////////////////////////////////
          // CColorizeDlg message handlers
          
          void CColorizeDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
            { 
            if (nIDCtl>=IDC_COLOR1 && nIDCtl<=IDC_COLOR16)
              {
              CDC dc;dc.Attach(lpDrawItemStruct->hDC);
              CRect rc(&(lpDrawItemStruct->rcItem));
              dc.FillSolidRect(&rc,drawcolor[nIDCtl-IDC_COLOR1]);
              if (!(lpDrawItemStruct->itemState & 1))
                dc.Draw3dRect(&rc,GetSysColor(COLOR_3DHILIGHT),GetSysColor(COLOR_3DSHADOW));
              dc.Detach();
              }
            else
              CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
            }

//--------------------------------------------------

BOOL CColorizeDlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  GetOwner()->EnableWindow(TRUE);	
  CPoint ps(GetMessagePos()); 
  SetWindowPos(NULL,ps.x,ps.y,0,0,SWP_NOZORDER|SWP_NOSIZE);
  ending=false;
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CColorizeDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
  {
  if (nState==WA_INACTIVE && coldlg.m_hWnd==NULL && !ending) 
    EndDialog(-1);
  CDialog::OnActivate(nState, pWndOther, bMinimized);
  }

//--------------------------------------------------

void CColorizeDlg::OnColorClick(UINT cmd)
  {
  cmd-=IDC_COLOR1;
  if (GetKeyState(VK_CONTROL) & 0x80)
    {	
    coldlg.m_cc.rgbResult=drawcolor[cmd];
    if (coldlg.DoModal()==IDOK)
      {
      drawcolor[cmd]=coldlg.m_cc.rgbResult;
      fillcolor[cmd]=(((coldlg.m_cc.rgbResult & 0xFEFEFE)+0xFEFEFE)>>1);
      pens[cmd].DeleteObject();
      pens[cmd].CreatePen(PS_SOLID,1,drawcolor[cmd]);
      ending=true;
      EndDialog(cmd);
      }
    }
  else
    {
    ending=true;
    EndDialog(cmd);
    }
  
  }

//--------------------------------------------------

void CColorizeDlg::ColorizeSelection(ObjectData *obj, int idx)
  {
  int cnt=obj->NFaces();
  for (int i=0;i<cnt;i++) if (obj->FaceSelected(i))
    {
    FaceT fc(obj,i);
    fc.SetFlags(fc.GetFlags()& ~FACE_COLORIZE_MASK);
    fc.SetFlags(fc.GetFlags()| ((idx*FACE_COLORIZE_STEP)&FACE_COLORIZE_MASK));
    }
  }

//--------------------------------------------------

CPen * CColorizeDlg::GetColorizePen(FaceT &fc)
  {
  int idx=(fc.GetFlags() & FACE_COLORIZE_MASK)>>FACE_COLORIZE_SHIFT;
  return pens+idx;
  }

//--------------------------------------------------

COLORREF CColorizeDlg::GetColorizeFillColor(FaceT &fc)
  {
  int idx=(fc.GetFlags() & FACE_COLORIZE_MASK)>>FACE_COLORIZE_SHIFT;
  return fillcolor[idx];
  }

  COLORREF CColorizeDlg::GetColorizeLineColor(FaceT &fc)
  {
    int idx=(fc.GetFlags() & FACE_COLORIZE_MASK)>>FACE_COLORIZE_SHIFT;
    return drawcolor[idx];
  }
//--------------------------------------------------

CPen CColorizeDlg::weightPens[COC_COLORCOUNT];
bool CColorizeDlg::inited=false;


CPen * CColorizeDlg::WeightColor(float weight)
  {
  if (!inited)
    {
    for (int i=0;i<COC_WEIGHTCOUNT;i++)
      {
      CPen& pen=weightPens[i];
      int ref=(i*256)/COC_WEIGHTCOUNT;
      pen.CreatePen(PS_SOLID,1,RGB(ref,0,255-ref));
      }
    inited=true;
    }
  int ref=(int)(weight*(COC_WEIGHTCOUNT-1)+0.5);
  if (ref>=COC_WEIGHTCOUNT) ref=COC_WEIGHTCOUNT;
  return weightPens+ref;
  }

//--------------------------------------------------

