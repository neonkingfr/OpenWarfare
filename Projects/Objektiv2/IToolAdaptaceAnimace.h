// IToolAdaptaceAnimace.h: interface for the CIToolAdaptaceAnimace class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ITOOLADAPTACEANIMACE_H__64069161_FD58_4933_8FB6_D6DD86EC8B65__INCLUDED_)
#define AFX_ITOOLADAPTACEANIMACE_H__64069161_FD58_4933_8FB6_D6DD86EC8B65__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CIToolAdaptaceAnimace : public CICommand  
  {
  struct istruct
    {
    int idxsel; //index selekce
    Vector3P odchylka;
    Vector3P center;
    };
  istruct workdata[MAX_NAMED_SEL];
  float reflod;
  
  public:
    CIToolAdaptaceAnimace(float lod);
    virtual ~CIToolAdaptaceAnimace();
    virtual int Execute(LODObject *lod);
    
  };

//--------------------------------------------------

#endif // !defined(AFX_ITOOLADAPTACEANIMACE_H__64069161_FD58_4933_8FB6_D6DD86EC8B65__INCLUDED_)
