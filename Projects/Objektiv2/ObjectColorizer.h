// ObjectColorizer.h: interface for the CObjectColorizer class.
//
//////////////////////////////////////////////////////////////////////

// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE
// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE
// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE
// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE// OBSOLETE

#if !defined(AFX_OBJECTCOLORIZER_H__F6602AB3_C8F2_4F7E_968B_B31C8FE5339C__INCLUDED_)
#define AFX_OBJECTCOLORIZER_H__F6602AB3_C8F2_4F7E_968B_B31C8FE5339C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define COC_COLORCOUNT 16
#define COC_WEIGHTCOUNT COC_COLORCOUNT

class CObjectColorizer  
  {
  COLORREF col[COC_COLORCOUNT];
  CPen pens[COC_COLORCOUNT];
  static CPen weightPens[COC_WEIGHTCOUNT];
  static bool inited;
  char *facecols;
  int faces;	
  public:
    bool Validate(ObjectData *obj);
    void Uncolorize();
    static CPen * WeightColor(float weight);
    bool ColorizeOneObject(ObjectData *obj, Selection *sel, int index);
    void ColorizeObject(ObjectData *obj);
    COLORREF GetObjectRGB(int facenum);
    CPen & GetObjectColor(int facenum);
    CObjectColorizer();
    virtual ~CObjectColorizer();
    
  };

//--------------------------------------------------

extern CObjectColorizer Colorizer;

#endif // !defined(AFX_OBJECTCOLORIZER_H__F6602AB3_C8F2_4F7E_968B_B31C8FE5339C__INCLUDED_)
