#include "StdAfx.h"
#include <projects/ObjektivLib/ObjToolTopology.h>
#include ".\mateditorplugin.h"
#include "MainFrm.h"
#include "CreatePrimDlg.h"


CMATEditorPlugin::CMATEditorPlugin(void *matService)
{  
  _plugin=reinterpret_cast<IMatPluginSide *>(matService);
  if (_plugin) _plugin->Init(this);
}

CMATEditorPlugin::~CMATEditorPlugin(void)
{
  _plugin=NULL;
}

typedef IMatGeneralPluginSide *(*InitProcType)();


void CMATEditorPlugin::UseMaterial(const char *filename)
{
}
bool CMATEditorPlugin::CanUseMaterial()
{
  return false;
}


ObjectData *CMATEditorPlugin::CreatePrimitive(const char *filename, const char *texture, const char *material)
{
  LODObject lod;
  if (lod.Load(filename,NULL,NULL)!=0) return NULL;
  ObjectData *level=lod.Active();
  ObjectData *newobj=new ObjectData(*level);
  if (texture!=NULL || material!=NULL)
    for (int i=0;i<newobj->NFaces();i++)
    {
      FaceT fc(newobj,i);
      if (texture) fc.SetTexture(texture);
      if (material) fc.SetMaterial(material);
    }
    newobj->RecalcNormals();
    newobj->ClearSelection();
    return newobj;
}

ObjectData *CMATEditorPlugin::CreatePrimitive(ObjPrimitiveType type, int segments, float scale, const char *texture, const char *material)
{
  CCreatePrimDlg dlg;
  HMATRIX mm;
  JednotkovaMatice(mm);
  Zoom(mm,scale,scale,scale);
  dlg.numX=dlg.numY=dlg.numZ=segments;
  switch (type)
  {
  case IMatAppSide::PrimBox: dlg.create_type=IDD_CREATE_PRIM_BOX;break;
  case IMatAppSide::PrimSpehere: dlg.create_type=IDD_CREATE_PRIM_SPEHERE;
    dlg.numX=dlg.numY=dlg.numX=segments*10;break;
  case IMatAppSide::PrimPlane: dlg.create_type=IDD_CREATE_PRIM_PLANE;break;
  case IMatAppSide::PrimCylinder: dlg.create_type=IDD_CREATE_PRIM_CYLINDER;dlg.numX*=10;mm[0][0]=mm[1][1]=scale*0.2f;break;
  default: 
    return NULL;
  }
  ObjectData *newobj=new ObjectData;  
  dlg.CreatePrimitive(newobj,mm);
  if (texture!=NULL || material!=NULL)
    for (int i=0;i<newobj->NFaces();i++)
    {
      FaceT fc(newobj,i);
      if (texture) fc.SetTexture(texture);
      if (material) fc.SetMaterial(material);
    }
    newobj->GetTool<ObjToolTopology>().AutoSharpEdges();
    newobj->RecalcNormals();
    return newobj;
}

void CMATEditorPlugin::DestroyPrimitive(ObjectData *obj)
{
  delete obj;
}

