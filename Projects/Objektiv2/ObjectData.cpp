// ObjectData.cpp: implementation of the CObjectData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "ObjectData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


bool CSelection::SelectVertex(int index)
  {
  if (lastquery>=index) return false;
  points.Insert(index);
  lastquery=index;
  return true;
  }

//--------------------------------------------------

bool CSelection::IsSelected(int index)
  {  
  int p;
  if (lastquery>=points.GetCount()) Reset();
  if (points[lastquery]>index) Reset();
  while ((p=points[lastquery])<index) lastquery++;
  return p==index;
  }

//--------------------------------------------------

