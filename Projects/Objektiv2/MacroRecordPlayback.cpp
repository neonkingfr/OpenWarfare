// MacroRecordPlayback.cpp: implementation of the CMacroRecordPlayback class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "objektiv2.h"
#undef RStringB
#include "MacroRecordPlayback.h"
#include <el/ParamFile/paramFile.hpp>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define SHOWDEBUGWINDOW 0

static CMacroRecordPlayback *curRecorder=NULL;

CMacroRecordPlayback::CMacroRecordPlayback()
{
_curHook=NULL;
_idleHook=NULL;
_playback=false;
}

CMacroRecordPlayback::~CMacroRecordPlayback()
{
UnhookWindowsHookEx(_curHook);
UnhookWindowsHookEx(_idleHook);
curRecorder=NULL;
}

static LRESULT CALLBACK GetMsgProc(int nCode, WPARAM wParam, LPARAM lParam)
  {
  return curRecorder->OnGetMsgEvent(nCode,wParam,(MSG *)lParam);
  }
 


void CMacroRecordPlayback::RecordMacro()
  {
  if (_playback) return;
  ASSERT(curRecorder==NULL);
  record.Clear();
  _curHook=SetWindowsHookEx(WH_GETMESSAGE, GetMsgProc, NULL,GetCurrentThreadId());
  int err=GetLastError();

  curRecorder=this;
  _lastHwnd=0;
  }

static int GetWindowPath(HWND hWnd, char *buff,int len)
  {
  HWND parent=::GetParent(hWnd);
  if (GetWindow(hWnd,GW_OWNER)==parent) 
    {
    strcpy(buff,"T");
    return 1;
    }
  int pos=GetWindowPath(parent,buff,len);   
  if (pos+10>len) return len;
  if (pos) buff[pos++]=',';
  HWND p=GetWindow(parent,GW_CHILD);  
  int i;
  for (i=0;p && p!=hWnd;i++) 
    p=GetNextWindow(p,GW_HWNDNEXT);
  if (p==hWnd) _itoa(i,buff+pos,10);
  else _itoa(-1,buff+pos,10);
  pos+=strlen(buff+pos);
  return pos;
  }


LRESULT CMacroRecordPlayback::OnGetMsgEvent(int nCode, WPARAM wParam, const MSG *msg)
  {
  if (_playback) return CallNextHookEx(_curHook,nCode,wParam,(LPARAM)msg);
  if (nCode<0 || wParam==PM_NOREMOVE) return CallNextHookEx(_curHook,nCode,wParam,(LPARAM)msg);
  if (msg->message==WM_KEYDOWN || msg->message==WM_KEYUP || msg->message==WM_SYSKEYDOWN || msg->message==WM_SYSKEYUP)
    {
#if SHOWDEBUGWINDOW
      int index=record.Size();
#endif
      AddKeyboardRecord(*msg);
#if SHOWDEBUGWINDOW
      DebugRecord(record[index],index);
//      _lastKeyFlags=msg->lParam;
      _lastVKey=msg->wParam;
//      _lasttime=msg->time;
#endif
    }
  else if (msg->message>=WM_MOUSEFIRST && msg->message<=WM_MOUSELAST)   
    {
#if SHOWDEBUGWINDOW
    int index=record.Size();
#endif
    AddMouseRecord(*msg);
#if SHOWDEBUGWINDOW
    DebugRecord(record[index],index);
#endif
    }
  else if (msg->message>=WM_NCMOUSEMOVE && msg->message<=WM_NCMBUTTONDBLCLK)
	{
	MSG fkmsg=*msg;
	fkmsg.wParam=0;
	if (GetKeyState(VK_LBUTTON) & 0x80) fkmsg.wParam|=MK_LBUTTON;
	if (GetKeyState(VK_RBUTTON) & 0x80) fkmsg.wParam|=MK_RBUTTON;
	if (GetKeyState(VK_MBUTTON) & 0x80) fkmsg.wParam|=MK_MBUTTON;
#if SHOWDEBUGWINDOW
    int index=record.Size();
#endif
    AddMouseRecord(fkmsg);
#if SHOWDEBUGWINDOW
    DebugRecord(record[index],index);
#endif
	}
  return CallNextHookEx(_curHook,nCode,wParam,(LPARAM)msg);
  }

void CMacroRecordPlayback::StopRecord()
  {
  if (_playback) return;
  UnhookWindowsHookEx(_curHook);
#if SHOWDEBUGWINDOW
  _debugWnd.DestroyWindow();
#endif
  curRecorder=NULL;
  _curHook=NULL;
  }

void CMacroRecordPlayback::CreateWindowIdentification(HWND hWnd, SMacroRecord& rcrd)
  {
  char temp[256];
  GetWindowPath(hWnd,temp,lenof(temp));
  rcrd.path=temp;
  HWND p,parent=hWnd;
  while ((p=GetParent(parent))!=GetWindow(parent,GW_OWNER)) parent=p;
  GetWindowText(parent,temp,lenof(temp));
  rcrd.topname=temp;
  GetClassName(parent,temp,lenof(temp));
  rcrd.topclass=temp;
  rcrd.ismainfrm=theApp.m_pMainWnd->GetSafeHwnd()==parent;
  rcrd.hasfocus=GetFocus()==hWnd;
//  rcrd.isactive=GetActiveWindow()==parent;
  rcrd.capture=GetCapture()==hWnd;
  rcrd.controlID=::GetDlgCtrlID(hWnd);
  CRect rc;
  GetClientRect(hWnd,&rc);
  rcrd.size.cx=rc.right;
  rcrd.size.cy=rc.bottom;
  ClientToScreen(hWnd,(POINT *)&rc);
  ClientToScreen(hWnd,(POINT *)&rc+1);
  rcrd.scrpt=rc.CenterPoint();
  }

static HWND FindWindowByPath(HWND hWnd,const char *path)
  {
  char *c=const_cast<char *>(path);
  while ((c=strchr(c,','))!=NULL && hWnd)
    {
    int order;
    c++;
    sscanf(c,"%d",&order);
    hWnd=GetWindow(hWnd,GW_CHILD);
    for (int i=0;i<order && hWnd;i++) hWnd=GetWindow(hWnd,GW_HWNDNEXT);    
    }
  return hWnd;
  }

static HWND FindWindowByIDInLevelRec(HWND hWnd, int id, int level, const CSize *sz=NULL)
  {
  if (level>0)
    {
    HWND ch=GetWindow(hWnd,GW_CHILD);
    while (ch) 
      {
      HWND res=FindWindowByIDInLevelRec(ch,id,level-1);
      if (res!=NULL) return res;
      ch=GetWindow(ch,GW_HWNDNEXT);
      }    
    return NULL;
    }
  else
    {
    if (::GetDlgCtrlID(hWnd)==id)
      {
      if (sz==NULL) return hWnd;
      CRect rc;
      GetClientRect(hWnd,&rc);
      if (rc.right==sz->cx && rc.bottom==sz->cy) return hWnd;
      }
    return NULL;
    }
  }

static HWND FindWindowByIDInLevel(HWND hWnd, int id, const char *path, const CSize *sz=NULL)
  {
  int level=0;
  while (*path) {if (*path==',') ++level;++path;}
  if (level==0) return hWnd;  
  return FindWindowByIDInLevelRec(hWnd,id,level,sz);
  }

HWND CMacroRecordPlayback::FindWindowByIdentification(const SMacroRecord& rcrd)
  {
  if (rcrd.capture && GetCapture()!=NULL) return GetCapture();
//  if (rcrd.hasfocus) return GetFocus();
  HWND parent=NULL;  
  if (rcrd.ismainfrm) parent=theApp.m_pMainWnd->GetSafeHwnd();
//  else if (rcrd.isactive) parent=GetActiveWindow();
  else 
    {
    parent=FindWindow(rcrd.topclass,rcrd.topname);
    if (parent==NULL)
      parent=FindWindow(rcrd.topclass,NULL);
    if (parent==NULL)
      parent=WindowFromPoint(rcrd.scrpt);
    }
  HWND fnd=FindWindowByPath(parent,rcrd.path);
  if (fnd==NULL || GetDlgCtrlID(fnd)!=rcrd.controlID)
    {
    const char *path=rcrd.path;
    fnd=FindWindowByIDInLevel(parent,rcrd.controlID,path,&rcrd.size);
    if (fnd==NULL)
      fnd=FindWindowByIDInLevel(parent,rcrd.controlID,path,NULL);
    if (parent==NULL)
      parent=WindowFromPoint(rcrd.scrpt);
    return fnd;
    }  
  return fnd;
  }


void CMacroRecordPlayback::AddMouseRecord(const MSG &msg)
  {
  HWND validWnd=NULL;
  CPoint pt;  
  HWND capture=GetCapture();
  if (capture)
    {
    char buff[50];
    GetClassName(WindowFromPoint(msg.pt),buff,lenof(buff));
    TRACE1("%s\n",buff);
    if (strcmp(buff,"#32768")) validWnd=capture;
    }
  if (validWnd==NULL)
    {
   HWND top=WindowFromPoint(msg.pt);  
    if (top)
      {
      pt=msg.pt;
      ScreenToClient(top,&pt);
      validWnd=ChildWindowFromPoint(top,pt);
      }
    if (validWnd==NULL) validWnd=top;
    }
  pt=msg.pt;
  SMacroRecord &nwrc=record.Append();
  CreateWindowIdentification(validWnd,nwrc);  
  ScreenToClient(validWnd,&pt);  
  if ((msg.message==WM_LBUTTONDOWN || msg.message==WM_MBUTTONDOWN || 
    msg.message==WM_RBUTTONDOWN || msg.message==WM_NCLBUTTONDOWN || msg.message==WM_NCMBUTTONDOWN || 
    msg.message==WM_NCRBUTTONDOWN) 
    && record.Size()>0 && record[record.Size()-1].capture==false  ) 
    nwrc.capture=false;
  nwrc.type=mrMouse;
  nwrc.msinfo.pt=pt;
  nwrc.scrpt=msg.pt;
  nwrc.msinfo.buttons=msg.wParam;
  nwrc.msinfo.message=msg.message;
  nwrc.hwndchange=_lastHwnd!=validWnd;
  _lastHwnd=validWnd ;
  }

void CMacroRecordPlayback::AddKeyboardRecord(const MSG &msg)
  {
  if (_lastVKey==msg.wParam && _lastMsgCode==msg.message && _lastPressTime==msg.time) return;
  _lastVKey=msg.wParam;
  _lastMsgCode=msg.message;
  _lastPressTime=msg.time;
  HWND validWnd=msg.hwnd;
  SMacroRecord &nwrc=record.Append();
  CreateWindowIdentification(validWnd,nwrc);
  nwrc.type=mrKeyboard;    
  nwrc.kbinfo.vKey=msg.wParam;
  nwrc.kbinfo.flags=msg.lParam;
  }


void CMacroRecordPlayback::DebugRecord(const SMacroRecord &rcrd, int index)
{
  if (_debugWnd.GetSafeHwnd()==NULL)
    {
    _debugWnd.CreateEx(WS_EX_TOPMOST,"EDIT","",ES_MULTILINE|WS_OVERLAPPEDWINDOW|WS_VISIBLE,CRect(10,10,300,300),NULL,0,NULL);
    theApp.m_pMainWnd->BringWindowToTop();
    }
CString s;
s.Format(
    "index %10d\r\n"
    "type %10d\r\n"
    "path %10s\r\n"
    "topname %10s\r\n"
    "topclass %10s\r\n"
    "ismainfrm %10d\r\n"
    "hasfocus %10d\r\n"
  //  "isactive %10d\r\n"
    "capture %10d\r\n"
    "controlID %10d\r\n"
    "size %10d%10d \r\n"
    "scrpt %10d%10d\r\n"
    "vKey %10d\r\n"
    "flags %10X\r\n"
    "pt %10d%10d\r\n"
    "buttons %10x\r\n",
    index,
    rcrd.type,
    (const char *)rcrd.path,
    (const char *)rcrd.topname,
    (const char *)rcrd.topclass,
    rcrd.ismainfrm,
    rcrd.hasfocus,
//    rcrd.isactive,
    rcrd.capture,
    rcrd.controlID,
    rcrd.size.cx,rcrd.size.cy,
    rcrd.scrpt.x,rcrd.scrpt.y,
    rcrd.kbinfo.vKey,
    rcrd.kbinfo.flags,
    rcrd.msinfo.pt.x,rcrd.msinfo.pt.y,
    rcrd.msinfo.buttons);
_debugWnd.SetWindowText(s);
_debugWnd.UpdateWindow();
}
static bool _reenterguard=false;

static LRESULT CALLBACK ForegroundIdleProc(  int code, WPARAM wParam, LPARAM lParam)
  {  
  if (_reenterguard) return 0;
  _reenterguard=true;
  LRESULT res=curRecorder->PlayOnRecordStep(code);
  _reenterguard=false;
  return res;
  }

static VOID CALLBACK TimerProc(
  HWND hwnd,     // handle of window for timer messages
  UINT uMsg,     // WM_TIMER message
  UINT idEvent,  // timer identifier
  DWORD dwTime   // current system time
)
  {  
  if (_reenterguard) return;
  _reenterguard=true;
  LRESULT res=curRecorder->PlayOnRecordStep(0);
  _reenterguard=false;
  return;
  }


void CMacroRecordPlayback::PlaybackRecord()
  {
  GetCursorPos(&_mouseSave);
  WPARAM previousFlags=0;
  _playback=true;
  _curindex=0;
  curRecorder=this;
  _previousFlags=0;
  _idleHook=SetWindowsHookEx(WH_FOREGROUNDIDLE,ForegroundIdleProc,NULL,GetCurrentThreadId());
  theApp.m_pMainWnd->SetTimer(9894,1,TimerProc);
  _waitTime=0;
  _waitRelease=false;
  _lastMsClick=CPoint(0,0);
  _lastHwnd=0;

  }

void CMacroRecordPlayback::PlaybackKeyboard(const SMacroRecord &rcrd)
  {
  INPUT input;
  BOOL isextended=(rcrd.kbinfo.flags & 0x1000000)!=0;
  BOOL iskeyup=(rcrd.kbinfo.flags & 0x80000000)!=0;
  input.type=INPUT_KEYBOARD;
  input.ki.dwFlags=(isextended?KEYEVENTF_EXTENDEDKEY:0) | (iskeyup?KEYEVENTF_KEYUP:0);
  input.ki.time=GetTickCount();
  input.ki.wScan=(rcrd.kbinfo.flags>>16)& 0xFF;  
  input.ki.wVk=rcrd.kbinfo.vKey;
  int repeat=(rcrd.kbinfo.flags & 0xFFFF);
  for (int i=0;i<repeat;i++)    
    SendInput(1,&input,sizeof(INPUT));
//if (!iskeyup) Sleep(250);
  }


bool CMacroRecordPlayback::PlaybackMouse(const SMacroRecord &rcrd)
  {      
  HWND hWnd;
  if (!rcrd.hwndchange && IsWindow(_lastHwnd)) 
    hWnd=_lastHwnd;
  else hWnd=FindWindowByIdentification(rcrd);
  if (hWnd==NULL) //nenasel jsem okno
    {
    if ((rcrd.msinfo.buttons & (MK_LBUTTON | MK_RBUTTON | MK_MBUTTON))==0)  //pouze se hybe mysi...
      return true;  //ignoruj tento zaznam
	ResetInputDevices(_curindex);
    AfxMessageBox(IDS_MACROPLAYBACKINTEGRITYERROR,MB_OK|MB_ICONSTOP); //ohlas chybu;
    return false;    
    }
  CPoint pt=rcrd.msinfo.pt;
  ClientToScreen(hWnd,&pt);
  INPUT input;
  input.type=INPUT_MOUSE;
  

  _lastHwnd=hWnd;

  input.mi.dx=65535*pt.x/GetSystemMetrics(SM_CXSCREEN);
  input.mi.dy=65535*pt.y/GetSystemMetrics(SM_CYSCREEN);
  input.mi.dwExtraInfo=0;
  input.mi.mouseData=0;
  DWORD &flags=input.mi.dwFlags;
  flags=MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_MOVE;
  WPARAM mixbutt=_previousFlags ^ rcrd.msinfo.buttons;
  if (mixbutt & MK_LBUTTON)
    {
    if (rcrd.msinfo.buttons & MK_LBUTTON) {flags|=MOUSEEVENTF_LEFTDOWN;}
    else {flags|=MOUSEEVENTF_LEFTUP;}
    }
  if (mixbutt & MK_RBUTTON)
    {
    if (rcrd.msinfo.buttons & MK_RBUTTON) {flags|=MOUSEEVENTF_RIGHTDOWN;}
    else {flags|=MOUSEEVENTF_RIGHTUP;}
    }
  if (mixbutt & MK_MBUTTON)
    {
    if (rcrd.msinfo.buttons & MK_RBUTTON) {flags|=MOUSEEVENTF_MIDDLEDOWN;}
    else {flags|=MOUSEEVENTF_MIDDLEUP;}
    }
  if (flags & (MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_MIDDLEDOWN))
	{
	CPoint diff=_lastMsClick-pt;
	if (abs(diff.x)<5 && abs(diff.y)<5) 	  
	  {
	  Sleep(500);
  	  _waitRelease=true;
	  }
	_lastMsClick=pt;
	}
  input.mi.time=GetTickCount();
  SendInput(1,&input,sizeof(input));
  if ((flags & (MOUSEEVENTF_LEFTUP | MOUSEEVENTF_RIGHTUP | MOUSEEVENTF_MIDDLEUP))!=0 && _waitRelease==true)
	{
	_waitTime=GetTickCount()+600;
	_waitRelease=false;
	_lastMsClick=CPoint(0,0);
	}
  _previousFlags=rcrd.msinfo.buttons;
  return true;
  }

DWORD CMacroRecordPlayback::PlayOnRecordStep(int nCode)
  {
  if (_waitTime>GetTickCount()) return 0;
  PostMessage(theApp.m_pMainWnd->GetSafeHwnd(),WM_APP+9698,0,0);
 // if (nCode<0) return CallNextHookEx(_idleHook,nCode,0,0);
  int sz=record.Size();
  if (_curindex<sz && !((GetKeyState(VK_ESCAPE) & 0x80) && (GetKeyState(VK_SHIFT) & 0x80)))
    {
#if SHOWDEBUGWINDOW
    DebugRecord(record[_curindex],_curindex);
#endif

  if (!CheckActiveWindow())
	{
	ResetInputDevices(_curindex);
	if (AfxMessageBox(IDS_MACRORECORDLOSTFOCUS,MB_YESNO|MB_ICONEXCLAMATION)==IDYES) return 0;
	}
   else if (record[_curindex].type==mrKeyboard) {PlaybackKeyboard(record[_curindex++]);return 0;}
   else if (record[_curindex].type==mrMouse) 
      if (PlaybackMouse(record[_curindex++])) return 0;
    }
  _playback=false;
  curRecorder=NULL;
  UnhookWindowsHookEx(_idleHook);
  theApp.m_pMainWnd->KillTimer(9894);
  _idleHook=0;
  SetCursorPos(_mouseSave.x,_mouseSave.y);  
  ResetInputDevices();
  return 0;
  }
 

void CMacroRecordPlayback::ResetInputDevices(int macropos)
  {
  if (macropos<-1 || macropos>=record.Size()) macropos=record.Size();    
  int keypressed[256];
  int i;
  memset(keypressed,0xFF,sizeof(keypressed));
  for (i=0;i<macropos;++i)
	{
	if (record[i].type==mrKeyboard)
	  {
	  if (record[i].kbinfo.flags & 0x80000000) 
		keypressed[record[i].kbinfo.vKey]=-1;
	  else 
		keypressed[record[i].kbinfo.vKey]=i;
	  }
	}
  for (i=0;i<256;i++) 
	if (keypressed[i]!=-1)
	  {
	  SMacroRecord rc;
	  rc=record[keypressed[i]];
	  rc.kbinfo.flags |=0x80000000;
	  PlaybackKeyboard(rc);
	  }
    
  }

bool CMacroRecordPlayback::CheckActiveWindow()
  {
  HWND hWnd=GetForegroundWindow();
  DWORD thrd=GetWindowThreadProcessId(hWnd,0);
  return thrd==GetCurrentThreadId();
  }

void SMacroRecord::SaveRecord(ParamClass *pclass)
{
  char buff[256];
  if (type==mrKeyboard)
  {
    pclass->Add("type","Kbd");
    pclass->Add("vKey",(int)kbinfo.vKey);
    pclass->Add("keyflags",_itoa(kbinfo.flags,buff,16));
  }
  else
  {
    pclass->Add("type","Ms");
    ParamEntryPtr arr=pclass->AddArray("point");
    arr->AddValue(msinfo.pt.x);
    arr->AddValue(msinfo.pt.y);
    pclass->Add("buttons",(int)msinfo.buttons);
    pclass->Add("message",(int)msinfo.message);
  }

  pclass->Add("hwndchange",hwndchange?1:0); 
  if (hwndchange)
  {
    pclass->Add("path",path);
    pclass->Add("topname",topname);
    pclass->Add("topclass",topclass);
    ParamEntryPtr flgs=pclass->AddArray("flags");
    if (ismainfrm) flgs->AddValue("ismainfrm");
    if (hasfocus) flgs->AddValue("hasfocus");
    if (capture) flgs->AddValue("capture");
    pclass->Add("control",controlID);
    ParamEntryPtr sz=pclass->AddArray("winsize");
    sz->AddValue(size.cx);
    sz->AddValue(size.cy);
    ParamEntryPtr escrpt=pclass->AddArray("scrpt");
    escrpt->AddValue(scrpt.x); 
    escrpt->AddValue(scrpt.y);
  }
}
bool SMacroRecord::LoadRecord(ParamClass *pclass)
{
  ParamEntryPtr entry;
  entry=pclass->FindEntry("type"); if (entry.IsNull()) return false;
  RStringI ttype=entry->GetValue();
  if (ttype==RStringI("Kbd"))
  {
    type=mrKeyboard;
    entry=pclass->FindEntry("vKey"); if (entry.IsNull()) return false;
    kbinfo.vKey=entry->GetInt();
    entry=pclass->FindEntry("keyflags"); if (entry.IsNull()) return false;
    if (sscanf(entry->GetValue(),"%X",&kbinfo.flags)!=1) return false;
  }
  else if (ttype==RStringI("Ms"))
  {
    type=mrMouse;
    entry=pclass->FindEntry("point"); if (entry.IsNull() || !entry->IsArray() || entry->GetSize()!=2) return false;
    msinfo.pt.x=entry->operator [](0).GetInt();
    msinfo.pt.y=entry->operator [](1).GetInt();
    entry=pclass->FindEntry("buttons"); if (entry.IsNull() || !entry->IsIntValue()) return false;
    msinfo.buttons=entry->GetInt();
    entry=pclass->FindEntry("message"); if (entry.IsNull() || !entry->IsIntValue()) return false;
    msinfo.message=entry->GetInt();
  }
  else
    return false;

  entry=pclass->FindEntry("hwndchange"); if (entry.IsNull() || !entry->IsIntValue()) return false;
  if (entry->GetInt())
  {
    hwndchange=true;      
    entry=pclass->FindEntry("path"); if (entry.IsNull()) return false;
    path=entry->GetValue();
    entry=pclass->FindEntry("topname"); if (entry.IsNull()) return false;
    topname=entry->GetValue();
    entry=pclass->FindEntry("topclass"); if (entry.IsNull()) return false;
    topclass=entry->GetValue();
    entry=pclass->FindEntry("flags"); if (entry.IsNull()) return false;
    ismainfrm=hasfocus=capture=false;
    for (int i=0;i<entry->GetSize();i++)
    {
      RStringI val=entry->operator [](i).GetValue();
      if (val==RStringI("ismainfrm")) ismainfrm=true;
      else if (val==RStringI("hasfocus")) hasfocus=true;
      else if (val==RStringI("capture")) capture=true;
    }
    entry=pclass->FindEntry("control"); if (entry.IsNull()) return false;
    controlID=entry->GetInt();
    entry=pclass->FindEntry("winsize"); if (entry.IsNull() || !entry->IsArray() || entry->GetSize()!=2) return false;
    size.cx=entry->operator[](0).GetInt();
    size.cy=entry->operator[](1).GetInt();
    entry=pclass->FindEntry("scrpt"); if (entry.IsNull() || !entry->IsArray() || entry->GetSize()!=2) return false;
    scrpt.x=entry->operator[](0).GetInt();
    scrpt.y=entry->operator[](1).GetInt();
  }
  else
    hwndchange=false;
  return true;
}

bool CMacroRecordPlayback::SaveMacro(const char *filename)
{
  ParamFile pfile;
  ParamClassPtr pclass=pfile.AddClass("Objektiv2Macro",true);
  for (int i=0;i<record.Size();i++)
  {
    char name[256];
    sprintf(name,"_%06d",i);
    ParamClassPtr icls=pclass->AddClass(name,true);
    record[i].SaveRecord(icls.GetPointer());
  }
  return pfile.Save(filename)==LSOK;
}

bool CMacroRecordPlayback::LoadMacro(const char *filename)
{
  ParamFile pfile;
  if (pfile.Parse(filename)==LSOK)
  {
    record.Clear();
    ParamEntryPtr entry=pfile.FindEntry("Objektiv2Macro");
    if (entry.IsNull() || !entry->IsClass()) return false;
    ParamClassPtr pclass=entry->GetClassInterface();
    int i=0;
    do 
    {
      char name[256];
      sprintf(name,"_%06d",i++);
      entry=pclass->FindEntry(name);
      if (entry.IsNull() || !entry->IsClass()) break;
      SMacroRecord &m=record.Append();
      if (m.LoadRecord(entry->GetClassInterface())==false) return false;
    } 
    while(true);
    return true;
  }
  return false;

}