// BIANMImportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "BIANMImportDlg.h"
#include ".\bianmimportdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBIANMImportDlg dialog


CBIANMImportDlg::CBIANMImportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBIANMImportDlg::IDD, pParent)
    , vAngle(0)
{
	//{{AFX_DATA_INIT(CBIANMImportDlg)
	vInvertX = FALSE;
	vInvertY = FALSE;
	vInvertZ = FALSE;
	vMasterScale = 0.0f;
	//}}AFX_DATA_INIT
    enableEdgeDetect=false;
}


void CBIANMImportDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CBIANMImportDlg)
  DDX_Check(pDX, IDC_INVERTX, vInvertX);
  DDX_Check(pDX, IDC_INVERTY, vInvertY);
  DDX_Check(pDX, IDC_INVERTZ, vInvertZ);
  DDX_Text(pDX, IDC_SCALE, vMasterScale);
  DDV_MinMaxFloat(pDX, vMasterScale, 1.e-010f, 1.e+010f);
  //}}AFX_DATA_MAP
  DDX_Text(pDX, IDC_EDGEDETECT, vAngle);
  DDX_Control(pDX, IDC_EDGEDETECT, wAngle);
  DDV_MinMaxInt(pDX, vAngle, 0, 180);
}


BEGIN_MESSAGE_MAP(CBIANMImportDlg, CDialog)
	//{{AFX_MSG_MAP(CBIANMImportDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBIANMImportDlg message handlers

#define SECTION "Maya Import"
#define MASTERSCALE "Master Scale"
#define ANGLE "Angle detect"
#define INVERTX "Invert X"
#define INVERTY "Invert Y"
#define INVERTZ "Invert Z"

BOOL CBIANMImportDlg::OnInitDialog() 
{
    vMasterScale=(float)atof(theApp.GetProfileString(SECTION,MASTERSCALE,"0.01"));
    vInvertX=theApp.GetProfileInt(SECTION,INVERTX,0);
    vInvertY=theApp.GetProfileInt(SECTION,INVERTY,0);
    vInvertZ=theApp.GetProfileInt(SECTION,INVERTZ,0);
    vAngle=theApp.GetProfileInt(SECTION,ANGLE,80);
	CDialog::OnInitDialog();

    wAngle.EnableWindow(enableEdgeDetect);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBIANMImportDlg::OnOK() 
  {
  CDialog::OnOK();
  char buff[50];
  sprintf(buff,"%f",vMasterScale);
  theApp.WriteProfileString(SECTION,MASTERSCALE,buff);
  theApp.WriteProfileInt(SECTION,INVERTX,vInvertX);
  theApp.WriteProfileInt(SECTION,INVERTY,vInvertY);
  theApp.WriteProfileInt(SECTION,INVERTZ,vInvertZ);
  theApp.WriteProfileInt(SECTION,ANGLE,vAngle);
  }

 