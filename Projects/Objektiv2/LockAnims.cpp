// LockAnims.cpp: implementation of the CLockAnims class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "LockAnims.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLockAnims::CLockAnims()
  {
  for (int i=0;i<CLA_LOCKSIZE;i++)weight[i]=NULL;
  Reset();
  }

//--------------------------------------------------

CLockAnims::~CLockAnims()
  {
  for (int i=0;i<CLA_LOCKSIZE;i++) delete [] weight[i];
  }

//--------------------------------------------------

void CLockAnims::Reset()
  {
  for (int i=0;i<CLA_LOCKSIZE;i++)
    {
    delete [] weight[i];
    weight[i]=NULL;
    levels[i]=0;
    lru[i]=0;
    size[i]=0;
    }
  lrucnt=0;
  }

//--------------------------------------------------

int CLockAnims::FindLod(float lod)
  {
  for (int i=0;i<CLA_LOCKSIZE;i++)
    if (lru[i])	  
      if (levels[i]==lod) 
        {lru[i]=++lrucnt;return i;	  }
  return -1;
  }

//--------------------------------------------------

int CLockAnims::CreateLod(float level, int anims)
  {
  DWORD min=lru[0];
  int idx=0;
  for (int i=0;i<CLA_LOCKSIZE;i++)
    if (lru[i]<min)	 
      {min=lru[i];idx=i;}  	
  lru[idx]=++lrucnt;
  levels[idx]=level;
  weight[idx]=new float[anims];
  memset(weight[idx],0,anims*sizeof(float));
  size[idx]=anims;
  return idx;
  }

//--------------------------------------------------

void CLockAnims::DeleteLod(int idx)
  {
  ASSERT(idx>=0 && idx<CLA_LOCKSIZE);
  delete [] weight[idx];
  levels[idx]=0.0f;
  lru[idx]=0;
  size[idx]=0;
  weight[idx]=NULL;
  }

//--------------------------------------------------

void CLockAnims::SetWeight(int idx, int i, float w)
  {
  ASSERT(idx>=0 && idx<CLA_LOCKSIZE);
  if (i<0 || i>size[idx]) return;
  weight[idx][i]=w;
  }

//--------------------------------------------------

float CLockAnims::GetWeight(int idx, int i)
  {
  if (idx<0) return 0.0f;
  ASSERT(idx<CLA_LOCKSIZE);
  if (i<0 || i>size[idx]) return 0.0f;
  return weight[idx][i];
  }

//--------------------------------------------------

void CLockAnims::CheckNewAnims(float lod, int anims)
  {
  int idx=FindLod(lod);
  if (idx==-1) return;	
  if (size[idx]!=anims)
    {
    float *p=new float[anims];
    float *g=weight[idx];
    for (int i=0;i<anims;i++) p[i]=i<size[idx]?g[i]:0.0f;
    size[idx]=anims;
    delete [] g;
    weight[idx]=p;
    }
  }

//--------------------------------------------------

void CLockAnims::DeleteFrame(float lod, int frame)
  {
  int idx=FindLod(lod);
  if (idx==-1) return;	
  if (frame>=size[idx]) return;
  ASSERT(frame>=0);
  float *g=weight[idx];
  memcpy(g+frame,g+frame+1,(size[idx]-frame-1)*sizeof(float));
  size[idx]--;
  }

//--------------------------------------------------

CLockAnims::CLockAnims(const CLockAnims &other)
  {
  memcpy(levels,other.levels,sizeof(levels));
  memcpy(lru,other.lru,sizeof(lru));
  memcpy(size,other.size,sizeof(size));
  lrucnt=lrucnt;
  for (int i=0;i<CLA_LOCKSIZE;i++)
    {
    weight[i]=new float[size[i]];
    memcpy(weight[i],other.weight[i],sizeof(float)*size[i]);
    }
  }

//--------------------------------------------------

