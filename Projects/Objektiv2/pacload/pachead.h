#ifndef PACHEAD_H_
#define PACHEAD_H_

#include <dbgwnd.h>

#ifdef __cplusplus
extern "C" 
  {
  #endif
  
  int GetPacXSize();
  int GetPacYSize();
  int ReadPac(const char *name);
  unsigned long GetPacPixel(int x,int y);
  void CleanPac();
  char *GetPacFormat();
  
  int InitPal2PacDll(const char *path);
  #define DBG(a) dbgprintf("%s %d\n%s\n",__FILE__,__LINE__,#a,a)
  
  #ifdef __cplusplus
  }

//--------------------------------------------------

#endif

#endif