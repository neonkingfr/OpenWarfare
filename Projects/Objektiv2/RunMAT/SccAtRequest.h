#pragma once
#include <el\interfaces\iscc.hpp>
#include <el/Scc/MsSccProject.hpp>

#define SCCNOTINITED -50

class SccAtRequest : public SccFunctions
{
  MsSccLib& provider;
  SccFunctions *_scc;  
public:
  SccAtRequest(MsSccLib &provider):provider(provider),_scc(0) {}
  ~SccAtRequest(void);

  bool InitScc();

    virtual SCCRTN Open(const char *project_name=NULL, const char *local_path=NULL, const char *server_name=NULL, const char *user_name=NULL)
    {
      if (InitScc()) return _scc->Open(project_name,local_path,server_name,user_name);else return SCCNOTINITED;
    }

    virtual SCCRTN Done() {if (InitScc()) return _scc->Done();else return 0;}
    virtual bool Opened() const {return _scc?_scc->Opened():true;}
    virtual SCCRTN ChooseProject() {if (InitScc()) return ChooseProject();else return 0;}
    virtual SCCRTN ChooseProjectEx(const char *project_name=NULL, const char *local_path=NULL, const char *server_name=NULL, const char *user_name=NULL)
      {if (InitScc()) return _scc->ChooseProjectEx(project_name,local_path,server_name,user_name);else return SCCNOTINITED;}
      virtual SCCRTN GetLatestVersion(const char* LocalFileName) {return InitScc()?_scc->GetLatestVersion(LocalFileName):SCCNOTINITED;}
    virtual SCCRTN GetLatestVersion(const char** LocalFileNames, int NFile) {return InitScc()?_scc->GetLatestVersion(LocalFileNames,NFile):SCCNOTINITED;}
    virtual SCCRTN GetLatestVersionDir(const char* local_file_name, bool recursive= false) {return InitScc()?_scc->GetLatestVersionDir(local_file_name,recursive):SCCNOTINITED;}
    virtual SCCRTN CheckOut(const char* local_file_name, const char *comment=NULL) {return InitScc()?_scc->CheckOut(local_file_name,comment):SCCNOTINITED;}
    virtual SCCRTN CheckOut(const char** local_file_names, int n_file, const char *comment=NULL) {return InitScc()?_scc->CheckOut(local_file_names,n_file,comment):SCCNOTINITED;}
    virtual SCCRTN CheckIn(const char* LocalFileName, const char* Comment= NULL) {return InitScc()?_scc->CheckIn(LocalFileName,Comment):SCCNOTINITED;}
    virtual SCCRTN CheckIn(const char** LocalFileNames, int NFile, const char* Comment= NULL) {return InitScc()?_scc->CheckIn(LocalFileNames,NFile,Comment):SCCNOTINITED;}
    virtual SCCRTN UndoCheckOut(const char* LocalFileName) {return InitScc()?_scc->UndoCheckOut(LocalFileName):SCCNOTINITED;}
    virtual SCCRTN UndoCheckOut(const char** LocalFileNames, int NFile) {return InitScc()?_scc->UndoCheckOut(LocalFileNames,NFile):SCCNOTINITED;}
    virtual SCCRTN Add(const char* LocalFileName, const char* Comment= NULL) {return InitScc()?_scc->Add(LocalFileName,Comment):SCCNOTINITED;}
    virtual SCCRTN Add(const char** LocalFileNames, int NFile, const char* Comment= NULL) {return InitScc()?_scc->Add(LocalFileNames,NFile,Comment):SCCNOTINITED;}
    virtual SCCRTN Remove(const char* LocalFileName, const char* Comment= NULL) {return InitScc()?_scc->Remove(LocalFileName,Comment):SCCNOTINITED;}
    virtual SCCRTN Remove(const char** LocalFileNames, int NFile, const char* Comment= NULL) {return InitScc()?_scc->Remove(LocalFileNames,NFile,Comment):SCCNOTINITED;}
    virtual SCCRTN Diff(const char* LocalFileName) {return InitScc()?_scc->Diff(LocalFileName):SCCNOTINITED;}
    virtual SCCRTN History(const char* LocalFileName) {return InitScc()?_scc->History(LocalFileName):SCCNOTINITED;}
    virtual SCCRTN History(const char** LocalFileNames, int NFile) {return InitScc()?_scc->History(LocalFileNames,NFile):SCCNOTINITED;}
    virtual SCCRTN Properties(const char* local_file_name) {return InitScc()?_scc->History(local_file_name):SCCNOTINITED;}
    virtual SCCSTAT Status(const char* local_file_name) {return _scc==0?-1:_scc->Status(local_file_name);}
    virtual bool UnderSSControl(const char* local_file_name) {return _scc==0?true:_scc->UnderSSControl(local_file_name);}
    virtual bool CheckedOut(const char* local_file_name)  {return _scc==0?false:_scc->CheckedOut(local_file_name);}
    virtual bool Deleted(const char* local_file_name)  {return _scc==0?false:_scc->Deleted(local_file_name);}
    virtual const char* GetUserName() const {return _scc==0?"":_scc->GetUserName();}
    virtual const char* GetProjName() const {return _scc==0?"":_scc->GetProjName();}
    virtual const char* GetLocalPath() const  {return _scc==0?"":_scc->GetLocalPath();}
    virtual const char* GetServerName() const {return _scc==0?"":_scc->GetServerName();}
    virtual SccFunctions *Clone() const 
    {     
      SccAtRequest* clon=new SccAtRequest(provider);
      const_cast<SccAtRequest *>(this)->InitScc();
      clon->_scc=_scc?_scc->Clone():0;
      return clon;
    }
    virtual bool UnderSSControlDir(const char* dir_name) {return _scc==0?false:_scc->UnderSSControlDir(dir_name);}

};
