//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Mat.rc
//
#define IDC_MYICON                      2
#define IDD_MAT_DIALOG                  102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDS_FILERO                      104
#define IDM_EXIT                        105
#define IDS_MATCLASS                    105
#define IDS_HELLO                       106
#define IDS_MATLOADFAILED               106
#define IDI_MAT                         107
#define IDS_STRING107                   107
#define IDI_SMALL                       108
#define IDR_MAINFRAME                   128
#define IDS_NOTSUPPORTED                129
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
