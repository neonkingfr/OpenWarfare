
#if !defined(AFX_MAT_H__9DD548E2_D8D0_4649_B246_F7B1C48D6477__INCLUDED_)
#define AFX_MAT_H__9DD548E2_D8D0_4649_B246_F7B1C48D6477__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"

#include "..\NewMAT\NewMatInterface.hpp"
#include <El/Interfaces/IScc.hpp>
#include "SccAtRequest.h"

using namespace ObjektivLib;

class CMatApp: public IMatAppSide, public IPluginGeneralAppSide
{
	HMODULE _hMatInst;
	IPluginGeneralPluginSide *_plugin;
    IMatPluginSide *_mat;
    MsSccLib SccProvider;
    SccAtRequest scc;
    HWND _lastMatHWND;

    ExternalViewer ExView;
    ExternalViewerConfig ExConfig;

public:
	bool Load(const char *filename);
protected:
    virtual void UpdateViewer();
    virtual void ReloadViewer();
    virtual SccFunctions *GetSccInterface();
    virtual void UseMaterial(const char *filename);
    virtual bool CanUseMaterial();
    virtual CWinApp *GetAppInteface();
    virtual const char *GetProjectRoot();
    virtual ROCheckResult TestFileRO(const _TCHAR *filename,int flags=0, HWND hWnd=NULL);
    virtual bool IsViewerActive();
    virtual IExternalViewer *GetViewerInterface();
    virtual bool OpenOptionsDialog();
    virtual ObjectData *CreatePrimitive(const char *filename, const char *texture, const char *material);
    virtual ObjectData *CreatePrimitive(ObjPrimitiveType type, int segments,float scale, const char *texture,  const char *material);
    virtual void DestroyPrimitive(ObjectData *obj);
    virtual void UpdateViewer(ObjectData *obj);
    virtual HMENU AddSubMenu(PopupMenu menuId, const char *menuName);
    virtual void SelectInO2(const char *resourceName, ResourceType resourceType);
    virtual void MatWindowClosed(); //plugin informuje klientskou aplikaci, �e okno MATu se zav�elo
    virtual bool AddMenuItem(HMENU mnu, const char *menuName, IPluginCommand *command);
    virtual bool AddMenuItem(PopupMenu menuId, const char *menuName, IPluginCommand *command);
    virtual bool AddPopupMenuItem(HMENU mnu, const char *menuName, IPluginCommand *command)     {return false;}
    virtual const LODObject *GetActiveObject();
    virtual const ObjectData *GetActiveLevel();
    virtual bool UpdateActiveObject(const LODObject &newObject);
    virtual bool UpdateActiveLevel(const ObjectData &objectData);
    virtual bool ModifyActiveLevel(const ObjectData *merge=0, Selection *delSelects=0);
    virtual bool BeginUpdate(const char *description);
    virtual bool EndUpdate();
    virtual const char *ReadConfigValue(int idsConfig) {return "";}
    virtual ProgressBarFunctions *GetProgressBarHandler();;
    virtual IFileDlgHistory *GetFileDialogExHandler();



public:
	CMatApp();
	~CMatApp();

	IMatPluginSide &Mat() {return *_mat;}
    IPluginGeneralPluginSide &Plugin() {return *_plugin;}
    void InitScc(void);
};


#endif // !defined(AFX_MAT_H__9DD548E2_D8D0_4649_B246_F7B1C48D6477__INCLUDED_)
