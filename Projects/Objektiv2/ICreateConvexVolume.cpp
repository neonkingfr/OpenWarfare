#include "StdAfx.h"
#include ".\icreateconvexvolume.h"
#include "gePlane.h"
#include <projects/ObjektivLib/Edges.h>
#include <projects/ObjektivLib/Edges.h>
#include <projects/ObjektivLib/ObjToolConvex.h>
#include "resource.h"
#include "Objektiv2Doc.h"
#include <Es\Algorithms\qsort.hpp>

typedef AutoArray<gePlaneGen> PlaneArray;

TypeIsSimple(gePlaneGen);

CICreateConvexVolume::CICreateConvexVolume(bool fast):fast(fast)
  {
  recordable=false;
  }

CICreateConvexVolume::~CICreateConvexVolume(void)
  {
  }

static int *CreatePointList(ObjectData &obj, int &plsize)
  {
  plsize=obj.CountSelectedPoints();
  int *pl=new int[plsize];
  int k=0;
  for (int i=0;i<obj.NPoints();i++) if (obj.PointSelected(i)) pl[k++]=i;
  return pl;
  }

class PlaneArrayCompare
  {
  public:
    int operator()(const gePlaneGen *p1,const gePlaneGen *p2)
      {
      if (fabs(p1->d-p2->d)>0.1f) return p1->d>p2->d?1:-1;
      if (fabs(p1->c-p2->c)>0.1f) return p1->c>p2->c?1:-1;
      if (fabs(p1->b-p2->b)>0.1f) return p1->b>p2->b?1:-1;
      if (fabs(p1->a-p2->a)>0.1f) return p1->a>p2->a?1:-1;
      return 0;
      }
  };

static void CreatePlaneArray(PlaneArray &planes, ObjectData &obj, int *pointlist, int pointcnt)
  {
  ProgressBar<int> pb(pointcnt);
  for (int i=0;i<pointcnt;i++)
    {
    geVector4 vx1=obj.Point(pointlist[i]);
    pb.AdvanceNext(1);
    for (int j=0;j<pointcnt;j++) if (i!=j)
      {
      geVector4 vx2=obj.Point(pointlist[j]);
      int best=-1;
      gePlaneGen bestplane;
      int k,l;
      for (k=0;k<2;k++)
        for (l=0;l<pointcnt;l++) if (l!=i && l!=j && l!=best)
          {                      
          geVector4 vx3=obj.Point(pointlist[l]);
          if (best==-1 || (bestplane.SideF(vx3)>0.00001f))
            {
            if (l<best) {best=-1;break;}
            gePlaneGen pl=gePlaneGen(vx1,vx2,vx3);
            pl.Normalize();
            if (pl.IsFinite())
              {
              bestplane=pl;            
              best=l;
              }
            }
          }
      if (best!=-1) 
        {
        bestplane.Normalize();
        if (bestplane.IsFinite()) 
          planes.Append()=bestplane;
        }

      }
    }
  PlaneArrayCompare cmp;
  QSort(&planes[0],planes.Size(),cmp);
  for (int j=1;j<planes.Size();j++)
    {
    if (cmp(&planes[j-1],&planes[j])==0) {planes.Delete(--j);}
    }
  }



class PoolCompare
  {
  gePlaneGen _sortplane;
  ObjectData *_obj;
  int _root;
  geVector4 _rtvx;
  public:
    PoolCompare(const gePlaneGen &p,ObjectData *obj,int root):_sortplane(p),_obj(obj),_root(root) {_rtvx=obj->Point(root);}
    int operator() (const int *a, const int *b)
      {      
      if (*a==_root) return -1;
      if (*b==_root) return 1;
      geVector4 pta=_obj->Point(*a);
      geVector4 ptb=_obj->Point(*b);
      gePlaneGen testPlane(_sortplane,geLine(_rtvx,ptb));
      float sidef=testPlane.SideF(pta);
      return -(sidef>0)+(sidef<0);
      }
  };

static void CreatePoolFaces(ObjectData *obj, AutoArray<int> &vxpool, gePlaneGen &plane)
  {
  PoolCompare cmp(plane,obj,vxpool[0]);
  QSort(&vxpool[0],vxpool.Size(),cmp);
  int i;
  for (i=2;i<vxpool.Size();i++)
    {
    FaceT fc;
    geVector4 vx1=obj->Point(vxpool[i-1]);
    geVector4 vx2=obj->Point(vxpool[i]);
    fc.CreateIn(obj);
    fc.SetN(3);
    fc.SetPoint(0,vxpool[0]);
    fc.SetPoint(1,vxpool[i-1]);
    fc.SetPoint(2,vxpool[i]);    
    }
  }

static void FindConvexPointsAndFaces(ObjectData *obj, PlaneArray &planes)
  {
  AutoArray<int> vxpool;  
  ProgressBar<int> pb(planes.Size());
  for (int i=0;i<planes.Size();i++)
    {
    pb.AdvanceNext(1);
    vxpool.Clear();
    for (int j=0;j<planes.Size();j++) if (j!=i)
      {
      for (int k=j+1;k<planes.Size();k++) if (k!=i)
        {
        geVector4 vx=planes[i].Intersection(planes[j],planes[k]);        
        if (vx.IsFinite())
          {
          for (int pl=vxpool.Size()-1;pl>=0;pl--)
            {
            geVector4 vxp=obj->Point(vxpool[pl]);
            if (vxp.Distance2(vx)<0.000001)
              goto skip;
            }
          int l;
          for (l=0;l<planes.Size();l++)
             if (l!=i && l!=j && l!=k && (planes[l].SideF(vx)>0.00001f)) 
              break;
          if (l==planes.Size())
            {            
            int pt=obj->ReservePoints(1);
            vxpool.Append()=pt;
            obj->Point(pt).SetPoint(vx);            
            obj->PointSelect(pt);
            }
skip:;
          }
        }
      }
    if (vxpool.Size()>2)
      CreatePoolFaces(obj,vxpool,planes[i]);
    }
  }


int CICreateConvexVolume::Execute(LODObject *lod)
  {
        ObjectData *obj=lod->Active();        
        if (fast)
            obj->GetTool<ObjToolConvex>().MakeConvex(*obj->GetSelection(),true,16,true);    
        else
            obj->GetTool<ObjToolConvex>().MakeConvex(*obj->GetSelection(),true);
        return 0;
}
        /*

  ProgressBar<float> pb(1);
  pb.AdvanceNext(0.7f);
  unsigned int fp=_controlfp(_PC_64,_MCW_PC);
  geVector4 vmin,vmax;
  float maxsize;
  PlaneArray planes;
  ObjectData *obj=lod->Active();
  int plc;
  int *pls=CreatePointList(*obj,plc);
  if (plc==0) {delete [] pls;return -1;}
  vmin=vmax=obj->Point(pls[0]);
  for (int i=1;i<plc;i++)
    {
    geVector4 cur=obj->Point(pls[i]);
    if (cur.x<vmin.x) vmin.x=cur.x;
    if (cur.y<vmin.y) vmin.y=cur.y;
    if (cur.z<vmin.z) vmin.z=cur.z;
    if (cur.x>vmax.x) vmax.x=cur.x;
    if (cur.y<vmax.y) vmax.y=cur.y;
    if (cur.z<vmax.z) vmax.z=cur.z;
    }
  maxsize=__max(fabs(vmin.x-vmax.x),__max(fabs(vmin.y-vmax.y),fabs(vmin.z-vmax.z)));
  CreatePlaneArray(planes,*obj,pls,plc);
  delete [] pls;
  for (int i=obj->NPoints()-1;i>=0;i--) if (obj->PointSelected(i)) obj->DeletePoint(i);
  pb.AdvanceNext(0.2f);
  FindConvexPointsAndFaces(obj,planes);  
  pb.AdvanceNext(0.1f);
  obj->MergePoints(maxsize/100.0f,true);
  obj->SelectFacesFromPoints();
  obj->Squarize(false);
  RecalcNormals2(obj,false);
  _controlfp(fp,0xFFFFFFFF);
  return 0;
  }  */