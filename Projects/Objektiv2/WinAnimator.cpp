// WinAnimator.cpp: implementation of the CWinAnimator class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "WinAnimator.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

float CWinAnimator::animtime=1.0f;

#define TIMHANDLES 8

static HWND timhandles[TIMHANDLES];
static CWinAnimator *timwa[TIMHANDLES];
bool forceinit=true;


static void CALLBACK TimerProc(HWND wnd, UINT val1, UINT val2, DWORD time)
  {
  for (int i=0;i<TIMHANDLES;i++)
    {
    if (timhandles[i]==wnd)
      {
      timwa[i]->Run(time);
      }
    }
  }

//--------------------------------------------------

static bool RegisterWindow(HWND wnd, CWinAnimator *aa)
  {
  for (int i=0;i<TIMHANDLES;i++)
    {
    if (timhandles[i]==0) 
      {
      timhandles[i]=wnd;
      timwa[i]=aa;
      return true;
      }
    }
  return false;
  }

//--------------------------------------------------

static bool UnregisterWindow(HWND wnd)
  {
  for (int i=0;i<TIMHANDLES;i++)	
    if (timhandles[i]==wnd) timhandles[i]=0;
  return true;
  }

//--------------------------------------------------

void CWinAnimator::RunOpen()
  {
  wnd.GetWindowRect(defrect);
  int xm=(defrect.left+defrect.right)>>1;
  int ym=(defrect.bottom+defrect.top)>>1;
  currect=CRect(xm,ym,xm,ym);
  if (RegisterWindow(wnd,this)==false) return;
  timid=wnd.SetTimer(WINANIMTIM,100,TimerProc);
  if (timid==0) 
    {
    UnregisterWindow(wnd);
    return;
    }
  wnd.ShowWindow(SW_HIDE);
  systime=GetTickCount();
  }

//--------------------------------------------------

void CWinAnimator::Run(DWORD tim)
  {
  float ss=(float)(tim-systime)/1000.0f;
  float fact=ss/animtime;
  if (fact>1.0f)
    {
    fact=1.0f;
    wnd.KillTimer(timid);
    UnregisterWindow(wnd);
    }
  int xm=(defrect.left+defrect.right)>>1;
  int ym=(defrect.bottom+defrect.top)>>1;
  currect.left=(int)(xm+(defrect.left-xm)*fact);
  currect.right=(int)(xm+(defrect.right-xm)*fact);
  currect.top=(int)(ym+(defrect.top-ym)*fact);
  currect.bottom=(int)(ym+(defrect.bottom-ym)*fact);
  wnd.MoveWindow(&currect,TRUE);
  wnd.ShowWindow(SW_SHOW);  
  }

//--------------------------------------------------

CWinAnimator::~CWinAnimator()
  {
  UnregisterWindow(wnd);
  }

//--------------------------------------------------

