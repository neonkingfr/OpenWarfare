#pragma once
#include <afxcmn.h>


// DlgProxyLevel dialog

class CObjektiv2Doc;

class DlgProxyLevel : public CDialog
{
	DECLARE_DYNAMIC(DlgProxyLevel)


    static DlgProxyLevel *oneInstance;
public:
	DlgProxyLevel(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgProxyLevel();



// Dialog Data
	enum { IDD = IDD_PROXYLEVEL };

    CObjektiv2Doc *document;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  virtual BOOL OnInitDialog();
protected:
  virtual void PostNcDestroy();
public:
  CSliderCtrl wSlider;
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
protected:
  virtual void OnOK();
  virtual void OnCancel();
};
