// IToolAdaptaceAnimace.cpp: implementation of the CIToolAdaptaceAnimace class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "IToolAdaptaceAnimace.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIToolAdaptaceAnimace::CIToolAdaptaceAnimace(float lod)
  {
  reflod=lod;
  recordable=false;
  }

//--------------------------------------------------

CIToolAdaptaceAnimace::~CIToolAdaptaceAnimace()
  {
  
  }

//--------------------------------------------------

static Vector3P GetCenterOfSelection(ObjectData &obj, Selection *sel)
  {
  Vector3P ps;
  ps[0]=0;
  ps[1]=0;
  ps[2]=0;
  int cnt=0;
  int ptc=obj.NPoints();
  for (int i=0;i<ptc;i++)
    {
    if (sel->PointSelected(i)) 
      {ps+=obj.Point(i);cnt++;}
    }
  ps/=cnt;
  return ps;
  }

//--------------------------------------------------

static void MoveSelectionToPos(ObjectData &obj, Selection *sel, Vector3P rel)
  {
  int cnt=0;
  int ptc=obj.NPoints();
  for (int i=0;i<ptc;i++)
    {
    if (sel->PointSelected(i)) 
      {
      PosT &ps=obj.Point(i);
      ps+=rel;
      }
    }
  }

//--------------------------------------------------

int CIToolAdaptaceAnimace::Execute(LODObject *lod)
  {
  int p;
  int scnt=0;
  int rlevel=lod->FindLevel(reflod);
  ObjectData &refobj=*(lod->Level(rlevel));
  ObjectData &curobj=*(lod->Active());
  if (curobj.NAnimations()) curobj.UseAnimation(0);
  if (refobj.NAnimations()) refobj.UseAnimation(0);
  else 
    {AfxMessageBox("No animations in reference object",MB_OK);return -1;}
  for (p=0;p<MAX_NAMED_SEL;p++)
    {
    NamedSelection *rns=refobj.GetNamedSel(p);
    if (rns)
      {
      NamedSelection *cns=curobj.GetNamedSel(rns->Name());
      if (rns && cns)
        {
        int idxsel=curobj.FindNamedSel(cns->Name());
        Vector3P rct=GetCenterOfSelection(refobj,rns);
        Vector3P cct=GetCenterOfSelection(curobj,cns);
        workdata[p].center=cct;
        workdata[p].odchylka=(cct-rct);
        workdata[p].idxsel=idxsel;
        }
      }
    }
  curobj.DeleteAllAnimations();
  AnimationPhase *phase=new AnimationPhase(&curobj);
  phase->SetTime(refobj.GetAnimation(refobj.CurrentAnimation())->GetTime());
  curobj.AddAnimation(*phase);
  curobj.RedefineAnimation(0);
  curobj.UseAnimation(0);
  delete phase;
  ProgressBar<int> pb(refobj.NAnimations());
  for (p=1;p<refobj.NAnimations();p++)
    {
    pb.AdvanceNext(1);
    refobj.UseAnimation(p);
    AnimationPhase *phase=new AnimationPhase(&curobj);
    phase->SetTime(refobj.GetAnimation(refobj.CurrentAnimation())->GetTime());
    curobj.RedefineAnimation(*phase);
    curobj.AddAnimation(*phase);
    delete phase;
    curobj.UseAnimation(p);
    for (int i=0;i<MAX_NAMED_SEL;i++)
      {
      NamedSelection *rns=refobj.GetNamedSel(i);
      if (rns)
        {
        NamedSelection *cns=refobj.GetNamedSel(workdata[i].idxsel);
        if (cns)
          {
          Vector3P cnt=GetCenterOfSelection(refobj,rns);
          Vector3P rel=cnt-workdata[i].center+workdata[i].odchylka;
          MoveSelectionToPos(curobj,cns,rel);
          }
        }
      }
    curobj.UseAnimation(0);
    }
  refobj.UseAnimation(0);
  return 0;
  }

//--------------------------------------------------

