// DlgInputFile.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgInputFile.h"
#include ".\dlginputfile.h"


// CDlgInputFile dialog

IMPLEMENT_DYNAMIC(CDlgInputFile, CDialog)
CDlgInputFile::CDlgInputFile(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgInputFile::IDD, pParent)
    , vFilename(_T(""))
  {
  save=false;
  browse=true;
}

CDlgInputFile::~CDlgInputFile()
{
}

void CDlgInputFile::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Control(pDX, IDC_EDIT1, wEdit);
DDX_Control(pDX, IDC_BROWSE, wBrowse);
DDX_Text(pDX, IDC_EDIT1, vFilename);
}


BEGIN_MESSAGE_MAP(CDlgInputFile, CDialog)
  ON_BN_CLICKED(IDC_BROWSE, OnBnClickedBrowse)
END_MESSAGE_MAP()


// CDlgInputFile message handlers

BOOL CDlgInputFile::OnInitDialog()
  {
  CDialog::OnInitDialog();

  SetWindowText(title);
  wBrowse.SetIcon(theApp.LoadIcon(IDI_BROWSE));
  AdjustEditPos();
  wBrowse.EnableWindow(browse);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

void CDlgInputFile::OnBnClickedBrowse()
  {
  UpdateData();
  DWORD flags=save?OFN_HIDEREADONLY:OFN_HIDEREADONLY|OFN_FILEMUSTEXIST;  
  CFileDialogEx fdlg(!save,NULL,vFilename,flags,filter,NULL);
  fdlg.m_ofn.lpstrTitle=title;
  int res=fdlg.DoModal();
  if (res==IDOK)
    {
    wEdit.SetWindowText(fdlg.GetPathName());
    AdjustEditPos();
    }
  }

void CDlgInputFile::GuessFilter(const char *filename)
  {
  CString ext;
  if (filename==NULL || filename[0]==0) filename=".*";
  ext.Format("*%s",Pathname::GetExtensionFromPath(filename));
  filter.Format(IDS_GUESSFILTERMASK,ext,ext);
  }
void CDlgInputFile::AdjustEditPos(void)
  {
  int len=wEdit.GetWindowTextLength();
  wEdit.SetSel(len,len);
  }
