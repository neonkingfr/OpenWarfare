// IChilliSkinner.h: interface for the CIChilliSkinner class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ICHILLISKINNER_H__C9002983_E402_4299_B8C9_A9EDA04693E0__INCLUDED_)
#define AFX_ICHILLISKINNER_H__C9002983_E402_4299_B8C9_A9EDA04693E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CIChilliSkinner : public CICommand  
{
    int beginface;
public:
	static int SelectBeginFaceFromPin(ObjectData &obj,LPHVECTOR pin);
	int Execute(LODObject *lod);
    CIChilliSkinner(int begfc);  
	virtual ~CIChilliSkinner();

};

#endif // !defined(AFX_ICHILLISKINNER_H__C9002983_E402_4299_B8C9_A9EDA04693E0__INCLUDED_)
