// IFlattenPoints.h: interface for the CIFlattenPoints class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IFLATTENPOINTS_H__D72EFBFC_CC2C_4F57_886C_AC48512A2EDB__INCLUDED_)
#define AFX_IFLATTENPOINTS_H__D72EFBFC_CC2C_4F57_886C_AC48512A2EDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CIFlattenPoints : public CICommand  
  {
  HVECTOR plane;
  public:
    virtual int Execute(LODObject *obj);
    virtual void Save(ostream &str);
    virtual int Tag();
    CIFlattenPoints(istream &str);
    static void GetPlaneFromModel(ObjectData *obj, HVECTOR plane);
    CIFlattenPoints(HVECTOR p);
    
  };

//--------------------------------------------------

#endif // !defined(AFX_IFLATTENPOINTS_H__D72EFBFC_CC2C_4F57_886C_AC48512A2EDB__INCLUDED_)
