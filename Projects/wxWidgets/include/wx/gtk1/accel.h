/////////////////////////////////////////////////////////////////////////////
// Name:        wx/gtk1/accel.h
// Purpose:     wxAcceleratorTable redirection file
// Author:      Julian Smart
// Modified by:
// Created:
// Copyright:   (c) Julian Smart
// RCS-ID:      $Id: accel.h,v 1.12 2006/01/23 02:27:54 MR Exp $
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

// VZ: keeping the old file just in case we're going to have a native GTK+
// wxAcceleratorTable implementation one day, but for now use the generic
// version
#include "wx/generic/accel.h"
