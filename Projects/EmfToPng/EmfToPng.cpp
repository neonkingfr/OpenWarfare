// EmfToTga.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


class PngWritter
{
  FILE *output;
  png_structp png_ptr;
  png_infop info_ptr;
public:
  PngWritter():output(0) {}
  ~PngWritter()
  {
    if (png_ptr || info_ptr) png_destroy_write_struct(&png_ptr, &info_ptr);
    if (output) fclose(output);
  }

  bool OpenPngFile(const char *name, unsigned int width, unsigned int height)
  {
    output=fopen(name,"wb");
    if (!output) return false;
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL,NULL,NULL);      
    if (!png_ptr) return false;
    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) return false;
    png_init_io(png_ptr,output);
    png_set_compression_level(png_ptr, Z_BEST_COMPRESSION);
    png_set_IHDR(png_ptr, info_ptr,
      width,	/* width */
      height,	/* height */
      8,	/* bit depth */
      PNG_COLOR_TYPE_RGB,
      PNG_INTERLACE_NONE,
      PNG_COMPRESSION_TYPE_DEFAULT,
      PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);   
    return true;
  }

  void WriteRow(const unsigned char *rowData)
  {
    png_write_row(png_ptr,(png_bytep)rowData);
  }
};

bool DrawEmfToBitmap(HENHMETAFILE meta, PngWritter &pic, const RECT &src, const RECT &bounding)
{
  RECT mapRect;
  mapRect.left=0;
  mapRect.top=0;
  mapRect.right=src.right-src.left;
  mapRect.bottom=src.bottom-src.top;

  unsigned long lineoffset=(mapRect.right*3+3)& ~0x3;

  BITMAPINFO bmi;

  ZeroMemory(&bmi,sizeof(bmi));
  bmi.bmiHeader.biSize=sizeof(bmi.bmiHeader);
  bmi.bmiHeader.biWidth=mapRect.right;
  bmi.bmiHeader.biHeight=mapRect.bottom;
  bmi.bmiHeader.biPlanes=1;
  bmi.bmiHeader.biSizeImage=mapRect.bottom*lineoffset;
  bmi.bmiHeader.biBitCount=24;
  bmi.bmiHeader.biCompression=BI_RGB;  
  puts("...alocating temporally bitmap");

  HWND wnd=GetDesktopWindow();
  HDC ddc=GetDC(wnd);
  HDC dc=CreateCompatibleDC(ddc);
  HBITMAP bmp=CreateCompatibleBitmap(ddc,mapRect.right,mapRect.bottom);
  ReleaseDC(wnd,ddc);
  if (bmp==0)
  {
    DeleteDC(dc);
    return false;
  }
  HBITMAP old=(HBITMAP)SelectObject(dc,bmp);
  SetMapMode(dc,MM_ANISOTROPIC);
  SetViewportExtEx(dc,mapRect.right,mapRect.bottom,0);
  SetWindowExtEx(dc,mapRect.right,mapRect.bottom,0);
  SetWindowOrgEx(dc,src.left,src.top,0);
  SetViewportOrgEx(dc,0,0,0);

  puts("...rendering vectors into the bitmap");
  PlayEnhMetaFile(dc,meta,&bounding);

  /*SelectObject(dc,GetStockObject(18));
  SetDCBrushColor(dc,RGB(0,255,255));
  Rectangle(dc,0,0,500,500);*/


  COLORREF px=GetPixel(dc,10,10);

  SelectObject(dc,old);

  puts("...converting pixel format");

  unsigned char *buff=new unsigned char[lineoffset*mapRect.bottom];
  if (buff==0) 
  {
    puts("ERROR: Unable to allocate memory");
    return false;
  }


  GetDIBits(dc,bmp,0,mapRect.bottom,buff,&bmi,DIB_RGB_COLORS);



  puts("...writting png");

  for (int y=0;y<mapRect.bottom;y++)
  {
      unsigned char *line=buff+(mapRect.bottom-1-y)*lineoffset;     
      unsigned char *p=line;
      for (int x=0;x<mapRect.right;x++,p+=3)
      {
        unsigned char a=p[0];
        p[0]=p[2];
        p[2]=a;        
      }
      pic.WriteRow(line);
  }
  puts("...cleanup");
  DeleteDC(dc);
  DeleteObject(bmp);
  delete [] buff;
  return true;
}

int _tmain(int argc, _TCHAR* argv[])
{

  if (argc<2)
  {
    puts("Usage: EmfToPng source_file [zoom]");
    return 0;
  }

  Pathname name(argv[1]);
  HENHMETAFILE meta=GetEnhMetaFile(name);
  if (meta==0 || meta==INVALID_HANDLE_VALUE)
  {
    puts("Error: Open metafile error");
    return -1;
  }

  float zoom=1;
  if (argc>2)
  {
    zoom=(float)atof(argv[2]);
    if (zoom<0.0001)
    {
      puts("Error: Invalid parameter zoom");
      return -2;
    }
  }

  ENHMETAHEADER hdr;
  GetEnhMetaFileHeader(meta,sizeof(hdr),&hdr);

  puts("Loading vector format into the memory");

  BYTE *buff=new BYTE[hdr.nBytes];
  GetEnhMetaFileBits(meta,hdr.nBytes,buff);
  DeleteEnhMetaFile(meta);

  meta=SetEnhMetaFileBits(hdr.nBytes,buff);
  delete [] buff;


  RECT bounds;
  bounds.left=toLargeInt(hdr.rclBounds.left*zoom);
  bounds.top=toLargeInt(hdr.rclBounds.top*zoom);
  bounds.right=toLargeInt((hdr.rclBounds.right+1)*zoom);
  bounds.bottom=toLargeInt((hdr.rclBounds.bottom+1)*zoom);


  puts("Creating the PNG file...");
  PngWritter png;
  name.SetExtension(".png");
  if (png.OpenPngFile(name,bounds.right-bounds.left,bounds.bottom-bounds.top)==false)
  {
    puts("Unable to create PNG file");
    DeleteEnhMetaFile(meta);
    return -1;
  }

  int step=bounds.bottom-bounds.top;
  for (int y=0,cnt=bounds.bottom-bounds.top;y<cnt;y+=step)
  {
    printf("Progress: %d%%\n",y*100/cnt);
    RECT srcRect;
    srcRect=bounds;
    srcRect.top=srcRect.top+y;
    srcRect.bottom=srcRect.top+step;
    if (!DrawEmfToBitmap(meta,png,srcRect,bounds))
    {
      step/=2;
      if (step==0)
      {
        puts("ERROR: Cannot render to bitmap - resource limit exceeded");
        break;
      }
      y-=step;
    }
  }

  DeleteEnhMetaFile(meta);
  return 0;
}

