#pragma once


class ObjectExport : public ObjectData
{
  float _masterScale;
public:
  ObjectExport(float masterScale=1):_masterScale(masterScale) {}
  bool ExtractGeometry(MFnMesh &fMesh, MDagPath &fDagPath, bool invert);  
  bool TessellatePolygon(int polygon, MFnMesh &mesh, RString texture, RString material, MStringArray &uvsetlist);
  bool ExtractWeights(MFnMesh &fMesh, MDagPath fDagPath);
};
