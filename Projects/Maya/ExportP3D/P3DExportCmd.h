#pragma once

class P3DExportCmd :  public MPxCommand
{
public:
  virtual MStatus P3DExportCmd::doIt( const MArgList& args );
  static void *Create();
  static MSyntax GetSyntax();
};
