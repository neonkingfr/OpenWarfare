//
// Copyright (C) 2005 ExportP3D 
// 
// File: pluginMain.cpp
//
// Author: Maya SDK Wizard
//


#include "common.h"
#include <maya/MFnPlugin.h>
#include "P3DExporter.h"
#include "P3DExportCmd.h"


MStatus initializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is loaded into Maya.  It 
//		registers all of the services that this plug-in provides with 
//		Maya.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{ 
	MStatus   status;
	MFnPlugin plugin( obj, "ExportP3D", "5.0", "Any");

    plugin.registerFileTranslator("Objektiv2 P3D object","",P3DExporter::Create);
    plugin.registerCommand("exportP3D",P3DExportCmd::Create,P3DExportCmd::GetSyntax);

	// Add plug-in feature registration here
	//

	return status;
}

MStatus uninitializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is unloaded from Maya. It 
//		deregisters all of the services that it was providing.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status;
	MFnPlugin plugin( obj );

	// Add plug-in feature deregistration here
	//
    plugin.deregisterFileTranslator("P3DExporter");
    plugin.deregisterCommand("exportP3D");

	return status;
}

