//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by checkVer.rc
//
#define IDC_MYICON                      2
#define IDD_CHECKVER_DIALOG             102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDC_CHECKVER                    109
#define IDR_MAINFRAME                   128
#define IDS_APPNAME                     129
#define IDS_NOPARAMETER                 130
#define IDS_PARSEERROR                  131
#define IDI_ICON1                       131
#define IDI_CHECKVER                    131
#define IDS_UPDATEAUTO                  132
#define IDS_UPDATEMAN                   133
#define IDS_INIERROR_NOLOCALNAME        134
#define IDS_INIERROR_EXCEPTINGCLASS     135
#define IDS_INIERROR_NOCHECKENTRY       136
#define IDS_SPAWNERROR                  137
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
