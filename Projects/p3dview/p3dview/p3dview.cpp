// p3dview.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "p3dview.h"
#include "p3dviewDlg.h"

#include "mlodhandler.h"
#include "paramparse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cp3dviewApp

BEGIN_MESSAGE_MAP(Cp3dviewApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// Cp3dviewApp construction

Cp3dviewApp::Cp3dviewApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only Cp3dviewApp object

Cp3dviewApp theApp;


// Cp3dviewApp initialization

BOOL Cp3dviewApp::InitInstance()
{
	SetRegistryKey(_T("p3dview"));

	if(__argc > 1) {
		if(_strcmpi(__argv[1], "-render")) {
			// Check if P3Dview instance is already running
			HWND cw = FindWindow("P3Dview", NULL);
			if(cw) {
				RptF("Sending: <%s>", __argv[1]);
				COPYDATASTRUCT cpd;
				cpd.dwData = 0;
				cpd.cbData = (DWORD)strlen(__argv[1])+1;
				cpd.lpData = __argv[1];
				SendMessage(cw, WM_COPYDATA, NULL, (LPARAM)&cpd);
				return FALSE;
			}
		} else {
			// TODO: ugly duplicate code from main.cpp here
			// Going to render a screenshot
			mlodloader *mlodh = new mlodloader(NULL, NULL);
			SHOTPARAM p;

			// Default parameters
			strcpy(p.infile, "default.p3d");
			strcpy(p.outfile, "default.tga");
			p.width = 512; p.height = 512;
			p.aalevel = 8;
			p.camx = -45; p.camy = 45;
			p.camdist = 1;
			p.ortho = false;
			p.nogrid = false;
			p.lodlevel = -1;
			p.background = 0x00DDDDDD;
			strcpy(p.texpath, ".");
			strcpy(p.proxypath, ".");

			// Parse command line parameters
			p.width = paramparse::getInt("-w", p.width);
			p.height = paramparse::getInt("-h", p.height);
			p.aalevel = paramparse::getInt("-aa", p.aalevel);

			p.lodlevel = paramparse::getFloat("-lod", p.lodlevel);
			p.camx = paramparse::getFloat("-camx", p.camx);
			p.camy = paramparse::getFloat("-camy", p.camy);
			p.camdist = paramparse::getFloat("-camd", p.camdist);

			p.ortho = paramparse::getBool("-ortho", p.ortho);		
			p.nogrid = paramparse::getBool("-nogrid", p.nogrid);

			paramparse::getString("-i", p.infile);
			paramparse::getString("-o", p.outfile);
			paramparse::getString("-texpath", p.texpath);
			paramparse::getString("-proxypath", p.proxypath);

			// Load selections to be hidden
			char hsfpath[256] = "";
			paramparse::getString("-hideselections", hsfpath);
			if(strlen(hsfpath) > 1) {
				vector<string> hnames;
				FILE *f = fopen(hsfpath, "rb");
				if(!f)
					printf("Unable to open %s", hsfpath), exit(-1);
				char line[256];
				printf("Hiding selections: ", line);
				while(fgets(line, 256, f)) {
					if(strlen(line) > 2) {
						char *c = strrchr(line, '\r');
						if(c) *c = 0x00;
						c = strrchr(line, '\n');
						if(c) *c = 0x00;
						printf("<%s> ", line);
						hnames.push_back(line);
					}
				}
				printf("\n", line);
				fclose(f);
				mlodh->setHiddenSelections(hnames, true);
			} else {
				// Want to still hide proxies
				vector<string> empty;
				mlodh->setHiddenSelections(empty, true);
			}
			mlodh->renderScreenshot(&p);

			delete mlodh;
			return FALSE;
		}
	}
	

	// Register P3Dview class
	WNDCLASS wc;
	::GetClassInfo(AfxGetInstanceHandle(), "#32770", &wc);
	wc.lpszClassName = "P3Dview";
	AfxRegisterClass(&wc);

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("P3Dview"));

	Cp3dviewDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
