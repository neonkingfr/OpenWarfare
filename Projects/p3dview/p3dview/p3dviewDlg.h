// p3dviewDlg.h : header file
//

#pragma once

#include "mlodhandler.h"
#include "Direct3Dview.h"
#include "afxwin.h"

// Cp3dviewDlg dialog
class Cp3dviewDlg : public CDialog
{
// Construction
public:
	Cp3dviewDlg(CWnd* pParent = NULL);	// standard constructor
	~Cp3dviewDlg();

// Dialog Data
	enum { IDD = IDD_P3DVIEW_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
	mlodloader *mlodh;
	void UpdateTitle();

	int lW, lH;

// Implementation
protected:
	HICON m_hIcon;
	CStatusBar m_bar;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	BOOL DestroyWindow();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	BOOL PreTranslateMessage(MSG* pMsg);
	CDirect3Dview cView;
	CListBox cLodList;
	afx_msg void OnBnClickedNextmodel();
	afx_msg void OnBnClickedPrevmodel();	
	afx_msg LRESULT OnRequestLoadFile(WPARAM, LPARAM);
	afx_msg LRESULT OnModelLoaded(WPARAM, LPARAM);
	afx_msg LRESULT OnModelList(WPARAM, LPARAM);
	afx_msg LRESULT OnLODLoaded(WPARAM, LPARAM);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLbnSelchangeLodlist();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CButton cButPrev;
	CButton cButNext;
	CButton cButBoxa;
	CButton cButBoxb;
	CButton cButBoxc;
	CButton cButBoxd;
	CButton cButBoxe;
	afx_msg void OnBnClickedBa();
	afx_msg void OnBnClickedBb();
	afx_msg void OnBnClickedBc();
	afx_msg void OnBnClickedBd();
	afx_msg void OnBnClickedBe();
	CButton cButBoxGrid;
public:
	afx_msg void OnBnClickedBgrid();
public:
	CListBox cFileList;
public:
	afx_msg void OnNMCustomdraw3dview(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
public:
	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
public:
	afx_msg void OnLbnSelchangeFilelist();
public:
	CButton cButCopypath;
public:
	CButton cButOpeno2;
public:
	CButton cButScreenshot;
public:
	CButton cButProperties;
public:
	afx_msg void OnBnClickedCopypath();
public:
	afx_msg void OnDropFiles(HDROP hDropInfo);
	CEdit cTexPath;
	afx_msg void OnEnChangeTexpath();
};
