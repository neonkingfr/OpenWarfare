//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by p3dview.rc
//
#define ID_INDICATOR_STATUS             101
#define IDD_P3DVIEW_DIALOG              102
#define ID_INDICATOR_VERTICES           103
#define ID_INDICATOR_FACES              104
#define IDR_MAINFRAME                   128
#define IDB_BOXA                        129
#define IDB_BOXB                        130
#define IDD_DIALOG1                     130
#define IDB_BOXC                        131
#define IDB_BOXD                        132
#define IDB_BOXE                        133
#define IDB_GRID                        134
#define IDC_LODLIST                     1002
#define IDC_NEXTMODEL                   1005
#define IDC_PREVMODEL                   1006
#define IDC_BA                          1007
#define IDC_BB                          1008
#define IDC_BC                          1009
#define IDC_BD                          1010
#define IDC_BE                          1011
#define IDC_BGRID                       1012
#define IDC_FILELIST                    1013
#define IDC_SCREENSHOT                  1017
#define IDC_PROPERTIES                  1018
#define IDC_RESWIDTH                    1018
#define IDC_COPYPATH                    1019
#define IDC_RESHEIGHT                   1019
#define IDC_OPENO2                      1020
#define IDC_EDIT3                       1020
#define IDC_COMBO1                      1021
#define IDC_EDIT4                       1022
#define IDC_EDIT5                       1023
#define IDC_COMBO2                      1024
#define IDC_CHECK1                      1025
#define IDC_EDIT6                       1026
#define IDC_EDIT1                       1027
#define IDC_TEXPATH                     1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
