namespace paramparse {
	void getString(char *param, char *out);
	int getInt(char *param, int def=0);
	float getFloat(char *param, float def=0);
	bool getBool(char *param, bool def=false);
}

static void paramparse::getString(char *param, char *out) {
	for(int i=0;i<__argc-1;i++)
		if(!_strcmpi(__argv[i], param))
			strcpy(out, __argv[i+1]);
}

static int paramparse::getInt(char *param, int def) {
	for(int i=0;i<__argc-1;i++)
		if(!_strcmpi(__argv[i], param))
			return atoi(__argv[i+1]);
	return def;
}

static float paramparse::getFloat(char *param, float def) {
	for(int i=0;i<__argc-1;i++)
		if(!_strcmpi(__argv[i], param))
			return (float)atof(__argv[i+1]);
	return def;
}

static bool paramparse::getBool(char *param, bool def) {
	for(int i=0;i<__argc;i++)
		if(!_strcmpi(__argv[i], param))
			return true;	
	return def;
}