#pragma once

#include <projects/ObjektivLib/ObjectData.h>
#include <projects/ObjektivLib/ObjToolAnimation.h>
#include <projects/ObjektivLib/ObjToolClipboard.h>
#include <projects/ObjektivLib/ObjToolsMatLib.h>
#include <projects/ObjektivLib/ObjToolProxy.h>
#include <projects/ObjektivLib/ObjToolTopology.h>
#include <projects/ObjektivLib/LODObject.h> 

#include <projects/Objektiv2/d3drenderer/d3drenderer.h>
#include <process.h>

using namespace ObjektivLib;
using namespace std;

// Screenshot parameters
typedef struct {
	char infile[256];	// Input p3d file
	char outfile[256];	// Output image file
	int	width, height;	// Image height in pixels
	int aalevel;		// Antialiasing level
	DWORD background;	// Background color (ie. 0xDDDDDDDD)
	float camx, camy;	// Camera rotation in degrees
	float camdist;		// Camera distance (relative to model size)
	bool ortho;			// Orthographic projection, perspective if false
	bool nogrid;		// No grid if true
	float lodlevel;		// LOD level to render
	char texpath[256];	// Path to load textures from
	char proxypath[256];	// Path to load proxy models from
} SHOTPARAM;

class mlodloader {
public:
	mlodloader(HWND rootwin, HWND rendwin);
	~mlodloader();
	bool loadModel(char *path,  bool updateList=true);
	void loadLOD(int id);
	void render();
	LODObject* getModel();
	ObjectData* getLOD();
	const char* getLODname(float res);	
	const char* GetModelName(int idx=-1);		 
	void SetDrawStyle(int style);
	void SetGrid(bool on) {drawGrid = on;};
	bool GetGrid() {return drawGrid;};
	void AdjustCamera(int dx, int dy, int dd);

	void setProxyPath(char *path) {d3dr.setProxyPath(path);};
	void setTexturePath(char path[256], bool doFlush=false);
	int getModelNumber() {return modelsCurID;};
	int getNumModels() {return (int)models.size();};
	bool GetProjection() {return camOrtho;};
	void SetProjection(bool isOrtho) {camOrtho = isOrtho;};
	void nextModel();
	void prevModel();
	void setModel(int idx);

	HWND createDummyWindow(void);
	bool renderScreenshot(SHOTPARAM *p);
	int updateModelListPath(char *path);
	void setHiddenSelections(vector<string> tohide, bool wantHideProxies);
	
	int drawstyle;
	enum {
		S_WIRE,
		S_SOLID,
		S_SOLIDWIRE,
		S_TEXTURE,
		S_TEXTUREWIRE,
	};

private:
	LODObject	mdl;
	d3drenderer	d3dr;
	HWND		win;	// Root window
	HWND		rwin;	// Render viewport window
	HANDLE		wgo;
	volatile bool	running;

	float	camx, camy, camdist;
	float	modelSize, modelHeight;	// Size&height of first lod level
	bool	camOrtho;
	bool	drawGrid;
	float DEG45;

	vector<string>	models;	// List of models in current directory
	int				modelsCurID;
	vector<string>	hiddenSelections;	// Names of selections not to be drawn
	bool			doHideProxies;	// Should proxies be hidden
		
	enum {
		ST_INVALID,	// No model loaded
		ST_LOADING,	// Model is currently being loaded
		ST_MODEL,	// Model is loaded
		ST_MODEL_VX,	// Model with vertex data
		ST_MODEL_TX,	// Model with vertex + texture data
	};
	volatile int status;

	// WM_USER messages sent to the GUI window to request updates
	const static int WM_USER_MODELLOADED = (WM_USER+501);
	const static int WM_USER_LODLOADED = (WM_USER+502);
	const static int WM_USER_MODELLIST = (WM_USER+503);

	// Model paths, currently loaded model and wanted model
	const static int	PATHLEN	= 512;
	char	curModel[PATHLEN];
	char	newModel[PATHLEN];
	int		wantLOD, curLOD;

	void refresh();
	void hideSelections(ObjectData *obj, vector<string> hlist, bool hideProxies);

	void worker();
	static void thread(void *ptr) {((mlodloader*)ptr)->worker();};
};

