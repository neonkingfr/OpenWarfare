// Direct3Dview.cpp : implementation file
//

#include "stdafx.h"
#include "p3dview.h"
#include "Direct3Dview.h"


// CDirect3Dview

IMPLEMENT_DYNAMIC(CDirect3Dview, CWnd)

CDirect3Dview::CDirect3Dview()
{

}

CDirect3Dview::~CDirect3Dview()
{
}

void CDirect3Dview::setMlodh(mlodloader *h) {
	mlodh = h;
}

BEGIN_MESSAGE_MAP(CDirect3Dview, CWnd)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()



// CDirect3Dview message handlers


BOOL CDirect3Dview::OnEraseBkgnd(CDC* pDC)  {
	return FALSE;
}

void CDirect3Dview::OnMouseMove(UINT nFlags, CPoint p) {		
	static int lx = p.x;
	static int ly = p.y;

	if(nFlags & MK_RBUTTON) {
		int dx = p.x - lx;
		int dy = p.y - ly;
		mlodh->AdjustCamera(dx, dy, 0);
	} else if(nFlags & MK_LBUTTON) {
		int dy = p.y - ly;
		mlodh->AdjustCamera(0, 0, dy);
	}
	lx = p.x;
	ly = p.y;
	//CDialog::OnMouseMove(nFlags, p);
}

void CDirect3Dview::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CWnd::OnPaint() for painting messages	
	if(mlodh) 
		mlodh->render();	
}
