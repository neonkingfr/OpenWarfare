#include "stdafx.h"

#include "mlodhandler.h"

mlodloader::mlodloader(HWND rootwin, HWND rendwin) {
	mdl = NULL;
	curModel[0] = 0x00;
	newModel[0] = 0x00;
	running = false;
	status = ST_INVALID;

	DEG45 = 0.7853f;
	drawstyle = S_TEXTURE;
	camx = -DEG45;
	camy = DEG45;
	camdist = 1;
	drawGrid = true;
	camOrtho = false;
	modelSize = 1;
	modelHeight = 0;
	wgo = NULL;

	doHideProxies = false;

	if(!rootwin)
		return;	// No d3d/thread initialization

	wgo = CreateEvent(NULL, FALSE, FALSE, "mlodloaderNewData");	

	// Init D3D and begin worker thread
	rwin = rendwin;
	win = rootwin;
	if(d3dr.initD3D(rwin, 4, true) == -1) {
		MessageBox(win, "Direct3D Initialization failed", "P3Dview", MB_OK);
		exit(0);
	} else {
		//d3dr.setTexturePath("W:/v/Projects/Objektiv2/Bin");
		d3dr.setTexturePath(".");
		_beginthread(thread, 0, this);
		while(!running) Sleep(1);
	}
}

mlodloader::~mlodloader() {
	running = false;	
	if(wgo) {
		SetEvent(wgo);
		CloseHandle(wgo);
		wgo = NULL;
	}
}


// Main worker thread
void mlodloader::worker() {
	RptF("mlodloader::worker begin");
	running = true;
	while(running) {
		WaitForSingleObject(wgo, INFINITE);
		if(!running)
			break;
		RptF("mlodloader::worker GO");

		if(_strcmpi(newModel, curModel)) {
			// New model load request
			char tmpModel[512];
			strcpy(tmpModel, newModel);
			RptF("Loading model %s...", newModel);
			status = ST_LOADING;
			PostMessage(win, WM_USER_MODELLOADED, false, NULL);						
			refresh();

			ifstream f;
			mdl = NULL;
			f.open(newModel, ios::in|ios::binary);
			if(mdl.Load(f, 0, 0) == -1) {
				RptF("Model load failed");
				status = ST_INVALID;
			} else {
				status = ST_MODEL;
			}			
			f.close();
			strcpy(curModel, tmpModel);

			curLOD = 0;

			// Refresh only if just loaded model is what is currently wanted
			if(!_strcmpi(newModel, tmpModel)) {
				refresh();
				if(status == ST_MODEL) {
					PostMessage(win, WM_USER_MODELLOADED, true, NULL);

					ObjectData *obj = mdl.Level(curLOD);
					hideSelections(obj, hiddenSelections, doHideProxies);
					
					d3dr.texPrepareFlush();
					d3dr.loadProxies(*obj);
					d3dr.createDataVL(*obj, false, false);
					status = ST_MODEL_VX;
					modelSize = d3dr.getModelSize();
					modelHeight = d3dr.getModelHeight();
					refresh();

					d3dr.createDataTX(*obj, true, false);
					d3dr.texFlushUnused();
					status = ST_MODEL_TX;refresh();
				}
			}
		}

		if(curLOD != wantLOD) {
			// Want to change LOD level
			if(status != ST_INVALID) {
				status = ST_MODEL;
				ObjectData *obj = mdl.Level(wantLOD);
				hideSelections(obj, hiddenSelections, doHideProxies);
				
				d3dr.texPrepareFlush();
				d3dr.loadProxies(*obj);
				d3dr.createDataVL(*obj, false, false);
				status = ST_MODEL_VX;
				d3dr.createDataTX(*obj, true, false);
				d3dr.texFlushUnused();
				status = ST_MODEL_TX;				
				PostMessage(win, WM_USER_LODLOADED, false, NULL);
			}
			curLOD = wantLOD;
			refresh();
		}
	}
	RptF("mlodloader::worker end");
}

void mlodloader::setHiddenSelections(vector<string> tohide, bool wantHideProxies) {
	hiddenSelections = tohide;
	doHideProxies = wantHideProxies;
}

// Hides unwanted selections such as proxies
void mlodloader::hideSelections(ObjectData *obj, vector<string> hlist, bool hideProxies) {
	// Unhide everything
	obj->UseHidden();
	obj->UnhideSelection();

	const static char *PROXYPFX = "proxy:";
	int i = 0;
	const char *sname;
	while(sname = obj->NamedSel(i)) {
		// Hide proxies
		if(hideProxies && !strncmp(sname, PROXYPFX, strlen(PROXYPFX))) {
			obj->UseNamedSel(sname);
			obj->HideSelection();
		}

		// Hide by name
		for(DWORD o=0;o<hlist.size();o++) {
			if(!stricmp(hlist[o].c_str(), sname)) {
				obj->UseNamedSel(sname);
				obj->HideSelection();
			}
		}
		i++;
	}

	obj->ClearSelection();	// Clear selection from loaded data, we dont want it displayed
};

void mlodloader::loadLOD(int id) {
	wantLOD = id;
	SetEvent(wgo);
}

bool mlodloader::loadModel(char *path, bool updateList) {
	wantLOD = 0;
	strncpy(newModel, path, PATHLEN);
	SetEvent(wgo);
	if(updateList)
		modelsCurID = updateModelListPath(path);
	return true;
}

void mlodloader::prevModel() {
	if(models.size() == 0)
		return;
	modelsCurID--;
	if(modelsCurID < 0)
		modelsCurID = 0;
	loadModel((char*)models[modelsCurID].c_str(), false);
	PostMessage(win, WM_USER_MODELLIST, false, NULL);
}

void mlodloader::nextModel() {
	if(models.size() == 0)
		return;
	modelsCurID++;
	if(modelsCurID >= (int)models.size())
		modelsCurID = (int)models.size()-1;
	loadModel((char*)models[modelsCurID].c_str(), false);
	PostMessage(win, WM_USER_MODELLIST, false, NULL);
}

void mlodloader::setModel(int idx) {
	if(models.size() == 0)
		return;
	modelsCurID = idx;
	loadModel((char*)models[modelsCurID].c_str(), false);
}

ObjectData* mlodloader::getLOD() {
	if(status != ST_INVALID && status != ST_LOADING)
		return mdl.Level(curLOD);
	else
		return NULL;
}

LODObject* mlodloader::getModel() {
	if(status != ST_INVALID && status != ST_LOADING)
		return &mdl;
	else
		return NULL;
}

void mlodloader::refresh() {
	InvalidateRect(rwin, NULL, FALSE);
	UpdateWindow(rwin);
}

void mlodloader::AdjustCamera(int dx, int dy, int dd) {
	const float s = 0.0075f;	// Sensitivity
	const float ds = 0.01f;
	camx += (float)dx * s;
	camy += (float)dy * s;
	camdist += (float)dd * ds;
	refresh();
}

void mlodloader::SetDrawStyle(int style) {
	drawstyle = style;
}

void mlodloader::render() {
	d3dr.initD3D(rwin, 4, true);

	RECT r;
	GetWindowRect(rwin, &r);
	d3dr.setRenderSize(r.right-r.left, r.bottom-r.top);

	if(camOrtho)
		d3dr.positionCameraOrtho(0, -modelHeight/3.0f, 0, camx, camy, 1000, 1.0f/modelSize/1.2f/camdist);
	else
		d3dr.positionCameraPerspective(0, -modelHeight/3.0f, 0, camx, camy, camdist * (modelSize*2), 1.04719755f);
	d3dr.positionLight();
	d3dr.clear(0xdddddddd);

	if(status != ST_INVALID && status != ST_LOADING) {
		bool nofaces = (getLOD()->NFaces()==0);		

		if((drawstyle==S_SOLID||drawstyle==S_SOLIDWIRE) && status == ST_MODEL_TX && !nofaces) {
			d3dr.renderDataProxies(D3DR_SOLID);
			d3dr.renderDataTX(D3DR_SOLID);
		}
		if((drawstyle==S_TEXTURE||drawstyle==S_TEXTUREWIRE) && status == ST_MODEL_TX && !nofaces) {
			d3dr.renderDataProxies(D3DR_TEXTURE);
			d3dr.renderDataTX(D3DR_TEXTURE);
		}
		if(drawstyle==S_WIRE||drawstyle==S_SOLIDWIRE||drawstyle==S_TEXTUREWIRE || status == ST_MODEL_VX || nofaces) {
			d3dr.renderDataProxies(D3DR_WIREFRAME+D3DR_VERTICES+D3DR_DEPTHTEST);
			d3dr.renderDataVL(D3DR_WIREFRAME+D3DR_VERTICES+D3DR_DEPTHTEST);
		}
	}

	if(drawGrid)
		d3dr.renderGrid();

	d3dr.presentWin(rwin);
}

// Given a path to mlod model (c:/foo/something.p3d), create list of all models in that dir
int mlodloader::updateModelListPath(char *mpath) {
	char path[512], spath[512];
	strncpy(path, mpath, 512);

	char *fn = strrchr(path, '\\');
	if(fn) *fn = 0x00;
	fn = strrchr(path, '/');
	if(fn) *fn = 0x00;
	snprintf(spath, 512, "%s\\*.p3d", path);

	HANDLE f;
	WIN32_FIND_DATA fd;
	f = FindFirstFile(spath, &fd);
	models.clear();
	int r = 0;
	do {
		snprintf(spath, 512, "%s\\%s", path, fd.cFileName);
		if(!_strcmpi(spath, mpath)) {
			r = (int)models.size();
		}
		models.push_back(spath);
	} while(FindNextFile(f, &fd));
	PostMessage(win, WM_USER_MODELLIST, false, NULL);
	return r;
}

void mlodloader::setTexturePath(char path[256], bool doFlush) {
	ObjectData *obj = mdl.Level(curLOD);

	d3dr.setTexturePath(path, doFlush);
	if(obj) {
		RptF("Rebuild model");
		d3dr.createDataTX(*obj, true, true);
		refresh();
	}
}

const char* mlodloader::GetModelName(int idx) {
	if(idx == -1)
		return newModel;
	else
		return models[idx].c_str();
}

const char* mlodloader::getLODname(float res) {
	static char name[512];

	if(ISMASS(res))
		return "Geometry";

#define LMAP(x,y) if(res == x) return #y;
	LMAP(LOD_MEMORY, Memory);
	LMAP(LOD_LANDCONTACT, Land Contact);
	LMAP(LOD_ROADWAY, Roadway);
	LMAP(LOD_PATHS, Paths);
	LMAP(LOD_HPOINTS, HitPoints);
	LMAP(LOD_VIEWGEO, View Geometry);
	LMAP(LOD_FIREGEO, Fire Geometry);
	LMAP(LOD_VIEW_CARGO_GEOMETRY, View Cargo Geometry);
	LMAP(LOD_VIEW_CARGO_FGEOMETRY, View Cargo Fire Geometry);
	LMAP(LOD_VIEW_COMMANDER, View Commander);
	LMAP(LOD_VIEW_COMM_GEOMETRY, View Commander Geometry);
	LMAP(LOD_VIEW_COMM_FGEOMETRY, View Commander Fire Geometry);
	LMAP(LOD_VIEW_PILOT_GEOMETRY, View Pilot Geometry);
	LMAP(LOD_VIEW_PILOT_FGEOMETRY, View Pilot Fire Geometry);
	LMAP(LOD_VIEW_GUNNER_GEOMETRY, View Gunner Geometry);
	LMAP(LOD_VIEW_GUNNER_FGEOMETRY, View Gunner Fire Geometry);
	LMAP(LOD_VIEW_GUNNER, View Gunner);
	LMAP(LOD_VIEW_PILOT, View Pilot);
	LMAP(LOD_VIEW_CARGO, View Cargo);

	if(res >= LOD_SHADOW_MIN && res <= LOD_SHADOW_MAX) {
		sprintf(name, "Shadow Volume: %.3f", res-LOD_SHADOW_MIN);
		return name;
	}

	// Normal LOD level
	sprintf(name, "%.3f", res);
	return name;
}

#define DTOR(x)	(x*0.0174532925f)

bool mlodloader::renderScreenshot(SHOTPARAM *p) {
	// Renders model screenshot	
	d3drenderer d3dr;
	HWND twin = createDummyWindow();	
	if(d3dr.initD3D(twin, p->aalevel, false) != 1) {
		printf("Direct3D Initialization failed\n");
		return false;
	}

	printf("Loading model <%s>\n", p->infile);
	ifstream f;
	LODObject mdll;
	f.open(p->infile, ios::in|ios::binary);
	if(mdl.Load(f, 0, 0) == -1) {
		printf("Model load failed\n");
		f.close();
		return false;
	}
	f.close();

	printf("Constructing model data\n");
	d3dr.setTexturePath(p->texpath);

/*
	//ObjectData *obj = mdl.Level(0);
	ObjectData *obj = NULL;
	if(p->lodlevel >= 0)
		obj = mdl.Level(mdl.FindLevel(p->lodlevel));
	else
		obj = mdl.Level(0);
*/
	//ObjectData *obj = mdl.Level(0);
	int level = 0;
	if(p->lodlevel >= 0)
		level = mdl.FindLevel(p->lodlevel);
	ObjectData *obj = mdl.Level(level);

	hideSelections(obj, hiddenSelections, doHideProxies);
	d3dr.texPrepareFlush();
	d3dr.setProxyPath(p->proxypath);
	d3dr.loadProxies(*obj);
	d3dr.createDataVL(*obj, false, false);	// This updates model size
	d3dr.createDataTX(*obj, true, false);
	d3dr.texFlushUnused();

	printf("Rendering model\n");
	d3dr.setRenderSize(p->width, p->height);
	
	if(p->ortho)
		d3dr.positionCameraOrtho(0, -d3dr.getModelHeight()/3.0f, 0, DTOR(p->camx), DTOR(p->camy), 1000, 1.0f/d3dr.getModelSize()/1.2f/p->camdist);
	else
		d3dr.positionCameraPerspective(0, -d3dr.getModelHeight()/3.0f, 0, DTOR(p->camx), DTOR(p->camy), p->camdist * (d3dr.getModelSize()*2), 1.04719755f);
	d3dr.positionLight();
	d3dr.clear(p->background);

	d3dr.renderDataProxies(D3DR_TEXTURE);
	d3dr.renderDataTX(D3DR_TEXTURE);
	if(!p->nogrid)
		d3dr.renderGrid();
	printf("Render done\n");

	printf("Saving result to <%s>\n", p->outfile);
	d3dr.saveModelScreenshot(p->outfile);
	printf("Save done\n");
	return true;
}

// Ugly Win32 crud below
void createWindow(int x, int y, int w, int h, HWND *outwin);
void __cdecl threadDummyWindow(void* param) {
	HWND *win = (HWND*) param;
	createWindow(0, 0, 100, 100, win);
}

// Creates an invisible window for Direct3D
HWND mlodloader::createDummyWindow(void) {
	volatile HWND dummyWindow = NULL;
	_beginthread(threadDummyWindow, 0, (void*)&dummyWindow);
	while(!dummyWindow)
		Sleep(1);
	return dummyWindow;
}

LRESULT WINAPI WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) { 
	return DefWindowProc(hWnd, uMsg, wParam, lParam); 
}

void createWindow(int x, int y, int w, int h, HWND *outwin) {
	static HINSTANCE hInstance = 0;
	WNDCLASS    wc;
	if(!hInstance) {
		hInstance = GetModuleHandle(NULL);
		wc.style         = CS_OWNDC;
		wc.lpfnWndProc   = (WNDPROC) WindowProc;
		wc.cbClsExtra    = 0;
		wc.cbWndExtra    = 0;
		wc.hInstance     = hInstance;
		wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO);
		wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);;
		wc.lpszMenuName  = NULL;
		wc.lpszClassName = "dummyWin";
		if(!RegisterClass(&wc)) {
			MessageBox(NULL, "RegisterClass() failed: Cannot register window class.", "Error", MB_OK);
			return;
		}
	}

    HWND hWnd = CreateWindowEx(NULL, "dummyWin", "window", WS_SYSMENU,
                               x, y, w, h, NULL, NULL, hInstance, NULL);
	if(!hWnd)
		RptF("CreateWindowEx failed");
	*outwin = hWnd;
	MSG msg;
	//while(GetMessage(&msg, hWnd, 0, 0)) {//  Put a message into MSG variable
	while(WaitMessage()) {
		if(PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}