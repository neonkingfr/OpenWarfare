// DiagramSimulator.h: interface for the CDiagramSimulator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIAGRAMSIMULATOR_H__DF8BC5BA_F0E0_4878_A2F3_54BB11381D45__INCLUDED_)
#define AFX_DIAGRAMSIMULATOR_H__DF8BC5BA_F0E0_4878_A2F3_54BB11381D45__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define CDS_ERR_OK 0
#define CDS_ERR_SYNTAXERROR -1
#define CDS_ERR_CANNOTGETVALUE -2
#define CDS_ERR_CANNOTSETVALUE -3
#define CDS_ERR_UNKNOWNFUNCTION -4
#define CDS_ERR_INVALIDCOUNTOPERANDS -5

#define CDS_PIN_ERROR -1
#define CDS_PIN_THIS 0
#define CDS_PIN_INPUT_MIN 1
#define CDS_PIN_INPUT_MAX 99
#define CDS_PIN_OUTPUT_MIN 100
#define CDS_PIN_OUTPUT_MAX 199

#define CDS_ISPIN_THIS(index) (index==CDS_PIN_THIS)
#define CDS_ISPIN_INPUT(index) (index>=CDS_PIN_INPUT_MIN && index<=CDS_PIN_INPUT_MAX)
#define CDS_ISPIN_OUTPUT(index) (index>=CDS_PIN_OUTPUT_MIN && index<=CDS_PIN_OUTPUT_MAX)

#define CDS_MAXLOCALVARIABLES 256

struct SSimulationEvent
  {
  double time;
  int index;
  double value;
  bool ispin;
  };

TypeIsSimple(SSimulationEvent);



class CDiagramSimulator  
{
	double _localVariables[CDS_MAXLOCALVARIABLES];
	AutoArray<SSimulationEvent> _events;
	double _curtime;
public:
	double GetNAN();
	bool GetAnyVariableValue(const char *ptr, double &value, int chars, int *read);
	CDiagramSimulator();
	virtual ~CDiagramSimulator();

	virtual double QueryValue(int index, bool ispin)=0;
	virtual bool SetValue(int index, bool ispin, double value)=0;
	virtual bool SetError(int errnumb, int errdata)=0;
	virtual double QueryFunction(const char *name, double *operands, int count) 
	  {this->SetError(CDS_ERR_UNKNOWNFUNCTION,0);return GetNAN();}
  

	const char *IsExpression(const char *text);
	bool IsAffectedBy(const char *expression, int index);

	static void IndexToVariable(int index, char *buff); //reserve min 50 chars;
	static int VariableToIndex(const char *buff, int chars=-1, int *read=NULL); //chars ==-1 will find terminating zero

	static void IndexToPin(int index, char *buff); //reserver min 50 chars;
	int PinToIndex(const char *buff, int chars=-1, int *read=NULL); //chars ==-1 will find terminating zero

	int LocalToIndex(const char *buff, int chars=-1, int *read=NULL); //chars ==-1 will find terminating zero

	void DoCalc(const char *expression, double &value, double &waittime);
	void Reset() {_events.Clear();_curtime=0;}
	double Step();
	void AddEvent(double waittime, int index,  bool ispin, double value);
	bool IsWaiting() {return _events.Size()>0;}
	double GetCurTime() {return _curtime;}

protected:
	static bool ParseNumber(const char *line, double &value, int *read=NULL);
	static char ParseSymbol(const char *line, int *read=NULL);
	static bool ParseFunctionName(const char *line, char *buff, int buffsz, int *read=NULL);

	double CalcE(const char **line);
	double CalcR(const char **line);  //relacni operatory
	double CalcLJ(const char **line); //logicke spojky	
	double CalcT(const char **line); //Total
	double CalcF(const char **line); //Factor
	double CalcV(const char **line); //Value
	double CalcFunction(const char **line, const char *functname);
	double GetWaitTime(const char **line);

  };

#endif // !defined(AFX_DIAGRAMSIMULATOR_H__DF8BC5BA_F0E0_4878_A2F3_54BB11381D45__INCLUDED_)
