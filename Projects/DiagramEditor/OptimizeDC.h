// OptimizeDC.h: interface for the COptimizeDC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OPTIMIZEDC_H__8434A99C_289F_43C5_8104_9E17EF53B96B__INCLUDED_)
#define AFX_OPTIMIZEDC_H__8434A99C_289F_43C5_8104_9E17EF53B96B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class COptimizeDC : public CDC  
{
  CFont curfont;  
public:
	CFont *SelectObject(CFont *font);
	void ReturnHandles();
	COptimizeDC();
	virtual ~COptimizeDC();

};

#endif // !defined(AFX_OPTIMIZEDC_H__8434A99C_289F_43C5_8104_9E17EF53B96B__INCLUDED_)
