// DlgBarFont.cpp : implementation file
//

#include "stdafx.h"
#include "DiagramEditor.h"
#include "DlgBarFont.h"
#include ".\dlgbarfont.h"
#include "MainFrm.h"



// DlgBarFont dialog

IMPLEMENT_DYNAMIC(DlgBarFont, CDialogBar)
DlgBarFont::DlgBarFont(CWnd* pParent /*=NULL*/)
	: CDialogBar()
{
vColor=0;
_enablechange=true;
}

DlgBarFont::~DlgBarFont()
{
}



BEGIN_MESSAGE_MAP(DlgBarFont, CDialogBar)
//{{AFX_MSG_MAP(DlgBarFont)
  ON_WM_DRAWITEM()
  ON_WM_MEASUREITEM()
  ON_BN_CLICKED(IDC_FONTBOLD, OnBnStyleClicked)
	ON_CBN_EDITCHANGE(IDC_FONTFACE, OnEditchangeFontface)
	ON_EN_CHANGE(IDC_FONTSIZE, OnChangeFontsize)
	ON_EN_CHANGE(IDC_WIDTH, OnChangeWidth)
	ON_EN_CHANGE(IDC_ANGLE, OnChangeAngle)
	ON_CBN_SELCHANGE(IDC_FONTFACE, OnSelchangeFontface)
	ON_BN_CLICKED(IDC_COLORBUTT, OnColorbutt)
	ON_BN_CLICKED(IDC_CALIGN, OnSetAlign)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FONTSIZESPIN, OnDeltaposFontsizespin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_ANGLESPIN, OnDeltaposAnglespin)
  ON_BN_CLICKED(IDC_FONTITALIC, OnBnStyleClicked)
  ON_BN_CLICKED(IDC_FONTUNDERLINE, OnBnStyleClicked)
  ON_BN_CLICKED(IDC_FONTSTRICKEOUT, OnBnStyleClicked)
	ON_BN_CLICKED(IDC_LALIGN, OnSetAlign)
	ON_BN_CLICKED(IDC_RALIGN, OnSetAlign)
	ON_NOTIFY(UDN_DELTAPOS, IDC_WIDTHSPIN, OnDeltaposWidthspin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// DlgBarFont message handlers

static int __stdcall EnumFontCB(const LOGFONT *lpelfe, const TEXTMETRIC *lpntme, 
  DWORD flags,LPARAM lParam)
  {
  HWND list=(HWND)lParam;
  SendMessage(list,CB_ADDSTRING,0,(LPARAM)lpelfe->lfFaceName);
  return 1;
  }


BOOL DlgBarFont::OnInitDialog(void)
  {
  int arridc[]=
    {IDC_FONTBOLD,IDI_BOLDFONT,
     IDC_FONTITALIC,IDI_ITALICFONT,
     IDC_FONTUNDERLINE,IDI_ULINEFONT,
     IDC_FONTSTRICKEOUT,IDI_STRICKFONT,
     IDC_LALIGN,IDI_LALIGN,
     IDC_CALIGN,IDI_CALIGN,
     IDC_RALIGN,IDI_RALIGN
    };
  HINSTANCE hInst=AfxGetInstanceHandle();
  CButton *b;
  for (int i=0;i<sizeof(arridc)/sizeof(arridc[0]);i+=2)
    {
    b=(CButton *)GetDlgItem(arridc[i]);
    ASSERT(b!=NULL);
    b->SetIcon(LoadIcon(hInst,MAKEINTRESOURCE(arridc[i+1])));
    }
  HDC dc=::GetDC(*this);
  LOGFONT fnt;
  fnt.lfCharSet=EASTEUROPE_CHARSET;
  fnt.lfFaceName[0]=0;
  fnt.lfPitchAndFamily=0;
  EnumFontFamiliesEx(dc,&fnt,EnumFontCB,(LPARAM)(GetDlgItem(IDC_FONTFACE)->GetSafeHwnd()),0);
  ::ReleaseDC(*this,dc);	

  //initialize values
  SendDlgItemMessage(IDC_FONTFACE,CB_SELECTSTRING,-1,(LPARAM)("arial"));
  CDataExchange exch(this,FALSE);
  int h=10;
  int ang=0;
  int w=0;
  DDX_Text(&exch,IDC_FONTSIZE,h);
  DDX_Text(&exch,IDC_ANGLE,ang);
  DDX_Text(&exch,IDC_WIDTH,w);

  return 0;  
  }

BOOL DlgBarFont::Create(CWnd * parent, UINT id)
  {
  BOOL ok=CDialogBar::Create(parent,IDD,CBRS_TOP|CBRS_TOOLTIPS|CBRS_FLYBY,id);
  if (!ok) return ok;
  EnableDocking(CBRS_ALIGN_TOP|CBRS_ALIGN_BOTTOM);
  OnInitDialog();
  return 0;
  }

void DlgBarFont::OnDrawFontFace(LPDRAWITEMSTRUCT lpDis)
  {
  if (lpDis<0) return;
  CDC dc;
  dc.Attach(lpDis->hDC);
  if (lpDis->itemState & ODS_SELECTED)
	{
	dc.SetBkColor(GetSysColor(COLOR_HIGHLIGHT));
	dc.SetTextColor(GetSysColor(COLOR_HIGHLIGHTTEXT));
	}
  else
	{
	dc.SetBkColor(GetGValue(vColor)<128?RGB(255,255,255):RGB(0,0,0));
	dc.SetTextColor(vColor);
	}
  LOGFONT lg;
  memset(&lg,0,sizeof(lg));
  ::SendMessage(lpDis->hwndItem,CB_GETLBTEXT,lpDis->itemID,(LPARAM)lg.lfFaceName);
  lg.lfHeight=16;
  lg.lfWeight=IsDlgButtonChecked(IDC_FONTBOLD)?FW_BOLD:FW_NORMAL;
  lg.lfItalic=IsDlgButtonChecked(IDC_FONTITALIC);
  lg.lfStrikeOut=IsDlgButtonChecked(IDC_FONTSTRIKEOUT);
  lg.lfUnderline=IsDlgButtonChecked(IDC_FONTUNDERLINE);
  lg.lfCharSet=EASTEUROPE_CHARSET;
  CFont fnt;
  fnt.CreateFontIndirect(&lg);
  CFont *old=dc.SelectObject(&fnt);
  dc.ExtTextOut(0,0,ETO_OPAQUE,&lpDis->rcItem,NULL,0,NULL);
  dc.DrawText(lg.lfFaceName,strlen(lg.lfFaceName),&(lpDis->rcItem),DT_VCENTER|DT_SINGLELINE|DT_LEFT|DT_NOPREFIX);  
  dc.SelectObject(old);
  dc.Detach();
  }

void DlgBarFont::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
  {
  switch (nIDCtl)
	{
	case IDC_FONTFACE: OnDrawFontFace(lpDrawItemStruct);break;
	case IDC_COLORBUTT: OnDrawColorButton(lpDrawItemStruct);break;
	}
  }

void DlgBarFont::OnDrawColorButton(LPDRAWITEMSTRUCT lpDis)
  {
  CDC dc;
  dc.Attach(lpDis->hDC);
  CRect rc=lpDis->rcItem;
  dc.DrawFrameControl(&rc, DFC_BUTTON, DFCS_BUTTONPUSH|((lpDis->itemState & 1)?DFCS_PUSHED:0));
  rc-=CRect(3,3,3,3);
  dc.FillSolidRect(&rc,vColor);
  dc.Detach();
  }

void DlgBarFont::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
  {
  CRect rc(0,0,14,9);
  MapDialogRect(*this,&rc);
  lpMeasureItemStruct->itemHeight=rc.bottom;
  CDialogBar::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
  }

void DlgBarFont::OnBnStyleClicked()
  {
  GetDlgItem(IDC_FONTFACE)->Invalidate();
  int idc=GetFocus()->GetDlgCtrlID();
  AfxGetMainWnd()->SendNotifyMessage(DBFM_BOOLSTYLECHANGED ,idc,IsDlgButtonChecked(idc));
  }

void DlgBarFont::UpdateFields(CDiagramView::ItemInfo * info)
  {
  _enablechange=false;
  CDataExchange exch(this,FALSE);
  int h=(int)info->ffHeight;
  int ang=info->lgFont.lfOrientation/10;
  int w=(int)(info->ffWidth*100/info->ffHeight);
  DDX_Text(&exch,IDC_FONTSIZE,h);
  DDX_Text(&exch,IDC_ANGLE,ang);
  DDX_Text(&exch,IDC_WIDTH,w);
  BOOL b=info->lgFont.lfWeight>500;
  DDX_Check(&exch,IDC_FONTBOLD,b);
  b=info->lgFont.lfItalic;
  DDX_Check(&exch,IDC_FONTITALIC,b);
  b=info->lgFont.lfUnderline;
  DDX_Check(&exch,IDC_FONTUNDERLINE,b);
  b=info->lgFont.lfStrikeOut;
  DDX_Check(&exch,IDC_FONTSTRICKEOUT,b);
  DDX_Radio(&exch,IDC_LALIGN,info->textAlign);
  vColor=info->fontColor;
  GetDlgItem(IDC_COLORBUTT)->Invalidate();
  if (info->lgFont.lfFaceName[0]==0) //no font selected, say it is Arial
  {
    strcpy(info->lgFont.lfFaceName,"Arial");
  }
  SendDlgItemMessage(IDC_FONTFACE,CB_SELECTSTRING,-1,(LPARAM)(info->lgFont.lfFaceName));
  _enablechange=true;
  }

void DlgBarFont::OnEditchangeFontface() 
  {  
  }

void DlgBarFont::OnChangeFontsize() 
  {
  if (_enablechange)
    AfxGetMainWnd()->SendNotifyMessage(DBFM_FONTSTYLECHANGED,2,(LPARAM)this);  	
  }

void DlgBarFont::OnChangeWidth() 
  {
  if (_enablechange)
    AfxGetMainWnd()->SendNotifyMessage(DBFM_FONTSTYLECHANGED,4,(LPARAM)this);  	
  }

void DlgBarFont::OnChangeAngle() 
  {
  if (_enablechange)
    AfxGetMainWnd()->SendNotifyMessage(DBFM_FONTSTYLECHANGED,8,(LPARAM)this);  	
  }

void DlgBarFont::OnSelchangeFontface() 
  {
  if (_enablechange)
	AfxGetMainWnd()->SendNotifyMessage(DBFM_FONTSTYLECHANGED,1,(LPARAM)this);  	

  }

void DlgBarFont::OnColorbutt() 
  {
  CMainFrame *frm=static_cast<CMainFrame *>(AfxGetMainWnd());
  frm->colorDialog.m_cc.rgbResult=vColor;
  if (frm->colorDialog.DoModal()==IDOK)
	{
	vColor=frm->colorDialog.GetColor();
	AfxGetMainWnd()->SendNotifyMessage(DBFM_FONTCOLORCHANGED,0,vColor);  	
	Invalidate();
	}
  }

void DlgBarFont::OnUpdateCmdUI(CFrameWnd *pTarget, BOOL bDisableIfNoHndler)
  {
  
  }

void DlgBarFont::OnSetAlign() 
  {
  int align;
  CDataExchange exch(this,TRUE);
  DDX_Radio(&exch,IDC_LALIGN,align);
  if (_enablechange)
  	AfxGetMainWnd()->SendNotifyMessage(DBFM_TEXTALIGNCHANGED,0,align);  	

	
  }

void DlgBarFont::DeltaPosSpin(int idc, int min, int max, int delta)
  {
  int pos=(int)GetDlgItemInt(idc,NULL,TRUE);
  pos-=delta;
  if (pos<min) pos=min;
  if (pos>max) pos=max;
  SetDlgItemInt(idc,pos,TRUE);
  }







void DlgBarFont::OnDeltaposFontsizespin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	DeltaPosSpin(IDC_FONTSIZE,1,999,pNMUpDown->iDelta);
		
	*pResult = 0;
}

void DlgBarFont::OnDeltaposAnglespin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	DeltaPosSpin(IDC_ANGLE,0,360,pNMUpDown->iDelta);
	
	*pResult = 0;
}

void DlgBarFont::OnDeltaposWidthspin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	DeltaPosSpin(IDC_WIDTH,0,999,pNMUpDown->iDelta);
	
	*pResult = 0;
}
