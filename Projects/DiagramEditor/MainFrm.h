// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__1130D198_2F08_4082_955E_EE1712935C39__INCLUDED_)
#define AFX_MAINFRM_H__1130D198_2F08_4082_955E_EE1712935C39__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DiagramView.h"
#include "DlgBarFont.h"
#include "dlgprinting.h"
#include "undo.h"

class ParamFile;

class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();

	CColorDialog colorDialog;
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  wStatusBar;
	CToolBar    wToolBar;
	CToolBar    wEditBar;
	CDiagramView  wGraph;
    DlgBarFont wFontBar;

    UndoRedo undo;
    bool undosaved;
    CString curfname;
    CString _basicWindowText;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnDiagramInsertnew();
	afx_msg void OnModeDesign();
	afx_msg void OnUpdateModeDesign(CCmdUI* pCmdUI);
	afx_msg void OnModeZoom();
	afx_msg void OnUpdateModeZoom(CCmdUI* pCmdUI);
	afx_msg void OnModeLinks();
	afx_msg void OnUpdateModeLinks(CCmdUI* pCmdUI);
	afx_msg void OnDiagramShapeBox();
	afx_msg void OnDiagramShapeDiamond();
	afx_msg void OnDiagramShapeEllipse();
	afx_msg void OnUpdateSelected(CCmdUI* pCmdUI);
	afx_msg void OnViewResetzoom();
	afx_msg void OnDiagramShapeParallelogramleft();
	afx_msg void OnDiagramShapeParallelogramright();
	afx_msg void OnDiagramShapeDoublesidedbox();
	afx_msg void OnDiagramLinesStyle();
	afx_msg void OnDiagramShapeRoundedbox();
	afx_msg void OnDiagramBackgroundColor();
	afx_msg void OnDiagramBackgroundTransparent();
	afx_msg void OnUpdateDiagramBackgroundTransparent(CCmdUI* pCmdUI);
	afx_msg void OnDiagramLinesColor();
	afx_msg void OnDiagramLinesTransparent();
	afx_msg void OnUpdateDiagramLinesTransparent(CCmdUI* pCmdUI);
	afx_msg void OnDiagramEdittext();
	afx_msg void OnUpdateDiagramInsertnew(CCmdUI* pCmdUI);
	afx_msg LPARAM OnNewItemDragged(WPARAM wParam,LPARAM lParam);
	afx_msg void OnEditDelete();
  afx_msg void OnSelectAll() {wGraph.SelectAllItems();}
  afx_msg void OnEditCut();
  afx_msg void OnEditCopy();
  afx_msg void OnEditPaste();
  afx_msg void OnUpdatePaste(CCmdUI* pCmdUI);
	afx_msg void OnEditDuplicate();
	afx_msg void OnEditGroup();
	afx_msg void OnUpdateEditGroup(CCmdUI* pCmdUI);
	afx_msg void OnEditMovefront();
	afx_msg void OnEditMoveback();
  afx_msg void OnFileSaveemf();
  afx_msg void OnFileSaveemfAs();
    afx_msg LPARAM OnSaveUndo(WPARAM,LPARAM);
    afx_msg LPARAM OnNotifyDoubleclick(WPARAM,LPARAM);
    afx_msg LPARAM OnNotifyEndSel(WPARAM,LPARAM);
    afx_msg void OnFilePagesetup();
    afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
    afx_msg void OnPopup1Linkcolor();
    afx_msg void OnFileNew();
    afx_msg void OnPopup1Uselastcolor();
    afx_msg void OnDiagramFontinselection();
    afx_msg void OnFileSaveas();
    afx_msg void OnFileSave();
    afx_msg void OnFileOpen();
    afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
    afx_msg void OnEditUndo();
    afx_msg void OnEditRedo();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LPARAM OnBoolStyleChanged(WPARAM wParam,LPARAM lParam);
    afx_msg LPARAM OnFontChanged(WPARAM wParam,LPARAM lParam);
    afx_msg LPARAM OnFontColorChanged(WPARAM wParam,LPARAM lParam);
	afx_msg LPARAM OnTextAlignChanged(WPARAM wParam,LPARAM lParam);
	afx_msg void OnKillfocusTextinput();
	afx_msg void OnViewShowvarianles();
	afx_msg void OnUpdateViewShowvarianles(CCmdUI* pCmdUI);
	afx_msg void OnPopup1Arrowtype();
	afx_msg void OnPopup1Pinid();
	afx_msg void OnSimulationStep();
	afx_msg void OnUpdateSimulationStep(CCmdUI* pCmdUI);
	afx_msg void OnSimulationRewind();
	afx_msg void OnSimulationRecalculateall();
	afx_msg void OnSimulationStepover();
	afx_msg void OnUpdateSimulationStepover(CCmdUI* pCmdUI);
    afx_msg LRESULT OnInitialUpdate(WPARAM,LPARAM);
    afx_msg LRESULT OnSerializeWindow(WPARAM,LPARAM larchive);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	bool CallDiagramEditText(void);
public:
  bool OnAskExit(void);
  void OnExit(void);
  afx_msg void OnClose();
  afx_msg BOOL OnQueryEndSession();
  afx_msg void OnEndSession(BOOL bEnding);
  afx_msg void OnFilePrint();
  DlgPrinting wPrinting;
  afx_msg void OnDiagramInactive();
  afx_msg void OnUpdateDiagramInactive(CCmdUI *pCmdUI);
  void SaveToEmf(CString pathName);
  void UpdateCaption();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__1130D198_2F08_4082_955E_EE1712935C39__INCLUDED_)
