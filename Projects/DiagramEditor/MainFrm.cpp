// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "DiagramEditor.h"
#include "..\Bredy.Libs\archive\ArchiveParamFile.h"

#include "MainFrm.h"
#include "DlgLineStyle.h"
#include "DlgText.h"
#include "OptimizeDC.h"
#include ".\mainfrm.h"
#include "DlgPageSetup.h"
#include <el/paramfile/paramfile.hpp>
#include "DlgPinID.h"
#include "DlgLinkProperties.h"
#include <afxpriv.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define BARS "BARS"

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
  //{{AFX_MSG_MAP(CMainFrame)
  ON_WM_CREATE()
  ON_WM_SETFOCUS()
  ON_COMMAND(ID_DIAGRAM_INSERTNEW, OnDiagramInsertnew)
  ON_COMMAND(ID_MODE_DESIGN, OnModeDesign)
  ON_UPDATE_COMMAND_UI(ID_MODE_DESIGN, OnUpdateModeDesign)
  ON_COMMAND(ID_MODE_ZOOM, OnModeZoom)
  ON_UPDATE_COMMAND_UI(ID_MODE_ZOOM, OnUpdateModeZoom)
  ON_COMMAND(ID_MODE_LINKS, OnModeLinks)
  ON_UPDATE_COMMAND_UI(ID_MODE_LINKS, OnUpdateModeLinks)
  ON_COMMAND(ID_DIAGRAM_SHAPE_BOX, OnDiagramShapeBox)
  ON_COMMAND(ID_DIAGRAM_SHAPE_DIAMOND, OnDiagramShapeDiamond)
  ON_COMMAND(ID_DIAGRAM_SHAPE_ELLIPSE, OnDiagramShapeEllipse)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_BOX, OnUpdateSelected)
  ON_COMMAND(ID_VIEW_RESETZOOM, OnViewResetzoom)
  ON_COMMAND(ID_DIAGRAM_SHAPE_PARALLELOGRAMLEFT, OnDiagramShapeParallelogramleft)
  ON_COMMAND(ID_DIAGRAM_SHAPE_PARALLELOGRAMRIGHT, OnDiagramShapeParallelogramright)
  ON_COMMAND(ID_DIAGRAM_SHAPE_DOUBLESIDEDBOX, OnDiagramShapeDoublesidedbox)
  ON_COMMAND(ID_DIAGRAM_LINES_STYLE, OnDiagramLinesStyle)
  ON_COMMAND(ID_DIAGRAM_SHAPE_ROUNDEDBOX, OnDiagramShapeRoundedbox)
  ON_COMMAND(ID_DIAGRAM_BACKGROUND_COLOR, OnDiagramBackgroundColor)
  ON_COMMAND(ID_DIAGRAM_BACKGROUND_TRANSPARENT, OnDiagramBackgroundTransparent)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_BACKGROUND_TRANSPARENT, OnUpdateDiagramBackgroundTransparent)
  ON_COMMAND(ID_DIAGRAM_LINES_COLOR, OnDiagramLinesColor)
  ON_COMMAND(ID_DIAGRAM_LINES_TRANSPARENT, OnDiagramLinesTransparent)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_LINES_TRANSPARENT, OnUpdateDiagramLinesTransparent)
  ON_COMMAND(ID_DIAGRAM_EDITTEXT, OnDiagramEdittext)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_INSERTNEW, OnUpdateDiagramInsertnew)
  ON_MESSAGE(MSG_NEWITEM,OnNewItemDragged)
  ON_COMMAND(ID_EDIT_DELETE, OnEditDelete)
  ON_COMMAND(ID_EDIT_CUT, OnEditCut)
  ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
  ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
  ON_COMMAND(ID_EDIT_SELECT_ALL, OnSelectAll)
  ON_COMMAND(ID_EDIT_DUPLICATE, OnEditDuplicate)
  ON_COMMAND(ID_EDIT_GROUP, OnEditGroup)
  ON_UPDATE_COMMAND_UI(ID_EDIT_GROUP, OnUpdateEditGroup)
  ON_COMMAND(ID_EDIT_MOVEFRONT, OnEditMovefront)
  ON_COMMAND(ID_EDIT_MOVEBACK, OnEditMoveback)
  ON_COMMAND(ID_FILE_SAVEEMF, OnFileSaveemf)
  ON_COMMAND(ID_FILE_SAVEEMF_AS, OnFileSaveemfAs)
  ON_MESSAGE(MSG_SAVEUNDO,OnSaveUndo)
  ON_MESSAGE(MSG_NOTIFYDBLCLICK,OnNotifyDoubleclick)
  ON_MESSAGE(MSG_NOTIFYENDSEL ,OnNotifyEndSel)
  ON_COMMAND(ID_FILE_PAGESETUP, OnFilePagesetup)
  ON_WM_CONTEXTMENU()
  ON_COMMAND(ID_POPUP1_LINKCOLOR, OnPopup1Linkcolor)
  ON_COMMAND(ID_FILE_NEW, OnFileNew)
  ON_COMMAND(ID_POPUP1_USELASTCOLOR, OnPopup1Uselastcolor)
  ON_COMMAND(ID_DIAGRAM_FONTINSELECTION, OnDiagramFontinselection)
  ON_COMMAND(ID_FILE_SAVEAS, OnFileSaveas)
  ON_COMMAND(ID_FILE_SAVE, OnFileSave)
  ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
  ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
  ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
  ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
  ON_WM_TIMER()
  ON_MESSAGE(DBFM_BOOLSTYLECHANGED,OnBoolStyleChanged)
  ON_MESSAGE(DBFM_FONTSTYLECHANGED,OnFontChanged)
  ON_MESSAGE(DBFM_FONTCOLORCHANGED,OnFontColorChanged)
  ON_MESSAGE(DBFM_TEXTALIGNCHANGED,OnTextAlignChanged)
  ON_COMMAND(ID_VIEW_SHOWVARIANLES, OnViewShowvarianles)
  ON_UPDATE_COMMAND_UI(ID_VIEW_SHOWVARIANLES, OnUpdateViewShowvarianles)
  ON_COMMAND(ID_POPUP1_ARROWTYPE, OnPopup1Arrowtype)
  ON_COMMAND(ID_POPUP1_PINID, OnPopup1Pinid)
  ON_COMMAND(ID_SIMULATION_STEP, OnSimulationStep)
  ON_UPDATE_COMMAND_UI(ID_SIMULATION_STEP, OnUpdateSimulationStep)
  ON_COMMAND(ID_SIMULATION_REWIND, OnSimulationRewind)
  ON_COMMAND(ID_SIMULATION_RECALCULATEALL, OnSimulationRecalculateall)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_DIAMOND, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_ELLIPSE, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_PARALLELOGRAMRIGHT, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_PARALLELOGRAMLEFT, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_DOUBLESIDEDBOX, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_ROUNDEDBOX, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_EDITTEXT, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_EDIT_DELETE, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdatePaste)
  ON_UPDATE_COMMAND_UI(ID_EDIT_DUPLICATE, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_EDIT_MOVEFRONT, OnUpdateSelected)
  ON_UPDATE_COMMAND_UI(ID_EDIT_MOVEBACK, OnUpdateSelected)
  ON_COMMAND_EX(ID_VIEW_DIAGRAMTOOLBAR, OnBarCheck)
  ON_COMMAND_EX(ID_VIEW_TEXTPROPERTIES, OnBarCheck)
  ON_UPDATE_COMMAND_UI(ID_VIEW_DIAGRAMTOOLBAR, OnUpdateControlBarMenu)
  ON_UPDATE_COMMAND_UI(ID_VIEW_TEXTPROPERTIES, OnUpdateControlBarMenu)
  ON_COMMAND(ID_SIMULATION_STEPOVER, OnSimulationStepover)
  ON_UPDATE_COMMAND_UI(ID_SIMULATION_STEPOVER, OnUpdateSimulationStepover)
  ON_MESSAGE(DLGEDIT_STARTUP,OnInitialUpdate)
  ON_MESSAGE(MSG_SERIALIZEWINDOW,OnSerializeWindow)
  //}}AFX_MSG_MAP

  ON_WM_CLOSE()
  ON_WM_QUERYENDSESSION()
  ON_WM_ENDSESSION()
  ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
  ON_COMMAND(ID_DIAGRAM_INACTIVE, OnDiagramInactive)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_INACTIVE, OnUpdateDiagramInactive)
END_MESSAGE_MAP()

static UINT indicators[] =
{
  ID_SEPARATOR,           // status line indicator
    ID_INDICATOR_CAPS,
    ID_INDICATOR_NUM,
    ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame():
  colorDialog(0,CC_FULLOPEN|CC_RGBINIT|CC_ANYCOLOR)
    , wPrinting(0), undo(1000)
{
  undosaved=false;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
    return -1;
  // create a view to occupy the client area of the frame
  if (!wGraph.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
    CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
  {
    TRACE0("Failed to create view window\n");
    return -1;
  }
  GetWindowText(_basicWindowText); //fill it

  if (!wToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
    | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
    !wToolBar.LoadToolBar(IDR_MAINFRAME))
  {
    TRACE0("Failed to create toolbar\n");
    return -1;      // fail to create
  }
  if (!wEditBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
    | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC,CRect(0,0,0,0),ID_VIEW_DIAGRAMTOOLBAR) ||
    !wEditBar.LoadToolBar(IDR_EDITTOOL))
  {
    TRACE0("Failed to create toolbar\n");
    return -1;      // fail to create
  }

  if (!wStatusBar.Create(this) ||
    !wStatusBar.SetIndicators(indicators,
    sizeof(indicators)/sizeof(UINT)))
  {
    TRACE0("Failed to create status bar\n");
    return -1;      // fail to create
  }

  wFontBar.Create(this,ID_VIEW_TEXTPROPERTIES);


  //  be dockable
  wToolBar.EnableDocking(CBRS_ALIGN_ANY);
  wEditBar.EnableDocking(CBRS_ALIGN_ANY);
  EnableDocking(CBRS_ALIGN_ANY);
  DockControlBar(&wToolBar,AFX_IDW_DOCKBAR_TOP);
  DockControlBar(&wEditBar,AFX_IDW_DOCKBAR_LEFT);
  DockControlBar(&wFontBar,AFX_IDW_DOCKBAR_TOP);
  wGraph.Mode(SGRM_DESIGN);
  LoadBarState(BARS);
  SetClassLong(*this,GCL_HICON,
    (LONG)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_MAINFRAME),IMAGE_ICON,32,32,0));
  SetClassLong(*this,GCL_HICONSM,
    (LONG)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_MAINFRAME),IMAGE_ICON,16,16,0));
  return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
  if( !CFrameWnd::PreCreateWindow(cs) )
    return FALSE;

  //  the CREATESTRUCT cs

  cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
  cs.lpszClass = AfxRegisterWndClass(0);
  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
  CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
  CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
  // forward focus to the view window
  wGraph.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
  // let the view have first crack at the command
  if (wGraph.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
    return TRUE;

  // otherwise, do default handling
  return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnDiagramInsertnew() 
{
  wGraph.Mode(SGRM_NEWITEM);
}

void CMainFrame::OnModeDesign() 
{
  wGraph.Mode(SGRM_DESIGN);
}

void CMainFrame::OnUpdateModeDesign(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.Mode(SGRM_GETCURRENT)==SGRM_DESIGN);
}

void CMainFrame::OnModeZoom() 
{
  wGraph.Mode(SGRM_ZOOM);
}

void CMainFrame::OnUpdateModeZoom(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.Mode(SGRM_GETCURRENT)==SGRM_ZOOM);
}

void CMainFrame::OnModeLinks() 
{
  wGraph.Mode(SGRM_NORMAL);
}

void CMainFrame::OnUpdateModeLinks(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.Mode(SGRM_GETCURRENT)==SGRM_NORMAL);
}

void CMainFrame::OnDiagramShapeBox() 
{
  wGraph.ChangeShape(CDiagramView::stBox);

}

void CMainFrame::OnDiagramShapeDiamond() 
{
  wGraph.ChangeShape(CDiagramView::stDiamond);

}

void CMainFrame::OnDiagramShapeEllipse() 
{
  wGraph.ChangeShape(CDiagramView::stEllipse);

}

void CMainFrame::OnUpdateSelected(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(wGraph.EnumItems(-1,true)!=-1);
}

void CMainFrame::OnViewResetzoom() 
{
  SFloatRect rc=wGraph.GetBoundingBox();
  float xs=(rc.right-rc.left)*0.2f;
  float ys=(rc.bottom-rc.top)*0.2f;
  rc.left-=xs;
  rc.top-=ys;
  rc.right+=xs;
  rc.bottom+=ys;
  wGraph.SetPanZoom(rc,true);
  wGraph.Update();
}

void CMainFrame::OnDiagramShapeParallelogramleft() 
{
  wGraph.ChangeShape(CDiagramView::stParallelogramL);

}

void CMainFrame::OnDiagramShapeParallelogramright() 
{
  wGraph.ChangeShape(CDiagramView::stParallelogramR);

}

void CMainFrame::OnDiagramShapeDoublesidedbox() 
{

}

void CMainFrame::OnDiagramLinesStyle() 
{
  DlgLineStyle dlg;
  CDiagramView::ItemInfo *nfo=wGraph.GetRefItem();
  dlg.vLineStyle=nfo->lStyle;
  dlg.vLineWidth=nfo->lWidth;
  if (dlg.DoModal())
  {
    wGraph.ChangeLineWidth(dlg.vLineWidth,dlg.vLineStyle);
  }  
}

void CMainFrame::OnDiagramShapeRoundedbox() 
{
  wGraph.ChangeShape(CDiagramView::stRoundedBox);	
}

void CMainFrame::OnDiagramBackgroundColor() 
{
  colorDialog.m_cc.rgbResult=wGraph.GetRefColor();
  if (colorDialog.DoModal())
  {
    wGraph.ChangeBkColor(colorDialog.GetColor());
  }
}

void CMainFrame::OnDiagramBackgroundTransparent() 
{
  wGraph.ChangeBkColor(TRANSPARENTCOLOR);
}

void CMainFrame::OnUpdateDiagramBackgroundTransparent(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.GetRefColor()==TRANSPARENTCOLOR);
}

void CMainFrame::OnDiagramLinesColor() 
{
  colorDialog.m_cc.rgbResult=wGraph.GetRefItem()->lineColor;
  if (colorDialog.DoModal())
  {
    wGraph.ChangeLineColor(colorDialog.GetColor());
  }
}

void CMainFrame::OnDiagramLinesTransparent() 
{
  wGraph.ChangeLineColor(TRANSPARENTCOLOR);	
}

void CMainFrame::OnUpdateDiagramLinesTransparent(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.GetRefItem()->lineColor==TRANSPARENTCOLOR);
}

bool CMainFrame::CallDiagramEditText(void)
{
  CDiagramView::ItemInfo *info=wGraph.GetRefItem();
  int align[3]=
  {DT_LEFT,DT_CENTER,DT_RIGHT};
  DlgText dlg;
  dlg.vText=info->text;
  dlg.vFont=info->lgFont;
  dlg.vColor=info->fontColor;
  for (dlg.vAlign=0;dlg.vAlign<3 && align[dlg.vAlign]!=info->textAlign;dlg.vAlign++);
  if (dlg.DoModal()==IDOK)
  {
    wGraph.SetTextContentAndStyle(dlg.vText,dlg.vFont,dlg.vColor,align[dlg.vAlign]);
    return true;
  }
  return false;
}

void CMainFrame::OnDiagramEdittext() 
{
  wGraph.BeginEditText();
}

void CMainFrame::OnUpdateDiagramInsertnew(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.Mode(SGRM_GETCURRENT)==SGRM_NEWITEM );	
}

LPARAM CMainFrame::OnNewItemDragged(WPARAM wParam,LPARAM lParam)
{
  for (int itm=-1;(itm=wGraph.EnumItems(itm,true))!=-1;) wGraph.SelectItem(itm,false);
  wGraph.SelectItem(wParam);
  /*  if (CallDiagramEditText()==false)
  wGraph.DeleteItem(wParam);*/
  wGraph.Update();
  wGraph.Mode(SGRM_DESIGN);
  return 1;
}

void CMainFrame::OnEditDelete() 
{
  wGraph.DeleteSelected();	
}
void CMainFrame::OnEditCut() 
{
  OnEditCopy();
  wGraph.DeleteSelected();	
}
void CMainFrame::OnEditCopy() 
{
  if (OpenClipboard()==FALSE) {AfxMessageBox("OpenClipboard failed");return;}
  if (EmptyClipboard()==FALSE) {AfxMessageBox("EmptyClipboard failed");CloseClipboard();return;}
  QOStrStream *newClipboardData = wGraph.GetDataForClipboard();
  int size = newClipboardData->pcount()+sizeof(int);
  HANDLE glob=GlobalAlloc(GMEM_MOVEABLE,size);
  int *ptr=(int *)GlobalLock(glob);
  *ptr = size;
  memcpy(ptr+1,newClipboardData->str(),newClipboardData->pcount());
  GlobalUnlock(glob);
  if (SetClipboardData(CF_DIAGRAM_EDITOR,glob)==0) AfxMessageBox("SetClipboardData failed");
  CloseClipboard();
  delete newClipboardData;
  LogF("***Clipboard copy: size=%d",size);
}
void CMainFrame::OnEditPaste() 
{
  if (OpenClipboard()==TRUE)
  {
    HANDLE glob=GetClipboardData(CF_DIAGRAM_EDITOR);
    if (glob)
    {
      wGraph.SaveUndo();
      wGraph.Deselect(); //for (id=-1;(id = wGraph.EnumItems(id,true)) != -1;)  ...
      int *data=(int *)GlobalLock(glob);
      QIStrStream in(data, *data);
      wGraph.PasteFromClipboard(in);
      GlobalUnlock(glob);
      CloseClipboard();
    }
  }
  wGraph.Mode(SGRM_DESIGN);
}
void CMainFrame::OnUpdatePaste(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(IsClipboardFormatAvailable(CF_DIAGRAM_EDITOR));
}

void CMainFrame::OnEditDuplicate() 
{
  wGraph.DuplicateSelected();	
}

void CMainFrame::OnEditGroup() 
{
  wGraph.GroupSelect(!wGraph.IsGroupSelect());
}

void CMainFrame::OnUpdateEditGroup(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(wGraph.EnumItems(-1,true)!=-1);
  pCmdUI->SetCheck(wGraph.IsGroupSelect());	
}

void CMainFrame::OnEditMovefront() 
{
  wGraph.OrderSelected(1);	
}

void CMainFrame::OnEditMoveback() 
{
  wGraph.OrderSelected(-1);  	
}

#include "El/Pathname/Pathname.h"
void CMainFrame::OnFileSaveemf() 
{
  if (curfname.GetLength())
  {
    Pathname path;
    path.SetPathName(curfname.GetBuffer());
    path.SetExtension("emf");
    CString emfFileName=path.GetFullPath();
    SaveToEmf(emfFileName);
  }
  else OnFileSaveemfAs();
}

void CMainFrame::SaveToEmf(CString pathName)
{
  CRect bound;

  if (!wGraph._pageClip)
  {
    SFloatRect frc=wGraph.GetBoundingBox();
    bound=wGraph.MapRectToWindow(frc);
    bound+=CRect(10,10,10,10);
  }
  else
  {
    bound=wGraph.GetClipRect();
  }
  CRect pxbound=bound;
  CDC *dc=GetDC();
  int iWidthMM = GetDeviceCaps(*dc, HORZSIZE); 
  int iHeightMM = GetDeviceCaps(*dc, VERTSIZE); 
  int iWidthPels = GetDeviceCaps(*dc, HORZRES); 
  int iHeightPels = GetDeviceCaps(*dc, VERTRES); 

  bound.left = (bound.left * iWidthMM * 100)/iWidthPels; 
  bound.top = (bound.top * iHeightMM * 100)/iHeightPels; 
  bound.right = (bound.right * iWidthMM * 100)/iWidthPels; 
  bound.bottom = (bound.bottom * iHeightMM * 100)/iHeightPels; 

  CDC metafile;
  metafile.Attach(CreateEnhMetaFile(*dc,pathName,&bound,NULL));
  ReleaseDC(dc);
  if (metafile.GetSafeHdc()==NULL)
  {
    AfxMessageBox(IDS_EXPORTFAILED);
    return;
  }
  wGraph.DrawExport(metafile,pxbound);
  HENHMETAFILE mf=CloseEnhMetaFile(metafile.Detach());
  DeleteEnhMetaFile(mf);
}

void CMainFrame::OnFileSaveemfAs() 
{
  CString filter,defext,title;
  filter.LoadString(IDS_EMFFILEFILTER);
  defext.LoadString(IDS_EMFDEFEXT);
  title.LoadString(IDS_EMFTITLE);
  CFileDialog dlg(FALSE,defext,NULL,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,filter);
  dlg.m_ofn.lpstrTitle=title;
  if (dlg.DoModal()==IDOK)
  {
    CString pathName = dlg.GetPathName();
    SaveToEmf(pathName);
  }
}

void CMainFrame::OnFilePagesetup()
{
  DlgPageSetup dlg;
  dlg.vColor=wGraph._pageColorVal;
  dlg.vForceColor=wGraph._pageColor;
  dlg.vImageX=wGraph._pageImageX;
  dlg.vImageY=wGraph._pageImageY;
  dlg.vForceDim=wGraph._pageClip;
  dlg.vEnableGrid=wGraph.GetGrid()!=0.0f;
  dlg.vGrid=dlg.vEnableGrid?wGraph.GetGrid():5.0f;
  dlg.vShowGrid=wGraph.IsGridVisible();
  if (dlg.DoModal()==IDOK)
  {
    wGraph._pageColorVal=dlg.vColor;
    wGraph._pageColor=dlg.vForceColor!=FALSE;
    wGraph._pageImageX=dlg.vImageX;
    wGraph._pageImageY=dlg.vImageY;
    wGraph._pageClip=dlg.vForceDim!=FALSE;
    wGraph.SetGrid(dlg.vEnableGrid?dlg.vGrid:0,dlg.vShowGrid!=FALSE);
    wGraph.Update();
  }
}

void CMainFrame::OnContextMenu(CWnd* pWnd, CPoint pt)
{
  CPoint clpt=pt;
  wGraph.ScreenToClient(&clpt);
  if (wGraph._curlink!=-1 && wGraph.MouseAtLink(wGraph._curlink,clpt))
  {
    CMenu mnu;
    mnu.LoadMenu(IDR_POPUP);
    CMenu *sub;
    sub=mnu.GetSubMenu(0);
    sub->TrackPopupMenu(TPM_RIGHTBUTTON,pt.x,pt.y,this,NULL);
  }
  else
  {
    float x,y;
    wGraph.MapPointFromWindow(clpt,&x,&y);
    int i=wGraph.ItemFromPoint(x,y);
    if (i!=-1) 
    {
      if (!(GetKeyState(VK_CONTROL) & 0x8000)) wGraph.SelectItems(SFloatRect(0,0,0,0),true);
      wGraph.SelectItem(i);
      wGraph.Update();
    }
    CMenu mnu;
    mnu.LoadMenu(IDR_MAINFRAME);
    CMenu *sub=mnu.GetSubMenu(3);
    wGraph.SaveEditedText();
    sub->TrackPopupMenu(TPM_RIGHTBUTTON,pt.x,pt.y,this,NULL);    
  }

}

void CMainFrame::OnPopup1Linkcolor()  
{  
  if (wGraph._curlink!=-1)
  {
    colorDialog.m_cc.rgbResult=wGraph.GetLink(wGraph._curlink).color;
    if (colorDialog.DoModal()==IDOK)
    {
      wGraph.SetLinkColor(wGraph._curlink,colorDialog.GetColor());
      wGraph.Update();
    }
  }
}

void CMainFrame::OnFileNew()
{
  curfname=CString("");
  if (AfxMessageBox(IDS_NEWASK,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDYES)
  {
    wGraph.ResetContent();
    wGraph.Update();
    UpdateCaption();
  }
}

void CMainFrame::OnPopup1Uselastcolor()
{
  if (wGraph._curlink!=-1)  
  {
    wGraph.SetLinkToLastColor(wGraph._curlink);  
    wGraph.Update();
  }
}

void CMainFrame::OnDiagramFontinselection()
{
  CDiagramView::ItemInfo *info=wGraph.GetRefItem();
  int align[3]=
  {DT_LEFT,DT_CENTER,DT_RIGHT};
  DlgText dlg;  
  LOGFONT reffont=info->lgFont;
  COLORREF refcolor=info->fontColor;
  dlg.vFont=info->lgFont;
  dlg.vColor=info->fontColor;
  dlg.vAlign=-1;
  dlg.notext=true;
  if (dlg.DoModal()==IDOK)
  {
    wGraph.ChangeTextFontChanges(reffont,dlg.vFont,refcolor,dlg.vColor,dlg.vAlign==-1?dlg.vAlign:align[dlg.vAlign]);   
    wGraph.Update();
  }
}

#include "El/Pathname/Pathname.h"
void CMainFrame::UpdateCaption()
{
  Pathname path(curfname);
  CString fname = path.GetFilename();
  CString text = _basicWindowText + ": " + fname;
  SetWindowText(text);
}

void CMainFrame::OnFileSaveas()
{
  CString defext;defext.LoadString(IDS_DIAGRAMDEFEXT);
  CString filter;filter.LoadString(IDS_DIAGRAMFILTER);
  CString name;name.LoadString(IDS_SAVEDIAGRAMTITLE);
  CFileDialog fdlg(FALSE,defext,curfname,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,filter);
  fdlg.m_ofn.lpstrTitle=name;
  if (fdlg.DoModal()==IDOK)
  {
    curfname=fdlg.GetPathName();
    if (wGraph.Save(curfname)==false)
      AfxMessageBox(IDS_SAVEFAILED);
  }
  UpdateCaption();
}

void CMainFrame::OnFileSave()
{
  if (curfname.GetLength())
  {
    if (wGraph.Save(curfname)==false)
      AfxMessageBox(IDS_SAVEFAILED);
    UpdateCaption();
  }
  else OnFileSaveas();
}

void CMainFrame::OnFileOpen()
{
  CString defext;defext.LoadString(IDS_DIAGRAMDEFEXT);
  CString filter;filter.LoadString(IDS_DIAGRAMFILTER);
  CString name;name.LoadString(IDS_OPENDIAGRAMTITLE);
  CFileDialog fdlg(TRUE,defext,NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,filter);
  fdlg.m_ofn.lpstrTitle=name;
  if (fdlg.DoModal()==IDOK)
  {
    curfname=fdlg.GetPathName();
    if (wGraph.Load(curfname)==false)
      AfxMessageBox(IDS_LOADFAILED);
    wGraph.Update();
    wGraph.SetDirty(false);
    UpdateCaption();
  }
}

LPARAM CMainFrame::OnSaveUndo(WPARAM,LPARAM)
{
  wGraph.SetDirty(true);
  if (undosaved) return 0;
  ParamFile *undopf = new ParamFile();
  if (!undopf) return 0;
  ArchiveParamFile arch(undopf,true);
  wGraph.Streaming(arch, false);
  //SetTimer(888,4000,NULL);
  undo.SaveUndo(undopf);
  undosaved=false;
  TRACE0("SAVEUNDO OK\n");
  return 0;
}

LPARAM CMainFrame::OnNotifyDoubleclick(WPARAM,LPARAM)
{
  this->OnDiagramEdittext();
  return 1;
}

void CMainFrame::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
//  pCmdUI->Enable(undo!=NULL);
}

#pragma optimize ("g",off) //Problems with optimalization in MSVC 6.0 in following function frame

void CMainFrame::OnEditUndo()
{
  if (undo.RedoUninitialized()) 
  {
    OnSaveUndo(0,0);
    undo.Undo();
    undo.DecrementRedo();
  }
  ParamFile *undopf = undo.Undo();
  if (!undopf) return;
  wGraph.ResetContent();
  ArchiveParamFile arch(undopf,false);
  wGraph.Streaming(arch, false);
  wGraph.Update();
}

void CMainFrame::OnEditRedo()
{
  ParamFile *undopf = undo.Redo();
  if (!undopf) return;
  wGraph.ResetContent();
  ArchiveParamFile arch(undopf,false);  
  wGraph.Streaming(arch, false);
  wGraph.Update();
}

#pragma optimize ("",on)

void CMainFrame::OnTimer(UINT nIDEvent)
{
  if (nIDEvent==888 && GetForegroundWindow()==this)
  {
    undosaved=false;
    KillTimer(nIDEvent);
    TRACE0("SAVEUNDO ENABLED\n");
  }

  CFrameWnd::OnTimer(nIDEvent);
}

LPARAM CMainFrame::OnBoolStyleChanged(WPARAM wParam,LPARAM lParam)
{
  CDiagramView::ItemInfo *info=wGraph.GetRefItem();
  LOGFONT reffont=info->lgFont;
  LOGFONT newfont=reffont;
  switch (wParam)
  {
  case IDC_FONTBOLD: newfont.lfWeight=lParam?700:400;break;
  case IDC_FONTITALIC: newfont.lfItalic=lParam;break;
  case IDC_FONTSTRICKEOUT: newfont.lfStrikeOut=lParam;break;
  case IDC_FONTUNDERLINE: newfont.lfUnderline=lParam;break;
  }
  wGraph.ChangeTextFontChanges(reffont,newfont,0,0,-1);   
  wGraph.Update();        
  return 0;
}

LPARAM CMainFrame::OnNotifyEndSel(WPARAM,LPARAM)
{
  CDiagramView::ItemInfo *info=wGraph.GetRefItem();
  wFontBar.UpdateFields(info);
  return 0;
}

LPARAM CMainFrame::OnFontChanged(WPARAM wParam,LPARAM lParam)
{
  CDialog *dlg=(CDialog *)lParam;
  CComboBox *lsb=static_cast<CComboBox *>(dlg->GetDlgItem(IDC_FONTFACE));

  CString font;
  lsb->GetLBText(lsb->GetCurSel(),font);
  int h=dlg->GetDlgItemInt(IDC_FONTSIZE,NULL,FALSE);
  int w=dlg->GetDlgItemInt(IDC_WIDTH,NULL,FALSE);  
  int a=dlg->GetDlgItemInt(IDC_ANGLE,NULL,FALSE)*10;
  const char *fc=font;
  if (~wParam & 1) fc=NULL;
  if (~wParam & 2) h=-1;
  if (~wParam & 4) w=-1;
  if (~wParam & 8) a=-1;
  wGraph.ChangeFontFaceSize(fc,h,w,a);
  return 0;
}

LPARAM CMainFrame::OnFontColorChanged(WPARAM wParam,LPARAM lParam)
{
  wGraph.ChangeTextColor(lParam);
  return 0;
}

LPARAM CMainFrame::OnTextAlignChanged(WPARAM wParam,LPARAM lParam)
{
  wGraph.ChangeTextAlign(lParam);
  return 0;
}


void CMainFrame::OnKillfocusTextinput()
{
}

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg) 
{
  return CFrameWnd::PreTranslateMessage(pMsg);
}

bool CMainFrame::OnAskExit(void)
{
  if (wGraph.IsDirty()) 
  {
    int res=AfxMessageBox(IDS_ASKBEFOREEXIT,MB_YESNOCANCEL);
    if (res==IDYES)
    {
      this->OnFileSave();
      if (wGraph.IsDirty()) return false;
    }
    else if (res==IDCANCEL) return false;
  }
  return true;
}

void CMainFrame::OnExit(void)
{
  SaveBarState(BARS);
}

void CMainFrame::OnClose()
{
  if (OnAskExit()) 
  {
    OnExit();
    CFrameWnd::OnClose();
  }
}

BOOL CMainFrame::OnQueryEndSession()
{
  if (!CFrameWnd::OnQueryEndSession())
    return FALSE;

  if (!OnAskExit()) return FALSE;
  // TODO:  Add your specialized query end session code here

  return TRUE;
}

void CMainFrame::OnEndSession(BOOL bEnding)
{
  if (bEnding) OnExit();
  CFrameWnd::OnEndSession(bEnding);

  // TODO: Add your message handler code here
}

void CMainFrame::OnFilePrint()
{
  CRect bound;

  if (!wGraph._pageClip)
  {
    SFloatRect frc=wGraph.GetBoundingBox();
    bound=wGraph.MapRectToWindow(frc);
    bound+=CRect(10,10,10,10);
  }
  else
  {
    bound=wGraph.GetClipRect();
  }

  wPrinting.vDiagram=&wGraph;
  wPrinting.vClip=bound;
  wPrinting.DoModal();
  /*  CPrintDialog dlg(FALSE);
  if (dlg.DoModal()==IDOK)
  {
  HDC hdcPrinter = dlg.GetPrinterDC();
  if (hdcPrinter == NULL)
  {
  AfxMessageBox(IDS_NOPRINTERSELECTED);
  return;
  }
  // create a CDC and attach it to the default printer
  CDC dcPrinter;
  dcPrinter.Attach(hdcPrinter);
  // call StartDoc() to begin printing
  DOCINFO docinfo;
  memset(&docinfo, 0, sizeof(docinfo));
  docinfo.cbSize = sizeof(docinfo);
  char buff[256];
  LoadString(AfxGetInstanceHandle(),IDR_MAINFRAME,buff,sizeof(buff));
  docinfo.lpszDocName=buff;
  // if it fails, complain and exit gracefully
  if (dcPrinter.StartDoc(&docinfo) < 0)
  {
  AfxMessageBox(IDS_CANNOTINICIALIZEPRINTER);
  }
  else
  {
  // start a page
  if (dcPrinter.StartPage() < 0)
  {
  AfxMessageBox(_T("Could not start page"));
  dcPrinter.AbortDoc();
  }
  else
  {
  wGraph.Draw(dcPrinter,bound);
  dcPrinter.EndPage();
  dcPrinter.EndDoc();        
  }
  }
  }*/
}




void CMainFrame::OnViewShowvarianles() 
{
  wGraph.ShowVariables(!wGraph.IsVariablesShown());
}

void CMainFrame::OnUpdateViewShowvarianles(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.IsVariablesShown());
  // TODO: Add your command update UI handler code here

}

void CMainFrame::OnPopup1Arrowtype() 
{
  if (wGraph._curlink!=-1)
  {
    DlgLinkProperties dlg;
    SDLinkInfo *flags;
    flags=wGraph.GetLinkExtraData(wGraph._curlink);
    dlg.vUseArrows=flags!=NULL;
    if (flags)
    {
      dlg.vArrowSize=flags->arrowsize;
      dlg.vLineStyle=flags->linestyle;
      dlg.vLineWidth=flags->linewidth;
      dlg.vArrowType=flags->arrowtype;
      dlg.vUseBKColor=flags->usebkcolor;
    }
    if (dlg.DoModal()==IDOK)
    {
      if (dlg.vUseArrows)
      {      
        SDLinkInfo *sflags;
        if (flags) sflags=new SDLinkInfo(*flags);else sflags=new SDLinkInfo;
        sflags->arrowsize=dlg.vArrowSize;
        sflags->arrowtype=dlg.vArrowType;
        sflags->linestyle=dlg.vLineStyle;
        sflags->linewidth=dlg.vLineWidth;
        sflags->usebkcolor=dlg.vUseBKColor!=FALSE;
        wGraph.SetArrowType(wGraph._curlink,sflags);
      }
      else
        wGraph.SetArrowType(wGraph._curlink,NULL);
      wGraph.Update();
    }
  }  
}

void CMainFrame::OnPopup1Pinid() 
{
  DlgPinID dlg;
  if (wGraph._curlink!=-1)
  {
    SDLinkInfo *flags;
    flags=wGraph.GetLinkExtraData(wGraph._curlink);
    dlg.value=flags==NULL?0:flags->pinid-1;
    if (dlg.DoModal()==IDOK)
    {
      SDLinkInfo *nw=(flags==NULL)?(new SDLinkInfo):(new SDLinkInfo(*flags));
      nw->pinid=dlg.value+1;
      wGraph.SetArrowType(wGraph._curlink,nw);
      wGraph.RecalculateDiagram();
      wGraph.Update();
    }
  }
}

void CMainFrame::OnSimulationStep() 
{
  wGraph.SimulateStep(false);
  wGraph.Update();
}

void CMainFrame::OnUpdateSimulationStep(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(wGraph.IsWaiting());	
}

void CMainFrame::OnSimulationRewind() 
{
  wGraph.Reset();	
  wGraph.RecalculateDiagram();
  wGraph.Update();
}

void CMainFrame::OnSimulationRecalculateall() 
{
  wGraph.RecalculateDiagram();
  wGraph.Update();
}

void CMainFrame::OnSimulationStepover() 
{
  wGraph.SimulateStep(true);
  wGraph.Update();

}

void CMainFrame::OnUpdateSimulationStepover(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(wGraph.IsWaiting());	
  // TODO: Add your command update UI handler code here
}

LRESULT CMainFrame::OnInitialUpdate(WPARAM,LPARAM)
{
  if (__argc>1)
  {
    int idx=1;
    while (idx<__argc && __argv[idx][0]=='-') idx++;
    if (idx<__argc)
    {
      curfname=__argv[idx];
      if (wGraph.Load(curfname)==false)
        AfxMessageBox(IDS_LOADFAILED);
      wGraph.Update();
      wGraph.SetDirty(false);
      UpdateCaption();
    }
  }
  return 0;
}

LRESULT CMainFrame::OnSerializeWindow(WPARAM,LPARAM larchive)
{
  IArchive &arch=*(IArchive *)larchive;
  WINDOWPLACEMENT wp;
  wp.length=sizeof(wp);
  GetWindowPlacement(&wp);
  arch("Flags",wp.flags);
  arch("MaxPosX",wp.ptMaxPosition.x);
  arch("MaxPosY",wp.ptMaxPosition.y);
  arch("MinPosX",wp.ptMinPosition.x);
  arch("MinPosY",wp.ptMinPosition.y);
  arch("Left",wp.rcNormalPosition.left);
  arch("Top",wp.rcNormalPosition.top);
  arch("Right",wp.rcNormalPosition.right);
  arch("Bottom",wp.rcNormalPosition.bottom);
  arch("ShowCmd",wp.showCmd);
  if (-arch) 
  {    
    SetWindowPlacement(&wp);
    HMONITOR hmon=MonitorFromWindow(*this,MONITOR_DEFAULTTONULL);
    if (hmon==NULL) SetWindowPos(NULL,0,0,0,0,SWP_NOZORDER|SWP_NOSIZE);
  }
  return 0;
}


static bool HasInactiveItems(CDiagramView &wGraph)
{  
  for (int i=-1;(i=wGraph.EnumItems(i,true))!=-1;)
  {
    DWORD flags=wGraph.GetItemFlags(i);
    if ((flags & LINKACTIVE) != LINKACTIVE) return true;
  }
  return false; 
}

void CMainFrame::OnDiagramInactive()
{
  bool ina=HasInactiveItems(wGraph);
  for (int i=-1;(i=wGraph.EnumItems(i,true))!=-1;)
  {
    wGraph.SetItemFlags(i,ina?LINKACTIVE:0,LINKACTIVE);
  }
}

void CMainFrame::OnUpdateDiagramInactive(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(HasInactiveItems(wGraph));
  pCmdUI->Enable(wGraph.EnumItems(-1,true)!=-1);
}
