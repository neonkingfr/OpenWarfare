// DlgPinID.cpp : implementation file
//

#include "stdafx.h"
#include "diagrameditor.h"
#include "DlgPinID.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPinID dialog


DlgPinID::DlgPinID(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPinID::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPinID)
	//}}AFX_DATA_INIT
}


void DlgPinID::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPinID)
	DDX_Control(pDX, IDC_LIST1, wID);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPinID, CDialog)
	//{{AFX_MSG_MAP(DlgPinID)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPinID message handlers

BOOL DlgPinID::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	for (int i='A'; i<='Z';i++)
	  {
	  char d[10];
	  sprintf(d,"%c -> %c",i,tolower(i));
	  wID.AddString(d);
	  }
	wID.SetCurSel(value);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgPinID::OnOK() 
  {	
  value=wID.GetCurSel();
  CDialog::OnOK();
  }
