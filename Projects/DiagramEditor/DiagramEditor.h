// DiagramEditor.h : main header file for the DIAGRAMEDITOR application
//

#if !defined(AFX_DIAGRAMEDITOR_H__B02921C1_3417_480D_970A_07660F4437FE__INCLUDED_)
#define AFX_DIAGRAMEDITOR_H__B02921C1_3417_480D_970A_07660F4437FE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#define CF_DIAGRAM_EDITOR (CF_PRIVATEFIRST+0x3a)
/////////////////////////////////////////////////////////////////////////////
// CDiagramEditorApp:
// See DiagramEditor.cpp for the implementation of this class
//



class CDiagramEditorApp : public CWinApp
{
public:
	CDiagramEditorApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDiagramEditorApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	//{{AFX_MSG(CDiagramEditorApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIAGRAMEDITOR_H__B02921C1_3417_480D_970A_07660F4437FE__INCLUDED_)
