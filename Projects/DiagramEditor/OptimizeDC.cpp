// OptimizeDC.cpp: implementation of the COptimizeDC class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DiagramEditor.h"
#include "OptimizeDC.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COptimizeDC::COptimizeDC()
{
}

COptimizeDC::~COptimizeDC()
  {
  ReturnHandles();
  }

void COptimizeDC::ReturnHandles()
  {
  SelectStockObject(DEVICE_DEFAULT_FONT);
  }

CFont *COptimizeDC::SelectObject(CFont *font)
  {
  if (font==NULL) return &curfont;
  CFont *cur=GetCurrentFont();
  if (cur->GetSafeHandle()==GetStockObject(DEVICE_DEFAULT_FONT))
	CDC::SelectObject(&curfont);
  LOGFONT f1,f2;
  cur->GetLogFont(&f1);
  font->GetLogFont(&f2);
  int fc1=strlen(f1.lfFaceName);
  int fc2=strlen(f2.lfFaceName);
  memset(f1.lfFaceName+fc1,0,sizeof(f1.lfFaceName)-fc1);
  memset(f2.lfFaceName+fc2,0,sizeof(f2.lfFaceName)-fc2);
  if (memcmp(&f1,&f2,sizeof(f1))==0) return &curfont;
  SelectStockObject(DEVICE_DEFAULT_FONT);
  HFONT fnt=(HFONT)curfont.Detach();
  curfont.CreateFontIndirect(&f2);
  CDC::SelectObject(&curfont);
  DeleteObject(fnt);
  return &curfont;
  }
