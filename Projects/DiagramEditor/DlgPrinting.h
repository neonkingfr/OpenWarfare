#pragma once
#include "afxwin.h"
#include "diagramview.h"


// DlgPrinting dialog

class CDiagramView;
class DlgPrinting : public CDialog
{
	DECLARE_DYNAMIC(DlgPrinting)

    int _xmax;
    int _ymax;
	int _fxmax;
	int _fymax;
	int _xdpi;
    int _ydpi;
    CDiagramView _toPrint;
    int _lineWeight;
    bool _change;
	

    float _drawAspect;

    CRect _zoomrect;

public:
	DlgPrinting(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgPrinting();

// Dialog Data
	//{{AFX_DATA(DlgPrinting)
	enum { IDD = IDD_PRINTING };
	CSpinButtonCtrl	wSpinZoom;
	CSpinButtonCtrl	wSpinTop;
	CSpinButtonCtrl	wSpinLineWeight;
	CSpinButtonCtrl	wSpinLeft;
    CEdit wLineWeight;
    CButton wPreview;
	UINT	vMarginBottom;
	UINT	vMarginLeft;
	UINT	vMarginRight;
	UINT	vMarginTop;
	UINT	vTop;
	UINT	vZoom;
	UINT	vLeft;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPrinting)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual void OnOK();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPrinting)
	virtual BOOL OnInitDialog();
    afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg void OnBnClickedSetupprinter();
    afx_msg void OnDestroy();
	afx_msg void OnChangeLeft();
	afx_msg void OnChangeTop();
	afx_msg void OnChangeZoom();
	afx_msg void OnChangeLineweight();
	afx_msg void OnChangeMargin();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

public:
	void GetPosInfo(int &left, int &top, int &zoom);
  CDiagramView *vDiagram;
  CRect vClip;
  CPrintDialog *wPrintDlg;
  void UpdatePageInfo(void);
  void UpdateScaleInfo();
  void CopyDiagram(void);
  SFloatRect CalcFakeScaleDiagram(const CRect & rect);
};
