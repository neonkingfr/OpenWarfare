// DiagramView.h: interface for the CDiagramView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIAGRAMVIEW_H__B67B7FB5_5A57_4692_95D6_09A7180E4C59__INCLUDED_)
#define AFX_DIAGRAMVIEW_H__B67B7FB5_5A57_4692_95D6_09A7180E4C59__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\Bredy.Libs\GraphView\GraphCtrl.h"
#include "PROJECTS\BREDY.LIBS\GRAPHVIEW\GraphCtrl.h"	// Added by ClassView
#include "DiagramSimulator.h"


#define LINKACTIVE (SGRI_CANSELFLINKED|SGRI_CANBELINKED|SGRI_CANMAKELINKS|SGRI_CANMOVE|SGRI_CANRESIZE)


class ParamFile;

#define TRANSPARENTCOLOR 0xFFFFFFFF

#define SGRM_NEWITEM 0x4
#define MSG_NEWITEM (WM_APP+10)
#define MSG_SAVEUNDO (WM_APP+11)
#define MSG_NOTIFYDBLCLICK (WM_APP+12)
#define MSG_NOTIFYENDSEL (WM_APP+13)
#define DLGEDIT_STARTUP (WM_APP+14)
#define MSG_SERIALIZEWINDOW (WM_APP+15)


#define MAXITERS 10000

struct SDLinkFlags1
  {
  unsigned int frozen:1;
  unsigned int custom:1;
  unsigned int dontfocus:1;
  unsigned int arrowtype:2;
  unsigned int arrowsize:3;
  unsigned int linestyle:4;
  unsigned int linewidth:4;
  };

struct SDLinkInfo
{
  unsigned char arrowtype;
  unsigned char arrowsize;
  unsigned char linestyle;
  unsigned char linewidth;
  bool usebkcolor;
  unsigned char pinid;

  SDLinkInfo():
    arrowtype(0),
    arrowsize(1),
    linestyle(0),
    linewidth(1),
    pinid(0),
    usebkcolor(false)
    {}
  SDLinkInfo(const SDLinkInfo& other) {memcpy(this,&other,sizeof(*this));}

  void Convert(SDLinkFlags1 &flgs, int pin)
  {
    arrowtype=flgs.arrowtype;
    linestyle=flgs.linestyle;
    linewidth=flgs.linewidth;
    arrowsize=flgs.arrowsize;
    pinid=pin;
  }

  void Serialize(IArchive &arch)
  {
    arch("ArrowType",arrowtype,(unsigned char)0);
    arch("ArrowSize",arrowsize,(unsigned char)1);
    arch("LineStyle",linestyle,(unsigned char)0);
    arch("LineWidth",linewidth,(unsigned char)1);
    arch("UseBKColor",usebkcolor,false);
    arch("Pin",pinid,(unsigned char)0);
  }
};

class QIStrStream;
class QOStrStream;
class CDiagramView : public CGraphCtrl , public CDiagramSimulator
{
  bool _newitemmode;
  CRect dragrc;
  int grpcnt;
  bool _grpselected;
  
  HPEN _oldpen;
  HFONT _oldfont;
  HBRUSH _oldbrush;
  
  bool _exportdraw;
  
  float _grid;
  bool _gridshow;
  bool _showVariables;

  CEdit	   wTextEdit;
  CFont	   wEditFont;
  CBrush   wEditBrush;
  
  CFont	   wVarFont;

  int _expressContext;	//expression context
  bool _expressChanged; //expression calculations changed
  bool _savingText;

  bool _dirty;

  
  CDC *optimizedDC;  
  public:
    bool _pageClip;
    bool _pageColor;
    COLORREF _pageColorVal;
    UINT _pageImageX;
    UINT _pageImageY;
    int _curlink;
    float _mouseHotSpotX, _mouseHotSpotY;
    enum ShapeType 
    {stBox, stEllipse, stDiamond, stParallelogramR, stParallelogramL,stBox2, stRoundedBox};
    
    struct ItemInfo
    {
      CString text;
      ShapeType shape;
      COLORREF lineColor;
      LOGFONT lgFont;
      COLORREF fontColor;
      int textAlign;
      int lWidth;
      int lStyle;
      int group;
	  bool recalc;
      float ffHeight;
      float ffWidth;
	  double value;
	  int lasterror;
	  int lasterrordata;
      ItemInfo();
      void FromDefault();
      void SaveAsDefault();
    };
    
    
  public:
    void ChangeShape(ShapeType shape);
    void NewItem();
    void DeleteItem(int id);
    virtual void DeleteLink(int &enm);
    ItemInfo * GetItemExtraData(int itemid);
    SDLinkInfo * GetLinkExtraData(int itemid);
    void ResetContent();
    virtual void OnLinkItems(int beginitem, int enditem);
    virtual void OnChangeLink(int lnk, int beginitem, int enditem, int newitem, void *data);
    virtual void OnUnlinkItems(int lnk, int beginitem, int enditem, void *data);
    virtual void OnTouchLink(int lnk, int beginitem, int enditem, void *data) 
    {_curlink=lnk;}
    virtual bool OnCustomDraw(int item, SGraphItem &itm, CDC &dc, CRect &rc);
    virtual bool OnCustomDrawLink(int lnk, int beginitem, int enditem, int order, void *data, CDC &dc, CRect rc) ;
    virtual void OnEndSelection();
    virtual void OnResizeItem(int item, int corner, float xs, float ys) 
    {}
    virtual void OnMoveItem(int item, float xs, float ys) 
    {}
    virtual void OnEndMoveResize(int corner) ;
    virtual void BeforeMoveResize() {}
    virtual void BeforeMoveItem() {SaveUndo();}
    virtual void OnLinkNewItem(int beginitem, float x, float y);
    
    bool TestMouseOffset(int cnt, int *mapId, float curmousex, float curmousey);
    void DrawExport(CDC &dc, const CRect &cliprc)
	  {_exportdraw=true;Draw(dc,cliprc);_exportdraw=false;}
    void Draw(CDC &dc, const CRect &cliprc);
  protected:
    static void DrawSingleLine(CDC &dc,const char *line,CRect &rc, int align, int curline, int totallines, int angle);
    static void DrawMultipleLines(CDC &dc, const char *text, CRect &rc, int align, int angle);
    //{{AFX_MSG(CDiagramView)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
	afx_msg void OnLButtonUp( UINT nFlags, CPoint point );
	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	afx_msg HBRUSH OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor );
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
      
    public:
	    void SetArrowType(int lnk, SDLinkInfo *flags);
      virtual bool CalcLinkRect(int from, int to, int order, CRect &rc);
	    int GetNextItem(int current, bool prev);
	    void SaveEditedText();
	    void BeginEditText();
		bool IsEditing() {return wTextEdit.GetSafeHwnd()!=NULL;}
	    void ChangeText(const CString &text);
	    SFloatRect GetFirstSelectItemRect();
	void ChangeTextAlign(int align);
    void ChangeTextColor(DWORD color);
    void ChangeFontFaceSize(const char *facename=NULL, int height=-1,int width=-1, int angle=-1);
    void OrderSelected(int order);
    int OrderItem(int item, int order);
    void SelectGroup();
    void GroupSelect(bool group);
    bool IsGroupSelect() 
    {return _grpselected;}
    void DuplicateSelected();
    QOStrStream *GetDataForClipboard();
    void PasteFromClipboard(QIStrStream &in);
    void DeleteSelected();
    int Mode(int mode);
    void SetTextContentAndStyle(CString text, LOGFONT &lg, COLORREF color, int align);
    void ChangeLineColor(COLORREF color);
    COLORREF GetRefColor();
    void ChangeBkColor(COLORREF color);
    ItemInfo * GetRefItem();
    void ChangeLineWidth(int width, int style);
    
	bool IsVariablesShown() {return _showVariables;}
	void ShowVariables(bool show) {_showVariables=show;Update();}

    CDiagramView();
    virtual ~CDiagramView();
    
    void PrepareToOptimize(CDC * optimizeDC);
    void PrepareToDrawingCell(CDC &dc, ItemInfo *info, COLORREF bgcolor);
    CRect GetClipRect(void);
    void SetLinkColor(int link, COLORREF color);
    
    void SetLinkToLastColor(int link);
    
    float GetGrid() 
    {return _grid;}
    bool IsGridVisible() 
    {return _gridshow;}
    void SetGrid(float grid,bool show) 
    {
      _grid=grid;
      _gridshow=show;
    }
    void AlignToGrid(SFloatRect & rc);
    void ChangeTextFontChanges(LOGFONT & srcfont, LOGFONT & trgfont, COLORREF srccolor , COLORREF trgcolor, int trgalign);
    bool Save(const char * filename);
    bool Load(const char * filename);
    void SaveUndo(void);
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    void GetPixelSize(float & x, float & y);
    void Streaming(IArchive & arch, bool serializeWindow=true);
	BOOL PreTranslateMessage(MSG* pMsg);
    bool IsDirty() {return _dirty;}
    void SetDirty(bool val) {_dirty=val; }

/// diagram simulator

	virtual double QueryValue(int index, bool ispin);
	virtual bool SetValue(int index, bool ispin, double value);
	virtual bool SetError(int errnumb, int errdata);
    void RecalculateDiagram();
    int FindItemByLinkID(int srcitem, int index);
    int FindItemByPin(int srcitem, int trgitem,int pin);
	void SimulateStep(bool stepover);

    


};

#endif // !defined(AFX_DIAGRAMVIEW_H__B67B7FB5_5A57_4692_95D6_09A7180E4C59__INCLUDED_)
