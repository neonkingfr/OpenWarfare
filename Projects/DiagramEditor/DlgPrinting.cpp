// DlgPrinting.cpp : implementation file
//

#include "stdafx.h"
#include "DiagramEditor.h"
#include "DlgPrinting.h"
#include ".\dlgprinting.h"
#include <Es/Common/fltopts.hpp>


// DlgPrinting dialog

IMPLEMENT_DYNAMIC(DlgPrinting, CDialog)
DlgPrinting::DlgPrinting(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPrinting::IDD, pParent)
    , vDiagram(NULL),
    wPrintDlg(NULL),
    _zoomrect(0,0,-1,-1),
    _lineWeight(96)
 {
	//{{AFX_DATA_INIT(DlgPrinting)
	vMarginBottom = 20;
	vMarginLeft = 20;
	vMarginRight = 20;
	vMarginTop = 20;
	vTop = 0;
	vZoom = 0;
	vLeft = 0;
	//}}AFX_DATA_INIT
  _change=false;
}

DlgPrinting::~DlgPrinting()
{
if (wPrintDlg!=NULL) delete wPrintDlg;
}

void DlgPrinting::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPrinting)
	DDX_Control(pDX, IDC_SPINZOOM, wSpinZoom);
	DDX_Control(pDX, IDC_SPINTOP, wSpinTop);
	DDX_Control(pDX, IDC_SPINLINEWEIGHT, wSpinLineWeight);
	DDX_Control(pDX, IDC_SPINLEFT, wSpinLeft);
DDX_Control(pDX, IDC_LINEWEIGHT, wLineWeight);
DDX_Control(pDX, IDC_PREVIEW, wPreview);
	DDX_Text(pDX, IDC_MARGINBOTTOM, vMarginBottom);
	DDX_Text(pDX, IDC_MARGINLEFT, vMarginLeft);
	DDX_Text(pDX, IDC_MARGINRIGHT, vMarginRight);
	DDX_Text(pDX, IDC_MARGINTOP, vMarginTop);
	DDX_Text(pDX, IDC_TOP, vTop);
	DDX_Text(pDX, IDC_ZOOM, vZoom);
	DDX_Text(pDX, IDC_LEFT, vLeft);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPrinting, CDialog)
	//{{AFX_MSG_MAP(DlgPrinting)
  ON_WM_DRAWITEM()
  ON_BN_CLICKED(IDC_SETUPPRINTER, OnBnClickedSetupprinter)
  ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_LEFT, OnChangeLeft)
	ON_EN_CHANGE(IDC_TOP, OnChangeTop)
	ON_EN_CHANGE(IDC_ZOOM, OnChangeZoom)
	ON_EN_CHANGE(IDC_LINEWEIGHT, OnChangeLineweight)
	ON_EN_CHANGE(IDC_MARGINTOP, OnChangeMargin)
	ON_EN_CHANGE(IDC_MARGINRIGHT, OnChangeMargin)
	ON_EN_CHANGE(IDC_MARGINBOTTOM, OnChangeMargin)
	ON_EN_CHANGE(IDC_MARGINLEFT, OnChangeMargin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// DlgPrinting message handlers

void DlgPrinting::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
  {
  CDC drawDC;  
  drawDC.Attach(lpDrawItemStruct->hDC);
  drawDC.FillSolidRect(&lpDrawItemStruct->rcItem,RGB(255,255,255));
  CRect itemRect(lpDrawItemStruct->rcItem);
  CSize itemSz=itemRect.Size();
  drawDC.SetMapMode(MM_ISOTROPIC);
  drawDC.SetWindowExt(4,4);
  drawDC.SetViewportExt(1,1);
  float scalex=(float)itemSz.cx*4/(float)_xmax;
  float scaley=(float)itemSz.cy*4/(float)_ymax;
  float scale=__min(scalex,scaley);
  CRect rc(0,0,toLargeInt(_xmax*scale),toLargeInt(_ymax*scale));
  drawDC.SelectStockObject(BLACK_PEN);
  drawDC.SelectStockObject(HOLLOW_BRUSH);
  drawDC.Rectangle(&rc);
  CRect zm;
  int left,top,zoom;
  GetPosInfo(left,top,zoom);
  zm.left=toLargeInt(((float)left-zoom/2)*scale);
  zm.right=toLargeInt(((float)left+zoom/2)*scale);
  zm.top=toLargeInt(((float)top-zoom*_drawAspect/2)*scale);
  zm.bottom=toLargeInt(((float)top+zoom*_drawAspect/2)*scale);
  
  SFloatRect fakerc=CalcFakeScaleDiagram(zm);
  SFloatRect curzoom=vDiagram->GetPanZoom();
  vDiagram->SetPanZoom(fakerc,false);
  vDiagram->DrawExport(drawDC,zm);
  vDiagram->SetPanZoom(curzoom,true);
  drawDC.Detach();
  }

void DlgPrinting::UpdatePageInfo(void)
  {
  _xmax=0;
  _ymax=0;
  CDC printDC;
  printDC.Attach(wPrintDlg->CreatePrinterDC());
  printDC.SetMapMode(MM_ISOTROPIC);
  if (printDC==NULL) return;
  _xdpi=printDC.GetDeviceCaps(LOGPIXELSX);  
  _ydpi=printDC.GetDeviceCaps(LOGPIXELSY);  
  _fxmax=printDC.GetDeviceCaps(PHYSICALWIDTH);
  _fymax=printDC.GetDeviceCaps(PHYSICALHEIGHT);
  wSpinLineWeight.SetRange32(50,_xdpi);
  UpdateScaleInfo();
  }

void DlgPrinting::UpdateScaleInfo()
  {
  if (wPreview.GetSafeHwnd()==NULL) return;
  UpdateData(TRUE);
  int ml=vMarginLeft*10*_xdpi/254;
  int mt=vMarginTop*10*_ydpi/254;
  int mr=vMarginRight*_xdpi*10/254;
  int mb=vMarginBottom*10*_ydpi/254;
  _xmax=(_fxmax-ml-mr)*_lineWeight/_xdpi;
  _ymax=(_fymax-mt-mb)*_lineWeight/_ydpi;  
  }

BOOL DlgPrinting::OnInitDialog()
  {  
  CDialog::OnInitDialog();
  if (wPrintDlg==NULL)
    {
    wPrintDlg=new CPrintDialog(TRUE);
    wPrintDlg->GetDefaults();
    }

  SFloatRect frc=vDiagram->GetBoundingBox();
  _drawAspect=(frc.bottom-frc.top)/(frc.right-frc.left);
  UpdatePageInfo();
  SetDlgItemInt(IDC_LINEWEIGHT,_lineWeight,FALSE);
   _change=true;

  wSpinZoom.SetRange32(1,100);
  wSpinLeft.SetRange32(0,100);
  wSpinTop.SetRange32(0,100);
  wSpinLeft.SetPos(50);
  wSpinTop.SetPos(50);
  wSpinZoom.SetPos(100);


  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }


void DlgPrinting::OnBnClickedSetupprinter()
  {
  wPrintDlg->m_pd.Flags&=~PD_RETURNDEFAULT;
  if (wPrintDlg->DoModal()==IDOK)
    {
    UpdatePageInfo();
    wPreview.Invalidate();
    }
  DWORD error=CommDlgExtendedError();
  if (error)
    {
    delete wPrintDlg;
    wPrintDlg=new CPrintDialog(TRUE); 
    if (wPrintDlg->DoModal()==IDOK)
      {
      UpdatePageInfo();
      wPreview.Invalidate();
      }  
    }
  }

void DlgPrinting::CopyDiagram(void)
  {
  }

void DlgPrinting::OnDestroy()
  {
  _toPrint.ResetContent();
  }

SFloatRect DlgPrinting::CalcFakeScaleDiagram(const CRect & rect)
  {
  CRect client;
  CSize csz;
  CSize rsz;
  SFloatRect frc=vDiagram->GetBoundingBox();
  vDiagram->GetClientRect(&client);
  csz=client.Size();
  rsz=rect.Size();
  SFloatRect map1=vDiagram->MapRectFromWindow(rect);
  SFloatRect map2=vDiagram->MapRectFromWindow(client);
  map2.right=map2.left+(float)csz.cx/(float)rsz.cx*(frc.right-frc.left);
  map2.bottom=map2.top+(float)csz.cy/(float)rsz.cy*(frc.bottom-frc.top);
  float xofs=frc.left-map2.left-rect.left*(map2.right-map2.left)/csz.cx;
  float yofs=frc.top-map2.top-rect.top*(map2.bottom-map2.top)/csz.cy;
  map2.left+=xofs;
  map2.right+=xofs;
  map2.bottom+=yofs;
  map2.top+=yofs;  
  return map2;
  }

void DlgPrinting::OnOK()
  {
  HDC hdcPrinter = wPrintDlg->CreatePrinterDC();
  if (hdcPrinter == NULL)
    {
    AfxMessageBox(IDS_NOPRINTERSELECTED);
    return;
    }
   // create a CDC and attach it to the default printer
   CDC dcPrinter;
   dcPrinter.Attach(hdcPrinter);
   // call StartDoc() to begin printing
   DOCINFO docinfo;
   memset(&docinfo, 0, sizeof(docinfo));
   docinfo.cbSize = sizeof(docinfo);
   char buff[256];
   LoadString(AfxGetInstanceHandle(),IDR_MAINFRAME,buff,sizeof(buff));
   docinfo.lpszDocName=buff;
   // if it fails, complain and exit gracefully
   if (dcPrinter.StartDoc(&docinfo) < 0)
     {
     AfxMessageBox(IDS_CANNOTINICIALIZEPRINTER);
     }
    else
       {
      // start a page
      if (dcPrinter.StartPage() < 0)
        {
         AfxMessageBox(_T("Could not start page"));
         dcPrinter.AbortDoc();
        }
      else
        {
        dcPrinter.SetMapMode(MM_ISOTROPIC); 
        dcPrinter.SetWindowExt(_lineWeight,_lineWeight); 
        dcPrinter.SetViewportExt( dcPrinter.GetDeviceCaps( LOGPIXELSX), 
                              dcPrinter.GetDeviceCaps( LOGPIXELSY));         
	    int lmarg=toLargeInt(vMarginLeft*10*_lineWeight/254);
		int tmarg=toLargeInt(vMarginTop*10*_lineWeight/254);
	 	CRect zm;
	    int left,top,zoom;
		GetPosInfo(left,top,zoom);
		zm.left=toLargeInt(((float)left-zoom/2)+lmarg);
		zm.right=toLargeInt(((float)left+zoom/2)+lmarg);
		zm.top=toLargeInt(((float)top-zoom*_drawAspect/2)+tmarg);
		zm.bottom=toLargeInt(((float)top+zoom*_drawAspect/2)+tmarg);
        SFloatRect fakerc=CalcFakeScaleDiagram(zm);
        SFloatRect curzoom=vDiagram->GetPanZoom();
        vDiagram->SetPanZoom(fakerc,false);
        vDiagram->DrawExport(dcPrinter,zm);
        vDiagram->SetPanZoom(curzoom,true);
        dcPrinter.EndPage();
        dcPrinter.EndDoc();        
        }
       }
     CDialog::OnOK();
    }
  
  


void DlgPrinting::OnChangeLeft() 
  {
  if (wPreview.GetSafeHwnd()==NULL) return;
  vLeft=GetDlgItemInt(IDC_LEFT,NULL,FALSE);
  wPreview.Invalidate();
  }

void DlgPrinting::OnChangeTop() 
  {
  if (wPreview.GetSafeHwnd()==NULL) return;
  vTop=GetDlgItemInt(IDC_TOP,NULL,FALSE);
  wPreview.Invalidate();
  } 

void DlgPrinting::OnChangeZoom() 
  {
  if (wPreview.GetSafeHwnd()==NULL) return;
  vZoom=GetDlgItemInt(IDC_ZOOM,NULL,FALSE);
  wPreview.Invalidate();
  } 

void DlgPrinting::OnChangeLineweight() 
  {
  if (wPreview.GetSafeHwnd()==NULL) return;
  _lineWeight=GetDlgItemInt(IDC_LINEWEIGHT,NULL,FALSE);
  UpdateScaleInfo();  	
  }

void DlgPrinting::OnChangeMargin() 
{
  if (wPreview.GetSafeHwnd()==NULL) return;
  UpdateScaleInfo();  	
}

void DlgPrinting::GetPosInfo(int &left, int &top, int &zoom)
  {
  int maxz;
  if (_xmax<_ymax) maxz=_xmax;else maxz=_ymax;
  left=vLeft*_xmax/100;
  top=vTop*_ymax/100;
  zoom=vZoom*maxz/100;
  }
