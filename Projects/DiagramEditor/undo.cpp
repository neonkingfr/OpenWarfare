#include "StdAfx.h"
#include "undo.h"

UndoRedo::UndoRedo(int max) : maxCount(max)
{
  undoBuf = new ParamFile*[maxCount];
  for (int i=0; i<maxCount; i++) undoBuf[i]=NULL;
  ix = redoIx = undoCount = 0;
}

UndoRedo::~UndoRedo()
{
  for (int i=0; i<maxCount; i++)
    if (undoBuf[i]) delete undoBuf[i];
  delete[] undoBuf;
}

void UndoRedo::SaveUndo(ParamFile *arch)
{
  if (++undoCount>maxCount) undoCount--; //cyclic
  if (undoBuf[ix]) delete undoBuf[ix];
  undoBuf[ix]=arch;
  ix=redoIx=(ix+1)%maxCount;
}

ParamFile *UndoRedo::Undo()
{
  ParamFile *retval=NULL;
  if (undoCount>0)
  {
    ix=(ix+maxCount-1)%maxCount;
    undoCount--;
    retval=undoBuf[ix];
  }
  return retval;
}

ParamFile *UndoRedo::Redo()
{
  ParamFile *retval=NULL;
  if (redoIx!=ix)
  {
    undoCount++;
    ix=(ix+1)%maxCount;
    retval=undoBuf[ix];
  }
  return retval;
}
