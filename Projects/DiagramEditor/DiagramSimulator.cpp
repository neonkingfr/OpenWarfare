// DiagramSimulator.cpp: implementation of the CDiagramSimulator class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "diagrameditor.h"
#include "DiagramSimulator.h"
#include <es/common/fltopts.hpp>
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define DISTANCE_AZ ('Z'-'A'+1)

CDiagramSimulator::CDiagramSimulator()
{

}

CDiagramSimulator::~CDiagramSimulator()
{

}

const char *CDiagramSimulator::IsExpression(const char *text)
  {
  const char *c=text;
  if (*c!='=')
	{
	c=strchr(text,':');
	if (c==NULL) return NULL;
    c++;
	while (*c<33 && *c!=0) c++;
	if (*c!='=') return NULL;	
	}
  c++;
  return c;
  }

bool CDiagramSimulator::IsAffectedBy(const char *expression, int index)
  {
  char buff[50];
  IndexToVariable(index,buff);
  char *fnd=strstr(expression,buff);
  if (fnd==NULL) return false;
  fnd+=strlen(buff);
  return !isalpha(*fnd);
  }

void CDiagramSimulator::IndexToVariable(int index, char *buff)
  {
  char *p=buff;
  *p++='$';
  *p=0;
  do 
	{
	memmove(p+1,p,strlen(p)+1);
	*p='A'+(index%DISTANCE_AZ);
	index/=DISTANCE_AZ;
	}
  while (index>0);
  }

int CDiagramSimulator::VariableToIndex(const char *buff, int chars, int *read)
  {
  if (chars==0) return -1;
  const char *ptr=buff;
  if (ptr[0]!='$') return -1;
  ptr++;chars--;
  if (chars==0 || !isalpha(*ptr)) return -1;
  int index=0;
  while (chars!=0 && isalpha(*ptr) )
	{
	index=index*DISTANCE_AZ+(toupper(*ptr)-'A');
	ptr++;chars--;
	}
  if (read) *read=ptr-buff;
  return index;
  }

void CDiagramSimulator::IndexToPin(int index, char *buff)
  {
  if (index==CDS_PIN_THIS)
	{
	strcpy(buff,"@this");	
	}
  else if (CDS_ISPIN_INPUT(index))
	{
	index-=CDS_PIN_INPUT_MIN;
	if (index>=DISTANCE_AZ) sprintf(buff,"@a[%d]",index);
	else sprintf(buff,"@%c",index+'a');
	}
  else if (CDS_ISPIN_OUTPUT(index))
	{
	index-=CDS_PIN_OUTPUT_MIN;
	if (index>=DISTANCE_AZ) sprintf(buff,"@A[%d]",index);
	else sprintf(buff,"@%c",index+'A');
	}
  }

int CDiagramSimulator::PinToIndex(const char *buff, int chars, int *read)
  {
  if (chars==0) return CDS_PIN_ERROR;
  const char *ptr=buff;
  if (ptr[0]!='@') return CDS_PIN_ERROR;
  ptr++;chars--;
  if (chars==0 || !isalpha(ptr[0])) return CDS_PIN_ERROR;
  if (chars<0 && strcmp(ptr,"this")==0 || chars>0 && strncmp(ptr,"this",chars)==0) 
	{
	if (read) *read=5; 
    return CDS_PIN_THIS;
	}
  bool output;
  int index;
  if (*ptr>='A' && *ptr<='Z') output=true;else output=false;
  index=toupper(*ptr)-'A';
  ptr++;chars--;
  if (*ptr=='[') 
	{
	int ird=0;
	double value;
	if (GetAnyVariableValue(ptr,value,chars,&ird)==false)
	  return CDS_PIN_ERROR;
	ptr+=ird;
	if (*ptr!=']') return CDS_PIN_ERROR;
	ptr++;
	index+=toLargeInt((float)value);	
	}
  if (read) *read=ptr-buff;
  if (output) 
	{
	index+=CDS_PIN_OUTPUT_MIN;
	return CDS_ISPIN_OUTPUT(index)?index:CDS_PIN_ERROR;
	}
  else
	{
	index+=CDS_PIN_INPUT_MIN;
	return CDS_ISPIN_INPUT(index)?index:CDS_PIN_ERROR;
	}
  }

bool CDiagramSimulator::GetAnyVariableValue(const char *ptr, double &value, int chars, int *read)
  {
  int var=PinToIndex(ptr,chars,read);
  if (var==CDS_PIN_ERROR)
	{
	var=VariableToIndex(ptr,chars,read);
	if (var==-1)
	  {
	  var=LocalToIndex(ptr,chars,read);
	  if (var==-1) return false;
	  value=_localVariables[var];
	  }
	else
	  {
	  value=QueryValue(var,false);
	  }	
	}
  else
	{
	value=QueryValue(var,true);
	}
  return !_isnan(value);
  }

int CDiagramSimulator::LocalToIndex(const char *buff, int chars, int *read)
  {
  if (chars==0) return -1;
  const char *ptr=buff;
  if (!isalpha(ptr[0])) return -1;
  int index;
  index=toupper(*ptr)-'A';
  ptr++;chars--;
  if (*ptr=='[') 
	{
	int ird=0;
	double value;
	if (GetAnyVariableValue(ptr,value,chars,&ird)==false)
	  return CDS_PIN_ERROR;
	ptr+=ird;
	if (*ptr!=']') return CDS_PIN_ERROR;
	ptr++;
	index+=toLargeInt((float)value);	
	}
  if (read) *read=ptr-buff;
  return (index>=0 && index<CDS_MAXLOCALVARIABLES)?index:-1;
  }

bool CDiagramSimulator::ParseNumber(const char *line, double &value, int *read)
  {
  const char *ptr=line;
  bool exp=false;
  if (*ptr=='+' || *ptr=='-') ptr++;
  if (*ptr!='.')
	{
	while (isdigit(*ptr)) ptr++;
	exp=true;
	}
  if (*ptr=='.')
	{
	exp=true;
	ptr++;
	while (isdigit(*ptr)) ptr++;
	}
  if (exp && toupper(*ptr)=='E')
	{
	ptr++;
	if (*ptr=='+' || *ptr=='-') ptr++;
  	while (isdigit(*ptr)) ptr++;
	}	
  if (read) *read=ptr-line;
  if (ptr==line) return false;
  char format[20];
  sprintf(format,"%%%dlf",ptr-line);  
  return sscanf(line,format,&value)==1;
  }

#define SYMBOL_TABLE
//one char symbols are represented as char with ascii code of simbol
//following more char simbols

#define SYMB_EQUAL 'E'
#define SYMB_LESSEQUAL 'L'
#define SYMB_GREATEQUAL 'G'
#define SYMB_NOTEQUAL 'N'
#define SYMB_LOGICAND 'A'
#define SYMB_LOGICOR 'O'
#define SYMB_VARIABLE '$'
#define SYMB_PIN '@'
#define SYMB_EOF 0
#define SYMB_ERROR 1
 
char CDiagramSimulator::ParseSymbol(const char *line, int *read)
  {
  const char *ptr=line;
  char symb=SYMB_ERROR;
  switch (*ptr)
	{
	case 0: symb=SYMB_EOF;break;
	case '>':
	  ptr++;
	  if (*ptr=='=') {symb=SYMB_GREATEQUAL;ptr++;}
	  else symb='>';
	  break;
	case '<':
	  ptr++;
	  if (*ptr=='=') {symb=SYMB_LESSEQUAL;ptr++;}
	  else symb='<';
	  break;
	case '!':
	  ptr++;
	  if (*ptr=='=') {symb=SYMB_NOTEQUAL;ptr++;}
	  else symb='!';
	  break;
	case '=':
	  ptr++;
	  if (*ptr=='=') {symb=SYMB_EQUAL;ptr++;}
	  else symb='=';
	  break;
	default:
	  if (isalnum(*ptr)) symb=SYMB_ERROR;
	  else symb=*ptr++;
	}
  if (read) *read=ptr-line;
  return symb;
  }

bool CDiagramSimulator::ParseFunctionName(const char *line, char *buff, int buffsz, int *read)
  {
  const char *ptr=line;
  int flen=0;
  if (!isalpha(ptr[0])) return false;
  ptr++;
  if (!isalpha(ptr[0])) return false;
  ptr++;
  while  (isalpha(ptr[0])) ptr++;
  flen=ptr-line;
  while (isspace(ptr[0])) ptr++;
  if (ptr[0]!='(') return false;
  if (flen>=buffsz) return false;
  ptr++;
  if (read) *read=ptr-line;
  strncpy(buff,line,flen);
  buff[flen]=0;
  strlwr(buff);
  return true;
  }

double CDiagramSimulator::CalcE(const char **line)
  {
  int read;
  char symb;
  double value;
rep:
  value=CalcLJ(line);
  symb=ParseSymbol(*line,&read);
  if (symb==';')
	{
	line[0]+=read;
	goto rep;
	}
  return value;
  }

double CDiagramSimulator::CalcR(const char **line)
  {
  double val1;
  double val2;
  int read;
  char symb;

  val1=CalcT(line);
  symb=ParseSymbol(*line,&read);
  switch (symb)
	{
	case SYMB_EQUAL:
	  line[0]+=read;
	  val2=CalcR(line);
	  return val1==val2;
	case SYMB_NOTEQUAL:
	  line[0]+=read;
	  val2=CalcR(line);
	  return val1!=val2;
	case '>':
	  line[0]+=read;
	  val2=CalcR(line);
	  return val1>val2;
	case '<':
	  line[0]+=read;
	  val2=CalcR(line);
	  return val1<val2;
	case SYMB_LESSEQUAL:
	  line[0]+=read;
	  val2=CalcR(line);
	  return val1<=val2;
	case SYMB_GREATEQUAL:
	  line[0]+=read;
	  val2=CalcR(line);
	  return val1>=val2;
	}
  return val1;
  }

double CDiagramSimulator::CalcLJ(const char **line)
  {
  double val1;
  double val2;
  int read;
  char symb;

  val1=CalcR(line);
  symb=ParseSymbol(*line,&read);
  switch (symb)
	{
	case SYMB_LOGICAND:
	  line[0]+=read;
	  val2=CalcLJ(line);
	  return (val1!=0 && val2!=0)?1.0f:0.0f;
	case SYMB_LOGICOR:
	  line[0]+=read;
	  val2=CalcLJ(line);
	  return (val1!=0 || val2!=0)?1.0f:0.0f;	  
	}
  return val1;
  }

double CDiagramSimulator::CalcT(const char **line)
  {
  double val1;
  double val2;
  int read;
  char symb;

  val1=CalcF(line);
  symb=ParseSymbol(*line,&read);
  switch (symb)
	{
	case '+':
	  line[0]+=read;
	  val2=CalcT(line);
	  return val1+val2;
	case '-':
	  line[0]+=read;
	  val2=CalcT(line);
	  return val1-val2;
	}
  return val1;
  }


double CDiagramSimulator::CalcF(const char **line)
  {
  double val1;
  double val2;
  int read;
  char symb;

  val1=CalcV(line);
  symb=ParseSymbol(*line,&read);
  switch (symb)
	{
	case '*':
	  line[0]+=read;
	  val2=CalcF(line);
	  return val1*val2;
	case '/':
	  line[0]+=read;
	  val2=CalcF(line);
	  return val1/val2;
	case '%':
	  line[0]+=read;
	  val2=CalcF(line);
	  return fmod(val1,val2);
	}
  return val1;
  }


double CDiagramSimulator::CalcV(const char **line)
  {
  int read;
  char symb;
  double val;

  static char buff[50];
  if (ParseFunctionName(*line,buff,sizeof(buff),&read)==false)
	{
	int var=PinToIndex(*line,-1,&read);
	if (var==CDS_PIN_ERROR)
	  {
	  var=VariableToIndex(*line,-1,&read);
	  if (var==-1)
		{
		var=LocalToIndex(*line,-1,&read);
		if (var==-1) 
		  {
		  if (ParseNumber(*line,val,&read)==false)
			{
			symb=ParseSymbol(*line,&read);
			if (symb==SYMB_ERROR)
				{SetError(CDS_ERR_SYNTAXERROR,0);return GetNAN();}
			else		  
			  switch (symb)
				{
				case '(':
				  {
				  line[0]+=read;
				  double val=CalcE(line);
				  if (ParseSymbol(*line,&read)!=')') 
					{SetError(CDS_ERR_SYNTAXERROR,0);return GetNAN();}
  				  line[0]+=read;
				  return val;
				  }
				default: 
  				  {SetError(CDS_ERR_SYNTAXERROR,0);return GetNAN();}
				}
			}
		  else
			{
     		line[0]+=read;
			return val;
			}
		  }
		else
		  {
		  line[0]+=read;
		  if (ParseSymbol(*line,&read)=='=')
			{
			line[0]+=read;
			val=CalcR(line);
			_localVariables[var]=val;		  
			}
		  return _localVariables[var];
		  }
		}
	  else
		{
  		line[0]+=read;
		if (ParseSymbol(*line,&read)=='=')
		  {
		  line[0]+=read;
		  val=CalcR(line);
		  double wait=GetWaitTime(line);
		  if (wait>=0.0f) AddEvent(wait,var,false,val);
		  else if (wait<0.0f)
				  if (SetValue(var,false,val)==false) SetError(CDS_ERR_CANNOTSETVALUE,var);
		  return val;
		  }
		else
		  {
		  val=QueryValue(var,false);
		  if (_isnan(val)) SetError(CDS_ERR_CANNOTGETVALUE,var);
		  return val;
		  }
		}
	  }
	else
	  {
  	  line[0]+=read;
	  if (ParseSymbol(*line,&read)=='=')
		{
		line[0]+=read;
		val=CalcR(line);
  	    double wait=GetWaitTime(line);
  	    if (wait>=0.0f) AddEvent(wait,var,false,val);
		else if (wait<0.0f)
		            if (SetValue(var,true,val)==false) SetError(CDS_ERR_CANNOTSETVALUE,-var);
		return val;
		}
	  else
		{
		val=QueryValue(var,true);
		if (_isnan(val)) SetError(CDS_ERR_CANNOTGETVALUE,-var);
		return val;
		}
	  }
	}
  else
	{
	line[0]+=read;
	return CalcFunction(line,buff);
	}

  }


double CDiagramSimulator::GetNAN()
  {
  double ret;
  memset(&ret,0xFF,sizeof(ret));
  return ret;
  }

double CDiagramSimulator::CalcFunction(const char **line, const char *functname)
  {
  double operands[50];
  char symb;
  int read;
  int i=0;
  symb=ParseSymbol(*line,&read);
  if (symb!=')') 
	for (i=0;i<50;i++)
	  {
	  operands[i]=CalcE(line);
	  symb=ParseSymbol(*line,&read);	  
	  if (symb==')') {i++;break;}
	  if (symb!=',') 
		  {SetError(CDS_ERR_SYNTAXERROR,0);return GetNAN();}
	  line[0]+=read;
	  }
  line[0]+=read;
  if (strcmp(functname,"sin")==0)
	if (i!=1) {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
	else return sin(operands[0]);
  if (strcmp(functname,"cos")==0)
	if (i!=1) {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
	else return cos(operands[0]);
  if (strcmp(functname,"tan")==0)
	if (i!=1) {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
	else return sin(operands[0])/cos(operands[0]);
  if (strcmp(functname,"atan")==0)
	if (i==1) return atan(operands[0]);
	else if (i==2) return atan2(operands[0],operands[1]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"pi")==0)
	if (i==0) return 3.1415926535;
	else if (i==1) return operands[0]*3.1415926535;
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"log")==0)
	if (i==1) return log(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"exp")==0)
	if (i==1) return exp(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"floor")==0)
	if (i==1) return floor(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"ceil")==0)
	if (i==1) return ceil(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"abs")==0)
	if (i==1) return fabs(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"acos")==0)
	if (i==1) return acos(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"asin")==0)
	if (i==1) return asin(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"cosh")==0)
	if (i==1) return cosh(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"sinh")==0)
	if (i==1) return sinh(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"pow")==0)
	if (i==2) return pow(operands[0],operands[1]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"sqrt")==0)
	if (i==1) return sqrt(operands[0]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"sqr")==0)
	if (i==1) return operands[0]*operands[0];
	else if (i==2) return pow(operands[0],operands[1]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"length")==0)
	if (i==2) return _hypot(operands[0],operands[1]);
	else if (i==3) return sqrt(operands[0]*operands[0]+operands[1]*operands[1]+operands[2]*operands[2]);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"frac")==0)
	if (i==1) return modf(operands[0],operands+1);
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"intp")==0)
	if (i==1) {modf(operands[0],operands+1);return operands[1];}
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  if (strcmp(functname,"simtime")==0)
	if (i==0) {return _curtime;}
	else {SetError(CDS_ERR_INVALIDCOUNTOPERANDS,i);return GetNAN();}
  else
	return QueryFunction(functname,operands,i);
  }

void CDiagramSimulator::DoCalc(const char *expression, double &value,double &waittime)
  {
  const char *ptr=expression;
  value=CalcE(&ptr);
  waittime=GetWaitTime(&ptr);
  if (*ptr!=0) 
	SetError(CDS_ERR_SYNTAXERROR,0);  
  }

void CDiagramSimulator::AddEvent(double waittime, int index, bool ispin, double value)
  {
  double newtime=_curtime+waittime;
  int cnt=_events.Size();
  int i;
  for (i=0;i<cnt;i++)
	if (_events[i].time==newtime && _events[i].ispin==ispin && _events[i].index==index)
	  {
	  _events[i].value=value;
	  return;
	  }
  _events.Access(cnt);
  for (i=cnt;i>0 && _events[i-1].time>newtime;i--)
	{
	_events[i]=_events[i-1];
	}
  _events[i].index=index;
  _events[i].value=value;
  _events[i].time=newtime;
  _events[i].ispin=ispin;
  }

double CDiagramSimulator::Step()
  {
  if (_events.Size()==0) return _curtime;
  _curtime=_events[0].time;
  SetValue(_events[0].index,_events[0].ispin,_events[0].value);
  _events.Delete(0);
  return _curtime;
  }

double CDiagramSimulator::GetWaitTime(const char **line)
  {
  int read;
  if (ParseSymbol(*line,&read)==':')
	{
	line[0]+=read;
	double value=CalcV(line);
	return value;
	}
  return -1.0f;
  }
