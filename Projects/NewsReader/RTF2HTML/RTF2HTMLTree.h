#ifndef _T_TREE_H_INCLUDED
#define _T_TREE_H_INCLUDED

#pragma warning(disable:4786)
#include "vector"
using namespace std;


template <class Type> class RTF2HTML_Ref;

template <class Type> class RTF2HTML_Data {
private:
	typedef unsigned long ULONG;
	RTF2HTML_Data(Type *p=NULL) { if(!p) p=new Type; pData=p; dwRefs=1;};
	ULONG AddRef() { dwRefs++; return dwRefs;};
 	ULONG Release() { 
		dwRefs--; if(dwRefs==0) {delete this;return 0;} return dwRefs;};

// accessors
	Type *operator->() { return pData;};
	operator Type&() { return *pData;};
	Type &get_Data() { return *pData;};
	~RTF2HTML_Data() { if(pData) delete pData;};
	// op new
	//
	Type *pData;
	ULONG dwRefs;
	friend class RTF2HTML_Ref<Type>;
};

template <class Type> class RTF2HTML_Ref {
	typedef RTF2HTML_Data<Type> DataT;
	
public:
	typedef unsigned long ULONG;
	typedef unsigned long KEY;
	// ctors
	RTF2HTML_Ref(const Type &t) { pData=new DataT(new Type(t));};
	RTF2HTML_Ref(Type *p) { if(p) pData=new DataT(p); else pData=NULL;};
	RTF2HTML_Ref() { pData=new DataT();};
	//RTF2HTML_Ref(RTF2HTML_Data<Type> *p) : pData(p) { if(p) p->AddRef();};
	RTF2HTML_Ref(const RTF2HTML_Ref<Type> &rhs) { pData=(rhs.pData); if(pData) pData->AddRef();};
	//dtor
	virtual ~RTF2HTML_Ref() {if(pData) pData->Release();};

	//operators =
	RTF2HTML_Ref &operator=(const RTF2HTML_Ref<Type> &rhs) 
	{ Release(); pData=rhs.pData; if(pData) pData->AddRef(); return *this;};
	RTF2HTML_Ref &operator=(const Type &t) 
	{ Release(); pData=new DataT(new Type(t)); return *this;};
	RTF2HTML_Ref &operator=(Type *p) 
	{ Release(); if(p) pData=new DataT(p); else pData=NULL; return *this;};
	// operator ==
	bool operator==(const RTF2HTML_Ref<Type> &rhs) 
	{ return pData==rhs.pData;};
	
	RTF2HTML_Ref Clone() { if(!pData) return RTF2HTML_Ref(); return RTF2HTML_Ref(pData->get_Data());};
	void Release() { if(pData) pData->Release(); pData=NULL;};
	bool IsNull() { return pData==NULL;};

// accessors	
	Type *operator->() {return pData->operator->();};
	Type &get_Data() { return pData->get_Data();};
	operator Type&() { return pData->get_Data();};
	KEY Key() { return ((KEY)pData)+1;};
protected: 
	ULONG GetRefCount() { if(!pData) return 0; return pData->dwRefs;};
	RTF2HTML_Ref(KEY key) { try{pData=(DataT*)(key-1);if(pData) pData->AddRef();}catch(...){pData=NULL;}};
private:
	RTF2HTML_Data<Type> *pData;
	friend class RTF2HTML_Ref<Type>;
// niama new	
};



template <class Type,class DataType2=RTF2HTML_NodeData<Type> > class RTF2HTML_Tree;
template <class Type> class RTF2HTML_NodeData {
private:	
	typedef RTF2HTML_Ref<RTF2HTML_NodeData> NodeRef;
	RTF2HTML_NodeData(const Type &RTF2HTML_Data) : tData(RTF2HTML_Data){};
	RTF2HTML_NodeData(Type *pData) : tData(pData) {};
	RTF2HTML_NodeData() : Parent((RTF2HTML_NodeData*)NULL) {};
	virtual ~RTF2HTML_NodeData() {};
private:
	RTF2HTML_Ref<Type> tData;
	std::vector<NodeRef> vChilds;
	RTF2HTML_Ref<RTF2HTML_NodeData> Parent;
	friend class RTF2HTML_Tree<Type>;
	friend class RTF2HTML_Ref<RTF2HTML_NodeData>;
	friend RTF2HTML_Data<RTF2HTML_NodeData>;
};



template <class Type,class DataType2=RTF2HTML_NodeData<Type> > class RTF2HTML_Tree : private RTF2HTML_Ref<DataType2> {
	typedef RTF2HTML_Ref<DataType2> RTF2HTML_NodeBase;
public:
	typedef RTF2HTML_Tree<Type> Node;
//	Node node;
	Node Parent() { if(!RTF2HTML_NodeBase::get_Data().IsNull()) return RTF2HTML_NodeBase::get_Data().Parent; return *this;};
	operator Type&() { return (RTF2HTML_NodeBase::get_Data().tData.get_Data()); };
	Type *operator->() {return &(RTF2HTML_NodeBase::get_Data().tData.get_Data());};
	__declspec( property( get=get_Data) ) RTF2HTML_Ref<Type> RTF2HTML_Data;
	RTF2HTML_Ref<Type> get_Data() { return RTF2HTML_NodeBase::get_Data().tData;};

	bool IsLeaf() { return RTF2HTML_NodeBase::get_Data().vChilds.size()==0;};
	bool IsNode() { return RTF2HTML_NodeBase::get_Data()vChilds.size()>0;};
	bool IsRoot() { return RTF2HTML_NodeBase::get_Data().Parent.IsNull();};
	bool operator ==(const Node &rhs) { return *(RTF2HTML_NodeBase*)this==(RTF2HTML_NodeBase &)rhs;};

	__declspec( property( get=get_Nodes) ) Node Nodes[];
	Node get_Nodes(int nIndex) { return (Node)RTF2HTML_NodeBase::get_Data().vChilds[nIndex]; };
	__declspec( property( get=get_Count) ) int Count;
	int get_Count() { return RTF2HTML_NodeBase::get_Data().vChilds.size();};
	__declspec( property( get=get_Key) ) unsigned long Key;
	unsigned long get_Key() { return RTF2HTML_NodeBase::Key();};

	Node AddNode(const Type &t) { Node n(t); ((RTF2HTML_NodeBase &)n).get_Data().Parent=(*this); RTF2HTML_NodeBase::get_Data().vChilds.push_back(n); return n;};
	void Delete() {
		if(!RTF2HTML_NodeBase::get_Data().Parent->IsNull()) RTF2HTML_NodeBase::get_Data().Parent->DeleteNode(*this);
		//Release();
	};
	void DeleteNode(int nIndex) {
		RTF2HTML_NodeBase::get_Data().vChilds.erase(RTF2HTML_NodeBase::get_Data().vChilds.begin()+nIndex);
	};
	void DeleteNode(Node &node) {
		vector<RTF2HTML_NodeBase>::iterator = find(RTF2HTML_NodeBase::get_Data().vChilds.begin(),RTF2HTML_NodeBase::get_Data().vChilds.end(),node);
		if(iterator!=RTF2HTML_NodeBase::get_Data().vChilds.end()) {
			RTF2HTML_NodeBase::get_Data().vChilds.erase(iterator);
		}
	};
	RTF2HTML_Tree(const Type &RTF2HTML_Data) : RTF2HTML_NodeBase(RTF2HTML_Data) {};
	RTF2HTML_Tree(Type *RTF2HTML_Data) : RTF2HTML_NodeBase(RTF2HTML_Data) {};
	RTF2HTML_Tree() {};
	RTF2HTML_Tree(const RTF2HTML_Tree<Type> &rhs) : RTF2HTML_NodeBase(rhs) {};
	RTF2HTML_Tree(const RTF2HTML_NodeBase &rhs) : RTF2HTML_NodeBase(rhs) {};
	RTF2HTML_Tree(KEY key) : RTF2HTML_NodeBase(key) {};
	virtual ~RTF2HTML_Tree() 
	{	// if RefCount==ChildsCount+1, now is the time for release this (i.e. remove all refs to parent from childs
		ReleaseNode();
	};

private:
	void ReleaseNode() {
		if(RTF2HTML_NodeBase::IsNull()) return;
		ULONG nRefs=RTF2HTML_NodeBase::GetRefCount();
		int nChilds=RTF2HTML_NodeBase::get_Data().vChilds.size();
		if(nRefs==nChilds+1) {
			std::vector<RTF2HTML_NodeBase> &vChilds=RTF2HTML_NodeBase::get_Data().vChilds;
			for(int n=0;n<vChilds.size();n++) {
				vChilds[n]->Parent.Release();
				((Node &)vChilds[n]).ReleaseNode();
			}
		}
		Release();
	}
};


#endif