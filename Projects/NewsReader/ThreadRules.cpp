#include "stdafx.h"
#include <el/BTree/Btree.h>
#include <es/strings/rstring.hpp>
#include "Archive.h"
#include "ArchiveInline.h"
#include ".\threadrules.h"


void ThreadRules::AddRule(const ThreadRule &rule)
{
  _rules.Add(rule);
}

void ThreadRules::InsertRule(int pos,const ThreadRule &rule)
{
  _rules.Insert(pos,rule);
  if (_highlight>=pos) _highlight++;
  if (_filter>=pos) _filter++;
}

void ThreadRules::MoveUp(int pos)
{
  if (pos>0)
  {
    InsertRule(pos-1,_rules[pos]);
    RemoveRule(pos+1);
  }
}

void ThreadRules::MoveDown(int pos)
{ 
  if (pos+1<_rules.Size())
  {
    InsertRule(pos+2,_rules[pos]);
    RemoveRule(pos);
  }
}

void ThreadRules::SetFilter(int id)
{
  if (id<0 || id>=_rules.Size()) _filter=-1;
  else _filter=id;
}

int ThreadRules::GetFilter() const
{
  return _filter;
}

void ThreadRules::SetHighlight(int id)
{
  if (id<0 || id>=_rules.Size()) _highlight=-1;
  else _highlight=id;
}

int ThreadRules::GetHighlight() const
{
  return _highlight;
}

void ThreadRules::RemoveRule(int id)
{
  _rules.Delete(id);
  if (_filter>=id) _filter--;
  if (_highlight>=id) _highlight--;
}
void ThreadRules::Streaming(IArchive &arch)
{
  int cnt=_rules.Size();
  arch("count",cnt);
  if (-arch)
  {
    if (cnt>0) _rules.Access(cnt-1);else _rules.Clear();
  }
  for(int i=0;i<_rules.Size();i++)
    _rules[i].Streaming(arch);
  arch("filter",_filter);
  arch("highlight",_highlight);
}


void ThreadRule::Streaming(IArchive &arch)
{
  arch("name",_ruleName);
  arch("cond",_ruleCond);
}

void ThreadRules::SetRule(int pos, const ThreadRule &rule)
{
  _rules[pos]=rule;
}

void ThreadRules::LoadFromKwd(CString kwd, int curFilter, int curHighlight)
{
  CString tmp=kwd;
  char *buff=tmp.LockBuffer();
  char *c=buff;
  char *n=buff;;
  char *r=0;
  while (*c)
  {
    if (*c=='~')
    {
      *c=0;
      r=c+1;
    }
    else if (*c=='\n')
    {
      *c=0;
      AddRule(ThreadRule(n,r));
      n=c+1;
    }
    c++;
  }
  tmp.UnlockBuffer();
  SetFilter(curFilter);
  SetHighlight(curHighlight);
}

int ThreadRules::FindByName(const char *name)
{
  for (int i=0;i<_rules.Size();i++)
  {
    if (_rules[i].GetName().CompareNoCase(name)==0) return i;
  }
  return -1;
}