// MessCache.cpp: implementation of the CMessCache class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "MessCache.h"
#include <io.h>
using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define IDX_EXTENSION ".idx"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMessCache::CMessCache()
  {
  open=false;
  fullname=NULL;
  lastmsgid=NULL;
  }

//--------------------------------------------------

CMessCache::~CMessCache()
  {
  free(fullname);
  free(lastmsgid);
  }

//--------------------------------------------------

bool CMessCache::InitCache(const char *pathname,DWORD maxsize)
  {
  char buff[MAX_PATH+40];
  CScoopCritSect scoop(lock);
  GetFullPathName(pathname,sizeof(buff),buff,NULL);
  free(fullname);
  fullname=strdup(buff);
  char *c=strrchr(buff,'.');
  strcpy(c,IDX_EXTENSION);
  if (open) CloseCache();
  if (_access(buff,0)==-1) remove(fullname); //remove cache, is index does not exists
  dta.clear();
  dta.open(fullname,ios::in|ios::out|ios::binary);
  if (!dta) {dta.clear();dta.open(fullname,ios::in|ios::out|ios::binary|ios::trunc);}
  if (!dta) return false;
  dta.seekg(0,ios::end);
  if (dta.tellg().operator ==(0)) 
    dta.write("CACHE",6);
  if (!dta) return false;
  if (!index.Open(buff)) return false;
  this->maxsize=maxsize;
  open=true;
  return true;
  }

//--------------------------------------------------

iostream *CMessCache::OpenMessage(const char *messageid)
  {  
  if (!open) return NULL;
  ASSERT(lastmsgid==NULL);
  lock.Lock();
  read=true;
  unsigned long offset;
  CDI_Errors error=index.Find(messageid,&offset);
  if (error!=CDI_OK)
    {
    lock.Unlock();
    return NULL;
    }
  dta.clear();
  dta.seekg(offset);
  if (!dta.good()) 
    {
    lock.Unlock();
    return NULL;
    dta.clear();
    }
  return &dta;
  }

//--------------------------------------------------

void CMessCache::CloseMessage()
  {  
  if (lastmsgid)
    {
    DWORD sz=(long)dta.tellp()-start;
    index.AddKey(lastmsgid,start,sz);
    free(lastmsgid);
    lastmsgid=NULL;
    }
  lock.Unlock();
  dta.clear();
  }

//--------------------------------------------------

iostream* CMessCache::WriteMessage(const char *messageid)
  {
  if (!open) return NULL;
  lock.Lock();
  ASSERT(lastmsgid==NULL);
  dta.clear();
  dta.seekp(0,ios::end);
  if (!dta.good()) 
    {
    lock.Unlock();
    return NULL;
    }
  start=dta.tellp();
  lastmsgid=strdup(messageid);  
  return &dta;
  }

//--------------------------------------------------

bool CMessCache::IsCacheFull()
  {
  dta.clear();
  dta.seekp(0,ios::end);
  DWORD size=dta.tellp();  
  if (size==0xffffffff) size=0;
  return size>maxsize;
  }

//--------------------------------------------------

bool CMessCache::MoveCache(const char *newname)
  {
  char newdat[MAX_PATH+40];
  char newidx[MAX_PATH+40];
  char olddat[MAX_PATH+40];
  char oldidx[MAX_PATH+40];
  char *c;
  CScoopCritSect scoop(lock);
  if (open) CloseCache();
  
  GetFullPathName(newname,sizeof(newdat),newdat,NULL);
  strcpy(newidx,newdat);
  c=strrchr(newidx,'.');strcpy(c,IDX_EXTENSION);
  GetFullPathName(fullname,sizeof(olddat),olddat,NULL);
  strcpy(oldidx,olddat);
  c=strrchr(oldidx,'.');strcpy(c,IDX_EXTENSION);
  DeleteFile(newdat);
  DeleteFile(newidx);
  if (MoveFile(olddat,newdat)==FALSE) return false;
  if (MoveFile(oldidx,newidx)==FALSE) return false;
  InitCache(newdat);
  return true;
  }

//--------------------------------------------------

void CMessCache::DeleteMessage(const char *messageid)
  {
  CScoopCritSect scoop(lock);
  index.DeleteKey(messageid);  
  }

//--------------------------------------------------


bool CMessCache::FindInCache(const char *messageid)
  {
  unsigned long offset;
  lock.Lock();
  CDI_Errors error=index.Find(messageid,&offset);
  lock.Unlock();
  return  error==CDI_OK;
  }
