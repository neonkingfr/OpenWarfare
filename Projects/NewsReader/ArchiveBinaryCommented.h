// ArchiveBinaryCommented.h: interface for the ArchiveBinaryCommented class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVEBINARYCOMMENTED_H__7CD90A31_CA38_4315_829A_019D68CAD6F2__INCLUDED_)
#define AFX_ARCHIVEBINARYCOMMENTED_H__7CD90A31_CA38_4315_829A_019D68CAD6F2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Archive.h"

class ArchiveBinaryCommented : public Archive  
{
public:
	ArchiveBinaryCommented();
	virtual ~ArchiveBinaryCommented();

};

#endif // !defined(AFX_ARCHIVEBINARYCOMMENTED_H__7CD90A31_CA38_4315_829A_019D68CAD6F2__INCLUDED_)
