// DiskIndex2.h: interface for the CDiskIndex2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DISKINDEX2_H__12FB7FAA_0D69_4C47_959D_009152BD2AED__INCLUDED_)
#define AFX_DISKINDEX2_H__12FB7FAA_0D69_4C47_959D_009152BD2AED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define CDI_INDEX_MAXSIZE 32
#define CDI_BLOCK_BITS 3


#define CDI_BLOCK_SIZE (1<<CDI_BLOCK_BITS)
#define CDI_BLOCK_MASK (CDI_BLOCK_SIZE-1)

enum CDI_Errors 
  {CDI_OK, CDI_NotFound, CDI_DiskReadError, CDI_DiskWriteError};

//--------------------------------------------------

class CDiskIndex2  
  {
  enum ChunkMode 
    {CHK_Empty=0, CHK_NextLevel=1, CHK_EndChunk=-1};
  struct IndexChunk
    {
    ChunkMode mode:2;
    unsigned levelpos:30;
    };
  
  struct EndChunk
    {
    char fullkey[CDI_INDEX_MAXSIZE];
    unsigned long seekpos;
    long size;
    };
  
  struct BlockChunk
    {
    unsigned long loadpos;
    IndexChunk chunks[CDI_BLOCK_SIZE];
    public:
      bool Save(std::iostream &io);
      bool Load(std::iostream &io, unsigned long pos);
    };
  
  struct BlockEChunk
    {
    unsigned long loadpos;
    EndChunk endchunk;
    public:
      bool Save(std::iostream &io);
      bool Load(std::iostream &io, unsigned long pos);
    };
  
  std::fstream _indexfile;
  
  
  public:
    CDI_Errors DeleteKey(const char *key); //!<WARNING: It does'n reclain unused space. Only marks key deleted
    CDI_Errors AddKey(const char *key, unsigned long seekpos, long size);
    CDI_Errors Find(const char *key, unsigned long *seekpos=NULL, long *size=NULL);
    bool Open(const char *indexfilename);
    void Close();
    
    static void Test();
    
    
    CDiskIndex2();
    virtual ~CDiskIndex2();
  private:
    bool MoveChunkToNextLevel(BlockChunk &chunk, int pos, BlockEChunk &endchk, int bitindex);
    static int GetChunkPosFromKey(const char *key, int bitindex);
    CDI_Errors Find(const char *key, BlockEChunk &info, BlockChunk& prevchunk, int &prevchunkindex, int &bitindex);
    
  };

//--------------------------------------------------

#endif // !defined(AFX_DISKINDEX2_H__12FB7FAA_0D69_4C47_959D_009152BD2AED__INCLUDED_)
