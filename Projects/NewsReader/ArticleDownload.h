// ArticleDownload.h: interface for the CArticleDownloadControl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARTICLEDOWNLOAD_H__0BBBB1F4_AD9E_4767_8CBB_9C6470B9FB3D__INCLUDED_)
#define AFX_ARTICLEDOWNLOAD_H__0BBBB1F4_AD9E_4767_8CBB_9C6470B9FB3D__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define MAXDOWNLOADNOTIFYPOS TOTALTHREADS


class CNewsArtHolder;
class CNewsAccount;
class CNewsGroup;
class CWebBrowser2;

class CArticleDownloadControl
  { 
  CNewsArtHolder *downloadArticles[MAXDOWNLOADNOTIFYPOS];
  CWebBrowser2 **browselist;
  int browsecount;
public:
	enum EDownloadState
	  {
	  ED_IsReady, //Article is ready
	  ED_DowloadStarted,	  //Download has been started. Wait
	  ED_InProgress,  //Article is being downloaded
	  ED_Full		  //No download position
	  };
	enum EDownloadResult
	  {
	  ED_Ok,	//Article was downloaded and ready
	  ED_ConnectError,	  //Download error (no connection)
	  ED_NotFound,		  //Article has not been found on server
	  ED_Interrupted,	  //Download interrupted
	  ED_Offline,		  //NR operate in offline mode
	  ED_TooLong,		  //Article is too long
	  };


  CArticleDownloadControl();
public:
  void NotifyBrowsers(CNewsArtHolder *hld, EDownloadResult result);
  void UnregisterBrowser(CWebBrowser2 *brw);
  bool RegisterBrowser(CWebBrowser2 *brw);
  EDownloadState PrepareArticle(CNewsAccount *acc, CNewsGroup *grp, CNewsArtHolder *hlda, bool download_long=false);
  };



#endif // !defined(AFX_ARTICLEDOWNLOAD_H__0BBBB1F4_AD9E_4767_8CBB_9C6470B9FB3D__INCLUDED_)
