// ReadWriteLock.cpp: implementation of the CReadWriteLock class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ReadWriteLock.h"
#include <malloc.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

inline DWORD CorrectWaiting(DWORD timeout, DWORD starttime)
  {
  if (timeout==INFINITE) return timeout;
  DWORD ellapsed=GetTickCount()-starttime;
  if (ellapsed>timeout) return 0;
  else return timeout-ellapsed;
  }

DWORD WaitForObjectProcessMessages(HANDLE pHandles,DWORD dwMilliseconds, DWORD dwWakeMask=QS_SENDMESSAGE,
    HWND hWnd=0, int msglow=0, int msghigh=0)
  {
  DWORD stime=GetTickCount();
  do
    {    
    DWORD remain=CorrectWaiting(dwMilliseconds,stime);
    DWORD res=MsgWaitForMultipleObjects(1,&pHandles,FALSE,remain,dwWakeMask);
    if (res==WAIT_OBJECT_0) return res;
    else if (res==WAIT_OBJECT_0 + 1)
      {
      MSG msg;
      while (PeekMessage(&msg,hWnd,msglow,msghigh,PM_REMOVE | (dwWakeMask<<16)))
        DispatchMessage(&msg);
      }
    else if (res==WAIT_ABANDONED_0)
      return res;    
    else return WAIT_TIMEOUT;
    }
  while (true);
  }


CRWLockThreadList::CRWLockThreadList()
  {
  _size=0;
  _mutexes=0;
  _counts=0;
  _ids=0;
  }

CRWLockThreadList::~CRWLockThreadList()
  {
  Reset();
  }

void CRWLockThreadList::Reset()
  {
  for (int i=0;i<_size;i++) CloseHandle(_mutexes[i]);
  free(_mutexes);
  free(_counts);
  free(_ids);
  _mutexes=0;
  _counts=0;
  _ids=0;
  _size=0;
  }


int CRWLockThreadList::Resize()
  {
  int p=_size;
  _size++;
  HANDLE *mutexes=(HANDLE *)realloc(_mutexes,sizeof(HANDLE)*_size);
  DWORD *counts=(DWORD *)realloc(_counts,sizeof(DWORD)*_size);
  DWORD *ids=(DWORD *)realloc(_ids,sizeof(DWORD)*_size);
  if (mutexes) _mutexes=mutexes;
  if (ids) _ids=ids;
  if (counts) _counts=counts;
  if (mutexes==NULL || counts==NULL || ids==NULL)  return -1;
  if ((mutexes[p]=CreateMutex(NULL,FALSE,NULL))==NULL)
    {_size=p;return -1;}
  _counts[p]=0;
  _ids[p]=0;
  return p;
  }

BOOL CRWLockThreadList::AcquireMutex()
  {
  int i;
  BOOL res;
  for (i=0;i<GetCount();i++)
    {
    res=WaitForSingleObject(_mutexes[i],0);
    if (res==WAIT_OBJECT_0)
      {
      _ids[i]=GetCurrentThreadId();
      _counts[i]=1;
      return TRUE;
      }
    }
  if (i==GetCount())
    {
    i=Resize();
    if (i!=-1)
      {
      WaitForSingleObject(_mutexes[i],0);
      _ids[i]=GetCurrentThreadId();
      _counts[i]=1;
      return TRUE;
      }
    }
  return FALSE;  
  }

int CRWLockThreadList::FindThreadMutex(DWORD id)
  { 
  if (id==0) id=GetCurrentThreadId();
  for (int i=0;i<_size;i++) if (_ids[i]==id) return i;
  return -1;
  }



CReadWriteLock::CReadWriteLock()
  {
  _writelock=CreateMutex(NULL,0,NULL);
  _wcount=0;
  InitializeCriticalSection(&_tunnel);
  }

CReadWriteLock::~CReadWriteLock()
  {
  CloseHandle(_writelock);
  DeleteCriticalSection(&_tunnel);
  }


BOOL CReadWriteLock::Lock(bool lockWrite, DWORD miliseconds, DWORD pm, HWND hWnd, int msglow,int msghigh)
  {
  if (_wcount && GetCurrentThreadId()==_wid)  //write lock counter is always zero, if no owns this lock
    {    
    _wcount++;  //add owners reference
//    TRACE3("+RWLock(W): Thread %x takes over lock %x for writting (ref %d)\n",GetCurrentThreadId(),this,_wcount);
    return TRUE; //Lock was successful
    }
  if (lockWrite) //locking for writting
    {
    int backget=0; //pocet vnoreni cteciho mutexu
    int i;
    DWORD stime=GetTickCount();    
    DWORD out;
    HANDLE *hh;
    int count=0;    
	BOOL res=TRUE;
    
    
    while (Unlock()) backget++; //release all locks for reading if possible
     //it will release all threads waiting for write in queue

       //Wait for writting mutex
//    TRACE3("+RWLock(W): Thread %x is waiting for %x for writting (backget %d)\n",GetCurrentThreadId(),this,backget);

    out=WaitForObjectProcessMessages(_writelock,miliseconds,pm,hWnd,msglow,msghigh);
    if (out==WAIT_TIMEOUT) 
      {
//      TRACE1("+RWLock(W): Thread %x timeout (1)\n",GetCurrentThreadId());      
      res=FALSE; //it is timeout, but we have some jobs here, so we will not leave
      }
	else    
	  {
//	  TRACE2("+RWLock(W): Thread %x is waiting for %x for readers \n",GetCurrentThreadId(),this);
	  //wait for all reading mutexes to release    
		{
		ScoopLock lock(&_tunnel);  //critical section
		count=_readlock.GetCount();
		hh=(HANDLE *)alloca(count*sizeof(HANDLE));
		for (i=0;i<_readlock.GetCount();i++) hh[i]=_readlock.GetMutex(i);
		}                          //end if critical section
	  for (i=0;i<count;i++) 
		{
		DWORD remain=CorrectWaiting(miliseconds,stime);
		out=WaitForObjectProcessMessages(hh[i],remain,pm,hWnd,msglow,msghigh);
		if (out==WAIT_TIMEOUT) 
		  {
//		  TRACE1("+RWLock(W): Thread %x timeout (2)\n",GetCurrentThreadId());     
		  ReleaseMutex(_writelock); //release mutex that has been get abowe
		  res=FALSE; //it is timeout, but we have some jobs here, so we will not leave
		  break;
		  }
		}
      //release all owned mutexes (mutexes will be free, because we are in 
      //_writeLock section
      for (i=count-1;i>=0;i--) ReleaseMutex(hh[i]);
	  }
    
    if (backget)      //return all lock for reading
      for (i=0;i<backget;i++) Lock(false,miliseconds,pm,hWnd,msglow,msghigh);      
	if (res==FALSE) return res; //it is good time to leave here, if an error has occured.

    //currently owning this lock
    _wcount=1; //setup write lock counter
    _wid=GetCurrentThreadId();
//    TRACE2("+RWLock(W): Thread %x is takes over lock %x\n",GetCurrentThreadId(),this);
    return TRUE;
    }
  else      //locking for reading
    {
      {
      ScoopLock lock(&_tunnel); //critical section
      int p=_readlock.FindThreadMutex(); //find mutex of current thread
      if (p!=-1) //mutex found
        {
//        TRACE2("+RWLock(R): Adding reference for owners thread %x in lock %x\n",GetCurrentThreadId(),this);
        _readlock.AddRefMutex(p); //add reference and exit success
        return TRUE; 
        }
      }                          //end of critical section
    DWORD res;
    BOOL ok;
tryagain:
    
    //wait for writting mutex
//    TRACE2("+RWLock(R): Thread %x is waiting for %x for reading \n",GetCurrentThreadId(),this);
    res=WaitForObjectProcessMessages(_writelock,miliseconds,pm,hWnd,msglow,msghigh);
    if (res==WAIT_TIMEOUT) 
      {
//      TRACE1("+RWLock(R): Thread %x timeout\n",GetCurrentThreadId());    
      return FALSE; //it is timeout
      }
      {
      ScoopLock lock(&_tunnel);
      ok=_readlock.AcquireMutex();  //acquire reading mutex and setup it
      }
     ReleaseMutex(_writelock);  //release writting lock, so reading lock is owned
     if (!ok) 
      {
      // ERROR allocating space;
      // wait for any mutex released
      Sleep(1000);
      goto tryagain; //lets try again
      }      
//    TRACE3("+RWLock(R): Thread %x is takes over lock %x (%d)\n",GetCurrentThreadId(),this,_readlock.FindThreadMutex());
    }
  return TRUE;
  }


BOOL CReadWriteLock::Unlock()
  {
  ScoopLock lock(&_tunnel);
  BOOL res;
  
  if (_wcount>1 && GetCurrentThreadId()==_wid)    //still I owning lock
    {
//    TRACE3("-RWLock(W): Thread %x releases reference of lock %x (ref %d)\n",GetCurrentThreadId(),this,_wcount);
    _wcount--;      //release one reference
    return TRUE;    //Unlock was successful;
    }

  res=ReleaseMutex(_writelock); //try to release writting lock;
  if (res==FALSE) //unsuccessful, assume, that Unlock reading section
    {
    int i=_readlock.FindThreadMutex(); //try to find read mutex
    if (i==-1) //unsuccessful, no lock currently has, return error;
      return FALSE;
    res=_readlock.DecRefMutex(i);
    if (res)  //final count reached, release mutex
      {
//      TRACE3("-RWLock(R): Thread %x final release of lock %x (%d)",GetCurrentThreadId(),this,i);
      while (ReleaseMutex(_readlock.GetMutex(i)));// TRACE1(" [!] ",i); //release mutex
//      TRACE0("\n");
      return TRUE;        
      }
//    else
//      TRACE2("-RWLock(R): Thread %x releases reference of lock %x (still owns) \n",GetCurrentThreadId(),this);
    }
  else
//   TRACE2("-RWLock(W): Thread %x final release of lock %x\n",GetCurrentThreadId(),this);
  _wcount=0;
  _wid=0;

  return TRUE; //write lock has been release.
  }

CRWLockGuard::CRWLockGuard(CReadWriteLock &lock, bool writelock, bool initialy):
  _lock(lock),_writelock(writelock)
    {
    _locked=false;
    if (initialy) Lock(INFINITE);
    }
  
  CRWLockGuard::~CRWLockGuard()
    {
    if (_locked) Unlock();
    }
  
  BOOL CRWLockGuard::Lock(DWORD timeout)
    {
    if (_locked) return TRUE;
    if (_lock.Lock(_writelock,timeout)==FALSE) return FALSE;
    _locked=true;
    return TRUE;
    }
  
  BOOL CRWLockGuard::LockPM(DWORD miliseconds, DWORD pm, HWND hWnd, int msglow, int msghigh)
    {
    if (_locked) return TRUE;
    if (_lock.Lock(_writelock,miliseconds, pm, hWnd, msglow, msghigh)==FALSE) return FALSE;
    _locked=true;
    return TRUE;
    }
  
  void CRWLockGuard::Unlock()
    {
    if (_locked) _lock.Unlock();
    _locked=false;
    }

 CReadWriteLock testlock;

 static UINT TestThread(LPVOID test)
   {
   srand((unsigned int)test);
   int p=0;
   for (int i=0;i<100;i++)
	 {
	 if (p>3) 
	   {
	   testlock.Unlock();
	   p--;
	   }
	 else
	   if (rand() & 1) 
		 {
		 bool lock=(rand() & 3)==3;
		 TRACE3("TestMutex: Thread %x lock: %d - continues in level %d\n",GetCurrentThreadId(),lock,p);
		 testlock.Lock(lock,(rand() & 7)==7?5000:INFINITE);
		 p++;
		 }
	   else 
		 {
		 TRACE2("TestMutex: Thread %x unlock - continues in level %d\n",GetCurrentThreadId(),p);
		 testlock.Unlock();
		 p--;
		 if (p<0) p=0;
		 }
	 Sleep((rand() & 1023)+250);
	 }
   return 0;
   }

  void	TestMutex()	
	{
	for (int i=0;i<10;i++)
	  AfxBeginThread((AFX_THREADPROC)TestThread,(LPVOID)rand());
	}