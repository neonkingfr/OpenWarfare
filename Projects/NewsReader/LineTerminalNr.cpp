// LineTerminalNr.cpp: implementation of the CLineTerminalNr class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "LineTerminalNr.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define DEFAULTTIMEOUT 1000
#define CONNECTTIMEOUT 5000


HANDLE CLineTerminalNr::debugout=FALSE;
bool CLineTerminalNr::stop=false;

CLineTerminalNr::CLineTerminalNr():
LineTerminal(_tcpin,_tcpout),_tcpin(_conn,DEFAULTTIMEOUT,false),_tcpout(_conn,DEFAULTTIMEOUT,false)
{
stopwnd=NULL;
}

CLineTerminalNr::~CLineTerminalNr()
{

}

void CLineTerminalNr::Init()
  {
  stop=false;
  runflag=true;
  }

void CLineTerminalNr::OpenDebug()
  {
  AllocConsole();
  debugout=GetStdHandle(STD_OUTPUT_HANDLE);
  }
  

void CLineTerminalNr::CloseDebug()
  {
  FreeConsole();
  debugout=NULL;
  }

bool CLineTerminalNr::DebugOpened()
  {
  return debugout!=NULL;
  }

int CLineTerminalNr::Connect(const char *address, unsigned int port)
  {
  stop=false;
  runflag=true;
  SOCKADDR_IN sin;
  if (GetHostByName(address,sin)==false) return CLT_CONNECTFAILED;
  _conn.Create(SOCK_STREAM);
  _conn.SetNonBlocking(true);
  sin.sin_port=htons(port);
  if (theApp.proxy.sockenable)
	{ 
	SOCKADDR_IN psin;
	if (GetHostByName(theApp.proxy.address,psin)==false) return CLT_CONNECTFAILED;
	psin.sin_port=htons(theApp.proxy.port);
	if (_conn.Connect(psin)==false || _conn.Wait(CONNECTTIMEOUT,Socket::WaitWrite)!=Socket::WaitWrite) 
	  {
	  _conn.Close();
	  return CLT_CONNECTFAILED;
	  }
    unsigned char buff[20];
    buff[0]=4;
    buff[1]=1;
    buff[2]=port>>8;
    buff[3]=port & 0xFF;
    buff[4]=sin.sin_addr.S_un.S_un_b.s_b1;
    buff[5]=sin.sin_addr.S_un.S_un_b.s_b2;
    buff[6]=sin.sin_addr.S_un.S_un_b.s_b3;
    buff[7]=sin.sin_addr.S_un.S_un_b.s_b4;
    buff[8]=0;
	_tcpout.DataExchange(buff,9);
	_tcpin.DataExchange(buff,9);
    if (buff[1]!=90 || _tcpin.IsError())  
	  {
	  _conn.Close();
	  return CLT_CONNECTFAILED;
	  }
	_tcpin.DataExchange(buff,6);
	if (_tcpin.IsError()) return CLT_CONNECTFAILED;
	} 
  else
	{	
	if (_conn.Connect(sin)==false || _conn.Wait(CONNECTTIMEOUT,Socket::WaitWrite)!=Socket::WaitWrite) 
	  {
	  _conn.Close();
	  return CLT_CONNECTFAILED;
	  }
	}
  return CLT_OK;
  }


bool CLineTerminalNr::GetHostByName(const char *name, SOCKADDR_IN &host)
  {
  //fill requed header for SOCKADDR_IN
  host.sin_family=AF_INET; 
  //try get address from name, if it is "dotted numbers"
  host.sin_addr.s_addr = inet_addr(name);
  //return immediateli, if successful
  if (host.sin_addr.s_addr != INADDR_NONE) return true;

  //create notify window
  HWND wnd=CreateWindow("STATIC","Notify",0,0,0,10,10,NULL,NULL,AfxGetInstanceHandle(),0);
  char buffer[MAXGETHOSTSTRUCT];
  //request asynchronious GetHostByName
  COleDateTime laststamp=COleDateTime::GetCurrentTime();
  HANDLE h=WSAAsyncGetHostByName(wnd,WM_APP,name,buffer,MAXGETHOSTSTRUCT);
  MSG msg;
  //mark this stamp
  BOOL result;
  SetTimer(wnd,100,1000,NULL);
  //wait for complette operation
  do
	{
	do
	  {
	  WaitMessage();
	  if (::PeekMessage(&msg,0,WM_KEYDOWN,WM_KEYDOWN,PM_NOREMOVE)==TRUE)	
		if (msg.wParam==VK_ESCAPE)
		  {
		  stop=true;
		  ::PeekMessage(&msg,0,WM_KEYDOWN,WM_KEYDOWN,PM_REMOVE);
		  } 	
	  }
	while (PeekMessage(&msg,wnd,0,0,PM_REMOVE)==FALSE && !stop);
	if (msg.message==WM_KEYDOWN && msg.wParam==VK_ESCAPE) stop=true;
	else DispatchMessage(&msg);
	if (stop) break; //when user cancels operation exit
	if ((COleDateTime::GetCurrentTime()-laststamp)>COleDateTimeSpan(0,0,0,CONNECTTIMEOUT/1000)) break; //when timeout occured	
	}
  while (msg.message!=WM_APP);
  result=false;
  if (msg.message==WM_APP) //event has been returned
	{
	int error=WSAGETASYNCERROR(msg.lParam); //get error code
	if (error==0)  //noerror, get result
	  {
	  HOSTENT *pp=(HOSTENT *)buffer;
	  memcpy(&host.sin_addr.s_addr,pp->h_addr_list[0],4); 
	  result=true;
	  }
	else
	  result=FALSE;
	}  
  else
	WSACancelAsyncRequest(h); 
  //destroy notify window
  KillTimer(wnd,100);
  DestroyWindow(wnd);
  //flush thread queue
  while (::PeekMessage(&msg,wnd,WM_APP,WM_APP,PM_REMOVE)==TRUE);
  return result!=FALSE;
  }

