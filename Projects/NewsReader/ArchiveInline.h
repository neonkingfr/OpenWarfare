#ifndef _ARCHIVE_INLINE_OPERATOR_DEF_
#define _ARCHIVE_INLINE_OPERATOR_DEF_

#include "iarchive.h"

extern BTree<CString> sharedTable;

template <>
static inline void BTreeUnsetItem(CString &item)
{
  item="";
}


static inline void OptimizeString(BTree<CString> &table, CString &str)
{
  CString *fnd=table.Find(str);
  if (fnd) str=*fnd;
  else  table.Add((LPCTSTR)str);
}


static inline void ArchiveVariableExchange(IArchive &arch, CString &string)
  {
  if (arch.IsReading())
    {
    char *z=NULL;
    arch(z);
    string=z;
    OptimizeString(sharedTable,string);
    delete [] z;
    }
  else
    {
    char *c=const_cast<char *>((LPCTSTR)string);
    arch(c);
    }
  }

static inline void ArchiveVariableExchange(IArchive &arch, RString &string)
  {
  if (arch.IsReading())
    {
    char *z=NULL;
    arch(z);
    string=z;
    delete [] z;
    }
  else
    {
    char *c=const_cast<char *>((LPCTSTR)string);
    arch(c);
    }
  }

static inline void ArchiveVariableExchange(IArchive &arch, RStringB &string)
{
  RString sub;
  if (arch.IsStoring()) sub=string;
  ArchiveVariableExchange(arch,sub);
  if (arch.IsReading()) string=sub;
}


static inline void ArchiveVariableExchange(IArchive &arch, CTime &time)
  {  
  SYSTEMTIME systm;
  time.GetAsSystemTime(systm);
  arch("Year",systm.wYear);
  arch("Month",systm.wMonth);
  arch("Day",systm.wDay);
  arch("Hour",systm.wHour);
  arch("Minute",systm.wMinute);
  arch("Second",systm.wSecond);
  arch("Milliseconds",systm.wMilliseconds);
  TRY
  {  
    time=CTime(systm,0);
  }
  CATCH_ALL(e)
  {
    time=CTime(1980,1,1,0,0,0);
  }
  END_CATCH_ALL
  }

static inline void ArchiveVariableExchange(IArchive &arch, COleDateTime &time)
  {  
  SYSTEMTIME systm;
  time.GetAsSystemTime(systm);
  arch("Year",systm.wYear);
  arch("Month",systm.wMonth);
  arch("Day",systm.wDay);
  arch("Hour",systm.wHour);
  arch("Minute",systm.wMinute);
  arch("Second",systm.wSecond);
  arch("Milliseconds",systm.wMilliseconds);
  time=COleDateTime(systm);
  }



static inline void ArchiveVariableExchange(IArchive &arch, CTimeSpan &timespan)
  {
  time_t astimet=timespan.GetTotalSeconds();
  arch("timespan",astimet);
  timespan=CTimeSpan(astimet);  
  }

#endif