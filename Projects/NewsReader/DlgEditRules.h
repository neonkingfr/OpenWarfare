#pragma once

#include "ThreadRules.h"
#include "afxcmn.h"
#include "afxwin.h"

// DlgEditRules dialog

class DlgEditRules : public CDialog
{
	DECLARE_DYNAMIC(DlgEditRules)

  ThreadRules _rules;
  CImageListEx ilist;
  int _saveItem;

  SRef<CImageList> _dragImage;

public:
	DlgEditRules(const ThreadRules &rules, CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgEditRules();

// Dialog Data
	enum { IDD = IDD_EDITRULES };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnLvnGetdispinfoRulelist(NMHDR *pNMHDR, LRESULT *pResult);
  virtual BOOL OnInitDialog();
  CListCtrl wRuleList;
  afx_msg void OnLvnItemchangedRulelist(NMHDR *pNMHDR, LRESULT *pResult);
  CEdit wRule;
  afx_msg void OnEnChangeCondition();
  afx_msg void OnLvnSetdispinfoRulelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnEndlabeleditRulelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedDelete();
  afx_msg void OnBnClickedNewrule();

  const ThreadRules &GetRules() const {return _rules;}
  CButton wDelete;
  afx_msg void OnLvnBegindragRulelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnEnKillfocusCondition();
};
