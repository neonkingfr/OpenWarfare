// NewsArticle.cpp: implementation of the CNewsArticle class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "NewsTerminal.h"
#include "NewsAccount.h"
#include "NewsArticle.h"
#include "DogWindow.h"
#include "Archive.h"
#include "ArchiveInline.h"
#include "ArchiveStreamMemory.h"
#include "ArchiveStreamBase64Filter.h"
#include "MailHdrParser.h"
using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CReadWriteLock articleLock;

CNewsArticle::CNewsArticle()
  {
  ref=1;
  }

//--------------------------------------------------

CNewsArticle::~CNewsArticle()
  {
  }

//--------------------------------------------------

int CNewsArticle::DownloadArticle(int &index, CNewsTerminal &term, bool mode)
  {  
  if (index!=-1)
    {
    if (mode==true) term.SendFormatted("ARTICLE %d",index);
    else term.SendFormatted("HEAD %d",index);
    int p=term.ReceiveNumReply()/10;  
    if (p!=22) return p;
    }
  CString text;
  const char *line=term.ReadLine();
  int rdlines=0;
  int crlines=0;
  while (line && strcmp(line,".") && strcmp(line,""))
    {	
    text+=line;
    text+="\r\n";	
	if (strncmp(line,"Lines:",6)==0)
	  sscanf((LPCTSTR)(line)+6,"%d",&rdlines);
	line=term.ReadLine();
	if (line==NULL) return CLT_CONNECTIONRESET;
    CGdiObject::DeleteTempMap();
    }
  if (mode && rdlines) DogSetMax(rdlines);
  SetHeader(text);
  strstream ss;
//  ss.rdbuf()->setbuf(NULL,163840);
  crlines=0;
  while (line && strcmp(line,"."))
    {
    if (line[0]=='.') ss<<(line+1);else ss<<line;
    ss<<"\r\n";
	if (mode && rdlines) 
	  {
	  crlines++;
	  DogSetPos(crlines);
	  }
	line=term.ReadLine();
	if (line==NULL) return CLT_CONNECTIONRESET;
    CGdiObject::DeleteTempMap();
    }
  if (line==NULL) return CLT_CONNECTIONRESET;
  ss.put((char)0);
  SetText(ss.str());
  ss.rdbuf()->freeze(0);
  return 0;
  }

//--------------------------------------------------


static void MimeHeader(char *name)
  {
  char *src=name,*trg=name;
  if (name==NULL) return;
  while (*src)
    {
    if (src[0]=='=' && src[1]=='?')
      {	  
      char *p=strchr(src+2,'?');	  
      if (p!=NULL) 
        {
        p++;
        if (*p=='Q') 
          {
          p++;
          if (*p=='?') 
            {
            src=p+1;
            while (*src && !(src[0]=='?' && src[1]=='='))
              {
              if (*src=='=')
                {
                src++;
                int val=(*src>='A' && *src<='F')?*src-55:*src-48;
                val*=16;src++;
                val+=(*src>='A' && *src<='F')?*src-55:*src-48;
                src++;
                *trg++=(char)val;
                }
              else if (*src=='_') {*trg++=' ';src++;}
              else
                *trg++=*src++;
              }
            if (*src) src+=2;
            continue;
            }
          }
        else if (*p=='B')
          {
          p++;
          if (*p=='?') 
            {
            src=p+1;
            char *end=src;
            while (*end && !(end[0]=='?' && end[1]=='=')) end++;
            ArchiveStreamMemoryIn memin(src,end-src,false);
            ArchiveStreamBase64Filter base64(&memin);
            char c;
            base64.ExSimple(c);
            while (!base64.IsError()) 
              {
              *trg++=c;
              base64.ExSimple(c);
              }
            src=end;
            if (*src) src+=2;
            }
          }
        }
      }
    *trg++=*src++;
    }
  *trg=0;
  }



//--------------------------------------------------

void CNewsArtHolder::RescanHeader(CNewsArticle *art)
  {
    class HeaderEnum: public INewsArticleEnumCallback
    {
    protected:
      CNewsArtHolder *hld;
      virtual void HdrResult(const char *var, const char *value)
      {
        if (stricmp(var,ARTFROM)==0) hld->from=value;
        else if (stricmp(var,ARTSUBJECT)==0) hld->subject=value;
        else if (stricmp(var,ARTMESSID)==0) hld->messid=value;
        else if (stricmp(var,ARTDATE)==0) hld->newestdate=hld->date=ParseStringDate(value);
        else if (stricmp(var,ARTICON)==0) hld->icon=value;
        else if (stricmp(var,ARTLINES)==0) sscanf(value,"%d",&(hld->lines));
        else if (stricmp(var,ARTCONTTYPE)==0) hld->content=value;
        else if (stricmp(var,ARTXCANCEL)==0) hld->Xcancel=value;
        else if (stricmp(var,ARTXWIKI)==0) hld->XWikiLink=value;
        else if (stricmp(var,ARTREFID)==0) 
        {
          const char *c=strrchr(value,'<');
          hld->refid=c;
        }
      }
    public:
      HeaderEnum(CNewsArtHolder *hld):hld(hld) {}
    };

  HeaderEnum hdrenm(this);
  art->EnumHeader(&hdrenm);
  if (theApp.config.dwLines<=(unsigned)lines) attachment=NHATT_MAYBE;
  else attachment=NHATT_NONE;  
  MimeHeader(subject.LockBuffer());subject.UnlockBuffer();
  MimeHeader(from.LockBuffer());from.UnlockBuffer();
  }

//--------------------------------------------------

static int mycmp(const char *a, const char *b)
  {
  while (*a==*b && *a && *b)
    {
    TRACE2("%c%c",*a,*b);
    a++;
    b++;
    }
  //  TRACE2("%c%c",*a,*b);
  //  TRACE0("\n");
  return (*a>*b)-(*a<*b);
  }

//--------------------------------------------------

CNewsArtHolder * CNewsArtHolder::FindMessage(const CString &msid, bool deleted)
  {
  if (msid[0]==0) return NULL;  
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  CNewsArtHolder *tt=this;
  while (tt)
    {
    if (!tt->IsTrash())
      {
      //	TRACE2("%s %s",tt->messid,msid);
      if (deleted)
        {if (tt->Xcancel.GetLength())
            if (tt->Xcancel.Find(msid)!=-1) return tt;}
      else
        if (strcmp(tt->messid,msid)==0) return tt;
      if (tt->child) 
        {
        CNewsArtHolder *pp=tt->child->FindMessage(msid,deleted);
        if (pp) return pp;
        }
      }
    tt=tt->next;
    }
  return NULL;
  }

CNewsArtHolder * CNewsArtHolder::FindMessage(int index)
  {
  if (index==0) return NULL;
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  CNewsArtHolder *tt=this;
  while (tt)
    {
    //	TRACE2("%s %s",tt->messid,msid);
    if (tt->GetIndex()==index) return tt;
    if (tt->child) 
      {
      CNewsArtHolder *pp=tt->child->FindMessage(index);
      if (pp) return pp;
      }
    tt=tt->next;
    }
  return NULL;
  }

//--------------------------------------------------

void CNewsArticle::Serialize(CGenArchive &arch)
  {
  arch.Serialize(_header);
  arch.Serialize(_text);
  }

//--------------------------------------------------

void CNewsArtHolder::Serialize(CGenArchive &arch, int minindex)
  {
  char state;
  CNewsArtHolder *x=this;
  do
    {
    state=((x->next!=NULL) << 1) | (x->child !=NULL);
    arch.VarSerial(state);
    if (!arch.IsStoring)
      {
      if (state & 2) x->SetNext(new CNewsArtHolder());
      if (state & 1) x->SetChild(new CNewsArtHolder());
      x->dwnnew=false;	  
      }
    arch.Serialize(x->from);
    arch.Serialize(x->subject);
    arch.Serialize(x->messid);
    arch.Serialize(x->refid);
    if (arch.version>=0x108) arch.Serialize(x->content);
    if (arch.version>=0x101) 
      {
      arch.Serialize(x->icon);
      arch.VarSerial(x->lines);
      arch.VarSerial(x->attachment);
      if (x->attachment==-1) x->attachment=NHATT_ARCHIVE;
      }
    if (arch.version==0x114) arch.VarSerial(x->incache); //only this version has incache flag
    else if (!arch.IsStoring) x->incache=-1;
    x->dwnnew=false; //download articles as new;    
    if (arch.version>=0x105) 
      {
      bool op=!x->opened;
      arch.VarSerial(op);
      x->opened=!op;
      }    
    arch.VarSerial(x->index);   
    bool tmp;
    tmp=x->unread;
    arch.VarSerial(tmp);
    x->unread=tmp;
    arch.VarSerial(x->date);
    if (!arch.IsStoring) x->newestdate=x->date;
    if (arch.version>=0x106) arch.VarSerial(x->rdate);
    else if (!arch.IsStoring) x->rdate=x->date;
    arch.VarSerial(x->state);
    if (x->child) x->child->Serialize(arch,minindex);
    x=x->next;
    }
  while (x);
  }

//--------------------------------------------------

CNewsArtHolder * CNewsArtHolder::RebuildRemoveOld(int minindex)
  {
  if (this==NULL) return NULL;
  CNewsArtHolder *p=this;
  CNewsArtHolder *out=p;
  CNewsArtHolder *last=NULL;
  do
    {
    CNewsArtHolder *b=p->next;
    if (p->GetIndex()<minindex || p->trash)
      {
      CNewsArtHolder *z=p->GetChild();
      if (z!=NULL)
        {
        b=z;
        while (z->next!=NULL)
          {
          z->parent=p->parent;
          z=z->next;
          }
        z->parent=p->parent;
        z->next=p->next;
        if (last==NULL) out=b;else last->next=b;
        }
      else
        {
        if (last==NULL) out=p->next;else last->next=p->next;
        }
      CNewsArtHolder *pom=b;
      p->Unlink();
      p->child=NULL;
      if (p) p->Release();
      p=pom;	  
      }
    else
      {
      last=p;
      if (p->child) 
        {p->child=p->child->RebuildRemoveOld(minindex);}	    
      }
    p=b;
    }
  while (p!=NULL);
  return out;
  }

//--------------------------------------------------

int CNewsArtHolder::GetNewCount() const
  {
  int suma=0;
  const CNewsArtHolder *a=this;
  while (a)
    {
    if (!a->hidden)
      {
      if (a->GetUnread()) suma++;
      suma+=a->chldnew;
      }
    a=a->next;
    }
  return suma;
  }

//--------------------------------------------------

int CNewsArtHolder::GetHighlightedCount() const
  {
  int suma=0;
  const CNewsArtHolder *a=this;
  while (a)
    {
    if (!a->hidden)
      {
      if (a->GetHighlighted()) suma++;
      //suma+=a->chldnew;
      }
    a=a->next;
    }
  return suma;
  }

//--------------------------------------------------

const char *CNewsArtHolder::keywords=NULL;

static void GetKeyword(const char *from, int index, char *buff, int size)
  {
  buff[--size]=0;
  buff[0]=0;
  char *p=(char *)from;
  while (index--)
    {
    p=strchr(p,',');
    if (p==NULL) return;
    p++;
    }
  while (size && *p && *p!=',') 
    {*buff++=*p++;size--;}
  *buff=0;
  }

//--------------------------------------------------

static bool CompareKeywords(const char *keyword1,const char *keyword2)
  {
  char buff1[50],buff2[50];
  int i=0,j=0;
  do
    {
    j=0;
    GetKeyword(keyword1,i,buff1,sizeof(buff1));
    if (buff1[0])
      do
        {
        GetKeyword(keyword2,j,buff2,sizeof(buff2));
        if (buff2[0])
          if (stricmp(buff1,buff2)==0) return false;
        j++;
        }
    while (buff2[0]);
    i++;
    }
  while (buff1[0]);
  return true;
  }

//--------------------------------------------------

/*
  bool CNewsArtHolder::CalcHierarchy(bool rebuild,int level,CNewsArtHolder *parent)
  {
  CNewsArtHolder *a=this;
  bool isnew=false;
  while (a)
  {	
  //	a->childnew=unread;
  a->level=level;
  a->parent=parent;	
  a->hidden=false;
  if (strncmp(a->subject,CLOSETHREADSUBJECT,CLOSETHREADSUBJECTLEN)==0)	
  if (keywords && CompareKeywords(keywords,(LPCTSTR)(a->subject)+CLOSETHREADSUBJECTLEN))
		{a->hidden=true;a->unread=false;}
        isnew|=a->unread;
        if (a->child) 
        {
        bool isn;
        isnew|=isn=a->child->CalcHierarchy(rebuild,level+(a->IsArchive()?0:1),a);
        if (a->level!=0 && theApp.config.autoExpand || isn && theApp.config.newExpand) a->opened=true;	  
        else if (rebuild) a->opened=false;
        //	  childnew|=isn;
        }
        if (a->parent && a->hidden) a->parent->hidden=true;
        
          a=a->next;
          }
          return isnew;
          }
*/
void CNewsArtHolder::LoadArticle(CNewsArticle *art, unsigned long forceowner)
  {
  RescanHeader(art);
  theApp.Lock();
  
  PMessagePart msg;
  if (art->GetText()[0]!=0)
    {
    char *beg=(char *)art->GetText();
    CMessagePart *ptr=new CMessagePart();
    ptr->LoadMessage(istrstream(beg,0));
    ptr->InsertCache(forceowner?forceowner:MEMCACHEOWNER(this));  
    msg=memcache.LockObject(forceowner?forceowner:MEMCACHEOWNER(this));
    }    
  theApp.Unlock();
  CMessagePart *p=msg;
  while (p && p->GetType()==CMessagePart::text) p=p->GetNext();
  if (p) attachment=NHATT_ISATTACH;else attachment=NHATT_NONE;
  dwnnew=false;
  rdate=COleDateTime::GetCurrentTime();
  }

//--------------------------------------------------

int CNewsArtHolder::ReloadArticle(CNewsTerminal &term,CNewsGroup *grp, bool nocommands, unsigned long forceowner)
  {
  int result;  
  int ix;
  if (IsArchive()) 
    {
    theApp.Lock();
    CMessagePart *ptr;
    ptr=new CMessagePart();
    char buff[]="  ARCHIVE  \n";
    ptr->LoadMessage(istrstream(buff));
    theApp.Unlock();
    ptr->InsertCache(forceowner?forceowner:MEMCACHEOWNER(this));
    return 0;
    }
  if (!nocommands)
    {
    if (grp && index!=0) 
      {
      term.SendFormatted("GROUP %s",this->group->GetName());
      result=term.ReceiveNumReply();
      if (result/10!=21) return -2;
      }
    ix=index;
    }
  else
    ix=-1;
  if (index==0) 
    {
    term.SendFormatted("ARTICLE %s",this->messid);
    result=term.ReceiveNumReply();
    if (result/100!=2) return -2;
    ix=-1;
    }
  CNewsArticle *art=new CNewsArticle();
  if (art->DownloadArticle(ix,term,true)!=0) 
    {delete art;return -1;}
  LoadArticle(art,forceowner);
  delete art;
  return 0;
  }

//--------------------------------------------------

void CNewsArtHolder::CatchUp()
  {
  CNewsArtHolder *p=this;
  LogMarkAsRead(p->messid,false, 10);
  while (p)
    {
    p->unread=false;
    if (p->child && p->chldnew) 
      {
      p->child->CatchUp();
      p->chldnew=0;
      }
    p=p->next;
    }  
  }

//--------------------------------------------------

const char *stristr(const char *source, const char *fnd)
  {
  if (fnd[0]==0) return source;
  do
    {
    while (*source && toupper(*source)!=(toupper (*fnd))) source++;
    if (*source)
      {
      int q=1;
      while (fnd[q] && source[q]) 
        if (toupper(source[q])!=toupper(fnd[q])) break;else q++;
      if (fnd[q]==0) return source;
      source++;
      }
    }
  while (*source);
  return NULL;
  }

//--------------------------------------------------

/*!
  Match article headers, content may not be available yet
*/
/*
static bool MatchArticleHeaders(GRPFINDINFO &fnd, CNewsArtHolder *preload)
{
if (fnd.from && !fnd.from->Find(preload->from)) return false;
if (fnd.subject && !fnd.subject->Find(preload->GetSubject())) return false;
if (fnd.en_before && fnd.before>=preload->date) return false;
if (fnd.en_after && fnd.after<=preload->date) return false;
if (fnd.attach && !preload->attachment) return false;
return true;
}
*/


/*!
Calculate score of article headers
*/

static float ArticleHeadersScore(GRPFINDINFO &fnd, CNewsArtHolder *preload)
  {
  float sum=1.0f;
  if (fnd.en_before && fnd.before>=preload->date) return 0;
  if (fnd.en_after && fnd.after<=preload->date) return 0;
  if (fnd.attach && !preload->attachment) return 0;
  if (fnd.from && fnd.subject)
    {
    // if subject does not match, do not get score for from
    sum *= fnd.subject->GetScore(preload->GetSubject());
    if (sum>0.0f)
      {
      sum *= fnd.from->GetScore(preload->from);
      }
    }
  else
    {
    if (fnd.from) sum *= fnd.from->GetScore(preload->from);
    if (fnd.subject) sum *= fnd.subject->GetScore(preload->GetSubject());
    }
  return sum;
  }

//--------------------------------------------------

void CNewsArtHolder::Search(GRPFINDINFO &fnd, CNewsTerminal &term)
  {
  CNewsArtHolder *cur=this;
  CNewsArtHolder *preload=this;
  CFindList *&fndp=fnd.firstfnd;
  int w=0;
  memset(fnd.fifo,0,sizeof(fnd.fifo));
  CString line;
  if (term.IsConnected())   //Pokus jsme spojeni (hleda se na serveru)
  for (int i=0;i<fnd.maxfifo && preload;) //nejprve se poslou nejake pozadavky napred
    {
    float score = ArticleHeadersScore(fnd,preload);
    if (score>0.0f)      //stahuj prispevky ktere projdou testem na hlavicku
      {
      if (fnd.message && fnd.message->IsCompiled())  
        {
        if (fnd.cache!=NULL && fnd.cache->OpenMessage(preload->messid)!=NULL)
          {
          fnd.cache->CloseMessage(); //test na to, zda je article v cache
          }
        else
          {
          term.SendFormatted("ARTICLE %d",preload->GetIndex()); //posli pozadavek
          fnd.fifo[w]=preload;    //uloz pozadavek do fifo
          fnd.score[w] = score;
          w=(w+1)%fnd.maxfifo;
          i++;
          }
        }
      }
    preload=preload->EnumNext();    //jdi na dalsi
    }
  w=0;
  while (cur!=NULL)
    {
    float score=0.0f;
    float subscore = 1.0f; //score subjectu;
    if (fnd.subject==NULL || fnd.subject->IsCompiled()==FALSE) //hledani v subjektu
      if (fnd.message && fnd.message->IsCompiled())
        subscore=fnd.message->GetScore(cur->subject);
    if (fnd.fifo[w]==cur)         //pro prispeveke ve fifo - hlavicku mame prohledanou
      {
      score = fnd.score[w];  //zjisti score hlavicky
      fnd.fifo[w]=NULL;                   //vynuluj polozku ve fifo (prevzato)
      CNewsArticle art;
      int index=-1;                       
      fnd.msgindex=cur->GetIndex();   
      if (fnd.FndCallback)              //volani callback pro aktualizaci stavu hledani
      if (!fnd.FndCallback(fnd)) return;
      if (term.ReceiveNumReply()/10==22)  //prijmi odpoved ze serveru
      if (art.DownloadArticle(index,term,true)==0)  //pri uspechu stahni prispevek
        {
        float textscore = 0; //score textu;
        textscore = fnd.message->GetScore(art.GetText());    //normalni hledani
        score*=__max(textscore,subscore);
        }
      else
        score*=subscore;
      if (preload)                  //zde posilej dalsi pozadavky na server
        {
        bool chk=true;
        while (chk && preload)
          {
          float score = ArticleHeadersScore(fnd,preload);
          if (score>0.0f)
            {
            if (fnd.message && fnd.message->IsCompiled())
              {
              if (fnd.cache!=NULL && fnd.cache->OpenMessage(preload->messid)!=NULL)
                {
                fnd.cache->CloseMessage();
                }
              else
                {
                term.SendFormatted("ARTICLE %d",preload->GetIndex());
                chk=false;
                fnd.fifo[w]=preload;
                fnd.score[w] = score;
                }
              }
            }
          preload=preload->EnumNext();				  
          }
        }
      w=(w+1)%fnd.maxfifo;
      }					  
    else  //read: if (fnd.fifo[w]!=cur)  - prispevek je v cache nebo v pameti
      {
      score = ArticleHeadersScore(fnd,cur);  //vypocet hlavicky
      if (score>0.0f)
        {
        PMessagePart msgtext=cur->GetArticle();
        CMessagePart *a=msgtext;            //vezmi text article
        fnd.msgindex=cur->GetIndex();
        if (fnd.message && fnd.message->IsCompiled())
          {
          if (!a && fnd.cache)                    //pokud neni article, a je definovana cache
            {
            bool succ=cur->LoadFromCache(*fnd.cache,GetCurrentThreadId()); //najdi article v cache a nacti
            if (fnd.FndCallback) 
              if (!fnd.FndCallback(fnd)) return;  //aktualizuj status, popripade stop
            if (succ) a=msgtext=cur->GetArticle(GetCurrentThreadId());                  //ziskej odkaz na article
			else a=NULL;
            }       
          if (a)    
            {
			float textscore=0;
            while (a) //pro vsechny casti article
              {
              if (a->GetType()==CMessagePart::text)  //pokud se hleda v textu
                {			
				float tt= fnd.message->GetScore(a->GetData()); //hledej v textu
                if (tt>textscore) textscore=tt;  //ber vetsi vysledek ze vsech zjistenych
                }
              a=a->GetNext(); //vem dalsi cast prispevku
              }
            score*=__max(textscore,subscore);
			}
          else 
            score*=subscore;
          }
        }
      }
   if (score>0.0f)   //zarad, pokud je nalezeno
      {
      CFindList *p=new CFindList;  //zarad do vysledku
      p->next=fndp;
      p->grp=fnd.grp;
      p->hlda=cur;
	  cur->AddRef();
      p->accuracy = score;
      fndp=p;
      }
    cur=cur->EnumNext(); //vem dalsi prispevek
    }
  if (fnd.FndCallback) 
    if (!fnd.FndCallback(fnd)) return;
  }

//--------------------------------------------------

/* OBSOLETE FUNCTION 
  void CNewsArtHolder::RecursiveLoadAndSearch(GRPFINDINFO &fnd, CNewsTerminal &term, char mode)
  {
  CString line;
  int md=mode;
  CNewsArtHolder *lp=this;
  CFindList *&fndp=fnd.firstfnd;
  while (lp)
  {
		AverageCalc score = ArticleHeadersScore(fnd,lp);
        if (score>0.0f)
        {
        CMessagePart *a=lp->GetArticle();
        if (fnd.message && fnd.message->IsCompiled())
        {
        if (a==NULL && fnd.cache)
        {
        fnd.msgindex=lp->GetIndex();
        lp->LoadFromCache(*fnd.cache,false);		  
        if (fnd.FndCallback) 
        if (!fnd.FndCallback(fnd)) return;
        a=lp->GetArticle();
        }
        if (a)
        while (a)
        {
        if (a->GetType()==CMessagePart::text)
        if (fnd.message) 
        {
        score += fnd.message->GetScore(a->GetData());
        if (score>0.0f)
        {
        CFindList *p=new CFindList;
        p->next=fndp;
        p->grp=fnd.grp;
        p->hlda=lp;
        p->accuracy = score;
        fndp=p;
        }
        }
        a=a->GetNext();
        }
        else 
        {
        if (term.IsConnected())
        {
        if (fnd.fifo[fnd.index])
        {
        CNewsArticle art;
        int index=-1;
        fnd.msgindex=fnd.fifo[fnd.index]->GetIndex();
        if (fnd.FndCallback) 
        if (!fnd.FndCallback(fnd)) return;
        if (term.ReceiveNumReply(line)/10==22)
        if (art.DownloadArticle(index,term,true)==0)
        {
        if (fnd.message)
        {
        score += fnd.message->GetScore(art.GetText());
        if (score>0.0f)
        {
        CFindList *p=new CFindList;
        p->next=fndp;
        p->grp=fnd.grp;
        p->hlda=fnd.fifo[fnd.index];
        p->accuracy = score;
        fndp=p;
        }
        }		  
        }
        fnd.fifo[fnd.index]=NULL;
        }					  
        fnd.fifo[fnd.index]=lp;
        fnd.index++;
        if (fnd.index==fnd.maxfifo) fnd.index=0;
        term.SendFormatted("ARTICLE %d",lp->GetIndex());		  
        }
        }
        }
        else
        {
        CFindList *p=new CFindList;
        p->next=fndp;
        p->grp=fnd.grp;
        p->hlda=lp;
        p->accuracy=score;
        fndp=p;
        }		  
        }
        if (lp->child)
        {
        lp->child->RecursiveLoadAndSearch(fnd,term,mode+1);
        CFindList *fi=lp->child->RecursiveLoadAndSearch(fnd,term,mode+1);
        CFindList *en=fi;
        if  (en) 
        {
        while (en->next!=NULL) en=en->next;
        en->next=fndp;
        fndp=fi;
        }
        }
        lp=lp->next;
        }
        if (mode==0)
        while (fnd.fifo[fnd.index])
        {
        CNewsArticle art;
        int index=-1;
        fnd.msgindex=fnd.fifo[fnd.index]->GetIndex();
        if (fnd.FndCallback) 
        if (!fnd.FndCallback(fnd)) return;
        if (term.ReceiveNumReply(line)/10==22)
        if (art.DownloadArticle(index,term,true)==0)
        {
        AverageCalc score = fnd.score[fnd.index];
        if (fnd.message)
        {
        score += fnd.message->GetScore(art.GetText());
        if (score>0.0f)
        {
        CFindList *p=new CFindList;
        p->next=fndp;
        p->grp=fnd.grp;
        p->hlda=fnd.fifo[fnd.index];
        p->accuracy=score;
        fndp=p;
        }
        }		  
        }
        fnd.fifo[fnd.index]=NULL;
        fnd.index++;
        if (fnd.index==fnd.maxfifo) fnd.index=0;
        }	  
        
          if (fnd.FndCallback) 
          fnd.FndCallback(fnd);
          }
*/
static void CorrectName(CString &name)
  {
  for (int i=0;i<name.GetLength();i++)
    {
    char c=name[i];
    if (!__iscsym(c) && c!='.') name.SetAt(i,'_');
    }
  }

//--------------------------------------------------

static CCriticalSection CacheSection;

bool CNewsArtHolder::SaveToCache(CMessCache& chc, unsigned long forceowner)
  {
  CScoopCritSect sect(CacheSection);
  incache=false;
  PMessagePart ptr=memcache.LockObject(forceowner?forceowner:MEMCACHEOWNER(this));
  if (ptr.IsNull()) return false;
  iostream *ip=chc.WriteMessage(messid);
  if (ip==NULL) return false;
  CGenArchiveOut arch(*ip);
  ptr->Serialize(arch);
  chc.CloseMessage();  
  incache=true;
  return true;
  }

//--------------------------------------------------

bool CNewsArtHolder::LoadFromCache(CMessCache& chc,  unsigned long forceowner)
  {
  CScoopCritSect sect(CacheSection);
  iostream *ip=chc.OpenMessage(messid);
  if (ip==NULL) 
    {
    incache=false;
    return false;
    }
  CGenArchiveIn arch(*ip);
  CMessagePart *ptr=new CMessagePart();
  ptr->Serialize(arch);
  chc.CloseMessage();
  if (!ptr->IsValid()) 
    {
    ::DogShowErrorV(IDS_INVALIDCACHEDETECTED,subject);
    delete ptr;
    ptr=NULL;
    return false;
    }
  ptr->InsertCache(forceowner?forceowner:MEMCACHEOWNER(this));  
  incache=true;
  return true;
  }


//--------------------------------------------------

CString CNewsArtHolder::GetSubject()
  {
  return subject;
  /* if (strncmp(subject,CLOSETHREADSUBJECT,CLOSETHREADSUBJECTLEN)==0 && parent) return parent->GetSubject();
  else return subject;*/
  }

//--------------------------------------------------

int CNewsArtHolder::CompareItems(CNewsArtHolder *left, CNewsArtHolder *right, int what)
  {
  int diff = 0;
  switch (what)
    {
    case 0:
    diff = (left->attachment>right->attachment)-(left->attachment<right->attachment);
    break;
    case 1:
      {
      // compare based on importance
      // if article is highlighted, assume it is more important than normal,
      // but less imortant then EAImportant
      int lScore = left->state;
      int rScore = right->state;
      if (lScore>=EAImportant) lScore++;
      else if (left->GetHighlighted()) lScore = EAImportant;
      if (rScore>=EAImportant) rScore++;
      else if (right->GetHighlighted()) rScore = EAImportant;
      diff = rScore-lScore;
      break;
      }
    case 2:
    diff = stricmp(left->subject,right->subject);
    break;
    case 3:
      {
      CString outl, outr;
      GetNameFrom(left->from,outl);
      GetNameFrom(right->from,outr);
      diff = stricmp(outl,outr);
      break;
      }
    case 4:
    break;
    case 16:   return (left->date<right->date)-(left->date>right->date);
    default: return (left->GetIndex()>right->GetIndex())-(left->GetIndex()<right->GetIndex());
    }
  // if score based on primary criterion is the same, sort by date
  if (diff) return diff;
  return (left->newestdate<right->newestdate)-(left->newestdate>right->newestdate);
  }

//--------------------------------------------------

/*
static int qsortcontext;

static int qsortcompareproc(const void *left, const void *right)
  {
  int p=CNewsArtHolder::CompareItems(*(CNewsArtHolder **)left,*(CNewsArtHolder **)right,qsortcontext & 0x7f);
  if (qsortcontext & 0x80) p=-p;
  return p;
  }

*/

static CNewsArtHolder *Slucuj(CNewsArtHolder *left, CNewsArtHolder *right, int by)
  /* Funkce Slucuj slouci dva zretezene seznamy - levy a pravy podle kriteria _by_
Slucovani predpoklada, ze oba seznamy jsou uz serazene
Vysledkem slouceni je novy zretezeny seznam tez serazeny
*/
  {
  CNewsArtHolder *z,*q;
  int res;
  res=q->CompareItems(left,right,by & 0x7f);  //porovnej prvni dva prvky
  if (by & 0x80)
    {
    res=-res;                   //otoceni razeni
    }
  if (res==0) 
    res=left->sortpos-right->sortpos; //pokud jsou stejne, rozhoduje puvodni poradi
    if (res<=0) 
      {z=left;left=left->GetNext();}else 
      {z=right;right=right->GetNext();}
  //^^ podle vysledku zacni bud levym nebo pravym a v tom seznamu se posun na dalsi
  z->Unlink();  //vyjmi vybrany prvek ze seznamu
  q=z;          //ukazatel pro pridavani dalsich prvku do seznamu
  while (left && right) //dokud je neco vlevo i vpravo
    {
    res=q->CompareItems(left,right,by & 0x7f);  //porovnej prvni prvky
    if (by & 0x80)
      {
      res=-res;
      }
    if (res==0) 
      res=left->sortpos-right->sortpos; 
    if (res<=0)     //pokud je mensi levy
      {
      q->SetNext(left);   //pridej levy prvek do noveho seznamu
      left=left->GetNext(); //a posun se na dalsi
      }
    else          //pokud je mensi pravy
      {
      q->SetNext(right);   //pridej pravy prvek do noveho seznamu
      right=right->GetNext(); //a posun se na dalsi
      }
    q=q->GetNext();     //posun se na nove pridany prvek
    q->Unlink();        //vyjmi ho ze seznamu
    }
  if (left) q->SetNext(left);else q->SetNext(right); //zbyvajici seznam pridej na konec
  return z; //vrat novy seznam serazeny
  }

//--------------------------------------------------

static void MergeSort(CNewsArtHolder **src, int count,int by)
  {
  int pn,pp;
  while (count>1)         //opakuj, dokud neexistuje jen jeden zretezeny seznam
    {
    for (pp=0,pn=0;pp<count;pp+=2,pn++) //pp zdroj index, pn cil index
      {
      if (pp+1>=count) src[pn]=src[pp]; //jednu polozku neni s cim porovnat
      else src[pn]=Slucuj(src[pp],src[pp+1],by); //porovnej a sluc dva zretezene seznamy
      }  //opakuj pro vsechny dvojice, vznikaji zretezene seznamy
    count=pn; //count je novy pocet zretezenych seznamu
    }  
  //zretezeny seznam je serazen.
  }

//--------------------------------------------------

CNewsArtHolder *CNewsArtHolder::Sort(int by)
  {
  CNewsArtHolder *x=this;
  int cnt=0;
  while (x)       //spocitej pocet prispevku ve vlakne a zaroven setrid podvlakna
    {
    if (x->child) x->child=x->child->Sort(0x84);
    cnt++;    //tady se pocitaji prispevky ve vlakne
    x=x->next;
    }
  if (cnt<2) return this;   //pokud je tam mene nez dva prispevky, neni co resit
  CNewsArtHolder **block=cnt<1000?
    ((CNewsArtHolder **)alloca(sizeof(CNewsArtHolder *)*cnt)):
  (new CNewsArtHolder *[cnt]);
  //Vyrob pomocne pole prispevku - pro pocet prispevku mensi nez 1000 v zasobniku, jinak na halde
  
  x=this;   
  CNewsArtHolder *frst=x;           //zacni prvnim
  int i=0;                          
  while (x)
    {
    block[i]=x;                 //umistuj prispevky do pomocneho pole
    x->sortpos=i;               //sortpos je poradi prispevku pred tridenim - pro...
    CNewsArtHolder *p=x->next;  //...zabezpeceni stability trideni
    x->Unlink();               //vyjmi prispevek ze struktury (bezpecne vyjmuti)
    x=p;                        
    i++;
    }
  
  /* pokud behem trideni jine vlakno prochazi prispevky, muze bud projit uspesne, nebo narazi
    na NULL. Kazde prochazeni musi byt pri NULL ukonecno!!!
    
      Ve vyslednem poli jsou ulozeny prispevky samostatne, a kazdy ma v next NULL
  */
  
  MergeSort(block,cnt,by); //provadi rekunzivni Merge Sort
  frst=block[0];           //vysledkem Merge Sortu je spojovy seznam na prvnim prvku
  if (cnt>=1000) delete [] block;         //zlikviduj pomocne pole
  return frst;
  }

//--------------------------------------------------

CNewsArtHolder * CNewsArtHolder::EnumNextNoChild()
  {
  if (this==NULL) return NULL;
  if (next) return next;
  CNewsArtHolder *p=parent;
  while (p!=NULL)
    {
    if (p->next) return p->next;
    p=p->parent;
    }
  return p;
  }

//--------------------------------------------------

CNewsArtHolder *CNewsArtHolder::createArchive(CNewsArtHolder *hp)
  {
  if (hp!=NULL) 
    {
    if (hp->IsArchive()) return hp;
    while (hp->GetNext())
      {
      hp=hp->GetNext();
      if (hp->IsArchive()) return hp;
      }
    if (hp->IsArchive()) return hp;
    }
  CNewsArtHolder *q=new CNewsArtHolder();
  CString archname;
  archname.LoadString(IDS_ARCHIVE);
  q->subject=(LPCTSTR)archname;
  q->attachment=NHATT_ARCHIVE;
  if (hp) 
    {
    while (hp->next!=NULL) hp=hp->next;
    hp->next=q;
    }
  else 
    hp=q;
  q->lines=0;		
  q->index=0x7FFFFFFF;
  q->MarkAsRead(8);
  return q;
  }

//--------------------------------------------------

CNewsArtHolder * CNewsArtHolder::Archivate(COleDateTime archtime)
  {
  if (this==NULL) return NULL;
  CNewsArtHolder **arch=NULL;
  CNewsArtHolder *cur=this,*prev=NULL, *ret=this;
  while (cur)
    {
    if (!cur->IsArchive())
      if (cur->date.GetStatus()!=COleDateTime::valid || archtime>cur->date)
        {
        if (arch==NULL) 
          {
          CNewsArtHolder *aar=createArchive(ret);
          aar->date=archtime;
          arch=&aar->child;
          while (*arch) 
            {aar=*arch;arch=&aar->child;}
          }	  
        CNewsArtHolder *nx=cur->next;
        if (prev) prev->next=nx;
        else ret=nx;
        cur->next=NULL;
        *arch=cur;
        arch=&cur->next;
        cur=nx;
        }
    else
      {	  
      prev=cur;
      cur=cur->next;
      }	
    else
      {
      prev=cur;
      cur=cur->next;
      }	
    }  
  return ret;
  }

//--------------------------------------------------

void CNewsArtHolder::DeleteOlder(COleDateTime &dtm)
  {
  CNewsArtHolder *p=this;
  while (p)
    {
    if (p->date.GetStatus()==COleDateTime::valid &&dtm>p->date)
      {
      p->SetTrash();
      }
    p=p->next;
    }
  }

//--------------------------------------------------

void CNewsArtHolder::CalcHierarchy2(CALCHIERARCHYSTRUCT *info)
  {
  CNewsArtHolder *a=this,*parent=info->parent;
  bool isnew=false;
  while (a)
    {
    if (info->curlevel==0) info->hidflag=false;
    a->parent=parent;
    a->level=info->curlevel;
    a->chldnew=0;
    if (info->group) a->group=info->group;
    //a->highlighted=false;
    if (a->IsArchive()) a->SetTrash(true);
    if (parent)
      {
      if (a->unread) 
        parent->chldnew++;
      if (a->state<parent->state) a->state=parent->state;
      a->ParentPropagate();

      }
    if (a->child)
      {
      if (!a->IsArchive()) info->curlevel++; //don't enter level if it is a archive
      info->parent=a;  //set parent 
      a->child->CalcHierarchy2(info); //recursive calc hierarchy
      if (!a->IsArchive()) info->curlevel--; else info->hidflag=false;//don't leave level if it was a archive	  
      if (a->level!=0 && a->chldnew) a->opened=true; //open inner threads
      else if (theApp.config.newExpand && a->chldnew) a->opened=true; //open thread if there is new article
      else if (info->close && a->level==0) a->opened=false; //close thread in case of compact
      if (parent) 
        {
        parent->chldnew+=a->chldnew; //sumarize count of new articles	  
        a->ParentPropagate();
        }
      }
    //a->hidden=false;
    //a->highlighted=false;
    a=a->next;
    }
  }

//--------------------------------------------------

bool CNewsArtHolder::CalcHierarchy2(CNewsGroup *grp,const char *kwdlist, int setindex, bool close)
  {
  CALCHIERARCHYSTRUCT st;
  st.group=grp;
  st.close=close;
  st.curlevel=0;
  /*  if (kwdlist!=NULL)
  {
  char *lastn=(char *)kwdlist;
  char *q=lastn-1;
  do
  {
  lastn=q+1;
  q=strchr(lastn,'\n');
  if (q==NULL) {q=strchr(lastn,0);break;}
  }
  while (*q!=0 && setindex--);
  int sz=(q-lastn);
  char *p=(char *)alloca(sz+1);
  memcpy(p,lastn,sz);
  p[sz]=0;
  st.keyword=p;
  }
  else*/
  st.keyword=NULL;
  st.parent=NULL;  
  CalcHierarchy2(&st);
  return true;
  }

//--------------------------------------------------

CString CNewsArtHolder::GetLocalProperties(bool virtual_properties)
  {
  // check all direct children
  CString out;
  if (!child)
    {
    return out;
    }
  bool unread = chldnew>0;
  CNewsArtHolder *found = NULL;
  for (CNewsArtHolder *walk = child; walk; walk = walk->next)
    {
    int sq = walk->subject.Find(SUBTHREADPROPMARK);
    if (sq>=0)
      {
      // some local properties found - apply them to the parent
      found = walk;
      }
    }
  if (!found) return out;
  int sq = found->subject.Find(SUBTHREADPROPMARK);
  if (sq>=0)
    {
    out = found->subject.Mid(sq+strlen(SUBTHREADPROPMARK));
    }
  if (virtual_properties)
    {
    if (out.IsEmpty())
      {
      out = "_noprop";
      }
    if (unread) 
      {out+=",_unread";}
    }
  return out;
  }

//--------------------------------------------------

/*!
  \param localProp [out] true when any more local properites were found
*/
CString CNewsArtHolder::GetThreadProperties(bool virtual_properties, bool *localProp)
  {
  if (localProp) *localProp = false;
  CNewsArtHolder *top=this,*found=NULL, *traverse, *end;
  while (top->parent!=NULL && !top->parent->IsArchive()) top=top->parent;
  traverse=top;
  bool funread=false;
  end=top->EnumNextNoChild();
  while (traverse && traverse!=end)
    {
    ASSERT(traverse!=NULL);
    if (localProp && traverse!=top)
      {
      int sq=traverse->subject.Find(SUBTHREADPROPMARK);
      if (sq!=-1)
        {
        *localProp = true;
        }
      }
    int q=traverse->subject.Find(THREADPROPMARK);
    if (q!=-1)
      {
      if (found==NULL || found->date<traverse->date) found=traverse;
      }
    funread|=traverse->unread;
    traverse=traverse->EnumNext();
    }
  CString out;
  if (found)
    {
    int q=found->subject.Find(THREADPROPMARK);
    if (q!=-1) 
      {
      out = found->subject.Mid(q+strlen(THREADPROPMARK));
      }
    }
  if (virtual_properties)
    {
    if (out.IsEmpty()) 
      {out="_noprop";}
    if (funread) 
      {out+=",_unread";}
    if (top->state==EAImportant) 
      {out+=",_watched";}
    if (top->state==EAHidden) 
      {out+=",_ignored";}
    } 
  return out;
  }

//--------------------------------------------------

void CNewsArtHolder::HighlightSubthread(bool state)
  {
  CNewsArtHolder *traverse=this;
  
  CNewsArtHolder *end=EnumNextNoChild();
  while (traverse!=end && traverse) //ukazuje se, ze je dobre testovat i NULL
    {
    traverse->highlighted = state;
    traverse=traverse->EnumNext();
    }
  }

//--------------------------------------------------

void CNewsArtHolder::HighlightThread(bool state)
  {
  // mark all articles in the thread
  CNewsArtHolder *top=this;
  
  while (top->parent!=NULL && !top->parent->IsArchive()) top=top->parent;
  
  top->HighlightSubthread(state);
  
  }

//--------------------------------------------------

void CNewsArtHolder::HideSubthread(bool state)
  {
  CNewsArtHolder *traverse=this;
  
  CNewsArtHolder *end=EnumNextNoChild();
  while (traverse!=end && traverse) //ukazuje se, ze je dobre testovat i NULL
    {
    traverse->hidden = state;
    traverse=traverse->EnumNext();
    }
  }

void CNewsArtHolder::DeleteSubthread()
  {
  CNewsArtHolder *traverse=this;
  
  CNewsArtHolder *end=EnumNextNoChild();
  while (traverse!=end && traverse) //ukazuje se, ze je dobre testovat i NULL
    {
    traverse->trash=true;
    traverse=traverse->EnumNext();
    }
  }

//--------------------------------------------------

void CNewsArtHolder::HideThread(bool state)
  {
  CNewsArtHolder *top=this;//,*traverse;
  while (top->parent!=NULL && !top->parent->IsArchive()) top=top->parent;
  HideSubthread(state);
  }

void CNewsArtHolder::DeleteThread()
  {
  CNewsArtHolder *top=this;//,*traverse;
  while (top->parent!=NULL && !top->parent->IsArchive()) top=top->parent;
  DeleteSubthread();
  }

//--------------------------------------------------


CNewsArtHolder * CNewsArtHolder::Streaming(CNewsArtHolder *top, IArchive &arch, int minindex)
  {
  CNewsArtHolder *ret=NULL,*pos;
  if (!arch) return ret;
  ArchiveSection archsect(arch);
  while (archsect.Block("ArticleNode"))
    {
    if (arch.IsReading())
      {
      int cnt;
      arch("Count",cnt);
      for (int i=0;i<cnt;i++) 
        {
        CNewsArtHolder *cur=new CNewsArtHolder();
	    if (ret==NULL) ret=pos=cur;else pos->SetNext(cur);
	    pos=cur;
        arch.vSectionData=i;
	    cur->Streaming(arch,minindex);
	    if (!arch) return ret;
        }
      }
    else
      {
      int cnt=0;    
      CNewsArtHolder *cur,*end=top==NULL?NULL:top->parent->EnumNextNoChild();
      for (cur=top;cur && cur!=end;)
        {
        if (cur->trash || cur->index<minindex) cur=cur->EnumNext();
        else {cnt++;cur=cur->EnumNextNoChild();}
        }
      arch("Count",cnt);
      int i=0;
      for (cur=top;cur && cur!=end;)
        {
        if (cur->trash || cur->index<minindex) cur=cur->EnumNext();
        else 
          {
          arch.vSectionData=i++;
          cur->Streaming(arch,minindex);
          cur=cur->EnumNextNoChild();
          }
        }
      ret=top;
      }
    }
  return ret;
  }

void CNewsArtHolder::Streaming(IArchive &arch, int minindex)
  {
  ArchiveSection archsect(arch);
  while (archsect.Block("Article"))
    {	
    arch("from",from);
    arch("subject",subject);
    arch("messageid",messid);
    arch("reference",refid);
    arch("content-type",content);
    if (arch.IsVer(0x203))   arch("xcancel",Xcancel);
    if (arch.IsVer(0x205))   arch("xwikilink",XWikiLink);
    arch("icon",icon);
    lines=arch.RValue("lines",(int)lines);
    arch("attachment",attachment);
    opened=arch.RValue("expanded",opened,false);
    unread=arch.RValue("unread",unread,false);
    arch("index",index);
    arch("date",date);
    if (arch.IsReading()) newestdate=date;
    arch("last-read-date",rdate);
    state=(EArticleState)arch.RValue("state",(int)state);
    child=Streaming(child,arch,minindex);
    dwnnew=false; 
    }
  }


CNewsArtHolder::CNewsArtHolder(const CNewsArtHolder &other,bool tracenext,CNewsArtHolder  *ptrparent)
{
  child=other.child?new CNewsArtHolder(*other.child,true,this):NULL;
  chldnew=other.chldnew;
  content=other.content;
  date=other.date;
  dwnnew=other.dwnnew;
  from=other.from;
  group=other.group;
  hidden=other.hidden;
  hidtoshow=other.hidtoshow;
  highlighted=other.highlighted;
  icon=other.icon;
  incache=other.incache;
  index=other.index;
  indexed=other.indexed;
  level=other.level;
  lgwarn=other.lgwarn;
  lines=other.lines;
  messid=other.messid;
  newestdate=other.newestdate;
  if (tracenext)
    next=other.next?new CNewsArtHolder(*other.next,true,ptrparent):NULL;
  else
    next=NULL;
  ntnew=other.ntnew;
  opened=other.opened;
  parent=ptrparent;
  rdate=other.rdate;
  refcnt=1;
  refid=other.refid;
  sortpos=other.sortpos;
  state=other.state;
  subchanged=other.subchanged;
  subject=other.subject;
  trash=other.trash;
  unread=other.unread;
  Xcancel=other.Xcancel;
}

void CNewsArticle::EnumHeader(INewsArticleEnumCallback *callback)
{
  class Parser: public CMailHdrParser
  {
    INewsArticleEnumCallback *callback;
  public:
    Parser(INewsArticleEnumCallback *callback):callback(callback) {}
    void HdrResult(const char *command, const char *data)
    {
      callback->HdrResult(command,data);
    }
  };

  Parser p(callback);
  p.ParseHeader(GetHeader());
}

void CNewsArtHolder::ParentPropagate()
{
  if (parent)
  {
    if (parent->newestdate<newestdate) 
    {
      parent->newestdate=newestdate;
      if (XWikiLink.GetLength()!=0)
      {
        parent->XWikiLink=XWikiLink;
        XWikiLink="";
      }
    }
  }
}