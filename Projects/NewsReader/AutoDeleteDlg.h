#if !defined(AFX_AUTODELETEDLG_H__2FF2A9E3_C475_4212_A2FC_2A84C908AE31__INCLUDED_)
#define AFX_AUTODELETEDLG_H__2FF2A9E3_C475_4212_A2FC_2A84C908AE31__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AutoDeleteDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAutoDeleteDlg dialog

#include "NewsAccount.h"

class CAutoDeleteDlg : public CDialog
{
// Construction
public:
	CAutoDeleteDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAutoDeleteDlg)
	enum { IDD = IDD_AUTODELETE };
	CEdit	wDelInterval;
	CListCtrl	wGroupList;
	CComboBox	wFilter;
	CButton	wDelEnabled;
	BOOL	vDelEnabled;
	UINT	vDayInterval;
	BOOL	vAutoMark;
	//}}AFX_DATA
    CNewsAccount *vAcc;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAutoDeleteDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

    CNewsGroup *vGroupList;

// Implementation
protected:
    void SetItemTextAutoArrange(int line, int col, const char *text);
    void EditItem(int item);
    void DialogRules();
	// Generated message map functions
	//{{AFX_MSG(CAutoDeleteDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedGrouplist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeleteenabled();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTODELETEDLG_H__2FF2A9E3_C475_4212_A2FC_2A84C908AE31__INCLUDED_)
