// DiskIndex2.cpp: implementation of the CDiskIndex2 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "DiskIndex2.h"
using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDiskIndex2::CDiskIndex2()
  {
  
  }

//--------------------------------------------------

CDiskIndex2::~CDiskIndex2()
  {
  
  }

//--------------------------------------------------

bool CDiskIndex2::BlockChunk::Load(iostream &io, unsigned long pos)
  {
  loadpos=(unsigned long)-1;
  io.seekg(pos);
  io.read((char *)chunks,sizeof(chunks));
  if (io.gcount()!=sizeof(chunks) || !io) 
    {
    io.clear();
    return false;
    }
  loadpos=pos;
  return true;
  }

//--------------------------------------------------

bool CDiskIndex2::BlockChunk::Save(iostream &io)
  {
  if (loadpos==(unsigned long)-1) return false;
  io.seekp(loadpos);
  io.write((char *)chunks,sizeof(chunks));
  if (!io) 
    {
    io.clear();
    return false;
    }
  return true;
  }

//--------------------------------------------------

bool CDiskIndex2::BlockEChunk::Load(iostream &io, unsigned long pos)
  {
  loadpos=(unsigned long)-1;
  io.seekg(pos);
  io.read((char *)&endchunk,sizeof(endchunk));
  if (io.gcount()!=sizeof(endchunk) || !io) 
    {
    io.clear();
    return false;
    }
  loadpos=pos;
  return true;
  }

//--------------------------------------------------

bool CDiskIndex2::BlockEChunk::Save(iostream &io)
  {
  if (loadpos==(unsigned long)-1) return false;
  io.seekp(loadpos);
  io.write((char *)&endchunk,sizeof(endchunk));
  if (!io) 
    {
    io.clear();
    return false;
    }
  return true;
  }

//--------------------------------------------------

CDI_Errors CDiskIndex2::Find(const char *key, BlockEChunk &info, BlockChunk &prevchunk,int &prevchunkindex, int &bitindex)
  {
  unsigned long offset=0;
  bitindex=0;
  while (prevchunk.Load(_indexfile,offset) && (bitindex>>3)<CDI_INDEX_MAXSIZE)
    {
    int value=GetChunkPosFromKey(key,bitindex);
    IndexChunk &idx=prevchunk.chunks[value];
    prevchunkindex=value;
    if (idx.mode==CHK_Empty) return CDI_NotFound;      
    if (idx.mode==CHK_EndChunk)
      {
      offset=idx.levelpos;
      if (info.Load(_indexfile,offset)==false) return CDI_DiskReadError;
      if (strncmp(key,info.endchunk.fullkey,CDI_INDEX_MAXSIZE)==0) return CDI_OK;
      return CDI_NotFound;
      }
    offset=idx.levelpos;
    bitindex+=CDI_BLOCK_BITS;
    }
  return CDI_DiskReadError;
  }

//--------------------------------------------------

void CDiskIndex2::Close()
  {
  _indexfile.close();
  }

//--------------------------------------------------

bool CDiskIndex2::Open(const char *indexfilename)
  {
  _indexfile.clear();
  _indexfile.open(indexfilename,ios::in|ios::out|ios::binary);
  if (!_indexfile)
  {
    _indexfile.clear();
    _indexfile.open(indexfilename,ios::in|ios::out|ios::binary|ios::trunc);
  }
  if (!_indexfile)
    return false;

  _indexfile.seekp(0,ios::end);
  if (_indexfile.tellp().operator ==(0))
    {
    BlockChunk chunk;
    memset(&chunk,0,sizeof(chunk));
    chunk.Save(_indexfile);
    }
  return (!(!_indexfile));
  }

//--------------------------------------------------

CDI_Errors CDiskIndex2::Find(const char *key, unsigned long *seekpos, long *size)
  {
  BlockEChunk endchunk;
  BlockChunk prevchunk;
  int bitindex;
  int chunkindex;
  CDI_Errors result=Find(key,endchunk,prevchunk,chunkindex,bitindex);
  if (result!=CDI_OK) return result;
  if (seekpos) *seekpos=endchunk.endchunk.seekpos;
  if (size) *size=endchunk.endchunk.size;
  return result;
  }

//--------------------------------------------------

int CDiskIndex2::GetChunkPosFromKey(const char *key, int bitindex)
  {
  int byteindex=bitindex>>3;
  unsigned short value=(unsigned)key[byteindex]*256;
  if (value!=0 && byteindex+1<CDI_INDEX_MAXSIZE) value+=(unsigned)key[byteindex+1];
  value<<=bitindex & 0x7;
  value>>=16-CDI_BLOCK_BITS;
  return value;
  }

//--------------------------------------------------

CDI_Errors CDiskIndex2::AddKey(const char *key, unsigned long seekpos, long size)
  {
  BlockEChunk endchunk;
  BlockChunk prevchunk;
  int bitindex;
  int chunkindex;
  CDI_Errors result=Find(key,endchunk,prevchunk,chunkindex,bitindex);
  if (result==CDI_OK)
    {
    endchunk.endchunk.seekpos=seekpos;
    endchunk.endchunk.size=size;
    endchunk.Save(_indexfile);
    return result;
    }
  if (result==CDI_NotFound)
    {
    while (prevchunk.chunks[chunkindex].mode==CHK_EndChunk)
      {
      if (MoveChunkToNextLevel(prevchunk,chunkindex,endchunk,bitindex)==false) return CDI_DiskWriteError;
      bitindex+=CDI_BLOCK_BITS;
      chunkindex=GetChunkPosFromKey(key,bitindex);
      }
    _indexfile.seekp(0,ios::end);
    endchunk.loadpos=_indexfile.tellp();
    strncpy(endchunk.endchunk.fullkey,key,CDI_INDEX_MAXSIZE);
    endchunk.endchunk.seekpos=seekpos;
    endchunk.endchunk.size=size;
    if (endchunk.Save(_indexfile)==false) return CDI_DiskWriteError;
    prevchunk.chunks[chunkindex].levelpos=endchunk.loadpos;
    prevchunk.chunks[chunkindex].mode=CHK_EndChunk;
    if (prevchunk.Save(_indexfile)==false) return CDI_DiskWriteError;
    return CDI_OK;
    }
  return result;
  }

//--------------------------------------------------

bool CDiskIndex2::MoveChunkToNextLevel(BlockChunk &chunk, int pos, BlockEChunk &endchk, int bitindex)
  {
  BlockChunk newchunk;
  int nwbt=bitindex+CDI_BLOCK_BITS;
  if ((nwbt>>3)>=CDI_INDEX_MAXSIZE) return false;
  if (chunk.chunks[pos].mode!=CHK_EndChunk) return false;
  
  int newpos=GetChunkPosFromKey(endchk.endchunk.fullkey,nwbt);
  memset(&newchunk,0,sizeof(newchunk));
  newchunk.chunks[newpos].levelpos=endchk.loadpos;
  newchunk.chunks[newpos].mode=CHK_EndChunk;
  _indexfile.seekp(0,ios::end);
  newchunk.loadpos=_indexfile.tellp();
  if (newchunk.Save(_indexfile)==false) return false;
  
  chunk.chunks[pos].levelpos=newchunk.loadpos;
  chunk.chunks[pos].mode=CHK_NextLevel;
  if (chunk.Save(_indexfile)==false) return false;  
  chunk=newchunk;
  return true;
  }

//--------------------------------------------------

void CDiskIndex2::Test()
  {
  char pokusnyklic[]="TOTO JE POKUSNY KLIC";
  char zzz[32];
  CDiskIndex2 diskindex;
  diskindex.Open("c:\\testovani\\testindex.idx");
  diskindex.AddKey(pokusnyklic,12345,32);
  for (int i=0;i<10024;i++)
    {
    int j;
    for (j=0;j<31;j++) zzz[j]=(rand()&31)+64;
    zzz[j]=0;
    diskindex.AddKey(zzz,i,32);
    }
  }

//--------------------------------------------------

CDI_Errors CDiskIndex2::DeleteKey(const char *key)
  {
  BlockEChunk endchunk;
  BlockChunk prevchunk;
  int bitindex;
  int chunkindex;
  CDI_Errors result=Find(key,endchunk,prevchunk,chunkindex,bitindex);
  if (result==CDI_OK)
    {
    prevchunk.chunks[chunkindex].mode=CHK_Empty;
    if (prevchunk.Save(_indexfile)==false) return CDI_DiskWriteError;
    }
  return result;
  }

//--------------------------------------------------

