// AskWorkMode.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "AskWorkMode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAskWorkMode dialog


CAskWorkMode::CAskWorkMode(CWnd* pParent /*=NULL*/)
	: CDialog(CAskWorkMode::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAskWorkMode)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAskWorkMode::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAskWorkMode)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAskWorkMode, CDialog)
	//{{AFX_MSG_MAP(CAskWorkMode)
	ON_BN_CLICKED(IDRETRY, OnRetry)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAskWorkMode message handlers

void CAskWorkMode::OnRetry() 
  {	
  EndDialog(IDRETRY);
  }

BOOL CAskWorkMode::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	MessageBeep(MB_ICONQUESTION);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
