// ArticleHeader.cpp: implementation of the ArticleHeader class.
//
//////////////////////////////////////////////////////////////////////

#include "ArticleDBHeader.h"
#include "ArticleHeader.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ArticleHeader::ArticleHeader()
  {
  for (int i=0;i<AHS_MAXITEMS;i++) ahdm[i]="";
  _base=NULL;
  _handle=NULL;
  _lock=NULL;
  }
  
ArticleHeader::~ArticleHeader()
  {
  if (_base) free(_base);
  if (_lock) _lock->Unlock();
  }

unsigned long ArticleHeader::GetSize(unsigned short version) const
  {

#if ADB_CURRENTVERSION!=0x100
#error Adjust version in this block
#endif
  
  
  unsigned long size=sizeof(*ahst);
  /* Add version specific size corrections

   Example:
	if (version<xxyy) size-=sizeof(variable); //variable exists after version xxyy

  */
  int dmitems=AHS_MAXITEMS;

  /* Add version specific count corrections */
  
  for (int i=0;i<dmitems;i++) size+=strlen(ahdm[i])+1;

 
  return size;
  }

bool ArticleHeader::Clone(const ArticleHeader& source, void *base, DMMHANDLE handle) 
  {
  if (_base!=NULL) free(_base);
  if (_lock!=NULL) _lock->Unlock();
  if (base==NULL && handle!=NULL) return false;
  if (base==NULL)
	{
	base=malloc(GetSize());
	if (base==NULL) return false;
	}
  ostrstream str((char *)base,-1);
  str.write((char *)source.ahst,sizeof(*source.ahst));
  int dmitems=AHS_MAXITEMS;
  for (int i=0;i<dmitems;i++) 
    {
    ahdm[i]=(char *)base+str.tellp();
    str.write(source.ahdm[i],strlen(source.ahdm[i])+1);  
    }
  if (handle==NULL) _base=base;
  else _handle=handle;  
  _lock=source._lock;
  if (_lock) _lock->Lock();
  return true;
  }

bool ArticleHeader::AttachArticle(void *base, DMMHANDLE handle, unsigned short version)
  {
  if (base==NULL && handle!=NULL) return false;
  char *ptr=(char *)base;

#if ADB_CURRENTVERSION!=0x100
#error Adjust version in this block
#endif

  ahst=(ArticleHeaderStatic *)ptr;
  ptr+=sizeof(ArticleHeaderStatic);
  
  /* Add version specific size corrections

   Example:
	if (version<xxyy) p-=sizeof(variable); //variable exists after version xxyy

  */
  
  int dmitems=AHS_MAXITEMS;
  int i;

  /* Add version specific count corrections */
  
  for (i=0;i<dmitems;i++) {ahdm[i]=ptr;ptr+=strlen(ptr)+1;}
  for (i=dmitems;i<AHS_MAXITEMS;i++) ahdm[i]=ptr-1;    
  if (handle) {_base=NULL;_handle=handle;}
  else {_base=base;_handle=NULL;}
  return true;
  }


//--------------------------------------------------

static char Months[12][4]=
  {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

//--------------------------------------------------

static int Dayz[12]=
  {31,29,31,30,31,30,31,31,30,31,30,31};

//--------------------------------------------------

static char Days[7][4]=
  {"Sun","Mon","Tue","Wen","Thr","Fri","Sat"};

//--------------------------------------------------
bool ArticleDateTime::ParseUnixTime(const char *date)
  {
  while (*date && !isdigit(*date)) date++;
  int day=0;
  while (*date && isdigit(*date)) day=day*10+(*(date++)-'0');
  char mnthstr[4];
  int i;
  while (*date && !isalnum(*date)) date++;
  for (i=0;*date && isalnum(*date) && i<3;i++) mnthstr[i]=*date++;
  mnthstr[3]=0;
  for (i=0;i<12;i++) if (stricmp(Months[i],mnthstr)==0) break;
  if (i==12) i=0;
  int month=i+1;
  while (*date && !isdigit(*date)) date++;
  int year=0;
  while (*date && isdigit(*date)) year=year*10+(*(date++)-'0');
  if (year<100)
    if (year<50) year+=2000;
  else year+=1900;
  while (*date && !isdigit(*date)) date++;
  int hour=0;
  while (*date && isdigit(*date)) hour=hour*10+(*(date++)-'0');
  while (*date && !isdigit(*date)) date++;
  int min=0;
  while (*date && isdigit(*date)) min=min*10+(*(date++)-'0');
  while (*date && !isdigit(*date)) date++;
  int sec=0;
  while (*date && isdigit(*date)) sec=sec*10+(*(date++)-'0');
  bool plus=true;
  while (*date && *date==' ') date++;
  if (*date=='+') plus=true;
  if (*date=='-') plus=false;
  int offset=0;  
  date++;
  sscanf(date,"%d",&offset);
  if (!plus)
    {
    min+=offset%100;
    hour+=offset/100;
    while (min>59) 
      {hour++;min-=60;}
    while (hour>23) 
      {day++;hour-=24;}
    while (day>Dayz[month-1]) 
      {day-=Dayz[month-1];month++;}
    while (month>12) 
      {year++;month-=12;}
    }
  else
    {
    min-=offset%100;
    hour-=offset/100;
    while (min<0) 
      {hour--;min+=60;}
    while (hour<0) 
      {day--;hour+=24;}
    while (day<1) 
      {month--;day+=Dayz[month-1];}
    while (month<1) 
      {year--;month+=12;}
    }
  this->year=year-1900;
  this->month=month;
  this->day=day;
  this->hour=hour;
  this->minute=min;
  this->second=sec;
  return true;
  }

ArticleLock::ArticleLock()
  {
  _event=CreateEvent(NULL,TRUE,TRUE,NULL);
  InitializeCriticalSection(&cloc);
  InitializeCriticalSection(&cglob);
  _lockCount=0;
  }

ArticleLock::~ArticleLock()
  {
  DeleteCriticalSection(&cloc);
  DeleteCriticalSection(&cglob);
  CloseHandle(_event);
  }

void ArticleLock::Lock()
  {
  EnterCriticalSection(&cglob);
  EnterCriticalSection(&cloc);
  ResetEvent(_event);
  ++_lockCount;
  LeaveCriticalSection(&cloc);
  LeaveCriticalSection(&cglob);
  }

void ArticleLock::Unlock()  
  {
  EnterCriticalSection(&cloc);
  if (--_lockCount==0) SetEvent(_event);  
  LeaveCriticalSection(&cloc);
  }

void ArticleLock::CriticalLock()
  {
  EnterCriticalSection(&cglob);
  WaitForSingleObject(_event,INFINITE);
  SetEvent(_event);
  }

void ArticleLock::CriticalUnlock()
  {
  LeaveCriticalSection(&cglob);
  }

ArticleHeader::ArticleHeader(const ArticleHeader& source)
  {
  if (source._base)
    {
    _base=0;
    _handle=0;
    _lock=0;
    Clone(source,source._base,NULL);
    return;
    }
  else
    {
    ahst=source.ahst;
    memcpy(ahdm,source.ahdm,sizeof(source.ahdm));
    _base=source._base;
    _handle=source._handle;
    _lock=source._lock;
    if (_lock) _lock->Lock();
    return ;
    }
  }  

ArticleHeader &ArticleHeader::operator=(const ArticleHeader& source)
  {  
  if (source._base)
    {
    Clone(source,source._base,NULL);
    return *this;
    }
  else
    {
    ArticleHeader::~ArticleHeader();
    ahst=source.ahst;
    memcpy(ahdm,source.ahdm,sizeof(source.ahdm));
    _base=source._base;
    _handle=source._handle;
    _lock=source._lock;
    if (_lock) _lock->Lock();
    return *this;
    }
  }