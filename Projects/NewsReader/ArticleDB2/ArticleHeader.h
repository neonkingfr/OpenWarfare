// ArticleHeader.h: interface for the ArticleHeader class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARTICLEHEADER_H__505ED1D6_7D21_4E99_91B2_28E1A2CC8BD8__INCLUDED_)
#define AFX_ARTICLEHEADER_H__505ED1D6_7D21_4E99_91B2_28E1A2CC8BD8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define ADB_CURRENTVERSION 0x100

#include "DiskMappedMemory.h"

enum ArticleAttachmentMark {attchNone, attchDoKnow, attchOk};
enum ArticleWatchMark {aWatchLow=-1,aWatchNormal=0,aWatchHigh=1};

  union ArticleHeaderFlags
	{
	DWORD _storage;
	struct
	  {
	  bool opened:1; //!<article childs is opened
	  bool trash:1; //!<article is marked as deleted
	  bool hidden:1; //!<article cannot be seen
	  bool highlighted:1; //!< article is highlighted (as a result of filtering)
	  bool lgwarn:1; //!<article can be downloaded, if it is too long
	  bool ntnew:1; //!<notify new;
	  bool dwnnew:1; //!<new info for download
	  bool hidtoshow:1; //!<if true, hidden thread will be showen grayed
	  bool unread:1; //!<article is unread
      ArticleAttachmentMark attachment:3; 
      ArticleWatchMark watchMark:3;
	  };
	};


struct ArticleDateTime //In GMT
  {
  unsigned char year,month,day,hour,minute,second; //base for year is 1900 
  bool ParseUnixTime(const char *text);
  int Compare(const ArticleDateTime& other) const
	{
	int res;
	res=(year>other.year)-(year<other.year);if (res) return res;
	res=(month>other.month)-(month<other.month);if (res) return res;
	res=(day>other.day)-(day<other.day);if (res) return res;
	res=(hour>other.hour)-(hour<other.hour);if (res) return res;
	res=(minute>other.minute)-(minute<other.minute);if (res) return res;
	res=(second>other.second)-(second<other.second);return res;
	}
  bool operator==(const ArticleDateTime &other) const {return Compare(other)==0;}
  bool operator>=(const ArticleDateTime &other) const {return Compare(other)>=0;}
  bool operator<=(const ArticleDateTime &other) const {return Compare(other)<=0;}
  bool operator!=(const ArticleDateTime &other) const {return Compare(other)!=0;}
  bool operator>(const ArticleDateTime &other) const {return Compare(other)>0;}
  bool operator<(const ArticleDateTime &other) const {return Compare(other)<0;}
  };
#pragma warning( disable : 4284)



struct ArticleHeaderStatic
  {
  ArticleHeaderFlags flags;
  ArticleDateTime datePost; ///<date posted
  ArticleDateTime dateRead; ///<date read
  ArticleDateTime dateNewest; ///<date of newest article in thread
  size_t lines;
  DMMHANDLE threadID; ///<id first article of this thread - calculates during indexing
  DMMHANDLE parentID; ///<id parent article of this thread - calculates during indexing
  };

enum ArticleHeaderStrings 
  {
  AHS_MSGID=0,			  ///<current article message id
  AHS_REFERENCE=1,		  ///<current article reference id
  AHS_FROM=2,			  ///<from email
  AHS_SUBJECT=3,		  ///<subject name (without properties)
  AHS_PROPERTIES=4,		  ///<subject properties
  AHS_XDELETE=5,		  ///<xdelete header value
  AHS_ICON=6,			  ///<icon value
  AHS_CONTENT=7,		  ///<content type;
  AHS_GROUPLIST=8,        ///<groups, that contains this article;
  AHS_MAXITEMS
  };

typedef char *ArticleHeaderDynamic[AHS_MAXITEMS];

class ArticleLock
  {
	DWORD _lockCount;
	HANDLE _event;
	CRITICAL_SECTION cloc;
	CRITICAL_SECTION cglob;
  public:
	ArticleLock();
	~ArticleLock();
	void Lock();
	void Unlock();
	void CriticalLock();
	void CriticalUnlock();
  };

class ArticleHeader  
{
	
	void *_base;  ///< pointet to memory allocated for article header. If NULL, article is allocated in DMM.
	DMMHANDLE _handle; ///< handle for memory allocated in DMM. if NULL, article is not allocated in DMM;
	// If both _base & _handle is NULL, article is allocated in stack or static memory, no cleaning performed
	ArticleLock *_lock;
public:
	ArticleHeaderStatic *ahst;
	ArticleHeaderDynamic ahdm;
public:

    ArticleHeader(const ArticleHeader& source);
    ArticleHeader &operator=(const ArticleHeader& source);
	bool AttachArticle(void *base, DMMHANDLE handle, unsigned short version=ADB_CURRENTVERSION);
  
	bool Clone(const ArticleHeader& source, void *base=NULL, DMMHANDLE handle=NULL);
	unsigned long GetSize(unsigned short version=ADB_CURRENTVERSION) const;

	DMMHANDLE GetHandle() const {return _handle;}
  
	bool IsStatic() const {return _base==NULL && _handle==NULL;}

	void SetLock(ArticleLock *lock)
	  {
	  if (_lock) _lock->Unlock();
	  _lock=lock;
	  if (_lock) _lock->Lock();
	  }



	ArticleHeader();
	~ArticleHeader();
};



#endif // !defined(AFX_ARTICLEHEADER_H__505ED1D6_7D21_4E99_91B2_28E1A2CC8BD8__INCLUDED_)
