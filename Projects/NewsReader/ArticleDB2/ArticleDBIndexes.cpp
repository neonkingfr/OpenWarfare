
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ArticleTable.h"
#include "ArticleDBIndexes.h"
#include "..\CharMapCz.h"

extern CCharMapCz CzechCharMap;

int CmpSubstring::Compare(const char *src, const char *substr, bool exact)
  { 
  while (*src && isspace(*src)) src++;	  //trim left spaces
  while (*substr && isspace(*substr)) substr++; //trim left spaces
  while (*src && *substr)
	{
	char a=CzechCharMap(*src);
	char b=CzechCharMap(*substr);
	if (isspace(a)) a=' '; else if (!isalnum(a)) a='_';else a=toupper(a);
	if (isspace(b)) b=' '; else if (!isalnum(b)) b='_';else b=toupper(b);
	if (a>b) return 1;
	if (b>a) return -1;
	src++;
	substr++;
    if (a==' ') while (*src && isspace(*src)) src++; //short any space, or trim right spaces
    if (b==' ') while (*substr && isspace(*substr)) substr++; //short any space, or trim right spaces
	}
  if (*src<*substr) return 1; 
  if (*src>*substr && exact) return -1; //if exact=false, compare is return equal, if substr ends but source not
  return 0;
  }

int CmpDateTime::Compare(const ArticleDateTime *src, const ArticleDateTime *test, bool exact)
  {
  return src->Compare(*test);
  }

