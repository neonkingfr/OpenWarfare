// ArticleTable.h: interface for the ArticleTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARTICLETABLE_H__302B1E1A_B5DC_4576_929E_D43D19E474E2__INCLUDED_)
#define AFX_ARTICLETABLE_H__302B1E1A_B5DC_4576_929E_D43D19E474E2__INCLUDED_

#include "DiskMappedMemory.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DiskMappedMemory.h"

#include "ArticleHeader.h"

struct ArticleDBFileHeader
  {
  unsigned long DBMagicMark; ///<mark to check valid database
  unsigned short DBVersion;	///<current database version
  DMMHANDLE DBStart; ///< Handle of first article in DB
  DMMHANDLE DBFirstFree; ///< First unused block, currently reserved, not used, should be zero.
  unsigned long Reserver[32]; ///<Reserved memory for future use
  char description[1];	//application specific description;
  };


class ArticleTable : public  CDiskMappedMemory  
{
	ArticleLock _critLock;
public:
	ArticleDBFileHeader * GetTableHeader();
	DMMHANDLE CreateArticle(ArticleHeader &ahdr);
    void GetArticle(DMMHANDLE handle, ArticleHeader &hdr);
	ArticleTable();
	virtual ~ArticleTable();

	ArticleHeader operator() (DMMHANDLE handle)
	  {
	  ArticleHeader hdr;
	  GetArticle(handle,hdr);
	  return hdr;
	  }

	bool EnumArticles(ArticleHeader &article);
	char CheckTable();
	bool DeleteArticle(ArticleHeader &article);


protected:
	void CreateTable();
};

#endif // !defined(AFX_ARTICLETABLE_H__302B1E1A_B5DC_4576_929E_D43D19E474E2__INCLUDED_)
