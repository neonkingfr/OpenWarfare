#ifndef BTREEIOARCH_HH
#define BTREEIOARCH_HH


#include "IArchive.h"
#include "BTreeMemory.h"
#include "BTree.h"

template<class T>
class BTreeIO
  {
  public:
   static void SerializeBTree(IArchive &arch, BTree<T> &tree);
   static void SerializeBTreeNode(IArchive &arch, BTreeNode<T>* &tree);	
  };

template <class T>
void ArchiveVariableExchange(IArchive &arch,BTree<T> &tree)
  {
  BTreeIO<T>::SerializeBTree(arch,tree);
  }

template<class T>
void BTreeIO<T>::SerializeBTree(IArchive &arch, BTree<T> &tree)
  {
  arch("order",tree._order);
  SerializeBTreeNode(arch,tree._root);
  }

template<class T>
void BTreeIO<T>::SerializeBTreeNode(IArchive &arch, BTreeNode<T>* &node)
  {
  short order;
  if (+arch) order=node?node->_order:0;
  arch("order",order);
  if (-arch)
	if (!order) {node=NULL; return;}
	else node=new BTreeNode<T>(order);
  if (order)
	  {
	  int i;
	  arch("keyCount",node->_keyCount);
	  arch("isLeaf",node->_isLeaf);
	  arch("subtreeSize",node->_subtreeSize);
	  for (i=0;i<node->_keyCount;i++) arch("Item",node->item[i]);
	  for (i=0;i<node->_keyCount+1;i++) arch("Node",node->_subtree[i]);
	  }	
  }
#endif