// ArticleTableReader.cpp: implementation of the ArticleTableReader class.
//
//////////////////////////////////////////////////////////////////////

#include "ArticleDBHeader.h"
#include "../LineTerminal2.h"
#include "ArticleTableReader.h"
#include "ArticleTable.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ArticleTableReader::ArticleTableReader(ArticleTable &table):_table(table)
{

}

char *ArticleTableReader::HeaderNames[ArticleTableReader::OVEnd]=
  {
  "Subject",
	"From",
	"Date",
	"Message-ID",
	"References",
	"Lines",
	"Content-type",
	"X-Delete",
	"Icon",
	"Newsgroups"
  };
  

int ReadStatus(LineTerminal &lTerm, char **msg=NULL);

static int ReadStatus(LineTerminal &lTerm, char **msg)
  {
  int ret=-1;
  const char *c=lTerm.ReadLine();
  if (c==NULL) return ret;
  sscanf(c,"%d",&ret);
  if (msg) lTerm.SplitAt(c,NULL,msg,' ');
  return ret;
  }

bool ArticleTableReader::ReadOverviewHeader(LineTerminal &lTerm)
  {
  if (!lTerm.SendLine("LIST OVERVIEW.FMT")) return false;
  memset(_OVStatus,0,sizeof(_OVStatus));
  if (ReadStatus(lTerm)!=215) return false;
  const char *line=lTerm.ReadLine();
  int order=0;
  while (line && line[0]!='.')
	{
	char *left,*right;
	lTerm.SplitAt(line,&left,&right,':');
	int fnd;
	for (fnd=OVSubject;fnd<OVEnd;fnd++)
	  if (stricmp(HeaderNames[fnd],left)==0) break;
	if (fnd!=OVEnd)
	  {
	  _OVStatus[fnd]=stricmp(right,"full")==0?OVS_Full:OVS_Standard;
	  _OVOrder[fnd]=order;
	  }
	order++;
	line=lTerm.ReadLine();
	}
  return true;
  }

ArticleTableReader::ArticleInfo::ArticleInfo()
  {
  memset(this,0,sizeof(*this));
  }


ArticleTableReader::ArticleInfo::~ArticleInfo()
  {
  free(subject);
  free(from);
  free(messageid);
  free(references);
  free(content);
  free(xdelete);
  free(icon);
  }

#define AWARENULL(x) (x)?(x):""

int ArticleTableReader::DownloadGroup(const char *group, LineTerminal &lTerm, int from)
  {
  lTerm.SendLine("GROUP bistudio.general");
  const char *line=lTerm.ReadLine();
  int st,cnt,min,max;
  sscanf(line,"%d %d %d %d",&st,&cnt,&min, &max);
  if (st!=211) return -1;
  max;
  cnt=(max+1)-from;
  if (cnt<=0) return from;  
  ArticleInfo *aatmp=new ArticleInfo[cnt];
  memset(aatmp,0,sizeof(ArticleInfo)*cnt);
  int realcnt=0;
  int i;
  lTerm.SendFormatted("XOVER %d-%d",from,max);
  for (i=0;i<OVEnd;i++)
	if (_OVStatus[i]==OVS_Missing)
	  lTerm.SendFormatted("XHDR %s %d-%d",HeaderNames[i],from,max);
  if (ReadStatus(lTerm)==224) 
	{
	while ((line=lTerm.ReadLine())!=NULL && line[0]!='.')
	  {
	  ArticleInfo &nfo=aatmp[realcnt];
	  char *param,*remain=const_cast<char *>(line);
	  int order=0;	  
	  lTerm.SplitAt(remain,&param,&remain,'\t',false);
	  nfo.index=atoi(param);
	  LoadProgress(nfo.index-from,cnt);
	  while (remain)
		{
		lTerm.SplitAt(remain,&param,&remain,'\t',false);
		int pos;
		for (pos=0;pos<OVEnd;pos++)
		  if (_OVOrder[pos]==order) break;
		if (pos!=OVEnd)		  
		  SetArticleParam(nfo, (OverViewHeaders)pos, param);
		order++;
		}
	  realcnt++;
	  }
	}
  for (i=0;i<OVEnd;i++)
	if (_OVStatus[i]==OVS_Missing)
	  {
	  if (ReadStatus(lTerm)==221) 
		{
		int curindex=0;		
		while ((line=lTerm.ReadLine())!=NULL && line[0]!='.')
		  {
		  char *param,*remain=const_cast<char *>(line);
		  lTerm.SplitAt(remain,&param,&remain,' ');
		  int index=atoi(param);
		  while (aatmp[curindex].index<index && curindex<realcnt)
			curindex++;
		  if (aatmp[curindex].index==index)
			{
			SetArticleParam(aatmp[curindex],(OverViewHeaders)i,remain);
			}
		  }
		}
	  }
  if (realcnt==0) return -1;
  for (i=0;i<realcnt;i++)
	{
	if (OnBeforeLoad(aatmp[i].messageid,group)==true) continue;
	ArticleInfo &nfo=aatmp[i];
	DMMHANDLE handle=0;
	ArticleHeader ahdr;

	ArticleDateTime datetime;
	datetime.ParseUnixTime(nfo.date);

    ArticleHeaderStatic ahdst;
    ahdst.dateNewest=datetime;
    ahdst.datePost=datetime;
    ahdst.dateRead=datetime;
    ahdst.lines=nfo.lines;
    ahdst.flags.attachment=attchNone;
    ahdst.flags.watchMark=aWatchNormal;
    ahdst.flags._storage=0;
    ahdst.flags.dwnnew=1;
    ahdst.flags.ntnew=1;
    ahdst.flags.unread=1;    
    
    ahdr.ahst=&ahdst;
    ahdr.ahdm[AHS_MSGID]=AWARENULL(nfo.messageid);
    ahdr.ahdm[AHS_REFERENCE]=AWARENULL(nfo.references);
    ahdr.ahdm[AHS_FROM]=AWARENULL(nfo.from);
    ahdr.ahdm[AHS_SUBJECT]=AWARENULL(nfo.subject);
	char *prop=strstr(nfo.subject,"#$");
	if (prop!=NULL)
	  {
	  *prop=0;
	  ahdr.ahdm[AHS_PROPERTIES]=prop+2;
	  }
    ahdr.ahdm[AHS_XDELETE]=AWARENULL(nfo.xdelete);
    ahdr.ahdm[AHS_ICON]=AWARENULL(nfo.icon);
    ahdr.ahdm[AHS_CONTENT]=AWARENULL(nfo.content);
    ahdr.ahdm[AHS_GROUPLIST]=AWARENULL(nfo.groups);

    prop=strrchr(ahdr.ahdm[AHS_REFERENCE],' ');
	if (prop) 
	  ahdr.ahdm[AHS_REFERENCE]=prop+1;
	handle=_table.CreateArticle(ahdr);
    _table.GetArticle(handle,ahdr);
	OnAfterLoad(handle,ahdr);
    ahdr.ahst->parentID=0;
    ahdr.ahst->threadID=handle;
	}

  delete [] aatmp;
  return max+1;  
  }

void ArticleTableReader::SetArticleParam(ArticleInfo &nfo, OverViewHeaders hdr, const char *param)
{
		  switch (hdr)
			{
			case OVSubject:free(nfo.subject);nfo.subject=strdup(param);break;
			case OVFrom:free(nfo.from);nfo.from=strdup(param);break;
			case OVDate:free(nfo.date);nfo.date=strdup(param);break;
			case OVMessageID:free(nfo.messageid);nfo.messageid=strdup(param);break;
			case OVReferences:free(nfo.references);nfo.references=strdup(param);break;
			case OVLines:nfo.lines=-1;sscanf(param,"%d",&nfo.lines);break;
			case OVContentType:free(nfo.content);nfo.content=strdup(param);break;			  
			case OVXDelete:free(nfo.xdelete);nfo.xdelete=strdup(param);break;			  
			case OVIcon:free(nfo.icon);nfo.icon=strdup(param);break;
			case OVGroups:free(nfo.groups);nfo.groups=strdup(param);break;
			}			  
}
