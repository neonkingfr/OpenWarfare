// ArticleDBView.cpp: implementation of the ArticleDBView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ArticleDBView.h"
#include "ArticleDB.h"
#include "../CharMapCz.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

extern CCharMapCz CzechCharMap;

void *ArticleDBViewCond::operator new(size_t sz,const char *szText)
  {
  ArticleDBViewCond *ret=(ArticleDBViewCond *)malloc(sizeof(ArticleDBViewCond)+strlen(szText));
  strcpy(ret->szData,szText);
  ret->itemType=ItDynItem;
  ret->AHSIndex=AHS_MSGID;
  ret->op=Equal;
  ret->negate=false;
  ret->final=true;
  return ret;
  }

void *ArticleDBViewCond::operator new(size_t sz)
  {
  ArticleDBViewCond *ret=(ArticleDBViewCond *)malloc(sizeof(ArticleDBViewCond));
  ret->szData[0]=0;
  ret->itemType=ItFlag;
  ret->flag=ItFUnread;
  ret->op=IsSet;
  ret->negate=false;
  ret->final=true;
  return ret;
  }


void ArticleDBViewCond::operator delete(void *sz,const char *szText)
  {
  free(sz);
  }
void ArticleDBViewCond::operator delete(void *sz)
  {
  free(sz);
  }

bool ArticleDBViewCond::Evaluate(const ArticleHeader &hdr) const
  {
  bool ret;
  switch (itemType)
    {
    case ItDynItem: ret= EvaluateDynItem(hdr);break;
    case ItFlag: ret= EvaluateFlag(hdr);break;
    case ItDate: ret= EvaluateDate(hdr);break;
    case ItLines: ret= EvaluateInt(hdr);break;
    case ItGroup: ret= EvaluateGroup(hdr);    break;    
    default: return false;
    }
  if (negate) ret=!ret;  
  return ret;
  }

static bool stristr(const char *source, const char *fnd)
  {
  if (fnd[0]==0) return true;
  do
	{
	while (*source && toupper(CzechCharMap(*source))!=(toupper (CzechCharMap(*fnd)))) source++;
	if (*source)
	  {
	  int q=1;
	  while (fnd[q] && source[q]) 
		if (toupper(CzechCharMap(source[q]))!=toupper(CzechCharMap(fnd[q]))) break;else q++;
	  if (fnd[q]==0) return true;
	  source++;
	  }
	}
  while (*source);
  return false;
  }


bool ArticleDBViewCond::EvaluateDynItem(const ArticleHeader &hdr) const
  {
  if (op==Contain)    
    return stristr(hdr.ahdm[AHSIndex],szData);    
  
  int len=strlen(szData);         //lenght of data to be tested

  if (szData[len-1]=='*') len--;else len++; //if contain asterix, remove it and test begin of text
          //if not add ending zero to test whole string
  char *c=(char *)alloca(len+1); //create helper buffer
  strncpy(c,hdr.ahdm[AHSIndex],len); //copy part of string
  c[len]=0;
  switch (op)
    {
    case Equal: return stricmp(c,szData)==0;
    case Less:  return stricmp(c,szData)==-1;
    case Great:  return stricmp(c,szData)==1;    
    default: return false;
    }
  }

bool ArticleDBViewCond::EvaluateFlag(const ArticleHeader &hdr) const
  {
  if (op!=IsSet) return false;
  switch (flag)
    {
    case ItFAttachm: return hdr.ahst->flags.attachment!=attchNone;
    case ItFHighlighted: return hdr.ahst->flags.highlighted;
    case ItFUnread: return hdr.ahst->flags.unread;
    default: return false;
    }
  }

bool ArticleDBViewCond::EvaluateDate(const ArticleHeader &hdr) const
  {
  switch (op)
    {
    case Equal:return hdr.ahst->datePost==valDate; 
    case Less: return hdr.ahst->datePost<valDate;
    case Great: return hdr.ahst->datePost>valDate;
    case Contain:
      return hdr.ahst->datePost.day==valDate.day &&
             hdr.ahst->datePost.month==valDate.month &&
             hdr.ahst->datePost.year==valDate.year;
    default: return false;
    }
  }

bool ArticleDBViewCond::EvaluateInt(const ArticleHeader &hdr) const
  {
  switch (op)
    {
    case Equal:return (signed)hdr.ahst->lines==valInt; 
    case Less: return (signed)hdr.ahst->lines<valInt;
    case Great: return (signed)hdr.ahst->lines>valInt;
    default: return false;    
    }
  }

bool ArticleDBViewCond::EvaluateGroup(const ArticleHeader &hdr) const
  {
  const char *p=hdr.ahdm[AHS_GROUPLIST];
  while (true)
    {
    const char *q=szData; //take testing group
    while (*q && toupper(*p)==toupper(*q)) {*q++;*p++;} //test group in list
    if (*p==0 || *p==',') return true;  //found end of string of q? test if it end of p
    p=strchr(p,',');      //not, find next group
    if (p) p++;         //found, prepare to next search
    else return false; //no next group, return failed.
    }
  }



ArticleDBView::ArticleDBView(ArticleDB & currDB):
  _currDB(currDB)
  {
  _stringBeg=0;
  _condsUsed=0;

  _name[0]=0;

  _sortItem=sortMark;
  _sortDirty=true;
  _reverseSort=false;
  _onlyThreads=true;
  _maxItems=10000;
  _testSubthreads=true;
  _testParents=true;
  }

ArticleDBView::~ArticleDBView()
  {
  DeleteAllConditions();
  }


bool ArticleDBView::AddCondition(ArticleDBViewCond *cond)
  {
  if (_condsUsed>=DBVIEW_MAXCONDS) return false;
  _conds[_condsUsed]=cond;
  _condsUsed++;
  return true;
  }

void ArticleDBView::DeleteAllConditions()
  {
  for (int i=0;i<_condsUsed;i++) delete _conds[i];
  _condsUsed=0;
  }

bool ArticleDBView::TestConditions(const ArticleHeader &hdr) const
  {
  int i=0;
  while (i<_condsUsed)
    {
    if (_conds[i]->Evaluate(hdr)==false)  //not Evaluated
      {      
      while (i<_condsUsed) if (_conds[i]->final) break; //try to find end of this condition
      i++;                                  //add one, because now we found end of previous condition
      if (i>=_condsUsed) return false; //not found, not evaluated
      }
    else                                //evaluated
      {
      if (_conds[i]->final) return true; //if it is final condition, then report success
      i++;                                //not final, you must evaluate next
      }
    }
  return true;    //reached end... all conditions evaluated, then return success
  }

void ArticleDBView::RemoveAll(bool reindex)
  {
  if (!reindex && _stringBeg)
	{
	_currDB.GetStrings().FreeString(_stringBeg);
	}
  _stringBeg=0;
  }

bool ArticleDBView::SetName(const char *name)
  {
  int len=strlen(name)+1;
  if (len>DBVIEW_MAXNAME) return false;
  memcpy(_name,name,len);
  return true;
  }


void ArticleDBView::UpdateSorting()
  {
  // not written yet
  _sortDirty=false;
  }

void ArticleDBView::AppendNewArticle(const ArticleHeader &hdr)
  {
  if (_onlyThreads)
    {
    if (hdr.ahst->parentID)
      {
      if (!_testParents) return;
      ArticleHeader parent;
      _currDB.GetTable().GetArticle(hdr.ahst->parentID,parent);
      if (TestConditions(parent)==true) return;
      }
    }
  if (TestConditions(hdr)==false) return;
  _stringBeg=_currDB.GetStrings().AppendNewItem(_stringBeg,hdr.GetHandle(),true);
  _totalInView=-1;
  _sortDirty=true;
  }

bool ArticleDBView::RetestArticle(const ArticleHeader &hdr)
  {
  if (TestConditions(hdr)==true) return true;
  _currDB.GetStrings().FreeItemInString(hdr.GetHandle(),_stringBeg);
  _totalInView=-1;
  return false;
  }


ArticleDBViewRecordset::ArticleDBViewRecordset(ArticleDB &currDB,DMMHANDLE ifrst,int remain):
  _currDB(currDB),_next(ifrst),_remainItems(remain)
  { }

bool ArticleDBViewRecordset::FindNext(ArticleHeader &hdr)
  {
  if (!_next || _remainItems==0)
    {
    hdr=ArticleHeader();
    return false;
    }
  SingleStringInfo *nfo=_currDB.GetStrings().GetItem(_next);
  _currDB.GetTable().GetArticle(nfo->curhandle,hdr);
  _next=nfo->nexthandle;
  --_remainItems;
  return true;
  }

ArticleDBViewRecordset ArticleDBView::EnumArticlesInView(bool all) const
  {
  if (all) 
    {
    if (_totalInView<0) _totalInView=_currDB.GetStrings().GetStringLength(_stringBeg);
    return ArticleDBViewRecordset(_currDB,_stringBeg,_totalInView);   
    }
  int from=0;
  if (_maxItems<0)
    {
    if (_totalInView<0) _totalInView=_currDB.GetStrings().GetStringLength(_stringBeg);
    from=_totalInView+_maxItems;
    if (from<0) from=0;
    }
  DMMHANDLE start=_stringBeg;
  while (from && start)
    {
    start=_currDB.GetStrings().GetNextIndex(start);
    --from;
    }
  return ArticleDBViewRecordset(_currDB,start,_maxItems);
  }

ArticleSubthreadRecordset::ArticleSubthreadRecordset(ArticleDB & currDB,const ArticleDBView &currView,const ArticleHeader &childof):
  ArticleRecordset<AStringIndex>(currDB.GetReferenceIndex(),
                                 &currDB.GetTable(),
                                 AStringIndex(currDB.GetTable(),
                                              childof,
                                              childof.ahdm[AHS_MSGID]
                                              )
                                ),
  _currDB(currDB),
  _currView(currView)
  {  }

bool ArticleSubthreadRecordset::FindNext(ArticleHeader &hdr)
  {
  bool nextfnd=ArticleRecordset<AStringIndex>::FindNext(hdr);
  while (nextfnd)
    {
    if (_currView._testSubthreads)      
      if (_currView.TestConditions(hdr)) return true;
    else
      return true;
    nextfnd=ArticleRecordset<AStringIndex>::FindNext(hdr);
    }
  return false;
  }


void ArticleWholeThreadRecordset::CreateSubThreadList(const ArticleHeader &childOf, DMMHANDLE addafter)
  {
  ArticleSubthreadRecordset recordset(_currDB,_currView,childOf);
  ArticleHeader hdr;

  while (recordset.FindNext(hdr))
    {        
    addafter=_currDB.GetStrings().InsertNewItem(addafter,hdr.GetHandle());
    if (_current==0) _current=addafter;    
    }
  }

bool ArticleWholeThreadRecordset::FindNext(ArticleHeader &hdr)
  {
  if (_current==0xFFFFFFFF) 
    {
    _current=0;
    CreateSubThreadList(hdr,0);        
    }
  if (_current)
    {
    DMMHANDLE save;
    SingleStringInfo *nfo=_currDB.GetStrings().GetItem(_current);
    _currDB.GetTable().GetArticle(nfo->curhandle,hdr);    
    CreateSubThreadList(hdr,_current);
    save=_current;
    nfo=_currDB.GetStrings().GetItem(_current);
    _current=nfo->nexthandle;
    _currDB.GetStrings().FreeIndex(save);
    return true;
    }
  return false; 
  }

