// stdafx.cpp : source file that includes just the standard includes
//	NewsReader.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

CCriticalSection glob_lock;

DWORD ShadeColor(COLORREF color, int factor)
{
  unsigned char *p=(unsigned char *)&color;
  if (p[0]<0x80 && p[1]<0x80 && p[2]<0x80)
  {
    p[0]=255-factor*(255-(int)(p[0]))/100;
    p[1]=255-factor*(255-(int)(p[1]))/100;
    p[2]=255-factor*(255-(int)(p[2]))/100;
  }
  else
  {
    p[0]=(int)(p[0])*factor/100;
    p[1]=(int)(p[1])*factor/100;
    p[2]=(int)(p[2])*factor/100;
  }
  return color;
}

DWORD ShadeSysColor(int color,int factor)
{
  DWORD colr=GetSysColor(color);
  return ShadeColor(colr,factor);
}

