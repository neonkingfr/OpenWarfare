#if !defined(AFX_LINETERMINAL_H__900E28F9_F6B0_4FAA_B635_EDA435006E75__INCLUDED_)
#define AFX_LINETERMINAL_H__900E28F9_F6B0_4FAA_B635_EDA435006E75__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LineTerminal.h : header file
//

#define TEMPMSG "nwreader.tmp"


/////////////////////////////////////////////////////////////////////////////
// CLineTerminal command target

#define CLT_OK 0
#define CLT_READTIMEOUT -1
#define CLT_WRITETIMEOUT -2
#define CLT_CONNECTIONRESET -3
#define CLT_INVALIDREPLY -5
#define CLT_CONNECTFAILED -6
#define CLT_INTERRUPTED -7

class CLineTerminal : public CSocket
{
// Attributes
public:
	CTime laststamp; //to check timeouts;
	CTimeSpan timeout; //curtimeout;
    char buffer[256];
	int read;
	HWND stopwnd; //window to handle STOP button
// Operations
public:
	CLineTerminal();
	virtual ~CLineTerminal();
	static HANDLE debugout;
	static bool stop;

// Overrides
public:
	void Init();
	void Attach(SOCKET s);
	bool GetHostByName(const char *name, SOCKADDR_IN &host);
	static bool DebugOpened();
	static void CloseDebug();
	static void OpenDebug();
	int ShowError(int result);
	void SetTimeout(CTimeSpan tim);
	int Connect(const char *address, unsigned int port);
	int ReceiveNumReply(CString& comment);
	int SendFormatted(const char *format, ...);
	int SendFormattedV(const char *format, va_list list);
	int SendLine(const char *line);
	int ReceiveLine(CString &str);
	bool IsConnected() {return m_hSocket!=INVALID_SOCKET;}
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLineTerminal)
	public:
	virtual BOOL OnMessagePending();
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CLineTerminal)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINETERMINAL_H__900E28F9_F6B0_4FAA_B635_EDA435006E75__INCLUDED_)
