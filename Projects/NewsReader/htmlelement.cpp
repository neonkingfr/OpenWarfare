// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "stdafx.h"
#include "htmlelement.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CHTMLElement properties

/////////////////////////////////////////////////////////////////////////////
// CHTMLElement operations

void CHTMLElement::setAttribute(LPCTSTR strAttributeName, const VARIANT& AttributeValue, long lFlags)
{
	static BYTE parms[] =
		VTS_BSTR VTS_VARIANT VTS_I4;
	InvokeHelper(0x800101f5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 strAttributeName, &AttributeValue, lFlags);
}

VARIANT CHTMLElement::getAttribute(LPCTSTR strAttributeName, long lFlags)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x800101f6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		strAttributeName, lFlags);
	return result;
}

BOOL CHTMLElement::removeAttribute(LPCTSTR strAttributeName, long lFlags)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x800101f7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		strAttributeName, lFlags);
	return result;
}

void CHTMLElement::SetClassName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x800103e9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CHTMLElement::GetClassName()
{
	CString result;
	InvokeHelper(0x800103e9, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetId(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x800103ea, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CHTMLElement::GetId()
{
	CString result;
	InvokeHelper(0x800103ea, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

CString CHTMLElement::GetTagName()
{
	CString result;
	InvokeHelper(0x800103ec, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH CHTMLElement::GetParentElement()
{
	LPDISPATCH result;
	InvokeHelper(0x80010008, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH CHTMLElement::GetStyle()
{
	LPDISPATCH result;
	InvokeHelper(0x8001004a, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnhelp(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x8001177d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnhelp()
{
	VARIANT result;
	InvokeHelper(0x8001177d, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnclick(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011778, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnclick()
{
	VARIANT result;
	InvokeHelper(0x80011778, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOndblclick(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011779, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOndblclick()
{
	VARIANT result;
	InvokeHelper(0x80011779, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnkeydown(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011775, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnkeydown()
{
	VARIANT result;
	InvokeHelper(0x80011775, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnkeyup(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011776, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnkeyup()
{
	VARIANT result;
	InvokeHelper(0x80011776, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnkeypress(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011777, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnkeypress()
{
	VARIANT result;
	InvokeHelper(0x80011777, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnmouseout(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011771, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnmouseout()
{
	VARIANT result;
	InvokeHelper(0x80011771, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnmouseover(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011770, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnmouseover()
{
	VARIANT result;
	InvokeHelper(0x80011770, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnmousemove(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011774, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnmousemove()
{
	VARIANT result;
	InvokeHelper(0x80011774, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnmousedown(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011772, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnmousedown()
{
	VARIANT result;
	InvokeHelper(0x80011772, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnmouseup(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011773, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnmouseup()
{
	VARIANT result;
	InvokeHelper(0x80011773, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH CHTMLElement::GetDocument()
{
	LPDISPATCH result;
	InvokeHelper(0x800103fa, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetTitle(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x80010045, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CHTMLElement::GetTitle()
{
	CString result;
	InvokeHelper(0x80010045, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetLanguage(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x800113ec, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CHTMLElement::GetLanguage()
{
	CString result;
	InvokeHelper(0x800113ec, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnselectstart(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011795, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnselectstart()
{
	VARIANT result;
	InvokeHelper(0x80011795, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::scrollIntoView(const VARIANT& varargStart)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x800103fb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 &varargStart);
}

BOOL CHTMLElement::contains(LPDISPATCH pChild)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x800103fc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		pChild);
	return result;
}

long CHTMLElement::GetSourceIndex()
{
	long result;
	InvokeHelper(0x80010400, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT CHTMLElement::GetRecordNumber()
{
	VARIANT result;
	InvokeHelper(0x80010401, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetLang(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x80011391, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CHTMLElement::GetLang()
{
	CString result;
	InvokeHelper(0x80011391, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long CHTMLElement::GetOffsetLeft()
{
	long result;
	InvokeHelper(0x800103f0, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long CHTMLElement::GetOffsetTop()
{
	long result;
	InvokeHelper(0x800103f1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long CHTMLElement::GetOffsetWidth()
{
	long result;
	InvokeHelper(0x800103f2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long CHTMLElement::GetOffsetHeight()
{
	long result;
	InvokeHelper(0x800103f3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH CHTMLElement::GetOffsetParent()
{
	LPDISPATCH result;
	InvokeHelper(0x800103f4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetInnerHTML(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x80010402, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CHTMLElement::GetInnerHTML()
{
	CString result;
	InvokeHelper(0x80010402, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetInnerText(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x80010403, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CHTMLElement::GetInnerText()
{
	CString result;
	InvokeHelper(0x80010403, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOuterHTML(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x80010404, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CHTMLElement::GetOuterHTML()
{
	CString result;
	InvokeHelper(0x80010404, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOuterText(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x80010405, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString CHTMLElement::GetOuterText()
{
	CString result;
	InvokeHelper(0x80010405, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CHTMLElement::insertAdjacentHTML(LPCTSTR where, LPCTSTR html)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x80010406, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 where, html);
}

void CHTMLElement::insertAdjacentText(LPCTSTR where, LPCTSTR text)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x80010407, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 where, text);
}

LPDISPATCH CHTMLElement::GetParentTextEdit()
{
	LPDISPATCH result;
	InvokeHelper(0x80010408, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL CHTMLElement::GetIsTextEdit()
{
	BOOL result;
	InvokeHelper(0x8001040a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void CHTMLElement::click()
{
	InvokeHelper(0x80010409, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH CHTMLElement::GetFilters()
{
	LPDISPATCH result;
	InvokeHelper(0x8001040b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOndragstart(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011793, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOndragstart()
{
	VARIANT result;
	InvokeHelper(0x80011793, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString CHTMLElement::toString()
{
	CString result;
	InvokeHelper(0x8001040c, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnbeforeupdate(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011785, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnbeforeupdate()
{
	VARIANT result;
	InvokeHelper(0x80011785, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnafterupdate(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011786, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnafterupdate()
{
	VARIANT result;
	InvokeHelper(0x80011786, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnerrorupdate(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011796, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnerrorupdate()
{
	VARIANT result;
	InvokeHelper(0x80011796, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnrowexit(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011782, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnrowexit()
{
	VARIANT result;
	InvokeHelper(0x80011782, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnrowenter(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011783, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnrowenter()
{
	VARIANT result;
	InvokeHelper(0x80011783, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOndatasetchanged(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011798, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOndatasetchanged()
{
	VARIANT result;
	InvokeHelper(0x80011798, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOndataavailable(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x80011799, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOndataavailable()
{
	VARIANT result;
	InvokeHelper(0x80011799, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOndatasetcomplete(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x8001179a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOndatasetcomplete()
{
	VARIANT result;
	InvokeHelper(0x8001179a, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void CHTMLElement::SetOnfilterchange(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x8001179b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT CHTMLElement::GetOnfilterchange()
{
	VARIANT result;
	InvokeHelper(0x8001179b, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH CHTMLElement::GetChildren()
{
	LPDISPATCH result;
	InvokeHelper(0x8001040d, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH CHTMLElement::GetAll()
{
	LPDISPATCH result;
	InvokeHelper(0x8001040e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}
