#include <windows.h>
#include ".\winsharedmemory.h"

#define HWNDTargetID (('H'<<24)|('W'<<16)|('N'<<8)|('D'))
#define MessageID (('W'<<24)|('P'<<16)|('A'<<8)|('R'))


unsigned long WinS_HWNDTarget::GetTargetType() const
  {
  return HWNDTargetID;
  }

const WinS_HWNDTarget *WinS_HWNDTarget::IsMyTarget(const ISharedMemoryTarget &target)
  {
  if (target.GetTargetType()==HWNDTargetID) return static_cast<const WinS_HWNDTarget *>(&target);
  return NULL;
  }

unsigned long WinS_Message::GetTargetType() const
  {
  return MessageID;
  }

const WinS_Message*WinS_Message::IsMyTarget(const ISharedMemoryTarget &target)
  {
  if (target.GetTargetType()==MessageID) return static_cast<const WinS_Message *>(&target);
  return NULL;
  }


static size_t GetMappingSize(HANDLE mapping)
  {
  SYSTEM_INFO sysinfo;
  GetSystemInfo(&sysinfo);
  DWORD mask=0x80000000;
  size_t size=0;
  while (mask>=sysinfo.dwPageSize)
    {
    size^=mask;
    LPVOID trymap=MapViewOfFile(mapping,FILE_MAP_READ,0,0,size);
    if (trymap==NULL) size^=mask;else UnmapViewOfFile(trymap);
    mask>>=1;
    }
  return size;
  }

WinSharedMemory::WinSharedMemory(bool enablegloballocks, HANDLE file):_target(NULL)
{
 _lock=NULL;
  _memory=NULL;
  _memsize=0;
  _lockoffset=0;
  _locksize=0;
  _islocked=0;
  _lockptr=NULL;
  _sharelockenabled=enablegloballocks;    
  _file=file;
}

bool WinSharedMemory::Synchronize(WPARAM wParam, LPARAM lParam)
  {
  if (_islocked) Unlock();
  SafeLock();
  
  if (_sharelockenabled) 
    {
    WaitForSingleObject((HANDLE)wParam,INFINITE);
    HANDLE oldlock=_lock;
    _lock=(HANDLE)wParam;   //change lock. After unlock, other thread will try acquire it 
    ReleaseMutex(oldlock);
    CloseHandle(oldlock);
    ReleaseMutex(_lock);
    }
  else     
    if (_lock==NULL) _lock=CreateMutex(NULL,FALSE,NULL);
  
  _memory=(HANDLE)lParam;  
  _memsize=GetMappingSize(_memory);
  return true;
  }

bool WinSharedMemory::SafeLock(DWORD timeout) const
  {
  int res;
  HANDLE curlock=_lock; //get current lock
  while (curlock)   //until current lock is valid
    {
    res=WaitForSingleObject(curlock,timeout); //try to wait for lock
    if (res==WAIT_TIMEOUT) return false;    //timeout ellapsed, return fail
    if (curlock==_lock) return true;    //we have acquired lock, return success
    curlock=_lock;     //otherwise, lock was destroyed and created the new one. We must acquire it.
    }
  //no lock has been created, it seems, that memory was destroyed. return error;
  return false;
  }

void WinSharedMemory::Free()
  {
  if (IsLocked()) Unlock();            //if memory is locked for me, unlock it
  bool res=SafeLock();                 //lock for exclusive access, no thread should lock memory during destroying
  if (res==false) return;              //unable to lock memory. Maybe has been destroyed, and all data is invalid! flee!
  if (_memory) CloseHandle(_memory);   //close memory object
  _memory=NULL;                        
  _memsize=0;
  HANDLE finalgate=_lock;              //remove lock from object
  _lock=NULL;
  if (finalgate) 
  { 
    ReleaseMutex(finalgate);
    CloseHandle(finalgate);  
  }
  _islocked=false;
  }

WinSharedMemory::~WinSharedMemory(void)
  {
  Free();
  }

bool WinSharedMemory::Allocate(size_t size)
  {
  if (_islocked) return false;
  if (_memory) return false;
  SECURITY_ATTRIBUTES security;
  memset(&security,0,sizeof(security));  
  security.nLength=sizeof(security);
  security.bInheritHandle=TRUE;
  _memory=CreateFileMapping(_file,&security,PAGE_READWRITE,0,size,NULL);
  if (_memory==NULL) return false;
  _memsize=GetMappingSize(_memory);
  _lock=CreateMutex(&security,FALSE,NULL);
  _lockoffset=_locksize=0;
  _islocked=false;
  _lockptr=NULL;
  return true;
  }

void *WinSharedMemory::Lock(size_t offset, size_t size, bool exclusive, DWORD waittime)
  {
  if (_memory==NULL) return NULL;  
  if (exclusive) if (SafeLock(waittime)==false) return NULL;
  if (_islocked) {if (exclusive) ReleaseMutex(_lock);return NULL;}
  _lockptr=MapViewOfFile(_memory,FILE_MAP_ALL_ACCESS,0,offset,size);
  if (_lockptr==NULL) {if (exclusive) ReleaseMutex(_lock);return NULL;}
  _islocked=true;
  _lockoffset=offset;
  _locksize=size==0?_memsize-offset:size;
  return _lockptr;
  }

bool WinSharedMemory::Unlock()
  {
  if (!IsLocked()) return false;
  UnmapViewOfFile(_lockptr);
  _lockoffset=0;
  _locksize=0;
  _islocked=false;
  _lockptr=0;
  ReleaseMutex(_lock);
  return true;
  }

bool WinSharedMemory::SetTarget(const ISharedMemoryTarget &target)
  {
  const WinS_HWNDTarget *mytarget=WinS_HWNDTarget::IsMyTarget(target);
  if (mytarget==NULL) return false;
  _target=*mytarget;
  return true;
  }

const ISharedMemoryTarget &WinSharedMemory::GetCurrentTarget() const
  {
  return _target;
  }

bool WinSharedMemory::Share(unsigned int messageId, unsigned long userData) const
  {
  HWND wntarget=_target._hWnd;
  if (wntarget==NULL) return false; //NO TARGET!
  if (_memory==NULL) return false; //NO MEMORY ALLOCATED
  if (!IsWindow(wntarget)) return false; //APPLICATION DOESN'T RUN
  HANDLE dup_lock,dup_mem;
  HANDLE process;
  DWORD processID;  
  if (GetWindowThreadProcessId(wntarget,&processID)==0) return false; //Cannot get process id
  process=OpenProcess(PROCESS_DUP_HANDLE,TRUE,processID);
  if (process==NULL) return false; //cannot get process handle
  DuplicateHandle(GetCurrentProcess(),_memory,process,&dup_mem,0,TRUE,DUPLICATE_SAME_ACCESS);
  WPARAM wParam;
  LPARAM lParam;
  if (_sharelockenabled) 
    {
    DuplicateHandle(GetCurrentProcess(),_lock,process,&dup_lock,0,TRUE,DUPLICATE_SAME_ACCESS);
    wParam=(WPARAM)dup_lock;
    }
  else
    wParam=(WPARAM)userData;
  lParam=(LPARAM)dup_mem;
  PostMessage(wntarget,messageId,wParam,lParam);
  CloseHandle(process);
  return true;
  }

size_t WinSharedMemory::Size()const
  {
  return _memsize;
  }
  
bool WinSharedMemory::IsLocked(size_t *offset, size_t *size) const
  {
  if (IsBusy()) return false; //object is locked by another thread, for me is unlocked;
  bool locked=_islocked;
  if (locked)
    {
    if (offset) *offset=_lockoffset;
    if (size) *size=_locksize;    
    }  
  return locked;
  }

bool WinSharedMemory::SendPackage( unsigned int messageId, const void *data, size_t size, unsigned long userData)
  {
  if (_memory) return false;
  if (Allocate(size)==false) return false;
  if (SafeLock()==false) return false; //this lock is acquired to prevent infinite waiting between Share and Free, 
                                  //when other side locks received data. Other side will acquire lock
                                  //after SendPacked is complette
  void *lockptr=Lock(0,0,true,0);
  if (lockptr==NULL) {ReleaseMutex(_lock);Free();return false;}
  memcpy(lockptr,data,size);
  Unlock();
  bool res=Share(messageId,userData);
  HANDLE finalgate;
  DuplicateHandle(GetCurrentProcess(),_lock,GetCurrentProcess(),&finalgate,0,FALSE,DUPLICATE_SAME_ACCESS);
      //handle _lock is destroyed by Free(). To unlock Mutex, we need duplicate its value
  Free();                     
  ReleaseMutex(finalgate);
  return res;
  }

bool WinSharedMemory::Synchronize(const ISharedMemoryTarget &message)
  {
  const WinS_Message *messageId=WinS_Message::IsMyTarget(message);
  if (messageId==NULL) return false;
  Synchronize(messageId->wParam,messageId->lParam);
  return true;
  }

bool WinSharedMemory::IsBusy() const
  {
  bool ret=!SafeLock(0);
  if (!ret && _lock) ReleaseMutex(_lock);
  return ret;
  }