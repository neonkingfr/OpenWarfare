#if !defined(AFX_GOTOREFDLG_H__4A8F3FA1_9CA0_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
#define AFX_GOTOREFDLG_H__4A8F3FA1_9CA0_11D5_B3A0_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GotoRefDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGotoRefDlg dialog

class CGotoRefDlg : public CDialog
  {
  // Construction
  public:
    CGotoRefDlg(CWnd* pParent = NULL, int idd=IDD_GOTOREF);   // standard constructor
    CNewsAccount *acc;

    // Dialog Data
    //{{AFX_DATA(CGotoRefDlg)
	enum { IDD = IDD_MESSAGEREFERENCE };
    CString	ref;
	//}}AFX_DATA
    bool _do_not_paste;
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CGotoRefDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CGotoRefDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEdit1();
	afx_msg void OnPaste();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GOTOREFDLG_H__4A8F3FA1_9CA0_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
