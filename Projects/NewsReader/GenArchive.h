// GenArchive.h: interface for the CGenArchive class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GENARCHIVE_H__9D3F868E_1C20_42B2_815D_843B327E774C__INCLUDED_)
#define AFX_GENARCHIVE_H__9D3F868E_1C20_42B2_815D_843B327E774C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <iostream>
#include <strstream>

#include <es/strings/rstring.hpp>

typedef int vartype;

class CGenArchive  
{
  std::iostream& iostr;
public:
	int version;
	bool IsStoring;
  CGenArchive(std::iostream& str):iostr(str) {}

	virtual void Serialize(void *data, int size)=0;	
  std::iostream *operator->() {return &iostr;}
	int operator! () {return !iostr;}
	void Serialize(CTime& tim);
	void Serialize(CTimeSpan& tim);
	void Serialize(CString& s);
	void Serialize(RString& s);
	void VarSerial(vartype var) {}
	void ArrSerial(vartype var[]) {}
	void ArrVarSerial(vartype *var, int items) {}
	virtual void Reserve(int size, unsigned char bytes)=0;
  };

#define VarSerial(var) Serialize((void *)&var,sizeof(var))
#define ArrSerial(arr) Serialize((void *)arr,sizeof(arr))
#define ArrVarSerial(arr,len) Serialize((void *)arr,sizeof(*arr)*len)

class CGenArchiveIn : public CGenArchive  
  {	
public:
  CGenArchiveIn(std::iostream& str):CGenArchive(str) {IsStoring=false;}
	virtual void Serialize(void *data, int size)
	  {
	  (*this)->read((char *)data,size);
	  }
	virtual void Reserve(int size, unsigned char bytes)
	  {
	  while (size--) (*this)->read((char *)&bytes,1);
	  }

  };

class CGenArchiveOut : public CGenArchive  
  {	
public:
  CGenArchiveOut(std::iostream& str):CGenArchive(str) {IsStoring=true;}
	virtual void Serialize(void *data, int size)
	  {
	  (*this)->write((char *)data,size);
	  }
	virtual void Reserve(int size, unsigned char bytes)
	  {
	  while (size--) (*this)->write((char *)&bytes,1);
	  }
  };

class CStrArchiveIn : public CGenArchiveIn  
  {
    std::strstream str;
public:
  CStrArchiveIn(void *buff, int size):str((char *)buff,size,std::ios::in),CGenArchiveIn(str) 
	  {
	  }
	virtual ~CStrArchiveIn()
	  {
	  }
  };

class CStrArchiveOut : public CGenArchiveOut
  {
    std::strstream str;
public:
	CStrArchiveOut(int expand=2048):CGenArchiveOut(str) 
	  {
	  }
	virtual ~CStrArchiveOut()
	  {
	  }
	void *Lock()
	  {
	  return (void *)str.str();
	  }
	void Unlock()
	  {
      str.freeze(false);
	  }
  };

#endif // !defined(AFX_GENARCHIVE_H__9D3F868E_1C20_42B2_815D_843B327E774C__INCLUDED_)


