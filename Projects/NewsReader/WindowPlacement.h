// WindowPlacement.h: interface for the CWindowPlacement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WINDOWPLACEMENT_H__AB404CA0_A0AA_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
#define AFX_WINDOWPLACEMENT_H__AB404CA0_A0AA_11D5_B3A0_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

enum EWP_mode {EWP_normal,EWP_maxonly, EWP_minonly, EWP_both};

class CWindowPlacement : public WINDOWPLACEMENT  
{
public:
	void Restore(HWND hWnd, EWP_mode mode, bool postcommand=true);
	void Retrieve(HWND hWnd);
	void Store(char *registryitem);
	void Load(char *registryitem);
	};

#endif // !defined(AFX_WINDOWPLACEMENT_H__AB404CA0_A0AA_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
