// MessCache.h: interface for the CMessCache class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSCACHE_H__29501D60_8B92_11D5_B39E_00C0DFAE7D0A__INCLUDED_)
#define AFX_MESSCACHE_H__29501D60_8B92_11D5_B39E_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define CACHEMAGIC 0x17A847B3

#include "DiskIndex2.h"

struct SMessFileIndex
  {
  char messid[124]; //message id (shorted)
  DWORD magic;   //magic nuber to detect this struct
  };

//--------------------------------------------------

struct SMessFileHead
  {
  DWORD posbeg; //offset of begin of cache
  DWORD fadd; //offset of first empty field;
  };

//--------------------------------------------------

//cbeg is offset in file, where next block will be placed
//ibeg is index to dir of next block will be used for
//iend is index to dir of first block will be released before area is reserved

#include <fstream>

class CMessCache  
  {
    std::fstream dta;
  DWORD maxsize;  
  bool read;
  bool open;
  char *fullname;
  char *lastmsgid;
  DWORD start;
  CDiskIndex2 index;
  public:
	  bool FindInCache(const char *messageid);
    void DeleteMessage(const char *messageid);
    bool MoveCache(const char *newname);
    bool IsCacheFull();
    CCriticalSection lock;
    std::iostream* WriteMessage(const char *messageid);
    void CloseMessage();
    std::iostream *OpenMessage(const char *messageid);
    bool InitCache(const char *pathname, DWORD maxsize=1024*1024);
    CMessCache();
    virtual ~CMessCache();
    void CloseCache() 
      {lock.Lock();dta.close();index.Close();lock.Unlock();}
    const char *GetFullName() 
      {return fullname;}
    DWORD GetCacheMaxSize() 
      {return maxsize;};
    DWORD GetCacheSize() 
      {
      CSingleLock sl(&lock,true);
      dta.seekp(0,std::ios::end);return dta.tellp();
      }
    
  };

//--------------------------------------------------

#endif // !defined(AFX_MESSCACHE_H__29501D60_8B92_11D5_B39E_00C0DFAE7D0A__INCLUDED_)
