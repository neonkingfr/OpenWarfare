// TextFind.h: interface for the CTextFind class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXTFIND_H__B58ECF40_AED5_11D5_B3A0_0050FCFE63F1__INCLUDED_)
#define AFX_TEXTFIND_H__B58ECF40_AED5_11D5_B3A0_0050FCFE63F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "iTextFind.h"

//! boolean search expression
class CTextFind: public ITextFind  
{  
  class FindOp
	{
	public:
	virtual bool Calculate(const char *text) {return false;}
	virtual ~FindOp() {}
	};

  class FindWord:public FindOp
	{
	int wpt;
	char **wordtab;
	public:
	  virtual bool Calculate(const char *text) 
		{
		char *wpst=wpt==-1?NULL:wordtab[0]+wpt;
		if (wpst==NULL) return false;
		if (text==NULL) {*wpst=-1;return false;}
		if (*wpst==-1) CTextFind::FindInText(wpst,text);
		return *wpst!=0;
		}
	  FindWord(char **wordtab,int wpt):wordtab(wordtab),wpt(wpt) {}
	};

  class FindUn:public FindOp
	{
	protected:
	FindOp *left, *right;
	public:
	  FindUn(FindOp *left, FindOp *right):left(left),right(right) {}
	  virtual ~FindUn() {delete left;delete right;}	  
	  virtual bool Calculate(const char *text)
		{left->Calculate(text);return right->Calculate(text);}
	};

  class FindAnd:public FindUn
	{
	public:
	  FindAnd(FindOp *left, FindOp *right):FindUn(left,right) {}
	  virtual bool Calculate(const char *text) 
		{
		if (text==NULL) return FindUn::Calculate(text);
		return left->Calculate(text) && right->Calculate(text);
		}
	};

  class FindOr:public FindUn
	{
	public:
	  FindOr(FindOp *left, FindOp *right):FindUn(left,right) {}
	  virtual bool Calculate(const char *text)
		{
		if (text==NULL) if (text==NULL) return FindUn::Calculate(text);
		return left->Calculate(text) || right->Calculate(text);
		}

	};

  class FindNot:public FindOp
	{
	  FindOp *op;
	public:
	  FindNot(FindOp *op):op(op) {}
	  virtual bool Calculate(const char *text) 
		{return !op->Calculate(text);}
	  virtual ~FindNot() {delete op;}	  
		
	};
  
  
  char *wordtab;
  int wpsize;
  FindOp *tree;
  
public:
	bool Find(const char *intext);
	bool CompileCriteria(const char *criteria);
	static void FindInText(char *wpst, const char *text);
	FindOp * DoNotOp(char **text);
	FindWord *InsertWord(char **text);
	CTextFind();
	bool IsCompiled() {return tree!=NULL;}
	virtual ~CTextFind();

protected:
	FindOp * DoAndOp(char **text);
	FindOp * DoOrOp(char **text);
};



#endif // !defined(AFX_TEXTFIND_H__B58ECF40_AED5_11D5_B3A0_0050FCFE63F1__INCLUDED_)
