#if !defined(AFX_FINDDLG_H__52714EE1_173C_40B7_99EB_0A420FB1EA88__INCLUDED_)
#define AFX_FINDDLG_H__52714EE1_173C_40B7_99EB_0A420FB1EA88__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FindDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFindDlg dialog

#define NRM_FILLLIST (WM_APP+1000)
#define NRM_SHOWWAITDLG (WM_APP+1001)

class CMessageList;

class CAccountList;
class CFindList ;
struct GRPFINDINFO;
class CNewsTerminal;
class CFindDlg : public CDialog
{
  // Construction
  CFindList *fndlist;
  CWinThread *findthrd;
  int _repeatSortTimer;
  CSize _minsize;
  CSize _lastsize;
  LPARAM _lastsort;
  CNewsGroup *searchGrp;
  bool _needresort;
  public:
    void NewFindThread();
    void DialogRules();
    void SortList();
    void ResetList();
    virtual  ~CFindDlg();
    void LoadFindList(CFindList *fl);
    virtual void OnCancel();
    virtual void OnOK();
    CNewsGroup *CreateSearchGrp(CNewsAccount *acc);
    void Open();
    CAccountList *acclist;
    CTreeCtrl *lefttree;
    CMessageList *msglist;
    CFindDlg(CWnd* pParent = NULL);   // standard constructor
    
    bool IsBusy();
    void FindExternalCall();

    char fndstatbuf[256];
    bool stop;
    bool forceclose;
    float ext_minScore;
    
    
    // Dialog Data
    //{{AFX_DATA(CFindDlg)
	enum { IDD = IDD_FIND };
	CDateTimeCtrl	wDateOldest;
	CDateTimeCtrl	wDateNewest;
	CComboBox	wSearchUsing;
    CButton	bnewsearch;
    CButton	bfind;
    CButton	bstop;
    CListCtrl	list;
    CString	from;
    CString	message;
    CString	subject;
    BOOL	allgroups;
    BOOL	connserver;
    BOOL	attch;
    BOOL	searchWizard;
	BOOL	resInGrp;
	int		vSearchMode;
	COleDateTime	vDateNewest;
	COleDateTime	vDateOldest;
	BOOL	vCheckDateOldest;
	BOOL	vCheckDateNewest;
	//}}AFX_DATA
    bool	substrings;
    int     ft_minScore;
    bool    ft_unknown; 
	CWnd	msgwnd;

    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CFindDlg)
   public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);
   protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
   protected:
    void DoFindRecurse(HTREEITEM it,GRPFINDINFO &grpinfo,CNewsTerminal &term);
    void FindFulltextThread();
    void SearchMode(bool search);

   public:
	   void LoadSettings();
	   void SaveSettings();
    // Generated message map functions
    //{{AFX_MSG(CFindDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnDestroy();
    afx_msg void OnStopfind();
    afx_msg void OnFindnow();
    afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
    afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnAllgroups();
    afx_msg void OnColumnclickList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnNewsearch();
    afx_msg void OnSearchwizard();
    afx_msg void OnBoolsearch();
    afx_msg void OnChangeCriteria();
    afx_msg void OnHasattchm();
    afx_msg LRESULT OnFillList(WPARAM wParam, LPARAM lParam);
    afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg LRESULT OnResetList(WPARAM wParam, LPARAM lParam);
	afx_msg void OnOptions();
	afx_msg void OnSelchangeSearchusing();
	afx_msg LRESULT ShowWaitMsg(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT UpdateByHLDA(WPARAM wParam, LPARAM lParam);
	afx_msg void OnChecknewest();
	afx_msg void OnCheckoldest();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnSmartwords();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FINDDLG_H__52714EE1_173C_40B7_99EB_0A420FB1EA88__INCLUDED_)
