// ThreadPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "ThreadPropDlg.h"
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CThreadPropDlg dialog
#define VALSNAME1 "thrdvals.ini"
#define VALSNAME2 "thrdvalsg.ini"

CThreadPropDlg::CThreadPropDlg(CWnd* pParent /*=NULL*/)
: CDialog(CThreadPropDlg::IDD, pParent)
  {
  for (int i=0;i<CTPD_LINECOUNT;i++)
    {
    value[i] = _T("");
    varible[i] = _T("");
    }
  //{{AFX_DATA_INIT(CThreadPropDlg)
  comment = FALSE;
  //}}AFX_DATA_INIT
  load=true;
  vallist=NULL;
  
  }

//--------------------------------------------------

void CThreadPropDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  for (int i=0;i<CTPD_LINECOUNT;i++)
    {
    DDX_Control(pDX, IDC_VARIBLE1+i, wVarible[i]);
    DDX_Control(pDX, IDC_VALUE1+i, wValue[i]);
    DDX_CBString(pDX, IDC_VALUE1+i, value[i]);
    DDX_CBString(pDX, IDC_VARIBLE1+i, varible[i]);
    }
  //{{AFX_DATA_MAP(CThreadPropDlg)
  DDX_Check(pDX, IDC_COMMENT, comment);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CThreadPropDlg, CDialog)
//{{AFX_MSG_MAP(CThreadPropDlg)
ON_WM_TIMER()
ON_BN_CLICKED(IDC_DELPAIR, OnDelpair)
ON_CBN_KILLFOCUS(IDC_VARIBLE1, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE2, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE3, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE4, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE5, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE6, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE7, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE8, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE9, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE10, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE11, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE12, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE13, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE14, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE15, OnEditchangeVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE1, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE2, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE3, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE4, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE5, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE6, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE7, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE8, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE9, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE10, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE11, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE12, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE13, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE14, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE15, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE1, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE2, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE3, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE4, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE5, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE6, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE7, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE8, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE9, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE10, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE11, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE12, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE13, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE14, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE15, OnEditupdateVarible1)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThreadPropDlg message handlers

void CThreadPropDlg::LoadValues(const char *file, bool global)
  {
  const char *name=theApp.FullPath(file,global);
  ifstream in(name,ios::in);
  if (!in) return;
  char buff[256];
  while (!in.eof())
    {
    buff[0]=0;
    buff[255]=0;
    ws(in);
    in.getline(buff,sizeof(buff),'~');
    if (buff[255]>=32) in.ignore(0x7fffffff,'~');
    if (!buff[0]) break;
    TName *nm=vallist->Find(buff);
    if (nm==NULL)
      {
      nm=new TName(buff);
      nm->next=vallist;vallist=nm;
      }
    int zn=in.get();
    while (zn!='#')
      {
      buff[0]=0;
      buff[255]=0;
      in.putback((char)zn);
      in.getline(buff,sizeof(buff),'~');
      if (buff[255]>=32) in.ignore(0x7fffffff,'~');
      if (!buff[0]) break;
      TName::Add(&nm,nm->name,buff,global);
      zn=in.get();
      }
    }
  }

//--------------------------------------------------

void CThreadPropDlg::SaveValues(const char *filename)
  {
  const char *name=theApp.FullPath(filename,false);
  ofstream out(name,ios::out|ios::trunc);
  if (!out) return;
  TName *nm=vallist;
  while (nm)
    {
    bool frst=true;
    TValue *vl=nm->values;
    while (vl)
      {
      if (!vl->nostore) 
        {
        if (frst) 	
          {out<<nm->name<<'~';frst=false;}
        out<<vl->value<<'~';
        }
      vl=vl->next;
      }
    out<<"#\n";
    nm=nm->next;
    }
  }

//--------------------------------------------------

BOOL CThreadPropDlg::OnInitDialog() 
  {
  int i;
  CDialog::OnInitDialog();
  
  if (load)
    {
    LoadValues(VALSNAME1,false);
    LoadValues(VALSNAME2,true);
    load=false;
    }
  for (i=0;i<CTPD_LINECOUNT;i++) LoadNamesCombo(wVarible[i]);
  ParseProperties();
  for (i=0;i<CTPD_LINECOUNT;i++) 
    if (varible[i].GetLength()!=0)
      LoadValuesCombo(wValue[i],varible[i],false);
    HideLines();
    SetTimer(10,500,NULL);
    lastid=0;
    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CThreadPropDlg::LoadNamesCombo(CComboBox &cb)
  {
  TName *pr=vallist;
  cb.ResetContent();
  while (pr)
    {
    cb.AddString(pr->name);
    pr=pr->next;
    }
  }

//--------------------------------------------------

void CThreadPropDlg::LoadValuesCombo(CComboBox &cb, const char *name, bool reset)
  {
  TValue *vl=vallist->FindValues(name);
  CString cont;
  cb.GetWindowText(cont);
  if (reset) cb.ResetContent();
  while (vl)
    {
    cb.AddString(vl->value);
    if (stricmp(vl->value,cont)==0) cb.SetWindowText(vl->value);
    vl=vl->next;
    }  
  }

//--------------------------------------------------

CThreadPropDlg::~CThreadPropDlg()
  {
  delete vallist;
  }

//--------------------------------------------------

void CThreadPropDlg::ParseProperties()
  {
  CStringArray arr;
  ParseProperties(properties,arr);
  for (int i=0;i<arr.GetSize();i++)
    {
    int pos=i/2;
    if (i & 1) 
      {
      wValue[pos].SetWindowText(arr.GetAt(i));
      value[pos]=arr.GetAt(i);
      }
    else
      {
      wVarible[pos].SetWindowText(arr.GetAt(i));
      varible[pos]=arr.GetAt(i);
      }
    }  
  }

//--------------------------------------------------

void CThreadPropDlg::ParseProperties(const char *line, CStringArray &array)
  {
  char buff[256];
  int c;
  istrstream in((char *)line);
  bool data=false;
  CString comd,dta;
  do
    {
    ostrstream out(buff,256);
    c=in.get();
    while (c!=':' && c!=',' && c!=EOF)
      {
      out.put((char)c);
      c=in.get();
      }
    out.put((char)0);
    if (c==':')
      {
      data=true;
      comd=buff;
      }
    else
      {
      if (data)
        {
        dta=buff;data=false;
        }
      else
        {
        comd=buff;dta="";
        }
      comd.TrimLeft();
      comd.TrimRight();
      dta.TrimLeft();
      dta.TrimRight();
      array.Add(comd);
      array.Add(dta);
      }
    }
    while (c!=EOF);
  }

//--------------------------------------------------

CString CThreadPropDlg::BuildProperties(CStringArray &array)
  {
  CString out;
  for(int i=0;i<array.GetSize();i++)
    {
    CString txt=array.GetAt(i);
    if (i==0) out=txt;
    else if (i & 1 ) 
      {if (txt!="") out=out+":"+txt;}
    else out=out+","+txt;
    }
  return out;
  }

//--------------------------------------------------

void CThreadPropDlg::HideLines()
  {  
  int i;
  for (i=CTPD_LINECOUNT-1;i>0;i--)
    {
    if (wVarible[i-1].GetWindowTextLength()==0)
      {
      wVarible[i].ShowWindow(SW_HIDE);
      wValue[i].ShowWindow(SW_HIDE);
      }  
    else
      break;
    }
  for (;i>=0;i--)
    {
    wVarible[i].ShowWindow(SW_SHOW);
    wValue[i].ShowWindow(SW_SHOW);
    }
  }

//--------------------------------------------------

void CThreadPropDlg::OnTimer(UINT nIDEvent) 
  {
  HideLines();	
  CDialog::OnTimer(nIDEvent);
  CWnd *wnd=GetFocus();
  if (IsWindow(wnd->GetSafeHwnd()))
    {
    wnd=wnd->GetParent();
    if (IsWindow(wnd->GetSafeHwnd()))
      {
      int id=wnd->GetDlgCtrlID();
      if ((id>=IDC_VARIBLE1 && id<IDC_VARIBLE1+CTPD_LINECOUNT)||
        (id>=IDC_VALUE1 && id<IDC_VALUE1+CTPD_LINECOUNT)) 
        lastid=id;  
      }
    }
  }

//--------------------------------------------------

void CThreadPropDlg::OnOK() 
  {
  CDialog::OnOK();
  CStringArray arr;
  for (int i=0;i<CTPD_LINECOUNT;i++)
    {
    value[i].TrimLeft();
    value[i].TrimRight();
    varible[i].TrimLeft();
    varible[i].TrimRight();	
    if (varible[i]!="")
      {
      int pos;
      while ((pos=varible[i].Find(' '))>=0) varible[i].SetAt(pos,'_');
      while ((pos=value[i].Find(' '))>=0) value[i].SetAt(pos,'_');
      if (value[i]!="") vallist->Add(&vallist,varible[i],value[i],false);
      arr.Add(varible[i]);
      arr.Add(value[i]);
      }
    }
  properties=BuildProperties(arr);
  SaveValues(VALSNAME1);
  }

//--------------------------------------------------

void CThreadPropDlg::OnDelpair() 
  {
  if (lastid!=0)
    {
    int id=lastid;
    if (id>=IDC_VALUE1) id=id-IDC_VALUE1;
    else id=id-IDC_VARIBLE1;	
    CString val;
    CString var;
    wValue[id].GetWindowText(val);
    wVarible[id].GetWindowText(var);
    if (var.GetLength()!=0)
      {
      CString msg;
      AfxFormatString2(msg,IDS_DELETETPPAIR,var,val);
      if (NrMessageBox(msg,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDYES)
        {
        TName *nam=vallist->Find(var);
        if (nam)
          {
          TValue *p=nam->values;
          if (p && stricmp(p->value,val)==0)
            {
            nam->values=p->next;
            delete p;
            }
          else
            {
            while (p->next!=NULL)
              {
              if (stricmp(p->next->value,val)==0)
                {
                TValue *q=p->next;
                p->next=q->next;
                delete q;
                break;
                }
              p=p->next;			
              }
            }
          if (nam->values==NULL)
            {
            TName *q=vallist;
            if (q==nam) 
              {
              vallist=q->next;
              }
            else
              {
              while (q && q->next!=nam) q=q->next;
              if (q->next==nam)
                {
                q->next=nam->next;				
                }
              }
            nam->next=NULL;
            delete nam;
            }
          }
        for (int i=0;i<CTPD_LINECOUNT;i++) 
          {
          wVarible[i].GetWindowText(var);
          LoadNamesCombo(wVarible[i]);	
          wVarible[i].SetWindowText(var);
          }
        SyncLists();
        wValue[id].SetWindowText("");
        }
      }
    }
  }

//--------------------------------------------------

void CThreadPropDlg::OnEditchangeVarible1() 
  {
  if (update)
    {
    const MSG *msg=GetCurrentMessage();
    int id=LOWORD(msg->wParam);
    SyncLists(id);
    update=false;
    }
  }

//--------------------------------------------------

void CThreadPropDlg::OnEditupdateVarible1() 
  {
  update=true;	
  }

//--------------------------------------------------

void CThreadPropDlg::SyncLists(int id)
  {
  CString var;
  CComboBox	*ccb=(CComboBox	*)GetDlgItem(id);
  ccb->GetWindowText(var);
  CComboBox	*ncb=(CComboBox	*)GetDlgItem(id+20);
  LoadValuesCombo(*ncb, var);
  }

//--------------------------------------------------

void CThreadPropDlg::SyncLists()
  {
  for (int i=0;i<CTPD_LINECOUNT;i++) SyncLists(IDC_VARIBLE1+i);
  }

//--------------------------------------------------

