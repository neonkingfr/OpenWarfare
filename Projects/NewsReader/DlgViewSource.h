#if !defined(AFX_DLGVIEWSOURCE_H__7ACC7166_5D2E_4875_9574_DDC2368A44C8__INCLUDED_)
#define AFX_DLGVIEWSOURCE_H__7ACC7166_5D2E_4875_9574_DDC2368A44C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgViewSource.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgViewSource dialog


class LineTerminal;
class CDlgViewSource : public CDialog
{
// Construction
	CString msg;
	CFont curier;
public:
	CDlgViewSource(CWnd* pParent = NULL);   // standard constructor

	LineTerminal *lterm;

// Dialog Data
	//{{AFX_DATA(CDlgViewSource)
	enum { IDD = IDD_VIEWSOURCE };
	CEdit	wSource;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgViewSource)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgViewSource)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGVIEWSOURCE_H__7ACC7166_5D2E_4875_9574_DDC2368A44C8__INCLUDED_)
