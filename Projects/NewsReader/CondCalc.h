// CondCalc.h: interface for the CCondCalc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONDCALC_H__799DB310_29DC_4203_9B15_1C1EA2F76ACD__INCLUDED_)
#define AFX_CONDCALC_H__799DB310_29DC_4203_9B15_1C1EA2F76ACD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define CCC_ZAVORKA1 "("
#define CCC_ZAVORKA2 ")"
#define CCC_AND "&"
#define CCC_OR "|"
#define CCC_EQUAL "=="
#define CCC_NEQUAL "!="
#define CCC_INVERT "!"
#define CCC_SYMBSIZE 256

class CCondCalc  
{
  public:
  class _Operator
	{
	public:
	  virtual bool Calculate(const char *inputLine)=0;
    virtual void Output(std::ostream &out)=0;
	  virtual int Priority()=0;
	  virtual ~_Operator() {};
	};

  class _UnOp: public _Operator
	{
	protected:
	_Operator *next;
	public:
	  _UnOp(_Operator *next):next(next) {};
	  virtual ~_UnOp() {delete next;}	  
	};  

  class _BinOp: public _Operator
	{
	protected:
	_Operator *left,*right;
	public:
	  _BinOp(_Operator *l, _Operator *r):left(l),right(r) {}
	  virtual ~_BinOp() {delete left;delete right;}
	};
  

  class NegateOp: public _UnOp
	{
	public:
	  NegateOp(_Operator *op):_UnOp(op) {}
	  bool Calculate(const char *inputLine) {return !next->Calculate(inputLine);}
	  int Priority() {return 10;}
    void Output(std::ostream &out)
		{
		out<<CCC_INVERT;
		if (Priority()>next->Priority()) out<<" "CCC_ZAVORKA1" ";
		 next->Output(out);
		if (Priority()>next->Priority()) out<<" "CCC_ZAVORKA2;
		}
	};

  class AndOp: public _BinOp
	{
	public:
	  AndOp(_Operator *l,_Operator *r):_BinOp(l,r) {}
	  bool Calculate(const char *inputLine) 
		{
		if (left->Calculate(inputLine)) return right->Calculate(inputLine);
		return false;
		}
	  int Priority() {return 5;}
    void Output(std::ostream &out)
		{
		if (Priority()>left->Priority()) {out<<CCC_ZAVORKA1" "; left->Output(out); out<<" "CCC_ZAVORKA2;}
		else left->Output(out);
		out<<" "CCC_AND" ";
		if (Priority()>right->Priority()) {out<<CCC_ZAVORKA1" "; right->Output(out); out<<" "CCC_ZAVORKA2;}
		else right->Output(out);
		}
	};

  class OrOp: public _BinOp
	{
	public:
	  OrOp(_Operator *l, _Operator *r):_BinOp(l,r) {}
	  bool Calculate(const char *inputLine) 
		{
		if (left->Calculate(inputLine))  return true;
		return right->Calculate(inputLine);
		}
	  int Priority() {return 4;}
    void Output(std::ostream &out)
		{
		if (Priority()>left->Priority()) {out<<CCC_ZAVORKA1" "; left->Output(out); out<<" "CCC_ZAVORKA2;}
		else left->Output(out);
		out<<" "CCC_OR" ";
		if (Priority()>right->Priority()) {out<<CCC_ZAVORKA1" "; right->Output(out); out<<" "CCC_ZAVORKA2;}
		else right->Output(out);
		}
	};

  class SingleParm: public _Operator
	{
	protected:
	char *parmname;	
	public:
	  SingleParm(const char *name) {parmname=strdup(name);}
	  ~SingleParm() {free(parmname);}
	  bool Calculate(const char *inputLine)
		{
		return CCondCalc::FindParam(inputLine,parmname);
		}
	  int Priority() {return 10;}
    void Output(std::ostream &out)
		{
		out<<parmname;
		}
	};

  class Error: public SingleParm
	{
	public:
	  Error():SingleParm("<parm?>") {}
  	  bool Calculate(const char *inputLine)
		{
		return false;
		}
	};

    class ErrorOp: public _UnOp
	{
	public:
	  ErrorOp(_Operator *below):_UnOp(below) {};
  	  bool Calculate(const char *inputLine)
		{
		return false;
		}
    void Output(std::ostream &str)
		{
		next->Output(str);
		str<<" <op?>";
		}
	  int Priority() {return next->Priority();}
	};

  
  class SingleParmValue: public SingleParm
	{
	protected:
	  char *value;
	  bool equ;
	public:
	  SingleParmValue(const char *parm, const char *val, bool eq):SingleParm(parm),equ(eq)
		{value=strdup(val);}
	  ~SingleParmValue() {free(value);}
	  bool Calculate(const char *inputLine)
		{
		if (equ)
		  return CCondCalc::CheckParamValue(inputLine,parmname,value);
		else
		  return CCondCalc::CheckParamValue(inputLine,parmname,value)==false &&
			CCondCalc::FindParam(inputLine,parmname)==true;
			
		}
	  int Priority() {return 9;}
    void Output(std::ostream &out)
		{
		SingleParm::Output(out);
		if (equ) out<<CCC_EQUAL;else out<<CCC_NEQUAL;
		out<<value;
		}
	};

  enum SymbType {token,symbol,fin,error};

  struct SymbInfo
	{
	char name[CCC_SYMBSIZE];
	SymbType type;
	};

	protected:
_Operator *compiled;
public:
	bool Compile(const char *line);
  void Print(std::ostream &out);
	int Evalute(const char *line) ;
	CCondCalc();
	virtual ~CCondCalc();
	bool IsCompiled() {return compiled!=NULL;}

static bool FindParam(const char *line, const char *param);
static bool CheckParamValue(const char *line, const char *param, const char *value);

};

#endif // !defined(AFX_CONDCALC_H__799DB310_29DC_4203_9B15_1C1EA2F76ACD__INCLUDED_)
