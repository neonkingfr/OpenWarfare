// ScanArticlesThread.h: interface for the CScanArticlesThread class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCANARTICLESTHREAD_H__E6DD4C32_BC2A_4C8B_BF96_6EF3EC23ACC4__INCLUDED_)
#define AFX_SCANARTICLESTHREAD_H__E6DD4C32_BC2A_4C8B_BF96_6EF3EC23ACC4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "NewsAccount.h"

class CScanArticlesThread:public CRunner
{	
  CNewsGroup *grp;
  CNewsAccount *acc;
public:
	virtual void Run();
  CScanArticlesThread(CNewsAccount *a, CNewsGroup *g):grp(g),acc(a) {a->Lock();}


};

#endif // !defined(AFX_SCANARTICLESTHREAD_H__E6DD4C32_BC2A_4C8B_BF96_6EF3EC23ACC4__INCLUDED_)
