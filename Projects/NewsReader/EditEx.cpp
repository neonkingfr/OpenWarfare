// EditEx.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "EditEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditEx

CEditEx::CEditEx()
{
lastident=0;
smartident=false;
}

CEditEx::~CEditEx()
{
}


BEGIN_MESSAGE_MAP(CEditEx, CEdit)
	//{{AFX_MSG_MAP(CEditEx)
	ON_WM_GETDLGCODE()
	ON_WM_CHAR()
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditEx message handlers

UINT CEditEx::OnGetDlgCode() 
  {
	
  return DLGC_WANTALLKEYS;
  }

void CEditEx::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
  {
  switch (nChar)
  	{
	case 9: 
	  if (GetKeyState(VK_SHIFT) & 0x80)
		{
        if (GetSel()==0)
          {
          CWnd *prev=GetParent()->GetNextDlgTabItem(this,TRUE);
          prev->SetFocus();
          }
        else
          {
		  int p=GetBeginLinePos();
		  if (p)
		    {
		    p=((p-1)/4)*4;
		    p+=LineIndex();
		    SetSel(p,p);
		    }
          }
		}
	  else
		{
        if (GetSel()==0)
          {
          CWnd *prev=GetParent()->GetNextDlgTabItem(this,FALSE);
          prev->SetFocus();
          }
        else
          {
		  int p=GetBeginLinePos();
  		  p=((p+4)/4)*4-p;
		  ReplaceSel(("    ")+(4-p),TRUE);
          }
		}
	  break;
	case 13:
	  {
	  int p=CalculateIdent();
	  if (p && p==lastident) smartident=true;
	  else if (p==0 && lastident==0) smartident=false;
	  if (smartident)
		{
		char *s=(char *)alloca(p+3);
		s[0]='\r';
		s[1]='\n';
		memset(s+2,32,p);
		s[p+2]=0;
		ReplaceSel(s,TRUE);
		}
	  else CEdit::OnChar(nChar, nRepCnt, nFlags);
	  lastident=p;
	  break;
	  }
	case 1: SetSel(0,-1);break;
	default:CEdit::OnChar(nChar, nRepCnt, nFlags);
	}
  }

int CEditEx::GetBeginLinePos()
  {
  int start,end;
  GetSel(start,end);
  int p=__min(start,end);
  int l=LineIndex(LineFromChar(p));
  return p-l;
  }

int CEditEx::CalculateIdent()
  {
  char buff[256];
  int cnt=GetLine(LineFromChar(),buff,sizeof(buff));
  int p=0;
  while (p<cnt && buff[p]<33) p++;
  return __min(p,cnt);
  }

void CEditEx::OnContextMenu(CWnd* pWnd, CPoint point) 
  {
  CMenu mnu;
  mnu.LoadMenu(IDR_POPUPS);
  CMenu *pop=mnu.GetSubMenu(2);
  int q=(int)pop->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON|TPM_NONOTIFY|TPM_RETURNCMD,
	point.x, point.y,this,NULL);
  switch(q)
	{
	case ID_EDIT_UNDO: SendMessage(WM_UNDO,0,0);break;
	case ID_EDIT_COPY: SendMessage(WM_COPY,0,0);break;
	case ID_EDIT_CUT: SendMessage(WM_CUT,0,0);break;
	case ID_EDIT_PASTE: SendMessage(WM_PASTE,0,0);break;
	case ID_EDIT_CLEAR: SendMessage(WM_CLEAR,0,0);break;
	case ID_EDIT_SELECT_ALL: SetSel(0,-1);break;
	case ID_EDITPOPUP_FONT: 
	case ID_EDITPOPUP_COLOR: GetParent()->SendMessage(WM_COMMAND,q,NULL);break;
	}
  }
