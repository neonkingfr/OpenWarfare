# Microsoft Developer Studio Project File - Name="NewsReader" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=NewsReader - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "NewsReader.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "NewsReader.mak" CFG="NewsReader - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "NewsReader - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "NewsReader - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "NewsReader - Win32 Demo" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/NewsReader", GWAAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /Zi /O2 /I "..\..\extern" /D "NDEBUG" /D "_RELEASE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "MFC_NEW" /D "USESOCKETCLASS" /FR /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 winmm.lib rsamt.lib /nologo /subsystem:windows /map /debug /machine:I386 /libpath:"..\..\extern"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /w /W0 /Gm /GR /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "MFC_NEW" /D "USESOCKETCLASS" /Fr /FD /GZ /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /win32
# ADD BASE RSC /l 0x405 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 winmm.lib rsamt.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"..\..\extern"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "NewsReader___Win32_Demo"
# PROP BASE Intermediate_Dir "NewsReader___Win32_Demo"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "NewsReader___Win32_Demo"
# PROP Intermediate_Dir "NewsReader___Win32_Demo"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GR /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /Zi /O2 /D "NDEBUG" /D "DEMO" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "MFC_NEW" /D "USESOCKETCLASS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT BASE LINK32 /map
# ADD LINK32 winmm.lib /nologo /subsystem:windows /debug /machine:I386 /out:"NewsReader___Win32_Demo/NewsReaderDemo.exe"
# SUBTRACT LINK32 /map

!ENDIF 

# Begin Target

# Name "NewsReader - Win32 Release"
# Name "NewsReader - Win32 Debug"
# Name "NewsReader - Win32 Demo"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AccountList.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\AccountPropDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ArchiveBinaryCommented.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\archivestream.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamBase64Filter.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamFileMapping.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamMemory.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamTCP.cpp
# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamTCP.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamWindowsFile.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ArticleDownload.cpp
# End Source File
# Begin Source File

SOURCE=.\AskWorkMode.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\AutoDeleteDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CacheMaintanceDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\CharMapCz.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ChildView.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\CloseThreadDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\CompactCacheDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\CompletteThreadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CondCalc.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ConfigDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ContitionsDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\CriticalSection.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\es\memory\debugAlloc.cpp
# End Source File
# Begin Source File

SOURCE=.\DiskIndex2.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgSortModeAsk.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgViewSource.cpp
# End Source File
# Begin Source File

SOURCE=.\DogWindow.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\DownloadProgress.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\EditAccountDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\EditEx.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\EditRulesDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\es\memory\fastAlloc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=.\FindDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\FreezeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FullTextIndex.cpp
# End Source File
# Begin Source File

SOURCE=.\GenArchive.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\GetUrlDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\GotoRefDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\GroupDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\HashTable.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\htmldocument2.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\htmlelement.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\IEDispPropDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\IPA.cpp
# End Source File
# Begin Source File

SOURCE=.\iTextFind.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\KeyCheck.cpp
# End Source File
# Begin Source File

SOURCE=.\LastReadDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\LineTerminal2.cpp
# End Source File
# Begin Source File

SOURCE=.\LostFoundDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MailHeadersScanner.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MailList.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\mapisend.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\FreeOnDemand\memFreeReq.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\FreeOnDemand\memFreeReqUseDefault.cpp
# End Source File
# Begin Source File

SOURCE=.\MessageHistory.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MessageList.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MessagePart.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MessagePartCache.cpp
# End Source File
# Begin Source File

SOURCE=.\MessCache.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MMapBTree.cpp
# End Source File
# Begin Source File

SOURCE=.\MsgBox.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MsgIdList.cpp
# End Source File
# Begin Source File

SOURCE=.\MsgPrevewDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\NewAccWiz.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\NewsAccount.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\NewsArticle.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\NewsReader.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\NewsTerminal.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\NRNotify.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\PathBrowser.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Pathname\Pathname.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\perfLog.cpp
# End Source File
# Begin Source File

SOURCE=.\POPSession.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ProgessDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ProxySetupDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qbstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qstreamUseBankDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qstreamUseFServerDefault.cpp
# End Source File
# Begin Source File

SOURCE=.\ReadWriteLock.cpp
# End Source File
# Begin Source File

SOURCE=..\..\es\strings\rstring.cpp
# End Source File
# Begin Source File

SOURCE=.\ScanArticlesThread.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ScanGroupsThread.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SelKwdSet.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SendDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SendingDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SendOptionsDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\cdkey\serial.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\serializeBin.cpp
# End Source File
# Begin Source File

SOURCE=.\smartFind.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Socket.cpp
# End Source File
# Begin Source File

SOURCE=.\Splash.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SplashWnd.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SplitWindow.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /GR /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\ssCompress.cpp
# End Source File
# Begin Source File

SOURCE=.\text2html.cpp
# End Source File
# Begin Source File

SOURCE=.\TextFind.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ThreadPropDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ThreadPropExDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UpdateProgressDlg.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\es\framework\useAppFrameDefault.cpp
# End Source File
# Begin Source File

SOURCE=.\Web.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\webbrowser2.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\WindowPlacement.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\WinSharedMemory.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AccountList.h
# End Source File
# Begin Source File

SOURCE=.\AccountPropDlg.h
# End Source File
# Begin Source File

SOURCE=.\Archive.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveBinaryCommented.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveInline.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveParamFile.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveStream.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamBase64Filter.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamFileMapping.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamMemory.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveStreamWindowsFile.h
# End Source File
# Begin Source File

SOURCE=.\ArticleDownload.h
# End Source File
# Begin Source File

SOURCE=.\AskWorkMode.h
# End Source File
# Begin Source File

SOURCE=.\AutoDeleteDlg.h
# End Source File
# Begin Source File

SOURCE=..\BTree\btree.h
# End Source File
# Begin Source File

SOURCE=.\BTreeDB.h
# End Source File
# Begin Source File

SOURCE=.\CacheMaintanceDlg.h
# End Source File
# Begin Source File

SOURCE=.\CharMapCz.h
# End Source File
# Begin Source File

SOURCE=.\ChildView.h
# End Source File
# Begin Source File

SOURCE=.\CloseThreadDlg.h
# End Source File
# Begin Source File

SOURCE=.\CompactCacheDlg.h
# End Source File
# Begin Source File

SOURCE=.\CompletteThreadDlg.h
# End Source File
# Begin Source File

SOURCE=.\CondCalc.h
# End Source File
# Begin Source File

SOURCE=.\ConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\ContitionsDlg.h
# End Source File
# Begin Source File

SOURCE=.\CriticalSection.h
# End Source File
# Begin Source File

SOURCE=.\DiskIndex2.h
# End Source File
# Begin Source File

SOURCE=.\DlgSortModeAsk.h
# End Source File
# Begin Source File

SOURCE=.\DlgViewSource.h
# End Source File
# Begin Source File

SOURCE=.\DogWindow.h
# End Source File
# Begin Source File

SOURCE=.\DownloadProgress.h
# End Source File
# Begin Source File

SOURCE=.\EditAccountDlg.h
# End Source File
# Begin Source File

SOURCE=.\EditEx.h
# End Source File
# Begin Source File

SOURCE=.\EditRulesDlg.h
# End Source File
# Begin Source File

SOURCE=.\FindDlg.h
# End Source File
# Begin Source File

SOURCE=.\FreezeDlg.h
# End Source File
# Begin Source File

SOURCE=.\FullTextIndex.h
# End Source File
# Begin Source File

SOURCE=.\GenArchive.h
# End Source File
# Begin Source File

SOURCE=.\GetUrlDlg.h
# End Source File
# Begin Source File

SOURCE=.\GotoRefDlg.h
# End Source File
# Begin Source File

SOURCE=.\GroupDlg.h
# End Source File
# Begin Source File

SOURCE=.\HashTable.h
# End Source File
# Begin Source File

SOURCE=.\htmldocument2.h
# End Source File
# Begin Source File

SOURCE=.\htmlelement.h
# End Source File
# Begin Source File

SOURCE=.\IArchive.h
# End Source File
# Begin Source File

SOURCE=.\IEDispPropDlg.h
# End Source File
# Begin Source File

SOURCE=.\IPA.h
# End Source File
# Begin Source File

SOURCE=.\ISharedMemory.h
# End Source File
# Begin Source File

SOURCE=.\iTextFind.h
# End Source File
# Begin Source File

SOURCE=.\KeyCheck.h
# End Source File
# Begin Source File

SOURCE=.\LastReadDlg.h
# End Source File
# Begin Source File

SOURCE=.\LineTerminal2.h
# End Source File
# Begin Source File

SOURCE=.\LostFoundDlg.h
# End Source File
# Begin Source File

SOURCE=.\LZWC.H
# End Source File
# Begin Source File

SOURCE=.\MailHeadersScanner.h
# End Source File
# Begin Source File

SOURCE=.\MailList.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\mapisend.h
# End Source File
# Begin Source File

SOURCE=.\MessageHistory.h
# End Source File
# Begin Source File

SOURCE=.\MessageList.h
# End Source File
# Begin Source File

SOURCE=.\MessagePart.h
# End Source File
# Begin Source File

SOURCE=.\MessagePartCache.h
# End Source File
# Begin Source File

SOURCE=.\MessCache.h
# End Source File
# Begin Source File

SOURCE=.\MMapBTree.h
# End Source File
# Begin Source File

SOURCE=.\MsgBox.h
# End Source File
# Begin Source File

SOURCE=.\MsgIdList.h
# End Source File
# Begin Source File

SOURCE=.\MsgPrevewDlg.h
# End Source File
# Begin Source File

SOURCE=.\NewAccWiz.h
# End Source File
# Begin Source File

SOURCE=.\NewsAccount.h
# End Source File
# Begin Source File

SOURCE=.\NewsArticle.h
# End Source File
# Begin Source File

SOURCE=.\NewsReader.h
# End Source File
# Begin Source File

SOURCE=.\NewsTerminal.h
# End Source File
# Begin Source File

SOURCE=.\NRNotify.h
# End Source File
# Begin Source File

SOURCE=.\PathBrowser.h
# End Source File
# Begin Source File

SOURCE=..\..\El\Pathname\Pathname.h
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\perfProf.hpp
# End Source File
# Begin Source File

SOURCE=.\POPSession.h
# End Source File
# Begin Source File

SOURCE=.\ProgessDlg.h
# End Source File
# Begin Source File

SOURCE=.\ProxySetupDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=.\ReadWriteLock.h
# End Source File
# Begin Source File

SOURCE=.\ScanArticlesThread.h
# End Source File
# Begin Source File

SOURCE=.\ScanGroupsThread.h
# End Source File
# Begin Source File

SOURCE=.\SelKwdSet.h
# End Source File
# Begin Source File

SOURCE=.\SendDlg.h
# End Source File
# Begin Source File

SOURCE=.\SendingDlg.h
# End Source File
# Begin Source File

SOURCE=.\SendOptionsDlg.h
# End Source File
# Begin Source File

SOURCE=SharedResource.h
# End Source File
# Begin Source File

SOURCE=.\SmartFind.h
# End Source File
# Begin Source File

SOURCE=.\Socket.h
# End Source File
# Begin Source File

SOURCE=.\Splash.h
# End Source File
# Begin Source File

SOURCE=.\SplashWnd.h
# End Source File
# Begin Source File

SOURCE=.\SplitWindow.h
# End Source File
# Begin Source File

SOURCE=.\TextFind.h
# End Source File
# Begin Source File

SOURCE=.\ThreadPropDlg.h
# End Source File
# Begin Source File

SOURCE=.\ThreadPropExDlg.h
# End Source File
# Begin Source File

SOURCE=.\UpdateProgressDlg.h
# End Source File
# Begin Source File

SOURCE=.\Web.h
# End Source File
# Begin Source File

SOURCE=.\webbrowser2.h
# End Source File
# Begin Source File

SOURCE=.\WindowPlacement.h
# End Source File
# Begin Source File

SOURCE=.\WinSharedMemory.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\attachic.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bigmainf.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\blank.gif
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\delete.ico
# End Source File
# Begin Source File

SOURCE=.\res\doganim1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\doganim3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\doganim4.bmp
# End Source File
# Begin Source File

SOURCE=.\res\doganim6.bmp
# End Source File
# Begin Source File

SOURCE=.\res\doganim7.bmp
# End Source File
# Begin Source File

SOURCE=.\res\downicon.ico
# End Source File
# Begin Source File

SOURCE=.\res\download.gif
# End Source File
# Begin Source File

SOURCE=.\res\empty.ico
# End Source File
# Begin Source File

SOURCE=.\res\enableht.ico
# End Source File
# Begin Source File

SOURCE=.\res\goto.ico
# End Source File
# Begin Source File

SOURCE=.\res\hourglass.gif
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00003.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00004.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icons.bmp
# End Source File
# Begin Source File

SOURCE=.\res\insertfi.ico
# End Source File
# Begin Source File

SOURCE=.\res\inserturl.ico
# End Source File
# Begin Source File

SOURCE=.\res\meserr.gif
# End Source File
# Begin Source File

SOURCE=.\res\movedown.ico
# End Source File
# Begin Source File

SOURCE=.\res\moveup.ico
# End Source File
# Begin Source File

SOURCE=.\res\My_Fish_Green.ico
# End Source File
# Begin Source File

SOURCE=.\res\NewsReader.ico
# End Source File
# Begin Source File

SOURCE=.\res\NewsReader.rc2
# End Source File
# Begin Source File

SOURCE=.\NewsReader.rgs
# End Source File
# Begin Source File

SOURCE=.\res\newsreader_splash.bmp
# End Source File
# Begin Source File

SOURCE=.\nodrop.cur
# End Source File
# Begin Source File

SOURCE=.\res\notify.ico
# End Source File
# Begin Source File

SOURCE=.\res\notifystop.ico
# End Source File
# Begin Source File

SOURCE=.\res\offline.gif
# End Source File
# Begin Source File

SOURCE=.\res\print.ico
# End Source File
# Begin Source File

SOURCE=.\res\referenc.ico
# End Source File
# Begin Source File

SOURCE=.\res\reply.ico
# End Source File
# Begin Source File

SOURCE=.\res\replymai.ico
# End Source File
# Begin Source File

SOURCE=.\resource.hm
# End Source File
# Begin Source File

SOURCE=.\res\see.ico
# End Source File
# Begin Source File

SOURCE=.\res\send.bmp
# End Source File
# Begin Source File

SOURCE=.\res\sendicon.ico
# End Source File
# Begin Source File

SOURCE=.\res\sleep.ico
# End Source File
# Begin Source File

SOURCE=.\res\splanim.bmp
# End Source File
# Begin Source File

SOURCE=.\res\sponka.ico
# End Source File
# Begin Source File

SOURCE=.\res\synchron.bmp
# End Source File
# Begin Source File

SOURCE=.\test.ico
# End Source File
# Begin Source File

SOURCE=.\res\tolong.gif
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\upicon.ico
# End Source File
# Begin Source File

SOURCE=.\res\uptoconv.gif
# End Source File
# Begin Source File

SOURCE=.\res\vlastnos.ico
# End Source File
# End Group
# Begin Group "Es"

# PROP Default_Filter ""
# Begin Group "Types"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Types\pointers.hpp
# End Source File
# End Group
# Begin Group "Framework"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\debugLog.hpp
# End Source File
# End Group
# Begin Group "Threads"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\es\Threads\threadSync.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# End Group
# Begin Group "memory"

# PROP Default_Filter ""
# End Group
# Begin Group "strings"

# PROP Default_Filter ""
# End Group
# Begin Group "Containers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\ES\Containers\bankArray.hpp
# End Source File
# End Group
# End Group
# Begin Group "El"

# PROP Default_Filter ""
# Begin Group "Text"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Text\wordCompare.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Text\wordCompare.hpp
# End Source File
# End Group
# Begin Group "Debugging"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Debugging\debugTrap.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\debugUseAppInfoDefault.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\imexhnd.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\imexhnd.h
# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\mapFile.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\mapFile.hpp
# End Source File
# End Group
# Begin Group "CRC"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\CRC\crc.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# End Group
# Begin Group "QStream"

# PROP Default_Filter ""
# End Group
# Begin Group "Common"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Common\globalAlive.cpp

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# End Group
# Begin Source File

SOURCE=..\..\EL\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iClassDb.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iEval.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iLocalize.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iPreproc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iSumCalc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Common\langExt.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Types\lLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\FreeOnDemand\memFreeReq.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\rStringArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamArchive\serializeClass.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\TCPIPBasics\SharedResource.h
# End Source File
# Begin Source File

SOURCE=..\..\EL\TCPIPBasics\Socket.h
# End Source File
# Begin Source File

SOURCE=..\..\ES\Types\softLinks.hpp
# End Source File
# End Group
# Begin Group "Misc"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\es\containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\strings\bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\memory\checkmem.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\Files\commandLine.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\containers\compactbuf.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\CRC\crc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\memory\debugAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\memory\debugnew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\debugTrap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\elementpch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\memory\fastalloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\Files\filenames.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\containers\forEach.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\containers\hashmap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Interfaces\iAppInfo.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\iQFBank.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\containers\list.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\containers\listbidir.hpp
# End Source File
# Begin Source File

SOURCE=.\LZWC.C

!IF  "$(CFG)" == "NewsReader - Win32 Release"

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "NewsReader - Win32 Demo"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\es\memory\memalloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\types\memtype.h
# End Source File
# Begin Source File

SOURCE=..\..\es\Threads\multisync.hpp
# End Source File
# Begin Source File

SOURCE=.\NewsReader.rc
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\normalConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\es\memory\normalnew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\perfLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\platform.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\algorithms\qsort.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\..\es\strings\rstring.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\types\scopeLock.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\containers\staticArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\stdIncludes.h
# End Source File
# Begin Source File

SOURCE=..\..\es\Threads\threadSync.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\containers\typedefines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\containers\typeopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\es\common\win.h
# End Source File
# End Group
# Begin Group "StdAfx"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\res\blank.htm
# End Source File
# Begin Source File

SOURCE=.\res\html1.htm
# End Source File
# Begin Source File

SOURCE=.\res\htmleditor.htm
# End Source File
# Begin Source File

SOURCE=.\res\loadfull.htm
# End Source File
# Begin Source File

SOURCE=.\res\loading.htm
# End Source File
# Begin Source File

SOURCE=.\res\messerr.htm
# End Source File
# Begin Source File

SOURCE=.\res\noselect.htm
# End Source File
# Begin Source File

SOURCE=.\res\offline.htm
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\res\tolong.htm
# End Source File
# End Target
# End Project
# Section NewsReader : {D30C1661-CDAF-11D0-8A3E-00C04FC9E26E}
# 	2:5:Class:CWebBrowser2
# 	2:10:HeaderFile:webbrowser2.h
# 	2:8:ImplFile:webbrowser2.cpp
# End Section
# Section NewsReader : {8856F961-340A-11D0-A96B-00C04FD705A2}
# 	2:21:DefaultSinkHeaderFile:webbrowser2.h
# 	2:16:DefaultSinkClass:CWebBrowser2
# End Section
# Section NewsReader : {0050004F-0045-0052-5400-59005F005200}
# 	1:14:IDR_NEWSREADER:102
# End Section
