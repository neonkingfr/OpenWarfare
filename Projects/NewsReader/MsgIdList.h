// MsgIdList.h: interface for the CMsgIdList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MSGIDLIST_H__809B28C4_ECCF_4ABF_B6F1_A041A21F87D0__INCLUDED_)
#define AFX_MSGIDLIST_H__809B28C4_ECCF_4ABF_B6F1_A041A21F87D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Projects/BTree/BTree.h>
#include "NewsArticle.h"

class CMsgIdList  
{
    struct IDInfo
    {
      Ref<CNewsArtHolder> hld;
      int Compare(const IDInfo &other) const
      {
        if (hld==NULL || other.hld==NULL) return (hld.GetRef()>other.hld.GetRef())-(hld.GetRef()<other.hld.GetRef());
        return stricmp(hld->messid,other.hld->messid);
      }
      bool operator==(const IDInfo &other) const {return Compare(other)==0;}
      bool operator>=(const IDInfo &other) const {return Compare(other)>=0;}
      bool operator<=(const IDInfo &other) const {return Compare(other)<=0;}
      bool operator!=(const IDInfo &other) const {return Compare(other)!=0;}
      bool operator>(const IDInfo &other) const {return Compare(other)>0;}
      bool operator<(const IDInfo &other) const {return Compare(other)<0;}
      IDInfo(CNewsArtHolder *p=NULL):hld(p) {}
      IDInfo &operator=(int zero) {hld=NULL;return *this;}
    };

    struct IDXCancel
    {
      Ref<CNewsArtHolder> hld;
      int Compare(const IDXCancel &other) const
      {
        if (hld==NULL || other.hld==NULL) return (hld.GetRef()>other.hld.GetRef())-(hld.GetRef()<other.hld.GetRef());
        return stricmp(hld->Xcancel,other.hld->Xcancel);
      }
      bool operator==(const IDXCancel &other) const {return Compare(other)==0;}
      bool operator>=(const IDXCancel &other) const {return Compare(other)>=0;}
      bool operator<=(const IDXCancel &other) const {return Compare(other)<=0;}
      bool operator!=(const IDXCancel &other) const {return Compare(other)!=0;}
      bool operator>(const IDXCancel &other) const {return Compare(other)>0;}
      bool operator<(const IDXCancel &other) const {return Compare(other)<0;}
      IDXCancel(CNewsArtHolder *p=NULL):hld(p) {}
      IDXCancel&operator=(int zero) {hld=NULL;return *this;}
    };

    BTree<IDInfo> _idinfo;
    BTree<IDXCancel> _xcancel;
    CCriticalSection _mt;

public:
    void Reset() {_idinfo.Clear();}    
	CNewsArtHolder * Find(const char *msgid);
	bool Add(CNewsArtHolder *hld);
	bool IsEmpty() {return _idinfo.Size()==0;}

};

#endif // !defined(AFX_MSGIDLIST_H__809B28C4_ECCF_4ABF_B6F1_A041A21F87D0__INCLUDED_)
