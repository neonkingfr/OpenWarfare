// SendOptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "SendOptionsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSendOptionsDlg dialog


CSendOptionsDlg::CSendOptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSendOptionsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSendOptionsDlg)
	server = _T("");
	port = 0;
	cctome = FALSE;
	//}}AFX_DATA_INIT
}


void CSendOptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSendOptionsDlg)
	DDX_Text(pDX, IDC_SMTPSERVER, server);
	DDX_Text(pDX, IDC_PORT, port);
	DDV_MinMaxUInt(pDX, port, 0, 65535);
	DDX_Check(pDX, IDC_CCTOME, cctome);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSendOptionsDlg, CDialog)
	//{{AFX_MSG_MAP(CSendOptionsDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSendOptionsDlg message handlers

#define GROUP "SendOptions"
#define SMTP "SMTP Server"
#define PORT "SMTP Port"
#define CCTOME "CCToMe"

void CSendOptionsDlg::LoadConfig()
  {
  port=theApp.GetProfileInt(GROUP,PORT,25);
  server=theApp.GetProfileString(GROUP,SMTP,"(none)");
  cctome=theApp.GetProfileInt(GROUP,CCTOME,1)!=0;
  }

void CSendOptionsDlg::SaveConfig()
  {
  theApp.WriteProfileString(GROUP,SMTP,server);
  theApp.WriteProfileInt(GROUP,PORT,port);
  theApp.WriteProfileInt(GROUP,CCTOME,(int)cctome);
  }
