// GotoRefDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "NewsAccount.h"
#include "GotoRefDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGotoRefDlg dialog


CGotoRefDlg::CGotoRefDlg(CWnd* pParent /*=NULL*/,int idd /*=IDD_GOTOREF*/)
  : CDialog(idd, pParent)
    {
    //{{AFX_DATA_INIT(CGotoRefDlg)
    ref = _T("");
    //}}AFX_DATA_INIT
    acc=NULL;
    _do_not_paste=false;
    }

//--------------------------------------------------

void CGotoRefDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CGotoRefDlg)
  DDX_Text(pDX, IDC_EDIT1, ref);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CGotoRefDlg, CDialog)
  //{{AFX_MSG_MAP(CGotoRefDlg)
	ON_EN_CHANGE(IDC_EDIT1, OnChangeEdit1)
	ON_BN_CLICKED(IDC_PASTE, OnPaste)
	//}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CGotoRefDlg message handlers
    

BOOL CGotoRefDlg::OnInitDialog() 
{
    if (!_do_not_paste)
      {    
      if (OpenClipboard())
        { 
        HANDLE p=GetClipboardData(CF_TEXT);
        if (p==NULL) p=GetClipboardData(CF_OEMTEXT);
        if (p==NULL) p=GetClipboardData(CF_DSPTEXT);
        if (p)
          {
          const char *c=(const char *)GlobalLock(p);
          if (strncmp(c,"news:",5)==0) ref=(c+5);	  
          GlobalUnlock(p);
          }
        CloseClipboard();
        }
      }
	
	CDialog::OnInitDialog();
    OnChangeEdit1();

    return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGotoRefDlg::OnChangeEdit1() 
  {
  CString id;
  GetDlgItemText(IDC_EDIT1,id);
  if (id.Mid(0,5)=="news:")
    {
    id.Delete(0,5);
    SetDlgItemText(IDC_EDIT1,id);
    }
  id="<"+id+">";
  if (acc)
    {
    CNewsArtHolder *hld=acc->msgidlist.Find(id);
    if (hld) id.Format("%s... | %s",hld->GetSubject().Left(20),hld->from);    
    else id.LoadString(IDS_UNKNOWNMESSAGEID);
    }
  else id.LoadString(IDS_UNKNOWNMESSAGEID);
  SetDlgItemText(IDC_REFNAME,id);
  }

void CGotoRefDlg::OnPaste() 
  {
  SetDlgItemText(IDC_EDIT1,"");
  GetDlgItem(IDC_EDIT1)->SendMessage(WM_PASTE);	
  }
