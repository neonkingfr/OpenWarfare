#include "stdafx.h"
#include <MAPI.H>
#include <direct.h>

typedef ULONG (FAR PASCAL *MAPISendMailFnc)(
                                            LHANDLE lhSession, 
                                            HWND hWnd, 
                                            lpMapiMessage lpMessage, 
                                            FLAGS flFlags, 
                                            ULONG ulReserved
                                            );

static BOOL DoSendMailFile(LPCTSTR sender, LPCTSTR recipient, LPCTSTR subject, LPCTSTR originmsg)
  {
  BOOL bSuccess = FALSE;
  bool mapiok;
  
  MapiRecipDesc MapiRecp[2];
  memset(&MapiRecp,0,sizeof(MapiRecp));
  MapiRecp[0].ulRecipClass = MAPI_TO;
  MapiRecp[0].lpszName   = const_cast<char *>(recipient);
  MapiRecp[0].lpszAddress = const_cast<char *>(recipient);
  MapiRecp[1].ulRecipClass = MAPI_ORIG;
  MapiRecp[1].lpszName   = const_cast<char *>(sender);
  MapiRecp[1].lpszAddress = const_cast<char *>(sender);
  
  
  MapiMessage  MapiMsg;
  memset(&MapiMsg,0,sizeof(MapiMsg));
  MapiMsg.lpszSubject  = const_cast<char *>(subject);
  MapiMsg.lpszNoteText = const_cast<char *>(originmsg);
  MapiMsg.nRecipCount  = 2;
  MapiMsg.lpRecips = MapiRecp;

  HINSTANCE hLib = LoadLibrary("mapi32.dll");
  if (hLib==NULL) mapiok=false;
  MAPISendMailFnc MAPISendMail=NULL;
  
  if (mapiok) 
	{
	MAPISendMail = (MAPISendMailFnc)GetProcAddress(hLib, "MAPISendMail"); 
	if (MAPISendMail==NULL) mapiok=false;
	}
  if (mapiok)
	{
	try
	  {
	  char *z=getcwd(NULL,MAX_PATH);
      DWORD nRes = MAPISendMail(0,::GetForegroundWindow(),&MapiMsg,MAPI_DIALOG|MAPI_NEW_SESSION,0);
	  if (nRes==SUCCESS_SUCCESS) mapiok=true;
	  else if (nRes!=MAPI_E_USER_ABORT) mapiok=false;	  
	  chdir(z);
	  free(z);
	  }
	catch(...)
	  {
	  mapiok=false;
	  }
	}	
  if (hLib!=NULL) FreeLibrary(hLib);
  return mapiok;
  }; 
