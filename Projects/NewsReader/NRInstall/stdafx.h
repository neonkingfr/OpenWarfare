// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


// Windows Header Files:
#include <windows.h>
#include <windowsx.h>
// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <stdio.h>

#define AssertDesc(x,y) assert(x)
#include <TextClass.h>
#include <stringres.h>
#include <shlobj.h>
#include <Pathname.h>
#include <StreamParser.h>
#include <fstream>
using namespace std;



extern CStringRes GStrRes;

// TODO: reference additional headers your program requires here
