// ArticleTableReader.h: interface for the ArticleTableReader class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARTICLETABLEREADER_H__36EBF10C_428A_4650_9E61_81A95A6EBE14__INCLUDED_)
#define AFX_ARTICLETABLEREADER_H__36EBF10C_428A_4650_9E61_81A95A6EBE14__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ArticleTable;
class LineTerminal;
struct ArticleHeader;



class ArticleTableReader  
{
struct ArticleInfo
  {
  int index;
  char *subject;
  char *from;
  char *messageid;
  char *date;
  char *references;
  size_t lines;
  char *content;
  char *xdelete;
  char *groups;
  char *icon;  
  ArticleInfo();
  ~ArticleInfo();
  };

  enum OverViewHeaders
	{
	OVSubject=0,OVFrom=1,OVDate=2,OVMessageID=3,OVReferences=4,OVLines=5,OVContentType=6,OVXDelete=7,OVIcon=8,OVGroups=9,OVEnd
	};
  enum OverViewStatus
	{
	OVS_Missing=0, OVS_Standard, OVS_Full
	};
  static char *HeaderNames[OVEnd];
  int _OVStatus[OVEnd];
  int _OVOrder[OVEnd];

  ArticleTable &_table;

public:
	int DownloadGroup(const char *group, LineTerminal &lTerm, int from);
	bool ReadOverviewHeader(LineTerminal &lTerm);
	ArticleTableReader(ArticleTable &table);

	///function checks existence of article, with messageID.
	/**Function should looks into index, and determine presence of article.
	If passed, it means, that article has been downloaded, before because it has been multiposted
	into multiple groups.
	@param messageID Message-ID tested
	@param group name of group that currently processed
	@return true, article test passed, so it will not added into table, false if test failed and article
	is new.
	*/
	virtual bool OnBeforeLoad(const char *messageId, const char *group)
	  {return false;}
	virtual void OnAfterLoad(unsigned long handle, ArticleHeader &hdr) {}
	virtual void LoadProgress(int cur, int max) {}

protected:
	void SetArticleParam(ArticleInfo &nfo,OverViewHeaders hdr, const char *text);
};

#endif // !defined(AFX_ARTICLETABLEREADER_H__36EBF10C_428A_4650_9E61_81A95A6EBE14__INCLUDED_)
