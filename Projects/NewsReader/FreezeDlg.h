#if !defined(AFX_FREEZEDLG_H__C789A066_0854_4AE7_A595_224F534EE9DE__INCLUDED_)
#define AFX_FREEZEDLG_H__C789A066_0854_4AE7_A595_224F534EE9DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FreezeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFreezeDlg dialog

class CFreezeDlg : public CDialog
{
// Construction
	CImageListEx doganim;
	int anmpos;
public:
	void DrawAnimIcon(CDC &dc);
	CFreezeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFreezeDlg)
	enum { IDD = IDD_FREEZE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFreezeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFreezeDlg)
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

UINT WatchDog(LPVOID param);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FREEZEDLG_H__C789A066_0854_4AE7_A595_224F534EE9DE__INCLUDED_)
