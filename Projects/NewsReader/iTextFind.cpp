// TextFind.cpp: implementation of the CTextFind class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "iTextFind.h"
#include "TextFind.h"
#include "SmartFind.h"

ITextFind *NewFind(bool substrings, bool booleanSearch, const char *text)
{
	if (booleanSearch)
	{
		// boolean search
		CTextFind *ret = new CTextFind;
		if (!ret->CompileCriteria(text))
		{
			delete ret;
			return NULL;
		}
		return ret;
	}
	else
	{
		CSmartFind *ret = new CSmartFind;
		if (!ret->Init(text))
		{
			delete ret;
			return NULL;
		}
		ret->SetSubstrings(substrings);
		return ret;
	}
	return NULL;
}

