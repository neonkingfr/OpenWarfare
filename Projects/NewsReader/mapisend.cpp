#include "stdafx.h"
#include <MAPI.H>
#include <direct.h>
#include <process.h>
#include "NewsReader.h"
#include "ArchiveStreamMemory.h"


typedef ULONG (FAR PASCAL *MAPISendMailFnc)(
                                            LHANDLE lhSession, 
                                            HWND hWnd, 
                                            lpMapiMessage lpMessage, 
                                            FLAGS flFlags, 
                                            ULONG ulReserved
                                            );



static BOOL CALLBACK SendWindows( HWND hwnd,LPARAM lParam)
  {
  char buff[256];
  GetClassName(hwnd,buff,256);
  if (strcmp(buff,"ATH_Note")==0)
    NrRegWindow(CWnd::FromHandle(hwnd));  
  return TRUE;
  }



static void SendThread(LPVOID data)
  {
  MapiMessage *MapiMsg=(MapiMessage *)data;
  DWORD *id=(DWORD *)MapiMsg->ulReserved;
  *id=GetCurrentThreadId();
  MapiMsg->ulReserved=0;
  bool mapiok=true;

  HINSTANCE hLib = LoadLibrary("mapi32.dll");
  if (hLib==NULL) mapiok=false;
  MAPISendMailFnc MAPISendMail=NULL;
  
  if (mapiok) 
	{
	MAPISendMail = (MAPISendMailFnc)GetProcAddress(hLib, "MAPISendMail"); 
	if (MAPISendMail==NULL) mapiok=false;
	}
  if (mapiok)
	{
	try
	  {
	  char *z=getcwd(NULL,MAX_PATH);
      DWORD nRes = MAPISendMail(0,0,MapiMsg,MAPI_DIALOG|MAPI_NEW_SESSION,0);
	  if (nRes==SUCCESS_SUCCESS) mapiok=true;
	  else if (nRes!=MAPI_E_USER_ABORT) mapiok=false;	  
	  chdir(z);
	  free(z);
	  }
	catch(...)
	  {
	  mapiok=false;
	  }
	}	
  if (hLib!=NULL) FreeLibrary(hLib);
  MapiRecipDesc *MapiRecp=MapiMsg->lpRecips;
  free(MapiRecp[0].lpszName);
  free(MapiRecp[0].lpszAddress);
  free(MapiRecp[1].lpszName);
  free(MapiRecp[1].lpszAddress);
  free(MapiMsg->lpszSubject);
  free(MapiMsg->lpszNoteText);
  delete MapiRecp;
  delete MapiMsg;
  NrRemoveInvalidWindows();
  _endthreadex(mapiok);
  }

static char *WordWrap(const char *originmsg)
{
  ArchiveStreamMemoryOut archout;
  ArchiveStreamMemoryIn archin(originmsg,strlen(originmsg),false);  
  int wrap=0;
  char sipka[5]="\r\n> ";
  unsigned char c=0;
  archout.DataExchange(sipka+2,2);
  archin.ExSimple(c);
  while (!archin.IsError())
  {
	if (c=='\n') 
	{
	  archout.DataExchange(sipka+1,3);	  
	  wrap=0;
	}	
	else 
	{
	  if (isspace(c) && wrap>50)
	  {
		archout.DataExchange(sipka,4);	  
		wrap=0;
	  }	
	  else
	  {
		archout.ExSimple(c);
  		wrap++;
	  }
	}
	archin.ExSimple(c);
  }
  c=0;
  archout.ExSimple(c);
  return archout.DetachBuffer();
}




BOOL DoSendMail(LPCTSTR sender, LPCTSTR recipient, LPCTSTR subject, LPCTSTR originmsg)
  {
  
  originmsg=WordWrap(originmsg);

  MapiRecipDesc *MapiRecp=new MapiRecipDesc[2];
  memset(MapiRecp,0,sizeof(*MapiRecp)*2);
  MapiRecp[0].ulRecipClass = MAPI_TO;
  MapiRecp[0].lpszName   = strdup(recipient);
  MapiRecp[0].lpszAddress = strdup(recipient);
  MapiRecp[1].ulRecipClass = MAPI_ORIG;
  MapiRecp[1].lpszName   = strdup(sender);
  MapiRecp[1].lpszAddress = strdup(sender);
  
  
  MapiMessage *MapiMsg=new MapiMessage;
  memset(MapiMsg,0,sizeof(*MapiMsg));
  MapiMsg->lpszSubject  = strdup(subject);
  MapiMsg->lpszNoteText = const_cast<char *>(originmsg);
  MapiMsg->nRecipCount  = 1;
  MapiMsg->lpOriginator = MapiRecp+1;
  MapiMsg->lpRecips = MapiRecp;
  DWORD remoteid;
  MapiMsg->ulReserved=(DWORD)(&remoteid);
  HANDLE thr=(HANDLE)_beginthread(SendThread,0,MapiMsg);  
  DWORD result=WaitForSingleObject(thr,5000);  
  if (result==WAIT_OBJECT_0)
    {
    DWORD exitcode;
    GetExitCodeThread(thr,&exitcode);
    CloseHandle(thr);
    if (exitcode==0) return false;
    }
  else
    CloseHandle(thr);
  EnumThreadWindows(remoteid,SendWindows,0);
  return true;
  }; 
