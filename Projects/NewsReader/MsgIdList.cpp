// MsgIdList.cpp: implementation of the CMsgIdList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "MsgIdList.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNewsArtHolder * CMsgIdList::Find(const char *msgid)
{
  CSingleLock lock(&_mt,TRUE);
  char fakebuff[sizeof(CNewsArtHolder)];
  CNewsArtHolder *fakehld=new(fakebuff) CNewsArtHolder;
  fakehld->messid=msgid;
  fakehld->Xcancel=msgid;  
  IDInfo *nfo=NULL;
  IDXCancel *xinfo=NULL;
  nfo=_idinfo.Find(IDInfo(fakehld));
  if (!nfo) 
    xinfo=_xcancel.Find(IDXCancel(fakehld));
  fakehld->~CNewsArtHolder();
  if (nfo) return nfo->hld;
  if (xinfo) return xinfo->hld;
  return NULL;
}

bool CMsgIdList::Add(CNewsArtHolder *hld)
{
  CSingleLock lock(&_mt,TRUE);
  IDInfo *nfo=_idinfo.Find(IDInfo(hld));
  if (!nfo) _idinfo.Add(IDInfo(hld));
  if (hld->Xcancel.GetLength())
  {
    IDXCancel *xnfo=_xcancel.Find(IDXCancel(hld));
    if (!xnfo) _xcancel.Add(IDXCancel(hld));
  }
  return true;
}
