// GenArchive.cpp: implementation of the CGenArchive class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "GenArchive.h"
#include <es/strings/rstring.hpp>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


void CGenArchive::Serialize(CString &s)
  {
  if (IsStoring)
	{
	int var=s.GetLength();
	VarSerial(var);
	const char *buff=s;
	ArrVarSerial(buff,var);
	}
  else
	{
	int var=-1;
	VarSerial(var);
	if (var<1) {s="";return;}
	char *buff=s.GetBufferSetLength(var);
	ArrVarSerial(buff,var);
	s.ReleaseBuffer(var);
	}
  }

void CGenArchive::Serialize(RString& s)
{
  {
  if (IsStoring)
	{
	int var=s.GetLength();
	VarSerial(var);
	const char *buff=s;
	ArrVarSerial(buff,var);
	}
  else
	{
	int var=-1;
	VarSerial(var);
	if (var<1) {s="";return;}
	char *buff=s.CreateBuffer(var);
	ArrVarSerial(buff,var);
	}
  }
  
}


//////////////////////////////////////////////////////////////////////
// CGenArchiveIn Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// CStrArchiveIn Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void CGenArchive::Serialize(CTime& tim)
  {
  time_t t=tim.GetTime();
  VarSerial(t);
  tim=t;
  }

void CGenArchive::Serialize(CTimeSpan& tim)
  {
  if (IsStoring)
	{
	LONG sec=tim.GetTotalSeconds();
	VarSerial(sec);
	}
  else
	{
	LONG sec;
	VarSerial(sec);
	tim=CTimeSpan(sec);
	}
  }
