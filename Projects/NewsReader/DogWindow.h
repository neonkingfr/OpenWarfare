#if !defined(AFX_DOGWINDOW_H__EAC4A7FA_A56C_40A2_86A2_C6125AF7DCA6__INCLUDED_)
#define AFX_DOGWINDOW_H__EAC4A7FA_A56C_40A2_86A2_C6125AF7DCA6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DogWindow.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDogWindow window

#define DOG_ANIMCOUNT 5

class CDogWindow : public CWnd
{
// Construction
  CImageListEx anims[DOG_ANIMCOUNT];
  int anim;
  int frame;
  bool repeat;
  bool end;
  int speed;
  int curspeed;
  CString status;
  CString error;
  int timeout;
  int errtimeout;
  CFont base;
  int progressmax;
  int progresspos;
  DWORD progresstm;
  DWORD progresslk;
public:
	CDogWindow();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDogWindow)
	//}}AFX_VIRTUAL

// Implementation
public:
	void InvalidateProgress();
	void SetProgressPos(int pos);
	void SetProgressMax(int max);
	void ResetProgress();
	void DrawDog(CDC *dc);
	void SetError(const char *error);
	void SetStatus(const char *text);
	bool PlayAnim(int anim, int speed=1);
	void Create(CWnd *parent, CRect &rc, UINT nID=0);
	virtual ~CDogWindow();

	// Generated message map functions
protected:
	//{{AFX_MSG(CDogWindow)
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

void DogSetStatusWindow(CDogWindow *status);
void DogShowStatus(const char *text);
void DogShowStatusV(const char *text,...);
void DogShowStatusV(int ids,...);
void DogShowError(const char *error);
void DogShowErrorV(const char *error,...);
void DogShowErrorV(int ids,...);
void DogSetPos(int pos);
void DogSetMax(int max);
void DogResetProgress();
void DogUpdateWindow();

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOGWINDOW_H__EAC4A7FA_A56C_40A2_86A2_C6125AF7DCA6__INCLUDED_)
