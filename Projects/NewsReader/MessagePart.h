// MessagePart.h: interface for the CMessagePart class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGEPART_H__77726291_6B85_40C5_9B08_7FFDCD20469C__INCLUDED_)
#define AFX_MESSAGEPART_H__77726291_6B85_40C5_9B08_7FFDCD20469C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <iostream>
#include <strstream>

#define MEMCACHEOWNER(a) ((unsigned long)a)

class CMessagePart  
  {
  public:
    enum MessageType
      {
      unknown,text,binary_uue,html
      };
  protected:
    char *data;
    int datasize;
    MessageType type;
    CMessagePart *next;
    char *filename;
    char *tempfname;
  public:
    CMessagePart();
    virtual ~CMessagePart();
    
    const char *GetFilename() 
      {return filename;}
    MessageType GetType() 
      {return type;}
    char *GetData();
    void LoadMessage(std::istream &str);
    CMessagePart *GetNext() 
      {return next;}
    int GetSize() 
      {return datasize;}
    void InsertCache(unsigned long owner);
    void CacheOneTimeUse(void **backptr);
    bool IsValid() {return type==text || type==binary_uue;}
    
  public:
    static CMessagePart *LoadMultipartMessage(std::istream &in, const char *content_type);
    void RemoveFromCache();
    void Serialize(CGenArchive& arch);
    const char * CreateAsFile(void *uid, const char *contenttype=NULL);
    
  };

//--------------------------------------------------

typedef bool (*uuencode_callback)(const char *buffer, void *context);
bool uuencode(const char *filename, std::istream &data, uuencode_callback proc, void *context);

#include "MessagePartCache.h"

extern CMessagePartCache memcache;

#endif // !defined(AFX_MESSAGEPART_H__77726291_6B85_40C5_9B08_7FFDCD20469C__INCLUDED_)
