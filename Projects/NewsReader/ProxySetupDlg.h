#if !defined(AFX_PROXYSETUPDLG_H__4E8788A0_8928_11D5_B39E_00C0DFAE7D0A__INCLUDED_)
#define AFX_PROXYSETUPDLG_H__4E8788A0_8928_11D5_B39E_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProxySetupDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProxySetupDlg dialog

class CProxySetupDlg : public CDialog
{
// Construction
public:
	void CheckRules();
	CProxySetupDlg(CWnd* pParent = NULL);   // standard constructor

	void SaveConfig();
	void LoadConfig();


// Dialog Data
	//{{AFX_DATA(CProxySetupDlg)
	enum { IDD = IDD_PROXYSETUP };
	CEdit	wAdr;
	CEdit	wPort;
	BOOL	sockenable;
	UINT	port;
	CString	address;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProxySetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CProxySetupDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSock4enable();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROXYSETUPDLG_H__4E8788A0_8928_11D5_B39E_00C0DFAE7D0A__INCLUDED_)
