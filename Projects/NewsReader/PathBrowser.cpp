// PathBrowser.cpp: implementation of the CPathBrowser class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "PathBrowser.h"
#include "CriticalSection.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static CCriticalSection sect;

static CString cb_curpath;

int WINAPI PosBrowseCallbackProc(HWND hwnd,UINT uMsg,LPARAM lParam,LPARAM lpData)
{
	if (uMsg == BFFM_INITIALIZED)
	{
		::SendMessage(hwnd,BFFM_SETSELECTION,TRUE,(LPARAM)((LPCSTR)cb_curpath));
		::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)((LPCSTR)cb_curpath));
	} else
	if (uMsg == BFFM_SELCHANGED)
	{
		char buff[_MAX_PATH];
		if (SHGetPathFromIDList((LPITEMIDLIST)lParam,buff))
		{
			CString sPath = buff;
			::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)((LPCSTR)sPath));
		}
	}
	return 0;
};





bool AskForPath(CString &curpath)
  {
  CScoopCritSect sc(sect);

  BROWSEINFO brw;
  char path[MAX_PATH];  
  ITEMIDLIST *il;

  HWND hWnd=GetForegroundWindow();

  memset(&brw,0,sizeof(brw));
  brw.hwndOwner=hWnd;
  brw.pidlRoot=NULL;
  strncpy(path,curpath,MAX_PATH);
  brw.pszDisplayName=path;
  cb_curpath=curpath;  
  brw.lpszTitle=NULL;
  brw.ulFlags= BIF_RETURNONLYFSDIRS |BIF_STATUSTEXT; 	
  brw.lpfn = (BFFCALLBACK)(PosBrowseCallbackProc);
  il=SHBrowseForFolder( &brw );
  if (il==NULL) return false;
  SHGetPathFromIDList(il,path);
  if (path[0]==0) return false;
  curpath=path;
  return true;
  }