// NewAccWiz.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "NewAccWiz.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz1 dialog


CNewAccWiz1::CNewAccWiz1(CWnd* pParent /*=NULL*/)
	: CNewAccWizBase(CNewAccWiz1::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewAccWiz1)
	m_email = _T("");
	m_name = _T("");
	//}}AFX_DATA_INIT
}


void CNewAccWiz1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewAccWiz1)
	DDX_Text(pDX, IDC_EMAIL, m_email);
	DDX_Text(pDX, IDC_NAME, m_name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewAccWiz1,  CNewAccWizBase)
	//{{AFX_MSG_MAP(CNewAccWiz1)
	ON_EN_CHANGE(IDC_NAME, OnChangeName)
	ON_EN_CHANGE(IDC_EMAIL, OnChangeName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz1 message handlers
/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz2 dialog


CNewAccWiz2::CNewAccWiz2(CWnd* pParent /*=NULL*/)
	: CNewAccWizBase(CNewAccWiz2::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewAccWiz2)
	m_login = FALSE;
	m_name = _T("");
	m_port = 0;
	//}}AFX_DATA_INIT
}


void CNewAccWiz2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewAccWiz2)
	DDX_Check(pDX, IDC_LOGIN, m_login);
	DDX_Text(pDX, IDC_NAME, m_name);
	DDX_Text(pDX, IDC_PORT, m_port);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewAccWiz2,  CNewAccWizBase)
	//{{AFX_MSG_MAP(CNewAccWiz2)
	ON_EN_CHANGE(IDC_NAME, OnChangeName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz2 message handlers
/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz3 dialog


CNewAccWiz3::CNewAccWiz3(CWnd* pParent /*=NULL*/)
	:CNewAccWizBase(CNewAccWiz3::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewAccWiz3)
	m_name = _T("");
	m_password = _T("");
	//}}AFX_DATA_INIT
}


void CNewAccWiz3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewAccWiz3)
	DDX_Text(pDX, IDC_NAME, m_name);
	DDX_Text(pDX, IDC_PASSWORD, m_password);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewAccWiz3,  CNewAccWizBase)
	//{{AFX_MSG_MAP(CNewAccWiz3)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz3 message handlers
/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz4 dialog


CNewAccWiz4::CNewAccWiz4(CWnd* pParent /*=NULL*/)
	: CNewAccWizBase(CNewAccWiz4::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewAccWiz4)
	m_smtp = FALSE;
	m_sock4 = FALSE;
	m_tune = FALSE;
	//}}AFX_DATA_INIT
}


void CNewAccWiz4::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewAccWiz4)
	DDX_Check(pDX, IDC_SMTP, m_smtp);
	DDX_Check(pDX, IDC_SOCK4, m_sock4);
	DDX_Check(pDX, IDC_TUNE, m_tune);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewAccWiz4,  CNewAccWizBase)
	//{{AFX_MSG_MAP(CNewAccWiz4)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz4 message handlers
/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz5 dialog


CNewAccWiz5::CNewAccWiz5(CWnd* pParent /*=NULL*/)
	: CNewAccWizBase(CNewAccWiz5::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewAccWiz5)
	m_name = _T("");
	//}}AFX_DATA_INIT
}


void CNewAccWiz5::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewAccWiz5)
	DDX_Text(pDX, IDC_NAME, m_name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewAccWiz5,  CNewAccWizBase)
	//{{AFX_MSG_MAP(CNewAccWiz5)
	ON_EN_CHANGE(IDC_NAME, OnChangeName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_MESSAGE_MAP(CNewAccWizBase,  CDialog)
	//{{AFX_MSG_MAP(CNewAccWizBase)
		//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz5 message handlers

static bool AskExit()
  {
  return NrMessageBox(IDS_EXITWIZARD,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDYES;
  }

static bool ExitDlg(CDialog* dlg, UINT cmd)
  {
  switch (cmd)
	{
	case IDOK: break;
	case IDCANCEL: if (AskExit()) dlg->EndDialog(cmd);break;
	case IDC_NEXT: if (dlg->UpdateData(TRUE)) dlg->EndDialog(cmd); break;	
	case IDC_BACK: dlg->UpdateData(TRUE); dlg->EndDialog(cmd); break;	
	default: return false;
	}
  return true;
  }

BOOL CNewAccWizBase::OnInitDialog() 
  {
  CDialog::OnInitDialog();
	
  OnDlgRules();	
  return TRUE;  // return TRUE unless you set the focus to a control
                // EXCEPTION: OCX Property Pages should return FALSE
  }

BOOL CNewAccWizBase::OnCommand(WPARAM wParam, LPARAM lParam) 
  {
  if (ExitDlg(this,LOWORD(wParam))) return TRUE;
  return CDialog::OnCommand(wParam,lParam);
  }

void CNewAccWiz1::OnDlgRules() 
  {
  CString s1,s2;
  GetDlgItemText(IDC_NAME,s1);
  GetDlgItemText(IDC_EMAIL,s2);
  GetDlgItem(IDC_NEXT)->EnableWindow(s1.GetLength()!=0 && s2.Find('@')!=-1);
  }

void CNewAccWiz1::OnChangeName() 
  {
  OnDlgRules();	
  }

void CNewAccWiz2::OnDlgRules()
  {
  GetDlgItem(IDC_NEXT)->EnableWindow(GetDlgItem(IDC_NAME)->GetWindowTextLength()!=0);
  }

void CNewAccWiz2::OnChangeName() 
  {
  OnDlgRules();	
  }

void CNewAccWiz3::OnDlgRules()
  {

  }

void CNewAccWiz4::OnDlgRules()
  {

  }

void CNewAccWiz5::OnChangeName() 
  {
  OnDlgRules();	
  }

void CNewAccWiz5::OnDlgRules()
  {
  GetDlgItem(IDC_NEXT)->EnableWindow(GetDlgItem(IDC_NAME)->GetWindowTextLength()!=0);
  }

bool RunNewAccWizard(CNewAccWiz1& w1,
				     CNewAccWiz2& w2,
				     CNewAccWiz3& w3,
				     CNewAccWiz4& w4,
				     CNewAccWiz5& w5)
  {
  int cmd;
back1:
  cmd=w1.DoModal();
  if (cmd==IDCANCEL) return false;
back2:
  cmd=w2.DoModal();
  if (cmd==IDCANCEL) return false;
  if (cmd==IDC_BACK) goto back1;
  if (w2.m_login==FALSE) goto back4;
back3:
  cmd=w3.DoModal();
  if (cmd==IDCANCEL) return false;
  if (cmd==IDC_BACK) goto back2;
back4:
  if (w5.m_name=="") w5.m_name=w2.m_name;
  cmd=w5.DoModal();
  if (cmd==IDCANCEL) return false;
  if (cmd==IDC_BACK) 
	if (w2.m_login) goto back3;else goto back2;
  
  cmd=w4.DoModal();
  if (cmd==IDCANCEL) return false;
  if (cmd==IDC_BACK) goto back4;
  return true;
  }
