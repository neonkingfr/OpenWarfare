#if !defined(AFX_NEWACCWIZ_H__65B68372_73DC_439D_9E2C_D8265F42B69C__INCLUDED_)
#define AFX_NEWACCWIZ_H__65B68372_73DC_439D_9E2C_D8265F42B69C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewAccWiz.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz1 dialog

class CNewAccWizBase:  public CDialog
  {
  public:
  CNewAccWizBase(UINT temp,CWnd* pParent = NULL):CDialog(temp,pParent) {}
  virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
  virtual void OnDlgRules()=0;
  virtual BOOL OnInitDialog();
	//{{AFX_MSG(CNewAccWizBase)
	//}}AFX_MSG
  protected:
  DECLARE_MESSAGE_MAP()
  };

class CNewAccWiz1 : public CNewAccWizBase
{
// Construction
public:
	CNewAccWiz1(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewAccWiz1)
	enum { IDD = IDD_NEWACCW1 };
	CString	m_email;
	CString	m_name;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewAccWiz1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
  virtual void OnDlgRules();
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewAccWiz1)
	afx_msg void OnChangeName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz2 dialog

class CNewAccWiz2 : public CNewAccWizBase
{
// Constructi
public:
	CNewAccWiz2(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewAccWiz2)
	enum { IDD = IDD_NEWACCW2 };
	BOOL	m_login;
	CString	m_name;
	UINT	m_port;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewAccWiz2)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewAccWiz2)
	afx_msg void OnDlgRules();
	afx_msg void OnChangeName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz3 dialog

class CNewAccWiz3 : public CNewAccWizBase
{
// Construction
public:
	virtual void OnDlgRules();
	CNewAccWiz3(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewAccWiz3)
	enum { IDD = IDD_NEWACCW3 };
	CString	m_name;
	CString	m_password;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewAccWiz3)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewAccWiz3)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz4 dialog

class CNewAccWiz4 : public CNewAccWizBase
{
// Construction
public:
	virtual void OnDlgRules();
	CNewAccWiz4(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewAccWiz4)
	enum { IDD = IDD_NEWACCW4 };
	BOOL	m_smtp;
	BOOL	m_sock4;
	BOOL	m_tune;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewAccWiz4)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewAccWiz4)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CNewAccWiz5 dialog

class CNewAccWiz5 : public CNewAccWizBase
{
// Construction
public:
	virtual void OnDlgRules();
	CNewAccWiz5(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewAccWiz5)
	enum { IDD = IDD_NEWACCW5 };
	CString	m_name;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewAccWiz5)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewAccWiz5)
	afx_msg void OnChangeName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

bool RunNewAccWizard(CNewAccWiz1& w1,
				     CNewAccWiz2& w2,
				     CNewAccWiz3& w3,
				     CNewAccWiz4& w4,
				     CNewAccWiz5& w5);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWACCWIZ_H__65B68372_73DC_439D_9E2C_D8265F42B69C__INCLUDED_)

