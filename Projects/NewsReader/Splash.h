#if !defined(AFX_SPLASH_H__DBCDB75C_021E_448C_90C4_59159EE8B7CA__INCLUDED_)
#define AFX_SPLASH_H__DBCDB75C_021E_448C_90C4_59159EE8B7CA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Splash.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CSplash thread

#include "SplashWnd.h"

class CSplash : public CWinThread
{
	DECLARE_DYNCREATE(CSplash)
	CSplashWnd spwnd;
	CSplashOut spout;
public:
	bool kill;
    bool splashout;
	CSplash();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	BOOL OnIdle(LONG lCount);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSplash)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	virtual ~CSplash();
protected:

	// Generated message map functions
	//{{AFX_MSG(CSplash)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPLASH_H__DBCDB75C_021E_448C_90C4_59159EE8B7CA__INCLUDED_)
