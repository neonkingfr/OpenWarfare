#if !defined(AFX_ACCOUNTPROPDLG_H__7E88C660_AAF5_11D5_B3A0_0050FCFE63F1__INCLUDED_)
#define AFX_ACCOUNTPROPDLG_H__7E88C660_AAF5_11D5_B3A0_0050FCFE63F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AccountPropDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAccountPropDlg dialog

class FullTextIndex;

class CAccountPropDlg : public CDialog
{
// Construction
public:
	CAccountPropDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAccountPropDlg)
	enum { IDD = IDD_ACCOUNTPROP };
	CComboBox	wFullTextInclude;
	CEdit	wCachePath;
	CButton	wAutocompact;
	CEdit	wCompacto;
	CEdit	wArchDate;
	CEdit	wDelDate;
	CEdit	wCache;
	BOOL	archcreate;
	BOOL	enablecache;
	UINT	cachesize;
	CString	keywrds;
	UINT	deldate;
	BOOL	enabledel;
	UINT	archdate;
	int		delcheck;
	UINT	compacto;
	BOOL	autocompact;
	BOOL	loadstartup;
	CString	urgent;
	BOOL	nonewnotify;
	BOOL	downloadondemand;
	CString	cachepath;
	int		fulltextopt;
	CString	vIgnoredWords;
	BOOL	vSaveInPreviousVersion;
	BOOL	vFullTextReg;
	//}}AFX_DATA
	FullTextIndex *vFTIndex;
    unsigned long fulltextflgs;	
    CFont boldFont;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAccountPropDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAccountPropDlg)
	afx_msg void OnCache();
	afx_msg void OnArchcreate();
	afx_msg void OnOlddelete();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnUrgentbrowse();
	afx_msg void OnPlay();
	afx_msg void OnCachebrose();
	afx_msg void OnFulltextdump();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnSelendokFulltextinclude();
	afx_msg void OnEnableincludefulltex();
	afx_msg void OnSaveinprevious();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  	  void LoadIgnoredWords();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACCOUNTPROPDLG_H__7E88C660_AAF5_11D5_B3A0_0050FCFE63F1__INCLUDED_)
