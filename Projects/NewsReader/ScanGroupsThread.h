// ScanGroupsThread.h: interface for the CScanGroups Thread class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCANGROUPSTHREAD_H__A4B6F743_1A02_4660_B095_A4DBBAF1E350__INCLUDED_)
#define AFX_SCANGROUPSTHREAD_H__A4B6F743_1A02_4660_B095_A4DBBAF1E350__INCLUDED_

#include "AccountList.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "NewsReader.h"

class CScanGroupsThreadEx : public CRunner
{
  bool scandel;
public:  
  bool error;
  CNewsAccount *acc;
  bool allgroups;
  CTime lastcheck;
  CNewsGroup *grlist;
  HWND nwnotify;
  CScanGroupsThreadEx(CNewsAccount *acc, bool scandel):acc(acc),scandel(scandel)
    {acc->Lock();grlist=NULL;allgroups=false;error=false;nwnotify=NULL;  LogWindowMessage(0,1,0);}
  virtual void Run();
  virtual ~CScanGroupsThreadEx() 
    {acc->Release();delete grlist;  LogWindowMessage(0,0,0);}
  CNewsGroup *UnlinkList() {CNewsGroup *out=grlist;grlist=NULL;return out;}
  };

class CScanNewerOlderThread: public CRunner
  {
  CNewsAccount *acc;
  CNewsGroup *grp;
  bool scandel;
  public:
	CScanNewerOlderThread(CNewsAccount *acc, CNewsGroup *grp, bool scandel);
	virtual ~CScanNewerOlderThread();
	virtual void Run();
  };

class CDownloadArticleThread: public CRunner
  {
  HWND call;
  CNewsArtHolder *hld;
  CNewsAccount *acc;
  CNewsGroup *grp;
  bool download_long;
  public:
	CDownloadArticleThread(CWnd *p, CNewsArtHolder *hld,CNewsAccount *acc,CNewsGroup *grp,bool download_long);
	virtual ~CDownloadArticleThread();
	virtual void Run();
  };

class CSaveNewsThread: public CRunner
  {
  CAccountList &acclist;
  public:
	CSaveNewsThread(CAccountList *acc):acclist(*acc) {};
	virtual void Run();
  };

class CSaveAccThread: public CRunner
  {
  CAccountList &acclist;
  HTREEITEM it;
  public:
	CSaveAccThread(CAccountList *acc,HTREEITEM it):acclist(*acc),it(it) {};
	virtual void Run();
  };

UINT CheckNewerVersionThread(LPVOID nothing);

class CAccountCompactThread: public CRunner
  {
  CAccountList &acclist;
  public:
	CAccountCompactThread(CAccountList *accl):acclist(*accl) {}
	virtual void Run();
  };

#endif // !defined(AFX_SCANGROUPSTHREAD_H__A4B6F743_1A02_4660_B095_A4DBBAF1E350__INCLUDED_)
