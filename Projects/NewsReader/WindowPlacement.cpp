// WindowPlacement.cpp: implementation of the CWindowPlacement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "newsreader.h"
#include "WindowPlacement.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


void CWindowPlacement::Retrieve(HWND hWnd)
  {
  length=sizeof(WINDOWPLACEMENT);
  ::GetWindowPlacement(hWnd,this);
  }

void CWindowPlacement::Restore(HWND hWnd, EWP_mode mode,bool postcommand)
  {
  if (length<sizeof(WINDOWPLACEMENT)) return;
  int show=showCmd;
  int swsave=show;
  switch (mode)
	{
	case EWP_normal: show=SW_RESTORE;break;
	case EWP_maxonly: if (show==SW_SHOWMINIMIZED) show=SW_RESTORE;break;
	case EWP_minonly: if (show==SW_SHOWMAXIMIZED) show=SW_RESTORE;break;
  	}
  if (postcommand)
	showCmd=SW_RESTORE;
  ::SetWindowPlacement(hWnd,this);
  if (postcommand)
	switch (show)
	  {
	  case SW_SHOWMINIMIZED:  PostMessage(hWnd,WM_SYSCOMMAND,SC_MINIMIZE,0);break;
	  case SW_SHOWMAXIMIZED:  PostMessage(hWnd,WM_SYSCOMMAND,SC_MAXIMIZE,0);break;
	  }
  showCmd=swsave;
  }

#define SAVESTRING(app,name,item) ((app)->WriteProfileString(name,#item,item))
#define SAVEINT(app,name,item) ((app)->WriteProfileInt(name,#item,item))

#define LOADSTRING(app,name,item) item=((app)->GetProfileString(name,#item,""))
#define LOADINT(app,name,item) item=((app)->GetProfileInt(name,#item,0))

void CWindowPlacement::Store(char *name)
  {
  CWinApp *app=AfxGetApp();
  SAVEINT(app,name,length);
  SAVEINT(app,name,flags);
  SAVEINT(app,name,showCmd);
  SAVEINT(app,name,ptMinPosition.x);
  SAVEINT(app,name,ptMinPosition.y);
  SAVEINT(app,name,ptMaxPosition.x);
  SAVEINT(app,name,ptMaxPosition.y);
  SAVEINT(app,name,rcNormalPosition.left);
  SAVEINT(app,name,rcNormalPosition.right);
  SAVEINT(app,name,rcNormalPosition.top);
  SAVEINT(app,name,rcNormalPosition.bottom);
  }

void CWindowPlacement::Load(char *name)
  {
  CWinApp *app=AfxGetApp();
  LOADINT(app,name,length);
  LOADINT(app,name,flags);
  LOADINT(app,name,showCmd);
  LOADINT(app,name,ptMinPosition.x);
  LOADINT(app,name,ptMinPosition.y);
  LOADINT(app,name,ptMaxPosition.x);
  LOADINT(app,name,ptMaxPosition.y);
  LOADINT(app,name,rcNormalPosition.left);
  LOADINT(app,name,rcNormalPosition.right);
  LOADINT(app,name,rcNormalPosition.top);
  LOADINT(app,name,rcNormalPosition.bottom);
  }
