// CompactCacheDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "CompactCacheDlg.h"
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCompactCacheDlg dialog


CCompactCacheDlg::CCompactCacheDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CCompactCacheDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CCompactCacheDlg)
    mode = -1;
    //}}AFX_DATA_INIT
    targetsize=0;
    }

//--------------------------------------------------

void CCompactCacheDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CCompactCacheDlg)
  DDX_Control(pDX, IDC_PROGRESS1, progress);
  DDX_Radio(pDX, IDC_COMPACT1, mode);
  //}}AFX_DATA_MAP	
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CCompactCacheDlg, CDialog)
  //{{AFX_MSG_MAP(CCompactCacheDlg)
  ON_WM_TIMER()
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CCompactCacheDlg message handlers
      
      BOOL CCompactCacheDlg::OnInitDialog() 
        {  
        ASSERT(targetsize!=0);
        CDialog::OnInitDialog();
        SetMode(0);
        SetTimer(100,1000,NULL);	
        lp.Create(IDB_DOGANIM7,50,1,RGB(255,0,255));
        lp.SetBkColor(GetSysColor(COLOR_3DFACE));
        SetTimer(50,100,NULL);
        progress.SetRange(0,100);
        animpos=0;
        return TRUE;  // return TRUE unless you set the focus to a control
        // EXCEPTION: OCX Property Pages should return FALSE
        }

//--------------------------------------------------

void CCompactCacheDlg::OnTimer(UINT nIDEvent) 
  {
  if (nIDEvent==100)
    {
    KillTimer(nIDEvent);
    stop=false;
    RunCompact();
    }
  if (nIDEvent==50)
    {
    CRect rc;
    GetDlgItem(IDC_DOGWIN)->GetClientRect(&rc);
    GetDlgItem(IDC_DOGWIN)->MapWindowPoints(this,&rc);
    CDC *dc=GetDC();
    lp.Draw(dc,animpos,CPoint(rc.right-50,rc.top),ILD_NORMAL);
    animpos++;
    if (animpos>lp.GetImageCount()) animpos=0;
    }
  CDialog::OnTimer(nIDEvent);
  }

//--------------------------------------------------

void CCompactCacheDlg::SetProgress(int value, int max)
  {  
  progress.SetPos(MulDiv(value,100,max));
  RunEvents();
  }

//--------------------------------------------------

void CCompactCacheDlg::SetMode(int mode)
  {
  this->mode=mode;
  UpdateData(FALSE);
  RunEvents();
  }

//--------------------------------------------------

bool CCompactCacheDlg::RunEvents()
  {
  MSG msg;
  while (::PeekMessage(&msg,0,0,0,PM_REMOVE))
    DispatchMessage(&msg);	
  return stop;
  }

//--------------------------------------------------

void CCompactCacheDlg::OnCancel() 
  {
  stop=true;	
  CDialog::OnCancel();
  }

//--------------------------------------------------

static int ArtCompare(const void *art1, const void *art2)
  {
  const CNewsArtHolder *hld1=*(const CNewsArtHolder **)art1;
  const CNewsArtHolder *hld2=*(const CNewsArtHolder **)art2;
  if (hld1->rdate.GetStatus() != hld1->rdate.valid) 
    return 1;
  if (hld2->rdate.GetStatus() != hld2->rdate.valid) 
    return -1;
  return (hld1->rdate>hld2->rdate)-(hld1->rdate<hld2->rdate);
  }

//--------------------------------------------------

void CCompactCacheDlg::RunCompact()
  {
  CRWLockGuard guard(articleLock,true);guard.LockPM();
  
  CMessCache tempcache;
  CNewsGroup *grp;
  CNewsArtHolder *art;
  int artcnt=0;
  CNewsArtHolder **artlist;
  DWORD *numlist; //offsety
  int pos;
  int i;
  
  //* Nejprve zjisti kolik je articlu;
  SetMode(0);
  if (!acc->IsLoaded())
    {
    SetMode(1);
    if (acclist->EnsureLoaded(acc)==false) {EndDialog(0);return;}
    }
  for (grp=acc->GetGroups();grp;grp=grp->GetNext())
    for (art=grp->GetList();art;art=art->EnumNext())
      if (acc->cache.FindInCache(art->messid))
        {artcnt++;RunEvents();}
  pos=0;
  artlist=new CNewsArtHolder *[artcnt];
  numlist=new DWORD [artcnt];
  SetMode(1);
  if (stop) goto term;
  for (grp=acc->GetGroups();grp;grp=grp->GetNext())
    {
    if (grp->GetList())
      {
      for (art=grp->GetList();art;art=art->EnumNext())
       if (acc->cache.FindInCache(art->messid))
         artlist[pos++]=art;
      SetProgress(pos,artcnt);
      }
    }
  artcnt=pos;
  SetCursor(theApp.LoadStandardCursor(IDC_WAIT));
  qsort(artlist,artcnt,sizeof(artlist[0]),ArtCompare);
  if (stop) goto term;
  if (targetsize==0) targetsize=acc->offline*acc->compacto/100;
  tempcache.InitCache(CString(acc->cache.GetFullName())+".$$$",targetsize);
  pos=0;
  SetMode(2);
  for (i=artcnt-1;i>=0;i--)
    {
    if (tempcache.IsCacheFull()) break;
    CNewsArtHolder *hld=artlist[i];
    CMessagePart part;
    iostream *in=acc->cache.OpenMessage(hld->messid);
    if (in)
      {
      CGenArchiveIn archin(*in);
      part.Serialize(archin);
      if (part.IsValid())
        {
        iostream *out=tempcache.WriteMessage(hld->messid);
        if (out)
          {
          CGenArchiveOut archout(*out);
          part.Serialize(archout);
          tempcache.CloseMessage();
          }
        else
          hld->DropFromCache();
        }
      else
        hld->DropFromCache();
      acc->cache.CloseMessage();
      }
    else
      hld->DropFromCache();
    pos++;
    if (stop) goto term2;
    SetProgress(tempcache.GetCacheSize(),tempcache.GetCacheMaxSize());
    }
  for (i;i>=0;i--) numlist[i]=0;
  SetMode(3);
  acc->cache.CloseCache();
  tempcache.MoveCache(acc->cache.GetFullName());
  tempcache.CloseCache();
  acc->InitCache();
  //  for (i=0;i<artcnt;i++) artlist[i]->chpos=numlist[i];  
  acclist->AccountStreaming(acclist->GetAccountItem(it),true);
  //  remove(tempcache.GetFullName());
  delete artlist;
  delete numlist;
  EndDialog(IDOK);
  return;
  term2:
  tempcache.CloseCache();
  remove(tempcache.GetFullName());
  term:
  delete artlist;
  delete numlist;
  EndDialog(IDCANCEL);
  }

//--------------------------------------------------

void CCompactCacheDlg::OnOK()
  {
  
  }

//--------------------------------------------------

