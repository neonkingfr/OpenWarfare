// GroupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "GroupDlg.h"
#include "ScanGroupsThread.h"
#include "NewsArticle.h"
#include "MsgBox.h"
#include "NewsTerminal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define viewall (viewmode==0)
#define viewnew (viewmode==1)
#define viewsign (viewmode==2)

/////////////////////////////////////////////////////////////////////////////
// CGroupDlg dialog


CGroupDlg::CGroupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGroupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGroupDlg)
	//}}AFX_DATA_INIT
	tmr=-1;
}


void CGroupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGroupDlg)
	DDX_Control(pDX, IDC_COMBOMODE, wMode);
	DDX_Control(pDX, IDC_TAB, wTab);
	DDX_Control(pDX, IDC_SIGNUP, signup);
	DDX_Control(pDX, IDC_GROUPS, grlist);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGroupDlg, CDialog)
	//{{AFX_MSG_MAP(CGroupDlg)
	ON_BN_CLICKED(IDC_RELOAD, OnReload)
	ON_WM_TIMER()
	ON_NOTIFY(NM_RELEASEDCAPTURE , IDC_GROUPS, OnReleaseList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_GROUPS, OnItemchangedList)
	ON_BN_CLICKED(IDC_SIGNUP, OnSignup)
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_FILTER, OnChangeFilter)
	ON_WM_SETCURSOR()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB, OnSelchangeTab)
	ON_CBN_SELENDOK(IDC_COMBOMODE, OnSelendokCombomode)
	ON_NOTIFY(NM_DBLCLK, IDC_GROUPS, OnDblclkGroups)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGroupDlg message handlers

BOOL CGroupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
		
	CenterWindow();
	SetCursor(LoadCursor(NULL,IDC_WAIT));
	CString head;
	head.LoadString(IDS_GROUPNAME);
	grlist.SetImageList(&theApp.ilist,LVSIL_SMALL);
	grlist.InsertColumn(0,head,LVCFMT_LEFT,200,0);	
	grlist.InsertColumn(1,"",LVCFMT_CENTER,100,1);	
	head.LoadString(IDS_GROUPALL);wTab.InsertItem(0,head);
	head.LoadString(IDS_GROUPNEW);wTab.InsertItem(1,head);
	head.LoadString(IDS_GROUPSIGNED);wTab.InsertItem(2,head);	
	SetViewMode(CheckDupFaster());
	modes[0].LoadString(IDS_DWNMODEHEADERS);
	modes[1].LoadString(IDS_DWNMODENEW);
	modes[2].LoadString(IDS_FULLTEXTINDEX);
	modes[3].LoadString(IDS_DWNMODEALL);
	for (int i=0;i<4;i++) wMode.AddString(modes[i]);
	ShowWindow(SW_SHOW);	
	UpdateWindow();	
	checker=curacc->GetGroups();
	SetTimer(1,500,NULL);
	tmr=SetTimer(2,500,NULL);
	recurse=false;	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGroupDlg::SetViewMode(int vm)
  {
  wTab.SetCurSel(vm);
  if (vm==2) ReformatList(40);
  else ReformatList(15);
  viewmode=vm;
  }

void CGroupDlg::LoadToList()
  {
  if (recurse) {filterop=1;return;}
  CNewsGroup *grp;
  int i;
  CString flt;
rep:
  filterop=0;
  grlist.DeleteAllItems();
/*  if (viewsign)
	grlist.SetExtendedStyle(grlist.GetExtendedStyle()|LVS_EX_CHECKBOXES);
  else
	grlist.SetExtendedStyle(grlist.GetExtendedStyle()&~LVS_EX_CHECKBOXES);*/
  wMode.EnableWindow(FALSE);
  recurse=true;
  grp=curacc->GetGroups();
  i=0;
  GetDlgItemText(IDC_FILTER,flt);
  SetCursor(::LoadCursor(NULL,IDC_WAIT));
  while (grp)
	{
	if (viewall || (viewnew && grp->isnew) || (viewsign &&grp->sign))
	  {
	  const char *p=grp->GetName();
	  if (flt=="" || strstr(p,flt)!=NULL)
		{
		int p=grlist.InsertItem(i++,grp->GetName(),grp->sign?1:2);
		grlist.SetItemData(p,(DWORD)grp);
		char buf[25];
		if (viewsign)
		  grlist.SetItemText(p,1,modes[grp->fullload]);
		else
		  grlist.SetItemText(p,1,itoa(grp->GetMax()-grp->GetMin(),buf,10));
//		grlist.SetItemState(p,INDEXTOSTATEIMAGEMASK((grp->fullload)+1), LVIS_STATEIMAGEMASK);
		}
	  }
	MSG msg;
	while (::PeekMessage(&msg,0,0,0,PM_REMOVE)) {runflag=true;TranslateMessage(&msg);DispatchMessage(&msg);}
	if (filterop==1) goto rep;
	if (filterop==2) return;	
	grp=grp->GetNext();
	}
  recurse=false;
  }


void CGroupDlg::OnReload() 
  {
  const char *regnm="ReloadGroup";
  int ask=theApp.GetRegInt(regnm,1);
  if (ask)
	{
	CMsgBox box;
	box.Flags.okbutt=box.Flags.noagain=1;
	box.message.LoadString(IDS_RELOADWARN);
	if (box.DoModal()==IDCANCEL) return;
	if (box.noagain) theApp.SetRegInt(regnm,0);
	}
  CNewsTerminal::StopTrafic();
  SetCursor(::LoadCursor(NULL,IDC_WAIT));
  curacc->mutex.Lock(INFINITE);
  CNewsGroup *p=curacc->GetGroups();
  CNewsGroup *t=p,*bg=NULL;
  while (t)
	{
	if (t->sign) 
	  {
	  bg=t;
	  t=bg->GetNext();
	  }
	else
	  {
	  CNewsGroup *nx=t->GetNext();
	  if (bg==NULL) {curacc->UnlinkGroup(t);}else 
		{	  
		t->Unlink();	 
		bg->Unlink();
		bg->SetNext(nx);
		}
	  delete t;
	  t=nx;
	  }
	}
  curacc->SetLastCheck(CTime((time_t)1));
  curacc->SetForceDownload(true);
  curacc->mutex.Unlock();
  filterop=2;
  EndDialog(IDCANCEL);
/*  theApp.RunBackground(0,NULL,5000);
  if (AfxMessageBox(IDS_RELOADWARNING,MB_YESNO|MB_ICONWARNING)==IDNO) return;
  EnableWindow(FALSE);
  checker=NULL;
  curacc->DeleteGroups();
  curacc->SetLastCheck(CTime(1980,1,1,0,0,0));
  SetCursor(LoadCursor(NULL,IDC_WAIT));  
  CScanGroupsThread thrd(curacc);
  curacc->SetLastCheck(CTime(1980,1,1,0,0,0));
  thrd.Run();
  curacc->SetLastCheck(thrd.lastcheck);
  CNewsGroup *newptr;
  newptr=thrd.UnlinkList();
  curacc->AppendGrouplist(newptr);
  curacc->SetNew(newptr);
  EnableWindow(TRUE);
  LoadToList();
  checker=curacc->GetGroups();*/
  }

void CGroupDlg::OnTimer(UINT nIDEvent) 
  {
  LRESULT res;
  if (nIDEvent==tmr)
	{
	KillTimer(tmr);
	LoadToList();
	}
//  if (!dupscan) RemoveDuplicity();
  if (selscan) OnReleaseList(NULL,&res);
  CDialog::OnTimer(nIDEvent);
  }

void CGroupDlg::OnReleaseList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  if (selscan)
	{
	int i=grlist.GetNextItem(-1,LVNI_SELECTED);
	int state=0;
	char mode=-1;
	int last=-1;

	while (last<i)
	  {
	  CNewsGroup *grp=(CNewsGroup *)grlist.GetItemData(i);
	  if (grp->sign) state|=2;else state|=1;	  
	  if (mode==-1) mode=grp->fullload;
	  else if (mode!=grp->fullload) mode=-2;
	  last=i;
	  i=grlist.GetNextItem(last,LVNI_SELECTED);
	  }
	if (state!=0) state-=1;
	signup.SetCheck(state);
	if (mode<0) wMode.SetCurSel(-1);
	else wMode.SetCurSel(mode);
	wMode.EnableWindow(mode!=-1 && viewsign);
	}
  selscan=false;
  *pResult = 0;    
  }

void CGroupDlg::OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
	NMLISTVIEW  *phdn = (NMLISTVIEW *) pNMHDR;
	if (GetKeyState(VK_RBUTTON) & 0x80) return;  	  
	// TODO: Add your control notification handler code here
	if ((phdn->uNewState & LVIS_SELECTED) || (phdn->uOldState & LVIS_SELECTED))
	  {
	  selscan=true;
	  }	
/*	int q=-1;
	if (phdn->uNewState & 0x2000) q=1;
	if (phdn->uNewState & 0x1000) q=0;
	if (q>=0)
	  {
	  CNewsGroup *grp=(CNewsGroup *)phdn->lParam;
	  if (grp && viewsign) 
		grp->fullload=q!=0;
	  if (selscan==false)
		{
		selscan=true;
		for (int i=-1;i=grlist.GetNextItem(i,LVNI_SELECTED),i!=-1;)
		  {
		  grlist.SetItemState(i,INDEXTOSTATEIMAGEMASK((q)+1), LVIS_STATEIMAGEMASK);
		  }
		selscan=false;
		}
	  }*/
	*pResult = 0;
  }

void CGroupDlg::OnSignup() 
  {
  int sigstate=signup.GetCheck();
  if (sigstate==2) return;
  int i=grlist.GetNextItem(-1,LVNI_SELECTED);
  bool state=sigstate!=0;
  while (i!=-1)
	{
	CNewsGroup *grp=(CNewsGroup *)grlist.GetItemData(i);
	grp->sign=state;
	LVITEM it;
	it.mask=LVIF_IMAGE ;
	it.iItem=i;
	it.iSubItem=0;
	it.iImage=state?1:2;
	grlist.SetItem(&it);
	i=grlist.GetNextItem(i,LVNI_SELECTED);
	}
  }


//DEL void CGroupDlg::RemoveDuplicity()
//DEL   {
//DEL   int cnt=0;
//DEL   if (dupscan) return;
//DEL   dupscan=true;
//DEL   int grpcount=checker->GetGroupCount();
//DEL   while (checker!=NULL && dupscan)
//DEL 	{
//DEL 	progress.SetRange32(0,grpcount);
//DEL 	CNewsGroup *grp=checker->GetNext()->FindGroup(checker->GetName());
//DEL 	while (grp!=NULL)
//DEL 	  {
//DEL 	  CNewsGroup *p=checker,*q;
//DEL 	  while (p->GetNext()!=grp) p=p->GetNext();
//DEL 	  q=grp->GetNext();
//DEL 	  grp->Unlink();
//DEL 	  if (grp==curacc->GetNew()) 
//DEL 		curacc->SetNew(q);
//DEL 	  p->SetNext(q);
//DEL 	  LVFINDINFO fi;
//DEL 	  fi.flags=LVFI_PARAM;
//DEL 	  fi.lParam=(LPARAM)grp;
//DEL 	  int fnd=grlist.FindItem(&fi);
//DEL 	  grlist.DeleteItem(fnd);
//DEL 	  grp=p->GetNext()->FindGroup(checker->GetName());
//DEL 	  cnt++;
//DEL 	  }
//DEL 	checker=checker->GetNext();
//DEL     runflag=true;
//DEL 	cnt++;
//DEL   	MSG msg;
//DEL 	if (::PeekMessage(&msg,NULL,0,0,PM_REMOVE))
//DEL 	  {
//DEL 	  DispatchMessage(&msg);
//DEL 	  progress.SetPos(cnt);
//DEL 	  }
//DEL 	}
//DEL   progress.SetPos(grpcount);
//DEL   dupscan=false;
//DEL }



void CGroupDlg::OnDestroy() 
  {
  KillTimer(1);
  dupscan=false;		
  CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
  }

void CGroupDlg::OnCancel() 
  {
  KillTimer(1);
  filterop=2;
  CDialog::OnCancel();
  }

static int qsortproc(const void *a, const void *b)
  {
  CNewsGroup *ga=*(CNewsGroup **)a;
  CNewsGroup *gb=*(CNewsGroup **)b;
  int i=strcmp(ga->GetName(),gb->GetName());
  if (i==0)
	i=(ga->sign<gb->sign)-(ga->sign>gb->sign);
  return i;
  }

static CNewsGroup *Slucuj(CNewsGroup *left, CNewsGroup *right)
  {
  CNewsGroup *z,*q;
  int res;
  res=qsortproc(&left,&right);
  if (res<=0) {z=left;left=left->GetNext();}else {z=right;right=right->GetNext();}
  z->Unlink();
  q=z;
  while (left && right)
	{
	res=qsortproc(&left,&right);
	if (res<=0)
	  {
	  q->SetNext(left);
	  left=left->GetNext();
	  }
	else
	  {
	  q->SetNext(right);
	  right=right->GetNext();
	  }
	q=q->GetNext();
	q->Unlink();
	}
  if (left) q->SetNext(left);else q->SetNext(right);
  return z;
  }

static void MergeSort(CNewsGroup **src, int count)
  {
  int pn,pp;
  while (count>1)
	{
	for (pp=0,pn=0;pp<count;pp+=2,pn++)
	  {
	  if (pp+1>=count) src[pn]=src[pp];
	  else src[pn]=Slucuj(src[pp],src[pp+1]);
	  }
	count=pn;
	}  
  }


int CGroupDlg::CheckDupFaster()
  {
  int vmode=0;
  if (curacc->GetGroups()==NULL) return vmode;
  CNewsGroup *p=curacc->GetGroups();
  int grpcount=p->GetGroupCount();
  CNewsGroup **grplist=new CNewsGroup *[grpcount];
  int i;
  for (i=0;i<grpcount;i++)
	{
	if (p->sign && vmode!=1) vmode=2;
	if (p->isnew) vmode=1;
	grplist[i]=p;p=p->GetNext();curacc->UnlinkGroup(grplist[i]);
	}
  MergeSort(grplist,grpcount);
  CNewsGroup *a=grplist[0];
  while (a!=NULL)
	{
	if (a->GetNext() && strcmp(a->GetNext()->GetName(),a->GetName())==0)
	  {
	  CNewsGroup *b=a->GetNext();
	  CNewsGroup *c=b->GetNext();
	  b->Unlink();
	  a->SetNext(c);
	  }
	else
	  {
	  a=a->GetNext();
	  }
	}  
  curacc->AppendGrouplist(grplist[0]);
  delete [] grplist;
  return vmode;
  }

void CGroupDlg::OnChangeFilter() 
  {
  if (tmr!=-1) KillTimer(tmr);
  tmr=SetTimer(1000,1000,NULL);
  }

BOOL CGroupDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	if (IsWindowEnabled()==FALSE) 
	  {
	  SetCursor(::LoadCursor(NULL,IDC_WAIT));
	  return 0;
	  }
	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}



void CGroupDlg::OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	viewmode=wTab.GetCurSel();
	if (viewmode==2) ReformatList(40);
	else ReformatList(15);
    LoadToList();
	*pResult = 0;
}

void CGroupDlg::ReformatList(int perc)
  {
  CRect rc;
  grlist.GetClientRect(&rc);
  int sz=rc.right;
  sz-=GetSystemMetrics(SM_CXVSCROLL)+2*GetSystemMetrics(SM_CXBORDER);
  int sz2=sz*perc/100;
  int sz1=sz-sz2;
  grlist.SetColumnWidth(0,sz1);
  grlist.SetColumnWidth(1,sz2);
  }

void CGroupDlg::OnSelendokCombomode() 
  {
  POSITION pos=grlist.GetFirstSelectedItemPosition();
  int state=wMode.GetCurSel();
  if (state && curacc->offline==0)
	{
	if (NrMessageBox(IDS_NOCACHESET,MB_OKCANCEL|MB_ICONSTOP)==IDOK)	  
	  curacc->offline=4096*1024;
	else
	  wMode.SetCurSel(state=0);
	}
  while (pos)
	{
	int nItem = grlist.GetNextSelectedItem(pos);
	CNewsGroup *grp=(CNewsGroup *)grlist.GetItemData(nItem);
	grp->fullload=state;	
	grlist.SetItemText(nItem,1,modes[state]);
	}  
  }

void CGroupDlg::OnDblclkGroups(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  signup.SetCheck(!signup.GetCheck());
  OnSignup();
  *pResult = 0;
  }
