#if !defined(AFX_UPDATEPROGRESSDLG_H__C4CB9720_B45E_11D5_B3A0_0050FCFE63F1__INCLUDED_)
#define AFX_UPDATEPROGRESSDLG_H__C4CB9720_B45E_11D5_B3A0_0050FCFE63F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UpdateProgressDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUpdateProgressDlg dialog

class CUpdateProgressDlg : public CDialog
{
// Construction
  int cntr;
  CImageListEx anims;
public:
	void DoneDownloadNotify();
	bool stop;
	CUpdateProgressDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUpdateProgressDlg)
	enum { IDD = IDD_UPDATEDLG };
	CProgressCtrl	progress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUpdateProgressDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUpdateProgressDlg)
	virtual void OnCancel();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UPDATEPROGRESSDLG_H__C4CB9720_B45E_11D5_B3A0_0050FCFE63F1__INCLUDED_)
