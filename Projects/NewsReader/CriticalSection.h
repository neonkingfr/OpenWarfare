// CriticalSection.h: interface for the CCriticalSection class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CRITICALSECTION_H__F53BE3CE_644D_4128_AA25_3D32E37D5D7D__INCLUDED_)
#define AFX_CRITICALSECTION_H__F53BE3CE_644D_4128_AA25_3D32E37D5D7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*
class CCriticalSection  
{
  CRITICAL_SECTION cs;
public:
	CCriticalSection()
	  {
	  InitializeCriticalSection(&cs);
	  }
	~CCriticalSection()
	  {
	  LeaveCriticalSection(&cs);
	  }
	void Lock() 
	  {
	  EnterCriticalSection(&cs);
	  }
	void Unlock() 
	  {
	  LeaveCriticalSection(&cs);
	  }
};
*/

class CScoopCritSect
  {
  CCriticalSection &cs;
  public:
	CScoopCritSect(CCriticalSection& s):cs(s) {cs.Lock();}
	~CScoopCritSect() {cs.Unlock();}
  };

class CScoopMutex
  {
  CMutex &cs;
  bool owned;
  public:
	CScoopMutex(CMutex& s,DWORD timeout):cs(s) {owned=cs.Lock(timeout)!=FALSE;}
	~CScoopMutex() {if (owned) cs.Unlock();}
	bool IsOwned() {return owned;}
	bool operator!() {return !owned;}
  };

#endif // !defined(AFX_CRITICALSECTION_H__F53BE3CE_644D_4128_AA25_3D32E37D5D7D__INCLUDED_)
