// FullTextIndex.h: interface for the FullTextIndex class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FULLTEXTINDEX_H__CD72315D_F322_4B94_83FB_78D757BF46A7__INCLUDED_)
#define AFX_FULLTEXTINDEX_H__CD72315D_F322_4B94_83FB_78D757BF46A7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Projects/BTree/BTree.h>
#include <io.h>
#include "MMapBTree.h"

class DiskBTreePosixIO;

#define FULLTEXT_OFFSETERROR 0xFFFFFFFF
#define FULLTEXT_OFFSETNOTFOUND 0

#define DECLAREOPERATOR(op) bool operator op(const FullIndexWordItem &other) const {return Compare(other) op 0;}

int FullTextCzechCompare(const char *a, const char *b);

class FullTextWordList
{
friend class FullTextIndex;
friend class FullTextSearch;
struct FullIndexWordItem
{
  const char *word;
  int Compare(const FullIndexWordItem &other)const 
  {
    if (word==NULL) return other.word==NULL?0:-1;
    if (other.word==NULL) return 1;
    return FullTextCzechCompare(word,other.word);
  }
  DECLAREOPERATOR(==)  
  DECLAREOPERATOR(!=)  
  DECLAREOPERATOR(>)  
  DECLAREOPERATOR(<)  
  DECLAREOPERATOR(>=)  
  DECLAREOPERATOR(<=)  
  FullIndexWordItem(const char *w=NULL) {word=w;}
};

  BTree<FullIndexWordItem>_index;
  char *_begfile;      //begin of file
  char *_beglist;      //begin of list
  char *_endlist;      //end of list
  char *_endfile;      //end of file.

  HANDLE _maphandle;   //handle to mapped object
  HANDLE _mapfile;     //handle to mapped file;
  Pathname _filename;  //filename of mapped file;

  bool IndexFile();
  bool AllocSpace(size_t size);
  FullTextWordList(const FullTextWordList& other);
  void Init();
  void Done();
public:
  FullTextWordList();
  ~FullTextWordList();
  bool Create(const char *filename);
  bool IsCreated() {return _beglist!=NULL;}
  unsigned long AddWord(const char *word);
  unsigned long FindWord(const char *word);
  const char *GetWord(unsigned long offset);
  bool ResetFile();  //it will delete word list and prepare for regenerate it
  bool ResetIndex();  //it will delete index to force reindexation at next access
  bool ForceIndex();  //will check, if index exists. If not, it will generate it now
  void Close();  //will close file a make instance uninicialized
};

#undef DECLAREOPERATOR

#define DECLAREOPERATOR(op) bool operator op(const FullTextIncidenceIndex &other) const {return Compare(other) op 0;}
class FullTextInciList
{
  struct FullTextIncidenceItem
  {
    unsigned long wordOffset:31;   //offset do tabulky slov nebo msgId
    unsigned long markMsgId:1;  //1 znaci ze jde o offset do tabulky msgId

    FullTextIncidenceItem(unsigned long wo=0, bool mark=true)
    {
      wordOffset=wo;
      markMsgId=mark;
    }
    FullTextIncidenceItem &operator=(unsigned long zero)
    {
      wordOffset=zero;
      markMsgId=true;
      return *this;
    }
  };

  struct FullTextIncidenceIndex
  {
    const FullTextIncidenceItem *index;
    int Compare(const FullTextIncidenceIndex& other) const
    {
    if (index==NULL) return other.index==NULL?0:-1;
    if (other.index==NULL) return 1;
      if (index->wordOffset==other.index->wordOffset)
      {
        unsigned long p1=index->markMsgId?0xFFFFFFFF:(unsigned long)index;
        unsigned long p2=other.index->markMsgId?0xFFFFFFFF:(unsigned long)other.index;
        return (p1<p2)-(p1>p2);
      }
      else
        return (index->wordOffset>other.index->wordOffset)-(index->wordOffset<other.index->wordOffset);
    }   
    DECLAREOPERATOR(==)  
    DECLAREOPERATOR(!=)  
    DECLAREOPERATOR(>)  
    DECLAREOPERATOR(<)  
    DECLAREOPERATOR(>=)  
    DECLAREOPERATOR(<=)  
    FullTextIncidenceIndex(const FullTextIncidenceItem *w=NULL) {index=w;}
  };

  friend class FullTextIndex;

  unsigned long lastMsgIdOffset;      //msgid offset naposled zapsane
  const FullTextIncidenceItem *lastChangeOffset; //adresa naposled zapsaneho msgid (pro rychlejsi hledani)

  BTree<FullTextIncidenceIndex>_index;
  char *_begfile;      //begin of file
  FullTextIncidenceItem *_beglist;      //begin of list
  FullTextIncidenceItem *_endlist;      //end of list
  char *_endfile;      //end of file.

  HANDLE _maphandle;   //handle to mapped object
  HANDLE _mapfile;     //handle to mapped file;
  Pathname _filename;  //filename of mapped file;

  bool IndexFile();
  bool AllocSpace(size_t size);
  FullTextInciList(const FullTextInciList& other);
  void Init();
public:
  FullTextInciList();
  ~FullTextInciList();
  bool Create(const char *filename);
  bool IsCreated() {return _beglist!=NULL;}
  bool AddIncidence(unsigned long word, unsigned long id);
  bool Find(unsigned long word, BTreeIterator<FullTextIncidenceIndex> &iter);
  FullTextIncidenceItem *GetIncidence(unsigned long offset);
  unsigned long GetMsgIdOffset(unsigned long offset);
  unsigned long GetMsgIdOffset(const FullTextIncidenceItem *item)
  {
    while (!item->markMsgId && (char *)item>_begfile) item--;
    return item->wordOffset;
  }
  bool ResetFile();  //it will delete word list and prepare for regenerate it
  bool ResetIndex();  //it will delete index to force reindexation at next access
  bool ForceIndex();  //will check, if index exists. If not, it will generate it now
  void Close();  //will close file a make instance uninicialized

};

#undef DECLAREOPERATOR
/*

class FullTextDiskIncidence
{
  struct IncidenceItem
  {
    unsigned long wordOffset;
    unsigned long msgOffset;
    bool operator>(const IncidenceItem &other) const
    {
      return wordOffset>other.wordOffset || 
        (wordOffset==other.wordOffset && msgOffset>other.msgOffset);
    }
    bool operator<(const IncidenceItem &other) const
    {
      return wordOffset<other.wordOffset || 
        (wordOffset==other.wordOffset && msgOffset<other.msgOffset);
    }
  };

  
  DiskBTreePosixIO *_io;
  DiskBTree<IncidenceItem> *_index;
  int _file;

public:
  FullTextDiskIncidence();
  ~FullTextDiskIncidence();
  bool Create(const char *filename);
  bool IsCreated() {return _file!=0;}
  bool AddIncidence(unsigned long word, unsigned long id);
  bool Find(unsigned long word, DiskBTreeEnum& enumerator);
  bool ResetFile();  //it will delete word list and prepare for regenerate it
  bool ResetIndex() {return true;}
  bool ForceIndex() {return true;}
  void Close();  //will close file a make instance uninicialized
  friend class FullTextIndex;

};
*/
class FullTextSearch;

class FullTextIndex  
{

  friend class FullTextSearch;
  struct WordCmp
  {
    char *text;
    bool constant;
    int compare(const WordCmp& other) const
    {
      if (text==NULL) return (other.text!=NULL);
      if (other.text==NULL) return -1;
      return strcmp(text,other.text);
    }
    bool operator > (const WordCmp& other) const {return compare(other.text)>0;}
    bool operator < (const WordCmp& other) const {return compare(other.text)<0;}
    bool operator == (const WordCmp& other) const {return compare(other.text)==0;}
    bool operator >= (const WordCmp& other) const {return compare(other.text)>=0;}
    bool operator <= (const WordCmp& other) const {return compare(other.text)<=0;}
    bool operator != (const WordCmp& other) const {return compare(other.text)!=0;}
    WordCmp():text(NULL),constant(true) {}
    WordCmp(const char *cval): text(const_cast<char *>(cval)),constant(true) {}
    WordCmp(const WordCmp &other): text(strdup(other.text)),constant(false) {}
    WordCmp &operator=(const char *cval) {if (text && !constant) free(text); if (cval==NULL) {text=const_cast<char *>(cval);constant=true;} else {text=strdup(cval);constant=false;}return *this;}
    WordCmp &operator=(const WordCmp &other) {if (text && !constant) free(text); text=strdup(other.text);constant=false;return *this;}
    ~WordCmp() {if (!constant) free(text);}
  };
  FullTextWordList _wordList;
  FullTextWordList _msgIdList;
  MMapBTree _incidenceList;
  HANDLE _incFile;
  CCriticalSection _mt;
  BTree<WordCmp> ignoredWords;
  unsigned long lastmsgid;
public:
	bool IsIndexed(const char *messageId);
    bool NeedIndex(const char *messageId);
    void IndexEmpty(const char *messageId)
    {
      _msgIdList.AddWord(messageId);
    }

    FullTextIndex();
    
    bool Create(const char *accountName, const char *cachePath=NULL, bool recreate=false);
	virtual ~FullTextIndex();
    
    bool Add(const char *word, const char *messageId);
    
    template <class Functor>
    bool QueryWord(const char *word, Functor &fcall)
    {
      CSingleLock guard(&_mt,TRUE);
      unsigned long wo=_wordList.FindWord(word);
      if (wo==FULLTEXT_OFFSETERROR) return false;
      if (wo==FULLTEXT_OFFSETNOTFOUND) return false;
      MMapBTreeEnum iter;
	  MMapBTreeBlockItem search;
	  search.key=wo;
	  search.dataOffset=0;
      _incidenceList.Find(search,iter);
      while (_incidenceList.Next(iter,search) && search.key==wo)
      {
        const char *msgid=_msgIdList.GetWord(search.dataOffset);
        if (msgid) 
          {if (!fcall(msgid)) return true;}
        else
          return false;
      }      
      return true;
    }

/*    template <class Functor>
    bool QueryWord(const char *word, Functor &fcall)
    {
      CSingleLock guard(&_mt,TRUE);
      unsigned long wo=_wordList.FindWord(word);
      if (wo==FULLTEXT_OFFSETERROR) return false;
      if (wo==FULLTEXT_OFFSETNOTFOUND) return false;
      BTreeIterator<FullTextInciList::FullTextIncidenceIndex> iter(_incidenceList._index);
      _incidenceList.Find(wo,iter);
      FullTextInciList::FullTextIncidenceIndex *found;
      while ((found=iter.Next())!=NULL && found->index->wordOffset==wo)
      {
        const char *msgid=_msgIdList.GetWord(_incidenceList.GetMsgIdOffset(found->index));
        if (msgid) 
          {if (!fcall(msgid)) return true;}
        else
          return false;
      }      
      return true;
    }
  */      
    /*template <class Functor>
    bool QueryWord(const char *word, Functor &fcall)
    {
      CSingleLock guard(&_mt,TRUE);
      unsigned long wo=_wordList.FindWord(word);
      if (wo==FULLTEXT_OFFSETERROR) return false;
      if (wo==FULLTEXT_OFFSETNOTFOUND) return false;
      DiskBTreeEnum enumerator;
      _incidenceList.Find(wo,enumerator);
      FullTextDiskIncidence::IncidenceItem found;
      while (_incidenceList._index->GetNext(enumerator,&found) && found.wordOffset==wo)
      {
        const char *msgid=_msgIdList.GetWord(found.msgOffset);
        if (msgid) 
          {if (!fcall(msgid)) return true;}
        else
          return false;
      }      
      return true;
    }*/

    
    bool ForceIndex();
    bool ResetIndex();  //it will delete index to force reindexation at next access 
    void Reset();
    void Close()  //will close file a make instance uninicialized
    {
      CSingleLock guard(&_mt,TRUE);
      _wordList.Close();
      _msgIdList.Close();
      _incidenceList.Close();
    }
    bool IsCreated() {return _wordList.IsCreated();}


    void ProcessText(const char *text, const char *messageId);

    void DumpStatistics(bool (*progress)(unsigned long cur, bool total)=NULL);

    void LoadIgnoredWords(const char *igwords);
  

};

class FullTextSearch
{  
  FullTextIndex &_index;
  unsigned long _stat;   //number of words that have more than 50%  
  unsigned char _maxscore; //maximum score that can have one word    
  unsigned char _last_maxscore;
  unsigned char *_searchField;
  unsigned long _fieldSize;


  unsigned long _curScore;
  void SearchByStringInt(char *string);

public:
  enum Flags
  {
    fMustIncluded=1,
    fNotIcluded=2,
    fNoFuzzy=4,
    fPartialWord=8,
    fPartialMax=16    //internal    
  };

  ~FullTextSearch();
  FullTextSearch(FullTextIndex &index);
  void Search(const char *word, unsigned long flags);
  unsigned long GetNumWords() {return _stat;}
  unsigned long GetNextMsgIDIndex(unsigned long last=-1,unsigned char level=25);
  unsigned char GetScore(unsigned long index);
  const char *GetMessageID(unsigned long index);
  void Combine(const FullTextSearch &other, unsigned long flags);

  bool operator() (const char *msgid);

  void SearchByString(const char *string);


};




#endif // !defined(AFX_FULLTEXTINDEX_H__CD72315D_F322_4B94_83FB_78D757BF46A7__INCLUDED_)
