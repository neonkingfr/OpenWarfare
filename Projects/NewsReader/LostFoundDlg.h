#if !defined(AFX_LOSTFOUNDDLG_H__8D30C2E2_9654_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
#define AFX_LOSTFOUNDDLG_H__8D30C2E2_9654_11D5_B39F_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LostFoundDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLostFoundDlg dialog

#include "SendDlg.h"


class CLostFoundDlg : public CDialog
{
// Construction
	int lasty;
	CSendDlg *snd;
	CBrush br;
public:
	virtual void OnCancel();
	CLostFoundDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLostFoundDlg)
	enum { IDD = IDD_LOSTFOUND };
	CEdit	preview;
	CListCtrl	list;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLostFoundDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	bool IsSeparator(CPoint &pt);
	void OpenMessage(int i);
	void DeleteMessage(int i);

	// Generated message map functions
	//{{AFX_MSG(CLostFoundDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOSTFOUNDDLG_H__8D30C2E2_9654_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
