// MailList.h: interface for the CMailList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAILLIST_H__DCB15FA0_F8E8_11D4_B399_00C0DFAE7D0A__INCLUDED_)
#define AFX_MAILLIST_H__DCB15FA0_F8E8_11D4_B399_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMailMessage;

typedef bool (*CMM_ENUMPROC)(CMailMessage *msg, const char *var, const char *value, void *context);

class CMailMessage
  {
  protected:
  CString header;
  CString text;
  char state;
  public:
	void EnumProperties(CMM_ENUMPROC proc, void *context);
	void GetMailProperty(const char *name,CString& str);
	CMailMessage(const char *header, const char *text);
	CMailMessage() {}
	const char *GetText() {return text;}
	const char *GetHeader() {return header;}
	void SetText(const char *text) {this->text=text;}
	void SetHeader(const char *head) {header=head;}
  void Save(std::ostream& str) {str << (const char *)header << (const char *)text;}
	CString operator [](const char *val);
	void SetState(char state) {this->state=state;}
	char GetState() {return state;}
  protected:
    void LoadHeaderValue(std::istream &in, CString& data);
    void ScanHeader(std::istream& in, char *buffer,int size);
  };


class CMailList  
  {
  int count;
  long *ids;  //list of IDS - each message has its own ID number. ID number is derivated from real size
			  //of the message. There is possibility, that there will be two messages with the same ID
			  //then - both messages must be downloaded each time, when mailbox is checked (in few cases)			  
  CMailMessage *list;
  			  //list of messages
  public:
	int GetFreeMailpos();
	void RemoveDuplicity();
    void SetMailMessage(int mailpos,CMailMessage *msg,long ID);
	void SetID(int mailpos, long ID) {if (mailpos>=0 && mailpos<count) ids[mailpos]=ID;}
	CMailMessage *GetMessageByPos(int pos) {return (pos>=0 && pos<count)?list+pos:NULL;}
    CMailMessage *GetMessage(long id);
    bool CopyMessByID(int mailpos, long ID, CMailList& src);
	CMailList() {count=0;ids=NULL;list=NULL;}
	CMailList(int newcount);
	int GetCount() {return count;}
	virtual ~CMailList() {delete [] list;delete [] ids;}
	void SetMessState(int mess, char state) {if (mess>=0 && mess<count) list[mess].SetState(state);}
	char GetMessState(int mess) {return (mess>=0 && mess<count)?list[mess].GetState():0;}
	CMailMessage& operator[] (int mailpos)
	  {
	  static CMailMessage temp,*out=GetMessageByPos(mailpos);
	  return out==NULL?temp:*out;
	  }
  };

#endif // !defined(AFX_MAILLIST_H__DCB15FA0_F8E8_11D4_B399_00C0DFAE7D0A__INCLUDED_)
