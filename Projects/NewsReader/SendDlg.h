#if !defined(AFX_SENDDLG_H__7C9888F5_CC80_45EB_B6BD_9770C4A09FE4__INCLUDED_)
#define AFX_SENDDLG_H__7C9888F5_CC80_45EB_B6BD_9770C4A09FE4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SendDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSendDlg dialog
struct GRPFINDINFO;
#include "EditEx.h"
#include "SendingDlg.h"
#include "GetUrlDlg.h"	// Added by ClassView
#include "ThreadPropExDlg.h"	// Added by ClassView
#include <ole2.h>
#include "MessagePartCache.h"
#include "WebBrowser2.h"

class CNewsAccount;
class CNewsGroup;
class CNewsTerminal;

#define WM_OPENDLGMSG (WM_APP)

class CSendDlg : public CDialog, public IDropTarget
  {
  // Construction
  CSize defsize;
  POINT minpos;
  CFont edtfont;
  LOGFONT lgedfont;
  CImageListEx attachIcon;
  int edtcolor;
  CString msgname;
  bool PostMess(CNewsTerminal &term, const char *text);
  CSendingDlg sendingDlg;
  int showcmd;
  bool ensync;
  bool dropfiles;
  CString newthrdprop;
  enum {thDlg_Left,thDlg_Right,thDlg_Bottom,thDlg_Hide} thDlgPlace;
  CBrush _winBrush;
  CWebBrowser2 webEditor;
  public:
	  void UpdateTexFromWebEditor();
	  void UpdateWebEditor();
	  void SearchRelated();
	  void OpenCloseAttach();
	  void AddToAttachList(const char *filename);
	  void AttachListReset();
	  void ReadjustWindow();
	  void PlaceThreadPropertiesDialog();
	  CThreadPropExDlg _thrPropDlg;
    bool bAutoDelete;
    bool OutboxSend(const char *name);
    CGetUrlDlg geturl;
    void Create(bool visible=true,CWnd *parent=NULL);
    int SendDataInDlg();
    bool LoadMessage(const char *name);
    bool SaveMessage();
    bool PostAttachments(CNewsTerminal &term);
    int PostByEmail(const char *text);
    int PostToNews(CNewsAccount *acc, const char *group);
    void SetNewsTarget(CNewsAccount *acc,CNewsGroup *grp);
    virtual void OnCancel();
    CSendDlg(CWnd* pParent = NULL);   // standard constructor
    
    CString thrdprop;
    CString cancelid;
    CString wikiLink;
	PMessagePart lockedPart;
    // Dialog Data
    //{{AFX_DATA(CSendDlg)
	enum { IDD = IDD_SENDMESSAGE };
	CButton	wEnableHtml;
	CListCtrl	wRelated;
	CListCtrl	wAttachList;
	CButton	bReference;
	CButton	bInsUrl;
    CButton	bSend;
    CButton	bMessRef;
    CEditEx	wnd_msgbody;
    CButton	bStopThread;
    CButton	bInsFile;
    CButton	bAttach;
    CButton bWiki;
    CString	from;
    CString	msgbody;
    CString	ref;
    CString	subject;
    CString	to;
    BOOL	raw;
	BOOL	vEnableHtml;
	//}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CSendDlg)
  public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual void PostNcDestroy();
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void Serialize(CGenArchive &arch);
    
    // Generated message map functions
    //{{AFX_MSG(CSendDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnInsertfile();
    afx_msg void OnQuote();
    afx_msg void OnSend();
    afx_msg void OnAttach();
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    afx_msg void OnAttachpopupDelete();
    afx_msg void OnAttachpopupOpen();
    afx_msg void OnDropFiles(HDROP hDropInfo);
    afx_msg void OnMessref();
    afx_msg void OnStopthread();
    afx_msg LRESULT OnNextDlgCtl(WPARAM wParam, LPARAM lParam);
    afx_msg void OnEditpopupFont();
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    afx_msg void OnEditpopupColor();
    afx_msg void OnClose();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LRESULT OnOpenSendDlg(WPARAM wParam, LPARAM lParam);
    afx_msg void OnInserturl();
	afx_msg void OnMove(int x, int y);
    afx_msg LPARAM OnReadjustWindow(WPARAM wParam,LPARAM lParam);
	afx_msg void OnReplacemsg();
	afx_msg void OnDblclkAttachlist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusSubject();
	afx_msg void OnDblclkRelated(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickRelated(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDestroy();
	afx_msg void OnEnablehtml();
	afx_msg void OnChangeMsgbody();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

  virtual HRESULT STDMETHODCALLTYPE QueryInterface( 
                /* [in] */ REFIID riid,
                /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
  virtual ULONG STDMETHODCALLTYPE AddRef( void);
  virtual ULONG STDMETHODCALLTYPE Release( void);
  virtual HRESULT STDMETHODCALLTYPE DragEnter( 
            /* [unique][in] */ IDataObject *pDataObj,
            /* [in] */ DWORD grfKeyState,
            /* [in] */ POINTL pt,
            /* [out][in] */ DWORD *pdwEffect);
        
  virtual HRESULT STDMETHODCALLTYPE DragOver( 
            /* [in] */ DWORD grfKeyState,
            /* [in] */ POINTL pt,
            /* [out][in] */ DWORD *pdwEffect);
        
  virtual HRESULT STDMETHODCALLTYPE DragLeave( void);
        
  virtual HRESULT STDMETHODCALLTYPE Drop( 
            /* [unique][in] */ IDataObject *pDataObj,
            /* [in] */ DWORD grfKeyState,
            /* [in] */ POINTL pt,
            /* [out][in] */ DWORD *pdwEffect);
  FORMATETC format;
  FORMATETC filedrop;

  private:
	  bool _stopRelatedSearch;
	  CString _lastSearch;
	  CWinThread *_searchRelatedThread;
      CNewsArtHolder *_lastSearchThread;
      int _lastThreadPos;
	  CNewsAccount * FindAccount(const char *accname);
	  LRESULT RelatedCallback(WPARAM wParam,LPARAM lParam);
	  friend bool FindRelatedCallback(GRPFINDINFO& fnd);
	  void StopRelatedSearch();
	  void UpdateRelatedWindow();
  public:
    afx_msg void OnBnClickedWiki();
};

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENDDLG_H__7C9888F5_CC80_45EB_B6BD_9770C4A09FE4__INCLUDED_)
