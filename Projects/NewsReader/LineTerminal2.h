// LineTerminal.h: interface for the LineTerminal class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINETERMINAL_H__36E16C21_F677_4784_8AFA_8BA72A6C79F8__INCLUDED_)
#define AFX_LINETERMINAL_H__36E16C21_F677_4784_8AFA_8BA72A6C79F8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArchiveStreamMemory.h"

class LineTerminal  
{
    ArchiveStream &_input;
    ArchiveStream &_output;
    ArchiveStreamMemoryOut _line;
public:
	bool SplitAt(const char *part, char **left, char **right, char splitter, bool dupsplitter=true);
	const char * ReadLine();
	LineTerminal(ArchiveStream &input,ArchiveStream &output);
	virtual ~LineTerminal();
    bool SendFormattedV(const char *format, va_list args);
    bool SendFormatted(const char *format,...);
	bool SendLine(const char *text, int len=-1, bool sendcrlf=true);
    ArchiveStream &GetInput() {return _input;}
    ArchiveStream &GetOutput() {return _output;}
	const char *GetLastMessage() {return _line.GetBuffer();}
protected:
};

#endif // !defined(AFX_LINETERMINAL_H__36E16C21_F677_4784_8AFA_8BA72A6C79F8__INCLUDED_)
