#if !defined(AFX_POPSESSION_H__3AC7AFD0_F43B_11D4_B399_00C0DFAE7D0A__INCLUDED_)
#define AFX_POPSESSION_H__3AC7AFD0_F43B_11D4_B399_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// POPSession.h : header file
//


#include "MailList.h"

/////////////////////////////////////////////////////////////////////////////
// CPOPSession command target

#define CPOP_IOERROR -1
#define CPOP_UNABLETOCONNECT -2
#define CPOP_POPERROR -3
#define CPOP_SMTPERROR -4
#define CPOP_UNEXCEPTEDREPLY -5
#define CPOP_USERUNKNOWN -6
#define CPOP_ACCESSDENIED -7
#define CPOP_UNABLETOCONNECTSMTP -8
#define CPOP_REJECTED -9
#define CPOP_OPERATIONCANCELED -10


#pragma warning( disable : 4290 ) 



class CPOPException
  {
  int error;
  char *extended;
  public:
	const char * GetErrorText();
	CPOPException(int error):error(error),extended(NULL) {};
	CPOPException(int error,const char *ext):error(error) 
	  {
	  extended=strdup(ext);
	  }
	CPOPException(const CPOPException& other):error(other.error)
	  {
	  extended=strdup(other.extended);
	  }
	int GetError() {return error;}
	~CPOPException() {free(extended);}	
  };

class CPOPSession : CSocket
{
// Attributes

	CTime laststamp; //to check timeouts;
	CTimeSpan timeout; //curtimeout;
    char buffer[256];
	int read;	
	CWnd *status;

public:
	bool debug; //set TRUE if you want to send messages to status window with debug info
		//WPARAM = 2, LPARAM = (const char *) text : debug text read
		//WPARAM = 3, LPARAM = (const char *) text : debug text written
		//WPARAM = 4, clear debug log

// Operations
public:
	CPOPSession();
	virtual ~CPOPSession();

// Overrides
public:
	void ReadMailMessage(int mailslot, CMailMessage &msg);
	void ReadMessageIDs(long *list, int count);
	static void TranslateText(const char *src, CString &trg, int maxchars);
	void DeleteMessage(int index);
	void QuitMailbox();
	void UndoMailboxChanges();
    void SetStatusBar(CWnd *s) {status=s;}
	void ConnectByEmail(CString &email,CString& login, CString &password, CString &pop3, int port);
	bool IsConnected();
	void CloseConnection();
	void CloseSendMail();
	void SendLine(const char *line);
	void OpenSendMail();
	void MailTo(const char *to);
	void MailFrom(const char *from);
	int GetMailboxStat();
	void ConnectSMTP(const char *smtpname, int port=25, const char *myname=NULL);
	void ConnectPop(const char *username, const char *password, const char *popserver, int port=110);
	void ReadLine(CString& line) ;
	void WriteLine(const char *line);
	void WriteFormatted(const char *format,...);
	void WriteFormattedV(const char *format, va_list list);
	void ReciveMessage(int msg, CString& from, CString& subject, CString &text, CString& header, CString& to, CString& date);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPOPSession)
	public:
	virtual BOOL OnMessagePending();
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CPOPSession)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
private:
	bool ReceivePopReply(CString &comment) throw( CPOPException );
	int DecodeFirstValue(const char *line, int def=0);
	int GetSMTPReply(CString &comment);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POPSESSION_H__3AC7AFD0_F43B_11D4_B399_00C0DFAE7D0A__INCLUDED_)
