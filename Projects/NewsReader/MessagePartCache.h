// MessagePartCache.h: interface for the CMessagePartCache class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGEPARTCACHE_H__C6C3E756_89DE_49F9_B8CD_963DB7222717__INCLUDED_)
#define AFX_MESSAGEPARTCACHE_H__C6C3E756_89DE_49F9_B8CD_963DB7222717__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMessagePartCache;
class CMessagePart;

class PMessagePart
  {
  CMessagePart **_msg;
  mutable CMessagePartCache *_cache;
  int _index;
  public:
    PMessagePart(CMessagePart **msgptr,CMessagePartCache *cache, int index):
        _msg(msgptr),_index(index),_cache(cache) {}
    PMessagePart():
        _msg(NULL),_index(NULL),_cache(NULL) {}

    PMessagePart(const PMessagePart &other);
    PMessagePart &operator=(const PMessagePart &other);

    bool IsNull() const {return _msg==NULL;}
    operator CMessagePart *() {return IsNull()?NULL:*_msg;}
    operator const CMessagePart *() const {return IsNull()?NULL:*_msg;}
    CMessagePart *operator->() {return *_msg;}
    const CMessagePart *operator->() const {return *_msg;}
    ~PMessagePart();
  };

#define CMC_MAXCACHESIZE 256

class CMessagePartCache  
{
unsigned long _globcntr;
struct CacheItem
  {
  CMessagePart *msg;
  unsigned long owner;
  unsigned long lru_info;
  unsigned long locks;
  };

CacheItem cache[CMC_MAXCACHESIZE];
int _curcachesize;

CCriticalSection _lock;

public:
	void FlushCache();
	PMessagePart LockObject(unsigned long owner);
	void UnlockObject(int index);
	void LockObject(int index);
	int FindObject(unsigned long owner);
	int FindEmptyPos();
	void RemoveFromCache(int index);
	void AddToCache(CMessagePart *message,unsigned long owner);
    CMessagePartCache();
    virtual ~CMessagePartCache();
    void Reset(unsigned long owner);
	static void InitCache();

    void SetCacheSize(int items);
    
};



#endif // !defined(AFX_MESSAGEPARTCACHE_H__C6C3E756_89DE_49F9_B8CD_963DB7222717__INCLUDED_)
