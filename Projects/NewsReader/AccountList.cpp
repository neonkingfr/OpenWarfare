// AccountList.cpp: implementation of the CAccountList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "AccountList.h"
#include "Archive.h"
#include "ArchiveStreamFileMapping.h"
#include "ScanGroupsThread.h"
#include <fstream>
#include <io.h>
using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CAccountList::~CAccountList()
  {
  }

//--------------------------------------------------

void CAccountList::InsertAccount(CNewsAccount *acc,bool select)
  {
  HTREEITEM it=tree.InsertItem(acc->GetAccName(),ICON_NWSERVER,ICON_NWSERVER,TVI_ROOT,TVI_SORT);
  tree.SetItemData(it,(DWORD)acc);
  if (select) tree.SelectItem(it);
  acc->treeitem=it;
  }

//--------------------------------------------------

void CAccountList::DeleteAccount(HTREEITEM it)
  {
  if (GetItemType(it)!=Itm_Account) return;
  theApp.Lock();
  CNewsAccount *acc=(CNewsAccount *)tree.GetItemData(it);
  if (acc) 
    {
    TRACE1("Releasing account %s\n",acc->GetAccName());
    acc->Release();
    }
  tree.DeleteItem(it);  
  theApp.Unlock();
  }

//--------------------------------------------------

CNewsAccount * CAccountList::GetAccount(HTREEITEM it)
  {
  if (hlast==it) return last;
  hlast=it;
  if (it==NULL) return last=NULL;
  if (GetItemType(it)!=Itm_Account) return last=NULL;
  CNewsAccount *acc=(CNewsAccount *)tree.GetItemData(it);
  return last=acc;
  }

//--------------------------------------------------

void CAccountList::AlterAccount(HTREEITEM it,bool delfull)
  { 
  if (GetItemType(it)!=Itm_Account) return;
  CNewsAccount *acc=(CNewsAccount *)tree.GetItemData(it);
  tree.SetItemText(it,acc->GetAccName());
  CNewsGroup *grp=acc->GetGroups();
  if (delfull)
    {
    HTREEITEM sub;
    while ((sub=tree.GetNextItem(it,TVGN_CHILD))!=NULL)
      tree.DeleteItem(sub);
    while (grp) 
      {grp->iteminlist=NULL;grp=grp->GetNext();}
    }
  grp=acc->GetGroups();
  grp->LoadToTree(tree,it,acc->viewastree);
  acc->dirty=true;
  if (!acc->IsLoaded())
    {
    TVITEM itp;
    itp.mask=TVIF_CHILDREN|TVIF_HANDLE;
    itp.hItem=it;
    itp.cChildren=1;
    tree.SetItem(&itp);
    }
  }

//--------------------------------------------------

void CAccountList::DeleteAll()
  {
  HTREEITEM it=tree.GetNextItem(NULL,TVGN_ROOT);
  while (it!=NULL)
    {
    DeleteAccount(it);
    it=tree.GetNextItem(NULL,TVGN_ROOT);
    }
  }

//--------------------------------------------------

void CAccountList::InitList()
  {
  if (itcreate==NULL)
    {
    CString str;
    str.LoadString(IDS_CREATENEWACCOUNT);
    itcreate=tree.InsertItem(str);
    }
  }

//--------------------------------------------------

bool CAccountList::AddGroup(HTREEITEM account,CNewsGroup *grp,bool bold)
  {
  if (GetItemType(account)!=Itm_Account) return NULL;
  CNewsAccount *acc=GetAccount(account);
  if (acc->AddGroup(grp)==false) return false;
  HTREEITEM it=tree.InsertItem(grp->GetName(),1,1,account,TVI_SORT);
  if (bold) tree.SetItemState(it,TVIS_BOLD,TVIS_BOLD);
  tree.SetItemData(it,(DWORD)grp);
  return true;
  }

//--------------------------------------------------

CNewsGroup * CAccountList::GetGroup(HTREEITEM it)
  {
  if (GetItemType(it)!=Itm_Group) return NULL;
  CNewsGroup *acc=(CNewsGroup *)tree.GetItemData(it);
  return acc;
  }

//--------------------------------------------------

bool CAccountList::DeleteGroup(HTREEITEM it)
  {
  if (GetItemType(it)!=Itm_Group) return false;
  CNewsGroup *grp=GetGroup(it);
  HTREEITEM itp=tree.GetNextItem(it,TVGN_PARENT);
  if (itp==NULL) return false;
  CNewsAccount *acc=GetAccount(itp);
  if (acc->UnlinkGroup(grp)==false) return false;
  delete grp;
  tree.DeleteItem(it);
  return true;
  }

//--------------------------------------------------

void CAccountList::SerializeAccount(HTREEITEM it, bool save)
  {
  if (GetItemType(it)!=Itm_Account) return;
  CNewsAccount *acc=GetAccountEx(it);
  if (acc==NULL) return;
  if (acc->mutex.Lock(10000)==FALSE) return;
  fstream s;
  ASSERT(save==false); // Nelze zapisovat ve starem formate.
  if (save)
    {
    if (acc->IsLoaded())
      {
      CString tempname=theApp.FullPath(CString(acc->GetFName())+".save");
      TRACE1("Saving account info for: %s\n", acc->GetAccName());
      s.open(tempname,ios::out|ios::trunc|ios::binary);
      acc->Serialize(CGenArchiveOut(s));
      CString name=theApp.FullPath(acc->GetFName()+CString(ACCEXT));      
      ListState(it,s,true);
      if (!(!s))
        {
        s.close();
        if (_access(tempname,0)==0)
          {
          DeleteFile(name);
          MoveFile(tempname,name);
          }
        }
      }
    }
  else
    {
    TRACE1("Loadin account info for: %s\n", acc->GetFName());
    s.open(theApp.FullPath(acc->GetFName()+CString(ACCEXT)),ios::in|ios::binary);
    acc->Serialize(CGenArchiveIn(s));
    TRACE1("Found & loaded account : %s\n", acc->GetAccName());
    acc->Compact(true);
    /*	CNewsGroup *grp=acc->GetGroups();
	theApp.Lock();
	acc->ActivateKeywords();
	while (grp) {grp->GetList()->CalcHierarchy();grp=grp->GetNext();}
	theApp.Unlock();*/
    AlterAccount(it);
    if (acc->IsLoaded()) ListState(it,s,false);
    }
  acc->mutex.Unlock();
  }

//--------------------------------------------------

static void CheckForLost(CAccountList &ls)
  {
  CFileFind fnd;
  if (fnd.FindFile(theApp.FullPath("*.save")))
    {
    BOOL rep;
    do
      {
      CString msg;
      rep=fnd.FindNextFile();
      CString fromfile=fnd.GetFilePath();
      CString tofile=fromfile;
      tofile.Delete(tofile.ReverseFind('.'),5);
      tofile+=ACCNEWEXT;
      if (_access(tofile,0)==-1)
        {
        AfxFormatString1(msg,IDS_FOUNDLOSTACC,fnd.GetFileName());
        int id=NrMessageBox(msg,MB_YESNOCANCEL|MB_ICONQUESTION);
        if (id==IDYES)
          {
          if (MoveFile(fromfile,tofile)==0)
            {
            NrMessageBox(IDS_FONDLOSTFAIL,MB_OK);
            }
          }
        if (id==IDNO)
          DeleteFile(fnd.GetFilePath());
        if (id==IDCANCEL) rep=FALSE;
        }
      }
    while (rep);
    }
  }

//--------------------------------------------------

void CAccountList::SerializeList(bool save)
  {
  if (!save)
    {
    CheckForLost(*this);
    CFileFind fnd;
    if (fnd.FindFile(theApp.FullPath("*"ACCNEWEXT)))
      {
      BOOL rep;
      #ifndef DEMO
      do
        #endif
        {
        rep=fnd.FindNextFile();
        CString p=fnd.GetFileName();		
        p=p.Left(p.GetLength()-strlen(ACCNEWEXT));
        CNewsAccount *acc=new CNewsAccount();
        acc->SetFName(p);
        InsertAccount(acc,false);
        }
      #ifndef DEMO
      while (rep);
      #endif
      }
    if (fnd.FindFile(theApp.FullPath("*"ACCEXT)))
      {
      BOOL rep;
      #ifndef DEMO
      do
        #endif
        {
        rep=fnd.FindNextFile();
        CString p=fnd.GetFileName();
        p=p.Left(p.GetLength()-strlen(ACCEXT));
        HTREEITEM lst=tree.GetRootItem();
        while (lst)
          {
          CNewsAccount *acc=(CNewsAccount *)tree.GetItemData(lst);
          if (acc && p==acc->GetFName()) goto skip;
          lst=tree.GetNextItem(lst,TVGN_NEXT);
          }
          {
          CNewsAccount *acc=new CNewsAccount();
          acc->SetFName(p);
          InsertAccount(acc,false);
          }
skip:;
        }
      #ifndef DEMO
      while (rep);
      #endif
      }
    }
  else
    {
    char *p=strdupst(theApp.FullPath("",false));
    int l=strlen(p);
    if (p[l-1]=='\\') p[l-1]=0;
    CreateDirectory(p,NULL);
    }
  HTREEITEM it=tree.GetNextItem(NULL,TVGN_ROOT);
  while (it)
    {    
    if (AccountStreaming(it,save)==false)
      {
      CNewsAccount *acc=(CNewsAccount *)tree.GetItemData(it);
      if (acc && !save)
        {
        CString msg;
        AfxFormatString1(msg,IDS_INVALIDACCOUNTFILE,(LPCTSTR)acc->GetFName());
        if (AfxMessageBox(msg,MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
          {
		  acc->RestoreInvalidAccount();
          if (AccountStreaming(it,save)==false)
            {
            AfxMessageBox(IDS_UNABLETORESTORE,MB_OK|MB_ICONEXCLAMATION);
            acc->Release();
            HTREEITEM del=it;
            it=tree.GetNextItem(it,TVGN_NEXT);
            tree.DeleteItem(del);
            continue;
            }            
          }
        else
          {
          HTREEITEM del=it;
          it=tree.GetNextItem(it,TVGN_NEXT);
          tree.DeleteItem(del);
          continue;
          }
        }
      }
    it=tree.GetNextItem(it,TVGN_NEXT);
    }
  
  }

//--------------------------------------------------

HTREEITEM CAccountList::FindAccount(const char *name)
  {
  HTREEITEM it=tree.GetNextItem(NULL,TVGN_ROOT);
  while (it)
    {
    CNewsAccount *acc=GetAccount(it);
    if (acc!=NULL)
      if (stricmp(acc->GetAddress(),name)==0) return it;
    it=tree.GetNextItem(it,TVGN_NEXT);
    }
  return NULL;
  }

//--------------------------------------------------

ItemType CAccountList::GetItemType(HTREEITEM it)
  {
  int img1,img2;
  tree.GetItemImage(it,img1,img2);
  switch (img1)
    {
    case ICON_NWSERVER: return Itm_Account;
    case ICON_NWGROUP: case ICON_NWGROUPMARK:
    return Itm_Group;		
    case ICON_NWFOLDER: return Itm_Folder;		
    }
  return Itm_Null;
  }

//--------------------------------------------------

void CAccountList::AlterGroup(HTREEITEM it)
  {
  CNewsGroup *grp=GetGroup(it);
  if (grp==NULL) return;
  int cnt=grp->GetNews();
  CString p=tree.GetItemText(it);
  char *buffer=(char *)alloca(p.GetLength()+20);
  bool bld=(tree.GetItemState(it,TVIS_BOLD) & TVIS_BOLD)!=0;
  strcpy(buffer,p);
    {
    char *z=strchr(buffer,'*');
    if (z!=NULL) z[0]=0;
    if (grp->notify) strcat(buffer,"*");
    }
  //if (bld) 
    {
    char *z=strrchr(buffer,'('); 	  
    if (z!=NULL && z!=buffer && z[-1]==' ') 
      {
      z[-1]=0;
      }
    }
    {
    char *z=strrchr(buffer,'{');
    if (z!=NULL && z!=buffer && z[-1]==' ') 
      {
      z[-1]=0;
      }
    }
  int highlightCount = grp->GetHighlights();
  if (cnt)
    {
    char *nw=strchr(buffer,0);
    sprintf(nw," (%d)",cnt);
    }
  /*
	else if (highlightCount)
	{
		char *nw=strchr(buffer,0);
		sprintf(nw," {%d}",highlightCount);
	}
	*/
  #if 1
  // icon highlighting - causes strange freezing
  if (highlightCount>0)
    {
    tree.SetItemImage(it,ICON_NWGROUPMARK,ICON_NWGROUPMARK);
    }
  else
    {
    tree.SetItemImage(it,ICON_NWGROUP,ICON_NWGROUP);
    }
  #endif
  tree.SetItemText(it,buffer);
  bld=(cnt>0);
  BoldGroup(it,bld);
  }

//--------------------------------------------------

void CAccountList::BoldGroup(HTREEITEM it, bool boldstatus)
  {
  bool bld=(tree.GetItemState(it,TVIS_BOLD) & TVIS_BOLD)!=0;
  int marker;
  if (bld && !boldstatus) marker=-1;
  else if (!bld && boldstatus) marker=1;
  else return;
  tree.SetItemState(it,boldstatus?TVIS_BOLD:0,TVIS_BOLD);
  it=tree.GetNextItem(it,TVGN_PARENT);
  while (it)
    {
    if (GetItemType(it)==Itm_Folder)
      {
      DWORD dw=tree.GetItemData(it);
      dw+=marker;
      tree.SetItemData(it,dw);
      tree.SetItemState(it,dw?TVIS_BOLD:0,TVIS_BOLD);
      }
    it=tree.GetNextItem(it,TVGN_PARENT);
    }
  }

//--------------------------------------------------

HTREEITEM CAccountList::EnumAccounts(HTREEITEM last)
  { 
  if (GetAccount(last)==NULL) last=NULL;
  if (last==NULL) last=tree.GetRootItem();
  else last=tree.GetNextItem(last,TVGN_NEXT);
  while (last && GetAccount(last)==NULL)
    last=tree.GetNextItem(last,TVGN_NEXT);
  return last;
  }

//--------------------------------------------------

CNewsAccount * CAccountList::GetAccountEx(HTREEITEM it)
  {
  if (hlastex==it) return lastex;
  hlastex=it;
  while (it && GetItemType(it)!=ICON_NWSERVER) it=tree.GetNextItem(it,TVGN_PARENT);
  return lastex=GetAccount(it);
  }

//--------------------------------------------------

extern "C"
  {
  #include "lzwc.h"
  }

//--------------------------------------------------

#define MAGIC "BREDY-NEWSREADER"

bool ExportAccount(const char *srcfile, const char *targetfile)
  {
  HANDLE h=NULL,t=NULL;
  h=CreateFile(srcfile,GENERIC_READ,0,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
  DWORD fsize=GetFileSize(h,NULL),rd;
  DWORD osize=-1;
  bool ok=true;
  void *p=malloc(fsize);
  void *trg=NULL;
  char buf[32]=MAGIC;
  if (p==NULL) ok=false;
  if (ok) 
    {
    ReadFile(h,p,fsize,&rd,NULL);
    if (rd!=fsize) ok=false;
    }
  if (ok)
    {
    trg=malloc(fsize+100);
    init_lzw_compressor(8);
    memset(trg,0,fsize+100);
    osize=lzw_encode((unsigned char *)p,trg,fsize);
    done_lzw_compressor();
    if (osize==-1)
      {
      osize=fsize;
      free(trg);
      trg=p;
      }
    t=CreateFile(targetfile,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
    if (t==NULL) ok=false;	
    }
  if (ok)
    {
    WriteFile(t,buf,sizeof(buf),&rd,NULL);
    if (rd!=sizeof(buf)) ok=false;
    }
  if (ok)
    {
    WriteFile(t,&fsize,sizeof(fsize),&rd,NULL);
    if (rd!=sizeof(fsize)) ok=false;
    }
  if (ok)
    {
    WriteFile(t,&osize,sizeof(osize),&rd,NULL);
    if (rd!=sizeof(osize)) ok=false;
    }
  if (ok)
    {
    WriteFile(t,trg,osize,&rd,NULL);
    if (rd!=osize) ok=false;
    }
  CloseHandle(h);
  if (t) CloseHandle(t);
  if (trg!=p && trg!=NULL) free(trg);
  free(p);
  return ok;
  }

//--------------------------------------------------

bool ImportAccount(const char *srcfile, const char *targetfile)
  {
  HANDLE h=NULL,t=NULL;
  h=CreateFile(srcfile,GENERIC_READ,0,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
  DWORD fsize=0,rd;
  DWORD osize=-1;
  bool ok=true;
  void *p=NULL;
  void *trg=NULL;
  if (ok)
    {
    char buf[32];
    ReadFile(h,buf,sizeof(buf),&rd,NULL);
    if (strcmp(buf,MAGIC) || rd!=sizeof(buf)) ok=false;
    }
  if (ok)
    {
    ReadFile(h,&fsize,sizeof(fsize),&rd,NULL);
    if (rd!=sizeof(fsize)) ok=false;
    }
  if (ok)
    {
    ReadFile(h,&osize,sizeof(osize),&rd,NULL);
    if (rd!=sizeof(osize)) ok=false;
    }
  if (ok)
    {
    p=malloc(osize);
    trg=malloc(fsize);
    if (trg==NULL || p==NULL) ok=false;
    }
  if (ok)
    {	
    ReadFile(h,p,osize,&rd,NULL);
    if (rd!=osize) ok=false;
    }
  if (ok)
    {
    init_lzw_compressor(8);
    lzw_decode(p,(unsigned char *)trg);
    t=CreateFile(targetfile,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
    if (t==NULL) ok=false;	
    done_lzw_compressor();
    }
  if (ok)
    {
    WriteFile(t,trg,fsize,&rd,NULL);
    if (rd!=fsize) ok=false;
    }
  CloseHandle(h);
  if (t) CloseHandle(t);
  if (trg!=p && trg!=NULL) free(trg);
  free(p);
  return ok;
  }

//--------------------------------------------------

HTREEITEM CAccountList::FindItem(HTREEITEM from, DWORD value)
  {
  if (from==NULL) from=tree.GetRootItem();
  while (from)
    {
    if (value==tree.GetItemData(from)) return from;
    HTREEITEM child=tree.GetChildItem(from);
    if (child) 
      {
      child=FindItem(child,value);
      if (child) return child;
      }
    from=tree.GetNextItem(from,TVGN_NEXT);
    }
  return NULL;
  }

//--------------------------------------------------

void CAccountList::ListState(HTREEITEM it, iostream &s, bool save, bool one)
  {
  if (save)
    {
    while (it!=NULL)
      {
      unsigned char state=((tree.GetItemState(it,TVIS_EXPANDED) & TVIS_EXPANDED)!=0);
      s.put(state);
      HTREEITEM sub=tree.GetChildItem(it);
      if (sub) ListState(sub,s,save,false);
      it=tree.GetNextItem(it,TVGN_NEXT);
      if (one) break;
      }
    }
  else
    {
    while (it!=NULL)
      {
      unsigned char state=s.get();
      if (state) tree.Expand(it,TVE_EXPAND);else tree.Expand(it,TVE_COLLAPSE);
      HTREEITEM sub=tree.GetChildItem(it);
      if (sub) ListState(sub,s,save,false);
      it=tree.GetNextItem(it,TVGN_NEXT);
      if (one) break;
      }
    }
  }

//--------------------------------------------------

HTREEITEM CAccountList::GetAccountItem(HTREEITEM it)
  {
  HTREEITEM p;
  while ((p=tree.GetNextItem(it,TVGN_PARENT))!=NULL) it=p;
  return it;
  }

//--------------------------------------------------

bool CAccountList::EnsureLoaded(HTREEITEM it)
  {
  CNewsAccount *acc=GetAccountEx(it);
  if (acc) return EnsureLoaded(acc);
  else return false;
  }

//--------------------------------------------------

bool CAccountList::EnsureLoaded(CNewsAccount *acc)
  {
  if (!acc->IsLoaded())
    {
    SetCursor(::LoadCursor(NULL,IDC_WAIT));    
    if (AccountStreaming(acc->treeitem,false)==false)
      {
        CString msg;
        AfxFormatString1(msg,IDS_INVALIDACCOUNTFILE,(LPCTSTR)acc->GetFName());
        if (AfxMessageBox(msg,MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
		{
	      SetCursor(::LoadCursor(NULL,IDC_WAIT));    
		  acc->RestoreInvalidAccount();
          if (AccountStreaming(acc->treeitem,false)==false)
		  {
            AfxMessageBox(IDS_UNABLETORESTORE,MB_OK|MB_ICONEXCLAMATION);
			return false;
		  }
		  return true;
		}
      return false;
      }
    AlterAccount(acc->treeitem);
//    theApp.RunBackground(0,new CAccountCompactThread(this),INFINITE);
    }
  return true;
  }

//--------------------------------------------------

void CAccountList::AlterGroup(CNewsGroup *grp)
  {
  AlterGroup(grp->iteminlist);
  }

//--------------------------------------------------

void CAccountList::AlterAllGroups(CNewsAccount *acc)
  {
  CNewsGroup *grp=acc->GetGroups();
  while (grp)
    {
    if (grp->iteminlist) 
      {
      grp->CountNewsAndHighlights();
      AlterGroup(grp->iteminlist);
      }
    grp=grp->GetNext();
    }
  }

//--------------------------------------------------


bool CAccountList::AccountStreaming(HTREEITEM it, bool save)
  {
  if (GetItemType(it)!=Itm_Account) return false;
  CNewsAccount *acc=GetAccountEx(it);
  if (acc==NULL) return false;
  if (acc->mutex.Lock(10000)==FALSE) return false;
  if (save)
    {
    if (acc->IsLoaded())
      {
      CString tempname=theApp.FullPath(CString(acc->GetFName())+".save");
      CString name=theApp.FullPath(acc->GetFName()+CString(ACCNEWEXT));
      bool error;

      TRACE1("Saving account info for: %s\n", acc->GetAccName());
      HANDLE file=CreateFile(tempname,GENERIC_WRITE|GENERIC_READ,FILE_SHARE_READ,NULL,
            CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,NULL);
        {
        ArchiveStreamFileMappingOut stream(file,true,1024*1024,1024*1024);
        ArchiveSimpleBinary arch(stream);
        acc->Streaming(arch);
        ListStateStreaming(it,arch); 
        error=!arch;
        }   //destructor will close stream
      if (!error)
        {
        if (_access(tempname,0)==0)
          {
          DeleteFile(name+".bak");
          MoveFile(name,name+".bak");
          MoveFile(tempname,name);
          }
        }
      else
        {
        acc->mutex.Unlock();
        return false;
        }
      }
    }
  else
    {
    TRACE1("Loadin account info for: %s\n", acc->GetFName());
    CString name=theApp.FullPath(acc->GetFName()+CString(ACCNEWEXT));
    HANDLE file=CreateFile(name,GENERIC_READ,FILE_SHARE_READ,NULL,
            OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,NULL);
    if (file==INVALID_HANDLE_VALUE)
      {
      acc->mutex.Unlock();
      SerializeAccount(it,save);
      return true;
      }
    ArchiveStreamFileMappingIn stream(file,true,64*1024);
    ArchiveSimpleBinary arch(stream);
    acc->Streaming(arch);
    if (!arch) 
      {
      acc->mutex.Unlock();
      return false;
      }
    AlterAccount(it);
    if (acc->IsLoaded()) ListStateStreaming(it,arch); 
    TRACE1("Found & loaded account : %s\n", acc->GetAccName());
    acc->Compact(true);
    }
  acc->mutex.Unlock();
  return true;
  }

void CAccountList::ListStateStreaming(HTREEITEM it, Archive &arch, bool one)
  {  
  ArchiveSection archsect(arch);
  while (archsect.Block("TreeStates"))
  while (it!=NULL)
    {
    unsigned char state=((tree.GetItemState(it,TVIS_EXPANDED) & TVIS_EXPANDED)!=0);
    arch("Node",state);
    if (state) tree.Expand(it,TVE_EXPAND);else tree.Expand(it,TVE_COLLAPSE);
    HTREEITEM sub=tree.GetNextItem(it,TVGN_CHILD);
    if (sub) ListStateStreaming(sub,arch,false);
    it=tree.GetNextItem(it,TVGN_NEXT);
    if (one) break;
    }
  }


void CNewsAccount::RestoreInvalidAccount()
{
          CString old=theApp.FullPath(CString(GetFName())+ACCNEWEXT+".old");
          CString cur=theApp.FullPath(CString(GetFName())+ACCNEWEXT);
          CString bak=theApp.FullPath(CString(GetFName())+ACCNEWEXT+".bak");
          DeleteFile(old);
          MoveFile(cur,old);
          MoveFile(bak,cur);
          MoveFile(old,bak);
}
