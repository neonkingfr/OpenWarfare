#if !defined(AFX_SELKWDSET_H__58FEBE00_B6C5_11D5_B3A0_0050FCFE63F1__INCLUDED_)
#define AFX_SELKWDSET_H__58FEBE00_B6C5_11D5_B3A0_0050FCFE63F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelKwdSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelKwdSet dialog

class CSelKwdSet : public CDialog
{
// Construction
public:
	virtual void OnOK();
	CSelKwdSet(CWnd* pParent = NULL);   // standard constructor

	CString kwrds;
	int cursel;
// Dialog Data
	//{{AFX_DATA(CSelKwdSet)
	enum { IDD = IDD_SELECTKWDSET };
	CListBox	wKwList;
	CString	kwList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelKwdSet)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelKwdSet)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELKWDSET_H__58FEBE00_B6C5_11D5_B3A0_0050FCFE63F1__INCLUDED_)
