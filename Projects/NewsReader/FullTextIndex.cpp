// FullTextIndex.cpp: implementation of the FullTextIndex class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include <El/Pathname/Pathname.h>
#include <Es/Types/Pointers.hpp>
#include "FullTextIndex.h"
#include "CharMapCz.h"
#include <locale.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <es/containers/array.hpp>
/*#include "DiskBTree.tli"*/

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define ALLOC_GRANUALITY 4096

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define HEADER "FULLTEXT WORDLIST DATABASE"
#define HEADERINCIDENCE "FULLTEXT INCIDENCE DATABASE"

void FullTextWordList::Init()
{
  _begfile=_beglist=_endlist=_endfile=0;
  _maphandle=_mapfile=0;
  _filename=0;
}
FullTextWordList::FullTextWordList()
{
  Init();
}

FullTextWordList::~FullTextWordList()
{
  Done();
}

void FullTextWordList::Done()
{
  if (_beglist!=NULL)  UnmapViewOfFile(_begfile);  
  if (_maphandle!=NULL) CloseHandle(_maphandle);
  if (_mapfile!=NULL) CloseHandle(_mapfile);
}

bool FullTextWordList::Create(const char *filename)
{
  TRACE1("FULLTEXT: Openning database %s\n",filename);
  _mapfile=CreateFile(filename,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_ALWAYS,
    FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS,NULL);
  if (_mapfile==INVALID_HANDLE_VALUE) return FALSE;
  DWORD fsize=GetFileSize(_mapfile,NULL);
  bool init=true;
  if (fsize<=0) fsize=ALLOC_GRANUALITY;else init=false;
  _maphandle=CreateFileMapping(_mapfile,NULL,PAGE_READWRITE,0,fsize,NULL);
  if (_maphandle==NULL)
  {
    CloseHandle(_mapfile);_mapfile=NULL;
    return false;
  }
  _begfile=(char *)MapViewOfFile(_maphandle,FILE_MAP_ALL_ACCESS,0,0,0);
  if (_begfile==NULL)
  {
    CloseHandle(_maphandle);
    CloseHandle(_mapfile);
    return false;
  }
  _endfile=_begfile+fsize;
  if (init) 
  {
    strcpy(_begfile,HEADER);
    _beglist=strchr(_begfile,0)+1;
    _endlist=_beglist;
    *_endlist=0;    
  }
  else
  {
    if (strcmp(_begfile,HEADER)!=0)
    {
      UnmapViewOfFile(_begfile);
      CloseHandle(_maphandle);
      CloseHandle(_mapfile);
      return false;           //invalid file
    }
    _beglist=strchr(_begfile,0)+1;
    _endlist=_endfile;  //currently we don't know end of list - reindexation is needed.    
  }
  _filename=filename;
  TRACE0("FULLTEXT: success\n");
  return true;
}

bool FullTextWordList::IndexFile()
{
  TRACE1("FULLTEXT: Index procedure started - file %s\n",_filename.GetFilename());
  _index.Clear();
  const char *ptr=_beglist;
  if (ptr==NULL) return false;
  while (*ptr!=0 && ptr<_endfile)
  {
    _index.Add(FullIndexWordItem(ptr));
    ptr=strchr(ptr,0);
    ptr++;
  }
  _endlist=const_cast<char *>(ptr);
  TRACE0("FULLTEXT: Index procedure success\n");
  return true;
}

bool FullTextWordList::AllocSpace(size_t size)
{
  if (_endlist+size<_endfile) return true;
  TRACE1("FULLTEXT: Expanding %s\n",_filename.GetFilename());
  UnmapViewOfFile(_begfile);
  CloseHandle(_maphandle);
  char *savedBeg=_begfile;
  bool res;  
  DWORD cursize=_endfile-_begfile;
  DWORD newsize=cursize+ALLOC_GRANUALITY*(cursize/ALLOC_GRANUALITY/2+1);
  _maphandle=CreateFileMapping(_mapfile,NULL,PAGE_READWRITE,0,newsize,NULL);
  if (_maphandle==NULL) //cannot expand, try alloc previous size;
  {
    _maphandle=CreateFileMapping(_mapfile,NULL,PAGE_READWRITE,0,cursize,NULL);
    if (_maphandle==NULL) //cannot get mapping object back, general error!
    {
      _index.Clear();     //empty index
      _begfile=_endfile=_beglist=_endlist=NULL; //make instance invalid
      return false;       //return error;
    }
    _begfile=(char *)MapViewOfFile(_maphandle,FILE_MAP_ALL_ACCESS,0,0,0); //map file back
    res=false;            //but this is error
  }
  else
  {
    _begfile=(char *)MapViewOfFile(_maphandle,FILE_MAP_ALL_ACCESS,0,0,0); //map new space
    res=true;                       //this is ok
    cursize=newsize;      //update file size
  }
  _endfile=_begfile+cursize;    //update end of file
  _beglist=strchr(_begfile,0)+1; //update _beglist (skip header mark)
  if (savedBeg!=_begfile&& _index.Size())  //test, if index relocation is needed
  {
    int shift=(_begfile-savedBeg);    //relocation shift
    BTreeIterator<FullIndexWordItem> iter(_index);   //relocate index
    iter.Reset();
    FullIndexWordItem *ptr;
    while ((ptr=iter.Next())!=NULL) ptr->word=ptr->word+shift; //relocate pointer
    _endlist=_endlist+shift;
  }
  TRACE2("FULLTEXT: Expanded %s, newsize %d\n",_filename.GetFilename(),cursize);
  return res;
}

unsigned long FullTextWordList::AddWord(const char *word)
{
  if (!IsCreated()) return FULLTEXT_OFFSETERROR;
  if (ForceIndex()==false) return FULLTEXT_OFFSETERROR;
  FullIndexWordItem search(word);
  FullIndexWordItem *found=_index.Find(search);
  if (found) return found->word-_begfile;
  size_t size=strlen(word)+1;
  bool res=AllocSpace(size+1);
  if (res==false) return FULLTEXT_OFFSETERROR;
  strcpy(_endlist,word);
  _index.Add(FullIndexWordItem(_endlist));
  word=_endlist;
  _endlist+=size;
  *_endlist=0;
  return word-_begfile;
}

unsigned long FullTextWordList::FindWord(const char *word)
{
  if (!IsCreated()) return FULLTEXT_OFFSETERROR;
  if (ForceIndex()==false) return FULLTEXT_OFFSETERROR;
  FullIndexWordItem search(word);
  FullIndexWordItem *found=_index.Find(search);
  if (found==NULL) return FULLTEXT_OFFSETNOTFOUND;
  unsigned long offset=found->word-_begfile;
  return offset;

}

const char *FullTextWordList::GetWord(unsigned long offset)
{
  if (!IsCreated()) return NULL;
  const char *pos=_begfile+offset;
  if (pos>_endlist) return NULL;
  return pos;
}

bool FullTextWordList::ResetFile()  //it will delete word list and prepare for regenerate it
{
  if (!IsCreated()) return false;
  ResetIndex();
  _endlist=_beglist;
  *_endlist=0;
  return true;
}

bool FullTextWordList::ResetIndex()  //it will delete index to force reindexation at next access
{
  if (!IsCreated()) return false;
  _index.Clear();
  return true;
}

bool FullTextWordList::ForceIndex()  //will check, if index exists. If not, it will generate it now
{
  if (_index.Size()==0) return IndexFile();
  else return true;
}

void FullTextWordList::Close()
{
Done();
Init();
}

void FullTextInciList::Init()
{
  _begfile=0;
  _beglist=_endlist=0;
  _endfile=0;
  _maphandle=_mapfile=0;
  _filename=0;
  lastMsgIdOffset=0;
}
FullTextInciList::FullTextInciList()
{
  Init();
}

FullTextInciList::~FullTextInciList()
{
  if (_beglist!=NULL)  UnmapViewOfFile(_begfile);  
  if (_maphandle!=NULL) CloseHandle(_maphandle);
  if (_mapfile!=NULL) CloseHandle(_mapfile);
}

bool FullTextInciList::Create(const char *filename)
{
  TRACE1("FULLTEXT: Openning database %s\n",filename);
  _mapfile=CreateFile(filename,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_ALWAYS,
    FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS,NULL);
  if (_mapfile==INVALID_HANDLE_VALUE) return FALSE;
  DWORD fsize=GetFileSize(_mapfile,NULL);
  bool init=true;
  if (fsize<=0) fsize=ALLOC_GRANUALITY;else init=false;
  _maphandle=CreateFileMapping(_mapfile,NULL,PAGE_READWRITE,0,fsize,NULL);
  if (_maphandle==NULL)
  {
    CloseHandle(_mapfile);_mapfile=NULL;
    return false;
  }
  _begfile=(char *)MapViewOfFile(_maphandle,FILE_MAP_ALL_ACCESS,0,0,0);
  if (_begfile==NULL)
  {
    CloseHandle(_maphandle);
    CloseHandle(_mapfile);
    return false;
  }
  _endfile=_begfile+fsize;
  if (init) 
  {
    strcpy(_begfile,HEADERINCIDENCE);
    _beglist=(FullTextIncidenceItem *)(strchr(_begfile,0)+1);
    _endlist=_beglist;
    *_endlist=0;    
  }
  else
  {
    if (strcmp(_begfile,HEADERINCIDENCE)!=0)
    {
      UnmapViewOfFile(_begfile);
      CloseHandle(_maphandle);
      CloseHandle(_mapfile);
      return false;           //invalid file
    }
    _beglist=(FullTextIncidenceItem *)(strchr(_begfile,0)+1);
    _endlist=(FullTextIncidenceItem *)_endfile;  //currently we don't know end of list - reindexation is needed.    
  }
  _filename=filename;
  lastMsgIdOffset=0;
  TRACE0("FULLTEXT: success\n");
  return true;
}

bool FullTextInciList::IndexFile()
{
  TRACE1("FULLTEXT: Index procedure started - file %s\n",_filename.GetFilename());
  _index.Clear();
  const FullTextIncidenceItem *ptr=_beglist;
  if (ptr==NULL) return false;
  while (ptr->wordOffset!=0 && (char *)ptr+sizeof(FullTextIncidenceItem)<_endfile)
  {
    if (!ptr->markMsgId)
      _index.Add(FullTextIncidenceIndex(ptr));    
    else
    {
      lastMsgIdOffset=ptr->wordOffset;
      lastChangeOffset=ptr;
    }
    ptr++;
  }
  _endlist=const_cast<FullTextIncidenceItem *>(ptr);
  TRACE0("FULLTEXT: Index procedure success\n");
  return true;
}

bool FullTextInciList::AllocSpace(size_t size)
{
  if ((char *)_endlist+size<_endfile) return true;
  TRACE1("FULLTEXT: Expanding %s\n",_filename.GetFilename());
  UnmapViewOfFile(_begfile);
  CloseHandle(_maphandle);
  char *savedBeg=_begfile;
  bool res;
  DWORD cursize=_endfile-_begfile;
  DWORD newsize=cursize+ALLOC_GRANUALITY*(cursize/ALLOC_GRANUALITY/2+1);
  _maphandle=CreateFileMapping(_mapfile,NULL,PAGE_READWRITE,0,newsize,NULL);
  if (_maphandle==NULL) //cannot expand, try alloc previous size;
  {
    _maphandle=CreateFileMapping(_mapfile,NULL,PAGE_READWRITE,0,cursize,NULL);
    if (_maphandle==NULL) //cannot get mapping object back, general error!
    {
      _index.Clear();     //empty index
      _begfile=_endfile=NULL;
      _beglist=_endlist=NULL; //make instance invalid
      return false;       //return error;
    }
    _begfile=(char *)MapViewOfFile(_maphandle,FILE_MAP_ALL_ACCESS,0,0,0); //map file back
    res=false;            //but this is error
  }
  else
  {
    _begfile=(char *)MapViewOfFile(_maphandle,FILE_MAP_ALL_ACCESS,0,0,0); //map new space
    res=true;                       //this is ok
    cursize=newsize;      //update file size
  }
  _endfile=_begfile+cursize;    //update end of file
  _beglist=(FullTextIncidenceItem *)(strchr(_begfile,0)+1); //update _beglist (skip header mark)
  if (savedBeg!=_begfile&& _index.Size())  //test, if index relocation is needed
  {
    int shift=(_begfile-savedBeg);    //relocation shift
    BTreeIterator<FullTextIncidenceIndex> iter(_index);   //relocate index
    iter.Reset();
    FullTextIncidenceIndex *ptr;
    while ((ptr=iter.Next())!=NULL) ptr->index=(FullTextIncidenceItem *)((char *)ptr->index+shift); //relocate pointer
    lastChangeOffset=(FullTextIncidenceItem *)((char *)lastChangeOffset+shift); //relocate pointer
    _endlist=(FullTextIncidenceItem *)((char *)_endlist+shift);
  }
  TRACE2("FULLTEXT: Expanded %s, newsize %d\n",_filename.GetFilename(),cursize);
  return res;
}

bool FullTextInciList::AddIncidence(unsigned long word, unsigned long id)
{
  if (!IsCreated()) return false;
  if (ForceIndex()==false) return false;
  FullTextIncidenceItem search(word);  
  BTreeIterator<FullTextIncidenceIndex> iter(_index);
  if (_index.Size())
  {
    iter.BeginFrom(FullTextIncidenceIndex(&search));
    FullTextIncidenceIndex *found;
    bool ffound=false;
    while ((found=iter.Next())!=NULL && found->index->wordOffset==word && !found->index->markMsgId)
    {
      if (id==lastMsgIdOffset)
        {
        if (found->index>lastChangeOffset) ffound=true;
        break;
        }
      else
        break;        
    }  
    if (ffound) 
      return true;  
  }
  bool res=AllocSpace(3*sizeof(FullTextIncidenceItem ));
  if (res==false) return false;
  if (id!=lastMsgIdOffset) 
  {
    *_endlist++=FullTextIncidenceItem (id,true);
    lastMsgIdOffset=id;
    lastChangeOffset=_endlist;
  }
  *_endlist=FullTextIncidenceItem (word,false);
  _index.Add(FullTextIncidenceIndex(_endlist));
  _endlist++;
  *_endlist=0;
  return true;
}

bool FullTextInciList::Find(unsigned long word, BTreeIterator<FullTextIncidenceIndex> &iter)
{
  if (!IsCreated()) return false;
  if (ForceIndex()==false) return false;
  FullTextIncidenceItem search(word);
  iter.BeginFrom(FullTextIncidenceIndex(&search));
  return true;
}

FullTextInciList::FullTextIncidenceItem *FullTextInciList::GetIncidence(unsigned long offset)
{
  if (!IsCreated()) return NULL;
  FullTextIncidenceItem *pos=(FullTextIncidenceItem *)(_begfile+offset);
  if (pos>=_endlist) return NULL;
  return pos;
}

unsigned long FullTextInciList::GetMsgIdOffset(unsigned long offset)
{
  if (!IsCreated()) return 0;
  FullTextIncidenceItem *pos=(FullTextIncidenceItem *)(_begfile+offset);
  if (pos>=_endlist) return 0;
  while (!pos->markMsgId) pos--;
  return pos->wordOffset;
}

bool FullTextInciList::ResetFile()  //it will delete word list and prepare for regenerate it
{
  if (!IsCreated()) return false;
  ResetIndex();
  _endlist=_beglist;
  *_endlist=0;
  return true;
}

bool FullTextInciList::ResetIndex()  //it will delete index to force reindexation at next access
{
  if (!IsCreated()) return false;
  _index.Clear();
  return true;
}

bool FullTextInciList::ForceIndex()  //will check, if index exists. If not, it will generate it now
{
  if (_index.Size()==0) return IndexFile();
  else return true;
}

void FullTextInciList::Close()
{
FullTextInciList::~FullTextInciList();
Init();
}

FullTextIndex::FullTextIndex()
{
  
}

FullTextIndex::~FullTextIndex()
{

}

bool FullTextIndex::Create(const char *accountName, const char *cachePath,bool recreate)
{
  CSingleLock guard(&_mt,TRUE);
  setlocale(LC_CTYPE,"Czech");
  Pathname work;
  if (cachePath) work.SetDirectory(cachePath);
  work.SetFiletitle(accountName);
  work.SetExtension(".fwl");
  if (recreate) DeleteFile(work);
  if (_wordList.Create(work)==false) return false;
  work.SetExtension(".fml");
  if (recreate) DeleteFile(work);
  if (_msgIdList.Create(work)==false) 
  {
    _wordList.Close();
    return false;
  }
  work.SetExtension(".fti");
  if (recreate) DeleteFile(work);
  if  (_incidenceList.Create(work)==false)
  {
     _wordList.Close();
     _msgIdList.Close();
     return false;

  }
//  DumpStatistics();
  return true;
}


bool FullTextIndex::Add(const char *word, const char *messageId)
{  
  CSingleLock guard(&_mt,TRUE); 
  MMapBTreeBlockItem incd;
  incd.key=_wordList.AddWord(word);
  if (incd.key==FULLTEXT_OFFSETERROR) return false;
  incd.dataOffset=_msgIdList.AddWord(messageId);
  if (incd.dataOffset==FULLTEXT_OFFSETERROR) return false;
  _incidenceList.Add(incd);    
  return true;
  
}

bool FullTextIndex::ForceIndex()
{
  CSingleLock guard(&_mt,TRUE);
  _wordList.ForceIndex();
  _msgIdList.ForceIndex();
/*  _incidenceList.ForceIndex();*/
  return true;
}


bool FullTextIndex::ResetIndex()
{
  CSingleLock guard(&_mt,TRUE);
  _wordList.ResetIndex();
  _msgIdList.ResetIndex();
/*  _incidenceList.ResetIndex();*/
  return true;
}
int FullTextCzechCompare(const char *a, const char *b)
{  
  char *tmp_a=strcpy((char *)alloca(strlen(a)+1),a);
  char *tmp_b=strcpy((char *)alloca(strlen(b)+1),b);
  czmap(tmp_a);
  czmap(tmp_b);
  int res=strcmp(tmp_a,tmp_b);
  if (res==0) res=strcmp(a,b);
  return res;
}

static inline bool WordSeparator(char c)
{
  if ((unsigned char)c>=33 && c!='.' && c!=',' && c!=';' && c!=':' && c!='(' && c!='<' && c!='{' && c!='[' && c!='-')
    return false;
  return true;
}

static inline bool WordSeparator2(char c)
{
  if ((unsigned char)c>=33 && c!='.' && c!=',' && c!=';' && c!=':')
    return false;
  return true;
}


static inline bool isalnumczex(char c)
{
  return (isalnum((unsigned char)czmap(c)) || c=='_');
}

static bool DetectBinaryStream(const char *ptr)
{
  int len = strlen(ptr);
  if (len <= 4) //short words are not binary always
      return false;
  static char *samohl="aeiouy��������rlh";
  int numsouhl=0;
  int maxnumsouhl=0;
  int cntalfnum=0;
  bool wasnum=false;
  int symbs = 0;
  while (*ptr!=0)
  {
    if (isdigit((unsigned char)*ptr))
      if (!wasnum)
      {
        wasnum=true;
        cntalfnum++;
      }
      else;
    else
      if (wasnum)
      {
        wasnum=false;
        cntalfnum++;
      }
      if (WordSeparator(*ptr)) {
          symbs++;
      } else {
        char *sam=strchr(samohl,*ptr);
        if (*ptr != '_' && sam==NULL)    
          numsouhl++;
        else
        {
          if (numsouhl>maxnumsouhl) maxnumsouhl=numsouhl;
          numsouhl=0;
        }
      }
    ptr++;
  }
  return maxnumsouhl>7 || cntalfnum>3 || symbs>len/2 ;

}
//first word contain whole word with symbol.
//second word contain only word without symbols.
//if function returns true, first buff contain word
//second buff can contain word only if is different from first buff
static bool ReadWord(const char * &ptr, char *buff1, int size1, char *buff2, int size2)
{
  buff1[0]=0;
  char *bufsav=buff1;
  char *bufsav1=buff1;
  char *bufsav2=buff2;
  while (WordSeparator2(*ptr) && *ptr) 
    ptr++;
  if (*ptr==0) return false;
  bool waitend=false;
  while (--size1>0)
  {
    if (WordSeparator2(*ptr)) break;
    if (isalnumczex(*ptr)) waitend=true;
    else if (waitend)
    {
      if (!WordSeparator(*ptr)) *buff1++=*ptr++;
      break;
    }
    *buff1++=*ptr++;
  }
  *buff1=0;
  while (*bufsav && !isalnumczex(*bufsav)) bufsav++;
  while (*bufsav && isalnumczex(*bufsav) && --size2>0) *buff2++=*bufsav++;
  *buff2=0;
  if (strcmp(bufsav1,bufsav2)==0) bufsav2[0]=0;
  return true;
}

static inline void czStrLwr(const char *buffer, int buffsize, char *output, int outputsize)
{
  LCID lang=MAKELANGID(LANG_CZECH,SUBLANG_DEFAULT); 
  LCMapStringA(lang,LCMAP_LOWERCASE,buffer,buffsize,output,outputsize);
}

void FullTextIndex::ProcessText(const char *text, const char *messageId)
{
  int textlen=strlen(text)+1;
  char *temp=(char *)(textlen>32000?malloc(textlen):alloca(textlen));
  czStrLwr(text,textlen,temp,textlen);
  text=temp;
  CSingleLock guard(&_mt,TRUE);
  char buff1[128];
  char buff2[128];
  while (ReadWord(text,buff1,128,buff2,128))  
  {    
    if (DetectBinaryStream(buff1)==false)
    {
      if (buff1[0] && ignoredWords.Find(WordCmp(buff1))==NULL) Add(buff1,messageId);
      if (buff2[0] && ignoredWords.Find(WordCmp(buff2))==NULL) Add(buff2,messageId);
    }
  }
  if (textlen>32000) free(temp);
}

bool FullTextIndex::IsIndexed(const char *messageId)
  {
  CSingleLock guard(&_mt,TRUE);
  unsigned long offset=_msgIdList.FindWord(messageId);
  return offset!=FULLTEXT_OFFSETNOTFOUND && offset!=FULLTEXT_OFFSETERROR;
  }

bool FullTextIndex::NeedIndex(const char *messageId)
  {
  CSingleLock guard(&_mt,TRUE);
  unsigned long offset=_msgIdList.FindWord(messageId);
  return offset==FULLTEXT_OFFSETNOTFOUND && offset!=FULLTEXT_OFFSETERROR;
  }

class FullTextIndex_DumpHelper
{
  int _cnt;
public:
  FullTextIndex_DumpHelper():_cnt(0) {}
  int GetCount() {return _cnt;}
  bool operator() (const char *msgid) {_cnt++;return true;}
};

struct FullTextIndex_DumpSort
{
  const char *word;
  unsigned long count;
};

TypeIsSimple(FullTextIndex_DumpSort);

static int CompareItems(const void *left, const void *right)
{
  const FullTextIndex_DumpSort *a1=(const FullTextIndex_DumpSort *)left;
  const FullTextIndex_DumpSort *a2=(const FullTextIndex_DumpSort *)right;
  if (a1->count==a2->count) return strcmp(a1->word,a2->word);
  else
	return (a1->count<a2->count)-(a1->count>a2->count);
}

void FullTextIndex::DumpStatistics(bool (*progress)(unsigned long cur, bool total) )
{ 
  CSingleLock guard(&_mt,TRUE);
  AutoArray<FullTextIndex_DumpSort> statlist;
  const char *ptr=_wordList._beglist;
  _wordList.ForceIndex();
  unsigned long total=_wordList._index.Size();
  if (progress) progress(total,true);
  unsigned long cur=0;
  while (*ptr)
  {
    FullTextIndex_DumpHelper hlp;
    QueryWord(ptr,hlp);
	FullTextIndex_DumpSort &item=statlist.Append();
	item.word=ptr;
	item.count=hlp.GetCount();
    ptr=strchr(ptr,0)+1;
    if (progress) progress(cur++,false);
  }
  statlist.QSortBin(CompareItems);
  guard.Unlock();
  Pathname pth,pth2;
  pth.SetTempDirectory();
  pth.SetTempFile();
  pth2=pth;
  pth.SetExtension(".txt");
  TRACE1("FULLTEXT: Dumping statistics %s\n",pth.GetFullPath());
  HANDLE file=CreateFile(pth,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,0,NULL);
  if (file==INVALID_HANDLE_VALUE) return;
  for (int i=0;i<statlist.Size();i++)
  {
	char buff[256];
	DWORD wrt;
    sprintf(buff,"%15d ",statlist[i].count);
    WriteFile(file,buff,strlen(buff),&wrt,NULL);
    WriteFile(file,statlist[i].word,strlen(statlist[i].word),&wrt,NULL);
    WriteFile(file,"\r\n",2,&wrt,NULL);
  }
  CloseHandle(file);
  TRACE1("FULLTEXT: Dump done.\n",pth.GetFullPath());
  ShellExecute(NULL,NULL,pth.GetFullPath(),NULL,NULL,SW_SHOWNORMAL);
  Sleep(10000);
  DeleteFile(pth);
  DeleteFile(pth2);
}

void FullTextIndex::LoadIgnoredWords(const char *igwords)
{
  CSingleLock guard(&_mt,TRUE);
  ignoredWords.Clear();
  int textlen=strlen(igwords)+1;
  char *temp=(char *)(textlen>32000?malloc(textlen):alloca(textlen));
  czStrLwr(igwords,textlen,temp,textlen);
  igwords=temp;
  char buff1[128];
  char buff2[128];  
  while (ReadWord(igwords,buff1,128,buff2,128)) 
  {    
    ignoredWords.Add(WordCmp(buff1));
  }
  if (textlen>32000) free(temp);
}

void FullTextIndex::Reset()
{
  CSingleLock guard(&_mt,TRUE);
  _wordList.ResetFile();
  _msgIdList.ResetFile();
  _incidenceList.Clear();
}

/*
FullTextDiskIncidence::FullTextDiskIncidence()
{
  _index=NULL;
  _io=NULL;
  _file=0;
}


FullTextDiskIncidence::~FullTextDiskIncidence()
{
  delete _index;
  delete _io;
  if (_file) close(_file);
}

bool FullTextDiskIncidence::Create(const char *filename)
{
  _file=open(filename,_O_BINARY|_O_RANDOM|_O_RDWR|_O_CREAT,_S_IREAD | _S_IWRITE);
  if (_file==-1) return false;
  _io=new DiskBTreePosixIO(_file,512);
  _index=new DiskBTree<IncidenceItem> (_io);
  if (_index->GetError()==DSKBTERR_UNABLEREADHEADER) return ResetFile();
  return true;
}

 bool FullTextDiskIncidence::AddIncidence(unsigned long word, unsigned long id)
 {
   if (_index->GetError()) return false;
   IncidenceItem search;
   search.msgOffset=id;
   search.wordOffset=word;
   if (_index->Find(search)) return true;
   if (_index->GetError()) return false;
   _index->Add(search);
   if (_index->GetError()) return false;
   return true;
 }

bool FullTextDiskIncidence::Find(unsigned long word, DiskBTreeEnum& enumerator)
{
  IncidenceItem search;
  search.wordOffset=word;
  search.msgOffset=0;
  enumerator=_index->BeginSearch(search);
  return _index->GetError()==0;
}

bool FullTextDiskIncidence::ResetFile()
{
    _lseek(_file,0,SEEK_SET);
    write(_file,NULL,0);  
    if (_index->CreateHeader("FULTEXIC",1)==false) 
    {
      close(_file);
      return false;
    }   
    return true;  
}

 void FullTextDiskIncidence::Close()
 {
   FullTextDiskIncidence::~FullTextDiskIncidence();
   _index=NULL;
   _io=NULL;
   _file=0;
 }
 */


FullTextSearch::FullTextSearch(FullTextIndex &index):_index(index), _curScore(10)
{
  _index._mt.Lock();
  _stat=0;
  _last_maxscore=_maxscore=0;
  _index._wordList.ForceIndex();
  _index._msgIdList.ForceIndex();
  _fieldSize=_index._msgIdList._index.Size();
  _searchField=new unsigned char[_fieldSize]; 
  memset(_searchField,0,_index._msgIdList._index.Size());
}


FullTextSearch::~FullTextSearch()
{
  _index._mt.Unlock();
  delete [] _searchField;
}


bool FullTextSearch::operator() (const char *msgid)
{
  int pos=_index._msgIdList._index.RankOf(FullTextWordList::FullIndexWordItem(msgid));
  unsigned char &score=_searchField[pos];
  _searchField[pos]=__max(_searchField[pos],_curScore);
  return true;
}

static char CompareWordsScore(const char *w1, const char *w2)
{
  float curscr=10.0f;
  int w1l=strlen(w1);
  int w2l=strlen(w2);
  if (w1l>w2l) curscr-=(float)(w1l-w2l)*(10.0f/w1l);
  else if (w1l<w2l) curscr-=(float)(w2l-w1l)*(10.0f/w2l); 
  if (curscr<0) return 0;
  char *bw1=czmap(strcpy((char *)alloca(w1l+1),w1));
  char *bw2=czmap(strcpy((char *)alloca(w2l+1),w2));
  int fails=0;
  int maxchr=__min(w1l,w2l);
  while (*bw1 && *bw2)
  {
    if (*bw1!=*bw2) 
    {
      if (bw1[1]==bw2[0] && bw1[2]==bw1[1]) bw1++;
      else if (bw1[0]==bw2[1] && bw1[1]==bw2[2]) bw2++;
      fails++;
    }    
    bw1++;
    bw2++;
  }
  if (fails) curscr-=(fails*10.0f)/maxchr+1;
  if (curscr==10.0f)
    if (strcmp(w1,w2)!=0) curscr-=1.0f;
  if (curscr<0) return 0;
  return (char)((int)(curscr));      
}

void FullTextSearch::Search(const char *word, unsigned long flags)
{
  if (flags & fPartialWord)
  {
    int len=strlen(word);
    _index._wordList.ForceIndex();
    BTreeIterator<FullTextWordList::FullIndexWordItem> iter(_index._wordList._index);
    iter.BeginFrom(FullTextWordList::FullIndexWordItem(word));
    FullTextWordList::FullIndexWordItem *result;
    FullTextSearch helper(_index);
    while ((result=iter.Next()) && (strncmp(result->word,word,len)==0))    
        helper.Search(result->word,fPartialMax);    
    Combine(helper,flags);
  }
  else if (flags & fNoFuzzy)
  {
    FullTextSearch helper(_index);
    helper.Search(word,fPartialMax);
    Combine(helper,flags);
  }
  else if (flags & fPartialMax)
  {	
    _index.QueryWord(word,*this);
  }
  else 
  {
    bool wasAdded=false;
    int diff=0;
    int wsize=_index._wordList._index.Size();
    int pos=_index._wordList._index.RankOf(FullTextWordList::FullIndexWordItem(word));
    FullTextSearch helper(_index);

    do
    {
      wasAdded=false;
      int left=pos-diff-1;
      int right=diff+pos;
      const char *leftw=left>=0?_index._wordList._index.ItemWithRank(left)->word:0;
      const char *rightw=right<wsize?_index._wordList._index.ItemWithRank(right)->word:0;
      int lsc=leftw?CompareWordsScore(word,leftw):0;
      int rsc=rightw?CompareWordsScore(word,rightw):0;
      if (lsc)
      {
        helper._curScore=lsc;
        helper.Search(leftw,fPartialMax);
        wasAdded=true;
      }
      if (rsc)
      {
        helper._curScore=rsc;
        helper.Search(rightw,fPartialMax);
        wasAdded=true;
      }
      diff++;
    }
    while (wasAdded && diff<1000);
    Combine(helper,flags);
  }

}

void FullTextSearch::Combine(const FullTextSearch &other, unsigned long flags) 
  {  
  if (other._fieldSize!=_fieldSize) return;
  if (_maxscore==0 && (flags & (fMustIncluded|fNotIcluded))!=0)
  {
	_maxscore=10;
	memset(_searchField,_maxscore,_fieldSize);
  }
  unsigned long last_maxscore=_maxscore;
  if (flags & fMustIncluded)
    for (int i=0;i<_fieldSize;i++)
      _searchField[i]=_searchField[i]*other._searchField[i]/10;
  else if (flags & fNotIcluded)
    for (int i=0;i<_fieldSize;i++)
      _searchField[i]=_searchField[i]*(10-other._searchField[i])/10;
  else
  {
    for (int i=0;i<_fieldSize;i++)
      _searchField[i]=_searchField[i]+other._searchField[i];
    _maxscore+=10;
  }
  _stat=0;
  unsigned long hranice=_maxscore>>2;
  for (int i=0;i<_fieldSize;i++)
    if (_searchField[i]>hranice) ++_stat;
  }

unsigned long FullTextSearch::GetNextMsgIDIndex(unsigned long last,unsigned char flevel)
  {
    unsigned char level=_maxscore*flevel/100;
    while (++last<_fieldSize)
    {
      if (_searchField[last] && _searchField[last]>=level) return last;
    }
    return -1;
  }

unsigned char FullTextSearch::GetScore(unsigned long word)
{
  return _searchField[word]*100/_maxscore;
}

const char *FullTextSearch::GetMessageID(unsigned long index)
{
  return _index._msgIdList._index.ItemWithRank(index)->word;
}


void FullTextSearch::SearchByString(const char *string)
{
  int len=strlen(string)+1;
  char *cpy=(char *)alloca(len);
  czStrLwr(string,len,cpy,len);
  SearchByStringInt(cpy);
}




void FullTextSearch::SearchByStringInt(char *cpy)
{
  size_t buff1size = strlen(cpy)+1;
  size_t buff2size = strlen(cpy)+1;
  char *buff1=(char *)alloca(buff1size);
  char *buff2=(char *)alloca(buff2size);

  char *p=cpy;
  bool rep;
  do
  {
    while (*p && (!isspace(*p) || *p<0)) p++;
    rep=*p!=0;
    *p=0;
    if (*cpy)
    {
      unsigned long flags=0;
      if (*cpy=='+') 
	  {
		flags |=fMustIncluded;
		cpy++;
		if (rep) SearchByStringInt(p+1);  //unsigned words analyze first
		rep=false;		
	  }
      else if (*cpy=='-')
	  {
		flags |=fNotIcluded;
		cpy++;
		if (rep) SearchByStringInt(p+1);  //unsigned words analyze first
		rep=false;		
	  }
      if (*cpy=='!') {flags |=fNoFuzzy;cpy++;}
	  if (*cpy=='?') {flags |=fNoFuzzy;}
      if (p[-1]=='*') {flags |=fPartialWord;p[-1]=0;}	  
      if (_index.ignoredWords.Find(FullTextIndex::WordCmp(cpy))==NULL) {
          const char *ptr = cpy;
          while (ReadWord(ptr,buff1,buff1size,buff2,buff2size)) {
            Search(buff1,flags);
            if (buff2[0]) Search(buff2,flags);
          }              
      }
    }
    cpy=p+1;      
    p=cpy;
  }
  while (rep);
}