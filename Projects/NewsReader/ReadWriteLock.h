// ReadWriteLock.h: interface for the CReadWriteLock class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_READWRITELOCK_H__89868D45_875D_4BA6_B3A7_ABEA3CB5652E__INCLUDED_)
#define AFX_READWRITELOCK_H__89868D45_875D_4BA6_B3A7_ABEA3CB5652E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <windows.h>


class CRWLockThreadList  
{

    HANDLE *_mutexes;
    DWORD *_counts;
    DWORD *_ids;
    int _size;

public:
	int Resize();
	int FindThreadMutex(DWORD id=0);
	void Reset();
	BOOL AcquireMutex();
	CRWLockThreadList();
	virtual ~CRWLockThreadList();
    HANDLE GetMutex(int pos) {return _mutexes[pos];}
    void AddRefMutex(int pos) {_counts[pos]++;}
    BOOL DecRefMutex(int pos) 
      {
      if (--_counts[pos]<=0)
        {_counts[pos]=0;_ids[pos]=0;return TRUE;}
      else
        return FALSE;
      }
    int GetCount()  {return _size;}
};


class CReadWriteLock  
{
    CRITICAL_SECTION _tunnel;
    HANDLE _writelock;
    CRWLockThreadList _readlock;
    DWORD _wcount;
    DWORD _wid;
    struct ScoopLock
      {
      CRITICAL_SECTION *_sect;
      ScoopLock(CRITICAL_SECTION *p):_sect(p)
        {EnterCriticalSection(_sect);}
      ~ScoopLock()
        {LeaveCriticalSection(_sect);}
      };
public:
	BOOL Unlock();
	CReadWriteLock();
	virtual ~CReadWriteLock();
	BOOL Lock(bool lockWrite, DWORD miliseconds, DWORD pm=QS_SENDMESSAGE, HWND hWnd=NULL, int msglow=0, int msghigh=0);
    

protected:
};

class CRWLockGuard
  {
  CReadWriteLock &_lock; //!<Reference to the locker
  bool _writelock;    //!<This guard guarding writting area
  bool _locked;     //!<Object is currently locked and should be unlocked in destructor
  public:
    CRWLockGuard(CReadWriteLock &lock, bool writelock=false, bool initialy=false);
    ~CRWLockGuard();
    
    BOOL Lock(DWORD timeout=INFINITE);
    BOOL LockPM(DWORD miliseconds=INFINITE, DWORD pm=QS_SENDMESSAGE, HWND hWnd=NULL, int msglow=0, int msghigh=0);
    void Unlock();
    
  };

void	TestMutex();
#endif // !defined(AFX_READWRITELOCK_H__89868D45_875D_4BA6_B3A7_ABEA3CB5652E__INCLUDED_)
