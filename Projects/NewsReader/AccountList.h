// AccountList.h: interface for the CAccountList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ACCOUNTLIST_H__1E21312F_BE79_41F6_84A1_114C252A7BB8__INCLUDED_)
#define AFX_ACCOUNTLIST_H__1E21312F_BE79_41F6_84A1_114C252A7BB8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Archive;

#include "NewsAccount.h"

enum ItemType
  {Itm_Account,Itm_Group,Itm_Folder,Itm_Null};

#define ACCUNDEF (CNewsAccount *)0xFFFFFFFF
class CAccountList
  {  
  CTreeCtrl &tree;
  HTREEITEM itcreate;  
  CNewsAccount *last,*lastex;
  HTREEITEM hlast,hlastex;
  public:
		void AlterGroup(CNewsGroup *grp);
	  void AlterAllGroups(CNewsAccount *acc);
	  bool EnsureLoaded(CNewsAccount *acc);
	  bool EnsureLoaded(HTREEITEM it);
	  HTREEITEM GetAccountItem(HTREEITEM it);
	  HTREEITEM FindItem(HTREEITEM from, DWORD value);
	  bool astree;
	  CNewsAccount * GetAccountEx(HTREEITEM it);
	  HTREEITEM EnumAccounts(HTREEITEM last);
	  void BoldGroup(HTREEITEM it, bool boldstatus);
	  void AlterGroup(HTREEITEM it);
	  ItemType GetItemType(HTREEITEM it);
	  HTREEITEM FindAccount(const char *name);
	  void SerializeList(bool save);
	  void SerializeAccount(HTREEITEM it,bool save);
	  bool DeleteGroup(HTREEITEM it);
	  CNewsGroup * GetGroup(HTREEITEM it);
	  bool AddGroup(HTREEITEM account,CNewsGroup *grp,bool bold=true);
	  void InitList();
	  void DeleteAll();
	  void AlterAccount(HTREEITEM it,bool delfull=false);
	  CNewsAccount * GetAccount(HTREEITEM it);
	  void DeleteAccount(HTREEITEM it);
	  CTreeCtrl &GetTree() {return tree;}
	void InsertAccount(CNewsAccount *acc, bool select=true);
	CAccountList(CTreeCtrl &c):tree(c) {itcreate=NULL;astree=true;last=lastex=ACCUNDEF;};
	virtual ~CAccountList();
	CTreeCtrl *operator->() {return &tree;}

    bool AccountStreaming(HTREEITEM it, bool save);

  protected:
    void ListState(HTREEITEM it,std::iostream &s, bool save, bool one=true);
    void ListStateStreaming(HTREEITEM it, Archive &arch, bool one=true);
  };

bool ExportAccount(const char *srcfile, const char *targetfile);
bool ImportAccount(const char *srcfile, const char *targetfile);


#endif // !defined(AFX_ACCOUNTLIST_H__1E21312F_BE79_41F6_84A1_114C252A7BB8__INCLUDED_)
