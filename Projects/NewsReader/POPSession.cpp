// POPSession.cpp : implementation file
//

#include "stdafx.h"
#include "Remote Mailbox.h"
#include "POPSession.h"
#include <strstrea.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPOPSession

CPOPSession::CPOPSession():timeout(0,0,0,30)
  { 
  read=0;
  buffer[0]=0;  
  status=NULL;
  debug=false;
  }

CPOPSession::~CPOPSession()
  {

  }


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CPOPSession, CSocket)
	//{{AFX_MSG_MAP(CPOPSession)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CPOPSession member functions

BOOL CPOPSession::OnMessagePending() 
  {
	CTime tim;
	tim=tim.GetCurrentTime();
	if (tim>laststamp+timeout) 
	  CancelBlockingCall();
	MSG msg;
	if (::PeekMessage(&msg,NULL,WM_NCMOUSEMOVE, WM_NCMBUTTONDBLCLK,PM_REMOVE))
	  {
	  ::DispatchMessage(&msg);
	  }
	else 	if (::PeekMessage(&msg,NULL,WM_PAINT, WM_PAINT,PM_REMOVE))
	  {
	  ::DispatchMessage(&msg);
	  }
	else 	if (::PeekMessage(&msg,NULL,WM_SYSCOMMAND, WM_SYSCOMMAND,PM_REMOVE))
	  {
	  ::DispatchMessage(&msg);
	  }
	else if (::PeekMessage(&msg,NULL,WM_KEYDOWN,WM_KEYDOWN,PM_REMOVE))
	  {
	  if (msg.wParam==VK_ESCAPE) 
		throw CPOPException(CPOP_OPERATIONCANCELED);
	  }
	DWORD p=GetTickCount()/200;
	SetCursor(theApp.mailwaita[p & 7]);
  return FALSE;
  }

void CPOPSession::ReadLine(CString &line)
  {
  char *entr;
  line="";
  laststamp=laststamp.GetCurrentTime();
  do
	{
	if (buffer[0]==13) 
	  strcpy(buffer,buffer+1);
	entr=strchr(buffer,'\n');
	if (entr==NULL)
	  {
	  line+=buffer;
	  read=Receive(buffer,sizeof(buffer)-1);
	  if (read<1)
		{
		read=0;
		throw CPOPException(CPOP_IOERROR);
		}
  	  buffer[read]=0;
	  if (buffer[read-1]==13)
		buffer[read-1]=0;
	  }
	else
	  {
	  *entr=0;
	  if (entr>buffer && entr[-1]==0x0D) entr[-1]=0;
	  line+=buffer;
	  strcpy(buffer,entr+1);
	  }
	}
  while (entr==NULL);
  if (debug && status)
	status->SendMessage(WM_APP,2,(LPARAM)((LPCTSTR)line));
  }

void CPOPSession::WriteLine(const char *line)
  {  
  if (debug && status)
	 status->SendMessage(WM_APP,3,(LPARAM)line);
  laststamp=laststamp.GetCurrentTime();
  int len=strlen(line);
  if (len)
	{
	int ss=Send((void *)line,len);
	if (ss<1) throw CPOPException(CPOP_IOERROR);
	while (ss!=len)
	  {
	  line=(const char *)((char *)line+ss);
	  len-=ss;
	  ss=Send(line,len);
	  if (ss<1) throw CPOPException(CPOP_IOERROR);
	  }
	}
  char *l="\r";
  while (Send((void *)l,1)!=1);
  l="\n";
  while (Send((void *)l,1)!=1);
  }

void CPOPSession::WriteFormattedV(const char *format, va_list list)
  {
  CString f;
  f.FormatV(format,list);
  WriteLine(f);
  }

void CPOPSession::WriteFormatted(const char *format, ...)
  {
  va_list list;
  va_start(list,format);
  WriteFormattedV(format,list);
  }

bool CPOPSession::ReceivePopReply(CString &comment)
  {
  ReadLine(comment);
  if (comment=="") throw CPOPException(CPOP_UNEXCEPTEDREPLY);
  char p=comment[0];
  if (p=='+') comment.Delete(0,__min(4,comment.GetLength()));
  else if (p!='-') throw CPOPException(CPOP_UNEXCEPTEDREPLY);
  return p=='+';
  }


void CPOPSession::ConnectPop(const char *username, const char *password, const char *popserver, int port)
  {
  CString line;
  Close();
  if (debug && status) status->SendMessage(WM_APP,4,0);
  Create();
  laststamp=laststamp.GetCurrentTime();
  if (Connect(popserver,port)==FALSE)
	throw CPOPException (CPOP_UNABLETOCONNECT,popserver);
  if (ReceivePopReply(line)==false) throw CPOPException(CPOP_POPERROR,line);
  WriteFormatted("USER %s",username);
  if (ReceivePopReply(line)==false) throw CPOPException(CPOP_USERUNKNOWN,line);
  WriteFormatted("PASS %s",password);
  if (ReceivePopReply(line)==false) throw CPOPException(CPOP_ACCESSDENIED,line);
  }


static char *GetPOPErrorText(int error)
  {
  switch (error)
	{
	case CPOP_IOERROR:return "IO Error: Connection has been reset by peer.";
	case CPOP_UNABLETOCONNECT: return "Unable to connect to your mailbox. Check POP3 server address.";
	case CPOP_POPERROR: return "POP3 Server reports an unexcepted error.";
	case CPOP_SMTPERROR: return "SMTP Server reports an unexcepted error. Message couldn't be sent.";
	case CPOP_UNEXCEPTEDREPLY: return "Server returns an unexcepted reply.";
	case CPOP_USERUNKNOWN: return "User was not found at this server.";
	case CPOP_ACCESSDENIED: return "Access denied. Check your username or password.";
	case CPOP_UNABLETOCONNECTSMTP: return "Unable to connect to SMTP. Check SMTP server address.";
	case CPOP_REJECTED: return "SMTP Reject your request.";
	case CPOP_OPERATIONCANCELED: return "Operation canceled (ESC pressed)";
	default: return "Internal error reported. Operation was canceled.";
	}
  }

const char * CPOPException::GetErrorText()
  {
  static char *buf=NULL;
  if (extended==NULL) return GetPOPErrorText(error);
  delete buf;
  const char *err=GetPOPErrorText(error);
  buf=new char[strlen(err)+strlen(extended)+10];
  sprintf(buf,"%s : %s",err,extended);
  return buf;
  }

void CPOPSession::ConnectSMTP(const char *smtpname, int port, const char *myname)
  {
  CString line;
  Close();
  Create();
  if (debug && status) status->SendMessage(WM_APP,4,0);
  laststamp=laststamp.GetCurrentTime();
  if (myname==NULL) myname="remote.mailbox";
  if (Connect(smtpname,port)==FALSE)  throw CPOPException(CPOP_SMTPERROR,smtpname);
  if (GetSMTPReply(line)!=220) throw CPOPException(CPOP_SMTPERROR,line);
  WriteFormatted("HELO %s",myname);
  if (GetSMTPReply(line)!=250) throw CPOPException(CPOP_SMTPERROR,line);
  }

int CPOPSession::DecodeFirstValue(const char *line, int def)
  {
  sscanf(line,"%d",&def);
  return def;
  }

int CPOPSession::GetSMTPReply(CString &comment)
  {
  ReadLine(comment);
  int id;
  if (comment=="") throw CPOPException(CPOP_UNEXCEPTEDREPLY);
  if (sscanf(comment,"%d",&id)!=1)
	throw CPOPException(CPOP_UNEXCEPTEDREPLY);
  char *c=strchr(comment,32);
  if (c==NULL) 
	{
	comment="";
	}
  else
	{
	c++;
	comment=c;
	}
  return id;
  }

int CPOPSession::GetMailboxStat()
  {
  CString comment;
  WriteLine("STAT");
  if (ReceivePopReply(comment)==false) throw CPOPException(CPOP_POPERROR,comment);
  return DecodeFirstValue(comment);
  }

static bool TranslateLine(CString& s)
  {
  bool newline=false;
  char *p=s.LockBuffer();
  char *t=p;
  while (*p)
	{
	if (*p=='=')
	  {
	  int chr;
	  p++;
	  char buff[3];
	  newline=false;
	  if (*p==0) break;
	  buff[0]=p[0];
	  buff[1]=p[1];
	  buff[2]=0;
	  sscanf(buff,"%X",&chr);
	  *t++=chr;
	  p+=2;
	  }
	else
	  {
	  *t++=*p++;
	  }
	newline=true;
	}
  *t++=0;
  return newline;
  }

const char *GetProperty(const char *srcline,const char *prop)
  {
  char *line=(char *)srcline;
  while (*prop)
	{
	if (tolower(*line)!=tolower(*prop)) return NULL;
	line++;
	prop++;
	}
  while (*line<33 && *line!=0 && *line!='\n') line++;
  return line;
  }
void CPOPSession::ReciveMessage(int msg, CString& from, CString& subject, CString &text, CString& header, CString& to, CString& date)
  {
  ostrstream head,tail;
  head.rdbuf()->setbuf(NULL,2*1024);  //prepare buffers
  tail.rdbuf()->setbuf(NULL,256*1024);

  try
	{
	CString line;
	int linec=0;;
	WriteFormatted("RETR %d",msg);
	if (ReceivePopReply(line)==false) throw CPOPException(CPOP_POPERROR,line);
	header="";
	text="";
	from="";
	subject="";
	to="";
	date="";
	if (status) status->SendMessage(WM_APP,0,linec++);
	ReadLine(line);

	while (line!="." && line!="" && line!=" ")
	  {
	  const char *prop=NULL;
	  prop=GetProperty(line,"from:");
	  if (prop) from=prop;
	  else
		{
		prop=GetProperty(line,"subject:");  
		if (prop) subject=prop;
		else 
		  {
		  prop=GetProperty(line,"to:");
		  if (prop) to=prop;
		  else 
			{
			prop=GetProperty(line,"date:");
			if (prop) date=prop;
			}
		  }
		}
	  head<<line<<"\r\n";
	  if (status) status->SendMessage(WM_APP,0,linec++);
	  ReadLine(line);    
	  }
	CString p;
	while (line!=".")
	  {
	  tail << ((LPCTSTR)line);
	  tail << "\r\n";
	  if (status) status->SendMessage(WM_APP,0,linec++);
	  ReadLine(line);    
	  }
	if (status) status->SendMessage(WM_APP,1,0);
	head.put((char )0);
	tail.put((char )0);
	header=head.str();
	text=tail.str();
	}
  catch(CPOPException e)
	{
	head.put((char )0);
	tail.put((char )0);
	header=head.str();
	text=tail.str();
	free(head.rdbuf()->str());
	free(tail.rdbuf()->str());
	throw e;
	}
  free(head.rdbuf()->str());
  free(tail.rdbuf()->str());
  }

void CPOPSession::ReadMessageIDs(long *list, int count)
  {
  CString line;
  WriteLine("LIST");
  if (ReceivePopReply(line)==false) throw CPOPException(CPOP_POPERROR,line);
  memset(list,0,sizeof(long)*count);
  for (int i=0;i<count;i++)
	{
	ReadLine(line);
	int p;
	if (line!="" && line[0]=='.') return;	
	sscanf(line,"%d%d",&p,list+i);
	}  
  ReadLine(line);
  }

void CPOPSession::ReadMailMessage(int mailslot, CMailMessage &msg)
  {
  ostrstream head,tail;
  head.rdbuf()->setbuf(NULL,2*1024);  //prepare buffers
  tail.rdbuf()->setbuf(NULL,256*1024);  

  try
	{
	CString line;
	int linec=0;;
	WriteFormatted("RETR %d",mailslot);
	if (ReceivePopReply(line)==false) throw CPOPException(CPOP_POPERROR,line);

	if (status) status->SendMessage(WM_APP,0,linec++);
	ReadLine(line);

	while (line!="." && line!="" && line!=" ")
	  {
	  head<<line<<"\r\n";
	  if (status) status->SendMessage(WM_APP,0,linec++);
	  ReadLine(line);
	  }
	while (line!=".")
	  {
	  tail << ((LPCTSTR)line);
	  tail << "\r\n";
	  if (status) status->SendMessage(WM_APP,0,linec++);
	  ReadLine(line);    
	  }
	if (status) status->SendMessage(WM_APP,1,0);	
	}
  catch (CPOPException e)
	{
	head.put((char)0);tail.put((char)0);
	msg.SetText(tail.str());msg.SetHeader(head.str());
	free(head.str());free(tail.str());
	throw e;
	}
  head.put((char)0);tail.put((char)0);
  msg.SetText(tail.str());msg.SetHeader(head.str());
  free(head.str());free(tail.str());
  }

void CPOPSession::MailFrom(const char *from)
  {
  CString line;
  WriteFormatted("MAIL FROM:%s",from);
  if (GetSMTPReply(line)!=250) throw CPOPException(CPOP_SMTPERROR,line);  
  }

void CPOPSession::MailTo(const char *to)
  {
  CString line;
  WriteFormatted("RCPT TO:%s",to);
  if (GetSMTPReply(line)!=250) throw CPOPException(CPOP_SMTPERROR,line);  
  }

void CPOPSession::OpenSendMail()
  {
  CString line;
  WriteLine("DATA");
  if (GetSMTPReply(line)!=354) throw CPOPException(CPOP_SMTPERROR,line);  
  }

void CPOPSession::SendLine(const char *line)
  {
  if (strcmp(line,".")==0)
	{
	WriteLine(" .");
	}
  else
	WriteLine(line);
  }

void CPOPSession::CloseSendMail()
  {
  CString line;
  WriteLine(".");
  if (GetSMTPReply(line)!=250) throw CPOPException(CPOP_SMTPERROR,line);  
  }
	

void CPOPSession::CloseConnection()
  {
  buffer[0]=0;
  Close();
  }

bool CPOPSession::IsConnected()
  {
  if (IsBlocking()) return true;
  SOCKADDR_IN addr;
  int addrlen=sizeof(addr);
  return GetSockName((SOCKADDR *)&addr,&addrlen)!=0;
  }

void CPOPSession::ConnectByEmail(CString &email,CString& login, CString &password, CString &pop3, int port)
  {
  char *p=strrchr(email,'@');
  if (p==NULL)
	{
	ConnectPop(login,password,pop3,port);
	}
  else
	{
	CString probe(p+1);	
	try
	  {
	  ConnectPop(login,password,probe,port);
	  goto ok;
	  }
	catch (CPOPException e) {if (e.GetError()==CPOP_OPERATIONCANCELED) throw e;}	
	probe="pop3."+probe;
	try
	  {
	  ConnectPop(login,password,probe,port);
	  goto ok;
	  }
	catch (CPOPException e) {if (e.GetError()==CPOP_OPERATIONCANCELED) throw e;}
	probe=pop3;
    ConnectPop(login,password,probe,port);
ok:	
	pop3=probe;
	}
  }

void CPOPSession::UndoMailboxChanges()
  {
  CString line;
  WriteLine("RSET");
  if (ReceivePopReply(line)==false) throw CPOPException(CPOP_POPERROR,line);
  }

void CPOPSession::QuitMailbox()
  {
  WriteLine("QUIT");
  CloseConnection();
  }

void CPOPSession::DeleteMessage(int index)
  {
  CString line;
  WriteFormatted("DELE %d",index);
  if (ReceivePopReply(line)==false) throw CPOPException(CPOP_POPERROR,line);
  }

void CPOPSession::TranslateText(const char *src, CString &trg, int maxchars)
  {
  istrstream in((char *)(LPCTSTR)src);
  ostrstream out;
  if (maxchars<1) out.rdbuf()->setbuf(NULL,strlen(src));
  else out.rdbuf()->setbuf(NULL,maxchars+10);
  int i=in.get();
  while (i!=EOF && maxchars--)
	{
	if (i=='=')
	  {
	  char buf[3];
	  buf[0]=in.get();
	  buf[1]=in.get();
	  buf[2]=0;
	  if (strcmp(buf,"\r\n")==0) {}
	  else
		{
		int c;
		int p=sscanf(buf,"%X",&c);
		if (p==1) 
		  {
		  out.put((char)c);
		  maxchars--;
		  }
		else 
		  {
		  trg=src;
		  return;
		  }
		}
	  }
	else
	  {
	  out.put((char)i);maxchars--;
	  }
	i=in.get();
	}
  out.put((char)0);
  trg=out.str();
  free(out.str());
  }


