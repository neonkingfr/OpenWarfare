#if !defined(AFX_SPLASHWND_H__8E263800_8253_4925_83FF_4EBAC39B2EC0__INCLUDED_)
#define AFX_SPLASHWND_H__8E263800_8253_4925_83FF_4EBAC39B2EC0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SplashWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSplashWnd window

class CSplashWnd : public CWnd
{
// Construction
  protected:
CBitmap pic;
CImageListEx imglst;
int cntr;
int tmr;
int animx;
public:
	CSplashWnd();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSplashWnd)
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
public:
	void OnDraw(CDC& dc);
	void Create(CWnd *parent=NULL);
	void Close();
	virtual ~CSplashWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSplashWnd)
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CAboutWindow window

class CAboutWindow : public CSplashWnd
{
// Construction
public:
	CAboutWindow();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutWindow)
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	void Create(CWnd *owner);
	virtual ~CAboutWindow();	

	// Generated message map functions
protected:
	//{{AFX_MSG(CAboutWindow)
	afx_msg void OnOK();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CSplashOut window

class CSplashOut : public CWnd
{
CImageListEx imglst;
int pos;
// Construction
public:
	CSplashOut();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSplashOut)
	//}}AFX_VIRTUAL

// Implementation
public:
	void Create();
	virtual ~CSplashOut();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSplashOut)
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPLASHWND_H__8E263800_8253_4925_83FF_4EBAC39B2EC0__INCLUDED_)
