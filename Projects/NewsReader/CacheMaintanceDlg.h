#if !defined(AFX_CACHEMAINTANCEDLG_H__F9212CB3_36D2_452B_91B1_CC87C52018F8__INCLUDED_)
#define AFX_CACHEMAINTANCEDLG_H__F9212CB3_36D2_452B_91B1_CC87C52018F8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CacheMaintanceDlg.h : header file
//

#include "AccountList.h"

/////////////////////////////////////////////////////////////////////////////
// CCacheMaintanceDlg dialog

class CCacheMaintanceDlg : public CDialog
{
// Construction
public:
	CCacheMaintanceDlg(CWnd* pParent = NULL);   // standard constructor
	HTREEITEM it;
	CNewsAccount *acc;
	CAccountList *acclist;
	DWORD halfmax;
	DWORD halfcur;
	DWORD cur;
	DWORD max;
// Dialog Data
	//{{AFX_DATA(CCacheMaintanceDlg)
	enum { IDD = IDD_CACHEMAINTANCE };
	CProgressCtrl	progress;
	CEdit	m_wForceSize;
	CStatic	m_targetsize;
	CStatic	m_maxsize;
	CStatic	m_cursize;
	int		mode;
	UINT	m_forcesize;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCacheMaintanceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void UpdateCacheStatistcs();

	// Generated message map functions
	//{{AFX_MSG(CCacheMaintanceDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCompactClick();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CACHEMAINTANCEDLG_H__F9212CB3_36D2_452B_91B1_CC87C52018F8__INCLUDED_)
