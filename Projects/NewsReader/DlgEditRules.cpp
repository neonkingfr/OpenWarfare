// DlgEditRules.cpp : implementation file
//

#include "stdafx.h"
#include "NewsReader.h"
#include "DlgEditRules.h"
#include ".\dlgeditrules.h"
#include "CondCalc.h"


// DlgEditRules dialog

IMPLEMENT_DYNAMIC(DlgEditRules, CDialog)
DlgEditRules::DlgEditRules(const ThreadRules &rules,CWnd* pParent /*=NULL*/)
	: CDialog(DlgEditRules::IDD, pParent),
  _rules(rules)
{
}

DlgEditRules::~DlgEditRules()
{
}

void DlgEditRules::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_RULELIST, wRuleList);
  DDX_Control(pDX, IDC_CONDITION, wRule);
  DDX_Control(pDX, IDC_DELETE, wDelete);
}


BEGIN_MESSAGE_MAP(DlgEditRules, CDialog)
  ON_NOTIFY(LVN_GETDISPINFO, IDC_RULELIST, OnLvnGetdispinfoRulelist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_RULELIST, OnLvnItemchangedRulelist)
  ON_EN_CHANGE(IDC_CONDITION, OnEnChangeCondition)
  ON_NOTIFY(LVN_SETDISPINFO, IDC_RULELIST, OnLvnSetdispinfoRulelist)
  ON_NOTIFY(LVN_ENDLABELEDIT, IDC_RULELIST, OnLvnEndlabeleditRulelist)
  ON_BN_CLICKED(IDC_DELETE, OnBnClickedDelete)
  ON_BN_CLICKED(IDC_NEWRULE, OnBnClickedNewrule)
  ON_NOTIFY(LVN_BEGINDRAG, IDC_RULELIST, OnLvnBegindragRulelist)
  ON_WM_MOUSEMOVE()
  ON_WM_LBUTTONUP()
//  ON_NOTIFY(NM_KILLFOCUS, IDC_RULELIST, OnNMKillfocusRulelist)
  ON_EN_KILLFOCUS(IDC_CONDITION, OnEnKillfocusCondition)
END_MESSAGE_MAP()


// DlgEditRules message handlers

void DlgEditRules::OnLvnGetdispinfoRulelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  // TODO: Add your control notification handler code here
  *pResult = 0;

  if (pDispInfo->item.mask & LVIF_IMAGE)
  {
    pDispInfo->item.iImage=0;
  }
  if (pDispInfo->item.mask & LVIF_TEXT)
  {
    strncpy(pDispInfo->item.pszText,_rules.GetRule(pDispInfo->item.iItem).GetName(),pDispInfo->item.cchTextMax);
  }
}

BOOL DlgEditRules::OnInitDialog()
{
  CDialog::OnInitDialog();

  ilist.Create(IDB_RULEBITMAP,32,0,RGB(255,0,255));
  wRuleList.SetImageList(&ilist,LVSIL_NORMAL);
  wRuleList.SetItemCount(_rules.GetRuleIdMax()-_rules.GetRuleIdMin()+1);
  wRule.EnableWindow(FALSE);
  wDelete.EnableWindow(FALSE);
  _saveItem=-1;

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgEditRules::OnLvnItemchangedRulelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
  *pResult = 0;
  int cnt=wRuleList.GetSelectedCount();

  if (cnt==1 && pNMLV->uNewState & LVIS_SELECTED)
  {
    wRule.SetWindowText(_rules.GetRule(pNMLV->iItem).GetCond());
    wRule.EnableWindow(TRUE);
    _saveItem=pNMLV->iItem;    
  }
  else
  {
    wRule.EnableWindow(FALSE);
    _saveItem=-1;
  }

  wDelete.EnableWindow(cnt!=0);
}

void DlgEditRules::OnEnChangeCondition()
{
/*  int sel=wRuleList.GetNextItem(-1,LVIS_SELECTED);
  if (sel>=0)
  {
    CString str;
    wRule.GetWindowText(str);
    _rules.SetRule(sel,ThreadRule(_rules.GetRule(sel).GetName(),str));
  }*/
}

void DlgEditRules::OnLvnSetdispinfoRulelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  // TODO: Add your control notification handler code here
  *pResult = 0;

  if (pDispInfo->item.mask & LVIF_TEXT)
  {
    _rules.SetRule(pDispInfo->item.iItem,ThreadRule(pDispInfo->item.pszText,_rules.GetRule(pDispInfo->item.iItem).GetCond()));
  }
}

void DlgEditRules::OnLvnEndlabeleditRulelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  // TODO: Add your control notification handler code here  
  *pResult = pDispInfo->item.pszText!=0 && pDispInfo->item.pszText[0]!=0;
}

void DlgEditRules::OnBnClickedDelete()
{
  POSITION pos=wRuleList.GetFirstSelectedItemPosition();
    int offset=0;
    while (pos)
    {
      int itm=wRuleList.GetNextSelectedItem(pos);
      itm-=offset;
      _rules.RemoveRule(itm);
      offset++;
    }
  int cnt=_rules.GetRuleIdMax()-_rules.GetRuleIdMin()+1;
  wRuleList.SetItemCount(cnt);
  wRuleList.RedrawItems(0,cnt-1);
  wDelete.EnableWindow(FALSE);
}

void DlgEditRules::OnBnClickedNewrule()
{
  _rules.AddRule(ThreadRule(CString(MAKEINTRESOURCE(IDS_NEWRULE)),CString()));
  int cnt=_rules.GetRuleIdMax()-_rules.GetRuleIdMin()+1;
  wRuleList.SetItemCount(cnt);
  wRuleList.RedrawItems(0,cnt-1);
  wRuleList.UpdateWindow();
  wRuleList.SetFocus();
  wRuleList.SetItemState(cnt-1,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);
  wRuleList.EditLabel(cnt-1);
  
}

void DlgEditRules::OnLvnBegindragRulelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
  // TODO: Add your control notification handler code here
  *pResult = 0;


  int itm=wRuleList.GetNextItem(-1,LVIS_SELECTED);
  if (itm>=0)
  {
    CPoint pt;
    _dragImage=wRuleList.CreateDragImage(itm,&pt);
    SetCapture();
    _dragImage->BeginDrag(0,CPoint(16,16));
    pt=GetMessagePos();
    wRuleList.ScreenToClient(&pt);
    _dragImage->DragEnter(&wRuleList,pt);
  }

}

void DlgEditRules::OnMouseMove(UINT nFlags, CPoint point)
{
  if (GetCapture()==this)
  {
    MapWindowPoints(&wRuleList,&point,1);
    _dragImage->DragLeave(&wRuleList);
    LVHITTESTINFO hittest;
    hittest.pt=point;
    int item=wRuleList.SubItemHitTest(&hittest);
    wRuleList.SetHotItem(item);
    _dragImage->DragEnter(&wRuleList,point);
  }

  CDialog::OnMouseMove(nFlags, point);
}

void DlgEditRules::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (GetCapture()==this)
  {
    ReleaseCapture();
    _dragImage->EndDrag();
    _dragImage=0;
    int itm=wRuleList.GetHotItem();
    if (itm>=0)
    {
    

    ThreadRules rls;    
    POSITION pos=wRuleList.GetFirstSelectedItemPosition();
    while (pos)
    {
      int i=wRuleList.GetNextSelectedItem(pos);
      rls.AddRule(_rules.GetRule(i));
    }
    OnBnClickedDelete();
    if (itm>_rules.GetRuleIdMax()) itm=_rules.GetRuleIdMax()+1;
    for (int i=rls.GetRuleIdMin();i<=rls.GetRuleIdMax();i++)
    {
      _rules.InsertRule(itm,rls.GetRule(i));
      itm++;
    }
    int cnt=_rules.GetRuleIdMax()-_rules.GetRuleIdMin()+1;
    wRuleList.SetItemCount(cnt);
    wRuleList.RedrawItems(0,cnt-1);
    wRuleList.UpdateWindow();


    wRuleList.SetHotItem(-1);
    }


  }

  CDialog::OnLButtonUp(nFlags, point);
}


void DlgEditRules::OnEnKillfocusCondition()
{
  if (_saveItem>=0)
  {
    CString str;
    wRule.GetWindowText(str);
    CCondCalc eval;
    if (eval.Compile(str)==false)
    {
      std::ostrstream str; eval.Print(str); str<<'\0';
      CString z;
      AfxFormatString1(z,IDS_COMPILEERROR,str.str());
      str.rdbuf()->freeze(0);
      NrMessageBox(z,MB_OK);      
    }
    _rules.SetRule(_saveItem,ThreadRule(_rules.GetRule(_saveItem).GetName(),str));
  }
}

