#if !defined(AFX_LASTREADDLG_H__16E5A26C_5447_4912_AF2B_BAB7282B818B__INCLUDED_)
#define AFX_LASTREADDLG_H__16E5A26C_5447_4912_AF2B_BAB7282B818B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LastReadDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLastReadDlg dialog

class CLastReadDlg : public CDialog
{
// Construction
public:
	CLastReadDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLastReadDlg)
	enum { IDD = IDD_LASTREAD };
	COleDateTime	date;
	COleDateTime	time;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLastReadDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLastReadDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LASTREADDLG_H__16E5A26C_5447_4912_AF2B_BAB7282B818B__INCLUDED_)
