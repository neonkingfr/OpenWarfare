// FreezeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "FreezeDlg.h"
#include <tlhelp32.h>
#include <winnt.h>
#define CCALL 
#include <El\Debugging\imexhnd.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFreezeDlg dialog

BOOL watchdogalive;

CFreezeDlg::CFreezeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFreezeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFreezeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CFreezeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFreezeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFreezeDlg, CDialog)
	//{{AFX_MSG_MAP(CFreezeDlg)
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////
// WatchDog thread

typedef HANDLE (APIENTRY *def_OpenThread)(DWORD dwDesiredAccess,BOOL bInheritHandle,DWORD dwThreadId);



static BOOL GenerateReport()
  {
  def_OpenThread myOpenThread;
  HMODULE mod=GetModuleHandle("KERNEL32.DLL");
  if (mod==NULL) return FALSE;
  myOpenThread=(def_OpenThread)GetProcAddress(mod,"OpenThread");
  if (myOpenThread==NULL) return FALSE;
  HANDLE snapshot=CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD,0); //sejmy stavy vsech vlaken;
  GDebugExceptionTrap.LogLine("!!!!! WatchDog detect that application freezes.");
  GDebugExceptionTrap.LogLine("Dumping states of all threads that currently running.");
  GDebugExceptionTrap.LogLine("");
  THREADENTRY32 threadinfo;
  threadinfo.dwSize=sizeof(threadinfo);
  BOOL rep;
  for (rep=Thread32First(snapshot,&threadinfo);rep;rep=Thread32Next(snapshot,&threadinfo))
	{ //for all currently running threads
	if (threadinfo.th32OwnerProcessID==::GetCurrentProcessId() && threadinfo.th32ThreadID!=GetCurrentThreadId()) //except current thread
	  {
	  HANDLE thr=myOpenThread(THREAD_ALL_ACCESS,FALSE,threadinfo.th32ThreadID); //open thread handle
	  if (thr==NULL) 
		{
		CloseHandle(snapshot);	  //detect that windows supports thread handles;
	    GDebugExceptionTrap.LogLine("OpenThread function is not supported by this windows version!");
		return FALSE;	//if not, return error
		}
	  DWORD suscnt=::SuspendThread(thr); //suspend all running threads	 
	  char addinfo[256]="";
	  if (threadinfo.th32ThreadID==theApp.m_nThreadID) strcpy(addinfo,"is main GUI thread");
	  else 
		{
		int i;
		for (i=0;i<TOTALTHREADS;i++) 
		  if (theApp.backgrnd[i]!=NULL && theApp.backgrnd[i]->m_nThreadID==threadinfo.th32ThreadID)
			sprintf(addinfo,"is running on backgroun slot nr. %d",i);
		}
	  GDebugExceptionTrap.LogLine("Thread 0x%08X state: %s ref %d %s",threadinfo.th32ThreadID,suscnt?"waiting":"running",suscnt,addinfo);
	  CloseHandle(thr); //we will not need handle at this time
	  }
	}
  //All threads are suspended, so we can create dump
  for (rep=Thread32First(snapshot,&threadinfo);rep;rep=Thread32Next(snapshot,&threadinfo))
	{
	if (threadinfo.th32OwnerProcessID==::GetCurrentProcessId())
	  {
	  HANDLE thr=myOpenThread(THREAD_ALL_ACCESS,FALSE,threadinfo.th32ThreadID); //open thread handle
	  CONTEXT Context;
	  Context.ContextFlags=CONTEXT_FULL;
		if (GetThreadContext(thr,&Context)==FALSE)		
		  GDebugExceptionTrap.LogLine("Unable to get context of thread 0x%08X. Skipping...",threadinfo.th32ThreadID);
		else
		  {		
		  char buff[256];
		  sprintf(buff,"Dump of thread 0x%08X",threadinfo.th32ThreadID);
		  GDebugExceptionTrap.ReportContext(buff,&Context);
		  }
	   CloseHandle(thr);
  	  GDebugExceptionTrap.LogLine("");
	  }
	}
  CloseHandle(snapshot);
  GDebugExceptionTrap.LogLine("###################### END OF THREAD DUMP ####################");  
  return TRUE;
  }


UINT WatchDog(LPVOID param)
  {
  HWND hWnd=(HWND)param;
  //...........

  CFreezeDlg dlg(CWnd::GetDesktopWindow());
  int p;
  do
	{
	do
	  {
	  watchdogalive=FALSE;
	  Sleep(30000);
      if (!IsWindow(hWnd)) 
        return 0;
	  }
	while (watchdogalive);    
	p=dlg.DoModal();
	}
  while (p==IDOK);
  return 0;
  }

/////////////////////////////////////////////////////////////////////////////
// CFreezeDlg message handlers



void CFreezeDlg::OnPaint() 
  {
  CPaintDC dc(this); // device context for painting
  DrawAnimIcon(dc);
	
	// Do not call CDialog::OnPaint() for painting messages
  }

BOOL CFreezeDlg::OnInitDialog() 
{ 
	CDialog::OnInitDialog();
	doganim.Create(IDB_DOGANIM5,50,1,RGB(255,0,255));
	anmpos=0;
	SetTimer(1,200,NULL);
	
	// TODO: Add extra initialization here
	SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFreezeDlg::OnDestroy() 
  {
  CDialog::OnDestroy();
	
  doganim.DeleteImageList();	
  }

void CFreezeDlg::OnTimer(UINT nIDEvent) 
  {
  anmpos++;
  if (anmpos>=4) anmpos=0;
  CDC *dc=GetDC();
  DrawAnimIcon(*dc);
  ReleaseDC(dc);
  CDialog::OnTimer(nIDEvent);
  if (watchdogalive==TRUE) EndDialog(IDOK);
  }

void CFreezeDlg::DrawAnimIcon(CDC &dc)
  {
  CPoint pt(0,0);
  ::MapWindowPoints(::GetDlgItem(*this,IDC_DOGICON),*this,&pt,1);  
  doganim.Draw(&dc,anmpos,pt,ILD_NORMAL);
  }

void CFreezeDlg::OnCancel() 
{
  CString msg;

  SetCursor(::LoadCursor(NULL,IDC_WAIT));
  if (GenerateReport()==FALSE)		
	msg.LoadString(IDS_GENERATEERROR);
  else
	msg.LoadString(IDS_REPORTGENERATED);
  int id=::MessageBox(*this,msg,"WatchDog",MB_RETRYCANCEL);
  if (id==IDRETRY)
	{
	char *buff=(char *)alloca(MAX_PATH*4);
	GetModuleFileName(NULL,buff,MAX_PATH*4);
	ShellExecute(NULL,NULL,buff,NULL,NULL,SW_SHOW);
	}
  ExitProcess(0);
}
