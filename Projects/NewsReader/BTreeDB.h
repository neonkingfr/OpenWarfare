#ifndef ___BTREEDB_BREDY_HEADER__
#define ___BTREEDB_BREDY_HEADER__

#include <es/containers/array.hpp>

/* Operace s BTree s typem T

int GetKeyPart(int bit, int sz) - vraci bitovou cast klice. shift znamena cislo bitu, sz pocet bitu
int GetBitLen() - vraci celkovy pocet bitu klice
bool KeySame(const T &other) - vraci true, jestlize klice jsou shodne
*/

#define BTREEDB_CLUSTER 4
#define BTREEDB_CLUSTER_BIT 2

template<class T, class Allocator=MemAllocD>
class BTreeDB
  {
  AutoArray<T,Allocator> _items;
  AutoArray<int> _structure;
  public:
	BTreeDB();

	T *FindKey(const T &keyinfo)
	  {
	  int index=FindKeyIndex(keyinfo);
	  if (index!=-1) return &_items[index];
	  else return NULL;
	  }
	int FindKeyIndex(const T &keyinfo);
	T *AddKey(const T &keyinfo);
	int GetNKeys() {return _items.Size();}
	T &GetKey(int pos) {return _items[pos];}

	void DeleteKey(const T& keyinfo);
	int EnumItems(const T& partial_key, int partial_bits, int (*KeyEnumerator)(T &item, void *context),void *context);
  private:
	int EnumRecursive(int itr, int (*KeyEnumerator)(T &item, void *context),void *context);
  };

template<class T, class Allocator>
BTreeDB<T,Allocator>::BTreeDB()
  {
  for (int i=0;i<BTREEDB_CLUSTER;i++) _structure.Append()=0;
  }

template<class T, class Allocator>
int BTreeDB<T,Allocator>::FindKeyIndex(const T &keyinfo)
  {
  int itr=0;
  int bit=0;
  int pos=keyinfo.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
  while (itr+pos<_structure.Size() && _structure[itr+pos]>0)	
	{
	itr=_structure[itr+pos];
	bit+=BTREEDB_CLUSTER_BIT;	
	pos=keyinfo.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
	}
  int idx=_structure[itr+pos];
  if (idx==0) return -1;
  T &item=GetKey(-idx-1);
  if (item.KeySame(keyinfo)) return -idx-1;
  else return -1;
  }

template<class T, class Allocator>
T *BTreeDB<T,Allocator>::AddKey(const T &keyinfo)
  {
  int itr=0;
  int bit=0;
  int pos=keyinfo.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
  while (itr+pos<_structure.Size() && _structure[itr+pos]>0)	
	{
	itr=_structure[itr+pos];
	bit+=BTREEDB_CLUSTER_BIT;	
	pos=keyinfo.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
	}
  if (_structure[itr+pos]==0) 
	{
	int sz=_items.Size();
	_items.Append()=keyinfo;
	_structure[itr+pos]=-(sz+1);
	return &_items[sz];
	}
  else 
	{
	int orgpos=_structure[itr+pos];
	T &item=GetKey(-(orgpos+1));
	if (item.KeySame(keyinfo))
	  {
	  item=keyinfo;
	  return &item;
	  }
	int a;
	int b;
	do
	  {
	  _structure[itr+pos]=_structure.Size();
	  itr=_structure[itr+pos];
	  bit+=BTREEDB_CLUSTER_BIT;
	  for (int i=0;i<BTREEDB_CLUSTER;i++) _structure.Append()=0;
	  a=item.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
	  b=keyinfo.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
	  if (a==b)
		if (item.GetBitLen()<bit && keyinfo.GetBitLen()<bit) return NULL;
		else 
		  {
		  pos=a;
		  }
	  else
		{
		_structure[itr+a]=orgpos;
		int sz=_items.Size();
		_items.Append()=keyinfo;
		_structure[itr+b]=-(sz+1);
		return &_items[sz];		
		}
	  }
	while (true);		
	}
  }

template<class T, class Allocator>
void BTreeDB<T,Allocator>::DeleteKey(const T& keyinfo)
  {
  int itr=0;
  int bit=0;
  int pos=keyinfo.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
  while (itr+pos<_structure.Size() && _structure[itr+pos]>0)	
	{
	itr=_structure[itr+pos];
	bit+=BTREEDB_CLUSTER_BIT;	
	pos=keyinfo.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
	}
  int idx=_structure[itr+pos];
  if (idx==0) return;
  idx=-idx;
  _items.Delete(idx-1);
  _structure[itr+pos]=0;
  for (int i=0;i<_structure.Size();i++)	
	if (_structure[i]<-idx) _structure[i]++;
  }

template<class T, class Allocator>
int BTreeDB<T,Allocator>::EnumItems(const T& partial_key, int partial_bits, int (*KeyEnumerator)(T &item, void *context),void *context)
  {
  int itr=0;
  int bit=0;
  int pos=keyinfo.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
  while (bit<partial_bits && itr+pos<_structure.Size() && _structure[itr+pos]>0)	
	{
	itr=_structure[itr+pos];
	bit+=BTREEDB_CLUSTER_BIT;	
	pos=keyinfo.GetKeyPart(bit,BTREEDB_CLUSTER_BIT);
	}
  idx=_structure[itr+i];
  if (idx==0) return 0;
  if (idx<0) return KeyEnumerator(_item[-idx-1],context);
  return EnumRecursive(itr,pos,KeyEnumerator,context);
  }

template<class T, class Allocator>
int BTreeDB<T,Allocator>::EnumRecursive(int itr, int (*KeyEnumerator)(T &item, void *context),void *context)
  {
  int idx;
  int ret=0;
  for (int i=0;i<BTREEDB_CLUSTER && !ret;i++)
	{
	idx=_structure[itr+i];
	if (idx<0)
	  ret=KeyEnumerator(_item[-idx-1],context);
	else
	  ret=EnumRecursive(idx,KeyEnumerator,context);
	}
  return ret;
  }

#endif