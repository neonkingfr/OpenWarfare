// SendDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "SendDlg.h"
#include "MainFrm.h"
#include "NewsAccount.h"
#include "NewsTerminal.h"
#include "MessagePart.h"
#include "WindowPlacement.h"
#include "ScanGroupsThread.h"
#include <multimon.h>
#include <mshtml.h>
//#include <atlbase.h>
using namespace std;

#define MSG_ADDRELATED (WM_APP-1)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define BREAKLINES 80

/////////////////////////////////////////////////////////////////////////////
// CSendDlg dialog


CSendDlg::CSendDlg(CWnd* pParent /*=NULL*/)
: CDialog(CSendDlg::IDD, pParent),defsize(0,0),sendingDlg(this),geturl(this)
  {
  //{{AFX_DATA_INIT(CSendDlg)
  from = _T("");
  msgbody = _T("");
  ref = _T("");
  subject = _T("");
  to = _T("");
  raw = FALSE;
	vEnableHtml = FALSE;
	//}}AFX_DATA_INIT
  ensync=true;
  thDlgPlace=thDlg_Left;
  _searchRelatedThread=NULL;
  }

//--------------------------------------------------

void CSendDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CSendDlg)
	DDX_Control(pDX, IDC_ENABLEHTML, wEnableHtml);
	DDX_Control(pDX, IDC_RELATED, wRelated);
	DDX_Control(pDX, IDC_ATTACHLIST, wAttachList);
	DDX_Control(pDX, IDC_REFERENCE, bReference);
	DDX_Control(pDX, IDC_INSERTURL, bInsUrl);
  DDX_Control(pDX, IDC_SEND, bSend);
  DDX_Control(pDX, IDC_MSGBODY, wnd_msgbody);
  DDX_Control(pDX, IDC_STOPTHREAD, bStopThread);
  DDX_Control(pDX, IDC_INSERTFILE, bInsFile);
  DDX_Control(pDX, IDC_ATTACH, bAttach);
  DDX_Control(pDX, IDC_WIKI, bWiki);
  DDX_Text(pDX, IDC_FROM, from);
  DDX_Text(pDX, IDC_MSGBODY, msgbody);
  DDX_Text(pDX, IDC_REF, ref);
  DDX_Text(pDX, IDC_SUBJECT, subject);
  DDX_Text(pDX, IDC_TO, to);
	DDX_Check(pDX, IDC_ENABLEHTML, vEnableHtml);
	//}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CSendDlg, CDialog)
//{{AFX_MSG_MAP(CSendDlg)
ON_WM_SIZE()
ON_WM_GETMINMAXINFO()
ON_BN_CLICKED(IDC_INSERTFILE, OnInsertfile)
ON_COMMAND(IDC_QUOTE, OnQuote)
ON_BN_CLICKED(IDC_SEND, OnSend)
ON_BN_CLICKED(IDC_ATTACH, OnAttach)
ON_WM_CONTEXTMENU()
ON_COMMAND(ID_ATTACHPOPUP_DELETE, OnAttachpopupDelete)
ON_COMMAND(ID_ATTACHPOPUP_OPEN, OnAttachpopupOpen)
ON_WM_DROPFILES()
ON_BN_CLICKED(IDC_MESSREF, OnMessref)
ON_BN_CLICKED(IDC_STOPTHREAD, OnStopthread)
ON_MESSAGE(WM_NEXTDLGCTL,OnNextDlgCtl)
ON_COMMAND(ID_EDITPOPUP_FONT, OnEditpopupFont)
ON_WM_CTLCOLOR()
ON_COMMAND(ID_EDITPOPUP_COLOR, OnEditpopupColor)
ON_WM_CLOSE()
ON_WM_TIMER()
ON_MESSAGE(WM_OPENDLGMSG,OnOpenSendDlg)
ON_BN_CLICKED(IDC_INSERTURL, OnInserturl)
	ON_WM_MOVE()
ON_MESSAGE(SDM_READJUSTWINDOW, OnReadjustWindow)
	ON_BN_CLICKED(IDC_REFERENCE, OnReplacemsg)
	ON_NOTIFY(NM_DBLCLK, IDC_ATTACHLIST, OnDblclkAttachlist)
	ON_EN_KILLFOCUS(IDC_SUBJECT, OnKillfocusSubject)
	ON_NOTIFY(NM_DBLCLK, IDC_RELATED, OnDblclkRelated)
	ON_NOTIFY(NM_CLICK, IDC_RELATED, OnClickRelated)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_ENABLEHTML, OnEnablehtml)
	ON_MESSAGE(MSG_ADDRELATED,RelatedCallback)
	ON_EN_CHANGE(IDC_MSGBODY, OnChangeMsgbody)
	//}}AFX_MSG_MAP
	
  ON_BN_CLICKED(IDC_WIKI, OnBnClickedWiki)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSendDlg message handlers

#define SECTION "SendDlg"
#define FONT "Font"
#define COLOR "Color"
#define MSGID "NextMessageID"


static void DropFile(CEdit *editBox, const char *filename)
{
  int spaces=0;
  const char *c=filename;
  char *d;
  while (*c) if ((unsigned)(*c++)<33) spaces++;
  char *buff=(char *)alloca(strlen(filename)+2*spaces+11);
  c=filename;
  strcpy(buff,"file:///");
  d=buff+8;
  while (*c)
  {
	if ((unsigned)(*c)<33) 
	{
	  sprintf(d,"%%%02X",*c);
	  d+=3;
	}
	else if (*c==':') *d++='|';
	else if (*c=='\\') *d++='/';
	else
	  *d++=*c;
  c++;
  }
  *d++=' ';
  *d=0;
  editBox->ReplaceSel(buff,TRUE);
}

BOOL CSendDlg::OnInitDialog() 
  {
  LOGFONT *lp;
  UINT bp;
  BOOL q=theApp.GetProfileBinary(SECTION,FONT,(BYTE **)&lp,&bp);
  if (q)
    {
    lgedfont=*lp;
    delete lp;	  
    }
  else
    {
    memset(&lgedfont,0,sizeof(lgedfont));
    lgedfont.lfHeight=-12;
    lgedfont.lfWeight=FW_REGULAR;
    lgedfont.lfCharSet=DEFAULT_CHARSET;
    lgedfont.lfOutPrecision=OUT_DEFAULT_PRECIS;
    lgedfont.lfClipPrecision=CLIP_DEFAULT_PRECIS;
    lgedfont.lfQuality=DEFAULT_QUALITY;
    lgedfont.lfPitchAndFamily=DEFAULT_PITCH|FF_MODERN;
    strcpy(lgedfont.lfFaceName,"Courier");
    }
  edtcolor=theApp.GetProfileInt(SECTION,COLOR,GetSysColor(COLOR_WINDOWTEXT));
  int p=theApp.GetProfileInt(SECTION,MSGID,0);
  msgname.Format("save%04X.msg",p++);
  ExpandFilename(msgname,msgname);
  theApp.WriteProfileInt(SECTION,MSGID,p);
  edtfont.CreateFontIndirect(&lgedfont);
  int pos=subject.Find(THREADPROPMARK);
  if (pos!=-1) 
    {
    subject.Delete(pos,subject.GetLength()-pos);
    subject.TrimRight();
    subject.TrimLeft();
    }
  CDialog::OnInitDialog();

  attachIcon.Create(IDB_ATTACHICON,32,1,RGB(255,0,255));
  wAttachList.SetImageList(&attachIcon,LVSIL_NORMAL);
  
  bSend.SetIcon(theApp.LoadIcon(IDI_SENDICON));		
  bInsFile.SetIcon(theApp.LoadIcon(IDI_INSERTFILEREF));		
  bInsUrl.SetIcon(theApp.LoadIcon(IDI_INSERTURLREF));		
  bAttach.SetIcon(theApp.LoadIcon(IDI_SPONKA));		
  bStopThread.SetIcon(theApp.LoadIcon(IDI_VLASTNOSTI));		
  bReference.SetIcon(theApp.LoadIcon(IDI_REFERENCE));
  wEnableHtml.SetIcon(theApp.LoadIcon(IDI_ENABLEHTML));
  bWiki.SetIcon(theApp.LoadIcon(IDI_WIKI));
  bWiki.SetCheck(wikiLink!="");
  CRect rc;
  CRect parent;
  GetClientRect(&rc);
  defsize=rc.Size();
  GetWindowRect(&parent);
  GetDlgItem(IDC_MSGBODY)->GetWindowRect(&rc);
  rc-=CPoint(parent.left,parent.top);
  minpos.y=rc.top+10;
  GetDlgItem(IDC_FROM)->GetWindowRect(&rc);
  rc-=CPoint(parent.left,parent.top);
  minpos.x=rc.left+20;	
  GetDlgItem(IDC_MSGBODY)->SetFont(&edtfont,FALSE);	
  //	GetDlgItem(IDC_FROM)->SetFont(&theApp.messfont,FALSE);
  //  GetDlgItem(IDC_TO)->SetFont(&theApp.messfont,FALSE);
  //	GetDlgItem(IDC_REF)->SetFont(&theApp.messfont,FALSE);
  //	GetDlgItem(IDC_SUBJECT)->SetFont(&theApp.messfont,FALSE);


   newthrdprop=thrdprop;
  _thrPropDlg.properties=thrdprop;
  _thrPropDlg.Create(_thrPropDlg.IDD,this);

  
  if (msgbody!="" && cancelid=="") PostMessage(WM_COMMAND,IDC_QUOTE);
//  if (cancelid!="") {GetDlgItem(IDC_EDITWARNING)->ShowWindow(SW_SHOW);}
  if (to=="") GetDlgItem(IDC_TO)->SetFocus();
  else 
    {
    CEdit *edit=(CEdit *)GetDlgItem(IDC_SUBJECT);
    edit->SetFocus();
    edit->SetSel(0,-1);
    }    
  SetTimer(1,60000,NULL);
  CWindowPlacement wp;
  wp.Load(SECTION);
  wp.showCmd=showcmd;
  wp.Restore(*this,EWP_both,false);
  ShowWindow(SW_SHOW);
  (int &)thDlgPlace=theApp.GetProfileInt(SECTION,"Place",thDlg_Hide);

  //	SetWindowPos(NULL,parent.left+(rand() & 0x7f),parent.top+(rand() & 0x7f),0,0,SWP_NOSIZE|SWP_NOZORDER);
  PlaceThreadPropertiesDialog();
  wnd_msgbody.SetLimitText(256*1024);
  
  format.cfFormat=CF_TEXT;
  format.dwAspect=1;
  format.lindex=-1; 
  format.ptd=0;
  format.tymed=TYMED_HGLOBAL;
  filedrop.cfFormat=CF_HDROP;
  filedrop.dwAspect=1;
  filedrop.lindex=-1;
  filedrop.ptd=0;
  filedrop.tymed=TYMED_HGLOBAL;
  RegisterDragDrop(*this,this);

  CString related;
  related.LoadString(IDS_RELATEDARTICLES);
  wRelated.ShowWindow(SW_HIDE);
  wRelated.InsertColumn(0,"",LVCFMT_LEFT,200,0);
  wRelated.InsertColumn(1,"",LVCFMT_LEFT,0,1);
  UpdateRelatedWindow();
  if (_winBrush.GetSafeHandle()==NULL)  
	_winBrush.CreateSolidBrush(GetSysColor(COLOR_BTNFACE));  
  return FALSE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

static bool send_line(const char *line, void *context)
  {
  CNewsTerminal *term=(CNewsTerminal *)context;
  if (line[0]=='.') 
    {
    char tecka=line[0];
    term->GetOutput().ExSimple(tecka);
    }
  term->SendLine(line);
  return true;
  }

void CSendDlg::OnCancel()
  {
  SendMessage(WM_CLOSE,0,0);
  }

//--------------------------------------------------

void CSendDlg::PostNcDestroy() 
  {
  
  NrUnregWindow(this);
  CDialog::PostNcDestroy();
  if (bAutoDelete) delete this;
  }

//--------------------------------------------------

void CSendDlg::OnSize(UINT nType, int cx, int cy) 
  {
  CDialog::OnSize(nType, cx, cy);
  if (nType==SIZE_MINIMIZED) return;
  if ((HWND)bSend==NULL) return;
  int rszx=cx-defsize.cx;
  int rszy=cy-defsize.cy;
  /*
  RelMoveWnd(&bSend,rszx,0);
  RelMoveWnd(&bInsFile,rszx,0);
  RelMoveWnd(&bMessRef,rszx,0);
  RelMoveWnd(&bAttach,rszx,0);
  RelMoveWnd(&bStopThread,rszx,0);*/
  RelSizeWnd(GetDlgItem(IDC_FROM),rszx,0);
  RelSizeWnd(GetDlgItem(IDC_TO),rszx,0);
  RelSizeWnd(GetDlgItem(IDC_SUBJECT),rszx,0);
  //	RelSizeWnd(GetDlgItem(IDC_REF),rszx,0);
  RelMoveWnd(GetDlgItem(IDC_ATTACHLIST),rszx,0);
  RelSizeWnd(GetDlgItem(IDC_ATTACHLIST),0,rszy);
  RelSizeWnd(&wRelated,rszx,0);


  int rmax=cx;
  int bmax=cy;
  CRect rc;
  if (wAttachList.IsWindowVisible())
    {
    wAttachList.GetWindowRect(&rc);
    ScreenToClient(&rc);
    rmax=rc.left;
    }  
  if (webEditor.GetSafeHwnd())
  {
    webEditor.GetWindowRect(&rc);
    ScreenToClient(&rc);
    int mid=(cy-rc.top)/2+rc.top;
    webEditor.MoveWindow(rc.left,rc.top,rmax-10,mid-2-rc.top);
    bmax=cy-2;
    rc.top=mid+2;
  }
  else
  {
    GetDlgItem(IDC_MSGBODY)->GetWindowRect(&rc);
    ScreenToClient(&rc);
    bmax=cy-2;
  }
  GetDlgItem(IDC_MSGBODY)->MoveWindow(5,rc.top,rmax-10,bmax-rc.top);

  defsize.cx=cx;
  defsize.cy=cy;
  Invalidate(TRUE);
  // TODO: Add your message handler code here
  PlaceThreadPropertiesDialog();
  if (nType==SIZE_MAXIMIZED) ReadjustWindow();
  UpdateRelatedWindow();
  }

//--------------------------------------------------

void CSendDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
  {
  if ((HWND)bSend!=NULL) lpMMI->ptMinTrackSize=minpos;    
/*if (_thrPropDlg.GetSafeHwnd()!=NULL)
    {
    CRect rc;
    GetWindowRect(&rc);
    HMONITOR mon=MonitorFromRect(&rc,MONITOR_DEFAULTTONEAREST);
    MONITORINFO nfo;
    nfo.cbSize=sizeof(nfo);
    GetMonitorInfo(mon,&nfo);
    lpMMI->ptMaxPosition.x=0;
    lpMMI->ptMaxPosition.y=0;
    lpMMI->ptMaxSize.x=nfo.rcWork.right-nfo.rcWork.left;
    lpMMI->ptMaxSize.y=nfo.rcWork.bottom-nfo.rcWork.top;
    switch (thDlgPlace)
      {
      case thDlg_Left:lpMMI->ptMaxSize.x-=_thrPropDlg.GetDefaultWidth();lpMMI->ptMaxPosition.x+=_thrPropDlg.GetDefaultWidth();break;
      case thDlg_Bottom:lpMMI->ptMaxSize.y-=_thrPropDlg.GetDefaultHeight();break;
      case thDlg_Right:lpMMI->ptMaxSize.x-=_thrPropDlg.GetDefaultWidth();break;
      default:break;
      }
    }*/
  }

//--------------------------------------------------

void CSendDlg::OnInsertfile() 
  {
  CFileDialog dlg(TRUE,NULL,NULL,OFN_ALLOWMULTISELECT|OFN_FILEMUSTEXIST|OFN_NOCHANGEDIR,NULL,this);
  if (dlg.DoModal()==IDCANCEL) return;
  POSITION pos=dlg.GetStartPosition();
  while (pos)
    {
    CString name=dlg.GetNextPathName(pos);
	DropFile(&wnd_msgbody,name);
    }
  }

//--------------------------------------------------

void CSendDlg::SetNewsTarget(CNewsAccount *acc, CNewsGroup *grp)
  {
  to=CString("news:")+acc->GetAddress()+"/"+grp->GetName();
  }

//--------------------------------------------------

void CSendDlg::OnQuote() 
  {
  int cnt,i;
  cnt=wnd_msgbody.GetLineCount();
  for (i=0;i<cnt;i++)
    {
    int s=wnd_msgbody.LineIndex(i);
    int lnln=wnd_msgbody.LineLength(s);
    int z=s+lnln;
    wnd_msgbody.SetSel(s,s);
    wnd_msgbody.ReplaceSel("> ");
    if (lnln<2 || wnd_msgbody.LineLength(s)>2)
      {
      z=s+wnd_msgbody.LineLength(wnd_msgbody.LineIndex(i));
      int q=wnd_msgbody.LineIndex(i+1);
      if (q==z)
        {
        wnd_msgbody.SendMessage(WM_KEYDOWN,VK_END,1);
        wnd_msgbody.SendMessage(WM_KEYUP,VK_END,0);
        wnd_msgbody.SendMessage(WM_CHAR,0xD,1);
        }
      }
    else
      i++;
    cnt=wnd_msgbody.GetLineCount();
    MSG msg;
    if (PeekMessage(&msg,NULL,WM_KEYDOWN,WM_KEYDOWN,PM_REMOVE))
      {
      break;
      }
    }
  wnd_msgbody.SetSel(0,0);
  }

//--------------------------------------------------

/*
class CSendDlgProgress
{
public:
CWnd *parent;
CWinThread *thrd;  
volatile CSendingDlg *dlg;
CSendDlgProgress(CWnd *parent);
~CSendDlgProgress();
operator HWND() {return dlg->m_hWnd;}
};

  static CCriticalSection cclock;
  
    static UINT StopDlgThread(LPVOID ptr)
    {
    CSendDlgProgress *p=(CSendDlgProgress *)ptr;
    CSendingDlg dlg;
    p->dlg=&dlg;
    dlg.Create(IDD_SENDING,CWnd::GetDesktopWindow());
    dlg.CenterWindow(CWnd::FromHandle(p->parent->m_hWnd));
    MSG msg;
    cclock.Lock();
    while (dlg.m_hWnd)
    {
    GetMessage(&msg,0,0,0);
    if (dlg.PreTranslateMessage(&msg)==FALSE) DispatchMessage(&msg);
    cclock.Unlock();
    Sleep(1);
    cclock.Lock();
    }
    cclock.Unlock();
    return 0;
    }
    
      CSendDlgProgress::CSendDlgProgress(CWnd *parent)
      {
      dlg=NULL;
      this->parent=parent;
      thrd=AfxBeginThread(StopDlgThread,(LPVOID)this,THREAD_PRIORITY_LOWEST,16384);
      while (dlg==NULL) Sleep(5);
      }
      CSendDlgProgress::~CSendDlgProgress()
      {
      cclock.Lock();
      ((CSendingDlg *)dlg)->DestroyWindow();
      cclock.Unlock();
      }
*/  
static UINT SentThread(LPVOID object)
  {
  CSendDlg *dlg=(CSendDlg *)object;
/*  if (!AfxSocketInit(NULL)) return 1;
  
#ifndef _AFXDLL	
  AFX_MODULE_THREAD_STATE *state=AfxGetModuleThreadState();
  if (state->m_pmapSocketHandle==NULL)
    {
    state->m_pmapSocketHandle=new CMapPtrToPtr();
    state->m_pmapDeadSockets=new CMapPtrToPtr();
    state->m_plistSocketNotifications=new CPtrList();
    }
#endif
    */
  if (dlg->SendDataInDlg()==0)
    dlg->PostMessage(WM_OPENDLGMSG,2,0);
  return 0;
  }

//--------------------------------------------------

class CSendDlgProgress
  {
  CWnd *notifywnd;
  public:
    CSendDlgProgress(CWnd *not):notifywnd(not) 
      {notifywnd->PostMessage(WM_OPENDLGMSG,1,0);}
    ~CSendDlgProgress()
      {notifywnd->PostMessage(WM_OPENDLGMSG,0,0);}
  };

//--------------------------------------------------

void CSendDlg::OnSend() 
  {
  if (_thrPropDlg.Save(true)==false) return;
  newthrdprop=_thrPropDlg.properties;
  if (UpdateData()==FALSE) return;
  
  if (subject=="" && strncmp(to,"news:",4)==0) 
    {NrMessageBox(IDS_MISSINGSUBJECT,MB_ICONSTOP|MB_OK);return;}
  CWindowPlacement wp;
  wp.Retrieve(*this);
  wp.Store(SECTION);
  if (theApp.offline)
    {
    CString old=msgname;
    char *p=msgname.LockBuffer();
    char *q=strrchr(p,'\\');
    if (q==NULL) q=p;else q++;
    strncpy(q,"send",4);
    msgname.UnlockBuffer();
    if (SaveMessage()==false) return;
    if (old!=msgname) DeleteFile(old);	  
    PostMessage(WM_OPENDLGMSG,2,0);
    }
  else 
    {
    if (SaveMessage()==false) return;
    AfxBeginThread((AFX_THREADPROC)SentThread,this,THREAD_PRIORITY_NORMAL,16384);
    }
  }

//--------------------------------------------------

static void FormatMsgBody(CString &msg)
  {
  int i;
  int j;
  int cnt=msg.GetLength();
  for (i=0,j=0;i<cnt;i++,j++)
    {	
    if (msg[i]==' ' && j>BREAKLINES) 
      {
      msg.Insert(i+1,'\n');
      msg.SetAt(i,'\t');
      cnt++;
      i+=1;
      j=-1;
      }
    else if (msg[i]=='\r')
      {
      msg.Delete(i);
      cnt--;
      i--;
      j--;
      }
    else if (msg[i]=='\n')
      {
      while (i>0 && msg[i-1]=='\t')
        {
        msg.Delete(i-1);
        i--;
        cnt--;
        }
      j=-1;
      }
    }
  }

//--------------------------------------------------

static int CheckDeleted(CNewsTerminal &term, const char *parent)
  {
  int err=220;
  term.SendFormatted("STAT %s",parent);
  const char *line=term.ReadLine();
  if (line==NULL) return 0;
  sscanf(line,"%d",&err);
  err/=10;
  if (err!=22) return -1;
  return 0;
  }

static int CheckReferenced(CNewsTerminal &term, const char *parent, const char *group)
  {
  int err=220,min,mid,max;
  const char *line;
  term.SendFormatted("GROUP %s",group);
  line=term.ReadLine();
  if (line==NULL) return 0;
  sscanf(line,"%d %d %d %d",&err,&min,&mid,&max);
  if (err/10!=21) return 0;  
  term.SendFormatted("XPAT References %d-%d %s",min,max,parent);
  err=term.ReceiveNumReply()/10;
  if (err!=22) return 0;
  int c=0;
  line=term.ReadLine();
  while (line && line[0]!='.') 
    {
    const char *space=strchr(line,' ');
    if (space) 
      {
      space++;
      if (strstr(parent,space)==0) c++;
      }
    line=term.ReadLine();
    }
  if (c) return -2;
  return 0;
  }

int CSendDlg::PostToNews(CNewsAccount *acc, const char *group)
  {
  int err;
  CString line;
  CNewsTerminal term;
  // term.stopwnd=stopwnd;
  err=acc->OpenConnection(term);
  if (err) return term.ShowError(err);  
  if (cancelid!="") 
    {
    int res=CheckReferenced(term,cancelid,group);    
    if (res==-2 && NrMessageBox(IDS_EDITWARNING,MB_YESNO)==IDNO)
      {
      BringWindowToTop();
      return term.ShowError(res);
      }
    CNewsArtHolder *hld=acc->msgidlist.Find(cancelid);
    if (hld==NULL)
      {
      CString msg;
      AfxFormatString1(msg,IDS_MESSAGETOCANCELDOESNTFOUND,cancelid);
      if (NrMessageBox(msg,MB_YESNO)==IDNO) return -1;
      }
    else
      CancelMessage(term,hld,group);
    }
  else if (ref!="")
    {
    int res=CheckDeleted(term,ref);    
    if (res==-1 && NrMessageBox(IDS_MESSAGEWASEDITED,MB_YESNO)==IDNO)
      {
      BringWindowToTop();
      return term.ShowError(res);
      }
    }

  term.SendLine("POST");
  err=term.ReceiveNumReply()/10;
  if (err!=34) return term.ShowError(err);
  if (!raw)
    {
    if (this->thrdprop!=this->newthrdprop) subject=subject+" "+THREADPROPMARK+newthrdprop;
    term.SendFormatted("From: %s",from);
    term.SendFormatted("Newsgroups: %s", group);
    term.SendFormatted("Subject: %s", subject);
    term.SendFormatted("References: %s", ref);
    if (cancelid!="") term.SendFormatted("X-Cancel: %s", cancelid);
    if (strcmp(acc->GetIcon(),"")!=0) term.SendFormatted("Icon: %s", acc->GetIcon());
    if (vEnableHtml) term.SendFormatted("Content-type: text/html; charset=Windows-1250");
      else term.SendFormatted("Content-type: text/plain; charset=Windows-1250");
    if (wikiLink!="") term.SendFormatted("X-Wiki: %s",wikiLink.GetString());
    }
  CString msg;
  wnd_msgbody.GetWindowText(msg);
  FormatMsgBody(msg);
  /* int ln=wnd_msgbody.GetLineCount();
  for (int i=0;i<ln;i++)
  {
  char buffer[256];
  int p=wnd_msgbody.GetLine(i,buffer,sizeof(buffer)-1);
  buffer[p]=0;
  msg+=buffer;
  msg+='\n';
  }*/
  if (PostMess(term,msg)==false) return -1;
  err=term.ReceiveNumReply()/10;
  if (err!=24) return term.ShowError(err);
  term.SendLine("QUIT");
  if (ensync)
    {
    CNewsGroup *grp=acc->GetGroups();
    while (grp) if (strcmp(grp->GetName(),group)==0) break;else grp=grp->GetNext();
    if (grp)
      {
      CScanNewerOlderThread *thr=new CScanNewerOlderThread(acc,grp,grp->delcheck);
      if (theApp.RunBackground(0,thr,0)==FALSE) delete thr; 
      }
    }
  return CNewsTerminal::IsStopped();
  }

//--------------------------------------------------

bool CSendDlg::PostMess(CNewsTerminal &term, const char *text)
  {
  char buffer[256];
  const char *p=strchr(text,'\n');
  char *last=(char *)text;
  if (!raw) term.SendLine("");
  if (vEnableHtml) send_line("<HTML><BODY>",&term);
  while (p!=NULL)
    {
    int size=p-last;
    if (size>255) size=255;
    strncpy(buffer,last,size);
    buffer[size]=0;
//    if (buffer[0]=='.' && buffer[1]==0) buffer[0]=',';
    send_line(buffer,&term);
    last+=size;
    if (*last=='\n') last++;
    p=strchr(last,'\n');
    }
  send_line(last,&term);
  if (vEnableHtml) send_line("</BODY></HTML><!--",&term);
  if (wAttachList.GetItemCount()>0)
    {
    if (PostAttachments(term)==false) return false;
    }
  if (vEnableHtml) send_line("-->",&term);
  term.SendLine(".");
  return !term.IsStopped();
  }

//--------------------------------------------------

int CSendDlg::PostByEmail(const char *text)
  {
  CNewsTerminal term;
  CString line;
  CString eml;
  int err;
  //  term.stopwnd=stopwnd;
  err=term.Connect(theApp.sendopt.server,theApp.sendopt.port);
  if (err) return term.ShowError(err);
  term.SendLine("HELO BIStudio.NewsReader");
  err=term.ReceiveNumReply();
  if (err!=250) return term.ShowError(err);
  GetEmailFromSender(from,eml);
  term.SendFormatted("MAIL FROM: %s",eml);
  err=term.ReceiveNumReply();
  if (err!=250) return term.ShowError(err);
  if (theApp.sendopt.cctome)
    {
    term.SendFormatted("RCPT TO: %s",eml);;
    err=term.ReceiveNumReply();
    if (err!=250) return term.ShowError(err);
    }
  GetEmailFromSender(to,eml);
  term.SendFormatted("RCPT TO: %s",eml);
  err=term.ReceiveNumReply();
  if (err!=250) return term.ShowError(err);
  term.SendLine("DATA");
  err=term.ReceiveNumReply();
  if (err!=354) return term.ShowError(err);
  term.SendFormatted("From: %s",from);
  term.SendFormatted("To: %s",to);
  term.SendFormatted("Subject: %s",subject);
  term.SendFormatted("References: %s",ref);
  if (vEnableHtml) term.SendFormatted("Content-type: text/html; charset=Windows-1250");
//      else term.SendFormatted("Content-type: text/plain; charset=Windows-1250");

  if (PostMess(term,text)==false) return -1;
  err=term.ReceiveNumReply();
  if (err!=250) return term.ShowError(err);
  term.Quit();
  return term.IsStopped();
  }

//--------------------------------------------------

void CSendDlg::OnAttach() 
  {
  CFileDialog dlg(TRUE,NULL,NULL,OFN_ALLOWMULTISELECT|OFN_FILEMUSTEXIST|OFN_NOCHANGEDIR,NULL,this);
  if (dlg.DoModal()==IDCANCEL) return;
  POSITION pos=dlg.GetStartPosition();
  while (pos)
    AddToAttachList(dlg.GetNextPathName(pos));
  }

//--------------------------------------------------

void CSendDlg::OnContextMenu(CWnd* pWnd, CPoint point) 
  {
  if (pWnd==&wAttachList)
    {
    CMenu mnu;
    CMenu *sub;
    mnu.LoadMenu(IDR_POPUPS);
    sub=mnu.GetSubMenu(1);
    if (wAttachList.GetSelectedCount()<0)
      {
      sub->EnableMenuItem(ID_ATTACHPOPUP_OPEN,MF_GRAYED);
      sub->EnableMenuItem(ID_ATTACHPOPUP_DELETE,MF_GRAYED);
      }
    sub->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON|TPM_LEFTBUTTON,point.x,point.y,this);
    }
  }

//--------------------------------------------------

void CSendDlg::OnAttachpopupDelete() 
  {
  int p;
  while ((p=wAttachList.GetNextItem(-1,LVNI_SELECTED))!=-1)
    {
    char *name=(char *)wAttachList.GetItemData(p);
    free(name);
    wAttachList.DeleteItem(p);
    }
  }

//--------------------------------------------------

void CSendDlg::OnAttachpopupOpen() 
  {
  int p=-1;
  while ((p=wAttachList.GetNextItem(p,LVNI_SELECTED))!=-1)
    {
    char *name=(char *)wAttachList.GetItemData(p);
    if ((int)ShellExecute(*this,NULL,name,NULL,NULL,SW_SHOWNORMAL)<=32)
      if (NrMessageBox(IDS_UNABLEOPENFILE,MB_OKCANCEL)==IDCANCEL) break;
    }
  }

//--------------------------------------------------


//--------------------------------------------------

bool CSendDlg::PostAttachments(CNewsTerminal &term)
  {
  int cnt=wAttachList.GetItemCount();
  int i;
  for (i=0;i<cnt;i++)
    {
    CString err;
    const char *file=(const char *)wAttachList.GetItemData(i);
    ifstream ifn(file,ios::in|ios::binary);
    if (!ifn)
      {	  
      AfxFormatString1(err,IDS_ATTACHNOTFOUND,file);
      int p=NrMessageBox(err,MB_ABORTRETRYIGNORE);
      if (p==IDABORT) return false;
      if (p==IDRETRY) i--;	  
      }
    else
      {
      term.SendLine("");
      if (uuencode(file,ifn,send_line,&term)==false)
        {
        AfxFormatString1(err,IDS_ATTACHREADERROR,file);
        NrMessageBox(err,MB_OK);
        return false;
        }
      }
    }
  return true;
  }

//--------------------------------------------------

void CSendDlg::OnDropFiles(HDROP hDropInfo) 
  {
  int cnt;
  int max=1;
  cnt=::DragQueryFile(hDropInfo,0xFFFFFFFF,NULL,0);
  int i;
  for (i=0;i<cnt;i++)
    {
    int p=::DragQueryFile(hDropInfo,i,NULL,0);
    if (max<p) max=p;
    }
  max++;
  char *buff=(char *)alloca(max);
  for (i=0;i<cnt;i++)
    {
    ::DragQueryFile(hDropInfo,i,buff,max);
	if ((GetAsyncKeyState(VK_SHIFT) & 0x8000) && (GetAsyncKeyState(VK_CONTROL) & 0x8000))
	  DropFile(&wnd_msgbody,buff);
    else
	  AddToAttachList(buff);
    }	
  ::DragFinish(hDropInfo);
  }

//--------------------------------------------------

void CSendDlg::OnMessref() 
  {
  char buff[256];
  LRESULT res=theApp.m_pMainWnd->SendMessage(NRM_GETSELMESSAGEREF,sizeof(buff),(LPARAM)buff);
  if (res && buff[0])
    {
    char *p=buff;if (*p=='<') p++;
    char *c=strchr(p,'>');
    if (c) *c=0;
    wnd_msgbody.ReplaceSel("news:");
    wnd_msgbody.ReplaceSel(p);	
    }
  }

//--------------------------------------------------

#include "ThreadPropDlg.h"

void CSendDlg::OnStopthread() 
  {
  CMenu mnu;
  mnu.LoadMenu(IDR_POPUPS);
  CMenu *sub=mnu.GetSubMenu(3);
  CRect rc;
  GetDlgItem(IDC_STOPTHREAD)->GetWindowRect(&rc);
  int id=sub->TrackPopupMenu(TPM_LEFTALIGN|TPM_NONOTIFY|TPM_RETURNCMD|TPM_LEFTBUTTON,rc.left,rc.bottom,this);
  switch (id)
    {
    case ID_PLACEMENU_LEFT: thDlgPlace=thDlg_Left;break;
    case ID_PLACEMENU_RIGHT: thDlgPlace=thDlg_Right;break;
    case ID_PLACEMENU_BOTTOM: thDlgPlace=thDlg_Bottom;break;
    case ID_PLACEMENU_HIDE: thDlgPlace=thDlg_Hide;break;
    }
  PlaceThreadPropertiesDialog();
  ReadjustWindow();
  theApp.WriteProfileInt(SECTION,"Place",thDlgPlace);
  
 /*  CThreadPropDlg dlg(this);
  dlg.properties=newthrdprop;
  if (dlg.DoModal()==IDOK)
    {
    newthrdprop=dlg.properties;
    }*/
  }

//--------------------------------------------------

LRESULT CSendDlg::OnNextDlgCtl(WPARAM wParam, LPARAM lParam)
  {
  if (GetFocus()==&wnd_msgbody && wParam==0)   
    
    return 0;
  else return Default();
  }

//--------------------------------------------------

void CSendDlg::OnEditpopupFont() 
  {
  CHOOSEFONT lpcf;
  memset(&lpcf,0,sizeof(lpcf));
  lpcf.lStructSize=sizeof(lpcf);
  lpcf.hwndOwner=*this;
  lpcf.lpLogFont=&lgedfont;
  lpcf.Flags=CF_INITTOLOGFONTSTRUCT|CF_SCREENFONTS|CF_EFFECTS;
  lpcf.rgbColors=edtcolor;
  if (ChooseFont(&lpcf)==TRUE)
    {
    edtcolor=lpcf.rgbColors;
    GetDlgItem(IDC_MSGBODY)->SetFont(CFont::FromHandle((HFONT)GetStockObject(DEVICE_DEFAULT_FONT)),FALSE);
    edtfont.DeleteObject();
    edtfont.CreateFontIndirect(&lgedfont);
    GetDlgItem(IDC_MSGBODY)->SetFont(&edtfont,TRUE);
    theApp.WriteProfileBinary(SECTION,FONT,(BYTE *)&lgedfont,sizeof(lgedfont));
    theApp.WriteProfileInt(SECTION,COLOR,edtcolor);
    }   
  }

//--------------------------------------------------

HBRUSH CSendDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
  {
  HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
  
  if (pWnd==&wnd_msgbody)
    {
    pDC->SetTextColor(edtcolor);
    }
  else if (pWnd==&wRelated)
  {
	pDC->SetBkColor(GetSysColor(COLOR_BTNFACE));
	return _winBrush;
  }
  // TODO: Return a different brush if the default is not desired
  return hbr;
  }

//--------------------------------------------------

void CSendDlg::OnEditpopupColor() 
  {
  CHOOSECOLOR cc;  
  static COLORREF cccols[16]=
    {0xFFFFFFFF};
  if (cccols[0]==0xFFFFFFFF)
    for (int i=0;i<16;i++) cccols[i]=RGB(rand()&0xFF,rand()&0xFF,rand()&0xFF);
    memset(&cc,0,sizeof(cc));
    cc.lStructSize=sizeof(cc);
    cc.hwndOwner=*this;
    cc.rgbResult=edtcolor;
    cc.Flags=CC_RGBINIT|CC_SOLIDCOLOR;
    cc.lpCustColors=cccols;
    if (ChooseColor(&cc))
      {
      edtcolor=cc.rgbResult;
      theApp.WriteProfileInt(SECTION,COLOR,edtcolor);
      }
    
  }

//--------------------------------------------------

BOOL CSendDlg::PreTranslateMessage(MSG* pMsg) 
  {
  if (pMsg->message==WM_KEYDOWN)
    {
    if (pMsg->wParam==VK_RETURN && (GetKeyState(VK_CONTROL) & 0x80)==0 && GetFocus()->GetDlgCtrlID()!=IDC_MSGBODY)
      pMsg->wParam=VK_TAB;
    }
  else
  if (pMsg->message==WM_CHAR)
    {
    switch (pMsg->wParam)
      {
      case 19:if (SaveMessage()==false) 
                NrMessageBox(IDS_SAVEMSGFAILED);
        return TRUE;      
      case 10:PostMessage(WM_COMMAND,IDC_SEND,0);return TRUE;
      case 20:_thrPropDlg.SetFocus();return TRUE;
      case 13:if (pMsg->hwnd==wRelated) 
              {
                LRESULT res;
                OnClickRelated(NULL,&res);
                OnDblclkRelated(NULL,&res);
                return TRUE;
              }
        break;
      case 32:if (pMsg->hwnd==wRelated) 
              {
                LRESULT res;
                OnClickRelated(NULL,&res);
                return TRUE;
              }
        break;
      default: return CDialog::PreTranslateMessage(pMsg);
      }
    }
  return CDialog::PreTranslateMessage(pMsg);
  }

//--------------------------------------------------

void CSendDlg::OnClose() 
  {
  RevokeDragDrop(*this);
  SetActiveWindow();
  if (sendingDlg.m_hWnd) return;
  int p=NrMessageBox(IDS_STOREMSGASK,MB_YESNOCANCEL|MB_ICONQUESTION);
  if (p==IDCANCEL) return;
  if (p==IDNO) DeleteFile(msgname);
  else   	  
    if (SaveMessage()==false) 
      {
      NrMessageBox(IDS_SAVEMSGFAILED);
      return;
      }
    
    CWindowPlacement wp;
    wp.Retrieve(*this);
    wp.Store(SECTION);
    AttachListReset();
    attachIcon.DeleteImageList();
    DestroyWindow();
  }

//--------------------------------------------------

bool CSendDlg::SaveMessage()
  {
  fstream out(msgname,ios::out|ios::binary|ios::trunc);
  if (!out) return false;
  UpdateData(TRUE);
  Serialize(CGenArchiveOut(out));
  if (!out) return false;
  return true;
  }

//--------------------------------------------------

bool CSendDlg::LoadMessage(const char *name)
  {
  fstream in(name,ios::in|ios::binary);
  if (!in) return false;
  Serialize(CGenArchiveIn(in));
  if (!in) return false;
  msgname=name;
  if (m_hWnd) 
    {
    UpdateData(FALSE);
    OpenCloseAttach();
    }
  return true;
  }

//--------------------------------------------------

void CSendDlg::Serialize(CGenArchive &arch)
  {
  int p;
  arch.Serialize(subject);
  arch.Serialize(to);
  arch.Serialize(from);
  arch.Serialize(msgbody);
  arch.Serialize(ref);
  if (!arch.IsStoring && m_hWnd==NULL) return;
  if (!arch.IsStoring) AttachListReset();
  p=wAttachList.GetItemCount();
  arch.VarSerial(p);
  CString q;
  for (int i=0;i<p;i++)
    if (arch.IsStoring)
      {
      q=(const char *)wAttachList.GetItemData(i);
      arch.Serialize(q);
      } 
    else
      {
      arch.Serialize(q);
      AddToAttachList(q);
      }
  if (arch.IsStoring)
	{
	_thrPropDlg.Save(false);
	arch.Serialize(_thrPropDlg.properties);
	}
  else
	{
	arch.Serialize(_thrPropDlg.properties);
	_thrPropDlg.ParseProperties();
	}
  arch.Serialize(this->newthrdprop);
  arch.Serialize(this->thrdprop);  
  arch.Serialize(this->cancelid);
  }

//--------------------------------------------------

void CSendDlg::OnTimer(UINT nIDEvent) 
  {
  if (nIDEvent==1) SaveMessage();	
  if (nIDEvent==2)
  {
    CWnd *wnd=GetFocus();
    while (wnd && wnd!=this && wnd!=&webEditor) wnd=wnd->GetParent();
    if (wnd==&webEditor) UpdateTexFromWebEditor();
  }
  CDialog::OnTimer(nIDEvent);
  }

//--------------------------------------------------

#ifdef DEMO
int CSendDlg::SendDataInDlg()
  {
  NrMessageBox("This is a DEMO version. Sending articles is disabled in this demo. NewsReader will only simulate this operation. Your message will be saved");
  CSendDlgProgress prgs(this);  
  Sleep(3000);
  return 0;
  }

//--------------------------------------------------

#else

int CSendDlg::SendDataInDlg()
  {
  CString line;
  CSendDlgProgress prgs(this);  
  Sleep(500);
  if (strncmp(to,"news:",4)==0)
    {
		CNewsAccount *acc=FindAccount(to);
		if (acc==NULL) NrMessageBox(IDS_INVALIDNEWSFORMAT);
		const char *q=strchr(to,'//');        
		if (q==NULL) NrMessageBox(IDS_INVALIDNEWSFORMAT);
		q++;
        int err=PostToNews(acc,q);
        if (err==0) 
          {
          DeleteFile(msgname);
          return 0;
          }
    }            
  else
    //Send by email
    {
    CString msg=msgbody;
    FormatMsgBody(msg);
    if (PostByEmail(msg)==0) 
      {
      DeleteFile(msgname);
      return 0;
      }
    }
  return -1;
  }

//--------------------------------------------------

#endif

void CSendDlg::Create(bool visible,CWnd *parent)
  {
  bAutoDelete=true;
  if (visible) showcmd=SW_SHOW;else showcmd=SW_HIDE;
  CDialog::Create(IDD,theApp.config.owned?NULL:GetDesktopWindow());
  NrRegWindow(this);
  SetIcon(theApp.LoadIcon(IDI_NEWMSG),FALSE);
  SetIcon(theApp.LoadIcon(IDI_NEWMSG),TRUE);
  }

//--------------------------------------------------

LRESULT CSendDlg::OnOpenSendDlg(WPARAM wParam, LPARAM lParam)
  {	
  if (wParam==1)
    if (sendingDlg.m_hWnd==NULL) sendingDlg.DoModal();
    if (wParam==0)
      if (sendingDlg.m_hWnd!=NULL) sendingDlg.EndDialog(0);
      if (wParam==2)
        {
        CWindowPlacement wp;
        wp.Retrieve(*this);
        wp.Store(SECTION);
        DestroyWindow();
        }
      return 0;
  }

//--------------------------------------------------

void CSendDlg::OnInserturl() 
  {
  if (geturl.DoModal()==IDOK)	
    wnd_msgbody.ReplaceSel("<URL>\r\n"+geturl.m_url+"\r\n</URL>");	
  }

//--------------------------------------------------

bool CSendDlg::OutboxSend(const char *name)
  {
  Create(false);
  LoadMessage(name);
  UpdateWindow();
  ensync=false;
  CWinThread *thrd=AfxBeginThread((AFX_THREADPROC)SentThread,this,THREAD_PRIORITY_NORMAL,16384,CREATE_SUSPENDED);  
  thrd->m_bAutoDelete =false;
  thrd->ResumeThread();
  DWORD waitres;
  bAutoDelete=false;
  do
    {
    waitres=MsgWaitForMultipleObjects(1,&(thrd->m_hThread),FALSE,INFINITE,QS_ALLINPUT);
    MSG msg;
    while (::PeekMessage(&msg,0,0,0,PM_REMOVE)) DispatchMessage(&msg);
    }
    while (waitres==WAIT_OBJECT_0+1);
    bool out=m_hWnd==NULL;
    if (m_hWnd) DestroyWindow();
    delete thrd;  
    return out;
  }

//--------------------------------------------------


void CSendDlg::OnMove(int x, int y) 
{
	CDialog::OnMove(x, y);
    if (!IsIconic())
	 PlaceThreadPropertiesDialog();
	// TODO: Add your message handler code here
	
}

void CSendDlg::PlaceThreadPropertiesDialog()
{
  if (_thrPropDlg.GetSafeHwnd()!=NULL)
    {
    CRect rc;
    GetWindowRect(&rc);    
    switch (thDlgPlace)
      {
      case thDlg_Left:_thrPropDlg.PlaceLeft(CPoint(rc.left,rc.top),rc.bottom-rc.top);break;
      case thDlg_Bottom:_thrPropDlg.PlaceBottom(CPoint(rc.left,rc.bottom),rc.right-rc.left);break;
      case thDlg_Right:_thrPropDlg.PlaceRight(CPoint(rc.right,rc.top),rc.bottom-rc.top);break;
      default:break;
      }
    if (thDlgPlace==thDlg_Hide) _thrPropDlg.ShowWindow(SW_HIDE);
    else  _thrPropDlg.ShowWindow(SW_SHOW);
    }

}

void CSendDlg::ReadjustWindow()
  {
  if (IsZoomed())
    {
    CRect rc;
    GetWindowRect(&rc);
    HMONITOR mon=MonitorFromRect(&rc,MONITOR_DEFAULTTONEAREST);
    MONITORINFO nfo;
    nfo.cbSize=sizeof(nfo);
    GetMonitorInfo(mon,&nfo);
    rc=nfo.rcWork;
    switch (thDlgPlace)
      {
      case thDlg_Left:rc.left+=_thrPropDlg.GetDefaultWidth();break;
      case thDlg_Bottom:rc.bottom-=_thrPropDlg.GetDefaultHeight();break;
      case thDlg_Right:rc.right-=_thrPropDlg.GetDefaultWidth();break;
      default:break;
      }
    MoveWindow(&rc);
    }
  }

LPARAM CSendDlg::OnReadjustWindow(WPARAM wParam,LPARAM lParam)
  {
  ReadjustWindow();
  return 1;
  }

#include "GotoRefDlg.h"
#include ".\senddlg.h"

void CSendDlg::OnReplacemsg() 
  {
  CGotoRefDlg dlg(this,IDD_MESSAGEREFERENCE);
  dlg._do_not_paste=true;
  CString tofield;
  GetDlgItemText(IDC_TO,tofield);
  if (tofield.Mid(0,5)=="news:")
    {
    int i=tofield.Find('/',5);
    if (i!=-1)
      {
      tofield=tofield.Mid(5,i-5);
      dlg.acc=(CNewsAccount *)theApp.m_pMainWnd->SendMessage(NRM_FINDACCOUNT,0,(LPARAM)(LPCTSTR)tofield);
      }
    }
  if (ref!="") dlg.ref=ref.Mid(1,ref.GetLength()-2);
  if (dlg.DoModal()==IDOK) 
    ref="<"+dlg.ref+">";
  SetDlgItemText(IDC_REF,ref);
  }

void CSendDlg::AttachListReset()
  {
  int cnt=wAttachList.GetItemCount();
  for (int i=0;i<cnt;i++) free((char *)wAttachList.GetItemData(i));
  wAttachList.DeleteAllItems();
  }

void CSendDlg::AddToAttachList(const char *filename)
  {
  int cnt=wAttachList.GetItemCount();
  for (int i=0;i<cnt;i++) if (stricmp(filename,(char *)wAttachList.GetItemData(i))==0) return;
  LVITEM item;
  item.mask=LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM;
  item.iItem=cnt;
  item.iSubItem=0;
  item.pszText=const_cast<char *>(strrchr(filename,'\\'))+1;
  item.lParam=(DWORD)strdup(filename);
  item.iImage=0;
  wAttachList.InsertItem(&item);
  OpenCloseAttach();
  }

void CSendDlg::OpenCloseAttach()
  {
  int cnt=wAttachList.GetItemCount();
  if (cnt==0) wAttachList.ShowWindow(SW_HIDE);
  else wAttachList.ShowWindow(SW_SHOW);
  CRect rc;
  GetClientRect(&rc);
  OnSize(0,rc.right,rc.bottom);
  }

void CSendDlg::OnDblclkAttachlist(NMHDR* pNMHDR, LRESULT* pResult) 
{
OnAttachpopupOpen();
	*pResult = 0;
}

HRESULT CSendDlg::QueryInterface(  REFIID riid, void __RPC_FAR *__RPC_FAR *ppvObject)
{
  return E_NOINTERFACE;
}


ULONG CSendDlg::AddRef( void)
{
  return 1;
}

ULONG CSendDlg::Release( void)
{
  return 0;
}

HRESULT CSendDlg::DragEnter(IDataObject *pDataObj,DWORD grfKeyState, POINTL pt,DWORD *pdwEffect)
{
  dropfiles=pDataObj->QueryGetData(&filedrop)==S_OK;
  if (pDataObj->QueryGetData(&format)==S_OK || dropfiles)   
  {
	if (dropfiles)
	{
	if ((grfKeyState & (MK_CONTROL | MK_SHIFT))==(MK_CONTROL | MK_SHIFT))
	  *pdwEffect=DROPEFFECT_LINK;
	else
	  *pdwEffect=DROPEFFECT_COPY;
	}
  }
  else 
  {
    return E_UNEXPECTED;
  }  
  return S_OK;
}

HRESULT CSendDlg::DragOver( DWORD grfKeyState,POINTL pt, DWORD *pdwEffect)
{  
  if ((grfKeyState & (MK_CONTROL | MK_SHIFT))==(MK_CONTROL | MK_SHIFT) || !dropfiles) 
  {
    POINT ptt;
    ptt.x=pt.x;
    ptt.y=pt.y;
    wnd_msgbody.ScreenToClient(&ptt);
	CRect rc;
	wnd_msgbody.GetClientRect(&rc);
	if (rc.PtInRect(ptt))
	{
      wnd_msgbody.SetFocus();
      int charpos=wnd_msgbody.CharFromPos(ptt);
	  charpos=LOWORD(charpos);
      wnd_msgbody.SetSel(charpos,charpos);
	}
  }
  if (dropfiles)
  {
	if ((grfKeyState & (MK_CONTROL | MK_SHIFT))==(MK_CONTROL | MK_SHIFT))
	  *pdwEffect=DROPEFFECT_LINK;
	else
	  *pdwEffect=DROPEFFECT_COPY;
  }
  return 0;
}

HRESULT CSendDlg::DragLeave( void)
{  
return S_OK;
}
        
HRESULT CSendDlg::Drop( IDataObject *pDataObj,DWORD grfKeyState, POINTL pt,DWORD *pdwEffect)
{
  STGMEDIUM med;

  if (pDataObj->GetData(&format,&med)==S_OK)
  {
    if (med.tymed==TYMED_HGLOBAL)
    {
      int sz=GlobalSize(med.hGlobal);
      void *lock=GlobalLock(med.hGlobal);      
      wnd_msgbody.ReplaceSel((char *)lock);
      GlobalUnlock(med.hGlobal);
      return S_OK;
    }
  }
  else if (pDataObj->GetData(&filedrop,&med)==S_OK)
  {
    if (med.tymed==TYMED_HGLOBAL)
    {
      HDROP drop=(HDROP)med.hGlobal;
      int count=DragQueryFile(drop,0xFFFFFFFF,NULL,0);
      int maxsize=0;
      int i;
      for (i=0;i<count;i++)
      {
        int size=DragQueryFile(drop,i,NULL,0);
        if (size>maxsize) maxsize=size;
      }
      char *buff=(char *)alloca(maxsize+20);
      for (i=0;i<count;i++)
      {
        DragQueryFile(drop,i,buff,maxsize+1);        
		if ((grfKeyState & (MK_CONTROL | MK_SHIFT))!=(MK_CONTROL | MK_SHIFT)) 
		  AddToAttachList(buff);		
		else		
		  DropFile(&wnd_msgbody,buff);		
      }      
      DragFinish(drop);
      return S_OK;
    }
  }

return E_UNEXPECTED;
}

static bool FindRelatedCallback(GRPFINDINFO& fnd)
{
  CSendDlg *snd=(CSendDlg *)fnd.context;  
  return snd->SendMessage(MSG_ADDRELATED,0,(LPARAM)&fnd)!=0;
}

static int CALLBACK SortRealtedItems(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
  return (lParam1<lParam2)-(lParam1>lParam2);
}



LRESULT CSendDlg::RelatedCallback(WPARAM wParam,LPARAM lParam)
{
  GRPFINDINFO &fnd=*(GRPFINDINFO *)lParam;
  CFindList *found=fnd.firstfnd;
  fnd.firstfnd=NULL;
  CFindList *nx=found;
  bool resort=false;
  while (nx)
  {
	if (nx->accuracy>0.6f)
	{
      CNewsArtHolder *par=nx->hlda;
      int accu=(int)(nx->accuracy*100);
      while (par->GetParent()!=NULL) par=par->GetParent();
      if (par==_lastSearchThread)
      {
        if (wRelated.GetItemData(_lastThreadPos)<=accu)
        {
          wRelated.SetItemText(_lastThreadPos,0,nx->hlda->subject);
          wRelated.SetItemText(_lastThreadPos,1,nx->hlda->messid);
          wRelated.SetItemData(_lastThreadPos,accu);
        }
      }
      else
      {
        _lastSearchThread=par;
        wRelated.SortItems(SortRealtedItems,0);
	    UpdateRelatedWindow();
	    wRelated.ShowWindow(SW_SHOW);
	    int i=wRelated.InsertItem(wRelated.GetItemCount(),nx->hlda->subject);
  	    wRelated.SetItemText(i,1,nx->hlda->messid);
	    wRelated.SetItemData(i,nx->accuracy*100);	
  	    resort=true;
        _lastThreadPos=i;
      }
	}
	nx=nx->next;
  }
  delete found;
  return !_stopRelatedSearch;
}

void CSendDlg::SearchRelated()
{
  GRPFINDINFO nfo;  
  memset(&nfo,0,sizeof(nfo));
  CString subj;
  GetDlgItemText(IDC_SUBJECT,subj);
  if (subj.GetLength()<4) return;
  nfo.subject=NewFind(false,false,subj);
  nfo.context=this;
  nfo.FndCallback=FindRelatedCallback;
  CString accname;
  GetDlgItemText(IDC_TO,accname);
  if (strnicmp(accname,"news:",5)) return;
  CNewsAccount *acc=FindAccount(accname);
  if (acc==NULL) return;
  CNewsGroup *grp=acc->GetGroups();
  CNewsGroup *presrch=NULL;
  int index=accname.Find("/",7);
  
  wRelated.DeleteAllItems();
  CNewsTerminal term;
 
 if (index!=-1)
  {
    presrch=grp;
    const char *grpname=(LPCTSTR)accname+index+1;
    while (presrch && stricmp(presrch->GetName(),grpname)!=0) presrch=presrch->GetNext();
    if (presrch) presrch->FindInGroup(nfo,term);
  }

  while (grp && !_stopRelatedSearch)
  {
    _lastThreadPos=-1;
    _lastSearchThread=NULL;
	if (grp!=presrch && grp->sign)
    {
      grp->FindInGroup(nfo,term);
      if (_lastSearchThread!=NULL)
        wRelated.SortItems(SortRealtedItems,0);
    }
	grp=grp->GetNext();	
  }
}

CNewsAccount * CSendDlg::FindAccount(const char *to)
{
  //Send to news group;
  const char *p=strchr(to,':')+1;
  while (*p && *p<33) p++;
  if (*p)
  {
	const char *q=strchr(p,'/');
	if (q==NULL)
	  p=NULL;
	else
	{
	  int size=q-p;
	  char *temp=(char *)alloca(size+1);
	  strncpy(temp,p,size);
	  temp[size]=0;
	  CNewsAccount *acc=(CNewsAccount *)theApp.m_pMainWnd->SendMessage(NRM_FINDACCOUNT,0,(LPARAM)temp);
	  return acc;
	}
  }
  return NULL;
}

static UINT SearchRelatedThread(LPVOID ptr)
{
  CSendDlg *dlg=(CSendDlg *)ptr;
  dlg->SearchRelated();
  return 0;
}

void CSendDlg::OnKillfocusSubject() 
{  
  if (cancelid=="")
  {
	CString tosrch;
	GetDlgItemText(IDC_SUBJECT,tosrch);
	if (tosrch!=_lastSearch)
	{
      _lastThreadPos=-1;
      _lastSearchThread=NULL;
	  _lastSearch=tosrch;
	  StopRelatedSearch();
	  wRelated.ShowWindow(SW_HIDE);
	  _stopRelatedSearch=false;
	  _searchRelatedThread=AfxBeginThread(SearchRelatedThread,this,THREAD_PRIORITY_BELOW_NORMAL,0,CREATE_SUSPENDED,NULL);
	  _searchRelatedThread->m_bAutoDelete=false;
	  _searchRelatedThread->ResumeThread();
	}
  }
}

void CSendDlg::StopRelatedSearch()
{
  if (_searchRelatedThread!=NULL)
  {
	_stopRelatedSearch=true;
	while (MsgWaitForMultipleObjects(1,&_searchRelatedThread->m_hThread,FALSE,INFINITE,QS_SENDMESSAGE)==WAIT_OBJECT_0+1)
	  AfxGetThread()->PumpMessage();
  }
  delete _searchRelatedThread;
  _searchRelatedThread=NULL;
}

void CSendDlg::OnDblclkRelated(NMHDR* pNMHDR, LRESULT* pResult) 
{
	  theApp.m_pMainWnd->BringWindowToTop();
	*pResult = 0;
}

void CSendDlg::UpdateRelatedWindow()
{
  CRect rc;
  wRelated.GetClientRect(&rc);
  rc.right-=GetSystemMetrics(SM_CXVSCROLL)+GetSystemMetrics(SM_CXDLGFRAME)*2;
  wRelated.SetColumnWidth(0,rc.right-rc.left);  
}
 

void CSendDlg::OnClickRelated(NMHDR* pNMHDR, LRESULT* pResult) 
{
	POSITION pos=wRelated.GetFirstSelectedItemPosition();
	if (pos)
	{
	  int i=wRelated.GetNextSelectedItem(pos);
	  CString s;
	  s=wRelated.GetItemText(i,1);
	  ((CMainFrame *)theApp.m_pMainWnd)->GotoMessageId(s);
	}
	*pResult = 0;
}

void CSendDlg::OnDestroy() 
{
	StopRelatedSearch();
	CDialog::OnDestroy();	
}

void CSendDlg::OnEnablehtml() 
{
  CRect rc;
  if (wEnableHtml.GetCheck())
  {
    if (wnd_msgbody.GetWindowTextLength() && NrMessageBox(IDS_CONVERTTEXTTOHTML,MB_YESNO)==IDYES)
    {
      CString text;
      wnd_msgbody.GetWindowText(text);
      istrstream in(const_cast<char *>((LPCTSTR)text),0);
      ostrstream out;
      Text2Html(in,out,NULL);
      out.put((char)0);
      wnd_msgbody.SetWindowText(out.str());
      out.rdbuf()->freeze(false);
    }
    wnd_msgbody.GetWindowRect(&rc);
    ScreenToClient(&rc);
    webEditor.Create(AfxRegisterWndClass(0),"",WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_GROUP,rc,this,198764);
    SetWindowLong(webEditor,GWL_EXSTYLE,WS_EX_CLIENTEDGE);
    webEditor.ResNavigate("HTMLEDITOR.HTM");
    IDispatch* idispdoc=webEditor.GetHtmlDocument();
    IHTMLDocument2* pDoc;    
    idispdoc->QueryInterface(IID_IHTMLDocument2,(void**)&pDoc);
    pDoc->put_designMode(L"On");
    idispdoc->Release();
    pDoc->Release();
    SetTimer(2,1000,0);
/*    MSG msg;
    IHTMLElement *body=0;
    while (body==0) 
    {
      GetMessage(&msg,0,0,0);
      DispatchMessage(&msg);    
      pDoc->get_body(&body);
    }
    CString text;
    wnd_msgbody.GetWindowText(text);
    istrstream in(const_cast<char *>((LPCTSTR)text),0);
    ostrstream out;
    Text2Html(in,out,NULL);
    int wsz=out.pcount();

    unsigned short *wstr;
    wstr=new unsigned short[wsz+1];
    wsz=MultiByteToWideChar(1250,0,out.str(),out.pcount(),wstr,wsz);
    wstr[wsz]=0;
    CComBSTR bstr(wstr);
    delete [] wstr;
    body->put_innerHTML(bstr);
    body->Release();
    idispdoc->Release();     
    out.rdbuf()->freeze(false);
    pDoc->Release();*/
    UpdateWebEditor();
  }
  else
  {
    webEditor.GetWindowRect(&rc);
    ScreenToClient(&rc);
    webEditor.DestroyWindow();
    wnd_msgbody.MoveWindow(&rc);
    KillTimer(2);
  }
  GetClientRect(&rc);
  OnSize(0,rc.right,rc.bottom);
}

void CSendDlg::UpdateWebEditor()
{
  if (webEditor.GetSafeHwnd())
  {
    IDispatch* idispdoc=webEditor.GetHtmlDocument();
    IHTMLDocument2* pDoc;    
    idispdoc->QueryInterface(IID_IHTMLDocument2,(void**)&pDoc);
    MSG msg;
    IHTMLElement *body=0;
    while (body==0) 
    {
      GetMessageA(&msg,0,0,0);
      DispatchMessage(&msg);    
      pDoc->get_body(&body);
    }
    CString text;
    wnd_msgbody.GetWindowText(text);
    int wsz=text.GetLength();
    wchar_t *wstr;
    wstr=new wchar_t[wsz+1];
    wsz=MultiByteToWideChar(1250,0,text,text.GetLength(),wstr,wsz);
    wstr[wsz]=0;
    CComBSTR bstr(wstr);
    delete [] wstr;
    body->put_innerHTML(bstr);
    body->Release();
    idispdoc->Release();     
    pDoc->Release();
  }
}

void CSendDlg::OnChangeMsgbody() 
{
UpdateWebEditor();	
}

void CSendDlg::UpdateTexFromWebEditor()
  {
    CString oldText;
    wnd_msgbody.GetWindowText(oldText);
    IDispatch* idispdoc=webEditor.GetHtmlDocument();
    IHTMLDocument2* pDoc;    
    idispdoc->QueryInterface(IID_IHTMLDocument2,(void**)&pDoc);
    MSG msg;
    IHTMLElement *body=0;
    pDoc->get_body(&body);
    if (body!=0)
    {
      BSTR res=0;
      body->get_innerHTML(&res);
      if (res)
      {
        int len=wcslen(res);
        char *text=new char[len+1];
        WideCharToMultiByte(1250,0,res,len,text,len,0,0);
        text[len]=0;
        SysFreeString(res);
        if (len!=oldText.GetLength())
        {
          wnd_msgbody.SetWindowText(text);
          if (oldText.GetLength() && len)
          {
            const char *oldt=oldText;
            int a=0;
            while (text[a] && oldt[a]==text[a]) a++;
            int begch=a;
            a=oldText.GetLength()-1;
            int b=len-1;
            while (a>0 && b>0 && oldt[a]==text[b]) {a--,b--;};
            int endch=b;
            if (begch>endch) endch=begch;
            wnd_msgbody.SetSel(begch,endch+1);
          }
        }
        delete [] text;
      }
      body->Release();
    }
    pDoc->Release();
    idispdoc->Release();

  }

  void CSendDlg::OnBnClickedWiki()
  {
    geturl.m_url=wikiLink;
    if (geturl.DoModal()==IDOK)	
      wikiLink=geturl.m_url;
    bWiki.SetCheck(wikiLink!="");    
  }
