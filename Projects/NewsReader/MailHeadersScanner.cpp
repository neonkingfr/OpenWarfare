// MailMessage.cpp: implementation of the MailHeadersScanner class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MailHeadersScanner.h"
#include "ArchiveStreamMemory.h"
#include "ArchiveStreamBase64Filter.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

bool MailHeadersScanner::ScanHeader(ArchiveStream &in)
  {
  if (!in.IsReading()) return false;
  ArchiveStreamMemoryOut linebuf;
  char curchar;
  in.ExSimple(curchar);
  while (!in.IsError())
    {
    if (curchar!='\r')
      {
      if (curchar=='\n')
        {
		if (linebuf.GetBufferSize()==0) 
		  return true;
        in.ExSimple(curchar);
        if (!in.IsError())
          {
          if (curchar=='\t')
            {
            while (isspace(curchar) && !in.IsError()) in.ExSimple(curchar);
            }
          else
            {
            char zero=0;
            linebuf.ExSimple(zero);
            char *buffer=linebuf.GetBuffer();
            if (strcmp(buffer,".")==0 || strcmp(buffer,"")==0) 
              return true;
            char *dotdot=strchr(buffer,':');
            if (dotdot!=NULL)
              {
              *dotdot=0;
              dotdot++;
              while (isspace(*dotdot) && *dotdot!=0) dotdot++;
              ParseLine(buffer,dotdot);
              }
            linebuf.Rewind();
            }
          continue;
          }
        }
      else
        linebuf.ExSimple(curchar);
      }
    in.ExSimple(curchar);
    }
  return true;
  }

void MimeHeader(char *name)
  {
  char *src=name,*trg=name;
  while (*src)
    {
    if (src[0]=='=' && src[1]=='?')
      {	  
      char *p=strchr(src+2,'?');	  
      if (p!=NULL) 
        {
        p++;
        if (*p=='Q') 
          {
          p++;
          if (*p=='?') 
            {
            src=p+1;
            while (*src && !(src[0]=='?' && src[1]=='='))
              {
              if (*src=='=')
                {
                src++;
                int val=(*src>='A' && *src<='F')?*src-55:*src-48;
                val*=16;src++;
                val+=(*src>='A' && *src<='F')?*src-55:*src-48;
                src++;
                *trg++=(char)val;
                }
              else if (*src=='_') {*trg++=' ';src++;}
              else
                *trg++=*src++;
              }
            if (*src) src+=2;
            continue;
            }
          }
        else if (*p=='B')
          {
          p++;
          if (*p=='?') 
            {
            src=p+1;
            char *end=src;
            while (*end && !(end[0]=='?' && end[1]=='=')) end++;
            ArchiveStreamMemoryIn memin(src,end-src,false);
            ArchiveStreamBase64Filter base64(&memin);
            char c;
            base64.ExSimple(c);
            while (!base64.IsError()) 
              {
              *trg++=c;
              base64.ExSimple(c);
              }
            src=end;
            if (*src) src+=2;
            }
          }
        }
      }
    *trg++=*src++;
    }
  *trg=0;
  }


