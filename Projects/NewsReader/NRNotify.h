#if !defined(AFX_NRNOTIFY_H__0A0D1381_A137_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
#define AFX_NRNOTIFY_H__0A0D1381_A137_11D5_B3A0_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NRNotify.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNRNotify window

#define MAXINMENU 50
#define NRN_NEWMESSAGE (WM_APP+1) 
  //WPARAM 0 LPARAM CNewsArtHolder *
  //WPARAM 1 LPARAM CNewsGroup *
  //WPARAM 2 LPARAM CNewsAccount *

class CNewsArtHolder;
class CNewsAccount;
class CNewsGroup;
class CNRNotify : public CWnd
{
// Construction
  HICON deficon;
  HICON ntficon;
  HICON stpicon;
  HICON emptyicn;
  NOTIFYICONDATA nid;

  bool dirtystate;
  CNewsArtHolder *hldlist[MAXINMENU];
  CNewsAccount *acclist[MAXINMENU];
  CNewsGroup *grplist[MAXINMENU];
  CNewsAccount *curacc;
  CNewsGroup *curgrp;
  int totalcnt;
  bool blink;
  UINT blinkevent;

public:
	CNRNotify();
	bool IsEnabled() {return m_hWnd!=NULL;}

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNRNotify)
	//}}AFX_VIRTUAL

// Implementation
public:
	void CheckAfterCatchUp();
	void Blink();
	bool ContainUrgent();
	void Reset();
	void Repaint() {dirtystate=true;}
	void DeleteArticle(CNewsArtHolder *hld);
	void NewArticleIncome(CNewsArtHolder *hlda);
	bool Create(int deficon,int msgicon,int stopicon, CWnd *parent);
	virtual ~CNRNotify();

	// Generated message map functions
protected:
	//{{AFX_MSG(CNRNotify)
	afx_msg void OnDestroy();
	afx_msg LRESULT OnShellNotify(WPARAM wParam, LPARAM lParam);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg LRESULT OnNRNNewMessage(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void SetTooltipText();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NRNOTIFY_H__0A0D1381_A137_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
