// Web.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "Web.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Web dialog


Web::Web(CWnd* pParent /*=NULL*/)
	: CDialog(Web::IDD, pParent)
{
	//{{AFX_DATA_INIT(Web)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void Web::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Web)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Web, CDialog)
	//{{AFX_MSG_MAP(Web)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Web message handlers

BOOL Web::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CRect rc;
	GetClientRect(&rc);

	m_web.Create(NULL,"",WS_CHILD|WS_VISIBLE,rc,this,0,NULL);

	m_web.Navigate(url,NULL,NULL,NULL,NULL);		
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
