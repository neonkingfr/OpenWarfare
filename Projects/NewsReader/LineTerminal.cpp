// LineTerminal.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "LineTerminal.h"
#include "DogWindow.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLineTerminal

HANDLE CLineTerminal::debugout=FALSE;
bool CLineTerminal::stop=false;

CLineTerminal::CLineTerminal():timeout(0,0,0,30)
{
stopwnd=NULL;
}

CLineTerminal::~CLineTerminal()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CLineTerminal, CSocket)
	//{{AFX_MSG_MAP(CLineTerminal)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CLineTerminal member functions

int CLineTerminal::ReceiveLine(CString &line)
  {
  runflag=true;
  char *entr;
  line="";
  laststamp=laststamp.GetCurrentTime();
  do
	{
	if (buffer[0]==13) 
	  strcpy(buffer,buffer+1);
	entr=strchr(buffer,'\n');
	if (entr==NULL)
	  {
	  line+=buffer;
	  if (stop) 
		return CLT_INTERRUPTED;
	  read=Receive(buffer,sizeof(buffer)-1);	 
	  MSG msg;
	  while (::PeekMessage(&msg, NULL,0,0,PM_REMOVE)) DispatchMessage(&msg);
	  if (read<1)
		{
		if (read==0) return CLT_CONNECTIONRESET;
		read=0;
		return CLT_READTIMEOUT;
		}
  	  buffer[read]=0;
	  if (buffer[read-1]==13)
		buffer[read-1]=0;
	  }
	else
	  {
	  *entr=0;
	  if (entr>buffer && entr[-1]==0x0D) entr[-1]=0;
	  line+=buffer;
	  strcpy(buffer,entr+1);
	  }
	}
  while (entr==NULL);
  DWORD out;
  if (debugout) 
	{
	WriteFile(debugout,(LPCVOID)((LPCTSTR)line),line.GetLength(),&out,NULL);
	WriteFile(debugout,(LPCVOID)"\r\n",2,&out,NULL);
	}
  return CLT_OK;
  }

int CLineTerminal::SendLine(const char *line)
  {
  if (stop) return CLT_INTERRUPTED;
  DWORD out;
  if (debugout) WriteFile(debugout,(LPCVOID)(line),strlen(line),&out,NULL);
  laststamp=laststamp.GetCurrentTime();
  int len=strlen(line);
  if (len)
	{
	int ss=Send((void *)line,len);
	if (ss==0) return CLT_CONNECTIONRESET;
	if (ss<1) return CLT_WRITETIMEOUT;
	while (ss!=len)
	  {
	  line=(const char *)((char *)line+ss);
	  len-=ss;
	  ss=Send(line,len);
	  if (ss==0) return CLT_CONNECTIONRESET;
	  if (ss<1) return CLT_WRITETIMEOUT;
	  }
	}
  if (line[len-1]!='\n' || line[len-2]!='\r')
	return SendLine("\r\n");
  OnMessagePending();
  return CLT_OK;
  }

int CLineTerminal::SendFormattedV(const char *format, va_list list)
  {
  CString f;
  f.FormatV(format,list);
  return SendLine(f);
  }

int CLineTerminal::SendFormatted(const char *format, ...)
  {
  va_list list;
  va_start(list,format);
  return SendFormattedV(format,list);
  }

int CLineTerminal::ReceiveNumReply(CString &comment)
  {
  int rep=ReceiveLine(comment);
  if (rep<0) return rep;
  if (sscanf(comment,"%d",&rep)!=1) return CLT_INVALIDREPLY;
  char *c=strchr(comment,' ');
  if (c==NULL) 
	comment="";
  else
	{
	CString p=(c+1);
	comment=p;
	}
  return rep;
  }

int CLineTerminal::Connect(const char *address, unsigned int port)
  {
  stop=false;
  Create();
  runflag=true;
  SOCKADDR_IN sin;
  if (GetHostByName(address,sin)==false) return CLT_CONNECTFAILED;
  sin.sin_port=htons(port);
  if (theApp.proxy.sockenable)
	{ 
	SOCKADDR_IN psin;
	if (GetHostByName(theApp.proxy.address,psin)==false) return CLT_CONNECTFAILED;
	psin.sin_port=htons(theApp.proxy.port);
	if (CSocket::Connect((SOCKADDR *)&psin,sizeof(psin))==FALSE) return CLT_CONNECTFAILED;
    unsigned char buff[20];
    buff[0]=4;
    buff[1]=1;
    buff[2]=port>>8;
    buff[3]=port & 0xFF;
    buff[4]=sin.sin_addr.S_un.S_un_b.s_b1;
    buff[5]=sin.sin_addr.S_un.S_un_b.s_b2;
    buff[6]=sin.sin_addr.S_un.S_un_b.s_b3;
    buff[7]=sin.sin_addr.S_un.S_un_b.s_b4;
    buff[8]=0;
    Send(buff,9);
    Receive(buff,8);
    if (buff[1]!=90)  
	  {
	  Close();
	  return CLT_CONNECTFAILED;
	  }
	Receive(buff,6);
	} 
  else
	{
	laststamp=laststamp.GetCurrentTime();  
	if (CSocket::Connect((SOCKADDR *)&sin,sizeof(sin))==FALSE)
	  return CLT_CONNECTFAILED;
	}
  buffer[0]=0;
  return CLT_OK;
  }

void CLineTerminal::SetTimeout(CTimeSpan tim)
  {
  timeout=tim;
  }

static BOOL CALLBACK EnumDispatch(HWND hWnd, LPARAM lParam)
  {
  MSG msg;
  EnumChildWindows(hWnd,EnumDispatch,lParam+1);
  while (::PeekMessage(&msg,hWnd,0,0,PM_REMOVE))
	{
	TranslateMessage(&msg);
	DispatchMessage(&msg);
	}
  return TRUE;
  }

BOOL CLineTerminal::OnMessagePending() 
  {
  MSG msg;
/*  while (::PeekMessage(&msg,0,WM_TIMER,WM_TIMER,PM_REMOVE)) DispatchMessage(&msg);
  if (stopwnd) 
	{
	EnumChildWindows(stopwnd,EnumDispatch,0);
	EnumDispatch(stopwnd,0);
	}*/
  if (::PeekMessage(&msg,0,WM_KEYDOWN,WM_KEYDOWN,PM_NOREMOVE)==TRUE)	
	if (msg.wParam==VK_ESCAPE)
	  {
	  stop=true;
	  ::PeekMessage(&msg,0,WM_KEYDOWN,WM_KEYDOWN,PM_REMOVE);
	  }	
  if (stop) 
	{
	CancelBlockingCall();
	}
  else
	{
	CTime tim;
	tim=tim.GetCurrentTime();
    if (tim>laststamp+timeout) 
	  CancelBlockingCall();
	}
  ::runflag=true;
  CSocket::OnMessagePending();
  return FALSE;
  }

int CLineTerminal::ShowError(int result)
  {
  if (result!=0)
	{
	const char *p;
	switch (result)
	  {
	  case CLT_READTIMEOUT:p="Connection read timeout!";break;
	  case CLT_WRITETIMEOUT:p="Connection write timeout!";break;
	  case CLT_CONNECTIONRESET: p="Connection has been reset by the peer!";break;
	  case CLT_INVALIDREPLY: p="Invalid reply by remote computer.";break;
	  case CLT_CONNECTFAILED: p="Connect failed. Check address, port and try it again later. Server may be down.";break;
	  case CLT_INTERRUPTED: p="Transfer Interrupted";break;
	  default: p="Unknown error code.";break;
	  }
	DogShowError(p);
	}
  return result;  
  }

void CLineTerminal::OpenDebug()
  {
  AllocConsole();
  debugout=GetStdHandle(STD_OUTPUT_HANDLE);
  }
  

void CLineTerminal::CloseDebug()
  {
  FreeConsole();
  debugout=NULL;
  }

bool CLineTerminal::DebugOpened()
  {
  return debugout!=NULL;
  }

bool CLineTerminal::GetHostByName(const char *name, SOCKADDR_IN &host)
  {
  //fill requed header for SOCKADDR_IN
  host.sin_family=AF_INET; 
  //try get address from name, if it is "dotted numbers"
  host.sin_addr.s_addr = inet_addr(name);
  //return immediateli, if successful
  if (host.sin_addr.s_addr != INADDR_NONE) return true;

  //create notify window
  HWND wnd=CreateWindow("STATIC","Notify",0,0,0,10,10,NULL,NULL,AfxGetInstanceHandle(),0);
  char buffer[MAXGETHOSTSTRUCT];
  //request asynchronious GetHostByName
  laststamp=CTime::GetCurrentTime();
  HANDLE h=WSAAsyncGetHostByName(wnd,WM_APP,name,buffer,MAXGETHOSTSTRUCT);
  MSG msg;
  //mark this stamp
  BOOL result;
  SetTimer(wnd,100,1000,NULL);
  //wait for complette operation
  do
	{
	do
	  {
	  WaitMessage();
	  if (::PeekMessage(&msg,0,WM_KEYDOWN,WM_KEYDOWN,PM_NOREMOVE)==TRUE)	
		if (msg.wParam==VK_ESCAPE)
		  {
		  stop=true;
		  ::PeekMessage(&msg,0,WM_KEYDOWN,WM_KEYDOWN,PM_REMOVE);
		  } 	
	  }
	while (PeekMessage(&msg,wnd,0,0,PM_REMOVE)==FALSE && !stop);
	if (msg.message==WM_KEYDOWN && msg.wParam==VK_ESCAPE) stop=true;
	else DispatchMessage(&msg);
	if (stop) break; //when user cancels operation exit
	if ((CTime::GetCurrentTime()-laststamp)>timeout) break; //when timeout occured	
	}
  while (msg.message!=WM_APP);
  result=false;
  if (msg.message==WM_APP) //event has been returned
	{
	int error=WSAGETASYNCERROR(msg.lParam); //get error code
	if (error==0)  //noerror, get result
	  {
	  HOSTENT *pp=(HOSTENT *)buffer;
	  memcpy(&host.sin_addr.s_addr,pp->h_addr_list[0],4); 
	  result=true;
	  }
	else
	  result=FALSE;
	}  
  else
	WSACancelAsyncRequest(h); 
  //destroy notify window
  KillTimer(wnd,100);
  DestroyWindow(wnd);
  //flush thread queue
  while (::PeekMessage(&msg,wnd,WM_APP,WM_APP,PM_REMOVE)==TRUE);
  return result!=FALSE;
  }

void CLineTerminal::Attach(SOCKET s)
  {
  CSocket::Attach(s);
  stop=false;
  buffer[0]=0;
  }

void CLineTerminal::Init()
  {
  stop=false;
  runflag=true;
  buffer[0]=0;
  }
