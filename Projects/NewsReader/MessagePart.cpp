// MessagePart.cpp: implementation of the CMessagePart class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "MessagePart.h"
using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMessagePartCache memcache;

#define UUEMARK "begin "
#define UUEMARKSIZE 10

#define PARTCACHE theApp.config.cache


static CCriticalSection glob_lock;


void CMessagePart::InsertCache(unsigned long owner)
  {
  memcache.AddToCache(this,owner);
  }


//--------------------------------------------------

CMessagePart::CMessagePart()
  {
  type=unknown;
  next=NULL;
  filename=NULL;
  data=NULL;  
  tempfname=NULL;
  }

//--------------------------------------------------

CMessagePart::~CMessagePart()
  {
  if (tempfname) 
  {
	DeleteFile(tempfname);
	*strrchr(tempfname,'\\')=0;
	RemoveDirectory(tempfname);
  }
  delete tempfname;
  delete next;
  free(filename);
  free(data);
  }

//--------------------------------------------------

#define DEC(c)	(((c) - ' ') & 077)


inline void Remove_CR(char *buff)
  {
  int l=strlen(buff);
  if (l && buff[l-1]==13) buff[l-1]=0;
  }

//--------------------------------------------------

inline bool IsTab(char *buff)
  {
  int l=strlen(buff);
  if (l && buff[l-1]=='\t')
    {
    buff[l-1]=0;
    return true;
    }
  return false;
  }

//--------------------------------------------------

static void outdec(char *p, ostream& f, int n)
  {
  int c1, c2, c3;
  
  c1 = (DEC(*p) << 2) | (DEC(p[1]) >> 4);
  c2 = (DEC(p[1]) << 4) | (DEC(p[2]) >> 2);
  c3 = (DEC(p[2]) << 6) | DEC(p[3]);
  if (n >= 1)
    f.put((char)c1);
  if (n >= 2)
    f.put((char)c2);
  if (n >= 3)
    f.put((char)c3);
  }

//--------------------------------------------------

static void LoadUUE(istream& in, ostream& out)
  {
  char buf[80];
  char *bp;
  int n;
  
  while (!in.eof())
    {
    /* for each input line */
    in.getline(buf,sizeof(buf));
    if (!in) 
      break;
    Remove_CR(buf);
    n=DEC(buf[0]);
    if (n<=0) break;
    bp=&buf[1];
    while (n>0)
      {
      outdec(bp, out, n);
      bp += 4;
      n -= 3;		 
      }
    }
  }

//--------------------------------------------------

static bool IsUUEMark(char *buff)
  {
  if (strncmp(buff,UUEMARK,6)) return false;
  int i;
  if (sscanf(buff+6,"%o",&i)!=1) return false;
  if (i<0 && i>0777) return false;
  if (strlen(buff)<12) return false;
  return true;
  }

//--------------------------------------------------

inline void ReadUntilControl(istream &str, char *buff, int chars)
  {
  int i;
  chars--;
  while (chars--)
    {
    i=str.get();
    if (i<32) 
      {str.putback((char)i);break;}
    *buff++=i;
    }
  *buff=0;
  }

//--------------------------------------------------

void CMessagePart::LoadMessage(istream& in)
  {
  theApp.Lock();
  char buf[80]="";
  ostrstream out;
//  out.rdbuf()->setbuf(NULL,16384);
  int i=in.get();
  while (i!=EOF && i<33) i=in.get();
  in.putback(i);
  if (type==unknown)
    {
    ReadUntilControl(in,buf,sizeof(buf)/sizeof(char));
    if (IsUUEMark(buf))
      {
      type=binary_uue;
      filename=strdup(buf+UUEMARKSIZE);
      }
    else
      type=text;
    }
  if (type==text)
    {
    out<<buf;
    int i=in.peek();
    while (i!=EOF)
      {
      if (i=='\r')
        {
        in.ignore();
        }
      else if (i=='\t')
        {
        i=in.get();
        if (in.peek()=='\r') in.ignore();
        if (in.peek()=='\n') 
          {out<<" ";in.ignore();}
        else out<<'\t';
        }
      else if (i=='\n') 
        {out<<"\r\n";in.ignore();}
      else if (i<32) 
        {out<<i;in.ignore();}
      ReadUntilControl(in,buf,sizeof(buf)/sizeof(char));
      if (IsUUEMark(buf))
        {
        next=new CMessagePart();
        next->type=binary_uue;
        next->filename=strdup(buf+UUEMARKSIZE);
        next->LoadMessage(in);		
        break;
        }	  
      out<<buf;
      i=in.peek();
      }
    out.put((char)0);
    
    }
  else if (type==binary_uue)
    {
    ws(in);
    LoadUUE(in,out);
    in.getline(buf,sizeof(buf)/sizeof(char)-1,'\n');
    Remove_CR(buf);
    if (strncmp(buf,"end",3)==0) in.getline(buf,sizeof(buf)/sizeof(char)-1,'\n');
    next=new CMessagePart();
    next->LoadMessage(in);
    }
  data=out.str();
  int p=out.tellp();
  if (p<0) p=0;
  datasize=p;
  data=(char *)realloc(data,p*sizeof(char));
  theApp.Unlock();
  }

//--------------------------------------------------

static int tempcntr=(int)GetTickCount();

const char * CMessagePart::CreateAsFile(void* uid, const char *contenttype)
  {  
  if (data==NULL) return "";
  if (tempfname!=NULL) return tempfname;
  glob_lock.Lock();      
  static char buffer[MAX_PATH+500];
  ::GetTempPath(sizeof(buffer),buffer);
  sprintf(strchr(buffer,0),"NRTEMP_%X",uid);
  CreateDirectory(buffer,NULL);
  strcat(buffer,"\\");
  char *temp;
  if (filename)
	temp=strcat(strcpy((char *)alloca(strlen(buffer)+strlen(filename)+1),buffer),filename);      
  else if (contenttype!=NULL)
	temp=strcat(strcpy((char *)alloca(strlen(buffer)+20),buffer),"content.eml");      
  else
	temp=strcat(strcpy((char *)alloca(strlen(buffer)+20),buffer),"content.txt");      
  ofstream out;
  int openmode=ios::out|ios::trunc;
  /*if (type!=text) */openmode|=ios::binary;
  out.open(temp,openmode);
  if (!out) 
    {
    glob_lock.Unlock();
    return "";
    }
  if (contenttype!=NULL) out<<"Content-Type: "<<contenttype<<"\n\n";
  out.write(data,datasize);
  out.close();
  glob_lock.Unlock();
  return tempfname=strdup(temp);;
  }

//--------------------------------------------------

char *CMessagePart::GetData()
  {
  return data;
  }

//--------------------------------------------------

/* ENC is the basic 1 character encoding function to make a char printing */
#define ENC(c) (((c) & 077) + ' ')/*
/*
 * output one group of 3 bytes, pointed at by p, on file f.
 */
static void outenc(char *p, char *line, int& idx)
  {
  int c1, c2, c3, c4;
  
  c1 = *p >> 2;
  c2 = (*p << 4) & 060 | (p[1] >> 4) & 017;
  c3 = (p[1] << 2) & 074 | (p[2] >> 6) & 03;
  c4 = p[2] & 077;
  line[idx++]=ENC(c1);
  line[idx++]=ENC(c2);
  line[idx++]=ENC(c3);
  line[idx++]=ENC(c4);
  }

//--------------------------------------------------

/* copy from in to out, encoding as you go along.
 */
static bool SaveUUE(istream &in,  uuencode_callback proc, void *context)
  {
  char buf[80];
  char out[80];
  int i, n;
  int idx=0;
  
  for (;;) 
    {
    idx=0;
    /* 1 (up to) 45 character line */
    n=in.read(buf,45).gcount();
    out[idx++]=ENC(n);	//encode line length	
    for (i=0; i<n; i += 3) outenc(&buf[i], out, idx); //encode line
    out[idx]=0;	//append zero character
    if (proc(out,context)==false) return false; //send line to output
    if (n <= 0) //if it is end, exit;
    break;
    }
  return true;
  }

//--------------------------------------------------

bool uuencode(const char *filename, istream &data, uuencode_callback proc, void *context)
  {
  char buf[80];
  char *lom=const_cast<char *>(strrchr(filename,'\\'));
  if (lom==NULL) lom=(char *)filename;else lom++;
  _snprintf(buf,sizeof(buf),"begin 666 %s",lom);
  buf[79]=0;
  if (proc(buf,context)==false) return false;
  if (SaveUUE(data,proc,context)==false) return false;
  strcpy(buf,"end");
  if (proc(buf,context)==false) return false;
  return true;
  }

//--------------------------------------------------

void CMessagePart::Serialize(CGenArchive &arch)
  {
  arch.VarSerial(type);
  if (!IsValid()) return;
  arch.VarSerial(next);
  CString f=filename;
  arch.Serialize(f);
  if (!arch.IsStoring)
    {
    if (f=="") 
      {free(filename);filename=NULL;}
    else filename=strdup(f);	
    if (next) next=new CMessagePart();
    }
  arch.VarSerial(datasize);
  if (!arch.IsStoring)
    {
    free(data);
    data=(char *)malloc(datasize);
    }
  arch.Serialize(data,datasize);
  if (next) next->Serialize(arch);
  }

//--------------------------------------------------

void CMessagePart::RemoveFromCache()
  {
  abort();
  }

//--------------------------------------------------

struct SPartMsgHeader
  {
  CString content_type;
  CString boundary;
  CString charset;
  CString name;
  CString encoding;
  CString fullheader;
  };

//--------------------------------------------------

static void GetBeforeVar(istream& in, CString &out)
  {
  ws(in);
  int p=in.get();
  out="";
  while (p!=EOF && p!=';') 
    {
    out+=char(p);
    p=in.get();
    }
  if (p==';') p=in.get();  
  out.TrimLeft();
  out.TrimRight();
  }

//--------------------------------------------------

static bool GetHeaderVarible(istream &in, CString &out)
  {
  ws(in);
  int p=in.get();
  out="";
  while (p!=EOF && p!='=')
    {
    out+=char(p);
    p=in.get();
    }
  if (p==EOF) return false;
  out.TrimRight();
  }

//--------------------------------------------------

static bool GetHeaderVaribleValue(istream &in, CString &out)
  {
  ws(in);
  int p=in.get();
  out="";
  if (p!='"') return false;
  p=in.get();
  while (p!=EOF && p!='"')
    {
    out+=char(p);
    p=in.get();
    }
  if (p==EOF) return false;  
  while (p!=EOF && p!=';') p=in.get();
  return true;
  }

//--------------------------------------------------

/*
static bool LoadHeaderProc(CMailMessage *msg, const char *var, const char *value, void *context)
  {
   SPartMsgHeader *hdr=(SPartMsgHeader *)context;
  if (stricmp(var,"content-type")==0) 
	{
	strstream wrk(value,0,ios::in);
	GetBeforeVar(wrk,hdr->content_type);
	CString sname,svalue;
	while (GetHeaderVarible(wrk,sname)==true && GetHeaderVaribleValue(wrk,svalue)==true)
	  {
	  if (stricmp(sname,"boundary")==0) hdr->boundary=svalue;
	  if (stricmp(sname,"charset")==0) hdr->charset=svalue;
	  if (stricmp(sname,"name")==0) hdr->name=svalue;
	  }
	}
  if (stricmp(var,"Content-Transfer-Encoding")==0) hdr->encoding=value;
  return true;
  }

static bool LoadHeader(istream &in, SPartMsgHeader &hdr)
  {
  strstream work;
  bool emptyline=false;
  int znk=in.get();
  while (!emptyline && znk!='\n' && znk!=EOF) 
	{
	if (znk=='\n') emptyline=true;
	else if (znk>=32) emptyline=false;
	work<<znk;
	znk=in.get();
	}
  CMailMessage msg;
  msg.SetHeader(work.str());
  work.freeze(0);
  msg.EnumProperties(LoadHeaderProc,(void *))&hdr);
  return true;
  }

static bool FindNextBoundary(istream &in, const char *boundary)
  {
  char buff[256];
  while (in.eof()==false)
	{
	in.getline(buff,sizeof(buff));
	if (strcmp(buff,boundary)==0) return true;
	}  
  }

CMessagePart *CMessagePart::LoadMultipartMessage(istream &in, const char *content_type)
  {
  SPartMsgHeader hdr;
  if (content_type!=NULL)
	{
	LoadHeaderProc(NULL,"content-type",content_type,(void *)&hdr);
	if (hdr.boundary=="") return NULL;
	if (FindNextBoundary(in,hdr.boundary)==false) return NULL;
	}
  LoadHeader(in,hdr);
  if (hdr.content_type=="") return NULL;
  if (stricmp(hdr.
  }
*/
