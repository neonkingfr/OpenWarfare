#pragma once

#include <es/containers/array.hpp>

class IArchive;

class ThreadRule
{
  CString _ruleName;
  CString _ruleCond;
public:
  ThreadRule() {}
  ThreadRule(CString ruleName,CString ruleCond):_ruleName(ruleName),_ruleCond(ruleCond) {}

  const CString &GetName() const {return _ruleName;}
  const CString &GetCond() const {return _ruleCond;}

  void Streaming(IArchive &arch);

  ClassIsMovable(ThreadRule)
};

class ThreadRules
{
  AutoArray<ThreadRule> _rules;

  int _filter;
  int _highlight;
public:
  ThreadRules(void):_filter(-1),_highlight(-1) {}

  void AddRule(const ThreadRule &rule);
  void InsertRule(int pos,const ThreadRule &rule);
  void MoveUp(int pos);
  void MoveDown(int pos);
  void SetFilter(int id);
  int GetFilter() const;
  void SetHighlight(int id);
  int GetHighlight() const;
  int GetRuleIdMin() const {return 0;}
  int GetRuleIdMax() const {return _rules.Size()-1;}
  const ThreadRule &GetRule(int id) const {return _rules[id];}
  void RemoveRule(int id);
  void Streaming(IArchive &arch);
  void SetRule(int pos, const ThreadRule &rule);
  void LoadFromKwd(CString kwd, int curFilter, int curHighlight);
  int FindByName(const char *name);
};
