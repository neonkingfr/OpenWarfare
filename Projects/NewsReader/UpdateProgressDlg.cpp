// UpdateProgressDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "UpdateProgressDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUpdateProgressDlg dialog


CUpdateProgressDlg::CUpdateProgressDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUpdateProgressDlg::IDD, pParent)
{

	//{{AFX_DATA_INIT(CUpdateProgressDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CUpdateProgressDlg::DoDataExchange(CDataExchange* pDX)
{
stop=false;
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUpdateProgressDlg)
	DDX_Control(pDX, IDC_PROGRESS1, progress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUpdateProgressDlg, CDialog)
	//{{AFX_MSG_MAP(CUpdateProgressDlg)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUpdateProgressDlg message handlers

void CUpdateProgressDlg::OnCancel() 
{
  stop=true;
}

void CUpdateProgressDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	CRect rc;
	CWnd *wnd=GetDlgItem(IDC_STATUS);
	wnd->GetWindowRect(&rc);	
	ScreenToClient(&rc);
	CDC *dc=GetDC();
	anims.Draw(dc,cntr,CPoint(rc.left,rc.top),ILD_NORMAL);
	cntr++;
	if (cntr>=anims.GetImageCount()) cntr=0;
	CDialog::OnTimer(nIDEvent);
	ReleaseDC(dc);
}

BOOL CUpdateProgressDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	cntr=0;
	anims.Create(IDB_DOGANIM6,50,1,RGB(255,0,255));
	anims.SetBkColor(GetSysColor(COLOR_3DFACE));
	SetTimer(1,100,0);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUpdateProgressDlg::DoneDownloadNotify()
  {
  CString done;
  done.LoadString(IDS_UPDATEDONE);
  SetDlgItemText(IDC_STATUS,done);
  done.LoadString(IDS_RESTART);
  SetDlgItemText(IDCANCEL,done);
  BringWindowToTop();
  anims.DeleteImageList();
  anims.Create(IDB_DOGANIM5,50,1,RGB(255,0,255));
  MessageBeep(MB_ICONASTERISK);
  while (!stop) 
	{
	MSG msg;
	GetMessageA(&msg,0,0,0);
	DispatchMessage(&msg);
	}
  }
