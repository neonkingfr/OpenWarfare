#pragma once
#include <malloc.h>

class DiskBTreeDefIO
{
public:
  virtual size_t GetBlockGranuality()=0;
  virtual bool ReadBlock(int blockIndex, char *buffer)=0;
  virtual bool WriteBlock(int blockIndex, char *buffer)=0;
  virtual int GetNewBlockIndex()=0; //returns index for new block (doesn't create it - use write block to create it)
  virtual ~DiskBTreeDefIO()=0 {}
};

#define DSKBTERR_UNABLEREADHEADER -1
#define DSKBTERR_UNABLEREADBLOCK  -2
#define DSKBTERR_UNABLEWRITEBLOCK  -3

struct DiskBTreeEnum
{
  unsigned long blockId;
  unsigned long itemId;
  SRef<char> cachedBlock;
  DiskBTreeEnum():blockId(0),itemId(),cachedBlock(NULL) {}  
};

template <class T, class DiskIO=DiskBTreeDefIO>
class DiskBTree
{
  DiskIO *_iohandle;  //handler for i/o
  size_t _blockSz;   //size for block  

  struct FileHeader
  {
    char mark[8];   //user defined mark
    unsigned long version; //user defined version
    unsigned long firstBlock; //pointer to first block
    char unused[1];   //unused data until end of block;
  };

  struct DataItem
  {
    T data;           //data in tree
    unsigned long subLevel; //pointer to subLevel of next item
  };

  struct DataBlock
  {
    unsigned long numItems; //count of items in block;
    unsigned long parentLevel;  //block index of parent level 
    unsigned long subLevel;   //block index of first level (or null)
    DataItem items[1];      //items until end of block;
  };

  unsigned long _maxItemsInBlock; //maximum items in block 
  FileHeader *_header;
  int _error;  


public:
  DiskBTree(DiskIO *iohandle)
  {
    _iohandle=iohandle;
    _error=0;
    _blockSz=iohandle->GetBlockGranuality();    
    _maxItemsInBlock=(_blockSz-sizeof(DataBlock))/sizeof(DataItem)+1;        
    _header=(FileHeader *)new char[_blockSz];
    if (_iohandle->ReadBlock(0,(char *)_header)==false) _error=DSKBTERR_UNABLEREADHEADER;
  }

  ~DiskBTree()
  {
    delete [] (char *)_header;
  }

  bool CreateHeader(char *mark, unsigned long version)
  {
    strncpy(_header->mark,mark,8);
    _header->version=version;
    _header->firstBlock=0;
    _error=0;
    return _iohandle->WriteBlock(0,(char *)_header);
  }

  void Add(const T &item);
  bool Find(const T &key, T *result=NULL);
  DiskBTreeEnum BeginSearch(const T &key);
  bool GetNext(DiskBTreeEnum& enumerator, T *result);
  int GetError() {return _error;}


private:

  unsigned long _openedLevel;
  unsigned long *_openedLevelParent;
  bool SearchSubLevel(unsigned long levelId,const T &key, T *result, DiskBTreeEnum *enumer=NULL);
  unsigned long SearchLevel (const T &key, DataBlock *block, int &cmpres);
  unsigned long AddToLevel(unsigned long levelId, unsigned long itemId, const T &key, unsigned long subLevelInfo=0, DataBlock *prealloc=NULL);
  T *DivideIntoTwo(DataBlock *vychozi, DataBlock *druhy);
  bool UpdateParents(DataBlock *block, unsigned long blockId);
};

