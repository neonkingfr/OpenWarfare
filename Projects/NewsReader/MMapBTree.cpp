// MMapBTree.cpp: implementation of the MMapBTree class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "MMapBTree.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MMapBTree::MMapBTree()
{
  _base=0;
  _curSize=0;
  _hFile=0;
  _hMem=0;  
}

MMapBTree::~MMapBTree()
{
  Close();
}

void MMapBTree::UnmapFile()
{ 
  if (_base) {UnmapViewOfFile(_base);_base=0;}
  if (_hMem) {CloseHandle(_hMem);_hMem=0;}
}

bool MMapBTree::MapFile(unsigned long newsize)
  {
  UnmapFile();
  _hMem=CreateFileMapping(_hFile,NULL,PAGE_READWRITE,0,newsize,NULL);
  if (_hMem==NULL) return false;
  _base=(DataBlock *)MapViewOfFile(_hMem,FILE_MAP_ALL_ACCESS,0,0,0);
  if (_base==NULL) return false;
  _header=(Header *)_base;
  _curSize=newsize;
  return true;
  }

unsigned long MMapBTree::AllocNewBlock()
{
  unsigned long blockId=_header->nextBlock;
  unsigned long expand=(blockId+1)*sizeof(DataBlock);
  if (expand>_curSize)
  {
    if (MapFile(_curSize+(_curSize/4096/2+1)*4096)==false) return 0;
  }
  _header->nextBlock++;
  return blockId;
}

unsigned long MMapBTree::FindItemInLevel(const MMapBTreeBlockItem  &item, unsigned long level, char *cmpres)
{
  //simplified
  DataBlock *block=GetBlock(level);
  char cmp=1; //when no item in list, it is same as bigger item in fist position
  unsigned long i;
  for (i=0;i<block->numItems;i++)
  {    
    cmp=(block->items[i]>item)-(block->items[i]<item);    
    if (cmp>=0) 
    {
      if (cmpres) *cmpres=cmp;
      return i;
    }
  }
  if (cmpres) *cmpres=cmp;  
  return i;
}

bool MMapBTree::Find(const MMapBTreeBlockItem  &item, MMapBTreeEnum &iter)
{  
  iter.level=0;
  if (_header->firstItem==0) return false;
  unsigned long level=_header->firstItem;
  while (iter.level<MMAPBTREEMAXSIZE)
  {
    char cmpres;
    unsigned long pos=FindItemInLevel(item,level,&cmpres);
    iter.itemPos[iter.level]=(char)pos;
    iter.blockPath[iter.level]=level;
    iter.level++;
    DataBlock *block=GetBlock(level);
    if (block->leftSubTree==0) return cmpres==0;
    if (cmpres>0)
    {
      if (pos==0) level=block->leftSubTree;
      else level=block->subTree[pos-1];
    }
    else 
      if (cmpres==0) 
      {
        level=block->subTree[pos];
        iter.itemPos[iter.level-1]++; //this pointer needs to point to next block we read
      } 
    else
      level=block->subTree[pos-1];
  }
  ASSERT(false); //too many levels
  return false;
}

bool MMapBTree::Next(MMapBTreeEnum &iter, MMapBTreeBlockItem  &value)
{
  if (iter.level<0) return false;
  unsigned long curlevel=iter.level-1;
  DataBlock *block=GetBlock(iter.blockPath[curlevel]);
  while (iter.level>0 && iter.itemPos[curlevel]>=block->numItems)
  {
	iter.level--;
	curlevel=iter.level-1;
	block=GetBlock(iter.blockPath[curlevel]);	  
  }
  if (iter.level==0) return false;
  value=block->items[iter.itemPos[curlevel]];
  if (block->leftSubTree) 
    {      	  
      iter.blockPath[iter.level]=block->subTree[iter.itemPos[curlevel]];
      iter.itemPos[iter.level]=0;
      block=GetBlock(iter.blockPath[iter.level]);
      iter.itemPos[curlevel]++;
      iter.level++;
      while (block->leftSubTree && iter.level<MMAPBTREEMAXSIZE)
      {
        iter.blockPath[iter.level]=block->leftSubTree;
        iter.itemPos[iter.level]=0;
        iter.level++;
        block=GetBlock(block->leftSubTree);
      }
    }  
  else
   iter.itemPos[curlevel]++;
  return true;
}

bool MMapBTree::Add(const MMapBTreeBlockItem &value)
{
  if (_header->firstItem==0)
      _header->firstItem=AllocNewBlock();    
  MMapBTreeEnum iter;
  if (Find(value,iter)==true) return false;
  unsigned long curlevel=iter.level-1;
  DataBlock *block=GetBlock(iter.blockPath[curlevel]);
  if (block->numItems>=MMAPBTREEBLOCKLEAF)
  {
    Divide(iter,curlevel);
    block=GetBlock(iter.blockPath[curlevel]);
  }
  unsigned long zarazka=iter.itemPos[curlevel];
  for (unsigned long i=block->numItems;i>zarazka;--i)
    block->items[i]=block->items[i-1];
  block->items[zarazka]=value;
  block->numItems++;
  return true;
}

void MMapBTree::Divide(MMapBTreeEnum &iter, unsigned long curlevel)
{
  unsigned long levelid=iter.blockPath[curlevel];
  unsigned long newlevel=AllocNewBlock(); //middle item level id
  DataBlock *block=GetBlock(levelid);
  DataBlock *blck2nd=GetBlock(newlevel);
  const MMapBTreeBlockItem *middle=NULL;       //middle item id
  unsigned long center=block->numItems/2;
  for (unsigned long i=center+1;i<block->numItems;i++)
  {
	unsigned long newps=i-center-1;
	blck2nd->items[newps]=block->items[i];
	if (block->leftSubTree) blck2nd->subTree[newps]=block->subTree[i];
  }
  blck2nd->numItems=block->numItems-center-1;
  block->numItems=center;  
  if (block->leftSubTree) blck2nd->leftSubTree=block->subTree[center];else blck2nd->leftSubTree=0;
  if (iter.itemPos[curlevel]>center)
  {
	iter.blockPath[curlevel]=newlevel;
	iter.itemPos[curlevel]-=(char)(center+1);
  }
  if (curlevel==0)
  {
	unsigned long parentId=AllocNewBlock();
	block=GetBlock(levelid);
	middle=block->items+center;
	DataBlock *parent=GetBlock(parentId);
	parent->leftSubTree=levelid;
	parent->subTree[0]=newlevel;
	parent->numItems=1;
	parent->items[0]=*middle;
	_header->firstItem=parentId;
  }
  else
  {
	curlevel--;
	DataBlock *parent=GetBlock(iter.blockPath[curlevel]);
	if (parent->numItems>=MMAPBTREEBLOCKITEMS)
	{
	  Divide(iter,curlevel);
	  parent=GetBlock(iter.blockPath[curlevel]);
	}
    unsigned long zarazka=iter.itemPos[curlevel];
    for (unsigned long i=parent->numItems;i>zarazka;--i)
	{
	  parent->items[i]=parent->items[i-1];
	  parent->subTree[i]=parent->subTree[i-1];
	}
	block=GetBlock(levelid);
	middle=block->items+center;
    parent->items[zarazka]=*middle;
	parent->subTree[zarazka]=newlevel;
    parent->numItems++;
  }
}

bool MMapBTree::Create(const char *filename)
{
   TRACE1("FULLTEXT: Openning incidence database %s\n",filename);
  HANDLE _mapfile=CreateFile(filename,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_ALWAYS,
    FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS,NULL);
  if (_mapfile==INVALID_HANDLE_VALUE) return false;  
  _hFile=_mapfile;
  _curSize=GetFileSize(_hFile,NULL);
  if (_curSize==0)
  {
    MapFile(4*1024);
    _header->firstItem=0;
    _header->nextBlock=1;
  }
  else
    MapFile(_curSize);
  return true;
} 