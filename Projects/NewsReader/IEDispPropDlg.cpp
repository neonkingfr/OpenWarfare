// IEDispPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "IEDispPropDlg.h"
#include "htmldocument2.h"
#include "htmlelement.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define GROUP "Config.Font"
#define FACE "Face"
#define SIZE "Size"
#define BOLD "Bold"
#define ITALIC "Italic"
#define BGCOLEN "Enable BgColor"
#define FGCOLEN "Enable FgColor"
#define LNCOLEN "Enable LnColor"
#define BGCOLOR "BgColor"
#define FGCOLOR "FgColor"
#define LNCOLOR "LnColor"

/////////////////////////////////////////////////////////////////////////////
// CIEDispPropDlg dialog


CIEDispPropDlg::CIEDispPropDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIEDispPropDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIEDispPropDlg)
	bold = FALSE;
	en_bgcolor = FALSE;
	italic = FALSE;
	font = _T("");
	en_linkcol = FALSE;
	size = _T("");
	en_texcolor = FALSE;	
	//}}AFX_DATA_INIT
}


void CIEDispPropDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIEDispPropDlg)
	DDX_Control(pDX, IDC_COLBUTT3, bColLink);
	DDX_Control(pDX, IDC_COLBUTT2, bColBg);
	DDX_Control(pDX, IDC_COLBUTT1, bColText);
	DDX_Control(pDX, IDC_FSIZE, wSize);
	DDX_Control(pDX, IDC_FONT, wFont);
	DDX_Check(pDX, IDC_BBOLD, bold);
	DDX_Check(pDX, IDC_BGCOL, en_bgcolor);
	DDX_Check(pDX, IDC_BITALIC, italic);
	DDX_LBString(pDX, IDC_FONT, font);
	DDX_Check(pDX, IDC_LINKCOLOR, en_linkcol);
	DDX_LBString(pDX, IDC_FSIZE, size);
	DDX_Check(pDX, IDC_TEXTCOL, en_texcolor);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIEDispPropDlg, CDialog)
	//{{AFX_MSG_MAP(CIEDispPropDlg)
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_TEXTCOL, OnRules)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_COLBUTT1, OnColor)
	ON_LBN_SELCHANGE(IDC_FONT, OnPreview)
	ON_BN_CLICKED(IDC_LINKCOLOR, OnRules)
	ON_BN_CLICKED(IDC_BGCOL, OnRules)
	ON_BN_CLICKED(IDC_COLBUTT2, OnColor)
	ON_BN_CLICKED(IDC_COLBUTT3, OnColor)
	ON_LBN_SELCHANGE(IDC_FSIZE, OnPreview)
	ON_BN_CLICKED(IDC_BBOLD, OnPreview)
	ON_BN_CLICKED(IDC_BITALIC, OnPreview)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIEDispPropDlg message handlers
#include "Windowsx.h"

static int CALLBACK EnumFontsProc(
  LOGFONT *lplf,     // pointer to logical-font data
  TEXTMETRIC *lptm,     // pointer to physical-font data
  DWORD dwType,  // font type
  LPARAM lpData )
  {
  HWND hWnd=(HWND)lpData;
  if (IsWindow(hWnd))
	{
	int q=ListBox_FindString(hWnd,-1,lplf->lfFaceName);
	if (q==LB_ERR)	  
	  ListBox_AddString(hWnd,lplf->lfFaceName);	  
	return 1;
	}
  else 
	return 0;
  }

static UINT LoadFontsThread(LPVOID list)
  {  
  HDC hdc=CreateDC("DISPLAY",NULL,NULL,NULL);
  EnumFonts(hdc,NULL,(FONTENUMPROC)EnumFontsProc,(LPARAM)list);
  return 0;
  }

BOOL CIEDispPropDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CRect rc;
	GetDlgItem(IDC_PREVIEW)->GetWindowRect(&rc);
	ScreenToClient(&rc);
	preview.Create(NULL,"",WS_CHILD|WS_VISIBLE,rc,this,0,NULL);
	AfxBeginThread(LoadFontsThread,::GetDlgItem(*this,IDC_FONT),THREAD_PRIORITY_NORMAL,16384);
	char buff[20]="  ";	
	for (int i=1;i<8;i++)	  
	  wSize.AddString(itoa(i,buff+2,10));
	bColText.SetWindowText(itoa(textcol,buff,16));
	bColBg.SetWindowText(itoa(bgcol,buff,16));
	bColLink.SetWindowText(itoa(linkcol,buff,16));
	UpdateData(FALSE);
	OnRules();
	SetTimer(100,500,NULL);
	preview.Navigate("about:blank",NULL,NULL,NULL,NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CIEDispPropDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
  {
	// TODO: Add your message handler code here and/or call default
	CButton *bt=(CButton *)CButton::FromHandle(lpDrawItemStruct->hwndItem);
	CDC dc;dc.Attach(lpDrawItemStruct->hDC);
	if (bt->IsWindowEnabled())
	  {
	  CBrush br,*old;
	  char buff[20];
	  bt->GetWindowText(buff,20);
	  COLORREF col;
	  sscanf(buff,"%X",&col);
	  br.CreateSolidBrush(col);
	  old=dc.SelectObject(&br);
	  dc.SetBkColor(col);
	  if (lpDrawItemStruct->itemState & ODS_SELECTED)		
		dc.DrawEdge(&lpDrawItemStruct->rcItem,EDGE_RAISED,BF_ADJUST|BF_RECT|BF_FLAT);		
	  else
		dc.DrawEdge(&lpDrawItemStruct->rcItem,EDGE_RAISED,BF_RECT);		
	  CRect rc(lpDrawItemStruct->rcItem);
	  rc.top+=3;rc.bottom-=3;
	  rc.left+=3;rc.right-=3;
	  dc.FillRect(&rc,&br);
	  dc.SelectObject(old);
	  }
	else
	  dc.FillSolidRect(&lpDrawItemStruct->rcItem,GetSysColor(COLOR_BTNFACE));
  dc.Detach();
  }

void CIEDispPropDlg::OnRules() 
  {
  bColBg.EnableWindow(IsDlgButtonChecked(IDC_BGCOL)!=0);
  bColText.EnableWindow(IsDlgButtonChecked(IDC_TEXTCOL)!=0);
  bColLink.EnableWindow(IsDlgButtonChecked(IDC_LINKCOLOR)!=0);
  OnPreview();
  }

void CIEDispPropDlg::OnTimer(UINT nIDEvent) 
  {
  if (nIDEvent==100)
	{
	if (font!="") 
	  {
	  if (wFont.SelectString(-1,font)==LB_ERR) return;
	  OnDrawPreview();
	  }
	KillTimer(100);
	}
  if (nIDEvent==50)
	OnDrawPreview();
  CDialog::OnTimer(nIDEvent);
  }

void CIEDispPropDlg::OnColor() 
  {
  CButton *btm=(CButton *)GetFocus();
  LONG col=GetColorFromButton(*btm);
  CColorDialog cdlg(col,CC_ANYCOLOR|CC_RGBINIT);
  cdlg.m_cc.lpCustColors=theApp.config.usercolors;
  if (cdlg.DoModal()==IDOK)
	{
	char buff[20];
	col=cdlg.GetColor();
	btm->SetWindowText(itoa(col,buff,16));
	OnPreview();
	}
  }

void CIEDispPropDlg::OnPreview() 
  {
  SetTimer(50,250,NULL);
  preview.Navigate("about:blank",NULL,NULL,NULL,NULL);
  }

LONG CIEDispPropDlg::GetColorFromButton(CButton &butt)
  {
  char buff[20];
  butt.GetWindowText(buff,20);
  LONG col;
  sscanf(buff,"%x",&col);
  return col;
  } 

void CIEDispPropDlg::OnDrawPreview()
  {
  CHTMLDocument2 doc=preview.GetHtmlDocument();
  CHTMLElement elm=doc.GetBody();
  if (IsDlgButtonChecked(IDC_BGCOL)) doc.SetBgColor(GetColorFromButton(bColBg));
  if (IsDlgButtonChecked(IDC_TEXTCOL)) doc.SetFgColor(GetColorFromButton(bColText));
  if (IsDlgButtonChecked(IDC_LINKCOLOR)) 
	{
	doc.SetLinkColor(GetColorFromButton(bColLink));
	doc.SetVlinkColor(GetColorFromButton(bColLink));
	doc.SetAlinkColor(~GetColorFromButton(bColLink));
	}
  CString beg,end;
  CreateFontBeginSeq(beg,true);
  CreateFontEndSeq(end,true);
  elm.SetInnerHTML("<center>"+beg+"Normal<br><a href=\"about:blank\">Link</a>"+end+"</center>");
  KillTimer(50);
  }

void CIEDispPropDlg::CreateFontBeginSeq(CString &beg,bool fromdlg)
  {
  char *face;
  char *size;
  if (fromdlg)
	{	
	face=(char *)alloca(wFont.GetTextLen(wFont.GetCurSel())+1);
	wFont.GetText(wFont.GetCurSel(),face);
	size=(char *)alloca(wSize.GetTextLen(wSize.GetCurSel())+1);
	wSize.GetText(wSize.GetCurSel(),size);
	}
  else
	{
	face=(char *)((LPCTSTR)font);
	size=(char *)((LPCTSTR)this->size);
	}
  beg.Format("<FONT face=\"%s\" size=\"%s\">",face,size);
  BOOL p;
  if (fromdlg) p=IsDlgButtonChecked(IDC_BBOLD);else p=bold;
  if (p) beg+="<B>";
  if (fromdlg) p=IsDlgButtonChecked(IDC_BITALIC);else p=italic;
  if (p) beg+="<I>";
  }

void CIEDispPropDlg::CreateFontEndSeq(CString &end, bool fromdlg)
  {
  BOOL p;
  if (fromdlg) p=IsDlgButtonChecked(IDC_BITALIC);else p=italic;
  if (p) end+="</I>";
  if (fromdlg) p=IsDlgButtonChecked(IDC_BBOLD);else p=bold;
  if (p) end+="</B>";
  end+="</FONT>";
  }

void CIEDispPropDlg::OnOK() 
  {
  textcol=GetColorFromButton(bColText);
  bgcol=GetColorFromButton(bColBg);
  linkcol=GetColorFromButton(bColLink);
  CDialog::OnOK();
  SaveSettings();
  }

void CIEDispPropDlg::SaveSettings()
  {
  theApp.WriteProfileString(GROUP,FACE,font);
  theApp.WriteProfileString(GROUP,SIZE,size);
  theApp.WriteProfileInt(GROUP,BOLD,bold);
  theApp.WriteProfileInt(GROUP,ITALIC,italic);
  theApp.WriteProfileInt(GROUP,BGCOLEN,en_bgcolor);
  theApp.WriteProfileInt(GROUP,FGCOLEN,en_texcolor);
  theApp.WriteProfileInt(GROUP,LNCOLEN,en_linkcol);
  theApp.WriteProfileInt(GROUP,BGCOLOR,bgcol);
  theApp.WriteProfileInt(GROUP,FGCOLOR,textcol);
  theApp.WriteProfileInt(GROUP,LNCOLOR,linkcol);
  }
  

void CIEDispPropDlg::LoadSettings()
  {
  font=theApp.GetProfileString(GROUP,FACE,0);
  size=theApp.GetProfileString(GROUP,SIZE,0);
  bold=theApp.GetProfileInt(GROUP,BOLD,0);
  italic=theApp.GetProfileInt(GROUP,ITALIC,0);
  en_bgcolor=theApp.GetProfileInt(GROUP,BGCOLEN,0);
  en_texcolor=theApp.GetProfileInt(GROUP,FGCOLEN,0);
  en_linkcol=theApp.GetProfileInt(GROUP,LNCOLEN,0);
  bgcol=theApp.GetProfileInt(GROUP,BGCOLOR,0);
  textcol=theApp.GetProfileInt(GROUP,FGCOLOR,0);
  linkcol=theApp.GetProfileInt(GROUP,LNCOLOR,0);
  }
