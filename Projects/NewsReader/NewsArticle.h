// NewsArticle.h: interface for the CNewsArticle class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NEWSARTICLE_H__12142625_A338_411B_BF0A_2EED608ACA60__INCLUDED_)
#define AFX_NEWSARTICLE_H__12142625_A338_411B_BF0A_2EED608ACA60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MailList.h"
#include "MessCache.h"
#include "iTextFind.h"
//#include "iTextFind.h"
//#include "textFind.h"
#include <Es/essencePch.hpp>
#include <Es/Types/pointers.hpp>
#include <Es/Strings/rstring.hpp>

#include "ReadWriteLock.h"
#include "MessagePart.h"
#include <el/MultiThread/Interlocked.h>


class IArchive;

#define ARTFROM "From"
#define ARTSUBJECT "Subject"
#define ARTMESSID "Message-ID"
#define ARTREFID "References"
#define ARTDATE "Date"
#define ARTICON "Icon"
#define ARTLINES "Lines"
#define ARTCONTTYPE "Content-Type"
#define ARTGROUPS "NewsGroups"
#define ARTXCANCEL "X-Cancel"
#define ARTXWIKI "X-Wiki"

enum EArticleState 
  {EAImportant=1,EANormal=0, EAHidden=-1 };

//--------------------------------------------------

class INewsArticleEnumCallback
{
public:
  virtual void HdrResult(const char *command, const char *data)=0;
};

class CFindList;
class CNewsArticle 
  {
    CString _text;
    CString _header;
  int ref;
  public:
    void Serialize(CGenArchive &arch);
    int DownloadArticle(int &index, CNewsTerminal &term, bool mode);
    CNewsArticle *AddRef() 
      {ref++;return this;}
    void Release() 
      {if (this) if (ref==1) delete this;else ref--;}
    CNewsArticle();
    virtual ~CNewsArticle();	

    void SetText(const char *text) {_text=text;}
    void SetHeader(const char *header) {_header=header;}    
    const char *GetText() const {return _text;}
    const char *GetHeader() const {return _header;}

    void EnumHeader(INewsArticleEnumCallback *callback);
  };

//--------------------------------------------------

class CNewsArtHolder;
class CNessCache;
class CFindList;

extern CReadWriteLock articleLock;

struct AverageCalc
  {
  float sum;
  float samples;
  
  AverageCalc()
    {
    sum = 0;
    samples = 0;
    }
  AverageCalc(float x)
    {
    sum = x;
    samples = 1;
    }
  operator float () const
    {
    if (samples<=0) return 100;
    return sum/samples;
    }
  bool operator > (float x) const
    {
    if (samples<=0) return 100>x;
    return sum>x*samples;
    }
  void operator += (float x)
    {
    sum += x;
    samples += 1;
    }
  };

//--------------------------------------------------

struct GRPFINDINFO
  {
  //Requied data
  SRef<ITextFind> from;
  SRef<ITextFind> subject;
  SRef<ITextFind> message;
  COleDateTime before;
  COleDateTime after;
  bool en_before;
  bool en_after;
  bool attach;
  bool booleanSearch; //! use boolean search
  bool substrings; //! use word similiraty operator
  float maxAcceptableMatch; //! max.acceptable difference to score a match
  bool (*FndCallback)(GRPFINDINFO& fnd); //called in cycle
  CMessCache *cache; //set is to NULL, if account has no cache
  
  //Context data (don't set)
  CNewsGroup *grp;
  CNewsArtHolder *fifo[33];
  AverageCalc score[33]; // score corresponding to FIFO
  
  int index;
  int maxfifo;
  //Callback info data (valid in callback function)
  void *context; //context pointer sets by user application
  int msgindex; //index of message has been processed (for progressbar)
  CFindList *firstfnd;  //prvni nalezeny prvek (prvky jsou razeny do seznamu)
  GRPFINDINFO() 
    {firstfnd=0;memset(fifo,0,sizeof(fifo));index=0;maxfifo=theApp.config.acc_cmd;FndCallback=NULL;cache=NULL;}
  };

//--------------------------------------------------

#define NHATT_NONE 0
#define NHATT_ISATTACH 2
#define NHATT_MAYBE 1
#define NHATT_ARCHIVE 3

#include "MessagePart.h" 
class CNewsArtHolder
  {
  bool opened:1; //!<article childs is opened
  bool trash:1; //!<article is marked as deleted
  bool hidden:1; //!<article cannot be seen
  bool highlighted:1; //!< article is highlighted (as a result of filtering)
  bool lgwarn:1; //!<article can be downloaded, if it is too long
  bool ntnew:1; //!<notify new;
  bool dwnnew:1; //!<new info for download
  bool hidtoshow:1; //!<if true, hidden thread will be showen grayed
  bool unread:1; //!<article is unread
  bool subchanged:1;  //!<article and subarticles changed (for thread only)
  long refcnt;  //!<reference counter
  int index;  //!<article index in group
  unsigned char level;  //!<level of article in tree  
  char incache; //!<1 if article is in disk cache, 0 if not and -1 if don't know
  short chldnew; //!<count of new articles in child thread
  CNewsArtHolder *next,*child,*parent; //!<tree pointes
  CNewsArtHolder *createArchive(CNewsArtHolder *hp);
  struct CALCHIERARCHYSTRUCT
    {
    CNewsArtHolder *parent;
    CNewsGroup     *group;
    char *keyword;
    int curlevel;
    bool close;
    bool hidflag;    
    //CTextFind tfnd;	
    };
  void CalcHierarchy2(CALCHIERARCHYSTRUCT *info);
  void ChangeNwCount(int ref) 
    {
    chldnew+=ref;
    if (chldnew<0) chldnew=0;
    if (parent) parent->ChangeNwCount(ref);
    subchanged=true;
    } //works with childnew

  public:
	  void Streaming(IArchive &arch, int minindex);
	  static CNewsArtHolder * Streaming(CNewsArtHolder *top, IArchive &arch, int minindex);
    bool CalcHierarchy2(CNewsGroup *grp=NULL,const char *kbdlist=NULL, int setindex=0, bool close=FALSE);
    void DeleteOlder(COleDateTime& dtm);
    CNewsArtHolder * Archivate(COleDateTime archtime);
    CNewsArtHolder * EnumNext() 
      {if (child) return child;else return EnumNextNoChild();}
    CNewsArtHolder * EnumNextNoChild();
    CNewsArtHolder *Sort(int by); //returns first item in list
    static int CompareItems(CNewsArtHolder *left, CNewsArtHolder *right, int what);
    static const char *keywords; //obsolete
    void RemoveFromCache() {Reset();}
    bool LoadFromCache(CMessCache& chc, unsigned long forceowner=0);
    bool SaveToCache(CMessCache& chc, unsigned long forceowner=0);
    //		void RecursiveLoadAndSearch(GRPFINDINFO &fnd, CNewsTerminal &term, char mode); OBSOLETTE
    void CatchUp();
    EArticleState state; //article state
    int ReloadArticle(CNewsTerminal &term,CNewsGroup *grp, bool nocommands, unsigned long forceowner=0);
    //  bool CalcHierarchy(bool rebuild=false,int level=0, CNewsArtHolder *parent=NULL);
    int GetNewCount() const;
    int GetHighlightedCount() const;
    CNewsArtHolder * RebuildRemoveOld(int minindex);
    void Serialize(CGenArchive& arch,int minindex);
    void AddRef() 
      {MultiThreadSafe::MTIncrement(&refcnt);}
    void Release()
      {
      if (MultiThreadSafe::MTDecrement(&refcnt)==0)
        {
        if (child) 
          {child->Release();child=NULL;}
        while (next) 
          {
          CNewsArtHolder *grp=next;
          next=grp->next;
          grp->Unlink();	  
          grp->Release();
          }	
        if (next) next->Release();
        next=NULL;
        delete this;
        }
      }
    bool IsArchive() 
      {return attachment==NHATT_ARCHIVE;}
    CNewsArtHolder * FindMessage(const CString &msid, bool deleted=false);
    CNewsArtHolder * FindMessage(int index);
    CString messid; //!<message ID
    CString refid;  //!<reference ID in thread (message ID of original message)
    CString subject; //!<subject
    CString from;  //!<email address and name
    CString icon;  //!<icon address
    CString content; //!<contenttype;
    CString Xcancel; //!<contain message to cancel - it valid only during synchronization. Otherwise is empty
    CString XWikiLink; //!<Link to wiki page - for the thread
    COleDateTime date; //!<post date
    COleDateTime rdate; //!<last read date
    COleDateTime newestdate; //!<date of newest article in thread
    CNewsGroup *group;    //pointer to group that article belongs to.
    int sortpos; //!<position for sorting - stabilize sorting
    unsigned short lines;   //!<total count of lines (reported in header)
    char attachment; //!<attachment status   
    bool indexed;   //article body is indexed (not saved, but restored everytime)
    CString GetSubject();
    void RescanHeader(CNewsArticle *art); //!<Scans header of article.
    CNewsArtHolder()
      :next(0),child(0),parent(0),level(0),
    unread(true),highlighted(false),opened(true),trash(false),state(EANormal),
    hidden(false),lgwarn(false),refcnt(1),ntnew(1),chldnew(0),dwnnew(true),hidtoshow(false),subchanged(true)
      {  rdate=COleDateTime::GetCurrentTime();incache=-1;indexed=false;group=NULL;}
    CNewsArtHolder(int index)
      : next(0),child(0),parent(0),index(index),level(0),
    unread(true),highlighted(false),opened(true),trash(false),state(EANormal),subchanged(true),
    hidden(false),lgwarn(false),refcnt(1),ntnew(1),chldnew(0),dwnnew(true),hidtoshow(false)
      {  rdate=COleDateTime::GetCurrentTime();incache=-1;indexed=false;group=NULL;}

    CNewsArtHolder(const CNewsArtHolder &other,bool tracenext=false,CNewsArtHolder  *ptrparent=NULL);

    const CString &GetWikiLink() const
    {
      if (parent) return parent->GetWikiLink();
      else return XWikiLink;
    }

    virtual ~CNewsArtHolder() 
      {      
      if (child) child->Release();
      while (next) 
        {
        CNewsArtHolder *grp=next;
        next=grp->next;
        grp->Unlink();	  
        grp->Release();
        }	
      if (next) next->Release();
      }
    void UnlinkChild() 
      {child=NULL;}
    
    CNewsArtHolder *Unlink() 
      {
      /*			CNewsArtHolder *temp;       //OBSOLETE
			if (root==this) {next=NULL;return next;}
			if (parent!=NULL) 
			{
				if (parent->child==this) 
				{
					parent->child=next;next=NULL;return root;
				}
				temp=parent->child;
			}
			else
				temp=root;
			while (temp && temp->next!=this) temp=temp->next;
			if (temp) 
			{
				temp->next=next;
			}
			next=NULL;
			return root;*/
      CNewsArtHolder *nn=next;
      next=NULL;
      return nn;
      }
    void SetNext(CNewsArtHolder *hh) 
      {if (next) next->Release();next=hh;}
    void SetChild(CNewsArtHolder *hh) 
      {if (child) child->Release();child=hh;}
    void SetParent(CNewsArtHolder *hh) 
      {parent=hh;level=hh->level+1;}
    void InsertNext(CNewsArtHolder *nn)
      {
      nn->SetNext(next);
      next=nn;
      nn->parent=parent;
      }
    void InsertChild(CNewsArtHolder *nn)
      {
      nn->SetNext(child);
      child=nn;
      nn->parent=this;
      }
    PMessagePart GetArticle(unsigned long forceowner=0) {return memcache.LockObject(forceowner?forceowner:MEMCACHEOWNER(this));}
    CNewsArtHolder *GetNext() 
      {return next;}
    CNewsArtHolder *GetChild() 
      {return child;}
    CNewsArtHolder *GetParent() 
      {return parent;}
    bool GetUnread() const 
      {return unread;}
    void MarkAsRead(int section, bool set=true) 
      {
      if (unread && set) 
        {unread=false;if (parent) parent->ChangeNwCount(-1);}
      else if (!unread && !set) 
        {unread=true;if (parent) parent->ChangeNwCount(1);}
	  LogMarkAsRead(messid, !set, section);
      }
    int GetChildNew() const 
      {return chldnew;}
    int GetIndex() const 
      {return index;}
    void SetIndex(int index) {this->index=index;}
    void OpenClose() 
      {opened=!opened;}
    bool GetOpened() const 
      {return opened;}
    bool GetHidden() const 
      {return hidden;}
    void SetHidden(bool state) 
      {hidden=state;}
    bool GetHighlighted() const 
      {return highlighted;}
    void SetHighlighted(bool state) 
      {highlighted=state;}
    int GetLevel() 
      {return level;}  
    void Reset() 
      {memcache.Reset(MEMCACHEOWNER(this));}
    void SetTrash(bool set=true) 
      {trash=set;unread=false;}
    bool IsTrash() const 
      {return trash;}
    bool GetLgWarn() const 
      {return !lgwarn;}
    void KillLgWarn() 
      {lgwarn=true;}
    bool GetNtNew() 
      {bool ret=ntnew;ntnew=false;return ret;}
    bool GetDwnNew() 
      {bool ret=dwnnew;return ret;}
    
    CString GetLocalProperties(bool virtual_properties);
    CString GetThreadProperties(bool virtual_properties, bool *localProp=NULL);
    void HighlightThread(bool state);
    void HighlightSubthread(bool state);
    
    void HideThread(bool state);
    void HideSubthread(bool state);
    void DeleteThread();
    void DeleteSubthread();
    
    void GrayThread(bool gr)
      {
      hidtoshow=gr;
      if (parent) parent->GrayThread(gr);
      }
    bool IsGrayed() 
      {return hidtoshow;}
    
    void Search(GRPFINDINFO &fnd, CNewsTerminal &term);
    void LoadArticle(CNewsArticle *art,  unsigned long forceowner=0);
    
    char IsInCache()  //1 - incache, 0 - notincache, -1 - don't know
      {return incache;}
    void SetInCache(char val) {incache=val;}
    void DropFromCache() 
      {incache=false;}

    bool Changed(bool set) {bool res=subchanged;if (set) subchanged=false;return res;}
    void StateChange() {if (parent) parent->StateChange();subchanged=true;}

    void ParentPropagate();
  };

//--------------------------------------------------

#endif // !defined(AFX_NEWSARTICLE_H__12142625_A338_411B_BF0A_2EED608ACA60__INCLUDED_)
