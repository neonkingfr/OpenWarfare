// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__55E680BE_242D_48EA_9F8B_22F29FB71B75__INCLUDED_)
#define AFX_MAINFRM_H__55E680BE_242D_48EA_9F8B_22F29FB71B75__INCLUDED_

#include "NRNotify.h"	// Added by ClassView
#include "AccountList.h"	// Added by ClassView
#include "SplitWindow.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ChildView.h"
#include "MessageList.h"
#include "GroupDlg.h"
#include "WebBrowser2.h"
#include "DogWindow.h"	// Added by ClassView
#include "FindDlg.h"	// Added by ClassView
#include "ThreadPropDlg.h"


#define NRM_AUTOSIGNUP (WM_APP+1000)
#define NRM_GRPREADDONE (WM_APP+1001)
#define NRM_FINDACCOUNT (WM_APP+1002) //LPARAM (LPCTRSTR *)name, return pointer to account
#define NRM_SELECTITEM (WM_APP+1003) //LPARAM (CFindList *)find item
#define NRM_GETSELMESSAGEREF (WM_APP+1004) //LPARAM (char *)buff, WPARAM buffsize;
#define NRM_SAVEACCSTATE (WM_APP+1005) //LPARAM (CNewsAccount *)account
#define NRM_DOWNLOADCOMPLETTED (WM_APP+1006) //LPARAM (CNewsArtHolder *)article
#define NRM_GOTOMESSAGE (WM_APP+1007) //WPARAM handle event, LPARAM handle to shared memory

//WPARAM = (CArticleDownloadControl::EDownloadResult) result

class CNewsAccount;

#define DOGTIMER 500
#define DOGFRAMES 3
#define CHECKTIMER 60000

extern BOOL watchdogalive;

class CMainFrame : public CFrameWnd
  {
  
  CGroupDlg grpdlg;
  CNewsAccount *lastcheck;
  CNewsAccount *lastcur;
  UINT dogtimer;
  CBitmap dog[DOGFRAMES];
  UINT dogcntr;
  int cleardog;
  HTREEITEM lastenum;
  UINT checktimer;
  UINT checktiming;
  UINT checkelapse;
  CNewsGroup *chkgroup;
  CThreadPropDlg thrdpropdlg;  
  bool _findBusy;

  
  public:
    CMainFrame();
  protected: 
    DECLARE_DYNAMIC(CMainFrame)
      
      // Attributes
    public:    
      static const MSG* PASCAL GetCurrentMessage( ) {return CWnd::GetCurrentMessage();}
    // Operations
  public:

    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMainFrame)
  public:
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
    bool LoadToolbars(bool big);
    CNRNotify notify;
    void GotoMessageId(const CString &msgid,bool ignoreSpecial=false);
    void CleanUp();
    CFindDlg fnddlg;
    void LoadWndState();
    void SaveWndState();
    bool CallSend(int ID);
    void CheckNewGroups(bool force=false, bool sauto=false);
    void AlterGroup(CNewsGroup *grp);
    virtual ~CMainFrame();
    #ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
    #endif
    
  protected:  // control bar embedded members
    CSplitWindowVert splitw;
    CSplitWindowHorz splith;
    CSplitWindowHorz splith2;
    CStatusBar  m_wndStatusBar;
    CToolBar    m_wndToolBar;	
    CToolBar    m_wndSyncTB;
    CDialogBar  m_wndFilter;
    CDialogBar  m_wndHighlight;
    CDialogBar  m_wndFindBar;
    CMessageList meslist;
    CAccountList acclist;
    CTreeCtrl lefttree;
    CWebBrowser2 msgtext;
    CDogWindow dogwnd;
    CComboBox wFilters;
    CComboBox wHighlights;
    
    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CMainFrame)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSetFocus(CWnd *pOldWnd);
    afx_msg void OnAccountsNew();
    afx_msg void OnClickTree(NMHDR* pNMHDR, LRESULT* pResult) ;
    afx_msg void OnRClickTree(NMHDR* pNMHDR, LRESULT* pResult) ;
    afx_msg void OnItemExpanding(NMHDR* pNMHDR, LRESULT* pResult) ;
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    afx_msg void OnUpdateAccounts(CCmdUI* pCmdUI);
    afx_msg void OnAccountsDelete();
    afx_msg void OnAccountsEdit();
    afx_msg void OnAccountsSignup();
    afx_msg void OnClose();
    afx_msg LRESULT OnFindAccount(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnSaveAccState(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnAutoSignup(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnGroupUpdate(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnSelectMessage(WPARAM wParam, LPARAM lParam);
    afx_msg void OnSelchangedTree(NMHDR* pNMHDR, LRESULT* pResult) ;
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnViewDebugconsole();
    afx_msg void OnUpdateViewDebugconsole(CCmdUI* pCmdUI);
    afx_msg void OnAccountsSynchronize();
    afx_msg void OnUpdateAccountsSynchronize(CCmdUI* pCmdUI);
    afx_msg void OnAccountsStop();
    afx_msg void OnUpdateAccountsStop(CCmdUI* pCmdUI);
    afx_msg void OnUpdateMessageSendnew(CCmdUI* pCmdUI);
    afx_msg void OnUpdateMessageReplyto(CCmdUI* pCmdUI);
    afx_msg void OnFileConfiguresmtp();
    afx_msg void OnUpdateMessageSendemail(CCmdUI* pCmdUI);
    afx_msg void OnViewMessagefilterDisplayallmessages();
    afx_msg void OnUpdateViewMessagefilterDisplayallmessages(CCmdUI* pCmdUI);
    afx_msg void OnViewMessagefilterDisplayonlyimportant();
    afx_msg void OnUpdateViewMessagefilterDisplayonlyimportant(CCmdUI* pCmdUI);
    afx_msg void OnViewMessagefilterHidehidden();
    afx_msg void OnUpdateViewMessagefilterHidehidden(CCmdUI* pCmdUI);
    afx_msg void OnViewGroupsastree();
    afx_msg void OnUpdateViewGroupsastree(CCmdUI* pCmdUI);
    afx_msg void OnGroupsCatchup();
    afx_msg void OnMessagesSelectionCancelmessages();
    afx_msg void OnGroupsUnsubscribe();
    afx_msg void OnFileOptions();
    afx_msg void OnGroupsCompact();
    afx_msg void OnFileExport();
    afx_msg void OnFileImport();
    afx_msg void OnMessagesSelectall();
    afx_msg void OnListpopupProperties();
    afx_msg void OnListpopupShowparent();
    afx_msg void OnFileProxyoptions();
    afx_msg void OnGroupsFind();
    afx_msg void OnMessagesClosethread();
    afx_msg BOOL OnQueryEndSession();
    afx_msg void OnMessageSendnew(UINT cmd) ;
    afx_msg BOOL OnMessageSendnewBool(UINT cmd) ;
    afx_msg void OnUpdateMessagesSelection(CCmdUI* pCmdUI);
    afx_msg void OnMessagesSelection(UINT cmd);
    afx_msg void OnMessagesMark(UINT id);
    afx_msg void OnUpdateGroups(CCmdUI* pCmdUI);
    afx_msg void OnEndSession(BOOL bEnding);
    afx_msg void OnMessagesRereadfromserver();
    afx_msg LRESULT OnNrmGetselmessageref(WPARAM wParam,LPARAM lParam);
    afx_msg LRESULT OnNrmDownloadComplette(WPARAM wParam,LPARAM lParam);
    afx_msg void OnMessagesBack();
    afx_msg void OnUpdateMessagesBack(CCmdUI* pCmdUI);
    afx_msg void OnMessagesForward();
    afx_msg void OnUpdateMessagesForward(CCmdUI* pCmdUI);
    afx_msg void OnMessagesSelectionDownload();
    afx_msg void OnMessagesLostsaved();
    afx_msg void OnNextWnd();
    afx_msg void OnPrevWnd();
    afx_msg void OnFileOffline();
    afx_msg void OnUpdateFileOffline(CCmdUI* pCmdUI);
    afx_msg void OnFullLoad();
    afx_msg void OnUpdateFullLoad(CCmdUI* pCmdUI);
    afx_msg void OnMessagesGoto();
    afx_msg void OnMsgNtTest();
    afx_msg void OnSize(UINT type, int cx, int cy);
    afx_msg void OnCheckVersion();
    afx_msg void OnUpdateCheckVersion(CCmdUI* pCmdUI);
    afx_msg void OnViewKwdSet();
    afx_msg void OnCacheMaintance();
    afx_msg BOOL OnDisplayVersion(UINT cmd);
    afx_msg void OnExtendedSynchronize();
    afx_msg void OnCopyShortcut();
    afx_msg void OnThreadsAdd();
    afx_msg void OnInitMenuPopup( CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu );
    afx_msg void OnUseEditCond(UINT cmdid);
    afx_msg void OnUseEditHighlight(UINT cmdid);
    afx_msg BOOL OnUseEditCondBool(UINT cmdid);
    afx_msg BOOL OnUseEditHighlightBool(UINT cmdid);
    afx_msg void OnAccountRepair();
    afx_msg void OnSetThreadProp();
    afx_msg void OnAccountSetLastRead();
    afx_msg void OnAccountsUnload();
    afx_msg void OnCheckfornewgroups();
    afx_msg void OnUpdateCheckfornewgroups(CCmdUI* pCmdUI);
    afx_msg void OnMessagesNextNew();
    afx_msg void OnGroupsNotify();
    afx_msg void OnUpdateGroupsNotify(CCmdUI* pCmdUI);
    afx_msg void OnFilterChange();
    afx_msg void OnFilterOpen();
    afx_msg void OnHighlightChange();
    afx_msg void OnHighlightOpen();
    afx_msg void OnSelectionSaveMessages();
    afx_msg void OnUpdateGroupsSubscribe(CCmdUI* pCmdUI);
    afx_msg void OnGroupsSubscribe();    
    afx_msg void OnAccountSetLastReadSmart();
    afx_msg void OnListpopupEdittext();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
    afx_msg void OnCompletteThread();
    afx_msg void OnExportParamFile();
	afx_msg void OnViewMessageSource();
	afx_msg void OnUpdateViewDeletedMessages(CCmdUI* pCmdUI);
	afx_msg void OnViewDeletedMessages();
    afx_msg LRESULT OnGotoMessageShortcut(WPARAM wParam, LPARAM lParam);
    afx_msg void OnMessagesReplyToKecal();
    afx_msg void OnUpdateMessagesReplyToKecal(CCmdUI* pCmdUI);
    afx_msg void OnQFFindNow();
    afx_msg void OnQFStop();
    afx_msg void EnterPress();
    afx_msg void OnUpdateQFFindNow(CCmdUI* pCmdUI);
    afx_msg void OnUpdateQFStop(CCmdUI* pCmdUI);
    afx_msg void OnHotkeyF7();
    afx_msg void OnQFCancel();
    afx_msg void OnUpdateQFCancel(CCmdUI* pCmdUI);
    afx_msg void OnGotoContext();
    afx_msg void OnUpdateGotoContext(CCmdUI* pCmdUI);
    afx_msg void OnFilterMyTasks();
    afx_msg void OnGroupsDownladAllArticles();
    afx_msg void OnAccountsDownladAllArticles();
	afx_msg void OnFlushMemoryCache() ;
    afx_msg void OnAutodeleteThreads();
	afx_msg BOOL OnGroupsFindSimilar(UINT cmd);
    
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    private:
    void LoadFiltersToCombo(CNewsAccount *acc);
    void InitFilterSubmenu( CMenu* pPopupMenu, int idCmd, int idNone, int kwpos);
    bool GotoNewsShortcut(const char *shortcut, CString *message=0);
    public:
       static void LoadFiltersToCombo(CComboBox &combo, CNewsAccount *acc, int kwpos);

       afx_msg void OnMessagesOpenrelatedurl();
       afx_msg void OnUpdateMessagesOpenrelatedurl(CCmdUI *pCmdUI);
};

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__55E680BE_242D_48EA_9F8B_22F29FB71B75__INCLUDED_)

