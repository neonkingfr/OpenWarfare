// ScanGroupsThread.cpp: implementation of the CScanGroupsThread class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "ScanGroupsThread.h"
#include "NewsTerminal.h"
#include "NewsAccount.h"
#include "MainFrm.h"
#include "NRNotify.h"
#include <afxinet.h>
#include "MsgBox.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////




static bool ScanDel(CNewsGroup *grp, CNewsTerminal& term)
  {
  DogShowStatusV(IDS_DELCHECK,grp->GetName());
  int preload=theApp.config.acc_cmd*2;
  int pp=0;
  CNewsArtHolder **ql=(CNewsArtHolder **)alloca(sizeof(CNewsArtHolder *)*preload);
  int i;
  for (i=0;i<preload;i++)ql[i]=NULL;
  int pos=0;
  int res;
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  CNewsArtHolder *z=grp->GetList();
  while (pp || z)
    {
    if (term.IsStopped()) break;
    if (ql[pos])
      {
      res=term.ReceiveNumReply();
      if (res<0) 
        {term.ShowError(res);return false;}
      if (res/10!=22) 
        ql[pos]->SetTrash();
      ql[pos]=NULL;
      pp--;
      }
    if (z)
      {
      if (!z->IsTrash() && !z->IsArchive())
        {
        term.SendFormatted("STAT %d",z->GetIndex());
        ql[pos]=z;pp++;
        }
      z=z->EnumNext();
      }
    pos++;
    if (pos>=preload) pos=0;
    }	
  return true;
  }

//--------------------------------------------------

static CNewsArtHolder *frst;

static bool DownloadArticles(CNewsAccount *acc, CNewsGroup *grp, CNewsTerminal& term, char mode)
  {
  bool all=(mode & 0x80)!=0;
  mode=mode & ~0x80;
  bool switchgrp=true;
  int preload=theApp.config.acc_cmd;
  int pp=0;
  CNewsArtHolder **ql=(CNewsArtHolder **)alloca(sizeof(CNewsArtHolder *)*preload);
  int i;
  for (i=0;i<preload;i++)ql[i]=NULL;
  int pos=0;
  int res;
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  CNewsArtHolder *z=grp->GetList();
  while (pp || z)
    {
    if (term.IsStopped()) break;
    if (ql[pos])
      {
      guard.Unlock();
      if (frst==NULL) frst=ql[pos];
      DogShowStatusV(IDS_DOWNLARTICLE,ql[pos]->GetSubject());
      res=term.ReceiveNumReply();
      if (res<0) 
        {term.ShowError(res);goto clean;}
      if (res/10!=22) ql[pos]->SetTrash();
      else
        if (ql[pos]->ReloadArticle(term,NULL,true,GetCurrentThreadId())==0)
              ql[pos]->SaveToCache(acc->cache,GetCurrentThreadId());
      guard.Lock();
      ql[pos]->Release();
      ql[pos]=NULL;
      pp--;
      }
    if (z)
      {
      if (z->IsInCache()==-1) z->SetInCache(acc->cache.FindInCache(z->messid));
      if (!z->IsTrash() && !z->IsArchive() && !z->IsInCache() && z->GetIndex()!=0 && (!theApp.config.lgWarm || (unsigned)z->lines<theApp.config.dwLines))
        if (all || (mode==NGDM_STOREBODIES || mode==NGDM_STOREBODIESINDEX) && (z->GetDwnNew() || z->GetUnread()))
          {
            if (switchgrp) 
              {
              if (grp->SetActiveGroup(term)==false) goto clean;
              switchgrp=false;
              }
            term.SendFormatted("ARTICLE %d",z->GetIndex());
            ql[pos]=z;pp++;
            z->AddRef();
        }
      if (z->IsArchive() && mode==2)
        z=z->GetNext();
      else
        z=z->EnumNext();
      }
    pos++;
    if (pos>=preload) pos=0;
    }	
  return true;
clean:
  for (int i=0;i<preload;i++) if (ql[i]) ql[i]->Release();
  return false;
  }

static bool DetectAttachmentData(const char *text, int size)
{
  int mezera=0;
  int wordLenTot=0;
  int wordLenCnt=0;
  int countLines=0;
  while (size--)
  {
    unsigned char c=*(unsigned char *)text;
    if (!(
      (c>=32 && c<127)|| (c>128 && c<255) || c=='\t' || c=='\r' || c=='\n' || c=='\a' || c=='\b' || c=='\f'
      || c=='\v')) return true;
    if (isspace(c))
    {
      if (mezera) {wordLenTot+=mezera;wordLenCnt++;}
      mezera=0;
    }
    else
    {
      mezera++;      
    }
    if (text[0]=='\r' && text[1]!='\n') return true;
    if (text[0]=='\n') countLines++;
    text++;
  }
  return (wordLenTot/wordLenCnt)>20;
}

static void IndexArticle(CNewsAccount *acc, CNewsArtHolder *hld, unsigned long owner, unsigned long indexFlags)
{
  FullTextIndex &fulltext=acc->fulltext;
  if (indexFlags & FTXI_DOWNLOADBODY)
  {
    int numa=0;
    PMessagePart message=hld->GetArticle(owner);
    CMessagePart *part=message;
    while (part)
    {
      if ((indexFlags & FTXI_ARTICLEBODIES) && (part->GetType()==CMessagePart::text || part->GetType()==CMessagePart::html))
         acc->fulltext.ProcessText(part->GetData(),hld->messid);  
      if (part->GetType()==CMessagePart::binary_uue)
      {
        numa++;
        if (indexFlags & FTXI_HASATTCHMENT)
            fulltext.Add("?a",hld->messid);
        if (indexFlags & FTXI_DETECTATTACHMENT)
        {
          bool binAtt=DetectAttachmentData(part->GetData(),part->GetSize());
          if (!binAtt && (indexFlags & FTXI_HASTEXTATTACHMENT))
            fulltext.Add("?txt",hld->messid);
          if (binAtt && (indexFlags & FTXI_HASBINARYATTACHMENT))
            fulltext.Add("?bin",hld->messid);
          if (!binAtt && (indexFlags & FTXI_COUNTATTACHMENTS))
            fulltext.ProcessText(part->GetData(),hld->messid);
        }
        if (indexFlags & FTXI_HASPICTUREATTACH)
        {
          const char *ext=Pathname::GetExtensionFromPath(part->GetFilename());
          if (stricmp(ext,".jpg")==0 ||
              stricmp(ext,".gif")==0 ||
              stricmp(ext,".emf")==0 ||
              stricmp(ext,".png")==0 ||
              stricmp(ext,".tga")==0 ||
              stricmp(ext,".psd")==0 ||
              stricmp(ext,".paa")==0 ||
              stricmp(ext,".pac")==0 ||
              stricmp(ext,".bmp")==0 ||
              stricmp(ext,".ico")==0) 
              fulltext.Add("?img",hld->messid);
        }
        if (indexFlags & FTXI_ATTACHMENTFNAME)
		{
		  CString name=part->GetFilename();
		  name.MakeLower();
          fulltext.Add(name,hld->messid);
		}
        if (indexFlags & FTXI_ATTACHMENTEXT)
		{
  		  CString name=Pathname::GetNameFromPath(part->GetFilename());
		  name.MakeLower();
          fulltext.Add(name,hld->messid);
		}
      }
      part=part->GetNext();
    }
    if (numa && (indexFlags & FTXI_COUNTATTACHMENTS))
    {
      char buff[100];
      sprintf(buff,"?%da",numa);
      fulltext.Add(buff,hld->messid);
    }
  }
  if (indexFlags & FTXI_SIZEFLAGS)
  {
    if (hld->lines<4) fulltext.Add("?s",hld->messid);
    if (hld->lines>50) fulltext.Add("?l",hld->messid);
    if (hld->lines>500) fulltext.Add("?xl",hld->messid);
  }
  if (indexFlags & FTXI_FROMLINE)
  {
    fulltext.ProcessText(hld->from,hld->messid);
  }
  if (indexFlags & FTXI_SUBJECTLINE)
  {
    fulltext.ProcessText(hld->subject,hld->messid);
  }
  else if (indexFlags & FTXI_SUBJECTWITHOUTPROP)
  {
    CString subj=hld->subject;
    int p=subj.Find(THREADPROPMARK);
    if (p!=-1) subj=subj.Mid(0,p);
    fulltext.ProcessText(subj,hld->messid);
  }
  fulltext.IndexEmpty(hld->messid);
  hld->indexed=true;
}

static bool IndexArticles(CNewsAccount *acc, CNewsTerminal& term, CNewsGroup *forcegrp=NULL)
{
  int preload=theApp.config.acc_cmd/2+1;
  int i;
  acc->FTSetupIgnoredWords();
  CNewsArtHolder **ql=(CNewsArtHolder **)alloca(sizeof(CNewsArtHolder *)*preload);
  for (i=0;i<preload;i++)ql[i]=NULL;
  int pos=0,pp=0;
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  CNewsGroup *grp=forcegrp?forcegrp:acc->GetGroups();
  while (grp)
  {
    CNewsArtHolder *hld=grp->GetList();    
    if (grp->fullload==NGDM_INDEXONLY || grp->fullload==NGDM_STOREBODIESINDEX) 
    {
	int total=-1;
	int progress=0;
    DWORD showtm=GetTickCount();
	::DogResetProgress();
	::DogSetMax(total);
    while (hld!=NULL || pp)
    {
      CNewsArtHolder *hldd=ql[pos];
      if (hldd!=NULL)
      {
        hldd->AddRef();guard.Unlock();
        int res=term.ReceiveNumReply();
        if (res<0) 
           {term.ShowError(res);return true;}
        if (res/10==22 && hldd->ReloadArticle(term,NULL,true,GetCurrentThreadId())==0)
        {
          IndexArticle(acc,hldd,GetCurrentThreadId(),acc->GetFTInclude());
/*          PMessagePart message=hldd->GetArticle(GetCurrentThreadId());
          CMessagePart *part=message;
          while (part)
          {
            if (part->GetType()==CMessagePart::text || part->GetType()==CMessagePart::html)                
               acc->fulltext.ProcessText(part->GetData(),hldd->messid);  
            part=part->GetNext();
          }
          acc->fulltext.ProcessText(hldd->subject,hldd->messid);
//          acc->fulltext.ProcessText(hldd->from,hldd->messid);*/
        }
        ql[pos]=0;
        --pp;
        guard.LockPM();
        hldd->Release();
      }
      if (hld!=NULL)
      {
        if (!hld->indexed)
        {
		  if (hld->GetParent()==NULL)
          {
            if (total==-1)
            {
              ++total;
              CNewsArtHolder *z=hld;
          	  while (z) {if (!z->indexed) total++; z=z->GetNext();}
            }
		    progress++;
            DWORD tm=GetTickCount();
            if (tm-showtm>1000)
            {
              showtm=tm;
              int perc=progress*100/total;
              if (perc>100) perc=100;
              ::DogShowStatusV(IDS_DOGINDEXINGMESSAGE,grp->GetName(),perc);
            }
		  }
          hld->AddRef();
          guard.Unlock();
          if (!acc->FTNeedRescan() && acc->fulltext.IsIndexed(hld->messid)) hld->indexed=true;
          else
          {
            if ((acc->GetFTInclude() & FTXI_DOWNLOADBODY)==0)
              IndexArticle(acc,hld,GetCurrentThreadId(),acc->GetFTInclude());
            else
            {
              if (hld->IsInCache()==-1) hld->SetInCache(acc->cache.FindInCache(hld->messid));        
              if (hld->IsInCache())
              {
                if (hld->LoadFromCache(acc->cache,GetCurrentThreadId()))
                {
                  IndexArticle(acc,hld,GetCurrentThreadId(),acc->GetFTInclude());
  /*                PMessagePart message=hld->GetArticle(GetCurrentThreadId());
                  CMessagePart *part=message;
                  while (part)
                  {
                    if (part->GetType()==CMessagePart::text || part->GetType()==CMessagePart::html)                
                       acc->fulltext.ProcessText(part->GetData(),hld->messid);  
                    part=part->GetNext();
                  }
                  acc->fulltext.ProcessText(hld->subject,hld->messid);
    //              acc->fulltext.ProcessText(hld->from,hld->messid);*/
                }
                else
                  hld->SetInCache(0);                                  
              }
              if (!hld->IsInCache() && term.IsConnected())
              {
               if (term.SendFormatted("ARTICLE %s",hld->messid)==true)
               {
               ql[pos]=hld;
               ++pp;             
               }
              }
            }
          }
          guard.Lock();
          hld->Release();
        }
        hld=hld->EnumNext();
        if (term.IsStopped()) {hld=NULL;forcegrp=grp;}
      }
      ++pos;if (pos>=preload)  pos=0;        
        
    }
    }
  grp=grp->GetNext();
  if (forcegrp) break;
  }
return true;
}

//--------------------------------------------------

void CScanGroupsThreadEx::Run()
  {
  CNewsGroup *grp;
  
  bool beep=false;
  CScoopMutex scoop(acc->mutex,10000);
  if (!scoop) return;
  acc->mutex.Unlock();
  bool extsync=acc->extsync;
  acc->extsync=false;
  if (nwnotify) SendMessage(nwnotify,NRN_NEWMESSAGE,2,(LPARAM)acc);
  CNewsTerminal term;
  error=true;
  DogShowStatusV(IDS_NEGOTIATING);
  
  if (term.ShowError(acc->OpenConnection(term))!=0) return;
  if (!acc->IsLoadOnDemand() || acc->IsDownloadForced())
    {
    CTime ll=acc->GetLastCheck();
    lastcheck=CTime::GetCurrentTime();
    if (allgroups) ll=CTime((time_t)1);
    CNewsGroup kernel;
    DogShowStatusV(IDS_CHECKNEWS,acc->GetAccName());
    if (term.ShowError(term.GetListOfGroups(ll,&kernel))!=0) return;
    delete grlist;
    grlist=kernel.GetNext();
    kernel.Unlink();
    theApp.Lock();
    grp=UnlinkList();
    if (grp!=NULL)
      {
      acc->SetNew(grp);
      acc->AppendGrouplist(grp);
      acc->Lock();
      theApp.m_pMainWnd->PostMessage(NRM_AUTOSIGNUP,0,(LPARAM)acc);		
      }	
    theApp.Unlock();
    acc->SetLastCheck(lastcheck);
    }
  
  
  grp=acc->GetGroups();  
  while (grp)
    {
    if (grp->sign)
      {
      long urgent=false;
      if (nwnotify) SendMessage(nwnotify,NRN_NEWMESSAGE,1,(LPARAM)grp);
      DogShowStatusV(IDS_GROUPDOWNLOAD,grp->GetName());
      if (term.ShowError(grp->ReadGroup(term,extsync,theApp.config.sentread?acc->GetMyEmail():NULL,acc->msgidlist,&urgent))) return;
      acc->Compact(false,grp);
      acc->ApplyCond(false,grp);
      acc->ApplyHighlight(false,grp);
      acc->PostApplyConds(grp);
      if (nwnotify)
        {
        CRWLockGuard guard(articleLock,false);guard.LockPM();
        CNewsArtHolder *hld=grp->GetList();
        while (hld)
          {
          if (hld->GetHidden()) hld=hld->GetNext();
          else 
            {
            if (hld->GetNtNew() && hld->GetUnread())
              {
              hld->AddRef();
              SendMessage(nwnotify,NRN_NEWMESSAGE,0,(LPARAM)hld);
            if (acc->lastread.GetStatus()==COleDateTime::valid) 
                hld->MarkAsRead(9,acc->lastread>=hld->date);
              }
            
            hld=hld->EnumNext();
            }
          }
        }
/*      grp->CountNewsAndHighlights();	  */
      grp->delcheck=true;
      if (urgent && !beep )
        {
        acc->PlayUrgent();
        beep=true;
        }
      }
    if (grp->iteminlist)
      theApp.m_pMainWnd->PostMessage(NRM_GRPREADDONE,0,(LPARAM)grp->iteminlist);	
    grp=grp->GetNext();
    }  
  error=false;
  
  if (acc->offline)
    {
    frst=NULL;
    grp=acc->GetGroups();
    while (grp && !term.IsStopped())
      {
      if (grp->sign && grp->fullload)
        {
        ::DogShowStatusV(IDS_LOOKING);
        if (DownloadArticles(acc, grp, term, grp->fullload|(grp->_downloadall?0x80:0))==false) break;
        grp->_downloadall=false;
        }
      grp=grp->GetNext();		
      }
    }
  if (acc->GetFTGenerateOpt()==1 && !acc->FTWasGenerated() || acc->GetFTGenerateOpt()==2)
  {
    IndexArticles(acc,term);
    if (!term.IsStopped()) acc->FTMarkGenerated();
  }
  if (scandel)
    {
    grp=acc->GetGroups();
    while (grp && !term.IsStopped())
      {
      if (grp->sign)		
        if (grp->SetActiveGroup(term))
          if (ScanDel(grp,term)==false) break;
      grp=grp->GetNext();		
      }
    }
  DogShowStatusV(IDS_DONE);
  acc->dirty=true;
  term.Quit();
  acc->Lock();
  theApp.m_pMainWnd->PostMessage(NRM_SAVEACCSTATE,0,(LPARAM)acc);	
  HWND fore=GetForegroundWindow();
  DWORD foreid;
  ::GetWindowThreadProcessId(fore,&foreid);
  if (GetCurrentProcessId()!=foreid)
  {
      TRACE0("Newsreader is idle, droping index files");
      acc->fulltext.ResetIndex();
    }
  }

//--------------------------------------------------

CScanNewerOlderThread::CScanNewerOlderThread(CNewsAccount *acc, CNewsGroup *grp, bool scandel):
acc(acc),grp(grp),scandel(scandel)
  {
  acc->Lock();  
  LogWindowMessage(0,2,0);
  }

//--------------------------------------------------

CScanNewerOlderThread::~CScanNewerOlderThread()
  {
  acc->Release();
  LogWindowMessage(0,0,0);
  }

//--------------------------------------------------

struct SIntegrityThreadInfo
  {
  CNewsAccount *acc;
  CNewsGroup *grp;
  };

//--------------------------------------------------

/*
static UINT IntegrityScanThread(LPVOID context)
  {
  SIntegrityThreadInfo *nfo=(SIntegrityThreadInfo *)context;
  CNewsArtHolder *hld=nfo->grp->GetList();
  while (hld)
	{
	if (hld->chpos)
	  {
	  iostream *str=nfo->acc->cache.OpenMessage(hld->messid);
	  if (str==NULL)	  
		hld->chpos=0;	  
	  else
		nfo->acc->cache.CloseMessage();
	  }
    hld=hld->EnumNext();
	}
  nfo->grp->delcheck=false;
  ::DogShowStatusV(IDS_NEGOTIATING);
  return 0;
  }
*/

void CScanNewerOlderThread::Run()
  {
  CScoopMutex scoop(acc->mutex,10000);
  if (!scoop) return;
  CNewsTerminal term;
  CString line;
  SIntegrityThreadInfo nfo;
  nfo.acc=acc;
  nfo.grp=grp;
  CWinThread *p;
  /*  if (acc->cachecheck && grp->delcheck)
	{
	::DogShowStatusV(IDS_CACHECHECK);
	p=AfxBeginThread(IntegrityScanThread,&nfo,THREAD_PRIORITY_NORMAL,16384,CREATE_SUSPENDED,NULL);
	p->m_bAutoDelete=false;
	p->ResumeThread();
	}
  else*/
    {p=NULL;  ::DogShowStatusV(IDS_NEGOTIATING);}
  int res=term.ShowError(acc->OpenConnection(term));
  ::DogShowStatus("");
  if (p) 
    {
    WaitForSingleObject(p->m_hThread,INFINITE);
    delete p;
    }
  if (res!=0) return;
  DogShowStatusV(IDS_GROUPDOWNLOAD,grp->GetName());
  long beep=false;
  if (term.ShowError(grp->ReadGroup(term,false,theApp.config.sentread?acc->GetMyEmail():NULL,acc->msgidlist,&beep))) return;
  /*
  theApp.Lock();
  acc->ActivateKeywords();
  grp->GetList()->CalcHierarchy();
  theApp.Unlock();*/
  acc->Compact(false,grp);
  acc->ApplyCond(false,grp);
  acc->ApplyHighlight(false,grp);
  acc->PostApplyConds(grp);
  if (grp->iteminlist)
    theApp.m_pMainWnd->PostMessage(NRM_GRPREADDONE,0,(LPARAM)grp->iteminlist);	
  if ((grp->_downloadall || grp->fullload==NGDM_STOREBODIES || grp->fullload==NGDM_STOREBODIESINDEX) && acc->offline)
    if (DownloadArticles(acc, grp, term, grp->fullload|(grp->_downloadall?0x80:0))==false) goto end;
  grp->_downloadall=false;
  if (acc->GetFTGenerateOpt()==2)
  {
    IndexArticles(acc,term,grp);
  }
  DogShowStatusV(IDS_DONE);
end:;
  term.SendLine("QUIT");
  if (beep) acc->PlayUrgent();
  }

//--------------------------------------------------

CDownloadArticleThread::CDownloadArticleThread(CWnd *p, CNewsArtHolder *hld,CNewsAccount *acc,CNewsGroup *grp,bool download_long):
call(*p),hld(hld),acc(acc),grp(grp),download_long(download_long)
  {
  acc->Lock();
  }

//--------------------------------------------------

CDownloadArticleThread::~CDownloadArticleThread()
  {
  acc->Release();
  }

//--------------------------------------------------

#include "ArticleDownload.h"


void CDownloadArticleThread::Run()
  {  
  if (acc->offline && hld->LoadFromCache(acc->cache))
    {
    PostMessage(call,NRM_DOWNLOADCOMPLETTED,(WPARAM)CArticleDownloadControl::ED_Ok,(LPARAM)hld);
    return;
    }
  if (theApp.offline) 
    {
    PostMessage(call,NRM_DOWNLOADCOMPLETTED,(WPARAM)CArticleDownloadControl::ED_Offline,(LPARAM)hld);
    return;
    }
  if (theApp.config.lgWarm && (unsigned)hld->lines>theApp.config.dwLines && hld->GetLgWarn())
    {
    PostMessage(call,NRM_DOWNLOADCOMPLETTED,(WPARAM)CArticleDownloadControl::ED_TooLong,(LPARAM)hld);
    return;
    }
  CNewsTerminal term;
  int res=acc->OpenConnection(term);
  if (res) 
    {term.ShowError(res);PostMessage(call,NRM_DOWNLOADCOMPLETTED,(WPARAM)CArticleDownloadControl::ED_ConnectError,(LPARAM)hld);return;}
  int out=hld->ReloadArticle(term,grp,false);
  term.Quit();
  if (out<0 && !term.IsStopped()) 
    {
    DogShowErrorV(IDS_ARTICLENOTEXISTS,hld->GetIndex());
    hld->SetTrash();
    PostMessage(call,NRM_DOWNLOADCOMPLETTED,(WPARAM)CArticleDownloadControl::ED_NotFound,(LPARAM)hld);	  
    return;
    }
  else if (out==0)
    {
    DogShowStatusV(IDS_ARTICLELOADED,hld->GetIndex());
    PostMessage(call,NRM_DOWNLOADCOMPLETTED,(WPARAM)CArticleDownloadControl::ED_Ok,(LPARAM)hld);
    term.SendLine("QUIT");
    if (acc->offline) hld->SaveToCache(acc->cache);
    }
  else PostMessage(call,NRM_DOWNLOADCOMPLETTED,(WPARAM)CArticleDownloadControl::ED_Interrupted,(LPARAM)hld);	  
  return;
  }

//--------------------------------------------------

//void NavigateArticle( CNewsAccount *acc, CNewsGroup *grp, CNewsArtHolder *hld);

void CSaveNewsThread::Run()
  {
  for (HTREEITEM p=NULL;p=acclist.EnumAccounts(p);)
    {
    CNewsAccount *acc=acclist.GetAccount(p);
    if (acc->dirty && acc->mutex.Lock(1) )
      {
      acclist.AccountStreaming(p,true);
      ::DogShowStatusV(IDC_SAVECOMPLETTED);
      acc->mutex.Unlock();
      }
    }
  }

//--------------------------------------------------

void CSaveAccThread::Run()
  {
  CNewsAccount *acc=acclist.GetAccount(it);
  if (acc)
    if (acc->dirty && acc->mutex.Lock(2000) )
      {
      ::DogShowStatusV(IDS_SAVINGSTATUS,acc->GetAccName());
      acclist.AccountStreaming(it,true);
      ::DogShowStatusV(IDS_BUILDINGHASH);
      acc->BuildHash();
      ::DogShowStatusV(IDC_SAVECOMPLETTED);
      acc->mutex.Unlock();
      theApp.m_pMainWnd->PostMessage(NRM_GRPREADDONE,0,0);
      }
  SetProcessWorkingSetSize(GetCurrentProcess(),0xffffffff,0xffffffff);
  }

//--------------------------------------------------

#define NEWSREADER "NewsReader.exe"
#define PACKAGE "NRUpdate.$$$"
#define PACKAGE2 "NRUpdate.pak"
#define NRUPDATE "NRUPDATE.EXE"

#define GROUP  "Update"
#define TIMEH "TimeH"
#define TIMEL "TimeL"
/*
UINT CheckNewerVersionThread(LPVOID *nothing)
  {
  CInternetSession inet;
  CFtpConnection *ftp=inet.GetFtpConnection("ftp.webzdarma.cz","mobilus.webz.cz","hexagold",21,FALSE);
  if (ftp==NULL)
	inet.GetFtpConnection("ftp.webzdarma.cz","mobilus.webz.cz","hexagold",21,TRUE);
  if (ftp==NULL) return 0;
  ftp->SetCurrentDirectory("NewsReader");
  CFtpFileFind ff(ftp);  
  if (ff.FindFile(NEWSREADER))
	{
	ff.FindNextFile();
	FILETIME ft;
	ff.GetLastWriteTime(&ft);
	FILETIME old;
	old.dwLowDateTime=AfxGetApp()->GetProfileInt(GROUP,TIMEL,0);
	old.dwHighDateTime=AfxGetApp()->GetProfileInt(GROUP,TIMEH,0);
	if ((old.dwHighDateTime<ft.dwHighDateTime) || (old.dwHighDateTime==ft.dwHighDateTime && old.dwLowDateTime<ft.dwLowDateTime))
	  if (AfxMessageBox(IDS_NEWVERSION,MB_YESNO|MB_ICONQUESTION)==IDYES)
	  {
	  if (ftp->GetFile(NEWSREADER,PACKAGE,FALSE))
		{
		AfxGetApp()->WriteProfileInt(GROUP,TIMEL,ft.dwLowDateTime);
		AfxGetApp()->WriteProfileInt(GROUP,TIMEH,ft.dwHighDateTime);
		HINSTANCE inst=AfxGetInstanceHandle();
		HRSRC rsrc=FindResource(inst,NRUPDATE,"EXE");
		HGLOBAL glob=LoadResource(inst,rsrc);
		void *p=LockResource(glob);
		ofstream file(NRUPDATE,ios::binary|ios::trunc|ios::out);
		if (!(!file))
		  {
		  file.write((char *)p,SizeofResource(inst,rsrc));
		  file.close();
		  CString done;
		  done.LoadString(IDS_UPDATEDONE);
		  MessageBox(NULL,done,NEWSREADER,MB_OK|MB_SYSTEMMODAL);
		  ShellExecute(NULL,NULL,NRUPDATE,NULL,NULL,SW_SHOWNORMAL);
		  ftp->Close();
		  delete ftp;
		  return 0;
		  }
		}
	  AfxMessageBox(IDS_UPDATEERROR,MB_OK|MB_ICONSTOP);

		}
	}
  ftp->Close();  
  delete ftp;
  return 0;
  }
*/  

#include "UpdateProgressDlg.h"

UINT CheckNewerVersionThread(LPVOID ask)
  {
  BOOL askd=(BOOL)ask;
  char *askme="Update";
  BOOL aa=theApp.GetRegInt(askme);
  if (aa==TRUE && askd==FALSE) return 0;
  const char *tmp=theApp.FullPath("",true);
  char *pth=(char *)alloca(strlen(tmp)+1);
  strcpy(pth,tmp);
  int l=strlen(pth);
  if (pth[l-1]=='\\') pth[--l]=0;
  char *chkp=(char *)alloca(l+50);
  sprintf(chkp,"%s\\"INSTALLNAME" checkver",pth);
  STARTUPINFO sinfo;
  memset(&sinfo,0,sizeof(sinfo));
  sinfo.cb=sizeof(sinfo);
  PROCESS_INFORMATION proc;
  memset(&proc,0,sizeof(proc));
  BOOL failed=FALSE;
  BOOL uptodate=TRUE;
  if (!failed)
    if (CreateProcess(NULL,chkp,NULL,NULL,TRUE,NORMAL_PRIORITY_CLASS,NULL,tmp,&sinfo,&proc)==FALSE) failed=TRUE;
  if (!failed && WaitForSingleObject(proc.hProcess,10000)==WAIT_TIMEOUT) failed=TRUE;
  if (!failed)
    {
    DWORD w;
    if (GetExitCodeProcess(proc.hProcess,&w)==FALSE) failed=TRUE;
    else	  
      uptodate=!w;	  
    }
  CloseHandle(proc.hProcess);
  CloseHandle(proc.hThread);
  if (failed)
    {
    CMsgBox box;
    box.Flags.okbutt=box.Flags.noagain=1;
    box.noagain=aa;
    box.message.LoadString(IDS_UNABLETOCHECKVERSION);
    box.DoModal();
    theApp.SetRegInt(askme,box.noagain);
    return 0;
    }
  if (!uptodate)
    {
    CMsgBox box;
    box.Flags.yesbutt=box.Flags.nobutt=box.Flags.noagain=1;
    box.noagain=aa;
    box.message.LoadString(IDS_NEWVERSION);
    int res=box.DoModal();
    theApp.SetRegInt(askme,box.noagain);
    if (res==IDNO) return 0;
    
    HWND hWnd=theApp.m_pMainWnd->m_hWnd;
    theApp.m_pMainWnd->SendMessage(WM_CLOSE,0,0);
    if (IsWindow(hWnd)) return 0;
    char *d=(char *)alloca(strlen(chkp)+strlen(tmp)+10);
    sprintf(d,"%s\\"INSTALLNAME" %s",pth,pth);
    CreateProcess(NULL,d,NULL,NULL,FALSE,CREATE_NEW_PROCESS_GROUP|NORMAL_PRIORITY_CLASS,NULL,pth,&sinfo,&proc);	
    WaitForInputIdle(proc.hProcess,INFINITE);
    CloseHandle(proc.hProcess);
    CloseHandle(proc.hThread);
    return 1;
    }
  else if (askd)
    {
    CMsgBox box;
    box.Flags.okbutt=box.Flags.noagain=1;
    box.noagain=aa;
    box.message.LoadString(IDS_VERSIONISUPTODATE);
    box.DoModal();
    theApp.SetRegInt(askme,box.noagain);
    }
  return 0;
  }

//--------------------------------------------------

#include "CompactCacheDlg.h"

void CAccountCompactThread::Run()
  {
  Sleep(500);
  CString ss;
  for (HTREEITEM it=NULL;it=acclist.EnumAccounts(it);)
    {
    CNewsAccount *acc=acclist.GetAccount(it);
    acc->mutex.Lock();
    acc->Lock();
    if (acc->offline && acc->cache.IsCacheFull())
      {
      AfxFormatString1(ss,IDS_COMPACTCACHE,acc->GetAccName());	
      if (acc->Autocompact() || NrMessageBox(ss,MB_YESNO|MB_ICONQUESTION)==IDYES)
        {
        CCompactCacheDlg dlg;
        dlg.acc=acc;
        dlg.acclist=&acclist;
        dlg.it=it;
        dlg.targetsize=acc->offline*acc->compacto/100;
        dlg.DoModal();
        }
      }
    acc->Release();
    acc->mutex.Unlock();
    }
  }

//--------------------------------------------------
