#pragma once



class CMailHdrParser
{  

  void CorrectOneLine(char *ln);
  bool ParseOneLine(char *ln);
public:

  void ParseHeader(const char *hdr);
  virtual void HdrResult(const char *command, const char *data)=0;
};
