// MMapBTree.h: interface for the MMapBTree class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MMAPBTREE_H__A0A8DDDF_D2E6_443C_97D1_E702A1A46B49__INCLUDED_)
#define AFX_MMAPBTREE_H__A0A8DDDF_D2E6_443C_97D1_E702A1A46B49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MMAPBTREEBLOCKSIZE 256
#define MMAPBTREEBLOCKITEMS ((MMAPBTREEBLOCKSIZE-8)/(sizeof(MMapBTreeBlockItem)+4))
#define MMAPBTREEBLOCKLEAF ((MMAPBTREEBLOCKSIZE-8)/sizeof(MMapBTreeBlockItem))
#define MMAPBTREEMAXSIZE 128

struct MMapBTreeEnum
{
  unsigned long blockPath[MMAPBTREEMAXSIZE];
  unsigned char itemPos[MMAPBTREEMAXSIZE];
  int level;
};

struct MMapBTreeBlockItem  
{
  unsigned long key;      //key value
  unsigned long dataOffset; 
  bool operator>(const MMapBTreeBlockItem& other) const
	{
	return key>other.key || (key==other.key && dataOffset>other.dataOffset);
	}
  bool operator<(const MMapBTreeBlockItem& other) const
	{
	return key<other.key || (key==other.key && dataOffset<other.dataOffset);
	}
};


class MMapBTree  
{

    struct DataBlock
    {
      unsigned long numItems;      
      unsigned long leftSubTree; //when this value is zero, item is leaf
      MMapBTreeBlockItem items[MMAPBTREEBLOCKITEMS];
	  unsigned long subTree[MMAPBTREEBLOCKITEMS];
	  unsigned char padding[MMAPBTREEBLOCKSIZE-2*sizeof(unsigned long)-sizeof(MMapBTreeBlockItem)*MMAPBTREEBLOCKITEMS-sizeof(unsigned long )*MMAPBTREEBLOCKITEMS]; 
	  //padding je nutny. MMAPBTREEBLOCKLEAF  pocita s velikostu bloku MMAPBTREEBLOCKSIZE, takze musi byt dodrzena
    };

    struct Header
    {
      unsigned long firstItem;
      unsigned long nextBlock;
      char dummy[MMAPBTREEBLOCKITEMS*4];
    };

    HANDLE _hFile;  // handle of mapped file
    HANDLE _hMem;   // handle of mapped object
    unsigned long _curSize; //current size of file
    DataBlock *_base;
    Header *_header;

public:
	MMapBTree();
	virtual ~MMapBTree();
    bool Create(const char *filename);
    bool Find(const MMapBTreeBlockItem &value, MMapBTreeEnum &iter);
    bool Next(MMapBTreeEnum &iter, MMapBTreeBlockItem  &value);
    bool Add(const MMapBTreeBlockItem &value);
    void Close()
    {
      UnmapFile();
      if (_hFile) CloseHandle(_hFile);
      _hFile=NULL;
    }

    void Clear()
    {
      UnmapFile();
      SetFilePointer(_hFile,0,NULL,FILE_BEGIN);
      SetEndOfFile(_hFile);
      if (MapFile(4096)==false) return;
      _header->firstItem=0;
      _header->nextBlock=1;      
    }
    



private:
	void UnmapFile();
	bool MapFile(unsigned long newsize);
    unsigned long AllocNewBlock();
    DataBlock *GetBlock(unsigned long id) {return _base+id;}
    unsigned long FindItemInLevel(const MMapBTreeBlockItem &value, unsigned long level, char *cmpres=NULL);
    void Divide(MMapBTreeEnum &iter, unsigned long level);

};

#endif // !defined(AFX_MMAPBTREE_H__A0A8DDDF_D2E6_443C_97D1_E702A1A46B49__INCLUDED_)
