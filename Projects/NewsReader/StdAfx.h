// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__54B13828_F2A9_4D21_B9A8_0394BD8E43FD__INCLUDED_)
#define AFX_STDAFX_H__54B13828_F2A9_4D21_B9A8_0394BD8E43FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers


#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

//#include <afxsock.h>		// MFC socket extensions
#include <afxmt.h>
#include <multimon.h>
#include <WinSock2.h>

#include "GenArchive.h"

#define ACCEXT ".acc"
#define ACCEXTX ".axx"
#define ACCNEWEXT ".nr"
#include <malloc.h>
#include <fstream>
#include <strstream>
#include "ImageListEx.h"


#include "CriticalSection.h"

#include "IPA.h"
#include "Socket.h"

WINBASEAPI BOOL WINAPI TryEnterCriticalSection( LPCRITICAL_SECTION lpCriticalSection  );

#define CANCELTHREADSUBJECT "#$**THREADCANCELED**$#"

#define CLOSETHREADSUBJECT "#$Close:"
#define CLOSETHREADSUBJECTLEN 8
#define THREADPROPMARK "#$"
#define SUBTHREADPROPMARK "#!"

#define INSTALLNAME "InetSetup.exe"
#define NEWINSTNAME "install.bin"

extern CCriticalSection glob_lock;

DWORD ShadeSysColor(int color,int factor);
DWORD ShadeColor(COLORREF color, int factor);

void LogMarkAsRead(const char *messageid, bool state, int section);
void LogWindowMessage(UINT msg, WPARAM wParam, LPARAM lParam);
void CompactLogMarkAsRead();

#define strdupst(string) strcpy((char *)alloca(strlen(string)+sizeof(char)),string)

//extern int _globalSortItem;


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__54B13828_F2A9_4D21_B9A8_0394BD8E43FD__INCLUDED_)


