// scanner3DDlg.h : header file
//

#if !defined(AFX_SCANNER3DDLG_H__48C1C8E0_0CA2_4E25_89A1_9C532453F92F__INCLUDED_)
#define AFX_SCANNER3DDLG_H__48C1C8E0_0CA2_4E25_89A1_9C532453F92F__INCLUDED_

#include "Scene.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CScanner3DDlg dialog

class CScanner3DDlg;

class CDlgScene: public Scene
{
  public:
    int ppos;
    CScanner3DDlg *wnd;
    unsigned long normalinvert;	
    int totlpts;
    int ovrpts;	
    
    virtual void Status(int phase);
    virtual void PointHit(int fc, int x, int y, float depth, geVector4 &normal);
};

class CScanner3DDlg : public CDialog
{
  friend CDlgScene;
  // Construction
  CBitmap curbmp;
  CString input,outpath;
  CTime inputtime;
  DWORD begtime;
  
  BitMap<unsigned long> depth;
  BitMap<unsigned char> depth8;
  BitMap<unsigned char> alpha;
  BitMap<unsigned long> normal;
  BitMap<unsigned long> tex;
  public:
    void WritePSD();
    void Start();
    void PostProcessing();
    void SelectBitmap(int tab=-1);
    void PrepareMaps();
    void SetBitmap(BitMap<unsigned long> &src);
    void SetBitmap(BitMap<unsigned char> &src);
    void SetProgress(int cur, int max=100);
    void SetStatus(const char *text, ...);
    void SetStatistics(const char *text, ...);
    CDlgScene scene;
    void DialogRules();
    CScanner3DDlg(CWnd* pParent = NULL);	// standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CScanner3DDlg)
    enum 
      { IDD = IDD_SCANNER3D_DIALOG };
    CTabCtrl	wTab;
    CStatic	wStatus;
    CEdit	wStatistics;
    CEdit	wDepthMin;
    CEdit	wDepthMax;
    CProgressCtrl	wProgress;
    BOOL	bAutodetectDepth;
    float	fDepthMax;
    float	fDepthMin;
    BOOL	bSaveAlpha;
    BOOL	bSaveDepth;
    BOOL	bSaveNormal;
    BOOL	bSaveTex;
    BOOL	bInvertDepth;
    BOOL	bNormInvertX;
    BOOL	bNormInvertY;
    BOOL	bNormInvertZ;
    int		iNormOrder;
    BOOL	bTexVisible;
    BOOL	b24Depth;
    BOOL	bAlphaInvert;
    //}}AFX_DATA
    
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CScanner3DDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    HICON m_hIcon;
    
    // Generated message map functions
    //{{AFX_MSG(CScanner3DDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void OnSaveXXXX();
    afx_msg void OnAutodetectdepth();
    afx_msg void OnStart();
    afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg void OnSelchangeViewtab(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnTimer(UINT nIDEvent);
    virtual void OnCancel();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCANNER3DDLG_H__48C1C8E0_0CA2_4E25_89A1_9C532453F92F__INCLUDED_)
