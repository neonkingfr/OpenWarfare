// scanner3D.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "scanner3D.h"
#include "scanner3DDlg.h"
#include "BitMap.h"
#include "Scene.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CStringRes StrRes;

/////////////////////////////////////////////////////////////////////////////
// CScanner3DApp

BEGIN_MESSAGE_MAP(CScanner3DApp, CWinApp)
  //{{AFX_MSG_MAP(CScanner3DApp)
  // NOTE - the ClassWizard will add and remove mapping macros here.
  //    DO NOT EDIT what you see in these blocks of generated code!
  //}}AFX_MSG
  ON_COMMAND(ID_HELP, CWinApp::OnHelp)
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CScanner3DApp construction
      
      CScanner3DApp::CScanner3DApp()
      {
        // TODO: add construction code here,
        // Place all significant initialization in InitInstance
      }

/////////////////////////////////////////////////////////////////////////////
// The one and only CScanner3DApp object

CScanner3DApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CScanner3DApp initialization

BOOL CScanner3DApp::InitInstance()
{
  // Standard initialization
  // If you are not using these features and wish to reduce the size
  //  of your final executable, you should remove from the following
  //  the specific initialization routines you do not need.
  
  #ifdef _AFXDLL
  Enable3dControls();			// Call this when using MFC in a shared DLL
  #else
  Enable3dControlsStatic();	// Call this when linking to MFC statically
  #endif
  StrRes.SetInstance(AfxGetInstanceHandle());
  
  CScanner3DDlg dlg;
  m_pMainWnd = &dlg;
  int nResponse = dlg.DoModal();
  if (nResponse == IDOK)
  {
    // TODO: Place code here to handle when the dialog is
    //  dismissed with OK
  }
  else if (nResponse == IDCANCEL)
  {
    // TODO: Place code here to handle when the dialog is
    //  dismissed with Cancel
  }
  
  /*	Scene sc;
	ifstream file("S:\\Bredy\\scaner3D\\bredy01.wrl");
	ASSERT(!(!file));
	sc.LoadScene(file);
	sc.CheckScene();*/
  
  // Since the dialog has been closed, return FALSE so that we exit the
  //  application, rather than start the application's message pump.
  return FALSE;
}

