// SimplePhotoshopFile.h: interface for the CSimplePhotoshopFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SIMPLEPHOTOSHOPFILE_H__E5F539C3_2B54_4318_B472_99387D1F9DF6__INCLUDED_)
#define AFX_SIMPLEPHOTOSHOPFILE_H__E5F539C3_2B54_4318_B472_99387D1F9DF6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

void FWrite(ostream &out, void *data, int size, int pad);
inline void FWrite(ostream &out, char data) 
{FWrite(out,&data,1,sizeof(data));}

inline void FWrite(ostream &out, unsigned char data) 
{FWrite(out,&data,1,sizeof(data));}

inline void FWrite(ostream &out, short data) 
{FWrite(out,&data,1,sizeof(data));}

inline void FWrite(ostream &out, unsigned short data) 
{FWrite(out,&data,1,sizeof(data));}

inline void FWrite(ostream &out, long data) 
{FWrite(out,&data,1,sizeof(data));}

inline void FWrite(ostream &out, unsigned long data) 
{FWrite(out,&data,1,sizeof(data));}

inline void FWrite(ostream &out, int data) 
{FWrite(out,&data,1,sizeof(data));}

inline void FWrite(ostream &out, unsigned int data) 
{FWrite(out,&data,1,sizeof(data));}

#define PSPM_BITMAP 0
#define PSPM_GRAYSCALE 1
#define PSPM_INDEXED 2
#define PSPM_RGB 3
#define PSPM_CMYK 4
#define PSPM_MULTICHAN 7
#define PSPM_DUOTONE 8
#define PSPM_LAB 9

class CSimplePhotoshopFile  
{
  public:
    class SectionWriter
    {
      ostream &out;
      unsigned long pos;
      public:
        SectionWriter(ostream &q):out(q) 
        {
          unsigned long zero=0;
          FWrite(out,zero);
          pos=out.tellp();
        }
        ~SectionWriter() 
        {
          unsigned long save=out.tellp();
          out.seekp(pos);
          FWrite(out,save-pos);
          out.seekp(save);
        }
    };
    
    ofstream file;
    
  public:
    void WriteGlobalLayerMaskInfo();
    void BeginLayerSection(short nlayes);
    void WriteRGBChannelImageData(CRect rect, unsigned long *data);
    void LayerDef(CRect layerrc, short channels);
    void WriteColorModel(const char *data, unsigned long size);
    void WriteHeader(unsigned short channels, unsigned long width, unsigned long height, unsigned short depth, unsigned short mode);
    bool CreateFile(const char *fname);
    CSimplePhotoshopFile();
    virtual ~CSimplePhotoshopFile();
    
};

#endif // !defined(AFX_SIMPLEPHOTOSHOPFILE_H__E5F539C3_2B54_4318_B472_99387D1F9DF6__INCLUDED_)
