// Searcher.h: interface for the Searcher class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SEARCHER_H__B39070D7_E49F_4CC2_9192_D89A4B1511DD__INCLUDED_)
#define AFX_SEARCHER_H__B39070D7_E49F_4CC2_9192_D89A4B1511DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <iostream.h>

class Searcher  
{
  CString text;
  int index;	
  public:
    bool Advance(char znak);
    Searcher(CString &searchtext);
    
};

int FileSearch(istream &in, Searcher **list, int count);

#endif // !defined(AFX_SEARCHER_H__B39070D7_E49F_4CC2_9192_D89A4B1511DD__INCLUDED_)
