// scanner3D.h : main header file for the SCANNER3D application
//

#if !defined(AFX_SCANNER3D_H__D85A743B_34A2_4A3A_8C5A_C1F746F2D92B__INCLUDED_)
#define AFX_SCANNER3D_H__D85A743B_34A2_4A3A_8C5A_C1F746F2D92B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "..\Bredy.Libs\StrRes\StringRes.h"

/////////////////////////////////////////////////////////////////////////////
// CScanner3DApp:
// See scanner3D.cpp for the implementation of this class
//

class CScanner3DApp : public CWinApp
{
  public:
    CScanner3DApp();
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CScanner3DApp)
  public:
    virtual BOOL InitInstance();
    //}}AFX_VIRTUAL
    
    // Implementation
    
    //{{AFX_MSG(CScanner3DApp)
    // NOTE - the ClassWizard will add and remove member functions here.
    //    DO NOT EDIT what you see in these blocks of generated code !
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

extern CStringRes StrRes;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCANNER3D_H__D85A743B_34A2_4A3A_8C5A_C1F746F2D92B__INCLUDED_)
