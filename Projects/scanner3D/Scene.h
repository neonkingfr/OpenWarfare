// Scene.h: interface for the Scene class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCENE_H__B05308F6_83B3_4132_BE56_5BCDE3E44AF2__INCLUDED_)
#define AFX_SCENE_H__B05308F6_83B3_4132_BE56_5BCDE3E44AF2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\Bredy.Libs\AutoArray\geAutoArray.h"
#include "..\Bredy.Libs\3DMath\geVector4.h"
#include "..\Bredy.Libs\3DMath\gePlane.h"
#include ".\BitMap.h"
#include <iostream.h>


struct Stuv
{
  float u,v;
};

struct SFace
{
  int vx[4];
};

geDefineBinMove(Stuv);
geDefineBinMove(SFace);
geDefineBinMove(geVector4);

class Scene  
{
  FILE *logfile;
  bool LoadTexIndex(istream &vrml);
  bool LoadVertices(istream &vrml);
  bool LoadFaces(istream &vrml, geAutoArray<SFace> &arr);
  bool LoadTexcoords(istream& in);
  bool LoadTexture(istream &vrml);
  public:
    geAutoArray<geVector4> vlist;
    geAutoArray<Stuv> tuv;
    geAutoArray<SFace> faces;
    geAutoArray<SFace> uvindex;
    BitMap<unsigned long> tex;
    float minz;
    float maxz;
    int statmaperrors;
    bool halt;
    
  public:
    void ProcessPoints();
    void FindPointsForFace(int fc);
    void Reset();
    virtual void Status(int phase) 
    {}	
    virtual void PointHit(int fc, int x, int y, float depth, geVector4 &normal) 
    {}
    
    bool CheckScene();
    int LoadScene(istream &vrml);
    Scene();
    virtual ~Scene();
};

#endif // !defined(AFX_SCENE_H__B05308F6_83B3_4132_BE56_5BCDE3E44AF2__INCLUDED_)
