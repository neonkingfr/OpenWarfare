// Scene.cpp: implementation of the Scene class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "scanner3D.h"
#include "Scene.h"
#include "Searcher.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Scene::Scene()
{
  
}

Scene::~Scene()
{
  
}

int Scene::LoadScene(istream &vrml)
{
  Searcher texture(CString("Texture2{image"));
  Searcher texcoord(CString("TextureCoordinate2{point["));
  Searcher vectors(CString("Coordinate3{point["));
  Searcher faces(CString("IndexedFaceSet{coordIndex["));
  Searcher texindex(CString("textureCoordIndex["));
  Searcher *list[]=
  {&texture,&texcoord,&vectors,&faces,&texindex};
  
  int p=FileSearch(vrml,list,5);
  while (p!=-1)
  {
    switch (p)
    {
      case 0: if (!LoadTexture(vrml))return p+1;break;
      case 1: if (!LoadTexcoords(vrml)) return p+1;break;
      case 2: if (!LoadVertices(vrml))return p+1;break;
      case 3: if (!LoadFaces(vrml,this->faces)) return p+1;break;
      case 4: if (!LoadFaces(vrml,this->uvindex)) return p+1;break;
    }
    Status(p);
    p=FileSearch(vrml,list,5);
  }
  
  return 0;
}

bool Scene::LoadTexture(istream &vrml)
{
  int w,h,c;
  vrml>>w>>h>>c;
  tex.Create(w,h);
  vrml.eatwhite();
  int x=0, y=h-1;
  hex(vrml);
  while (vrml.get()=='0' && vrml.get()=='x')
  {
    unsigned long data;
    vrml>>data;
    tex(y,x)=data;
    x++;
    if (x>=w) 
    {x-=w;y--;}
    if (y<0) break;
    vrml.eatwhite();
  }
  dec(vrml);
  return true;
}

bool Scene::LoadTexcoords(istream &in)
{
  in.eatwhite();
  while (isdigit(in.peek()) || in.peek()=='.')
  {
    Stuv uv;
    in>>uv.u>>uv.v;
    tuv.Add(uv);
    in.eatwhite();
    if (in.get()!=',') break;
    in.eatwhite();
  }
  return true;
}

bool Scene::LoadFaces(istream &vrml, geAutoArray<SFace> &arr)
{
  int cnt=0;
  int p=-2;
  SFace fc;
  fc.vx[0]=fc.vx[1]=fc.vx[2]=fc.vx[3]=-1;
  vrml>>p;
  while (p!=-2)
  {
    if (p==-1) 
    {
      cnt=0;
      arr.Add(fc);
      fc.vx[0]=fc.vx[1]=fc.vx[2]=fc.vx[3]=-1;
    }
    else
    {
      if (cnt==4) return false;
      fc.vx[cnt]=p;
      cnt++;
    }
    vrml.eatwhite();
    if (vrml.get()!=',') break;
    vrml>>p;
  }
  if (cnt!=0)  arr.Add(fc);
  return true;
}

bool Scene::LoadVertices(istream &vrml)
{
  geVector4 vx;
  vrml>>vx.x>>vx.y>>vx.z;
  vrml.eatwhite();
  while (vrml.get()==',')
  {
    vlist.Add(vx);
    vrml>>vx.x>>vx.y>>vx.z;
    vrml.eatwhite();
  }
  vlist.Add(vx);
  return true;
}

bool Scene::CheckScene()
{
  int vcnt=vlist.GetCount();
  int tcnt=tuv.GetCount();
  int fcnt=faces.GetCount();
  int icnt=uvindex.GetCount();
  int i;
  bool fs=true;
  for (i=0;i<fcnt;i++)
  {
    for (int j=0;j<4;j++)
    {
      int fp=faces[i].vx[j];
      if (fp==-1) break;
      if (fp<0 || fp>=vcnt) return false;
      if (fs)
      {
        maxz=minz=vlist[faces[i].vx[j]].z;
        fs=false;
      }
      else
      {
        float z=vlist[faces[i].vx[j]].z;
        if (maxz<z) maxz=z;
        if (minz>z) minz=z;
      }
    }
  }
  for (i=0;i<icnt;i++)
  {
    for (int j=0;j<4;j++)
    {
      int fp=uvindex[i].vx[j];
      if (fp==-1) break;
      if (fp<0 || fp>=tcnt) return false;
    }
  }
  faces.FreeExtra();
  tuv.FreeExtra();
  uvindex.FreeExtra();
  vlist.FreeExtra();
  return true;
}

void Scene::Reset()
{
  faces.RemoveAll();
  if (tex.IsCreated())tex.Destroy();
  tuv.RemoveAll();
  uvindex.RemoveAll();
  vlist.RemoveAll();
}

static int ptorder[2][3]=
{
  {0,1,2},
  {0,2,3}};

void Scene::FindPointsForFace(int fc)
{
  CRect rc;
  for (int i=0;i<4;i++)
  {
    int p=uvindex[fc].vx[i];
    if (p==-1) break;
    Stuv &uv=tuv[p];
    float u=uv.u*tex.W();
    float v=(1-uv.v)*tex.H();
    if (i==0) 
    {rc.left=rc.right=(int)floor(u+0.5);rc.top=rc.bottom=(int)floor(v+0.5);}
    else
    {
      int ui=(int)floor(u+0.5),vi=(int)floor(v+0.5);
      if (ui<rc.left) rc.left=ui;
      if (ui>rc.right) rc.right=ui;
      if (vi<rc.top) rc.top=vi;
      if (vi>rc.bottom) rc.bottom=vi;
    }
  }
  if ((rc.left<=0 && rc.top>=tex.H()-1) || (rc.Size().cx>5 || rc.Size().cy>5)) 
  {
    statmaperrors++;
    return;
  }
  rc+=CRect(2,2,2,2);
  //  fprintf(logfile,"%8d %3d %3d %3d %3d \n",fc,rc.left,rc.top,rc.right,rc.bottom);
  for (int fp=0;fp<2;fp++)	//triangulizace
  {
    geVector4 pts[3];		//vyzvedni face ze tri bodu (trojuhelnik)
    int pi,ti;
    for (int fz=0;fz<3;fz++)
    {
      pi=faces[fc].vx[ptorder[fp][fz]];	
      ti=uvindex[fc].vx[ptorder[fp][fz]];
      if (pi==-1 || ti==-1) break;
      pts[fz].z=vlist[pi].z;		//souradnice se skladaji u,v,z souradnic
      pts[fz].x=tuv[pi].u*tex.W();  //u,v je transformovano podle velikosti bitmapy
      pts[fz].y=(1.0f-tuv[pi].v)*tex.H();  
    }
    if (fz!=3) break; //neni dalsi face -> konec
    gePlane2V plane(pts[0],pts[1],pts[2]);	//rovina zadana tremi vrcholy
    for (int y=rc.top;y<=rc.bottom;y++)
      for (int x=rc.left;x<=rc.right;x++)
      {
        geLine pline(geVector4(x+0.5f,y+0.5f,0),geVector4(0,0,1),true);
        geVector4 rsk=plane.Intersection(pline);
        if (_finite(rsk.z))
        {
          if (rsk.x>0 && rsk.y>0 && rsk.x<=1-rsk.y)
          {
            geVector4 opt=pline.GetPoint(rsk.z); //vem K z vysledku (mapovano do Z);
            gePlaneGen pgen(plane);
            PointHit(fc,x,y,opt.z,geVector4(pgen.a,pgen.b,pgen.c));			
          }
        }
      }
  }
}

void Scene::ProcessPoints()
{
  //  logfile=fopen("convert.log","w");
  statmaperrors=0;
  halt=false;
  for (int i=0;i<faces.GetCount() && !halt;i++) FindPointsForFace(i);
  //  fclose(logfile);
}

