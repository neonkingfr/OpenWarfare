// FaceCalcs.h: interface for the FaceCalcs class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FACECALCS_H__CB06D4D2_C1C5_438E_8E10_9B0764B69FAF__INCLUDED_)
#define AFX_FACECALCS_H__CB06D4D2_C1C5_438E_8E10_9B0764B69FAF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "scene.h"

class FaceCalcs  
{
  public:
    FaceCalcs(int width, int height, Scene &scn, int fcindex);
    virtual ~FaceCalcs();
    
};

#endif // !defined(AFX_FACECALCS_H__CB06D4D2_C1C5_438E_8E10_9B0764B69FAF__INCLUDED_)
