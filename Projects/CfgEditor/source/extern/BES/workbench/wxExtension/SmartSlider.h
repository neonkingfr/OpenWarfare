
#ifndef SMART_SLIDER_H
#define SMART_SLIDER_H

#include "wx/window.h"

//--------- EVT_SMART_SLIDER_MOVE_END event declaration START ------------------
BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_SMART_SLIDER_MOVE_END, 7790)
END_DECLARE_EVENT_TYPES()


#define EVT_SMART_SLIDER_MOVE_END(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY( \
        wxEVT_SMART_SLIDER_MOVE_END, id, -1, \
        (wxObjectEventFunction)(wxEventFunction)(wxCommandEventFunction)&fn, \
        (wxObject *) NULL \
    ),
//--------- EVT_SMART_SLIDER_MOVE_END event declaration END ------------------

enum //smart slider style
{
	SSS_CURRENT_VALUE_LEFT = 1,
	SSS_CURRENT_VALUE_RIGHT = 2,
};

//=======================================================================
class SmartSlider : public wxWindow
{
public:
	SmartSlider(wxWindow* parent, wxWindowID id, int value, int MinValue, int MaxValue, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, long ExtraStyle = 0);
	SmartSlider(wxWindow* parent, wxWindowID id, float value, float MinValue, float MaxValue, float step, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, long ExtraStyle = 0);
	static void DrawInt(wxDC& dc, const wxRect& rect, int value, int MinValue, int MaxValue, bool DrawValue, bool DuringMoving);
	static void DrawFloat(wxDC& dc, const wxRect& rect, float value, float MinValue, float MaxValue, bool DrawValue, int NumFloatDigits, bool DuringMoving);
	static void Draw(wxDC& dc, const wxRect& rect, float SliderRatePos, const wxString& ValueText, bool DuringMoving);
	static int GetValueAreaWidth(const char* ValueText);
	static int GetValueAreaWidth(int MinValue, int MaxValue);
	static int GetValueAreaWidth(float MinValue, float MaxValue, int NumFloatDigits);
	bool CurrentValueVisible() const;
	void SetNumFloatValueDigits(int NumFloatDigits);
	void RepaintFromClient();
	void SimulateLMBdown(const wxPoint& point);
	inline float GetIntValue(){return ValueInt;}
	inline float GetFloatValue(){return ValueFloat;}
private:
	static void CalculateProportions(const wxSize& size, float SliderRatePos, const char* ValueText, wxRect& TextArea, wxRect& SliderArea, wxRect& SliderRect);
	static float GetSliderRatePos(int value, int MinValue, int MaxValue);
	static float GetSliderRatePos(float value, float MinValue, float MaxValue);
	static wxString GetValueText(int value);
	static wxString GetValueText(float value, int NumFloatDigits);
	bool SliderHitTest(const wxPoint& pos, int* OffsetFromCenter);
	unsigned int ScreenPosToTick(const wxPoint& pos);
	void BeginDrag();
	void EndDrag();
	void SetValueFromTick(unsigned int tick);
	void OnPaint(wxPaintEvent& event);
	void OnLMBdown(wxMouseEvent& event);
	void OnLMBup(wxMouseEvent& event);
	void OnMouseMove(wxMouseEvent& event);
	void OnMouseCaptureLost(wxMouseCaptureLostEvent& event);
	int ValueInt;
	int MinValueInt;
	int MaxValueInt;
	float ValueFloat;
	float MinValueFloat;
	float MaxValueFloat;
	unsigned int NumTicks;
	bool UsingFloats;
	long ExtraStyle;
	int NumFloatDigits;
	bool DraggingSlider;
	int DragOffset;
//	uint CurrentTick;
	DECLARE_EVENT_TABLE()
};

#endif