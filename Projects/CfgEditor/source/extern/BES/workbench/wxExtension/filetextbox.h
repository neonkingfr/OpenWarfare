
#ifndef FILETEXTBOX_H
#define FILETEXTBOX_H

#include "WxExtension/TextBoxWithButton.h"

const int FTB_DIALOGTYPE_FILE = 1;	//type file
const int FTB_DIALOGTYPE_DIR = 2;	//type directory

//====================================================================================
class FileTextBox: public TextboxWithButton
{
public:
	wxString FileMask;
	wxString DefaultDir;
#ifdef WORKBENCH
	RichFileDialog* ExternDialog;
	FileTextBox(wxWindow *parent, wxWindowID id, RichFileDialog* ExternDialog, wxPoint pos, wxSize size, int TextBoxStyle, char** ButtonXPMBitmap = NULL, int ExtraFlags = FTB_DIALOGTYPE_FILE);
	FileTextBox(wxWindow *parent, wxWindowID id, RichFileDialog* ExternDialog, wxArrayString& choices, wxPoint pos, wxSize size );
#endif
	FileTextBox(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize size, int TextBoxStyle, char** ButtonXPMBitmap = NULL, int ExtraFlags = FTB_DIALOGTYPE_FILE);
	~FileTextBox();
private:
	int ExtraFlags;
	void OnButtonClick(wxCommandEvent &event);
	void OnSetFromCombo(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

#endif