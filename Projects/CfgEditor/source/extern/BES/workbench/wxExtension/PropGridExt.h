
#ifndef PROP_GRID_EXT_H
#define PROP_GRID_EXT_H

#include "wx/propgrid/propgrid.h"
#include <wx/propgrid/propdev.h>
#include "wx/propgrid/advprops.h"

#include "WxExtension/Validators.h"

//slider validatory nepotrebuje ale pouzijeme ich ako snad jedinu moznost ako dostat do samotneho slideru potrebne hodnoty
//================================================================================
class IntSliderValidator : public IntValidator
{
public:
	IntSliderValidator(int MinValue, int MaxValue);
	virtual wxObject* Clone() const;
	inline int GetMinValue(){return MinValue;}
	inline int GetMaxValue(){return MaxValue;}
protected:
};

//================================================================================
class FloatSliderValidator : public FloatValidator
{
public:
	FloatSliderValidator(float MinValue, float MaxValue, float step);
	virtual wxObject* Clone() const;
	inline float GetMinValue(){return MinValue;}
	inline float GetMaxValue(){return MaxValue;}
	inline float GetStep(){return step;}
protected:
	float step;
};

//===============================================================================
class WXDLLIMPEXP_PG wxPGIntSliderEditor : public wxPGEditor
{
    WX_PG_DECLARE_EDITOR_CLASS()
public:
    wxPGIntSliderEditor() {}
    virtual ~wxPGIntSliderEditor();
    WX_PG_IMPLEMENT_EDITOR_CLASS_STD_METHODS()

    virtual void DrawValue( wxDC& dc, wxPGProperty* property, const wxRect& rect ) const;
    virtual void SetControlIntValue( wxWindow* ctrl, int value ) const;
};

WX_PG_DECLARE_EDITOR(IntSlider);
WX_PG_DECLARE_PROPERTY(wxIntSliderProperty,long,0)

//===============================================================================
class WXDLLIMPEXP_PG wxPGFloatSliderEditor : public wxPGEditor
{
    WX_PG_DECLARE_EDITOR_CLASS()
public:
    wxPGFloatSliderEditor() {}
    virtual ~wxPGFloatSliderEditor();
    WX_PG_IMPLEMENT_EDITOR_CLASS_STD_METHODS()

    virtual void DrawValue( wxDC& dc, wxPGProperty* property, const wxRect& rect ) const;
    virtual void SetControlIntValue( wxWindow* ctrl, int value ) const;
};

WX_PG_DECLARE_EDITOR(FloatSlider);
WX_PG_DECLARE_PROPERTY(wxFloatSliderProperty,double,0)

#endif