
#include "SmartSlider.h"
#include "SmartSlider.h"
#include "common/WxNativeConvert.h"
#include "common/NativeUtil.h"
#include "common/enf_memory.h"
#include "common/enf_types.h"

#include "wx/dcclient.h"

//definuje novy typ eventu nastavitelny makrom EVT_SMART_SLIDER_MOVE_END
DEFINE_EVENT_TYPE(wxEVT_SMART_SLIDER_MOVE_END)

BEGIN_EVENT_TABLE(SmartSlider, wxWindow)
	EVT_LEFT_DOWN(SmartSlider::OnLMBdown)
	EVT_LEFT_UP(SmartSlider::OnLMBup)
	EVT_MOTION(SmartSlider::OnMouseMove)
	EVT_MOUSE_CAPTURE_LOST(SmartSlider::OnMouseCaptureLost) 
	EVT_PAINT(SmartSlider::OnPaint)
END_EVENT_TABLE()

//--------------------------------------------------------------------------------------------------
SmartSlider::SmartSlider(wxWindow* parent, wxWindowID id, int value, int MinValue, int MaxValue, const wxPoint& pos, const wxSize& size, long style, long ExtraStyle)
	:wxWindow(parent, id, pos, size, style)
{
	enf_assert(MaxValue > MinValue);
	enf_assert(value >= MinValue);
	enf_assert(value <= MaxValue);

	ValueInt = value;
	MinValueInt = MinValue;
	MaxValueInt = MaxValue;
	ValueFloat = 0.0f;
	MinValueFloat = 0.0f;
	MaxValueFloat = 0.0f;
	NumTicks = MaxValue - MinValue;
	UsingFloats = false;
	NumFloatDigits = 0;
	DraggingSlider = false;
	DragOffset = 0;
}

//--------------------------------------------------------------------------------------------------
SmartSlider::SmartSlider(wxWindow* parent, wxWindowID id, float value, float MinValue, float MaxValue, float step, const wxPoint& pos, const wxSize& size, long style, long ExtraStyle)
	:wxWindow(parent, id, pos, size, style)
{
	enf_assert(MaxValue > MinValue);
	enf_assert(value >= MinValue);
	enf_assert(value <= MaxValue);
	enf_assert(step > 0);
	enf_assert(step < MaxValue);

	ValueFloat = value;
	MinValueFloat = MinValue;
	MaxValueFloat = MaxValue;
	ValueInt = 0;
	MinValueInt = 0;
	MaxValueInt = 0;

	double range = (double)MaxValue - (double)MinValue;
	double fNumTicks = ((double)range / (double)step);
	int iNumTicks = (int)fNumTicks;

	if(((double)iNumTicks) < fNumTicks)	//ked nesedi presne na ticky tak pridame jeden naviac
		iNumTicks++;

	NumTicks = iNumTicks;
	UsingFloats = true;
	this->ExtraStyle = ExtraStyle;
	NumFloatDigits = 2;
	DraggingSlider = false;
	DragOffset = 0;
}

//--------------------------------------------------------------------------------------------------
bool SmartSlider::CurrentValueVisible() const
{
	if((ExtraStyle & SSS_CURRENT_VALUE_LEFT) || (ExtraStyle & SSS_CURRENT_VALUE_RIGHT))
		return true;

	return false;
}

//--------------------------------------------------------------------------------------------------
bool SmartSlider::SliderHitTest(const wxPoint& pos, int* OffsetFromCenter)
{
	wxRect TextArea;
	wxRect SliderArea;
	wxRect SliderRect;

	if(UsingFloats)
		CalculateProportions(GetClientSize(), GetSliderRatePos(ValueFloat, MinValueFloat, MaxValueFloat), GetValueText(ValueInt), TextArea, SliderArea, SliderRect);
	else
		CalculateProportions(GetClientSize(), GetSliderRatePos(ValueInt, MinValueInt, MaxValueInt), GetValueText(ValueFloat, NumFloatDigits), TextArea, SliderArea, SliderRect);

	if(SliderRect.Contains(pos))
	{
		int SliderHCenter = SliderRect.x + ((SliderRect.GetWidth() - 1) / 2);
		*OffsetFromCenter = pos.x - SliderHCenter;
		return true;
	}

	return false;
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::SimulateLMBdown(const wxPoint& point)
{
	if(SliderHitTest(point, &DragOffset))
	{
		CaptureMouse();
		BeginDrag();
	}
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::OnLMBdown(wxMouseEvent& event)
{
	wxPoint MousePos = event.GetPosition();
	SimulateLMBdown(MousePos);
	event.Skip();
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::OnLMBup(wxMouseEvent& event)
{
	if(GetCapture() == this)
		ReleaseMouse();

	EndDrag();
	event.Skip();
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::OnMouseMove(wxMouseEvent& event)
{
	wxPoint MousePos = event.GetPosition();

	if(DraggingSlider)
	{
		uint tick = ScreenPosToTick(wxPoint(MousePos.x - DragOffset, MousePos.y));
		SetValueFromTick(tick);
		RepaintFromClient();
	}

	event.Skip();
}

//-----------------------------------------------------------------------------------------
void SmartSlider::OnMouseCaptureLost(wxMouseCaptureLostEvent& event)
{
	EndDrag();
	//nesmie byt volane event.Skip() resp musi tu byt aj tento prazdny event handler ked sa pouziva CaptureMouse a ReleaseCaptute().
	//inac to vyhadzuje assert z wx z dovodu ze event je nespracovany
//	event.Skip();
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::OnPaint(wxPaintEvent& event)
{
	wxPaintDC PaintDC(this);
	wxRect rect(wxPoint(0, 0), GetClientSize());
	bool DrawCurrentValue = CurrentValueVisible();

	if(UsingFloats)
		DrawFloat(PaintDC, rect, ValueFloat, MinValueFloat, MaxValueFloat, DrawCurrentValue, NumFloatDigits, DraggingSlider);
	else
		DrawInt(PaintDC, rect, ValueInt, MinValueInt, MaxValueInt, DrawCurrentValue, DraggingSlider);

	event.Skip();
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::RepaintFromClient()
{
	wxClientDC ClientDC(this);
	wxRect rect(wxPoint(0, 0), GetClientSize());
	bool DrawCurrentValue = CurrentValueVisible();

	if(UsingFloats)
		DrawFloat(ClientDC, rect, ValueFloat, MinValueFloat, MaxValueFloat, DrawCurrentValue, NumFloatDigits, DraggingSlider);
	else
		DrawInt(ClientDC, rect, ValueInt, MinValueInt, MaxValueInt, DrawCurrentValue, DraggingSlider);
}

//staticka funkcia. je mozne vykreslit slider i z vonka
//--------------------------------------------------------------------------------------------------
void SmartSlider::DrawInt(wxDC& dc, const wxRect& rect, int value, int MinValue, int MaxValue, bool DrawValue, bool DuringMoving)
{
	wxString ValueText = ItoA(value);
	Draw(dc, rect, GetSliderRatePos(value, MinValue, MaxValue), GetValueText(value), DuringMoving);
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::DrawFloat(wxDC& dc, const wxRect& rect, float value, float MinValue, float MaxValue, bool DrawValue, int NumFloatDigits, bool DuringMoving)
{
	Draw(dc, rect, GetSliderRatePos(value, MinValue, MaxValue), GetValueText(value, NumFloatDigits), DuringMoving);
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::Draw(wxDC& dc, const wxRect& rect, float SliderRatePos, const wxString& ValueText, bool DuringMoving)
{
	wxRect TextArea;
	wxRect SliderArea;
	wxRect SliderRect;
	CalculateProportions(rect.GetSize(), SliderRatePos, ValueText, TextArea, SliderArea, SliderRect);

/*	const wxFont& font = dc.GetFont();
	wxCoord w;
	this->GetTextExtent(ValueText, &w, NULL, NULL, NULL, &font);
	int ValueAreaWidth = w + 8;*/
	int ValueAreaWidth = GetValueAreaWidth(ValueText);

	wxPoint& BasePos = rect.GetPosition();
	wxSize size = rect.GetSize();

	wxPen OldPen = dc.GetPen();
	wxBrush OldBrush = dc.GetBrush();

	dc.SetBrush(*wxWHITE_BRUSH);
	dc.SetPen(*wxWHITE_PEN);
	dc.SetFont(*wxNORMAL_FONT);
	dc.DrawRectangle(rect);	//namiesto dc.Clear() ktore sa pouzit nesmie
	dc.SetPen(*wxBLACK_PEN);



	int LineX1 = BasePos.x + SliderArea.x;
	int LineY1 = BasePos.y + SliderArea.y + (SliderArea.GetHeight() / 2);
	int LineX2 = LineX1 + SliderArea.GetWidth();
	int LineY2 = LineY1;

	dc.SetPen(*wxLIGHT_GREY_PEN);
	dc.DrawLine(LineX1, LineY1, LineX2, LineY2);
	dc.SetPen(*wxGREY_PEN);
	dc.DrawLine(LineX1, LineY1 + 1, LineX2, LineY2 + 1);
	
	if(DuringMoving)
		dc.SetBrush(wxBrush(wxColour(255, 255, 0)));
	else
		dc.SetBrush(*wxLIGHT_GREY_BRUSH);

	dc.DrawRoundedRectangle(BasePos.x + SliderRect.x, BasePos.y + SliderRect.y, SliderRect.GetWidth(), SliderRect.GetHeight(), 2);
//	dc.DrawRoundedRectangle(SliderLeft + 1, SliderTop + 1, SliderWidth - 2, SliderHeight - 2, 1);

	if(ValueText)
		dc.DrawText(ValueText, BasePos.x + TextArea.x + 3, BasePos.y + TextArea.y + 2);

	dc.SetPen(OldPen);
	dc.SetBrush(OldBrush);
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::SetNumFloatValueDigits(int NumFloatDigits)
{
	enf_assert(NumFloatDigits >= 0);
	this->NumFloatDigits = NumFloatDigits;
}

//--------------------------------------------------------------------------------------------------
int SmartSlider::GetValueAreaWidth(const char* ValueText)
{
	return 32;//(8 * strlen(ValueText)) + 4;
}

//--------------------------------------------------------------------------------------------------
int SmartSlider::GetValueAreaWidth(int MinValue, int MaxValue)
{
	return 0;
}

//--------------------------------------------------------------------------------------------------
int SmartSlider::GetValueAreaWidth(float MinValue, float MaxValue, int NumFloatDigits)
{
	return 0;
}

//--------------------------------------------------------------------------------------------------
float SmartSlider::GetSliderRatePos(int value, int MinValue, int MaxValue)
{
	uint irange = MaxValue - MinValue;
	double range = (double)irange;
	uint pos = value - MinValue;
	return (float)((double)pos / range);
}

//--------------------------------------------------------------------------------------------------
float SmartSlider::GetSliderRatePos(float value, float MinValue, float MaxValue)
{
	double range = (MaxValue - MinValue);
	return (float)(((double)value - (double)MinValue) / range);
}

//--------------------------------------------------------------------------------------------------
wxString SmartSlider::GetValueText(int value)
{
	return ItoA(value);
}

//--------------------------------------------------------------------------------------------------
wxString SmartSlider::GetValueText(float value, int NumFloatDigits)
{
	return FtoA(value, NumFloatDigits);
}

//--------------------------------------------------------------------------------------------------
uint SmartSlider::ScreenPosToTick(const wxPoint& pos)
{
	wxRect TextArea;
	wxRect SliderArea;
	wxRect SliderRect;

	if(UsingFloats)
		CalculateProportions(GetClientSize(), GetSliderRatePos(ValueFloat, MinValueFloat, MaxValueFloat), GetValueText(ValueFloat, NumFloatDigits), TextArea, SliderArea, SliderRect);
	else
		CalculateProportions(GetClientSize(), GetSliderRatePos(ValueInt, MinValueInt, MaxValueInt), GetValueText(ValueInt), TextArea, SliderArea, SliderRect);

	int SliderHalfWidth = (SliderRect.GetWidth() - 1) / 2;
	int min = SliderArea.x;// - SliderHalfWidth;
	int max = SliderArea.x + SliderArea.GetWidth();// - SliderHalfWidth;
	int range = max - min;
	int spos = pos.x - min;

	if(spos < 0)
		spos = 0;

	if(spos > range)
		spos = range;

	double rate = (double)spos / (double) range;
	enf_assert(rate >= 0.0f);
	enf_assert(rate <= 1.0f);

	if(NumTicks < 1000)
	{
		double fticks = RoundUp2Double(rate * (double)NumTicks);
		return (uint)fticks;
	}
	else
		return RateOfUint(NumTicks, rate);
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::SetValueFromTick(uint tick)
{
	double rate = (double)tick / (double)NumTicks;

	if(UsingFloats)
	{
		double range = (double)(MaxValueFloat - MinValueFloat);
		double rangepos = range * rate;

		if(rangepos > range)
			rangepos = range;

		ValueFloat = MinValueFloat + (float)rangepos;
	}
	else
	{
		uint range = MaxValueInt - MinValueInt;

		if(NumTicks < 1000)
		{
			uint rangepos = (int)RoundUp2((double)range * rate);
			uint val = MinValueInt + rangepos;
			ValueInt = val;
		}
		else
		{
			uint val = MinValueInt + RateOfUint(range, rate);
			ValueInt = val;
		}
	}
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::CalculateProportions(const wxSize& size, float SliderRatePos, const char* ValueText, wxRect& TextArea, wxRect& SliderArea, wxRect& SliderRect)
{
	enf_assert(SliderRatePos >= 0.0f);
	enf_assert(SliderRatePos <= 1.0f);

/*	const wxFont& font = dc.GetFont();
	wxCoord w;
	this->GetTextExtent(ValueText, &w, NULL, NULL, NULL, &font);
	int ValueAreaWidth = w + 8;*/
	int ValueAreaWidth = GetValueAreaWidth(ValueText);

	TextArea.SetSize(size);
	TextArea.SetWidth(ValueAreaWidth);

	int HorzBorder = 7;
	SliderArea.x = ValueAreaWidth + HorzBorder;
	SliderArea.SetSize(size);
	SliderArea.SetWidth(SliderArea.GetWidth() - SliderArea.x - HorzBorder);

	float fSliderPos = RoundUp2((SliderRatePos * (float)SliderArea.GetWidth()));
	int SliderPos = fSliderPos;

	int SliderHeight = size.y - 2;
	float fSliderWidth = (float)SliderHeight * 0.6666;
	int SliderWidth = RoundUp2(fSliderWidth);

	if(!(SliderWidth & 1))
		SliderWidth++;

	int SliderHalfWidth = (SliderWidth - 1) / 2;

	int SliderLeft = SliderArea.x + (SliderPos - SliderHalfWidth);
	int SliderTop = 1;

	SliderRect.x = SliderLeft;
	SliderRect.y = SliderTop;
	SliderRect.SetWidth(SliderWidth);
	SliderRect.SetHeight(SliderHeight);
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::BeginDrag()
{
	DraggingSlider = true;
	RepaintFromClient();
}

//--------------------------------------------------------------------------------------------------
void SmartSlider::EndDrag()
{
	DraggingSlider = false;
	RepaintFromClient();

	wxCommandEvent CustomEvent(wxEVT_SMART_SLIDER_MOVE_END);
	CustomEvent.SetId(this->GetId());
	CustomEvent.SetEventObject(this);
//	CustomEvent.SetString(ParentProperty->GetName());
//	PGPropertyValueString val(GetPropertyValueAsString(ParentProperty));
//	WBPropGridEventData EventData(ParentProperty, &val);
//	CustomEvent.SetClientObject(&EventData);
	GetEventHandler()->ProcessEvent(CustomEvent);
}