
#include "wbPrecomp.h"
#include "FileTextBox.h"
#include "wx/filedlg.h"
#include "wx/dirdlg.h"
#include "wx/msgdlg.h"
#include "Common/NativeUtil.h"
#include "Common/WinApiUtil.h"

#ifdef WORKBENCH
#include "Layout/EngineImplement.h"
#include "WxExtension/RichFileDialog.h"
#endif

//====================================================================================
//==== edit control with filerequester ===============================================
//====================================================================================
BEGIN_EVENT_TABLE(FileTextBox, TextboxWithButton)
	EVT_COMBOBOX(-1, FileTextBox::OnSetFromCombo)
	EVT_TEXT_ENTER(-1, FileTextBox::OnSetFromCombo)
END_EVENT_TABLE()

#ifdef WORKBENCH
//----------------------------------------------------------------------------------------
FileTextBox::FileTextBox(wxWindow* parent, wxWindowID id, RichFileDialog* ExternDialog, wxPoint pos, wxSize size, int TextBoxStyle, char** ButtonXPMBitmap, int ExtraFlags) : TextboxWithButton(parent, id, pos, size, TextBoxStyle, ButtonXPMBitmap)
{
	this->ExternDialog = ExternDialog;
	FileMask = wxT("All files (*.*)|*.*");	//defaultne sa budu dat vybrat vsetky subory
	this->ExtraFlags = ExtraFlags;
}

//----------------------------------------------------------------------------------------
FileTextBox::FileTextBox(wxWindow* parent, wxWindowID id, RichFileDialog* ExternDialog, wxArrayString& choices, wxPoint pos, wxSize size) : TextboxWithButton(parent, id, pos, size, choices, 0, NULL/*fileopen_xpm*/)
{
	this->ExternDialog = ExternDialog;
	FileMask = wxT("All files (*.*)|*.*");	//defaultne sa budu dat vybrat vsetky subory
}
#endif

//----------------------------------------------------------------------------------------
FileTextBox::FileTextBox(wxWindow* parent, wxWindowID id, wxPoint pos, wxSize size, int TextBoxStyle, char** ButtonXPMBitmap, int ExtraFlags) : TextboxWithButton(parent, id, pos, size, TextBoxStyle, ButtonXPMBitmap)
{
	FileMask = wxT("All files (*.*)|*.*");	//defaultne sa budu dat vybrat vsetky subory
	this->ExtraFlags = ExtraFlags;
}

//----------------------------------------------------------------------------------------
FileTextBox::~FileTextBox()
{
}

//----------------------------------------------------------------------------------------
void FileTextBox::OnSetFromCombo(wxCommandEvent& event)
{
	wxCommandEvent eventCustom(wxEVT_FILETEXTBOX);
	eventCustom.SetId(this->GetId());
	eventCustom.SetEventObject(this);
	wxPostEvent(this, eventCustom);
}

//----------------------------------------------------------------------------------------
void FileTextBox::OnButtonClick(wxCommandEvent &event)
{
#ifdef WORKBENCH
	if(ExternDialog)
	{
		wxString CurrentFilename = GetValue();
		CurrentFilename.LowerCase();
//		ExternDialog->SetDefaultDir(DefaultDir);	//uz ho ma nastaveny obycajne tam kde vznika. ked subor v ImputValue nexistuje tak sa otvara default dir

		if(!CurrentFilename.IsEmpty())	//nevrazame mu prazdnu trasu pretoze inak by tam hodil default directory a to chceme iba pri prvom pouziti dialogu ked tam este ziadna rozumna trasa neni. potom si uz uchovava to co bolo naposledy zvolene
			ExternDialog->SetInputValue(CurrentFilename);

		if(ExternDialog->ShowAsModal() == wxID_OK)
		{
			SetValue(ExternDialog->GetResultValue());
			wxCommandEvent eventCustom(wxEVT_FILETEXTBOX);
			eventCustom.SetId(this->GetId());
			eventCustom.SetEventObject(this);
			wxPostEvent(this, eventCustom);
		}
		return;
	}
#endif
	wxString ResultDir;
	int ResultID = -1;
	wxString CurrentPath = GetValue();
	wxString AbsOpenPath;
	wxString AbsDefaultDir;

#ifdef WORKBENCH
	wxString AbsDefaultDir = g_EngineImpl->GetAbsolutePath(DefaultDir.c_str());
	
	if(!CurrentPath.IsEmpty())
		AbsOpenPath = g_EngineImpl->GetAbsolutePath(CurrentPath.c_str());
#endif

	if(ExtraFlags & FTB_DIALOGTYPE_FILE)
	{
		wxFileDialog dialog(	this,	_T("Select file to open"),	_T(""), _T(""), FileMask, wxOPEN);

		if(AbsOpenPath.IsEmpty())
			dialog.SetDirectory(AbsDefaultDir);
		else
			dialog.SetPath(AbsOpenPath);

		ResultID = dialog.ShowModal();

		if(ResultID == wxID_OK)
			ResultDir = dialog.GetPath();
	}
	else if(ExtraFlags & FTB_DIALOGTYPE_DIR)
	{
		if(taFileExists(CurrentPath))
			AbsOpenPath = CurrentPath;

		wxString Inputpath;

		if(AbsOpenPath.IsEmpty())
			Inputpath = AbsDefaultDir;
		else
			Inputpath = AbsOpenPath;

		wxDirDialog dialog(this, "Choose a directory", Inputpath, wxDD_DEFAULT_STYLE, wxDefaultPosition, wxDefaultSize);

/*		if(AbsOpenPath.IsEmpty())
			dialog.SetPath(AbsDefaultDir);
		else
			dialog.SetPath(AbsOpenPath);*/

		ResultID = dialog.ShowModal();

		if(ResultID == wxID_OK)
			ResultDir = dialog.GetPath();
	}

	if(ResultID == wxID_OK)
	{
		char ResultPath[256];
		strcpy(ResultPath, ResultDir.ToAscii());
		ReplaceCharacters(ResultPath, "\\", "/");
		ResultDir = ResultPath;
		ResultDir.MakeLower();
		wxString ResultDirRel;

#ifdef WORKBENCH
		ResultDirRel = g_EngineImpl->GetRelativePath(ResultDir.c_str());
#else
		ResultDirRel = ResultDir;
#endif

		if(!ResultDirRel.IsEmpty() || (ExtraFlags & FTB_DIALOGTYPE_DIR))	//pre adresar je validna trasa aj prazdny string pretoze je relativna
		{
	//		assert(!ResultDir.IsEmpty());
			SetValue(ResultDirRel);

			wxCommandEvent eventCustom(wxEVT_FILETEXTBOX);
			eventCustom.SetId(this->GetId());
			eventCustom.SetEventObject(this);
			wxPostEvent(this, eventCustom);
		}
		else
		{
			wxString msg = ResultDir + " is not valid path!";
			::wxMessageBox(msg, "Invalid path", wxOK|wxICON_INFORMATION, this);
		}
	}
	else
	{
		wxCommandEvent eventCustom(wxEVT_FILETEXTBOX);
		eventCustom.SetId(this->GetId());
		eventCustom.SetEventObject(this);
		eventCustom.SetInt(-1);
		wxPostEvent(this, eventCustom);
	}

	event.Skip();
}