
#ifndef VALIDATORS_H
#define VALIDATORS_H

#include "wx/validate.h"

//================================================================================
class IntValidator : public wxValidator
{
public:
	IntValidator(int MinValue, int MaxValue);
	virtual wxObject* Clone() const;
	virtual bool Validate(wxWindow* parent);
protected:
	int MinValue, MaxValue;
};

//================================================================================
class EnumIntValidator : public IntValidator
{
public:
	EnumIntValidator(int MinValue, int MaxValue);
	virtual wxObject* Clone() const;
	virtual bool Validate(wxWindow* parent);
	void AddEnum(const char* str);
private:
	wxArrayString enums;
};

//================================================================================
class FloatValidator : public wxValidator
{
public:
	FloatValidator(float MinValue, float MaxValue);
	virtual wxObject* Clone() const;
	virtual bool Validate(wxWindow* parent);
protected:
	float MinValue, MaxValue;
};
/*
//================================================================================
class FileValidator : public wxValidator
{
public:
	FileValidator(bool DirectoryFiles = false);
	virtual wxObject* Clone() const;
	virtual bool Validate(wxWindow* parent);
	void AddExtension(const char* str);
private:
	bool DirectoryFiles;
	wxArrayString extensions;
};
*/
#endif