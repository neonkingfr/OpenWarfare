
#include "RichComboBox.h"
#include "common/WinApiUtil.h"

BEGIN_EVENT_TABLE(RichComboBox, wxComboBox)
//	EVT_SET_FOCUS(RichComboBox::OnSetFocus)
	EVT_KILL_FOCUS(RichComboBox::OnLostFocus)
	EVT_LEFT_DOWN(RichComboBox::OnLMBdown)
//	EVT_TEXT(-1, RichComboBox::OnTextChanging)
	EVT_PAINT(RichComboBox::OnPaint)
END_EVENT_TABLE()

//----------------------------------------------------------------------------------------
RichComboBox::RichComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style, const int InpType, wxString MinValue, wxString MaxValue, bool ForceNicerBorder) : wxComboBox(parent, id, value, pos, size, n, choices, style)
{
	this->ForceNicerBorder = ForceNicerBorder;
	InputType = InpType;
	MaxVal = MinValue;
	MaxVal = MaxValue;
	InitSize = size;
	ClickTime = 0;
//	LMB = false;
}

//----------------------------------------------------------------------------------------
RichComboBox::RichComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style, const int InpType, wxString MinValue, wxString MaxValue, const wxValidator& validator, const wxString& name, bool ForceNicerBorder)
	:wxComboBox(parent, id, value, pos, size, choices, style, validator, name)
{
	this->ForceNicerBorder = ForceNicerBorder;
	InputType = InpType;
	MaxVal = MinValue;
	MaxVal = MaxValue;
	InitSize = size;
	ClickTime = 0;
//	LMB = false;
}

//----------------------------------------------------------------------------------------
RichComboBox::~RichComboBox()
{
}

//----------------------------------------------------------------------------------------
void RichComboBox::OnLMBdown(wxMouseEvent& event)
{
	ClickTime = (uint)::timeGetTime();
	event.Skip();
}
/*
//----------------------------------------------------------------------------------------
void RichComboBox::OnSetFocus(wxFocusEvent& event)
{
	event.Skip();
}*/

//----------------------------------------------------------------------------------------
void RichComboBox::OnLostFocus(wxFocusEvent& event)
{
	long style = GetWindowStyle();

	if(!(style & wxCB_READONLY) /*&& (style & wxCB_DROPDOWN) /*&& (style & wxTE_PROCESS_ENTER)*/)
	{
		uint CurTime = (uint)::timeGetTime();
		uint diff = CurTime - ClickTime;

		if(diff > 100)	//pri rozkliknuti comba nastava lost focus takze tento event je v takom pripade neziaduci
		{
			wxCommandEvent MyEvent(wxEVT_COMMAND_TEXT_ENTER, m_windowId);
			const int sel = GetSelection();
			MyEvent.SetInt(sel);
			MyEvent.SetString(GetValue());
			InitCommandEventWithItems(MyEvent, sel);
			ProcessCommand(MyEvent);	//vola GetEventHandler()->ProcessEvent(MyEvent) a nic ineho v tejto verzii;
		}
	}

	event.Skip();
}
/*
//----------------------------------------------------------------------------------------
void RichComboBox::OnTextChanging(wxCommandEvent &event)
{
	wxString before = event.GetString();
	wxString after = CorrectizeStringFromInput(before, InputType);

	if(before != after)
	{
		SetValue(after);
		SetSelection(after.Length(), after.Length());
	}
	event.Skip();
}*/

//----------------------------------------------------------------------------------------
void RichComboBox::OnPaint(wxPaintEvent& event)
{
/*	if(ForceNicerBorder)
	{
		COMBOBOXINFO cbi;	//asi to neni prave spravne miesto ale jedine co zabera :(
		ZeroMemory(&cbi, sizeof(COMBOBOXINFO));
		cbi.cbSize = sizeof(COMBOBOXINFO);
		::GetComboBoxInfo((HWND)this->GetHWND(), &cbi);
		wxSize sz = GetClientSize();
		SendMessage(cbi.hwndCombo, CB_SETITEMHEIGHT, (WPARAM)-1, (LPARAM)InitSize.y - 4);	//nastavi skurvenu vysku komba ktora sa inac neda zmenit. -1 znamena combo. ine cislo je item po rozbaleni
		SetWindowPos(cbi.hwndItem, HWND_TOP, 1, 1, sz.x - 19, InitSize.y - 1, 0);						//vytiahneme editbox z comba do popredia a nastavime pos a size
	}*/
	event.Skip();
}
