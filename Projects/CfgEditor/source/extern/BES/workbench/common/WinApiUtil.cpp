
#include "wbPrecomp.h"
#include "WinApiUtil.h"
#include <sys/stat.h>	//stat
#include <sys/types.h>	//_stat
#include "NativeUtil.h"

#pragma warning(disable : 4995)	//vypne dementny warnig ktory neviem co ma znamenat

// ----------------------------------------------------------------------------
void taShowCursor(bool status)
{
	int cursor;

	if(status == true)
	{
		int cursor = 0;
		while(cursor <= 0)
			cursor = ::ShowCursor(true);
	}
	else
	{
		cursor = 1;
		while(cursor >= 0)
			cursor = ::ShowCursor(false);
	}
}

//------------------------------------------------------------------------------
bool taFileExists(const char* fileName)
{
	if(::GetFileAttributes(fileName) == INVALID_FILE_ATTRIBUTES)
		return false;

	return true;
}

//------------------------------------------------------------------------------
bool taIsDirectory(const char* fileName)
{
	DWORD attr = ::GetFileAttributes(fileName);

	if(attr == INVALID_FILE_ATTRIBUTES)	//neexistuje
		return false;

	return ((attr & FILE_ATTRIBUTE_DIRECTORY) != 0);
}

//-----------------------------------------------------------------------------
void taGetAplicationPath(char* str)
{
	char paths[1024], path[1024];
	LPTSTR ptr = NULL;
	GetModuleFileName(NULL, paths, 1024);
	GetFullPathName(paths, 1024, path, &ptr);
	*ptr = 0;
	ReplaceCharacters(path, "\\", "/");
	taToLower(path);
	strcpy(str, path);
}

//-----------------------------------------------------------------------------
void taGetAplicationDir(char* str)
{
	char paths[1024], path[1024];
	LPTSTR ptr = NULL;
	GetModuleFileName(NULL, paths, 1024);
	GetFullPathName(paths, 1024, path, &ptr);
	*(ptr - 1) = 0;
	ReplaceCharacters(path, "\\", "/");
	taToLower(path);
	strcpy(str, path);
}

//-----------------------------------------------------------------------------
bool GetFileTimes(const char* FileName, FILETIME* CreationTime, FILETIME* LastAccesTime, FILETIME* LastWriteTime)
{
	HANDLE hFile = CreateFile(FileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if(hFile == INVALID_HANDLE_VALUE) 
		return false;

	BOOL res = GetFileTime(hFile,	CreationTime, LastAccesTime, LastWriteTime);
	CloseHandle(hFile);
	return (res == TRUE);
}

//-----------------------------------------------------------------------------
bool WriteTimesToFile(const char* filename, FILETIME* CreationTime, FILETIME* LastAccesTime, FILETIME* LastWriteTime)
{
	HANDLE hFile = CreateFile(filename, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_WRITE_ATTRIBUTES, NULL);
	if(hFile == INVALID_HANDLE_VALUE) 
		return false;

	bool TimeSet = (SetFileTime(hFile, CreationTime, LastAccesTime, LastWriteTime) != 0);
	bool FileClosed = (CloseHandle(hFile) != 0);
	return (TimeSet && FileClosed);
}

//-----------------------------------------------------------------------------
void ConvertTimeToString(const FILETIME* time, char* result)
{
	SYSTEMTIME stUTC, stLocal;
	// Convert the last-write time to local time.
	FileTimeToSystemTime(time, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	// Build a string showing the date and time.
	wsprintf(result, TEXT("%02d/%02d/%d  %02d:%02d"),
		stLocal.wMonth, stLocal.wDay, stLocal.wYear,
		stLocal.wHour, stLocal.wMinute);
}

//-----------------------------------------------------------------------------
void taSystemTimeToStr(SYSTEMTIME* time, char* DateString, char* TimeString)
{
	if(DateString)
		wsprintf(DateString, TEXT("%02d/%02d/%d"), time->wDay, time->wMonth, time->wYear);

	if(TimeString)
		wsprintf(TimeString, TEXT("%02d:%02d"), time->wHour, time->wMinute);
}

//-----------------------------------------------------------------------------
void taSystemTimeToStr(SYSTEMTIME* time, char* DateAndTimeString)
{
	if(DateAndTimeString)
		wsprintf(DateAndTimeString, TEXT("%02d/%02d/%d %02d:%02d"), time->wDay, time->wMonth, time->wYear, time->wHour, time->wMinute);
}

//-------------------------------------------------------------------------------
HICON GetFileIconHandle(const char* strFileName, BOOL bSmallIcon)
{
	
	SHFILEINFO    sfi;
	if (bSmallIcon)
	{
		SHGetFileInfo(
		   (LPCTSTR)strFileName, 
		   FILE_ATTRIBUTE_NORMAL,
		   &sfi, 
		   sizeof(SHFILEINFO), 
		   SHGFI_ICON | SHGFI_SMALLICON | SHGFI_USEFILEATTRIBUTES);
	}
	else
	{
		SHGetFileInfo(
		   (LPCTSTR)strFileName, 
		   FILE_ATTRIBUTE_NORMAL,
		   &sfi, 
		   sizeof(SHFILEINFO), 
		   SHGFI_ICON | SHGFI_LARGEICON | SHGFI_USEFILEATTRIBUTES);
	}
	return sfi.hIcon;
}

//-------------------------------------------------------------------------------
HICON GetFolderIconHandle(BOOL bSmallIcon )
{
	SHFILEINFO    sfi;
	if (bSmallIcon)
	{
		 SHGetFileInfo(
		 (LPCTSTR)"Doesn't matter", 
		 FILE_ATTRIBUTE_DIRECTORY,
		 &sfi, 
		 sizeof(SHFILEINFO), 
		 SHGFI_ICON | SHGFI_SMALLICON | SHGFI_USEFILEATTRIBUTES);
	}
	else
	{
		 SHGetFileInfo(
		 (LPCTSTR)"Does not matter", 
		 FILE_ATTRIBUTE_DIRECTORY,
		 &sfi, 
		 sizeof(SHFILEINFO), 
		 SHGFI_ICON | SHGFI_LARGEICON | SHGFI_USEFILEATTRIBUTES);
	}
        return sfi.hIcon;
}

#ifdef UNICODE
//-----------------------------------------------------------------------
	void StrCopy(WCHAR* dest, const WCHAR* src)
	{
		wcscpy((wchar_t*)dest, src);
	}

//-----------------------------------------------------------------------
	void StrCopyAt(WCHAR* dest, const WCHAR* src)
	{
		wcscat((wchar_t*)dest, src);
	}

//-----------------------------------------------------------------------
	void StrCopy(CHAR* dest, const CHAR* src)
	{
		strcpy(dest, src);
	}

//-----------------------------------------------------------------------
	void StrCopyAt(CHAR* dest, const CHAR* src)
	{
		strcat(dest, src);
	}

//-----------------------------------------------------------------------
	void StrCopy(WCHAR* dest, const CHAR* src)
	{
		AsciiToUnicode(dest, src);
	}

//-----------------------------------------------------------------------
	void StrCopyAt(WCHAR* dest, const CHAR* src)
	{
		AsciiToUnicodeAt(dest, src);
	}

//-----------------------------------------------------------------------
	void StrCopy(CHAR* dest, const WCHAR* src)
	{
		UnicodeToAscii(dest, src);
	}

//-----------------------------------------------------------------------
	void StrCopyAt(CHAR* dest, const WCHAR* src)
	{
		UnicodeToAsciiAt(dest, src);
	}

//-----------------------------------------------------------------------
	UINT StrLength(const WCHAR* str)
	{
		return wcslen(str);
	}

//-----------------------------------------------------------------------
	UINT StrLength(const CHAR* str)
	{
		return strlen(str);
	}
#else
//-----------------------------------------------------------------------
	void StrCopy(CHAR* dest, const CHAR* src)
	{
		strcpy(dest, src);
	}

//-----------------------------------------------------------------------
	void StrCopyAt(CHAR* dest, const CHAR* src)
	{
		strcat(dest, src);
	}

//-----------------------------------------------------------------------
	UINT StrLength(const CHAR* src)
	{
		return strlen(src);
	}
#endif




//----------------------------------------------------------------------------------
void AsciiToUnicode(WCHAR* target, const CHAR* source)
{
	UINT n;
	for(n = 0; n < strlen(source); n++)
		target[n] = (WCHAR)source[n];

	target[n] = '\0';
}

//----------------------------------------------------------------------------------
void UnicodeToAscii(CHAR* target, const WCHAR* source)
{
	UINT n;
	for(n = 0; n < wcslen(source); n++)
		target[n] = (CHAR)source[n];

	target[n] = '\0';
}

//----------------------------------------------------------------------------------
void AsciiToUnicodeAt(WCHAR* target, const CHAR* source)
{
	int last = wcslen(target);

	UINT n;
	for(n = 0; n < strlen(source); n++)
		target[last + n] = (WCHAR)source[n];

	target[last + n] = '\0';
}

//----------------------------------------------------------------------------------
void UnicodeToAsciiAt(CHAR* target, const WCHAR* source)
{
	int last = strlen(target);

	UINT n;
	for(n = 0; n < wcslen(source); n++)
		target[last + n] = (CHAR)source[n];

	target[last + n] = '\0';
}
