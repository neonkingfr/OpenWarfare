
#include "wbPrecomp.h"
#include "WxUtil.h"
#include "wx/menu.h"
#include "wx/frame.h"
#include "wx/toolbar.h"

//---------------------------------------------------------------------------------------------
wxMenuItem* FindMenuItem(wxMenu* menu, wxString& ItemName)
{
	wxMenuItemList& MenuItems = menu->GetMenuItems();

	for(unsigned int n = 0; n < MenuItems.GetCount(); n++)	//musime precislovat idcka ked nejaky vypadne
	{
		if(MenuItems[n]->GetText() == ItemName)
			return MenuItems[n];
	}
	return NULL;
}

//---------------------------------------------------------------------------------------------
bool CheckMenuItemFromName(wxMenu* menu, wxString& CheckedItem)
{
	wxMenuItemList& MenuItems = menu->GetMenuItems();
	bool ret = false;

	for(unsigned int n = 0; n < MenuItems.GetCount(); n++)	//musime precislovat idcka ked nejaky vypadne
	{
		wxMenuItem* item = MenuItems[n];

		if(item->GetText() == CheckedItem)
		{
			if(item->IsCheckable() && !item->IsChecked())
				item->Check(true);

			ret = true;
		}
		else
		{
			if(item->IsCheckable())
				item->Check(false);
		}
	}
	return ret;
}

//---------------------------------------------------------------------------------------------
int GetLowestFreeMenuId(wxMenu* menu, int LowestID)
{
	wxMenuItemList& MenuItems = menu->GetMenuItems();
	int BestID = LowestID - 1;
	bool Found = true;

	while(Found)
	{
		Found = false;
		BestID++;

		for(unsigned int n = 0; n < MenuItems.GetCount(); n++)	//musime precislovat idcka ked nejaky vypadne
		{
			wxMenuItem* item = MenuItems[n];
			int ItemId = item->GetId();

			if(ItemId == BestID)
			{
				Found = true;
				break;
			}
		}
	}
	return BestID;
}

//----------------------------------------------------------------------------
bool RemoveSharedMenu(wxFrame* window, wxMenu* menu)
{
	assert(menu);
	assert(window);
	wxMenuBar* bar = window->GetMenuBar();

	if(bar)
	{
		for(unsigned int n = 0; n < bar->GetMenuCount(); n++)
		{
			if(bar->GetMenu(n) == menu)
			{
				bar->Remove(n);
				return true;
			}
		}
	}
	return false;
}

//----------------------------------------------------------------------------
bool AssignSharedMenu(wxFrame* window, wxMenu* menu, wxString name, int pos)
{
	assert(menu);
	assert(window);
	wxMenuBar* bar = window->GetMenuBar();
	//is it already present?
	unsigned int n;
	for(n = 0; n < bar->GetMenuCount(); n++)
	{
		if(bar->GetMenu(n) == menu)
			break;
	}

	if(n >= bar->GetMenuCount() && bar->FindMenu(name) == wxNOT_FOUND)
		return bar->Insert(pos, menu, name);

	return false;
}

//----------------------------------------------------------------------------
void UpdateMenuBarOfFrame(wxFrame* window)
{
	wxMenuBar* bar = window->GetMenuBar();
	assert(bar);
	window->SetMenuBar(NULL);
	window->SetMenuBar(bar);
}

//----------------------------------------------------------------------------
void EnableToolBar(wxToolBar* toolbar, bool enable)
{
	assert(toolbar);

	//neni mozne prejst tooly pretoze v api nic nemaju
/*	for(uint n = 0; n < toolbar->GetToolsCount(); n++)
	{
		wxToolBarToolBase* tool = toolbar->FindToolForPosition(n, 0);
		toolbar->EnableTool(tool->GetId(), enable);
	}*/

	toolbar->Enable(enable);
}