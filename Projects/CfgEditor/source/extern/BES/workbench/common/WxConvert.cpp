#include "wbPrecomp.h"
#include "WxConvert.h"

#include "windows.h"
#include "WindowsUndef.h"

#include "wx/image.h"

wxString VecToStr(const Vector3& vec, unsigned char NumFloatUnits)
{
	return FtoA(vec.x, NumFloatUnits) + " " + FtoA(vec.y, NumFloatUnits) + " " + FtoA(vec.z, NumFloatUnits);
}

Vector3 StrToVec(const wxString& str)
{
	int pos;
	Vector3 vv(0.0f);

	if(str.Length() < 5)	//neni to vector
		return vv;

//	pos = AnsiPos(" ", str);
	pos = str.Find(" ");

	if(pos <= 0)
		return vv;

   vv.x = atof((const char*)str.SubString(0, pos).c_str());

	wxString str2 = str.SubString(pos + 1, str.Length());
	pos = str2.Find(" ");
//	pos = AnsiPos(" ", str2);

	if(pos <= 0)
		return vv;

   vv.y = atof((const char*)str2.SubString(0, pos).c_str());

  	str2 = str2.SubString(pos + 1, str2.Length());
   vv.z = atof((const char*)str2.c_str());

	return vv;
}

wxString NumToSpaces(int NumSpaces)
{
   if(NumSpaces <= 0)
      return wxT("");

   wxString tmp = wxT("");

   for(int n = 0; n < NumSpaces; n++)
      tmp += wxT(" ");

   return tmp;
}

void ReplaceChar(wxString *Str, char OldChar, char NewChar)
{
   char Buffer[256];
   ZeroMemory(Buffer, sizeof(Buffer));
   int lng = Str->Length();
   strcpy(Buffer, (const char*)Str->c_str());

   for(int n = 0; n < lng; n++)
   {
      if(Buffer[n] == OldChar)
         Buffer[n] = NewChar;
   }

	*Str = wxString::FromAscii(Buffer);
}
