
#ifndef TEXT_FILE_H
#define TEXT_FILE_H

#include "SharedApiDef.h"
#include<string>
using namespace std;

//=============================================================================================
class SHARED_API TextParser
{
public:
	int	ParseLine(string Line, string* tokens, int* PosArray);
	bool	AddSplitSymbol(string symbol, bool AsToken);
	bool	RemoveSplitSymbol(string symbol);
	void	RemoveAllSplitSymbols();
			TextParser();
private:
	string	SplitSymbols[32];
	bool		IsTokenSymbol[32];
	int		NumSplitSymbols;
};

//extern TextParser TxtParser;

//=============================================================================================
class SHARED_API TxtLine
{
public:
	void		GetText(char* array);
	string	GetText();
	bool		SetText(const char* array);
	bool		SetText(string& text);
	int		GetNumTokens();
	bool		GetToken(int index, char* array);
	string	GetToken(int index);
	bool		SetToken(int index, string& NewText);
	bool		RemoveToken(int index);
	bool		InsertToken(int index, string& Token);
	int		FindSingleLineComment(int FromToken);			//vracia index tokenu ktory je "//"
	int		FindMultiLineCommentStart(int FromToken);		//vracia index tokenu ktory je "/*"
	int		FindMultiLineCommentEnd(int FromToken);		//vracia index tokenu ktory je "*/"
	void		Clear();
//	friend TxtLine	operator+(const TxtLine& Line1, const TxtLine& Line2);

				TxtLine(TextParser* parser);
				TxtLine(TextParser* parser, const char* array);
				TxtLine(TextParser* parser, string& text);
				~TxtLine();
private:
	TextParser* Parser;
	string		Buffer;
	string		tokens[256];
	int			TokensPos[256];
	int			NumTokens;
	void			ReparseTokens();
};

//=============================================================================================
class SHARED_API TxtFile
{
public:
	bool		Open(const char *FileName);		//otvori subor.
	bool		Save(const char *FileName);		//ulozi subor.
	int		GetFirstLine(char *array);
	string	GetFirstLine();
	TxtLine	GetFirstLineEx();
	int		GetLastLine(char *array);
	string	GetLastLine();
	TxtLine	GetLastLineEx();
	int		GetNextLine(char *array);			//do pola array vrazi dalsi riadok. vracia cislo toho riadku. -1 ked uz ziadny dalsi neexistuje
	string	GetNextLine();
	TxtLine	GetNextLineEx();
	int		GetPreviousLine(char *array);	//do pola array vrazi predchadzajuci riadok. vracia cislo toho riadku. -1 ked uz pred nim ziadny neni
	string	GetPreviousLine();
	TxtLine	GetPreviousLineEx();
	int		GetCurrentLine(char *array);		//do pola array vrazi aktualny riadok. vracia cislo toho isteho riadku.
	string	GetCurrentLine();
	TxtLine	GetCurrentLineEx();
	int		SetCurrentLine(int Line);			
	int		GetAll(char *array);				//do array nasype cely obsah suboru
	const char*	GetAll();							//vraciame pointer na buffer
	int		GetNumLines();						//pocet riadkov
	int		AddLine(const char *array);		//zapise riadok (vlozi znaky konca riadka). vracia cislo zapisaneho riadku
	int		AddLine(string& Line);
	int		AddLine(TxtLine& Line);
	int		AddBuffer(const char *array);	//vlozi pole znakov. znaky konca riadka nepridava. vracia cislo riadku ktory sa zapisal ako posledny
	bool		SetLine(int Line, const char* array);
	bool		SetLine(int Line, string& NewText);
	bool		SetLine(int Line, TxtLine &NewLine);
	int		RemoveLine(int Line);				//vyhodi konkretnu lajnu. vracia aktualnu. (ak zmazem dvojku lajnu tak current bude potom jednotka)
	int		RemoveComments();					//vyhodi vsetky poznamky. jednoriadkove aj viacriadkove. vracia pocet vyhodenych komentarov
	void		Clear();								//vymaze obsah. CurrentLine = -1
				TxtFile(TextParser* parser);
				~TxtFile();
private:
	TextParser* Parser;
	string	FullFile;				//sluzi ako buffer celeho suboru	
	bool		bTest;						//koli resultom z IO funkcii
	int		CurrentLine;				//prave citany/zapisovany riadok
	unsigned int PosInFile;				//pozicia v myslenom subore ked skladame znaky do pola
	bool		GetLinePos(int Line, int* Start, int* End);	//vracia true pokial ide o poslednu lajnu bez znakov ukoncenia riadka
};

#endif