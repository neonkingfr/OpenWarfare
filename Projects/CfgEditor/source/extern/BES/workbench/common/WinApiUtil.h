
#include "SharedApiDef.h"

#include "windows.h"
#include "WindowsUndef.h"

//class FILETIME;
//class SYSTEMTIME;

SHARED_API void		taShowCursor(bool status);	//spolahlivo zapne/vypne kurzor
SHARED_API bool		taFileExists(const char* fileName);
SHARED_API bool		taIsDirectory(const char* fileName);
SHARED_API void		taGetAplicationPath(char* str);
SHARED_API void		taGetAplicationDir(char* str);

SHARED_API bool		GetFileTimes(const char* FileName, FILETIME* CreationTime, FILETIME* LastAccesTime, FILETIME* LastWriteTime);
SHARED_API bool		WriteTimesToFile(const char* filename, FILETIME* CreationTime, FILETIME* LastAccesTime, FILETIME* LastWriteTime);
SHARED_API void		ConvertTimeToString(const FILETIME* time, char* result);
SHARED_API void		taSystemTimeToStr(SYSTEMTIME* time, char* DateString, char* TimeString);
SHARED_API void		taSystemTimeToStr(SYSTEMTIME* time, char* DateAndTimeString);
SHARED_API HICON		GetFolderIconHandle(BOOL bSmallIcon );
SHARED_API HICON		GetFileIconHandle(const char* strFileName, BOOL bSmallIcon);

SHARED_API void		AsciiToUnicode(WCHAR* target, const CHAR* source);
SHARED_API void		UnicodeToAscii(CHAR* target, const WCHAR* source);
SHARED_API void		AsciiToUnicodeAt(WCHAR* target, const CHAR* source);
SHARED_API void		UnicodeToAsciiAt(CHAR* target, const WCHAR* source);


#ifdef UNICODE
	SHARED_API void StrCopy(WCHAR* dest, const WCHAR* src);
	SHARED_API void StrCopyAt(WCHAR* dest, const WCHAR* src);

	SHARED_API void StrCopy(CHAR* dest, const CHAR* src);
	SHARED_API void StrCopyAt(CHAR* dest, const CHAR* src);

	SHARED_API void StrCopy(WCHAR* dest, const CHAR* src);
	SHARED_API void StrCopyAt(WCHAR* dest, const CHAR* src);

	SHARED_API void StrCopy(CHAR* dest, const WCHAR* src);
	SHARED_API void StrCopyAt(CHAR* dest, const WCHAR* src);

	SHARED_API UINT StrLength(const WCHAR* str);
	SHARED_API UINT StrLength(const CHAR* str);
#else
	SHARED_API void StrCopy(CHAR* dest, const CHAR* src);
	SHARED_API void StrCopyAt(CHAR* dest, const CHAR* src);
	SHARED_API UINT StrLength(const CHAR* str);
#endif

	SHARED_API inline bool		taBtoB(BOOL b){return b == TRUE;};
	SHARED_API inline BOOL		taBtoB(bool b){return b == true;};