
#ifndef WX_NATIVE_CONVERT
#define WX_NATIVE_CONVERT

#include "wx/string.h"
#include "wx/colour.h"

wxString ItoA(int Number);
int		AtoI(const wxString& str);
wxString FtoA(float val, short int NumFloatUnits = 6);
float		AtoF(const wxString& str);
int		wxColourToARGB(wxColour& color);

#endif