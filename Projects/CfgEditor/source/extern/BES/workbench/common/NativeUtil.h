#ifndef NATIVE_UTIL_H
#define NATIVE_UTIL_H

#include "SharedApiDef.h"

SHARED_API bool			GetFileExtension(const char* filename, char* result);				//ziska koncovku (suboru) zo stringu
SHARED_API void			ReverseCharArray(char* s);
//SHARED_API void			CleanFloatString(char* str);
//SHARED_API void			CleanVectorString(char* str);
SHARED_API void			RemoveQuotes(char* str);
SHARED_API int				ReplaceCharacters(char* str, const char* ToFind, const char* ToReplace);
SHARED_API void			HumanizeFloat(float* fl);
SHARED_API float			Fmodulo(float n1, float n2);
SHARED_API float			RoundUp(float f);
SHARED_API float			RoundUp2(float f);
SHARED_API double			RoundUp2Double(double f);
SHARED_API bool			taBeforeLastChar(const char* source, char* destination, const char* ToFind);
SHARED_API bool			taAfterLastChar(const char* source, char* destination, const char* ToFind);
SHARED_API void			taToLower(char* array);
SHARED_API inline bool	IsNumber(char ch);
SHARED_API bool			IsIntNumber(const char* val);
SHARED_API bool			IsFloatNumber(const char* val);
SHARED_API char*			stristr(const char* String, const char* Pattern);
SHARED_API unsigned int	RateOfUint(unsigned int value, double rate);
//SHARED_API char* __fastcall stristr(const char* pszMain, const char* pszSub);	//insensitive strstr()

#endif
