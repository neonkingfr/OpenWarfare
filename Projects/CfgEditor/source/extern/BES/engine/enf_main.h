//-----------------------------------------------------------------------------
// File: enf_main.h
//
// Desc: Common definitions
//
// Copyright (c) 2000-2005 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#ifndef ENF_MAIN_H
#define ENF_MAIN_H

#ifdef CRTDBG_MAP_ALLOC
	#include <crtdbg.h>
#endif

#include "enf_config.h"
#include "common/enf_types.h"
#include "common/enf_staticbuffer.h"

//-----------------------------------------------------------------------------
// Little/Big endian stuff
//-----------------------------------------------------------------------------
inline float SwapFloat(float i)
{
	int in;

	*(float *)&in = i;

	in = (((in >> 24) & 0xffL)|((in >> 8) & 0xff00L) | ((in << 8) & 0xff0000L) | ((in << 24) & 0xff000000L));

	return *(float *)&in;
}

inline uint SwapInt(uint in)
{
	return (((in >> 24) & 0xffL) | ((in >> 8) & 0xff00L) | ((in << 8) & 0xff0000L) | ((in << 24) & 0xff000000L));
}

inline ushort SwapShort(ushort in)
{
	return (((in >> 8) & 0xff)|((in << 8) & 0xff00));
}

#if ENF_INTEGER_BIG_ENDIAN
#define BE_INT(a) (a)
#define LE_INT(a) SwapInt(a)
#define BE_SHORT(a) (a)
#define LE_SHORT(a) SwapShort(a)
#else
#define BE_INT(a) SwapInt(a)
#define LE_INT(a) (a)
#define BE_SHORT(a) SwapShort(a)
#define LE_SHORT(a) (a)
#endif

#if ENF_FLOAT_BIG_ENDIAN
#define BE_FLOAT(a) (a)
#define LE_FLOAT(a) SwapFloat(a)
#define BE_DOUBLE(a) (a)
#define LE_DOUBLE(a) SwapDouble(a)
#else
#define BE_FLOAT(a) SwapFloat(a)
#define LE_FLOAT(a) (a)
#define BE_DOUBLE(a) SwapDouble(a)
#define LE_DOUBLE(a) (a)
#endif

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
//global statistics
struct EngineStats
{
	static uint m_iLinkCount;
	static uint m_iPLinkCount;
	static uint m_iBTraceCount;
	static uint m_iLTraceCount;
	static uint m_iEntityCount;
	static uint m_iDLightCount;
	static uint m_iSpecularCount;
	static uint m_iBatchCount;
	static uint m_iBatchBytesIndices;
	static uint m_iBatchBytesVertexes;
	
	static uint m_iShadowTriangleCount;
	static uint m_iParticleCount;
	static uint m_iSurfaceCount;
	static uint m_iModelCount;
	static uint m_iSpriteCount;
	static uint m_iMeshCount;
	static uint m_iCreatureCount;
	static uint m_iApplyMaterialCount;
	static uint m_iMaterialCount;

	static void Reset()
	{
		m_iLinkCount = 0;
		m_iPLinkCount = 0;
		m_iBTraceCount = 0;
		m_iLTraceCount = 0;
		m_iEntityCount = 0;
		m_iDLightCount = 0;
		m_iSpecularCount = 0;
		m_iBatchCount = 0;
		m_iBatchBytesIndices = 0;
		m_iBatchBytesVertexes = 0;

		m_iShadowTriangleCount = 0;
		m_iParticleCount = 0;
		m_iSurfaceCount = 0;
		m_iModelCount = 0;
		m_iSpriteCount = 0;
		m_iMeshCount = 0;
		m_iCreatureCount = 0;
		m_iApplyMaterialCount = 0;
		m_iMaterialCount = 0;
	}
};

//-----------------------------------------------------------------------------
// Basic types
//-----------------------------------------------------------------------------
#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }

//-----------------------------------------------------------------------
const uint	LOG_FLAG_CREATE	= 1;
const uint	LOG_FLAG_APPEND	= 2;

const uint	LOG_FORM_HEXA		= 1;
const uint	LOG_FORM_DEC		= 2;
const uint	LOG_FORM_INTEGER	= 4;
const uint	LOG_FORM_FLOAT		= 8;

//-----------------------------------------------------------------------
class TLog
{
private:
	char		TmpBuffer[1024];
	void*		LogFile;
	char		Name[256];
	int		Open();
	void		Close();
	int		m_iFlags;
	bool		m_bFirst;

public:
   TLog();
   TLog(char *name,int flags);
   ~TLog();
	void TimeStamp();
   void Open(char *name,int flags);
	void Printf(char *text,...);
	void operator<<(char *text);
   void operator<<(int inum);
   void operator<<(float fnum);
   void operator<<(char chr);
   void Dump(void *data,int num, int size, int row, int flags);
};

//-----------------------------------------------------------------------

#if defined(ENF_EXCEPTION) && !defined(_DEBUG)
	#define BEGIN_GUARD try {
#else
	#define BEGIN_GUARD ((void)0);
#endif

#if defined(ENF_EXCEPTION) && !defined(_DEBUG)
	#define END_GUARD(a) } catch(...) { Error(a); }
#else
	#define END_GUARD(a) ((void)0);
#endif

class MainConfig
{
public:
	static bool		m_bEnableScreenshots;
	static bool		m_bEnableDebugRender;
	static bool		m_bEnableConsole;

	static bool		m_bShowCrash;
	static bool		m_bNoCrash;
	static bool		m_bShowConsole;
	static bool		m_bLogConsole;
	static TLog		m_ConsoleLog;
	static TLog		m_CrashLog;
	static bool		m_bShowConfig;
	static uint		m_iNumCrashes;
	static uint		m_iMaxCrashes;

	static uint		m_iMemMegabytes;

//script
	static int		m_iOptimize;
	static bool		m_bCDebug;
	static bool		m_bList;
};


extern StaticBuffer* g_pStatic;

//-----------------------------------------------------------------------------
// Error handling
//-----------------------------------------------------------------------------
void		Error(const char *text,...);
void		Error2(const char *type, const char *text,...);

//-----------------------------------------------------------------------
class Settings
{
public:
	static void	*Open(char *key);
	static void Close(void *ptr);

	static bool	ReadInt(void *ptr, const char *key, int *value);
	static bool	SetInt(void *ptr, const char *key, int value);
	static bool	ReadString(void *ptr, const char *key, char *value);
	static bool	ReadString(void *ptr, const char *key, SString<>& value);
	static bool	SetString(void *ptr, const char *key, char *value);
	static bool SetData(void *ptr, const char *key, void *value, int size);
	static int  ReadData(void *ptr, const char *key, void *value, int size);
};

int StringWeight(const char *str);
int StringWeight(const char *str, int len);

uint GetCRC(ubyte *p, int size);

enum FPUPrecision {FPU_PRECISION_MINIMUM, FPU_PRECISION_MEDIUM, FPU_PRECISION_MAXIMUM};

void SetFPUPrecision(FPUPrecision prec);

//to avoid include windows.h
void _OutputDebugString(const char* str);
void* _LoadLibrary(const char* name);
void* _GetProcAddress(void* module, const char* procname);
short _GetAsyncKeyState(int vkey);

#endif //ENF_MAIN_H
