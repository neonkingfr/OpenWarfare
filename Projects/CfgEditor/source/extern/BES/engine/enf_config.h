//-----------------------------------------------------------------------------
// File: enf_config.h
//
// Desc: 
//
// Copyright (c) 2000-2005 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#ifndef ENF_CONFIG_H
#define ENF_CONFIG_H

#ifdef WINVER
	#undef WINVER
#endif

#ifdef _WIN32_WINNT
	#undef _WIN32_WINNT
#endif

#define WINVER 0x0501
#define _WIN32_WINNT 0x0501

#define ENF_DYNAMICLIGHT
const unsigned int MAX_FILENAME_LENGTH	= 256;

#ifdef _MSC_VER
#pragma warning( disable : 4100 ) //warning C4100: 'world' : unreferenced formal parameter
#pragma warning( disable : 4127 ) //warning C4127: conditional expression is constant
#pragma warning( disable : 4131 ) //warning C4131: '_tr_align' : uses old-style declarator
#pragma warning( disable : 4201 ) //warning C4201: nonstandard extension used : nameless struct/union

#endif

#pragma warning (disable : 4251)	// disable no DLL interface
#pragma warning (disable : 4244)	// disable conversion
//#pragma warning (disable : 4624)	// private destructor

//Consoles are clear switch configuration
#ifdef ENF_XBOX
	#undef ENF_INTEL
	#define ENF_INTEL

	#undef ENF_SSE
	#define ENF_SSE

	#undef ENF_STATIC
	#define ENF_STATIC
#endif

#ifdef ENF_XENON
	#undef ENF_INTEL
	#undef ENF_SSE
	#undef ENF_STATIC
	#define ENF_STATIC
#endif

//define the endianess of the integers
#if defined(__APPLE__) || defined(MACOSX) || defined(ENF_XENON)
	#define ENF_INTEGER_BIG_ENDIAN 1
#else
	#define ENF_INTEGER_BIG_ENDIAN 0
#endif

//define the endianess of floats (not necessarily the same as integers)
#if defined(__APPLE__) || defined(MACOSX) || defined(ENF_XENON)
	#define ENF_FLOAT_BIG_ENDIAN 1
#else
	#define ENF_FLOAT_BIG_ENDIAN 0
#endif

#ifdef WIN32
	#if !defined(ENF_DLL_EXPORT) && !defined(ENF_STATIC) 
		#ifndef ENF_DLL
			#define ENF_DLL
		#endif
	#endif
#endif

#define PUBLIC_API
#define FORCE_PUBLIC_API __declspec(dllexport)

//For STARFORCE releases we don't want any exports
#if !defined(ENF_STARFORCE) && !defined(ENF_STATIC)
	#ifdef ENF_DLL
		#undef PUBLIC_API
		#define PUBLIC_API __declspec( dllimport )
	#endif

	#ifdef ENF_DLL_EXPORT
		#undef PUBLIC_API
		#define PUBLIC_API __declspec( dllexport )
	#endif
#endif

#ifndef NULL
	#define NULL 0L
#endif

#define ENF_STDCALL __stdcall
//#define ENF_INLINE __forceinline
#define ENF_INLINE inline
#define ENF_INLINEFUNC inline

enum ErrorResponse
{
	ER_ABORT,
	ER_RETRY,
	ER_IGNORE
};

PUBLIC_API ErrorResponse _ShowError(const char* title, const char* error, const char* file, unsigned line);

#ifdef  NDEBUG
	#define enf_assert(exp) ((void)0)
	#define enf_assert2(exp, desc) ((void)0)
#else
	PUBLIC_API void _Assert(const char *, const char *, unsigned);

	#define enf_assert(exp) (void)( (exp) || (_Assert(#exp, __FILE__, __LINE__), 0) )
	#define enf_assert2(exp, desc) (void)( (exp) || (_Assert(desc, __FILE__, __LINE__), 0) )
#endif  /* NDEBUG */

#ifdef _DEBUG
	struct PUBLIC_API _StackTag
	{
		_StackTag(const char* fname, const char* file, int line);
		~_StackTag();
	};

	#define ENF_STACK_LOG() _StackTag __tag(__FUNCTION__, __FILE__, __LINE__)
#else
	struct PUBLIC_API _StackTag
	{
		_StackTag(const char* fname);
		~_StackTag();
	};

	#define ENF_STACK_LOG() _StackTag __tag(__FUNCTION__)
#endif

#define _CRT_SECURE_NO_DEPRECATE 1
#endif
