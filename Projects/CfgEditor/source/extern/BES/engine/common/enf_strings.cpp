//-----------------------------------------------------------------------------
// File: enf_strings.cpp
//
// Desc: Strings library
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#include "precomp.h"
#include "common/enf_types.h"
#include "common/enf_staticbuffer.h"
#include <stdlib.h>

void __vsprintf(char* buff, const char* fmt, va_list argptr)
{
	vsprintf(buff, fmt, argptr);
}

//-----------------------------------------------------------------------------
char * enf::strchr (const char * string, int ch)
{
	enf_assert(string != NULL);

	while (*string && *string != (char)ch)
		string++;

	if (*string == (char)ch)
		return((char *)string);

	return(NULL);
}

//-----------------------------------------------------------------------------
char * enf::strrchr (const char * string, int ch)
{
	enf_assert(string != NULL);

	char *start = (char *)string;
	const char *rchr = NULL;

	while (*string)	// find end of string
	{
		if(*string == ch)
			rchr = string; //remember last occurence
		string++;
	}
	
	return (char*)(rchr);
}

//-----------------------------------------------------------------------------
int enf::strcmpi (const char * dst, const char * src)
{
	enf_assert(dst != NULL && src != NULL);

	int f, l;

	do
	{
		if ( ((f = (unsigned char)(*(dst++))) >= 'A') && (f <= 'Z') )
			f -= 'A' - 'a';
		if ( ((l = (unsigned char)(*(src++))) >= 'A') && (l <= 'Z') )
			l -= 'A' - 'a';
	} while ( f && (f == l) );

	return(f - l);
}

//-----------------------------------------------------------------------------
int enf::strcmp (const char * src, const char * dst)
{
	enf_assert(dst != NULL && src != NULL);

	int ret = 0 ;

	while( ! (ret = *(unsigned char *)src - *(unsigned char *)dst) && *dst)
		++src, ++dst;

	if ( ret < 0 )
		ret = -1 ;
	else if ( ret > 0 )
		ret = 1 ;

	return( ret );
}

//-----------------------------------------------------------------------------
size_t enf::strlen (const char * str)
{
	enf_assert(str != NULL);

	const char *eos = str;

	while( *eos++ ) ;

	return( (int)(eos - str - 1) );
}

//-----------------------------------------------------------------------------
char* enf::strcat (char * dst, const char * src)
{
	enf_assert(dst != NULL && src != NULL);

	char * cp = dst;

	while( *cp )
				cp++;                   /* find end of dst */

	while( *cp++ = *src++ ) ;       /* Copy src to end of dst */

	return( dst );                  /* return dst */
}

//-----------------------------------------------------------------------------
char* enf::strcpy(char * dst, const char * src)
{
	enf_assert(dst != NULL && src != NULL);

	char * cp = dst;

	while( *cp++ = *src++ ); /* Copy src over dst */

	return( dst );
}

//int __cdecl _input(FILE *, const unsigned char *, va_list);

#ifndef ENF_NOCSTR

//-----------------------------------------------------------------------------
int enf::CStr::scanf(const char* format, ...) const
{
	enf_assert(!"Not implemented");

/*	va_list arglist;

	FILE str;
	register FILE *infile = &str;
	register int retval;

	va_start (arglist, format);
	
	enf_assert(m_pCString != NULL);
	enf_assert(format != NULL);

	infile->_flag = _IOREAD|_IOSTRG|_IOMYBUF;
	infile->_ptr = infile->_base = (char *) m_pCString;
	infile->_cnt = ((int)enf::strlen(m_pCString))*sizeof(char);
	
	retval = _input(infile, (const unsigned char*)format,arglist);

   return(retval);
*/
	return 0;
}

//-----------------------------------------------------------------------------
double enf::CStr::ToDouble() const
{
	//not correct at all, but for common purpose it's okay
	if(m_pCString == NULL)
		return 0.0;

	return ::atof(m_pCString);
}

//-----------------------------------------------------------------------------
float enf::CStr::ToFloat() const
{
	//not correct at all, but for common purpose it's okay
	if(m_pCString == NULL)
		return 0.0f;

	return (float)::atof(m_pCString);
}

//-----------------------------------------------------------------------------
int enf::CStr::ToInt() const
{
	//not correct at all, but for common purpose it's okay
	if(m_pCString == NULL)
		return 0;

	return ::atol(m_pCString);
}

//-----------------------------------------------------------------------------
int enf::CStr::ncmp(enf::CStr str, size_t length) const
{
	if(m_pCString == str.m_pCString)
		return 0;

	if(m_pCString == NULL)
		return -1;

	if(str.m_pCString == NULL)
		return 1;

	return strncmp(m_pCString, str.m_pCString, length);
}

//-----------------------------------------------------------------------------
const char* enf::CStr::strstr(enf::CStr str) const
{
	if(m_pCString == NULL)
		return NULL;
	return ::strstr(m_pCString, str.cstr());
}

//-----------------------------------------------------------------------------
const char* enf::CStr::strchr(int chr) const
{
	if(m_pCString == NULL)
		return NULL;
	return ::strchr(m_pCString, chr);
}

//-----------------------------------------------------------------------------
const char* enf::CStr::strrchr(int chr) const
{
	if(m_pCString == NULL)
		return NULL;
	return ::strrchr(m_pCString, chr);
}

//-----------------------------------------------------------------------------
uint enf::CStr::Length() const
{
	if(m_pCString == NULL)
		return 0;
	return enf::strlen(m_pCString);
}

//-----------------------------------------------------------------------------
bool enf::CStr::operator==(const char* str) const
{
	//allow optimization for constant strings from the same StaticBuffer
	//as sideeffect, we can test it on NULL string
	if(m_pCString == str)
		return true;

	if(str == NULL || m_pCString == NULL)
	{
		return false;
	}

	return m_pCString == str || enf::strcmp(m_pCString, str) == 0;
}

//-----------------------------------------------------------------------------
bool enf::CStr::operator!=(const char* str) const
{
	if(str == NULL)
	{
		return(m_pCString != NULL);
	}

	return enf::strcmp(m_pCString, str) != 0;
}

//-----------------------------------------------------------------------------
bool enf::CStr::operator==(const enf::CStr& str) const
{
	return *this == str.m_pCString;
}

//-----------------------------------------------------------------------------
bool enf::CStr::operator!=(const enf::CStr& str) const
{
	return *this != str.m_pCString;
}

//-----------------------------------------------------------------------------
bool enf::CStr::operator<(const enf::CStr& cstr) const
{
	if(m_pCString == NULL || cstr.m_pCString == NULL)
	{
		if(m_pCString == NULL && cstr.m_pCString != NULL)
			return true;
		return false;
	}
	return (enf::strcmp(m_pCString, cstr.m_pCString) < 0);
}

//-----------------------------------------------------------------------------
bool enf::CStr::operator>(const enf::CStr& cstr) const
{
	if(cstr.m_pCString == NULL || m_pCString == NULL)
	{
		if(cstr.m_pCString == NULL && m_pCString != NULL)
			return true;
		return false;
	}
	return (enf::strcmp(m_pCString, cstr.m_pCString) > 0);
}
#endif

const DString::size_type DString::npos = static_cast< size_type >(-1);

// Null rep.
DString::Rep DString::nullrep_ = { 0, 0, '\0' };

StaticBuffer* DString::staticbuff_ = NULL;

void DString::init(size_type sz, size_type cap)
{
	if (cap)
	{
		// Lee: the original form:
		//	rep_ = static_cast<Rep*>(operator new(sizeof(Rep) + cap));
		// doesn't work in some cases of new being overloaded. Switching
		// to the normal allocation, although use an 'int' for systems
		// that are overly picky about structure alignment.
		const size_type bytesNeeded = sizeof(Rep) + cap;
		const size_type intsNeeded = ( bytesNeeded + sizeof(int) - 1 ) / sizeof( int ); 
		rep_ = reinterpret_cast<Rep*>( MemoryManager::Alloc(intsNeeded * sizeof(int), __FILE__, __LINE__) );

		rep_->size = sz;
		rep_->str[ sz ] = '\0';
		rep_->capacity = cap;
	}
	else
	{
		rep_ = &nullrep_;
	}
}

void DString::quit()
{
	if (rep_ != &nullrep_)
	{
		if(staticbuff_ != NULL && staticbuff_->IsAllocated(rep_))
		{
			return;
		}
		// The rep_ is really an array of ints. (see the allocator, above).
		// Cast it back before delete, so the compiler won't incorrectly call destructors.
		MemoryManager::Free( reinterpret_cast<int*>( rep_ ) );
	}
}

DString& DString::setConstant(const char * copy)
{
	if(staticbuff_ == NULL)
	{
		assign( copy, (size_type)enf::strlen(copy));
	}
	else
	{
		enf_assert2(staticbuff_->IsAllocated(copy), "Trying to set constant string from another StaticBuffer");

		//deallocate old buffer
		quit();
		rep_ = (Rep*)copy;
	}
	return *this;
}

void DString::reserve (size_type cap)
{
	if (cap > capacity())
	{
		DString tmp;
		tmp.init(length(), cap);
		memcpy(tmp.start(), data(), length());
		swap(tmp);
	}
}


DString& DString::assign(const char* str, size_type len)
{
	size_type cap = capacity();
	if (len > cap || cap > 3*(len + 8))
	{
		DString tmp;
		tmp.init(len);
		memcpy(tmp.start(), str, len);
		swap(tmp);
	}
	else
	{
		memmove(start(), str, len);
		set_size(len);
	}
	return *this;
}


DString& DString::append(const char* str, size_type len)
{
	size_type newsize = length() + len;
	if (newsize > capacity())
	{
		reserve (newsize + capacity());
	}
	memmove(finish(), str, len);
	set_size(newsize);
	return *this;
}


DString operator + (const DString & a, const DString & b)
{
	DString tmp;
	tmp.reserve(a.length() + b.length());
	tmp += a;
	tmp += b;
	return tmp;
}

DString operator + (const DString & a, const char* b)
{
	DString tmp;
	DString::size_type b_len = static_cast<DString::size_type>( enf::strlen(b) );
	tmp.reserve(a.length() + b_len);
	tmp += a;
	tmp.append(b, b_len);
	return tmp;
}

DString operator + (const char* a, const DString & b)
{
	DString tmp;
	DString::size_type a_len = static_cast<DString::size_type>( enf::strlen(a) );
	tmp.reserve(a_len + b.length());
	tmp.append(a, a_len);
	tmp += b;
	return tmp;
}

//-----------------------------------------------------------------------
void enf::estrcnv(enf::UNICHAR *dest, const char *src)
{
	while(true)
	{
		*dest++ = c2u(*src);

		if(endchar(*src))
			break;

		src++;
	}
}

//-----------------------------------------------------------------------
int enf::estrlen(const enf::UNICHAR *str)
{
	int len = 0;

	while(str[len] != 0)
		len++;

	return len;
}

//-----------------------------------------------------------------------
void enf::estrcpy(enf::UNICHAR *dest, const enf::UNICHAR *src)
{
	while(true)
	{
		*dest++ = *src;

		if(enf::endchar(*src))
			break;

		src++;
	}
}

//-----------------------------------------------------------------------
void enf::estrcat(enf::UNICHAR *dest, const enf::UNICHAR *src)
{
	while(!enf::endchar(*dest))
		dest++;

	enf::estrcpy(dest, src);

}
