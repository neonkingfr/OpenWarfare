//-----------------------------------------------------------------------------
// File: enf_memory.h
//
// Desc: Memory allocator
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------
#pragma once
#ifndef ENF_MEMORY_H
#define ENF_MEMORY_H

#include "enf_config.h"
#include <malloc.h>
#include <new.h>
//#include "enf_mutex.h"

#define SAFE_DESTROY(p)  { if(p) { (p)->Destroy();     (p)=NULL; } }
#define SAFE_DELETE(p)  { if(p) { delete (p);     (p)=NULL; } }
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }

#define ALLOCA(type, size) (type*)_alloca(sizeof(type) * (size))

#ifdef ENF_64BIT
	#define ALLOCA16(size) (void *)((unsigned __int64) ((char*)_alloca((size) + 16) + 15) & ~0x0fui64)
#else
	#define ALLOCA16(size) (void *)((unsigned int)((char*)_alloca((size) + 16) + 15) & ~0x0fL)
#endif

struct MemStats
{
	static size_t	m_iAnims;
	static size_t	m_iTextures;
	static size_t	m_iRenderTargets;
	static size_t	m_iTotalVertices;
	static size_t	m_iTotalIndices;
	static size_t	m_iObjectVertices;
	static size_t	m_iObjectIndices;
	static size_t	m_iParticles;
	static size_t	m_iObjects;
	static size_t	m_iLevelData;
};


#ifdef ENF_INTEL
void ENF_INLINEFUNC __fastcall FillMemory32(void *mptr, int num, int data)
{
	__asm {
		mov	edi, mptr
		mov	ecx, num
		mov	eax, data
		rep	stosd
	}
}

//-----------------------------------------------------------------------------
void ENF_INLINEFUNC __fastcall CopyMem(void *dest, const void *src, int len)
{
	__asm {
		mov	ecx, len
		cmp	ecx, 0
		je		skip
		mov	edi, dest
		mov	esi, src
		mov	eax, ecx
		and	eax, 3
		shr	ecx, 2
		rep	movsd
		mov	ecx, eax
		rep	movsb
skip:
	}
}

//-----------------------------------------------------------------------------
void ENF_INLINEFUNC __fastcall CopyMem32(void *dest, const void *src, int len)
{
	__asm {
		mov	edi, dest
		mov	esi, src
		mov	ecx, len
		rep	movsd
	}
}

//-----------------------------------------------------------------------------
void ENF_INLINEFUNC __fastcall FillMem(void *dest, int len, int data)
{
	__asm {
		mov	edi, dest
		mov	ecx, len
		mov	ebx, data
		mov	bh, bl
		mov	eax, ebx
		shl	eax, 16
		mov	ax, bx
		mov	ebx, ecx
		and	ebx, 3
		shr	ecx, 2
		rep	stosd
		mov	ecx, ebx
		rep	stosb
	}
}

//-----------------------------------------------------------------------------
void ENF_INLINEFUNC __fastcall ZeroMem(void *dest, int len)
{
	__asm {
		mov	edi, dest
		mov	ecx, len
		xor	eax, eax
		mov	edx, ecx
		and	edx, 3
		shr	ecx, 2
		rep	stosd
		mov	ecx, edx
		rep	stosb
	}
}
//-----------------------------------------------------------------------------
void ENF_INLINEFUNC __fastcall ZeroMem32(void *dest, int len)
{
	__asm {
		mov	edi, dest
		mov	ecx, len
		xor	eax, eax
		rep	stosd
	}
}

#else
#define	CopyMem(a,b,c) memcpy(a,b,c)
#define	CopyMem32(a,b,c) memcpy(a,b,c*4)
#define	FillMem(a,b,c) memset(a,b,c)
#define	ZeroMem(a,b) memset(a,0,b)
#define	ZeroMem32(a,b) memset(a,0,b*4)
#endif


/*!
General memory manager. ATM it's wrapper over heap allocator
with added leak detector. All Enforce objects has to allocate
throught this. Either by calling Alloc/Free methods or by
macro ENF_DECLARE_MMOBJECT in their class body.
*/
class PUBLIC_API MemoryManager
{
public:
	struct MemoryChunk;

private:
	static size_t	m_iTotalAllocatedBytes;
	static size_t	m_iTotalAllocatedBlocks;
	//static CMutex	m_Mutex;
	static MemoryChunk* m_pChunks;

public:
	/*!
	Returns number of allocated blocks
	\return
	Number of blocks
	*/
	static ENF_INLINE size_t GetTotalChunks()
	{
		return m_iTotalAllocatedBlocks;
	}

	/*!
	Returns allocated memory in bytes
	*/
	static ENF_INLINE size_t GetTotalAllocated()
	{
		return m_iTotalAllocatedBytes;
	}

	/*!
	Returns estimated overhead caused by many tiny
	allocations
	*/
	static ENF_INLINE size_t GetAllocOverhead()
	{
//		return m_iTotalAllocatedBlocks * 48; //probably correct value
		return m_iTotalAllocatedBlocks * 32; //probably correct value
	}

	/*!
	Gets size of allocation.
	\param ptr
	Pointer to allocation. It must be pointer returned by Alloc() or ReAlloc() method!
	\return
	Size of allocation
	*/
	static size_t GetSize(void* ptr);

	/*!
	Reallocates memory. If it's possible, allocated block is just enlarged.
	Otherwise new block is allocated and old one is freed. In both cases
	data from old allocation are copied to the new block
	\param ptr
	Pointer to allocation. It must be pointer returned by Alloc() or ReAlloc() method!
	\param size
	Needed size
	\return
	Pointer to new allocation
	*/
	static void* ReAlloc(void* ptr, size_t size);

#ifdef ENF_LEAK_DETECTOR
	/*!
	Allocates memory
	\param size
	Needed size
	\param src
	Name of source file
	\param line
	Line in source code
	\return
	Pointer to new allocation
	*/
	static void* Alloc(size_t size, const char *src, int line);

	/*!
	Allocates memory
	\param size
	Needed size
	\return
	Pointer to new allocation
	*/
	ENF_INLINE static void* Alloc(size_t size)
	{
		return Alloc(size, __FILE__, __LINE__);
	}

	/*!
	Allocates memory, aligned to multiply of 16
	\param size
	Needed size
	\param src
	Name of source file
	\param line
	Line in source code
	\return
	Pointer to new allocation
	*/
	static void* Alloc16(size_t size, const char *src, int line);

	/*!
	Allocates memory, aligned to multiply of 16
	\param size
	Needed size
	\return
	Pointer to new allocation
	*/
	ENF_INLINE static void* Alloc16(size_t size)
	{
		return Alloc16(size, __FILE__, __LINE__);
	}

#else
	static void* Alloc(size_t cb);
	static void* Alloc16(size_t cb);
	ENF_INLINE static void* Alloc(size_t cb, const char *src, int line)
	{
		return Alloc(cb);
	}
	ENF_INLINE static void* Alloc16(size_t cb, const char *src, int line)
	{
		return Alloc16(cb);
	}
#endif
	/*!
	Frees memory allocated by Alloc() / ReAlloc()
	\param ptr
	Pointer to allocation to be freed
	*/
	static void Free(void *ptr);
	
	/*!
	Frees memory allocated by Alloc16()
	\param ptr
	Pointer to allocation to be freed
	*/
	static void Free16(void *ptr);

	/*!
	Dumps current memory usage to VS Output window
	\param title
	String put before report
	*/
	static void Dump(const char* title);

	/*!
	Must be called prior to any usage!
	*/
	static void Init();

	MemoryManager();
	~MemoryManager();
};

#ifdef ENF_LEAK_DETECTOR
	void *operator new[](size_t cb, const char* file, int line);
	void *operator new(size_t cb, const char* file, int line);
	void operator delete[](void *p, const char* file, int line);
	void operator delete(void *p, const char* file, int line);

	void *operator new[](size_t cb);
	void *operator new(size_t cb);
	void operator delete[](void *p);
	void operator delete(void *p);

	#define ALLOC16(size) MemoryManager::Alloc16(size, __FILE__, __LINE__)
	#define FREE16(ptr) MemoryManager::Free16(ptr)
	#define ENF_NEW new(__FILE__, __LINE__)

#else

	#define ENF_NEW new

	#define ALLOC16(size) MemoryManager::Alloc16(size)
	#define FREE16(ptr) MemoryManager::Free16(ptr)

#endif


struct MemoryStats
{
	//!Total allocated memory
	size_t	Allocated;
	//!Overhead and cached allocations
	size_t	Overhead;
	//!Number of allocated blocks
	size_t	NumAllocations;
	//!Number of internal heap allocations
	size_t	NumBlocks;
};
/*!
SmallBlockAllocator for managing small memory allocs.
Only blocks of fixed size may be allocated by this.
*/
class PUBLIC_API SmallBlockAllocator
{
public:
	virtual void *Alloc(size_t size, const char* file, int line)=0;
	virtual void *Alloc(size_t size)=0;

	//!Release memory, return true if successfully
	virtual bool Free(void *pointer)=0;
	
	//!Releases all memory
	virtual void FreeAll()=0;
	
	//!Returns statistics
	virtual void GetStats(MemoryStats& stats) const=0;

	//!Dumps statistics into debug output window
	virtual void Dump(const char* str) const=0;

	//!Flushes unused memory
	virtual void Flush()=0;
};

extern PUBLIC_API SmallBlockAllocator* g_pSmallBlockAllocator;

#define ENF_DECLARE_SMALLOBJECT(type) \
	inline void* operator new(size_t size, char const * file, int line){ return g_pSmallBlockAllocator->Alloc(size, file, line);}\
	inline void* operator new(size_t size){ return g_pSmallBlockAllocator->Alloc(size);} \
	inline void operator delete(void * ptr, char const * file, int line){g_pSmallBlockAllocator->Free(ptr);}\
	inline void operator delete(void* ptr) { g_pSmallBlockAllocator->Free(ptr);}

#ifdef ENF_LEAK_DETECTOR
	#define ENF_DECLARE_MMOBJECT(type) \
		inline void* operator new(size_t size, char const * file, int line){ return MemoryManager::Alloc(size, file, line);}\
		inline void* operator new[](size_t size, char const * file, int line){ return MemoryManager::Alloc(size, file, line);}\
		inline void* operator new(size_t size){ return MemoryManager::Alloc(size);} \
		inline void* operator new[](size_t size){ return MemoryManager::Alloc(size);} \
		inline void operator delete(void* ptr, char const * file, int line){MemoryManager::Free(ptr);}\
		inline void operator delete[](void* ptr, char const * file, int line){MemoryManager::Free(ptr);}\
		inline void operator delete(void* ptr) { MemoryManager::Free(ptr);}\
		inline void operator delete[](void* ptr) { MemoryManager::Free(ptr);}
#else
	#define ENF_DECLARE_MMOBJECT(type) \
		inline void* operator new(size_t size){ return MemoryManager::Alloc(size);} \
		inline void* operator new[](size_t size){ return MemoryManager::Alloc(size);} \
		inline void operator delete(void* ptr) { MemoryManager::Free(ptr);}\
		inline void operator delete[](void* ptr) { MemoryManager::Free(ptr);}
#endif

/*
//dummy operators for explicit constructor calling
ENF_INLINE void* operator new(size_t size, void *p){ return p;}
ENF_INLINE void* operator new[](size_t size, void *p){ return p;}
ENF_INLINE void operator delete(void* ptr, void* p) {  }
ENF_INLINE void operator delete[](void* ptr, void* p) {  }
*/
#endif //ENF_MEMORY_H
