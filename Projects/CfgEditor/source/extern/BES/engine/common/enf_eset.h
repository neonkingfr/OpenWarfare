//-----------------------------------------------------------------------------
// File: enf_eset.h
//
// Desc: Sorted array
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

#ifndef ENF_ESET_H
#define ENF_ESET_H

#include "enf_config.h"
#include "common/enf_memory.h"

#define ENF_SET_DEFAULT_SIZE 0

//-----------------------------------------------------------------------------
/*!
ESet
*/
//-----------------------------------------------------------------------------
template <class T> class ESet
{
public:
	//-----------------------------------------------------------------------
	/*!
	Default Set constructor.
	Allocates enough room for SET_DEFAULT_SIZE elements.
	*/
	//-----------------------------------------------------------------------
	ESet():
		size(0),
		count(0),
		data1(NULL)
	{
		if (ENF_SET_DEFAULT_SIZE > 0)
		{
			Grow(ENF_SET_DEFAULT_SIZE);
		}
	}


	//-----------------------------------------------------------------------
	/*!
	Set constructor.
	Allocates enough room for size elements. 

	\param nsize
	Size of set to be defined. 
	*/
	//-----------------------------------------------------------------------
	ESet(unsigned int nsize):
		count(0),
		size(0),
		data1(NULL)
	{
		if (nsize > 0)
		{
			Grow(nsize);
		}
	}

	void Reserve(unsigned int newsize)
	{
		if(this->size < newsize)
			Grow(newsize);
	}

	void SetSize(unsigned int newsize)
	{
		if(this->size != newsize)
			Grow(newsize);

		count = newsize;
	}

	//-----------------------------------------------------------------------
	/*!
	Set Constructor.

	\param array[]
	Specifies an array with sorted data.
	\param size
	Specifies the maximum number of array entries.
	*/
	//-----------------------------------------------------------------------
	ESet(const T array[], const unsigned int size)
	{
		data1 = NULL;
		count = size;
		if (size > 0)
		{
			Grow(size);
			for(unsigned int n = 0; n < size; n++)
				data1[n] = array[n];
		}
	}

	//-----------------------------------------------------------------------
	/*!
	Set copy constructor.

	\param s
	The Set that will be copied.
	*/
	//-----------------------------------------------------------------------
	ESet(const ESet<T> &s):
		count(0),
		size(0),
		data1(NULL)
	{
		if(s.count > 0)
		{
			Grow(s.count);
			count = s.count;
			for(unsigned int n = 0; n < count; n++)
				data1[n] = s[n];
		}
	}
	
   //-----------------------------------------------------------------------
	/*!
	Default destructor.	
	*/
	//-----------------------------------------------------------------------
	~ESet()
	{
		//destroy all elements
		for(unsigned int i = 0; i < size; i++)
		{
			(data1+i)->~T();
		}
		if (data1) MemoryManager::Free(data1);
	}

	//-----------------------------------------------------------------------
	/*!
	Set assignment operator.
	\param s
	The set that will be copied
	\return
	A reference to the assigned set.
	*/
	//-----------------------------------------------------------------------
	/*
	ENF_INLINE const ESet<T>& operator= (const ESet<T>& s)
	{
		count = s.count;
		if(size < s.count)
		{
			size = s.count;
			data1 = (T*)MemoryManager::ReAlloc(data1, sizeof(T) * size);
			enf_assert(data1!=NULL);
		}
		if (s.count > 0)
			memcpy(data1, s.GetArray(), s.count*sizeof(T));
		return *this;
	}
*/

	//-----------------------------------------------------------------------
	/*!
	Return an element from this set. Operation is O(1).
	\param i
	The index of the element that will be returned.
	\return
	Element i in the array.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE T& GetElement(unsigned int i) const
	{
		enf_assert(i < count);
		return data1[i];
	}


	//-----------------------------------------------------------------------
	/*!
	Return an element from this set. Operation is O(1).
	\param i
	The index of the element that will be returned.
	\return
	Element i in the array.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE T& GetElement(unsigned int i)
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	/*!
	Return the last element from this set. Operation is O(1).
	\return
	Element (cardinality-1) in the set.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE T& GetLastElement()
	{
		enf_assert(!IsEmpty());
		return data1[GetCardinality() - 1];
	}

	//-----------------------------------------------------------------------
	/*!
	Return the last element from this set. Operation is O(1).
	\return
	Element (cardinality-1) in the set.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE const T& GetLastElement() const
	{
		enf_assert(!IsEmpty());
		return data1[GetCardinality() - 1];
	}


	//-----------------------------------------------------------------------
	/*!
	Set indexing operator.
	\param i
	The index of the element that will be returned.
	\return
	The element i in the array.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE T& operator[] (unsigned int i) const
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	/*!
	Set indexing operator.
	\param i
	The index of the element that will be returned.
	\return
	The element i in the array.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE T& operator[] (unsigned int i)
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	/*!
	Returns the sets array representation.
	\return
	The sets representation.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE const T* GetArray() const
	{
		return data1;
	}


	//-----------------------------------------------------------------------
	/*!
	Returns the number of entries in the set. This can, and probably will,
	differ from the array size.
	\return
	The number of of entries in the set.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE unsigned int GetCardinality() const
	{	//number of elements
		return count;
	}


	//-----------------------------------------------------------------------
	/*!
	Returns true if the set is empty.
	\return
	True if the set has no entries, false otherwise.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE bool IsEmpty() const
	{
		return (count == 0);
	}


	//-----------------------------------------------------------------------
	/*!
	Clears the set.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE void Clear()
	{
		count = 0;
	}


	//-----------------------------------------------------------------------
	/*!
	Return index of a specific element.

	\param a  
	The element to search for.
	\return
	The index of this element, or -1 if not found.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE int GetIndexOf(const T &a) const
	{
		int l = 0;
		int r = count - 1;
		int i;
		for(;r >= l;)
		{
			i = (l + r) / 2;
			if(data1[i] > a)
			{
				r = i - 1;
			}
			else
			{
				if(data1[i] == a)
				{
					return i;
				}
				l = i + 1;
			}
		}
		return -1;
	}

	ENF_INLINE int FindGE(const T &a) const
	{
		int l = 0;
		int r = count - 1;
		int i;
		for(;r >= l;)
		{
			i = (l + r) / 2;
			if(data1[i] > a)
			{
				r = i - 1;
			}
			else
			{
				//we want to avoid <= operator which is not common
				if(!(data1[i] > a) && ((i == ((int)count - 1)) || data1[i + 1] > a))
				{
					return i;
				}
				l = i + 1;
			}
		}
		return -1;
	}
	//-----------------------------------------------------------------------
	/*!
	Determine if this set contains a specific element.

	\param a
	The element
	\return
	True if the element is in the set.
	False if the element is not in the set.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE bool Contains(const T &a) const
	{
		return GetIndexOf(a) != -1;
	}

	//-----------------------------------------------------------------------
	/*!
	Insert a element to the set if it does not already exists within it. 
	If this Set is full it will be expanded to hold the new element. 

	\param a
	The element that will be added to this set.
	\param index
	Optional pointer to index where element is inserted.
	\return
	True if the element was inserted.
	False if the element was already in the set.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE bool Insert(const T &a, unsigned int* index = NULL)
	{
		int p = 0;
		int r = count - 1;
		int i;
		for(;r >= p;)
		{
			i = (p + r) / 2;
			if(data1[i] > a)
			{
				r = i - 1;
			}
			else
			{
				if(data1[i] == a)
				{
					if (index) *index = i;
					return false;
				}
				p = i + 1;
			}
		}

		//move the elements [l count-1] to [l+1 count]
		if (size <= count) Grow(1 + size * 2);
		if (count - p > 0)
			memmove(&data1[p + 1], &data1[p], (count - p) * sizeof(T));
		
		//initialize this element as new one, as it was duplicated to [p + 1] position
		new ((void*)(&data1[p])) T;

		data1[p] = a;
		count++;
		if (index) *index = p;
		return true;
	}


	//-----------------------------------------------------------------------
	/*!
	Subtracts the element on position i from the set.
	Operation is O(n). Fastest removal on high index elements.
	\param i
	The position of the element that will be subtracted from this set.
	\return
	The element that was removed.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE T RemoveElement(unsigned int i)
	{
		enf_assert(i < count);
		T tmp = data1[i];
		//destroy removed element
		(data1+i)->~T();
		memmove(data1 + i, data1 + i + 1, (count - i - 1) * sizeof(T));
		//reinitialize element at the end, which become duplicated now
		new ((void*)(&data1[count - 1])) T;

		count--;
		return tmp;
	}

	//-----------------------------------------------------------------------
	/*!
	Remove one element from the set, if it exists.
	\param a
	The element that will be subtracted from this set.
	\return
	True if the element was removed. False if no 
	element was found.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE bool Remove(const T &a)
	{
		int l = 0;
		int r = count - 1;
		int i;
		for(;r >= l;)
		{
			i = (l + r) / 2;
			if(data1[i] > a)
			{
				r = i - 1;
			}
			else
			{
				if(data1[i] == a)
				{
					RemoveElement(i);
					return true;
				}
				l = i + 1;
			}
		}
		return false;
	}


	//-----------------------------------------------------------------------
	/*!
	Insert a given set to this set, more formaly called a union between the sets.
	\param s
	The set that will be added to this one.
	*/
	//-----------------------------------------------------------------------	
	ENF_INLINE void Insert(const ESet *s)
	{
		if (s == NULL) return;
		
		//Make room for this set + this set and other set
		Grow(2 * GetCardinality() + s->GetCardinality());
		T* data2 = data1 + GetCardinality();

		unsigned int i = 0, j = 0, k = 0; //index in local, forigin and destination arrays
		if(s->GetCardinality() == 0) return;
		if(count == 0)
		{
			*this = *s;
			return;
		}
		while(true)
		{
			if(data1[i] < s->GetElement(j))
			{
				data2[k++] = data1[i++];
			}
			else
			{
				if(data1[i] > s->GetElement(j))
				{
					data2[k++] = s->GetElement(j++);
				}
				else
				{
					data2[k++] = data1[i++];
					j++;
				}
			}
			if(i == count)
			{
				memcpy(data2 + k, s->data1 + j, (s->GetCardinality() - j) * sizeof(T));
				k += s->GetCardinality() - j;
				break;
			}

			if(j == s->GetCardinality())
			{
				memcpy(&data2[k], &data1[i], (count - i) * sizeof(T));
				k += count - i;
				break;
			}
		}
		memmove(data1, data2, k * sizeof(T));
		count = k;
	}


	//-----------------------------------------------------------------------
	/*!
	Remove a given set from this set, more formaly called a difference between
	this set and the given one.
	\param s
	The given set.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE void Remove(const ESet *s)
	{
		if (s == NULL) return;

		//Make room for twice this set
		grow(2 * GetCardinality());
		T* data2 = data1 + GetCardinality();

		unsigned int i = 0, j = 0, k = 0; //index in local, foreign and destination array
		if(s->GetCardinality() == 0) return;
		if(count == 0) return;

		while(true)
		{
			if(data1[i] < s->GetElement(j))
			{
				data2[k++] = data1[i++];
			}
			else
			{
				if(data1[i] == s->GetElement(j)) i++;
				j++;
			}
			if(i == count) break;
			if(j == s->GetCardinality())
			{
				memcpy(data2 + k, data1 + i, (count - i) * sizeof(T));
				k += count - i;
				break;
			}
		}
		memmove(data1, data2,k * sizeof(T));
		count = k;
	}

	ESet<T>& operator=(const ESet<T>& set)
	{
		Clear();
		for(uint i = 0; i < set.GetCardinality(); i++)
			Insert(set.GetElement(i));

		return *this;
	}

private:
	T *data1;
	unsigned int size;
	unsigned int count;

	//-----------------------------------------------------------------------
	/*!
	Resizes the available memory for the array representing the set.

	\param nsize
	The number of entries that the set should be able to hold.
	*/
	//-----------------------------------------------------------------------
	void Grow(unsigned int nsize)
	{
		data1 = (T*)MemoryManager::ReAlloc(data1, nsize * sizeof(T));

		enf_assert(data1);

		//call constructors for new objects
		for(unsigned int n = this->size; n < nsize; n++)
		{
			T* tmp=data1 + n;
			new ((void*)(tmp)) T;
		}

		this->size = nsize;
	}

	//-----------------------------------------------------------------------
	/*!
	\return
	Returns the number of elements the current array can hold.
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE unsigned int GetArraySize()
	{
		return size;
	}
};

//-----------------------------------------------------------------------------
/*!
EPair is convenient way when you want to store some types into ESet
but you want to sort it by other key than the elements itself.
K stands for key element and T stands for type you want to order
*/
//-----------------------------------------------------------------------------
template <class K, class T> struct EPair
{
	K		Key;
	T		Data;

	bool operator==(const EPair<K, T>& other) const
	{
		return (Key == other.Key);
	}

	bool operator<(const EPair<K, T>& other) const
	{
		return (Key < other.Key);
	}

	bool operator>(const EPair<K, T>& other) const
	{
		return (Key > other.Key);
	}

	EPair()
	{
	}

	EPair(K key)
	{
		Key = key;
	}

	EPair(K key, T data)
	{
		Key = key;
		Data = data;
	}
};

//!Creates EPair based upon used types
template <class K, class D> EPair<K,D> MakePair(K k, D d) {return EPair<K,D>(k,d);}


#endif //ENF_ESET_H