//-----------------------------------------------------------------------------
// File: enf_hashmap.h
//
// Desc: Hash map
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------
#pragma once
#ifndef ENF_HASHMAP_H
#define ENF_HASHMAP_H

#include "enf_config.h"
#include "common/enf_types.h"

struct ExactStringHashFunc
{
	uint operator()(const char *str) const
	{
		return (uint)str;
	}

	int compare(const char* str1, const char* str2) const
	{
		return (str1 == str2) ? 0 : 1;
	}
};

struct StringHashFunc
{
	uint operator()(const char *str) const
	{
		int c;
		unsigned int hashval = 0;

		while ( (c = *str++) != 0 )
			hashval = hashval * 37 + tolower(c);

		return hashval;
	}

	int compare(const char* str1, const char* str2) const
	{
		return enf::strcmpi(str1, str2);
	}
};

struct StringHashFuncCaseSensitive
{
	uint operator()(const char *str) const
	{
		int c;
		unsigned int hashval = 0;

		while ( (c = *str++) != 0 )
			hashval = hashval * 37 + c;

		return hashval;
	}

	int compare(const char* str1, const char* str2) const
	{
		return enf::strcmp(str1, str2);
	}
};


const int ENF_HASHMAP_SIZE = 8;

//-----------------------------------------------------------------------------
/*!
	EHashMap is a Hashed Associative Container that associates objects of type
	Key with objects of type Data. It is also a Unique Associative Container, 
	meaning that no two elements have keys that compare equal using 
	bool Key::operator==(const Key&).
	HashFunc is a class with a const function operator that converts Key objects 
	into uintegers.
	<code>
		struct HashFunc {uint operator()(const Key *) const; };
	</code>
*/
template <class Key, class Data, class HashFunc = StringHashFunc> class EHashMap
{
	struct Node
	{
		enum { EMPTY, OCCUPIED, REMOVED } m_Status;
		Key	m_Key;
		Data	m_Data;
		Node() : m_Status(EMPTY), m_Key(), m_Data() {}
	};

public:
	ENF_DECLARE_MMOBJECT(EHashMap);

	/*!
		Creates a hashmap with initial size ENF_HASHMAP_SIZE
	*/
	EHashMap() : m_Table(NULL), m_Size(0), m_Removed(0), m_Elements(0)
	{
		m_Size = ENF_HASHMAP_SIZE;
		m_Table = (Node*) MemoryManager::Alloc(m_Size * sizeof(Node), __FILE__, __LINE__);

		for(uint i = 0; i < m_Size; i++)
		{
			Node* tmp = m_Table + i;
			new ((void*)(tmp)) Node;
		}
	}
	
	/*!
		Constructor with initial minimum capacity.
		The actual table size will be the smallest power-of-two 
		greater than or equal to 2 * _minimumCapacity.
	*/
	EHashMap(uint _minimumCapacity) : m_Size(0), m_Elements(0), m_Removed(0), m_Table(NULL)
	{
		for (m_Size = 4; m_Size < 2 * _minimumCapacity && m_Size < 0x80000000; m_Size <<= 1);
		m_Table = (Node*) MemoryManager::Alloc(m_Size * sizeof(Node), __FILE__, __LINE__);
		enf_assert(m_Table != NULL);

		for(uint i = 0; i < m_Size; i++)	
		{
			Node* tmp = m_Table + i;
			new ((void*)(tmp)) Node;
		}
	}

	virtual ~EHashMap()
	{
		if (m_Table)
		{
			// free memory for old table
			for(uint i=0; i<m_Size; i++)
			{
				(m_Table + i)->~Node();
			}
			MemoryManager::Free(m_Table);
			m_Table = NULL;
		}
	}

	/*!
	Insert new element into hash map.
		
	\param _key
	Key of element to be inserted.
	\param _data
	Data of element to be inserted.
	*/
	bool Insert(Key _key, Data _data)
	{
		//make sure we have room in the table
		EnsureCapacity();

		//initial hash position and probe increment
		uint pos;
		uint delta;
		DoubleHash(_key, &pos, &delta);

		//find element, empty or removed
		while(m_Table[pos].m_Status == Node::OCCUPIED && m_Hash.compare(m_Table[pos].m_Key, _key) != 0)
			pos = (pos + delta) % m_Size;

		// if element was already here return false
		if (m_Table[pos].m_Status == Node::OCCUPIED)
			return false;

		uint freePos = pos;

		// continue looking for the element until empty 
		while(m_Table[pos].m_Status != Node::EMPTY && !(m_Table[pos].m_Status == Node::OCCUPIED && m_Hash.compare(m_Table[pos].m_Key, _key) == 0)) 
			pos = (pos + delta) % m_Size;
		
		// if element was already here return false
		if (m_Table[pos].m_Status == Node::OCCUPIED)
			return false;

		//insert element
		Node *n = &m_Table[freePos];
	
		// decrease removed counter if the node was a removed one
		if (n->m_Status == Node::REMOVED) 
			m_Removed--;
	
		n->m_Status = Node::OCCUPIED;
		n->m_Key = _key;
		n->m_Data = _data;
	
		//increment element counter
		m_Elements++;

		// return true to indicate that a new element was inserted
		return true;
	}
	
	/*!
	Remove an element from hash map.
		
	\param _key
	Key of element to be removed.
	*/
	bool Remove(const Key & _key)
	{
		if (m_Elements == 0) return false;
		
		//initial hash position and probe increment
		uint pos;
		uint delta;
		DoubleHash(_key, &pos, &delta);

		//find element or empty
		while(m_Table[pos].m_Status != Node::EMPTY && !(m_Table[pos].m_Status == Node::OCCUPIED && m_Hash.compare(m_Table[pos].m_Key, _key) == 0)) 
			pos = (pos + delta) % m_Size;
		
		//was element found?
		if (m_Table[pos].m_Status == Node::OCCUPIED)
		{
			// mark node as deleted
			m_Table[pos].m_Status = Node::REMOVED;
			// update counters		
			m_Elements--;
			m_Removed++;
			//indicate success
			return true;
		}

		//indicate failure
		return false;
	}

	/*!
	Search for an key in the hash map.
		
	\param _key
	The key of the element to find
	\return
	True if found, false otherwise
	*/
	bool Contains(const Key& _key) const
	{	
		if(m_Elements == 0) return false;

		//initial hash position and probe increment
		uint pos, delta;

		DoubleHash(_key, &pos, &delta);

		//find element or NULL
		while(m_Table[pos].m_Status != Node::EMPTY && !(m_Table[pos].m_Status == Node::OCCUPIED && m_Hash.compare(m_Table[pos].m_Key, _key) == 0)) 
			pos = (pos + delta) % m_Size;

		//was element found?
		return (m_Table[pos].m_Status == Node::OCCUPIED);
	}

	/*!
		Search for an element with the given key.
			
		\param _key
		The key of the element to find
		\return
		Pointer to element data if found, NULL otherwise.
	*/
	Data Find(const Key& _key) const
	{	
		if (m_Elements == 0) return NULL;

		//initial hash position and probe increment
		uint pos, delta;

		DoubleHash(_key, &pos, &delta);

		//find element or NULL
		while(m_Table[pos].m_Status != Node::EMPTY && !(m_Table[pos].m_Status == Node::OCCUPIED && m_Hash.compare(m_Table[pos].m_Key, _key) == 0)) 
			pos = (pos + delta) % m_Size;

		//return element if found
		return (m_Table[pos].m_Status == Node::OCCUPIED) ? m_Table[pos].m_Data : NULL;
	}

	bool Find(const Key& _key, Data& _data) const
	{	
		if (m_Elements == 0) return false;

		//initial hash position and probe increment
		uint pos, delta;

		DoubleHash(_key, &pos, &delta);

		//find element or NULL
		while(m_Table[pos].m_Status != Node::EMPTY && !(m_Table[pos].m_Status == Node::OCCUPIED && m_Hash.compare(m_Table[pos].m_Key, _key) == 0)) 
			pos = (pos + delta) % m_Size;

		//return element if found
		if(m_Table[pos].m_Status == Node::OCCUPIED)
		{
			_data = m_Table[pos].m_Data;
			return true;
		}
		return false;
	}

	/*!
	Return the i:th key in the map.
	Note: This operation is O(n) complexity. Use with care!

	\param i
	The position of the key in the map
	\return
	The key on the i:th position
	*/
	Key GetKey(uint i)
	{
		uint c = 0;
		for(uint j = 0; j < m_Size; j++)
		{
			if (m_Table[j].m_Status == Node::OCCUPIED)
			{
				if (c++ == i) return m_Table[j].m_Key;
			}
		}
		return NULL;
	}

	/*!
	Return the i:th element in the map.
	Note: This operation is O(n) complexity. Use with care!

	\param i
	The position of the element in the map
	\return
	The element on the i:th position
	*/
	Data GetElement(uint i)
	{
		uint c = 0;
		for(uint j = 0; j < m_Size; j++)
		{
			if (m_Table[j].m_Status == Node::OCCUPIED)
			{
				if (c++ == i) return m_Table[j].m_Data;
			}
		}
		return NULL;
	}

	/*!
		\return
		The number of elements in the hashmap.
	*/
	inline uint GetCardinality() const
	{
		return m_Elements;
	}

	/*!
		Clears the hash map.
	*/
	void Clear()
	{
		for(uint i = 0; i < m_Size; i++)
			m_Table[i].m_Status = Node::EMPTY;
		
		m_Elements = 0;
		m_Removed = 0;
	}

	/*!
		Rehash the hash map.
	*/
	void Rehash()
	{
		// store old table
		Node* oldTable = m_Table;

		//allocate table
		m_Table = (Node*)MemoryManager::Alloc(m_Size * sizeof(Node), __FILE__, __LINE__);
		enf_assert(m_Table != NULL);
		m_Removed = 0;

		uint i;

		for(i = 0; i < m_Size; i++)	
		{
			Node* tmp = m_Table + i;
			new ((void*)(tmp)) Node;
		}
		
		//rehash
		for(i = 0; i < m_Size; i++)
		{
			if (oldTable[i].m_Status == Node::OCCUPIED)
			{
				//initial hash position and probe increment
				uint pos;
				uint delta;
				doubleHash(oldTable[i].m_Key);

				//find element, empty or removed
				while(m_Table[pos].m_Status == Node::OCCUPIED)
					pos = (pos + delta) % m_Size;
				
				m_Table[pos].m_Status = Node::OCCUPIED;
				m_Table[pos].m_Key = oldTable[i].m_Key;
				m_Table[pos].m_Data = oldTable[i].m_Data;
			}
		}
		
		// free memory for old table
		for(i = 0;i < m_Size; i++)
		{
			(oldTable+i)->~Node();
		}
		MemoryManager::Free(oldTable);
	}

protected:
	Node*		m_Table;
	uint		m_Size;
	// the number of nodes marked as deleted
	uint		m_Removed;
	// the number of nodes actually used
	uint		m_Elements;
	HashFunc	m_Hash;

	inline void DoubleHash(const Key& _key, uint* h, uint* p) const
	{
		uint hash = m_Hash(_key);
		*h = hash % m_Size;
		*p = (2 * hash + 1) % m_Size;
	}

	void EnsureCapacity()
	{
		if (m_Elements + m_Removed >= m_Size / 2)
		{
			int i;

			// store old table and size
			uint oldSize = m_Size;
			Node* oldTable = m_Table;
		
			// determine the new size
			if (m_Elements >= m_Size / 2) m_Size *= 2;

			// allocate the new table
			m_Table = (Node*)MemoryManager::Alloc(m_Size * sizeof(Node), __FILE__, __LINE__);
			enf_assert(m_Table != NULL);
			m_Removed = 0;

			//GCC internal compiler error if for-loop used here
			i = 0;
			while(i < (int)m_Size)
			{
				Node* tmp = m_Table + i;
				new (tmp) Node;
				i++;
			}
               
			// rehash
			for(i = 0; i < (int)oldSize; i++)
			{
				if (oldTable[i].m_Status == Node::OCCUPIED)
				{	
					//initial hash position and probe increment
					uint pos;
					uint delta;						
					DoubleHash(oldTable[i].m_Key, &pos, &delta);

					//find element, empty or removed
					while(m_Table[pos].m_Status == Node::OCCUPIED)
						pos = (pos + delta) % m_Size;
					m_Table[pos].m_Status = Node::OCCUPIED;
					m_Table[pos].m_Key = oldTable[i].m_Key;
					m_Table[pos].m_Data = oldTable[i].m_Data;
				}
			}

			// free memory for old table				
			//GCC internal compiler error if for-loop used here
			i = 0;
			while(i < (int)oldSize)
			{
				(oldTable + i)->~Node();
				i++;
			}

			MemoryManager::Free((void*)oldTable);
		}
	}
};


#endif //ENF_HASHMAP