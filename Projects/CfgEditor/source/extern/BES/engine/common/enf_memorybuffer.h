//-----------------------------------------------------------------------------
// File: enf_memorybuffer.h
//
// Desc: Growable memory buffer
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#ifndef ENF_MEMORYBUFFER_H
#define ENF_MEMORYBUFFER_H

#include "enf_config.h"
#include "common/enf_types.h"

/*!
MemoryBuffer is a dynamic-sized general memorybuffer.
*/
class PUBLIC_API MemoryBuffer
{
public:
	ENF_DECLARE_MMOBJECT(MemoryBuffer);

	/*!
	Construct a new memorybuffer.

	\param length
	Initial length to allocate for the buffer.
	*/
	MemoryBuffer(uint length = 1024);

	/*!
	Destructor. Deallocates the data associated with this buffer.
	*/
	~MemoryBuffer();

	/*!
	Set current position in the buffer. If the position is past the end of
	buffer, its set to last byte.
	*/
	void Seek(uint pos);

	/*!
	Get current position in the buffer.
	*/
	ENF_INLINE uint Tell() const
	{
		return m_iPos;
	}

	/*!
	Get the current used size of the buffer in bytes. 
	Use getLength to get the total buffer length (which may be larger).
	*/
	ENF_INLINE uint GetSize() const
	{
		return m_iSize;
	}

	/*!
	Return true if we reached last byte in buffer.
	*/
	ENF_INLINE bool Eof() const
	{ 
		return (m_iPos == m_iSize);
	}

	/*!
	Write data into the buffer. The buffer will grow if needed.

	\param buffer
	Data to write into the MemoryBuffer
	\param length
	Length of the data in buffer in bytes.
	*/
	void Write(const unsigned char* buffer, const uint length);

	/*!
	Read data from the buffer.

	\param buffer
	Data to read from the MemoryBuffer. The buffer must be of sufficient
	size to hold the data.
	\param length
	Length of the data to read in bytes.
	*/
	int Read(unsigned char* buffer, const uint length);

	/*!
	Grow the buffer to new size.
	This function cannot be used to shrink the buffer.

	\param newMinLength
	New minimum length that the buffer should be able to hold.
	*/
	void Grow(uint newMinLength);

	/*!
	Release the old buffer and allocate a new one.

	\param newLength
	New minimum length that the buffer should be able to hold.
	*/
	void Reallocate(uint newLength);

	/*!
	Get the length of the buffer.
	Use getSize if you want to know how much of the buffer that is used.
	*/
	ENF_INLINE uint GetLength() const
	{
		return m_iLength;
	}

	/*!
	Get a raw pointer to the data kept in this buffer.
	*/
	ENF_INLINE unsigned char* GetData()
	{
		return m_pBuffer;
	}

protected:
	unsigned char*	m_pBuffer;
	uint				m_iLength;
	uint				m_iPos;
	uint				m_iSize;
};

#endif

