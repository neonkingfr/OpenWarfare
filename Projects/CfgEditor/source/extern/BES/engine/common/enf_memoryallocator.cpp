//-----------------------------------------------------------------------------
// File: enf_memoryallocator.cpp
//
// Desc: Memory allocator for small blocks
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#include "precomp.h"
#include "common/enf_memory.h"
#include "common/enf_types.h"
#include "common/enf_eset.h"
#include "enf_mutex.h"
#include "enf_main.h"

#ifdef ENF_LEAK_DETECTOR
	#include "common/enf_memoryreport.h"
	const uint ENF_DEBUG_FULL = 0xdebaf001;
	const uint ENF_DEBUG_FREE = 0xdebafeee;
#endif

const uint MEMALLOC_BLOCK		= 4000;		//minimum size of block
const uint MEMALLOC_MIN_ITEMS	= 256;		//minimum items in block

class PUBLIC_API ISmallBlockAllocator: public SmallBlockAllocator
{
private:
	class MemoryBlock;

	CMutex	m_Mutex;

	//!granularity of allocator
	static const unsigned int QUANTIZE	= 8;
	//!Max size of item
	static const unsigned int MAX_SIZE	= 512;

	//!Max blocks needed for that
	static const int MAX_BLOCKS = (MAX_SIZE / QUANTIZE);

	MemoryBlock*		m_pBlocks[MAX_BLOCKS];
	ESet<const void*>	m_pSortedBlocks;

	size_t	m_iAllocated;
	size_t	m_iNumAllocations;
	size_t	m_iTotalAllocated;

	//-----------------------------------------------------------------------------------
	// quantize size
	//-----------------------------------------------------------------------------------
	static __forceinline size_t FixSize(size_t size)
	{
		return ((size + (QUANTIZE - 1)) & (~(QUANTIZE - 1)));
	}

public:
	//memory item 
	struct MemItem
	{
		MemItem* m_pNext;
	};

	//struct for one memory block containing items, struct is allocated 
	//m_iHeadSize memory = sizeof(BlockHead) + m_iBlockSize -> the pointer 
	//to real data is &BlockHead + sizeof(BlockHead)
	struct BlockHead
	{
		MemoryBlock*	m_pMemoryBlock;		//our owner
		BlockHead**		m_pPreviousBlock;		//pointer to prev memory block
		BlockHead*		m_pNextBlock;			//pointer to next memory block
		MemItem*			m_pFirstFree;			//pointer to first free item in block
		int				m_iFullItems;			//number of full items, max can be m_iBlockItems, then it is necessary to create new block
	};

#ifdef ENF_LEAK_DETECTOR
	//struct for debug
	struct DebugMemory
	{
		uint			m_iDummy;		//testing int
		const char* m_pFileName;	//name of file
		uint			m_iLine;			//line in source file
	};
#endif

	ISmallBlockAllocator();
	~ISmallBlockAllocator();

	ENF_INLINE bool Register(size_t size);
	ENF_INLINE bool UnRegister(size_t size);

	void GetStats(MemoryStats& stats) const;

	void Dump(const char* str) const;

	void Flush() { };

	void *Alloc(size_t size, const char* file, int line);
	void *Alloc(size_t size);
	bool Free(void *pointer);
	void FreeAll();
};

ISmallBlockAllocator g_SmallBlockAllocator;
SmallBlockAllocator* g_pSmallBlockAllocator = &g_SmallBlockAllocator;

//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::GetStats(MemoryStats& stats) const
{
	stats.Allocated = m_iAllocated;
	stats.NumAllocations = m_iNumAllocations;
	stats.NumBlocks = m_pSortedBlocks.GetCardinality();
	stats.Overhead = (m_iTotalAllocated + m_pSortedBlocks.GetCardinality() * sizeof(void*)) - m_iAllocated;
}

//-----------------------------------------------------------------------------------
// SmallBlockAllocator for managing small memory allocs
//-----------------------------------------------------------------------------------
class ISmallBlockAllocator::MemoryBlock
{
private:
	friend class ISmallBlockAllocator;

	ISmallBlockAllocator*	m_pAllocator;	//our owner
	size_t		m_iItemSize;				//registered size of item
	size_t		m_iBlockSize;				//size of whole block
	size_t		m_iHeadSize;				//size of data and block together (is in one memory area)
	size_t		m_iBlockItems;				//size of one item in block, must be bigger than 3 and m_iBlockSize is divisible by 

	size_t		m_iRegSize;					//registered size

	BlockHead*	m_pFirstBlock;				//first block with items
	BlockHead*	m_pFreeBlock;				//block with free items

protected:
	ENF_INLINE MemoryBlock(ISmallBlockAllocator* allocator, size_t size); 
	~MemoryBlock();
	
	//-----------------------------------------------------------------------------------
	// Name : Release
	// Desc : release all memory allocated by manager
	//-----------------------------------------------------------------------------------
	ENF_INLINE void Release();

	//-----------------------------------------------------------------------------------
	// Name : Create
	// Desc : create new
	//-----------------------------------------------------------------------------------
	ENF_INLINE void Create(size_t size);

	//-----------------------------------------------------------------------------------
	// Name : AllocNewBlock
	// Desc : create new block
	//-----------------------------------------------------------------------------------
	ENF_INLINE BlockHead* AllocNewBlock();

	//-----------------------------------------------------------------------------------
	// Name : AllocNewBlock
	// Desc : create new block
	//-----------------------------------------------------------------------------------
	ENF_INLINE void ReleaseBlock(BlockHead* block);

	//-----------------------------------------------------------------------------------
	// Name : GetBestFreeBlock
	// Desc : returns the block with the largest number of unused items
	//-----------------------------------------------------------------------------------
	ENF_INLINE void FindBestFreeBlock();


	#ifdef ENF_LEAK_DETECTOR
		void DebugInfo (DebugMemory* p, int info, const char *file = NULL, int num = -1);
		void TestDebugInfo(DebugMemory *p, int info);
		void DumpMemory(BlockHead *pblock);
	#endif

public:
	ENF_DECLARE_MMOBJECT(MemoryBlock);

	//-----------------------------------------------------------------------------------
	// Name : Alloc
	// Desc : return pointer to new allocated memory 
	//-----------------------------------------------------------------------------------
		ENF_INLINE void *Alloc(const char *file, int line);
		ENF_INLINE void *Alloc();

	//-----------------------------------------------------------------------------------
	// Name : FreeAll
	// Desc : deallocate all the allocated the memory 
	//-----------------------------------------------------------------------------------
	ENF_INLINE void FreeAll(void *pointer);


	//-----------------------------------------------------------------------------------
	// Name : GetInstanceSize
	// Desc : returns instance size in memory (with all allocated items)
	//-----------------------------------------------------------------------------------
	ENF_INLINE size_t GetInstanceSize() const;
};

//-----------------------------------------------------------------------------------
// constructor
//-----------------------------------------------------------------------------------
ISmallBlockAllocator::MemoryBlock::MemoryBlock(ISmallBlockAllocator* allocator, size_t size):
	m_pAllocator(allocator),
	m_pFirstBlock(NULL)
{
	Create (size);
}

//-----------------------------------------------------------------------------------
// destructor
//-----------------------------------------------------------------------------------
ISmallBlockAllocator::MemoryBlock::~MemoryBlock()
{
	Release();
}

//-----------------------------------------------------------------------------------
// Name : Release
// Desc : release all memory allocated by manager
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::MemoryBlock::Release()
{
	BlockHead* next = m_pFirstBlock, *tmp;

	//loop through all blocks
	while (next)
	{
		tmp  = next;
		next = next->m_pNextBlock;
		m_pAllocator->m_pSortedBlocks.Remove(tmp);
		MemoryManager::Free(tmp);
		m_pAllocator->m_iTotalAllocated -= m_iHeadSize;
	}

	m_pFirstBlock = NULL;
}


//-----------------------------------------------------------------------------------
// Name : Create
// Desc : create new
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::MemoryBlock::Create(size_t size)
{
	Release();

	m_iRegSize =  size;

	//set size
	size = enf_max(size, 4U);

	#ifdef ENF_LEAK_DETECTOR
		//4 bytes dummy object == ENF_DEBUG_NUM
		//4 bytes pointer to filename __FILE__
		//4 bytes pointer to line in filename__NUM__
		size += sizeof(DebugMemory);
	#endif

	m_iItemSize		= size;

	//compute size of block
	if (m_iItemSize * MEMALLOC_MIN_ITEMS < MEMALLOC_BLOCK)
		m_iBlockSize = MEMALLOC_BLOCK;
	else
		m_iBlockSize = ((m_iItemSize * MEMALLOC_MIN_ITEMS) / MEMALLOC_BLOCK + 1) * MEMALLOC_BLOCK;

	m_iBlockItems = m_iBlockSize/size;
	m_iHeadSize = sizeof(BlockHead) + m_iBlockSize;
	m_pFreeBlock = m_pFirstBlock = AllocNewBlock();
	m_pFirstBlock->m_pPreviousBlock = &m_pFirstBlock;
}


//-----------------------------------------------------------------------------------
// Name : AllocNewBlock
// Desc : create new block
//-----------------------------------------------------------------------------------
ISmallBlockAllocator::BlockHead* ISmallBlockAllocator::MemoryBlock::AllocNewBlock()
{
	//create new block head
	BlockHead *newone = (BlockHead*)MemoryManager::Alloc(m_iHeadSize, __FILE__, __LINE__);
	m_pAllocator->m_iTotalAllocated += m_iHeadSize;

	m_pAllocator->m_pSortedBlocks.Insert(newone);
	newone->m_pMemoryBlock = this;
	newone->m_pPreviousBlock = NULL;
	newone->m_pNextBlock = NULL;
	newone->m_iFullItems = 0;

	//link items inside block
	ubyte *pstart = (ubyte*) newone + sizeof(BlockHead);

	newone->m_pFirstFree = (MemItem*) pstart;

	for (uint i = 0; i < m_iBlockItems - 1; i++)
	{
		//pointer to next free block
		*((void**) pstart) = (void*) (pstart + m_iItemSize);

		#ifdef ENF_LEAK_DETECTOR
			DebugInfo((DebugMemory*) (pstart + m_iRegSize), ENF_DEBUG_FREE);
		#endif

		pstart += m_iItemSize;
	}


	//pointer to next free block
	*((uint*) pstart) = NULL;

	#ifdef ENF_LEAK_DETECTOR
	DebugInfo((DebugMemory*) (pstart + m_iRegSize), ENF_DEBUG_FREE);
	#endif

	return newone;
}


//-----------------------------------------------------------------------------------
// Name : ReleaseBlock
// Desc : releases block
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::MemoryBlock::ReleaseBlock(BlockHead* block)
{
	//unlink with previous
	*block->m_pPreviousBlock = block->m_pNextBlock;

	//unlink with next
	if (block->m_pNextBlock)
		block->m_pNextBlock->m_pPreviousBlock = block->m_pPreviousBlock;

	//find best free block
	if (m_pFreeBlock == block)
		FindBestFreeBlock();

	m_pAllocator->m_pSortedBlocks.Remove(block);
	//delete
	MemoryManager::Free(block);
	m_pAllocator->m_iTotalAllocated -= m_iHeadSize;
}

//-----------------------------------------------------------------------------------
// Name : GetBestFreeBlock
// Desc : returns the block with the largest number of unused items
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::MemoryBlock::FindBestFreeBlock() 
{ 
	BlockHead* pstart = m_pFirstBlock;

	//loop through all items
	while (pstart)
	{
		if (pstart->m_pFirstFree)
		{
			m_pFreeBlock = pstart;
			return;
		}

		pstart = pstart->m_pNextBlock;
	}

	//create new
	pstart = AllocNewBlock();

	//and link it at the begin
	pstart->m_pPreviousBlock = &m_pFirstBlock;
	pstart->m_pNextBlock = m_pFirstBlock;
	if(m_pFirstBlock)
		m_pFirstBlock->m_pPreviousBlock = &pstart->m_pNextBlock;
	m_pFreeBlock = m_pFirstBlock = pstart;
}


//-----------------------------------------------------------------------------------
// Name : Alloc
// Desc : alloc new
//-----------------------------------------------------------------------------------
void *ISmallBlockAllocator::MemoryBlock::Alloc(const char *file, int line)
{
	MemItem* pfree = m_pFreeBlock->m_pFirstFree;

	#ifdef ENF_LEAK_DETECTOR
		DebugMemory* deb = (DebugMemory*) ((ubyte*) pfree + m_iRegSize);

		TestDebugInfo(deb, ENF_DEBUG_FREE);
		DebugInfo(deb, ENF_DEBUG_FULL, file, line);
	#endif

	m_pFreeBlock->m_pFirstFree = pfree->m_pNext;
	m_pFreeBlock->m_iFullItems++;

	//if we are on the end of block find another block with the most free items for another allocation
	if (!m_pFreeBlock->m_pFirstFree)
		FindBestFreeBlock();	

	return pfree;
}

//-----------------------------------------------------------------------------------
void *ISmallBlockAllocator::MemoryBlock::Alloc() 
{ 
	MemItem* pfree = m_pFreeBlock->m_pFirstFree;

	#ifdef ENF_LEAK_DETECTOR
		DebugMemory* deb = (DebugMemory*) ((ubyte*) pfree + m_iRegSize);

		TestDebugInfo(deb, ENF_DEBUG_FREE);
		DebugInfo(deb, ENF_DEBUG_FULL, NULL, -1);
	#endif

	m_pFreeBlock->m_pFirstFree = pfree->m_pNext;
	m_pFreeBlock->m_iFullItems++;

	#ifdef _DEBUG
		//fill allocated memory by 0xbaadf00d as MSVC does
		FillMemory32(pfree, (m_iRegSize / 4), 0xbaadf00d);
	#endif

	//if we are on the end of block find another block with the most free items for another allocation
	if (!m_pFreeBlock->m_pFirstFree)
		FindBestFreeBlock();	

	return pfree;
}

//-----------------------------------------------------------------------------------
// Name : FreeAll
// Desc : deallocate all the allocated the memory 
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::MemoryBlock::FreeAll(void *pointer)
{
	Release();
	
	m_pFreeBlock = m_pFirstBlock = AllocNewBlock();
	m_pFirstBlock->m_pPreviousBlock = &m_pFirstBlock;
}

//-----------------------------------------------------------------------------------
// Name : GetInstanceSize
// Desc : returns instance size in memory
//-----------------------------------------------------------------------------------
size_t ISmallBlockAllocator::MemoryBlock::GetInstanceSize() const
{
	int num = 0;

	BlockHead* pblock = m_pFirstBlock;

	while (pblock)
	{
		pblock->m_pNextBlock;
		num++;
	}

	return sizeof(MemoryBlock) + num * m_iHeadSize;
}


#ifdef ENF_LEAK_DETECTOR

//-----------------------------------------------------------------------------------
// Name : DebugInfo
// Desc : fill to p the debug info
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::MemoryBlock::DebugInfo (DebugMemory* p, int info, const char *file, int num)
{
	p->m_iDummy		= info;
	p->m_pFileName	= file;
	p->m_iLine		= num;
}

//-----------------------------------------------------------------------------------
// Name : DebugInfo
// Desc : fill to p the debug info
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::MemoryBlock::TestDebugInfo(DebugMemory *p, int info)
{
	if (p->m_iDummy == info)
		return;

	char str[512];

	if (p->m_pFileName)
		sprintf(str, "SmallBlockAllocator::MemoryBlock(%d, %d) memory overwritten in file %s on line %d", m_iRegSize, p->m_pFileName, p->m_iLine);
	else
		sprintf(str, "SmallBlockAllocator::MemoryBlock(%d, %d) memory overwritten, no file info available", m_iRegSize);

	_OutputDebugString(str);

	enf_assert (p->m_iDummy == info);
}

//-----------------------------------------------------------------------------------
// Name : DebugInfo
// Desc : fill to p the debug info
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::MemoryBlock::DumpMemory(BlockHead *pblock)
{
	if (!pblock)
		return;

	size_t cnt = 0, unalloc = 0;
	size_t unknown = 0;
	char str[512];
	
	AllocationSet chunks;

	//test all available blocks, should be just one in correct case
	while (pblock)
	{
		if (pblock->m_iFullItems)
		{
 			unalloc += pblock->m_iFullItems;

			ubyte *pstart = (ubyte*) pblock + sizeof(BlockHead);

			//test all inside, debug flag must be ENF_DEBUG_FREE
			for (uint i = 0; i < m_iBlockItems; i++)
			{
				//test if the memory was correctly released
				DebugMemory* deb = (DebugMemory*) (pstart + m_iRegSize);
				if (deb->m_iDummy != ENF_DEBUG_FREE)
				{
					if(deb->m_pFileName)
						chunks.Insert(deb->m_pFileName, deb->m_iLine, m_iRegSize, pstart);
					else
					{
						unknown += deb->m_iDummy;
					}
				}
				pstart += m_iItemSize;
			}
		}

		pblock = pblock->m_pNextBlock;
	}

	if (unalloc)
	{
		sprintf(str, "Block size %d, %d item/s were not deallocated : \n", m_iRegSize, unalloc);
		_OutputDebugString(str);

		//write all
		for (uint i = 0; i < chunks.GetCardinality(); i++)
		{
			sprintf (str, "  %s(%d) %8d %8d\n", chunks[i].File, chunks[i].Line, chunks[i].Count, chunks[i].Size);
			_OutputDebugString(str);
		}

		if(unknown)
		{
			sprintf (str, "  unknown source, %d KB \n", unknown);
			_OutputDebugString(str);
		}
	}
}
#endif
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::Dump(const char* title) const
{
#ifdef ENF_LEAK_DETECTOR
	if(title)
		_OutputDebugString(title);

	CMutexLock lock(m_Mutex);

	for (int i = 1; i < MAX_BLOCKS; i++)
	{
		if (m_pBlocks[i])
		{
			m_pBlocks[i]->DumpMemory(m_pBlocks[i]->m_pFirstBlock);
		}
	}
#endif
}

//-----------------------------------------------------------------------------------
// constructor, just clears the memory
//-----------------------------------------------------------------------------------
ISmallBlockAllocator::ISmallBlockAllocator():
	m_iAllocated(NULL),
	m_iTotalAllocated(NULL),
	m_iNumAllocations(NULL)
{ 
	memset(&m_pBlocks, 0, sizeof(MemoryBlock*) * MAX_BLOCKS); 
}


//-----------------------------------------------------------------------------------
// destructor, free
//-----------------------------------------------------------------------------------
ISmallBlockAllocator::~ISmallBlockAllocator() 
{ 
	FreeAll();
}

//-----------------------------------------------------------------------------------
// Name : Register
// Desc : register the size for memory allocator, returns true if successfully 
// registered, false if the size has been already registered
//-----------------------------------------------------------------------------------
bool ISmallBlockAllocator::Register(size_t size)
{
	CMutexLock lock(m_Mutex);

	size = FixSize(size);
	uint block = size / QUANTIZE;

	enf_assert (size > 0 && (size / QUANTIZE) < MAX_BLOCKS);

	if (size < 1 || block >= MAX_BLOCKS || m_pBlocks[block])
		return false;

	m_pBlocks[block] = ENF_NEW MemoryBlock(this, size);

	return true;
}


//-----------------------------------------------------------------------------------
// Name : UnRegister
// Desc : unregister the size for memory allocator, returns true if successfully 
// unregistered, false if the size has not been registered
//-----------------------------------------------------------------------------------
bool ISmallBlockAllocator::UnRegister(size_t size)
{
	CMutexLock lock(m_Mutex);

	size = FixSize(size);
	uint block = size / QUANTIZE;

	enf_assert (size > 0 && block < MAX_BLOCKS);

	if (size < 1 || block >= MAX_BLOCKS || !m_pBlocks[block])
		return false;

	SAFE_DELETE(m_pBlocks[block]);
	return true;
}

//-----------------------------------------------------------------------------------
// Name : Alloc(size)
// Desc : alloc memory of requested size
//-----------------------------------------------------------------------------------
void *ISmallBlockAllocator::Alloc(size_t size, const char* file, int line)
{
	CMutexLock lock(m_Mutex);
	
	size = FixSize(size);
	uint block = size / QUANTIZE;

	enf_assert2(size > 0 && block < MAX_BLOCKS, "SmallBlockAllocator::Alloc size must be 1-512 bytes");

	m_iAllocated += size;
	m_iNumAllocations++;

	if (!m_pBlocks[block])
		Register(size);

	return m_pBlocks[block]->Alloc(file, line);
}

//-----------------------------------------------------------------------------------
void *ISmallBlockAllocator::Alloc(size_t size)
{
	CMutexLock lock(m_Mutex);

	size = FixSize(size);
	uint block = size / QUANTIZE;

	enf_assert2(size > 0 && block < MAX_BLOCKS, "SmallBlockAllocator::Alloc size must be 1-512 bytes");

	m_iAllocated += size;
	m_iNumAllocations++;

	if (!m_pBlocks[block])
		Register(size);

	return m_pBlocks[block]->Alloc();
}

//-----------------------------------------------------------------------------------
// Name : Release(pointer, size)
// Desc : release memory, return true if successfully
//-----------------------------------------------------------------------------------
bool ISmallBlockAllocator::Free(void *pointer)
{
	CMutexLock lock(m_Mutex);
	int index = m_pSortedBlocks.FindGE((const MemoryBlock*)pointer);
	if(index == -1)
		return false;

	BlockHead* head = (BlockHead*)m_pSortedBlocks[index];

	//just to be sure, that we are from this block
	enf_assert2((ubyte*)pointer >= (ubyte*)(head + 1) &&  (ubyte*)pointer < ((ubyte*)(head + 1) + head->m_pMemoryBlock->m_iBlockSize),
		"Trying to unallocate memory not from this heap");

	m_iAllocated -= head->m_pMemoryBlock->m_iRegSize;
	m_iNumAllocations--;

#ifdef ENF_LEAK_DETECTOR
	DebugMemory* deb = (DebugMemory*) ((ubyte*) pointer + head->m_pMemoryBlock->m_iRegSize);
	head->m_pMemoryBlock->TestDebugInfo(deb, ENF_DEBUG_FULL);
	deb->m_iDummy = ENF_DEBUG_FREE;
#endif

#ifdef _DEBUG
	//fill freed memory by 0xfeeefeee as MSVC does
	FillMemory32(pointer, (head->m_pMemoryBlock->m_iRegSize / 4), 0xfeeefeee);
#endif

	//retype and link with free items
	((MemItem*) pointer)->m_pNext	= head->m_pFirstFree;
	head->m_pFirstFree = (MemItem*) pointer;

	head->m_iFullItems--;

	//the pointer was removed more then once
	enf_assert(head->m_iFullItems >= 0);

	//if it's empty, but it's not the last one, release it
	if (head->m_iFullItems == 0 && (head != head->m_pMemoryBlock->m_pFirstBlock || head->m_pNextBlock != NULL))
		head->m_pMemoryBlock->ReleaseBlock(head);

	return true;
}

//-----------------------------------------------------------------------------------
// Name : FreeAll()
// Desc : release whole memory allocated by alocator, return true if successfully
//-----------------------------------------------------------------------------------
void ISmallBlockAllocator::FreeAll()
{
	CMutexLock lock(m_Mutex);

	for (int i = 1; i < MAX_BLOCKS; i++)
	{
		if (m_pBlocks[i])
		{
			SAFE_DELETE(m_pBlocks[i]);
		}
	}
	m_pSortedBlocks.Clear();
}
