
#ifndef PRECOMP_H
#define PRECOMP_H

#undef WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <es\common\win.h>

/*//DirectX
#include "d3d9.h"
#include <D3dx9.h>
#include <D3dx9effect.h>
#include <d3dx9tex.h>
*/

//System
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <commctrl.h>
#include <richedit.h>

//wxWidgets
#include "wx/wx.h"
#include "wx/msw/private.h"
#include "wx/splitter.h"
#include "wx/aui/aui.h"
#include "wx/tglbtn.h"
#include "wx/spinctrl.h"
#include "wx/clrpicker.h"
#include "wx/listctrl.h"
#include "wx/treectrl.h"
#include "wx/colordlg.h"
#include "wx/grid.h"
#include "wx/laywin.h"
#include "wx/dnd.h"
#include "wx/arrimpl.cpp"
#include "wx/splash.h"
#include "wx/textfile.h"

//enforce standard types/templates
#include "common/enf_memory.h"
#include "common/enf_types.h"
#include "common/enf_eset.h"
#include "common/enf_hashmap.h"
#include "common/enf_strings.h"
#include "common/enf_mathlib.h"
#include "common/enf_staticbuffer.h"

#endif