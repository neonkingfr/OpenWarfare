
#include "precomp.h"
#include "PropertiesPanel.h"
#include "base/MainFrame.h"
#include "base/GenericEditor.h"
#include "base/ParamConfig.h"
#include "base/EditActions.h"
#include "base/ParamFileTree.h"
#include "base/ImageList.h"
#include "base/AttrDescriptions.h"
#include "Common/wxNativeConvert.h"
#include "Common/NativeUtil.h"
#include "Common/NativeConvert.h"
#include "Common/WinApiUtil.h"
#include "wxExtension/Validators.h"
#include "tinyxml/tinyxml.h"
#include <limits>

//----------------------------------------------------------------------------------
int GetClassOrArrayMemberNameFromUser(PCNode* node, wxString& input)
{
	wxTextEntryDialog dialog(NULL, _T(""), wxString("Enter new param name"), input, wxOK | wxCANCEL);
	dialog.CentreOnParent();

	if(dialog.ShowModal() != wxID_OK)
		return 0;

	input = dialog.GetValue();

	if(input.IsEmpty())
	{
		wxMessageDialog dialog( g_MainFrame, "empty string is not valid param name!", "Bad input", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return -1;
	}

	if(input.Find(' ') > -1)	//space character are not valid
	{
		wxMessageDialog dialog( g_MainFrame, "Space characters are not allowed!", "Bad input", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return -1;
	}

	if(IsNumber(input[0]))
	{
		wxMessageDialog dialog( g_MainFrame, "first character must be not numeric!", "Bad input", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return -1;
	}

	if(node->ToClass())
	{
		if(node->ToClass()->GetMember(input.c_str()))
		{
			wxString msg = input + " already exist!";
			wxMessageDialog dialog( g_MainFrame, msg, "Bad input", wxICON_INFORMATION | wxOK);
			dialog.CentreOnParent();
			dialog.ShowModal();
			return -1;
		}
	}
	else if(node->ToArray())
	{
		if(node->ToArray()->GetMember(input.c_str()))
		{
			wxString msg = input + " already exist!";
			wxMessageDialog dialog( g_MainFrame, msg, "Bad input", wxICON_INFORMATION | wxOK);
			dialog.CentreOnParent();
			dialog.ShowModal();
			return -1;
		}
	}
	return 1;
}

//----------------------------------------------------------------------------------
PPPropertyGrid::PPPropertyGrid(wxWindow* parent, PropertiesPanel* PropPanel, wxWindowID id, const wxPoint& pos, const wxSize& size,  long style, const wxChar* name)
	:WBPropertyGrid(parent, id, pos, size, style, name)
{
	this->PropPanel = PropPanel;
}

//----------------------------------------------------------------------------------
void PPPropertyGrid::OnRMBUp(wxMouseEvent& event)
{
	PropPanel->OnPropGridRMBUp(event);
}

//----------------------------------------------------------------------------------
void PPPropertyGrid::OnLMBUp(wxMouseEvent& event)
{
	PropPanel->OnPropGridLMBUp(event);
}

//----------------------------------------------------------------------------------
void PPPropertyGrid::OnKeyDown(wxKeyEvent& event)
{
	PropPanel->OnPropGridKeyDown(event);
	event.Skip();
}

//----------------------------------------------------------------------------------
DescriptionPanel::DescriptionPanel(wxWindow *parent, PropertiesPanel *PropPanel, const char* AttrFilelName)
:wxPanel(parent, -1)
{
	this->PropPanel = PropPanel;

	AttrDesc = new AttrDescriptions(AttrFilelName);
	
	Description = new wxStaticText(this, wxID_ANY, "", wxDefaultPosition, wxSize(50, 100), wxST_NO_AUTORESIZE | wxLEFT | wxRIGHT | wxGROW);
	ClearDescription();

	Sizer = new wxBoxSizer(wxVERTICAL);
	Sizer->Add(Description, 1, wxEXPAND | wxALL | wxGROW, 10);
	SetSizer(Sizer);
}
//----------------------------------------------------------------------------------
DescriptionPanel::~DescriptionPanel()
{
	SAFE_DELETE(AttrDesc);
}
//----------------------------------------------------------------------------------
void DescriptionPanel::SetDescription(const wxString &name, const wxString &className)
{
	wxString DescText = AttrDesc->GetDescriptionText(name, className);
	Description->SetLabel(DescText);
}

//----------------------------------------------------------------------------------
void DescriptionPanel::ClearDescription()
{
	Description->SetLabel("");
}

//----------------------------------------------------------------------------------

BEGIN_EVENT_TABLE(PPPropertyGrid, WBPropertyGrid)
	EVT_RIGHT_UP(PPPropertyGrid::OnRMBUp)
	EVT_LEFT_UP(PPPropertyGrid::OnLMBUp)
	EVT_KEY_DOWN(PPPropertyGrid::OnKeyDown)
END_EVENT_TABLE()


BEGIN_EVENT_TABLE(PropertiesPanel, wxPanel)
	EVT_TOOL(-1, PropertiesPanel::OnToolClick)
	EVT_CHECKBOX(-1, PropertiesPanel::OnCheckBoxClick)
	EVT_WORKBENCH_PROPGRID(-1, OnPropertyChanged)
END_EVENT_TABLE()

//----------------------------------------------------------------------------------
PropertiesPanel::PropertiesPanel(wxWindow* parent)
	:wxPanel(parent, -1)
{
	PropSetColor = wxColour(255, 210, 210);//GetBackgroundColour();
	VariableBitmap = g_ImageList->GetBitmap(BI_PARAM_VARIABLE);
	ExternVariableBitmap = g_ImageList->GetBitmap(BI_PARAM_VARIABLE_READ_ONLY);
	ArrayBitmap = g_ImageList->GetBitmap(BI_PARAM_ARRAY);
	ExternArrayBitmap = g_ImageList->GetBitmap(BI_PARAM_ARRAY_READ_ONLY);
	
	PropGrid = new PPPropertyGrid(this, this, PROPGRID, wxDefaultPosition, wxDefaultSize, /*wxPG_AUTO_SORT |*/ wxPG_SPLITTER_AUTO_CENTER | wxPG_DEFAULT_STYLE | wxNO_BORDER);
	PropGrid->SetExtraStyle( wxPG_EX_HELP_AS_TOOLTIPS | /*wxPG_EX_GREY_LABEL_WHEN_DISABLED|*/ wxPG_EX_NATIVE_DOUBLE_BUFFERING );

	char AppDir[256];
	taGetAplicationDir(AppDir);
	wxString DescFile = AppDir;
	DescFile += "/AttrDescriptions.xml";
	DescPanel = new DescriptionPanel(this, this, DescFile);

//	toolbar = new wxToolBar(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 24), wxTB_FLAT | wxTB_TEXT | wxTB_NOICONS);
	OverloadModeCheck = new wxCheckBox(this, CHECKBOX_OVERLOAD_MODE, "Top base overload mode", wxDefaultPosition, wxSize(-1, 20), 0);
//	toolbar->AddControl(OverloadModeCheck);
//	toolbar->Realize();

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
//	sizer->Add(toolbar, 0, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
	sizer->Add(OverloadModeCheck, 0, wxGROW | wxALIGN_LEFT | wxLEFT, 15);
	sizer->Add(PropGrid, 1, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
	sizer->Add(DescPanel, 0, wxGROW | wxALIGN_LEFT | wxLEFT, 5);
	SetSizer(sizer);
}
//--------------------------------------------------------------------------------------
void PropertiesPanel::Clear()
{
	PropGrid->Clear();
}

//--------------------------------------------------------------------------------------
bool PropertiesPanel::InOverloadMode()
{
	return OverloadModeCheck->IsChecked();
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::SetOverloadMode(bool OverloadMode)
{
	if(OverloadMode != InOverloadMode())
		OverloadModeCheck->SetValue(OverloadMode);
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::CanCreateActions(bool& InsertProperty, bool& DeleteProperty, bool& OverloadProperty)
{
	InsertProperty = false;
	DeleteProperty = false;
	OverloadProperty = false;

	bool OverloadMode = InOverloadMode();

	if(OverloadMode)	//in overload mode cannot create insert and delete actions
		return;

	ParamConfig* PConfig = g_editor->GetParamConfig();
	wxPGId SelProp = PropGrid->GetSelection();
	PCNode* ParentNode = NULL;
	PCNode* SelectedNode = NULL;

	if(!SelProp.IsOk() || PropGrid->IsPropertyCategory(SelProp))
	{
		ParentNode = g_editor->GetMainSelection();
	}
	else
	{
		const wxString& SelPropName = PropGrid->GetPropertyName(SelProp);
		SelectedNode = PConfig->FindNode(SelPropName.c_str());

		if(SelectedNode)
			ParentNode = SelectedNode->GetOwner();
		else
		{
			wxPGId ParentProp = PropGrid->GetPropertyParent(SelProp);

			if(ParentProp.IsOk())
			{
				const wxString& ParentPropName = PropGrid->GetPropertyName(ParentProp);
				ParentNode = PConfig->FindNode(ParentPropName.c_str());
			}
		}
	}

	if(ParentNode == NULL)
		return;

	PCClass* cl = ParentNode->ToClass();
	PCArray* arr = ParentNode->ToArray();
	PCVar* var = ParentNode->ToVariable();

	if(cl && !cl->IsDefined())
		return;

	if(SelectedNode && SelectedNode->IsExtern() && ParentNode->ToClass() && !ParentNode->IsExtern() && SelProp.IsOk() && !PropGrid->IsPropertyCategory(SelProp))
		OverloadProperty = true;

	if(SelectedNode && (SelectedNode->IsReadOnly() && !SelectedNode->IsExtern()))
		return;

	if(ParentNode->IsReadOnly())
		return;

	if(cl || arr || (SelectedNode == PConfig))
		InsertProperty = true;

	if(SelectedNode && !SelectedNode->IsReadOnly())
		DeleteProperty = true;
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::OnPropGridLMBUp(wxMouseEvent& event)
{
	UpdateItemDescription();
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::OnPropGridRMBUp(wxMouseEvent& event)
{
	bool CanDeleteProperty, CanInsertProperty, CanOverloadProperty;
	CanCreateActions(CanInsertProperty, CanDeleteProperty, CanOverloadProperty);

	wxMenu PropertyMenu;

	if(CanInsertProperty)
	{
		PropertyMenu.Append(MENU_INSERT_VARIABLE, _T("Insert variable"), _T("Insert variable"));
		PropertyMenu.Append(MENU_INSERT_ARRAY, _T("Insert array"), _T("Insert array"));
	}

	if(CanDeleteProperty)
		PropertyMenu.Append(MENU_DELETE_PARAM, _T("Delete"), _T("delete"));

	if(CanOverloadProperty)
		PropertyMenu.Append(MENU_OVERLOAD_PARAM, _T("Overload"), _T("Overload"));

	PropGrid->PopupMenu(&PropertyMenu, event.GetPosition());
}

//----------------------------------------------------------------------------------
void PropertiesPanel::OnPropGridKeyDown(wxKeyEvent& event)
{
	int KeyCode = event.GetKeyCode();

	switch(KeyCode)
	{
		case WXK_DELETE:
		{
			if(!event.ShiftDown())
			{
				bool CanDeleteProperty, CanInsertProperty, CanOverloadProperty;
				CanCreateActions(CanInsertProperty, CanDeleteProperty, CanOverloadProperty);

				if(CanDeleteProperty)
					CreateActionFromMenu(MENU_DELETE_PARAM);
			}
			break;
		}
		case WXK_INSERT:
		{
			if(!event.ShiftDown())
			{
				bool CanDeleteProperty, CanInsertProperty, CanOverloadProperty;
				CanCreateActions(CanInsertProperty, CanDeleteProperty, CanOverloadProperty);

				if(CanInsertProperty)
					CreateActionFromMenu(MENU_INSERT_VARIABLE);
				break;
			}
		}
	}
	UpdateItemDescription();
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::OnCheckBoxClick(wxCommandEvent& event)
{
	switch(event.GetId())
	{
		case CHECKBOX_OVERLOAD_MODE:
		{
			EArray<bool> CategoryExpand;
			wxPGId id = PropGrid->GetFirstCategory();
			while ( id.IsOk() )
			{
				CategoryExpand.Insert(PropGrid->IsPropertyExpanded(id));
				id = PropGrid->GetNextCategory(id);
			}

			ShowSelectionProperties();
			PropGrid->Refresh();

			id = PropGrid->GetFirstCategory();
			uint order = 0;
			while ( id.IsOk() )
			{
				bool IsExpanded = PropGrid->IsPropertyExpanded(id);
				if(CategoryExpand[order++] != IsExpanded)
				{
					if(IsExpanded)
						PropGrid->Collapse(id);
					else
						PropGrid->Expand(id);
				}

				id = PropGrid->GetNextCategory(id);
			}
			break;
		}
	}
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::OnToolClick(wxCommandEvent& event)
{
	switch(event.GetId())
	{
		case MENU_INSERT_VARIABLE:
		case MENU_INSERT_ARRAY:
		case MENU_DELETE_PARAM:
		case MENU_OVERLOAD_PARAM:
			CreateActionFromMenu(event.GetId());
			break;
	}

//	event.Skip();
} 

//--------------------------------------------------------------------------------------
void PropertiesPanel::OnPropertyChanged(wxCommandEvent& event)
{
	if(event.GetId() == PROPGRID)
	{
		WBPropGridEventData* EventData = (WBPropGridEventData*)event.GetClientObject();
		enf_assert(EventData);
		enf_assert(EventData->GetValue());

		wxPGProperty* prop = EventData->GetProperty();
		enf_assert(prop);

		wxPGId PropCategory = PropGrid->GetPropertyCategory(prop->GetId());
		enf_assert(PropCategory.IsOk());
		wxString PropCategoryLabel = PropGrid->GetPropertyLabel(PropCategory);

		const wxString& ParamId = PropGrid->GetPropertyName(prop->GetId());
		wxString ParamName = ParamId.AfterLast('/');
//		const wxString& ParamName = PropGrid->GetPropertyLabel(prop->GetId());
		wxString ParamValue = EventData->GetValueAsString();

		ParamConfig* PConfig = g_editor->GetParamConfig();
		PCClass* MainSel = g_editor->GetMainSelection();

		PCNode* ParamNode = PConfig->FindNode(ParamId.c_str());	//find node if edited param. can be NULL (not existing)

		wxString ParamOwnerId = ParamId;
		ParamOwnerId = ParamOwnerId.BeforeLast('/');
		PCNode* OwnerNode = PConfig->FindNode(ParamOwnerId.c_str());
		enf_assert(OwnerNode);
		PCClass* OwnerClass = OwnerNode->ToClass();
		PCArray* OwnerArray = OwnerNode->ToArray();

		EArray<PCClass*> ShownClasses;
		GetClassesToView(ShownClasses);	//classes currently shown in GUI

		//if owner of property/param is array or changed property is not from last category (represent common params from selected classes), edited will be only single property instance from single class
		bool SinglePropertyOnly = (OwnerArray || (strcmpi(PropCategoryLabel.c_str(), MainSel->GetName()) != 0));

		EArray<PCNode*> ParamOwners;

		if(SinglePropertyOnly)
			ParamOwners.Insert(OwnerNode);
		else
			GetParamOwnersFromSelection(ParamOwners, ParamName.c_str());	//edit property is targeted to multiple classes

		enf_assert(ParamOwners.GetCardinality() > 0);
//		PCNode* node = (PCNode*)PropGrid->GetUserDataFromProperty(prop);
//		enf_assert(node);

		HistoryPoint* point = g_editor->CreateHistoryPoint("Modify Variable");
		uint NumActions = 0;
		wxString UndoPropSel;

		for(uint n = 0; n < ParamOwners.GetCardinality(); n++)
		{
			PCNode* ParamOwner = ParamOwners[n];
			PCClass* ParamOwnerCl = ParamOwner->ToClass();
			PCArray* ParamOwnerArr = ParamOwner->ToArray();

			if(ParamOwner->IsReadOnly())
				continue;

			EditAction* RedoAction = NULL;
			EditAction* UndoAction = NULL;

			DString CurParamID = ParamOwner->GetId();
			CurParamID += '/';
			CurParamID += ParamName;

			PCNode* CurParamNode = PConfig->FindNode(CurParamID.c_str());
			PCVar* CurVariable = CurParamNode ? CurParamNode->ToVariable() : NULL;
			enf_assert(CurParamNode == CurVariable);

			if(CurVariable)	//variable just exist, changed will be only value
			{
				if(CurVariable->IsDefinedAsReadOnly())
					continue;

				if(CurVariable->IsExtern())
				{
					point->AppendRedoAction(ENF_NEW EASetNodeExtern(CurParamID.c_str(), false));
					point->AppendUndoAction(ENF_NEW EASetNodeExtern(CurParamID.c_str(), true));
				}

				RedoAction = ENF_NEW EAModifyVariable(CurParamID.c_str(), ParamValue.c_str());
				UndoAction = ENF_NEW EAModifyVariable(CurParamID.c_str(), CurVariable->GetValue());
			}
			else	//variable just not exist, therefore must be created now
			{
				uint PosInOwner;

				if(ParamOwnerCl)
					PosInOwner = ParamOwnerCl->GetNodesCount();
				else
				{
					enf_assert(ParamOwnerArr);
					PosInOwner = OwnerArray->GetCardinality();
				}

				RedoAction = ENF_NEW EACreateVariable(CurParamID.c_str(), ParamName.c_str(), ParamValue.c_str(), PosInOwner);
				UndoAction = ENF_NEW EADeleteNode(CurParamID.c_str());
			}

			if(RedoAction && UndoAction)
			{
				point->AppendRedoAction(RedoAction);
				point->AppendUndoAction(UndoAction);
				NumActions++;
			}
		}

		if(ParamNode)	//if param node was existing before, node selection for undo will be his ID
		{
			UndoPropSel = ParamId;
		}
		else	//in the opposite direction need search node selection fro undo
		{
			if(OwnerClass)
			{
				wxPGId SelectedAfter = FindPropSelectionAfterDelete(prop->GetId());

				if(SelectedAfter.IsOk())
					UndoPropSel = PropGrid->GetPropertyName(SelectedAfter);
				else
					UndoPropSel = ParamOwnerId.c_str();
			}
			else
			{
				UndoPropSel = ParamOwnerId.c_str();	//for undo cannot select "empty slot" property because of different (unique ID) of this property after undo
				PropGrid->SetPropertyLabel(prop->GetId(), "");	//rename "empty slot" to ""
			}
		}

		if(NumActions > 0)
		{
			EArray<DString> ClassSel;
			g_editor->GetSelection(ClassSel);
			point->SetRedoClassSel(ClassSel);
			point->SetUndoClassSel(ClassSel);

			point->SetRedoPropSel(ParamId.c_str());
			point->SetUndoPropSel(UndoPropSel.c_str());

			g_editor->AddHistoryPoint(point);

			ParamFileTree* tree = g_MainFrame->GetParamTree();
			tree->Freeze();

			g_editor->RealizeActions(point->GetRedoActions());
			g_editor->UpdateUndoRedoAccessibility();
			NumActions++;

			tree->Thaw();
		}
		else
		{
			SAFE_DELETE(point);
		}

		bool OverloadMode = InOverloadMode();

		if(OverloadMode)
		{
			PropGrid->SetPropertyColour(prop->GetId(), PropSetColor);

			//refresh inherited values in classes
			wxPGId CatProp = PropGrid->GetNextCategory(PropCategory);
			while ( CatProp.IsOk() )
			{
				for(wxPGId pr = PropGrid->GetFirstChild(CatProp); pr.IsOk(); pr = PropGrid->GetNextProperty(pr))
				{
					if(PropGrid->GetChildrenCount(pr) > 0)	//ignore arrays
						continue;

					if((PropGrid->GetUserDataFromProperty(pr)) != NULL)	//contain inherited value
					{
						if(PropGrid->GetPropertyLabel(pr) == ParamName)
						{
							PropGrid->SetPropertyValue(pr, PropGrid->GetPropertyValueAsString(prop->GetId()));
							break;
						}
					}
				}

				CatProp = PropGrid->GetNextCategory(CatProp);
			}
		}
	}

	PropGrid->Refresh(false);
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::UpdateItemDescription()
{
	wxPGId SelItem = PropGrid->GetSelection();

	if(!SelItem.IsOk())
		DescPanel->ClearDescription();
	else
	{
		wxString Name = PropGrid->GetPropertyLabel(SelItem);
		wxString Class = "";
		if(SelItem.GetProperty().GetParent())
			Class = SelItem.GetProperty().GetParent()->GetLabel();
		DescPanel->SetDescription(Name, Class);
	}
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::GetClassesToView(EArray<PCClass*>& classes)
{
	EArray<PCClass*>& selection =	g_editor->GetSelection();
	PCClass* MainSel = g_editor->GetMainSelection();
	PCClass* MainSelBase = MainSel->GetBase();
	classes.Insert(MainSel);

	while(MainSelBase)
	{
		if(!MainSelBase->IsHidden())	//do not show hidden classes
		{
			bool InheritedByAll = true;

			for(uint n = 0; n < selection.GetCardinality(); n++)
			{
				PCClass* sel = selection[n];
				
				if(!sel->IsInheritedFrom(MainSelBase))
				{
					InheritedByAll = false;
					break;
				}
			}

			if(InheritedByAll)
				classes.Insert(MainSelBase);
		}
	
		MainSelBase = MainSelBase->GetBase();
	}
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::GetParamOwnersFromSelection(EArray<PCNode*>& ParamOwners, const char* param)
{
	enf_assert(param);
	PCClass* MainSel = g_editor->GetMainSelection();
//	PCClass* MainSelTopBase = MainSel->GetTopBase();
	EArray<PCClass*>& selection =	g_editor->GetSelection();

	for(uint n = 0; n < selection.GetCardinality(); n++)
	{
		PCClass* cl = selection[n];

		ParamOwners.Insert(cl);
		continue;

		if(cl == MainSel)
		{
			ParamOwners.Insert(cl);
			continue;
		}

		PCNode* ValueNode = cl->GetValueNode(param);	//find node in class members, if not found, find continues to base class. recursive

		if(ValueNode)	//if value found in class or any of base classes
		{
			if(ValueNode->ToVariable())	//cannot edit array but only variables from array
				ParamOwners.Insert(cl);
		}
	}
}

//--------------------------------------------------------------------------------------
void CreateSetNodeExternActions(HistoryPoint* point, PCNode* node)
{
	enf_assert(point);
	enf_assert(node);
	enf_assert(!node->ToClass());	//only variiables and arrays

	DString ID = node->GetId();
	point->AppendRedoAction(ENF_NEW EASetNodeExtern(ID.c_str(), false));
	point->AppendUndoAction(ENF_NEW EASetNodeExtern(ID.c_str(), true));

	PCArray* arr = node->ToArray();

	if(arr)
	{
		for(uint n = 0; n < arr->GetCardinality(); n++)
		{
			CreateSetNodeExternActions(point, arr->GetItem(n));
		}
	}
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::ShowSelectionProperties()
{
//	PropGrid->Freeze();
/*	if(PropGrid->GetEditorControl())
	{
		wxPGId sel = PropGrid->GetSelection();
		wxPGProperty* prop = PropGrid->GetPropertyPtr(sel);
//		PropGrid->ClearSelection();
		prop->Hide(true);
//		prop->RecreateEditor();
//		PropGrid->EditorsValueWasModified();
	}*/
/*
	wxPGId SelProp = PropGrid->GetSelection();

	if(SelProp.IsOk())
	{
		wxPGId category = PropGrid->GetPropertyCategory(SelProp);

		if(category.IsOk())
			PropGrid->SelectProperty(category);
	}*/

//	PropGrid->ClearSelection();
	PropGrid->Clear();

	EArray<PCClass*>& selection =	g_editor->GetSelection();

	if(selection.GetCardinality() == 0)
	{
		g_editor->OnShowSelectionProperies();
		return;
	}

	bool OverloadMode = InOverloadMode();

	//create list of common base classes from selected classes
	EArray<PCClass*> classes;
	GetClassesToView(classes);

	uint NumClasses = classes.GetCardinality();
	PCClass* TopBase = (NumClasses > 0) ? classes[NumClasses - 1] : NULL;
	int UniCategoryIndex = 0;

	if(!OverloadMode)	//normal mode. in this mode need be shown parms only from class members
	{
		for(int n = (int)classes.GetCardinality() - 1; n >= 0 ; n--)
		{
			PCClass* cl = classes[n];

			wxPGId ExistingCat = PropGrid->FindCategory(cl->GetName());
			wxString CatName = cl->GetName();

			if(ExistingCat.IsOk())
				CatName = CatName += " (" + ItoA(++UniCategoryIndex) + ")";

			wxPGProperty* catprop = PropGrid->AppendCategory( CatName.c_str() );
			PropGrid->SetPropertyClientData (catprop->GetId(), cl);	//mark category property. needed for finding using FindCategory()

			if(!cl->IsExtern())
				PropGrid->SetCaptionTextColour(catprop->GetId(), *wxBLACK);

//			PropGrid->SetPropertyTextColour(catprop->GetId(), ClassLabelTextColor);

			if(n > 0)
				PropGrid->Collapse(catprop);

			for(uint m = 0; m < cl->GetNodesCount(); m++)
			{
				PCNode* node = cl->GetNode(m);
				PCVar* var = node->ToVariable();
				PCArray* arr = node->ToArray();
//				const PCDefParam* def = arr->GetDefinition();

				const char* PropLabel = node->GetName();
				DString PropName = node->GetId();

				if(var)
				{
					wxPGProperty* prop = CreatePropertyForVariable(var->GetOwner(), var, var->GetName(), PropName.c_str(), var->GetValue(), wxPGId(), -1);
//					wxPGProperty* prop = PropGrid->AppendEditBoxProperty(catprop->GetId(), PropName.c_str(), PropLabel, var->GetValue(), wxDefaultValidator);
//					PropGrid->SetUserDataToProperty(prop, (void*)var);	//store class owner pointer to GUI property
				}
				else if(arr)
				{
					if(!arr->IsHidden())
						ShowArrayProperties(arr, catprop, PropLabel, PropName.c_str(), true, arr->IsReadOnly());
				}

				
			}
		}
	}
	else	//OverloadMode. in this mode is needed show "all possible parms to edit" this means all parameters from top base class
	{
		for(int n = (int)classes.GetCardinality() - 1; n >= 0 ; n--)
		{
			PCClass* cl = classes[n];
		
			bool ReadOnlyClass = cl->IsReadOnly();
			wxPGId ExistingCat = PropGrid->FindCategory(cl->GetName());
			wxString CatName = cl->GetName();

			if(ExistingCat.IsOk())
				CatName = CatName += " (" + ItoA(++UniCategoryIndex) + ")";

			wxPGProperty* catprop = PropGrid->AppendCategory( CatName.c_str() );
			PropGrid->SetPropertyClientData (catprop->GetId(), cl);	//needed for next find using FindCategory()

//			wxPGProperty* catprop = PropGrid->AppendCategory( cl->GetName() );

			if(!cl->IsExtern())
				PropGrid->SetCaptionTextColour(catprop->GetId(), *wxBLACK);

			PropGrid->SetPropertyImage(catprop->GetId(), ExternArrayBitmap);
//			else
//				PropGrid->SetCaptionTextColour(catprop->GetId(), *wxLIGHT_GREY);

			if(n > 0)
				PropGrid->Collapse(catprop);

			for(uint m = 0; m < TopBase->GetNodesCount(); m++)
			{
				PCNode* BaseNode = TopBase->GetNode(m);
				PCNode* ClassNode = cl->GetMember(BaseNode->GetName());					//node from class
				PCNode* ValueNode = cl->GetValueNode(BaseNode->GetName(), TopBase);	//node from class or from any of base classes
				enf_assert(ValueNode);
				bool StoredInClass = (ClassNode != NULL);	//true found in actual class

				PCVar* var = ValueNode->ToVariable();
				PCArray* arr = ValueNode->ToArray();

				const char* PropLabel = BaseNode->GetName();
				DString PropName;

//				if(strcmpi(PropLabel, "type") == 0)
//					int gg = 0;

				if(StoredInClass)
					PropName = ClassNode->GetId();	//if is from actual class then have unique ID
				else
				{
					PropName = cl->GetId();	//generate id for node
					PropName += '/';
					PropName += BaseNode->GetName();
				}

				wxPGProperty* prop = NULL;

				if(var)
				{
					PCVar* ClassVar = ClassNode ? ClassNode->ToVariable() : NULL;
					prop = CreatePropertyForVariable(cl, ClassVar, var->GetName(), PropName.c_str(), var->GetValue(), wxPGId(), -1);
//					prop = PropGrid->AppendEditBoxProperty(catprop->GetId(), PropName.c_str(), PropLabel, var->GetValue(), wxDefaultValidator);

					if(prop && !StoredInClass)
						PropGrid->SetUserDataToProperty(prop, (void*)var);	//store owner class
				}
				else if(arr)
				{
					if(!arr->IsHidden())
						prop = ShowArrayProperties(arr, catprop, PropLabel, PropName.c_str(), true, true/*arr->IsReadOnly()*/);
				}

				if(prop)
				{
					if(StoredInClass && /*(ClassNode != BaseNode) &&*/ !ClassNode->IsExtern())	//mark this param/property as overrloaded
						PropGrid->SetPropertyBackgroundColour(prop->GetId(), PropSetColor);

					if(!StoredInClass && ReadOnlyClass)
					{
						PropGrid->DisableProperty(prop->GetId());
//						PropGrid->SetPropertyTextColour(prop->GetId(), *wxLIGHT_GREY);
					}
				}
			}
		}
	}

	DescPanel->ClearDescription();
	g_editor->OnShowSelectionProperies();
}

//--------------------------------------------------------------------------------------
wxPGProperty* PropertiesPanel::ShowArrayProperties(PCArray* arr, wxPGProperty* ParentProp, const char* label, const char* name, bool ClassOwner, bool ReadOnly)
{
	wxPGProperty* prop = wxCustomProperty(label, name);
	wxPGId PropID = PropGrid->AppendIn(ParentProp->GetId(), prop );
	DString ArrID = arr->GetId();

//	if(ReadOnly)
	PropGrid->DisableProperty(PropID);	//this can be disabled always. editable will be only subproperties

	if(ClassOwner)
	{
		if(!InOverloadMode() && arr->IsExtern())
		{
//			PropGrid->SetPropertyImage(PropID, ExternArrayBitmap);
			PropGrid->SetPropertyTextColour(PropID, *wxLIGHT_GREY);
		}
//		else
//			PropGrid->SetPropertyImage(PropID, ArrayBitmap);
	}

	for(uint a = 0; a < arr->GetCardinality(); a++)
	{
		PCNode* item = arr->GetItem(a);
		PCVar* var = item->ToVariable();
		bool IsVariable = (var != NULL);
		wxString PropName;
		DString ItemName;
		DString UniID = item->GetId();
		PropName = UniID.c_str();
		ItemName = item->GetName();

		if(IsVariable)
		{
			wxPGProperty* prop = PropGrid->InsertEditBoxProperty(PropID, PropName, /*ItemName.c_str()*/"", var->GetValue(), wxDefaultValidator, -1);

			if(ReadOnly)
				PropGrid->DisableProperty(prop->GetId());
		}
		else	//is array
		{
			PCArray* Arr = item->ToArray();
			enf_assert(Arr);
			ShowArrayProperties(Arr, prop, "subarray"/*ItemName.c_str()*/, PropName.c_str(), false, ReadOnly);
		}
	}

	if(arr->GetCardinality() == 0)	//if array is empty, insert a "empty slot" item because of array property must be expand/collapseable
	{
		DString SubVarName = g_editor->GetParamConfig()->GenerateUniqueName(UID_VARIABLE);
		wxString SubVarID = ArrID.c_str();
		SubVarID += "/";
		SubVarID += SubVarName.c_str();
		wxPGProperty* SubProp = PropGrid->InsertEditBoxProperty(prop->GetId(), SubVarID, "empty slot", "", wxDefaultValidator, -1);

		if(ReadOnly)
			PropGrid->DisableProperty(SubProp->GetId());
	}

	return prop;
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::CreateActionFromMenu(int MenuID)
{
	ParamConfig* PConfig = g_editor->GetParamConfig();
	wxPGId SelProp = PropGrid->GetSelection();
	wxString SelPropName;
	PCNode* SelectedNode = NULL;
	PCNode* OwnerNode = NULL;
	uint PosInOwner = INDEX_NOT_FOUND;
	uint VirtPosInOwner = INDEX_NOT_FOUND;	//position in owner but skip class members from counting
	wxPGId ActiveCategory;

	if(SelProp.IsOk() && !PropGrid->IsPropertyCategory(SelProp))
	{
		SelPropName = PropGrid->GetPropertyName(SelProp);
		SelectedNode = PConfig->FindNode(SelPropName.c_str());
//		enf_assert(SelectedNode);
		ActiveCategory = PropGrid->GetPropertyCategory(SelProp);

		if(SelectedNode)
		{
			OwnerNode = SelectedNode->GetOwner();
			PosInOwner = SelectedNode->GetPosInOwner();
			VirtPosInOwner = SelectedNode->GetPosInOwner(true);

			if(MenuID == MENU_INSERT_VARIABLE || MenuID == MENU_INSERT_ARRAY)
			{
				PosInOwner++;
				VirtPosInOwner++;
			}
		}
		else	//if menu was shown from "empty slot" property (possible only one in array)
		{
			wxString OwnerName = SelPropName.BeforeLast('/');
			OwnerNode = PConfig->FindNode(OwnerName.c_str());
			enf_assert(OwnerNode);
			PosInOwner = 0;
			VirtPosInOwner = 0;
			PropGrid->Delete(SelProp);	//delete "empty slot" property
		}
	}
	else
	{
		OwnerNode = g_editor->GetMainSelection();
		enf_assert(OwnerNode);

		if(!OwnerNode)
			OwnerNode = PConfig;

		PosInOwner = 0;
		VirtPosInOwner = 0;
		ActiveCategory = PropGrid->FindCategory(OwnerNode/*->GetName()*/);
	}

	enf_assert(OwnerNode);

	if(!OwnerNode)
		return;

	DString OwnID = OwnerNode->GetId();
	PCClass* ClassOwner = OwnerNode->ToClass();
	PCArray* ArrayOwner = OwnerNode->ToArray();
	wxString NewPropName;

	enf_assert(ActiveCategory.IsOk());
	wxPGId NextCategory = PropGrid->GetNextCategory(ActiveCategory);
	bool MultiActions = (!NextCategory.IsOk() && !OwnerNode->ToArray());	//multiactions possible only in last property category (represent all selected classes) and if owner node is not array

	EArray<PCNode*> TargetOwners;

	if(MultiActions)
	{
		EArray<PCClass*>& selection =	g_editor->GetSelection();

		for(uint n = 0; n < selection.GetCardinality(); n++)
			TargetOwners.Insert(selection[n]);
	}
	else
		TargetOwners.Insert(OwnerNode);

	enf_assert(TargetOwners.GetCardinality() > 0);

	HistoryPoint* point = g_editor->CreateHistoryPoint("unnamed");

	switch(MenuID)
	{
		case MENU_INSERT_VARIABLE:
		{
			if(ClassOwner)
			{
				int res = -1;

				while(res == -1)
					res = GetClassOrArrayMemberNameFromUser(ClassOwner, NewPropName);

				if(res == 0)
					return;
			}
			else	//(ArrayOwner)
			{
				enf_assert(ArrayOwner);
//				UniIdPrefix prefix = /*IsVariable ? UID_VARIABLE :*/ UID_ARRAY;
				DString UniName = g_editor->GetParamConfig()->GenerateUniqueName(UID_VARIABLE);
				NewPropName = UniName.c_str();
			}

			wxString PropLabel = NewPropName;
			DString ID = OwnerNode->GetId();
			wxString PropName = ID.c_str();
			PropName += '/';
			PropName += PropLabel;

			wxPGProperty* prop;

			if(ArrayOwner)
			{
				wxPGId OwnerPropID = PropGrid->FindProperty(OwnID.c_str());
				enf_assert(OwnerPropID.IsOk());
				prop = PropGrid->InsertEditBoxProperty(OwnerPropID, PropName, /*PropLabel*/"", "", wxDefaultValidator, (int)VirtPosInOwner);
			}
			else
				prop = CreatePropertyForVariable(OwnerNode, NULL, PropLabel.c_str(), PropName.c_str(), "", ActiveCategory, (int)VirtPosInOwner);

			bool selected = PropGrid->SelectProperty(prop->GetId(), false);
			enf_assert(selected);

			for(uint n = 0; n < TargetOwners.GetCardinality(); n++)
			{
				PCNode* TargetOwnerNode = TargetOwners[n];
				PCClass* TargetOwnerCl = TargetOwnerNode->ToClass();
				PCArray* TargetOwnerArr = TargetOwnerNode->ToArray();

				if(TargetOwnerCl && TargetOwnerCl->GetMember(PropLabel.c_str()))
					continue;

				DString NewVarID = TargetOwnerNode->GetId();
				NewVarID += "/";
				NewVarID += PropLabel.c_str();

				uint PosInOwn;

				if(TargetOwnerNode == OwnerNode)
					PosInOwn = PosInOwner;
				else
					PosInOwn = TargetOwnerCl ? TargetOwnerCl->GetNodesCount() : TargetOwnerArr->GetCardinality();

				point->AppendRedoAction(ENF_NEW EACreateVariable(NewVarID.c_str(), PropLabel.c_str(), "", PosInOwn));
				point->AppendUndoAction(ENF_NEW EADeleteNode(NewVarID.c_str()));
			}

			point->SetRedoPropSel(PropName.c_str());
			point->SetUndoPropSel(SelPropName.c_str());
			break;
		}
		case MENU_INSERT_ARRAY:
		{
			if(ClassOwner)
			{
				int res = -1;

				while(res == -1)
					res = GetClassOrArrayMemberNameFromUser(ClassOwner, NewPropName);

				if(res == 0)
					return;
			}
			else	//(ArrayOwner)
			{
				enf_assert(ArrayOwner);
				DString UniName = g_editor->GetParamConfig()->GenerateUniqueName(UID_ARRAY);
				NewPropName = UniName.c_str();
			}

			wxString PropLabel = ClassOwner ? NewPropName : "subarray";
			DString ID = OwnerNode->GetId();
			wxString PropName = ID.c_str();
			PropName += '/';
			PropName += PropLabel;

			DString SubVarName = g_editor->GetParamConfig()->GenerateUniqueName(UID_VARIABLE);
			wxString SubVarID = PropName + "/" + SubVarName.c_str();

			wxPGProperty* prop = wxCustomProperty(PropLabel, PropName);
			wxPGProperty* nprop = NULL;

			if(ArrayOwner)
			{
				wxPGId OwnerID = PropGrid->FindProperty(OwnID.c_str());
				enf_assert(OwnerID.IsOk());
//				nprop = PropGrid->AppendIn(OwnerID, prop);
				nprop = PropGrid->Insert(OwnerID,(int)VirtPosInOwner, prop);
			}
			else
				nprop = PropGrid->Insert(ActiveCategory,(int)VirtPosInOwner, prop);
//				nprop = PropGrid->Append(prop);

			PropGrid->DisableProperty(nprop->GetId());

			PropGrid->InsertEditBoxProperty(nprop->GetId(), SubVarID, "empty slot", "", wxDefaultValidator, (int)VirtPosInOwner);

//			if(ClassOwner)
//				PropGrid->SetPropertyImage(nprop->GetId(), ArrayBitmap);

			bool selected = PropGrid->SelectProperty(prop->GetId(), false);
			enf_assert(selected);
			
			for(uint n = 0; n < TargetOwners.GetCardinality(); n++)
			{
				PCNode* TargetOwnerNode = TargetOwners[n];
				PCClass* TargetOwnerCl = TargetOwnerNode->ToClass();
				PCArray* TargetOwnerArr = TargetOwnerNode->ToArray();

				if(TargetOwnerCl && TargetOwnerCl->GetMember(PropLabel.c_str()))
					continue;

				DString NewVarID = TargetOwnerNode->GetId();
				NewVarID += "/";
				NewVarID += PropLabel.c_str();

				uint PosInOwn;

				if(TargetOwnerNode == OwnerNode)
					PosInOwn = PosInOwner;
				else
					PosInOwn = TargetOwnerCl ? TargetOwnerCl->GetNodesCount() : TargetOwnerArr->GetCardinality();

				point->AppendRedoAction(ENF_NEW EACreateArray(NewVarID.c_str(), GetConfigPtr()->CreateArray(PropLabel.c_str(), NULL, NULL), PosInOwn));
				point->AppendUndoAction(ENF_NEW EADeleteNode(NewVarID.c_str()));
			}

			point->SetRedoPropSel(PropName.c_str());
			point->SetUndoPropSel(SelPropName.c_str());
			break;
		}
		case MENU_DELETE_PARAM:
		{
			enf_assert(SelProp.IsOk());
			enf_assert(SelectedNode);

/*			wxPGId SelectedAfter = PropGrid->GetNextSibling(SelProp);

			if(!SelectedAfter.IsOk())
				SelectedAfter = PropGrid->GetPropertyParent(SelProp);
*/
			wxPGId SelectedAfter = FindPropSelectionAfterDelete(SelProp);

			if(!SelectedAfter.IsOk())
				NewPropName = "";
			else
				NewPropName = PropGrid->GetPropertyName(SelectedAfter);

			if(SelProp.IsOk())
				PropGrid->Delete(SelProp);

			if(SelectedAfter.IsOk())
			{
				bool selected = PropGrid->SelectProperty(SelectedAfter, false);
				enf_assert(selected);
			}

			PCVar* var = SelectedNode->ToVariable();
			PCArray* arr = SelectedNode->ToArray();
			DString SelID = SelectedNode->GetId();
			DString SelName = SelectedNode->GetName();

			for(uint n = 0; n < TargetOwners.GetCardinality(); n++)
			{
				PCNode* TargetOwnerNode = TargetOwners[n];
				PCClass* TargetOwnerCl = TargetOwnerNode->ToClass();
				PCArray* TargetOwnerArr = TargetOwnerNode->ToArray();
				PCNode* DelNode = NULL;

				if(TargetOwnerCl)
					DelNode = TargetOwnerCl->GetMember(SelName.c_str());
				else
				{
					enf_assert(TargetOwnerArr);
					enf_assert(TargetOwners.GetCardinality() == 0);	//not possible delete member from array in multiple classes
					DelNode = SelectedNode;
				}

				if(!DelNode)
					continue;

				PCVar* DelVar = DelNode->ToVariable();
				PCArray* DelArr = DelNode->ToArray();
				enf_assert(DelVar || DelArr);

				uint PosInOwn = DelNode->GetPosInOwner();
				DString DelNodeID = DelNode->GetId();

				point->AppendRedoAction(ENF_NEW EADeleteNode(DelNodeID.c_str()));

				if(DelVar)
					point->AppendUndoAction(ENF_NEW EACreateVariable(DelNodeID.c_str(), DelVar->GetName(), DelVar->GetValue(), PosInOwner));
				else
				{
					enf_assert(DelArr);
					point->AppendUndoAction(ENF_NEW EACreateArray(DelNodeID.c_str(), DelArr->Clone(NULL), PosInOwner));
				}
			}

			point->SetRedoPropSel(NewPropName.c_str());
			point->SetUndoPropSel(SelID.c_str());
			break;
		}
		case MENU_OVERLOAD_PARAM:
		{
			enf_assert(SelProp.IsOk());
			enf_assert(SelectedNode);	//if not "only parms node" only
			enf_assert(SelectedNode->IsExtern());

/*			if(SelectedNode->ToArray())
				GetPropGrid()->SetPropertyImage(SelProp, ArrayBitmap);
			else
			{
				enf_assert(SelectedNode->ToVariable());
				GetPropGrid()->SetPropertyImage(SelProp, VariableBitmap);
			}
*/
			EnableChildProperties(SelProp);

			for(uint n = 0; n < TargetOwners.GetCardinality(); n++)
			{
				PCNode* TargetOwnerNode = TargetOwners[n];
				PCClass* TargetOwnerCl = TargetOwnerNode->ToClass();
				enf_assert(TargetOwnerCl);

				PCNode* TargNode = TargetOwnerCl->GetMember(SelectedNode->GetName());

				if(!TargNode && TargNode->ToClass())
					continue;

				CreateSetNodeExternActions(point, TargNode);
			}

			point->SetRedoPropSel(SelPropName.c_str());
			point->SetUndoPropSel(SelPropName.c_str());
			break;
		}
	}

	if(point->GetRedoActions().GetCardinality() > 0)
	{
		EArray<DString> ClassSel;
		g_editor->GetSelection(ClassSel);
		point->SetRedoClassSel(ClassSel);
		point->SetUndoClassSel(ClassSel);

		g_editor->AddHistoryPoint(point);

		ParamFileTree* tree = g_MainFrame->GetParamTree();
		tree->Freeze();

		g_editor->RealizeActions(point->GetRedoActions());
//		g_MainFrame->GetParamTree()->EnsureSelectionVisible();
		g_editor->UpdateUndoRedoAccessibility();

		tree->Thaw();
	}
	else
	{
		SAFE_DELETE(point);
	}
}

//--------------------------------------------------------------------------------------
void PropertiesPanel::EnableChildProperties(wxPGId& prop)
{
	wxPGId ChildProp = GetPropGrid()->GetFirstChild(prop);

	if(!ChildProp.IsOk())
	{
		GetPropGrid()->EnableProperty(prop);
		GetPropGrid()->SetPropertyTextColour(prop, *wxBLACK);
		GetPropGrid()->SelectProperty(GetPropGrid()->GetPropertyCategory(prop), false);
		GetPropGrid()->SelectProperty(prop, false);
	}

	while(ChildProp.IsOk())
	{
		GetPropGrid()->EnableProperty(ChildProp);
		GetPropGrid()->SetPropertyTextColour(ChildProp, *wxBLACK);
		EnableChildProperties(ChildProp);
		ChildProp = GetPropGrid()->GetNextSibling(ChildProp);
	}
}

//--------------------------------------------------------------------------------------
wxPGProperty* PropertiesPanel::CreatePropertyForVariable(PCNode* VarOwner, PCVar* variable, const char* VarName, const char* id, const char* VarValue, wxPGId ParentProp, int position)
{
//	if(strcmpi(VarName, "alpha") == 0)
//		int gg = 0;
	WBPropertyGrid* PropGrid = GetPropGrid();
	PCClass* VarClassOwner = VarOwner->ToClass();
	enf_assert(VarName);

	if(variable)	//variable now exist
	{
		if(variable->IsHidden())	//if exist, hidden flag must be checked from IsHidden() function
			return NULL;

		if(VarOwner->IsHidden())
			return NULL;

//		if(!VarClassOwner->IsExtern() && variable->IsExtern())
//			return NULL;
	}
	else	//not existing variable
	{
		if(VarOwner->IsHidden())
			return NULL;

		const char* Hidden = VarClassOwner->GetPropertyDefAttr(VarName, "hidden");

		if(Hidden && (strcmpi(Hidden, "true") == 0))
			return NULL;
	}

	bool IsReadOnly = false;

	if(variable)
	{
		if(InOverloadMode())
			IsReadOnly = (variable->IsDefinedAsReadOnly() || VarOwner->IsReadOnly() || !VarClassOwner->IsDefined());
		else
		{
			IsReadOnly = variable->IsReadOnly();
		}
	}
	else
	{
		if(VarOwner->IsReadOnly())
			IsReadOnly = true;
		else if(!VarClassOwner->IsDefined())
			IsReadOnly = true;

		if(!IsReadOnly)
		{
			const char* ReadOnly = VarClassOwner->GetPropertyDefAttr(VarName, "readonly");
			IsReadOnly = ReadOnly ? (strcmpi(ReadOnly, "true") == 0) : false;
		}
	}

	PCVarType VarType = String2VarType(VarClassOwner->GetPropertyDefAttr(VarName, "type"));
	const TiXmlElement* ValuesElement = VarClassOwner->GetPropertyValuesDef(VarName);
	const char* GuiType = VarClassOwner->GetPropertyDefAttr(VarName, "gui");
	const char* desc = VarClassOwner->GetPropertyDefAttr(VarName, "desc");

	if(VarType == PCVT_UNKNOWN)
		VarType = PCVT_STRING;

	if(!GuiType)
		GuiType = "editbox";

	DString PropID = id;
	const char* PropLabel = VarName;
	const char* PropName = PropID.c_str();
	wxPGProperty* prop = NULL;

	bool BoolValue;
	int IntValue;
	float FloatValue;
	const char* StringValue = VarValue;
//	const char* DefaultValue = def->GetDefaultValue();

	switch(VarType)
	{
		case PCVT_BOOL:
		{
			BoolValue = (taAtoI(StringValue) != 0);
			break;
		}
		case PCVT_INT:
		{
			IntValue = taAtoI(StringValue);
			BoolValue = (IntValue != 0);
			break;
		}
		case PCVT_FLOAT:
		{
			FloatValue = taAtoF(StringValue);
			break;
		}
		case PCVT_STRING:
		{
//			StringValue = StringValue;
			break;
		}
		default:
			enf_assert(false);
			break;
	}

	int MinIntValue = INT_MIN;
	int MaxIntValue = INT_MAX;
	float MinFloatValue = -9999999999;//FLT_MIN;
	float MaxFloatValue = 9999999999;//FLT_MAX;

	if(VarType == PCVT_INT || VarType == PCVT_FLOAT)
	{
		const char* MinValue = VarClassOwner->GetPropertyDefAttr(VarName, "minvalue");
		const char* MaxValue = VarClassOwner->GetPropertyDefAttr(VarName, "maxvalue");

		if(VarType == PCVT_INT)
		{
			if(MinValue) MinIntValue = taAtoI(MinValue);
			if(MaxValue) MaxIntValue = taAtoI(MaxValue);

			if(IntValue < MinIntValue)
			{
				enf_assert(false);
				IntValue = MinIntValue;
			}
			else if(IntValue > MaxIntValue)
			{
				enf_assert(false);
				IntValue = MaxIntValue;
			}
		}
		else if(VarType == PCVT_FLOAT)
		{
			if(MinValue) MinFloatValue = taAtoF(MinValue);
			if(MaxValue) MaxFloatValue = taAtoF(MaxValue);

			if(FloatValue < MinFloatValue)
			{
				enf_assert(false);
				FloatValue = MinFloatValue;
			}
			else if(FloatValue > MaxFloatValue)
			{
				enf_assert(false);
				FloatValue = MaxFloatValue;
			}
		}
	}

	wxPGChoices choices;

	if(ValuesElement)
	{
		for(const TiXmlNode* node = ValuesElement->FirstChild(); node != NULL; node = node->NextSibling())
		{
			const TiXmlElement* elm = node->ToElement();

			if(!elm)
				continue;

			const char* ElmName = elm->Value();

			if(strcmpi(ElmName, "value") == 0)
			{
				const char* name = elm->Attribute("name");
				const char* value = elm->Attribute("value");

				if(!value)
					continue;

				if(name)
					choices.Add(name, taAtoI(value));
				else
					choices.Add(value);
			}
			else if(strcmpi(ElmName, "ClassMembers") == 0)
			{
				const char* path = elm->Attribute("path");
				PCClass* SourceClass = NULL;
				
				if(path)
				{
					PCNode* SourcNode = g_editor->GetParamConfig()->FindNode(path);
					SourceClass = SourcNode ? SourcNode->ToClass() : NULL;
				}

				if(SourceClass)	//volby plnime z nazvov memberov classu
				{
					const char* classes = elm->Attribute("classes");
					const char* arrays = elm->Attribute("arrays");
					const char* variables = elm->Attribute("variables");
					bool Classes = classes ? (strcmpi(classes, "true") == 0) : false;
					bool Arrays = arrays ? (strcmpi(arrays, "true") == 0) : false;
					bool Variables = variables ? (strcmpi(variables, "true") == 0) : false;

					for(uint n = 0; n < SourceClass->GetNodesCount(); n++)
					{
						PCNode* node = SourceClass->GetNode(n);
			
						if(!Classes && node->ToClass())
							continue;

						if(!Arrays && node->ToArray())
							continue;

						if(!Variables && node->ToVariable())
							continue;

						choices.Add(node->GetName());
					}
				}
			}
		}
	}
	else	//if values for property are not defined, this is chance to get their from any other source
	{
		g_editor->GetGuiChoices(PropLabel, StringValue, choices);	//virtual
	}

	if(strcmpi(GuiType, "editcombobox") == 0 || strcmpi(GuiType, "readonlycombobox") == 0 || strcmpi(GuiType, "flags") == 0)
	{
		if(!choices.IsOk())
			GuiType = "editbox";
	}
	else if(strcmpi(GuiType, "slider") == 0)
	{
		if(VarType != PCVT_INT && VarType != PCVT_FLOAT)
		{
			enf_assert(false);
			GuiType = "editbox";
		}
	}

	if(strcmpi(GuiType, "editbox") == 0)
	{
		if(VarType == PCVT_INT)
			prop = PropGrid->InsertEditBoxProperty(ParentProp, PropName, PropLabel, IntValue, IntValidator(MinIntValue, MaxIntValue), position);
		else if(VarType == PCVT_FLOAT)
			prop = PropGrid->InsertEditBoxProperty(ParentProp, PropName, PropLabel, FloatValue, FloatValidator(MinFloatValue, MaxFloatValue), position);
		else	//string
			prop = PropGrid->InsertEditBoxProperty(ParentProp, PropName, PropLabel, StringValue, wxDefaultValidator, position);
	}
	else if(strcmpi(GuiType, "readonlycombobox") == 0)
	{
		enf_assert(choices.IsOk());

		if(VarType == PCVT_INT)
			prop = PropGrid->InsertComboBoxProperty(ParentProp, PropName, PropLabel, IntValue, wxDefaultValidator/*IntValidator(MinIntValue, MaxIntValue)*/, choices, true, position);
		else	//string
			prop = PropGrid->InsertComboBoxProperty(ParentProp, PropName, PropLabel, StringValue, wxDefaultValidator, choices, true, position);
	}
	else if(strcmpi(GuiType, "editcombobox") == 0)
	{
		enf_assert(choices.IsOk());

		if(VarType == PCVT_INT)
		{
			EnumIntValidator validator(MinIntValue, MaxIntValue);
			const wxArrayString& EnumNames = choices.GetLabels();
//				const wxArrayInt& EnumValues = choices.GetValues();

			for(uint ch = 0; ch < EnumNames.GetCount(); ch++)
				validator.AddEnum(EnumNames[ch].c_str());

			prop = PropGrid->InsertComboBoxProperty(ParentProp, PropName, PropLabel, IntValue, validator, choices, false, position);
		}
		else	//string
			prop = PropGrid->InsertComboBoxProperty(ParentProp, PropName, PropLabel, StringValue, wxDefaultValidator, choices, false, position);
	}
	else if(strcmpi(GuiType, "checkbox") == 0)
	{
		enf_assert(VarType == PCVT_BOOL);
		prop = PropGrid->InsertCheckBoxProperty(ParentProp, PropName, PropLabel, BoolValue, position);
	}
	else if(strcmpi(GuiType, "filedialog") == 0)
	{
		const char* FileExt = VarClassOwner->GetPropertyDefAttr(VarName, "FileExt");
		prop = PropGrid->InsertFileProperty(ParentProp, PropName, PropLabel, StringValue, FileExt, wxDefaultValidator, position);
	}
	else if(strcmpi(GuiType, "flags") == 0)
	{
		prop = PropGrid->InsertFlagsProperty(ParentProp, PropName, PropLabel, IntValue, choices, position);
	}
	else if(strcmp(GuiType, "slider") == 0)
	{
		if(VarType == PCVT_INT)
			prop = PropGrid->InsertSliderProperty(ParentProp, PropName, PropLabel, IntValue, MinIntValue, MaxIntValue, -1);
		else if(VarType == PCVT_FLOAT)
		{
			const char* StepValue = VarClassOwner->GetPropertyDefAttr(VarName, "step");

			if(!StepValue)
				StepValue = "0.1";

			float StepVal = taAtoF(StepValue);

			if(StepVal == 0.0f)
				StepVal = 0.1f;

			prop = PropGrid->InsertSliderProperty(ParentProp, PropName, PropLabel, FloatValue, MinFloatValue, MaxFloatValue, StepVal, -1);
		}
		else
		{
			enf_assert(false);
		}
	}
/*	else if(strcmpi(GuiType, "colordialog") == 0)
	{
		int r = (int)(VectorValue.x * 255.0f);
		int g = (int)(VectorValue.y * 255.0f);
		int b = (int)(VectorValue.z * 255.0f);
		prop = GetPropGrid()->AppendColorProperty(PropName, PropLabel, wxColour(r, g, b));
	}*/

	if(prop)
	{
		if(desc)
			GetPropGrid()->SetPropertyHelpString(prop->GetId(), desc);

		if(IsReadOnly)
			GetPropGrid()->DisableProperty(prop->GetId());

		if(!InOverloadMode() && variable && variable->IsExtern())
		{
//			GetPropGrid()->SetPropertyImage(prop->GetId(), ExternVariableBitmap);
			GetPropGrid()->SetPropertyTextColour(prop->GetId(), *wxLIGHT_GREY);
		}
//		else
//			GetPropGrid()->SetPropertyImage(prop->GetId(), VariableBitmap);
	}

	return prop;
}

//--------------------------------------------------------------------------------------
wxPGId PropertiesPanel::FindPropSelectionAfterDelete(const wxPGId& PropID)
{
	enf_assert(PropID.IsOk());

	wxPGId SelectedAfter = PropGrid->GetNextVisible(PropID);

	while(SelectedAfter.IsOk() && !PropGrid->IsPropertyCategory(SelectedAfter) && PropGrid->GetPropertyParent(SelectedAfter) == PropID)
		SelectedAfter = PropGrid->GetNextVisible(SelectedAfter);

	if(!SelectedAfter.IsOk())
	{
		SelectedAfter = PropGrid->GetPrevVisible(PropID);

		while(SelectedAfter.IsOk() && !PropGrid->IsPropertyCategory(SelectedAfter) && PropGrid->GetPropertyParent(SelectedAfter) == PropID)
			SelectedAfter = PropGrid->GetPrevVisible(SelectedAfter);
	}

	return SelectedAfter;
}