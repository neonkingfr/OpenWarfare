#ifndef PARAM_FILE_TREE_H
#define PARAM_FILE_TREE_H

//#include "wx/treectrl.h"
#include "wxExtension/TreeListCtrl.h"

class PCClass;

enum
{
	MENU_INSERT_CLASS,
	MENU_INSERT_INNER_CLASS,
	MENU_DELETE_CLASS,
	MENU_RENAME_CLASS,
	MENU_SET_CLASS_BASE,
	MENU_SET_CLASS_EXTERN,
	MENU_SET_CLASS_NOT_EXTERN,
	MENU_SET_CLASS_EXTERN_WITH_MEMBERS,
	MENU_SET_CLASS_NOT_EXTERN_WITH_MEMBERS,
	MENU_SET_CLASS_NOT_EXTERN_AS_FORWARD_DECL,
	MENU_SET_CLASS_DELETED,
	MENU_SET_CLASS_NOT_DELETED,
	MENU_SET_CLASS_DEFINED,
	MENU_COPY_CLASSNAME_TO_CLIPBOARD,
	MENU_GOTO_BASE_CLASS,
	PFT_LAST_UNUSED_ID,
};

//=============================================================================
// ParamFileTree item data
//=============================================================================
class CTItemData : public wxTreeItemData
{
public:
	CTItemData(PCClass* cl);
	inline PCClass* GetClass(){return Class;}
private:
	PCClass* Class;
};

//=============================================================================
// treeview control from "Class tree" window
//=============================================================================
class ParamFileTree : public wxTreeListCtrl
{
public:
			ParamFileTree(wxWindow* parent, wxWindowID id);
	int	AddClass(PCClass* cl, wxTreeItemId parent, uint pos = INDEX_NOT_FOUND);
	bool	RemoveClass(const PCClass* cl);
	bool	RenameClass(const PCClass* cl, const char* NewName);
	wxTreeItemId GetItem(const char* path);
	wxTreeItemId GetChildItem(wxTreeItemId& parent, const char* name);
	wxTreeItemId GetChildWithClientData(const wxTreeItemId& parent, const PCClass* cl);
	PCClass* GetClassFromItem(const wxTreeItemId& item);
	void	EnsureSelectionVisible();
	void	Clear();
	void	SetSkipSelEvent(bool skip);
	void	UpdateClassItemDesign(PCClass* cl, const wxTreeItemId& item);
	int	GetImageIndexForClass(PCClass* cl);
	void	ShowBaseClasses(bool show);
protected:
	virtual wxMenu* CreateSelClassMenu();
private:
	EHashMap<DString, wxTreeItemId, DStringHashFunc> ItemsHash;
	bool SkipSelEvent;
	float Col1RateWidth;
	wxColour ExternItemColor;
	void UpdateColumnWidths();
	void OnSelChanged(wxTreeEvent& event);
	void OnSelChanging(wxTreeEvent& event);
	void OnRMBup(wxTreeEvent& event);
	void OnGetToolTip(wxTreeEvent& event);
	void OnSelectFromMenu(wxCommandEvent& event);
	void OnKeyDown(wxTreeEvent& event);
	void CreateActionFromMenu(int MenuID, PCClass* cl);
	void OnSetFocus(wxFocusEvent& event);
	void OnColDragEnd(wxListEvent& event);
	void OnSize(wxSizeEvent& event);
	void OnMouse(wxMouseEvent& event);
	DECLARE_EVENT_TABLE()
};

#endif