#ifndef ATTR_DESCRIPTIONS_H
#define ATTR_DESCRIPTIONS_H

#include <map>

class TiXmlElement;

class AttrDescriptions
{
public:

	struct DescItem 
	{
		typedef EArray<std::pair<wxString, wxString>> TParams;
		wxString name;
		TParams params;
	};

	typedef std::map<wxString, DescItem> TItems;

	struct ClassDescriptions
	{
		wxString className;
		TItems descItems;
	};

	AttrDescriptions(const char* file);
	~AttrDescriptions();

	wxString GetDescriptionText(const wxString &name, const wxString &className);
	
protected:

	void LoadXmlFile(const char* file);

	void LoadClassDescriptions(const TiXmlElement* classElement);

	void LoadCommonDescriptions(const TiXmlElement* commonElement);

	void LoadDescriptions(const TiXmlElement* parentElement, TItems &items);
	
	typedef std::map<wxString, ClassDescriptions> TClassItems;

	TItems descItems;
	TClassItems classDescItems;

};

#endif