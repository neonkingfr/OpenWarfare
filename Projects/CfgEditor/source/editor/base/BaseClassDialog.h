
#ifndef BASE_CLASS_DIALOG_H
#define BASE_CLASS_DIALOG_H

#include "wx/dialog.h"

class PCClass;

//===========================================================================
//Simple dialog to select base class
//===========================================================================
class BaseClassDialog : public wxDialog
{
public:
	BaseClassDialog(wxWindow* parent, PCClass* SelectedClass);
	wxString GetBaseName();
	void SetBaseName(const wxString& name);
private:
	wxComboBox* BaseNameEditBox;
	wxButton*	OkButton;
	wxButton*	CancelButton;
};

#endif