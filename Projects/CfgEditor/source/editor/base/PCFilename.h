
#ifndef PC_FILENAME_H
#define PC_FILENAME_H

//===========================================================
//ParamConfigFilename
//stores other file dependencies required to load single param config
//===========================================================
class PCFilename
{
public:
	PCFilename();
	PCFilename(const char* FileName);
	PCFilename(const char* FileName, bool protect, bool CanUpdateBase);
	const DString& GetFilename() const {return FileName;}
	bool ProtectByUpdate() const {return protect;}
	bool CanUpdateBaseByUpdate() const {return CanUpdateBase;}
private:
	DString FileName;
	bool protect;
	bool CanUpdateBase;
};

#endif