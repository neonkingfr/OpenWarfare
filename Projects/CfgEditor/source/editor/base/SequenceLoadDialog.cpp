
#include "precomp.h"
#include "SequenceLoadDialog.h"
#include "ImageList.h"
#include "MainFrame.h"
#include "GenericEditor.h"
#include "common/WXConvert.h"
#include "common/XMLConfig.h"
#include "common/WinApiUtil.h"

BEGIN_EVENT_TABLE(CheckListBox, wxCheckListBox)
	EVT_LEFT_DOWN(CheckListBox::OnListBoxLMBDown)
	EVT_MOTION(CheckListBox::OnListBoxMouseMove)
END_EVENT_TABLE()

const int CheckListBoxLineHeight = 15;

//=============================================================================
CheckListBox::CheckListBox(SequenceLoadDialog* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style)
	:wxCheckListBox(parent, id, pos, size, n, choices, style)
{
	dialog = parent;
	MouseEvents = true;
}

//-----------------------------------------------------------------------------
void CheckListBox::OnListBoxLMBDown(wxMouseEvent& event)
{
	if(MouseEvents)
		dialog->OnListBoxLMBDown(event);

	event.Skip();
}

//-----------------------------------------------------------------------------
void CheckListBox::OnListBoxMouseMove(wxMouseEvent& event)
{
	if(MouseEvents)
		dialog->OnListBoxMouseMove(event);

	if(!event.LeftIsDown())
		MouseEvents = true;

	event.Skip();
}

//-----------------------------------------------------------------------------
void CheckListBox::EnableMouseEvents(bool enable)
{
	MouseEvents = enable;
}

//-----------------------------------------------------------------------------
int CheckListBox::GetVirtScrollPos()
{
	int ScrollRange = GetScrollRange(wxVERTICAL);
	int ScrollPos = GetScrollPos(wxVERTICAL);
	int VirtualHeight = GetCount() * CheckListBoxLineHeight;
	double ScrollRate = (double)ScrollPos / (double)ScrollRange;
	ScrollPos = (int)(ScrollRate * (double)VirtualHeight);
	return ScrollPos;
}


BEGIN_EVENT_TABLE(SequenceLoadDialog, wxDialog)
	EVT_BUTTON(-1, SequenceLoadDialog::OnButton)
	EVT_LISTBOX(-1, SequenceLoadDialog::OnListBoxSelection)
END_EVENT_TABLE()

//=============================================================================
SequenceLoadDialog::SequenceLoadDialog(wxWindow* parent)
:wxDialog(parent, -1, "Load config as update of another config(s)", wxDefaultPosition, wxSize(512, 240))
{
	ListBox = new CheckListBox(this, CHECKLISTBOX_FILES, wxDefaultPosition, wxDefaultSize, 0, NULL, 0);
	AddButton = new wxBitmapButton(this, BUTTON_ADD, g_ImageList->GetBitmap(BI_ADD), wxDefaultPosition, wxSize(20, 20), wxBU_AUTODRAW);
	DelButton = new wxBitmapButton(this, BUTTON_DELETE, g_ImageList->GetBitmap(BI_DELETE), wxDefaultPosition, wxSize(20, 20), wxBU_AUTODRAW);
	UpButton = new wxBitmapButton(this, BUTTON_UP, g_ImageList->GetBitmap(BI_UP), wxDefaultPosition, wxSize(20, 20), wxBU_AUTODRAW);
	DownButton = new wxBitmapButton(this, BUTTON_DOWN, g_ImageList->GetBitmap(BI_DOWN), wxDefaultPosition, wxSize(20, 20), wxBU_AUTODRAW);
	OkButton = new wxButton(this, wxID_OK, "Ok", wxDefaultPosition, wxSize(80, 25));
	CancelButton = new wxButton(this, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(80, 25));

	ChangeButton = new wxButton(ListBox, BUTTON_CHANGE, "..", wxDefaultPosition, wxSize(CheckListBoxLineHeight + 1, CheckListBoxLineHeight + 1));

	wxStaticText* DialogLabel = new wxStaticText(this, -1, "Chose config files into load/update sequence and select config to edit.\nunchecked configs are ignored, selected config defines edit context", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	wxBoxSizer* ButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
	ButtonsSizer->Add(AddButton, 0, wxALIGN_LEFT | wxALL, 0);
	ButtonsSizer->AddSpacer(10);
	ButtonsSizer->Add(DelButton, 0, wxALIGN_LEFT | wxALL, 0);
	ButtonsSizer->AddSpacer(10);
	ButtonsSizer->Add(UpButton, 0, wxALIGN_LEFT | wxALL, 0);
	ButtonsSizer->AddSpacer(10);
	ButtonsSizer->Add(DownButton, 0, wxALIGN_LEFT | wxALL, 0);
	ButtonsSizer->AddStretchSpacer();
	ButtonsSizer->Add(OkButton, 0, wxALIGN_LEFT | wxALL, 0);
	ButtonsSizer->AddSpacer(10);
	ButtonsSizer->Add(CancelButton, 0, wxALIGN_LEFT | wxALL, 0);

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(DialogLabel, 0, wxALIGN_LEFT | wxALL, 10);
	sizer->Add(ListBox, 1, wxGROW | wxALIGN_LEFT | wxALL, 10);
	sizer->Add(ButtonsSizer, 0, wxGROW | wxALIGN_LEFT | wxALL, 10);
	SetSizer(sizer);

	LoadFromConfig();
	OkButton->SetFocus();
	UpdateAccesibility();
	UpdateChangeButtonPos();
	ChangeButton->Show(false);
}

//---------------------------------------------------------------------------------------
void SequenceLoadDialog::GetFileNamesToLoad(wxArrayString& filenames)
{
	int CurSel = ListBox->GetSelection();

	if(CurSel == -1)
		return;

	for(int n = 0; n <= CurSel; n++)
	{
		bool enabled = ListBox->IsChecked(n);

		if(!enabled)
			continue;

		wxString filename = ListBox->GetString(n);
		filenames.Add(filename);
	}
}

//---------------------------------------------------------------------------------------
void SequenceLoadDialog::UpdateAccesibility()
{
	int selection = ListBox->GetSelection();
	int count = ListBox->GetCount();
	int LastIndex = count - 1;
	bool AnyItemSelected = (count > 0 && selection >= 0);
	DelButton->Enable(AnyItemSelected);
	UpButton->Enable(AnyItemSelected && selection > 0);
	DownButton->Enable(AnyItemSelected && selection < LastIndex);
	ChangeButton->Show(selection >= 0);
}

//---------------------------------------------------------------------------------------
void SequenceLoadDialog::UpdateChangeButtonPos(int ForcedSelection)
{
	int selection = (ForcedSelection >= 0) ? ForcedSelection : ListBox->GetSelection();

	if(selection >= 0)
	{
		wxSize ListBoxSize = ListBox->GetClientSize();
		wxSize ButtonSize = ChangeButton->GetSize();
		float fpos = (float)selection * (float)CheckListBoxLineHeight;
		int pos = fpos;
		int ScrollPos = ListBox->GetVirtScrollPos();
		pos -= ScrollPos;
		wxPoint ButtonPos(ListBoxSize.x - ButtonSize.x, pos);
		ChangeButton->Move(ButtonPos);
	}
}

//---------------------------------------------------------------------------------------
int SequenceLoadDialog::CalculateSelectionFromPoint(wxPoint& point)
{
	if(point.x <= 16)	//chekbox
		return -1;

	int LastLine = ListBox->GetCount() - 1;
	int ScrollPos = ListBox->GetVirtScrollPos();
	int VirtPoint = ScrollPos + point.y;
	int sel = VirtPoint / CheckListBoxLineHeight;

	if(LastLine >= 0)
	{
		if(sel < 0)
			sel = 0;
		else if(sel > LastLine)
			sel = LastLine;
	}

	return sel;
}

//---------------------------------------------------------------------------------------
void SequenceLoadDialog::OnListBoxLMBDown(wxMouseEvent& event)
{
	UpdateChangeButtonPos(CalculateSelectionFromPoint(event.GetPosition()));
}

//---------------------------------------------------------------------------------------
void SequenceLoadDialog::OnListBoxMouseMove(wxMouseEvent& event)
{
	if(event.LeftIsDown())
		UpdateChangeButtonPos(CalculateSelectionFromPoint(event.GetPosition()));
}

//---------------------------------------------------------------------------------------
void SequenceLoadDialog::OnListBoxSelection(wxCommandEvent& event)
{
	if(event.GetId() == CHECKLISTBOX_FILES)
	{
		UpdateChangeButtonPos();
		UpdateAccesibility();
	}

	event.Skip();
}

//---------------------------------------------------------------------------------------
wxString SequenceLoadDialog::GetPathFromUser()
{
	wxFileDialog dialog(this, _T("Select Config"), _T(""), _T(""), _T("Config files (*.cpp;*.bin)|*.cpp;*.bin"), wxOPEN | wxFILE_MUST_EXIST);
	dialog.Centre();

	if(dialog.ShowModal() == wxID_OK)
	{
		wxString path = dialog.GetPath();
		path.Replace("\\", "/");
		return path;
	}
	return wxString();
}

//---------------------------------------------------------------------------------------
void SequenceLoadDialog::OnButton(wxCommandEvent& event)
{
	switch(event.GetId())
	{
		case BUTTON_ADD:
		{
			wxString path = GetPathFromUser();

			if(!path.IsEmpty())
			{
				int CurSel = ListBox->GetSelection();
				int InsertPos = (CurSel != wxNOT_FOUND) ? CurSel + 1 : -1;

				if(InsertPos == -1 || InsertPos >= ListBox->GetCount())
					InsertPos = ListBox->Append(path);
				else
					ListBox->Insert(path, InsertPos);

				if(InsertPos >= 0)
				{
					ListBox->Check(InsertPos);
					ListBox->Select(InsertPos);
				}
			}
			UpdateChangeButtonPos();
			UpdateAccesibility();
			break;
		}
		case BUTTON_DELETE:
		{
			int CurSel = ListBox->GetSelection();

			if(CurSel != -1)
			{
				ListBox->Delete(CurSel);

				if(CurSel < ListBox->GetCount())
					ListBox->Select(CurSel);
				else if(CurSel > 0)
					ListBox->Select(--CurSel);
			}

			UpdateChangeButtonPos();
			UpdateAccesibility();
			break;
		}
		case BUTTON_UP:
		{
			int CurSel = ListBox->GetSelection();

			if(CurSel > 0)
			{
				int DelPos = CurSel - 1;
				wxString DelStr = ListBox->GetString(DelPos);
				bool DelCheck = ListBox->IsChecked(DelPos);
				ListBox->SetString(DelPos, ListBox->GetString(CurSel));
				ListBox->SetString(CurSel, DelStr);
				ListBox->Check(DelPos, ListBox->IsChecked(CurSel));
				ListBox->Check(CurSel, DelCheck);
				ListBox->Select(DelPos);
			}

			UpdateChangeButtonPos();
			UpdateAccesibility();
			break;
		}
		case BUTTON_DOWN:
		{
			int CurSel = ListBox->GetSelection();
			int LastIndex = ListBox->GetCount() - 1;

			if(CurSel >= 0 && CurSel < LastIndex)
			{
				int DelPos = CurSel + 1;
				wxString DelStr = ListBox->GetString(DelPos);
				bool DelCheck = ListBox->IsChecked(DelPos);
				ListBox->SetString(DelPos, ListBox->GetString(CurSel));
				ListBox->SetString(CurSel, DelStr);
				ListBox->Check(DelPos, ListBox->IsChecked(CurSel));
				ListBox->Check(CurSel, DelCheck);
				ListBox->Select(DelPos);
			}

			UpdateChangeButtonPos();
			UpdateAccesibility();
			break;
		}
		case BUTTON_CHANGE:
		{
			int CurSel = ListBox->GetSelection();

			if(CurSel >= 0)
			{
				ListBox->EnableMouseEvents(false);
				wxString path = GetPathFromUser();

				if(!path.IsEmpty())
					ListBox->SetString(CurSel, path);
			}

			UpdateChangeButtonPos();
			UpdateAccesibility();
			break;
		}
		case wxID_OK:
		{
			wxArrayString filenames;
			GetFileNamesToLoad(filenames);

			if(filenames.GetCount() == 0)
			{
				wxMessageDialog dialog(this, "No config to load", _T("Loading config as update"), wxICON_INFORMATION | wxOK);
				dialog.CentreOnParent();
				dialog.ShowModal();
				return;
			}

			for(uint n = 0; n < filenames.GetCount(); n++)
			{
				wxString path = filenames[n];
				if(!taFileExists(path.c_str()))
				{
					wxString msg("file ");
					msg += path;
					msg += " not exist";
					wxMessageDialog dialog(this, msg, _T("Invalid config file"), wxICON_INFORMATION | wxOK);
					dialog.CentreOnParent();
					dialog.ShowModal();
					ListBox->Select(n);
					UpdateChangeButtonPos();
					UpdateAccesibility();
					return;
				}
			}

			int CurSel = ListBox->GetSelection();

			if(CurSel == -1)
			{
				wxString msg("No config selected. U must select single config to define edit context");
				wxMessageDialog dialog(this, msg, _T("Missing edit context"), wxICON_INFORMATION | wxOK);
				dialog.CentreOnParent();
				dialog.ShowModal();
				return;
			}

			if(!ListBox->IsChecked(CurSel))
			{
				wxString msg("Selected config (config to edit) must be checked as valid member of loading/updating sequence");
				wxMessageDialog dialog(this, msg, _T("Missing edit context"), wxICON_INFORMATION | wxOK);
				dialog.CentreOnParent();
				dialog.ShowModal();
				return;
			}

			SaveToConfig();	
			break;
		}
	}

	event.Skip();
}

//---------------------------------------------------------------------------------------
void SequenceLoadDialog::LoadFromConfig()
{
	ListBox->Clear();
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);
	node = node->GetChildNode("SequenceDialogFiles", true);
	int LastEnabled = -1;

	for(XMLConfigNode* fnode = node->GetFirstChildNode(); fnode; fnode = fnode->GetNextSiblingNode())
	{
		if(strcmpi(fnode->GetName(), "file") != 0)
			continue;

		wxString path;
		bool enabled = false;
		if(fnode->GetAttributeValue("path", path))
		{
			fnode->GetAttributeValue("enabled", enabled);
			int index = ListBox->Append(path);
			ListBox->Check(index, enabled);

			if(enabled)
				LastEnabled = index;
		}
	}

	wxString LastSelectedFile;
	if(node->GetValue("LastSelectedFile", LastSelectedFile))
		ListBox->SetStringSelection(LastSelectedFile);
	
	if(ListBox->GetSelection() == -1 && LastEnabled != -1)
		ListBox->Select(LastEnabled);

	UpdateChangeButtonPos();
	UpdateAccesibility();
}

//---------------------------------------------------------------------------------------
void SequenceLoadDialog::SaveToConfig()
{
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);
	node = node->GetChildNode("SequenceDialogFiles", true);
	node->RemoveChildNodes();

	for(uint n = 0; n < ListBox->GetCount(); n++)
	{
		wxString path = ListBox->GetString(n);
		bool enabled = ListBox->IsChecked(n);
		XMLConfigNode* fnode = node->AppendChildNode("file");
		fnode->SetAttributeValue("path", path);
		fnode->SetAttributeValue("enabled", enabled);
	}

	int sel = ListBox->GetSelection();

	if(sel != -1)
	{
		wxString SelPath = ListBox->GetString(sel);

		if(!SelPath.IsEmpty())
			node->SetValue("LastSelectedFile", SelPath);
	}
}
