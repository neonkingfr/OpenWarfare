#include "precomp.h"
#include "base/ParamFileTree.h"
#include "base/ParamConfig.h"
#include "base/MainFrame.h"
#include "base/GenericEditor.h"
#include "base/ImageList.h"
#include "base/PropertiesPanel.h"
#include "base/EditActions.h"
#include "base/CreateClassDialog.h"
#include "base/BaseClassDialog.h"
#include "base/ListMessageDialog.h"
#include "Common/NativeUtil.h"
#include "Common/TextFile.h"
#include "wxExtension/WBPropertyGrid.h"
#include "wx/clipbrd.h"


//--------------------------------------------------------------------------------
int CheckClassName(const wxString& name, bool NoEmptyName)
{
	if(NoEmptyName)
	{
		if(name.IsEmpty())
		{
			wxMessageDialog dialog( g_MainFrame, "You must enter any name for class!", "Bad input", wxICON_INFORMATION | wxOK);
			dialog.CentreOnParent();
			dialog.ShowModal();
			return -1;
		}
	}

	if(name.Find(' ') > -1)	//cannot use space characters in class name
	{
		wxMessageDialog dialog( g_MainFrame, "Space characters are not allowed!", "Bad input", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return -1;
	}

	if(IsNumber(name[0]))
	{
		wxMessageDialog dialog( g_MainFrame, "First character must be not numeric!", "Bad input", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return -1;
	}
	return 1;
}

//======================================================================
CTItemData::CTItemData(PCClass* cl)
	:wxTreeItemData()
{
	assert(cl);
	Class = cl;
}

/*
BEGIN_EVENT_TABLE(ParamFileTree, wxTreeCtrl)
	EVT_TREE_SEL_CHANGED(-1, ParamFileTree::OnSelChanged) 
	EVT_TREE_ITEM_MENU(-1, ParamFileTree::OnItemMenu)
	EVT_MENU(-1, OnSelectFromMenu)
	EVT_TREE_KEY_DOWN(-1, ParamFileTree::OnKeyDown)
	EVT_TREE_ITEM_GETTOOLTIP(-1, ParamFileTree::OnGetToolTip) 
//	EVT_SET_FOCUS(ParamFileTree::OnSetFocus)
END_EVENT_TABLE()
*/

BEGIN_EVENT_TABLE(ParamFileTree, wxTreeListCtrl)
	EVT_TREE_SEL_CHANGED(-1, ParamFileTree::OnSelChanged)
	EVT_TREE_SEL_CHANGING(-1, ParamFileTree::OnSelChanging)
	EVT_MENU(-1, ParamFileTree::OnSelectFromMenu)
	EVT_TREE_KEY_DOWN(-1, ParamFileTree::OnKeyDown)
	EVT_TREE_ITEM_RIGHT_CLICK(-1, ParamFileTree::OnRMBup) 
//	EVT_LIST_COL_DRAGGING(-1, ParamFileTree::OnColDragging)
	EVT_LIST_COL_END_DRAG(-1, ParamFileTree::OnColDragEnd)
	EVT_SIZE(ParamFileTree::OnSize)
	EVT_MOTION(ParamFileTree::OnMouse)
END_EVENT_TABLE()

//======================================================================
ParamFileTree::ParamFileTree(wxWindow* parent, wxWindowID id)
	:wxTreeListCtrl(parent, id, wxDefaultPosition, wxSize(-1, -1), wxTR_DEFAULT_STYLE | wxTR_MULTIPLE | wxTR_FULL_ROW_HIGHLIGHT | wxNO_BORDER)
{
	SkipSelEvent = false;
	Col1RateWidth = 0.666;
	ExternItemColor = wxColour(127, 127, 127);

/*	wxSize sz = GetClientSize ();
	int col2 = (int)((float)sz.x * 0.3333f);
	int col1 = sz.x - col2;*/
	AddColumn (_T("Class"), 200, wxALIGN_LEFT);
	SetColumnEditable (0, false);
	AddColumn (_T("Base class"), 100, wxALIGN_LEFT);
	SetColumnEditable (1, false);
//	SetColumnShown (1, false);
//	SetColumnAlignment (1, wxALIGN_RIGHT);
}

//----------------------------------------------------------------------
void ParamFileTree::SetSkipSelEvent(bool skip)
{
	SkipSelEvent = skip;
}

//----------------------------------------------------------------------
int ParamFileTree::GetImageIndexForClass(PCClass* cl)
{
	int ImageIndex = 0;

	if(cl->ToConfig())
		ImageIndex = BI_PARAM_FILE;
	else
	{
		if(cl->IsReadOnly())
		{
			if(cl->IsDeleted())
				ImageIndex = BI_PARAM_CLASS_DELETED;
			else
				ImageIndex = cl->IsDefined() ? BI_PARAM_CLASS_READ_ONLY : BI_PARAM_CLASS_DECL_READ_ONLY;
		}
		else
		{
			ImageIndex = cl->IsDefined() ? BI_PARAM_CLASS : BI_PARAM_CLASS_DECL;
		}
	}
	return ImageIndex;
}

//----------------------------------------------------------------------
void ParamFileTree::UpdateClassItemDesign(PCClass* cl, const wxTreeItemId& item)
{
	enf_assert(cl);
	enf_assert(item.IsOk());
	int ImageIndex = GetImageIndexForClass(cl);

//	if(strcmpi(cl->GetName(), "RscTitle") == 0)
//		int gg = 0;

	SetItemImage(item, ImageIndex, wxTreeItemIcon_Normal);

	if(cl->IsExtern())
	{
//		SetItemTextColour(item, ExternItemColor);
		SetItemTextColour(item, 0, ExternItemColor);
	}
	else
	{
//		SetItemTextColour(item, *wxBLACK);
		SetItemTextColour(item, 0, *wxBLACK);
	}

	PCClass* base = cl->GetBase();

	if(base)
	{
		SetItemText(item, 1, base->GetName());

		if(base->IsExtern())
			SetItemTextColour(item, 1, ExternItemColor);
		else
			SetItemTextColour(item, 1, *wxBLACK);

		ImageIndex = GetImageIndexForClass(base);
		SetItemImage(item, 1, ImageIndex, wxTreeItemIcon_Normal);
	}
	else
	{
		SetItemText(item, 1, "");
		SetItemImage(item, 1, -1, wxTreeItemIcon_Normal);
	}
}

//----------------------------------------------------------------------
int ParamFileTree::AddClass(PCClass* cl, wxTreeItemId parent, uint pos)
{
//	if(strcmpi("rscPicture", cl->GetName()) == 0)
//		int gg = 0;

	if(cl->IsHidden())	//do not insert hidden classes
		return 0;

	wxTreeItemId item;

	if(cl->ToConfig())
	{
		wxTreeItemId root = GetRootItem();
		assert(!root.IsOk());
		item = AddRoot(cl->GetName());
	}
	else
	{
		if(pos == INDEX_NOT_FOUND)
			item = AppendItem(parent, cl->GetName());
		else
			item = InsertItem(parent, pos, cl->GetName());
	}

	enf_assert(item.IsOk());
	SetItemData(item, new CTItemData(cl));
	UpdateClassItemDesign(cl, item);

	bool inserted = ItemsHash.Insert(cl->GetId(), item);
	enf_assert(inserted);

	int NumChildClasses = 0;

	for(uint n = 0; n < cl->GetNodesCount(); n++)
	{
		PCClass* SubClass = cl->GetNode(n)->ToClass();

		if(!SubClass)
			continue;

		NumChildClasses++;
		AddClass(SubClass, item, INDEX_NOT_FOUND);
	}
	return NumChildClasses;
}

//----------------------------------------------------------------------
bool ParamFileTree::RemoveClass(const PCClass* cl)
{
	//remove all childs first
	for(uint n = 0; n < cl->GetNodesCount(); n++)
	{
		PCClass* SubClass = cl->GetNode(n)->ToClass();

		if(!SubClass)
			continue;

		RemoveClass(SubClass);	//can return valid false if SubClass is hidden
	}

	if(cl->IsHidden())	//hidden classes not found in tree
		return false;

	DString id = cl->GetId();

	wxTreeItemId item = ItemsHash.Find(id);
	enf_assert(item.IsOk());
	Delete(item);

	bool removed = ItemsHash.Remove(id);
	enf_assert(removed);

	return removed;
}

//----------------------------------------------------------------------
bool ParamFileTree::RenameClass(const PCClass* cl, const char* NewName)
{
	enf_assert(cl);
	DString id = cl->GetId();

	wxTreeItemId item = ItemsHash.Find(id);
	enf_assert(item.IsOk());

	bool removed = ItemsHash.Remove(id);
	enf_assert(removed);

	SetItemText(item, NewName);

	wxString NewId = id.c_str();
	NewId = NewId.BeforeLast('/');
	NewId += '/';
	NewId += NewName;
	DString NewId2 = NewId.c_str();

	bool inserted = ItemsHash.Insert(NewId2, item);
	enf_assert(inserted);
	return inserted;
}

//----------------------------------------------------------------------
wxTreeItemId ParamFileTree::GetChildWithClientData(const wxTreeItemId& parent, const PCClass* cl)
{
	CTItemData* data = (CTItemData*)GetItemData(parent);
	assert(data);

	PCClass* ItemClass = data->GetClass();
	assert(ItemClass);

	if(ItemClass == cl)
		return parent;


//	if(ItemHasChildren(parent) == false)
	if(HasChildren(parent) == false)
		return wxTreeItemId();

	wxTreeItemIdValue cookie;
	for(wxTreeItemId CurItem = GetFirstChild(parent, cookie); CurItem.IsOk(); CurItem = GetNextSibling(CurItem))
	{
		wxTreeItemId id = GetChildWithClientData(CurItem, cl);

		if(id.IsOk())
			return id;
	}
	return wxTreeItemId();
}

//----------------------------------------------------------------------
void ParamFileTree::Clear()
{
	DeleteRoot();
//	DeleteAllItems();
	ItemsHash.Clear();
}

//----------------------------------------------------------------------
wxTreeItemId ParamFileTree::GetItem(const char* path)
{
	return ItemsHash.Find(path);
}

//----------------------------------------------------------------------------------------------
wxTreeItemId ParamFileTree::GetChildItem(wxTreeItemId& parent, const char* name)
{
	wxTreeItemIdValue cookie;

	for(wxTreeItemId CurItem = GetFirstChild(parent, cookie); CurItem.IsOk(); CurItem = GetNextSibling(CurItem))
	{
		if(strcmp(GetItemText(CurItem).c_str(), name) == 0)
			return CurItem;
	}
	return wxTreeItemId();
}

//----------------------------------------------------------------------
PCClass* ParamFileTree::GetClassFromItem(const wxTreeItemId& item)
{
	assert(item.IsOk());

	CTItemData* data = (CTItemData*)GetItemData(item);
	assert(data);

	PCClass* cl = data->GetClass();
	assert(cl);
	return cl;
}

//----------------------------------------------------------------------
void ParamFileTree::EnsureSelectionVisible()
{
	wxArrayTreeItemIds sel;
	GetSelections(sel);

	for(uint n = 0; n < sel.GetCount(); n++)
	{
		wxTreeItemId id = sel[n];

//		if(!IsVisible(id))
			EnsureVisible(id);
	}
}

//----------------------------------------------------------------------
void ParamFileTree::OnSelChanging(wxTreeEvent& event)
{
	WBPropertyGrid* PropGrid = g_MainFrame->GetPropGrid();
	wxWindow* control = PropGrid->GetPrimaryEditor();
/*
	//resolving problem with property grid when edit control of property is shown and focused. OnSelChanged event occurs before OnPropertyChanged event.
	//pure solution is veto OnSelChanged event

	if(PropGrid->EditorIsFocused())
	{
		event.Veto();
		PropGrid->UnfocusEditor();
	}
*/
//	PropGrid->UnfocusEditor();



	if(control && control->IsShown())
	{
		//if value in edit control of PropGrid is invalid, restore edit control to previous value
		if(PropGrid->EditorValidate() == false)
		{
			wxPGId id = PropGrid->GetSelection();

			if(id.IsOk())
			{
				wxPGProperty* prop = PropGrid->GetPropertyPtr(id);
				enf_assert(prop);

				if(prop)
				{
					const wxPGEditor* editor = PropGrid->GetPropertyEditor(id);
					editor->UpdateControl(prop, control);
				}
			}
			event.Veto();
		}
	}
}

//----------------------------------------------------------------------
void ParamFileTree::OnSelChanged(wxTreeEvent& event)
{
	wxTreeItemId Item = event.GetItem();
	wxTreeItemId OldItem = event.GetOldItem();

	//if(Item.IsOk() == false) means unselect event. next select event is comming after this one
	if(SkipSelEvent || g_MainFrame->IsDestroying() || !Item.IsOk())
	{
		event.Skip();
		return;
	}

	wxArrayTreeItemIds sel;
	GetSelections(sel);
	EArray<PCClass*> classes;
	PCClass* MainSel = NULL;

	//when this event is caused by select action using keyboard, OldItem.IsOk() == true. OldItem (previous selected and focused) is visualy selected but GetSelections not returns it. this is probably wxWidgets bug
/*	if(OldItem.IsOk())
	{
		bool OldItemInSelection = false;
		for(uint n = 0; n < sel.GetCount(); n++)
		{
			if(sel[n] == OldItem)
			{
				OldItemInSelection = true;
				break;
			}
		}

		if(!OldItemInSelection)
			sel.Add(OldItem);
	}*/

	for(uint n = 0; n < sel.GetCount(); n++)
	{
		wxTreeItemId id = sel[n];

		wxString ItemText = GetItemText(id);
		const char* sss = ItemText.c_str();

		CTItemData* data = (CTItemData*)GetItemData(id);
		assert(data);

		PCClass* cl = data->GetClass();
		assert(cl);

		if(id == Item)
		{
			MainSel = cl;
			continue;
		}

		classes.Insert(cl);
	}

	if(MainSel)
		classes.Insert(MainSel);

	g_editor->Select(classes, true);

	PropertiesPanel* PropPanel = g_MainFrame->GetPropPanel();
	PropPanel->GetPropGrid()->Refresh();
	event.Skip();	
}

//----------------------------------------------------------------------
wxMenu* ParamFileTree::CreateSelClassMenu()
{
	PCClass* MainSel = g_editor->GetMainSelection();
	assert(MainSel);
	PCClass* MainSelOwner = MainSel->GetOwner();

	wxMenu* ClassMenu = new wxMenu();

	if(!MainSel->ToConfig() && MainSelOwner && !MainSelOwner->IsReadOnly() && MainSelOwner->IsDefined())
		ClassMenu->Append(MENU_INSERT_CLASS, _T("Insert class"), _T("Insert class"));

	if(!MainSel->IsReadOnly())
	{
		if(MainSel->IsDefined())
			ClassMenu->Append(MENU_INSERT_INNER_CLASS, _T("Insert inner class"), _T("Insert class into selected class"));
		else
			ClassMenu->Append(MENU_SET_CLASS_DEFINED, _T("Remake to defined class"), _T("Remake to normal (defined) class"));

		if(!MainSel->ToConfig())	//never delete config file class
		{
//			ClassMenu->Append(MENU_DELETE_CLASS, _T("Delete"), _T("Delete selected classes"));
			ClassMenu->Append(MENU_RENAME_CLASS, _T("Rename"), _T("Rename selected class"));

			if(MainSel->IsDefined())
				ClassMenu->Append(MENU_SET_CLASS_BASE, _T("Set base class"), _T("Set base class of selected class"));
		}
	}

	if(!MainSel->ToConfig())
		ClassMenu->Append(MENU_DELETE_CLASS, _T("Delete"), _T("Delete selected classes"));

	bool AllSelectedCommonOwner = true;
	bool AllSelectedDefinedOwner = true;
	int AllSelectedExtern = -1;
	int AllSelectedExternOnLoad = -1;
	int AllSelectedDeleted = -1;

	EArray<PCClass*>& selection =	g_editor->GetSelection();

	for(uint n = 0; n < selection.GetCardinality(); n++)
	{
		PCClass* sel = selection[n];
		PCClass* own = sel->GetOwner();

		if(own != MainSelOwner)
			AllSelectedCommonOwner = false;

		if(own && !own->IsDefined())
			AllSelectedDefinedOwner = false;

		if(AllSelectedExtern != -2)
		{
			int ext = sel->IsExtern() ? 1 : 0;

			if(AllSelectedExtern == -1)
				AllSelectedExtern = ext;
			else if(ext != AllSelectedExtern)
				AllSelectedExtern = -2;
		}

		if(AllSelectedExternOnLoad != -2)
		{
			int ext = sel->WasExternOnLoad() ? 1 : 0;

			if(AllSelectedExternOnLoad == -1)
				AllSelectedExternOnLoad = ext;
			else if(ext != AllSelectedExternOnLoad)
				AllSelectedExternOnLoad = -2;
		}

		if(AllSelectedDeleted != -2)
		{
			int ext = sel->IsDeleted() ? 1 : 0;

			if(AllSelectedDeleted == -1)
				AllSelectedDeleted = ext;
			else if(ext != AllSelectedDeleted)
				AllSelectedDeleted = -2;
		}
	}

	if(MainSelOwner && !MainSelOwner->IsExtern() && AllSelectedCommonOwner && AllSelectedDefinedOwner)
	{
		if(AllSelectedExtern >= 0 && AllSelectedDeleted == 0)
		{
			if(AllSelectedExtern == 1)
			{
				ClassMenu->Append(MENU_SET_CLASS_NOT_EXTERN, _T("Overload"), _T("Overload selected extern classes"));
				ClassMenu->Append(MENU_SET_CLASS_NOT_EXTERN_WITH_MEMBERS, _T("Overload with params"), _T("Overload selected extern classes with all param members. not recursive"));

				if(selection.GetCardinality() == 1)
					ClassMenu->Append(MENU_SET_CLASS_NOT_EXTERN_AS_FORWARD_DECL, _T("Overload as forward declaration"), _T("Overload selected extern classes only as forward declarations"));
			}
/*			else	//cannot safety realize unoverload because in some special situations
			{
				if(AllSelectedExternOnLoad)
				{
					ClassMenu->Append(MENU_SET_CLASS_EXTERN, _T("UnOverload"), _T("UnOverload selected extern classes"));
					ClassMenu->Append(MENU_SET_CLASS_EXTERN_WITH_MEMBERS, _T("UnOverload with members"), _T("UnOverload selected extern classes with all members. not recursive"));
				}
			}*/
		}

		if(AllSelectedDeleted >= 0 && AllSelectedExtern == 1)
		{
			if(AllSelectedDeleted == 1)
				ClassMenu->Append(MENU_SET_CLASS_NOT_DELETED, _T("Remove deleted flag"), _T("Remove deleted flag"));
			else
			{
				if(AllSelectedExternOnLoad)
					ClassMenu->Append(MENU_SET_CLASS_DELETED, _T("Set deleted flag"), _T("Set deleted flag"));
			}
		}
	}

//	ClassMenu->AppendSeparator();
	if(!MainSel->ToConfig())
	ClassMenu->Append(MENU_COPY_CLASSNAME_TO_CLIPBOARD, _T("Copy name"), _T("Copy class name to clipboard"));

	if(MainSel->GetBase() && !MainSel->GetBase()->IsHidden())
		ClassMenu->Append(MENU_GOTO_BASE_CLASS, _T("base class"), _T("Select base class"));

	return ClassMenu;
}

//----------------------------------------------------------------------
void ParamFileTree::OnRMBup(wxTreeEvent& event)
{
	wxTreeItemId item = event.GetItem();

	if(!item.IsOk())	//caused by double right click outside items. wxWidgets bug? feature?
		return;

	PCClass* ItemClass = GetClassFromItem(item);

	if(!g_editor->IsSelected(ItemClass))
	{
		UnselectAll();
		SelectItem(item);
		g_editor->Select(ItemClass, true);
	}

	wxMenu* ClassMenu = CreateSelClassMenu();	//virtual
	assert(ClassMenu);

	wxPoint pt = event.GetPoint();
	pt.y += GetHeaderHeight();

	SetFocus();
	PopupMenu(ClassMenu, pt);

	delete ClassMenu;
	ClassMenu = NULL;

	event.Skip();
}

//----------------------------------------------------------------------------------------------
void ParamFileTree::UpdateColumnWidths()
{
	wxSize sz = GetClientSize();
	int Col1Width = (int)(Col1RateWidth * (float)sz.x);

	if(Col1Width < sz.x && Col1Width < 10)
		Col1Width = 10;

	int Col2Width = sz.x - Col1Width;
	SetColumnWidth(0, Col1Width);
	SetColumnWidth(1, Col2Width);
}

//----------------------------------------------------------------------------------------------
void ParamFileTree::OnSize(wxSizeEvent& event)
{
	UpdateColumnWidths();
	event.Skip();
}

//----------------------------------------------------------------------------------------------
void ParamFileTree::OnColDragEnd(wxListEvent& event)
{
	wxPoint pt = event.GetPoint();
	wxSize sz = GetClientSize();
	int ColWidth = GetColumnWidth(0);

	if(ColWidth >= sz.x)
		ColWidth = sz.x - 10;

	Col1RateWidth = (float)ColWidth / (float)sz.x;
	Col1RateWidth = min(Col1RateWidth, 1.0f);
	UpdateColumnWidths();
	event.Skip();
}

//----------------------------------------------------------------------------------------------
void ParamFileTree::OnGetToolTip(wxTreeEvent& event)
{
	wxTreeItemId item = event.GetItem();

	if(!item.IsOk())
		return;

	CTItemData* data = (CTItemData*)GetItemData(item);
	enf_assert(data);

	if(data)
	{
		PCClass* ItemClass = data->GetClass();
		enf_assert(ItemClass);

		if(ItemClass)
		{
			wxString tip("base: ");
			PCClass* base = ItemClass->GetBase();

			if(base)
				tip += base->GetName();
			else
				tip += "none";

			event.SetToolTip(tip); 
		}
	}
	event.Skip();
}

//----------------------------------------------------------------------------------------------
void ParamFileTree::OnSetFocus(wxFocusEvent& event)
{
	event.Skip();
}

//----------------------------------------------------------------------------------------------
void ParamFileTree::OnKeyDown(wxTreeEvent& event)
{
	int KeyCode = event.GetKeyCode();

	switch(KeyCode)
	{
		case WXK_RETURN:
		{
			wxArrayTreeItemIds sels;
			uint NumSel = GetSelections(sels);

			if(NumSel > 0)
			{
				wxTreeItemId item = sels[0];

				if(IsExpanded(item))
					Collapse(item);
				else
					Expand(item);
			}
			return;
		}
		case WXK_DELETE:
		{
			PCClass* MainSel = g_editor->GetMainSelection();
			assert(MainSel);

			if(MainSel && /*!MainSel->IsReadOnly() &&*/ !MainSel->ToConfig())
				CreateActionFromMenu(MENU_DELETE_CLASS, MainSel);

			return;
		}
		case WXK_INSERT:
		{
			PCClass* MainSel = g_editor->GetMainSelection();
			assert(MainSel);

			if(MainSel && !MainSel->ToConfig())
			{
				PCClass* MainSelOwner = MainSel->GetOwner();

				if(MainSelOwner && !MainSelOwner->IsReadOnly() && MainSelOwner->IsDefined())
					CreateActionFromMenu(MENU_INSERT_CLASS, MainSel);
			}

			return;
		}
	}

	event.Skip();
}

//----------------------------------------------------------------------
void ParamFileTree::OnMouse(wxMouseEvent& event)
{
	if(event.LeftIsDown())
	{
		int neco = 0;
	}
	event.Skip();
}

//----------------------------------------------------------------------
void ParamFileTree::OnSelectFromMenu(wxCommandEvent& event)
{
	PCClass* MainSel = g_editor->GetMainSelection();
	assert(MainSel);

	switch(event.GetId())
	{
		case MENU_INSERT_CLASS:
			CreateActionFromMenu(MENU_INSERT_CLASS, MainSel);
			return;
		case MENU_INSERT_INNER_CLASS:
			CreateActionFromMenu(MENU_INSERT_INNER_CLASS, MainSel);
			return;
		case MENU_DELETE_CLASS:
			CreateActionFromMenu(MENU_DELETE_CLASS, MainSel);
			return;
		case MENU_RENAME_CLASS:
			CreateActionFromMenu(MENU_RENAME_CLASS, MainSel);
			return;
		case MENU_SET_CLASS_BASE:
			CreateActionFromMenu(MENU_SET_CLASS_BASE, MainSel);
			return;
		case MENU_SET_CLASS_EXTERN:
			CreateActionFromMenu(MENU_SET_CLASS_EXTERN, MainSel);
			return;
		case MENU_SET_CLASS_NOT_EXTERN:
			CreateActionFromMenu(MENU_SET_CLASS_NOT_EXTERN, MainSel);
			return;
		case MENU_SET_CLASS_EXTERN_WITH_MEMBERS:
			CreateActionFromMenu(MENU_SET_CLASS_EXTERN_WITH_MEMBERS, MainSel);
			return;
		case MENU_SET_CLASS_NOT_EXTERN_WITH_MEMBERS:
			CreateActionFromMenu(MENU_SET_CLASS_NOT_EXTERN_WITH_MEMBERS, MainSel);
			return;
		case MENU_SET_CLASS_NOT_EXTERN_AS_FORWARD_DECL:
			CreateActionFromMenu(MENU_SET_CLASS_NOT_EXTERN_AS_FORWARD_DECL, MainSel);
			return;
		case MENU_SET_CLASS_DELETED:
			CreateActionFromMenu(MENU_SET_CLASS_DELETED, MainSel);
			return;
		case MENU_SET_CLASS_NOT_DELETED:
			CreateActionFromMenu(MENU_SET_CLASS_NOT_DELETED, MainSel);
			return;
		case MENU_SET_CLASS_DEFINED:
			CreateActionFromMenu(MENU_SET_CLASS_DEFINED, MainSel);
			return;
		case MENU_COPY_CLASSNAME_TO_CLIPBOARD:
		{
		  if (wxTheClipboard->Open())
		  {
			 wxTheClipboard->SetData( new wxTextDataObject(MainSel->GetName()) );
			 wxTheClipboard->Close();
		  }
			break;
		}
		case MENU_GOTO_BASE_CLASS:
		{
			PCClass* base = MainSel->GetBase();

			if(base && !base->IsHidden())
			{
				g_editor->Select(base, false);
				PropertiesPanel* PropPanel = g_MainFrame->GetPropPanel();
				PropPanel->GetPropGrid()->Refresh();
			}

			break;
		}
	}

	event.Skip();
}
/*
//-----------------------------------------------------------------------
int __cdecl CompareClasses(const void* a, const void* b)
{
	PCClass* cl1 = *(PCClass**)a;
	PCClass* cl2 = *(PCClass**)b;

	PCClass* owner1 = cl1->GetOwner();
	PCClass* owner2 = cl2->GetOwner();

	if(owner1 && owner1 == owner2)	//if common owner, sort result is position inside owner
	{
		return cl1->GetPosInOwner() - cl2->GetPosInOwner();
	}

	if(cl1->IsInheritedFrom(cl2))
		return 1;
	else if(cl2->IsInheritedFrom(cl1))
		return -1;

	return cl1->GetAbsorptionLevel() - cl2->GetAbsorptionLevel();
}
*/

//-----------------------------------------------------------------------
int __cdecl CompareClasses(const void* a, const void* b)
{
	PCClass* cl1 = *(PCClass**)a;
	PCClass* cl2 = *(PCClass**)b;

	int al1 = cl1->GetAbsorptionLevel();
	int al2 = cl2->GetAbsorptionLevel();
	int comp = al2 - al1;

	if(comp != 0)
		return comp;

	return cl1->GetPosInOwner() - cl2->GetPosInOwner();
}

//----------------------------------------------------------------------
void ParamFileTree::CreateActionFromMenu(int MenuID, PCClass* cl)
{
	ParamConfig* PConfig = g_editor->GetParamConfig();
	EArray<PCClass*>& selection =	g_editor->GetSelection();
	PCClass* owner = NULL;
	uint PosInOwner;
	wxString OwnerName;

	EditAction* RedoAction = NULL;
	EditAction* UndoAction = NULL;
	HistoryPoint* point = NULL;
	wxString RedoSelClass;

	if(MenuID == MENU_INSERT_CLASS || MenuID == MENU_INSERT_INNER_CLASS)
	{
		if(MenuID == MENU_INSERT_CLASS)
		{
			owner = cl->GetOwner();
			assert(owner);
			PosInOwner = cl->GetPosInOwner() + 1;	//+1 pretoze to bude za selektnutym classom
		}
		else
		{
			owner = cl;
			PosInOwner = owner->GetNodesCount();
		}

		OwnerName = owner->GetName();

		CreateClassDialog dialog(this, cl, (MenuID == MENU_INSERT_INNER_CLASS));
		dialog.CentreOnParent();
		wxString name;
		wxString BaseName;
		bool DeclarationOnly = false;
again:
		dialog.SetName(name);
		dialog.SetBaseName(BaseName);
		dialog.SetDeclarationOnly(DeclarationOnly);

		if(dialog.ShowModal() == wxID_OK)
		{
			name = dialog.GetName();
			BaseName = dialog.GetBaseName();
			DeclarationOnly = dialog.GetDeclarationOnly();
			DString BaseID;
			DString OwnerID;
			wxString ID;

			if(CheckClassName(name, true) == -1)
				goto again;

			if(CheckClassName(BaseName, false) == -1)
				goto again;

			if(owner->GetMember(name.c_str()))
			{
				wxString msg = "Invalid class name. ";
				msg += name + " already exist inside class " + OwnerName;

				wxMessageDialog dialog( g_MainFrame, msg, "Bad input", wxICON_INFORMATION | wxOK);
				dialog.CentreOnParent();
				dialog.ShowModal();
				goto again;
			}

			OwnerID = owner->GetId();
			ID =  OwnerID.c_str();
			ID += '/';
			ID += name;

			PCClass* base = NULL;
			const char* BaseIDCStr = NULL;

			if(!BaseName.IsEmpty())
			{
				base = owner->FindClass(BaseName.c_str(), true, true);

				if(!base)
				{
					wxString msg = "Invalid base (inherit) class name. Not found class ";
					msg += BaseName;

					wxMessageDialog dialog( g_MainFrame, msg, "Bad input", wxICON_INFORMATION | wxOK);
					dialog.CentreOnParent();
					dialog.ShowModal();
					goto again;
				}

				BaseID = base->GetId();
				BaseIDCStr = BaseID.c_str();
			}

			if(base)
			{
				if(base->IsExtern())
				{
					wxString msg = base->GetName();
					msg += " is valid base class but this class is from extern/base config. U must overload this class first";
					wxMessageDialog dialog( g_MainFrame, msg, "Bad base class detected", wxICON_INFORMATION | wxOK);
					dialog.CentreOnParent();
					dialog.ShowModal();
					goto again;
				}
				else if(base->GetOwner() == owner)
				{
					uint BasePosInOwner = base->GetPosInOwner();

					if(BasePosInOwner >= PosInOwner)
					{
						wxString msg = "Base class must be located before new class. U should maybe create forward declaration of base class to resolve this problem.";
						wxMessageDialog dialog( g_MainFrame, msg, "Bad location of base class", wxICON_INFORMATION | wxOK);
						dialog.CentreOnParent();
						dialog.ShowModal();
						goto again;
					}
				}
			}

			const PCDefClass* def = PConfig->GetDefinition()->GetClassDef(ID.c_str());	//definition vytvorime uz tu pretoze v CreateClass moze byt potrebny
			point = g_editor->CreateHistoryPoint("Create class");
			int ClassInfoFlags = PConfig->GetClassInfoFlags(owner, base);
			PCClass* StorageClass = PConfig->CreateClass(name.c_str(), NULL, OwnerID.c_str(), NULL, BaseIDCStr, !DeclarationOnly, def, false, ClassInfoFlags);

			if(base)
				StorageClass->SetBaseID(BaseID.c_str());

			RedoAction = ENF_NEW EACreateClass(ID.c_str(), BaseID.c_str(), OwnerID.c_str(), StorageClass, PosInOwner, ClassInfoFlags);
			UndoAction = ENF_NEW EADeleteNode(ID.c_str());
			point->AppendRedoAction(RedoAction);
			point->AppendUndoAction(UndoAction);
			RedoSelClass = ID.c_str();
		}
	}
	else if(MenuID ==  MENU_DELETE_CLASS)
	{
		EArray<PCClass*>& selection =	g_editor->GetSelection();
		DString ID = cl->GetId();
		point = g_editor->CreateHistoryPoint("Delete class");
		bool SkipAction = false;

		EArray<PCClass*> RefClasses;

		//get all classes to delete with selected classes. its all inherited from any of selected class
		for(uint n = 0; n < selection.GetCardinality(); n++)
		{
			PCClass* sel = selection[n];

			if(/*!sel->IsExtern() &&*/ !sel->ToConfig())
				PConfig->GetAllInheritClasses(sel, RefClasses, true);
		}

		//from RefClasses remove all selected classes
		EArray<PCClass*> ToRemoveFromRefClasses;
		for(uint n = 0; n < RefClasses.GetCardinality(); n++)
		{
			PCClass* RefCl = RefClasses[n];

			if(selection.GetIndexOf(RefCl) != INDEX_NOT_FOUND)
				ToRemoveFromRefClasses.Insert(RefCl);
		}

		for(uint n = 0; n < ToRemoveFromRefClasses.GetCardinality(); n++)
			RefClasses.Remove(ToRemoveFromRefClasses[n]);

		if(RefClasses.GetCardinality() > 0)
		{
			ListMessageDialog dialog( g_MainFrame, "Delete class");
			dialog.SetMessage("This operation has these side effects:\n Classes below will be deleted too! continue?");
			dialog.CentreOnParent();
			wxListBox* listbox = dialog.GetListBox();

			for(uint n = 0; n < RefClasses.GetCardinality(); n++)
			{
				PCClass* RefCl = RefClasses[n];
				DString id = RefCl->GetId();
				listbox->Append(id.c_str());
			}

			if(dialog.ShowModal() != wxID_OK)
			{
				SkipAction = true;
				SAFE_DELETE(point);
			}
		}

		//now create list of all classes to delete
		EArray<PCClass*> DelClasses;

		for(uint n = 0; n < selection.GetCardinality(); n++)
		{
			PCClass* sel = selection[n];

			if(/*!sel->IsExtern() &&*/ !sel->ToConfig())
				DelClasses.Insert(sel);
		}

		for(uint n = 0; n < RefClasses.GetCardinality(); n++)
			DelClasses.Insert(RefClasses[n]);

		//remove classes if there are childs of other class
		EArray<PCClass*> ToRemoveFromDelClasses;
		for(uint n = 0; n < DelClasses.GetCardinality(); n++)
		{
			PCClass* DelCl = DelClasses[n];

			for(uint r = 0; r < DelClasses.GetCardinality(); r++)
			{
				PCClass* DelCl2 = DelClasses[r];

				if(DelCl->IsChildOf(DelCl2, true))
				{
					ToRemoveFromDelClasses.Insert(DelCl);
					break;
				}
			}
		}

		for(uint n = 0; n < ToRemoveFromDelClasses.GetCardinality(); n++)
			DelClasses.Remove(ToRemoveFromDelClasses[n]);

		//sort classes to correct creating order for undo
		qsort(DelClasses.GetArray(), DelClasses.GetCardinality(), sizeof(PCClass*), CompareClasses);

		if(!SkipAction)
		{
			EArray<EditAction*> RedoActions;

			for(uint n = 0; n < DelClasses.GetCardinality(); n++)
			{
				PCClass* DelCl = DelClasses[n];

				g_editor->CreateDeleteClassAction(DelCl, &RedoAction, &UndoAction);
				assert(RedoAction);
				assert(UndoAction);
				RedoActions.Insert(RedoAction);
//				point->AppendRedoAction(RedoAction);
				point->AppendUndoAction(UndoAction);
			}

			//delete actions (for redo) must be stored in reverse order like create actions (for undo)
			for(int n = (int)RedoActions.GetCardinality() - 1; n >= 0; n--)
				point->AppendRedoAction(RedoActions[n]);

			wxTreeItemId DelItem = GetItem(ID.c_str());
			enf_assert(DelItem.IsOk());

			wxTreeItemId NextSelItem = GetNextSibling(DelItem);
			PCClass* NextSelClass = NULL;

			while(NextSelItem.IsOk() && NextSelClass == NULL)
			{
				NextSelClass = GetClassFromItem(NextSelItem);

				if(RefClasses.GetIndexOf(NextSelClass) == INDEX_NOT_FOUND || selection.GetIndexOf(NextSelClass) == INDEX_NOT_FOUND)
				{
					bool AnyParentToDelete = false;
					PCClass* own = NextSelClass->GetOwner();
					while(own)
					{
						if(own == cl || RefClasses.GetIndexOf(own) != INDEX_NOT_FOUND || selection.GetIndexOf(own) != INDEX_NOT_FOUND)
						{
							AnyParentToDelete = true;
							break;
						}

						own = own->GetOwner();
					}

					if(!AnyParentToDelete)
						break;
				}

				NextSelClass = NULL;
				NextSelItem = GetNextSibling(NextSelItem);
			}

			NextSelItem = GetPrevSibling(DelItem);

			while(NextSelItem.IsOk() && NextSelClass == NULL)
			{
				NextSelClass = GetClassFromItem(NextSelItem);

				if(RefClasses.GetIndexOf(NextSelClass) == INDEX_NOT_FOUND || selection.GetIndexOf(NextSelClass) == INDEX_NOT_FOUND)
				{
					bool AnyParentToDelete = false;
					PCClass* own = NextSelClass->GetOwner();
					while(own)
					{
						if(own == cl || RefClasses.GetIndexOf(own) != INDEX_NOT_FOUND || selection.GetIndexOf(own) != INDEX_NOT_FOUND)
						{
							AnyParentToDelete = true;
							break;
						}

						own = own->GetOwner();
					}

					if(!AnyParentToDelete)
						break;
				}

				NextSelClass = NULL;
				NextSelItem = GetPrevSibling(NextSelItem);
			}

			if(!NextSelClass)
			{
				NextSelItem = GetItemParent(DelItem);
				assert(NextSelItem.IsOk());
				NextSelClass = GetClassFromItem(NextSelItem);
			}

			assert(NextSelClass);
			DString NextSelClassID = NextSelClass->GetId();
			RedoSelClass = NextSelClassID.c_str();
		}
	}
	else if(MenuID == MENU_RENAME_CLASS)
	{
		owner = cl->GetOwner();
		enf_assert(owner);
		OwnerName = owner->GetName();

		DString ID = cl->GetId();
		wxString OldName = cl->GetName();
		wxString NewName = OldName;
		wxTextEntryDialog dialog(NULL, _T(""), wxString("Rename class"), NewName, wxOK | wxCANCEL);
		dialog.CentreOnParent();

again2:

		if(dialog.ShowModal() == wxID_OK)
		{
			NewName = dialog.GetValue();

			if(CheckClassName(NewName, true) == -1)
				goto again2;

			if(owner->GetMember(NewName.c_str()))
			{
				wxString msg = "Invalid class name. ";
				msg += NewName + " already exist inside class " + OwnerName;

				wxMessageDialog dialog( g_MainFrame, msg, "Bad input", wxICON_INFORMATION | wxOK);
				dialog.CentreOnParent();
				dialog.ShowModal();
				goto again2;
			}

			wxString NewID = ID.c_str();
			NewID = NewID.BeforeLast('/');
			NewID += "/";
			NewID += NewName;


			point = g_editor->CreateHistoryPoint("Rename class");

			//create actions needed as feedback after rename this class
			g_editor->CreateClassRenameFeedbackActions(cl, NewName.c_str(), point->GetRedoActions(), point->GetUndoActions());

			RedoAction = ENF_NEW EARenameNode(ID.c_str(), NewName.c_str());
			UndoAction = ENF_NEW EARenameNode(NewID.c_str(), OldName.c_str());
			point->AppendRedoAction(RedoAction);
			point->AppendUndoAction(UndoAction);
//			UndoSelClass = ID.c_str();
			RedoSelClass = NewID.c_str();
		}			
	}
	else if(MenuID == MENU_SET_CLASS_BASE)
	{
		owner = cl->GetOwner();
		enf_assert(owner);

		DString ID = cl->GetId();
		PCClass* OldBase = cl->GetBase();
		PCClass* NewBase = NULL;
		wxString BaseName = OldBase ? OldBase->GetName() : "";
		DString OldBaseID = OldBase ? OldBase->GetId() : "";

		BaseClassDialog dialog(g_MainFrame, cl);
		dialog.CentreOnParent();
again3:
		dialog.SetBaseName(BaseName);

		if(dialog.ShowModal() == wxID_OK)
		{
			BaseName = dialog.GetBaseName();

			if(BaseName.IsEmpty())
				NewBase = NULL;
			else
			{
				NewBase = owner->FindClass(BaseName.c_str(), true, true);

				if(!NewBase)
				{
					wxString msg = "Invalid base class name ";
					msg += BaseName;

					wxMessageDialog dialog( g_MainFrame, msg, "Bad input", wxICON_INFORMATION | wxOK);
					dialog.CentreOnParent();
					dialog.ShowModal();
					goto again3;
				}
			}

			if(OldBase != NewBase)
			{
				DString BaseID;

				if(NewBase)
					BaseID = NewBase->GetId();

				point = g_editor->CreateHistoryPoint("Set base class");

				RedoAction = ENF_NEW EASetBaseClass(ID.c_str(), BaseID.c_str());
				UndoAction = ENF_NEW EASetBaseClass(ID.c_str(), OldBaseID.c_str());
				point->AppendRedoAction(RedoAction);
				point->AppendUndoAction(UndoAction);
			}
		}
	}
	else if(MenuID == MENU_SET_CLASS_EXTERN || MenuID == MENU_SET_CLASS_NOT_EXTERN || MenuID == MENU_SET_CLASS_NOT_EXTERN_AS_FORWARD_DECL)
	{
		owner = cl->GetOwner();
		enf_assert(owner);
		OwnerName = owner->GetName();
		bool MakeExtern = (MenuID == MENU_SET_CLASS_EXTERN);

		point = g_editor->CreateHistoryPoint("Overload class");

		for(uint n = 0; n < selection.GetCardinality(); n++)
		{
			PCClass* sel = selection[n];
			DString ID = sel->GetId();

			RedoAction = ENF_NEW EASetNodeExtern(ID.c_str(), MakeExtern);
			UndoAction = ENF_NEW EASetNodeExtern(ID.c_str(), !MakeExtern);
			point->AppendRedoAction(RedoAction);
			point->AppendUndoAction(UndoAction);

			if(MenuID == MENU_SET_CLASS_NOT_EXTERN_AS_FORWARD_DECL)
			{
				point->AppendRedoAction(ENF_NEW EASetClassDefined(ID.c_str(), false));
				point->AppendUndoAction(ENF_NEW EASetClassDefined(ID.c_str(), true));
			}
		}
	}
	else if(MenuID == MENU_SET_CLASS_EXTERN_WITH_MEMBERS || MenuID == MENU_SET_CLASS_NOT_EXTERN_WITH_MEMBERS)
	{
		owner = cl->GetOwner();
		enf_assert(owner);
		OwnerName = owner->GetName();
		bool MakeExtern = (MenuID == MENU_SET_CLASS_EXTERN_WITH_MEMBERS);

		point = g_editor->CreateHistoryPoint("Overload class");

		for(uint n = 0; n < selection.GetCardinality(); n++)
		{
			PCClass* sel = selection[n];
			DString ID = sel->GetId();
			RedoAction = ENF_NEW EASetNodeExtern(ID.c_str(), MakeExtern);
			UndoAction = ENF_NEW EASetNodeExtern(ID.c_str(), !MakeExtern);
			point->AppendRedoAction(RedoAction);
			point->AppendUndoAction(UndoAction);

			for(uint k = 0; k < sel->GetNodesCount(); k++)
			{
				PCNode* node = sel->GetNode(k);

				if(node->ToClass())
					continue;

				if(node->IsExtern() == MakeExtern)
					continue;

				DString ID = node->GetId();
				RedoAction = ENF_NEW EASetNodeExtern(ID.c_str(), MakeExtern);
				UndoAction = ENF_NEW EASetNodeExtern(ID.c_str(), !MakeExtern);
				point->AppendRedoAction(RedoAction);
				point->AppendUndoAction(UndoAction);
			}
		}
	}
	else if(MenuID == MENU_SET_CLASS_DELETED || MenuID == MENU_SET_CLASS_NOT_DELETED)
	{
		owner = cl->GetOwner();
		enf_assert(owner);
		OwnerName = owner->GetName();
		bool MakeDeleted = (MenuID == MENU_SET_CLASS_DELETED);

		point = g_editor->CreateHistoryPoint("Make class deleted");

		for(uint n = 0; n < selection.GetCardinality(); n++)
		{
			PCClass* sel = selection[n];
			DString ID = sel->GetId();
			RedoAction = ENF_NEW EASetClassDeleted(ID.c_str(), MakeDeleted);
			UndoAction = ENF_NEW EASetClassDeleted(ID.c_str(), !MakeDeleted);
			point->AppendRedoAction(RedoAction);
			point->AppendUndoAction(UndoAction);
		}
	}
	else if(MenuID == MENU_SET_CLASS_DEFINED)
	{
		owner = cl->GetOwner();
		enf_assert(owner);
		OwnerName = owner->GetName();

		point = g_editor->CreateHistoryPoint("Make class defined");

		for(uint n = 0; n < selection.GetCardinality(); n++)
		{
			PCClass* sel = selection[n];
			DString ID = sel->GetId();
			RedoAction = ENF_NEW EASetClassDefined(ID.c_str(), true);
			UndoAction = ENF_NEW EASetClassDefined(ID.c_str(), false);
			point->AppendRedoAction(RedoAction);
			point->AppendUndoAction(UndoAction);
		}
	}

	if(point && RedoAction && UndoAction)
	{
		EArray<DString> ClassRedoSel;
		EArray<DString> ClassUndoSel;

		if(!RedoSelClass.IsEmpty())
			ClassRedoSel.Insert(RedoSelClass.c_str());
		else
			g_editor->GetSelection(ClassRedoSel);

		g_editor->GetSelection(ClassUndoSel);

		point->SetRedoClassSel(ClassRedoSel);
		point->SetUndoClassSel(ClassUndoSel);

		DString PropSel = g_editor->GetPropertySelectionID();
		point->SetRedoPropSel(PropSel.c_str());
		point->SetUndoPropSel(PropSel.c_str());

		g_editor->AddHistoryPoint(point);

		Freeze();

		SetSkipSelEvent(true);

		g_editor->RealizeActions(point->GetRedoActions());

		SetSkipSelEvent(false);
		g_editor->UpdateUndoRedoAccessibility();

		g_editor->Select(point->GetRedoClassSel(), false);	//after calling RealizeActions!
//		g_editor->UpdateSelectionGui();
//		EnsureSelectionVisible();

		if(MenuID == MENU_INSERT_CLASS || MenuID == MENU_INSERT_INNER_CLASS)
			g_editor->OnClassesCreatedFromGui();
		else if(MenuID == MENU_DELETE_CLASS)
			g_editor->OnClassesDeletedFromGui();
		else if(MenuID == MENU_RENAME_CLASS)
			g_editor->OnClassRenamedFromGui();

		g_editor->GetMainFrame()->GetPropGrid()->Refresh(false);

		Thaw();
	}
	else
	{
		SAFE_DELETE(point);
	}
}

//----------------------------------------------------------------------
void ParamFileTree::ShowBaseClasses(bool show)
{
	SetColumnShown(1, show);
	long style = GetWindowStyle();

	if(show != ((style & wxTR_FULL_ROW_HIGHLIGHT) != 0))
		style = style ^ wxTR_FULL_ROW_HIGHLIGHT;

	SetWindowStyle (style);
}