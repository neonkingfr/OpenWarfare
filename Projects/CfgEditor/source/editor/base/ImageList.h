
#ifndef IMAGE_LIST_H
#define IMAGE_LIST_H

#include "wx/imaglist.h"

extern int BI_NEW;
extern int BI_OPEN;
extern int BI_OPEN_AS_UPDATE;
extern int BI_SAVE;
extern int BI_UNDO;
extern int BI_REDO;
extern int BI_COPY;
extern int BI_PASTE;
extern int BI_PARAM_FILE;
extern int BI_PARAM_CLASS;
extern int BI_PARAM_CLASS_READ_ONLY;
extern int BI_PARAM_CLASS_DECL;
extern int BI_PARAM_CLASS_DECL_READ_ONLY;
extern int BI_PARAM_CLASS_DELETED;
extern int BI_PARAM_VARIABLE;
extern int BI_PARAM_VARIABLE_READ_ONLY;
extern int BI_PARAM_ARRAY;
extern int BI_PARAM_ARRAY_READ_ONLY;
extern int BI_ADD;
extern int BI_DELETE;
extern int BI_UP;
extern int BI_DOWN;
extern int BI_SHOW_BASE_CLASS;
extern int BI_EXPAND;
extern int BI_COLLAPSE;

//================================================================================
//shared image list for common image resources
//================================================================================
class ImageList : public wxImageList
{
public:
				ImageList(int width, int height, const bool mask = true, int initialCount = 1);
				~ImageList();
	wxSize	GetBitmapSize(){return wxSize(16, 16);}
};

extern ImageList* g_ImageList;

#endif