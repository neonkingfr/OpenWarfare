
#include "precomp.h"
#include "base/GenericEditor.h"
#include "base/EditActions.h"
#include "base/MainFrame.h"
#include "base/ParamFileTree.h"
#include "base/PropertiesPanel.h"
#include "base/IDSet.h"
#include "Common/XMLConfig.h"
#include "Common/WinApiUtil.h"
#include "SequenceLoadDialog.h"



GenericEditor* g_editor = NULL;

//-------------------------------------------------------------------------
GenericEditor::GenericEditor()
{
	g_editor = this;
	PConfig = NULL;
	MFrame = NULL;
	config = NULL;
	AnyConfigChanges = false;
}

//-------------------------------------------------------------------------
GenericEditor::~GenericEditor()
{
	assert(!PConfig);
	assert(!UManager);
	assert(!config);
	g_editor = NULL;
}

//-------------------------------------------------------------------------
CResult GenericEditor::OnAppInit()
{
	config = CreateEditorConfig();	//virtual

	if(config->Load() == false)
	{
		wxMessageDialog dialog(NULL, "Config file load failed. Using default", "Warning", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
	}

	PConfig = CreateParamConfig();	//virtual
	MFrame = CreateMainFrame();		//virtual
	UManager = ENF_NEW UndoManager();
	g_editor->OnAppConfigChanged();
	return New();	//new (unamed) config
}

//-------------------------------------------------------------------------
int GenericEditor::OnAppExit()
{
	SAFE_DELETE(PConfig);
	SAFE_DELETE(UManager);

	config->Save();
	SAFE_DELETE(config);
	return 0;
}

//-------------------------------------------------------------------------
XMLConfig* GenericEditor::CreateEditorConfig()
{
	//vytvorime config
	char AppDir[256];
	taGetAplicationDir(AppDir);

	wxString ConfigFile = AppDir;
	ConfigFile += "/CfgEditorGeneric.xml";

	return ENF_NEW XMLConfig(ConfigFile.c_str(), "AppConfig");
}

//-------------------------------------------------------------------------
ParamConfig* GenericEditor::CreateParamConfig()
{
	return ENF_NEW ParamConfig();
}

//-------------------------------------------------------------------------
MainFrame* GenericEditor::CreateMainFrame()
{
	XMLConfigNode* node = config->GetNode("defaults", true);

	bool HideUnusedExternClasses = false;
	node->GetValue("HideUnusedExternClasses", HideUnusedExternClasses);

	node = node->GetChildNode("MainWindow", true);
	wxPoint pos(-1, -1);
	wxSize size(1024, 768);

	node->GetValue("left", pos.x);
	node->GetValue("top", pos.y);
	node->GetValue("width", size.x);
	node->GetValue("height", size.y);

	if(pos.x < -1)
		pos.x = -1;

	if(pos.y < -1)
		pos.y = -1;

	if(size.x < 640)
		size.x = 640;

	if(size.y < 480)
		size.y = 480;

	MainFrame* frame = new MainFrame();
	frame->Create(NULL, wxID_ANY, "Config Editor", pos, size, wxDEFAULT_FRAME_STYLE, "GenericConfigEditor");
	return frame;
}

//-------------------------------------------------------------------------
bool GenericEditor::AnyChanges()
{
	return AnyConfigChanges;
}

//-------------------------------------------------------------------------
void GenericEditor::SetChanged(bool changed)
{
	AnyConfigChanges = changed;
}

//-------------------------------------------------------------------------
ParamConfig* GenericEditor::GetParamConfig()
{
	return PConfig;
}

//-------------------------------------------------------------------------
MainFrame* GenericEditor::GetMainFrame()
{
	return MFrame;
}

//----------------------------------------------------------------------
bool GenericEditor::ReadyToClose()
{
	if(AnyChanges() && !PConfig->IsEmpty())
	{
		wxMessageDialog dialog(MFrame, _T("Save changes before quit?"), _T("Quit"), wxICON_QUESTION | wxYES_NO);
		dialog.CentreOnParent();

		if(dialog.ShowModal() == wxID_YES)
			return Save();
	}

	return true;
}

//-------------------------------------------------------------------------
CResult GenericEditor::Load(EArray<PCFilename>& FileNames)
{
	MFrame->SetCursor(wxCURSOR_WAIT);
	return PConfig->Load(FileNames);
	MFrame->SetCursor(wxNullCursor);
}

//-------------------------------------------------------------------------
CResult GenericEditor::Save(const char* FileName)
{
	MFrame->SetCursor(wxCURSOR_WAIT);
	return PConfig->Save(FileName);
	MFrame->SetCursor(wxNullCursor);
}

//---------------------------------------------------------------------------------------------
CResult GenericEditor::SaveConfigDef()
{
	wxFileDialog SaveDialog(MFrame, _T("Save Config Definition"), _T(""), _T(""), _T("Config definition files (*.xml)|*.xml"), wxSAVE | wxFD_OVERWRITE_PROMPT);
	SaveDialog.SetPath(PConfig->GetFileName().c_str()); 
	SaveDialog.CentreOnParent();
	CResult saved = false;

	if(SaveDialog.ShowModal() == wxID_OK)
	{
		wxString SavePath = SaveDialog.GetPath();
		SavePath.Replace("\\", "/");
		saved = PConfig->GetDefinition()->Save(SavePath.c_str());
	}
	return saved;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::New()
{
	if(AnyChanges() && !PConfig->IsEmpty())
	{
		wxMessageDialog dialog(MFrame, _T("Save changes of current config?"), _T("Save"), wxICON_QUESTION | wxYES_NO);
		dialog.CentreOnParent();

		if(dialog.ShowModal() == wxID_YES)
		{
			if(!Save())
				return false;
		}
	}

	Clear();
	SetChanged(true);
	OnConfigLoaded();
	return true;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::Load()
{
	if(AnyChanges() && !PConfig->IsEmpty())
	{
		wxMessageDialog dialog(MFrame, _T("Save changes before load another config?"), _T("Unsaved data detected"), wxICON_QUESTION | wxYES_NO);
		dialog.CentreOnParent();

		if(dialog.ShowModal() == wxID_YES)
			Save();
	}

	wxFileDialog LoadDialog(MFrame, _T("Load Config"), _T(""), _T(""), _T("Config files (*.cpp;*.bin)|*.cpp;*.bin"), wxOPEN | wxFILE_MUST_EXIST);
	wxString InitPath = PConfig->GetFileName().c_str();
	InitPath = InitPath.BeforeLast('.');		//path must be without extension for correct filedialog functionality (filter switch)
	LoadDialog.SetPath(InitPath); 
	LoadDialog.CentreOnParent();
	CResult result = false;

	if(LoadDialog.ShowModal() == wxID_OK)
	{
		Clear();
		wxString LoadPath = LoadDialog.GetPath();
		LoadPath.Replace("\\", "/");
		wxYield();
		EArray<PCFilename> fnames;
		fnames.Insert(PCFilename(LoadPath.c_str()));
		result = Load(fnames);

//		result = Load(LoadPath.c_str());
	}

	if(result.IsOk())
		OnConfigLoaded();

	if(result.IsCommented())
	{
		wxMessageDialog dialog(MFrame, result.GetComment(), _T("Loading config"), wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
	}

	return result.IsOk();
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::SequenceLoad()
{
	CResult result = false;
	SequenceLoadDialog dialog(MFrame);
	dialog.CentreOnParent();

	if(dialog.ShowModal() == wxID_OK)
	{
		wxArrayString filenames;
		dialog.GetFileNamesToLoad(filenames);
		wxYield();

		if(filenames.GetCount() > 0)
		{
			Clear();
			EArray<PCFilename> fnames;

			for(int n = 0; n < filenames.GetCount(); n++)
			{
				wxString& filename = filenames[n];
				fnames.Insert(PCFilename(filename.c_str()));
			}

			result = Load(fnames);
		}
	}

	if(result.IsOk())
		OnConfigLoaded();

	if(result.IsCommented())
	{
		wxMessageDialog dialog(MFrame, result.GetComment(), _T("Loading config"), wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
	}

	return result.IsOk();
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::Save()
{
	DString path = PConfig->GetSaveFilename();

	if(path.empty() || !taFileExists(path.c_str()))
		return SaveAs();

	return DoSave(path.c_str());
}

//---------------------------------------------------------------------------------------------
wxString GenericEditor::GetSaveDialogFilter()
{
	//optimize format of filedialog filter to current file extension
	wxString CppFilesFilter = "Source config files (*.cpp)|*.cpp";
	wxString BinFilesFilter = "Binary config files (*.bin)|*.bin";
	wxString FileDialogFilter;

	wxString CurFileExtension(PConfig->GetFileName().c_str());
	CurFileExtension = CurFileExtension.AfterLast('.');

	if(CurFileExtension.CmpNoCase("bin") == 0)
		FileDialogFilter = BinFilesFilter + wxString("|") + CppFilesFilter;
	else //if(CurFileExtension.CmpNoCase("cpp") == 0)
		FileDialogFilter = CppFilesFilter + wxString("|") + BinFilesFilter;

	return FileDialogFilter;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::SaveAs()
{
	wxFileDialog SaveDialog(MFrame, "Save Config", "", "", GetSaveDialogFilter(), wxSAVE | wxFD_OVERWRITE_PROMPT);
	wxString InitPath = PConfig->GetFileName().c_str();
	InitPath = InitPath.BeforeLast('.');		//path must be without extension for correct filedialog functionality (filter switch)
	SaveDialog.SetPath(InitPath); 
	SaveDialog.CentreOnParent();
	bool saved = false;

	if(SaveDialog.ShowModal() == wxID_OK)
	{
		wxString SavePath = SaveDialog.GetPath();
		SavePath.Replace("\\", "/");
		wxYield();
		saved = DoSave(SavePath.c_str());

		if(saved)
			UpdateConfigTreeItemName();
	}
	return saved;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoSave(const char* FileName)
{
	CResult result = GenericEditor::Save(FileName);

	if(result.IsOk())
	{
		SetChanged(false);
		MFrame->UpdateTitle();
		UpdateSaveAccesibility();
	}
	else
	{
		wxMessageDialog dialog(MFrame, result.GetComment(), _T("Save Error"), wxICON_ERROR | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
	}
	return result.IsOk();
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::Undo()
{
	bool ret = UManager->Undo();
	UpdateSelectionGui();
	UpdateUndoRedoAccessibility();
	return ret;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::Redo()
{
	bool ret = UManager->Redo();
	UpdateSelectionGui();
	UpdateUndoRedoAccessibility();
	return ret;
}

//---------------------------------------------------------------------------------------------
HistoryPoint* GenericEditor::CreateHistoryPoint(const char* name)
{
	return ENF_NEW HistoryPoint(name);
}

//---------------------------------------------------------------------------------------------
void GenericEditor::AddHistoryPoint(HistoryPoint* point)
{
	assert(point);
	assert(GetUndoManager());

	PropertiesPanel* PropPanel = g_MainFrame->GetPropPanel();
	point->SetUndoProperyGridMode(PropPanel->InOverloadMode());
	point->SetRedoProperyGridMode(PropPanel->InOverloadMode());

	if(GetUndoManager())
		GetUndoManager()->Add(point);

	UpdateUndoRedoAccessibility();
//	UpdateHistoryWindow();
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::RestoreHistoryPoint(HistoryPoint* point, bool undo)
{
	assert(point);
	bool ret;
	PropertiesPanel* PropPanel = MFrame->GetPropPanel();
	WBPropertyGrid* PropGrid = MFrame->GetPropGrid();
	ParamFileTree* tree = MFrame->GetParamTree();
	tree->Freeze();
	PropGrid->Freeze();
	PropGrid->ClearSelection();
	wxString SelPropName;

	if(undo)
	{
		ret = RealizeActions(point->GetUndoActions());							//write to param config
		PropPanel->SetOverloadMode(point->GetUndoProperyGridMode());	//set property edit mode
		g_editor->Select(point->GetUndoClassSel(), false);						//select causes show properties (class content)
//		PropGrid->ClearSelection();
		SelPropName = point->GetUndoPropSel().c_str();
	}
	else	//redo
	{
		ret = RealizeActions(point->GetRedoActions());
		PropPanel->SetOverloadMode(point->GetRedoProperyGridMode());
		g_editor->Select(point->GetRedoClassSel(), false);
		SelPropName = point->GetRedoPropSel().c_str();
	}

	wxPGId SelProp;

	if(!SelPropName.IsEmpty())
	{
		SelProp = PropGrid->FindProperty(SelPropName.c_str());

		if(SelProp.IsOk())
			PropGrid->EnsureVisible(SelProp);	//must be called before PropGrid->Thaw() to prevent flicking
	}

	PropGrid->Thaw();

	if(SelProp.IsOk())
	{
		bool selected = PropGrid->SelectProperty(SelProp, false);	//must be called after PropGrid->Thaw() to prevent flicking
		enf_assert(selected);
	}

//	tree->EnsureSelectionVisible();
	UpdateUndoRedoAccessibility();
	tree->Thaw();
//	PropGrid->Refresh();
	return ret;
}

//---------------------------------------------------------------------------------------------
void GenericEditor::UpdateUndoRedoAccessibility(bool ForceDisable)
{
	bool NextUndoAvailable = ForceDisable ? false : (GetUndoManager()->GetUndoStatesCount() > 0);
	MFrame->GetEditMenu()->Enable(MENU_UNDO, NextUndoAvailable);
	MFrame->GetMainToolBar()->EnableTool(MENU_UNDO, NextUndoAvailable);

	bool NextRedoAvailable = ForceDisable ? false : (GetUndoManager()->GetRedoStatesCount() > 0);
	MFrame->GetEditMenu()->Enable(MENU_REDO, NextRedoAvailable);
	MFrame->GetMainToolBar()->EnableTool(MENU_REDO, NextRedoAvailable);
}

//---------------------------------------------------------------------------------------------
void GenericEditor::UpdateSaveAccesibility()
{
	bool enable = AnyChanges();
	MFrame->GetFileMenu()->Enable(MENU_SAVE, enable);
//	MFrame->GetFileMenu()->Enable(MENU_SAVE_AS, enable);
	MFrame->GetMainToolBar()->EnableTool(MENU_SAVE, enable);
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::RealizeActions(const EArray<EditAction*>& actions)
{
	bool ok = true;

	bool AnyClassCreated = false;
	PCNode* RenamedNode = NULL;

	for(uint n = 0; n < actions.GetCardinality(); n++)
	{
		EditAction* action = actions[n];

		if(!RenamedNode && action->GetType() == EA_RENAME_NODE)	//must be located before RealizeAction()
		{
			RenamedNode = PConfig->FindNode(action->GetId().c_str());
			enf_assert(RenamedNode);
		}

		if(RealizeAction(action) == false)
			ok = false;
		else
		{
			if(!AnyClassCreated && action->GetType() == EA_CREATE_CLASS)
				AnyClassCreated = true;
		}
	}

	if(AnyClassCreated)
	{
		enf_assert(!RenamedNode);
		PConfig->UpdateClassBasePtr(PConfig, true);	//update all pointers to base classes
	}

	if(RenamedNode)	//only single node can be renamed in action strip
	{
		enf_assert(!AnyClassCreated);
		PConfig->UpdateClassBaseID(PConfig, true);	//update all base IDs of base classes

		PCClass* cl = RenamedNode->ToClass();

		if(cl)
		{
			//if class is renamed, rename base items in tree
			ParamFileTree* tree = MFrame->GetParamTree();
			EArray<PCClass*> InheritedClasses;
			PConfig->GetDirectlyInheritedClasses(cl, InheritedClasses);

			for(uint n = 0; n < InheritedClasses.GetCardinality(); n++)
			{
				PCClass* c = InheritedClasses[n];

				if(c->IsHidden())
					continue;

				DString id = c->GetId();
				wxTreeItemId item = tree->GetItem(id.c_str());
				enf_assert(item.IsOk());
				tree->UpdateClassItemDesign(c, item);
			}
		}
	}

	if(ok)
	{
		SetChanged(true);
		MFrame->UpdateTitle();
		UpdateSaveAccesibility();
	}

	return ok;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::RealizeAction(const EditAction* action)
{
	assert(action);

	switch(action->GetType())
	{
	case EA_CREATE_CLASS:		//create class
		return DoRealizeAction((const EACreateClass*)action);
		break;
	case EA_RENAME_NODE:		//rename node name (class, array, variable)
		return DoRealizeAction((const EARenameNode*)action);
		break;
	case EA_SET_BASE_CLASS:		//set base class
		return DoRealizeAction((const EASetBaseClass*)action);
		break;
	case EA_CREATE_VARIABLE:	//create variable
		return DoRealizeAction((const EACreateVariable*)action);
		break;
	case EA_CREATE_ARRAY:	//create array
		return DoRealizeAction((const EACreateArray*)action);
		break;
	case EA_MODIFY_VARIABLE:	//change variable value
		return DoRealizeAction((const EAModifyVariable*)action);
		break;
	case EA_DELETE_NODE:		//delete class, variable or array
		return DoRealizeAction((const EADeleteNode*)action);
		break;
	case EA_SET_NODE_EXTERN:	//set class extern or not. if class is extern, will be not saved (is from extern - not editable config)
		return DoRealizeAction((const EASetNodeExtern*)action);
		break;
	case EA_SET_CLASS_DELETED:	//set extern class deleted or not. this causes add/remove "delete ClassName" command to begin of not extern owner body
		return DoRealizeAction((const EASetClassDeleted*)action);
		break;
	case EA_SET_CLASS_DEFINED:	//set class as defined or not. normal class or forward declaration only
		return DoRealizeAction((const EASetClassDefined*)action);
		break;
	default:
		assert(false);
	}
	return false;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EACreateClass* action)
{
	const char* id = action->GetId().c_str();
	const char* OwnerID = action->GetOwnerID().c_str();
	const char* BaseID = action->GetBaseId().c_str();

#ifdef _DEBUG
	PCNode* node = PConfig->FindNode(id);
	enf_assert(!node);
#endif

	PCNode* OwnerNode = PConfig->FindNode(OwnerID);
	assert(OwnerNode);
	PCNode* BaseNode = action->GetBaseId().empty() ? NULL : PConfig->FindNode(BaseID);

	PCClass* OwnerClass = OwnerNode->ToClass();
	assert(OwnerClass);
	PCClass* BaseClass = BaseNode ? BaseNode->ToClass() : NULL;

	uint PosInOwner = action->GetPosInOwner();
	ParamConfigDef* PConfigDef = PConfig->GetDefinition();
	enf_assert(PConfigDef);
	const PCDefClass* def = PConfigDef->GetClassDef(id);

	PCClass* StorageCopy = action->GetStorage()->Clone(OwnerClass);
	StorageCopy->SetDefinition(def);

	OwnerClass->Insert(StorageCopy, PosInOwner);
	PConfig->UpdateClassBasePtr(StorageCopy, false);

	if(!StorageCopy->IsHidden())
	{
		uint PosInOwnerVirt = StorageCopy->GetPosInOwner(true);

		wxTreeItemId item = MFrame->GetParamTree()->GetItem(id);
		assert(!item.IsOk());
		wxTreeItemId OwnerItem = MFrame->GetParamTree()->GetItem(OwnerID);
		assert(OwnerItem.IsOk());

		MFrame->GetParamTree()->AddClass(StorageCopy, OwnerItem, PosInOwnerVirt);
	}
	return true;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EACreateArray* action)
{
	const char* id = action->GetId().c_str();
#ifdef _DEBUG
	PCNode* node = PConfig->FindNode(id);
	assert(!node);
#endif

	wxString OwnerID(id);

	if(OwnerID.Find('/') > -1)
		OwnerID = OwnerID.BeforeLast('/');
	else
		OwnerID.Clear();

	PCNode* OwnerNode = PConfig->FindNode(OwnerID.c_str());
	assert(OwnerNode);

	PCClass* OwnerClass = OwnerNode->ToClass();
	PCArray* OwnerArray = OwnerNode->ToArray();
	PCArray* arr = action->GetStorage()->Clone(OwnerNode);

	if(OwnerClass)
	{
		const PCDefClass* odef = OwnerClass->GetDefinition();
		enf_assert(odef);
		const PCDefParam* def = odef->GetParamDef(arr->GetName());
		enf_assert(def);
		arr->SetDefinition(def);
	}

	uint PosInOwner = action->GetPosInOwner();

	if(OwnerClass)
//		return OwnerClass->AppendNode(arr);
		return OwnerClass->Insert(arr, PosInOwner);
	else if(OwnerArray)
//		return OwnerArray->Add(arr);
		return OwnerArray->Insert(arr, PosInOwner);

	return false;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EACreateVariable* action)
{
	const char* id = action->GetId().c_str();
#ifdef _DEBUG
	PCNode* node = PConfig->FindNode(id);
	assert(!node);
#endif

	wxString OwnerID(id);

	if(OwnerID.Find('/') > -1)
		OwnerID = OwnerID.BeforeLast('/');
	else
		OwnerID.Clear();

	PCNode* OwnerNode = PConfig->FindNode(OwnerID.c_str());
	assert(OwnerNode);

	PCClass* OwnerClass = OwnerNode->ToClass();
	PCArray* OwnerArray = OwnerNode->ToArray();
	const PCDefParam* def = NULL;

	if(OwnerClass)	//ked je owner pole tak definition nema
	{
		const PCDefClass* OwnerDef = OwnerClass->GetDefinition();
		enf_assert(OwnerDef);
		def = OwnerDef->GetParamDef(action->GetName());
		enf_assert(def);
	}

	PCVar* var = PConfig->CreateVariable(action->GetName(), OwnerNode, def);
	var->SetValue(action->GetValue());

	uint PosInOwner = action->GetPosInOwner();

	if(OwnerClass)
//		return OwnerClass->AppendNode(var);
		return OwnerClass->Insert(var, PosInOwner);
	else if(OwnerArray)
//		return OwnerArray->Add(var);
		return OwnerArray->Insert(var, PosInOwner);

	return false;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EADeleteNode* action)
{
	const DString& id = action->GetId();
	PCNode* node = PConfig->FindNode(id.c_str());
	enf_assert(node);

	PCNode* OwnerNode = node->GetOwner();
	PCClass* OwnerClass = OwnerNode->ToClass();
	PCArray* OwnerArray = OwnerNode->ToArray();

	if(OwnerClass)
	{
		if(OwnerClass->RemoveNode(node))
		{
			PCClass* cl = node->ToClass();

			if(cl && !cl->IsHidden())
			{
				bool removed = MFrame->GetParamTree()->RemoveClass(cl);
				enf_assert(removed);
			}

			SAFE_DELETE(node);
			return true;
		}
	}

	if(OwnerArray)
	{
		if(OwnerArray->Remove(node))
		{
			SAFE_DELETE(node);
			return true;
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EARenameNode* action)
{
	ParamFileTree* tree = MFrame->GetParamTree();
	const DString& id = action->GetId();
	const DString& NewName = action->GetNewName();
	PCNode* node = PConfig->FindNode(id.c_str());
	enf_assert(node);
	enf_assert(!node->IsInsideArray());	//nodes inside array not have editable name
	DString OldName = node->GetName();
	PCClass* cl = node->ToClass();

	if(cl)
	{
		if(!cl->IsHidden())
			tree->RenameClass(cl, NewName.c_str());	//rename tree item before node is renamed
	}

//	SetBaseID(const char* BaseID)
	node->SetName(NewName.c_str());

	if(cl)
		cl->AfterRename(OldName.c_str());

	return true;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EASetBaseClass* action)
{
	ParamFileTree* tree = MFrame->GetParamTree();
	const DString& id = action->GetId();
	const DString& BaseID = action->GetBaseID();
	PCNode* node = PConfig->FindNode(id.c_str());
	enf_assert(node);
	PCNode* BaseNode = BaseID.empty() ? NULL : PConfig->FindNode(BaseID.c_str());

	if(!node)
		return false;

	PCClass* cl = node->ToClass();
	enf_assert(cl);

	if(!cl)
		return false;

	PCClass* BaseCl = BaseNode ? BaseNode->ToClass() : NULL;

	enf_assert((BaseCl != NULL) == (!BaseID.empty()));
	cl->SetBase(BaseCl);
	cl->SetBaseID(BaseID.c_str());

	if(!cl->IsHidden())
	{
		wxTreeItemId item = MFrame->GetParamTree()->GetItem(id.c_str());
		enf_assert(item.IsOk());

		if(item.IsOk())
			MFrame->GetParamTree()->UpdateClassItemDesign(cl, item);
	}

	return true;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EAModifyVariable* action)
{
	const DString& id = action->GetId();
	const DString& value = action->GetValue();
	PCNode* node = PConfig->FindNode(id.c_str());

	if(!node)
		return false;

	PCVar* variable = node->ToVariable();
	assert(variable);

	if(!variable)
		return false;
  
	variable->SetValue(value.c_str());
	return true;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EASetNodeExtern* action)
{
	const DString& id = action->GetId();
	bool MakeExtern = action->GetExtern();

	PCNode* node = PConfig->FindNode(id.c_str());

	if(!node)
		return false;

	node->SetExtern(MakeExtern);
	PCClass* cl = node->ToClass();

	if(cl && !cl->IsHidden())
	{
		wxTreeItemId item = MFrame->GetParamTree()->GetItem(id.c_str());
		enf_assert(item.IsOk());

		if(item.IsOk())
			MFrame->GetParamTree()->UpdateClassItemDesign(cl, item);
	}
	return true;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EASetClassDeleted* action)
{
	const DString& id = action->GetId();
	bool MakeDeleted = action->GetDeleted();

	PCNode* node = PConfig->FindNode(id.c_str());

	if(!node)
		return false;

	PCClass* cl = node->ToClass();
	enf_assert(cl);

	cl->SetDeleted(MakeDeleted);

	if(!cl->IsHidden())
	{
		wxTreeItemId item = MFrame->GetParamTree()->GetItem(id.c_str());
		enf_assert(item.IsOk());

		if(item.IsOk())
			MFrame->GetParamTree()->UpdateClassItemDesign(cl, item);
	}

	return true;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::DoRealizeAction(const EASetClassDefined* action)
{
	const DString& id = action->GetId();
	bool MakeDefined = action->GetDefined();

	PCNode* node = PConfig->FindNode(id.c_str());

	if(!node)
		return false;

	PCClass* cl = node->ToClass();
	enf_assert(cl);

	cl->SetDefined(MakeDefined);

	if(!cl->IsHidden())
	{
		wxTreeItemId item = MFrame->GetParamTree()->GetItem(id.c_str());
		enf_assert(item.IsOk());

		if(item.IsOk())
			MFrame->GetParamTree()->UpdateClassItemDesign(cl, item);
	}

	return true;
}

//---------------------------------------------------------------------------------------------
void GenericEditor::Clear()
{
	UManager->Clear();
	MFrame->GetParamTree()->Clear();
	MFrame->GetPropPanel()->Clear();
	GetParamConfig()->Clear();
}
/*
//---------------------------------------------------------------------------------------------
void GenericEditor::ShowUnneededMembersFrom(PCClass* parent)
{
	for(uint n = 0; n < parent->GetNodesCount(); n++)
	{
		PCNode* node = PConfig->GetNode(n);
		PCClass* cl = node->ToClass();

		if(!cl)
			continue;

		if(cl->IsExtern())
			continue;

		MakeUnneededChildsHidden(cl);
	}
}*/

//---------------------------------------------------------------------------------------------
void GenericEditor::GetAllClassesFrom(const PCClass* owner, EArray<PCClass*>& result)
{
	for(uint n = 0; n < owner->GetNodesCount(); n++)
	{
		PCNode* node = owner->GetNode(n);
		PCClass* cl = node->ToClass();

		if(!cl)
			continue;

		result.Insert(cl);
		GetAllClassesFrom(cl, result);
	}
}

//---------------------------------------------------------------------------------------------
void GenericEditor::MarkUnusedMembersFrom(PCClass* parent)
{
	EArray<PCClass*> classes;
	GetAllClassesFrom(parent, classes);

	for(uint n = 0; n < classes.GetCardinality(); n++)
	{
		classes[n]->MarkFlags = 0;
	}

	for(uint n = 0; n < classes.GetCardinality(); n++)
	{
		PCClass* cl = classes[n];

		if(cl->IsExtern())
			continue;

		PCClass* b = cl->GetBase();

		while(b)
		{
			if(b->IsExtern())
				b->MarkFlags = 1;	//mark extern, but inherited by not extern class

			b = b->GetBase();
		}
	}

	for(uint n = 0; n < classes.GetCardinality(); n++)
	{
		PCClass* cl = classes[n];

		if(!cl->IsExtern())
			continue;

		if(cl->MarkFlags == 0)
			cl->SetUnused(true);
		else
			cl->SetUnused(false);

		cl->MarkFlags = 0;
	}
}

//---------------------------------------------------------------------------------------------
void GenericEditor::PrepareConfigToEdit()
{
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

	bool HideUnusedExternClasses = false;
	node->GetValue("HideUnusedExternClasses", HideUnusedExternClasses);

	if(HideUnusedExternClasses)
		MarkUnusedMembersFrom(PConfig);
}

//---------------------------------------------------------------------------------------------
void GenericEditor::OnConfigLoaded()
{
	PrepareConfigToEdit();	//make extern nodes read only and more...

	ParamFileTree* tree = MFrame->GetParamTree();
	tree->Freeze();
	tree->AddClass(PConfig, wxTreeItemId());

	wxTreeItemId root = tree->GetRootItem();

	if(!tree->IsExpanded(root))
		tree->Expand(root);

	tree->Thaw();
//	tree->Refresh(true);

	Select(PConfig, false);
	SetChanged(false);
	MFrame->UpdateTitle();
	UpdateUndoRedoAccessibility(true);
	UpdateSaveAccesibility();
	WBPropertyGrid* PropGrid = GetMainFrame()->GetPropPanel()->GetPropGrid();
	PropGrid->Refresh(false);
	MFrame->GetFileMenu()->Enable(MENU_SAVE_AS, true);
	MFrame->GetFileMenu()->Enable(MENU_GENERATE_DEF_FILE, true);
//	MFrame->GetAUIManager()->Update();
	tree->RedrawScrollbars();	//because of bug in tree
}

//---------------------------------------------------------------------------------------------
EArray<PCClass*>&	GenericEditor::GetSelection()
{
	return selection;
}

//---------------------------------------------------------------------------------------------
void GenericEditor::GetSelection(EArray<DString>& selection)
{
	selection.Clear();

	for(uint n = 0; n < this->selection.GetCardinality(); n++)
	{
		PCClass* cl = this->selection[n];
		DString id = cl->GetId();
		selection.Insert(id.c_str());
	}
}

//---------------------------------------------------------------------------------------------
PCClass* GenericEditor::GetMainSelection()
{
	uint count = selection.GetCardinality();
	if(count == 0)
		return NULL;

	return selection[count - 1];	//posledny je main selection
}

//---------------------------------------------------------------------------------------------
DString GenericEditor::GetPropertySelectionID()
{
	WBPropertyGrid* PropGrid = GetMainFrame()->GetPropPanel()->GetPropGrid();

	wxString SelPropName;
	wxPGId SelProp = PropGrid->GetSelection();

	if(SelProp.IsOk())
		SelPropName = PropGrid->GetPropertyName(SelProp);

	return SelPropName.c_str();
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::IsSelected(PCClass* cl)
{
	assert(cl);
	return (selection.GetIndexOf(cl) != INDEX_NOT_FOUND);
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::IsSelected(const char* id)
{
	PCClass* cl = PConfig->FindClassNode(id);
	return (cl != NULL) ? IsSelected(cl) : false;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::AddSelection(PCClass* cl, bool FromTree)
{
	assert(cl);
	assert(!IsSelected(cl));
	bool res = (selection.Insert(cl) != INDEX_NOT_FOUND);

	if(res)
		OnSelectionChanged(FromTree);

	return res;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::AddSelection(const char* id, bool FromTree)
{
	PCClass* cl = PConfig->FindClassNode(id);
	return (cl != NULL) ? AddSelection(cl, FromTree) : false;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::Select(PCClass* cl, bool FromTree)
{
	assert(cl);
	selection.Clear();
	bool res = (selection.Insert(cl) != INDEX_NOT_FOUND);
	OnSelectionChanged(FromTree);
	return res;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::Select(const EArray<DString>& set, bool FromTree)
{
	EArray<PCClass*> classes;
	bool res = true;

	for(uint n = 0; n < set.GetCardinality(); n++)
	{
		const DString& id = set[n];
		PCClass* cl = PConfig->FindClassNode(id.c_str());

		if(cl)
			classes.Insert(cl);
		else
			res = false;
	}
	bool SelRes = Select(classes, FromTree);
	return (res && SelRes);
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::Select(const EArray<PCClass*>& classes, bool FromTree)
{
	selection.Clear();
	bool res = true;

	for(uint n = 0; n < classes.GetCardinality(); n++)
	{
		PCClass* cl = classes[n];

		bool inserted = (selection.Insert(cl) != INDEX_NOT_FOUND);

		if(!inserted)
			res = false;
	}

	OnSelectionChanged(FromTree);
	return res;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::Select(const char* id, bool FromTree)
{
	PCClass* cl = PConfig->FindClassNode(id);

	if(!cl)
		return false;

	return Select(cl, FromTree);
}
/*
//---------------------------------------------------------------------------------------------
bool GenericEditor::Unselect(PCClass* cl, bool FromTree)
{
	assert(cl);
	bool res = selection.Remove(cl);
	OnSelectionChanged(FromTree);
	return res;
}*/
/*
//---------------------------------------------------------------------------------------------
bool GenericEditor::Unselect(const char* id, bool FromTree)
{
	PCClass* cl = PConfig->FindClassNode(id);

	if(!cl)
		return false;

	return Unselect(cl, FromTree);
}*/

//---------------------------------------------------------------------------------------------
bool GenericEditor::Unselect(const EArray<PCClass*>& classes, bool FromTree)
{
	bool res = true;

	for(uint n = 0; n < classes.GetCardinality(); n++)
	{
		PCClass* cl = classes[n];
		bool removed = selection.Remove(cl);

		if(!removed)
			res = false;
	}

	OnSelectionChanged(FromTree);
	return res;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::Unselect(const EArray<DString>& set, bool FromTree)
{
	EArray<PCClass*> classes;
	bool res = true;

	for(uint n = 0; n < set.GetCardinality(); n++)
	{
		const DString& id = set[n];
		PCClass* cl = PConfig->FindClassNode(id.c_str());

		if(cl)
			classes.Insert(cl);
		else
			res = false;
	}
	bool SelRes = Unselect(classes, FromTree);
	return (res && SelRes);
}

//---------------------------------------------------------------------------------------------
void GenericEditor::UnselectAll(bool FromTree)
{
	selection.Clear();
	OnSelectionChanged(FromTree);
}

//---------------------------------------------------------------------------------------------
void GenericEditor::UpdateSelectionGui()
{
	ParamFileTree* tree = MFrame->GetParamTree();
	tree->Freeze();
	wxTreeItemId FocusedItem;
	tree->UnselectAll();
	tree->SetSkipSelEvent(true);
	uint count = selection.GetCardinality();
	uint LastIndex = count - 1;

	for(uint n = 0; n < count; n++)	
	{
		PCClass* sel = selection[n];
		DString id = sel->GetId();
		wxTreeItemId item = tree->GetItem(id.c_str());
		enf_assert(item.IsOk());

		if(item.IsOk())
		{
			tree->SelectItem(item, (wxTreeItemId*)0, false);
//			tree->SelectItem(item, true);

			if(n == LastIndex)
				FocusedItem = item;

			wxTreeItemId parent = tree->GetItemParent(item);

			if(parent.IsOk())
			{
				if(!tree->IsExpanded(parent))
					tree->Expand(parent);
			}
		}
//			tree->ToggleItemSelection(item);
	}

	if(FocusedItem.IsOk())
		tree->EnsureVisible(FocusedItem);

	tree->SetSkipSelEvent(false);
	tree->Thaw();
}

//---------------------------------------------------------------------------------------------
void GenericEditor::OnSelectionChanged(bool FromTree)
{
	PropertiesPanel* PropPanel = g_MainFrame->GetPropPanel();
	ParamFileTree* tree = MFrame->GetParamTree();

	if(!FromTree)
	{
		UpdateSelectionGui();
//		tree->RedrawScrollbars();
	}

	PropPanel->ShowSelectionProperties();
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::CreateDeleteClassAction(PCClass* cl, EditAction** RedoAction, EditAction** UndoAction)
{
	DString ID = cl->GetId();
/*
	if(!cl->IsExtern() && cl->WasExternOnLoad())	cl->WasExternBeforeLoad() needed
	{
		*RedoAction = ENF_NEW EASetNodeExtern(ID.c_str(), true);
		*UndoAction = ENF_NEW EASetNodeExtern(ID.c_str(), false);
		return true;
	}*/

	PCClass* owner = cl->GetOwner();
	PCClass* base = cl->GetBase();
	DString OwnerID = owner->GetId();
	DString BaseID;

	if(base)
		BaseID = base->GetId();

	uint PosInOwner = cl->GetPosInOwner();

	*RedoAction = ENF_NEW EADeleteNode(ID.c_str());
	*UndoAction = ENF_NEW EACreateClass(ID.c_str(), BaseID.c_str(), OwnerID.c_str(), cl->Clone(NULL), PosInOwner, PConfig->GetClassInfoFlags(owner, base));
	return true;
}

//---------------------------------------------------------------------------------------------
void GenericEditor::UpdateConfigTreeItemName()
{
	ParamFileTree* tree = MFrame->GetParamTree();	//zmenilo sa meno configu tak to zmenime aj v gui
	wxTreeItemId item = tree->GetRootItem();
	enf_assert(item.IsOk());
	tree->SetItemText(item, PConfig->GetName());
}

//---------------------------------------------------------------------------------------------
const char*	GenericEditor::GetAppName()
{
	return "Config editor";
}

//---------------------------------------------------------------------------------------------
void GenericEditor::OnAppConfigChanged()
{
	//apply zmien
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

	wxString WorkingDir;
	node->GetValue("WorkingDir", WorkingDir);

	if(!WorkingDir.IsEmpty() && taFileExists(WorkingDir.c_str()))
		this->SetWorkingDirectory(WorkingDir.c_str());

	wxString RootDataDir;
	node->GetValue("RootDataDir", RootDataDir);

	if(RootDataDir.IsEmpty() || !taFileExists(RootDataDir.c_str()))
		RootDataDir = GetWorkingDirectory();

	GetMainFrame()->GetPropPanel()->GetPropGrid()->SetDataRootDir(RootDataDir);
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::SetWorkingDirectory(const char* dir)
{
	enf_assert(dir);
	enf_assert(taFileExists(dir));
	bool result = ::wxSetWorkingDirectory(dir);
	enf_assert(result);
	return result;
}

//---------------------------------------------------------------------------------------------
wxString GenericEditor::GetWorkingDirectory()
{
	wxString dir = ::wxGetWorkingDirectory();
	dir.Replace("\\", "/");
	return dir;
}

//---------------------------------------------------------------------------------------------
bool GenericEditor::PreProcessMessage(WXMSG* msg)
{
	return false;
}