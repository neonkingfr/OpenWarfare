#ifndef SEQUENCE_LOAD_DIALOG_H
#define SEQUENCE_LOAD_DIALOG_H

class SequenceLoadDialog;

class CheckListBox : public wxCheckListBox
{
public:
	CheckListBox(SequenceLoadDialog* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0);
	int GetVirtScrollPos();
	void EnableMouseEvents(bool enable);
	inline bool MouseEventsEnabled(){return MouseEvents;}
private:
	void OnListBoxLMBDown(wxMouseEvent& event);
	void OnListBoxMouseMove(wxMouseEvent& event);
	SequenceLoadDialog* dialog;
	bool MouseEvents;
	DECLARE_EVENT_TABLE()
};

enum
{
	CHECKLISTBOX_FILES = 100,
	BUTTON_ADD,
	BUTTON_DELETE,
	BUTTON_UP,
	BUTTON_DOWN,
	BUTTON_CHANGE,
};

//========================================================================
class SequenceLoadDialog : public wxDialog
{
	friend class CheckListBox;
public:
	SequenceLoadDialog(wxWindow* parent);
	void GetFileNamesToLoad(wxArrayString& filenames);
private:
	void LoadFromConfig();
	void SaveToConfig();
	CheckListBox* ListBox;
	wxBitmapButton* AddButton;
	wxBitmapButton* DelButton;
	wxBitmapButton* UpButton;
	wxBitmapButton* DownButton;
	wxButton* ChangeButton;
	wxButton* OkButton;
	wxButton* CancelButton;
	int CalculateSelectionFromPoint(wxPoint& point);
	wxString GetPathFromUser();
	void UpdateChangeButtonPos(int ForcedSelection = -1);
	void UpdateAccesibility();
	void OnListBoxLMBDown(wxMouseEvent& event);
	void OnListBoxMouseMove(wxMouseEvent& event);
	void OnButton(wxCommandEvent& event);
	void OnListBoxSelection(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

#endif