#ifndef EDIT_CONTEXT_H
#define EDIT_CONTEXT_H

#include "base/ParamConfig.h"
//#include "base/IDSet.h"
#include "wxExtension/WBPropertyGrid.h"

class ConfigEdApp;
class ConfigEdAnmApp;
class ParamConfig;
class ParamConfigDef;
class GenericEditor;
class MainFrame;
class XMLConfig;
class UndoManager;
class HistoryPoint;
class EditAction;
class EACreateClass;
class EADeleteNode;
class EACreateVariable;
class EACreateArray;
class EAModifyVariable;
class EARenameNode;
class EASetBaseClass;
class EASetNodeExtern;
class EASetClassDeleted;
class EASetClassDefined;

//===============================================================================
//generic config editor main object
//for custom versions of editor is good idea inherit this class and override virtual methods if any different implemetation is needed
//===============================================================================
class GenericEditor
{
	friend class ConfigEdApp;	//friend classes only from GenericEditorBase project. do not add any extern friends! use virtual methods instead
	friend class ConfigEdAnmApp;
	friend class MainFrame;
	friend class PropertiesPanel;
	friend class ParamFileTree;
public:
								GenericEditor();
	virtual					~GenericEditor();
	virtual bool			New();				//"new" user action event
	virtual bool			Load();				//"load" user action event
	virtual bool			SequenceLoad();	//"SequenceLoad" action event
	bool						Save();				//"save" user action event
	bool						SaveAs();			//"save as" user action event
	bool						Undo();				//"undo" user action event
	bool						Redo();				//"redo" user action event
	CResult					SaveConfigDef();	//save config file definition file (not used at this time)
	virtual ParamConfig*	GetParamConfig();	//create "config file" edit object
	virtual MainFrame*	GetMainFrame();	//editor main window pointer
	inline UndoManager*	GetUndoManager(){return UManager;}	//undo/redo mamager
	inline XMLConfig*		GetConfig(){return config;}	//application config pointer
	virtual bool			ReadyToClose();					//called if application is attempt to quit
	bool						AnyChanges();						//returns true if any unsaved data are available
	void						SetChanged(bool changed);		//this forces unsaved data attribute to editor

	EArray<PCClass*>&		GetSelection();						//get selected classes in "Class tree" window
	void						GetSelection(EArray<DString>& selection);	//get IDs of selected classes int "Class tree" window
	PCClass*					GetMainSelection();					//get focused class (selected as main)
	DString					GetPropertySelectionID();			//unique ID of selected property in "Class content" window
	bool						IsSelected(PCClass* cl);			//class is selected or no
	bool						IsSelected(const char* id);		//class is selected or no
	bool						AddSelection(PCClass* cl, bool FromTree);						//add class to selection
	bool						AddSelection(const char* id, bool FromTree);					//add class to selection
	bool						Select(PCClass* cl, bool FromTree);								//select class. previous selection will be unselected
	bool						Select(const char* id, bool FromTree);							//select class. previous selection will be unselected
	bool						Select(const EArray<PCClass*>& classes, bool FromTree);	//select class list. previous selection will be unselected
	bool						Select(const EArray<DString>& set, bool FromTree);			//select class list. previous selection will be unselected
	bool						Unselect(const EArray<PCClass*>& classes, bool FromTree);//unselect class list
	bool						Unselect(const EArray<DString>& set, bool FromTree);		//unselect class list
	void						UnselectAll(bool FromTree);										//unselect all classes
	void						UpdateSelectionGui();												//update class selection treeview + next gui if deppend on class selection
	void						UpdateUndoRedoAccessibility(bool ForceDisable = false);	//enable or disable undo/redo menu and items in main toolbar
	virtual void			UpdateSaveAccesibility();											//same for save menu/tools
	virtual HistoryPoint* CreateHistoryPoint(const char* name);							//create history point class for undo/redo system
	virtual void			AddHistoryPoint(HistoryPoint* point);								//register strip of actions to realize undo a redo later
	virtual bool			RestoreHistoryPoint(HistoryPoint* point, bool undo = false);//History point/state need to restore (called from undo manager)
	virtual bool			RealizeActions(const EArray<EditAction*>& actions);			//realize action strip over ParamConfig class object
	virtual bool			RealizeAction(const EditAction* action);							//realize single action over ParamConfig class object
	bool						DoRealizeAction(const EACreateClass* action);					//realize create class action over ParamConfig class object
	bool						DoRealizeAction(const EACreateVariable* action);				//realize create variable action over ParamConfig class object
	bool						DoRealizeAction(const EACreateArray* action);					//realize create array action over ParamConfig class object
	bool						DoRealizeAction(const EADeleteNode* action);						//realize delete any of class, array or variable. only over ParamConfig class object
	bool						DoRealizeAction(const EARenameNode* action);						//realize rename any of class, array or variable. only over ParamConfig class object
	bool						DoRealizeAction(const EASetBaseClass* action);					//realize change of base class
	virtual bool			DoRealizeAction(const EAModifyVariable* action);				//realize change value action over ParamConfig class object
	bool						DoRealizeAction(const EASetNodeExtern* action);					//set node extern or not. if node is extern, will be not saved (is from extern - not editable config)
	bool						DoRealizeAction(const EASetClassDeleted* action);				//set extern class deleted or not. this causes add/remove "delete ClassName" command to begin of not extern owner body
	bool						DoRealizeAction(const EASetClassDefined* action);				//set class defined or not. makes normal class with body or forward declaration only
	bool						CreateDeleteClassAction(PCClass* cl, EditAction** RedoAction, EditAction** UndoAction);		//helper funtion only
	virtual void			CreateClassRenameFeedbackActions(PCClass* cl, const char* NewName, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions){};
	virtual bool			GetGuiChoices(const char* property, const char* value, wxPGChoices& choices){return false;}		//if for any of property from "Class content" window is defined control with choices but choices are not available, this function is called. this is opportunity to fill choices object with any custom choices
	virtual const char*	GetAppName();	//app name
	virtual bool			PreProcessMessage(WXMSG* msg);	//opportunity to preprocess or skip any windows message
	void						GetAllClassesFrom(const PCClass* owner, EArray<PCClass*>& result);
	bool						SetWorkingDirectory(const char* dir);	//set application working directory
	wxString					GetWorkingDirectory();

								ENF_DECLARE_MMOBJECT(GenericEditor);
protected:
   virtual CResult		OnAppInit();	//application init event
	virtual int				OnAppExit();	//application exit event
	virtual void			OnSelectionChanged(bool FromTree);	//class selection in treeview was changed
	virtual void			OnAppConfigChanged();					//application config was changed (maybe from Options Dialog)
	virtual void			OnShowSelectionProperies(){};			//properties (class content) of selected classes are shown in "Class content window"
	virtual void			OnClassesCreatedFromGui(){};			//one or more classes was created from GUI because of user activity
	virtual void			OnClassesDeletedFromGui(){};			//one or more classes was deleted from GUI because of user activity
	virtual void			OnClassRenamedFromGui(){};				//class was renamed
	virtual XMLConfig*	CreateEditorConfig();					//je potreba vytvorit config pre aplikaciu. moznost zmenit mu minimalne meno pre konkretny editor
	virtual ParamConfig* CreateParamConfig();						//vytvara sa editovany object. moznost vytvorit vlastny - podedeny
	virtual MainFrame*	CreateMainFrame();						//vytvara sa hlavne okno editoru. moznost vytvorit vlastne pozdedene z MainFrame
	wxString					GetSaveDialogFilter();
	void						UpdateConfigTreeItemName();
	void						PrepareConfigToEdit();
	void						MarkUnusedMembersFrom(PCClass* parent);
	bool						DoSave(const char* FileName);			//save process (to native BIS param file format) called from Save() function
	CResult					Load(EArray<PCFilename>& FileNames);	//load from BIS param file
//	CResult					Load(const char* FileName);
	virtual CResult			Save(const char* FileName);			//load BIS param file
	virtual void			Copy(){};									//space to realize copy action in custom version of editor
	virtual void			Paste(){};									//space to realize paste action in custom version of editor
	virtual void			Clear();										//clear editor to init state
	virtual void			OnConfigLoaded();							//config loaded event
	ParamConfig*			PConfig;										//editable object
	MainFrame*				MFrame;										//editor main window
	XMLConfig*				config;										//application config
	UndoManager*			UManager;
	bool						AnyConfigChanges;
	EArray<PCClass*>		selection;
};

extern GenericEditor* g_editor;

#endif