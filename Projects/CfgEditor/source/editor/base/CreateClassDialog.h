
#ifndef CREATE_CLASS_DIALOG_H
#define CREATE_CLASS_DIALOG_H

#include "wx/dialog.h"

class wxCheckBox;
class PCClass;

enum
{
	CHECKBOX_DECL_ONLY = 100,
};

//===========================================================================
//Simple dialog to get new classname and classname to inherit from
//===========================================================================
class CreateClassDialog : public wxDialog
{
public:
	CreateClassDialog(wxWindow* parent, PCClass* SelectedClass, bool CreateInside);
	wxString GetName();
	wxString GetBaseName();
	bool GetDeclarationOnly();
	void SetName(const wxString& name);
	void SetBaseName(const wxString& name);
	void SetDeclarationOnly(bool declaration);
private:
	wxTextCtrl* NameEditBox;
	wxComboBox* BaseNameEditBox;
	wxCheckBox*	DeclOnlyCheck;
	wxButton*	OkButton;
	wxButton*	CancelButton;
	void OnCheckBoxChanged();
	void OnCheckBoxSwitch(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

#endif