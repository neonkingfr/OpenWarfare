
#include "precomp.h"
#include "EditActions.h"
#include "base/GenericEditor.h"

//===============================================================================
EditAction::EditAction(const char* id, int type)
{
	assert(id);
	this->id = id;
	this->type = type;
}

//===============================================================================
EACreateClass::EACreateClass(const char* id, const char* BaseID, const char* OwnerID, PCClass* storage, uint PosInOwner, int ClassInfoFlags)
	:EditAction(id, EA_CREATE_CLASS)
{
	this->BaseID = BaseID;
	this->OwnerID = OwnerID;
	this->storage = storage;
	this->PosInOwner = PosInOwner;
	this->ClassInfoFlags = ClassInfoFlags;
}

//-------------------------------------------------------------------------------
EACreateClass::~EACreateClass()
{
	SAFE_DELETE(storage);
}

//===============================================================================
EACreateArray::EACreateArray(const char* id, PCArray* storage, uint PosInOwner)
	:EditAction(id, EA_CREATE_ARRAY)
{
	assert(storage);
	this->storage = storage;
	this->PosInOwner = PosInOwner;
}

//-------------------------------------------------------------------------------
EACreateArray::~EACreateArray()
{
	SAFE_DELETE(storage);
}

//-------------------------------------------------------------------------------
const char* EACreateArray::GetName() const
{
	return storage->GetName();
}

//===============================================================================
EACreateVariable::EACreateVariable(const char* id, const char* name, const char* value, uint PosInOwner)
	:EditAction(id, EA_CREATE_VARIABLE)
{
	this->name = name;
	this->value = value;
	this->PosInOwner = PosInOwner;
}

//-------------------------------------------------------------------------------
const char* EACreateVariable::GetName() const
{
	return name.c_str();
}

//-------------------------------------------------------------------------------
const char* EACreateVariable::GetValue() const
{
	return value.c_str();
}


//===============================================================================
EADeleteNode::EADeleteNode(const char* id)
	:EditAction(id, EA_DELETE_NODE)
{
}

//===============================================================================
EARenameNode::EARenameNode(const char* id, const char* NewName)
	:EditAction(id, EA_RENAME_NODE)
{
	enf_assert(NewName);
	this->NewName = NewName;
}

//===============================================================================
EASetBaseClass::EASetBaseClass(const char* id, const char* BaseID)
	:EditAction(id, EA_SET_BASE_CLASS)
{
	this->BaseID = BaseID;
}

//===============================================================================
EAModifyVariable::EAModifyVariable(const char* id, const char* value)
	:EditAction(id, EA_MODIFY_VARIABLE)
{
	assert(value);
	this->value = value;
}

//==========================================================================
EASetNodeExtern::EASetNodeExtern(const char* id, bool MakeExtern)
	:EditAction(id, EA_SET_NODE_EXTERN)
{
	this->MakeExtern = MakeExtern;
}

//==========================================================================
EASetClassDeleted::EASetClassDeleted(const char* id, bool MakeDeleted)
	:EditAction(id, EA_SET_CLASS_DELETED)
{
	this->MakeDeleted = MakeDeleted;
}

//==========================================================================
EASetClassDefined::EASetClassDefined(const char* id, bool MakeDefined)
	:EditAction(id, EA_SET_CLASS_DEFINED)
{
	this->MakeDefined = MakeDefined;
}

//==========================================================================
HistoryPoint::HistoryPoint(const char* name)
	:UndoState(0, name)
{
}

//--------------------------------------------------------------------------
HistoryPoint::~HistoryPoint()
{
	for(uint n = 0; n < UndoActions.GetCardinality(); n++)
	{
		EditAction* action = UndoActions[n];
		delete action;
	}
	UndoActions.Clear();

	for(uint n = 0; n < RedoActions.GetCardinality(); n++)
	{
		EditAction* action = RedoActions[n];
		delete action;
	}
	RedoActions.Clear();
}

//--------------------------------------------------------------------------
bool HistoryPoint::AppendRedoAction(EditAction* action)
{
	return (RedoActions.Insert(action) != INDEX_NOT_FOUND);
}

//--------------------------------------------------------------------------
bool HistoryPoint::AppendUndoAction(EditAction* action)
{
	return (UndoActions.Insert(action) != INDEX_NOT_FOUND);
}

//--------------------------------------------------------------------------
bool HistoryPoint::Undo()
{
	bool res = g_editor->RestoreHistoryPoint(this, true);
	assert(res);
	return res;
}

//--------------------------------------------------------------------------
bool HistoryPoint::Redo()
{
	bool res = g_editor->RestoreHistoryPoint(this, false);
	assert(res);
	return res;
}

//--------------------------------------------------------------------------
EArray<EditAction*>& HistoryPoint::GetRedoActions()
{
	return RedoActions;
}

//--------------------------------------------------------------------------
EArray<EditAction*>& HistoryPoint::GetUndoActions()
{
	return UndoActions;
}

//--------------------------------------------------------------------------
const EArray<DString>& HistoryPoint::GetUndoClassSel()
{
	return UndoClassSel;	
}

//--------------------------------------------------------------------------
const EArray<DString>& HistoryPoint::GetRedoClassSel()
{
	return RedoClassSel;	
}

//--------------------------------------------------------------------------
const DString& HistoryPoint::GetUndoPropSel()
{
	return UndoPropSel;
}

//--------------------------------------------------------------------------
const DString& HistoryPoint::GetRedoPropSel()
{
	return RedoPropSel;
}

//--------------------------------------------------------------------------
void HistoryPoint::SetUndoClassSel(const EArray<DString>& sel)
{
	UndoClassSel = sel;
}

//--------------------------------------------------------------------------
void HistoryPoint::SetRedoClassSel(const EArray<DString>& sel)
{
	RedoClassSel = sel;
}

//--------------------------------------------------------------------------
void HistoryPoint::SetUndoPropSel(const char* sel)
{
	UndoPropSel = sel;
}

//--------------------------------------------------------------------------
void HistoryPoint::SetRedoPropSel(const char* sel)
{
	RedoPropSel = sel;
}

//--------------------------------------------------------------------------
void HistoryPoint::SetRedoProperyGridMode(bool OverloadMode)
{
	RedoProperyGridMode = OverloadMode;
}

//--------------------------------------------------------------------------
void HistoryPoint::SetUndoProperyGridMode(bool OverloadMode)
{
	UndoProperyGridMode = OverloadMode;
}

//--------------------------------------------------------------------------
bool HistoryPoint::GetRedoProperyGridMode()
{
	return RedoProperyGridMode;
}

//--------------------------------------------------------------------------
bool HistoryPoint::GetUndoProperyGridMode()
{
	return UndoProperyGridMode;
}