
#include "precomp.h"
#include "ImageList.h"

#include "bitmaps/new.xpm"
#include "bitmaps/open.xpm"
#include "bitmaps/OpenAsUpdate.xpm"
#include "bitmaps/save.xpm"
#include "bitmaps/undo.xpm"
#include "bitmaps/redo.xpm"
#include "bitmaps/copy.xpm"
#include "bitmaps/paste.xpm"
#include "bitmaps/ParamFile.xpm"
#include "bitmaps/ParamClass.xpm"
#include "bitmaps/ParamClassReadOnly.xpm"
#include "bitmaps/ParamClassDecl.xpm"
#include "bitmaps/ParamClassDeclReadOnly.xpm"
#include "bitmaps/ParamClassDeleted.xpm"
#include "bitmaps/ParamVariable.xpm"
#include "bitmaps/ParamVariableReadOnly.xpm"
#include "bitmaps/ParamArray.xpm"
#include "bitmaps/ParamArrayReadOnly.xpm"
#include "bitmaps/add.xpm"
#include "bitmaps/delete.xpm"
#include "bitmaps/up.xpm"
#include "bitmaps/down.xpm"
#include "bitmaps/ShowBaseClass.xpm"
#include "bitmaps/expand.xpm"
#include "bitmaps/collapse.xpm"

int BI_NEW = -1;
int BI_OPEN = -1;
int BI_OPEN_AS_UPDATE = -1;
int BI_SAVE = -1;
int BI_UNDO = -1;
int BI_REDO = -1;
int BI_COPY = -1;
int BI_PASTE = -1;
int BI_PARAM_FILE = -1;
int BI_PARAM_CLASS = -1;
int BI_PARAM_CLASS_READ_ONLY = -1;
int BI_PARAM_CLASS_DECL = -1;
int BI_PARAM_CLASS_DECL_READ_ONLY = -1;
int BI_PARAM_CLASS_DELETED = -1;
int BI_PARAM_VARIABLE = -1;
int BI_PARAM_VARIABLE_READ_ONLY = -1;
int BI_PARAM_ARRAY = -1;
int BI_PARAM_ARRAY_READ_ONLY = -1;
int BI_ADD = -1;
int BI_DELETE = -1;
int BI_UP = -1;
int BI_DOWN = -1;
int BI_SHOW_BASE_CLASS = -1;
int BI_EXPAND = -1;
int BI_COLLAPSE = -1;

ImageList* g_ImageList = NULL;

//--------------------------------------------------------------------------------------------------
ImageList::ImageList(int width, int height, const bool mask, int initialCount)
	:wxImageList(width, height, mask, initialCount)
{
	assert(g_ImageList == NULL);
	g_ImageList = this;

	BI_NEW = Add(wxBitmap(new_xpm));
	BI_OPEN = Add(wxBitmap(open_xpm));
	BI_OPEN_AS_UPDATE = Add(wxBitmap(OpenAsUpdate_xpm));
	BI_SAVE = Add(wxBitmap(save_xpm));
	BI_UNDO = Add(wxBitmap(undo_xpm));
	BI_REDO = Add(wxBitmap(redo_xpm));
	BI_COPY = Add(wxBitmap(copy_xpm));
	BI_PASTE = Add(wxBitmap(paste_xpm));
	BI_PARAM_FILE = Add(wxBitmap(ParamFile_xpm));
	BI_PARAM_CLASS = Add(wxBitmap(ParamClass_xpm));
	BI_PARAM_CLASS_READ_ONLY = Add(wxBitmap(ParamClassReadOnly_xpm));
	BI_PARAM_CLASS_DECL = Add(wxBitmap(ParamClassDecl_xpm));
	BI_PARAM_CLASS_DECL_READ_ONLY = Add(wxBitmap(ParamClassDeclReadOnly_xpm));
	BI_PARAM_CLASS_DELETED = Add(wxBitmap(ParamClassDeleted_xpm));
	BI_PARAM_VARIABLE = Add(wxBitmap(ParamVariable_xpm));
	BI_PARAM_VARIABLE_READ_ONLY = Add(wxBitmap(ParamVariableReadOnly_xpm));
	BI_PARAM_ARRAY = Add(wxBitmap(ParamArray_xpm));
	BI_PARAM_ARRAY_READ_ONLY = Add(wxBitmap(ParamArrayReadOnly_xpm));
	BI_ADD = Add(wxBitmap(Add_xpm));
	BI_DELETE = Add(wxBitmap(Delete_xpm));
	BI_UP = Add(wxBitmap(Up_xpm));
	BI_DOWN = Add(wxBitmap(Down_xpm));
	BI_SHOW_BASE_CLASS = Add(wxBitmap(ShowBaseClass_xpm));
	BI_EXPAND = Add(wxBitmap(expand_xpm));
	BI_COLLAPSE = Add(wxBitmap(collapse_xpm));
}

//--------------------------------------------------------------------------------------------------
ImageList::~ImageList()
{
	assert(g_ImageList);
	g_ImageList = NULL;
}
