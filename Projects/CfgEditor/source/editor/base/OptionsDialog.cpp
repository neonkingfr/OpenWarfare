
#include "precomp.h"
#include "OptionsDialog.h"
#include "base/GenericEditor.h"
#include "base/MainFrame.h"
#include "common/XmlConfig.h"
#include "wxExtension/FileTextBox.h"

//===============================================================================
OptionsDialogPanel::OptionsDialogPanel(OptionsDialog* parent)
	:wxPanel(parent, -1)
{
//	SetBackgroundColour(wxColour(255, 0 , 0));
}

BEGIN_EVENT_TABLE(OptionsDialog, wxDialog)
	EVT_BUTTON(-1, OptionsDialog::OnButtonPress)
END_EVENT_TABLE()

//===============================================================================
OptionsDialog::OptionsDialog(wxWindow* parent)
	:wxDialog(parent, -1, "Options", wxDefaultPosition, wxSize(640, 480))
{
	wxButton* OkButton = new wxButton(this, wxID_OK, _T("Ok"), wxDefaultPosition, wxSize(120, 30));
	wxButton* CancelButton = new wxButton(this, wxID_CANCEL, _T("Cancel"), wxDefaultPosition, wxSize(120, 30));

	WorkingDirFileBox = new FileTextBox(this, FILEBOX_WORKING_DIR, wxDefaultPosition, wxSize(-1, 21), wxTE_PROCESS_ENTER, NULL, FTB_DIALOGTYPE_DIR);
	wxStaticText* WorkingDirFileBoxLabel = new wxStaticText(this, -1, "Working directory (needed to load some config files with relative names inside)", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	RootDirFileBox = new FileTextBox(this, FILEBOX_ROOT_DIR, wxDefaultPosition, wxSize(-1, 21), wxTE_PROCESS_ENTER, NULL, FTB_DIALOGTYPE_DIR);
	wxStaticText* RootDirFileBoxLabel = new wxStaticText(this, -1, "Root directory (using for relative paths in filedialogs)", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	WorkingDirFileBox->button->SetLabel("...");
	RootDirFileBox->button->SetLabel("...");

	HideUnusedExternClasses = new wxCheckBox(this, -1, "hide unused classes from external configs (config reload required)");

	wxBoxSizer* ButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
	ButtonsSizer->Add(OkButton, 0, wxALIGN_TOP | wxALL, 0);
	ButtonsSizer->AddSpacer(20);
	ButtonsSizer->Add(CancelButton, 0, wxALIGN_TOP | wxALL, 0);

	wxBoxSizer* MainSizer = new wxBoxSizer(wxVERTICAL);
	MainSizer->AddSpacer(10);
	MainSizer->Add(WorkingDirFileBoxLabel, 0, wxALIGN_LEFT | wxLEFT, 10);
	MainSizer->Add(WorkingDirFileBox, 0, wxGROW | wxALIGN_RIGHT | wxLEFT | wxRIGHT, 10);
	MainSizer->AddSpacer(10);
	MainSizer->Add(RootDirFileBoxLabel, 0, wxALIGN_LEFT | wxLEFT, 10);
	MainSizer->Add(RootDirFileBox, 0, wxGROW | wxALIGN_RIGHT | wxLEFT | wxRIGHT, 10);
	MainSizer->AddSpacer(10);
	MainSizer->Add(HideUnusedExternClasses, 0, wxGROW | wxALIGN_RIGHT | wxLEFT | wxRIGHT, 10);

	ExternPanel = g_editor->GetMainFrame()->CreateOptionsPanel(this);	

	if(ExternPanel)
		MainSizer->Add(ExternPanel, 1, wxGROW | wxALIGN_RIGHT | wxLEFT | wxRIGHT, 0);
	else
		MainSizer->AddStretchSpacer();

	MainSizer->Add(ButtonsSizer, 0, wxGROW | wxALIGN_RIGHT | wxALL, 10);
	SetSizer(MainSizer);

	ValuesFromConfig();
}

//-------------------------------------------------------------------------------
OptionsDialog::~OptionsDialog()
{
}

//-------------------------------------------------------------------------------
void OptionsDialog::OnButtonPress(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case wxID_OK:
		ValuesToConfig();
		EndModal(wxID_OK);
		break;
	case wxID_CANCEL:
		EndModal(wxID_CANCEL);
		break;
	}
}

//-------------------------------------------------------------------------------
void OptionsDialog::ValuesFromConfig()
{
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

//	if(node->GetValue("WorkingDir", StringValue))
//		WorkingDirFileBox->SetValue(StringValue);
	WorkingDirFileBox->SetValue(g_editor->GetWorkingDirectory());

	wxString StringValue;
	bool BoolValue;

	if(node->GetValue("RootDataDir", StringValue))
		RootDirFileBox->SetValue(StringValue);
	else
		RootDirFileBox->SetValue(WorkingDirFileBox->GetValue());

	if(node->GetValue("HideUnusedExternClasses", BoolValue))
		HideUnusedExternClasses->SetValue(BoolValue);

	if(ExternPanel)
		ExternPanel->ValuesFromConfig();
}

//-------------------------------------------------------------------------------
void OptionsDialog::ValuesToConfig()
{
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);
	node->SetValue("RootDataDir", RootDirFileBox->GetValue());
	node->SetValue("WorkingDir", WorkingDirFileBox->GetValue());
	node->SetValue("HideUnusedExternClasses", HideUnusedExternClasses->GetValue());

	if(ExternPanel)
		ExternPanel->ValuesToConfig();
}