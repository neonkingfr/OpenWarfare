#include "precomp.h"
#include "ParamFileTreeAnm.h"
#include "ParamConfigAnm.h"
#include "AnimEditor.h"
#include "base/PropertiesPanel.h"

/*
#include "base/MainFrame.h"
#include "base/GenericEditor.h"
#include "base/ImageList.h"
#include "base/EditActions.h"
#include "base/CreateClassDialog.h"
#include "base/BaseClassDialog.h"
#include "base/ListMessageDialog.h"
#include "Common/NativeUtil.h"
#include "Common/TextFile.h"
#include "wxExtension/WBPropertyGrid.h"
#include "wx/clipbrd.h"*/

BEGIN_EVENT_TABLE(ParamFileTreeAnm, ParamFileTree)
	EVT_MENU(-1, ParamFileTreeAnm::OnSelectFromMenu)
END_EVENT_TABLE()

//======================================================================
ParamFileTreeAnm::ParamFileTreeAnm(wxWindow* parent, wxWindowID id)
	:ParamFileTree(parent, id)
{
}

//----------------------------------------------------------------------
void ParamFileTreeAnm::OnSelectFromMenu(wxCommandEvent& event)
{
	switch(event.GetId())
	{
		case MENU_GOTO_ACTIONS_CLASS:
		{
			PCClass* MainSel = g_aeditor->GetMainSelection();
			enf_assert(MainSel);

			ASClass* state = dynamic_cast<ASClass*>(MainSel);
			enf_assert(state);
			
			if(state)
			{
				PCClass* ActionsClass = state->GetActionsClass();

				if(ActionsClass && !ActionsClass->IsHidden())
				{
					g_editor->Select(ActionsClass, false);
					PropertiesPanel* PropPanel = g_MainFrame->GetPropPanel();
					PropPanel->GetPropGrid()->Refresh();
				}
			}
			break;
		}
	default:
		event.Skip();
	}
}

//----------------------------------------------------------------------
wxMenu* ParamFileTreeAnm::CreateSelClassMenu()
{
	//create menu from base project and append AEG specific items
	wxMenu* menu = ParamFileTree::CreateSelClassMenu();
	enf_assert(menu);

	PCClass* MainSel = g_aeditor->GetMainSelection();
	enf_assert(MainSel);

	ASClass* state = dynamic_cast<ASClass*>(MainSel);
	
	if(state)
	{
		PCClass* ActionsClass = state->GetActionsClass();

		if(ActionsClass && !ActionsClass->IsHidden())
		{
			if(MainSel->GetBase() && !MainSel->GetBase()->IsHidden())
				menu->Append(MENU_GOTO_ACTIONS_CLASS, _T("actions class"), _T("Select actions class"));
		}
	}
 
	return menu;
}