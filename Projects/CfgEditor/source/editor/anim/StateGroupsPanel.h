
#ifndef STATE_GROUPS_PANEL
#define STATE_GROUPS_PANEL

#include "wx/panel.h"

class wxComboBox;
class wxToolBar;
class ASGroup;
class DiagramWindowPanel;
class ConnectionPanel;

enum
{
	TOOL_ADD_GROUP = 2000,
	TOOL_DELETE_GROUP,
	COMBO_CURRENT_CHARACTER,
	COMBO_CURRENT_GROUP,
};

//================================================================================
//panel with character, current states group and graph/diagram window
//================================================================================
class StateGroupsPanel : public wxPanel
{
public:
	StateGroupsPanel(wxWindow* parent);
	uint AddGroup(const char* group);
	bool RemoveGroup(const char* group);
	void SetCurrentGroup(int group);
	void ClearGroups();
	void UpdateGroupButtonsAccesibility();

	uint AddCharacter(const char* character);
	bool RemoveCharacter(const char* character);
	void SetCurrentCharacter(int character);
	void ClearCharacters();

	inline DiagramWindowPanel* GetDiagramPanel(){return DiagramPanel;}
	inline wxComboBox* GetCharatersCombo(){return CharatersCombo;}
	inline wxComboBox* GetGroupsCombo(){return GroupsCombo;}
private:
	wxComboBox* CharatersCombo;
	wxComboBox* GroupsCombo;
	wxToolBar*	toolbar;
//	ConnectionPanel* ConPanel;
	DiagramWindowPanel* DiagramPanel;
	int GetGroupNameFromUser(wxString& input);
	void OnComboSwitch(wxCommandEvent& event);
	void OnToolClick(wxCommandEvent& event);
	DECLARE_EVENT_TABLE();
};

#endif