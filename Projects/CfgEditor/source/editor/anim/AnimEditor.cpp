
#include "precomp.h"
#include "AnimEditor.h"
#include "StateGroupsPanel.h"
#include "ConnectionPanel.h"
#include "anim/EditActionsAnm.h"
#include "anim/ImageListAnm.h"
#include "anim/StateFindDialog.h"
#include "anim/ActionsPanel.h"
#include "base/ParamFileTree.h"
#include "DiagramWin/DiagramWindow.h"
#include "DiagramWin/DiagramNode.h"
#include "Common/XMLConfig.h"
#include "Common/WinApiUtil.h"
#include "Common/NativeConvert.h"
#include "Common/WxConvert.h"

AnimEditor* g_aeditor = NULL;

//-------------------------------------------------------------------------
AnimEditor::AnimEditor()
	:GenericEditor()
{
	enf_assert(!g_aeditor);
	g_aeditor = this;
	AnimDef = NULL;
	PCI_Connect = INDEX_NOT_FOUND;
	PCI_Interpolate = INDEX_NOT_FOUND;
	PCI_Both = INDEX_NOT_FOUND;
  CfgMovesBasicClassName = "CfgMovesBasic";
  CfgMovesBasicClassID = "file/CfgMovesBasic";
}

//-------------------------------------------------------------------------
AnimEditor::~AnimEditor()
{
	g_aeditor = NULL;
	DestroyCopyData();
}

//-------------------------------------------------------------------------
CResult AnimEditor::OnAppInit()
{
	CResult res = GenericEditor::OnAppInit();

	if(!res.IsOk())
		return res;
	//vytvorit veci

	enf_assert(!AnimDef);
	AnimDef = ENF_NEW AnimConfigDef();

	char AppDir[256];
	taGetAplicationDir(AppDir);
	wxString AnimDefPath(AppDir);
	AnimDefPath += "/animdef.xml";

	if(AnimDef->Load(AnimDefPath.c_str()) == false)
	{
		res = false;
		res.SetComment("Missing animation system definition file animdef.xml \nMust be located at the executable file).");
	}

	if(res == true)
		GetMainFrame()->OnAppInit();

	return res;
}

//-------------------------------------------------------------------------
int AnimEditor::OnAppExit()
{
	//zmazat veci
	SAFE_DELETE(AnimDef);

	return GenericEditor::OnAppExit();
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::Load()
{
	if(AnyChanges() && !GetParamConfig()->IsEmpty())
	{
		wxMessageDialog dialog(MFrame, _T("Save changes before load another config?"), _T("Unsaved data detected"), wxICON_QUESTION | wxYES_NO);
		dialog.CentreOnParent();

		if(dialog.ShowModal() == wxID_YES)
			Save();
	}

	wxFileDialog LoadDialog(MFrame, _T("Load Config"), _T(""), _T(""), _T("XML config files (*.xml)|*.xml"), wxOPEN | wxFILE_MUST_EXIST);
	wxString InitPath = PConfig->GetFileName().c_str();
	InitPath = InitPath.BeforeLast('.');		//path must be without extension for correct filedialog functionality (filter switch)
	LoadDialog.SetPath(InitPath);
	LoadDialog.CentreOnParent();
	CResult result = false;

	if(LoadDialog.ShowModal() == wxID_OK)
	{
		Clear();
		wxString LoadPath = LoadDialog.GetPath();
		LoadPath.Replace("\\", "/");
		wxYield();
		result = Load(LoadPath.c_str());
	}

	if(result.IsOk())
		OnConfigLoaded();

	if(result.IsCommented())
	{
		wxMessageDialog dialog(MFrame, result.GetComment(), _T("Loading config"), wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
	}

	return result.IsOk();
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::Save()
{
	DString fname = PConfig->GetFileName();
	DString path = PConfig->GetSaveFilename();

	//if is imported from another file (path != fname)
	if(path.empty() || (path.cmpi(fname) != 0) || !taFileExists(path.c_str()))
		return SaveAs();

	return DoSave(path.c_str());
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::SaveAs()
{
	wxFileDialog SaveDialog(MFrame, _T("Save Anim Config"), _T(""), _T(""), _T("XML config files (*.xml)|*.xml"), wxSAVE | wxFD_OVERWRITE_PROMPT);
	wxString InitPath = PConfig->GetFileName().c_str();
	InitPath = InitPath.BeforeLast('.');		//path must be without extension for correct filedialog functionality (filter switch)
	SaveDialog.SetPath(InitPath); 
	SaveDialog.CentreOnParent();
	bool saved = false;

	if(SaveDialog.ShowModal() == wxID_OK)
	{
		wxString SavePath = SaveDialog.GetPath();
		SavePath.Replace("\\", "/");
		wxYield();
		saved = DoSave(SavePath.c_str());

		if(saved)
			UpdateConfigTreeItemName();
	}
	return saved;
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::DoSave(const char* FileName)
{
	MFrame->SetCursor(wxCURSOR_WAIT);
	CResult result = Save(FileName);

	//automaticaly export to .cpp format START
	DString OldFilename = PConfig->GetSaveFilename();
	wxString ExportFilename = OldFilename.c_str();
	ExportFilename = ExportFilename.BeforeLast('.');
	ExportFilename += ".cpp";
	GenericEditor::DoSave(ExportFilename.c_str());
	PConfig->SetFileName(OldFilename.c_str());
	//automaticaly export to .cpp format END

	MFrame->SetCursor(wxNullCursor);

	if(result.IsOk())
	{
		SetChanged(false);
		MFrame->UpdateTitle();
		UpdateSaveAccesibility();
	}
	else
	{
		wxMessageDialog dialog(MFrame, result.GetComment(), _T("Save Error"), wxICON_ERROR | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
	}
	return result.IsOk();
}

//-------------------------------------------------------------------------
CResult AnimEditor::Import(const char* FileName)
{
	MFrame->SetCursor(wxCURSOR_WAIT);
	return GetParamConfig()->Import(FileName);
	MFrame->SetCursor(wxNullCursor);
}

//-------------------------------------------------------------------------
wxString	AnimEditor::GetImportDialogFilter()
{
	return GenericEditor::GetSaveDialogFilter();
}

//-------------------------------------------------------------------------
wxString	AnimEditor::GetExportDialogFilter()
{
	return GenericEditor::GetSaveDialogFilter();
}

//-------------------------------------------------------------------------
bool AnimEditor::Import()
{
	if(AnyChanges() && !GetParamConfig()->IsEmpty())
	{
		wxMessageDialog dialog(MFrame, _T("Save changes before importing another config?"), _T("Unsaved data detected"), wxICON_QUESTION | wxYES_NO);
		dialog.CentreOnParent();

		if(dialog.ShowModal() == wxID_YES)
			Save();
	}

	wxFileDialog LoadDialog(MFrame, _T("Import Config"), _T(""), _T(""), GetImportDialogFilter(), wxOPEN | wxFILE_MUST_EXIST);
	wxString InitPath = PConfig->GetFileName().c_str();
	InitPath = InitPath.BeforeLast('.');		//path must be without extension for correct filedialog functionality (filter switch)
	LoadDialog.SetPath(InitPath); 
	LoadDialog.CentreOnParent();
	CResult result = false;

	if(LoadDialog.ShowModal() == wxID_OK)
	{
		Clear();
		wxString LoadPath = LoadDialog.GetPath();
		LoadPath.Replace("\\", "/");
		wxYield();
		result = Import(LoadPath.c_str());
	}

	if(result.IsOk())
		OnConfigLoaded();

	if(result.IsCommented())
	{
		wxMessageDialog dialog(MFrame, result.GetComment(), _T("Importing config"), wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
	}

	SetChanged(true);	//po importe sa meni nazov otvoreneho suboru
	MFrame->UpdateTitle();
	UpdateSaveAccesibility();
	return result.IsOk();
}

//-------------------------------------------------------------------------
bool AnimEditor::Export()
{
	wxFileDialog SaveDialog(MFrame, "Export Config", "", "", GetExportDialogFilter(), wxSAVE | wxFD_OVERWRITE_PROMPT);
	wxString InitPath = PConfig->GetFileName().c_str();
	InitPath = InitPath.BeforeLast('.');
	SaveDialog.SetPath(InitPath);		//path must be without extension for correct filedialog functionality (filter switch)
	SaveDialog.CentreOnParent();
	bool saved = false;

	if(SaveDialog.ShowModal() == wxID_OK)
	{
		DString OldFilename = PConfig->GetSaveFilename();
		wxString SavePath = SaveDialog.GetPath();
		SavePath.Replace("\\", "/");
		wxYield();
		saved = GenericEditor::DoSave(SavePath.c_str());
		PConfig->SetFileName(OldFilename.c_str());
		MFrame->UpdateTitle();
	
		if(saved)
			UpdateConfigTreeItemName();
	}
	return saved;
//	return GenericEditor::SaveAs();
}

//-------------------------------------------------------------------------
CResult AnimEditor::Load(const char* FileName)
{
	MFrame->SetCursor(wxCURSOR_WAIT);
	return GetParamConfig()->Load(FileName);
	MFrame->SetCursor(wxNullCursor);
}

//-------------------------------------------------------------------------
CResult AnimEditor::Save(const char* FileName)
{
	return GetParamConfig()->Save(FileName);
}

//-------------------------------------------------------------------------
bool AnimEditor::New()
{
	if(AnyChanges() && !PConfig->IsEmpty())
	{
		wxMessageDialog dialog(MFrame, _T("Save changes before creating another config?"), _T("Unsaved data detected"), wxICON_QUESTION | wxYES_NO);
		dialog.CentreOnParent();

		if(dialog.ShowModal() == wxID_YES)
			Save();
	}

//	bool res = GenericEditor::New();
	Clear();
	SetChanged(true);
	OnConfigLoaded();
	GetDiagramWindow()->RepaintFromClient();
	GetMainFrame()->GetActionsPanel()->Clear();
	return true;
}

//-------------------------------------------------------------------------
void AnimEditor::Clear()
{
	DestroyCopyData();
	UpdateCopyPasteAccesibility();
	DiagramWindow* DiagWin = GetDiagramWindow();
	DiagWin->Clear();
	DiagWin->RepaintFromClient();
	GetMainFrame()->GetGroupsPanel()->ClearGroups();
	GetMainFrame()->GetGroupsPanel()->ClearCharacters();
	GetMainFrame()->GetActionsPanel()->Clear();

	StateFindDialog* FindDialog = GetMainFrame()->GetStateFindDialog();

	if(FindDialog && FindDialog->IsShown())
		FindDialog->ShowNormal(false);

	GenericEditor::Clear();
}

//-------------------------------------------------------------------------
ParamConfigAnm* AnimEditor::CreateParamConfig()
{
	return ENF_NEW ParamConfigAnm();
}

//-------------------------------------------------------------------------
MainFrameAnm* AnimEditor::CreateMainFrame()
{
	XMLConfigNode* node = config->GetNode("defaults", true);
	node = node->GetChildNode("MainWindow", true);
	wxPoint pos(-1, -1);
	wxSize size(1024, 768);

	node->GetValue("left", pos.x);
	node->GetValue("top", pos.y);
	node->GetValue("width", size.x);
	node->GetValue("height", size.y);

	if(pos.x < -1)
		pos.x = -1;

	if(pos.y < -1)
		pos.y = -1;

	if(size.x < 640)
		size.x = 640;

	if(size.y < 480)
		size.y = 480;

	MainFrameAnm* frame = new MainFrameAnm();
	frame->Create(NULL, wxID_ANY, "Anim Config Editor", pos, size, wxDEFAULT_FRAME_STYLE, "AnimConfigEditor");

//	MFrame->Maximize();
//	wxYield();

	return frame;
}

//-------------------------------------------------------------------------
XMLConfig* AnimEditor::CreateEditorConfig()
{
	char AppDir[256];
	taGetAplicationDir(AppDir);

	wxString ConfigFile = AppDir;
	ConfigFile += "/CfgEditorAnim.xml";

	return ENF_NEW XMLConfig(ConfigFile.c_str(), "AppConfig");
}

//-------------------------------------------------------------------------
ParamConfigAnm* AnimEditor::GetParamConfig()
{
	return (ParamConfigAnm*)PConfig;
}

//-------------------------------------------------------------------------
MainFrameAnm* AnimEditor::GetMainFrame()
{
	return (MainFrameAnm*)MFrame;
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnConfigLoaded()
{
	GenericEditor::OnConfigLoaded();
	PrepareStatesToEdit();	//reorganize all connections to common (one way) direction. this minimizes complexity of editing connections

	UpdateCharacterManagerGuiContent();
	UpdateGroupsManagerGuiContent();		//it show current anim state group of current character in graph window
	OnASGroupChanged();
	MFrame->GetFileMenu()->Enable(MENU_EXPORT, true);
	GetMainFrame()->GetGroupsPanel()->GetCharatersCombo()->Enable(GetMainFrame()->GetGroupsPanel()->GetCharatersCombo()->GetCount() > 0);
	GetMainFrame()->GetGroupsPanel()->GetGroupsCombo()->Enable(GetMainFrame()->GetGroupsPanel()->GetGroupsCombo()->GetCount() > 0);
	UpdateStatesIconsInTree();
	UpdateCopyPasteAccesibility();
	DiagramWindow* DiagWin = GetDiagramWindow();
	DiagWin->Refresh(false);
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnCharacterChanged()
{
	UpdateGroupsManagerGuiContent();
	UpdateCurrentGroupGui();
	GetDiagramWindow()->RepaintFromClient();

	ConnectionPanel* ConnPanel = GetMainFrame()->GetConnectionPanel();
	ConnPanel->UpdateAccesibility(true);
	UpdateCopyPasteAccesibility();

	StateFindDialog* FindDialog = GetMainFrame()->GetStateFindDialog();

	if(FindDialog && FindDialog->IsShown())
		FindDialog->ShowNormal(false);
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnASGroupChanged()
{
	ConnectionPanel* ConnPanel = GetMainFrame()->GetConnectionPanel();
	ConnPanel->UpdateAccesibility(true);
	UpdateCopyPasteAccesibility();
	UpdateCurrentGroupGui();

	StateFindDialog* FindDialog = GetMainFrame()->GetStateFindDialog();

	if(FindDialog && FindDialog->IsShown())
		FindDialog->UpdateResult();
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnSelectionChanged(bool FromTree)
{
	GenericEditor::OnSelectionChanged(FromTree);

	if(FromTree)
	{
		DiagramWindow* DiagWin = GetDiagramWindow();
		DiagWin->ClearSelection();
		ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();

		if(character)
		{
			EArray<DiagramNode*> NodesToSelect;

			for(uint n = 0; n < selection.GetCardinality(); n++)
			{
				PCClass* cl = selection[n];
				ASClass* state = dynamic_cast<ASClass*>(cl);

				if(!state)
					continue;

				if(state->GetCharacter() != character)
					continue;

				DiagramNode* node = DiagWin->FindNode(state->GetName());

				if(!node)
					continue;

				NodesToSelect.Insert(node);
			}

			if(NodesToSelect.GetCardinality() > 0)
				DiagWin->Select(NodesToSelect, false);

			DiagWin->RepaintFromClient();
		}
	}
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnClassesCreatedFromGui()
{
	UpdateCurrentGroupGui();
	GetDiagramWindow()->RepaintFromClient();

	StateFindDialog* FindDialog = GetMainFrame()->GetStateFindDialog();

	if(FindDialog && FindDialog->IsShown())
		FindDialog->UpdateResult();
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnClassesDeletedFromGui()
{
	UpdateCurrentGroupGui();
	GetDiagramWindow()->RepaintFromClient();

	StateFindDialog* FindDialog = GetMainFrame()->GetStateFindDialog();

	if(FindDialog && FindDialog->IsShown())
		FindDialog->UpdateResult();
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnClassRenamedFromGui()
{
	UpdateCurrentGroupGui();
	GetDiagramWindow()->RepaintFromClient();

	StateFindDialog* FindDialog = GetMainFrame()->GetStateFindDialog();

	if(FindDialog && FindDialog->IsShown())
		FindDialog->UpdateResult();
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnShowSelectionProperies()
{
	PCClass* MainSelection = GetMainSelection();
	ASClass* SelectedState = MainSelection ? (dynamic_cast<ASClass*>(MainSelection)) : NULL;
	ActionsPanel* ActPanel = GetMainFrame()->GetActionsPanel();
	ActPanel->ShowActionsOfState(SelectedState);
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::GetGuiChoices(const char* property, const char* value, wxPGChoices& choices)
{
	bool ValueInChoices = false;

	if(strcmpi(property, "actions") == 0)
	{
		ParamConfig* PConfig = GetParamConfig();
		PCNode* cnode = PConfig->FindNode("file/CfgMovesBasic/Actions");
		PCClass* ActionsClass = cnode ? cnode->ToClass() : NULL;

		if(ActionsClass)
		{
			for(uint n = 0 ; n < ActionsClass->GetNodesCount(); n++)
			{
				PCClass* cl = ActionsClass->GetNode(n)->ToClass();

				if(!cl)
					continue;

				const char* ClName = cl->GetName();

				if(!ValueInChoices && strcmpi(ClName, value) == 0)
					ValueInChoices = true;

				choices.Add(ClName);
			}
		}
	}
	return ValueInChoices;
}

//---------------------------------------------------------------------------------------------
void AnimEditor::UpdateGroupsManagerGuiContent()
{
	StateGroupsPanel* GroupsPanel = GetMainFrame()->GetGroupsPanel();
	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();
	GroupsPanel->ClearGroups();

	if(character)
	{
		for(uint n = 0; n < character->GetGroupsCount(); n++)
		{
			ASGroup* group = character->GetGroup(n);
			GroupsPanel->AddGroup(group->GetName().c_str());
		}

		GroupsPanel->SetCurrentGroup(character->GetCurrentGroup());
		wxComboBox* GroupsCombo = GroupsPanel->GetGroupsCombo();
		GroupsCombo->Enable(character->GetGroupsCount() > 0);
	}
}

//---------------------------------------------------------------------------------------------
void AnimEditor::UpdateCharacterManagerGuiContent()
{
	StateGroupsPanel* GroupsPanel = GetMainFrame()->GetGroupsPanel();
	ParamConfigAnm* config = GetParamConfig();
	GroupsPanel->ClearCharacters();

	for(uint n = 0; n < config->GetCharactersCount(); n++)
	{
		ACharacterClass* character = config->GetCharacter(n);
		GroupsPanel->AddCharacter(character->GetName());
	}

	GroupsPanel->SetCurrentCharacter(config->GetCurrentCharacter());
	GroupsPanel->UpdateGroupButtonsAccesibility();
}

class ConnectionInfo
{
public:
	ASClass*			SrcState;
	ASClass*			TargState;
	bool				connect;
	bool				interpolate;

	ConnectionInfo()
	{
		SrcState = NULL;
		TargState = NULL;
		connect = false;
		interpolate = false;
	}

	ConnectionInfo(ASClass* SrcState, ASClass* TargState, bool connect, bool interpolate)
	{
		this->SrcState = SrcState;
		this->TargState = TargState;
		this->connect = connect;
		this->interpolate = interpolate;
	}

	bool operator> (const ConnectionInfo& ci)
	{
		if(SrcState == ci.SrcState)
			return (TargState > ci.TargState);
		
		return (SrcState > ci.SrcState);
	}

	bool operator< (const ConnectionInfo& ci)
	{
		if(SrcState == ci.SrcState)
			return (TargState < ci.TargState);
		
		return (SrcState < ci.SrcState);
	}

	bool operator== (const ConnectionInfo& ci)
	{
		return ((SrcState == ci.SrcState) && (TargState == ci.TargState));
	}
};

//---------------------------------------------------------------------------------------------
//this function recreate content of graph. it depends on current character, current state group of this character,
//state classes referencies to this group and conections betwen these states
//need be called allways after any of these elements were modified, deleted...
//---------------------------------------------------------------------------------------------
bool AnimEditor::UpdateCurrentGroupGui()
{
	DiagramWindow* DiagWin = GetDiagramWindow();
	DiagWin->Clear();

	ParamConfigAnm* config = GetParamConfig();

	ACharacterClass* character = config->GetCurrentCharacterPtr();

	if(!character)
		return false;

	ASGroup* CurGroup = character->GetCurrentGroupPtr();

	if(!CurGroup)
		return false;

	if(CurGroup->GetGraphZoomLevel() != -1)
		DiagWin->SetZoomLevel(CurGroup->GetGraphZoomLevel());

	DiagWin->SetViewPoint(CurGroup->GetGraphViewPoint());

	wxMenu* GraphMenu = GetMainFrame()->GetGraphMenu();
	bool ConnectTo = GraphMenu->IsChecked(MENU_CONNECT_TO);
	bool InterpolateTo = GraphMenu->IsChecked(MENU_INTERPOLATE_TO);

	EArray<ASClass*>& states = character->GetStates();	//vsetky staty z tohto characteru
	EArray<ASClass*> ConnStates;	//odfiltrujeme si iba tie ktore patria do aktualnej grupy na grafe
	EArray<DiagramNode*>	DNodes;

	for(uint n = 0; n < states.GetCardinality(); n++)
	{
		ASClass* state = states[n];

		ASGroupRef* GroupRef = state->FindGroupRef(CurGroup->GetName().c_str());	//iba staty ktore patria do tejto grupy

		if(!GroupRef)
			continue;

		ConnStates.Insert(state);

		DiagramNode* dnode = ENF_NEW DiagramNode(state->GetName());
		DiagWin->AddNode(dnode);
//		FPoint pt = GroupRef->GetGraphPos();

		FSize NodeSize(DiagWin->CalculateNodeVirtualWidth(state->GetName()), 44);
		float HalfHeight = NodeSize.y * 0.5f;
		dnode->SetPos(GroupRef->GetGraphPos());
		dnode->SetSize(NodeSize);
		dnode->SetClientData(state);
		dnode->AddDNDArea(ENF_NEW DNDArea(dnode, DND_AREA_DROP_TARGET, FPoint(0, 0), FSize(NodeSize.y, NodeSize.y), DNDF_DROP, PCI_Selected));
		dnode->AddDNDArea(ENF_NEW DNDArea(dnode, DND_AREA_DRAG_SOURCE, FPoint(NodeSize.x - NodeSize.y, 0), FSize(NodeSize.y, NodeSize.y), DNDF_DRAG, PCI_Selected));

		dnode->AddConnectionSlot(ENF_NEW DCSlot(1, DCST_INPUT, DCSA_LEFT));
		dnode->AddConnectionSlot(ENF_NEW DCSlot(1, DCST_OUTPUT, DCSA_RIGHT));

		DNodes.Insert(dnode);
	}

	int FindFlags = 0;

	if(ConnectTo)
		FindFlags |= CT_CONNECT;

	if(InterpolateTo)
		FindFlags |= CT_INTERPOLATE;

	ESet<ConnectionRef> connections;
	GetAllConnectionsFromStates(ConnStates, FindFlags, character, connections);

	for(uint n = 0; n < connections.GetCardinality(); n++)
	{
		ConnectionRef& con = connections[n];
		DiagramNode* SrcNode = DiagWin->FindNode(con.SrcState->GetName());
		DiagramNode* TargNode = DiagWin->FindNode(con.TargState->GetName());

		if(SrcNode && TargNode)
		{
			uint ConnectionCategory = 0;
			enf_assert(con.connect || con.interpolate);

			if(con.connect)
				ConnectionCategory |= CT_CONNECT;

			if(con.interpolate)
				ConnectionCategory |= CT_INTERPOLATE;

			ConnectionCategory--;
			DiagWin->Connect(SrcNode, 1, TargNode, 0, ConnectionCategory);
		}
	}

	for(uint n = 0; n < DNodes.GetCardinality(); n++)
	{
		DiagramNode* dnode = DNodes[n];
		FSize SlotsSize = dnode->OrganizeSlots();
	}

	UpdateStatesIconsInTree();
	return true;
}

//---------------------------------------------------------------------------------------------
void AnimEditor::UpdateStatesIconsInTree()
{
	ParamConfigAnm* config = GetParamConfig();

	ACharacterClass* character = config->GetCurrentCharacterPtr();

	if(!character)
		return;

	ASGroup* CurGroup = character->GetCurrentGroupPtr();

	DString CharacterClassID = character->GetId();
	DString StatesClassID = CharacterClassID;
	StatesClassID += "/States";
	ParamFileTree* tree = GetMainFrame()->GetParamTree();
	tree->Freeze();

	wxTreeItemId StatesItem = tree->GetItem(StatesClassID.c_str());
//	enf_assert(StatesItem.IsOk());

	if(StatesItem.IsOk())	//nemusi ho mat ked bol vytvoreny character class a states node este nie
	{
		wxTreeItemIdValue cookie;
		wxString ItemName;
		for(wxTreeItemId item = tree->GetFirstChild(StatesItem, cookie); item.IsOk(); item = tree->GetNextSibling(item))
		{
			CTItemData* data = (CTItemData*)tree->GetItemData(item);
			PCClass* StateClass = data->GetClass();
			enf_assert(StateClass);
			ASClass* state = dynamic_cast<ASClass*>(StateClass);
			enf_assert(state);
			bool InGraph = (CurGroup && state->FindGroupRef(CurGroup->GetName().c_str()) != NULL);
			int ImageIndex = InGraph ? BI_STATE_CLASS_ON_GRAPH : BI_STATE_CLASS;
			tree->SetItemImage(item, ImageIndex, wxTreeItemIcon_Normal);
		}
	}

	tree->Thaw();
}

//---------------------------------------------------------------------------------------------
void AnimEditor::GetStatesFromConnectionArray(PCClass* StatesClass, PCArray* arr, EArray<ASClass*>& states)
{
	uint count = arr->GetCardinality();

	for(uint c = 0; c < count; c+=2)
	{
		if((c + 1) >= count)
			break;

		PCNode* item0 = arr->GetItem(c);
		PCNode* item1 = arr->GetItem(c + 1);
		PCVar* StateVar = item0->ToVariable();
		PCVar* SpeedVar = item1->ToVariable();
		enf_assert(StateVar);
		enf_assert(SpeedVar);

		PCNode* TargetStateNode = StatesClass->GetMember(StateVar->GetValue());

		if(TargetStateNode)
		{
			ASClass* TargetState = dynamic_cast<ASClass*>(TargetStateNode);

			if(TargetState)
				states.Insert(TargetState);
		}
	}
}

//---------------------------------------------------------------------------------------------
const char*	AnimEditor::GetAppName()
{
	return "Config editor";
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnStateGraphNodesMoved(const EArray<DiagramNode*>* nodes)
{
	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();
	enf_assert(character);

	ASGroup* group = character->GetCurrentGroupPtr();
	enf_assert(group);

	HistoryPointAnm* point = CreateHistoryPoint("Move Nodes");

	EAModifySGroupRefs* RedoAction = ENF_NEW EAModifySGroupRefs(character->GetName());
	EAModifySGroupRefs* UndoAction = ENF_NEW EAModifySGroupRefs(character->GetName());

	for(uint n = 0; n < nodes->GetCardinality(); n++)
	{
		DiagramNode* node = (*nodes)[n];
		enf_assert(node);
		ASClass* state = (ASClass*)node->GetClientData();
		enf_assert(state);

		ASGroupRef* GroupRef = state->FindGroupRef(group->GetName().c_str());
		enf_assert(GroupRef);

		ASGroupRef ChangedRef(group->GetName().c_str());
		ChangedRef.SetGraphPos(node->GetPos());

		UndoAction->AddStateRefData(state->GetName(), *GroupRef);	//do undo aktualny stav
		RedoAction->AddStateRefData(state->GetName(), ChangedRef);	//do undo aktualny stav
	}

	point->AppendRedoAction(RedoAction);
	point->AppendUndoAction(UndoAction);

	EArray<DString> ClassSel;
	GetSelection(ClassSel);
	point->SetRedoClassSel(ClassSel);
	point->SetUndoClassSel(ClassSel);

	DString SelPropID = GetPropertySelectionID();
	point->SetRedoPropSel(SelPropID.c_str());
	point->SetUndoPropSel(SelPropID.c_str());

	AddHistoryPoint(point);

	RealizeActions(point->GetRedoActions());
	UpdateUndoRedoAccessibility();
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnGraphZoomChanged(uint zoom)
{
	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();
//	enf_assert(character);

	if(character)
	{
		ASGroup* group = character->GetCurrentGroupPtr();

		if(group)
			group->SetGraphZoomLevel(zoom);
	}
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnGraphViewPointChanged(const FPoint& pt)
{
	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();
//	enf_assert(character);

	if(character)
	{
		ASGroup* group = character->GetCurrentGroupPtr();

		if(group)
			group->SetGraphViewPoint(pt);
	}
}

//---------------------------------------------------------------------------------------------
DiagramWindow* AnimEditor::GetDiagramWindow()
{
	return GetMainFrame()->GetGroupsPanel()->GetDiagramPanel()->GetDiagramWin();
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::OnGraphWindowDropStates(wxCoord x, wxCoord y, const wxArrayString& StateIDs)
{
	DiagramWindow* DiagWin = GetDiagramWindow();
	ParamConfigAnm* config = GetParamConfig();
	ACharacterClass* character = config->GetCurrentCharacterPtr();

	if(!character)
		return false;

	ASGroup* group = character->GetCurrentGroupPtr();

	if(!group)
	{
		wxMessageDialog dialog(GetMainFrame(), "You must create new states group first. Use G+ button", "Dropping to graph", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return false;
	}

	FPoint DropPos = DiagWin->ScreenToVirtual(wxPoint(x, y));
	EArray<ASClass*> DroppedStates;
	bool WrongCharacter = false;

	HistoryPointAnm* point = CreateHistoryPoint("Insert Nodes");

	EACreateSGroupRefs* RedoAction = ENF_NEW EACreateSGroupRefs(character->GetName());
	EADeleteSGroupRefs* UndoAction = ENF_NEW EADeleteSGroupRefs(group->GetName().c_str(), character->GetName());

	for(uint n = 0; n < StateIDs.GetCount(); n++)
	{
		const wxString& ID = StateIDs[n];
		PCNode* StateNode = config->FindNode(ID.c_str());
		enf_assert(StateNode);
		ASClass* state = dynamic_cast<ASClass*>(StateNode);
		enf_assert(state);

		if(state->GetCharacter() != character)
		{
			WrongCharacter = true;
			continue;
		}

		ASGroupRef* ref = state->FindGroupRef(group->GetName().c_str());
		
		if(ref)
			continue;	//uz tam je

		DroppedStates.Insert(state);
	}

	ASGroupRef reference(group->GetName().c_str());

	for(uint n = 0; n < DroppedStates.GetCardinality(); n++)
	{
		ASClass* state = DroppedStates[n];
		reference.SetGraphPos(FPoint(DropPos.x, DropPos.y + ((float)n * 100.0f)));

		RedoAction->AddStateRefData(state->GetName(), reference);
		UndoAction->AddStateName(state->GetName());
	}

	if(DroppedStates.GetCardinality() > 0)
	{
		point->AppendRedoAction(RedoAction);
		point->AppendUndoAction(UndoAction);

		EArray<DString> UndoClassSel;
		EArray<DString> RedoClassSel;
		GetSelection(UndoClassSel);

		for(uint n = 0; n < DroppedStates.GetCardinality(); n++)
		{
			ASClass* st = DroppedStates[n];
			DString StateID = st->GetId();
			RedoClassSel.Insert(StateID.c_str());
		}

		point->SetRedoClassSel(RedoClassSel);
		point->SetUndoClassSel(UndoClassSel);

		DString SelPropID = GetPropertySelectionID();
		point->SetRedoPropSel(SelPropID.c_str());
		point->SetUndoPropSel(SelPropID.c_str());

		AddHistoryPoint(point);

		RealizeActions(point->GetRedoActions());
		UpdateUndoRedoAccessibility();

		UpdateCurrentGroupGui();
		Select(DroppedStates, false);
		SelectStatesInGraph(DroppedStates);
		UpdateCopyPasteAccesibility();
		DiagWin->SetFocus();
		DiagWin->RepaintFromClient();
		SetChanged(true);
		MFrame->UpdateTitle();
		UpdateSaveAccesibility();

		StateFindDialog* FindDialog = GetMainFrame()->GetStateFindDialog();

		if(FindDialog && FindDialog->IsShown())
			FindDialog->UpdateResult();
	}
	else
	{
		SAFE_DELETE(point);

		if(WrongCharacter)
		{
			wxMessageDialog dialog(GetMainFrame(), "Canot insert states from another character into graph of current character", "Dropping to graph", wxICON_INFORMATION | wxOK);
			dialog.CentreOnParent();
			dialog.ShowModal();
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnGraphStateMenu(ASClass* state, const wxPoint& pt)
{
	enf_assert(state);
	g_MenuState = state;	//global
	wxMenu StateMenu;

	wxMenu* SourceMenu = new wxMenu();
	SourceMenu->Append(MENU_INSERT_ALL_SOURCES_TO_GRAPH, _T("all"), _T("insert all states that are connect or interpolate to"));
	SourceMenu->Append(MENU_INSERT_CONNECT_SOURCES_TO_GRAPH, _T("all connected"), _T("insert all states that are connect to"));
	SourceMenu->Append(MENU_INSERT_INTERPOLATE_SOURCES_TO_GRAPH, _T("all interpolated"), _T("insert all states that are interpolate to"));

	wxMenu* TargetMenu = new wxMenu();
	TargetMenu->Append(MENU_INSERT_ALL_TARGETS_TO_GRAPH, _T("all"), _T("insert all states that are connect or interpolate to"));
	TargetMenu->Append(MENU_INSERT_CONNECT_TARGETS_TO_GRAPH, _T("all connected"), _T("insert all states that are connect to"));
	TargetMenu->Append(MENU_INSERT_INTERPOLATE_TARGETS_TO_GRAPH, _T("all interpolated"), _T("insert all states that are interpolate to"));

	wxMenu* RemSourceMenu = new wxMenu();
	RemSourceMenu->Append(MENU_REMOVE_ALL_SOURCES_FROM_GRAPH, _T("all"), _T("remove all states that are connect or interpolate to"));
	RemSourceMenu->Append(MENU_REMOVE_CONNECT_SOURCES_FROM_GRAPH, _T("connected only"), _T("remove all states that are connect to only"));
	RemSourceMenu->Append(MENU_REMOVE_INTERPOLATE_SOURCES_FROM_GRAPH, _T("interpolated only"), _T("remove all states that are interpolate to only"));

	wxMenu* RemTargetMenu = new wxMenu();
	RemTargetMenu->Append(MENU_REMOVE_ALL_TARGETS_FROM_GRAPH, _T("all"), _T("remove all states that are connect or interpolate to"));
	RemTargetMenu->Append(MENU_REMOVE_CONNECT_TARGETS_FROM_GRAPH, _T("connected only"), _T("remove all states that are connect to"));
	RemTargetMenu->Append(MENU_REMOVE_INTERPOLATE_TARGETS_FROM_GRAPH, _T("interpolated only"), _T("remove all states that are interpolate to"));

	StateMenu.AppendSubMenu(SourceMenu, "Insert sources");
	StateMenu.AppendSubMenu(TargetMenu, "Insert targets");
	StateMenu.AppendSubMenu(RemSourceMenu, "Remove sources");
	StateMenu.AppendSubMenu(RemTargetMenu, "Remove targets");

	StateMenu.Append(MENU_REMOVE_STATE_FROM_CURRENT_GRAPH, _T("remove from graph"), _T("remove from graph (from current group only)"));
	StateMenu.Append(MENU_COPY_STATE_NAME_CURRENT_GRAPH, _T("copy name to clipboard"), _T("copy state name to clipboard"));

	GetDiagramWindow()->PopupMenu(&StateMenu, pt);
}

//---------------------------------------------------------------------------------------------
uint AnimEditor::SelectStatesInGraph(const EArray<ASClass*>& states)
{
	DiagramWindow* DiagWin = GetDiagramWindow();
	EArray<DiagramNode*> nodes;

	for(uint n = 0; n < states.GetCardinality(); n++)
	{
		ASClass* state = states[n];
		DiagramNode* node = DiagWin->FindNode(state->GetName());

		if(!node)
			continue;	//not found in graph

		nodes.Insert(node);
	}

	if(nodes.GetCardinality() > 0)
	{
		DiagWin->ClearSelection();
		DiagWin->AddToSelection(nodes);
	}

	return nodes.GetCardinality();
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::Select(const EArray<ASClass*>& states, bool FromTree)
{
	EArray<PCClass*> classes;

	for(uint n = 0; n < states.GetCardinality(); n++)
		classes.Insert(states[n]);

	return GenericEditor::Select(classes, FromTree);
}

//---------------------------------------------------------------------------------------------
void AnimEditor::GetAllTargetStatesFrom(const EArray<ASClass*>& states, EArray<ASClass*>& result, int ConnectionTypeFlags)
{
	if(ConnectionTypeFlags & CT_CONNECT)
	{
		for(uint s = 0; s < states.GetCardinality(); s++)
		{
			ASClass* state = states[s];

			PCNode* node = state->GetMember(ConnectToArrayName);
			PCArray* arr = node ? node->ToArray() : NULL;

			EArray<StateRef> refs;
			FindStateRefsFromArray(arr, state->GetCharacter(), refs);

			for(uint n = 0; n < refs.GetCardinality(); n++)
			{
				StateRef& ref = refs[n];

				if(result.GetIndexOf(ref.state) == INDEX_NOT_FOUND)
					result.Insert(ref.state);
			}
		}
	}

	if(ConnectionTypeFlags & CT_INTERPOLATE)
	{
		for(uint s = 0; s < states.GetCardinality(); s++)
		{
			ASClass* state = states[s];

			PCNode* node = state->GetMember(InterpolateToArrayName);
			PCArray* arr = node ? node->ToArray() : NULL;

			EArray<StateRef> refs;
			FindStateRefsFromArray(arr, state->GetCharacter(), refs);

			for(uint n = 0; n < refs.GetCardinality(); n++)
			{
				StateRef& ref = refs[n];

				if(result.GetIndexOf(ref.state) == INDEX_NOT_FOUND)
					result.Insert(ref.state);
			}
		}
	}
}

//---------------------------------------------------------------------------------------------
void AnimEditor::GetAllSourceStatesFrom(const EArray<ASClass*>& states, EArray<ASClass*>& result, int ConnectionTypeFlags)
{
	ParamConfigAnm* config = GetParamConfig();
	ACharacterClass* character = config->GetCurrentCharacterPtr();

	if(!character)
		return;

	EArray<ASClass*>& AllStates = character->GetStates();

	for(uint n = 0; n < AllStates.GetCardinality(); n++)
	{
		ASClass* CurState = AllStates[n];
		bool added = false;

		if(ConnectionTypeFlags & CT_CONNECT)
		{
			PCNode* node = CurState->GetMember(ConnectToArrayName);
			PCArray* arr = node ? node->ToArray() : NULL;

			EArray<StateRef> refs;
			FindStateRefsFromArray(arr, character, refs);

			for(uint r = 0; r < refs.GetCardinality(); r++)
			{
				StateRef& ref = refs[r];

				if(states.GetIndexOf(ref.state) != INDEX_NOT_FOUND)
				{
					if(result.GetIndexOf(CurState) == INDEX_NOT_FOUND)
					{
						result.Insert(CurState);
						added = true;
						break;
					}
				}
			}
		}

		if(!added && (ConnectionTypeFlags & CT_INTERPOLATE))
		{
			PCNode* node = CurState->GetMember(InterpolateToArrayName);
			PCArray* arr = node ? node->ToArray() : NULL;

			EArray<StateRef> refs;
			FindStateRefsFromArray(arr, character, refs);

			for(uint r = 0; r < refs.GetCardinality(); r++)
			{
				StateRef& ref = refs[r];

				if(states.GetIndexOf(ref.state) != INDEX_NOT_FOUND)
				{
					if(result.GetIndexOf(CurState) == INDEX_NOT_FOUND)
					{
						result.Insert(CurState);
						added = true;
					}
					break;
				}
			}
		}
	}
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnGraphDragAndDrop(DWE_DragAndDrop* DEvent)
{
	DiagramWindow* DiagWin = GetDiagramWindow();

	DNDArea* SourceArea = DEvent->GetSource();
	DNDArea* TargetArea = DEvent->GetTarget();
	enf_assert(SourceArea);
	enf_assert(TargetArea);

	DiagramNode* SourceNode = SourceArea->GetOwner();
	DiagramNode* TargetNode = TargetArea->GetOwner();
	enf_assert(SourceNode);
	enf_assert(TargetNode);

	ASClass* SourceState = (ASClass*)SourceNode->GetClientData();
	ASClass* TargetState = (ASClass*)TargetNode->GetClientData();
	enf_assert(SourceState);
	enf_assert(TargetState);

	DiagWin->SelectConnection(SourceNode, 1, TargetNode, 0, PCI_Selected);

	enf_assert(SourceNode);
	enf_assert(TargetState);
	ConnectionRef connection = GetConnectionInfo(SourceState, TargetState);
	ConnectionPanel* ConnPanel = GetMainFrame()->GetConnectionPanel();
	ConnPanel->Set(SourceState->GetName(), TargetState->GetName(), connection.connect, connection.interpolate, FtoA(connection.ConnectWeight).c_str(), FtoA(connection.InterpolateWeight).c_str());
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnGraphSelectionChanged()
{
	DiagramWindow* DiagWin = GetDiagramWindow();
	ConnectionPanel* ConnPanel = GetMainFrame()->GetConnectionPanel();
	ParamFileTree* tree = MFrame->GetParamTree();

	if(DiagWin->GetSelectionCount() == 0)
	{
		DiagWin->UnselectConnection();
		ConnPanel->UpdateAccesibility(true);
		UnselectAll(false);
	}
	else
	{
		EArray<ASClass*> GraphSelStates;
		GetStatesSelectedInGraph(GraphSelStates);
		ParamFileTree* tree = MFrame->GetParamTree();
		MFrame->GetPropGrid()->Freeze();
		tree->Freeze();
		Select(GraphSelStates, false);	//state classes just selected in graph must be selected in editor (in Class tree window)
//		UpdateSelectionGui();
		MFrame->GetPropGrid()->Thaw();
		tree->Thaw();
	}

	UpdateCopyPasteAccesibility();
}

//---------------------------------------------------------------------------------------------
void AnimEditor::OnGraphKey(int keycode, bool down)
{
	switch(keycode)
	{
		case WXK_DELETE:
		{
			EArray<ASClass*> states;
			GetStatesSelectedInGraph(states);
			RemoveStatesFromCurrentGraph(states);
			break;
		}
		case WXK_LEFT:
		{
			EArray<ASClass*> states;
			uint focused = GetStatesSelectedInGraph(states);

			if(focused != INDEX_NOT_FOUND)
				InsertSourceStatesToGraph(states, states[focused]->GetName(), CT_CONNECT | CT_INTERPOLATE);
			break;
		}
		case WXK_RIGHT:
		{
			EArray<ASClass*> states;
			uint focused = GetStatesSelectedInGraph(states);

			if(focused != INDEX_NOT_FOUND)
				InsertTargetStatesToGraph(states, states[focused]->GetName(), CT_CONNECT | CT_INTERPOLATE);
			break;
		}
		case WXK_RETURN:
		{
			ConnectionPanel* ConnPanel = GetMainFrame()->GetConnectionPanel();
			ConnPanel->PressOkButton();
			break;
		}
	}
}

//---------------------------------------------------------------------------------------------
void AnimEditor::RemoveStatesFromCurrentGraph(const EArray<ASClass*>& states)
{
	DiagramWindow* DiagWin = GetDiagramWindow();
	ParamConfigAnm* config = GetParamConfig();
	ACharacterClass* character = config->GetCurrentCharacterPtr();

	if(!character)
		return;

	ASGroup* group = character->GetCurrentGroupPtr();

	if(!group)
		return;

	const char* GroupName = group->GetName().c_str();
	HistoryPointAnm* point = CreateHistoryPoint("Remove Nodes");

	EADeleteSGroupRefs* RedoAction = ENF_NEW EADeleteSGroupRefs(group->GetName().c_str(), character->GetName());
	EACreateSGroupRefs* UndoAction = ENF_NEW EACreateSGroupRefs(character->GetName());
	uint NumToRemove = 0;

	for(uint n = 0; n < states.GetCardinality(); n++)
	{
		ASClass* state = states[n];
		ASGroupRef* GroupRef = state->FindGroupRef(GroupName);

		if(GroupRef == NULL)
			continue;

		RedoAction->AddStateName(state->GetName());
		UndoAction->AddStateRefData(state->GetName(), *GroupRef);
		NumToRemove++;
	}

	if(NumToRemove > 0)
	{
		point->AppendRedoAction(RedoAction);
		point->AppendUndoAction(UndoAction);

		EArray<DString> UndoClassSel;
		EArray<DString> RedoClassSel;
		GetSelection(UndoClassSel);

		point->SetRedoClassSel(RedoClassSel);
		point->SetUndoClassSel(UndoClassSel);

		DString SelPropID = GetPropertySelectionID();
		point->SetRedoPropSel(SelPropID.c_str());
		point->SetUndoPropSel(SelPropID.c_str());

		AddHistoryPoint(point);

		RealizeActions(point->GetRedoActions());
		UpdateUndoRedoAccessibility();

		UpdateCurrentGroupGui();
		DiagWin->RepaintFromClient();
		SetChanged(true);
		MFrame->UpdateTitle();
		UpdateSaveAccesibility();

		StateFindDialog* FindDialog = GetMainFrame()->GetStateFindDialog();

		if(FindDialog && FindDialog->IsShown())
			FindDialog->UpdateResult();
	}
	else
	{
		SAFE_DELETE(point);
	}
}

//---------------------------------------------------------------------------------------------
uint AnimEditor::GetStatesSelectedInGraph(EArray<ASClass*>& states)
{
	DiagramWindow* DiagWin = GetDiagramWindow();
	uint ret = INDEX_NOT_FOUND;
	DiagramNode* FocusedNode = DiagWin->GetFocusedSelection();

	for(uint n = 0; n < DiagWin->GetSelectionCount(); n++)
	{
		DiagramNode* node = DiagWin->GetSelectionMember(n);
		ASClass* state = (ASClass*)node->GetClientData();
		enf_assert(state);
		states.Insert(state);

		if(node == FocusedNode)
			ret = n;
	}
	return ret;
}

//---------------------------------------------------------------------------------------------
HistoryPointAnm* AnimEditor::CreateHistoryPoint(const char* name)
{
	return ENF_NEW HistoryPointAnm(name);
}

//---------------------------------------------------------------------------------------------
void AnimEditor::AddHistoryPoint(HistoryPoint* point)
{
	//before registering new history point here will be stored some gui states to it
	HistoryPointAnm* PointAnm = dynamic_cast<HistoryPointAnm*>(point);
	enf_assert(PointAnm);

	DString RedoCharacterSel;
	DString UndoCharacterSel;
	DString RedoGroupSel;
	DString UndoGroupSel;

	ParamConfigAnm* config = GetParamConfig();
	//for undo current character and current group selection is actual state
	ACharacterClass* character = config->GetCurrentCharacterPtr();		

	if(character)
	{
		UndoCharacterSel = character->GetName();
		ASGroup* group = character->GetCurrentGroupPtr();

		if(group)
			UndoGroupSel = group->GetName().c_str();
	}

	PointAnm->SetUndoCharacterSel(UndoCharacterSel.c_str());
	PointAnm->SetUndoGroupSel(UndoGroupSel.c_str());
	//for redo actual character and states group as default but can be changed in next lines
	RedoCharacterSel = UndoCharacterSel;
	RedoGroupSel = UndoGroupSel;


	//now need check if any character will created from actions of this history point. if yes, character selected after realizing these actions will be the finded one
	wxString CharacterToCreate;
	wxArrayString CharactersToDelete;

	//finding create character action from redo actions from this history point. possible is only one action of this type in RedoActions strip
	EArray<EditAction*>& RedoActions = PointAnm->GetRedoActions();

	for(uint n = 0; n < RedoActions.GetCardinality(); n++)
	{
		EditAction* action = RedoActions[n];

		if(action->GetType() == EA_CREATE_CLASS)
		{
			EACreateClass* act = dynamic_cast<EACreateClass*>(action);
			enf_assert(act);

			if(act->GetClassInfoFlags() & CI_CHARACTER)
			{
				CharacterToCreate = act->GetId().c_str();
				CharacterToCreate = CharacterToCreate.AfterLast('/');
			}
		}
		else if(action->GetType() == EA_DELETE_NODE)
		{
			EADeleteNode* act = dynamic_cast<EADeleteNode*>(action);
			enf_assert(act);
			PCNode* node = config->FindNode(act->GetId().c_str());
			enf_assert(node);

			if(node->ToClass())
			{
				ACharacterClass* ch = dynamic_cast<ACharacterClass*>(node);

				if(ch)
					CharactersToDelete.Add(ch->GetName());
			}
		}
	}

	//found any action to create new character? yes? this character will be selected after realize this history point
	if(!CharacterToCreate.IsEmpty())
	{
		RedoCharacterSel = CharacterToCreate;
		RedoGroupSel.clear();
	}
	else if(!CharactersToDelete.IsEmpty())
	{
		bool AnyCharacterSel = false;
		//if character classes will be deleted, selected character after action must be one other one if any
		for(uint i = 0; i < config->GetCharactersCount(); i++)
		{
			ACharacterClass* ch = config->GetCharacter(i);
			bool IsDeleteAdept = false;

			for(uint n = 0; n < CharactersToDelete.GetCount(); n++)
			{
				const wxString& ToDelete = CharactersToDelete[n];

				if(strcmpi(ch->GetName(), ToDelete.c_str()) == 0)
				{
					IsDeleteAdept = true;
					break;
				}
			}

			if(!IsDeleteAdept)
			{
				RedoCharacterSel = ch->GetName();
				ASGroup* gr = ch->GetCurrentGroupPtr();

				if(gr)
					RedoGroupSel = gr->GetName();
				else
					RedoGroupSel.clear();

				AnyCharacterSel = true;
			}
		}

		if(!AnyCharacterSel)
		{
			RedoCharacterSel.clear();
			RedoGroupSel.clear();
		}
	}

	PointAnm->SetRedoCharacterSel(RedoCharacterSel.c_str());
	PointAnm->SetRedoGroupSel(RedoGroupSel.c_str());

	DiagramWindow* DiagWin = GetDiagramWindow();
	PointAnm->SetGraphViewPos(DiagWin->GetViewPoint());
	PointAnm->SetGraphZoomLevel(DiagWin->GetZoomLevel());

	GenericEditor::AddHistoryPoint(point);
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::RestoreHistoryPoint(HistoryPoint* point, bool undo)
{
	HistoryPointAnm* PointAnm = dynamic_cast<HistoryPointAnm*>(point);
	enf_assert(PointAnm);

	DiagramWindow* DiagWin = GetDiagramWindow();
	StateGroupsPanel* GroupsPanel = GetMainFrame()->GetGroupsPanel();
	ParamConfigAnm* config = GetParamConfig();

	DiagWin->Freeze();	//do not redraw window

	bool result = GenericEditor::RestoreHistoryPoint(point, undo);

	if(undo)
	{
		const DString& CharacterName = PointAnm->GetUndoCharacterSel();
		const DString& GroupName = PointAnm->GetUndoGroupSel();
		const char* CharacterNameStr = (CharacterName.empty()) ? NULL : CharacterName.c_str();
		const char* GroupNameStr = (GroupName.empty()) ? NULL : GroupName.c_str();

		if(config->SetCurrentCharacterAndGroup(CharacterNameStr, GroupNameStr) == false)
		{
			enf_assert(false);
		}
	}
	else	//redo
	{
		const DString& CharacterName = PointAnm->GetRedoCharacterSel();
		const DString& GroupName = PointAnm->GetRedoGroupSel();
		const char* CharacterNameStr = (CharacterName.empty()) ? NULL : CharacterName.c_str();
		const char* GroupNameStr = (GroupName.empty()) ? NULL : GroupName.c_str();

		if(config->SetCurrentCharacterAndGroup(CharacterNameStr, GroupNameStr) == false)
		{
			enf_assert(false);
		}
	}

	UpdateCharacterManagerGuiContent();
	UpdateGroupsManagerGuiContent();	//insert actual group into gui
	UpdateCurrentGroupGui();

	EArray<ASClass*> SelStates;
	GetSelectedStates(SelStates);
	SelectStatesInGraph(SelStates);
	
	DiagWin->SetViewPoint(PointAnm->GetGraphViewPos());
	DiagWin->SetZoomLevel(PointAnm->GetGraphZoomLevel());
	DiagWin->RepaintFromClient();
	UpdateCopyPasteAccesibility();

	DiagWin->Thaw();	//window redraw now enabled

	StateFindDialog* FindDialog = GetMainFrame()->GetStateFindDialog();

	if(FindDialog && FindDialog->IsShown())	//find dialog must accept all changes
		FindDialog->UpdateResult();

	return result;
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::RealizeActions(const EArray<EditAction*>& actions)
{
	bool AnyCharacterCreatedOrDeleted = false;

	for(uint n = 0; n < actions.GetCardinality(); n++)
	{
		EditAction* action = actions[n];

		if(action->GetType() == EA_DELETE_NODE)
		{
			EADeleteNode* DeleteAction = (EADeleteNode*)action;
			PCNode* node = PConfig->FindNode(DeleteAction->GetId().c_str());
			enf_assert(node);

			if(dynamic_cast<ACharacterClass*>(node) != NULL)
				AnyCharacterCreatedOrDeleted = true;	
		}
	}

	bool ok = GenericEditor::RealizeActions(actions);

	for(uint n = 0; n < actions.GetCardinality(); n++)
	{
		EditAction* action = actions[n];

		if(action->GetType() == EA_CREATE_CLASS)
		{
			EACreateClass* CreateClAction = (EACreateClass*)action;
			PCNode* node = PConfig->FindNode(CreateClAction->GetId().c_str());
			enf_assert(node);
		
			ACharacterClass* character = dynamic_cast<ACharacterClass*>(node);
			
			if(character)
				AnyCharacterCreatedOrDeleted = true;
		}
	}

	//if character created or deleted, characters and groups combo need be updated
	if(AnyCharacterCreatedOrDeleted)
	{
		UpdateCharacterManagerGuiContent();
		UpdateGroupsManagerGuiContent();
		UpdateCurrentGroupGui();	//update graph content
		GetDiagramWindow()->RepaintFromClient();
	}

	return ok;
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::RealizeAction(const EditAction* action)
{
	assert(action);

	switch(action->GetType())
	{
	case EA_CREATE_SGROUP:		//vytvara skupinu
		return DoRealizeAction((const EACreateSGroup*)action);
		break;
	case EA_DELETE_SGROUP:		//maze skupinu
		return DoRealizeAction((const EADeleteSGroup*)action);
		break;
	case EA_CREATE_SGROUP_REFS:	//vytvori premennu/param
		return DoRealizeAction((const EACreateSGroupRefs*)action);
		break;
	case EA_DELETE_SGROUP_REFS:	//vytvori premennu/param
		return DoRealizeAction((const EADeleteSGroupRefs*)action);
		break;
	case EA_MODIFY_SGROUP_REFS:	//vytvori premennu/param
		return DoRealizeAction((const EAModifySGroupRefs*)action);
		break;
	default:
		return GenericEditor::RealizeAction(action);	//neni to akcia tykajuca sa anim casti
	}
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::DoRealizeAction(const EACreateSGroup* action)
{
	ParamConfigAnm* config = GetParamConfig();
	ACharacterClass* character = config->FindCharacter(action->GetCharacterName().c_str());
	enf_assert(character);
	const ASGroup* storage = action->GetStorage();
	ASGroup* group = NULL;

	if(storage)
		group = storage->Clone();
	else
		group = ENF_NEW ASGroup(action->GetName().c_str());

	character->AddGroup(group);
	return true;
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::DoRealizeAction(const EADeleteSGroup* action)
{
	ParamConfigAnm* config = GetParamConfig();
	ACharacterClass* character = config->FindCharacter(action->GetCharacterName().c_str());
	enf_assert(character);
	ASGroup* group = character->FindGroup(action->GetName().c_str());
	enf_assert(group);
	return character->RemoveGroup(group);
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::DoRealizeAction(const EACreateSGroupRefs* action)
{
	ParamConfigAnm* config = GetParamConfig();
	ACharacterClass* character = config->FindCharacter(action->GetCharacterName().c_str());
	enf_assert(character);

	for(uint n = 0; n < action->GetStateRefDataCount(); n++)
	{
		const DString& StateName = action->GetStateName(n);
		const ASGroupRef& StateRef = action->GetStateRefData(n);
		
		ASClass* state = character->FindStateClass(StateName.c_str());
		enf_assert(state);
		state->AddRef(StateRef);
	}

	return true;
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::DoRealizeAction(const EADeleteSGroupRefs* action)
{
	ParamConfigAnm* config = GetParamConfig();
	ACharacterClass* character = config->FindCharacter(action->GetCharacterName().c_str());
	enf_assert(character);

	ASGroup* group = character->FindGroup(action->GetGroupName().c_str());
	enf_assert(group);

	ASGroupRef GroupRef(group->GetName().c_str());
	const EArray<DString>& StateNames = action->GetStateNames();

	for(uint n = 0; n < StateNames.GetCardinality(); n++)
	{
		const DString& StateName = StateNames[n];
		ASClass* state = character->FindStateClass(StateName.c_str());
		enf_assert(state);

		bool removed = state->RemoveRef(GroupRef);
		enf_assert(removed);
	}

	return true;
}

//---------------------------------------------------------------------------------------------
bool AnimEditor::DoRealizeAction(const EAModifySGroupRefs* action)
{
	ParamConfigAnm* config = GetParamConfig();
	ACharacterClass* character = config->FindCharacter(action->GetCharacterName().c_str());
	enf_assert(character);

	for(uint n = 0; n < action->GetStateRefDataCount(); n++)
	{
		const DString& StateName = action->GetStateName(n);
		const ASGroupRef& StateRef = action->GetStateRefData(n);
		
		ASClass* state = character->FindStateClass(StateName.c_str());
		enf_assert(state);

		ASGroupRef* ref = state->FindGroupRef(StateRef.GetGroupName().c_str());
		enf_assert(ref);

		*ref = StateRef;
	}
	return true;
}

//-----------------------------------------------------------------------
void AnimEditor::OnAppConfigChanged()
{
	GenericEditor::OnAppConfigChanged();
}

//-----------------------------------------------------------------------
void AnimEditor::GetAllConnectionsFromStates(const EArray<ASClass*>& states, int ConnectTypeFlags, ACharacterClass* character, ESet<ConnectionRef>& result)
{
	enf_assert(character);
	PCNode* node;
	PCArray* arr;

	for(uint n = 0; n < states.GetCardinality(); n++)
	{
		ASClass* state = states[n];

		if(ConnectTypeFlags & CT_CONNECT)
		{
			node = state->GetValueNode(ConnectToArrayName);
			arr = node ? node->ToArray() : NULL;

			if(arr)
				GetStateConnectionsFromArray(state, arr, character, true, CONNECT_DIRECTION_TO, result);

			node = state->GetValueNode(ConnectFromArrayName);
			arr = node ? node->ToArray() : NULL;

			if(arr)
				GetStateConnectionsFromArray(state, arr, character, true, CONNECT_DIRECTION_FROM, result);

			node = state->GetValueNode(ConnectWithArrayName);
			arr = node ? node->ToArray() : NULL;

			if(arr)
				GetStateConnectionsFromArray(state, arr, character, true, CONNECT_DIRECTION_BOTH, result);
		}
//---
		if(ConnectTypeFlags & CT_INTERPOLATE)
		{
			node = state->GetValueNode(InterpolateToArrayName);
			arr = node ? node->ToArray() : NULL;

			if(arr)
				GetStateConnectionsFromArray(state, arr, character, false, CONNECT_DIRECTION_TO, result);

			node = state->GetValueNode(InterpolateFromArrayName);
			arr = node ? node->ToArray() : NULL;

			if(arr)
				GetStateConnectionsFromArray(state, arr, character, false, CONNECT_DIRECTION_FROM, result);

			node = state->GetValueNode(InterpolateWithArrayName);
			arr = node ? node->ToArray() : NULL;

			if(arr)
				GetStateConnectionsFromArray(state, arr, character, false, CONNECT_DIRECTION_BOTH, result);
		}
	}
}

//-----------------------------------------------------------------------
void AnimEditor::GetStateConnectionsFromArray(ASClass* state, PCArray* arr, ACharacterClass* character, bool connect, int direction, ESet<ConnectionRef>& result)
{
	EArray<StateRef> StateRefs;
	FindStateRefsFromArray(arr, character, StateRefs);

	for(uint r = 0; r < StateRefs.GetCardinality(); r++)
	{
		StateRef& SRef = StateRefs[r];
		ASClass* source;
		ASClass* target;
		int NumConnections = (direction == CONNECT_DIRECTION_BOTH) ? 2 : 1;

		if(direction == CONNECT_DIRECTION_FROM)
		{
			source = SRef.state;
			target = state;
		}
		else
		{
			source = state;
			target = SRef.state;
		}

		for(int n = 0; n < NumConnections; n++)
		{
			if(n == 1)
			{
				ASClass* flip = source;
				source = target;
				target = flip;
			}

			ConnectionRef CRef(source, target);
			uint index = result.GetIndexOf(CRef);

			if(index == INDEX_NOT_FOUND)
			{
				result.Insert(CRef, &index);
			}

			ConnectionRef& CRef2 = result[index];

			if(connect)
			{
				if(!CRef2.connect)
					CRef2.SetConnect(SRef.weight);
			}
			else	//interpolate
			{
				if(!CRef2.interpolate)
					CRef2.SetInterpolate(SRef.weight);
			}
		}
	}
}

//-----------------------------------------------------------------------
void AnimEditor::PrepareStatesToEdit()
{
	ParamConfigAnm* config = GetParamConfig();

	for(uint ch = 0; ch < config->GetCharactersCount(); ch++)
	{
		ACharacterClass* character = config->GetCharacter(ch);						//for every character

		EArray<ASClass*>& ChatacterStates = character->GetStates();					//get all state classes for this character
		ESet<ConnectionRef> connections;
		GetAllConnectionsFromStates(ChatacterStates, CT_CONNECT | CT_INTERPOLATE, character, connections);	//convert all connections to one way direction
		DeleteAllConnectionParamsFromStates(ChatacterStates);							//delete of all parameters that are just defines connections betwen states
		InsertMissingConnectionParamsToStates(ChatacterStates);						//create empty connectTo and interpolateTo array parameters to reduce edit complexity later
		AddConnectionsToStates(connections);												//write stored connections as only ConnectToArrayName and InterpolateToArrayName array parameters
	}
}

//-----------------------------------------------------------------------
void AnimEditor::FindStateRefsFromArray(PCArray* arr, ACharacterClass* character, EArray<StateRef>& result)
{
	uint count = arr->GetCardinality();
	enf_assert(!(count & 1));

	for(uint n = 0; n < count; n += 2)
	{
		uint second = n + 1;

		if(second >= count)
			break;

		PCNode* item0 = arr->GetItem(n);
		PCNode* item1 = arr->GetItem(second);

		PCVar* var0 = item0->ToVariable();
		PCVar* var1 = item1->ToVariable();

		if(!var0)
			continue;

		if(!var1)
			continue;

		const char* StateName = var0->GetValue();
		const char* WeightValue = var1->GetValue();
		ASClass*		StatePtr = character->FindStateClass(StateName);

		if(StatePtr)
			result.Insert(StateRef(StatePtr, taAtoF(WeightValue)));
	}
}

//-----------------------------------------------------------------------
void AnimEditor::FindStateRefsFromArray(PCArray* arr, ASClass* state, EArray<PCVar*>& result)
{
	uint count = arr->GetCardinality();
	enf_assert(!(count & 1));

	for(uint n = 0; n < count; n += 2)
	{
		uint second = n + 1;

		if(second >= count)
			break;

		PCNode* item0 = arr->GetItem(n);
		PCNode* item1 = arr->GetItem(second);

		PCVar* var0 = item0->ToVariable();
		PCVar* var1 = item1->ToVariable();

		if(!var0)
			continue;

		if(!var1)
			continue;

		const char* StateName = var0->GetValue();

		if(strcmpi(StateName, state->GetName()) == 0)
			result.Insert(var0);
	}
}

//-----------------------------------------------------------------------
void AnimEditor::DeleteAllConnectionParamsFromStates(const EArray<ASClass*>& states)
{
	for(uint s = 0; s < states.GetCardinality(); s++)
	{
		ASClass* state = states[s];
		EArray<PCArray*> ToDelete;

		for(uint n = 0; n < state->GetNodesCount(); n++)
		{
			PCNode* node = state->GetNode(n);
			PCArray* arr = node ? node->ToArray() : NULL;

			if(!arr)
				continue;

			if((strcmpi(arr->GetName(), ConnectToArrayName) == 0) ||
				(strcmpi(arr->GetName(), ConnectFromArrayName) == 0) ||
				(strcmpi(arr->GetName(), ConnectWithArrayName) == 0) ||
				(strcmpi(arr->GetName(), InterpolateToArrayName) == 0) ||
				(strcmpi(arr->GetName(), InterpolateFromArrayName) == 0) ||
				(strcmpi(arr->GetName(), InterpolateWithArrayName) == 0))
			{
				ToDelete.Insert(arr);
//				arr->Clear();
			}
		}

		for(uint n = 0; n < ToDelete.GetCardinality(); n++)
		{
			PCArray* darr = ToDelete[n];
			state->RemoveNode(darr);
			delete darr;
		}
	}
}

//-----------------------------------------------------------------------
void AnimEditor::InsertMissingConnectionParamsToStates(const EArray<ASClass*>& states)
{
	ParamConfig* PConfig = GetParamConfig();

	for(uint s = 0; s < states.GetCardinality(); s++)
	{
		ASClass* state = states[s];
		const PCDefClass* StateDef = state->GetDefinition();
		bool NeedConnectTo = true;
		bool NeedInterpolateTo = true;

		for(uint n = 0; n < state->GetNodesCount(); n++)
		{
			PCNode* node = state->GetNode(n);
			PCArray* arr = node ? node->ToArray() : NULL;

			if(!arr)
				continue;

			if(strcmpi(arr->GetName(), ConnectToArrayName) == 0)
				NeedConnectTo = false;

			if(strcmpi(arr->GetName(), InterpolateToArrayName) == 0)
				NeedInterpolateTo = false;
		}

		if(NeedConnectTo)
		{
			PCDefParam* ParamDef = StateDef ? (PCDefParam*)StateDef->GetParamDef(ConnectToArrayName) : NULL;
			assert(ParamDef);
			ParamDef->SetReadOnly(true);
			PCArray* arr = PConfig->CreateArray(ConnectToArrayName, state, ParamDef);
			state->AppendNode(arr);
		}

		if(NeedInterpolateTo)
		{
			PCDefParam* ParamDef = StateDef ? (PCDefParam*)StateDef->GetParamDef(InterpolateToArrayName) : NULL;
			assert(ParamDef);
			ParamDef->SetReadOnly(true);
			PCArray* arr = PConfig->CreateArray(InterpolateToArrayName, state, ParamDef);
			state->AppendNode(arr);
		}
	}
}

//-----------------------------------------------------------------------
void AnimEditor::AddConnectionsToStates(const ESet<ConnectionRef>& connections)
{
	ParamConfig* PConfig = GetParamConfig();

	for(uint c = 0; c < connections.GetCardinality(); c++)
	{
		ConnectionRef& connection = connections[c];
		ASClass* SrcState = connection.SrcState;
		ASClass* TargState = connection.TargState;
		bool connect = connection.connect;
		bool interpolate = connection.interpolate;
		const PCDefClass* StateDef = SrcState->GetDefinition();
		
		if(connect)
		{
			wxString ConnectWeight = FtoA(connection.ConnectWeight);

			PCNode* node = SrcState->GetMember(ConnectToArrayName);
			PCArray* arr = node ? node->ToArray(): NULL;

			if(!arr)
			{
				const PCDefParam* ParamDef = StateDef ? StateDef->GetParamDef(ConnectToArrayName) : NULL;
				assert(ParamDef);
				arr = ENF_NEW PCArrayA(ConnectToArrayName, SrcState, ParamDef);
				SrcState->AppendNode(arr);
			}

			DString UniName = PConfig->GenerateUniqueName(UID_VARIABLE);
			PCVar* var0 = ENF_NEW PCVarA(UniName.c_str(), arr, NULL);
			UniName = PConfig->GenerateUniqueName(UID_VARIABLE);
			PCVar* var1 = ENF_NEW PCVarA(UniName.c_str(), arr, NULL);

			var0->SetValue(TargState->GetName());
			var1->SetValue(ConnectWeight.c_str());
			arr->Add(var0);
			arr->Add(var1);
		}

		if(interpolate)
		{
			wxString InterpolateWeight = FtoA(connection.InterpolateWeight);

			PCNode* node = SrcState->GetMember(InterpolateToArrayName);
			PCArray* arr = node ? node->ToArray(): NULL;

			if(!arr)
			{
				const PCDefParam* ParamDef = StateDef ? StateDef->GetParamDef(InterpolateToArrayName) : NULL;
				assert(ParamDef);
				arr = ENF_NEW PCArrayA(InterpolateToArrayName, SrcState, ParamDef);
				SrcState->AppendNode(arr);
			}

			DString UniName = PConfig->GenerateUniqueName(UID_VARIABLE);
			PCVar* var0 = ENF_NEW PCVarA(UniName.c_str(), arr, NULL);
			UniName = PConfig->GenerateUniqueName(UID_VARIABLE);
			PCVar* var1 = ENF_NEW PCVarA(UniName.c_str(), arr, NULL);

			var0->SetValue(TargState->GetName());
			var1->SetValue(InterpolateWeight.c_str());
			arr->Add(var0);
			arr->Add(var1);
		}
	}
}

//-----------------------------------------------------------------------
ConnectionRef AnimEditor::GetConnectionInfo(ASClass* source, ASClass* target)
{
	//just must be config file reorganized and parameters that defines connections must be only connectTo a interpolateTo arrays
	PCNode* ConnectToNode = source->GetMember(ConnectToArrayName);
	PCNode* InterpolateToNode = source->GetMember(InterpolateToArrayName);
	PCArray* ConnectTo = ConnectToNode ? ConnectToNode->ToArray() : NULL;
	PCArray* InterpolateTo = InterpolateToNode ? InterpolateToNode->ToArray() : NULL;
	ACharacterClass* character = source->GetCharacter();
	ConnectionRef connection(source, target);
	
	if(ConnectTo)
	{
		EArray<StateRef> refs;
		FindStateRefsFromArray(ConnectTo, character, refs);
		uint index = refs.GetIndexOf(StateRef(target, 0.0f));

		if(index != INDEX_NOT_FOUND)
		{
			StateRef& rf = refs[index];
			connection.SetConnect(rf.weight);
		}
	}

	if(InterpolateTo)
	{
		EArray<StateRef> refs;
		FindStateRefsFromArray(InterpolateTo, character, refs);
		uint index = refs.GetIndexOf(StateRef(target, 0.0f));

		if(index != INDEX_NOT_FOUND)
		{
			StateRef& rf = refs[index];
			connection.SetInterpolate(rf.weight);
		}
	}

	return connection;
}

//-----------------------------------------------------------------------
void AnimEditor::UpdateConnection(ConnectionRef& connection)
{
	ParamConfig* cfg = GetParamConfig();
	HistoryPointAnm* point = CreateHistoryPoint("Update Conection");

	ASClass* SrcState = connection.SrcState;
	ASClass* TargState = connection.TargState;

	for(uint i = 0; i < 2; i++)
	{
		const char* ConnectArrayParam;
		bool connect;
		wxString weight;

		if(i == 0)	//connect
		{
			ConnectArrayParam = ConnectToArrayName;
			connect = connection.connect;
			weight = FtoA(connection.ConnectWeight);
		}
		else	//interpolate
		{
			ConnectArrayParam = InterpolateToArrayName;
			connect = connection.interpolate;
			weight = FtoA(connection.InterpolateWeight);
		}

		PCNode* node = SrcState->GetMember(ConnectArrayParam);
		enf_assert(node);	//must have this node/member. not possible if no
		PCArray* ConnectToArr = node->ToArray();
		enf_assert(ConnectToArr);	//and must be array

		EArray<StateRef> StateRefs;
		FindStateRefsFromArray(ConnectToArr, SrcState->GetCharacter(), StateRefs);
		uint VarIndex = StateRefs.GetIndexOf(StateRef(TargState, 0.0f));
		uint OldItem0Index = INDEX_NOT_FOUND;
		PCVar* OldItem0 = ConnectToArr->GetVariableMemberFromValue(TargState->GetName(), &OldItem0Index);
		uint OldItem1Index = OldItem0Index + 1;
		PCVar* OldItem1 = OldItem0 ? ConnectToArr->GetItem(OldItem1Index)->ToVariable() : NULL;
		bool OldConnect = (VarIndex != INDEX_NOT_FOUND);
		uint PosInOwner;

		if(OldConnect && !connect)	//until now this connection was existing but now must be deleted
		{
			enf_assert(OldItem0);
			enf_assert(OldItem1);
			DString OldItem0Id = OldItem0->GetId();
			DString OldItem1Id = OldItem1->GetId();
			PosInOwner = OldItem0->GetPosInOwner();
			point->AppendRedoAction(ENF_NEW EADeleteNode(OldItem0Id.c_str()));	//mazeme itemy z pola meno statu / vaha
			point->AppendRedoAction(ENF_NEW EADeleteNode(OldItem1Id.c_str()));

			point->AppendUndoAction(ENF_NEW EACreateVariable(OldItem0Id.c_str(), OldItem0->GetName(), OldItem0->GetValue(), PosInOwner));	//mazeme itemy z pola meno statu / vaha
			point->AppendUndoAction(ENF_NEW EACreateVariable(OldItem1Id.c_str(), OldItem1->GetName(), OldItem1->GetValue(), PosInOwner + 1));
		}
		else if(!OldConnect && connect)	//until now this connection was not existing and now need create it
		{
			DString NewItem0Name = cfg->GenerateUniqueName(UID_VARIABLE);
			DString NewItem1Name = cfg->GenerateUniqueName(UID_VARIABLE);
			DString NewItem0Id = ConnectToArr->GetId(); NewItem0Id += '/'; NewItem0Id += NewItem0Name; 
			DString NewItem1Id = ConnectToArr->GetId(); NewItem1Id += '/'; NewItem1Id += NewItem1Name; 
			PosInOwner = ConnectToArr->GetCardinality();

			point->AppendRedoAction(ENF_NEW EACreateVariable(NewItem0Id.c_str(), NewItem0Name.c_str(), TargState->GetName(), PosInOwner));
			point->AppendRedoAction(ENF_NEW EACreateVariable(NewItem1Id.c_str(), NewItem1Name.c_str(), weight.c_str(), PosInOwner + 1));

			point->AppendUndoAction(ENF_NEW EADeleteNode(NewItem0Id.c_str()));
			point->AppendUndoAction(ENF_NEW EADeleteNode(NewItem1Id.c_str()));
		}

		bool NeedModifyWeight = (OldConnect && connect && OldItem1 && strcmpi(OldItem1->GetValue(), weight.c_str()) != 0);

		if(NeedModifyWeight)
		{
			DString OldItem1Id = OldItem1->GetId();
			point->AppendRedoAction(ENF_NEW EAModifyVariable(OldItem1Id.c_str(), weight.c_str()));
			point->AppendUndoAction(ENF_NEW EAModifyVariable(OldItem1Id.c_str(), OldItem1->GetValue()));
		}
	}

	if(point->GetRedoActions().GetCardinality() > 0)
	{
		EArray<DString> ClassSel;
		GetSelection(ClassSel);
		point->SetRedoClassSel(ClassSel);
		point->SetUndoClassSel(ClassSel);

		DString SelPropID = GetPropertySelectionID();
		point->SetRedoPropSel(SelPropID.c_str());
		point->SetUndoPropSel(SelPropID.c_str());

		AddHistoryPoint(point);
		RealizeActions(point->GetRedoActions());
		UpdateUndoRedoAccessibility();
		UpdateCurrentGroupGui();
		GetDiagramWindow()->RepaintFromClient();
		SetChanged(true);
		MFrame->UpdateTitle();
		UpdateSaveAccesibility();
	}
	else
	{
		SAFE_DELETE(point);
	}
}

//-----------------------------------------------------------------------
void AnimEditor::InsertStatesToGraph(bool sources, int ConnectTypeFlags)
{
	EArray<ASClass*> states;
	uint focused = g_aeditor->GetStatesSelectedInGraph(states);

	if(focused != INDEX_NOT_FOUND)
	{
		if(sources)
			InsertSourceStatesToGraph(states, states[focused]->GetName(), ConnectTypeFlags);
		else
			InsertTargetStatesToGraph(states, states[focused]->GetName(), ConnectTypeFlags);
	}
}

//-----------------------------------------------------------------------
void AnimEditor::RemoveStatesFromGraph(bool sources, int ConnectTypeFlags, bool ConnectOrInterpolate)
{
	EArray<ASClass*> states;
	uint focused = g_aeditor->GetStatesSelectedInGraph(states);

	if(focused != INDEX_NOT_FOUND)
	{
		if(sources)
			RemoveSourceStatesFromGraph(states, ConnectTypeFlags, ConnectOrInterpolate);
		else
			RemoveTargetStatesFromGraph(states, ConnectTypeFlags, ConnectOrInterpolate);
	}
}

//-----------------------------------------------------------------------
void AnimEditor::InsertSourceStatesToGraph(const EArray<ASClass*>& from, const char* GraphNode, int ConnectTypeFlags)
{
	DiagramWindow* DiagWin = GetDiagramWindow();

	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();
	enf_assert(character);

	ASGroup* group = character->GetCurrentGroupPtr();
	enf_assert(group);

	DiagramNode* node = DiagWin->FindNode(GraphNode);
	enf_assert(node);

	if(node)
	{
		FPoint GraphPos = node->GetPos();
		GraphPos.x -= 100.0f;
		EArray<ASClass*> states;
		GetAllSourceStatesFrom(from, states, ConnectTypeFlags);

		if(states.GetCardinality() > 0)
		{
			HistoryPointAnm* point = CreateHistoryPoint("Insert Nodes");

			EACreateSGroupRefs* RedoAction = ENF_NEW EACreateSGroupRefs(character->GetName());
			EADeleteSGroupRefs* UndoAction = ENF_NEW EADeleteSGroupRefs(group->GetName().c_str(), character->GetName());

			EArray<ASClass*> InsertedStates;
			EArray<float> widths;
			bool AlignRight = true;
			ASGroupRef reference(group->GetName().c_str());

			for(uint n = 0; n < states.GetCardinality(); n++)
			{
				ASClass* st = states[n];

				if(st->FindGroupRef(group->GetName().c_str()) != NULL)
					continue;	//now found in graph

				InsertedStates.Insert(st);
				widths.Insert(DiagWin->CalculateNodeVirtualWidth(st->GetName()));
			}

			for(uint n = 0; n < InsertedStates.GetCardinality(); n++)
			{
				ASClass* st = InsertedStates[n];
				float XOffset = AlignRight ? -widths[n] : 0.0f;
				reference.SetGraphPos(GraphPos + FPoint(XOffset, ((float)n * 100.0f)));

				RedoAction->AddStateRefData(st->GetName(), reference);
				UndoAction->AddStateName(st->GetName());
			}

			if(InsertedStates.GetCardinality() > 0)
			{
				point->AppendRedoAction(RedoAction);
				point->AppendUndoAction(UndoAction);

				EArray<DString> UndoClassSel;
				EArray<DString> RedoClassSel;
				GetSelection(UndoClassSel);

				for(uint n = 0; n < InsertedStates.GetCardinality(); n++)
				{
					ASClass* st = InsertedStates[n];
					DString StateID = st->GetId();
					RedoClassSel.Insert(StateID.c_str());
				}

				point->SetRedoClassSel(RedoClassSel);
				point->SetUndoClassSel(UndoClassSel);

				DString SelPropID = GetPropertySelectionID();
				point->SetRedoPropSel(SelPropID.c_str());
				point->SetUndoPropSel(SelPropID.c_str());

				AddHistoryPoint(point);

				RealizeActions(point->GetRedoActions());
				UpdateUndoRedoAccessibility();

				UpdateCurrentGroupGui();
				Select(InsertedStates, false);
				SelectStatesInGraph(InsertedStates);
				UpdateCopyPasteAccesibility();
				DiagWin->RepaintFromClient();
				SetChanged(true);
				MFrame->UpdateTitle();
				UpdateSaveAccesibility();
			}
			else
			{
				SAFE_DELETE(point);

				wxMessageDialog dialog(GetMainFrame(), "All requested states found in graph", "Find/Insert states result", wxICON_INFORMATION | wxOK);
				dialog.CentreOnParent();
				dialog.ShowModal();
			}
		}
		else
		{
			wxMessageDialog dialog(GetMainFrame(), "Requested states not found", "Find/Insert states result", wxICON_INFORMATION | wxOK);
			dialog.CentreOnParent();
			dialog.ShowModal();
		}
	}
}

//-----------------------------------------------------------------------
void AnimEditor::InsertTargetStatesToGraph(const EArray<ASClass*>& from, const char* GraphNode, int ConnectTypeFlags)
{
	DiagramWindow* DiagWin = GetDiagramWindow();

	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();
	enf_assert(character);

	ASGroup* group = character->GetCurrentGroupPtr();
	enf_assert(group);
 
	DiagramNode* node = DiagWin->FindNode(GraphNode);
	enf_assert(node);

	if(node)
	{
		FPoint GraphPos = node->GetPos();
		GraphPos.x += node->GetSize().x + 100.0f;
		EArray<ASClass*> states;
		GetAllTargetStatesFrom(from, states, ConnectTypeFlags);

		if(states.GetCardinality() > 0)
		{
			HistoryPointAnm* point = CreateHistoryPoint("Insert Nodes");

			EACreateSGroupRefs* RedoAction = ENF_NEW EACreateSGroupRefs(character->GetName());
			EADeleteSGroupRefs* UndoAction = ENF_NEW EADeleteSGroupRefs(group->GetName().c_str(), character->GetName());

			EArray<ASClass*> InsertedStates;
			EArray<float> widths;
			bool AlignRight = false;
			ASGroupRef reference(group->GetName().c_str());

			for(uint n = 0; n < states.GetCardinality(); n++)
			{
				ASClass* st = states[n];

				if(st->FindGroupRef(group->GetName().c_str()) != NULL)
					continue;	//now found in graph

				InsertedStates.Insert(st);
				widths.Insert(DiagWin->CalculateNodeVirtualWidth(st->GetName()));
			}

			for(uint n = 0; n < InsertedStates.GetCardinality(); n++)
			{
				ASClass* st = InsertedStates[n];
				float XOffset = AlignRight ? -widths[n] : 0.0f;
				reference.SetGraphPos(GraphPos + FPoint(XOffset, ((float)n * 100.0f)));

				RedoAction->AddStateRefData(st->GetName(), reference);
				UndoAction->AddStateName(st->GetName());
			}

			if(InsertedStates.GetCardinality() > 0)
			{
				point->AppendRedoAction(RedoAction);
				point->AppendUndoAction(UndoAction);

				EArray<DString> UndoClassSel;
				EArray<DString> RedoClassSel;
				GetSelection(UndoClassSel);

				for(uint n = 0; n < InsertedStates.GetCardinality(); n++)
				{
					ASClass* st = InsertedStates[n];
					DString StateID = st->GetId();
					RedoClassSel.Insert(StateID.c_str());
				}

				point->SetRedoClassSel(RedoClassSel);
				point->SetUndoClassSel(UndoClassSel);

				DString SelPropID = GetPropertySelectionID();
				point->SetRedoPropSel(SelPropID.c_str());
				point->SetUndoPropSel(SelPropID.c_str());

				AddHistoryPoint(point);

				RealizeActions(point->GetRedoActions());
				UpdateUndoRedoAccessibility();

				UpdateCurrentGroupGui();
				Select(InsertedStates, false);
				SelectStatesInGraph(InsertedStates);
				DiagWin->RepaintFromClient();
				UpdateCopyPasteAccesibility();
				SetChanged(true);
				MFrame->UpdateTitle();
				UpdateSaveAccesibility();
			}
			else
			{
				SAFE_DELETE(point);

				wxMessageDialog dialog(GetMainFrame(), "All requested states found in graph", "Find/Insert states result", wxICON_INFORMATION | wxOK);
				dialog.CentreOnParent();
				dialog.ShowModal();
			}
		}
		else
		{
			wxMessageDialog dialog(GetMainFrame(), "Requested states not found", "Find/Insert states result", wxICON_INFORMATION | wxOK);
			dialog.CentreOnParent();
			dialog.ShowModal();
		}
	}
}

//-----------------------------------------------------------------------
void AnimEditor::RemoveSourceStatesFromGraph(const EArray<ASClass*>& from, int ConnectTypeFlags, bool ConnectOrInterpolate)
{
	ParamConfigAnm* PConfig = GetParamConfig();
	ACharacterClass* character = PConfig->GetCurrentCharacterPtr();
	enf_assert(character);
	ASGroup* group = character->GetCurrentGroupPtr();
	enf_assert(group);
	const char* GroupName = group->GetName().c_str();

	DiagramWindow* DiagWin = GetDiagramWindow();
	ESet<DPConnetionRef>& connections = DiagWin->GetConnections();
	EArray<ASClass*> ToRemove;

	for(uint n = 0; n < connections.GetCardinality(); n++)
	{
		DPConnetionRef& ref = connections[n];
		DNConnection* connection = ref.GetConnection();
		int ConnectionFlags = (int)connection->GetCategory() + 1;

		if(ConnectOrInterpolate)
		{
			if(!(ConnectionFlags & ConnectTypeFlags))	//one from flags must pass
				continue;
		}
		else
		{
			if(ConnectionFlags != ConnectTypeFlags)	//all flags must be equal
				continue;
		}

		DPCSocket& CSource = connection->GetSource();
		DPCSocket& CTarget = connection->GetTarget();
		DiagramNode* SourceNode = CSource.GetNode();
		DiagramNode* TargetNode = CTarget.GetNode();
		ASClass* SourceState = (ASClass*)SourceNode->GetClientData();
		ASClass* TargetState = (ASClass*)TargetNode->GetClientData();

		if(from.GetIndexOf(TargetState) != INDEX_NOT_FOUND && from.GetIndexOf(SourceState) == INDEX_NOT_FOUND)	//connection from state to the same state is ignored here
		{
			if(ToRemove.GetIndexOf(SourceState) == INDEX_NOT_FOUND)
				ToRemove.Insert(SourceState);
		}
	}

	if(ToRemove.GetCardinality() > 0)
	{
		HistoryPointAnm* point = CreateHistoryPoint("Remove Nodes");

		EADeleteSGroupRefs* RedoAction = ENF_NEW EADeleteSGroupRefs(group->GetName().c_str(), character->GetName());
		EACreateSGroupRefs* UndoAction = ENF_NEW EACreateSGroupRefs(character->GetName());
		uint NumToRemove = 0;

		for(uint n = 0; n < ToRemove.GetCardinality(); n++)
		{
			ASClass* state = ToRemove[n];
			ASGroupRef* GroupRef = state->FindGroupRef(GroupName);

			if(GroupRef == NULL)
				continue;

			RedoAction->AddStateName(state->GetName());
			UndoAction->AddStateRefData(state->GetName(), *GroupRef);
			NumToRemove++;
		}

		if(NumToRemove > 0)
		{
			point->AppendRedoAction(RedoAction);
			point->AppendUndoAction(UndoAction);

			EArray<DString> UndoClassSel;
			EArray<DString> RedoClassSel;
			GetSelection(UndoClassSel);

			point->SetRedoClassSel(RedoClassSel);
			point->SetUndoClassSel(UndoClassSel);

			DString SelPropID = GetPropertySelectionID();
			point->SetRedoPropSel(SelPropID.c_str());
			point->SetUndoPropSel(SelPropID.c_str());

			AddHistoryPoint(point);

			RealizeActions(point->GetRedoActions());
			UpdateUndoRedoAccessibility();

			UpdateCurrentGroupGui();
			DiagWin->RepaintFromClient();
			SetChanged(true);
			MFrame->UpdateTitle();
			UpdateSaveAccesibility();
		}
		else
		{
			SAFE_DELETE(point);
		}
	}
}

//-----------------------------------------------------------------------
void AnimEditor::RemoveTargetStatesFromGraph(const EArray<ASClass*>& from, int ConnectTypeFlags, bool ConnectOrInterpolate)
{
	ParamConfigAnm* PConfig = GetParamConfig();
	ACharacterClass* character = PConfig->GetCurrentCharacterPtr();
	enf_assert(character);
	ASGroup* group = character->GetCurrentGroupPtr();
	enf_assert(group);
	const char* GroupName = group->GetName().c_str();
	DiagramWindow* DiagWin = GetDiagramWindow();

	ESet<DPConnetionRef>& connections = DiagWin->GetConnections();
	EArray<ASClass*> ToRemove;

	for(uint n = 0; n < connections.GetCardinality(); n++)
	{
		DPConnetionRef& ref = connections[n];
		DNConnection* connection = ref.GetConnection();
		int ConnectionFlags = (int)connection->GetCategory() + 1;

		if(ConnectOrInterpolate)
		{
			if(!(ConnectionFlags & ConnectTypeFlags))	//one from flags must pass
				continue;
		}
		else
		{
			if(ConnectionFlags != ConnectTypeFlags)	//all flags must be equal
				continue;
		}

		DPCSocket& CSource = connection->GetSource();
		DPCSocket& CTarget = connection->GetTarget();
		DiagramNode* SourceNode = CSource.GetNode();
		DiagramNode* TargetNode = CTarget.GetNode();
		ASClass* SourceState = (ASClass*)SourceNode->GetClientData();
		ASClass* TargetState = (ASClass*)TargetNode->GetClientData();

		if(from.GetIndexOf(SourceState) != INDEX_NOT_FOUND && from.GetIndexOf(TargetState) == INDEX_NOT_FOUND)	//connection from state to the same state is ignored here
		{
			if(ToRemove.GetIndexOf(SourceState) == INDEX_NOT_FOUND)
				ToRemove.Insert(TargetState);
		}
	}

	if(ToRemove.GetCardinality() > 0)
	{
		HistoryPointAnm* point = CreateHistoryPoint("Remove Nodes");

		EADeleteSGroupRefs* RedoAction = ENF_NEW EADeleteSGroupRefs(group->GetName().c_str(), character->GetName());
		EACreateSGroupRefs* UndoAction = ENF_NEW EACreateSGroupRefs(character->GetName());
		uint NumToRemove = 0;

		for(uint n = 0; n < ToRemove.GetCardinality(); n++)
		{
			ASClass* state = ToRemove[n];
			ASGroupRef* GroupRef = state->FindGroupRef(GroupName);

			if(GroupRef == NULL)
				continue;

			RedoAction->AddStateName(state->GetName());
			UndoAction->AddStateRefData(state->GetName(), *GroupRef);
			NumToRemove++;
		}

		if(NumToRemove > 0)
		{
			point->AppendRedoAction(RedoAction);
			point->AppendUndoAction(UndoAction);

			EArray<DString> UndoClassSel;
			EArray<DString> RedoClassSel;
			GetSelection(UndoClassSel);

			point->SetRedoClassSel(RedoClassSel);
			point->SetUndoClassSel(UndoClassSel);

			DString SelPropID = GetPropertySelectionID();
			point->SetRedoPropSel(SelPropID.c_str());
			point->SetUndoPropSel(SelPropID.c_str());

			AddHistoryPoint(point);

			RealizeActions(point->GetRedoActions());
			UpdateUndoRedoAccessibility();

			UpdateCurrentGroupGui();
			DiagWin->RepaintFromClient();
			SetChanged(true);
			MFrame->UpdateTitle();
			UpdateSaveAccesibility();
		}
		else
		{
			SAFE_DELETE(point);
		}
	}
}

//-----------------------------------------------------------------------
void AnimEditor::CreateClassRenameFeedbackActions(PCClass* cl, const char* NewName, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions)
{
	ParamConfigAnm* config = GetParamConfig();

	//get all "action" classes
	EArray<PCClass*> actions;
	GetAllActionClasses(actions);

	ASClass* state = dynamic_cast<ASClass*>(cl);

	if(state)
	{
		//if state class was renamed, rename all referencies to this state in other state classes
		ACharacterClass* character = state->GetCharacter();
		enf_assert(character);

		EArray<ASClass*>& states = character->GetStates();

		for(uint n = 0; n < states.GetCardinality(); n++)
		{
			ASClass* CurState = states[n];
			PCNode* node = CurState->GetMember(ConnectToArrayName);
			PCArray* arr = node ? node->ToArray() : NULL;
			enf_assert(arr);

			if(!arr)
				continue;

			EArray<PCVar*> refs;
			FindStateRefsFromArray(arr, state, refs);

			for(uint r = 0; r < refs.GetCardinality(); r++)
			{
				PCVar* ref = refs[r];
				DString ID = ref->GetId();
				RedoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), NewName));
				UndoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), ref->GetValue()));
			}

			node = CurState->GetMember(InterpolateToArrayName);
			arr = node ? node->ToArray() : NULL;
			enf_assert(arr);

			if(!arr)
				continue;

			refs.Clear();
			FindStateRefsFromArray(arr, state, refs);

			for(uint r = 0; r < refs.GetCardinality(); r++)
			{
				PCVar* ref = refs[r];
				DString ID = ref->GetId();
				RedoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), NewName));
				UndoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), ref->GetValue()));
			}
		}

		//rename all referencies in action classes
		for(uint n = 0; n < actions.GetCardinality(); n++)
		{
			PCClass* action = actions[n];

			for(uint n = 0; n < action->GetNodesCount(); n++)
			{
				PCNode* node = action->GetNode(n);
				PCVar* var = node->ToVariable();
				PCArray* arr = node->ToArray();

				if(!var && !arr)
					continue;

				if(var)
				{
					if(strcmpi(var->GetValue(), cl->GetName()) == 0)
					{
						DString ID = var->GetId();
						RedoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), NewName));
						UndoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), var->GetValue()));
					}
				}
				else if(arr)
				{
					for(uint a = 0; a < arr->GetCardinality(); a++)
					{
						PCNode* node = arr->GetItem(a);
						PCVar* var = node->ToVariable();

						if(!var)	//ignore subarrays
							continue;

						if(strcmpi(var->GetValue(), cl->GetName()) == 0)
						{
							DString ID = var->GetId();
							RedoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), NewName));
							UndoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), var->GetValue()));
						}
					}
				}
			}
		}
	}
	else if(actions.GetIndexOf(cl) != INDEX_NOT_FOUND)	//is "action" class. rename all referencies in state classes from all characters
	{
		for(uint ch = 0; ch < config->GetCharactersCount(); ch++)
		{
			ACharacterClass* character = config->GetCharacter(ch);
			EArray<ASClass*>& states = character->GetStates();

			for(uint n = 0; n < states.GetCardinality(); n++)
			{
				ASClass* CurState = states[n];
				PCNode* node = CurState->GetMember("actions");
				PCVar* var = node ? node->ToVariable() : NULL;

				if(!var)
					continue;

				if(strcmpi(var->GetValue(), cl->GetName()) == 0)
				{
					DString ID = var->GetId();
					RedoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), NewName));
					UndoActions.Insert(ENF_NEW EAModifyVariable(ID.c_str(), var->GetValue()));
				}
			}
		}
	}
}

//-----------------------------------------------------------------------
void AnimEditor::GetAllActionClasses(EArray<PCClass*>& result)
{
	ParamConfigAnm* config = g_aeditor->GetParamConfig();
	PCClass* ActionsOwner = config->FindClassNode(CfgMovesBasicClassID);

	if(!ActionsOwner)
		return;

	PCClass* actions = ActionsOwner->GetClassMember("Actions");

	if(!actions)
		return;

	for(uint n = 0; n < actions->GetNodesCount(); n++)
	{
		PCNode* node = actions->GetNode(n);
		PCClass* cl = node->ToClass();

		if(!cl)
			continue;

		GetAllBaseClassesFrom(cl, result);
	}

	for(uint n = 0; n < result.GetCardinality(); n++)
	{
		PCClass* cl = result[n];
		cl->MarkFlags = 0;
	}
}

//-----------------------------------------------------------------------
void AnimEditor::GetAllBaseClassesFrom(PCClass* cl, EArray<PCClass*>& result)
{
	if(cl->MarkFlags != 0)	//every class only once
		return;

	result.Insert(cl);
	cl->MarkFlags = 1;

	PCClass* base = cl->GetBase();

	if(base)
		GetAllBaseClassesFrom(base, result);
}

//-----------------------------------------------------------------------
void AnimEditor::GetSelectedStates(EArray<ASClass*>& states)
{
	for(uint n = 0; n < selection.GetCardinality(); n++)
	{
		PCClass* cl = selection[n];
		ASClass* state = dynamic_cast<ASClass*>(cl);

		if(!state)
			continue;

		states.Insert(state);
	}
}

//-----------------------------------------------------------------------
void AnimEditor::CreateStatesGroup(const char* GroupName)
{
	enf_assert(GroupName);
	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();
	enf_assert(character);
	enf_assert(character->FindGroup(GroupName) == NULL);

	DiagramWindow* DiagWin = GetDiagramWindow();

	HistoryPointAnm* point = CreateHistoryPoint("Create Group");

	EACreateSGroup* RedoAction = ENF_NEW EACreateSGroup(GroupName, character->GetName());
	EADeleteSGroup* UndoAction = ENF_NEW EADeleteSGroup(GroupName, character->GetName());

	point->AppendRedoAction(RedoAction);
	point->AppendUndoAction(UndoAction);

	EArray<DString> ClassSel;
	GetSelection(ClassSel);
	point->SetRedoClassSel(ClassSel);
	point->SetUndoClassSel(ClassSel);

	DString SelPropID = GetPropertySelectionID();
	point->SetRedoPropSel(SelPropID.c_str());
	point->SetUndoPropSel(SelPropID.c_str());

	AddHistoryPoint(point);

	point->SetRedoGroupSel(GroupName);	//override of set in AddHistoryPoint

	RealizeActions(point->GetRedoActions());
	UpdateUndoRedoAccessibility();

	ASGroup* CreatedGroup = character->FindGroup(GroupName);
	StateGroupsPanel* GroupsPanel = GetMainFrame()->GetGroupsPanel();
	GroupsPanel->AddGroup(GroupName);
	GroupsPanel->SetCurrentGroup(character->GetCurrentGroup());

	OnASGroupChanged();
//	UpdateCurrentGroupGui();
	DiagWin->RepaintFromClient();
	SetChanged(true);
	MFrame->UpdateTitle();
	UpdateSaveAccesibility();
}

//-----------------------------------------------------------------------
void AnimEditor::DeleteStatesGroup(const char* GroupName)
{
	enf_assert(GroupName);
	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();
	enf_assert(character);
	ASGroup* group = character->FindGroup(GroupName);
	enf_assert(group);

	DiagramWindow* DiagWin = GetDiagramWindow();

	HistoryPointAnm* point = CreateHistoryPoint("Delete Group");

	//first must be deleted all referencies to this group
	EADeleteSGroupRefs* RAction = ENF_NEW EADeleteSGroupRefs(GroupName, character->GetName());
	EACreateSGroupRefs* UAction = ENF_NEW EACreateSGroupRefs(character->GetName());

	const EArray<ASClass*>& states = character->GetStates();

	for(uint n = 0; n < states.GetCardinality(); n++)
	{
		ASClass* state = states[n];
		ASGroupRef* ref = state->FindGroupRef(GroupName);

		if(!ref)
			continue;

		RAction->AddStateName(state->GetName());
		UAction->AddStateRefData(state->GetName(), *ref);
	}

	point->AppendRedoAction(RAction);
	point->AppendUndoAction(UAction);

	//now can delete group
	EADeleteSGroup* RedoAction = ENF_NEW EADeleteSGroup(GroupName, character->GetName());
	EACreateSGroup* UndoAction = ENF_NEW EACreateSGroup(group->Clone(), character->GetName());

	point->AppendRedoAction(RedoAction);
	point->AppendUndoAction(UndoAction);

	EArray<DString> ClassSel;
	GetSelection(ClassSel);
	point->SetRedoClassSel(ClassSel);
	point->SetUndoClassSel(ClassSel);

	DString SelPropID = GetPropertySelectionID();
	point->SetRedoPropSel(SelPropID.c_str());
	point->SetUndoPropSel(SelPropID.c_str());

	AddHistoryPoint(point);

	RealizeActions(point->GetRedoActions());
	UpdateUndoRedoAccessibility();

	group = character->GetCurrentGroupPtr();

	if(group)
		point->SetRedoGroupSel(group->GetName().c_str());	//after realize actions is now selected another group
	else
		point->SetRedoGroupSel("");	//this was set in AddHistoryPoint() funtion. here must be overwriten

	StateGroupsPanel* GroupsPanel = GetMainFrame()->GetGroupsPanel();
	GroupsPanel->RemoveGroup(GroupName);
	GroupsPanel->SetCurrentGroup(character->GetCurrentGroup());
	OnASGroupChanged();

	DiagWin->RepaintFromClient();
	SetChanged(true);
	MFrame->UpdateTitle();
	UpdateSaveAccesibility();
}

//-----------------------------------------------------------------------
void AnimEditor::Copy()
{
	DiagramWindow* DiagWin = GetDiagramWindow();

	if(DiagWin->GetSelectionCount() == 0)
		return;

	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();

	if(!character)
		return;

	ASGroup* group = character->GetCurrentGroupPtr();

	if(!group)
		return;

	DestroyCopyData();
	CopyCharacter = character->GetName();
	EArray<DiagramNode*> CopiedNodes;

	for(uint n = 0; n < DiagWin->GetSelectionCount(); n++)
	{
		DiagramNode* node = DiagWin->GetSelectionMember(n);
		ASClass* state = (ASClass*)node->GetClientData();
		enf_assert(state);
		ASGroupRef* GroupRef = state->FindGroupRef(group->GetName().c_str());
		enf_assert(GroupRef);
		CopyData.Insert(ENF_NEW CopyStorage(state->GetName(), *GroupRef));
		CopiedNodes.Insert(node);
	}

	if(CopiedNodes.GetCardinality() > 0)
	{
		CopyNodesRect = DiagWin->GetBoundBoxOfNodes(CopiedNodes);
		CopyViewPoint = DiagWin->GetViewPoint();
		CopyZoomLevel = DiagWin->GetZoomLevel();
	}

	UpdateCopyPasteAccesibility();
}

//-----------------------------------------------------------------------
void AnimEditor::Paste()
{
	DiagramWindow* DiagWin = GetDiagramWindow();
	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();

	if(!character)
	{
		wxMessageDialog dialog(MFrame, "Paste is possible only if any character is set", "Paste", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return;
	}

	if(strcmpi(character->GetName(), CopyCharacter.c_str()) != 0)
	{
		wxMessageDialog dialog(MFrame, "Cannot copy & paste betwen two differents characters", "Paste", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return;
	}

	ASGroup* group = character->GetCurrentGroupPtr();

	if(!group)
	{
		wxMessageDialog dialog(MFrame, "Paste is possible only if active states group is available. Use G+ button to create new states group", "Paste", wxICON_INFORMATION | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return;
	}

	if(CopyData.GetCardinality() == 0)
		return;

	CopyStorage* first = CopyData[0];

	if(first->GetStateGraphRef().GetGroupName() == group->GetName())
		return;

	const char* GroupName = group->GetName().c_str();

	HistoryPointAnm* point = CreateHistoryPoint("Paste Nodes");
	EACreateSGroupRefs* RedoAction = ENF_NEW EACreateSGroupRefs(character->GetName());
	EADeleteSGroupRefs* UndoAction = ENF_NEW EADeleteSGroupRefs(GroupName, character->GetName());
	EArray<ASClass*> PastedStates;

	for(uint n = 0; n < CopyData.GetCardinality(); n++)
	{
		CopyStorage* storage = CopyData[n];
		
		const char* StateName = storage->GetStateName().c_str();
		ASClass* state = character->FindStateClass(StateName);

		if(!state)	//during time betwen copy and paste actions could be deleted. it's ok if NULL
			continue;

		if(state->FindGroupRef(GroupName))	//if now available in graf (has refrence to graph)
			continue;

		ASGroupRef GroupRef = storage->GetStateGraphRef();
		GroupRef.SetGroupName(GroupName);

		RedoAction->AddStateRefData(state->GetName(), GroupRef);
		UndoAction->AddStateName(state->GetName());
		PastedStates.Insert(state);
	}

	point->AppendRedoAction(RedoAction);
	point->AppendUndoAction(UndoAction);

	if(RedoAction->GetStateRefDataCount() > 0)
	{
		if(!CopyNodesRect.Overlap(DiagWin->GetVisibleRect())) //if pasted nodes will be not visible, need do chanche zoom and view pos of graph to values at copy action
		{
			group->SetGraphViewPoint(CopyViewPoint);
			group->SetGraphZoomLevel(CopyZoomLevel);
			DiagWin->SetZoomLevel(CopyZoomLevel);
			DiagWin->SetViewPoint(CopyViewPoint);
		}

		EArray<DString> UndoClassSel;
		EArray<DString> RedoClassSel;
		GetSelection(UndoClassSel);

		for(uint n = 0; n < PastedStates.GetCardinality(); n++)
		{
			ASClass* st = PastedStates[n];
			DString StateID = st->GetId();
			RedoClassSel.Insert(StateID.c_str());
		}

		point->SetRedoClassSel(RedoClassSel);
		point->SetUndoClassSel(UndoClassSel);

		DString SelPropID = GetPropertySelectionID();
		point->SetRedoPropSel(SelPropID.c_str());
		point->SetUndoPropSel(SelPropID.c_str());

		AddHistoryPoint(point);

		RealizeActions(point->GetRedoActions());
		UpdateUndoRedoAccessibility();

		UpdateCurrentGroupGui();
		SelectStatesInGraph(PastedStates);
		DiagWin->SetFocus();
		DiagWin->RepaintFromClient();
		SetChanged(true);
		MFrame->UpdateTitle();
		UpdateSaveAccesibility();
	}
	else
	{
		SAFE_DELETE(point);
	}

	UpdateCopyPasteAccesibility();
}

//-----------------------------------------------------------------------
void AnimEditor::DestroyCopyData()
{
	for(uint n = 0; n < CopyData.GetCardinality(); n++)
	{
		CopyStorage* storage = CopyData[n];
		delete storage;
	}
	CopyData.Clear();
	CopyCharacter.clear();
}

//-----------------------------------------------------------------------
void AnimEditor::UpdateCopyPasteAccesibility()
{
	bool copy = true;
	bool paste = true;

	DiagramWindow* DiagWin = GetDiagramWindow();

	if(DiagWin->GetSelectionCount() == 0)
		copy = false;

	ACharacterClass* character = GetParamConfig()->GetCurrentCharacterPtr();

	if(!character)
	{
		copy = false;
		paste = false;
	}

	ASGroup* group = character ? character->GetCurrentGroupPtr() : NULL;

	if(!group)
	{
		copy = false;
		paste = false;
	}

	if(CopyData.GetCardinality() == 0)
		paste = false;

	wxMenu* menu = MFrame->GetEditMenu();
	menu->Enable(MENU_COPY, copy);
	menu->Enable(MENU_PASTE, paste);

	wxToolBar* toolbar = MFrame->GetMainToolBar();
	toolbar->EnableTool(MENU_COPY, copy);
	toolbar->EnableTool(MENU_PASTE, paste);
}

//---------------------------------------------------------------------------------------------
void AnimEditor::UpdateSaveAccesibility()
{
	GenericEditor::UpdateSaveAccesibility();
/*
	bool enable = AnyChanges();
	MFrame->GetFileMenu()->Enable(MENU_IMPORT, enable);
	MFrame->GetFileMenu()->Enable(MENU_EXPORT, enable);*/
}