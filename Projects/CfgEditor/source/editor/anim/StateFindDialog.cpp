
#include "precomp.h"
#include "StateFindDialog.h"
#include "anim/AnimEditor.h"
#include "anim/ImageListAnm.h"
#include "base/ParamFileTree.h"
#include "common/XmlConfig.h"
#include "common/WxNativeConvert.h"
#include "wxExtension/FileTextBox.h"

BEGIN_EVENT_TABLE(SFListBox, wxVListBox)
	EVT_KEY_DOWN(SFListBox::OnKeyDown)
	EVT_MOTION(SFListBox::OnMouseMove)
	EVT_LEFT_DOWN(SFListBox::OnLMBDown)
	EVT_LEFT_UP(SFListBox::OnLMBUp)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------
SFListBox::SFListBox(wxWindow* parent)
		:wxVListBox(parent, -1, wxDefaultPosition, wxSize(400, -1), wxLB_MULTIPLE | wxSUNKEN_BORDER)
{
	dragging = false;
	ReselectOnLMBUp = false;
}

//-------------------------------------------------------------------------------
void SFListBox::ClearAll()
{
	StateItems.Clear();
	Clear();
}

//-------------------------------------------------------------------------------
void SFListBox::Append(SFListBoxItem& item)
{
	StateItems.Insert(item);
}

//-------------------------------------------------------------------------------
const wxString& SFListBox::GetString(int n)
{
	return StateItems[n].name;
}

//-------------------------------------------------------------------------------
void SFListBox::OnDrawItem(wxDC& dc, const wxRect& rect, size_t n) const
{
	const SFListBoxItem& item = StateItems[n];
	enf_assert(n >= 0);
	g_ImageList->Draw(item.IconIndex, dc, rect.x + 1, rect.y + 1); 

	dc.SetFont(g_aeditor->GetMainFrame()->GetParamTree()->GetFont());

	if(IsSelected(n))
		dc.SetTextForeground(*wxWHITE);
	else
		dc.SetTextForeground(*wxBLACK);

	dc.DrawText(item.name, rect.x + 22, rect.y + 2);
}

//------------------------------------------------------------------------------------------
void SFListBox::OnDrawSeparator(wxDC& dc, wxRect& rect, size_t n) const
{
	enf_assert(n >= 0);
	int y = rect.y + rect.height - 2;
	dc.SetPen(wxPen(wxColour(159, 156, 150)));
	dc.DrawLine(rect.x, y + 1, rect.x + rect.width, y + 1);
}

//-------------------------------------------------------------------------------
wxCoord SFListBox::OnMeasureItem(size_t n) const
{
	return 19;
}

//-------------------------------------------------------------------------------
void SFListBox::OnKeyDown(wxKeyEvent& event)
{
	if(event.ControlDown() && event.GetKeyCode() == 'A')
	{
		Freeze();

		for(uint n = 0; n < GetItemCount(); n++)
			Select(n);

		Thaw();
		return;
	}
	event.Skip();
}

//-------------------------------------------------------------------------------
void SFListBox::OnLMBDown(wxMouseEvent& event)
{
	int index = HitTest(event.GetPosition());

	if(index != wxNOT_FOUND)
	{
		if(IsSelected(index))
		{
			ReselectOnLMBUp = true;
			return;	//do not next proccesing event. LMBup event is disabled
		}
	}

	event.Skip();
}

//-------------------------------------------------------------------------------
void SFListBox::OnLMBUp(wxMouseEvent& event)
{
	if(ReselectOnLMBUp)
	{
		int HitIndex = HitTest(event.GetPosition());

		unsigned long cookie;
		int index = GetFirstSelected(cookie);
		while (index != wxNOT_FOUND)
		{
			if(index != HitIndex)
				Select(index, false);
			else
				Select(index, true);

			index = GetNextSelected(cookie);
		}

		ReselectOnLMBUp = false;
	}

	event.Skip();
}

//-------------------------------------------------------------------------------
void SFListBox::OnLMBDoubleClick(wxMouseEvent& event)
{
	unsigned long cookie;
	int index = GetFirstSelected(cookie);
	while (index != wxNOT_FOUND)
	{
		Select(index, false);
		index = GetNextSelected(cookie);
	}

	event.Skip();
}

//-------------------------------------------------------------------------------
void SFListBox::OnRMBDown(wxMouseEvent& event)
{
	unsigned long cookie;
	int index = GetFirstSelected(cookie);
	while (index != wxNOT_FOUND)
	{
		Select(index, false);
		index = GetNextSelected(cookie);
	}

	event.Skip();
}

//-------------------------------------------------------------------------------
void SFListBox::OnMouseMove(wxMouseEvent& event)
{
	if(!dragging)
	{
		wxMouseState MouseInfo = wxGetMouseState();

		if(MouseInfo.LeftDown())
		{
			ReselectOnLMBUp = false;
			ACharacterClass* character = g_aeditor->GetParamConfig()->GetCurrentCharacterPtr();
			ASGroup* CurrentGroup = character ? character->GetCurrentGroupPtr() : NULL;

			if(character)
			{
				DString CurrentGroupName = CurrentGroup ? CurrentGroup->GetName() : "";
				EArray<ASClass*> StatesToDrop;
				EArray<int> StatesToDropInd;

				unsigned long cookie;
				int index = GetFirstSelected(cookie);
				while (index != wxNOT_FOUND)
				{
					const wxString& item = GetString(index);
					ASClass* state = character->FindStateClass(item.c_str());
					enf_assert(state);

					if(state)
					{
						StatesToDrop.Insert(state);
						StatesToDropInd.Insert(index);
					}

					index = GetNextSelected(cookie);
				}

				if(StatesToDrop.GetCardinality() > 0)
				{
					wxFileDataObject ddata;

					for(uint n = 0; n < StatesToDrop.GetCardinality(); n++)
					{
						ASClass* state = StatesToDrop[n];
						DString StateID = state->GetId();
						ddata.AddFile(StateID.c_str());
					}

					wxDropSource source(ddata, this, wxDROP_ICON(dnd_copy), wxDROP_ICON(dnd_move), wxDROP_ICON(dnd_none));

					bool MoveByDefault = true;
					bool MoveAllowed = true;

					int flags = 0;
					if ( MoveByDefault )
						flags |= wxDrag_DefaultMove;
					else if ( MoveAllowed )
						flags |= wxDrag_AllowMove;

					dragging = true;
					wxDragResult result = source.DoDragDrop(flags);
					dragging = false;

					if(!CurrentGroupName.empty())
					{
						for(uint n = 0; n < StatesToDrop.GetCardinality(); n++)
						{
							ASClass* state = StatesToDrop[n];
							SFListBoxItem& item = StateItems[StatesToDropInd[n]];

							if(state->FindGroupRef(CurrentGroupName.c_str()))
								item.IconIndex = BI_STATE_CLASS_ON_GRAPH;
							else
								item.IconIndex = BI_STATE_CLASS;
						}
					}

					Refresh();
				}
			}
		}
	}
	event.Skip();
}


BEGIN_EVENT_TABLE(StateFindDialog, wxDialog)
	EVT_BUTTON(-1, StateFindDialog::OnButtonClick)
	EVT_COMBOBOX(-1, StateFindDialog::OnComboBoxChanged)
	EVT_KEY_DOWN(StateFindDialog::OnKeyDown)
	EVT_TEXT(-1, StateFindDialog::OnTextChanged)
END_EVENT_TABLE()

//===============================================================================
StateFindDialog::StateFindDialog(wxWindow* parent)
	:wxDialog(parent, -1, "Find states", wxDefaultPosition, wxSize(700, 480), wxDEFAULT_DIALOG_STYLE | wxMAXIMIZE_BOX)
{
	ResultLabel = new wxStaticText(this, -1, "", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);	

//	FindButton = new wxButton(this, BUTTON_FIND, _T("Find"), wxDefaultPosition, wxSize(-1, 30));
	ExitButton = new wxButton(this, wxID_CANCEL, _T("Exit"), wxDefaultPosition, wxSize(-1, 30));
	ResultListBox = new SFListBox(this);

	wxStaticText* StringLabel = new wxStaticText(this, -1, "string", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);	
	StringBox = new wxTextCtrl(this, EDITBOX_STRING, wxEmptyString, wxDefaultPosition, wxSize(-1, 21), 0);
				

	AnimConfigDef*	AnimDef = g_aeditor->GetAnimDef();

	wxStaticBox* PrefGroupsSizerBox = new wxStaticBox(this, -1, "first sequence", wxDefaultPosition, wxDefaultSize);
	wxBoxSizer* PrefGroupsSizer = new wxStaticBoxSizer(PrefGroupsSizerBox, wxVERTICAL);
	PrefGroupsSizer->AddSpacer(12);

	wxStaticBox* PrefGroupsSizer2Box = new wxStaticBox(this, -1, "second sequence", wxDefaultPosition, wxDefaultSize);
	wxBoxSizer* PrefGroupsSizer2 = new wxStaticBoxSizer(PrefGroupsSizer2Box, wxVERTICAL);
	PrefGroupsSizer2->AddSpacer(12);

	int ComboCounter = 0;
	for(uint n = 0; n < AnimDef->GetPrefixGroupsCount(); n++)
	{
		ASPrefixGroupDef* PrefixGroup = AnimDef->GetPrefixGroup(n);

		wxStaticText* PefixComboLabel = new wxStaticText(this, -1, PrefixGroup->GetName(), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
		wxStaticText* PefixCombo2Label = new wxStaticText(this, -1, PrefixGroup->GetName(), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
		wxComboBox* PefixCombo = new wxComboBox(this, COMBOBOX_FIRST_PREFIX_GROUP + ComboCounter, wxEmptyString, wxDefaultPosition, wxSize(130, -1), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY, wxDefaultValidator, PrefixGroup->GetName());
		wxComboBox* PefixCombo2 = new wxComboBox(this, COMBOBOX_FIRST_PREFIX_GROUP2 + ComboCounter, wxEmptyString, wxDefaultPosition, wxSize(130, -1), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY, wxDefaultValidator, PrefixGroup->GetName());
		ComboCounter++;

		PefixCombo->Append("");
		PefixCombo2->Append("");

		wxString item;

		for(uint g = 0; g < PrefixGroup->GetPrefixesCount(); g++)
		{
			const ASPrefixDef& prefix = PrefixGroup->GetPrefixDef(g);

			const char* PrefixName = prefix.GetName();
			const char* PrefixLabel = prefix.GetLabel();

			if(!PrefixName)
			{
				assert(false);
				continue;
			}

			item = PrefixName;

			if(PrefixLabel && strlen(PrefixLabel) > 0)
			{
				item += " (";
				item += PrefixLabel;
				item += " )";
			}

			PefixCombo->Append(item);
			PefixCombo2->Append(item);
		}

		PrefixCombos.Insert(PefixCombo);
		PrefixCombos2.Insert(PefixCombo2);

		if(!PrefixCombos.IsEmpty())
		{
			PrefGroupsSizer->AddSpacer(4);
			PrefGroupsSizer2->AddSpacer(4);
		}

		PrefGroupsSizer->Add(PefixComboLabel, 0, wxALIGN_TOP | wxLEFT | wxRIGHT, 5);
		PrefGroupsSizer->Add(PefixCombo, 0, wxALIGN_TOP | wxLEFT | wxRIGHT, 5);

		PrefGroupsSizer2->Add(PefixCombo2Label, 0, wxALIGN_TOP | wxLEFT | wxRIGHT, 5);
		PrefGroupsSizer2->Add(PefixCombo2, 0, wxALIGN_TOP | wxLEFT | wxRIGHT, 5);
	}

	wxBoxSizer* CombosSizer = new wxBoxSizer(wxHORIZONTAL);
	CombosSizer->Add(PrefGroupsSizer, 0, wxGROW | wxALIGN_TOP | wxALL, 0);
	CombosSizer->AddSpacer(10);
	CombosSizer->Add(PrefGroupsSizer2, 0, wxGROW | wxALIGN_TOP | wxALL, 0);

	wxBoxSizer* LeftSizer = new wxBoxSizer(wxVERTICAL);
	LeftSizer->Add(StringLabel, 0, wxALIGN_TOP | wxALL, 0);
	LeftSizer->Add(StringBox, 0, wxGROW | wxALIGN_TOP | wxALL, 0);
	LeftSizer->AddSpacer(8);
	LeftSizer->Add(CombosSizer, 0, wxGROW | wxALIGN_TOP | wxALL, 0);
	LeftSizer->AddSpacer(10);
//	LeftSizer->Add(FindButton, 0, wxGROW | wxALIGN_TOP | wxALL, 0);
//	LeftSizer->AddSpacer(10);
	LeftSizer->Add(ExitButton, 0, wxGROW | wxALIGN_TOP | wxALL, 0);

	wxBoxSizer* ResultSizer = new wxBoxSizer(wxVERTICAL);
	ResultSizer->Add(ResultLabel, 0, wxGROW | wxALIGN_TOP | wxALL, 0);
	ResultSizer->Add(ResultListBox, 1, wxGROW | wxALIGN_TOP | wxALL, 0);

	wxBoxSizer* ContentSizer = new wxBoxSizer(wxHORIZONTAL);
	ContentSizer->Add(LeftSizer, 0, wxGROW | wxALIGN_TOP | wxALL, 10);
	ContentSizer->Add(ResultSizer, 1, wxGROW | wxALIGN_TOP | wxRIGHT | wxTOP | wxBOTTOM, 10);

	wxBoxSizer* MainSizer = new wxBoxSizer(wxVERTICAL);
	MainSizer->Add(ContentSizer, 1, wxGROW | wxALIGN_TOP | wxALL, 0);
	MainSizer->AddSpacer(10);

	SetSizer(MainSizer);
	MainSizer->Fit(this);

	StringBox->SetFocus();

	FindMasks[0] = new ASPrefixMask();
	FindMasks[1] = new ASPrefixMask();
}

//-------------------------------------------------------------------------------
StateFindDialog::~StateFindDialog()
{
	SAFE_DELETE(FindMasks[0]);
	SAFE_DELETE(FindMasks[1]);
}

//-------------------------------------------------------------------------------
void StateFindDialog::OnButtonClick(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case BUTTON_FIND:
		UpdateResult();
		break;
	case wxID_CANCEL:
		ShowNormal(false);
		break;
	}
}

//-------------------------------------------------------------------------------
void StateFindDialog::OnKeyDown(wxKeyEvent& event)
{
	switch(event.GetKeyCode())
	{
	case WXK_ESCAPE:
		ShowNormal(false);
		break;
	}
	event.Skip();
}

//-------------------------------------------------------------------------------
void StateFindDialog::OnComboBoxChanged(wxCommandEvent& event)
{
	if(event.GetId() >= COMBOBOX_FIRST_PREFIX_GROUP)
	{
		UpdatePrefixMask();
		UpdateResult();
	}
}

//-------------------------------------------------------------------------------
void StateFindDialog::OnTextChanged(wxCommandEvent& event)
{
	if(event.GetId() >= EDITBOX_STRING)
	{
		UpdatePrefixMask();
		UpdateResult();
	}
}

//-------------------------------------------------------------------------------
 void StateFindDialog::ShowNormal(const bool show)
 {
	 if(show)
		UpdateResult();

	 Show(show);
 }

//-------------------------------------------------------------------------------
void StateFindDialog::UpdatePrefixMask()
{
	FindMasks[0]->Clear();
	FindMasks[1]->Clear();

	for(uint n = 0; n < PrefixCombos.GetCardinality(); n++)
	{
		wxComboBox* combo = PrefixCombos[n];
		int selected = combo->GetSelection();

		if(selected <= 0)	//-1 unselected, 0 - nic neni zvoleno
			continue;

		FindMasks[0]->Set(n, selected - 1);
	}

	for(uint n = 0; n < PrefixCombos2.GetCardinality(); n++)
	{
		wxComboBox* combo = PrefixCombos2[n];
		int selected = combo->GetSelection();

		if(selected <= 0)	//-1 unselected, 0 - nic neni zvoleno
			continue;

		FindMasks[1]->Set(n, selected - 1);
	}
}

//-----------------------------------------------------------------------
int __cdecl CompareStates(const void* a, const void* b)
{
	ASClass* s1 = *(ASClass**)a;
	ASClass* s2 = *(ASClass**)b;
	const AnimStateID& id1 = s1->GetAnimId();
	const AnimStateID& id2 = s2->GetAnimId();

	int result;
	if(id1 == id2)
		result = 0;
	else
		result = (id1 < id2) ? -1 : 1;

	return result;
}

//-------------------------------------------------------------------------------
void StateFindDialog::UpdateResult()
{
	ResultListBox->Freeze();
	ResultListBox->ClearAll();

	UpdatePrefixMask();
	ParamConfigAnm* PConfig = g_aeditor->GetParamConfig();
	ACharacterClass* character = PConfig->GetCurrentCharacterPtr();

	if(!character)
	{
		ResultListBox->Thaw();
		return;
	}

	ASGroup* CurGroup = character->GetCurrentGroupPtr();

	if(!CurGroup)
	{
		ResultListBox->Thaw();
		return;
	}

	EArray<ASClass*> states;
	ASPrefixMask* SecondMask = FindMasks[1]->AnySet() ? FindMasks[1] : NULL;
	wxString FindString = StringBox->GetValue();
	character->FindStates(FindString.c_str(), FindMasks[0], SecondMask, states);

	qsort(states.GetArray(), states.GetCardinality(), sizeof(ASClass*), CompareStates);
	SFListBoxItem StateItem;
	DString CurGroupName = CurGroup ? CurGroup->GetName() : "NoGroup";

	for(uint n = 0; n < states.GetCardinality(); n++)
	{
		ASClass* state = states[n];
		StateItem.name = state->GetName();
		StateItem.IconIndex = (CurGroup && state->FindGroupRef(CurGroupName.c_str())) ? BI_STATE_CLASS_ON_GRAPH : BI_STATE_CLASS;
		ResultListBox->Append(StateItem);
	}

	ResultListBox->SetItemCount(states.GetCardinality());
	ResultListBox->Thaw();

	wxString ResultText = ItoA(states.GetCardinality());
	ResultText += " states found";
	ResultLabel->SetLabel(ResultText);
/*
	if(states.GetCardinality() == 0)
	{
		wxMessageDialog dialog(g_aeditor->GetMainFrame(), "specified states not found", _T("Find result"), wxICON_INFORMATION | wxOK);
		dialog.ShowModal();
	}*/
}