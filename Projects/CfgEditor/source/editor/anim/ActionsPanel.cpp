
#include "precomp.h"
#include "anim/ActionsPanel.h"
#include "anim/AnimEditor.h"
#include "anim/ConfigEdAnmApp.h"
#include "anim/ImageListAnm.h"
#include "base/ParamFileTree.h"
#include "common/wxNativeConvert.h"

#include "bitmaps/AddGroup.xpm"
#include "bitmaps/DeleteGroup.xpm"

//=========================================================================================
ActionsPanel::ActionsPanel(wxWindow* parent)
	:wxPanel(parent, -1)
{
	PropGrid = new WBPropertyGrid(this, -1, wxDefaultPosition, wxDefaultSize, /*wxPG_AUTO_SORT |*/ wxPG_SPLITTER_AUTO_CENTER | wxPG_DEFAULT_STYLE | wxNO_BORDER);
	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(PropGrid, 1, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
	SetSizer(sizer);
}

//-------------------------------------------------------------------------------
void ActionsPanel::Clear()
{
	PropGrid->Clear();
}

//-------------------------------------------------------------------------------
void ActionsPanel::ShowActionsOfState(ASClass* state)
{
	Clear();

	if(!state)
		return;

	PCClass* ActionsClass = state->GetActionsClass();

	if(!ActionsClass)
		return;

	EArray<PCClass*> InheritStrip;
	PCClass* cl = ActionsClass;

	while(cl)
	{
		InheritStrip.Insert(cl);
		cl = cl->GetBase();
	}

	uint NumClasses = InheritStrip.GetCardinality();
	PCClass* TopBase = (NumClasses > 0) ? InheritStrip[NumClasses - 1] : NULL;
	int UniCategoryIndex = 0;
	PropGrid->Freeze();

	for(int n = (int)InheritStrip.GetCardinality() - 1; n >= 0 ; n--)
	{
		PCClass* cl = InheritStrip[n];

		wxPGId ExistingCat = PropGrid->FindCategory(cl->GetName());
		wxString CatName = cl->GetName();

		if(ExistingCat.IsOk())
			CatName = CatName += " (" + ItoA(++UniCategoryIndex) + ")";

		wxPGProperty* catprop = PropGrid->AppendCategory( CatName.c_str() );
		PropGrid->SetPropertyClientData (catprop->GetId(), cl);	//podla tohto budeme dohladavat cze FindCategory()

//			PropGrid->SetPropertyTextColour(catprop->GetId(), ClassLabelTextColor);

		if(n > 0)
			PropGrid->Collapse(catprop);

		for(uint m = 0; m < cl->GetNodesCount(); m++)
		{
			PCNode* node = cl->GetNode(m);

			if(node->ToClass())
				continue;

			PCVar* var = node->ToVariable();
			PCArray* arr = node->ToArray();

			const char* PropLabel = node->GetName();
			DString PropName = node->GetId();
			wxPGProperty* prop = NULL;

			if(var)
			{
				prop = PropGrid->InsertEditBoxProperty(catprop->GetId(), PropName.c_str(), PropLabel, var->GetValue(), wxDefaultValidator, -1);
			}
			else
			{
				enf_assert(arr);
				prop = ShowArrayProperties(arr, catprop, PropLabel, PropName.c_str(), true);
			}

			if(prop)
				PropGrid->DisableProperty(prop->GetId());
		}
	}

	PropGrid->Thaw();
}

//--------------------------------------------------------------------------------------
wxPGProperty* ActionsPanel::ShowArrayProperties(PCArray* arr, wxPGProperty* ParentProp, const char* label, const char* name, bool ClassOwner)
{
	wxPGProperty* prop = wxCustomProperty(label, name);
	wxPGId PropID = PropGrid->AppendIn(ParentProp->GetId(), prop );
	DString ArrID = arr->GetId();

	PropGrid->DisableProperty(PropID);	//this can be disabled always. editable will be only subproperties

	for(uint a = 0; a < arr->GetCardinality(); a++)
	{
		PCNode* item = arr->GetItem(a);
		PCVar* var = item->ToVariable();
		bool IsVariable = (var != NULL);
		wxString PropName;
		DString ItemName;
		DString UniID = item->GetId();
		PropName = UniID.c_str();
		ItemName = item->GetName();

		if(IsVariable)
		{
			wxPGProperty* prop = PropGrid->InsertEditBoxProperty(PropID, PropName, /*ItemName.c_str()*/"", var->GetValue(), wxDefaultValidator, -1);
			PropGrid->DisableProperty(prop->GetId());
		}
		else	//is array
		{
			PCArray* Arr = item->ToArray();
			enf_assert(Arr);
			ShowArrayProperties(Arr, prop, "subarray"/*ItemName.c_str()*/, PropName.c_str(), false);
		}
	}

	if(arr->GetCardinality() == 0)	//if array is empty, insert a "empty slot" item because of array property must be expand/collapseable
	{
		DString SubVarName = g_editor->GetParamConfig()->GenerateUniqueName(UID_VARIABLE);
		wxString SubVarID = ArrID.c_str();
		SubVarID += "/";
		SubVarID += SubVarName.c_str();
		wxPGProperty* SubProp = PropGrid->InsertEditBoxProperty(prop->GetId(), SubVarID, "empty slot", "", wxDefaultValidator, -1);
		PropGrid->DisableProperty(SubProp->GetId());
	}

	return prop;
}