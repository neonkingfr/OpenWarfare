#ifndef PARAM_FILE_TREE_ANM_H
#define PARAM_FILE_TREE_ANM_H

#include "base/ParamFileTree.h"

enum
{
	MENU_GOTO_ACTIONS_CLASS = PFT_LAST_UNUSED_ID,
};

//=============================================================================
// extension of ParamFileTree class from base project
//=============================================================================
class ParamFileTreeAnm : public ParamFileTree
{
public:
	ParamFileTreeAnm(wxWindow* parent, wxWindowID id);
protected:
	virtual wxMenu* CreateSelClassMenu();
private:
	void OnSelectFromMenu(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

#endif