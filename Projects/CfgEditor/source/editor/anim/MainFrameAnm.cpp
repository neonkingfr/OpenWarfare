
#include "precomp.h"
#include "anim/MainFrameAnm.h"
#include "anim/DiagramWin/DiagramWindow.h"
#include "anim/DiagramWin/DiagramNode.h"
#include "anim/StateGroupsPanel.h"
#include "anim/ConnectionPanel.h"
#include "anim/ActionsPanel.h"
#include "anim/AnimEditor.h"
#include "anim/ImageListAnm.h"
#include "anim/StateFindDialog.h"
#include "anim/ParamFileTreeAnm.h"
#include "base/PropertiesPanel.h"
#include "base/OptionsDialog.h"
#include "common/XmlConfig.h"
#include "wx/clipbrd.h"

extern ASClass* g_MenuState = NULL;

BEGIN_EVENT_TABLE(MainFrameAnm, MainFrame)
	EVT_MENU(-1, MainFrameAnm::OnSelectFromMenu)
	EVT_DIAGRAMWIN(-1, MainFrameAnm::OnDiagramEvent)
	EVT_TREE_BEGIN_DRAG(-1, MainFrameAnm::OnTreeBeginDrag) 
	EVT_ACTIVATE(MainFrameAnm::OnActivate)
END_EVENT_TABLE()

//================================================================================
MainFrameAnm::MainFrameAnm()
	:MainFrame()
{
	GroupsPanel = NULL;
	ConnPanel = NULL;
	ActPanel = NULL;
	GraphMenu = NULL;
	SFindDialog = NULL;

	//get current windows cursor clip area for possible reset after
	RECT CursorRect;
	GetClipCursor(&CursorRect); 
	ScreenCursorClipArea.x = CursorRect.left;
	ScreenCursorClipArea.y = CursorRect.top;
	ScreenCursorClipArea.width = CursorRect.right - CursorRect.left;
	ScreenCursorClipArea.height = CursorRect.bottom - CursorRect.top;
}

//----------------------------------------------------------------------
MainFrameAnm::~MainFrameAnm()
{
//	SAFE_DELETE(SFindDialog);
}

//----------------------------------------------------------------------
wxMenu* MainFrameAnm::CreateFileMenu()
{
   wxMenu* menu = new wxMenu;
	menu->Append(MENU_NEW, _T("New\tCtrl-N"), _T("create new param config"));
	menu->Append(MENU_LOAD, _T("Open\tCtrl-O"), _T("load work version of param file config"));
	menu->Append(MENU_SAVE, _T("Save\tCtrl-S"), _T("save work version of param file config"));
	menu->Append(MENU_SAVE_AS, _T("Save As"), _T("save work version of param file config"));
	menu->Append(MENU_IMPORT, _T("Import\tCtrl-I"), _T("import final version param config (without edit data)"));
	menu->Append(MENU_EXPORT, _T("Export\tCtrl-E"), _T("export final version param config (without edit data)"));
	menu->Append(MENU_GENERATE_DEF_FILE, _T("Generate definition"), _T("generate config definition file"));
	menu->Enable(MENU_SAVE, false);
	menu->Enable(MENU_SAVE_AS, false);
	menu->Enable(MENU_EXPORT, false);
	menu->Enable(MENU_GENERATE_DEF_FILE, false);
	return menu;
}

//----------------------------------------------------------------------
wxMenu* MainFrameAnm::CreateEditMenu()
{
   wxMenu* menu = new wxMenu;
	menu->Append(MENU_COPY, _T("Copy\tCtrl-C"), _T("copy"));
	menu->Append(MENU_PASTE, _T("Paste\tCtrl-V"), _T("paste"));
	menu->Append(MENU_UNDO, _T("Undo\tCtrl-Z"), _T("undo"));
	menu->Append(MENU_REDO, _T("Redo\tCtrl-Y"), _T("redo"));
	menu->AppendSeparator();
	menu->Append(MENU_FIND_STATES_DIALOG, _T("Find States\tCtrl-F"), _T("find states"));

	menu->Enable(MENU_COPY, false);
	menu->Enable(MENU_PASTE, false);
	menu->Enable(MENU_UNDO, false);
	menu->Enable(MENU_REDO, false);
	return menu;
}

//-------------------------------------------------------------------------
//create windows for advanced UI manager. this is main layout of main window 
//-------------------------------------------------------------------------
void MainFrameAnm::CreateAUIPanes()
{
//	MainFrame::CreateAUIPanes();	//generic editor panes
	MainToolBar = CreateMainToolBar();

	PropPanel = new PropertiesPanel(this);

	ParamTree = new ParamFileTreeAnm(this, TREE_PARAM);
	ParamTree->SetImageList(g_ImageList);

	AuiManager->AddPane(MainToolBar, wxAuiPaneInfo().Layer(1).ToolbarPane().LeftDockable(false).RightDockable(false).Top().BestSize(-1, 23).Name("Toolbar").Caption("Toolbar"));
	AuiManager->AddPane(ParamTree, wxAuiPaneInfo().Layer(1).Left().BestSize(400, 100).FloatingSize(300, 300).Name("ConfigFileWindow").Caption("Class tree").MaximizeButton(true));
	AuiManager->AddPane(PropPanel, wxAuiPaneInfo().Layer(1).Left().BestSize(300, 200).FloatingSize(300, 300).Name("ClassParmsWindow").Caption("Class content").MaximizeButton(true));

	GroupsPanel = new StateGroupsPanel(this);
	DiagramWindow* DiagWin = GroupsPanel->GetDiagramPanel()->GetDiagramWin();
	DiagWin->SetDropTarget(new AnimPFTreeTarget());

	ActPanel = new ActionsPanel(this);
	AuiManager->AddPane(ActPanel, wxAuiPaneInfo().Layer(1).Left().BestSize(400, 200).Name("ActionsPanelWindow").Caption("Actions").MaximizeButton(true));
	
	//create palete in graph window
	g_aeditor->PCI_Connect = DiagWin->AddPaleteColor(wxColour(255,0,0));
	g_aeditor->PCI_Interpolate = DiagWin->AddPaleteColor(wxColour(0,255,0));
	g_aeditor->PCI_Both = DiagWin->AddPaleteColor(wxColour(0,0,255));
	g_aeditor->PCI_Selected = DiagWin->AddPaleteColor(wxColour(255,255,0));

	//create some connection categories in graph window
	PCConnectionCategory cat;
	cat.SetPaleteColorIndex(g_aeditor->PCI_Connect);
	DiagWin->AddConnectionCategory(cat);

	cat.SetPaleteColorIndex(g_aeditor->PCI_Interpolate);
	DiagWin->AddConnectionCategory(cat);

	cat.SetPaleteColorIndex(g_aeditor->PCI_Both);
	DiagWin->AddConnectionCategory(cat);

	cat.SetPaleteColorIndex(g_aeditor->PCI_Selected);
	DiagWin->AddConnectionCategory(cat);

	AuiManager->AddPane(GroupsPanel, wxAuiPaneInfo().CenterPane().Name("GraphWindow").Caption("Graph").CaptionVisible(true).MaximizeButton(true));

	ConnPanel = new ConnectionPanel(this);
	AuiManager->AddPane(ConnPanel, wxAuiPaneInfo().Layer(1).Left().BestSize(200, 100).MaxSize(200, 100).FloatingSize(269, 164).Name("ConnectionWindow").Caption("Connection"));
}

//-------------------------------------------------------------------------------------
wxString MainFrameAnm::GetDefaultPanesPerspective()
{
	return "layout2|name=Toolbar;caption=Toolbar;state=2108156;dir=1;layer=1;row=0;pos=0;prop=100000;bestw=-1;besth=23;minw=100;minh=10;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=ConfigFileWindow;caption=Class tree;state=6293500;dir=4;layer=1;row=0;pos=0;prop=100000;bestw=400;besth=100;minw=100;minh=100;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=300;floath=600|name=ClassParmsWindow;caption=Class content;state=6309884;dir=4;layer=1;row=0;pos=1;prop=106861;bestw=300;besth=200;minw=100;minh=100;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=300;floath=600|name=ActionsPanelWindow;caption=Actions;state=6293500;dir=4;layer=1;row=0;pos=2;prop=99416;bestw=400;besth=200;minw=100;minh=100;maxw=-1;maxh=-1;floatx=300;floaty=300;floatw=300;floath=600|name=GraphWindow;caption=Graph;state=4196096;dir=5;layer=0;row=0;pos=0;prop=100000;bestw=20;besth=20;minw=100;minh=100;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=ConnectionWindow;caption=Connection;state=2099196;dir=4;layer=1;row=0;pos=3;prop=93723;bestw=200;besth=100;minw=100;minh=100;maxw=200;maxh=100;floatx=-1;floaty=-1;floatw=269;floath=164|dock_size(1,1,0)=25|dock_size(4,1,0)=304|dock_size(5,0,0)=22|";
}

//----------------------------------------------------------------------
void MainFrameAnm::OnAppInit()
{
	SFindDialog = new StateFindDialog(this);
}

//----------------------------------------------------------------------
wxMenuBar* MainFrameAnm::CreateMenuBar()
{
	wxMenuBar* MenuBar = MainFrame::CreateMenuBar();

   GraphMenu = new wxMenu;
	GraphMenu->AppendCheckItem(MENU_CONNECT_TO, _T("connect"), _T(""));
	GraphMenu->AppendCheckItem(MENU_INTERPOLATE_TO, _T("interpolate"), _T(""));
	GraphMenu->Check(MENU_CONNECT_TO, true);
	GraphMenu->Check(MENU_INTERPOLATE_TO, true);
	GraphMenu->AppendSeparator();
	GraphMenu->AppendCheckItem(MENU_DRAW_ONLY_LINES, _T("draw lines only (faster)"), _T(""));
	GraphMenu->Check(MENU_DRAW_ONLY_LINES, false);

	MenuBar->Append(GraphMenu, _T("&Graph"));
	return MenuBar;
}

//---------------------------------------------------------------------------------------------
wxToolBar* MainFrameAnm::CreateMainToolBar()
{
	wxToolBar* bar = new wxToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_FLAT | wxTB_NODIVIDER);
	bar->SetToolBitmapSize(wxSize(16, 16));
	bar->AddTool(MENU_NEW, wxT("New"), g_ImageList->GetBitmap(BI_NEW), wxT("New"));
	bar->AddTool(MENU_LOAD, wxT("Open"), g_ImageList->GetBitmap(BI_OPEN), wxT("Open"));
	bar->AddTool(MENU_SAVE, wxT("Save"), g_ImageList->GetBitmap(BI_SAVE), wxT("Save"));
	bar->AddTool(MENU_UNDO, wxT("Undo"), g_ImageList->GetBitmap(BI_UNDO), wxT("Undo"));
	bar->AddTool(MENU_REDO, wxT("Redo"), g_ImageList->GetBitmap(BI_REDO), wxT("Redo"));
	bar->AddTool(MENU_COPY, wxT("Copy"), g_ImageList->GetBitmap(BI_COPY), wxT("Copy"));
	bar->AddTool(MENU_PASTE, wxT("Paste"), g_ImageList->GetBitmap(BI_PASTE), wxT("Paste"));
	bar->AddCheckTool(MENU_BASE_CLASSES_IN_TREE, wxT("Show base classes int tree"), g_ImageList->GetBitmap(BI_SHOW_BASE_CLASS), wxNullBitmap, wxT("Show base classes in tree"));
	bar->EnableTool(MENU_SAVE, false);
	bar->EnableTool(MENU_UNDO, false);
	bar->EnableTool(MENU_REDO, false);
//	bar->AddSeparator();
	bar->Realize();
	return bar;
}

//-------------------------------------------------------------------------
void MainFrameAnm::CreateImageList()
{
	new ImageListAnm(16, 16, true);	//common image list. g_ImageList pointer on
}

//-------------------------------------------------------------------------
void MainFrameAnm::OnSelectFromMenu(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case MENU_LOAD:
		g_aeditor->Load();
		break;
	case MENU_SAVE:
		g_aeditor->Save();
		break;
	case MENU_SAVE_AS:
		g_aeditor->SaveAs();
		break;
	case MENU_IMPORT:
		g_aeditor->Import();
		break;
	case MENU_EXPORT:
		g_aeditor->Export();
		break;
	case MENU_CONNECT_TO:
		g_aeditor->UpdateCurrentGroupGui();
		GetGroupsPanel()->GetDiagramPanel()->GetDiagramWin()->RepaintFromClient();
		break;
	case MENU_INTERPOLATE_TO:
		g_aeditor->UpdateCurrentGroupGui();
		GetGroupsPanel()->GetDiagramPanel()->GetDiagramWin()->RepaintFromClient();
		break;
	case MENU_DRAW_ONLY_LINES:
		GetGroupsPanel()->GetDiagramPanel()->GetDiagramWin()->DrawOnlyLines(event.GetInt() != 0);
		GetGroupsPanel()->GetDiagramPanel()->GetDiagramWin()->RepaintFromClient();
		break;
	case MENU_REMOVE_STATE_FROM_CURRENT_GRAPH:
	{
		EArray<ASClass*> states;
		g_aeditor->GetStatesSelectedInGraph(states);
		g_aeditor->RemoveStatesFromCurrentGraph(states);

		if(GetStateFindDialog()->IsShown())
			GetStateFindDialog()->UpdateResult();
		break;
	}
	case MENU_COPY_STATE_NAME_CURRENT_GRAPH:
	  if (wxTheClipboard->Open())
	  {
		 wxTheClipboard->SetData( new wxTextDataObject(g_MenuState->GetName()) );
		 wxTheClipboard->Close();
	  }
	  break;
	case MENU_INSERT_CONNECT_TARGETS_TO_GRAPH:
	{
		g_aeditor->InsertStatesToGraph(false, CT_CONNECT);
		break;
	}
	case MENU_INSERT_INTERPOLATE_TARGETS_TO_GRAPH:
	{
		g_aeditor->InsertStatesToGraph(false, CT_INTERPOLATE);
		break;
	}
	case MENU_INSERT_ALL_TARGETS_TO_GRAPH:
	{
		g_aeditor->InsertStatesToGraph(false, CT_CONNECT | CT_INTERPOLATE);
		break;
	}
	case MENU_INSERT_CONNECT_SOURCES_TO_GRAPH:
	{
		g_aeditor->InsertStatesToGraph(true, CT_CONNECT);
		break;
	}
	case MENU_INSERT_INTERPOLATE_SOURCES_TO_GRAPH:
	{
		g_aeditor->InsertStatesToGraph(true, CT_INTERPOLATE);
		break;
	}
	case MENU_INSERT_ALL_SOURCES_TO_GRAPH:
	{
		g_aeditor->InsertStatesToGraph(true, CT_CONNECT | CT_INTERPOLATE);
		break;
	}

	case MENU_REMOVE_CONNECT_TARGETS_FROM_GRAPH:
		g_aeditor->RemoveStatesFromGraph(false, CT_CONNECT, false);
		break;
	case MENU_REMOVE_INTERPOLATE_TARGETS_FROM_GRAPH:
		g_aeditor->RemoveStatesFromGraph(false, CT_INTERPOLATE, false);
		break;
	case MENU_REMOVE_ALL_TARGETS_FROM_GRAPH:
		g_aeditor->RemoveStatesFromGraph(false, CT_CONNECT | CT_INTERPOLATE, true);
		break;
	case MENU_REMOVE_CONNECT_SOURCES_FROM_GRAPH:
		g_aeditor->RemoveStatesFromGraph(true, CT_CONNECT, false);
		break;
	case MENU_REMOVE_INTERPOLATE_SOURCES_FROM_GRAPH:
		g_aeditor->RemoveStatesFromGraph(true, CT_INTERPOLATE, false);
		break;
	case MENU_REMOVE_ALL_SOURCES_FROM_GRAPH:
		g_aeditor->RemoveStatesFromGraph(true, CT_CONNECT | CT_INTERPOLATE, true);
		break;
	case MENU_FIND_STATES_DIALOG:
		ShowFindDialog();
		break;
	default:
		event.Skip();
	}
}

//-------------------------------------------------------------------------
void MainFrameAnm::OnDiagramEvent(wxCommandEvent& event)
{
	DiagWinEvent* DWEvent = dynamic_cast<DiagWinEvent*>(event.GetClientObject());
	enf_assert(DWEvent);
	DiagWinEvtType EventType = DWEvent->GetType();

	switch(EventType)
	{
		case DWET_NodesMoved:
		{
			DWE_NodesMoved* DEvent = (DWE_NodesMoved*)DWEvent;
			g_aeditor->OnStateGraphNodesMoved(DEvent->GetNodes());
			break;
		}
		case DWET_ZoomChanged:
		{
			DWE_ZoomChanged* DEvent = (DWE_ZoomChanged*)DWEvent;
			g_aeditor->OnGraphZoomChanged(DEvent->GetZoom());
			break;
		}
		case DWET_ViewPointChanged:
		{
			DWE_ViewPointChanged* DEvent = (DWE_ViewPointChanged*)DWEvent;
			g_aeditor->OnGraphViewPointChanged(DEvent->GetPoint());
			break;
		}
		case DWET_NodeMenu:
		{
			DWE_NodeMenu* DEvent = (DWE_NodeMenu*)DWEvent;
			DiagramNode* node = DEvent->GetNode();
			enf_assert(node);
			ASClass* state = (ASClass*)node->GetClientData();
			enf_assert(state);
			g_aeditor->OnGraphStateMenu(state, DEvent->GetPoint());
			break;
		}
		case DWET_DragAndDrop:
		{
			DWE_DragAndDrop* DEvent = (DWE_DragAndDrop*)DWEvent;
			g_aeditor->OnGraphDragAndDrop(DEvent);
			break;
		}
		case DWET_SelectionChanged:
		{
			DWE_SelectionChanged* DEvent = (DWE_SelectionChanged*)DWEvent;
			g_aeditor->OnGraphSelectionChanged();
			break;
		}
		case DWET_Key:
		{
			DWE_Key* DEvent = (DWE_Key*)DWEvent;
			g_aeditor->OnGraphKey(DEvent->GetKeyCode(), DEvent->IsDown());
			break;
		}
	};
}

//-------------------------------------------------------------------------
void MainFrameAnm::OnTreeBeginDrag(wxTreeEvent& event)
{
	if(event.GetId() == TREE_PARAM)
	{
//		wxTreeItemId item = event.GetItem();

		wxArrayTreeItemIds sels;
		uint NumSel = GetParamTree()->GetSelections(sels);
		wxFileDataObject ddata;

		for(uint n = 0; n < sels.GetCount(); n++)
		{
			wxTreeItemId item = sels[n];
			CTItemData* data = (CTItemData*)GetParamTree()->GetItemData(item);
			enf_assert(data);

			PCClass* cl = data->GetClass();
			ASClass* StateClass = dynamic_cast<ASClass*>(cl);

			if(!StateClass)
				continue;

			DString StateID = StateClass->GetId();
			ddata.AddFile(StateID.c_str());
		}

		wxDropSource source(ddata, this, wxDROP_ICON(dnd_copy), wxDROP_ICON(dnd_move), wxDROP_ICON(dnd_none));

		bool MoveByDefault = true;
		bool MoveAllowed = true;

		int flags = 0;
		if ( MoveByDefault )
			flags |= wxDrag_DefaultMove;
		else if ( MoveAllowed )
			flags |= wxDrag_AllowMove;

		wxDragResult result = source.DoDragDrop(flags);
	}
}

//-------------------------------------------------------------------------------------
void MainFrameAnm::OnActivate(wxActivateEvent& event)
{
	bool active = event.GetActive();

	GetGroupsPanel()->GetDiagramPanel()->GetDiagramWin()->OnMainFrameActivate(active);

	wxRect& OrgClipRect = ScreenCursorClipArea;

	RECT rect;
	rect.left = OrgClipRect.x;
	rect.top = OrgClipRect.y;
	rect.right = rect.left + OrgClipRect.width;
	rect.bottom = rect.top + OrgClipRect.height;
	ClipCursor(&rect);

	event.Skip();
}

//-------------------------------------------------------------------------------------
AnimPFTreeTarget::AnimPFTreeTarget()
{
}

//-------------------------------------------------------------------------------------
bool AnimPFTreeTarget::OnDropFiles(wxCoord x, wxCoord y, const wxArrayString& filenames)
{
	g_aeditor->OnGraphWindowDropStates(x, y, filenames);
	return true;
}

//-------------------------------------------------------------------------------------
void MainFrameAnm::ShowFindDialog()
{
	if(g_aeditor->GetParamConfig()->GetCurrentCharacterPtr())
		SFindDialog->ShowNormal(true);
}

//-------------------------------------------------------------------------------------
OptionsDialogPanel* MainFrameAnm::CreateOptionsPanel(OptionsDialog* parent)
{
	return NULL;//new OptionsDialogPanelAnm(parent);
}