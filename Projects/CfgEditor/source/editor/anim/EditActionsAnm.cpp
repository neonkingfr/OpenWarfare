
#include "precomp.h"
#include "EditActionsAnm.h"

//===============================================================================
EACreateSGroup::EACreateSGroup(const char* name, const char* CharacterName)
	:EditAction("", EA_CREATE_SGROUP)
{
	enf_assert(name);
	enf_assert(CharacterName);
	this->name = name;
	this->CharacterName = CharacterName;
	storage = NULL;
}

//===============================================================================
EACreateSGroup::EACreateSGroup(ASGroup* storage, const char* CharacterName)
	:EditAction("", EA_CREATE_SGROUP)
{
	enf_assert(storage);
	this->storage = storage;
	enf_assert(CharacterName);
	this->CharacterName = CharacterName;
	this->name = storage->GetName();
}

//-------------------------------------------------------------------------------
EACreateSGroup::~EACreateSGroup()
{
	SAFE_DELETE(storage);
}

//===============================================================================
EADeleteSGroup::EADeleteSGroup(const char* name, const char* CharacterName)
	:EditAction("", EA_DELETE_SGROUP)
{
	enf_assert(name);
	enf_assert(CharacterName);
	this->name = name;
	this->CharacterName = CharacterName;
}

//===============================================================================
EACreateSGroupRefs::EACreateSGroupRefs(const char* CharacterName)
	:EditAction("", EA_CREATE_SGROUP_REFS)
{
	enf_assert(CharacterName);
	this->CharacterName = CharacterName;
}

//-------------------------------------------------------------------------------
void EACreateSGroupRefs::AddStateRefData(const char* StateName, const ASGroupRef& reference)
{
	StateNames.Insert(StateName);
	StateRefDatas.Insert(reference);
}

//-------------------------------------------------------------------------------
uint EACreateSGroupRefs::GetStateRefDataCount() const
{
	return StateRefDatas.GetCardinality();
}

//-------------------------------------------------------------------------------
const	DString& EACreateSGroupRefs::GetStateName(uint n) const
{
	return StateNames[n];
}

//-------------------------------------------------------------------------------
const	ASGroupRef& EACreateSGroupRefs::GetStateRefData(uint n) const
{
	return StateRefDatas[n];
}


//===============================================================================
EADeleteSGroupRefs::EADeleteSGroupRefs(const char* GroupName, const char* CharacterName)
	:EditAction("", EA_DELETE_SGROUP_REFS)
{
	enf_assert(GroupName);
	enf_assert(CharacterName);
	this->GroupName = GroupName;
	this->CharacterName = CharacterName;
}

//-------------------------------------------------------------------------------
void EADeleteSGroupRefs::AddStateName(const char* name)
{
	enf_assert(name);
	StateNames.Insert(name);
}

//===============================================================================
EAModifySGroupRefs::EAModifySGroupRefs(const char* CharacterName)
	:EditAction("", EA_MODIFY_SGROUP_REFS)
{
	enf_assert(CharacterName);
	this->CharacterName = CharacterName;
}

//-------------------------------------------------------------------------------
void EAModifySGroupRefs::AddStateRefData(const char* StateName, const ASGroupRef& reference)
{
	StateRefDatas.Insert(reference);
	StateNames.Insert(StateName);
}

//-------------------------------------------------------------------------------
const	ASGroupRef& EAModifySGroupRefs::GetStateRefData(uint n) const
{
	return StateRefDatas[n];
}

//-------------------------------------------------------------------------------
const	DString& EAModifySGroupRefs::GetStateName(uint n) const
{
	return StateNames[n];
}

//-------------------------------------------------------------------------------
uint EAModifySGroupRefs::GetStateRefDataCount() const
{
	return StateRefDatas.GetCardinality();
}

//-------------------------------------------------------------------------------
HistoryPointAnm::HistoryPointAnm(const char* name)
	:HistoryPoint(name)
{
	GraphZoomLevel = -1;
}

//-------------------------------------------------------------------------------
void HistoryPointAnm::SetUndoCharacterSel(const char* character)
{
	UndoCharacterSel = character;
}

//-------------------------------------------------------------------------------
void HistoryPointAnm::SetRedoCharacterSel(const char* character)
{
	RedoCharacterSel = character;
}

//-------------------------------------------------------------------------------
void HistoryPointAnm::SetUndoGroupSel(const char* group)
{
	UndoGroupSel = group;
}

//-------------------------------------------------------------------------------
void HistoryPointAnm::SetRedoGroupSel(const char* group)
{
	RedoGroupSel = group;
}

//-------------------------------------------------------------------------------
void HistoryPointAnm::SetGraphViewPos(const FPoint& ViewPos)
{
	GraphViewPos = ViewPos;
}

//-------------------------------------------------------------------------------
void HistoryPointAnm::SetGraphZoomLevel(int ZoomLevel)
{
	GraphZoomLevel = ZoomLevel;
}