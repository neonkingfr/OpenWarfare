
#ifndef CONFIG_ED_ANM_APP_H
#define CONFIG_ED_ANM_APP_H

#include "wx/app.h"

class AnimEditor;
class MainFrame;

//application class. instance is created using macros DECLARE_APP a IMPLEMENT_APP
//-------------------------------------------------------------------------------------------------------
class ConfigEdAnmApp : public wxApp
{
public:
	ConfigEdAnmApp();
	inline AnimEditor* GetEditor(){return editor;}
private:
   virtual bool	OnInit();
	virtual int		OnExit();
	int				MainLoop();
	AnimEditor*	editor;
};

DECLARE_APP(ConfigEdAnmApp)

#endif