
#ifndef FLOATING_2D_H
#define FLOATING_2D_H

class FSize
{
public:
	FSize(float x, float y)
	{
		this->x = x;
		this->y = y;
	}
	FSize()
	{
		x = y = 0.0f;
	}

	FSize operator+(const FSize& pt) const
	{
		return FSize(x + pt.x, y + pt.y);
	}

	FSize operator-(const FSize& pt) const
	{
		return FSize(x - pt.x, y - pt.y);
	}

	void operator+=(const FSize& pt)
	{
		x += pt.x;
		y += pt.y;
	}

	void operator-=(const FSize& pt)
	{
		x -= pt.x;
		y -= pt.y;
	}

	void operator*=(float f)
	{
		x *= f;
		y *= f;
	}

	void operator/=(float f)
	{
		x /= f;
		y /= f;
	}

	FSize operator*(float f) const
	{
		return FSize(x * f, y * f);
	}

	float x;
	float y;
};

class FPoint
{
public:
	FPoint(float x, float y)
	{
		this->x = x;
		this->y = y;
	}
	FPoint()
	{
		x = y = 0.0f;
	}

	inline float Dot(const FPoint& vec) const
	{
		return x * vec.x + y * vec.y;
	}

	inline FPoint Lerp(const FPoint& vec, float lerp) const
	{
		return FPoint((vec.x - x) * lerp + x, (vec.y - y) * lerp + y);
	}

	inline float Normalize()
	{
		float length = x * x + y * y;

		if(length != 1 && length > 0)
		{
			length = sqrtf(length);

			float ilength = 1.0f / length;
			x *= ilength;
			y *= ilength;
		}

		return length;
	}

	inline float Length() const
	{
		double d = x * x + y * y;

		if(d == 0.0)
			return 0.0f;

		if(d == 1.0)
			return 1.0f;

		return (float)sqrt(d);
	}

/*
	FPoint Rotate(float radians)
	{
		float px = x * sin(radians);
		float py = y * cos(radians);
		return FPoint(px, py);
	}*/

	FPoint operator+(const FPoint& pt) const
	{
		return FPoint(x + pt.x, y + pt.y);
	}

	FPoint operator-(const FPoint& pt) const
	{
		return FPoint(x - pt.x, y - pt.y);
	}

	FPoint operator+(const FSize& sz) const
	{
		return FPoint(x + sz.x, y + sz.y);
	}

	FPoint operator-(const FSize& sz) const
	{
		return FPoint(x - sz.x, y - sz.y);
	}

	void operator+=(const FPoint& pt)
	{
		x += pt.x;
		y += pt.y;
	}

	void operator-=(const FPoint& pt)
	{
		x -= pt.x;
		y -= pt.y;
	}

	void operator*=(float f)
	{
		x *= f;
		y *= f;
	}

	void operator/=(float f)
	{
		x /= f;
		y /= f;
	}

	float x;
	float y;
};

class FRect
{
public:
	FRect()
	{
		x = y = width = height = 0;
	}

	FRect(float x, float y, float width, float height)
	{
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
	}

	FRect(const FPoint& pos, const FSize& size)
	{
		x = pos.x;
		y = pos.y;
		width = size.x;
		height = size.y;
	}

	void SetPos(const FPoint& pos)
	{
		x = pos.x;
		y = pos.y;
	}

	void SetSize(const FSize& size)
	{
		width = size.x;
		height = size.y;
	}

	const FPoint GetPos() const
	{
		return FPoint(x, y);
	}

	const FSize GetSize() const
	{
		return FSize(width, height);
	}

	bool Contains(const FPoint& pos) const
	{
		if(pos.x < x || pos.x > x + width || pos.y < y || pos.y > y + height)
			return false;

		return true;
	}

	inline float GetRight() const
	{
		return x + width;
	}

	inline float GetBottom() const
	{
		return y + height;
	}

	bool Overlap(const FRect& rc) const
	{
		float right = GetRight();
		float bottom = GetBottom();
		float right2 = rc.GetRight();
		float bottom2 = rc.GetBottom();

		if(rc.x > right) return false;
		if(right2 < x) return false;
		if(rc.y > bottom) return false;
		if(bottom2 < y) return false;

		return true;
	}

	float x;
	float y;
	float width;
	float height;
};

#endif