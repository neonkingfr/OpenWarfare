
#ifndef DIAGRAM_NODE_H
#define DIAGRAM_NODE_H

#include "DiagramNode.h"
#include "DiagramConnection.h"
#include "Floating2D.h"

class DiagramNode;
class DNConnection;
class DiagramWindow;

//======================================================================
enum DCSDirection	//DiagramConnectionSlotType
{
	DCST_INPUT,
	DCST_OUTPUT
};

//======================================================================
enum DCSAllignment
{
	DCSA_LEFT,
	DCSA_RIGHT,
	DCSA_TOP,
	DCSA_BOTTOM,
};

const int SlotWidth = 18;
const int SlotHeight = 18;

//======================================================================
//Diagram node connection slot
//======================================================================
class DCSlot	
{
	friend class DiagramNode;
public:
	DCSlot(int type, DCSDirection direction, DCSAllignment alignment)
	{
		this->type = type;
		this->direction = direction;
		this->alignment = alignment;
	}

	virtual ~DCSlot()
	{
	}
	inline DCSDirection	GetDirection(){return direction;}
	inline DCSAllignment	GetAllignment(){return alignment;}
	inline uint				GetID(){return id;}
	inline int				GetType(){return type;}
	inline FRect&			GetRect(){return rect;}
	inline DiagramNode*	GetOwner(){return owner;}
	FPoint					GetConnectionPoint();
	ENF_DECLARE_MMOBJECT(DCSlot);
private:
	void SetOwner(DiagramNode* owner);
	void SetID(uint id)
	{
		this->id = id;
	}

	DiagramNode*	owner;
	uint				id;
	int				type;
	DCSDirection	direction;
	DCSAllignment	alignment;
	FRect				rect;
};

enum //drag and drop flags
{
	DNDF_DRAG = 1,
	DNDF_DROP = 2,
};

//======================================================================
class DNDArea
{
public:
	DNDArea(DiagramNode* owner, int id, const FPoint& pos, const FSize& size, int flags, uint PaleteColorIndex);
	virtual void	RepaintContent(DiagramWindow*, wxDC& dc);
	inline const FRect&	GetRect(){return rect;}
	void	Show(bool visible){this->visible = visible;}
	inline bool IsVisible(){return visible;}
	inline uint GetPaleteColorIndex(){return PaleteColorIndex;}
	void SetPaleteColorIndex(uint index);
	inline			DiagramNode* GetOwner(){return owner;}
	inline			int GetId(){return id;}
	inline			int GetFlags(){return flags;}
	ENF_DECLARE_MMOBJECT(DNDArea);
private:
	DiagramNode*	owner;
	int				id;
	int				flags;
	FRect				rect;
	bool				visible;
	uint				PaleteColorIndex;
};

//======================================================================
class DiagramNode
{
	friend class DiagramWindow;
public:
									DiagramNode(const wxString& name);
									~DiagramNode();
	uint							AddConnectionSlot(DCSlot* slot);
	DCSlot*						GetSlot(uint slot);
	bool							AddConnection(DNConnection* connection);
	void							RemoveConnection(DNConnection* connection);
	ESet<DPConnetionRef>&	GetConnections(){return connections;}
	FSize							OrganizeSlots();
	inline wxString&			GetName(){return name;}
	void							RepaintContent(wxDC& dc);
	uint							GetSlotsCount(){return slots.GetCardinality();}
	void							SetPos(const FPoint& pos);
	void							SetSize(const FSize& size);
	inline FRect&				GetRect(){return rect;}
	FPoint						GetPos();
	FSize							GetSize();
	inline bool					IsVisible(){return visible;}
	void							SetClientData(void* data);
	void*							GetClientData();
	inline bool					IsSelected(){return selected;}
	bool							IsFocused();
	uint							AddDNDArea(DNDArea* area);
	DNDArea*						CheckDNDArea(const FPoint& pt);
	ENF_DECLARE_MMOBJECT(DiagramNode);
private:
	void							SetCanvas(DiagramWindow* canvas);
	inline void					SetVisible(bool visible){this->visible = visible;}
	void							OnSelect(bool select);
	FRect							rect;
	wxString						name;
	EArray<DCSlot*>			slots;
	EArray<DNDArea*>			DNDAreas;
	ESet<DPConnetionRef>		connections;
	wxPoint						PickOffset;
	DiagramWindow*				canvas;
	bool							visible;
	void*							ClientData;
	bool							selected;
};

#endif