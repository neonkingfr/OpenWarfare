
#include "precomp.h"
#include "AnimConfigDef.h"
#include "tinyxml/tinyxml.h"
#include "common/NativeUtil.h"

extern AnimConfigDef* g_animdef = NULL;

//-----------------------------------------------------------------------------------------
TiXmlElement* GetChildElement2(TiXmlNode* parent, const char* name)
{
	for(TiXmlNode* node = parent->FirstChild(); node; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(elm && strcmp(elm->Value(), name) == 0)
			return elm;
	}
	return NULL;
}

//===============================================================================
ACDefNode::ACDefNode(TiXmlNode* node)
{
//	enf_assert(node);
	this->node = node;
}

//-------------------------------------------------------------------------------
ACDefNode::ACDefNode()
{
	node = NULL;
}

//-----------------------------------------------------------------------------------------
TiXmlNode* ACDefNode::GetNode() const
{
	assert(node);
	return node;
}

//-------------------------------------------------------------------------------
TiXmlElement* ACDefNode::GetElement() const
{
	assert(node);
	return node->ToElement();
}


//===============================================================================
ASPrefixDef::ASPrefixDef(TiXmlElement* element)
	:ACDefNode(element)
{
}

//===============================================================================
ASPrefixDef::ASPrefixDef()
{
}

//-------------------------------------------------------------------------------
const char* ASPrefixDef::GetName() const
{
	assert(GetNode());
	return GetElement()->Attribute("name");
}

//-------------------------------------------------------------------------------
const char* ASPrefixDef::GetLabel() const
{
	assert(GetNode());
	return GetElement()->Attribute("label");
}

//-------------------------------------------------------------------------------
const char* ASPrefixDef::GetDescription() const
{
	assert(GetNode());
	return GetElement()->Attribute("desc");
}

//===============================================================================
ASPrefixGroupDef::ASPrefixGroupDef(TiXmlElement* element)
	:ACDefNode(element)
{
	for(TiXmlNode* node = GetElement()->FirstChild(); node != NULL; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(!elm)
			continue;

		if(strcmpi(elm->Value(), "prefix") == 0)
			prefixes.Insert(ASPrefixDef(elm));
	}
}

//-------------------------------------------------------------------------------
uint ASPrefixGroupDef::GetPrefixesCount()
{
	return prefixes.GetCardinality();
}

//-------------------------------------------------------------------------------
const ASPrefixDef& ASPrefixGroupDef::GetPrefixDef(uint n)
{
	return prefixes[n];
}

//-------------------------------------------------------------------------------
const char* ASPrefixGroupDef::GetName()
{
	assert(GetNode());
	return GetElement()->Attribute("name");
}

//===============================================================================
ASPrefixMask::ASPrefixMask()
{
	Clear();
	int gg = 0;
}

//-------------------------------------------------------------------------------
ASPrefixMask::ASPrefixMask(const ASPrefixMask& other)
{
	memcpy(this->mask, other.mask, MAX_PREFIX_GROUPS * sizeof(int));
	int gg = 0;
}

//-------------------------------------------------------------------------------
void ASPrefixMask::Clear()
{
	memset(mask, -1, MAX_PREFIX_GROUPS * sizeof(int));
}

//-------------------------------------------------------------------------------
void ASPrefixMask::Set(int group, int PrefixIndex)
{
	enf_assert(group >= 0);
	enf_assert(group < MAX_PREFIX_GROUPS);
	mask[group] = PrefixIndex;
}

//-------------------------------------------------------------------------------
bool ASPrefixMask::Contains(const ASPrefixMask& mask) const
{
	for(int n = 0; n < MAX_PREFIX_GROUPS; n++)
	{
		if(mask.mask[n] == -1)
			continue;

		if(mask.mask[n] != this->mask[n])
			return false;
	}
	return true;
}

//-------------------------------------------------------------------------------
bool ASPrefixMask::AnySet() const
{
	for(int n = 0; n < MAX_PREFIX_GROUPS; n++)
	{
		if(mask[n] != -1)
			return true;
	}
	return false;
}

//-------------------------------------------------------------------------------
void ASPrefixMask::operator= (const ASPrefixMask& mask)
{
	memcpy(this->mask, mask.mask, MAX_PREFIX_GROUPS * sizeof(int));
}

//-------------------------------------------------------------------------------
bool ASPrefixMask::operator> (const ASPrefixMask& mask) const
{
	for(int n = 0; n < MAX_PREFIX_GROUPS; n++)
	{
		if(this->mask[n] > mask.mask[n])
			return true;
	}
	return false;
}

//-------------------------------------------------------------------------------
bool ASPrefixMask::operator< (const ASPrefixMask& mask) const
{
	for(int n = 0; n < MAX_PREFIX_GROUPS; n++)
	{
		if(this->mask[n] > mask.mask[n])
			return true;
	}
	return false;
}

//-------------------------------------------------------------------------------
bool ASPrefixMask::operator== (const ASPrefixMask& mask) const
{
	for(int n = 0; n < MAX_PREFIX_GROUPS; n++)
	{
		if(mask.mask[n] != this->mask[n])
			return false;
	}
	return true;
}

//===============================================================================
AnimConfigDef::AnimConfigDef()
{
	g_animdef = this;
	doc = new TiXmlDocument();
}

//-------------------------------------------------------------------------------
AnimConfigDef::~AnimConfigDef()
{
	g_animdef = NULL;

	for(uint n = 0; n < PrefixGroups.GetCardinality(); n++)
	{
		ASPrefixGroupDef* group = PrefixGroups[n];
		delete group;
	}

	SAFE_DELETE(doc);
}

//-------------------------------------------------------------------------------
CResult AnimConfigDef::Load(const char* FileName)
{
	assert(doc);

	if(!doc->LoadFile(FileName))
	{
		DString comment("Anim config definition file not found ");
		comment += "(";
		comment += FileName;
		comment += ")";
		return CResult(false, comment.c_str());
	}

	TiXmlElement* root = doc->RootElement();
	assert(root);

	if(!root)
	{
		DString comment("Anim config definition file is invalid. is empty ");
		return CResult(false, comment.c_str());
	}

	//nahrajeme definicie pre editovanie animacii
	TiXmlElement* PrefixesElm = GetChildElement2(root, "prefixes");
	assert(PrefixesElm);

	if(!PrefixesElm)
	{
		DString comment("Anim config definition file is invalid. cannot find \"prefixes\" section ");
		return CResult(false, comment.c_str());
	}

	for(TiXmlNode* PrefNode = PrefixesElm->FirstChild(); PrefNode != NULL; PrefNode = PrefNode->NextSibling())
	{
		TiXmlElement* PrefElm = PrefNode->ToElement();

		if(!PrefElm)
			continue;

		if(strcmpi(PrefElm->Value(), "prefixgroup") == 0)
			PrefixGroups.Insert(ENF_NEW ASPrefixGroupDef(PrefElm));
	}

	assert(PrefixGroups.GetCardinality() > 0);

	if(PrefixGroups.GetCardinality() == 0)
	{
		DString comment("Anim config definition file is invalid. cannot find prefix definitions ");
		return CResult(false, comment.c_str());
	}

	return true;
}

//-------------------------------------------------------------------------------
bool AnimConfigDef::FindPrefix(const char* PrefixName, uint* group, uint* prefix) const
{
	for(uint g = 0; g < PrefixGroups.GetCardinality(); g++)
	{
		ASPrefixGroupDef* gr = PrefixGroups[g];

		for(uint n = 0; n < gr->GetPrefixesCount(); n++)
		{
			const ASPrefixDef& prefixdef = gr->GetPrefixDef(n);

			if(strcmpi(prefixdef.GetName(), PrefixName) == 0)
			{
				(*group) = g;
				(*prefix) = n;
				return true;
			}
		}
	}
	return false;
}

//-------------------------------------------------------------------------------
const char* AnimConfigDef::FindFirstPrefixInString(const char* str, uint* group, uint* prefix, uint* PrefixLength) const
{
	uint BestGroup = INDEX_NOT_FOUND;
	uint BestPrefix = INDEX_NOT_FOUND;
	uint BestPrefixLength = 0;
	const char* BestPos = str + (strlen(str));
	const char* result = NULL;

	for(uint g = 0; g < PrefixGroups.GetCardinality(); g++)
	{
		ASPrefixGroupDef* gr = PrefixGroups[g];

		for(uint n = 0; n < gr->GetPrefixesCount(); n++)
		{
			const ASPrefixDef& prefixdef = gr->GetPrefixDef(n);

			char* pos = stristr(str, prefixdef.GetName());

			if(pos)
			{
				if(pos < BestPos || ((pos == BestPos) && strlen(prefixdef.GetName()) > BestPrefixLength))
				{
					BestPos = pos;
					BestGroup = g;
					BestPrefix = n;
					BestPrefixLength = strlen(prefixdef.GetName());
					result = pos;
				}
			}
		}
	}

	(*group) = BestGroup;
	(*prefix) = BestPrefix;
	(*PrefixLength) = BestPrefixLength;
	return result;
}

//-------------------------------------------------------------------------------
uint AnimConfigDef::GetPrefixGroupsCount()
{
	return PrefixGroups.GetCardinality();
}

//-------------------------------------------------------------------------------
ASPrefixGroupDef* AnimConfigDef::GetPrefixGroup(uint n)
{
	return PrefixGroups[n];
}
