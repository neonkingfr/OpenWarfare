
#ifndef PARAM_CONFIG_ANM_H
#define PARAM_CONFIG_ANM_H

#include "base/ParamConfig.h"
#include "anim/AnimConfigDef.h"
#include "DiagramWin/Floating2D.h"

class ParamConfigAnm;
class ASClass;

//#define CfgMovesBasicClassName	"CfgMovesBasic"
//#define CfgMovesBasicClassID		"file/CfgMovesBasic"		//CfgMovesBasic class ID/location. config classes inherited from CfgMovesBasic class are defined as "characters"
#define CharacterStatesClassName "States"						//every character class must contain class member named "States". this defines states classes for character
#define ActionsParamName			"actions"					//name of param where action class is defined
#define ConnectToArrayName			"ConnectTo"					//arrays defined connections
#define ConnectFromArrayName		"ConnectFrom"
#define ConnectWithArrayName		"ConnectWith"
#define InterpolateToArrayName	"InterpolateTo"
#define InterpolateFromArrayName	"InterpolateFrom"
#define InterpolateWithArrayName	"InterpolateWith"


//class info flags
const int CI_CHARACTER = 8;
const int CI_STATE = 16;

//===============================================================================
// ID of anim state for fast sorting/searching. can be prefixed like AmovPercMstpSras or no
//===============================================================================
class AnimStateID
{
public:
	AnimStateID();
	AnimStateID(const AnimStateID& id);
	AnimStateID(const char* name);		//not prefixed ID
	~AnimStateID();
	bool operator> (const AnimStateID& id) const;
	bool operator< (const AnimStateID& id) const;
	bool operator== (const AnimStateID& id) const;
	void operator= (const AnimStateID& id);
	void operator= (const char* name);
	const ASPrefixMask* GetMask(uint mask) const;
	inline const DString& GetName() const {return name;}
	uint GetMaskCount() const;
	bool IsPrefixed() const;
//	ENF_DECLARE_MMOBJECT(AnimStateID);
private:
	void						CreateMasks();
	void						DestroyMasks();
	const char*				ParseStateToken(const char* str, char* token) const;
	void						Copy(const AnimStateID& id);
	EArray<ASPrefixMask*> masks;
	DString					name;
};

//==================================================================================
//reference to anim state group of character
//stores anim state group name and position on graph (anim state group == single graph canvas)
//==================================================================================
class ASGroupRef
{
public:
	ASGroupRef();
	ASGroupRef(const char* GroupName);
	void SetGraphPos(const FPoint& pt);
	void SetGraphPos(float x, float y);
	void SetGroupName(const char* GroupName);
	const DString& GetGroupName() const;
	const FPoint& GetGraphPos() const;
	bool operator> (const ASGroupRef& ref);
	bool operator< (const ASGroupRef& ref);
	bool operator== (const ASGroupRef& ref);
//	ENF_DECLARE_MMOBJECT(ASGroupRef);
private:
	DString GroupName;
	FPoint GraphPos;	//position on graph
};

//==================================================================================
//anim state group. this stores only graph window state parametres but every instance with referencies from anim state classes define specific graph view
//==================================================================================
class ASGroup
{
public:
	ASGroup(const char* name);
	~ASGroup();
	inline const DString& GetName() const {return name;}
	void Save(TiXmlElement* parent);
	void SetGraphZoomLevel(int zoom);
	inline int GetGraphZoomLevel(){return GraphZoom;}
	void SetGraphViewPoint(const FPoint& pt);
	inline const FPoint& GetGraphViewPoint(){return GraphViewPoint;}
	ASGroup* Clone() const;
	ENF_DECLARE_MMOBJECT(ASGroup);
private:
	DString name;
	int GraphZoom;
	FPoint GraphViewPoint;
};

//==================================================================================
//param config variable in "anim" version of editor
//==================================================================================
class PCVarA : public PCVar
{
public:
	PCVarA(const char* name, PCNode* owner, const PCDefParam* definition = NULL);
	virtual void Save(TiXmlElement* parent);
	ENF_DECLARE_MMOBJECT(PCVarA);
};

//==================================================================================
//param config array in "anim" version of editor
//==================================================================================
class PCArrayA : public PCArray
{
public:
	PCArrayA(const char* name, PCNode* owner, const PCDefParam* definition = NULL);
	virtual void Save(TiXmlElement* parent);
	ENF_DECLARE_MMOBJECT(PCArrayA);
};

//==================================================================================
//param config class in "anim" version of editor
//==================================================================================
class PCClassA : public PCClass
{
public:
	PCClassA(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition = NULL);
	virtual PCClass* CreateSameClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition = NULL) const;
	virtual void LoadEditData(TiXmlElement* elm);
	virtual void SaveEditData(TiXmlElement* elm);
	ENF_DECLARE_MMOBJECT(PCClassA);
};

//==================================================================================
//param config command in "anim" version of editor
//==================================================================================
class PCCommandA : public PCCommand
{
public:
	PCCommandA(const char* name, PCNode* owner);
	virtual void Save(TiXmlElement* parent);
	ENF_DECLARE_MMOBJECT(PCCommandA);
};

//==================================================================================
//param config character class. is a logical owner and manager for animstate groups (ASGroup) and anim state classes (ASClass)
//this class register all anim state classes (ASClass) that are in file structure located under it for fast searching by name and "prefix mask"
//==================================================================================
class ACharacterClass : public PCClassA
{
public:
	ACharacterClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition = NULL);
	virtual ~ACharacterClass();
	virtual PCClass* CreateSameClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition = NULL) const;
	virtual PCClass* Clone(PCNode* owner) const;
	virtual void LoadEditData(TiXmlElement* elm);
	virtual void SaveEditData(TiXmlElement* elm);
	ParamConfigAnm*			GetParamConfig();
	uint							AddGroup(ASGroup* group);
	bool 							RemoveGroup(ASGroup* group);
	ASGroup*						GetGroup(uint group);
	uint							GetGroupsCount();
	void							ClearGroups();
	int							GetCurrentGroup();
	ASGroup*						GetCurrentGroupPtr();
	bool							SetCurrentGroup(int group);
	ASGroup*						FindGroup(const char* name, int* index = NULL);
	void							RegisterStateClass(ASClass* state);
	void							UnregisterStateClass(ASClass* state);
	ASClass*						FindStateClass(const char* name);	//fast state search from name
	inline EArray<ASClass*>& GetStates(){return StatesArray;}
	void							FindStates(const char* str, const ASPrefixMask* mask1, const ASPrefixMask* mask2, EArray<ASClass*>& result);	//find states from prefix mask(s)
	ENF_DECLARE_MMOBJECT(ACharacterClass);
private:
	EHashMap<const char*, ASClass*, StringHashFunc> StatesHash;	//states storage for fast searching from name
	EArray<ASClass*>	StatesArray;										//duplicity states storage for iterations
	EArray<ASGroup*>	StateGroups;
	bool					registered;
	int					CurGroup;
};

//==================================================================================
//anim state class. in graph window visualized as rounted box with connections from/to
//this class stores referencies to all created graph views (ASGroup class represent single graph view) where is contained
//==================================================================================
class ASClass : public PCClassA	//anim state class
{
public:
	ASClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition = NULL);
	virtual ~ASClass();
	virtual PCClass* CreateSameClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition = NULL) const;
	virtual PCClass* Clone(PCNode* owner) const;
	virtual void LoadEditData(TiXmlElement* elm);
	virtual void SaveEditData(TiXmlElement* elm);
	virtual void AfterRename(const char* OldName);
	uint AddRef(const ASGroupRef& ref);					//add state group reference. this makes state visible on single graph view defined with ASGroupRef
	bool RemoveRef(const ASGroupRef& item);			//remove state group reference. this causes remove state from specific graph view
	void ClearRefs();											//this causes remove state from all graph views
	const ASGroupRef& GetReference(uint member) const;
	ASGroupRef* FindGroupRef(const char* name);
	uint GetRefsCount() const;
	ACharacterClass* GetCharacter();
	inline const AnimStateID& GetAnimId(){return AnimID;}
	PCClassA* GetActionsClass();
	ENF_DECLARE_MMOBJECT(ASClass);
private:
	ESet<ASGroupRef>	refs;
	AnimStateID			AnimID;
};

//==================================================================================
//extended version of ParamConfig from GenericEditorBase project
//first difference to ParamConfig is creating all members in extended version with postfix "A" like PCClassA inherited from PCClass, PCVarA inherited from PCVar ... this is needed to storing some edit data and save in working XML fromat
//second diference to ParamConfig is that this class understand of meaning "anim" config file structure and on the ground of this creates these classes in extended "anim" version:
//ACharacterClass - class represent anim character
//ASClass         - class represent anim state
//this class register all character classes in one list for fast acces during edit process
//==================================================================================
class ParamConfigAnm : public ParamConfig
{
public:
									ParamConfigAnm();
	virtual						~ParamConfigAnm();
	virtual inline				ParamConfigAnm* ToConfig()  {return this;}
	virtual void				Clear();
	CResult						Load(const char* FileName);			//load from working XML version
	CResult						Save(const char* FileName);			//load to working XML version
	void							Load(TiXmlElement* elm);
	TiXmlElement*				SaveClass(PCClass* Cl, TiXmlElement* parent);
	PCClass*						LoadClass(PCClass* owner, TiXmlElement* elm);
	PCArray*						LoadArray(PCNode* owner, TiXmlElement* elm);
	PCVar*						LoadVariable(PCNode* owner, TiXmlElement* elm);
	PCCommand*					LoadCommand(PCClass* owner, TiXmlElement* elm);
	CResult						Import(const char* BISParamFile);	//load from BIS param file
	CResult						Export(const char* BISParamFile);	//export to BIS param file
	virtual DString			GetSaveFilename();
//	virtual const char*		GetWorkFileExtension();
	bool							ConvertToPrefixedID(AnimStateID& id);
	bool							ConvertToUnprefixedID(AnimStateID& id);
//	DString						GetStateName(const AnimStateID& id);
	PCClass*						GetState(const AnimStateID& id);
	void							RegisterCharacterClass(ACharacterClass* character);
	void							UnregisterCharacterClass(ACharacterClass* character);
	ACharacterClass*			GetCurrentCharacterPtr();
	int							GetCurrentCharacter();
	void							SetCurrentCharacter(int character);
	ACharacterClass*			FindCharacter(const char* name, int* position = NULL);
	uint							GetCharactersCount();
	ACharacterClass*			GetCharacter(uint index);
	bool							SetCurrentCharacterAndGroup(const char* CharacterName, const char* GroupName);
	virtual int					GetClassInfoFlags(PCClass* owner, PCClass* base);
	virtual PCClassA*			CreateClass(const char* name, PCNode* owner, const char* OwnerID, PCClass* base, const char* BaseID, bool defined, const PCDefClass* definition, bool FromLoading, int ClassInfoFlags);
	virtual PCArray*			CreateArray(const char* name, PCNode* owner, const PCDefParam* definition = NULL);
	virtual PCVar*				CreateVariable(const char* name, PCNode* owner, const PCDefParam* definition = NULL);
	virtual PCCommand*		CreateCommand(const char* name, PCNode* owner);
									ENF_DECLARE_MMOBJECT(ParamConfigAnm);
private:
	EArray<ACharacterClass*>	characters;
	int								CurCharacter;
};

extern ParamConfigAnm* GetAnimConfigPtr();

#endif

