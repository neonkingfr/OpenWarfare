
#ifndef ACTIONS_PANEL
#define ACTIONS_PANEL

#include "wx/panel.h"
#include "wx/vlbox.h"

class ASClass;
class PCArray;
class WBPropertyGrid;
class wxPGProperty;

// panel to show fixes actions list of selected (and focused) state in AnimParamFileTree
//================================================================================
class ActionsPanel : public wxPanel
{
public:
	ActionsPanel(wxWindow* parent);
	void ShowActionsOfState(ASClass* state);
	void Clear();
	wxPGProperty* ShowArrayProperties(PCArray* arr, wxPGProperty* ParentProp, const char* label, const char* name, bool ClassOwner);
private:
	WBPropertyGrid*	PropGrid;
};

#endif