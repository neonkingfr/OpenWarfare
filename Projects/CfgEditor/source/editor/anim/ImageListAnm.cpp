
#include "precomp.h"
#include "ImageListAnm.h"

#include "bitmaps/AnimState.xpm"
#include "bitmaps/AnimStateInGraph.xpm"

int BI_STATE_CLASS;
int BI_STATE_CLASS_ON_GRAPH;

//--------------------------------------------------------------------------------------------------
ImageListAnm::ImageListAnm(int width, int height, const bool mask, int initialCount)
	:ImageList(width, height, mask, initialCount)
{
	BI_STATE_CLASS = Add(wxBitmap(AnimState_xpm));
	BI_STATE_CLASS_ON_GRAPH = Add(wxBitmap(AnimStateInGraph_xpm));
}
