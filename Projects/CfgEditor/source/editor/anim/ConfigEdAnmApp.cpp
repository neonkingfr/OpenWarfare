
#include "precomp.h"
#include "ConfigEdAnmApp.h"
#include "base/GenericEditor.h"
#include "base/MainFrame.h"
#include "anim/AnimEditor.h"
#include "anim/MainFrameAnm.h"
#include "common/XmlConfig.h"

#include "wx/evtloop.h"
#include "wx/app.h"
#include "wx/msw/private.h"

#ifdef _DEBUG
//#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

IMPLEMENT_APP(ConfigEdAnmApp)

//----------------------------------------------------------------------------------------------------
class ConfigEdAnmAppEventLoop : public wxEventLoop
{
	bool PreProcessMessage(WXMSG* msg)
	{
		if(wxGetApp().GetEditor())
		{
			if(wxGetApp().GetEditor()->PreProcessMessage(msg) == true)
				return true;	//without next pocessing
		}

		return wxEventLoop::PreProcessMessage(msg);
	}
};

//---------------------------------------------------------------------------------------------
int ConfigEdAnmApp::MainLoop()
{
	wxEventLoop* myloop = new ConfigEdAnmAppEventLoop;
	wxEventLoop* prevloop = m_mainLoop;
	m_mainLoop = myloop;

   int res = m_mainLoop->Run();
	m_mainLoop = prevloop;
	delete myloop;
	return res;
}

// ---------------------------------------------------------------------------
ConfigEdAnmApp::ConfigEdAnmApp()
{
	editor = NULL;
}

// ---------------------------------------------------------------------------
bool ConfigEdAnmApp::OnInit()
{
#ifdef _DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif
//	_CrtSetBreakAlloc(449372);
	
	editor = new AnimEditor();

	CResult InitResult = editor->OnAppInit();	//editor init

	if(!InitResult.IsOk())
	{
		if(InitResult.IsCommented())
		{
			wxMessageDialog dialog(NULL, InitResult.GetComment(), "Init error", wxICON_ERROR | wxOK);
			dialog.ShowModal();
		}
		return false;
	}
	
	MainFrame* frame = editor->GetMainFrame();
	frame->Show(true);
	SetTopWindow(frame);

	XMLConfigNode* node = editor->GetConfig()->GetNode("defaults", true);
	node = node->GetChildNode("MainWindow", true);
	bool maximize = false;
	node->GetValue("maximize", maximize);

	if(maximize)
		frame->Maximize();

	return true;
}

//---------------------------------------------------------------------------------------------
int ConfigEdAnmApp::OnExit()	//application exit
{
	int res = wxApp::OnExit();	//first delete all GUI. windows here save configurations to config and more

	editor->OnAppExit();			//editor must delete all allocations
	delete editor;
	editor = NULL;

#ifdef ENF_LEAK_DETECTOR
          MemoryManager::Dump("\n\nMemoryManager: Memory leaks report\n");
#endif
	return res;
}
