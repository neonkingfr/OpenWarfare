
#include "precomp.h"
#include "StateGroupsPanel.h"
#include "ParamConfigAnm.h"
#include "anim/DiagramWin/DiagramWindow.h"
#include "anim/AnimEditor.h"
#include "anim/ConfigEdAnmApp.h"

#include "bitmaps/AddGroup.xpm"
#include "bitmaps/DeleteGroup.xpm"

BEGIN_EVENT_TABLE(StateGroupsPanel, wxPanel)
	EVT_COMBOBOX(-1, StateGroupsPanel::OnComboSwitch)
	EVT_TOOL(-1, StateGroupsPanel::OnToolClick)
END_EVENT_TABLE()

//=========================================================================================
StateGroupsPanel::StateGroupsPanel(wxWindow* parent)
	:wxPanel(parent, -1)
{
	toolbar = new wxToolBar(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 24), wxTB_FLAT | wxTB_NODIVIDER /*| wxTB_TEXT | wxTB_NOICONS | wxSTATIC_BORDER*/);
//	toolbar->SetToolBitmapSize(g_ImageList->GetBitmapSize());
	wxStaticText* CharatersComboLabel = new wxStaticText(toolbar, wxID_ANY, " character ", wxDefaultPosition, wxSize(-1, -1), wxALIGN_CENTER/*|wxST_NO_AUTORESIZE| wxRAISED_BORDER*/);
	wxStaticText* GroupsComboLabel = new wxStaticText(toolbar, wxID_ANY, "     group ", wxDefaultPosition, wxSize(-1, -1), wxALIGN_CENTER/*|wxST_NO_AUTORESIZE| wxRAISED_BORDER*/);
	CharatersCombo = new wxComboBox(toolbar, COMBO_CURRENT_CHARACTER, wxEmptyString, wxDefaultPosition, wxSize(150, -1), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);
	GroupsCombo = new wxComboBox(toolbar, COMBO_CURRENT_GROUP, wxEmptyString, wxDefaultPosition, wxSize(150, -1), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);

	CharatersCombo->Disable();
	GroupsCombo->Disable();
	UpdateGroupButtonsAccesibility();

	toolbar->AddControl(CharatersComboLabel);
	toolbar->AddControl(CharatersCombo);
	toolbar->AddControl(GroupsComboLabel);
	toolbar->AddControl(GroupsCombo);
	toolbar->AddTool(TOOL_ADD_GROUP, wxT("add group"), wxBitmap(AddGroup_xpm), "Add current state group");
	toolbar->AddTool(TOOL_DELETE_GROUP, wxT("delete group"), wxBitmap(DeleteGroup_xpm), "Delete current state group");
	toolbar->Realize();

	DiagramPanel = new DiagramWindowPanel(this, &wxGetApp());

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(toolbar, 0, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
	sizer->Add(DiagramPanel, 1, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
//	sizer->Add(ConPanel, 0, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
	SetSizer(sizer);
}

//------------------------------------------------------------------------------------------
uint StateGroupsPanel::AddGroup(const char* group)
{
	return GroupsCombo->Append(group);
}

//------------------------------------------------------------------------------------------
bool StateGroupsPanel::RemoveGroup(const char* group)
{
	int item = GroupsCombo->FindString(group);
	enf_assert(item >= 0);
	GroupsCombo->Delete(item);
	return true;
}

//------------------------------------------------------------------------------------------
void StateGroupsPanel::ClearGroups()
{
	GroupsCombo->Clear();
}

//------------------------------------------------------------------------------------------
void StateGroupsPanel::SetCurrentGroup(int group)
{
	enf_assert(group < (int)GroupsCombo->GetCount());
	GroupsCombo->Select(group);
}

//------------------------------------------------------------------------------------------
uint StateGroupsPanel::AddCharacter(const char* character)
{
	return CharatersCombo->Append(character);
}

//------------------------------------------------------------------------------------------
bool StateGroupsPanel::RemoveCharacter(const char* character)
{
	int item = CharatersCombo->FindString(character);
	enf_assert(item >= 0);
	CharatersCombo->Delete(item);
	return true;
}

//------------------------------------------------------------------------------------------
void StateGroupsPanel::SetCurrentCharacter(int character)
{
	enf_assert(character < (int)CharatersCombo->GetCount());
	CharatersCombo->Select(character);	
}

//------------------------------------------------------------------------------------------
void StateGroupsPanel::ClearCharacters()
{
	CharatersCombo->Clear();
}

//------------------------------------------------------------------------------------------
void StateGroupsPanel::OnComboSwitch(wxCommandEvent& event)
{
	if(event.GetId() == COMBO_CURRENT_GROUP)
	{
		ParamConfigAnm* config = g_aeditor->GetParamConfig();
		ACharacterClass* character = config->GetCurrentCharacterPtr();
		enf_assert(character);
		character->SetCurrentGroup(GroupsCombo->GetSelection());
		g_aeditor->OnASGroupChanged();
		g_aeditor->GetDiagramWindow()->RepaintFromClient();
	}
	else if(event.GetId() == COMBO_CURRENT_CHARACTER)
	{
		ParamConfigAnm* config = g_aeditor->GetParamConfig();
		config->SetCurrentCharacter(CharatersCombo->GetSelection());
		g_aeditor->OnCharacterChanged();
	}
}

//------------------------------------------------------------------------------------------
void StateGroupsPanel::OnToolClick(wxCommandEvent& event)
{
	ParamConfigAnm* config = g_aeditor->GetParamConfig();
	ACharacterClass* character = config->GetCurrentCharacterPtr();
	enf_assert(character);

	switch(event.GetId())
	{
		case TOOL_ADD_GROUP:
		{
			wxString input;
			int res = -1;
			while (res == -1)
			{
				res = GetGroupNameFromUser(input);

				if(res == 1 && character->FindGroup(input.c_str()))
				{
					res = -1;
					wxString msg("Group ");
					msg += input;
					msg += "already exist! use another name";
					wxMessageDialog dialog( g_MainFrame, msg, "Group duplicity detected", wxICON_QUESTION | wxYES_NO);
					dialog.ShowModal();
				}
			}

			if(res == 1)
			{
				g_aeditor->CreateStatesGroup(input.c_str());
				GroupsCombo->Enable(true);
				UpdateGroupButtonsAccesibility();
			}
			break;
		}
		case TOOL_DELETE_GROUP:
		{
			int CurIndex = character->GetCurrentGroup();

			if(CurIndex != -1)
			{
				ASGroup* group = character->GetCurrentGroupPtr();
				enf_assert(group);

				if(group)
				{
					wxString msg = "Realy delete animstate group ";
					msg += group->GetName().c_str();
					msg += '?';
					wxMessageDialog dialog( g_MainFrame, msg, "Delete anim state group", wxICON_QUESTION | wxYES_NO);

					if(dialog.ShowModal() == wxID_YES)
					{
						g_aeditor->DeleteStatesGroup(group->GetName().c_str());
						GroupsCombo->Enable(GroupsCombo->GetCount() > 0);
						UpdateGroupButtonsAccesibility();
					}
				}
			}
			break;
		}
	}

	event.Skip();
}

//------------------------------------------------------------------------------------------
int StateGroupsPanel::GetGroupNameFromUser(wxString& input)
{
	wxTextEntryDialog dialog(NULL, _T(""), wxString("Enter new group name"), input, wxOK | wxCANCEL);

	if(dialog.ShowModal() != wxID_OK)
		return 0;

	input = dialog.GetValue();

	if(input.IsEmpty())
	{
		wxMessageDialog dialog( g_MainFrame, "Empty string is not valid group name!", "Bad input", wxICON_INFORMATION | wxOK);
		dialog.ShowModal();
		return -1;
	}
	return 1;
}

//------------------------------------------------------------------------------------------
void StateGroupsPanel::UpdateGroupButtonsAccesibility()
{
	ParamConfigAnm* config = g_aeditor->GetParamConfig();
	ACharacterClass* character = config ? config->GetCurrentCharacterPtr() : NULL;
	bool add = (character != NULL);
	bool del = ((character != NULL) && character->GetGroupsCount() > 0);
	toolbar->EnableTool(TOOL_ADD_GROUP, add);
	toolbar->EnableTool(TOOL_DELETE_GROUP, del);
}