
#ifndef CONNECTION_PANEL_H
#define CONNECTION_PANEL_H

#include "wx/panel.h"

class wxCheckBox;
class wxTextCtrl;
class wxStaticText;
class wxButton;

enum
{
	CHECKBOX_CONNECT,
	CHECKBOX_INTERPOLATE,
	CHECKBOX_IGNORE_SET_VALUES,
	EDITBOX_WEIGHT_CONNECT,
	EDITBOX_WEIGHT_INTERP,
	BUTTON_OK
};

//==========================================================================
//GUI to create/delete/modify connection betwen two anim states
//==========================================================================
class ConnectionPanel : public wxPanel
{
public:
	ConnectionPanel(wxWindow* parent);
	void Set(const char* SrcLabel, const char* DestLabel, bool connect, bool interpolate, const char* ConnectWeight, const char* InterpWeight);
	void UpdateAccesibility(bool DisableAll = false);
	void PressOkButton();
private:
	wxString SourceName;
	wxString DestName;
	wxStaticText* SourceLabel;
	wxStaticText* DestLabel;
	wxCheckBox* ConnectCheck;
	wxCheckBox* InterpCheck;
	wxCheckBox* IgnoreSetValuesCheck;
	wxTextCtrl*	ConnectWeightEditBox;
	wxTextCtrl*	InterpWeightEditBox;
	wxButton*	OkButton;
	void OnCheckBox(wxCommandEvent& event);
	void OnButton(wxCommandEvent& event);
	void OnText(wxCommandEvent& event);
	void OnTextEnter(wxCommandEvent& event);
	void OnOkButton();
	DECLARE_EVENT_TABLE()
};

#endif