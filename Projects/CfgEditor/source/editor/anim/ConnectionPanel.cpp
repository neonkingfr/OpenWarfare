
#include "precomp.h"
#include "ConnectionPanel.h"
#include "anim/AnimEditor.h"
#include "anim/ParamConfigAnm.h"
#include "common/WxConvert.h"
#include "common/NativeUtil.h"
#include "wx/wx.h"

BEGIN_EVENT_TABLE(ConnectionPanel, wxPanel)
	EVT_CHECKBOX(-1, ConnectionPanel::OnCheckBox)
	EVT_BUTTON(-1, ConnectionPanel::OnButton)
	EVT_TEXT(-1, ConnectionPanel::OnText)
	EVT_TEXT_ENTER(-1, ConnectionPanel::OnTextEnter)
END_EVENT_TABLE()

//=======================================================================
ConnectionPanel::ConnectionPanel(wxWindow* parent)
	:wxPanel(parent, -1, wxDefaultPosition, wxSize(-1, 30))
{
	SourceLabel = new wxStaticText(this, -1, "from:", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	DestLabel = new wxStaticText(this, -1, "to:", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	ConnectCheck = new wxCheckBox(this, CHECKBOX_CONNECT, "connect", wxDefaultPosition, wxDefaultSize);
	InterpCheck = new wxCheckBox(this, CHECKBOX_INTERPOLATE, "interpolate", wxDefaultPosition, wxDefaultSize);
	ConnectWeightEditBox = new wxTextCtrl(this, EDITBOX_WEIGHT_CONNECT, wxEmptyString, wxDefaultPosition, wxSize(80, 20), wxTE_PROCESS_ENTER);
	InterpWeightEditBox = new wxTextCtrl(this, EDITBOX_WEIGHT_INTERP, wxEmptyString, wxDefaultPosition, wxSize(80, 20), wxTE_PROCESS_ENTER);
	OkButton = new wxButton(this, BUTTON_OK, "Accept", wxPoint(189, 67), wxSize(63, 63));
//	wxStaticText* ConnectWeightEditBoxLabel = new wxStaticText(this, -1, "weight", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
//	wxStaticText* InterpWeightEditBoxLabel = new wxStaticText(this, -1, "weight", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	wxBoxSizer* ConnectSizer = new wxBoxSizer(wxHORIZONTAL);
	ConnectSizer->Add(ConnectCheck, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);
	ConnectSizer->AddSpacer(20);
	ConnectSizer->Add(ConnectWeightEditBox, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);

	wxBoxSizer* InterpSizer = new wxBoxSizer(wxHORIZONTAL);
	InterpSizer->Add(InterpCheck, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);
	InterpSizer->AddSpacer(6);
	InterpSizer->Add(InterpWeightEditBox, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);

	wxStaticBox* ValuesBox = new wxStaticBox(this, -1, "", wxDefaultPosition, wxDefaultSize);
	wxBoxSizer* ValuesSizer = new wxStaticBoxSizer(ValuesBox, wxVERTICAL);
	ValuesSizer->AddSpacer(16);
	ValuesSizer->Add(ConnectSizer, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);
	ValuesSizer->Add(InterpSizer, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->AddSpacer(10);
	sizer->Add(SourceLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 10);
	sizer->AddSpacer(10);
	sizer->Add(DestLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 10);
	sizer->AddSpacer(15);
	sizer->Add(ValuesSizer, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 10);
	sizer->AddSpacer(10);
	SetSizer(sizer);

	IgnoreSetValuesCheck = new wxCheckBox(this, CHECKBOX_IGNORE_SET_VALUES, "lock these settings", wxPoint(15, 61), wxDefaultSize);
	UpdateAccesibility(true);
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::Set(const char* SrcLabel, const char* DestLabel, bool connect, bool interpolate, const char* ConnectWeight, const char* InterpWeight)
{
	SourceName = SrcLabel;
	DestName = DestLabel;
	this->SourceLabel->SetLabel(wxString("from: ") + SrcLabel);
	this->DestLabel->SetLabel(wxString("to:     ") + DestLabel);

	if(!IgnoreSetValuesCheck->IsChecked())
	{
		ConnectCheck->SetValue(connect);
		InterpCheck->SetValue(interpolate);
		ConnectWeightEditBox->SetValue(ConnectWeight);
		InterpWeightEditBox->SetValue(InterpWeight);
	}

	UpdateAccesibility();
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::UpdateAccesibility(bool DisableAll)
{
	if(DisableAll)
	{
		ConnectCheck->Enable(false);
		ConnectWeightEditBox->Enable(false);
		InterpCheck->Enable(false);
		InterpWeightEditBox->Enable(false);
		OkButton->Enable(false);
		IgnoreSetValuesCheck->Enable(false);
	}
	else
	{
		ConnectCheck->Enable(true);
		ConnectWeightEditBox->Enable(ConnectCheck->IsChecked());
		InterpCheck->Enable(true);
		InterpWeightEditBox->Enable(InterpCheck->IsChecked());
		OkButton->Enable(true);
		IgnoreSetValuesCheck->Enable(true);
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnCheckBox(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case CHECKBOX_CONNECT:
		UpdateAccesibility();
		break;
	case CHECKBOX_INTERPOLATE:
		UpdateAccesibility();
		break;
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnText(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case EDITBOX_WEIGHT_CONNECT:
		OkButton->Enable(true);
		break;
	case EDITBOX_WEIGHT_INTERP:
		OkButton->Enable(true);
		break;
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnTextEnter(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case EDITBOX_WEIGHT_CONNECT:
		if(InterpWeightEditBox->IsEnabled())
		{
			InterpWeightEditBox->SetFocus();
			InterpWeightEditBox->SetSelection(0, InterpWeightEditBox->GetLineLength(0));
		}
		else
			OkButton->SetFocus();
		break;
	case EDITBOX_WEIGHT_INTERP:
		OkButton->SetFocus();
		break;
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnButton(wxCommandEvent& event)
{
	switch(event.GetId())
	{
		case BUTTON_OK:
		{
			OnOkButton();
			break;
		}
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnOkButton()
{
	wxString ConnectWeightStr = ConnectWeightEditBox->GetValue();
	wxString InterpWeightStr = InterpWeightEditBox->GetValue();

	float ConnectWeight = AtoF(ConnectWeightStr);
	float InterpWeight = AtoF(InterpWeightStr);

	if(!IsFloatNumber(ConnectWeightStr.c_str()) || !IsFloatNumber(InterpWeightStr.c_str()) || ConnectWeight < 0.0f || /*ConnectWeight > 1.0f ||*/ InterpWeight < 0.0f /*|| InterpWeight > 1.0f*/) // Weight can be bigger that 1
	{
		wxMessageDialog dialog(g_editor->GetMainFrame(), "weight values in editboxes must be floating value in range 0 - 1", _T("Update connection error"), wxICON_INFORMATION | wxOK);
		dialog.ShowModal();
		return;
	}

	ParamConfigAnm* PConfig = g_aeditor->GetParamConfig();
	ACharacterClass* character = PConfig->GetCurrentCharacterPtr();

	ASClass* SrcState = character->FindStateClass(SourceName.c_str());
	ASClass* DestState = character->FindStateClass(DestName.c_str());
	enf_assert(SrcState);
	enf_assert(DestState);

	if(SrcState && DestState)
	{
		ConnectionRef connection(SrcState, DestState);

		if(ConnectCheck->IsChecked())
			connection.SetConnect(ConnectWeight);

		if(InterpCheck->IsChecked())
			connection.SetInterpolate(InterpWeight);

		g_aeditor->UpdateConnection(connection);
		OkButton->Enable(false);
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::PressOkButton()
{
	if(OkButton->IsEnabled())
		OnOkButton();
}