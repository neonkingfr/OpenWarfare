#include "precomp.h"
#include "base/MainFrame.h"
#include "base/GenericEditor.h"
#include "base/PropertiesPanel.h"
#include "base/ParamFileTree.h"
#include "common/WxNativeConvert.h"
#include "base/ImageList.h"
#include "wxExtension/WBPropertyGrid.h"
#include "bitmaps/ParamClassSuper.xpm"
#include "wx/listbook.h"
#include "base/OptionsDialog.h"

#include "VBSMainFrame.h"
#include "VBSConfigEditor.h"
#include "VBSOptionsDialog.h"


BEGIN_EVENT_TABLE(VBSMainFrame, MainFrame)
EVT_MENU(-1, VBSMainFrame::OnSelectFromMenu)
//EVT_CLOSE(VBSMainFrame::OnClose)
//EVT_AUI_PANE_CLOSE(VBSMainFrame::OnPaneClose)
//	EVT_IDLE(MainFrame::OnIdle)
END_EVENT_TABLE()

//----------------------------------------------------------------------
wxMenuBar* VBSMainFrame::CreateMenuBar()
{
	wxMenuBar* MenuBar = new wxMenuBar;										

	FileMenu = CreateFileMenu();
	MenuBar->Append(FileMenu, _T("&File"));

	EditMenu = CreateEditMenu();
	MenuBar->Append(EditMenu, _T("&Edit"));

	/*
	ToolsMenu = CreateToolsMenu();
	MenuBar->Append(ToolsMenu, _T("&Tools"));
	*/

	WindowMenu = CreateWindowsMenu();
	MenuBar->Append(WindowMenu, _T("&Window"));

	OptionsMenu = new wxMenu;
	OptionsMenu->Append(MENU_OPTIONS_DIALOG, _T("Settings"), _T("app settings"));

	MenuBar->Append(OptionsMenu, _T("&Options"));

	return MenuBar;
}

//----------------------------------------------------------------------
wxMenu* VBSMainFrame::CreateFileMenu()
{
//	return MainFrame::CreateFileMenu();

   wxMenu* menu = new wxMenu;
	menu->Append(MENU_NEW, _T("New\tCtrl-N"), _T("create new param config"));
	menu->Append(MENU_LOAD, _T("Open\tCtrl-O"), _T("load param file config"));
//	menu->Append(MENU_SEQUENCE_LOAD, _T("Open sequence\tCtrl-U"), _T("load param file config with preload"));
	menu->Append(MENU_SAVE, _T("Save\tCtrl-S"), _T("save param file config"));
	menu->Append(MENU_SAVE_AS, _T("Save As"), _T("save param file config"));
	menu->Append(MENU_GENERATE_DEF_FILE, _T("Generate definition"), _T("generate config definition file"));
	menu->Enable(MENU_SAVE, false);
	menu->Enable(MENU_SAVE_AS, false);
	menu->Enable(MENU_GENERATE_DEF_FILE, false);
	return menu;
}

//---------------------------------------------------------------------------------------------
wxToolBar* VBSMainFrame::CreateMainToolBar()
{
	wxToolBar* bar = new wxToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_FLAT | wxTB_NODIVIDER);
	bar->SetToolBitmapSize(wxSize(16, 16));
	bar->AddTool(MENU_NEW, wxT("New"), g_ImageList->GetBitmap(BI_NEW), wxT("New"));
	bar->AddTool(MENU_LOAD, wxT("Open"), g_ImageList->GetBitmap(BI_OPEN), wxT("Open"));
//	bar->AddTool(MENU_SEQUENCE_LOAD, wxT("Open sequence"), g_ImageList->GetBitmap(BI_OPEN_AS_UPDATE), wxT("Open"));
	bar->AddTool(MENU_SAVE, wxT("Save"), g_ImageList->GetBitmap(BI_SAVE), wxT("Save"));
	bar->AddTool(MENU_UNDO, wxT("Undo"), g_ImageList->GetBitmap(BI_UNDO), wxT("Undo"));
	bar->AddTool(MENU_REDO, wxT("Redo"), g_ImageList->GetBitmap(BI_REDO), wxT("Redo"));
	bar->AddCheckTool(MENU_BASE_CLASSES_IN_TREE, wxT("Show base classes int tree"), g_ImageList->GetBitmap(BI_SHOW_BASE_CLASS), wxNullBitmap, wxT("Show base classes in tree"));
	bar->EnableTool(MENU_SAVE, false);
	bar->EnableTool(MENU_UNDO, false);
	bar->EnableTool(MENU_REDO, false);
	bar->Realize();
	return bar;
}

//=============================================================================
OptionsDialogPanel* VBSMainFrame::CreateOptionsPanel(OptionsDialog* parent)
{
	OptionsDialogPanel* panel = new VBSOptionsDialogPanel(parent);

	return panel;
}

//=============================================================================
wxMenu* VBSMainFrame::CreateToolsMenu()
{
	wxMenu* menu = new wxMenu;
	menu->Append(MENU_CHANGE_TEMP_DIR, _T("Change workdir\tCtrl-W"), _T("change_dir"));
	menu->Append(MENU_CHANGE_DATA_DIR, _T("Change data directory\tCtrl-D"), _T("change_data_dir"));
	menu->Append(MENU_CHANGE_BIN_DIR, _T("Change external bin directory\tCtrl-E"), _T("change_bin_dir"));
	menu->Enable(MENU_CHANGE_TEMP_DIR,false);

	return menu;
}


//=============================================================================
int VBSMainFrame::GetIndexOfPane(wxString& PaneCaption)
{
	wxAuiPaneInfoArray& panes = AuiManager->GetAllPanes();

	for(uint n = 0; n < panes.GetCount(); n++)
	{
		if(panes[n].caption == PaneCaption)
			return n;
	}
	return -1;
}

//=============================================================================
void VBSMainFrame::OnSelectFromMenu(wxCommandEvent& event)
{
	int ID = event.GetId();

	switch(ID)
	{
	case MENU_SAVE:
		/*static_cast<VBSConfigEditor*>(g_editor)->*/g_editor->Save();
		break;
	case MENU_CHANGE_TEMP_DIR:
		static_cast<VBSConfigEditor*>(g_editor)->SetTempDir();
		break;
	case MENU_CHANGE_DATA_DIR:
		static_cast<VBSConfigEditor*>(g_editor)->SetDataDir();
		break;
	case MENU_CHANGE_BIN_DIR:
		static_cast<VBSConfigEditor*>(g_editor)->SetBinDir();
		break;

	}
	event.Skip();	//message continues to MainFrame::OnSelectFromMenu()
}
/*
//=============================================================================
void VBSMainFrame::UpdateTitle()
{
	wxString title(g_editor->GetAppName());
	title += " - ";
	VBSParamConfig* cfg = static_cast<VBSParamConfig*>(g_editor->GetParamConfig());
	assert(cfg);
	DString file = cfg->GetPBOName().c_str();

	if(!file.empty())
	{
		wxString file2 = file.c_str();

		if(file2.Find('/'))
			file2 = file2.AfterLast('/');

		title += file2;
	}
	else
		title += "unnamed";

	if(g_editor->AnyChanges())
		title += '*';

	SetTitle(title);
}
*/

//=============================================================================
void VBSMainFrame::OnClose(wxCloseEvent& event)
{
	// procced event in parent
	event.Skip();
}
