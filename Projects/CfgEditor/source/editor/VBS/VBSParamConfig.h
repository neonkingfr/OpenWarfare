#pragma once

#include "base/ParamConfig.h"
#include <vector>

//===============================================================================
class VBSParamConfig : public ParamConfig
{
public:
	VBSParamConfig();
	~VBSParamConfig();
	typedef std::vector<std::string> TVecConfigNames;

	/** Load config file from disk.

		\param FileName loading file name
		\param IsPbo VBS addon loading flag
	*/
	virtual CResult	Load(const char* FileName, bool IsPbo);

	/** Save config file to disk.

		\param FileName saving file name
	*/
	virtual CResult	Save(const char* FileName);

	/** Load VBS packed file from disk.

		\param pboFileName loading file name
		\param configFile VBS addon config file in pbo structure
	*/
	CResult	LoadPBOFile(const std::string &pboFilename, const std::string &configFile);

	/** Save config file to disk.

		\param FileName filename of temporary config in working
						directory
	*/
	virtual CResult	SaveConfigFile(const std::string &fileName);

	/** Save config file from working directory into VBS addon.
		NOTE. it may take a few minutes(depends on pbo size). 
		Method starts BankRev.exe app for unpack pbo, makes 
		neccesary changes and runs FileBank.exe for new addon
		build.		

		\param FileName Filename of temporary config in working
						directory
	*/
	CResult SavePBOFile(const char* FileName);

	/** Set working directory.
		NOTE. old working directory will be erase.

		\param dir new working directory
	*/
	void SetTempDir(const std::string &dir) { m_tempDir = dir; }
	void SetDataDir(const std::string &dir) { m_dataDir = dir; }
	void SetBinDir(const std::string &dir) { m_extBinPath = dir; }
	void SetCfgsDir(const std::string &dir) { m_baseCfgsPath = dir; }

	/** Remove entire working dir structure.

		\param buildPathAgain flag if path will be restored after
	*/
	bool CleanUpTemp(bool buildPathAgain);

	/// Return original addon filename
	std::string &GetPBOName() { return m_originalPBOpath; }
	
	/** Returns all config file from packed addon.
		Method goes through entire package and returns 
		all config files in its structure. Result will be store
		in <i>out</i> vector.

		\param from filenam of VBS addon
		\param out target vector using for result
	*/
	void GetAllConfigFiles(const std::string &from, TVecConfigNames &out);

	/// Clean up param config and set variables back to default.
	virtual void Clear();	

	void SetWorkDirPersist() { m_forceNotCleanDir=true;	}
	void CheckPersist();

protected:
	/// Translate result from LSError to CResult
	CResult	TranslateResult(LSError result);

	/** Read params from file and store them into <i>out</i> string.

		\param from file that has been generated from
					BankRev.exe and stores params for
					VBS addon

		\param out string with all params from input file
	*/
	void BuildPackParam(const std::string &from, std::string &out) const;

	/** Return name of VBS addon from internal variable.
		Result will be stored in <i>out</i>;

		\param out result string with PBO name
	*/
	void GetPBONameFromFile(std::string &out) const;

	/// Build TEMP(addon) directory in working directory.
	void CreateTempPath();

	/// full path to config.cpp/config.bin
	std::string m_originalConfigpath;

	/// full filename of opened VBS addon
	std::string m_originalPBOpath;

	/// path to temporary addon directory
	std::string m_pboDirectory;

	/// path to working directory
	std::string m_tempDir;
	
	/// path to data directory
	std::string m_dataDir;

	/// path to bankrev and filebank
	std::string m_extBinPath;

	/// path to base configs
	std::string m_baseCfgsPath;

	/// flag that store if config is from PBO
	bool m_isPbo;

private:

	/// flag for RESET ALL variables
	bool m_clearAll;

	bool m_forceNotCleanDir;
};