#pragma once

#include "wx/dialog.h"

//===========================================================================
class TreeMessageDialog : public wxDialog
{
public:
	TreeMessageDialog(wxWindow* parent, const wxString& title);
	
	/// Return handle to tree.
	inline wxTreeCtrl* GetTreeCtrl(){ return m_tree; }
	
	/// Set display message.
	void SetMessage(const wxString& message);			
	
	/// Return selected pbo path.
	const std::string &GetSelectedPath() { return m_selected; }
private:
	wxStaticText* m_messageArea;
	wxTreeCtrl* m_tree;
	wxButton*	m_okButton;
	wxButton*	m_cancelButton;
	wxTreeItemId m_itemId;
	std::string m_selected;

	void OnOkButtonClick(wxCommandEvent& event);
	void OnTreeSelChanged(wxTreeEvent& event);
	void OnTreeSubmit(wxTreeEvent& event);
	DECLARE_EVENT_TABLE();	
};
