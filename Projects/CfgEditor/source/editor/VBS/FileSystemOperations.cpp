#include "precomp.h"
#include "FileSystemOperations.h"
#include <windows.h>
#include <algorithm>
#include <cctype>

static const std::string DEFAULT_PATH = "..\\bin";
static const std::string FILEBANK_EXEC = "filebank.exe";
static const std::string BANKREV_EXEC = "bankrev.exe";

//=============================================================================
int CFileSystemOperations::GetFiles(const std::string &szDir, 
			 CFileSystemOperations::TvecFiles& fileList, 
			 bool bCountHidden)
{
	std::string path = szDir;
	WIN32_FIND_DATAA fd;
	DWORD dwAttr = FILE_ATTRIBUTE_DIRECTORY;
	if( !bCountHidden) dwAttr |= FILE_ATTRIBUTE_HIDDEN;
	path += "\\*";
	HANDLE hFind = FindFirstFileA(path.c_str(), &fd);
	if(hFind != INVALID_HANDLE_VALUE)
	{
		int count = 0;
		do
		{			
			if( !(fd.dwFileAttributes & dwAttr))
			{
				CFileSystemOperations::SFileInfo 
					fileinfo(fd.cFileName,fd.nFileSizeLow);

				fileList.push_back(fileinfo);
				++count;
			}

		}while( FindNextFileA( hFind, &fd));
		FindClose( hFind);
		return count;
	}

	return -1;
}

//=============================================================================
int CFileSystemOperations::GetFiles(const std::string &path, TvecFiles& fileList, 
		const std::string &file_ext, bool bCountHidden/*=false*/)
{
	// get all files from directory
	CFileSystemOperations::TvecFiles resultList;
	int result = GetFiles(path,resultList);

	// if files count is greater than zero
	if (result>0)
	{
		// get files with extension [file_ext]
		result = GetFiles(resultList,file_ext);

		// copy result 
		fileList = resultList;
	}
	else
		fileList.clear();

	// and return count of files(with all extensions)
	return result;
}

//=============================================================================
bool CFileSystemOperations::IsDirectoryEmpty(const std::string &pathStr)
{
	std::string path = pathStr;
	WIN32_FIND_DATAA fd;
	path += "\\*";
	HANDLE hFind = FindFirstFile(path.c_str(), &fd);
	
	if(hFind != INVALID_HANDLE_VALUE)
	{
		int count = 0;
		do {++count;} while( FindNextFileA( hFind, &fd));
		FindClose( hFind);

		// [.] [..] is ares files too 
		// doesn't work on root of drive c:/ e:/ etc...
		return count == 2;
	}

	return true;
}

//=============================================================================
bool CFileSystemOperations::DeleteFiles(const std::string &path, TvecFiles& fileList)
{
	bool res = true;
	for (TvecFiles::iterator it = fileList.begin(); it != fileList.end(); ++it)
	{
		std::string fileName = path + "/" + (*it).filename;
		if (!DeleteFile(fileName.c_str()))
			res = false;
	}
	return res;
}

//=============================================================================
bool CFileSystemOperations::IsExt(const std::string& strFilename, 
								 const std::string &strFile_ext)
{
	// test if filename has [file_ext] extension

	// copy to temporary string
	std::string filename = strFilename;
	std::string file_ext = strFile_ext;
	
	// change file ext to lower case
	std::transform(file_ext.begin(), file_ext.end(), file_ext.begin(),
               (int(*)(int)) std::tolower);

	// change entire filename to lower case
	std::transform(filename.begin(), filename.end(), filename.begin(),
		(int(*)(int)) std::tolower);

	// locate [file_ext] substring
	std::string::size_type extLoc = filename.find(file_ext);

	// if extension has been founded
	if (extLoc != std::string::npos)
	{
		// return if extension is [file_ext] type
		std::string::size_type locPos = filename.size()-file_ext.size();
		bool res = (locPos == extLoc);

		// and return result
		return res;
	}

	// otherwise return false
	return false;
}

//=============================================================================
int CFileSystemOperations::GetFiles(CFileSystemOperations::TvecFiles &fileList, 
									const std::string &file_ext)
{
	// get files from file list with [file_ext] extension

	// clear counter
	int count = 0;
	CFileSystemOperations::TvecFiles filteredVec;
	
	// go through entire file list and test file extension
	for (CFileSystemOperations::TvecFiles::iterator it=fileList.begin(); 
		it!=fileList.end(); ++it)
		if ( IsExt((*it).filename,file_ext) )
		{
			// if extensions passed
			// increase counter
			// and store file into result list
			++count;
			filteredVec.push_back(*it);
		}
		
	// so copy int result list 
	fileList = filteredVec;

	// and return count of files match
	return count;
}

//=============================================================================
std::string CFileSystemOperations::GetPath(const std::string &path)
{
	size_t rloc = path.rfind('\\',path.size());

	// if found
	// returns empty string
	if (rloc == std::string::npos)
		return std::string();

	return path.substr(0,rloc);
}

//=============================================================================
std::string CFileSystemOperations::GetFileName(const std::string &path)
{
	size_t rloc = path.rfind('\\',path.size());

	// if not found
	// filename is in root, so we 
	// can return it
	if (rloc == std::string::npos)
		return path;

	return path.substr(rloc+1);
}

//=============================================================================
CFileSystemOperations::TvecDirectories 
	CFileSystemOperations::GetDirectoriesToRoot(const std::string &path)
{
	// struct that stores result list
	CFileSystemOperations::TvecDirectories result;

	// helpers
	std::string homeDir = path;
	std::string currentDir;

	while (1)
	{
		// file name in directory structure
		// is current directory ;)
		currentDir = GetFileName(homeDir);
		
		// if isn't root store
		// directory into list
		if (homeDir.size())
		{
			result.push_back(currentDir);
			homeDir = GetPath(homeDir);
		}
		else
			break;
	}

	// and return result list
	return result;
}

//=============================================================================
bool IsDots(const TCHAR* str) 
{
	if(_tcscmp(str,".") && _tcscmp(str,"..")) 
		return false;

	return true;
}

//=============================================================================
bool CFileSystemOperations::DeleteDirectory(const TCHAR* sPath) 
{
	HANDLE hFind;    // file handle
	WIN32_FIND_DATA FindFileData;

	TCHAR DirPath[MAX_PATH];
	TCHAR FileName[MAX_PATH];

	_tcscpy(DirPath,sPath);
	_tcscat(DirPath,"\\*");    // searching all files
	_tcscpy(FileName,sPath);
	_tcscat(FileName,"\\");

	// find the first file
	hFind = FindFirstFile(DirPath,&FindFileData);
	if(hFind == INVALID_HANDLE_VALUE) return false;
	_tcscpy(DirPath,FileName);

	bool bSearch = true;
	while(bSearch) 
	{    // until we find an entry
		if(FindNextFile(hFind,&FindFileData)) 
		{
			if(IsDots(FindFileData.cFileName)) continue;
			_tcscat(FileName,FindFileData.cFileName);
			if((FindFileData.dwFileAttributes &
				FILE_ATTRIBUTE_DIRECTORY)) 
			{

					// we have found a directory, recurse
					if(!DeleteDirectory(FileName)) 
					{
						FindClose(hFind);
						return false;    // directory couldn't be deleted
					}
					// remove the empty directory
					RemoveDirectory(FileName);
					_tcscpy(FileName,DirPath);
			}
			else 
			{
				if(FindFileData.dwFileAttributes &
					FILE_ATTRIBUTE_READONLY)
					// change read-only file mode
					_chmod(FileName, _S_IWRITE);
				if(!DeleteFile(FileName)) 
				{    // delete the file
					FindClose(hFind);
					return false;
				}
				_tcscpy(FileName,DirPath);
			}
		}
		else 
		{
			// no more files there
			if(GetLastError() == ERROR_NO_MORE_FILES)
				bSearch = false;
			else 
			{
				// some error occurred; close the handle and return FALSE
				FindClose(hFind);
				return false;
			}

		}

	}
	FindClose(hFind);                  // close the file handle

	return (bool)RemoveDirectory(sPath);     // remove the empty directory
}

//=============================================================================
bool CFileSystemOperations::UnpackPBO(const std::string &path, 
									  const std::string &fullAddonName,
									  const std::string &whereDir)
{
	// path /bin/bankRev.exe
	// "c:\\workdir\\VBS\\bin\\BankRev.exe";
	// check if ext bin path exist
	std::string finalPath = path;
	if (!path.size())
		finalPath = DEFAULT_PATH;

	// find external app
	bool exist = CFileSystemOperations::CheckFileExist(BANKREV_EXEC,finalPath);
	if (!exist)
		return false;

	std::string appName = finalPath +"\\"+  BANKREV_EXEC;/*"..\\bin\\BankRev.exe";*/
	std::string fullCmd = std::string('"' + fullAddonName + '"' + " -f " + '"' + whereDir + '"');
	wxString tst = fullCmd;
	tst.Replace("/","\\");

	// tudiz cesta prasete
	system(std::string(appName +" "+  tst).c_str());

	return true;
}

//=============================================================================
bool CFileSystemOperations::PackPBO(const std::string &path,
									const std::string &addonDirectory, 
									const std::string &params,
									const std::string &targetPBO)
{
	// path /bin/bankRev.exe
	// "c:\\bpo_TEMP\\TEST\\FileBank.exe";
	// check if ext bin path exist
	std::string finalPath = path;
	if (!path.size())
		finalPath = DEFAULT_PATH;

	// find external app
	bool exist = CFileSystemOperations::CheckFileExist(FILEBANK_EXEC,finalPath);
	if (!exist)
		return false;

	std::string appName = finalPath +"\\"+  FILEBANK_EXEC;/*"..\\bin\\FileBank.exe";*/
	std::string fullCmd = std::string(params + " " + '"' + addonDirectory + '"');

	wxString tst = fullCmd;
	tst.Replace("/","\\");

	wxString whereDir = targetPBO;
	whereDir.Replace("/","\\");
	whereDir = whereDir.BeforeLast('\\');
	whereDir = '"' + whereDir + '"';

	std::string cmd = "-dst " + whereDir + " " + tst;
	// and execute 
	//ShellExecute(GetDesktopWindow(), ("open"), appName.c_str(), tst.c_str(), NULL, SW_NORMAL);
	system(std::string(appName + " " + cmd).c_str());

	return true;
}

//=============================================================================
bool CFileSystemOperations::CheckFileExist(const std::string &what, 
										   const std::string &where)
{
	CFileSystemOperations::TvecFiles out;
	return CFileSystemOperations::GetFiles(where,out,what)>0;
}