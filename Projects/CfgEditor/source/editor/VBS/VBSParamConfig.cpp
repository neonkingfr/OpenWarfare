#include "precomp.h"
#include "base/ParamConfig.h"
#include "base/TypesConvert.h"
#include "Common/WxConvert.h"
#include "Common/NativeConvert.h"
#include "Common/NativeUtil.h"
#include "Common/TextFile.h"
#include "Common/WinApiUtil.h"
#include "FileSystemOperations.h"
#include "VBSParamConfig.h"
#include <Windows.h>
#include <ImageHlp.h>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <sstream>

// some const
static const std::string CONFIG_FILE = "config.bin";
static const std::string CONFIG_FILE_NO_BIN = "config.cpp";
static const std::string CONFIG_DEF = "config_def.xml";
static const std::string TEST_CONFIG_DEF = "config_def.xml";
static const std::string DEF_DIR  = "definitions";
static const std::string PBO_DIRECTORY = "TEST";
static const std::string PBO_EXT = ".pbo";
static const std::string BIN_EXT = ".bin";
static const std::string CPP_EXT = ".cpp";
static const std::string TXT_EXT = ".txt";
//=============================================================================
bool IsConfig(const std::string filename)
{
	/*
	// try to find config.bin/config.cpp

	if (filename.find(CONFIG_FILE) != std::string::npos)
		return true;

	if (filename.find(CONFIG_FILE_NO_BIN) != std::string::npos)
		return true;
*/
	// try to find all *.bin or *.cpp
	if (CFileSystemOperations::IsExt(filename,BIN_EXT) || 
		CFileSystemOperations::IsExt(filename,CPP_EXT))
			return true;

	return false;
}

void StoreConfigFiles( const FileInfoO& fi, const FileBankType* files, void* context )
{
	std::string filename = std::string(fi.name);
	VBSParamConfig::TVecConfigNames* list = 
		static_cast<VBSParamConfig::TVecConfigNames*>(context);

	if (IsConfig(filename))
		list->push_back(filename);
}

//=============================================================================
void VBSParamConfig::GetAllConfigFiles(const std::string &from, 
									VBSParamConfig::TVecConfigNames &out)
{
	// make space in file bank
	ParamFile PFile;
	int index = GFileBanks.Add();
	QFBank& bank = GFileBanks[index];

	// erase extension
	wxString fullFilename = from;
	fullFilename = fullFilename.BeforeLast('.');

	// open bank for load configs
	bank.open( fullFilename.c_str() );	

	// fill addon internal files
	bank.ForEach( StoreConfigFiles, &out );

	GFileBanks[index].close();
}

//=============================================================================
CResult VBSParamConfig::SaveConfigFile(const std::string &fileName)
{
	ParamFile PFile;
	LSError result = LSOK;
	CResult res;

	// try to open config bank
	bool some = PFile.ParseBin(fileName.c_str());	
	// if fail.. try find config.cpp
	if (!some)
		result = PFile.Parse(fileName.c_str());

	res = TranslateResult(result);
	if (res==false)
		return res;

	if (CFileSystemOperations::IsExt(m_originalConfigpath,BIN_EXT))
	{
		std::string path = 
			m_tempDir + "\\" + m_pboDirectory + "\\" + CONFIG_FILE;

		bool allOk = PFile.SaveBin(path.c_str());
		if (!allOk)
		{
			res.Set(false,"Binary save failed!");
			return res;
		}
	}

	return TranslateResult(result);
}

//=============================================================================
void VBSParamConfig::GetPBONameFromFile(std::string &out) const
{
	// extract pbo directory from 
	// full filename
	wxString directoryName = m_originalPBOpath;
	directoryName.Replace("\\","/");
	directoryName = directoryName.BeforeLast('.');
	directoryName = directoryName.AfterLast('/');

	out = directoryName.c_str();
}

//=============================================================================
bool VBSParamConfig::CleanUpTemp(bool buildUpAgain)
{
	if (m_forceNotCleanDir)
		return false;

	bool res = CFileSystemOperations::DeleteDirectory(std::string(m_tempDir+"\\").c_str());		
	if (buildUpAgain)
		CreateTempPath();

	return res;
}

//=============================================================================
void VBSParamConfig::CheckPersist()
{
	if (!CFileSystemOperations::IsDirectoryEmpty(m_tempDir))
	{
		wxMessageBox("working directory isn't empty.\n CE will NOT erase it on exit.", 
			"WARNING",wxICON_EXCLAMATION);

		SetWorkDirPersist();
	}
}

//=============================================================================
void VBSParamConfig::CreateTempPath()
{
	// create temp
	std::string path = m_tempDir + "\\" + m_pboDirectory+"\\";
	wxString wxPath = path;
	wxPath.Replace("/","\\");
	path = wxPath.c_str();
	MakeSureDirectoryPathExists(path.c_str());

	// copy config_def.xml
	// from /bin
	char buf[256];
	taGetAplicationDir(buf);

	std::string defXml = path + TEST_CONFIG_DEF;
	std::string fromXml = std::string(buf) + "\\"+DEF_DIR+"\\" + TEST_CONFIG_DEF;
	CopyFile(fromXml.c_str(),defXml.c_str(),false);	
}

//=============================================================================
void VBSParamConfig::BuildPackParam(const std::string &from, std::string &out) const
{
	using namespace std;
	// read all params from file
	ifstream fileFrom(from.c_str(),ios::in);
	ostringstream os;

	while (fileFrom.good())
	{
		// one param per line
		std::string buf;
		getline(fileFrom,buf);
		
		// HACk kvuli Kuby
		// prefix=ca\some\ => prefix=ca\some
		if ( (buf.find("prefix=")==0) && 
			(buf.rfind('\\')==buf.size()-1) )
				buf.resize(buf.size()-1);

		// if data dir was set, change it
		if (buf.find("prefix=")==0 && m_dataDir.size())
			buf = "prefix="+m_dataDir;
		
		// add property prefix
		if (buf.size())
			os<< /*"-property " <<*/ buf <<' ';
	}

	// and store params into out string
	out = os.str();

	// remove last ' '
	if (out.size())
	{
		out.resize(out.size()-1);
		out = "-property " + out;
	}

	fileFrom.close();		
}

//=============================================================================
CResult	VBSParamConfig::SavePBOFile(const char* FileName)
{
	CResult res = SaveConfigFile(FileName);

	const std::string path = m_tempDir + "\\" + m_pboDirectory;
	// rozbalit puvodni PBO
	if (!CFileSystemOperations::UnpackPBO(m_extBinPath,m_originalPBOpath,path))
		return CResult(false,"ERROR during unpacking file\n(missing config.cpp or bankrev.exe?)");

	std::string directoryName; 
	GetPBONameFromFile(directoryName);

	// and build target directory
	std::string wherePath = path + "\\" + directoryName + "\\" + m_originalConfigpath;

	// select config.cpp or config.bin
	std::string from = CONFIG_FILE;
	if (CFileSystemOperations::IsExt(m_originalConfigpath,CPP_EXT))
		from = CONFIG_FILE_NO_BIN;

	// build from directory
	from = 
		m_tempDir + "\\" + m_pboDirectory + "\\" + from;

	// replace original config	
	BOOL tst = CopyFile(from.c_str(),wherePath.c_str(),true);
	if (!tst)
	{
		DeleteFile(wherePath.c_str());
		CopyFile(from.c_str(),wherePath.c_str(),true);
	}

	// vycist vsechny parametry
	std::string fromFile = path + "\\" + directoryName + TXT_EXT;
	std::string params;
	BuildPackParam(fromFile,params);

	// znovu pbo zapakovat
	std::string unpackPath = path + "\\" + directoryName;
	if (!CFileSystemOperations::PackPBO(m_extBinPath,unpackPath,params,m_originalPBOpath))
		return CResult(false,"ERROR during repacking file\n(missing config.cpp or filebank.exe?)");

	return res;
}

//=============================================================================
CResult	VBSParamConfig::LoadPBOFile(const std::string &pboFilename, const std::string &configFile)
{
	// make space in file bank
	ParamFile PFile;
	int index = GFileBanks.Add();
	QFBank& bank = GFileBanks[index];

	LSError result = LSOK;

	// erase extension
	wxString fullFilename = pboFilename.c_str();	
	fullFilename = fullFilename.BeforeLast('.');

	// and store file
	bool ifOpen = bank.open(fullFilename.c_str());
	bool some = PFile.ParseBin(bank,configFile.c_str());	

	// if fail.. try find config.cpp
	if (!some)
		result = PFile.Parse(bank,configFile.c_str());

	CResult res = TranslateResult(result);
	// ted mam pristup ke configu
	// ten vyexportuji a normalne zavoam
	// Load
	if (res == false)
		return res;


	// ulozim promenne
	/////////////////////////
	m_originalConfigpath = configFile;
	m_originalPBOpath    = pboFilename;
	m_pboDirectory		 = PBO_DIRECTORY;
	GetPBONameFromFile(m_pboDirectory);

	// clean up dir
	CleanUpTemp(true);

	/*
	assert(wasCleanok);
	if (!wasCleanok)
	{
	GFileBanks[index].close();

	return CResult(false,"cannot clean up temp directory");
	}
	*/

	// TEMP/'pboname'/dgffg
	CreateTempPath();

	// path to temp config
	std::string path = 
		m_tempDir + "\\" + m_pboDirectory + "\\" + CONFIG_FILE_NO_BIN;

	// delete old file
	//DeleteFile(path.c_str());
	PFile.Save(path.c_str());	
	GFileBanks[index].close();

	return Load(path.c_str(),true);
}


//=============================================================================
CResult VBSParamConfig::TranslateResult(LSError result)
{
	CResult res;

	switch(result)
	{
	case LSOK:
		res.Set(true);
		break;
	case LSFileNotFound:
		res.Set(true, "no such file");
		break;
	case LSBadFile:
		res.Set(false, "error in loaded file (CRC error...)");
		break;
	case LSStructure:
		res.Set(false, "fire structure error - caused by programm bug");
		break;
	case LSUnsupportedFormat:
		res.Set(false, "attempt to load other file format");
		break;
	case LSVersionTooNew:
		res.Set(false, "attempt to load unknown version");
		break;
	case LSVersionTooOld:
		res.Set(false, "attempt to load version that is no longer supported");
		break;
	case LSDiskFull:
		res.Set(false, "cannot save - disk full");
		break;
	case LSAccessDenied:
		res.Set(false, "file is read only");
		break;
	case LSDiskError:
		res.Set(false, "some disk error");
		break;
	case LSNoEntry:
		res.Set(false, "entry in ParamArchive not found");
		break;
	case LSNoAddOn:
		res.Set(false, "ADDED in patch 1.01 - AddOns check");
		break;
	case LSUnknownError:
		res.Set(false, "unknown error");
		break;
	default:
		assert(false);
		res.Set(false, "unknown error");
	}

	return res;
}

//===============================================================================
CResult VBSParamConfig::Load(const char* FileName, bool IsPbo/*=false*/)
{
	m_clearAll = false;

	char buff[MAX_PATH];
	sprintf(buff, "%s/%s", m_baseCfgsPath.c_str(), "config.cpp");

	EArray<PCFilename> fnames;
	fnames.Insert(PCFilename(buff));			//first to load
	fnames.Insert(PCFilename(FileName));	//load as update of first. this is the edit context because is last
	CResult res = ParamConfig::Load(fnames);

	m_isPbo = IsPbo;
	return res;
}


//===============================================================================
CResult VBSParamConfig::Save(const char* FileName)
{
	CResult res = ParamConfig::Save(FileName);
	
	if (m_isPbo)
		return SavePBOFile(FileName);

	return res;
}

//===============================================================================
VBSParamConfig::VBSParamConfig()
	:ParamConfig()
{
	m_isPbo = false;
	m_clearAll = true;
	m_forceNotCleanDir = false;
}

//===============================================================================
VBSParamConfig::~VBSParamConfig()
{
	Clear();
	// and erase temp dir
	if (!m_forceNotCleanDir)
		CleanUpTemp(false);
}

//===============================================================================
void VBSParamConfig::Clear()
{
	ParamConfig::Clear();
	
	if (m_clearAll)
	{
		m_originalConfigpath = "";
		m_originalPBOpath	 = "";
		m_pboDirectory		 = "";
		//m_tempDir			 = "";
		m_isPbo				 = false;
		m_clearAll			 = true;
	}
}