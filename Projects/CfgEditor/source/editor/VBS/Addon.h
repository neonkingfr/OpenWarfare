#pragma once
#include <vector>
#include <string>
#include <set>
#include <algorithm>
#include <cctype>      
#include <map>
#include "FileSystemOperations.h"
#include "EL/ParamFile/ParamFile.hpp"

class CAddonManager;
//#include "AddonManager.h"

//=============================================================================

/// CAddon class
class CAddon
{
public:
	typedef std::vector<std::string> TVecAddonsNames;
	typedef std::vector<CAddon*> TVecAddons;
	typedef std::set<CAddon*> TSetAddons;

	CAddon(void):m_dependecyCheck(false),m_addonManager(NULL),m_valid(false)
		,m_root(new CFileSystemOperations::SFileInfoNode("ROOT",true)),
		m_bankIndex(0){};
	
	CAddon(const ParamClass* source):
		m_dependecyCheck(false),m_addonManager(NULL),m_valid(true)
			,m_root(new CFileSystemOperations::SFileInfoNode("ROOT",true)),
			m_bankIndex(0)
	{
		LoadAddon(source);
	};

	~CAddon(void) 
	{
		CleanUp();
		m_addonManager = NULL; 
	}

	/** Add list of dependencies names(duplicity check)
		If addon name hasn't been in m_DependenciesName,
		put them into

		\param dependencies adding dependencies names list
	*/
	void AddDependenciesVec(const TVecAddonsNames &dependencies);

	/// Return list of addon's dependecies names
	const TVecAddonsNames &GetDependeciesNames() const { return m_dependenciesNames; }

	/// Returns list of addon's dependencies Objects
	const TVecAddons &GetDependecies();

	/// Returns list of addon's valid dependencies Objects
	const TVecAddons &GetValidDependecies();

	/// Returns list of addon's invlaid dependencies Objects
	const TVecAddons &GetInvalidDependecies();
	
	/// Return all dependecies recursively
	void GetAllDependecies(CAddon::TVecAddons &buffer);

	/** Get all addon's dependencies.
		And dependencies from addons with same
		filename as this addon

		\param buffer List of dependencies
	*/
	void GetAllPBODependecies(CAddon::TVecAddons &buffer);

	/// Fill valid and invalid addon map
	const TVecAddons &CreateDependecies();

	/// Return collection of addon files
	const CFileSystemOperations::TvecFiles &GetAddonFilesContainer() const
		{ return m_files; }

	/**
	 	\brief Push file into addon files

		At begining of list is main addon file with pbo
		extension

		\param fileinfo file that will be store into fileList.
	 					file is from "real" OS structure		
	*/
	void PushAddonFile(const CFileSystemOperations::SFileInfo &fileinfo) 
		{ m_files.push_back(fileinfo); }

	/** 
		\brief Push file into addon internal files list

		list is consist of all files in addon bank(pbo)

		\param fileinfo file that will be store into internal file list.
						Files can be acceses from GetFilesFromNode() function

	*/
	void PushAddonInternalFile(const CFileSystemOperations::SFileInfo &fileinfo) 
		{ m_addonInternalFiles.push_back(fileinfo); }

	/** 
		\brief Method loads addon information.
				
		From addon file bank(pbo) e.g. name, dependecies,...

		\param root the main node of bank entities structure

	*/
	bool LoadAddon(const ParamClass* root);

	/// Get addon name
	const std::string &GetName() const { return m_addonName; }

	/// Returns addon file names
	const CFileSystemOperations::TvecFiles &GetFiles() const  { return m_files; }

	/// Get addon name
	std::string GetFileName() const 
	{ 
		if (m_files.size())
			return (*(m_files.begin())).filename;
		else
			return	std::string("Unkown filename");
	}

	/// Get addon size
	unsigned int GetFileSize() const 
	{ 
		if (m_files.size())
			return (*(m_files.begin())).filesize;
		else
			return 0;
	}

	/// Set addon name
	void SetAddonName(const std::string &addonName) { m_addonName = addonName; }

	/** 
		\brief Set addon manager

		needed to load addon dependencies functions

		\param mgr CAddonManager object with manager
	*/
	void SetAddonManager(CAddonManager* mgr) { m_addonManager = mgr; }

	/// Return addon manager.(NULL if not set)
	CAddonManager* GetAddonManager() { return m_addonManager; }

	/** 
		\brief Return valid flag

		returns true if addon was created correctly
		by CAddonManager and it has valid main file
	*/
	bool IsValid() const { return  m_valid; }

	/// Set path to addon
	void SetPath(const std::string &path) { m_path = path; }

	/// Get path to addon
	const std::string &GetPath() { return m_path; }

	/// Set valid flag operations
	void SetValid() { m_valid = true; }

	/** 
		\brief Set invalid flag.

		That means addon was created as empty
		object
	*/
	void SetInvalid() { m_valid = false; }

	/** Return string in lower case.
	
		\param what Input string
	*/
	static std::string ToLower(const std::string &what)
	{
		std::string buf = what;

		// change entire string's chars to lower case
		std::transform(buf.begin(), buf.end(), buf.begin(),
			(int(*)(int)) std::tolower);

		return buf;
	}	

	/** 
		\brief Return if addon is in addons list.
		
		Function is based on std::find function from STL.
		Returns boolean value instead object iterator
	*/
	static bool IsIn(const TVecAddons &whereList, const CAddon* what)
	{
		return find(whereList.begin(),whereList.end(),what) != whereList.end();
	}

	/** 
		\brief Return if addon name is in addons name list.

		Function is based on std::find function from STL.
		Returns boolean value instead object iterator
	*/
	static bool IsIn(const TVecAddonsNames &whereList, const std::string &what)
	{
		return find_if(whereList.begin(),whereList.end(),
			CInsesitiveStringComparator(what)) != whereList.end();
	}

	/// Create directories map
	void MakeDirectoriesMap();

	/// Return root node of internal filesystem structure
	const CFileSystemOperations::SFileInfoNode* GetRootNode() const { return m_root; }

	/**
		\brief Return files from file structure node.
		
		Function recieves full path to directory from node and
		try to find same key as fullPath in directory map.
		If key exists, returns list of files int that directory

		\param node file structure node
	*/
	const CFileSystemOperations::TvecFiles &GetFilesFromNode
		(const CFileSystemOperations::SFileInfoNode* node);

	/** Store QFBank index
	
		\param index QFBank index
	*/
	void SetBankIndex(int index) { m_bankIndex = index; }
	
	/// Return index from QFBank.
	int GetBankIndex() { return m_bankIndex; }

	/** Inherit internal addon files.
		Internal files structure has already
		computed, so we can copy it.
			
		\param from 'parent' CAddon
	*/
	void InheritInternalFiles(const CAddon* from);

private:
	/// QFBank index
	int m_bankIndex;

	/// Addon manager container
	CAddonManager* m_addonManager;
	
	/// Dependency vector(down direction)
	TVecAddonsNames m_dependenciesNames;

	/// List of all dependencies (valid/invalid)
	TVecAddons m_dependencies;

	/// List of all valid dependencies
	TVecAddons m_validDependencies;

	/// List of all invalid dependencies
	TVecAddons m_invalidDependencies;

	/// Addon file list
	CFileSystemOperations::TvecFiles m_files;

	/// Addon internal file list(files in addon package)
	CFileSystemOperations::TvecFiles m_addonInternalFiles;
	
	/// Directory tree map
	CFileSystemOperations::TmapDirectory m_directoryMap;

	/// Directory structure
	CFileSystemOperations::SFileInfoNode* m_root;

	/// Dependecy check flag
	bool m_dependecyCheck;

	/// Name of addon loaded from PFile
	std::string m_addonName;	

	/// Full path to addon
	std::string m_path;

	/// Flag if addon is valid
	bool m_valid;

	/** 
		\brief Load module name from PFile

		\param source PFile entry that contains 
				addon name

		\param dependenciesToo if true function loads 
				addon dependecies within addon name too
	*/
	const std::string &LoadAddonName
		(const ParamClass* source, bool dependenciesToo=false);

	/** Load addon dependencies

		load Module dependencies from PFile array and 
		check their validity by CAddonManager

		\param entr PFile entry that stores dependencies
				array
	*/
	void LoadDependecies(ConstParamEntryPtr entr);

	/** 
		\brief Load addon dependencies

		load Module dependencies from PFile array and 
		check their validity by CAddonManager

		\param source PFile entry that stores dependencies
				array
	*/
	void LoadDependecies(const ParamClass* source);
	
	/** 
		\brief Return PFile entry with dependcies list

		If exist...

		\param source PFile entry with addon header
	*/
	ConstParamEntryPtr GetDependeciesArray(const ParamClass* source) const;

	/** 
		\brief Return all dependecies recursively

		\param dependencyList reference to dependency
				list which will be filled with dependencies
	*/
	void MakeAllDependecies(TVecAddons &dependencyList);

	/// Build directories lists for directory struct
	void BuildDirectoriesNodes();

	/** 
		\brief Force insert node and it's children

		Create tree from list of directory and link
		nodes children

		\param dirs directory structure
	*/
	void MakeDirectoryNode
		(const CFileSystemOperations::TvecDirectories &dirs);

	/// Remove fileInfoNodes from memory
	void CleanUp();

	/// CInsesitiveStringComparator helper class
	class CInsesitiveStringComparator
	{
	public:
		CInsesitiveStringComparator(std::string elem):
		  elem(elem){}

		/** Overloaded operator() for compare two string.
			
			\param what Comparabling string elem
		*/
		bool operator() (std::string what)
		{
			return ToLower(elem).compare(ToLower(what)) == 0;
		}
		
	private:
		/// string elem given from std::algorithm
		std::string elem;
	};
};
