#pragma once
#include "base/MainFrame.h"

enum EVBSConfigEditorGuiIDs
{
	MENU_CHANGE_TEMP_DIR = 1000,
	MENU_CHANGE_DATA_DIR,
	MENU_CHANGE_BIN_DIR
};

//=============================================================================
class VBSMainFrame : public MainFrame
{
public:
	//virtual void UpdateTitle();

protected:
	virtual wxMenuBar* CreateMenuBar();
	virtual wxMenu*    CreateFileMenu();
	virtual wxMenu*	   CreateToolsMenu();
	virtual wxToolBar* CreateMainToolBar();
	virtual OptionsDialogPanel* CreateOptionsPanel(OptionsDialog* parent);
	/// tool item in main menu
	wxMenu*	ToolsMenu;

private:
	int GetIndexOfPane(wxString& PaneCaption);
	void OnClose(wxCloseEvent& event);
	void OnPaneClose(wxAuiManagerEvent& event);
	void OnSelectFromMenu(wxCommandEvent& event);
	void OnIdle(wxIdleEvent& event);
	DECLARE_EVENT_TABLE();
};