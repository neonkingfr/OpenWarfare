#pragma once
#include <vector>
#include "VBSConfigEditor.h"
#include "Addon.h"

/// type definitions
//////////////////////////////////
typedef std::vector<VBSConfigEditor::STreeNodeData*> TDataList;

//=============================================================================
class CGarbageCollector
{
public:
	/// list with trash data
	TDataList m_trash;

	~CGarbageCollector() { m_instance=NULL; }

	/// Store handle to data in trash list
	void PushData(VBSConfigEditor::STreeNodeData* data) { m_trash.push_back(data); }

	/// Remove handled data from trash and memory.
	void CleanUpTrash() 
	{ 
		for (TDataList::iterator it=m_trash.begin();it!=m_trash.end();++it)
			delete *it;

		m_trash.clear(); 
	}

	/// Get singleton instance of class
	static CGarbageCollector* GetSingleton() 
	{ 
		if (!m_instance)
			m_instance = new CGarbageCollector();

		return m_instance;
	}

private:	
	/// handle to class instance
	static CGarbageCollector* m_instance;

	CGarbageCollector(){}	
};

