// SSMan.cpp : Defines the entry point for the console application.
//
/*XXX*/
#include "stdafx.h"
#include <shlwapi.h>

class BSTRRef
{
  BSTR _ref;
public:
  BSTRRef(BSTR ref) {_ref = ref;}
  ~BSTRRef() {SysFreeString(_ref);}
};

class ComRef
{
  IUnknown *_ref;
public:
  ComRef(IUnknown *ref) {_ref = ref;}
  ~ComRef() {_ref->Release();}
};

class FolderItems
{
private:
  BSTR *_actual;
  int _actualCount;
  BSTR *_deleted;
  int _deletedCount;
  void DeleteDeleted(int index)
  {
    SysFreeString(_deleted[index]);
    for (int i = index; i < _deletedCount - 1; i++)
    {
      _deleted[i] = _deleted[i + 1];
    }
    _deletedCount--;
  }
public:
  FolderItems(int itemsCount)
  {
    _actual = new BSTR[itemsCount];
    _actualCount = 0;
    _deleted = new BSTR[itemsCount];
    _deletedCount = 0;
  }
  ~FolderItems()
  {
    for (int i = 0; i < _actualCount; i++) SysFreeString(_actual[i]);
    delete [] _actual;
    for (int i = 0; i < _deletedCount; i++) SysFreeString(_deleted[i]);
    delete [] _deleted;
  }
  int ActualCount() const {return _actualCount;}
  BSTR Actual(int index) const {return _actual[index];}
  int DeletedCount() const {return _deletedCount;}
  BSTR Deleted(int index) const {return _deleted[index];}
  void AddItem(BSTR &name, bool deleted)
  {
    if (deleted)
    {
      _deleted[_deletedCount++] = name;
    }
    else
    {
      _actual[_actualCount++] = name;
    }
  }
  void RemoveRecoveredFromDeleted()
  {
    int i = 0;
    while (i < _deletedCount)
    {
      bool found = false;
      for (int j = 0; j < _actualCount; j++)
      {
        if (wcsicmp(_deleted[i], _actual[j]) == 0)
        {
          found = true;
          break;
        }
      }
      if (found)
      {
        DeleteDeleted(i);
      }
      else
      {
        i++;
      }
    }
  }
};

bool DeleteFolderW(BSTR &folderName)
{
  // Empty the directory
  WCHAR fileName[1024];
  wsprintfW(fileName, L"%s\\*.*", folderName);
  WIN32_FIND_DATAW data;
  HANDLE handle;
  if ((handle = FindFirstFileW(fileName, &data)) != INVALID_HANDLE_VALUE)
  {
    do
    {
      if (StrCmpW(data.cFileName, L".") == 0) continue;
      if (StrCmpW(data.cFileName, L"..") == 0) continue;
      WCHAR subFileName[1024];
      wsprintfW(subFileName, L"%s\\%s", folderName, data.cFileName);
      BSTR subFileNameB = SysAllocString(subFileName);
      if (!DeleteFolderW(subFileNameB))
      {
        // Try to delete it as a file
        SetFileAttributesW(subFileNameB, FILE_ATTRIBUTE_NORMAL);
        DeleteFileW(subFileNameB);
      }
      SysFreeString(subFileNameB);
    } while (FindNextFileW(handle, &data));
    FindClose(handle);
  }

  // Remove the directory
  if (RemoveDirectoryW(folderName))
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool GetItemRecursive(IVSSItem* pVSS_Item, _TCHAR* ignoredFolderNames[], int ignoredFolderNamesCount, int depth, int depthLimit, bool deleteDeleted)
{
  // Get the item type
  int itemType;
  if (S_OK != pVSS_Item->get_Type(&itemType))
  {
    printf("Error: get_Type failed\n");
    return false;
  }

  // Decide upon the item type what to do
  if (itemType == VSSITEM_PROJECT)
  {
    // Finish in case we've reached the depth limit
    if ((depthLimit >= 0) && (depth >= depthLimit)) return true;

    // Get the children
    IVSSItems *pVSS_Items;
    if (S_OK != pVSS_Item->get_Items(TRUE, &pVSS_Items))
    {
      printf("Error: get_Items failed\n");
      return false;
    }
    ComRef pVSS_ItemsRef(pVSS_Items);

    // Get the number of children
    long itemsCount;
    if (S_OK != pVSS_Items->get_Count(&itemsCount))
    {
      printf("Error: get_Count failed\n");
      return false;
    }

    // Create folder items list
    FolderItems fi(itemsCount);

    // Traverse the children and add them to the folder items and process the not-deleted ones
    for (int i = 0; i < itemsCount; i++)
    {
      // Get child
      IVSSItem *pVSS_ChildItem;
      if (S_OK != pVSS_Items->get_Item(_variant_t(i+1L), &pVSS_ChildItem))
      {
        printf("Error: get_Item failed\n");
        return false;
      }
      ComRef pVSS_ChildItemRef(pVSS_ChildItem);

      // Get the deleted flag
      VARIANT_BOOL isDeleted;
      if (S_OK != pVSS_ChildItem->get_Deleted(&isDeleted))
      {
        printf("Error: get_Deleted failed\n");
        return false;
      }

      // Get child name
      BSTR childItemName;
      if (S_OK != pVSS_ChildItem->get_Name(&childItemName))
      {
        printf("Error: get_Name failed\n");
        return false;
      }
      BSTRRef childItemNameRef(childItemName);

      // Try to find the item in the ignore list
      bool found = false;
      for (int j = 0; j < ignoredFolderNamesCount; j++)
      {
        if (wcsicmp(ignoredFolderNames[j], childItemName) == 0)
        {
          found = true;
          break;
        }
      }

      // Remember the item it in the folder items
      if (!found)
      {
        // Get the local name
        BSTR localSpec;
        if (S_OK != pVSS_ChildItem->get_LocalSpec(&localSpec))
        {
          printf("Error: get_LocalSpec failed\n");
          return false;
        }
        //BSTRRef localSpecRef(localSpec); // Must not be called (FolderItems will be released automatically)

        if (isDeleted)
        {
          fi.AddItem(localSpec, true);
        }
        else
        {
          fi.AddItem(localSpec, false);
          if (!GetItemRecursive(pVSS_ChildItem, ignoredFolderNames, ignoredFolderNamesCount, depth + 1, depthLimit, deleteDeleted))
          {
            return false;
          }
        }
      }
    }

    // Process deleted items (delete them)
    if (deleteDeleted)
    {
      // Remove those deleted items whose name is corresponding to one of the actual items
      fi.RemoveRecoveredFromDeleted();

      // Delete the items
      for (int i = 0; i < fi.DeletedCount(); i++)
      {
        BSTR name = fi.Deleted(i);
        SetFileAttributesW(name, FILE_ATTRIBUTE_NORMAL);

        // Try to delete it as a file and if not succeeded, then delete it as a folder
        if (DeleteFileW(name))
        {
          wprintf(L"File %s deleted\n", name);
        }
        else
        {
          if (DeleteFolderW(name))
          {
            wprintf(L"Folder %s deleted\n", name);
          }
        }
      }
    }
  }
  else
  {
    // Get the local name
    BSTR localSpec;
    if (S_OK != pVSS_Item->get_LocalSpec(&localSpec))
    {
      printf("Error: get_LocalSpec failed\n");
      return false;
    }
    BSTRRef localSpecRef(localSpec);

    // Check the file has changed
    VARIANT_BOOL hasChanged;
    if (S_OK == pVSS_Item->get_IsDifferent(localSpec, &hasChanged))
    {
    }
    else
    {
      // File most likely doesn't exist
      hasChanged = TRUE;
    }

    // If file has changed, then return the new version (if not checked out by me)
    if (hasChanged)
    {
      // Check the file is checked out by me
      long fileStatus;
      if (S_OK != pVSS_Item->get_IsCheckedOut(&fileStatus))
      {
        printf("Error: get_IsCheckedOut failed\n");
        return false;
      }
      if (fileStatus != VSSFILE_CHECKEDOUT_ME)
      {
        wprintf(L"%s ... ", localSpec);
        if (S_OK != pVSS_Item->Get(&localSpec, VSSFLAG_REPREPLACE | VSSFLAG_RECURSNO))
        {
          printf("Error: Get failed\n");
          return false;
        }
        printf("OK\n");
      }
    }
  }

  // Succeeded
  return true;
}


#define MANDATORY_ARGUMENTS 3


int _tmain(int argc, _TCHAR* argv[])
{

  if (argc < MANDATORY_ARGUMENTS)
  {
    printf("Error: Insufficient number of arguments");
    return 0;
  }

  // Get the user name
  WCHAR userName[1024];
  DWORD userNameSize = 1024;
  if (!GetUserNameW(userName, &userNameSize))
  {
    printf("Error: User name too long");
    return 0;
  }

  CLSID clsid;
  IClassFactory *pClf;
  IVSSDatabase *pVdb;
  //BSTR bstrPath = SysAllocString(L"\\\\new_server\\vss\\Pgm\\srcsafe.ini");
  BSTR bstrPath = SysAllocString(argv[1]);
  BSTR bstrUName = SysAllocString(userName);
  BSTR bstrUPass = SysAllocString(L"");

  // Check the recursion depth limit
  int ignoredFoldersStart = MANDATORY_ARGUMENTS;
  int depthLimit = -1;
  bool deleteDeleted = false;
  while ((ignoredFoldersStart < argc) && (argv[ignoredFoldersStart][0] == L'-'))
  {
    if (StrCmpW(argv[MANDATORY_ARGUMENTS], L"-dr") == 0)
    {
      depthLimit = 1;
    }
    else if (StrCmpW(argv[MANDATORY_ARGUMENTS], L"-dd") == 0)
    {
      deleteDeleted = true;
    }
    ignoredFoldersStart++;
  }

  CoInitialize(0);
  if (S_OK == CLSIDFromProgID(L"SourceSafe", &clsid))
  {
    if (S_OK == CoGetClassObject(clsid, CLSCTX_ALL, NULL, IID_IClassFactory, (void**)&pClf))
    {
      if (S_OK == pClf->CreateInstance(NULL, IID_IVSSDatabase, (void **) &pVdb))
      {
        if (S_OK == pVdb->Open(bstrPath, bstrUName, bstrUPass))
        {
          //Database Successfully Opened!
          //Add code here to use the open database.

          BSTR bstrBaseSourcesafeProject = SysAllocString(argv[2]);
          IVSSItem *pVSS_Item;
          if (S_OK == pVdb->get_VSSItem(bstrBaseSourcesafeProject, FALSE, &pVSS_Item))
          {
            GetItemRecursive(pVSS_Item, &argv[ignoredFoldersStart], argc - ignoredFoldersStart, 0, depthLimit, deleteDeleted);
            pVSS_Item->Release();
          }
          else
          {
            printf("Error: get_VSSItem failed\n");
          }
          SysFreeString(bstrBaseSourcesafeProject);
        }
        else
        {
          printf("Error: Open failed\n");
        }
        pVdb->Release();
      }
      else
      {
        printf("Error: CreateInstance failed\n");
      }
      pClf->Release();
    }
    else
    {
      printf("Error: CoGetClassObject failed\n");
    }
  }
  else
  {
    printf("Error: CLSIDFromProgID failed\n");
  }
  CoUninitialize();

  SysFreeString(bstrPath);
  SysFreeString(bstrUName);
  SysFreeString(bstrUPass);

	return 0;
}

