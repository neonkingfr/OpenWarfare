#define STRINGIFY_X(x)            #x
#define STRINGIFY(x)              STRINGIFY_X(x)

#define GET_VER_X(a, b, c, d)     a.b.c.d
#define GET_VER(a, b, c, d)       GET_VER_X(a, b, c, d)

#define GET_VERSION_X(a, b, c, d) Version: a.b.c.d
#define GET_VERSION(a, b, c, d)   GET_VERSION_X(a, b, c, d)

#include "Projects/appInfoRev.h" // for APP_BREV and eventual APP_BREVHI and APP_BREVLO
#ifndef APP_BREVHI
#define APP_BREVHI      0
#define APP_BREVLO      APP_BREV
#endif

#define BUILD_NO        APP_BREV // compatibility with older products
#define APP_BREVSTR     STRINGIFY(APP_BREV)
#define APP_BDATE       __DATE__
#define APP_BTIME       __TIME__
#ifndef APP_SUB
#define APP_SUB         0
#endif
#define APP_VER         STRINGIFY(GET_VER(APP_MAJOR, APP_MINOR, APP_SUB, APP_BREV))
#define APP_VERSION     STRINGIFY(GET_VERSION(APP_MAJOR, APP_MINOR, APP_SUB, APP_BREV))
#ifndef APP_ADDITION
#ifdef _DEBUG
#define APP_ADDITION    " (Dev)"
#else
#define APP_ADDITION    ""
#endif
#endif
#ifndef APP_FULLNAME
#define APP_FULLNAME    APP_NAME APP_ADDITION " " APP_VER
#endif
#ifndef COMPANY
#define COMPANY         "Bohemia Interactive"
#endif
#define COPYRIGHT       "Copyright " STRINGIFY(APP_CREATED) "-2010 " COMPANY ". All rights reserved."
#define COPYRIGHT2      "All rights reserved."

#define APPVER          APP_MAJOR, APP_MINOR, APP_BREVHI, APP_BREVLO
#define APPVERSTR       STRINGIFY(APPVER)
