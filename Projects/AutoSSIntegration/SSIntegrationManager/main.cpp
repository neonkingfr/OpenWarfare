#include "stdafx.h"
#include "DlgMain.h"
#include "resource.h"
#include <CommCtrl.h>

type_InitCommands InitCommands;
type_EnforceReInject EnforceReInject;
type_EnforceRestore EnforceRestore;

static HINSTANCE hInst;

CStringRes StrRes;

HINSTANCE AfxGetInstanceHandle() {return hInst;}



INT WINAPI WinMain( HINSTANCE hInstance, HINSTANCE, LPSTR, INT )
{
  hInst=hInstance;
  StrRes.SetInstance(hInstance);

  HMODULE mod=LoadLibrary(_T("HookDll.Dll"));
  if (mod==0) return -1;
  
  InitCommands=(type_InitCommands)GetProcAddress(mod,"InitCommands");
  EnforceReInject=(type_EnforceReInject)GetProcAddress(mod,"EnforceReInject");
  EnforceRestore=(type_EnforceRestore)GetProcAddress(mod,"EnforceRestore");
  
  InitCommands();
  EnforceReInject();


  HOOKPROC proc=(HOOKPROC)GetProcAddress(mod,"_HookCallWndProc@12");
  HHOOK hk=SetWindowsHookEx(WH_CALLWNDPROC,proc,mod,0);//,GetCurrentThreadId());
  if (hk==0)
  {
    MessageBox(0,_T("Failed to install hook"),_T("HOOK"),MB_OK);
  }
  else
  {
    InitCommonControls();
    DlgMain dlg;
    dlg.Create(IDD_MAINDLG,0);
    MSG msg;
    while (GetMessage(&msg,0,0,0)) if (!IsDialogMessage(dlg,&msg))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);

    }

    UnhookWindowsHookEx(hk);
    int err=GetLastError();
  }
  return 0;
}
