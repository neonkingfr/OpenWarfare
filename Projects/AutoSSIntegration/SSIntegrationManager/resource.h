//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SSIntegrationManager.rc
//
#define IDD_MAINDLG                     101
#define IDS_LISTNAME                    103
#define IDS_SYSTRAYTIP                  104
#define IDI_MAINICON                    105
#define IDS_BALOONTEXT                  105
#define IDS_BALOONTITLE                 106
#define IDC_LIST                        1003
#define IDC_ATTACH                      1004
#define IDC_DETACH                      1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
