#define _WIN32_IE 0x0500
#include <tchar.h>
#include <windows.h>
#include <WindowsX.h>
#include <CommCtrl.h>
#include <Tlhelp32.h>
#include <shellapi.h>
#include "StringRes.h"
#include <el/pathname/pathname.h>

HINSTANCE AfxGetInstanceHandle();
extern CStringRes StrRes;

typedef void (*type_InitCommands)();
typedef void (*type_EnforceReInject)();
typedef void (*type_EnforceRestore)();

extern type_InitCommands InitCommands;
extern type_EnforceReInject EnforceReInject;
extern type_EnforceRestore EnforceRestore;
