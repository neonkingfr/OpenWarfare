#include "stdafx.h"
#include ".\dlgmain.h"
#include "resource.h"

#define WM_NOTIFYICON (WM_APP+1000)

DlgMain::DlgMain(void)
{
}

DlgMain::~DlgMain(void)
{
}


Pathname GetApplicationModuleName(DWORD id)
{
  HANDLE h=CreateToolhelp32Snapshot(TH32CS_SNAPMODULE,id);
  if (h==0) return Pathname(PathNull);
  MODULEENTRY32 entry;
  entry.dwSize=sizeof(entry);
  if (!Module32First(h,&entry)) return Pathname(PathNull);
  CloseHandle(h);
  return Pathname(entry.szExePath); 
}

bool IsIntegrationEnabled(Pathname exePath)
{
  if (exePath.IsNull()) return false;
  exePath.SetExtension(_T(".ssint.ini"));
  return exePath.TestFile();
}

static Pathname myPathName;

static BOOL WINAPI EnumAppWindows(HWND hWnd, LPARAM lParam)
{
  HWND list=(HWND)lParam;
  HWND own=GetWindow(hWnd,GW_OWNER);
  if (own==0 && IsWindowVisible(hWnd))
  {
    _TCHAR chr[256];
    GetWindowText(hWnd,chr,sizeof(chr)/sizeof(_TCHAR));        
    if (chr[0])
    {
      DWORD id;
      GetWindowThreadProcessId(hWnd,&id);
      Pathname exePath=GetApplicationModuleName(id);      
      if (_tcsicmp(exePath.GetDirectoryWithDrive(),myPathName.GetDirectoryWithDrive()))
      {      
        LVITEM itm;
        itm.mask=LVIF_TEXT|LVIF_PARAM;
        itm.pszText=chr;
        itm.iItem=0;
        itm.iSubItem=0;
        itm.lParam=id;      
        int p=ListView_InsertItem(list,&itm);    
        ListView_SetCheckState(list,p,IsIntegrationEnabled(exePath));
      }
    }    
  }
  return TRUE;
}

void DlgMain::ReadAppList()
{
  myPathName=myPathName.GetExePath(0);
  _skipNotify=true;
  ListView_DeleteAllItems(_list);
  EnumWindows(EnumAppWindows,(LPARAM)_list);
  ListView_SetColumnWidth(_list,0,-1);
  _skipNotify=false;
}


LRESULT DlgMain::OnInitDialog()
{
  _list=GetDlgItem(IDC_LIST);
  ListView_SetExtendedListViewStyleEx(_list,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
  
  LVCOLUMN lvcol;
  lvcol.mask=LVCF_FMT|LVCF_SUBITEM|LVCF_TEXT|LVCF_WIDTH;
  lvcol.fmt=LVCFMT_LEFT;
  lvcol.iSubItem=0;
  lvcol.pszText=const_cast<_TCHAR *>(StrRes[IDS_LISTNAME]);
  lvcol.cx=-1;
  
  
  ListView_InsertColumn(_list,0,&lvcol);
  ReadAppList();
  SetTimer(*this,1,5000,0);
  SetWindowPos(*this,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  MinimizeTray();
  return 1;
}

LRESULT DlgMain::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch (msg)
  {
  case WM_TIMER: if (IsWindowVisible(*this) && !IsIconic(*this)) ReadAppList();return 1;
  case WM_NOTIFYICON: if (lParam==WM_LBUTTONDBLCLK)
                      {
                        RestoreTray();
                      }
                      return 1;
  case WM_SIZE: if (wParam==SIZE_MINIMIZED) MinimizeTray();
                else ReadAppList();
                break;
  }
  return CDialogClass::DlgProc(msg,wParam,lParam);
}

LRESULT DlgMain::OnNotify(int id, NMHDR *ntf)
{
  if (id==IDC_LIST && ntf->code==LVN_ITEMCHANGED && !_skipNotify)
  {
    NMLISTVIEW *itm=reinterpret_cast<NMLISTVIEW *>(ntf);
    if (itm->uChanged & LVIF_STATE)
    {
      if ((itm->uNewState ^ itm->uOldState) & 0x2000)
      {
        if (ListView_GetCheckState(_list,itm->iItem))
          IntegrateSS(itm->lParam);
        else
          DisableIntegrationSS(itm->lParam);     
      }
    }
  }
  return 1;
}

void DlgMain::DisableIntegrationSS(DWORD id)
{
  Pathname cfg=GetApplicationModuleName(id);
  Pathname offcfg=cfg;
  if (cfg.IsNull()) return;
  cfg.SetExtension(_T(".ssint.ini"));
  offcfg.SetExtension(_T(".ssint.off"));
  WIN32_FIND_DATA dta;
  HANDLE h=FindFirstFile(cfg,&dta);
  if (h==INVALID_HANDLE_VALUE) return;
  FindClose(h);
  if (dta.nFileSizeLow==0 && dta.nFileSizeHigh==0)
  {
    DeleteFile(cfg);
    EnforceRestore();
    return;
  }
  MoveFile(cfg,offcfg);
  EnforceRestore();
}

void DlgMain::IntegrateSS(DWORD id)
{
  Pathname cfg=GetApplicationModuleName(id);
  Pathname offcfg=cfg;
  if (cfg.IsNull()) return;
  cfg.SetExtension(_T(".ssint.ini"));
  offcfg.SetExtension(_T(".ssint.off"));
  if (offcfg.TestFile())
  {
    MoveFile(offcfg,cfg);
  }
  else
  {
    HANDLE h=CreateFile(cfg,GENERIC_WRITE,0,0,CREATE_ALWAYS,0,0);
    CloseHandle(h);
  }
  EnforceReInject();
}

void DlgMain::MinimizeTray()
{
  CloseWindow(*this);
  ZeroMemory(&nif,sizeof(nif));
  nif.cbSize=sizeof(nif);
  nif.hWnd=*this;
  nif.uID=1;
  nif.uFlags=NIF_ICON|NIF_MESSAGE|NIF_INFO;
  nif.uCallbackMessage=WM_NOTIFYICON;
  nif.hIcon=(HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_MAINICON),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
  _tcscpy(nif.szTip,StrRes[IDS_SYSTRAYTIP]);
  _tcscpy(nif.szInfo,StrRes[IDS_BALOONTEXT]);
  _tcscpy(nif.szInfoTitle,StrRes[IDS_BALOONTITLE]);
  nif.uTimeout=15000;
  nif.dwInfoFlags=NIIF_INFO;
  ::Shell_NotifyIcon(NIM_ADD,&nif);
  ShowWindow(*this,SW_HIDE);
}

void DlgMain::RestoreTray()
{
  nif.szTip[0]=0;
  nif.szInfo[0]=0;
  nif.szInfoTitle[0]=0;
  ::Shell_NotifyIcon(NIM_DELETE,&nif);
  DestroyIcon(nif.hIcon);
  ShowWindow(*this,SW_SHOW);
  OpenIcon(*this);
}

LRESULT DlgMain::OnOK()
{ 
  MinimizeTray();
  return 1;
}

LRESULT DlgMain::OnCancel()
{
  PostQuitMessage(0);
  return 1;
}