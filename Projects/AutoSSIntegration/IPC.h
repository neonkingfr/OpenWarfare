
///Bounds app to the client
/**
 * @param wParam Not used
 * @param lParam Process ID
 * @return config see CFG_XXX constants
 */
#define CNM_SETSERVERID (WM_APP+1)
///Notifies client about opened file
/**
 *  @param wParam Not used
 *  @param lParam Handle to the shared memory that contains MsgPackage structure
 *  @retval true permition to open specified file
 *  @retval false send Access Denied to the server application
 */
#define CNM_FILEACTIONREPORT (WM_APP+2)

///Notifies client, that HookDll is being unloaded
/**
 * No parameters, message is posted, so you don't need to reply on it. 
 */
#define CNM_EXIT (WM_APP+3)

#define CNM_ACTIVEWINDOWCHANGED (WM_APP+4)


struct MsgPackage
{
  DWORD dwDesiredAccess;    //dwDesiredAccess from CreateFile
  DWORD dwCreationDisposition; //dwCreationDisposition from CreateFile
  wchar_t name[2030];
};

///Config constanst
#define CFG_READY 1 ///< reports to hook, that client application is active and ready
#define CFG_NOCOMDLG 2 ///< don't report files, if app is any ComDlg32 dialog.



#define MAINWNDCLASS  _T("AutoSSCntrlClass")
