#pragma once

#include "XMLCommon.h"
#include "XMLParser.h"

namespace XMLInOutHelp
{
  int TextUTF8ToUnicode(const char *text, wchar_t *outbuf, int bufSz);
  int TextUnicodeToUTF8(const wchar_t *text, char *outbuf, int bufSz);
}


template<class Stream, bool autoDelete=false>
class XMLIn: protected XMLParser<Stream>
{
public:
  enum Error
  {
    noError=0,
    errUncomplette,
    errEOF,
    errInvalidNumFormat,
    errMemAllocFailed,
    errInvalidBoolFormat
  };
protected:
  Error _err;
public:

  XMLIn(Stream *stream):XMLParser<Stream>(stream,autoDelete) {_err=noError;}

  ~XMLIn(void)
  {
    if (autoDelete) delete _stream;
  }

  bool OpenSection(const char *name)
  {
    if (_err!=noError) return false;

    if (name[0]=='_') //attribute
    {
      return OpenAttribute(XMLTagName(name+1,(char *)alloca(strlen(name)+1)));
    }
    else
    {
      return OpenTag(XMLTagName(name,(char *)alloca(strlen(name)+1)));
    }
  }

  void CloseSection(const char *name)
  {
    if (_err!=noError) return;
    if (name[0]=='_') CloseAttribute(XMLTagName(name+1,(char *)alloca(strlen(name)+1)));
    else CloseTag(XMLTagName(name,(char *)alloca(strlen(name)+1)));
  }

  int PeekSection(char *buffer, int bufferSize)
  {
    if (_err!=noError) return 0;
    bool res=PeekTag(buffer,bufferSize);
    if (res==false) return 0;
    return strlen(buffer);
  }

  bool IsWritting() const
  {
    return false;
  }

  bool IsError() const
  {
    return _err!=noError;
  }

  void BinExchange(void *data, int size)
  {
    if (_err!=noError) return 0;
    char *arr=reinterpret_cast<char *>(data);
    for (int i=0;i<size;i++) 
    {
      int x=GetChar();
      if (x==-1) 
      {
        _err=errUncomplette;
        return;
      }
      arr[i]=(char)x;
    }
  }

  void Exchange(char &item)
  {
    if (_err!=noError) return;
    int x=GetChar();
    if (x<0) _err=errEOF;
    item=(char)x;
  }

  void Exchange(unsigned char &item) {item=(unsigned char)ParseUnsigned();}
  void Exchange(short &item) {item=(short)ParseSigned();}
  void Exchange(unsigned short &item) {item=(unsigned short)ParseUnsigned();}
  void Exchange(long &item) {item=(long)ParseSigned();}
  void Exchange(unsigned long &item) {item=(unsigned long)ParseUnsigned();}
  void Exchange(int &item) {item=(int)ParseSigned();}
  void Exchange(unsigned int &item) {item=(unsigned int)ParseUnsigned();}
  void Exchange(long long &item) {item=ParseBigSigned();}
  void Exchange(unsigned long long &item) {item=ParseBigUnsigned();}
  void Exchange(bool &item) {item=ParseBool();}
  void Exchange(float &item) {item=(float)ParseFloat();}
  void Exchange(double &item) {item=(double)ParseFloat();}

  char *ReadText()
  {
    if (_err!=noError) return 0;
    char *buff=0;
    int pos=0;
    int sz=0;
    int x=GetChar();
    while (x>=0 && isspace(x)) x=GetChar();
    while (x>=0)
    {
      if (pos+1>=sz)
      { 
        int nwsz;
        char *nw;
        if (sz==0)
        {
          nwsz=32;
          nw=(char *)malloc(nwsz);
        }
        else 
        {        
          nwsz=sz?sz*3/2:32;
          nw=(char *)realloc(buff,nwsz);
        }
        if (nw==0)
        {
          if (pos>0) buff[pos]=0;
          else buff=_strdup(buff);
          _err=errMemAllocFailed;
          return buff;
        }
        buff=nw;
        sz=nwsz;        
      }
      buff[pos++]=(char)x;
      x=GetChar();
    }
    if (pos>0) 
    {
      while (isspace((unsigned char)(buff[pos-1]))) pos--;
      buff[pos]=0;
    }
    else buff=_strdup(buff);
    return buff;
  }

  char *ReadText(char *buff, int buffSz)
  {
    int pos=0;
    int x=GetChar();
    while (x>=0 && pos+1<buffSz)
    {
      buff[pos++]=(char)x;
      x=GetChar();
    }
    buff[pos]=0;
    return buff;
  }

  unsigned long ParseUnsigned()
  {
    if (_err!=noError) return 0;
    char buff[200];
    unsigned long val=0;
    if (sscanf(ReadText(buff,lenof(buff)),"%u",&val)!=1)
      _err=errInvalidNumFormat;
    return val;
  }
 
  long ParseSigned()
  {
    if (_err!=noError) return 0;
    char buff[200];
    long val=0;
    if (sscanf(ReadText(buff,lenof(buff)),"%d",&val)!=1)
      _err=errInvalidNumFormat;
    return val;
  }

  unsigned long long ParseBigUnsigned()
  {
    if (_err!=noError) return 0;
    char buff[200];
    unsigned long long val=0;
    if (sscanf(ReadText(buff,lenof(buff)),"%I64u",&val)!=1)
      _err=errInvalidNumFormat;
    return val;
  }

  long long ParseBigSigned()
  {
    if (_err!=noError) return 0;
    char buff[200];
    long long val=0;
    if (sscanf(ReadText(buff,lenof(buff)),"%I64d",&val)!=1)
      _err=errInvalidNumFormat;
    return val;
  }

  bool ParseBool()
  {
    if (_err!=noError) return 0;
    char buff[6];        
    ReadText(buff,lenof(buff));
    if (_stricmp(buff,"true")==0) return true;
    if (_stricmp(buff,"false")==0) return false;
    _err=errInvalidBoolFormat;
    return false;
  }

  double ParseFloat()
  {
    if (_err!=noError) return 0;
    char buff[200];
    float val=0;
    if (sscanf(ReadText(buff,lenof(buff)),"%g",&val)!=1)
      _err=errInvalidNumFormat;
    return val;
  }

  void TextExchange(char *& text)
  {
    if (_err!=noError) return;
    free(text);
    text=ReadText();   
  }


  void TextExchange(wchar_t *& wtext)
  {
    if (_err!=noError) return;
    delete wtext;
    char *text=ReadText();   
    int bufsz=XMLInOutHelp::TextUTF8ToUnicode(text,0,0);
    wtext=new wchar_t[bufsz];
    XMLInOutHelp::TextUTF8ToUnicode(text,wtext,bufsz);
    free(text);
  }

  void Ver(int &ver, const char *name)
  {
    OpenSection(name);
    Exchange(ver);
    CloseSection(name);
  }

  bool ReadHeader(char **version, char **encoding)
  {
    if (OpenTag("?xml")==false) return false;
    if (version) 
    {
      OpenAttribute("version");
      *version=ReadText();
    }
    if (encoding) 
    {
      OpenAttribute("encoding");
      *encoding=ReadText();
    }
    CloseTag("?xml");
    return true;
  }

};
