// DialogClass.cpp: implementation of the CDialogClass class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DialogClass.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDialogClass *CDialogClass::newDialog=NULL;

CDialogClass::CDialogClass()
  {
  minset=false;
  memset(&mmx,0,sizeof(mmx));
  RECT rc;
  GetClientRect(GetDesktopWindow(),&rc);
  if (rc.right>rc.bottom*2) rc.left+=rc.right/2; //(dualhead)
  mmx.ptMaxTrackSize.y=rc.bottom-rc.top;
  mmx.ptMaxTrackSize.x=rc.right-rc.left;
  mmx.ptMaxSize=mmx.ptMaxTrackSize;
  lastcx=0;
  lastcy=0;
  hWnd=NULL;
  }

//--------------------------------------------------

CDialogClass::~CDialogClass()
  {
  if (hWnd) DestroyWindow(hWnd);
  }

//--------------------------------------------------

LRESULT CALLBACK DialogProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
  {
  CDialogClass *dlg=(CDialogClass *)GetWindowLong(hWnd,DWL_USER);
  if (dlg==NULL)
    {
    if (CDialogClass::newDialog) 
      {
      CDialogClass::newDialog->Attach(hWnd);
      SetWindowLong(hWnd,DWL_USER,(LONG)CDialogClass::newDialog);
      dlg=CDialogClass::newDialog;
      CDialogClass::newDialog=NULL;	  
      }
    else return 0;
    }
  if (dlg->hWnd!=hWnd) return 0;
  return dlg->DlgProc(msg,wParam,lParam);
  }

//--------------------------------------------------

int CDialogClass::DoModal(int idc,HWND parent)
  {
  newDialog=this;
  int p=DialogBoxParam(AfxGetInstanceHandle(),MAKEINTRESOURCE(idc),parent,(DLGPROC)DialogProc,0);
  Detach();
  return p;
  }

//--------------------------------------------------

LRESULT CDialogClass::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  switch (msg)
    {
    case WM_COMMAND: return OnCommand(LOWORD(wParam),HIWORD(wParam),(HWND)lParam);
    case WM_INITDIALOG: return OnInitDialog();
    case WM_PAINT: return OnPaint((HDC)wParam);
    case WM_APP+10: return OnGatewayMessage(wParam,lParam);
    case WM_NCDESTROY: hWnd=NULL;return 0;
    case WM_NOTIFY: return OnNotify(wParam,(NMHDR *)lParam);
    case WM_GETMINMAXINFO: 
      {
      MINMAXINFO *nfo=(MINMAXINFO *)lParam;
      if (minset) 
        {mmx.ptReserved=nfo->ptReserved;*nfo=mmx;return 1;}
      return 0;
      }
    case WM_SIZE:
      {
      LRESULT ll;
      if (lParam==0)		
        ll=OnSize(LOWORD(lParam),HIWORD(lParam),0,0,wParam);
      else
        {
        ll=OnSize(LOWORD(lParam),HIWORD(lParam),LOWORD(lParam)-lastcx,HIWORD(lParam)-lastcy,wParam);
        lastcx=LOWORD(lParam);
        lastcy=HIWORD(lParam);
        }
      return ll;
      }
    }
  return 0;
  }

//--------------------------------------------------

void CDialogClass::EndDialog(int nResult)
  {
  ::EndDialog(hWnd,nResult);
  }

//--------------------------------------------------

LRESULT CDialogClass::OnOK()
  {
  EndDialog(IDOK);
  return 1;
  }

//--------------------------------------------------

LRESULT CDialogClass::OnCancel()
  {
  EndDialog(IDCANCEL);
  return 1;
  }

//--------------------------------------------------

LRESULT CDialogClass::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  switch (wID)
    {
    case IDOK: return OnOK();
    case IDCANCEL: return OnCancel();
    }
  return 0;
  }

//--------------------------------------------------

_TCHAR *CDialogClass::GetDlgItemText(int idc)
  {
  HWND hWnd=GetDlgItem(idc);
  if (hWnd==NULL) return NULL;
  int size=GetWindowTextLength(hWnd)+1;
  _TCHAR *buff=(_TCHAR *)malloc(size*sizeof(_TCHAR));
  GetWindowText(hWnd,buff,size);
  return buff;
  }

//--------------------------------------------------

BOOL CDialogClass::Create(int idc,HWND parent)
  {
  newDialog=this;
  hWnd=CreateDialog(AfxGetInstanceHandle(),MAKEINTRESOURCE(idc),parent,(DLGPROC)DialogProc);
  return hWnd!=0;
  }

//--------------------------------------------------

LRESULT CDialogClass::OnInitDialog()
  {
  RECT rc;
  GetClientRect(hWnd,&rc);
  lastcx=rc.right;
  lastcy=rc.bottom;
  return 1;
  }

//--------------------------------------------------

void CDialogClass::SetMinMaxInfo(MINMAXINFO &nfo)
  {
  mmx=nfo;
  minset=true;
  }

//--------------------------------------------------

