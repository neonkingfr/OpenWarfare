#pragma once



namespace ArchiveTemplates
{
  template<class Archive, class T>
  void SerializeRef(T &item, Archive &arch)
  {
    item.Serialize(arch);
  }

  template<class T, class Archive>
  class PolyTypeDef
  {
    typedef PolyTypeDef This;

    This *_next;    
  public:
    static This *sList;

    PolyTypeDef()
    {
      _next=sList;
      sList=this;
    }
    ~PolyTypeDef()
    {
      if (sList==this)
      {
        sList=_next;       
      }
      else
      {
        This *k=sList;
        while (k && k->_next!=this) k=k->_next;
        if (k) k->_next=_next;
      }
      _next=0;
    }

    virtual bool Write(const T *object, Archive &arch) {return false;}
    virtual bool Read(const char *peekedSection, T **object, Archive &arch) {return false;}
    virtual int GetSectionSize() const=0;
    This *GetNext() const {return _next;}

  };


  template<class T, class Archive>
  PolyTypeDef<T,Archive> *PolyTypeDef<T,Archive> ::sList=0;


  template<class Archive, class T>
  void SerializePtr(T * &item, Archive &arch)
  {
    PolyTypeDef<T,Archive> *list=PolyTypeDef<T,Archive>::sList;

    if (+arch)
    {
      while (list)
      {
        if (list->Write(item,arch)) return;
        list=list->GetNext();
      }
      abort();  ///serializing type that is not registred
    }
    else
    {
      int ssize=list->GetSectionSize();
      char *buff=(char *)alloca(ssize+1);
      ssize=arch.PeekSection(buff,ssize);
      if (ssize>0)
      {      
        buff[ssize]=0;
        while (list)
        {
          if (list->Read(buff,&item,arch)) return;
          list=list->GetNext();
        }
        arch.OpenSection(buff);
        arch.CloseSection(buff);
      }
      item=0;
    }
  }
}

template<class Parser, bool autoDelete>
class Archive
{
protected:
  Parser *_parser;    ///<current parser that is used for serializing
  int _curVer;        ///<current archive version
  int _state;         ///<last known state (or error)  
  int _type;  ///<user value to easy identify derived class. Default is 0 - base class.


  Archive(Parser *parser, int type):_parser(parser),_type(type),_state(0) {}

public:

  typedef Parser ParserType;  
  static const bool autoDeleteParm=autoDelete;


  ///construction of archive
  /**
   * @param parse parser object used for data exchange and parsing basic types.   
   */
  Archive(Parser *parser):_parser(parser),_type(0),_state(0) {}

  ~Archive(void)
  {
    if (autoDelete && _parser) delete _parser;
  }

  template<class T>
  void operator()(T &item)
  {
    ArchiveTemplates::SerializeRef(item,*this);
  }

  template<class T>
  void operator()(T * &item)
  {
    ArchiveTemplates::SerializePtr(item,*this);
  }

  template<class T>
  bool operator()(const char *name, T &item)
  {
    if (_parser->OpenSection(name)==false) return false;
    (*this)(item);
    _parser->CloseSection(name);
    return true;
  }

  template<class T>
  T RValue(const T &item)
  {
    if (+(*this)) 
    {
      (*this)(const_cast<T &>(item));
      return item;
    }
    else
    {
      T ret=item;
      (*this)(ret);
      return ret;
    }
  }

  bool operator+() const {return _parser->IsWritting();}
  bool operator-() const {return !_parser->IsWritting();}
  bool operator!() const {return _parser->IsError() || _state<0;}

  template<class T>
  T RValue(const char *name, const T &item)
  {
    if (_parser->OpenSection(name)==false) return item;
    T res=RValue(item);
    _parser->CloseSection(name);
    return res;
  }

  template<class T>
  void operator()(const char *name, T &item, const T &defValue)
  {
    if (operator+() && item==defValue) return;
    if ((*this)(name,item)==false) 
    {
      item=defValue;
    }    
  }

  template<class T>
  T RValue(const char *name, const T &item, const T &defValue)
  {
    if (operator+() && item==defValue) return item;
    if (_parser->OpenSection(name)==false) return defValue;
    T res=RValue(item);
    _parser->CloseSection(name);
    return res;    
  }

  template<class T>
  void Bin(T *data, int size)
  {
    _parser->BinExchange(data,size);
  }

  template<class T>
  void BinVariable(T *data, int &size)
  {
    _parser->BinVarExchange(data,size);
  }

  template<class T>
  bool Bin(const char *name, T *data, int size)
  {
    if (_parser->OpenSection(name)==false) return false;
    _parser->BinExchange(data,size);
    _parser->CloseSection(name);
    return true;
  }

  template<class T>
  bool BinVariable(const char *name, T *data, int &size)
  {
    if (_parser->OpenSection(name)==false) return false;
    _parser->BinVarExchange(data,size);
    _parser->CloseSection(name);
    return true;
  }

  int PeekSection(char *buffer, int bufferSize)
  {
    return _parser->PeekSection(buffer,bufferSize);
  }

  bool OpenSection(const char *name)
  {
    return _parser->OpenSection(name);
  }

  void CloseSection(const char *name)
  {
    _parser->CloseSection(name);
  }


  template<> void operator()(char &item)  { return _parser->Exchange(item); }
  template<> void operator()(unsigned char &item)  { return _parser->Exchange(item); }
  template<> void operator()(short &item)  { return _parser->Exchange(item); }
  template<> void operator()(unsigned short &item)  { return _parser->Exchange(item); }
  template<> void operator()(long &item)  { return _parser->Exchange(item); }
  template<> void operator()(unsigned long &item)  { return _parser->Exchange(item); }
  template<> void operator()(long long &item)  { return _parser->Exchange(item); }
  template<> void operator()(unsigned long long &item)  { return _parser->Exchange(item); }
  template<> void operator()(int &item)  { return _parser->Exchange(item); }
  template<> void operator()(unsigned int &item)  { return _parser->Exchange(item); }
  template<> void operator()(float &item)  { return _parser->Exchange(item); }
  template<> void operator()(double &item)  { return _parser->Exchange(item); }
  template<> void operator()(bool &item)  { return _parser->Exchange(item); }
  template<> void operator()(char * &item)  { return _parser->TextExchange(item); }  
  template<> void operator()(wchar_t * &item)  { return _parser->TextExchange(item); }    

  bool Ver(int reqVer, const char *name="_version")
  {
    _curVer=reqVer;
    _parser->Ver(_curVer,name);
    return (_curVer<=reqVer);
  }

  bool IsVer(int reqVer)
  {
    return (_curVer>=reqVer);
  }

  void SetState(int state)
  {
    if (_state<0) return;
    _state=state;
  }
  int GetState() const
  {
    return _state;
  }

  void CleanState()
  {
    _state=0;
  }

  int ArchiveType() const {return _type;}

};

template<class Archive>
class ArchiveSection
{
  const char *name;
  Archive &arch;
public:
  ArchiveSection(Archive &arch):name(0),arch(arch) {}
  ~ArchiveSection()
  {
    if (name) arch.CloseSection(name);
  }
  bool operator()(const char *name)
  {
    if (this->name==name) 
    {
      arch.CloseSection(name);
      this->name=0;
      return false;
    }
    else
    {
      if (this->name!=0) arch.CloseSection(this->name);
      if (arch.OpenSection(name))
      {
        this->name=name;
        return true;
      }
      else
      {
        this->name=0;
        return false;
      }
    }
  }
};


template<class Parser, bool autoDelete>
class NamedArchive: public Archive<Parser,autoDelete>
{
  Pathname _name;
  static const int id=1;
public:

  static NamedArchive<Parser,autoDelete> *
              GetInstance(Archive<Parser,autoDelete> &arch)
  {
    if (arch.ArchiveType()==id)
      return static_cast<NamedArchive<Parser,autoDelete> *>(&arch);
    else
      return 0;
  }

  NamedArchive(Parser *parser, const Pathname &name):Archive<Parser,autoDelete>(parser,id),_name(name) {}
  
  const Pathname &GetName() const {return _name;}  

};
