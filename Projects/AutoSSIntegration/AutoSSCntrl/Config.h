#pragma once

static inline _TCHAR *DupStr(const _TCHAR *str)
{
  if(str==0) return 0;
  _TCHAR *out=new _TCHAR[_tcslen(str)+1];
  _tcscpy(out,str);
  return out;
}

class SimpleStr: public SRef<_TCHAR,SRefArrayTraits<_TCHAR> >
{
public:
  SimpleStr( _TCHAR *source ): SRef<_TCHAR,SRefArrayTraits<_TCHAR> >(source) {}
  SimpleStr(const  _TCHAR *source ): SRef<_TCHAR,SRefArrayTraits<_TCHAR> >(DupStr(source)) {}
  SimpleStr( ) {}
  ClassIsMovable(SimpleStr);
};


_TCHAR *DupStrMB(const char *str);
char *DupStrMB(const _TCHAR *str);

class Config
{
public:
  SimpleStr SSprovidersDll;
  SimpleStr SSserver;
  SimpleStr SSlocalpath;
  SimpleStr SSusername;
  SimpleStr SSproject;  

  bool askOnFirstAccess;
  bool detectComDlgOpen;

  AutoArray<SimpleStr> alwaysSSPaths;
  AutoArray<SimpleStr> neverSSPaths;
 
 
  int savedExpiration;


  Pathname configName;
  RECT winRect;

  

  template<class Archive>
  void Serialize(Archive &arch)
  {
    arch("_askss",askOnFirstAccess);
    arch("_provider",SSprovidersDll);
    arch("_ssserver",SSserver);
    arch("_sslocalpath",SSlocalpath);
    arch("_ssusername",SSusername);
    arch("_ssproject",SSproject);
    arch("_nocomdlg",detectComDlgOpen);

    arch("alwaysSSPaths",alwaysSSPaths);
    arch("neverSSPaths",neverSSPaths);

    arch("savedExpiration",savedExpiration);
    arch("winRect",winRect);    
  }
    

public:
  ~Config(void);
  Config(void);

  void LoadConfig(const Pathname &name);
  bool SaveConfig() const;
};
