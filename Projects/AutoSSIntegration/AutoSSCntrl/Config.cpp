#include "StdAfx.h"
#include "Config.h"
#include <ShlObj.h>

using namespace std;

namespace ArchiveTemplates
{
  template<class Archive>
  void SerializeRef(SimpleStr &item, Archive &arch)
  {
    wchar_t *z=item.Unlink();
    arch(z);
    item=z;
  }

  template<class Archive>
  void SerializeRef(RECT &item, Archive &arch)
  {
    arch("_left",item.left);
    arch("_top",item.top);
    arch("_right",item.right);
    arch("_bottom",item.bottom);
  }

  template<class Archive, class Item, class Alloc>
  void SerializeRef(AutoArray<Item,Alloc> &item, Archive &arch)
  {
    if (+arch)
    {
      for (int i=0;i<item.Size();i++)
      {
        arch("item",item[i]);
      }
    }
    else
    {
      item.Clear();
      Item tmp;
      while (arch("item",tmp)) item.Add(tmp);
    }
  }
}


Config::Config(void)
{
  askOnFirstAccess=false;
  savedExpiration=0;
  Pathname pth;
  pth.SetDirectorySpecial(CSIDL_PROGRAM_FILES);
  neverSSPaths.Add(SimpleStr(pth.GetDirectoryWithDriveWLBS()));
  pth.SetDirectorySpecial(CSIDL_WINDOWS);
  neverSSPaths.Add(SimpleStr(pth.GetDirectoryWithDriveWLBS()));
  pth.SetDirectorySpecial(CSIDL_DESKTOP);
  neverSSPaths.Add(SimpleStr(pth.GetDirectoryWithDriveWLBS()));
  pth.SetDirectorySpecial(CSIDL_STARTMENU);
  neverSSPaths.Add(SimpleStr(pth.GetDirectoryWithDriveWLBS()));
  pth.SetDirectorySpecial(CSIDL_SENDTO);
  neverSSPaths.Add(SimpleStr(pth.GetDirectoryWithDriveWLBS()));
  pth.SetTempDirectory();  
  neverSSPaths.Add(SimpleStr(pth.GetDirectoryWithDriveWLBS()));
  winRect.bottom=500;
  winRect.left=0;
  winRect.right=250;
  winRect.top=0;
  detectComDlgOpen=true;

}

Config::~Config(void)
{
}

_TCHAR *DupStrMB(const char *str)
{
#ifdef _UNICODE
  size_t mblen=strlen(str)+1;
  size_t len=MultiByteToWideChar(CP_THREAD_ACP,0,str,mblen,0,0);
  _TCHAR *out=new _TCHAR[len];
  MultiByteToWideChar(CP_THREAD_ACP,0,str,mblen,out,len);
  return out;
#else
  return DupStr(str)
#endif
}

char *DupStrMB(const _TCHAR *str)
{
  if (str==0) return 0;
#ifdef _UNICODE
  size_t wclen=wcslen(str)+1;
  size_t len=WideCharToMultiByte(CP_THREAD_ACP,0,str,wclen,0,0,0,0);
  char *out=new char[len];
  WideCharToMultiByte(CP_THREAD_ACP,0,str,wclen,out,len,0,0);
  return out;
#else
  return DupStr(str)
#endif
}

void Config::LoadConfig(const Pathname &name)
{
  ifstream in(name.SBS_GetFullPath(),ios::in);
  if (!in) return;
  StandardStream<ifstream,false> stream(&in);
  XMLIn<StandardStream<ifstream,false>,false> xmlin(&stream);
  Archive<XMLIn<StandardStream<ifstream,false>,false>,false> arch(&xmlin);

  configName=name;
  
  if (xmlin.ReadHeader(0,0)==false) return;

  arch("config",*this);  
}

bool Config::SaveConfig() const
{
  ofstream out(configName.SBS_GetFullPath(),ios::in);
  if (!out) return false;
  StandardStream<ofstream,false> stream(&out);
  XMLOut<StandardStream<ofstream,false>,false> xmlout(&stream);
  Archive<XMLOut<StandardStream<ofstream,false>,false>,false> arch(&xmlout);
  
  xmlout.WriteHeader("UTF-8");

  arch("config",*const_cast<Config *>(this));  
  return (!(!out));
}
