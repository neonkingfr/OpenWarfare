#include "StdAfx.h"
#include "resource.h"
#include "DlgOptions.h"

DlgOptions::DlgOptions(Config &config,IDlgOptions *notify):_config(config),_notify(notify)
{
}

DlgOptions::~DlgOptions(void)
{
}

static _TCHAR *PackPaths(const Array<SimpleStr> &arr)
{
  size_t needsz=0;
  for (int i=0;i<arr.Size();i++) needsz+=_tcslen(arr[i])+1;
  _TCHAR *z=new _TCHAR[needsz+1];
  _TCHAR *q=z;
  for (int i=0;i<arr.Size();i++)
  {
    _tcscpy(q,arr[i]);
    q+=_tcslen(q);
    if (i+1<arr.Size()) *q++=';';
  }
  *q=0;
  return z;
}

static _TCHAR *Trim(_TCHAR *buff)
{
  _TCHAR *end=buff+_tcslen(buff);
  while (end>buff && isspace(end[-1])) end--;
  *end=0;
  while (*buff && isspace(*buff)) buff++;
  return buff;
}

static AutoArray<SimpleStr> UnpackPaths(const _TCHAR *paths)
{
  AutoArray<SimpleStr> list;
  _TCHAR *dup=DupStr(paths);
  _TCHAR *brk=dup;
  _TCHAR *p=dup;
  while (*p)
  {
    if (*p==';')
    {
      *p=0;
      brk=Trim(brk);
      if (brk[0]) list.Add(DupStr(brk));
      brk=p+1;
    }
    p++;
  }
  brk=Trim(brk);
  if (brk[0])
      list.Add(DupStr(Trim(brk))); 
  delete [] dup;
  return list;
}

LRESULT DlgOptions::OnInitDialog()
{
  
  SetDlgItemTextA(*this,IDC_SERVICEPROVIDER,_providerName);

  if (_config.askOnFirstAccess) CheckDlgButton(*this,IDC_ASKMEESS,BST_CHECKED);
  if (_config.detectComDlgOpen) CheckDlgButton(*this,IDC_NOCOMDLG,BST_CHECKED);

  _TCHAR *alwaysSSPaths=PackPaths(_config.alwaysSSPaths);
  _TCHAR *neverSSPaths=PackPaths(_config.neverSSPaths);
  SetDlgItemText(IDC_EXCLUDED,neverSSPaths);
  SetDlgItemText(IDC_INCLUDED,alwaysSSPaths);
  
  delete []alwaysSSPaths;
  delete []neverSSPaths;
  
  return TRUE;  
}

LRESULT DlgOptions::OnOK()
{
  bool oldDetect=_config.detectComDlgOpen;
  _config.askOnFirstAccess=IsDlgButtonChecked(*this,IDC_ASKMEESS)==BST_CHECKED;
  _config.detectComDlgOpen=IsDlgButtonChecked(*this,IDC_NOCOMDLG)==BST_CHECKED;
  
  _config.alwaysSSPaths=UnpackPaths(SimpleStr(GetDlgItemText(IDC_INCLUDED)));
  _config.neverSSPaths=UnpackPaths(SimpleStr(GetDlgItemText(IDC_EXCLUDED)));

  if (oldDetect!=_config.detectComDlgOpen)
    MessageBox(*this,StrRes[IDS_APPLICATIONMUSTBERESTARTED],StrRes[IDS_NEEDRESTART],MB_OK|MB_ICONEXCLAMATION);

  return CDialogClass::OnOK();
}

static BOOL OpenServiceProviderDll(HWND hWnd,_TCHAR *fname, size_t chars)
{
  OPENFILENAME ofn;
  ZeroMemory(&ofn,sizeof(ofn));
  ofn.lStructSize=sizeof(ofn);
  ofn.Flags=OFN_ENABLESIZING|OFN_FILEMUSTEXIST|OFN_FORCESHOWHIDDEN|OFN_HIDEREADONLY|OFN_NOCHANGEDIR;
  ofn.hwndOwner=hWnd;
  ofn.hInstance=(HINSTANCE)GetWindowLong(hWnd,GWL_HINSTANCE);
  ofn.lpstrFilter=_T("DLL\0*.DLL\0");
  ofn.lpstrFile=fname;
  ofn.nMaxFile=chars;

  return GetOpenFileName(&ofn);
}



LRESULT DlgOptions::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
{
  switch (wID)
  {
  case IDC_CHANGESP:
    OnChangeServiceProvider();   
    return 1;
  case IDC_DEFAULT:
    OnDefaultserviceProvider();
    return 1;
  }
  return CDialogClass::OnCommand(wID,wNotifyCode,clt);
}

void DlgOptions::OnChangeServiceProvider()
{
  _TCHAR buff[4096];
  _tcsncpy(buff,_config.SSprovidersDll?_config.SSprovidersDll:_T(""),sizeof(buff)/sizeof(buff[0]));
  buff[sizeof(buff)/sizeof(buff[0])-1]=0;

  if (OpenServiceProviderDll(*this,buff,sizeof(buff)/sizeof(buff[0])))
  {
    _providerName = _notify->ChangeServiceProvider(buff);
    SetDlgItemTextA(*this,IDC_SERVICEPROVIDER,_providerName);

  }
}


void DlgOptions::OnDefaultserviceProvider()
{
  _providerName = _notify->ChangeServiceProvider(0);
  SetDlgItemTextA(*this,IDC_SERVICEPROVIDER,_providerName);
}