// AutoSSCntrl.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "AutoSSCntrl.h"
#include "MainWnd.h"
#include "../IPC.h"

// Global Variables:
static HINSTANCE hInst;								// current instance

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

SRef<MainWnd> mainWnd;

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
  {

 	// TODO: Place code here.
	MSG msg;

	MyRegisterClass(hInstance);

	// Perform application initialization:  
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}



	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
			TranslateMessage(&msg);
			DispatchMessage(&msg);            
	}

  

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MAINICON));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= 0;
	wcex.lpszMenuName	= 0;
	wcex.lpszClassName	= MAINWNDCLASS;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_MAINICON));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowEx(WS_EX_TOPMOST,MAINWNDCLASS, StrRes[IDS_UNBOUND], WS_OVERLAPPED|WS_MINIMIZEBOX|WS_CAPTION|WS_SYSMENU|WS_THICKFRAME|WS_VISIBLE|WS_MINIMIZE,
      CW_USEDEFAULT, 0, 250, 500, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   mainWnd=new MainWnd(hWnd);


   ShowWindow(hWnd,SW_SHOW);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT , WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{  
  return mainWnd?mainWnd->WndProc(hWnd,message,wParam,lParam):DefWindowProc(hWnd,message,wParam,lParam);
}


void LogF(char const *txt,...)
{
#ifdef _UNICODE
  DWORD needsz=MultiByteToWideChar(CP_THREAD_ACP,0,txt,strlen(txt)+1,0,0);
  wchar_t *buff=(wchar_t *)alloca(needsz*sizeof(wchar_t));
  MultiByteToWideChar(CP_THREAD_ACP,0,txt,strlen(txt),buff,needsz);
#else
  char *buff=const_cast<char *>(txt);
#endif;

  va_list args;
  va_start(args,txt);
  _TCHAR outbuf[2048];
  
  _vsntprintf(outbuf,sizeof(outbuf)/sizeof(outbuf[0]),buff,args);
  OutputDebugString(outbuf);
}

void __cdecl ErrF(char const *,...)
{
}

HINSTANCE AfxGetInstanceHandle() {return hInst;}