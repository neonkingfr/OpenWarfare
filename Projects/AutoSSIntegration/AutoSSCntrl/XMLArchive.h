#include <iostream>
#include <fstream>
#include "StandardStream.h"
#include "Archive.h"
#include "XMLIn.h"
#include "XMLOut.h"

typedef StandardStream<std::iostream> StandardIOStream;
typedef XMLIn<StandardIOStream> StandardXMLIn;
typedef Archive<StandardXMLIn> XMLInArchive;

typedef XMLOut<StandardIOStream> StandardXMLOut;
typedef Archive<StandardXMLOut> XMLOutArchive;