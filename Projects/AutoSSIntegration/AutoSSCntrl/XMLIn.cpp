#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "XMLIn.h"

namespace XMLInOutHelp
{
  int TextUTF8ToUnicode(const char *text, wchar_t *outbuf, int bufSz)
  {
    if (text==0)
    {
      if (outbuf) outbuf[0]=0;
      return 1;
    }
    return MultiByteToWideChar(CP_UTF8,0,text,strlen(text)+1,outbuf,bufSz);
  }

  int TextUnicodeToUTF8(const wchar_t *text, char *outbuf, int bufSz)
  {
    if (text==0)
    {
      if (outbuf) outbuf[0]=0;
      return 1;
    }
    return WideCharToMultiByte(CP_UTF8,0,text,wcslen(text)+1,outbuf,bufSz,0,0);
  }
}