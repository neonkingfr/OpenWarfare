#if !defined(AFX_INSERTITEMDLG_H__1611635E_662C_4482_B7E1_04F4FDD6713E__INCLUDED_)
#define AFX_INSERTITEMDLG_H__1611635E_662C_4482_B7E1_04F4FDD6713E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InsertItemDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInsertItemDlg dialog

class CInsertItemDlg : public CDialog
{
  // Construction
  public:
    CInsertItemDlg(CWnd* pParent = NULL);   // standard constructor
    
    CString text;
    DWORD flags;
    DWORD color;
    // Dialog Data
    //{{AFX_DATA(CInsertItemDlg)
    enum 
      { IDD = IDD_INSERTITEMDLG };
    BOOL	m_canbelinked;
    BOOL	m_canlinkself;
    BOOL	m_canmakelinks;
    BOOL	m_canmove;
    BOOL	m_canresize;
    BOOL	m_customdraw;
    CString	m_itemtext;
    float	m_dimx;
    float	m_dimy;
    CString	m_colortxt;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CInsertItemDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CInsertItemDlg)
    virtual BOOL OnInitDialog();
    virtual void OnOK();
    afx_msg void OnPick();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSERTITEMDLG_H__1611635E_662C_4482_B7E1_04F4FDD6713E__INCLUDED_)
