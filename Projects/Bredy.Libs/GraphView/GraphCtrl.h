#if !defined(AFX_GRAPHCTRL_H__E4DB9B5C_0718_41BC_AB4D_EE78BFBD140E__INCLUDED_)
#define AFX_GRAPHCTRL_H__E4DB9B5C_0718_41BC_AB4D_EE78BFBD140E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GraphCtrl.h : header file
//

#include <Es/essencepch.hpp>
#include <Es/Containers/array.hpp>

/////////////////////////////////////////////////////////////////////////////
// CGraphCtrl window

struct SFloatRect	//definice floatoveho rectangle.
{
  float left,top,right,bottom;
  SFloatRect() 
  {}
  SFloatRect(float l,float t,float r, float b):left(l),top(t),right(r),bottom(b) 
  {}

  template<class Archive>
  void Serialize(Archive &arch)
  {
    arch("_left",left);
    arch("_top",top);
    arch("_right",right);
    arch("_bottom",bottom);
  }
};

#define SGRI_MAXTEXTLEN 64

#define SGRI_UNUSEDITEM 0x1 //interni pouziti - oznaceni nepouzite polozky
#define SGRI_CUSTOMDRAW 0x2  //vnitrek policka se vykresluje ve funkci
#define SGRI_FILLBKCOLOR 0x4  //pri pouziti ownerdraw pred vykreslenim obsah policka vymaze barvou
#define SGRI_CANRESIZE 0x8 // je dovoleno menit velikost policka
#define SGRI_CANMOVE 0x10 //je dovoleno posouvat polickem (automaticky zapnuto pri CANRESIZE)
#define SGRI_CANSELFLINKED 0x20 //je dovoleno linkovat sama na sebe
#define SGRI_CANBELINKED 0x40 //je dovoleno linkovat na toto policko
#define SGRI_CANMAKELINKS 0x80 //je dovoleno linkovat z toho policka

#define SGRI_NORMAL (SGRI_CANMOVE | SGRI_CANBELINKED | SGRI_CANMAKELINKS)
#define SGRI_NORMALRESIZE (SGRI_NORMAL | SGRI_CANRESIZE)
#define SGRI_NORMALSELFLINK (SGRI_NORMAL | SGRI_CANSELFLINKED)
#define SGRI_ALL (SGRI_NORMAL |SGRI_CANRESIZE | SGRI_CANSELFLINKED )

//stavove vlajky
#define SGRI_SELECTED 0x1000 //policko je vybrano
#define SGRI_NOTDRAWED 0x2000 //policko neni vykresleno, protoze bylo moc male

//specialni vlajky
#define SGRI_NOGUESSPOSITION 0x1 //pri vkladani nebude hledat vhodnou pozici, ale item umisiti na 0,0

//vlajky linku
#define SGRI_FROZEN 0x1 //link je zmrazen, nelze jej editovat
#define SGRI_CUSTOMDRAW 0x2  
#define SGRI_DONTFOCUS 0x4 

//mody
#define SGRM_NORMAL 0x1
#define SGRM_ZOOM 0x2
#define SGRM_DESIGN 0x3
#define SGRM_GETCURRENT 0x0

struct SGraphItem 
{
  SFloatRect rect;
  DWORD color;
  DWORD flags;
  char text[SGRI_MAXTEXTLEN];
  void *data;
};

struct SGraphLink
{
  int fromitem;
  int toitem;
  short order;
  short flags;
  DWORD color;
  void *data;
};

TypeIsMovable(SGraphItem);
TypeIsMovable(SGraphLink);


class CGraphCtrl : public CWnd
{
  // Construction
  friend class CGraphCtrlExt;
  AutoArray<SGraphItem> items;
  AutoArray<SGraphLink> links;
  SFloatRect pzoom,spzoom,bbox;
  CPoint begdrg; //begin point of dragging
  CPoint enddrg;
  CRect selrect; //selection rectangle
  bool alt;
  bool updatebbox;
  bool aspect;
  bool showhelp;  
  int clxs;
  int clys;
  CFont *deffont;
  DWORD textcolor;
  DWORD ffree;
  DWORD bkcolor;
  int smallx,smally;
  int padding;
  int focused;
  int focusedlink;
  int drophilted;
  int resizecorner;
  unsigned int timer;
  bool bmpdraw;
  float arrowSz;
  CBitmap bmp;  
  enum e_mode 
  {e_normal,e_zoom,e_draglink,e_begindraglink,e_design,e_begdragdesign,e_dragdesign,e_begdraggraph};
  e_mode mode, lastMode;
  public:
    CGraphCtrl();
    
    // Attributes
  public:
    
    // Operations
  public:
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CGraphCtrl)
	//}}AFX_VIRTUAL
    
    // Implementation
  public:
    //Inserts an item into control. You can associate a data with item
    //Returns item ID
    int InsertItem(DWORD flags, DWORD color, const char *text, float width, float height, void *data=NULL);
    //Deletes item identified by ID
    void DeleteItem(int item);
    //Finds random position for item. 	
    SFloatRect GuessPosition(float width, float height);
    //Calculates and returns bounding box about whole area;
    SFloatRect& GetBoundingBox();
    //Changes text on an item
    void SetItemText(int item, const char *text);
    //Retrieves text of an item
    const char * GetItemText(int item);
    //Changes position and dimensions of an item
    void SetItemRect(int item, const SFloatRect &rect);
    //Retrieves position and dimensions of an item
    SFloatRect GetItemRect(int item);
    //Sets user-defined data pointer in an item
    void SetItemData(int item, void *data);
    //Retrieves user-defined data pointer in an item
    void * GetItemData(int item) const;
    DWORD GetItemFlags(int item) 
    {return items[item].flags;}
    void SetItemFlags(int item, DWORD invert, DWORD zero)
    {items[item].flags=(items[item].flags & ~zero) ^ invert;}
    // number of items
    int NItems() const {return items.Size();}
    //Retrieves full item descriptor
    bool GetItem(int item, SGraphItem *pitm);
    //Changes item using item-descriptor
    bool SetItem(int item, SGraphItem *pitm);
    //Enumeraters items
    //Set enumerator to -1 at begining of cycle
    //Use result of this function as enumerator in next cycles
    //result is items ID too
    //if result is -1, no items remain
    //EX: for (int enum=-1;(enum=x.EnumItems(enum))!=-1;) {...use enum as ID ...}
    int EnumItems(int enumerator,bool selected=false) const;
    //Returns true, if item is currently selected
    bool ItemSelected(int item);
    //Selects or unselets an item. Notify function is always called, if selection changed
    void SelectItem(int item, bool select=true);
    //Selects items inside rectangle. If clearsel is sets, it removes previous selection
    void SelectItems(const SFloatRect &rc, bool clearsel=true);
    //Select all items
    void SelectAllItems();
    //Removes selection	
    void Deselect();
    //Finds item at point and returns its ID. If no item is found, retuns -1
    //if selonly is sets, search in selected items only
    int ItemFromPoint(float x, float y, bool selonly=false);
    //Converts point in client coordinates into canvas coordiantes
    void MapPointFromWindow(const CPoint &pt, float *x, float *y); //mapuje bod z obrazovky do floatu - pokud je souradnice NULL, nepocita se
    //Converts point in canvas coordinates into client coordiantes
    CPoint MapPointToWindow(float x, float y); //mapuje bod na obrazovku
    //Converts rectangle in client coordinates into canvas coordinates
    SFloatRect MapRectFromWindow(const CRect &rect);
    //Converts rectangle in canvas coordinates into client coordinates
    CRect MapRectToWindow(const SFloatRect &frect);	
    //Enables or disables caching of drawing in bitmap
    //If caching is enabled, content of control is drawed into bitmap and then blitted into window
    //If caching is disabled, content of control is drawed directly to window
    void EnableBitmapDrawing(bool enable);
    //Forces control to update drawing soon.
    void Update();
    //Updates scrollbars
    void UpdateScrollbars();
    //Draws content of control to a DC. clxs and clys is dimensions of DC
    virtual void CGraphCtrl::Draw(CDC &dc, const CRect &cliprc);
    //Sets canvas rectangle that will be mapped into window rectangle. 
    //Drawing will be zoomed or panned.
    void SetPanZoom(const SFloatRect &panzoom, bool aspec=true);
    SFloatRect GetPanZoom(bool original=false)
    {if (original) return pzoom;else return SFloatRect(spzoom.left,spzoom.top,spzoom.left+spzoom.right,spzoom.top+spzoom.bottom);}
    //Changes default font of texts. Passed handle is not freeed when control is destroyed
    //You should call Update(), to redraw control
    void SetFont(CFont *fnt) 
    {deffont=fnt;}
    //Returns current font
    CFont *GetFont() 
    {return deffont;}
    //Sets color of all texts. You should call Update(), to redraw control
    void SetTextColor(DWORD tc) 
    {textcolor=tc;}
    //Retrieves current text color
    DWORD GetTextColor() 
    {return textcolor;}
    //Sets dimension (in pixels) of small items. Smaller items aren't displayed, until
    //they zoomed
    void SetSmallSize(int smlx,int smly) 
    {smallx=smlx,smally=smly;}
    //Sets padding of items. You should call Update(), to redraw control
    void SetPadding(int pad) 
    {padding=pad;}
    //Retrieves current padding of items.
    int GetPadding() 
    {return padding;}
    //Removes all items and links
    void ResetContent() 
    {links.Clear();items.Clear();ffree=0;}
    void FreeExtra() 
    {items.Compact();links.Compact();}
    int Mode(int mode);
    virtual ~CGraphCtrl();
    
  public:
    //Creates new link using color and associates data with it
    bool InsertLink(int fromitem, int toitem, DWORD color, short flags=0, void *data=NULL);
    //Returns temporally ID of current focused link (use in event handler)
    int GetFocusedLink();
    //Returns state, if mouse points to link
    //pt - point in client area
    //link - ID of enumerated link
    //returns - 0 - does not pointing, 1 - pointing and it is p2p link, 2 - pointing and it is self-link
    int MouseAtLink(int link, CPoint &pt);
    //Finds link from point, returns temporally ID that can be used in next operations.
    int LinkFromPoint(CPoint &pt);
    //Calculates link rectangle, if link started at "from", ended at "to", and his order is "order" 
    //"order" means order in listo of links, that have same "from" and "to" values.
    //Returns false, if link coudln't be displayed
    virtual bool CalcLinkRect(int from, int to, int order, CRect &rc);
    void PrecalcLinkRect(int from, int to, int order, SFloatRect *&fr, SFloatRect *&tt, SFloatRect &ln);
    //Enumerates links that have some properties.
    //If fromitem is not set to -1, then enumerates only links where fromitem match the value
    //If toitem is not set to -1, then enumerates only links where toitem match the value
    //If data is not set to NULL, then enumerates only links, where data has specified value
    //Returns ID of link, this value have to be passed to function to get next ID of link
    int EnumLinks(int enm, int fromitem=-1, int toitem=-1,void *data=NULL);
    //Gets link descriptor from ID. Use EnumLinks to generate IDs
    SGraphLink &GetLink(int enm) 
    {return links[enm];}
    //Removes link identified by ID. Use EnumLinks to generate IDs. After deleting link, ID is invalid,
    //but you can still pass this ID into EnumLinks function to get ID of next link
    virtual void DeleteLink(int &enm) 
    {links.Delete(enm);enm--;focusedlink=-1;}
    //Removes links that match filter. Read description of EnumLinks function
    void DeleteLinks(int fromitem=-1, int toitem=-1, void *data=NULL);
    //Removes all links
    void DeleteAllLinks() 
    {links.Clear();focusedlink=-1;}
    //This function should be called after all operations with links are finished. Reorganizates list of links,
    //calculates its order to correct display.
    void OrderLinks();
    //Sets link flags
    void SetLinkFlags(int lnk, short invert, short zero)
    {links[lnk].flags=(links[lnk].flags & ~zero) ^ invert;}
    short GetLinkFlags(int lnk) const
    {return links[lnk].flags;}
    //This function is called to draw something into any corner of the selected item
    virtual void DrawNormalSelection(CDC &dc, int x, int y) {};

	  bool IsValidItem(int item)
	  {
	  return (item>=0 && item<items.Size()) && !(items[item].flags & SGRI_UNUSEDITEM);
	  }

    // notify virtual functions
    
    //Vola se pri zmene stavu policka - napriklad vybrani, nebo nevybrani
    virtual void OnItemStateChanged(int item) 
    {}
    //Vola se pri ukonceni vyberu poicek mysi
    virtual void OnEndSelection() 
    {}
    //Vola se pri nastavenem priznaku SGRI_CUSTOMDRAW 
    //Funkce ke kresleni vyuzije DC a ramecek.
    //Pokud funkce vrati false, pak control dokresli policko podle ramecku rc (lze jej vtipne upravit)
    virtual bool OnCustomDraw(int item, SGraphItem &itm, CDC &dc, CRect &rc) 
    {return false;}
    //Vola se pri spojeni dvou policek linkem
    virtual void OnLinkItems(int beginitem, int enditem) 
    {}
    //Vola se pri vytvoreni linku do "nikam"
    virtual void OnLinkNewItem(int beginitem, float x, float y) 
    {}
    //Vola se pri zmene spojeni a to i v pripade, ze uzivatel vybere stejnou polozku jako predtim
    virtual void OnChangeLink(int lnk, int beginitem, int enditem, int newitem, void *data) 
    {}
    //Vola se pri odtazeni spojeni mimo jakekoliv policko
    virtual void OnUnlinkItems(int lnk, int beginitem, int enditem, void *data) 
    {}
    //Vola se pri dotyku mysi linku. Take se vola, kdyz uzivatel opusti mysi link
    //V tom pripade funkce nastavuje vsechny hodnoty krome data na -1 a data nastavuje na NULL
    virtual void OnTouchLink(int lnk, int beginitem, int enditem, void *data) 
    {}
    //Vola se pri vykresleni linku, pokud je link zapnuty CUSTOMDRAW
    virtual bool OnCustomDrawLink(int lnk, int beginitem, int enditem, int order, void *data, CDC &dc, CRect rc) 
    {return false;}
    //Vola se po zmene velikosti policka 
    virtual void OnResizeItem(int item, int corner, float xs, float ys) 
    {}
    //Vola se po zmene polohy policka 
    virtual void OnMoveItem(int item, float xs, float ys) 
    {}
    //Vola se po ukonceni uprav poloh a velikosti policek
    //pokud je corner==-1 pak se provadelo move;
    //jinak urcuje cislo rohu, za ktery se tahalo
    virtual void OnEndMoveResize(int corner) 
    {}
    virtual void BeforeResizeItem(int corner)
    {}
    virtual void BeforeMoveItem() 
    {}
    virtual void BeforeMoveResize() 
    {}
    
    // Generated message map functions
  protected:
    void DrawSelectionRect();
    //{{AFX_MSG(CGraphCtrl)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnCancelMode();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    private:
    void DrawLink(CDC &dc, int lnk);
    int TestUchyt(CPoint &pt);
    void CalcSpzoom();
    void ResizeSelected(int corner, float xs, float ys);
  public:
    
    void SetBKColor(COLORREF color)  
    {bkcolor=color;}
    void SetArrowSize(float newSz) {arrowSz=newSz;}
  protected:
    void DrawSelection(CDC &dc, CRect &rc,short flags);
    //  virtual void DoDataExchange(CDataExchange* pDX);
};

void ClipLineByBox(SFloatRect &line, SFloatRect &box, bool begin);

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAPHCTRL_H__E4DB9B5C_0718_41BC_AB4D_EE78BFBD140E__INCLUDED_)
