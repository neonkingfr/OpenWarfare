// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__52D46D01_7EF9_4CCC_A7C3_F16833820380__INCLUDED_)
#define AFX_MAINFRM_H__52D46D01_7EF9_4CCC_A7C3_F16833820380__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GraphCtrlExample.h"

class CMainFrame : public CFrameWnd
{
  
  public:
    CMainFrame();
  protected: 
    DECLARE_DYNAMIC(CMainFrame)
      
      // Attributes
    public:
    
    // Operations
  public:
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMainFrame)
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
    void ResetZoom();
    virtual ~CMainFrame();
    #ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
    #endif
    CGraphCtrlExample    View;
    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CMainFrame)
    afx_msg void OnSetFocus(CWnd *pOldWnd);
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnEditInsertitem();
    afx_msg void OnEditDeleteitems();
    afx_msg void OnUpdateEditDeleteitems(CCmdUI* pCmdUI);
    afx_msg void OnPropertiesEdititem();
    afx_msg void OnUpdatePropertiesEdititem(CCmdUI* pCmdUI);
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    afx_msg void OnUpdatePropertiesCanbelinked(CCmdUI* pCmdUI);
    afx_msg void OnUpdatePropertiesCanmakelinks(CCmdUI* pCmdUI);
    afx_msg void OnUpdatePropertiesCanmove(CCmdUI* pCmdUI);
    afx_msg void OnUpdatePropertiesCanselflinked(CCmdUI* pCmdUI);
    afx_msg void OnUpdatePropertiesCansize(CCmdUI* pCmdUI);
    afx_msg void OnUpdatePropertiesCustomdraw(CCmdUI* pCmdUI);
    afx_msg void OnPropertiesCustomdraw();
    afx_msg void OnPropertiesCansize();
    afx_msg void OnPropertiesCanselflinked();
    afx_msg void OnPropertiesCanmove();
    afx_msg void OnPropertiesCanmakelinks();
    afx_msg void OnPropertiesCanbelinked();
    afx_msg void OnLinkNormal();
    afx_msg void OnLinkDesign();
    afx_msg void OnLinkZoom();
    afx_msg void OnUpdateLinkNormal(CCmdUI* pCmdUI);
    afx_msg void OnUpdateLinkZoom(CCmdUI* pCmdUI);
    afx_msg void OnUpdateLinkDesign(CCmdUI* pCmdUI);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__52D46D01_7EF9_4CCC_A7C3_F16833820380__INCLUDED_)
