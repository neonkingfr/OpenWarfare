//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by GraphView.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_GRAPHVTYPE                  129
#define IDS_QCREATELINK                 129
#define IDS_QCHANGELINK                 130
#define IDB_BITMAP1                     131
#define IDS_QDELETELINK                 131
#define IDB_COMPUTERMASK                131
#define IDD_INSERTITEMDLG               132
#define IDS_QDELETE                     132
#define IDB_COMPUTER                    133
#define IDS_DONTFOCUSWARNING            133
#define IDR_LINKMENU                    134
#define IDS_ZOOMWARNING                 134
#define IDR_GRAPHZOOMCURSOR             135
#define IDC_ITEMTEXT                    1000
#define IDC_CANMOVE                     1001
#define IDC_CANRESIZE                   1002
#define IDC_CANBELINKED                 1003
#define IDC_CANMAKELINKS                1004
#define IDC_CANLINKSELF                 1005
#define IDC_CUSTOMDRAW                  1006
#define IDC_DIMX                        1007
#define IDC_DIMY                        1008
#define IDC_COLOR                       1009
#define IDC_PICK                        1010
#define ID_EDIT_INSERTITEM              32771
#define ID_PROPERTIES_CANMOVE           32772
#define ID_PROPERTIES_CANSIZE           32773
#define ID_PROPERTIES_CANBELINKED       32774
#define ID_PROPERTIES_CANMAKELINKS      32775
#define ID_PROPERTIES_CANSELFLINKED     32776
#define ID_PROPERTIES_CUSTOMDRAW        32777
#define ID_EDIT_DELETEITEMS             32778
#define ID_PROPERTIES_EDITITEM          32779
#define ID_LINK_ZAPNOUTPOPISEK          32780
#define ID_LINK_FROZEN                  32781
#define ID_LINK_NORMAL                  32782
#define ID_LINK_DESIGN                  32783
#define ID_LINK_ZOOM                    32784
#define ID_LINK_DONTFOCUS               32785

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32786
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
