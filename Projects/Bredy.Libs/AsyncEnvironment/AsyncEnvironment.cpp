// AsyncEnvironment.cpp : Defines the entry point for the console application.
//
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "AsyncEnvironment.h"
#include <typeinfo.h>


Tls<AsyncEnvironment> *AsyncEnvironment::thisTls=0;

#define ON_STARTUP_PRIORITY_USERMEMORYINIT AsyncEnvTlsInit

static void AsyncEnvTlsInit()
{
  static Tls<AsyncEnvironment> tls;
  AsyncEnvironment::thisTls=&tls;
}

#include <es/common/startup.hpp>


void ___AsyncEnvReportNewThread(AsyncEnvironment *self)
{
  char dbgmessage[256];
  sprintf(dbgmessage,"The thread 0x%X has started: '%s'\r\n",GetCurrentThreadId(),typeid(*self).name());
  OutputDebugStringA(dbgmessage);
}

#ifdef ASYNCENV_DEMO
#include "stdafx.h"
#include "AsyncEnvWinGui.h"
#include "AsyncEnvMultiple.h"
#include "AsyncClass.h"


class ExampleMasterClass: public AsyncClass<>
{
public:
  void Run();
  DEF_MESSAGE2(CalcResult, ExampleMasterClass, int, double);

private:
  void OnCalcResult(AsyncMessage *msg, int value, double result)
    {      
      printf("Value: %d Result: %g\r\n",value,result);  
    }
};


class ExampleSlaveClass: public AsyncClass<AsyncBaseAttachable>
{
public:
  DEF_MESSAGE2(Calculate, ExampleSlaveClass, int, AsyncEnvironment *);

private:
  void OnCalculate(AsyncMessage *msg, int value, AsyncEnvironment *reply)
  {
    double res=1;
    for (int i=1;i<=value;i++) res=res*i;
    printf("Calculated: %d\r\n",value);
	reply->Send(new ExampleMasterClass::CalcResult(value,res));
    msg->Reply();
    Sleep(190);
  }
};

void ExampleMasterClass::Run()
{
  Attach();
  ExampleSlaveClass calc;
  AsyncEnvSingle async;
  calc.Attach(&async);

  SetNonPernamentMode(2000);   //set timeout to stop message procession 
  for(int i=0;i<100;i++) 
  {calc.SendAndWait(new ExampleSlaveClass::Calculate(i,this));}
  
  PumpAllMessages();

  Detach();
}

int _tmain(int argc, _TCHAR* argv[])
{
	ExampleMasterClass master;
	master.Run();
	return 0;
}

#endif