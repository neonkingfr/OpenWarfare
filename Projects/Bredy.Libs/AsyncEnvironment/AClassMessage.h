#pragma once
#include "asyncenvironment.h"


///Template for creating messages
/**
To define new message for class, use following template. You have to specify following types
@param Owner Owner class, which will receive the messages
@oaram Base Base class  defines type of declared message (AsyncMessage or AsyncBlockableMessage)
*/
template<class Owner, class Base=AsyncMessage>
class AClassMessage :  public Base
{
public:
  Owner *_owner;
};

///Template for creating asynchronous classes
/**
Asynchronous class must derive following template. You have to specify following types
@param Derived Enter name of class you are declaring
@param Base Type if environment which is used by declared class (AsyncEnvSimple,AsyncEnvWinGui,AsyncEnvMultiple etc).
*/
template<class Derived, class Base=AsyncEnvSimple>
class AsynchronousClass: public Base
{
public:
  virtual void Send(AsyncMessage *message)
  {
    AClassMessage<Derived> *msg=dynamic_cast<AClassMessage<Derived> *>(message);
    if (msg) msg->_owner=(static_cast<Derived *>(this));
    Base::Send(message);
  }
};

///Declares message without parameters
/**
  DECLARE_MESSAGE(AsyncMessage,MyClass,ExampleMessage);

  Each message is sent by Send function. Sent message is processed on class by calling On&lt;name&gt; member function
*/
#define DECLARE_MESSAGE(base,owner,name) \
  class name:public AClassMessage<owner,base> \
  {                 \
  public: \
    virtual void Run() {_owner->On##name(*this);}\
  };                \
  friend name






///Declares message with one parameter
/**
  DECLARE_MESSAGE(AsyncMessage,MyClass,ExampleMessage, int, value);

  object->Send(new MyClass::ExampleMessage(1234));
*/
#define DECLARE_MESSAGE_1P(base,owner,name,type1,param1) \
  class name:public AClassMessage<owner,base> \
  {                 \
  public: \
    type1 param1; \
    name(type1 param1):param1(param1) {} \
    virtual void Run() {_owner->On##name(*this);}\
  };                \
  friend name






///Declares message with two parameters
#define DECLARE_MESSAGE_2P(base,owner,name,type1,param1,type2,param2) \
  class name:public AClassMessage<owner,base> \
  {                 \
  public: \
    type1 param1; \
    type2 param2; \
    name(type1 param1,type2 param2):param1(param1),param2(param2) {} \
    virtual void Run() {_owner->On##name(*this);}\
  };          \
  friend name



///Declares message with three parameters
#define DECLARE_MESSAGE_3P(base,owner,name,type1,param1,type2,param2,type3,param3) \
  class name:public AClassMessage<owner,base> \
  {                 \
  public: \
    type1 param1; \
    type2 param2; \
    type3 param3; \
    name(type1 param1,type2 param2,type3 param3):param1(param1),param2(param2),param3(param3) {} \
    virtual void Run() {_owner->On##name(*this);}\
  };                \
  friend name







///Declares message with four parameters
#define DECLARE_MESSAGE_4P(base,owner,name,type1,param1,type2,param2,type3,param3,type4,param4) \
  class name:public AClassMessage<owner,base> \
  {                 \
  public: \
    type1 param1; \
    type2 param2; \
    type3 param3; \
    type4 param4; \
    name(type1 param1,type2 param2,type3 param3,type4 param4):param1(param1),param2(param2),param3(param3),param4(param4) {} \
    virtual void Run() {_owner->On##name(*this);}\
  };                \
  friend name


///Declares message with complex parameters
/**
BEGIN_COMPLEX_MESSAGE(AsyncMessage,MyClass,ExampleComplexMessage)
  int p1;
  char chr;
  RString text;
  float factor;
END_COMPLEX_MESSAGE(ExampleComplexMessage);
*/
#define BEGIN_COMPLEX_MESSAGE(base,owner,name,var1) \
  class name:public AClassMessage<owner,base> \
  {                 \
  public: \
    virtual void Run() {_owner->On##name(*this);}

#define END_COMPLEX_MESSAGE(name) \
  };\
  friend name

