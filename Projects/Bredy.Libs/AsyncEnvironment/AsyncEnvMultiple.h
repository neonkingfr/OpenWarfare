#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "asyncenvironment.h"

///Implements asynchronous environment with multiple threads
/** In AsyncEnvMultiple environment, each message has assigned separate thread. This environment can process 
messages concurrently.

In other words, each message is processed in separate thread. Class can be used to easy start the new threads or 
new environments
*/
class AsyncEnvMultiple : public AsyncEnvironment
{  
  friend unsigned int _stdcall ThreadProc(void *data);
public:

  ///Sends message - starts new thread
  /**
  @param message message to process
  */
  virtual void Send(AsyncMessage *message);

  ///Notifies derived class that new thread has been created
  /**
  Derived class should call base implementation, if it don't need store the handle
  @param thread handle of newly created thread
  @param threadId id of newly created thread
  */
  virtual void ThreadStarted(HANDLE thread, DWORD threadID) {CloseHandle(thread);}

  ///Each message starts a new thread. 
  /**This function always returns false, because none message has the same context as
  another
  */
  virtual bool IsMyContext() {return false;}


};
