// FastBlocker.h: interface for the FastBlocker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FASTBLOCKER_H__4C4B1F9A_3902_44F6_B80A_238F3157C6D3__INCLUDED_)
#define AFX_FASTBLOCKER_H__4C4B1F9A_3902_44F6_B80A_238F3157C6D3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class FastBlocker  
{
public:
	FastBlocker();
	virtual ~FastBlocker();

};

#endif // !defined(AFX_FASTBLOCKER_H__4C4B1F9A_3902_44F6_B80A_238F3157C6D3__INCLUDED_)
