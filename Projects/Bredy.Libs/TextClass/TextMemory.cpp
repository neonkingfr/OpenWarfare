#define WIN32_LEAN_AND_MEAN
#include "TextCommonHeader.h"
#include <Windows.h>
#include "TextCProxy.h"
#include ".\textmemory.h"
#include <CriticalSection.h>
#define assert Assert
#include <BTree.h>

class TextDefaultMemory: public ITextMemory
{
#ifdef _MT
  CriticalSection _textMemGuard;
  class TextMemGuard
  {
    CriticalSection &g;
  public:
    TextMemGuard(CriticalSection &g):g(g)
    {
      g.Lock();
    }
    ~TextMemGuard()
    {
      g.Unlock();
    }
  };

#endif

#ifdef _DEBUG
  
  unsigned __int64 _maxBytesAllocated;    //max bytes allocated at time
  unsigned __int64 _curBytesAllocated;    //cur bytes allocated
  unsigned __int64 _totalBytesAllocated;  //total bytes allocated from memory pool
  unsigned __int64 _totalBytesRestored;  //total bytes restored from memory cache
  unsigned int _totalAllocRequests;  
  unsigned int _totalCachedRequests; 
  unsigned int _totalProxyAllocated;  //total proxies allocated
  unsigned int _totalProxyUsed;  //total proxies used


#endif
  enum Constants{
	MaxChunkedMemory=1024,
	ChunkStep=32,
	MaxChunks=MaxChunkedMemory/ChunkStep
  };
  int _maxMemoryPerChunk;
  void *_memorychunks[MaxChunks];
  size_t _memoryusage[MaxChunks];
  TextProxy *_freeProxyList;

  struct ShareDBItem
  {
    TextProxy *_str;
    ShareDBItem(TextProxy *str=NULL):_str(str) {}
    int Compare(const ShareDBItem& other) const 
	  {
	  if (_str==NULL) return other._str!=NULL?1:0;
	  if (other._str==NULL) return -1;
	  return _tcscmp(_str->_redirect,other._str->_redirect);
	  }

    bool operator==(const ShareDBItem& other) const {return Compare(other)==0;}
    bool operator>=(const ShareDBItem& other) const {return Compare(other)>=0;}
    bool operator<=(const ShareDBItem& other) const {return Compare(other)<=0;}
    bool operator!=(const ShareDBItem& other) const {return Compare(other)!=0;}
    bool operator>(const ShareDBItem& other) const {return Compare(other)>0;}
    bool operator<(const ShareDBItem& other) const {return Compare(other)<0;}
  };

  BTree<ShareDBItem> _shareDB;


public:
  virtual void *AllocProxy(const CharT * text=0, size_t countChars=-1);
  virtual void FreeProxy(void *proxy);
  virtual TextProxy *ShareString(TextProxy *proxy);
  virtual void UnshareString(TextProxy *proxy);  
  TextDefaultMemory();
  ~TextDefaultMemory();

};


#pragma init_seg(lib)
static TextDefaultMemory GTextDefaultMemory;
ITextMemory *GCurrentTextMemoryManager=&GTextDefaultMemory;



#ifdef _MT
  #define GUARDEDSECTION TextMemGuard ___guard(_textMemGuard);
#else
  #define GUARDEDSECTION
#endif


TextDefaultMemory::TextDefaultMemory()
{
  _maxMemoryPerChunk=256*1024/MaxChunks;
  memset(_memorychunks,0,sizeof(_memorychunks));
  memset(_memoryusage,0,sizeof(_memoryusage));
  _freeProxyList=0;
#ifdef _DEBUG
  _maxBytesAllocated=0;    //max bytes allocated at time
  _curBytesAllocated=0;    //max bytes allocated at time
  _totalBytesAllocated=0;  //total bytes allocated from memory pool
  _totalBytesRestored=0;  //total bytes restored from memory cache
  _totalProxyAllocated=0;  //total proxies allocated
  _totalProxyUsed=0;  //total proxies used at time
  _totalAllocRequests=0;  
  _totalCachedRequests=0; 
#endif
}

void *TextDefaultMemory::AllocProxy(const CharT * text, size_t countChars)
{
  GUARDEDSECTION;
  if (text==0 && countChars==-1)
  {
#ifdef _DEBUG
    _totalProxyUsed++;
#endif
    if (_freeProxyList==0) 
    {
#ifdef _DEBUG
      _totalProxyAllocated++;
#endif
      return malloc(sizeof(TextProxy));
    }
    else
    {
      TextProxy *z=_freeProxyList;
      _freeProxyList=z->_baseString;
      return z;
    }
  }
  else
  {
    void *p;
    if (countChars==-1) countChars=_tcslen(text);
    int totalmem=(countChars+1)*sizeof(CharT)+sizeof(TextProxy);
    if (totalmem>MaxChunkedMemory) 
      p=malloc(totalmem);
    else
    {
      int chunk=(totalmem-1)/ChunkStep;
      void *chk=_memorychunks[chunk];
      if (chk==0) 
        p=malloc((chunk+1)*ChunkStep);
      else
      {
#ifdef _DEBUG
      _totalBytesRestored+=totalmem;
      _totalCachedRequests++;
#endif
      _memorychunks[chunk]=*reinterpret_cast<void **>(chk);
      _memoryusage[chunk]-=(chunk+1)*ChunkStep;
      p=chk;
      }
    }
#ifdef _DEBUG
    int ss=_msize(p);
    _totalBytesAllocated+=ss;
    _curBytesAllocated+=ss;
    if (_curBytesAllocated>_maxBytesAllocated) _maxBytesAllocated=_curBytesAllocated;
    _totalAllocRequests++;
#endif
    CharT *t=reinterpret_cast<CharT *>(reinterpret_cast<char *>(p)+sizeof(TextProxy));
    if (text)
      _tcsncpy(t,text,countChars);
    t[countChars]=0;
    return p;
  }
}

void TextDefaultMemory::FreeProxy(void *proxy)
{
  GUARDEDSECTION;
  TextProxy *z=reinterpret_cast<TextProxy *>(proxy);
  size_t sz=_msize(proxy);
  if (z->_operation!=z->OpMemBlck || (z->_blockData2==0 && z->_blockData!=0)) 
  {
    TextProxy *z=reinterpret_cast<TextProxy *>(proxy);
    z->_baseString=_freeProxyList;
    _freeProxyList=z;
  }
  else
  {
    if (z->_blockData2 & 0x80) UnshareString(z);
#ifdef _DEBUG
    _curBytesAllocated-=sz;
#endif
    int chunkd=sz/ChunkStep-1;
    if (chunkd<0 || chunkd>=MaxChunks) {free(proxy);}
    else
    {
      if (_memoryusage[chunkd]>=(unsigned)_maxMemoryPerChunk) 
        {free(proxy);}
      else
      {
        _memoryusage[chunkd]+=sz;
        void **ptr=reinterpret_cast<void **>(proxy);
        *ptr=_memorychunks[chunkd];
        _memorychunks[chunkd]=proxy;
      }
    }
  }
  
}

TextDefaultMemory::~TextDefaultMemory()
{
  GUARDEDSECTION;
  while (_freeProxyList)
  {
    void *q=_freeProxyList;
    _freeProxyList=_freeProxyList->_baseString;
    free(q);
  }
  for (int i=0;i<MaxChunks;i++)
  {
    while (_memorychunks[i])
    {
       void *chk=_memorychunks[i];
      _memorychunks[i]=*reinterpret_cast<void **>(chk);
      free(chk);
    }
  }

#ifdef _DEBUG
  char buff[2048];
  sprintf(buff,"Text Memory Statistics:\r\n"
    "Max Bytes used at time:      %15I64d\r\n"
    "Total bytes requested:       %15I64d\r\n"
    "Total bytes restored:        %15I64d\r\n"
    "Memory fysical allocation:   %15I64d\r\n"
    "Total text alloc.requests:   %15d\r\n"
    "Cached text alloc. requests: %15d\r\n"
    "Text malloc requests:        %15d\r\n"
    "Max proxy used at time:      %15d\r\n"
    "Total proxy allocated:       %15d\r\n"
    "Total memory allocation:     %15I64d\r\n"
    "Total malloc request:        %15d\r\n",
    _maxBytesAllocated,
    _totalBytesAllocated,
    _totalBytesRestored,
    _totalBytesAllocated-_totalBytesRestored,
    _totalAllocRequests,
    _totalCachedRequests,
    _totalAllocRequests-_totalCachedRequests,
    _totalProxyUsed,
    _totalProxyAllocated,
    (__int64)(_totalBytesAllocated+(__int64)sizeof(TextProxy)*(__int64)_totalProxyUsed),
    _totalAllocRequests-_totalCachedRequests+_totalProxyUsed);

    OutputDebugStringA(buff);
#endif
}

TextProxy *TextDefaultMemory::ShareString(TextProxy *proxy)
{
  if (proxy->_operation!=proxy->OpMemBlck || proxy->_blockData2==0 || (proxy->_blockData2 & 0x80)!=0) return proxy;

  GUARDEDSECTION;


  proxy->_blockData2|=0x80;	  //block is subject of sharing
  proxy->_redirect=proxy->GetStringFromMemBlock(); //setup pointer to string
  ShareDBItem *found=_shareDB.Find(ShareDBItem(proxy)); 
  if (found) {proxy->_blockData2&=~0x80;proxy=found->_str;}
  else _shareDB.Add(ShareDBItem(proxy));

  return proxy;
}

void TextDefaultMemory::UnshareString(TextProxy *proxy)
{
  if (proxy->_operation!=TextProxy::OpMemBlck || (proxy->_blockData2 & 0x80)==0) return;
  GUARDEDSECTION;
  _shareDB.Remove(ShareDBItem(proxy));  
  proxy->_blockData2&=~0x80;
}
