#pragma once

class TextProxy;
class ITextMemory
{
public:
  ///Allocates space for proxy. Returned pointer is used in new operator
  virtual void *AllocProxy(const CharT * text=0, size_t countChars=-1)=0;
  ///Releases proxy
  virtual void FreeProxy(void *proxy)=0;
  
    ///Function allows sharing the strings
  /** This function is called by Text::Share function
  Function mark proxy shared. If string is same as shared,
  it returns pointer to proxy with proxy of string that contains
  the same text.
  Read description of Text::Share for more informations
  */
  virtual TextProxy *ShareString(TextProxy *proxy)=0;

  ///Function disables sharing of the string
  virtual void UnshareString(TextProxy *proxy)=0;  
};

extern ITextMemory *GCurrentTextMemoryManager;

