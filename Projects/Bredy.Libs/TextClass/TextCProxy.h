// TextProxy.h: interface for the TextProxy class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WSTRINGPROXY_H__863ADE82_7789_4E54_BE7D_B8F0740FD81B__INCLUDED_)
#define AFX_WSTRINGPROXY_H__863ADE82_7789_4E54_BE7D_B8F0740FD81B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TextCommonHeader.h"
#include "TextMemory.h"
#include "ITextEffect.h"
#include <Synchronized.h>

/*
List of proxy types

Memory_Proxy      _operation=OpMemBlck
                  _blockData==0 and _blockData2==0
                  or 
                  _blockData!=0 and _blockData2!=0
				  (_redirect - debug pointer)
				  (_blockData - 1)

Proxy_Const       _operation=OpMemBlck
                  _redirect - pointer to string
                  _blockData2=0

Proxy_Shared      _operation=OpMemBlck
                  _redirect - pointer to string
                  _blockData2=0x80;

Proxy_Immediate   _operation=OpMemBlck
                  both parameters are used by immediate memory manager

Proxy_Concat      _operation=OpConcat
                  _baseString and _secondString is valid

Proxy_Link        _operation=OpSubstr
                  _baseString is valid
                  _stringSize==_baseString->_stringSize
                  _offset==0;
                  NOTE: Can be used without rendering

Proxy_RightSub    _operation=OpSubstr
                  _baseString is valid
                  _offset<=_baseString->_stringSize
                  _stringSize==_baseString->_stringSize-_offset
                  NOTE: Can be used without rendering

Proxy_SubString   _operation=OpSubstr
                  _baseString is valid
                  _offset<=_baseString->_stringSize
                  _stringSize<=_baseString->_stringSize-_offset

Proxy_Effect      _operation=OpEffect
                  _baseString is valid
                  _stringSize=_baseString->_stringSize
                  _effect defining effect code < 0xFFFF

Proxy_UserEffect  _operation=OpEffect
                  _baseString is valid
                  _stringSize=_baseString->_stringSize
                  _effect>0xFFFF
                  _userEffect defining pointer to effect interface

*/

#define TEXTPROXF_REDIRECT 0 ///<redirect points to string (default)
#define TEXTPROXF_DIRECTVALUE 1 ///<string follows proxy immediately 
#define TEXTPROXF_MBSTRING 2 ///Multibyte version follows after unicode string
#define TEXTPROXF_SHARED 0x80 ///String is registred for sharing


class TextProxy  
{
public:
  enum Operation 
  {
	OpConcat=0,	//proxy contains two links to con cat it
	OpSubstr=1,	//proxy contains link to another proxy and specified substring
	OpMemBlck=-2,	//proxy contains informations about following string
	OpEffect=-1,	//proxy describing some effect with string
  };

  enum Effect
  {
	EfLower,	//effect lower case
	EfUpper,	//effect upper case
	EfReverse,	//effect reverse string
  };

  union
  {
	const CharT * _redirect;   //used for OpMemBlock, when _blockData2 is zero.
	TextProxy *_baseString;	//pointer to next proxy referenced by this proxy
	unsigned long _blockData;	//user defined block data for OpMemBlock proxy type
  };

  SyncInt _refCount;  //reference count
  unsigned long _stringSize:30;  //string size in characters (maximum size 1073741823 characters ~ 2147483646 bytes)
  Operation _operation:2;		//operation with string or proxy type


  union
  {
	TextProxy *_secondString;  //pointer to second string for OpConcat
	unsigned long _offset;		  //offset of substring for OpSubstr
	Effect _effect;				  //effect selector for OpEffect
	ITextEffect *_userEffect;  //user effect defined by IWStringEffect interface (valid when _effect is invalid)
	unsigned long _blockData2;	//user defined block data for OpMemBlock proxy type - exception: if this value is zero, member _redirect is valid
	
  };

  void FastRenderString(CharT *renderPtr, size_t offset, size_t size);

  void RenderStringToBuffer(CharT * renderPtr);
  void RenderStringToBufferSubstr(CharT * renderPtr, size_t offset, size_t size);
  TextProxy *TransitivniUzaver();
  void RenderSimpleEffect(CharT * renderPtr);

public:
  TextProxy():_refCount(0),_operation(OpMemBlck),_baseString(0),_secondString(0) 
    {if (_stringSize!=0) {_blockData2=1;_redirect=reinterpret_cast<const CharT *>(this+1);}}

  TextProxy(TextProxy *other):
	  _refCount(0),
	  _stringSize(other->GetLength()),
      _baseString(other->GetLength()?other:SEmptyStringProxy),
	  _operation(OpSubstr),
	  _offset(0) 
	  {_baseString->AddRef();} 

  TextProxy(TextProxy *other, size_t offset, size_t size):
	  _refCount(0),
	  _stringSize(size),
      _baseString(size?other:SEmptyStringProxy),
	  _operation(OpSubstr),
	  _offset(offset) 
	  {_baseString->AddRef();} 
	  
  TextProxy(TextProxy *a, TextProxy *b):
	  _refCount(0),
	  _stringSize(a->GetLength()+b->GetLength()),
      _baseString(a),
	  _operation(OpConcat),
      _secondString(b)
	  {_baseString->AddRef();_secondString->AddRef();} 

  TextProxy(TextProxy *a, Effect effect):
	  _refCount(0),
	  _stringSize(a->GetLength()),
      _baseString(a),
	  _operation(OpEffect),
	  _effect(effect)
	  {_baseString->AddRef();} 

  TextProxy(TextProxy *a, ITextEffect *userEffect):
	  _refCount(0),
	  _stringSize(a->GetLength()+userEffect->GetEffectExtraSize(a->GetLength())),
	  _baseString(a),
	  _operation(OpEffect),
	  _userEffect(userEffect)
	  {_baseString->AddRef();} 

  TextProxy(const CharT *immText):
	  _refCount(0),
	  _stringSize(_tcslen(immText)),
	  _operation(OpMemBlck),
	  _redirect(immText),
	  _blockData2(0)
	  {} 

  TextProxy(const TextProxy &other)
  {
	memcpy(this,&other,sizeof(*this));
	if (_operation!=OpMemBlck) _baseString->AddRef();
	if (_operation==OpConcat) _secondString->AddRef();
  }

  TextProxy& operator=(const TextProxy &other)
  {	
	TextProxy::~TextProxy();			//call destructor to destruct current proxy
	memcpy(this,&other,sizeof(*this));		//construct new proxy from template
	if (_operation!=OpMemBlck) _baseString->AddRef();
	if (_operation==OpConcat) _secondString->AddRef();
  }

  TextProxy *RenderString();

  ~TextProxy()
  {
  	if (_operation!=OpMemBlck) _baseString->Release();
	if (_operation==OpConcat) _secondString->Release();
  }


  unsigned long GetLength() {return _stringSize;}

  void AddRef() 
  {
    if (this) ++_refCount;
  }

  void Release()
  {
    if (this) 
      if (--_refCount==0)
        delete this;
  }

  void *operator new(size_t sz)
  {
    TextProxy *p=reinterpret_cast<TextProxy *>(GCurrentTextMemoryManager->AllocProxy(0,-1));
    p->_stringSize=0;
    return p;
  }

  void *operator new(size_t memsz, const CharT * text, size_t textsz)
  {
    if (textsz==-1) textsz=_tcslen(text);
    TextProxy *p=reinterpret_cast<TextProxy *>(GCurrentTextMemoryManager->AllocProxy(text,textsz));
    p->_stringSize=textsz;
    return p;
  }

  void *operator new( size_t memsz, const CharT * text)
  {
    size_t textsz=_tcslen(text);
    TextProxy *p=reinterpret_cast<TextProxy *>(GCurrentTextMemoryManager->AllocProxy(text,textsz));
    p->_stringSize=textsz;
    return p;
  }

  void operator delete(void *ptr)
  {
    GCurrentTextMemoryManager->FreeProxy(ptr);
  }

  void operator delete(void *ptr, const CharT * text, size_t textsz)
  {
    GCurrentTextMemoryManager->FreeProxy(ptr);
  }

  void operator delete( void *ptr , const CharT * text)
  {
    GCurrentTextMemoryManager->FreeProxy(ptr);
  }


  const CharT * GetString()
  {
	if (_operation==OpMemBlck) return (const CharT * )(this+1);
	TextProxy *p=RenderString();
	(*this)=*p;
	return GetStringFromMemBlock();
  }

  const CharT * GetStringFromMemBlock()
  {
	AssertDesc(_operation==OpMemBlck, "Cannot get string from single proxy. OpMemBlck needed");
	if (_blockData2==0 && _redirect!=NULL) return _redirect;
	else return (const CharT * )(this+1);
  }

  bool IsShared() {return _refCount>1;}

  void RecalcLength()
  {
	const CharT * str=GetStringFromMemBlock();
  	_stringSize=_tcslen(str);	
  }

  static TextProxy *SEmptyStringProxy;

};

#endif // !defined(AFX_WSTRINGPROXY_H__863ADE82_7789_4E54_BE7D_B8F0740FD81B__INCLUDED_)
