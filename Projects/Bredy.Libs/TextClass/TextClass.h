// TextClass.h: interface for the Text class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WSTRING_H__481164FB_3BB7_4824_B0E3_8B371F0AAF3A__INCLUDED_)
#define AFX_WSTRING_H__481164FB_3BB7_4824_B0E3_8B371F0AAF3A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TextCProxy.h"

#define WSTRING_SB_CURRENT_CODE_PAGE 3

class Text  
{
protected:
  mutable TextProxy *_ref;



public:
  ///constructs empty string
  Text():_ref(TextProxy::SEmptyStringProxy) {_ref->AddRef();}

  ///constructs string. 
  explicit Text(const CharT * string):
  _ref(string==NULL||string[0]==0?TextProxy::SEmptyStringProxy:new(string,-1) TextProxy) {_ref->AddRef();};

  ///constructs string from wide-char array with specified length
  Text(const CharT * string, size_t sz):_ref(new(string,sz) TextProxy) {_ref->AddRef();};

  ///copy constructor
  /**constructor doesn't copy the string, only reference. 
  String is shared until it is changed
  */
  Text(const Text &other):_ref(other._ref) {_ref->AddRef();}  

  ///constructs string from string-proxy. Used internally with WStringMemory::AllocXXX functions
  Text(TextProxy *proxy):_ref(proxy) {_ref->AddRef();}

  explicit Text(CharT  c):_ref(0) {CharT  buff[2]={c,0};*this=Text(buff);}

  ///assignment operator
  /** assignment operator doesn't copy the string only reference.
  String is shared until it is changed */
  Text &operator=(const Text &other) 
  {other._ref->AddRef();_ref->Release();_ref=other._ref;return *this;}

  ///Destructor - releases reference
  ~Text() {_ref->Release();}

  ///function converts instance of WString to CharT  pointer
  /**
  Function will perform RenderString, if it is needed.  
  */
  const CharT * GetString() const
  {
    if (_ref->_operation!=TextProxy::OpMemBlck)
    {    
      TextProxy *str=_ref->RenderString();
      if (_ref!=str) 
      {
        str->AddRef();_ref->Release();_ref=str;
      }
    }
    return _ref->GetStringFromMemBlock();
  }

  ///operator can convert WString to CharT  pointer anytime 
  operator const CharT * () const {return GetString();}

  ///function returns count of characters in string
  /** function doesn't need to render string, so it can be called anytime 
  without loosing the benefit of "pseudo-strings" boosting */
  size_t GetLength() const
  {
    if (_ref) return _ref->GetLength();
    else return 0;
  }

  ///function creates string as sum of this and another string
  /** function creates "pseudo-string" that represents sum of two string.
  pseudo-string is rendered to "real-string" automatically, when it is needed */
  Text operator+(const Text &other) const
  {
    if (_ref==TextProxy::SEmptyStringProxy) return other;
    if (other._ref==TextProxy::SEmptyStringProxy) return *this;
    return Text(new TextProxy(TextProxy(_ref,other._ref)));
  }

  Text &operator+=(const Text &other) 
  {
    *this=*this+other;
    return *this;
  }

  ///function creates string as substring of another string
  /** function creates "pseudo-string" that represents substring of anothers string.
  Pseudo-string is rendered to "real-string" automatically, when it is needed */
  Text Mid(size_t begin, size_t len) const
  {
    if (begin==0) return Left(len);
    if (_ref==NULL || begin>GetLength()) return Text();
    if (begin+len>GetLength()) return Right(begin);
    return Text(new TextProxy(TextProxy(_ref,begin,len)));
  }

  Text Left(size_t len) const
  {
    if (_ref==NULL) return Text();
    if (len>=GetLength()) return *this;
    return Text(new TextProxy(TextProxy(_ref,0,len)));
  }

  Text Right(size_t begin) const
  {	  
    if (_ref==NULL || begin>GetLength()) return Text();
    if (begin==0) return *this;
    return Text(new TextProxy(TextProxy(_ref,begin,GetLength()-begin)));
  }

  Text RightR(size_t count) const
  {
    if (_ref==NULL || count==0) return Text();
    if (count>=GetLength()) return *this;
    return Text(new TextProxy(TextProxy(_ref,GetLength()-count,count)));
  }

  Text Delete(size_t begin, size_t count) const
  {
    if (_ref==NULL) return Text();
    if (begin==0) return Right(count);
    if (begin+count>=GetLength()) return Left(begin);
    return Text(new TextProxy(TextProxy(Left(begin)._ref,Right(begin+count)._ref)));
  }

  Text Insert(const Text &what, size_t index) const
  {
    if (_ref==NULL) return what;
    if (index==0) return what+*this;
    if (index>=GetLength()) return *this+what;
    return Left(index)+what+Right(index);
  }

  /// function allows to access any char in string
  CharT  operator[](int index) const
  {
    AssertDesc(index<=(int)GetLength() && index>=0,"Index out of range");
    return GetString()[index];
  }

  /// functions allows lock string for accesing it directly.
  /** Function returns pointer to buffer which holds string.
  Buffer size is equal to string length plus terminating zero.
  Application can modify content of buffer.
  If string is shared with another WString object, LockBuffer
  creates copy of string
  Do not forger to call UnlockBuffer, when you finish modifiing the content
  */
  CharT * LockBuffer()
  {
    if (_ref==NULL) return NULL;
    GetString();
    if (_ref->IsShared() || _ref->_blockData2==0)
    {
      TextProxy *nw=new (_ref->GetStringFromMemBlock()) TextProxy;
      _ref->Release();
      _ref=nw;
      _ref->AddRef();
    }
    return const_cast<CharT * >(_ref->GetStringFromMemBlock());
  }

  /// Function creates buffer for string and returns its address
  /** Application can use buffer in functions, that cannot work with
  WString objects. Size of buffer is specified by sz value, and it is in characters.
  Application must call UnlockBuffer after writes all data into buffer.

  NOTE: Prevoius content of string is lost.
  NOTE: sz specifies buffer size without terminating zero character
  */
  CharT * CreateBuffer(size_t sz)
  {
    _ref->Release();
    _ref=new(NULL,sz+(sz==0)) TextProxy;
    _ref->AddRef();
    return const_cast<CharT * >(_ref->GetStringFromMemBlock());
  }

  /// Function creates buffer, and copies string into it.
  /** Parameter sz specifies size of new buffer in characters
  When sz is smaller then size of string, string is truncated
  When sz is larger then size of string, string is copied unchanged,
  but extra characters can be appended.

  NOTE: sz specifies buffer size without terminating zero character
  */
  CharT * CreateBufferCopy(size_t sz)
  {
    Text save=*this;
    CharT * wbuff=CreateBuffer(sz);
    int minsz=__min(sz,save.GetLength());
    _tcsncpy(wbuff,save.GetString(),minsz);
    return wbuff;
  }

  /// Function unlocks internal buffer
  /** parameter sz specifies final length of string in buffer. Default
  value -1 forces function calculate size by own
  */
  void UnlockBuffer(int sz=-1)
  {
    if (_ref==NULL) return;	
    CharT * wbuff=const_cast<CharT * >(_ref->GetStringFromMemBlock());
    if (sz<0) sz=_tcslen(wbuff);
    else wbuff[sz]=0;
    if (sz!=(signed)_ref->GetLength())
    {
      _ref->RecalcLength();
    }
  }

  class WStringAtCharHelper
  {
    Text &_str;
    size_t _index;
  public:
    WStringAtCharHelper(Text &str,size_t index):_str(str),_index(index) {}
    Text &operator=(CharT  z)
    {
      CharT * buff=_str.LockBuffer();
      AssertDesc(buff && _index<_str.GetLength(),"Index out of range");
      buff[_index]=z;
      _str.UnlockBuffer();
      return _str;
    }
    operator CharT () 
    {
      AssertDesc(_index<_str.GetLength(),"Index out of range");	  
      return ((const CharT * )_str)[_index];
    }
  };

  /// Function will return helper object, which can access single character in string
  /**
  Using array operator is slower than accessing characters by LockBuffer function
  */
  WStringAtCharHelper operator[](int index)
  {
    return WStringAtCharHelper(*this,index);
  }

  int Find(CharT  z,size_t from=0) const
  {
    if (from>=GetLength()) return -1;
    const CharT * res=_tcschr(GetString()+from,z);
    if (res) return res-GetString();
    else return -1;
  }

  int FindLast(CharT  z) const
  {
    const CharT * res=_tcsrchr(GetString(),z);
    if (res) return res-GetString();
    else return -1;
  }

  int Find(const CharT * z,size_t from=0) const
  {
    if (z==NULL) return -1;
    if (from>=GetLength()) return -1;
    const CharT * res=_tcsstr(GetString()+from,z);
    if (res) return res-GetString();
    else return -1;
  }

  int FormatV(const CharT * format, va_list lst);

  int Format(const CharT * format, ...);

  ///Scans string for format
  /** function is limited, item count is limited up to 20 items */
  int ScanStringV(const CharT * format, va_list lst) const;

  ///Scans string for format
  /** function is limited, item count is limited up to 20 items */
  int ScanString(const CharT * format, ...) const;

  bool ReplaceOnce(const Text &findWhat,const Text &replaceWith);

  bool ReplaceAll(const Text &findWhat,const Text &replaceWith);

  Text TrimLeft() const;

  Text TrimRight() const;

  Text TrimLeft(CharT * trimChars) const;

  Text TrimRight(CharT * trimChars) const;

  ///Function splits string into two
  /** Left part of string is returned.
  Right part of string is leaved in object
  @param splitPos split position. 
  @return if splitPos==0, function returns empty string. if splitPos>=length, function
  moves string from object to result*/
  Text Split(size_t splitPos)
  {
    Text result=Left(splitPos);
    *this=Right(splitPos);
    return result;
  }

  bool IsEmpty() const {return _ref==NULL;}

  void Empty() const {_ref->Release();_ref=NULL;}

  int Compare(const CharT * other) const
  {
    return _tcscmp(*this,other);
  }

  bool operator>(const Text &other) const {return Compare(other)>0;}
  bool operator<(const Text &other) const {return Compare(other)<0;}
  bool operator>=(const Text &other) const {return Compare(other)>=0;}
  bool operator<=(const Text &other) const {return Compare(other)<=0;}
  bool operator!=(const Text &other) const {return Compare(other)<=0;}
  bool operator==(const Text &other) const {return Compare(other)<=0;}

  Text Upper() const;
  Text Lower() const;
  Text Reverse() const;

  ///Applies user effect on string
  /** pointer should be static allocated object, or must be valid during lifetime of resulting string */
  Text Effect(ITextEffect *effect) const;



  ///Function reads string from stream
  /** 
  Function calls streamReader repeatly until function returns zero. In each call, streamReader returns
  number of characters, that has been readed. 
  @param streamReader pointer to function which provides reading from a stream
  @param context user defined context pointer (most in cases, pointer to stream is passed)
  @param buffer space allocated for data readed from stream
  @param bufferLen maximum count of characters can be written to buffer
  @return streamReader returns number of characters, that has been written to buffer. Function must return zero
  to stop reading
  */
  void ReadFromStream(size_t (*streamReader)(CharT * buffer, size_t bufferLen, void *context),void *context);

  Text operator() (int from) const {return from<0?RightR(-from):Right(from);}
  Text operator() (int from, int to) const 
  {
    if (from>=0)  
      return to<0?Mid(from,-to):Mid(from,to-from);
    else
      return to<0?Mid(GetLength()+from,-to):Mid(GetLength()+from,GetLength()-from-to);  
  }

  ///Enables global string sharing.
  /** Function Share allows share strings that contains same text. It can reduce memory
  usage. Function is useful when there is many objects that contains string with same text.

  Basically, two strings is shared, if one string is created as copy of another string. Strings 
  is sharing they content, until one of them is modified. 

  Calling Share function, string is registered for global sharing. If string with same content is already 
  registered, then content of current string is released, and string is shared. 

  To successfully share two strings, both strings must call Share() function. Sharing is provided until one 
  of strings is modified. After modifications are done, Share function of modified string must be called again.

  Remember: Global sharing can reduce amount of memory, if there are multiple strings that containging 
  the same content. But sharing of each unique string takes small piece of memory to store sharing 
  informations.

  To share string automatically, use WSharedString class
  WSC strings cannot be shared!
  */
  void Share() const
  {
    TextProxy *curref=_ref;
    GetString();
    _ref=GCurrentTextMemoryManager->ShareString(curref);
    _ref->AddRef();
    curref->Release();
  }

  ///Disables global string sharing
  /** Function unregisters string from global sharing database. If string is shared, function doesn't break 
  it. But all strings that currently sharing content will not be shared with newly created strings. 
  */
  void Unshare()
  {
    GCurrentTextMemoryManager->UnshareString(_ref);
  }

  template <class Archive>
    void Serialize(Archive &arch)
  {
    if (+arch)
    {
      CharT * data=const_cast<CharT * >(this->GetString());
      arch(data);
    }
    else 
    {
      CharT * data=0;
      arch(data);
      (*this)=Text(data);
    }
  }

  ///Forces class to render string 
  void Render()
  {
    GetString();
  }

  enum FormatAlign
  {
    FormatAlignLeft=0,
    FormatAlignRight=2,
    FormatAlignCenter=1,
  };

  ///Formats text
  /**
  @param space space of characters to fit string
  @param align Align of text
  @param fillchar Character to fill empty space
  @param crop enables cropping, if string doesn't fit space
  @return result of operation
  */
  Text Format(size_t space, FormatAlign align=FormatAlignLeft, bool crop=false, CharT fillchar=' ');
};


class SharedText: public Text
{
public:
  SharedText() {}

  ///constructs string. 
  explicit SharedText(const CharT * string):Text(string) {Share();}

  ///constructs string from wide-char array with specified length
  SharedText(const CharT * string, size_t sz):Text(string,sz) {Share();};

  ///copy constructor
  /**constructor doesn't copy the string, only reference. 
  String is shared until it is changed
  */
  SharedText(const SharedText &other):Text(other) {}
  SharedText(const Text &other):Text(other) {Share();}

  ///assignment operator
  /** assignment operator doesn't copy the string only reference.
  String is shared until it is changed */
  SharedText &operator=(const Text &other) 
  {Text::operator =(other);Share();return *this;}

  const CharT * GetString() const
  {
    const CharT * res=Text::GetString();
    Share();
    return res;
  }
  operator const CharT * () const {return GetString();}
  CharT  operator[](int index) const
  {
    AssertDesc(index<=(int)GetLength() && index>=0, "Index out of range");
    return GetString()[index];
  }

  void UnlockBuffer(int sz=-1)
  {
    Text::UnlockBuffer(sz);
    Share();	
  }

};

///provides MultiByte Text 
/** It useful only for constructing Text class from multibyte text. 
In multibyte environment, this class does nothing
*/
class MBText: public Text
{
#ifdef _UNICODE
  long _codePage; ///<code page of text data for conversion back or forward
#endif

public:
  MBText(const Text &other):Text(other) {SetCodePage(-1);}
  MBText(const Text &other, int codepage):Text(other) {SetCodePage(codepage);}
  MBText():Text() 
  {
    SetCodePage(-1);
  }
#ifdef _UNICODE
  ///Function constructs Text from multibyte text. 
  /**
  @param mbtext pointer to multibyte text
  @param codepage ID of codepage (see MultiByteToWideChar). Default value will use application current code page*/
  explicit MBText(const char *mbtext, size_t charsz=-1, long codepage=-1);  

  ///Enables access to text by multibyte libraries.
  /*
  @copydoc MBText::GetString
  */
  operator const char *() const {return GetString();}

  ///Enables access to text by multibyte libraries.
  /**Note, function creates copy of string only first time. It can cause problem, when 
  buffer is locked and modified outside.

  @return pointer to text, pointer is valid until value of this object is changed.
  */
  const char *GetString() const;

  ///Enables to multibyte library modify multibyte text in buffer
  /** Function will use GetString and then locks buffer for writting. Returned pointer
  can be used to modify data.
  @note don't use GetString during object is locked. It can have unpredictable result.
  Don't create copy of locked object.
  @return pointer to locked buffer
  */
  char *LockBuffer();

  ///Enables to multibyte library fill multibyte text into buffer
  /** Function allocates buffer in specified size for multibyte version of string.
  @note don't use GetString during object is locked. It can have unpredictable result
  Don't create copy of locked object.
  @param sz number of bytes to allocate (not characters, some charactes can take multiple bytes)
  except terminating zero
  @return pointer to allocated buffer
  */
  char *CreateBuffer(size_t sz);

  ///Enables to multibyte library fill multibyte text into buffer
  /** Function allocates buffer in specified size for multibyte version of string.
  Function also copies previous text to buffer up to sz bytes. If buffer is larger, text is
  terminated by zero bud there is still space in buffer for usage
  @note don't use GetString during object is locked. It can have unpredictable result
  Don't create copy of locked object.
  @param sz number of bytes to allocate (not characters, some charactes can take multiple bytes)
  except terminating zero
  @return pointer to allocated buffer
  */
  char *CreateBufferCopy(size_t sz);

  ///Unlocks buffer and converts the text
  /**
  Function converts text from multibyte into unicode.
  @param sz new size of text in bytes. Use default value, when text is terminated by zero.
  */

  void UnlockBuffer(int sz=-1);

  void SetCodePage(int codePage);
  int GetCodePage() {return _codePage;}

#else

  MBText(const char *mbtext, size_t charsz=-1, long codepage=-1): Text(mbtext,charsz) {}

  void SetCodePage(int codePage) {}
  int GetCodePage() {return -1;}

#endif
  MBText operator=(const char *text) {(*this)=MBText(text,-1,GetCodePage());return *this;}

};


///Class is used to create Text object as pointer to static text
class TextRef: public Text
{
public:
  TextRef(const CharT *text):Text(new TextProxy(text)) {}
  TextRef &operator=(const CharT *text) {Text::operator =(TextRef(text));return *this;}  
};


template<class CompareTraits>
class TextCmp: public Text
{
public:
  TextCmp(const Text &other): Text(other) {}
  TextCmp(const CharT *text): Text(text) {}
  TextCmp(const CharT *text, size_t sz): Text(text,sz) {}


  bool operator>(const Text &other) const {return CompareTraits::Compare(GetString(),other)>0;}
  bool operator<(const Text &other) const {return CompareTraits::Compare(GetString(),other)<0;}
  bool operator>=(const Text &other) const {return CompareTraits::Compare(GetString(),other)>=0;}
  bool operator<=(const Text &other) const {return CompareTraits::Compare(GetString(),other)<=0;}
  bool operator!=(const Text &other) const {return CompareTraits::Compare(GetString(),other)<=0;}
  bool operator==(const Text &other) const {return CompareTraits::Compare(GetString(),other)<=0;}
};

class TextCompareTraitsCaseInsensitive
{
public:
  static int Compare(const CharT *left, const CharT * right)
  {
    return _tcsicmp(left,right);
  }
};

typedef TextCmp<TextCompareTraitsCaseInsensitive> TextICmp;

#define TSC(x) TextRef(_T(x))

class TextInt: public Text
{
public:
  TextInt(int value, int radix=10)
  {
    char buff[33];
    itoa(value,buff,radix);
    _ref=new(buff) TextProxy;_ref->AddRef();
  }
  TextInt(const Text &other): Text(other) {}

  int operator() (bool &success) const
  {
    int res;
    success=sscanf(GetString(),"%d",&res)==1;
    return res;
  }
  operator int() const
  {
    bool succ;
    return (*this)(succ);
  }
};

class TextUInt: public Text
{
public:
  TextUInt(unsigned int value, int radix=10)
  {
    char buff[33];
    _ui64toa(value,buff,radix);
    _ref=new(buff) TextProxy;_ref->AddRef();
  }
  TextUInt(const Text &other): Text(other) {}

  unsigned int operator() (bool &success) const
  {
    unsigned int res;
    success=sscanf(GetString(),"%u",&res)==1;
    return res;
  }
  operator unsigned int() const
  {
    bool succ;
    return (*this)(succ);
  }
};

class TextFloat: public Text
{
public:
  TextFloat(double value)
  {
    char buff[128];
    sprintf(buff,"%g",value);
    _ref=new(buff) TextProxy;_ref->AddRef();
  }

  TextFloat(double value, int decimals)
  {
    char buff[128];
    char format[50];
    sprintf(format,"%%1.%df",decimals);
    sprintf(buff,format,value);
    _ref=new(buff) TextProxy;_ref->AddRef();
  }

  TextFloat(const Text &other): Text(other) {}

  float operator() (bool &success) const
  {
    float res;
    success=sscanf(GetString(),"%f",&res)==1;
    return res;
  }
  operator float() const
  {
    bool succ;
    return (*this)(succ);
  }
};

#endif // !defined(AFX_WSTRING_H__481164FB_3BB7_4824_B0E3_8B371F0AAF3A__INCLUDED_)
