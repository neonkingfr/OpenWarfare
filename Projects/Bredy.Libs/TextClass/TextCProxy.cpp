// TextProxy.cpp: implementation of the TextProxy class.
//
//////////////////////////////////////////////////////////////////////


#include "TextCommonHeader.h"
#include "TextCProxy.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static unsigned long SEmptyProxyDef[]=
{
  0, //redirect=0
    1, //refcount=1
    0x80000000, //stringSize=0, operation=OpMemBlock
    1, //flags = 1
    0, //an empty string;
};

TextProxy *TextProxy::SEmptyStringProxy=reinterpret_cast<TextProxy *>(SEmptyProxyDef);

/** Funkce vytvo�� transitivn� uz�v�r.
to znamen�, �e zru�� v�echny z�et�zen� redirecty.
*/
TextProxy *TextProxy::TransitivniUzaver()
{
  if (_operation==OpSubstr && _offset==0 && _stringSize==_baseString->GetLength())
  {
    TextProxy *next=_baseString->TransitivniUzaver();
    if (next==NULL) return this;
    next->AddRef();
    _baseString->Release();
    _baseString=next;
    return next;
  }
  return NULL;
}

/** Main render procedure
It creates allocates buffer proxy and render tree into it
Then turns self into redirect proxy a redirect self into newly created string
Returns pointer to newly created proxy
*/
TextProxy *TextProxy::RenderString()
{
  if (_operation==OpMemBlck) return this;

  TextProxy *pp=TransitivniUzaver();
  if (pp!=NULL) return pp->_baseString->RenderString();

  TextProxy *newProxy=new(0,_stringSize) TextProxy;
  CharT * renderPtr=const_cast<CharT * >(newProxy->GetStringFromMemBlock());

  FastRenderString(renderPtr,0,_stringSize);

  newProxy->AddRef();
  renderPtr[_stringSize]=0;
  this->~TextProxy();
  _operation=OpSubstr;
  _offset=0;
  _baseString=newProxy;

  return newProxy;
}

/** Fast render routine.
It processes all proxies and fills up buffer by result
*/
void TextProxy::FastRenderString(CharT *renderPtr, size_t offset, size_t size)
{
  size_t tmp;
  TextProxy *prox;
  switch(_operation)
  {
  case OpConcat:
    tmp=_baseString->GetLength();
    if (tmp>offset) 
    {
      _baseString->FastRenderString(renderPtr,offset,size);    
      tmp-=offset;
      if (tmp<size) _secondString->FastRenderString(renderPtr+tmp,0,size-tmp);
    }
    else
    {
      offset-=tmp;
      _secondString->FastRenderString(renderPtr,offset,size);
    }
    break;

  case OpSubstr:
    prox=_baseString;
    offset+=_offset;
    while (prox->_operation==OpSubstr)
    {
      offset+=prox->_offset;
      prox=prox->_baseString;
    }
    prox->FastRenderString(renderPtr,offset,size);
    break;

  case OpMemBlck:
    {
      //rendering from memory, final stop in recursion
      //Get pointer string 
      const CharT * str=GetStringFromMemBlock();
      //Calculate real string size
      tmp=GetLength()-offset;
      if (tmp<size) size=tmp;
      //copy substring from string into render buffer
      _tcsncpy(renderPtr,str+offset,size);
    };
    break;

  case OpEffect:	
    {
      if (offset!=0 || size<_stringSize)
      {
        RenderString();
        FastRenderString(renderPtr,offset,size);
      }
      else
      {
        unsigned long offset=0;
        renderPtr[_stringSize]=0;		  //we can append zero, because right side of string is not yet rendered  
        //if this is end of string, one extra character for zero is also allocated.
        //efect functions can rely on it.
        if (_blockData2>=256) offset=_userEffect->PreRenderString(renderPtr,_baseString->GetLength()); //call prerender to prepare begin of buffer
        _baseString->FastRenderString(renderPtr+offset,0,_baseString->GetLength());		//render string to rest of buffer
        if (_blockData2>=256) _userEffect->RenderString(renderPtr,_baseString->GetLength()); //apply effect to buffer
        else RenderSimpleEffect(renderPtr);
      }
    }
    break;
  }
}


/** Function renders simple effect into buffer
Function needs buffer with nul terminated string
*/
void TextProxy::RenderSimpleEffect(CharT * renderPtr)
{
  AssertDesc(_operation==OpEffect,"Cannot render effect on non-effect proxy");
  switch (_effect)
  {
  case EfLower: _tcslwr(renderPtr);break;
  case EfUpper: _tcsupr(renderPtr);break;
  case EfReverse: _tcsrev(renderPtr);break;
  }

}

