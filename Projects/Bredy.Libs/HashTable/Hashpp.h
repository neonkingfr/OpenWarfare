// Hashpp.h: interface for the CHashTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HASHPP_H__6E37D786_04DB_11D5_B399_00C0DFAE7D0A__INCLUDED_)
#define AFX_HASHPP_H__6E37D786_04DB_11D5_B399_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef unsigned long IHASH ;

class CHashKey	//Abstraktni trida pro hasovaci klice
{
  public:
    static int CompareBinary(const void *data1, const void *data2, int size);
    static int CompareStringsN(const char *str1, const char *str2, int size);
    static int CompareStrings(const char *str1, const char *str2);
    static int CompareStringsI(const char *str1, const char *str2);
    static IHASH GetKeyFromStringN(const char *string, int size, IHASH module);
    static IHASH CombineKeys(int keys, IHASH module, ...);
    static IHASH GetKeyFromString(const char *string, IHASH module);
    static IHASH GetKeyFromStringI(const char *string, IHASH module);
    static IHASH GetKeyFromBinary(const void *ptr, int size, IHASH module);	  
    virtual IHASH GetHashIndex(IHASH module)=0; //pocita klic
    virtual IHASH GetHashIndex(IHASH module, int keyindex) 
    {return GetHashIndex(module);}
    virtual void *GetThis()=0;  //Vraci ukazatel na potomka, ktery dedi tuto tridu
    virtual int CompareKeys(CHashKey *data)=0; //Vraci true, pokud jsou klice stejne
    virtual int CompareKeys(CHashKey *data, int keyindex) 
    {return CompareKeys(data);}
    virtual void DestroyKey()=0; //Vola tabulka, kdyz nici klic (a s nim polozku)
};

class CHashIterator;

class CHashTable  
{
  friend CHashIterator;
  protected:
    int keyindex;
  public:
    virtual bool AddKey(CHashKey *key)=0;
    virtual CHashIterator *BeginSearch(CHashKey *key)=0;
    virtual bool Resize(IHASH items)=0;
    void *Find(CHashKey& key);
    void Flush();
    void *operator[] (CHashKey &key) 
    {return Find(key);}
    virtual IHASH GetSize()=0;
    
    int GetKeyIndex() 
    {return keyindex;}
    void SetKeyIndex(int index) 
    {keyindex=index;}
  protected:
    virtual void *FindNext(CHashIterator *it)=0;
    virtual void EndSearch(CHashIterator *it)=0;
    virtual CHashKey *RemoveKey(CHashIterator *it)=0;
    virtual bool KillKey(CHashIterator *it)=0;
    virtual void *GetItem(CHashIterator *it)=0;
};

class CHashIterator
{  
  CHashTable &srctable;
  CHashKey *key;
  bool destroykey;
  public:
    CHashIterator(CHashTable &src, CHashKey *key):srctable(src),key(key),destroykey(false) 
    {}
    void *FindNext() 
    {return srctable.FindNext(this);}
    void EndSearch() 
    {if (destroykey && key) key->DestroyKey();srctable.EndSearch(this);}
    bool KillItem() 
    {return srctable.KillKey(this);}
    CHashKey *RemoveItem() 
    {return srctable.RemoveKey(this);}
    void *GetItem() 
    {return srctable.GetItem(this);}
    CHashKey *GetKey() 
    {return key;}
    CHashTable *operator->() 
    {return &srctable;}
    void SetKey(CHashKey *key) 
    {this->key=key;}
    void EnableDestroyKey(bool enable) 
    {destroykey=enable;}
};

class CHashGIterator	// Guarded iterator (ends search at end of scope);
{
  CHashIterator *it;
  public:
    CHashGIterator(CHashIterator *it):it(it) 
    {}
    ~CHashGIterator() 
    {if (it) it->EndSearch();}
    
    void *FindNext() 
    {return it->FindNext();}
    void EndSearch() 
    {it->EndSearch();it=0;}
    bool KillItem() 
    {return it->KillItem();}
    CHashKey *RemoveItem() 
    {return it->RemoveItem();}
    void *GetItem() 
    {return it->GetItem();}
    CHashKey *GetKey() 
    {return it->GetKey();}
    CHashTable *operator->() 
    {return it->operator->();}
    void EnableDestroyKey(bool enable) 
    {it->EnableDestroyKey(enable);}
};

class CHashInsideIterator;

class CHashInside:public CHashTable
{
  CHashKey **table;
  IHASH count;
  CHashInsideIterator *freelist;
  private:
    virtual bool KillKey(CHashIterator *it);
    virtual void * GetItem(CHashIterator *it);
    virtual void *FindNext(CHashIterator *it);
    virtual CHashKey * RemoveKey(CHashIterator *it);
    virtual void EndSearch(CHashIterator *it);
  public:	
    virtual CHashIterator * BeginSearch(CHashKey *key);
    virtual bool Resize(IHASH items);
    virtual bool AddKey(CHashKey *key);
    virtual ~CHashInside();
    virtual IHASH GetSize() 
    {return count;}
    CHashInside(IHASH tabsize=101);
};

class CHashOutsideIterator;

#define CHO_ADDEND 0x1
#define CHO_ADDBEGIN 0x2

class CHashOutside: public CHashTable
{
  struct SHashMini
  {
    SHashMini *next;
    unsigned long refcount;
    CHashKey *list[1];
    public:
      void DeleteList();
      SHashMini * GetLast();
      bool AddAfter(CHashKey *key, int cnt);
      bool AddBefore(CHashKey *key, int cnt);
      SHashMini *FindKey(CHashKey *key, int cnt, int& pos); //vraci ukazatel na ukazatel!!, pokud vrati NULL znamena to, ze ji nezna. pos=-1 znamena nenalezeno
      //pos==-1 hledani od zacatku
  };
  SHashMini *minfreelist;
  CHashOutsideIterator *itrfree;
  SHashMini **table;
  unsigned long flgs;
  int cluster;
  IHASH count;
  void FreeCluster(SHashMini *cluster, bool destroy);
  virtual bool KillKey(CHashIterator *it);
  virtual CHashKey * RemoveKey(CHashIterator *it);
  virtual void EndSearch(CHashIterator *it);
  virtual void * FindNext(CHashIterator *it);
  virtual void * GetItem(CHashIterator *it);
  SHashMini * AlocateCluster();
  public:
    virtual  ~CHashOutside();
    virtual bool Resize(IHASH item);
    virtual CHashIterator * BeginSearch(CHashKey *key);
    virtual bool AddKey(CHashKey *key);
    virtual IHASH GetSize() 
    {return count;}
    CHashOutside(IHASH size=101, int clustersize=8, unsigned long flags=CHO_ADDBEGIN);
};

#endif // !defined(AFX_HASHPP_H__6E37D786_04DB_11D5_B399_00C0DFAE7D0A__INCLUDED_)
