// Hashpp.cpp: implementation of the CHashTable class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
#include "Hashpp.h"
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>



IHASH CHashKey::GetKeyFromBinary(const void *ptr, int size, IHASH module)
{
  IHASH val=0;
  unsigned char *pp=(unsigned char *)ptr;
  while (size--)
  {
    val+=*pp++;
    if (val>=module) val%=module;
  }
  return val;
}

IHASH CHashKey::GetKeyFromString(const char *string, IHASH module)
{
  IHASH val=0;
  while (*string)
  {
    val+=*(unsigned char *)string++;
    if (val>=module) val%=module;
  }
  return val;
}

IHASH CHashKey::GetKeyFromStringI(const char *string, IHASH module)
{
  IHASH val=0;
  while (*string)
  {
    val+=(unsigned char )toupper(*string++);
    if (val>=module) val%=module;
  }
  return val;
}

IHASH CHashKey::CombineKeys(int keys, IHASH module, ...)
{
  va_list list;
  va_start(list,module);
  IHASH ret=0;
  for (int i=0;i<keys;i++)
  {
    ret+=va_arg(list,IHASH);
    if (ret>module) ret%=module;
  }
  return ret;
}

IHASH CHashKey::GetKeyFromStringN(const char *string, int size, IHASH module)
{
  IHASH val=0;
  while (*string && size--)
  {
    val+=*string++;
    if (val>=module) val%=module;
  }
  return val;
}

int CHashKey::CompareStrings(const char *str1, const char *str2)
{
  return strcmp(str1,str2);
}

int CHashKey::CompareStringsI(const char *str1, const char *str2)
{
  return stricmp(str1,str2);
}

int CHashKey::CompareStringsN(const char *str1, const char *str2, int size)
{
  return strncmp(str1,str2,size);
}

int CHashKey::CompareBinary(const void *data1, const void *data2, int size)
{
  return memcmp(data1,data2,size);
}

inline void *operator new(size_t size,void *place) 
{if (place==NULL) return (void *)new char[size]; else return place;}

inline void operator delete(void *pos, void *place) 
{if (place==NULL) free(pos);}

class CHashInsideIterator : public CHashIterator
{
  friend CHashInside;  
  IHASH searchindex;
  CHashInsideIterator *next;  
  CHashInsideIterator(CHashTable &src,CHashKey *key):CHashIterator(src,key) 
  {searchindex=~0;next=0;}
  
};

CHashInside::CHashInside(IHASH tabsize)
{
  table=new CHashKey *[tabsize];
  memset(table,0,sizeof(CHashKey *)*tabsize);
  count=tabsize;
  freelist=NULL;
}

CHashInside::~CHashInside()
{
  Flush();
  delete [] table;
  while (freelist)
  {
    CHashInsideIterator *x=freelist;
    freelist=freelist->next;
    delete x;
  }
}

bool CHashInside::AddKey(CHashKey *key)
{ 
  IHASH map=key->GetHashIndex(count,keyindex);
  IHASH top=map;
  do
  {
    if (table[map]==NULL) break;
    map++;
    if (map>=count) map-=count;
  }
  while (map!=top);
  if (table[map]) return false;
  table[map]=key;
  return true;
}

CHashIterator * CHashInside::BeginSearch(CHashKey *key)
{ 
  CHashInsideIterator *ptr;
  if (freelist) 
  {ptr=freelist;freelist=ptr->next;ptr=new(ptr) CHashInsideIterator(*this,key);}
  else ptr=new CHashInsideIterator(*this,key);
  return ptr;
}

void CHashInside::EndSearch(CHashIterator *it)
{
  CHashInsideIterator *ptr=(CHashInsideIterator *)it;
  ptr->next=freelist;
  freelist=ptr;
  return;
}

void *CHashInside::FindNext(CHashIterator *it)
{
  CHashInsideIterator *ptr=(CHashInsideIterator *)it;
  CHashKey *key=ptr->GetKey();
  if (key==0)
  {
    do
    {
      ptr->searchindex++;
      if (ptr->searchindex>=count) return NULL;
    }
    while (table[ptr->searchindex]==NULL);
    return table[ptr->searchindex]->GetThis();
  }
  else
  {
    IHASH map=key->GetHashIndex(count,keyindex);
    if (ptr->searchindex==~0)
      ptr->searchindex=map;
    if (ptr->searchindex>=count) 
      return NULL;
    do
    {
      if (table[ptr->searchindex]!=NULL && key->CompareKeys(table[ptr->searchindex],keyindex)==0) return table[ptr->searchindex]->GetThis();
      ptr->searchindex++;
      if (ptr->searchindex>=count) ptr->searchindex-=count;
    }
    while (ptr->searchindex!=map)	  ;
  }
  ptr->searchindex=count;
  return NULL;  
}

void * CHashInside::GetItem(CHashIterator *it)
{
  CHashInsideIterator *ptr=static_cast<CHashInsideIterator *>(it);
  if (ptr->searchindex>=count) return NULL;
  return table[ptr->searchindex]->GetThis();
}

bool CHashInside::KillKey(CHashIterator *it)
{
  CHashInsideIterator *ptr=static_cast<CHashInsideIterator *>(it);
  if (ptr->searchindex>=count) return false;
  table[ptr->searchindex]->DestroyKey();
  table[ptr->searchindex]=NULL;
  return true;
}

CHashKey * CHashInside::RemoveKey(CHashIterator *it)
{
  CHashInsideIterator *ptr=(CHashInsideIterator *)it;
  if (ptr->searchindex>=count) return NULL;
  CHashKey *hh=table[ptr->searchindex];
  table[ptr->searchindex]=NULL; 
  return hh;
}

bool CHashInside::Resize(IHASH items)
{
  CHashInside nw(items);				//vyrob novou tabulku
  CHashIterator *it=BeginSearch(NULL);	//prochazej vsechny prvky
  while (it->FindNext())				//pro kazdy prvek
  { 
    CHashKey *k=it->RemoveItem();		//vyjmi prvek z teto tabulky
    if (nw.AddKey(k)==false)			//dej ho do nove tabulky. Neprojde-li to
    {
      CHashIterator *it2=nw.BeginSearch(NULL);	//musis nyni vratit vse do puvodniho stavu
      while (it2->FindNext())			//v nove tabulce najdi vsechny prvky
      {
        CHashKey *p=it2->RemoveItem();	//presun je zpet
        AddKey(p);
      }
      it2->EndSearch();					//ukonci hledani v nove tabulce
      it->EndSearch();					//ukonci hledani ve stare tabulce
      return false;
    }
  }
  delete [] table;						//vymaz aktualni tabulku
  count=nw.count;						//prevezmi tabulku z nove tabulky
  table=nw.table;					
  nw.table=NULL;						//oznac novou tabulku jako neinicalizovanou
  nw.count=0;
  it->EndSearch();						//ukonci hledani
  return true;							//vrat uspech
}

void CHashTable::Flush()
{
  CHashIterator *it=BeginSearch(NULL);
  while (it->FindNext())
  {
    it->KillItem();
  }
  it->EndSearch();
}

void * CHashTable::Find(CHashKey& key)
{
  CHashIterator *it=BeginSearch(&key);
  void *out=it->FindNext();
  it->EndSearch();
  return out;
}

CHashOutside::SHashMini *CHashOutside::SHashMini::FindKey(CHashKey *key, int cnt, int& pos)
{  
  SHashMini *t=this;
  while (t)
  {
    pos++;
    while (pos<cnt)
    {
      if ((key==0 && list[pos]!=0) || (key==list[pos])) return t;
      pos++;
    }
    pos=-1;
    t=t->next;
  }
  return NULL;
}

bool CHashOutside::SHashMini::AddBefore(CHashKey *key, int cnt)
{
  for (int i=0;i<cnt;i++) if (list[i]) break;
  i--;
  if (i<0) return false;
  list[i]=key;  
  return true;
}

bool CHashOutside::SHashMini::AddAfter(CHashKey *key, int cnt)
{
  for (int i=cnt-1;i>=0;i--) if (list[i]) break;
  i++;
  if (i==cnt) return false;
  list[i]=key;
  return true;
}

CHashOutside::SHashMini * CHashOutside::SHashMini::GetLast()
{
  SHashMini *t=this;
  while (t->next!=NULL) t=t->next;
  return t;
}

CHashOutside::CHashOutside(IHASH size, int clustersize, unsigned long flags)
{
  count=size;
  table=new SHashMini *[count];
  memset(table,0,sizeof(SHashMini *)*count);
  cluster=clustersize;
  flgs=flags;
  itrfree=NULL;
  minfreelist=NULL;
}

CHashOutside::SHashMini * CHashOutside::AlocateCluster()
{
  SHashMini *out;
  if (minfreelist==NULL) 
    out=(SHashMini *)malloc(sizeof(SHashMini)+sizeof(out->list[0])*(cluster-1));
  else
  {
    out=minfreelist;
    minfreelist=out->next;
  }
  memset(out->list,0,sizeof(out->list[0])*cluster);
  out->next=NULL;
  out->refcount=1;
  return out;
}

#define ReleaseCluster(cl) if ((--(cl)->refcount)==0) FreeCluster(cl,false)

bool CHashOutside::AddKey(CHashKey *key)
{
  IHASH row=key->GetHashIndex(count,keyindex);
  SHashMini *m=table[row];
  if (m==NULL) 
  {
    table[row]=m=AlocateCluster();
  }
  if (flgs & CHO_ADDEND)
  {
    m=m->GetLast();
    if (m->AddAfter(key,cluster)==false)
    {
      SHashMini *q=AlocateCluster();
      q->AddAfter(key,cluster);
      m->next=q;
    }
  }
  else
  {
    if (m->AddBefore(key,cluster)==false)
    {
      SHashMini *q=AlocateCluster();
      q->AddBefore(key,cluster);
      q->next=m;
      table[row]=q;
    }
  }
  return true;
}

class CHashOutsideIterator: public CHashIterator
{
  friend CHashOutside;
  IHASH row;
  void *cluster;
  int pos;
  CHashOutsideIterator *next;
  CHashOutsideIterator(CHashTable& table, CHashKey *key):
  CHashIterator(table,key)
  {
    row=~0;
    cluster=NULL;
    pos=-1;
  }
};

CHashIterator * CHashOutside::BeginSearch(CHashKey *key)
{
  CHashOutsideIterator *ptr;
  if (itrfree) 
  {ptr=itrfree;itrfree=ptr->next;ptr=new(ptr) CHashOutsideIterator(*this,key);}
  else ptr=new CHashOutsideIterator(*this,key);
  return ptr;
}

void * CHashOutside::FindNext(CHashIterator *it)
{
  CHashOutsideIterator *ptr=(CHashOutsideIterator *)it;
  if (ptr->row>=count && ptr->row!=~0) return NULL; //na konci hledani? Vrat NULL;
  if (ptr->GetKey()==NULL) //ENUMERACE
  {
    SHashMini *next=(SHashMini *)(ptr->cluster);
    do
    {
      ptr->pos++;
      if (ptr->cluster==NULL || ptr->pos>=cluster) //na konci clusteru?
      {
        if (ptr->cluster!=NULL)  //nejprve detachuj cluster
        {
          SHashMini *cl=(SHashMini *)ptr->cluster;
          next=cl->next;
          ReleaseCluster(cl); 
        }
        if (next==NULL) //nova row
        {
          ptr->row++;
          while (ptr->row<count && table[ptr->row]==NULL) ptr->row++;
          if (ptr->row>=count) return NULL;
          next=table[ptr->row];
        }
        ptr->cluster=(void *)next;
        ptr->pos=0;
        next->refcount++; //atachuj cluster		
      }
    }
    while (next->list[ptr->pos]==NULL);
    return next->list[ptr->pos]->GetThis();
  }
  else //HLEDANI
  {
    SHashMini *cl=(SHashMini *)ptr->cluster;
    if (cl==NULL) //prvni hledani
    {
      ptr->row=it->GetKey()->GetHashIndex(count,keyindex);
      cl=table[ptr->row]; //najdi prvni cluster v radce
      if (cl==NULL) //neexistuje-li, urcite neexistuje ani hledany klic
      {
        ptr->row=count;	//oznac konec hledani
        return NULL;  //vrat NULL
      }
      cl->refcount++; //atachuj cluster
    }
    CHashKey *key;
    do
    {
      ptr->pos++;	//posun na dalsi pozici
      if (ptr->pos>=cluster)
      {
        SHashMini *kill=cl;
        cl=cl->next;
        ReleaseCluster(kill); 
        if (cl==NULL) 
        {
          ptr->row=count;
          return NULL;
        }
        cl->refcount++;
        ptr->pos=0;
        ptr->cluster=cl;
      }
      key=cl->list[ptr->pos];
      if (key && key->CompareKeys(ptr->GetKey(),keyindex)==0) 
      {
        ptr->cluster=cl;
        return key->GetThis();	  
      }
    }
    while (true);
  }
}

void CHashOutside::FreeCluster(SHashMini *cluster, bool destroy)
{
  if (destroy) free(cluster);
  else
  {
    if (cluster->next) 
      ReleaseCluster(cluster->next); 
    cluster->next=minfreelist;
    minfreelist=cluster;
  }
}

void CHashOutside::EndSearch(CHashIterator *it)
{
  CHashOutsideIterator *ptr=(CHashOutsideIterator *)it;
  if (ptr->row<count)
    if (ptr->cluster)
    {
      SHashMini *cl=(SHashMini *)ptr->cluster;
      ReleaseCluster(cl); 
    }
  ptr->next=itrfree;
  itrfree=ptr;
}

CHashKey * CHashOutside::RemoveKey(CHashIterator *it)
{
  CHashKey *out=NULL;
  CHashOutsideIterator *ptr=(CHashOutsideIterator *)it;
  if (ptr->row<count && ptr->cluster)
  {
    SHashMini *cl=(SHashMini *)ptr->cluster;
    out=cl->list[ptr->pos];
    cl->list[ptr->pos]=NULL;
    for (int i=0;i<cluster;i++) if (cl->list[i]) break;
    if (i==cluster) 
    {
      if (table[ptr->row]==cl)
        table[ptr->row]=cl->next;
      else
      {
        SHashMini *row=table[ptr->row];
        while (row && row->next!=cl) row=row->next;
        if (row) row->next=cl->next;
      }
      if (cl->next) cl->next->refcount++;		
      cl->refcount--;
    }	  
  }
  return out;
}

bool CHashOutside::KillKey(CHashIterator *it)
{
  CHashKey *pt=RemoveKey(it);
  if (pt) 
  {
    pt->DestroyKey();
    return true;
  }
  return false;
}

bool CHashOutside::Resize(IHASH item)
{
  CHashOutside nw(count,cluster,flgs);	//vyrob novou tabulku
  CHashIterator *it=BeginSearch(NULL);	//prochazej vsechny prvky
  while (it->FindNext())				//pro kazdy prvek
  { 
    CHashKey *k=it->RemoveItem();		//vyjmi prvek z teto tabulky
    nw.AddKey(k);						//dej ho do nove tabulky. Vzdycky projde
  }
  delete [] table;						//vymaz aktualni tabulku
  count=nw.count;						//prevezmi tabulku z nove tabulky
  table=nw.table;					
  nw.table=NULL;						//oznac novou tabulku jako neinicalizovanou
  nw.count=0;
  it->EndSearch();						//ukonci hledani
  return true;							//vrat uspech
}

CHashOutside::~CHashOutside()
{
  Flush();
  delete [] table;
  minfreelist->DeleteList();
  while (itrfree!=NULL)
  {
    CHashOutsideIterator *it=itrfree;
    itrfree=it->next;
    delete it;
  }
}

void CHashOutside::SHashMini::DeleteList()
{  
  SHashMini *p=this;
  while (p!=NULL)
  {
    SHashMini *q=p;	
    p=p->next;
    delete q;
  }
}

void * CHashOutside::GetItem(CHashIterator *it)
{
  CHashOutsideIterator *ptr=(CHashOutsideIterator *)it;
  CHashKey *out;
  if (ptr->row<count && ptr->cluster!=NULL)
  {
    SHashMini *min=(SHashMini *)(ptr->cluster);
    out=min->list[ptr->pos];
  }
  return out;
}

