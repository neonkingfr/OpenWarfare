// ctArchiveRefExt.h: interface for the ctArchiveRefExt class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTARCHIVEREFEXT_H__FAB93E57_AEC5_480C_AF89_42F415256ADC__INCLUDED_)
#define AFX_CTARCHIVEREFEXT_H__FAB93E57_AEC5_480C_AF89_42F415256ADC__INCLUDED_

#include "ctLongLongTable.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ctArchive.h"

class ctArchiveRefExt : public ctArchiveEvent  
{  
  ctLongLongTable instance;
  long instcntr; //instancni citac
  public:
    ctArchiveRefExt();
    ctArchiveRefExt(const ctArchiveRefExt& other);
    virtual ~ctArchiveRefExt() 
    {}
    
    virtual bool OnBeforeLoadClass(CTLPSC &cls);
    virtual void OnLoadClass(CTLPSC cls);
    virtual void OnAfterLoadClass(CTLPSC cls, bool kill);
    virtual bool OnBeforeSaveClass(CTLPSC cls);
    virtual ctArchiveEvent*OnClone() 
    {return new ctArchiveRefExt(*this);}
    
};

#endif // !defined(AFX_CTARCHIVEREFEXT_H__FAB93E57_AEC5_480C_AF89_42F415256ADC__INCLUDED_)
