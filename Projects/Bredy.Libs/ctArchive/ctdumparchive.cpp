// ctdumparchive.cpp: implementation of the ctDumpArchive class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ctArchive.h"
#include "ctdumparchive.h"
#include "ctClassInfo.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define LEADCHAR "\""

ctDumpArchive::ctDumpArchive(ostream &file,unsigned long flags):out(file),ctArchiveOut(flags)
{
  voffset=0;
  char line[256];
  sprintf(line,"%-8s %-10s  %-12s  %-20s %s\n","frame","offset","size","type","interpreted data");
  out<<line;
  for (int i=0;i<75;i++) out<<'-';
  out<<'\n';  
  frame=0;
}

static DWORD stackframe;
#define STACKFRAMESAVE() __asm mov stackframe,ebp
void ctDumpArchive::DumpOut(ostream &out, DWORD offset, DWORD size, const char *type, const char *comment, const char *lead)
{
  char line[256];
  int cmnsz=strlen(comment);
  if (cmnsz>40) cmnsz=40;
  char *cmn=(char *)alloca(cmnsz*10);
  const char *src=comment;
  char *trg=cmn;
  for (int i=0;i<cmnsz;i++,src++)
  {
    if (*(unsigned char *)src<32 || *src==lead[0] || *src=='\\')
    {
      *trg++='\\';
      switch (*src)
      {
        case '\n':*trg++='n';break;
        case '\r':*trg++='r';break;
        case '\t':*trg++='t';break;
        //		case '\0':*trg++='0';break;
        case '"':*trg++='"';break;
        case '\\':*trg++='\\';break;
        default: sprintf(trg,"x%02X",*(unsigned char *)src);
        trg+=3;break;
      }
    }
    else *trg++=*src;
  }  
  if (cmnsz==40) strcpy(trg,"...");else *trg++=0;
  if (frame==0) frame=stackframe;
  sprintf(line,"%+8d 0x%08X  %12d  %-20s %s%.40s%s\n",(int)frame-(int)stackframe,offset,size,type,lead,cmn,lead);
  out<<line;
}

CTLPSC ctDumpArchive::serial(CTLPSC src)
{
  STACKFRAMESAVE();
  if (src==NULL)
  {
    DumpOut(out,voffset,0,"class","NULL pointer");
    src=ctArchiveOut::serial(src);
  }
  else
  {
    ctClassInfo *cls=ctClassTable::GetClass(src);
    const char *name;
    if (cls==NULL) name="??? UNKNOWN ???";
    else name=cls->GetClassName();
    DumpOut(out,voffset,0,"class:Begin",name,LEADCHAR);
    src=ctArchiveOut::serial(src);
    DumpOut(out,voffset,0,"class:End",name,LEADCHAR);
  }
  return src;
}

bool ctDumpArchive::ver(unsigned long version)
{
  STACKFRAMESAVE();
  char buff[50];
  sprintf(buff,"%d.%d (%08X) (%d)",version/256,version%256,version,version);
  DumpOut(out,voffset,sizeof(version),"version info",buff);
  voffset+=sizeof(version);
  return true;
}

bool ctDumpArchive::check(const char *tagId)
{
  STACKFRAMESAVE();
  DumpOut(out,voffset,strlen(tagId)+1,"check mark",tagId,LEADCHAR);
  voffset+=strlen(tagId)+1;
  return true;
}

void ctDumpArchive::serial(void *ptr, unsigned long size)
{
  STACKFRAMESAVE();
  char data[41];
  int msz=size>10?10:size;
  int i;
  char *p=data;
  for (i=0;i<msz;i++) 	
  {
    unsigned char znak=*((unsigned char *)ptr+i);
    sprintf(data+3*i,"%02X ",znak);
  }
  for (i=0;i<msz;i++) 	
  {
    unsigned char znak=*((unsigned char *)ptr+i);
    sprintf(data+30+i,"%c",znak<32?'.':znak);
  }
  DumpOut(out,voffset,size,"indistinct data",data,LEADCHAR);
  voffset+=size;
}

ctArchive& ctDumpArchive::operator>>=(bool &val)
{
  STACKFRAMESAVE();
  DumpOut(out,voffset,sizeof(val),"boolean",val?"true":"false");
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(char &val)
{
  STACKFRAMESAVE();
  char buff[20];
  sprintf(buff,"%d (%02X) '%c'",val,val,val);
  DumpOut(out,voffset,sizeof(val),"signed char",buff);
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(unsigned char &val)
{
  STACKFRAMESAVE();
  char buff[20];
  sprintf(buff,"%u (%02X) '%c'",val,val,val);
  DumpOut(out,voffset,sizeof(val),"unsigned char",buff);
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(short &val)
{
  STACKFRAMESAVE();
  char buff[50];
  sprintf(buff,"%d (%04X) \"%.2s\"",val,val,&val);
  DumpOut(out,voffset,sizeof(val),"signed short",buff);
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(unsigned short &val)
{
  STACKFRAMESAVE();
  char buff[50];
  sprintf(buff,"%u (%04X) \"%.2s\"",val,val,&val);
  DumpOut(out,voffset,sizeof(val),"unsigned short",buff);
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(int &val)
{
  STACKFRAMESAVE();
  char buff[50];
  sprintf(buff,"%d (%08X) \"%.4s\"",val,val,&val);
  DumpOut(out,voffset,sizeof(val),"signed int",buff);
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(unsigned int &val)
{
  STACKFRAMESAVE();
  char buff[50];
  sprintf(buff,"%u (%08X) \"%.4s\"",val,val,&val);
  DumpOut(out,voffset,sizeof(val),"unsigned int",buff);
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(long &val)
{
  STACKFRAMESAVE();
  char buff[50];
  sprintf(buff,"%d (%08X) \"%.4s\"",val,val,&val);
  DumpOut(out,voffset,sizeof(val),"signed int",buff);
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(unsigned long &val)
{
  STACKFRAMESAVE();
  char buff[50];
  sprintf(buff,"%u (%08X) \"%.4s\"",val,val,&val);
  DumpOut(out,voffset,sizeof(val),"unsigned int",buff);
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(float &val)
{
  STACKFRAMESAVE();
  char buff[50];
  sprintf(buff,"%g",val);
  DumpOut(out,voffset,sizeof(val),"float",buff);
  voffset+=sizeof(val);
  return *this;
}

ctArchive& ctDumpArchive::operator>>=(double &val)
{
  STACKFRAMESAVE();
  char buff[50];
  sprintf(buff,"%g",val);
  DumpOut(out,voffset,sizeof(val),"double",buff);
  voffset+=sizeof(val);
  return *this;
}

char * ctDumpArchive::operator<<(const char *str)
{
  STACKFRAMESAVE();
  DumpOut(out,voffset,strlen(str)+1,"StringZ",str,LEADCHAR);
  voffset+=strlen(str)+1;
  return (char *)str;
}

short * ctDumpArchive::operator<<(const short *str)
{
  STACKFRAMESAVE();
  int len=wcslen((const unsigned short *)str);
  int mylen;
  if (len>40) mylen=40;else mylen=len;
  unsigned char *z=(unsigned char *)alloca(mylen+1);
  int i;
  for (i=0;i<mylen;i++) if (str[i]>254) z[i]='.';else z[i]=str[i] & 0xFF;
  z[i]=0;
  DumpOut(out,voffset,(len+1)*sizeof(str[0]),"Unicode StringZ",(char *)z,LEADCHAR);
  voffset+=(len+1)*sizeof(str);
  return (short *)str;
}

void ctDumpArchive::reserve(unsigned long bytes)
{
  STACKFRAMESAVE();
  DumpOut(out,voffset,bytes,"reseved","reserved");
  voffset+=bytes;
}

