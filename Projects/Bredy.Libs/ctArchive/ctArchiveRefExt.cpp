// ctArchiveRefExt.cpp: implementation of the ctArchiveRefExt class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ctArchiveRefExt.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ctArchiveRefExt::ctArchiveRefExt()
{
  instcntr=0;
}

ctArchiveRefExt::ctArchiveRefExt(const ctArchiveRefExt& other):
instance(other.instance)
{
  instcntr=other.instcntr;
}

bool ctArchiveRefExt::OnBeforeLoadClass(CTLPSC &cls)
{
  (*arch)>>=instcntr;
  if (instcntr==0) 
  {cls=NULL;return true;}
  long p=instance[instcntr];
  if (p==-1)
  {
    cls=NULL;
    return true;
  }
  if (p==0)
  {
    return false;
  }
  cls=(CTLPSC)p;
  return true;
}

void ctArchiveRefExt::OnLoadClass(CTLPSC cls) 
{
  long p=(long)cls;;
  if (p==0) p=-1;
  instance.Add(instcntr,p);
}

void ctArchiveRefExt::OnAfterLoadClass(CTLPSC cls, bool kill) 
{
  if (kill && cls)
  {
    long p=(long)cls;;
    long z=-1;
    long index=instance.FindValueIndex(p,z);
    if (index) instance.Add(index,-1);
  }
}

bool ctArchiveRefExt::OnBeforeSaveClass(CTLPSC clss)
{
  long p=(long)clss;
  if (p==0) 
  {
    (*arch)>>=p;
    return true;
  }
  long inst=instance[p];
  if (inst==0) 
  {
    inst=++instcntr;
    (*arch)>>=inst;
    instance.Add(p,inst);
    return false;
  }
  else
  {
    (*arch)>>=inst;
    return true;
  }
}

