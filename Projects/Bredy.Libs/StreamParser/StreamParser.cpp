// StreamParser.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "StreamParser.tli"
#include <iostream>
#include <fstream>
#include <malloc.h>
#include "SimpleParamFileApi.h"
#include "ArrayOperations.h"
#include "XMLParser.tli"
using namespace std;


template<class Item>
class IOParserStream
{
  istream &_input;
public:
  IOParserStream(istream &input):_input(input) {}
  bool ReadStream(Item *array, size_t toread, size_t &wasread)
  {
	_input.read((char *)array,toread*sizeof(Item));
	wasread=_input.gcount()/sizeof(Item);
	return !_input.bad();
  }
};


class ParsedResult
{
public:
  //called before class is processed
  bool OnBeginClass(const char *className) {cout<<"BeginClass:"<<className<<"\r\n";return true;}
  //called after class is processed
  bool OnEndClass(const char *className) {cout<<"EndClass:"<<className<<"\r\n";return true;}
  //called before single item is processed
  bool OnBeginItem(const char *name) {cout<<"BeginItem:"<<name<<"\r\n";return true;}
  //called after single item is processed
  bool OnEndItem(const char *name) {cout<<"EndItem:"<<name<<"\r\n";return true;}
  //called on start of array
  bool OnBeginArray() {cout<<"BeginArray\r\n";return true;}
  //called on end of array
  bool OnEndArray() {cout<<"EndArray\r\n";return true;}
  //called befor array item is processed (OnBeginArray is following)
  bool OnBeginArrayItem(const char *name) {cout<<"BeginArrayItem:"<<name<<"\r\n";return true;}
  //called after array item is processed (after OnEndArray)
  bool OnEndArrayItem(const char *name) {cout<<"EndArrayItem:"<<name<<"\r\n";return true;}
  
  //called after int value has been parsed
  bool OnIntValue(int value) {cout<<"Int value:"<<value<<"\r\n";return true;}
  //called after float value has been parsed
  bool OnFloatValue(float value) {cout<<"Float value:"<<value<<"\r\n";return true;}
  //called after string value has been parsed
  bool OnStringValue(const char *value) {cout<<"String value:'"<<value<<"'\r\n";return true;}

  //function is called before OnIntValue
  bool OnIntVariable(const char *name, int value) {cout<<name<<"="<<value<<"\r\n";return true;}
  //function is called before OnFloatValue
  bool OnFloatVariable(const char *name, float value) {cout<<name<<"="<<value<<"\r\n";return true;}
  //function is called before OnStringValue
  bool OnStringVariable(const char *name, const char *value) {cout<<name<<"='"<<value<<"'\r\n";return true;}
  //Allows to store context for each parser's level
  void *OnPushContext() {return NULL;}
  //called everytime parser exits from stack
  void OnPopContext(void *context) {}
};


class StringToArray
{
  const char *_text;
  unsigned int _length;
public:
  StringToArray(const char *text,unsigned int length=0):_text(text),_length(length==0?strlen(text):length) {}
  const char &operator[] (unsigned int index) const {return _text[index];}
  unsigned int Size() const {return _length;}
};

void CombArrayTest()
{
StringToArray text1("Ahoj svete!");
StringToArray text2("Jak se mas!");
StringToArray text3("Co delas!");
CombinedArrays<StringToArray,char> comb1(text2,text3);
CombinedArraysMixed<StringToArray,CombinedArrays<StringToArray,char>,char> comb2(text1,comb1);
SubArray<CombinedArraysMixed<StringToArray,CombinedArrays<StringToArray,char>,char>,char> subArr(comb2,5,20);
char *res=StackStrArraySeg(subArr);
puts(res);
}

static void DisplayContent(XMLParser<IOParserStream<char> > &xmlparser)
{
        int chr=xmlparser.GetChar();
        while (chr!=-1) {putchar(chr);chr=xmlparser.GetChar();}
        puts("");
}

int main(int argc, char* argv[])
{
  CombArrayTest();
  ifstream filein("C:\\Msdev\\BIS\\Projects\\bredy.libs\\Archive\\XMLArchive\\Pokus.xml",ios::in);
  if (!filein) abort();
	IOParserStream<char> input(filein);
/*
  SimpleParamFileApi<IOParserStream<char> ,ParsedResult> parser(input,results);
  int err=parser.Parse();
  char buff[80];
  if (err)
  {
    cout<<"Parser error: "<<err<<" near\r\n";
    parser.PeekStream(buff,80);
    cout<<buff;
  }
*/
  XMLParser<IOParserStream<char> > xmlparser(&input,false);
  if (xmlparser.OpenTag("?xml"))
  {
    if (xmlparser.OpenTag("xml"))
    {
      if (xmlparser.OpenTag("block1"))
      {
        if (xmlparser.OpenAttribute("loctext"))
        {
          DisplayContent(xmlparser);
          xmlparser.CloseAttribute("loctext");
        }
        if (xmlparser.OpenTag("name"))
        {
          DisplayContent(xmlparser);
          xmlparser.CloseTag("name");
        }
        if (xmlparser.OpenTag("description"))
        {
          DisplayContent(xmlparser);
          xmlparser.CloseTag("description");
        }
        xmlparser.CloseTag("block1");
      }
      xmlparser.CloseTag("xml");
    }
  }

	return 0;
}
