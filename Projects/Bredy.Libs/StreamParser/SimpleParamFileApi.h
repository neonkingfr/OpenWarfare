#pragma once

#include "StreamParser.h"
#include "StackStrings.h"

class SimpleParamFileClass
{
public:
  //called before class is processed
  bool OnBeginClass(const char *className) {return true;}
  //called after class is processed
  bool OnEndClass(const char *className) {return true;}
  //called before single item is processed
  bool OnBeginItem(const char *name) {return true;}
  //called after single item is processed
  bool OnEndItem(const char *name) {return true;}
  //called on start of array
  bool OnBeginArray() {return true;}
  //called on end of array
  bool OnEndArray() {return true;}
  //called befor array item is processed (OnBeginArray is following)
  bool OnBeginArrayItem(const char *name) {return true;}
  //called after array item is processed (after OnEndArray)
  bool OnEndArrayItem(const char *name) {return true;}
  
  //called after int value has been parsed
  bool OnIntValue(int value) {return true;}
  //called after float value has been parsed
  bool OnFloatValue(float value) {return true;}
  //called after string value has been parsed
  bool OnStringValue(const char *stringValue) {return true;}

  //function is called before OnIntValue
  bool OnIntVariable(const char *name, int value) {return true;}
  //function is called before OnFloatValue
  bool OnFloatVariable(const char *name, float value) {return true;}
  //function is called before OnStringValue
  bool OnStringVariable(const char *name, const char * value) {return true;}
  //Allows to store context for each parser's level
  void *OnPushContext() {return NULL;}
  //called everytime parser exits from stack
  void OnPopContext(void *context) {}
};


template <class T>
class SimpleParamFileClassContext
{
  T &_ifc;
  void *_context;
  SimpleParamFileClassContext(T &ifc):_ifc(ifc),
    _context(ifc.OnPushContext()) {}
  ~SimpleParamFileClassContext()
  {_ifc.OnPopContext(_context);}
};


template <class Stream, class Event>
class SimpleParamFileApi
{
  Stream &_instr;
  Event &_event;
  StreamParser<char,Stream> _parser;
public:
  
  enum ParseStatus
  {
    Ok=0,
    ErrExceptingEndOfFile,  
    ErrExceptingEndOfClass,
    ErrExceptingBeginOfArray,
    ErrExceptingEndOfArray,
    ErrExceptingSemicolon,
    ErrExceptingValue,
    ErrUnknownData,
    ErrArrayIsUnexceptedHere,

    ErrCanceled
  };

  SimpleParamFileApi(Stream &instr,Event &event):_instr(instr),_event(event),_parser(&_instr,false) {};
  int Parse();
  int GetParsingOffset() {return _parser.GetBufferUsage();}

  int PeekStream(char *buff, size_t size) 
  {
    size_t read=0;
    _parser.Read(buff,size-1,read,true);
    buff[read]=0;
    return read;
  }

protected:
  int ParseClassBody();
  int ParseSingleVariable();
  int ParseArrayVariable();
  int ParseClass();
  int ParseArray();
  int ParseArrayContent();
  int ParseSingleValue(bool canbearray, const char *name);

 
};

template<class Stream,class Event>
int SimpleParamFileApi<Stream,Event>::Parse()
{
  int res=ParseClassBody();
  if (res) return res;
  if (_parser.Test("^[\\s]*$")) return 0;
  return ErrExceptingEndOfFile;
}


template<class Stream,class Event>
int SimpleParamFileApi<Stream,Event>::ParseClassBody()
{
  do
  {  
    if (_parser.Test("^[\\s]*class_+([a-zA-Z0-9_]+)[\\s]*{"))
    {
      int res=ParseClass();
      if (res!=0) return res;
    }
    else if (_parser.Test("^[\\s]*([a-zA-Z0-9_]+)_*="))
    {
      int res=ParseSingleVariable();
      if (res!=0) return res;
    }
    else if (_parser.Test("^[\\s]*([a-zA-Z0-9_]+)\\[\\]_*="))
    {
      int res=ParseArrayVariable();
      if (res!=0) return res;
    }
    else return 0;
    if (_parser.Test("^[\\s]*;")==false)
      return ErrExceptingSemicolon;
    _parser.Ack();
  }
  while (1);
}

template<class Stream,class Event>
int SimpleParamFileApi<Stream,Event>::ParseSingleVariable()
{
  char *varname=StackStrArraySeg(_parser.GetSubExpText());  
  _parser.Ack();
  if (!_event.OnBeginItem(varname)) return ErrCanceled;
  
  int res=ParseSingleValue(false,varname); //cannot be array, name
  
  if (!_event.OnEndItem(varname) && !res) return ErrCanceled;
  
  return res;
}


template<class Stream,class Event>
int SimpleParamFileApi<Stream,Event>::ParseArrayVariable()
{
  char *varname=StackStrArraySeg(_parser.GetSubExpText());
  _parser.Ack();
  if (!_event.OnBeginArrayItem(varname)) return ErrCanceled;

  int res=ParseArray(); //is array, name

 if (!_event.OnEndArrayItem(varname) && !res) return ErrCanceled;

  return res;
}

template<class Stream,class Event>
int SimpleParamFileApi<Stream,Event>::ParseClass()
{
  char *varname=StackStrArraySeg(_parser.GetSubExpText());  
  _parser.Ack();
  
  if (!_event.OnBeginClass(varname)) return ErrCanceled;
  int res=ParseClassBody();
  if (!_event.OnEndClass(varname) && !res) return ErrCanceled;

  if (res==0 && !_parser.Test("^[\\s]*}")) res=ErrExceptingEndOfClass;
  else _parser.Ack();

  return res;

}

template<class Stream,class Event>
int SimpleParamFileApi<Stream,Event>::ParseArray()
{
  if (_parser.Test("^[\\s]*{")==false) return ErrExceptingBeginOfArray;
  _parser.Ack();
  if (!_event.OnBeginArray()) return ErrCanceled;
  int res=ParseArrayContent();
  if (!_event.OnEndArray() && !res) return ErrCanceled;
  if (_parser.Test("^[\\s]*}")==false) return ErrExceptingEndOfArray;
  _parser.Ack();

  return res;
}

template<class Stream,class Event>
int SimpleParamFileApi<Stream,Event>::ParseArrayContent()
{
  int res=ParseSingleValue(true,NULL);
  if (res==ErrExceptingValue) return 0; //empty array
  while (_parser.Test("^,"))  
  {
    _parser.Ack();
    res=ParseSingleValue(true,NULL);
    if (res) return res;
  }
  return 0;
}

template<class Stream,class Event>
int SimpleParamFileApi<Stream,Event>::ParseSingleValue(bool canbearray, const char *name)
{
  _parser.SkipWhite();
  if (_parser.Test("^([+-]?[0-9]*[.][0-9]([eEdD][+-]?[0-9]+)?)"))
  {
    float value=(float)_parser.GetSubExpDouble();
    if (name) if (!_event.OnFloatVariable(name,value)) return ErrCanceled;
    if (!_event.OnFloatValue(value)) return ErrCanceled;
    _parser.Ack();
    return 0;
  }
  else if (_parser.Test("^([+-]?[0-9]+)"))
  {
    int value=_parser.GetSubExpInt(1);
    if (name) if (!_event.OnIntVariable(name,value)) return ErrCanceled;
    if (!_event.OnIntValue(value)) return ErrCanceled;
    _parser.Ack();
    return 0;
  }
  else if (_parser.Test("^{"))
  {
    if (canbearray) 
    {
      return ParseArray();   
    }
    else
      return ErrArrayIsUnexceptedHere;
  }
  else if (_parser.Test("^\"(([^\"]|(\"\"))*)\""))
  {
    int datalen=_parser.GetSubExpLen();
    char *value=strncpy((char *)(datalen>1024?alloca(datalen+1):new char[datalen+1]),_parser.GetSubExp(1),datalen);
    value[datalen]=0;
    char *a,*b;
    for (a=value,b=value;*b;b++,a++)
    {
      if (b[0]=='"' && b[1]=='"') b++;
      *a=*b;
    }
    *a=0;
    if (name) if (!_event.OnStringVariable(name,value)) return ErrCanceled;
    if (!_event.OnStringValue(value)) return ErrCanceled;
    if (datalen>1024) delete [] value;
    _parser.Ack();
    return 0;
  }
  else if (_parser.Test("^([^,;}]*)"))
  {
    int datalen=_parser.GetSubExpLen();
    char *value=strncpy((char *)(datalen>1024?alloca(datalen+1):new char[datalen+1]),_parser.GetSubExp(1),datalen);
    value[datalen]=0;
    if (name) if (!_event.OnStringVariable(name,value)) return ErrCanceled;
    if (!_event.OnStringValue(value)) return ErrCanceled;
    if (datalen>1024) delete [] value;
    _parser.Ack();
    return 0;
  }
 return ErrUnknownData;
}

























