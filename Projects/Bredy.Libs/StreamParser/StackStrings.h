#ifndef _STACK_STRINGS_DEFINED_
#define _STACK_STRINGS_DEFINED_

#include <malloc.h>

#pragma once

#define StackStrA(str) strcpy((char *)alloca(strlen(str)+1),str)
#define StackStrW(str) wcscpy((wchar_t *)alloca((wcslen(str)+1)*sizeof(wchar_t)),str)

#define StackStrBuffA(sz) (char *)alloca(sz+1)
#define StackStrBuffW(sz) (wchar_t *)alloca(sz*sizeof(wchar_t))

template <class CharType,class Array>
CharType *StackStrCopyLimited(CharType *buff, Array arr)
{
  for (size_t i=0,cnt=arr.Size();i<cnt;i++) buff[i]=arr[i];
  buff[arr.Size()]=0;
  return buff;
}


#define StackStrArraySegA(str) StackStrCopyLimited((char *)alloca(str.Size()+1),str)
#define StackStrArraySegW(str) StackStrCopyLimited((wchar_t *)alloca((wcslen(str)+1)*sizeof(wchar_t)),str)

#define StackStrCatA(str1,str2) strcat(strcpy((char *)alloca(strlen(str1)+strlen(str2)+1),str1),str2)
#define StackStrCatW(str1,str2) wcscat(wcscpy((wchar_t *)alloca((wcslen(str1)+wcslen(str2)+1)*sizeof(wchar_t)),str1),str2)

#define StackStrCatCatA(str1,str2,str3) strcat(strcat(strcpy((char *)alloca(strlen(str1)+strlen(str2)+strlen(str3)+1),str1),str2),str3)
#define StackStrCatCatW(str1,str2,str3) wcscat(wcscat(wcscpy((wchar_t *)alloca((wcslen(str1)+wcslen(str2)+strlen(str3)+1)*sizeof(wchar_t)),str1),str2),str3)

#define StackStrExpandA(str,chrs) strcpy((char *)alloca(strlen(str)+1+chrs),str)
#define StackStrExpandW(str,chrs) wcscat((wchar_t *)alloca((wcslen(str)+1+chrs)*sizeof(wchar_t)),str)

#ifdef _UNICODE

#define StackStr(str) StackStrW(str)
#define StackStrBuff(sz) StackStrBuffW(sz)
#define StackStrArraySeg(str) StackStrArraySegW(str)
#define StackStrCat(str1,str2) StackStrCatW(str1,str2)
#define StackStrCatCar(str1,str2,str3) StackStrCatCatW(str1,str2,str3)
#define StackStrExpand(str,chrs) StackStrExpandW(str,chrs) 


#else
#endif

#define StackStr(str) StackStrA(str)
#define StackStrBuff(sz) StackStrBuffA(sz)
#define StackStrArraySeg(str) StackStrArraySegA(str)
#define StackStrCat(str1,str2) StackStrCatA(str1,str2)
#define StackStrCatCar(str1,str2,str3) StackStrCatCatA(str1,str2,str3)
#define StackStrExpand(str,chrs) StackStrExpandA(str,chrs) 

#endif