#pragma once
#include "propshgroup.h"

class PropShGridGroup :
  public PropShGroup
{
    
  int _cols;
  int _minwidth;
  int _minwidthitem;
  bool _righttoleft;
public:
  PropShGridGroup(int columncnt=4, bool border=false, bool righttoleft=false);
  void SetColumnsCount(int columncnt) {_cols=columncnt;}

  void UpdateGroupContent(PropShHDWP &hdwp, int x, int y, int wspace, int hspace, int &width, int &height);
};
