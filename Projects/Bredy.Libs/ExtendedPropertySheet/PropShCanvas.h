// PropShCanvas.h: interface for the PropShCanvas class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROPSHCANVAS_H__CEB74F0B_A77A_42BB_8626_AA05667FD473__INCLUDED_)
#define AFX_PROPSHCANVAS_H__CEB74F0B_A77A_42BB_8626_AA05667FD473__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PropShItem.h"


class PropShCanvas : public PropShItem  
{
protected:
	HWND _hCanvas;
    int _paddingX;
    int _paddingY;
	unsigned int _xsize,  _ysize; //fixed canvas size
    bool _nohscroll, _novscroll;  //in fixed canvas size, disable scrollbars
	void ResizeWindow(PropShHDWP &hdwp,int left, int top, int cx,int cy,int scroll, int *outCx, int *outCy);
    virtual void LayoutChanged();
    float _titleSize;
public:
	PropShCanvas();
	virtual ~PropShCanvas();

	virtual BOOL OnCreate(LPCREATESTRUCT lpcs) {return TRUE;}
	virtual LRESULT OnMessage(UINT message, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnCommand(WORD wNotifyCode,WORD wID, HWND ctl);		

	virtual HWND GetWindowHandle() {return _hCanvas;} 

	bool CreateCanvas(const tchar *name, const tchar *title, PropShItem *parent, HINSTANCE hInst=NULL);
	virtual bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
	virtual HWND CreateCanvasWindow(const tchar *className, const tchar *title, HWND parent, HINSTANCE hInst);
    virtual void ResizeCanvas(PropShHDWP &hdwp, int left, int top, int cx,int cy, int *outCx=NULL, int *outCy=NULL);    
	static const tchar *GetPropShClass();
    void RecalcLayout();

    void SetPadding(int padX,int padY)
    {
      _paddingX=padX;
      _paddingY=padY;
      ThrowEvent().LayoutDirty(this);
    }

	///Sets static size of canvas
	/** @param xs horizontal size of canvas. zero means automatic size
		@param ys vertical size of canvas. zero means automatic size
		*/
	void SetCanvasSize(unsigned int xs=0, unsigned int ys=0)
	{
	  _xsize=xs;
	  _ysize=ys;
	  ThrowEvent().LayoutDirty(this,false);
	}
    void DisableScrollbars(bool nohscroll, bool novscroll)
    {
      _novscroll=novscroll;
      _nohscroll=nohscroll;
    }
    virtual void ScrollPosChanged() {}
	void GetScrollOffset(int &x, int &y);

    virtual bool ItemPropertySupport(int propertyId, void *data, const type_info &type, bool retrieve);



};

#endif // !defined(AFX_PROPSHCANVAS_H__CEB74F0B_A77A_42BB_8626_AA05667FD473__INCLUDED_)
