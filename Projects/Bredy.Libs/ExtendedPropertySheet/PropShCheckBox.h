#pragma once
#include "propshtitleonly.h"

class PropShCheckBox :  public PropShTitleOnly
{
public:
  enum Style
  {
    Standard=BS_AUTOCHECKBOX,
    PushLike=BS_AUTOCHECKBOX|BS_PUSHLIKE,
    Flat=BS_AUTOCHECKBOX|BS_FLAT
  };
protected:
  HWND _hButton;
  Style _style;
public:
  PropShCheckBox(Style style=Standard);
  ~PropShCheckBox(void);

  virtual bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
  virtual HWND GetWindowHandle() {return _hButton;} //default - no canvas
  virtual void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height);
  virtual void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);

  virtual int GetDataSize() const {return 1;}
  virtual bool GetStringData(tchar *buffer, size_t size) const {return false;}
  virtual bool GetBinaryData(void *buffer, size_t size) const;

  virtual void SetStringData(const tchar *data) {}
  virtual void SetBinaryData(const void *data, size_t size);

};
