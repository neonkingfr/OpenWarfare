#pragma once
#include "propshscrollingcanvas.h"

class PropShContainer :
  public PropShScrollingCanvas
{
  PropShItem *_content;
public:
  PropShContainer(void);
  ~PropShContainer(void);

  void SetContent(PropShItem *content);
  bool SetContent(PropShItem *item, const tchar *name, const tchar *title); 

  void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int &width, int &height);

  PropShBase *Find(const tchar *name)
    {return _content?_content->Find(name):NULL;}
  
  void ScrollPosChanged(bool vert, int value);
};
