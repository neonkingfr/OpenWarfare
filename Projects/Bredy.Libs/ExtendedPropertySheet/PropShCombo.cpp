#include "PropShCommon.h"
#include ".\propshcombo.h"
#include <windowsx.h>

PropShCombo::PropShCombo(bool dropList,bool sortItems,bool allowNull)
{
  _dropList=dropList;
  _sortItems=sortItems;
  _allowNull=allowNull;
}

PropShCombo::~PropShCombo(void)
{
}


bool PropShCombo::Create(const tchar *name, const tchar *title, PropShItem *parent)
{
  if (PropShItem::Create(name,title,parent)==false) return false;  
  _hTitle=CreateWindow("STATIC",title,WS_CHILD|SS_LEFT|SS_CENTERIMAGE|SS_ENDELLIPSIS|WS_VISIBLE,0,0,100,100,*parent,NULL,parent->GetInstance(),NULL);
  long style=CBS_AUTOHSCROLL|(_dropList?CBS_DROPDOWNLIST:CBS_DROPDOWN)|CBS_DISABLENOSCROLL|
            CBS_NOINTEGRALHEIGHT|(_sortItems?CBS_SORT:0)|WS_VSCROLL|
            WS_TABSTOP|WS_CHILD|WS_VISIBLE;
  
  _hControl=CreateWindow("COMBOBOX","",style,0,0,100,300,*parent,NULL,parent->GetInstance(),NULL);
/*  if (_hControl) 
  {
    COMBOBOXINFO cnfo;
    memset(&cnfo,0,sizeof(cnfo));
    cnfo.cbSize=sizeof(cnfo);

    GetComboBoxInfo(_hControl,&cnfo);
    SetWindowLong(cnfo.hwndItem,GWL_STYLE,WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|ES_LEFT);
  }*/
  return _hTitle!=NULL && _hControl!=NULL;	

}
  

int PropShCombo::GetDataSize() const
{
  if (_dropList) 
  {
    int i=ComboBox_GetCurSel(_hControl);
    if (i==-1) return 1;
    return ComboBox_GetLBTextLen(_hControl,i);
  }
  else return GetWindowTextLength(_hControl);
}

bool PropShCombo::GetStringData(tchar *buffer, size_t size) const
{
  if (_dropList)
  {
    int i=ComboBox_GetCurSel(_hControl);
    if (i==-1) {buffer[0]=0;return true;}
    size_t maxSize=ComboBox_GetLBTextLen(_hControl,i);
    if (size<maxSize) return false;
    ComboBox_GetLBText(_hControl,i,buffer);
  }
  else
  {
    GetWindowText(_hControl,buffer,size);
  }
  return true;
}

void PropShCombo::SetStringData(const tchar *data)
{
  if (_dropList)
  {
    int i=ComboBox_FindStringExact(_hControl,-1,data);
    if (i==CB_ERR && !_allowNull) i=0;
    ComboBox_SetCurSel(_hControl,i);
  }
  else
    SetWindowText(_hControl,data);
}


void PropShCombo::AddItem(const tchar *value, long userData)
{
  int i=ComboBox_AddString(_hControl,value);
  ComboBox_SetItemData(_hControl,i,userData);
}

int PropShCombo::FindItem(const tchar *value, int index)
{
  return ComboBox_FindStringExact(_hControl,index,value);
}

bool PropShCombo::DeleteItem(int index)
{
  return ComboBox_DeleteString(_hControl,index)!=0;
}

int PropShCombo::GetCurItem()
{
  return ComboBox_GetCurSel(_hControl);
}

void PropShCombo::SelectItem(int index)
{
  ComboBox_SetCurSel(_hControl, index);
}

long PropShCombo::GetItemData(int index)
{
  return ComboBox_GetItemData(_hControl,index);
}

bool PropShCombo::SetItemData(int index, long data)
{
  return ComboBox_SetItemData(_hControl, index, data)!=0;
}

void PropShCombo::DropDown()
{
  SendMessage(_hControl,CB_SHOWDROPDOWN,true,0);
}

void PropShCombo::CloseUp()
{
  SendMessage(_hControl,CB_SHOWDROPDOWN,false,0);
}

bool PropShCombo::IsDroppedDown()
{
  return SendMessage(_hControl,CB_GETDROPPEDSTATE,0,0)!=0;
}

void PropShCombo::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{
  height=CalculateFontHeight(_hControl)+4;
  int midl=this->ResizeTitle(hdwp,left,top,wspace,height);
  hdwp.SetWindowPos(_hControl,left+midl,top,wspace-midl,300);  

  width=wspace;    
}
