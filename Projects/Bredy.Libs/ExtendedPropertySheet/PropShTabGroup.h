#pragma once
#include "propshgroup.h"

class PropShTabGroup :public PropShCanvas
{
  HWND _tab;
  HWND _toHide;
  int _adjustWidth,_adjustHeight;
public:
  PropShTabGroup(void);
  ~PropShTabGroup(void);

  bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
  bool AddItem(PropShCanvas *item, const tchar *name, const tchar *title);
  void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height);
  virtual void UpdateTabItem(int left, int top, int wspace, int hspace, int &width, int &height);
  virtual LRESULT OnMessage(UINT message, WPARAM wParam, LPARAM lParam);
  virtual void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);
  bool ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result);
};
