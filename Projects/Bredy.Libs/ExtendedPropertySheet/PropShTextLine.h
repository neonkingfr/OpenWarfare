// PropShTextLine.h: interface for the PropShTextLine class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PropShTextLine_H__9F1F2126_BCD0_41E3_998E_958AB771CE48__INCLUDED_)
#define AFX_PropShTextLine_H__9F1F2126_BCD0_41E3_998E_958AB771CE48__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PropShItem.h"


class PropShTextLine : public PropShItem
{
protected:
	HWND _hTitle;
	HWND _hControl;
public:
	PropShTextLine();
	virtual ~PropShTextLine();

	/// Creates item in parent canvas. The item has the name, and the title
	virtual bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
	virtual HWND GetWindowHandle() {return _hControl;} //default - no canvas
	virtual const tchar *GetName() const {return _name;}
	virtual void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height);
	virtual void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);
	virtual int GetDataSize() const;
	virtual bool GetStringData(tchar *buffer, size_t size) const;
	virtual void SetStringData(const tchar *data);

    int ResizeTitle(PropShHDWP &dwp, int left, int top, int wspace, int hspace);
    int CalculateFontHeight(HWND hControl);

    virtual void EnableItem(bool enable);

};

#endif // !defined(AFX_PropShTextLine_H__9F1F2126_BCD0_41E3_998E_958AB771CE48__INCLUDED_)
