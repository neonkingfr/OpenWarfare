#include "PropShCommon.h"
#include ".\propshcontainer.h"

PropShContainer::PropShContainer(void):_content(NULL)
{
}

PropShContainer::~PropShContainer(void)
{
  delete _content;
}

void PropShContainer::SetContent(PropShItem *content)
{
  if (_content==content) return;
  if (_content) delete _content;
  _content=content;  
}

bool PropShContainer::SetContent(PropShItem *item, const tchar *name, const tchar *title)
{
  if (item->Create(name,title,this)==false) 
  {
    delete item;
    return false;
  }
  SetContent(item);
  return true;
}



void PropShContainer::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int &width, int &height)
{
  int shiftx=GetScrollPos(*this,SB_HORZ);
  int shifty=GetScrollPos(*this,SB_VERT);
  _content->UpdateLayout(hdwp,left-shiftx,top-shifty,wspace,width,height);
  ResizeCanvas(hdwp,width,height,&width,&height);
}

void PropShContainer::ScrollPosChanged(bool vert, int value)
{
  HWND sub=*_content;
  if (sub) 
  {
    int shiftx=GetScrollPos(*this,SB_HORZ);
    int shifty=GetScrollPos(*this,SB_VERT);
    SetWindowPos(sub,NULL,-shiftx,-shifty,0,0,SWP_NOSIZE|SWP_NOZORDER);
  }
  else
  {
    // do nothing for now
  }
}