#pragma once
#include "propshcanvas.h"

class PropShGroup :
  public PropShCanvas
{
protected:
  PropShItem **_items;
  int _count;
  int _reserved;

  int _itemSpace;
  bool _border;
  bool _dirty;
 
  bool Expand();
  int FindItemIndex(const tchar *name) const;
public:
  PropShGroup(bool border=false);
  ~PropShGroup(void);

  void DeleteAllItems();

  ///adds created/prepared item  
  bool AddItem(PropShItem *item); 

  ///adds item. The item should not be prepared. AddItem will call Create itself.
  /** if error occured, item is destroyed automaticly */
  bool AddItem(PropShItem *item, const tchar *name, const tchar *title); 

  PropShBase *Find(const tchar *name) const;

  virtual const tchar *IsMyName(const tchar *name) const;


  void DeleteItem(PropShItem *item);
  int GetItemCount() {return _count;}
  PropShItem *GetItem(int item) {return _items[item];}

  virtual void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height);
  virtual void UpdateGroupContent(PropShHDWP &hdwp, int x, int y, int wspace, int hspace, int &width, int &height);
  HWND CreateCanvasWindow(const tchar *className, const tchar *title, HWND parent, HINSTANCE hInst);

  void SetItemSpace(int space)
  {
    _itemSpace=space;
    ThrowEvent().LayoutDirty(this);
  }

  virtual void ScrollPosChanged();
  void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);

  virtual bool ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result);
};
