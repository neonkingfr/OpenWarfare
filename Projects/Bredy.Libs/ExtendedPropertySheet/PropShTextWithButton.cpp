// PropShTextWithButton.cpp: implementation of the PropShTextWithButton class.
//
//////////////////////////////////////////////////////////////////////

#include "PropShCommon.h"
#include "PropShTextWithButton.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

PropShTextWithButton::PropShTextWithButton()
{
  _hButton=NULL;
}

PropShTextWithButton::~PropShTextWithButton()
{
  if (_hButton) DestroyWindow(_hButton);
}

bool PropShTextWithButton::Create(const tchar *name, const tchar *title, PropShItem *parent)
{
  if (PropShTextLine::Create(name,title,parent)==false) return false;
  HINSTANCE hInst=parent->GetInstance();
  _hButton=CreateWindow("BUTTON","...",WS_VISIBLE|WS_CHILD|BS_PUSHBUTTON,0,0,0,0,parent->GetWindowHandle(),NULL,hInst,NULL);
  return _hButton!=NULL;	
}

void PropShTextWithButton::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{
  height=CalculateFontHeight(_hControl);
  int shift=height;
  int midl=ResizeTitle(hdwp,left,top,wspace,height);
  hdwp.SetWindowPos(_hControl,left+midl,top,wspace-midl-shift-GetSystemMetrics(SM_CXBORDER)*2,height);
  hdwp.SetWindowPos(_hButton,left+wspace-shift+GetSystemMetrics(SM_CXBORDER),top,shift,height);
  width=wspace;  
}

void PropShTextWithButton::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  PropShTextLine::BroadcastMessage(message,wParam,lParam);
  SendMessage(_hButton,message,wParam,lParam);
}

bool PropShTextWithButton::ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result)
{
  bool enabled=IsWindowEnabled(_hButton)!=FALSE;
  if (msg==WM_COMMAND && hWnd==_hButton && HIWORD(wParam)==BN_CLICKED && enabled)
	{OnButtonPressed();return true;}
  else if (msg==WM_COMMAND  && LOWORD(wParam)==PROPCMD_ACTIONBUTTON5 && (hWnd==_hButton || hWnd==_hControl) && enabled)
	{OnButtonPressed();return true;}
  return false;
}

void PropShTextWithButton::EnableItem(bool enable)
{
  EnableWindow(_hButton,enable);
  __super::EnableItem(enable);
}