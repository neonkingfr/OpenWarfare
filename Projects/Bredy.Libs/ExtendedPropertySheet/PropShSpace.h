#pragma once
#include "propshitem.h"

class PropShSpace :public PropShItem
{
protected:
  int _vspace;
  int _hspace;
public:
  PropShSpace(int vspace=10, int hspace=0);
  void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height);
};
