// PropShTextWithButton.h: interface for the PropShTextWithButton class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROPSHTEXTWITHBUTTON_H__689B5BFF_2095_40A9_9AF2_880C40CBBED5__INCLUDED_)
#define AFX_PROPSHTEXTWITHBUTTON_H__689B5BFF_2095_40A9_9AF2_880C40CBBED5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PropShTextLine.h"

class PropShTextWithButton : public PropShTextLine  
{
protected:
	HWND _hButton;
public:
	PropShTextWithButton();
	virtual ~PropShTextWithButton();

	virtual bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
	virtual void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height);
	virtual void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);
	virtual void OnButtonPressed() {}
	bool ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result);
    virtual void EnableItem(bool enable);
};

#endif // !defined(AFX_PROPSHTEXTWITHBUTTON_H__689B5BFF_2095_40A9_9AF2_880C40CBBED5__INCLUDED_)
