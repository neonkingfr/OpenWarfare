#pragma once
#include "propshtextwithbutton.h"
#include <float.h>

class PropShSliderLine :
  public PropShTextWithButton
{
  friend LRESULT WINAPI SliderDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
protected:
  HWND _sliderWnd;
  HWND _sliderCntr;

  double _rangeMin;
  double _rangeMax;
  int _steps;
  int _ticks;
  int _decimals;
public: enum Mode
  {
    Default,CheckRange,Relative,Wrap
  };
protected:
  Mode _mode;
  double _relpos;


  virtual LRESULT OnSliderDlgMsg(UINT msg, WPARAM wParam, LPARAM lParam);
public:
  PropShSliderLine(double minval=-DBL_MAX, double maxval=DBL_MAX,int sldrSteps=101, int sldrTicks=0, int decimals=-2, Mode mode=Default);
  ~PropShSliderLine(void);

  void OnButtonPressed();
  bool ReflectMessage(HWND hWnd,UINT message,WPARAM wParam, LPARAM lParam, LRESULT *result);
  void SetValue(double value);
  double GetValue();

};
