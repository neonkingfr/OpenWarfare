// ExtendedPropertySheet.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "PropShCommon.h"

#include "PropSheetWindow.h"
#include "PropShTextLine.h"
#include "propshcombo.h"
#include "propshgridgroup.h"
#include "propshtabgroup.h"
#include "PropShFileLine.h"
#include "PropShSliderLine.h"
#include "PropShTitleOnly.h"
#include "PropShSpace.h"
#include "PropShFrameGroup.h"
#include "PropShCheckBox.h"

#include ".\propshpanelgroup.h"
#include <Commdlg.h>



int main(int argc, char* argv[])
{

	PropSheetWindow wnd;
	wnd.Create("Pokus",WS_VISIBLE|WS_OVERLAPPEDWINDOW,10,10,300,600,NULL);
  
    PropShTabGroup *tab=new PropShTabGroup;
    wnd.AddItem(tab,_T("tab"),_T("TabGroup"));
    PropShGroup *grp,*grp2;

    tab->AddItem(grp=new PropShGroup,"Tab1","TabTest");
    
	grp->AddItem(new PropShTextLine,_T("item1"),_T("pokusny radek"));
    grp->AddItem(new PropShFileLine("Text files|*.txt|AllFiles|*.*","txt",false,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_SHAREAWARE,"c:\\testovani\\"),_T("item2"),_T("Textov� soubor"));
    grp->AddItem(new PropShSliderLine(-10,10,20,5,2),_T("item3"),_T("Slider test"));
    grp->AddItem(new PropShCheckBox(),_T(""),_T("Check Box 1"));
    grp->AddItem(new PropShCheckBox(PropShCheckBox::PushLike),_T(""),_T("Check Box 2"));
    grp->AddItem(new PropShCheckBox(PropShCheckBox::Flat),_T(""),_T("Check Box 3"));
    grp->AddItem(new PropShSpace(-100,0),_T(""),_T(""));
    grp->AddItem(new PropShTitleOnly,_T(""),_T("Title only"));
    grp->AddItem(new PropShTitleOnly(PropShTitleOnly::HorzLine),_T(""),_T(""));
    grp2=new PropShFrameGroup;
    grp->AddItem(grp2,_T("framegrp"),_T("Frame group"));    

    PropShCombo *combo=new PropShCombo(true);
    grp2->AddItem(combo,_T("combo"),_T("Combobox"));
    combo->AddItem("Ahoj");
    combo->AddItem("Nazdar");
    combo->AddItem("Cau");
    grp2->AddItem(new PropShTextLine,_T("item3"),_T("pokusny radek 3"));

    tab->AddItem(grp=new PropShGroup,_T("Tab2"),_T("FixedGroup"));
    
    grp2=new PropShPanelGroup(true);
    grp->AddItem(grp2,_T("Panel"),_T("Panel group"));
    grp=grp2;

	grp->AddItem(new PropShTextLine,_T("item1"),_T("pokusny radek"));
	grp->AddItem(new PropShTextLine,_T("item2"),_T("pokusny radek 2"));
	grp->AddItem(new PropShTextLine,_T("item3"),_T("pokusny radek 3"));

    PropShGridGroup *grid=new PropShGridGroup(4,true);
    tab->AddItem(grid,_T("grid"),_T("Grid Example"));
    for (int i=0;i<16;i++)
      grid->AddItem(new PropShTextLine,_T("abc"),_T("ABC"));
  

    grp=new PropShGroup;
    PropShTabGroup *subtab;
    tab->AddItem(subtab=new PropShTabGroup,_T("recursive"),_T("SubTab"));
    subtab->AddItem(grp,_T("t"),_T("Test"));
    wnd.SetFont("MS Sans Serif",12);   


	MSG msg;
	while (GetMessage(&msg,0,0,0))
      if (!wnd.IsMyMessage(&msg))
	{
	  TranslateMessage(&msg);
	  DispatchMessage(&msg);
	}

	return 0;
}
