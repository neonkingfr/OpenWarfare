#pragma once
#include "propshgroup.h"

class PropShFrameGroup :
  public PropShGroup
{
protected:
  HWND _frame;
  int _padding;
public:
  PropShFrameGroup(void);
  ~PropShFrameGroup(void);
  void UpdateGroupContent(PropShHDWP &hdwp, int x, int y, int wspace, int hspace, int &width, int &height);
  bool Create(const tchar *name, const tchar *title, PropShItem *parent);
  void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);

};
