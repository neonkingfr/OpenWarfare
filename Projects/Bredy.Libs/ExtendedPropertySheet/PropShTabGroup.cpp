#include "PropShCommon.h"
#include ".\propshtabgroup.h"
#include <commctrl.h>

PropShTabGroup::PropShTabGroup(void)
{
  _tab=NULL;
  _toHide=NULL;
  _adjustWidth=0;
  _adjustHeight=0;
}

PropShTabGroup::~PropShTabGroup(void)
{
  if (_tab) DestroyWindow(_tab); 
}

bool PropShTabGroup::Create(const tchar *name, const tchar *title, PropShItem *parent/*=NULL*/)
{
  if (PropShCanvas::Create(name,title,parent)==false) return false;
  InitCommonControls();
  _tab=CreateWindowEx(0,WC_TABCONTROL,title,WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|
      TCS_HOTTRACK|TCS_RAGGEDRIGHT|TCS_SINGLELINE|TCS_TABS|TCS_TOOLTIPS|WS_TABSTOP,
      0,0,0,0,*this,NULL,parent->GetInstance(),NULL);
  return _tab!=NULL;
}

bool PropShTabGroup::AddItem(PropShCanvas *item, const tchar *name, const tchar *title)
{
  if (item->Create(name,title,this)==false) return false;
  TCITEM tabitem;
  tabitem.mask=TCIF_TEXT|TCIF_PARAM;
  tabitem.pszText=(tchar *)title;
  tabitem.lParam=(LPARAM)item;


  int cnt=SendMessage(_tab,TCM_GETITEMCOUNT,0,0);
  SendMessage(_tab,TCM_INSERTITEM,cnt,(LPARAM)&tabitem);
  ShowWindow(*item,SW_HIDE);
  SetWindowPos(_tab,HWND_BOTTOM,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  return true;
}

void PropShTabGroup::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{  
  SetWindowPos(_tab,NULL,0,0,wspace,hspace,SWP_NOZORDER);    
  RECT rc;
  rc.left=rc.top=0;
  rc.bottom=hspace;
  rc.right=wspace;
  SendMessage(_tab,TCM_ADJUSTRECT,FALSE,(LPARAM)&rc);    
  width=rc.right-rc.left-_adjustWidth;
  height=rc.bottom-rc.top-_adjustHeight;
  int neww,newh;
  UpdateTabItem(rc.left,rc.top,width,height,neww,newh);
  if (neww!=rc.right-rc.left || newh!=rc.bottom-rc.top)
  {
    _adjustWidth=neww-width;
    _adjustHeight=newh-height;
    width-=_adjustWidth;
    height-=_adjustHeight;
    UpdateTabItem(rc.left,rc.top,width,height,width,height);
  }
  else
  {
    width=neww;
    height=newh;
  }
  rc.right=rc.left+width;
  rc.bottom=rc.top+height;
  SendMessage(_tab,TCM_ADJUSTRECT,TRUE,(LPARAM)&rc);    
  SetWindowPos(_tab,NULL,rc.left,rc.top,rc.right,rc.bottom,SWP_NOZORDER);  
  width=rc.right-rc.left;
  height=rc.bottom-rc.top;
  ResizeCanvas(hdwp,left,top,width,height,&width,&height);
}

void PropShTabGroup::UpdateTabItem(int left, int top, int wspace, int hspace,int &width, int &height)
{
  int current=SendMessage(_tab,TCM_GETCURSEL,0,0);
  if (current==-1) return;
  TCITEM item;
  item.mask=TCIF_PARAM;
  SendMessage(_tab,TCM_GETITEM,current,(LPARAM)&item);
  PropShCanvas *canvas=(PropShCanvas *)item.lParam;
  if (*canvas!=_toHide) {ShowWindow(_toHide,SW_HIDE);_toHide=*canvas;}
  ShowWindow(_toHide,SW_SHOW);  
  PropShHDWP hdwp;
  canvas->SetCanvasSize(wspace,hspace);
  canvas->UpdateLayout(hdwp,left,top,wspace,hspace,width,height);
}

LRESULT PropShTabGroup::OnMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  if (message==WM_NOTIFY)
  {
    LPNMHDR lpnmhdr = (LPNMHDR) lParam;
    if (lpnmhdr->hwndFrom==_tab && lpnmhdr->code==TCN_SELCHANGE) 
    {
      _adjustWidth=0;
      _adjustHeight=0;
      RecalcLayout();
    }
    else return PropShCanvas::OnMessage(message,wParam,lParam);
  }
  else if (message==WM_DESTROY)
  {
    for (int i=0,cnt=SendMessage(_tab,TCM_GETITEMCOUNT,0,0);i<cnt;i++)
    {
      TCITEM item;
      item.mask=TCIF_PARAM;
      SendMessage(_tab,TCM_GETITEM,i,(LPARAM)&item);
      PropShCanvas *canvas=(PropShCanvas *)item.lParam;
      delete canvas;
    }
  }
  else return PropShCanvas::OnMessage(message,wParam,lParam);
  return 0;
}

void PropShTabGroup::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  for (int i=0,cnt=SendMessage(_tab,TCM_GETITEMCOUNT,0,0);i<cnt;i++)
  {
    TCITEM item;
    item.mask=TCIF_PARAM;
    SendMessage(_tab,TCM_GETITEM,i,(LPARAM)&item);
    PropShCanvas *canvas=(PropShCanvas *)item.lParam;
    canvas->BroadcastMessage(message,wParam,lParam);
  }
  SendMessage(_tab,message,wParam,lParam);
}

bool PropShTabGroup::ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result)
{
  for (int i=0,cnt=SendMessage(_tab,TCM_GETITEMCOUNT,0,0);i<cnt;i++)
  {
    TCITEM item;
    item.mask=TCIF_PARAM;
    SendMessage(_tab,TCM_GETITEM,i,(LPARAM)&item);
    PropShCanvas *canvas=(PropShCanvas *)item.lParam;
    if (canvas->ReflectMessage(hWnd, msg, wParam, lParam, result)) return true;
  }
  return false;
}
