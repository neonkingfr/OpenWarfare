#include "PropShCommon.h"
#include ".\propshgroup.h"
#include <assert.h>

PropShGroup::PropShGroup(bool border):_items(NULL),_count(NULL),_reserved(NULL),_itemSpace(2),_border(border)
{
  _dirty=false;
}

PropShGroup::~PropShGroup(void)
{
  DeleteAllItems();
}


void PropShGroup::DeleteAllItems()
{
  PropShItem **tmp=_items;
  int cnt=_count;
  _count=0;
  _reserved=0;
  _items=0;

  for (int i=0;i<cnt;i++)
  {
    delete tmp[i];
  }
  free(tmp);
}

bool PropShGroup::Expand()
{
  if (_reserved==0) _reserved=32;
  else _reserved*=2;
  void *nx=realloc(_items,sizeof(PropShItem *)*_reserved);
  if (nx==0) return false;
  _items=(PropShItem **)nx;
  return true;
}

bool PropShGroup::AddItem(PropShItem *item)
{
  if (_count==_reserved)
    if (!Expand()) return false;
  _items[_count++]=item;
  _dirty=true;
  return true;
}

bool PropShGroup::AddItem(PropShItem *item, const tchar *name, const tchar *title)
{
  if (item->Create(name,title,this)==false) 
  {
    delete item;
    return false;
  }
  if (AddItem(item)==false)
  {
    delete item;
    return false;
  }
  return true;
}

int PropShGroup::FindItemIndex(const tchar *name) const
{
  for (int i=0;i<_count;i++) if (_tcsicmp(_items[i]->GetName(),name)==0) return i;
  return -1;
}

PropShBase *PropShGroup::Find(const tchar *name) const
{
  for (int i=0;i<_count;i++)
  {
    const tchar *nextPart=_items[i]->IsMyName(name);
    if (nextPart) 
    {
      if (*nextPart) return _items[i]->Find(name);
      else return _items[i];
    }
  }
  return NULL;
}

const tchar *PropShGroup::IsMyName(const tchar *name) const
{          
	  const tchar *myname=GetName();
      if (myname[0]==0) 
        if (Find(name)!=NULL) return name;
	  while (*name==*myname) 
		if (!*name) return name;
		else {name++;myname++;}
      if (*name=='.') return name+1;
	  return NULL;
}

void PropShGroup::DeleteItem(PropShItem *item)
{
  PropShItem *parent=item->GetParentItem();
  if (parent==this) 
  {
    int j=0;
    for (int i=0;i<_count;i++)
    {      
      if (_items[i]!=item) 
      {
        if (j!=i) _items[j]=_items[i];
        j++;
      }
      else
        _dirty=true;
    }
    _count=j;
    delete item;  
  }
  else
  {
    PropShGroup *grp;
#ifdef _CPPRTTI
    grp=dynamic_cast<PropShGroup *>(parent);
#else
    grp=static_cast<PropShGroup *>(parent);
#endif
    assert(grp!=0);
    grp->DeleteItem(item);
  }
}

void PropShGroup::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{
  
  int shiftx;
  int shifty;
  GetScrollOffset(shiftx,shifty);
  PropShHDWP subhdwp;


  int x=_paddingX-shiftx;
  int y=_paddingY-shifty;
  RECT rc;
  rc.left=rc.top=0;
  rc.right=wspace;
  rc.bottom=hspace;
  AdjustWindowRectEx(&rc,GetWindowLong(*this,GWL_STYLE),GetMenu(*this)!=NULL,GetWindowLong(*this,GWL_EXSTYLE));  
  int cwspace=2*wspace-(rc.right-rc.left);
  int chspace=2*hspace-(rc.bottom-rc.top);

  if (_xsize) wspace=cwspace=_xsize;
  if (_ysize) hspace=chspace=_ysize;

  cwspace-=2*_paddingX;
  chspace-=2*_paddingY;

  UpdateGroupContent(subhdwp,x,y,cwspace,chspace,width,height);
  width+=2*_paddingX;
  height+=2*_paddingY;

  ResizeCanvas(hdwp,left,top,width,height,&width,&height);
}

void PropShGroup::UpdateGroupContent(PropShHDWP &hdwp, int x, int y, int wspace, int hspace, int &width, int &height)
{
  int maxWidth=0;
  int h=y;
  for (int i=0,cnt=GetItemCount();i<cnt;i++)
  {
    int width;
    int height;
    if (i!=0) y+=_itemSpace;
    _items[i]->UpdateLayout(hdwp,x,y,wspace,hspace,width,height);
    if (width>maxWidth) maxWidth=width;
    y+=height;
  }
  width=maxWidth;
  height=y-h;
}

void PropShGroup::ScrollPosChanged()
{
  RecalcLayout();

/*  int shiftx;
  int shifty;
  GetScrollOffset(shiftx,shifty);

  int x=_paddingX;
  int y=_paddingY;
  PropShHDWP hdwp;

  for (int i=0,cnt=GetItemCount();i<cnt;i++)
  {
	RECT rc;
	GetWindowRect(*_items[i],&rc);
	int height=rc.bottom-rc.top;
    if (i!=0) y+=_itemSpace;
  	hdwp.SetWindowPos(*_items[i],x,y);
	y+=height;
  }*/
}

void PropShGroup::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  for (int i=0;i<_count;i++) 
    _items[i]->BroadcastMessage(message,wParam,lParam);
}

HWND PropShGroup::CreateCanvasWindow(const tchar *className, const tchar *title, HWND parent, HINSTANCE hInst)
{
  return CreateWindowEx(WS_EX_CONTROLPARENT,className,title,WS_CHILD|WS_VISIBLE|(_border?WS_BORDER:0),0,0,0,0,parent,NULL,hInst,this);
}

bool PropShGroup::ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result)
{
  for (int i=0;i<_count;i++) 
    if (_items[i]->ReflectMessage(hWnd, msg, wParam, lParam, result)) return true;
  return false;
}