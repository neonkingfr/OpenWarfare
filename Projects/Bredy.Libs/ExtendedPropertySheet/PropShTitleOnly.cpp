#include "PropShCommon.h"
#include ".\propshtitleonly.h"
#include "WindowPos.h"

PropShTitleOnly::PropShTitleOnly(Style style):_style(style)
{
  _hTitle=NULL;
}

PropShTitleOnly::~PropShTitleOnly(void)
{
  DestroyWindow(_hTitle);
}

bool PropShTitleOnly::Create(const tchar *name, const tchar *title, PropShItem *parent/*=NULL*/)
{
  if (PropShItem::Create(name,title,parent)==false) return false;
  HWND hparent=parent?parent->GetWindowHandle():NULL;
  HINSTANCE hInst=hparent?(HINSTANCE)GetWindowLong(hparent,GWL_HINSTANCE):GetModuleHandle(NULL);
  _hTitle=CreateWindow("STATIC",title,WS_CHILD|_style|SS_CENTERIMAGE|SS_ENDELLIPSIS|WS_VISIBLE,0,0,100,100,hparent,NULL,hInst,NULL);
  return _hTitle!=NULL;	
}

void PropShTitleOnly::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{
  if (_style!=HorzLine)
  {
    HDC infc=::GetDC(_hTitle);
    HFONT font=(HFONT)SendMessage(_hTitle,WM_GETFONT,0,0);
    HFONT old=(HFONT)SelectObject(infc,font);
    SIZE sz;
    GetTextExtentPoint32(infc,"W",1,&sz);
    height=sz.cy+2;
    hdwp.SetWindowPos(_hTitle,left,top,wspace,height);
    width=wspace;  
    SelectObject(infc,old);
    ReleaseDC(_hTitle,infc);
  }
  else
  {
    height=5;
    hdwp.SetWindowPos(_hTitle,left,top+2,wspace,2);
    width=wspace;
  }
  InvalidateRect(_hTitle,NULL,TRUE);
}

void PropShTitleOnly::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  SendMessage(_hTitle,message,wParam,lParam);
}

int PropShTitleOnly::ResizeTitle(PropShHDWP &dwp, int left, int top, int wspace, int hspace)
{
  float percentsize;
  GetParentItem()->GetItemProperty(PROPSH_TITLESIZE,percentsize,0.5f);
  int midl=percentsize<1?((int)(percentsize*wspace)):((int)percentsize);
  dwp.SetWindowPos(_hTitle,left,top,midl,hspace);
  InvalidateRect(_hTitle,NULL,TRUE);
  return midl;
}

int PropShTitleOnly::CalculateFontHeight(HWND hControl)
{
/*  HDC infc=::GetDC(_hControl);
  HFONT font=(HFONT)SendMessage(hControl,WM_GETFONT,0,0);
  HFONT old=(HFONT)SelectObject(infc,font);
  SIZE sz;
  GetTextExtentPoint32(infc,"W",1,&sz);
  int height=sz.cy+2;
  SelectObject(infc,old);
  ReleaseDC(_hControl,infc);
  */  
  return GetWindowsTextSize(hControl).cy;
}