// PropShItem.h: interface for the PropShItem class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PropShItem_H__AB0E8138_A527_4229_B2C7_DF86236A94B8__INCLUDED_)
#define AFX_PropShItem_H__AB0E8138_A527_4229_B2C7_DF86236A94B8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "PropShBase.h"

#define PROPCMD_ACTIONBUTTON1 9990 //Alt + arrow down
#define PROPCMD_ACTIONBUTTON2 9991 //Alt + arrow up
#define PROPCMD_ACTIONBUTTON3 9992 //Alt + arrow left
#define PROPCMD_ACTIONBUTTON4 9993 //Alt + arrow right
#define PROPCMD_ACTIONBUTTON5 9994 //Ctrl + space

class PropShHDWP
{
  HDWP _hdwp;
public:
  PropShHDWP(int wincount=100):_hdwp(BeginDeferWindowPos(wincount)) {}
  ~PropShHDWP() 
    {BOOL res=EndDeferWindowPos(_hdwp);
    assert(res==TRUE);
    }
  
  void SetWindowPos(HWND hWnd, int x, int y, int cx, int cy)
	{
      _hdwp=DeferWindowPos(_hdwp,hWnd,NULL,x,y,cx,cy,SWP_NOZORDER);
      assert(_hdwp);
    }
  void SetWindowPos(HWND hWnd, int x, int y)
	{
      _hdwp=DeferWindowPos(_hdwp,hWnd,NULL,x,y,0,0,SWP_NOZORDER|SWP_NOSIZE);
      assert(_hdwp);
    }
  void SetWindowSize(HWND hWnd, int cx, int cy)
	{
      _hdwp=DeferWindowPos(_hdwp,hWnd,NULL,0,0,cx,cy,SWP_NOZORDER|SWP_NOMOVE);
      assert(_hdwp);
    }
  void SetWindowRect(HWND hWnd, const RECT &rc)
	{
      _hdwp=DeferWindowPos(_hdwp,hWnd,NULL,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,SWP_NOZORDER);
      assert(_hdwp);
    }

  void Flush(int newwndcount=100)
  {
    BOOL res=EndDeferWindowPos(_hdwp);
    assert(res==TRUE);
    _hdwp=BeginDeferWindowPos(newwndcount);
  }

};

/*
class PropShHDWP
{
  
public:
  
  void SetWindowPos(HWND hWnd, int x, int y, int cx, int cy)
	{::SetWindowPos(hWnd,NULL,x,y,cx,cy,SWP_NOZORDER);}
  void SetWindowPos(HWND hWnd, int x, int y)
	{::SetWindowPos(hWnd,NULL,x,y,0,0,SWP_NOZORDER|SWP_NOSIZE);}
  void SetWindowSize(HWND hWnd, int cx, int cy)
	{::SetWindowPos(hWnd,NULL,0,0,cx,cy,SWP_NOZORDER|SWP_NOMOVE);}
  void SetWindowRect(HWND hWnd, const RECT &rc)
	{::SetWindowPos(hWnd,NULL,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,SWP_NOZORDER);}

};
*/
class PropShItem;

class IPropShEvent
{
public:
  
  class ActionData
  {
    int _command;
  public:
    ActionData(int command):_command(command) {}
    virtual ~ActionData() {};    
    int GetCommand() const {return _command;}
    bool TestCommand(int command) const {return _command==command;}
    bool operator==(int c) const {return TestCommand(c);}
    bool operator!=(int c) const {return !TestCommand(c);}    
  };

  ///Informs interface, that layout is dirty and should be updated
  /** @param item the item that has been changed and caused that layout is dirty
      @param animatedChange if this var true, layout is changed during animation
      */
  virtual void LayoutDirty(PropShItem *item,bool animatedChange=false);
  virtual void Action(PropShItem *item,const ActionData &action) {}

  IPropShEvent &ThrowEvent(PropShItem *item, IPropShEvent *curEvent) const;
};

class PropShItem : public PropShBase  
{
  friend IPropShEvent;
    IPropShEvent *_event;
protected:
  	tchar *_name;
	PropShItem *_parent;
public:
	PropShItem();
	virtual ~PropShItem();

	virtual bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
	virtual HWND GetWindowHandle() {return NULL;} //default - no handle
	virtual const tchar *GetName() const {return _name;}
	virtual void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam) {}
    PropShItem *GetParentItem() const {return _parent;}
    virtual void LayoutChanged();
    
    IPropShEvent &ThrowEvent() const;
    void SetEventCatcher(IPropShEvent &catcher) {_event=&catcher;}

    operator HWND() {return this?GetWindowHandle():NULL;}
    IPropShEvent *GetEventInterface() {return _event;}
    HINSTANCE GetInstance();    
	virtual bool ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result) {return false;}    
    virtual void EnableItem(bool enable) {if (GetWindowHandle()!=NULL) EnableWindow(GetWindowHandle(),enable);}

};

#endif // !defined(AFX_PropShItem_H__AB0E8138_A527_4229_B2C7_DF86236A94B8__INCLUDED_)
