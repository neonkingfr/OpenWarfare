// PropShCanvas.cpp: implementation of the PropShCanvas class.
//
//////////////////////////////////////////////////////////////////////

#include "PropShCommon.h"
#include "PropShCanvas.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define PropShCanvasName _T("PropShCanvasClass@BIS@Bredy")

PropShCanvas::PropShCanvas():_hCanvas(NULL),_paddingX(3),_paddingY(3),_xsize(0),_ysize(0),
    _nohscroll(false), _novscroll(false),_titleSize(0.5f)
{

}

PropShCanvas::~PropShCanvas()
{
  if (_hCanvas) DestroyWindow(_hCanvas);
}

bool PropShCanvas::Create(const tchar *name, const tchar *title, PropShItem *parent/*=NULL*/)
{
  if (PropShItem::Create(name,title,parent)==false) return false;
  return CreateCanvas(name,title, parent);
}

static bool classRegistred=false;

static LRESULT CanvasWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  if (message==WM_CREATE)
  {
	LPCREATESTRUCT lpcs=(LPCREATESTRUCT)lParam;
	SetWindowLong(hWnd,GWL_USERDATA,(LONG)lpcs->lpCreateParams);
	PropShCanvas *self=(PropShCanvas *)lpcs->lpCreateParams;
	return self->OnCreate(lpcs)?0:-1;
  }
  else
  {
	PropShCanvas *self=(PropShCanvas *)GetWindowLong(hWnd,GWL_USERDATA);
	if (self) return self->OnMessage(message,wParam,lParam);
	else return DefWindowProc(hWnd,message,wParam,lParam);
  }
}

static bool RegisterCanvasClass(HINSTANCE hInst, const tchar *regName)
{
  WNDCLASS cls;
  if (GetClassInfo(hInst,regName,&cls)) return true;
  memset(&cls,0,sizeof(cls));
  cls.hbrBackground=(HBRUSH)(COLOR_BTNFACE+1);
  cls.hCursor=LoadCursor(NULL,IDC_ARROW);
  cls.hInstance=hInst;
  cls.lpfnWndProc=(WNDPROC)CanvasWndProc;
  cls.lpszClassName=regName;
  cls.style=0;
  return RegisterClass(&cls)!=0;

}

bool PropShCanvas::CreateCanvas(const tchar *name, const tchar *title, PropShItem *parent, HINSTANCE hInst/*=NULL*/)
{
  HWND hparent=parent?parent->GetWindowHandle():NULL;  
  if (hInst==NULL) 
	if (hparent==NULL)
	  hInst=GetModuleHandle(NULL);
	else
	  hInst=(HINSTANCE)GetWindowLong(hparent,GWL_HINSTANCE);
  RegisterCanvasClass(hInst,PropShCanvasName);  
  _hCanvas=CreateCanvasWindow(PropShCanvasName,title,hparent,hInst);
  return _hCanvas!=NULL;
}

HWND PropShCanvas::CreateCanvasWindow(const tchar *className, const tchar *title, HWND parent, HINSTANCE hInst)
{
  return CreateWindowEx(WS_EX_CONTROLPARENT,className,title,WS_CHILD|WS_VISIBLE,0,0,0,0,parent,NULL,hInst,this);
}

LRESULT PropShCanvas::OnCommand(WORD wNotifyCode,WORD wID, HWND ctl)
{
  return 0;
}


void PropShCanvas::ResizeWindow(PropShHDWP &hdwp,int left, int top, int cx,int cy,int scroll,int *outCx, int *outCy)
{
  HWND hWnd=*this;
  RECT rc;
  rc.left=0;
  rc.top=0;
  rc.right=cx;
  rc.bottom=cy;
  AdjustWindowRectEx(&rc,GetWindowLong(hWnd,GWL_STYLE),GetMenu(hWnd)!=NULL,GetWindowLong(hWnd,GWL_EXSTYLE));  
  if (scroll & WS_VSCROLL) rc.right+=GetSystemMetrics(SM_CXVSCROLL);
  if (scroll & WS_HSCROLL) rc.bottom+=GetSystemMetrics(SM_CYHSCROLL);
  hdwp.SetWindowPos(hWnd,left,top,rc.right-rc.left,rc.bottom-rc.top);  
  if (outCx) *outCx=rc.right-rc.left;
  if (outCy) *outCy=rc.bottom-rc.top;
}

void PropShCanvas::ResizeCanvas(PropShHDWP &hdwp, int left, int top, int cx,int cy,int *outCx, int *outCy)
{
  int rszx,rszy;
  if (_xsize) rszx=_xsize;else rszx=cx;
  if (_ysize) rszy=_ysize;else rszy=cy;

  int scroll=0;
  if (rszx<cx) if (_nohscroll) rszx=cx; else scroll|=WS_HSCROLL;
  if (rszy<cy) if (_novscroll) rszy=cy; else scroll|=WS_VSCROLL;
  if (rszx>=cx || rszy>=cy)
  {
     int shiftx,shifty;
     GetScrollOffset(shiftx,shifty);
     if (shiftx && rszx>=cx) {scroll|=WS_HSCROLL;cx=rszx+shiftx+1;}
     if (shifty && rszy>=cy) {scroll|=WS_VSCROLL;cy=rszy+shifty+1;}
  }


  ResizeWindow(hdwp,left,top,rszx,rszy,scroll,outCx,outCy);


  SCROLLINFO nfo;
  memset(&nfo,0,sizeof(nfo));
  nfo.cbSize=sizeof(nfo);
  nfo.fMask=SIF_PAGE|SIF_RANGE;
  {
    nfo.nMax=cx-1;
    nfo.nMin=0;
    nfo.nPage=rszx;
    SetScrollInfo(*this,SB_HORZ,&nfo,TRUE);
  }
  {
    nfo.nMax=cy-1;
    nfo.nMin=0;
    nfo.nPage=rszy;
    SetScrollInfo(*this,SB_VERT,&nfo,TRUE);
  }
}

LRESULT PropShCanvas::OnMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  LRESULT res;
  switch (message)
  {
  case WM_COMMAND:
	if (ReflectMessage((HWND)lParam,message,wParam,lParam,&res)) return res;
	return OnCommand(HIWORD(wParam),LOWORD(wParam),(HWND)lParam);  
  case WM_CTLCOLORMSGBOX:
  case WM_CTLCOLOREDIT:
  case WM_CTLCOLORLISTBOX:
  case WM_CTLCOLORBTN:
  case WM_CTLCOLORDLG:
  case WM_CTLCOLORSCROLLBAR:
  case WM_CTLCOLORSTATIC:
	if (ReflectMessage((HWND)lParam,message,wParam,lParam,&res)) return res;
	return DefWindowProc(_hCanvas,message,wParam,lParam);
  case WM_DRAWITEM:
	{
	  LPDRAWITEMSTRUCT item=(LPDRAWITEMSTRUCT)lParam;
  	  if (ReflectMessage(item->hwndItem,message,wParam,lParam,&res)) return res;
  	  return DefWindowProc(_hCanvas,message,wParam,lParam);
	}
  case WM_MEASUREITEM:
	if (ReflectMessage(NULL,message,wParam,lParam,&res)) return res;
	return DefWindowProc(_hCanvas,message,wParam,lParam);
  case WM_COMPAREITEM:
	{
	  LPCOMPAREITEMSTRUCT item=(LPCOMPAREITEMSTRUCT)lParam;
  	  if (ReflectMessage(item->hwndItem,message,wParam,lParam,&res)) return res;
  	  return DefWindowProc(_hCanvas,message,wParam,lParam);
	}
  case WM_NOTIFY:
	{
	  LPNMHDR item=(LPNMHDR)lParam;
  	  if (ReflectMessage(item->hwndFrom,message,wParam,lParam,&res)) return res;
  	  return DefWindowProc(_hCanvas,message,wParam,lParam);
	}
  case WM_DELETEITEM:
	{
	  LPDELETEITEMSTRUCT item=(LPDELETEITEMSTRUCT)lParam;
  	  if (ReflectMessage(item->hwndItem,message,wParam,lParam,&res)) return res;
  	  return DefWindowProc(_hCanvas,message,wParam,lParam);
	}  
  case WM_VSCROLL: if (lParam==NULL)
    {
      SCROLLINFO nfo;
      nfo.cbSize=sizeof(nfo);
      nfo.fMask=SIF_PAGE|SIF_RANGE|SIF_POS;
      GetScrollInfo(*this,SB_VERT,&nfo);
      int trackPos=HIWORD(wParam);
      switch (LOWORD(wParam))
      {
      case SB_BOTTOM: trackPos=nfo.nMax;break;
      case SB_TOP: trackPos=nfo.nMin;break;
      case SB_LINEDOWN: trackPos=nfo.nPos+8;break;
      case SB_LINEUP: trackPos=nfo.nPos-8;break;
      case SB_PAGEDOWN: trackPos=nfo.nPos+nfo.nPage/2;break;
      case SB_PAGEUP: trackPos=nfo.nPos-nfo.nPage/2;break;
      case SB_THUMBPOSITION: 
      case SB_THUMBTRACK: break;
      case SB_ENDSCROLL: return 0;
      }
      SetScrollPos(*this,SB_VERT,trackPos,TRUE);
      ScrollPosChanged();
      break;
    }
	else
	{
  	  if (ReflectMessage((HWND)lParam,message,wParam,lParam,&res)) return res;
  	  return DefWindowProc(_hCanvas,message,wParam,lParam);
	}
  case WM_HSCROLL: if (lParam==NULL)
    {
      SCROLLINFO nfo;
      nfo.cbSize=sizeof(nfo);
      nfo.fMask=SIF_PAGE|SIF_RANGE|SIF_POS;
      GetScrollInfo(*this,SB_HORZ,&nfo);
      int trackPos=HIWORD(wParam);
      switch (LOWORD(wParam))
      {
      case SB_RIGHT: trackPos=nfo.nMax;break;
      case SB_LEFT: trackPos=nfo.nMin;break;
      case SB_LINERIGHT: trackPos+=8;break;
      case SB_LINELEFT: trackPos-=8;break;
      case SB_PAGERIGHT: trackPos+=nfo.nPage/2;break;
      case SB_PAGELEFT: trackPos-=nfo.nPage/2;break;
      case SB_THUMBPOSITION: 
      case SB_THUMBTRACK: break;
      case SB_ENDSCROLL: return 0;
      }
      SetScrollPos(*this,SB_HORZ,trackPos,TRUE);
      ScrollPosChanged();
      break;
    }
	else
	{
  	  if (ReflectMessage((HWND)lParam,message,wParam,lParam,&res)) return res;
  	  return DefWindowProc(_hCanvas,message,wParam,lParam);
	}
  default: 	return DefWindowProc(_hCanvas,message,wParam,lParam);
  }
  return 0;
}

void PropShCanvas::GetScrollOffset(int &x, int &y)
{
  long scroll=GetWindowLong(*this,GWL_STYLE);
  if (scroll & WS_HSCROLL) x=GetScrollPos(*this,SB_HORZ); else x=0;
  if (scroll & WS_VSCROLL) y=GetScrollPos(*this,SB_VERT);else y=0;
}

void PropShCanvas::RecalcLayout()
{
    HWND hWnd=*this;
    assert(hWnd!=NULL);
    RECT rc;
	RECT wrc;
    GetClientRect(hWnd,&rc);
	GetWindowRect(hWnd,&wrc);
    HWND parent=GetParent(hWnd);
    if (parent) 
    {
      ScreenToClient(parent,(POINT *)&wrc);
      ScreenToClient(parent,(POINT *)&wrc+1);
    }
    int width;
    int height;
    PropShHDWP hdwp;
    UpdateLayout(hdwp,wrc.left,wrc.top,rc.right,rc.bottom,width,height);
    hdwp.Flush();
    UpdateWindow(hWnd);
}

void PropShCanvas::LayoutChanged()
{
  if (_parent) _parent->LayoutChanged();
  else RecalcLayout();
}


bool PropShCanvas::ItemPropertySupport(int propertyId, void *data, const type_info &type, bool retrieve)
{
  switch (propertyId)
  {
  case PROPSH_TITLESIZE:
    if (type==typeid(float))
    {
      float &val=*(float *)data;
      if (retrieve) val=_titleSize;
      else _titleSize=val;
      return true;
    }
  }
  return __super::ItemPropertySupport(propertyId,data,type,retrieve);
}
