#pragma once


///returns false, if window is not fully visible. newpos contains recomended position
static inline bool EnsureWindowVisible(HWND hWnd, POINT &newpos)
{
  HMONITOR mon=MonitorFromWindow(hWnd,MONITOR_DEFAULTTONEAREST);
  MONITORINFO minfo;
  minfo.cbSize=sizeof(minfo);
  GetMonitorInfo(mon,&minfo);
  RECT rc;
  GetWindowRect(hWnd,&rc);
  newpos.x=rc.left;
  newpos.y=rc.top;
  if (rc.left<minfo.rcWork.left) newpos.x=minfo.rcWork.left;
  if (rc.top<minfo.rcWork.top) newpos.y=minfo.rcWork.top;
  if (rc.bottom>minfo.rcWork.bottom) newpos.y=minfo.rcWork.bottom-(rc.bottom-rc.top);
  if (rc.right>minfo.rcWork.right) newpos.x=minfo.rcWork.right-(rc.right-rc.left);
  return rc.left!=newpos.x || rc.top!=newpos.y;

}

static inline SIZE GetWindowsTextSize(HWND hWnd, const tchar *text=_T("W"), int textsz=-1)
{
   if (textsz<0) textsz=_tcslen(text);
   HFONT font=(HFONT)SendMessage(hWnd,WM_GETFONT,0,0);
   HDC wdc=GetDC(hWnd);
   HFONT oldfnd=(HFONT)SelectObject(wdc,font);
   SIZE sz;
   GetTextExtentPoint32(wdc,text,textsz,&sz);
   SelectObject(wdc,oldfnd);
   ReleaseDC(hWnd,wdc);
   return sz;
}

static inline int GetWindowsTextSizeFixedWith(HWND hWnd, int width, const tchar *text, int textsz=-1)
{
  
  if (textsz<0) textsz=_tcslen(text);
   HFONT font=(HFONT)SendMessage(hWnd,WM_GETFONT,0,0);
   HDC wdc=GetDC(hWnd);
   HFONT oldfnd=(HFONT)SelectObject(wdc,font);
   SIZE sz;
   RECT rc;
   rc.left=0;
   rc.top=0;
   rc.right=width;
   rc.bottom=1;
   int height=DrawText(wdc,text,textsz,&rc,DT_CALCRECT|DT_WORDBREAK);
   GetTextExtentPoint32(wdc,text,textsz,&sz);
   SelectObject(wdc,oldfnd);
   ReleaseDC(hWnd,wdc);   
   if (rc.right>width) height*=(1+rc.right/width);
   return height;
}