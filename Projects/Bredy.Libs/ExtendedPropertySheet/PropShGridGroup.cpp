#include "PropShCommon.h"
#include ".\propshgridgroup.h"
#include <stdlib.h>

PropShGridGroup::PropShGridGroup(int columncnt,bool border,bool righttoleft): _cols(columncnt),PropShGroup(border),_righttoleft(righttoleft)
{
  _minwidthitem=-1;
}

void PropShGridGroup::UpdateGroupContent(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{
  int sspace=(wspace-(_cols-1)*_itemSpace)/_cols;  
  int colcnt=_cols;
  int maxWidth=0;
  int maxHeight=0;
  if (_minwidthitem<0 || _minwidthitem>=GetItemCount()) {_minwidthitem=-1;_minwidth=0;}
  int defwdth=__max(sspace,_minwidth);
  int x=_righttoleft?(wspace-(defwdth+_itemSpace)):0;
  int y=0;
  int cnt=cnt=GetItemCount();
  for (int i=0;i<cnt;i++)
  {
    int width;
    int height;
    if (i!=0 && colcnt==_cols) y+=_itemSpace;
    _items[i]->UpdateLayout(hdwp,x+left,y+top,i==_minwidthitem?sspace:defwdth,hspace,width,height);    
    if (width>sspace) 
    {
      if (width>_minwidth) {_minwidthitem=i;_minwidth=width;};
      width=width+_itemSpace;
    }
    else width=sspace+_itemSpace;
    if (_righttoleft) x-=defwdth;else x+=width;
    colcnt--;
    if (height>maxHeight) maxHeight=height;
    if (colcnt==0 || x<0) 
    {
      colcnt=_cols;
      y=y+maxHeight;
      maxHeight=0;
      x-=_itemSpace;
      if (x>maxWidth) maxWidth=x;
      x=_righttoleft?(wspace-(defwdth+_itemSpace)):0;;
    }
  }
  if (_righttoleft) x=wspace;
  width=__max(maxWidth,x);
  if (cnt==_cols) height=y;
  else height=y+maxHeight;
}
