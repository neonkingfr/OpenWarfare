#if !defined(AFX_ISTRINGRES_H__69BE0992_F681_49E8_BCC8_350B0EBCF1D3__INCLUDED_)
#define AFX_ISTRINGRES_H__69BE0992_F681_49E8_BCC8_350B0EBCF1D3__INCLUDED_

#pragma once

class IStringResource
{
public:
  virtual const char *operator[](int stringId)=0;
  virtual ~IStringResource() {}
};

#endif