// StringRes.h: interface for the CStringRes class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STRINGRES_H__69BE0992_F681_49E8_BCC8_350B0EBCF1D3__INCLUDED_)
#define AFX_STRINGRES_H__69BE0992_F681_49E8_BCC8_350B0EBCF1D3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BTree.h"
#include "IResourceContainer.hpp"

struct SStringResInfo
  {
  DWORD idc;
  _TCHAR *text;
  bool operator==(const SStringResInfo &other) const {return idc==other.idc;}
  bool operator>=(const SStringResInfo &other) const {return idc>=other.idc;}
  bool operator<=(const SStringResInfo &other) const {return idc<=other.idc;}
  bool operator!=(const SStringResInfo &other) const {return idc!=other.idc;}
  bool operator>(const SStringResInfo &other) const {return idc>other.idc;}
  bool operator<(const SStringResInfo &other) const {return idc<other.idc;}
  };

typedef const _TCHAR *CStringResConstChar;

class CStringRes:public IResourceContainer<const _TCHAR *,int>
{
  BTree<SStringResInfo> strlist;  
  HINSTANCE hInst;
public:
	CStringRes();
	void SetInstance(HINSTANCE h) {hInst=h;}
	const _TCHAR *operator[](int idc);
	virtual ~CStringRes();

};


#endif // !defined(AFX_STRINGRES_H__69BE0992_F681_49E8_BCC8_350B0EBCF1D3__INCLUDED_)
