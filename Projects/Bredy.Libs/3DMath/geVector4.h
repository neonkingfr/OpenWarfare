#ifndef __GEVECTOR4__H__05464065749840616089740964160_ONDREJ_NOVAK_
#define __GEVECTOR4__H__05464065749840616089740964160_ONDREJ_NOVAK_

enum geAxis 
{geAX=0,geAY=1,geAZ=2,geAW=3};

typedef const float *CHVECTOR;

#include <math.h>
#include <float.h>

class geVector4
{
  public:
    union
    {
      struct
      {
        float x;
        float y;
        float z;
        union
        {
          float w;
          unsigned long uflags;
          unsigned long sflags;		  
        };
      };
      float _e[4];  
    };
    
    geVector4(float x=0,float y=0,float z=0,float w=1)
    {_e[geAX]=x;_e[geAY]=y;_e[geAZ]=z;_e[geAW]=w;}
    geVector4(CHVECTOR v)
    {_e[geAX]=v[geAX];_e[geAY]=v[geAY];_e[geAZ]=v[geAZ];_e[geAW]=v[geAW];}
    
    //Default copy constructor not needed
    //Default assigment not needed
    //---------------------------
    
  public:
    operator float *() 
    {return _e;}
    operator const float *() const 
    {return _e;} 
    
  public:
    geVector4 operator+(const geVector4& other) const
    {
      return geVector4(_e[0]+other._e[0],_e[1]+other._e[1],_e[2]+other._e[2]);
    }
    geVector4 operator-(const geVector4& other) const
    {
      return geVector4(_e[0]-other._e[0],_e[1]-other._e[1],_e[2]-other._e[2]);
    }
    float operator*(const geVector4& other) const
    {
      return _e[0]*other._e[0]+_e[1]*other._e[1]+_e[2]*other._e[2];
    }
    geVector4 operator^(const geVector4& other) const
    {
      return geVector4(_e[1]*other._e[2]-_e[2]*other._e[1],
      _e[2]*other._e[0]-_e[0]*other._e[2],
      _e[0]*other._e[1]-_e[1]*other._e[0]);					 
    }
    geVector4 operator*(float value) const
    {
      return geVector4(_e[0]*value,_e[1]*value,_e[2]*value);
    }
    geVector4 operator/(float value) const
    {
      return operator*(1.0f/value);
    }
    geVector4& operator+=(const geVector4 &other)
    {
      _e[0]+=other._e[0];
      _e[1]+=other._e[1];
      _e[2]+=other._e[2];
      return *this;
    }
    geVector4& operator-=(const geVector4 &other)
    {
      _e[0]-=other._e[0];
      _e[1]-=other._e[1];
      _e[2]-=other._e[2];
      return *this;
    }
    geVector4& operator*=(float value)
    {
      _e[0]*=value;
      _e[1]*=value;
      _e[2]*=value;
      return *this;
    }
    geVector4& operator/=(float value)
    {
      return operator*=(1.0f/value);
    }
    geVector4 operator-() const
    {
      return geVector4(-_e[0],-_e[1],-_e[2]);
    }
    float operator~() const  //module ^2
    {
      return (*this)*(*this);
    }
    float Module2() const
    {
      return (*this)*(*this);
    }
    float Module() const
    {
      return (float)sqrt(Module2());
    }
    geVector4 Norm(float n=1) const
    {
      float z=n/(float)sqrt(operator~());	  
      return geVector4(_e[0]*z,_e[1]*z,_e[2]*z);
    }
    const float operator[] (unsigned int idx) const 
    {return _e[idx & 3];}
    float& operator[] (unsigned int idx) 
    {return _e[idx & 3];}
    
    //Operations	
    void Extend(float val=1.0f) 
    {_e[3]=val;} //sets w parameters to extend 3D vector for 3D Transformations
    void Correct() 
    {operator/=(_e[3]);_e[3]=1.0f;}	//Corrects x,y,z using w parameter and w sets to  1.0
    bool IsFinite() const //Returns true, if vector is valid (not infinite)
    {return _finite(_e[0]) && _finite(_e[1]) && _finite(_e[2]);}
    float Distance(const geVector4& v) const //Calculates distance to another point
    {
      return (float)sqrt((_e[0]-v._e[0])*(_e[0]-v._e[0])+(_e[1]-v._e[1])*(_e[1]-v._e[1])+(_e[2]-v._e[2])*(_e[2]-v._e[2]));
    }
    float Distance2(const geVector4& v) const //Calculates square distance to another point
    {
      return (_e[0]-v._e[0])*(_e[0]-v._e[0])+(_e[1]-v._e[1])*(_e[1]-v._e[1])+(_e[2]-v._e[2])*(_e[2]-v._e[2]);
    }
    friend float CosAngle(const geVector4& v1, const geVector4& v2)	//Calculates cosinus of angle between two vectors
    {
      return v1*v2/(float)sqrt(~v1*~v2);
    }	  
    friend geVector4 Interpolate(const geVector4& v1,const geVector4& v2, float f) //calculate linear interpolation
    {
      return geVector4(v1._e[0]+(v2._e[0]-v1._e[0])*f,v1._e[1]+(v2._e[1]-v1._e[1])*f,v1._e[2]+(v2._e[2]-v1._e[2])*f,1.0f);
    }
    friend geVector4 operator*(float f,const geVector4& v)	
    {
      return geVector4(v._e[0]*f,v._e[1]*f,v._e[2]*f);
    }
};

inline float fpi() 
{return 3.1415926535897932384626433832795f;}

inline float fpi(float mult) 
{return fpi()*mult;}

#if defined(AFX_CTARCHIVE_H__F66DAB8E_9620_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
inline ctArchive& operator>>=(ctArchive &arch, geVector4 data)
{
  arch.serial(&data,sizeof(data));
  return arch;
}

#endif
#endif