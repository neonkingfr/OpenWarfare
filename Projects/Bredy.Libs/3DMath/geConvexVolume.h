// geConvexVolume.h: interface for the geConvexVolume class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GECONVEXVOLUME_H__A376DB23_6640_4A3A_8A12_179369EBB6EC__INCLUDED_)
#define AFX_GECONVEXVOLUME_H__A376DB23_6640_4A3A_8A12_179369EBB6EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//geCOnvexVolume popisuje jakykoliv konvexni (ohraniceny) prostor. Je to abstraktni trida

#include "gePlane.h"
#include "geMatrix4.h"
#include "geVector4.h"

#define GECV_ENTERPT 0x1
#define GECV_EXITPT 0x2
#define GECV_ENTERPL 0x4
#define GECV_EXITPL 0x8
#define GECV_ALL (GECV_ENTERPT|GECV_EXITPT|GECV_ENTERPL|GECV_EXITPL)

class ctArchive;
class geConvexVolume  
{
  public:
    struct TestInfo
    {
      unsigned long flags;	  //flags which items are important
      float enterpt;		  //poloha bodu, ktery vstupuje do objemu
      float exitpt;		  //poloha bodu, ktery vystupuje z objemu
      gePlaneGen enterplane;	//tecna plocha, kterou primka vstupuje
      gePlaneGen exitplane;	//tecna plocha, kterou primka vystupuje
      TestInfo(unsigned long flgs=GECV_ALL): flags(flgs) 
      {}
    };
  public:
    virtual bool TestPoint(const geVector4& pt)=0; //testovani zda bod lezi vne nebo uvnitr
    virtual bool TestLine(const geLine& pt, TestInfo* info=NULL)=0; //test, zda primka protina prostor
    virtual geVector4 GetCenter()=0;
    virtual ~geConvexVolume() 
    {}	  //virtualni destruktor
};

//Testuje bod, zda lezi v konvexnim objemu
bool geTestPoint(const geVector4 &pt, const gePlaneGen *planes, int count);
//Testuje primku, zda protina konvexni objem
bool geTestLine(const geLine& ln, const gePlaneGen *planes, int count, float sign, geConvexVolume::TestInfo *nfo=NULL);
//Testuje zda je konvexni objem kladny nebo zaporny. Neni zrovna nejrychlejsi operace, count>3
float geTestConvexVolume(const gePlaneGen *planes, int count);
//Najde bod na konvexnim objemu, ktery je nejblizsi k zadanemu bodu (slozitost n)
//geVector4 geFindPointAtCV(const gePlaneGen *planes, int count, const geVector4& pt);

class geBoundingBox: public geConvexVolume
{  
  protected:
    gePlaneGen planes[6];
    float signum;
  public:
    geBoundingBox() 
    {};
    //lv - levy dolni predni, pv - pravy horni zadni
    geBoundingBox(const geVector4 &lv, const geVector4& pv, float signum=0.0); //bounding box zadany dvema body
    geBoundingBox(const geVector4 *vlist8, float signum=0.0); //8 bodu v prostoru organizovanych 2x 4 body (2 zakladny)
    geBoundingBox(const gePlaneGen *plist6, float signum=0.0); //6 rovin v prostoru
    geBoundingBox(const gePlane2V *plist6, float signum=0.0); //6 rovin v prostoru
    
    virtual bool TestPoint(const geVector4& pt); //testovani zda bod lezi vne nebo uvnitr
    virtual bool TestLine(const geLine& pt, TestInfo* info=NULL); //test, zda primka protina prostor
    virtual geVector4 GetCenter();
    float GetSign() 
    {
      if (signum==0.0) signum=geTestConvexVolume(planes,6);
      return signum;
    }
    friend ctArchive& operator>>=(ctArchive &arch, geBoundingBox &box);
};

class geBoundingSphere: public geConvexVolume
{
  protected:
    geMatrix4 itransf;
    geVector4 center;
  public:
    geBoundingSphere() 
    {};
    geBoundingSphere(const geVector4& pt,float radius);
    geBoundingSphere(const geMatrix4 &trans);
    
    virtual bool TestPoint(const geVector4& pt); //testovani zda bod lezi vne nebo uvnitr
    virtual bool TestLine(const geLine& pt, TestInfo* info=NULL); //test, zda primka protina prostor
    friend ctArchive& operator>>=(ctArchive &arch, geBoundingSphere &sph);
    virtual geVector4 GetCenter() 
    {return center;}
    
};

class geConvexVolumePlane: public geConvexVolume
{
  
};

#endif // !defined(AFX_GECONVEXVOLUME_H__A376DB23_6640_4A3A_8A12_179369EBB6EC__INCLUDED_)
