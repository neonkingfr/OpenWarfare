// geAutoArray.h: interface for the geAutoArray class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GEAUTOARRAY_H__039F6DC2_0FA8_11D5_B39A_00C0DFAE7D0A__INCLUDED_)
#define AFX_GEAUTOARRAY_H__039F6DC2_0FA8_11D5_B39A_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string.h>
#include <malloc.h>

void _AARangeError(void *arrayptr, int &index);
void _AAMemoryError(void *arrayptr);

template<class T> class geAAClassMove
  {
  public:
    static void Move(T *src,T *trg);
    static T *Realloc(T *src, int count);
  };










template<class T> class geAASpecMove 
  {
  public:
    static void Move(T *src,T *trg);	
    static T *Realloc(T *src, int count);
  };










template<class T> class geAAMethodMove
  {
  public:
    static void Move(T *src,T *trg);	
    static T *Realloc(T *src, int count);
  };










template<class T> class geAABinMove
  {
  public:
    static void Move(T *src,T *trg) 
      {memcpy(trg,src,sizeof(*trg));}
    static T *Realloc(T *src, int count) 
      {return (T *)realloc(src,sizeof(*src)*count);}
  };










template<class T>
class geAutoArray  
  {
  T* array;
  int _n;
  int _alloc;  
  protected:
    void ReleaseItem(int idx);
    T* CreateItem(int idx, const T *src);
    T* CreateItem(int idx);
    void Shift(int shto, int shfrom);
    void Insert(int idx, int count, const T *src);    
    void DoCopy(const geAutoArray<T>& other);
  public:
    geAutoArray(int initial=0);
    geAutoArray(const geAutoArray<T>& other) 
      {DoCopy(other);}
    virtual ~geAutoArray();
    
  public:
    geAutoArray<T>& operator=(const geAutoArray<T>& other)
      {
      geAutoArray<T>::~geAutoArray();
      DoCopy(other);
      return *this;
      }
    void Expand(int itemstoadd);
    void FreeExtra();
    T *Add();
    T *Add(const T& item);
    geAutoArray<T>& operator<< (T item) 
      {const T& p=item;Add(p);}
    
  public:
    const T& operator[] (int idx) const
      { if (idx<0 || idx>=_n) _AARangeError((void *)this,idx);
      return array[idx];
      }
    T& operator[] (int idx)
      {if (idx<0 || idx>=_n) _AARangeError(this,idx);
      return array[idx];
      }
    void AddMulti(int count)
      {for (int i=0;i<count;i++) Add();}
    void AddMulti(int count,const T& item)
      {for (int i=0;i<count;i++) Add(item);}	
    void Insert(int idx, int count=1);
    void Insert(int idx, const T& src, int count=1) 
      {Insert(idx,count,&src);};
    void Remove(int idxstart, int count=1);
    void RemoveAll() 
      {if (_n) Remove(0,_n); }
    int GetCount() const 
      {return _n;}
    void Sort(int start=0, int len=-1); 
    const T *GetData() const 
      {return array;}
    T *GetData() 
      {return array;}
  };










template<class T>
int geCompare(const T *src1, const T *src2)
  {
  return (int)((*src1)>(*src2))-(int)((*src1)<(*src2));
  }










#define geDefineBinMove(T) \
inline void geAAMoveFunct(T *src, T *trg) {geAABinMove<T>::Move(src,trg);}\
inline   T* geAAReallocFunct(T *src, int count) {return geAABinMove<T>::Realloc(src,count);}

#define geDefineClassMove(T) \
inline void geAAMoveFunct(T *src, T *trg) {geAAClassMove<T>::Move(src,trg);}\
inline   T* geAAReallocFunct(T *src, int count) {return geAAClassMove<T>::Realloc(src,count);}

#define geDefineSpecMove(T) \
inline void geAAMoveFunct(T *src, T *trg) {geAASpecMove<T>::Move(src,trg);}\
inline   T* geAAReallocFunct(T *src, int count) {return geAASpecMove<T>::Realloc(src,count);}

#define geDefineMethodMove(T) \
inline void geAAMoveFunct(T *src, T *trg) {geAAMethodMove<T>::Move(src,trg);}\
inline   T* geAAReallocFunct(T *src, int count) {return geAAMethodMove<T>::Realloc(src,count);}

#define geDefineFriendMove(T) \
friend void geAAMoveFunct(T *src, T *trg);\
friend  T* geAAReallocFunct(T *src, int count)

geDefineBinMove(bool);
geDefineBinMove(char);
geDefineBinMove(unsigned char);
geDefineBinMove(short);
geDefineBinMove(unsigned short);
geDefineBinMove(int);
geDefineBinMove(unsigned int);
geDefineBinMove(long);
geDefineBinMove(unsigned long);
geDefineBinMove(float);
geDefineBinMove(double);
geDefineBinMove(void *);
geDefineBinMove(bool *);
geDefineBinMove(char *);
geDefineBinMove(unsigned char *);
geDefineBinMove(short *);
geDefineBinMove(unsigned short *);
geDefineBinMove(int *);
geDefineBinMove(unsigned int *);
geDefineBinMove(long *);
geDefineBinMove(unsigned long *);
geDefineBinMove(float *);
geDefineBinMove(double *);


#include "geAutoArray.tli"

#if defined(AFX_CTARCHIVE_H__F66DAB8E_9620_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
template <class T>
ctArchive& operator>>=(ctArchive& arch, geAutoArray<T>& arr)
  {
  int c=arr.GetCount();
  arch>>=c;
  if (-arch) 
    {arr.RemoveAll();arr.AddMulti(c);}
  for (int i=0;i<c;i++) arch>>=arr[i];
  return arch;
  }










#endif



#endif // !defined(AFX_GEAUTOARRAY_H__039F6DC2_0FA8_11D5_B39A_00C0DFAE7D0A__INCLUDED_)

