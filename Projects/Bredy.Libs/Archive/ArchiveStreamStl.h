// ArchiveStreamStl.h: interface for the ArchiveStreamStl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVESTREAMSTL_H__525BE405_5751_49EC_A1DC_BB96B83FFAAD__INCLUDED_)
#define AFX_ARCHIVESTREAMSTL_H__525BE405_5751_49EC_A1DC_BB96B83FFAAD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArchiveStream.h"
#include <iostream>

using namespace std;

class ArchiveStreamStlIn;
class ArchiveStreamStlOut;

typedef void (*ArchiveStreamStlInDestroyStreamCallback)(ArchiveStreamStlIn &owner,istream *stream);
typedef void (*ArchiveStreamStlOutDestroyStreamCallback)(ArchiveStreamStlOut &owner, ostream *stream);

class ArchiveStreamStlIn : public ArchiveStream  
  {
  istream *_file;
  int err;
  ArchiveStreamStlInDestroyStreamCallback _killcb;
public:
	ArchiveStreamStlIn(istream *file,ArchiveStreamStlInDestroyStreamCallback killcb=NULL);
	virtual ~ArchiveStreamStlIn();
    virtual bool IsStoring() {return false;}
    virtual int DataExchange(void *buffer, int maxsize);
    virtual int IsError();
    virtual void Reset();
    virtual __int64 Tell();
    virtual void Seek(__int64 lOff, SeekOp seekop);
    virtual void Reserved(int bytes);
  };

class ArchiveStreamStlOut : public ArchiveStream  
  {
  ostream *_file;
  int err;
  ArchiveStreamStlOutDestroyStreamCallback _killcb;
public:
	ArchiveStreamStlOut(ostream *file,ArchiveStreamStlOutDestroyStreamCallback killcb=NULL);
	virtual ~ArchiveStreamStlOut();
    virtual bool IsStoring() {return true;}
    virtual int DataExchange(void *buffer, int maxsize);
    virtual int IsError();
    virtual void Reset();
    virtual __int64 Tell();
    virtual void Seek(__int64 lOff, SeekOp seekop);
    void Flush() {_file->flush();}

  };


#endif // !defined(AFX_ARCHIVESTREAMSTL_H__525BE405_5751_49EC_A1DC_BB96B83FFAAD__INCLUDED_)
