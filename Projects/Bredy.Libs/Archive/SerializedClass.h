// SerializedClass.h: interface for the SerializedClass class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIALIZEDCLASS_H__8951E5A4_16A0_4E5D_B933_1AE6C731976E__INCLUDED_)
#define AFX_SERIALIZEDCLASS_H__8951E5A4_16A0_4E5D_B933_1AE6C731976E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef _CPPRTTI 
#include <typeinfo.h>
#else
#pragma message (__FILE__"(15) : warning : RTTI Disabled. It is not error, but with RTTI, class serializing may run faster")
#endif

#include "IArchive.h"
#include "SerializedClassInstanceWatcher.h"

#define CLASS_STAMP_VAR_NAME "_ID"

#ifndef ASSERT 
#include <assert.h>
#define ASSERT(p) assert(p)
#endif

#define SERIALIZEDCLASS_UNKNOWNSTAMP -5 ///< Archive found stamp that was not registred.
#define SERIALIZEDCLASS_UNKNOWNINSTANCEID -6 ///<Archive found undefined instance id.

///Declaration of base class for serialization whole classes
class SerializedClass  
{
public:
  ///Pure function that implements serialization for this class
  /** If class is part of concatenated fields, it is better use for serialize
   pointer to the following field function SerializeChain()*/
  virtual void Serialize(IArchive &arch)=0;

  ///Serializes each following field. 
  /**Function should return pointer to the following field in chain.
  If archive is writting, it serializes following field and return current
  address of following field. If archive is reading, its creates and serializes
  new field, contact fields and returns address of new field. 
  Function should return NULL, if chain ends.
  */
  virtual SerializedClass *SerializeChain(IArchive &arch) {return NULL;}
};

//--------------------------------------------------------------------
/// Base template for ClassRegistrator

/// Base class for unique acces to various registrator.
/** Major task for this class is handle whole class serialization.    
	*/
class SC_ClassRegistratorBase
  {
  public:
	  ///Function handles class serialization
	  /**
	  @param arch archive that provides data exchange with a stream.
	  @param cls serialized class. If archive is reading, this pointer should be NULL,
				 otherwise function will call destroy instance that pointer points. 
				 If archive is storing, pointer should point to instance of class 
				 being serialized.
	  @return If archive is reading, function returns a pointer to the new instance of 
			  class readed from archive, or NULL, if archive contain NULL record. 
			  Function also return NULL, if archive contain invalid record, or class 
			  stored in archive is not registred. This situation is reported in error 
			  status. Function returns NULL, if there is any error in serialization 
			  detected.	If archive is storing, function always return value of cls 
			  parameter. Check state of archive for errors.
	  */

	  virtual SerializedClass *SerializeClass(IArchive &arch, SerializedClass *cls)=0;
	  
  };


///Base class created for each StampType
/** StampType is type used for storing class stamps. Each class has assigned unique stamp. 
Stamps are stored into stream at beginning of instance data and helps identify class, when
structure is reconstructed.<p>

Usually stamps is numbers, type int, unsigned long etc (default StampType is unsigned long),
but can be used others, depends on project.<p>

Each StampType must support following operators:<p>
  
	operator !, that returns true, if Stamp is NULL<br>
	operator == to compare two stamps, returns true, if equal<br>
	standard copiing constructor and assignment.
	constructor StampType(int) - or compatible, where if int is zero, type is inicialized to NULL
*/
template<class StampType>
class SC_ClassRegistrator: public SC_ClassRegistratorBase
  {  
  SC_ClassRegistrator *_next; ///<Next class registration
  SC_ClassRegistrator **_owner; ///<Owner registrator
  StampType _stamp;           ///<Stamp for current class
  unsigned long _classUID;			  ///<UID for current class
  public:
	static unsigned long GetClassUID(const SerializedClass *clsptr);
	static SC_ClassRegistrator **GetGlobalRegistrator();
	
	///Register new class information into registrator
	SC_ClassRegistrator(unsigned long classUID,const StampType &stamp, SC_ClassRegistrator **globregistrator=NULL);
	~SC_ClassRegistrator();
	
	bool TestUID(unsigned long uid) const 
	  {return _classUID==uid;}
	bool TestStamp(const StampType &stamp) const 
	  {return stamp==_stamp;}
	static void SerializeStamp(IArchive &arch,StampType &stamp)
	  {arch(CLASS_STAMP_VAR_NAME,stamp);}

	static SC_ClassRegistrator *FindClassUID(unsigned long uid, SC_ClassRegistrator *reg=NULL);
	static SC_ClassRegistrator *FindClassStamp(const StampType &_stamp, SC_ClassRegistrator *reg=NULL);
	
	virtual SerializedClass * CreateClass()=0;
	virtual SerializedClass *SerializeClass(IArchive &arch, SerializedClass *cls);

  };

void SG_SelectDefaultRegistrator(IArchive &arch, SC_ClassRegistrator<unsigned long> *registrator=NULL);

inline SerializedClass *SC_DoDataExchange(IArchive &arch, SerializedClass *cls)
  {  
  long instanceid;
  if (arch.instanceWatcher)
	{
	if (+arch)	  
	  {
	  if (!arch.instanceWatcher->SaveInstanceID(arch,cls)) return cls;
	  }
	else
	  {
	  SerializedClass *nwcls=NULL;
	  instanceid=arch.instanceWatcher->LoadInstanceID(arch,&nwcls);
	  if (instanceid==-1)  {arch.SetError(SERIALIZEDCLASS_UNKNOWNINSTANCEID);return nwcls;}
	  if (nwcls!=NULL || instanceid==0) return cls;
	  }
	}
  if (arch.classSerializationInfo==NULL)
	{
	SG_SelectDefaultRegistrator(arch);
	ASSERT(arch.classSerializationInfo!=NULL); //Registrator is not selected and default cannot be used!
	}
  cls=arch.classSerializationInfo->SerializeClass(arch,cls);	  
  if (-arch && arch.instanceWatcher)
	arch.instanceWatcher->RegisterNewInstance(instanceid,cls);
  if (cls==NULL) return NULL;
  else	
	cls->Serialize(arch);
  return cls;
  }

/// Main class used for registrating classes, that can be serialized and assign them stamp
/**
Recomended usage of this class is following:<p>

@example ARegisterClass static ARegisterClass<MyClass, StampType> __someVariable(id);<p>

@param MyClass is identifier for class that can be serialized (must extend SerializedClass interface
@param StampType is type used for stamps. It is optional and if not specified, unsigned long is assumed.
@param __someVariable unimportant variable name, that holds registration. Usually is never
       directly accessed, because class is registred during construction
@param id Id assigned to the class in archive (has to be unique), in type StampType.<p>

Each StampType has own registrator, so they cannot be mixed together. There are also two types
of registrators. Global and Local<p>

Global registrator is created at application's start and destroyed before exit. <br>
Local registrator can be created and destroyed anytime.<p>

For global registrators is recomended use static allocation for ARegisterClass<br>
For local registrators is recomended use the same type allocation as registrator.Usually 
automatic allocation at stack frame.<br>
@example SC_ClassRegistrator *myregistrator=NULL;

Registrator can be specified with registration.
@example static ARegisterClass<MyClass, StampType> __someVariable(id,&myregistrator);<p>

To archive use local registrator, it must be selected using .
@example SC_SelectRegistrator(archive,myregistrator)<p>

Registrations for local registrator must be destroyed in reverse order than was created. <br>
Registrator must be destroyed as the last.
*/



template <class T, class StampType=unsigned long>
class ARegisterClass: public SC_ClassRegistrator<StampType>
  {
  public:
	ARegisterClass(const StampType& stamp,SC_ClassRegistrator<StampType> **globregistrator=NULL);
	static unsigned long GetClassUID();
	virtual SerializedClass *CreateClass();
    friend void ArchiveVariableExchange(IArchive &arch,T * &data)
	  {
	  SerializedClass *cls=data;
	  cls=SC_DoDataExchange(arch,cls);
	  data=static_cast<T *>(cls);	
	  }
  };


///Implementation for SerializeClass function depending on StampType
template <class StampType>
SerializedClass *SC_ClassRegistrator<StampType>::SerializeClass(IArchive &arch, SerializedClass *cls)
	  {
	  if (!arch) return NULL;
	  if (+arch)
		{
		if (cls==NULL)
		  {
		  StampType null(0);  //if there is error, constructor StampType(int zero) should be defined for class.
		  SerializeStamp(arch,null);
		  }
		else
		  {
		  SC_ClassRegistrator<StampType> *fnd=FindClassUID(GetClassUID(cls));
		  ASSERT(fnd!=NULL); //serializing class is not registred!
		  SerializeStamp(arch,fnd->_stamp);
		  }
		return cls;
		}
	  else
		{
		if (cls) delete cls;
		StampType stamp;
		SerializeStamp(arch,stamp);
		if (!arch) return NULL;
		if (!stamp) return NULL;  //if there is an error, operator ! should return true, if stamp is  NULL
	    SC_ClassRegistrator<StampType> *fnd=FindClassStamp(stamp);
		if (fnd==NULL) 
		  {
		  //Class has not been found, it is not registred, report it as error;
		  arch.SetError(SERIALIZEDCLASS_UNKNOWNSTAMP);
		  return NULL;
		  }
		cls=fnd->CreateClass();
		return cls;
		}
	  }


///Function suggests unique identifier for class type. 
/** This identifier is only valid for current application instance, but is used to
convert pointer to StampType. Function use RTTI if avaiable, or use alternative method reading
vf_table pointer. 
@param clsptr pointer to unidentified instance. 
@return Unique identifier for this instance as unsigned long
*/
template <class StampType>
unsigned long SC_ClassRegistrator<StampType>::GetClassUID(const SerializedClass *clsptr)
  {
#ifdef _CPPRTTI 
  const type_info &nfo=typeid(*clsptr);
  return (unsigned long)nfo.raw_name();
#else
  unsigned long *p=(unsigned long *)clsptr; //Get address of vf_table;
  return *p;	  //return address of vf_table - it is unique for each class
#endif      
  }


///Function returns pointer to global class registrator for specified StampType
template <class StampType>
SC_ClassRegistrator<StampType> **SC_ClassRegistrator<StampType>::GetGlobalRegistrator()
  {
  static SC_ClassRegistrator<StampType> *ptr=NULL;
  return &ptr;
  }

///Constructor register class using specified registrator 
/**
@param classUID unique identifier for class, the compatible with GetClassUID return;
@param stamp a stamp assigned to the class
@param globregistrator optional registrator, if not specified, constructor use GetGlobalRegistrator()
*/
template <class StampType>
SC_ClassRegistrator<StampType>::SC_ClassRegistrator(unsigned long classUID, const StampType &stamp, SC_ClassRegistrator **globregistrator):
 _stamp(stamp),_classUID(classUID)
  {
  if (globregistrator==NULL) globregistrator=GetGlobalRegistrator();
  _next=*globregistrator;
  *globregistrator=this;
  _owner=globregistrator;
  }

template<class StampType>
 SC_ClassRegistrator<StampType>::~SC_ClassRegistrator()
  {
  SC_ClassRegistrator<StampType> **trc=_owner;
  while (*trc && *trc!=this) trc=&((*trc)->_next);
  if (*trc==this) 
	{
	*trc=_next;
	_next=NULL;
	}
  }

///Finds class registration by its unique identuifier
template <class StampType>
SC_ClassRegistrator<StampType> *SC_ClassRegistrator<StampType>::FindClassUID(unsigned long uid, SC_ClassRegistrator *reg)
  {
  if (reg==NULL) reg=*GetGlobalRegistrator();
  while (reg) 
	{
	if (reg->TestUID(uid)) return reg;
	reg=reg->_next;
	}
  return NULL;
  }

///Finds class registration by its stamp
template <class StampType>
SC_ClassRegistrator<StampType> *SC_ClassRegistrator<StampType>::FindClassStamp(const StampType &stamp, SC_ClassRegistrator *reg)
  {
  if (reg==NULL) reg=*GetGlobalRegistrator();
  while (reg) 
	{
	if (reg->TestStamp(stamp)) return reg;
	reg=reg->_next;
	}
  return NULL;
  }



template <class T, class StampType>
unsigned long ARegisterClass<T,StampType>::GetClassUID()
  {
#ifdef _CPPRTTI 
  const type_info &nfo=typeid(T);
  return (unsigned long)nfo.raw_name();
#else
  T helper; //construct helper object (construction may be slow)
  unsigned long *p=(unsigned long *)(&helper); //Get address of vf_table;
  return *p;	  //return address of vf_table - it is unique for each class
#endif      
  }

template <class T, class StampType>
ARegisterClass<T,StampType>::ARegisterClass(const StampType& stamp,SC_ClassRegistrator<StampType> **globregistrator):
  SC_ClassRegistrator<StampType>(GetClassUID(),stamp, globregistrator) {}

template <class T, class StampType>
SerializedClass *ARegisterClass<T,StampType>::CreateClass()
  {
  return new T();
  }

template <class StampType>
void SG_SelectRegistrator(IArchive &arch, SC_ClassRegistrator<StampType> *registrator=NULL)
  {
  if (registrator==NULL) registrator=*SC_ClassRegistrator<StampType>::GetGlobalRegistrator();
  arch.classSerializationInfo=registrator;
  }

static inline void SG_SelectDefaultRegistrator(IArchive &arch, SC_ClassRegistrator<unsigned long> *registrator)
  {
  if (registrator==NULL) registrator=*SC_ClassRegistrator<unsigned long>::GetGlobalRegistrator();
  arch.classSerializationInfo=registrator;
  }

static inline void ArchiveVariableExchange(IArchive &arch,SerializedClass * &data)
	{
	SerializedClass *cls=data;
	cls=SC_DoDataExchange(arch, cls);
    data=cls;
	}

#endif // !defined(AFX_SERIALIZEDCLASS_H__8951E5A4_16A0_4E5D_B933_1AE6C731976E__INCLUDED_)
