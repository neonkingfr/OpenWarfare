// ArchiveStreamCompressFilter.h: interface for the ArchiveStreamCompressFilter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVESTREAMCOMPRESSFILTER_H__F59E44F9_E7C9_4A42_9856_B90F46F9DDFD__INCLUDED_)
#define AFX_ARCHIVESTREAMCOMPRESSFILTER_H__F59E44F9_E7C9_4A42_9856_B90F46F9DDFD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArchiveStream.h"

#define ARCHCMPRS_MAXBITS 14  //cislo udava pocet bitu, ktere uz nejsou povoleny
#define ARCHCMPRS_DICTSIZE (1<<ARCHCMPRS_MAXBITS)
#define ARCHCMPRS_SHUTDOWN -11 // stream je uzavren, nelze cist ani zapisovat do zavolani Reset()
#define ARCHCMPRS_INTEGRITYERROR -12 // stream obsahuje chybu integrity, nelze dekomprimovat

class ArchiveStreamCompressFilter : public ArchiveStream  
{
protected:

  struct Dictionary
    {
    unsigned short symbol;  //symbol ktery je na aktualni urovni
    unsigned short parent; //index urovne ze ktere uroven vychazi
    unsigned short child; //index prvniho potomka dalsi urovne 
    unsigned short sibling;  //index na dalsi dceriny symbol vychazejici z predchozi urovne
    public:
	    void Init(unsigned short symbol);
    };
 
  Dictionary *_dict; ///!dictionary for compressing / decompressing

  unsigned long _accum; ///!accumulator for compressing
  unsigned char _accUse; ///!usage of accumulator
  unsigned short _nextItem; ///!next item in dictionary
  unsigned short _dicRdPos; ///!current read position in dictionary while decompressing
  unsigned short _curBits;  ///!current bit level for writes  
  unsigned short _levelCode; ///!code for next level
  unsigned short _clearCode; ///!code for clear dictionary
  unsigned short _eofCode; ///!code for end of stream (or separator code)

  ArchiveStream *_host; ///!Host stream

  ArchiveStream *GetStream() {return _host;}
  
  ArchiveStream *SetStream(ArchiveStream *input) 
      {ArchiveStream *old=_host;_host=input;return old;}

public:
	void WriteEndStream();
	ArchiveStreamCompressFilter(ArchiveStream *host);
	virtual ~ArchiveStreamCompressFilter();

    int IsError() {return _host->IsError();}
    void SetError(int err) {_host->SetError(err);}
	void Reset();
    virtual __int64 Tell() 
      {
      if (IsError()) return -1;
      SetError(-1);return -1;
      }
    virtual void Seek(__int64 lOff, SeekOp seekop) 
      {
      if (IsError()) return;
      SetError(-1);
      }
    virtual bool IsStoring() {return _host->IsStoring();}    
    int DataExchange(void *buffer, int maxsize);

    virtual void Flush();

private:
	unsigned short ReadBits(int bits);
	void WriteClearCode();
	void WriteBits(unsigned short value, int bits);    
protected:
	unsigned short ReadGroup();
	unsigned char Read();
	void FlushWrite();
	void Write(unsigned char zn);
};

#endif // !defined(AFX_ARCHIVESTREAMCOMPRESSFILTER_H__F59E44F9_E7C9_4A42_9856_B90F46F9DDFD__INCLUDED_)
