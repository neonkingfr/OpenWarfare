// ArchiveStreamBase64Filter.h: interface for the ArchiveStreamBase64Filter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVESTREAMBASE64FILTER_H__30D66F66_95EA_4D42_A590_E12460FD722F__INCLUDED_)
#define AFX_ARCHIVESTREAMBASE64FILTER_H__30D66F66_95EA_4D42_A590_E12460FD722F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArchiveStream.h"

#define ARCHBASE64_STREAMERROR -64

class ArchiveStreamBase64Filter : public ArchiveStream  
  { 
protected:
	  void FlushWrite();
    ArchiveStream *_input;
    unsigned char _smallbuf[4];
    char _bufpos;
    int _cols;
    bool _wrap;
    static unsigned char alphabet[65];
    static unsigned char inalphabet[256],decoder[256];
public:
	  void Encode(void *data, int size);
	  void Decode(void *data, int size);
	ArchiveStreamBase64Filter(ArchiveStream *input);
    ~ArchiveStreamBase64Filter();

    ArchiveStream *GetStream() {return _input;}
  
    ArchiveStream *SetStream(ArchiveStream *input) 
      {ArchiveStream *old=_input;_input=input;return old;}

    int DataExchange(void *buffer, int maxsize);
    int IsError() {return _input->IsError();}
    void SetError(int err) {_input->SetError(err);}
    void Reset(); 
    virtual __int64 Tell() 
      {
      if (IsError()) return -1;
      SetError(-1);return -1;
      }
    virtual void Seek(__int64 lOff, SeekOp seekop) 
      {
      if (IsError()) return;
      SetError(-1);
      }
    virtual bool IsStoring() {return _input->IsStoring();}    

    void TextWrap(bool enable) {_wrap=enable;}

    virtual void Flush();
  };

#endif // !defined(AFX_ARCHIVESTREAMBASE64FILTER_H__30D66F66_95EA_4D42_A590_E12460FD722F__INCLUDED_)
