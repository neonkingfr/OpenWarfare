// Archive.h: interface for the Archive class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVE_H__958DD3D6_BBE3_4485_B6F8_56EFBAAB155D__INCLUDED_)
#define AFX_ARCHIVE_H__958DD3D6_BBE3_4485_B6F8_56EFBAAB155D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IArchive.h"
#include "ArchiveStream.h"

#define ARCHEXTRADATA_CLASSEXCHANGE 1
#define ARCHEXTRADATA_SECTCOUNTER 2


class ArchiveClassExchange; //exchange whole class and structure
class Archive: public IArchive
  {
  protected:
    ArchiveStream *_stream; ///<Current stream attached to archive, with supports IO.

  public:
    ArchiveClassExchange *_classex; ///<Pointer to object to serialize whole clases and structure
    int _sect_counter;   ///<Section counter, for various use

  public:
    Archive(ArchiveStream &str): _stream(&str) 
      {
      _classex=NULL;
      _sect_counter=0;
      InitInterface(str.IsStoring());
      }

    ArchiveStream *GetStream() {return _stream;}
  
    ArchiveStream *SetStream(ArchiveStream *nwstream) 
      {
      ArchiveStream *old=_stream;
      _stream=nwstream;
      Reset();
      _stream->Reset();
      InitInterface(_stream->IsStoring());
      return old;
      }
  
    virtual void OverrideSetError(int error)
      {
      _stream->SetError(error);
      }

    virtual int OverrideGetError(void)
      {
      return _stream->IsError();
      }

    virtual bool OpenSection(const char *name,int type)
      {
      _sect_counter++;
      return true;
      }
    virtual void CloseSection(const char *name,int type)
      {    
      }
    void *OverideGetInstanceData(int dataId)
      {
      switch (dataId)
        {
        case ARCHEXTRADATA_CLASSEXCHANGE: return _classex;
        case ARCHEXTRADATA_SECTCOUNTER: return (void *)_sect_counter;
        }
      return NULL;
      }
  };

#define ARCHIVEDECLAREDATAEXCHANGE(type) \
	protected: virtual int OverrideDoExchange(type &data) \
      {_stream->DataExchange(&data,sizeof(data));return _stream->IsError();}
#define ARCHIVEDECLARESTRINGEXCHANGE(type) \
	protected: virtual int OverrideDoExchange(type &data) \
      {ArchiveStringExchange(*this,data);return _stream->IsError();}


template <class T> 
void ArchiveStringExchange(Archive &arch, T * &data)
  {
  if (arch.IsStoring())
    {   
    int p=-1;
    do arch.GetStream()->ExSimple(data[++p]); while (data[p]!=0);
    }
  else
    {
    T smallbuff[511/sizeof(T)+1];
    int alloc=511/sizeof(T)+1;
    int pos=0;
    T *buff=smallbuff;
    T val;
    do
      {
      if (arch.GetStream()->ExSimple(val)!=0) break;
      if (pos==alloc)
        {
        alloc*=2;
        T *newbuf=new T[alloc];
        for (int i=0;i<pos;i++) newbuf[i]=buff[i];
        if (buff!=smallbuff) delete [] buff;
        buff=newbuf;        
        }
      buff[pos++]=val;
      }
    while (val!=0 && !(!arch));
    if (data!=0) delete [] data;
    data=new T[pos];
    for (int i=0;i<pos;i++) data[i]=buff[i];
    if (buff!=smallbuff) delete [] buff;
    }
  }


class ArchiveSimpleBinary: public Archive
  {
  public:
	ARCHIVEDECLAREDATAEXCHANGE(bool)
	ARCHIVEDECLAREDATAEXCHANGE(char)
	ARCHIVEDECLAREDATAEXCHANGE(unsigned char)
	ARCHIVEDECLAREDATAEXCHANGE(short)
	ARCHIVEDECLAREDATAEXCHANGE(unsigned short)
	ARCHIVEDECLAREDATAEXCHANGE(int)
	ARCHIVEDECLAREDATAEXCHANGE(unsigned int)
	ARCHIVEDECLAREDATAEXCHANGE(long)
	ARCHIVEDECLAREDATAEXCHANGE(unsigned long)
	ARCHIVEDECLAREDATAEXCHANGE(__int64)
	ARCHIVEDECLAREDATAEXCHANGE(unsigned __int64)
	ARCHIVEDECLAREDATAEXCHANGE(float)
	ARCHIVEDECLAREDATAEXCHANGE(double)
	ARCHIVEDECLARESTRINGEXCHANGE(unsigned char *)
	ARCHIVEDECLARESTRINGEXCHANGE(char *)
	ARCHIVEDECLARESTRINGEXCHANGE(unsigned short *)
	ARCHIVEDECLARESTRINGEXCHANGE(short *)
    virtual bool OverrideExchangeVersion(int &ver)
      {
      return _stream->ExSimple(ver)==0;      
      }
  public:
  	virtual void Reserved(int bytes)
      {
      _stream->Reserved(bytes);
      }

    virtual int OverrideDoBinaryExchange(void *ptr, unsigned long size)
      {
      return _stream->DataExchange(ptr,size);
      }
    ArchiveSimpleBinary(ArchiveStream &str): Archive(str) {}

  virtual bool Check(const char *text)
    {
    char z;
    const char *c=text;
    while (*c)
      {
      z=*c;
      (*this)(z);
      if (z!=*c) return false;
      c++;
      }
    return !(!(*this));
    }


  };

#undef ARCHIVEDECLAREDATAEXCHANGE
#undef ARCHIVEDECLARESTRINGEXCHANGE


#endif
