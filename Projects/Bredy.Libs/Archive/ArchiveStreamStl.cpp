// ArchiveStreamStl.cpp: implementation of the ArchiveStreamStl class.
//
//////////////////////////////////////////////////////////////////////

#include "ArchiveStreamStl.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ArchiveStreamStlIn::ArchiveStreamStlIn(istream *file, ArchiveStreamStlInDestroyStreamCallback killcb):
  _file(file),_killcb(killcb)
  {
  Reset();
  }

ArchiveStreamStlIn::~ArchiveStreamStlIn()
  {
  if (_killcb) _killcb(*this,_file);
  }


int ArchiveStreamStlIn::DataExchange(void *buffer, int maxsize)
  {
  if (err) return err;
  _file->read((char *)buffer,maxsize);
  if (_file->gcount()!=maxsize)
    ReportUncomplette(_file->gcount());
  else
    if (!(*_file)) err=-1; 
  _file->clear();
  return err;
  }

int ArchiveStreamStlIn::IsError()
  {
  return err;
  }

void ArchiveStreamStlIn::Reset()
  {
  err=0;
  }

__int64 ArchiveStreamStlIn::Tell()
  {
  return _file->tellg();
  }

void ArchiveStreamStlIn::Seek(__int64 lOff, SeekOp seekop)
  {
  if (err) return;
  switch (seekop)
    {
    case begin:_file->seekg(lOff,ios::beg);break;
    case current:_file->seekg(lOff,ios::cur);break;
    case end:_file->seekg(lOff,ios::end);break;
    }
  if (!(*_file)) err=-1; 
  }

void ArchiveStreamStlIn::Reserved(int bytes)
  {
  if (err) return;
  _file->ignore(bytes);
  if (!(*_file)) err=-1; 
  }

ArchiveStreamStlOut::ArchiveStreamStlOut(ostream *file, ArchiveStreamStlOutDestroyStreamCallback killcb):
  _file(file),_killcb(killcb)
  {
  Reset();
  }

ArchiveStreamStlOut::~ArchiveStreamStlOut()
  {
  if (_killcb) _killcb(*this,_file);
  }


int ArchiveStreamStlOut::DataExchange(void *buffer, int maxsize)
  {
  if (err) return err;
  _file->write((char *)buffer,maxsize);
  if (!(*_file)) err=-1; 
  return err;
  }

int ArchiveStreamStlOut::IsError()
  {
  return err;
  }

void ArchiveStreamStlOut::Reset()
  {
  err=0;
  }

__int64 ArchiveStreamStlOut::Tell()
  {
  return _file->tellp();
  }

void ArchiveStreamStlOut::Seek(__int64 lOff, SeekOp seekop)
  {
  if (err) return;
  switch (seekop)
    {
    case begin:_file->seekp(lOff,ios::beg);break;
    case current:_file->seekp(lOff,ios::cur);break;
    case end:_file->seekp(lOff,ios::end);break;
    }
  if (!(*_file)) err=-1; 
  }
