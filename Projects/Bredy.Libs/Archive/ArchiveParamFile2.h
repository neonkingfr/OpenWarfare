#pragma once
#include "iarchive.h"
#include <El/ParamFile/ParamFile.hpp>
#include <el/BTree/BTree.h>
#include "ArchiveStreamMemory.h"

#define APFPHF_CLASSNAME 0x1  //classname defined
#define APFPHF_VARIABLENAME 0x2 //variable name defined
#define APFPHF_STRINGVALUE 0x4 //stringvalue
#define APFPHF_INTVALUE 0x8  //intvalue
#define APFPHF_REALVALUE 0x10 //real float value
#define APFPHF_VALUE (APFPHF_STRINGVALUE|APFPHF_INTVALUE|APFPHF_REALVALUE) 
#define APFPHF_INARRAY 0x20

#define APF_ADDCLASSFAILED -100
#define APF_CLASSNOTFOUND -101
#define APF_VARIABLEISTOOLONG -102
#define APF_UNABLETOGETVARIABLE -103

#define Thread   __declspec( thread )


struct ArchiveParamItemCounter
  {
  int itemName;
  int count;
  static Thread ArchiveStreamMemoryOut *_stringMem;
  ArchiveParamItemCounter() {itemName=-1;count=0;}  
  int Compare(const ArchiveParamItemCounter& other) const
	{
    ASSERT(_stringMem!=NULL);
    if (itemName==-1) return 1;
    const char *stringBase=_stringMem->GetBuffer();
    return strcmp(stringBase+itemName,stringBase+other.itemName);
    }
  bool operator==(const ArchiveParamItemCounter& other) const {return Compare(other)==0;}
  bool operator>(const ArchiveParamItemCounter& other) const {return Compare(other)>0;}
  bool operator<(const ArchiveParamItemCounter& other) const {return Compare(other)<0;}
  bool operator!=(const ArchiveParamItemCounter& other) const {return Compare(other)!=0;}
  bool operator>=(const ArchiveParamItemCounter& other) const {return Compare(other)>=0;}
  bool operator<=(const ArchiveParamItemCounter& other) const {return Compare(other)<=0;}
  int operator=(int p) {if (p==0) {itemName=-1;count=0;}return 0;}
  };


struct ArchiveParamEntryLevel
  {
  ParamEntryPtr ptr;  //current class pointer
  BTree<ArchiveParamItemCounter>level;  //list of created items in this level
  int stringPos;      //pointer to _stringMem for releasing memory, when level is removed
  int defArrayIndex;

  ArchiveParamEntryLevel(ParamEntryPtr &p):ptr(p) {stringPos=-1;defArrayIndex=-1;}
  ArchiveParamEntryLevel() {}
  ArchiveParamEntryLevel(const ParamEntryPtr &p):ptr(p) {}
  int GuessItemIndex(ArchiveStreamMemoryOut &_stringMem, const char *itemName);
  void BeforeFreeLevel(ArchiveStreamMemoryOut &_stringMem);
  operator const PEntryPtr<class ParamEntry>& () const {return ptr;}
  };

TypeIsMovable(ArchiveParamEntryLevel);


class ArchiveParamFile2 :public IArchive
  {
  protected:
    RString _className;
    RString _variableName;
    RString _strValue;
    int _intValue;
    float _realValue;

    int _inArrayCnt; //pocet vnoreni Open Section a Close Section ve stavu INARRAY

    ArchiveStreamMemoryOut _stringMem;
    
    unsigned long _state;

    ParamFile &_pfile;
    AutoArray<ArchiveParamEntryLevel> _stack;
    int _error;

  protected:
	ArchiveParamEntryLevel &CurEntry() {return _stack[_stack.Size()-1];}
    void Push(ParamEntryPtr &entry) {_stack.Append()=entry;}
    void Pop() 
      {
        ASSERT(_stack.Size());
        CurEntry().BeforeFreeLevel(_stringMem);
        _stack.Delete(_stack.Size()-1);
      }


  public:
    ArchiveParamFile2(ParamFile &pfile);
    virtual ~ArchiveParamFile2(void);

    void Reserved(int bytes) {}
    virtual void OverrideSetError(int error) {_error=error;}
    virtual int OverrideGetError() {return _error;}
    virtual bool HandleDefaultValue() {return true;}
    virtual bool CanCloseSection() {return true;}
    virtual bool OverrideExchangeVersion(int &ver);
    virtual void *OverideGetInstanceData(int dataId)
      {return NULL;}

  };

class ArchiveParamFile2Out: public ArchiveParamFile2
  {
    
  protected:
    void CreateArray();
    void SaveArrayValue(ParamEntry *array);
    void SaveValue();
    void ExchangeInt(int data);
    void ExchangeFloat(float data);
    void ExchangeString(const char *string);
    void ExchangeBinary(const void *bindata, size_t size);
    void SaveDefault(int state);
  public:
    bool OpenSection(const char *name,int type=0);
    void CloseSection(const char *name,int type=0);
    

    int OverrideDoExchange(int &p) {ExchangeInt(p);return 0;}
    int OverrideDoExchange(float &p) {ExchangeFloat(p);return 0;}
	int OverrideDoExchange(bool &p) {ExchangeInt(p);return 0;}
	int OverrideDoExchange(char &p) {ExchangeInt(p);return 0;}
	int OverrideDoExchange(unsigned char &p) {ExchangeInt((int)p);return 0;}
	int OverrideDoExchange(short &p) {ExchangeInt((int)p);return 0;}
	int OverrideDoExchange(unsigned short &p) {ExchangeInt((int)p);return 0;}
	int OverrideDoExchange(unsigned int &p) {ExchangeInt((int)p);return 0;}
	int OverrideDoExchange(long &p) {ExchangeInt((int)p);return 0;}
	int OverrideDoExchange(unsigned long &p) {ExchangeInt((int)p);return 0;}
	int OverrideDoExchange(__int64 &p) {ExchangeInt((int)p);return 0;}
	int OverrideDoExchange(unsigned __int64 &p)	{ExchangeInt((int)p);return 0;}
	int OverrideDoExchange(double &p) {ExchangeFloat((float)p);return 0;}
    int OverrideDoBinaryExchange(void *ptr, unsigned long size) {ExchangeBinary(ptr,size);return 0;}
    int OverrideDoExchange(char *&p) {ExchangeString(p);return 0;}
    int OverrideDoExchange(short *&p) {return -1;}
    int OverrideDoExchange(unsigned short *&p) {return -1;}
    int OverrideDoExchange(unsigned char *&p) {ExchangeString((char *&)p);return 0;}

    virtual bool Check(const char *text);

    ArchiveParamFile2Out(ParamFile &pfile): ArchiveParamFile2(pfile) 
      {
      ArchiveParamFile2Out::InitInterface(true,false);
      }


  };

class ArchiveParamFile2In: public ArchiveParamFile2
  {
    bool _popClass;   //Close section will pop class. This variable is false, if current item is not class

    ParamEntryPtr _item;

    int _arrayIndex;
    bool FindDefaultItem();

    void ExchangeInt(int &data);
    void ExchangeFloat(float &data);
    void ExchangeString(char *&string);
    void ExchangeBinary(void *bindata, size_t size);
  public:
    bool OpenSection(const char *name,int type=0);
    void CloseSection(const char *name,int type=0);
    

    int OverrideDoExchange(int &p) {ExchangeInt(p);return 0;}
    int OverrideDoExchange(float &p) {ExchangeFloat(p);return 0;}
	int OverrideDoExchange(bool &p) {int x=p;ExchangeInt(x);p=x!=0;return 0;}
	int OverrideDoExchange(char &p) {int x=p;ExchangeInt(x);p=(char)x;return 0;}
	int OverrideDoExchange(unsigned char &p) {int x=p;ExchangeInt(x);p=(unsigned char)x;return 0;}
	int OverrideDoExchange(short &p) {int x=p;ExchangeInt(x);p=(short)x;return 0;}
	int OverrideDoExchange(unsigned short &p) {int x=p;ExchangeInt(x);p=(unsigned short)x;return 0;}
	int OverrideDoExchange(unsigned int &p) {int x=p;ExchangeInt(x);p=(unsigned int)x;return 0;}
	int OverrideDoExchange(long &p) {int x=p;ExchangeInt(x);p=(long)x;return 0;}
	int OverrideDoExchange(unsigned long &p) {int x=p;ExchangeInt(x);p=(unsigned long)x;return 0;}
	int OverrideDoExchange(__int64 &p) {int x=(int)p;ExchangeInt(x);p=x;return 0;}
	int OverrideDoExchange(unsigned __int64 &p)	{int x=(int)p;ExchangeInt(x);p=x;return 0;}
	int OverrideDoExchange(double &p) {float f=(float)p;ExchangeFloat((float)f);p=f;return 0;}
    int OverrideDoBinaryExchange(void *ptr, unsigned long size) {ExchangeBinary(ptr,size);return 0;}
    int OverrideDoExchange(char *&p) {ExchangeString(p);return 0;}
    int OverrideDoExchange(short *&p) {return -1;}
    int OverrideDoExchange(unsigned short *&p) {return -1;}
    int OverrideDoExchange(unsigned char *&p) {ExchangeString((char *&)p);return 0;}

    virtual bool Check(const char *text);

    ArchiveParamFile2In(ParamFile &pfile): ArchiveParamFile2(pfile) 
      {
      ArchiveParamFile2In::InitInterface(true,false);
      _arrayIndex=-1;
      }


  };
