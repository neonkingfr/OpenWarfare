// SerializedClassInstanceWatcher.h: interface for the SerializedClassInstanceWatcher class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SerializedClassInstanceWatcher_H__1ABB7449_FDE0_4695_9055_CAB77721B352__INCLUDED_)
#define AFX_SerializedClassInstanceWatcher_H__1ABB7449_FDE0_4695_9055_CAB77721B352__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
class SerializedClass;

#define SCIW_ERROR (SerializedClass *)0xFFFFFFFF


class tBTreeDB_SC;
class IArchive;
class SerializedClassInstanceWatcher  
{
    tBTreeDB_SC *db;
	unsigned long counter;
public:
	void ResetDBSection(unsigned long section);
	bool SaveInstanceID(IArchive &arch,SerializedClass *cls);
	long LoadInstanceID(IArchive &arch,SerializedClass **cls);
	void RegisterNewInstance(long keyid, SerializedClass *cls);
	SerializedClass *FindInstance(long keyid);
	SerializedClassInstanceWatcher();	
	~SerializedClassInstanceWatcher();	
};

#endif // !defined(AFX_SerializedClassInstanceWatcher_H__1ABB7449_FDE0_4695_9055_CAB77721B352__INCLUDED_)
