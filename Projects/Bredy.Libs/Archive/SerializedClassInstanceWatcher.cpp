// SerializedClassInstanceWatcher.cpp: implementation of the SerializedClassInstanceWatcher class.
//
//////////////////////////////////////////////////////////////////////

#include "SerializedClassInstanceWatcher.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


#include <projects/BTree/BTree.h>
#include "IArchive.h"
#include "assert.h"

#define ASSERT assert

#define INSTANCE_VAR_NAME "_Link"

struct SerializeClassInstanceInfo
  {
  long keyid;
  unsigned long dataid;
  unsigned long section;  
  bool operator>(const SerializeClassInstanceInfo &other) const {return keyid>other.keyid;}
  bool operator<(const SerializeClassInstanceInfo &other) const {return keyid<other.keyid;}
  bool operator==(const SerializeClassInstanceInfo &other) const {return keyid==other.keyid;}
  bool operator>=(const SerializeClassInstanceInfo &other) const {return keyid>=other.keyid;}
  bool operator<=(const SerializeClassInstanceInfo &other) const {return keyid<=other.keyid;}
  bool operator!=(const SerializeClassInstanceInfo &other) const {return keyid!=other.keyid;}
  };


template<>
void BTreeUnsetItem(SerializeClassInstanceInfo &_item)
  {
  _item.keyid=0;
  }



class tBTreeDB_SC: public BTree<SerializeClassInstanceInfo>
  {
  };

typedef BTreeIterator<SerializeClassInstanceInfo> tBTreeDBIter;

SerializedClassInstanceWatcher::SerializedClassInstanceWatcher()
  {
  db=NULL;
  counter=1;
  }

SerializedClassInstanceWatcher::~SerializedClassInstanceWatcher()
  {
  delete db;
  }

void SerializedClassInstanceWatcher::ResetDBSection(unsigned long section)
  {  
  if (db && db->Size())
    {
    bool opak=true;
    while (opak)
      {
      opak=false;
      tBTreeDBIter iter(*db);
      SerializeClassInstanceInfo *item=db->Smallest();
      iter.BeginFrom(*item);
      while ((item=iter.Next())!=NULL)
        {
        db->Remove(*item);
        opak=true;
        }
      }
    }
  }

bool SerializedClassInstanceWatcher::SaveInstanceID(IArchive &arch,SerializedClass *cls)
  {
  if (cls==NULL)
	{
	long null=0;
	arch(INSTANCE_VAR_NAME,null);
	return true;
	}
  if (db==NULL) db=new tBTreeDB_SC;
  SerializeClassInstanceInfo key,*found;
  key.keyid=(unsigned long)cls;  
  found=db->Find(key);
  if (found)
	{
	long cnt=-(long)(found->dataid);
	arch(INSTANCE_VAR_NAME,cnt);
	return false;
	}
  else
	{
	key.dataid=counter++;
	ASSERT(counter<0x7FFFFFFF);
	key.section=0; //not supported yet
    db->Add(key);
	arch(INSTANCE_VAR_NAME,key.dataid);
	return true;
	}
  }

long SerializedClassInstanceWatcher::LoadInstanceID(IArchive &arch,SerializedClass **cls)
	{
    if (db==NULL) db=new tBTreeDB_SC;
	SerializeClassInstanceInfo key,*found;
    arch(INSTANCE_VAR_NAME,key.keyid);
	if (key.keyid==0) 
	  {
	  *cls=NULL;
	  return 0;
	  }
	bool folowinstance=key.keyid>0;
	key.keyid=abs(key.keyid);
	found=db->Find(key);
	if (found && folowinstance) return -1;
	if (!found && !folowinstance) return -1;
	if (!found) 
	  {
	  *cls=NULL;
	  return (long)key.keyid;
	  }
	else
	  {
	  *cls=(SerializedClass *)found->dataid;
	  return (long)key.keyid;
	  }
	}

void SerializedClassInstanceWatcher::RegisterNewInstance(long keyid, SerializedClass *cls)
  {
  if (db==NULL) db=new tBTreeDB_SC;
  SerializeClassInstanceInfo key;
  key.keyid=keyid;
  key.dataid=(unsigned long)cls;
  db->Add(key);
  }

SerializedClass *SerializedClassInstanceWatcher::FindInstance(long keyid)
  {
  SerializeClassInstanceInfo key,*found;
  key.keyid=keyid;
  found=db->Find(key);
  if (found==NULL) return NULL;
  return (SerializedClass *)found->dataid;
  }