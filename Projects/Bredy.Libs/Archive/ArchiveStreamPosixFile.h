// ArchiveStreamPosixFile.h: interface for the ArchiveStreamPosixFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVESTREAMPOSIXFILE_H__7C0A4DF7_254D_4665_9BF4_5B6513FAD754__INCLUDED_)
#define AFX_ARCHIVESTREAMPOSIXFILE_H__7C0A4DF7_254D_4665_9BF4_5B6513FAD754__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArchiveStream.h"

class ArchiveStreamPosixFile : public ArchiveStream  
  {
  protected:
    int _handle;
    int _err;
    bool _autoclose;
    
    
  public:
    ArchiveStreamPosixFile(int handle,bool autoclose);
    virtual ~ArchiveStreamPosixFile();
    virtual int IsError();
    virtual void Reset();
    virtual __int64 Tell();
    virtual void Seek(__int64 lOff, SeekOp seekop); 
    
  };










class ArchiveStreamPosixFileIn : public ArchiveStreamPosixFile
  {
  public:
    ArchiveStreamPosixFileIn(int handle,bool autoclose):ArchiveStreamPosixFile(handle,autoclose) 
      {}
    virtual ~ArchiveStreamPosixFileIn();
    virtual bool IsStoring() 
      {return false;}
    virtual int DataExchange(void *buffer, int maxsize);
    virtual void Reserved(int bytes);
    
  };










class ArchiveStreamPosixFileOut : public ArchiveStreamPosixFile
  {
  public:
    ArchiveStreamPosixFileOut(int handle,bool autoclose):ArchiveStreamPosixFile(handle,autoclose) 
      {}
    virtual ~ArchiveStreamPosixFileOut();
    virtual bool IsStoring() 
      {return true;}
    virtual int DataExchange(void *buffer, int maxsize);
    virtual void Reserved(int bytes);
    
  };










#endif // !defined(AFX_ARCHIVESTREAMPOSIXFILE_H__7C0A4DF7_254D_4665_9BF4_5B6513FAD754__INCLUDED_)
