// Archive.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Archive.h"
#include "ArchiveStreamWindowsFile.h"
#include "ArchiveStreamFileMapping.h"
#include "ArchiveStreamMemory.h"
#include "ArchiveStreamBase64Filter.h"
#include "ArchiveStreamCompressFilter.h"
#include "ArchiveStreamQ.h"
#include "ArchiveStreamTCP.h"
#include "assert.h"
#include <process.h>
#include "WSA.h"
#include <El\ParamFile\paramFile.hpp>
#include "ArchiveParamFile2.h"
#include "SerializedClass.h"
#include "SerializedClassInstanceWatcher.h"

struct ExampleStruct
  {
  int fill[10];
  ExampleStruct() {for (int i=0;i<10;i++) fill[i]=i;}
  void Serialize(IArchive &arch) 
    {
	char buff[20];
    for (int i=0;i<10;i++) 
	  {
	  sprintf(buff,"value",i);
	  arch(buff,fill[i],5);
	  }
    }
  };

void ArchiveVariableExchange(IArchive &arch,ExampleStruct &data)
  {
  int p=10;
  arch(p);
  arch(p);
  data.Serialize(arch);
  }

class ExampleClass1:public SerializedClass
  {
  public:
	int a;
	ExampleClass1(int a):a(a) {}
	ExampleClass1() {}
    ExampleStruct example;
	virtual void Serialize(IArchive &arch);
  };


void ExampleClass1::Serialize(IArchive &arch)
  {
  arch("a",a);
  arch("ExampleStruct",example);
  }

class ExampleClass2:public SerializedClass
  {
  public:
	float b;
	ExampleClass2(float b):b(b) {}
	ExampleClass2() {}
	virtual void Serialize(IArchive &arch);
  };

void ExampleClass2::Serialize(IArchive &arch)
  {
  arch("b",b);
  }

class ExampleClassWhole:public SerializedClass
  {
  public:
	SerializedClass *a1;
	SerializedClass *a2;
	SerializedClass *a3;
	ExampleClassWhole();
	~ExampleClassWhole();
	void Serialize(IArchive &arch);
  };

ExampleClassWhole::ExampleClassWhole()
  {
  a1=new ExampleClass1(10);
  a2=new ExampleClass2(20.0f);
  a3=a1;
  }

ExampleClassWhole::~ExampleClassWhole()
  {
  delete a1;
  delete a2;
  }

void ExampleClassWhole::Serialize(IArchive &arch)
  {
  arch("a1",a1);
  arch("a3",a3);
  arch("a2",a2);
  }

static ARegisterClass<ExampleClassWhole> ___reg1(0x10);
static ARegisterClass<ExampleClass1> ___reg2(0x20);
static ARegisterClass<ExampleClass2> ___reg3(0x30);



DWORD GetMappingSize(HANDLE mapp)
  {
  DWORD initial=0;
  DWORD switchbit=0x10000000;
  while (switchbit)
	{
	initial^=switchbit;
	LPVOID trymap=MapViewOfFile(mapp,FILE_MAP_READ,0,0,initial);
	if (trymap==NULL) initial^=switchbit;
	else UnmapViewOfFile(trymap);
	switchbit>>=1;
	}
  return initial;
  }

int main(int argc, char* argv[])
  {
/*  HANDLE mapp=CreateFileMapping(NULL,NULL,PAGE_READWRITE,0,0x70a0a0a0,NULL);
  DWORD size=GetMappingSize(mapp);*/

  ExampleClassWhole *v=NULL;
  v=new ExampleClassWhole;

  ParamFile pf;
	{
    SerializedClassInstanceWatcher ww;  
	ArchiveParamFile2Out arch(pf);
	arch.instanceWatcher=&ww;
    arch("v",v);
	}
  delete v; 
  v=NULL;
  pf.Save("pokus.pf");	
/*
  ArchiveStreamMemoryIn memin(memout.GetBuffer(),memout.GetBufferSize(),false);
	{
    SerializedClassInstanceWatcher ww;  
	ArchiveSimpleBinary arch(memin);
    arch.instanceWatcher=&ww;
	arch("v",v);
	}
*/

  return 0;
  }  
