// Pathname.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include "Pathname.h"

int main(int argc, char* argv[])
{
  Pathname cur="C:\\WINDOWS\\sys-media\\sound files\\jungle\\project.dsw";
  Pathname pth("file.dat",cur);
  printf("%s\n",pth.GetFullPath());
  printf("%s\n",cur.GetFullPath());
  pth.FullToRelative(cur);
  printf("%s\n",pth.GetFullPath());
  pth.RelativeToFull(cur);
  printf("%s\n",pth.GetFullPath());
  cur.SetPathName("pokus.dat");
  return 0;
}

