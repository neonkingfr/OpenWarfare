# Microsoft Developer Studio Project File - Name="Pathname" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=Pathname - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Pathname.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Pathname.mak" CFG="Pathname - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Pathname - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "Pathname - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Projects/Bredy.Libs/Pathname", NITAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Pathname - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "Pathname - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ  /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ  /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Pathname - Win32 Release"
# Name "Pathname - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Pathname.cpp
# End Source File
# Begin Source File

SOURCE=.\WFilePathMain.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Pathname.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Group "document"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\doc\annotated.html
# End Source File
# Begin Source File

SOURCE=.\doc\classPathname.html
# End Source File
# Begin Source File

SOURCE=.\doc\doxygen.css
# End Source File
# Begin Source File

SOURCE=.\doc\doxygen.png
# End Source File
# Begin Source File

SOURCE=.\doc\files.html
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2blank.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2doc.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2folderclosed.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2folderopen.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2lastnode.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2link.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2mlastnode.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2mnode.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2node.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2plastnode.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2pnode.png
# End Source File
# Begin Source File

SOURCE=.\doc\ftv2vertline.png
# End Source File
# Begin Source File

SOURCE=.\doc\functions.html
# End Source File
# Begin Source File

SOURCE=.\doc\globals.html
# End Source File
# Begin Source File

SOURCE=.\doc\index.html
# End Source File
# Begin Source File

SOURCE=.\doc\main.html
# End Source File
# Begin Source File

SOURCE=.\doc\Pathname_8cpp.html
# End Source File
# Begin Source File

SOURCE=".\doc\Pathname_8h-source.html"
# End Source File
# Begin Source File

SOURCE=.\doc\Pathname_8h.html
# End Source File
# Begin Source File

SOURCE=.\doc\tree.html
# End Source File
# Begin Source File

SOURCE=.\doc\tree.js
# End Source File
# Begin Source File

SOURCE=.\doc\treeview.js
# End Source File
# Begin Source File

SOURCE=.\doc\WFilePathMain_8cpp.html
# End Source File
# End Group
# Begin Source File

SOURCE=.\module.xml
# End Source File
# End Target
# End Project
