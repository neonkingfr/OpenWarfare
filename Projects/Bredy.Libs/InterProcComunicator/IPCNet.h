#include "IProcComunicator.h"

#include <El/TCPIPBasics/Socket.h>
#include <El/TCPIPBasics/IPA.h>

class ConnectionTCP;

#define IPC_PACKAGE_TCP_TAG 105

class SendPackageTCP:public ISendPackage
{
  friend ConnectionTCP;
  void *_data;
  size_t _size;
  size_t _alloc;
  size_t _pos;
public:
  SendPackageTCP();
  virtual ~SendPackageTCP(void);

  ///Function allocates space in package and locks it for modifications
  /**
  @param sz requested size of package
  @return pointer to locked data, or NULL, when error has occured
  */
  virtual void *Lock(size_t sz);

  ///Funtion unlocks the package. 
  /** 
  @param sz final size of package. This size can be lower or equal to requested
  size in Lock function. Use 0 to unlock whole package.*/
  virtual bool Unlock(size_t sz=0);

  ///Function writes data into package
  /**
  This function simulates streamed package, but it can be applied to
  nonstreamed package. Data is written to buffer, and after writting done,
  all data in buffer is sent. Streamed package writes data directly to the 
  connection, and this data can be immediatelly readed on other side  
  @param data pointer to data
  @param sz size of data
  @param block if block is true, function will wait for all writtings. 
    If block is false, function will not wait but, it can write less 
    than specified data (or none).
  @return size of written data, or zero, if no data has been written, or 
    an error occured.
  */
  virtual size_t Write(const void *data, size_t sz, bool block=true)
    {
    void *ptr=Lock(sz);
    if (ptr==NULL) return 0;
    memcpy(ptr,data,sz);
    Unlock();
    return sz;
    }

  ///Function returns true, if connection has been lost
    virtual bool Lost() {return false;}

  ///Sets the package options.
  /** Some kinds of packages allow associoate a unsigned options, which
  are transfered with the package. Examples: WPARAM, LPARAM in WinAPI messages,
  HTTP header in HTTP connection, etc.

  Most of packages needs set these data before any Locks or Writes processed.

  @param type Connection specified type of data
  @param data Connection specified data
  @return true - data has been accepted and will be transfered. false - data has
    not been accepted, for example unknown type, or is too late.
  */
  virtual bool SetOption(unsigned long type, int data) {return false;}
  virtual bool SetOption(unsigned long type, const char *data) {return false;}
  virtual bool SetOption(unsigned long type, const unsigned short *data) {return false;}

  enum commonTypes
  {
      ///defines value for WPARAM in WinAPI message
    typeWParam=1,     
      ///defines value for LPARAM in WinAPI message
    typeLParam=2,     
      ///adds header line to HTTP request
    typeHTTPHeader=3, 
      ///defines value for message
      /** Receivers are often set to send specified message to thread, when
         package arrives. This option can override this setting.
         */
    typeMessage=4,    
  };
};

class ConnectionTCP:public IConnection
{
  Socket _sock;
public:  
  ConnectionTCP(Socket connection):_sock(connection) {}  
  virtual ~ConnectionTCP(void);

  ///Function opens the package for writting
  /** Nonstreamed connection only allocates new package. Streamed connections
  prepares other side to receive the new stream. Streamed connections mostly
  allows to have one opened package at the time
  @return pointer to new package, or NULL, when error. 
  */
  virtual ISendPackage *OpenPackage();

  ///Function closes the package.
  /** Nonstreamed connection now sends the package to other side. Streamed 
  connecton will set stream to EOS state.
  */
  virtual bool ClosePackage(ISendPackage *package);
};

class ReceivedPackageTCP:public IReceivedPackage
{  
  void *_data;
  size_t _size;
  size_t _rdpos;
public:  
  ReceivedPackageTCP():_data(0),_size(0),_rdpos(0) {}
  bool BuildPackage(Socket &_sock);

  virtual ~ReceivedPackageTCP(void) {free(_data);}
  
  ///Returns size of package. Returns 0 if package is stream
  virtual size_t Size() const {return _size;}  
  ///Returns true, if package is streamed
  virtual bool IsStream() const {return false;}
  ///Reads package
  /**Reads data from package and increase internal reading pointer
  if package is stream, reads the stream. 
  @param buffer pointer to memory reserved for data;
  @param size size of memory
  @param block if block parameter is true, function will wait for all
               requested data. This can happen with streamed package only.
               Setting block to false causes that function can read
               less than requested size. 
  @return function returns number of bytes read. Function can return zero
          if there was no data ready. Function also returns 0, when end of
          stream has been reached. To test this situation, call Eos.*/
  virtual size_t Read(void *buffer, size_t size, bool block=true);

  ///function return true, when end of stream has been reached
  virtual bool Eos() const {return _rdpos>=_size;}

  ///Function returns pointer to received data.
  /**Function returns NULL, when package is stream. 
  Generally, it is more flexibile using Read function
  */
  virtual const void *Data() const {return _data;}

  ///function returns package's tag. It is useful to separate different types of packages
  virtual unsigned long PackageTag() const {return IPC_PACKAGE_TCP_TAG;}
};

class ConnReceiverTCP:public IConnReceiver
{
  Socket _sock;
  ReceivedPackageTCP *_attached;
  bool _fail;
public:
  ConnReceiverTCP(Socket sock):_sock(sock),_fail(false) {_attached=0;}
  ///Converts handle to IReceivedPackage, that will help to access the data
  /**
  @param dataHandle handle to data received using the common messaging system 
    (for example Windows Messaging)
  @param detach if detach is false, returned pointer is valid until
    next function call. During processing the package, receiver can be locked.
    If detach is true, interface complettly detaches the package, and returned
    pointer is valid until ClosePackage is called.*/
  virtual IReceivedPackage *OpenPackage(ICHANDLE dataHandle, bool detach=true);
  
  ///Function destroys package created by OpenPackage function
  virtual bool ClosePackage(IReceivedPackage *package);

  bool Failed() {return _fail;}


  
  virtual ~ConnReceiverTCP(void) {delete _attached;}
};
