#include "common.h"
#include ".\IpcPipe.h"

SendPackagePipe::SendPackagePipe(HWND target, UINT message, DWORD thread):
  _target(target),_message(message),_wParam(0),_lParam(0),
    _thisSide(0),_lockData(NULL),
    _lockSize(0),_state(notConnected),_blocking(true),
    _thread(thread)
{  
}

SendPackagePipe::~SendPackagePipe()
{
  if (_lockData) free(_lockData);
  if (_thisSide) CloseHandle(_thisSide);
}


#include <Tlhelp32.h>
static DWORD FindProcessToThread(DWORD thread)
{
  HANDLE snapshot=CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD,0);
  THREADENTRY32 entry;
  if (Thread32First(snapshot,&entry))
  {
    do
    {
      if (entry.th32ThreadID==thread) 
      {
        CloseHandle(snapshot);
        return entry.th32OwnerProcessID;
      }
    }
    while (Thread32Next(snapshot,&entry));
  }
  return 0;
}

bool SendPackagePipe::InitiateConnection()
{
  HANDLE otherSide;
  if (CreatePipe(&otherSide,&_thisSide,NULL,0)==FALSE) return false;
  HANDLE targetprocess=NULL;
  if (_target==0)
  {
    DWORD process;
    if (::GetWindowThreadProcessId(_target,&process)==0) return false;
    targetprocess=OpenProcess(PROCESS_DUP_HANDLE,FALSE,process);
    if (targetprocess==NULL) return false;
  }
  else
  {
    DWORD process=FindProcessToThread(_thread);
    if (process==0) return false;
    targetprocess=OpenProcess(PROCESS_DUP_HANDLE,FALSE,process);
    if (targetprocess==NULL) return false;
  }
  if (DuplicateHandle(GetCurrentProcess(),otherSide,targetprocess,&otherSide,0,TRUE,DUPLICATE_SAME_ACCESS|DUPLICATE_CLOSE_SOURCE)==FALSE)
    {CloseHandle(targetprocess);return false;}
  BOOL succ=FALSE;
  if (_thread)
  {
    succ=PostThreadMessage(_thread,_message,_wParam,(LPARAM)otherSide);
  }
  else if (_target)
  {
    succ=PostMessage(_target,_message,_wParam,(LPARAM)otherSide);
  }
  CloseHandle(targetprocess);
  if (succ==FALSE)
  {
    CloseHandle(_thisSide);_thisSide=NULL;
    return false;
  }
  else
  {
    _state=Connected;
    return true;
  }
}

void *SendPackagePipe::Lock(size_t sz)
{  
  if (_state==notConnected && InitiateConnection()==false)  {_state=LostConn;return NULL;}

  if (_lockData) return NULL;

  _lockData=malloc(sz);
  _lockSize=sz;
  return _lockData;
}
bool SendPackagePipe::Unlock(size_t sz)
{
  if (sz>_lockSize) return false;
  if (sz==0) sz=_lockSize;
  if (Write(_lockData,_lockSize)!=_lockSize) _state=LostConn;
  free(_lockData);
  _lockData=NULL;
  return true;
}

size_t SendPackagePipe::Write(const void *data, size_t sz, bool block)
{
  if (_state==LostConn) return 0;
  if (block!=_blocking)
  {
    if (block) 
    {
      DWORD flags=PIPE_WAIT|PIPE_READMODE_BYTE;
      SetNamedPipeHandleState(_thisSide,&flags,NULL,NULL);
    }
    else
    {
      DWORD flags=PIPE_NOWAIT|PIPE_READMODE_BYTE;
      SetNamedPipeHandleState(_thisSide,&flags,NULL,NULL);
    }
    _blocking=block;
  }
  DWORD written=0;
  if (WriteFile(_thisSide,data,(DWORD)sz,&written,NULL)==FALSE)
  {
    _state=LostConn;
  }
  return written;

}

bool SendPackagePipe::SetOption(unsigned long type, int data)
{
  switch (type)
  {
  case typeWParam: _wParam=data;return true;
  case typeMessage: _message=data;return true;
  }
  return false;
}

bool SendPackagePipe::Lost()
{
  return _state==LostConn;
}

ISendPackage *ConnectionPipe::OpenPackage()
{ 
  return new SendPackagePipe(_target,_message,_thread);
}
bool ConnectionPipe::ClosePackage(ISendPackage *package)
{
  delete package;
  return true;
}

ReceivedPackagePipe::ReceivedPackagePipe(HANDLE pipe):_pipe(pipe),_eos(false),
_blocking(true)
{
}


size_t ReceivedPackagePipe::Read(void *pointer, size_t size, bool block)
{
  if (_eos) return 0;
  if (block!=_blocking)
  {
    if (block) 
    {
      DWORD flags=PIPE_WAIT|PIPE_READMODE_BYTE;
      SetNamedPipeHandleState(_pipe,&flags,NULL,NULL);
    }
    else
    {
      DWORD flags=PIPE_NOWAIT|PIPE_READMODE_BYTE;
      SetNamedPipeHandleState(_pipe,&flags,NULL,NULL);
    }
    _blocking=block;
  }
  DWORD written=0;
  if (ReadFile(_pipe,pointer, (DWORD)size,&written,NULL)==FALSE)
    {_eos=true;}
  return written;
}

IReceivedPackage *ConnReceiverPipe::OpenPackage(ICHANDLE dataHandle, bool detach)
{
  IReceivedPackage *package=new ReceivedPackagePipe((HANDLE)dataHandle);
  if (!detach)
  {
    if (_curPackage) delete _curPackage;
    _curPackage=package;
  }
  return package;
}

bool ConnReceiverPipe::ClosePackage(IReceivedPackage *package)
{
  if (package->PackageTag()==ReceivedPackagePipe_TAG)
  {
    if (_curPackage==package) return true;
    delete package;
    return true;
  }
  else 
    return false;
}

ReceivedPackagePipe::~ReceivedPackagePipe(void)
{
  CloseHandle(_pipe);
}

ConnReceiverPipe::~ConnReceiverPipe(void)
{
  if (_curPackage) delete _curPackage;
}