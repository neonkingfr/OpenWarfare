#pragma once
#include "iproccomunicator.h"


class SendPackageSharedMem:public ISendPackage
{
public:
  HANDLE _sharedMem;
  size_t _sharedSize;
  size_t _sharedAlloc;
  void *_lockedData;
  size_t _lockedSize;
  UINT _message;
  WPARAM _wParam;
  HWND _targetWnd;
  DWORD _targetThread;
  DWORD _sendTimeOut;
  DWORD_PTR *_sendResult;
public:
  
  SendPackageSharedMem(HWND targetWindow, DWORD targetThread, UINT message):
      _sharedMem(NULL),_sharedSize(0),_sharedAlloc(0),
        _lockedData(0),_lockedSize(0),
        _message(message),
        _wParam(0),
        _targetWnd(targetWindow),
        _targetThread(targetThread),_sendTimeOut(INFINITE),_sendResult(0) {}
  ~SendPackageSharedMem(void);

  virtual void *Lock(size_t sz);

  virtual bool Unlock(size_t sz=0);
  virtual size_t Write(const void *data, size_t sz, bool block=true);
  virtual bool Lost() {return false;}
  virtual bool SetOption(unsigned long type, int data);

  enum Options
  {
    OptSendTimeout=100,
    OptSendResult=101,
  };

  bool SendPackage();

};

class ConnectionSharedMem:public IConnection
{
  HWND _targetWnd;
  DWORD _targetThread;
  UINT _message;
public:  
  ConnectionSharedMem(HWND _targetWnd, UINT message);
  ConnectionSharedMem(DWORD _targetThread, UINT message);
  virtual ISendPackage *OpenPackage();
  virtual bool ClosePackage(ISendPackage *package);
};

#define ReceivedPackageSharedMem_TAG 4558

class ReceivedPackageSharedMem:public IReceivedPackage
{  
  size_t _rdpos;
  size_t _size;
  void *_data;
  HANDLE _sharedMem;
public:  
  ReceivedPackageSharedMem(HANDLE sharedMem);
  virtual ~ReceivedPackageSharedMem(void);
  virtual size_t Size() const {return _size;}
  virtual bool IsStream() const {return false;}
  virtual size_t Read(void *buffer, size_t size, bool block=true);

  virtual bool Eos() const {return _rdpos>=_size;}
  virtual const void *Data() const {return _data;}
  virtual unsigned long PackageTag() const {return ReceivedPackageSharedMem_TAG;}
};

class ConnReceiverSharedMem:public IConnReceiver
{
  IReceivedPackage *_current;
public:  
  ConnReceiverSharedMem();
  virtual IReceivedPackage *OpenPackage(ICHANDLE dataHandle, bool detach=true);
  virtual bool ClosePackage(IReceivedPackage *package);
  virtual ~ConnReceiverSharedMem(void);
};
