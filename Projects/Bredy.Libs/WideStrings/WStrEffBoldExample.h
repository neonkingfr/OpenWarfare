// WStrEffBoldExample.h: interface for the WStrEffBoldExample class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WSTREFFBOLDEXAMPLE_H__51AEF453_9363_44C6_84FD_90D59EA92A3F__INCLUDED_)
#define AFX_WSTREFFBOLDEXAMPLE_H__51AEF453_9363_44C6_84FD_90D59EA92A3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IWStringEffect.h"

class WStrEffBoldExample : public IWStringEffect  
{
public:
  unsigned long GetEffectExtraSize(unsigned long curSize);
  unsigned long PreRenderString(wchar_t *renderPtr,unsigned long curSize);
  void RenderString(wchar_t *renderPtr, unsigned long rendered);
};

extern WStrEffBoldExample GWStrEffBold;

#endif // !defined(AFX_WSTREFFBOLDEXAMPLE_H__51AEF453_9363_44C6_84FD_90D59EA92A3F__INCLUDED_)
