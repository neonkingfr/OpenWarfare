#pragma once

class WStringProxy;

class WStringImmMemory
{
  char *_memory;      //pointer to preallocated memory 
  size_t _memsz;      //total size available in memory
  size_t _nextBlock;   //pointer to next block that will be allocated
  size_t _memused;    //size of used memory (top of memory)
  size_t _totalAlloc; //count of allocations

protected:
  WStringProxy *FindFreeBlock(size_t sz);
public:
  WStringImmMemory(void);
  ~WStringImmMemory(void);

  bool IsInited() {return _memory!=0;}

  bool Create(size_t sz);

  //strSz is in bytes
  WStringProxy *AllocStringBlock(size_t strSz);
  bool FreeStringBlock(WStringProxy *block);


};
