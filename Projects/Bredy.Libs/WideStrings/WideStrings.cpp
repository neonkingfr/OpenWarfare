// WideStrings.cpp : Defines the entry point for the console application.
//

#include "WString.h"
#include "WStrEffBoldExample.h"
#include "WPathname.h"
#include <crtdbg.h>


int wmain(int argc, wchar_t* argv[])
{    
    WString a((WSC("ahoj ")+WSC("nazdar ")+WSC("cau")).Upper().Delete(3,5));
	WString b(a);


	a.GetString();
	b.GetString();

    WPathname fpath;    
    fpath=fpath.GetExePath();
    wprintf(L"%s\n",fpath.GetFullPath());
    fpath+=L"..\\..\\Test\\Project.hpp";
    wprintf(L"%s\n",fpath.GetFullPath());
    fpath.FullToRelative(fpath.GetExePath());
    wprintf(L"%s\n",fpath.GetFullPath());
    fpath+=L"d:\\pokus\\xxx.txt";
    wprintf(L"%s\n",fpath.GetFullPath());


	return 0;
}
