// WStrEffBoldExample.cpp: implementation of the WStrEffBoldExample class.
//
//////////////////////////////////////////////////////////////////////


#include "WStrEffBoldExample.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

unsigned long WStrEffBoldExample::GetEffectExtraSize(unsigned long curSize) 
{
  return 7; //7 extra characters total "<B></B>"
}

unsigned long WStrEffBoldExample::PreRenderString(wchar_t *renderPtr,unsigned long curSize)
{
  wcscpy(renderPtr,L"<B>");
  return 3; //3 characters has been rendered
}

void WStrEffBoldExample::RenderString(wchar_t *renderPtr, unsigned long rendered)
{
  wcscpy(renderPtr+rendered+3,L"</B>");
}

WStrEffBoldExample GWStrEffBold;