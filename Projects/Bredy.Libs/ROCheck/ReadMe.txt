Na vcs://Pgm/El/ROCheck/ je pokus o standarizov�n� chov�n� programu p�i otev�r�n� a z�pisu RO souboru. 

Knihovna se nach�z� v souborech ROCheck.cpp a ROCheck.h. 
K zapojen� do programu je t�eba je�t� ROCheck.rc2

Instalace:
==========

Krom� vlo�en� zdroj�k� do projektu a "zainkludov�n�" hlavi�ky je nutn� je�t� nainstalovat dialogov� okno. N�sleduje jednoduch� postup
1) Otev�ete RC soubor va�� aplikace jako Text (v okne Open vyberte Text, nebo pou�ijte jin� textov� editor)
2) Do m�st, kde se p�e 
        "non-Microsoft Visual C++ edited resources" 
      pop�. pod
         #ifndef APSTUDIO_INVOKED
      nebo na konec soubor napi�te        
         #include <el/ROCheck/ROCheck.rc2>
3) A soubor ulo�te, zav�ete, a reloadujte v MSDEV 

Nev�m jak to funguje v .NET. Z�kladem je do resourc� vlo�it soubor pomoc� include tak, aby to wizard pozd�ji p��padn� nesmazal.

T�m je instalace kompletn�.


Pou�it�:
========

Knihovna m� jedinou funkci:

ROCheckResult TestFileRO(
        HWND hWndOwner, 
        const _TCHAR *szFilename, 
        int flags=0,
        HICON hIcon=NULL, 
        HINSTANCE hInst=NULL
        );

Funkce testuje,zda soubor szFilename je RO. Pokud ano, zobraz� dialog.

hWndOwner = handle okna, pod kter�m se m� p��padn� dialog vytvo�en. M��e b�t NULL, pak funkce hled� aktivn� okno aplikace. Pokud jej nenajde, otev�e se na desktopu (v li�t�)

szFilename = jm�no souboru v�etn� cesty, jen� m� b�t zkontrolov�no.

flags = kombinace vlajek:
        ROCHF_DisableSaveAs = zak�e tla��tko SaveAs (vhodn� pro varov�n� p�i Open)
        ROCHF_DisableExploreButton = zak�e tla��tko Explore (nenapad� m� vyu�it�)
        ��dn� vlajky odpov�d� 0
          
hIcon = handle ikony, kter� se zobraz� v okn� vlevo. Pokud je NULL, zobraz� se vyk�i�n�k ve �lut�m troj�heln�ku

hInst = Handle Instance ve kter� je definov�n dialog "READONLYCHECKDIALOG". Pokud je NULL, bere se handle instance aplikace. Toto je nutn� v p��pad�, �e je knihovna pou�ita v modulu DLL.

Funkce vrac�:

ROCHK_FileOK = Soubor je R/W nebo neexistuje (�ili je mo�n� zapisovat)
ROCHK_FileRO = Soubor je R/O a u�ivatel s t�m nic neud�lal (�ili nelze zapisovat)
ROCHK_FileSaveAs = Soubor je R/O, ale u�ivatel si p�eje data ulo�it pod jin�m jm�nem (SaveAs)

Pokud u�ivatel pou�ije tla��tko Explore a pomoc� Exploreru soubor vyma�e nebo zru�� p��znak na R/O, pak po odklepnut�  okna (OK) vrac� ROCHK_FileOK - testuje stav R/O i po uzav�en� dialogu. 

Pokud nastane chyba p�i zobrazov�n� dialogu, program zobraz� varovn� okno a pak vrac� ROCHK_FileRO, ��m� zabr�n� v z�pisu, Chyba vznikne v�t�inou v p��pad�, �e jste zapom�li instalovat dialog do resources, nebo �e do�li syst�mov� zdroje, nebo v p��pad�, �e se windows�m dialog nel�b� :o)

To� v�e.

