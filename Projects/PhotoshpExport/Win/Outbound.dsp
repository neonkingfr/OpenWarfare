# Microsoft Developer Studio Project File - Name="Outbound" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=Outbound - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Outbound.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Outbound.mak" CFG="Outbound - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Outbound - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Outbound - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "Outbound"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Outbound - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ".\Release"
# PROP BASE Intermediate_Dir ".\Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ".\Release"
# PROP Intermediate_Dir ".\temp"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /c
# ADD CPP /nologo /G5 /MT /W3 /GR /GX /I "..\Common" /I "..\..\..\Common\Headers\Photoshop" /I "..\..\..\Common\Headers\Photoshop\ADM" /I "..\..\..\Common\Headers\Photoshop\AS" /I "..\..\..\Common\Headers\Photoshop\PICA" /I "..\..\..\Common\Headers\Photoshop\PS-Suites" /I "..\..\..\Common\Headers\SDK" /I "..\..\..\Common\Rez-files\Photoshop" /I "..\..\..\Common\Rez-files\SDK" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D MSWindows=1 /D "MFC_NEW" /YX /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /i "..\Common" /i "..\..\..\Common\Headers\Photoshop" /i "..\..\..\Common\Headers\Photoshop\ADM" /i "..\..\..\Common\Headers\Photoshop\AS" /i "..\..\..\Common\Headers\Photoshop\PICA" /i "..\..\..\Common\Headers\Photoshop\PS-Suites" /i "..\..\..\Common\Headers\SDK" /i "..\..\..\Common\Rez-files\Photoshop" /i "..\..\..\Common\Rez-files\SDK" /d "NDEBUG" /d MSWindows=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 comdlg32.lib user32.lib kernel32.lib gdi32.lib version.lib advapi32.lib /nologo /subsystem:windows /dll /machine:I386 /nodefaultlib:"libc.lib" /out:"C:\Program Files\Adobe\Photoshop 6.0\Plug-Ins\Adobe Photoshop Only\Import-Export\PAA_PAC.8be" /libpath:"..\Common" /libpath:"..\..\..\Common\Headers\Photoshop" /libpath:"..\..\..\Common\Headers\Photoshop\ADM" /libpath:"..\..\..\Common\Headers\Photoshop\AS" /libpath:"..\..\..\Common\Headers\Photoshop\PICA" /libpath:"..\..\..\Common\Headers\Photoshop\PS-Suites" /libpath:"..\..\..\Common\Headers\SDK" /libpath:"..\..\..\Common\Rez-files\Photoshop" /libpath:"..\..\..\Common\Rez-files\SDK"

!ELSEIF  "$(CFG)" == "Outbound - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\Debug"
# PROP BASE Intermediate_Dir ".\Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ".\Debug"
# PROP Intermediate_Dir ".\temp"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /c
# ADD CPP /nologo /G5 /MTd /W3 /Gm /GR /GX /ZI /Od /I "..\Common" /I "..\..\..\Common\Headers\Photoshop" /I "..\..\..\Common\Headers\Photoshop\ADM" /I "..\..\..\Common\Headers\Photoshop\AS" /I "..\..\..\Common\Headers\Photoshop\PICA" /I "..\..\..\Common\Headers\Photoshop\PS-Suites" /I "..\..\..\Common\Headers\SDK" /I "..\..\..\Common\Rez-files\Photoshop" /I "..\..\..\Common\Rez-files\SDK" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D MSWindows=1 /D "MFC_NEW" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /i "..\Common" /i "..\..\..\Common\Headers\Photoshop" /i "..\..\..\Common\Headers\Photoshop\ADM" /i "..\..\..\Common\Headers\Photoshop\AS" /i "..\..\..\Common\Headers\Photoshop\PICA" /i "..\..\..\Common\Headers\Photoshop\PS-Suites" /i "..\..\..\Common\Headers\SDK" /i "..\..\..\Common\Rez-files\Photoshop" /i "..\..\..\Common\Rez-files\SDK" /d "_DEBUG" /d MSWindows=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386
# ADD LINK32 comdlg32.lib user32.lib kernel32.lib gdi32.lib version.lib advapi32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /nodefaultlib:"libc.lib" /out:"C:\Program Files\Adobe\Photoshop 6.0\Plug-Ins\Adobe Photoshop Only\Import-Export\PAA_PAC.8be" /libpath:"..\Common" /libpath:"..\..\..\Common\Headers\Photoshop" /libpath:"..\..\..\Common\Headers\Photoshop\ADM" /libpath:"..\..\..\Common\Headers\Photoshop\AS" /libpath:"..\..\..\Common\Headers\Photoshop\PICA" /libpath:"..\..\..\Common\Headers\Photoshop\PS-Suites" /libpath:"..\..\..\Common\Headers\SDK" /libpath:"..\..\..\Common\Rez-files\Photoshop" /libpath:"..\..\..\Common\Rez-files\SDK"

!ENDIF 

# Begin Target

# Name "Outbound - Win32 Release"
# Name "Outbound - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;hpj;bat;for;f90"
# Begin Source File

SOURCE=..\..\..\El\Color\colorsK.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\Color\colorsP.cpp
# End Source File
# Begin Source File

SOURCE=.\ctLongLongTable.cpp
# End Source File
# Begin Source File

SOURCE=.\DialogClass.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\Evaluator\evaluatorExpress.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\Evaluator\evalUseLocalizeDefault.cpp
# End Source File
# Begin Source File

SOURCE=.\ExportDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\Evaluator\express.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\IMG\PICS\Fileutil.c
# End Source File
# Begin Source File

SOURCE=..\FileUtilities.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\IMG\GIF\Gif.c
# End Source File
# Begin Source File

SOURCE=..\..\..\IMG\GIF\Gifdecod.c
# End Source File
# Begin Source File

SOURCE=..\..\..\IMG\PICS\Imgload.c
# End Source File
# Begin Source File

SOURCE=..\..\..\IMG\PICTURE\lzcompr.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Outbound.cpp
# End Source File
# Begin Source File

SOURCE=.\Outbound.def
# End Source File
# Begin Source File

SOURCE=..\Common\OutboundScripting.cpp
# End Source File
# Begin Source File

SOURCE=.\OutboundUIWin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\IMG\PICTURE\picture.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\IMG\PICTURE\pictureConfig.cpp
# End Source File
# Begin Source File

SOURCE=..\PIUtilities.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\Common\randomGen.cpp
# End Source File
# Begin Source File

SOURCE=.\StringRes.cpp
# End Source File
# Begin Source File

SOURCE=..\WinDialogUtils.cpp
# End Source File
# Begin Source File

SOURCE=..\WinUtilities.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=..\BASETSD.H
# End Source File
# Begin Source File

SOURCE=.\ctLongLongTable.h
# End Source File
# Begin Source File

SOURCE=.\DialogClass.h
# End Source File
# Begin Source File

SOURCE=.\ExportDlg.h
# End Source File
# Begin Source File

SOURCE=..\FileUtilities.h
# End Source File
# Begin Source File

SOURCE=.\globals.h
# End Source File
# Begin Source File

SOURCE=".\Outbound-sym.h"
# End Source File
# Begin Source File

SOURCE=..\Common\Outbound.h
# End Source File
# Begin Source File

SOURCE=.\temp\Outbound.pipl
# End Source File
# Begin Source File

SOURCE=..\Common\OutboundTerminology.h
# End Source File
# Begin Source File

SOURCE=..\PIAbout.h
# End Source File
# Begin Source File

SOURCE=..\PIActions.h
# End Source File
# Begin Source File

SOURCE=..\..\..\IMG\PICTURE\picture.hpp
# End Source File
# Begin Source File

SOURCE=..\PIDefines.h
# End Source File
# Begin Source File

SOURCE=..\PIExport.h
# End Source File
# Begin Source File

SOURCE=..\PIGeneral.h
# End Source File
# Begin Source File

SOURCE=..\PIResDefines.h
# End Source File
# Begin Source File

SOURCE=..\PITerminology.h
# End Source File
# Begin Source File

SOURCE=..\PITypes.h
# End Source File
# Begin Source File

SOURCE=..\PIUtilities.h
# End Source File
# Begin Source File

SOURCE=.\PIUtilities.r
# End Source File
# Begin Source File

SOURCE=..\SPAdapts.h
# End Source File
# Begin Source File

SOURCE=..\SPBasic.h
# End Source File
# Begin Source File

SOURCE=..\SPCaches.h
# End Source File
# Begin Source File

SOURCE=..\SPConfig.h
# End Source File
# Begin Source File

SOURCE=..\SPFiles.h
# End Source File
# Begin Source File

SOURCE=..\SPMData.h
# End Source File
# Begin Source File

SOURCE=..\SPPiPL.h
# End Source File
# Begin Source File

SOURCE=..\SPPlugs.h
# End Source File
# Begin Source File

SOURCE=..\SPProps.h
# End Source File
# Begin Source File

SOURCE=..\SPStrngs.h
# End Source File
# Begin Source File

SOURCE=..\SPTypes.h
# End Source File
# Begin Source File

SOURCE=.\StringRes.h
# End Source File
# Begin Source File

SOURCE=..\WinDialogUtils.h
# End Source File
# Begin Source File

SOURCE=..\WinUtilities.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=..\Common\Outbound.r

!IF  "$(CFG)" == "Outbound - Win32 Release"

# Begin Custom Build - Compiling PiPL resource...
IntDir=.\temp
InputPath=..\Common\Outbound.r
InputName=Outbound

"$(IntDir)\$(InputName).pipl" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	cl /I..\..\..\Common\Headers\Photoshop\AS /I..\..\..\Common\Headers\Photoshop\ADM   /I..\..\..\Common\Headers\Photoshop\PICA /I..\..\..\Common\Headers\Photoshop\PS-Suites   /I..\..\..\Common\Headers\SDK /I..\..\..\Common\Headers\Photoshop   /I..\..\..\Common\Rez-files\Photoshop /I..\..\..\Common\Rez-files\SDK   /EP /DMSWindows=1 /Tc$(InputPath) > $(IntDir)\$(InputName).rr 
	cnvtpipl $(IntDir)\$(InputName).rr $(IntDir)\$(InputName).pipl 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "Outbound - Win32 Debug"

# Begin Custom Build
IntDir=.\temp
InputPath=..\Common\Outbound.r
InputName=Outbound

"$(IntDir)\$(InputName).pipl" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	cl /I..\..\..\Common\Headers\Photoshop\AS /I..\..\..\Common\Headers\Photoshop\ADM   /I..\..\..\Common\Headers\Photoshop\PICA /I..\..\..\Common\Headers\Photoshop\PS-Suites   /I..\..\..\Common\Headers\SDK /I..\..\..\Common\Headers\Photoshop   /I..\..\..\Common\Rez-files\Photoshop /I..\..\..\Common\Rez-files\SDK   /EP /DMSWindows=1 /Tc$(InputPath) > $(IntDir)\$(InputName).rr 
	cnvtpipl $(IntDir)\$(InputName).rr $(IntDir)\$(InputName).pipl 
	
# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Outbound.rc
# End Source File
# End Group
# Begin Group "El"

# PROP Default_Filter ""
# Begin Group "QStream"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\El\QStream\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\QStream\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\QStream\fileOverlapped.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\..\..\El\QStream\qbstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\QStream\qstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\QStream\qstreamUseBankDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\QStream\qstreamUseFServerDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\QStream\serializeBin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\QStream\ssCompress.cpp
# End Source File
# End Group
# Begin Group "ParamFile"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\El\ParamFile\classDbParamFile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\ParamFile\paramFile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\ParamFile\paramFileUseEvalDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\ParamFile\paramFileUseLocalizeDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\ParamFile\paramFileUsePreprocC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\ParamFile\paramFileUseSumCalcDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\PreprocC\Preproc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\PreprocC\preprocC.cpp
# End Source File
# End Group
# Begin Group "ParamArchive"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\El\ParamArchive\paramArchive.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\ParamArchive\paramArchiveDb.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\El\ParamArchive\paramArchiveUseDbParamFile.cpp
# End Source File
# End Group
# Begin Group "Interfaces"

# PROP Default_Filter ""
# End Group
# Begin Source File

SOURCE=..\..\..\el\FreeOnDemand\memFreeReq.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\FreeOnDemand\memFreeReqUseDefault.cpp
# End Source File
# End Group
# Begin Group "Es"

# PROP Default_Filter ""
# Begin Group "Strings"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\Es\Strings\rString.cpp
# End Source File
# End Group
# Begin Group "Framework"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\Es\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Es\Framework\useAppFrameDefault.cpp
# End Source File
# End Group
# Begin Group "Memory"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\Es\Memory\fastAlloc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\ES\Memory\useMallocMemFunctions.cpp
# End Source File
# End Group
# Begin Group "Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\Es\Files\filenames.cpp
# End Source File
# End Group
# End Group
# Begin Source File

SOURCE=..\..\..\es\framework\appframe.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE="C:\Program Files\Microsoft Visual Studio\VC98\Include\BASETSD.H"
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Strings\bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\ParamFile\classDbParamFile.hpp
# End Source File
# Begin Source File

SOURCE=.\Cnvtpipl.exe
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Containers\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Memory\debugAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\framework\debuglog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\elementpch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\Enum\enumNames.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\..\..\el\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Files\filenames.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\img\Pics\Fileutil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\img\Gif\Gif.h
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Containers\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\Interfaces\iClassDb.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\Interfaces\iEval.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\extern\ijl.h
# End Source File
# Begin Source File

SOURCE=..\..\..\el\Interfaces\iLocalize.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\img\Pics\Imgload.h
# End Source File
# Begin Source File

SOURCE=..\..\..\el\Interfaces\iPreproc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\QStream\iQFBank.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\Interfaces\iSumCalc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Containers\list.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Containers\listBidir.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\types\lLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\img\Picture\lzcompr.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\img\Pics\Macros.h
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\FreeOnDemand\memFreeReq.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\img\Pics\Memspc.h
# End Source File
# Begin Source File

SOURCE=..\..\..\es\types\memtype.h
# End Source File
# Begin Source File

SOURCE=..\..\..\el\PCH\normalConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Memory\normalNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\ParamArchive\paramArchive.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\ParamArchive\paramArchiveDb.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\ParamFile\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\ParamFile\paramFileDecl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\Common\perfLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\platform.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Algorithms\qsort.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Strings\rString.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Containers\rStringArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\ParamArchive\serializeClass.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\types\softLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Containers\staticArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\el\PCH\stdIncludes.h
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Containers\typeOpts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\es\Common\win.h
# End Source File
# End Target
# End Project
