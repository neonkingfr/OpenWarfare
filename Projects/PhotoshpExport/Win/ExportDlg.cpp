// ExportDlg.cpp: implementation of the CExportDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "globals.h"
#include "ExportDlg.h"
#include "Outbound-sym.h"
#include <es\Files\filenames.hpp>
#include "..\..\..\IMG\PICTURE\picture.hpp"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define MAPFILE "PAAPACPhotoshopExport"

CExportDlg::CExportDlg()
  {
  
  hMapFile = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, MAPFILE);
  if (hMapFile==0)
    {
    hMapFile = CreateFileMapping((HANDLE)0xFFFFFFFF,NULL,PAGE_READWRITE|SEC_RESERVE,0,1024*1024,MAPFILE); 
    created=true;
    }
  else
    created=false;
  defcount=(int *)MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0); // Map entire file. 
  initdir=(char *)(defcount+1);
  def=(DefValues *)(initdir+MAX_PATH);
  if (created)
    {
    VirtualAlloc(defcount,4+MAX_PATH,MEM_COMMIT,PAGE_READWRITE);
    *defcount=0;
    initdir[0]=0;
    }
  }

//--------------------------------------------------

CExportDlg::~CExportDlg()
  {
  if (!created)
    {
    UnmapViewOfFile(&def);
    CloseHandle(hMapFile);
    }
  }

//--------------------------------------------------

/*static bool PAAExport=true;
static bool PACExport=false;
static bool DXCompress=true;
static char initdir[MAX_PATH]="";
static char lastname[MAX_PATH]="";
static CExportDlg p;*/

static void PaaPac(char *fname, bool paa)
  {
  if (fname[0]==0) return;
  char *q=GetFileExt(GetFilenameExt(fname));
  if (q==NULL || q[0]==0) 
    {
    q=strchr(q,0);
    *q++='.';
    }
  else 
    q++;
  if (paa) strcpy(q,"paa");
  else strcpy(q,"pac");
  }

//--------------------------------------------------

LRESULT CExportDlg::OnInitDialog()
  {  
  DefValues *p=Find(key);
  if (p) 
    {
    cur=*p;
    SetDlgItemText(IDC_OUTFILE,cur.lastname);
    }
  else
    {
    memset(&cur,0,sizeof(cur));
    cur.PAAExport=this->paasave;
    cur.PACExport=!this->paasave;
    cur.kcolor=0;
    cur.kenable=false;
    cur.MipFilter=TexMipDefault;
    cur.limitSize=4096;
    if (key[0]<32)
      cur.lastname[0]=0;
    else
      GetFilename(cur.lastname,key);
    PaaPac(cur.lastname,paasave);
    EnableWindow(GetDlgItem(IDOK),FALSE);
    }
  SendDlgItemMessage(IDC_FILTER,CB_ADDSTRING,0,(LPARAM)StrRes[IDS_FILTER1]);
  SendDlgItemMessage(IDC_FILTER,CB_ADDSTRING,0,(LPARAM)StrRes[IDS_FILTER2]);
  SendDlgItemMessage(IDC_FILTER,CB_ADDSTRING,0,(LPARAM)StrRes[IDS_FILTER3]);
  CheckDlgButton(*this,IDC_EXPORTPAA,cur.PAAExport?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_EXPORTPAC,cur.PACExport?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_COLKEY,cur.kenable?BST_CHECKED:BST_UNCHECKED);
  kcolor=cur.kcolor;
  if (!p) ByHints(cur.lastname);
  strcpy(filename,cur.lastname);
  return 0;
  }

//--------------------------------------------------

char *MakeFilter(const char *text)
  {
  char *c=(char *)text;
  while ((c=strchr(c,'|')))
    {
    *c=0;
    c++;
    }
  return (char *)text;
  }

//--------------------------------------------------

LRESULT CExportDlg::OnOK()
  {
  ::GetDlgItemText(*this,IDC_OUTFILE,filename,MAX_PATH);  
  strcpy(cur.lastname,filename);
  paasave=cur.PAAExport=IsDlgButtonChecked(*this,IDC_EXPORTPAA)==BST_CHECKED;
  cur.PACExport=IsDlgButtonChecked(*this,IDC_EXPORTPAC)==BST_CHECKED;
  kenable=IsDlgButtonChecked(*this,IDC_COLKEY)==BST_CHECKED;
  cur.kcolor=kcolor;
  cur.kenable=kenable;
  SaveDefValues(key,&cur);  
  return CDialogClass::OnOK();
  }

//--------------------------------------------------

LRESULT CExportDlg::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  switch (msg)
    {
    case WM_DRAWITEM: 
    if (wParam==IDC_KKEY)
      {
      LPDRAWITEMSTRUCT lpdis=(LPDRAWITEMSTRUCT)lParam;
      bool chk=(lpdis->itemState & 1)!=0;
      DrawFrameControl(lpdis->hDC,&(lpdis->rcItem),DFC_BUTTON,DFCS_BUTTONPUSH|(chk?DFCS_PUSHED:0));
      RECT rc=lpdis->rcItem;
      rc.left+=4;
      rc.top+=4;
      rc.bottom-=4;
      rc.right-=4;
      HBRUSH brh=CreateSolidBrush(kcolor);
      FillRect(lpdis->hDC,&rc,brh);
      DeleteObject(brh);
      }
    if (wParam==IDC_OUTFILE)
      {
      LPDRAWITEMSTRUCT lpdis=(LPDRAWITEMSTRUCT)lParam;
      bool chk=(lpdis->itemState & 1)!=0;
      DrawFrameControl(lpdis->hDC,&(lpdis->rcItem),DFC_BUTTON,DFCS_BUTTONPUSH|(chk?DFCS_PUSHED:0));
      RECT rc=lpdis->rcItem;
      rc.left+=4;
      rc.top+=4;
      rc.bottom-=4;
      rc.right-=4;
      char p[MAX_PATH];
      ::GetDlgItemText(*this,wParam,p,MAX_PATH);
      DrawText(lpdis->hDC,p,-1,&rc,DT_VCENTER|DT_PATH_ELLIPSIS|DT_CENTER|DT_NOPREFIX);
      }
    return 1;
    default: return CDialogClass::DlgProc(msg,wParam,lParam);
    
    }
  
  }

//--------------------------------------------------

LRESULT CExportDlg::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  switch (wID)
    {
    case IDC_KKEY:
      {
      CHOOSECOLOR chcol;
      COLORREF precol[16];
      chcol.lStructSize=sizeof(chcol);
      chcol.hwndOwner=*this;
      chcol.hInstance=NULL;
      chcol.rgbResult=kcolor;
      chcol.lpCustColors=precol;
      chcol.Flags=CC_ANYCOLOR|CC_FULLOPEN|CC_RGBINIT;
      chcol.lCustData=0;
      chcol.lpfnHook=0;
      chcol.lpTemplateName=0;
      if (ChooseColor(&chcol))
        {
        kcolor=chcol.rgbResult;
        InvalidateRect(*this,NULL,FALSE);
        CheckDlgButton(*this,IDC_COLKEY,BST_CHECKED);
        }
      }
    return 1;
    case IDC_OUTFILE:
      {
      OPENFILENAME ofn;
      memset(&ofn,0,sizeof(ofn));
      ofn.lStructSize=sizeof(ofn);
      ofn.hwndOwner=*this;
      ofn.hInstance=hInstance;
      ofn.lpstrFilter=MakeFilter(IsDlgButtonChecked(*this,IDC_EXPORTPAA)==BST_CHECKED?StrRes[IDS_PAAFILTER]:StrRes[IDS_PACFILTER]);
      ofn.lpstrDefExt=def->PAAExport?StrRes[IDS_PAAEXT]:StrRes[IDS_PACEXT];
      ofn.lpstrFile=filename;
      if (GetWindowTextLength(GetDlgItem(IDC_OUTFILE)))  ::GetDlgItemText(*this,IDC_OUTFILE,filename,MAX_PATH);
      ofn.lpstrTitle=StrRes[IDS_EXPORTTITLE];
      if (initdir[0]==0) ofn.lpstrInitialDir=NULL;else  ofn.lpstrInitialDir=initdir;
      ofn.nMaxFile=MAX_PATH;
      ofn.Flags=OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY|OFN_PATHMUSTEXIST;
      if (GetSaveFileName(&ofn))
        {		
        SetDlgItemText(IDC_OUTFILE,filename);
        ByHints(filename);	
        strcpy(initdir,filename);
        char *c=strrchr(initdir,'\\');
        c[1]=0;
        EnableWindow(GetDlgItem(IDOK),TRUE);
        }			  
      }	
    return 1;
    case IDC_EXPORTPAA:
    case IDC_EXPORTPAC: 
      {
      char *p=GetDlgItemText(IDC_OUTFILE);
      p=(char *)realloc(p,strlen(p)+10);
      BOOL paa=IsDlgButtonChecked(*this,IDC_EXPORTPAA)==BST_CHECKED;
      if (p[0])
        {
        PaaPac(p,paa!=0);
        ByHints(p);
        }
      SetDlgItemText(IDC_OUTFILE,p);
      free(p);
      EnableWindow(GetDlgItem(IDC_FADEOUT),paa);
      }
    return 1;
    default: return CDialogClass::OnCommand(wID,wNotifyCode,clt);
    
    }
  }

//--------------------------------------------------

CExportDlg::DefValues * CExportDlg::Find(const char *k)
  {
  for (int i=0;i<*defcount;i++)
    if (stricmp(def[i].key,k)==0) return def+i;
  return NULL;
  }

//--------------------------------------------------

void CExportDlg::SaveDefValues(const char *k,DefValues *df)
  {
  DefValues *p=Find(k);
  if (p) *p=*df;
  else
    {
    DefValues *nw=def+*defcount;
    if (VirtualAlloc(nw,sizeof(*nw),MEM_COMMIT,PAGE_READWRITE)==NULL) return;
    defcount[0]++;
    *nw=*df;
    strcpy(nw->key,k);
    }
  }

//--------------------------------------------------

void CExportDlg::ByHints(const char *fname)
  {
  }

//--------------------------------------------------

