/*******************************************************************/
/*                                                                 */
/*                      ADOBE CONFIDENTIAL                         */
/*                   _ _ _ _ _ _ _ _ _ _ _ _ _                     */
/*                                                                 */
/* Copyright 1990 - 1999 Adobe Systems Incorporated                */
/* All Rights Reserved.                                            */
/*                                                                 */
/* NOTICE:  All information contained herein is, and remains the   */
/* property of Adobe Systems Incorporated and its suppliers, if    */
/* any.  The intellectual and technical concepts contained         */
/* herein are proprietary to Adobe Systems Incorporated and its    */
/* suppliers and may be covered by U.S. and Foreign Patents,       */
/* patents in process, and are protected by trade secret or        */
/* copyright law.  Dissemination of this information or            */
/* reproduction of this material is strictly forbidden unless      */
/* prior written permission is obtained from Adobe Systems         */
/* Incorporated.                                                   */
/*                                                                 */
/*******************************************************************/
//-------------------------------------------------------------------
/*
	File: OutboundUIWin.c

	Copyright 1990-1991, Thomas Knoll.
	All rights reserved.

	Windows platform dependent C source file for Export example.
*/

#include "Outbound.h"
#include "../WinDialogUtils.h"
#include "./DialogClass.h"
#include "./Outbound-sym.h"

/*****************************************************************************/

const uint8 kDEditText = 4;

/*****************************************************************************/

BOOL WINAPI ExportProc(HWND hDlg, UINT wMsg, WPARAM wParam, LPARAM lParam);

/*****************************************************************************/

void DoAbout (AboutRecordPtr about)
  {
  CDialogClass dlg;
  dlg.DoModal(IDD_ABOUT,GetForegroundWindow());

  }

/*****************************************************************************/

Boolean DoUI (GPtr globals)
{
	char *my_lpstrFile = (char *)gStuff->filename;
	char my_filename[256];
	char my_lpstrFileTitle[256];
	char my_extension[_MAX_EXT] = ".exp";
	char my_lpstrInitialDir[256];
	char my_dir[256];
	const char *my_lpstrDefExt = "exp";
	const char *my_lpstrFilter = "Outbound export (*.exp)\0*.exp\0\0\0";
	const char *my_lpstrTitle = "Outbound export";
	PlatformData *platform;
	int nResult;
	int len = gStuff->filename[0];
	short hlen;
	OPENFILENAME lpofn;

	gStuff->filename[len+1] = 0; // make sure null terminated

	if (gAliasHandle != NULL)
	{
		/* we've been handed an aliasHandle. Use that instead of querying */
		PIHandle2PString(gAliasHandle, (unsigned char *)&my_lpstrInitialDir);
		hlen = my_lpstrInitialDir[0];

		if (my_lpstrInitialDir [hlen] == '/' || my_lpstrInitialDir [hlen] == '\\')
		{
			_splitpath((const char *)&gStuff->filename[1],
					   NULL, /* drive */
					   NULL, /* dir */
					   (Ptr)gStuff->filename,
					   NULL); /* ext */
			AppendString((unsigned char *)&my_lpstrInitialDir, gStuff->filename, 0, (short)strlen((const char *)gStuff->filename));
			AppendCharToString((unsigned char *)&my_lpstrInitialDir, '.');
			AppendString((unsigned char *)&my_lpstrInitialDir, (const unsigned char *)my_lpstrDefExt, 0, (short)strlen(my_lpstrDefExt));
			hlen = my_lpstrInitialDir[0];
		}
		else my_lpstrInitialDir [hlen+1] = 0;
		
		gStuff->filename[ (gStuff->filename[0] = 0)+1 ] = 0;
		AppendString(gStuff->filename, 
					 (const unsigned char *)&my_lpstrInitialDir, 1,
					 hlen);
		gStuff->filename[ gStuff->filename[0]+1 ] = 0;
		/* makes string pascal and null terminated */

		/* This has to be a Photoshop handle, since it was handed
		   to us by the host, so dispose it (it'll be recreated
		   later.) */
		PIDisposeHandle((Handle)gAliasHandle);
		gAliasHandle = NULL;
	}
	else
		PICopy (my_lpstrInitialDir, &gStuff->filename[1], (short)gStuff->filename[0]);
	
	_splitpath((const char *)&gStuff->filename[1],
			   my_lpstrInitialDir, /* drive */
			   my_dir, /* dir */
			   (Ptr)gStuff->filename,
			   my_extension); /* ext */
	strncat (my_lpstrInitialDir, my_dir, 255); 
	
	platform = (PlatformData *)((ExportRecordPtr) gStuff)->platformData;

	if (!gQueryForParameters)
	{
		strncat(my_lpstrInitialDir, (const char *)gStuff->filename, 255);
		strncat(my_lpstrInitialDir, ".", 2);
		strncat(my_lpstrInitialDir, my_lpstrDefExt, 5);
		strncpy((Ptr)gStuff->filename, my_lpstrInitialDir, 255);
		return TRUE;
	}
	else // gQueryForParameters
	{
		/* initialize OPENFILENAME structure */

		strncpy(my_filename, (const char *)gStuff->filename, 255);
		strncat(my_filename, ".", 2);
		strncat(my_filename, my_lpstrDefExt, 5);

		lpofn.lStructSize = sizeof(lpofn);
		lpofn.hwndOwner = (HWND)platform->hwnd;
		lpofn.hInstance = (HINSTANCE)hDllInstance;
		lpofn.lpstrFilter = my_lpstrFilter;
		lpofn.lpstrCustomFilter = NULL;
		lpofn.nMaxCustFilter = 0;
		lpofn.nFilterIndex = 0;
		lpofn.lpstrFile = my_lpstrFile;
		lpofn.nMaxFile = 255;
		lpofn.lpstrFileTitle = my_lpstrFileTitle;
		lpofn.nMaxFileTitle = 255;
		lpofn.lpstrInitialDir = my_lpstrInitialDir;
		lpofn.lpstrTitle = my_lpstrTitle;
		lpofn.Flags = OFN_LONGNAMES |
					  OFN_HIDEREADONLY | 
					  OFN_PATHMUSTEXIST |
					  OFN_OVERWRITEPROMPT;
		lpofn.nFileOffset = 0;
		lpofn.nFileExtension = 0;
		lpofn.lpstrDefExt = my_lpstrDefExt;
		lpofn.lCustData = 0;
		lpofn.lpfnHook = NULL;
		lpofn.lpTemplateName = NULL;
		lpofn.lStructSize = sizeof(lpofn);

		if (!(nResult = GetSaveFileName(&lpofn)))
			gResult = userCanceledErr;
		else
			gSameNames = (strncmp(my_filename, my_lpstrFileTitle, 255) == noErr);

		return (nResult);
	}
}


/*****************************************************************************/

Boolean CreateExportFile (GPtr globals)
	{
	
	OFSTRUCT ostr;
	OSErr  result;
	HFILE file;
	char *filename;

	/* Delete the file if it exists. */

	gFRefNum = 0;

	filename = (char *) gStuff->filename;
	result = OpenFile(filename,&ostr,(WORD)OF_DELETE);

	if (result == -1)
		{
		if (ostr.nErrCode != ENOENT)
			{
			gResult = openErr;
			return FALSE;
			}
		}


	/* Create a new file */

	file = OpenFile(filename,&ostr,OF_CREATE|OF_WRITE|OF_SHARE_EXCLUSIVE);

	if (file == -1)
		{
		gResult = openErr;
		return FALSE;
		}

	else
		{
		gFRefNum = file;
		return TRUE;
		}

	}
	

/*****************************************************************************/

Boolean CloseExportFile (GPtr globals)
{

	char *filename;
	char my_lpstrInitialDir[256];
	char my_dir[256];
	char my_extension[_MAX_EXT] = ".exp";
	int	len = 0;

	OFSTRUCT	ostr;
	
	if (gFRefNum != 0)
		_lclose(gFRefNum);

	/* Delete the file if we had an error. */

	if (gResult != noErr)
		{
		filename = (char *) gStuff->filename;
		OpenFile(filename,&ostr,(WORD)OF_DELETE);
		}
		
	/* Mark the file as clean. */
	
	gStuff->dirty = FALSE;
	
	/* Create alias for scripting system */

	strncpy(my_lpstrInitialDir, (const char *)gStuff->filename, 255);
	
	len = strlen((const char *)gStuff->filename)+1;

	if (gSameNames)
	{
		_splitpath((const char *)gStuff->filename,
				   my_lpstrInitialDir, /* drive */
				   my_dir, /* dir */
				   (Ptr)gStuff->filename,
				   my_extension); /* ext */
		strncat(my_lpstrInitialDir, my_dir, 255);
		len = strlen(my_lpstrInitialDir)+1;
	}
		
	my_lpstrInitialDir [len] = 0;

	/* If same names, then ignore filename.  Else, leave it in. */

	/* uncomment out to make a pascal string
	gStuff->filename[0] = len;
	strncpy(&gStuff->filename[1], my_lpstrInitialDir, len);
	*/
	strncpy((Ptr)gStuff->filename, my_lpstrInitialDir, len); // comment out for pascal
	gStuff->filename[len+1] = 0; // just in case


	gAliasHandle = PINewHandle(len+1);
	if (gAliasHandle != NULL)
	{
		HLock(gAliasHandle);
		strncpy(*gAliasHandle, (const char *)gStuff->filename, len+1);
		HUnlock(gAliasHandle);
	}

	return TRUE;
	
}

/*****************************************************************************/
/* Dispose alias handle */

void DestroyAliasHandle (GPtr globals)
{
	if (gAliasHandle != NULL)
		PIDisposeHandle((Handle)gAliasHandle);
	gAliasHandle = NULL;
}

/****************************************************************************/
/* Example for ShowAlert() function which takes a string ID as parameter    */
/* and displays a message box                                               */
/****************************************************************************/

short ShowAlert (short stringID)
{
	char szMessage[256];
	char szTitle[128];

	LoadString((HINSTANCE)hDllInstance, stringID, szMessage, sizeof szMessage);
	LoadString((HINSTANCE)hDllInstance, 2, szTitle, sizeof szTitle);
	return  ( MessageBox(NULL, szMessage, szTitle, MB_OK | MB_ICONHAND) );

}

/*****************************************************************************/

/* Dialog box code to ask for export parameters. */

#if defined(__BORLANDC__)
#pragma argsused // lParam in next function not used, so disable warning
#endif

BOOL WINAPI ExportProc(HWND hDlg, UINT wMsg, WPARAM wParam, LPARAM lParam)   // Win32 Change
{
	 static char *filename;
	 int idd;	// Win32 Change

    switch  (wMsg)
	{
		case WM_INITDIALOG:
            // Initialize file name with default values
			filename = (char *)lParam;
            StuffText(hDlg, kDEditText, (unsigned char *)&filename);
            CenterDialog(hDlg);

            return  FALSE;

		case WM_COMMAND   :
	  		idd = COMMANDID (wParam);              // WIN32 Change
            switch  (idd)
			{
				case ok:
					FetchText(hDlg, kDEditText, (unsigned char *)&filename);
					/* drop into cancel */
				case cancel:
					EndDialog(hDlg, idd);
					break;
				default:
                    return FALSE;
            }
            break;

		default:
			return  FALSE;
	}
    return  TRUE;
}

/*****************************************************************************/

/* Initialization and termination code for window's dlls. */

// Win32 Change
#ifdef WIN32

// Every 32-Bit DLL has an entry point DLLInit

BOOL APIENTRY DLLInit(HANDLE hInstance, DWORD fdwReason, LPVOID lpReserved)
{

	if (fdwReason == DLL_PROCESS_ATTACH)
		hDllInstance = hInstance;

	return TRUE;   // Indicate that the DLL was initialized successfully.
}

#else
/* ------------------------------------------------
 *   Code from Borland's window's dll example code.
 * ------------------------------------------------
 */
#if defined(__BORLANDC__)
// Turn off warning: Parameter '' is never used; effects next function only
#pragma argsused
#endif

// Every DLL has an entry point LibMain and an exit point WEP.
int FAR PASCAL LibMain( HANDLE hInstance, WORD wDataSegment,
								   WORD wHeapSize, LPSTR lpszCmdLine )
{
	// Required when using Zortech; causes blink to include startup code
	extern __acrtused_dll;

	// The startup code for the DLL initializes the local heap (if there is one)
	// with a call to LocalInit which locks the data segment.
	if ( wHeapSize != 0 )
		UnlockData( 0 );

	hDllInstance = hInstance;
	return 1;   // Indicate that the DLL was initialized successfully.
}

int FAR PASCAL WEP(int nParam)
{
	switch  (nParam) {
	  case  WEP_SYSTEM_EXIT: // System shutdown in progress
	  case  WEP_FREE_DLL   : // DLL use count is 0
	  default :              // Undefined;  ignore
			return  1;
	}
}
#endif /* WIN32 */
