// ExportDlg.h: interface for the CExportDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXPORTDLG_H__5E95CF7C_8D8F_4471_B52E_19CEEB571446__INCLUDED_)
#define AFX_EXPORTDLG_H__5E95CF7C_8D8F_4471_B52E_19CEEB571446__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"
#include "..\..\..\IMG\PICTURE\picture.hpp"

class CExportDlg : public CDialogClass  
  {	  
  struct DefValues
    {
    char key[MAX_PATH];
    bool PAAExport;
    bool PACExport;
    bool DXCompress;
    bool dynarange;
    TexMipmapFilter MipFilter;
    int limitSize;
    char lastname[MAX_PATH];
    DWORD kcolor;
    bool kenable;
    };
  
  HANDLE hMapFile;
  bool created;
  public:
    void ByHints(const char *fname);
    void SaveDefValues(const char *k,DefValues *df);
    DefValues * Find(const char *k);
    LRESULT OnOK();
    
    DefValues *def;
    DefValues cur;
    int *defcount;
    char *initdir;
    
    bool compress;
    char filename[MAX_PATH];
    DWORD kcolor;
    bool kenable;
    bool paasave;
    const char *key; //fname of source object
    
    CExportDlg();
    virtual LRESULT OnInitDialog();
    virtual ~CExportDlg();
    LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
    LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
    
    
  };

//--------------------------------------------------

#endif // !defined(AFX_EXPORTDLG_H__5E95CF7C_8D8F_4471_B52E_19CEEB571446__INCLUDED_)
