/*******************************************************************/
/*                                                                 */
/*                      ADOBE CONFIDENTIAL                         */
/*                   _ _ _ _ _ _ _ _ _ _ _ _ _                     */
/*                                                                 */
/* Copyright 1986 - 1999 Adobe Systems Incorporated                */
/* All Rights Reserved.                                            */
/*                                                                 */
/* NOTICE:  All information contained herein is, and remains the   */
/* property of Adobe Systems Incorporated and its suppliers, if    */
/* any.  The intellectual and technical concepts contained         */
/* herein are proprietary to Adobe Systems Incorporated and its    */
/* suppliers and may be covered by U.S. and Foreign Patents,       */
/* patents in process, and are protected by trade secret or        */
/* copyright law.  Dissemination of this information or            */
/* reproduction of this material is strictly forbidden unless      */
/* prior written permission is obtained from Adobe Systems         */
/* Incorporated.                                                   */
/*                                                                 */
/*******************************************************************/
//-------------------------------------------------------------------
/*
 * Name:
 *	SPBasic.h
 *
 *
 * Purpose:
 *	SP Basic Suite
 *
 * Distribution:
 *	PUBLIC
 *
 * Version history:
 *	1.0.0 4/1/1995	DL	First version.
 *		Created by Adobe Systems Incorporated.
 */

#ifndef __SPBasic__
#define __SPBasic__


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "SPTypes.h"

#ifdef __cplusplus
extern "C" {
#endif


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kSPBasicSuite				"SP Basic Suite"
#define kSPBasicSuiteVersion		4


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct SPBasicSuite {

	SPAPI SPErr (*AcquireSuite)( char *name, long version, void **suite );
	SPAPI SPErr (*ReleaseSuite)( char *name, long version );
	SPAPI SPBoolean (*IsEqual)( char *token1, char *token2 );
	SPAPI SPErr (*AllocateBlock)( long size, void **block );
	SPAPI SPErr (*FreeBlock)( void *block );
	SPAPI SPErr (*ReallocateBlock)( void *block, long newSize, void **newblock );
	SPAPI SPErr (*Undefined)( void );

} SPBasicSuite;


SPAPI SPErr SPBasicAcquireSuite( char *name, long version, void **suite );
SPAPI SPErr SPBasicReleaseSuite( char *name, long version );
SPAPI SPBoolean SPBasicIsEqual( char *token1, char *token2 );
SPAPI SPErr SPBasicAllocateBlock( long size, void **block );
SPAPI SPErr SPBasicFreeBlock( void *block );
SPAPI SPErr SPBasicReallocateBlock( void *block, long newSize, void **newblock );
SPAPI SPErr SPBasicUndefined( void );


/*******************************************************************************
 **
 **	Errors
 **
 **/

/* Suite errors are in SPSuites.h */
#define kSPBadParameterError		'Parm'	


#ifdef __cplusplus
}
#endif

#endif
