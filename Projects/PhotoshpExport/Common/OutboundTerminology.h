/*******************************************************************/
/*                                                                 */
/*                      ADOBE CONFIDENTIAL                         */
/*                   _ _ _ _ _ _ _ _ _ _ _ _ _                     */
/*                                                                 */
/* Copyright 1990 - 1999 Adobe Systems Incorporated                */
/* All Rights Reserved.                                            */
/*                                                                 */
/* NOTICE:  All information contained herein is, and remains the   */
/* property of Adobe Systems Incorporated and its suppliers, if    */
/* any.  The intellectual and technical concepts contained         */
/* herein are proprietary to Adobe Systems Incorporated and its    */
/* suppliers and may be covered by U.S. and Foreign Patents,       */
/* patents in process, and are protected by trade secret or        */
/* copyright law.  Dissemination of this information or            */
/* reproduction of this material is strictly forbidden unless      */
/* prior written permission is obtained from Adobe Systems         */
/* Incorporated.                                                   */
/*                                                                 */
/*******************************************************************/
//-------------------------------------------------------------------
//-------------------------------------------------------------------------------
//
//	File:
//		OutboundTerminology.h
//
//	Copyright 1990-1992, Thomas Knoll.
//	All Rights Reserved.
//
//	Description:
//		This file contains the resource definitions
//		for the Export module Outbound, a module that
//		creates a file and stores raw pixel data in it.
//
//	Use:
//		This module shows how to export raw data to a file.
//		It uses a simple "FileUtilities" library that comes
//		with the SDK.  You use it via File>>Export>>Outbound.
//
//	Version history:
//		Version 1.0.0.	4/1/1990	Created for Photoshop 2.0.
//			Written by Thomas Knoll.
//
//		Version 1.5.0	5/1/1993	Update for Photoshop 3.0.
//			Cleaned up suites, documentation.
//
//		Version 2.0.0.	6/1/1996	Update for Photoshop 4.0.
//			Added scripting, new parameters.
//
//		Version 2.0.1.	7/11/1997	Updated for Photoshop 4.0.1.
//			CodeWarrior Pro release 1.
//
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//	Definitions -- Scripting keys
//-------------------------------------------------------------------------------

// None

//-------------------------------------------------------------------------------
//	Definitions -- Resources
//-------------------------------------------------------------------------------

#define kPrompt				16100
#define kCreatorAndType		kPrompt+1

//-------------------------------------------------------------------------------
// end OutboundTerminology.h
