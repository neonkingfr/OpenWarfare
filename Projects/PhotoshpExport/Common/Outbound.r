//*******************************************************************/
//*                                                                 */
//*                      ADOBE CONFIDENTIAL                         */
//*                   _ _ _ _ _ _ _ _ _ _ _ _ _                     */
//*                                                                 */
//* Copyright 1990 - 1999 Adobe Systems Incorporated                */
//* All Rights Reserved.                                            */
//*                                                                 */
//* NOTICE:  All information contained herein is, and remains the   */
//* property of Adobe Systems Incorporated and its suppliers, if    */
//* any.  The intellectual and technical concepts contained         */
//* herein are proprietary to Adobe Systems Incorporated and its    */
//* suppliers and may be covered by U.S. and Foreign Patents,       */
//* patents in process, and are protected by trade secret or        */
//* copyright law.  Dissemination of this information or            */
//* reproduction of this material is strictly forbidden unless      */
//* prior written permission is obtained from Adobe Systems         */
//* Incorporated.                                                   */
//*                                                                 */
/*******************************************************************/
//-------------------------------------------------------------------
//-------------------------------------------------------------------------------
//
//	File:
//		Outbound.r
//
//	Copyright 1990-1992, Thomas Knoll.
//	All Rights Reserved.
//
//	Description:
//		This file contains the resource information
//		for the Export module Outbound, a module that
//		creates a file and stores raw pixel data in it.
//
//	Use:
//		This module shows how to export raw data to a file.
//		It uses a simple "FileUtilities" library that comes
//		with the SDK.  You use it via File>>Export>>Outbound.
//
//	Version history:
//		Version 1.0.0.	4/1/1990	Created for Photoshop 2.0.
//			Written by Thomas Knoll.
//
//		Version 1.5.0	5/1/1993	Update for Photoshop 3.0.
//			Cleaned up suites, documentation.
//
//		Version 2.0.0.	6/1/1996	Update for Photoshop 4.0.
//			Added scripting, new parameters.
//
//		Version 2.0.1.	7/11/1997	Updated for Photoshop 4.0.1.
//			CodeWarrior Pro release 1.
//
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//	Definitions -- Required by include files.
//-------------------------------------------------------------------------------

// The About box and resources are created in DialogUtilities.r.
// You can easily override them, if you like.

#define plugInName			"PAA/PAC files"
#define plugInCopyrightYear	"2003"
#define plugInDescription \
	"Photoshop export plugin into Flashpoint PAA/PAC files."

//-------------------------------------------------------------------------------
//	Definitions -- Required by other resources in this rez file.
//-------------------------------------------------------------------------------

// Dictionary (aete) resources:

#define vendorName				"AdobeSDK"
#define plugInAETEComment		"history export plug-in"

#define plugInSuiteID			'sdK5'
#define plugInClassID			'pacF'
#define plugInEventID			typeNull // must be this

//-------------------------------------------------------------------------------
//	Set up included files for Macintosh and Windows.
//-------------------------------------------------------------------------------

#include "../PIDefines.h"

#ifdef __PIMac__
	#include "Types.r"
	#include "SysTypes.r"
	#include "PIGeneral.r"
	#include "PIUtilities.r"
	#include "DialogUtilities.r"
#elif defined(__PIWin__)
	#include "../PIGeneral.h"
	#include "../win/PIUtilities.r"
	#include "../WinDialogUtils.h"
#endif

#include "../PIActions.h"

#include "OutboundTerminology.h"	// Terminology specific to this plug-in.

//-------------------------------------------------------------------------------
//	PiPL resource
//-------------------------------------------------------------------------------

resource 'PiPL' (ResourceID, plugInName " PiPL", purgeable)
{
	{
		Kind { Export },
		Name { plugInName "..." },
		Version { (latestExportVersion << 16) | latestExportSubVersion },

		#ifdef __PIMac__
			Code68K { Export, $$ID },
			CodePowerPC { 0, 0, "" },

		#elif defined(__PIWin__)
			CodeWin32X86 { "ENTRYPOINT" },
		#endif

		HasTerminology
		{
			plugInClassID,		// class ID
			plugInEventID,		// event ID
			ResourceID,			// aete ID
			"" 					// uniqueString
		},
		
		SupportedModes
		{
			noBitmap, noGrayScale,
			noIndexedColor, doesSupportRGBColor,
			noCMYKColor, noHSLColor,
			noHSBColor, noMultichannel,
			noDuotone, noLABColor
		},
			
		EnableInfo
		{
			"in ( PSHOP_ImageMode , RGBMode )"
		}

	}
};

//-------------------------------------------------------------------------------
//	PiMI resource (Photoshop 2.5 and other older hosts)
//-------------------------------------------------------------------------------

resource 'PiMI' (ResourceID, plugInName " PiMI", purgeable)
{ 
	  latestExportVersion,
	  latestExportSubVersion,
	  0,
	  supportsRGBColor,
	  '    ', /* No required host */
	  {},
};

//-------------------------------------------------------------------------------
//	Dictionary (scripting) resource
//-------------------------------------------------------------------------------

resource 'aete' (ResourceID, plugInName " dictionary", purgeable)
{
	1, 0, english, roman,									/* aete version and language specifiers */
	{
		vendorName,											/* vendor suite name */
		"Adobe - Flashpoint plug-ins",  							/* optional description */
		plugInSuiteID,										/* suite ID */
		1,													/* suite code, must be 1 */
		1,													/* suite level, must be 1 */
		{},													/* structure for filters */
		{													/* non-filter plug-in class here */
			vendorName " " plugInName,						/* unique class name */
			plugInClassID,										/* class ID, must be unique or Suite ID */
			plugInAETEComment,								/* optional description */
			{												/* define inheritance */
				"<Inheritance>",							/* must be exactly this */
				keyInherits,								/* must be keyInherits */
				classExport,								/* parent: Format, Import, Export */
				"parent class export",						/* optional description */
				flagsSingleProperty,						/* if properties, list below */
				
				"in",										/* our path */
				keyIn,										/* common key */
				typePlatformFilePath,						/* correct path for platform */
				"file path",								/* optional description */
				flagsSingleProperty
				
				/* no more properties */
			},
			{}, /* elements (not supported) */
			/* class descriptions */
		},
		{}, /* comparison ops (not supported) */
		{}	/* any enumerations */
	}
};

//-------------------------------------------------------------------------------
//	Resource text entries
//
//	Entering these as separate resources because then they
//	transfer directly over to windows via CNVTPIPL.
//
//	If I use a Macintosh 'STR#' resource, I could put all these
//	strings into a single resource, but there is no
//	parallel on Windows.  'STR ' resources, which hold
//	one string per ID, exist on Windows and Macintosh.
//-------------------------------------------------------------------------------

// Prompt string:
resource StringResource (kPrompt, plugInName " Prompt", purgeable)
{
	"PAA/PAC export to file:"
};

// Creator and type:
resource StringResource (kCreatorAndType, plugInName " CreatorAndType", purgeable)
{
	"Bredy"		// creator
	"Export"		// type

};

//-------------------------------------------------------------------------------

// end Outbound.r
