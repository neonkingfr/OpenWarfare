/*******************************************************************/
/*                                                                 */
/*                      ADOBE CONFIDENTIAL                         */
/*                   _ _ _ _ _ _ _ _ _ _ _ _ _                     */
/*                                                                 */
/* Copyright 1990 - 1999 Adobe Systems Incorporated                */
/* All Rights Reserved.                                            */
/*                                                                 */
/* NOTICE:  All information contained herein is, and remains the   */
/* property of Adobe Systems Incorporated and its suppliers, if    */
/* any.  The intellectual and technical concepts contained         */
/* herein are proprietary to Adobe Systems Incorporated and its    */
/* suppliers and may be covered by U.S. and Foreign Patents,       */
/* patents in process, and are protected by trade secret or        */
/* copyright law.  Dissemination of this information or            */
/* reproduction of this material is strictly forbidden unless      */
/* prior written permission is obtained from Adobe Systems         */
/* Incorporated.                                                   */
/*                                                                 */
/*******************************************************************/
//-------------------------------------------------------------------
//-------------------------------------------------------------------------------
//
//	File:
//		Outbound.h
//
//	Copyright 1990-1992, Thomas Knoll.
//	All Rights Reserved.
//
//	Description:
//		This file contains the headers and prototypes
//		for the Export module Outbound, a module that
//		creates a file and stores raw pixel data in it.
//
//	Use:
//		This module shows how to export raw data to a file.
//		It uses a simple "FileUtilities" library that comes
//		with the SDK.  You use it via File>>Export>>Outbound.
//
//	Version history:
//		Version 1.0.0.	4/1/1990	Created for Photoshop 2.0.
//			Written by Thomas Knoll.
//
//		Version 1.5.0	5/1/1993	Update for Photoshop 3.0.
//			Cleaned up suites, documentation.
//
//		Version 2.0.0.	6/1/1996	Update for Photoshop 4.0.
//			Added scripting, new parameters.
//
//		Version 2.0.1.	7/11/1997	Updated for Photoshop 4.0.1.
//			CodeWarrior Pro release 1.
//
//-------------------------------------------------------------------------------

#ifndef __Outbound_H__				// Has this been defined yet?
#define __Outbound_H__				// Only include once by predefining it.

#include "../PIExport.h"				// Export Photoshop header file.
#include "../PIUtilities.h"			// SDK Utility library.
#include "OutboundTerminology.h"	// Terminology specific to this plug-in.

//-------------------------------------------------------------------------------
//	Globals -- structures
//-------------------------------------------------------------------------------

// Windows handle for our plug-in (seen as a dynamically linked library):
extern HANDLE hDllInstance;

// The region of the image being exported (an area and a range of planes):

typedef struct ExportRegion
{
	Rect rect;
	int16 loPlane;
	int16 hiPlane;

} ExportRegion;

typedef struct Globals
{ // This is our structure that we use to pass globals between routines:

	short					*result;				// Must always be first in Globals.
	ExportRecord			*exportParamBlock;		// Must always be second in Globals.

	Boolean					queryForParameters;
	Boolean					sameNames;
	FileHandle				fRefNum;

	// AliasHandle on Mac, Handle on Windows:
	PIPlatformFileHandle	aliasHandle;
		
} Globals, *GPtr, **GHdl;
	
// The routines that are dispatched to from the jump list should all be
// defined as
//		void RoutineName (GPtr globals);
// And this typedef will be used as the type to create a jump list:
typedef void (* FProc)(GPtr globals);

//-------------------------------------------------------------------------------
//	Globals -- definitions and macros
//-------------------------------------------------------------------------------

#define gResult 				(*(globals->result))
#define gStuff  				(globals->exportParamBlock)

#define gQueryForParameters		(globals->queryForParameters)
#define gSameNames				(globals->sameNames)
#define gAliasHandle			(globals->aliasHandle)
#define gFRefNum				(globals->fRefNum)

//-------------------------------------------------------------------------------
//	Prototypes
//-------------------------------------------------------------------------------

Boolean ReadScriptParams (GPtr globals);	// Read any scripting params.
OSErr WriteScriptParams (GPtr globals);		// Write any scripting params.

void DoAbout (AboutRecordPtr about); 	    // Pop about box.
Boolean DoUI (GPtr globals);				// Show the UI.

Boolean CreateExportFile (GPtr globals);
Boolean WriteExportFile (GPtr globals);
Boolean CloseExportFile (GPtr globals);

void MarkExportFinished (ExportRecord *stuff);

OSErr FetchData (ExportRecord *stuff, 		// in/out
				 ExportRegion *region, 		// in
				 void **data, 				// out
				 int32 *rowBytes); 			// out

//-------------------------------------------------------------------------------

#endif // __Outbound_H__
