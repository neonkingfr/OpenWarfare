#pragma once


class ProxyMerge
{
  LODObject _proxyObject;
  LODObject * _curObject;
  RString _projectRoot;
  RString _mergedName;
  Matrix4 _firstProxyTransform;
  int _firstProxyFace;

  int _nMergedProxies;
public:
  ProxyMerge(const char *projectRoot);
  ~ProxyMerge(void);

  bool LoadProxyObject(const char *name);
  void SetMergedName(const char *name) {_mergedName=name;}
  bool MergeProxy(const char *proxyName);
  void CombineParts();
  bool SaveProxyObject(const char *name);
  bool SaveMergedObject();

};
