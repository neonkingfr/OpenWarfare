#include "common.hpp"
#include "ObjToolConvex.h"
#include "Edges.h"
#include "ObjToolTopology.h"
#include <queue>
#include <es/algorithms/qsort.hpp>
#include "LodObject.h"

static const float fpi = 3.1415926535f;
static const float outsideAccuracy = 0;       
static const float insideAccuracy = 1e-3f;
static const float planeSimilarity = 1e-6f;
static const float splitToConvexAccuracy = 1;

namespace ObjektivLib  {

    struct SimpleFace{
        int i1,i2,i3;
        ClassIsSimple(ObjektivLib::SimpleFace);

        SimpleFace(int i1, int i2, int i3): i1(i1),i2(i2),i3(i3) {}
    };

    void ObjToolConvex::PrepareImportantPoints(const Array<int> &selection, int regions, AutoArray<int> &result) const {

        ///retrieve selection center
        Vector3 center = Point(selection[0]);
        for (int i = 1; i < selection.Size(); i++) center = center + Point(selection[i]);
        center = center / (Coord)selection.Size();

        int zregions = regions / 2;

        ///Prepare important points region
        int **importantPts = (int **)alloca(sizeof(int *) * zregions);
        for (int i = 0; i <zregions; i++) {
            importantPts[i] = (int *)alloca(sizeof(int) * regions);
            for (int j = 0; j <regions; j++) importantPts[i][j] = -1;
        }

        for (int i = 0; i < selection.Size(); i++) {

            Vector3 pos = Point(selection[i]);
            Vector3 rpos = pos - center;
            float azimut = atan2(rpos.Z(),rpos.X());
            float zenit = atan2(rpos.Y(), rpos.SizeXZ());
            int zreg = toInt((zenit + fpi/2.0f) * zregions / fpi);
            if (zreg < 0) zreg = 0;
            if (zreg >= zregions) zreg = zregions - 1;
            int areg = toInt((azimut + fpi) * regions / (2*fpi));
            if (areg < 0) areg = 0;
            if (areg >= regions) areg = regions - 1;

            int &curPt = importantPts[zreg][areg];
            if (curPt == -1) curPt = selection[i];
            else {
                float d = Point(curPt).Distance2(center);
                float nd = rpos.SquareSize();
                if (nd > d)
                    curPt = selection[i];
            }
        }

        result.Clear();
        result.Reserve(regions * zregions);
        for (int i = 0; i < zregions; i++)
            for (int j = 0; j < regions; j++) {
                int pt = importantPts[i][j];
                if (pt != -1) result.Add(pt);
            }

            //Too less points has been selected. 
            //Probably model has small count of vertices
            //we will use all points as important
            if (result.Size() < 4) result = AutoArray<int>(selection);

    }

    float ObjToolConvex::PlaneNfo::Distance(const Vector3 &pt) const {
        //test location of the point relative to plane
        float ptd = planeNorm.DotProduct(pt);
        //calculate difference
        float dff = position + ptd ;
        //when points is inside of volume
        return dff;
    }

    int ObjToolConvex::PlaneNfo::Side(const Vector3 &pt) const {
        //test location of the point relative to plane
        float ptd = planeNorm.DotProduct(pt);
        //calculate difference
        float dff = position + ptd ;
        //when points is inside of volume
        if (dff < -insideAccuracy) 
            return -1;
        //point is outside of volume
        if (dff > outsideAccuracy)
            return 1;

        //a shortcut
        return 0;

    }

    bool ObjToolConvex::PlaneNfo::Test(const Vector3 &pt) const {
        return Side(pt) <= 0;
    }


    ObjToolConvex::PlaneNfo::PlaneNfo(const Vector3 &p1, 
        const Vector3 &p2, 
        const Vector3 &p0) {

            /*            Vector3 p1=obj.Point(points[0]);
            Vector3 p0=obj.Point(points[2]);
            Vector3 p2=obj.Point(points[1]);*/
            planeNorm = (p1-p0).CrossProduct(p2-p0).Normalized();
            if (!planeNorm.IsFinite()) planeNorm = Vector3(0,0,0);
            float d = p1.DotProduct(planeNorm);
            position = -d;

    }

    ObjToolConvex::PlaneNfo::PlaneNfo(const FaceT &fc) {
        planeNorm = -fc.CalculateNormal(); //negated, because faces has reversed normal
        if (!planeNorm.IsFinite()) planeNorm = Vector3(0,0,0);
        float d = fc.GetPointVector<Vector3>(0).DotProduct(planeNorm);
        position = -d;
    }

    int ObjToolConvex::FindPlane(const Array<int> &points, int pt1, int pt2, PlaneNfo &result) const {

        int pt3 = -1;
        int i;
        for (i = 0; i < points.Size(); i++ ) if (points[i] != pt1 && points[i] != pt2) {
            pt3 = points[i];
            break;
        }

        if (pt3 == -1) 
            return -1;

        PlaneNfo tmp(Point(pt1),Point(pt2),Point(pt3));

        for (i++; i <points.Size(); i++) if (points[i] != pt1 && points[i] != pt2)  {

            if (tmp.planeNorm.SquareSize()<0.5f || !tmp.Test(Point(points[i])))  {
                pt3 = points[i];
                PlaneNfo tmp2 = PlaneNfo(Point(pt1),Point(pt2),Point(pt3));
                /*if (tmp2.planeNorm.SquareSize()>0.5f) */tmp = tmp2;
            }

        }

        for (i = 0 ; i <points.Size(); i++) {

            if (!tmp.Test(Point(points[i]))) 
                return -1;

        }

        result = tmp;
        return pt3;
    }


    bool ObjToolConvex::IsPointInside(const PlaneArr &planes, const Vector3 &vx) const {

        for (int i = 0; i < planes.Size(); i++)
            if (!planes[i].Test(vx))
                return false;
        return true;

    }


    void ObjToolConvex::MakePlaneList(PlaneList &planes, Array<int> &points, AutoArray<SimpleFace> *faces) const {    

        ProgressBar<int> pb(points.Size());
        CEdges edges(NPoints());
        planes.Clear();
        if (faces) faces->Clear();

        for (int i = 0; i < points.Size(); i++) {
            pb.AdvanceNext(1);
            if (pb.StopSignaled()) return;
            for (int j = 0; j < points.Size(); j++)
                if (i != j 
                        && Point(points[i]).Distance2(Point(points[j]))>insideAccuracy
                        && !edges.IsEdgeInDirection(points[i],points[j])) {
                   PlaneNfo plane;
                   int k  = FindPlane(points,points[i],points[j],plane);
                    //k equal -1 means, that point cannot be find
                   if (k != -1 && !edges.IsEdgeInDirection(points[j],k) &&
                       !edges.IsEdgeInDirection(k,points[i]))
                        {
                            if (faces) faces->Add(SimpleFace(points[i],points[j],k));
                            edges.SetEdge(points[i],points[j]);
                            edges.SetEdge(points[j],k);
                            edges.SetEdge(k,points[i]);
                            planes.Add(plane);
                        }
                }
        }

    }

#if 0
    void ObjToolConvex::MakePlaneList(PlaneList &planes, CEdges &pointsOnPlanes, Array<int> &faces) const {    

        ProgressBar<int> pb(points.Size());

        pointsOnPlanes.Clear();
        planes.Clear();

        for (int i = 0; i < points.Size(); i++) {
            pb.AdvanceNext(1);
            if (pb.StopSignaled()) return;
            for (int j = 0; j < points.Size(); j++)
                if (i != j && Point(points[i]).Distance2(Point(points[j]))>insideAccuracy) {
                    int nx;
                    //both points should not be located on already found plane
                    //process all planes connected to first point and check,that second points 
                    //has not connected the same plane
                    //we should reach -1 here
                    /*if (nx == -1)*/ {
                        //okay search third point
                        PlaneNfo plane;
                        //find plane
                        int k  = FindPlane(points,points[i],points[j],plane);
                        //k equal -1 means, that point cannot be find
                        if (k != -1) {
                            for (int epos = 0; (nx = pointsOnPlanes.GetEdge(points[i],epos)) != -1; epos++) 
                                if (pointsOnPlanes.IsEdgeInDirection(points[j],nx)
                                    && pointsOnPlanes.IsEdgeInDirection(k,nx))
                                    break;
                            if (nx == -1) {

                                int cnt = 0;
                                //for each two points, there is always no point or exactly one point.
                                //create new mark
                                int mark = planes.Size();
                                //add plane to list
                                planes.Add(plane);
                                //search all points and locate points that lyies on the plane
                                for (int z = 0; z < points.Size();z++) {
                                    int pt = points[z];
                                    if (z == i || z == j || pt == k || plane.Side(Point(points[z])) == 0) {
                                        //if points is lying on the plane, connect it with the plane
                                        pointsOnPlanes.SetEdge(points[z], mark);
                                        cnt ++;
                                    }

                                }
                                Assert(cnt >=3);
                            }
                        }
                    }
                }
        }
    }


#endif
    void ObjToolConvex::CreatePolygons(PlaneList &planes, CEdges &pointsOnPlanes, 
                                    Array<int> &points, Selection &toDel) {

        AutoArray<int> tmplist;
        CEdges allocEdges(NPoints());
        for (int trycycle = 0; trycycle>=0; trycycle--) {
            for (int i = 0; i < planes.Size(); i++) {

                tmplist.Clear();
                for (int j = 0; j < points.Size(); j++) {
                    if (pointsOnPlanes.IsEdgeInDirection(points[j],i) &&
                        pointsOnPlanes.GetEdge(points[j],1) != -1){
                            tmplist.Add(points[j]);
                    }
                }
                
                if (tmplist.Size()<3)
                    continue;
                ObjToolTopology &topo = GetTool<ObjToolTopology>();
                topo.SortAroundPoint(tmplist,-planes[i].planeNorm);
                bool rep;
                do {
                    rep = false;
                    for (int t = tmplist.Size() - 2, u = tmplist.Size() - 1, v = 0; v < tmplist.Size();t = u, u = v, v++) {

                        Vector3 lastr = Point(tmplist[u]) - Point(tmplist[t]);
                        Vector3 nxt = Point(tmplist[v]) - Point(tmplist[u]);
                        Vector3 xprod = nxt.CrossProduct(lastr);
                        float dotprod = xprod.DotProduct(planes[i].planeNorm);
                        if (dotprod <= 0 || allocEdges.IsEdgeInDirection(tmplist[t],tmplist[u])) {
                            pointsOnPlanes.RemoveEdge(tmplist[u], i);
                            tmplist.Delete(u);                        
                            if (t > u) t--;
                            if (v > u) v--;
                            u = t;
                            
                            
                            rep = tmplist.Size()>2;
                        }
                        
                    }
                } while (rep);
                    
                if (trycycle == 0) {
                    for (int u = tmplist.Size() - 1, v = 0; v < tmplist.Size();u = v, v++) {
                        allocEdges.SetEdge(tmplist[u],tmplist[v]);
                    }

                    if (tmplist.Size() > 2) {
                        topo.TessellatePolygon(tmplist.Size(), tmplist.Data());
                    }
                    for (int i = 0; i < tmplist.Size(); i++)
                        toDel.PointSelect(tmplist[i],false);
                }
            }
        }
    }


    void ObjToolConvex::CreateMesh(const Array<SimpleFace> &faces, Selection &todel) {
        
        CEdges egs(NPoints());
        
        for (int i = 0; i < faces.Size(); i++) {
            FaceT fc;
            fc.CreateIn(this);
            fc.SetN(3);
            fc.SetPoint(2,faces[i].i1);
            fc.SetPoint(1,faces[i].i2);
            fc.SetPoint(0,faces[i].i3);
            egs.SetEdge(faces[i].i1,faces[i].i2);
            egs.SetEdge(faces[i].i2,faces[i].i3);
            egs.SetEdge(faces[i].i3,faces[i].i1);
            todel.PointSelect(faces[i].i1,false);
            todel.PointSelect(faces[i].i2,false);
            todel.PointSelect(faces[i].i3,false);
        }

        for (int i = 0; i < NPoints(); i++) {
            for (int k,j = 0; (k = egs.GetEdge(i,j))!=-1;j++) {
                if (!egs.IsEdgeInDirection(k,i)) {
                    CloseGeometry(egs, i, k, todel);
                }
            }
        }

    }
    void ObjToolConvex::CloseGeometry(CEdges &egs, int p1, int p2, Selection &todel) {
        AutoArray<int> vertices;
        vertices.Add(p1);
        vertices.Add(p2);
        egs.SetEdge(p1,p2);
        while(p2 != p1) {
            int z;
            for (int k,j = 0; (k = egs.GetEdge(p2,j))!=-1;j++) {
                z = k;
            }
            vertices.Add(z);
            egs.SetEdge(p2,z);
            p2 = z;
        }
        ObjToolTopology &topo = GetTool<ObjToolTopology>();
        topo.TessellatePolygon(vertices.Size(),vertices.Data());
    }

    void ObjToolConvex::MakeConvex(const Selection &sel, bool removeOriginal, int regions, bool stage1only)
    {    
        ProgressBar<int> pb(100);
        //retrieve selected points
        AutoArray<int> points = sel.GetSelectedPoints();
        //exit on empty selection
        if (points.Size() == 0) return;

        //prepare important points
        AutoArray<int> impPtsArr;
        //calculate important points
        PrepareImportantPoints(points, regions,impPtsArr);

        //prepare selection with important points
        Selection impPtsSel(this);

        bool rebuild = false;
        //allocate volume
        PlaneList volume;
        //pointsOnPlanes contains list of planes for each point
        CEdges pointsOnPlanes(NPoints());

        //select points
        impPtsSel.SelectPoints(impPtsArr);

        pb.SetPos(0);
        pb.SetNextPos(regions/4);
        AutoArray<SimpleFace> simpfaces;
        //calculate volume from important points
        MakePlaneList(volume,impPtsArr,&simpfaces);
        //process all points, and points that lay outside the volume include into important points collection
        int limit = volume.Size() * 2;
        if (volume.Size()>=4)
        {
            ProgressBar<int> pb(points.Size());
            for (int i = 0; i < points.Size(); i++) {
                pb.AdvanceNext(1);
                if (!impPtsSel.PointSelected(points[i]) &&  //not included yet 
                    !IsPointInside(volume, Point(points[i]))) //and not inside
                {
                    impPtsArr.Add(points[i]);                  
                    rebuild = true;

                }

            }
        } else {
            rebuild = true;
            impPtsArr = points;
        }

        if (pb.StopSignaled()) return;
        if (!stage1only && impPtsArr.Size() > 750 && regions < 32) 
        {
            pb.SetNextPos(100);
            MakeConvex(sel,removeOriginal, regions + 4);
            return;
        }

        pb.SetNextPos(100);
        //calculate final volume, if this is necesery
        if (!stage1only && rebuild) 
            MakePlaneList(volume, impPtsArr, &simpfaces);


        Selection todel = sel;        
/*        Selection polygon(this);
        Selection polygon2(this);                                   */

        pb.SetNextPos(100);

        if (pb.StopSignaled()) return;

        int n = NFaces();

        CreatePolygons(volume, pointsOnPlanes, impPtsArr, todel);
#if 0
        AutoArray<int> polygons;

        CreatePolygons(volume, pointsOnPlanes, impPtsArr, polygons);


        for (int i = 0; i < volume.Size(); i++) {

            ObjToolTopology &topo = GetTool<ObjToolTopology>();
            polygon.Clear();
            int cnt = 0;
            for (int j = 0; j < impPtsArr.Size(); j++) {
                int pt = impPtsArr[j];
                if (pointsOnPlanes.IsEdgeInDirection(pt,i) && 
                    pointsOnPlanes.GetEdge(pt,1) != -1)  {
                        polygon.PointSelect(pt);
                        todel.PointSelect(pt, false);
                        cnt++;
                }

            }



            /*            Selection tosave(this);
            PlaneNfo plane = volume[i];
            char buff[100];
            sprintf(buff,"%g %g %g",plane.planeNorm[0],plane.planeNorm[1],plane.planeNorm[2]);*/
            int fc = topo.NFaces();
            /*            if (polygon.NPoints() < 3)
            topo.TessellateFromPointSelection(polygon2);
            else*/            
            if (cnt >=3 )
                topo.TessellateFromPointSelection(polygon);
            while (fc < topo.NFaces()) {
                FaceT ff(topo,fc);
                //                tosave.FaceSelect(fc);
                Vector3 norm = ff.CalculateRawNormal();
                if (norm.DotProduct(volume[i].planeNorm) > 0)
                    ff.Reverse();
                
/*                if (CheckFaceSharePts(ff,pointsOnPlanes,i)) {
                    todel.FaceSelect(ff.GetFaceIndex());
                    for (int i = 0; i < ff.N(); i++)
                        pointsOnPlane
                }*/
                
                
                fc++;
            }

            char buff[200];
            sprintf(buff,"-sel-%05d",i);
            polygon.Validate();
            polygon.SelectFacesFromPoints();
            SaveNamedSel(buff,&polygon);

            //            tosave.SelectPointsFromFaces();
            //            SaveNamedSel(buff,&tosave);
        }
#endif

        int maxcycles = 1000;
        while (RunEdgeTest(n,NFaces(),todel) && maxcycles) {
            if (pb.StopSignaled()) break;
            maxcycles--;
        }
        if (maxcycles==0)
            Log("Error: Failed to finish geometry, to many errors on it!");

        //remove original mesh, if this option is enabled
        if (removeOriginal)
            SelectionDelete(&todel); 



    }

    int ObjToolConvex::CalculateFaceCull(int face, PlaneArr &volume) const {

        int count = 0;
        PlaneNfo plane(FaceT(*this,face));
        for (int i = 0; i < NPoints(); i++)
            if (IsPointInside(volume,Point(i)) && plane.Test(Point(i)))
                count++;        
        return count;
    }

    int ObjToolConvex::CalculatePlaneVolumePenalty(const Selection &sel, 
        const PlaneNfo &plane, Selection &newsel) const 
    {
        int penalty = 0;
        for (int i = 0; i < NFaces(); i++) {
            if (sel.FaceSelected(i)) 
            {            
                FaceT fc(this,i);
                int j = 0;
                while (j < fc.N() && plane.Test(fc.GetPointVector<Vector3>(j))) 
                    j++;
                if (j != fc.N()) {
                    penalty++;
                } else {
                    newsel.FaceSelect(i);
                    for (j = 0; j < fc.N(); j++) newsel.PointSelect(fc.GetPoint(j));
                }
            }
        }
        return penalty;
    }

    struct QueueItem {

        int face;
        int penalty;
        int srcface;
        float score;

        QueueItem(int face, int penalty, int srcface, float score):
        face(face),penalty(penalty),srcface(srcface),score(score) {}

    };

    struct CompareQueueItem {
    public:
        bool operator()(const QueueItem &left, const QueueItem &right) const {
            return left.penalty>right.penalty ||
                (left.penalty == right.penalty && left.score>right.score);
        }
    };

    bool ObjToolConvex::SplitToConvex_stage2(ObjToolConvex &trg, int regions, bool stage1only)  {

        /*            GetTool<ObjToolTopology>().CloseTopology();
        Selection all(this);
        all.SelectAll();                
        Triangulate(tmConvex,&all);     */   
opakuj:
        if (NFaces() > 0) {
            Selection excludedFaces(this);

            CEdges graph(NFaces()),nocross;
            BuildFaceGraph(graph,nocross);

            PlaneList volume;
            Selection vertices(this);

            std::priority_queue<QueueItem,std::vector<QueueItem>, CompareQueueItem  >fcqueue;
            fcqueue.push(QueueItem(0,0,-1,0));
            excludedFaces.FaceSelect(0,true);
            vertices.FaceSelect(0,true);
            int generation = 0;
runAgain:
            while (!fcqueue.empty()) {

                QueueItem item = fcqueue.top();
                fcqueue.pop();

                FaceT fcnx(this,item.face);
                PlaneNfo plane(fcnx);

                if (item.srcface>=0 && !vertices.FaceSelected(item.srcface)) 
                    continue;

                Selection vertres(this);
                int penalty = CalculatePlaneVolumePenalty(vertices,plane,vertres);
                if (penalty > item.penalty) {
                    if (penalty != vertices.NFaces())
                        fcqueue.push(QueueItem(item.face,penalty,item.srcface,item.score));
                    continue;
                }


                vertices = vertres;

                LogF("Vertices: %d",vertices.NPoints());

                int j;
                for (j = 0; j < fcnx.N(); j++) {                    
                    if (!IsPointInside(volume,fcnx.GetPointVector<Vector3>(j)))
                        break;
                }
                if (j != fcnx.N())
                    continue;

                for (j = 0; j < fcnx.N(); j++) {                    
                    vertices.PointSelect(fcnx.GetPoint(j));
                }
                vertices.FaceSelect(item.face);


                volume.Add(plane);
                for (int i = 0,nx; (nx = graph.GetEdge(item.face,i)) != -1; i++) 
                    if (!excludedFaces.FaceSelected(nx))
                    {
                        float maxdist = 0;
                        FaceT fc(this,nx);
                        int fakeface = -1;
                        bool notuseful = false;
                        for (int i = 0; i < fc.N(); i++) {
                            float ds = plane.Distance(fc.GetPointVector<Vector3>(i));
                            if (ds > outsideAccuracy) {
                                notuseful = true;
                                break;                                

                            }
                            ds = -ds;
                            if (ds > maxdist) maxdist = ds;
                        }
                        if (notuseful) continue;

                        FaceT fcnx(this,fakeface != -1?fakeface:i);
                        PlaneNfo plane(fcnx);

                        int penalty = CalculatePlaneVolumePenalty(vertices,plane,vertres);
                        fcqueue.push(QueueItem(nx,penalty,item.face,maxdist));
                        excludedFaces.FaceSelect(nx,true);
                    }
            }

            bool again = false; {
                for (int i = 0; i < NFaces(); i++) if (!excludedFaces.FaceSelected(i)) {
                    FaceT fc(this,i);
                    bool notuseful = false;
                    for (int j = 0; j < fc.N(); j++) {
                        bool ds = !IsPointInside(volume,fc.GetPointVector<Vector3>(j));
                        if (ds) {
                            notuseful = true;
                            break;
                        }
                    }                    
                    if (!notuseful) {
                        fcqueue.push(QueueItem(i,0,-1,0));
                        excludedFaces.FaceSelect(i,true);
                        goto runAgain;
                    }
                }
            }

            if (volume.Size() < 2 || vertices.NPoints() < 4) {
                DeleteFace(0);
                goto opakuj;
            }

            vertices.SelectPointsFromFaces();            

            if (vertices.NFaces() < 1) {
                DeleteFace(0);
                goto opakuj;
            }


            SelectionSplit(&vertices);
            AutoArray<int> vxlist = vertices.GetSelectedPoints();
            trg.ReservePoints(vxlist.Size());
            for (int i = 0; i < vxlist.Size(); i++) {
                trg.Point(i) = Point(vxlist[i]);
            }
            Selection wholeTrg(&trg);
            wholeTrg.SelectAll();
            trg.MakeConvex(wholeTrg,true,regions,stage1only);

            /*            LODObject test;
            test.AddLevel(trg,1);
            test.AddLevel(*this,2);    */
            SelectionDelete(&vertices);
            /*            test.AddLevel(*this,3);
            test.DeleteLevel(0);
            test.Save(Pathname("T:\\result.p3d"),9999,false,0,0);
            */
            return true;
        }
        else
            return false;
    }



    void ObjToolConvex::SplitToConvexByComponents(Selection sel, const SplitToConvexProp &prop) {
        /*    const char *tmpname = "!SplitToConvexByComponents - qopweiuqpwoeipo owiednoqwiehjqowdnwqqwe";
        SaveNamedSel(tmpname,&sel);
        const Selection *tmpsel = GetNamedSel(tmpname);
        ProgressBar<int> pb(tmpsel->NFaces());    
        int nfaces = 
        for (int i = 0; i < NFaces(); i++) if (tmpsel->FaceSelected(i)) {        
        Selection wrk(this);
        wrk.FaceSelect(i);
        wrk.SelectPointsFromFaces();
        wrk.SelectConnectedFaces();
        pb.AdvanceNext(wrk.NFaces());
        SplitToConvex2(wrk,removeOriginal,regions, stage1only);
        if (!removeOriginal) {
        *const_cast<Selection *>(tmpsel) -= wrk;
        }
        tmpsel = GetNamedSel(tmpname);
        i = -1;
        }
        DeleteNamedSel(tmpname);*/

        SplitToConvex(sel,prop);

    }

    class ConvexIncidence {
        bool **incd;
        int size;
        AutoArray<int> selfacesmap;   
        float accuracy;

    public:
        ConvexIncidence(const ObjectData &obj, const Array<int> selected,float accuracy)
            :accuracy(accuracy) {
            size = selected.Size();
            incd = new bool *[size];
            for (int i = 0; i < size; i++) try {
                incd[i] = new bool[size];
            } catch (...) {
                while (i>0) delete [] incd[--i];
                delete [] incd;
                throw;
            }

            for (int i = 0; i < selected.Size(); i++)
                for (int j = 0; j < selected.Size();  j++)
                    incd[i][j] = testFaces(obj,selected[i],selected[j]);

            selfacesmap.Resize(obj.NFaces());
            for (int i = 0; i < obj.NFaces(); i++) selfacesmap[i] = 0;
            for (int i = 0; i < selected.Size();i++) selfacesmap[selected[i]] = i;
        }

        ~ConvexIncidence() {
            for (int i = 0; i < size; i++) delete [] incd[i];
            delete [] incd;        
        }   


        bool get(int slave, int master) const {
            return incd[slave][master];
        }

        bool getBoth(int slave, int master) const {
            return incd[slave][master] && incd[master][slave];
        }

        bool getMapped(int slave, int master) const {
            slave = selfacesmap[slave];
            master = selfacesmap[master];
            return incd[slave][master];
        }

        bool getBothMapped(int slave, int master) const {
            slave = selfacesmap[slave];
            master = selfacesmap[master];
            return incd[slave][master] && incd[master][slave];
        }

    protected:
        bool testFaces(const ObjectData &obj, int i, int j) {
            FaceT slave(obj,i);
            FaceT master(obj,j);

            Vector3 planeNorm = -master.CalculateNormal(); //negated, because faces has reversed normal
            if (!planeNorm.IsFinite()) planeNorm = Vector3(0,0,0);
            float d = master.GetPointVector<Vector3>(0).DotProduct(planeNorm);
            float position = -d;

            for (int k = 0; k < slave.N(); k++) {
                const Vector3 &pt = slave.GetPointVector<Vector3>(k);
                //test location of the point relative to plane
                float ptd = planeNorm.DotProduct(pt);
                //calculate difference
                float dff = position + ptd ;

                if (dff > accuracy) 
                    return false;
            }
            return true;
        };



    };


    void ObjToolConvex::SplitToConvex(Selection wrk, const SplitToConvexProp &prop)
    {
        
        ProgressBar<int> pbout(1);
        Triangulate(tmConvex, &wrk);

        Selection accum(this);
        wrk.Validate();
        
        AutoArray<int> selfaces = wrk.GetSelectedFaces();


        if (selfaces.Size() > 32000) {
            selfaces.Delete(32000, selfaces.Size() - 32000);
            wrk.Clear();
            wrk.SelectFaces(selfaces);
        }


        ConvexIncidence incd(*this, selfaces,prop.convexAccuracy);   
        CEdges graph(NFaces()),nocross;
        BuildFaceGraph(graph,nocross);

        pbout.AdvanceNext(1);
        ProgressBar<int> pb(selfaces.Size());
        int lastSz = selfaces.Size();
        pb.AdvanceNext(selfaces.Size()/5);

        AutoArray<int> compIndexes;

        while (selfaces.Size() && !pb.StopSignaled()) {
            

            AutoArray<int> curGroup;
            curGroup.Reserve(selfaces.Size());
            Selection tmp(this);
            AutoArray<int> bestgroup = SplitToConvex2_CreateBestGroup(selfaces[0],
                incd, graph, wrk, curGroup, tmp,prop.maxLevels);

            Selection newsel(this);        
            for (int i = 0; i < bestgroup.Size(); i++)  {
                newsel.FaceSelect(bestgroup[i]);
                wrk.FaceSelect(bestgroup[i],false);
            }
            newsel.SelectPointsFromFaces();


            SplitToConvex2_ModifyFaceGraph(wrk,newsel,graph);

            int idx = NFaces();
            compIndexes.Add(idx);
            MakeConvex(newsel,false,prop.regions,prop.stage1only);
            Selection convex(this);
            while (idx <NFaces()) 
                convex.FaceSelect(idx++);
            convex.SelectPointsFromFaces();
            SelectionSplit(&convex);

            accum+=newsel;
            wrk.Validate();
            selfaces = wrk.GetSelectedFaces();
            int step = lastSz - selfaces.Size();
            lastSz = selfaces.Size();
            pb.SetPos(pb.GetMax() - selfaces.Size());
            if (step > selfaces.Size()) step = selfaces.Size();
            pb.AdvanceNext(step);

            

/*            LODObject test;
            test.AddLevel(*this,1);            
                               
            Selection testsel(newsel,test.Level(1));
            test.Level(1)->SaveNamedSel("NewSel",&testsel);


            test.Save(Pathname("T:\\result.p3d"),9999,false,0,0);*/
            
        }

        compIndexes.Add(NFaces());
        for (int i = 1 ; i<compIndexes.Size();) {
            if (compIndexes[i - 1] == compIndexes[i])
                compIndexes.Delete(i - 1);
            else
                i++;
        }

        for (int i = 0; i < compIndexes.Size() -1 ;i++)
            for (int j = i+1; j < compIndexes.Size() -1 ;j++) 
                OptimizeComponents(compIndexes[i],compIndexes[i+1],
                                   compIndexes[j],compIndexes[j+1],
                                   accum,prop);


        accum.SelectPointsFromFaces();
        SelectionSplit(&accum);
        SelectionDelete(&accum);

        

    }

    AutoArray<int> ObjToolConvex::SplitToConvex2_CreateBestGroup(
            int start,
            const ConvexIncidence &incd,
            const CEdges &graph,
            Selection &sel,
            AutoArray<int> &curgroup,
            Selection &wrk,
            int level
            )
    {
        AutoArray<int> bestSubgroup;
        int saveSize = curgroup.Size();
        
        std::deque<int> qf; //queue of faces
        std::deque<int> conflicts; //conflict faces to explore later

        wrk.FaceSelect(start);
        curgroup.Add(start);
        
        qf.push_back(start);
        while (!qf.empty()) {
            
            int fc = qf.front();
            qf.pop_front();

            for (int i = 0, nx; (nx = graph.GetEdge(fc,i)) != -1; i++)  
                if (sel.FaceSelected(nx) && !wrk.FaceSelected(nx)) {
                    if (!incd.getBothMapped(nx,fc))
                        continue;
                    bool okaj = true;           
                    for (int j = 0; j < curgroup.Size(); j++) 
                        if (!incd.getBothMapped(nx,curgroup[j])) {
                            conflicts.push_back(nx);
                            okaj = false;
                            break;
                        }

                    if (okaj) {
                        curgroup.Add(nx);
                        qf.push_back(nx);
                        wrk.FaceSelect(nx);
                    }
                }
        }



        ProgressBar<int> pb(conflicts.size());

        if (level <= 0) {
            AutoArray<int> newgroup;
            newgroup.Reserve(curgroup.Size());
        
            while (!conflicts.empty()) {
                int p = conflicts.front();
                for (int i = 0; i < curgroup.Size(); i++)
                if (incd.getBothMapped(p,curgroup[i]))
                    newgroup.Add(curgroup[i]);
                curgroup = SplitToConvex2_FilterLargestConnectedRegion(graph,newgroup);
                conflicts.pop_front();
            }   
            return curgroup;

        }
            

        if (conflicts.empty()) bestSubgroup = curgroup;
        else while (!conflicts.empty()) {
            
            pb.AdvanceNext(1);
            
            AutoArray<int> grp = SplitToConvex2_TryToRemoveConflict(
                conflicts.front(),
                incd, graph, sel, curgroup, wrk,level-1 );
            if (grp.Size() > bestSubgroup.Size())
                bestSubgroup = grp;

            conflicts.pop_front();
            if (pb.StopSignaled()) break;
        }

        //cleanup
        for (int i = saveSize; i < curgroup.Size(); i++)
            wrk.FaceSelect(curgroup[i],false);
        
        curgroup.Delete(saveSize, curgroup.Size() - saveSize);
        
        return bestSubgroup;
    }

    AutoArray<int>  ObjToolConvex::SplitToConvex2_TryToRemoveConflict(
                int start, const ConvexIncidence &incd,
                const CEdges &graph, Selection &sel,
                const AutoArray<int> &curgroup,Selection &wrk,
                int level) {

        AutoArray<int> newgroup;
        newgroup.Reserve(curgroup.Size());
        
        for (int i = 0; i < curgroup.Size(); i++)
            if (incd.getBothMapped(start,curgroup[i]))
                newgroup.Add(curgroup[i]);
        
        newgroup = SplitToConvex2_FilterLargestConnectedRegion(graph, newgroup);

        return SplitToConvex2_CreateBestGroup(start, incd,graph,sel,newgroup,wrk,level);
    }
                
    void ObjToolConvex::SplitToConvex2_ModifyFaceGraph(const Selection &origin, 
            const Selection &toremove, CEdges &graph) 
    {
        
        int bg = -1;
        for (int i = 0; i<NFaces();i++) if (origin.FaceSelected(i) 
                                            && !toremove.FaceSelected(i)) {
            
            FaceT fc(this,i);
            for (int j = 0; j < fc.N(); j++)
                if (toremove.PointSelected(fc.GetPoint(j))) {
                    if (bg != -1)
                        graph.SetEdge(bg,i);
                    bg = i;
                    break;
                }
        }
            

    }

    AutoArray<int> ObjToolConvex::SplitToConvex2_FilterLargestConnectedRegion(
           const CEdges &graph, const AutoArray<int> &group) 
    {
        Selection mask(this);
        mask.SelectFaces(group);

        AutoArray<int> out;
        int start = mask.GetSelectedFacesRange().first;
        while (start != -1) {
            
            std::deque<int> qf; //queue of faces
            qf.push_back(start);
            mask.FaceSelect(start,false);
            AutoArray<int> accum;
            accum.Add(start);
            while (!qf.empty()) {
                int fc = qf.front();
                qf.pop_front();
                for (int i = 0, nx; (nx = graph.GetEdge(fc,i)) != -1; i++)  
                    if (mask.FaceSelected(nx) ) {
                        qf.push_back(nx);
                        mask.FaceSelect(nx,false);
                        accum.Add(nx);
                    }
            }

            if (accum.Size() > out.Size())
                out = accum;;
            start = mask.GetSelectedFacesRange().first;
        }

        return out;
    }


    void ObjToolConvex::OptimizeComponents(int i1, int i2, 
        int j1, int j2, Selection &delFaces,const SplitToConvexProp &prop) {

            if (!OptimizeComponents2(i1,i2,j1,j2,delFaces,prop))
                OptimizeComponents2(j1,j2,i1,i2,delFaces,prop);
    }


    bool ObjToolConvex::OptimizeComponents2(int i1, int i2, int j1, int j2, Selection &delFaces,const SplitToConvexProp &prop) {

        bool optimized = false;
        PlaneList planes;
        planes.Reserve(j2 - j1);
        for (int j = j1; j < j2; j++) {
            planes.Add(PlaneNfo(FaceT(this,j)));
        }

        int pts = 0;

        Selection isel(this);
        Selection inside(this);
        for (int i = i1; i < i2; i++) {
            FaceT fc(this,i);
            for (int j = 0; j < fc.N(); j++)
                if (!isel.PointSelected(fc.GetPoint(j))) {
                    Vector3 &vx = fc.GetPointVector<Vector3>(j);
                    pts++;
                    bool finside = true;
                    for (int k = 0; k < planes.Size(); k++) {
                        float dist = planes[k].Distance(vx);
                        if (dist > prop.overlapTolerance) {
                            finside = false; 
                            break;
                        }
                    }
                    inside.PointSelect(fc.GetPoint(j),finside);                        
                    isel.PointSelect(fc.GetPoint(j));
                }
        }

        if (inside.NPoints() >= pts) {
            inside.SelectFacesFromPoints();
            delFaces|=inside;
            optimized = true;
            
        }

        return optimized;
    }


    bool ObjToolConvex::RunEdgeTest(int firstFace, int lastFace,Selection &todel) {

//        LogF("(re)starting convex corrector");
        bool recall = false;
        CEdges egs(NPoints());
        todel.Validate();

        for (int i = firstFace; i < lastFace; i++) if (!todel.FaceSelected(i)) {

            FaceT fc(this,i);
            for (int k = fc.N()-1, i = 0; i < fc.N();k = i++) {
                int vx1 = fc.GetPoint(k);
                int vx2 = fc.GetPoint(i);
                if (egs.IsEdgeInDirection(vx1,vx2)) {
//                    LogF("Convex corrector: Double edge %d-%d, removing invalid face %d",vx1,vx2,fc.GetFaceIndex());
                    todel.FaceSelect(fc.GetFaceIndex());
                    recall = true;
                } else
                    egs.SetEdge(vx1,vx2);
            }
        }

/*        for (int i = firstFace; i < lastFace; i++) if (!todel.FaceSelected(i)) {
            int errors = 0;
            FaceT fc(this,i);
            for (int k = fc.N()-1, i = 0; i < fc.N();k = i++) {
                int vx1 = fc.GetPoint(k);
                int vx2 = fc.GetPoint(i);
                if (!egs.IsEdgeInDirection(vx2,vx1))
                    errors++;
            }
            if (errors>1) {
                LogF("Convex corrector: Invalid %d (errors 2) face will be removed",i);
                todel.FaceSelect(i);
                recall = true;
                for (int k = fc.N()-1, i = 0; i < fc.N();k = i++) {
                    int vx1 = fc.GetPoint(k);
                    int vx2 = fc.GetPoint(i);
                    egs.RemoveEdge(vx1,vx2);
                }
            }
        }
  */
        for (int i = firstFace; i < lastFace; i++) if (!todel.FaceSelected(i)) {
            int errors = 0;
            FaceT fc(this,i);
            for (int k = fc.N()-1, i = 0; i < fc.N();k = i++) {
                int vx1 = fc.GetPoint(k);
                int vx2 = fc.GetPoint(i);
                if (!egs.IsEdgeInDirection(vx2,vx1))
                    errors++;
            }
            if (errors==1) {
                for (int k = fc.N()-1, i = 0; i < fc.N();k = i++) {
                    int vx1 = fc.GetPoint(k);
                    int vx2 = fc.GetPoint(i);
                    if (!egs.IsEdgeInDirection(vx2,vx1)) {
                        //LogF("Convex corrector: Found non-closed geometry, trying to patch it (edge %d-%d)",vx1,vx2);
                        for (int u,v=0;(u = egs.GetEdge(vx2,v))!=-1;v++) {
                            if (!egs.IsEdgeInDirection(u,vx2) && !egs.IsEdgeInDirection(vx1,u)) {
                                //LogF("Convex corrector:... trying to continue on vertex %d",u);
                                FaceT nwface;
                                nwface.CreateIn(this);
                                nwface.SetN(3);
                                nwface.SetPoint(0,vx2);
                                nwface.SetPoint(1,vx1);
                                nwface.SetPoint(2,u);
                                Vector3 norm = fc.CalculateRawNormal();
                                if (norm.DotProduct(nwface.CalculateRawNormal())>-0.8) {
                                    float d1 = norm * Point(u);
                                    float d2 = norm * Point(vx1);
                                    if (d2 < d1) {
                                        //LogF("Convex corrector:... success, new face looks valid.",u);
                                        egs.SetEdge(vx2,vx1);
                                        egs.SetEdge(vx1,u);
                                        egs.SetEdge(u,vx2);
                                        todel.Validate();
                                        recall = true;
                                        goto goon;
                                    }
                                }
                                DeleteFace(nwface.GetFaceIndex());
                                recall = true;
                            }
                        }
                        
                        //LogF("Convex corrector:... failed to patch non-closed geometry, deleting errornous face");
                        todel.FaceSelect(fc.GetFaceIndex());
                        for (int k = fc.N()-1, i = 0; i < fc.N();k = i++) {
                            int vx1 = fc.GetPoint(k);
                            int vx2 = fc.GetPoint(i);
                            egs.RemoveEdge(vx1,vx2);
                        }
                        goto goon;
                    }
            }
        }
    }
goon:;
//        LogF("Exiting convex corrector: %s",recall?"(restart)":"done");
        return recall;
    }
};


