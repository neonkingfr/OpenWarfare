#pragma once

#pragma warning(disable: 4661)

#include "SdkCommon.h"
#include "Adapter.h"
#include <vector>

#include "Array.h"
#include "Vector.h"
#include "Selection.h"
#include "Face.h"
#include "Frame.h"
#include "Edges.h"
#include "UVSet.h"

namespace OxygeneSDK {  

    class Selection;
    class Frame;
    class Edges;
    class Face;;
    class UVSet;
    
    ///Selection operation mode
    enum SelMode {
        SelSet, ///< replaces the current selection
        SelAdd, ///< combines the current selection and the new
        SelSub, ///< removes selection from the current selection
        SelAnd ///< leaves selected items that are selected in the new selection
    } ;

    ///Mesh
    class DLLEXTERN Mesh: public Adapter<ObjektivLib::ObjectData> {
        typedef Adapter<ObjektivLib::ObjectData> Super;
    public:        
        Mesh(const ObjektivLib::ObjectData *x):Super(x) {}
        Mesh(ObjektivLib::ObjectData *x,Super::Owner owner):Super(x, owner) {}

        Mesh &operator = ( const Mesh& src) {
            Super::operator=(src);return *this;
        }
        ///Create new mesh
        Mesh();
        ///Create mesh from the frame index
        Mesh(const Mesh &other, int frameIndex);

        ///Merge one mesh to the another
        /**
        @param src source mesh
        @param createSel name of selection to create with this mesh
        @param selections Merges also all selections inside of source mesh.
        @retval success status
        */
        bool Merge( const Mesh &src, const char *createSel=NULL, bool selections=true);


        /** @name Vertices */
        /** @{ */

        ///Retrieves count of vertices
        int NPoints() const;
        ///Retrieves vertex on the index for reading 
        const Vertex Point( int i ) const;
        ///Sets vertex at index
        void SetPoint( int i, const Vertex &vx);
        ///Reserves vertices in vertex table
        /**
            @param count of vertices to reserve
            @return index of first vertex. Next vertex has index+1, next index+2, etc.

            @note Adding and deleting vertices will invalidate all references to another
            vertices in this mesh. 
        */
        int ReservePoints( int count );
        ///Creates new vertex
        /**
            @note Adding and deleting vertices will invalidate all references to another
            vertices in this mesh. 
        */
        Vertex NewPoint();
        
        ///Removes vertex and all faces connected on it
        /**
            @note Adding and deleting vertices will invalidate all references to another
            vertices in this mesh. 
        */
        void DeletePoint( int index );

        /** @} */

        /** @name Normals */
        /** @{ */
        ///Retrieves count of normals
        int NNormals() const;
        ///Retrieves normal at position
        const Vector Normal( int i ) const;
        ///
        void SetNormal( int i, const Vector &vx );
        ///Recalculates all normals
        void RecalcNormals(); 
        Vector NewNormal();
        /** @} */

        /** @name Faces*/
        /** @{ */
        ///Retrieves count of faces */
        int NFaces() const;

        ///Creates new face
        /**
            @note Adding and deleting faces will invalidate all references to another
            faces in this mesh. 
        */
        int NewFace();

        ///Reserves faces in face table
        /**
            @param count of faces to reserve
            @return index of first face. Next face has index+1, next index+2, etc.

            @note Adding and deleting faces will invalidate all references to another
            faces in this mesh. 
        */
        int ReserveFaces( int count );

        void DeleteFace( int index );
        /** @} */
        

        /** @name Selections*/
        /** @{ */

        ///Saves selection as named selection
        /**
         @param name name of the selection. If selection exists, function will 
            redefine this selection.
         @param sel selection to save. Use NULL to save current selection
        @retval true success
        @retval false failed
         */
        bool SaveNamedSel( const char *name , const Selection *sel=NULL);

        ///Copies named selection into current selection
        /**
        @param name name of selection to use
        @param mode combination mode 
        -*/         
        bool UseNamedSel( const char *name, SelMode mode=SelSet );
        ///Removes named selection
        bool DeleteNamedSel( const char *name );
        ///Renames named selection
        bool RenameNamedSel( const char *name, const char *newname );
        ///Uses other named selection
        /** 
        @param sel selection that will be copied into current selection
        @param mode combination mode
        @retval true success
        @retval false failed
        */
        bool UseSelection(const Selection &sel, SelMode mode=SelSet );
        ///Retrieves internal number of the selection    
        int FindNamedSel( const char *name ) const;
        ///Retrieves named selection at given internal index
        const NamedSelection GetNamedSel( int i ) const;
        ///Retrieves named selection by the name
        const NamedSelection GetNamedSel( const char *name ) const;
        ///Retrieves named selection at given internal index
        NamedSelection GetNamedSel( int i );
        ///Retrieves named selection by the name
        NamedSelection GetNamedSel( const char *name );
        ///Retrieves internal indices of all selections
        Array<int> EnumSelections() const;
        ///Retrieves current selection    
        const Selection GetSelection() const;
        ///Retrieves current hidden selection
        const Selection GetHidden() const ;
        ///Retrieves current locked selection
        const Selection GetLocked() const  ;

        ///Retrieves current selection    
        Selection GetSelection() ;
        ///Retrieves current hidden selection
        Selection GetHidden()     ;
        ///Retrieves current locked selection
        Selection GetLocked()      ;
                                    
        ///Makes locked selection current
        void UseLocked() ;
        ///Makes hidden selection current
        void UseHidden() ;
        ///Clears current selection
        void ClearSelection();
        ///Removes selections with empty points and faces
        void CleanEmptySelections();
        ///
        void HideSelection();
        ///
        void UnhideSelection();
        ///
        void LockSelection();
        ///
        void UnlockSelection();
        ///
        bool SelectionDelete(Selection *sel=NULL);
        ///
        bool SelectionDeleteFaces(Selection *sel=NULL);

        /** @} */


        /** @name Named properties*/
        /** @{ */

        ///Retrieves property text
        /**
        @param name Name of property. Name is case insensitive . Returns NULL, when
            named property doesn't exists
        */
        const char *GetNamedProp( const char *name ) const; // NULL if not existing
        ///Sets new named property
        /**
        @param name name of new property. when property exists then it is replaced. 
            Name is not case sensitive
        @param value new value of the property
        @retval true success
        @retval false failed
        
        */
        bool SetNamedProp( const char *name, const char *value );
        ///Removes named property
        /*
        @param name name of the property to remove. Name is not case sensitive
        @retval true success
        @retval false failed
        */
        bool DeleteNamedProp( const char *name );
        ///Renames named property
        /*
        @param name name of the property to rename. Name is not case sensitive
        @param newname new name of the property.
        @retval true success
        @retval false failed
        */
        bool RenameNamedProp( const char *name, const char *newname );
        
        ///Retrieves name of the propert at given internal index
        /**
          @return name of name property, NULL if index is not used
          */
        const char *GetNamedProp( int i ) const;
        ///Retrieves value of the propert at given internal index
        /**
          @return value of name property, NULL if index is not used
          */
        const char *GetNamedPropValue( int i ) const;
        ///Retrieves list of indices for properties
        Array<int> EnumProperties() const;
        /** @} */

        /** @name Animations*/
        /** @{ */
        ///Retrieves count of animation frames 
        /**
          @return count of frames. Value 0 means, that mesh is not animated
        */
        int NFrames() const;
        ///Retrieves frame at given index
        const Frame GetFrame( int i ) const;
        ///Retrieves frame at given index
        Frame GetFrame( int i );
        ///Converts time to index
        int TimeToFrameIndex( float time ) const;
        ///Converts time to nearest index
        int TimeToFrameIndexNearest( float time ) const;
        ///Adds frame to the animation
        bool AddFrame( const Frame &src );
        ///Delete frame by time
        bool DeleteFrame( float time );
        ///Removes animation from the mesh
        void DeleteAllFrames();
        ///Sorts frames by the time.
        bool SortAnimations(); 
        ///Retrieves current animation frame
        int CurrentFrame() const;
        ///Saves all changes in current frame into vertex cache at given frame 
        void SaveFrame( int i );
        ///Makes another frame active (saves changes in current frame)
        void UseFrame( int i );
        ///Saves all changes in current frame into foreign vertex cache
        void SaveFrame( Frame &frame );
        ///Makes another frame active
        void UseFrame( Frame &frame );
        /** @} */


        
        /** @name Misc*/
        /** @{ */
        ///Select faces that is not visible from the point
        void BackfaceCull( const Vector &pin );
        ///Splits the selection
        bool SelectionSplit();
        ///Triangulate
        /**
        @param allFaces all faces, otherwise current selection is used
        */
        void Triangulate( bool allFaces=false );
        ///Squarize
        /**
        @param allFaces all faces, otherwise current selection is used
        */
        void Squarize( bool allFaces=false );
        ///Builds face grafh
        /** Builds graph, where points are faces, and edges are incidence between faces.
        @param result Receives result graph of incidence
        @param nocross List of edges in object, that cannot be included into incidence
        @param FaceTestCallback callback function to exclude some faces. If callback returns false, face is excluded
        @param context user defined context passed into callback function*/
        void BuildFaceGraph(Edges &result, const Edges &nocross, bool (*FaceTestCallback)(const Face &fc,  void *context)=NULL,void *context=NULL) const;
        ///Merges points
        /**
         @param limit how far can be vertices to merge
         @param selected process only selected vertices, false to process all vertices
         @param sel NULL means current selection, otherwise specifies selection to use
         @return success state
         */
        bool MergePoints( double limit, bool selected, Selection *sel=NULL );
        /** @} */

    
       

        /** @name Sharp edges*/
        /** @{ */
        ///Enumerates sharp edges
        /**
          @param enm enumeratior. To start with first sharp edge, set this field to zero
          @param from variable which receives the first point of the sharp edge
          @param to variable which receieves the second point of the sharp edge
          @retval true success
          @retval false no sharp edges retrieved, returned when enumerator reached its end
        */
        bool EnumEdges(int &enm, int &from, int &to) const;
        ///Retrievs sharp edges as Edges class
        /**
        @return object with edges. Note that returned object is copy of the edges.
         Any changes in sharp edges must be saved using the SetSharpEdges
        */
        Edges GetSharpEdges() const;
        ///Sets sharp edges
        /**
        @param edges object that contains edges
        @param merge when true, current sharp edges are not removed
        */
        void SetSharpEdges(const Edges &edges, bool merge=false);
        /** @} */


        /** @name Point mass*/
        /** @{ */        
        ///
        void SetPointMass( int i, double mass );
        ///
        double GetPointMass( int i ) const ;
        ///
        void SetSelectionMass( double mass, bool constTotal=false );
        ///
        double GetSelectionMass() const;
        ///
        double GetTotalMass() const;
        ///
        double GetMaxMass() const;
        ///
        Vector GetMassCentre(bool selection);
        /** @} */


        /** @name UV Sets*/
        /** @{ */        
        ///
        UVSet GetUVSet(int uvsetid);
        ///
        const UVSet GetUVSet(int uvsetid) const;
        ///        
        int GetUVSetCount() const;
        ///
        bool SetActiveUVSet(int uvsetid);
        ///
        UVSet AddUVSet(int uvsetid=-1);
        ///
        UVSet GetActiveUVSet();
        ///
        const UVSet GetActiveUVSet() const;
        ///
        Array<UVSet> EnumUVSets() const;
        ///
        bool DeleteUvSet(int id);        
        ///
        void SortUVSets();
    };




}

