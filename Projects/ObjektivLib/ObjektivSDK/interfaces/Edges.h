#pragma once

#pragma warning(disable: 4661)

#include "SdkCommon.h"
#include "Adapter.h"
#include <vector>

#include "Mesh.h"



namespace OxygeneSDK {  

    class Mesh;

    class DLLEXTERN Edges: public Adapter<ObjektivLib::CEdges> {
        typedef Adapter<ObjektivLib::CEdges> Super;
    public:
        Edges(const ObjektivLib::CEdges *x):Super(x) {}
        Edges(ObjektivLib::CEdges *x,Super::Owner owner):Super(x, owner) {}
        Edges(int items = 0);
        
        int GetEdge(int idx,int i) const;
        void Clear();
        void RemoveEdge(int va, int vb);
        void SetVertices(int cnt, int mode=1);
        void SetEdge(int a, int b);
        bool IsEdge(int va,int vb) const;
        bool IsEdgeInDirection(int va,int vb) const;
        bool operator()(int a, int b) {return IsEdge(a,b);}
        int GetN() const;
    };

}