#include "StdAfx.h"
#include "../interfaces/SdkCommon.h"          
#include "../interfaces/Mesh.h"
#include "../interfaces/Frame.h"

namespace OxygeneSDK {

    Frame::Frame(Mesh &object):Super(new SmallObject<ObjektivLib::AnimationPhase>(object),owner) {}
    Frame::Frame(const Frame& other, Mesh &object )
        :Super(new SmallObject<ObjektivLib::AnimationPhase>(other,object),owner) {}

    void Frame::SetTime( float time ) {return object->SetTime(time);}
    float Frame::GetTime() const {return object->GetTime();}
    void Frame::Redefine(Mesh &data ) {return object->Redefine(data);}

    const Vector Frame::operator[](int i) const {return Vector(object->operator[](i));}
    void Frame::set(int i, const Vector &vx) {object->operator[](i) = vx;}
    void Frame::Validate() {return object->Validate(0);}

}