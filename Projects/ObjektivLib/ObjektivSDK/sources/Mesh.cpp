#include "stdafx.h"
#include "../interfaces/Array.h"
#include "../interfaces/SdkCommon.h"          
#include "../interfaces/OxygeneSdk.h"          
#include "../interfaces/LODMesh.h"          
#include "../interfaces/Selection.h"
#include "../interfaces/Mesh.h"
#include "../interfaces/Frame.h"
#include "../interfaces/Edges.h"
#include "../interfaces/UVSet.h"
#include "Adapter.cpp"

namespace OxygeneSDK {
//    using namespace ObjektivLib;

    Mesh::Mesh() {}
    Mesh::Mesh(const Mesh &other, int frameIndex):Super(new SmallObject<ObjektivLib::ObjectData>(other,frameIndex)) {}

    bool Mesh::Merge( const Mesh &src, const char *createSel, bool selections) {
        return object->Merge(src,createSel,selections);
    }



    int Mesh::NPoints() const {return object->NPoints();}
    const Vertex Mesh::Point( int i ) const {return Vertex(&object->Point(i));}
    void Mesh::SetPoint( int i, const Vertex &vx) {object->Point(i) = vx;}

    int Mesh::ReservePoints( int count ) {return object->ReservePoints(count);}
    Vertex Mesh::NewPoint() {return Vertex(object->NewPoint());}

    void Mesh::DeletePoint( int index ) {return object->DeletePoint(index);}

    int Mesh::NNormals() const {return object->NNormals();}

    const Vector Mesh::Normal( int i )const  {return Vector(object->Normal(i));}
    void Mesh::SetNormal( int i, const Vector &vx ) {object->Normal(i) = vx;}
    void Mesh::RecalcNormals() {object->RecalcNormals();}
    Vector Mesh::NewNormal() {return Vector(object->NewNormal());}
    
    int Mesh::NFaces() const {return object->NFaces();}

    int Mesh::NewFace() {return object->NewFace();}

    int Mesh::ReserveFaces( int count ) {return object->ReserveFaces(count);}

    void Mesh::DeleteFace( int index ) {return object->DeleteFace(index);}

    bool Mesh::SaveNamedSel( const char *name , const Selection *sel) {
        if (sel) return object->SaveNamedSel(name);
        else return object->SaveNamedSel(name,(*sel));
    }

    bool Mesh::UseNamedSel( const char *name, SelMode mode) {
        return object->UseNamedSel(name,(ObjektivLib::SelMode)mode);}
    bool Mesh::DeleteNamedSel( const char *name ) {return object->DeleteNamedSel(name);}
    bool Mesh::RenameNamedSel( const char *name, const char *newname ) {return object->RenameNamedSel(name,newname);}
    bool Mesh::UseSelection(const Selection &sel, SelMode mode) {return object->UseSelection(sel,(ObjektivLib::SelMode)mode);}
    int Mesh::FindNamedSel( const char *name ) const {return object->FindNamedSel(name);}
    const NamedSelection Mesh::GetNamedSel( int i ) const {return object->GetNamedSel(i);}
    const NamedSelection Mesh::GetNamedSel( const char *name ) const {return object->GetNamedSel(name);}
    NamedSelection Mesh::GetNamedSel( int i ) {return object->GetNamedSel(i);}
    NamedSelection Mesh::GetNamedSel( const char *name ) {return object->GetNamedSel(name);}
    Array<int> Mesh::EnumSelections() const {
        std::vector<int> res;
        for (int i = 0; i < MAX_NAMED_SEL; i++) if (object->GetNamedSel(i) !=0) res.push_back(i);
        return res;
    }
    const Selection Mesh::GetSelection() const {return object->GetSelection();}
    const Selection Mesh::GetHidden() const {return object->GetHidden();}
    const Selection Mesh::GetLocked() const {return object->GetLocked();}

    Selection Mesh::GetSelection() {return object->GetSelection();}
    Selection Mesh::GetHidden()    {return object->GetHidden();} 
    Selection Mesh::GetLocked()    {return object->GetLocked();} 

    void Mesh::UseLocked() {return object->UseLocked();}
    void Mesh::UseHidden() {return object->UseHidden();}
    void Mesh::ClearSelection() {return object->ClearSelection();}
    void Mesh::CleanEmptySelections() {return object->CleanEmptySelections();}
    void Mesh::HideSelection() {return object->HideSelection();}
    void Mesh::UnhideSelection() {return object->UnhideSelection();}
    void Mesh::LockSelection() {return object->LockSelection();}
    void Mesh::UnlockSelection() {return object->UnlockSelection();}
    bool Mesh::SelectionDelete(Selection *sel) {return object->SelectionDelete(*sel);}
    bool Mesh::SelectionDeleteFaces(Selection *sel) {return object->SelectionDeleteFaces(*sel);}

    const char *Mesh::GetNamedProp( const char *name ) const {return object->GetNamedProp(name);}
    bool Mesh::SetNamedProp( const char *name, const char *value ) {return object->SetNamedProp(name,value);}
    bool Mesh::DeleteNamedProp( const char *name ) {return object->DeleteNamedProp(name);}
    bool Mesh::RenameNamedProp( const char *name, const char *newname ) {return object->RenameNamedProp(name,newname);}
    const char *Mesh::GetNamedProp( int i ) const {return object->GetNamedProp(i);}
    const char *Mesh::GetNamedPropValue( int i ) const {return object->GetNamedPropValue(i);}
    Array<int> Mesh::EnumProperties() const {
        std::vector<int> res;
        for (int i = 0; i< MAX_NAMED_PROP;i++) if (object->GetNamedProp(i)) res.push_back(i);
        return res;   
    }

    int Mesh::NFrames() const {return object->NAnimations();}
    const Frame Mesh::GetFrame( int i ) const {return object->GetAnimation(i);}
    Frame Mesh::GetFrame( int i ) {return object->GetAnimation(i);}
    int Mesh::TimeToFrameIndex( float time ) const {return object->AnimationIndex(time);}
    int Mesh::TimeToFrameIndexNearest( float time ) const {return object->NearestAnimationIndex(time);}
    bool Mesh::AddFrame( const Frame &src ) {return object->AddAnimation(src);}
    bool Mesh::DeleteFrame( float time ) {return object->DeleteAnimation(time);}
    void Mesh::DeleteAllFrames() {return object->DeleteAllAnimations();}
    bool Mesh::SortAnimations() {return object->SortAnimations();}
    int Mesh::CurrentFrame() const {return object->CurrentAnimation();}
    void Mesh::SaveFrame( int i ) {return object->RedefineAnimation(i);}
    void Mesh::UseFrame( int i ) {return object->UseAnimation(i);}
    void Mesh::SaveFrame( Frame &frm ) {return object->RedefineAnimation(frm);}
    void Mesh::UseFrame( Frame &frm ) {return object->UseAnimation(frm);}

    void Mesh::BackfaceCull( const Vector &pin ) {return object->BackfaceCull(pin);}
    bool Mesh::SelectionSplit() {return object->SelectionSplit();}
    void Mesh::Triangulate( bool allFaces) {return object->Triangulate( allFaces);}
    void Mesh::Squarize( bool allFaces) {return object->Squarize( allFaces);}   
/*    void Mesh::BuildFaceGraph(Edges &result, 
                             const Edges &nocross, 
                             bool (*FaceTestCallback)(const ObjektivLib::FaceT &fc,  void *context),
                             void *context) const {
        object->BuildFaceGraph(result,nocross,FaceTestCallback,context);
    }*/
    bool Mesh::MergePoints( double limit, bool selected, Selection *sel) {
        return object->MergePoints(limit,selected,*sel);
    }
    bool Mesh::EnumEdges(int &enm, int &from, int &to) const {return object->EnumEdges(enm,from,to);}
    Edges Mesh::GetSharpEdges() const {
        ObjektivLib::CEdges *egs = new SmallObject<ObjektivLib::CEdges>(object->NPoints());
        object->LoadSharpEdges(*egs);
        return Edges(egs,Edges::owner);
    }
    void Mesh::SetSharpEdges(const Edges &edges, bool merge) {
        object->SaveSharpEdges(edges, merge);
    }
    void Mesh::SetPointMass( int i, double mass ) {return object->SetPointMass(i,mass);}
    double Mesh::GetPointMass( int i ) const  {return object->GetPointMass(i);}
    void Mesh::SetSelectionMass( double mass, bool constTotal) {
        return object->SetSelectionMass(mass,constTotal); }
    double Mesh::GetSelectionMass() const {return object->GetSelectionMass();}
    double Mesh::GetTotalMass() const {return object->GetTotalMass();}
    double Mesh::GetMaxMass() const {return object->GetMaxMass();}
    Vector Mesh::GetMassCentre(bool selection) {return Vector(object->GetMassCentre(selection));}
    UVSet Mesh::GetUVSet(int uvsetid) {return UVSet(object->GetUVSet(uvsetid));}
    const UVSet Mesh::GetUVSet(int uvsetid) const {return UVSet(object->GetUVSet(uvsetid));}
    int Mesh::GetUVSetCount() const {return object->GetUVSetCount();}
    bool Mesh::SetActiveUVSet(int uvsetid) {return object->SetActiveUVSet(uvsetid);}
    UVSet Mesh::AddUVSet(int uvsetid) {return UVSet(object->AddUVSet(uvsetid));}
    UVSet Mesh::GetActiveUVSet() {return UVSet(object->GetActiveUVSet());}
    const UVSet Mesh::GetActiveUVSet() const {return UVSet(object->GetActiveUVSet());}
    class EnumUVSFunctor {
        mutable std::vector<UVSet> &items;
    public:
        EnumUVSFunctor(vector<UVSet> &items):items(items) {}
        bool operator()(const ObjektivLib::ObjUVSet *uv) const {
            items.push_back(UVSet(uv));
            return false;
        }
    };
    Array<UVSet> Mesh::EnumUVSets() const {
        vector<UVSet> uvids;
        object->EnumUVSets(EnumUVSFunctor(uvids));
        return uvids;
    }
    bool Mesh::DeleteUvSet(int id) {return object->DeleteUvSet(id);}
    void Mesh::SortUVSets() {return object->SortUVSets();}

    
}