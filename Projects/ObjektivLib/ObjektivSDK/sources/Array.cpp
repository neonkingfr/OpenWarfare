#include "stdafx.h"
#include "Adapter.cpp"
#include "../interfaces/Array.h"

namespace OxygeneSDK {

    template DLLEXTERN class Array<int>;
    template DLLEXTERN class Array<UVSet>;

};