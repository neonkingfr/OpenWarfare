#include "StdAfx.h"
#include "../interfaces/SdkCommon.h"          
#include "../interfaces/Mesh.h"
#include "../interfaces/Selection.h"

namespace OxygeneSDK {

    Selection::Selection(Mesh &object):
        Super(new SmallObject<ObjektivLib::Selection>(object),owner) {}
    Selection::Selection(const Selection &src, Mesh &object):
        Super(new SmallObject<ObjektivLib::Selection>(src,object)) {}

        void Selection::operator += ( const Selection &src ) {return object->operator +=(*src.object);}
    void Selection::operator -= ( const Selection &src )  {return object->operator -=(*src.object);}
    void Selection::operator &= ( const Selection &src ) {return object->operator &=(*src.object);}
    void Selection::operator |= ( const Selection &src ) {return object->operator |=(*src.object);}
    void Selection::Invert() {object->operator ~();}
    float Selection::PointWeight( int i ) const {return object->PointWeight(i);}
    void Selection::SetPointWeight( int i, float w ) {return object->SetPointWeight(i,w);}
    bool Selection::PointSelected( int i ) const {return object->PointSelected(i);}
    void Selection::PointSelect( int i, bool sel) {return object->PointSelect(i,sel);}
    bool Selection::FaceSelected( int i ) const {return object->FaceSelected(i);}
    void Selection::FaceSelect( int i, bool sel ) {return object->FaceSelect(i);}
    void Selection::Clear() {return object->Clear();}
    bool Selection::IsEmpty() const {return object->IsEmpty();}
    int Selection::NPoints() const {return object->NPoints();}
    int Selection::NFaces() const {return object->NFaces();}
    void Selection::SelectPointsFromFaces() {return object->SelectPointsFromFaces();}
    void Selection::SelectFacesFromPoints() {return object->SelectFacesFromPoints();}
    bool Selection::SelectFacesFromPointsTouchMode() {return object->SelectFacesFromPointsTouchMode();}
    void Selection::SelectConnectedFaces() {return object->SelectConnectedFaces();}
    void Selection::SetObject(Mesh &obj) {return object->SetOwner(obj);}
    const Mesh Selection::GetObject() const {return object->GetOwner();}

    void Selection::Validate() {return object->Validate();}
    Vector Selection::GetCenter() const {return Vector(object->GetCenter());}
    std::pair<Vector, Vector> Selection::GetBoundingBox() const {
        return std::pair<Vector,Vector>(object->GetBoundingBox());
    }

    void Selection::SelectAll() {return object->SelectAll();}

    std::pair<int, int>  Selection::GetSelectedPointsRange() const {return object->GetSelectedPointsRange();}
    std::pair<int, int>  Selection::GetSelectedFacesRange() const {return object->GetSelectedFacesRange();}



    const char *NamedSelection::Name() const {
        const ObjektivLib::Selection *sel = object;
        const ObjektivLib::NamedSelection *nsel = static_cast<const ObjektivLib::NamedSelection *>(sel);
      return nsel->Name();
    }
    void NamedSelection::SetName(const char *name) {
        return static_cast<ObjektivLib::NamedSelection *>(object)->SetName(name);
    }

    NamedSelection::NamedSelection(const ObjektivLib::NamedSelection *x):Super(x) {}
    NamedSelection::NamedSelection(ObjektivLib::NamedSelection  *x,Super::Owner owner):Super(x,owner) {}

};