#include "common.hpp"
#include "data3d.h"
#include ".\objuvset.h"
#include "ObjectData.h"

namespace ObjektivLib {

void ObjUVSet::Validate()
{
  int faces=_owner->NFaces();
  int maxindex=GetUVIndex(faces);
  _uvsets.Resize(maxindex);
}


static ObjUVSet_Item SEmptyUV(0,0);

void ObjUVSet::SetUV(int face, int vs, const ObjUVSet_Item& uv)
{
  int idx=GetUVIndex(face,vs);
  if (idx>=_uvsets.Size()) Validate();
  _uvsets[idx]=uv;
}

ObjUVSet_Item ObjUVSet::GetUV(int face, int vs) const
{
  int idx=GetUVIndex(face,vs);
  if (idx>=_uvsets.Size()) return SEmptyUV;
  else return _uvsets[idx];
}

void ObjUVSet::DeleteSelected(const Selection &sel)
{
  Validate();
  int a=0,b=0,cnt=_owner->NFaces();
  while (a<cnt)
  {
    if (!sel.FaceSelected(a))
    {      
      if (a!=b) 
        for (int j=0;j<MAX_DATA_POLY;j++)
          _uvsets[GetUVIndex(b,j)]=_uvsets[GetUVIndex(a,j)];
      b++;
    }
    a++;
  }
  if (a!=b) _uvsets.Resize(GetUVIndex(b));
}

void ObjUVSet::DeleteFace(int face, int count)
{
  Validate();
  _uvsets.Delete(GetUVIndex(face),count*MAX_DATA_POLY);
}
int ObjUVSet::CalculateSaveSize()
{
  int sz=0;
  for (int i=0;i<_owner->NFaces();i++)
  {
    sz+=sizeof(ObjUVSet_Item)*_owner->Face(i).n;
  }
  sz+=sizeof(int);
  return sz;
}

void ObjUVSet::Save(ostream &out)
{
  out.write((char *)&_setIndex,sizeof(_setIndex));
  for (int i=0;i<_owner->NFaces();i++)
  {
    for (int j=0;j<_owner->Face(i).n;j++)
    {
      const ObjUVSet_Item &itm=GetUV(i,j);
      out.write((char *)&itm,sizeof(ObjUVSet_Item));
    }
  }
}


void ObjUVSet::Load(istream &in)
{
  in.read((char *)&_setIndex,sizeof(_setIndex));
  Validate();
  for (int i=0;i<_owner->NFaces();i++)
  {
    for (int j=0;j<_owner->Face(i).n;j++)
    {
      ObjUVSet_Item &itm=_uvsets[GetUVIndex(i,j)];
      in.read((char *)&itm,sizeof(ObjUVSet_Item));
    }
  }
}

}