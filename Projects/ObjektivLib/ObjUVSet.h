#pragma once

namespace ObjektivLib {


class Selection;
struct ObjUVSet_Item
{
  float u,v;
  ObjUVSet_Item() {}
  ObjUVSet_Item(float u,float v):u(u),v(v) {}
  ClassIsSimpleZeroed(ObjUVSet_Item);
};

class ObjectData;

class ObjUVSet
{  
  AutoArray<ObjUVSet_Item> _uvsets;
  int _setIndex;
  ObjectData *_owner;

  static int GetUVIndex(int face,int vs=0) {return face*MAX_DATA_POLY+vs;}
  static int GetFaceIndex(int uvindex) {return uvindex/MAX_DATA_POLY;}
  static int GetVertexIndex(int uvindex) {return uvindex%MAX_DATA_POLY;}

public:
  ObjUVSet(ObjectData *owner, int id):_owner(owner),_setIndex(id) {}
  ObjUVSet(const ObjUVSet &other, ObjectData *newOwner): _uvsets(other._uvsets),
    _setIndex(other._setIndex),_owner(newOwner) {}
//ObjUVSet(const ObjUVSet &other);

  ObjectData *GetOwner() const {return _owner;}
  int GetIndex() const {return _setIndex;}
  void SetIndex(int index) {_setIndex=index;}  

  void Validate();

  void SetUV(int face, int vs, const ObjUVSet_Item& uv);

  ObjUVSet_Item GetUV(int face, int vs) const;

  void DeleteSelected(const Selection &sel);

  void DeleteFace(int face, int count=1);

  int CalculateSaveSize();

  void Save(std::ostream &out);

  void Load(std::istream &in);
};



}