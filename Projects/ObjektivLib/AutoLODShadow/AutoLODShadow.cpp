// AutoLODShadow.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MassProcess.h"
#include "../ObjToolsMatLib.h"

class AutoLODShadow:public MassProcess
  {
  public:
    bool ProcessFile(const char *filename);
  };




int main(int argc, char* argv[])
{
bool recursive=false;
const char *folder;
if (argc<2)
  {
  puts("Parameters: [/s] folder\n\n/s          process recursive\nfolder      folder to process");
  return -1;
  }
if (stricmp(argv[1], "/s")==0)
  {
  if (argc<3)
    {
    puts("Missing parameter");
    return -1;
    }
  recursive=true;
  folder=argv[2];
  }
else
  folder=argv[1];
AutoLODShadow mass;
mass.ProcessFolders(folder,"*.p3d",recursive);
return 0;
}


bool AutoLODShadow::ProcessFile(const char *filename)
  {
  LODObject lod;
  if (lod.Load(filename,NULL,NULL)!=0)
    fprintf(stderr,"Error opening: %s\n",filename);
  else
    {
    BTree<ObjMatLibItem> texmat;
    for (int i=0;i<lod.NLevels();i++)
      {
      ObjToolMatLib &matlib=lod.Level(i)->GetTool<ObjToolMatLib>();
      matlib.ReadMatLib(texmat,ObjToolMatLib::ReadTextures);
      matlib.ReadMatLib(texmat,ObjToolMatLib::ReadMaterials);
      }
    if (texmat.Smallest()!=NULL)
      {
      BTreeIterator<ObjMatLibItem> texmatit(texmat);
      texmatit.BeginFrom(*texmat.Smallest());
      ObjMatLibItem *item;
      while (item=texmatit.Next())
        {
        if (item->name.GetLength()!=0)
          printf("P3D: %s --> REF: %s\n",filename,item->name.Data());
        }
      }
    else
        printf("P3D: %s --> ## No Reference ##\n",filename);
    }
  return true;
  }