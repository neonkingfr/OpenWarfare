//define parsing function
//param - IOStream, opened file, or another stream, return - array parsed paramfile;

paramFileRead=
{
	private "_ReadClass";
	private "_ReadRawClass";
	private "_ReadArray";
	private "_ReadSimpleValue";
	private "_ReadSimpleItem";
	//localfunction _ReadClass
	//param - IOStream, return Array ["<name>","class",[....] ]

	_ReadClass=		//input: <class name> { ... };
	{
		private "_content";
	    private "_classname";
	    private "_result";
		eatWS _this;										//eat whitespaces
		if (testIdentifier _this) then						//test stream for standard identifier
		{				
		   _classname=_this get 0;							//get classname (get everything tested)
		   eatWS _this;										//eat whitespaces
		   if (_this exploreFor "{") then					//test stream if contains {
		   {
				_this ignore 1;								//ignore character {
				_content=_this call _ReadRawClass;			//Read content of class
				if ((!isnil "_content") && (_this exploreFor "}[\S]*;")) then //test end of class }; [\S] is whitechar
				{
					_result=+[_classname,"class",_content];	//store values as result
					_this ignore 0;							//skip everything tested };
				}
				else
				{
					_result=nil							//error is nil
				};
		   }
		   else
		   {
				_result=nil;						//error is nil
		   };
		}
		else
		{
			_result=nil;							//error is nil
		};
	_result											//return result
	};


	_ReadSimpleValue= //input="any text which may represent value"
	{
		private "_value";												
		eatWS _this;								//eat whitespwaces
		switch (true) do							//switch for true
		{
			case testNumber _this:					//test for number
			{								
				_value=val (_this get 0);			//it is number, get all and convertit
			};
			case (_this exploreFor "(""[^""]*"")+[^""]"): // "([^"]*)+[^"] - begins with ", endings with " and something which is not "
			{										//test for string
				_this ignore 1;						//ignore first quote
				_value=_this get -2;				//get remaining excluding 2 characters (quote and one extra character)
				_this ignore 1;						//ignore last quite 
			};
			case (_this exploreFor "{"):			//subarray test for {
			{
				_this ignore 1;						//skip {
				eatWS _this;						//eat whitespaces
				private ["_arr","_subitem"];		
				_arr=+[];							//prepare array
				_subitem=_this call _ReadSimpleValue;	//read first value	
				eatWS _this;						//eat extra whitespaces
				while {_this exploreFor ","} do		//while additional values present
				{
					_this ignore 0;					//ignore all tested (comma ,)
					_arr set [count _arr,_subitem];	//store previous value
					_subitem=_this call _ReadSimpleValue;	//read next value	
					eatWS _this;					//eat extra whitespaces
				};			
				_arr set [count _arr,_subitem];		//store last value
				if (_this exploreFor "}") then		//test for end of array
				{
					_value=_arr;					//store result as value
					eatWS (_this ignore 1);			//ignore } and eat whitespaces
				}
				else								//error is nil
				{
					_value=nil;
				};
			};
			default {_value=nil;};					//we failed all tests, report error;
		};
	_value											//result
	};
	
	_ReadArray=	//input: name[]=[
	{
		private "_name";
		private "_content";
		private "_result";
		testIdentifier _this;	//get name without []
		_name=_this get 0;
		_this exploreFor "[^=]*="; //get everything until = (including =)
		_this ignore 0;
		eatWS _this;
		if (_this exploreFor "{") then
		   {
		   _content=_this call _ReadSimpleValue;
		   if (!isnil "_content") then
				{
				if (_this exploreFor ";") then
					{
					eatWS (_this ignore 1);					
				   _result=+[_name,"array",_content];
					}
				else
					{
					_result=nil;
					};
				}
			else
				{
				_result=nil;
				};
		   }
		   else
		   {
				_result=nil;
		   };
	_result		
	};

	_ReadSimpleItem=		//input: name=
	{
		private ["_name","_content","_result"];
		testIdentifier _this;
		_name=_this get 0;
		eatWS (eatWS _this ignore 1);
		_content=_this call _ReadSimpleValue;
		if (!isnil "_content") then 
		{
			if (_this exploreFor ";") then
			{
				eatWS (_this ignore 1);					
				_result=+[_name,"simple",_content];
			}
			else
			{
				_result=nil;
			};
		}
		else
		{
			_result=nil;	
		};
	_result		
	};

	//localfunction _ReadRawClass
	//param - IOStream, return Array, that represents inside of class (items)
	_ReadRawClass=
	{
		private ["_loop","_item","_result"];
		_result=+[];
		_loop=true;
		while {_loop} do
		{
			eatWS _this;
			_item=nil;
			switch (true) do
			{				
				case (_this exploreFor "class "):		//in case class word
				{
					_this ignore 0;					
					_item=_this call _ReadClass ;
				};
				case (_this exploreFor "[_a-zA-Z][_a-zA-Z0-9]*\[\]_*="):	//in case "name[]=" (_ - is whitespace without new line)
				{
					_item=_this call _ReadArray ;
				};
				case (_this exploreFor "[_a-zA-Z][_a-zA-Z0-9]*_*="):  //in case "name=" (_ - is whitespace without new line)
				{
					_item=_this call _ReadSimpleItem ;
				};
				default								//in case we don't know
				{
					_loop=false;
				};				
			};
			if !(isnil "_item") then 
				{ _result set [count _result,_item];}
			else
				{ _loop=false;};
		};
	_result
	};

private "_content";

_content=_this call _ReadRawClass;
if (!eof(_this)) then {_content=nil;};
_content
};

paramFileWrite={  //[stream, paramFileArray]
	private "_writeClass";
	private "_writeArray";
	private "_writeVariable";
	private "_writeValue";
	private "_writeInsideClass";
	private	"_out";
	private "_block";	
	



	_out=_this select 0;
	_level=0;
	_block="";
	
	

	_writeValue={			//_this input is any variable
		switch (typeName _this) do
		{
			case "STRING":{_out<<""""<<_this<<"""";};
			case "SCALAR":{_out<<str(_this);};
			case "ARRAY": 
			{
				_out<<"{";
				private "_comma";
				_comma="";
				{
					_out<<_comma;
					_x call _writeValue;
					_comma=",";					
				} forEach _this;
				_out<<"}";					
			};
			default {_out<<""""<<str(_this)<<"""";};
		};
		
	};

	_writeVariable={		//_this=[name,"simple",value]
		_out<<_block<<(_this select 0)<<"=";
		(_this select 2) call _writeValue;
		_out<<";"<<eoln;
	};

	_writeArray={			//_this=[name,"array",[values]]
		_out<<_block<<(_this select 0)<<"[]=";
		(_this select 2) call _writeValue;
		_out<<";"<<eoln;
	};

	_writeClass={			//_this=[name,"class",[values]]
		_out<<_block<<"class "<<(_this select 0)<<eoln<<_block<<"{"<<eoln;
		_block=_block+"    ";
		_writeInsideClass forEach (_this select 2);
		_block=_block @ [0,count _block-4];
		_out<<_block<<"};"<<eoln;	
	};

	_writeInsideClass={		//_x=[name,type,value(s)]
		private "_this";
		_this=_x;
		switch (_this select 1) do
		{
			case "class": _writeClass;
			case "array": _writeArray;
			case "simple": _writeVariable;
		};	
	};


	_writeInsideClass forEach (_this select 1);

};


paramFileFind={		//_this=[paramFile,name1,name2,name3...]
	private "_pfile";
	private "_result";
	private "_paramFilePartialFind";
	
	_paramFilePartialFind={ //_this=[paramFile,name];
		private "_i";
		private "_found";
		private "_pfile";
		_pfile=_this select 0;
		for "_i" from 0 to count _pfile do
		{
			if ((_pfile select _i) select 0==_this select 1) then
			{
				_found=_pfile select _i;
				_i=nil;
			};
		};
		_found
	};
	
	_pfile=_this select 0;
	{
		if (!isnil "_pfile" && typeName _x=="STRING") then
		{
			_result=([_pfile,_x] call _paramFilePartialFind) ;
			_pfile=_result select 2
		};
	}
	forEach _this;
	_result
};