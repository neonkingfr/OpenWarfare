#include "common.hpp"
#include "FaceT.h"
#include "ObjectData.h"
#include <malloc.h>

namespace ObjektivLib {


    class FaceHelper: public FaceTraditional
    {

        friend class FunctorEnumStages;
        class FunctorEnumStages
        {
            mutable FaceHelper *_object;
            int _face;
            int _vscount;
        public:
            FunctorEnumStages(FaceHelper *object, int face, int vscount):_object(object),_face(face),_vscount(vscount) {}
            bool operator() (ObjUVSet *uvset) const
            {
                for (int i=0;i<_vscount;i++)
                {
                    (*_object->SetUV(uvset->GetIndex(),i))=uvset->GetUV(_face,i);
                }
                return false;
            }
        };

    public:
        static const int maxStages=11;
        ObjUVSet_Item uv[maxStages][MAX_DATA_POLY];
        int _stages[maxStages];
        int _current;

        FaceHelper()
        {
            memset(_stages,0xFF,sizeof(_stages));
            memset(uv,0,sizeof(uv));
            _current=0;
        }

        int GetStageIndex(int idx,bool add)
        {
            int i=idx%maxStages;
            int j=i;
            int empty=-1;
            do 
            {
                if (_stages[i]==idx) return i;
                if (empty==-1 && _stages[i]==-1) empty=i;
                i=(i+1)%maxStages;
            }
            while(i!=j);
            if (!add) return -1;
            if (empty==-1) empty=idx%maxStages;
            _stages[empty]=idx;    
            return empty;
        }

        ObjUVSet_Item *GetUV(int stage, int vs) const
        {
            int idx=const_cast<FaceHelper *>(this)->GetStageIndex(stage,false);
            if (idx==-1) return 0;
            else return const_cast<ObjUVSet_Item *>(uv[idx]+vs);
        }

        ObjUVSet_Item *GetCurrentUV(int vs) const {return GetUV(_current,vs);}  

        ObjUVSet_Item *SetUV(int stage,int vs)
        {
            int idx=GetStageIndex(stage,true);
            return uv[idx]+vs;
        }

        ObjUVSet_Item *SetCurrentUV(int vs) {return SetUV(_current,vs);}  

        void SetFromFace(const ObjectData *objectData, int face)
        {
            FaceTraditional::operator=(objectData->Face(face));
            objectData->EnumUVSets(FunctorEnumStages(this,face,n));
        }
        void SetActiveStage(int stage)
        {
            _current=stage;
        }

        int GetActiveStage() const
        {
            return _current;
        }
    };

    inline int Modulo( int i, int n )
    {
        if( i>=n ) i-=n;
        if( i<0 ) i+=n;
        return i;
    }

    void FaceT::swap_pts(int pt1,int pt2)
    {
        int blksz=GetPointInfoSize();
        void *block1=alloca(blksz);
        void *block2=alloca(blksz);
        SavePointInfo(pt1,block1);
        SavePointInfo(pt2,block2);
        LoadPointInfo(pt2,block1);
        LoadPointInfo(pt1,block2);
    }


    bool FaceT::ContainsPoint( int vertex ) const
    {
        for( int i=0; i<N(); i++ ) if( GetPoint(i)==vertex ) return true;
        return false;
    }

    int FaceT::ContainsPoints( const bool *vertices ) const
    {
        int count=0;
        for( int i=0; i<N(); i++ ) count+=vertices[GetPoint(i)];
        return count;
    }

    bool FaceT::ContainsEdge( int v1, int v2 ) const
    {
        for( int i=0,last=N()-1; i<N(); last=i,i++ )
        {
            if( GetPoint(last)==v1 && GetPoint(i)==v2 ) return true;
            //if( vs[i].point==v2 && vs[last].point==v1 ) return true;
        }
        return false;
    }

    bool FaceT::IsNeighbourgh( const FaceT &face ) const
    {
        for( int i=0,last=N()-1; i<N(); last=i,i++ )
        {
            int actV=GetPoint(i);
            int lastV=GetPoint(last);
            if( face.ContainsEdge(actV,lastV) ) return true;
        }
        return false;
    }

    bool FaceT::GetEdge(const FaceT &other, int &v1,int &v2) const
    {
        for( int i=0,last=N()-1; i<N(); last=i,i++ )
        {
            int actV=GetPoint(i);
            int lastV=GetPoint(last);
            if( other.ContainsEdge(actV,lastV) ) 
            {
                v1=lastV;
                v2=actV;
                return true;
            }
        }
        return false;
    }

    bool FaceT::GetEdgeIndices(const FaceT &other, int &i1a,int &i1b, int &i2a, int &i2b) const
    {
        for( int i=0,last=N()-1; i<N(); last=i,i++ )
        {
            int actV=GetPoint(i);
            int lastV=GetPoint(last);
            for (int j=0,lastj=other.N()-1;j<other.N();lastj=j,j++)
            {
                if (GetPoint(i)==other.GetPoint(lastj) && GetPoint(last)==other.GetPoint(j))
                {
                    i1a=i;
                    i1b=last;
                    i2a=lastj;
                    i2b=j;
                    return true;
                }
            }
        }
        return false;

    }


    bool FaceT::ContainsNormal( int normal ) const
    {
        for( int i=0; i<N(); i++ ) if( GetNormal(i)==normal ) return true;
        return false;
    }

    int FaceT::PointAt( int vertex ) const
    {
        for( int i=0; i<N(); i++ ) if( GetPoint(i)==vertex ) return i;
        return -1;
    }

    void FaceT::Reverse()
    {
        int n=N();
        for (int i=n/2;i<n;i++)
        {
            int j=n-i-1;
            if (i!=j) swap_pts(i,j);
        }
    }

    void FaceT::Cross()
    {
        if( N()==4 )
        {
            static bool swap23=false;
            swap23=!swap23;
            if( swap23 ) swap_pts(2,3);
            else swap_pts(1,2);
        }
    }

    void FaceT::Shift1To0()
    {
        swap_pts(0,1);
        swap_pts(1,2);
    }

    void FaceT::Shift0To1()
    {
        swap_pts(1,2);
        swap_pts(0,1);
    }

    double FaceT::CalculatePerimeter(  ) const
    {
        double perimeter=0;
        for( int i=0,last=N()-1; i<N(); last=i++ )
        {
            int iLast=GetPoint(last);
            int iCurr=GetPoint(i);
            Vector3 p1=(Vector3)_obj->Point(iLast)-(Vector3)_obj->Point(iCurr);
            perimeter+=p1.Size();
        }
        return perimeter;
    }

    void FaceT::AutoUncross( )
    {
        if( N()==4 )
        {
            // crossed face has bigger perimeter than non-crossed
            // from all possible permutations we will select the one with the smallest perimeter
            FaceT temp=*this;
            float minPeri=1e20f;
            int i,minI=0;
            for( i=0; i<4; i++ )
            {
                float peri=(float)temp.CalculatePerimeter();
                if( minPeri>peri ) minPeri=peri,minI=i;
                temp.Cross();
            }
            for( i=0; i<minI; i++ ) Cross();
        }
    }

    void FaceT::AutoReverse(  )
    {
        // calculate topology defect for this face
        int neighbourghs=0;
        for( int j=0,last=N()-1; j<N(); last=j,j++ )
        {
            int iLast=GetPoint(last);
            int iCurr=GetPoint(j);
            for( int vs=0; vs<_obj->NFaces(); vs++ )
            {
                FaceT other(_obj,vs);
                if( other.ContainsEdge(iCurr,iLast) ) neighbourghs++;
                if( other.ContainsEdge(iLast,iCurr) ) neighbourghs--;
            }
        }
        // each face is its own negative neighbourgh
        // face with no neighbourghs has neighbourghs=-n
        // reversing face means all neighbourgs but the face itself will change their sign
        // i.e. -n-(neighbourghs+n)=-2n-neighbourghs;
        // we want neighbourghs to be as close to 0 as possible
        if( abs(neighbourghs)>abs(2*N()+neighbourghs) ) Reverse();
    }

    void FaceT::CopyPointInfo(int trgpt, int srcpt)
    {
        int blksz=GetPointInfoSize();
        void *block1=alloca(blksz);
        SavePointInfo(srcpt,block1);
        LoadPointInfo(trgpt,block1);
    }

    void FaceT::CopyPointInfo(int trgpt, FaceT &other, int srcpt)
    {
        int blksz=other.GetPointInfoSize();
        void *block1=alloca(blksz);
        other.SavePointInfo(srcpt,block1);
        LoadPointInfo(trgpt,block1);

    }

    Vector3 FaceT::CalculateNormal
        (
        const AnimationPhase &phase
        ) const
    {
        Vector3 sum=VZero;
        // calculate average of normal in all vertices
        // (important when face is not exactly linear)
        for( int j=0; j<N(); j++ )
        {
            int act=GetPoint(j);
            int prev=GetPoint(Modulo(j+N()-1,N()));
            int next=GetPoint(Modulo(j+1,N()));
            Vector3 p1=phase[act];
            Vector3 p0=phase[prev];
            Vector3 p2=phase[next];
            sum=sum+(p1-p0).CrossProduct(p2-p0);
        }
        return sum.Normalized();
    }

    Vector3 FaceT::CalculateRawNormal(  ) const
    {
        Vector3 sum=VZero;
        // calculate average of normal in all vertices
        // (important when face is not exactly linear)
        for( int j=0; j<N(); j++ )
        {
            int act=GetPoint(j);
            int prev=GetPoint(Modulo(j+N()-1,N()));
            int next=GetPoint(Modulo(j+1,N()));
            Vector3 p1=_obj->Point(act);
            Vector3 p0=_obj->Point(prev);
            Vector3 p2=_obj->Point(next);
            sum=sum+(p1-p0).CrossProduct(p2-p0);
        }
        return sum;
    }

    Vector3 FaceT::CalculateNormalAtPointAdjusted(int vs) const
    {
        int act=GetPoint(vs);
        int prev=GetPoint(Modulo(vs+N()-1,N()));
        int next=GetPoint(Modulo(vs+1,N()));
        const PosT &p0=_obj->Point(act);
        Vector3 p1=_obj->Point(prev)-p0;
        Vector3 p2=_obj->Point(next)-p0;
        if (p0.flags & POINT_SPECIAL_ANGLENORMAL)
            return p2.CrossProduct(p1).Normalized()*acos(p1.CosAngle(p2)); 
        else
            return p2.CrossProduct(p1);

    }

    Vector3 FaceT::CalculateNormal( ) const
    {
        Vector3 sum=CalculateRawNormal();
        sum.Normalize();
        return sum;
    }

    double FaceT::CalculateArea( ) const
    {
        //Vector3 sum(0,0,0);
        float sum=0;
        // calculate average of normal in all vertices
        // (important when face is not exactly linear)
        for( int j=0; j<N(); j++ )
        {
            int act=GetPoint(j);
            int prev=GetPoint(Modulo(j+N()-1,N()));
            int next=GetPoint(Modulo(j+1,N()));
            Vector3 p1=_obj->Point(act);
            Vector3 p0=_obj->Point(prev);
            Vector3 p2=_obj->Point(next);
            //sum=sum+(p1-p0).CrossProduct(p2-p0);
            sum+=(p1-p0).CrossProduct(p2-p0).Size();
        }
        // each triangle is counted three times
        //return sum.Size()*(1.0/6);
        return sum*(1.0/6);
    }

    bool FaceT::IsConvex(  ) const
    {
        // for planar convex polygon all triangles of triangulation
        // must have the same normal
        Vector3 normal=CalculateNormal();
        float sum=0;
        // calculate average of normal in all vertices
        // (important when face is not exactly linear)
        for( int j=0; j<N(); j++ )
        {
            int act=GetPoint(j);
            int prev=GetPoint(Modulo(j+N()-1,N()));
            int next=GetPoint(Modulo(j+1,N()));
            Vector3 p1=_obj->Point(act);
            Vector3 p0=_obj->Point(prev);
            Vector3 p2=_obj->Point(next);
            //sum=sum+(p1-p0).CrossProduct(p2-p0);
            Vector3 tNormal=(p1-p0).CrossProduct(p2-p0);
            tNormal.Normalize();
            if( (tNormal-normal).SquareSize()>1e-2 ) return false;
        }
        return true;
    }




    bool FaceT::PointInHalfSpace( const Vector3 &normal, float d, const Vector3 &p )
    {
        float inside=p*normal+d;
        if( inside>=0 ) return true;
        // if the point is too out, fail
        if( inside*inside<normal.SquareSize()*1e-6 ) return true;
        return false;
    }


    int FaceT::PointInHalfSpaceEx(const Vector3 &normal, float d, const Vector3 &p)
    {
        float inside=p*normal+d;
        if( inside>1e-5 ) return 1;
        if( inside<-1e-5 ) return -1;
        return 0;
    }

    bool FaceT::PointInHalfSpace
        (
        const Vector3 &p
        ) const
    {
        Vector3 normal=CalculateRawNormal();
        Vector3 p0=_obj->Point(GetPoint(0));
        float d=-(normal*p0);
        return PointInHalfSpace(normal,d,p);
    }



    bool FaceT::FaceInHalfSpace
        (
        const FaceT &face
        ) const
    {
        Vector3 normal=CalculateRawNormal();
        Vector3 p0=_obj->Point(GetPoint(0));
        float d=-(normal*p0);
        for( int v=0; v<face.N(); v++ )
        {
            Vector3 p=_obj->Point(face.GetPoint(v));
            float inside=p*normal+d;
            if( PointInHalfSpace(normal,d,p) ) continue;
            return false;
        }
        return true;
    }


    struct FaceTraditionalEmptyList:public FaceHelper
    {
    public:
        static FaceTraditionalEmptyList *fc_empty_list;
        static int autofreemem;
        FaceTraditionalEmptyList *next;

        void *operator new(size_t sz);
        void operator delete(void *ptr);
        FaceTraditionalEmptyList();
    };

    FaceTraditionalEmptyList *FaceTraditionalEmptyList::fc_empty_list=NULL;
    int FaceTraditionalEmptyList::autofreemem=0;


    struct FaceTraditionalEmptyListCleaner
    {
    public:
        ~FaceTraditionalEmptyListCleaner()
        {
            while (FaceTraditionalEmptyList::fc_empty_list)
            {
                FaceTraditionalEmptyList *ptr=FaceTraditionalEmptyList::fc_empty_list;
                FaceTraditionalEmptyList::fc_empty_list=ptr->next;
                free(ptr);
            }
        }
    };
    static FaceTraditionalEmptyListCleaner fccleaner;


    void *FaceTraditionalEmptyList::operator new(size_t sz)
    {
        if (fc_empty_list==NULL) return malloc(sizeof(FaceTraditionalEmptyList));
        else 
        {
            FaceTraditionalEmptyList *ptr=fc_empty_list;
            fc_empty_list=ptr->next;
            return ptr;
        }
    }

    void FaceTraditionalEmptyList::operator delete(void *ptr)
    {
        if (autofreemem==0) 
        {
            free(ptr);
            autofreemem=10;
        }
        else
        {
            FaceTraditionalEmptyList *ftptr=reinterpret_cast<FaceTraditionalEmptyList *>(ptr);
            ftptr->next=fc_empty_list;
            fc_empty_list=ftptr;
            if (fc_empty_list) autofreemem--;
        }
    }

    FaceTraditionalEmptyList::FaceTraditionalEmptyList()
    {
        for (int i=0;i<4;i++) memset(vs+i,0,sizeof(vs[i]));
        n=0;
        flags=0;
        next=NULL;  
    }

    FaceT::FaceT(const FaceT &other):_obj(other._obj),_faceIndex(-1),_helper(0)
    {
        ASSERT(other.IsValid());
        CreateUnbound(_obj);
        if (other._helper) *_helper=*other._helper;
        else _helper->SetFromFace(other._obj,other._faceIndex);
    }

    FaceT &FaceT::operator=(const FaceT &other)
    {
        if (_faceIndex==-1) {
          Release();
          ASSERT(other.IsValid());  
          if (_obj==NULL) _obj=other._obj;
          CreateUnbound(_obj);
          if (other._helper) *_helper=*other._helper;
          else _helper->SetFromFace(other._obj,other._faceIndex);
          return *this;
        } else {
            other.CopyFaceTo(*this);
        }
        return *this;
    }

    void FaceT::CreateUnbound(ObjectData *obj)
    {
        ASSERT(!IsValid());
        _obj=obj;
        _faceIndex=-1;
        _helper=new FaceTraditionalEmptyList;
    }

    void FaceT::Release()
    {
        if (_helper) delete static_cast<FaceTraditionalEmptyList *>(_helper);
        _faceIndex=-1;
        _helper=NULL;
    }

    void FaceT::CreateIn(ObjectData *obj)
    {
        ASSERT(!IsValid());
        _obj=obj;  
        _faceIndex=const_cast<ObjectData *>(_obj)->NewFace();
    }

    void FaceT::BindTo(ObjectData *obj, int index)
    {
        Release();
        _faceIndex=index;
        _obj=obj;
    }

    void FaceT::BindTo(const FaceT &other)
    {
        Release();  
        if (other._helper!=NULL)
            (*this)=other;
        else
        {
            _faceIndex=other._faceIndex;
            _obj=other._obj;
        }
    }

    void FaceT::SetTexture(RString name,int stage)
    {
        ASSERT(IsValid());
        if (_helper) _helper->vTexture=name;  
        else _obj->Face(_faceIndex).vTexture=name;
    }

    void FaceT::SetTexture(const char *name, int stage) {
        SetTexture(RString(name),stage);
    }

    void FaceT::SetMaterial(const char *name, int stage) {
        SetMaterial(RString(name),stage);
    }

    void FaceT::SetMaterial(RString name, int stage)  
    {
        ASSERT(IsValid());
        if (_helper) _helper->vMaterial=name;  
        else _obj->Face(_faceIndex).vMaterial=name;
    }

    void FaceT::SetVxCount(int n)
    {
        ASSERT(IsValid());
        ASSERT(n>=0 && n<5);
        if (_helper) _helper->n=n;
        else _obj->Face(_faceIndex).n=n;
    }

    void FaceT::SetPoint(int vs, int pt)
    {
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);
        if (_helper) _helper->vs[vs].point=pt;
        else _obj->Face(_faceIndex).vs[vs].point=pt;
    }

    void FaceT::SetNormal(int vs, int nr)
    {
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);
        if (_helper) _helper->vs[vs].normal=nr;
        else _obj->Face(_faceIndex).vs[vs].normal=nr;
    }

    ObjUVSet_Item *FaceT::FindUVSetItem(int stage, int vs, bool set)
    {  
        if (set)
        {
            if (stage==-1) return _helper->SetCurrentUV(vs);else return _helper->SetUV(stage,vs);
        }
        else
        {
            if (stage==-1) return _helper->GetCurrentUV(vs);else return _helper->SetUV(stage,vs);
        }
    }

    const ObjUVSet_Item *FaceT::FindUVSetItem(int stage, int vs) const
    {  
        if (stage==-1) return _helper->SetCurrentUV(vs);else return _helper->SetUV(stage,vs);
    }

    void FaceT::SetU(int vs, float tu, int stage)
    {  
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);  
        if (_helper) FindUVSetItem(stage,vs,true)->u=tu;
        else 
        {
            ObjUVSet *set=stage==-1?_obj->GetActiveUVSet():_obj->GetUVSet(stage);
            if (!set) set=_obj->AddUVSet(stage);
            ObjUVSet_Item uv=set->GetUV(_faceIndex,vs);
            uv.u=tu;
            set->SetUV(_faceIndex,vs,uv);
        }
    }

    void FaceT::SetV(int vs, float tv, int stage)
    {
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);  
        if (_helper) FindUVSetItem(stage,vs,true)->v=tv;
        else 
        {
            ObjUVSet *set=stage==-1?_obj->GetActiveUVSet():_obj->GetUVSet(stage);
            if (!set) set=_obj->AddUVSet(stage);
            ObjUVSet_Item uv=set->GetUV(_faceIndex,vs);
            uv.v=tv;
            set->SetUV(_faceIndex,vs,uv);
        }
    }

    void FaceT::SetUV(int vs, float tu, float tv, int stage)
    {
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);
        if (_helper) 
        {
            ObjUVSet_Item *itm=FindUVSetItem(stage,vs,true);
            itm->u=tu;
            itm->v=tv;
        }
        else 
        {
            ObjUVSet *set=stage==-1?_obj->GetActiveUVSet():_obj->GetUVSet(stage);
            if (!set) set=_obj->AddUVSet(stage);
            set->SetUV(_faceIndex,vs,ObjUVSet_Item(tu,tv));
        }
    }

    void FaceT::SetFlags(unsigned long flags)
    {
        ASSERT(IsValid());
        if (_helper) _helper->flags=flags;
        else _obj->Face(_faceIndex).flags=flags;
    }

    unsigned long FaceT::SetFlags(unsigned long zerobits, unsigned long invertbits)
    {
        ASSERT(IsValid());
        unsigned long &flags=_helper?_helper->flags:_obj->Face(_faceIndex).flags;
        flags=flags & ~zerobits ^ invertbits;
        return flags;
    }

    const RString &FaceT::GetTexture(int stage) const
    {
        ASSERT(IsValid());
        return _helper?_helper->vTexture:_obj->Face(_faceIndex).vTexture;
    }

    const RString &FaceT::GetMaterial(int stage) const
    {
        ASSERT(IsValid());
        return _helper?_helper->vMaterial:_obj->Face(_faceIndex).vMaterial;
    }

    int FaceT::GetVxCount() const 
    {
        ASSERT(IsValid());
        return _helper?_helper->n:_obj->Face(_faceIndex).n;
    }

    int FaceT::GetPoint(int vs) const 
    {
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);
        return _helper?_helper->vs[vs].point:_obj->Face(_faceIndex).vs[vs].point;
    }

    int FaceT::GetNormal(int vs) const 
    {
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);
        return _helper?_helper->vs[vs].normal:_obj->Face(_faceIndex).vs[vs].normal;
    }

    float FaceT::GetU(int vs, int stage) const 
    {
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);
        if (_helper)
        {
            const ObjUVSet_Item *itm=FindUVSetItem(stage,vs);
            return itm?itm->u:0;
        }
        else
        {
            ObjUVSet *set=stage==-1?_obj->GetActiveUVSet():_obj->GetUVSet(stage);
            return set?set->GetUV(_faceIndex,vs).u:0;
        }
    }

    float FaceT::GetV(int vs, int stage) const 
    {
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);
        if (_helper)
        {
            const ObjUVSet_Item *itm=FindUVSetItem(stage,vs);
            return itm?itm->v:0;
        }
        else
        {
            ObjUVSet *set=stage==-1?_obj->GetActiveUVSet():_obj->GetUVSet(stage);
            return set?set->GetUV(_faceIndex,vs).v:0;
        }
    }

    ObjUVSet_Item FaceT::GetUV(int vs, int stage) const
    {
        ASSERT(IsValid());
        ASSERT(vs>=0 && vs<4);
        if (_helper)
        {
            const ObjUVSet_Item *itm=FindUVSetItem(stage,vs);
            return itm?*itm:ObjUVSet_Item(0,0);
        }
        else
        {
            ObjUVSet *set=stage==-1?_obj->GetActiveUVSet():_obj->GetUVSet(stage);
            return set?set->GetUV(_faceIndex,vs):ObjUVSet_Item(0,0);
        }
    }

    unsigned long FaceT::GetFlags() const 
    {
        ASSERT(IsValid());
        return _helper?_helper->flags:_obj->Face(_faceIndex).flags;
    }

    class FunctorCopyUVS
    {
        int _from;
        int _to;
        int _vscount;
    public:
        FunctorCopyUVS(int from,int to, int vscount):_from(from),_to(to),_vscount(vscount) {}
        bool operator()(ObjUVSet *set) const
        {
            for (int i=0;i<_vscount;i++)
                set->SetUV(_to,i,set->GetUV(_from,i));
            return false;
        }
    };

    class FunctorCopyUVSObj
    {
        int _from;
        int _to;
        ObjectData *_toObj;
        int _vscount;
    public:
        FunctorCopyUVSObj(int from,int to, ObjectData *toObj, int vscount):_from(from),_to(to),_toObj(toObj),_vscount(vscount) {}
        bool operator()(ObjUVSet *set) const
        {
            ObjUVSet *toSet=_toObj->GetUVSet(set->GetIndex());
            if (!toSet) toSet=_toObj->AddUVSet(set->GetIndex());
            for (int i=0;i<_vscount;i++)
                toSet->SetUV(_to,i,set->GetUV(_from,i));
            return false;
        }
    };


    void FaceT::CopyFaceTo(int index) const
    {
        ASSERT(IsValid());
        FaceTraditional &face=_helper?*_helper:_obj->Face(_faceIndex);
        _obj->Face(index)=face;
        if (_helper)
        {
            for (int i=0;i<_helper->maxStages;i++) if (_helper->_stages[i]!=-1)
            {
                ObjUVSet *uvset=_obj->GetUVSet(_helper->_stages[i]);
                if (!uvset) uvset=_obj->AddUVSet(_helper->_stages[i]);
                for (int j=0;j<_helper->n;j++)
                    uvset->SetUV(index,j,_helper->uv[i][j]);
            }
        }
        else
        {
            _obj->EnumUVSets(FunctorCopyUVS(_faceIndex,index,N()));
        }
    }

    void FaceT::CopyFaceTo(FaceT &other) const
    {
        ASSERT(IsValid());
        if (_helper && other._helper) 
        {
            *other._helper=*_helper;
        }
        else if (other._helper)
        {    
            other._helper->SetFromFace(_obj,_faceIndex);
        }
        else if (_obj==other._obj)
        {
            CopyFaceTo(other._faceIndex);
        }
        else 
        {
            FaceTraditional &face=_helper?*_helper:_obj->Face(_faceIndex);
            other._obj->Face(other._faceIndex)=face;
            if (_helper)
            {
                for (int i=0;i<_helper->maxStages;i++) if (_helper->_stages[i]!=-1)
                {
                    ObjUVSet *uvset=other._obj->GetUVSet(_helper->_stages[i]);
                    if (!uvset) uvset=other._obj->AddUVSet(_helper->_stages[i]);
                    for (int j=0;j<_helper->n;j++)
                        uvset->SetUV(other._faceIndex,j,_helper->uv[i][j]);
                }
            }
            else
            {
                _obj->EnumUVSets(FunctorCopyUVSObj(_faceIndex,other._faceIndex,other._obj,N()));
            }

        }
    }

    int FaceT::GetPointInfoSize() const 
    {
        if (_helper) return sizeof(FaceHelper)-sizeof(FaceTraditional);
        else
        {  
            FaceTraditional *p=NULL;
            ASSERT(IsValid());
            return sizeof(p->vs[0])+12*_obj->GetUVSetCount()+4;
        }
    }

    class FunctorSaveFaceStage
    {
        char **ptr;
        int face;
        int point;
    public:
        FunctorSaveFaceStage(char **ptr, int face, int point):ptr(ptr),face(face),point(point) {}
        bool operator()(const ObjUVSet *set) const
        {
            char *c=*ptr;
            *(int *)c=set->GetIndex();c+=sizeof(int);
            *(ObjUVSet_Item *)c=set->GetUV(face,point);c+=sizeof(ObjUVSet_Item);
            *ptr=c;
            return false;
        }
    };

    void FaceT::SavePointInfo(int point, void *nfostruct) const
    {
        ASSERT(IsValid());
        ASSERT(point>=0 && point<4);
        char *z=(char *)nfostruct;
        memcpy(z,_helper?_helper->vs+point:_obj->Face(_faceIndex).vs+point,sizeof(_helper->vs[0]));
        z+=sizeof(_helper->vs[0]);
        if (_helper)
        {
            for (int i=0;i<_helper->maxStages;i++)
                if (_helper->_stages[i]!=-1)
                {
                    *(int *)z=_helper->_stages[i];z+=sizeof(_helper->_stages[i]);
                    *(ObjUVSet_Item *)z=_helper->uv[i][point];z+=sizeof(ObjUVSet_Item);        
                }
        }
        else
        {
            _obj->EnumUVSets(FunctorSaveFaceStage(&z,_faceIndex,point));
        }
        *(int *)z=-1;z+=sizeof(int);
    }

    void FaceT::LoadPointInfo(int point, const void *nfostruct)
    {
        ASSERT(IsValid());
        ASSERT(point>=0 && point<4);
        char *z=(char *)nfostruct;
        memcpy(_helper?_helper->vs+point:_obj->Face(_faceIndex).vs+point,z,sizeof(_helper->vs[0]));
        z+=sizeof(_helper->vs[0]);
        if (_helper)
        {
            int stage=*(int *)z;z+=sizeof(int);
            while (stage!=-1)
            {
                ObjUVSet_Item *item=FindUVSetItem(stage,point,true);
                *item=*(ObjUVSet_Item *)z;z+=sizeof(ObjUVSet_Item);
                stage=*(int *)z;z+=sizeof(int);
            }
        }
        else
        {
            int stage=*(int *)z;z+=sizeof(int);
            while (stage!=-1)
            {
                ObjUVSet_Item itm=*(ObjUVSet_Item *)z;z+=sizeof(ObjUVSet_Item);
                ObjUVSet *uv=_obj->GetUVSet(stage);
                if (!uv) uv=_obj->AddUVSet(stage);
                uv->SetUV(_faceIndex,point,itm);
                stage=*(int *)z;z+=sizeof(int);
            }
        }
    }

    bool FaceT::Check(bool repair/*=true*/, bool removeInvalid/*=false*/, float ptDistance/*=-1*/, bool reportRepaired/*=false*/)
    {
        int n=N();
        float normlen=CalculateNormal().SquareSize();
        if (n<3 || n>4 || normlen<1.0E-5 || !_finite(normlen))
        {
            if (removeInvalid)
            {
                if (_helper=0) {_obj->DeleteFace(_faceIndex);return !reportRepaired;}
                return false;
            }
            else 
                return false;  
        }
        float dist2=ptDistance*ptDistance;
        bool repaired=false;
        for (int i=0;i<n;i++)      
            for (int j=i+1;j<n;j++)
            {
                int pt1=GetPoint(i),pt2=GetPoint(j);      
                float distance=_obj->Point(pt1).Distance2(_obj->Point(pt2));      
                if (pt1==pt2 || (ptDistance>=0 && 
                    distance<=dist2) || !_finite(distance))
                {
                    if (!repair)
                    {
                        if (removeInvalid)
                        {
                            if (_helper=0) {_obj->DeleteFace(_faceIndex);return !reportRepaired;}
                            return false;
                        }
                        else 
                            return false;
                    }
                    else
                    {
                        if (n<4 || j!=i+1 && !(i==0 && j==n-1)) //cannot repair
                        {
                            if (removeInvalid)
                            {
                                if (_helper=0) {_obj->DeleteFace(_faceIndex);return !reportRepaired;}
                                return false;
                            }
                            else 
                                return false;
                        }
                        else
                        {
                            for (int k=j+1;k<n;k++)
                                CopyPointInfo(k-1,k);
                            SetN(--n);
                            j=i;
                            repaired=true;
                        }
                    }

                }
            }
            return  !(reportRepaired && repaired);
    }

    static inline float UVWrap(float a)
    {
        a=a-floor(a);
        if (a<0) a+=1.0f;
        return a;
    }

    bool FaceT::HasSameMappingOnEdge(FaceT &other)
    {
        int vx[4];
        if (!GetEdgeIndices(other,vx[0],vx[1],vx[2],vx[3])) return false;
        return 
            /*fabs(UVWrap(GetU(vx[0]))-UVWrap(other.GetU(vx[2])))<0.01 && 
            fabs(UVWrap(GetV(vx[0]))-UVWrap(other.GetV(vx[2])))<0.01 && 
            fabs(UVWrap(GetU(vx[1]))-UVWrap(other.GetU(vx[3])))<0.01 && 
            fabs(UVWrap(GetV(vx[1]))-UVWrap(other.GetV(vx[3])))<0.01;*/
            fabs((GetU(vx[0]))-(other.GetU(vx[2])))<0.01 && 
            fabs((GetV(vx[0]))-(other.GetV(vx[2])))<0.01 && 
            fabs((GetU(vx[1]))-(other.GetU(vx[3])))<0.01 && 
            fabs((GetV(vx[1]))-(other.GetV(vx[3])))<0.01;
    }

    bool FaceT::ContinueMappingOnEdge(FaceT &other)
    {
        int vx[4];
        if (!GetEdgeIndices(other,vx[0],vx[1],vx[2],vx[3])) return false;  

        float diffU_0 = other.GetU(vx[2]) - GetU(vx[0]);
        float diffV_0 = other.GetV(vx[2]) - GetV(vx[0]);
        float diffU_1 = other.GetU(vx[3]) - GetU(vx[1]);
        float diffV_1 = other.GetV(vx[3]) - GetV(vx[1]);

        if (fabs(diffU_0 - diffU_1) > 0.01 || fabs(diffV_0 - diffV_1) > 0.01)
            return false;

        if ((fabs(diffU_0) - floor(fabs(diffU_0)) > 0.01 && ceil(fabs(diffU_0)) - fabs(diffU_0) > 0.01 ) || 
            (fabs(diffV_0) - floor(fabs(diffV_0)) > 0.01 && ceil(fabs(diffV_0)) - fabs(diffV_0) > 0.01 ))
            return false;


        for(int i = 0; i < N(); i++ )
        {
            SetU(i, GetU(i) + diffU_0);
            SetV(i, GetV(i) + diffV_0);    
        }

        return true;
    }

    bool FaceT::RepairDegenerated()
    {
        if (N()==4)
            if (GetPoint(0)==GetPoint(2) || GetPoint(1)==GetPoint(3)) return false;
        int j=N()-1;
        for (int i=0;i<N();i++)
        {
            if (GetPoint(j)==GetPoint(i))
                if (N()<4) return false;
                else
                {
                    for (int k=i+1;k<N();k++)
                        SetPoint(k-1,GetPoint(k));
                    SetN(N()-1);
                    i--;
                }
            else
                j=i;
        }
        return true;
    }

    bool FaceT::SetActiveStage(int stage)
    {
        if (_helper) {_helper->SetActiveStage(stage);return true;}
        else {return _obj->SetActiveUVSet(stage);}
    }

    int FaceT::GetActiveStage() const
    {
        if (_helper) return _helper->GetActiveStage();
        else return _obj->GetActiveUVSet()->GetIndex();
    }

    FaceT::FaceToFaceLocation FaceT::FaceLocation(const Vector3 &normal, float d) const
    {
        int side=0;
        bool first=true;
        for (int i=0;i<N();i++)
        {
            const Vector3 &vc=GetPointVector<Vector3>(i);
            int x=PointInHalfSpaceEx(normal,d,vc);
            if (x==0) continue;
            if (first) {side=x;first=false;}
            else
                if (x!=side) return fIntermediate;
        }
        if (side>0) return fBelow;
        if (side<0) return fAbove;
        return fIntermediate;
    }

    FaceT::FaceToFaceLocation FaceT::FaceLocation(const FaceT &other) const
    {
        Vector3 normal=other.CalculateRawNormal();
        float d=-other.GetPointVector<Vector3>(0)*normal;
        return FaceLocation(normal,d);
    }

    FaceT::FaceToFaceLocation FaceT::FaceLocation2(const FaceT &other) const
    {
        FaceT::FaceToFaceLocation test1=FaceLocation(other);
        FaceT::FaceToFaceLocation test2=other.FaceLocation(*this);

        if (test1==test2)
        {
            if (test1==fAbove) return fAboveBoth;
            if (test1==fBelow) return fBelowBoth;
            return fCross;
        }

        if (test1==fAbove) return fAbove;
        if (test1==fBelow) return fBelow;
        if (test2==fAbove) return fBelow;
        if (test2==fBelow) return fAbove;
        return fCross;
    }

    bool FaceT::HasSomeMapping() const
    {
        ObjUVSet_Item uv = GetUV(0);
        for (int i = 1; i<N();i++) 
        {
            ObjUVSet_Item uv2 = GetUV(i);
            if (fabs(uv.u - uv2.u)>1e-10 || fabs(uv.v - uv2.v)>1e-10)
                return true;
        }
        return false;
    }

    void FaceT::ClearMapping()
    {
        for (int i = 0; i<N();i++) 
            SetUV(i,0,0);
    }

    /**
     Ax + t*xa = Bx + s*xb
     Ay + t*ya = By + s*yb

     t*xa - s*xb = Bx - Ax
     t*ya - s*yb = By - Ay

     xa     -xb
     ya     -yb

     (Bx-Ax) -xb
     (By-Ay) -yb

     xa     (Bx-Ax)
     ya     (By-Ay)
     */

    //calculates intersections of two lines in UV space
    static bool Intersection(const ObjUVSet_Item &l1,
        const ObjUVSet_Item &l2,
        const ObjUVSet_Item &r1,
        const ObjUVSet_Item &r2) 
    {

        ObjUVSet_Item dl(l2.u - l1.u,l2.v - l1.v);
        ObjUVSet_Item dr(r2.u - r1.u,r2.v - r1.v);
        float d =  dl.u * (-dr.v) - dl.v * (-dr.u);
        float rsu = r1.u - l1.u;
        float rsv = r1.v - l1.v;
        if (fabs(d) < 0.00000000001f) return false;

        float dx = rsu * (-dr.v) - rsv * (-dr.u);
        float dy = dl.u * rsv - dl.v * rsu;
        float x = dx/d;
        float y = dy/d;
        return (x>0.0001f && x<0.9999f && y>0.001f && y<0.9999f);

    }

    //tests collision by testing points of what face whether are inside face against
    static bool TestCollisionPart1(const FaceT &what, const FaceT &against) {

        ObjUVSet_Item center(0,0);
        for (int i = 0; i < what.N(); i++) {
            center.u += what.GetU(i);
            center.v += what.GetV(i);
            if (against.IsUVCoordInside(what.GetUV(i))) return true;
        }
        center.u /= what.N();
        center.v /= what.N();

        return against.IsUVCoordInside(center);
    }

    //tests collision by finding intersections on edges
    static bool TestCollisionPart2(const FaceT &what, const FaceT &against) {

        ObjUVSet_Item l1 = what.GetUV(what.N() - 1);
        for (int i = 0; i < what.N(); i++) {
            ObjUVSet_Item l2 = what.GetUV(i);
            ObjUVSet_Item r1 = against.GetUV(against.N() - 1);
            for (int j = 0; j < against.N(); j++) {
                ObjUVSet_Item r2 = what.GetUV(j);
                if (Intersection(l1,l2,r1,r2))
                    return true;
                r1 = r2;
            }
            l1 = l2;
        }

        return false;    
    }


    bool FaceT::IsUVCollision(const FaceT &other) const {

        return TestCollisionPart2(other,*this)
            || TestCollisionPart1(*this,other)
            || TestCollisionPart1(other,*this);

    }

    bool FaceT::IsUVCoordInside(const ObjUVSet_Item &uv) const {

        int side = 0;
        ObjUVSet_Item from = GetUV(N() - 1);
        for (int i = 0 ; i< N(); i++) {
            ObjUVSet_Item to = GetUV(i);

            float du = to.u - from.u;
            float dv = to.v - from.v;
            float tu = uv.u - from.u;
            float tv = uv.v - from.v;
            float pos = du * tv - dv * tu;
            int cside = (pos > 0.0001) - (pos < -0.0001);
            if (!cside) return false;
            if (!side) side = cside;
            else if (side != cside) return false;
            from = to;
        }
        return true;
    }

    float FaceT::CalcAngleAtPoint(int vs) const {

        std::pair<float,float> res = CalcAngleAtPointRaw(vs);
        return atan2(res.first,res.second);
    }
    
    std::pair<float,float> FaceT::CalcAngleAtPointRaw(int vs) const {
        Vector3 normal = CalculateRawNormal();
        int vs1 = vs - 1;
        int vs2 = vs + 1;
        if (vs1 < 0) vs1+=N();
        if (vs2 >= N() ) vs2-=N();
        Vector3 a = (GetPointVector<Vector3>(vs1) - GetPointVector<Vector3>(vs)).Normalized();
        Vector3 b = (GetPointVector<Vector3>(vs2) - GetPointVector<Vector3>(vs)).Normalized();
        Vector3 c = b.CrossProduct(a);
        float dir = c.DotProduct(normal);
        return std::pair<float,float>(
                c.Size() * (dir<0?-1.0f:1.0f),
                a.DotProduct(b));
    }

}