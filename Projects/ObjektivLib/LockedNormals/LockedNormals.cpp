// LockedNormal.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MassProcess.h"
#include "../ObjToolsMatLib.h"

class LockedNormal:public MassProcess
  {
  public:
    bool ProcessFile(const char *filename);
  };




int main(int argc, char* argv[])
{
bool recursive=false;
const char *folder;
if (argc<2)
  {
  puts("Parameters: [/s] folder\n\n/s          process recursive\nfolder      folder to process");
  return -1;
  }
if (stricmp(argv[1], "/s")==0)
  {
  if (argc<3)
    {
    puts("Missing parameter");
    return -1;
    }
  recursive=true;
  folder=argv[2];
  }
else
  folder=argv[1];
LockedNormal mass;
mass.ProcessFolders(folder,"*.p3d",recursive);
return 0;
}


bool LockedNormal::ProcessFile(const char *filename)
  {
  LODObject lod;
  if (lod.Load(filename,NULL,NULL)!=0)
    fprintf(stderr,"Error opening: %s\n",filename);
  else
    {
    for (int level=0;level<lod.NLevels();level++)
      {
      float resol=lod.Resolution(level);
      if (LOD_SHADOW_MIN<=resol && LOD_SHADOW_MAX>=resol)
        {
        ObjectData *obj=lod.Level(level);
        RString vxs;
        for (int i=0;i<obj->NPoints();i++)
          {
          PosT &ps=obj->Point(i);
          if (ps.flags & POINT_SPECIAL_LOCKNORMAL) 
            {
            char buff[50];
            vxs=vxs+" "+itoa(i,buff,10);
            }
          }
        if (vxs.GetLength()) 
          printf("Locked normal on vertices: %s LOD: %g File: %s\n",vxs.Data(),lod.Resolution(level),filename);
        }
      }
    fprintf(stderr,"Processed: %s\n",filename);
    }
  return true;
  }