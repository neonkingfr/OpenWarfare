#pragma once
#include "objectdata.h"

namespace ObjektivLib {


struct UnigueMaterial
{
  RString _texture; //< path to texture
  RString _material; //< path to rvmat 

  bool operator==(const UnigueMaterial& src) const {return _texture == src._texture && _material == src._material;};  
 ClassIsMovable(UnigueMaterial);
};


class TextureSet : public RefCount
{
protected:
  FindArray<UnigueMaterial> _from;
  FindArray<UnigueMaterial> _to;
  AutoArray<float> _UVshift;
public:

  const FindArray<UnigueMaterial>& GetFrom() const {return _from;};
  const FindArray<UnigueMaterial>& GetTo() const {return _to;};
  bool operator==(const TextureSet& set) const;  
  ClassIsMovable(TextureSet)
};

struct CMFMData {
  Pathname _model;  //< path to the model
  Matrix4 _trans;   //< model will be transformed by this transformation
  RString _selection; //< name of selection, that will be copied   
  RString _resSelection; //< select copied content in result model by the selection with this name... 
  const TextureSet * _textureSet;
  ClassIsMovableZeroed(CMFMData);
} ;


class ObjToolJoinProxy :  public ObjectData
{
protected:
  /** Finds all polygons mapped by the given texture.
    * @param polygons array of polygons. Polygons are defined by position of the border vertices.
    * @param text name of the texture
    * @param workOn if not null search only faces selected by this selection.
    */
  void GetPolygonsWithTexture(AutoArray<AutoArray<Vector3> >& polygons, const RString& text, Selection * workOn) const;
public:
  /** Function creates model by copying proxy models into it
   * @param out result model
   * @param list  array of pointers to named selections representing proxies
   * @size number of proxies in list
   * @resTrans (out) reference transformation to which proxy was saved.
   * @shiftResult if false resTrans is identity...
   * @makeSelections if true in the result model a content of each proxy will in a selection named by the proxy filenname.
   */
  void CreateModelFromProxies(LODObject& out, 
    NamedSelection ** list, 
    int size, 
    Matrix4& resTrans, 
    const char * basePath,  
    bool shiftResult = true,
    bool makeSelections = false) const;  

  /**Enumerates selected proxies;  
   * @param sel selection to be searched in
   * @param list array reserved for pointers to NamedObject contains selection of proxy. 
   *  If NULL specified, return value will contain count of proxies  
   * @return count of proxies matched.
   */
  int EnumProxiesFromSelection(const Selection * sel, NamedSelection ** list)  const;  

  /** Divides proxies into groups and try to build groups so that it has between minMembers
   * and maxMembers number of members.
   * @param groupName the name of created selections numbers are added to the end...
   * @param minMembers minimal number of members (but at result it can be low, this is only recommended value)
   * @param maxMembers maximal number of members
   * @return number of created groups
   */
  int CreateProxiesGroup(const char * groupName,int minMembers, int maxMembers);

  /** Creates proxies from group of proxies. This function is quite high level do not use if you do not understand its functionality.
   * @param groupPrefix the prefix with identifies group name selection 
   * @param basePath the path to the project root
   */
  void ReplaceGroupsByProxies(AutoArray<RString>& created,const char * groupPrefix,const char * basePath);
  
  /** Finds face with vertexes approximately on the same position as points. The vertexes must be also ordered in same order as points.
   * @param points array of points
   * @returns id of found face or -1, if not found
   */
  int FindFace(AutoArray<Vector3>& points) const;

  /** Creates LODObject from group of models. It can also copy only a part of each model defined by selection.
    * All LODs are copied. 
    * @param out result model
    * @param models array with model specifications.
    */
  static void CreateModelFromModels(LODObject& out, const AutoArray<CMFMData>& models);   
  static void CreateModelFromModelsFillLODs(LODObject& out, const AutoArray<CMFMData>& models, bool positive = true); 
  static void CreateModelFromModels(LODObject& out, const AutoArray<CMFMData>& models, const AutoArray<float>& LODs); 

  
  /** Compares faces with the pattern object and defines how material was changed.
    * @param from materials in this object
    * @param to materials in the pattern object
    * @param pattern pattern object
    */
  void FindMaterialChanges(FindArray<UnigueMaterial>& from,FindArray<UnigueMaterial>& to, const ObjectData&  patern) const;

  /** Replaces materials. 
    * @param from materials in this object
    * @param to new materials. Materials defined in from will be replaced by this materials.
    * @param sel if not NULL work only on this selection
    */
  void ChangeMaterials(const FindArray<UnigueMaterial>& from, const FindArray<UnigueMaterial>& to, Selection * sel); 

  /** Saves info about instances represented by proxies into param file. Very house specific function.
    * It also finds material changes by comparing it with pattern.
    * @param paramFile path to the result param file
    * @param pattern object with materials
    * @param list array of pointers to named selections representing proxies
    * @param size number of proxies in list
    * @param basePath project root
    */
  void SaveInstParamFile(const char * paramFile, const ObjectData& materialPatern, NamedSelection ** list, int size, const char * basePath) const;

  /** Saves info about instances of breakable house parts. Very house specific function.
  * It reads file generated by SaveInstParamFile and add info about parts. The parts are defined by the glue system.
  * @param paramFileIn path to the param file produced by SaveInstParamFile
  * @param paramFileOut path to the result param file
  * @param basePath project root
  */
  static void SavePartInstParamFile(const char * paramFileIn, const char * paramFileOut, const char * basePath);

  /** It creates subparts LOD for breakable house according to parts specification created by SavePartInstParamFile. 
  * Very house specific function.
  * @param paramFileIn path to the param file produced by SavePartInstParamFile
  * @param partsDir directory where parts will be saved  
  * @param basePath project root
  */
  void BreakableHouse(AutoArray<RString>& created, const char * paramFileIn, const char * partsDir, const char * basePath);

  static void UnbreakableHouse(LODObject& out,const char * paramFileIn, const char * basePath);

  /** Centers models referenced by proxies and shifts proxies in level to compensate the centralization.
  * @param list array of pointers to named selections representing proxies
  * @param size number of proxies in list
  * @param basePath project root
  */
  void CenterProxies(NamedSelection ** list, int size, const char * basePath);

  /** Optimalizes shadow lod created from more parts. It joins several components into one and retesalate polygons. 
   * In ideal case it will reduce polycount. Only two components with open edges lying very close to each other will be joined 
   * (their open edges will be replaced by shared edge)
   * @param splitPolygons - creates open edges on components (only on double faces). 
   */
  void ShadowLod(bool splitPolygons);

protected:
  /** Split polygons in selection, used by ShadowLod. 
  */
  void SplitPolygons(); 

  /** Returns true if edge is open or is connecte to exactly two faces with opposite normals. */
  bool IsOpenOrSuperSharp(int pt0, int pt1);

  /*bool AreAllDoubleFaces(Selection &sel);*/
};


}
