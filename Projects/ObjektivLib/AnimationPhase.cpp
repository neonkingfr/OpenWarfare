#include "common.hpp"
#include "AnimationPhase.h"
#include "ObjectData.h"


namespace ObjektivLib {

void AnimationPhase::Redefine( ObjectData *data )
  {
  // import external data
  Validate();
  if( N()!=data->NPoints() ) return;
  for( int i=0; i<N(); i++ )
    {
    (*this)[i]=data->Point(i);
    }
  }

}