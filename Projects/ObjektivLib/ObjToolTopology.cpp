#include "common.hpp"
#include "ObjToolTopology.h"
#include "ObjToolClipboard.h"
#include "ObjToolCheck.h"
#include "Edges.h"
#include "GlobalFunctions.h"
#include <malloc.h>
#include <el/BTree/Btree.h>


namespace ObjektivLib {


struct Edge
{
  int a,b;
  Edge( int v1, int v2 )
  {a=v1,b=v2;}
  Edge()
  {}
  int Compare( const Edge &with ) const
  {
    if( a!=with.a ) return a-with.a;
    if( b!=with.b ) return b-with.b;
    return 0;
  }
  bool operator ==  ( const Edge &with ) const 
  {return Compare(with)==0;}

 ClassIsSimple(Edge);
};

#ifndef VERIFY
#ifdef _DEBUG
#define VERIFY assert
#else
#define VERIFY
#endif
#endif









class Edges: public FindArray<Edge>
{
public:
  double HullVolume( const ObjectData *obj ) const; //  calculate volume of convex hull
  int HullNPoints( const ObjectData *obj ) const; //  calculate volume of convex hull

};










/*
static void CheckMinMax( Vector3 &min, Vector3 &max, const Vector3 &val )
{
if( min.x>val.x ) min.x=val.x;
if( min.y>val.y ) min.y=val.y;
if( min.z>val.z ) min.z=val.z;
if( max.x<val.x ) max.x=val.x;
if( max.y<val.y ) max.y=val.y;
if( max.z<val.z ) max.z=val.z;
}
*/

int Edges::HullNPoints( const ObjectData *obj ) const
{
  FindArray<int> points;
  for( int i=0; i<Size(); i++ )
  {
    const Edge &edge=(*this)[i];
    points.AddUnique(edge.a);
    points.AddUnique(edge.b);
  }
  return points.Size();
}










double Edges::HullVolume( const ObjectData *obj ) const
{
  //  calculate volume of convex hull
  Vector3 mmin(+1e10,+1e10,+1e10),mmax(-1e10,-1e10,-1e10);
  for( int i=0; i<Size(); i++ )
  {
    const Edge &edge=(*this)[i];
    const PosT &pA=obj->Point(edge.a);
    //Vector3 pA=obj->Point(edge.a);
    const PosT &pB=obj->Point(edge.b);
    CheckMinMax(mmin,mmax,pA);
    CheckMinMax(mmin,mmax,pB);
  }
  //return (max.x-min.x)*(max.y-min.y)*(max.z-min.z);
  return (mmax-mmin).SquareSize();
}



void ObjToolTopology::CheckClosedTopology()
{
  // check all edges for topology closiness
  int i;

  ClearSelection();
  for( i=0; i<NFaces(); i++ )
  {
    FaceT face(this,i);
    for( int j=0,last=face.N()-1; j<face.N(); last=j,j++ )
    {
      int iCurr=face.GetPoint(j);
      int iLast=face.GetPoint(last);
      int neighbourghs=0;
      for( int vs=0; vs<NFaces(); vs++ )
      {
        FaceT other(this,vs);
        if( other.ContainsEdge(iCurr,iLast) ) neighbourghs++;
        if( other.ContainsEdge(iLast,iCurr) ) neighbourghs--;
      }
      if( neighbourghs )
      {
        PointSelect(iCurr);   
        PointSelect(iLast);   
      }
    }
    //FaceSelect(i,open);
  }
  //SelectPointsFromFaces();
}

void ObjToolTopology::CloseTopology()
{
  // check all edges for topology closiness
  int i;

  ClearSelection();

#define DO_REPEAT 0
#if DO_REPEAT
Repeat:
#endif
  Edges openEdges;
  for( i=0; i<NFaces(); i++ )
  {
    FaceT face(this,i);
    bool open=false;
    for( int j=0,last=face.N()-1; j<face.N(); last=j,j++ )
    {
      int iCurr=face.GetPoint(j);
      int iLast=face.GetPoint(last);
      int neighbourghs=0;
      for( int vs=0; vs<NFaces(); vs++ )
      {
        FaceT other(this,vs);
        if( other.ContainsEdge(iCurr,iLast) ) neighbourghs++;
        if( other.ContainsEdge(iLast,iCurr) ) neighbourghs--;
      }
      if( neighbourghs )
      {
        PointSelect(iCurr);
        PointSelect(iLast);
      }
      while( neighbourghs>0 )
      {
        openEdges.Add(Edge(iLast,iCurr));
        neighbourghs--;
      }
      while( neighbourghs<0 )
      {
        openEdges.Add(Edge(iCurr,iLast));
        neighbourghs++;
      }
    }
  }
  // use list of all open edges
  // scan for connected components
  // and triangulate them
  for(;;)
  {
    bool someFace=false;
    int sV1=0,sV2=0,sV3=0;
    double minArea2=1e30;
    for( int s=0; s<openEdges.Size(); s++ )
    {
      // there must be at least two edges to produce face
      int v1=openEdges[s].a;
      int v2=openEdges[s].b;
      // we have to create a face with edge (v1,v2)
      for( int e=s+1; e<openEdges.Size(); e++ )
      {
        bool case1=false;
        bool case2=false;
        if( openEdges[e].a==v2 && openEdges[e].b!=v1 ) case1=true;
        if( openEdges[e].a!=v2 && openEdges[e].b==v1 ) case2=true;
        if( case1 || case2 )
        {
          int v3=case1 ? openEdges[e].b : openEdges[e].a;
          // avoid co-linear faces
          // we will try to create a new edge f1,f2
          //if( FindEdge(f1,f2) ) continue; // no edge
          Vector3 p1=(Vector3)Point(v1)-(Vector3)Point(v3);
          Vector3 p2=(Vector3)Point(v2)-(Vector3)Point(v3);
          Vector3 p3=(Vector3)Point(v2)-(Vector3)Point(v1);
          float cosFi=p1*p2/(p1.Size()*p2.Size());
          if( fabs(cosFi)>=0.999 ) continue; // co-linear points
          //double area2=p1.CrossProduct(p2).SquareSize();

          // this face will change the number of close edges:
          bool sureYes=!someFace;
          bool sureNo=false;
#if 1
          FaceT face(this);
          face.SetN(3);
          face.SetPoint(0,v1);
          face.SetPoint(1,v2);
          face.SetPoint(2,v3);
          double area2=face.CalculateArea();
          //area2=face.CalculatePerimeter(this);
          if( !sureYes && !sureNo )
          {
            if( minArea2>area2 ) sureYes=true;
            else if( minArea2<area2 ) sureNo=true;
          }
#endif
          if( sureYes )
          {
            sV1=v1;
            sV2=v2;
            sV3=v3;
            minArea2=area2;
            someFace=true;
          }
        } // if( case )
      } // for(e)
    } // for(s)
    if( !someFace ) break; // no face found
    int index=NFaces();
    FaceT face(this,NewFace());
    face.SetN(3);
    face.SetPoint(0,sV1);
    face.SetPoint(1,sV2);
    face.SetPoint(2,sV3);
    for (int tmpi=0;tmpi<3;tmpi++) face.SetNormal(tmpi,0);
    FaceSelect(index);
    // (sV1,sV2), (sV2,sV3), (sV3,sV1) are added negNeighbourghs
    // this can be done as: either remove original pair or add reversed pair
    int f;
    if( (f=openEdges.Find(Edge(sV1,sV2)))<0 ) openEdges.Add(Edge(sV2,sV1));
    else openEdges.Delete(f);
    if( (f=openEdges.Find(Edge(sV2,sV3)))<0 ) openEdges.Add(Edge(sV3,sV2));
    else openEdges.Delete(f);
    if( (f=openEdges.Find(Edge(sV3,sV1)))<0 ) openEdges.Add(Edge(sV1,sV3));
    else openEdges.Delete(f);
#if DO_REPEAT
    goto Repeat;
#endif
  }
  //  RecalcNormals();
  Squarize(false);
  RecalcNormals();
}


int ObjToolTopology::CalculateInsideFaces (int faceIndex, bool onlySelected) const
{
  const FaceT face(this,faceIndex);
  Vector3 normal=face.CalculateNormal();
  Vector3 p0=Point(face.GetPoint(0));
  float d=-(normal*p0);
  int count=0;
  for( int i=0; i<NFaces(); i++ ) if( i!=faceIndex )
  {
    const FaceT check(this,i);
    for( int v=0; v<check.N(); v++ )
    {
      Vector3 p=Point(check.GetPoint(v));
      float inside=p*normal+d;
      if( inside<=0 ) count++;
    }
  }
  return count;
}

void ObjToolTopology::SelectionToConvexComponents()
{
  // merge in single components
  SRef<ObjectData> result=new ObjectData();
  Temp<bool> scanned(NFaces());
  Temp<bool> inSelection(NFaces());
  Temp<bool> convexFaces(NFaces());
  Temp<bool> convexPoints(NPoints());
  int i;
  int nConvexPoints=0;
  // scan only for selected faces
  for( i=0; i<NFaces(); i++ ) inSelection[i]=FaceSelected(i);
  for( i=0; i<NFaces(); i++ ) scanned[i]=!FaceSelected(i);
  for( i=0; i<NPoints(); i++ ) convexPoints[i]=false;
  for( i=0; i<NFaces(); i++ ) convexFaces[i]=false;
  //bool convex=false;
  bool allScanned;
  int nParts=0;
  for(;;)
  {
    allScanned=true;
    // find last non-scanned face
    for(;;)
    { // scan all faces to see if we can add something
      bool noFace=true;
      for( i=NFaces(); --i>=0; ) if( !scanned[i] )
      {
        allScanned=false;
        // check if the component will remain convex
        // after add face 'i'
        // i.e. check if all faces until now are inside 
        FaceT faceI(this,i);
        bool neighbourgh=false;
        bool convex=true;
        if( nConvexPoints>=2 )
        {
          if( faceI.ContainsPoints(convexPoints)<2 ) continue;
        }
        else
        {
          neighbourgh=true;
        }
        // check if there is come common egde between faceI and convex faces
        int f;
        for( f=0; f<NFaces(); f++ ) if( convexFaces[f] )
        {
          FaceT faceF(this,f);
          if( faceI.IsNeighbourgh(faceF) ) neighbourgh=true;
          if
            (
            !faceI.FaceInHalfSpace(faceF) ||
            !faceF.FaceInHalfSpace(faceI)
            )
          {
            convex=false;
            break;
          }
        }
        if( convex && neighbourgh )
        {
          scanned[i]=true;
          convexFaces[i]=true;
          int v;
          for( v=0; v<faceI.N(); v++ )
          {
            convexPoints[faceI.GetPoint(v)]=true;
            nConvexPoints++;
          }
          noFace=false;
        }
      }
      if( noFace ) break; // all faces scanned - nothing to add
    }
    // copy component we have
    if( allScanned ) break; // no more components
    nParts++;
    ClearSelection();
    int f;
    for( f=0; f<NFaces(); f++ )
    {
      FaceSelect(f,convexFaces[f]);
    }
    SelectPointsFromSelectedFaces(-1,SelSet);
#if 1
    // save current selection to a temporary object
    SRef<ObjectData> temp=new ObjectData(*this);
    temp->GetTool<ObjToolClipboard>().ExtractSelection();

    // paste in the saved selection
    result->Merge(*temp);

#else
    static int convex=0;
    WString name;
    name.Sprintf("Convex%02d",++convex);
    SaveNamedSel(name);
#endif

    for( i=0; i<NFaces(); i++ ) convexFaces[i]=false;
    for( i=0; i<NPoints(); i++ ) convexPoints[i]=false;
    nConvexPoints=0;
  }

  if( nParts>1 )
  {
    // it the result is more than one component
    // remove all faces that belonged to the component

    Temp<bool> wasUsed(NPoints());
    Temp<bool> isUsed(NPoints());
    Temp<bool> normalUsed(NNormals());
    for( i=0; i<NPoints(); i++ ) wasUsed[i]=isUsed[i]=false;
    for( i=0; i<NNormals(); i++ ) normalUsed[i]=false;
    // scan which points were used
    for( i=0; i<NFaces(); i++ )
    {
      const FaceT face(this,i);
      for( int v=0; v<face.N(); v++ ) wasUsed[face.GetPoint(v)]=true;
    }
    // remove all faces of the component
    for( i=NFaces(); --i>=0; ) if( inSelection[i] )
    {
      DeleteFace(i);
    }
    // scan which points and normals are now used
    for( i=0; i<NFaces(); i++ )
    {
      const FaceT face(this,i);
      for( int v=0; v<face.N(); v++ )
      {
        isUsed[face.GetPoint(v)]=true;
        normalUsed[face.GetNormal(v)]=true;
      }
    }
    // remove all points that are no longer used (but was before)
    for( i=NPoints(); --i>=0; ) if( wasUsed[i] && !isUsed[i] )
    {
      DeletePoint(i);
    }
    // remove all normals that are no longer used
    for( i=NNormals(); --i>=0; ) if( !normalUsed[i] )
    {
      DeleteNormal(i);
    }

    // merge convex components
    Merge(*result);
    ClearSelection();
  }
}


void ObjToolTopology::CheckConvexComponent()
{
  Selection sel(this);  // check a component for convexity
  // check all pairs of vertices
  // check if all component vertices are inside of all component faces
  // 
  Temp<bool> badPoints(NPoints());
  int i,f;
  for( i=0; i<NPoints(); i++ ) badPoints[i]=false;
  for( f=0; f<NFaces(); f++ ) if( FaceSelected(f) )
  {
    FaceT face(this,f);
    Vector3 normal=face.CalculateRawNormal();
    Vector3 p0=Point(face.GetPoint(0));
    float d=-(normal*p0);
    for( i=0; i<NPoints(); i++ ) if( PointSelected(i) )
    {
      Vector3 pI=Point(i);
      if( !FaceT::PointInHalfSpace(normal,d,pI) )
      {
        badPoints[i]=true;
      }
    }
  }  
  for( i=0; i<NPoints(); i++ ) if (badPoints[i]) sel.PointSelect(i);
  if (sel.NPoints()==0)
  {
    // check for double sided faces
    // convex component cannot have double sided faces
    sel=*GetSelection();
    SelectDoubleSided(&sel);
  }
  UseSelection(&sel);
}


void ObjToolTopology::RemoveComponents()
{
  // to do: remove any named selection formated like Component<number>
 /* int skip=50;
  for( int i=1; skip>0; i++ )
  {
    char buff[256];
    snprintf(buff,sizeof(buff),"Component%02d",i);
    if( !DeleteNamedSel(buff) ) skip--;
  }*/

  for(int i= 0; i < MAX_NAMED_SEL; i++)
  {
   const char * sel = NamedSel(i);
   if (sel && (strncmp(sel,"Component",9) == 0 || strncmp(sel,"cc_",3) == 0))
     DeleteNamedSel(sel);
  }
}

int ObjToolTopology::CreateComponents(const Selection *forbidenFaces)
{
  Selection save=_sel;
  // select components as used when usign 'O' selections
  bool *visited=new bool[NPoints()];
  if( !visited ) return 0;
  int i;
  for( i=0; i<NPoints(); i++ ) visited[i]=false;
  CEdges *edges=new CEdges(NPoints());
  if( !edges ) 
  {delete[] visited;return 0;}
  for( i=0; i<NFaces(); i++ )
  {
    const FaceT face(this,i);
    int j;
    for( j=0; j<face.N(); j++ )
    {
      int i0=face.GetPoint(j);
      int i1=face.GetPoint((j+1)%face.N());
      edges->SetEdge(i0,i1);
    }
  }
  // remove any components
  RemoveComponents();
  int component=0;
  for( i=0; i<NPoints(); i++ )
  {
    if( !visited[i] )
    {
      ClearSelection();
      PointSelect(i,true);
      visited[i]=true;
      PromoteComponent(visited,*edges,i,true);
      SelectFacesFromPoints(-1,SelSet);
      char buff[256];
      if (forbidenFaces) UseSelection(forbidenFaces,SelSub);
      if (!GetSelection()->IsEmpty())
      {
        snprintf(buff,sizeof(buff),"Component%02d",++component);
        SaveNamedSel(buff);
      }
    }
  }
  delete edges;
  delete[] visited;
  _sel=save;
  return component;
}



#if 0
void ObjToolTopology::CreateConvexComponents()
{
  // scan for components
  CreateComponents();
  int i;
  for( i=1; ; i++ )
  {
    WString name;
    name.Sprintf("Component%02d",i);
    if( !UseNamedSel(name) ) break; // all components done
    // split selection to convex components
    SelectionToConvexComponents();
  }
  // components changed - rescan
  CreateComponents();
}
#endif

void ObjToolTopology::CheckConvexComponents()
{
  // scan for components
  Selection result(this);
  result.Clear();
  CreateComponents();
  int i;
  for( i=1; ; i++ )
  {
    char buff[256];
    snprintf(buff,sizeof(buff),"Component%02d",i);
    if( !UseNamedSel(buff) ) break; // all components done
    // split selection to convex components
    CheckConvexComponent();
    result+=*GetSelection();
  }
  _sel=result;
}

bool ObjToolTopology::FaceCanBeInConvexHull
(
 int i, int j, int k, bool allPoints
 )
{
  if( j==i || k==i || j==k ) return false;
  FaceT face(this);
  face.SetN(3);
  face.SetPoint(0,i);
  face.SetPoint(1,j);
  face.SetPoint(2,k);
  Vector3 normal=face.CalculateRawNormal();
  Vector3 p0=Point(face.GetPoint(0));
  float d=-(normal*p0);
  int l;
  // check if all points are inside the new face
  for( l=0; l<NPoints(); l++ ) if( allPoints || PointSelected(l) )
  {
    Vector3 pL=Point(l);
    if( !FaceT::PointInHalfSpace(normal,d,pL) ) return false;
  }

  // avoid co-linear faces
  Vector3 p1=(Vector3)Point(j)-(Vector3)Point(i);
  Vector3 p2=(Vector3)Point(k)-(Vector3)Point(i);
  float cosFi=p1*p2/(p1.Size()*p2.Size());
  if( fabs(cosFi)>=0.999 ) return false; // co-linear points

  return true;
}


void ObjToolTopology::CreateConvexHull( bool allPoints )
{
  ProgressBar<int> progress(NPoints());
  for(;;)
  {
    // first of all remove any co-linear points
    bool noChange=true;
    int i,j,k;
    for( i=0; i<NPoints(); i++ ) if( allPoints || PointSelected(i) )
      for( j=0; j<NPoints(); j++ ) if( allPoints || PointSelected(j) )
        for( k=0; k<NPoints(); k++ ) if( allPoints || PointSelected(k) )
        {
          // avoid co-linear triplets
          if( i==k || i==j || j==k ) continue;
          Vector3 p1=(Vector3)Point(j)-(Vector3)Point(i);
          Vector3 p2=(Vector3)Point(k)-(Vector3)Point(i);
          float cosFi=p1*p2/(p1.Size()*p2.Size());
          if( cosFi<=-0.999 )
          {
            // i is middle point of the triplet
            noChange=false;
            DeletePoint(i);
            progress.SetPos(i);
            goto NextPoint;
          }
        }
NextPoint:
        if( noChange ) break;
  }
  Temp<bool> wasUsed(NPoints());
  Temp<bool> isUsed(NPoints());
  int i;
  for( i=0; i<NPoints(); i++ ) wasUsed[i]=isUsed[i]=false;
  // scan which points were used
  for( i=0; i<NFaces(); i++ )
  {
    const FaceT face(this,i);
    for( int v=0; v<face.N(); v++ ) wasUsed[face.GetPoint(v)]=true;
  }

  Edges openEdges; // list of edges that we need to close
  Edges closedEdges; // list of edges that we need to close
  // all open edges need to be closed
  // no edge can be opened twice
  // no edge can be closed twice

  // scan all possible triangles
  // maintain a list of "open" edges
  {
    // start by finding the smallest face
    // that could belong to the convex hull
    int minI=-1,minJ=-1,minK=-1;
    double minArea=1e50;
    // if there are some open edges, close them
    int i,j,k;
    for( i=0; i<NPoints(); i++ ) if( allPoints || PointSelected(i) )
      for( j=0; j<NPoints(); j++ ) if( allPoints || PointSelected(j) )
        for( k=0; k<NPoints(); k++ ) if( allPoints || PointSelected(k) )
        {
          if( !FaceCanBeInConvexHull(i,j,k,allPoints) ) continue;
          FaceT face(this);
          face.SetN(3);
          face.SetPoint(0,i);
          face.SetPoint(1,j);
          face.SetPoint(2,k);
          face.SetNormal(0,0);
          face.SetNormal(1,0);
          face.SetNormal(2,0);
          double area=face.CalculateArea();
          double perimeter=face.CalculatePerimeter();
          area=perimeter/area;
          if( minArea>area ) minArea=area,minI=i,minJ=j,minK=k;
        }

        if( minI<0 ) return; // no face
        // create a new face
        int index=NFaces();
        FaceT newFace(this,NewFace());
        newFace.SetN(3);
        newFace.SetPoint(0,minI);
        newFace.SetPoint(1,minJ);
        newFace.SetPoint(2,minK);
        newFace.SetNormal(0,0);
        newFace.SetNormal(1,0);
        newFace.SetNormal(2,0);
        FaceSelect(index);

        // open all new edges
        openEdges.Add(Edge(minI,minJ));
        openEdges.Add(Edge(minJ,minK));
        openEdges.Add(Edge(minK,minI));
  }


  while( openEdges.Size()>0 )
  {
    // while there are some open edges, close them
    int minI=-1,minJ=-1,minK=-1;
    int maxClose=-1; // select the one that will close most edges
    double minArea=1e50; // select the one wtih the smallest area
    int ei;
    for( ei=0; ei<openEdges.Size(); ei++ )
    {
      // try to create some triangle with edge (edge.b,edge.a)
      // this would close some edge
      Edge &edge=openEdges[ei];
      int i=edge.b;
      int j=edge.a;
      int k;

      // check if this edge had already been closed
      if( closedEdges.Find(Edge(i,j))>=0 )
      {
        LogF("Some closed edge is opened.");
        continue;
      }

      // try to create triangle i,j,k
      for( k=0; k<NPoints(); k++ ) if( allPoints || PointSelected(k) )
      {
        if( !FaceCanBeInConvexHull(i,j,k,allPoints) ) continue;

        // no edge can be closed twice
        if( closedEdges.Find(Edge(j,k))>=0 ) continue;
        if( closedEdges.Find(Edge(k,i))>=0 ) continue;
        FaceT face(this);
        face.SetN(3);
        face.SetPoint(0,i);
        face.SetPoint(1,j);
        face.SetPoint(2,k);
        // calculate perimeter/area (small means nice triangle)
        double area=face.CalculateArea();
        double perimeter=face.CalculatePerimeter();
        area=perimeter/area;
        // check how many edges we will close
        int nClose=0;
        if( openEdges.Find(Edge(j,i))>=0 ) nClose++;
        if( openEdges.Find(Edge(k,j))>=0 ) nClose++;
        if( openEdges.Find(Edge(i,k))>=0 ) nClose++;
        bool sureYes=false,sureNo=false;
        if( maxClose<nClose ) sureYes=true;
        else if( maxClose>nClose ) sureNo=true;
        if( !sureYes && !sureNo )
        {
          if( minArea>area ) sureYes=true;
          else if( minArea<area ) sureNo=true;
        }
        if( sureYes ) minArea=area,maxClose=nClose,minI=i,minJ=j,minK=k;
      }		
    }
    // create a new face
    if( minI<0 )
    { // no more faces - some open edges - that is strange
      break;
    }
    int index=NFaces();
    FaceT newFace(this,NewFace());
    newFace.SetN(3);
    newFace.SetPoint(0,minI);
    newFace.SetPoint(1,minJ);
    newFace.SetPoint(2,minK);
    newFace.SetNormal(0,0);
    newFace.SetNormal(1,0);
    newFace.SetNormal(2,0);
    FaceSelect(index);
    // close edges (minJ,minI), (minK,minJ), (minI,minK)
    // or open edges (minI,minJ), (minJ,minK), (minK,minI)
    int v1,v2,eIndex;
    v1=minI,v2=minJ;
    if( (eIndex=openEdges.Find(Edge(v2,v1)))>=0 )
    {
      closedEdges.Add(Edge(v1,v2));
      closedEdges.Add(Edge(v2,v1));
      openEdges.Delete(eIndex);
    }
    else
    {
      if( closedEdges.Find(Edge(v2,v1))<0 ) openEdges.Add(Edge(v1,v2));
    }

    v1=minJ,v2=minK;
    if( (eIndex=openEdges.Find(Edge(v2,v1)))>=0 )
    {
      closedEdges.Add(Edge(v1,v2));
      closedEdges.Add(Edge(v2,v1));
      openEdges.Delete(eIndex);
    }
    else
    {
      if( closedEdges.Find(Edge(v2,v1))<0 ) openEdges.Add(Edge(v1,v2));
    }

    v1=minK,v2=minI;
    if( (eIndex=openEdges.Find(Edge(v2,v1)))>=0 )
    {
      closedEdges.Add(Edge(v1,v2));
      closedEdges.Add(Edge(v2,v1));
      openEdges.Delete(eIndex);
    }
    else
    {
      if( closedEdges.Find(Edge(v2,v1))<0 ) openEdges.Add(Edge(v1,v2));
    }
  }
  // scan which points are used
  for( i=0; i<NFaces(); i++ )
  {
    const FaceT face(this,i);
    for( int v=0; v<face.N(); v++ ) isUsed[face.GetPoint(v)]=true;
  }
  // delete any unused points
  for( i=NPoints(); --i>=0; ) if( allPoints || PointSelected(i) )
  {
    //if( !isUsed[i] && wasUsed[i] ) DeletePoint(i);
    if( !isUsed[i] ) DeletePoint(i);
  }
  Squarize();
  //RecalcNormals(); // will be done is squarize
}

void ObjToolTopology::ComponentConvexHull()
{
  // scan for components
  CreateComponents();
  int i;
  for( i=1; ; i++ )
  {
    char name[256];
    snprintf(name,sizeof(name),"Component%02d",i);
    if( !UseNamedSel(name) ) break; // all components done
    // split selection to convex components
    // delete any faces
    SelectionDeleteFaces();
    CreateConvexHull();
  }
  CreateComponents();
  ClearSelection();
}


#if 0
bool ObjectData::FaceCanBeInConvexHull
(
 int i, int j, int k, bool allPoints
 )
{
  if( j==i || k==i || j==k ) return false;
  FaceT face(this);
  face.SetN(3);
  face.SetPoint(0,i);
  face.SetPoint(1,j);
  face.SetPoint(2,k);
  Vector3 normal=face.CalculateRawNormal();
  Vector3 p0=Point(face.GetPoint(0));
  float d=-(normal*p0);
  int l;
  // check if all points are inside the new face
  for( l=0; l<NPoints(); l++ ) if( allPoints || PointSelected(l) )
  {
    Vector3 pL=Point(l);
    if( !FaceT::PointInHalfSpace(normal,d,pL) ) return false;
  }

  // avoid co-linear faces
  Vector3 p1=(Vector3)Point(j)-(Vector3)Point(i);
  Vector3 p2=(Vector3)Point(k)-(Vector3)Point(i);
  float cosFi=p1*p2/(p1.Size()*p2.Size());
  if( fabs(cosFi)>=0.999 ) return false; // co-linear points

  return true;
}










void ObjectData::CreateConvexHull( bool allPoints )
{
  int npts=NPoints();
  for(;;)
  {
    // first of all remove any co-linear points
    bool noChange=true;
    int i,j,k;
    for( i=0; i<NPoints(); i++ ) if( allPoints || PointSelected(i) )
      for( j=0; j<NPoints(); j++ ) if( allPoints || PointSelected(j) )
        for( k=0; k<NPoints(); k++ ) if( allPoints || PointSelected(k) )
        {
          // avoid co-linear triplets
          if( i==k || i==j || j==k ) continue;
          Vector3 p1=(Vector3)Point(j)-(Vector3)Point(i);
          Vector3 p2=(Vector3)Point(k)-(Vector3)Point(i);
          float cosFi=p1*p2/(p1.Size()*p2.Size());
          if( cosFi<=-0.999 )
          {
            // i is middle point of the triplet
            noChange=false;
            DeletePoint(i);
            ObjData_SetProgress(i,npts);
            goto NextPoint;
          }
        }
NextPoint:
        if( noChange ) break;
  }
  Temp<bool> wasUsed(NPoints());
  Temp<bool> isUsed(NPoints());
  int i;
  for( i=0; i<NPoints(); i++ ) wasUsed[i]=isUsed[i]=false;
  // scan which points were used
  for( i=0; i<NFaces(); i++ )
  {
    const FaceT face(this,i);
    for( int v=0; v<face.N(); v++ ) wasUsed[face.GetPoint(v)]=true;
  }

  Edges openEdges; // list of edges that we need to close
  Edges closedEdges; // list of edges that we need to close
  // all open edges need to be closed
  // no edge can be opened twice
  // no edge can be closed twice

  // scan all possible triangles
  // maintain a list of "open" edges
  {
    // start by finding the smallest face
    // that could belong to the convex hull
    int minI=-1,minJ=-1,minK=-1;
    double minArea=1e50;
    // if there are some open edges, close them
    int i,j,k;
    for( i=0; i<NPoints(); i++ ) if( allPoints || PointSelected(i) )
      for( j=0; j<NPoints(); j++ ) if( allPoints || PointSelected(j) )
        for( k=0; k<NPoints(); k++ ) if( allPoints || PointSelected(k) )
        {
          if( !FaceCanBeInConvexHull(i,j,k,allPoints) ) continue;
          FaceT face(this);
          face.SetN(3);
          face.SetPoint(0,i);
          face.SetPoint(1,j);
          face.SetPoint(2,k);
          face.SetNormal(0,0);
          face.SetNormal(1,0);
          face.SetNormal(2,0);
          double area=face.CalculateArea();
          double perimeter=face.CalculatePerimeter();
          area=perimeter/area;
          if( minArea>area ) minArea=area,minI=i,minJ=j,minK=k;
        }

        if( minI<0 ) return; // no face
        // create a new face
        int index=NFaces();
        FaceT newFace(this,NewFace());
        newFace.SetN(3);
        newFace.SetPoint(0,minI);
        newFace.SetPoint(1,minJ);
        newFace.SetPoint(2,minK);
        newFace.SetNormal(0,0);
        newFace.SetNormal(1,0);
        newFace.SetNormal(2,0);
        FaceSelect(index);

        // open all new edges
        openEdges.Add(Edge(minI,minJ));
        openEdges.Add(Edge(minJ,minK));
        openEdges.Add(Edge(minK,minI));
  }


  while( openEdges.Size()>0 )
  {
    // while there are some open edges, close them
    int minI=-1,minJ=-1,minK=-1;
    int maxClose=-1; // select the one that will close most edges
    double minArea=1e50; // select the one wtih the smallest area
    int ei;
    for( ei=0; ei<openEdges.Size(); ei++ )
    {
      // try to create some triangle with edge (edge.b,edge.a)
      // this would close some edge
      Edge &edge=openEdges[ei];
      int i=edge.b;
      int j=edge.a;
      int k;

      // check if this edge had already been closed
      if( closedEdges.Find(Edge(i,j))>=0 )
      {
        LogF("Some closed edge is opened.");
        continue;
      }

      // try to create triangle i,j,k
      for( k=0; k<NPoints(); k++ ) if( allPoints || PointSelected(k) )
      {
        if( !FaceCanBeInConvexHull(i,j,k,allPoints) ) continue;

        // no edge can be closed twice
        if( closedEdges.Find(Edge(j,k))>=0 ) continue;
        if( closedEdges.Find(Edge(k,i))>=0 ) continue;
        FaceT face(this);
        face.SetN(3);
        face.SetPoint(0,i);
        face.SetPoint(1,j);
        face.SetPoint(2,k);
        // calculate perimeter/area (small means nice triangle)
        double area=face.CalculateArea();
        double perimeter=face.CalculatePerimeter();
        area=perimeter/area;
        // check how many edges we will close
        int nClose=0;
        if( openEdges.Find(Edge(j,i))>=0 ) nClose++;
        if( openEdges.Find(Edge(k,j))>=0 ) nClose++;
        if( openEdges.Find(Edge(i,k))>=0 ) nClose++;
        bool sureYes=false,sureNo=false;
        if( maxClose<nClose ) sureYes=true;
        else if( maxClose>nClose ) sureNo=true;
        if( !sureYes && !sureNo )
        {
          if( minArea>area ) sureYes=true;
          else if( minArea<area ) sureNo=true;
        }
        if( sureYes ) minArea=area,maxClose=nClose,minI=i,minJ=j,minK=k;
      }		
    }
    // create a new face
    if( minI<0 )
    { // no more faces - some open edges - that is strange
      break;
    }
    int index=NFaces();
    FaceT newFace(this,NewFace());
    newFace.SetN(3);
    newFace.SetPoint(0,minI);
    newFace.SetPoint(1,minJ);
    newFace.SetPoint(2,minK);
    newFace.SetNormal(0,0);
    newFace.SetNormal(1,0);
    newFace.SetNormal(2,0);
    FaceSelect(index);
    // close edges (minJ,minI), (minK,minJ), (minI,minK)
    // or open edges (minI,minJ), (minJ,minK), (minK,minI)
    int v1,v2,eIndex;
    v1=minI,v2=minJ;
    if( (eIndex=openEdges.Find(Edge(v2,v1)))>=0 )
    {
      closedEdges.Add(Edge(v1,v2));
      closedEdges.Add(Edge(v2,v1));
      openEdges.Delete(eIndex);
    }
    else
    {
      if( closedEdges.Find(Edge(v2,v1))<0 ) openEdges.Add(Edge(v1,v2));
    }

    v1=minJ,v2=minK;
    if( (eIndex=openEdges.Find(Edge(v2,v1)))>=0 )
    {
      closedEdges.Add(Edge(v1,v2));
      closedEdges.Add(Edge(v2,v1));
      openEdges.Delete(eIndex);
    }
    else
    {
      if( closedEdges.Find(Edge(v2,v1))<0 ) openEdges.Add(Edge(v1,v2));
    }

    v1=minK,v2=minI;
    if( (eIndex=openEdges.Find(Edge(v2,v1)))>=0 )
    {
      closedEdges.Add(Edge(v1,v2));
      closedEdges.Add(Edge(v2,v1));
      openEdges.Delete(eIndex);
    }
    else
    {
      if( closedEdges.Find(Edge(v2,v1))<0 ) openEdges.Add(Edge(v1,v2));
    }
  }
  // scan which points are used
  for( i=0; i<NFaces(); i++ )
  {
    const FaceT face(this,i);
    for( int v=0; v<face.N(); v++ ) isUsed[face.GetPoint(v)]=true;
  }
  // delete any unused points
  for( i=NPoints(); --i>=0; ) if( allPoints || PointSelected(i) )
  {
    //if( !isUsed[i] && wasUsed[i] ) DeletePoint(i);
    if( !isUsed[i] ) DeletePoint(i);
  }
  Squarize();
  //RecalcNormals(); // will be done is squarize
}










void ObjectData::ComponentConvexHull()
{
  // scan for components
  CreateComponents();
  int i;
  for( i=1; ; i++ )
  {
    char name[256];
    snprintf(name,sizeof(name),"Component%02d",i);
    if( !UseNamedSel(name) ) break; // all components done
    // split selection to convex components
    // delete any faces
    SelectionDeleteFaces();
    CreateConvexHull();
  }
  CreateComponents();
  ClearSelection();
}




#endif

void ObjToolTopology::AutoSharpEdges(float angle)
{
  Vector3 cx1,cx2,sum;
  float cmpf;
  cx1[0]=cos(angle);
  cx1[1]=sin(angle);
  cx1[2]=0;
  cx2[0]=1;
  cx2[1]=0;
  cx2[2]=0;
  sum=cx1+cx2;
  cmpf=sum.Size();
  CEdges facegraph;
  CEdges nocross;  
  facegraph.SetVertices(NFaces());
  BuildFaceGraph(facegraph,nocross);
  for (int i=0;i<NFaces();i++) if (FaceSelected(i))
  {
    int p;
    for (int j=0;(p=facegraph.GetEdge(i,j))!=-1;j++)
    {
      FaceT fc1(this,i);
      FaceT fc2(this,p);
      Vector3 v1=fc1.CalculateNormal();
      Vector3 v2=fc2.CalculateNormal();
      sum=v1+v2;
      float szsum=sum.Size();
      if (szsum<cmpf)
      {       
        int b=fc1.N()-1;
        for (int a=0;a<fc1.N();b=a,a++)
        {
          int d=fc2.N()-1;
          for (int c=0;c<fc2.N();d=c,c++)
          {
            if (fc1.GetPoint(a)==fc2.GetPoint(d) && fc1.GetPoint(b)==fc2.GetPoint(c))
            {
              AddSharpEdge(fc1.GetPoint(a),fc1.GetPoint(b));
              //goto konec; must work also on double faces... 
            }
          }
        }
      }
//konec:;
    }
  }}

void ObjToolTopology::MakeEdgesSharp()
{
  for( int i=0; i<NFaces(); i++ )
  {

    const FaceT face(this,i);
    int prev=face.N()-1;
    for( int v=0; v<face.N(); v++ )
    {
      int v0=face.GetPoint(prev);
      int v1=face.GetPoint(v);
      if( PointSelected(v0) && PointSelected(v1) )
      {
        AddSharpEdge(v0,v1);
      }
      prev=v;
    }
  }
  RecalcNormals();
}


void ObjToolTopology::ReadSmoothGroups(unsigned long *groups,  bool selfaces)
{
  ObjectData &obj=*this;
  SRef<unsigned long> flags=new unsigned long[obj.NFaces()];
  int g=0;
  for (int i=0;i<obj.NFaces();i++) if (!selfaces || obj.FaceSelected(i))  
    flags[i]=groups[g++];
  else
    flags[i]=0;
  CEdges graph(obj.NFaces());
  CEdges nocross(obj.NFaces());
  obj.BuildFaceGraph(graph,nocross);
  for (int i=0;i<obj.NFaces();i++) if (!selfaces || obj.FaceSelected(i))
  {
    int soused_poradi=0;
    int soused_index=graph.GetEdge(i,soused_poradi);
    while (soused_index!=-1)
    {
      if (soused_index>i)
      {
        if ((flags[soused_index] & flags[i])==0)
        {
          FaceT fc1(obj,i);
          FaceT fc2(obj,soused_index);
          int v1,v2;
          if (fc1.GetEdge(fc2,v1,v2))
            obj.AddSharpEdge(v1,v2);
        }
      }
      soused_index=graph.GetEdge(i,++soused_poradi);
    }
  }
  obj.SortSharpEdges();
}

bool ObjToolTopology::BuildSmoothGroups(unsigned long *groups)
{
  ProgressBar<int> pb(NFaces());  
  CEdges sharp;
  RecalcNormals();
  LoadSharpEdges(sharp);  
  SRef<char> normalFlags=new char[NNormals()];    //0 - unknown item, 1 - open item, 2 - closed iten
  SRef<char> pointFlags=new char[NPoints()];
  SRef<char> faceFlags=new char[NFaces()];
  char *normflags=normalFlags;
  char *ptflags=pointFlags;
  char *fcflags=faceFlags;
  memset(groups,0,NFaces()*sizeof(unsigned long));  
  do
  {
    int startFace=0;
    while (startFace<NFaces() && groups[startFace]) startFace++;
    if (startFace>=NFaces()) return true; //all faces processed, quit
    pb.SetPos(startFace);
    //ObjData_SetProgress(startFace,NFaces());
    memset(ptflags,0,NPoints());  
    memset(normflags,0,NNormals());
    memset(fcflags,0,NFaces());
    fcflags[startFace]=1;
    bool rep;
    int cycles=0;
    do
    {
      rep=false;
      for (int i=0;i<NFaces();i++) if (fcflags[i]>0 && fcflags[i]<3) //pouze facy majici nejaky priznak
      {
        FaceT fc(*this,i);
        for (int j=0;j<fc.N();j++) //projdi vsechny vrcholy
        {
          int normal=fc.GetNormal(j); //vytahni sdilene normaly
          int point=fc.GetPoint(j);
          if (ptflags[point]<fcflags[i]) {ptflags[point]=fcflags[i];} //oznac body podle stavu face
          normflags[normal]=1;  //oznac sdilenou normalu
        }
        fcflags[i]++; //oznac face jako zpracovanu
      }
      for (int i=0;i<NFaces();i++) if (fcflags[i]==0) //pouze facy, ktere jeste nezname
      {
        FaceT fc(*this,i);                          
        bool sharing=false;
        for (int j=0;j<fc.N();j++) if (normflags[fc.GetNormal(j)]) {sharing=true;break;}
        if (sharing)
        {
          char max=0,cur;          
          for (int j=0;j<fc.N();j++) 
          {
            cur=ptflags[fc.GetPoint(j)];           //zjisti, jake vlajky maji body v okoli
            if (cur>max) max=cur;
          }
          if (max==1)                   //pokud jsou jen otevrene, muze tento face sdilet smoothgroupu
          {
            int j,k;      //test na ostre hrany v okoli, ktere jsou na otevrenych bodech
            for (j=0,k=fc.N()-1;j<fc.N();k=j,j++)
            {
              int pta=fc.GetPoint(k),ptb=fc.GetPoint(j);
              if (sharp.IsEdge(pta,ptb) &&
                ptflags[pta]==1 && ptflags[ptb]==1) break; //pokud je nalezena ostra hrana na otevrenem bode, face nas nezajima
            }
            if (j==fc.N())
            {
              fcflags[i]=1;               //oznac tuto face za zajimavou
              rep=true;
            }
          }
        }
      }
      cycles++;
    }
    while (rep);
    DWORD usedgroups=0;
    for (int i=0;i<NFaces();i++) if (fcflags[i]==0 && groups[i]!=0)
    {
      FaceT fc(this,i);
      for (int j=0;j<fc.N();j++) 
        if (ptflags[fc.GetPoint(j)])
        {
          usedgroups|=groups[i];
          break;
        }
    }
    DWORD unusedgroups=~usedgroups;
    DWORD selgroup;
    for (int i=0;i<32;i++)
    {
      selgroup=1<<i;
      if (selgroup & unusedgroups) break;
    }
    if (selgroup==0) 
    {
      selgroup=1;
    }
    for (int i=0;i<NFaces();i++) if (fcflags[i]) groups[i]|=selgroup;
  }
  while (true);
}

void ObjToolTopology::DetectEdgesFromNormals()
{
  CEdges nocross(NFaces());
  CEdges graph(NFaces());
  BuildFaceGraph(graph,nocross);

  RemoveAllSharpEdges();

  for (int i=0;i<NFaces();i++)
  {
    int j,k;
    FaceT cur(*this,i);
    for (j=0;(k=graph.GetEdge(i,j))!=-1;j++)
    {
      FaceT neib(*this,k);
      int i1a,i1b,i2a,i2b;
      if (cur.GetEdgeIndices(neib,i1a,i1b,i2a,i2b))
      {
        VecT n1a,n1b,n2a,n2b;
        n1a=Normal(cur.GetNormal(i1a));
        n2a=Normal(neib.GetNormal(i2a));
        n1b=Normal(cur.GetNormal(i1b));
        n2b=Normal(neib.GetNormal(i2b));
        if (n1a.Distance2(n2a)>0.01f || n1b.Distance2(n2b)>0.01f)
          AddSharpEdge(cur.GetPoint(i1a),cur.GetPoint(i1b));
      }
    }
  }

}


static void CreateNonConvexEdges(ObjectData *obj, CEdges &faceface, CEdges &nonconvex, bool selectedges)
{
  int f1,f2,i;
  nonconvex.SetVertices(selectedges?obj->NPoints():obj->NFaces());
  for (f1=0;f1<obj->NFaces();f1++)
  {
    FaceT fc1(obj,f1);
    Vector3 pt=obj->Point(fc1.GetPoint(0));
    Vector3 normal(fc1.CalculateNormal());
    float d=-(normal*pt);
    for (i=0;(f2=faceface.GetEdge(f1,i))!=-1;i++) if (f1!=f2)
    {
      bool removeface=true;
      FaceT fc2(obj,f2);
      for (int j=0;j<fc2.N();j++) 
      {
        Vector3 pi=obj->Point(fc2.GetPoint(j));
        if (!FaceT::PointInHalfSpace(normal,d,pi))
        {
          if (selectedges)
          {
            int a=-1,b=-1;
            for (int u=0;u<fc1.N();u++)
              for (int v=0;v<fc2.N();v++)
                if (fc1.GetPoint(u)==fc2.GetPoint(v))
                  if (a!=-1) 
                  {
                    b=fc1.GetPoint(u);
                    goto jumpout;
                  }
                  else
                    a=fc1.GetPoint(u);
jumpout:  nonconvex.SetEdge(a,b);
          }
          else 
            if (!nonconvex.IsEdge(f1,f2)) 
            {
              nonconvex.SetEdge(f1,f2);
              break;
            }
        }
      }
    }
  }
}

static void SetSharpEdges(ObjectData *obj,const CEdges &sharp)
{
  obj->RemoveAllSharpEdges();
  obj->ClearSelection();
  for (int i=0;i<obj->NPoints();i++)
  {
    int a,b,j;
    a=i;
    for (j=0;(b=sharp.GetEdge(a,j))!=-1;j++)
    {
      obj->AddSharpEdge(a,b);
      obj->PointSelect(a);
      obj->PointSelect(b);
    }
  }
}

static bool FaceTestSelOnly(const FaceT &fc, void *context)
  {
      return fc.GetObject()->FaceSelected(fc.GetFaceIndex());
  }

void ObjToolTopology::CheckConvexity(bool selonly)
{  
  CEdges edges(NPoints());
  CEdges nonconvex(NPoints());
  BuildFaceGraph(edges,nonconvex,selonly?FaceTestSelOnly:NULL);
  CreateNonConvexEdges(this,edges,nonconvex,true);
  SetSharpEdges(this,nonconvex); 
  if (GetSelection()->NPoints()==0)
  {
    Selection sel(this);    
    if (selonly) sel=*GetSelection();
    else sel.SelectAll();
    SelectDoubleSided(&sel);
    UseSelection(&sel);
  }
}

#define FPI 3.1415926535f

static float safe_acos(float a)
{
  if (a>=1.0f) return 0;
  if (a<=-1.0f) return FPI;
  else return acos(a);
}

static inline float GetTriangleAngle(const Vector3 &a, const Vector3 &b, const Vector3 &c, const Vector3 &refNorm)
{
  Vector3 ab(a-b),cb(b-c);
  //we need full angle. First calculate cos angle;
  float angle=safe_acos(ab.CosAngle(cb));
  //calculate normal direction;
  Vector3 dir=ab.CrossProduct(cb);
  //compare direction with ref direction
  float comp=dir.DotProduct(refNorm);
  if (comp<0) //Direction to second side, correct angle
    angle=-angle;
  return angle;
}

static inline bool GetRefNormal(const Vector3 &a, const Vector3 &b, const Vector3 &c, Vector3 &refNormal)
{
  Vector3 ab(a-b),cb(c-b);
  //we need full angle. First calculate cos angle;
  //float angle=acos(ab.DotProduct(cb));
  //calculate normal direction;
  refNormal=ab.CrossProduct(cb);

  return refNormal.SquareSize() * 4 > ab.SquareSize() * cb.SquareSize(); /// at least 30 degrees for good normal
  
}
static inline int GetNextVertex(int start, bool *usedVx, int max)
{
  for (int i=start+1;i<max;i++) if (usedVx[i]==false) return i;
  return start;
}

static inline int GetNextVertex2(int start, bool *usedVx, int max)
{
  int res=GetNextVertex(start,usedVx,max);
  if (res==start)
    res=GetNextVertex(-1,usedVx,max);
  return res;
}

static inline bool IsAnyInTriangle(const AutoArray<Vector3>& vert, const Vector3& v1, const Vector3& v2, const Vector3& v3)
{
  Vector3 n[3];
  float d[3];

  Vector3 t1 = v1 - v2;
  Vector3 t2 = v3 - v2;
  t2.Normalize();

  n[0] = t1 - t2 * (t1 * t2);
  d[0] = n[0] * v2;

  t1 = v2 - v1;
  t2 = v3 - v1;
  t2.Normalize();

  n[1] = t1 - t2 * (t1 * t2);
  d[1] = n[1] * v1;

  t1 = v3 - v1;
  t2 = v2 - v1;
  t2.Normalize();

  n[2] = t1 - t2 * (t1 * t2);
  d[2] = n[2] * v1;

  for(int i = 0; i < vert.Size(); i++)
  { 
    const Vector3& v = vert[i];
    if (v == v1 || v == v2 || v == v3)
      continue;

    int j = 0;
    for(; j < 3; j++)
      if (v * n[j] - d[j] <= 0)
        break;

    if (j==3)
      return true;
  }
  return false;
}

bool ObjToolTopology::TessellatePolygon(int nvertices, const int *indexList, 
                        const float *uvlist /*=NULL*/, 
                        const char *textureName /*=NULL*/, 
                        const char *materialName /*=NULL*/,
                        Array<STessellateInfo> *outTesInfo /*=NULL*/)
{

  if (nvertices<4)  //in special case, o2 know quads, but we will not use them
  {
    ASSERT(nvertices >= 3);    
    FaceT fc;
    fc.CreateIn(this);
    fc.SetN(nvertices);
    for (int i=0;i<nvertices;i++)
    {
      //int index=nvertices-i-1;
      fc.SetPoint(i,indexList[i]);
      if (uvlist)
      {
        fc.SetU(i,uvlist[i*2]);
        fc.SetV(i,uvlist[i*2+1]);
      }
      if (outTesInfo)
      {
        STessellateInfo &nfo=outTesInfo->operator [](i);
        nfo.index=i;
        nfo.face=fc.GetFaceIndex();
        nfo.vx=i;
      }
    }
    if (textureName) fc.SetTexture(textureName);
    if (materialName) fc.SetMaterial(materialName);
    if (outTesInfo)
    {
      *outTesInfo=Array<STessellateInfo>(outTesInfo->Data(),3);
    }
    FaceSelect(fc.GetFaceIndex()); // select face;
    return true;
  }
  if (nvertices<3) return false;
  Vector3 baltazar=Point(indexList[nvertices-2]);
  Vector3 melichar=Point(indexList[nvertices-1]);
  Vector3 kaspar=Point(indexList[0]);
  Vector3 refNormal; 
  int i=1;
  while (!GetRefNormal(kaspar,melichar,baltazar,refNormal) && i<nvertices)
  {
    baltazar=melichar;
    melichar=kaspar;
    kaspar=Point(indexList[i]);    
    i++;
  }

  refNormal.Normalize();
  
  float angle=0;
  baltazar=Point(indexList[nvertices-2]);
  melichar=Point(indexList[nvertices-1]);
  
  //spocitej orientaci polygonu
  AutoArray<Vector3> ptVx; 
  ptVx.Reserve(nvertices,nvertices);

  for (i=0;i<nvertices;i++)
  {
    kaspar=Point(indexList[i]);
    ptVx.AddFast(kaspar);

    angle=angle+GetTriangleAngle(kaspar, melichar, baltazar,refNormal);
    baltazar=melichar;
    melichar=kaspar;
    //soucet vsech uhlu v polygonu musi byt 180 stupnu
  }
  if (angle<0)  //pokud vyslo zaporne cislo, je polygon definovan obracene
    refNormal *= -1;

  //priznaky, kazdy vrchol zde ma true, nebo false. true znamena, ze uz byl pouzit
  bool *usedVx=(bool *)alloca(nvertices);
 
  memset(usedVx,0,nvertices);
  int remainVx=nvertices-2; //pocet zbyvajicich vrcholu
  int tesInfoIndex=0;
  while(remainVx)
  {
    int best_kvx=0,best_mvx=0,best_bvx=0;
    float best_angle=FPI;    

    int bvx=GetNextVertex2(-1,usedVx,nvertices);
    int start=bvx;
    int mvx=GetNextVertex2(bvx,usedVx,nvertices);
    baltazar=ptVx[bvx];
    if (mvx!=-1)
    {
      melichar=ptVx[mvx];
      int kvx=GetNextVertex2(mvx,usedVx,nvertices);

      // create even bad triangle if nothing was found
      best_kvx=kvx; 
      best_mvx=mvx;
      best_bvx=bvx;   
      do
      {
        kaspar=ptVx[kvx];
        float angle0=GetTriangleAngle(kaspar,melichar,baltazar,refNormal); 
        float angle1=GetTriangleAngle(baltazar,kaspar,melichar,refNormal); 
        float angle2=GetTriangleAngle(melichar,baltazar,kaspar,refNormal); 

        angle = max(angle0, angle1);
        angle = max(angle, angle2);

        if (angle > 0 && angle < best_angle)
          //if (angle > best_angle)
        {
          // check that no other point lies inside triangle
          if (!IsAnyInTriangle(ptVx, baltazar, melichar, kaspar))
          {                    
            best_kvx=kvx;
            best_mvx=mvx;
            best_bvx=bvx;
            best_angle=angle;
          }
        }
        bvx=mvx;
        mvx=kvx;              
        baltazar=melichar;
        melichar=kaspar;
        kvx=GetNextVertex2(mvx,usedVx,nvertices);
      }
      while (bvx!=start);
    }     
    //if (best_bvx<0) best_bvx+=nvertices;
    //if (best_angle==0) return false;
    FaceT fc;  
    fc.CreateIn(this);
    fc.SetN(3);
    fc.SetPoint(2,indexList[best_kvx]);
    fc.SetPoint(1,indexList[best_mvx]);
    fc.SetPoint(0,indexList[best_bvx]);
    if (outTesInfo)
    {
      if (tesInfoIndex+3<=outTesInfo->Size())
      {      
        int fcindex=fc.GetFaceIndex();
        {
          STessellateInfo &nfo=outTesInfo->operator [](tesInfoIndex++);
          nfo.face=fcindex;
          nfo.vx=2;
          nfo.index=best_kvx;
        }
        {
          STessellateInfo &nfo=outTesInfo->operator [](tesInfoIndex++);
          nfo.face=fcindex;
          nfo.vx=1;
          nfo.index=best_mvx;
        }
        {
          STessellateInfo &nfo=outTesInfo->operator [](tesInfoIndex++);
          nfo.face=fcindex;
          nfo.vx=0;
          nfo.index=best_bvx;
        }
      }
    }
    if (uvlist)
    {
      fc.SetUV(2,uvlist[best_kvx*2],uvlist[best_kvx*2+1]);
      fc.SetUV(1,uvlist[best_mvx*2],uvlist[best_mvx*2+1]);
      fc.SetUV(0,uvlist[best_bvx*2],uvlist[best_bvx*2+1]);
    }
    if (textureName) fc.SetTexture(textureName);
    if (materialName) fc.SetMaterial(materialName);
    usedVx[best_mvx]=true;    //mark only "melichar" that cannot be used
    remainVx--;

    FaceSelect(fc.GetFaceIndex()); // select face;
  }
  if (outTesInfo)
  {
    *outTesInfo=Array<STessellateInfo>(outTesInfo->Data(),tesInfoIndex);
  }
  return true;
}

void O2Polygon::PreparePar(const ObjectData * src)
{
  ASSERT(_faces.Size() > 0);
  ASSERT(_indexes.Size() > 0);

  FaceT refFace(src, _faces[0]);

  _textureName = refFace.GetTexture();
  _materialName = refFace.GetMaterial();

  // search for UVList...
  _uvlist.Resize(2 * _indexes.Size());
  for(int i = 0; i < _faces.Size(); i++)
  {
    FaceT refFace(src, _faces[i]);

    for(int j = 0; j < refFace.N(); j++)
    {
      int vt = _indexes.Find(refFace.GetPoint(j));
      if (vt >= 0)
      {
        _uvlist[2 * vt] = refFace.GetU(j);
        _uvlist[2 * vt + 1] = refFace.GetV(j);
      }      
    }
  }
};

void O2Polygon::CleanUnused(const AutoArray<int>& numUse)
{
  for(int i = 0; i < _indexes.Size(); i++)
  {
    ASSERT(numUse[_indexes[i]] >= 0);
    if (numUse[_indexes[i]] == 0)
    {
      _indexes.DeleteAt(i);
      i--;
    }
  }
}

void O2Polygon::Select(Selection& points, Selection& faces) const
{
  for(int i = 0; i < _indexes.Size(); i++) points.PointSelect(_indexes[i]);
  for(int i = 0; i < _faces.Size(); i++) faces.FaceSelect(_faces[i]);
}

void O2Polygon::Create(int * indexList, int size, Selection * faces,const ObjectData& src, bool filterOnLines)
{    
  if (filterOnLines)
  {
    // filter points on lines
    for(int i = 0; i < size; i++) 
    {
      int stop = i + 1;
      for(;stop < size && indexList[stop] >= 0; stop++);

      int start = i;
      ASSERT(stop - start >= 3);
      int used = 0;
      for(; i < stop; i++)
      {    

        int i_before = i != start? i - 1 : stop - 1;
        int i_after = i != (stop - 1)? i + 1 : start;

        Vector3 v1 = src.Point(indexList[i]) - src.Point(indexList[i_before]);
        v1.Normalize();
        Vector3 v2 = src.Point(indexList[i_after]) - src.Point(indexList[i]);
        v2.Normalize();

        if (v1 * v2 < 0.95f // we can be without that point
          || used + (stop - i) == 3) // at least even wrong 3 indexes must left
        {
          _indexes.Add(indexList[i]);
          used++;
        }
      }
    }      
  }
  else
  {
    for(int i = 0; i < size; i++) if (indexList[i] >=0 ) _indexes.Add(indexList[i]);
  }
  for(int i = 0; i < src.NFaces(); i++) if (faces->FaceSelected(i)) _faces.Add(i);
}

void O2Polygon::Create(int * indexList, int size, Selection * faces,const ObjectData& src, AutoArray<int>& numUse)
{

  for(int i = 0; i < src.NFaces(); i++) if (faces->FaceSelected(i)) _faces.Add(i);

  // local use of vertexes
  AutoArray<int> localUse;
  localUse.Resize(src.NPoints());
  for(int i = 0; i < src.NPoints(); i++) localUse[i] = 0;

  for(int i = 0; i < _faces.Size(); i++)
  {
    FaceT f(&src, _faces[i]);
    for(int j = 0; j < f.N(); j++)
    {
      localUse[f.GetPoint(j)]++;
    }
  }

  // filter points on lines
  for(int i = 0; i < size; i++) 
  {
    int stop = i + 1;
    for(;stop < size && indexList[stop] >= 0; stop++);

    int start = i;
    ASSERT(stop - start >= 3);
    int notUse = 0;
    for(; i < stop; i++)
    {    
      _indexes.Add(indexList[i]);
      int i_before = i != start? i - 1 : stop - 1;
      int i_after = i != (stop - 1)? i + 1 : start;

      Vector3 v1 = src.Point(indexList[i]) - src.Point(indexList[i_before]);
      v1.Normalize();
      Vector3 v2 = src.Point(indexList[i_after]) - src.Point(indexList[i]);
      v2.Normalize();

      if (v1 * v2 > 0.9999f && // we can be without that point
        (stop - start - notUse) > 3) // at least even wrong 3 indexes must left
      {
        numUse[indexList[i]]-= localUse[indexList[i]];    
        notUse++;
      }
    }
  }  
}

struct  FaceToDelete
{
  int _face;
  int _first;
  int _last;

  int _next;
 ClassIsSimple(FaceToDelete);
};

void ObjToolTopology::RetesellatePolygon(Selection& border, Selection& faces, Selection * deleteFaces, Selection * deletePoints)
{
  Selection toDelete(this); // select points to delete
  toDelete = faces;
  toDelete.SelectPointsFromFaces();
  toDelete -= border;

  /// Move face selection into objectdata it will be than updated automaticaly...
  for(int i = 0; i < NFaces(); i++)
  {
    FaceSelect(i, faces.FaceSelected(i));
  }

  int n = toDelete.NPoints();
  for(int i = 0; i < n; i++)
  {
    // find new candidate
    int vertToDelete = 0;
    for(; vertToDelete < NPoints(); vertToDelete++)
    {
      if (toDelete.PointSelected(vertToDelete))
        break;
    }
    toDelete.PointSelect(vertToDelete, false);
    if (deletePoints)
      deletePoints->PointSelect(vertToDelete, true);

    ASSERT(vertToDelete != NPoints());

    // Find Faces
    AutoArray<FaceToDelete> faceToDelete;

    int nBorderInd = 0; 
    for(int k = 0; k < NFaces(); k++)
    {
      if (!FaceSelected(k))
        continue;

      FaceT f(this,k);

      int fp = f.PointAt(vertToDelete);
      if (fp < 0)
        continue;

      nBorderInd += f.N() - 1; 

      FaceToDelete& ftd = faceToDelete.Append();

      ftd._face = k;
      ftd._first = f.GetPoint((fp + 1) == f.N() ? 0 : (fp + 1));
      ftd._last = f.GetPoint((fp - 1) < 0 ?  (f.N() - 1): (fp - 1));
    }
    if (faceToDelete.Size() == 0) // TODO just fixing crash I dunno why faceToDelete is empty
      continue; 

    // polygon from faces       
    int first = 0;
    for(int k = 0; k < faceToDelete.Size(); k++) faceToDelete[k]._next = -1;
     
    for(int k = 0; k < faceToDelete.Size(); k++)
    {     
      int searchFor = faceToDelete[k]._first;
      int l = 0;
      for(;l < faceToDelete.Size(); l++)
      {
        if (faceToDelete[l]._last == searchFor)
        {
          faceToDelete[l]._next = k;
          break;
        }
      }

      if (l == faceToDelete.Size())
        first = k;
    }

    O2Polygon polygon;

    int pos = first;
    do {
      FaceToDelete& ftd = faceToDelete[pos];
      polygon._faces.Add(ftd._face);
      FaceSelect(ftd._face, false);
      if (deleteFaces)
        deleteFaces->FaceSelect(ftd._face);
      
      FaceT f(this,ftd._face);
      int fp = f.PointAt(vertToDelete);
      ASSERT(fp >= 0);

      int fpend = fp == 0 ? f.N() - 1: fp - 1;
      for(int k = (fp + 1) == f.N() ? 0 : (fp + 1); k != fpend; ++k == f.N() ? k = 0: 0)
      {
        polygon._indexes.Add(f.GetPoint(k));
      }

      pos = ftd._next;
      if (pos < 0)
        polygon._indexes.Add(f.GetPoint(fpend));

    } while (pos != first && pos >= 0);

    // because of quads there can be 1 point several times.    

    for(int j = 0; j < polygon._indexes.Size() && polygon._indexes.Size() >= 3; j++)
    {
      if (polygon._indexes[j] == polygon._indexes[j < 2 ? polygon._indexes.Size() - (2 - j) : j - 2])
      {
        polygon._indexes.DeleteAt(j); 
        j--;
      }
    }

    
    polygon.PreparePar(this);
    if (polygon._indexes.Size() >= 3) // do not create degenerated faces...
      TessellatePolygon(polygon._indexes.Size(), polygon._indexes.Data(), polygon._uvlist.Data(), polygon._textureName, polygon._materialName); // retesellate 
  }
}

int ObjToolTopology::FindPolygon(int startFace, const CEdges &faceGraph, int *indexList, 
                    int indexCount,  unsigned long flags, 
                    const CEdges *sharpEdges/*=NULL*/, Selection *polyfaces/*=NULL*/, Selection *workOn/*=NULL*/) const
{
  if (indexCount<3) return 0;
  int maxFaces=indexCount-2;
  unsigned char *marked=(unsigned char *)alloca(NFaces()/8 + 1);
  memset(marked,0,NFaces()/8 + 1);
  int *faceList=(int *)alloca(sizeof(int)*NFaces()); 
    
  float fprecision=(float)pow(0.1f,(int)(flags & 0xF));
  fprecision*=fprecision;

  int faceAddNext=1; 
  int faceReadNext=0;
  CEdges outsideEdges(NPoints());
  CEdges insideEdges(NPoints());

  faceList[0]=startFace; //add first face into list;
  marked[startFace>>3]|=(1<<(startFace & 7));  
  Vector3 fcn;
  {
    FaceT fc(this,startFace);
    fcn=fc.CalculateNormal();
  }
  while (faceAddNext>faceReadNext)  //while there is any face in list;
  {
    int fcid=faceReadNext++;
    FaceT fc(this,faceList[fcid]);
    int l=fc.N()-1;
    for (int f=0;f<fc.N();l=f++)
    {
      int a=fc.GetPoint(l);
      int b=fc.GetPoint(f);
      if (!insideEdges.IsEdge(a,b))
      {
        if (outsideEdges.IsEdge(a,b))
        {
          outsideEdges.RemoveEdge(a,b);
          insideEdges.SetEdge(a,b);
        }
        else
        {
          outsideEdges.SetEdge(a,b);
        }
      }      
    }
    for (int i=0,p;(p=faceGraph.GetEdge(faceList[fcid],i))!=-1;i++)
    {
      if (workOn && !workOn->FaceSelected(p)) continue;
      if (faceAddNext>=maxFaces) break;
      unsigned char markedbit=1<<(p & 7);
      int markedpos=p>>3;
      if (marked[markedpos] & markedbit) continue;
      bool validFace=true;
      FaceT nxFc(this,p);
      if (sharpEdges)
      {
        int a,b;
        nxFc.GetEdge(fc,a,b);
        validFace=!sharpEdges->IsEdge(a,b);
      }     
      if (validFace && (flags & fpSameFlags))
      {
        validFace=nxFc.GetFlags()==fc.GetFlags();
      }
      if (validFace && (flags & fpSameMaterial))
      {
        validFace=_stricmp(fc.GetMaterial(),nxFc.GetMaterial())==0;
      }
      if (validFace && (flags & fpSameTextue))
      {
        validFace=_stricmp(fc.GetTexture(),nxFc.GetTexture())==0;
      }
      if (validFace)
      {
        Vector3 nxn=nxFc.CalculateNormal();
        float d=nxn.Distance2(fcn);
        validFace=(d<fprecision);
      }
      if (validFace && (flags & fpSameMapping))
      {
        validFace=nxFc.HasSameMappingOnEdge(fc);
      }
      if (validFace)
      {        
        faceList[faceAddNext++]=p;
        marked[markedpos]|=markedbit;
      }
    }
    if (polyfaces) polyfaces->FaceSelect(fc.GetFaceIndex());
  }
 
  // return 
  int cnt = 0;
  while (true)
  {
    int start = 0;
    for (; start<NPoints();start++)
    {
      if (outsideEdges.GetEdge(start,0) != -1)
        break;     
    }

    if (start == NPoints())
      return cnt;

    if (cnt != 0) indexList[cnt++]=-1;

    int pos = start;    
    for (;cnt<indexCount;)
    {
      indexList[cnt++]=pos;      
      int posnew = outsideEdges.GetEdge(pos,0);
      outsideEdges.RemoveEdge(pos, posnew);
      pos = posnew;      
      if (pos==start) break;
      if (pos < 0 || pos >= NPoints())
      {
        ASSERT(pos >= 0 && pos < NPoints());
        break;
      }
    }

    if (cnt == indexCount)
      return cnt;    
  }
}

void ObjToolTopology::MoveUVInPolygon(int startFace, const CEdges &faceGraph, unsigned long flags, 
                     const CEdges *sharpEdges/*=NULL*/, Selection *polyfaces/*=NULL*/,Selection *workOn/*=NULL*/)
{  
  unsigned char *marked=(unsigned char *)alloca(NFaces()/8 + 1);
  memset(marked,0,NFaces()/8 + 1);
  int *faceList=(int *)alloca(sizeof(int)*NFaces()); 

  float fprecision=(float)pow(0.1f,(int)(flags & 0xF));
  fprecision*=fprecision;

  int faceAddNext=1; 
  int faceReadNext=0;  

  faceList[0]=startFace; //add first face into list;
  marked[startFace>>3]|=(1<<(startFace & 7));  
  Vector3 fcn;
  {
    FaceT fc(this,startFace);
    fcn=fc.CalculateNormal();
  }
  while (faceAddNext>faceReadNext)  //while there is any face in list;
  {
    int fcid=faceReadNext++;
    FaceT fc(this,faceList[fcid]);
    int l=fc.N()-1;
    
    for (int i=0,p;(p=faceGraph.GetEdge(faceList[fcid],i))!=-1;i++)
    {
      if (workOn && !workOn->FaceSelected(p)) continue;     
      unsigned char markedbit=1<<(p & 7);
      int markedpos=p>>3;
      if (marked[markedpos] & markedbit) continue;
      bool validFace=true;
      FaceT nxFc(this,p);
      if (sharpEdges)
      {
        int a,b;
        nxFc.GetEdge(fc,a,b);
        validFace=!sharpEdges->IsEdge(a,b);
      }     
      if (validFace && (flags & fpSameFlags))
      {
        validFace=nxFc.GetFlags()==fc.GetFlags();
      }
      if (validFace && (flags & fpSameMaterial))
      {
        validFace=_stricmp(fc.GetMaterial(),nxFc.GetMaterial())==0;
      }
      if (validFace && (flags & fpSameTextue))
      {
        validFace=_stricmp(fc.GetTexture(),nxFc.GetTexture())==0;
      }
      if (validFace)
      {
        Vector3 nxn=nxFc.CalculateNormal();
        float d=nxn.Distance2(fcn);
        validFace=(d<fprecision);
      }
      if (validFace)
      {
        validFace=nxFc.ContinueMappingOnEdge(fc);
      }
      if (validFace)
      {        
        faceList[faceAddNext++]=p;
        marked[markedpos]|=markedbit;
      }
    }
    if (polyfaces) polyfaces->FaceSelect(fc.GetFaceIndex());
  } 
}

struct ObjToolTopology_OrderInfo
{
  int index;
  float angle;
};

static int SortOrder(const void *a, const void *b)
{
  const ObjToolTopology_OrderInfo *oa=reinterpret_cast<const ObjToolTopology_OrderInfo *>(a);
  const ObjToolTopology_OrderInfo *ob=reinterpret_cast<const ObjToolTopology_OrderInfo *>(b);
  return (oa->angle>ob->angle)-(oa->angle<ob->angle);
}

void ObjToolTopology::SortAroundPoint(Array<int> &ptlist, const Vector3 &up, const Vector3 *center) {

  ObjToolTopology_OrderInfo *order=(ObjToolTopology_OrderInfo *)alloca(ptlist.Size()*sizeof(ObjToolTopology_OrderInfo));

  VecT tmpcnt(0,0,0);

  if (center == NULL) 
  {      
    center=&tmpcnt;
    for (int i = 0; i < ptlist.Size(); i++) tmpcnt = tmpcnt + Point(ptlist[i]);
    tmpcnt = tmpcnt / (Coord)ptlist.Size();
  }

  for (int i = 0; i < ptlist.Size(); i++)
      order[i].index = ptlist[i];
  
  Vector3 a=Point(order[0].index);
  order[0].angle=-FPI;
  for (int i=1;i<ptlist.Size();i++)
  {
    Vector3 b=Point(order[i].index);
    order[i].angle=GetTriangleAngle(a,*center,b,up);
  }

  qsort(order,ptlist.Size(),sizeof(*order),SortOrder);

  for (int i = 0; i < ptlist.Size(); i++)
      ptlist[i] = order[i].index;

}

bool ObjToolTopology::TessellateFromPointSelection(const Selection &vertices,      //selected vertices to tessellatte
                                        const Selection *templateFaces, //faces used as source of mapping U,V
                                        const char *textureName, //new texture name
                                        const char *materialName, // new material name
                                        const VecT *center) //point of view, NULL - will be calculated using GetCenterOfSelection
{
  float *uvlist=NULL;
  float *utemp=NULL;
  float *vtemp=NULL;
  int vxcount=vertices.NPoints();
  ObjToolTopology_OrderInfo *order=(ObjToolTopology_OrderInfo *)alloca(vxcount*sizeof(ObjToolTopology_OrderInfo));

  VecT tmpcnt;
  if (center==NULL) 
  {
    center=&tmpcnt;
    tmpcnt=vertices.GetCenter();
  }
  
  Vector3 sumnorm(0,0,0);
  {        
    int lastPt=-1;
    FaceT helpfc(this);    
    helpfc.SetN(3);    
    for (int i=0,cnt=NPoints(),pos=0;i<cnt;i++) if (vertices.PointSelected(i))
    {
      order[pos++].index=i;
      if (lastPt==-1) {helpfc.SetPoint(0,i);lastPt=0;}
      else if (lastPt==0) lastPt=i;
      else
      {
        helpfc.SetPoint(1,lastPt);
        helpfc.SetPoint(2,i);
        Vector3 normal=helpfc.CalculateRawNormal();
        if (normal.CosAngle(sumnorm)<0) normal=-normal;
        sumnorm+=normal;
        lastPt=i;        
      }
      
    }
  }
  sumnorm=sumnorm.Normalized();

  Vector3 a=Point(order[0].index);
  order[0].angle=-FPI;
  for (int i=1;i<vxcount;i++)
  {
    Vector3 b=Point(order[i].index);
    order[i].angle=GetTriangleAngle(a,*center,b,sumnorm);
  }
  qsort(order,vxcount,sizeof(*order),SortOrder);
  
  if (templateFaces) 
  {
    uvlist=(float *)alloca(vertices.NPoints()*2*sizeof(float));
    utemp=new float[NPoints()];
    vtemp=new float[NPoints()];
    for (int i=0,cnt=NFaces();i<cnt;i++) if (templateFaces->FaceSelected(i))
    {
      FaceT fc(this,i);
      for (int j=0;j<fc.N();j++)
      {
        utemp[fc.GetPoint(j)]=fc.GetU(j);
        vtemp[fc.GetPoint(j)]=fc.GetU(j);
      }
    }
    for (int i=0;i<vxcount;i++)
    {
      uvlist[i*2]=utemp[order[i].index];
      uvlist[i*2+1]=vtemp[order[i].index];
    }
    delete [] utemp;
    delete [] vtemp;
  }
  int *verticeList=reinterpret_cast<int *>(order);
  for (int i=0;i<vxcount;i++)
    verticeList[i]=order[i].index;
  int n=NFaces();
  bool succ=TessellatePolygon(vxcount,verticeList,uvlist,textureName,materialName);  
  for (int i=n,cnt=NFaces();i<cnt;i++) FaceSelect(i);
  return succ;
}
class All
{
public:
  bool operator()(const O2Polygon& pol1, const O2Polygon& pol2) const {return true;};
  unsigned long  FindPolyFlags() const {return ObjToolTopology::fpSameTextue | ObjToolTopology::fpSameMaterial | ObjToolTopology::fpSameMapping | ObjToolTopology::fpPrecisionMedium | ObjToolTopology::fpSameFlags;};  
};

void ObjToolTopology::SelectDoublePolygons(const Selection * in, Selection& out) const
{
  All all;
  SelectDoublePolygons(in, out,all);
};

void ObjToolTopology::RetesellatePolygons()
{
  /// Create field with number of point usage...
  
  AutoArray<int> numUse; 
  numUse.Resize(NPoints());

  for(int i = 0; i < NPoints(); i++) numUse[i] = 0;
  for(int i = 0;i < NFaces(); i++)
  { 
    FaceT f(this, i);
    for(int j = 0; j < f.N(); j++)
      numUse[f.GetPoint(j)]++;
  }


  // find polygons
  int indexList[1024];
  AutoArray<O2Polygon> polygons;

  Selection usedFaces(this);

  CEdges faceGraph;
  faceGraph.SetVertices(NFaces());
  CEdges nocross;

  BuildFaceGraph(faceGraph, nocross);
  

  Selection workOn(*GetSelection());
  for(int i = 0;i < NFaces(); i++)
  {  
    if (!workOn.FaceSelected(i) || usedFaces.FaceSelected(i))
      continue;

    Selection sel(this);
    int nIndexes = FindPolygon(i, faceGraph, indexList, 1024, 
      fpPrecisionMedium | fpSameTextue | fpSameMaterial | fpSameFlags | fpSameMapping, NULL, &sel, &workOn);

    if (nIndexes < 0)
      continue;
    // add selection
    usedFaces += sel;
    workOn -= sel;
    polygons.Append().Create(indexList,nIndexes,&sel,*this,numUse);   
  }
 
  // re-tesellate them
  ClearSelection();
 
  SaveNamedSel("temp_RetesellatePolygons_faces");
  NamedSelection * deleteFaces = GetNamedSel("temp_RetesellatePolygons_faces");

  SaveNamedSel("temp_RetesellatePolygons_points");
  NamedSelection * deletePoints = GetNamedSel("temp_RetesellatePolygons_points");
 
  for(int i = 0;i < polygons.Size(); i++)
  {
    O2Polygon& pol = polygons[i]; 
    pol.CleanUnused(numUse);
    Selection border(this);     
    Selection faces(this);
    pol.Select(border, faces);   
    
    RetesellatePolygon(border, faces, deleteFaces, deletePoints);    
    
  }

  SelectionDeleteFaces(deleteFaces);  

  GetTool<ObjToolCheck>().IsolatedPoints();
  SelectionDelete();

  *deletePoints &= *GetSelection(); // must be also isolated points...

  Selection tmp(this);
  tmp = *deletePoints;
  SelectionDelete(&tmp); 
};

void ObjToolTopology::MoveUVInInterval01()
{
  for(int i = 0;i < NFaces(); i++)
  { 
    if (!FaceSelected(i))
      continue;

    float minU = FLT_MAX;
    float minV = FLT_MAX;

    FaceT f(this, i);
    for(int j = 0; j < f.N(); j++)
    {
      minU = min(f.GetU(j), minU);
      minV = min(f.GetV(j), minV);
    }


    if (minU  < 0 || minU  >= 1 || minV   < 0 || minV >= 1)
    {
      minU=-floor(minU);
      minV=-floor(minV);

      for(int j = 0; j < f.N(); j++)
      {
        f.SetU(j,f.GetU(j) + minU);
        f.SetV(j,f.GetV(j) + minV);
      }
    }
  }
}

void ObjToolTopology::MoveUVInPolygons()
{
  Selection usedFaces(this);

  CEdges faceGraph;
  faceGraph.SetVertices(NFaces());
  CEdges nocross;

  BuildFaceGraph(faceGraph, nocross);


  Selection workOn(*GetSelection());
  for(int i = 0;i < NFaces(); i++)
  {  
    if (!workOn.FaceSelected(i) || usedFaces.FaceSelected(i))
      continue;

    Selection sel(this);
    MoveUVInPolygon(i, faceGraph, 
      fpPrecisionMedium | fpSameTextue | fpSameMaterial | fpSameFlags , NULL, &sel, &workOn);
   
    // add selection
    usedFaces += sel;
    workOn -= sel;
  }
};

bool ObjIsFaceAlphaDefault::IsFaceAlpha(const FaceT &fc) const
{
  const char *tex=fc.GetTexture(0);
  const char *ext=Pathname::GetExtensionFromPath(tex);
  if (_stricmp(ext,".paa")==0) return true;
  ext-=3;
  if (ext>tex && _strnicmp(ext,"_ca",3)==0) return true;
  return false;
}



template<class A>
static bool BothInCycle(const A &array, int a, int b)
{
  int mask=0;
  for (int i=0;i<array.Size();i++)
  {
    if (a==array[i]) mask|=1;
    if (b==array[i]) mask|=2;
    if (mask==3) return true;
  }
  return false;
}

void ObjToolTopology::SortAlphaFaces(const IObjIsFaceAlpha &alphaTester,const Selection *sel)
{
  AutoArray<int,MemAllocLocal<int,256> > alphaIndex;
  AutoArray<int,MemAllocLocal<int,256> > sorter;
  for (int i=0;i<NFaces();i++) if (sel==0 || sel->FaceSelected(i))
  {
    FaceT fc(this,i);
    if (alphaTester.IsFaceAlpha(fc))
    {
      alphaIndex.Add(i);      
    }
  }

  sorter=alphaIndex;
 
  CEdges detectCycle(sorter.Size());

  ProgressBar<> pb(sorter.Size()*(sorter.Size()+1)/2);

  for (int i=0;i<sorter.Size();i++)
  {
    int testFace=i;
    bool rep=true;
    detectCycle.Clear();
    pb.AdvanceNext(sorter.Size()-i);
    while(rep)
    {
      if (pb.StopSignaled()) goto stopped;
      rep=false;;
      for (int k=i;k<sorter.Size();k++) if (k!=testFace)
      {
        FaceT t1(this,sorter[testFace]);
        FaceT t2(this,sorter[k]);
        FaceT::FaceToFaceLocation test=t1.FaceLocation2(t2);
        if (test==FaceT::fAbove)
        {
          if (!detectCycle.IsEdgeInDirection(testFace,k)) 
          {
            rep=true; 
            detectCycle.SetEdge(testFace,k); 
            testFace=k;
          }
          else
          {
            if (testFace<k)
            {
              int x=sorter[k];
              sorter[k]=sorter[testFace];
              sorter[testFace]=x;
              detectCycle.RemoveEdge(testFace,k);
              detectCycle.SetEdge(k,testFace);
            }
          }
        }
      }    
    }
    if (testFace!=i)
    {
      int x=sorter[i];
      sorter[i]=sorter[testFace];
      sorter[testFace]=x;
    }
  }
stopped:;
  ObjectData copy=*this;
  for (int i=0;i<alphaIndex.Size();i++)
  {
    FaceT trg(this,alphaIndex[i]);
    FaceT src(copy,sorter[i]);
    src.CopyFaceTo(trg);
  }
}

struct FaceSearchInfo
{
  int vertices[MAX_DATA_POLY];
  int cnt;
public:
  FaceSearchInfo() {}
  FaceSearchInfo(int i) {}
  FaceSearchInfo(FaceT &fc)
  {
    for (int i=0;i<fc.N();i++)    
      vertices[i]=fc.GetPoint(i);
    cnt=fc.N();
    for (int i=0;i<cnt;i++)
      for (int j=0;j<(i-1);j++)
        if (vertices[j]>vertices[j+1])
        {
          int x=vertices[j];
          vertices[j]=vertices[i];
          vertices[i]=x;
        }
  }

  int CompareWith(const FaceSearchInfo &other) const
  {
    if (cnt!=other.cnt) return cnt>other.cnt?1:-1;
    return memcmp(vertices,other.vertices,cnt*sizeof(int));
  }
  
  bool operator==(const FaceSearchInfo &other) const {return CompareWith(other)==0;}
  bool operator>=(const FaceSearchInfo &other) const {return CompareWith(other)>=0;}
  bool operator<=(const FaceSearchInfo &other) const {return CompareWith(other)<=0;}
  bool operator!=(const FaceSearchInfo &other) const {return CompareWith(other)!=0;}
  bool operator>(const FaceSearchInfo &other) const {return CompareWith(other)>0;}
  bool operator<(const FaceSearchInfo &other) const {return CompareWith(other)<0;}

  void SelectPoints(Selection *sel) const
  {
    for (int i=0;i<cnt;i++) sel->PointSelect(vertices[i]);
  }

};

void ObjToolTopology::SelectDoubleSided(Selection *inselection)
{
  BTree<FaceSearchInfo> dbfaces;
  Selection res(this);
  for (int i=0;i<NFaces();i++) if (inselection==0 || inselection->FaceSelected(i))
  {
    FaceT fc(this,i);
    FaceSearchInfo srch(fc);
    if (dbfaces.Find(srch)==0)
    {
      dbfaces.Add(srch);
    }
    else
    {
      srch.SelectPoints(&res);
    }
  }
  if (inselection)
    *inselection=res;
  else
    UseSelection(&res);
}

void ObjToolTopology::BreakApart(const Selection *selection) {

    CEdges edges(NPoints());
    CEdges edges2(NPoints());
    CEdges nocross(NPoints());

    Selection breakPts(this);

    for (int i = 0; i < NFaces(); i++) if (selection->FaceSelected(i)) {

        FaceT fc(this,i);
        for (int j = 0, k = fc.N()-1; j<fc.N(); k = j++) {          
            if (edges.IsEdge(fc.GetPoint(k),fc.GetPoint(j))) {
                if (edges2.IsEdge(fc.GetPoint(k),fc.GetPoint(j))) 
                    nocross.SetEdge(fc.GetPoint(k),fc.GetPoint(j));
                else
                    edges2.SetEdge(fc.GetPoint(k),fc.GetPoint(j));
            }
            else 
                edges.SetEdge(fc.GetPoint(k),fc.GetPoint(j));
        }        
    }

    CEdges faceGraph(NFaces());
    BuildFaceGraph(faceGraph,CEdges());

    Selection allwork(*selection);
    int start = allwork.GetSelectedFacesRange().first;
    Selection work(this);
    while (start != -1) {      
        work.Clear();
        std::queue<int> fqueue;
        fqueue.push(start);
        work.FaceSelect(start);
        while (!fqueue.empty()) {
            int ich = fqueue.front();
            fqueue.pop();

            FaceT fcIch(this,ich);
            for (int pos = 0, trg; (trg = faceGraph.GetEdge(ich,pos)) != -1; ++pos) {

                if (work.FaceSelected(trg) == false) {

                    FaceT fcTrg(this,trg);
                    int v1,v2;
                    fcTrg.GetEdge(fcIch,v1,v2);
                    if (!nocross.IsEdge(v1,v2)) {
                        fqueue.push(trg);
                        work.FaceSelect(trg);
                    }
                }
            }
        }

        work.SelectPointsFromFaces();
        SelectionSplit(&work);
        
        allwork -= work;
        start = allwork.GetSelectedFacesRange().first;
        
    }



    SelectionDelete(&breakPts);
}
}

