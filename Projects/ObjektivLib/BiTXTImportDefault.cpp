#include "common.hpp"
#include <malloc.h>
#include "LODObject.h"
#include "ObjectData.h"
#include "BiTXTimportDefault.h"


#define IDS_LOD_GEOMETRY        "Geometry"
#define IDS_LOD_xxx             "???"
#define IDS_LOD_MEMORY          "Memory"
#define IDS_LOD_LANDCONTACT     "LandContact"
#define IDS_LOD_ROADWAY         "RoadWay"
#define IDS_LOD_PATHS           "Paths"
#define IDS_LOD_HPOINTS         "Hit-points"
#define IDS_LOD_VIEWGEO         "View Geometry"
#define IDS_LOD_FIREGEO         "Fire Geometry"
#define IDS_LOD_VIEW_GUNNER     "View - Gunner"
#define IDS_LOD_VIEW_PILOT      "View - Pilot"
#define IDS_LOD_VIEW_CARGO      "View - Cargo"
#define IDS_LOD_VIEW_CARGO_GEOMETRY "View - Cargo - Geometry"
#define IDS_LOD_VIEW_CARGO_FGEOMETRY "View - Cargo - Fire Geometry"
#define IDS_LOD_VIEW_COMMANDER  "View - Commander"
#define IDS_LOD_VIEW_COMM_GEOMETRY "View - Commander -Geometry"
#define IDS_LOD_VIEW_COMM_FGEOMETRY "View - Commander -Fire Geometry"
#define IDS_LOD_VIEW_PILOT_GEOMETRY "View - Pilot - Geometry"
#define IDS_LOD_VIEW_PILOT_FGEOMETRY "View - Pilot - Fire Geometry"
#define IDS_LOD_VIEW_GUNNER_GEOMETRY "View - Gunner - Geometry"
#define IDS_LOD_VIEW_GUNNER_FGEOMETRY "View - Gunner - Fire Geometry"
#define IDS_LOD_SHADOW          "ShadowVolume"
#define IDS_LOD_EDITTEMP        "Edit"
#define IDS_LOD_VIEWCARGOSHADOWVOLUME "View - Cargo - ShadowVolume"
#define IDS_LOD_VIEWPILOTSHADOWVOLUME "View - Pilot - ShadowVolume"
#define IDS_LOD_VIEWGUNNERSHADOWVOLUME "View - Gunner - ShadowVolume"
#define IDS_LOD_WRECK "Wreck"
#define IDS_LOD_VIEWCARGOSHADOWVOLUME2 "ShadowVolume - View - Cargo"
#define IDS_LOD_VIEWPILOTSHADOWVOLUME2 "ShadowVolume - View - Pilot"
#define IDS_LOD_VIEWGUNNERSHADOWVOLUME2 "ShadowVolume - View - Gunner"
#define IDS_LOD_SUB_PARTS "Sub Parts"


#define strCockpitGunner IDS_LOD_VIEW_GUNNER
#define strCockpitPilot IDS_LOD_VIEW_PILOT
#define strCockpitCommander IDS_LOD_VIEW_CARGO

#define GetStringRes(ids) ids

namespace ObjektivLib {

int StrICmpIgnoreChars(const char* _str1,const char* _str2,const char* _ignore = "")
{
  const char * _i= _str1, *_j = _str2;
  for(; *_i!=0 && *_j!=0;++_i,++_j)
  {
    while (strchr(_ignore,*_i)) ++_i;
    while (strchr(_ignore,*_j)) ++_j;
    if ((((*_j>='a')&&(*_j<='z'))?(*_j-'a'+'A'):*_j)!=(((*_i>='a')&&(*_i<='z'))?(*_i-'a'+'A'):*_i)) break;
  }
  return (*_i)-(*_j);
};
int RemoveChars(const char* _str1,char* _str2,const char* _ignore = "")
{ 
  const char * _i= _str1;
  char * _j = _str2;
  for(; *_i!=0 ;++_i)
  {
    while (strchr(_ignore,*_i)) ++_i;
    *_j=*_i;
    ++_j;
  }
  *_j = '\0';
  return _str2-_j;
}


float LODNameToResolPositive(const char *name)
{
  const char ignore[] =  " _-";
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_GEOMETRY),ignore) ) return 1e13f;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_MEMORY),ignore) ) return SPEC_LOD*1;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_LANDCONTACT),ignore) ) return SPEC_LOD*2; 
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_ROADWAY),ignore) ) return SPEC_LOD*3;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_PATHS),ignore) ) return SPEC_LOD*4;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_HPOINTS),ignore) ) return SPEC_LOD*5;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEWGEO),ignore) ) return SPEC_LOD*6;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_FIREGEO),ignore) ) return SPEC_LOD*7;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEW_CARGO_GEOMETRY),ignore) ) return SPEC_LOD*8;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEW_CARGO_FGEOMETRY),ignore) ) return SPEC_LOD*9;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEW_COMMANDER),ignore) ) return SPEC_LOD*10;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEW_COMM_GEOMETRY),ignore) ) return SPEC_LOD*11;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEW_COMM_FGEOMETRY),ignore) ) return SPEC_LOD*12;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEW_PILOT_GEOMETRY),ignore) ) return SPEC_LOD*13;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEW_PILOT_FGEOMETRY),ignore) ) return SPEC_LOD*14;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEW_GUNNER_GEOMETRY),ignore) ) return SPEC_LOD*15;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEW_GUNNER_FGEOMETRY),ignore) ) return SPEC_LOD*16;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEWCARGOSHADOWVOLUME),ignore) ) return SPEC_LOD*18;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEWCARGOSHADOWVOLUME2),ignore) ) return SPEC_LOD*18;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEWPILOTSHADOWVOLUME),ignore) ) return SPEC_LOD*19;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEWPILOTSHADOWVOLUME2),ignore) ) return SPEC_LOD*19;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEWGUNNERSHADOWVOLUME),ignore) ) return SPEC_LOD*20;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_VIEWGUNNERSHADOWVOLUME2),ignore) ) return SPEC_LOD*20;
  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_WRECK),ignore) ) return SPEC_LOD*21;


  if( !StrICmpIgnoreChars(name,GetStringRes(IDS_LOD_SUB_PARTS),ignore) )return SPEC_LOD*17;
  if( !StrICmpIgnoreChars(name,GetStringRes(strCockpitGunner),ignore) ) return 1000;
  if( !StrICmpIgnoreChars(name,GetStringRes(strCockpitPilot),ignore) ) return 1100;
  if( !StrICmpIgnoreChars(name,GetStringRes(strCockpitCommander),ignore) ) return 1200;

  char *newName = (char *) malloc(strlen(name)+1);
  RemoveChars(name,newName,ignore);

  RString sprf=GetStringRes(IDS_LOD_SHADOW);
  if (!strncmp(newName,sprf,sprf.GetLength()))
    {
    float z=0.0f;
    sscanf(newName+sprf.GetLength(),"%f",&z);
    return z+10000.0f;
    }
  sprf=GetStringRes(IDS_LOD_EDITTEMP);
  if (!strncmp(newName,sprf,sprf.GetLength()))
    {
    float z=0.0f;
    sscanf(newName+sprf.GetLength(),"%f",&z);
    return z+20000.0f;
    }
  return (float)atof(newName);

}

float CBiTXTImportDefault::LODNameToResol(const char *name)
{
  if (name[0] != '-')
    return LODNameToResolPositive(name);

  return - LODNameToResolPositive(name + 1);
}

#define GetStringRes(ids) RStringB(ids)

RStringB ResolToName(float resol)
{
  char buff[256];
  if (ISMASS(resol)) return GetStringRes(IDS_LOD_GEOMETRY);
  if( resol>SPEC_LOD*0.5 )
  {
    int resolId=(resol+SPEC_LOD*0.5)/SPEC_LOD;
    switch( resolId )
    {
    case 0: return GetStringRes(IDS_LOD_xxx);
    case 1: return GetStringRes(IDS_LOD_MEMORY);
    case 2: return GetStringRes(IDS_LOD_LANDCONTACT);
    case 3: return GetStringRes(IDS_LOD_ROADWAY);
    case 4: return GetStringRes(IDS_LOD_PATHS);
    case 5: return GetStringRes(IDS_LOD_HPOINTS);
    case 6: return GetStringRes(IDS_LOD_VIEWGEO);
    case 7: return GetStringRes(IDS_LOD_FIREGEO);
    case 8: return GetStringRes(IDS_LOD_VIEW_CARGO_GEOMETRY);
    case 9: return GetStringRes(IDS_LOD_VIEW_CARGO_FGEOMETRY);
    case 10: return GetStringRes(IDS_LOD_VIEW_COMMANDER);
    case 11: return GetStringRes(IDS_LOD_VIEW_COMM_GEOMETRY);
    case 12: return GetStringRes(IDS_LOD_VIEW_COMM_FGEOMETRY);
    case 13: return GetStringRes(IDS_LOD_VIEW_PILOT_GEOMETRY);
    case 14: return GetStringRes(IDS_LOD_VIEW_PILOT_FGEOMETRY);
    case 15: return GetStringRes(IDS_LOD_VIEW_GUNNER_GEOMETRY);
    case 16: return GetStringRes(IDS_LOD_VIEW_GUNNER_FGEOMETRY);
    case 17: return GetStringRes(IDS_LOD_SUB_PARTS);
    case 18: return GetStringRes(IDS_LOD_VIEWCARGOSHADOWVOLUME);
    case 21: return GetStringRes(IDS_LOD_WRECK);
    default:
      {
        char buff[256];
        sprintf(buff,"%g Spec",resol);        
        return GetStringRes(buff);
      }
    }
  }
  if( resol>999 && resol<1001 ) return GetStringRes(strCockpitGunner);
  if( resol>1099 && resol<1101 ) return GetStringRes(strCockpitPilot);
  if( resol>1199 && resol<1201 ) return GetStringRes(strCockpitCommander);
  if (resol>=10000 && resol<20000) 
  {
    sprintf(buff,"%s %.3f",GetStringRes(IDS_LOD_SHADOW).Data(),resol-10000.0f);        
    return GetStringRes(buff);
  }
  else if (resol>=20000 && resol<30000) 
  {
    sprintf(buff,"%s %.3f",GetStringRes(IDS_LOD_EDITTEMP).Data(),resol-20000.0f);        
    return GetStringRes(buff);
  }
  else
  {
    snprintf(buff,sizeof(buff),"%7.3f",resol);
    return GetStringRes(buff);
  }
}

RStringB CBiTXTImportDefault::LODResolToName(float resol) const
{
  return ResolToName(resol);
}

}