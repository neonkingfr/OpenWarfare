#include "common.hpp"
#include ".\objtoolsmatlib.h"
#include "io.h"
#include <malloc.h>

template<>
void BTreeUnsetItem(ObjektivLib :: ObjMatLibItem &item)
  {
  item.name="";
  item.status=false;
  }

namespace ObjektivLib {




void ObjToolMatLib :: ReadMatLib(BTree<ObjMatLibItem>& container, ObjToolMatLib::ReadMode mode) const
  {
  int i;
  for (i=0;i<NFaces();i++)
    {
    FaceT fc(*this,i);
    ObjMatLibItem item;
    switch (mode)
      {
      case ReadTextures: item.name=fc.GetTexture();break;
      case ReadMaterials: item.name=fc.GetMaterial();break;
      default: return;
      }
    item.status=true;
    if (container.Find(item)==NULL) container.Add(item);
    }
  }

void ObjToolMatLib :: CheckFilesExists(BTree<ObjMatLibItem>& container,const char *basepath)
  {
  ObjMatLibItem *first=container.Smallest();
  if (first)
    {
    ObjMatLibItem *cur;
    BTreeIterator<ObjMatLibItem> itr(container);
    itr.BeginFrom(*first);
    while ((cur=itr.Next())!=NULL)
      {
      RString a(basepath,cur->name);
      cur->status=_access(a,0)==0;      
      }
    }
  }

void ObjToolMatLib::ConvContainerToArray(const BTree<ObjMatLibItem>& container, AutoArray<RString>& outarray, int convmode)
  {
  ObjMatLibItem *first=container.Smallest();
  if (first)
    {    
    outarray.Clear();
    ObjMatLibItem *cur;
    BTreeIterator<ObjMatLibItem> itr(container);
    itr.BeginFrom(*first);
    while ((cur=itr.Next())!=NULL)
      {
      if (convmode & InclExist && cur->status) outarray.Append()=cur->name;
      else if (convmode & InclNonExist && !cur->status) outarray.Append()=cur->name;     
      }
    }
  }

static const char *FindInMask(const char *text, const char *mask)
  {
  if (*mask!='*' && *mask!='?')
    {
    while (*text!=*mask && *text) text++;
    if (*text)
      {
      const char *comp=mask;
      const char *tbeg=text;
      while (*tbeg && *tbeg==*comp && *comp!='*' && *comp!='?') {tbeg++;comp++;}
      if (*tbeg==*comp || *comp=='*' || *comp=='?') return text;
      text++;
      }
    if (*mask==0) return text;
    }
  return NULL;
  }

static RString TryToRename(const char *src, const char *fndmask, const char *newname)
  {
  char *outbuff=(char *)alloca(strlen(src)+strlen(newname)+10);
  char *srccnv=(char *)_strupr(strcpy((char *)alloca(strlen(src)+1),src));
  char *fndcnv=(char *)_strupr(strcpy((char *)alloca(strlen(fndmask)+1),fndmask));
  char *outptr=outbuff;
  const char *fndptr=fndcnv;
  const char *newptr=newname;
  const char *srcptr=srccnv;
  while (true)
    {
    if (*fndptr=='*')
      {      
      fndptr++;
      const char *begfnd=FindInMask(srcptr,fndptr);
      if (begfnd==NULL) return src;
      if (*newptr=='*') 
        {
        memcpy(outptr,src+(srcptr-srccnv),begfnd-srcptr);
        outptr+=begfnd-srcptr;
        newptr++;
        }      
      srcptr=begfnd;       // a ve zdroji
      }
    else if (*fndptr=='?') 
        {
        if (!*srcptr) return src;
        *outptr++=src[srcptr-srccnv];
        srcptr++;
        fndptr++;
        if (*newptr=='?') newptr++;
        else if (*newptr!='*') return src;
        else if (*fndptr!='?') newptr++;
        }
    else 
      {      
      const char *lastfnd=fndptr;
      if (FindInMask(srcptr,fndptr)!=srcptr) return src;
      while (*fndptr && *fndptr!='*' && *fndptr!='?') {fndptr++;srcptr++;}
      while (*newptr!='*' && *newptr!='?' && *newptr) *outptr++=*newptr++;
      if (*newptr==0 || *fndptr==0) 
        {
        *outptr++=0;
        return RString(outbuff);
        }
      }
    }  
  }

void ObjToolMatLib :: RenameTexOrMat(const char *oldname, const char *newname, SearchMode smode)
  {
  for (int i=0;i<NFaces();i++)
    {    
    FaceT fc(*this,i);
    if (smode!= SearchMaterials) fc.SetTexture(TryToRename(fc.GetTexture(),oldname,newname));
    if (smode!= SearchTextures) fc.SetMaterial(TryToRename(fc.GetMaterial(),oldname,newname));
    }
  }

void ObjToolMatLib :: CreateSelection(const char *name, SearchMode smode, Selection &newsel) const
  {
  for (int i=0;i<NFaces();i++)
    {
    FaceT fc(*this,i);
    if (smode==SearchMaterials) newsel.FaceSelect(i,_stricmp(fc.GetMaterial(),name)==0);
    else if (smode==SearchTextures) newsel.FaceSelect(i,_stricmp(fc.GetTexture(),name)==0);
    else if (smode==SearchBoth) newsel.FaceSelect(i,_stricmp(fc.GetTexture(),name)==0 || _stricmp(fc.GetMaterial(),name)==0);
    }  
  newsel.SelectPointsFromFaces();
  }

void ObjToolMatLib :: SetTexOrMat(const char *texOrMat, ReadMode mode, const Selection *sel)
  {
  if (sel==NULL) sel=GetSelection();
  for (int i=0;i<NFaces();i++) if (sel->FaceSelected(i))
    {
    FaceT fc(*this,i);
    if (mode==ReadTextures) fc.SetTexture(texOrMat);
    else if (mode==ReadMaterials) fc.SetMaterial(texOrMat);
    }
  }


void ObjToolMatLib :: ExploreMaterial(const char *matName, BTree<RStringI> &container)
  {  
  ifstream in(matName,ios::in);
  if (!in) return;
  int p=in.get();
  AutoArray<char> buff;
  while (p!=EOF)
    {
    while (p!=EOF && p!='"') p=in.get();    
    if (p!=EOF)
      {
      p=in.get();
      buff.Clear();
      while (p!=EOF && p!='"' && p!='\r' && p!='\n') 
        {
        buff.Append()=(char)p;
        p=in.get();
        }
      in.get();
      buff.Append()='\0';
      const char *path=buff.Data();
      const char *ext=Pathname::GetExtensionFromPath(path);
      if (_stricmp(ext,".paa")==0 || _stricmp(ext,".pac")==0 || _stricmp(ext,".tga")==0 || _stricmp(ext,".rvmat")==0 || _stricmp(ext,".bisurf")==0)
        {
        RString res=path;
        if (container.Find(res)==NULL) container.Add(res);
        }
      }
    }
  }

}