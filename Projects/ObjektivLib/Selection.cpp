#include "common.hpp"
#include "ObjectData.h"


namespace ObjektivLib {
    

void Selection::Save( ostream &f )
{
    Validate();
    Temp<byte> vertW(vertSel.Size());
    for( int i=0; i<vertSel.Size(); i++ ) vertW[i]=-vertSel[i]; // make 0xff from 1
    if (vertSel.Size()) f.write((char *)&vertW[0],vertSel.Size()*sizeof(byte));
    if (faceSel.Size()) f.write((char *)faceSel.Data(),faceSel.Size()*sizeof(bool));
    //f.write((char *)norm,sizeNorm);
}

void Selection::Load( istream &f, int sizeVert, int sizeFace, int sizeNorm )
{
    int n=sizeVert;
    Temp<byte> vertW(n);
    if( sizeVert>0 )
    {
        f.read((char *)&vertW[0],sizeVert);
    }
    ValidateFaces(sizeFace);
    ValidatePoints(sizeVert);
    if( sizeFace>0 )
    {
        f.read((char *)faceSel.Data(),sizeFace);
    }
    for( int i=0; i<n; i++ ) vertSel[i]=-vertW[i]; // make 0xff from 1
    f.seekg(sizeNorm,ios::cur);
}

void Selection::operator += ( const Selection &src )
{
    int i;
    Validate();
    for( i=0; i<src.vertSel.Size(); i++ )
    {
        int vi=vertSel[i]+src.vertSel[i];
        if( vi>255 ) vi=255;
        vertSel[i]=vi;
    }
    for( i=0; i<src.faceSel.Size(); i++ ) faceSel[i]=faceSel[i] || src.faceSel[i];
}

void Selection::operator -= ( const Selection &src )
{
    int i;
    Validate();    
    for( i=0; i<src.vertSel.Size(); i++ )
    {
        int vi=vertSel[i]-src.vertSel[i];
        if( vi<0 ) vi=0;
        vertSel[i]=vi;
    }
    for( i=0; i<src.faceSel.Size(); i++ ) faceSel[i]=faceSel[i] && !src.faceSel[i];
}


void Selection::operator &= ( const Selection &src )
{
    int i;
    Validate();
    for( i=0; i<src.vertSel.Size(); i++ )
    {
        int vi=__min(vertSel[i],src.vertSel[i]);   
        vertSel[i]=vi;
    }
    for( i=0; i<src.faceSel.Size(); i++ ) faceSel[i]=faceSel[i] && src.faceSel[i];
}

void Selection::operator |= ( const Selection &src )
{
    int i;
    Validate();
    src.Validate();
    for( i=0; i<src.vertSel.Size(); i++ )
    {
        int vi=__max(vertSel[i],src.vertSel[i]);
        vertSel[i]=vi;
    }
    for( i=0; i<src.faceSel.Size(); i++ ) faceSel[i]=faceSel[i] || src.faceSel[i];
}

Selection::Selection( const Selection &src )
{
    _object=src._object;
    vertSel=src.vertSel;
    faceSel=src.faceSel;
}

//--------------------------------------------------

Selection::Selection( const Selection &src, ObjectData *object )
{
    _object=object;
    vertSel=src.vertSel;
    faceSel=src.faceSel;
}

//--------------------------------------------------

void Selection::DeletePoint( int index )
{
    Validate();
    vertSel.Delete(index);
}

//--------------------------------------------------

void Selection::DeleteFace( int index )
{
    Validate();
    faceSel.Delete(index);
}

//--------------------------------------------------

void Selection::Clear()
{
    vertSel.Clear();
    faceSel.Clear();
    Validate();
}

void Selection::ClearFaces()
{
    faceSel.Clear();
    Validate();
}

void Selection::ClearPoints()
{
    vertSel.Clear();
    Validate();
}
//--------------------------------------------------

void Selection::ValidatePoints( int nPoints )  const
{
    int i;
    int oldVert=vertSel.Size();
    int newVert=_object->NPoints();
    if( newVert<nPoints ) newVert=nPoints;
    if( newVert!=oldVert )
    {
        const_cast<Selection *>(this)->vertSel.Resize(newVert);
        for( i=oldVert; i<newVert; i++ ) 
            const_cast<Selection *>(this)->vertSel[i]=0;
    }
}

//--------------------------------------------------

void Selection::ValidateFaces( int nFaces )   const
{
    int i;
    int oldFace=faceSel.Size();
    int newFace=_object->NFaces();
    if( newFace<nFaces ) newFace=nFaces;
    if( newFace!=oldFace )
    {
        const_cast<Selection *>(this)->faceSel.Resize(newFace);
        for( i=oldFace; i<newFace; i++ ) 
            const_cast<Selection *>(this)->faceSel[i]=false;
    }
}

//--------------------------------------------------

void Selection::Validate() const
{
    ValidatePoints();
    ValidateFaces();
}

//--------------------------------------------------

int Selection::NPoints() const
{
    int i;
    int count=0;
    for( i=0; i<vertSel.Size(); i++ ) if( vertSel[i] ) count++;
    return count;
}

//--------------------------------------------------

int Selection::NFaces() const
{
    int i;
    int count=0;
    for( i=0; i<faceSel.Size(); i++ ) if( faceSel[i] ) count++;
    return count;
}

//--------------------------------------------------

bool Selection::IsEmpty() const
{
    if( NPoints()>0 ) return false;
    if( NFaces()>0 ) return false;
    return true;
}

//--------------------------------------------------

void Selection::PermuteFaces( const int *permutation )
{
    Selection newSel=*this;
    int i;
    for( i=0; i<_object->NFaces(); i++ )
    {
        faceSel[permutation[i]]=newSel.faceSel[i];
    }
}

//--------------------------------------------------

void Selection::PermuteVertices( const int *permutation )
{
    Validate();
    Selection newSel=*this;
    int i;
    for( i=0; i<_object->NPoints(); i++ )
    {
        vertSel[permutation[i]]=newSel.vertSel[i];
    }
}

void Selection::SelectPointsFromFaces()
{
    Validate();
    int i;
    for( i=0; i<_object->NFaces(); i++ ) if (FaceSelected(i))
    {
        FaceT fc(_object,i);
        for (int j=0;j<fc.N();j++) PointSelect(fc.GetPoint(j));
    }
}

void Selection::SelectFacesFromPoints()
{
    Validate();
    int i;
    for( i=0; i<_object->NFaces(); i++ ) if (!FaceSelected(i))
    {    
        FaceT fc(_object,i);
        int j;
        for (j=0;j<fc.N();j++) if (PointSelected(fc.GetPoint(j))==false) break;
        if (j==fc.N()) FaceSelect(i);
    }
}

bool Selection::SelectFacesFromPointsTouchMode()
{
    Validate();
    bool anyselect = false;
    int i;
    for( i=0; i<_object->NFaces(); i++ ) if (!FaceSelected(i))
    {    
        FaceT fc(_object,i);
        int j;
        for (j=0;j<fc.N();j++) 
            if (PointSelected(fc.GetPoint(j))) 
            {
                FaceSelect(i);
                anyselect  = true;
                break;
            }
    }
    return anyselect;
}

void Selection::SelectConnectedFaces() {
    
    while (SelectFacesFromPointsTouchMode()) 
        SelectPointsFromFaces();
    SelectPointsFromFaces();             
}

VecT Selection::GetCenter() const
{
    VecT ps;
    ps[0]=0;
    ps[1]=0;
    ps[2]=0;
    int cnt=0;
    int ptc=_object->NPoints();
    for (int i=0;i<ptc;i++)
    {
        if (PointSelected(i)) 
        {ps+=_object->Point(i)*PointWeight(i);cnt++;}
    }
    ps/=(float) cnt;
    return ps;
};

void Selection::SelectAll()
{
    Validate();
    for(int i = 0; i < faceSel.Size(); i++) faceSel[i] = true;
    for(int i = 0; i < vertSel.Size(); i++) vertSel[i] = 255;
}

Selection& Selection::operator~()
{
    Validate();
    for(int i = 0; i < faceSel.Size(); i++) faceSel[i] = !faceSel[i];
    for(int i = 0; i < vertSel.Size(); i++) vertSel[i] = 255 - vertSel[i];
    return *this;
}


std::pair<VecT, VecT> Selection::GetBoundingBox() const
{
    VecT v1, v2;
    for (int i=0;i<_object->NPoints();i++)
    {
        if (PointSelected(i))
        {
            v2=v1=_object->Point(i++);
            for (;i<_object->NPoints();i++) if (PointSelected(i))
            {
                const VecT &pos=_object->Point(i);
                if (pos[0]<v1[0]) v1[0]=pos[0];
                if (pos[0]>v2[0]) v2[0]=pos[0];
                if (pos[1]<v1[1]) v1[1]=pos[1];
                if (pos[1]>v2[1]) v2[1]=pos[1];
                if (pos[2]<v1[2]) v1[2]=pos[2];
                if (pos[2]>v2[2]) v2[2]=pos[2];        
            }
            return std::make_pair(v1,v2);
        }
    }
    return std::make_pair(VecT(0,0,0),VecT(-1,-1,-1));
}


std::pair<int, int>  Selection::GetSelectedPointsRange() const 
{

    for (int i = 0; i <  vertSel.Size(); i++)
        if (PointSelected(i)) 
            for (int j = vertSel.Size() - 1; j > i; j++)
                if (PointSelected(j)) return std::make_pair(i,j);
    return std::make_pair(-1,-1);

}

std::pair<int, int>  Selection::GetSelectedFacesRange() const
{

    for (int i = 0; i <  faceSel.Size(); i++)
        if (FaceSelected(i)) 
            for (int j = faceSel.Size() - 1; j > i; j--)
                if (FaceSelected(j)) return std::make_pair(i,j);
    return std::make_pair(-1,-1);

}


AutoArray<int> Selection::GetSelectedPoints() const {
    ValidatePoints();
    AutoArray<int> tmp;
    tmp.Reserve(_object->NPoints());
    for (int i = 0; i < _object->NPoints(); i++) if (PointSelected(i))
        tmp.Add(i);    
    return AutoArray<int>(tmp);
}
AutoArray<int> Selection::GetSelectedFaces() const {
    ValidateFaces();
    AutoArray<int> tmp;
    tmp.Reserve(_object->NFaces());
    for (int i = 0; i < _object->NFaces(); i++) if (FaceSelected(i))
        tmp.Add(i);    
    return AutoArray<int>(tmp);
}

void Selection::SelectPoints(const Array<int> &pts) {
    for (int i = 0; i <  pts.Size(); i++)
        PointSelect(pts[i]);
}
void Selection::SelectFaces(const Array<int> &fcs) {
    for (int i = 0; i <  fcs.Size(); i++)
        FaceSelect(fcs[i]);
}

}
