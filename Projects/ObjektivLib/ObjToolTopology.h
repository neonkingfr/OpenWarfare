#ifndef __OBJ_TOOL_TOPOLOGY_CLASS_HEADER_
#define __OBJ_TOOL_TOPOLOGY_CLASS_HEADER_

#pragma once


#include "ObjectData.h"
#include "Edges.h"


namespace ObjektivLib {

class IObjIsFaceAlpha
{
public:
  virtual bool IsFaceAlpha(const FaceT &fc) const=0;
};

class ObjIsFaceAlphaDefault: public IObjIsFaceAlpha
{
public:
  bool IsFaceAlpha(const FaceT &fc) const;
};

class ObjToolTopology: public ObjectData
  { 
  public:
    void CheckClosedTopology();
    void CloseTopology();
    
    int CalculateInsideFaces( int face, bool onlySelected ) const;
    
    void SelectionToConvexComponents();
    void CheckConvexComponent(); // check selection
    
    void RemoveComponents();
    int CreateComponents(const Selection *forbidenFaces=0);
    void CreateConvexComponents();
    void CheckConvexComponents();
    
    bool FaceCanBeInConvexHull(int i, int j, int k, bool allPoints);
    void CreateConvexHull( bool allVertices=false );
    void ComponentConvexHull();    

    void AutoSharpEdges(float angle=3.14159565/4.0f);
    void MakeEdgesSharp(); // make all selected edges sharp.
    /** pointer groups points to array containing smooth group for each face in selection.
     Must be big as number of faces in selection; */
    void ReadSmoothGroups(unsigned long *groups, bool selfaces=true);
    
    /** pointer groups points to array allocated for storing groups. 
    Space mush have at least NFaces()*4 bytes.
    */
    bool BuildSmoothGroups(unsigned long *groups);

    /** function generates edges from normals
    It compares edge between each two faces and compares its two normals. If 
    it found on one point two or more normals, creates edge. It compares
    direction of normal, not index
    */
    void DetectEdgesFromNormals();

    /** Finds and select nonconvex points and edges
    */
    void CheckConvexity(bool selonly=true);

    void SelectDoubleSided(Selection *inselection);

    ///Structure used for optaining tessellate info
    struct STessellateInfo
    {
      int index; ///< index that describing this structure
      int face; ///< index of face assigned to the index
      int vx;  ///< index of face-vertex assigned index
      
      ClassIsSimpleZeroed(ObjToolTopology::STessellateInfo);
    };


    /// Creates set of triangles from one polygon
    /**
    function calculates best tessellation for polygon. New faces are added into object    
    @param nvertices number of vertices in polygon
    @param indexList array of vertices in polygon. This array must 
                     have nvertices size. Indices are referencing vertices 
                     in current object
    @param uvlist array of UV coordinates. Each vertex in polygon has two 
                  values in array. If the parameter is not used, all UV coordinates
                  are set to zero
    @param textureName name of texture for polygon. If not used, texture is set
                  to empty.
    @param materialName name of texture for polygon. If not used, texture is set
                  to empty.
    @param outTesInfo Information about result. For each vertice, there must be at least
                  3 items! Note! Don't use AutoArray derived class. Function sets
                  new size of that array.
                  
    @retval true success
    @retval false fail
    */
    bool TessellatePolygon(int nvertices, const int *indexList, 
                           const float *uvlist=NULL, 
                           const char *textureName=NULL, 
                           const char *materialName=NULL,
                           Array<STessellateInfo> *outTesInfo=NULL);

    static const unsigned long fpPrecisionLow=1;     //low precision with normal comparsion 0.1
    static const unsigned long fpPrecisionMedium=2;  //medium precision with normal comparsion 0.01
    static const unsigned long fpPrecisionHigh=3;    //high precision with normal comparsion 0.001
    static const unsigned long fpPrecisionVeryHigh=4;//very high precision with normal comparsion 0.0001
    static const unsigned long fpSameTextue=0x10;     //polygon must have same texture
    static const unsigned long fpSameMaterial=0x20;   //polygon must have same material
    static const unsigned long fpSameFlags=0x40;     //polygon must have same flags
    static const unsigned long fpSameMapping=0x80;   //mapping on edges must be same    

    /// Combines faces into polygon
    /**
      @param startFace Index of face, that will be used as start of polygon search.
      @param faceGraph graph returned by function BuildFaceGraph. 
                        Can be used for multiple searches.
      @param indexList Array reserved for indices of new polygon.
      @param indexCount Size of reserved array. If array is big enough to hold all 
                        vertices, function generates smaller polygons.
                        For example, indexCount=5 will find polygon with maximum 5
                        vertices.

      @param flags      Defines precision and other flags. See fpXXXX flags
      @param sharpEdges Defines sharp edges. If pointer is used, function doesn't cross sharp edges                  
      @param polyfaces  Pointer to selection, which will hold polygon faces. 
      
      @return function return number of vertices of found polygon. Array indexList holds
        indices of found polygon in right order. polyfaces selection holds face, that has
        been used for polygon.
    */
    int FindPolygon(int startFace, const CEdges &faceGraph, int *indexList, 
                    int indexCount,  unsigned long flags, 
                    const CEdges *sharpEdges=NULL, Selection *polyfaces=NULL,Selection *workOn=NULL) const;    


    bool TessellateFromPointSelection(const Selection &vertices,      //selected vertices to tessellatte
                                      const Selection *templateFaces=NULL, //faces used as source of mapping U,V
                                      const char *textureName=NULL, //new texture name
                                      const char *materialName=NULL, // new material name            
                                      const VecT *center=NULL); //point of view, NULL - will be calculated using GetCenterOfSelection
    /** Function delete all double polygons */
    template<class Functor>
    void SelectDoublePolygons(const Selection * in, Selection& out, Functor f) const;

    void SelectDoublePolygons(const Selection * in, Selection& out) const;
    /** Function simplifies polygons tesellation. It removes not necessary points. */
    void RetesellatePolygons();
    /** Function shifts UV interval on each selected faces. Result mapping on each face will have minimum U and V coord in interval (0 - 1).
    */
    void MoveUVInInterval01();

    void MoveUVInPolygons();

    void MoveUVInPolygon(int startFace, const CEdges &faceGraph, unsigned long flags, 
      const CEdges *sharpEdges=NULL, Selection *polyfaces=NULL,Selection *workOn=NULL);

    void SortAlphaFaces(const IObjIsFaceAlpha &alphaTester=ObjIsFaceAlphaDefault(),const Selection *sel=0);

    ///Breaks mesh into parts, if contains shared vertices.
    void BreakApart(const Selection *selection);

    void SortAroundPoint(Array<int> &ptlist, const Vector3 &up, const Vector3 *cent = 0);

  protected:    
    void RetesellatePolygon(Selection& border, Selection& faces,Selection * deleteFaces, Selection * deletePoints);
    
  };

  class O2Polygon 
  {
  public:
    FindArray<int> _indexes; // TODO: registered selection
    FindArray<int> _faces;  // TODO: registered selection
    AutoArray<float> _uvlist;
    RString _textureName;
    RString _materialName;

    void PreparePar(const ObjectData * src);    
    void CleanUnused(const AutoArray<int>& numUse);
    void Select(Selection& points, Selection& faces) const;    
    void Create(int * indexList, int size, Selection * faces,const ObjectData& src, bool filterOnLines = false);
    void Create(int * indexList, int size, Selection * faces,const ObjectData& src, AutoArray<int>& numUse);   
    ClassIsMovable(O2Polygon);
  };


  template<class Functor>
  void ObjToolTopology::SelectDoublePolygons(const Selection * in, Selection& out, Functor f) const
  {
    int indexList[1024];
    AutoArray<O2Polygon> polygons;
    Selection usedFaces(this);

    CEdges faceGraph;
    faceGraph.SetVertices(NFaces());
    CEdges nocross;

    BuildFaceGraph(faceGraph, nocross);  

    for(int i = 0;i < NFaces(); i++)
    {  
      if (!(in ? in->FaceSelected(i) : FaceSelected(i))  || usedFaces.FaceSelected(i))
        continue;

      Selection sel(this);
      int nIndexes = FindPolygon(i, faceGraph, indexList, 1024, 
       f.FindPolyFlags(), NULL, &sel);

      if (nIndexes < 0)
        continue;

      // add selection
      usedFaces += sel;

      polygons.Append().Create(indexList,nIndexes,&sel,*this, true);
    }

    AutoArray<O2Polygon> toDie;
    for(int i = 0; i < polygons.Size(); i++)
    {
      O2Polygon& pol1 = polygons[i];
      for(int j = i + 1; j < polygons.Size(); j++)
      {
        // do they share a index?
        O2Polygon& pol2 = polygons[j];

        bool possibleToDelete = false; 
        // check if all points are in second polygon 
        if (pol1._indexes.Size() == pol2._indexes.Size())
          possibleToDelete = true;

        for(int k = 0;possibleToDelete && k < pol1._indexes.Size(); k++)
        {
          if (pol2._indexes.Find(pol1._indexes[k]) < 0)
            possibleToDelete = false;
        }

        if (possibleToDelete)
        {
          toDie.Add(pol1);
          toDie.Add(pol2);
        }
      }
    }

    // now delete polygons.
    out.Clear();

    //ClearSelection();
    // select faces for selection
    for(int i = 0; i < toDie.Size(); i+=2)
    {
      O2Polygon& polygon0 = toDie[i];
      O2Polygon& polygon1 = toDie[i+1];
      polygon0.PreparePar(this);
      polygon1.PreparePar(this);

      if (f(polygon0,polygon1))
      {      
        for(int j = 0; j < polygon0._faces.Size(); j++)
        {
          out.FaceSelect(polygon0._faces[j],true);
        }
        for(int j = 0; j < polygon1._faces.Size(); j++)
        {
          out.FaceSelect(polygon1._faces[j],true);
        }
      }
    }

    //SelectionDeleteFaces(&usedFaces);
  };


}
#endif
