/*  wpch.hpp
    This is the source for the precompiled header file.
*/

#ifndef COMMON_HPP_INCLUDED
#define COMMON_HPP_INCLUDED

#include <Es/Common/win.h>
#include <utility>

#include <fstream>
#include <strstream>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <queue>

#include <Es/essencePch.hpp>
#include <Es/Types/pointers.hpp>
#include <Es/Containers/array.hpp>
#include <El/Pathname/Pathname.h>
#include <el/ProgressBar/ProgressBar.h>


#include <el/math/math3d.hpp>


#include <Es/Common/fltOpts.hpp>
#include "RStringB_MT.h"

namespace ObjektivLib {

    using namespace std;

    typedef unsigned char byte;

inline RString NumStr( double a )
  {
  char Buf[80];
  sprintf(Buf,"%.3f",a);
  return RString(Buf);
  }

//--------------------------------------------------

inline RString NumStr( int a )
  {
  char Buf[80];
  sprintf(Buf,"%d",a);
  return RString(Buf);
  }

//--------------------------------------------------

template <class T> 
void __swap(T &v1, T& v2)
  {
  T tmp=v1;v1=v2;v2=tmp;
  }

inline int ModuloCounting( int i, int n )
{
  while( i>=n ) i-=n;
  while( i<0 ) i+=n;
  return i;
}


#endif

}