// Canvas.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <strstream>
#include "CanvasWindow.h"

void __cdecl LogF(char const *format,...)
{
  va_list list;
  va_start(list,format);
  vfprintf(stderr,format,list);
}

static char buffer1[1024];
static char buffer2[1024];

int _tmain(int argc, _TCHAR* argv[])
{
  CanvasWindow wnd;

  int bgrn;
  int resizable;

  while(true)
  {
    scanf("%1024s %d %d %1024[^\n]",buffer1,&resizable,&bgrn,buffer2);
    if (feof(stdin)) return 0;
    if (strcmp(buffer1,"openmax")==0)
    {
      wnd.OpenWindow(CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,buffer2,bgrn,resizable!=0);
      ShowWindow(wnd,SW_SHOWMAXIMIZED);
      break;
    }
    else if (strcmp(buffer1,"open")==0)
    {
      int cx,cy;
      if (scanf("%d %d",&cx,&cy)==2) 
      {    
        wnd.OpenWindow(CW_USEDEFAULT,CW_USEDEFAULT,cx,cy,buffer2,bgrn,resizable!=0);
        break;
      }
    }
    else if (strcmp(buffer1,"openat")==0)
    {
      int x,y,cx,cy;
      if (scanf("%d %d %d %d",&x,&y,&cx,&cy)==4)
      {
        wnd.OpenWindow(x,y,cx,cy,buffer2,bgrn,resizable!=0);    
        break;
      };
    }
    puts("syntax error");
  }

  while(true)
  {
    scanf("%1024s",buffer1);
    if (feof(stdin)) break;
    if (strcmp(buffer1,"quit")==0) break;
    if (strcmp(buffer1,"textout")==0)
    {
      int x=0,y=0;
      scanf("%d %d ",&x,&y);
      std::ostrstream text;
      int i=getchar();
      while (i!=EOF && i!='\n')
      {
        text.put((char)i);
        i=getchar();
      }
      text.put((char)0);
      wnd.TextOutAt(x,y,text.str());
      text.freeze(false);
    }
    else if (strcmp(buffer1,"textcolor")==0)
    {
      int r=0,g=0,b=0;
      scanf("%d %d %d",&r,&g,&b);
      wnd.SetTextColor(r,g,b);
    }
    else if (strcmp(buffer1,"backcolor")==0)
    {
      int r=0,g=0,b=0;
      scanf("%d %d %d",&r,&g,&b);
      wnd.SetBackColor(r,g,b);
    }
    else if (strcmp(buffer1,"present")==0)
    {
      wnd.Present();
    }
    else if (strcmp(buffer1,"enablebgrn")==0)
    {
      int r;
      scanf("%d",&r);
      wnd.EnableBackground(r!=0);
    }
    else if (strcmp(buffer1,"line")==0)
    {
      int x1,y1,x2,y2;
      scanf("%d %d %d %d",&x1,&y1,&x2,&y2);
      wnd.Line(x1,y1,x2,y2);
    }
    else if (strcmp(buffer1,"rectangle")==0)
    {
      int x1,y1,x2,y2;
      scanf("%d %d %d %d",&x1,&y1,&x2,&y2);
      wnd.Rectangle(x1,y1,x2,y2);
    }
    else if (strcmp(buffer1,"roundrect")==0)
    {
      int x1,y1,x2,y2,w,h;
      scanf("%d %d %d %d %w %h",&x1,&y1,&x2,&y2,&w,&h);
      wnd.RoundRect(x1,y1,x2,y2,w,h);
    }
    else if (strcmp(buffer1,"circle")==0 || strcmp(buffer1,"ellipse")==0)
    {
      int x1,y1,x2,y2;
      scanf("%d %d %d %d",&x1,&y1,&x2,&y2);
      wnd.Circle(x1,y1,x2,y2);
    }
    else if (strcmp(buffer1,"arc")==0)
    {
      int x1,y1,x2,y2,x3,y3,x4,y4;
      scanf("%d %d %d %d %d %d %d %d",&x1,&y1,&x2,&y2,&x3,&y3,&x4,&y4);
      wnd.Arc(x1,y1,x2,y2,x3,y3,x4,y4);
    }
    else if (strcmp(buffer1,"chord")==0)
    {
      int x1,y1,x2,y2,x3,y3,x4,y4;
      scanf("%d %d %d %d %d %d %d %d",&x1,&y1,&x2,&y2,&x3,&y3,&x4,&y4);
      wnd.Arc(x1,y1,x2,y2,x3,y3,x4,y4);
    }
    else if (strcmp(buffer1,"pie")==0)
    {
      int x1,y1,x2,y2,x3,y3,x4,y4;
      scanf("%d %d %d %d %d %d %d %d",&x1,&y1,&x2,&y2,&x3,&y3,&x4,&y4);
      wnd.Arc(x1,y1,x2,y2,x3,y3,x4,y4);
    }
    else if (strcmp(buffer1,"bezier")==0)
    {
      int x1,y1,x2,y2,x3,y3,x4,y4;
      scanf("%d %d %d %d %d %d %d %d",&x1,&y1,&x2,&y2,&x3,&y3,&x4,&y4);
      wnd.Arc(x1,y1,x2,y2,x3,y3,x4,y4);
    }
    else if (strcmp(buffer1,"polygon")==0)
    {
      AutoArray<int,MemAllocStack<int,256> > pts;
      int k;
      while (scanf("%d",&k)==1) pts.Add(k);
      wnd.Polygon(pts.Data(),pts.Size());     
    }
    else if (strcmp(buffer1,"polyline")==0)
    {
      AutoArray<int,MemAllocStack<int,256> > pts;
      int k;
      while (scanf("%d",&k)==1) pts.Add(k);
      wnd.PolyLine(pts.Data(),pts.Size());     
    }
    else if (strcmp(buffer1,"pencolor")==0)
    {
      int r,g,b;
      scanf("%d %d %d",&r,&g,&b);
      wnd.SetPenColor(r,g,b);
    }
    else if (strcmp(buffer1,"brushcolor")==0)
    {
      int r,g,b;
        scanf("%d %d %d",&r,&g,&b);
      wnd.SetBrushColor(r,g,b);
    }
    else if (strcmp(buffer1,"penstyle")==0)
    {
      int r,g,b,w,s;
        scanf("%d %d %d %d %s",&r,&g,&b, &w, &s);
      wnd.SetPenStyle(r,g,b,w,s);
    }

    else if (strcmp(buffer1,"fontname")==0)
    {
      char buff[256];
        scanf("%256[^\n]",buff);
      wnd.SetFontName(buff);
    }
    else if (strncmp(buffer1,"char",4)==0)
    {    
      if (strcmp(buffer1,"charheight")==0)
      {
        int h;
        scanf("%d",&h);
        wnd.SetCharHeight(h);
      }
      else if (strcmp(buffer1,"charwidth")==0)
      {
        int w;
        scanf("%d",&w);
        wnd.SetCharWidth(w);
      }
      else if (strcmp(buffer1,"charorientation")==0)
      {
        int o;
        scanf("%d",&o);
        wnd.SetCharOrientation(o);
      }
      else if (strcmp(buffer1,"charstyle")==0)
      {
        int b,i,u,s;
        scanf("%d %d %d %d",&b,&i, &u, &s);
        wnd.SetCharStyle(b,i,u,s);
      }
    }
    else if (strncmp(buffer1,"path",4)==0)
    {
      if (strcmp(buffer1,"pathbegin")==0) wnd.BeginClipPath();
      else if (strcmp(buffer1,"pathend")==0) wnd.EndClipPath();
      else if (strcmp(buffer1,"pathuse")==0) wnd.UseClipPath();
      else if (strcmp(buffer1,"pathstroke")==0) wnd.StrokeClipPath();
      else if (strcmp(buffer1,"pathfill")==0) wnd.FillClipPath();
      else if (strcmp(buffer1,"pathclosefig")==0) wnd.CloseFigure();
    }
    else if (strcmp(buffer1,"pixel")==0)
    {
      int r,g,b,x,y;
      scanf("%d %d %d %d %s",&x,&y,&r,&g,&b);
      wnd.SetPixel(x,y,r,g,b);
    }
    else if (strcmp(buffer1,"capturems")==0)
    {
      int r;
      scanf("%d",&r);
      wnd.CaptureMS(r!=0);
    }
    else if (strcmp(buffer1,"cls")==0)
    {
      wnd.Cls();
      wnd.Present();
    }
    else if (strcmp(buffer1,"ping")==0)
    {
      wnd.Ping();     
    }
    else
      puts("Syntax error");
    
  }


  return 0;
}

