#pragma once

struct StartStruct;

class CanvasWindow :
  public MultiThread::ThreadBase
{
  HWND _curWindow;
  HBITMAP _savebp;
  HBRUSH _bgrn;
  bool _sysbrush;
  HDC _buffer;
  LPMINMAXINFO _minMaxInfo;  
  MultiThread::CriticalSection _guard;

  StartStruct *_startStruct;

  HBRUSH _oldbrush;
  HPEN _oldpen;
  HFONT _oldfont;

public:
  CanvasWindow(void);
  ~CanvasWindow(void);

  static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
  LRESULT WinProc(UINT msg, WPARAM wParam, LPARAM lParam);

  bool OpenWindow(int left, int top, int width, int height, const char *title, int bgrn, bool resizeable);
  bool CloseWindow();

  virtual unsigned long Run();

  operator HWND() const {return _curWindow;}  

  void TextOutAt(int x, int y, const char *text);
  void SetTextColor(int r,int g,int b);
  void SetBackColor(int r,int g,int b);
  void EnableBackground(bool enable);  
  void Present();
  void Line(int x1, int y1, int x2, int y2);
  void Rectangle(int x1,int y1, int x2, int y2);
  void RoundRect(int x1,int y1, int x2, int y2,int width,int height);
  void Circle(int x1,int y1, int x2, int y2);
  void Arc(int x1, int y1, int x2, int y2, int xstart, int ystart, int xend, int yend);
  void Chord(int x1, int y1, int x2, int y2, int xstart, int ystart, int xend, int yend);
  void Pie(int x1, int y1, int x2, int y2, int xstart, int ystart, int xend, int yend);
  void SetPenColor(int r,int g, int b);
  void SetBrushColor(int r,int g, int b);
  void SetPenStyle(int r,int g, int b, int width, int style);  
  void SetFontName(const char *fontName);
  void SetCharHeight(int height);
  void SetCharWidth(int width);
  void SetCharOrientation(int orientation);
  //-1 nochange, 0 - off, 1 - on
  void SetCharStyle(int bold, int italic, int underline, int strickout);

  void Bezier(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4);
  void Polygon(const int *array, int count);
  void PolyLine(const int *array, int count);
  void BeginClipPath();
  void EndClipPath();
  void UseClipPath();
  void StrokeClipPath();
  void FillClipPath();  
  void CloseFigure();

  void SetPixel(int x, int y, int r, int g, int b);
  void CaptureMS(bool capture);

  void Cls();
  void Ping();
};

