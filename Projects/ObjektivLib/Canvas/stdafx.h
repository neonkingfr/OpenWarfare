// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _WIN32_WINNT 0x0500

#include <iostream>
#include <tchar.h>
#include <windows.h>
#include <es/containers/array.hpp>
#include <el/MultiThread/ThreadBase.h>
#include <el/MultiThread/CriticalSection.h>
#include <el/MultiThread/BlockerSimpleBase.h>

#ifndef GET_X_LPARAM
#define GET_X_LPARAM(lParam)	((int)(short)LOWORD(lParam))
#endif
#ifndef GET_Y_LPARAM
#define GET_Y_LPARAM(lParam)	((int)(short)HIWORD(lParam))
#endif


// TODO: reference additional headers your program requires here
