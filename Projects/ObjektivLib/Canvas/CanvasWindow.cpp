#include "StdAfx.h"
#include ".\canvaswindow.h"

#define WMT_DESTROYMSG (WM_USER+1)
#define WMT_SETCAPTURE (WM_USER+2)


CanvasWindow::CanvasWindow(void)
{
  _bgrn=0;
  _buffer=0;
  _curWindow=0;
  _savebp=0;
  _minMaxInfo=0;
  _oldbrush=0;
  _oldpen=0;
  _oldfont=0;
  _sysbrush=false;
}

CanvasWindow::~CanvasWindow(void)
{
  if (_curWindow)
    CloseWindow();
}


LRESULT CALLBACK CanvasWindow::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{                                                              
  CanvasWindow *instance=static_cast<CanvasWindow *>(GetCurrentRunnable());
  if (instance)
  {
    if (instance->_curWindow==hWnd || instance->_curWindow==0)
    {
      instance->_curWindow=hWnd;
      instance->_guard.Lock();
      LRESULT res=instance->WinProc(msg,wParam,lParam);
      fflush(stdout);
      instance->_guard.Unlock();
      return res;
    }
  }
  return DefWindowProc(hWnd,msg,wParam,lParam);
}

struct StartStruct
{
  int left,top,width,height;
  const char *title;
  bool resizeable;
  MultiThread::EventBlocker event;
};

bool CanvasWindow::OpenWindow(int left, int top, int width, int height, const char *title, int bgrn, bool resizeable)
{
  WNDCLASS wclass;
  ZeroMemory(&wclass,sizeof(wclass));
  LOGBRUSH lgb;
  if (bgrn==-1) 
  {
    _sysbrush=true;
    _bgrn=0;
  }
  else if (bgrn<-1)  
  {
    _sysbrush=true;
    _bgrn=GetSysColorBrush(-bgrn-1);  
  }
  else
  {
    _sysbrush=false;
    lgb.lbColor=bgrn;
    lgb.lbHatch=0;
    lgb.lbStyle=BS_SOLID;
    _bgrn=CreateBrushIndirect(&lgb);
  }

  wclass.hbrBackground=0;
  wclass.hCursor=::LoadCursor(0,IDC_ARROW);
  wclass.hIcon=0;
  wclass.hInstance=GetModuleHandle(0);
  wclass.lpfnWndProc=WndProc;
  wclass.lpszClassName="CanvasClass";
  wclass.lpszMenuName=0;
  wclass.style=CS_DBLCLKS|CS_VREDRAW|CS_HREDRAW;
  RegisterClass(&wclass);


  StartStruct str;
  str.event.Block();
  str.height=height;
  str.width=width;
  str.left=left;
  str.top=top;
  str.title=title;
  str.resizeable=resizeable;
  _startStruct=&str;
  if (Start()==false) return false;
  _startStruct->event.Acquire();
  _startStruct->event.Release();
  return _curWindow!=0;

}
bool CanvasWindow::CloseWindow()
{

  PostMessage(_curWindow,WMT_DESTROYMSG,0,0);
  Join();
  if (_buffer)
  {
    if (_savebp) 
      DeleteObject(SelectObject(_buffer,_savebp));
    DeleteDC(_buffer);
  }
  if (!_sysbrush) DeleteObject(_bgrn);
  if (_oldbrush) DeleteObject(SelectObject(_buffer,_oldbrush));
  if (_oldpen) DeleteObject(SelectObject(_buffer,_oldpen));
  if (_oldfont) DeleteObject(SelectObject(_buffer,_oldfont));
  _buffer=0;
  _savebp=0;
  _bgrn=0;
  _curWindow=0;
  
  delete _minMaxInfo;
  _minMaxInfo=0;
  return true;
}

LRESULT CanvasWindow::WinProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch (msg)
  {
  case WM_PAINT:
    {
      PAINTSTRUCT ps;
      HDC dc=BeginPaint(_curWindow,&ps);
      BITMAP bp;
      GetObject(GetCurrentObject(_buffer,OBJ_BITMAP),sizeof(bp),&bp);
      if (_buffer)
      {
        BitBlt(dc,0,0,bp.bmWidth,bp.bmHeight,_buffer,0,0,SRCCOPY);
      }
      EndPaint(_curWindow,&ps);
      return 0;
    }
  case WM_SIZE:
    {
      int cx=LOWORD(lParam), cy=HIWORD(lParam);
      if (wParam==SIZE_MINIMIZED) return 0;
      HDC dc=GetDC(_curWindow);
      if (!_buffer)
      {
        _buffer=CreateCompatibleDC(dc);
      }
      else if (_savebp)
      {
        BITMAP bp;
        GetObject(GetCurrentObject(_buffer,OBJ_BITMAP),sizeof(bp),&bp);
        if (bp.bmWidth==cx && bp.bmHeight==cy) return 0;

        DeleteObject(SelectObject(_buffer,_savebp));
        _savebp=0;
      }
      _savebp=(HBITMAP)SelectObject(_buffer,CreateCompatibleBitmap(dc,cx,cy));
      if (_bgrn)
      {      
        RECT rc={0,0,0,0};
        rc.right=cx;
        rc.bottom=cy;
        FillRect(_buffer,&rc,_bgrn);
      }
      ReleaseDC(_curWindow,dc);
      printf("resize %d %d\n",cx,cy);
      return 0;
    }
  case WM_CLOSE:    
    puts("close");
    break;    
  case WM_ENDSESSION:
    puts("endsession");
    break;
  case WM_KEYDOWN:
    printf("keydown %d %d %d\n",wParam,LOWORD(lParam),HIWORD(lParam));
    break;
  case WM_KEYUP:
    printf("keyup %d %d %d\n",wParam,LOWORD(lParam),HIWORD(lParam));
    break;
  case WM_CHAR:
    printf("char %d %d %d\n",wParam,LOWORD(lParam),HIWORD(lParam));
    break;
  case WM_LBUTTONDOWN:
  case WM_RBUTTONDOWN:
  case WM_MBUTTONDOWN:
  case WM_LBUTTONUP:
  case WM_RBUTTONUP:
  case WM_MBUTTONUP:
  case WM_LBUTTONDBLCLK:
  case WM_RBUTTONDBLCLK:
  case WM_MBUTTONDBLCLK:
  case WM_MOUSEMOVE:
    {      
    printf("mouse %d %d %d %d\n",msg,GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam),wParam);
    break;
    }
  case WMT_DESTROYMSG:
    DestroyWindow(_curWindow);
    break;
  case WMT_SETCAPTURE:
    if (wParam)
      SetCapture(_curWindow);
    else
      ReleaseCapture();
    break;
  case WM_DESTROY:
    {
    puts("destroy");
    PostQuitMessage(0);
    break;
    }  
  case WM_GETMINMAXINFO:
    {
      MINMAXINFO *nfo=(MINMAXINFO *)lParam;
      if (_minMaxInfo)
      {
        *nfo=*_minMaxInfo;
      }
      else
      {      
        DefWindowProc(_curWindow,msg,wParam,lParam);
        if (_minMaxInfo==0)
        {
          _minMaxInfo=new MINMAXINFO(*nfo);
        }
      }
      return 0;
    }
  default:
    return DefWindowProc(_curWindow,msg,wParam,lParam);
  }
  return 0;
}

unsigned long CanvasWindow::Run()
{
   DWORD flags=WS_OVERLAPPED|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX|WS_VISIBLE;
  if (_startStruct->resizeable) flags|=WS_THICKFRAME|WS_MAXIMIZEBOX;
  else flags|=WS_DLGFRAME;
  RECT rc={0,0,0,0};
  rc.right=_startStruct->width;
  rc.bottom=_startStruct->height;
  AdjustWindowRect(&rc,flags,FALSE);

  _curWindow=CreateWindow("CanvasClass",
    _startStruct->title,flags,
    _startStruct->left,
    _startStruct->top,
    rc.right-rc.left,
    rc.bottom-rc.top,0,0,GetModuleHandle(0),0);

  _startStruct->event.Unblock();

  MSG msg;
  while (GetMessage(&msg,0,0,0))
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }
  return msg.wParam;
}

void CanvasWindow::TextOutAt(int x, int y, const char *text)
{
  _guard.Lock();
  TextOutA(_buffer,x,y,text,strlen(text));
  _guard.Unlock();
}
void CanvasWindow::SetTextColor(int r,int g,int b)
{
  _guard.Lock();  
  ::SetTextColor(_buffer,RGB(r,g,b));
  _guard.Unlock();
}

void CanvasWindow::SetBackColor(int r,int g,int b)
{
  _guard.Lock();
  ::SetBkColor(_buffer,RGB(r,g,b));
  _guard.Unlock();
}

void CanvasWindow::Present()
{
  InvalidateRect(_curWindow,0,FALSE);
}

void CanvasWindow::EnableBackground(bool enable)
{
  _guard.Lock();
  SetBkMode(_buffer,enable?OPAQUE:TRANSPARENT);
  _guard.Unlock();
}

void CanvasWindow::Line(int x1, int y1, int x2, int y2)
{
  _guard.Lock();
  POINT cp;
  GetCurrentPositionEx(_buffer,&cp);
  if (cp.x!=x1 || cp.y!=y1) MoveToEx(_buffer,x1,y1,0);
  LineTo(_buffer,x2,y2);
  _guard.Unlock();
}

void CanvasWindow::Rectangle(int x1,int y1, int x2, int y2)
{
  _guard.Lock();
  ::Rectangle(_buffer,x1,y1,x2,y2);  
  _guard.Unlock();
}

void CanvasWindow::Circle(int x1,int y1, int x2, int y2)
{
  _guard.Lock();
  ::Ellipse(_buffer,x1,y1,x2,y2);  
  _guard.Unlock();
}

void CanvasWindow::Arc(int x1, int y1, int x2, int y2, int xstart, int ystart, int xend, int yend)
{
  _guard.Lock();
  ::Arc(_buffer,x1,y1,x2,y2,xstart,yend,xend,yend);
  _guard.Unlock();
}

void CanvasWindow::Chord(int x1, int y1, int x2, int y2, int xstart, int ystart, int xend, int yend)
{
  _guard.Lock();
  ::Chord(_buffer,x1,y1,x2,y2,xstart,yend,xend,yend);
  _guard.Unlock();
}

void CanvasWindow::Pie(int x1, int y1, int x2, int y2, int xstart, int ystart, int xend, int yend)
{
  _guard.Lock();
  ::Pie(_buffer,x1,y1,x2,y2,xstart,yend,xend,yend);
  _guard.Unlock();
}

void CanvasWindow::SetPenColor(int r,int g, int b)
{
  _guard.Lock();
  if (_oldpen) DeleteObject(SelectObject(_buffer,GetStockObject(DC_PEN)));
  else SelectObject(_buffer,GetStockObject(DC_PEN));
  _oldpen=0;
  SetDCPenColor(_buffer,RGB(r,g,b));  
  _guard.Unlock();
}

void CanvasWindow::SetBrushColor(int r,int g, int b)
{
  _guard.Lock();
  if (_oldbrush) DeleteObject(SelectObject(_buffer,GetStockObject(DC_BRUSH)));
  else SelectObject(_buffer,GetStockObject(DC_BRUSH));
    _oldpen=0;
  SetDCBrushColor(_buffer,RGB(r,g,b));  
  _guard.Unlock();
}

void CanvasWindow::SetPenStyle(int r,int g, int b, int width, int style)
{
  _guard.Lock();
  if (_oldpen) DeleteObject(SelectObject(_buffer,_oldpen));
  _oldpen=(HPEN)SelectObject(_buffer,CreatePen(style,width,RGB(r,g,b)));
  _guard.Unlock();
}

void CanvasWindow::SetFontName(const char *fontName)
{
  _guard.Lock();
  LOGFONT lg;
  GetObject(GetCurrentObject(_buffer,OBJ_FONT),sizeof(lg),&lg);
  strncpy(lg.lfFaceName,fontName,sizeof(lg.lfFaceName));
  if (_oldfont) DeleteObject(SelectObject(_buffer,_oldfont));
  _oldfont=(HFONT)SelectObject(_buffer,CreateFontIndirect(&lg));
  _guard.Unlock();
}

void CanvasWindow::SetCharHeight(int height)
{
  _guard.Lock();
  LOGFONT lg;
  GetObject(GetCurrentObject(_buffer,OBJ_FONT),sizeof(lg),&lg);
  lg.lfHeight=height;
  lg.lfWidth=0;
  if (_oldfont) DeleteObject(SelectObject(_buffer,_oldfont));
  _oldfont=(HFONT)SelectObject(_buffer,CreateFontIndirect(&lg));
  _guard.Unlock();
}

void CanvasWindow::SetCharWidth(int width)
{
  _guard.Lock();
  LOGFONT lg;
  GetObject(GetCurrentObject(_buffer,OBJ_FONT),sizeof(lg),&lg);
  lg.lfWidth=width;
  if (_oldfont) DeleteObject(SelectObject(_buffer,_oldfont));
  _oldfont=(HFONT)SelectObject(_buffer,CreateFontIndirect(&lg));
  _guard.Unlock();
}

void CanvasWindow::SetCharOrientation(int orientation)
{
  _guard.Lock();
  LOGFONT lg;
  GetObject(GetCurrentObject(_buffer,OBJ_FONT),sizeof(lg),&lg);
  lg.lfOrientation=orientation;
  lg.lfEscapement=orientation;
  if (_oldfont) DeleteObject(SelectObject(_buffer,_oldfont));
  _oldfont=(HFONT)SelectObject(_buffer,CreateFontIndirect(&lg));
  _guard.Unlock();
}

void CanvasWindow::SetCharStyle(int bold, int italic, int underline, int strickout)
{
  _guard.Lock();
  LOGFONT lg;
  GetObject(GetCurrentObject(_buffer,OBJ_FONT),sizeof(lg),&lg);
  if (bold>=0) lg.lfWeight=bold?FW_BOLD:FW_NORMAL;
  if (italic>=0) lg.lfItalic=italic>0;
  if (underline>=0) lg.lfUnderline=underline>0;
  if (strickout>=0) lg.lfStrikeOut=strickout>0;
  if (_oldfont) DeleteObject(SelectObject(_buffer,_oldfont));
  _oldfont=(HFONT)SelectObject(_buffer,CreateFontIndirect(&lg));
  _guard.Unlock();
}


void CanvasWindow::Bezier(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
{
  _guard.Lock();
  POINT pt[4];
  pt[0].x=x1;
  pt[1].x=x2;
  pt[2].x=x3;
  pt[3].x=x4;
  pt[0].y=y1;
  pt[1].y=y2;
  pt[2].y=y3;
  pt[3].y=y4;
  PolyBezier(_buffer,pt,4);
  _guard.Unlock();
}

void CanvasWindow::Polygon(const int *array, int count)
{
  count/=2;
  if (count==0) return;
  _guard.Lock();
  ::Polygon(_buffer,(POINT *)array,count);
  _guard.Unlock();
}

void CanvasWindow::PolyLine(const int *array, int count)
{
  count/=2;
  if (count==0) return;
  _guard.Lock();
  Polyline(_buffer,(POINT *)array,count);
  _guard.Unlock();
}

void CanvasWindow::BeginClipPath()
{
  _guard.Lock();
  ::BeginPath(_buffer);
}
void CanvasWindow::EndClipPath()
{
  ::EndPath(_buffer);
  _guard.Unlock();
}
void CanvasWindow::UseClipPath()
{
  _guard.Lock();
  ::SelectClipPath(_buffer,RGN_COPY);
  _guard.Unlock();
}
void CanvasWindow::StrokeClipPath()
{
  _guard.Lock();
  StrokePath(_buffer);
  _guard.Unlock();
}
void CanvasWindow::FillClipPath()
{
  _guard.Lock();
  FillPath(_buffer);
  _guard.Unlock();
}

void CanvasWindow::CloseFigure()
{
  _guard.Lock();
  ::CloseFigure(_buffer);
  _guard.Unlock();
}

void CanvasWindow::SetPixel(int x, int y, int r, int g, int b)
{
  _guard.Lock();
  ::SetPixel(_buffer,x,y,RGB(r,g,b));
  _guard.Unlock();
}

void CanvasWindow::CaptureMS(bool capture)
{
  SendMessage(_curWindow,WMT_SETCAPTURE,capture,0);
}

void CanvasWindow::RoundRect(int x1,int y1, int x2, int y2,int width,int height)
{
  _guard.Lock();
  ::RoundRect(_buffer,x1,y1,x2,y2,width,height);
  _guard.Unlock();
}

void CanvasWindow::Cls()
{
  RECT rc;
  GetClientRect(*this,&rc);
  FillRect(_buffer,&rc,_bgrn);
}

void CanvasWindow::Ping()
{
  _guard.Lock();
  puts("ping");
  fflush(stdout);
  _guard.Unlock();

}