#pragma once
#include "objectdata.h"

namespace ObjektivLib  {

    class ConvexIncidence;
    struct SimpleFace;

    class ObjToolConvex :
        public ObjectData
    {
    public:

        ///Creates convex volume from the selection
        /**
         @param sel selection to use
         @param removeOriginal if true, removes original mesh. If false, convex volume is created 
            around original mesh
         @param regions hint value specifies initial count of regions, where the important points are searched.
            Lowest value is 2, that creates tree regions (each for one degree of freedom). Value is always
            recalculated by expression regions * (regions/2), so specify 8 to create 32 regions. Low count of regions can cause, that
            a lot of vertices will be outside of pregenerated volume - generated from important points - 
            and will not help to speedup generation. Too high count of regions can cause, that count of 
            important vertices will slow pregeneration of the volume.
         @param stage1only will use result of first stage as final result. This can dramtically speedup 
            calculation, but result is only aproximation
         */
        void MakeConvex(const Selection &sel, bool removeOriginal = true, int regions = 8,bool stage1only = false);

        struct SplitToConvexProp {
            ///true to remove original selection from the mesh (default:true)
            bool removeOriginal;
            ///convexity will be calculated only in first stage only (faster) (def: false)
            bool stage1only;
            ///count of regions used to calculate first stage (default: 8)
            int regions;
            ///Max distance from plane on negative side of point, which will be also treat as inside
            float convexAccuracy; 
            ///Max distance from plane of points for overlapping components
            float overlapTolerance;            
            ///Limit to maximum recursive levels when best component is searched
            int maxLevels;

            SplitToConvexProp()
                :removeOriginal(true)
                ,stage1only(false)
                ,regions(8)
                ,convexAccuracy(0.2f)
                ,overlapTolerance(0.05f)
                ,maxLevels(20) {}
        };



        ///Creates set of convex volumes from the selection
        /**
          @param sel selection to use
          @param removeOriginal if true, removes original mesh. If false, convex volume is created 
             around original mesh
          @param regions hint value specifies initial count of regions, where the important points are searched.
             Lowest value is 2, that creates tree regions (each for one degree of freedom). Value is always
             recalculated by expression regions * (regions/2), so specify 8 to create 32 regions. Low count of regions can cause, that
             a lot of vertices will be outside of pregenerated volume - generated from important points - 
             and will not help to speedup generation. Too high count of regions can cause, that count of 
             important vertices will slow pregeneration of the volume.
          @param stage1only will use result of first stage as final result. This can dramtically speedup 
            calculation, but result is only aproximation
        */
        void SplitToConvex(Selection sel, const SplitToConvexProp &prop);

        ///Creates set of convex volumes from the selection
        /**
        Works similar to SplitToConvex, with difference that it splits mesh into components and
        then it processes SplitToConvex component by component
        */
        void SplitToConvexByComponents(Selection sel, const SplitToConvexProp &prop);



    protected:

        struct PlaneNfo {

            Vector3 planeNorm;  ///< normal of plane (calculated from these three points
            float position;     ///< position of the plane ([x,y,z]*[planeNorm] + position)

            bool Test(const Vector3 &pt) const;
            int Side(const Vector3 &pt) const;
            float Distance(const Vector3 &pt) const;
            ClassIsMovable(PlaneNfo);
            PlaneNfo(const Vector3 &pt1, 
                     const Vector3 &pt2, 
                     const Vector3 &pt3);
            PlaneNfo(const FaceT &fc);
            PlaneNfo() {}

        };

        typedef Array<PlaneNfo> PlaneArr;
        typedef AutoArray<PlaneNfo> PlaneList;


        void PrepareImportantPoints(const Array<int> &selection, int regions, AutoArray<int> &result) const;

        ///Finds plane for given volume
        /**
         @param points defines the volume
         @param pt1 first point of plane
         @param pt2 second point of plane
         @param result receives found plane
         @retval >=0 third point of the plane
         @retval -1 failed
         @note function finds third point and fills result. If there is no usable plane, function
         return -1 and will not modify the result
         */
        int FindPlane(const Array<int> &points, int pt1, int pt2, PlaneNfo &result) const;

        void CreatePolygons(PlaneList &planes, CEdges &pointsOnPlanes, 
                            Array<int> &points, Selection &toDel);


        bool RunEdgeTest(int firstFace, int lastFace, Selection &todel);
        ///Test's whether point is inside of volume
        /**
         @param planes volume specified as list of planes
         @param vx vector to test
         */
 
        bool IsPointInside(const PlaneArr &planes, const Vector3 &vx) const;

        int CalculateFaceCull(int face, PlaneArr &volume) const;

        
        
        void MakePlaneList(PlaneList &planes, Array<int> &points, AutoArray<SimpleFace> *faces = 0) const;    
//        void MakePlaneList(PlaneList &planes, CEdges &pointsOnPlanes, Array<int> &points) const;    

        void CreateMesh(const Array<SimpleFace> &faces, Selection &todel);

        void CloseGeometry(CEdges &egs, int p1, int p2, Selection &todel);

        bool SplitToConvex_stage2(ObjToolConvex &trg, int regions, bool stage1only) ;

        int CalculatePlaneVolumePenalty(const Selection &sel, 
                                        const PlaneNfo &plane, Selection &newsel) const;

        AutoArray<int> SplitToConvex2_CreateBestGroup(
            int start,
            const ConvexIncidence &incd,
            const CEdges &graph,
            Selection &sel,
            AutoArray<int> &curgroup,
            Selection &wrk,
            int level
            );


        AutoArray<int>  SplitToConvex2_TryToRemoveConflict(
            int start,
            const ConvexIncidence &incd,
            const CEdges &graph,
            Selection &sel,
            const AutoArray<int> &curgroup,
            Selection &wrk,
            int level
            );

        
        void SplitToConvex2_ModifyFaceGraph(const Selection &origin, const Selection &toremove,
                    CEdges &graph);

        AutoArray<int> SplitToConvex2_FilterLargestConnectedRegion(const CEdges &graph, const AutoArray<int> &group);

        ///Optimizes two components
        /**
           @param i1 first index belongs to first component
           @param i2 first index that is not belongs to first component, i1<i2
           @param j1 first index belongs to second component
           @param j2 first index that is not belongs to second component, i1<i2
           @param delFaces selection which will be filled with faces designed to remove
           @param prop properties for operation
           @note function will run for i->j and j->i
         */
        void OptimizeComponents(int i1, int i2, int j1, int j2, Selection &delFaces,const SplitToConvexProp &prop);
        ///Optimizes two components
        /**
           @param i1 first index belongs to first component
           @param i2 first index that is not belongs to first component, i1<i2
           @param j1 first index belongs to second component
           @param j2 first index that is not belongs to second component, i1<i2
           @param delFaces selection which will be filled with faces designed to remove
           @param prop properties for operation
           @note function will run only for i->j
           @retval true any operation performed
           @retval false no operation performed
         */
        bool OptimizeComponents2(int i1, int i2, int j1, int j2, Selection &delFaces,const SplitToConvexProp &prop);

#if 0        
        ///Creates convex group
        /**
         @param original original selection
         @param convex selected points that can be used to create convex volume - convex group
         @param remain remain vertices. Selection can be used to generate new convex group in the chain
         @param faceGraph precalculated face graph. This is constant during calculation
        */
        void CreateConvexGroup(const Selection &original, 
                                Selection &convex, Selection &remain,
                                const CEdges &faceGraph);

        int calculateFaceCull(int face,const Array<int> &verticeList) const;
        
        void CreateConvexGroupRecursive(const CEdges &faceGraph,
                Selection &wrk,
                int startFace, const Array<int> &vertices,
                AutoArray<int> &bestGrp);

#endif
};


/*
class MinipartCreator {


    struct FaceInfo {

        int vx[3];      ///<cisla vertexu
        int link[3];   ///<indexy sousedu  (-1 neni definovan)


        FaceInfo() {}
        FaceInfo(int a, int b, int c, int l1, int l2, int l3) {
            vx[0] = a;
            vx[1] = b;
            vx[2] = c;
            link[0] = l1;
            link[1] = l2;
            link[2] = l3;
        }

        ClassIsSimpleZeroed(FaceInfo);
    };    

    struct Tetrahedron {
        
        FaceInfo f1[4]; ///<facy ctyrstenu
        int link[4];    ///<indexy sousedu (-1 neni definovan)

        ClassIsSimpleZeroed(Tetrahedron);    
    };

    struct LinkedFace: public FaceInfo {
        int tetraLink;
    public:
        LinkedFace():tetraLink(-1) {}
        LinkedFace(int a, int b, int c, int l1, int l2, int l3):
                FaceInfo(a,b,c,l1,l2,l3) {}
        ClassIsSimpleZeroed(LinkedFace);
    };


    typedef AutoArray<Tetrahedron> TetraList;
    typedef AutoArray<LinkedFace> SourceObject;

    TetraList _tetraList;
    SourceObject _sourceObject;

    

};
 */
}