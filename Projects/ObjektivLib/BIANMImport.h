// BIANMImport.h: interface for the CBIANMImport class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BIANMIMPORT_H__5FF9DB64_2C98_45E4_8C1D_563EF7B82E9D__INCLUDED_)
#define AFX_BIANMIMPORT_H__5FF9DB64_2C98_45E4_8C1D_563EF7B82E9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define CBIANM_MAXITEMS 256
#define CBIANM_MAXITEMSSTR "256"


namespace ObjektivLib {

struct CBIANMHierarchyItem
  {
  enum Channels
    {ch_tx,ch_ty, ch_tz, ch_rx, ch_ry, ch_rz, ch_sx, ch_sy, ch_sz, ch_unused};
  RString name;  
  Matrix4 curmx;  ///<aktualni matice
  Matrix4 bindmx; ///<vazaci matice
  Matrix4 globmx; ///<celkova matice
  char rotorder[3];   ///<XYZ nebo YZX atd
  char trnorder[3];   ///<XYZ nebo YZX atd
  char sclorder[3];   ///<XYZ nebo YZX atd
  float t[3];    ///< kanaly pro translaci
  float r[3];     ///< kanaly pro rotaci
  float s[3];     ///< kanaly pro scale
  int parent;     ///< cislo parentu
  Channels channels[9];
  public:
      int LoadChannels(std::istream &infile);
    void CalcCurrentMatrix(bool bindframe);
  protected:
  };










class CBIANMImport  
  {
  CBIANMHierarchyItem _skeleton[CBIANM_MAXITEMS];
  int _maxbones;
  RString _nextword;
  enum _LexSymb 
    {lx_JOINT,lx_ROOT,lx_PARENT,lx_TRANSLATE,lx_ORIENT,lx_SCALE,lx_CHANNELS,lx_END,lx_NONE,lx_EOF, lx_XYZORDER,lx_FRAME,lx_XSTEP,lx_ZSTEP};
  
  struct _LexInfo
    {
    const char *text;
    _LexSymb value;
    };
  
  _LexSymb _nextsymb;
  
  static _LexInfo lxinfo[];
  double _xstep;
  double _zstep;
  bool _usesteps;
  
  
  public:
      int ParseNextFrame(std::istream &infile, int *errline=NULL);
    void LoadAnimationFrame(ObjectData &obj,AnimationPhase &anm,float masterscale, bool invertx, bool inverty, bool invertz);
    void ImportBones(ObjectData &obj,float scale, bool invertX, bool invertY, bool invertZ);
    int FindBone(const char *name);
    _LexSymb IsLexSymb(const char *text);
    const char * GetError(int id);
    int ParseSkeleton(std::istream &infile, int *errline=NULL);
    CBIANMImport();
    virtual ~CBIANMImport();

    static void CreateBonePrimitive(ObjectData &obj,Vector3 direction, float length);


  protected:
      int ReadNextFrame(std::istream &infile);
    void CalcBoneMatrices(bool bindframe);
    void SortHierarchy();
    int ReadBone(std::istream &infile, CBIANMHierarchyItem &bone, RString &parent);
    int ReadSkeleton(std::istream &infile);
    bool ReadNext(std::istream &infile);
    int AddBone(std::istream &infile);
  };

}









#endif // !defined(AFX_BIANMIMPORT_H__5FF9DB64_2C98_45E4_8C1D_563EF7B82E9D__INCLUDED_)

