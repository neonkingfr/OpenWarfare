#pragma once

namespace ObjektivLib {


class ObjToolMapping: public ObjectData
{
public:
    enum NMState
    {
        ///okay, succes
        nmsOk,
        ///failed, no mapped faces found
        nmsNoMapped,
        ///failed, no unmapped faces found
        nmsNoUnmaped
    };

    NMState NeighbourMapping(const Selection *sel = 0);    

    ///Creates some UV mapping, that have texel 1:1 aspect ratio
    void MapFaceDefault(FaceT &fc);

    void AutoMapping(AutoArray<int> *mappingGroups = 0);
    void AutoMappingAndArrange();

private:

    int findBestNgFace(Array<int> faces, const CEdges &faceGraph, const Selection *limitFaces);
    bool calcNeighbourMapping(int face, const CEdges &faceGraph, const Selection *limitFaces = 0);
    float getNgScore(FaceT &fc, const CEdges &faceGraph, const Selection *limitFaces);
    bool isInCollision(const Array<int> &facesPlaced, int faceToTest) const;



};

  struct FPoint2D
  {
    float x, y;
    FPoint2D() {}
    FPoint2D(float x, float y):x(x),y(y) {}
    FPoint2D operator-() const
    {
      return FPoint2D(-x,-y);
    }
    float Distance2(const FPoint2D &from)
    {
      return ((*this)-from).Size2();
    }
    FPoint2D operator-(const FPoint2D &other) const
    {
      return FPoint2D(x-other.x,y-other.y);
    }
    FPoint2D operator+(const FPoint2D &other) const
    {
      return FPoint2D(x+other.x,y+other.y);
    }
    FPoint2D operator*(float v) const
    {
      return FPoint2D(x*v,y*v);
    }
    float Size2()
    {
      return x*x+y*y;
    }
    float Size()
    {
      return sqrt(Size2());
    }
    float Distance(const FPoint2D &other)
    {
      return sqrt(Distance2(other));
    }
  };

  struct FRect
  {
    FPoint2D leftTop;
    FPoint2D rightBottom;
    float Cx() const {return rightBottom.x-leftTop.x;}
    float Cy() const {return rightBottom.y-leftTop.y;}
    FRect() {}
    FRect(float left,float top, float right, float bottom):
    leftTop(left,top),rightBottom(right,bottom) {}
    FRect(const FPoint2D &pt1,const FPoint2D &pt2):
    leftTop(pt1),rightBottom(pt2) {}

    bool IsNull() const
    {
      return leftTop.x==0 && leftTop.y==0 && rightBottom.x==0 && rightBottom.y==0;
    }
    bool IsEmpty() const
    {
      return leftTop.x>=rightBottom.x && leftTop.y>=rightBottom.y;
    }
    bool IsValid() const
    {
      return leftTop.x<=rightBottom.x && leftTop.y<=rightBottom.y;
    }
    FPoint2D CenterPoint() const
    {
      return FPoint2D((leftTop.x+rightBottom.x)*0.5f,(leftTop.y+rightBottom.y)*0.5f);
    }
    bool PtInRect(const FPoint2D &pt) const
    {
      return pt.x>=leftTop.x && pt.x<rightBottom.x &&
          pt.y>=leftTop.y && pt.y<rightBottom.y;
    }


  };

  struct FTransform
  {
    float mx[3][2];

    FPoint2D Transform(const FPoint2D &pt, float w=1.0f) const
    {   
      register float x,y;
      x=mx[0][0]*pt.x+mx[1][0]*pt.y+mx[2][0]*w;
      y=mx[0][1]*pt.x+mx[1][1]*pt.y+mx[2][1]*w;
      return FPoint2D(x,y);
    }
    FTransform operator*(const FTransform &other) const
    {
      FPoint2D pt;
      FTransform res;
      pt=other.Transform(FPoint2D(mx[0][0],mx[0][1]),0);
      res.mx[0][0]=pt.x;res.mx[0][1]=pt.y;
      pt=other.Transform(FPoint2D(mx[1][0],mx[1][1]),0);
      res.mx[1][0]=pt.x;res.mx[1][1]=pt.y;
      pt=other.Transform(FPoint2D(mx[2][0],mx[2][1]),1.0f);
      res.mx[2][0]=pt.x;res.mx[2][1]=pt.y;
      return res;
    }

    FTransform &Identity()
    {
      mx[0][0]=mx[1][1]=1;
      mx[0][1]=mx[1][0]=mx[2][0]=mx[2][1]=0;
      return *this;
    }

    FTransform &Rotation(float angle)
    {
      float sins,coss;
      sins=(float)sin(angle);
      coss=(float)cos(angle);
      mx[0][0]=coss;
      mx[0][1]=sins;
      mx[1][0]=-sins;
      mx[1][1]=coss;
      mx[2][0]=0;
      mx[2][1]=0;
      return *this;
    }
    FTransform &Translation(FPoint2D pt)
    {
      mx[0][0]=mx[1][1]=1;
      mx[0][1]=mx[1][0]=0;
      mx[2][0]=pt.x;mx[2][1]=pt.y;
      return *this;
    }
    FTransform &Scale(FPoint2D &scale)
    {
      mx[0][0]=scale.x;
      mx[1][1]=scale.y;
      mx[0][1]=mx[1][0]=mx[2][0]=mx[2][1]=0.0f;
      return *this;
    }
  };

}