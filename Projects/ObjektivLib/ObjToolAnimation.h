#ifndef __OBJ_TOOL_ANIMATION_CLASS_HEADER_
#define __OBJ_TOOL_ANIMATION_CLASS_HEADER_

#pragma once


#include "ObjectData.h"

namespace ObjektivLib {


struct CoordSpace
  {
  int pt[4];
  };


class ObjToolAnimation: public ObjectData
  {
  public:
    void AutoTimeAnimations(bool last1); // rename positive time animations
    void HalfRateAnimation();
    void MirrorAnimation();
    void SoftenAnimation (float valueX, float valueY, float valueZ,bool actualVsAverage, float timeRange, bool interpolate, bool looped);
    int FindCoordSpaces(CoordSpace coord[MAX_NAMED_SEL], const AnimationPhase &src0) const;
    void AutoAnimation( ObjectData *src );
    void AutoAnimation( const char *file );
    Vector3 GetStep(const char *selection) const; // return step length
    float StepLength(const char *selection) const 
      {return GetStep(selection).Z();} // return step length
    float XStepLength(const char *selection) const 
      {return GetStep(selection).X();} // return step length
    
    void RemoveStep( Vector3 vec ); // perform step correction
    
    void RemoveZStep( float len )
      {RemoveStep(Vector3(0,0,len));} // perform step correction
    void RemoveXStep( float len )
      {RemoveStep(Vector3(len,0,0));} // perform step correction
    void NormalizeCenter( bool normX, bool normZ, const char *centername);
    
    void ExportAnimation( const char *file, float step, float xStep ) const;
    // measure step length and perform correction before looped export
    void ExportAnimationLooped( const char *file,const char *stepsel ) const;
  };

typedef void (*ObjData_NoCoordinatePointsMessage_Function)(const char *selection_name);
extern ObjData_NoCoordinatePointsMessage_Function ObjData_NoCoordinatePointsMessage;

}

#endif