// BIANMImport.cpp: implementation of the CBIANMImport class.
//
//////////////////////////////////////////////////////////////////////

#include "Common.hpp"
#include "LODObject.h"
#include "ObjectData.h"
#include "BIANMImport.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define KWD_JOINT "JOINT:"
#define KWD_ROOT "ROOT:"
#define kWD_PARENT "PARENT:"

namespace ObjektivLib {

CBIANMImport::CBIANMImport()
  {
  _maxbones=0;

  }

CBIANMImport::~CBIANMImport()
  {

  }

CBIANMImport::_LexInfo CBIANMImport::lxinfo[]=
  {
  {"JOINT:",CBIANMImport::lx_JOINT},
  {"ROOT:",CBIANMImport::lx_ROOT},
  {"PARENT:",CBIANMImport::lx_PARENT},
  {"CHANNELS:",CBIANMImport::lx_CHANNELS},
  {"TRANSLATE:",CBIANMImport::lx_TRANSLATE},
  {"ORIENT:",CBIANMImport::lx_ORIENT},
  {"SCALE:",CBIANMImport::lx_SCALE},
  {"FRAME:",CBIANMImport::lx_FRAME},
  {"END",CBIANMImport::lx_END},
  {"XYZ",CBIANMImport::lx_XYZORDER},
  {"YZX",CBIANMImport::lx_XYZORDER},
  {"ZXY",CBIANMImport::lx_XYZORDER},
  {"XZY",CBIANMImport::lx_XYZORDER},
  {"ZYX",CBIANMImport::lx_XYZORDER},
  {"YXZ",CBIANMImport::lx_XYZORDER},
  {"XY",CBIANMImport::lx_XYZORDER},
  {"XZ",CBIANMImport::lx_XYZORDER},
  {"YX",CBIANMImport::lx_XYZORDER},
  {"YZ",CBIANMImport::lx_XYZORDER},
  {"ZX",CBIANMImport::lx_XYZORDER},
  {"ZY",CBIANMImport::lx_XYZORDER},
  {"X",CBIANMImport::lx_XYZORDER},
  {"Z",CBIANMImport::lx_XYZORDER},
  {"Y",CBIANMImport::lx_XYZORDER},
  {"XSTEP:",CBIANMImport::lx_XSTEP},
  {"ZSTEP:",CBIANMImport::lx_ZSTEP},
  };




static int GetErrorLine(istream &infile)
  {
  int p=1;
  infile.clear();
  int pos=infile.tellg();
  infile.seekg(0);
  infile.ignore(0x7fffffff,'\n');
  while (pos>infile.tellg()) 
    {
    p++;
    infile.ignore(0x7fffffff,'\n');
    if (!infile) return p;
    infile.clear();
    }
  return p;
  }

int CBIANMImport::ParseSkeleton(istream &infile, int *errline)
  {
  int err=ReadSkeleton(infile);
  if (err && errline) *errline=GetErrorLine(infile);
  return err;
  }

bool CBIANMImport::ReadNext(istream &infile)
  {
  char buff[256];
  int c;
  int i=0;
  ws(infile);
  for(c=infile.get(),i=0;!isspace(c) && c!=EOF && i<255;c=infile.get(),i++) buff[i]=c;
  buff[i]=0;
 _nextword=buff;
  if (buff[0]==0 && c==EOF)    
    _nextsymb=lx_EOF;
  else
    _nextsymb=IsLexSymb(buff);
  return !(!infile);
  }

int CBIANMImport::ReadSkeleton(istream &infile)
  {
  int err=0;
  _maxbones=0;
  if (!ReadNext(infile)) return -1;
  while ((_nextsymb==lx_JOINT || _nextsymb==lx_ROOT) && err==0)    
    err=AddBone(infile);
  if (!err) 
    {
    SortHierarchy();
    CalcBoneMatrices(true);
    }
  _usesteps=false;
  while (_nextsymb==lx_XSTEP || _nextsymb==lx_ZSTEP)
  {
    _LexSymb symb=_nextsymb;
    if (!ReadNext(infile)) return -1;
    if (symb==lx_XSTEP) _xstep=(float)strtod(_nextword,NULL);
    if (symb==lx_ZSTEP) _zstep=(float)strtod(_nextword,NULL);
    if (!ReadNext(infile)) return -1;
    _usesteps=true;
  }
  return err;
  }

const char * CBIANMImport::GetError(int id)
  {
  switch (id)
    {
    case 0: return "No errors found";
    case -1: return "General read error";
    case -2: return "Excepting JOINT: or ROOT: keyword (maybe missing one bone definition)";
    case -3: return "Excepting bone name";
    case -4: return "Excepting TRANSLATE or ORIENT or SCALE or CHANNELS or END";
    case -5: return "Excepting axis order (eg. 'XYZ' or 'ZYX' etc.)";
    case -6: return "Excepting real number";
    case -7: return "Unknown channel";
    case -8: return "Maximum bones reached. Number of bones in one skeleton is limited to "CBIANM_MAXITEMSSTR" items. ";
    case -9: return "Excepting integer number";
    case -10: return "Referenced bone has not been defined in skeleton";
    case -11: return "Channel definition mismatch (error in skeleton definition)";
    case -12: return "No more frames (import ok)";
    case -13: return "Exception ':'";
    case -14: return "Excepting FRAME or end of file";
    }
  return "Unspecified error";
  }

int CBIANMImport::AddBone(istream &infile)
  {
  if (_maxbones>=CBIANM_MAXITEMS) 
    return -8;
  RString parentname;
  CBIANMHierarchyItem &item=_skeleton[_maxbones];
  _maxbones++;
  int err=ReadBone(infile,item,parentname);  
  if (err) return err;
  ReadNext(infile);
  if (parentname[0])
    {
    int bone=FindBone(parentname);
    while (bone==-1) 
      {
      err=AddBone(infile);
      if (err) return err;
      bone=FindBone(parentname);
      }
    item.parent=bone;
    }
  else
    item.parent=-1;
  return 0;
  }

CBIANMImport::_LexSymb CBIANMImport::IsLexSymb(const char *text)
  {
  for (int i=0;i<sizeof(lxinfo)/sizeof(lxinfo[0]);i++)    
    if (strcmp(lxinfo[i].text,text)==0) return  lxinfo[i].value;
  return lx_NONE;
  }

int CBIANMImport::FindBone(const char *name)
  {
  for (int i=0;i<_maxbones;i++)
    {
    if (_skeleton[i].name==name) return i;
    }
  return -1;
  }

int CBIANMImport::ReadBone(istream &infile, CBIANMHierarchyItem &item, RString &parentname)
  {
  if (_nextsymb!=lx_JOINT && _nextsymb!=lx_ROOT) return -2;
  ReadNext(infile);
  if (_nextsymb!=lx_NONE) return -3;
  RString bonename=_nextword;
  ReadNext(infile);
  if (_nextsymb==lx_PARENT)
    {
    ReadNext(infile);
    if (_nextsymb!=lx_TRANSLATE && _nextsymb!=lx_ORIENT && _nextsymb!=lx_SCALE && _nextsymb!=lx_CHANNELS && _nextsymb!=lx_END) 
      {
      parentname=_nextword;      
      ReadNext(infile);
      }
    }
  while (_nextsymb!=lx_END)
    {
    if (_nextsymb==lx_NONE) return -4;
    switch (_nextsymb)
      {
      case lx_TRANSLATE: 
          ReadNext(infile);
          if (_nextsymb!=lx_XYZORDER) return -5;
          strncpy(item.trnorder,_nextword,3);
          ReadNext(infile);
          if (item.trnorder[0])
            {
            if (sscanf(_nextword,"%f",&item.t[0])!=1) return -6;
            ReadNext(infile);
            if (item.trnorder[1])
              {
              if (sscanf(_nextword,"%f",&item.t[1])!=1) return -6;
              ReadNext(infile);
              if (item.trnorder[2])
                {
                if (sscanf(_nextword,"%f",&item.t[2])!=1) return -6;
                ReadNext(infile);
                }
              }
            }
        break;
      case lx_ORIENT: 
          ReadNext(infile);
          if (_nextsymb!=lx_XYZORDER) return -5;
          strncpy(item.rotorder,_nextword,3);
          ReadNext(infile);
          if (item.rotorder[0])
            {
            if (sscanf(_nextword,"%f",&item.r[0])!=1) return -6;
            ReadNext(infile);
            if (item.rotorder[1])
              {
              if (sscanf(_nextword,"%f",&item.r[1])!=1) return -6;
              ReadNext(infile);
              if (item.rotorder[2])
                {
                if (sscanf(_nextword,"%f",&item.r[2])!=1) return -6;
                ReadNext(infile);
                }
              }
            }
        break;
      case lx_SCALE: 
          ReadNext(infile);
          if (_nextsymb!=lx_XYZORDER) return -5;
          strncpy(item.sclorder,_nextword,3);
          ReadNext(infile);
          if (item.sclorder[0])
            {
            if (sscanf(_nextword,"%f",&item.s[0])!=1) return -6;
            ReadNext(infile);
            if (item.sclorder[1])
              {
              if (sscanf(_nextword,"%f",&item.s[1])!=1) return -6;
              ReadNext(infile);
              if (item.sclorder[2])
                {
                if (sscanf(_nextword,"%f",&item.s[2])!=1) return -6;
                ReadNext(infile);
                }
              }
            }
        break;
      case lx_CHANNELS:
        {
        int pos=0;
          ReadNext(infile);
          while (_nextsymb==lx_NONE && pos<9)
            {
            if (_nextword.GetLength()!=2) return -7;
            short mark=MAKEWORD(_nextword[0],_nextword[1]);
            CBIANMHierarchyItem::Channels channame;
            switch (mark)
              {
              case MAKEWORD('t','x'): channame=CBIANMHierarchyItem::ch_tx;break;
              case MAKEWORD('t','y'): channame=CBIANMHierarchyItem::ch_ty;break;
              case MAKEWORD('t','z'): channame=CBIANMHierarchyItem::ch_tz;break;
              case MAKEWORD('r','x'): channame=CBIANMHierarchyItem::ch_rx;break;
              case MAKEWORD('r','y'): channame=CBIANMHierarchyItem::ch_ry;break;
              case MAKEWORD('r','z'): channame=CBIANMHierarchyItem::ch_rz;break;
              case MAKEWORD('s','x'): channame=CBIANMHierarchyItem::ch_sx;break;
              case MAKEWORD('s','y'): channame=CBIANMHierarchyItem::ch_sy;break;
              case MAKEWORD('s','z'): channame=CBIANMHierarchyItem::ch_sz;break;
              default: return -7;
              }
            item.channels[pos++]=channame;
            ReadNext(infile);
            }
          if (pos<9) item.channels[pos]=CBIANMHierarchyItem::ch_unused;
        }
        break;
      default:
        return -4;
      }
    }
  item.name=bonename;
  return 0;
  }

void CBIANMImport::SortHierarchy()
  {
  CBIANMHierarchyItem temp[CBIANM_MAXITEMS]; //zalozni pole kosti
  int tempvals[CBIANM_MAXITEMS];  //pomocne prochazeci pole
  int i;      //prochazec
  int pos=0;  //ukazatel na dalsi volnou kost
  for (i=0;i<_maxbones;i++) 
    {
    temp[i]=_skeleton[i]; //prekopiruj pole kosti;
    tempvals[i]=-1;    //oznac kost za neprochazenou
    if (temp[i].parent==-1) //najdi parent
      {
      if (i!=pos) _skeleton[pos]=temp[i]; //soupni ho na prvni pozici
      tempvals[i]=pos; //poznac ci ze je tam
      pos++;
      }
    }
  bool rep;
  do
    {
    rep=false;
    for (i=0;i<_maxbones;i++)
      {
      if (tempvals[i]==-1 && tempvals[temp[i].parent]!=-1) //je to primy potomek?
        {
        if (i!=pos) _skeleton[pos]=temp[i]; //soupni ho dalsi pozici
        tempvals[i]=pos; //poznac si ze je tam
        _skeleton[pos].parent=tempvals[temp[i].parent]; //nastav novy parent;
        pos++;        
        rep=true; //vyzadej si dalsi cyklus
        }
      }
    }
  while (rep); //dokud je co presouvat
  }

void CBIANMImport::CalcBoneMatrices(bool bindframe)
  {
  int i=0;
  for (i=0;i<_maxbones;i++) _skeleton[i].CalcCurrentMatrix(bindframe);
  for (i=0;i<_maxbones;i++)
    {
    CBIANMHierarchyItem &it=_skeleton[i];
    if (_skeleton[i].parent==-1) it.globmx=it.curmx;
    else it.globmx=_skeleton[it.parent].globmx*it.curmx;
    }
  }

static void OrderAxis(char *order, float *values, float &x, float &y, float &z)
  {
  int i;
  for (i=0;i<3 && order[i];i++) 
    switch (order[i])
      {
      case 'X': x=values[i];break;
      case 'Y': y=values[i];break;
      case 'Z': z=values[i];break;
      }
  }

#define M_PI 3.1415926536f
#define torad(x) ((x)*M_PI/180)

static void OrderRotation(char *order, float *values, Matrix4 &mm)
  {
  int i;
  mm=Matrix4(M4Identity);
  Matrix4 pom;  
  for (i=0;i<3 && order[i];i++) 
    {
    switch (order[i])
      {
      case 'X': pom.SetRotationX(torad(values[i]));break;
      case 'Y': pom.SetRotationY(-torad(values[i]));break;
      case 'Z': pom.SetRotationZ(torad(values[i]));break;
      }
//    SoucinMatic(pom,mm,pom2);
//    CopyMatice(mm,pom2);
    mm=pom*mm;
    }
  }


void CBIANMHierarchyItem::CalcCurrentMatrix(bool bindframe)
  {
  Matrix4 pom(MIdentity);
  float x,y,z;
  OrderAxis(sclorder,s,x,y,z);
  curmx.SetScale(x,y,z);
  OrderRotation(rotorder,r,pom);
  if (bindframe) bindmx=pom;
    else 
      {
      Matrix4 p2;
      p2=pom;
      pom=bindmx*p2;
      }
  curmx=pom*curmx;
  OrderAxis(trnorder,t,x,y,z);
  pom.SetTranslation(Vector3(x,y,z));
  curmx=pom*curmx;
  }

void CBIANMImport::ImportBones(ObjectData &obj,float scale, bool invertX, bool invertY, bool invertZ)
  {
  obj.ClearSelection();
  int p=obj.NPoints();
  for (int i=0;i<_maxbones;i++)
    {
    for (int j=i+1;j<_maxbones;j++)
      if (_skeleton[j].parent==i)
        {
        Vector3 direction;
        direction=_skeleton[j].curmx.Position();
        float length=direction.Size();
        if (length>0.0f)
          {
          direction=direction*(1.0f/length);
          CreateBonePrimitive(obj,direction,length);
          }
        }
    for (;p<obj.NPoints();p++) obj.PointSelect(p);
    obj.SelectFacesFromPoints();
    obj.SaveNamedSel(_skeleton[i].name);
    obj.ClearSelection();    
    }
  if (_usesteps)
    {
      char buff[256];
      sprintf(buff,"%g",_zstep*scale*(invertZ?-1:1));
      obj.SetNamedProp("step",buff);
      sprintf(buff,"%g",_xstep*scale*(invertX?-1:1));
      obj.SetNamedProp("Xstep",buff);
    }
  }

void CBIANMImport::LoadAnimationFrame(ObjectData &obj,AnimationPhase &anm,float masterscale, bool invertx, bool inverty, bool invertz)
  {
  for (int i=0;i<_maxbones;i++) 
    {
    Selection *sel=obj.GetNamedSel(_skeleton[i].name);
    float ix=invertx?-1.0f:1.0f;
    float iy=inverty?-1.0f:1.0f;
    float iz=invertz?-1.0f:1.0f;
    if (sel)
      {
      for (int j=0;j<obj.NPoints();j++) if (sel->PointSelected(j))
        {
        PosT &pos=obj.Point(j);
        Vector3 vx;
        vx.SetFastTransform(_skeleton[i].globmx,pos);
        VecT vp;
        vp[0]=vx[0]*masterscale*ix;
        vp[1]=vx[1]*masterscale*iy;
        vp[2]=vx[2]*masterscale*iz;
        anm[j]=vp;
        }
      }
    }
  }

int CBIANMImport::ReadNextFrame(istream &infile)
  {
  if (_nextsymb==lx_EOF) return -12;
  if (_nextsymb!=lx_FRAME) return -14;
  ReadNext(infile);
  float numb;
  if (sscanf(_nextword,"%f",&numb)!=1) return -9;
  ReadNext(infile);  
  while (_nextsymb!=lx_END)
    {
    if (_nextword[_nextword.GetLength()-1]!=':') return -13;
    int bon=FindBone(_nextword.Mid(0,_nextword.GetLength()-1));
    if (bon==-1) return -10;
    CBIANMHierarchyItem &itm=_skeleton[bon];
    int err=itm.LoadChannels(infile);
    if (err) return err;
    ReadNext(infile);
    }
  CalcBoneMatrices(false);
  ReadNext(infile);
  return 0;
  }

int CBIANMHierarchyItem::LoadChannels(istream &infile)
  {
  memset(r,0,sizeof(r));
  for (int i=0;i<9 && channels[i]!=ch_unused;i++)
    {
    float val;
    infile>>val;
    if (!infile) return -6;
    char ch;
    float *field;
    char *order;
    switch (channels[i])
      {
      case ch_tx: ch='X';field=t;order=trnorder; break;
      case ch_ty: ch='Y';field=t;order=trnorder; break;
      case ch_tz: ch='Z';field=t;order=trnorder; break;
      case ch_rx: ch='X';field=r;order=rotorder; break;
      case ch_ry: ch='Y';field=r;order=rotorder; break;
      case ch_rz: ch='Z';field=r;order=rotorder; break;
      case ch_sx: ch='X';field=s;order=sclorder; break;
      case ch_sy: ch='Y';field=s;order=sclorder; break;
      case ch_sz: ch='Z';field=s;order=sclorder; break;
      }
    int pos;
    for (pos=0;pos<3 && order[pos] ;pos++) if (order[pos]==ch) break;
    if (pos==4 || order[pos]!=ch) return -11;
    field[pos]=val;
    }
  return 0;
  }

int CBIANMImport::ParseNextFrame(istream &infile, int *errline)
  {
  int err=ReadNextFrame(infile);
  if (err && err!=-12 && errline) *errline=GetErrorLine(infile);
  return err;
  }


void CBIANMImport::CreateBonePrimitive(ObjectData &obj,Vector3 direction, float length)
  {
    float ml=length/5;  
    if (fabs(length)<0.00001f) return;
    Vector3 x,y,z,t1(1,0,0),t2(0,1,0),tmp;
    x=direction;
    x.Normalize();
    tmp=x.CrossProduct(t1);
    y=x.CrossProduct(t2);
    if (tmp.SquareSize()>y.SquareSize()) y=tmp;
    y.Normalize();
    z=x.CrossProduct(y);
    z.Normalize();
    int p=obj.NPoints();
    obj.ReservePoints(5);
    PosT ps;
    ps[0]=-y[0]*ml; ps[1]=-y[1]*ml; ps[2]=-y[2]*ml;
    ps.flags=0;
    obj.Point(p)=ps;
    ps[0]=x[0]*length; ps[1]=x[1]*length; ps[2]=x[2]*length;
    obj.Point(p+1)=ps;
    ps[0]=y[0]*ml; ps[1]=y[1]*ml; ps[2]=y[2]*ml;
    obj.Point(p+2)=ps;
    ps[0]=z[0]*ml; ps[1]=z[1]*ml; ps[2]=z[2]*ml;
    obj.Point(p+3)=ps;
    ps[0]=-z[0]*ml; ps[1]=-z[1]*ml; ps[2]=-z[2]*ml;
    obj.Point(p+4)=ps;
    int f=obj.NFaces();
    obj.ReserveFaces(5);
    FaceT fc(obj);  
    fc.SetN(3);fc.SetPoint(0,p+0);fc.SetPoint(1,p+1);fc.SetPoint(2,p+4);
    fc.SetFlags(FACE_COLORIZE_MASK,1<<FACE_COLORIZE_SHIFT);
    fc.CopyFaceTo(f);  
    fc.SetPoint(0,p+1);fc.SetPoint(1,p+2);fc.SetPoint(2,p+4);
    fc.SetFlags(0);
    fc.CopyFaceTo(f+1);  
    fc.SetPoint(0,p+1);fc.SetPoint(1,p+3);fc.SetPoint(2,p+2);
    fc.CopyFaceTo(f+2);  
    fc.SetPoint(0,p+0);fc.SetPoint(1,p+3);fc.SetPoint(2,p+1);
    fc.CopyFaceTo(f+4);  
    fc.SetN(4);
    fc.SetPoint(0,p+0);fc.SetPoint(1,p+4);fc.SetPoint(2,p+2);fc.SetPoint(3,p+3);
    fc.CopyFaceTo(f+3);    
  }

}