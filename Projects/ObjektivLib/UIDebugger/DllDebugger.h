
#include <El\Evaluator\ScriptDebuggerBase.h>

enum DllDebugMallocFreeOp {
    DDMallocFree,
    DDNewDelete,
    DDNewDeleteArr
};

#ifdef __DEBUGGER_EXPORT_SYMBOLS_
extern "C"
{
_declspec(dllexport) void  SetupDebuggerMemoryManager(void *malloc_funct, void *free_funct);
_declspec(dllexport) ScriptDebuggerBase *  InitDebugger();
_declspec(dllexport) void  DoneDebugger(ScriptDebuggerBase *_debugger);
_declspec(dllexport) ScriptDebuggerBase *  InitDebugger2(HWND owner, const char *windowTitle);
_declspec(dllexport) void  DoneDebuggerMemoryManagment();
}

#else

typedef void (* type_dlldebugger_SetupDebuggerMemoryManager)(void *malloc_funct, void *free_funct);
typedef ScriptDebuggerBase * (*type_dlldebugger_InitDebugger)();
typedef void (*type_dlldebugger_DoneDebugger)(ScriptDebuggerBase *_debugger);
typedef ScriptDebuggerBase * (*type_dlldebugger_InitDebugger2)(HWND owner, const char *windowTitle);
typedef void  (*type_dlldebugger_DoneDebuggerMemoryManagment)();

#endif
