#pragma once

#include "DllDebugger.h"
#include <Es/Common/win.h>

/*#ifndef _MT_ELES
#error _MT_ELES not defined. Application must be compiled with EL/ES libraries for multithread.
#endif
*/
class DllDebuggerProvider
{
  HMODULE _debugmod;
public:
type_dlldebugger_InitDebugger InitDebugger;
type_dlldebugger_DoneDebugger DoneDebugger;
type_dlldebugger_DoneDebuggerMemoryManagment DoneDebuggerMemoryManagment;
type_dlldebugger_InitDebugger2 InitDebugger2;
bool CreateProvider(const char *providerDll);
bool IsLoaded() {return _debugmod!=NULL;}
DllDebuggerProvider();
~DllDebuggerProvider();
};

class DllDebuggingSession
{
ScriptDebuggerBase *_debugger;
DllDebuggerProvider &_provider;

public:
  DllDebuggingSession(DllDebuggerProvider &provider);
  DllDebuggingSession(DllDebuggerProvider &provider, HWND owner, const char *title);
  ~DllDebuggingSession(void);
  operator ScriptDebuggerBase *() {return _debugger;}
};
