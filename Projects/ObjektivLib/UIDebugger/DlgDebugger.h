#pragma once
#include "resource.h"
#include "afxcmn.h"
#include "afxwin.h"
#include ".\dlgevaluate.h"
#include "DlgBreakWait.h"


// DlgDebugger dialog

#define DBGM_CONSOLEOUTPUT (WM_APP+1) //LPARAM text
#define DBGM_QUITDEBUGGER (WM_APP+2) //noparameters
#define DBGM_ONTRACE (WM_APP+3) //noparameters
#define DBGM_ONVARCHANGE (WM_APP+4) //WPARAM (const char *)name, LPARAM (GameValue *) value
#define DBGM_ONERROR (WM_APP+5) //WPARAM (const char *)name, LPARAM (GameValue *) value
#define DBGM_TRAYMESSAGE (WM_APP+6)

class CDebuggerSession ;
class IArchive;



class DlgDebugger : public CDialog
{
    struct BreakPoint
    {
      int blockChksumChars; //lengh of block, where breakpoint has been set
      DWORD blockChksum;   //checksum of block
      const char *expPos;   //pointer to block in current level
      const char *scopePos; //pointer to block, where pointer has been set      
      int offset;     //offset of breakpoint;
      bool checked;   //true, if breakpoint has been checked and it is active
      bool hstate;    //state to calculate impulse, that make breakpoint to break program. This happens when hstate changes from false to true
      
      
    };


	DECLARE_DYNAMIC(DlgDebugger)
    CCriticalSection _debuggerLock;
    CEvent _debuggerWait;
    CDebuggerSession  *_dbgobject;
    bool _breakSign;  //if true, script should be broken soon
    bool _inDebugger; //if true, script is stopped, and debugging is in process
    bool _termSign ;  //if true, script should terminate soon
    int _curTab;    //holds last known tab positions
    int _maxTab;//holds constant to convert tab pos to level pos _level=_maxTab-_curTab
    CFont fixfont;
    DlgEvaluate evalDlg;
    NOTIFYICONDATA _trayicon;
    float _splitx,_splity;
    bool _moveVert;
    bool _nonstoprun;
    BreakPoint _runToBreakpoint;
    bool _runToMode;
    DlgBreakWait _dlgbreakwait;
    DWORD _waitingThread;
    UINT _BTWTTtimer; //bring thread windows to the top
    CPoint _cmenupt;
    HWND _cmenuw;
    UINT _cmenutimer;
    BOOL _cmenudisable;
    HACCEL _hotkeys;
    bool _hotkeysdisable;
    CString *_enrouteError;

public:
	DlgDebugger(CDebuggerSession  *dbgobject, CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgDebugger();
    CString _dbgOutput;

// Dialog Data
	enum { IDD = IDD_DEBUGGER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    void DebuggerStateChange(bool state);
    bool CheckBreakpoint(BreakPoint &breakpt);

    
    DECLARE_MESSAGE_MAP()
public:
  CTabCtrl wContextTab;
  afx_msg void OnSize(UINT nType, int cx, int cy);
  void RecalcLayout(void);
  CRichEditCtrl wSourceView;
  CListCtrl wWatch;
  CListCtrl wAutowatch;
  virtual BOOL OnInitDialog();
  void BreakScript() {
    _breakSign=true;
    _nonstoprun=false;
    if (_dlgbreakwait.GetSafeHwnd()==NULL)
    {
      _dlgbreakwait.notify=this;
      _dlgbreakwait.lastThread=_waitingThread;
      _dlgbreakwait.Create(_dlgbreakwait.IDD,this);
    }
    _dlgbreakwait.CenterWindow();
  }
  void TerminateScript() {_termSign=true;BreakScript();}
  bool ShouldBreak() {return _breakSign;}
  bool GetRunNonstop() {return _nonstoprun;}
  bool GetTestBreakpoints();
  RString GetTypeName(GameType typd);
  static const char *FindBestBreakPosition(const char *text,const char *begin);
  static const char *FindBlockBegin(const char *text,const char *begin);
  static const char *FindBlockEnd(const char *text);


  LRESULT OnConsoleOutput(WPARAM wParam, LPARAM lParam);
  LRESULT OnQuitDebugger(WPARAM wParam, LPARAM lParam);
  LRESULT OnScriptError(WPARAM wParam, LPARAM lParam);
  LRESULT OnTrace(WPARAM wParam, LPARAM lParam);
  LRESULT OnEvaluateVariableChange(WPARAM wParam, LPARAM lParam);
  LRESULT OnTrayMessage(WPARAM wParam, LPARAM lParam);
  LRESULT OnVarBreak(WPARAM wParam, LPARAM lParam);
  LRESULT OnCancelBreak(WPARAM wParam, LPARAM lParam);
  LRESULT OnForceBreak(WPARAM wParam, LPARAM lParam);



    ///notified debugger, that process reached debugging trap, and waits until debugger releases the process
    /** After debugger notified, it may access context structure, a made any changes 
    in GGameState object, without need any MT synchronization. In this phase, 
    debuggie waiting in safe section, until ReleaseProgram called.*/
  void SwitchToDebugger(UINT msg, WPARAM wParam, LPARAM lParam); 
   
    ///Releases program.
    /**After debugger finished all request operation whith GGameState object, it can
    release program, and the it will continue to run. After return this function, debugger
    shouldn't access any variable in script state.
    */
  void ReleaseProgram();

  void ConsoleOutput(const char *text);
  afx_msg void OnDebuggingRun();
  afx_msg void OnDebuggerStepinto();
  afx_msg void OnDebuggerStepover();
  afx_msg void OnDebuggingBreak();
  afx_msg void OnDebuggingStepout();
  afx_msg void OnDebuggingTerminate();
protected:
  virtual void OnOK();
  virtual void OnCancel();
public:
  void UpdateTabs(void);
  afx_msg void OnTcnSelchangeContexttab(NMHDR *pNMHDR, LRESULT *pResult);
  void UpdateSourceView(void);
  void UpdateAutowatchVariableList(void);
  void UpdateWatchVariableList(void);
  void HandleDoubleclickOnList(CListCtrl &list);
  afx_msg void OnNMDblclkAutowatch(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnWatchSetvariablevalue();
  afx_msg void OnLvnEndlabeleditWatch(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkWatch(NMHDR *pNMHDR, LRESULT *pResult);
  void MinimizeTray(void);
  void RestoreTray(void);
  afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
  afx_msg void OnUpdateDebuggingCommand(CCmdUI *pCmdUI);
  afx_msg void OnUpdateWatchSetvariablevalue(CCmdUI *pCmdUI);
  afx_msg void OnWatchAddwatch();
  afx_msg void OnUpdateWatchAddwatch(CCmdUI *pCmdUI);
  afx_msg void OnWatchEditwatch();
  afx_msg void OnUpdateWatchEditwatch(CCmdUI *pCmdUI);
  afx_msg void OnWatchDeletewatch();
  afx_msg void OnUpdateWatchDeletewatch(CCmdUI *pCmdUI);
  CStatic wHShiftBar;
  CStatic wVShiftBar;
  afx_msg void OnStnClickedHshiftbar();
  afx_msg void OnStnClickedVshiftbar();
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnDebuggingRunnon();
  afx_msg void OnWatchWatchbreakpoints();
  afx_msg void OnNMRdblclkAutowatch(NMHDR *pNMHDR, LRESULT *pResult);
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  afx_msg void OnNMRdblclkWatch(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDebuggingRunto();
  void SaveState(void); 
  void LoadState(void);
  void SerializeState(IArchive &arch);
  afx_msg void OnTimer(UINT nIDEvent);
  afx_msg void OnDebuggerSetsourceposition();
  afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
  afx_msg void OnUpdateDebuggerSetsourceposition(CCmdUI *pCmdUI);
  afx_msg void OnUpdateDebuggingRun(CCmdUI *pCmdUI);
  afx_msg void OnLvnBeginlabeleditWatch(NMHDR *pNMHDR, LRESULT *pResult);
protected:
public:
  afx_msg void OnDestroy();
  afx_msg void OnWatchDetectundeclared();
  afx_msg void OnUpdateWatchDetectundeclared(CCmdUI *pCmdUI);

  void SetOwnerAndTitle(HWND owner, const char *title)
  {
  }

};
