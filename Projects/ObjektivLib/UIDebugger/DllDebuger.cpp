#include "stdafx.h"
#define __DEBUGGER_EXPORT_SYMBOLS_
#include "DebuggerSession.h"
#include "DllDebugger.h"


static void *(*_malloc_funct)(size_t,DllDebugMallocFreeOp);
static void (*_free_funct)(void *ptr,DllDebugMallocFreeOp);
static HMODULE thismodule;


extern "C"
{

  BOOL WINAPI _DllMainCRTStartup(
    HANDLE  hDllHandle,
    DWORD   dwReason,
    LPVOID  lpreserved
    );

  _declspec(dllexport) ScriptDebuggerBase *  InitDebugger()
{
AFX_MANAGE_STATE(AfxGetStaticModuleState());
CDebuggerSession *session=new CDebuggerSession;
session->runevent=CreateEvent(NULL,TRUE,FALSE,NULL);
session->m_bAutoDelete=FALSE;
session->Reset();
session->CreateThread();
WaitForSingleObject(session->runevent,INFINITE);
return session;
}
_declspec(dllexport) void  DoneDebugger(ScriptDebuggerBase *_debugger)
{
  AFX_MANAGE_STATE(::AfxGetModuleState());
  CDebuggerSession *session=static_cast<CDebuggerSession *>(_debugger);
  session->QuitDebugger();
  delete session;
}


_declspec(dllexport) void  SetupDebuggerMemoryManager(void *malloc_funct,void *free_funct)
{
  *(void **)&_malloc_funct=malloc_funct;
  *(void **)&_free_funct=free_funct;
  _DllMainCRTStartup(thismodule,DLL_PROCESS_ATTACH,0);
}

_declspec(dllexport) ScriptDebuggerBase *  InitDebugger2(HWND owner, const char *windowTitle)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  CDebuggerSession *session=new CDebuggerSession;
  session->SetOwnerAndTitle(owner,windowTitle);
  session->runevent=CreateEvent(NULL,TRUE,FALSE,NULL);
  session->m_bAutoDelete=FALSE;
  session->Reset();
  session->CreateThread();
  WaitForSingleObject(session->runevent,INFINITE);
  return session;
}

BOOL WINAPI UIDebuggerEntry(
                            HANDLE  hDllHandle,
                            DWORD   dwReason,
                            LPVOID  lpreserved
                            )
{
  if (dwReason!=DLL_PROCESS_ATTACH)
  {
   return  _DllMainCRTStartup(hDllHandle,dwReason,lpreserved);
  }
  else
  {
    thismodule=(HMODULE)hDllHandle;
    return TRUE;
  }
}


}

#define MAX_TEMP_ALLOC 16

static BTree<DWORD_PTR> *dll_alloc=NULL;
static DWORD_PTR dll_alloc_imdarray[MAX_TEMP_ALLOC];
static int dll_alloc_imdarray_cnt=0;
static bool dll_alloc_inalloc=false;

static inline void *DllAlloc(size_t sz, DllDebugMallocFreeOp op)
{
/*  if (_malloc_funct==NULL)          //definovan novy malloc?
  {
    void *res=malloc(sz);           //ne pouzij standardni alloc
    if (dll_alloc_inalloc)
    {
      ASSERT(dll_alloc_imdarray_cnt<MAX_TEMP_ALLOC);
      dll_alloc_imdarray[dll_alloc_imdarray_cnt++]=(DWORD_PTR)res;      
    }
    else
    {
      dll_alloc_inalloc=true;
      if (dll_alloc==NULL) dll_alloc=new BTree<DWORD_PTR>;
      dll_alloc->Add((DWORD_PTR)res);   //mame databazi, vloz ukazatel do databaze
     //TRACE1("ALLOC: %X\n",res);
      for (int i=0;i<dll_alloc_imdarray_cnt;i++)
      {
        dll_alloc->Add(dll_alloc_imdarray[i]);
      //  TRACE1("ALLOC: %X\n",dll_alloc_imdarray[i]);
      }
      dll_alloc_imdarray_cnt=0;
      dll_alloc_inalloc=false;
    }
    return res;
  }
  else*/
    return _malloc_funct(sz,op);       //pouzij novy malloc;

}

static inline void DllFree(void *p, DllDebugMallocFreeOp op)
{
//  if (dll_alloc_inalloc)
  {
    //TRACE1("BTREE-FREE: %X\n",p);
    _free_funct(p,op);    
  }
/*  else
  {
    DWORD_PTR *ptr=dll_alloc->Find((DWORD_PTR)p);   //najdi ukazatel v databazi
    if (ptr==NULL && _free_funct )  //nenalezen?
      _free_funct(p);                       //alokovan novym mallocem... uvolni adekvatnim free
    else
    {
      dll_alloc_inalloc=true;
      free(p);                            //nalezen? pouzij puvodni malloc      
//      TRACE1("FREE: %X\n",p);
      dll_alloc->Remove(*ptr);             //odstran zaznam z databaze
      dll_alloc_inalloc=false;
    }
  }*/
}

void *operator new(size_t sz)
{
  return DllAlloc(sz,DDNewDelete);
}

void operator delete(void *p)
{
 DllFree(p,DDNewDelete);
}

void *operator new[](size_t sz)
{
  return DllAlloc(sz,DDNewDeleteArr);
}

void operator delete[](void *p)
{
 DllFree(p,DDNewDeleteArr);
}

void DoneDebuggerMemoryManagment()
{
}

