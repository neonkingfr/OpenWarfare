//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UIDebugger.rc
//
#define IDC_CONTEXTTAB                  2000
#define IDR_MENU1                       2001
#define IDR_DEBUGMENU                   2001
#define IDC_SOURCEVIEW                  2002
#define IDS_WATCHTITLE                  2002
#define IDS_AUTOWATCHTITLE              2003
#define IDC_WATCH                       2004
#define IDS_OUTPUTTITLE                 2004
#define IDS_LEVELTITLE                  2005
#define IDS_DEBUGGERHALTED              2006
#define IDC_EDIT1                       2006
#define IDS_DEBUGSCRIPTERROR            2007
#define IDC_SCRIPT                      2007
#define IDS_LEVELNOTAVAILABLE           2008
#define IDC_RESULT                      2008
#define IDS_WATCHTYPE                   2009
#define IDC_VARIABLE                    2009
#define IDC_AUTOWATCH                   2010
#define IDS_VARUNAVAILABLE              2010
#define IDB_WATCHICO                    2011
#define IDC_HSHIFTBAR                   2011
#define IDS_FORCEWARN                   2011
#define IDD_SMALLEVALUATE               2012
#define IDC_VSHIFTBAR                   2012
#define IDC_KILLWARN                    2012
#define IDS_ERRORINEXPRESSION           2013
#define IDC_STOP                        2013
#define IDD_EVALUATE                    2014
#define IDS_CONTEXTGETFAILED            2014
#define IDS_UNABLETOGETSOURCEPOSITION   2015
#define IDS_TRAYTEXT                    2016
#define IDC_TERMINATE                   2016
#define IDR_TRAYICON                    2018
#define IDB_WATCHSTATE                  2020
#define IDS_WATCHBREAKPOINT             2021
#define IDS_VARIABLECHANGEMSG           2022
#define IDD_BREAKINPROGRESS             2022
#define IDR_ACCELERATOR1                2023
#define IDD_DEBUGGER                    11000
#define ID_DEBUGGING_BREAK              32771
#define ID_DEBUGGING_RUN                32772
#define ID_DEBUGGING_RUNTO              32773
#define ID_DEBUGGING_STEPOVER           32774
#define ID_DEBUGGING_STEPOVER32775      32775
#define ID_DEBUGGING_STEPOUT            32776
#define ID_DEBUGGING_RUNNON             32777
#define ID_DEBUGGING_TERMINATE          32778
#define ID_WATCH_ADDWATCH               32779
#define ID_WATCH_DELETEWATCH            32780
#define ID_WATCH_WATCHBREAKPOINTS       32782
#define ID_Menu                         32783
#define ID_WATCH_SETVARIABLEVALUE       32784
#define ID_WINDOW_ALWAYSONTOP           32785
#define ID_DEBUGGER_STEPINTO            32786
#define ID_DEBUGGER_STEPOVER            32787
#define ID_Menu32791                    32791
#define ID_DEBUGGER_SETSOURCEPOSITION   32792
#define ID_WATCH_EDITWATCH              32804
#define ID_WATCH_DETECTUNDECLARED       32806

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2024
#define _APS_NEXT_COMMAND_VALUE         32807
#define _APS_NEXT_CONTROL_VALUE         2016
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
