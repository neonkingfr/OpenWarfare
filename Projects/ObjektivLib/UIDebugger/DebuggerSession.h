#pragma once


#include "DlgDebugger.h"


// CDebuggerSession

class CDebuggerSession : public CWinThread, public ScriptDebuggerBase
{
    HWND _owner;
    CString _title;


    DlgDebugger _dbgwin;
    friend class DlgDebugger;


public:
	CDebuggerSession();          
	virtual ~CDebuggerSession();

    HANDLE runevent;


public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

    void QuitDebugger();

protected:
	DECLARE_MESSAGE_MAP()


/*    const char *dbgGetCurrentPositon(int level) {return IScriptDebugger::GetCurrentPositon(level);}
    const char *dbgGetCurrentExpression(int level) {return IScriptDebugger::GetCurrentExpression(level);}
    GameValue *GetCurrentValueStack(int level) {return IScriptDebugger::GetCurrentValueStack(level);}
    GameVarSpace *GetLocalVariables(int level) {return IScriptDebugger::GetLocalVariables(level);}
    GameVarSpace *GetLocalVariables() {return IScriptDebugger::GetLocalVariables();}*/


public: //debugger events
  virtual void OnTrace();
  virtual void OnVariableBreakpoint(const char *name, const GameValue &val);
  virtual bool OnConsoleOutput(GameState &script,const char *text);
  virtual bool OnError(GameState &script,const char *error);
  virtual void TestBreak() 
  {
    if (GetCurrentThreadId()==m_nThreadID)
    { 
      AfxPumpMessage();
    }
    else
    {
       if (_dbgwin.GetTestBreakpoints()) OnTrace();
     else
       if (_dbgwin.ShouldBreak()) SetTraceMode(traceBreakNext);
    }
      
  }
  void SetOwnerAndTitle(HWND owner, const char *title)
  {
    _owner=owner;
    _title=title;
  }


};


