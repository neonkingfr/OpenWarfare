
namespace ObjektivLib {

class NamedProperty
  {
  private:
    char _name[64];
    char _value[64];
    
  public:
    NamedProperty( const char *name, const char *value="" )
      {
      strncpy(_name,name,sizeof(_name));
      strncpy(_value,value,sizeof(_value));
      }
    
    const char *Name() const 
      {return _name;}
    const char *Value() const 
      {return _value;}
    void SetName( const char *name ) 
      {strncpy(_name,name,sizeof(_name));}
    void SetValue( const char *value ) 
      {strncpy(_value,value,sizeof(_value));}
  };

}