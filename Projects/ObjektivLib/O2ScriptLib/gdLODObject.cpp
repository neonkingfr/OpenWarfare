#include "..\common.hpp"
#include "..\LODObject.h"
#include "..\GlobalFunctions.h"
#include ".\gdlodobject.h"
#include <malloc.h>
#include "globalState.h"
#include <el/ParamFile/paramFile.hpp>
#include <el/BTree/Btree.h>
#include "..\MergeTexturesTool.h"

#define Category "O2ScriptLib::LODObjects"

static GameStringType ScriptProgressFunct;
static GameVarSpace progressSpace(false);
static GameState *curGameState=NULL;


GdLODObject::GdLODObject(void):_objname(""),_lodobj(new LODObjectRef)
{
}

GdLODObject::GdLODObject(LODObjectRef *object):_lodobj(object),_objname("")
{

}
GdLODObject::GdLODObject(LODObjectRef *object, const Pathname &name):_lodobj(object),_objname(name)
{

}

GdLODObject::GdLODObject(const GdLODObject &other):_lodobj(other._lodobj),_objname(other._objname)
{
  
}

GameStringType GdLODObject::GetText() const
{
  const char *name=_objname.IsNull()?"<unnamed>":_objname.GetFilename();
  char *text=(char *)alloca(strlen(name)+100);  
  sprintf(text,"\"%s\" c:%d r:%g n:%d",name,_lodobj->_ref->ActiveLevel(),_lodobj->_ref->Resolution(_lodobj->_ref->ActiveLevel()),_lodobj->_ref->NLevels());
  return GameStringType(text);
}

bool GdLODObject::IsEqualTo(const GameData *data) const
{
  const GdLODObject *other=dynamic_cast<const GdLODObject *>(data);
  if (other==NULL) return false;
  return other->_lodobj==_lodobj;
}

DEFINE_FAST_ALLOCATOR(GdLODObject)

static GameValue loadP3D (const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  GdLODObject *lodobj=static_cast<GdLODObject *>(oper1.GetData());
  Pathname pname=(GameStringType)oper2;
  if (lodobj->_lodobj->_ref->Load(pname,NULL,NULL))
  {
    return GameValue(false);
  }
  else
  {
  lodobj->_objname=pname;
  return GameValue(true); 
  }
}

static GameValue setActive(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  int index=toInt(oper2);
  if (index<0 || index>=lodobj->_ref->NLevels()) return GameValue(false);
  else 
  { 
    lodobj->_ref->SelectLevel(index);
    return GameValue(true);
  }
}

static GameValue setActiveSpecial(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  float index=oper2;
  int fnd=lodobj->_ref->FindLevelExact(index);
  if (fnd==-1) return GameValue(false);
  lodobj->_ref->SelectLevel(fnd);
  return GameValue(true);
}

static GameValue setResolution(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  float resol=oper2;
  int lev=lodobj->_ref->ActiveLevel();
  lodobj->_ref->SetResolution(lev,resol);
  return GameValue(true);
}

static GameValue getObject(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  int index=toInt(oper2);
  if (index<0 || index>=lodobj->_ref->NLevels()) return GameValue();
  return GameValue(new GdObjectData(lodobj->_ref->Level(index),lodobj));
  
}

class Functor_ResetVariablesPointedToObject
{
    ObjektivLib::ObjectData *_obj;
public:
  Functor_ResetVariablesPointedToObject(ObjektivLib::ObjectData *pobj):_obj(pobj) {}
  bool operator()(GameVariable &var, VarBankType *bank)
  {
    GdObjectData *odata=dynamic_cast<GdObjectData *>(var._value.GetData());
    if (odata!=NULL && odata->_object==_obj)
      var._value=GameValue();
    return false;
  }
};

static GameValue deleteLevel(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  int index=toInt(oper2);
  ObjektivLib::ObjectData *levToDel=lodobj->_ref->Level(index);
  GameVarSpace *space=const_cast<GameVarSpace *>(gs->GetContext());
  while (space)
  {
      space->_vars.ForEachF(Functor_ResetVariablesPointedToObject(levToDel));
      space=const_cast<GameVarSpace *>(space->_parent);
  }
  if (index<0 || index>=lodobj->_ref->NLevels()) return GameValue(false);
  lodobj->_ref->DeleteLevel(index);
  return GameValue(true);
}

static GameValue AsFunction(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  Ref<GdLODObject>lodobj=static_cast<GdLODObject *>(oper1.GetData());
  Ref<GdLODObject>clone=new GdLODObject(*lodobj);
  clone->_objname=Pathname((GameStringType)oper2,lodobj->_objname);
  if (clone->_objname.IsNull()) clone->_objname=(GameStringType)oper2;
  return GameValue(clone);  
}

static GameValue findLevel(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  float resol=oper2;
  int p=lodobj->_ref->FindLevel(resol);
  if (p==-1) return GameValue();
  else return GameValue((float)p);
}

static GameValue findLevelSpecial(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  float resol=oper2;
  int p=lodobj->_ref->FindLevelExact(resol);
  if (p==-1) return GameValue();
  else return GameValue((float)p);
}


static GameValue getResolution( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
    Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
    int index=toInt(oper2);
    if (index<0 || index>=lodobj->_ref->NLevels())
    {
      state->SetError(EvalForeignError,"index is out of range");
      return GameValue();
    }
    return GameValue(lodobj->_ref->Resolution(index));
}

static GameValue CopyFunct( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
    Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
    Ref<LODObjectRef>other=static_cast<GdLODObject *>(oper2.GetData())->GetObject();
    *(lodobj->_ref)=*(other->_ref);
    return oper1;
}

static GameValue Merge( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  Ref<LODObjectRef>todobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  const GameArrayType &arr=oper2;
  Ref<LODObjectRef>from=static_cast<GdLODObject *>(arr[0].GetData())->GetObject();
  (todobj->_ref)->Merge(*(from->_ref), (GameBoolType) arr[1], (GameStringType) arr[2], (GameBoolType) arr[3]);
  return oper1;
}

static GameValue mergeTextures(const GameState *gs, GameValuePar oper1, GameValuePar oper2 )
{
  Ref<LODObjectRef>todobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  RString ptmName=oper2;
  ObjektivLib::MergeTexturesTool merge;
  if (merge.LoadPtm(ptmName)!=LSOK) return GameValue();
  return merge.MergeTextures(todobj->_ref);
}


static GameValue mergeMaterialsByTextures( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  Ref<LODObjectRef>todobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  const GameArrayType &arr=oper2;
  if (arr.Size()==2 && arr[0].GetType()==GameString && arr[1].GetType()==GameString)
  { 
    RString ptm=arr[0];
    RString base=arr[1];
    ObjektivLib::MergeTexturesTool merge;
    if (merge.LoadPtm(ptm)!=LSOK) return GameValue();
    return merge.MergeMaterialsUsingTextures(todobj->_ref,base);
  }    
  return GameValue();
}

#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameBool, "loadP3D",function, loadP3D,EvalType_LODObject, GameString, "LOObject","String", "Loads file into object", "_object=newLODObject;_result=_object loadP3D \"example.p3d\"", "true, if load has been successful", "", "", Category) \
  XX(GameBool, "setActive", function,setActive,EvalType_LODObject, GameScalar, "LODObject","Scalar", "Selects active LOD from LOD array. Scalar is zero based integer number", "_success=_object setActive 5.0", "true, operation was successfull", "", "", Category) \
  XX(GameBool, "setActiveSpecial", function,setActiveSpecial,EvalType_LODObject, GameScalar, "LODObject","Scalar", "Selects active LOD from LOD array. Scalar one of constants declared in LODObject.inc", "_success=_object setActiveSpecial LOD_VIEW_COMMANDER", "true, operation was successfull", "", "", Category) \
  XX(GameBool, "setResolution", function,setResolution,EvalType_LODObject, GameScalar, "LODObject","Scalar", "Sets resolution of active level.You can use special constants declared in LODObject.inc", "_success=_object setResolution LOD_VIEW_COMMANDER", "always true, operation was successfull", "", "", Category) \
  XX(EvalType_ObjectData, "getObject", function,getObject,EvalType_LODObject, GameScalar, "LODObject","Scalar", "Sets resolution of active level.You can use special constants declared in LODObject.inc", "_success=_object setResolution LOD_VIEW_COMMANDER", "always true, operation was successfull", "", "", Category) \
  XX(GameBool, "deleteLevel",function, deleteLevel, EvalType_LODObject, GameScalar,"LODObject","Scalar", "Deletes specified level. After it, levels are reindexed", "while \"_p3d DeleteLevel 0\" {};", "true", "", "", Category) \
  XX(GameScalar, "findLevel",function, findLevel, EvalType_LODObject, GameScalar,"LODObject","Scalar", "Finds level by resolution. If resolution doesn't exists, finds nearest", "", "", "", "", Category) \
  XX(GameScalar, "getResolution",function, getResolution, EvalType_LODObject, GameScalar,"LODObject","index", "Gets resolution of object by index", "", "", "", "", Category) \
  XX(GameScalar, "findLevelSpecial",function, findLevelSpecial, EvalType_LODObject, GameScalar,"LODObject","Scalar", "Finds special level. use LOD_ constants to specify special level", "", "", "", "", Category) \
  XX(EvalType_LODObject,":=",nula, CopyFunct, EvalType_LODObject, EvalType_LODObject,"LODObject","LODObject", "Copies content of second object to first object", "this:=_object", "", "", "", Category) \
  XX(EvalType_LODObject,"as",functionFirst, AsFunction, EvalType_LODObject, GameString,"LODObject","String", "Returns clone of object but with another name", "_copy=copyLODObject _object as \"test.p3d\";", "LODObject", "", "", Category) \
  XX(EvalType_LODObject,"merge",function, Merge, EvalType_LODObject, GameArray,"LODObject","LODObject", "Copies content of second object to first object", "", "", "", "", Category) \
  XX(GameBool,"mergeTextures",function, mergeTextures, EvalType_LODObject, GameString,"LODObject","_ptmName", "Merges textures on Model using PTM file", "p3d mergeTextures \"p:\\merged.ptm\"", "true, if there at least one texture that has been changed. otherwise false. Returns nil, if an error occurred", "", "", Category) \
  XX(GameBool,"mergeMaterialsByTextures",function, mergeMaterialsByTextures, EvalType_LODObject, GameArray,"LODObject","[_ptmName,_basePath]", "Merges materials on Model using PTM file for textures. It assumes, that name of NO maps or NS map has the same as CO/CA map (suffix is excluded). _basePath specifies base path for converting relative path to absolute", "p3d mergeTexturesByMaterials [\"p:\\merged.ptm\",\"P:\\\"]", "true, if there at least one material that has been changed. otherwise false. Returns nil, if an error occurred", "", "", Category) \
  

static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

static GameValue copyLODObject(const GameState *gs, GameValuePar oper1)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  GdLODObject *data=new GdLODObject(new LODObjectRef(*lodobj));
  return GameValue(data);
}

static GameValue activeLevel(const GameState *gs, GameValuePar oper1)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();  
  return GameScalarType(lodobj->_ref->ActiveLevel());
}

static GameValue activeResolution(const GameState *gs, GameValuePar oper1)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();  
  return GameScalarType(lodobj->_ref->Resolution(lodobj->_ref->ActiveLevel()));
}


static void ScriptProgress(int cur, int max)
  {
    progressSpace.VarSet("_x",GameValue((float)cur/(float)max),true);
    curGameState->BeginContext(&progressSpace);
    curGameState->EvaluateMultiple(ScriptProgressFunct);
    curGameState->EndContext();
  }

static GameValue setProgressProc(const GameState *gs, GameValuePar oper1)
{
  return GameValue();
}

static GameValue getObjectsArray(const GameState *gs, GameValuePar oper1)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();  
  GameDataArray *arrayData=new GameDataArray;
  GameArrayType &array=arrayData->GetArray();
  for (int i=0;i<lodobj->_ref->NLevels();i++)
  {
    array.Append()=GameValue(new GdObjectData(lodobj->_ref->Level(i),lodobj));
  }
return GameValue(arrayData);
}

static GameValue getResolutionArray(const GameState *gs, GameValuePar oper1)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();  
  GameDataArray *arrayData=new GameDataArray;
  GameArrayType &array=arrayData->GetArray();
  for (int i=0;i<lodobj->_ref->NLevels();i++)
  {
    array.Append()=GameValue(lodobj->_ref->Resolution(i));
  }
  return GameValue(arrayData);
}

static GameValue getActiveObject(const GameState *gs, GameValuePar oper1)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();  
  return GameValue(new GdObjectData(lodobj->_ref->Active(),lodobj));
}

static GameValue addLevel(const GameState *gs, GameValuePar oper1)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  float res;
  if (lodobj->_ref->NLevels()==0) res=0;else res=lodobj->_ref->Resolution(lodobj->_ref->NLevels()-1);
  res+=res/100.0f;
  int index=lodobj->_ref->AddLevel(ObjektivLib::ObjectData(),res);
  int add=lodobj->_ref->FindLevelExact(res);
  lodobj->_ref->SelectLevel(add);
  return GameValue(new GdObjectData(lodobj->_ref->Active(),lodobj));
}

static GameValue saveFunction(const GameState *gs, GameValuePar oper1)
{
  Ref<GdLODObject>lodobj=static_cast<GdLODObject *>(oper1.GetData());
  if (lodobj->_objname.IsNull()) return GameValue(false);
  for (int i=0;i<lodobj->_lodobj->_ref->NLevels();i++) lodobj->_lodobj->_ref->Level(i)->RecalcNormals(false);
  int res=lodobj->_lodobj->_ref->Save(lodobj->_objname,OBJDATA_LATESTVERSION,false,NULL,NULL);
  return GameValue(res==0);
}

static GameValue countLevels(const GameState *gs, GameValuePar oper1)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
  return GameValue((float)(lodobj->_ref->NLevels()));
}

static GameValue nameOfLodObj(const GameState *gs, GameValuePar oper1)
{
  Ref<GdLODObject>lodobj=static_cast<GdLODObject *>(oper1.GetData());
  return RString(lodobj->_objname);
}


#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(EvalType_LODObject, "copyLODObject", copyLODObject, EvalType_LODObject, "LODObject", "Creates new LODObject as copy of referenced object", "_copy=copyLODObject _object", "copy of object created", "", "", Category) \
  XX(EvalType_LODObject, "+", copyLODObject, EvalType_LODObject, "LODObject", "Creates new LODObject as copy of referenced object", "_copy=copyLODObject _object", "copy of object created", "", "", Category) \
  XX(GameScalar, "activeLevel", activeLevel, EvalType_LODObject, "object", "Gets index  of active level", "_level=activeLevel _object", "1.0", "", "", Category) \
  XX(GameScalar, "activeResolution", activeResolution, EvalType_LODObject, "LODObject", "Gets resolution (or special level id)  of active level", "_level=activeResolution _object; if (_level==LOD_VIEW_CARGO_GEOMETRY) echo \"cargo\"", "", "", "", Category) \
  XX(GameString, "onProgressEvent", setProgressProc, GameString, "Code", "Sets function to display progress status", "onProgressEvent {echo (\"progress\"+str(_x*100)+\"%\");}", "Nothing", "", "", Category) \
  XX(GameArray, "getObjects", getObjectsArray, EvalType_LODObject, "LODObject", "Returns an array of objects in LODObject", "{echo (NFaces _x)} forEach getObjects _p3d", "Array", "", "", Category) \
  XX(GameArray, "getResolutions", getResolutionArray, EvalType_LODObject, "LODObject", "Returns an array of objects in LODObject", "{echo (NFaces _x)} forEach getObjects _p3d", "Array", "", "", Category) \
  XX(EvalType_ObjectData, "getActiveObject", getActiveObject, EvalType_LODObject, "LODObject", "Returns active object", "_object=getActiveObject _p3d;", "ObjectData", "", "", Category) \
  XX(EvalType_ObjectData, "addLevel", addLevel, EvalType_LODObject, "LODObject", "Creates new level and returns it. Added level becomes active", "_object=AddLevel _p3d;", "ObjectData", "", "", Category) \
  XX(GameScalar, "count", countLevels, EvalType_LODObject, "LODObject", "Returns number of levels in object", "", "", "", "", Category) \
  XX(GameBool, "save", saveFunction, EvalType_LODObject, "LODObject", "Saves object into the disk. It used assigned file name. To specify new filename, use as operator", "save (_object as \"test.p3d\");", "true means no error", "", "", Category) \
  XX(GameString, "nameOf", nameOfLodObj, EvalType_LODObject, "obj", "Return full name of object", "nameOf _p3d", "", "", "", Category) \

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};

static GameValue newLODObject(const GameState *gs)
{
  GdLODObject *data=new GdLODObject(new LODObjectRef);
  return GameValue(data);
}

#define NULARS_DEFAULT(XX, Category) \
  XX(EvalType_LODObject, "newLODObject", newLODObject ,"Creates new empty object. Best usage is assign result to the variable. New object is shared between variables.", "_object = newLODObject", "", "", "", Category) \

static GameNular NularyFuncts[]=
{
  NULARS_DEFAULT(REGISTER_NULAR, Category)
};

static GameData *CreateLodObject(ParamArchive *ar) {return new GdLODObject;}

TYPES_LOD_OBJECT(DEFINE_TYPE, "Objektiv")


#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  NULARS_DEFAULT(COMREF_NULAR, Category)
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
  TYPES_DEFAULT(COMREF_TYPE, Category)
/*  TYPES_DEFAULT_COMB(COMREF_TYPE, "Default")*/
};
#endif


void GdLODObject::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState; // helper to make macro works
  TYPES_LOD_OBJECT(REGISTER_TYPE, "Objektiv")

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
  gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
  gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}

