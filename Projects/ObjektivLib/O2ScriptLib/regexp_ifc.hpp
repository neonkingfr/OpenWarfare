#ifndef __stream_char_interface__
#define __stream_char_interface__

/// this is interface for stream_char in regexp.hpp

class stream_char_interface
{
  public:
    virtual char operator[](int pos)=0;
};

#endif