#pragma once
#include <el\evaluator\express.hpp>

#ifdef _DEBUG
#define ObjektivPluginMain ObjektivPluginMainD
#define ObjektivPluginMainStr "ObjektivPluginMainD"
#else
#define ObjektivPluginMainStr "ObjektivPluginMain"
#endif

// Interface for FBX.dll import/export
class GdFBXplugin
{
public:
  static void RegisterToGameState(GameState *gState);
};