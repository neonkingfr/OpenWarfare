#include "..\common.hpp"
#include "..\LODObject.h"
#include "gdLODObject.h"
#include "gdbitxtimport.h"
#include "..\BiTXTImportDefault.h"

using namespace ObjektivLib;



static RString lastImportError;
#define Category "O2Scripts::ImportBiTXT"

static GameValue importBiTXT(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
    using namespace ObjektivLib;
    Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
    CBiTXTImportDefault import;
    import.Import((RString)oper2,lodobj->_ref);
    if (import.IsError())   
      import.FormatError(lastImportError.CreateBuffer(1024),1024);    
    else
        lastImportError=RString();
    return !import.IsError();
}

static GameValue getLastError(const GameState *gs)
{
  return lastImportError;
}

static GameValue exportBiTXT(const GameState *gs, GameValuePar oper1, GameValuePar oper2, CBiTXTExport::ExportMode mode)
{
   Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
   CBiTXTExport texport;
   return texport.Export((RString)oper2,lodobj->_ref,mode);
}

static GameValue exportBiTXTSG(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  return exportBiTXT(gs,oper1,oper2,CBiTXTExport::ExpSG);
}

static GameValue exportBiTXTN(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  return exportBiTXT(gs,oper1,oper2,CBiTXTExport::ExpNormals);
}

static GameValue exportBiTXTEL(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  return exportBiTXT(gs,oper1,oper2,CBiTXTExport::ExpEdges);
}

#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameBool,"importBiTXT", function, importBiTXT, EvalType_LODObject, GameString,"p3d","name", "Loads (imports) BiTXT file into LODObject. It doesn't change internal name of object.", "p3d=newLODObject;result=p3d importBiTXT \"shape.txt\";", "true, if file has been successfully parsed and imported. false, if there was an error. Use getLastBiTXTImportError to get reason of fail.", "", "", Category) \
  XX(GameBool,"exportBiTXT_SG", function, exportBiTXTSG, EvalType_LODObject, GameString,"p3d","name", "Saves (exports) LODObject to BiTXT file. Sharp edges are exported as smoothgroups. It doesn't change internal name of object.", "p3d exportBiTXT<SG> \"shape.txt\";", "true, if file has been successfully exported.", "", "", Category) \
  XX(GameBool,"exportBiTXT_Normals", function, exportBiTXTN, EvalType_LODObject, GameString,"p3d","name", "Saves (exports) LODObject to BiTXT file. Each face also contain normal vectors. It doesn't change internal name of object.", "p3d exportBiTXT<SG> \"shape.txt\";", "true, if file has been successfully exported.", "", "", Category) \
  XX(GameBool,"exportBiTXT_Edges", function, exportBiTXTEL, EvalType_LODObject, GameString,"p3d","name", "Saves (exports) LODObject to BiTXT file. Sharp edges are exported as list of edges. It doesn't change internal name of object.", "p3d exportBiTXT<SG> \"shape.txt\";", "true, if file has been successfully exported.", "", "", Category) \

#define NULAR_DEFAULT(XX, Category) \
  XX(GameString, "getLastBiTXTImportError", getLastError, "Returns error string of last error of importBiTXT.","","", "", "", Category) \


static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

static GameNular NularyFuncts[]=
{
  NULAR_DEFAULT(REGISTER_NULAR, Category)
};



#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
  NULAR_DEFAULT(COMREF_NULAR, Category)
};
#endif

void GdBiTXTImport::RegisterToGameState(GameState *gState)
{
  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));

#endif
}
