#ifndef _TRADITIONAL_NAMED_OBJECT_H_
#define _TRADITIONAL_NAMED_OBJECT_H_

#pragma once

#include <ES/Strings/rString.hpp>

namespace ObjektivLib {




template<class T>
class NamedObject: public T
  {
  private:
    RString _name;
    
  public:
    NamedObject( ObjectData *object, const char *name )
      :T(object),_name(name)
        {
        
        }
    NamedObject( const T &sel, const char *name )
      :T(sel),_name(name)
        {
        
        }
    const char *Name() const 
      {return _name;}
    void SetName( const char *name ) 
      {_name=name;}
  };


}
#endif