#ifndef __OBJ_TOOL_CLIPBOARD_CLASS_HEADER_
#define __OBJ_TOOL_CLIPBOARD_CLASS_HEADER_

#pragma once


#include "ObjectData.h"

namespace ObjektivLib {


class ObjToolClipboard: public ObjectData
  {  
  public:
    bool LoadClipboard( unsigned int format );
    bool SaveClipboard( unsigned int format );
    
    void ExtractSelection(const Selection *sel=NULL);
    
    bool SelectionCopy( unsigned int format );
    bool SelectionCut( unsigned int format );
    bool SelectionPaste( unsigned int format );
    
  };

}
#endif