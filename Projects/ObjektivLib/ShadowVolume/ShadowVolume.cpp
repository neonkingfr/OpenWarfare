// ShadowVolume.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MassProcess.h"

class ShadowTestFile: public MassProcess
  {
  public:
    virtual bool ProcessFile(const char *filename);
  };

bool ShadowTestFile::ProcessFile(const char *pathname)
{
  LODObject lod;
  if (lod.Load(pathname,NULL,NULL)!=0)
    {
    fprintf(stderr,"Error opening: %s\n", pathname);
    return true;
    }
  bool has=false;
  for (int i=0;i<lod.NLevels();i++)
    if (lod.Resolution(i)>=LOD_SHADOW_MIN && lod.Resolution(i)<=LOD_SHADOW_MAX)
    {
      printf("ShadowVolume Level: %g (%s)\n",lod.Resolution(i)-LOD_SHADOW_MIN,pathname);
      if (lod.Resolution(i)!=LOD_SHADOW_MIN) printf("Warning: Strange Level: %g(%s)\n",lod.Resolution(i)-LOD_SHADOW_MIN,pathname);
      has=true;
    }
    if (has==false)
      printf("No Shadow Volume in %s\n", pathname);
return true;
}

int main(int argc, char* argv[])
{
bool recursive=false;
const char *folder;
if (argc<2)
  {
  puts("Parameters: [/s] folder\n\n/s          process recursive\nfolder      folder to process, ex: X:\\data\n");
  return -1;
  }
if (stricmp(argv[1], "/s")==0)
  {
  if (argc<3)
    {
    puts("Missing parameter");
    return -1;
    }
  recursive=true;
  folder=argv[2];
  }
else
  {
  folder=argv[1];
  }
ShadowTestFile mass;
mass.ProcessFolders(folder,"*.p3d",recursive);
return 0;
}