
#include "common.hpp"
#include <malloc.h>
#include "LODObject.h"
#include ".\objtooljoinproxy.h"
#include ".\objtoolproxy.h"
#include ".\ObjToolCheck.h"
#include ".\ObjToolTopology.h"
#include "Edges.h"
#undef RStringB
#include <El\ParamFile\paramFile.hpp>


namespace ObjektivLib {


void ObjToolJoinProxy::CreateModelFromProxies(LODObject& out, 
                                              NamedSelection ** list, 
                                              int size, 
                                              Matrix4& resTrans, 
                                              const char * basePath,
                                              bool shiftResult, 
                                              bool makeSelections) const
{ 
  int firstProxyFace = -1;
  Matrix4 firstProxyTrans;
  AutoArray<CMFMData> models;

  for(int i = 0; i < size; i++)
  {  
    const NamedSelection * proxySel = list[i];

    int proxyFace = 0;
    for (;proxyFace<NFaces();proxyFace++) if (proxySel->FaceSelected(proxyFace)) break;
    if (proxyFace==NFaces()) continue;

    CMFMData& model = models.Append();
    model._model = Pathname(proxySel->Name()+strlen("proxy:"),basePath);    
    model._model.SetExtension(".p3d");
    if (makeSelections)
      model._resSelection = model._model.GetTitle();

    //LODObject lodProxy;
    //if (lodProxy.Load(proxyPath,NULL,NULL)!=0) continue; 

    FaceT proxy(this,proxyFace);
    ObjToolProxy::GetProxyTransform(proxy,model._trans);

    if (firstProxyFace==-1)
    {
      firstProxyFace=proxyFace;
      if (!shiftResult)
      {
        resTrans = M4Identity;
        firstProxyTrans = M4Identity;
      }
      else
      {
        firstProxyTrans.SetInvertGeneral(model._trans);
        resTrans = model._trans;
        model._trans.SetIdentity();     
      }
    }
    else
    {
      model._trans=firstProxyTrans*model._trans;
    }

    /*for (int i=0;i<lodProxy.NLevels();i++)
    {
      ObjectData *objProxy=lodProxy.Level(i);
      int targetLevel=out.FindLevelExact(lodProxy.Resolution(i));
      if (targetLevel==-1) targetLevel=out.AddLevel(ObjectData(),lodProxy.Resolution(i));
      ObjectData *objTarget=out.Level(targetLevel);

      for (int j=0;j<objProxy->NPoints();j++)
      {
        PosT &pos=objProxy->Point(j);
        pos.SetPoint(proxyTrn.FastTransform(pos));
      }

      objTarget->Merge(*objProxy,false);
    }*/

    /// add proxy into break lod
    int targetLevel=out.FindLevelExact(LOD_SUB_PARTS);
    if (targetLevel==-1) targetLevel=out.AddLevel(ObjectData(), LOD_SUB_PARTS); // Lod for proxies
    ObjectData *objTarget=out.Level(targetLevel);
    objTarget->GetTool<ObjToolProxy>().CreateProxy(proxySel->Name()+strlen("proxy:"), model._trans);    
  } 

  CreateModelFromModelsFillLODs(out, models);

  // remove LOD 0 if empty
  int targetLevel=out.FindLevelExact(0);
  if (targetLevel==-1) 
    return;

  ObjectData *objTarget=out.Level(targetLevel);
  if (objTarget->NFaces() == 0 && objTarget->NPoints() == 0)
    out.DeleteLevel(targetLevel);  
}

int ObjToolJoinProxy::EnumProxiesFromSelection(const Selection * sel, NamedSelection ** list) const
{
  int nProxies = GetTool<ObjToolProxy>().EnumProxies(NULL);
  NamedSelection ** allProxies = new NamedSelection*[nProxies];
  GetTool<ObjToolProxy>().EnumProxies(allProxies);

  int foundProxies = 0;
  for(int i = 0; i < NFaces(); i++)
  {
    if (sel->FaceSelected(i))
    {
      // exist proxy with such face?
      for(int j = 0; j < nProxies; j++)
      {
        if (allProxies[j]->FaceSelected(i))
        {
          if (list)          
            list[foundProxies] = allProxies[j];
          foundProxies++;
          break;
        }        
      }      
    }
  }

  delete [] allProxies;

  return foundProxies;
}

typedef AutoArray<AutoArray<RStringB> > StringArray2D;

class Divider
{
  class Node : public RefCount
  {
  protected:
    RStringB _name;
    RStringB _type; // no represented by source dir... 
    int _group;
    RefArray<Node> _neighbours;
    int _activeNeighbours;

    Matrix4 _trans;
  public:

    Node();
    virtual ~Node() {}; 

    void SetGroup(int group);
    bool IsActive() const {return _group < 0;};    
    void AddNeighbour(Ref<Node> node);

    friend class Divider;
  };

  RefArray<Node> _nodes;

  Ref<Node> FindGroupHeart();
  int FindGroupAround(Ref<Node> node, int num, int groupID); 
  bool ReleaseGroup(int groupID);
  RStringB GetNodeType() const;

  void CollectProxies( ObjectData * proxyLod);

public:
  Divider() {};
  ~Divider(void) {};

  bool operator() (StringArray2D& mergeList, ObjectData * proxyLod, int minMembers, int maxMembers );
};

int ObjToolJoinProxy::CreateProxiesGroup(const char * groupName,int minMembers, int maxMembers)
{
  StringArray2D mergeList;
  Divider div;
  div(mergeList, this, minMembers, maxMembers);

  // create selections
  char * selName = new char[strlen(groupName) + 4];
  for(int i = 0; i < mergeList.Size(); i++)
  {
    AutoArray<RStringB>& list =  mergeList[i];
    Selection sel(this);
    for(int j = 0; j < list.Size(); j++)
    {
      sel.FaceSelect(GetTool<ObjToolProxy>().FindProxyFace(list[j]));
    }

    sel.SelectPointsFromFaces();

    sprintf(selName, "%s%03d", groupName, i);
    SaveNamedSel(selName,&sel);   
  }

  delete selName;

  return mergeList.Size();
}

void ObjToolJoinProxy::ReplaceGroupsByProxies(AutoArray<RString>& created,const char * groupPrefix,const char * basePath)
{

  /// Get all named selections starting with start -sbp.
  int groupPrefixLength = strlen(groupPrefix);
  for(int i = 0; i < MAX_NAMED_SEL; i++)
  {
    const char * selName = NamedSel(i);
    if (selName && strncmp(selName, groupPrefix, groupPrefixLength) == 0)
    {
      // interesting selection ---> joint it...
      NamedSelection * nameSel = GetNamedSel(i);
      int nProxies = GetTool<ObjToolJoinProxy>().EnumProxiesFromSelection(nameSel, NULL);
      if (nProxies <= 0)
        continue;

      NamedSelection ** list = new NamedSelection*[nProxies];
      GetTool<ObjToolJoinProxy>().EnumProxiesFromSelection(nameSel,list);

      Matrix4 proxyTrans;
      Pathname proxyPath(selName + groupPrefixLength,basePath);
      proxyPath.SetExtension(".p3d");
      proxyPath.CreateFolder();
      LODObject proxyModel;
      GetTool<ObjToolJoinProxy>().CreateModelFromProxies(proxyModel,list, nProxies, proxyTrans, basePath);
      proxyModel.Save(proxyPath,OBJDATA_LATESTVERSION,false, NULL, NULL);

      created.Add(proxyPath.GetFullPath());

      // create proxy
      GetTool<ObjToolProxy>().CreateProxy(selName + groupPrefixLength, proxyTrans);

      // remove unused proxies
      for(int j = 0; j < nProxies; j++)
      {
        SelectionDeleteFaces(list[j]);
        GetTool<ObjToolCheck>().IsolatedPoints();
        SelectionDelete();
        DeleteNamedSel(list[j]->Name());
      }

      delete [] list; 
    }
  }
}


Divider::Node::Node()
{
  _group = -1;
  _activeNeighbours = 0;
}

void Divider::Node::AddNeighbour(Ref<Node> node)
{  
  _activeNeighbours++;
  _neighbours.Add(node);
}

void Divider::Node::SetGroup(int group)
{  
  _group = group;

  for(int i = 0; i < _neighbours.Size(); i++)
  {
    _neighbours[i]->_activeNeighbours--;
  }
}

RStringB GetType(const char *name)
{
  char * rev = new char[strlen(name) + 1];
  strcpy(rev, name);
  _strrev(rev);

  char * dirStart = strpbrk(rev,"\\/") + 1;
  dirStart = strpbrk(dirStart,"\\/") + 1;
  char * dirEnd = strpbrk(dirStart,"\\/");

  Assert(dirStart && dirEnd);
  char * type = new char[dirEnd - dirStart + 1];
  strncpy(type, dirStart, dirEnd - dirStart);
  type[dirEnd - dirStart] = 0;
  _strrev(type);

  delete rev;
  return type;
}

void Divider::CollectProxies( ObjectData * proxyLod)
{

  // collect proxies
  int nProxies = proxyLod->GetTool<ObjToolJoinProxy>().EnumProxiesFromSelection(proxyLod->GetSelection(), NULL);
  NamedSelection ** list = new NamedSelection*[nProxies];
  proxyLod->GetTool<ObjToolJoinProxy>().EnumProxiesFromSelection(proxyLod->GetSelection(), list);
  
  for(int i = 0; i < nProxies; i++)
  {
    NamedSelection* sel = list[i];

    int proxyFace = proxyLod->GetTool<ObjToolProxy>().FindProxyFace(sel->Name());
    if (proxyFace < 0)
      continue;

    FaceT proxy(proxyLod,proxyFace);
    Matrix4 trans;
    ObjToolProxy::GetProxyTransform(proxy,trans);

  
    Ref<Node> node = new Node();
    node->_name = sel->Name();
    node->_type = GetType(sel->Name());

    node->_trans = trans;

    LODObject node1Obj;
    Pathname fileName(node->_name+strlen("proxy:"),"p:\\");
    fileName.SetExtension(".p3d");   
    if (node1Obj.Load(fileName, NULL, NULL) < 0)
    {
      _nodes.Add(node);
      continue;
    }

    int node1Level = node1Obj.FindLevel(1);
    if (node1Level < 0) 
    {
      _nodes.Add(node);
      continue;
    }

    ObjectData * node1ObjData = node1Obj.Level(node1Level);

    for(int k = 0; k < node1ObjData->NPoints(); k++)
    {
      Vector3 p1 = trans * node1ObjData->Point(k);

      if (p1[1] < 0)
      {
        // probably ground joint group together
        node->_group = 0;
        break;
      }
    }

    _nodes.Add(node);
  }

  delete [] list;

  // find neighbours 
  // Extremaly primitive algorithm just for testing 
  for(int i = 0; i < _nodes.Size(); i++)
  {
    // Load object
    LODObject node1Obj;
    Pathname fileName(_nodes[i]->_name+strlen("proxy:"),"p:\\");
    fileName.SetExtension(".p3d");   
    if (node1Obj.Load(fileName, NULL, NULL) < 0)
      continue;

    int node1Level = node1Obj.FindLevel(1);
    if (node1Level < 0) continue;
    ObjectData * node1ObjData = node1Obj.Level(node1Level);

    Matrix4& trans1 = _nodes[i]->_trans;

    for(int j = i + 1; j < _nodes.Size(); j++)
    {     
      /* Obsolete system
      if (_nodes[i]->_group == 0 || _nodes[j]->_group == 0 || (_nodes[i]->_type != _nodes[j]->_type && (_nodes[i]->_type != RStringB("Exteriers") &&  
        _nodes[i]->_type != RStringB("Interiers"))))
        continue; 

      float maxSizeSquare = 9;
      if (_nodes[i]->_type == RStringB("Floors"))
        maxSizeSquare = 16;

      if (_nodes[i]->_type == RStringB("Roofs") || _nodes[i]->_type == RStringB("Timbers"))
        maxSizeSquare = 9;*/

      if (_nodes[i]->_group == 0 || _nodes[j]->_group == 0)
        continue; 

      float maxSizeSquare = 9;


      if ((trans1.Position() - _nodes[j]->_trans.Position()).SquareSize() < maxSizeSquare)
      {
        /*if (_nodes[i]->_type == RStringB("Floors") && fabs(_nodes[i]->_trans.Position()[1] - _nodes[j]->_trans.Position()[1]) > 1)
          continue;*/

        // verify that proxies are really in touch
        LODObject node2Obj;
        Pathname fileName(_nodes[j]->_name+strlen("proxy:"),"p:\\");
        fileName.SetExtension(".p3d");   
        if (node2Obj.Load(fileName, NULL, NULL) < 0)
          continue;

        int node2Level = node2Obj.FindLevel(1);
        if (node2Level < 0) continue;
        ObjectData * node2ObjData = node2Obj.Level(node2Level);

        Matrix4& trans2 = _nodes[j]->_trans;

        const float maxAllowedDistance2 = 0.25f;

        // find if there are such close points
        bool neighbour = false;
        for(int k = 0; k < node1ObjData->NPoints() && !neighbour; k++)
        {
          Vector3 p1 = trans1 * node1ObjData->Point(k);

          for(int l = 0; l < node2ObjData->NPoints() && !neighbour; l++)
          {
            Vector3 p2 = trans2 * node2ObjData->Point(l);
            if (p1.Distance2(p2) < maxAllowedDistance2)
              neighbour = true;
          }
        }
        
        if (neighbour)
        {        
          _nodes[i]->AddNeighbour(_nodes[j]);
          _nodes[j]->AddNeighbour(_nodes[i]);
        }
      }
    }
  }
}

bool Divider::operator()(StringArray2D& mergeList, ObjectData * proxyLod, int minMembers, int maxMembers)
{
  CollectProxies(proxyLod);
  // Sort nodes 
  Ref<Node> node = FindGroupHeart();

  int groupID = 1; 
  int membersNumMin = minMembers; 
  int membersNumMax = maxMembers; 
  int membersNum = membersNumMin;
  while(node.NotNull())
  {  
    int actualMembersNum = membersNum;
   /* if (node->_type == RStringB("Floors"))
      membersNum = 5;  

    if (node->_type == RStringB("Timbers"))
      membersNum = 20; */

    int membersFound = FindGroupAround(node,membersNum, groupID);

    if (membersFound < 3)
    {
      if (!ReleaseGroup(groupID))
        groupID++; 
    }
    else
      groupID++; 

    if (++membersNum == membersNumMax)
      membersNum = membersNumMin;

    node = FindGroupHeart();
  }

  for(int i = 0; i < _nodes.Size(); i++)
  {
    Ref<Node> node = _nodes[i];

    if (mergeList.Size() <= node->_group)
    {
      mergeList.Resize(node->_group + 1);
    }

    mergeList[node->_group].Add(node->_name);
  }

  return true;
}

Ref<Divider::Node> Divider::FindGroupHeart()
{
  // One with lowest active neighbours
  Ref<Divider::Node> node;

  for(int i = 0; i < _nodes.Size(); i++)
  {
    if (_nodes[i]->IsActive() && (node.IsNull() || node->_activeNeighbours > _nodes[i]->_activeNeighbours))
      node = _nodes[i];
  }

  return node;
}

int Divider::FindGroupAround(Ref<Node> node, int num, int groupID)
{
  node->SetGroup(groupID);

  int found = 1;

  RefArray<Divider::Node> borderNeighbours;
  borderNeighbours.Add(node);
  while (borderNeighbours.Size() != 0 && found < num)
  {
    RefArray<Divider::Node> newBorderNeighbours;
    for(int i = 0; i < borderNeighbours.Size() && found < num; i++)
    {
      RefArray<Divider::Node>& neigh = borderNeighbours[i]->_neighbours;
      for(int j = 0; j < neigh.Size() && found < num; j++)
      {
        if (neigh[j]->IsActive())
        {
          neigh[j]->SetGroup(groupID);
          found++;
          newBorderNeighbours.Add(neigh[j]);
        }
      }
    }
    borderNeighbours = newBorderNeighbours;
  }

  return found;
}

bool Divider::ReleaseGroup(int groupID)
{
  bool keeped = false;
  for (int i = 0; i < _nodes.Size(); i++)
  {     
    if (_nodes[i]->_group == groupID)
    {
      // joint it to something else
      if (_nodes[i]->_neighbours.Size() == 0)
      {
        keeped = true;
        continue;
      }

      RefArray<Divider::Node>& neigh = _nodes[i]->_neighbours;
      int j = 0;
      while(j < neigh.Size() && (neigh[j]->IsActive() || neigh[j]->_group == groupID)) j++;

      if (j == neigh.Size())
      {
        keeped = true;
        continue;
      }

      _nodes[i]->_group = neigh[j]->_group;
    }
  }
  return !keeped;
}



////////////////////////////////////////
////////////////////////////////////////

int ObjToolJoinProxy::FindFace(AutoArray<Vector3>& points) const
{
  Vector3 center(VZero);

  for(int i = 0; i < points.Size(); i++) center += points[i];

  for(int i = 0;i < NFaces(); i++)
  {  
    FaceT f(this, i);

    if (points.Size() != f.N())
      continue;

    Vector3 faceCenter(VZero);
    for(int j = 0;j < f.N(); j++) faceCenter += Point(f.GetPoint(j));

    if ((center - faceCenter).SquareSize() > 0.0001f )
      continue;

    // check the points exactly
    int j = 0;
    for(;j < f.N(); j++) 
      if ((Point(f.GetPoint(j)) - points[0]).SquareSize() <  0.0001f)
        break;

    if (j == f.N())
      continue;

    // first point found
    int k = 1;
    for(; k < points.Size(); k++)
    {
      j++;
      if (j == f.N())
        j = 0;

      if ((Point(f.GetPoint(j)) - points[k]).SquareSize() >  0.0001f)
        break;        
    }

    if (k == points.Size())
      return i;  // face found
  }
  return -1;
}


void ObjToolJoinProxy::FindMaterialChanges(
         FindArray<UnigueMaterial>& from,
         FindArray<UnigueMaterial>& to, 
         const ObjectData& pattern) const
{
  AutoArray<Vector3> pos;
  for(int i = 0; i < NFaces(); i++)
  {
    FaceT f(this,i);

    UnigueMaterial mat;
    mat._texture = f.GetTexture();
    mat._material = f.GetMaterial();

    if (from.Find(mat) >= 0)
      // already found
      continue;
    
    pos.Resize(f.N());
    for(int j = 0; j < f.N(); j++) pos[j] = Point(f.GetPoint(j));

    int face = pattern.GetTool<ObjToolJoinProxy>().FindFace(pos);

    if (face < 0)
      // not found 
      continue; 

    FaceT fTo(&pattern, face);
    UnigueMaterial matTo;
    matTo._texture = fTo.GetTexture();
    matTo._material = fTo.GetMaterial();

    from.Add(mat);
    to.Add(matTo);
  }
}

void ObjToolJoinProxy::ChangeMaterials(const FindArray<UnigueMaterial>& from, const FindArray<UnigueMaterial>& to, Selection * sel)
{
  for(int i = 0; i < NFaces(); i++)
  {
    if (sel && !sel->FaceSelected(i)) continue;

    FaceT f(this,i);

    UnigueMaterial mat;
    mat._texture = f.GetTexture();
    mat._material = f.GetMaterial();

    int id = from.Find(mat);
    if (id < 0)
      continue;

    f.SetTexture(to[id]._texture);
    f.SetMaterial(to[id]._material);
  }
}

void ObjToolJoinProxy::SaveInstParamFile(const char * paramFile, const ObjectData & materialPatern,NamedSelection ** list, int size, const char * basePath) const
{
  ParamFile f;

  ParamClassPtr main = f.AddClass("XRefInstancies");
  ParamEntryPtr array = main->AddArray("names");

  BString<10> name;
  for(int i = 0; i < size; i++)
  {

    NamedSelection& sel = *list[i];
    // find materials change
    NamedSelection * act = &sel;
    LODObject src;
    Matrix4 resTrans;
    CreateModelFromProxies(src, &act, 1, resTrans, basePath, false);

    FindArray<UnigueMaterial> from;
    FindArray<UnigueMaterial> to;

    int textLevel = src.FindLevelExact(2000);
    if (textLevel >= 0)
    {    
      ObjectData * srcLevel = src.Level(textLevel);
      srcLevel->GetTool<ObjToolJoinProxy>().FindMaterialChanges(from, to, materialPatern);
    }

    
    // save into param file
    
    sprintf(name,"Inst%d",i);
    array->AddValue((const char *)name);
    ParamClassPtr inst = main->AddClass((const char *)name);

    int proxyFace = GetTool<ObjToolProxy>().FindProxyFace(sel.Name());
    if (proxyFace < 0)
      continue;

    FaceT proxy(this,proxyFace);
    Matrix4 trans;
    ObjToolProxy::GetProxyTransform(proxy,trans);

    ParamEntryPtr orient = inst->AddArray("orientation");
    for(int j = 0; j < 3; j++) for(int k = 0; k < 3; k++) orient->AddValue(trans.Orientation()(j,k));

    ParamEntryPtr pos = inst->AddArray("position");
    for(int j = 0; j < 3; j++) pos->AddValue(trans.Position()[j]);

    // model name
    Pathname proxyPath(sel.Name()+strlen("proxy:"),basePath);
    proxyPath.SetExtension(".p3d");

    inst->Add("model", proxyPath.GetFullPath() + strlen(basePath));

    ParamEntryPtr matChange = inst->AddArray("materialChange");

    for(int j = 0; j < from.Size(); j++)
    {
      sprintf(name,"Mat%d",j);
      matChange->AddValue((const char *)name);

      ParamClassPtr mat = inst->AddClass((const char *) name);
      mat->Add("from_texture",from[j]._texture);
      mat->Add("from_rvmat",from[j]._material);
      mat->Add("to_texture",to[j]._texture);
      mat->Add("to_rvmat",to[j]._material);
      mat->Add("u_shift",0);
      mat->Add("v_shift",0);
    }
  }

  f.Save(paramFile);
}



void ObjToolJoinProxy::GetPolygonsWithTexture(AutoArray<AutoArray<Vector3> >& polygons, const RString& text, Selection * workOn) const
{ 
  // find polygons
  int indexList[1024]; 

  CEdges faceGraph;
  faceGraph.SetVertices(NFaces());
  CEdges nocross;

  BuildFaceGraph(faceGraph, nocross);
 
  Selection restFaces(this);
  if (workOn)  
    restFaces = *workOn; 
  else
    restFaces.SelectAll();
      
  for(int i = 0;i < NFaces(); i++)
  {  
    if (!restFaces.FaceSelected(i) )
      continue;

    FaceT f(this,i);

    if (_stricmp(f.GetTexture().Data(),text) != 0) 
      continue;

    Selection sel(this);
    int nIndexes = GetTool<ObjToolTopology>().FindPolygon(i, faceGraph, indexList, 1024, 
      ObjToolTopology::fpPrecisionMedium | ObjToolTopology::fpSameTextue |  ObjToolTopology::fpSameMapping, NULL, &sel, &restFaces);

    if (nIndexes < 0)
      continue;

    restFaces -= sel;
    AutoArray<Vector3>& poly = polygons.Append();

    poly.Reserve(nIndexes,nIndexes);
    for(int j = 0; j < nIndexes; j++)
    {
      if (indexList[j] < 0)
        continue;

      poly.Add(Point(indexList[j]));
    }    
  }
}

struct PieceNode
{
  int _id;
  RString _modelInstance;
  RString _selection;

  FindArray<int> _neighbors;
  ClassIsMovable(PieceNode);
};


struct GlueFaceNode
{
  PieceNode * _pieceNode;
  AutoArray<Vector3> _face;
  Vector3 _center;
  
  void Neighbor(GlueFaceNode& neighbor)
  {
    if (_face.Size() ==  0 || _face.Size() != neighbor._face.Size() ||
        (_center - neighbor._center).SquareSize() > 0.0001f ||
         _pieceNode->_neighbors.Find(neighbor._pieceNode->_id) >= 0 ||
         neighbor._pieceNode->_id == _pieceNode->_id)
      return;

    // check the points exactly
    int j = 0;
    for(;j < _face.Size(); j++) 
      if ((neighbor._face[j] - _face[0]).SquareSize() <  0.0001f)
        break;

    if (j == _face.Size())
      return;

    // first point found
    
    for(int k = 1; k < _face.Size(); k++)
    {      
      if (--j < 0 ) // opposite normal
        j = _face.Size() - 1;

      if ((neighbor._face[j] - _face[k]).SquareSize() > 0.0001f)
        return;        
    }

    // is neighbor... 
    _pieceNode->_neighbors.Add(neighbor._pieceNode->_id);
    neighbor._pieceNode->_neighbors.Add(_pieceNode->_id);
  }
  ClassIsMovable(GlueFaceNode);
};





void ObjToolJoinProxy::SavePartInstParamFile(const char * paramFileIn, const char * paramFileOut, const char * basePath)
{
  ParamFile fIn;
  fIn.Parse(paramFileIn);

  ParamEntryPtr entry = fIn.FindEntry("XRefInstancies");
  if (!entry || !entry->IsClass())
  {
    Assert(FALSE);
    return;
  }

  ParamClassPtr main = entry->GetClassInterface();
  ParamEntryVal names = *main >> "names";
  Assert(names.IsArray());

  AutoArray<GlueFaceNode> gluedFaces;
  AutoArray<PieceNode> pieces;

  pieces.Reserve(names.GetSize() * 10, names.GetSize() * 10); // MUST BE enough

  /// Fill pieces and gluedFaces
  int n = names.GetSize();
  for(int i = 0; i < names.GetSize(); i++)
  {
    ParamEntryPtr entry = main->FindEntry((RStringB) names[i]);
    if (!entry || !entry->IsClass())
      continue;

    ParamClassPtr inst = entry->GetClassInterface();

    RString modelName = *inst >> "model";

    Pathname modelPath(modelName, basePath);

    LODObject model;
    if (model.Load(modelPath,NULL, NULL) < 0)
      continue; // model does not exist

    int level = model.FindLevelExact(-1);
    if (level < 0)
      continue;

    ObjectData * src = model.Level(level);

    /// Get transformation 
    Matrix4 trans;
    ParamEntryVal orient = *inst >> "orientation";
    for(int j = 0; j < 3; j++) for(int k = 0; k < 3; k++) trans(j,k) = orient[j*3 + k];
    ParamEntryVal pos = *inst >> "position";
    trans.SetPosition(Vector3(pos[0], pos[1], pos[2]));     


    /// Find all selections.
    for(int j = 0; j < MAX_NAMED_SEL; j++)
    {
      NamedSelection * sel = src->GetNamedSel(j);
      if (sel)
      {
        PieceNode& piece = pieces.Append();
        piece._modelInstance = (RStringB) names[i];
        piece._selection = sel->Name();
        piece._id = pieces.Size() - 1;

        AutoArray<AutoArray<Vector3> > polygons;
        src->GetTool<ObjToolJoinProxy>().GetPolygonsWithTexture(polygons, RString("ofp2\\structures\\buildings\\data\\temp\\glue_a.tga"), sel);

        for(int k = 0; k < polygons.Size(); k++)
        {
          GlueFaceNode& node = gluedFaces.Append();
          node._pieceNode = &pieces[pieces.Size() - 1]; // dangerous code
          node._face.Resize(polygons[k].Size());
          node._center = VZero;
          for(int l = 0; l < polygons[k].Size(); l++) node._center += (node._face[l] = trans * polygons[k][l]);                     
          node._center /= (float) polygons[k].Size();

        }         
      }
    }
  }

  /// Check gluedFaces every body to everybody  
  for(int i = 0; i < gluedFaces.Size(); i++) for(int j = i + 1; j < gluedFaces.Size(); j++) gluedFaces[i].Neighbor(gluedFaces[j]);

  /// collect and save result...
  ParamFile fOut;
  fOut.Parse(paramFileIn); // copy previous file

  AutoArray<bool> used;
  used.Resize(pieces.Size());
  for(int i = 0; i < pieces.Size(); i++) used[i] = false;

  ParamClassPtr partMain = fOut.AddClass("PartInstancies");
  ParamEntryPtr array = partMain->AddArray("names"); 

  BString<10> namePart;

  for(int i = 0; i < pieces.Size(); i++)
  {
    while(i <  pieces.Size() && used[i] ) i++;
    if (i == pieces.Size())
      break;

    sprintf(namePart, "Part%d", array->GetSize());
    array->AddValue((const char *)namePart);
    ParamClassPtr part = partMain->AddClass((const char *)namePart);
    ParamEntryPtr arrayPc = part->AddArray("pieces"); 

    FindArray<int> toDo;
    toDo.Add(i);
    int j = 0;
    while (j != toDo.Size())
    {
      /// save into paramfile 
      sprintf(namePart, "SubPart%d", arrayPc->GetSize());
      arrayPc->AddValue((const char *)namePart);
      ParamClassPtr piece = part->AddClass((const char *)namePart);          

      PieceNode& pc = pieces[toDo[j]];
      piece->Add("XRefInstance", pc._modelInstance);
      piece->Add("selection", pc._selection);

      for(int k = 0; k < pc._neighbors.Size(); k++) if (toDo.Find(pc._neighbors[k]) < 0) toDo.Add(pc._neighbors[k]);

      used[toDo[j]] = true;
      j++;
    }   
  }


  fOut.Save(paramFileOut);
}


void ObjToolJoinProxy::CreateModelFromModels(LODObject& out, const AutoArray<CMFMData>& models)
{  
  for(int j = 0; j < models.Size(); j++)
  {
    const CMFMData& model = models[j];

    LODObject src;
    if (src.Load(model._model, NULL, NULL) < 0)
      continue;

    for (int i=0;i<src.NLevels();i++)
    { 
      ObjectData *obj=src.Level(i);
      int targetLevel=out.FindLevelExact(src.Resolution(i));
      if (targetLevel==-1) targetLevel=out.AddLevel(ObjectData(),src.Resolution(i));
      ObjectData *objTarget=out.Level(targetLevel);

      /// remove everything except selection 
      NamedSelection * sel  = model._selection != RString("") ? obj->GetNamedSel(model._selection) : NULL;
      if (sel != NULL)
      {
        /// remove everything except selection
        ~(*sel);
        obj->SelectionDelete(sel);
      }

      if (model._textureSet)
      {
        /// change materials
        obj->GetTool<ObjToolJoinProxy>().ChangeMaterials(model._textureSet->GetFrom(), model._textureSet->GetTo(),NULL);
      }

      for (int j=0;j<obj->NPoints();j++)
      {
        PosT &pos=obj->Point(j);
        pos.SetPoint(model._trans.FastTransform(pos));
      }

      obj->RecalcNormals();

      if (model._resSelection != RString(""))
        objTarget->Merge(*obj,model._resSelection, true);
      else
        objTarget->Merge(*obj, NULL, true);
    }
  }
}

void ObjToolJoinProxy::CreateModelFromModelsFillLODs(LODObject& out, const AutoArray<CMFMData>& models, bool positive)
{
  FindArray<float> LODs; 
  for(int i = 0; i < models.Size(); i++)
  {
    const CMFMData& model = models[i];

    LODObject src;
    if (src.Load(model._model, NULL, NULL) < 0)
      continue;

    for(int j = 0; j < src.NLevels(); j++)
      if (((positive && src.Resolution(j) > 0) || (!positive && src.Resolution(j) < 0)) && LODs.Find(src.Resolution(j)) < 0)
        LODs.Add(src.Resolution(j));
  }

  CreateModelFromModels(out, models, LODs);
}

void ObjToolJoinProxy::CreateModelFromModels(LODObject& out, const AutoArray<CMFMData>& models, const AutoArray<float>& LODs)
{
  AutoArray<bool> used;
  for(int i = 0; i < models.Size(); i++)
  {
    const CMFMData& model = models[i];

    LODObject src;
    if (src.Load(model._model, NULL, NULL) < 0)
      continue;

    used.Resize(src.NLevels()) ;    
    for(int j = 0; j < src.NLevels(); j++) used[j] = false;
    

    for(int level = 0; level < LODs.Size(); level++)
    {
      float res = LODs[level]; // resolution
      bool isSpecial = res >= LOD_SHADOW_MIN || res <= -LOD_SHADOW_MIN;

      int srcLevel = -1;  
      for (int k=0; k<src.NLevels(); k++)
      { 
        if (isSpecial)
        {
          if (src.Resolution(k) == res)
          {
            srcLevel = k;
            break;
          }
        }
        else
        {
          if (res < 0)
          {
            if (src.Resolution(k) < 0 && res <= src.Resolution(k) && 
              (srcLevel < 0 || src.Resolution(k) < src.Resolution(srcLevel)))          
              srcLevel = k;
          }
          else
          {
            if (src.Resolution(k) > 0 && res >= src.Resolution(k) && 
              (srcLevel < 0 || src.Resolution(k) > src.Resolution(srcLevel)))          
              srcLevel = k;
          }
        }
      }
      if (srcLevel == -1)
        continue;

      ObjectData *obj=src.Level(srcLevel);
      int targetLevel=out.FindLevelExact(res);
      if (targetLevel==-1) targetLevel=out.AddLevel(ObjectData(),res);
      ObjectData *objTarget=out.Level(targetLevel);

      if (!used[srcLevel])
      {
        used[srcLevel] = true; 
      
        /// remove everything except selection 
        NamedSelection * sel  = model._selection != RString("") ? obj->GetNamedSel(model._selection) : NULL;
        if (sel != NULL)
        {
          /// remove everything except selection
          ~(*sel);
          obj->SelectionDelete(sel);
           ~(*sel);
        }

        if (model._textureSet)
        {
          /// change materials
          obj->GetTool<ObjToolJoinProxy>().ChangeMaterials(model._textureSet->GetFrom(), model._textureSet->GetTo(), sel);
        }

        for (int j=0;j<obj->NPoints();j++)
        {
          PosT &pos=obj->Point(j);
          pos.SetPoint(model._trans.FastTransform(pos));
        }

        obj->RecalcNormals();
      };

      if (model._resSelection != RString(""))
        objTarget->Merge(*obj,model._resSelection, true);
      else
        objTarget->Merge(*obj, NULL, true);      
    }
  }
}

class TextureSetLoadable : public TextureSet
{
public:
  void Load(ParamClassPtr modelInst);
  ClassIsMovable(TextureSetLoadable)
};

/// compare without UVShift...
bool TextureSet::operator==(const TextureSet& set) const
{
  /// compare... it can be in different order
  if (_from.Size() != set._from.Size())
    return false;

  for(int i = 0; i < _from.Size(); i++)
  {
    int id = _from.Find(set._from[i]);
    if (id < 0)
      return false;

    if (!(_to[id] == set._to[i]))
      return false;
  }  
  return true;
}

void TextureSetLoadable::Load(ParamClassPtr modelInst)
{
  _from.Resize(0);
  _to.Resize(0);
  _UVshift.Resize(0);

  ParamEntryVal mats = *modelInst >> "materialChange";

  for(int i = 0; i < mats.GetSize(); i++)
  {
    ParamEntryPtr entry = modelInst->FindEntry((RStringB) mats[i]);

    if (!entry || !entry->IsClass())
      continue;

    ParamClassPtr mat = entry->GetClassInterface();

    UnigueMaterial& fromMat = _from.Append();
    UnigueMaterial& toMat = _to.Append();
    fromMat._texture = *mat >> "from_texture";
    fromMat._material = *mat >> "from_rvmat";
    toMat._texture = *mat >> "to_texture";
    toMat._material = *mat >> "to_rvmat";
    _UVshift.Append() = *mat >> "u_shift";
    _UVshift.Append() = *mat >> "v_shift";
  }
}

class XRefModelInst
{
protected:
  RStringB _name;
  RStringB _model;
  Matrix4 _trans;
  Ref<TextureSetLoadable> _textureSet;

public:

  const RStringB& GetName() const {return _name;};
  const RStringB& GetModelFile() const {return _model;};
  const Matrix4& Trans() const {return _trans;};

  const TextureSet& GetTextureSet() const {return *_textureSet;};

  void Load(ParamClassPtr modelInst);
  ClassIsMovable(XRefModelInst);
};

void XRefModelInst::Load(ParamClassPtr modelInst)
{
  _name = modelInst->GetName();
  _model = *modelInst >> "model";
  ParamEntryVal orient = *modelInst >> "orientation";
  for(int j = 0; j < 3; j++) for(int k = 0; k < 3; k++) _trans(j,k) = orient[j*3 + k];

  ParamEntryVal pos = *modelInst >> "position";
  _trans.SetPosition(Vector3(pos[0], pos[1], pos[2]));

  _textureSet = new TextureSetLoadable();
  _textureSet->Load(modelInst);
}

// traits for key comparion and searching
struct XRefModelInstTraits
{
  typedef const RStringB &KeyType;
  /// check if two keys are equal
  static bool IsEqual(KeyType a, KeyType b) {return a==b;}
  /// get a key from an item
  static KeyType GetKey(const XRefModelInst &a) {return a.GetName();};
};

class XRefModelInstContainer : public FindArrayKey<XRefModelInst,XRefModelInstTraits> 
{
public:
  void Load(ParamFile& in)
  {
    Resize(0);
    ParamEntryPtr entry = in.FindEntry("XRefInstancies");
    if (!entry || !entry->IsClass()) 
      return;

    ParamClassPtr models = entry->GetClassInterface() ;
    ParamEntryVal insts = *models >> "names";

    Reserve(insts.GetSize(), insts.GetSize());

    for(int i = 0; i < insts.GetSize(); i++)
    {
      ParamEntryPtr entry = models->FindEntry((RStringB) insts[i]);
      if (!entry || !entry->IsClass())
        continue;

      ParamClassPtr modelInst = entry->GetClassInterface();
      XRefModelInst& model = Append();

      model.Load(modelInst);
    }
  }
};

class PieceInst 
{
protected:
  const XRefModelInst * _model;
  RStringB _selection;
public:
  const XRefModelInst * GetModel() const {return _model;};
  const RStringB& GetSelection() const  {return _selection;}; 

  void Load(ParamClassPtr piece, const XRefModelInstContainer& models)
  {
    RStringB modelName = *piece >> "XRefInstance";
    int id = models.FindKey(modelName);
    if (id >= 0)
      _model = &models[id];
    else
      _model = NULL;

    _selection = *piece >> "selection";
  }
  bool operator== (const PieceInst& piece) const {return _model == piece._model && _selection == piece._selection;};
  ClassIsMovable(PieceInst);
};

class PartInst 
{
protected:

  int _pivot;
  Vector3 _pivotOffset;
  FindArray<PieceInst> _pieces;
  RStringB _savedInto;
public:

  const FindArray<PieceInst> &GetPieces() const {return _pieces;};
  const PieceInst& GetPivot() const {return _pieces[_pivot];}; 
  int GetPivotID() const {return _pivot;};
  void SetPivotID(int i) {_pivot = i;};

  const Vector3& GetPivotOffset() const {return _pivotOffset;};
  void SetPivotOffset(const Vector3& offset) {_pivotOffset = offset;};

  void SetSaved(const RStringB& file) {_savedInto = file;};  
  const RStringB& GetSaved() const {return _savedInto;};
  
  void Load(ParamClassPtr inst, const XRefModelInstContainer& models) 
  {
    ParamEntryVal piecesNames = *inst >> "pieces";
    _pieces.Reserve(piecesNames.GetSize(),piecesNames.GetSize());

    for(int i = 0; i < piecesNames.GetSize(); i++)
    {
      ParamEntryPtr entry = inst->FindEntry((RStringB)piecesNames[i]);
      if (!entry || !entry->IsClass())
        continue;

      ParamClassPtr pieceInst = entry->GetClassInterface();
      _pieces.Append().Load(pieceInst, models);
    }
    _pivot = 0;
  }
  ClassIsMovable(PartInst);
};

class PartInstContainer : public AutoArray<PartInst> 
{
public:
  void Load(ParamFile& in, const XRefModelInstContainer& models)
  {
    Resize(0);
    ParamEntryPtr entry = in.FindEntry("PartInstancies");
    if (!entry || !entry->IsClass()) 
      return;

    ParamClassPtr parts = entry->GetClassInterface() ;
    ParamEntryVal insts = *parts >> "names";

    Reserve(insts.GetSize(), insts.GetSize());

    for(int i = 0; i < insts.GetSize(); i++)
    {
      ParamEntryPtr entry = parts->FindEntry((RStringB) insts[i]);
      if (!entry || !entry->IsClass())
        continue;

      ParamClassPtr modelInst = entry->GetClassInterface();
      Append().Load(modelInst, models);      
    }
  }
};

class SavePartInto
{
private:
  static bool IsEqualModel(const XRefModelInst& model1, const XRefModelInst& model2);
  static bool IsEqualPieceGroup(const FindArray<PieceInst>& pcs1,const FindArray<PieceInst>& pcs2, int i, const Matrix4& trans, int * map);
  static bool IsEqualPiece(const PieceInst& pc1, const PieceInst& pc2, Matrix4& trans);
public: 
  static bool SaveInto(PartInst& part, const PartInst& pattern);
  static void SaveInto(PartInst& part, const char * path, const char * basePath);
};

bool SavePartInto::IsEqualModel(const XRefModelInst& model1, const XRefModelInst& model2)
{
  if (model1.GetModelFile() != model2.GetModelFile())
    return false;

  //compare texture sets
  if (!(model1.GetTextureSet() == model2.GetTextureSet()))
    return false;

  return true;

}

const float NUM_ZERO = 0.0001f; 
bool IsSimilar(const Matrix4& m1, const Matrix4& m2)
{
  Matrix4 m = (m1 * m2.InverseRotation()) - M4Identity;
  return m.Direction().SquareSize() < NUM_ZERO && m.DirectionUp().SquareSize() < NUM_ZERO 
    && m.DirectionAside().SquareSize() < NUM_ZERO && m.Position().SquareSize() < NUM_ZERO;
}

bool SavePartInto::IsEqualPieceGroup(const FindArray<PieceInst>& pcs1,const FindArray<PieceInst>& pcs2, int i, const Matrix4& trans, int * map)
{
  /// find mapping for rest 
  int n = pcs1.Size();
  if (i == n)
    return true;

  const PieceInst& findFor = pcs1[i];

  for(int j = i; j < n; j++)
  {
    Matrix4 relTrans;
    if (SavePartInto::IsEqualPiece(findFor, pcs2[map[j]], relTrans) && IsSimilar(relTrans, trans))
    {
      /// found potential candidate
      int temp = map[j];
      map[j] = map[i];
      map[i] = temp;

      if (IsEqualPieceGroup(pcs1, pcs2, i + 1, trans, map))
        return true;
    }         
  }
  return false; 
}

bool SavePartInto::IsEqualPiece(const PieceInst& pc1, const PieceInst& pc2, Matrix4& trans)
{
  // is piece instance-able?
  if (pc1.GetSelection() != pc2.GetSelection() || 
    !IsEqualModel(*pc1.GetModel(), *pc2.GetModel()))    
    return false;  

  trans = pc1.GetModel()->Trans() * pc2.GetModel()->Trans().InverseRotation();
  return true;  
}

bool SavePartInto::SaveInto(PartInst& part, const PartInst& pattern)
{
  if (part.GetPieces().Size() != pattern.GetPieces().Size())
    return false;

  Matrix4 trans;
  if (part.GetPieces().Size() == 1 && IsEqualPiece(part.GetPieces()[0], pattern.GetPieces()[0], trans))
  {
    // trivial case 
    part.SetSaved(pattern.GetSaved());
    part.SetPivotOffset(pattern.GetPivotOffset());
    return true;
  }
 
  const FindArray<PieceInst>& pcs1 = pattern.GetPieces();
  const FindArray<PieceInst>& pcs2 = part.GetPieces();
  int n = pcs1.Size();

  const PieceInst& findFor = pcs1[0];  

  Buffer<int> map(n);
  for(int i = 0; i < n; i++)
  {
    Matrix4 trans;
    if (IsEqualPiece(findFor,pcs2[i], trans))
    {      
      for(int j = 0; j < n; j++) map[j] = j;
      map[0] = i;
      map[i] = 0;

      if (IsEqualPieceGroup(pcs1, pcs2, 1, trans, map.Data()))
      {
        part.SetSaved(pattern.GetSaved());
        part.SetPivotID(map[pattern.GetPivotID()]);
        part.SetPivotOffset(pattern.GetPivotOffset());
        return true;
      }
    }
  }
  return false;
}

void SavePartInto::SaveInto(PartInst& part, const char * path, const char * basePath)
{
  // coordinates will be defined by model with lowest name
  const FindArray<PieceInst>& pieces = part.GetPieces();
  const PieceInst& pivot = part.GetPivot();

  Matrix4 trans = pivot.GetModel()->Trans().InverseRotation();

  AutoArray<CMFMData> models;
  BString<20> resSelection; 
  for(int i = 0; i < pieces.Size(); i++)
  {
    const PieceInst& pc = pieces[i];
    CMFMData& model = models.Append();   

    model._model = Pathname(pc.GetModel()->GetModelFile(), basePath);
    model._trans = trans *  pc.GetModel()->Trans();
    model._selection = pc.GetSelection();
    model._textureSet = &pc.GetModel()->GetTextureSet();

    sprintf(resSelection, "SubPart%d", i);
    model._resSelection = (const char *)resSelection;
  }

  LODObject out;
  ObjToolJoinProxy::CreateModelFromModelsFillLODs(out, models, false);

  /// remove positive LODs
  for(int j = 0; j < out.NLevels(); j++)
  {
    if (out.Resolution(j) >= 0)
      out.DeleteLevel(j--);
  }

  /// negative to positive LODs
  for(int j = out.NLevels() - 1; j >= 0 ; j--)
  {           
    out.SetResolution(j, -out.Resolution(j));
  }

  /*for(int j = out.NLevels() - 1; j >= 0 ; j--)
  {           
    ObjectData * obj = out.Level(j);    

    // remove all double faces with glue texture... 
    //FilterTexture f(glue_a);
    //sel1->GetObject()->GetTool<ObjToolTopology>().SelectDoublePolygons(sel1,*sel1,f);

    Selection sel(obj);
    for(int i = 0; i < obj->NFaces(); i++)
    {
      FaceT f(obj,i);
      if (_stricmp(f.GetTexture().Data(),"ofp2\\structures\\buildings\\data\\temp\\glue_a.tga") == 0)
      {
        sel.FaceSelect(i);
      }
    }
    obj->SelectionDeleteFaces(&sel);
    obj->RecalcNormals();
  }*/

  /// Move Pivot into center... 
  Vector3 offset = out.CenterAll();

  Pathname savePath(path);
  savePath.CreateFolder();
  out.Save(path, OBJDATA_LATESTVERSION, false, NULL, NULL);
  part.SetSaved(path);
  part.SetPivotOffset(offset);
}

template<class Traits>
class CreateInstancableParts
{
protected:
  XRefModelInstContainer& _xRefs;
  PartInstContainer& _parts;
  AutoArray<int> _originals;

public:
  CreateInstancableParts(XRefModelInstContainer& xRefs, PartInstContainer& parts) : _parts(parts), _xRefs(xRefs) {};
  void operator()(AutoArray<RString>& created,const char * partsDir, const char * basePath);
};

template<class Traits>
void CreateInstancableParts<Traits>::operator()(AutoArray<RString>& created,const char * partsDir, const char * basePath)
{
  for(int i = 0; i < _parts.Size(); i++)
  {
    PartInst& part = _parts[i];

    int j = 0;
    for(;j < _originals.Size() && !Traits::SaveInto(part, _parts[_originals[j]]); j++);

    if (j == _originals.Size())
    {
      // new original
      _originals.Add(i);

      BString<256> out;
      sprintf(out,"%s\\part%d\\part%d.p3d",partsDir,_originals.Size(), _originals.Size());
      created.Add(RString(out));

      Traits::SaveInto(part,(const char *) out, basePath);
    }
  }
}

void ObjToolJoinProxy::BreakableHouse(AutoArray<RString>& created, const char * paramFileIn, const char * partsDir, const char * basePath)
{
  /// load info about instances
  XRefModelInstContainer xRefs;
  ParamFile in;
  in.Parse(paramFileIn);  
  xRefs.Load(in);

  PartInstContainer parts;
  parts.Load(in, xRefs);

  /// Compare them and save the originals....
  CreateInstancableParts<SavePartInto> saver(xRefs,parts); 
  saver(created,partsDir,basePath);

  /// Create proxies... 

  for(int i = 0; i < parts.Size(); i++)
  {  
    Matrix4 trans = parts[i].GetPivot().GetModel()->Trans();
    trans.SetPosition(trans.Position() + trans.Rotate(parts[i].GetPivotOffset()));

    //trans.SetPosition(trans.Position() * 1.1);
    GetTool<ObjToolProxy>().CreateProxy(((const char *)parts[i].GetSaved()) + strlen(basePath),trans);        
  }
}

void ObjToolJoinProxy::UnbreakableHouse(LODObject& out, const char * paramFileIn, const char * basePath)
{
  ParamFile in;
  in.Parse(paramFileIn);  
  XRefModelInstContainer xRefs;
  xRefs.Load(in);

  AutoArray<CMFMData> models;
  models.Reserve(xRefs.Size(),xRefs.Size());

  for(int i = 0; i < xRefs.Size(); i++)
  {
    CMFMData& model = models.Append();
    const XRefModelInst& refModel = xRefs[i];

    model._model = Pathname(refModel.GetModelFile(),basePath);
    model._trans = refModel.Trans();
    model._textureSet = &refModel.GetTextureSet();
  }

  CreateModelFromModelsFillLODs(out, models, true);

  // delete negative LODs... 
  for(int j = 0; out.NLevels() > 1 && j < out.NLevels(); j++)
  {       
    if (out.Resolution(j) <= 0)
      out.DeleteLevel(j--);
  }

  /// delete also 2000 lod
  int id = out.FindLevelExact(2000);
  if (id >= 0)
    out.DeleteLevel(id);
}


void ObjToolJoinProxy::CenterProxies(NamedSelection ** list, int size, const char * basePath)
{
  FindArray<Pathname> models;
  models.Reserve(size, size);

  AutoArray<Vector3> offsets;
  offsets.Reserve(size,size);

  for(int i = 0; i < size; i++)
  {  
    const NamedSelection * proxySel = list[i];

    int proxyFace = 0;
    for (;proxyFace<NFaces();proxyFace++) if (proxySel->FaceSelected(proxyFace)) break;
    if (proxyFace==NFaces()) continue;

    Pathname proxyPath(proxySel->Name()+strlen("proxy:"),basePath);
    proxyPath.SetExtension(".p3d");

    FaceT proxy(this,proxyFace);
    Matrix4 proxyTrn;
    ObjToolProxy::GetProxyTransform(proxy,proxyTrn);

    Vector3 offset;
    int id = models.Find(proxyPath);

    if (id >= 0)
    {
      // such model already centered... use offset
      offset = offsets[id];
    }
    else
    {
      // center model
      LODObject model;
      if (model.Load(proxyPath,NULL, NULL) <0)
        continue;

      offset = model.CenterAll();
      if (model.Save(proxyPath,OBJDATA_LATESTVERSION,false, NULL, NULL) < 0)
        continue;

      models.Add(proxyPath);
      offsets.Add(offset);
    }

    // move proxy
    offset = proxyTrn.Rotate(offset);
    for(int j = 0; j < proxy.N(); j++)
    {
      Point(proxy.GetPoint(j)) += offset;      
    }
  } 
}

/*bool ObjToolJoinProxy::AreAllDoubleFaces(Selection &sel)
{
  bool isDouble = true;
  for(int i = 0; isDouble && i < NFaces(); i++)
  {
    if (!sel.FaceSelected(i))
      continue;

    FaceT f1(this,i);

    Vector3 normal = -f1.CalculateNormal();

    isDouble = false;
    for(int j = 0; !isDouble && j < NFaces(); j++)
    {
      if (sel.FaceSelected(j))
        continue;

      FaceT f2(this,j);

      if (f1.N() != f2.N())
        continue;

      if (normal * f2.CalculateNormal() < 0.99f)
        continue; 

      isDouble = true; 
      for(int k = 0; isDouble && k < f1.N(); k++)
      {
        Vector3Val pt1 = Point(f1.GetPoint(k));

        isDouble = false;
        for(int l = 0; !isDouble && l < f2.N(); l++)
        {
          Vector3Val pt2 = Point(f2.GetPoint(l));
          if (pt1.Distance2(pt2) < 0.0001f)
            isDouble = true;
        }        
      }
    }    
  }

  return isDouble;
}*/

bool ObjToolJoinProxy::IsOpenOrSuperSharp(int pt0, int pt1)
{
  int found = 0;
  int face0 = -1;  
  for (int i = 0; i < NFaces(); i++)
  {
    FaceT f(this,i); 

    if (f.ContainsEdge(pt0,pt1) || f.ContainsEdge(pt1,pt0))
    {
      /// face has edge... 
      if (found == 2)
        return false; /// connects too many edges
      else if (found == 0)
      {
        face0 = i;
        found = 1;
      }
      else if (found == 1)
      {
        FaceT f1(this,face0);

        if (f.CalculateNormal() * f1.CalculateNormal() > -0.999f) 
          return false;

        found = 2;
      }
    }
  }
  return true;
}


void ObjToolJoinProxy::SplitPolygons() // split polygons in selection
{
  CEdges faceGraph;
  faceGraph.SetVertices(NFaces());
  CEdges nocross;

  BuildFaceGraph(faceGraph, nocross);  


  Selection todoSel(*GetSelection());

  int indexList[16384]; 
  Selection sel(this);
  for(int i = 0; i < NFaces(); i++)
  {
    if (!todoSel.FaceSelected(i))
      continue; 

    // find polygon
    sel.Clear();
    sel.Validate();
    
    int found = GetTool<ObjToolTopology>().FindPolygon(i, faceGraph, indexList, 1024, 
      ObjToolTopology::fpPrecisionLow, NULL, &sel);

    if (found <= 0)
      return;

    /// verify that all edges are super sharp (exist edge connects two faces with opposit normals)
    int prev = -1;

    bool superSharp = true; 
    for(int j = 0; j < found; j++)
    {
      if (indexList[j] >= 0)
      {
        sel.PointSelect(indexList[j]);
        if (prev >= 0)
        {
          if (!IsOpenOrSuperSharp(prev, indexList[j]))
          {
            superSharp = false;
            break; 
          }
        }
      }    
      
      prev = indexList[j];             
    }

    if (!superSharp)
    //if (!AreAllDoubleFaces(sel))
      continue;

    UseSelection(&sel);
    SelectionSplit();

    for(int j = 0; j < NFaces(); j++)
      if (sel.FaceSelected(j))
        todoSel.FaceSelect(j, false);
  }
}

void ObjToolJoinProxy::ShadowLod(bool splitFaces)
{
  
  Selection sel(this); /// 
  sel.SelectAll();
  UseSelection(&sel); 

  // split polygos if needed
  if (splitFaces)
  {
    SplitPolygons();  
  }

  // make all edges sharp...
  sel.Validate();
  sel.SelectAll();
  UseSelection(&sel); 

  GetTool<ObjToolTopology>().MakeEdgesSharp();

  // 
  GetTool<ObjToolTopology>().CheckClosedTopology();  
  const char * tmpSelName = "temp_shadowLod_points";
  SaveNamedSel(tmpSelName);
  NamedSelection * todoOpenEdges = GetNamedSel(tmpSelName);

  
  Selection doneFaces(this);

  for(int i = 0; i < NFaces(); i++)
  {
    if (doneFaces.FaceSelected(i))
      continue; 

    FaceT f(this,i);
    bool hasOpenEdge = false;

    sel.Clear();
    sel.Validate();


    for(int j = 0; j < f.N(); j++)
    {
      if (todoOpenEdges->PointSelected(f.GetPoint(j)))
      {
        hasOpenEdge = true;
        sel.PointSelect(f.GetPoint(j));       
      }
    }

    if (!hasOpenEdge)
      continue;


    for(int j = i + 1; j < NFaces(); j++)
    {
      if (doneFaces.FaceSelected(j))
        continue;

      FaceT f2(this,j);
      if (Normal(f.GetNormal(0)).Distance(Normal(f2.GetNormal(0))) < 0.01f )
      {
        doneFaces.FaceSelect(j, true);
        for(int k = 0; k < f2.N(); k++)
        {
          if (todoOpenEdges->PointSelected(f2.GetPoint(k)))
          {          
            sel.PointSelect(f2.GetPoint(k));
          }
        }
      }
    }

    MergePoints(0.01f, true, &sel);
  }

  DeleteNamedSel(tmpSelName);

  

  // select all not closed 
  GetTool<ObjToolTopology>().CheckClosedTopology();  

  // merge only points with opposite normals  
  SaveNamedSel(tmpSelName);
  todoOpenEdges = GetNamedSel(tmpSelName);
   
  doneFaces.Clear();
  doneFaces.Validate();

  for(int i = 0; i < NFaces(); i++)
  {
    if (doneFaces.FaceSelected(i))
      continue; 

    FaceT f(this,i);
    bool hasOpenEdge = false;

    sel.Clear();
    sel.Validate();

    
    for(int j = 0; j < f.N(); j++)
    {
      if (todoOpenEdges->PointSelected(f.GetPoint(j)))
      {
        hasOpenEdge = true;
        sel.PointSelect(f.GetPoint(j));       
      }
    }

    if (!hasOpenEdge)
      continue;

    
    for(int j = i + 1; j < NFaces(); j++)
    {
      if (doneFaces.FaceSelected(j))
        continue;

      FaceT f2(this,j);
      if (Normal(f.GetNormal(0)).Distance(Normal(f2.GetNormal(0))) < 0.01f || Normal(f.GetNormal(0)).Distance(-Normal(f2.GetNormal(0))) < 0.01f)
      {
        doneFaces.FaceSelect(j, true);
        for(int k = 0; k < f2.N(); k++)
        {
          if (todoOpenEdges->PointSelected(f2.GetPoint(k)))
          {          
            sel.PointSelect(f2.GetPoint(k));
          }
        }
      }
    }

    MergePoints(0.01f, true, &sel);
  }

  DeleteNamedSel(tmpSelName);
}

}

TypeIsMovable(Pathname);
