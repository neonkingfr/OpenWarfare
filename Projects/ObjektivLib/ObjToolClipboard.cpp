#include "common.hpp"
#include "ObjToolClipboard.h"


namespace ObjektivLib {


void ObjToolClipboard::ExtractSelection(const Selection *sel)
  {
  Selection _inv(this);
  int i,j;
  if (sel==NULL) sel=&_sel;
  for (i=0;i<NFaces();i++) _inv.FaceSelect(i,!sel->FaceSelected(i));
  for (i=0;i<NPoints();i++) _inv.PointSelect(i,!sel->PointSelected(i));
  for (i=0;i<NFaces();i++) if (!_inv.FaceSelected(i))
  {
    FaceT fc(*this,i);
    for (j=0;j<fc.N();j++) _inv.PointSelect(fc.GetPoint(j),false);
  }
  SelectionDelete(&_inv);
  RecalcNormals(true);
/*

  // copy whole object
  // delete all unselected faces
  for( i=NFaces(); --i>=0; )
    {
    if( !FaceSelected(i) ) DeleteFace(i);
    }
  // only selected faces left now.
  //  delete all unselected points not contained in any face
  // delete all normals not contained in any face
  Temp<bool> pointsUsed(NPoints());
  Temp<bool> normalsUsed(NNormals());
  memset(pointsUsed,0,NPoints()*sizeof(*pointsUsed));
  memset(normalsUsed,0,NNormals()*sizeof(*normalsUsed));
  for( i=0; i<NFaces(); i++ )
    {
    FaceT face(this,i);
    for( j=0; j<face.N(); j++ )
      {
      pointsUsed[face.GetPoint(j)]=true;
      normalsUsed[face.GetNormal(j)]=true;
      }
    }
  for( i=NPoints(); --i>=0; )
    {
    if( !pointsUsed[i] && !PointSelected(i) ) DeletePoint(i);
    }
  for( i=NNormals(); --i>=0; )
    {
    if( !normalsUsed[i] ) DeleteNormal(i);
    }
    */
  // delete all empty named selections
  for( i=0; i<MAX_NAMED_SEL; i++ ) if( _namedSel[i] )
    {
    if( _namedSel[i]->IsEmpty() ) delete _namedSel[i],_namedSel[i]=NULL;
    }
  }


bool ObjToolClipboard::SelectionCopy( UINT format )
  {
  bool ret=false;
  // open clipboard
  ::EmptyClipboard();
  // copy selected data
  SRef<ObjectData> obj=new ObjectData(*this,_autoSaveAnimation);
  if( !obj ) return false;
  obj->GetTool<ObjToolClipboard>().ExtractSelection();
  return obj->GetTool<ObjToolClipboard>().SaveClipboard(format);
  }

//--------------------------------------------------

static BOOL WINAPI GetFirstAppWindow(HWND hWnd,LPARAM lParam)
{
  HWND *save=(HWND *)lParam;
  *save=hWnd;
  return FALSE;
}

bool ObjToolClipboard::SaveClipboard( UINT format )
  {
  bool ret=false;
  bool closewnd=false;
  ostrstream f;
//  f.rdbuf()->setbuf(NULL,2048);
  if (SaveData(f,false,OBJDATA_LATESTVERSION)==-1) return false;
  if (SaveSetup(f,false,true,false)==-1) return false;

  HWND hWnd=NULL;
  ::EnumThreadWindows(GetCurrentThreadId(),GetFirstAppWindow,(LPARAM)&hWnd);
  if (hWnd==NULL) 
  {
    closewnd=true;
    hWnd=CreateWindow("STATIC","Objektiv2@ClipboardHelperWindow",0,0,0,0,0,NULL,NULL,GetModuleHandle(NULL),0);
  }

  if( ::OpenClipboard(hWnd) )
  {
    EmptyClipboard();
    f.seekp(0,ios::end);
    int size=f.tellp();
    const char *strData=f.str();
    if( strData )
      {
      // data valid
      HGLOBAL handle=GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE,size);
      if( handle )
        {
        void *memData=GlobalLock(handle);
        if( memData )
          {
          memcpy(memData,strData,size);
          ret=true;
          GlobalUnlock(handle);
          if (::SetClipboardData(format,(HANDLE)handle)==NULL) {ret=false;GlobalFree(handle);}
          }
        else 
          GlobalFree(handle);
        }
      }
    f.rdbuf()->freeze(0); // unlock data - destructor will deallocate
    //	free(f.str());
    ::CloseClipboard();
  } 
  if (closewnd) DestroyWindow(hWnd);
  return ret;
  }

//--------------------------------------------------

bool ObjToolClipboard::SelectionCut( UINT format )
  {
  if( SelectionCopy(format) ) return SelectionDelete();
  return false;
  }

//--------------------------------------------------

bool ObjToolClipboard::LoadClipboard( UINT format )
  {
  bool ret=false;
  if( !::OpenClipboard(NULL) ) return false;
  // get memory handle to object
  HGLOBAL handle=::GetClipboardData(format);
  if( handle )
    {
    void *data=GlobalLock(handle);
    if( data )
      {
      // data available
      DWORD size=GlobalSize(handle);
      // size can be larger than actual data size
      istrstream f((char *)data,size);
      LoadData(f);
      LoadSetup(f);
      ret=true;
      }
    GlobalUnlock(handle);
    }
  ::CloseClipboard();
  return ret;
  }

//--------------------------------------------------

bool ObjToolClipboard::SelectionPaste( UINT format )
  {
  SRef<ObjectData> obj=new ObjectData();
  if( obj )
    {
    if( obj->GetTool<ObjToolClipboard>().LoadClipboard(format) )
      {
      return Merge(*obj);
      }
    }
  return false;
  }


}
//--------------------------------------------------

