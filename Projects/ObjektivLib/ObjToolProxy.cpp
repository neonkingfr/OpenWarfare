#include "common.hpp"
#include <malloc.h>
#include ".\objtoolproxy.h"

namespace ObjektivLib {

const char *ObjToolProxy::ProxyPrefix="proxy:";

int ObjToolProxy::EnumProxies(NamedSelection **list, const char *filename, bool partial) const
  {
  if (filename==NULL) filename="";
  ASSERT(ProxyPrefix!=NULL); //You MUST assign prefix for proxies into ObjToolProxy::ProxyPrefix!
  char *buff=(char *)alloca(strlen(ProxyPrefix)+strlen(filename)+10);
  strcpy(buff,ProxyPrefix);
  if (filename[0]!='\\' && strchr(filename,'\\')!=NULL) strcat(buff,"\\");
  strcat(buff,filename);
  int cnt=0;
  int partsize=strlen(buff);
  if (!partial) partsize++;
  for (int i=0;i<MAX_NAMED_SEL;i++) if (GetNamedSel(i))
    {
    const NamedSelection *sel=GetNamedSel(i);
    const char *selname=sel->Name();
    int j;
    for (j=0;j<partsize;j++) if (toupper(selname[j])!=toupper(buff[j])) break;
    if (j==partsize) 
      {
      if (list) list[cnt]=const_cast<NamedSelection *>(sel);
      cnt++;
      }     
    }
  return cnt;
  }

void ObjToolProxy::GetProxyFilename(const char *src, char *trg)
  {
  ASSERT(ProxyPrefix!=NULL); //You MUST assign prefix for proxies into ObjToolProxy::ProxyPrefix!
  strcpy(trg,src+strlen(ProxyPrefix));
  char *endp=strrchr(trg,'.');
  if (endp) *endp=0;
  }

int ObjToolProxy::FilenameToProxyname(const char *filename, char *proxyname,  int index, int size)
  {
  ASSERT(ProxyPrefix!=NULL); //You MUST assign prefix for proxies into ObjToolProxy::ProxyPrefix!
  if (index<0 || index>999) return 0;
  char *fnamecopy=strcpy((char *)alloca(strlen(filename)+1),filename);
  char *ext=const_cast<char *>(Pathname::GetExtensionFromPath(fnamecopy));
  *ext=0;
  int ppfx=strlen(ProxyPrefix);
  int needsize=ppfx+strlen(filename)+3;
  bool slash=filename[0]!='\\' && strchr(filename,'\\')!=NULL;
  if (slash) needsize++;
  if (needsize>size) return needsize;
  sprintf(proxyname,"%s%s%s.%03d",ProxyPrefix,slash?"\\":"",fnamecopy,index);
  return needsize;
  }


bool ObjToolProxy ::SelectionToProxy(const char *filename, const Selection *sel)
  {
  ASSERT(ProxyPrefix!=NULL); //You MUST assign prefix for proxies into ObjToolProxy::ProxyPrefix!
  if (sel==NULL) sel=GetSelection();
  int index=0;
  int needsize=strlen(filename)+strlen(ProxyPrefix)+6;
  char *proxynewname=(char *)alloca(needsize);
  do
    {
    FilenameToProxyname(filename,proxynewname,++index,needsize);
    }
  while (GetNamedSel(proxynewname)!=NULL);
  return SaveNamedSel(proxynewname,sel);
  }

bool ObjToolProxy ::CreateProxy(const char *filename, Matrix4Val trans)
{  
  float pin[3];
  pin[0] = trans.Position()[0];
  pin[1] = trans.Position()[1];
  pin[2] = trans.Position()[2];

  return CreateProxy(filename,pin, &trans.Orientation());
}

bool ObjToolProxy ::CreateProxy(const char *filename, const float *pin, float size)
{  
  Matrix3P scale;  
  scale.SetScale(size,size,size);
  return CreateProxy(filename,pin, &scale);
}

bool ObjToolProxy ::CreateProxy(const char *filename,const float *pin, const Matrix3 * rot)
  {
  int index=NFaces();
  FaceT face;
  face.CreateIn(this);
  face.SetN(3);
  
 // create 3 new points
  int pIndex=ReservePoints(3);
  PosT *np0=&Point(pIndex+0);
  PosT *np1=&Point(pIndex+1);
  PosT *np2=&Point(pIndex+2);
  
  face.SetPoint(0,pIndex);
  face.SetPoint(1,pIndex+1);
  face.SetPoint(2,pIndex+2);
  face.SetNormal(0,0);
  face.SetNormal(1,0);
  face.SetNormal(2,0);
  
  Coord elemSize=1;
  
  if (pin) np0->SetPoint(Vector3P(pin[0],pin[1],pin[2]));
  else np0->SetPoint(Vector3P(0,0,0));
  np0->flags=POINT_SPECIAL_HIDDEN;
  
  if (rot)
  {  
    *np1=*np0;
    (*np1)[0]+= elemSize*2*(*rot)(0,1);
    (*np1)[1]+= elemSize*2*(*rot)(1,1);
    (*np1)[2]+= elemSize*2*(*rot)(2,1);

    *np2=*np0;
    (*np2)[0]+= elemSize*(*rot)(0,2);
    (*np2)[1]+= elemSize*(*rot)(1,2);
    (*np2)[2]+= elemSize*(*rot)(2,2);  
  }
  else
  {
    *np1=*np0,(*np1)[1]+=elemSize*2;
    *np2=*np0,(*np2)[2]+=elemSize;
  }
  
  if( NNormals()<=0 ) NewNormal();
  // select new face
  Selection newsel(this);  
  newsel.PointSelect(pIndex);
  newsel.PointSelect(pIndex+1);
  newsel.PointSelect(pIndex+2);
  newsel.FaceSelect(index);
  if (SelectionToProxy(filename,&newsel)==false) return false;
  UseSelection(&newsel);
  return true;
  }

void ObjToolProxy ::EnumProxies(BTree<ObjMatLibItem>& container, const char *filename, bool partial, const char *basepath) const
  {
  NamedSelection **sel=NULL;
  int count=EnumProxies(NULL, filename, partial);
  sel=(NamedSelection **)alloca(count*sizeof(NamedSelection *));
  EnumProxies(sel, filename, partial);
  for (int p=0;p<count;p++)
    {
    char *name;
    char buff[MAX_PATH];
    int plen=strlen(sel[p]->Name())+1;
    if (plen>MAX_PATH) name=new char[plen];else name=buff;
    GetProxyFilename(sel[p]->Name(),buff);
    ObjMatLibItem item;
    item.name=name;
    if (basepath)
    {
      if (item.name[0]=='\\') item.name=item.name.Mid(1);
      item.name=RString(basepath,item.name)+".p3d";
    }      
    item.status=true;
    if (container.Find(item)==0) container.Add(item);
    if (name!=buff) delete [] name;
    }
  }

int ObjToolProxy::FindProxyFace(const char *proxyName) const
{
  const NamedSelection *proxy=GetNamedSel(proxyName);
  if (proxy==NULL) return -1;
  for (int i=0;i<NFaces();i++) if (proxy->FaceSelected(i)) return i;
  return -1;
}

int ObjToolProxy::FindProxyFace(const NamedSelection *proxy) const
{  
  if (proxy==NULL) return -1;
  for (int i=0;i<NFaces();i++) if (proxy->FaceSelected(i)) return i;
  return -1;
}


void ObjToolProxy::GetProxyTransform(FaceT& fc, Matrix4 &trans, bool noscale)
  {
  
  const ObjectData *obj=fc.GetObject();
  // get proxy object coordinates
  // scan selection
  int pi0=fc.GetPoint(0),pi1=fc.GetPoint(1),pi2=fc.GetPoint(2);
  
  const Vector3 *p0=&obj->Point(pi0);
  const Vector3 *p1=&obj->Point(pi1);
  const Vector3 *p2=&obj->Point(pi2);
  
  float dist01=p0->Distance2(*p1);
  float dist02=p0->Distance2(*p2);
  float dist12=p1->Distance2(*p2);
  
  // p0,p1 should be the shortest distance
  if( dist01>dist02 ) __swap(p1,p2),__swap(dist01,dist02); // swap points 1,2
  if( dist01>dist12 ) __swap(p0,p2),__swap(dist01,dist12); // swap points 0,2
  // p0,p2 should be the second shortest distance
  if( dist02>dist12 ) __swap(p0,p1),__swap(dist02,dist12);
  //  verify results
  
  trans.SetPosition(*p0);
  trans.SetDirectionAndUp((*p1-*p0),(*p2-*p0));
  if (!noscale)
  {
    trans.SetOrientation(Matrix3(MScale,sqrt(dist01))*trans.Orientation());
  }
  }

const char *proxyShowSelName="!Shown proxies";


void ObjToolProxy::ShowProxyContent(bool show, Selection *sel)
{  
  const NamedSelection *showsel=GetNamedSel(proxyShowSelName);
  Selection work(this);
  if (showsel!=0) work=*showsel;

  if (sel==0) sel=&_sel;
  if (!show)
  {
    for (int i=0;i<NFaces();i++) if (sel->FaceSelected(i))
    {
      work.FaceSelect(i,false);
    }
    for (int i=0;i<NPoints();i++) if (sel->PointSelected(i))
    {
      work.PointSelect(i,false);
    }
  }
  else
  {
    int selCount=EnumProxies();
    NamedSelection **selList=(NamedSelection **)alloca(sizeof(NamedSelection *)*selCount);
    EnumProxies(selList);

    for (int i=0;i<NFaces();i++) if (sel->FaceSelected(i))
    {    
      int j;    
      for (j=0;j<selCount;j++) if (selList[j]->FaceSelected(i)) break;
      FaceT fc(*this,i);
      if (j!=selCount) 
      {
        work.FaceSelect(i);
        for (int i=0;i<fc.N();i++) work.PointSelect(fc.GetPoint(i));
      }      
    }
  }
  DeleteNamedSel(proxyShowSelName);
  SaveNamedSel(proxyShowSelName,&work);
}

const NamedSelection *ObjToolProxy::GetShowProxySelection()
{
  const NamedSelection *showsel=GetNamedSel(proxyShowSelName); 
  if (showsel) const_cast<NamedSelection *>(showsel)->Validate();
  return showsel;
}

const NamedSelection *ObjToolProxy::GetProxySelection(FaceT &fc, bool shownonly) const
{
  if (shownonly)
  {
    const NamedSelection *showsel=GetNamedSel(proxyShowSelName);
    if (showsel==0) return 0;
    if (showsel->FaceSelected(fc.GetFaceIndex())==false) return 0;
  }
  
  int selCount=EnumProxies();
  NamedSelection **selList=(NamedSelection **)alloca(sizeof(NamedSelection *)*selCount);
  EnumProxies(selList);

  for (int i=0;i<selCount;i++)
  {
    if (selList[i]->FaceSelected(fc.GetFaceIndex()))
    {
      return selList[i];
    }
  }
  return 0;
}

Pathname ObjToolProxy::GetProxyPathname(const char *proxyName, const char *projectRoot)
{
  char *buff=(char *)alloca(strlen(proxyName)+10);
  GetProxyFilename(proxyName,buff);
  return Pathname(buff,Pathname(projectRoot));
}

void ObjToolProxy::GetAllProxies(Selection &sel)
{
  int p=EnumProxies();
  NamedSelection **ss=(NamedSelection **)alloca(sizeof(NamedSelection *)*p);
  p=EnumProxies(ss);
  for (int i=0;i<p;i++)
  {
    int pp=FindProxyFace(ss[i]);
    if (pp>=0) sel.FaceSelect(pp,true);
  }
  sel.SelectPointsFromFaces();
}
}