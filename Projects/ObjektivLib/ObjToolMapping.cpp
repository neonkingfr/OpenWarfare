#include "common.hpp"
#include "ObjectData.h"
#include "ObjToolMapping.h"
#include "Edges.h"
#include <queue>

namespace ObjektivLib {


ObjToolMapping::NMState ObjToolMapping::NeighbourMapping(const Selection *sel)
{
    if (sel == 0) sel = GetSelection();

    CEdges facegraph(NFaces()),nocross;

    this->BuildFaceGraph(facegraph,nocross);

    AutoArray<int, MemAllocLocal<int,256> > faceindexes;

    int fcnt = 0;
    for (int i = 0;i<NFaces();i++) if (sel->FaceSelected(i)) {
        FaceT fc(this,i);
        if (!fc.HasSomeMapping()) 
        {
            fcnt++ ;
            faceindexes.Add(i);
        }
    }

    if (fcnt == 0)
        return nmsNoUnmaped;

    ProgressBar<int> pb(fcnt);
    for (int i = 0; i < fcnt; i++)
    {
        pb.AdvanceNext(1);

        int pos = findBestNgFace(faceindexes,facegraph,sel);
        if (pos == -1)
            return nmsNoMapped;
        
        calcNeighbourMapping(faceindexes[pos],facegraph,sel);

        faceindexes.Delete(pos,1);
    }

    return nmsOk;
}

int ObjToolMapping::findBestNgFace(Array<int> faces, const CEdges &faceGraph, const Selection *limitFaces)
{
    int bestpos = -1;
    float bestscore = 0;
    for (int i = 0, cnt = faces.Size(); i<cnt; i++)
    {
        FaceT fc(this,faces[i]);
        float score = getNgScore(fc,faceGraph,limitFaces);
        if (score > bestscore)
        {
            bestpos = i;
            bestscore = score;
        }
    }
    return bestpos;
}


static float getUVArea(const FaceT &fc)
{
    float sum = 0;
    for (int i = 2; i<fc.N(); i++) {

        ObjUVSet_Item uv0 = fc.GetUV(0);
        ObjUVSet_Item uv1 = fc.GetUV(i-1);
        ObjUVSet_Item uv2 = fc.GetUV(i);

        float ua = uv1.u - uv0.u;
        float va = uv1.v - uv0.v;
        float ub = uv2.u - uv0.u;
        float vb = uv2.v - uv0.v;
        float v = fabs(ua*vb - va*ub)/2;
        sum+=v;
    }

    return sum;
}

static float getUVAreaSigned(const FaceT &fc)
{
    float sum = 0;
    for (int i = 2; i<fc.N(); i++) {

        ObjUVSet_Item uv0 = fc.GetUV(0);
        ObjUVSet_Item uv1 = fc.GetUV(i-1);
        ObjUVSet_Item uv2 = fc.GetUV(i);

        float ua = uv1.u - uv0.u;
        float va = uv1.v - uv0.v;
        float ub = uv2.u - uv0.u;
        float vb = uv2.v - uv0.v;
        float v = (ua*vb - va*ub)/2;
        sum+=v;
    }

    return sum;
}

static float getUVOptimalArea(const FaceT &fc)
{
    float sum = 0;
    for (int i = 2; i<fc.N(); i++) {

        ObjUVSet_Item uv0 = fc.GetUV(0);
        ObjUVSet_Item uv1 = fc.GetUV(i-1);
        ObjUVSet_Item uv2 = fc.GetUV(i);

        float ua = uv1.u - uv0.u;
        float va = uv1.v - uv0.v;
        float ub = uv2.u - uv0.u;
        float vb = uv2.v - uv0.v;
        float v = sqrt((ua*ua+va*va)*(ub*ub+vb*vb))/2;
        sum+=v;
    }

    return sum;
}

/*
static void MapFace(Matrix4 mx, FaceT &fc, int *vslist)
{
    Vector3 ln1 = fc.GetPointVector<Vector3>(vslist[0]);
    Vector3 ln2 = fc.GetPointVector<Vector3>(vslist[1]);
    Vector3 ln3 = fc.GetPointVector<Vector3>(vslist[2]);

    Vector3 b = ln1;
    Vector3 dir1 = b - ln2;
    Vector3 dir2 = b - ln3;    

    Matrix4 ma;
    ma.SetUpAndAside(dir1,dir2);
    ma.SetPosition(b);

    Matrix4 mb = mx * ma;

    float bu = mb.Get(0,3);
    float bv = mb.Get(1,3);

    fc.SetUV(vslist[0],mb.Get(0,0) + bu, mb.Get(1,0) + bv) ;
    fc.SetUV(vslist[1],mb.Get(0,1) + bu, mb.Get(1,1) + bv);
    fc.SetUV(vslist[2],mb.Get(0,2) + bu, mb.Get(1,2) + bv);
}

static Matrix4 CalcUVTransform(FaceT &fc)
{
    Vector3 ln1 = fc.GetPointVector<Vector3>(0);
    Vector3 ln2 = fc.GetPointVector<Vector3>(1);
    Vector3 ln3 = fc.GetPointVector<Vector3>(2);
    const ObjUVSet_Item *uv0 = fc.GetUV(0);
    const ObjUVSet_Item *uv1 = fc.GetUV(1);
    const ObjUVSet_Item *uv2 = fc.GetUV(2);

    Vector3 b = ln1;
    Vector3 dir1 = b - ln2;
    Vector3 dir2 = b - ln3;    
    
    Matrix4 ma;
    ma.SetUpAndAside(dir1,dir2);
    ma.SetPosition(b);

    Matrix4 mb;
    mb.SetUpAndAside(Vector3(uv1->u - uv0->u,uv1->v - uv0->v,0),Vector3(uv2->u - uv0->u,uv2->v - uv0->v,0));
    mb.SetPosition(Vector3(uv1->u,uv1->v,0));

    Matrix4 mx = mb * ma.Inverted();

    
    return mx;
}



static Vector3P getUpRightDir(FaceT fc, baseVs, nextVs)
{
    int dir = baseVs - nextVs;
    int prevVs = baseVs + dir;
    while (prevVs < 0) prevVs+=fc.N();
    while (prevVs >= fc.N()) prevVs-=fc.N();
    Vector3P baseUV(fc.GetU(baseVs),fc.GetV(baseVs),0);
    Vector3P nextUV(fc.GetU(nextVs),fc.GetV(nextVs),0);
    Vector3P prevUV(fc.GetU(prevVs),fc.GetV(prevVs),0);
    const float fpi = 3.1415926535f;
    float angleUV = arccos((nextUV-baseUV).CosAngle(prevUV-baseUV)) - fpi*0.5f;
    float angleGEO = arccos((fc.GetPointVector<Vector3P>(nextVs) - fc.GetPointVector<Vector3P>(baseVs))
                .CosAngle(fc.GetPointVector<Vector3P>(prevVs) - fc.GetPointVector<Vector3P>(baseVs))) - fpi*0.5f;

    Matrix4 mx(MRotationZ


}
*/

static Matrix3 CalculateUVTransform(const Vector3P &refpt, const ObjUVSet_Item &uv,
                                    const Vector3P &refpt2, const ObjUVSet_Item &uv2,
                                    const Vector3P &nxpt3, const ObjUVSet_Item &uv3,
                                    const Vector3P &normal)
                                     
{
    Vector3P dir1 = refpt2 - refpt;
    Vector3P dir2 = nxpt3 - refpt;
    
    float cosang = dir1.CosAngle(dir2);
    float sinang = dir1.SinAngle(dir2, normal);    


    Vector3P fkdir1(dir1.Size(),0,0);
    Vector3P fkdir2(dir2.Size() * cosang, dir2.Size() * sinang, 0);

    Vector3P refuv(uv.u,uv.v,0);
    Vector3P diruv1(uv2.u - uv.u, uv2.v - uv.v,0);
    Vector3P diruv2(uv3.u - uv.u, uv3.v - uv.v,0);

    Matrix3 ma(fkdir1[0],fkdir2[0],0,fkdir1[1],fkdir2[1],0,0,0,1);
    Matrix3 mb(diruv1[0],diruv2[0],refuv[0],diruv1[1],diruv2[1],refuv[1],0,0,1);
    Matrix3 mx = mb * ma.InverseGeneral();

    return mx;
}

static ObjUVSet_Item GetUVFromTransform(const Vector3P &refpt,
                                    const Vector3P &refpt2, 
                                    const Vector3P &nxpt3,
                                    const Vector3P &normal,
                                    const Matrix3 &mx)
{

    Vector3P dir1 = refpt2 - refpt;
    Vector3P dir2 = nxpt3 - refpt;

    float cosang = dir1.CosAngle(dir2);
    float sinang = dir1.SinAngle(dir2, normal);    

    Vector3P fkdir1(dir1.Size(),0,0);
    Vector3P fkdir2(dir2.Size() * cosang, dir2.Size() * sinang, 1);

    Vector3P rx(VMultiply,mx,fkdir2);
    return ObjUVSet_Item(rx[0],rx[1]);
}

static Matrix3 CalculateUVTransform(const FaceT &fc, int ref1, int ref2)
{
    int pt3 = ref2 + (ref2 - ref1);
    while (pt3 < 0) pt3 += fc.N();
    while (pt3 >= fc.N()) pt3 -= fc.N();

    return CalculateUVTransform(fc.GetPointVector<Vector3P>(ref1),fc.GetUV(ref1),
                                fc.GetPointVector<Vector3P>(ref2),fc.GetUV(ref2),
                                fc.GetPointVector<Vector3P>(pt3),fc.GetUV(pt3),
                                fc.CalculateRawNormal());
}

static void UpdateUVInPt(FaceT &fc, int ref1, int ref2, int req,const Matrix3 &mx)
{
    Vector3P refpt1 = fc.GetPointVector<Vector3P>(ref1);
    Vector3P refpt2 = fc.GetPointVector<Vector3P>(ref2);
    Vector3P reqpt = fc.GetPointVector<Vector3P>(req);
    ObjUVSet_Item uv = GetUVFromTransform(refpt1,refpt2,reqpt,fc.CalculateRawNormal(),mx);
    fc.SetUV(req,uv.u,uv.v);
}


bool ObjToolMapping::calcNeighbourMapping(int face, const CEdges &faceGraph, const Selection *limitFaces)
{
    FaceT fc(this,face);
    bool maptex = !fc.GetTexture().IsEmpty();
    bool mapmat = !fc.GetMaterial().IsEmpty();
    Matrix3 lastKnownTransform;
    Matrix3 lastNbTransform;
    int lastRef1,lastRef2;
    float maxopt = FLT_MAX;    
    int sign = 0;


    bool vertices[4] = {false,false,false,false};
    bool anyneighb = false;
    for (int i = 0, k = 0; (k = faceGraph.GetEdge(fc.GetFaceIndex(),i))>=0; i++) 
    if (limitFaces == 0 || limitFaces->FaceSelected(k))
    {
        FaceT fc2(this,k);
        if (fc2.HasSomeMapping()) 
        {            
            anyneighb = true;
            int ix[2][2];
            fc.GetEdgeIndices(fc2,ix[0][0],ix[0][1],ix[1][0],ix[1][1]);
            fc.SetUV(ix[0][0],fc2.GetU(ix[1][0]),fc2.GetV(ix[1][0]));
            fc.SetUV(ix[0][1],fc2.GetU(ix[1][1]),fc2.GetV(ix[1][1]));
            vertices[ix[0][0]] = true;
            vertices[ix[0][1]] = true;
            if (!maptex && !fc2.GetTexture().IsEmpty())
                fc.SetTexture(fc2.GetTexture());
            if (!mapmat && !fc2.GetMaterial().IsEmpty())
                fc.SetMaterial(fc2.GetMaterial());
            Matrix3 transform = CalculateUVTransform(fc2,ix[1][0],ix[1][1]);
            Vector3 a1 = transform.DirectionAside();
            Vector3 a2 = transform.DirectionUp();
            float opt = fabs(a1.CosAngle(a2));
            if (opt < maxopt) 
            {
                lastRef1 = ix[0][0];
                lastRef2 = ix[0][1];
                lastNbTransform = lastKnownTransform = transform;
                maxopt = opt;                
            }
            float ss = getUVAreaSigned(fc2);
            if (ss<0) sign--;
            else if (ss>0) sign++;
        }   
    }


    if (!anyneighb) return false;

    int usedPts = 0;
    for (int i = 0; i < fc.N(); i++) if (vertices[i]) usedPts++;
    if (usedPts != fc.N())
    {
        /*
        if (usedPts == 3)
        {
            
          for (int i = 0; i < fc.N(); i++)
                if (vertices[i] == false) 
                {
                    int v1 = (i + 1) % fc.N();
                    int v2 = (v1 + 1) % fc.N();
                    lastRef1 = v1;
                    lastRef2 = v2;
                    lastKnownTransform = CalculateUVTransform(fc,lastRef1,lastRef2);
                }
        }*/

        

        for (int i = 0; i < fc.N(); i++)
            if (vertices[i] == false) 
                UpdateUVInPt(fc,lastRef1,lastRef2,i,lastKnownTransform);
    }

    if (sign < 0) sign = -1;
    else if (sign > 0) sign = 1;

    float ressig = getUVArea(fc);
    if ((ressig < 0 && sign>=0) || (ressig > 0 && sign !=0) || fabs(ressig) < 0.00001)
    {
        for (int i = 0; i < fc.N(); i++)
                UpdateUVInPt(fc,lastRef1,lastRef2,i,lastNbTransform);        
        return true;
    } else
        return false;
}

float ObjToolMapping::getNgScore(FaceT &fc, const CEdges &faceGraph, const Selection *limitFaces)
{
    float countNg = 0;
    float sumangles = 0;
    Vector3P rawNorm = fc.CalculateRawNormal();
    Vector3P normal = rawNorm;
    float normSz = normal.NormalizeSize();
    float refSz = (float)fc.CalculatePerimeter();
    for (int i = 0, k = 0; (k = faceGraph.GetEdge(fc.GetFaceIndex(),i))>=0; i++) 
    if (limitFaces == 0 || limitFaces->FaceSelected(k))
    {
        FaceT fc2(this,k);
        if (fc2.HasSomeMapping()) 
        {            
            Vector3 normal2=fc2.CalculateNormal();
            sumangles += 1 - (normal.CosAngle(normal2));

            int ix[2][2];
            fc.GetEdgeIndices(fc2,ix[0][0],ix[0][1],ix[1][0],ix[1][1]);

            Matrix3 transform = CalculateUVTransform(fc2,ix[1][0],ix[1][1]);
            Vector3 a1 = transform.DirectionAside();
            Vector3 a2 = transform.DirectionUp();
            float ng = 2 - fabs(a1.CosAngle(a2));
            countNg = countNg + ng;

        }
    }

    if (countNg > 0)
        return 20000.0f * (countNg +( 4 - fc.N() )) - sumangles * 100 + normSz / refSz * 10;
    else
        return 0;
}


void ObjToolMapping::MapFaceDefault(FaceT &fc) {

    fc.SetUV(0,0,0);
    fc.SetUV(1,1,0);    
    std::pair<float,float> a1 = fc.CalcAngleAtPointRaw(0);
    fc.SetUV(fc.N()-1,a1.second,a1.first);
    if (fc.N()>3) {
        std::pair<float,float> a2 = fc.CalcAngleAtPointRaw(1);        
        fc.SetUV(2,1.0f - a1.second,a1.first);
    }
    
}

static float getUVPerimeter(FaceT &fc) {

    float sum = 0;
    ObjUVSet_Item st = fc.GetUV(fc.N() - 1);
    for (int i = 0; i< fc.N(); i++) {
        ObjUVSet_Item nx = fc.GetUV(i);
        sum += sqrt((st.u - nx.u)*(st.u - nx.u) + (st.v - nx.v)*(st.v - nx.v));
        st = nx;
    }
    return sum;
        


}

void ObjToolMapping::AutoMapping(AutoArray<int> *mappingGroups) {

    CEdges facegraph(NFaces()),nocross;
    this->BuildFaceGraph(facegraph,nocross);
    Selection sel(this);
    AutoArray<int> bestSeq, workSeq;

    for (int i = 0; i < NFaces(); i++) if (!sel.FaceSelected(i)) {

        CEdges probenEdges(NFaces());
        workSeq.Clear();
        Selection placed(this);
        std::queue<int> placeQueue;
        placeQueue.push(i);    
        bool firstface = true;
        while (!placeQueue.empty()) {
            int top = placeQueue.front();
            placeQueue.pop();
            if (!placed.FaceSelected(top)) {
                
                bool anyprocessed = false;
                for (int i = 0, nf = facegraph.GetEdge(top,i); nf != -1;
                                nf = facegraph.GetEdge(top,++i))
                    if (!placed.FaceSelected(nf) && 
                        !sel.FaceSelected(nf) && 
                        !probenEdges.IsEdgeInDirection(top,nf)) {
                        placeQueue.push(nf);                        
                        probenEdges.SetEdge(top,nf);
                    }
                    else
                        anyprocessed = true;

                if (anyprocessed) {                    
                    calcNeighbourMapping(top,facegraph,&placed);
                    FaceT fc(this,top);
                    if (getUVPerimeter(fc)>getUVArea(fc) && 
                                    !isInCollision(workSeq,top)) {
                            workSeq.Add(top);
                            sel.FaceSelect(top);
                            placed.FaceSelect(top);
                    }
                }
                else {
                    if (firstface) {
                        workSeq.Add(top);
                        sel.FaceSelect(top);
                        placed.FaceSelect(top);
                        MapFaceDefault(FaceT(this,top));        
                    }
                   firstface = false;
                }
            }

        }
        if (mappingGroups) {
            if (mappingGroups->Size() != 0) mappingGroups->Add(-1);
            mappingGroups->Append(workSeq);            
        }
    }


        


}
        

bool ObjToolMapping::isInCollision(const Array<int> &facesPlaced, int faceToTest) const {
    FaceT testFc(this,faceToTest);
    for (int i = 0; i < facesPlaced.Size(); i++)
        if (FaceT(this,facesPlaced[i]).IsUVCollision(testFc)) return true;
    return false;
}

}
typedef AutoArray<int> FaceGroup;
typedef AutoArray<FaceGroup> GroupList;
TypeIsMovable(FaceGroup);

namespace ObjektivLib {

  class UVTransform
  {
    float a11,a12,a21,a22,a31,a32;
  public:
    UVTransform(Matrix3 &mx): a11(mx(1,1)),a12(mx(2,1)),
      a21(mx(1,2)),a22(mx(2,2)),
      a31(mx(1,3)),a32(mx(2,3)) {}

    UVTransform(float x, float y, float cx, float cy):a11(cx),a22(cy),a12(0),a21(0),
      a31(x),a32(y) {}

    UVTransform():a11(1),a22(1),a12(0),a21(0),a31(0),a32(0) {}

    UVTransform(float a11,float a12, float a21, float a22, float a31, float a32):
    a11(a11),a12(a12),a21(a21),a22(a22),a32(a32),a31(a31) {}

    float GetU(float u,float v) const {return u*a11+v*a21+a31;}
    float GetV(float u,float v) const {return u*a12+v*a22+a32;}
    float GetUNT(float u,float v) const {return u*a11+v*a21;}
    float GetVNT(float u,float v) const {return u*a12+v*a22;}

    UVTransform operator*(const UVTransform &other) const
    {
      return UVTransform(other.GetUNT(a11,a12),other.GetVNT(a11,a12),
        other.GetUNT(a21,a22),other.GetVNT(a21,a22),
        other.GetU(a31,a32),other.GetV(a31,a32));
    }
  };



template<class ArrType>
class BySize {

public:
    int operator()(const ArrType *a, const ArrType *b) const {
        return (a->Size()>b->Size()) - (a->Size()<b->Size());
    }
};

static void RealizeTransform(ObjectData &mesh, const Array<int> &atlas, 
                             const UVTransform &subtransform) 
{
    
    float minu = FLT_MAX, minv = FLT_MAX, maxu = -FLT_MAX, maxv = -FLT_MAX;
    for (int j = 0; j < atlas.Size(); j++) {

        FaceT fc(mesh, atlas[j]);
        for (int v = 0; v < fc.N(); v++) {
            ObjUVSet_Item uv = fc.GetUV(v);
            if (uv.u < minu) minu = uv.u;
            if (uv.v < minv) minv = uv.v;
            if (uv.u > maxu) maxu = uv.u;
            if (uv.v > maxv) maxv = uv.v;
        }

    }

    minu-=0.1f;
    minv-=0.1f;
    maxu+=0.1f;
    maxv+=0.1f;

    float origszu = maxu - minu,
          origszv = maxv - minv;
    UVTransform orig  (-minu/origszu,-minv/origszv,1.0f/origszu, 1.0f/origszv);
    UVTransform fintrn = orig * subtransform;

    for (int j = 0; j < atlas.Size(); j++) {
        
        FaceT fc(mesh, atlas[j]);
        for (int vs = 0; vs < fc.N(); vs++) {

            ObjUVSet_Item uv = fc.GetUV(vs);
            float uu = fintrn.GetU(uv.u, uv.v);
            float vv = fintrn.GetV(uv.u, uv.v);

            fc.SetU(vs,uu);
            fc.SetV(vs,vv);

        }

    }        

}

static void SplitAndTransform(ObjectData &mesh, const Array<const FaceGroup> &atlas, 
                const UVTransform &subtransform = UVTransform(),
                bool vsplit = false) 
{

    if (atlas.Size()>1) {
        int sum = 0, part = 0;
        int i,cnt;
        for (i = 0, cnt = atlas.Size(); i < cnt; sum += atlas[i++].Size());
        for (i = 0, part = 0; part < sum ; part+=atlas[i++].Size()*2);  
        if (i == cnt) i--;
        if (vsplit) {
            SplitAndTransform(mesh, Array<const FaceGroup>(atlas.Data(), i),
                UVTransform(0.0f,0.0f,0.5f,1.0f)*subtransform,!vsplit);
            SplitAndTransform(mesh, Array<const FaceGroup>(atlas.Data() + i, atlas.Size() - i),
                UVTransform(0.5f,0.0f,0.5f,1.0f)*subtransform,!vsplit);
        } else {
            SplitAndTransform(mesh, Array<const FaceGroup>(atlas.Data(), i),
                UVTransform(0.0f,0.0f,1.0f,0.5f)*subtransform,!vsplit);
            SplitAndTransform(mesh, Array<const FaceGroup>(atlas.Data() + i, atlas.Size() - i),
                UVTransform(0.0f,0.5f,1.0f,0.5f)*subtransform,!vsplit);
        }        
    } else {

        RealizeTransform(mesh,atlas[0],subtransform);

    }

}




void ObjToolMapping::AutoMappingAndArrange() {

    AutoArray<int> groups;
    AutoMapping(&groups);
    int beg, end;
    GroupList list;

    beg = -1;
    do {
        beg++;
        end = beg;
        while (end < groups.Size() && groups[end] != -1) end++;
        if (end != beg)
            list.Add(FaceGroup(groups.Data() + beg, end - beg));
        beg = end;
    } while(beg < groups.Size());

   SplitAndTransform(*this,list);



}



}




