#ifndef _G3DSYS_H_
#define _G3DSYS_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _tagG3dMsState
  {
  int cursor;
  int msx;
  int msy;
  int hotx,hoty;
  TSCREENCLIP clip;
  HTXITABLE txitable;
  char tl1,tl2,tl3;
  char dtl1,dtl2,dtl3;
  char flags;
  int counter;
  int wheelcounts;
  int event;
  int msdefx,msdefy;
  }TMSSTATE,*HMSSTATE;

typedef void (*IDLEPROC)(void);


HWND CreateG3DWindow(HINSTANCE hInst,WNDPROC WinProc,const char *title, const char *icon);
int g3dAppRun(IDLEPROC idleproc,WNDPROC wndproc);
int g3dGetAppLevel();
int g3dNotifyMouse( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
void g3dExitAppLevel(int retvalue);

void g3dMouseDisplay();
void g3dMouseCursor(int handle,char background);
void g3dMouseHide();
void g3dMouseShow();
void g3dMouseInit(HTXITABLE cursortable,int cursor);
void g3dGetMouseStatus(TMSSTATE *msstate);
void g3dSetMouseStatus(TMSSTATE *msstate);
void g3dSetMouseMode(int mode);

#define G3DBG_NOCHANGE -1  //kurzor nebo background se nemeni
#define G3DBG_SAVE     1  //pozadi se ma ukladat
#define G3DBG_DONTSAVE 0  //pozadi se nema ukladat

#define G3DA_NOIDLE ((IDLEPROC)1)
#define G3DA_PREVIOUS NULL

#define G3DMM_NORMAL 1
#define G3DMM_RELATIVE 2

#define G3DMS_MOVE 0x1
#define G3DMS_TL1D 0x2
#define G3DMS_TL1U 0x4
#define G3DMS_TL2D 0x8
#define G3DMS_TL2U 0x10
#define G3DMS_TL3D 0x20
#define G3DMS_TL3U 0x40
#define G3DMS_DTL1 0x80
#define G3DMS_DTL2 0x100
#define G3DMS_DTL3 0x200
#define G3DMS_WHEELF 0x400
#define G3DMS_WHEELB 0x800


#ifdef __cplusplus
  }
#endif

#endif
