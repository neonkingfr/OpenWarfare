#ifndef _G3D_DEV_H_
#define _G3D_DEV_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _MSC_VER
#define G3D_DECL     __cdecl
#else
#define G3D_DECL     _cdecl
#endif


#define IMPORT(retval,function,params) \
  typedef retval(G3D_DECL *_g3df_##function)params;\
  extern _g3df_##function function;
//for DLL use;
#define __G3D_DEF_IMPORT

#define G3D_RGBA(r, g, b, a) \
    ((((unsigned long)((a) * 255)) << 24) | (((unsigned long)((r) * 255)) << 16) | \
    (((unsigned long)((g) * 255)) << 8) | (unsigned long)((b) * 255))
#define G3D_RGB(r, g, b) \
    (0xFF000000 | (((unsigned long)((r) * 255)) << 16) | \
    (((unsigned long)((g) * 255)) << 8) | (unsigned long)((b) * 255))

#define G3D_RGB16(r, g, b) (((unsigned short)((r) * 32)<<11) | \
                            ((unsigned short)((g) * 64)<<5) | \
                            ((unsigned short)((b) * 32)))


#define G3D_OK 0
#define G3D_NOMEMORY -1
#define G3D_SCENEERROR -2
#define G3D_DISKERROR -3
#define G3D_OBJECTNOTFOUND -4
#define G3D_DISKOBJECTERROR -5
#define G3D_SAVEERROR -6    //Ukladaci chyba (ne diskova!)
#define G3D_CLIPFAILED -7  //selhaly funkce getclip/putclip;
#define G3D_INVALIDCOORDS -8 //souradnice clipu jsou chybne.
#define G3D_INVALIDPARAMS -9

#define G3D_SAVEAPPENDFILE 0
#define G3D_SAVECREATEFILE 1
//primitives
#define G3D_TRIANGLE 1  //kresli trojuhelniky. Pocet musi byt delitelny 3
#define G3D_STRIP 2  //kresli polygon definovany jako STRIP /\/\/\ ..
#define G3D_FAN 3    //kresli polygon definovany jako FAN   \|/
#define G3D_POLYGON 4  //kresli konvexni polygon.  \_/
#define G3D_LPOINT 5   //kresli svetelny bod nebo efekt. tu,tv udavaji velikost v bodech obrazovky.
                       //Transformuji se pouze souradnice.
#define G3D_LINE 6     //Kresli caru
#define G3D_STREAMLIGHT 7 //Kresli streamlight
#define G3D_BILLBOARD 8 //Kresli billboard
#define G3D_OPTICEFF 9  //Kresli opticky effekt (lensflare);
#define G3D_BONE 10     //Nekresli nic. Je to kost
#define G3D_KNEE 11     //Nekresli nic. Je to koleno
#define G3D_MIRRORING 16 //Textura se bude pri opakovani zrcadlit.
#define G3D_OPTIALWAYS 32 //Kresli opticky effekt i kdyz neni videt
#define G3D_DOUBLESIDE 64 //Vypina culling behem kresleni

typedef struct
  {
  HVECTOR coord;
  unsigned long color;
  float tv,tu;
  }G3D_TLVERTEX;

typedef struct _hw_info
  {
  char param1;
  short param2;
  long param4;
  char *desc;
  void *lpVoid;
  }G3D_HWINFO;


IMPORT(void,g3dSwap,(int wait));
//Prohazuje BackBuffer s FrontBufferem;
//Pro bezchybne prohozeni je nutne aby wait byl vetsi nez 0; (cekani na retrace)

IMPORT(void,g3dClearScreen,(float red,float green,float blue));
//Maze obrazovku urcenou barvou;

IMPORT(char,g3dInitHardware3D_,(unsigned long handle,int xres,int yres,int cols,int freq));
//Inicializuje 3D kartu v urcenym rozlyseni;
//vraci 1 pokud nastala chyba;
IMPORT(void,g3dResetHardware3D,());
//Nastavuje 3D pro vyuziti knihovnou G3D.

IMPORT(void,g3dCloseHardware3D_,());

char g3dInitHardware3D(unsigned long handle,int xres,int yres,int cols,int freq);
void g3dCloseHardware3D(void);

IMPORT(int,g3dSetCullMode,(int mode));
//Nastavuje zpusob kresleni ploch.
//mode<0 nekresli se plochy ktere maji zapornou normalu
//mode>0 nekresli se plochy ktere maji kladnou normalu (default)
//mode=0 kresli se vse.
//POZOR!. Osa Z je kladna ve smeru dovnitr. Plochy ktere sem maji nakreslit
//by meli mit normalu ven z obrazovky, proto je default nastaven na 1;
IMPORT(char,g3dEnableZBuffer,(char enable));
//je-li enable nastaveno na 1 (default), pak se provadi porovnavani v Zbufferu
//je-li enable nastaveno na 0 pak se porovnavani neprovadi a
// na obrazovku se kresli vse. Nicmene, hloubka se stale zaznamenava.
IMPORT(void,g3dFinish,());
//Tato funkce prinuti hardware, aby dokoncilo veskere graficke operace.
//Rizeni bude pozdrzeno tak dlohou, dokud se tak nestane.
//GLIDE EQUIVALENT: grFinish();
IMPORT(void,g3dDrawVertexArray,(int primtype, int count, G3D_TLVERTEX *primitiv));
//Kresli objekt. primtype je typ objektu, count je pocet vrcholu a primitiv
//je ukazatel na pole ukazatelu na vertexy.
IMPORT(void,g3dUnload,());


IMPORT(void,g3dClipWindow,(unsigned long wleft,unsigned long wtop,unsigned long wright,unsigned long wbottom));

IMPORT(int,g3dQueryHardware,(G3D_HWINFO **hw_table));
//Funkce zjistuje typ nainstalovaneho hardware.
// vraci pocet zarizeni a ukazatel na tabulku informaci o hardware
//Hodnoty v tabulkach jsou zavisle od pouziteho systemu
//Ukazatel na desc identifikuje zarizeni.
//vrati-li hodnotu 0 pak nelze G3D instalovat na tomto hardware.

IMPORT(char,g3dSelectHardware,(G3D_HWINFO *selected));
//Funkce vybira urcene hardware jako zakladni.
//Pokud neni volana, nastavuje se hardware prvni ze seznamu g3dQueryHardware.
//vraci 0 pokud vybrani hardware bylo uspesne.
IMPORT(char,g3dQueryVertexFormat,()); //Funkce vraci zapis vertexu pro renderovani
//Tuto funkci vola G3D aby zjistilo, jakym zpusobem ma prepocitavat
//jednotlive vertexy
#define G3D_CONV_TUV 1     //Engine ma konverovta TU a TV souradnice
#define G3D_VERTEX_XYZW 2  //Vertex je pozadovan v zakladnim tvaru
#define G3D_VERTEX_XYZQ 4   //Vertex je pozadovan v upravenem tvaru x/w,y/w,z/w,1/w
IMPORT(float,g3dGetZBufferValue,(int x,int y, char flags));
#define G3D_ZBWAIT 1
//Vraci hodnotu Z na souradnicich x a y.
//flags = G3D_ZBWAIT - funkce pocka na dokonceni kresby

IMPORT(char,g3dIsSuspended,(void));

IMPORT(char,g3dSetRenderTarget,(char buffer));
#define G3D_FRONTBUFFER 0x0
#define G3D_BACKBUFFER 0x1
#define G3D_CURBUFFER 0x2

IMPORT(int,g3dQuerySettingInt,(int value));

#define G3DQ_MAXX 1
#define G3DQ_MAXY 2
#define G3DQ_SCRSIZEX 3
#define G3DQ_SCRSIZEY 4
#define G3DQ_CLIPLEFT 5
#define G3DQ_CLIPRIGHT 6
#define G3DQ_CLIPTOP 7
#define G3DQ_CLIPBOTTOM 8
#define G3DQ_ZMIN 9
#define G3DQ_ZMAX 10
#define G3DQ_DISPLAYMODE 11   
//flags form G3DQ_DISPLAYMODE
#define G3DQD_FULLSCREEN 0
#define G3DQD_WINDOWED 1
#define G3DQ_RCLEFT 12
#define G3DQ_RCRIGHT 13
#define G3DQ_RCTOP 14
#define G3DQ_RCBOTTOM 15


//------------- getting clips ----//

typedef struct _tscreenclip
  {
  int orgx,orgy;
  int sizx,sizy;
  int format;
  int stridex;
  long texhandle;
  void *data;
  }TSCREENCLIP;

IMPORT(char,g3dNewClip,(TSCREENCLIP *clip, int x1, int y1, int x2, int y2,int flags));
IMPORT(char,g3dUpdateScreenClip,(TSCREENCLIP *clip,int buffer));
IMPORT(char,g3dPutScreenClip,(int command,...));
IMPORT(char,g3dDestroyClip,(TSCREENCLIP *clip));

#define g3dIsClipEmpty(clip) ((clip)==NULL || (clip)->data==NULL)
#define g3dSetEmptyClip(clip) ((clip)->data=NULL)

#define G3DGC_FRONTBUFFER 0x0
#define G3DGC_BACKBUFFER 0x1

#define G3DPC_ORGXY 0x1
#define G3DPC_CHROM 0x2
#define G3DPC_ZCOMP 0x3
#define G3DPC_BUFFER 0x4
#define G3DPC_NOPARAMS 0x0

#define g3dGetScreenClipEx(clip,x,y,xs,ys,buffer) g3dGetScreenClip(clip,x,y,(x)+(xs)-1,(y)+(ys)-1,buffer)

IMPORT(char,g3dConvertClip,(TSCREENCLIP *clip,long flags, void *tostore));
IMPORT(long,g3dGetConvClipSize,(TSCREENCLIP *clip,long flags));

IMPORT(char,g3dCreateClip,(TSCREENCLIP *clip, TUNIPICTURE *uni, int reduce,int flags));

#define G3DCC_SYSTEMMEMORY 1
#define G3DCC_VIDEOMEMORY 2
#define G3DCC_NOFLAGS 0


#define G3DCC_CONV565 0       //konvertuje na 565
#define G3DCC_CONV888 1       //konvertuje na 888
#define G3DCC_WINBITMAP (2|G3DCC_CONV888)
                              //uklada jako windows bitmapu
#define G3DCC_BITMAPFILE (4|G3DCC_WINBITMAP)
                              //pripojuje vsechny hlavicky bitmapy pro ulozeni do souboru

#include <assert.h>

#define g3dDebugPoint(name) \
    do                          \
      {                         \
      int name=0;               \
      g3dDebugPointSupport();   \
      assert(name);             \
      }                         \
    while (0)
#define g3dBreakPoint(name) g3dDebugPoint(name)

IMPORT(void,g3dDebugPointSupport,());

IMPORT(char,g3dSpawnToWindow,());
IMPORT(char,g3dSpawnBack,());

IMPORT(void,g3dSwitchToGDI,(char mode));

#define g3dRunGDI(funct) do \
                  { \
                  if (!g3dSpawnToWindow())\
                    { \
                    (funct);  \
                    g3dSpawnBack(); \
                    } \
                  } \
                  while (0) \

#define g3dRunGDI2(funct) do \
                  { \
                  g3dSwitchToGDI(1); \
                  (funct);  \
                  g3dSwitchToGDI(0);\
                  } \
                  while (0) \


IMPORT(long,g3dGetStat,(int index));
IMPORT(long,g3dClearStat,(int index));

IMPORT(void,g3dEnableFog,(char mode, unsigned long color, float parm1, float parm2));
IMPORT(int,g3dLoadFogTable,(unsigned char *fogtable));
IMPORT(float,g3dGetFogW,(int fogindex));
//if fogtable is NULL, function return number of entries in fogtable

#define G3D_FOGDISABLE 0
#define G3D_FOGLINEAR 1
#define G3D_FOGEXP 2
#define G3D_FOGEXP2 3
#define G3D_FOGTABLE 4

#include "g3dmc.h"

//-------------------- G3DTEX2 imports ---------------------


#include "colman.h"

#define TEXTUREHASH 1001


typedef struct _tagG3dTextureInfo
  {
  long handle;
  int xsize;
  int ysize;
  int reducefactor;
  char loadstatus;  
  }TG3DTEXINFO;


IMPORT(int,g3dtInitTexManager,());
IMPORT(int,g3dtDoneTexManager,());
IMPORT(long,g3dtRegisterTexture,(TUNIPICTURE *pic));
IMPORT(long,g3dtUnregisterTexture,(long handle));
IMPORT(long,g3dtSelectTexture,(int tmu, long handle));
IMPORT(void,g3dtFlushBuffers,(void));
IMPORT(void,g3dtSetTexMax,(int xs, int ys));
IMPORT(char,g3dDirectToScreen,(TUNIPICTURE *pic, int x, int y, int buffer, int reduce));
IMPORT(int,g3dtGetTexInfo,(long handle,TG3DTEXINFO *info));
IMPORT(long,g3dtRegisterClip,(TSCREENCLIP *clip));

IMPORT(char,g3dtEnableMipMapGen,(char mode));
#define G3DTM_OBTAIN 0	//obtain current mipmap generation level
#define G3DTM_DISABLE 1	//disable generating mipmaps
#define G3DTM_NEAREST 2	//generate nearest mipmaps
#define G3DTM_LINEAR 3	//generate linear mipmaps (is hardware supported this)

IMPORT(char,g3dtMipMapMax,(char maxcount));
#define G3DTMM_OBTAIN 0	//obtain current mipmap max count

#define G3DTI_TEXLOADED 1
#define G3DTI_TEXNOTLOADED 0
#define G3DTI_DONTKNOWN 2

//------------- NEW INTERFACE --------------------
#define G3DDM_OLDVERTEX 1
#define G3DDM_TUV2 2
#define G3DDM_TUV3 3
#define G3DDM_TUV4 4
#define G3DDM_TUV5 5
#define G3DDM_TUV6 6
#define G3DDM_TUV7 7
#define G3DDM_TUV8 8
#define G3DDM_SPECULAR 0x10
#define G3DDM_DIFFUSE 0x20
#define G3DDM_XYZQ 0x0  //transformed vertices
#define G3DDM_XYZW 0x40 //untransformed vertices

typedef struct _tagG3D_TUV
  {
  float tu,tv;
  }G3D_TUV;

typedef struct _tagG3D_TLVERTEX2
  {
  HVECTOR coord;
  unsigned long color;
  unsigned long specular;
  G3D_TUV tuv[8];
  }G3D_TLVERTEX2;

IMPORT(int,g3dDrawPrimitive,(int mode, int primtype, G3D_TLVERTEX2 *data, int vcount));



#include "g3dmodul.h"

#undef IMPORT
#undef __G3D_DEF_IMPORT


// ------------ G3D_DEV intefrace ---------------------

char g3dInitDevice(const char *devicename);
void g3dFreeDevice();
char g3dRegisterModule(const char *modulename);
void g3dInitModules();
char g3dIsImplemented(void *function);



#ifdef __cplusplus
}
#endif
#endif
