// GWin.h: interface for the CGWin class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GWIN_H__6A501924_07BB_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_GWIN_H__6A501924_07BB_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <windows.h>
#include <stdio.h>
#include "..\g3dlib.h"
#include "GBase.h"
#include "Messager.h"



#define GFD_NODRAW 0
#define GFD_SOMECHILDS 1
#define GFD_FULLREDRAW 2

#define CGWF_OVERLAPPED 0x0
#define CGWF_POPUP 0x1
#define CGWF_CHILD 0x2
#define CGWF_BORDERPARENTBGR 0x4
#define CGWF_BORDERPAINTCOLOR 0x8
#define CGWF_BORDER50ALPHA 0x10
#define CGWF_FLAT 0x20
#define CGWF_ABSOLUTEPOSX 0x40
#define CGWF_ABSOLUTEPOSY 0x80
#define CGWF_ABSOLUTEPOS (CGWF_ABSOLUTEPOSX|CGWF_ABSOLUTEPOSY)
#define CGWF_CENTERPARENT 0x100
#define CGWF_CENTERMOUSE 0x200
#define CGWF_AUTORESIZEX 0x400
#define CGWF_AUTORESIZEY 0x800
#define CGWF_RAISED 0x1000
#define CGWF_SUNKED 0x2000
#define CGWF_RAISEDFRAME 0x4000
#define CGWF_SUNKEDFRAME 0x8000
#define CGWF_CENTERSCREEN 0x10000
#define CGWF_VISIBLE 0x20000
#define CGWF_ENABLED 0x40000
#define CGWF_TOPMOST 0x80000
#define CGWF_CLEARVIEW 0x100000
#define CGWF_MOUSECAPTURE 0x200000
#define CGWF_ALIGNRIGHT 0x400000
#define CGWF_ALIGNBOTTOM 0x800000

#define CGWN_BEFOREENTER 100
#define CGWN_AFTERENTER 101
#define CGWN_BEFOREEXIT 102
#define CGWN_AFTEREXIT 103
#define CGWN_BEFOREUPDATE 104
#define CGWN_AFTERUPDATE 104
#define CGWN_ONCHANGE 105

enum CGWDEF
  {
  NextWin,
  PrevWin,
  NextIdWin,
  PrevIdWin,
  ParentWin,
  ChildWin,
  DependedWin,
  DesktopWin,
  DrawParent,
  FirstWin,
  FirstIdWin,
  LastWin,
  LastIdWin
  };

class CGWin : public CGMsgLink  
  {
  protected:
	G3DCONTEXT winctx;
	int winid;
	unsigned long flags;
	CGWin *next;
	CGWin *childwin;
	CGWin *dependedwin;
	CGWin *focus;
	CGWin *parent;
	char forceredraw;
	float zorder;
	float zmin;	
	unsigned char bordersize;
	unsigned char borderface;
	int mouseid;
  public:
	  CGWin *operator[] (int winid) {return FindWindow(winid);}
	  int operator= (int id) {return winid=id;}
	  int operator() (void) {return winid;}
	  int GetID() {return winid;}
	  bool HasFocus();
	  void SetCapture(bool set);
	  bool NeedRepaint() {return forceredraw!=GFD_NODRAW;}
	  CGWin *FindWindow(int winid);
	  CGWin *GetWindow(CGWDEF def);
	  bool SetFocus(CGWin *to);
	  bool SetFocus();
	  bool OnNotify(CGWin *child, CGMessage *msg);
	  void SetForeground();
	  void Enable(bool enable);
	  void SetVisible(bool visible);
	  void RecalcZ();
	  // Virtual functions
	  virtual bool Activate();
	  virtual void MouseActivate(TMSSTATE *mst);
	  virtual CGWin *MouseHitTest(int x, int y);
	  virtual void OnMouseEvent(TMSSTATE *msev);
	  virtual void OnChar(int nChar, int nRepCnt, int nFlags);
	  virtual void OnKeyUp(int vKey);
	  virtual void OnKeyDown(int vKey, long flags);
	  virtual void OnResize(int sx,int sy);
	  virtual void OnMove(int rx,int ry);
	  virtual void OnDraw();
	  virtual void OnDrawBorder();
	  virtual void PrepareDraw();
	  virtual void Update();
	  virtual void Invalidate();
	  virtual bool OnSetFocus() {Invalidate();return true;}
	  virtual bool OnKillFocus() {Invalidate();return true;}	  
	  //Constructor & destructor
	  CGWin(int winid, HG3DCONTEXT ctx, unsigned long flags, CGWin *parent=NULL);
	  CGWin(int winid, int xpos, int ypos, int xsize, int ysize, unsigned long flags, CGWin *parent=NULL);
	  virtual ~CGWin();
  private:
	  CGWin(const CGWin &) {} //forbidden copy-constructor
  	  void InvalidateDown();
      static CGWin **GetTopWindowReference(CGWin **ref,unsigned long flags);
  };

bool g3dgSetDesktop(CGWin *dt);
bool g3dgKillDesktop();
bool g3dguiOnKeyEvent(UINT msg, WPARAM wParam, LPARAM lParam);
bool g3dguiOnMouseEvent();
bool g3dguiRedrawDesktop(int buffer);

#endif // !defined(AFX_GWIN_H__6A501924_07BB_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
