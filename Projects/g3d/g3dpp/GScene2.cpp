// GScene2.cpp: implementation of the CGScene2 class.
//
//////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <g3dlib.h>
#include <malloc.h>
#include "GScene2.h"
#include <stdarg.h>

CGExecBuffer::CGExecBuffer(long align,int inital, int grow)
  {
  bufgrow=grow;
  buffer=malloc(inital);
  if (buffer==NULL) inital=0;
  bufalloc=inital;
  bufsize=0;
  error=false;
  alignmask=align-1;
  markadr=0;
  }

CGExecBuffer::~CGExecBuffer()
  {
  free(buffer);
  }

void *CGExecBuffer::Alloc(int size, bool align)
  {
  void *p;
  int als;
  if (error) return NULL;
  if (align) als=alignmask-(bufsize & alignmask);else als=0;
  size+=als;
  if (bufsize+size>bufalloc)
	{
	int asiz=bufsize+size,bsiz=bufgrow+bufalloc;
	asiz=__max(asiz,bsiz);
	void *q=realloc(buffer,asiz);
	if (q==NULL) {error=true; return NULL;}
	buffer=q;
	bufalloc=asiz;
	}
  if (als) memset(GetAdr(bufsize),0,als);
  p=GetAdr(bufsize+als);
  bufsize+=size;
  return p;
  }


bool CGExecBuffer::AddData(void *data,int size, bool align)
  {
  if (error) return false;
  void *p=Alloc(size,align);
  if (p==NULL) return false;
  memcpy(p,data,size);
  return true;
  }


_inline void *AllocCommand(CGExecBuffer *buf, int size, unsigned char command)
  {
  unsigned char *c=(unsigned char *)buf->Alloc(size+sizeof(command));
  if (c!=NULL) *c++=command;
  return (void *)c;
  }

#define AddCom(com,datatype) (datatype *)AllocCommand(&buf,sizeof(datatype),com)
#define AddComArr(com,datatype,count) (datatype *)AllocCommand(&buf,sizeof(datatype)*count,com)
#define AddComSpec(com,datatype,size) (datatype *)AllocCommand(&buf,size,com)

int CGSceneBuilder::SetOwnMaterial(G3DMATCOLORCALLBACK callback, int structsize)
  {
  curcolsize=structsize;
  SMatClass *mc=AddCom(curcom=EGC_SetMaterialClass,SMatClass);
  if (mc==NULL) return -1;
  mc->matclass=(void *)callback;
  mc->matstructsize=structsize;  
  curcolsize=structsize;
  return 0;

  }
#define vcount() ((long *)((char *)buf.GetAdr(0)+vcounter))

#define AllocVector(type) if (curcom!=type)\
  	{\
	long *av=AddCom(type,long);\
	if (av==NULL) return false;\
	*av=0;\
	vcounter=(char *)av-(char *)buf.GetAdr(0);\
	curcom=type;\
	}


int CGSceneBuilder::AddPosition(float x, float y, float z)
  {
  AllocVector(EGC_AddMultPos);
  float *vect=(float *)buf.Alloc(sizeof(float)*3,false);
  if (vect==NULL) return -1;
  vect[XVAL]=x;
  vect[YVAL]=y;
  vect[ZVAL]=z;
  vcount()[0]++;
  return pc++;
  }

int CGSceneBuilder::AddPosition(HVECTOR v)
  {
  AllocVector(EGC_AddMultPos)
  float *vect=(float *)buf.Alloc(sizeof(float)*3,false);
  if (vect==NULL) return -1;
  memcpy(vect,v,sizeof(float)*3);
  vcount()[0]++;
  return pc++;
  }

int CGSceneBuilder::AddTPosition(HVECTOR v,HVECTOR tv)
  {
  AllocVector(EGC_SetTPosition)
  float *vect=(float *)buf.Alloc(sizeof(float)*8,false);
  if (vect==NULL) return -1;
  memcpy(vect,v,sizeof(float)*4);
  memcpy(vect+4,tv,sizeof(float)*4);
  vcount()[0]++;
  return pc++;
  }

int CGSceneBuilder::AddNormal(float x, float y, float z)
  {
  AllocVector(EGC_AddMultNorm)
  float *vect=(float *)buf.Alloc(sizeof(float)*3,false);
  if (vect==NULL) return -1;
  vect[XVAL]=x;
  vect[YVAL]=y;
  vect[ZVAL]=z;
  vcount()[0]++;
  return nc++;
  }

int CGSceneBuilder::AddNormal(HVECTOR v)
  {
  AllocVector(EGC_AddMultNorm)
  float *vect=(float *)buf.Alloc(sizeof(float)*3,false);
  if (vect==NULL) return -1;
  memcpy(vect,v,sizeof(float)*3);
  vcount()[0]++;
  return nc++;
  }

int CGSceneBuilder::AddFace(unsigned char face, unsigned char flags, unsigned char nTextures,  ...)
  {
  va_list p;
  va_start(p,nTextures);
  if (nTextures<1) return -1;
  FaceInfo *f=AddComSpec(curcom=EGC_AddFace,FaceInfo,sizeof(FaceInfo)+sizeof(long)*(nTextures-1));
  if (f==NULL) return -1;
  fcounter=(char *)&f->vcount-(char *)buf.GetAdr(0);
  f->face=face;
  f->flags=flags;
  f->tcount=nTextures;
  f->vcount=0;
  for (int i=0;i<nTextures;i++)	f->texlist[i]=va_arg(p,long);	
  ntex=nTextures;
  return 0;
  }

#define fcount ((char *)buf.GetAdr(0)+fcounter)

int CGSceneBuilder::AddVertex(int pos, int norm, int col, double *tutv)
  {  
  if (curcom!=EGC_AddFace) return -1;
  if (*fcount==255) return -1;
  if (pos>=pc || norm>=nc || col>=cc) return -1;
  if (pos==-2) pos=0;else  pos=pc-pos;
  if (norm==-2) norm=0;else if (norm>=0) norm=nc-norm;
  if (col==-2) col=0;else if (col>=0) col=cc-col;
  FaceVertex *s=(FaceVertex *)buf.Alloc(sizeof(FaceVertex)+2*sizeof(float)*(ntex-1),false);
  if (s==NULL) return -1;
  s->pos=pos;
  s->norm=norm;
  s->col=col;
  for (int i=0;i<ntex;i++)
	{
	s->tuv[i][0]=(float)*tutv++;
	s->tuv[i][1]=(float)*tutv++;
	}
  fcount[0]++;
  return 0;
  }

int CGSceneBuilder::AddVertex(int posindex, int normindex, int colindex,...)
  {
  return AddVertex(posindex,normindex,colindex,(double *)(&colindex+1));
  }
/*
int CGSceneBuilder::AddVertex(HVECTOR pos, int normindex, int colindex,...)
  {
  int res=AddVertex(-2,normindex,colindex,(double *)(&colindex+1));
  buf.AddData(pos,sizeof(float)*3);pc++;
  return res;
  }

int CGSceneBuilder::AddVertex(int posindex, HVECTOR norm, int colindex,...)
  {
  int res=AddVertex(posindex,-2,colindex,(double *)(&colindex+1));
  buf.AddData(norm,sizeof(float)*3);nc++;
  return res;
  }

int CGSceneBuilder::AddVertex(HVECTOR pos, HVECTOR norm, int colindex,...)
  {
  int res=AddVertex(-2,-2,colindex,(double *)(&colindex+1));
  buf.AddData(pos,sizeof(float)*3);pc++;
  buf.AddData(norm,sizeof(float)*3);nc++;
  return res;
  }

int CGSceneBuilder::AddVertex(int posindex, int normindex, void *col,...)
  {
  int res=AddVertex(posindex,normindex,col?-2:-1,(double *)(&col+1));
  if (col) buf.AddData(col,curcolsize),cc++;
  return res;
  }

int CGSceneBuilder::AddVertex(HVECTOR pos, int normindex, void *col,...)
  {
  int res=AddVertex(-2,normindex,col?-2:-1,(double *)(&col+1));
  buf.AddData(pos,sizeof(float)*3);pc++;
  if (col) {buf.AddData(col,curcolsize);nc++;}
  return res;
  }

int CGSceneBuilder::AddVertex(int posindex, HVECTOR norm, void *col,...)
  {
  int res=AddVertex(posindex,-2,col?-2:-1,(double *)(&col+1));
  buf.AddData(norm,sizeof(float)*3);nc++;
  if (col) {buf.AddData(col,curcolsize);cc++;}
  return res;
  }

int CGSceneBuilder::AddVertex(HVECTOR pos, HVECTOR norm, void *col,...)
  {
  int res=AddVertex(-2,-2,col?-2:-1,(double *)(&col+1));
  buf.AddData(pos,sizeof(float)*3);pc++;
  buf.AddData(norm,sizeof(float)*3);nc++;
  if (col) {buf.AddData(col,curcolsize);cc++;}
  return res;
  }
*/
int CGSceneBuilder::AddMaterial(void *mat)
  {
  unsigned char com=curcom=EGC_AddMaterial;
  buf.AddData(&com,sizeof(com));
  buf.AddData(mat,curcolsize,false);
  cc++;
  return 0;
  }

int CGSceneBuilder::MatrixCommand(unsigned char com, HMATRIX mm)
  {
  curcom=com;
  buf.AddData(&com,sizeof(com));
  buf.AddData(mm,sizeof(HMATRIX),false);
  return 0;
  }

int CGSceneBuilder::PopMatrices(short count)
  {
  curcom=EGC_PopMatrices;
  buf.AddData(&curcom,sizeof(curcom));
  buf.AddData(&count,sizeof(count),false);
  return 0;
  }
  
int CGSceneBuilder::AddLight(CGLight& l)
  {
  curcom=EGC_AddLight;
  buf.AddData(&curcom,sizeof(curcom));
  buf.AddData(&l,sizeof(l),false);
  return 0;
  }

int CGSceneBuilder::AddBillboardVerts(int pos, int count, bool screen, double *vectxy)
  {
  int res;
  int curcom=screen?EGC_BillboardPosScr:EGC_BillboardPos;
  int *p=AddComSpec(curcom,int,sizeof(pos)+sizeof(count)+count*sizeof(float)*2);
  if (p==NULL) return -1;
  *p++=pc-pos;
  *p++=count;
  float *f=(float *)p;
  res=pc;
  for (int i=0;i<count;i++) 
	{
	*f++=(float)*vectxy++;
	*f++=(float)*vectxy++;
	pc++;
	}
  return res;
  }


int CGSceneBuilder::SetTXITable(HTXITABLE txi)
  {  
  HTXITABLE *t=AddCom(curcom=EGC_SetTXITable,HTXITABLE);
  if (t==NULL) return -1;
  *t=txi;
  lasttxi=txi;
  return 0;
  }

int CGSceneBuilder::InsertObject(void *data, long size, LPHMATRIX mm, HTXITABLE txi)
  {
  HTXITABLE l;
  if (txi) 
	{
	l=lasttxi;
	SetTXITable(txi);
	}
  if (mm) PushMatrixL(mm);
  buf.AddData(data,size);
  if (mm) PopMatrices(1);
  if (txi && l) SetTXITable(l);
  return 0;
  }

int CGSceneBuilder::InsertObject(CGExecBuffer &other, LPHMATRIX mm, HTXITABLE txi)
  {
  return InsertObject(other.GetAdr(0),other.GetBufSize(),mm,txi);
  }

int CGSceneBuilder::IncludeObject(CGExecBuffer &other, LPHMATRIX mm, HTXITABLE txi)
  {
  SInclude inc;
  inc.command=EGC_Include;
  inc.buff=&other;
  return InsertObject(&inc,sizeof(inc),mm,txi);
  }

bool CGSceneBuilder::LightTable(unsigned short tabsize)
  {
  unsigned short *d=AddComSpec(curcom=EGC_LightTable,unsigned short,sizeof(unsigned short)*2+tabsize);
  if (d==NULL) return false;
  *d++=tabsize;
  *d++=0;
  return true;
  }

/////////////////////////////////////

CGLightBuilder::CGLightBuilder(CGExecBuffer& ebuf):
  buf(ebuf)
  {
  if (buf.GetBufSize()>sizeof(char)+2*sizeof(unsigned short *))
	{
	unsigned char *c=(unsigned char *)buf.GetAdr(0);
	while (*c==0) c++;
	if (*c++==EGC_LightTable)
	  {
	  unsigned short *d=(unsigned short *)c;
	  ltsize=d++;
	  ltused=d;
	  c=(unsigned char *)(d+1);
	  ltptr=c+*ltused;
	  return;
	  }	
	}
  ltsize=NULL;
  ltused=NULL;
  }


bool CGLightBuilder::AddGen(unsigned char command, void *data, int size)
  {
  if (ltsize==NULL) return false;
  if (*ltused+size+sizeof(command)>*ltsize) return false;
  *ltptr++=command;
  ltused[0]+=sizeof(command);
  memcpy(ltptr,data,size);
  ltused[0]+=size;
  ltptr+=size;
  return true;
  }


bool CGLightBuilder::Insert(CGExecBuffer &subbuf)
  {
  CGLightBuilder lb(subbuf);
  if (lb.ltsize!=NULL)
	{
	if (*lb.ltused+*ltused>*ltsize) return false;
	memcpy(ltptr,lb.ltptr,*lb.ltused);
	ltptr+=*lb.ltused;
	ltused[0]+=lb.ltused[0];
	return true;
	}
  return true;
  }

TLIGHTDEF *CGLightBuilder::GetNextLight(int& index, CGSceneEnv& scnev)
  {
  if (ltsize==NULL) return NULL;
znova:
  if (index>=*ltused) return NULL;
  unsigned char *c=(unsigned char *)(ltused+1),com;
  c+=index;  
  com=*c++;
  switch (com)
	{
	case EGL_LDEF: index+=sizeof(TLIGHTDEF)+sizeof(*c);
				  return (TLIGHTDEF *)c;
	case EGL_INCLUDE:
	case EGL_HANDLE: index+=sizeof(void *)+sizeof(*c);
				void *p=*(void **)c;
					p=scnev[p];
					if (p==NULL) goto znova;
					if (com==EGL_HANDLE) return (TLIGHTDEF *)p;	
					else return NULL;
	

	}
  return NULL;
  }

CGExecBuffer *CGLightBuilder::GetSubtable(int& index, CGSceneEnv& scnev)
  {
  if (ltsize==NULL) return NULL;
  if (index>=*ltused) return NULL;
  unsigned char *c=(unsigned char *)(ltused+1);
  c+=index;
  if (*c++==EGL_INCLUDE)
	{
	index+=sizeof(void *)+sizeof(*c);
	void *p=*(void **)c;
	p=scnev[p];
	return (CGExecBuffer *)p;
	}
  return NULL;
  }

//////////////////////////////////////////////////////////////////////
// CGSceneEnv Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGSceneEnv::CGSceneEnv(int handles)
  {
  handlelist=new void *[handles];  
  if (handlelist==NULL) handles=0;
  count=handles;
  memset(handlelist,0,sizeof(void *)*count);
  }

CGSceneEnv::~CGSceneEnv()
  {
  delete [] handlelist;
  }

bool CGSceneEnv::SetHandle(int index, void *handle)
  {
  if (index>=count)
	{
	void **p=new void *[index+1];
	if (p==NULL) return false;	
	memcpy(p,handlelist,sizeof(void *)*count);
	memset(p+count,0,sizeof(void *)*(index-count+1));
	delete [] handlelist;
	handlelist=p;
	count=index+1;
	}
  handlelist[index]=handle;
  return true;
  }

int CGSceneBuilder::AddScene(TSCENEDEF * def, unsigned char flags)
  {
  CGSceneBuilder cb(buf);
  int i;
  if (def==NULL) return -1;
  TVERTEXCOLOR *col=def->cvtable;
  for (i=0;i<def->cvsize;i++,col++) cb.AddMaterial(col);
  TNORMALVERTEX *ntable=def->ntable;
  for (i=0;i<def->nsize;i++,ntable++) cb.AddMaterial(ntable->vector);
  TWORLDVERTEX *wtable=def->wtable;
  for (i=0;i<def->wsize;i++,wtable++) cb.AddMaterial(wtable->vector);
  TSCENEVERTEX *stable=def->stable;
  TFACEDRAWORDER *fctable=def->fctable;
  int c=0;
  for (i=0;i<def->fcsize;i++,fctable++)
	{
	if (c==0)
	  {
	  cb.AddFace((unsigned char)fctable->wFlags,flags,1,fctable->texture);
	  c=fctable->vcount;
	  }
	else
	  {
	  cb.AddVertex(stable[fctable->dwVertex].wvindex,stable[fctable->dwVertex].nvindex,
		fctable->dwVertexColor,fctable->tu,fctable->tv);
	  c--;
	  }
	}
  return 0;


  }

