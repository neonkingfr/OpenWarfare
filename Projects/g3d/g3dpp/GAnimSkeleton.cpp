// GAnimSkeleton.cpp: implementation of the CGAnimSkeleton class.
//
//////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "GAnimSkeleton.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGAnimSkeleton::CGAnimSkeleton(int grow)
  {
  bones=NULL;
  allocated=0;
  this->grow=grow;
  bonecount=0;
  maxframe=0;
  }

CGAnimSkeleton::~CGAnimSkeleton()
  {
  for (int i=0;i<bonecount;i++) if (bones[i]) bones[i]->Release();
  free(bones);
  }


bool CGAnimSkeleton::Expand()
  {
  CGAnimMatrix **p=(CGAnimMatrix **)realloc(bones,(allocated+grow)*sizeof(CGAnimMatrix *));
  if (p==NULL) return false;
  bones=p;
  allocated+=grow;
  return true;
  }


int CGAnimSkeleton::AddBone(CGAnimMatrix *mm, int parent)
  {
  if (bonecount>=allocated) 
	if (Expand()==false) return -1;
  bones[bonecount]=mm;
  if (mm)
	{
	mm->parent=parent;
	SGAnimState st;
	mm->NextFrame(CGAN_LASTFRAME,st);
	maxframe=__max(maxframe,st.framenum);
	}
  return bonecount++;
  }


bool CGAnimSkeleton::NextFrame(int flags, int * states)
  {
  SGAnimState st;
  if (ValidateStates(states)==false) return false;
  for (int i=0;i<bonecount;i++) if (bones[i])
	{
	st.framenum=states[1];
	st.indexnum=states[i+2];
	bones[i]->NextFrame(flags,st);
	states[i+2]=st.indexnum;
	}
  states[1]=st.framenum;
  return true;
  }

int *CGAnimSkeleton::AllocStates(int flags)
  {
  int *i=new int[bonecount+2];
  if (i==NULL) return NULL;
  i[0]=bonecount;
  NextFrame(flags,i);
  return i;
  }

bool CGAnimSkeleton::CalcSkeleton(int *states)
  {
  if (ValidateStates(states)==false) return false;
  SGAnimState st;
  st.framenum=states[1];
  for (int i=0;i<bonecount;i++) if (bones[i])
	{
	int par=bones[i]->parent;
    st.indexnum=states[i+2];
	if (par==-1 || bones[par]==NULL)
	  {if (bones[i]->GetFrame(st,bones[i]->temp)==false) return false;}
	else 
	  {
	  HMATRIX tmp;  
	  if (bones[i]->GetFrame(st,tmp)==false) return false;
	  SoucinMatic(tmp,bones[par]->temp,bones[i]->temp);
	  }
	}
  return true;
  }

bool CGAnimSkeleton::CombineSkeleton(CGAnimSkeleton& skell)
  {
  for (int i=0;i<bonecount;i++) if (bones[i])
	{
	if (!skell.bones[i]) return false;
	HMATRIX mm;
	SoucinMatic(skell.bones[i]->temp,bones[i]->temp,mm);
	CopyMatice(bones[i]->temp,mm);
	}
  return true;
  }

ostream& operator<<(ostream& out,const CGAnimSkeleton &skell)
  {
  out.write((char *)&skell.bonecount,sizeof(skell.bonecount));
  for (int i=0;i<skell.bonecount;i++) if (skell.bones[i])
	{
	out.write((char *)&i,sizeof(i));
	out<<(*skell.bones[i]);
	}
  return out;
  }

istream& operator>>(istream& in,CGAnimSkeleton &skell)
  {
  in.read((char *)&skell.bonecount,sizeof(skell.bonecount));
  int grsave=skell.grow;
  skell.grow=skell.bonecount-skell.allocated;
  if (skell.grow>0) if (skell.Expand()==false)
	throw int(-1);
  skell.grow=grsave;
  for (int i=0;i<skell.bonecount;i++)
	{
	int p;
	in.read((char *)&p,sizeof(p));
	CGAnimMatrix *am=new CGAnimMatrix(0);
	in>>(*am);
	skell.bones[p]=am;
	}
  return in;
  }

LPHMATRIX CGAnimSkeleton::GetMatrix(int bone)
  {
  if (bones[bone]) return bones[bone]->temp;else return NULL;
  }

CGAnimMatrix *CGAnimSkeleton::GetBone(int index)
  {
  if (index>=0 && index<bonecount) return bones[index];
  return NULL;
  }

bool CGAnimSkeleton::GotoFrame(int frame, int * states)
  {
  SGAnimState st;
  if (ValidateStates(states)==false) return false;
  for (int i=0;i<bonecount;i++) if (bones[i])
	{
	st.framenum=states[1];
	st.indexnum=states[i+2];
	bones[i]->GotoFrame(frame,st);
	states[i+2]=st.indexnum;
	}
  states[1]=st.framenum;
  return true;
  }
