# Microsoft Developer Studio Project File - Name="g3dpp" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=G3DPP - WIN32 RELEASE
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "g3dpp.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "g3dpp.mak" CFG="G3DPP - WIN32 RELEASE"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "g3dpp - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "g3dpp - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Projects/g3d/g3dpp", YQDAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "g3dpp - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".." /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\g3dpp.lib"

!ELSEIF  "$(CFG)" == "g3dpp - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /Z7 /Od /I ".." /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\g3dpp.lib"

!ENDIF 

# Begin Target

# Name "g3dpp - Win32 Release"
# Name "g3dpp - Win32 Debug"
# Begin Group "g3dpp headers"

# PROP Default_Filter "h"
# Begin Source File

SOURCE=.\CGWinBase.h
# End Source File
# Begin Source File

SOURCE=.\g3dmdl3.h
# End Source File
# Begin Source File

SOURCE=.\G3DObject.h
# End Source File
# Begin Source File

SOURCE=.\g3dpp.h
# End Source File
# Begin Source File

SOURCE=.\GAnim.h
# End Source File
# Begin Source File

SOURCE=.\GAnimMatrix.h
# End Source File
# Begin Source File

SOURCE=.\GAnimSkeleton.h
# End Source File
# Begin Source File

SOURCE=.\GBase.h
# End Source File
# Begin Source File

SOURCE=.\GFaceSort.h
# End Source File
# Begin Source File

SOURCE=.\GParser.h
# End Source File
# Begin Source File

SOURCE=.\GParticles.h
# End Source File
# Begin Source File

SOURCE=.\GScene2.h
# End Source File
# Begin Source File

SOURCE=.\GWin.h
# End Source File
# Begin Source File

SOURCE=.\Message.h
# End Source File
# Begin Source File

SOURCE=.\Messager.h
# End Source File
# Begin Source File

SOURCE=.\MsgObjects.h
# End Source File
# End Group
# Begin Group "g3dpp files"

# PROP Default_Filter "cpp"
# Begin Source File

SOURCE=.\CGWinBase.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\g3dmdl3.cpp
# End Source File
# Begin Source File

SOURCE=.\G3DObject.cpp
# End Source File
# Begin Source File

SOURCE=.\g3dpp.cpp
# End Source File
# Begin Source File

SOURCE=.\GAnim.cpp
# End Source File
# Begin Source File

SOURCE=.\GAnimMatrix.cpp
# End Source File
# Begin Source File

SOURCE=.\GAnimSkeleton.cpp
# End Source File
# Begin Source File

SOURCE=.\GBase.cpp
# End Source File
# Begin Source File

SOURCE=.\GFaceSort.cpp
# End Source File
# Begin Source File

SOURCE=.\GParser.cpp
# End Source File
# Begin Source File

SOURCE=.\GParticles.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\GScene2.cpp
# End Source File
# Begin Source File

SOURCE=.\GWin.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\Message.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\Messager.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\MsgObjects.cpp
# PROP Exclude_From_Build 1
# End Source File
# End Group
# End Target
# End Project
