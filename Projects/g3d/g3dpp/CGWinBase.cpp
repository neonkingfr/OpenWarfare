// CGWinBase.cpp: implementation of the CGDialogWin class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "CGWinBase.h"

G3DCONTEXT CGWinLabel::templ;
G3DCONTEXT CGButton::templ;
G3DCONTEXT CGLabel::templ;
G3DCONTEXT CGDialogWin::templ;

static HG3DCONTEXT ModifyDefContext(G3DCONTEXT &def, int xp, int yp, int xs, int ys)
  {
  def.wl=xp;
  def.wt=yp;
  def.wr=xp+xs-1;
  def.wb=yp+ys-1;
  return &def;
  }

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void CGDialogWin::AddTitle(char *text)
  {
  int winsx,winsy;
  HG3DCONTEXT old;
  CGWin *lb;
  int bsize,chx=0,chy=0;
  old=g3dSetActive(&winctx);  
  winsx=g3dGetSizeX();  
  g3dSetActive(&CGWinLabel::templ);  
  g3dGetCharExtent('X', &chx, &chy);
  bsize=__max(chx,chy)+2;
  bsize=__max(bsize,10);
  winsy=bsize+2;
  lb=new CGWinLabel(IDW_STATIC,0,0,winsx,winsy,
	  CGWF_CHILD|CGWF_FLAT|CGWF_VISIBLE|CGWF_AUTORESIZEX,
	  this,NULL);
  new CGButton(IDW_CLOSE,winsx-bsize,1,bsize,bsize,
	  CGWF_CHILD|CGWF_FLAT|CGWF_VISIBLE|CGWF_ALIGNRIGHT,
	  this,"X");
  lb->AttachToClient(&title);
  title=text;
  g3dSetActive(old);
  }

CGDialogWin::CGDialogWin(int winid, HG3DCONTEXT cnt,unsigned long flags,CGWin *parent,char *text):
  CGWin(winid,cnt,flags,parent)
	{
	AddTitle(text);
	}

CGDialogWin::CGDialogWin(int winid, int xpos, int ypos, int xsize, int ysize, unsigned long flags, CGWin *parent, char *text):
  CGWin(winid,ModifyDefContext(templ,xpos,ypos,xsize,ysize),flags,parent)
	{
	AddTitle(text);
	}

void CGDialogWin::Income(CGMsgLink *from, CGMessage *msg)
  {
  const char *text=msg->AsString(NULL,0);
  if (text) title=text;
  }

//////////////////////////////////////////////////////////////////////
// CGLabel Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGLabel::CGLabel(int winid, HG3DCONTEXT cnt,unsigned long flags,CGWin *parent,char *text):
  CGWin(winid,cnt,flags,parent)
	{
	label=NULL;
	SetText(text);
	}

CGLabel::CGLabel(int winid, int xp, int yp, int xs, int ys,unsigned long flags,CGWin *parent,char *text):
  CGWin(winid,ModifyDefContext(templ,xp,yp,xs,ys),flags,parent)
	{
	label=NULL;
	SetText(text);
	}

CGLabel::~CGLabel()
  {
  delete[]label;
  }

void CGLabel::SetText(const char *text)
  { 
  if (text==NULL) return;
  char *nw=new char[strlen(text)+1];
  strcpy(nw,text);
  if (nw==NULL) return;
  delete[]label;
  label=(char *)nw;
  Invalidate();
  }

void CGLabel::Income(CGMsgLink *from, CGMessage * msg)
  {
  const char *text=msg->AsString(NULL,0);
  SetText(text);
  }


void CGLabel::OnDraw()
  {
  int x,y,a;
  float alpha;
  unsigned long color;
  g3dClearContext();
  a=g3dGetAlign();
  if (a & G3DF_ALEFT) x=10;
  else if (a & G3DF_ARIGHT) x=g3dGetSizeX()-10;
  else x=g3dGetSizeX()/2;
  if (a & G3DF_ATOP) y=0;
  else if (a & G3DF_ABOTTOM) y=g3dGetSizeY()-1;
  else y=g3dGetSizeY()/2;
  color=g3dGetFontColor();
  if (!(flags & CGWF_ENABLED))
	{
	alpha=G3D_GETCOLOR_A(color)*0.5f;
	g3dSetFontColor((G3D_RGBA(0.0f,0.0f,0.0f,1.0f) | color) & G3D_RGBA(1.0f,1.0f,1.0f,a));
	}
  g3dMoveTo(x,y);
  if (label!=NULL) g3dOutText(label,0);
  g3dSetFontColor(color);
  }

//////////////////////////////////////////////////////////////////////
// CGButton Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGButton::CGButton(int winid, HG3DCONTEXT cnt,unsigned long flags,CGWin *parent,char *text):
  CGLabel(winid,cnt,flags,parent,text)
	{
	lbpress=false;
	winctx.font.align=0;
	}

CGButton::CGButton(int winid, int xpos, int ypos, int xsize, int ysize, unsigned long flags, CGWin *parent, char *text):
  CGLabel(winid,ModifyDefContext(templ,xpos,ypos,xsize,ysize),flags,parent,text)
	{
	lbpress=false;
	winctx.font.align=0;
	}

void CGButton::OnDraw()
  {
  if (lbpress) {winctx.wl+=2;winctx.wt+=2;}  
  CGLabel::OnDraw();
  if (lbpress) {winctx.wl-=2;winctx.wt-=2;}  
  int wl,wr,wt,wb;
  wl=0;
  wt=0;
  wr=g3dGetSizeX()-1;
  wb=g3dGetSizeY()-1;
  unsigned long col1=G3D_RGBA(1.0f,1.0f,1.0f,0.5f);
  unsigned long col2=G3D_RGBA(0.0f,0.0f,0.0f,0.5f);
  unsigned long old=g3dGetColor();
  if (lbpress) 
	{
	col1^=col2;
	col2^=col1;
	col1^=col2;
	}
  g3dSetColor(col1);
  g3dLine(wl,wt,wr,wt);
  g3dLine(wl,wt,wl,wb);
  if (lbpress)
	{
	g3dLine(wl+1,wt+1,wr-1,wt+1);
	g3dLine(wl+1,wt+1,wl+1,wb-1);
	}
  g3dSetColor(col2);
  g3dLine(wl,wb,wr,wb);
  g3dLine(wr,wt,wr,wb);
  if (lbpress)
	{
	g3dLine(wl+1,wb-1,wr-1,wb-1);
	g3dLine(wr-1,wt+1,wr-1,wb-1);
	}
  g3dSetColor(g3dGetFontColor());
  if (HasFocus()) g3dRectangle(wl+3,wt+3,wr-3,wb-3);
  g3dSetColor(old);
  }

void CGButton::OnMouseEvent(TMSSTATE * ms)
  {
  if (ms->event & G3DMS_TL1D) 
	{lbpress=true;SetCapture(true);}
  if (ms->event & G3DMS_MOVE && ms->tl1)
	{
	bool nw=g3dTestPoint(ms->msx,ms->msy)!=0;
	if (nw!=lbpress) {lbpress=nw;Invalidate();}
	}
  if (ms->event & G3DMS_TL1U)
	{
	SetCapture(false);
	CGWin *parent=GetWindow(DrawParent);
	if (parent!=NULL) parent->OnNotify(this,&CGMessage(CGMSG_COMMAND));
	}
  }

//////////////////////////////////////////////////////////////////////
// CGWinLabel Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGWinLabel::CGWinLabel(int winid, HG3DCONTEXT cnt,unsigned long flags,CGWin *parent,char *text):
 CGLabel(winid,cnt,flags,parent,text)
  {
  winctx.font.align=G3DF_ALEFT|G3DF_ACENTER;
  }

CGWinLabel::CGWinLabel(int winid, int xpos, int ypos, int xsize, int ysize, unsigned long flags, CGWin *parent, char *text):
 CGLabel(winid,ModifyDefContext(templ,xpos,ypos,xsize,ysize),flags,parent,text)
  {
  winctx.font.align=G3DF_ALEFT|G3DF_ACENTER;
  }


void CGWinLabel::OnMouseEvent(TMSSTATE * mss)
  {
  if (mss->event & G3DMS_TL1D)
	{
	SetCapture(true);
	xml=mss->msx;
	yml=mss->msy;
	}
  if (mss->event & G3DMS_MOVE && mss->tl1)
	{
	CGWin *parent=GetWindow(DrawParent);
	parent->OnMove(mss->msx-xml,mss->msy-yml);
	xml=mss->msx;
	yml=mss->msy;
	}
  if (mss->event & G3DMS_TL1U)
	{
	MessageBeep(0xFFFFFFFF);
	SetCapture(false);
	}
  }

//////////////////////////////////////////////////////////////////////
// CGDesktop Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGDesktop::CGDesktop():
  CGWin(0,g3dGetActive(),CGWF_FLAT|CGWF_ABSOLUTEPOS|CGWF_VISIBLE)
  {
  HG3DCONTEXT old=g3dSetActive(&winctx);
  zorder=0.9999f;
  zmin=0.0f;
  g3dSetContextZ(zorder);
  g3dSetActive(old);
  }

CGDesktop::~CGDesktop()
  {

  }
