// g3dmdl3.h: interface for the CGSceneList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_G3DMDL3_H__069F3E69_FCC6_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_G3DMDL3_H__069F3E69_FCC6_11D3_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "G3DObject.h"
#include "GBase.h"

class CGSceneList : public CG3DDispObject  
  {
	int count;
	int current;
	CGScene **field;
  public:
	int GetCount() {return count;}
    virtual bool Load(FILE *f);
    virtual bool Save(FILE *f);
    bool Expand(int count);
	void SetScene(int index, CGScene *scene);
	virtual int Move(int howmany);
	virtual int LoadToScene(HMATRIX mm, HTXITABLE txi=NULL);
	CGSceneList(int count=0);
	CGScene *operator[](int index)
	  {if (index<0 || index>=count) return NULL;
	  else return field[index];
	  }
	virtual ~CGSceneList();	

  };


struct SGFrame
  {
  int fnum;
  unsigned long flags;
  int scene;
  HMATRIX key;  
  void Init() {memset(this,0,sizeof(*this));scene=-1;}
  void Set(int f, unsigned long flags, HMATRIX key)
	{this->fnum=f;this->flags=flags;if (key) CopyMatice(this->key,key);}
  void SetScene(int s) {scene=s;}
  SGFrame(int f=0, unsigned long flags=0, LPHMATRIX mm=NULL)
	{Set(f,flags,mm);this->scene=-1;}
  };

#define G3DFF_MATRIX 0
#define G3DFF_KEY 1

#define G3DFA_SORTED 1

class CGAnimation : public CG3DObject  
	{
	SGFrame *list;
	int count;
	int grow;
	int allocated;
	int FindFrame(int frame);
	void Interpolate(int index1,int index2, float factor, SGFrame& frame);
public:
	int GetLastFrame();
	bool Expand(int grow);
	virtual bool Load(FILE *f);
	int seqid;
	int objectref;
	virtual bool Save(FILE *f);
	SGFrame operator[](int index);
	SGFrame *operator^(int index)
	  {
	  if (index<0 || index>=count) return NULL;
	  return list+index;
	  }
	void Prepare();
	bool GetFrame(int fnum, SGFrame& frame);
	bool DeleteFrame(int number);
	bool AddFrame(SGFrame& frm);
	CGAnimation(int grow=128);
    virtual ~CGAnimation();	
	private:
		int LookForIndex(int frame);
	};

struct SGObjectInfo
  {
  int parent;
  LPHMATRIX frame;
  const char *name;
  int sceneref;
public:
	bool Load(FILE *f);
	bool Save(FILE *f);
	bool SetName(const char *name);
	LPHMATRIX CalcMatrix(HMATRIX anim, SGObjectInfo *parents, HMATRIX def);
	bool Display(CGSceneList *sl, int altscene, HTXITABLE txi);
  };

struct SG3DObjectList
  {
  int count;
  SGObjectInfo list[1];
  long GetSelfSize() {return sizeof(SG3DObjectList)+(count-1)*sizeof(SGObjectInfo);}
  static long CalcReqSize(int count) {return sizeof(SG3DObjectList)+(count-1)*sizeof(SGObjectInfo);}  
  public:
  SG3DObjectList *ExpandSelf();
  void Release();
  static SG3DObjectList *Alloc(int count);
  static SG3DObjectList *Load(FILE *f);
  bool Save(FILE *f);
  };

class __single_inheritance CGModel;

class CGModelInfo : public CG3DObject  
  {
  friend CGModel;
  private:
	char *name;
	CGSceneList *slist;	  //list of scenes
	SG3DObjectList *olist; //list of objects (with scene references)
	CGAnimation **alist;  //list of animations (with object references)
	int seqcount;	//count of sequences (size of SGAnimation array)
	CGCamera *camera; //camera attached
	int camobject;	  //object to report camera changes
	bool drawcamobject; //true will draw scene include camera object
  public:	
    int * Link(CGModelInfo *linkgr);
	virtual bool Load(FILE *f);
	virtual bool Save(FILE *f);
	bool Display(int *seqlist, int *scnxlat, HMATRIX def, HTXITABLE txi, int frame);
	  //seqlist je seznam sekvenci, ktere se maji zobrazit
	  //scnxlat je tabulka pro preklad slistu. Pouziva se jen pri prilinkovane geometrii
	  //def je matice natoceni objektu
	  //txi je TXI tabulka
	  //frame je cislo frame ke zobrazeni
	int *BuildSeqIndexs(int seqnum, int & lastframe);
	  //sestavuje seznam sekvenci, ktere jsou pripojeny k urcitemu cislu sekvence
	int AddObject(int sceneref, int parent=-1, const char *name=NULL);	
	bool Attach(CGCamera *cam, int object, bool draw);
	bool Attach(CGAnimation *al, int object, int seqid);
    void Attach(CGSceneList *sl);
	const char *GetName() {return name;}
	bool SetName(const char *name);
	int FindObject(const char *name);
	int GetParentObject(int object)
	  {if (object<0 || object>olist->count) return G3DM_NOTFOUND;
	  else return olist->list[object].parent;}
	const char *GetObjectName(int object)
	  {if (object<0 || object>olist->count) return NULL;
	  else return olist->list[object].name;}
	bool SetObjectName(int object, const char *name)
	  {if (object<0 || object>olist->count) return false;
	  else return olist->list[object].SetName(name);}
	const SGObjectInfo* operator[] (int index);
	int EnumChild(int parent,int last);
	CGSceneList *GetSceneList() {return slist;}
	CGModelInfo();
	virtual ~CGModelInfo();
  };

class CGModel : public CG3DDispObject  
	{	
	HTXITABLE txi;
	bool owntxi;
	CGModelInfo *model;
	CGSceneList *scnlist;
	int *seqlist;
	int *scnxlat;
public:	
	bool Link(CGModel *graphics);
	bool Save(char *filename);
	bool Load(char *filename);
	virtual bool Save(FILE *f);
	virtual bool Load(FILE *f);
	virtual int LoadToScene(HMATRIX mm, HTXITABLE txi=NULL);
	virtual int Animate(int howmany);
	void Attach(CGSceneList *list) {model->Attach(list);}
	int SetCurFrame(int framenum);
	int SetCurSequence(int seqid, int direction=1, int flags=0);
	int SetCurObject(int object);
	int AddSequence(CGAnimation *anim, int seqid);
	int AddObject(int sceneref, int parent=-1, const char *name=NULL);
	bool Attach(HTXITABLE tbl, bool own);

	int id;
	int curframe;
	int trueframe;
	int lastframe;
	int cursequence;
	int curobject;
	int frame1000;
	int direction;
	int lastticker;
	int *ticker;
	int *indexes;
	int flags;
	 
	CGModel(CGModel *toclone=NULL);
	virtual ~CGModel();

};

#endif // !defined(AFX_G3DMDL3_H__069F3E69_FCC6_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
