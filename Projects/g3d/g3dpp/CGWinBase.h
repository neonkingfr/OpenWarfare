// CGWinBase.h: interface for the CGDialogWin class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CGWINBASE_H__6A501929_07BB_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_CGWINBASE_H__6A501929_07BB_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "GWin.h"
#include "MsgObjects.h"

#define IDW_STATIC -1
#define IDW_CLOSE 0




class CGDialogWin : public CGWin  
	{
	CGString title;
	void AddTitle(char *text);
public:
	static G3DCONTEXT templ;
	static void SetDefContext(HG3DCONTEXT cnt) {templ=*cnt;}
	CGDialogWin(int winid, HG3DCONTEXT cnt,unsigned long flags,CGWin *parent,char *text=NULL);
	CGDialogWin(int winid, int xpos, int ypos, int xsize, int ysize, unsigned long flags, CGWin *parent=NULL, char *text=NULL);
	virtual void Income(CGMsgLink *from, CGMessage *msg);	
	};


class CGLabel : public CGWin  
	{
	char *label;
public:
	void SetText(const char *text);
	virtual void OnDraw();
	virtual void Income(CGMsgLink *from, CGMessage *msg);
	virtual bool OnSetFocus() {return false;}
	CGLabel(int winid, HG3DCONTEXT cnt,unsigned long flags,CGWin *parent,char *text=NULL);
	CGLabel(int winid, int xpos, int ypos, int xsize, int ysize, unsigned long flags, CGWin *parent=NULL, char *text=NULL);
	virtual ~CGLabel();
	static G3DCONTEXT templ;
	static void SetDefContext(HG3DCONTEXT cnt) {templ=*cnt;}
	};


class CGButton : public CGLabel  
	{
	bool lbpress;
public:
	virtual void OnMouseEvent(TMSSTATE *ms);
	virtual void OnDraw();
	virtual bool OnSetFocus() {return true;}
	CGButton(int winid, HG3DCONTEXT cnt,unsigned long flags,CGWin *parent,char *text=NULL);
	CGButton(int winid, int xpos, int ypos, int xsize, int ysize, unsigned long flags, CGWin *parent, char *text=NULL);
	static void SetDefContext(HG3DCONTEXT cnt) {templ=*cnt;}
	static G3DCONTEXT templ;
	};


class CGWinLabel : public CGLabel  
	{
	int xml,yml;
public:
  	virtual bool OnSetFocus() {return true;}
	virtual void OnMouseEvent(TMSSTATE *mss);
	static void SetDefContext(HG3DCONTEXT cnt) {templ=*cnt;}	
	CGWinLabel(int winid, HG3DCONTEXT cnt,unsigned long flags,CGWin *parent,char *text=NULL);
	CGWinLabel(int winid, int xpos, int ypos, int xsize, int ysize, unsigned long flags, CGWin *parent, char *text=NULL);
	static G3DCONTEXT templ;
	};


class CGDesktop : public CGWin  
{
public:
	virtual void OnDraw()
	  {
	  CGWin::OnDraw();
	  }
	CGDesktop();
	virtual ~CGDesktop();

};

#endif // !defined(AFX_CGWINBASE_H__6A501929_07BB_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
