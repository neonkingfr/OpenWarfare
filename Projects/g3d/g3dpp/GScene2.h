// GScene2.h: interface for the CGScene2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GSCENE2_H__5EF80862_20E5_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_GSCENE2_H__5EF80862_20E5_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "G3DObject.h"
#include "g3dlib.h"
#include "GBase.h"


struct SVector
  {
  float vector[3];
  long bone;
  };

#define RVAL 0
#define GVAL 1
#define BVAL 2
#define AVAL 3

typedef void (*G3DMATCOLORCALLBACK)(void *matdef, HVECTOR color);

//enum EGCommands			  //params:
//  {
#define EGC_NoOp 0	  //for aligment
#define EGC_SetMaterialClass 0x80  //void *matcallback, unsigned long structsize
#define EGC_AddPosition 0x81 //float x, float y, float z
#define EGC_AddNormal 0x82	//float x, float y, float z
#define EGC_AddMultPos 0x83  //long count, xyz, xyz, xyz...
#define EGC_AddMultNorm 0x84 //long count, xyz, xyz, xyz...
#define EGC_PushMatrixL 0x85	//HMATRIX mm
#define EGC_PushMatrixR 0x86 //HMATRIX mm
#define EGC_PopMatrices 0x87	//short count,
#define EGC_AddMaterial 0x88 //matstruct mm
#define EGC_AddLight 0x89	//CGLight lght
#define EGC_BillboardPos 0x8A //float x,y,z [vychozi pozice]
					//int count [pocet vrcholu]
					//xy,xy,xy... [odchylky od vychozi pozice]
#define EGC_BillboardPosScr 0x8B
#define EGC_SetTPosition 0x8C //int vertex, float x,y,z,q
#define EGC_PushBoneMatrixL 0x8D //long bonehandle;
#define EGC_PushBoneMatrixR 0x8E //long bonehandle;
#define EGC_SetTXITable 0x8F //HTXITABLE txi
#define EGC_Include 0x90 //Recursive include: CGExecBuffer
#define EGC_LightTable 0x91 //LightTable> long ltlen;
#define EGC_2dMatrix 0x92 //2dMatrix pro transformaci billboardu
						  //2x2 float

#define EGC_AddFace 0xE0 // add face command

#define EGL_LDEF 0x80
#define EGL_HANDLE 0x81
#define EGL_INCLUDE 0x82

//  };

/* prikazy pro faces.
[01-7f] - prikaz pro face;
uchar flags - other flags;
uchar vcount - vertex count: max 255 for each face
uchar tcount - texture count: max 8 for each face (texture stages)
[for each vertex]
long relative pos index,
long relative norm index,
long relative col index
float tu1
float tv1
... (texture count)
if (pos==0) [float x,y,z - for position]
if (norm==0) [float x,y,z - for normal]
if (col==0) [matstruct mat]
[]
*/

class CGExecBuffer : public CG3DObject
  {
  protected:
	int bufgrow;
	int bufsize;
	int bufalloc;
	void *buffer;
	bool error;
	long alignmask;
	int markadr;
public:
	CGExecBuffer(long align=4,int inital=2048, int grow=2048);
	void *Alloc(int size, bool align=true);
	bool AddData(void *data,int size, bool align=true);
	void *GetAdr(int adr) {return (void *)((char *)buffer+adr);}
	bool IsValid(void *p) {return (long)buffer<=(long)p && ((long)buffer+bufsize>(long)p);}
	bool IsError() {return error;}
	void Reset() {bufsize=markadr=0;error=false;}
	void Trunc() {bufsize=markadr;}
	void MarkSize() {markadr=bufsize;}
	int GetBufSize() {return bufsize;}
	void FreeExtra() {realloc(buffer,bufalloc=bufsize);}
	virtual ~CGExecBuffer();
	};

#pragma pack (1)
class CGSceneStructs:  public CG3DObject
  {
  protected:
	struct SMatClass
	  {
	  void *matclass;
	  long matstructsize;
	  };
	struct SAddVector
	  {
	  float f[3];
	  };
	struct SetTPosition
	  {
	  HVECTOR vect;
	  HVECTOR rhw;
	  };
	struct SVertex
	  {
	  int pos,norm,col;
	  };
	struct FaceVertex
	  {
      int pos,norm,col;
	  float tuv[1][2];
	  };
	struct FaceInfo
	  {
	  unsigned char vcount;
	  unsigned char face;
	  unsigned char flags;
	  char tcount;
	  long texlist[1];
	  };

  };

class CGSceneBuilder: public CGSceneStructs
  {
  private:
	struct SInclude
	  {
	  unsigned char command;
	  CGExecBuffer *buff;
	  };
	CGExecBuffer& buf;
	int pc,nc,cc;
	int curcolsize;	
	long vcounter;
	long fcounter;
	unsigned char curcom;
	int ntex;
	HTXITABLE lasttxi;
	int MatrixCommand(unsigned char com, HMATRIX mm);
	int InsertObject(void *data, long size, LPHMATRIX mm=NULL, HTXITABLE txi=NULL);
  public:	
    void SetMaterialSize(int size) {curcolsize=size;}
    int AddScene(TSCENEDEF *def, unsigned char flags=0);
	CGSceneBuilder(CGExecBuffer& ebuf): buf(ebuf),
	  pc(0),nc(0),cc(0),curcolsize(sizeof(TVERTEXCOLOR)),
	  curcom(0),lasttxi(0) {}
	int SetOwnMaterial(G3DMATCOLORCALLBACK callback, int structsize);
	int AddPosition(float x, float y, float z);
	int AddPosition(HVECTOR v);
	int AddNormal(float x, float y, float z);
	int AddNormal(HVECTOR v);
	int AddFace(unsigned char face, unsigned char flags, unsigned char nTextures,  ...);
	int AddVertex(int posindex, int normindex, int matindex,...); //tu&tv coords here;
/*	int AddVertex(HVECTOR pos, int normindex, int matindex,...); //tu&tv coords here;
	int AddVertex(int posindex, HVECTOR norm, int matindex,...); //tu&tv coords here;
	int AddVertex(HVECTOR pos, HVECTOR norm, int matindex,...); //tu&tv coords here;
	int AddVertex(int posindex, int normindex, void *mat,...); //tu&tv coords here;
	int AddVertex(HVECTOR pos, int normindex, void *mat,...); //tu&tv coords here;
	int AddVertex(int posindex, HVECTOR norm, void *mat,...); //tu&tv coords here;
	int AddVertex(HVECTOR pos, HVECTOR norm, void *mat,...); //tu&tv coords here;*/
	int AddVertex(int pos, int norm, int mat, double *tutv);
	int AddMaterial(void *mat);
	int PushMatrixL(HMATRIX mm) {return MatrixCommand(EGC_PushMatrixL,mm);}
	int PushMatrixR(HMATRIX mm) {return MatrixCommand(EGC_PushMatrixR,mm);}
	int PopMatrices(short count);
	int AddLight(CGLight& l);
	int AddBillboardVerts(int pos, int count, bool screen, double *vectxy);
	int AddBillboardVerts(int pos, int count, bool screen, ...) 
	  {return AddBillboardVerts(pos,count,screen,(double *)((int *)&screen+1));}
	int AddTPosition(HVECTOR v,HVECTOR tv);
	int SetTXITable(HTXITABLE txi);
	int InsertObject(CGExecBuffer &other, LPHMATRIX mm=NULL, HTXITABLE txi=NULL);
	int IncludeObject(CGExecBuffer &other, LPHMATRIX mm=NULL, HTXITABLE txi=NULL);	
	bool LightTable(unsigned short tabsize)	;
  };



#pragma pack()

//* vlajky na face
#define CGFF_FORCESORT 0x1  //Face bude zatriden a kreslen naposled
#define CGFF_ALPHAFACE 0x2  //Oznacuje face s alphou
#define CGFF_ANTIALIASED 0x4 //Face je antialiazovany, (jako ALPHA)
#define CGFF_NORMAL 0x0 //normalni face

void g3d2SetLightCacheSize(int siz);


#ifndef G3D_MAXHANDLE
#define G3D_MAXHANDLE 1023
#endif
//define G3D_MAXHANDLE before including this header
//if you want more handle numbers in your application!

class CGSceneEnv  
  {
	void **handlelist;
	int count;
public:
	CGSceneEnv(int handles=0);
	bool SetHandle(int index, void *handle);
	virtual ~CGSceneEnv();
	void *operator[] (void *p)
	  {
	  long q=(long)p;
	  if (q>G3D_MAXHANDLE) return p;
	  if (q>=count) return NULL;
	  return handlelist[q];
	  }
  };


#define CGMAKEHANDLE(p) (void *)p

class CGLightBuilder  
{
	CGExecBuffer& buf;
	unsigned short *ltsize;
	unsigned short *ltused;
	unsigned char *ltptr;
	bool AddGen(unsigned char command, void *data, int size);
public:
	bool Add(HLIGHTDEF def) {return AddGen(EGL_LDEF,def,sizeof(*def));}
	bool Add(void *handle) {return AddGen(EGL_HANDLE,&handle,sizeof(handle));}
	bool Include(CGExecBuffer &subbuf) {void *p=subbuf.GetAdr(0);return AddGen(EGL_INCLUDE,&p,sizeof(p));}
	bool Include(void *p) {return AddGen(EGL_INCLUDE,&p,sizeof(p));}
	bool Insert(CGExecBuffer &subbuf);
	void *GetCommandAfter() {if (ltsize) return (void *)((char *)(ltused+1)+ltsize[0]);else return NULL;}
	TLIGHTDEF *GetNextLight(int& index, CGSceneEnv& scnev);
	 //funkce vraci NULL, pokud neni zadny dalsi svetlo,
	 //nebo pokud nasleduje odkaz na subtabulku;
	CGExecBuffer *GetSubtable(int& index, CGSceneEnv& scnev);
	 //funkce vraci ukazatel na subtabulku, nebo NULL, pokud
	 //zadna tabulka nenasleduje
	CGLightBuilder(CGExecBuffer& ebuf);
};
#endif // !defined(AFX_GSCENE2_H__5EF80862_20E5_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
