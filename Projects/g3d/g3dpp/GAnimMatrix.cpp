// GAnimMatrix.cpp: implementation of the CGAnimMatrix class.
//
//////////////////////////////////////////////////////////////////////

#include <string.h>
#include "GAnimMatrix.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

_inline void Fill(int frame,int f1, int f2, LPHMATRIX m1, LPHMATRIX m2, LPHMATRIX mm, int mtype)
  {
  if (f1!=f2)
	{
	float f=((float)(frame-f1)/(float)(f2-f1));
	if (mtype & CGAM_QKEY)
	  {
	  HMATRIX qmm;
	  mxInterpolateKey(m1,m2,f,qmm);
	  mxKeyToMatrix(qmm,mm);
	  }
	else
	  {
	  int i,j;
	  float q=1.0f-f;	  
	  for (i=0;i<4;i++)
		for (j=0;j<4;j++)
		  mm[i][j]=m1[i][j]*q+m2[i][j]*f;
	  }
	}
  else
   if (mtype & CGAM_QKEY)	
 	mxKeyToMatrix(m1,mm);
   else
 	 CopyMatice(m1,mm);
  }

bool CGAnimMatrix::FindFrame(int frame, LPHMATRIX mm)
  {
  LPHMATRIX m1,m2;
  int f1,f2;
  if (CGAnim::FindFrame(frame,(void **)&m1,(void **)&m2,&f1,&f2)==false) return false;
  Fill(frame,f1,f2,m1,m2,mm,mtype);
  return true;
  }

bool CGAnimMatrix::GetFrame(SGAnimState& st, LPHMATRIX mm)
  {
  LPHMATRIX m1,m2;
  int f1,f2;
  if (CGAnim::GetFrame(st,(void **)&m1,(void **)&m2,&f1,&f2)==false) return false;
  Fill(st.framenum,f1,f2,m1,m2,mm,mtype);
  return true;
  }

ostream& operator<< (ostream& out, const CGAnimMatrix& data)
  {
  out.write((char *)&data.mtype,sizeof(data.mtype));
  return out<<(CGAnim &)data;
  }

istream& operator>> (istream& in, CGAnimMatrix& data)
  {  
  in.read((char *)&data.mtype,sizeof(data.mtype));
  return in>>(CGAnim &)data;
  }

LPHMATRIX CGAnimMatrix::GetMatrix(int index)
  {
  void *data=GetData(index,NULL);
  return (LPHMATRIX)data;
  }