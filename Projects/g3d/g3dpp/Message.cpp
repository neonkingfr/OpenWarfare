// Message.cpp: implementation of the CGMessage class.
//
//////////////////////////////////////////////////////////////////////

#include "Message.h"
#include <string.h>
#include <stdlib.h>

static char szBuff[256];

char *inline PrepareBuffer(char *buffer, int& bufsize)
  {
  if (buffer==NULL) 
	{
	bufsize=256;
	return szBuff;
	}
  return buffer;
  }

const char *CGMsgInt::AsString(char *buffer, int bufsize)
{
buffer=PrepareBuffer(buffer,bufsize);
if (_snprintf(buffer,bufsize,"%d",value)>=0) return buffer;
return NULL;
}

const char *CGMsgFloat::AsString(char * buffer, int bufsize)
{
buffer=PrepareBuffer(buffer,bufsize);
if (_snprintf(buffer,bufsize,"%f",value)>=0) return buffer;
return NULL;
}

CGMsgString::CGMsgString(char * textmsg, int msg): CGMessage(msg)
  {
  int len=strlen(textmsg)+1;
  text=new char [len];
  if (text) strcpy(text,textmsg);  
  }

const char *CGMsgString::AsString(char * buffer, int bufsize)
  {
  if (text==NULL) return NULL;
  if (buffer==NULL) return text;
  if (bufsize<1) return NULL;
  buffer[bufsize-1]=0;
  strncpy(buffer,text,bufsize);
  return buffer;
  }

int CGMsgString::AsInt()
  {
  if (text==NULL) return 0;
  return atoi(text);
  }

float CGMsgString::AsFloat()
  {
  if (text==NULL) return 0.0f;
  return (float)atof(text);
  }

double CGMsgString::AsDouble()
  {
  if (text==NULL) return 0.0f;
  return (double)atof(text);
  }

CGMsgString::~CGMsgString()
  {
  free(text); 
  }
