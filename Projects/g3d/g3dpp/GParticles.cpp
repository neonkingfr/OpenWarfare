// GParticles.cpp: implementation of the CGParticles class.
//
//////////////////////////////////////////////////////////////////////

#include "GParticles.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGParticles::CGParticles(void *arr, int size, SGParticle def, CGCALCOLORBETWEEN cc=CGCalcDefMaterialBetween)
  {
  array=(SGParticle *)arr;
  partcount=size/sizeof(SGParticle);
  JednotkovaMatice(genmatrix);
  genradius=0.0f;	  
  vychozi=def;
  ccol=cc;
  memset(rotspeed,0,sizeof(rotspeed));
  nextsystem=NULL;
  pindex=0;
  }

CGParticles::~CGParticles()
  {
  if (nextsystem) nextsystem->Release();
  }

void CGParticles::AttachSystem(CGParticles * next)
  {
  if (nextsystem==next) return;
  if (nextsystem) nextsystem->Release();
  Attach(next);
  nextsystem=next;
  }

_inline float frand(float min, float max)
  {
  if (max==min) return min;
  float p=(float)rand()/(float)RANDMAX*(max-min);
  return p+min;
  }

bool CGParticles::AddParticle(HMATRIX modf)
  {
  if (partcount==0) return false;
  int i=pindex;
  do
	{
	if (i>=partcount) i=0;
	if (array[i].flags==SGPP_NOTUSED) break;
	i++;
	}
  while (i!=pindex);
  if (array[i].flags!=SGPP_NOTUSED) return false;
  SGParticle *p=array+i;
  *p=vychozi;
  HVECTOR v1,v2,tv1,tv2;
  HMATRIX nw;
  LPHMATRIX *use;
  if (modf==NULL) use=genmatrix;
  else {SoucinMatic(genmatrix,modf,nw);use=nw;}
  float a=frand(0,genradius);
  float b=frand(0,2*FPI);
  float c=(float)sin(a);
  CopyVektor(v1,Vector3((float)(c*cos(b)),(float)(c*sin(b)),(float)cos(a));
  CopyVektor(v2,Vector3(0,0,0));
  TransformVector(genmatrix,v1,tv1);
  TransformVector(genmatrix,v2,tv2);
  RozdilVektoru(v1,v2);
  c=frand(minspeed,maxspeed);
  SoucinVektoru(v1,c);
  CopyVektor(p->pos,v2);
  CopyVektor(p->dir,v2);
  p->rotofs[XVAL]=frand(p->rotspeedmin[XVAL],p->rotspeedmax[XVAL]);
  p->rotofs[YVAL]=frand(p->rotspeedmin[YVAL],p->rotspeedmax[YVAL]);
  p->rotofs[ZVAL]=frand(p->rotspeedmin[ZVAL],p->rotspeedmax[ZVAL]);
  p->phase=0.0f;  
  }
