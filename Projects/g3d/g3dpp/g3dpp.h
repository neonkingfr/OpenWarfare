#ifndef _G3DPP_H_
#define _G3DPP_H_

#include "GBase.h"
#include "GScene2.h"
#include "GParser.h"

struct SVertexProcessInfo
  {
  HMATRIX mm;
  float lclip,rclip,tclip,bclip,nclip,fclip;
  };

#include "GFaceSort.h"

class CGRenderParser: public CGParser
  {
  protected:
	SVertexProcessInfo si;
  	CGSceneEnv &env;
	CGCamera2 &cam;
	unsigned long flags;
	int lglevel; //pouze v lglevel=0 jsou svetla validni;
	HMATRIX camtoscreen; //matice pro prepocet z kamery na obrazovku
	HMATRIX screentocam; //matice pro prepocet z obrazovky na kameru
	int AddBillboard(int v, int count, float *data, bool trns);
	HTXITABLE txi;
	char curcull; //aktualni hodnota cullmode;
	bool dblsided; //posledni face byla dvoustranna;
  public:
    int SortedFaces(SGFaceInfo *fi);
    void PostRender();
	CGSceneEnv &GetEnv() {return env;}
	CGRenderParser(CGExecBuffer &scn, CGSceneEnv &e, CGCamera2 &c, unsigned long f):
		CGParser(scn),env(e),cam(c),flags(f) {}
	int PrepareToRender();
	virtual int AddPosition(const LPHVECTOR v, int count);
	virtual int AddNormal(const LPHVECTOR v, int count);
	virtual int PushMatrix(const LPHMATRIX m,bool left);
	virtual int PopMatrices(int count);
	virtual int AddMaterial(const void *material);
	virtual int AddBillboardPos(int v, int count, float *data);
	virtual int AddBillboardScr(int v, int count, float *data);
	virtual int SetTPosition(const LPHVECTOR v, int count);
	virtual int SetTXITable(HTXITABLE txi);
	virtual int Light(const TLIGHTDEF *def);
	virtual int Face(const FaceInfo *face,const FaceVertex *fvert);
  };

int g3dRender(CGExecBuffer &scn, CGSceneEnv &env, CGCamera2 &cam, unsigned long flags);

#define G3DR_NORMAL 0 //normalni renderovani
#define G3DR_SORTALPHA 1 //zarazuje alpha
#define G3DR_NEVERSORT 2 //nikdy nezarazuje
#define G3DR_SORTALWAYS 3 //vzdy zarazuje
#define G3DR_DISABLELENSFLARES 4 //nekresli se lensflares
#define G3DR_NOLIGHTS 8 //neprochazi se tabulky svetel

typedef unsigned long (*GETVERTEXCOLORCALLBACK)(const void *mat, float *light);
#endif