// GAnimSkeleton.h: interface for the CGAnimSkeleton class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GANIMSKELETON_H__FF97B5A6_42EA_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_GANIMSKELETON_H__FF97B5A6_42EA_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "GAnimMatrix.h"

  class CGAnimSkeleton : public CG3DObject  
{
protected:
	bool ValidateStates(int *states) {return states!=NULL && states[0]==bonecount;} 
	bool Expand();
	CGAnimMatrix **bones;
	int grow;
	int bonecount;
	int allocated;
	int maxframe;
public:	
	bool GotoFrame(int frame, int *states);
	CGAnimMatrix * GetBone(int index);
	LPHMATRIX GetMatrix(int bone);
	bool CalcSkeleton(int *states);
	bool CombineSkeleton(CGAnimSkeleton& skell);
	int *AllocStates(int flags);
	bool NextFrame(int flags, int *states);
	int AddBone(CGAnimMatrix *mm, int hierarchy);
	int GetMaxFrame() {return maxframe;}
	int GetBoneCount() {return bonecount;}
	CGAnimSkeleton(int grow=16);
	virtual ~CGAnimSkeleton();
	friend ostream& operator<<(ostream& out,const CGAnimSkeleton &skell);
	friend istream& operator>>(istream& out,CGAnimSkeleton &skell);
	
};

#endif // !defined(AFX_GANIMSKELETON_H__FF97B5A6_42EA_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
