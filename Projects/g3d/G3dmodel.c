#define _DLGSH_INCLUDED_
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include "G3DLIB.H"
#include "G3DMODEL.H"

#define FindPhase(p,n) while (p!=NULL && p->phasenumber!=(n) ) p=p->next;

static unsigned long animticker;

char g3dmCreateModel(HMODEL model) //vytvori obal pro animaci
  {
  memset(model,0,sizeof(TMODELHANDLE));
  return G3DM_OK;
  }

char g3dmCreatePhase(HMODEL model,int phase) //vytvori fazi animace
  {
  TANIMPHASER *phs,*c;

  phs=model->mainphaser;
  FindPhase(phs,phase);
  if (phs!=NULL) return G3DM_PHASEEXISTS;
  phs=malloc(sizeof(TANIMPHASER));
  if (phs==NULL) return G3DM_NOMEMORY;
  if (model->mainphaser==NULL) model->mainphaser=phs;
  else
    {
    c=model->mainphaser;
    while (c->next!=NULL)  c=c->next;
    c->next=phs;
    }
  phs->clones=1;
  phs->phasenumber=phase;
  phs->frames=NULL;
  phs->framecount=0;
  phs->logiccount=0;
  phs->next=NULL;
  model->curphase=phs;
  model->curphasenum=phase;
  model->curframe=phs->frames;
  model->curframenum=0;
  return G3DM_OK;
  }

char g3dmChangePhaseEx(HMODEL model,int phase,char dir) //zmeni aktualni fazi animace
  {
  TANIMPHASER *phs;
  phs=model->mainphaser;
  FindPhase(phs,phase);
  if (phs==NULL) return G3DM_PHASENOTEXISTS;
  model->curphasenum=phase;
  model->curphase=phs;
  model->curframe=phs->frames;
  model->curframenum=0;
  model->flags &= ~G3DMH_SMOOTHPHASE;
  if (dir<0) g3dmChangeFrame(model,phs->logiccount-1);
  model->animdir=dir;
  return G3DM_OK;
  }

#define TestCurPhase(model) \
     if ((model)->curphase==NULL) return G3DM_NOCURRENTPHASE; else\
     if (((model)->curphase)->phasenumber!=(model)->curphasenum) return G3DM_PHASELOST\


char g3dmSetFrameCount(HMODEL model,int framecount) //nastavi logicky animacnich pocet policek
  {
  TestCurPhase(model);
  (model->curphase)->logiccount=framecount;
  return G3DM_OK;
  }

char g3dmDeletePhase(HMODEL model,int phase) //maze nejakou fazi
  {
  TANIMPHASER *phs,*old;
  TANIMMODELFRAME *frst;

  phs=model->mainphaser;
  old=NULL;
  while (phs!=NULL && phs->phasenumber!=phase) {old=phs;phs=phs->next;}
  if (phs==NULL) return G3DM_PHASENOTEXISTS;
  frst=phs->frames;
  while (phs->frames!=NULL)
    {
    TANIMMODELFRAME *m;
    m=phs->frames;
    g3dDestroyScene(&m->model);
    phs->frames=m->next;
    free(m);
    if (phs->frames==frst) break;
    }
  if (old==NULL)
    {
    old=phs->next;
    if (old==NULL) return G3DM_USEDESTROYMODEL;
    old->clones=phs->clones;
    memcpy(phs,old,sizeof(*phs));
    }
  else
    {
    old->next=phs->next;
    free(phs);
    }
  return G3DM_OK;
  }

static void DestroyFrame(TANIMMODELFRAME *f)
  {
  if (~f->flags & G3DMF_ALIAS) g3dDestroyScene(&f->model);
  }

char g3dmAddFrameEx2(HMODEL model, TSCENEDEF *scene, int frame, TG3DM_FRAME *finfo) //prida frame specifickeho cisla.
  {
  TANIMPHASER *phs;
  TANIMMODELFRAME *frm,*f,*q;
  TestCurPhase(model);
  phs=model->curphase;
  frm=malloc(sizeof(*frm));
  if (frm==NULL) return G3DM_NOMEMORY;
  memset(frm,0,sizeof(*frm));
  if (finfo==NULL)
    {
    frm->model=*scene;
    frm->framenum=frame;
    frm->flags=0;
    }
  else
    {
    int flags=finfo->flags;
    frm->framenum=frame;
    if (flags & G3DMF_ALIAS)
      {
      f=phs->frames;
      if (f!=NULL)
        {
        while (f->next!=phs->frames && f->framenum!=finfo->aliasframe) f=f->next;
        if (f->framenum!=finfo->aliasframe) f=NULL;
        }
      if (f==NULL)
        {
        free(frm);
        return G3DM_FRAMENOTFOUND;
        }
      frm->alias=f;
      frm->aliasframe=finfo->aliasframe;
      }
    else
      {
      if (scene!=NULL) frm->model=*scene;
      }
    if (flags & G3DMF_TRANSLATE) frm->xr=finfo->xr,frm->yr=finfo->yr,frm->zr=finfo->zr;
      else frm->xr=frm->yr=frm->zr=0.0f;
    if (flags & G3DMF_ROTATEX) frm->rotx=finfo->rotx;else frm->rotx=0.0f;
    if (flags & G3DMF_ROTATEY) frm->roty=finfo->roty;else frm->roty=0.0f;
    if (flags & G3DMF_ROTATEZ) frm->rotz=finfo->rotz;else frm->rotz=0.0f;
    if (flags & G3DMF_SCALE) frm->scalx=finfo->scalx,frm->scaly=finfo->scaly,frm->scalz=finfo->scalz;
      else frm->scalx=frm->scaly=frm->scalz=1.0f;
    frm->flags=flags;
    frm->refpoint=finfo->refpoint;
    }
  f=phs->frames;
  if (f==NULL) phs->frames=frm,frm->next=frm;
  else if (f->framenum>frame) phs->frames=frm,frm->next=f;
  else
    {
    q=f;
    while (f->next!=phs->frames && f->framenum<frame) q=f,f=f->next;
    if (f->framenum==frame)
      {
      DestroyFrame(f);
      frm->next=f->next;*f=*frm;
      free(frm);
      }
    else if (f->framenum<frame)
      {
      frm->next=f->next;
      f->next=frm;
      }
    else
      {
      frm->next=q->next;
      q->next=frm;
      }
    }
  phs->framecount=max(phs->framecount,frame);
  phs->logiccount=max(phs->logiccount,phs->framecount);
  if (~frm->flags & G3DMF_ALIAS) memset(scene,0,sizeof(TSCENEDEF));
  return G3DM_OK;
  }
char g3dmAddFrame(HMODEL model,TSCENEDEF *scene) //Pridava policko do faze
  {
  char res;
  TANIMPHASER *phs;
  TestCurPhase(model);
  phs=model->curphase;
  res=g3dmAddFrameEx(model,scene,phs->framecount+1)
  return res;
  }


char g3dmAttachTextureXlat(HMODEL model,unsigned short *texture_xlat,int xlatcount,char copy)
  {
  unsigned short *tt;

  if (model->flags & G3DMH_ALLOCATED)
    {
    free(model->texture_xlat);
    model->flags&=~G3DMH_ALLOCATED;
    }
  if (copy && texture_xlat!=NULL && xlatcount!=0)
    {
    tt=malloc(xlatcount*sizeof(*tt));
    if (tt==NULL) return G3DM_NOMEMORY;
    memcpy(tt,texture_xlat,xlatcount*sizeof(*tt));
    texture_xlat=tt;
    model->flags|=G3DMH_ALLOCATED;
    }
  model->texture_xlat=texture_xlat;
  model->xlatcount=xlatcount;
  return G3DM_OK;
  }


char g3dmCloneModel(HMODEL model,HMODEL clone) //Klonuje model pro zmenu zakladnich vlastnosti
  {
  memcpy(clone,model,sizeof(TMODELHANDLE));
  (model->mainphaser)->clones++;
  return G3DM_OK;
  }

char g3dmDestroyModel(HMODEL model)
  {
  TANIMPHASER *phs,*x;
  TANIMMODELFRAME *frst;

  phs=model->mainphaser;
  phs->clones--;
  g3dmAttachTextureXlat(model,NULL,0,0); //Dettach texture map.
  memset(model,0,sizeof(TMODELHANDLE));
  if (phs->clones) return G3DM_OK;
  while (phs!=NULL)
    {
    frst=phs->frames;
    while (phs->frames!=NULL)
      {
      TANIMMODELFRAME *m;
      m=phs->frames;
      DestroyFrame(m);
      phs->frames=m->next;
      free(m);
      if (phs->frames==frst) break;
      }
    x=phs;
    phs=x->next;
    free(x);
    }
  return G3DM_DESTROYED;
  }

char g3dmAnimate(HMODEL model) //Pricte 1 k aktualni frame.
  {
  TANIMPHASER *phs;
  int p;

  TestCurPhase(model);
  phs=model->curphase;
  p=model->curframenum+1;
  if (p>=phs->logiccount)
    if (phs->flags & G3DMP_NOLOOP) return G3DM_LASTFRAME;
  else
    p=0;
  return g3dmChangeFrame(model,p);
  }

char g3dmChangeFrame(HMODEL model,int frame) //skokove zmeni aktualni frame
  {
  TANIMPHASER *phs;
  TANIMMODELFRAME *frm;
  int p,b,e;

  if (model->flags & G3DMH_SMOOTHPHASE)
    {
    model->curframenum=frame;
    frame=model->curframenum-model->smoothlen;
    if (frame<0) return G3DM_OK;
    }
  model->flags &= ~G3DMH_SMOOTHPHASE;
  TestCurPhase(model);
  phs=model->curphase;
  frm=model->curframe;
  if (frm==NULL) frm=phs->frames;
  if (frm==NULL) return G3DM_FRAMEOUTRANGE;
  if (phs->logiccount<=frame) return G3DM_FRAMEOUTRANGE;
  if (frame<0) return G3DM_FRAMEOUTRANGE;
  model->curframenum=frame;
  p=model->curframenum*phs->framecount/phs->logiccount;
  do
    {
    b=frm->framenum;
    if (frm->next==phs->frames) e=phs->framecount;else e=(frm->next)->framenum;
    if (p>=b && p<e) break;
    frm=frm->next;
    }
  while (1);
  model->curframe=frm;
  return G3DM_OK;
  }

char g3dmLoadInterpolated(TSCENEDEF *s1,TSCENEDEF *s2, float parc, HMATRIX mm, unsigned short *xlat,int xlatsize)
  {
  TSCENEDEF prac;
  int res,i;
  float iparc=1-parc;

  if (s1==s2) return G3DM_SAMEOBJECT;
  if (s1->wsize!=s2->wsize) return G3DM_CANNOTINTERPOLATE;
  prac=*s1;
  prac.wsize=0;
  res=g3dLoadObjectEx(&prac,mm,xlat,xlatsize);
  if (res) return G3DM_G3DERROR;
    {
    TWORLDVERTEX *cvt1=s1->wtable;
    TNORMALVERTEX *nt1=s1->ntable;
    TWORLDVERTEX *cvt2=s2->wtable;
    TNORMALVERTEX *nt2=s2->ntable;
    HVECTOR v;

    for (i=0;i<s1->wsize;i++,cvt1++,cvt2++)
      {
      v[XVAL]=cvt1->vector[XVAL]*iparc+cvt2->vector[XVAL]*parc;
      v[YVAL]=cvt1->vector[YVAL]*iparc+cvt2->vector[YVAL]*parc;
      v[ZVAL]=cvt1->vector[ZVAL]*iparc+cvt2->vector[ZVAL]*parc;
      v[WVAL]=cvt1->vector[WVAL]*iparc+cvt2->vector[WVAL]*parc;
      if (g3dAddPosition(v)==-1) return G3DM_G3DERROR;
      }
    }
  return G3DM_OK;
  }

static char g3dmAutoAnimate(HMODEL model,  TANIMPHASER *frm)
  {
  long diff;
  long add;

  if (model->flags & G3DMH_SPEED)
    {
    diff=(long)((animticker-model->curanimtick)/model->speed);
    add=(long)(diff*model->speed);
    }
  else
    {
    diff=(animticker-model->curanimtick);
    add=diff;
    }
  if (diff>0)
    {
    if (frm!=NULL)
      {
      long lc=frm->logiccount;

      if (model->animdir==G3DMD_BACKWARD) diff=-diff;
      diff+=model->curframenum;
      if (frm->flags & G3DMP_NOLOOP) {lc--;diff=max(min(diff,(long)lc),0);}
      else if (diff<0)diff+=lc;else if (diff>=lc) diff-=lc;
      g3dmChangeFrame(model,diff);
      model->curanimtick+=add;
      }
    else
      model->curframenum+=diff;
    }
  return G3DM_OK;
  }


static char BuildFrameMatrix(TANIMMODELFRAME *frm1, TANIMMODELFRAME *frm2, HMATRIX m, float ip)
  {
  int flags=frm1->flags | frm2->flags;
  float ir1,ir2;
  HMATRIX mp;

  if (!(flags & G3DMF_TRANSFORM)) return 0;
  if (flags & G3DMF_SMOOTH) ip=(1.0f-(float)cos(ip*3.1415))/2.0f;
  ir1=1-ip;ir2=ip;
  JednotkovaMatice(m);
  if (flags & G3DMF_SCALE)
    {Zoom(mp,frm1->scalx*ir1+frm2->scalx*ir2,
                 frm1->scaly*ir1+frm2->scaly*ir2,
                 frm1->scalz*ir1+frm2->scalz*ir2);SoucinMatic(m,mp,m);}
  if (flags & G3DMF_ROTATEX)
    {RotaceX(mp,frm1->rotx*ir1+frm2->rotx*ir2);SoucinMatic(m,mp,m);}
  if (flags & G3DMF_ROTATEY)
    {RotaceY(mp,frm1->roty*ir1+frm2->roty*ir2);SoucinMatic(m,mp,m);}
  if (flags & G3DMF_ROTATEZ)
    {RotaceZ(mp,frm1->rotz*ir1+frm2->rotz*ir2);SoucinMatic(m,mp,m);}
  if (flags & G3DMF_TRANSLATE)
    {Translace(mp,frm1->xr*ir1+frm2->xr*ir2,
                 frm1->yr*ir1+frm2->yr*ir2,
                 frm1->zr*ir1+frm2->zr*ir2);SoucinMatic(m,mp,m);}
  return 1;
  }

char g3dmGetInterFrame(HMODEL model, TANIMMODELFRAME **fr1, TANIMMODELFRAME **fr2,  float *pp, HMATRIX hm)
  {
  float p;
  TANIMPHASER *phs;
  TANIMMODELFRAME *frm,*frm1,*frm2;
  int b,e;
  int res;

  TestCurPhase(model);
  phs=model->curphase;
  frm=model->curframe;
  if (frm==NULL) return G3DM_NOCURRENTFRAME;  //hledani aktualniho frame
  if (model->flags & G3DMH_SMOOTHPHASE)
    {
    frm1=model->lastframe;
    frm2=model->curframe;
    if (model->flags & G3DMH_AUTOANIMATE) g3dmAutoAnimate(model,NULL);
    p=(float)model->curframenum/(float)model->smoothlen;
    if (p>=1.0f)
      {
      p=1.0f;
      model->curframenum=model->curframe->framenum;
      model->flags &= ~G3DMH_SMOOTHPHASE;
      }
    }
  else
    {
    if (model->flags & G3DMH_AUTOANIMATE)     //autoanimace, pokud je zapnuta
      {
      g3dmAutoAnimate(model, phs);
      frm=model->curframe;
      }
    p=(float)model->curframenum*(float)phs->framecount/(float)phs->logiccount;
    b=frm->framenum;            //vypocit mezisnimku
    e=(frm->next)->framenum;
    if (e<=b) e=phs->framecount;
    if (p<b || p>e) return G3DM_NOCURRENTFRAME; //kontrola mezisnimku
    if (e==b) p=0;else p=((float)(p-b))/(e-b);
    frm1=frm;                             //frm1 je levy mezisnimek
    frm2=frm->next;                       //frm2 je pravy mezisnimek
    }
  *pp=p;
  if (BuildFrameMatrix(frm1,frm2,hm,p))       //vypocet transformacni matice
    res=G3DM_OK;
  else
    res=G3DM_NOMATRIX;
  if (frm1->flags & G3DMF_ALIAS) frm1=frm1->alias;
  if (frm2->flags & G3DMF_ALIAS) frm2=frm2->alias;
  *fr1=frm1;
  *fr2=frm2;
  return res;
  }

char g3dmLoadModelFrameEx(HMODEL model,HMATRIX mm, char mode) //nacita model do sceny
  {
  float p;
  TANIMMODELFRAME *frm1,*frm2,*frm;
  TANIMPHASER *phs;
  HMATRIX md;
  HMATRIX *hm;
  int res;

  if (mode==G3DML_GETMATRIX)
    if (mm==NULL) return G3D_INVALIDPARAMS;
    else
      return g3dmGetInterFrame(model,&frm1,&frm2,&p,mm);
  res=g3dmGetInterFrame(model,&frm1,&frm2,&p,md);
  phs=model->curphase;
  frm=model->curframe;
  if (res!=G3DM_OK && res!=G3DM_NOMATRIX) return res;
  if (res==G3DM_OK)
    {
    hm=&md;
    if (mm!=NULL) SoucinMatic(md,mm,md);
    }
  else
    hm=(HMATRIX *)mm;
  if (frm1!=phs->frames && frm->model.fcsize==0)
    g3dLoadObjectEx(&(phs->frames)->model,*hm,model->texture_xlat,model->xlatcount);
  if (p<0.00001)
    {
    res=g3dLoadObjectEx(&frm1->model,*hm,model->texture_xlat,model->xlatcount);
    if (res==-1) return G3DM_G3DERROR;
    }
  else
    {
    res=g3dmLoadInterpolated(&frm1->model,&frm2->model,p,*hm,model->texture_xlat,model->xlatcount);
    if (res==G3DM_SAMEOBJECT || res==G3DM_CANNOTINTERPOLATE)
      {
      res=g3dLoadObjectEx(&frm1->model,*hm,model->texture_xlat,model->xlatcount);
      if (res==-1) return G3DM_G3DERROR;
      }
    else return res;
    }
  if (phs->flags & G3DMP_NOTIFYEND && !(model->flags & G3DMH_SMOOTHPHASE))
    if (model->animdir>-1 && model->curframenum==phs->logiccount-1) return G3DM_LASTFRAME;
    else if (model->animdir<0 && model->curframenum==0) return G3DM_LASTFRAME;
  return G3DM_OK;
  }

/*  float p;
  TANIMPHASER *phs;
  TANIMMODELFRAME *frm,*frm1,*frm2;
  int res,b,e;
  HMATRIX m,mt;


  TestCurPhase(model);
  phs=model->curphase;
  frm=model->curframe;
  if (frm==NULL) return G3DM_NOCURRENTFRAME;  //hledani aktualniho frame
  if (model->flags & G3DMH_AUTOANIMATE)     //autoanimace, pokud je zapnuta
    {
    g3dmAutoAnimate(model, phs);
    frm=model->curframe;
    }
  p=(float)model->curframenum*(float)phs->framecount/(float)phs->logiccount;
  b=frm->framenum;            //vypocit mezisnimku
  e=(frm->next)->framenum;
  if (e<=b) e=phs->framecount;
  if (p<b || p>e) return G3DM_NOCURRENTFRAME; //kontrola mezisnimku
  p=((float)(p-b))/(e-b);
  frm1=frm;                             //frm1 je levy mezisnimek
  frm2=frm->next;                       //frm2 je pravy mezisnimek
  if (mode==G3DM_GETMATRIX)             //v rezimu GETMATRIX
    {
    if (mm==NULL) return G3D_INVALIDPARAMS; //zjisti, zda mame kam ulozit vysledek
    if (!BuildFrameMatrix(frm1,frm2,mm,p)) return G3DM_NOMATRIX;      //vypocti matici
    return G3DM_OK;                         //vrat vysledek OK
    }
  if (BuildFrameMatrix(frm1,frm2,m,p))       //vypocet transformacni matice
    if (mm!=NULL)
      {
      SoucinMatic(m,mm,mt);
      hm=&mt;                               //matice spocitana, pripocti zakladni
      }
    else
      hm=&m;
  else hm=(HMATRIX *)mm;                           //matice nespocitana, pouzije se zakladni
  if (frm1->flags & G3DMF_ALIAS) frm1=frm1->alias;
  if (frm2->flags & G3DMF_ALIAS) frm2=frm2->alias;
  if (frm1!=phs->frames && frm->model.fcsize==0)
    g3dLoadObjectEx(&(phs->frames)->model,*hm,model->texture_xlat,model->xlatcount);
  if (p<0.00001)
    {
    res=g3dLoadObjectEx(&frm1->model,*hm,model->texture_xlat,model->xlatcount);
    if (res==-1) return G3DM_G3DERROR;
    }
  else
    {
    res=g3dmLoadInterpolated(&frm1->model,&frm2->model,p,*hm,model->texture_xlat,model->xlatcount);
    if (res==G3DM_SAMEOBJECT || res==G3DM_CANNOTINTERPOLATE)
      {
      res=g3dLoadObjectEx(&frm1->model,*hm,model->texture_xlat,model->xlatcount);
      if (res==-1) return G3DM_G3DERROR;
      }
    else return res;
    }
  if (phs->flags & G3DMP_NOTIFYEND && model->curframenum==phs->logiccount-1) return G3DM_LASTFRAME;
  return G3DM_OK;
  }

*/

char g3dmAddLoopBack(HMODEL model, int frame)
  {
  TANIMPHASER *phs;
  TANIMMODELFRAME *frm;
  int minframe;

  TestCurPhase(model);
  phs=model->curphase;
  frm=phs->frames;
  minframe=frm->framenum;frm=frm->next;
  while (frm!=phs->frames)
    {
    minframe=frm->framenum;
    frm=frm->next;
    }
  if (minframe>=frame) return G3DM_FRAMEOUTRANGE;
  phs->framecount=frame;
  return G3DM_OK;
  }

char g3dmChangePhaseFlags(HMODEL model, long setflags, long resetflags)
  {
  TANIMPHASER *phs;

  TestCurPhase(model);
  phs=model->curphase;
  phs->flags&=~resetflags;
  phs->flags|=setflags;
  return G3DM_OK;
  }

long g3dmAnimTick(int speed)
  {
  static int intcount=0;

  if (intcount) intcount--;
  else
    {
    intcount=speed;
    animticker+=1;
    }
  return animticker;
  }

long g3dmGetAnimTick()
  {
  return animticker;
  }


char g3dmSetFrameCountEx(HMODEL model,int fps)
  {
  TANIMPHASER *phs;

  phs=model->mainphaser;
  while (phs!=NULL)
    {
    phs->logiccount=phs->framecount*fps/1000;
    phs=phs->next;
    }
  return G3DM_OK;
  }

char g3dmCalcRefMatrix(HMODEL model, int rpref, HMATRIX mref,HMATRIX res)
  {
  HMATRIX temp1;
  HMATRIX temp2;
//  TANIMPHASER *phs;
  TWORLDVERTEX *wv1,*wv2,wc,wt;
  TANIMMODELFRAME *frm1,*frm2;
  float p,q,w1,w2;
  char r;

  r=g3dmGetInterFrame(model,&frm1,&frm2,&p,temp1);
  if (r!=G3DM_NOMATRIX && res!=G3DM_OK) return r;
  if (rpref>=frm1->model.ssize) return G3DM_NOMATRIX;
  wv1=frm1->model.wtable+frm1->model.stable[rpref+frm1->refpoint].wvindex;
  wv2=frm2->model.wtable+frm2->model.stable[rpref+frm2->refpoint].wvindex;
  q=1-p;
  w1=1/(wv1->vector[WVAL]);
  w2=1/(wv2->vector[WVAL]);
  q*=w1;
  p*=w2;
  wc.vector[XVAL]=wv1->vector[XVAL]*q+wv2->vector[XVAL]*p;
  wc.vector[YVAL]=wv1->vector[YVAL]*q+wv2->vector[YVAL]*p;
  wc.vector[ZVAL]=wv1->vector[ZVAL]*q+wv2->vector[ZVAL]*p;
  wc.vector[WVAL]=1.0;
  if (r!=G3DM_NOMATRIX) TransformVector(temp1,wc.vector,wt.vector);else wt=wc;
  if (wt.vector[WVAL]!=1.0f) CorrectVector(wt.vector);
  Translace(temp2,wt.vector[XVAL],wt.vector[YVAL],wt.vector[ZVAL]);
  if (mref==NULL) memcpy(res,temp2,sizeof(HMATRIX));
  else SoucinMatic(temp2,mref,res);
  return G3DM_OK;
  }

char g3dmChangePhaseSmooth(HMODEL model, int phase, int animcount)
  {
  if (model->curframe==NULL) return G3DM_NOCURRENTFRAME;
  model->lastframe=model->curframe;
  model->smoothlen=abs(animcount);
  model->curframenum=0;
  model->flags|=G3DMH_SMOOTHPHASE;
  g3dmChangePhaseEx(model,phase,(char)(animcount>0-animcount<0));
  return G3DM_OK;
  }

