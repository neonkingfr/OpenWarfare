#ifndef _SELHW_H_
#define _SELHW_H_

#ifdef __cplusplus
extern "C" {
#endif
//------------------ OBSOLETTE ------------------
typedef struct _resolution_list
  {
  int xres,yres,cols,href,flags;
  }HWRESOLUTION;


typedef struct _more_dlg
  {
  HINSTANCE hInst;	  //instance aplikace
  const char *szResource;	//jmeno zdroje pro MORE dialog
  DLGPROC lpDlgProc;	  //jmeno procedury pro obsluhu MORE dialogu
  long lParam;		//volitelny parametr
  }HWMOREDLG;

typedef struct _hw_dlg_info
  {
  unsigned long dwFlags;  //some flags
  HWRESOLUTION *lpRList;	//pass pointer to user resolution list
  long dwRListSize;			  //pass size of resolution list sizeof(HWRESOLUTION)*count
  HWMOREDLG *lpMore;	//More button definition
  const char **lpszCustomList;	//Seznam volitelnych voleb zakoncenych ukazatelem na NULL
  const char *szCustomName; //titulek u volitelnych voleb;
  const char *szProfile;	  //jmeno profilu, kam se ulozi nastaveni (soubor)
  const char *szTitle; //titulek v okne;
  const char *szDllPath; //cesta k dll Genie3D
  int iCustomSelect;	//index do customlistu. (in/out)
  int iHwResult;  //out: cislo zarizeni pro SelectHardware
  char *szDeviceName; //out: jmeno knihovny pro obsluhu Genie3D.
  HWRESOLUTION sResolution;	  //out: selected resolution in combobox
  }HWCONFIG;

#define HW_NOGLIDE 0x1 //nelze vybrat pro GLIDE
#define HW_NODX 0x2	//nelze vybrat pro D3D
#define HW_NOOPENGL 0x4  //nelze vybrat pro OPENGL
#define HW_ADD640X480 0x8  //prida standardni 640x480
#define HW_ADD800X600 0x10  //prida standardni 800x600
#define HW_ADD1024X768 0x20  //prida standardni 1024x768
#define HW_DONTLOAD 0x40 //necte data z profile file.
#define HW_DONTSAVE 0x80 //nezapisuje data do profile.
#define HW_CZECHLANG 0x100	//dialog je v �e�tin�
#define HW_IMINVALUES 0x200 //iMinX, iMinY, iMinBpp are used
#define HW_IMAXVALUES 0x400 //iMaxX, iMaxY, iMaxBpp are used

//-------------------- NEW INTERFACE

typedef struct _hw_dlg_info2
  {
  unsigned long dwSize;  //size of struct
  unsigned long dwFlags;  //some flags
  int iMinX,iMinY,iMaxX,iMaxY,iMinBpp,iMaxBpp; //maximal and minimal resolutions
  HWMOREDLG *lpMore;	//More button definition
  const char **lpszCustomList;	//Seznam volitelnych voleb zakoncenych ukazatelem na NULL
  const char *szCustomName; //titulek u volitelnych voleb;
  const char *szProfile;	  //jmeno profilu, kam se ulozi nastaveni (soubor)
  const char *szTitle; //titulek v okne;
  const char *szDllPath; //cesta k dll Genie3D
  int iCustomSelect;	//index do customlistu. (in/out)
  int iHwResult;  //out: cislo zarizeni pro SelectHardware
  char *szDeviceName; //out: jmeno knihovny pro obsluhu Genie3D.
  HWRESOLUTION sResolution;	  //out: selected resolution in combobox
  }HWCONFIG2;

char g3dConfigureHardware(HWCONFIG2 *cfg,unsigned long autoinit,...);

#define HWA_LOADDEVICE 0x1
#define HWA_SELECTHARDWARE 0x2
#define HWA_INITSCREEN 0x4
#define HWA_INITALL (HWA_LOADDEVICE|HWA_SELECTHARDWARE|HWA_INITSCREEN)
#define HWA_NONE 0

#define G3DRES(cfg) (cfg)->sResolution.xres,(cfg)->sResolution.yres,(cfg)->sResolution.cols,(cfg)->sResolution.href

#ifdef __cplusplus
  }
#endif
#endif
//vraci -1 pokud nelze najit DLL.

