#ifndef _G3DBODY_H_

#define _G3DBODY_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef float POINT3D[3];

typedef struct _tagBodyVector
  {
  POINT3D pt;
  unsigned short bone;
  unsigned short flags;
  }TBODYVECTOR;

typedef struct _tagBoneg3d
  {
  float length;
  POINT3D base;
  POINT3D direction;
  POINT3D key1;
  POINT3D key2;
  }TG3DBONE,*HG3DBONE;

typedef struct _tagBodyg3d
  {
  int count;
  int listlen;
  TG3DBONE list[1];
  }TG3DBODY,*HG3DBODY;


HG3DBODY g3dbCreateBody(int bonecount);
//tvorba kostry
int g3dbAddBone(HG3DBODY bd, POINT3D beg, POINT3D end, POINT3D normal);
//vklada kost v absolutnich souradnicich
//normal je predek kosti
int g3dbAddKnee(HG3DBODY bd, int b1, int b2, float factor);
//vklada koleno mezi dve kosti, vyzaduje dve kosti a faktor od 0 az 1 = bezne 0.5)
int g3dbAddBoneRel(HG3DBODY bd, int root, POINT3D direction, float len, POINT3D normal);
//vklada kost relativne vuci jine kosti (navaze na konec)
//dalsi kost je navazan ve smeru "direction"

#define g3dbNeedExpand(bd) ((bd)->count==(bd)->listlen)
char g3dbExpandBody(HG3DBODY *bd,int items);

//ukonceni tvorby
char g3dbDoneCreating(HG3DBODY *bd);
//zmensi velikost pameti alokovane kostrou.


//tyto prikazy pracuji se SCENEDEF. Zacatek kresleni musi byt uvozeno
//funkci g3dBeginScene. Kresleny objekt nelze zobrazit.

int g3dbAddPosition(HG3DBODY bd, unsigned short bonindex,HVECTOR vect);
int g3dbAddNormal(HG3DBODY bd, unsigned short bonindex, HVECTOR vect);
char g3dbAttachObject(HG3DBODY bd, unsigned short boneindex, TSCENEDEF *object);

//zobrazovani model podle kostry

  
char g3dbLoadObjectEx(HG3DBODY bd, TSCENEDEF *object, HMATRIX matrix, unsigned short *texture_table, int count);
#define g3dbLoadObject(bd,obj,m) g3dbLoadObjectEx(bd,obj,m,NULL,0)

#define g3dbDestroyBody(bd) free(bd);
//kostru lze znicit funkci free(body);

//chyby
#define G3DB_NOMEMORY -1
#define G3DB_OK 0
#define G3DB_NOBONES -50
#define G3DB_NEEDEXPAND -51



#ifdef __cplusplus
}
#endif






#endif





