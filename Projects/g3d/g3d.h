#ifndef _G3D_H_
#define _G3D_H_


#define G3D_FASTCALL _fastcall
//#define FASTCALL

#ifdef __cplusplus
extern "C" {
#endif


typedef struct _tvertexcolor
	{
  union
    {
    float pohlc_r;
    float range;    //for lights
    };
  union
    {
    float pohlc_g;
    float minradius; //for lights
    };
  union
    {
    float pohlc_b;
    float maxradius; //for lights
    };
	float emit_r,emit_g,emit_b;
	float ampl_r,ampl_g,ampl_b;
  float offs_r,offs_g,offs_b;
  float freq;
  union
    {
    float alpha;      //not used in lights
    long light_flags; //for lights
    };
  long flags;       //
	}TVERTEXCOLOR;
  
#define TVCOL_ALPHA 0x1
#define TVCOL_ANIMATED 0x2
#define TVCOL_ANIMDIFF 0x4
#define TVCOL_NODIFFUSE 0x8
#define TVCOL_LIGHT 0x8

typedef struct _tnormalvertex
	{
	HVECTOR vector;
	}TNORMALVERTEX;

typedef struct _tworldvertex
	{
	HVECTOR vector;
	}TWORLDVERTEX;

typedef struct _tscenevertex
	{
  long nvindex;
	long wvindex;
	}TSCENEVERTEX;

typedef struct _draworder
  {
  long dwVertexColor;
  union
    {
    unsigned long dwVertex;
    struct
      {
      unsigned short wFlags;
      unsigned short wZorder;
      };
    };
  union
    {
    float tu;
    long vcount;
    };
  union
    {
    float tv;
    long texture;
    };
  }TFACEDRAWORDER;

#define G3D_COMINDICATOR 0x7fffffff

#define G3D_TRIANGLE 1
#define G3D_STRIP 2
#define G3D_FAN 3
#define G3D_POLYGON 4
#define G3D_LPOINT 5
#define G3D_LINE 6     //Kresli caru
#define G3D_STREAMLIGHT 7 //Kresli streamlight
#define G3D_BILLBOARD 8 //Kresli billboard
#define G3D_OPTICEFF 9  //Kresli opticky effekt (lensflare);
#define G3D_BONE 10     //Nekresli nic. Je to kost
#define G3D_KNEE 11     //Nekresli nic. Je to koleno
#define G3D_MIRRORING 16 //Textura se bude pri opakovani zrcadlit.
#define G3D_OPTIALWAYS 32 //Kresli opticky effekt i kdyz neni videt
#define G3D_DOUBLESIDE 64 //Vypina culling behem kresleni

typedef struct _tlightdef
	{
	HVECTOR position;
	HVECTOR direction;
	float col_r,col_g,col_b;
	float ampl_r,ampl_g,ampl_b;
	float offs_r,offs_g,offs_b;
	float freq;
	float range,minradius,maxradius;
	long light_flags;
	}TLIGHTDEF,* HLIGHTDEF;

#define LGH_ACTIVE 1
#define LGH_POINTLIGHT 2
#define LGH_DIRECTLIGHT 4
#define LGH_SPOTLIGHT 8
#define LGH_SINEANIM 16
#define LGH_TURNONOFFANIM 32
#define LGH_AMBIENTLIGHT 64

typedef char (* TRAYTRACECALLBACK)(int mode, HVECTOR from, HVECTOR to);

#define LRT_LIGHT 0
#define LRT_HALFSHADOW 1
#define LRT_SHADOW 2
#define LRTM_FROMTO 0
#define LRTM_FROMDIR 1

typedef struct _tg3dobject
  {
  void **vtptr;
  }TG3DOBJECT,*HG3DOBJECT;

typedef struct _tscenedef
	{
  void **vtptr;
	TVERTEXCOLOR *cvtable;
	int cvsize;
	TNORMALVERTEX *ntable;
	int nsize;
	TWORLDVERTEX *wtable;
	int wsize;
	TSCENEVERTEX *stable;
	int ssize;
	TFACEDRAWORDER *fctable;
	int fcsize;
	}TSCENEDEF;

void g3dResetTables();
//likviduje aktualni scenu, uvolnuje pamet a resetuje vsechny ukazatele.
//POZOR! Jedna se o tvrdy reset sceny, na jakekoliv vnejsi reference neni bran ohled;

char G3D_FASTCALL g3dExpandTable(void **table, int *count, int polozka);
//Funkce rozsiri dynamickou tabulku o dalsich 256 polozek
//pri neuspechu vraci nenulovou hodnotu.

char g3dResizeLightTable(int lights);
//Nastavuje pocet svetel ve scene. Doporucuje se nevolat casto.
//Nici pripadna svetla, ktera by se do tabulky nevesla.

void g3dBeginScene(TSCENEDEF *load);
//Pripravi system pro kresleni objektu.
//Je-li load nenulovy, pak ukazuje na popisovac existujici sceny. Tato scena NENI pouzita jako template,
//ale je primo modifikovana pripadnymi funkcemi pro modifikaci sceny.
//Volani funkce bez predchoziho g3dEndScene ma zanasledek zniceni prechozi sceny.

/*int g3dLoadObject(TSCENEDEF *def, HMATRIX mm);*/
/********** Removed in new version***************/

#define g3dLoadObject(def,mm) g3dLoadObjectEx2(def,mm,NULL)
//Tato funkce vlozi do sceny drive definovany objekt.
//Jako objekt lze pouzit drive definovanou scenu.
//S objektem je provedena transformace podle matice mm
//Program neprovadi zadne optimalizace, takze v tabulce pozic
//a normal se mohou objevit dve polozky stejne.
int G3D_FASTCALL g3dLoadObjectEx(TSCENEDEF *def, HMATRIX mm, unsigned short *txit, int count);
//OBSOLETTE!!
int G3D_FASTCALL g3dLoadObjectEx2(TSCENEDEF *def, HMATRIX mm, HTXITABLE txi);
//Funkce funguje stejne jako g3dLoadObject s tim ze prelozi cisla textur podle
//tabulky.

void G3D_FASTCALL g3dBeginObject(HMATRIX wtrns);
//Pripravi system pro vlozeni objektu. "wtrns" urcuje lokalni transformaci objektu vuci svetu.
//pokud je "transform" nastaven na 1, pak se transformace uplatni.
//Vzdy je nutne predat platnou matici, i v pripade ze je transformace zakazana.

int G3D_FASTCALL g3dAddPosition(HVECTOR v);
//Vklada do sceny pozici. Vraci index teto pozice ve scene, ktery je mozne pozdeji pouzit ve funkci
//g3dAddVertex. Hodnota -1 oznamuje chybu
int G3D_FASTCALL g3dAddTPosition(HVECTOR v, HVECTOR vt);


int G3D_FASTCALL g3dAddNormal(HVECTOR v);
//Vklada do sceny normalu. Vraci index teto normaly ve scene, ktery je mozne pozdeji pouzit ve funkci
//g3dAddVertex. Hodnota -1 oznamuje chybu

int G3D_FASTCALL g3dAddColor(TVERTEXCOLOR *tdef);
//Vklada do sceny barvu. Vraci index teto barvy ve scene, ktery je mozne pozdeji pouzit ve funkci
//g3dAddVertex. Hodnota -1 oznamuje chybu

int G3D_FASTCALL g3dAddVertex(int posindex,int normalindex);
//Vklada do sceny vertex. Vertex se sklada ze tri casti. Pozice, Normala, Barva. Vraci index, ktery je mozne pouzit
//ve funkci g3dAddFaceVertex

int G3D_FASTCALL g3dAddFace(int vertices,int flags,long handle);
//Vklada do sceny primitivni objekt
//"vertices" - z kolika vertexu se sklada
//"flags" - vlajky a typ primitivniho objektu
//"handle" - rukojet k texture

int G3D_FASTCALL g3dAddFaceVertex(int vertex,int color,float tu, float tv);
//Specifikuje vertex primitivniho objektu
//"vertex" - index vertexu z funkce d3dAddVertex
//"tu" a "tv" - koordinaty textury

char G3D_FASTCALL g3dSetLight(int index, TLIGHTDEF *ldef);
//Nastavuje parametry svetla.
//Je-li index vetsi nez pocet alokovanych svetel, funkce neprovede zadnou cinnost.

void G3D_FASTCALL g3dEndScene(TSCENEDEF *tostore);
//Ukonci kresleni sceny. Je-li specifikovat pointer, pak definice sceny je ulozena do tabulky "tostore".
//Je-li "tostore"=NULL pak je scena zapomenuta a pri nejblizsi prilezitosti prepsana jinou scenou.
//Pokud je scena ulozena, pak jeji nove pouziti se provede volanim funkce g3dBeginScene() s parametrem ukazujicim prave
//na tuto strukturu.

void G3D_FASTCALL g3dDestroyScene(TSCENEDEF *todestroy);
//Nici ulozenou scenu. Prikaz se nesmi objevit mezi prikazy g3dBeginScene a g3dEndScene, pokud je nicena prave
//vykreslovana scena.

char g3dDuplScene(TSCENEDEF *scene, TSCENEDEF *newscene);

void G3D_FASTCALL g3dCalcFinalMatrix(HMATRIX view_t, HMATRIX proj_t);
//pocita transformacni matici cele sceny
//vysledna matice posleze muze byt vracena funkci g3dGetTransformMatrix

int G3D_FASTCALL g3dDrawScene(HMATRIX view_t, HMATRIX proj_t, char flags);
//view_t je transformacni matice sceny
//proj_t je projekcni matice sceny
//Knihovna neber ohled na nastaveni pohledoveho objemu a predpoklada kvadry o souradnicich
//(-1,-1,0)(1,1,0)(-1,-1,1)(1,1,1)
#define G3DD_SORT 1
#define G3DD_NOOPTICS 2

void G3D_FASTCALL g3dSetAmbientLight(float intensity);
// Funkce nastavuje bile ambientni svetlo. Svetlo odrazi vsechny objekty bez ohledu na smer
// a polohu.
char g3dSetLightTransformation(HMATRIX m,char mode);
// Nastavuje tranformaci svetel;
#define G3D_CLEARLT 0
#define G3D_LIGHTLT 1
#define G3D_SCENELT 2


char  g3dGetLight(int index, TLIGHTDEF *def);
// Funkce zjistuje nastaveni svetla. Vraci 1 pokud nelze udaje ziskat.

TLIGHTDEF *g3dGetLightPtr(int index);
// Funkce vraci ukazatel na svetlo. Ten je mozne pouzit k primemu nastavovani svetel bez nutnosti volat funkce
// GetLight a SetLight

void G3D_FASTCALL g3dSetViewport(float ZeroX,float ZeroY,float Width, float Height,char Clip);
//Nastavuje kreslici oblast. Je-li Clip=1 pak se zmeni i orezova oblast.
//Na zacatku je nutne zavolat
void g3dSetTraceRayProc(TRAYTRACECALLBACK proc);


int g3dDiskSaveObject(char *filename,TSCENEDEF *def,char flags);
int g3dDiskLoadAllObjects(char *filename,int object,TSCENEDEF **out);
int g3dDiskLoadObject(char *filename,int object,TSCENEDEF *out);

int g3dOptimizeNormals(float precision);
//Funkce optimalizuje pocet normal v databazi.
//slucuji se vsechny normaly, ktere se lisi o precision.
//vyhazuji se vsechny nepouzite normaly.
//Optimalizace ma slozitot n*log n. Je to pametove narocna operace
//proto by se mela provadet jen jednou, a to po dokonceni objektu.
//Vyhodou je pak rychlejsi vypocet sceny.
//Optimalizacni funkce se musi provadet v sekvenci Begin a End scene
//Napriklad:
// g3dBeginScene(Object);
// g3dOptimizeNormals(precision);
// g3dOptimizePositions(precision);
// g3dEndScene(Object);
int g3dOptimizePositions(float precision);
int g3dOptimizeVertices();
int g3dOptimizeScene(float posprec, float normprec);
int FindVertexFromPoint(float x,float y,int from);

void g3dGetTransformMatrix(HMATRIX m);
//Naplni m podobou transformacni matice predchoziho kresleni.
unsigned int g3dAnimTick(unsigned int speed);

typedef struct _tagG3DSCENEINFO
  {
  int colors_total;
  int colors_defined;
  int colors_undefined;
  int textures_total;
  int min_tex_index;
  int max_tex_index;
  int faces;
  }G3DSCENEINFO;

char g3dGetSceneInfo(TSCENEDEF *scn,G3DSCENEINFO *info);

#define rndmmi(min,max) (rand()*((max)-(min))/(RAND_MAX)+(min))
#define rndmmf(min,max) (((float)rand()/(float)RAND_MAX)*((max)-(min))+(min))

typedef struct _tagparticleitem
  {
  int lifetime;
  int lifetotal;
  HVECTOR position;
  HVECTOR direction;
  float size,rsiz;
  TVERTEXCOLOR col,rcol;
  }TPARTICLEITEM;


typedef struct _tagparticleprop
  {
  TVERTEXCOLOR val1,val2;
  HVECTOR emitor;
  float zenit;
  float azimut;
  float sizebeg,sizeend;
  float speed;
  int lifetime;
  }TPARTICLEPROP;

typedef struct _tagparticles
  {
  void **vtptr;
  TPARTICLEPROP stops[2];
  struct _tagparticles *nextsystem;
  int countmin,countmax;
  int texbeg,texend;
  TPARTICLEITEM *parray;
  int pcount;
  int create;
  int primtype;
  unsigned long flags;
  }TPARTICLES;

char g3dInitParticles(TPARTICLES *system);
int g3dMoveParticles(TPARTICLES *system);
char g3dLoadParticles(TPARTICLES *system, HMATRIX mm);

float *g3dAskPosition(int position);
float *g3dGetVertexPart(int vertex, int part);
#define G3DVP_POSITION 1
#define G3DVP_NORMAL 2
#define G3DVP_TPOSITION 3
void g3dCalcNormal(int p1,int p2, int p3, HVECTOR norm);

#define G3DPF_CHGSIZE 0x1
#define G3DPF_CHGDIFFCOLOR 0x2
#define G3DPF_CHGEMITCOLOR 0x4
#define G3DPF_CHGALPHA 0x8
#define G3DPF_CHGCOLOTHER 0x10
#define G3DPF_RNDSIZE 0x100
#define G3DPF_RNDDIFFCOLOR 0x200
#define G3DPF_RNDEMITCOLOR 0x400
#define G3DPF_RNDALPHA 0x800
#define G3DPF_RNDCOLOTHER 0x1000
#define G3DPF_RNDPOSITION 0x2000

#define g3dPtclSetValue(system,field,min,max) ((system)->stops[0].field=(min),(system)->stops[1].field=(max))
#define g3dPtclSetProp(system,field,value) ((system)->field=(value))
#define g3dPtclStop(system) ((system)->create=0)
#define g3dPtclStart(system,crt) ((system)->create=crt)


#define G3DVT_DESTROY 1
#define G3DVT_LOADEX 2
#define G3DVT_MOVE 3
#define G3DVT_DUPLICATE 4
#define G3DVT_IOSAVE 5
#define G3DVT_IOLOAD 6

typedef void (*G3DVF_DESTROY)(void *);
typedef char (*G3DVF_LOADEX)(void *,HMATRIX,HTXITABLE);
typedef int (*G3DVF_MOVE)(void *,int);
typedef char (*G3DVF_DUPL)(void *,void *);
typedef char (*G3DVF_IOSAVE)(void *,FILE *);
typedef char (*G3DVF_IOLOAD)(void *,FILE *);

#define g3dDestroy(object) ((G3DVF_DESTROY)(object)->vtptr[G3DVT_DESTROY])(object)
#define g3dLoadEx(object,mm,txi) ((G3DVF_LOADEX)(object)->vtptr[G3DVT_LOADEX])(object,mm,txi)
#define g3dMove(object,howmany) ((G3DVF_MOVE)(object)->vtptr[G3DVT_MOVE])(object,howmany)
#define g3dioSave(object,f) ((G3DVF_IOSAVE)(object)->vtptr[G3DVT_IOSAVE])(object,f)
#define g3dioLoad(object,f) ((G3DVF_IOLOAD)(object)->vtptr[G3DVT_IOLOAD])(object,f)
#define g3dDuplicate(object,newobj) ((G3DVF_DUPL)(object)->vtptr[G3DVT_DUPLICATE])(object,newobj)
#define g3dLoad(object,matrix) g3dLoadEx(object,matrix,NULL)

TSCENEDEF *g3dCompactScene(TSCENEDEF *scn);
TSCENEDEF *g3dSceneInicialize(TSCENEDEF *scn);
#define g3dCreateScene(scn) g3dSceneInicialize(scn)

typedef struct _g3dstatistics
  {
  long vertices;
  long normals;
  long positions;
  long points;
  long lines;
  long triangles;
  }G3D_STATISTICS;

void g3dClearStatistics();
void g3dGetStatistics(G3D_STATISTICS *stat);

LPHMATRIX g3dGetSystemMatrix(char matrix);
#define G3DSM_OBJECTMATRIX 0  //matrix in g3dBeginObject
#define G3DSM_MAINMATRIX 1	  //main transformation matrix (all matrices together)
#define G3DSM_LIGHTMATRIX 2	  //light transformation
#define G3DSM_SCENEMATRIX 3	  //scene transformation
#define G3DSM_CAMERAMATRIX 4 //projection * viewport
#define G3DSM_VIEWMATRIX 5	  //scene * view transformation
#define G3DSM_VIEWPORTMATRIX 6 //viewport matrix



#ifdef __cplusplus
}
#endif

#endif
