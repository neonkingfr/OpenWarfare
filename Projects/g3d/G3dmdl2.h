#ifndef _G3DMDL2_H_
#define _G3DMDL2_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#ifdef frm1
#undef frm1
#undef frm2
#undef frm3
#endif


//----------- SCENE LISTS ---------------

typedef struct _g3dmscnlists
  {
  void **vtptr;  //VirtualMethodPointer
  int count;    //Count of scenes
  TSCENEDEF *scenes[1]; //scenelist
  }TG3DSCENELIST,*HG3DSCENELIST;

typedef struct _g3dmobjframes
  {
  int frmnum;   //frame number
  int scene;    //index into scene list
  int points;   //point positions (into scene list) - if not defined, used "scene"
  HMATRIX mm;   //local transformation matrix
  int flags;    //some flags
  }TG3DMFRAME,*HG3DMFRAME;

#define G3DMF_NOMATRIX 1   //matice neni pouzita
#define G3DMF_HIDEOBJECT 2 //objekt je skryty
#define G3DMF_LOCALSCENELIST 4 //scenelist je sprazen s objektem
#define G3DMF_LOCALSCENELISTP 4 //scenelist je sprazen s objektem

typedef struct _g3dframelist
  {
  void **vtptr;   //Virtual Method Pointer
  int seqnumber; //number of sequnece
  int preallocated; //prealoc
  int count;    //count of frames in sequence
  TG3DMFRAME frames[1];
  }TG3DFRAMELIST,* HG3DFRAMELIST;

typedef struct _g3dobjectsequencer
  {
  int parent; //parent object in hiearchy
  HMATRIX tmp; //temp matrix space for calculations
  int nomatrix; //if 1, parent object has no matrixes
  int count; //count of sequences
  char *name; //object name
  HG3DSCENELIST scnlist; //lokalni scene-list
  HG3DFRAMELIST seqs[1]; //array of sequences
  }TG3DMSEQUENCER,* HG3DMSEQUENCER;

typedef struct _g3dmodeldef
  {
  int count;  //count of objects;
  int refcount; //count of references;
  char *name; //name of model;
  HG3DSCENELIST scnlist; //attached scene list;
  HG3DMSEQUENCER objects[1]; //object array
  }TMODELDEF,* HMODELDEF;


typedef struct _g3dmodel
  {
  void **vtptr;       //Virtual method pointer
  int uid;            //Unique identifier
  HMODELDEF modeldef; //model def reference
  HTXITABLE txitable; //txi table reference
  int curframe;       //current frame number
  int trueframe;      //cislo aktualniho frame v prepoctu frame1000
  int lastframe;      //last frame in sequence
  int cursequence;    //current sequence number
  int curobject;      //current object number
  int frame1000;      //real frame number for virtual frame 1000
  int direction;      //smer animace
  int lastticker;     //cislo posledniho ticku.
  int *ticker;        //adresa citace meniciho se podle casu
  int *indexes;       //array of indexes for fast sequence finding
  int flags;          //some flags
  }TG3DMODEL,* HG3DMODEL;



#define G3DMT_AUTOANIMATE 0x1 //object is animate using ticker
#define G3DMT_NOTIFYEND 0x2   //pri animaci se hlasi ze je posledni frame
#define G3DMT_DONTLOOP 0x4    //animace se zastavi na poslednim frame


#define G3DM_SEQNOTFOUND -20  //Sekvence nebyla nalezena
#define G3DM_NOCURRENTFRAME -31 //Zadny frame neni aktivni
#define G3DM_FRAMENOTFOUND -32  //fyzicke cislo frame nebylo nalezeno
#define G3DM_NOTFOUND -32
#define G3DM_LASTFRAME 22        //Informuje, ze bylo zobrazeno posledni frame.
#define G3DM_ENUMSTOPPED -33

//--------- Prace se scenelisty ---------

HG3DSCENELIST g3dmAllocSceneList(int scenes);
//Vyrobi seznam scen pro urcity pocet
int g3dmAddScene(HG3DSCENELIST *scnl,TSCENEDEF *def);
//Vytvari seznam scen postupne pridavanim nakonec (pomale)
char g3dmSetScene(HG3DSCENELIST scnl, int index, TSCENEDEF *def);
//Nastavi urcity index v liste novou scenu
#define g3dmQueryScene(scnl,index) ((scnl)->scenes[index])
//Ziska ukazatel na scenu v liste
char g3dmDestroySceneList(HG3DSCENELIST scnl);
//Znici seznam scen
char g3dmLoadSceneList(HG3DSCENELIST scnl, FILE *f);
//Ulozi scenelist do souboru (neuklada se count!!!)
char g3dmSaveSceneList(HG3DSCENELIST scnl, FILE *f);
//Precte scenelist ze souboru (nutno inicializovat)

//Na strukturu lze pouzit funkce g3dDestroy, g3dioLoad a g3dioSave
//Funkce g3dLoadEx zobrazi vsechny sceny naraz

//------- Prace s ramcema --------------

HG3DFRAMELIST g3dmAllocFrameList(int frames);
//Alokuje dalsi framelist
int g3dmAddFrameToList(HG3DFRAMELIST frml, HG3DMFRAME frame);
//Vlozi dalsi frame, vraci jeho index;
int g3dmPreallocFrames(HG3DFRAMELIST *frm, int count);
//Predalokuje dalsich count frames;
int g3dmPrepareFrames(HG3DFRAMELIST *frm);
//Pripravi framelist k pouziti
int g3dmSaveFrameList(HG3DFRAMELIST frm,FILE *f);
//Ulozi framelist na disk (uklada cisty seznam bez count)
int g3dmLoadFrameList(HG3DFRAMELIST frm,FILE *f);
//Cte list z disku (nutno inicializovat!!!);
#define g3dmDestroyFrameList(frm) free(frm);
//Lze pouzit g3dDestroy, g3dioLoad, g3dioSave
char g3dmFindFrame(HG3DFRAMELIST frml,int frm, HG3DMFRAME *frm1,HG3DMFRAME *frm2);
//Hleda dva frame ve kterem se zadane frame nachazi


//------- Prace s modelem --------------
char g3dmCreateEmptyModel(HG3DMODEL mdl);
//Funkce vytvori prazdny model a jeho handle
int g3dmAddObject(HG3DMODEL mdl,int parent);
//Funkce prida objekt do modelu a nastavi ho jako aktualni
//vraci cislo noveho objektu
int g3dmSetObjectName(HG3DMODEL mdl, char *name);
char *g3dmGetObjectName(HG3DMODEL mdl);
//Nastavuje jmeno
int g3dmSetCurObject(HG3DMODEL mdl,int object);
//Funkce nastavi objekt jako aktualni pro editaci
//vraci cislo aktualniho nadrazeneho objektu
int g3dmAddSequence(HG3DMODEL mdl,int seqnum, HG3DFRAMELIST frm);
//Pridava sekvenci do objektu. Vraci cislo seqnum
int g3dmSetCurSequence(HG3DMODEL mdl, int seqnum);
//Nastavuje sekvenci jako vychozi. Vraci cislo predchozi sekvence
int g3dmSetCurSequenceEx(HG3DMODEL mdl, int seqnum, int dir, int flags);
//Nastavuje sekvenci jako vychozi. Vraci cislo predchozi sekvence
int g3dmSetCurFrame(HG3DMODEL mdl, int framenum);
//Nastavuje aktivni cislo frame, pouzije se prepocet framu
//Vraci cislo predchoziho frame
int g3dmAttachSceneList(HG3DMODEL mdl, HG3DSCENELIST scnl, int object);
//Pripoji seznam scen. object==-1 - globalni
int g3dmAttachTxi(HG3DMODEL mdl, HTXITABLE txi);
//Pripoji tabulku txi
#define g3dmSetProperty(mdl,prop,value) ((mdl)->prop=(value))
#define g3dmGetProperty(mdl,prop) ((mdl)->prop)

int g3dmMove(HG3DMODEL mdl, int howmany);
//pricte k citaci framu hodnotu howmany (stejne jako g3dMove())
int g3dmLoadModelEx(HG3DMODEL mdl, HMATRIX mm, HTXITABLE txi);
//Nacte model do sceny. (stejne jako g3dLoad)
int g3dmDuplicModel(HG3DMODEL mdl, HG3DMODEL newmdl);
//Duplikuje model
int g3dmDestroyMdl2(HG3DMODEL mdl);
//Nici model
typedef char (*G3DMSEQCALLBACK)(HG3DMODEL mdl,HG3DFRAMELIST frml,void *user);

int g3dmEnumSequences(HG3DMODEL mdl,G3DMSEQCALLBACK callback, void *user);
//enumeruje sekvence k aktualnimu objektu
HG3DFRAMELIST g3dmGetSequence(HG3DMODEL mdl);
//vraci ukazatel na FRAMELIST aktualni sekvence a objektu
int g3dmDeleteSequence(HG3DMODEL mdl);
//nici aktualni sekvenci v dannem modelu
HG3DSCENELIST g3dmGetSceneList(HG3DMODEL mdl,int object);
//vraci pripojeni scenelistu. -1 je globalni
int g3dmCopyObject(HG3DMODEL mdl, HG3DMODEL newmdl, int newparent);
//Kopiruje objekt z jednoho modelu do druheho.
//Dobre, pri sestavovani nove hierarchie
int g3dmFindObject(HG3DMODEL mdl, int parent, int from);

int g3dmFindObjectByName(HG3DMODEL mdl, char *name, int from);

int g3dmGetObjectParent(HG3DMODEL mdl, int object);

int g3dmGetObjectCount(HG3DMODEL mdl);

int g3dmSetModelName(HG3DMODEL mdl, char *name);

char *g3dmGetModelName(HG3DMODEL mdl);

char g3dmSaveModelDisk(HG3DMODEL mdl, FILE *f);
char g3dmLoadModelDisk(HG3DMODEL mdl, FILE *f);
char g3dmSaveModelDiskEx(HG3DMODEL mdl, char *filename);
char g3dmLoadModelDiskEx(HG3DMODEL mdl, char *filename);



int g3dmAnimTick2(int howmany);

#ifdef __cplusplus
}
#endif

#endif
