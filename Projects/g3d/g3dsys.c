#include <windows.h>
#include <zmouse.h>
#include <stdio.h>
#include "g3dlib.h"

static TMSSTATE msstate;
static int lastmsx,lastmsy;

static void GetClientWindow(HWND hWnd,RECT *rect)
  {
  POINT pt={0,0};
  GetClientRect(hWnd,rect);
  ClientToScreen(hWnd,&pt);
  rect->left+=pt.x;
  rect->right+=pt.x;
  rect->top+=pt.y;
  rect->bottom+=pt.y;
  }

static int g3dMousePlace(HWND hWnd, LPARAM lParam)
  {
  RECT rc;
  int x,y;
  int maxx,maxy;

  maxx=g3dQuerySettingInt(G3DQ_SCRSIZEX);
  maxy=g3dQuerySettingInt(G3DQ_SCRSIZEY);
  GetClientRect(hWnd,&rc);
  x=LOWORD(lParam);
  y=HIWORD(lParam);
  if (lastmsx==x && lastmsy==y) return 0;
  lastmsx=x;
  lastmsy=y;
  x=maxx*x/rc.right;
  y=maxy*y/rc.bottom;
  msstate.msx=x;
  msstate.msy=y;
  msstate.msx-=msstate.msdefx;
  msstate.msy-=msstate.msdefy;
  return G3DMS_MOVE;
  }

int g3dNotifyMouse( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
  {
  int res=0;
  short whl;
  switch (uMsg)
    {
    case WM_MOUSEMOVE:res=g3dMousePlace(hWnd,lParam);break;
    case WM_LBUTTONDOWN:msstate.tl1=1;res=G3DMS_TL1D;break;
    case WM_LBUTTONUP:msstate.tl1=0;msstate.dtl1=0;res=G3DMS_TL1U;break;
    case WM_RBUTTONDOWN:msstate.tl2=1;res=G3DMS_TL2D;break;
    case WM_RBUTTONUP:msstate.tl2=0;msstate.dtl2=0;res=G3DMS_TL2U;break;
    case WM_LBUTTONDBLCLK:msstate.dtl1=1;res=G3DMS_DTL1;break;
    case WM_RBUTTONDBLCLK:msstate.dtl2=1;res=G3DMS_DTL2;break;
	case WM_MBUTTONDOWN:msstate.tl3=1;res=G3DMS_TL3D;break;
	case WM_MBUTTONUP:msstate.dtl3=msstate.tl3=0;res=G3DMS_TL3U;break;
	case WM_MBUTTONDBLCLK:msstate.dtl3=0;res=G3DMS_DTL3;break;
	case WM_MOUSEWHEEL: g3dMousePlace(hWnd,lParam);
						whl=HIWORD(wParam);
						msstate.wheelcounts+=whl;
						res=whl<0?G3DMS_WHEELB:G3DMS_WHEELF;
						break;						
    default: res=0;
    }
//  if (res) ProcessMouse();
  msstate.event|=res;
  return res;
  }



static WNDPROC CurProc=DefWindowProc;
static LRESULT CALLBACK SysWndProc(HWND hWnd, UINT msg, WPARAM wParam,LPARAM lParam)
  {
  return CurProc(hWnd,msg,wParam,lParam);
  }

HWND CreateG3DWindow(HINSTANCE hInst,WNDPROC WinProc,const char *title, const char *icon)
  {
  HWND hWnd;
  WNDCLASS   wc;
  int i;
  if (hInst==NULL)
	{
    hInst=GetModuleHandle(NULL);
	}
  if (WinProc==NULL) CurProc=DefWindowProc;else CurProc=WinProc;
  wc.style = CS_BYTEALIGNCLIENT | CS_DBLCLKS;
  wc.lpfnWndProc   = (WNDPROC)SysWndProc;
  wc.cbClsExtra    = 0;
  wc.cbWndExtra    = 0;
  wc.hInstance     = hInst;
  wc.hIcon         = LoadIcon(hInst,icon);
  wc.hCursor       = NULL;
  wc.hbrBackground = (HBRUSH)COLOR_MENUTEXT;
  wc.lpszMenuName  = title;
  wc.lpszClassName = title;

  if ( !RegisterClass( &wc ) ) return NULL;

  i=g3dQuerySettingInt(G3DQ_DISPLAYMODE);
  switch (i)
	{
	case G3DQD_FULLSCREEN:
	hWnd = CreateWindowEx(
	      WS_EX_TOPMOST,
                        title,
                        title,
                         WS_POPUP|WS_SYSMENU|WS_MINIMIZEBOX|WS_MINIMIZE|WS_VISIBLE,
                        CW_USEDEFAULT, 0,
                        CW_USEDEFAULT, 0,
                        NULL,
                        NULL,
                        hInst,
                        NULL
                      );break;
	case G3DQD_WINDOWED:
	hWnd = CreateWindowEx(0,
                        title,
                        title,
                        WS_OVERLAPPEDWINDOW|WS_VISIBLE,
                        CW_USEDEFAULT, 0,
                        CW_USEDEFAULT, 0,
                        NULL,
                        NULL,
                        hInst,
                        NULL
                      );break;
	default: hWnd=NULL;
	}
  return hWnd;
  }

static IDLEPROC idleproc=NULL;
static int AppLevel=0;

int g3dGetAppLevel()
  {
  return AppLevel;
  }


int g3dAppRun(IDLEPROC proc,WNDPROC wndproc)
  {
  MSG msg;
  IDLEPROC old=idleproc;
  WNDPROC oldproc;
  
  oldproc=CurProc;
  if (wndproc!=NULL) CurProc=wndproc;
  if (proc==G3DA_NOIDLE) idleproc=NULL;
  else if (proc!=NULL) idleproc=proc;
  AppLevel++;
  do
    {	
    if (idleproc==NULL)
      {
      if (!GetMessage( &msg, NULL, 0, 0)) break;
      TranslateMessage(&msg);
      DispatchMessage(&msg);
      }
    else
      {
      if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
        if (WM_QUIT == msg.message) break;
        else
          {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
          }
      else
        idleproc();
      }
    }
  while (1);
  idleproc=old;
  AppLevel--;
  CurProc=oldproc;
  return msg.wParam;
  }

void g3dExitAppLevel(int retvalue)
  {
  PostQuitMessage(retvalue);
  }

void g3dMouseDisplay()
  {
  if (!msstate.counter)
	{
	g3d2dShowMouse(msstate.msx-msstate.hotx,msstate.msy-msstate.hoty,
	  g3dTxiGetTexture(msstate.txitable,msstate.cursor),msstate.flags & G3DBG_SAVE?&msstate.clip:NULL);
	}  
  }

void g3dMouseHide()
  {
  if (!msstate.counter)
	g3d2dShowMouse(msstate.msx-msstate.hotx,msstate.msy-msstate.hoty,0,msstate.flags & G3DBG_SAVE?&msstate.clip:NULL);
  msstate.counter++;
  }

void g3dMouseShow()
  {
  if (msstate.counter) msstate.counter--;
  }

void g3dMouseInit(HTXITABLE cursortable,int cursor)
  {
  memset(&msstate,0,sizeof(msstate));
  msstate.txitable=cursortable;
  msstate.cursor=cursor;
  msstate.flags|=G3DBG_SAVE;
  }

void g3dGetMouseStatus(TMSSTATE *mss)
  {
  *mss=msstate;
  msstate.event=0;
  msstate.wheelcounts=0;
  }

void g3dSetMouseStatus(TMSSTATE *mss)
  {
  msstate=*mss;
  }

static char msmode=G3DMM_NORMAL;
static POINT mspos;

void g3dSetMouseMode(int mode)
  {
  if (mode==G3DMM_NORMAL)
    {
    msstate.msdefx=0;
    msstate.msdefy=0;
	if (msmode!=mode) SetCursorPos(mspos.x,mspos.y);
    }
  else
    {
    RECT r;
	POINT p;
	HWND hWnd;
	if (msmode!=mode) GetCursorPos(&mspos);
    msstate.msdefx=g3dQuerySettingInt(G3DQ_SCRSIZEX)>>1;
    msstate.msdefy=g3dQuerySettingInt(G3DQ_SCRSIZEY)>>1;
	hWnd=GetActiveWindow();
    GetClientWindow(hWnd,&r);
    SetCursorPos((r.right+r.left)>>1,(r.top+r.bottom)>>1);	
	GetCursorPos(&p);
	ScreenToClient(hWnd,&p);
	lastmsx=p.x;
	lastmsy=p.y;
    }
  msmode=mode;
  }

void g3dMouseCursor(int handle,char background)
  {
  if (handle!=G3DBG_NOCHANGE) msstate.cursor=handle;
  if (background!=G3DBG_NOCHANGE)
    {
    msstate.flags&=~0x1;
    msstate.flags|=(background==G3DBG_SAVE);
    }
  }
