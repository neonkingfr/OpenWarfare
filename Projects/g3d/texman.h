#ifndef _TEXMAN_H_
#define _TEXMAN_H_


#ifdef __cplusplus
extern "C" 
  {
#endif

typedef struct _txitexture
  {
  long handle;
  short frames;
  short flags;
  }TXITEXTURE;

typedef struct _txitable
  {
  int count;
  unsigned int frame;
  TXITEXTURE texturearr[1];
  }TTXITABLE,* HTXITABLE;


#include "hash.h"

#define TXIEXTENSION ".txi"


typedef long (*RegisterTextureProc)(char *texfilename);
#define TEXINVALIDHANDLE 0

char texInitManager(int hash);
long texRegisterTexName(char *name);
char *texRegisterTexHandle(long handle);
long texRegisterTexure(char *name, long handle);

long texLoadTexture(char *filename);//load but not register

long _TEX(char *name);

long texFindTexture(char *name);

HTXITABLE texLoadTXITable(char *mdlname, char *extension);

char texUnregisterTexture(char *name);
char texDoneManager();
char texForceReloadAllTextures();
  

void texSetTexturePath(char *path);

//------------ Texture tables ----------------


#define G3DTXI_NOANIM 0
#define G3DTXI_ANIMFORWARD 1
#define G3DTXI_ANIMBACKWARD 2
#define G3DTXI_ANIMPINGPONG 3

HTXITABLE g3dTxiAlloc(int tcount);
#define g3dTxiFree(htxi) free(htxi)
#define g3dTxiSetFrame(htxi,f) ((htxi)->frame=f)
  

__inline long g3dTxiGetTexture(HTXITABLE htxi, int index)
  {
  TXITEXTURE *tex;
  if (index>=htxi->count) return 0;
  tex=htxi->texturearr+index;
  switch (tex->flags)
	{
	case 0: return tex->handle;
	case 1: index+=htxi->frame%tex->frames;break;
	case 2: index+=tex->frames-1-htxi->frame%tex->frames;break;
	case 3:
	  {
	  int i=htxi->frame%(tex->frames*2-2);
	  if (i>=tex->frames) i=2*tex->frames-i-1;
	  break;
	  }
	}
  if (index>=htxi->count) return 0;
  return htxi->texturearr[index].handle;	  	  	
  }
  
__inline int g3dTxiSetTexture(HTXITABLE htxi, int index, long handle)
  {
  if (index>=htxi->count) return -1;
  htxi->texturearr[index].handle=handle;
  htxi->texturearr[index].flags=htxi->texturearr[index].frames=0;
  return 0;
  }
__inline int g3dTxiSetTextureEx(HTXITABLE htxi, int index, long handle, short frames, short flags)
  {
  if (index>=htxi->count) return -1;
  htxi->texturearr[index].handle=handle;
  htxi->texturearr[index].flags=flags;
  htxi->texturearr[index].frames=frames;
  return 0;
  }

#ifdef __cplusplus
  }
#endif

#endif