#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "g3dlib.h"

#define TEXNAMESIZE 63
static char *texPath=NULL;

typedef struct _tagTextureItem
  {
  char texname[TEXNAMESIZE];
  char zero;
  long handle;
  }TEXTUREITEM;


long texLoadTexture(char *filename);

static THASHTABLE textable;

char texInitManager(int hash)
  {

  return Hash_InitEx(&textable,hash,TEXTUREITEM,texname,HASHF_UPDATE);
  }

long texRegisterTexName(char *name)
  {
  TEXTUREITEM tex;  

  memset(&tex,0,sizeof(tex));
  strncpy(tex.texname,name,sizeof(tex.texname));
  tex.zero=0;
  tex.handle=texLoadTexture(name);
  if (Hash_Add(&textable,&tex)==NULL) return 0;
  return tex.handle;
  }


long texRegisterTexure(char *name, long handle)
  {
  TEXTUREITEM tex;

  memset(&tex,0,sizeof(tex));
  strncpy(tex.texname,name,sizeof(tex.texname));
  tex.zero=0;
  tex.handle=handle;
  if (Hash_Add(&textable,&tex)==NULL) return TEXINVALIDHANDLE;
  return tex.handle;
  }

long texFindTexture(char *name)
  {  
  TEXTUREITEM *tex;
  char nm[TEXNAMESIZE+1];
  memset(nm,0,sizeof(nm));
  strncpy(nm,name,sizeof(nm));
  tex=Hash_FindKey(&textable,nm);
  if (tex==NULL) return TEXINVALIDHANDLE;
  return tex->handle;
  }

static HTXITABLE LoadTXITableRecursive(FILE *f,int maxtex)
  {
  int index;
  static int i;
  static TEXTUREITEM tex;
  static char *c;
  static HTXITABLE res;
  long handle;

  i=fscanf(f,"%d",&index);
  if (i!=1) return NULL;
  while ((i=getc(f))==32);
  c=tex.texname;
  while (c!=&tex.zero && i!=EOF && i!='\n')
	{
	*c++=i;	i=getc(f);
	}
  *c=0;
  handle=texFindTexture(tex.texname);
  if (handle==TEXINVALIDHANDLE) handle=texRegisterTexName(tex.texname);  
  maxtex=max(maxtex,index);
  res=LoadTXITableRecursive(f,maxtex);
  if (res==NULL)
	{
	res=g3dTxiAlloc(maxtex+1);
	if (res==NULL) return NULL;
	}
  g3dTxiSetTexture(res,index,handle);
  return res;
  }

HTXITABLE texLoadTXITable(char *mdlname, char *extension)
  {
  FILE *f;
  HTXITABLE res;
  if (extension!=NULL)
	{
	char *c=(char *)alloca(strlen(mdlname)+strlen(extension)+1);
	char *d;
	char *e;

	strcpy(c,mdlname);
	d=strrchr(c,'.');
	e=strrchr(c,'\\');
	if (e!=NULL && d<e) d=strchr(c,0);
	else if (d==NULL) d=strchr(c,0);
	strcpy(d,extension);
	mdlname=c;
	}
  f=fopen(mdlname,"r");
  if (f==NULL) return NULL;
  res=LoadTXITableRecursive(f,0);
  fclose(f);
  return res;
  }

char texUnregisterTexture(char *name)
  {
  char nm[TEXNAMESIZE+1];

  memset(nm,0,sizeof(nm));
  strcpy(nm,name);  
  Hash_Delete(&textable,nm);
  return 0;
  }

static void UnregisterAll()
  {
  void *i;
  for (i=NULL;i=Hash_EnumData(&textable,i);)
	{
	TEXTUREITEM *it=(TEXTUREITEM *)i;
	g3dtUnregisterTexture(it->handle);
	}
  }

char texDoneManager()
  {
  UnregisterAll();
  Hash_Done(&textable);
  free(texPath);
  texPath=NULL;
  return 0;
  }

long _TEX(char *name)
  {
  long handle;
  handle=texFindTexture(name);
  if (handle==TEXINVALIDHANDLE)	
	handle=texRegisterTexName(name);	
  return handle;
  }

long texLoadTexture(char *filename)
  {
  TUNIPICTURE *pic;
  long handle;
  if (texPath)
	{
	char *c;

	c=alloca(strlen(texPath)+strlen(filename)+1);
	strcpy(c,texPath);
	strcat(c,filename);
	filename=c;
	}
  pic=cmLoadUni(filename);
  if (pic==NULL) return TEXINVALIDHANDLE;
  handle=g3dtRegisterTexture(pic);
  free(pic);
  return handle;
  }

void texSetTexturePath(char *path)
  {
  int s;  
  free(texPath); texPath=NULL;
  if (path==NULL) return;
  if (!path[0]) return;
  s=strlen(path);
  texPath=malloc(s+2);
  memcpy(texPath,path,s);
  if (texPath[s-1]!='\\') texPath[s++]='\\';
  texPath[s]=0;
  }

HTXITABLE g3dTxiAlloc(int tcount)
  {
  HTXITABLE txi;

  txi=malloc(tcount*sizeof(TXITEXTURE)+sizeof(TTXITABLE));
  if (txi!=NULL) 
	{
	txi->count=tcount;
	memset(txi->texturearr,0,sizeof(TXITEXTURE)*tcount);
	}
  return txi;
  }

char texForceReloadAllTextures()
  {
  UnregisterAll();
  Hash_Flush(&textable);
  return 0;
  }
