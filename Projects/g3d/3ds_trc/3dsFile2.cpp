// 3dsFile2.cpp: implementation of the C3dsChunk class.
//
//////////////////////////////////////////////////////////////////////

#include <string.h>
#include <malloc.h>
#include <stdio.h>
#include "3dsFile2.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

C3dsChunk::C3dsChunk()
  {
  next=chunklist=NULL;
  master=this;
  }

C3dsChunk::~C3dsChunk()
  {
  delete next;
  delete chunklist;
  }

bool C3dsChunk::ReadChunkHeader(istream& str)
  {
  streampos pos=str.tellg();
  chunkid=NULL_CHUNK;
  str.read((char *)&chunkid,2);
  str.read((char *)&chnkend,4);
  chnkend+=pos;
  chnkstart=str.tellg();
  chnkbeg=chnkstart;
  return (chnkstart-pos)==6;
  }


C3dsChunk *C3dsChunk::CreateSubChunk(C3dsChunk &chk)
  {
  C3dsChunk *p;
  p=new C3dsData();
  return p;
  }

bool C3dsChunk::ReadSubChunks(istream &str)
  {
  if (next) delete next;
  str.seekg(chnkbeg);
  while (str.tellg()<chnkend)
	{
	C3dsChunk *chk=new C3dsChunk();
	if (chk->ReadChunkHeader(str)==false) return false;
	if (chk->chnkend>chnkend || chk->chnkend<chk->chnkbeg)
	  {
	  delete chk;
	  delete chunklist;
    chunklist=0;
	  return false;
	  }
	chk->next=chunklist;
	chunklist=chk;
	str.seekg(chk->chnkend);
	}  
  return ParseChunkList(str);
  }

void C3dsChunk::CopyFrom(C3dsChunk & from)
  {
  chunkid=from.chunkid;
  chnkstart=from.chnkstart;
  chnkbeg=from.chnkbeg;
  chnkend=from.chnkend;
  }

bool C3dsChunk::ParseChunkList(istream &str)
  {
  C3dsChunk *p=chunklist,*q,*z;
  chunklist=NULL;
  while (p!=NULL)
	{
	z=master->CreateSubChunk(*p);
	z->master=master;
	z->CopyFrom(*p);
	str.seekg(z->chnkstart);
	if (z->LoadChunk(str)==true)
	  {
	  z->next=chunklist;
	  chunklist=z;
	  }
	else
	  delete z;
	q=p;
	p=q->next;
	q->next=NULL;
	delete q;
	}
  return true;
  }

bool C3dsChunk::LoadChunk(istream & str)
  {
  chnkbeg=str.tellg();
  ReadSubChunks(str);
  return true;
  }



bool C3dsNamedObject::LoadChunk(istream & str)
  {
  char szBuff[256];
  int i,j;
  
  i=0;
  do
	{
	if (i+1==sizeof(szBuff)) j=0;else j=str.get();
	szBuff[i++]=j;
	}
  while (j);
  name=_strdup(szBuff);
  return C3dsChunk::LoadChunk(str);
  }

bool C3dsRGBTriplet::LoadChunk(istream & str)
  {
  str.read((char *)&rgb,sizeof(rgb));
  return C3dsChunk::LoadChunk(str);
  }

void C3dsRGBTriplet::Inspect(ostream& str)
  {
  str<<"R:"<<(int)rgb.r<<" G:"<<(int)rgb.g<<" B:"<<(int)rgb.b;
  }

bool C3dsData::LoadChunk(istream &str)
  {
  if (ReadSubChunks(str)==false)
	{
	str.seekg(chnkstart);
	size=chnkend-chnkstart;
	data=malloc(size);
	if (data==NULL) 
	  return false;
	str.read((char *)data,size);
	}
  return true;
  }

void C3dsData::Inspect(ostream& str)
  {
  if (data!=NULL)
	{
	char text[10];	
	char hex[10];
	int idx=0;
	unsigned char *p=(unsigned char *)data;
	int adr=0;
	while (adr<size)
	  {
	  sprintf(hex,"%08X  ",adr);
	  str<<hex;
	  for (idx=0;idx<8;idx++)	if (adr<size)
		{
  	    sprintf(hex,"%02X ",*p);
		text[idx]=*p++;
	    if (text[idx]<32) text[idx]='.';
		str<<hex;
	    adr++;
		}
	  else
		{
		str<<".. ";
		text[idx]=' ';
		}
	  text[idx]=0;
	  str<<"| "<<text<<"\r\n";
	  idx=0;
	  }
	}
  }

C3dsData::~C3dsData()
  {
  free(data);
  }

bool C3dsPointArr::LoadChunk(istream &str)
  {
  str.read((char *)&count,sizeof(count));
  arr=new S3DSVECTOR[count];
  str.read((char *)arr,count*sizeof(S3DSVECTOR));
  return C3dsChunk::LoadChunk(str);
  }

void C3dsPointArr::Inspect(ostream& str)
  {
  for(int i=0;i<count;i++)
	{
	char a[256];
	sprintf(a,"%5d - X: %8.3f, Y: %8.3f, Z: %8.3f\r\n",i,arr[i].x,arr[i].y,arr[i].z);
	str<<a;
	}
  }

bool C3dsFaceArr::LoadChunk(istream &str)
  {
  str.read((char *)&count,sizeof(count));
  arr=new S3DSTRIANGLE[count];
  str.read((char *)arr,count*sizeof(S3DSTRIANGLE));
  return C3dsChunk::LoadChunk(str);
  }

void C3dsFaceArr::Inspect(ostream& str)
  {
  for(int i=0;i<count;i++)
	{
	char a[256];
	sprintf(a,"%5d - A: %5d, B: %5d, C: %5d, SMT: %d\r\n",i,arr[i].v1,arr[i].v2,arr[i].v3,arr[i].uk1);
	str<<a;
	}
  }

bool C3dsTexCoord::LoadChunk(istream &str)
  {
  str.read((char *)&count,sizeof(count));
  arr=new S3DSTEXCOORD[count];
  str.read((char *)arr,count*sizeof(S3DSTEXCOORD));
  return C3dsChunk::LoadChunk(str);
  }

void C3dsTexCoord::Inspect(ostream& str)
  {
  for(int i=0;i<count;i++)
	{
	char a[256];
	sprintf(a,"%5d - Tu: %8.3f, Tv: %8.3f\r\n",i,arr[i].tu,arr[i].tv);
	str<<a;
	}
  }

bool C3dsObjectMatrix::LoadChunk(istream &str)
  {
  for(int i=0;i<4;i++)	
    str.read((char *)(mx+i),sizeof(S3DSVECTOR));	
  return C3dsChunk::LoadChunk(str);
  }

void C3dsObjectMatrix::Inspect(ostream& str)
  {
  str<<"Matrix:\r\n\r\n";
  for(int i=0;i<4;i++)
	{
	char a[256];
	sprintf(a,"%8.3f %8.3f %8.3f %8.3f\r\n",mx[i].x,mx[i].y,mx[i].z,(double)(i==3));
	str<<a;
	}
  }

bool C3dsPercentage::LoadChunk(istream &str)
  {
  str.read((char *)&percent,2);
  return C3dsChunk::LoadChunk(str);
  }

void C3dsPercentage::Inspect(ostream& str)
  {
  str<<percent<<'%';
  }

bool C3dsXYZTrack::LoadChunk(istream &str)
  {
  str.read((char *)&trackhd,sizeof(trackhd));
  arr=new S3DSPOSTRACK[trackhd.records];
  str.read((char *)arr,trackhd.records*sizeof(S3DSPOSTRACK));
  return C3dsChunk::LoadChunk(str);
  }

void C3dsXYZTrack::Inspect(ostream& str)
  {
  str<<"Track info: "<<trackhd.uk1<<" frames: "<<trackhd.uk2<<" - "<<trackhd.maxframe;
  str<<" Keys: "<<trackhd.records<<"\r\n\r\n";
  for(unsigned int i=0;i<trackhd.records;i++)
	{
	char a[256];
	sprintf(a,"%5d - X: %8.3f, Y: %8.3f, Z: %8.3f, flags: %08X\r\n",i,arr[i].xpos,arr[i].ypos,arr[i].zpos, arr[i].uk1);
	str<<a;
	}
  }

static int RotFrame[]=
  {0,4,4,8,4,8,8,12,4,8,8,12,8,12,12,16};


bool C3dsRotTrack::LoadChunk(istream &str)
  {
  str.read((char *)&trackhd,sizeof(trackhd));
  rottrack=new S3DSROTTRACK[trackhd.records];
  unsigned long i;
  unsigned short l;
  for (i=0;i<trackhd.records;i++)
	{
	str.read((char *)&rottrack[i].frame,sizeof(rottrack[i].frame));
	str.read((char *)&l,2);
//	assert(l<16);
	str.seekg(RotFrame[l],ios::cur);
	str.read((char *)&rottrack[i].radius,sizeof(rottrack[i].radius));
	str.read((char *)&rottrack[i].xpos,sizeof(rottrack[i].xpos));
	str.read((char *)&rottrack[i].ypos,sizeof(rottrack[i].ypos));
	str.read((char *)&rottrack[i].zpos,sizeof(rottrack[i].zpos));
    if (rottrack[i].xpos==0 && rottrack[i].ypos==0 && rottrack[i].zpos==0) rottrack[i].xpos=1.0f;
	}
  return C3dsChunk::LoadChunk(str);
  }

void C3dsRotTrack::Inspect(ostream& str)
  {
  str<<"Track info: "<<trackhd.uk1<<" frames: "<<trackhd.uk2<<" - "<<trackhd.maxframe;
  str<<" Keys: "<<trackhd.records<<"\r\n\r\n";
  for(unsigned int i=0;i<trackhd.records;i++)
	{
	char a[256];
	sprintf(a,"%5d - X: %6.3f, Y: %6.3f, Z: %6.3f, R: %6.3f\r\n",rottrack[i].frame,rottrack[i].xpos,rottrack[i].ypos,rottrack[i].zpos,rottrack[i].radius);
	str<<a;
	}
  }

void C3dsMeshMatGroups::Inspect(ostream& str)
  {
  char *c;
  unsigned short *d;
  int i;
  c=(char *)data;
  str<<c<<"\r\n\r\n";
  c=strchr(c,0)+1;
  d=(unsigned short *)c;
  for (i=0;i<d[0];i++)
	{
	str<<d[i+1]<<" ";
	if ((i & 0x7)==0x7) str<<"\r\n";
	}
  }

bool C3dsPivot::LoadChunk(istream &str)
  {
  str.read((char *)&pivot,sizeof(pivot));
  return C3dsChunk::LoadChunk(str);  
  }

void C3dsPivot::Inspect(ostream& str)
  {
  char s[256];
  sprintf(s,"Pivot:\r\n\r\n X: %8.3f, Y: %8.3f, Z: %8.3f",pivot.pivotx,pivot.pivoty,pivot.pivotz);
  str<<s;
  }

bool C3dsNodeHdr::LoadChunk(istream &str)
  {
  char szBuff[256];
  int i,j;
  
  i=0;
  do
	{
	if (i+1==sizeof(szBuff)) j=0;else j=str.get();
	szBuff[i++]=j;
	}
  while (j);
  name=_strdup(szBuff);
  str.read((char *)&flags,sizeof(flags));
  str.read((char *)&parent,sizeof(parent));
  return C3dsChunk::LoadChunk(str);  
  }

void C3dsNodeHdr::Inspect(ostream& str)
  {
  str<<"Object: "<<name<<"\r\n\r\n";
  char s[256];
  sprintf(s,"Flags: %08X, Hierarchy: %d",flags,parent);
  str<<s;
  }

void C3dsSmoothGroups::Inspect(ostream& str)
  {
  unsigned long *l=(unsigned long *)data;
  int s=size/4;
  for (int i=0;i<s;i++,l++)
	{
	char s[100];
	sprintf(s,"%d  %08X\r\n",i,*l);
	str<<s;
	}
  }

void C3dsKFSegment::Inspect(ostream& str)
  {
  unsigned long *l=(unsigned long *)data;
  str<<"Frames: "<<l[0]<<" - "<<l[1];
  }

C3dsChunk *C3dsMaster::CreateSubChunk(C3dsChunk &chk)
  {
  C3dsChunk *p;
  switch (chk.GetChunkID())
	{
	case MAT_NAME:
	case INSTANCE_NAME:
	case MAT_MAPNAME:
	case NAMED_OBJECT: p=new C3dsNamedObject();break;
	case COLOR_24: p=new C3dsRGBTriplet();break;
	case POINT_ARRAY: p=new C3dsPointArr();break;
	case FACE_ARRAY: p=new C3dsFaceArr();break;					
	case TEX_VERTS: p=new C3dsTexCoord();break;
	case MESH_MATRIX: p=new C3dsObjectMatrix();break;
	case INT_PERCENTAGE: p=new C3dsPercentage();break;
	case POS_TRACK_TAG:
	case SCL_TRACK_TAG: p=new C3dsXYZTrack();break;
	case ROT_TRACK_TAG: p=new C3dsRotTrack();break;
	case MSH_MAT_GROUP: p=new C3dsMeshMatGroups();break;
	case PIVOT: p=new C3dsPivot();break;
	case NODE_HDR: p=new C3dsNodeHdr();break;
	case SMOOTH_GROUP: p=new C3dsSmoothGroups();break;
	case KFSEG: p=new C3dsKFSegment();break;
	default: return C3dsChunk::CreateSubChunk(chk);
	}  
  return p;
  }

bool C3dsMaster::Read3ds(istream& str)
  {
  DeleteNext(true,true);
  ReadChunkHeader(str);
  if (chunkid!=M3DMAGIC) return false;
  LoadChunk(str);
  return true;
  }

  C3dsChunk *C3dsChunk::FindChunkNext(CHUNKID chunk)
  {
    C3dsChunk *fnd=NULL;
    if (next!=NULL) 
      if (next->chunkid==chunk) fnd=next;
      else fnd=next->FindChunkNext(chunk);
    if (fnd==NULL && chunklist!=NULL)
      if (chunklist->chunkid==chunk) fnd=chunklist;
      else fnd=chunklist->FindChunkNext(chunk);
      return fnd;
  }

  C3dsChunk *C3dsChunk::FindChunkSub(CHUNKID chunk)
  {
    C3dsChunk *fnd=NULL;
    if (chunklist!=NULL)
      if (chunklist->chunkid==chunk) fnd=chunklist;
      else fnd=chunklist->FindChunkSub(chunk);
    if (fnd==NULL && next!=NULL) 
      if (next->chunkid==chunk) fnd=next;
      else fnd=next->FindChunkSub(chunk);
      return fnd;
  }

  bool C3dsData::TakeOver(C3dsData * ruler)
  {
    if (ruler->data!=NULL) return false;
    ruler->data=data;
    ruler->size=size;
    data=NULL;
    size=0;
    return true;
  }

C3dsChunk * C3dsChunk::FindChunk(int nodes, CHUNKID * path)
  {
  C3dsChunk *t=this;
  while (nodes)
	{
	while (t->GetChunkID()!=*path)
	  {
	  t=t->NextChunk();
	  if (t==NULL) return NULL;
	  }
	if (nodes>1) t=t->SubChunk();
	if (t==NULL) return NULL;
	nodes--;
	path++;
	}
  return t;
  }

void C3dsObjectMatrix::GetMatrix(float mm[4][4])
  {
  for (int i=0;i<4;i++)
	{
	mm[i][0]=mx[i].x;
	mm[i][1]=mx[i].y;
	mm[i][2]=mx[i].z;
	mm[i][3]=(float)i==3;
	}
  }

void C3dsObjectMatrix::SetMatrix(float mm[4][4])
  {
  for (int i=0;i<4;i++)
	{
	mx[i].x=mm[i][0];
	mx[i].y=mm[i][1];
	mx[i].z=mm[i][2];
	}
  }

void C3dsChunk::DeleteNext(bool sub, bool delall)
  {
  if (delall)
	if (sub)
	  {
	  if (chunklist) 
		{
		chunklist->DeleteNext(true,delall);
		chunklist->DeleteNext(false,delall);
		delete chunklist;
		chunklist=NULL;
		}
	  }
	else
	  {
	  if (next) 
		{
		next->DeleteNext(true,delall);
		next->DeleteNext(false,delall);
		delete chunklist;
		next=NULL;
		}
	  }
  else
	{
	if (sub)
	  {
	  C3dsChunk *chk=next;
	  if (chk) next=chk->next;
	  delete chk;
	  }
	else
	  {
	  C3dsChunk *chk=next;
	  if (chk) next=chk->next;
	  delete chk;
	  }
	}
  }


long C3dsChunk::CreateChunk(ostream& str)
  {
  long ret=str.tellp();
  str.write((char *)&chunkid,2);
  str.write((char *)&ret,4);
  return ret;
  }

void C3dsChunk::CloseChunk(ostream& str,long create_ret)
  {
  C3dsChunk *sub=SubChunk();
  while (sub)
	{
	sub->Save(str);
	sub=sub->NextChunk();
	}
  long back=str.tellp();
  str.seekp(create_ret+2);
  long ret=back-create_ret;
  str.write((char *)&ret,4);
  str.seekp(back);
  }

void C3dsChunk::Save(ostream& str)
  {
  long creat=CreateChunk(str);
  CloseChunk(str,creat);
  }

void C3dsData::Save(ostream& str)
  {
  long creat=CreateChunk(str);
  if (size) str.write((char *)data,size);
  CloseChunk(str,creat);
  }

void C3dsNamedObject::Save(ostream& str)
  {
  long creat=CreateChunk(str);
  str.write(name,strlen(name)+1);
  CloseChunk(str,creat);
  }

void C3dsRGBTriplet::Save(ostream &str)
  {
  long creat=CreateChunk(str);
  str.write((char *)&rgb,sizeof(rgb));
  CloseChunk(str,creat);
  }

void C3dsPointArr::Save(ostream &str)
  {
  long creat=CreateChunk(str);
  str.write((char *)&count,sizeof(count));
  str.write((char *)arr,count*sizeof(S3DSVECTOR));
  CloseChunk(str,creat);
  }

void C3dsFaceArr::Save(ostream &str)
  {
  long creat=CreateChunk(str);
  str.write((char *)&count,sizeof(count));
  str.write((char *)arr,count*sizeof(S3DSTRIANGLE));
  CloseChunk(str,creat);
  }

void C3dsTexCoord::Save(ostream &str)
  {
  long creat=CreateChunk(str);
  str.write((char *)&count,sizeof(count));
  str.write((char *)arr,count*sizeof(S3DSTEXCOORD));
  CloseChunk(str,creat);
  }

void C3dsObjectMatrix::Save(ostream &str)
  {
  long creat=CreateChunk(str);
  for(int i=0;i<4;i++)	
    str.write((char *)(mx+i),sizeof(S3DSVECTOR));	
  CloseChunk(str,creat);
  }

void C3dsPercentage::Save(ostream &str)
  {
  long creat=CreateChunk(str);
  str.write((char *)&percent,2);
  CloseChunk(str,creat);
  }

void C3dsXYZTrack::Save(ostream& str)
  {
  long creat=CreateChunk(str);
  str.write((char *)&trackhd,sizeof(trackhd));
  str.write((char *)arr,trackhd.records*sizeof(S3DSPOSTRACK));
  CloseChunk(str,creat);
  }

void C3dsRotTrack::Save(ostream &str)
  {
  long creat=CreateChunk(str);
  str.write((char *)&trackhd,sizeof(trackhd));
  unsigned long i;
  unsigned short l;
  for (i=0;i<trackhd.records;i++)
	{
	str.write((char *)&rottrack[i].frame,sizeof(rottrack[i].frame));
	l=0;
	str.write((char *)&l,2);
	str.write((char *)&rottrack[i].radius,sizeof(rottrack[i].radius));
	str.write((char *)&rottrack[i].xpos,sizeof(rottrack[i].xpos));
	str.write((char *)&rottrack[i].ypos,sizeof(rottrack[i].ypos));
	str.write((char *)&rottrack[i].zpos,sizeof(rottrack[i].zpos));
	}
  CloseChunk(str,creat);
  }

void C3dsPivot::Save(ostream &str)
  {
  long creat=CreateChunk(str);
  str.write((char *)&pivot,sizeof(pivot));
  CloseChunk(str,creat);
  }

void C3dsNodeHdr::Save(ostream& str)
  {
  long creat=CreateChunk(str);
  str.write(name,strlen(name)+1);
  str.write((char *)&flags,sizeof(flags));
  str.write((char *)&parent,sizeof(parent));
  CloseChunk(str,creat);
  }


void C3dsChunk::InsertChunk(bool sub, C3dsChunk *nw)
  {
  if (sub)
	{
	nw->next=chunklist;
	chunklist=nw;
	}
  else
	{
	nw->next=next;
	next=nw;
	}
  }

