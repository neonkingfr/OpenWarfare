#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "g3dlib.h"

#define G3DFONT "G3DFONT"
#define G3DFONTLEN 8
static int begincount;
static char drawbuff;

#include "g3dfiles.c"

TUNIPICTURE *LoadUnif(HANDLE f);

static HG3DFONT g3dLoadFontF(HANDLE f)
  {
  char text[G3DFONTLEN];
  HG3DFONT fnt;
  TUNIPICTURE *uni;
  int lasthandle;
  int i;
  
  if (!gread(text,G3DFONTLEN,1,f) || strncmp(text,G3DFONT,G3DFONTLEN)!=0) return NULL;
  fnt=(HG3DFONT)malloc(sizeof(*fnt));
  if (fnt==NULL) return fnt;
  if (!gread(fnt->chardef,sizeof(fnt->chardef),1,f)) {free(fnt);return NULL;}
  lasthandle=-1;
  for(i=0;i<256;i++)
	{
	if (lasthandle!=fnt->chardef[i].handle)
	  {
	  lasthandle=fnt->chardef[i].handle;
	  uni=LoadUnif(f);	
	  if (uni!=NULL) 
		{
		fnt->chardef[i].handle=g3dtRegisterTexture(uni);
		fnt->bitmapsize=uni->ysize;
		free(uni);
		}
	  else  fnt->chardef[i].handle=0;
	  }
	else fnt->chardef[i].handle=fnt->chardef[i-1].handle;
	}
  return fnt;
  }

HG3DFONT g3dLoadFont(char *filename)
  {
  HANDLE f;
  HG3DFONT font;

  f=gopenread(filename);
  if (f==NULL) return NULL;
  font=g3dLoadFontF(f);
  gclose(f);
  return font;
  }

void g3dDestroyFont(HG3DFONT fnt)
  {
  int i;
  int lasthandle=0;

  for(i=0;i<256;i++)	
	if (lasthandle!=fnt->chardef[i].handle)
	  {
	  lasthandle=fnt->chardef[i].handle;
	  g3dtUnregisterTexture(lasthandle);
	  }
  free(fnt);  
  }

//----------------- contexts ------------------------

HG3DCONTEXT g3dActiveContext;
#define act g3dActiveContext

static G3DCONTEXT basic;

void g3d2dInit()
  {
  memset(&basic,0,sizeof(basic));
  basic.wl=basic.wt=0;
  basic.wr=g3dQuerySettingInt(G3DQ_MAXX);
  basic.wb=g3dQuerySettingInt(G3DQ_MAXY);
  act=&basic;
  g3dSetColor(G3D_RGB(1.0f,1.0f,1.0f));
  g3dSetBkColor(G3D_RGB(0.0f,0.0f,0.0f));
  g3dSetFont(NULL);
  g3dSetFontSize(10);
  g3dSetAlign(G3DF_ALEFT|G3DF_ATOP);
  g3dSetFontColor(G3D_RGB(1.0f,1.0f,1.0f));
  g3dSetShadowDistance(0,0);
  g3dSetShadowColor(G3D_RGB(0.0f,0.0f,0.0f));
  g3dMoveTo(0,0);
  g3dSetContextZ(0.0f);
  begincount=0;
  act->font.aspect=1.0f;
  g3dSetWriteDirection(0);
  }


HG3DCONTEXT g3dCreateContext()
  {
  HG3DCONTEXT cnt=(HG3DCONTEXT)malloc(sizeof(*cnt));
  if (cnt==NULL) return cnt;
  memcpy(cnt,act,sizeof(*cnt));
  cnt->xcur=0;
  cnt->ycur=0;
  return cnt;
  }

HG3DCONTEXT g3dCreateWindow(int wl,int wt, int wr, int wb)
  {
  HG3DCONTEXT cnt=g3dCreateContext();
  if (cnt==NULL) return cnt;
  cnt->wl=wl;
  cnt->wr=wr;
  cnt->wt=wt;
  cnt->wb=wb;
  return cnt;
  }


HG3DCONTEXT g3dSetActive(HG3DCONTEXT cnt)
  {
  HG3DCONTEXT res=act;
  if (cnt==NULL) cnt=&basic;
  act=cnt;
  return res;
  }

void g3dSetFont(HG3DFONT sfont)
  {
  act->font.curfont=sfont;
  if (sfont==NULL) return;
  act->font.bmpmul=sfont->bitmapsize;
  act->font.size=1.0;
  }

void g3dSetContextZ(float z)
  {
  float min=(float)g3dQuerySettingInt(G3DQ_ZMIN);
  float max=(float)g3dQuerySettingInt(G3DQ_ZMAX);
  act->basezet=min+(max-min)*z;
  }

void g3dSetWriteDirection(float radx)
  {
  float xs,ys;

  xs=(float)cos(radx);
  ys=(float)sin(radx);
  act->font.xx=xs;
  act->font.xy=ys;
  act->font.yx=-ys;
  act->font.yy=xs;
  }

void g3dSetFontSkew(float radx)
  {
  float xs,ys;

  xs=-act->font.xy;
  ys=act->font.xx;
  act->font.yx=(float)(xs*cos(radx)-ys*sin(radx));
  act->font.yy=(float)(xs*sin(radx)+ys*cos(radx));
  }

void g3dClipToWindow()
  {
  unsigned long cl,cr,ct,cb;
  int maxx=g3dQuerySettingInt(G3DQ_MAXX);
  int maxy=g3dQuerySettingInt(G3DQ_MAXY);

  if (act->wl<0) cl=0;else cl=(unsigned long)act->wl;
  if (act->wt<0) ct=0;else ct=(unsigned long)act->wt;
  if (act->wr>maxx) cr=maxx;else cr=(unsigned long)act->wr;
  if (act->wb>maxy) cb=maxy;else cb=(unsigned long)act->wb;
  g3dClipWindow(cl,ct,cr,cb);
  }

void g3dClipToScreen()
  {
  unsigned long maxx=g3dQuerySettingInt(G3DQ_MAXX);
  unsigned long maxy=g3dQuerySettingInt(G3DQ_MAXY);

  g3dClipWindow(0,0,maxx,maxy);
  }

static void Bar(int x1, int y1, int x2, int y2, long color, G3D_TLVERTEX *vt)
  {
  vt[0].coord[XVAL]=(float)x1;
  vt[0].coord[YVAL]=(float)y1;
  vt[0].coord[ZVAL]=act->basezet;
  vt[0].coord[WVAL]=1.0f;
  vt[0].color=color;
  vt[0].tu=0.0f;
  vt[0].tv=0.0f;
  vt[1].coord[XVAL]=(float)x2+0.999f;
  vt[1].coord[YVAL]=(float)y1;
  vt[1].coord[ZVAL]=act->basezet;
  vt[1].coord[WVAL]=1.0f;
  vt[1].color=color;
  vt[1].tu=0.0f;
  vt[1].tv=0.999f;
  vt[2].coord[XVAL]=(float)x2+0.999f;
  vt[2].coord[YVAL]=(float)y2+0.999f;
  vt[2].coord[ZVAL]=act->basezet;
  vt[2].coord[WVAL]=1.0f;
  vt[2].color=color;
  vt[2].tu=0.999f;
  vt[2].tv=0.999f;
  vt[3].coord[XVAL]=(float)x1;
  vt[3].coord[YVAL]=(float)y2+0.999f;
  vt[3].coord[ZVAL]=act->basezet;
  vt[3].coord[WVAL]=1.0f;
  vt[3].color=color;
  vt[3].tu=0.999f;
  vt[3].tv=0.0f;
  }

void g3dLine(int x1, int y1,int x2, int y2)
  {
  G3D_TLVERTEX vt[2];

/*  if (x1<x2){x2+=1;}
  else {x1+=1;}
  if (y1<y2){y2+=1;}
  else {y1+=1;}*/
  vt[0].coord[XVAL]=(float)(x1+act->wl+0.5f);
  vt[0].coord[YVAL]=(float)(y1+act->wt+0.5f);
  vt[0].coord[ZVAL]=act->basezet;
  vt[0].coord[WVAL]=1.0f;
  vt[0].color=act->color;
  vt[0].tu=0.0f;
  vt[0].tv=0.0f;
  vt[1].coord[XVAL]=(float)(x2+act->wl+0.5f);
  vt[1].coord[YVAL]=(float)(y2+act->wt+0.5f);
  vt[1].coord[ZVAL]=act->basezet;
  vt[1].coord[WVAL]=1.0f;
  vt[1].color=act->color;
  vt[1].tu=0.0f;
  vt[1].tv=0.9999f;
  g3dtSelectTexture(0,0);
  g3dDrawVertexArray(G3D_LINE,2,vt);
  }

void g3dClearContext()
  {
  G3D_TLVERTEX vt[4];
  Bar(act->wl,act->wt,act->wr,act->wb,act->bgcolor,vt);
  g3dtSelectTexture(0,0);
  g3dDrawVertexArray(G3D_POLYGON,4,vt);
  }

void g3dFillTexture(long handle, float wl, float wt, float wr, float wb)
  {
  G3D_TLVERTEX vt[4];
  Bar(act->wl,act->wt,act->wr,act->wb,act->bgcolor,vt);
  vt[0].tv=vt[3].tv=wl;
  vt[1].tv=vt[2].tv=wr;
  vt[0].tu=vt[1].tu=wt;
  vt[2].tu=vt[3].tu=wb;
  g3dtSelectTexture(0,handle);
  g3dDrawVertexArray(G3D_POLYGON,4,vt);
  }

static int lastr,lastz,lastc;

void g3dBeginPaint(int buffer)
  {
  if (begincount++) return;
  drawbuff=buffer;
  lastr=g3dSetRenderTarget((char)buffer);
//  lastz=g3dEnableZBuffer(0);
  lastc=g3dSetCullMode(0);
  g3dtSelectTexture(0,0);
  }

void g3dEndPaint()
  {
  if (--begincount) return;
  g3dSetRenderTarget((char)lastr);
//  g3dEnableZBuffer((char)lastz);
  g3dSetCullMode(lastc);
  }

char g3dGetCharExtent(int chr, int *xs, int *ys)
  {
  float x,y,w;
  G3DCHARDEF *chd;
  if (act->font.curfont==NULL) return -1;
  chd=act->font.curfont->chardef+chr;
  y=chd->tu2-chd->tu1;
  x=chd->tv2-chd->tv1;
  w=act->font.bmpmul*act->font.size;
  x*=w;
  y*=w*act->font.aspect;
  *xs=(int)x;
  *ys=(int)y;
  return 0;
  }

char g3dGetTextExtent(char *text,int chrs, int *x, int *y)
  {
  int xs,ys,xa,ya,i;
  
  if (chrs==0) chrs=(signed)((unsigned)((signed)-1)>>1);
  xs=0;ys=0;  
  for(i=chrs; *text && i>0;i--)
	{
	if (g3dGetCharExtent(*text,&xa,&ya)) return -1;
	xs+=xa;
	ys+=max(ya,ys);
	}
  *x=xs+1;
  *y=ys+1;
  return 0;
  }

char g3dTestPoint(int xp, int yp)
  {
  return (act->wl<=xp && act->wr>=xp && act->wt<=yp && act->wb>=yp);
  }



char g3dDrawCharEx(int *x, int *y,int chr, char shadow)
  {
  int xs,ys;
  G3D_TLVERTEX vt[4];
  G3DCHARDEF *chd;
  float xx,xy,yx,yy;
  unsigned long color=shadow?act->font.shadowcolor:act->font.color;

  if (g3dGetCharExtent(chr,&xs,&ys)) return -1;
  chd=act->font.curfont->chardef+chr;
  xx=(xs)*act->font.xx;
  xy=(xs)*act->font.xy;
  yx=(ys)*act->font.yx;
  yy=(ys)*act->font.yy;
  vt[0].coord[XVAL]=(float)*x+0.5f; 
  vt[0].coord[YVAL]=(float)*y+0.5f;
  vt[0].coord[ZVAL]=act->basezet;
  vt[0].coord[WVAL]=1.0f;
  vt[0].tu=chd->tu1;
  vt[0].tv=chd->tv1;
  vt[0].color=color;
  vt[1].coord[XVAL]=*x+xx+0.5f;
  vt[1].coord[YVAL]=*y+xy+0.5f;
  vt[1].coord[ZVAL]=act->basezet;
  vt[1].coord[WVAL]=1.0f;
  vt[1].tu=chd->tu1;
  vt[1].tv=chd->tv2;
  vt[1].color=color;
  vt[2].coord[XVAL]=*x+xx+yx+0.5f;
  vt[2].coord[YVAL]=*y+xy+yy+0.5f;
  vt[2].coord[ZVAL]=act->basezet;
  vt[2].coord[WVAL]=1.0f;
  vt[2].tu=chd->tu2;
  vt[2].tv=chd->tv2;
  vt[2].color=color;
  vt[3].coord[XVAL]=*x+yx+0.5f;
  vt[3].coord[YVAL]=*y+yy+0.5f;
  vt[3].coord[ZVAL]=act->basezet;
  vt[3].coord[WVAL]=1.0f;
  vt[3].tu=chd->tu2;
  vt[3].tv=chd->tv1;
  vt[3].color=color;
  g3dtSelectTexture(0,chd->handle);
  g3dDrawVertexArray(G3D_POLYGON,4,vt);
  *x=(int)(x[0]+xx);
  *y=(int)(y[0]+xy);
  return 0;
  }

void g3dOutText(char *text, int len)
  {
  int xp;
  int yp;
  float xb;
  float yb;
  float xe;
  float ye;
  float sx,sy,b;
  int wsx=act->wr-act->wl+1;
  int wsy=act->wb-act->wt+1;
  char *fspace=NULL;
  char *c=text;
  char *last=text;
  int xs,ys,ysm=0,yl=0;
  char run;
  char shadows=act->font.shadowdistancex!=0 || act->font.shadowdistancey!=0;
 
  g3dGetCharExtent(32,&xs,&ys);  
  yl=ys;if (len==0) len--;
  do
	{
	xp=act->xcur;
	yp=act->ycur;
	xb=xe=(float)xp+act->wl;
	yb=ye=(float)yp+act->wt;
	ysm=0;
	while (*c && *c!='\n')
	  {
	  g3dGetCharExtent(*(unsigned char *)c,&xs,&ys);
	  if (ysm<ys) 
		{
		if (act->font.align & G3DF_ATOP) {sx=0.0f;sy=0.0f;}
		else if (act->font.align & G3DF_ABOTTOM) 
		  {
		  b=(float)(ys-ysm);
		  sx=act->font.yx*b;
		  sy=act->font.yy*b;
		  }
		else
		  {
		  b=(ys-ysm)/2.0f;
		  sx=act->font.yx*b;
		  sy=act->font.yy*b;
		  }
		xb-=sx;xe-=sx;
		yb-=sy;ye-=sy;
		yl=ysm=ys;
		}	  
	  sx=act->font.xx*xs;
	  sy=act->font.xy*xs;
	  if (act->font.align & G3DF_ALEFT) 
		{
		xe+=sx;
		ye+=sy;
		}
	  else if (act->font.align & G3DF_ARIGHT) 
		{
		xb-=sx;
		yb-=sy;
		}
	  else
		{
		sx*=0.5f;sy*=0.5f;
		xe+=sx;ye+=sy;
		xb-=sx;yb-=sy;
  		}
	  if (!g3dTestPoint((int)xb,(int)yb) || !g3dTestPoint((int)xe,(int)ye))
		{
		if (fspace!=NULL) c=fspace;
		break;
		}
	  if (*c==' ') fspace=c+1;
	  c++;
	  len--;
	  if (len==0) break;
	  }	
	if (last<c)
	  {
  	  xp=(int)xb;
	  yp=(int)yb;
	  while (last<c) 
		{		
		if (shadows)
		  {
		  int xa=xp+act->font.shadowdistancex;
		  int ya=yp+act->font.shadowdistancey;
		  g3dDrawCharEx(&xa,&ya,*(unsigned char *)last,1);
		  }
		g3dDrawCharEx(&xp,&yp,*(unsigned char *)last,0);
		last++;
		}
	  run=2;
	  }	
	else run=0;
	act->xcur=(int)(act->xcur-act->font.xy*yl);
	act->ycur=(int)(act->ycur+act->font.xx*yl);
	if (act->xcur<0) act->xcur=0;
	else if (act->xcur>wsx) act->xcur=wsx;
	else run++;
	if (act->ycur<0) act->ycur=0;
	else if (act->ycur>wsy) act->ycur=wsy;
	else run++;
	if (*c=='\n') c++;
	}
  while (*c && len && run>1);
  act->xcur=xp-act->wl;
  act->ycur=yp-act->wt;
  }

void g3d2dDone()
  {
  }

void g3d2dShowMouse(int x, int y, int handle, TSCREENCLIP *back)
  {
  G3D_TLVERTEX vt[4];
  TG3DTEXINFO tinfo;
  float z;

  if (!g3dIsClipEmpty(back))g3dPutScreenClip(G3DPC_NOPARAMS,back);	
  if (g3dtGetTexInfo(handle,&tinfo))
	{
	if (!g3dIsClipEmpty(back)) g3dDestroyClip(back);
	return;
	}
  if (back!=NULL)
	{
	if (tinfo.xsize!=back->sizx || tinfo.ysize!=back->sizy)
	  {
	  g3dDestroyClip(back);
	  g3dNewClip(back,x,y,x+tinfo.xsize-1,y+tinfo.ysize-1,G3DCC_NOFLAGS);	
	  }
	else
	  {
	  back->orgx=x;
	  back->orgy=y;
	  }
	g3dUpdateScreenClip(back,G3D_CURBUFFER);
	}
  z=act->basezet;
  act->basezet=(float)g3dQuerySettingInt(G3DQ_ZMIN);
  Bar(x,y,tinfo.xsize+x-1,tinfo.ysize+y-1,G3D_RGB(1.0f,1.0f,1.0f),vt);
  act->basezet=z;
  g3dtSelectTexture(0,handle);
  g3dDrawVertexArray(G3D_POLYGON,4,vt);
  }


void g3dRectangle(int wl,int wt, int wr, int wb)
  {
  g3dLine(wl,wt,wr,wt);
  g3dLine(wr,wt,wr,wb);
  g3dLine(wr,wb,wl,wb);
  g3dLine(wl,wb,wl,wt);
  }