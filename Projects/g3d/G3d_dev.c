#include <stdlib.h>
#include <windows.h>
#include <stdio.h>
#include <malloc.h>
#include "g3dlib.h"
#include <assert.h>

#undef g3dInitHardware
#undef g3dCloseHardware

#define TEXTUREHASH 1001



static void Unimplemented()
  {
  MessageBox(NULL,"Function was not found in DLL\n",NULL,MB_OK);
  assert(0);
  abort();
  }

#undef IMPORT
#define IMPORT(function) _g3df_##function function=(_g3df_##function)Unimplemented

#include "g3d_dev.imp"

//-------------------- plugins ----------------------------

IMPORT(g3dInitPlugin);

//-------------------- DLL load ----------------------------

static HMODULE hG3dDll=NULL;

#undef IMPORT
#define IMPORT(function) \
    function=(_g3df_##function)GetProcAddress(ImportHandle,#function);\
    if (function==NULL) function=(_g3df_##function)Unimplemented;


char g3dInitDevice(const char *devicename)
  {
  HMODULE ImportHandle;
  ImportHandle=hG3dDll=LoadLibrary(devicename);
  if (hG3dDll==NULL) return -1;

#include "g3d_dev.imp"

return 0;
}

static TG3DPLUGINIT *registred=NULL,*inited=NULL;
extern TUNIPICTURE (*LoadUniFCallBack)(HANDLE);
extern char *cmGetImportName(void);
void g3dCloseHardware3D();

char g3dRegisterModule(const char *modulename)
  {
  HMODULE ImportHandle;
  char *name;
  char res;
  TG3DPLUGINIT *plg;
  ImportHandle=LoadLibrary(modulename);
  IMPORT(g3dInitPlugin);
  if ((void *)g3dInitPlugin==(void *)Unimplemented) return G3DPE_FAILED;
  plg=(TG3DPLUGINIT *)malloc(sizeof(TG3DPLUGINIT));
  name=malloc(strlen(modulename)+1);
  if (plg==NULL || name==NULL) 
	{
	FreeModule(ImportHandle);
	free(plg);free(name);
	return G3DPE_FAILED;
	}
  memset(plg,0,sizeof(TG3DPLUGINIT));
  strcpy(name,modulename);
  plg->size=sizeof(*plg);
  plg->pathname=name;
  plg->hInstance=(long)ImportHandle;
  if ((res=g3dInitPlugin(plg))==G3DPE_OK) 
	{
	plg->next=registred;
	registred=plg;  
	return 0;
	}
  else
	{
	free(plg);
	free(name);
	FreeModule(ImportHandle);
	return res;
	}
  }

#undef IMPORT
#define IMPORT(function) function=(_g3df_##function)Unimplemented;

void g3dFreeDevice()
  {
  TG3DPLUGINIT *plg;
  if (g3dIsImplemented(g3dUnload)) g3dUnload();
  FreeLibrary(hG3dDll);  

#include "g3d_dev.imp"

  while (registred) 
	{
	FreeLibrary((HMODULE)registred->hInstance);
	plg=registred;registred=plg->next;
	free(plg->pathname);
	free(plg);
	}
  }

static char ClearPRes(char res, char *name)
  {
  char *error;
  char close;
  switch (res)
	{
	case G3DPE_OK:
	case G3DPE_FAILED: return 0;
	case G3DPE_DROPINIT:g3dCloseHardware3D();return -1;
	case G3DPE_DROPAPP: error="Plug-in \" %s \" failed and dropped application!";close=1;break;
	default: error="Plug-in \" %s \" returns unknown status!";close=0;break;
	}
  g3dSwitchToGDI(1);
	{
	char *text=(char *)alloca(strlen(name)+100);
    sprintf(text,error,name);
    MessageBox(NULL,text,NULL,MB_OK|MB_TASKMODAL);
	}
  if (close) {g3dCloseHardware3D();abort();}
  g3dSwitchToGDI(0);
  return 0;
  }

static TG3DINITSTRUCT init={0};

static char g3dInitPluginsCall(char dropenable)
  {
  TG3DPLUGINIT *plg,**top;
  plg=registred;top=&registred;
  init.ImportUni=(void **)&LoadUniFCallBack;
  init.CreateUni=(void *)cmCreateUni;
  init.CreateMipMap=(void *)cmCreateMipMap;
  init.malloc=malloc;
  init.free=free;
  init.GetImportName=cmGetImportName;
  init.size=sizeof(init);
  while (plg)
	{
	char pres;
	if (plg->g3dplgInitHardware)
	  {
	  pres=plg->g3dplgInitHardware(&init);
	  if (dropenable && ClearPRes(pres,plg->pathname)) {return -1;}
	  }
	else
	  pres=G3DPE_OK;
    if (pres==G3DPE_OK)
	  {
	  *top=plg->next;
	  plg->next=inited;
	  inited=plg;
	  plg=*top;
	  }
	else
	  {
	  top=&(plg->next);
	  plg=*top;
	  }	  
	}
  return 0;
  }

char g3dInitHardware3D(unsigned long handle,int xres,int yres,int cols, int freq)
  {
  int res=g3dInitHardware3D_(handle,xres,yres,cols,freq);
  if (res) return res;
  res=texInitManager(TEXTUREHASH);
  g3d2dInit();
  if (res) {g3dCloseHardware3D();return res;}
  init.xres=xres;
  init.yres=yres;
  init.bpp=16;
  init.freq=freq;
  init.wnd=handle;
  return g3dInitPluginsCall(1);
  }

void g3dInitModules(void)
  {
  if (init.size!=sizeof(init))
	{
	init.xres=0;
	init.yres=0;
	init.bpp=0;
	init.freq=0;
	init.wnd=0;
	}
  g3dInitPluginsCall(0);
  }

void g3dCloseHardware3D()
  {
  TG3DPLUGINIT *plg;
  while (inited)
	{
	plg=inited;
	if (plg->g3dplgDoneHardware)
	  {
	  plg->g3dplgDoneHardware(&init);
	  inited=plg->next;
	  plg->next=registred;
	  registred=plg;
	  }
	}
  g3d2dDone();
  texDoneManager();
  g3dCloseHardware3D_();
  init.size=0;
  }



char g3dIsImplemented(void *function)
  {
  return (function!=(void *)(Unimplemented));
  }
