# Microsoft Developer Studio Project File - Name="g3d" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=g3d - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "g3d.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "g3d.mak" CFG="g3d - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "g3d - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "g3d - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Projects/g3d", PPDAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "g3d - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /Ob2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"c:\msdev\projects\g3d\g3d.lib"

!ELSEIF  "$(CFG)" == "g3d - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /Z7 /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /YX"g3dlib.h" /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"g3d.lib"

!ENDIF 

# Begin Target

# Name "g3d - Win32 Release"
# Name "g3d - Win32 Debug"
# Begin Group "g3d files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\colman.c
# End Source File
# Begin Source File

SOURCE=.\colman.h
# End Source File
# Begin Source File

SOURCE=.\Dbgwnd.c
# End Source File
# Begin Source File

SOURCE=.\Dbgwnd.h
# End Source File
# Begin Source File

SOURCE=.\G3d.c
# End Source File
# Begin Source File

SOURCE=.\g3d.h
# End Source File
# Begin Source File

SOURCE=.\g3d2d.c
# End Source File
# Begin Source File

SOURCE=.\g3d2d.h
# End Source File
# Begin Source File

SOURCE=.\G3d_dev.c
# End Source File
# Begin Source File

SOURCE=.\G3d_dev.h
# End Source File
# Begin Source File

SOURCE=.\g3d_dev.imp
# End Source File
# Begin Source File

SOURCE=.\G3dbody.c
# End Source File
# Begin Source File

SOURCE=.\G3dbody.h
# End Source File
# Begin Source File

SOURCE=.\G3dcfg.c
# End Source File
# Begin Source File

SOURCE=.\G3dcfg.h
# End Source File
# Begin Source File

SOURCE=.\g3dfiles.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\G3dio.c
# End Source File
# Begin Source File

SOURCE=.\G3dio.h
# End Source File
# Begin Source File

SOURCE=.\G3dlib.h
# End Source File
# Begin Source File

SOURCE=.\g3dmc.h
# End Source File
# Begin Source File

SOURCE=.\G3dmdl2.c
# End Source File
# Begin Source File

SOURCE=.\G3dmdl2.h
# End Source File
# Begin Source File

SOURCE=.\G3dmodel.c
# End Source File
# Begin Source File

SOURCE=.\G3dmodel.h
# End Source File
# Begin Source File

SOURCE=.\g3dsys.c
# End Source File
# Begin Source File

SOURCE=.\g3dsys.h
# End Source File
# Begin Source File

SOURCE=.\Hash.c
# End Source File
# Begin Source File

SOURCE=.\Hash.h
# End Source File
# Begin Source File

SOURCE=.\invmat.cpp

!IF  "$(CFG)" == "g3d - Win32 Release"

!ELSEIF  "$(CFG)" == "g3d - Win32 Debug"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Lzwc.c
# End Source File
# Begin Source File

SOURCE=.\Lzwc.h
# End Source File
# Begin Source File

SOURCE=.\Matrix.c
# End Source File
# Begin Source File

SOURCE=.\matrix.h
# End Source File
# Begin Source File

SOURCE=.\texman.c
# End Source File
# Begin Source File

SOURCE=.\texman.h
# End Source File
# End Group
# End Target
# End Project
