#pragma once
#include <projects\bredy.libs\extendedpropertysheet\propshtextwithbutton.h>

class PropShComboEdit :
  public PropShTextWithButton
{
  HWND _list;
  int _listHeight;  
public:
  PropShComboEdit(void);
  ~PropShComboEdit(void);

  virtual void OnButtonPressed();
  virtual bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
  virtual void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);
  bool ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result);

  void AddItem(const tchar *value, long userData=0);
  int FindItem(const tchar *value, int index=-1);
  bool DeleteItem(int index);
  int GetCurItem();
  void SelectItem(int index);
  long GetItemData(int index);
  bool SetItemData(int index, long data);
  void DropDown();
  void CloseUp();
  bool IsDroppedDown();

};
