#pragma once
#include "afxwin.h"
#include <el/ParamFile/ParamFile.hpp>
#include <afxwin.h>


// DlgAddModelDefinition dialog

class DlgAddModelDefinition : public CDialog
{
	DECLARE_DYNAMIC(DlgAddModelDefinition)

public:
	DlgAddModelDefinition(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgAddModelDefinition();

// Dialog Data
	enum { IDD = IDD_ADDMODELDEFINITION };

    ParamClassPtr _cfgModels;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString vDefName;
  CEdit wDefName;
  CComboBox wDerived;
  CString vDerived;
  CString vSuffix;
  afx_msg void OnEnChangeName();
  virtual BOOL OnInitDialog();
  void DialogRules();

protected:
  virtual void OnOK();
public:
  CButton wClone;
  BOOL vClone;
  afx_msg void OnBnClickedCheck2();
  afx_msg void OnBnClickedButton1();
};
