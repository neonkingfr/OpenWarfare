#include "StdAfx.h"
#include ".\propshgroupwithswitches.h"

#define SWITCHES_BASE 32768

PropShGroupWithSwitches::PropShGroupWithSwitches(void)
{
  _ident=30;
  _lastItemCount=0;
}

PropShGroupWithSwitches::~PropShGroupWithSwitches(void)
{
}

void PropShGroupWithSwitches::UpdateChkBoxCount()
{
  int itmcnt=this->GetItemCount();
  if (_lastItemCount!=itmcnt)
  {
    if (itmcnt<_lastItemCount)
    {
      for (int i=itmcnt;i<_lastItemCount;i++)
        DestroyWindow(GetDlgItem(_hCanvas,SWITCHES_BASE+i));
    }
    else
    {
      for (int i=_lastItemCount;i<itmcnt;i++)
      {
        CreateWindow("BUTTON"," ",WS_VISIBLE|WS_CHILD|BS_AUTOCHECKBOX|WS_TABSTOP,0,0,0,0,_hCanvas,(HMENU)(SWITCHES_BASE+i),GetInstance(),NULL);        
        CheckDlgButton(_hCanvas,SWITCHES_BASE+i,BST_CHECKED);
      }
    }
    _lastItemCount=itmcnt;
  }
}

void PropShGroupWithSwitches::UpdateGroupContent(PropShHDWP &hdwp, int x, int y, int wspace, int hspace, int &width, int &height)
{
  __super::UpdateGroupContent(hdwp,x+_ident,y,wspace-_ident,hspace,width,height);  
  width+=_ident;
  int itmcnt=this->GetItemCount();
  hdwp.Flush();
  UpdateChkBoxCount();
  for (int i=0;i<itmcnt;i++)
  {
    PropShItem *itm=GetItem(i);
    HWND hWnd=itm->GetWindowHandle();
    if (hWnd)
    {
      RECT rc;
      GetWindowRect(hWnd,&rc);
      ScreenToClient(_hCanvas,reinterpret_cast<POINT *>(&rc));
      ScreenToClient(_hCanvas,reinterpret_cast<POINT *>(&rc)+1);
      rc.left=x;
      rc.right=x+_ident;
      hdwp.SetWindowRect(GetDlgItem(_hCanvas,i+SWITCHES_BASE),rc);
    }
  }

}

LRESULT PropShGroupWithSwitches::OnCommand(WORD wNotifyCode,WORD wID, HWND ctl)
{  
  if (wID>=SWITCHES_BASE)
  {
    int index=wID-SWITCHES_BASE;
    if (index<GetItemCount())
    {
      if (wNotifyCode==BN_CLICKED)
      {
        bool checked=IsDlgButtonChecked(_hCanvas,wID)==BST_CHECKED;  
        GetItem(index)->EnableItem(checked);
        return 0;
      }
    }
  }
  return __super::OnCommand(wNotifyCode,wID,ctl);
}

void PropShGroupWithSwitches::EnableItem(int index,bool enable)
{
  UpdateChkBoxCount();
  if (index>=0 && index<GetItemCount())
  {
    CheckDlgButton(_hCanvas,SWITCHES_BASE+index,enable?BST_CHECKED:BST_UNCHECKED);
    GetItem(index)->EnableItem(enable);
  }
}

void PropShGroupWithSwitches::HideCheckbox(int index,bool hide)
{
  UpdateChkBoxCount();
  ShowWindow(GetDlgItem(_hCanvas,SWITCHES_BASE+index),hide?SW_HIDE:SW_SHOW);
}

bool PropShGroupWithSwitches::IsItemEnabled(const PropShBase *item)
{
  for (int i=0,cnt=GetItemCount();i<cnt;i++)
  {
    if (GetItem(i)==item) 
    {
      return IsDlgButtonChecked(_hCanvas,SWITCHES_BASE+i)==BST_CHECKED;
    }
  }
  return false;
}