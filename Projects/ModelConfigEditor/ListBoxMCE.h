#pragma once

#include "ModelConfig.h"
#include "SafeEntryRef.h"


// CListBoxMCE
struct ContentInfo
{
  CString title;
  int ident;
  bool opened;
  SafeEntryRef entryInfo;
  int icon;
  bool bold;
  unsigned char palcolor;
  unsigned long lines;

  ContentInfo():ident(0),opened(false),icon(0),bold(false),lines(0),palcolor(0) {}
};

TypeIsMovable(ContentInfo);


class CListBoxMCE : public CListBox
{
	DECLARE_DYNAMIC(CListBoxMCE)

    AutoArray<ContentInfo> *_nfo;
    AutoArray<int> _mapindexes;
    SRef<CImageList> _imgList;
    CFont _bold;
    COLORREF _palette[256];

public:
  CListBoxMCE();
	virtual ~CListBoxMCE();

    void SetContentInfo(AutoArray<ContentInfo> *nfo);

protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

  void Update();
  afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  void OpenCloseItem(UINT index);
  int GetItemIndex(UINT index);
  int GetItemIdent(int index, const RECT *rcitem=0);
  void SetImageList(SRef<CImageList> imgList)
  {
    _imgList=imgList;
  }
//  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnDestroy();
  AutoArray<ContentInfo> *GetContentInfo() {return _nfo;}

  int MapIndex(int index) const {if (index<0) return index; else if (index>=_mapindexes.Size()) return -1;else return _mapindexes[index];}
  bool SetActiveItem(int item);
protected:
  virtual void PreSubclassWindow();
public:
  afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};


