#include "StdAfx.h"
#include ".\propshcomboedit.h"
#include <Windowsx.h>

PropShComboEdit::PropShComboEdit(void)
{
  _list=0;
  _listHeight=150;
}

PropShComboEdit::~PropShComboEdit(void)
{
  DestroyWindow(_list);
}

static LRESULT WINAPI FakeWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch (msg)
  {
  case WM_CANCELMODE:
    PostMessage(hWnd,WM_APP+1,0,0);break;
  case WM_MOVE:
    PostMessage(hWnd,WM_APP+1,0,0);break;
  case WM_KILLFOCUS:
    PostMessage(hWnd,WM_APP+1,wParam,lParam);break;  
  }
  return CallWindowProc((WNDPROC)GetWindowLong(hWnd,GWL_USERDATA),hWnd,msg,wParam,lParam);
}

bool PropShComboEdit::Create(const tchar *name, const tchar *title, PropShItem *parent)
{
  if (__super::Create(name,title,parent))
  {
    _list=CreateWindow("LISTBOX","",WS_POPUP|WS_BORDER|LBS_SORT|LBS_NOINTEGRALHEIGHT|WS_VSCROLL|LBS_USETABSTOPS,0,0,0,0,parent->GetWindowHandle(),NULL,parent->GetInstance(),NULL);
    WNDPROC _subclassAdr=(WNDPROC)GetWindowLong(_hControl,GWL_WNDPROC);
    SetWindowLong(_hControl,GWL_WNDPROC,(LONG)FakeWndProc);
    SetWindowLong(_hControl,GWL_USERDATA,(LONG)_subclassAdr);
    return true;
  }
  else
    return false;
}

void PropShComboEdit::AddItem(const tchar *value, long userData)
{
  int i=ListBox_AddString(_list,value);
  ListBox_SetItemData(_list,i,userData);
}

int PropShComboEdit::FindItem(const tchar *value, int index)
{
  return ListBox_FindStringExact(_list,index,value);
}

bool PropShComboEdit::DeleteItem(int index)
{
  return ListBox_DeleteString(_list,index)!=0;
}

int PropShComboEdit::GetCurItem()
{
  return ListBox_GetCurSel(_list);
}

void PropShComboEdit::SelectItem(int index)
{
  ListBox_SetCurSel(_list, index);
}

long PropShComboEdit::GetItemData(int index)
{
  return ListBox_GetItemData(_list,index);
}

bool PropShComboEdit::SetItemData(int index, long data)
{
  return ListBox_SetItemData(_list, index, data)!=0;
}

void PropShComboEdit::DropDown()
{
  SendMessage(_list,CB_SHOWDROPDOWN,true,0);
}

void PropShComboEdit::CloseUp()
{
  SendMessage(_list,CB_SHOWDROPDOWN,false,0);
}

bool PropShComboEdit::IsDroppedDown()
{
  return SendMessage(_list,CB_GETDROPPEDSTATE,0,0)!=0;
}

static void SelectStringInLB(HWND list,HWND hControl)
{
  int len=GetWindowTextLength(hControl)+1;
  tchar *buffer=(tchar *)alloca(len*sizeof(tchar));
  GetWindowText(hControl,buffer,len);
  ListBox_SelectString(list,-1,buffer);
}

static void SelectStringFromLB(HWND list,HWND hControl)
{
  int cursel=ListBox_GetCurSel(list);
  if (cursel==LB_ERR) return;
  int len=ListBox_GetTextLen(list,cursel);
  tchar *buffer=(tchar *)alloca(len*sizeof(tchar));
  ListBox_GetText(list,cursel,buffer);
  SetWindowText(hControl,buffer);
}

void PropShComboEdit::OnButtonPressed()
{
  HWND parent;
  RECT rc;
  RECT rc2;
  GetWindowRect(_hControl,&rc);
  GetWindowRect(_hButton,&rc2);
  rc.right=rc2.right;
  HMONITOR hmon=MonitorFromRect(&rc,MONITOR_DEFAULTTONEAREST);
  MONITORINFO mi;
  ZeroMemory(&mi,0);
  mi.cbSize=sizeof(mi);
  GetMonitorInfo(hmon,&mi);
  if (rc.bottom+_listHeight>mi.rcWork.bottom)
  {
    rc.bottom=rc.top;
    rc.top=rc.bottom-_listHeight;
    if (rc.top<mi.rcWork.top) rc.top=mi.rcWork.top;
  }
  else
  {
    rc.top=rc.bottom;
    rc.bottom=rc.top+_listHeight;
  }
  parent=GetParent(_list);
/*  ScreenToClient(parent,reinterpret_cast<POINT *>(&rc));
  ScreenToClient(parent,reinterpret_cast<POINT *>(&rc)+1);*/

  MoveWindow(_list,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,FALSE);  
  ShowWindow(_list,SW_SHOWNOACTIVATE);
  SelectStringInLB(_list,_hControl);
  SetFocus(_hControl);
  //SetCapture(_list);
  MSG msg;
  bool continueInModal=true;
  int lastsz=GetWindowTextLength(_hControl);
  while (continueInModal && GetMessage(&msg,0,0,0))
  {
    bool route=true;    
    switch (msg.message)
    {
    case WM_PAINT:
    case WM_LBUTTONUP:
    case WM_MOUSEMOVE: 
      {
        POINT lbpt=msg.pt;
/*        ScreenToClient(_list,&lbpt);*/
        int index=LBItemFromPt(_list,lbpt,FALSE);
        if (index!=-1)
        {
          ListBox_SetCurSel(_list,index);
          if (msg.message==WM_LBUTTONUP)
          {
            SelectStringFromLB(_list,_hControl);
            continueInModal=false;
          }          
        }
        else
        {
            SetFocus(_hControl);
        }
      }
      break;
    case WM_LBUTTONDOWN:
    case WM_RBUTTONDOWN:
    case WM_MBUTTONDOWN:
        if (!PtInRect(&rc,msg.pt))
          continueInModal=false;
        break;
    case WM_MOUSEWHEEL:
      msg.hwnd=_list;
      break;
    case WM_APP+1:
      if ((HWND)msg.wParam!=_list && (HWND)msg.wParam!=_hControl) 
        continueInModal=false;
      break;
    case WM_SYSKEYDOWN:
      if (msg.wParam==VK_DOWN || msg.wParam==VK_UP) 
        {SelectStringFromLB(_list,_hControl);continueInModal=false;}
      break;
    case WM_KEYDOWN:
    case WM_KEYUP:
      {
        if (msg.wParam==VK_DOWN || msg.wParam==VK_UP || msg.wParam==VK_NEXT || msg.wParam==VK_PRIOR)
        {
          msg.hwnd=_list;
          DispatchMessage(&msg);
          route=false;
        }
        else if (msg.wParam==VK_ESCAPE)
        {
          continueInModal=false;
          route=false;
        }
        else if (msg.wParam==VK_RETURN)
        {
          SelectStringFromLB(_list,_hControl);
          continueInModal=false;
          route=false;
        }
        else
        {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
          int sz=GetWindowTextLength(_hControl);
          if (sz!=lastsz)
          {
            if (sz>lastsz)
              SelectStringInLB(_list,_hControl);
            lastsz=sz;
          }
          route=false;
        }
      }
      break;
    }
    if (route)
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }    
  }
      ShowWindow(_list,SW_HIDE);    
}

void PropShComboEdit::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  SendMessage(_list,message,wParam,lParam);
  __super::BroadcastMessage(message,wParam,lParam);
}

bool PropShComboEdit::ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result)
{
  if (msg==WM_COMMAND  && (LOWORD(wParam)==PROPCMD_ACTIONBUTTON2 || LOWORD(wParam)==PROPCMD_ACTIONBUTTON1) && (hWnd==_hButton || hWnd==_hControl) && IsWindowEnabled(_hButton))
	{OnButtonPressed();return true;}
  else 
    return __super::ReflectMessage(hWnd,msg,wParam,lParam,result);
}