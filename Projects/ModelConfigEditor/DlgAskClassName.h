#pragma once
#include "afxwin.h"


// DlgAskClassName dialog

class DlgAskClassName : public CDialog
{
	DECLARE_DYNAMIC(DlgAskClassName)

public:
	DlgAskClassName(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgAskClassName();

// Dialog Data
	enum { IDD = IDD_ASKCLASSNAME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CEdit wClassName;
  CString vClassName;
  afx_msg void OnEnChangeEdit1();
  virtual BOOL OnInitDialog();
  void DialogRules(void);
};
