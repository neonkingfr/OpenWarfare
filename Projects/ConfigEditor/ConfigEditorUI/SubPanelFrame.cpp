// SubPanelFrame.cpp : implementation file
//

#include "stdafx.h"
#include "ConfigEditorUI.h"
#include "SubPanelFrame.h"
#include ".\subpanelframe.h"


// SubPanelFrame

IMPLEMENT_DYNAMIC(SubPanelFrame, CWnd)
SubPanelFrame::SubPanelFrame()
{
  _bgcolor=GetSysColor(COLOR_WINDOW);
  _brdcolor=GetSysColor(COLOR_WINDOWTEXT);
  _shadowcolor=GetSysColor(COLOR_WINDOWTEXT);
  _shadowAlpha=128;
  _shadowSize=4;
}

SubPanelFrame::~SubPanelFrame()
{
}


BEGIN_MESSAGE_MAP(SubPanelFrame, CWnd)
  ON_WM_PAINT()
END_MESSAGE_MAP()



// SubPanelFrame message handlers
class Section
{
  unsigned long *data;
  CRect region;
public:
  Section(unsigned long *data, CRect region):region(region),data(data) {}
  void SetPixel(CPoint pt, unsigned long pixel)
  {
    if (region.PtInRect(pt))
    {
      pt=pt-region.TopLeft();
      data[pt.x+pt.y*(region.right-region.left)]=pixel;
    }
  }

};

#define SETPIXEL(bitmap,linelen,x,y,pixel) bitmap[(x)+(y)*(linelen)]=(pixel)

void SubPanelFrame::OnPaint()
{
  CPaintDC dc(this);

  CRect rc;
  GetClientRect(&rc);
  dc.SelectStockObject(DC_PEN);
  dc.SelectStockObject(DC_BRUSH);
  dc.SetDCPenColor(_brdcolor);
  dc.SetDCBrushColor(_bgcolor);
  CRect brd=rc;
  brd.bottom-=_shadowSize;
  brd.right-=_shadowSize;
  dc.Rectangle(&brd);
/*
  DWORD *shadowDIB;
  CBitmap bitmap;  
  CDC shadowDc;
  shadowDc.CreateCompatibleDC(&dc);

  CRect clip;
  dc.GetClipBox(&clip);

  BITMAPINFO bmi;
  bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
  bmi.bmiHeader.biWidth = clip.Size().cx;
  bmi.bmiHeader.biHeight = -clip.Size().cy;
  bmi.bmiHeader.biPlanes = 1;
  bmi.bmiHeader.biBitCount = 32;         // four 8-bit components
  bmi.bmiHeader.biCompression = BI_RGB;
  bmi.bmiHeader.biSizeImage = clip.Size().cx * clip.Size().cy* 4;

  bitmap.Attach(CreateDIBSection(dc,&bmi,DIB_RGB_COLORS,(void **)&shadowDIB,0,0));
  if (bitmap.m_hObject)
  {  
    CBitmap *oldbmp=shadowDc.SelectObject(&bitmap);

    ZeroMemory(shadowDIB,bmi.bmiHeader.biSizeImage);
    Section sect(shadowDIB,clip);

    for (int i=0;i<_shadowSize;i++)
    {
      float curAlpha=(float)(_shadowSize-i)/(float)_shadowSize;
      float falphaFactor=_shadowAlpha*curAlpha/255.0f;
      DWORD color=(toInt(falphaFactor*255)<<24)|
          (toInt(GetRValue(_shadowcolor)*255*falphaFactor)<<16)|
          (toInt(GetGValue(_shadowcolor)*255*falphaFactor)<<8)|
          (toInt(GetBValue(_shadowcolor)*255*falphaFactor));
      for (int j=0;j<=i;j++)
      {
        sect.SetPixel(CPoint(_shadowSize-i,brd.bottom+j),color);
        sect.SetPixel(CPoint(rc.right-(_shadowSize-i),brd.bottom+j),color);
        sect.SetPixel(CPoint(brd.right+j,_shadowSize-i),color);
      }
      for (int j=_shadowSize-i+1;j<rc.right-(_shadowSize-i);j++)
        sect.SetPixel(CPoint(j,brd.bottom+i),color);
      for (int j=_shadowSize-i+1;j<brd.bottom;j++)
        sect.SetPixel(CPoint(brd.right+i,j),color);
    }
    BLENDFUNCTION fn;
    fn.AlphaFormat=AC_SRC_ALPHA;
    fn.BlendFlags=0;
    fn.SourceConstantAlpha=255;
    fn.BlendOp=AC_SRC_OVER;
    AlphaBlend(dc,clip.left,clip.top,clip.right-clip.left,clip.bottom-clip.top,shadowDc,0,0,clip.right-clip.left,clip.bottom-clip.top,fn);
    shadowDc.SelectObject(oldbmp);
  }
*/
}
