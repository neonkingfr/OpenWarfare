#pragma once


// Simple2DGraph

class ISimple2DGraphNotify
{
public:
  virtual void GraphChanged()=0;
  virtual void SelectionChanged() {}
};

class Simple2DGraph : public CWnd
{

  ISimple2DGraphNotify *_notify;
  
public:
  struct PointXY
  {
    float x, y;
    bool selected;
    ClassIsSimpleZeroed(PointXY);
    PointXY() {}
    PointXY(float x, float y):x(x),y(y),selected(false) {}
    template<class Archive>
    void Serialize(Archive &arch)
    {
      arch("_x",x);
      arch("_y",y);
    }
  };

  struct Transform
  {
    float a11,a12,a21,a22,a31,a32;
    Transform(float xMin, float xMax, float yMin, float yMax, int canvasWidth, int canvasHeight,int offsetX, int offsetY,bool swapAxis=false);
    Transform(PointXY r1,PointXY r2,PointXY r3):
      a11(r1.x),a12(r1.y),
      a21(r2.x),a22(r2.y),
      a31(r3.x),a32(r3.y) {}
    Transform():a11(1),a22(1),a12(0),a21(0),a31(0),a32(0) {}
    CPoint operator()(float x, float y) const;
    CPoint operator()(const PointXY &point) const;
    PointXY operator()(const CPoint &point) const;
    PointXY operator()(int row) const {return row==0?PointXY(a11,a12):(row==1?PointXY(a21,a22):PointXY(31,32));}    
    PointXY Trns(const PointXY &point);
    ///
    /**
     * @note it doesn't support rotation and skew 
     */
    Transform Inverted() const;
    Transform operator*(const Transform &other) const
    {

      return Transform(PointXY(a11*other.a11+a12*other.a21,a11*other.a12+a12*other.a22),
                       PointXY(a21*other.a11+a22*other.a21,a21*other.a12+a22*other.a22),
                       PointXY(a31*other.a11+a32*other.a21+other.a31,a31*other.a12+a32*other.a22+other.a32));
    }
  };

protected:

  AutoArray<PointXY,MemAllocLocal<PointXY,32> > _points;  
  float _xMin,_yMin,_xMax,_yMax;
  bool _swappedAxis;

  Transform _zoomTransform;

  CString _xTitle,_yTitle;

  CBitmap _doubleBuff;
  CPen _lineColor;
  CPen _pointColor;
  CBrush _pointSelFill;
  CPen _pointSelColor;
  int _pointSize;
  CRect _margin;
  CPen _axisColor;
  COLORREF _fontColor;
  HFONT _font;
  CFont _rotatedFont;
  CPen _selectionRect;

  CRect _dragRect;
  bool _noContext;
  bool _moveAny;  
  bool _deselect;
  int _dragPt;

  bool _constStep;

public:
	Simple2DGraph(ISimple2DGraphNotify *notify);
	virtual ~Simple2DGraph();

  void SetRange(float xMin, float yMin, float xMax, float yMax)
  {
    _xMin=xMin;
    _yMin=yMin;
    _xMax=xMax;
    _yMax=yMax;
  }

  void LoadPointsXY(const Array<float> &_pointsInterleaved);
  void LoadPointsY(const Array<float> &_points);
  void SetSwappedAxis(bool swap) {_swappedAxis=swap;}
  void ReplacePointsXY(const Array<float> &pointsInterleaved);


  void SetTitleX(const CString &xTitle) {_xTitle=xTitle;}
  void SetTitleY(const CString &yTitle) {_yTitle=yTitle;}

  int GetPointCount() const {return _points.Size();}
  void SavePoints(Array<float> &_pointsInterleaved, bool sorted) const;

  BOOL Create(DWORD ex_styles, DWORD styles, CWnd *parent, int nID);

  void Redraw(bool force=false);

  void SetMargin(CRect margin) {_margin=margin;}

  void Clear() {_points.Clear();_notify->GraphChanged();}  
  void DeleteSelected();  

  void SetSelectionAll(bool selectAll=true);
  void SelectPoint(int point, bool sel=true);
  void SelectRectangle(const CRect &rc, bool sel=true);
  bool IsPointSelected(int point) const {return _points[point].selected;}  
  bool AnySelected() const  {for (int i=0;i<_points.Size();i++) if (_points[i].selected) return true;return false;}

  void SetConstStep(bool cs) {_constStep=cs;}
  bool GetConstStep() const {return _constStep;}

  template<class Archive>
  void Serialize(Archive &arch)
  {
    arch("points",_points);
  }

  template<class Archive>
  void SerializeSel(Archive &arch)
  {
    AutoArray<PointXY,MemAllocLocal<PointXY,100> > save;
    if (+arch)
    {
    for (int i=0;i<_points.Size();i++) if (_points[i].selected)
      save.Add(_points[i]);
    }
    arch("points",save);
    if (-arch)
    {
      SetSelectionAll(false);
      for (int i=0;i<save.Size();i++) save[i].selected=true;
      _points.Merge(save);
    }
  }

protected:
	DECLARE_MESSAGE_MAP()

protected:

  void CheckPointRanges();
  Transform GetCurrentTransform() const;
  
  afx_msg void OnPaint();
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg LRESULT OnSetFont(WPARAM wParam, LPARAM lParam);

  int FindPointFromXY(CPoint &pt);
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);

  afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
  afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);

  afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnRButtonUp(UINT nFlags, CPoint point);


  afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
  afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
  afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
  afx_msg void OnSwapAxis();

public:
  static RString ReadGraphValues(const Simple2DGraph &_graph);
  static RString ReadGraphValuesPoly(const Simple2DGraph &_graph);


};

