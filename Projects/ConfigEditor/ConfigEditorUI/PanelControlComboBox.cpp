// PanelControlComboBox.cpp : implementation file
//

#include "stdafx.h"
//#include "LindaStudio.h"
#include "PanelControlComboBox.h"
#include "PanelContainer.h"


namespace LindaStudio
{


  PanelControlComboBox::PanelControlComboBox(IPanelControlComboBoxNotify *notify,DWORD style, DocumentBase<IPanelControlMsgs> *document, const char *labelText):
PanelControlWithLabel(document,labelText),_notify(notify)
{
  Create(style|WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),CWnd::FromHandle(GetDocument()->GetWindow()),0);
}

PanelControlComboBox::~PanelControlComboBox()
{
  DestroyWindow();
}


BEGIN_MESSAGE_MAP(PanelControlComboBox, CComboBox)
  ON_CONTROL_REFLECT(CBN_EDITCHANGE,OnEditChange)
  ON_CONTROL_REFLECT(CBN_SELCHANGE,OnSelChange)
  ON_CONTROL_REFLECT(CBN_SETFOCUS,OnControlSetFocus)
  ON_CONTROL_REFLECT(CBN_KILLFOCUS,OnControlKillFocus)
  ON_WM_KEYDOWN()
END_MESSAGE_MAP()

bool PanelControlComboBox::GetPreferredSize(int &xs, int &ys)
{
  int yy=CHeight,xx=xs; 
  PanelControlWithLabel::GetPreferredSize(xx,yy);
  ys=yy;
  return true;
}

bool PanelControlComboBox::SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
{
/*  SetDroppedWidth(xs);  */
  int cbheight=ys; 
  ComboBox_SetMinVisible(*this,30);
  cbheight=200;
  int xx=xs,yy=ys;
  PanelControlWithLabel::GetPreferredSize(xx,yy);
  PanelControlWithLabel::SetNewLocation(mw,x,y,xx,ys);
  ShowWindow(SW_HIDE);
  mw.SetNewPosition(*this,x+xx,y,xs-xx,cbheight);
  ShowWindow(SW_SHOW);
  SetEditSel(0,0);  
  return true;
}

bool PanelControlComboBox::SetEnable(IPanelControlMsgs *what, bool newState)
{
  if (PanelControlWithLabel::SetEnable(what,newState))
  {
    EnableWindow(newState);
    return true;
  }
  else
    return false;
}

bool PanelControlComboBox::SetVisible(IPanelControlMsgs *what, bool newState)
{
  if (PanelControlWithLabel::SetVisible(what,newState))
  {
    ShowWindow(newState?SW_SHOW:SW_HIDE);
    return true;
  }
  else
    return false;
}

bool PanelControlComboBox::GoNextControl(IPanelControlMsgs *from, bool previous)
{
  SetFocus();
  return true;
}

HWND PanelControlComboBox::GetWindow() const
{
  return *this;
}

bool PanelControlComboBox::SetFont(HFONT font)
{
  PanelControlWithLabel::SetFont(font);
  CComboBox::SetFont(CFont::FromHandle(font));
  return true;
}

void PanelControlComboBox::OnControlKillFocus()
{
  if (_modify)
  {
    _modify=false;
    int stl=GetWindowLong(*this,GWL_STYLE);
    CString txt;
    int curSel=GetCurSel();
    if ((stl & CBS_DROPDOWNLIST) == CBS_DROPDOWNLIST)
    {
        if (curSel==CB_ERR) return;
        GetLBText(curSel,txt);
    }
    else
    {
      GetWindowText(txt);
    }
    if (_notify) _notify->DataChanged(curSel,txt);
  }
}

void PanelControlComboBox::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  if (nChar==VK_UP)
  {
    GetDocument()->GoNextControl(this,true);
    return;
  }
  if (nChar==VK_DOWN)
  {
    GetDocument()->GoNextControl(this,false);
  }
  return CComboBox::OnKeyDown(nChar,nRepCnt,nFlags);
}


BOOL PanelControlComboBox::PreTranslateMessage(MSG* pMsg)
{
  if (pMsg->message==WM_KEYDOWN && GetDroppedState()==FALSE )
  {
    if (pMsg->wParam==VK_UP)
    {
      GetDocument()->GoNextControl(this,true);
      return TRUE;
    }
    if (pMsg->wParam==VK_DOWN)
    {
      GetDocument()->GoNextControl(this,false);
      return TRUE;
    }
  }
  return CComboBox::PreTranslateMessage(pMsg);
}


}