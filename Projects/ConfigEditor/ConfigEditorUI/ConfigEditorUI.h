// ConfigEditorUI.h : main header file for the ConfigEditorUI application
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols


// Application:
// See ConfigEditorUI.cpp for the implementation of this class
//

class Application : public CWinApp, public IFileDlgHistory
{
  int _frameCount;
  FindArray<RStringI> _folderHistory;
public:
	Application();

public:

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
protected:
	HMENU m_hMDIMenu;
	HACCEL m_hMDIAccel;

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()

public:
  void AddFrame() {_frameCount++;}
  void RemoveFrame();  
  virtual int ExitInstance();


  virtual void StoreHistoryFolder(const char *folderName);
  virtual const char *GetNextHistoryFolder(int index);
  virtual HBITMAP GetHistoryBitmap();
  virtual HBITMAP GetHistoryMenuBitmap();
};

extern Application theApp;