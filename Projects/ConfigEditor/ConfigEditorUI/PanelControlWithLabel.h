#pragma once

#include "PanelControlBase.h"
#include "TranspStatic.h"


namespace LindaStudio
{
  class IPanelControlWithLabelContainerInfo
  {
  public:
    DECLARE_IFCUIDA('P','C','W','L','C','I');

    virtual float GetLabelSize() const=0;
    virtual void SetLabelSize(float f) =0;
  };


  class PanelControlWithLabel : public PanelControlBase
  {
  protected:
    static float labelPos;

    CTranspStatic _label;
    CSize _lbminsize;

    void RecalculateLabelSize();
  public:
    PanelControlWithLabel(DocumentBase<IPanelControlMsgs> *document, const char *labelText);
    ~PanelControlWithLabel() {_label.DestroyWindow();}

    virtual bool GetPreferredSize(int &xs, int &ys);
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
    virtual HWND GetWindow() const;
    virtual bool SetFont(HFONT font);

    static CSize GetTextExtent(CWnd *wnd, const char *text, bool prefix=true, int mlinewith=-1);
    virtual IPanelControlMsgs *WhoseWindow(HWND window)
    {
      if (window==_label) return this;
      else return 0;
    }


  };

}