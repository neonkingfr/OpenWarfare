#pragma once


// CTranspButton

class CTranspButton : public CButton
{
	DECLARE_DYNAMIC(CTranspButton)

public:
	CTranspButton();
	virtual ~CTranspButton();

protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg HBRUSH CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/);
};


