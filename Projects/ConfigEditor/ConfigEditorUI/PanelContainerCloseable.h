#pragma once
#include "panelcontainer.h"
#include "PanelControlBase.h"
#include "ButtonWithNotify.h"

namespace LindaStudio
{
  class IPanelContainerCloseable
  {
  public:
    virtual void OpenCloseGroup(bool close)=0;
  };

  class PanelContainerCloseable :    public PanelControlBase, public IButtonNotify
  {
    CButtonWithNotify _titleButton;
    PanelContainer _container;    
    CButton _frameWithName;
    IPanelContainerCloseable *_notify;
    

    class ContainerServer: public DocumentBase<IPanelControlMsgs>
    {
      PanelContainerCloseable *outer;
    public:
      virtual bool GetPreferredSize(int &xs, int &ys)
      {
        return outer->InsideGetPreferredSize(xs,ys);
      }
      virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
      {
        return outer->InsideSetNewLocation(mw,x,y,xs,ys);
      }
      virtual bool SetEnable(IPanelControlMsgs *what, bool newState)
      {
        return outer->InsideSetEnable(what,newState);
      }
      virtual bool SetVisible(IPanelControlMsgs *what, bool newState)
      {
        return outer->InsideSetVisible(what,newState);
      }
      virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false)  
      {
        return outer->InsideGoNextControl(from,previous);
      }
      virtual HWND GetWindow() const
      {
        return outer->InsideGetWindow();
      }
      virtual bool SetFont(HFONT font)
      {
        return outer->InsideSetFont(font);
      }
      virtual IPanelControlMsgs *WhoseWindow(HWND window)
      {
        return outer->InsideWhoseWindow(window);
      }
      virtual void RecalcLayout()
      {
        outer->InsideRecalcLayout();
      }

      ContainerServer(PanelContainerCloseable *outer):outer(outer) {}
    };
    ContainerServer _server;
  public:
    PanelContainerCloseable(IPanelContainerCloseable *_notify, DocumentBase<IPanelControlMsgs> *document, const char *title, bool closed);
    ~PanelContainerCloseable(void);

    virtual bool GetPreferredSize(int &xs, int &ys);
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
    virtual HWND GetWindow() const;
    virtual bool SetFont(HFONT);
    virtual IPanelControlMsgs *WhoseWindow(HWND window);

    virtual void OnDocumentDropped() {delete this;}

    bool InsideGetPreferredSize(int &xs, int &ys);
    bool InsideSetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    bool InsideSetEnable(IPanelControlMsgs *what, bool newState);
    bool InsideSetVisible(IPanelControlMsgs *what, bool newState);
    bool InsideGoNextControl(IPanelControlMsgs *from, bool previous=false);  
    HWND InsideGetWindow() const;
    bool InsideSetFont(HFONT);
    void InsideRecalcLayout();
    IPanelControlMsgs *InsideWhoseWindow(HWND window);

    virtual void OnButtonPressed();
    virtual void ArrowPressed(int code);

    PanelContainer *GetContainer() {return &_container;}
  };
}
