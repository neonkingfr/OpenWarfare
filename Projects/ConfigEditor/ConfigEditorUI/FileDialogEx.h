#pragma once


// CFileDialogEx
class IFileDlgHistory
{
public:
  ///Stores history folder
  virtual void StoreHistoryFolder(const char *folderName)=0;
  ///Returns next history folder
  /**
   * To enumeration, set @b index to zero and repeat this function, until NULL is not returned 
   * @param index index of history folder
   * @retval string folder name at position
   * @retval 0 if there are no more folder
   * @note Returned pointer can be invalidated by next GetNextHistoryFolder call. Do not
   * store the pointer.
   */
  virtual const char *GetNextHistoryFolder(int index)=0;

  virtual HBITMAP GetHistoryBitmap()=0;
  virtual HBITMAP GetHistoryMenuBitmap()=0;
};


class CFileDialogEx : public CFileDialog
{
	DECLARE_DYNAMIC(CFileDialogEx)

  static IFileDlgHistory *historyInterface;

  friend class SubclassDlg;
  class SubclassDlg: public CWnd
  {
    CFileDialogEx *outer;
  public:
    SubclassDlg(CFileDialogEx *outer):outer(outer) {}
    virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
  };

  SubclassDlg sdlg;
  HWND _toolbarhwnd;
  int _histbutt;

public:
	CFileDialogEx(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL);
	virtual ~CFileDialogEx();


  static void SetHistoryInterface(IFileDlgHistory *history)  { historyInterface=history; }

  static IFileDlgHistory *GetHistoryInterface() {return historyInterface;}

protected:
	DECLARE_MESSAGE_MAP()
  virtual void OnInitDone();
  virtual void OnHistoryCommand();
  virtual BOOL OnFileNameOK();
  void GotoPath(LPCTSTR path);
};


