// MainFrm.cpp : implementation of the MainFrame class
//

#include "stdafx.h"
#include "ConfigEditorUI.h"

#include "MainFrm.h"
#include ".\mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// MainFrame

IMPLEMENT_DYNAMIC(MainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(MainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_CLOSE, OnFileClose)
	ON_WM_CLOSE()
  ON_COMMAND(ID_FILE_NEW, OnFileNew)
  ON_COMMAND(ID_VIEW_NEWWINDOW, OnViewNewwindow)
  ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
  ON_COMMAND(ID_FILE_SAVE, OnFileSave)
  ON_COMMAND(ID_FILE_SAVEAS, OnFileSaveas)
  ON_COMMAND(ID_VIEW_REFRESH, OnViewRefresh)
  ON_COMMAND(ID_VIEW_RELOADDEPENDENCIES, OnViewReloaddependencies)
  ON_COMMAND(ID_APP_EXIT, OnAppExit)
  ON_WM_SIZE()
  ON_MESSAGE(WM_ENTERSIZEMOVE,OnEnterSizeMove)
  ON_MESSAGE(WM_EXITSIZEMOVE,OnExitSizeMove)
END_MESSAGE_MAP()


// MainFrame construction/destruction

MainFrame::MainFrame(const UniRef<ConfigEditorWithUndo> &document):_document(document),_panelView(_panel)
{
  theApp.AddFrame();
  _sizing=false;
}

MainFrame::~MainFrame()
{
  theApp.RemoveFrame();
}


int MainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
  _panel.Create(WS_VISIBLE|WS_CHILD,this,AFX_IDW_PANE_FIRST);
  _panelView.AttachModel(_document);
  AttachModel(_document);
  _panelView.UpdateMe(0);
  UpdateMe(0);


	return 0;
}

BOOL MainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
  cs.lpszClass=AfxRegisterWndClass(0,LoadCursor(0,IDC_ARROW),(HBRUSH)(COLOR_BTNFACE+1),0);
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs  
  cs.dwExStyle&=~WS_EX_CLIENTEDGE;

	return TRUE;
}


// MainFrame diagnostics

#ifdef _DEBUG
void MainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void MainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// MainFrame message handlers

BOOL MainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext) 
{
	// base class does the real work

	if (!CFrameWnd::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
	{
		return FALSE;
	}
	return TRUE;
}

void MainFrame::OnFileClose()
{
   DestroyWindow();
}

void MainFrame::OnClose() 
{
	CFrameWnd::OnClose();
}

void MainFrame::OnFileNew()
{
  MainFrame *frame=new MainFrame(new ConfigEditorWithUndo);
  frame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL, NULL);
  frame->ShowWindow(SW_SHOW);
  frame->UpdateWindow();  
}

void MainFrame::OnViewNewwindow()
{
  MainFrame *frame=new MainFrame(_document);
  frame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL, NULL);
  frame->ShowWindow(SW_SHOW);
  frame->UpdateWindow();  
  
}

void MainFrame::OnFileOpen()
{
  CFileDialogEx fdlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST,CString(MAKEINTRESOURCE(IDS_OPENCONFIGFILTER)));
  CString title(MAKEINTRESOURCE(IDS_OPENCONFIGTITLE));
  fdlg.m_ofn.lpstrTitle=title;
  if (fdlg.DoModal()==IDOK)
  {
    bool res=_document->Load(RStringI(fdlg.GetPathName()));
    if (!res)
      AfxMessageBox(IDS_CONFIGLOADERROR,MB_OK|MB_ICONEXCLAMATION);
  }
}

void MainFrame::OnFileSave()
{
  // TODO: Add your command handler code here
}

void MainFrame::OnFileSaveas()
{
  // TODO: Add your command handler code here
}

void MainFrame::OnViewRefresh()
{
  _panelView.UpdateMe(0);
  UpdateMe(0);
}

void MainFrame::OnViewReloaddependencies()
{
  // TODO: Add your command handler code here
}

void MainFrame::OnAppExit()
{
  SendMessage(WM_CLOSE);
}

bool MainFrame::UpdateMe(ConfigMVC::CurModel *model)
{
  GetModel()->UpdateMe(this);
  return true;
}

void MainFrame::SetConfigName(const RStringI &name)
{
  SetTitle(name);
}
void MainFrame::OnSize(UINT nType, int cx, int cy)
{
  if (_sizing) return;

  __super::OnSize(nType, cx, cy);
  _panel.ReLayout(); 
}

LRESULT MainFrame::OnExitSizeMove(WPARAM wParam, LPARAM lParam)
{
//  SetWindowLong(*this,GWL_EXSTYLE,GetWindowLong(*this,GWL_EXSTYLE)& ~WS_EX_LAYERED);
  _sizing=false;
  CRect rc;
  GetClientRect(&rc);
  OnSize(0,rc.right,rc.bottom);
  return 0;
}

LRESULT MainFrame::OnEnterSizeMove(WPARAM wParam, LPARAM lParam) 
{
/*  SetWindowLong(*this,GWL_EXSTYLE,GetWindowLong(*this,GWL_EXSTYLE)|WS_EX_LAYERED);
  SetLayeredWindowAttributes(0,128,LWA_ALPHA);*/
 _sizing=true;return 0;
}