#include "StdAfx.h"
#include ".\panelcontrolsinglebutton.h"
#include "PanelContainer.h"

namespace LindaStudio
{


  PanelControlSingleButton::PanelControlSingleButton(IPanelControlSingleButtonNotify *notify, 
                                                      const char *text, 
                                                      DocumentBase<IPanelControlMsgs> *container, 
                                                      const char *labelText):
  PanelControlWithLabel(container,labelText),_notify(notify),_button(this)
  {
    _button.Create(text,WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_CENTER|BS_VCENTER,CRect(0,0,0,0),
      CWnd::FromHandle(GetDocument()->GetWindow()),0);
    
    CalcButtonSize();
  }

  PanelControlSingleButton::~PanelControlSingleButton(void)
  {
    _button.DestroyWindow();
  }

  void PanelControlSingleButton::OnDocumentDropped()
  {
    delete this;
  }


  bool PanelControlSingleButton::GetPreferredSize(int &xs, int &ys)
  {
    int xx=xs,yy=_buttSize.cy;
    PanelControlWithLabel::GetPreferredSize(xx,yy);
    xs=xx+_buttSize.cx;
    ys=yy;
    return true;
  }

  bool PanelControlSingleButton::SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
  {
    int xx=xs,yy=ys;
    PanelControlWithLabel::GetPreferredSize(xx,yy);
    PanelControlWithLabel::SetNewLocation(mw,x,y,xx,yy);   
    mw.SetNewPosition(_button,x+xx,y,_buttSize.cx,_buttSize.cy);
    return true;    
  }

  bool PanelControlSingleButton::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    bool res;
    if ((res=PanelControlWithLabel::SetEnable(what,newState))==true)
      _button.EnableWindow(res);
    return res;
  }

  bool PanelControlSingleButton::SetVisible(IPanelControlMsgs *what, bool newState)
  {
    bool res;
    if ((res=PanelControlWithLabel::SetVisible(what,newState))==true)
      _button.ShowWindow(res?SW_SHOW:SW_HIDE);
    return res;
  }

  bool PanelControlSingleButton::GoNextControl(IPanelControlMsgs *from, bool previous)
  {
    _button.SetFocus();
    return true;
  }

  HWND PanelControlSingleButton::GetWindow() const
  {
    return _button;
  }

  bool PanelControlSingleButton::SetFont(HFONT font)
  {
    PanelControlWithLabel::SetFont(font);
    _button.SetFont(CFont::FromHandle(font));
    CalcButtonSize();
    return true;
  }

  void PanelControlSingleButton::OnButtonPressed()
  {
    _notify->ButtonClicked();
  }
  void PanelControlSingleButton::ArrowPressed(int code)
  {
    if (code==VK_DOWN) GetDocument()->GoNextControl(this,false);
    if (code==VK_UP) GetDocument()->GoNextControl(this,true);
  }

  void PanelControlSingleButton::CalcButtonSize()
  {
    CString t;
    _button.GetWindowText(t);
    CSize sz=GetTextExtent(&_button,t);
    sz.cx+=4*GetSystemMetrics(SM_CXEDGE);
    sz.cy+=4*GetSystemMetrics(SM_CYEDGE); 
    _buttSize=sz;
  }

}