#pragma once

#include "PanelControlSingleButton.h"
#include "Simple2DGraph.h"

namespace LindaStudio
{

  class IPanelControlGraph: public IPanelControlSingleButtonNotify
  {
  public:
    virtual void GraphChanged(RString data)=0;
  };

  class PanelControlGraph:public PanelControlSingleButton, public ISimple2DGraphNotify
  {
  protected:
    Simple2DGraph _graph;
  public:
    PanelControlGraph(IPanelControlGraph *notify, 
                      const char *text, 
                      DocumentBase<IPanelControlMsgs> *container, 
                      const char *labelText);
    ~PanelControlGraph(void);

  virtual bool GetPreferredSize(int &xs, int &ys);
  virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
  virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
  virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
  virtual bool SetFont(HFONT font);

  void SetUpGraph(float minX, float maxX, float minY, float maxY, const char *titleX, const char *titleY, bool swapAxis)
  {
    _graph.SetRange(minX,minY,maxX,maxY);
    _graph.SetTitleX(titleX);
    _graph.SetTitleY(titleY);
    _graph.SetSwappedAxis(swapAxis);
    _graph.Redraw();
  }

  virtual IPanelControlMsgs *WhoseWindow(HWND window)
  {
    if (window==_graph) return this;
    else return PanelControlSingleButton::WhoseWindow(window);
  }

  virtual void UpdateGraph(const RString &from);

  virtual void GraphChanged();

  };

  class PanelControlGraphPoly: public PanelControlGraph
  {
  public:
    PanelControlGraphPoly(IPanelControlGraph *notify, 
                      const char *text, 
                      DocumentBase<IPanelControlMsgs> *container, 
                      const char *labelText):
    PanelControlGraph(notify,text,container,labelText)
    {
      _graph.SetConstStep(true);
    }

    void UpdateGraph(const RString &from);

    virtual void GraphChanged();

  };
}
