#include "StdAfx.h"
#include ".\slavecontainergrid.h"

SlaveContainerGrid::SlaveContainerGrid(PanelSubcontainer *outer,int avgItemWidth):_avgItemWidth(avgItemWidth),SlaveContainer(outer)
{
  _lastMaxCol=1;
}

SlaveContainerGrid::~SlaveContainerGrid(void)
{
}

void SlaveContainerGrid::GetPreferredSizeClient(int &width, int &height, int xs, int ys)
{
  int maxCols=(xs+_avgItemWidth/2)/_avgItemWidth; 
  bool recalc=true;
  if (maxCols<1) maxCols=1;
  int ypos=0;
  int cnt=GetCountRegistredViews();
  for (int i=0;i<10;i++) if (cnt%(maxCols+i)==0) 
  {
    maxCols+=i;
    break;
  }
  while (maxCols>0 && recalc)
  {
    ypos=0;
    int r=0;
    int c=0;
    recalc=false;
    int height=0;
    for(EachView it(this);it;)
    {
      int nextcol=dynamic_cast<PanelSubcontainer *>(it.GetItem())!=0?maxCols:c+1;
      int left=c*xs/maxCols;
      int right=nextcol*xs/maxCols;
      int xxs=right-left;
      int yys=ys;
      it->GetPreferredSize(xxs,yys);
      if (xxs>(right-left)) recalc=true;
      c=nextcol;
      if (yys>height) height=yys;
      if (c>=maxCols)
      {
        c=0;
        r++;
        ypos+=height;
        height=0;
      }
    }
    ypos+=height;
    if (recalc) maxCols--;
  }
  _lastMaxCol=maxCols;
  width=xs;
  height=ypos;
}


void SlaveContainerGrid::RepositionItems(int xpos, int ypos, CRect &rc)
{
  ScrollWinManipulator wlist;
  int maxCols=_lastMaxCol;
  int r=0;
  int c=0;
  int xs=rc.right;
  int ys=rc.bottom;
  int height=0;
  for(EachView it(this);it;)
    {
    int nextcol=dynamic_cast<PanelSubcontainer *>(it.GetItem())!=0?maxCols:c+1;
    int left=c*xs/maxCols;
    int right=nextcol*xs/maxCols;
    int xxs=right-left;
    int yys=ys;
    it->GetPreferredSize(xxs,yys);
    xxs=right-left;
    it->SetNewLocation(wlist,xpos+left,ypos,xxs,yys);
    c=nextcol;
    if (yys>height) height=yys;
    if (c>=maxCols)
    {
      c=0;
      r++;
      ypos+=height;
      height=0;
    }
  }
}