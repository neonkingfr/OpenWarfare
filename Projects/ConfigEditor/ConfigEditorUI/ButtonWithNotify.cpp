// ButtonWithNotify.cpp : implementation file
//

#include "stdafx.h"
//#include "LindaStudio.h"
#include "ButtonWithNotify.h"


// CButtonWithNotify

CButtonWithNotify::CButtonWithNotify(IButtonNotify *ntf):notify(ntf)
{

}

CButtonWithNotify::~CButtonWithNotify()
{
}


BEGIN_MESSAGE_MAP(CButtonWithNotify, CTranspButton)
  ON_CONTROL_REFLECT(BN_CLICKED,OnPress)
  ON_WM_KEYDOWN()

END_MESSAGE_MAP()


void CButtonWithNotify::OnPress()
{
  notify->OnButtonPressed();
}

void CButtonWithNotify::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  switch(nChar)
  {
  case VK_UP:
  case VK_DOWN:
  case VK_RIGHT:
  case VK_LEFT: notify->ArrowPressed(nChar);break;
  default: __super::OnKeyDown(nChar,nRepCnt,nFlags);
  }
}
// CButtonWithNotify message handlers

