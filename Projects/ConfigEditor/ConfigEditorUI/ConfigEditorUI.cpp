// ConfigEditorUI.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ConfigEditorUI.h"
#include "ConfigEditorWithUndo.h"
#include "MainFrm.h"
#include "SubPanelFrame.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Application

BEGIN_MESSAGE_MAP(Application, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
END_MESSAGE_MAP()


// Application construction

Application::Application()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only Application object

Application theApp;

// Application initialization

#include <es/types/UniRef.hpp>
#include ".\configeditorui.h"

typedef UniRef<int,UniRefDebugCompressed,UniRefDefDelete,UniRefNoThreading> UniRefT;



BOOL Application::InitInstance()
{
	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

  _frameCount=0;
  m_pMainWnd=new CWnd();
  CFileDialogEx::SetHistoryInterface(this);
  
  ConfigEditorWithUndo *document=new ConfigEditorWithUndo;
  // To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object
  	MainFrame* pFrame = new MainFrame(document);
	if (!pFrame)
		return FALSE;
	// create main MDI frame window
	if (!pFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
  // try to load shared MDI menus and accelerator table
	//TODO: add additional member variables and load calls for
	//	additional menu types your application may need
	HINSTANCE hInst = AfxGetResourceHandle();
	m_hMDIMenu  = ::LoadMenu(hInst, MAKEINTRESOURCE(IDR_ConfigEditorUITYPE));
	m_hMDIAccel = ::LoadAccelerators(hInst, MAKEINTRESOURCE(IDR_ConfigEditorUITYPE));
	// The one and only window has been initialized, so show and update it
	pFrame->ShowWindow(SW_SHOW);
	pFrame->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	return TRUE;
}


// Application message handlers



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void Application::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

void Application::RemoveFrame()
{
  _frameCount--;
  if (_frameCount==0) PostQuitMessage(0);
}

int Application::ExitInstance()
{
  delete m_pMainWnd;

  return CWinApp::ExitInstance();
}


void Application::StoreHistoryFolder(const char *folderName)
{
  RStringI f=folderName;
  _folderHistory.Delete(f);
  _folderHistory.Add(f);
}
const char *Application::GetNextHistoryFolder(int index)
{
  int i=_folderHistory.Size()-index-1;
  if (i<0) return 0;
  return _folderHistory[i];
}
HBITMAP Application::GetHistoryBitmap()
{
  return LoadBitmap(m_hInstance,MAKEINTRESOURCE(IDB_HISTORYBITMAP));
}
HBITMAP Application::GetHistoryMenuBitmap()
{
  return LoadBitmap(m_hInstance,MAKEINTRESOURCE(IDB_HISTORYMENUBITMAP));
}
