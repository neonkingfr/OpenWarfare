#pragma once

#include "PanelControlWithLabel.h"

namespace LindaStudio
{

  
  class IPanelControlEditNotify
  {
  public:
    virtual void TextChanged(const char *text)=0;
    virtual void DeleteControl()=0;
  };


  class PanelControlEdit : public CEdit, public PanelControlWithLabel
  {
  protected:
    IPanelControlEditNotify *notify;

    CSize _lastAsk;
    CSize _lastReply;
    bool _modify;
  public:
    PanelControlEdit(DocumentBase<IPanelControlMsgs> *document, const char *labelText, IPanelControlEditNotify *ntf);
    virtual ~PanelControlEdit();

    CString GetText() const;

  protected:
    DECLARE_MESSAGE_MAP()

    afx_msg void OnChangedText();
    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);

    afx_msg void OnSetFocus();
    afx_msg void OnKillFocus();

    virtual void TextChanged(const char *text) {if (notify) return notify->TextChanged(text);}    

    virtual bool GetPreferredSize(int &xs, int &ys);
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
    virtual HWND GetWindow() const;
    virtual bool SetFont(HFONT font);
    virtual IPanelControlMsgs *WhoseWindow(HWND window)
    {
      if (window==*this) return this;
      else return PanelControlWithLabel::WhoseWindow(window);      
    }
    void OnDocumentDropped();
 };

  class PanelControlMultilineEdit: public PanelControlEdit
  {
    int _lines;
    bool _wrap;
  public:
    PanelControlMultilineEdit(DocumentBase<IPanelControlMsgs> *document, const char *labelText, IPanelControlEditNotify *ntf, int lines, bool wrap);
    bool GetPreferredSize(int &xs, int &ys);
    void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
    DECLARE_MESSAGE_MAP()
 };


}