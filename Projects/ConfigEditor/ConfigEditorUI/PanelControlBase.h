#pragma once

#include "DocumentBase.h"
#include "IPanelControlMsgs.h"

namespace LindaStudio
{
  class PanelControlBase : public ViewBase<IPanelControlMsgs>
  {
    COLORREF _bgcolor;
  public:

    PanelControlBase(void): _bgcolor(0xFF000000) {}
    virtual void OnDocumentDropped()
    {
      delete this;
    }


    virtual COLORREF GetControlBgrColor() const {return _bgcolor;}
    void SetControlBgrColor(COLORREF color) {_bgcolor=color;}
  };
}
