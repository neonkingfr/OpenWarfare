// PanelControlPair.cpp : implementation file
//

#include "stdafx.h"
//#include "LindaStudio.h"
#include "PanelControlPair.h"
#include "PanelContainer.h"


#define IDC_EDITVAL 1000
#define IDC_EDITRAND 1001
#define IDC_SLIDERVAL 1002
#define IDC_SLIDERREAND 1003


namespace LindaStudio

{

  PanelControlPair::PanelControlPair(IPanelControlPairNotify *notify, const char *titleVal, const char *titleRand, float min, float max,DocumentBase<IPanelControlMsgs> *document, const char *labelText)
    :PanelControlWithLabel(document,labelText),_notify(notify)
  {
    _factor=1;
    float diff=max-min;
    if (diff>0)
    {    
      while (diff/_factor<1000) _factor/=10;
      while (diff/_factor>100000) _factor*=10;
    } 

    CWnd *wnd=CWnd::FromHandle(GetDocument()->GetWindow());
    Create(AfxRegisterWndClass(0,::LoadCursor(0,IDC_ARROW),(HBRUSH)(GetStockObject(HOLLOW_BRUSH))),"",WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),wnd,0);

    _titleval.Create(titleVal,WS_VISIBLE|WS_CHILD|SS_LEFT|SS_CENTERIMAGE,CRect(0,0,0,0),this);
    _titlerand.Create(titleRand,WS_VISIBLE|WS_CHILD|SS_LEFT|SS_CENTERIMAGE,CRect(0,0,0,0),this);
    _wVal.CreateEx(WS_EX_STATICEDGE,"EDIT","",WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|ES_RIGHT,CRect(0,0,0,0),this,IDC_EDITVAL);
    _wRand.CreateEx(WS_EX_STATICEDGE,"EDIT","",WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|ES_RIGHT,CRect(0,0,0,0),this,IDC_EDITRAND);
    _wValSld.Create(WS_CHILD|WS_VISIBLE|TBS_AUTOTICKS|TBS_BOTTOM|TBS_HORZ,CRect(0,0,0,0),this,IDC_SLIDERVAL);
    _wRandSld.Create(WS_CHILD|WS_VISIBLE|TBS_AUTOTICKS|TBS_BOTTOM|TBS_HORZ,CRect(0,0,0,0),this,IDC_SLIDERREAND);
    _labelLen=-1;
    _wValSld.SetTicFreq(toLargeInt(0.5f/_factor));
    _wRandSld.SetTicFreq(toLargeInt(0.5f/_factor)); 

    _wValSld.SetRange(toLargeInt(min/_factor),toLargeInt(max/_factor));
    _wRandSld.SetRange(toLargeInt(min/_factor),toLargeInt(max/_factor));
    _modify=false;
    _srcchange=false;
  }



  BEGIN_MESSAGE_MAP(PanelControlPair, CWnd)
    ON_WM_SIZE()
    ON_WM_HSCROLL()
    ON_EN_CHANGE(IDC_EDITVAL,OnChangeVal)
    ON_EN_CHANGE(IDC_EDITRAND,OnChangeRand)
    ON_EN_SETFOCUS(IDC_EDITRAND, OnEditSetFocus)
    ON_EN_SETFOCUS(IDC_EDITVAL, OnEditSetFocus)
    ON_EN_KILLFOCUS(IDC_EDITRAND, OnEditKillFocus)
    ON_EN_KILLFOCUS(IDC_EDITVAL, OnEditKillFocus)
    ON_WM_DESTROY()
    ON_WM_CTLCOLOR()
  END_MESSAGE_MAP()

  bool PanelControlPair::GetPreferredSize(int &xs, int &ys)
  {
    int xx=xs,yy=0;
    PanelControlWithLabel::GetPreferredSize(xx,yy);
    ys=PHeight*2;
    if (ys<yy) ys=yy;
    int xl=0;    
    if (_labelLen<0)
    {    
      CString t;
      _titleval.GetWindowText(t);
      CSize sz1=PanelControlWithLabel::GetTextExtent(&_titleval,t);
      _titlerand.GetWindowText(t);
      CSize sz2=PanelControlWithLabel::GetTextExtent(&_titlerand,t);
      xl=__max(sz1.cx,sz2.cx);
      _labelLen=xl+5;
    }

    xl=xx;
    xl+=PWidth;
    xl+=MSldWidth;
    if (xl>xs) xs=xl;
    return true;    
  }

  bool PanelControlPair::SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
  {
    if (_labelLen<0) GetPreferredSize(xs,ys);
    int xx=xs,yy=ys;
    PanelControlWithLabel::GetPreferredSize(xx,yy);
    PanelControlWithLabel::SetNewLocation(mw,x,y,xx-_labelLen,ys);
    mw.SetNewPosition(*this,x+xx-_labelLen,y,xs-xx+_labelLen,ys);
    return true;
  }
  bool PanelControlPair::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    if (PanelControlWithLabel::SetEnable(what,newState))
    {
      _wVal.EnableWindow(newState);
      _wRand.EnableWindow(newState);
      _titleval.EnableWindow(newState);
      _titlerand.EnableWindow(newState);
      _wValSld.EnableWindow(newState);
      _wRandSld.EnableWindow(newState);
      return true;
    }
    return false;
  }
  bool PanelControlPair::SetVisible(IPanelControlMsgs *what, bool newState)
  {
    if (PanelControlWithLabel::SetVisible(what,newState))
    {
      ShowWindow(newState?SW_SHOW:SW_HIDE);
      return true;
    }
    return false;
  }
  bool PanelControlPair::GoNextControl(IPanelControlMsgs *from, bool previous)
  {
    if (previous) _wRandSld.SetFocus();
    else _wValSld.SetFocus();
    return true;
  }
  HWND PanelControlPair::GetWindow() const
  {
    return *this;
  }
  bool PanelControlPair::SetFont(HFONT font)
  {
    _labelLen=-1;
    CFont *fnt=CFont::FromHandle(font);
    _titleval.SetFont(fnt);
    _titlerand.SetFont(fnt);
    _wVal.SetFont(fnt);
    _wRand.SetFont(fnt);
    _wValSld.SetFont(fnt);
    _wRandSld.SetFont(fnt);
    PanelControlWithLabel::SetFont(font);
    return true;
  }

  void PanelControlPair::OnSize(UINT nType, int cx, int cy)
  {
    if (cx>0 && cy>0)
    {    
      CSize sz=PanelControlWithLabel::GetTextExtent(&_wVal,"W")+CSize(2*GetSystemMetrics(SM_CXBORDER),2*GetSystemMetrics(SM_CYBORDER));
      if (_labelLen<0) {int xs=cx,ys=cy;GetPreferredSize(xs,ys);}
      _titleval.MoveWindow(0,0,_labelLen,PHeight);
      _titlerand.MoveWindow(0,PHeight,_labelLen,PHeight);
      _wVal.MoveWindow(_labelLen,0+(PHeight-sz.cy)/2,PWidth,sz.cy);
      _wRand.MoveWindow(_labelLen,PHeight+(PHeight-sz.cy)/2,PWidth,sz.cy);
      _wValSld.MoveWindow(_labelLen+PWidth,0,cx-(_labelLen+PWidth),PHeight);
      _wRandSld.MoveWindow(_labelLen+PWidth,PHeight,cx-(_labelLen+PWidth),PHeight);
    }
  }

  BOOL PanelControlPair::PreTranslateMessage(MSG* pMsg)
  {
    if (pMsg->message==WM_KEYDOWN)
    {
      int key=pMsg->wParam;
      CWnd *focus=GetFocus();
      switch (key)
      {
      case VK_TAB:
        {
          if (focus==&_wVal) _wValSld.SetFocus();
          else if (focus==&_wRand) _wRandSld.SetFocus();
          else if (focus==&_wValSld) _wVal.SetFocus();
          else if (focus==&_wRandSld) _wRand.SetFocus();
          break;
        }      
      case VK_UP:
        {
          if (focus==&_wVal || focus==&_wValSld) GetDocument()->GoNextControl(this,true);
          else 
            if (focus==&_wRand) _wVal.SetFocus();
            else _wValSld.SetFocus();
          break;
        }
      case VK_RETURN:
      case VK_DOWN:
        {
          if (focus==&_wRand || focus==&_wRandSld) GetDocument()->GoNextControl(this,false);
          else 
            if (focus==&_wVal) _wRand.SetFocus();
            else _wRandSld.SetFocus();
          break;
        }
      default: return FALSE;
      }
      return TRUE;
    }
    return FALSE;
  }

  void PanelControlPair::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
  {
    CWnd *scr=pScrollBar;
    int pos;
    CString t;
    if (nSBCode!=SB_ENDSCROLL)
    {
      if (scr==&_wRandSld)    
      {
          pos=_wRandSld.GetPos();
          t.Format("%g",pos*_factor);
          _wRand.SetWindowText(t);
      }
      else
      {
          pos=_wValSld.GetPos();
          _wRandSld.SetRangeMax(pos,TRUE);        
          t.Format("%g",pos*_factor);
          _wVal.SetWindowText(t);
      }
      _srcchange=true;
    }
    else
    {
      if (_srcchange) OnValueChange();
      _srcchange=false;
    }
  }

  void PanelControlPair::OnChangeVal()
  {
    _modify=true;
    CString c;
    _wVal.GetWindowText(c);
    float val;
    sscanf(c,"%f",&val);
    int max=toLargeInt(val/_factor);
    _wValSld.SetPos(max);
    _wRandSld.SetRangeMax(max);
  }
  void PanelControlPair::OnChangeRand()
  {
    _modify=true; 
    CString c;
    _wRand.GetWindowText(c);
    float val;
    sscanf(c,"%f",&val);
    int max=toLargeInt(val/_factor);
    _wRandSld.SetPos(max);
  }

  void PanelControlPair::OnEditSetFocus()
  {
    _modify=false;
  }
  void PanelControlPair::OnEditKillFocus()
  {
    if (_modify)
    {
      OnValueChange();
      _modify=false;
    }
  }

  void PanelControlPair::OnValueChange()
  {
    CString c;
    float val,rand;
    _wVal.GetWindowText(c);
    sscanf(c,"%f",&val);
    _wRand.GetWindowText(c);
    sscanf(c,"%f",&rand);
    _notify->DataChanged(val,rand);
  }

  void PanelControlPair::SetValues(float value, float random)
  {
    CString t;
    t.Format("%g",value);
    _wVal.SetWindowText(t);
    t.Format("%g",random);
    _wRand.SetWindowText(t);

    _wValSld.SetPos(toLargeInt(value/_factor));
    _wRandSld.SetPos(toLargeInt(random/_factor));
  }
  IPanelControlMsgs *PanelControlPair::WhoseWindow(HWND window)
  {
    if (window==*this || window==_wValSld || window==_wVal || window==_wRandSld || window==_wRand) return this;
    else return PanelControlWithLabel::WhoseWindow(window);
  }


  HBRUSH PanelControlPair::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
  {
    if (pWnd==&_wValSld || pWnd==&_wRandSld)
    {

      if (_sldBgr.m_hObject==0)
      {
        _sldBgr.CreateSolidBrush(GetControlBgrColor());
      }

      return _sldBgr;
    }
    else
      return __super::OnCtlColor(pDC,pWnd,nCtlColor);
  }

  // PanelControlSlider message handlers



}