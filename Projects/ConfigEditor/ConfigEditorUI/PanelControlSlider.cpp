// PanelControlSlider.cpp : implementation file
//

#include "stdafx.h"
//#include "LindaStudio.h"
#include "PanelControlSlider.h"
#include "PanelContainer.h"


// PanelControlSlider

#define IDC_SLIDERVAL 1001
#define IDC_EDITVAL 1002


namespace LindaStudio
{

  PanelControlSlider::PanelControlSlider(IPanelControlSliderNotify *notify,
    float min,
    float max,
    float step,
    float tick,
    int fprec,
    DocumentBase<IPanelControlMsgs> *document, 
    const char *labelText):
  PanelControlWithLabel(document,labelText),
    _min(min),
    _max(max),
    _step(step),
    _tick(tick),
    _change(false),
    _scrchange(false),
    _fprec(fprec),
    _notify(notify)
  {
    CWnd *parent=CWnd::FromHandle(GetDocument()->GetWindow());
    Create(AfxRegisterWndClass(0,LoadCursor(0,IDC_ARROW),(HBRUSH)(COLOR_BTNFACE+1),0),
      "",WS_VISIBLE|WS_CHILD,CRect(0,0,0,0),parent,0);

    _slider.Create(WS_CHILD|WS_VISIBLE|WS_TABSTOP|TBS_AUTOTICKS|TBS_BOTTOM|TBS_HORZ,CRect(0,0,0,0),this,IDC_SLIDERVAL);
    _val.CreateEx(WS_EX_STATICEDGE,"EDIT","",WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|ES_RIGHT,CRect(0,0,0,0),this,IDC_EDITVAL);

    _slider.SetRange(toLargeInt(_min/_step),toLargeInt(_max/_step));
    _slider.SetTicFreq(toLargeInt(_tick/_step));

  }

  BEGIN_MESSAGE_MAP(PanelControlSlider, CWnd)  
    ON_EN_SETFOCUS(IDC_EDITVAL, OnControlSetFocus)
    ON_EN_KILLFOCUS(IDC_EDITVAL, OnControlKillFocus)
    
    ON_WM_HSCROLL()
    ON_EN_CHANGE(IDC_EDITVAL,OnEditChange)
    ON_WM_SIZE()
    ON_WM_CTLCOLOR()
  END_MESSAGE_MAP()



  bool PanelControlSlider::GetPreferredSize(int &xs, int &ys)
  {  
    ys=PHeight;  
    int xx=xs; 
    PanelControlWithLabel::GetPreferredSize(xx,ys);
    return true;
  }

  bool PanelControlSlider::SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
  {
    int xx=xs,yy=ys;
    PanelControlWithLabel::GetPreferredSize(xx,yy);
    PanelControlWithLabel::SetNewLocation(mw,x,y,xx,ys);
    mw.SetNewPosition(*this,x+xx,y,xs-xx,ys);
    return true;
  }

  bool PanelControlSlider::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    if (PanelControlWithLabel::SetEnable(what,newState))
    {
      _slider.EnableWindow(newState);
      _val.EnableWindow(newState);
      return true;
    }
    return false;
  }

  bool PanelControlSlider::SetVisible(IPanelControlMsgs *what, bool newState)
  {
    if (PanelControlWithLabel::SetVisible(what,newState))
    {
      ShowWindow(newState?SW_SHOW:SW_HIDE);
      return true;
    }
    else
      return false;
  }

  bool PanelControlSlider::GoNextControl(IPanelControlMsgs *from, bool previous/* =false */)
  {
    _slider.SetFocus();
    return true;
  }

  HWND PanelControlSlider::GetWindow() const
  {
    return *this;
  }

  bool PanelControlSlider::SetFont(HFONT font) 
  {
    CFont *fnt=CFont::FromHandle(font);
    _slider.SetFont(fnt);
    _val.SetFont(fnt);
    PanelControlWithLabel::SetFont(font);
    return true;
  }


  void PanelControlSlider::OnControlSetFocus()
  {
    _change=false;
  }
  void PanelControlSlider::OnControlKillFocus()
  {
    if (_change)
    {
      ReadValue();
      _change=false;
    }
  }


  void PanelControlSlider::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
  {
    int pos=_slider.GetPos();
    float val=pos*_step;
    if (nSBCode==SB_ENDSCROLL)
    {
       if (_scrchange) ReadValue();
      _change=false;
    }
    else
    {
      _scrchange=true;
      SetValueToControl(val);
    }
  }

  void PanelControlSlider::SetValueToControl(float value)
  {
    char buff[50];
    sprintf(buff,"%%0.%df",_fprec);
    CString t;
    t.Format(buff,value);
    _val.SetWindowText(t);
  }

  bool PanelControlSlider::GetValueFromControl(float &val)
  {
    CString t;
    _val.GetWindowText(t);
    if (sscanf(t,"%f",&val)==1) return true;
    return false;
  }

  void PanelControlSlider::OnEditChange()
  {
    _change=true;
    float v;
    if (GetValueFromControl(v))
      _slider.SetPos(toLargeInt(v/_step));
  }

  void PanelControlSlider::ReadValue()
  {
    float v;
    if (GetValueFromControl(v))
      if (_notify) _notify->DataChanged(v);

  }

  void PanelControlSlider::OnSize(UINT nType, int cx, int cy)
  {
    if (cx>0 && cy>0)
    {    
      CSize sz=PanelControlWithLabel::GetTextExtent(&_val,"W")+CSize(2*GetSystemMetrics(SM_CXBORDER),2*GetSystemMetrics(SM_CYBORDER));      
      _slider.MoveWindow(PWidth,0,cx-PWidth,cy,TRUE);
      _val.MoveWindow(0,(PHeight-sz.cy)/2,PWidth,sz.cy,TRUE);
    }
  }

  void PanelControlSlider::SetValue(float val)
  {
    SetValueToControl(val);
    _slider.SetPos(toLargeInt(val/_step));
  }

  BOOL PanelControlSlider::PreTranslateMessage(MSG* pMsg)
  {
      if (pMsg->message==WM_KEYDOWN)
      {
        int key=pMsg->wParam;
        CWnd *focus=GetFocus();
        switch (key)
        {
        case VK_TAB:          
            if (focus==&_val) _slider.SetFocus();
            else if (focus==&_slider) _val.SetFocus();
            break;          
        case VK_UP:          
            GetDocument()->GoNextControl(this,true);
            break;          
        case VK_RETURN:
        case VK_DOWN:          
            GetDocument()->GoNextControl(this,false);
            break;
        default: return FALSE;
        }
        return TRUE;
      }
      return FALSE;
  }


  IPanelControlMsgs *PanelControlSlider::WhoseWindow(HWND window)
  {
    if (window==*this || window==_slider || window==_val) return this;
    else return PanelControlWithLabel::WhoseWindow(window);
  }


  static bool HasColor(CBrush &brsh, COLORREF color)
  {
    LOGBRUSH lg;
    brsh.GetLogBrush(&lg);
    return lg.lbColor==color;
  }

  HBRUSH PanelControlSlider::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)

  {
    if (pWnd==&_slider)
    {
    
        COLORREF bgcolor = GetControlBgrColor();
        if (bgcolor == 0xFF000000)
            return __super::OnCtlColor(pDC,pWnd,nCtlColor);
        if (_sldBgr.m_hObject==0 || !HasColor(_sldBgr,GetControlBgrColor()))
        {
          _sldBgr.DeleteObject();
          _sldBgr.CreateSolidBrush(GetControlBgrColor());
        }
    
        return _sldBgr;
    }
    else
      return __super::OnCtlColor(pDC,pWnd,nCtlColor);
  }

  // PanelControlSlider message handlers


}