#include "StdAfx.h"
#include ".\configeditor.h"
#include "ConfigDefaultFactory.h"
#include <el/Pathname/Pathname.h>
#include "ConfigClass.h"
#include "ConfigNamedArray.h"
#include "ConfigValue.h"
#include "ConfigArrayValue.h"

using namespace std;

ConfigEditor::ConfigEditor():_struct(_parser,ConfigDefaultFactory::Instance())
{
}

ConfigEditor::~ConfigEditor(void)
{
}

bool ConfigEditor::Load(const RString &name)
{
  ifstream in(name,ios::in);
  if (!in) return false;  
  return Load(in,name);
}

bool ConfigEditor::Save() const
{
  if (_name.GetLength()==0) return false;
  ofstream out(_name,ios::out|ios::trunc);
  if (_parser.SaveFile(out)==false) return false;

  return true;
}
bool ConfigEditor::Save(const RString &as)
{
  SetConfigName(as);
  return Save();  
}

bool ConfigEditor::Load(std::istream &input, const RString &name)
{
  SetConfigName(name);
  bool res=_parser.LoadFile(input);
  const char *c=Pathname::GetNameFromPath(name);
  _parser.ProcessIncludeDefault(name.Mid(0,c-name.Data()));
  _struct.CreateRoot();
  for(Each x(this);x;x->UpdateMe(0));
  return res;
}

bool ConfigEditor::LoadEmpty()
{
  _parser.DeleteSymbol(0,_parser.GetFileSymbols().Size());
  _struct.Clear();
  _struct.CreateRoot();
  return true;
}

bool ConfigEditor::Save(std::ostream &output) const
{
  return _parser.SaveFile(output);
}

const IConfigEntry *ConfigEditor::GetRoot() const
{
  return Find("$");
}

const IConfigEntry *ConfigEditor::Find(const RStringI &fullName) const
{
  return _struct.FindEntry(fullName);
}

const RStringI ConfigEditor::GetFullName(const IConfigEntry *entry) const
{
  return entry->GetFullName();
}

IConfigEntry::ServiceResult ConfigEditor::SetBase(const IConfigEntry *entry, const RStringI &baseName)
{
  if (entry==0) return IConfigEntry::srError;
  IConfigEntry::ServiceResult res=const_cast<IConfigEntry *>(entry)->SetBase(baseName);
  if (res==IConfigEntry::srOk) 
      for(Each x(this);x;x->SetBase(entry,baseName));
  return res;
}

IConfigEntry::ServiceResult ConfigEditor::SetName(const IConfigEntry *entry, const RStringI &newName)
{
  if (entry==0) return IConfigEntry::srError;
  IConfigEntry::ServiceResult res=const_cast<IConfigEntry *>(entry)->SetName(newName);
  if (res==IConfigEntry::srOk) 
    for(Each x(this);x;x->SetName(entry,newName));
  return res;
}

IConfigEntry::ServiceResult ConfigEditor::SetValue(const IConfigEntry *entry, const RString &value)
{
  if (entry==0) return IConfigEntry::srError;
  IConfigEntry::ServiceResult res=const_cast<IConfigEntry *>(entry)->SetValue(value);
  if (res==IConfigEntry::srOk) 
    for(Each x(this);x;x->SetValue(entry,value));
  return res;
}

ConfigEditor::CombinedResult ConfigEditor::AddValue(const IConfigEntry *subj, const IConfigEntry *before, bool newLine)
{
  IConfigEntry::ServiceResult err;
  if (subj==0) return IConfigEntry::srError;  
  RStringI beforeName=before?before->GetFullName():0;
  subj=const_cast<IConfigEntry *>(subj)->AddValue(const_cast<IConfigEntry *>(before),newLine,&err);
  if (err==IConfigEntry::srOk) 
  {  
    if (before) before=_struct.FindEntry(beforeName);
    for(Each x(this);x;x->AddValue(subj,before,newLine));
    return subj;
  }
  return err;
}

ConfigEditor::CombinedResult ConfigEditor::AddArray(const IConfigEntry *subj, const IConfigEntry *before, bool newLine)
{
  IConfigEntry::ServiceResult err;
  if (subj==0) return IConfigEntry::srError;
  RStringI beforeName=before?before->GetFullName():0;
  subj=const_cast<IConfigEntry *>(subj)->AddArray(const_cast<IConfigEntry *>(before),newLine,&err);
  if (err==IConfigEntry::srOk) 
  {  
    if (before) before=_struct.FindEntry(beforeName);
    for(Each x(this);x;x->AddArray(subj,before,newLine));
    return subj;
  }
  return err;
}

ConfigEditor::CombinedResult ConfigEditor::AddValue(const IConfigEntry *subj, RStringI name, const IConfigEntry *before)
{
  IConfigEntry::ServiceResult err;
  if (subj==0) return IConfigEntry::srError;  
  RStringI beforeName=before?before->GetFullName():0;
  subj=const_cast<IConfigEntry *>(subj)->AddValue(name,const_cast<IConfigEntry *>(before),&err);
  if (err==IConfigEntry::srOk) 
  {  
    if (before) before=_struct.FindEntry(beforeName);
    for(Each x(this);x;x->AddValue(subj,name,before));
    return subj;
  }
  return err;
}

ConfigEditor::CombinedResult ConfigEditor::AddArray(const IConfigEntry *subj, RStringI name, const IConfigEntry *before,bool newLine)
{
  IConfigEntry::ServiceResult err;
  if (subj==0) return IConfigEntry::srError;  
  RStringI beforeName=before?before->GetFullName():0;
  subj=const_cast<IConfigEntry *>(subj)->AddArray(name,const_cast<IConfigEntry *>(before),newLine,&err);
  if (err==IConfigEntry::srOk) 
  {  
    if (before) before=_struct.FindEntry(beforeName);
    for(Each x(this);x;x->AddArray(subj,name,before,newLine));
    return subj;
  }
  return err;
}


ConfigEditor::CombinedResult ConfigEditor::AddClass(const IConfigEntry *subj,RStringI name, const IConfigEntry *before)
{
  IConfigEntry::ServiceResult err;
  if (subj==0) return IConfigEntry::srError;  
  RStringI beforeName=before?before->GetFullName():0;
  subj=const_cast<IConfigEntry *>(subj)->AddClass(name,const_cast<IConfigEntry *>(before),&err);
  if (err==IConfigEntry::srOk) 
  {  
    if (before) before=_struct.FindEntry(beforeName);
    for(Each x(this);x;x->AddClass(subj,name,before));
    return subj;
  }
  return err;
}

ConfigEditor::CombinedResult ConfigEditor::AddClassLink(const IConfigEntry *subj,RStringI name, const IConfigEntry *before)
{
  IConfigEntry::ServiceResult err;
  if (subj==0) return IConfigEntry::srError;  
  RStringI beforeName=before?before->GetFullName():0;
  subj=const_cast<IConfigEntry *>(subj)->AddClassLink(name,const_cast<IConfigEntry *>(before),&err);
  if (err==IConfigEntry::srOk) 
  {  
    if (before) before=_struct.FindEntry(beforeName);
    for(Each x(this);x;x->AddClassLink(subj,name,before));
    return subj;
  }
  return err;
}

IConfigEntry::ServiceResult ConfigEditor::Delete(const IConfigEntry *what)
{
  if (what==0) return IConfigEntry::srError;  
  for(Each x(this);x;x->Delete(what));  
  return const_cast<IConfigEntry *>(what->GetParent())->Delete(const_cast<IConfigEntry *>(what));
}

IConfigEntry::ServiceResult ConfigEditor::EditSymbolStream(int atPos, int toDelete, const Array<LexSymbDesc> &symbols)
{
  _struct.DeleteSymbol(atPos,toDelete);
  _struct.InsertSymbol(atPos,symbols);
  _struct.CreateRoot();
  for(Each x(this);x;x->EditSymbolStream(atPos,toDelete,symbols));  
  return IConfigEntry::srOk;
}

ConfigEditor::CombinedResult ConfigEditor::Move(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before)
{
  ConfigClass *cls=dynamic_cast<ConfigClass *>(const_cast<IConfigEntry *>(where));
  if (cls==0) return IConfigEntry::srError;
  RString whatname=what->GetName();  

  IConfigEntry::ServiceResult res=cls->MoveItem(const_cast<IConfigEntry *>(what),const_cast<IConfigEntry *>(before),false);
  if (res!=IConfigEntry::srOk) return res;
  const IConfigEntry *newptr=cls->FindSubEntry(whatname);
  for(Each x(this);x;x->Move(newptr,where,0));
  return newptr;
}


ConfigEditor::CombinedResult ConfigEditor::Copy(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before, bool onlyStructure)
{
  if (onlyStructure)
  {
    IConfigEntry *wwhere=const_cast<IConfigEntry *>(where);
    const IConfigEntry *newptr=what->CloneTo(wwhere,const_cast<IConfigEntry *>(before));        
    return newptr;
  }
  else
  {
    ConfigClass *cls=dynamic_cast<ConfigClass *>(const_cast<IConfigEntry *>(where));
    if (cls==0) return IConfigEntry::srError;
    RString whatname=what->GetName();  

    IConfigEntry::ServiceResult res=cls->MoveItem(const_cast<IConfigEntry *>(what),const_cast<IConfigEntry *>(before),true);
    if (res!=IConfigEntry::srOk) return res;
    const IConfigEntry *newptr=cls->FindSubEntry(whatname);
    for(Each x(this);x;x->Move(newptr,where,0));
    return newptr;
  }
}

namespace Functors
{
  class UpdateMeFunctor
  {
    MVC::View<ConfigMVC> *view;
  public:
    UpdateMeFunctor(MVC::View<ConfigMVC> *view): view(view) {}
    bool operator()(const IConfigEntry *entry) const
    {
      IConfigEntry *parent=entry->GetParent();      
      ConfigEditor::CombinedResult res;
      if (dynamic_cast<const ConfigClass *>(entry)!=0)
        res=view->AddClass(entry,entry->GetName());
      else if (dynamic_cast<const ConfigNamedArray *>(entry)!=0)
        res=view->AddArray(entry,entry->GetName());
      else if (dynamic_cast<const ConfigArray *>(entry)!=0)
        res=view->AddArray(entry);
      else if (dynamic_cast<const ConfigValue *>(entry)!=0)
        res=view->AddValue(entry,entry->GetName());
      else if (dynamic_cast<const ConfigArrayValue *>(entry)!=0)
        res=view->AddValue(entry);
      return res!=IConfigEntry::srOk;
    }


  };
}

bool ConfigEditor::UpdateMe(MVC::View<ConfigMVC> *view)
{
  if (view)
  { 
    view->SetConfigName(_name);
    _struct.ForEachItem("$/",Functors::UpdateMeFunctor(view));
    return true;
  }
  else
    return MVC::Model<ConfigMVC>::UpdateMe(view);
}

void ConfigEditor::SetConfigName(const RStringI &name)
{
  _name=name;
  for(Each x(this);x;x->SetConfigName(name));
}

bool ConfigEditor::ForEach(const RStringI &mask,const IConfigEnumCallback &functor)
{
  return _struct.ForEachItem(mask,functor);
}