// StreamParser.h: interface for the StreamParser class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_StreamParser_H__BA6245A2_8B05_4CBB_8AA3_CCA0ECBA2739__INCLUDED_)
#define AFX_StreamParser_H__BA6245A2_8B05_4CBB_8AA3_CCA0ECBA2739__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "regexp.h"

///function for converting items to characters
/** function returns ASCII characters for items in range 0-127
and maps all other characters to range 128-255. For mapping
only 7 bytes is used. This allows use function with single byte
string, multibyte strings and also unicode strings
*/
template<class Item>
char Parser_ConvertToChar(const Item &item)
{
  if (item>=0 && item<128) return (char)item;
  else return (char)item | 0x80;
}


/// This is base template class
/** this is virtual stream class for parsing. Should be
used only when program needs parse from various streams.

To parse from one stream, use this class only as hint
how to build parser's stream class
*/

template<class Item>
class IStreamParser_Stream
{
public:
  /// Reads from the stream
  /** Function implements reading from stream.
  @param array pointer to array, that will be filled with items
  @param toread size of array in items, count of items that should be read
  @param wasread final count of items, function will fill this variable with count of items
	in array. Function can set this variable to zero, when stream reaches end of stream. 
  @return function returns true, when reading was successful, even if end of stream has been reached.
	Function returns false, when an error has occurred during reading, for example, disk read error.
	NOTE: False return is always reported by parser as reading error. End of stream return (wasread=0)
	is not error, parser only writes ending character
	*/
  virtual bool ReadStream(Item *array, size_t toread, size_t &wasread)=0;  
};

template<class Item>
class StreamParserResult
{
  const Item *begin;
  size_t size;
  public:
	const Item *Data() {return begin;}
	size_t Size() {return size;}
    operator const Item *() {return Data();}
	StreamParserResult(const Item *ptr, size_t size):begin(ptr),size(size) {}
};

template<class Item, class Stream>
class StreamParser
{
protected:

	Item *_buffer;
	size_t _allocated;
	size_t _used;

	bool Expand();
	
	int _subExpBegin[20];
	int _subExpEnd[20];

	bool _icase;
	bool _eof;

	int _lastError;

	Stream *_stream;
	bool _autodestruct;

public:
	StreamParser(Stream *_stream, bool autodestruct);
	~StreamParser();

	///Tests input stream for regular expression
	/**	
	@param regExp expression to test. Expression can contain any count of subexpressions
	@result result of test.
	*/
	bool Test(const char *regExp);

	///Tests input stream for regular expression (case insensitive)
	/**	
	@param regExp expression to test. Expression can contain any count of subexpressions
	@result result of test.
	NOTE: Case insensitive testing is done handle by function CharICompare.
	For other types than characters, you have to write own specialization */
	bool TestI(const char *regExp);

  ///Processes the test using a predefined functor
  /**
   * @param testFunctor functor called for the test. Functor
   * have to define operator()(char iterator,int *begSubExp, int *endSubExp, int maxSubExp).
   * 
   */
  template<class Functor>
  bool TestF(const Functor &testFunctor)
  {
    if (_lastError) return false;
    CharIterator<StreamParser<Item,Stream> >iterator(this),start(this);
    return testFunctor(iterator,_subExpBegin,_subExpEnd,sizeof(_subExpBegin)/sizeof(_subExpBegin[0]));
  }

	///Returns length (in Item) of sub-expressoin
	/**
	@param index specifies index of sub-expression. Default value means first
	  sub-expression in regexp. Valid range is <0,19> where 0 is size of 
	  matching string and 1-19 are sub-expressions,
	@return size of subexpression
	*/
	size_t GetSubExpLen(int index=1);

	///Returns pointer to subexpression. 
	/**
	@param subexp index of sub-expression.Default value means first
	  sub-expression in regexp. Valid range is <0,19> where 0 means 
	  whole matching string.
	@param size optional pointer to variable, that receives size of
	  sub-expression.
	@return function returns pointer to subexpression. This pointer
	  is valid until expression is acknowledged. NOTE: Returned
	  string IS NOT terminated by zero character. Use size
	  parameter to get number of valid characters in this subexpression
	  */
	const Item *GetSubExp(int subexp=1, size_t *size=NULL);


	StreamParserResult<Item> GetSubExpText(int subexp=1)
	{
	  return StreamParserResult<Item>(GetSubExp(subexp),GetSubExpLen(subexp));
	}
	///Function acknowledges input buffer and allows to continue parsing
	/**
	This is the last command before next round of tests. Function discards
	all data in buffer until the end of specified subexpression.
	@param subexp subexpression that specifies end of data, which will be
	  discarded. Default value (0) means, whole matching string
	@return true, if success, false if error, for example invalid subexp.
	Note: Function can be called, even if no regexp matched? In this case,
	function do nothing (and returns true) for subexp=0, otherwise returns 
	false.
	*/
	bool Acknowledge(int subexp=0);
    bool Ack(int subexp=0) {return Acknowledge(subexp);}

	///Function acknowledges nChars charcters in input buffer 
	/**
	@param nChars count of characters to acknowledge
	@return true, characters has been discarded, false, invalid parameter.
	*/
	bool AcknowledgeSize(size_t nChars);

	///Function reads subexpression as integer number
	/** Function does no tests, if expression is a number. Use Test for check number
	@param subexp index of subexpression
	@return value read from subexpression
	*/
	int GetSubExpInt(int subexp=1);

	///Function reads subexpression as real number
	/** Function does no tests, if expression is a number. Use Test for check number
	@param subexp index of subexpression
	@return value read from subexpression
	*/
	double GetSubExpDouble(int subexp=1);

	///Function discards any wait characters in input
	/** If there are no white characters, function does nothing.*/
	void SkipWhite();

	///Function reads character from buffer
	/** Function is used internally during parsing*/	
	bool ReadBuffer(int index, Item &item);

	///Function returns size of input buffer
	/** Function is used internally during parsing*/	
	size_t GetBufferUsage() const {return _used;}

	///Function returns pointer to input buffer
	/** Function is used internally during parsing*/	
	const Item *GetBuffer() const {return _buffer;}

	///Function pushes any string before input data. 
	/**This string will be testest on next Test command 
	NOTE: function is not optimized for simulating the stack.
	*/
	void Push(const Item *string, size_t stringSize);	

	///Function pops any string before input data. 
	/**Works same way as AcknonledgeSize
    NOTE: function is not optimized for simulating the stack.
	*/
	bool Pop(size_t sz) {return AcknowledgeSize(sz);}

	///Operator reads buffer as array of characters
	/** This needs internaly for comparing regular expression characters 
	Function uses template function Parser_ConvertToChar<Item>. 
	It is defined for single byte, multibyte or unicode characters.
	For other types, you need write own specialization */
	char operator[] (int index) const ;

	///Function returns last error code
	int GetLastError() {return _lastError;}

	///Function simple reads data from stream.
	/**
	@param list pointer to list that will be filed with items
	@param size size of list and count of items, that should be read.
	@param wasread variable will be filed with final count of items
	@param peek when this value is true, item is not discarded, and next call Read will read the
	  same data
	@return function returns true, when data has been read. Function returns false, 
	  when read error was reported
	 */
	bool Read(Item *list, size_t size, size_t &wasread, bool peek=false );

  bool Eof() {return _eof && _used==0;}

  ///Sets subexpression size
  /**
   *  @param begin changes begin of subexpression - use -1 for "nochange".
   *  @param size changes size of subexpression - use -1 for "nochange"
   */
  bool SetSubExpRange(int subexp, unsigned int begin=-1, unsigned int size=-1);

  unsigned int GetSubExpBegin(int subexp) const
  {
    return _subExpBegin[subexp];
  }

  unsigned int GetSubExpEnd(int subexp) const
  {
    return _subExpEnd[subexp];
  }



};

#endif // !defined(AFX_StreamParser_H__BA6245A2_8B05_4CBB_8AA3_CCA0ECBA2739__INCLUDED_)
