#pragma once

#include "IConfigEntry.h"
#include "LexSymbDesc.h"

class ConfigStructure;
class IConfigFactory
{
public:

  virtual IConfigEntry *CreateValue(ConfigStructure &owner) const=0;
  virtual IConfigEntry *CreateArray(ConfigStructure &owner) const=0;
  virtual IConfigEntry *CreateNamedArray(ConfigStructure &owner) const=0;
  virtual IConfigEntry *CreateClass(ConfigStructure &owner) const=0;
  virtual IConfigEntry *CreateArrayValue(ConfigStructure &owner) const=0;
  virtual IConfigEntry *CreateRoot(ConfigStructure &owner) const=0;
  virtual IConfigEntry *CreateDefinition(ConfigStructure &owner) const=0;

};
