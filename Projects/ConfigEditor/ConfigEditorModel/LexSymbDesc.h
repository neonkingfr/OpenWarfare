#pragma once

#include "LexFunctor.h"

class LexSymbDesc
{
  SymbolType _symbType;
  RString _symbText;
  bool _save;
public:
  LexSymbDesc(SymbolType symbType,RString symbText, bool save):_save(save),_symbText(symbText),_symbType(symbType) {}
  LexSymbDesc():_symbType(symEof) {}
  LexSymbDesc(const LexSymbDesc&first, const LexSymbDesc&second, SymbolType symbOverride=symEof)
    :_symbType(symbOverride==symEof?first.GetSymbolType():symbOverride),
      _symbText(first.GetSymbolText()+second.GetSymbolText()),
      _save(first._save) 
    {}
    LexSymbDesc(const LexSymbDesc&first, const LexSymbDesc&second, const LexSymbDesc&third, SymbolType symbOverride=symEof)
      :_symbType(symbOverride==symEof?first.GetSymbolType():symbOverride),
      _symbText(first.GetSymbolText()+second.GetSymbolText()+third.GetSymbolText()),
      _save(first._save) 
    {}
  
  LexSymbDesc(const LexSymbDesc&other, SymbolType symbOverride):
    _symbType(symbOverride),
    _symbText(other._symbText),
    _save(other._save)
    {}
  
  void EnableStore(bool enable) {_save=enable;}
  bool IsStoreEnabled() const {return _save;}

  SymbolType GetSymbolType() const {return _symbType;}
  RString GetSymbolText() const {return _symbText;}

  ClassIsMovable(LexSymbDesc);
};

