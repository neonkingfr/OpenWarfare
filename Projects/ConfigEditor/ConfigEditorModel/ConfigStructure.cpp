#include "StdAfx.h"
#include ".\configstructure.h"
#include "ConfigClass.h"

ConfigStructure::ConfigStructure(LexParser &dataSrc,IConfigFactory &factory):_dataSrc(dataSrc),factory(factory)
{
}

ConfigStructure::~ConfigStructure(void)
{
  Clear();
}

void ConfigStructure::Clear()
{
  Iter it(_data);
  Item *item=0;
  while((item=it.Next())!=0)
  {
    (*item)->Release();
  }
  _data.Clear();
}

const LexSymbDesc &ConfigStructure::GetSymbol(int index) const
{
  return _dataSrc.GetFileSymbols()[index];
}
void ConfigStructure::ReplaceSymbol(int index, const LexSymbDesc &symb)
{
  _dataSrc.ReplaceSymbol(index,symb);
}
void ConfigStructure::InsertSymbol(int index, const Array<LexSymbDesc> &symb)
{
  _dataSrc.InsertSymbols(index,symb);
  ShiftSymbolIndexes(index,symb.Size());
}

void ConfigStructure::ShiftSymbolIndexes(int start, int shift)
{
  Iter it(_data);
  Item *item=0;
  while((item=it.Next())!=0)
  {
    (*item)->ShiftSymbolIndexes(start,shift);
  }
}

IConfigEntry *ConfigStructure::FindEntry(const RStringI &fullname) const
{

  Item *i=_data.Find(&SearchClass(fullname));
  if (i==0) return 0;
  else return *i;
}

int ConfigStructure::PackToPlainText(int symbolId,SymbolType termSymbol, bool trim)
{
  return PackToPlainText(symbolId,Array<SymbolType>(&termSymbol,1),trim);
}

int ConfigStructure::PackToPlainText(int symbolId,const Array<SymbolType> &termSymbol, bool trim)
{
  SymbolType symb=GetSymbol(symbolId).GetSymbolType();
  for (int i=0;i<termSymbol.Size();i++)
    if (termSymbol[i]==symb)
    {
      LexSymbDesc empty(symPlainText,"",true);
      InsertSymbol(symbolId,Array<LexSymbDesc>(&empty,1));
    }
  int res=_dataSrc.PackToPlainText(symbolId,termSymbol,trim);
  if (res) ShiftSymbolIndexes(symbolId+1,res);
  return res;
}

int ConfigStructure::CountSymbols() const
{
  return _dataSrc.GetFileSymbols().Size();
}

bool ConfigStructure::IsQuoted(const RString &text)
{
  if (text.GetLength()>1 && text[0]=='"' && text[text.GetLength()-1]=='"')
  {
    bool ok=true;
    for (int i=1,cnt=text.GetLength()-1;i<cnt;i++)
      if (text[i]=='"')
      {
        if (i+1==cnt || text[i+1]!='"') {break;ok=false;}        
        else i++;
      }
      if (ok) return true;
  }
  return false;
}

RString ConfigStructure::QuoteText(const RString &text, bool preventDoubleQuote)
{
  if (preventDoubleQuote && IsQuoted(text)) return text;  
  int quotes=0;
  int cnt=text.GetLength();
  for (int i=0;i<cnt;i++) if (text[i]=='"') quotes++;
  RString res;
  char *z=res.CreateBuffer(cnt+quotes+2);  
  *z='"';
  z++;
  for (int i=0;i<cnt;i++) 
  {
    if (text[i]=='"') *z++='"';
    *z++=text[i];
  }
  *z='"';
  return res;
}

void ConfigStructure::RegisterNewItem(IConfigEntry *entry)
{
  Item *k=_data.Find(entry);
  if (k) 
  {
    Item z=*k;
    _data.Remove(entry);
    z->Release();
  }
  _data.Add(entry);
}

RString ConfigStructure::IndexToName(int idx)
{
  char buff[50];
  sprintf(buff,"[%06d]",idx);
  return RString(buff);
}

RString ConfigStructure::CreateIdent(int n)
{
  RString x;
  char *z=x.CreateBuffer(n);
  for (int i=0;i<n;i++) z[i]='\t';
  return x;
}

void ConfigStructure::DeleteSubitems(IConfigEntry *entry)
{
  RString mask(entry->GetFullName(),"/");
  Iter it(_data);
  Item *item=0;
  int masklen=mask.GetLength();
  SearchClass srch(mask);
  it.BeginFrom(&srch);
  while ((item=it.Next())!=0  && strnicmp((*item)->GetFullName().Data(),mask.Data(),masklen)==0)
  {
    Item i=*item;
    _data.Remove(i);
    i->Release();
    it.BeginFrom(&srch);
  }
}

void ConfigStructure::UnregisterItem(IConfigEntry *entry)
{
  DeleteSubitems(entry);
  _data.Remove(entry);
}

void ConfigStructure::DeleteSymbol(int index, int count)
{
  _dataSrc.DeleteSymbol(index,count);
  ShiftSymbolIndexes(index,-count);
}

int ConfigStructure::SkipBlindSymbols(int index,bool skipdef)
{
  int cnt=CountSymbols();
  while (index<cnt)
  {
    const LexSymbDesc &x=GetSymbol(index);
    SymbolType s=x.GetSymbolType();
    if (s!=symWhiteSpaces && s!=symNewLine && s!=symBlockComment && s!=symLineComment && s!=symOtherPreproc)
    {
      if (s==symDefine && skipdef)
      {
        while (index<cnt && GetSymbol(index).GetSymbolType()!=symDefineContent) index++;
        if (index<cnt) index++;
      }
      else if (s==symInclude)
      {
        index+=4;        
      }
      else break;
    }
    else 
      index++;
  }
  if (index>cnt) index=cnt;
  return index;
}

IConfigEntry *ConfigStructure::CreateRoot()
{
  ConfigItemBase *cls=static_cast<ConfigItemBase *>(Factory().CreateRoot(*this));
  cls->LoadStructure(0,0,0);
  return cls;
}


bool ConfigStructure::NeedQuote(const RString& x)
{
  if (IsQuoted(x)) return false;
  for (int i=0;i<x.GetLength();i++)
  {
    const char *z=strchr("[];\\/,{}:()#@*\" \t",x[i]);
    if (z!=0) return true;
  }
  return false;  
}

RString ConfigStructure::Unquote(const RString& x)
{
  if (x.GetLength()<2) return x;
  int needchr=0;
  const char *c=x+1;
  while (c[1]!=0)
  {
    if (c[0]=='"' && c[1]=='"') c++;
    needchr++;
    c++;
  }
  RString res;
  char *d=res.CreateBuffer(needchr);
  c=x+1;
  while (c[1]!=0)
  {
    if (c[0]=='"' && c[1]=='"') c++;
    *d++=*c++;
  }
  return res;

}

bool ConfigStructure::IsAValidIdentifier(const RString &x)
{
  if (x.GetLength()<1) return false;
  if (!isalpha(x[0]) && x[0]!='_') return false;
  for (int i=1;i<x.GetLength();i++)
  {
    if (!isalnum(x[i]) && x[i]!='_') return false;
  }
  return true;
}


const Array<LexSymbDesc> &ConfigStructure::GetSymbols() const
{
  return _dataSrc.GetFileSymbols();
}
