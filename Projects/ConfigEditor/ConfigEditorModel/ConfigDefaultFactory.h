#pragma once
#include "iconfigfactory.h"

class ConfigDefaultFactory :
  public IConfigFactory
{
public:
  virtual IConfigEntry *CreateValue(ConfigStructure &owner) const;
  virtual IConfigEntry *CreateArray(ConfigStructure &owner) const;
  virtual IConfigEntry *CreateNamedArray(ConfigStructure &owner) const;
  virtual IConfigEntry *CreateClass(ConfigStructure &owner) const;
  virtual IConfigEntry *CreateArrayValue(ConfigStructure &owner) const;
  virtual IConfigEntry *CreateRoot(ConfigStructure &owner) const;
  virtual IConfigEntry *CreateDefinition(ConfigStructure &owner) const;

  static IConfigFactory &Instance();
};

