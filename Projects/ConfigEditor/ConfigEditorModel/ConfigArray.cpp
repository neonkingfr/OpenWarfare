#include "StdAfx.h"
#include ".\configarray.h"

ConfigArray::ConfigArray(ConfigStructure &owner):ConfigItemBase(owner)
{
}

ConfigArray::~ConfigArray(void)
{
}

bool ConfigArray::LoadHeader(int index, int start, int &end, RStringI &subname)
{
  ConfigStructure &owner=GetOwner();

  _values=0;
  subname=owner.IndexToName(index);  
  
  int cnt=owner.CountSymbols();
  if (start>=cnt) return false;
  const LexSymbDesc &lx=owner.GetSymbol(start);
  if (lx.GetSymbolType()!=symLeftBrace) return false;  
  start++;
  start=owner.SkipBlindSymbols(start);
  end=start;
  return (start<cnt);
}

bool ConfigArray::LoadSubItems(int start, int &end)
{
  ConfigStructure &owner=GetOwner();
  int cnt=owner.CountSymbols();
  while (start<cnt && owner.GetSymbol(start).GetSymbolType()!=symRightBrace && owner.GetSymbol(start).GetSymbolType()!=symSemicolon)
  {
    const LexSymbDesc &lx=owner.GetSymbol(start);
    SymbolType sm=lx.GetSymbolType();
    if (sm==symWord || sm==symColon || sm==symAssign || sm==symQuotedText || 
      sm==symPlainText || sm==symDoubleSharp)
    {
      ConfigItemBase *newEntry=static_cast<ConfigItemBase *>(owner.Factory().CreateArrayValue(owner));
      int newEnd=newEntry->LoadStructure(_values,start,this);
      if (newEnd!=-1)
      {
        _values++;
        start=newEnd;
      }
      else 
        newEntry->Release();
    }
    else if (sm==symLeftBrace)
    {
      ConfigArray *newEntry=static_cast<ConfigArray *>(owner.Factory().CreateArray(owner));
      int newEnd=newEntry->LoadStructure(_values,start,this);
      if (newEnd!=-1)
      {
        _values++;
        start=newEnd;
        const LexSymbDesc &lx=owner.GetSymbol(start);
        if (lx.GetSymbolType()==symRightBrace) start++;
      }      
    }
    else
      start++;
    start=owner.SkipBlindSymbols(start);
    cnt=owner.CountSymbols();
 } 
  end=start;
  return true;
}

IConfigEntry *ConfigArray::Get(int idx)
{
  if (idx<0 || idx>=_values) return 0;
  return FindSubEntry(ConfigStructure::IndexToName(idx));
}
const IConfigEntry *ConfigArray::Get(int idx) const
{
  if (idx<0 || idx>=_values) return 0;
  return FindSubEntry(ConfigStructure::IndexToName(idx));
}

bool ConfigArray::InsertValue(int index, bool newline)
{
  ConfigStructure &owner=GetOwner();

  if (owner.GetSymbol(GetFirstSymbol()).IsStoreEnabled()==false) return false;

  LexSymbDesc seq[4];
  seq[0]=LexSymbDesc(symComma,",",true);
  seq[1]=LexSymbDesc(symNewLine,"\n",true);
  seq[2]=LexSymbDesc(symWhiteSpaces,owner.CreateIdent(GetIdent()),true);
  seq[3]=LexSymbDesc(symPlainText,"0",true);
  int z;  
  if (index==_values) z=GetTermSymbol();
  else z=static_cast<ConfigItemBase *>(Get(index))->GetFirstSymbol();
  if (index!=0) owner.InsertSymbol(z++,Array<LexSymbDesc>(seq+0,1));
  if (newline) owner.InsertSymbol(z,Array<LexSymbDesc>(seq+1,3));
  else owner.InsertSymbol(z,Array<LexSymbDesc>(seq+3,1));
  Reload();
  return true;
}
bool ConfigArray::InsertArray(int index, bool newline)
{
  ConfigStructure &owner=GetOwner();

  if (owner.GetSymbol(GetFirstSymbol()).IsStoreEnabled()==false) return false;

  LexSymbDesc seq[5];
  seq[0]=LexSymbDesc(symComma,",",true);
  seq[1]=LexSymbDesc(symNewLine,"\n",true);
  seq[2]=LexSymbDesc(symWhiteSpaces,owner.CreateIdent(GetIdent()),true);
  seq[3]=LexSymbDesc(symLeftBrace,"{",true);
  seq[4]=LexSymbDesc(symLeftBrace,"}",true);

  int z;  
  if (index==_values) z=GetTermSymbol();
  else z=static_cast<ConfigItemBase *>(Get(index))->GetFirstSymbol();
  if (index!=0) owner.InsertSymbol(z++,Array<LexSymbDesc>(seq+0,1));
  if (newline) owner.InsertSymbol(z,Array<LexSymbDesc>(seq+1,4));
  else owner.InsertSymbol(z,Array<LexSymbDesc>(seq+3,2));
  Reload();
  return true;
}


void ConfigArray::DeleteItem(int index)
{
  ConfigItemBase *p=static_cast<ConfigItemBase *>(Get(index));
  if (p==0) return;
  int beg=p->GetFirstSymbol();
  p->Delete(false);
  int i=0;
  ConfigStructure &owner=GetOwner();
  int sz=owner.CountSymbols();
  while (beg+i<sz)
  {
    const LexSymbDesc &x=owner.GetSymbol(beg+i);
    if (x.GetSymbolType()==symComma)
    {
      i++;
      break;
    }
    if (x.GetSymbolType()==symRightBrace || x.GetSymbolType()==symSemicolon)
    {
      break;
    }
    i++;
  }
  if (i) owner.DeleteSymbol(beg,i);
  Reload();
}

int ConfigArray::PerformAddChecks(IConfigEntry *before, ServiceResult *res)
{
  if (before && (before->GetFilePosition()<GetFirstSymbol() || before->GetFilePosition()>=GetTermSymbol()))
  {
    return Res(res,srError),-1;
  }
  ConfigStructure &owner=GetOwner();
  if (!owner.GetSymbol(GetFirstSymbol()).IsStoreEnabled())
  {
    return Res(res,srReadOnly),-1;
  }
  if (before) return before->GetFilePosition();
  else 
  {
    if (_values==0) 
    {
      int x=GetFirstSymbol();
      x=owner.SkipBlindSymbols(x);
      while (owner.GetSymbol(x).GetSymbolType()!=symLeftBrace) x=owner.SkipBlindSymbols(x+1);
      return x+1;
    }
    ConfigItemBase *prev=static_cast<ConfigItemBase *>(FindSubEntry(owner.IndexToName(_values-1)));
    Assert(prev!=0);    
    return prev->GetTermSymbol();
  }
}

IConfigEntry *ConfigArray::AddSymbols(const Array<LexSymbDesc> &symbs, IConfigEntry *before, bool newLine,ServiceResult *res)
{
  RString name;
  ConfigStructure &owner=GetOwner();
  if (before==0) name=owner.IndexToName(_values);
  else name=strrchr(before->GetFullName(),'/')+1;
  int beforeId=PerformAddChecks(before,res);
  if (beforeId==-1) return 0;

  AutoArray<LexSymbDesc,MemAllocStack<LexSymbDesc,32> > newSymb;

  if (before==0)
  {
    if (_values)    
      newSymb.Add(LexSymbDesc(symComma,",",true));
    if (newLine)
    {
      newSymb.Add(LexSymbDesc(symNewLine,"\n",true));
      newSymb.Add(LexSymbDesc(symWhiteSpaces,owner.CreateIdent(GetIdent()),true));
    }
  }
  newSymb.Append(symbs);
  if (before)
  {
    newSymb.Add(LexSymbDesc(symComma,",",true));
    if (newLine)
    {
      newSymb.Add(LexSymbDesc(symNewLine,"\n",true));
      newSymb.Add(LexSymbDesc(symWhiteSpaces,owner.CreateIdent(GetIdent()),true));
    }
  }
  owner.InsertSymbol(beforeId,newSymb);
  Reload();
  return Res(res,srOk),FindSubEntry(name);
  
}

IConfigEntry *ConfigArray::AddArray(IConfigEntry *before /* =0  */, bool newline /* =true  */,ServiceResult *res /* =0  */)
{
  LexSymbDesc x[2];
  x[0]=LexSymbDesc(symLeftBrace,"{",true);
  x[1]=LexSymbDesc(symRightBrace,"}",true);
  return AddSymbols(Array<LexSymbDesc>(x,2),before,newline,res);
}

IConfigEntry *ConfigArray::AddValue(IConfigEntry *before /* =0  */, bool newline /* =false  */,ServiceResult *res /* =0  */)
{
  LexSymbDesc x(symPlainText,"0",true);
  return AddSymbols(Array<LexSymbDesc>(&x,1),before,newline,res);
}

IConfigEntry::ServiceResult ConfigArray::Delete(IConfigEntry *what)
{
  ConfigStructure &owner=GetOwner();
  if (what==0) return srError;
  if (!owner.GetSymbol(GetFirstSymbol()).IsStoreEnabled()) return srReadOnly;
  int pos=what->GetFilePosition();
  if (pos<GetFirstSymbol() || pos>=GetTermSymbol()) return srError;
  
  static_cast<ConfigItemBase *>(what)->Delete(false);
  pos=owner.SkipBlindSymbols(pos);
  if (owner.GetSymbol(pos).GetSymbolType()==symComma)
    owner.DeleteSymbol(pos,1);
  Reload();
  return srOk;
}

IConfigEntry *ConfigArray::CloneTo(IConfigEntry *newParent, IConfigEntry *before) const
{
  ConfigArray *nwpr=dynamic_cast<ConfigArray *>(newParent);
  if (nwpr==0) return 0;
  IConfigEntry *it=nwpr->AddArray(before);
  return it;
}