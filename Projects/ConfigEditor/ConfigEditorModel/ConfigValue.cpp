#include "StdAfx.h"
#include ".\configvalue.h"
#include "ConfigClass.h"

ConfigValue::ConfigValue(ConfigStructure &owner):_symIdName(-1),_symIdValue(-1),ConfigItemBase(owner)
{
}

static SymbolType termSymb[]=
{
  symLeftBracket,
    symRightBracket,
    symRightBrace,
    symAssign,
    symComma,
    symColon,
    symSemicolon,
    symQuote,
    symLeftParenthesis,
    symRightParenthesis,
    symLeftBrace,
    symRightBrace,
    symBlockComment,
    symLineComment,
    symDefine,
    symDefineContent,
    symInclude,


};

bool ConfigValue::LoadHeader(int index, int start, int &end, RStringI &subname)
{
  ConfigStructure &owner=GetOwner();
  int cnt=owner.CountSymbols();
  if (start<cnt)
  {
//     owner.PackToPlainText(start,Array<SymbolType>(termSymb,lenof(termSymb)),true);      
     cnt=owner.CountSymbols();
    if (owner.GetSymbol(start).GetSymbolType()==symWord)
    {
      _symIdName=start;
      start++;
      start=owner.SkipBlindSymbols(start);
      if (start<cnt && owner.GetSymbol(start).GetSymbolType()!=symAssign) return false;
      start++;
      start=owner.SkipBlindSymbols(start);
      if (start<cnt)
      {
        owner.PackToPlainText(start,symSemicolon,false);
        _symIdValue=start;
        end=start+1;
        subname=owner.GetSymbol(_symIdName).GetSymbolText();
        return true;
      }
      else
      {
        return false;
      }
    }
  }
  return false;
}



IConfigEntry::ServiceResult ConfigValue::SetValue(RString value)
{
  ConfigStructure &owner=GetOwner();
  if (owner.NeedQuote(value)) value=owner.QuoteText(value,false);
  const LexSymbDesc &symb=owner.GetSymbol(_symIdValue);
  if (symb.IsStoreEnabled())
  {
    owner.ReplaceSymbol(_symIdValue,LexSymbDesc(symPlainText,value,true));
    return srOk;
  }
  else
    return srReadOnly;
}

IConfigEntry::ServiceResult ConfigValue::SetName(const RStringI &name)
{
  ConfigStructure &owner=GetOwner();
  if (!owner.IsAValidIdentifier(name)) return srNotValidName;
  IConfigEntry *parent=GetParent();
  if (parent)
  {
    if (parent->FindSubEntry(name)) return srDuplicate;
  }
  const LexSymbDesc &symb=owner.GetSymbol(_symIdName);
  if (symb.IsStoreEnabled())
  {
    owner.ReplaceSymbol(_symIdName,LexSymbDesc(symPlainText,name,true));
    Reload();
    return srOk;
  }
  else
    return srReadOnly;
}

RStringI ConfigValue::GetName(ServiceResult *res) const
{  
  const ConfigStructure &owner=GetOwner();
  const LexSymbDesc &symb=owner.GetSymbol(_symIdName);
  return Res(res,srOk),symb.GetSymbolText();
}
RStringS ConfigValue::GetValue(ServiceResult *res) const
{
  const ConfigStructure &owner=GetOwner();
  const LexSymbDesc &symb=owner.GetSymbol(_symIdValue);
  return Res(res,srOk),symb.GetSymbolText();
}

void ConfigValue::ShiftSymbolIndexes(int start, int shift)
{
  if (_symIdName>=start) _symIdName+=shift;
  if (_symIdValue>=start) _symIdValue+=shift;
  ConfigItemBase::ShiftSymbolIndexes(start,shift);
}

IConfigEntry *ConfigValue::CloneTo(IConfigEntry *newParent, IConfigEntry *before) const
{
  ConfigClass *nwpr=dynamic_cast<ConfigClass *>(newParent);
  if (nwpr==0) return 0;
  IConfigEntry *entry=nwpr->AddValue(GetName(),before);
  if (entry!=0) entry->SetValue(GetValue());
  return entry;
}