#pragma once

#include "LexSymbDesc.h"


class LexParser;
class ILexParserIncludeProcessor
{
public:
  virtual AutoArray<LexSymbDesc> ProcessInclude(const RString filename)=0;
};

class LexParser
{
protected:
  AutoArray<LexSymbDesc> _parsedSymbols;
  void Optimize();
public:

  bool LoadFile(std::istream &input);
  bool SaveFile(std::ostream &output) const;
  const Array<LexSymbDesc> &GetFileSymbols() const {return _parsedSymbols;}

  void ReplaceSymbol(int symbolId, const LexSymbDesc &newSymb);
  void InsertSymbol(int before, const LexSymbDesc &newSymb);
  void InsertSymbols(int before, const Array<LexSymbDesc> &symbList, bool setEnableStore=false, bool enableStore=false);
  void DeleteSymbol(int symbolId, int countSymbols=1);
  ///Packs symbols into plaintText symbol
  /**
   * @param symbolId id of first symbol, where pack starts
   * @param termSymbol type of terminating symbol. Symbol is not included
   * @param trim true=trim ending white spaces or newline
   * @return count of symbols packed (except the first). Result 0 means no symbols packed.
   * @note Function will not pack plainText symbols and quoted symbols. Packing always
   * stops on this symbol.
   */
  int PackToPlainText(int symbolId,const Array<SymbolType> &termSymbol, bool trim);

  void ProcessInclude(ILexParserIncludeProcessor *processor);

  void ProcessIncludeDefault(const RStringI &startPath);
};


class DefaultIncludeProcessor: public ILexParserIncludeProcessor
{
  RString basePath;
  FindArray<RStringI> *_processed;
public:
  DefaultIncludeProcessor(RString basePath,FindArray<RStringI> *processed):basePath(basePath),_processed(processed) {}
  virtual AutoArray<LexSymbDesc> ProcessInclude(const RString filename);
};