#include "StdAfx.h"
#include ".\lexparser.h"
#include "StreamParser.h"
#include "LexFunctor.tli"
#include "StreamParser.tli"
#include <El/Pathname/Pathname.h>

using namespace std;

class ParserStream
{
  istream &input;
public:
  ParserStream(istream &input):input(input) {}
  bool ReadStream(char *array, size_t toread, size_t &wasread)
  {
    input.read(array,(std::streamsize)toread);
    wasread=input.gcount();    
    return true;
  }
};

bool LexParser::LoadFile(std::istream &input)
{
  ParserStream stream(input);
  StreamParser<char,ParserStream> parser(&stream,false);
  LexFunctor helper;
  
  while (parser.TestF(helper))
  {
    RString rd;
    rd.CreateStringFromArray(parser.GetSubExpText(0));
    _parsedSymbols.Add(LexSymbDesc(helper.GetSymbolType(),rd,true));
    parser.Acknowledge();
  }
  Optimize();
  return helper.GetSymbolType()==symEof;
}

bool LexParser::SaveFile(std::ostream &output) const
{
  for (int i=0;i<_parsedSymbols.Size();i++) if (_parsedSymbols[i].IsStoreEnabled())
  {
    const RString &text=_parsedSymbols[i].GetSymbolText();
    output.write(text,text.GetLength());
  }
  return (!(!output));
}


void LexParser::Optimize()
{
  bool begLine=true;
  for (int i=0;i<_parsedSymbols.Size();i++)
  {
    int fe=_parsedSymbols.Size()-1;
    LexSymbDesc &desc=_parsedSymbols[i];
    switch(desc.GetSymbolType())
    {
    case symDoubleSharp:
      if (begLine && i+3<=fe && _parsedSymbols[i+1].GetSymbolText()=="include" 
        && _parsedSymbols[i+2].GetSymbolType()==symWhiteSpaces && 
           _parsedSymbols[i+3].GetSymbolType()==symQuote)
      {
        _parsedSymbols[i]=LexSymbDesc(LexSymbDesc(_parsedSymbols[i],_parsedSymbols[i+1]),LexSymbDesc(_parsedSymbols[i+2],_parsedSymbols[i+3]),symInclude);
        _parsedSymbols.Delete(i+1,3);
        fe=_parsedSymbols.Size()-1;
        i+=2;
        while (i<=fe && _parsedSymbols[i].GetSymbolType()!=symQuote)
        {          
          _parsedSymbols[i-1]=LexSymbDesc(_parsedSymbols[i-1],_parsedSymbols[i]);
          _parsedSymbols.Delete(i);
        }
        _parsedSymbols[i]=LexSymbDesc(_parsedSymbols[i],symDefineContent);
      }
      else if (begLine && i+1<=fe && _parsedSymbols[i+1].GetSymbolText()=="define")
      {
        _parsedSymbols[i]=LexSymbDesc(_parsedSymbols[i],_parsedSymbols[i+1],symDefine);
        _parsedSymbols.Delete(i+1);
        fe=_parsedSymbols.Size()-1;
        i+=1;
        while (i<=fe && _parsedSymbols[i].GetSymbolType()!=symNewLine && _parsedSymbols[i].GetSymbolType()!=symWhiteSpaces) i++;
        if (i<fe && _parsedSymbols[i].GetSymbolType()==symWhiteSpaces)
        {
          i++;
          LexSymbDesc &def=_parsedSymbols[i];
          def=LexSymbDesc(def,symDefineContent);
          i++;
          while (i<_parsedSymbols.Size() && _parsedSymbols[i].GetSymbolType()!=symNewLine)
          {
            LexSymbDesc cur=_parsedSymbols[i];
            def=LexSymbDesc(def,cur);
            _parsedSymbols.Delete(i);
            if (cur.GetSymbolText()=="\\" && i<_parsedSymbols.Size())
            {
              LexSymbDesc &cur=_parsedSymbols[i];
              def=LexSymbDesc(def,cur);              
              _parsedSymbols.Delete(i);
            }
          }
          i--;
        }
      }
      else if (begLine)
      {
        SymbolType term=symNewLine;
        PackToPlainText(i,Array<SymbolType>(&term,1),false);
        _parsedSymbols[i]=LexSymbDesc(_parsedSymbols[i],symOtherPreproc);
      }
      break;
    case symNewLine: begLine=true;continue;
    case symQuote:
      {
        bool rep;
        if (_parsedSymbols.Size()-1!=i)
        do 
        {
          LexSymbDesc &nx=_parsedSymbols[i+1];
          rep=true;
          if (nx.GetSymbolType()==symQuote)
          {
            desc=LexSymbDesc(desc,nx,symQuotedText);
            _parsedSymbols.Delete(i+1);
            LexSymbDesc &nx=_parsedSymbols[i+1];
            if (nx.GetSymbolType()!=symQuotedText) 
            {
              rep=false;
            }
            else 
            {
              desc=LexSymbDesc(desc,nx,symQuotedText);
              _parsedSymbols.Delete(i+1);
            }
          }
          else
          {
            desc=LexSymbDesc(desc,nx,symQuotedText);
            _parsedSymbols.Delete(i+1);
          }
        }
        while (rep && _parsedSymbols.Size()-1!=i);
      }
      break;
    }
    if (desc.GetSymbolType()!=symWhiteSpaces) begLine=false;
  }
}

void LexParser::ReplaceSymbol(int symbolId,const  LexSymbDesc &newSymb)
{  
  _parsedSymbols[symbolId]=newSymb;
}
void LexParser::InsertSymbol(int before,const  LexSymbDesc &newSymb)
{
  _parsedSymbols.Insert(before,newSymb);
}
void LexParser::InsertSymbols(int before, const Array<LexSymbDesc> &symbList, bool setEnableStore, bool enableStore)
{
  _parsedSymbols.InsertMultiple(before,symbList.Size());
  for (int i=0;i<symbList.Size();i++) 
  {
    _parsedSymbols[i+before]=symbList[i];
    if (setEnableStore)
      _parsedSymbols[i+before].EnableStore(enableStore);
  }
}
void LexParser::DeleteSymbol(int symbolId, int countSymbols)
{
  _parsedSymbols.Delete(symbolId,countSymbols);
}

int LexParser::PackToPlainText(int symbolId,const Array<SymbolType> &termSymbol, bool trim)
{
  LexSymbDesc &cur=_parsedSymbols[symbolId];
  LexSymbDesc *prev=0;
  if (cur.GetSymbolType()==symQuotedText) return 0;
  cur=LexSymbDesc(cur,symPlainText);
  int cnt=0;
  while (symbolId+1+cnt<_parsedSymbols.Size())
  {
    LexSymbDesc &nx=_parsedSymbols[symbolId+1+cnt];
    if (nx.GetSymbolType()==symQuotedText) break;    
    for (int i=0;i<termSymbol.Size();i++) if (nx.GetSymbolType()==termSymbol[i]) goto out; 
    if (prev) cur=LexSymbDesc(cur,*prev);
    prev=&nx;
    cnt++;
  }
out:;
  if (prev) 
  {
    if(!trim || prev->GetSymbolType()!=symWhiteSpaces && prev->GetSymbolType()!=symNewLine)
      cur=LexSymbDesc(cur,*prev);
    else
      cnt--;
  }

  if (cnt!=0)
    _parsedSymbols.Delete(symbolId+1,cnt);
  return cnt;
}

void LexParser::ProcessInclude(ILexParserIncludeProcessor *processor)
{
  for (int i=0;i<_parsedSymbols.Size();i++)
  {
    if (_parsedSymbols[i].GetSymbolType()==symInclude)
    {
      RString name=_parsedSymbols[i+1].GetSymbolText();
      AutoArray<LexSymbDesc> data=processor->ProcessInclude(name);
      InsertSymbols(i,data,true,false);
      i+=data.Size();
    }
  }
}

AutoArray<LexSymbDesc> DefaultIncludeProcessor::ProcessInclude(const RString filename)
{ 
  LexParser fl;
  RString fname=basePath+filename;
  if (_processed)
  {
    if (_processed->Find(fname)!=-1) return AutoArray<LexSymbDesc>();
    else _processed->Add(filename);
  }
  ifstream in(fname,ios::in);
  if (!in) return AutoArray<LexSymbDesc>();
  bool ok=fl.LoadFile(in);
  if (!ok) return AutoArray<LexSymbDesc>();
  const char *c=Pathname::GetNameFromPath(fname);  
  DefaultIncludeProcessor newproc(fname.Mid(0,c-fname.Data()),_processed);
  fl.ProcessInclude(&newproc);
  return fl.GetFileSymbols();
}

void LexParser::ProcessIncludeDefault(const RStringI &startPath)
{
  FindArray<RStringI> processed;
  DefaultIncludeProcessor p(startPath,&processed);
  ProcessInclude(&p);
}