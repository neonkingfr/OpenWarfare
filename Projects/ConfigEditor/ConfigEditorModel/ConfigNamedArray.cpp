#include "StdAfx.h"
#include ".\confignamedarray.h"
#include "ConfigClass.h"

ConfigNamedArray::ConfigNamedArray(ConfigStructure &owner):ConfigArray(owner)
{
}

static SymbolType termSymb[]=
{
    symLeftBracket,
    symRightBracket,
    symAssign,
    symComma,
    symColon,
    symSemicolon,
    symQuote,
    symBlockComment,
    symLineComment,
    symDefine,    
    symInclude,


};



bool ConfigNamedArray::LoadHeader(int index, int start, int &end, RStringI &subname)
{
  _values=0;
  RString name;
  ConfigStructure &owner=GetOwner();
  int cnt=owner.CountSymbols();
  while (start<cnt)
  {
//    owner.PackToPlainText(start,Array<SymbolType>(termSymb,lenof(termSymb)),true,true);
    cnt=owner.CountSymbols();
    if (owner.GetSymbol(start).GetSymbolType()==symWord)
    {
      name=owner.GetSymbol(start).GetSymbolText();
      start=owner.SkipBlindSymbols(start+1);
      if (start<cnt &&owner.GetSymbol(start).GetSymbolType()==symLeftBracket)
      {
        start++;
        if (start<cnt &&owner.GetSymbol(start).GetSymbolType()==symRightBracket)
        {
          start=owner.SkipBlindSymbols(start+1);
          if (start<cnt &&owner.GetSymbol(start).GetSymbolType()==symAssign)
          {
            start=owner.SkipBlindSymbols(start+1);
            if (start<cnt &&owner.GetSymbol(start).GetSymbolType()==symLeftBrace)
            {
              end=start+1;
              subname=name;
              return true;
            }
          }
        }
      }
    }
    start++;
  }
  return false;
}


bool ConfigNamedArray::LoadSubItems(int start, int &end)
{
  ConfigStructure &owner=GetOwner();

  ConfigArray::LoadSubItems(start,end);
  int cnt=owner.CountSymbols();
  start=end;
  if (start<cnt &&owner.GetSymbol(start).GetSymbolType()==symRightBrace)
    start++;
  if (start<cnt &&owner.GetSymbol(start).GetSymbolType()==symSemicolon)
    start++;
  end=start;
  return true;
}

IConfigEntry::ServiceResult ConfigNamedArray::SetName(const RStringI &str)
{
  ConfigStructure &owner=GetOwner();
  if (GetParent()->FindSubEntry(str,false)) return srDuplicate;

  const LexSymbDesc &cur=owner.GetSymbol(GetFirstSymbol());
  if (cur.IsStoreEnabled())
  {
    owner.ReplaceSymbol(GetFirstSymbol(),LexSymbDesc(symPlainText,str,true));
    Reload();
    return srOk;
  }
  else
  {
    return srReadOnly;
  }
}

RStringI ConfigNamedArray::GetName(ServiceResult *res) const
{
  const ConfigStructure &owner=GetOwner();
  const LexSymbDesc &cur=owner.GetSymbol(GetFirstSymbol());
  return Res(res,srOk),cur.GetSymbolText();
}

IConfigEntry *ConfigNamedArray::CloneTo(IConfigEntry *newParent, IConfigEntry *before) const
{
  ConfigClass *nwpr=dynamic_cast<ConfigClass *>(newParent);
  if (nwpr==0) return 0;
  IConfigEntry *res=nwpr->AddArray(GetName(),before,true);
  if (res)
    ForEachNoInherit(CloneFunctor(res),false);

  return res;
}