#include "StdAfx.h"
#include ".\configclass.h"


ConfigClass::ConfigClass(ConfigStructure &owner):ConfigItemBase(owner),_values(0),_base(0)
{  
}



bool ConfigClass::LoadHeader(int index, int start, int &end, RStringI &subname)
{
  _values=0;
  _baseSymbolId=0;
  _base=0;
  _searchBase=0;
  ConfigStructure &owner=GetOwner();
  int cnt=owner.CountSymbols();
  if (start<cnt && owner.GetSymbol(start).GetSymbolText()=="class")
  {
    start=owner.SkipBlindSymbols(start+1);
    if (start<cnt && owner.GetSymbol(start).GetSymbolType()==symWord)
    {
      subname=owner.GetSymbol(start).GetSymbolText();
      _nameId=start;
      _baseSymbolId=start+1;
      start=owner.SkipBlindSymbols(start+1);
      if (start<cnt)
      {
        if (owner.GetSymbol(start).GetSymbolType()==symColon)
        {
          start=owner.SkipBlindSymbols(start+1);
          if (start<cnt && owner.GetSymbol(start).GetSymbolType()==symWord)
          {
            _searchBase=owner.GetSymbol(start).GetSymbolText();
            start=owner.SkipBlindSymbols(start+1);
          }          
        }
        else if (owner.GetSymbol(start).GetSymbolType()==symSemicolon)
        {
          end=start;
          _values=-1;
          return true;
        }
        if (start<cnt && owner.GetSymbol(start).GetSymbolType()==symLeftBrace)
        {
          int x=_baseSymbolId;
          while (x<start && owner.GetSymbol(x).GetSymbolType()!=symWord) x++;
          if (x==start) x=_baseSymbolId;else x++;
          _baseEndId=x;
          start=owner.SkipBlindSymbols(start+1);
          end=start;
          return true;
        }
      }
    }
  }
  return false;
}

bool ConfigClass::LoadSubItems(int start, int &end)
{
  ConfigStructure &owner=GetOwner();
  int cnt=owner.CountSymbols();
  if (_values==-1) {end=start;return true;}
  int pos=0;
  if (_searchBase.GetLength()!=0)  
    _base=SearchBase(_searchBase);
  while (start<cnt && owner.GetSymbol(start).GetSymbolType()!=symRightBrace)
  {
    if (owner.GetSymbol(start).GetSymbolText()=="class")
    {
      ConfigItemBase *p=static_cast<ConfigItemBase *>(owner.Factory().CreateClass(owner));
      int newEnd=p->LoadStructure(pos++,start,this);
      if (newEnd!=-1)
      {
        _values++; 
        start=newEnd;
        cnt=owner.CountSymbols();
        if (start<cnt && owner.GetSymbol(start).GetSymbolType()==symRightBrace)
          start++;
      }
      else
        p->Release();
    }
    else if (owner.GetSymbol(start).GetSymbolType()==symDefine)
    {
      ConfigItemBase *p=static_cast<ConfigItemBase *>(owner.Factory().CreateDefinition(owner));
      int newEnd=p->LoadStructure(pos++,start,this);
      if (newEnd!=-1)
      {
        _values++;
        start=newEnd;
        cnt=owner.CountSymbols();        
      }
      else 
      {
        p->Release();
        start=owner.SkipBlindSymbols(start);
      }
    } 
    else
    {  

      int x=start;
      SymbolType s;
      while(x<cnt && (s=owner.GetSymbol(x).GetSymbolType())!=symAssign && s!=symLeftBracket && s!=symSemicolon && s!=symRightBrace) x++;
      if (x>=cnt) 
      {
        start=owner.SkipBlindSymbols(start,false);;
        continue;
      }
      if (s==symRightBrace) 
      {
        start=owner.SkipBlindSymbols(start,false);;
        continue;
      }
      if (s==symSemicolon) 
      {
        start=owner.SkipBlindSymbols(x+1,false);; 
        continue;
      }
      if (s==symAssign)
      {
        //normal value;
        ConfigItemBase *p=static_cast<ConfigItemBase *>(owner.Factory().CreateValue(owner));
        int newEnd=p->LoadStructure(pos++,start,this);
        if (newEnd!=-1)
        {
          _values++;
          start=newEnd;
        }
        else
        {
          p->Release();
          start=x+1;
        }
      }
      else if (s==symLeftBracket)
      {
        //array value
        //normal value;
        ConfigItemBase *p=static_cast<ConfigItemBase *>(owner.Factory().CreateNamedArray(owner));
        int newEnd=p->LoadStructure(pos++,start,this);
        if (newEnd!=-1)
        {
          _values++;
          start=newEnd;
        }
        else
          p->Release();
      }
    }
    start=owner.SkipBlindSymbols(start,false);
    cnt=owner.CountSymbols();
    if (start<cnt && owner.GetSymbol(start).GetSymbolType()==symSemicolon)
      start=owner.SkipBlindSymbols(start+1,false);
  }
  end=start+1;
  return true;
}

bool ConfigRootClass::LoadHeader(int index, int start, int &end, RStringI &subname)
{
  ConfigStructure &owner=GetOwner();
  subname="$";
  start=owner.SkipBlindSymbols(start,false);
  end=start;
  return true;
}

IConfigEntry *ConfigClass ::SearchBase(const RStringI &baseName) const
{
  IConfigEntry *parent=GetParent();  
  while (parent)
  {
    IConfigEntry *b=parent->FindSubEntry(baseName,false);
    if (b==this || b==0 || b->GetFilePosition()>GetFilePosition())
    {
      IConfigEntry *q=parent->GetBase();
      if (q) b=q->FindSubEntry(baseName,true);
    }
    if (b) return b;
    parent=parent->GetParent();
  }
  return 0;
}

RStringI ConfigClass::GetBaseName(ServiceResult *res) const
{
  Res(res,srOk);
  if (_base) return GetOwner().GetSymbol(_baseSymbolId+1).GetSymbolText();
  else return 0;
}

IConfigEntry::ServiceResult ConfigClass ::SetBase(RStringI baseName)
{
  if (_values==-1)
  {
    LinkToFullClass();
    Reload();
    return SetBase(baseName);
  }
  ConfigStructure &owner=GetOwner();
  if (baseName.GetLength()!=0 && !owner.IsAValidIdentifier(baseName)) return srNotValidName;
  if (!owner.GetSymbol(GetFirstSymbol()).IsStoreEnabled()) return srReadOnly;
  if (baseName.GetLength()==0)
  {
    if (_baseSymbolId==_baseEndId) return srOk;
    _base=0;
    owner.DeleteSymbol(_baseSymbolId,_baseEndId-_baseSymbolId);    
  }
  else
  {
    if (_baseEndId-_baseSymbolId>1)
    {
      owner.ReplaceSymbol(_baseSymbolId,LexSymbDesc(symColon,": ",true));
      owner.ReplaceSymbol(_baseSymbolId+1,LexSymbDesc(symWord,baseName,true));
    }
    else 
    {
      if (_baseEndId!=_baseSymbolId)
      {
        owner.DeleteSymbol(_baseSymbolId,_baseEndId-_baseSymbolId);     
      }
      LexSymbDesc x[2];
      x[0]=LexSymbDesc(symColon,": ",true);
      x[1]=LexSymbDesc(symWord,baseName,true);
      owner.InsertSymbol(_baseSymbolId,Array<LexSymbDesc>(x,2));
    }
  }
  Reload();
  return srOk;
}

void ConfigClass ::ShiftSymbolIndexes(int start, int shift)
{
  if (_baseSymbolId>=start && shift>0 || _baseSymbolId>start) _baseSymbolId+=shift;
  if (_baseEndId>=start) _baseSymbolId+=shift;
  if (_nameId>=start) _nameId+=shift;
  ConfigItemBase::ShiftSymbolIndexes(start,shift);

}

void ConfigClass::LinkToFullClass()
{
  ConfigStructure &owner=GetOwner();
  if (_values==-1)
  {
    LexSymbDesc x[4];
    x[0]=LexSymbDesc(symNewLine,"\n",true);
    x[1]=LexSymbDesc(symWhiteSpaces,owner.CreateIdent(GetIdent()-1),true);
    x[2]=LexSymbDesc(symLeftBrace,"{",true);
    x[3]=LexSymbDesc(symRightBrace,"}",true);
    owner.InsertSymbol(GetTermSymbol(),Array<LexSymbDesc>(x,lenof(x)));
  }

}

int ConfigClass ::PerformAddChecks(const RStringI &name, IConfigEntry *before, ServiceResult *res)
{
  ConfigStructure &owner=GetOwner();
  if (!owner.GetSymbol(GetFirstSymbol()).IsStoreEnabled()) return Res(res,srReadOnly),-1;
  if (!owner.IsAValidIdentifier(name)) return Res(res,srNotValidName),-1;
  IConfigEntry *nw=FindSubEntry(name,false);
  if (nw!=0) return Res(res,srDuplicate),-1;
  LinkToFullClass();
  int beforeId=GetTermSymbol()-1;
  if (before!=0)
    beforeId=before->GetFilePosition();
  if (beforeId<GetFirstSymbol() || beforeId>=GetTermSymbol()) return Res(res,srError),-1;
  return Res(res,srOk),beforeId;
}

IConfigEntry *ConfigClass ::AddClass(RStringI name, IConfigEntry *before /* =0  */,ServiceResult *res /* =0  */)
{  
  ConfigStructure &owner=GetOwner();
  int beforeId=PerformAddChecks(name,before,res);
  if (beforeId==-1) return 0;
  LexSymbDesc x[13];
  RString ident=owner.CreateIdent(GetIdent());
  RString endident=before==0?owner.CreateIdent(GetIdent()-1):ident;
  x[0]=LexSymbDesc(symWhiteSpaces,owner.CreateIdent(1),true);
  x[1]=LexSymbDesc(symWord,"class",true);
  x[2]=LexSymbDesc(symWhiteSpaces," ",true);
  x[3]=LexSymbDesc(symWord,name,true);
  x[4]=LexSymbDesc(symNewLine,"\n",true);
  x[5]=LexSymbDesc(symWhiteSpaces,ident,true);
  x[6]=LexSymbDesc(symLeftBrace,"{",true);
  x[7]=LexSymbDesc(symNewLine,"\n",true);
  x[8]=LexSymbDesc(symWhiteSpaces,ident,true);
  x[9]=LexSymbDesc(symRightBrace,"}",true);
  x[10]=LexSymbDesc(symSemicolon,";",true);
  x[11]=LexSymbDesc(symNewLine,"\n",true);
  x[12]=LexSymbDesc(symWhiteSpaces,endident,true);
  owner.InsertSymbol(beforeId,Array<LexSymbDesc>(before?x+1:x,lenof(x)+(before?-1:0)));
  Reload();
  return Res(res,srOk),FindSubEntry(name,false);
}


IConfigEntry *ConfigClass::AddValue(RStringI name, IConfigEntry *before/* =0 */,ServiceResult *res/* =0 */)
{
  ConfigStructure &owner=GetOwner();
  int beforeId=PerformAddChecks(name,before,res);
  if (beforeId==-1) return 0;
  LexSymbDesc x[7];
  RString eidnt=before==0?owner.CreateIdent(GetIdent()-1):owner.CreateIdent(GetIdent());
  x[0]=LexSymbDesc(symWhiteSpaces,owner.CreateIdent(1),true);
  x[1]=LexSymbDesc(symWord,name,true);
  x[2]=LexSymbDesc(symAssign,"=",true);
  x[3]=LexSymbDesc(symWord,"0",true);
  x[4]=LexSymbDesc(symSemicolon,";",true);
  x[5]=LexSymbDesc(symNewLine,"\n",true);
  x[6]=LexSymbDesc(symWhiteSpaces,eidnt,true);
  owner.InsertSymbol(beforeId,Array<LexSymbDesc>(before?x+1:x,lenof(x)+(before?-1:0)));
  Reload();
  return Res(res,srOk),FindSubEntry(name,false);
}

IConfigEntry *ConfigClass::AddArray(RStringI name, IConfigEntry *before /* =0  */, bool newline /* =true */,ServiceResult *res /* =0  */)
{
  ConfigStructure &owner=GetOwner();
  int beforeId=PerformAddChecks(name,before,res);
  if (beforeId==-1) return 0;
  RString eidnt=before==0?owner.CreateIdent(GetIdent()-1):owner.CreateIdent(GetIdent());
  if (newline)
  {
    RString ident=owner.CreateIdent(GetIdent());
    LexSymbDesc x[14];
    x[0]=LexSymbDesc(symWhiteSpaces,owner.CreateIdent(1),true);
    x[1]=LexSymbDesc(symWord,name,true);
    x[2]=LexSymbDesc(symLeftBracket,"[",true);
    x[3]=LexSymbDesc(symRightBracket,"]",true);
    x[4]=LexSymbDesc(symAssign,"=",true);
    x[5]=LexSymbDesc(symNewLine,"\n",true);
    x[6]=LexSymbDesc(symWhiteSpaces,ident,true);
    x[7]=LexSymbDesc(symLeftBrace,"{",true);
    x[8]=LexSymbDesc(symNewLine,"\n",true);
    x[9]=LexSymbDesc(symWhiteSpaces,ident,true);
    x[10]=LexSymbDesc(symRightBrace,"}",true);
    x[11]=LexSymbDesc(symSemicolon,";",true);
    x[12]=LexSymbDesc(symNewLine,"\n",true);
    x[13]=LexSymbDesc(symWhiteSpaces,eidnt,true);
    owner.InsertSymbol(beforeId,Array<LexSymbDesc>(before?x+1:x,lenof(x)+(before?-1:0)));
  }
  else
  {  
    LexSymbDesc x[10];
    x[0]=LexSymbDesc(symWhiteSpaces,owner.CreateIdent(1),true);
    x[1]=LexSymbDesc(symWord,name,true);
    x[2]=LexSymbDesc(symLeftBracket,"[",true);
    x[3]=LexSymbDesc(symRightBracket,"]",true);
    x[4]=LexSymbDesc(symAssign,"=",true);
    x[5]=LexSymbDesc(symLeftBrace,"{",true);
    x[6]=LexSymbDesc(symRightBrace,"}",true);
    x[7]=LexSymbDesc(symSemicolon,";",true);
    x[8]=LexSymbDesc(symNewLine,"\n",true);
    x[9]=LexSymbDesc(symWhiteSpaces,eidnt,true);
    owner.InsertSymbol(beforeId,Array<LexSymbDesc>(before?x+1:x,lenof(x)+(before?-1:0)));
  }
  Reload();
  return Res(res,srOk),FindSubEntry(name,false);
}


IConfigEntry *ConfigClass::AddClassLink(RString name, IConfigEntry *before ,ServiceResult *res )
{
  ConfigStructure &owner=GetOwner();
  int beforeId=PerformAddChecks(name,before,res);
  if (beforeId==-1) return 0;
  LexSymbDesc x[5];
  x[0]=LexSymbDesc(symWord,"class",true);
  x[1]=LexSymbDesc(symWhiteSpaces," ",true);
  x[2]=LexSymbDesc(symWord,name,true);
  x[3]=LexSymbDesc(symSemicolon,";",true);
  x[4]=LexSymbDesc(symNewLine,"\n",true);
  owner.InsertSymbol(beforeId,Array<LexSymbDesc>(x,lenof(x)));
  Reload();  
  return Res(res,srOk),FindSubEntry(name,false);
}

IConfigEntry::ServiceResult ConfigClass::Delete(IConfigEntry *what)
{
  ConfigStructure &owner=GetOwner();
  int pos=what->GetFilePosition();
  if (pos<GetFirstSymbol() || pos>=GetTermSymbol()) return srError;
  if (!owner.GetSymbol(GetFirstSymbol()).IsStoreEnabled()) return srReadOnly;
  static_cast<ConfigItemBase *>(what)->Delete(false);
  int q=pos;
  while (q>0 && owner.GetSymbol(q-1).GetSymbolType()==symWhiteSpaces) q--;
  pos=owner.SkipBlindSymbols(pos);
  if (pos<owner.CountSymbols() && owner.GetSymbol(pos).GetSymbolType()==symSemicolon)
    pos++;
  while (pos<owner.CountSymbols() && (owner.GetSymbol(pos).GetSymbolType()==symWhiteSpaces || 
    owner.GetSymbol(pos).GetSymbolType()==symBlockComment || 
    owner.GetSymbol(pos).GetSymbolType()==symLineComment))
  {
    pos++;
  }
  if (pos<owner.CountSymbols() && owner.GetSymbol(pos).GetSymbolType()==symNewLine)
    pos++;

  if (pos!=q) owner.DeleteSymbol(q,pos-q);
  Reload();
  return srOk;
}

RStringI ConfigClass::GetName(ServiceResult *res) const
{  
  const ConfigStructure &owner=GetOwner();
  const LexSymbDesc &symb=owner.GetSymbol(_nameId);
  return Res(res,srOk),symb.GetSymbolText();
}

IConfigEntry::ServiceResult ConfigClass::SetName(RStringI name)
{
  ConfigStructure &owner=GetOwner();
  if (!owner.IsAValidIdentifier(name)) return srNotValidName;
  IConfigEntry *parent=GetParent();
  if (parent)
  {
    if (parent->FindSubEntry(name)) return srDuplicate;
  }
  const LexSymbDesc &symb=owner.GetSymbol(_nameId);
  if (symb.IsStoreEnabled())
  {
    owner.ReplaceSymbol(_nameId,LexSymbDesc(symWord,name,true));
    Reload();
    return srOk;
  }
  else
    return srReadOnly;
}

IConfigEntry::ServiceResult ConfigClass::MoveItem(IConfigEntry *what, IConfigEntry *before, bool copy)
{
  if (what==0) return srError;
  RStringI myname=GetFullName();
  RStringI otherName=what->GetFullName();    
  if (strnicmp(myname,otherName,otherName.GetLength())==0) return srError; //cannot handle, inserting item that is my parent
  ConfigStructure *owner=&GetOwner();
  ConfigStructure *srcowner=&(static_cast<ConfigItemBase *>(what)->GetOwner());
  int pos=what->GetFilePosition();
  if (owner==srcowner)
  {

    if ((pos<GetFirstSymbol() || pos>=GetTermSymbol()))
    {
      if (copy) return srDuplicate;
    }
    else
    {
      RStringI subname=what->GetName();
      if (subname.GetLength()==0) return srError;
      if (FindSubEntry(subname,false)) return srDuplicate;
    }
  }
  ServiceResult res;
  int beforeId=PerformAddChecks("__unique000",before,&res);
  if (res!=srOk) return res;

  IConfigEntry *parentWhat=what->GetParent();

  const Array<LexSymbDesc> &symbs=srcowner->GetSymbols();

  ConfigItemBase *w=static_cast<ConfigItemBase *>(what);
  int sz=w->GetTermSymbol()-w->GetFirstSymbol();
  owner->InsertSymbol(beforeId,Array<LexSymbDesc>(const_cast<Array<LexSymbDesc> &>(symbs).Data()+w->GetFirstSymbol(),sz));
  beforeId+=sz;
  LexSymbDesc semic(symSemicolon,";",true);
  owner->InsertSymbol(beforeId,Array<LexSymbDesc>(&semic,1));  

  if (!copy)
  {
    parentWhat->Delete(what);    
  }
  Reload();
  return srOk;
}

IConfigEntry *ConfigClass::CloneTo(IConfigEntry *newParent, IConfigEntry *before) const
{
  ConfigClass *nwpr=dynamic_cast<ConfigClass  *>(newParent);
  if (nwpr==0) return 0;
  IConfigEntry *it=_values==-1?nwpr->AddClassLink(GetName(),before):
  nwpr->AddClass(GetName(),before);


  if (it!=0)
  {
    it->SetBase(GetBaseName());
    ForEachNoInherit(CloneFunctor(it),false);
  }

  return it;
}