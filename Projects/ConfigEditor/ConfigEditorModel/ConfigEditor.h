#pragma once
#include "lexparser.h"
#include "ConfigStructure.h"
#include "IConfigModel.h"

typedef MVC::DefineMVC<IConfigModel,MVC::MultipleThreads<MultiThread::CriticalSection> > ConfigMVC;


class ConfigEditor: public MVC::Model<ConfigMVC>
{
protected:
  LexParser _parser;
  ConfigStructure _struct;
  RString _name;  
public:

  ConfigEditor();
  ~ConfigEditor(void);


  bool Load(const RString &name);
  bool Save() const;
  bool Save(const RString &as);

  bool Load(std::istream &input, const RString &name=RString());
  bool Save(std::ostream &output) const;

  bool LoadEmpty();

  const IConfigEntry *GetRoot() const;  
  virtual const IConfigEntry *Find(const RStringI &fullName) const;
  virtual const RStringI GetFullName(const IConfigEntry *entry) const;


  IConfigEntry::ServiceResult SetBase(const IConfigEntry *entry, const RStringI &baseName);
  IConfigEntry::ServiceResult SetName(const IConfigEntry *entry, const RStringI &newName);
  IConfigEntry::ServiceResult SetValue(const IConfigEntry *entry, const RString &value);
  CombinedResult AddValue(const IConfigEntry *subj, const IConfigEntry *before=0, bool newLine=false);
  CombinedResult AddArray(const IConfigEntry *subj, const IConfigEntry *before=0, bool newLine=true);
  CombinedResult AddValue(const IConfigEntry *subj, RStringI name, const IConfigEntry *before=0);
  CombinedResult AddArray(const IConfigEntry *subj, RStringI name, const IConfigEntry *before=0, bool newLine=true);
  CombinedResult AddClass(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0);
  CombinedResult AddClassLink(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0);
  IConfigEntry::ServiceResult Delete(const IConfigEntry *what=0);
  IConfigEntry::ServiceResult EditSymbolStream(int atPos, int toDelete, const Array<LexSymbDesc> &symbols);
  CombinedResult Move(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before=0);
  CombinedResult Copy(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before=0, bool onlyStructure=false);
    
  virtual bool ForEach(const RStringI &mask,const IConfigEnumCallback &functor);


  virtual const IConfigEntry *Find(const IConfigEntry *ent, int index) const
  {
    return ent->FindSubEntry(ConfigStructure::IndexToName(index));
  }

  virtual bool UpdateMe(MVC::View<ConfigMVC> *view);

  void SetConfigName(const RStringI &name);

};
