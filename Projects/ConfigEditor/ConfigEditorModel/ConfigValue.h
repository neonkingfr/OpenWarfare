#pragma once
#include "ConfigItemBase.h"


class ConfigValue : public ConfigItemBase
{
  int _symIdName;
  int _symIdValue;
public:
  ConfigValue(ConfigStructure &owner);

  virtual bool LoadHeader(int index, int start, int &end, RStringI &subname);

  ServiceResult SetValue(RString value);
  ServiceResult SetName(const RStringI &name);
  RStringI GetName(ServiceResult *res=0) const;
  RStringS GetValue(ServiceResult *res=0) const;
  
  void ShiftSymbolIndexes(int start, int shift);
  IConfigEntry *CloneTo(IConfigEntry *newParent, IConfigEntry *before) const;
};
