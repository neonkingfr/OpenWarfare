#include "StdAfx.h"
#include ".\configarrayvalue.h"
#include "ConfigArray.h"

ConfigArrayValue::ConfigArrayValue(ConfigStructure &owner):ConfigItemBase(owner) 
{
}

ConfigArrayValue::~ConfigArrayValue(void)
{
}

static SymbolType termSymbs[]={symComma,symRightBrace,symBlockComment,symLineComment,symNewLine,symWhiteSpaces};

bool ConfigArrayValue::LoadHeader(int index, int start, int &end, RStringI &subname)
{
  ConfigStructure &owner=GetOwner();
  
  owner.PackToPlainText(start,Array<SymbolType>(termSymbs,lenof(termSymbs)),true);
  end=start+1;
  subname=owner.IndexToName(index);
  return true;
}


IConfigEntry::ServiceResult ConfigArrayValue::SetValue(RString val)
{
  ConfigStructure &owner=GetOwner();
  if (owner.NeedQuote(val)) val=owner.QuoteText(val,false);  
  int itm=GetFirstSymbol();
  const LexSymbDesc &s=owner.GetSymbol(itm);
  if (s.IsStoreEnabled())
    owner.ReplaceSymbol(itm,LexSymbDesc(s.GetSymbolType(),val,true));
  else
    return srReadOnly;

  return srOk;
}

RStringS ConfigArrayValue::GetValue(ServiceResult *res) const
{
  const ConfigStructure &owner=GetOwner();
  int itm=GetFirstSymbol();
  const LexSymbDesc &symb=owner.GetSymbol(itm);
  return Res(res,srOk),symb.GetSymbolText();  
}

IConfigEntry *ConfigArrayValue::CloneTo(IConfigEntry *newParent, IConfigEntry *before) const
{
  ConfigArray *nwpr=dynamic_cast<ConfigArray *>(newParent);
  if (nwpr==0) return 0;
  ConfigArrayValue *it=static_cast<ConfigArrayValue *>(nwpr->AddValue(before));
  it->SetValue(GetValue());
}
