#include <windows.h>
#include <stdio.h>

#include "BTree.h"

int main(int argc, char **argv)
  {
  BTree<int> tree(50);
  int p=10;
  tree.Add(p);
  int i;
  DWORD tck=GetTickCount();
  for (i=0;i<10000000;i++)
    tree.Add(i);
  printf("%d\n",GetTickCount()-tck);
  tck=GetTickCount();
  for (i=0;i<1000000;i++)
    {
    int *q=tree.Find(i);
    }
  printf("%d\n",GetTickCount()-tck);
  int *l=tree.Largest();
  int *s=tree.Smallest();
  int cnt=tree.Size();
  BTreeIterator<int> itr(tree);
  itr.BeginFrom(p);
  for (i=0;i<100;i++)
    {
    s=itr.Next();
    if (s==NULL) break;
    printf("%d ",*s);
    }
  return 0;
  }