#pragma once

#include "SdkCommon.h" 

///Oxygene SDK
namespace OxygeneSDK {  

    
    
    ///Object adapter
    /**
      Instance controls objects owner by the Oxygene SDK
    */
    template<class Object>
    class Adapter
    { 
    protected:


        Object *object;     ///< Pointer to the object
        mutable bool isref; ///< when true, object is ref, and will not deleted in destructor
    public:

        enum Owner {owner};
        ///Creates new instance of the given object
        /**
         This will call default constructor of the object. This cannot be used, when
         object has no default constructor
         */
        Adapter(void);

        ///Links constant object with this adapter
        /**
         Links existing instance with adapter and creates adapter instance. Linked
         instance will not destroyed in the destructor

         Best usage of this constructor is to create adapter instance to manipulate
         with the existing DLL-owner object

         @param object object to link.

         @note You should to use const Adapter if the  Adapter instance is created 
         from the const Object
         */
        Adapter(const Object *object);

        Adapter(Object *object, Owner owner);

        ///Creates copy of the object with instance of the adapter
        /**
         @param other Another adapter that contains original object
         @note Copy constructor will always create a full copy. 
         There is no reference sharing and counting.
          If you want to create reference, simply create reference to Adapter object

         @note copy can be created from the ref type adapter
          */
        Adapter(const Adapter &other);
        
        ///Destroys instance of adapter and the object until the adapter is not a ref.
        ~Adapter();

        ///Assignment wrap
        /**
         Function performs original assignment to the object. If both adapters
         are refs, assignemnts between referenced objects are performed.
         @param other object
         */
        Adapter &operator=(const Adapter<Object> &other);

        ///Unlinks nonref object 
        /**
         Unlinked object can be used after adapter is destroyed. Application is
         responsible of destrucion unlinked object. To destroy unlinked object, call
         Adapter::Destroy(object) 
         
         @return pointer to unlinked object
         @note you can anytime recreate Adapter using the one of ref-constructing 
         constructors

         @note Don't call delete operator on unlinked object
         */
        Object *Unlink() const;

        ///Destroyes unlinked object.
        /**
        @param o object to destroy.
        @note don't call delete operator on unlinked object. Using Destroy function
        is the best way, how to delete it.
        */
        static void Destroy(Object *o) {
            Adapter tmp(o,owner); //it will create adapter and destructor will destroy the instance
        }

        operator const Object &() const {return *object;}
        operator Object &() {return *object;}
        operator const Object *() const {return this?object:0;}
        operator Object *() {return this?object:0;}
        const Object * operator ->() const {return object;}
        Object *operator ->() {return object;}        

        bool IsNull() const {return object == 0;}
        bool NotNull() const {return object != 0;}
        bool operator!() const {return object == 0;}
        operator bool () const {return object != 0;}

    };




}

