#pragma once

#pragma warning(disable: 4661)

#include "SdkCommon.h"
#include "Adapter.h"
#include <vector>

#include "Vector.h"
#include "Selection.h"
#include "Face.h"

namespace OxygeneSDK {  

    struct VectorUV {
        float u;
        float v;
        VectorUV(float u, float v):u(u),v(v) {}      
        VectorUV(const ObjektivLib::ObjUVSet_Item &uv);
    };

    class DLLEXTERN UVSet: public Adapter<ObjektivLib::ObjUVSet> {
        typedef Adapter<ObjektivLib::ObjUVSet> Super;
    public:        
        UVSet(const ObjektivLib::ObjUVSet *x):Super(x) {}
        UVSet(ObjektivLib::ObjUVSet *x,Super::Owner owner):Super(x, owner) {}

        UVSet(Mesh &mesh, int id);
        UVSet(const UVSet &other, Mesh &newOwner);

        Mesh GetOwner() const;
        int GetIndex() const;
        void SetIndex(int index);
        void Validate();
        void SetUV(int face, int vs, float u, float v);
        void SetUV(int face, int vs, const VectorUV &uv);

        VectorUV GetUV(int face, int vs) const;
    };

    template<>
    inline Adapter<ObjektivLib::ObjUVSet>::Adapter() {}
};