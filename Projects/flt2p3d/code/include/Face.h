#pragma once

#pragma warning(disable: 4661)

#include "SdkCommon.h"
#include "Adapter.h"
#include <vector>


#include "FaceT.o2.h"


namespace OxygeneSDK {

    class DLLEXTERN Face: public ObjektivLib::FaceT {

        typedef ObjektivLib::FaceT Super;

    public:
        ///Creates access class to given face in given mesh
        Face(Mesh &obj,int faceIndex);
        ///Creates an anonymous face not included into the mesh (but refers it)
        Face(Mesh &obj);
        ///Creates copy. 
        Face(const Face &other);
        ///Converts FaceT to Face
        Face(Super &other);

        ///Copy face parameters
        Face &operator=(const Face &other) {
            Super::operator =(other);return *this;        
        };

        ///Returns face created in mesh
        static Face Add(Mesh &obj);
        ///Creates bound face instance
        static Face Bind(Mesh &obj, int faceIndex);

        const char *GetTexture(int stage=-1);
        const char *GetMaterial(int stage=-1);
        Mesh GetObject();
        const Mesh GetObject() const;

        Vertex GetVertex(int vs) const;
        Vector GetNornalVector(int vs) const;

};
}