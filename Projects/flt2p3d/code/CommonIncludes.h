
#pragma once

// osg includes
#include <osg/Notify>
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/texture2D>
#include <osg/Material>
#include <osg/texEnv>
#include <osg/ref_ptr>
#include <osg/MatrixTransform>
#include <osg/StateAttribute>
#include <osg/StateSet>

#include <osgDB/FileNameUtils>
#include <osgDB/Registry>
#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <osgDB/ReadFile>

#include <osgUtil/triStripVisitor>

// standard includes
#include <stdlib.h>
#include <string.h>
#include <set>
#include <map>
#include <vector>
#include <iostream>

using namespace std;
