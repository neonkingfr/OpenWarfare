<?php
function Content()
{
  global $in_grp,$StrRes,$in_hist;
    
    
  if (!isset($in_hist)) $hist="NOW()";
  else $hist="'$in_hist'";
  $query=new MyQuery("SELECT `group`.*, `user`.`login`, `user`.`id` AS `userid`  FROM `group` 
    LEFT JOIN `user` ON `group`.`bindUser`=`user`.`ID` WHERE `group`.`ID`='$in_grp' 
    AND `group`.`HistCreated`<=$hist AND `group`.`HistExpired`>$hist");
  $query->Next();
  $data=$query->data; 
    
  echo "<form method=\"post\" action=\"?page=grpprop\"> 
  <table class=\"dlg\">
  <caption class=\"dlg\">$StrRes[GroupProperties]: $data[Name]</caption>
  <tr>
  <th class=\"dlg\">$StrRes[GroupName]</th>
  <td class=\"dlg\"><input type=\"text\" name=\"dlg[name]\" value=\"".HTMLChars($data["Name"])."\" size=50></td>
  </tr>
  <tr>
  <th class=\"dlg\">$StrRes[GroupOwner]</th>
  <td class=\"dlg\"><a href=\"?page=showuser&amp;id=$data[userid]\">$data[login]</a></td>
  </tr>
  <tr>
  <th class=\"dlg\">$StrRes[HistCreated]</th>
  <td class=\"dlg\">$data[HistCreated]</td>
  </tr>
  <tr>
  <th class=\"dlg\">$StrRes[HistExpired]</th>
  <td class=\"dlg\">$data[HistExpired]</td>
  </tr> 
  <tr>
  <th class=\"dlg\">$StrRes[GroupFlags]</th>
  <td class=\"dlg\">";
  HTMLFlags('group','flags',$data["flags"],"data[flags]");
  echo "</td>
  </tr> 
  <tr>
  <th class=\"dlg\">$StrRes[GroupAutoExpire]</th>
  <td class=\"dlg\"><input type=\"text\" name=\"dlg[name]\" value=\"$data[expireDays]\" size=5></td>
  </tr> 
  <tr>
  <th class=\"dlg\"><a href=\"?page=grouphistory&amp;id=$data[ID]\">$StrRes[GroupPropShowHistory]</a></th>
  <td class=\"dlg\">
    <input type=\"submit\" name=\"dlg[save]\" value=\"$StrRes[GroupPropSave]\" size=5>&nbsp;&nbsp;&nbsp;&nbsp;";
  if (!isset($in_hist))
  {
    echo "<input type=\"submit\" name=\"dlg[delete]\" value=\"$StrRes[GroupPropDelete]\">";
    echo "<input type=\"hidden\" name=\"hist\" value=\"$in_hist\">";
  }
  else
    echo "<input type=\"submit\" name=\"dlg[undelete]\" value=\"$StrRes[GroupPropUnDelete]\">";
  echo "
  </td>
  </tr> 
  </table>
  
  </form>
  ";
  
  
}
?>
