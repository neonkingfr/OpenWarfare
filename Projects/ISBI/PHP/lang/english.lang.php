<?php

$StrRes=Array("MenuRefresh" => "Refresh",
              "MenuSetup" => "My profile",
              "MenuLogout" => "Logout",
              "YourEmail" => "Your E-mail",
              "YourPosition" => "Your position in company (example: designer)",
              "YourDescription" => "Any description about you:",
              "YourWebPage" => "Your personal web page (optional):",
              "YourImage" => "Your image (JPEG file, 64KB max)",
              "YourName" => "Your full name",
              "Save" => "Save",
              "SetupPage" => "Preferences",
              "PasswordsNotSame" => "Passwords are not the same.",
              "PasswordChanged" => "Note: Password has been changed. Please HIT <u>Refresh</u> in Menu to re-login.",
              "FileTooLong" => "File is too large.",
              "FileMustBeJpeg" => "File must have JPG type",
              "ShowMyProfile" => "Show my profile",
              "ProfileUser" => "Profile of user",
              "UserName" => "User's Name",
              "Email" => "E-mail",
              "Position" => "Position",
              "PersonalWebPage" => "Personal Web Page",
              "UserDescription" => "User's description",
              "LMenuUsers" => "Users",
              "LMenuSigned" => "Signed groups",
              "LMenuGAll" => "All groups",
              "LMenuTasks" => "My Tasks",
              "LMenuMyViews" => "My Views",
              "MenuSignUp" => "Sign Up Groups",
              "SignUpTable" => "Sign up groups",
              "GroupProperties" => "Properties",
              "SignUpButton" => "Save changes",
              "GroupProperties" => "Properties of group",
              "GroupName" => "Group name",
              "GroupOwner" => "Owner",
              "HistCreated" => "Last modification",
              "HistExpired" => "Expiration",
              "GroupFlags" => "Flags",
              "GroupAutoExpire" => "Auto-expiration of articles (days)",
              "GroupPropShowHistory" => "Show history",
              "GroupPropSave" => "Save settings",
              "GroupPropDelete" => "Delete group",
              "GroupPropUnDelete" => "Undelete group"
              
              );

$strRegErrForm="Invalid username, or password was not the same. Registration failed.";
$strRegErrUserExists="Registration failed - login name is already in use";
$strRegistrationComplette="Registration complette";
$strRegistrationLogin="Please <a href=\"index.php\">Login</a> into ISBI now.";
$strRegistrationTitle="Registration into ISBI";

$strRegNewUserName="New login name";
$strRegNewUserPassword="New password";
$strRegNewUserPasswordRetype="New password again";
$strRegSubmit="Register";





?>
