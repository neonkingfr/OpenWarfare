// HistoryWindow.cpp: implementation of the CHistoryWindow class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "HistoryWindow.h"
#include <CommCtrl.h>
#include <shlobj.h>
#include "resource.h"
#include <fstream>
#include "Application.h"

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define HISTORYWINDOW "History"

CHistoryWindow::CHistoryWindow()
  {
  hpath=NULL;
  }










CHistoryWindow::~CHistoryWindow()
  {
  free(hpath);
  }










LRESULT CHistoryWindow::OnInitDialog()
  {
  list=GetDlgItem(IDC_LIST);
  
  LVCOLUMN lcl;
  lcl.mask=LVCF_FMT|LVCF_TEXT|LVCF_SUBITEM|LVCF_WIDTH;
  lcl.fmt=LVCFMT_LEFT;
  lcl.cx=0;
  lcl.pszText="ID";
  lcl.iSubItem=0;
  ListView_InsertColumn(list,0,&lcl);
  lcl.cx=300;
  lcl.pszText=(char *)StrRes[IDS_HISTCOLNAME];
  lcl.iSubItem=1;
  ListView_InsertColumn(list,1,&lcl);
  lcl.cx=100;
  lcl.pszText=(char *)StrRes[IDS_HISTCOLDATE];
  lcl.iSubItem=2;
  ListView_InsertColumn(list,2,&lcl);
  ListView_SetExtendedListViewStyleEx(list,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
  MINMAXINFO mmf;
  GetMinMaxInfo(mmf);
  mmf.ptMinTrackSize.x=300;
  mmf.ptMinTrackSize.y=150;
  SetMinMaxInfo(mmf);
  int p=CDialogClass::OnInitDialog();
  LoadPosition(*this,HISTORYWINDOW,NULL);
  OnReload();
  CheckWindowVisibility(*this);
  return p;
  }










LRESULT CHistoryWindow::OnSize(int cx, int cy, int rx, int ry, int flags)
  {
  HDWP dwp=BeginDeferWindowPos(10);
  MoveWindowRel(list,0,0,rx,ry,dwp);
  MoveWindowRel(GetDlgItem(IDOK),rx,ry,0,0,dwp);
  MoveWindowRel(GetDlgItem(IDCANCEL),rx,ry,0,0,dwp);
  MoveWindowRel(GetDlgItem(IDC_DELETE),0,ry,0,0,dwp);
  MoveWindowRel(GetDlgItem(IDC_OBNOVIT),0,ry,0,0,dwp);
  EndDeferWindowPos(dwp);
  return 1;
  }










LRESULT CHistoryWindow::OnCancel()
  {
  if (fhandle) 
    {FindClose(fhandle);fhandle=NULL;}
  SavePosition(*this,HISTORYWINDOW );
  DestroyWindow(*this);
  return 1;
  }










LRESULT CHistoryWindow::OnOK()
  {
  int cnt=0;
  int nextwarn=1;
  int pos=	ListView_GetNextItem(list,-1,LVNI_SELECTED);
  while (pos!=-1)
    {
    char buff[256];
    ListView_GetItemText(list,pos,0,buff,ArrLength(buff));
    char *bf=(char *)alloca(strlen(hpath)+10+strlen(buff));
    sprintf(bf,"%s\\%s",hpath,buff);
    ShellExecute(*this,NULL,bf,NULL,NULL,SW_SHOWNORMAL);
    pos=ListView_GetNextItem(list,pos,LVNI_SELECTED);
    if (pos!=-1)
      {
      cnt++;
      if (cnt==nextwarn)
        {
        int p=MessageBox(hWnd,StrRes[IDS_OPENHISTWARN],appname,MB_YESNOCANCEL|MB_ICONQUESTION|MB_SYSTEMMODAL);
        if (p==IDCANCEL) return 0;
        if (p==IDNO) break;
        nextwarn*=10;
        }
      }
    }
  if (fhandle) 
    {FindClose(fhandle);fhandle=NULL;}
  SavePosition(*this,HISTORYWINDOW );
  DestroyWindow(*this);
  return 1;
  }










void CHistoryWindow::OnReload()
  {
  WIN32_FIND_DATA fnd;
  ListView_DeleteAllItems(list);
  char fname[256+MAX_PATH];
  SHGetSpecialFolderPath(NULL,fname,CSIDL_APPDATA ,TRUE);
  strcat(fname,"\\");
  strcat(fname,appname);
  free(hpath);
  hpath=strdup(fname);
  strcat(fname,"\\*");  
  strcat(fname,StrRes[IDS_HISTORYEXT]);
  fhandle=FindFirstFile(fname,&fnd);
  ShowWindow(list,SW_HIDE);
  if (fhandle)
    {
    LoadHistoryFileInfo(&fnd);
    while (FindNextFile(fhandle,&fnd)==TRUE)
      {
      LoadHistoryFileInfo(&fnd);
      cnt++;
      if ((cnt & 0xFF)==0) Sort();
      }
    FindClose(fhandle);
    fhandle=NULL;
    Sort();
    cnt=0;
    }
  ShowWindow(list,SW_SHOW);
  }










LRESULT CHistoryWindow::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  switch (wID)
    {
    case IDC_OBNOVIT:OnReload();break;
    case IDC_DELETE:OnDelete();break;
    default: return CDialogClass::OnCommand(wID,wNotifyCode,clt);
    }
  return 1;
  }










LRESULT CHistoryWindow::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  switch (msg)
    {
    case WM_TIMER:
      {
      }
    break;
    case WM_NOTIFY:
      {
      if (wParam==IDC_LIST)
        {
        LPNMLISTVIEW lpnmlv = (LPNMLISTVIEW) lParam;
        if (lpnmlv->hdr.code==NM_DBLCLK) OnOK();
        }
      }
    return 0;
    
    default: return CDialogClass::DlgProc(msg,wParam,lParam);
    }
  return 1;
  }










void CHistoryWindow::LoadHistoryFileInfo(LPWIN32_FIND_DATA dta)
  {
  SYSTEMTIME syst;
  char buff[2048];
  FileTimeToSystemTime(&(dta->ftLastWriteTime),&syst);
  sprintf(buff,"%d.%d.%d %02d:%02d:%02d",syst.wDay,syst.wMonth,syst.wYear,syst.wHour,syst.wMinute,syst.wSecond);
  LVITEM it;
  it.mask=LVIF_TEXT;
  it.iItem=ListView_GetItemCount(list);
  it.iSubItem=0;
  it.pszText=dta->cFileName;
  int p=ListView_InsertItem(list,&it);
  ListView_SetItemText(list,p,2,buff);
  char *bf=(char *)alloca(strlen(hpath)+10+strlen(dta->cFileName));
  sprintf(bf,"%s\\%s",hpath,dta->cFileName);
  ifstream in(bf,ios_base::in);
  if (!in) return;
  unsigned char tmp[3]="  ";
  buff[2047]=0;
  int i;
  for (i=0;i<2047;i++)
  {
    int p=in.get();
    if (isspace(tmp[0])) tmp[0]=32;
    if (i>2) buff[i-3]=tmp[0];    
    tmp[0]=tmp[1];
    tmp[1]=tmp[2];
    tmp[2]=(unsigned char)p;
    if (strncmp((char *)tmp,"\n--",3)==0) break;
  }
  buff[i-3]=0;
  ListView_SetItemText(list,p,1,buff);
  }










void CHistoryWindow::OnDelete()
  {
  int cnt=0;
  int nextwarn=1;
  int pos=	ListView_GetNextItem(list,-1,LVNI_SELECTED);
  if (pos!=-1 && MessageBox(*this,StrRes[IDS_DELETEHISTORY],appname,MB_YESNO)==IDNO) return;
  while (pos!=-1)
    {
    char buff[256];
    ListView_GetItemText(list,pos,0,buff,ArrLength(buff));
    char *bf=(char *)alloca(strlen(hpath)+10+strlen(buff));
    sprintf(bf,"%s\\%s",hpath,buff);
    DeleteFile(bf);
    ListView_DeleteItem(list,pos);
    pos=ListView_GetNextItem(list,-1,LVNI_SELECTED);
    }
  }










int CALLBACK CompareItems(LPARAM lParam1, LPARAM lParam2, 
                          LPARAM lParamSort)
  {
  HWND list=(HWND)lParamSort;
  char buff[256];
  DWORD w1,w2;
  int d,m,r,h,mm,s;
  ListView_GetItemText(list,lParam1,2,buff,256);
  sscanf(buff,"%d.%d.%d %d:%d:%d",&d,&m,&r,&h,&mm,&s);
  w1=s+(mm<<6)+(h<<12)+(d<<17)+(m<<22)+(r<<26);
  ListView_GetItemText(list,lParam2,2,buff,256);
  sscanf(buff,"%d.%d.%d %d:%d:%d",&d,&m,&r,&h,&mm,&s);
  w2=s+(mm<<6)+(h<<12)+(d<<17)+(m<<22)+(r<<26);
  if (w2<w1) return -1;
  if (w2>w1) return 1;
  return 0;
  }










void CHistoryWindow::Sort()
  {
  int cnt=ListView_GetItemCount(list);
  for (int i=0;i<cnt;i++)
    {
    LVITEM it;
    it.mask=LVIF_PARAM ;
    it.iItem=i;
    it.iSubItem=0;
    it.lParam=i;
    ListView_SetItem(list,&it);
    }
  ListView_SortItems(list,CompareItems,(LPARAM)list);
  }




void CHistoryWindow::OpenHistory(HWND hWnd, const ChannelID &id)
{
      char fname[256+MAX_PATH];
      SHGetSpecialFolderPath(NULL,fname,CSIDL_APPDATA ,TRUE);
      strcat(fname,"\\");
      strcat(fname,appname);
      strcat(fname,"\\");
      id.print(strchr(fname,0),256);
      strcat(fname,".txt");

      SHELLEXECUTEINFO nfo;
      memset(&nfo,0,sizeof(nfo));     
      nfo.cbSize=sizeof(nfo);
      nfo.fMask=SEE_MASK_NOCLOSEPROCESS;
      nfo.hwnd=hWnd;
      nfo.lpFile=fname;
      nfo.nShow=SW_NORMAL;


      if (ShellExecuteEx(&nfo)==FALSE)
      {
        MessageBox(hWnd,StrRes[IDS_OPENHISTORYFAILED],appname,MB_OK);
      }
      else
      {
        WaitForInputIdle(nfo.hProcess,INFINITE);
        Sleep(500);
        INPUT input;
        input.type=INPUT_KEYBOARD;
        input.ki.wVk=VK_CONTROL;
        input.ki.wScan=0x1D;
        input.ki.dwFlags=0;
        input.ki.time=GetTickCount();
        input.ki.dwExtraInfo=0;
        SendInput(1,&input,sizeof(input));
        input.ki.wVk=VK_END;
        input.ki.wScan=0x4F;
        SendInput(1,&input,sizeof(input));
        input.ki.dwFlags=KEYEVENTF_KEYUP;
        SendInput(1,&input,sizeof(input));
        input.ki.wVk=VK_CONTROL;
        input.ki.wScan=0x1D;
        SendInput(1,&input,sizeof(input));
     }
}