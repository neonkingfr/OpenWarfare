// RoomListDlg.cpp: implementation of the CRoomListDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "application.h"
#include "resource.h"
#include "RoomListDlg.h"



#define ROOMLIST "RoomList"
#define UNKNOWN "??? @";


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRoomListDlg::CRoomListDlg()
  {
  
  }

//--------------------------------------------------

CRoomListDlg::~CRoomListDlg()
  {
  
  }

//--------------------------------------------------

LRESULT CRoomListDlg::OnInitDialog()
  {
  list=GetDlgItem(IDC_LIST);
  button=GetDlgItem(IDC_CREATEROOM);
  edit=GetDlgItem(IDC_NAME);
  statc=GetDlgItem(IDC_NAMESTATIC);
  
  MINMAXINFO nfo;
  GetMinMaxInfo(nfo);
  nfo.ptMinTrackSize.y=100;
  nfo.ptMinTrackSize.x=120;
  SetMinMaxInfo(nfo);
  CDialogClass::OnInitDialog();
  WINDOWPLACEMENT wp;
  HKEY key=OpenProfileKey();
    {
    ctArchiveInReg arch(key,ROOMLIST);
    if (!(!arch))
      {
      arch.serial(&wp,sizeof(wp));
      wp.showCmd=SW_HIDE;
      SetWindowPlacement(*this,&wp);
      }
    }
  CheckWindowVisibility(*this);
  return TRUE;
  }

//--------------------------------------------------

void CRoomListDlg::SavePosition()
  {
  WINDOWPLACEMENT wp;
  wp.length=sizeof(wp);
  GetWindowPlacement(*this,&wp);
  HKEY key=OpenProfileKey();
    {
    ctArchiveOutReg arch(key,ROOMLIST);
    arch.serial(&wp,sizeof(wp));
    }
  RegCloseKey(key);
  }

//--------------------------------------------------

LRESULT CRoomListDlg::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  switch (msg)
    {
    case WM_EXITSIZEMOVE: 
      {
      LRESULT l=CDialogClass::DlgProc(msg,wParam,lParam);
      SavePosition();
      return l;
      }
    default: return CDialogClass::DlgProc(msg,wParam,lParam);
    }
  return FALSE;
  }

//--------------------------------------------------

LRESULT CRoomListDlg::OnSize(int cx, int cy, int rx, int ry, int flags)
  {
  HDWP dwp=BeginDeferWindowPos(5);
  MoveWindowRel(list,0,0,rx,ry,dwp);
  MoveWindowRel(statc,0,ry,0,0,dwp);
  MoveWindowRel(button,rx,ry,0,0,dwp);
  MoveWindowRel(edit,0,ry,rx,0,dwp);
  EndDeferWindowPos(dwp);
  InvalidateRect(list,NULL,FALSE);
  return 1;
  }

//--------------------------------------------------

LRESULT CRoomListDlg::OnCancel()
  {
  DestroyWindow(*this);
  return 1;
  }

//--------------------------------------------------

LRESULT CRoomListDlg::OnOK()
  {
  return 0;
  }

//--------------------------------------------------

void CRoomListDlg::CheckChannel(ChannelID &chan)
  {
  int i;
  int cnt=ListBox_GetCount(list);
  for (i=0;i<cnt;i++)
    {
    DWORD p=ListBox_GetItemData(list,i);
    if (chanlist[p]==chan) break;
    }
  if (i==cnt)
    {    
    char buff[128]=UNKNOWN;
    chan.print(buff+5,120);
    int pos=ListBox_AddString(list,buff);
    ListBox_SetItemData(list,pos,chanlist.Size());
    chanlist.Add(chan);
    }
  }

//--------------------------------------------------

void CRoomListDlg::CheckChannels(ChannelID *idlist, int count)
  {
  for (int i=0;i<count;i++) CheckChannel(idlist[i]);
  }

//--------------------------------------------------

