// PhoneOverNet.cpp: implementation of the CPhoneOverNet class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "PhoneOverNet.h"
#include "resource.h"
#include "AcmDriver.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWaveInImpl::CWaveInImpl(CPhoneOverNet &owner):
  _owner(owner)
  {
  }

  CWaveOutImpl::CWaveOutImpl(CPhoneOverNet &owner):
  _owner(owner)
  {
  }


CPhoneOverNet::CPhoneOverNet():
  _wavein(*this),_waveout(*this)
  {
  _playcnt=0;  
  }

CPhoneOverNet::~CPhoneOverNet()
  {

  }

int CPhoneOverNet::FindSender(ChannelID &chan)
  {
  for (int i=0;i<_receiver.Size();i++)
	if (_receiver[i].channel==chan) return i;
  return -1;
  }

int CPhoneOverNet::AddNewReceiver(ChannelID &chan)
  {
  SUserVoiceData *data=&_receiver.Append();
  memset(data,0,sizeof(*data));
  data->channel=chan;
  return _receiver.Size()-1;
  }

void CPhoneOverNet::IncomeData(ChannelID &channel, const char *data, int size)
  {
  int rcv=FindSender(channel);
  if (rcv==-1) rcv=AddNewReceiver(channel);
  int outsz=_receiver[rcv].GetSpaceSize();
  if (outsz)
	{
	DWORD used;
    _acmWave->LoadData(data,size);
  	_acmWave->ConvertData((unsigned char *)_receiver[rcv].GetDataSpace(),outsz,used,0);
	_receiver[rcv].blocksize+=used;
	}
  }

int SUserVoiceData::LoadData(const char *data, int size)
  {
  if (blocksize+size>WON_USERPRIVATEBUFFER) size=WON_USERPRIVATEBUFFER-blocksize;
  memcpy(block+blocksize,data,size);
  return 0;
  }

void SUserVoiceData::DataOut(int size)
  {
  if (size>blocksize || size==-1) 
	blocksize=0;
  else
	{
	memcpy(block,block+size,blocksize-size);
	blocksize-=size;
	}
  }

bool SUserVoiceData::IsWaiting()
  {
  return blocksize!=0;
  }

bool CPhoneOverNet::MixBuffer(char *buffer, int size)
  {
  DisplayLevel(IDC_REPLEVEL,(const short *)buffer,size/2,0);
  
  short *sbuff=(short *)buffer;
  int ssize=size/2;
  memset(sbuff,0,size);
  for (int i=0;i<_receiver.Size();i++)
	if (_receiver[i].IsWaiting())
	  {
	  short *udata=(short *)_receiver[i].block;
	  int usize=_receiver[i].blocksize/2;
	  if (usize>ssize) usize=ssize;
  	  for (int j=0;j<usize;j++) sbuff[j]+=udata[j];
	  _receiver[i].DataOut(usize*2);
	  }  
  return true;
  }

bool CWaveInImpl::BufferDrop(char *buff, int size)
  {
  return _owner.MicRead(buff,size);
  }

bool CWaveOutImpl::BufferDrop(char *buff, int size)
  {
  return _owner.MixBuffer(buff,size);
  }

bool CPhoneOverNet::MicRead(const char *buff, int size)
  {
  if (DisplayLevel(IDC_MICLEVEL,(const short *)buff,size/2,(short)SendDlgItemMessage(IDC_DETECT,TBM_GETPOS,0,0))==false) _playcnt--;
  else _playcnt=10;
  if (_playcnt<=5 && _playcnt>0)
	memset(const_cast<char *>(buff),0,size);
  if (_playcnt>0)
	{
	char *temp=(char *)alloca(size);
 
	_acmMp3->LoadData(buff,size);
	DWORD used;
	_acmMp3->ConvertData((unsigned char *)temp,size,used,0);
	  ChannelID chan(theApp->config.majid,theApp->config.minid);
	  IncomeData(chan,temp,used);
  
	// ********** odeslat data do site *****************!
	}
  else _playcnt=0;
  return true;
  }

static WAVEFORMATEX format_wave=
  {
  WAVE_FORMAT_PCM,1,11025,22050,2,16,sizeof(WAVEFORMATEX)
  };

static char format_buffer[100];

static WAVEFORMATEX format_mp3=(WAVEFORMATEX &)*format_buffer;



LRESULT CPhoneOverNet::OnInitDialog()
  {
  MMRESULT mmres;
  _acmWave=new ACMStream;
  _acmMp3=new ACMStream;
  format_mp3.cbSize=sizeof(format_mp3);
  format_mp3.wBitsPerSample=16;
  format_mp3.wFormatTag=0x55;
  format_mp3.nSamplesPerSec=11025;
  format_mp3.nChannels=1;
  acmFormatSuggest(NULL,&format_wave,&format_mp3,100,ACM_FORMATSUGGESTF_WFORMATTAG|ACM_FORMATSUGGESTF_NCHANNELS|ACM_FORMATSUGGESTF_NSAMPLESPERSEC);
  mmres=_acmWave->Open(&format_mp3,&format_wave);
  if (!mmres) mmres=_acmMp3->Open(&format_wave,&format_mp3);
  if (mmres)
	{
	MessageBox(*this, StrRes[IDS_CONVERTDRIVERERROR],appname,MB_OK|MB_ICONSTOP);
	OnCancel();
	return 0;
	}
  _wavein.AllocBuffer(10000);
  _waveout.AllocBuffer(10000);
  mmres=_wavein.Open(AfxGetInstanceHandle(),*this,WAVE_MAPPER,&format_wave);
  if (mmres)
	{
	MessageBox(*this, StrRes[IDS_CAPTUREDRIVEERROR],appname,MB_OK|MB_ICONSTOP);
	OnCancel();
	return 0;
	}
  mmres=_waveout.Open(AfxGetInstanceHandle(),*this,WAVE_MAPPER,&format_wave);
  if (mmres)
	{
	MessageBox(*this, StrRes[IDS_WAVEOUTDRIVEERROR],appname,MB_OK|MB_ICONSTOP);
	OnCancel();
	return 0;
	}
  _waveout.Start(); //prehravani bezi celou dobu behu telefonu
  SendDlgItemMessage(IDC_MICLEVEL,PBM_SETRANGE,0,MAKELPARAM(0,30000));
  SendDlgItemMessage(IDC_REPLEVEL,PBM_SETRANGE,0,MAKELPARAM(0,30000));
  SendDlgItemMessage(IDC_DETECT,TBM_SETRANGE,1,MAKELPARAM(0,30000));
  SendDlgItemMessage(IDC_DETECT,TBM_SETPOS,TRUE,4000);
  return 0;
  }

LRESULT CPhoneOverNet::OnOK() {return 1;}

LRESULT CPhoneOverNet::OnCancel()
  {
  _wavein.Close();
  _waveout.Close();
  _acmMp3->Close();
  _acmWave->Close();
  delete _acmMp3;
  delete _acmWave;
  DestroyWindow(*this);
  return 0;
  }

LRESULT CPhoneOverNet::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  switch (wID)
	{
	case IDC_SPEAK:
	  if (IsDlgButtonChecked(*this,IDC_SPEAK)==BST_CHECKED)
		_wavein.Start();
	  else
		_wavein.Stop();
	  return 1;	  
	default: return CDialogClass::OnCommand(wID,wNotifyCode,clt);
	}
  }

bool CPhoneOverNet::DisplayLevel(int idc, const short *buffer, int samples, short cmplevel)
  {
  short minval=0;
  for (int i=0;i<samples;i++) 
	{
	short cur=abs(buffer[i]);
	if (cur>minval) minval=cur;
	}
  SendDlgItemMessage(idc,PBM_SETPOS,minval,0);
  return minval>cmplevel;
  }
