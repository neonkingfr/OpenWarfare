// ChatWindow.h: interface for the CChatWindow class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHATWINDOW_H__E3686A02_ABB2_4EBD_B2C4_6BDBAE665F9C__INCLUDED_)
#define AFX_CHATWINDOW_H__E3686A02_ABB2_4EBD_B2C4_6BDBAE665F9C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"
#include "HistoryFile.h"	// Added by ClassView
#include <ole2.h>
#undef LOG

int geDefineBinMove(ChannelID);

#define WINMODE_POPUP 0
#define WINMODE_BLINK 1
#define WINMODE_HIDDEN 2
#define WINMODE_CLOSE 3

class CChatWindowList;
class CChatWindow ;

#define CCHW_DELIVERYSTATUS (WM_APP+15)


class CChatWindowDrop:public IDropTarget 
{
  int refcnt;
  CChatWindow *_registred;
  HWND _regWindow;
  FORMATETC format;
  FORMATETC filedrop;
  bool _noedit;
public:
  virtual HRESULT STDMETHODCALLTYPE QueryInterface( 
                /* [in] */ REFIID riid,
                /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
  virtual ULONG STDMETHODCALLTYPE AddRef( void);
  virtual ULONG STDMETHODCALLTYPE Release( void);
  virtual HRESULT STDMETHODCALLTYPE DragEnter( 
            /* [unique][in] */ IDataObject *pDataObj,
            /* [in] */ DWORD grfKeyState,
            /* [in] */ POINTL pt,
            /* [out][in] */ DWORD *pdwEffect);
        
  virtual HRESULT STDMETHODCALLTYPE DragOver( 
            /* [in] */ DWORD grfKeyState,
            /* [in] */ POINTL pt,
            /* [out][in] */ DWORD *pdwEffect);
        
  virtual HRESULT STDMETHODCALLTYPE DragLeave( void);
        
  virtual HRESULT STDMETHODCALLTYPE Drop( 
            /* [unique][in] */ IDataObject *pDataObj,
            /* [in] */ DWORD grfKeyState,
            /* [in] */ POINTL pt,
            /* [out][in] */ DWORD *pdwEffect);
  CChatWindowDrop(CChatWindow *reg,HWND regWnd, bool noedit);
  void Revoke();
  void ChangeRegisterChatWindow(CChatWindow *registred) {_registred=registred;}
};

class CChatWindow : public CDialogClass
  {
  HWND riched;
  HBRUSH brs;
  bool blk;
  char *inviteusr;	//osloveni novych lidi
  char *invitegrp;	//osloveni novych skupin
  bool hidden;
  int _separator;
  bool _writntfsent; //true, if first writting notify has been sent
  CChatWindowDrop *_drop;
  CChatWindowDrop *_drop2;
  /*CChatWindowDrop *_drop3;*/
  NOTIFYICONDATA nid;

  public:
    void UpdateUserStatus();
    void MoveSeparator(LPARAM lParam);
    bool OnSeparator(POINT &pt);
    void BeginMove(LPARAM lParam);
    char *soundgrp;
    bool Alone();
    void TaskMenu();
    void ToTaskBar(bool state);
    void SendOpenForNew();
    void InviteUsersGroups(int *users, int *groups);
    void SystemMessage(const char *text);
    void InitTopic(ChannelID id,const  char *topic, const char *from);
    CHistoryFile history;
    void SavePosition();
    void SetTopic(const char *t);
    LRESULT OnDrawListBoxItem(LPDRAWITEMSTRUCT lpdis);
    static void Move(CChatWindow *src, CChatWindow *trg);
    void OnBlinkTimer();
    void StopBlink();
    void StartBlink();
    CChatWindowList *owner;
    void CheckUser(char *username, bool present);
    void ReceiveText(const char *from, const char *text, bool myself, bool nohistory=false);
    LRESULT OnCancel();
    LRESULT OnOK();
    LRESULT OnInitDialog();
    ChannelID chan;
    char *topic;  
    bool peerchannel;
    bool peerlaststate;
    int msgid;
    
    virtual LRESULT OnSize(int cx, int cy, int rx, int ry, int flags);
    virtual LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);		
    virtual LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);

    void DropText(const char *text);

    void StartBlinkTyping();
    void StopBlinkTyping();
    
    
    LRESULT OnNotifyRichEditLink(ENLINK *lnk);
    CChatWindow();
    virtual ~CChatWindow();
  
    void PeerNotifyState(int cmd);
    void SendPeerNotify(int notify);


  };

//--------------------------------------------------

typedef SRef<CChatWindow> PChatWindow;
TypeIsMovable(PChatWindow);

  class CChatWindowList:public AutoArray<PChatWindow>
    {
    public:
      CChatWindow * GetDropTarget(POINT &pt);
      void OnDragSetCursor(POINT &screenpt);
      void CheckUserPeer(int userpos);
      void OpenPeerWindow(ChannelID chanel, char *topic, char *from,  bool myself);
      bool CheckChannel(ChannelID chan);
      void OpenChatWindow(ChannelID id, char *topic, char *from, char *sndgrp);
      void CreateListOfChannels(AutoArray<ChannelID>& list);
      void DestroyChatWindow(CChatWindow *ww);      
      ChannelID CreateChannel(int *users, int *groups);
      void CheckUser(char *username, ChannelID *idlist, int count);
      CChatWindow *ReceiveText(const char *from, const char *text, bool myself, ChannelID id);
      BOOL PreTranslateMessage(MSG *msg);
      CChatWindow *PresetText(ChannelID chan, const char *text);
      void PeerNotifyState(ChannelID nchn,int cmd);
    };

//--------------------------------------------------

#endif // !defined(AFX_CHATWINDOW_H__E3686A02_ABB2_4EBD_B2C4_6BDBAE665F9C__INCLUDED_)
