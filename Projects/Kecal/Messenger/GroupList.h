// GroupList.h: interface for the CGroupList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GROUPLIST_H__2639CA74_6769_44BB_A115_E65FA6791F20__INCLUDED_)
#define AFX_GROUPLIST_H__2639CA74_6769_44BB_A115_E65FA6791F20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define GROUPINFOVERSION 0x102	

struct SGroupInfo
  {
  char *groupname;
  char *owner;
  char *sound;
  AutoArray<ChannelID>poeple;
  bool privat;
  bool sign;
  bool autosign;
  SGroupInfo() 
    {groupname=owner=sound=NULL;privat=false;}
  void SetGroupInfo(const char *g,const char *o, bool issigned);
  ~SGroupInfo() 
    {free(groupname);free(owner);free(sound);}
  const char *GetName() 
    {return groupname;}
  const char *GetOwner() 
    {return owner;}
  void Serialize(ctArchive &arch);
  friend ctArchive& operator>>=(ctArchive& arch, SGroupInfo &nfo);
  };

//--------------------------------------------------

TypeIsMovable(SGroupInfo);

class CGroupList:public AutoArray<SGroupInfo>
  {
  public:
    CGroupList();
    virtual ~CGroupList();
    
    int FindGroupByOwner(const char *owner);
    int FindGroup(const char *group);
  };

//--------------------------------------------------

#endif // !defined(AFX_GROUPLIST_H__2639CA74_6769_44BB_A115_E65FA6791F20__INCLUDED_)
