// GroupList.cpp: implementation of the CGroupList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "GroupList.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void SGroupInfo::SetGroupInfo(const char *g,const char *o,bool issigned)
  {
  free(groupname);
  free(owner);
  sign=issigned;
  groupname=strdup(g);
  owner=strdup(o);  
  autosign=false;
  }

//--------------------------------------------------

void SGroupInfo::Serialize(ctArchive &arch)
  {
  if (arch.check("GR")==false) 
    {arch.err(-1);return;}
  arch.ver(GROUPINFOVERSION);
  arch.useMalloc();
  if (-arch) this->~SGroupInfo();
  arch>>=groupname;
  arch>>=owner;
  arch>>=sound;
  arch>>=sign;
  if (arch.ver()>=0x101) 
    {
    arch>>=poeple;
    arch>>=privat;	
    }
  if (arch.ver()>=0x102)
    arch>>=autosign;
  }

//--------------------------------------------------

ctArchive& operator>>=(ctArchive& arch, SGroupInfo &nfo)
  {
  nfo.Serialize(arch);
  return arch;
  }

//--------------------------------------------------

CGroupList::CGroupList()
  {
  
  }

//--------------------------------------------------

CGroupList::~CGroupList()
  {
  
  }

//--------------------------------------------------

int CGroupList::FindGroupByOwner(const char *owner)
  {
  for (int i=0;i<Size();i++) if (stricmp((*this)[i].owner,owner)==0) return i;
  return -1;    
  }

//--------------------------------------------------

int CGroupList::FindGroup(const char *group)
  {
  for (int i=0;i<Size();i++) if (stricmp((*this)[i].groupname,group)==0) return i;
  return -1;    
  }

//--------------------------------------------------

