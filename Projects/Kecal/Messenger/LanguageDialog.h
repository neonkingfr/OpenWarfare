// LanguageDialog.h: interface for the CLanguageDialog class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LANGUAGEDIALOG_H__10F1C7D7_9515_48AD_99E8_48623848D48A__INCLUDED_)
#define AFX_LANGUAGEDIALOG_H__10F1C7D7_9515_48AD_99E8_48623848D48A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"

class CLanguageDialog : public CDialogClass  
{
public:
	LCID lang;
	CLanguageDialog();
	virtual ~CLanguageDialog();

    virtual LRESULT OnInitDialog();
	virtual LRESULT OnOK();
	virtual LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
};



#endif // !defined(AFX_LANGUAGEDIALOG_H__10F1C7D7_9515_48AD_99E8_48623848D48A__INCLUDED_)
