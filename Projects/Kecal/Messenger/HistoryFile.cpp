// HistoryFile.cpp: implementation of the CHistoryFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "HistoryFile.h"
#include "resource.h"
#include <shlobj.h>
#include <time.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHistoryFile::CHistoryFile():out(*new ofstream)
  {
  
  }










CHistoryFile::~CHistoryFile()
  {
  delete &out;
  }










bool CHistoryFile::CreateHistory(const char *topic, const char *from, ChannelID &chan)
  {  
  char fname[256+MAX_PATH];
  SHGetSpecialFolderPath(NULL,fname,CSIDL_APPDATA ,TRUE);
  strcat(fname,"\\");
  strcat(fname,appname);
  CreateDirectory(fname,NULL);
  strcat(fname,"\\");
  char *c=strchr(fname,0);
  chan.print(c,ArrLength(fname));
  c=strchr(fname,0);  
  strcpy(c,StrRes[IDS_HISTORYEXT]);
  MiniLog(1);
  out.open(fname,ios::out|ios::app|ios::binary);
  MiniLog(2);
  if (!out) return false;
  if (out.tellp()==(fstream::pos_type)0) 
    {
    out<<topic<<" ("<<(from==NULL?"(null)":from)<<") \r\n";
    out<<"------ ";
    SYSTEMTIME systm;
    GetLocalTime(&systm);
    out<<systm.wDay<<"."<<systm.wMonth<<"."<<systm.wYear;
    out<<" -------------------------------------------\r\n";
    }
  MiniLog(3); 
  return true;
  }










void CHistoryFile::AddToHistory(const char *from, const char *text)
  {
  MiniLog(4);
  if (out.is_open())
    {
    char buff[256];
    SYSTEMTIME systm;
    GetLocalTime(&systm);
    _snprintf(buff,ArrLength(buff),"%s (%02d:%02d): \r\n",from,systm.wHour,systm.wMinute);
    MiniLog(5);
    out<<buff<<text<<"\r\n\r\n";
    out.flush();
    }  
  }

void CHistoryFile::MiniLog(int index)
  {
	  return;

    unsigned long *ptr=*(unsigned long **)((char *)&out+0x88);
    theApp->AddMimiLog(index,(unsigned long)ptr);
	if (ptr) theApp->AddMimiLog(index,*ptr); 
  }