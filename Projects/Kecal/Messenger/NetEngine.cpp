// NetEngine.cpp: implementation of the CNetEngine class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NetEngine.h"

#define KM_RAISEUP (WM_APP+2) 

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define ZAKLAD_CRC 0xC005
static unsigned short CalcCRC(void *from, long delka)
  {
  unsigned char *data=(unsigned char *)from;
  unsigned long l=0;
  do
    {
    l=(l<<8)|(delka>0?*data++:0);delka--;
    l=(l<<8)|(delka>0?*data++:0);delka--;
    l%=ZAKLAD_CRC;
    }
  while(delka>-1);
  return htons((unsigned short)(l & 0xffff));
  }

//--------------------------------------------------

static bool regclass=false;
static CNetEngine *wndattach;

CNetEngine::CNetEngine()
  {
  sock=INVALID_SOCKET;
  notifywnd=NULL;
  rcvcount=0;
  counter=0;
  }

//--------------------------------------------------

CNetEngine::~CNetEngine()
  {
  if (sock!=INVALID_SOCKET) closesocket(sock);
  if (notifywnd) DestroyWindow(notifywnd);
  }

//--------------------------------------------------

void CNetEngine::Close()
  {
  if (sock!=INVALID_SOCKET) closesocket(sock);
  if (notifywnd) DestroyWindow(notifywnd);
  }

//--------------------------------------------------

static LRESULT CALLBACK CallBackWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
  {


  //LogF("Received %x WM_COPYDATA %x KM_RAISEUP %x", msg, msg == WM_COPYDATA, msg == KM_RAISEUP);
  CNetEngine *net=(CNetEngine *)GetWindowLong(hWnd,GWL_USERDATA);
  if (net==NULL)
    {
    net=wndattach;
    wndattach=NULL;
    net->notifywnd=hWnd;
    if (net)
      SetWindowLong(hWnd,GWL_USERDATA,(LONG)net);
    else
      return 0;
    }
  return net->WinProc(msg,wParam,lParam);
  }

//--------------------------------------------------

static void UnregisterNetEngineClass()
  {
  UnregisterClass(SOCKCLASSNAME,AfxGetInstanceHandle());
  }

//--------------------------------------------------

BOOL CNetEngine::Create(DWORD addres, unsigned short port, bool debug)
  {
  if (!regclass)
    {	
    WNDCLASS cls;
    if (GetClassInfo(AfxGetInstanceHandle(),SOCKCLASSNAME,&cls)==1) UnregisterClass(SOCKCLASSNAME,AfxGetInstanceHandle());
    memset(&cls,0,sizeof(cls));
    cls.lpszClassName=SOCKCLASSNAME;
    cls.lpfnWndProc=CallBackWindowProc;
    cls.hInstance=AfxGetInstanceHandle();	
    if (RegisterClass(&cls)==FALSE)
      {
      MessageBox(NULL,"Windows: Unable to register window class. This error is unrecoverable",appname,MB_OK|MB_TASKMODAL);
      ExitProcess(10);
      }	
    regclass=true;
    atexit(UnregisterNetEngineClass);
    }
  wndattach=this;
  notifywnd=CreateWindowEx(WS_EX_TOOLWINDOW,SOCKCLASSNAME,appname,WS_POPUP,0,0,100,100,NULL,NULL,AfxGetInstanceHandle(),NULL);  
  if (notifywnd==NULL) return FALSE;
  
  sock=socket(AF_INET,SOCK_DGRAM,0);
  if (sock==INVALID_SOCKET) return FALSE;
  SOCKADDR_IN sin;
  memset(&sin,0,sizeof(sin));
  sin.sin_family=AF_INET;
  if (debug)
    {
    int p;
    for (p=0;p<32;p++)
      {
      sin.sin_port=htons(p+port);
      if (bind(sock,(SOCKADDR *)&sin,sizeof(sin))!=SOCKET_ERROR) break;
      }
    if (p==32) return FALSE;
    }
  else
    {
    sin.sin_port=htons(port);
    if (bind(sock,(SOCKADDR *)&sin,sizeof(sin))==SOCKET_ERROR) 
      {closesocket(sock);sock=INVALID_SOCKET;return FALSE;}
    }
  
  WSAAsyncSelect(sock,notifywnd,WM_APP,FD_READ);
  memset(&outadr,0,sizeof(outadr));
  outadr.sin_family=AF_INET;
  outadr.sin_addr.S_un.S_addr=addres;
  outadr.sin_port=htons(port);
  return TRUE;
  }

//--------------------------------------------------

LRESULT CNetEngine::WinProc(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  if (msg==WM_CLOSE) 
    {
    PostQuitMessage(0);
    }
  else if (msg==WM_ENDSESSION) return OnWmApp(msg,wParam,lParam);
  else if (msg==WM_APP) Receive();
  else return OnWmApp(msg,wParam,lParam);   
  return 0;
  }

//--------------------------------------------------

bool CNetEngine::Receive()
  {
  SOCKADDR_IN sin;
  int sinlen=sizeof(sin);
  int size=recvfrom(sock,inbuffer,sizeof(inbuffer)-1,0,(SOCKADDR *)&sin,&sinlen);
  if (size<2) return false;  
  size-=2;
  unsigned short *p=(unsigned short *)(inbuffer+size);
  if (*p!=CalcCRC(inbuffer,size)) return false;
  insize=size;
  for (int i=rcvcount-1;i>=0;i--)
    if (rlist[i] && rlist[i]->Receive(*this,sin)==true) break;
  return true;
  }

//--------------------------------------------------

bool CNetEngine::AddReceiver(CNetReceiver *rr)
  {
  if (rcvcount>=RCVMAX) return false;
  rlist[rcvcount]=rr;
  rcvcount++;
  return true;
  }

//--------------------------------------------------

bool CNetEngine::RemoveReceiver(CNetReceiver *rr)
  {
  int shft=0;
  for (int i=0;i<rcvcount;i++)
    {
    if (rlist[i]==rr) shft++;
    else rlist[i-shft]=rlist[i];
    }
  rcvcount-=shft;
  return shft!=0;
  }

//--------------------------------------------------

void CNetEngine::Send(int charcount,const SOCKADDR_IN *outaddr)
  {
  unsigned short *p=(unsigned short *)(outbuffer+charcount);
  *p=CalcCRC(outbuffer,charcount);
  charcount+=sizeof(*p);
  if (outaddr==NULL) outaddr=&this->outadr;
  while (sendto(sock,outbuffer,charcount,0,(SOCKADDR *)outaddr,sizeof(SOCKADDR))<0)
    if (WSAGetLastError()!=WSAEWOULDBLOCK) break;
  }

//--------------------------------------------------

char * CNetEngine::CreatePacket()
  { 
  sprintf(outbuffer,"%d\t",counter++);
  return strchr(outbuffer,0);
  }

//--------------------------------------------------

ostream *CNetEngine::OutStream()
  {
  return new ostrstream(outbuffer,ArrLength(outbuffer));
  }

//--------------------------------------------------

istream *CNetEngine::InStream()
  {
  return new istrstream(inbuffer,ArrLength(inbuffer));
  }

//--------------------------------------------------

bool CNetEngine::SendStream(ostream *out)
  {
  bool ret=false;
  ostrstream *o=dynamic_cast<ostrstream *>(out);
  if (o!=NULL && o->str()!=outbuffer)
    {
    if (o->pcount()<ArrLength(outbuffer))
      memcpy(outbuffer,o->str(),o->pcount());
    else
      {o->rdbuf()->freeze(0);o=NULL;}
    }
  if (o!=NULL) 
    {
    Send(o->pcount());
    o->rdbuf()->freeze(0);
    ret=true;
    }
  return ret;
  }

//--------------------------------------------------

BOOL CNetEngine::RestartService(DWORD addres, unsigned short port)
  {
  if (htons(port)!=outadr.sin_port)
    {
    SOCKET s;
    s=socket(AF_INET,SOCK_DGRAM,0);
    if (s==INVALID_SOCKET) return FALSE;
    SOCKADDR_IN sin;
    memset(&sin,0,sizeof(sin));
    sin.sin_family=AF_INET;
    sin.sin_port=htons(port);
    if (bind(s,(SOCKADDR *)&sin,sizeof(sin))==SOCKET_ERROR) 
      {closesocket(s);s=INVALID_SOCKET;return FALSE;}
    WSAAsyncSelect(s,notifywnd,WM_APP,FD_READ);
    closesocket(sock);
    sock=s;
    outadr.sin_port=sin.sin_port;
    }
  outadr.sin_family=AF_INET;
  outadr.sin_addr.S_un.S_addr=addres;
  return TRUE;
  }

//--------------------------------------------------

HWND CNetEngine::FindSelf()
  {
  return FindWindow(SOCKCLASSNAME,appname);
  }

//--------------------------------------------------

SendQueue *SendQueue::AddToQueue(SendQueue *what)
{
  if (this==NULL) return what;
  else
  {
    SendQueue *q=this;
    while (q->_next!=NULL) q=q->_next;
    q->_next=what;
    return this;
  }
}

SendQueue *SendQueue::RemoveFromQueue()
{
  SendQueue *q=this->_next;
  _next=NULL;
  return q;
}


SendQueue *SendQueue::Run(CNetEngine &net)
{
  if (this==NULL) return NULL;
  if (_retries<=0) 
  {
    SendQueue  *queue=RemoveFromQueue();
    if (_hwndnotify) SendMessage(_hwndnotify,_msgnotify,0,(LPARAM)this);
    delete this;
    return queue;
  }
  _retries--;
  void *data=(void *)net.GetOutcomeBuffer();
  int realsize=__min(65500,_size);
  memcpy(data,_data,realsize);
  net.Send(realsize,_target.sin_family!=AF_INET?NULL:&_target);
  return AddToQueue(RemoveFromQueue());
} 

SendQueue *SendQueue::RemoveFromQueueByID(unsigned long _id)
{
  if (this==NULL) return this;
  SendQueue *p=this;
  if (p->_id==_id) 
  {
    SendQueue  *queue=RemoveFromQueue();
    if (_hwndnotify) SendMessage(_hwndnotify,_msgnotify,1,(LPARAM)this);
    delete this;
    return queue;        
  }
  while (p->_next && p->_next->_id!=_id) p=p->_next;
  if (p->_next)
  {    
    SendQueue *z=p->_next;
    p->_next=z->RemoveFromQueue();    
    if (z->_hwndnotify) SendMessage(z->_hwndnotify,z->_msgnotify,1,(LPARAM)z);
    delete z;
  }
  return this;  
}

SendQueue::SendQueue(ostream *out, HWND hwndNotify, UINT msgNotify, int retries):
      _hwndnotify(hwndNotify),_msgnotify(msgNotify),_retries(retries),
        _next(NULL),_id(0) 
{
  ostrstream *str=dynamic_cast<ostrstream *>(out);
  if (str)
  {
    _data=malloc(str->pcount());
    memcpy(_data,str->str(),str->pcount());
    str->freeze(false);
    _size=str->pcount();
  }

}
