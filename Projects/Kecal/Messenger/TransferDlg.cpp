#include "stdafx.h"
#include "transferdlg.h"
#include "Application.h"
#include <assert.h>

#define TRANSFER_BLOCKSIZE 1024

CTransferDlg::CTransferDlg(void)
  {
  }

CTransferDlg::~CTransferDlg(void)
  {
  }

bool TransferInfo::InitFile(const char * filename, DWORD size, bool mode)
   {
   if (mode)
     {
     hFile=CreateFile(filename,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_RANDOM_ACCESS,NULL);
     if (hFile==INVALID_HANDLE_VALUE) return false;
     DWORD high;
     filesize=GetFileSize(hFile,&high);
     if (high!=0) return false; //files larger then 4GB not supported by Kecal;     
     }
   else
     {
     hFile=CreateFile(filename,GENERIC_READ|GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_FLAG_RANDOM_ACCESS,NULL);
     if (hFile==INVALID_HANDLE_VALUE) return false;
     filesize=size;
     }
   hMapping=CreateFileMapping(hFile,NULL,sendmode?PAGE_READONLY:PAGE_READWRITE,NULL,filesize,NULL);
   if (hMapping==NULL) {CloseHandle(hFile);return false;}
   mapped=(char *)MapViewOfFile(hMapping,FILE_MAP_READ|(sendmode?0:FILE_MAP_WRITE),0,0,filesize);
   if (mapped==NULL) {CloseHandle(hMapping);CloseHandle(hFile);return false;}
   sendmode=mode;
   nframes=(filesize+TRANSFER_BLOCKSIZE-1)/TRANSFER_BLOCKSIZE;
   int fsz=(nframes+7)/8;
   frameinfo=new unsigned char[fsz];
   memset(frameinfo,0,fsz);
   boostinit=16;
   boost=1;
   nextframe=0;
   remain=nframes;
   return true;
   }

void TransferInfo::Close()
  {
  UnmapViewOfFile(mapped);
  CloseHandle(hMapping);
  CloseHandle(hFile);
  delete [] frameinfo;
  }

bool TransferInfo::IsFrameCheckedIn(DWORD frame)
  {
  return (frameinfo[frame/8] & (1<<(frame & 7)))!=0;
  }

void TransferInfo::CheckInFrame(DWORD frame)
  {
  frameinfo[frame/8]|=(1<<(frame & 7));
  }

bool TransferInfo::ArrivedData(DWORD frame, void *data, int size)
  {
  assert(sendmode==false);
  if (size>TRANSFER_BLOCKSIZE) return false; //block je moc velky
  if (frame>=nframes) return false;
  DWORD addr=frame*TRANSFER_BLOCKSIZE;
  if (addr+size>filesize) return false;     //block je moc velky a nevejde se na konec souboru
  if (frame<nframes-1 && size<TRANSFER_BLOCKSIZE) return false; //maly blok muze byt jen posledni
  memcpy(mapped+addr,data,size);
  if (!IsFrameCheckedIn(frame))
    {
    CheckInFrame(frame);
    remain--;
    }
  return true;
  }

void *TransferInfo::RequestData(DWORD frame, int &size)
  {
  assert(sendmode==true);
  if (frame>=nframes) return NULL;
  size=TRANSFER_BLOCKSIZE;
  DWORD addr=frame*TRANSFER_BLOCKSIZE;
  if (size+addr>filesize) size=filesize-addr;
  if (!IsFrameCheckedIn(frame))
    {
    CheckInFrame(frame);
    remain--;
    }
  return (void *)(mapped+addr);
  }

int TransferInfo::ArrivedPacket(TransferDataPacket &packet, CNetEngine &engine,SOCKADDR_IN *target)
  {
  if (packet.minuser==minuser && packet.majuser==majuser && packet.transferID==transferID)
    {
    if (sendmode) 
      {
      if (packet.frame==0xFFFFFFFF) return 1; //oznameni konec prenosu
      if (packet.frame>=nframes) return -1;   //neplatny paket  
      void *data;
      int size;
      data=RequestData(packet.frame,size);
      char *bpacket=engine.CreatePacket();
      TransferDataPacket *ppacket=(TransferDataPacket *)bpacket;
      ppacket->majuser=theApp->config.majid;
      ppacket->minuser=theApp->config.minid;
      ppacket->id=transferID;
      ppacket->frame=packet.frame;
      ppacket->datasize=size;
      memcpy(ppacket->data,data,size);
      engine.Send(sizeof(*ppacket)+size,target);
      return 0;
      }
    else
      {
      if (ArrivedData(packet.frame,packet.data,packet.datasize)==false) return -1;
      TransferDataPacket *ppacket=(TransferDataPacket *)engine.CreatePacket();
      ppacket->majuser=theApp->config.majid;
      ppacket->minuser=theApp->config.minid;
      ppacket->id=transferID;
      ppacket->datasize=0;
      while (nextframe<nframes && IsFrameCheckedIn(nextframe)) nextframe++;
      if (nextframe==nframes)
        {
        nextframe=0;
        while (nextframe<nframes && IsFrameCheckedIn(nextframe)) nextframe++;
        if (nextframe==nframes)        
          ppacket->frame=0xFFFFFFFF;
        else ppacket->frame=nextframe;
        }      
      else ppacket->frame=nextframe;
      engine.Send(sizeof(*ppacket),target);
      return ppacket->frame==0xFFFFFFFF;
      }
    }
  return -1;
  }
