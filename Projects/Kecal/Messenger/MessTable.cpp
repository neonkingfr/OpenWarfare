// MessTable.cpp: implementation of the CMessTable class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MessTable.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMessTable::CMessTable()
  {
  memset(table,0,sizeof(table));
  idcnt=GetTickCount();
  }

//--------------------------------------------------

CMessTable::~CMessTable()
  {
  
  }

//--------------------------------------------------

bool CMessTable::Find(unsigned long uidmaj,unsigned long uidmin, int id)
  {
  Item p;
  p.uidmin=uidmin;
  p.uidmaj=uidmaj;
  p.id=id;
  for (int i=0;i<MESSTABLESIZE;i++)
    if (table[i]==p) return true;
  return false;
  }

//--------------------------------------------------

void CMessTable::Add(unsigned long uidmaj,unsigned long uidmin, int id)
  {
  ptr++;
  if (ptr>=MESSTABLESIZE) ptr=0;
  table[ptr].uidmin=uidmin;
  table[ptr].uidmaj=uidmaj;
  table[ptr].id=id;
  }

//--------------------------------------------------

