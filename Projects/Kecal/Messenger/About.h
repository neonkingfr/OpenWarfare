// About.h: interface for the CAbout class.
//
//////////////////////////////////////////////////////////////////////



#if !defined(AFX_ABOUT_H__BE9B4A9E_051E_459A_803E_AFF53817BBB2__INCLUDED_)
#define AFX_ABOUT_H__BE9B4A9E_051E_459A_803E_AFF53817BBB2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"

class CAbout : public CDialogClass  
  {
  public:
    
    
    CAbout();
    virtual ~CAbout();
    
    virtual LRESULT OnInitDialog();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_ABOUT_H__BE9B4A9E_051E_459A_803E_AFF53817BBB2__INCLUDED_)
