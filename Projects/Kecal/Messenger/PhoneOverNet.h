// PhoneOverNet.h: interface for the CPhoneOverNet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PHONEOVERNET_H__4008026B_B0ED_4556_B9E7_BE8788B240B8__INCLUDED_)
#define AFX_PHONEOVERNET_H__4008026B_B0ED_4556_B9E7_BE8788B240B8__INCLUDED_

#include "ACMStream.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"
#include "WaveIn.h"
#include "WaveOut.h"

class CPhoneOverNet;

#define WON_USERPRIVATEBUFFER (WAVEBUF_GRANUALITY*4)

class CWaveInImpl: public CWaveIn
  {
	CPhoneOverNet &_owner;
  public:
	CWaveInImpl(CPhoneOverNet &owner);
	virtual bool BufferDrop(char *buff, int size);
  };
	
class CWaveOutImpl: public CWaveOut
  {
	CPhoneOverNet &_owner;
  public:
	CWaveOutImpl(CPhoneOverNet &owner);
	virtual bool BufferDrop(char *buff, int size);
  };

struct SUserVoiceData
  {
  ChannelID channel;
  int blocksize;
  char block[WON_USERPRIVATEBUFFER];
  public:
	  char *GetDataSpace()  {return block+blocksize;}
	  int GetSpaceSize() {return WON_USERPRIVATEBUFFER-blocksize;}
	  bool IsWaiting();
	  void DataOut(int size);
	  int LoadData(const char *data, int size);
  };

TypeIsMovable(SUserVoiceData);

class CPhoneOverNet : public CDialogClass  
{
	CWaveInImpl _wavein;
	CWaveOutImpl _waveout;
	AutoArray<SUserVoiceData> _receiver;
	ACMStream *_acmWave;
	ACMStream *_acmMp3;
	int _playcnt;
public:
	bool DisplayLevel(int idc,const short *buffer, int samples, short cmplevel);
	LRESULT OnCancel();
	LRESULT OnOK();
	LRESULT OnInitDialog();
	bool MicRead(const char *buff, int size);
	bool MixBuffer(char *buffer, int size);
	void IncomeData(ChannelID &channel, const char *data, int size);
	int AddNewReceiver(ChannelID &chan);
	int FindSender(ChannelID &chan);
	CPhoneOverNet();
	virtual ~CPhoneOverNet();
	virtual LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);

};

#endif // !defined(AFX_PHONEOVERNET_H__4008026B_B0ED_4556_B9E7_BE8788B240B8__INCLUDED_)
