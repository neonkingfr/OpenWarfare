// MessTable.h: interface for the CMessTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSTABLE_H__CAFF688A_BF0B_4E40_AF46_210B30E78203__INCLUDED_)
#define AFX_MESSTABLE_H__CAFF688A_BF0B_4E40_AF46_210B30E78203__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MESSTABLESIZE 256

class CMessTable  
  {
  int idcnt;
  
  struct Item
    {
    unsigned long uidmin;
    unsigned long uidmaj;
    unsigned short port;
    DWORD id;
    bool operator==(const Item& other) const
      {return uidmin==other.uidmin && uidmaj==other.uidmaj && id==other.id;}
    };
  
  Item table[MESSTABLESIZE];
  unsigned int ptr;
  
  public:
    void Add(unsigned long uidmaj,unsigned long uidmin, int id);
    bool Find(unsigned long uidmaj,unsigned long uidmin, int id);
    bool MustHandle(unsigned long uidmaj,unsigned long uidmin, int id)
    {
      if (Find(uidmaj,uidmin,id)==false)
      {
        Add(uidmaj,uidmin,id);
        return true;
      }
      else
        return false;
    }
    CMessTable();
    virtual ~CMessTable();
    
    int GenID() 
      {
      idcnt=(idcnt+1)& 0x3FFFFFFF;
      return idcnt;
      }
    
  };

//--------------------------------------------------

#endif // !defined(AFX_MESSTABLE_H__CAFF688A_BF0B_4E40_AF46_210B30E78203__INCLUDED_)
