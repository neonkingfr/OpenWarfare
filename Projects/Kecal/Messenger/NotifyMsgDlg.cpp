// NotifyMsgDlg.cpp: implementation of the CNotifyMsgDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "resource.h"
#include "NotifyMsgDlg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNotifyMsgDlg::CNotifyMsgDlg()
  {
  oMessage=NULL;
  }

//--------------------------------------------------

CNotifyMsgDlg::~CNotifyMsgDlg()
  {
  free(oMessage);
  }

//--------------------------------------------------

LRESULT CNotifyMsgDlg::OnInitDialog()
  {
  free(oMessage);oMessage=NULL;
  SetDlgItemText(IDC_MSG,vMessage);
  return CDialogClass::OnInitDialog();
  }

//--------------------------------------------------

LRESULT CNotifyMsgDlg::OnOK()
  {
  oMessage=GetDlgItemText(IDC_MSG);
  return CDialogClass::OnOK();
  }

//--------------------------------------------------

