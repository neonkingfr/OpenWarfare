// WaveIn.h: interface for the CWaveIn class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WAVEIN_H__9F4A3C05_6ED7_466C_A474_1C9F1EF73536__INCLUDED_)
#define AFX_WAVEIN_H__9F4A3C05_6ED7_466C_A474_1C9F1EF73536__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WaveInOut.h"

class CWaveIn:public CWaveInOut
{
	HWAVEIN handle;
public:
	void Close();
	MMRESULT Open(HINSTANCE hInst, HWND hMainWnd, UINT uDeviceID, LPWAVEFORMATEX pwfx);
	CWaveIn();
	virtual ~CWaveIn();

	virtual bool BufferDrop(char *buff, int size)=0;
    virtual void OnBufferDrop(WAVEHDR *hdr);
	virtual MMRESULT Reset();
	virtual MMRESULT Start();
	virtual MMRESULT Stop();
	virtual MMRESULT Pause();
	virtual MMRESULT Resume();
	virtual void ReuseLeftBuffers();
	};

#endif // !defined(AFX_WAVEIN_H__9F4A3C05_6ED7_466C_A474_1C9F1EF73536__INCLUDED_)
