// Application.cpp: implementation of the CApplication class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "resource.h"
#include "SettingsDlg.h"
#include "GroupNameDlg.h"
#include "version.h"
#include "About.h"
#include "WinSharedMemory.h"
#include <mmsystem.h>
#include <shlobj.h>

#define LOGEVENT_MSG (WM_APP+9110)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static int bitmaporder[]=
{
  0,
    ID_MAIN_NOVOUSKUPINU,
    ID_MAIN_OTEVTKECALA,
    ID_MAIN_HISTORIE,
    ID_MAIN_STATISTIKA,
    ID_MAIN_NASTAVEN,
    ID_MAIN_OAPLIKACI,
    0,
    ID_MAIN_RESTART,
    ID_MAIN_EXIT,
    ID_MAIN_TELEFONPOSTI,
    ID_MAIN_PIDATSE
};

static HFONT menufont;
static HFONT menufontbold;

#define QUEUE_TIMER 1213

CApplication *theApp;

HINSTANCE AfxGetInstanceHandle()
{
  return theApp->hInst;
}

int TrackPopupMenuBitmaped(HMENU hMenu, UINT uFlags, int x, int y, int nReserved, HWND hWnd, CONST RECT *prcRect);

//--------------------------------------------------

CApplication::CApplication(HINSTANCE hInst,char *cmdln):_sendQueue(NULL)
{
  this->hInst=hInst;
  theApp=this;
  cmdline=strdup(cmdln);
  listlock=0;
  needgrpsync=false;
  savedstate=UST_Offline;
  uploadcounter=0;
  reqversion=VERSION_NR;
  requser=0;
  mnu_images=NULL;
  restart=false;
  miniLogPos=0;
}

void CApplication::AddMimiLog(unsigned long event, unsigned long value)
{
  miniLog[miniLogPos].event=event;
  miniLog[miniLogPos].value=value;
  miniLogPos++;
  if (miniLogPos>=MINILOGMAX) miniLogPos=0;
}

//--------------------------------------------------

CApplication::~CApplication()
{
  free(cmdline);
  free(requser);
}

//--------------------------------------------------

static void CALLBACK MinuteTimer(HWND hWnd,UINT uMsg, UINT idEvent,DWORD dwTime)
{
  CApplication *net=static_cast<CApplication *>((CNetEngine *)GetWindowLong(hWnd,GWL_USERDATA));
  if (net->mystate!=UST_Offline) net->MinuteTick();
}

/*
//--------------------------------------------------
void setOfflineMsg(CApplication *theApp)
{
  int i=0;
  for (; i< ((theApp)->users).Size(); i++)
  {
    //(((*(Array<SUserInfo>*)(&(*(AutoArray<SUserInfo,MemAllocD>*)(&(theApp).users)))))._data)[i]->username 
    if (strcmp((((theApp)->users)[i].username),"vilem_2") == 0) 
    {

      ((theApp)->users)[i].offlinemsg = "Ahoj3";
      LogF("NOW", i, ((theApp)->users)[i].username, ((theApp)->users)[i].offlinemsg);
    };
    LogF("%d %s %s", i, ((theApp)->users)[i].username, ((theApp)->users)[i].offlinemsg);    
  };
};
*/
int CApplication::Init()
{
  //  SetThreadLocale(MAKELCID(MAKELANGID(LANG_ENGLISH,SUBLANG_DEFAULT),SORT_DEFAULT));

  myip=0;

  console=NULL;
  riched=LoadLibrary("RICHED20.DLL");
  OleInitialize(NULL);
  LoadConfig(); 
  LoadUsers();
  LoadGroups();
  AddReceiver(this);
  BOOL res;
#ifdef _DEBUG
  res=Create(config.broadcast,config.port,true);
#else
  res=Create(config.broadcast,config.port,false);
#endif
  SetMyState(UST_Online);


  if (res==FALSE)	
    MessageBox(NULL,StrRes[IDS_UNABLESTARTNETENGINE],appname,MB_OK|MB_SYSTEMMODAL|MB_ICONSTOP);		
  if (config.showtray) UpdateShellIcon();
  SetTimer(notifywnd,1,USERREFRESH*1000,MinuteTimer);
  SendIdle();
  SendCommand(NULL,COMM_REFRESH);
  SendCommand(NULL,COMM_GROUPS);
  SendCommand(NULL,COMM_RELOADGROUPS);


  ActivateHotkeys(true);
  _inMenu=false;

  mnu_images=ImageList_LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_MENUIMAGES),
    16,1,RGB(255,0,255),IMAGE_BITMAP,LR_LOADMAP3DCOLORS);

  users.CheckVanish();
  return 0;
}

//--------------------------------------------------

void CApplication::LoadConfig()
{
  HKEY kk=OpenProfileKey();  
  ctArchiveInReg arch(kk,StrRes[IDS_CONFIGNAME]);
  if (!arch) 
  {config.AutoConfigure();SaveConfig();}
  else config.Serialize(arch);
  RegCloseKey(kk);

  if (config.CheckLanguage(NULL,false)) SaveConfig();
}

//--------------------------------------------------


int CApplication::SendMsgToUser (char *msgString, char *username)
{
  CChatWindowList &wlist=theApp->chatlist;
  ChannelID id;
  CChatWindow *win;

  int i;
  int users[10000];users[1]=-1;
  int groups[10000];groups[1]=-1;

  for (i=0;i<10000;i++)
  {
    users[i] = -1;
    groups[i] = -1;
  };

  int user=theApp->users.FindUser(username);
  if (user ==-1) 
  {
    groups[0]=theApp->groups.FindGroup("Trashbag");
    if (groups[0]==-1) return 1;
  }
  else
  {
    users[0]= user;
    groups[0]= -1;
  }

  id = wlist.CreateChannel(users,groups);	
  win = wlist.PresetText(id, msgString);
  win ->OnOK();

  
  ShowWindow(win->GetHWND(), SW_SHOWMINIMIZED);
    /*
  PostMessage(win ->GetHWND(),)

  ret = PostMessage(self,KM_RAISEUP,0,WM_RBUTTONUP); 
  */
  LogF("Msg (%s) sent to: %s id%d", msgString, username, theApp->users.FindUser(username));
  return 1;
};



int CApplication::Run()
{

  MSG msg;
  int i=0;
  //while ((i<50) && (GetMessage(&msg,NULL,0,0)))

//  theApp->SendMsgToUser("Ahoj hehe.","vilem_2");


  while ((GetMessage(&msg,NULL,0,0)))
  {
    //LogF("RUN: Received %x WM_COPYDATA %x KM_RAISEUP %x", msg.message, msg.message == WM_COPYDATA, msg.message == KM_RAISEUP);

    if (!chatlist.PreTranslateMessage(&msg) 
      && (!mainwnd.GetHWND() || !mainwnd.IsSubclassMsg(&msg)))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
      //LogF("%d %d %d %d %d %d %d", msg.hwnd, msg.message, msg.wParam, msg.lParam, msg.time, msg.pt.x, msg.pt.y);
        
      listlock=0;
    };
    i++;


    
    
    /*
    if (((float) i/1000.0) == floor (((float) i/1000.0))) 
    {      
      setOfflineMsg(this);
      LogF("%d", i);
    };
    */
    

  };
    return msg.wParam;

}

//--------------------------------------------------

void CApplication::Done()
{
  ActivateHotkeys(false);
  if (mystate!=UST_Offline)
  {
    SetMyState(UST_Offline);
    SendIdle();
    Sleep(100);
    SendIdle();
    Sleep(100);
    SendIdle();
    Sleep(100);
  }
  DestroyShellIcon();
  ImageList_Destroy(mnu_images);
  chatlist.Clear(); //clear all windows before we will free riched library
  FreeLibrary(riched);
  SaveUsers();
}

//--------------------------------------------------

void CApplication::UpdateShellIcon(int icon)
{
  MSG msg;
  PeekMessage(&msg,NULL,0,0,PM_NOREMOVE);
  nid.cbSize=sizeof(nid);
  if (icon==0) icon=IDI_OFFLINE+mystate;
  nid.hIcon=(HICON)LoadImage(hInst,MAKEINTRESOURCE(icon),IMAGE_ICON,16,16,LR_SHARED);
  LoadString(hInst,IDS_APPNAME,nid.szTip,sizeof(nid.szTip));
  nid.uCallbackMessage=KM_TRAYNOTIFY;
  nid.uFlags=NIF_ICON |NIF_MESSAGE|NIF_TIP;
  nid.hWnd=GetNotifyWnd();
  nid.uID=100;
  if (Shell_NotifyIcon(NIM_MODIFY,&nid)==0)
  {
    Shell_NotifyIcon(NIM_ADD,&nid);
  }
}

//--------------------------------------------------

void CApplication::DestroyShellIcon()
{
  Shell_NotifyIcon(NIM_DELETE,&nid);
}

//--------------------------------------------------



static void MeasureMenuItem(int itemid, const char *text,unsigned int &x, unsigned int &y)
{
  HDC hdc = GetDC(theApp->GetNotifyWnd());
  SIZE size; 
  GetTextExtentPoint32(hdc, text+2, strlen(text+2), &size); 
  size.cx=size.cx+20;
  x=__max(size.cx,20); 
  y=__max(size.cy,20); 
  ReleaseDC(theApp->GetNotifyWnd(),hdc);
}

static void DrawMenuItem(HDC hdc, RECT &rc, const char *text, int id, int state)
{
  COLORREF clrPrevText, clrPrevBkgnd; 
  HFONT old;
  if (state & ODS_GRAYED)
  {
    clrPrevText = SetTextColor(hdc, GetSysColor(COLOR_GRAYTEXT)); 
    clrPrevBkgnd = SetBkColor(hdc, GetSysColor(COLOR_MENU)); 
  }
  else if (state & ODS_SELECTED) 
  { 
    clrPrevText = SetTextColor(hdc,GetSysColor(COLOR_HIGHLIGHTTEXT)); 
    clrPrevBkgnd = SetBkColor(hdc, GetSysColor(COLOR_HIGHLIGHT)); 
  } 
  else 
  { 
    clrPrevText = SetTextColor(hdc, GetSysColor(COLOR_MENUTEXT)); 
    clrPrevBkgnd = SetBkColor(hdc, GetSysColor(COLOR_MENU)); 
  } 
  HIMAGELIST image=theApp->mnu_images;
  short idx;
  memcpy(&idx,text,2);
  if (idx==2) old=(HFONT)SelectObject(hdc,menufontbold);else old=(HFONT)SelectObject(hdc,menufont);
  text+=2;
  RECT rc2=rc;
  rc2.left+=25;
  ExtTextOut(hdc, rc.left, rc.top, ETO_OPAQUE,&rc, NULL,0, NULL); 
  DrawText(hdc,text,strlen(text),&rc2,DT_LEFT|DT_VCENTER|DT_SINGLELINE);
  if (idx>=0) ImageList_Draw(image,idx,hdc,rc.left+2,rc.top+2,ILD_NORMAL);
  SetTextColor(hdc, clrPrevText); 
  SetBkColor(hdc, clrPrevBkgnd); 
  SelectObject(hdc,old);
}

typedef struct tagMYREC
{
  char  user[200];
  char  message[200];        
} MYREC;

LRESULT CApplication::OnWmApp(UINT msg, WPARAM wParam, LPARAM lParam)
{  
  switch (msg)
  {	
  case WM_COPYDATA:
    {
      COPYDATASTRUCT *MyCDS;
      MYREC *data;

      MyCDS = (COPYDATASTRUCT *) lParam;
      data = (MYREC*) MyCDS->lpData;
      
      theApp->SendMsgToUser(data->message,data->user);      
      //LogF("%s %s", data->message, data->user);
    };
    break;
  case WM_ENDSESSION:if (wParam)
                     {
                       Done();
                       return 0;

                     }
                     break;

  case KM_TRAYNOTIFY:	  
    if (lParam==WM_RBUTTONUP)	
    {POINT pt;GetCursorPos(&pt);ProcessMenu(pt);}
    if (lParam==WM_LBUTTONDBLCLK) RaiseMainWnd();
    break;
  case KM_DOUBLEMSG :
    {
      _inMenu++;
      char *buff=(char *)alloca(1000);      
      HOSTENT *phe=gethostbyaddr((const char *)&lParam,sizeof(in_addr),0);      
      _snprintf(buff,1000,StrRes[IDS_DOUBLEDETECTED],inet_ntoa(*(in_addr *)&lParam),phe==NULL?"???":phe->h_name);
      MessageBox(NULL,buff,appname,MB_ICONSTOP|MB_OK);
      _inMenu--;
    }
    break;
  case KM_RAISEUP:
    RaiseMainWnd();
    break;
  case WM_HOTKEY:
    if (config.mainontop && mainwnd.GetHWND()!=NULL && IsWindowVisible(mainwnd))
      mainwnd.OnCancel();
    else
      if (GetForegroundWindow()==mainwnd) mainwnd.OnCancel();
    else  
      RaiseMainWnd();
    break;
  case WM_MEASUREITEM: if (wParam==0)
                       {
                         MEASUREITEMSTRUCT *lpmis = (LPMEASUREITEMSTRUCT) lParam;
                         MeasureMenuItem(lpmis->itemID,(const char *)lpmis->itemData,lpmis->itemWidth,lpmis->itemHeight);
                         return TRUE;
                       }
                       break;
  case WM_DRAWITEM: if (wParam==0)
                    {
                      LPDRAWITEMSTRUCT lpdis = (LPDRAWITEMSTRUCT) lParam;
                      DrawMenuItem(lpdis->hDC,lpdis->rcItem,(const char *)lpdis->itemData,lpdis->itemID,lpdis->itemState);
                      return TRUE;
                    }
                    break;
  case KM_REPLYTOKECAL:
    {
      WinSharedMemory shared;
      if (shared.Synchronize(WinS_Message(wParam,lParam))==true)
      {
        const char *reader=(const char *)shared.Lock();
        const char *identity=reader;
        reader=strchr(reader,0)+1;
        const char *topic=reader;
        reader=strchr(reader,0)+1;
        bool newid=*(bool *)reader;
        SendToKecalEntryPoint(identity,topic,newid);
        shared.Unlock();
      }
    }
    break;
  case LOGEVENT_MSG:
    {
      char *msg=(char *)lParam;
      MessageBox(mainwnd,msg,appname,MB_ICONSTOP|MB_SYSTEMMODAL);
      delete [] msg;    
    }
    break;
  default: return DefWindowProc(GetNotifyWnd(),msg,wParam,lParam);
  }
  return CNetEngine::OnWmApp(msg,wParam,lParam);
}

//--------------------------------------------------

void CApplication::ProcessMenu(POINT pt,bool opentop)
{
  HMENU mnu=LoadMenu(hInst,MAKEINTRESOURCE(IDR_MAINMENU));
  HMENU sub=GetSubMenu(mnu,0);
  RECT rc;
  SetForegroundWindow(notifywnd);
  if (_inMenu)
  {
    int cnt=::GetMenuItemCount(sub);
    for (int i=0;i<cnt;i++)
    {
      int id=::GetMenuItemID(sub,i);
      if (id!=ID_MAIN_EXIT && id!=ID_MAIN_RESTART && id!=ID_MAIN_OTEVTKECALA)
        EnableMenuItem(sub,i,MF_GRAYED|MF_BYPOSITION);
    }
  }
  _inMenu++;
  EnableMenuItem(sub,ID_MAIN_TELEFONPOSTI,MF_GRAYED);
  EnableMenuItem(sub,ID_MAIN_PIDATSE,MF_GRAYED);
  int cmd=TrackPopupMenuBitmaped(sub,TPM_RETURNCMD|TPM_NONOTIFY|(opentop?TPM_BOTTOMALIGN:0),pt.x,pt.y,0,notifywnd,&rc);
  DestroyMenu(mnu);
  if (cmd>=ID_MAIN_MJSTAV_NEJSEM && cmd<=ID_MAIN_MJSTAV_HRAJUHRY) SetMyState((EUserState)(cmd-ID_MAIN_MJSTAV_NEJSEM));	
  else
    switch (cmd)
  {
    case ID_MAIN_EXIT: if (MessageBox(NULL,StrRes[IDS_ASKEXIT],appname,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDYES)
                         PostQuitMessage(0);break;	
    case ID_MAIN_RESTART: if (MessageBox(NULL,StrRes[IDS_ASKRESTART],appname,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDYES)
                            PostQuitMessage(1);break;	
    case ID_MAIN_OTEVTKECALA: RaiseMainWnd();break;
    case ID_MAIN_NOVOUSKUPINU:AddGroup(); break;
    case ID_MAIN_HISTORIE: OpenHistory();break;
    case ID_MAIN_STATISTIKA: if (statistics) BringWindowToTop(statistics);else statistics.Create(IDD_STATUSBAR,notifywnd);break;
    case ID_MAIN_PIDATSE: if (roomlst) BringWindowToTop(roomlst);else roomlst.Create(IDD_CHATROOMS,notifywnd);break;
/*    case ID_MAIN_TELEFONPOSTI: if (phone) BringWindowToTop(phone);else phone.Create(IDD_TELEFON,notifywnd);break;*/
    case ID_MAIN_OAPLIKACI: 
      {
        CAbout about;
        about.DoModal(IDD_ABOUT,notifywnd);
      }
      break;
    case ID_MAIN_NASTAVEN:
      {
        CSettingsDlg dlg;
        dlg.cfg=&(config);
        ActivateHotkeys(false);
        if (dlg.DoModal(IDD_NASTAVENI,mainwnd==NULL?notifywnd:mainwnd)==IDOK) 
        {
          if (RestartService(config.broadcast,config.port)==FALSE)
            MessageBox(notifywnd,StrRes[IDS_INVALIDNETCONFIG],appname,MB_OK);
          if (config.showtray) UpdateShellIcon();else DestroyShellIcon();
          RunStartup(config.runstartup);
          SaveConfig();
          if (mainwnd) 
          {
            SetWindowPos(mainwnd,config.mainontop?HWND_TOPMOST:HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
            mainwnd.ReloadBgPic();
          }
          if (dlg.oldname && stricmp(dlg.oldname,config.myname))
          {
            for (int i=0;i<groups.Size();i++)
              if (stricmp(groups[i].owner,dlg.oldname)==0)
              {
                free(groups[i].owner);
                groups[i].owner=strdup(config.myname);
              }
              RefreshGroups();

          }
          SendIdle();
        }
        if (dlg.langrestart)
        {
          int id=MessageBox(this->GetNotifyWnd(),StrRes[IDS_LANGUAGECHANGEASK],appname,MB_YESNO);
          if (id==IDYES)
          {
            PostQuitMessage(1);
          }
        }
        ActivateHotkeys(true);
      }
  }
  _inMenu--;
}

//--------------------------------------------------

void CApplication::SetMyState(EUserState state, bool save)
{
  if (save) 
  {
    if (savedstate==UST_Offline) savedstate=mystate;
  }
  else
    savedstate=UST_Offline;
  mystate=state;
  if (config.showtray) UpdateShellIcon();
  if (mainwnd) mainwnd.SetState(mystate);
  SendIdle();
  if (config.logevents)
  {
    SOCKADDR_IN sin;
    sin.sin_addr.S_un.S_addr=myip;
    LogEventChange(state,config.myname,sin);
  }
}

//--------------------------------------------------

void CApplication::RunStartup(BOOL on)
{
  if (on)
  {
    char path[2048];
    GetModuleFileName(NULL,path,ArrLength(path));
    RunAtStartup(appname,path);
  }
  else
    RunAtStartup(appname,NULL);
}

//--------------------------------------------------

void CApplication::SaveConfig()
{
  HKEY kk=OpenProfileKey();  
  {
    ctArchiveOutReg arch(kk,StrRes[IDS_CONFIGNAME]);
    config.Serialize(arch);
  }
  RegCloseKey(kk);
}

//--------------------------------------------------

void CApplication::LoadUsers()
{
  HKEY kk=OpenProfileKey();  
  {
    ctArchiveInReg arch(kk,StrRes[IDS_USERLISTNAME]);
    if (!arch)
    {
    }
    else
      arch>>=users;
  }
  RegCloseKey(kk);
}

//--------------------------------------------------

void CApplication::SaveUsers()
{
  HKEY kk=OpenProfileKey();  
  {
    ctArchiveOutReg arch(kk,StrRes[IDS_USERLISTNAME]);
    arch>>=users;
  }
  RegCloseKey(kk);
}

//--------------------------------------------------

void CApplication::LoadGroups()
{
  HKEY kk=OpenProfileKey();  
  {
    ctArchiveInReg arch(kk,StrRes[IDS_GROUPLISTNAME]);
    if (!(!arch)) arch>>=groups;
    else
    {
      SGroupInfo *nfo=&groups.Append();
      nfo->SetGroupInfo("Vsem","---",true);
    }
  }
  RegCloseKey(kk);
}

//--------------------------------------------------

void CApplication::SaveGroups()
{
  HKEY kk=OpenProfileKey();  
  {
    ctArchiveOutReg arch(kk,StrRes[IDS_GROUPLISTNAME]);
    arch>>=groups;
  }
  RegCloseKey(kk);
}

//--------------------------------------------------

void CApplication::RaiseMainWnd()
{
  if (mainwnd.GetHWND())
  {
    ShowWindow(mainwnd,SW_SHOW);
    SetWindowPos(mainwnd,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
    if (!config.mainontop)SetWindowPos(mainwnd,HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
    SetForegroundWindow(mainwnd);
    SetActiveWindow(mainwnd);
    BringWindowToTop(mainwnd);
  }
  else
  {
    mainwnd.Create(IDD_MAINDLG,NULL);
    mainwnd.SetState(mystate);
    SetWindowPos(mainwnd,config.mainontop?HWND_TOPMOST:HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  }
}

//--------------------------------------------------

void CApplication::RefreshGroups(bool send)
{
  if (mainwnd.GetHWND())
    mainwnd.LoadUsersAndGroups();
  SaveGroups();
  if (send) SendCommand(&SendCommandAdditionalInfo(3),COMM_GROUPS);
}

//--------------------------------------------------

void CApplication::AddGroup()
{
  CGroupNameDlg dlg;
  dlg.sound=NULL;
  if (dlg.DoModal(IDD_NOVASKUPINA,mainwnd)==IDOK && dlg.result)
  {
    if (groups.FindGroup(dlg.result)!=-1)
      MessageBox(mainwnd,StrRes[IDS_GROUPALREADYEXISTS],appname,MB_OK|MB_ICONINFORMATION);
    else
    {
      SGroupInfo *nfo=&groups.Append();
      nfo->SetGroupInfo(dlg.result,config.myname,true);
      nfo->privat=dlg.privat;
      nfo->poeple=dlg.groupresult;
      nfo->sound=dlg.sound;
      RefreshGroups();
    }
  }
}

//--------------------------------------------------

void CApplication::DeleteGroup(int idx)
{
  LockLists();
  if (stricmp(groups[idx].GetOwner(),config.myname))
  {if (MessageBox(mainwnd,StrRes[IDS_ASKREMOVENOTMYGROUP],appname,MB_OKCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2)==IDCANCEL) goto stop;}
  else
    if (MessageBox(mainwnd,StrRes[IDS_ASKREMOVEGROUP],appname,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDNO) goto stop;
  groups.Delete(idx);
  RefreshGroups();
stop:
  UnlockLists();
}

//--------------------------------------------------

void CApplication::RenameGroup(int idx)
{
  LockLists();
  /*  if (stricmp(groups[idx].GetOwner(),config.myname))
  {
  MessageBox(mainwnd,StrRes[IDS_UNABLERENAME],appname,MB_OKCANCEL|MB_ICONEXCLAMATION);
  UnlockLists();
  return;
  }
  else*/
  {
    CGroupNameDlg dlg;
    dlg.grpname=groups[idx].groupname;
    dlg.groupcontent=groups[idx].poeple;
    dlg.privat=groups[idx].privat;
    dlg.editidx=idx;
    dlg.sound=groups[idx].sound;
    dlg.logauto=groups[idx].autosign;
    if (stricmp(groups[idx].owner,theApp->config.myname)) 
      dlg.soundonly=true;
    if (dlg.DoModal(IDD_NOVASKUPINA,mainwnd)==IDOK && dlg.result)
    {
      int p=groups.FindGroup(dlg.result);
      if (p!=-1 && p!=idx)
        MessageBox(mainwnd,StrRes[IDS_GROUPALREADYEXISTS],appname,MB_OK|MB_ICONINFORMATION);
      else
      {
        free(groups[idx].groupname);
        groups[idx].groupname=strdup(dlg.result);
      }
      groups[idx].poeple=dlg.groupresult;
      groups[idx].privat=dlg.privat;
      groups[idx].sound=dlg.sound;
      groups[idx].autosign=dlg.logauto;
    }
    RefreshGroups();
  }
  UnlockLists();
}

//--------------------------------------------------

static bool ReCheckMyIP(unsigned long &myip, unsigned long newip)
{
  PHOSTENT phost=gethostbyname("localhost");
  phost=gethostbyname(phost->h_name);
  for (int i=0;i<2;i++)
  {
    char **addr=phost->h_addr_list;
    while (*addr)
    {
      if (memcmp(&newip,*addr,4)==0)
      {
        myip=newip;
        return true;
      }
      addr++;
    }
    if (i==0)
    {
      char cmpname[256];
      DWORD size=sizeof(cmpname);
      GetComputerName(cmpname,&size);
      phost=gethostbyname(cmpname);
    }
  }
  return false;
}

static int SortCompare( const void *v0, const void *v1 )
{
  const SUserInfo &user1=*(SUserInfo *)v0;
  const SUserInfo &user2=*(SUserInfo *)v1;
  return (user1>user2)-(user1<user2);  
}

bool CApplication::Receive(CNetEngine& net,SOCKADDR_IN &sin)
{
  if (mystate==UST_Offline) return false;

  if (console) OutConsole(GetIncomeBuffer(),GetIncomeBufferSize());

  char *username;
  int id;
  EUserState state;
  int command;
  DWORD minid;
  DWORD majid;

  istrstream in(GetIncomeBuffer(),GetIncomeBufferSize());
  ctSArchiveIn arch(in);

  arch>>=id;
  if (id>=0x3FFFFFFF) return false;
  arch>>=username;
  arch>>=state;
  arch>>=command;
  arch>>=majid;
  arch>>=minid;

  statistics.AddMessageStatistics(GetIncomeBufferSize(),username);
  statistics.BlinkMsg(command);

  bool myself=stricmp(username,config.myname)==0 || (majid==config.majid && minid==config.minid);

  switch (command)
  {	
  case COMM_IDLE: if (messtab.MustHandle(majid,minid,id))
                  {
                    unsigned short reqver;
                    arch>>=reqver;
                    if (myself && myip!=sin.sin_addr.S_un.S_addr)
                    {
                      if (ReCheckMyIP(myip,sin.sin_addr.S_un.S_addr)==false)
                      {
                        PostMessage(GetNotifyWnd(),KM_DOUBLEMSG,0,sin.sin_addr.S_un.S_addr);
                        mystate=UST_Offline;
                        if (config.showtray) UpdateShellIcon();
                        if (mainwnd) mainwnd.SetState(mystate);    
                        free(username);
                        return true;
                      }
                    }
                    if (!myself && !IsListLocked())
                    {
                      int us=users.FindUser(username);
                      if (us==-1) 
                      {
                        us=users.FindUserByID(majid,minid);
                        if (us==-1)
                        {
                          us=users.Size();
                          SUserInfo *nfo=&users.Append();
                          nfo->SetName(username);
                          nfo->majid=majid;
                          nfo->minid=minid;
                        }
                        else
                          users[us].SetName(username);
                        SaveUsers();
                      }
                      else
                      {
                        users[us].majid=majid;
                        users[us].minid=minid;
                      }
                      users[us].SetLongTimeout(GetTickCount());
                      users[us].SetVanishDate();
                      users[us].lastknownip=sin;
                      users[us].version=reqver;
                      if (users[us].SetUserState(state)) 
                      {
                        UserStateChanged(us);
                        CheckForNotifyMsg(us);
                        chatlist.CheckUserPeer(us);
                        users.QSortBin(SortCompare);
                        if (mainwnd) mainwnd.LoadUsersAndGroups();
                      }
                      else
                        CheckForNotifyMsg(us);
                    }
                    if (reqver>reqversion)
                    {
                      uploadcounter=2;
                      reqversion=reqver;
                      download.DropBuffers();
                    }
                    if (uploadcounter && reqver==reqversion && reqver!=VERSION_NR && (requser==NULL || stricmp(requser,username)!=0))
                    {
                      free(requser);
                      requser=strdup(username);
                    }
                    ChannelID id;
                    chanlist.Clear();
                    arch>>=id;
                    while (id.majorid!=0 || id.minorid!=0) 
                    {
                      chanlist.Add(id);
                      arch>>=id;
                    }
                    chatlist.CheckUser(username,chanlist.Data(),chanlist.Size()); 
                    if (roomlst.GetHWND()) 
                      roomlst.CheckChannels(chanlist.Data(),chanlist.Size());
                  }
                  break;

  case COMM_GROUPS: if (!myself && messtab.MustHandle(majid,minid,id)) if (!IsListLocked()) SynchronizeGroups(arch,username);else needgrpsync=true;
    break;
  case COMM_REFRESH: if (messtab.MustHandle(majid,minid,id)) SendIdle();break;

  case COMM_RELOADGROUPS: if (!myself && messtab.MustHandle(majid,minid,id)) SendCommand(&SendCommandAdditionalInfo(3),COMM_GROUPS);break;

  case COMM_SENDMESSAGE: 
    {	  
      ChannelID nchn;
      char *p;
      arch.useMalloc();
      arch>>=nchn;
      if (chatlist.CheckChannel(nchn) && messtab.MustHandle(majid,minid,id))
      {
        arch>>=p;
        chatlist.ReceiveText(username,p,myself,nchn);
        free(p);
      }
      break;
    }

  case COMM_PEERNOTIFY: if (messtab.MustHandle(majid,minid,id))
    {
      int cmd;
      ChannelID forid;
      arch>>=cmd;
      arch>>=forid;
      if (forid.majorid==config.majid && forid.minorid==config.minid)
        chatlist.PeerNotifyState(ChannelID(majid,minid),cmd);
      break;
    }

  case COMM_OPENCHAT: if (messtab.MustHandle(majid,minid,id))
                      {
                        char *sndgroup=NULL;
                        if (mystate==UST_Deaf) break;
                        ChannelID nchn;
                        bool forme=false;
                        char *p;
                        arch>>=nchn;
                        arch.useMalloc();
                        arch>>=p;
                        while (p[0]) 
                        {
                          if (stricmp(config.myname,p)==0) 
                          {forme=true;}
                          free(p);
                          arch>>=p;
                        }
                        free(p);
                        arch>>=p;
                        while (p[0]) 
                        {
                          int i=groups.FindGroup(p);
                          if (i==-1)
                          {if (config.openunknown) 
                          {forme=true;}}
                          else
                          {if (groups[i].sign) 
                          {forme=true;sndgroup=groups[i].sound;break;}}
                          free(p);
                          arch>>=p;
                        }
                        free(p);
                        if (forme)
                        {
                          arch>>=p;
                          chatlist.OpenChatWindow(nchn,p,username,sndgroup);
                          free(p);
                        }
                      }
                      break;

  case COMM_DOWNLOADREQ: if (messtab.MustHandle(majid,minid,id))
                         {
                           char *p;
                           arch>>=p;
                           if (strcmp(p,config.myname)==0) VersionUpload();
                           free(p);
                         }
                         break;
  case COMM_DOWNLOADINFO:if (messtab.MustHandle(majid,minid,id))
                         {
                           int a,b;
                           arch>>=a;
                           arch>>=b;
                           if (!(!arch) && uploadcounter) 
                             download.PrepareDownload(a,b);
                           break;
                         }
  case COMM_DOWNLOADDATA:if (messtab.MustHandle(majid,minid,id))
                         {
                           char *p=(char *)alloca(DOWNLOAD_BLOCKSIZE);
                           int blkid;
                           arch>>=blkid;
                           arch.serial(p,DOWNLOAD_BLOCKSIZE);
                           if (!(!arch) && uploadcounter)
                           {
                             if (download.ReceiveBlock(blkid,p)==true)		  
                             {
                               uploadcounter=0;	  	  		  
                               restart=true;
                             }
                           }
                           break;
                         }
  case COMM_PEERMESSAGE:
    {	  
      ChannelID nchn;
      char *p;
      arch.useMalloc();
      arch>>=nchn;
      if (nchn.majorid==config.majid && nchn.minorid==config.minid ||
        majid==config.majid && minid==config.minid)
      {
        if (messtab.MustHandle(majid,minid,id))
        {
          arch>>=p;
          ChannelID from;
          if (myself) from=nchn;else
          {
            from.majorid=majid;
            from.minorid=minid;
          }
          chatlist.OpenPeerWindow(from,p,username,myself);
          free(p);
        }
        SendCommandAdditionalInfo nfo;
        nfo.target=sin;
        SendCommand(&nfo,COMM_PEERMESSAGEACK,id);
      }
      break;
    }
  case COMM_PEERMESSAGEACK: if (messtab.MustHandle(majid,minid,id))
                            {
                              int id;
                              arch>>=id;
                              _sendQueue=_sendQueue->RemoveFromQueueByID(id);
                              break;
                            }
  }
  free(username);
  return true;
}

//--------------------------------------------------

static DWORD GetMyUID()
{
  static DWORD uid=0;  //UID nemuze byt nula
  if (uid) return uid;  //pokud je UID definovane, vrat ho jako vysledek
  HOSTENT *phe; 
  phe=gethostbyname("localhost");  //ziskej muj zaznam o IP adresach
  if (phe!=NULL) phe=gethostbyname(phe->h_name);  //ziskej IP adresu pocitace meho jmena
  if (phe==NULL)		//pokud to nejde
  {
    DWORD sz=0;				
    char *p;
    GetComputerName(p,&sz);	//ziskej velikost jmena pocitace
    p=(char *)alloca(sz);	//zaalokuj prostor pro jmeno  
    GetComputerName(p,&sz);	//ziskej jmeno pocitace
    phe=gethostbyname(p);	//zkus ziskat jeho IP adresu
    if (phe==NULL)			//pokud to selze
    {
      uid=GetTickCount();	//nastav UID podle soucasnych hodin. Snad nikdo jiny nebude mit stejne UID
      return uid;
    }
  }
  memcpy(&uid,phe->h_addr_list[0],4);
  return uid;
}

//--------------------------------------------------

bool CApplication::SendCommand(const SendCommandAdditionalInfo *ainfo, int command, ...)
{
  ostrstream ostr(GetOutcomeBuffer(),GetOutcomeBufferSize()-1);
  ctSArchiveOut arch(ostr);
  va_list argl;
  va_start(argl,command);
  int id=messtab.GenID();
  arch>>=id;
  arch>>=config.myname;
  arch>>=mystate;
  arch>>=command;
  arch>>=config.majid;
  arch>>=config.minid;  
  switch (command)
  {
  case COMM_IDLE :	  	  
    {
      unsigned short ver=VERSION_NR;
      arch>>=ver;
      ChannelID *p=va_arg(argl,ChannelID *);
      for (;p->minorid!=0 || p->majorid!=0;p++) arch>>=*p;
      arch>>=*p;
      break;
    }

  case COMM_RELOADGROUPS:break;
  case COMM_REFRESH:break;


  case COMM_GROUPS: 
    {
      for (int i=0;i<groups.Size();i++) 
        if (stricmp(groups[i].owner,config.myname)==0 && !groups[i].privat) 
        {
          arch<<(groups[i].autosign?'!':' ');
          arch>>=groups[i].groupname;
        }
        arch<<"";
    }
    break;

  case COMM_OPENCHAT:
    {
      ChannelID *chid=va_arg(argl,ChannelID *);
      char *p;
      char *topic;

      arch>>=*chid;

      p=va_arg(argl,char *);
      while (*p)
      {arch<<p;p=strchr(p,0)+1;}
      arch<<p;

      p=va_arg(argl,char *);
      while (*p)
      {arch<<p;p=strchr(p,0)+1;}
      arch<<p;

      topic=va_arg(argl,char *);
      arch>>=topic;

      break;
    }

  case COMM_REQCHANTOPIC:
    {
      ChannelID *chid=va_arg(argl,ChannelID *);

      arch>>=*chid;
      break;
    }

  case COMM_CHANTOPIC:
    {
      ChannelID *chid=va_arg(argl,ChannelID *);
      char *topic=va_arg(argl,char *);

      arch>>=*chid;
      arch>>=topic;
      break;
    }
  case COMM_SENDMESSAGE:
    {
      ChannelID *chid=va_arg(argl,ChannelID *);
      char *topic=va_arg(argl,char *);

      arch>>=*chid;
      arch>>=topic;
      break;
    }
  case COMM_DOWNLOADREQ:
    {
      char *user=va_arg(argl,char *);
      arch>>=user;
    }
    break;
  case COMM_DOWNLOADINFO:
    {
      int p;
      p=va_arg(argl,int); //filesize
      arch>>=p;	  
      p=va_arg(argl,int); //blocks
      arch>>=p;
    }
    break;
  case COMM_DOWNLOADDATA:
    {
      int p;
      p=va_arg(argl,int); //blockid;
      arch>>=p;
      void *data=va_arg(argl,void *);
      arch.serial(data,DOWNLOAD_BLOCKSIZE); //blockdata;
      break;
    }
  case COMM_PEERMESSAGE :
    {
      ChannelID *chid=va_arg(argl,ChannelID *);
      char *topic=va_arg(argl,char *);

      arch>>=*chid;
      arch>>=topic;
      break;
    }
  case COMM_PEERMESSAGEACK:
    {
      int uid=va_arg(argl,int);
      arch>>=uid;
      break;
    }
  case COMM_PEERNOTIFY:
    {
      int id=va_arg(argl,int);
      ChannelID forid=*(va_arg(argl,ChannelID *));
      arch>>=id;
      arch>>=forid;
      break;
    }
  default: return false;
  }
  Send(ostr.pcount(),ainfo==NULL || ainfo->target.sin_family!=AF_INET?NULL:&ainfo->target);
  if (ainfo!=NULL) 
  {
    SendQueue *item=new SendQueue(GetOutcomeBuffer(),ostr.pcount(),true,ainfo->hNotify,ainfo->uMsg,ainfo->retries);
    item->SetTarget(ainfo->target);
    item->SetID(ainfo->useUID?ainfo->uid:id);
    _sendQueue=_sendQueue->AddToQueue(item);
    SetTimer(notifywnd,QUEUE_TIMER,500,NULL);
  }  
  return true;
}

//--------------------------------------------------

void CApplication::SendIdle()
{
  chanlist.Clear();
  if (mystate==UST_Online || mystate== UST_Invisible || mystate==UST_Working || mystate==UST_Deaf || mystate==UST_Away) chatlist.CreateListOfChannels(chanlist);
  ChannelID nul(0,0);
  chanlist.Add(nul);
  SendCommand(NULL,COMM_IDLE,chanlist.Data());
  if (users.CheckTimeout(this))
  {
    if (mainwnd) mainwnd.UpdateList();
  }
  if (!IsListLocked())
  {
    if (needgrpsync) SendCommand(&SendCommandAdditionalInfo(3),COMM_RELOADGROUPS);
    needgrpsync=false;
  }
}

//--------------------------------------------------

void CApplication::SynchronizeGroups(ctArchive &arch,char *owner)
{
  bool rfs=false;
  int i;
  for (i=0;i<groups.Size();i++)
    if (stricmp(groups[i].owner,owner)==0 && stricmp(groups[i].owner,config.myname) !=0)
    {
      free(groups[i].owner);
      groups[i].owner=NULL;
    }
    char *p;
    char sig;
    arch>>=sig;
    arch>>=p;
    while (p && p[0])
    {
      int sc=groups.FindGroup(p);
      if (sc!=-1)
      {
        free(groups[sc].owner);
        groups[sc].owner=strdup(owner);		
      }
      else
      {
        SGroupInfo *nfo=&groups.Append();
        nfo->SetGroupInfo(p,owner,sig=='!'?config.openfromnew:false);
        rfs=true;
      }
      free(p);
      arch>>=sig;
      arch>>=p;
    }
    for (i=0;i<groups.Size();i++)
      if (groups[i].owner==NULL) 
      {groups.Delete(i--);rfs=true;}
      if (rfs) RefreshGroups(false);
      free(p);
}

//--------------------------------------------------

void CApplication::OpenHistory()
{
  if (history==NULL)
    history.Create(IDD_HISTORY,notifywnd);
  SetForegroundWindow(history);
}

//--------------------------------------------------

void CApplication::VersionUpload()
{
  verupload.BeginUpload();
  /*  char fname[512];
  GetModuleFileName(AfxGetInstanceHandle(),fname,ArrLength(fname));
  HANDLE ff=CreateFile(fname,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
  if (ff==NULL) return;
  DWORD p=GetFileSize(ff,NULL);
  DWORD blk=(p+DOWNLOAD_BLOCKSIZE-1)/DOWNLOAD_BLOCKSIZE;
  SendCommand(3,COMM_DOWNLOADINFO,p,blk);
  char buff[DOWNLOAD_BLOCKSIZE];
  for (DWORD i=0;i<blk;i++)
  {
  DWORD ww;
  ReadFile(ff,buff,DOWNLOAD_BLOCKSIZE,&ww,NULL);
  SendCommand(1,COMM_DOWNLOADDATA,i,buff);
  Sleep(5);
  }
  CloseHandle(ff);*/
}

//--------------------------------------------------

static UINT CALLBACK OFNPlaySelectedSound(
  HWND hdlg,      // handle to child dialog window
  UINT uiMsg,     // message identifier
  WPARAM wParam,  // message parameter
  LPARAM lParam   // message parameter
  )
{
  if (uiMsg==WM_NOTIFY)
  {
    OFNOTIFY *notify=(LPOFNOTIFY) lParam;
    if (notify->hdr.code==CDN_SELCHANGE)
    {
      char buff[MAX_PATH*4];
      SendMessage(GetParent(hdlg),CDM_GETFILEPATH,MAX_PATH*4,(LPARAM)buff);
      PlaySound(buff,NULL,SND_ASYNC|SND_FILENAME |SND_NOWAIT);
      return 1;
    }

  }
  return 0;
}

//--------------------------------------------------

bool AskForSound(char *filename, int size, HWND parent)
{
  OPENFILENAME ofn;
  memset(&ofn,0,sizeof(ofn));
  char *temp=(char *)alloca(strlen(StrRes[IDS_WAVFILTER]+1));
  char *c=temp;
  strcpy(temp,StrRes[IDS_WAVFILTER]);

  while ((c=strchr(c,'|'))) *c++=0;
  ofn.lStructSize=sizeof(ofn);
  ofn.hInstance=AfxGetInstanceHandle();
  ofn.hwndOwner=GetForegroundWindow();
  ofn.lpfnHook=OFNPlaySelectedSound;
  ofn.lpstrFile=filename;
  ofn.lpstrFilter=temp;
  ofn.lpstrTitle=StrRes[IDS_VYBERZVUK];
  ofn.Flags=OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_EXPLORER|OFN_ENABLEHOOK;
  ofn.nMaxFile=size;
  BOOL p=GetOpenFileName(&ofn);  
  PlaySound(NULL,NULL,SND_PURGE);
  return p!=FALSE;
}

//--------------------------------------------------

void CApplication::ActivateHotkeys(bool activate)
{
  if (activate)
  {
    int modf=0;
    if (config.hotkey._modifiers& HOTKEYF_SHIFT) modf|=MOD_SHIFT;
    if (config.hotkey._modifiers& HOTKEYF_ALT) modf|=MOD_ALT;
    if (config.hotkey._modifiers& HOTKEYF_CONTROL) modf|=MOD_CONTROL;
    if (config.hotkey._modifiers& MOD_WIN) modf|=MOD_WIN;
    if (!RegisterHotKey(GetNotifyWnd(),1000,modf,config.hotkey._vk))
    {
      MessageBox(NULL,StrRes[IDS_HOTKEYREGFAILED],appname,MB_OK|MB_ICONSTOP);
    }
  }
  else
    UnregisterHotKey(GetNotifyWnd(),1000);

}

//--------------------------------------------------

void CApplication::MinuteTick()
{
  POINT q;
  GetCursorPos(&q);
  if (memcmp(&lastms,&q,sizeof(q))==0)
  {
    idletm++;
    if (idletm==IDLETIMEOUT) 
      SetMyState(UST_Away,true);
    if (idletm==NATIMEOUT)
    {
      SetMyState(UST_Deaf,true);
      if (restart) PostQuitMessage(1);
    }
  }
  else
  {
    idletm=0;
    if (savedstate!=UST_Offline)
      SetMyState(savedstate,false);
    lastms=q;
  }
  SendIdle();
  if (uploadcounter)
  {
    uploadcounter--;
    if (uploadcounter==0)
    {
      SendCommand(&SendCommandAdditionalInfo(1),COMM_DOWNLOADREQ,requser);
      uploadcounter=2;
    }
  }
}

//--------------------------------------------------

void CApplication::CheckForNotifyMsg(int userpos)
{
  SUserInfo &user=users[userpos];
  if (user.offlinemsg!=NULL && user.offlinemsg[0])
  {
    EUserState state=user.state;
    if (state==UST_Online || state==UST_Working || state==UST_Gaming)
    {
      ChannelID chan(user.majid,user.minid);
      SendCommand(&SendCommandAdditionalInfo(5),COMM_PEERMESSAGE,&chan,user.offlinemsg);
      user.SetNotifyMsg("");
      SaveUsers();
    }
  }
}

//--------------------------------------------------


static void OwnerDrawMenu(HMENU hMenu, bool destroy)
{
  int mnucnt=::GetMenuItemCount(hMenu);
  for (int i=0;i<mnucnt;i++)
  {
    static char mnutext[256];
    MENUITEMINFO mnfo;
    mnfo.cbSize=sizeof(mnfo);
    mnfo.fMask=MIIM_TYPE|MIIM_SUBMENU|MIIM_DATA|MIIM_ID;
    mnfo.cch=sizeof(mnutext)/sizeof(mnutext[0]);
    mnfo.dwTypeData=(mnutext);
    GetMenuItemInfo(hMenu,i,true,&mnfo);	
    if (destroy)
      free((void *)mnfo.dwItemData);
    else
    {
      if (~mnfo.fType & MFT_SEPARATOR) 
      {
        char *text=(char *)malloc(strlen(mnutext)+4);
        strcpy(text+2,mnutext);
        short idx;
        for (idx=0;idx<sizeof(bitmaporder)/sizeof(bitmaporder[0]);idx++) 
          if (bitmaporder[idx]==(signed)mnfo.wID) break;
        if (mnfo.hSubMenu) idx=7;
        if (idx==sizeof(bitmaporder)/sizeof(bitmaporder[0])) idx=-1;
        memcpy(text,&idx,2);
        mnfo.fMask=MIIM_TYPE|MIIM_SUBMENU|MIIM_DATA;
        mnfo.fType=MFT_OWNERDRAW;
        mnfo.dwItemData=(DWORD)text;
        SetMenuItemInfo(hMenu,i,MF_BYPOSITION,&mnfo);
      }
    }
    if (mnfo.hSubMenu) OwnerDrawMenu(mnfo.hSubMenu,destroy);
  }
}


int TrackPopupMenuBitmaped(HMENU hMenu, UINT uFlags, int x, int y, int nReserved, HWND hWnd, CONST RECT *prcRect)
{
  LOGFONT lg;
  GetObject(GetStockObject(DEFAULT_GUI_FONT),sizeof(lg),&lg);
  lg.lfCharSet=EASTEUROPE_CHARSET;
  menufont=CreateFontIndirect(&lg);
  lg.lfWeight=FW_BOLD;
  menufontbold=CreateFontIndirect(&lg);
  OwnerDrawMenu(hMenu,false);
  int res=TrackPopupMenu(hMenu,uFlags,x,y,nReserved,hWnd,prcRect);
  OwnerDrawMenu(hMenu,true);
  DeleteObject(menufont);
  DeleteObject(menufontbold);
  return res;
}

void CApplication::OutConsole(const char *text, int size)
{
  DWORD writt;
  char dot='.';
  char *beg="## ";
  char *end=" ##\r\n";
  WriteFile(console,beg,strlen(beg),&writt,NULL);
  for (int i=0;i<size;i++)
  {
    if (*text<32 && *text>=0) WriteFile(console,&dot,1,&writt,NULL);
    else WriteFile(console,text,1,&writt,NULL);
    text++;
  }
  WriteFile(console,end,strlen(end),&writt,NULL);
}

void CApplication::OpenConsole()
{
  if (console)
  {
    FreeConsole();
    console=NULL;
  }
  else
  {
    AllocConsole();
    console=GetStdHandle(STD_OUTPUT_HANDLE);
  }  
}

#include "dlgselectidentity.h"

void CApplication::SendToKecalEntryPoint(const char *identity, const char *topic, bool forcenew)
{
  int id=forcenew?-1:users.FindByIdentity(identity);
  if (id!=-1)
  {
    if (users[id].state==UST_Offline || users[id].state==UST_Deaf)
    {
      int ret=MessageBox(notifywnd,StrRes[IDS_USERISOFFLINESELECTIDENTITY],appname,MB_ABORTRETRYIGNORE|MB_ICONSTOP|MB_SYSTEMMODAL);
      if (ret==IDABORT) return;
      if (ret==IDRETRY) id=-1;
    }
  }
  if (id==-1)
  {
    DlgSelectIdentity dlg;
    dlg.identity=identity;
    theApp->RaiseMainWnd();
    if (dlg.DoModal(IDD_VYBERIDENTITU,mainwnd==NULL?notifywnd:mainwnd)==IDCANCEL) return;
    int p=users.FindUser(dlg.selected_user);
    if (p==-1) return;
    users[p].SetEmail(dlg.identity);
    id=p;
    SaveUsers();
  }
  int userlist[2];
  userlist[0]=id;
  userlist[1]=-1;
  ChannelID chan=chatlist.CreateChannel(userlist,userlist+1);
  CChatWindow *wnd=chatlist.PresetText(chan,topic);
  SetWindowPos(*wnd,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  SetForegroundWindow(*wnd);
  BringWindowToTop(*wnd);  
  SetWindowPos(*wnd,HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
}

LRESULT CApplication::WinProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
  if (msg==WM_TIMER && wParam==QUEUE_TIMER) {OnQueueTime();return 0;}
  else
    return __super::WinProc(msg,wParam,lParam);
}

void CApplication::OnQueueTime()
{
  _sendQueue=_sendQueue->Run(*this);
  if (_sendQueue==NULL) KillTimer(notifywnd,QUEUE_TIMER);
}


void CApplication::LogEventChange(EUserState state, const char *user, SOCKADDR_IN &lastKnownIP)
{
  const char *stateName=StrRes[state+IDS_STATE0];

  char fname[256+MAX_PATH];
  SHGetSpecialFolderPath(NULL,fname,CSIDL_APPDATA ,TRUE);
  strcat(fname,"\\");
  strcat(fname,appname);
  CreateDirectory(fname,NULL);
  strcat(fname,"\\");
  char *c=strchr(fname,0);
  SYSTEMTIME curtm;
  GetLocalTime(&curtm);
  sprintf(c,"Log-%04d-%02d.csv",curtm.wYear,curtm.wMonth);
  HANDLE h=CreateFile(fname,GENERIC_WRITE,FILE_SHARE_READ,0,OPEN_EXISTING,FILE_FLAG_WRITE_THROUGH,0);
  DWORD wrt;
  if (h==INVALID_HANDLE_VALUE)
  {
    h=CreateFile(fname,GENERIC_WRITE,FILE_SHARE_READ,0,CREATE_ALWAYS,FILE_FLAG_WRITE_THROUGH,0);
    if (h!=INVALID_HANDLE_VALUE) 
    {
      const char *header="Date,Time,User,State,IP\r\n";
      WriteFile(h,header,strlen(header),&wrt,0);
      }
  }
  char buff[512];
  _snprintf(buff,512,"%02d.%02d.%04d,%02d:%02d:%02d,\"%s\",\"%s\",\"%d.%d.%d.%d\"\r\n",      
    curtm.wDay,curtm.wMonth,
    curtm.wYear,curtm.wHour,curtm.wMinute,curtm.wSecond,
    user,
    stateName,
    lastKnownIP.sin_addr.S_un.S_un_b.s_b1,
    lastKnownIP.sin_addr.S_un.S_un_b.s_b2,
    lastKnownIP.sin_addr.S_un.S_un_b.s_b3,
    lastKnownIP.sin_addr.S_un.S_un_b.s_b4);
  if (h!=INVALID_HANDLE_VALUE)
  {
    SetFilePointer(h,0,0,FILE_END);
    WriteFile(h,buff,strlen(buff),&wrt,0);
    CloseHandle(h);
  }
  else
  {
    char *msg=new char [strlen(fname)+100+strlen(buff)];
    sprintf(msg,"Unable to write event:%s\r\nLog file \"%s\" is not accessible. Event will be lost!",buff,fname);
    PostMessage(notifywnd,LOGEVENT_MSG,0,(LPARAM)msg);
  }
}

void CApplication::UserStateChanged(int id)
{
  if (config.logevents) LogEventChange(users[id].state,users[id].username,users[id].lastknownip);
}