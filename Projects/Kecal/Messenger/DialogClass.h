// DialogClass.h: interface for the CDialogClass class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIALOGCLASS_H__F9B1D2E4_14AE_4200_B35A_6E52431DCF9F__INCLUDED_)
#define AFX_DIALOGCLASS_H__F9B1D2E4_14AE_4200_B35A_6E52431DCF9F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDialogClass  
  {
  int lastcx,lastcy;
  MINMAXINFO mmx;
  bool minset;
  protected:
    HWND hWnd;
    static CDialogClass *newDialog;
    void Attach(HWND hWnd) 
      {this->hWnd=hWnd;}
    HWND Detach() 
      {HWND h=hWnd;hWnd=NULL;SetWindowLong(h,DWL_USER,0);return h;}
  public:
    void SetMinMaxInfo(MINMAXINFO &nfo);
    void GetMinMaxInfo(MINMAXINFO &nfo) 
      {nfo=mmx;}
    int DoModal(int idc,HWND parent);
    BOOL Create(int idc,HWND parent);
    CDialogClass();
    virtual ~CDialogClass();
    
    operator HWND() 
      {return hWnd;}
    HWND GetHWND() 
      {return hWnd;}
    
    void EndDialog(int nResult);
    LRESULT SendDlgItemMessage(int idc, UINT msg, WPARAM wParam, LPARAM lParam)
      {return ::SendDlgItemMessage(hWnd,idc,msg,wParam,lParam);}
    BOOL EnableDlgWindow(int idc, BOOL enable)
      {return EnableWindow(GetDlgItem(idc),enable);}
    
    HWND GetDlgItem(int idc) 
      {return ::GetDlgItem(hWnd,idc);}
    
    BOOL SetDlgItemText(int idc,const _TCHAR *str)
      {return ::SetDlgItemText(hWnd,idc,str);}
    BOOL SetDlgItemInt(int idc,int value, BOOL sign)
      {return ::SetDlgItemInt(hWnd,idc,value, sign);}
    _TCHAR *GetDlgItemText(int idc);
    UINT GetDlgItemInt(int id,BOOL sign)
      {return ::GetDlgItemInt(hWnd,id,NULL,sign);}
    
    virtual LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
    virtual LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
    virtual LRESULT OnOK();
    virtual LRESULT OnCancel();
    virtual LRESULT OnInitDialog();
    virtual LRESULT OnPaint(HDC dc) 
      {return 0;}	
    virtual LRESULT OnGatewayMessage(WPARAM wParam, LPARAM lParam) 
      {return 0;}
    virtual LRESULT OnNotify(int id, NMHDR *ntf) 
      {return 0;}
    friend LRESULT CALLBACK DialogProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    virtual LRESULT OnSize(int cx, int cy, int rx, int ry, int flags) 
      {return 0;}
  };

//--------------------------------------------------

#endif // !defined(AFX_DIALOGCLASS_H__F9B1D2E4_14AE_4200_B35A_6E52431DCF9F__INCLUDED_)
