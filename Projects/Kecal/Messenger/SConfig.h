// SConfig.h: interface for the SConfig class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCONFIG_H__E0E9F019_B207_48CD_A31E_8BFDC9ABC5A0__INCLUDED_)
#define AFX_SCONFIG_H__E0E9F019_B207_48CD_A31E_8BFDC9ABC5A0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define NAMEHISTORYSPACE 256

struct SHotKey
  {
  int _vk;
  int _modifiers;
  SHotKey():_vk(0),_modifiers(0) 
    {}
  friend ctArchive &operator>>=(ctArchive &arch, SHotKey &hk)
    {
    arch>>=hk._vk;
    arch>>=hk._modifiers;
    return arch;
    }
  };

//--------------------------------------------------

struct SConfig  
  {	
  
  DWORD broadcast;
  int port;
  char *myname;
  char *soundfile;
  bool runstartup;
  bool openunknown;
  bool openfromnew;
  bool showtray;
  bool mainontop;
  bool chatontop;
  bool nopopup;
  bool noforesound;
  bool logevents;
  
  int notify_flags;

  DWORD majid;
  DWORD minid;
  int defchatwndmode;	
  SHotKey hotkey;
  char *skin_font;
  char *skin_image;
  bool skin_fontbold;
  bool skin_fontitalic;
  int skin_fontsize;
  bool skin_transparent;
  COLORREF skin_color;
  LCID language;
  
  const char * GetNameHistory();
  void AddToNameHistory(const char *name);
  void AutoConfigure();
  void Serialize(ctArchive &arch);
  SConfig();
  virtual ~SConfig();
  
  public:
	 bool  CheckLanguage(HWND app, bool force);
  };

//--------------------------------------------------

bool FindInNameHistory(const char *name,const char *hist);

#endif // !defined(AFX_SCONFIG_H__E0E9F019_B207_48CD_A31E_8BFDC9ABC5A0__INCLUDED_)