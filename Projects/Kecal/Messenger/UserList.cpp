// UserList.cpp: implementation of the CUserList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UserList.h"
#include "sconfig.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

bool CUserList::CheckTimeout(IUserListStateChange *notify)
  {
  int cnt=Size();
  bool q=false;
  DWORD curtime=GetTickCount();
  for (int i=0;i<cnt;i++)
    {
    if ((*this)[i].IsTimout(curtime))
      {
      if ((*this)[i].SetUserState(UST_Offline))
        if (notify) notify->UserStateChanged(i);
      q=true;
      }
    }
  return q;
  }

//--------------------------------------------------

int CUserList::FindUser(const char *logname)
  {
  for (int i=0;i<Size();i++) if (stricmp((*this)[i].username,logname)==0) return i;
  return -1;
  }

int CUserList::QuickSearchUser(const char *logname)
  {
  for (int i=0;i<Size();i++) if (strnicmp((*this)[i].username,logname,strlen(logname))==0) return i;
  return -1;
  }

  //--------------------------------------------------

void SUserInfo::Serialize(ctArchive &arch)
  {
  if (-arch) 
    {
    this->~SUserInfo();
    (*this)=SUserInfo();
    }
  if (arch.check("UR")==false) 
    {arch.err(-1);return;}
  arch.ver(USERINFOVERSION);
  arch.useMalloc();
  arch>>=username;
  arch>>=majid;
  arch>>=minid;
  if (arch.ver()>=0x101)
    arch>>=offlinemsg;
  if (arch.ver()>=0x102)
    {
    arch>>=email;
    arch>>=vanish_date;
    }
  }

//--------------------------------------------------

ctArchive& operator>>=(ctArchive& arch, SUserInfo &nfo)
  {
  nfo.Serialize(arch);
  return arch;
  }

//--------------------------------------------------

bool SUserInfo::SetUserState(EUserState st)
  {
  if (st==UST_Invisible)
    {
    if (state==UST_Offline || state==UST_Invisible) return false;
    state=UST_Offline;
    }
  else
    {
    if (state==st) return false;
    state=st;
    }
  return true;
  }

//--------------------------------------------------

bool CUserList::RemoveDeleted()
  {
  bool out=false;
  for (int i=0;i<Size();i++)
    {
    if ((*this)[i].state==UST_Deleted) 
      {Delete(i);i--;out=true;}
    }
  return out;
  }

//--------------------------------------------------

int SUserInfo::SortWeight() const
  {
  if (state==UST_Deleted) return 0;
  if (state==UST_Offline) return 1;
  if (state==UST_Deaf) return 2;
  if (state==UST_Away) return 3;
  if (state==UST_Gaming) return 4;
  return 5;
  }

//--------------------------------------------------

int CUserList::FindUserByID(DWORD majid, DWORD minid)
  {
  for (int i=0;i<Size();i++) if ((*this)[i].majid==majid && (*this)[i].minid==minid ) return i;
  return -1;
  }

//--------------------------------------------------

int CUserList::FindByIdentity(const char *identity)
  { 
  int score;
  int best=-1;
  int bestscore=-1;
  for (int i=0;i<Size();i++) 
  {
    SUserInfo &user=(*this)[i];
    if (user.email && stricmp(user.email,identity)==0)    
      if (user.state==UST_Online) return i;
      else
      {
        switch (user.state)
        {
        case UST_Offline:score=0;break;
        case UST_Away: score=2;break;
        case UST_Deaf: score=1;break;
        case UST_Deleted: score=0;break;
        default: score=3;
        }
        if (score>bestscore)
        {
          bestscore=score;
          best=i;
        }
      }
  }
  return best;
  }

void CUserList::CheckVanish()
{
  SYSTEMTIME systm;
  DWORD today;
  GetSystemTime(&systm);
  today=MAKELPARAM(MAKEWORD(systm.wDay,systm.wMonth),systm.wYear);

  for (int i=0;i<Size();i++)
    if ((*this)[i].vanish_date<today) this->Delete(i--);
}