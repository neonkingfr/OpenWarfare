// MainWindow.h: interface for the CMainWindow class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINWINDOW_H__B427EE00_EC8E_4BE9_95DF_EEE5C6C7C320__INCLUDED_)
#define AFX_MAINWINDOW_H__B427EE00_EC8E_4BE9_95DF_EEE5C6C7C320__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



#include "DialogClass.h"
#include "UserList.h"

class CChatWindow;

class CMainWindow : public CDialogClass  
  {
  HWND list;
  HWND state;
  POINT _dragPt;
  int _dragCnt;  
  HBITMAP framebgr;
  HPALETTE framepal,oldpal;
  HDC framebgrdc;
  HFONT listfont;
  bool paintbg;
  HANDLE console;
  HWND _captureWindow;
  bool _showIPs;
  char quicksearch[32];
  public:
	  void OpenDump();
	  void ReloadBgPic();
    BOOL IsSubclassMsg(MSG *msg);
    void SetSelectionNotifyMsg();
    LRESULT OnOK();
    void CheckPrivateGroup(int id);
    void OpenChannel(CChatWindow *towindow=NULL);
    void UpdateList();
    LRESULT OnContextMenu(HWND hWnd,short xpos,short ypos);
    void SetState(EUserState st);
    LRESULT OnCancel();
    void LoadUsersAndGroups();
    LRESULT OnDrawList(int idCtl,LPDRAWITEMSTRUCT lpdis);
    LRESULT OnDrawStates(int idCtl,LPDRAWITEMSTRUCT lpdis);
    LRESULT OnMeasureItem(UINT idCtl, LPMEASUREITEMSTRUCT lpmis);
    LRESULT OnInitDialog();
    CMainWindow();
    virtual ~CMainWindow();
    
    LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
    LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
    LRESULT OnSize(int cx, int cy, int rx, int ry, int flags);
    

    bool HandleCapture(bool select);
//    void SavePosition();
    
    
  };

//--------------------------------------------------

#endif // !defined(AFX_MAINWINDOW_H__B427EE00_EC8E_4BE9_95DF_EEE5C6C7C320__INCLUDED_)
