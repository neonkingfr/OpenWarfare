#include "stdafx.h"
#include ".\dlgselectidentity.h"
#include "Application.h"
#include "resource.h"

DlgSelectIdentity::DlgSelectIdentity(void)
{
  selected_user=NULL;
}

DlgSelectIdentity::~DlgSelectIdentity(void)
{
  free(const_cast<char *>(selected_user));
}

LRESULT DlgSelectIdentity::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
{
  if (wID==IDC_SWITCH)
  {
    CloseWindow(*this);
    theApp->RaiseMainWnd();
  }
  else return __super::OnCommand(wID,wNotifyCode,clt);
  return 1;
}
LRESULT DlgSelectIdentity::OnInitDialog()
{
  __super::OnInitDialog();
  SetDlgItemText(IDC_IDENTITY,identity);
  EnableWindow(theApp->mainwnd,TRUE);
  SendMessage(theApp->mainwnd,KM_CAPTUREUSERSELECT,0,(LPARAM)this->GetHWND());
  ShowWindow(*this,SW_SHOW);
  SetWindowPos(*this,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  return TRUE;
}


LRESULT DlgSelectIdentity::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
  if (msg==WM_DESTROY)
    SendMessage(theApp->mainwnd,KM_CAPTUREUSERSELECT,0,0);
  if (msg==KM_USERSELECTED)
  {
    SUserInfo *nfo=(SUserInfo *)lParam;    
    SetDlgItemText(IDC_USER,nfo->username);
    EnableWindow(GetDlgItem(IDOK),TRUE);
    if (wParam) PostMessage(*this,WM_COMMAND,IDOK,0);
  }
  return __super::DlgProc(msg,wParam,lParam);
}

LRESULT DlgSelectIdentity::OnOK()
{
  free((void *)selected_user);
  selected_user=GetDlgItemText(IDC_USER);
  return __super::OnOK();
}