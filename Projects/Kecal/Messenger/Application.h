// Application.h: interface for the CApplication class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_APPLICATION_H__58D8856E_024D_425E_8A16_DF5346BB6583__INCLUDED_)
#define AFX_APPLICATION_H__58D8856E_024D_425E_8A16_DF5346BB6583__INCLUDED_

#include "ParalelAgent.h"	// Added by ClassView
#include "StatusBarDlg.h"	// Added by ClassView
#include "MessTable.h"	// Added by ClassView
#include "MainWindow.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WinSharedmemory.h"


#define COMM_IDLE 1 //idle command {list of opened channels terminated by double zero}
#define COMM_REFRESH 3	// request to refresh user list {no params}
#define COMM_GROUPS 5  //sending groups   {grouplist terminated by empty string}
#define COMM_OPENCHAT 6 //open new channel to specific users {channelid + userlist/grouplist}
#define COMM_REQCHANTOPIC 8 //request of channel topic {channelid}
#define COMM_CHANTOPIC 9 //reply channel topic {channelid + topic as string}
#define COMM_SENDMESSAGE 10 //sends message to channel {channelid + text as string}
#define COMM_RELOADGROUPS 11 //request to send all avaible groups
#define COMM_DOWNLOADREQ 12 //request to download version
#define COMM_DOWNLOADINFO 13 //info block for download
#define COMM_DOWNLOADDATA 14 //data block for download
#define COMM_PEERMESSAGE 15 //Message for peer 2 peer comunication
#define COMM_PEERMESSAGEACK 16 //Message for peer 2 peer comunication ack
#define COMM_PEERNOTIFY 17 //Notifies other user about various state.

#define CNTF_CLOSEWINDOW 1
#define CNTF_OPENWINDOW 2
#define CNTF_WRITTINGTEXT 4
#define CNTF_STOPWRITTING 8


#define COMM_SPEAGENTREQUEST 99 // request for SPE agent
#define COMM_SPEAGENTPSTOP 98 // request for SPE agent

#define IDLETIMEOUT 5
#define NATIMEOUT 20

struct ChannelID
  {
  DWORD majorid;
  DWORD minorid;
  
  ChannelID(DWORD major, DWORD minor):majorid(major),minorid(minor) 
    {}
  ChannelID() 
    {}
  friend ctArchive& operator >>=(ctArchive &arch, ChannelID &id) 
    {arch>>=id.majorid;arch>>=id.minorid;return arch;}
  bool operator==(const ChannelID &other) 
    {return majorid==other.majorid && minorid==other.minorid;}
  bool operator!=(const ChannelID &other) 
    {return majorid!=other.majorid || minorid!=other.minorid;}
  bool scan(const char *str) 
    {return sscanf(str,"%X_%X",&majorid,&minorid)==2;}
  int print(char *str, int maxsize) const
    {return _snprintf(str,maxsize,"%08X_%08X",majorid,minorid);}
  };

TypeIsMovable(ChannelID);

//--------------------------------------------------

#include "ChatWindow.h"
#include "HistoryWindow.h"	// Added by ClassView
#include "DownloadControl.h"	// Added by ClassView
#include "VersionUpload.h"	// Added by ClassView
#include "RoomListDlg.h"	// Added by ClassView

//#include "PacketParser.h"
#include "NetEngine.h"
#include "SConfig.h"
#include "userlist.h"
#include "grouplist.h"
#include "PhoneOverNet.h"	// Added by ClassView

struct MiniLogItem
{
  unsigned long event;
  unsigned long value;
};

#define MINILOGMAX 64

struct SendCommandAdditionalInfo
{
  HWND hNotify;
  UINT uMsg;
  SOCKADDR_IN target;
  int retries;
  unsigned long uid;
  bool useUID;
  SendCommandAdditionalInfo(int retries=0):hNotify(0),uMsg(0),retries(retries),uid(0),useUID(false) {memset(&target,0,sizeof(target));}
};

class CApplication: public CNetEngine, public CNetReceiver, public IUserListStateChange
  {	
  int _inMenu;
  SendQueue *_sendQueue;
  public:
    NOTIFYICONDATA nid;
    
    char *cmdline;
    CUserList users;
    CGroupList groups;
    SConfig config;	
    HINSTANCE hInst;	
    EUserState mystate;
    CMessTable messtab;
    DWORD myip;
    HMODULE riched;
    
    EUserState savedstate;
    int idletm;
    POINT lastms;
    

    CChatWindowList chatlist;
    AutoArray<ChannelID> chanlist;
    CHistoryWindow history;
    
    CRoomListDlg roomlst;
    
	HIMAGELIST mnu_images;
    
    int listlock;
    bool needgrpsync;
    
    DWORD reqversion; //cislo verze kterou je potreba nahrat
    DWORD uploadcounter; //citac doby, za kterou bude pozadano o upload
    char *requser; //jmeno uzivatele, ktereho program pozada o novou verzi
    bool restart;
    
    CVersionUpload verupload;
    
    CApplication(HINSTANCE hInst,char *cmdln);
    virtual ~CApplication();
    
    int Init();
    int Run();
    void Done();

    int SendMsgToUser(char *string, char* user);
    
    HANDLE console;

    MiniLogItem miniLog[MINILOGMAX];
    int miniLogPos;


    void AddMimiLog(unsigned long event, unsigned long value);

  public:
	  void OpenConsole();
	  void OutConsole(const char *text, int size);
//    CPhoneOverNet phone;
    void CheckForNotifyMsg(int userpos);
    void MinuteTick();
    void ActivateHotkeys(bool activate);
    CParalelAgent spe;
    void VersionUpload();
    CDownloadControl download;
    CStatusBarDlg statistics;
    void OpenHistory();
    void SynchronizeGroups(ctArchive &arch, char *owner);
    void SendIdle();
    bool SendCommand(const SendCommandAdditionalInfo *ainfo,int command, ...);
    void RenameGroup(int idx);
    void DeleteGroup(int idx);
    void AddGroup();
    void RefreshGroups(bool send=true);
    void RaiseMainWnd();
    CMainWindow mainwnd;
    void SaveConfig();
    void RunStartup(BOOL on);
    void SetMyState(EUserState state, bool save=false);
    void ProcessMenu(POINT pt,bool opentop=true);
    void LoadConfig();
    void UpdateShellIcon(int icon=0);
    void DestroyShellIcon();
    LRESULT OnWmApp(UINT msg, WPARAM wParam, LPARAM lParam);
    
    void LoadUsers();
    void SaveUsers();
    void LoadGroups();
    void SaveGroups();
    
    bool Receive(CNetEngine& net,SOCKADDR_IN &sin);
    
    void LockLists() 
      {listlock++;}
    void UnlockLists() 
      {if (listlock!=0) listlock--;}
    bool IsListLocked() 
      {return listlock!=0;}
    void SendToKecalEntryPoint(const char *identity, const char *topic, bool forcenew);
    virtual LRESULT WinProc(UINT msg, WPARAM wParam, LPARAM lParam);
    void OnQueueTime();
    
    void LogEventChange(EUserState state, const char *user, SOCKADDR_IN &lastKnownIP);
    virtual void UserStateChanged(int id);
  };

//--------------------------------------------------

#define KM_TRAYNOTIFY (WM_APP+1) 
#define KM_RAISEUP (WM_APP+2) 
#define KM_DOUBLEMSG (WM_APP+3)
#define KM_CAPTUREUSERSELECT (WM_APP+4) //LPARAM hwnd capture / NULL = stop capture
#define KM_USERSELECTED (WM_APP+5) //LPARAM pointer to user WPARAM 0 - focus, 1 - select
#define KM_REPLYTOKECAL (WM_APP+6) //LPARAM a WPARAM parametry pro winSharedMemory
//#define KM_TEXT (WM_APP+7) //LPARAM a WPARAM 

extern CApplication *theApp;

bool AskForSound(char *filename, int size, HWND parent);

#endif // !defined(AFX_APPLICATION_H__58D8856E_024D_425E_8A16_DF5346BB6583__INCLUDED_)
