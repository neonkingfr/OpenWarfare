// NotifyMsgDlg.h: interface for the CNotifyMsgDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NOTIFYMSGDLG_H__019946E4_841C_461D_8E98_605C62A9AB5B__INCLUDED_)
#define AFX_NOTIFYMSGDLG_H__019946E4_841C_461D_8E98_605C62A9AB5B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"

class CNotifyMsgDlg : public  CDialogClass  
  {
  public:
    LRESULT OnOK();
    LRESULT OnInitDialog();
    CNotifyMsgDlg();
    virtual ~CNotifyMsgDlg();
    
    const char *vMessage;
    char *oMessage;
  };

//--------------------------------------------------

#endif // !defined(AFX_NOTIFYMSGDLG_H__019946E4_841C_461D_8E98_605C62A9AB5B__INCLUDED_)
