// NetEngine.h: interface for the CNetEngine class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NETENGINE_H__3E395F54_0B6B_4547_82D2_1520FF5E7735__INCLUDED_)
#define AFX_NETENGINE_H__3E395F54_0B6B_4547_82D2_1520FF5E7735__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define SOCKCLASSNAME "BISMessangerSocketWindow"

#include <iostream>
using namespace std;

class CNetEngine;
class CNetReceiver
  {
  public:
    virtual bool Receive(CNetEngine& net,SOCKADDR_IN &sin)=0;
  };










#define RCVMAX 16

class CNetEngine  
  {
  protected:
    HWND notifywnd;
    SOCKET sock;
    char inbuffer[65536];
    char outbuffer[65536];
    SOCKADDR_IN outadr;
    int rcvcount;
    int counter;
    int insize;
    CNetReceiver *rlist[RCVMAX];		
  public:
    HWND FindSelf();
    BOOL RestartService(DWORD address, unsigned short port);
    char * CreatePacket();
    void Send(int charcount, const SOCKADDR_IN *outaddr=NULL);
    bool RemoveReceiver(CNetReceiver *rr);
    bool AddReceiver(CNetReceiver *rr);
    bool Receive();
    virtual LRESULT WinProc(UINT msg, WPARAM wParam, LPARAM lParam);
    char * GetIncomeBuffer() 
      {return inbuffer;}
    int GetIncomeBufferSize() 
      {return insize;}
    char * GetOutcomeBuffer() 
      {return outbuffer;}
    int GetOutcomeBufferSize() 
      {return sizeof(outbuffer);}
    BOOL Create(DWORD addres, unsigned short port, bool debug);
    void Close();
    CNetEngine();	
    virtual ~CNetEngine();
    virtual LRESULT OnWmApp(UINT msg, WPARAM wParam,LPARAM lParam) 
      {return DefWindowProc(notifywnd,msg,wParam,lParam);}
    friend LRESULT CALLBACK CallBackWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    HWND GetNotifyWnd() 
      {return notifywnd;}
    
    ostream *OutStream();
    istream *InStream();
    
    bool SendStream(ostream *out);
    
  };


class SendQueue
{
  size_t _size;
  void*_data;
  int _retries;
  HWND _hwndnotify;
  UINT _msgnotify;
  SOCKADDR_IN _target;
  SendQueue *_next;
  unsigned long _id;
public:
  SendQueue(void *data, size_t sz, bool copyData, HWND hwndNotify, UINT msgNotify, int retries):
      _size(sz),_data(copyData?memcpy(malloc(sz),data,sz):data),_hwndnotify(hwndNotify),_msgnotify(msgNotify),_retries(retries),
        _next(NULL),_id(0) {}
  SendQueue(ostream *out, HWND hwndNotify, UINT msgNotify, int retries);
  ~SendQueue() {free(_data);delete _next;}
  void SetTarget(const SOCKADDR_IN &target) {_target=target;}
  void SetID(unsigned long id) {this->_id=id;}
  SendQueue *AddToQueue(SendQueue *what);
  SendQueue *RemoveFromQueue();
  SendQueue *Run(CNetEngine &net);
  SendQueue *RemoveFromQueueByID(unsigned long _id);
};



#endif // !defined(AFX_NETENGINE_H__3E395F54_0B6B_4547_82D2_1520FF5E7735__INCLUDED_)
