// SettingsDlg.cpp: implementation of the CSettingsDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "SettingsDlg.h"
#include "resource.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COLORREF custcolors[16];


CSettingsDlg::CSettingsDlg()
  {
  oldname=NULL;
  }

//--------------------------------------------------

CSettingsDlg::~CSettingsDlg()
  {
  free(oldname);
  }

//--------------------------------------------------

static int __stdcall EnumFontCB(const LOGFONT *lpelfe, const TEXTMETRIC *lpntme, 
  DWORD flags,LPARAM lParam)
  {
  HWND list=(HWND)lParam;
  SendMessage(list,CB_ADDSTRING,0,(LPARAM)lpelfe->lfFaceName);
  return 1;
  }
 

LRESULT CSettingsDlg::OnInitDialog()
  {
  free(oldname);oldname=NULL;
  strncpy(soundname,cfg->soundfile?cfg->soundfile:"",sizeof(soundname));
  soundname[sizeof(soundname)-1]=0;  
  strncpy(imagename,cfg->skin_image?cfg->skin_image:"",sizeof(imagename));
  imagename[sizeof(imagename)-1]=0;  
  SetButtonName(IDC_BROWSESOUND,soundname);
  SetButtonName(IDC_BROWSEPIC,imagename);
  SendDlgItemMessage(IDC_FONTBOLD, BM_SETIMAGE,IMAGE_ICON,(LPARAM)LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_BOLD)));
  SendDlgItemMessage(IDC_FONTITALIC, BM_SETIMAGE,IMAGE_ICON,(LPARAM)LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_ITALIC)));
  SendDlgItemMessage(IDC_TRANSPARENTCURSOR, BM_SETIMAGE,IMAGE_ICON,(LPARAM)LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_TRANSPARENT)));
  wOk=GetDlgItem(IDC_PORT);
  SendDlgItemMessage(IDC_IPADDRESS,IPM_SETADDRESS,0,htonl(cfg->broadcast));
  SetDlgItemText(IDC_NAME,cfg->myname);
  SetDlgItemInt(IDC_PORT,cfg->port,FALSE);
  CheckDlgButton(*this,IDC_OPENUNKNW,cfg->openunknown?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_OPENNEW,cfg->openfromnew?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_RUNTRAY,cfg->showtray?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_RUNSTARTUP,cfg->runstartup?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_ONTOPMAIN,cfg->mainontop?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_ONTOPCHAT,cfg->chatontop?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_ZVUKPOUZEVPOZADI,cfg->noforesound?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_NOPOPUP,cfg->nopopup?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_FONTBOLD,cfg->skin_fontbold?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_FONTITALIC,cfg->skin_fontitalic?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_TRANSPARENTCURSOR,cfg->skin_transparent?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_LOGEVENTS,cfg->logevents?BST_CHECKED:BST_UNCHECKED);
    {
    char buff[256];
    ChannelID chid(cfg->majid,cfg->minid);
    chid.print(buff,sizeof(buff));
    SetDlgItemText(IDC_UID,buff);
	for (int i=7;i<18;i++)  
	  SendDlgItemMessage(IDC_FONTSIZE,CB_ADDSTRING,0,(LPARAM)itoa(i,buff,10));
    }
  SetDlgItemInt(IDC_FONTSIZE,cfg->skin_fontsize,FALSE);
  SendDlgItemMessage(IDC_HOTKEY,HKM_SETHOTKEY,MAKEWORD(cfg->hotkey._vk,cfg->hotkey._modifiers & ~MOD_WIN),0); 
  if (cfg->hotkey._modifiers & MOD_WIN) CheckDlgButton(*this,IDC_WINKB,BST_CHECKED);

  CheckDlgButton(*this,IDC_SNTF_WRITE,(cfg->notify_flags&CNTF_WRITTINGTEXT)?BST_CHECKED:BST_UNCHECKED);
  CheckDlgButton(*this,IDC_SNTF_WINDOW,(cfg->notify_flags&CNTF_CLOSEWINDOW)?BST_CHECKED:BST_UNCHECKED);
  DialogRules();
  HDC dc=GetDC(*this);
  LOGFONT fnt;
  fnt.lfCharSet=EASTEUROPE_CHARSET;
  fnt.lfFaceName[0]=0;
  fnt.lfPitchAndFamily=0;
  EnumFontFamiliesEx(dc,&fnt,EnumFontCB,(LPARAM)GetDlgItem(IDC_FONTSELECT),0);
  int fnd=SendDlgItemMessage(IDC_FONTSELECT,CB_SELECTSTRING,-1,(LPARAM)(cfg->skin_font));
  ReleaseDC(*this,dc);
  fontcolor=cfg->skin_color;
  langrestart=false;
  return CDialogClass::OnInitDialog();
  }

//--------------------------------------------------

LRESULT CSettingsDlg::OnOK()
  {
  if (GetWindowTextLength(GetDlgItem(IDC_NAME))==0) return 0;
  char *p=GetDlgItemText(IDC_NAME);
  if (stricmp(p,cfg->myname)!=0  && theApp->users.FindUser(p)!=-1)
    {
    MessageBox(*this,StrRes[IDS_USEREXISTS],appname,MB_OK);
    free(p);
    return 0;
    }
  oldname=cfg->myname;  
  cfg->myname=p;
  SendDlgItemMessage(IDC_IPADDRESS,IPM_GETADDRESS,0,(LPARAM)(&(cfg->broadcast)));
  cfg->broadcast=htonl(cfg->broadcast);
  cfg->port=GetDlgItemInt(IDC_PORT,FALSE);
  cfg->openfromnew=IsDlgButtonChecked(*this,IDC_OPENNEW)==BST_CHECKED;
  cfg->openunknown=IsDlgButtonChecked(*this,IDC_OPENUNKNW)==BST_CHECKED;
  cfg->showtray=IsDlgButtonChecked(*this,IDC_RUNTRAY)==BST_CHECKED;
  cfg->runstartup=IsDlgButtonChecked(*this,IDC_RUNSTARTUP)==BST_CHECKED;
  cfg->mainontop=IsDlgButtonChecked(*this,IDC_ONTOPMAIN)==BST_CHECKED;
  cfg->chatontop=IsDlgButtonChecked(*this,IDC_ONTOPCHAT)==BST_CHECKED;
  cfg->noforesound=IsDlgButtonChecked(*this,IDC_ZVUKPOUZEVPOZADI)==BST_CHECKED;
  cfg->nopopup=IsDlgButtonChecked(*this,IDC_NOPOPUP)==BST_CHECKED;
  cfg->skin_fontbold=IsDlgButtonChecked(*this,IDC_FONTBOLD)==BST_CHECKED;
  cfg->skin_fontitalic=IsDlgButtonChecked(*this,IDC_FONTITALIC)==BST_CHECKED;
  cfg->skin_transparent=IsDlgButtonChecked(*this,IDC_TRANSPARENTCURSOR)==BST_CHECKED;
  cfg->logevents=IsDlgButtonChecked(*this,IDC_LOGEVENTS)==BST_CHECKED;
  cfg->notify_flags=0;
  if (IsDlgButtonChecked(*this,IDC_SNTF_WRITE)==BST_CHECKED) cfg->notify_flags|=CNTF_WRITTINGTEXT|CNTF_STOPWRITTING;
  if (IsDlgButtonChecked(*this,IDC_SNTF_WINDOW)==BST_CHECKED) cfg->notify_flags|=CNTF_CLOSEWINDOW|CNTF_OPENWINDOW;

  
  BOOL transl=false;
  int fontsize=(int)::GetDlgItemInt(*this,IDC_FONTSIZE,&transl,FALSE);
  if (!transl || fontsize<1 || fontsize>30)
	{
	MessageBox(*this,StrRes[IDS_FONTSIZEERROR],appname,MB_OK|MB_ICONEXCLAMATION);
	return 0;
	}
  cfg->skin_fontsize=fontsize;
  
  free(cfg->soundfile);
  cfg->soundfile=soundname[0]?strdup(soundname):NULL;
  free(cfg->skin_image);
  cfg->skin_image=strdup(imagename);
  free(cfg->skin_font);
  int cursel=SendDlgItemMessage(IDC_FONTSELECT,CB_GETCURSEL,0,0);
  if (cursel!=-1)
	{
	cfg->skin_font=(char *)calloc(SendDlgItemMessage(IDC_FONTSELECT,CB_GETLBTEXTLEN,cursel,0)+1,sizeof(char));
	SendDlgItemMessage(IDC_FONTSELECT,CB_GETLBTEXT,cursel,(LPARAM)cfg->skin_font);
	}
  else
	cfg->skin_font=strdup("");
  cfg->skin_color=fontcolor;
  DWORD hk=SendDlgItemMessage(IDC_HOTKEY,HKM_GETHOTKEY,0,0);
  cfg->hotkey._vk=LOWORD(LOBYTE(hk));
  cfg->hotkey._modifiers=LOWORD(HIBYTE(hk));
  if (IsDlgButtonChecked(*this,IDC_WINKB)==BST_CHECKED) cfg->hotkey._modifiers|=MOD_WIN;
  return CDialogClass::OnOK();
  }

//--------------------------------------------------

LRESULT CSettingsDlg::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  switch (wID)
    {
    case IDC_NAME: DialogRules();return 1;
    case IDC_GENERUJ:
    if (MessageBox(*this,StrRes[IDS_GENERUJASK],appname,MB_OKCANCEL|MB_ICONQUESTION)==IDOK)
      {
      GenerateUID(cfg->majid,cfg->minid);
      theApp->SendIdle();
      OnCancel();
      }
    break;
    case IDC_BROWSESOUND:
      {
      if (AskForSound(soundname,sizeof(soundname),*this))  SetButtonName(IDC_BROWSESOUND,soundname);
      }
    break;
    case IDC_BROWSECLEAR:
      {
      soundname[0]=0;
      SetButtonName(IDC_BROWSESOUND,soundname);
      }
    break;
    case IDC_BROWSEPIC:
      {
	  char *temp=strcpy((char *)alloca(strlen(StrRes[IDS_BMPFILTER]+1)),StrRes[IDS_BMPFILTER]);
	  char *c=temp;	  
	  while ((c=strchr(c,'|'))) *c++=0;

	  OPENFILENAME ofn;
	  memset(&ofn,0,sizeof(ofn));
	  ofn.lStructSize=sizeof(ofn);
	  ofn.hwndOwner=*this;
	  ofn.lpstrFilter=temp;
	  ofn.lpstrFile=imagename;
	  ofn.nMaxFile=sizeof(imagename)/sizeof(imagename[0]);
	  ofn.lpstrTitle=StrRes[IDS_VYBEROBRAZEK];
	  ofn.Flags=OFN_EXPLORER|OFN_FILEMUSTEXIST|OFN_HIDEREADONLY;
	  if (GetOpenFileName(&ofn)) SetButtonName(IDC_BROWSEPIC,imagename);
      }
    break;
    case IDC_PICCLEAR:
      {
      imagename[0]=0;
      SetButtonName(IDC_BROWSEPIC,imagename);
      }
    break;
	case IDC_DEFFONT:
	  {
	  fontcolor=CLR_DEFAULT;
	  SendDlgItemMessage(IDC_FONTSELECT,CB_SETCURSEL,-1,0);
	  DialogRules();
	  }
	break;
	case IDC_COLOR:
	  {
	  CHOOSECOLOR chclr;
	  memset(&chclr,0,sizeof(chclr));
	  chclr.lStructSize=sizeof(chclr);
	  chclr.hwndOwner=*this;
	  chclr.hInstance=NULL;
	  chclr.rgbResult=fontcolor;
	  chclr.lpCustColors=custcolors;
	  chclr.Flags=CC_SOLIDCOLOR|CC_RGBINIT|CC_FULLOPEN;
	  if (ChooseColor(&chclr))
		{
		fontcolor=chclr.rgbResult ;
		InvalidateRect(GetDlgItem(IDC_FONTSELECT),NULL,TRUE);
		}
	  }
	case IDC_FONTBOLD:
	case IDC_FONTSIZE:
	case IDC_FONTITALIC: InvalidateRect(GetDlgItem(IDC_FONTSELECT),NULL,TRUE);break;
	case IDC_FONTSELECT:DialogRules();break;
	case IDC_LANGUAGE: 
      if (cfg->CheckLanguage(*this,true)) langrestart=true;
      break;
    default: return CDialogClass::OnCommand(wID,wNotifyCode,clt);
    }
  return 0;
  }

//--------------------------------------------------

void CSettingsDlg::DialogRules()
  {
  EnableWindow(GetDlgItem(IDOK),GetWindowTextLength(GetDlgItem(IDC_NAME))!=0);
  BOOL fontset=SendDlgItemMessage(IDC_FONTSELECT,CB_GETCURSEL,0,0)!=-1;
  EnableWindow(GetDlgItem(IDC_FONTBOLD),fontset);
  EnableWindow(GetDlgItem(IDC_FONTITALIC),fontset);
  EnableWindow(GetDlgItem(IDC_FONTSIZE),fontset);
  }

//--------------------------------------------------

void CSettingsDlg::SetButtonName(int idc, const char *name)
  {
  const char *p=strrchr(name,'\\');
  if (p==NULL) p=(char *)name;else p++;
  SetDlgItemText(idc,p);
  }

//--------------------------------------------------


static void DrawCBItem(HWND wnd,LPDRAWITEMSTRUCT lpdis, COLORREF color)
  {
  if (lpdis->itemID<0) return;
  LOGFONT lg;
  memset(&lg,0,sizeof(lg));
  SendMessage(lpdis->hwndItem,CB_GETLBTEXT,lpdis->itemID,(LPARAM)lg.lfFaceName);
  lg.lfHeight=-(int)::GetDlgItemInt(wnd,IDC_FONTSIZE,NULL,FALSE);
  lg.lfWeight=IsDlgButtonChecked(wnd,IDC_FONTBOLD)==BST_CHECKED?FW_BOLD:FW_NORMAL;
  lg.lfItalic=IsDlgButtonChecked(wnd,IDC_FONTITALIC)==BST_CHECKED;
  lg.lfCharSet=EASTEUROPE_CHARSET;
  HFONT fnt=CreateFontIndirect(&lg);
  HFONT old=(HFONT)SelectObject(lpdis->hDC,fnt);
  if (lpdis->itemState & ODS_SELECTED)
	{
	SetBkColor(lpdis->hDC,GetSysColor(COLOR_HIGHLIGHT));
	SetTextColor(lpdis->hDC,GetSysColor(COLOR_HIGHLIGHTTEXT));
	}
  else
	{
	SetBkColor(lpdis->hDC,GetGValue(color)<128?RGB(255,255,255):RGB(0,0,0));
	SetTextColor(lpdis->hDC,color==CLR_DEFAULT?GetSysColor(COLOR_WINDOWTEXT):color);
	}
  ExtTextOut(lpdis->hDC,0,0,ETO_OPAQUE,&lpdis->rcItem,NULL,0,NULL);
  DrawText(lpdis->hDC,lg.lfFaceName,strlen(lg.lfFaceName),&(lpdis->rcItem),DT_VCENTER|DT_SINGLELINE|DT_LEFT|DT_NOPREFIX);  
  SelectObject(lpdis->hDC,old);
  DeleteObject(fnt);
  }


LRESULT CSettingsDlg::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  switch (msg)
	{
	case WM_DRAWITEM: if (wParam==IDC_FONTSELECT)
						{
						DrawCBItem(*this,(LPDRAWITEMSTRUCT) lParam,fontcolor);
						return 1;
						}
	  break;
	default: return CDialogClass::DlgProc(msg,wParam,lParam);break;
	}
  return 0;
  }
