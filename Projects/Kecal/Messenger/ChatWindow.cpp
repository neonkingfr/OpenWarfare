// ChatWindow.cpp: implementation of the CChatWindow class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "ChatWindow.h"
#include "resource.h"
#include <mmsystem.h>
#include <shlobj.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define CHATWINDOW "ChatWindow"

static void DropFile(CChatWindow *wnd, const char *filename)
{
  int spaces=0;
  const char *c=filename;
  char *d;
  while (*c) if ((unsigned)(*c++)<33) spaces++;
  char *buff=(char *)alloca(strlen(filename)+2*spaces+11);
  c=filename;
  strcpy(buff,"file:///");
  d=buff+8;
  while (*c)
  {
	if ((unsigned)(*c)<33) 
	{
	  sprintf(d,"%%%02X",*c);
	  d+=3;
	}
	else if (*c==':') *d++='|';
	else if (*c=='\\') *d++='/';
	else
	  *d++=*c;
  c++;
  }
  *d++=' ';
  *d=0;
  wnd->DropText(buff);  
}


CChatWindow::CChatWindow()
{
  _separator=-1;
  topic=NULL;
  brs=CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
  inviteusr=NULL;
  invitegrp=NULL;
  peerchannel=false;
  hidden=false;
  soundgrp=NULL;
  msgid=rand();
  _writntfsent=false;
}










CChatWindow::~CChatWindow()
{
  free(soundgrp);
  free(topic);
  DeleteObject(brs);
  free(inviteusr);
  free(invitegrp);
  if (hidden)
  {
    Shell_NotifyIcon(NIM_DELETE,&nid);
  }  
  if (_drop)
  {
    _drop->Revoke();
    _drop->Release();
    _drop2->Revoke();
    _drop2->Release();
    _drop=NULL;
  }
}










LRESULT CChatWindow::OnInitDialog()
{
  MINMAXINFO nfo;
  GetMinMaxInfo(nfo);
  nfo.ptMinTrackSize.x=300;
  nfo.ptMinTrackSize.y=150;
  SetMinMaxInfo(nfo);

  SetClassLong(*this,GCL_HICON,(LONG)LoadIcon(theApp->hInst,MAKEINTRESOURCE(IDI_MAIN)));

  riched=GetDlgItem(IDC_CHATWND);
  SendMessage(riched,EM_SETTEXTMODE,TM_RICHTEXT|TM_SINGLELEVELUNDO,0);
  SendMessage(riched,EM_AUTOURLDETECT,TRUE,0);
  SendMessage(riched,EM_SETEVENTMASK ,0 ,ENM_LINK );

  CDialogClass::OnInitDialog();

  WINDOWPLACEMENT wp;
  HKEY key=OpenProfileKey();
  {
    ctArchiveInReg arch(key,CHATWINDOW);
    if (!(!arch))
    {
      arch.serial(&wp,sizeof(wp));
      if (!(!arch))
        MoveWindow(*this,wp.rcNormalPosition.left,wp.rcNormalPosition.top,wp.rcNormalPosition.right-wp.rcNormalPosition.left,wp.rcNormalPosition.bottom-wp.rcNormalPosition.top,FALSE);
      arch>>=_separator;
      if (!(!arch))
        MoveSeparator(_separator);

    }

  }
  RegCloseKey(key);
  //  theApp->SendCommand(1,COMM_REFRESH);  

  nid.cbSize=sizeof(nid);
  nid.hWnd=*this;
  nid.uID=0;
  nid.uFlags=NIF_ICON | NIF_MESSAGE | NIF_TIP;
  nid.uCallbackMessage=WM_APP;
  nid.hIcon=LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_MSGWINDOW));
  GetWindowText(*this,nid.szTip,sizeof(nid.szTip));

  CheckWindowVisibility(*this);
  //  RegisterDragDrop(*this,this);
  _drop=new CChatWindowDrop(this,  GetDlgItem(IDC_MESSAGE),false);
  _drop2=new CChatWindowDrop(this,  *this,true);
    
  return FALSE;
}










LRESULT CChatWindow::OnOK()
{
  if (theApp->mystate==UST_Offline)
  {
    MessageBox(*this,StrRes[IDS_NESMISBYTOFFLINE],appname,MB_OK|MB_ICONSTOP);
    return 1;
  }
  char *p=GetDlgItemText(IDC_MESSAGE);
  if (p[0]==0) 
  {free(p);return 1;}
  if (peerchannel)
  {
    _writntfsent=false;
    int user=theApp->users.FindUserByID(chan.majorid,chan.minorid);
    //LogF("User: %d %s",user, ((theApp)->users)[user].username);
    if (user>=0)
    {
      EUserState st=theApp->users[user].state;
      if (st==UST_Offline || st==UST_Deaf)
      {
        SystemMessage(StrRes[IDS_SENDMSGUSEROFFLINE]);
        char *old=theApp->users[user].offlinemsg;
        int newln=(old?strlen(old):0)+strlen(p)+10;
        char *nw=(char *)malloc(newln);
        strcpy(nw,old);
        if (nw[0] && nw[strlen(nw)-1]!='\n') strcat(nw,"\r\n");
        sprintf(strchr(nw,0),"%s\r\n",p);
        theApp->users[user].SetNotifyMsg(nw);
        theApp->SaveUsers();
        free(nw);
        user=-1;    
        SetDlgItemText(IDC_MESSAGE,"");
      }
    }
    if (user>=0)
    {
      SendCommandAdditionalInfo nfo(20);
      nfo.target=theApp->users[user].lastknownip;                        
      if (theApp->users[user].version>=40) 
      {
        nfo.hNotify=*this;
        nfo.uMsg=CCHW_DELIVERYSTATUS;
      }
      else PostMessage(*this,CCHW_DELIVERYSTATUS,1,0);
      EnableWindow(GetDlgItem(IDC_MESSAGE),FALSE);
      EnableWindow(GetDlgItem(IDOK),FALSE);
      theApp->SendCommand(&nfo,COMM_PEERMESSAGE,&this->chan,p);
    }
  }
  else
  {
    if (topic==NULL) InitTopic(chan,p,theApp->config.myname);
    if (inviteusr || invitegrp)		
      SendOpenForNew();	
    theApp->SendCommand(&SendCommandAdditionalInfo(10),COMM_SENDMESSAGE,&this->chan,p);
    SetDlgItemText(IDC_MESSAGE,"");
  }
  free(p);
  return 1;
}










LRESULT CChatWindow::OnCancel()
{
  if (peerchannel || Alone())
  {
    if (peerchannel && GetWindowTextLength(GetDlgItem(IDC_MESSAGE))!=0)
    {
      if (MessageBox(*this,StrRes[IDS_CLOSEMSGWINDOWQUERY],appname,MB_YESNO|MB_DEFBUTTON2|MB_ICONQUESTION)==IDNO) return 0;
    }
    SendPeerNotify(CNTF_CLOSEWINDOW);
    SavePosition();
    SetDlgItemText(IDC_MESSAGE,"");
    ShowWindow(*this,SW_HIDE);
  }
  else
    ToTaskBar(true);

  /*  ShowWindow(*this,SW_HIDE);
  int p=MessageBox(*this,StrRes[IDS_CLOSEWARN],appname,MB_YESNOCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2);
  if (p==IDCANCEL) return 1;
  if (p==IDNO) {CloseWindow(*this);return 1;}*/
  return 1;
}










LRESULT  CChatWindow::OnNotifyRichEditLink(ENLINK *lnk)
{
  if (lnk->msg==WM_LBUTTONDOWN)
  {
    char *text=(char *)alloca(lnk->chrg.cpMax-lnk->chrg.cpMin+10);
    TEXTRANGE txr;
    txr.chrg=lnk->chrg;
    txr.lpstrText=text;
    SendMessage(riched,EM_GETTEXTRANGE,0,(LPARAM)(&txr));
    ShellExecute(NULL,NULL,text,NULL,NULL, SW_NORMAL);
    return 1;
  }
  return 0;
}










LRESULT CChatWindow::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch (msg)
  {
  case WM_DESTROY:
    if (_drop)
    {
      _drop->Revoke();
      _drop->Release();
      _drop2->Revoke();
      _drop2->Release();
      _drop=NULL;
    }
    break;
  case WM_NOTIFY:
    {
      NMHDR *hdr=(LPNMHDR)lParam;
      if (hdr->code==EN_LINK)
      {
        return OnNotifyRichEditLink((ENLINK *)hdr);
      }
    }
    return 0;
  case WM_ACTIVATE:
    if (peerchannel && GetWindowTextLength(GetDlgItem(IDC_MESSAGE))>0 
      && LOWORD(wParam)==WA_INACTIVE && _writntfsent) 
      {SendPeerNotify(CNTF_STOPWRITTING);_writntfsent=false;}    
    if (LOWORD(wParam)!=WA_INACTIVE && !hidden) StopBlink();return 1;
  case WM_DRAWITEM: if (wParam==IDC_USERLIST) return OnDrawListBoxItem((LPDRAWITEMSTRUCT)lParam);
  case WM_MEASUREITEM:
    {
      LPMEASUREITEMSTRUCT lpmis = (LPMEASUREITEMSTRUCT) lParam;
      lpmis->itemHeight=16;
      return TRUE;
    }
  case WM_CTLCOLORLISTBOX:return (DWORD)brs;
  case WM_APP:	  	
    if (lParam==WM_LBUTTONDBLCLK) ToTaskBar(false);
    if (lParam==WM_RBUTTONDOWN) TaskMenu();
    return TRUE;	
  case WM_LBUTTONDOWN:
    BeginMove(lParam);
    break;
  case WM_MOUSEMOVE:
    if (GetCapture()==*this) MoveSeparator(lParam);
    break;
  case WM_LBUTTONUP:
    if (GetCapture()==*this) ReleaseCapture();
    break;
  case WM_SETCURSOR:
    {
      LPARAM lp;
      lp=GetMessagePos();
      POINT pt=
      {GET_X_LPARAM(lp),GET_Y_LPARAM(lp)};
      ScreenToClient(*this,&pt);
      if (OnSeparator(pt))
      {
        SetCursor(LoadCursor(NULL,IDC_SIZENS));
        return TRUE;
      }
      else return CDialogClass::DlgProc(msg,wParam,lParam);	
    }
  case WM_PAINT:
    if (peerchannel)
    {
      PAINTSTRUCT ps;
      HDC dc=BeginPaint(*this,&ps);
      RECT rc;
      GetClientRect(GetDlgItem(IDC_USERSTATUS),&rc);
      POINT pt;pt.x=rc.left;pt.y=rc.top;
      ::MapWindowPoints(GetDlgItem(IDC_USERSTATUS),*this,&pt,1);
      int us=theApp->users.FindUserByID(chan.majorid,chan.minorid);
      if (us>=0)
      {
        HICON icn=(HICON)LoadImage(theApp->hInst,MAKEINTRESOURCE(IDI_OFFLINE+theApp->users[us].state),IMAGE_ICON,16,16,LR_SHARED);
        DrawIconEx(dc,pt.x,pt.y,icn,16,16,0,NULL,DI_NORMAL);
      }
      EndPaint(*this,&ps);
    }
    else
      return CDialogClass::DlgProc(msg,wParam,lParam);	
    break;
  case CCHW_DELIVERYSTATUS:
    {
      if (wParam==0) PostMessage(*this, CCHW_DELIVERYSTATUS,0xFF,0);
      if (wParam==1) 
      {
        char *text=GetDlgItemText(IDC_MESSAGE);
        ReceiveText(theApp->config.myname,text,true);
        SetDlgItemText(IDC_MESSAGE,"");
      }
      if (wParam==0xff)
      {
        int user=theApp->users.FindUserByID(chan.majorid,chan.minorid);
        if (user>=0)
        {
          theApp->users[user].state=UST_Deaf;
          InvalidateRect(*this,NULL,TRUE);
        } 
        MessageBox(*this,StrRes[IDS_DELIVERYFAILED],appname,MB_OK|MB_ICONSTOP);      
      }
      EnableWindow(GetDlgItem(IDOK),true);
      EnableWindow(GetDlgItem(IDC_MESSAGE),true);
      SetFocus(GetDlgItem(IDC_MESSAGE));
      return 1;
    }
    break;
  default: return CDialogClass::DlgProc(msg,wParam,lParam);	
  }
  return 1;
}










LRESULT CChatWindow::OnSize(int cx, int cy, int rx, int ry, int flags)
{  
  HDWP dwp=BeginDeferWindowPos(10);
  MoveWindowRel(riched,0,0,rx,ry,dwp);
  MoveWindowRel(GetDlgItem(IDC_MESSAGE),0,ry,rx,0,dwp);
  MoveWindowRel(GetDlgItem(IDOK),rx,ry,0,0,dwp);
  MoveWindowRel(GetDlgItem(IDC_HISTORY),rx,ry,0,0,dwp);
  MoveWindowRel(GetDlgItem(IDC_USERSTATUS),rx,ry,0,0,dwp);
  MoveWindowRel(GetDlgItem(IDC_USERLIST),rx,0,0,ry,dwp);
  MoveWindowRel(GetDlgItem(IDC_TYPING),rx,ry,0,0,dwp);
  HWND leavech=GetDlgItem(IDC_LEAVECHAT);
  if (leavech) MoveWindowRel(leavech,rx,ry,0,0,dwp);
  if (peerchannel) 
    InvalidateRect(GetDlgItem(IDC_USERLIST),NULL,TRUE);
  EndDeferWindowPos(dwp);
  UpdateUserStatus();
  _separator+=ry;
  return 1;
}










BOOL CChatWindowList::PreTranslateMessage(MSG *msg)
{
  for (int i=0;i<Size();i++)
  {
    if (GetParent(msg->hwnd)==*(*this)[i] && msg->message==WM_KEYDOWN && msg->wParam==VK_RETURN && GetKeyState(VK_CONTROL) & 0x80)
    {
      SendMessage(*(*this)[i],WM_COMMAND,IDOK,0);
      return TRUE;
    }
    if (IsDialogMessage(*(*this)[i],msg)) return TRUE;
  }
  return FALSE;
}





static void PerParagraphSetText(const char *text, HWND riched)
{
  char *copypg=(char *) alloca(strlen(text)+2);
  strcpy(copypg,text);
  char *c=copypg;
  while (*c) {if (*c=='\r') *c=0;c++;}
  c[1]=0;
  c=copypg;
  do
  {
    if (c[0]=='>') RichEdit_SetCurrentFormat(riched,CFM_ITALIC|CFM_BOLD|CFM_SIZE|CFM_FACE,CFE_ITALIC,0,"Verdana",170);
    else RichEdit_SetCurrentFormat(riched,CFM_ITALIC|CFM_BOLD|CFM_FACE|CFM_SIZE,0,0,"Verdana",200);
    RichEdit_ReplaceSel(riched,c);
    c=strchr(c,0);
    RichEdit_ReplaceSel(riched,"\r\n");
    c++;
  }
  while (*c++);
}




void CChatWindow::ReceiveText(const char *from, const char *text, bool myself, bool nohistory)
{
  char buff[256];
  COLORREF cref;
  if (myself) cref=RGB(0,0,255);else cref=RGB(255,0,0);
  SYSTEMTIME systm;
  GetLocalTime(&systm);
  _snprintf(buff,ArrLength(buff),"%s (%02d:%02d): \r\n",from,systm.wHour,systm.wMinute);
  history.MiniLog(10);
  CHARRANGE rg;
  rg.cpMin=rg.cpMax=GetWindowTextLength(riched);
  SendMessage(riched,EM_EXSETSEL,0,(LPARAM)(&rg));
  RichEdit_SetCurrentFormat(riched,CFM_BOLD|CFM_COLOR|CFM_SIZE|CFM_FACE,CFE_BOLD,cref,"Verdana",220);
  RichEdit_ReplaceSel(riched,buff);
  RichEdit_SetCurrentFormat(riched,CFM_BOLD|CFM_COLOR|CFM_SIZE|CFM_FACE,CFE_AUTOCOLOR,cref,"Verdana",200);
  PerParagraphSetText(text,riched);
  history.MiniLog(11);
  RichEdit_ReplaceSel(riched,"\r\n");
  PostMessage(riched,WM_VSCROLL,MAKEWPARAM(SB_BOTTOM,0),NULL);
  PostMessage(riched,WM_VSCROLL,MAKEWPARAM(SB_ENDSCROLL,0),NULL);
  if (!myself) StartBlink();
  /*  else 
  {
  ShowWindow(*this,SW_SHOW);
  SetWindowPos(*this,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  if (!theApp->config.chatontop) SetWindowPos(*this,HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  }*/
  history.MiniLog(12);
  int pos=theApp->users.FindUser(from);
  if (pos!=-1 && theApp->users[pos].state==UST_Offline) 
    theApp->users[pos].state=UST_Invisible;
  InvalidateRect(GetDlgItem(IDC_USERLIST),NULL,FALSE);
  history.MiniLog(13);
  if (!nohistory) history.AddToHistory(from,text);
  if (!myself && (!theApp->config.noforesound || GetForegroundWindow()!=*this))    
  {
    if (soundgrp!=NULL) PlaySound(soundgrp,NULL,SND_ASYNC|SND_FILENAME|SND_NODEFAULT);
    else
      if (theApp->config.soundfile) PlaySound(theApp->config.soundfile,NULL,SND_ASYNC|SND_FILENAME|SND_NODEFAULT);
  }
  StopBlinkTyping();
}










CChatWindow *CChatWindowList::ReceiveText(const char *from, const char *text, bool myself, ChannelID id)
{
  for (int i=0;i<Size();i++) if ((*this)[i]->chan==id) 
  {
    (*this)[i]->ReceiveText(from,text,myself);
    return (*this)[i];
  }
  return NULL;
}










void CChatWindow::CheckUser(char *username, bool present)
{
  if (peerchannel) return;  
  HWND  ulist=GetDlgItem(IDC_USERLIST);
  int i=SendMessage(ulist,LB_FINDSTRINGEXACT,-1,(LPARAM)username);
  if (i==LB_ERR)
  {
    if (present) 
    {
      char buff[256];
      _snprintf(buff,256,StrRes[IDS_ENTERCHAT],username);
      SystemMessage(buff);
      history.AddToHistory(NULL,buff);
      SendMessage(ulist,LB_ADDSTRING,0,(LPARAM)username);
    }
  }
  else
  {
    if (!present) 
    {
      char buff[256];
      _snprintf(buff,256,StrRes[IDS_LEAVECHAT],username);
      SystemMessage(buff);
      history.AddToHistory(NULL,buff);
      SendMessage(ulist,LB_DELETESTRING,i,0);
      if (hidden && Alone()) OnCancel();
    }
  }
}










void CChatWindowList::CheckUser(char *username, ChannelID *idlist, int count)
{
  int pos=theApp->users.FindUser(username);
  if (pos!=-1 && theApp->users[pos].state==UST_Offline) 
    for (int i=0;i<Size();i++) (*this)[i]->CheckUser(username,false);
  else
    for (int i=0;i<Size();i++)
    {
      ChannelID &id=(*this)[i]->chan;
      int j;
      for (j=0;j<count;j++) if (id==idlist[j]) break;
      (*this)[i]->CheckUser(username,j<count);	
    }
}










ChannelID CChatWindowList::CreateChannel(int *users, int *groups)
{
  ChannelID id;
  if (users[0]!=-1 && users[1]==-1 && groups[0]==-1)
  {
    id.majorid=theApp->users[users[0]].majid;
    id.minorid=theApp->users[users[0]].minid;
    for (int i=0;i<Size();i++) if ((*this)[i]->chan==id) 
    {
      if (!IsWindowVisible(*(*this)[i])) ShowWindow(*(*this)[i],SW_SHOWNOACTIVATE);
      if (IsIconic(*(*this)[i])) OpenIcon(*(*this)[i]);
      /*if (!theApp->config.nopopup)*/ BringWindowToTop(*(*this)[i]);
      (*this)[i]->SendPeerNotify(CNTF_OPENWINDOW);
      return id;
    }
    // Peer channel
  }
  GenerateUID(id.majorid,id.minorid);
  CChatWindow *ww=new CChatWindow;
  Append(ww);
  ww->chan=id;
  ww->owner=this;
  if (users[0]!=-1 && users[1]==-1 && groups[0]==-1)
    // Peer channel
  {
    id.majorid=theApp->users[users[0]].majid;
    id.minorid=theApp->users[users[0]].minid;
    ww->Create(IDD_CHATWINDOWPEER,NULL);  
    ww->InitTopic(id,theApp->users[users[0]].username,theApp->config.myname);
    ww->peerchannel=true;	
    ww->SendPeerNotify(CNTF_OPENWINDOW);
  }
  else
  {
    ww->peerchannel=false;
    ww->InviteUsersGroups(users,groups);
    ww->Create(IDD_CHATWINDOW,NULL);  
  }
  ShowWindow(*ww,SW_SHOW);
  /*  ww->InitTopic(id,topic,theApp->config.myname);
  ShowWindow(*ww,SW_SHOW);
  ww->owner=this;
  ww->ReceiveText(theApp->config.myname,topic,true);
  ww->StopBlink();    */
  return id;
}










void CChatWindowList::DestroyChatWindow(CChatWindow *ww)
{
  DestroyWindow(*ww);  
  int i;
  for (i=0;i<Size();i++) if ((*this)[i]==ww) break;  
  if (i<Size()) Delete(i);
  theApp->SendIdle();  
}










static VOID CALLBACK BlinkProc(
                               HWND hwnd,     // handle of window for timer messages
                               UINT uMsg,     // WM_TIMER message
                               UINT idEvent,  // timer identifier
                               DWORD dwTime   // current system time
                               )
{
  CChatWindow *ww=(CChatWindow *)GetWindowLong(hwnd,DWL_USER);
  ww->OnBlinkTimer();
}










void CChatWindow::StartBlink()
{
  SetTimer(*this,10,500,BlinkProc);
}










void CChatWindow::StopBlink()
{
  if (KillTimer(*this,10)) FlashWindow(*this,FALSE);
}










void CChatWindow::OnBlinkTimer()
{
  if (hidden)
  {
    if (blk)
    {
      nid.hIcon=LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_EMPTY));
      blk=!blk;
    }
    else
    {
      nid.hIcon=LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_MSGWINDOW));
      blk=!blk;
    }
    Shell_NotifyIcon(NIM_MODIFY,&nid);
  }
  else
  {
    FlashWindow(*this,TRUE);
    if (GetForegroundWindow()==*this && !hidden) 
      StopBlink();
  }
}










void CChatWindow::Move(CChatWindow *src, CChatWindow *trg)
{
  memcpy(trg,src,sizeof(CChatWindow));
  SetWindowLong(trg->hWnd,DWL_USER,(LONG)trg);
  trg->_drop->ChangeRegisterChatWindow(trg);
  trg->_drop2->ChangeRegisterChatWindow(trg);
}










void CChatWindowList::CreateListOfChannels(AutoArray<ChannelID> &list)
{  
  for (int i=0;i<Size();i++) 
  {
    CChatWindow &ww=*(*this)[i]; 
    list.Add(ww.chan);
  }
}










LRESULT CChatWindow::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
{
  switch (wID)
  {
  case IDC_HISTORY: 
    {
      if (peerchannel)
      {
        CHistoryWindow::OpenHistory(*this,chan);
      }
    else
      theApp->OpenHistory();
    }
    break;
  case IDC_LEAVECHAT: SavePosition();
    owner->DestroyChatWindow(this);
    break;
  case IDC_MESSAGE: 
    if (wNotifyCode==EN_CHANGE && !_writntfsent && peerchannel && GetWindowTextLength(GetDlgItem(wID))>0)    
      {SendPeerNotify(CNTF_WRITTINGTEXT);_writntfsent =true;}
    break;    
  default: return CDialogClass::OnCommand(wID,wNotifyCode,clt);
  }
  return 1;
}










void CChatWindowList::OpenChatWindow(ChannelID id, char *topic, char *from, char *sndgrp)
{
  for (int i=0;i<Size();i++) if ((*this)[i]->chan==id) return;
  CChatWindow *ww=new CChatWindow;  
  Append(ww);
  ww->Create(IDD_CHATWINDOW,NULL);  
  ww->InitTopic(id,topic,from);
  if (sndgrp) ww->soundgrp=strdup(sndgrp);
  ShowWindow(*ww,SW_SHOWNOACTIVATE);  
  ww->owner=this;
  //  ww->ReceiveText(from,topic,false);
  theApp->SendIdle();
  if (theApp->config.nopopup)    
    SetWindowPos(*ww,HWND_BOTTOM,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOSENDCHANGING|SWP_NOACTIVATE);
}










LRESULT CChatWindow::OnDrawListBoxItem(LPDRAWITEMSTRUCT lpdis)
{
  int id=lpdis->itemID;
  if (id==-1) return TRUE;
  HBRUSH bgr;
  bgr=CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
  SetTextColor(lpdis->hDC,GetSysColor(COLOR_WINDOWTEXT));
  char *title=(char *)alloca(SendMessage(lpdis->hwndItem,LB_GETTEXTLEN,id,0)+1);
  SendMessage(lpdis->hwndItem,LB_GETTEXT,id,(LPARAM)title);
  int upos=theApp->users.FindUser(title);
  EUserState state;
  if (upos==-1) 
    if (stricmp(theApp->config.myname,title)==0) state=theApp->mystate;
    else state=UST_Online;
  else state=theApp->users[upos].state;

  SetBkMode(lpdis->hDC,TRANSPARENT);
  SelectObject(lpdis->hDC,GetStockObject(NULL_PEN));
  FillRect(lpdis->hDC,&(lpdis->rcItem),bgr);
  RECT rc=lpdis->rcItem;
  HICON icn=(HICON)LoadImage(theApp->hInst,MAKEINTRESOURCE(IDI_OFFLINE+state),IMAGE_ICON,16,16,LR_SHARED);
  DrawIconEx(lpdis->hDC,lpdis->rcItem.left+1,lpdis->rcItem.top+1,icn,16,16,0,NULL,DI_NORMAL);
  rc.left+=20;
  DrawText(lpdis->hDC,title,-1,&rc,DT_NOPREFIX|DT_END_ELLIPSIS|DT_LEFT|DT_VCENTER);	
  DeleteObject(bgr);
  return 1;
}










void CChatWindow::SetTopic(const char *t)
{
  free(topic);
  int len=GetWindowTextLength(*this)+strlen(t)+20;
  char *buff=(char *)alloca(len);
  GetWindowText(*this,buff,len);
  char *c=strchr(buff,'-');
  if (c==NULL) 
  {strcat(buff," - ");c=strchr(buff,'-');}
  c++;
  sprintf(c," %s",t);
  topic=strdup(t);
  SetWindowText(*this,buff);
  GetWindowText(*this,nid.szTip,sizeof(nid.szTip));
}










void CChatWindow::SavePosition()
{
  WINDOWPLACEMENT wp;
  wp.length=sizeof(wp);
  GetWindowPlacement(*this,&wp);
  HKEY key=OpenProfileKey();
  {
    ctArchiveOutReg arch(key,CHATWINDOW);
    arch.serial(&wp,sizeof(wp));
    arch>>=_separator;
  }
  RegCloseKey(key);
}










void CChatWindow::InitTopic(ChannelID id, const char *topic, const char *from)
{
  chan=id;
  SetTopic(topic);
  history.CreateHistory(topic,from,id);
}










bool CChatWindowList::CheckChannel(ChannelID chan)
{
  for (int i=0;i<Size();i++) if ((*this)[i]->chan==chan) return true;
  return false;
}










void CChatWindow::SystemMessage(const char *text)
{
  CHARRANGE rg;
  rg.cpMin=rg.cpMax=GetWindowTextLength(riched);
  SendMessage(riched,EM_EXSETSEL,0,(LPARAM)(&rg));
  RichEdit_SetCurrentFormat(riched,CFM_ITALIC|CFM_COLOR|CFM_SIZE|CFM_FACE,CFE_ITALIC,RGB(64,32,0),"Verdana",150);
  RichEdit_ReplaceSel(riched,text);
  RichEdit_ReplaceSel(riched,"\r\n");
}










void CChatWindow::InviteUsersGroups(int *users, int *groups)
{
  int p;
  int i;
  char *lcus=inviteusr;
  char *lcgr=invitegrp;
  int lcussz=lcus?strlen(lcus):0;
  int lcgrsz=lcgr?strlen(lcgr):0;
  for (i=0,p=0;users[i]!=-1;i++) p=p+strlen(theApp->users[users[i]].username)+1;
  inviteusr=(char *)malloc(p+1+lcussz);
  for (i=0,p=0;users[i]!=-1;i++)
  {
    sprintf(inviteusr+p,"%s\01",theApp->users[users[i]].username);
    p+=strlen(inviteusr+p);
  }
  inviteusr[p]=0;
  if (lcus)
  {
    strcat(inviteusr,lcus);
    free(lcus);
  }
  for (i=0,p=0;groups[i]!=-1;i++) p=p+strlen(theApp->groups[groups[i]].groupname)+1;
  invitegrp=(char *)malloc(p+1+lcgrsz);
  for (i=0,p=0;groups[i]!=-1;i++) if (!theApp->groups[groups[i]].privat)
  {
    sprintf(invitegrp+p,"%s\01",theApp->groups[groups[i]].groupname);
    p+=strlen(invitegrp+p);
  }
  invitegrp[p]=0;
  if (lcgr)
  {
    strcat(invitegrp,lcgr);
    free(lcgr);
  }
}










void CChatWindow::SendOpenForNew()
{
  int p;
  char *c;
  if (inviteusr) for (p=0,c=inviteusr;*c;c++) if (*c=='\01') 
  {p++;*c=0;}
  if (invitegrp) for (p=0,c=invitegrp;*c;c++) if (*c=='\01') 
  {p++;*c=0;}
  theApp->SendCommand(&SendCommandAdditionalInfo(3),COMM_OPENCHAT,&chan,inviteusr,invitegrp,topic);
  free(inviteusr);
  free(invitegrp);
  inviteusr=NULL;
  invitegrp=NULL;
}










void CChatWindowList::OpenPeerWindow(ChannelID chanel, char *topic, char *from, bool myself)
{
  int i;
  for (i=0;i<Size();i++) if ((*this)[i]->chan==chanel) break;
  CChatWindow *ww;
  if (i==Size())
  {
    ww=new CChatWindow;Append(ww);  
    ww->Create(IDD_CHATWINDOWPEER,NULL);  
    int usr=theApp->users.FindUserByID(chanel.majorid,chanel.minorid);
    ww->InitTopic(chanel,usr!=-1?theApp->users[usr].username:"Unknown user???","");
    ww->owner=this;
    ww->history.MiniLog(6);
    theApp->SendIdle();
    ww->history.MiniLog(7);
    ShowWindow(*ww,SW_SHOWNA);
    if (theApp->config.nopopup)    
      SetWindowPos(*ww,HWND_BOTTOM,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOSENDCHANGING|SWP_NOACTIVATE);
    if (usr>=0 && theApp->users[usr].version<40)
      ww->SystemMessage(StrRes[IDS_OLDERVERSION]);
    ww->SendPeerNotify(CNTF_OPENWINDOW);
  }
  else
  {
    ww=(*this)[i];
    if (!IsWindowVisible(*ww)) 
    {
      ShowWindow(*ww,SW_SHOWNOACTIVATE);
      ww->SendPeerNotify(CNTF_OPENWINDOW);
    }
  }
  ww->peerchannel=true;
  ww->history.MiniLog(8);
  ww->ReceiveText(from,topic,myself);  
  if (!theApp->config.nopopup) BringWindowToTop(*(*this)[i]);

}










void CChatWindow::ToTaskBar(bool state)
{
  if (hidden==state) return;
  hidden=state;
  if (hidden)
  {
    Shell_NotifyIcon(NIM_ADD,&nid);
    ShowWindow(*this,SW_HIDE);
  }
  else
  {
    Shell_NotifyIcon(NIM_DELETE,&nid);
    ShowWindow(*this,SW_SHOW);
  }
}










void CChatWindow::TaskMenu()
{
  POINT pt;
  GetCursorPos(&pt);
  HMENU main=LoadMenu(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_MAINMENU));
  HMENU cur=GetSubMenu(main,3);
  SetFocus(*this);
  SetForegroundWindow(*this);
  int cmd=TrackPopupMenu(cur,TPM_NONOTIFY|TPM_RETURNCMD,pt.x,pt.y,0,*this,NULL);
  switch (cmd)
  {
  case ID_CHATWND_OPEN:ToTaskBar(false);break;
  case ID_CHATWND_ZAVT: 
    SavePosition();
    owner->DestroyChatWindow(this);
    break;
  }
}










bool CChatWindow::Alone()
{
  if (peerchannel) return false;
  HWND  ulist=GetDlgItem(IDC_USERLIST);
  int i=SendMessage(ulist,LB_GETCOUNT,0,0);
  if (i==0) return true;
  if (i>1) return false;
  i=SendMessage(ulist,LB_FINDSTRING,-1,(LPARAM)(theApp->config.myname));
  if (i!=LB_ERR) return true;
  return false;
}










void CChatWindow::BeginMove(LPARAM lParam)
{
  POINT pt;
  pt.x=LOWORD(lParam);
  pt.y=HIWORD(lParam);
  if (OnSeparator(pt))
  {
    SetCapture(*this);
  }
}










bool CChatWindow::OnSeparator(POINT &pt)
{
  RECT rc1,rc2;
  GetClientRect(GetDlgItem(IDC_CHATWND),&rc1);
  GetClientRect(GetDlgItem(IDC_MESSAGE),&rc2);
  ::MapWindowPoints(GetDlgItem(IDC_CHATWND),*this,(POINT *)&rc1,2);
  ::MapWindowPoints(GetDlgItem(IDC_MESSAGE),*this,(POINT *)&rc2,2);
  /*  ScreenToClient(*this,(POINT *)&rc1.left);
  ScreenToClient(*this,(POINT *)&rc1.right);
  ScreenToClient(*this,(POINT *)&rc2.left);
  ScreenToClient(*this,(POINT *)&rc2.right);*/
  if (pt.x>=rc1.left && pt.x<rc1.right)
    if (pt.y>=rc1.bottom && pt.y<rc2.top)
      return true;
  return false;
}










void CChatWindow::MoveSeparator(LPARAM lParam)
{
  POINT pt;
  pt.x=LOWORD(lParam);
  pt.y=HIWORD(lParam);
  RECT rc1,rc2;
  GetWindowRect(GetDlgItem(IDC_CHATWND),&rc1);
  GetWindowRect(GetDlgItem(IDC_MESSAGE),&rc2);
  ScreenToClient(*this,(POINT *)&rc1.left);
  ScreenToClient(*this,(POINT *)&rc1.right);
  ScreenToClient(*this,(POINT *)&rc2.left);
  ScreenToClient(*this,(POINT *)&rc2.right);
  rc1.bottom=pt.y-1;
  rc2.top=pt.y+1;
  if (rc1.bottom-rc1.top<30) return;
  if (rc2.bottom-rc2.top<30) return;
  MoveWindow(GetDlgItem(IDC_CHATWND),rc1.left,rc1.top,rc1.right-rc1.left,rc1.bottom-rc1.top,TRUE);
  MoveWindow(GetDlgItem(IDC_MESSAGE),rc2.left,rc2.top,rc2.right-rc2.left,rc2.bottom-rc2.top,TRUE);
  MINMAXINFO nfo;
  GetMinMaxInfo(nfo);
  nfo.ptMinTrackSize.y=60+rc2.bottom-rc2.top+10;
  SetMinMaxInfo(nfo);
  _separator=lParam;
}










void CChatWindowList::CheckUserPeer(int userpos)
{
  SUserInfo &user=theApp->users[userpos];
  ChannelID chan(user.majid,user.minid);
  for (int i=0;i<Size();i++)
  {
    if ((*this)[i]->chan==chan) (*this)[i]->UpdateUserStatus();
  }
}










void CChatWindow::UpdateUserStatus()
{
  RECT rc;
  GetClientRect(GetDlgItem(IDC_USERSTATUS),&rc);  
  ::MapWindowPoints(GetDlgItem(IDC_USERSTATUS),*this,(POINT *)&rc,2);
  rc.right=rc.left+32;
  rc.bottom=rc.top+32;
  rc.top-=16;
  rc.left-=16;
  InvalidateRect(*this,&rc,TRUE);
}










void CChatWindowList::OnDragSetCursor(POINT &screenpt)
{
  if (GetDropTarget(screenpt)) SetCursor(LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_DRAGARROW)));
  else SetCursor(LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_NODROP)));
}










CChatWindow * CChatWindowList::GetDropTarget(POINT &screenpt)
{
  HWND wnd=WindowFromPoint(screenpt);
  HWND par=GetParent(wnd);
  while (par)
  {
    wnd=par;
    par=GetParent(wnd);
  }
  for (int i=0;i<Size();i++)
    if (*(*this)[i]==wnd)
    {
      return (*this)[i];
    }
    return NULL;
}




CChatWindow * CChatWindowList::PresetText(ChannelID chan, const char *text)
{
  char *buff=strcpy((char *)alloca(strlen(text)+50),text);
  strcat(buff,"\r\n");
  for (int i=0;i<Size();i++) if ((*this)[i]->chan==chan) 
  {
    (*this)[i]->SetDlgItemText(IDC_MESSAGE,buff);
    int len=strlen(buff);
    (*this)[i]->SendDlgItemMessage(IDC_MESSAGE,EM_SETSEL,len,len);
    return ((*this)[i]);
  }
  return NULL;

}



HRESULT CChatWindowDrop::QueryInterface( REFIID riid,void __RPC_FAR *__RPC_FAR *ppvObject)
{
  return E_NOINTERFACE;
}

ULONG CChatWindowDrop::AddRef( void)
{
  return ++refcnt;
}

ULONG CChatWindowDrop::Release( void)
{
  int res=--refcnt;
  if (res==0) delete this;
  return res;
}

static void TRACE1(const char *text, ...)
{
  va_list list;
  va_start(list,text);
  char buff[512];
  _vsnprintf(buff,512,text,list);
  OutputDebugString(buff);
}


HRESULT CChatWindowDrop::DragEnter(IDataObject *pDataObj,DWORD grfKeyState, POINTL pt,DWORD *pdwEffect)
{
  if (pDataObj->QueryGetData(&format)==S_OK || pDataObj->QueryGetData(&filedrop)==S_OK)   
    *pdwEffect=DROPEFFECT_COPY;  
  else 
  {
    return E_UNEXPECTED;
  }
  return S_OK;
}

HRESULT CChatWindowDrop::DragOver( DWORD grfKeyState,POINTL pt, DWORD *pdwEffect)
{  
  if (!_noedit)
  {
    SetFocus(_regWindow);
    POINT ptt;
    ptt.x=pt.x;
    ptt.y=pt.y;
    ScreenToClient(_regWindow,&ptt);
    int charpos=LOWORD(SendMessage(_regWindow,EM_CHARFROMPOS,0,MAKELPARAM(ptt.x,ptt.y)));
    SendMessage(_regWindow,EM_SETSEL,charpos,charpos);
  }
  return 0;
}

HRESULT CChatWindowDrop::DragLeave( void)
{  
  return S_OK;
}

HRESULT CChatWindowDrop::Drop( IDataObject *pDataObj,DWORD grfKeyState, POINTL pt,DWORD *pdwEffect)
{
  STGMEDIUM med;
  /*    IEnumFORMATETC *enumfc;
  if (pDataObj->EnumFormatEtc(DATADIR_GET,&enumfc)==0)
  {
  FORMATETC fc;
  ULONG celt;
  do
  {        
  celt=0;
  if (enumfc->Next(1,&fc,&celt)==S_OK && celt)
  {
  TRACE1("fc.cfFormat=%d(%x)\n",fc.cfFormat,fc.cfFormat);
  TRACE1("fc.dwAspect=%d(%x)\n",fc.dwAspect,fc.dwAspect);
  TRACE1("fc.lindex=%d(%x)\n",fc.lindex,fc.lindex);
  TRACE1("fc.ptd=%d(%x)\n",fc.ptd,fc.ptd);
  TRACE1("fc.tymed=%d(%x)\n",fc.tymed,fc.tymed);
  }
  }
  while (celt);
  enumfc->Release();
  }  */
  if (pDataObj->GetData(&format,&med)==S_OK)
  {
    if (med.tymed==TYMED_HGLOBAL)
    {
      int sz=GlobalSize(med.hGlobal);
      char *mem=(char *)alloca(sz+10);
      char *outbuff=(char *)alloca(sz*3);
      char *endbuff=outbuff+sz*2;      
      void *lock=GlobalLock(med.hGlobal);      
      memcpy(mem,lock,sz);
      strcat(mem,"\r\n");
      char *c=mem;
      char *o=outbuff;
      *o++='>';
      *o++=' ';
      while (*c)
      {
        if (o>=endbuff) 
        {
          strcpy(o,c);
          break;
        }
        *o++=*c;
        if (*c=='\n' && c[1])
        {
          *o++='>';
          *o++=' ';
        }
        c++;
      }
      *o=0;
      GlobalUnlock(med.hGlobal);
      _registred->DropText(outbuff);
      return S_OK;
    }
  }
  else if (pDataObj->GetData(&filedrop,&med)==S_OK)
  {
    if (med.tymed==TYMED_HGLOBAL)
    {
      HDROP drop=(HDROP)med.hGlobal;
      int count=DragQueryFile(drop,0xFFFFFFFF,NULL,0);
      int maxsize=0;
      int i;
      for (i=0;i<count;i++)
      {
        int size=DragQueryFile(drop,i,NULL,0);
        if (size>maxsize) maxsize=size;
      }
      char *buff=(char *)alloca(maxsize+20);
      for (i=0;i<count;i++)
      {
        DragQueryFile(drop,i,buff,maxsize+1);        
        DropFile(_registred,buff);
      }      
      DragFinish(drop);
      return S_OK;
    }
  }

  return E_UNEXPECTED;
}
CChatWindowDrop::CChatWindowDrop(CChatWindow *reg,HWND regWnd,bool noedit)
{
  refcnt=1;
  _registred=reg;
  _regWindow=regWnd;
  format.cfFormat=CF_TEXT;
  format.dwAspect=1;
  format.lindex=-1; 
  format.ptd=0;
  format.tymed=TYMED_HGLOBAL;
  filedrop.cfFormat=CF_HDROP;
  filedrop.dwAspect=1;
  filedrop.lindex=-1;
  filedrop.ptd=0;
  filedrop.tymed=TYMED_HGLOBAL;
  _noedit=noedit;
  RegisterDragDrop(regWnd,this);
}
void CChatWindowDrop::Revoke()
{
  RevokeDragDrop(_regWindow);
  _registred=NULL;
} 


void CChatWindow::DropText(const char *text)
{
  SendDlgItemMessage(IDC_MESSAGE,EM_REPLACESEL,1,(LPARAM)text);
}

void CChatWindowList::PeerNotifyState(ChannelID nchn,int cmd)
{
  for (int i=0;i<Size();i++)
    if ((*this)[i]->chan==nchn && (*this)[i]->peerchannel)
    {
      (*this)[i]->PeerNotifyState(cmd);break;
    }
}

void CChatWindow::PeerNotifyState(int cmd)
{
  const char *text;
  switch (cmd)
  {
  case CNTF_CLOSEWINDOW: text=StrRes[IDS_NTFUSERCLOSESWINDOW];break;
  case CNTF_OPENWINDOW: text=StrRes[IDS_NTFUSEROPENSWINDOW];break;
  case CNTF_WRITTINGTEXT: StartBlinkTyping();return;
  case CNTF_STOPWRITTING: StopBlinkTyping();return;
  default: return;
  }
  SystemMessage(text);
}

void CChatWindow::SendPeerNotify(int notify)
{
  if ((notify & theApp->config.notify_flags)==0) return;
  SendCommandAdditionalInfo retries(5);
  int i=theApp->users.FindUserByID(chan.majorid,chan.minorid);
  if (i==-1) return;
  retries.target=theApp->users[i].lastknownip;    
  theApp->SendCommand(&retries,COMM_PEERNOTIFY,notify,&chan);  
}

static void CALLBACK BlinkTypingWindow(HWND hwnd,
    UINT uMsg,
    UINT_PTR idEvent,
    DWORD dwTime)
{
  HWND hTyp=GetDlgItem(hwnd,IDC_TYPING);
  if ((dwTime % 3000)<250)
    ShowWindow(hTyp,SW_HIDE);
  else
    ShowWindow(hTyp,SW_SHOW);
}


void CChatWindow::StartBlinkTyping()
{
  SetTimer(*this,20,250,BlinkTypingWindow);  
}
void CChatWindow::StopBlinkTyping()
{
  KillTimer(*this,20);
  ShowWindow(GetDlgItem(IDC_TYPING),SW_HIDE);
}
