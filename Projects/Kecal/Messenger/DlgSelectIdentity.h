#pragma once
#include "dialogclass.h"

class DlgSelectIdentity : public CDialogClass
{
public:
  DlgSelectIdentity(void);
  ~DlgSelectIdentity(void);

 const char *identity;
 const char *selected_user;

 virtual LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
 virtual LRESULT OnInitDialog();
 virtual LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
 virtual LRESULT OnOK();
};
