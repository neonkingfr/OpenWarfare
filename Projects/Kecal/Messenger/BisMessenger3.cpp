// BisMessenger3.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "resource.h"
#include "Application.h"
#include "Version.h"

#define COMPILE_MULTIMON_STUBS
#include <multimon.h>

CStringRes StrRes;


void __cdecl _AARangeError(void *,int &)
  {
  abort();
  }

//--------------------------------------------------

void __cdecl _AAMemoryError(void *)
  {
  abort();
  }

//--------------------------------------------------

static int InitWsa()
  {
  WORD wVersionRequested;
  WSADATA wsaData;
  int err;
  
  wVersionRequested = MAKEWORD( 1, 1 );
  
  err = WSAStartup( wVersionRequested, &wsaData );
  if ( err != 0 )  return -1;
  
  /* Confirm that the WinSock DLL supports 2.2.*/
  /* Note that if the DLL supports versions greater    */
  /* than 2.2 in addition to 2.2, it will still return */
  /* 2.2 in wVersion since that is the version we      */
  /* requested.                                        */
  
  if ( LOBYTE( wsaData.wVersion ) != 1 ||
    HIBYTE( wsaData.wVersion ) != 1 ) 
      {
      WSACleanup( );
      return -2; 
      }
  
  /* The WinSock DLL is acceptable. Proceed. */
  
  return 0;
  }

//--------------------------------------------------

int Start(HINSTANCE hInstance)
  {
  InitMultipleMonitorStubs();
  StrRes.SetInstance(hInstance);
  CApplication theApp(hInstance,"");  
  INITCOMMONCONTROLSEX icc;
  icc.dwSize=sizeof(icc);
  icc.dwICC=ICC_INTERNET_CLASSES |ICC_LISTVIEW_CLASSES | ICC_PROGRESS_CLASS |ICC_TREEVIEW_CLASSES | ICC_TAB_CLASSES;
  InitCommonControlsEx(&icc);
  int res=InitWsa();
  if (res) 
    {
    MessageBox(NULL,StrRes[IDS_WSAERROR-res-1],appname,MB_OK|MB_ICONSTOP);
    return -1;
    }  
  int p=theApp.Init();
  if (p) 
    {theApp.Done();return p;}
  p=theApp.Run();
  theApp.Done();
  return p;
  }

//--------------------------------------------------

int GetModuleVersion()
  {
  return VERSION_NR;  
  }

//--------------------------------------------------

