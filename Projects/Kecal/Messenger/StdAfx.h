// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#pragma warning(disable: 4996)

#if !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
#define AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <windowsx.h>
#include <winsock2.h>
#include <strstream>
#include <commdlg.h>
#include <commctrl.h>
#include <multimon.h>
#include <stdarg.h>

#include <memory.h>
#include <tchar.h>
#include <mmsystem.h>
#include <mmreg.h>
#include <msacm.h>

#include <fstream>


#include "ctArchive.h"
#include "ctArchiveRefExt.h"
#include "StringRes.h"
#include ".\geArchiveAA.h"
#define MFC_NEW
#include <es/containers/array.hpp>

#include "shellapi.h"

#include "richedit.h"

extern CStringRes StrRes;

#define appname StrRes[1]

HINSTANCE AfxGetInstanceHandle();

#define ArrLength(a) (sizeof(a)/sizeof(*a))


void MoveWindowRel(HWND hwnd, int xm, int ym, int xsm, int ysm,HDWP dwp);
HKEY OpenProfileKey();
HKEY OpenKey(HKEY base, const char *path);
void RunAtStartup(const char *name, const char *path);

void GenerateUID(DWORD &majid, DWORD &minid);

void RichEdit_ReplaceSel(HWND control,const char *text);
void RichEdit_SetCurrentFormat(HWND control,DWORD mask, DWORD values,DWORD color=0, char *face=0, int size=0);

bool LoadPosition(HWND hWnd, const char *filename, WINDOWPLACEMENT *wpp);
void SavePosition(HWND hWnd, const char *filename);

void CheckWindowVisibility(HWND hWnd);


template <class T>
ctArchive& operator>>=(ctArchive& arch, AutoArray<T>& arr)
  {
  int c=arr.Size();
  arch>>=c;
  if (-arch) 
    {arr.Clear();arr.Access(c-1);}
  for (int i=0;i<c;i++) arch>>=arr[i];
  return arch;
  }



// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
