// WaveIn.cpp: implementation of the CWaveIn class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WaveIn.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWaveIn::CWaveIn()
  {
  handle=NULL;
  }

CWaveIn::~CWaveIn()
  {
  if (handle!=NULL)
	{
    Close();
	}
  }

MMRESULT CWaveIn::Open(HINSTANCE hInst, HWND hMainWnd, UINT uDeviceID, LPWAVEFORMATEX pwfx)
  {
  if (CWaveInOut::Create(hInst, hMainWnd)==FALSE) return 0x80000000;
  MMRESULT mm=waveInOpen(&handle,uDeviceID,pwfx,(DWORD)GetCallbackHandle(),0,CALLBACK_WINDOW);
  if (mm)
	CWaveInOut::Release();
  return mm;
  } 

void CWaveIn::Close()
  {
  Reset();
  waveInClose(handle);
  handle=NULL;
  Release();
  }

MMRESULT CWaveIn::Start()
  {
  WAVEHDR *x=GetHeader();
  while (x)
	{
	waveInPrepareHeader(handle,x,sizeof(WAVEHDR));
	waveInAddBuffer(handle,x,sizeof(WAVEHDR));
	x=GetHeader();
	}
  return waveInStart(handle);
  }

MMRESULT CWaveIn::Reset()
  {
  return waveInReset(handle);
  }

MMRESULT CWaveIn::Stop()
  {
  return waveInStop(handle);
  }

MMRESULT CWaveIn::Pause()
  {
  return Stop();
  }

MMRESULT CWaveIn::Resume()
  {
  return Start();
  }

void CWaveIn::OnBufferDrop(WAVEHDR *hdr)	
  {
  waveInUnprepareHeader(handle,hdr,sizeof(WAVEHDR));
  ReleaseHeader(hdr);
  ReuseLeftBuffers();
  }

void CWaveIn::ReuseLeftBuffers()
  {
  WAVEHDR *x=GetHeader();
  while (x && BufferDrop((char *)(x->lpData),x->dwBytesRecorded))
	{
	waveInPrepareHeader(handle,x,sizeof(WAVEHDR));
	waveInAddBuffer(handle,x,sizeof(WAVEHDR));
	x=GetHeader();
	}
  if (x) ReleaseHeader(x);
  }
