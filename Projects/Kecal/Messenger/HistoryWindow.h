// HistoryWindow.h: interface for the CHistoryWindow class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HISTORYWINDOW_H__06E94F7A_0AFF_4F36_8C27_4FBDCFAE22F7__INCLUDED_)
#define AFX_HISTORYWINDOW_H__06E94F7A_0AFF_4F36_8C27_4FBDCFAE22F7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"
struct ChannelID;

class CHistoryWindow:public CDialogClass
  {
  HWND list;
  HANDLE fhandle;
  char *hpath;
  int cnt;
  public:
    void Sort();
    void OnDelete();
    void LoadHistoryFileInfo(LPWIN32_FIND_DATA dta);
    void OnReload();
    LRESULT OnOK();
    LRESULT OnCancel();
    LRESULT OnInitDialog();
    CHistoryWindow();
    
    virtual LRESULT OnSize(int cx, int cy, int rx, int ry, int flags);
    LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
    LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
    virtual ~CHistoryWindow();

    static void OpenHistory(HWND hWnd,const ChannelID &id);
    
  };

//--------------------------------------------------

#endif // !defined(AFX_HISTORYWINDOW_H__06E94F7A_0AFF_4F36_8C27_4FBDCFAE22F7__INCLUDED_)
