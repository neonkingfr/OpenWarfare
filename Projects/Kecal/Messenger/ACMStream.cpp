// ACMStream.cpp: implementation of the ACMStream class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ACMStream.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ACMStream::ACMStream()
  {
  drv=NULL;
  }

ACMStream::~ACMStream()
  {
  Close();
  }

MMRESULT ACMStream::Open(LPWAVEFORMATEX src, LPWAVEFORMATEX trg)
  {
  nextload=0;
  return acmStreamOpen(&drv,NULL,src,trg,NULL,0,0,0);
  }

void ACMStream::LoadData(const char *data, int size)
  {
  if (nextload+size>ACMMAXBUFF) return;
  memcpy(buffer+nextload,data,size);
  nextload+=size;
  }

MMRESULT ACMStream::ConvertData(unsigned char *out, DWORD maxsize, DWORD &used, DWORD flags)
  {
  ACMSTREAMHEADER hdr;
  memset(&hdr,0,sizeof(hdr));
  hdr.cbStruct=sizeof(hdr);
  hdr.pbSrc=buffer;
  hdr.pbDst=out;
  hdr.fdwStatus=0;
  hdr.cbDstLength=maxsize;
  hdr.cbSrcLength=nextload;
  MMRESULT mm=acmStreamPrepareHeader(drv,&hdr,0);
  if (mm==0) 
	{
	mm=acmStreamConvert(drv,&hdr,flags);
	if (mm) {acmStreamUnprepareHeader(drv,&hdr,0);return mm;}
	}
  if (mm==0) 
	{
	mm=acmStreamUnprepareHeader(drv,&hdr,0);
	memcpy(buffer,buffer+hdr.cbSrcLengthUsed,nextload-hdr.cbSrcLengthUsed);
	nextload-=hdr.cbSrcLengthUsed;
	used=hdr.cbDstLengthUsed;
	}
  return mm;
  }

void ACMStream::Close()
  {
  if (drv) acmStreamClose(drv,0);
  drv=NULL;
  }
