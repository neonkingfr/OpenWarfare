// StatusBarDlg.h: interface for the CStatusBarDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STATUSBARDLG_H__B3A727A6_9994_4D85_8C61_01A79E16D96C__INCLUDED_)
#define AFX_STATUSBARDLG_H__B3A727A6_9994_4D85_8C61_01A79E16D96C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"

class CStatusBarDlg : public CDialogClass  
  {
  DWORD trafic;
  DWORD totalmsg;
  DWORD totalbytes;
  DWORD lastrd;
  bool blk;
  bool tmr;
  public:
    LRESULT OnOK();
    void OnTimer();
    void AddMessageStatistics(DWORD bytes, char *lastuser);
    void BlinkMsg(int id);
    void Refresh(const char *lastuser=NULL);
    LRESULT OnCancel();
    LRESULT OnInitDialog();
    CStatusBarDlg();
    virtual ~CStatusBarDlg();
    
    virtual LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
    virtual LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
  };

//--------------------------------------------------

#endif // !defined(AFX_STATUSBARDLG_H__B3A727A6_9994_4D85_8C61_01A79E16D96C__INCLUDED_)
