// VersionUpload.h: interface for the CVersionUpload class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VERSIONUPLOAD_H__93CC9070_64B4_4B90_9CCF_383D3C601C21__INCLUDED_)
#define AFX_VERSIONUPLOAD_H__93CC9070_64B4_4B90_9CCF_383D3C601C21__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CVersionUpload  
  {
  DWORD p;
  DWORD blk;
  HANDLE ff;
  DWORD i;
  char *buff;
  int timevent;
  bool running;
  public:
    void TickTock();
    void BeginUpload();
    CVersionUpload();
    virtual ~CVersionUpload();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_VERSIONUPLOAD_H__93CC9070_64B4_4B90_9CCF_383D3C601C21__INCLUDED_)
