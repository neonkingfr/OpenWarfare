// WaveOut.cpp: implementation of the CWaveOut class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WaveOut.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWaveOut::CWaveOut()
{
 handle=NULL;

}

CWaveOut::~CWaveOut()
  {
  if (handle!=NULL)
	{
	Close();
	}
  }

MMRESULT CWaveOut::Open(HINSTANCE hInst, HWND hMainWnd, UINT uDeviceID, LPWAVEFORMATEX pwfx)
  {
  if (CWaveInOut::Create(hInst, hMainWnd)==FALSE) return 0x80000000;
  MMRESULT mm=waveOutOpen(&handle,uDeviceID,pwfx,(DWORD)GetCallbackHandle(),0,CALLBACK_WINDOW);
  if (mm)
	CWaveInOut::Release();
  return mm;
  } 

void CWaveOut::Close()
  {
  Reset();
  waveOutClose(handle);
  handle=NULL;
  Release();
  }

MMRESULT CWaveOut::Start()
  {
  ReuseLeftBuffers();
  return 0;
  }

MMRESULT CWaveOut::Reset()
  {
  return waveOutReset(handle);
  }

MMRESULT CWaveOut::Stop()
  {
  return waveOutReset(handle);
  }

MMRESULT CWaveOut::Pause()
  {
  return waveOutPause(handle);
  }

MMRESULT CWaveOut::Resume()
  {
  return waveOutRestart(handle);
  }

void CWaveOut::OnBufferDrop(WAVEHDR *hdr)	
  {
  waveOutUnprepareHeader(handle,hdr,sizeof(WAVEHDR));
  ReleaseHeader(hdr);
  ReuseLeftBuffers();
  }

void CWaveOut::ReuseLeftBuffers()
  {
  WAVEHDR *x=GetHeader();
  while (x && BufferDrop((char *)(x->lpData),x->dwBufferLength))
	{
	waveOutPrepareHeader(handle,x,sizeof(WAVEHDR));
	waveOutWrite(handle,x,sizeof(WAVEHDR));
	x=GetHeader();
	}
  if (x) ReleaseHeader(x);
  }
