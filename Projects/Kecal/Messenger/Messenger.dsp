# Microsoft Developer Studio Project File - Name="Messenger" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=Messenger - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Messenger.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Messenger.mak" CFG="Messenger - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Messenger - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Messenger - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Projects/Kecal/Messenger", QDQAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Messenger - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MESSENGER_EXPORTS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MESSENGER_EXPORTS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib comdlg32.lib advapi32.lib shell32.lib comctl32.lib ws2_32.lib winmm.lib msacm32.lib /nologo /dll /machine:I386 /out:"../Release/Messenger.dll"

!ELSEIF  "$(CFG)" == "Messenger - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MESSENGER_EXPORTS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MESSENGER_EXPORTS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib comdlg32.lib advapi32.lib shell32.lib comctl32.lib ws2_32.lib winmm.lib msacm32.lib /nologo /dll /debug /machine:I386 /out:"../Debug/Messenger.dll" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Messenger - Win32 Release"
# Name "Messenger - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\About.cpp
# End Source File
# Begin Source File

SOURCE=.\ACMStream.cpp
# End Source File
# Begin Source File

SOURCE=..\Application.cpp
# End Source File
# Begin Source File

SOURCE=..\bismessemger.rc
# End Source File
# Begin Source File

SOURCE=..\BisMessenger3.cpp
# End Source File
# Begin Source File

SOURCE=..\BisMessenger3.def
# End Source File
# Begin Source File

SOURCE=..\ChatWindow.cpp
# End Source File
# Begin Source File

SOURCE=..\ctArchive.cpp
# End Source File
# Begin Source File

SOURCE=..\ctArchiveRefExt.cpp
# End Source File
# Begin Source File

SOURCE=..\ctClassInfo.cpp
# End Source File
# Begin Source File

SOURCE=..\ctLongLongTable.cpp
# End Source File
# Begin Source File

SOURCE=..\DialogClass.cpp
# End Source File
# Begin Source File

SOURCE=.\DownloadControl.cpp
# End Source File
# Begin Source File

SOURCE=..\geArchiveAA.cpp
# End Source File
# Begin Source File

SOURCE=..\GroupList.cpp
# End Source File
# Begin Source File

SOURCE=..\GroupNameDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\HistoryFile.cpp
# End Source File
# Begin Source File

SOURCE=..\HistoryWindow.cpp
# End Source File
# Begin Source File

SOURCE=.\LanguageDialog.cpp
# End Source File
# Begin Source File

SOURCE=..\LexAn.cpp
# End Source File
# Begin Source File

SOURCE=..\MainWindow.cpp
# End Source File
# Begin Source File

SOURCE=.\Messenger.cpp
# End Source File
# Begin Source File

SOURCE=..\MessTable.cpp
# End Source File
# Begin Source File

SOURCE=..\NetEngine.cpp
# End Source File
# Begin Source File

SOURCE=.\NotifyMsgDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ParalelAgent.cpp
# End Source File
# Begin Source File

SOURCE=.\PhoneOverNet.cpp
# End Source File
# Begin Source File

SOURCE=.\RoomListDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\SConfig.cpp
# End Source File
# Begin Source File

SOURCE=..\SettingsDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\StatusBarDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\StringRes.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\TemaDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\UserList.cpp
# End Source File
# Begin Source File

SOURCE=.\VersionUpload.cpp
# End Source File
# Begin Source File

SOURCE=.\WaveIn.cpp
# End Source File
# Begin Source File

SOURCE=.\WaveInOut.cpp
# End Source File
# Begin Source File

SOURCE=.\WaveOut.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\About.h
# End Source File
# Begin Source File

SOURCE=.\ACMStream.h
# End Source File
# Begin Source File

SOURCE=..\Application.h
# End Source File
# Begin Source File

SOURCE=..\ChatWindow.h
# End Source File
# Begin Source File

SOURCE=..\ctArchive.h
# End Source File
# Begin Source File

SOURCE=..\ctArchiveRefExt.h
# End Source File
# Begin Source File

SOURCE=..\ctClassInfo.h
# End Source File
# Begin Source File

SOURCE=..\ctLongLongTable.h
# End Source File
# Begin Source File

SOURCE=..\ctWeakRef.h
# End Source File
# Begin Source File

SOURCE=..\DialogClass.h
# End Source File
# Begin Source File

SOURCE=.\DownloadControl.h
# End Source File
# Begin Source File

SOURCE=..\geArchiveAA.h
# End Source File
# Begin Source File

SOURCE=..\geAutoArray.h
# End Source File
# Begin Source File

SOURCE=..\GroupList.h
# End Source File
# Begin Source File

SOURCE=..\GroupNameDlg.h
# End Source File
# Begin Source File

SOURCE=..\HistoryFile.h
# End Source File
# Begin Source File

SOURCE=..\HistoryWindow.h
# End Source File
# Begin Source File

SOURCE=.\LanguageDialog.h
# End Source File
# Begin Source File

SOURCE=..\LexAn.h
# End Source File
# Begin Source File

SOURCE=..\MainWindow.h
# End Source File
# Begin Source File

SOURCE=.\Messenger.h
# End Source File
# Begin Source File

SOURCE=..\MessTable.h
# End Source File
# Begin Source File

SOURCE=..\NetEngine.h
# End Source File
# Begin Source File

SOURCE=.\NotifyMsgDlg.h
# End Source File
# Begin Source File

SOURCE=.\ParalelAgent.h
# End Source File
# Begin Source File

SOURCE=.\PhoneOverNet.h
# End Source File
# Begin Source File

SOURCE=..\resource.h
# End Source File
# Begin Source File

SOURCE=.\RoomListDlg.h
# End Source File
# Begin Source File

SOURCE=..\SConfig.h
# End Source File
# Begin Source File

SOURCE=..\SettingsDlg.h
# End Source File
# Begin Source File

SOURCE=..\StatusBarDlg.h
# End Source File
# Begin Source File

SOURCE=..\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\StringRes.h
# End Source File
# Begin Source File

SOURCE=..\TemaDlg.h
# End Source File
# Begin Source File

SOURCE=..\UserList.h
# End Source File
# Begin Source File

SOURCE=..\version.h
# End Source File
# Begin Source File

SOURCE=.\VersionUpload.h
# End Source File
# Begin Source File

SOURCE=.\WaveIn.h
# End Source File
# Begin Source File

SOURCE=.\WaveInOut.h
# End Source File
# Begin Source File

SOURCE=.\WaveOut.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=..\arrow.cur
# End Source File
# Begin Source File

SOURCE=..\res\biglogo.bmp
# End Source File
# Begin Source File

SOURCE=..\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=..\res\bold.ico
# End Source File
# Begin Source File

SOURCE=..\res\checkin.ico
# End Source File
# Begin Source File

SOURCE=..\res\checkout.ico
# End Source File
# Begin Source File

SOURCE=..\res\Deaf.ico
# End Source File
# Begin Source File

SOURCE=..\res\deleted.ico
# End Source File
# Begin Source File

SOURCE=..\res\empty.ico
# End Source File
# Begin Source File

SOURCE=..\res\gaming.ico
# End Source File
# Begin Source File

SOURCE=..\res\gray.bmp
# End Source File
# Begin Source File

SOURCE=..\res\Hard_work.ico
# End Source File
# Begin Source File

SOURCE=..\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=..\res\italic.ico
# End Source File
# Begin Source File

SOURCE=..\res\menuimag.bmp
# End Source File
# Begin Source File

SOURCE=..\res\msgwin.ico
# End Source File
# Begin Source File

SOURCE=..\res\My_Fish_Blue.ico
# End Source File
# Begin Source File

SOURCE=..\res\My_Fish_Gold.ico
# End Source File
# Begin Source File

SOURCE=..\My_Fish_Green.ico
# End Source File
# Begin Source File

SOURCE=..\res\My_Fish_Green.ico
# End Source File
# Begin Source File

SOURCE=..\res\My_Fish_Orange.ico
# End Source File
# Begin Source File

SOURCE=..\res\My_Fish_Red.ico
# End Source File
# Begin Source File

SOURCE=..\res\newgroup.bmp
# End Source File
# Begin Source File

SOURCE=..\nodrop.cur
# End Source File
# Begin Source File

SOURCE=..\res\notifyms.ico
# End Source File
# Begin Source File

SOURCE=..\res\private.ico
# End Source File
# Begin Source File

SOURCE=..\res\transp.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
