// ctArchive.h: interface for the ctArchive class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTARCHIVE_H__F66DAB8E_9620_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
#define AFX_CTARCHIVE_H__F66DAB8E_9620_11D5_B39F_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <iostream>
#include <assert.h>

using namespace std;

#define CTAF_READING 0x1 //archive is reading stream
#define CTAF_WRITTING 0x2 //archive is writting to the stream
#define CTAF_COUNTING 0x4 //archive is only counting size of objects
#define CTAF_HALTED 0x8 //archive is halted due error. 
#define CTAF_USESTRDUP 0x10 //use strdup instead of new for allocating strings and arrays

#define CTAFE_NOMEMORY -1
#define CTAFE_READERROR -2
#define CTAFE_WRITEERROR -3
#define CTAFE_NOTAGDEFINED -4 //class hasn't defined tag
#define CTAFE_COUNTINGERROR -5 //counting failed
#define CTAFE_UNKNOWNCLASS -6 //unknown class detected in archive
#define CTAFE_EOF -7

class ctArchive;

class ctSerializeClass
  {
  public:
    virtual void Serialize(ctArchive &archive)=0;
    virtual ctSerializeClass *SerializeLink(ctArchive &arch) 
      {return NULL;}
  };










typedef char *CTLPSTR;
typedef short *CTLPSTRW;
typedef ctSerializeClass *CTLPSC;

class ctArchiveEvent
  {
  protected:
    ctArchive *arch;
  public:
    ctArchiveEvent():arch(NULL) 
      {}
    virtual void OnAttach(ctArchive *a) 
      {assert(arch==NULL); arch=a;} //attach event to the archive
    virtual ctArchive *OnDettach() 
      {ctArchive *a=arch;arch=NULL;return a;} //dettach event from archive
    virtual bool OnBeforeLoadClass(CTLPSC &cls) 
      {return false;} //It can supply class serialization by own load routine
    virtual void OnLoadClass(CTLPSC cls) 
      {} //after class is create, but before is serialized
    virtual void OnAfterLoadClass(CTLPSC cls, bool kill) 
      {} //after class is fully serialized. if kill=true then error occured and class is invalid
    virtual bool OnBeforeSaveClass(CTLPSC clss)  
      {return false;} //it can supply class serialization by own save routine
    virtual void OnError(long error, bool halt) 
      {} //when error has occured	
    virtual void OnReportBeginSerialize(unsigned long classtag) 
      {} //when begin class serialization for report
    virtual void OnReportEndSerialize(unsigned long classtag) 
      {} //when end class serialization for report	
    virtual ctArchiveEvent *OnClone()=0; //clone is need
    virtual ~ctArchiveEvent() 
      {}
  };










class ctArchive  
  {
  protected:
    unsigned long version; //current version number of archive
    unsigned long mode; //flags
    long error; //last error
    bool nolink;
    ctArchiveEvent *event;
    unsigned long datamax; //maximalni pocet dat, ktere mohou byt precteny/zapsany
  public:
    unsigned long context; //user defined value	
    void *ptr;
  public:
    //Abstract interface
    ctArchive():context(0),ptr(NULL),mode(0),error(0),version(0),event(NULL),nolink(false),datamax(0xFFFFFFFF) 
      {}
    virtual ~ctArchive() 
      {if (event) DetachEvent();}
    
    virtual void ver(unsigned long version)=0; //sets archive version
    unsigned long ver() 
      {return version;} //returns current archive version
    
    void err(long code, bool halt=true) //sets the error
      {error=code;if (halt) mode|=CTAF_HALTED;if (event) event->OnError(code,halt);}
    unsigned long err() 
      {return error;} //returns last error code
    void clear(bool thaw=true)   //clears error code and thaws stream
      {error=0;if (thaw) mode&=~CTAF_HALTED;}
    
    unsigned long dataMax(unsigned long max) 
      {DWORD cur=datamax;datamax=max;return cur;}
    unsigned long subMax(unsigned long flow) 
      {if (flow>datamax) flow=datamax; datamax-=flow; return flow;}
    virtual void serial(void *ptr, unsigned long size)=0; //makes binary serialization
    
    virtual ctArchive& operator>>=(bool &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(char &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(unsigned char &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(short &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(unsigned short &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(int &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(unsigned int &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(long &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(unsigned long &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(float &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    virtual ctArchive& operator>>=(double &val) 
      {serial((void *)&val,sizeof(val));return *this;}
    
    virtual bool operator<< (bool val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual char operator<< (char val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual unsigned char operator<< (unsigned char val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual short operator<< (short val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual unsigned short operator<< (unsigned short val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual int operator<< (int val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual unsigned int operator<< (unsigned int val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual long operator<< (long val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual unsigned int operator<< (unsigned long val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual float operator<< (float val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual double operator<< (double val) 
      {serial((void *)&val,sizeof(val));return val;}
    virtual char *operator<< (const char *str);
    virtual short *operator<< (const short *str);
    
    virtual void reserve(unsigned long bytes)=0; //reserves/skips number of bytes
    virtual bool check(const char *tagId)=0; //store check mark/check archive mark
    
    void AttachEvent(ctArchiveEvent *ev);
    ctArchiveEvent *DetachEvent();
    ctArchiveEvent *GetEvent();
    
    ctArchive& operator>>=(CTLPSTR& str)
      {str=(*this)<<str;return *this;}
    ctArchive& operator>>=(CTLPSTRW& str)
      {str=(*this)<<str;return *this;}
    
    
    bool operator+() 
      {return (mode & CTAF_WRITTING)!=0;}
    bool operator-() 
      {return (mode & CTAF_READING)!=0;}	
    bool r() 
      {return (mode & CTAF_READING)!=0;} //returns true, if archive is reading the stream
    bool w() 
      {return (mode & CTAF_WRITTING)!=0;} //returns true, if archive is writting to the stream
    bool c() 
      {return (mode & CTAF_COUNTING)!=0;} //returns true, if archive is only counting bytes
    bool operator!() 
      {return (mode & CTAF_HALTED)!=0;} //returns true, if archive is freezed due error
    void useNew() 
      {mode &=~CTAF_USESTRDUP;}  //orders archive to use new operator for allocating strings
    void useMalloc() 
      {mode |=CTAF_USESTRDUP;} //orders archive to use malloc operator for allocating strings
    
    virtual CTLPSC serial(CTLPSC src)=0; //serializes class
  };










#define ctArchVer(arch,verr) if (arch.ver()>=verr)

class ctArchiveCounting: public ctArchive
  {
  struct sCount
    {
    ctSerializeClass *cls;
    unsigned long size;
    };
  sCount *arr;
  int narr;
  int nrd;
  int nwr;
  unsigned long globc;
  void Expand();
  int bpc; //bytes per class id;
  int bpi; //bytes per item;  
  CTLPSC DoSerial(CTLPSC what);
  public:
    void CallSerialize(CTLPSC cls);
    ctArchiveCounting(int bpc,int bpi=0);
    virtual ~ctArchiveCounting();
    virtual void serial(void *ptr, unsigned long size);
    unsigned long size(ctSerializeClass *c);
    unsigned long gsize() 
      {return globc;}
    void Reset();
    virtual CTLPSC serial(CTLPSC src);
    virtual void ver(unsigned long version) 
      {(*this)>>=version;}
    virtual void reserve(unsigned long bytes) 
      {globc+=bytes;}
    virtual bool check(const char *tagId) 
      {(*this)<<tagId;return true;}
  };










#define CTAF_NOSIZES 0x1 //no size information, no counting
#define CTAF_NOTAGS 0x2 //no tags
#define CTAF_UNKNOWNISERROR 0x4 //force report error if unknown class is found.
#define CTAF_FORCEERRORFREEZE 0x8 //forces any error to freeze stream
#define CTAF_DESTROYSTREAM 0x10 //forces to delete stream when finished



class ctArchiveOut: public ctArchive
  {
  ctArchiveCounting *ctp;
  void DoSerial(CTLPSC cls);
  protected:
    unsigned long flags;
  public:
    ctArchiveOut(unsigned long flags=0):flags(flags) 
      {ctp=NULL;mode|=CTAF_WRITTING;}
    virtual ctArchiveCounting *CreateCounter(int bpc,int bpi) 
      {return new ctArchiveCounting(bpc,bpi);}
    virtual CTLPSC serial(CTLPSC src); //serializes class
    virtual ~ctArchiveOut() 
      {delete ctp;}
    virtual void ver(unsigned long version) 
      {this->version=(*this)<<version;}
    virtual bool check(const char *tagId);
  };










class ctArchiveIn: public ctArchive
  {
  protected:
    unsigned long flags;	
    unsigned long lastunknown;
    unsigned long clssize;
    CTLPSC DoSerial(CTLPSC src);
  public:
    ctArchiveIn(unsigned long flags=0):flags(flags) 
      {mode|=CTAF_READING;lastunknown=0;}
    
    virtual CTLPSC serial(CTLPSC src); //serializes class
    virtual void ver(unsigned long version) 
      {this->version=(*this)<<version;}
    virtual bool check(const char *tagId);
  };










class ctSArchiveIn:public ctArchiveIn
  {
  istream &istr;
  public:
    ctSArchiveIn(istream &in, unsigned long flags=0):istr(in),ctArchiveIn(flags) 
      {}
    virtual ~ctSArchiveIn() 
      {if (flags & CTAF_DESTROYSTREAM) delete &istr;}
    virtual void serial(void *ptr, unsigned long size);
    virtual void reserve(unsigned long bytes);
  };










class ctSArchiveOut:public ctArchiveOut
  {
  ostream &ostr;
  public:
    ctSArchiveOut(ostream &out, unsigned long flags=0):ostr(out),ctArchiveOut(flags) 
      {}
    virtual ~ctSArchiveOut() 
      {if (flags & CTAF_DESTROYSTREAM) delete &ostr;}
    virtual void serial(void *ptr, unsigned long size);
    virtual void reserve(unsigned long bytes);
  };










#endif // !defined(AFX_CTARCHIVE_H__F66DAB8E_9620_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
