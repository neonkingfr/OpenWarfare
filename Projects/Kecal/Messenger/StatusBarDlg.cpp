// StatusBarDlg.cpp: implementation of the CStatusBarDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "StatusBarDlg.h"
#include "resource.h"
#include "Application.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStatusBarDlg::CStatusBarDlg()
  {
  lastrd=GetTickCount()/1000;
  trafic=0;
  totalbytes=0;
  totalmsg=0;
  tmr=false;
  }

//--------------------------------------------------

CStatusBarDlg::~CStatusBarDlg()
  {
  
  }

//--------------------------------------------------

LRESULT CStatusBarDlg::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  if (msg==WM_DRAWITEM)
    {
    LPDRAWITEMSTRUCT lpdis = (LPDRAWITEMSTRUCT) lParam;
    HBRUSH brs;
    if (blk)
      brs=CreateSolidBrush(RGB(0,255,0));
    else
      brs=CreateSolidBrush(RGB(0,128,0));
    SelectObject(lpdis->hDC,GetStockObject(BLACK_PEN));
    FillRect(lpdis->hDC,&lpdis->rcItem,brs);
    DeleteObject(brs);
    return 1;
    }
  else return CDialogClass::DlgProc(msg,wParam,lParam);
  }

//--------------------------------------------------

LRESULT CStatusBarDlg::OnInitDialog()
  {
  Refresh("<unknown>");
  blk=false;
  return CDialogClass::OnInitDialog();
  }

//--------------------------------------------------

LRESULT CStatusBarDlg::OnCancel()
  {
  DestroyWindow(*this);
  return 1;
  }

//--------------------------------------------------

void CStatusBarDlg::Refresh(const char *lastuser)
  {
  if (*this)
    {
    SetDlgItemInt(IDC_TRAFIC, trafic/10,FALSE);
    SetDlgItemInt(IDC_TOTALMSG, totalmsg,FALSE);
    SetDlgItemInt(IDC_BYTES, totalbytes,FALSE);
    if (lastuser) SetDlgItemText(IDC_LASTUSER,lastuser);
    }
  }

//--------------------------------------------------

VOID CALLBACK Unblink(
                      HWND hwnd,     // handle of window for timer messages
                      UINT uMsg,     // WM_TIMER message
                      UINT idEvent,  // timer identifier
                      DWORD dwTime   // current system time
                      )
    {
    CStatusBarDlg *dlg=static_cast<CStatusBarDlg *>((CDialogClass *)GetWindowLong(hwnd,DWL_USER));
    dlg->OnTimer();
    }

//--------------------------------------------------

void CStatusBarDlg::BlinkMsg(int id)
  {
  if (*this)
    {	
    blk=true;
    InvalidateRect(GetDlgItem(1000+id),NULL,TRUE);
    UpdateWindow(GetDlgItem(1000+id));
    //	KillTimer(*this,10);
    if (!tmr) SetTimer(*this,10,250,Unblink);
    tmr=true;
    blk=false;
    }
  }

//--------------------------------------------------

void CStatusBarDlg::AddMessageStatistics(DWORD bytes, char *lastuser)
  {
  DWORD p=GetTickCount()/1000;
  if (p!=lastrd)
    {
    DWORD persec=trafic*(p-lastrd)/10;
    if (persec>trafic) trafic=0;else
      trafic-=persec;
    lastrd=p;
    }
  trafic+=bytes*8;
  totalmsg++;
  totalbytes+=bytes; 
  if (*this) Refresh(lastuser);
  }

//--------------------------------------------------

void CStatusBarDlg::OnTimer()
  {
  for (int i=0;i<30;i++)
    {
    HWND hWnd=GetDlgItem(1000+i);
    if (hWnd)
      {
      InvalidateRect(hWnd,NULL,TRUE);
      UpdateWindow(hWnd);
      }
    KillTimer(*this,10);
    tmr=false;
    }
  }

//--------------------------------------------------

LRESULT CStatusBarDlg::OnOK()
  {
  totalbytes=0;
  totalmsg=0;
  trafic=0;
  Refresh();
  return 1;
  }

//--------------------------------------------------

static VOID CALLBACK OpenWindowProc(HWND hwnd,     // handle of window for timer messages
                                    UINT uMsg,     // WM_TIMER message
                                    UINT idEvent,  // timer identifier
                                    DWORD dwTime   // current system time
                                    )
  {
  
  ChannelID myid(theApp->config.majid,theApp->config.minid);
  theApp->SendCommand(&SendCommandAdditionalInfo(3),COMM_PEERMESSAGE,&myid,"test");
  KillTimer(hwnd,idEvent);
/*    theApp->SendToKecalEntryPoint("Bredy@bistudio.com","test");*/
  }

//--------------------------------------------------

LRESULT CStatusBarDlg::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  if (wID==IDC_OPENWINDOWTEST)
    {
    SetTimer(*this,1000,5000, OpenWindowProc);
    return 1;
    }
  else
    return CDialogClass::OnCommand(wID,wNotifyCode,clt);
  }

//--------------------------------------------------

