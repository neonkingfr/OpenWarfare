// ctArchive.cpp: implementation of the ctArchive class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ctArchive.h"
#include "ctClassInfo.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

template<class T>
struct stStringList
  {
  stStringList *next;
  T buff[256];
  
  
  bool SetChar(T p, int &pos)
    {buff[pos++]=p;return pos==256;}
  
  ~stStringList() 
    {delete next;}
  
  int GetTotalLen();
  void InitNext(stStringList<T> *q);
  void Concat(T *c);
  };

//--------------------------------------------------

template<class T>
void stStringList<T>::InitNext(stStringList<T> *q)
  {	
  q->next=NULL;
  if (this) next=q;
  }

//--------------------------------------------------

template<class T>
int stStringList<T>::GetTotalLen()
  {
  stStringList *z=this;
  int cnt=1;
  while (z)
    {
    if (z->next) cnt+=256;
    else 
      {
      T *q=buff;
      while (*q++) cnt++;
      }
    z=z->next;
    }
  return cnt;
  }

//--------------------------------------------------

template<class T>
void stStringList<T>::Concat(T *c)
  {
  stStringList *z=this;
  while (z)
    {
    if (z->next) 
      {memcpy(c,z->buff,sizeof(buff));c+=256;}
    else 
      {
      T *q=z->buff;
      while (*q) *c++=*q++;
      *c=0;
      }
    z=z->next;
    }
  }

//--------------------------------------------------

void ctArchive::AttachEvent(ctArchiveEvent *ev)
  {  
  if (event) event->OnDettach();
  event=ev;
  event->OnAttach(this);
  }

//--------------------------------------------------

ctArchiveEvent *ctArchive::DetachEvent()
  {
  if (event==NULL) return NULL;
  ctArchiveEvent *ev=event;
  event=NULL;
  ev->OnDettach();
  return ev;
  }

//--------------------------------------------------

char *ctArchive::operator<< (const char *str)
  {
  if (mode & CTAF_WRITTING)
    {	
    char *q=str?(char *)str:"";
    do (*this)<<*(q);while (*q++);
    }
  if (mode & CTAF_READING)
    {
    int bkcnt=0;
    stStringList<char> stl,*cur=&stl,*del=NULL;
    int chp=0;
    stl.next=0;
    char z=0;
    do
      {
      z=0;
      (*this)>>=z;
      if (cur->SetChar(z,chp))
        {
        bkcnt++;
        stStringList<char> *nw;
        if (bkcnt>10) 
          {
          nw=new stStringList<char>;		  
          if (del==NULL) del=nw;		  
          }
        else
          nw=(stStringList<char> *)alloca(sizeof(stStringList<char>));
        if (nw) 
          {
          cur->InitNext(nw);
          cur=nw;
          }
        else
          {
          err(CTAFE_NOMEMORY,false);
          }
        chp=0;
        }
      }
    while (z);
    int cnt=stl.GetTotalLen();
    if (mode & CTAF_USESTRDUP) str=(char *)malloc(cnt*sizeof(char));
    else str=new char[cnt];
    if (str)
      stl.Concat((char *)str);
    else
      err(CTAFE_NOMEMORY,false);	
    delete del;
    stl.next=NULL;
    }
  return (char *)str;
  }

//--------------------------------------------------

short *ctArchive::operator<< (const short *str)
  {
  if (mode & CTAF_WRITTING)
    {
    const short *q=str?str:((short *)"\0");
    do (*this)<<*q; while (*q++);
    }
  if (mode & CTAF_READING)
    {
    int bkcnt=0;
    stStringList<short> stl,*cur=&stl,*del=NULL;
    stl.next=0;
    int chp=0;
    short z=0;
    do
      {
      z=0;
      (*this)>>=z;
      if (cur->SetChar(z,chp))
        {
        bkcnt++;
        stStringList<short> *nw;
        if (bkcnt>10) 
          {
          nw=new stStringList<short>;		  
          if (del==NULL) del=nw;		  
          }
        else
          nw=(stStringList<short> *)alloca(sizeof(stStringList<short>));
        if (nw) 
          {
          cur->InitNext(nw);
          nw=cur;
          }
        else
          {
          err(CTAFE_NOMEMORY,false);
          }
        chp=0;
        }
      }
    while (z);
    int cnt=stl.GetTotalLen();
    if (mode & CTAF_USESTRDUP) str=(short *)malloc(cnt*sizeof(short));
    else str=new short[cnt];
    if (str)
      stl.Concat((short *)str);
    else
      err(CTAFE_NOMEMORY,false);	
    delete del;
    }
  return (short *)str;
  }

//--------------------------------------------------

ctArchiveCounting::ctArchiveCounting(int bpc,int bpi):bpc(bpc),bpi(bpi)
  {
  mode|=CTAF_WRITTING|CTAF_COUNTING;
  arr=NULL;
  narr=0;
  Reset();
  }

//--------------------------------------------------

ctArchiveCounting::~ctArchiveCounting()
  {
  delete [] arr;
  }

//--------------------------------------------------

void ctArchiveCounting::Expand()
  {
  int nws=narr*2;
  if (nws==0) nws=16;
  sCount *nw=(sCount *)realloc(arr,sizeof(*arr)*nws);
  if (nw==NULL)
    err(CTAFE_NOMEMORY);
  else
    {
    arr=nw;
    narr=nws;
    }
  }

//--------------------------------------------------

void ctArchiveCounting::Reset()
  {  
  nwr=-1;
  globc=0;
  nrd=0;
  }

//--------------------------------------------------

void ctArchiveCounting::serial(void *ptr, unsigned long size)
  {    
  globc+=size+bpi;
  }

//--------------------------------------------------

CTLPSC ctArchiveCounting::serial(CTLPSC src)
  {
  if (event)	
    if (event->OnBeforeSaveClass(src)) return src;		
  CTLPSC cur=src;
  bool lnk=!nolink;nolink=false;
  DoSerial(cur);
  if (lnk) 
    do
      {
      nolink=true;
      cur=cur->SerializeLink(*this);
      }
  while (cur && !nolink);
  return src;
  }

//--------------------------------------------------

CTLPSC ctArchiveCounting::DoSerial(CTLPSC src)
  {
  if (operator!()) return src;
  globc+=bpc;
  CallSerialize(src);
  return src;
  }

//--------------------------------------------------

unsigned long ctArchiveCounting::size(ctSerializeClass *c)
  {
  if (nrd>nwr) return 0xFFFFFFFF;
  if (arr[nrd].cls!=c) return 0xFFFFFFFF;
  return arr[nrd++].size;
  }

//--------------------------------------------------

CTLPSC ctArchiveOut::serial(CTLPSC src)
  {
  if (event)	
    if (event->OnBeforeSaveClass(src)) return src;		
  CTLPSC cur=src;
  bool lnk=!nolink;nolink=false;
  DoSerial(cur);
  if (lnk && cur) 
    do
      {
      nolink=true;
      cur=cur->SerializeLink(*this);
      }
  while (cur && !nolink);
  return src;
  }

//--------------------------------------------------

void ctArchiveOut::DoSerial(CTLPSC cls)
  {
  unsigned long tag=0;
  if (mode & CTAF_HALTED) return;
  if (!(flags & CTAF_NOTAGS))
    {
    if (cls)
      {
      ctClassInfo *nfo=ctClassTable::GetClass(cls);
      assert(nfo!=NULL);   //serialized class must be registred.
      if (nfo==NULL) tag=0;else tag=(unsigned)(nfo->GetDUID());
      if (tag==0) 	err(CTAFE_NOTAGDEFINED, (flags &CTAF_FORCEERRORFREEZE)!=0);		
      (*this)>>=tag;
      }
    else
      {
      tag=0;
      (*this)>>=tag;
      }
    }
  if (!(flags & CTAF_NOSIZES))
    {
    unsigned long size;
    if (ctp==NULL)
      {
      size=8;
      if (flags & CTAF_NOTAGS) size-=4;
      if (flags & CTAF_NOSIZES) size-=4;
      ctp=CreateCounter(size,0);
      }
    if ((size=ctp->size(cls))==0xFFFFFFFF)
      {
      ctp->Reset();
      if (event) ctp->AttachEvent(event->OnClone());
      ctp->CallSerialize(cls);
      if (event) delete ctp->DetachEvent();
      if (ctp->err())err(ctp->err());
      if (mode & CTAF_HALTED) return;
      size=ctp->size(cls);
      if (size==0xFFFFFFFF)
        {
        err(CTAFE_COUNTINGERROR);
        return;
        }
      }
    (*this)>>=size;
    }
  if (cls) 
    {
    unsigned long ctxsave=context;
    unsigned long versave=version;
    if (event) event->OnReportBeginSerialize(tag);
    cls->Serialize(*this);  
    if (event) event->OnReportEndSerialize(tag);
    version=versave;
    context=ctxsave;
    }
  }

//--------------------------------------------------

CTLPSC ctArchiveIn::serial(CTLPSC src)
  {
  if (event)
    {
    CTLPSC evld=src;
    if (event->OnBeforeLoadClass(evld)) return evld;
    }
  CTLPSC cur;
  bool lnk=!nolink;nolink=false;
  src=DoSerial(src);
  cur=src;
  if (lnk && cur) 
    do
      {
      nolink=true;
      cur=cur->SerializeLink(*this);
      }
  while (cur && !nolink);
  return src;
  }

//--------------------------------------------------

CTLPSC ctArchiveIn::DoSerial(CTLPSC src)
  {
  unsigned long tag=0;
  if (mode & CTAF_HALTED) return src;  
  if (!(flags & CTAF_NOTAGS))
    {
    (*this)>>=tag;
    src=NULL;
    if (tag)
      {
      ctClassInfo *nfo=ctClassTable::GetClass((long)tag);
      if (nfo)		
        src=nfo->CreateClass();
      else
        {
        lastunknown=tag;
        if (flags & CTAF_UNKNOWNISERROR) err(CTAFE_NOTAGDEFINED, (flags &CTAF_FORCEERRORFREEZE)!=0);
        }		
      }
    }
  if (!(flags & CTAF_NOSIZES))
    {
    unsigned long size=0;
    (*this)>>=size;
    if (src==NULL)	  
      reserve(size);	
    clssize=size;
    }
  if (event) event->OnLoadClass(src);
  if (src) 
    {
    unsigned long ctxsave=context;
    unsigned long versave=version;
    if (event) event->OnReportBeginSerialize(tag);
    src->Serialize(*this);
    if (event) event->OnReportEndSerialize(tag);
    context=ctxsave;
    version=versave;
    }
  bool kill=mode & CTAF_HALTED && !(flags & CTAF_NOTAGS);
  if (event) event->OnAfterLoadClass(src,kill);
  if (kill)	
    {
    ctClassTable::GetClass(tag)->DestroyClass(src);
    src=NULL;
    }
  return src;
  }

//--------------------------------------------------

void ctSArchiveIn::serial(void *ptr, unsigned long size)
  {
  if (mode & CTAF_HALTED) return;
  size=subMax(size);
  istr.read((char *)ptr,size);
  if (!istr) err(CTAFE_READERROR);
  }

//--------------------------------------------------

void ctSArchiveIn::reserve(unsigned long bytes)
  {
  if (mode & CTAF_HALTED) return;
  bytes=subMax(bytes);
  istr.ignore(bytes);
  if (!istr) err(CTAFE_READERROR);
  }

//--------------------------------------------------

void ctSArchiveOut::serial(void *ptr, unsigned long size)
  {
  if (mode & CTAF_HALTED) return;
  size=subMax(size);
  ostr.write((char *)ptr,size);
  if (!ostr) err(CTAFE_WRITEERROR);
  }

//--------------------------------------------------

void ctSArchiveOut::reserve(unsigned long bytes)
  {
  if (mode & CTAF_HALTED) return;
  bytes=subMax(bytes);
  long l=0;
  while (bytes>=sizeof(l)) 
    {ostr.write((char *)&l,sizeof(l));bytes-=sizeof(l);}
  if (bytes) ostr.write((char *)l,bytes);  
  if (!ostr) err(CTAFE_WRITEERROR);
  }

//--------------------------------------------------

bool ctArchiveIn::check(const char *tagId)
  {
  char z;
  do
    {
    (*this)>>=z;
    if (mode & CTAF_HALTED) return false;
    if (z!=*tagId) return false;
    tagId++;
    }
  while (z);
  return true;
  }

//--------------------------------------------------

bool ctArchiveOut::check(const char *tagId)
  {
  (*this)<<tagId;
  return true;
  }

//--------------------------------------------------

void ctArchiveCounting::CallSerialize(CTLPSC src)
  {
  unsigned long save=globc;
  nwr++;
  if (nwr>=narr) Expand();
  if (operator!()) return;
  int i=nwr;
  if (src) 
    {
    unsigned long ctxsave=context;
    unsigned long versave=version;
    src->Serialize(*this);
    version=versave;
    context=ctxsave;
    }
  arr[i].cls=src;
  arr[i].size=globc-save;
  }

//--------------------------------------------------

