#pragma once
#include "dialogclass.h"
#include "NetEngine.h"


#define TDP_PROVIDERID 0x40000000
struct TransferDataPacket
  {
  DWORD id; //idprovider 
  DWORD majuser, minuser;
  DWORD transferID;
  DWORD frame;
  DWORD datasize;
  char data[1];
  };

struct TransferInfo
  {
  UINT transferID;      //id prenosu
  DWORD majuser,minuser; //id uzivatele
  DWORD filesize;       //velikost souboru
  bool sendmode;        //true - odesilani, false - prijem
  DWORD nframes;        //celkovy pocet ramcu     - pouze pro prijem  
  unsigned char *frameinfo;     //kazdy frame ma jeden bit - pouze pro prijem
  HANDLE hFile;         //handle soubor jenz se ucastni prenosu
  HANDLE hMapping;      //handle mapovaneho souboru
  char *mapped;         //ukazatel na mapovany soubor
  int boost;            //citac boosteringu - pokud dosahne nuly, posle se jeden pozadavek navic
  int boostinit;        //vychozi hodnota citace, default 16
  int nextframe;        //index dalsiho frame k odeslani;
  int remain;           //pocet zbyvajicich framu;


  bool InitFile(const char * filename, DWORD size, bool mode);
  void Close();
  bool IsFrameCheckedIn(DWORD frame);
  void CheckInFrame(DWORD frame);
  bool ArrivedData(DWORD frame, void *data, int size);
  void *RequestData(DWORD frame, int &size);

  int ArrivedPacket(TransferDataPacket &packet, CNetEngine &engine, SOCKADDR_IN *target);
  };

class CTransferDlg : public CDialogClass, public CNetReceiver
  {
  char *filename;   //filename (pathname) to exchange;
  char *message;    //message

  public:
    CTransferDlg(void);
    ~CTransferDlg(void);
  };
