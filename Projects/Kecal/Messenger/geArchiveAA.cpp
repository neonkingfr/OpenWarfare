// ctArchiveMem.cpp: implementation of the ctArchiveOutMem class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "geArchiveAA.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

bool ctArchiveOutMem::Expand(int s)
  {
  if (size+s>alloc)
    {
    if (alloc==0) alloc=16;
    char *p=(char *)realloc(buffer,alloc*2+s);
    if (p==NULL) 
      {this->err(CTAFE_NOMEMORY,true);return true;}
    buffer=p;
    alloc=alloc*2+s;
    }
  return false;
  }

//--------------------------------------------------

void ctArchiveOutMem::serial(void *ptr, unsigned long s)
  {
  if (!(*this)) return;
  s=subMax(s);
  if (Expand(s)) return;
  memcpy(buffer+size,ptr,s);
  size+=s;
  }

//--------------------------------------------------

void ctArchiveOutMem::reserve(unsigned long s)
  {
  if (!(*this)) return;
  s=subMax(s);
  if (Expand(s)) return;
  memset(buffer+size,0,s);
  size+=s;
  }

//--------------------------------------------------

void ctArchiveInMem::serial(void *ptr, unsigned long s)
  {
  if (!(*this)) return;
  s=subMax(s);
  int rd=s;
  if (pos+rd>size)
    {
    rd=size-pos;
    err(CTAFE_EOF,true);
    }
  memcpy(ptr,buffer+pos,rd);
  pos+=rd;
  }

//--------------------------------------------------

void ctArchiveInMem::reserve(unsigned long s)
  {
  if (!(*this)) return;
  s=subMax(s);
  int rd=s;
  if (pos+rd>size)
    {
    rd=size-pos;
    err(CTAFE_EOF,true);
    }
  pos+=rd;
  }

//--------------------------------------------------

