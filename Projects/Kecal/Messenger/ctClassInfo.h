// ctClassInfo.h: interface for the ctClassInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTCLASSINFO_H__E26BCC3F_4C55_4F9D_AD6B_93A882EFECB7__INCLUDED_)
#define AFX_CTCLASSINFO_H__E26BCC3F_4C55_4F9D_AD6B_93A882EFECB7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ctArchive.h"
#include "ctLongLongTable.h"
#include <typeinfo.h>

typedef CTLPSC (*ctCreateClass)();
typedef void (*ctDestroyClass)(CTLPSC what);

class ctClassInfo  
  {
  long cuid; //code sensitive unique identificator
  long duid; //data sensitive unique identificator
  ctCreateClass creatclass; //function to create class
  ctDestroyClass destroy; //function to destroy class
  const char *classname; //name of the class
  void *userptr; //user data associated with class
  public:
    ctClassInfo(long cd, long dd, ctCreateClass cr, ctDestroyClass ds, const char *classname);
    static long CalculateDuid(const char *name);	  
    void SetData(void *data) 
      {userptr=data;}
    void * GetData() 
      {return userptr;}
    long GetCUID() 
      {return cuid;}
    long GetDUID() 
      {return duid;}
    CTLPSC CreateClass() 
      {return creatclass();}
    void DestroyClass(CTLPSC what) 
      {destroy(what);}
    const char *GetClassName() 
      {return classname;}	  	
  };

//--------------------------------------------------

class ctClassTable
  {
  static ctLongLongTable *cuidtab;
  static ctLongLongTable *duidtab;
  public:	
    static void Add(ctClassInfo *nfo);
    static ctClassInfo *GetClass(long tagid);
    static ctClassInfo *GetClass(CTLPSC ptr);
  };

//--------------------------------------------------

class ctLostDataClass
  {
  private:
    struct Data
      {
      CTLPSC cls;
      Data *next;
      };
    Data *list;
  public:
    ctLostDataClass():list(NULL) 
      {}
    ~ctLostDataClass() 
      {Reset();}
    void Reset() 
      {Data *q;while (list) 
        {q=list;list=q->next;delete q;}}
    void Enum(bool (*EnumProc)(CTLPSC cls,void *context),void *context);
    void Add(CTLPSC cls);
  };

//--------------------------------------------------

extern ctLostDataClass ctLostData;

template<class T>
ctArchive& operator>>=(ctArchive& arch, T *&cls)
  {
  CTLPSC lcls=arch.serial(cls); 
  cls=dynamic_cast<T *>(lcls);
  if (cls==NULL && lcls!=NULL) 
    {
    ctLostData.Add(lcls);
    }
  return arch;
  }

//--------------------------------------------------

#define ctRegisterClassFullEx(classname,idoffset, create,destroy)\
static ctClassInfo _classinfo_##classname((long)(typeid(classname).name()),idoffset, create,destroy,#classname);

#define ctRegisterClassFull(classname,idoffset,params)\
static CTLPSC _classcreat_##classname() {return new classname params;}\
static void _classdestroy_##classname(CTLPSC what) {delete what;}\
static ctClassInfo _classinfo_##classname((long)(typeid(classname).name()),idoffset, _classcreat_##classname,_classdestroy_##classname,#classname);

#define ctRegisterClass(classname,idoffset)\
static CTLPSC _classcreat_##classname() {return new classname;}\
static void _classdestroy_##classname(CTLPSC what) {delete what;}\
static ctClassInfo _classinfo_##classname((long)(typeid(classname).name()),idoffset, _classcreat_##classname,_classdestroy_##classname,#classname);

#define ctConnectLink(archive,type, linkname) \
  	  if (archive.PrevLink()) \
		{\
		CTLPSC sc=archive.PrevLink(); \
		type *link=dynamic_cast<type *>(sc); \
		if (link==NULL && sc!=NULL) ctLostData.Add(sc);	\
		else link->linkname=this;\
		}\
	else





#endif // !defined(AFX_CTCLASSINFO_H__E26BCC3F_4C55_4F9D_AD6B_93A882EFECB7__INCLUDED_)
