// SConfig.cpp: implementation of the SConfig class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SConfig.h"
#include <SHLWAPI.h>
#include "LanguageDialog.h"
#include "resource.h"
#include "application.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define CONFIGVER 0x108

SConfig::SConfig()
  {
  myname=NULL;
  broadcast=0;
  DWORD q=0;
  runstartup=false;
  openunknown=false;
  openfromnew=false;
  showtray=false;
  GenerateUID(majid,minid);
  defchatwndmode=0;
  nopopup=false;
  noforesound=0;
  soundfile=NULL;
  skin_font=strdup("");
  skin_color=CLR_DEFAULT;
  skin_image=strdup("");
  skin_fontbold=false;
  skin_fontitalic=false;
  skin_transparent=false;
  skin_fontsize=14;
  language=0xFFFFFFFF;
  notify_flags=CNTF_WRITTINGTEXT|CNTF_STOPWRITTING;
  logevents=false;
  }

//--------------------------------------------------

SConfig::~SConfig()
  {
  free(myname);
  free(soundfile);
  free(skin_font);
  free(skin_image);
  }

//--------------------------------------------------

  void SConfig::Serialize(ctArchive &arch)
  {
    if (!arch.check("CFG")) 
    {arch.err(-1);return;}
    arch.ver(CONFIGVER);
    if (-arch) this->~SConfig();
    arch.useMalloc();
    arch>>=broadcast;
    arch>>=myname;
    arch>>=port;
    arch>>=runstartup;
    arch>>=openunknown;
    arch>>=openfromnew;
    arch>>=showtray;
    arch>>=mainontop;
    arch>>=chatontop;
    arch>>=minid;
    arch>>=majid;
    arch>>=defchatwndmode;  
    if (arch.ver()>=0x101) arch>>=soundfile;
    if (arch.ver()>=0x102) arch>>=hotkey;
    if (arch.ver()>=0x103)
    {
      arch>>=nopopup;
      arch>>=noforesound;
    }
    if (arch.ver()>=0x104)
    {
      arch>>=skin_font;
      arch>>=skin_image;
      arch>>=skin_color;
    }
    else if (-arch)
    {
      skin_font=strdup("");
      skin_image=strdup("");
    }
    if (arch.ver()>=0x105)
    {
      arch>>=skin_fontbold;
      arch>>=skin_fontitalic;
      arch>>=skin_fontsize;
      arch>>=skin_transparent;
    }
    if (arch.ver()>=0x106)
    {
      arch>>=language;
    }
    if (arch.ver()>=0x107)
    {
      arch>>=notify_flags;
    }
    if (arch.ver()>=0x108)
    {
      arch>>=logevents;
    }
  }

//--------------------------------------------------

void SConfig::AutoConfigure()
  {
  this->~SConfig();
  broadcast=inet_addr("192.168.2.255");
  port=9865;
  DWORD p=256;
  myname=(char *)malloc(p);
  GetUserName(myname,&p);
  myname=(char *)_expand(myname,p);
  this->openfromnew=true;
  this->showtray=true;
  this->mainontop=false;
  this->chatontop=false;
  }

//--------------------------------------------------


bool SConfig::CheckLanguage(HWND app, bool force)
  {
  bool ret=false;
  if (language==0xFFFFFFFF || force)
	{
	CLanguageDialog dlg;
	if (dlg.DoModal(IDD_LANGUAGEDIALOG,app)==IDOK)
	  {
	  language=dlg.lang;
	  ret=true;
	  }
	}
  if (language==0xFFFFFFFF) ExitProcess(0xFF);
  SetThreadLocale(language);
  return ret;
  }
