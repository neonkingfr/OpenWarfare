// RoomListDlg.h: interface for the CRoomListDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROOMLISTDLG_H__F49CCE5B_6A8D_4F23_8ED8_4AC382F2F5ED__INCLUDED_)
#define AFX_ROOMLISTDLG_H__F49CCE5B_6A8D_4F23_8ED8_4AC382F2F5ED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"

class CRoomListDlg : public CDialogClass  
  {
  HWND list;
  HWND button;
  HWND edit;
  HWND statc;
  AutoArray<ChannelID> chanlist;
  public:
    void CheckChannels( ChannelID *idlist, int count);
    void CheckChannel(ChannelID &chan);
    LRESULT OnOK();
    LRESULT OnCancel();
    LRESULT OnSize(int cx, int cy, int rx, int ry, int flags);
    LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
    void SavePosition();
    LRESULT OnInitDialog();
    CRoomListDlg();
    virtual ~CRoomListDlg();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_ROOMLISTDLG_H__F49CCE5B_6A8D_4F23_8ED8_4AC382F2F5ED__INCLUDED_)
