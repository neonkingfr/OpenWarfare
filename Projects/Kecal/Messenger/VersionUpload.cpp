// VersionUpload.cpp: implementation of the CVersionUpload class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "VersionUpload.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVersionUpload::CVersionUpload()
  {
  buff=NULL;
  running=false;
  }

//--------------------------------------------------

CVersionUpload::~CVersionUpload()
  {
  delete [] buff;
  }

//--------------------------------------------------

static VOID CALLBACK UploadEnter(
                                 HWND hwnd,     // handle of window for timer messages
                                 UINT uMsg,     // WM_TIMER message
                                 UINT idEvent,  // timer identifier
                                 DWORD dwTime   // current system time
                                 )
    {
    CVersionUpload *ths=(CVersionUpload *)idEvent;
    ths->TickTock();
    }

//--------------------------------------------------

void CVersionUpload::BeginUpload()
  {
  if (running) return;
  char fname[512];
  GetModuleFileName(AfxGetInstanceHandle(),fname,ArrLength(fname));
  ff=CreateFile(fname,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
  if (ff==NULL) return;
  p=GetFileSize(ff,NULL);
  blk=(p+DOWNLOAD_BLOCKSIZE-1)/DOWNLOAD_BLOCKSIZE;
  theApp->SendCommand(&SendCommandAdditionalInfo(3),COMM_DOWNLOADINFO,p,blk);
  buff=new char[DOWNLOAD_BLOCKSIZE];
  i=0;
  running=true;
  timevent=SetTimer(theApp->GetNotifyWnd(),(UINT)this,10,UploadEnter);
  }

//--------------------------------------------------

void CVersionUpload::TickTock()
  {
  if (i<blk)
    {
    DWORD ww;
    ReadFile(ff,buff,DOWNLOAD_BLOCKSIZE,&ww,NULL);
    theApp->SendCommand(NULL,COMM_DOWNLOADDATA,i,buff);
    i++;
    }
  else
    {
    CloseHandle(ff);
    KillTimer(theApp->GetNotifyWnd(),(UINT)this);
    delete []buff;
    buff=NULL;
    running=false;
    }
  }

//--------------------------------------------------

