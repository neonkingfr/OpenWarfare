//-----------------------------------------------------------------------------
// File: enf_types.h
//
// Desc: Basic types
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

#ifndef ENF_TYPES_H
#define ENF_TYPES_H

#include "enf_config.h"
#include "common/enf_memory.h"

#ifdef ENF_XENON
	#include <xtl.h>
#else
	#include <string.h>
#endif

#ifdef ENF_SSE
	#include <xmmintrin.h>
#endif

typedef unsigned short	ushort;
typedef unsigned int		uint;
typedef unsigned char	uchar;
typedef unsigned char	ubyte;
typedef uint ERESULT;

#ifdef ENF_INTEL
	#define ALIGN16 __declspec( align(16) )
#else
	#define ALIGN16
#endif

#define INDEX_NOT_FOUND 0xffffffff

#define BITF(a) (1UL << (uint)(a))

#define STRUCT_MEMBER_OFFSET(t, m) ((int)&(((t *)0L)->m))


//-----------------------------------------------------------------------------
/*!
return maximum of two numbers
\param a		one number
\param b		second number
*/
//-----------------------------------------------------------------------------
template<class T> ENF_INLINEFUNC T enf_max(T a, T b) 
{
	return (a > b) ? a : b;
}


//-----------------------------------------------------------------------------
/*!
return minimum of two numbers
\param a		one number
\param b		second number
*/
//-----------------------------------------------------------------------------
template<class T> ENF_INLINEFUNC T enf_min(T a, T b) 
{
	return (a < b) ? a : b;
}


//-----------------------------------------------------------------------------
/*!
return number clamped to interval
\param a		number
\param min	minimum
\param max	maximum
*/
//-----------------------------------------------------------------------------
template<class T> ENF_INLINEFUNC T enf_limit(T a, T min, T max) 
{
	return (a < min) ? min : ((a > max) ? max : a);
}


//-----------------------------------------------------------------------------
/*!
return true when number is in interval, the borders of interval returns true
\param a		number
\param min	minimum
\param max	maximum
\return ...
*/
//-----------------------------------------------------------------------------
template<class T> ENF_INLINEFUNC bool enf_in(T a, T min, T max) 
{
	return (a >= min && a <= max);
}


//-----------------------------------------------------------------------------
/*!
return true when number is in interval, the borders of interval returns false
\param a		number
\param min	minimum
\param max	maximum
\return ...
*/
//-----------------------------------------------------------------------------
template<class T> ENF_INLINEFUNC bool enf_in_sharp(T a, T min, T max) 
{
	return (a > min && a < max);
}


//-----------------------------------------------------------------------------
/*!
swap two types
\param a1	first 
\param a2	second 
*/
//-----------------------------------------------------------------------------
template<class T> ENF_INLINEFUNC void enf_swap(T &a1, T &a2) 
{
	T tmp = a1;
	a1 = a2;
	a2 = tmp;
}



//--------------------------------------------------------------------------
/*!
get the highest set bit, count from 0: 1->0, 2->1...32->5, zero must be test
elsewhere, reason: to use the result directly in shift and power functions 
\param n	number
*/
//--------------------------------------------------------------------------
ENF_INLINEFUNC uint GetHighestBit(uint n) 
{ 
#if 0 //def ENF_INTEL
	_asm 
	{ 
		//the simplest way
		bsr eax, n
	}
#else
	uint bit = 0;
	if (n & 0xFFFF0000)
	{
		n >>= 16;
		bit = 16;
	}

	if (n & 0x0000FF00)
	{
		n >>= 8;
		bit += 8;
	}

	if (n & 0x000000F0)
	{
		n >>= 4;
		bit += 4;
	}

	if (n & 0x0000000C)
	{
		n >>= 2;
		bit += 2;
	}

	return bit + (n >> 1);
#endif
}




#include "common/enf_strings.h"


#define EARRAY_DEFAULT_SIZE 0

//-----------------------------------------------------------------------------
/*!
Dynamic array
*/
//-----------------------------------------------------------------------------
template <class T> class EArray
{
private:
	
	T *data1;
	uint size;
	uint count;

	//-----------------------------------------------------------------------
	ENF_INLINE uint GetArraySize()
	{
	  return size;
	}

	//-----------------------------------------------------------------------
	void Grow(uint nsize)
	{
		data1 = (T*)MemoryManager::ReAlloc(data1, nsize * sizeof(T));

		enf_assert(data1);

		//call constructors for new objects
		for(uint n = this->size; n < nsize; n++)
		{
			T* tmp=data1 + n;
			new ((void*)(tmp)) T;
		}

		this->size = nsize;
	}

public:

	//-----------------------------------------------------------------------
	void Reserve(uint newsize)
	{
		if(this->size != newsize)
			Grow(newsize);
	}

	//-----------------------------------------------------------------------
	void SetSize(uint newsize)
	{
		if(this->size != newsize)
			Grow(newsize);

		count = newsize;
	}

	//-----------------------------------------------------------------------
	EArray():
		count(0),
		size(0),
		data1(NULL)
	{
	}


	//-----------------------------------------------------------------------
	EArray(uint nsize):
		count(0),
		size(0),
		data1(NULL)
	{
		if (nsize > 0)
		{
			Grow(nsize);
		}
	}


	//-----------------------------------------------------------------------
	EArray(const EArray<T> &a):
		count(0),
		size(0),
		data1(NULL)
	{
		if (a.count > 0)
		{
			Grow(a.count);
			for(uint i = 0; i < a.count; i++)
			{
				data1[i] = a[i];
			}
			count = a.count;
		}
		else
		{
			//destroy all elements
			for(uint i = 0; i < size; i++)
			{
				(data1+i)->~T();
			}
			if (data1)
			{
				MemoryManager::Free(data1);
				data1 = NULL;
			}
			size = 0;
			count = 0;
		}
	}

	//-----------------------------------------------------------------------
	~EArray()
	{
		//destroy all elements
		for(uint i = 0; i < size; i++)
		{
			(data1+i)->~T();
		}
		if (data1) MemoryManager::Free(data1);
	}

	//-----------------------------------------------------------------------
/*
	ENF_INLINE const EArray<T>& operator= (const EArray<T>& s)
	{
		if(size < s.count)
		{
			size = s.count;
			data1 = (T*)MemoryManager::getInstance()->reallocate(data1, sizeof(T)*s.count);
			enf_assert(data1);
		}
		if (s.count > 0 )
			CopyMem(data1, s.getArray(), s.count * sizeof(T));
		
		count=s.count;
		return *this;
	}
*/
	//-----------------------------------------------------------------------
	ENF_INLINE const T& operator[] (uint i) const
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	ENF_INLINE T& operator[] (uint i)
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	ENF_INLINE const T& GetElement(uint i) const
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	ENF_INLINE T& GetElement(uint i)
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	ENF_INLINE const T* GetArray() const
	{
		return data1;
	}

	//-----------------------------------------------------------------------
	ENF_INLINE T* GetArray()
	{
		return data1;
	}

	//-----------------------------------------------------------------------
	ENF_INLINE uint GetCardinality() const
	{
	  return count;
	}

	//-----------------------------------------------------------------------
	ENF_INLINE bool IsEmpty() const
	{
	  return (count == 0);
	}

	//-----------------------------------------------------------------------
	ENF_INLINE void Clear()
	{
	  count = 0;
	}

	//-----------------------------------------------------------------------
	ENF_INLINE uint GetIndexOf(const T &a) const
	{
		uint i;
		for(i = 0; i < count; i++)
		{
			if(data1[i] == a)
				return i;
		}
		return -1;
	}

	//-----------------------------------------------------------------------
	/*!
	Inserts element at the end of array.
	\param a
	Element to be inserted
	\return
	Position at which element is inserted
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE uint Insert(const T &a)
	{
		if(size <= count)
			Grow(size * 2 + 1);
		data1[count] = a;

		return count++;
	}
 
	//-----------------------------------------------------------------------
	/*!
	Inserts element at certain position and moves all elements behind
	this position by one.
	\param a
	Element to be inserted
	\param index
	Position at which element is inserted. Must be less than EArray::GetCardinality()
	\return
	Number of elements after insertion
	*/
	ENF_INLINE uint InsertAt(const T &a, uint index)
	{
		enf_assert(index < count);
		if(size <= count)
			Grow(size * 2 + 1);

		memmove(&data1[index + 1], &data1[index], (count - index) * sizeof(T));
		//reinitialize the element, which become duplicated now
		new ((void*)(&data1[index])) T;
		data1[index] = a;

		return ++count;
	}

	//-----------------------------------------------------------------------
	ENF_INLINE uint Push(const T &a)
	{
		return Insert(a);
	}

	//-----------------------------------------------------------------------
	ENF_INLINE T& Pop()
	{
		enf_assert(count > 0);
		count--;
		return data1[count];
	}

	//-----------------------------------------------------------------------
	void Insert(const EArray<T> *a)
	{
		if (a == NULL)
			return;

		if(size <= count + a->count)
			Grow((count + a->count + 1) * 2);
		
		for(uint n = 0; n < a->count; n++)
			data1[count + n], a->data1[n];
		
		count += a->count;
	}

	//-----------------------------------------------------------------------
	/*!
	Removes element from array. The empty position is replaced by
	last element, so removal is quite fast but do not retain order.
	\param i
	Index of element to be removed
	\return
	Removed element
	*/
	T RemoveElement(uint i)
	{
		enf_assert(count > 0);
		enf_assert(i < count);
		T tmp = data1[i];
		data1[i] = data1[--count];
		return tmp;
	}

	//-----------------------------------------------------------------------
	/*!
	Removes element from array, but retain all elements ordered. It's
	slower than RemoveElement
	\param i
	Index of element to be removed
	\return
	Removed element
	*/
	T RemoveElementOrdered(uint i)
	{
		enf_assert(count > 0);
		enf_assert(i < count);
		T tmp = data1[i];
		//(data1+i)->~T(); //destroy element
		memmove(&data1[i], &data1[i + 1], sizeof(T) * (count - i - 1));
		//reinitialize element at the end, which become duplicated now
		new ((void*)(&data1[count - 1])) T;
		--count;
		return tmp;
	}


//-----------------------------------------------------------------------
	bool Remove(const T &a)
	{
		uint i;
		uint low = 0;
		bool ret = false;
		do
		{
			for(i = low; i < count; i++)
			{
				if(data1[i] == a)
					break;
			}
		
			if (i < count)
			{
				RemoveElement(i);
				ret = true;
				low = i;
			}
		} while(i < count);
		return ret;
	}

//-----------------------------------------------------------------------
	void Remove(const EArray<T> *a)
	{
		if (a == NULL)
			return;

		for(uint i = 0; i < a->GetCardinality(); i++)
			Remove(a->GetElement(i));
	}

	EArray<T>& operator=(const EArray<T>& array)
	{
		Clear();
		for(uint i = 0; i < array.GetCardinality(); i++)
			Insert(array.GetElement(i));

		return *this;
	}
};

/*!
Dynamic array of mostly constant size. Do not have
any overhead, but it's not efficient for
insertion/removal of element
*/
template <class T> class CArray
{
private:

	T*		data1;
	uint	count;

public:

	//-----------------------------------------------------------------------
	void SetSize(uint newcount)
	{
		if(this->count != newcount)
		{
			T* newdata = NULL;
			if(newcount)
			{
				T* newdata = T[newcount];
				for(uint n = 0; n < enf_min(newcount, count); n++)
					newdata[n] = data1[n];
			}
			if(data1)
				delete[] data1;
			data1 = newdata;
			count = newcount;
		}
	}

	//-----------------------------------------------------------------------
	CArray():
		count(0),
		data1(NULL)
	{
	}

	//-----------------------------------------------------------------------
	~CArray()
	{
		if (data1)
		{
			delete[] data1;
			data1 = NULL;
		}
	}

	//-----------------------------------------------------------------------
	ENF_INLINE const T& operator[] (uint i) const
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	ENF_INLINE T& operator[] (uint i)
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	ENF_INLINE const T& GetElement(uint i) const
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	ENF_INLINE T& GetElement(uint i)
	{
		enf_assert(i < count);
		return data1[i];
	}

	//-----------------------------------------------------------------------
	ENF_INLINE const T* GetArray() const
	{
		return data1;
	}

	//-----------------------------------------------------------------------
	ENF_INLINE T* GetArray()
	{
		return data1;
	}

	//-----------------------------------------------------------------------
	ENF_INLINE uint GetCardinality() const
	{
		return count;
	}

	//-----------------------------------------------------------------------
	ENF_INLINE bool IsEmpty() const
	{
		return (count == 0);
	}

	//-----------------------------------------------------------------------
	ENF_INLINE void Clear()
	{
		SetSize(0);
	}

	//-----------------------------------------------------------------------
	ENF_INLINE uint Insert(const T &a)
	{
		SetSize(count + 1);
		data1[count - 1] = a;
		return count - 1;
	}

	//-----------------------------------------------------------------------
	/*!
	Removes element from array. The empty position is replaced by
	last element, so removal is quite fast but do not retain order.
	\param i
	Index of element to be removed
	\return
	Removed element
	*/
	T RemoveElement(uint i)
	{
		enf_assert(count > 0);
		enf_assert(i < count);
		T tmp = data1[i];
		data1[i] = data1[--count];

		return tmp;
	}

	//-----------------------------------------------------------------------
	/*!
	Removes element from array, but retain all elements ordered. It's
	slower than RemoveElement
	\param i
	Index of element to be removed
	\return
	Removed element
	*/
	T RemoveElementOrdered(uint i)
	{
		enf_assert(count > 0);
		enf_assert(i < count);
		T tmp = data1[i];
		//(data1+i)->~T(); //destroy element
		memmove(&data1[i], &data1[i + 1], sizeof(T) * (count - i - 1));
		//reinitialize element at the end, which become duplicated now
		new ((void*)(&data1[count - 1])) T;
		--count;
		return tmp;
	}


	EArray<T>& operator=(const EArray<T>& array)
	{
		SetSize(array.count);
		for(uint i = 0; i < array.GetCardinality(); i++)
			data1[i] = array[i];

		return *this;
	}
};

//-----------------------------------------------------------------------------
/*!
Static array with size by template parameter
*/
//-----------------------------------------------------------------------------
template <class T, int S> class SArray
{
protected:
	uint		Num;
	T			m_Array[S];

public:
	ENF_INLINE T* GetArray()
	{
		return m_Array;
	}

	ENF_INLINE const T* GetArray() const
	{
		return m_Array;
	}

	ENF_INLINE uint Max() const
	{
		return S;
	}

	ENF_INLINE T& operator[](uint element)
	{
		enf_assert(element < Num);
		return m_Array[element];
	}

	ENF_INLINE const T& operator[](uint element) const
	{
		enf_assert(element < Num);
		return m_Array[element];
	}

	ENF_INLINE T& GetElement(uint element)
	{
		enf_assert(element < Num);
		return m_Array[element];
	}
	
	ENF_INLINE const T& GetElement(uint element) const
	{
		enf_assert(element < Num);
		return m_Array[element];
	}

	ENF_INLINE T RemoveElement(uint i)
	{
		enf_assert(Num > 0);
		enf_assert(i < Num);
		T tmp = m_Array[i];
		m_Array[i] = m_Array[--Num];
		return tmp;
	}

	ENF_INLINE T& Last()
	{
		return m_Array[Num];
	}

	ENF_INLINE uint operator++(int)
	{
		enf_assert(Num < S);
		return Num++;
	}

	ENF_INLINE uint Insert(const T& data)
	{
		enf_assert(Num < S);
		m_Array[Num] = data;
		return Num++;
	}

	ENF_INLINE T Pop()
	{
		enf_assert(Num > 0);
		return m_Array[--Num];
	}

	ENF_INLINE int GetIndexOf(const T &a) const
	{
		for(uint i = 0; i < Num; i++)
		{
			if(m_Array[i] == a)
				return i;
		}
		return -1;
	}

	ENF_INLINE bool Contains(const T &a) const
	{
		return GetIndexOf(a) != -1;
	}

	ENF_INLINE bool Find(const T& a) const
	{
		return Contains(a);
	}

	ENF_INLINE void Clear()
	{
		Num = 0;
	}

	ENF_INLINE uint GetCardinality() const
	{
		return Num;
	}

	ENF_INLINE bool IsEmpty() const
	{
	  return (Num == 0);
	}

	ENF_INLINE bool IsFull() const
	{
		return (Num == S);
	}

	SArray()
	{
		Num = 0;
	}
};


#define STRUCT_FROM_CLINK(l, t, m) ((t *)((char *)l - (int)&(((t *)0)->m)))

//-----------------------------------------------------------------------------
/*!
CLink, cyclic link
*/
//-----------------------------------------------------------------------------
struct CLink
{
	CLink*	Prev;
	CLink*	Next;

	ENF_INLINE CLink()
	{
		Prev = Next = this;
	}

	ENF_INLINE void Clear()
	{
		Prev = Next = this;
	}

	ENF_INLINE bool IsLinked()
	{
		return (Next != this && Prev != this);
	}

	ENF_INLINE void Insert(CLink *l)
	{
		Next = l;
		Prev = l->Prev;
		Prev->Next = this;
		Next->Prev = this;
	}
	ENF_INLINE void InsertBefore(CLink *l)
	{
		Next = l->Next;
		Prev = l;
		Prev->Next = this;
		Next->Prev = this;
	}

	ENF_INLINE void Remove()
	{
		Next->Prev = Prev;
		Prev->Next = Next;
	}
};

#define ENF_LIST_NEXT m_pNextNode
#define ENF_LIST_PREV m_pPrevNode

//-----------------------------------------------------------------------
/*!
Doubly linked list with no additional memory allocation
as the nodes themselves holds the list representation.
*/
//-----------------------------------------------------------------------
template <class ListNode> class EList
{
public:
	EList()
	{
		Clear();
	}

	inline int GetIndexOf(const ListNode *node) const
	{
		uint i = 0;
		ListNode *tmp = FirstNode;
		while(tmp != NULL)
		{
			if (tmp == node) return i;
			tmp = tmp->ENF_LIST_NEXT;
			i++;
		}
		return -1;
	}

	inline ListNode *GetElement(uint i) const
	{
		enf_assert(i < Count);
		ListNode *tmp = FirstNode;
		while(i-- > 0)
		{
			tmp = tmp->ENF_LIST_NEXT;
		}
		return tmp;
	}

	inline ListNode *operator[](uint i) const
	{
		return GetElement(i);
	}

	//-----------------------------------------------------------------------
	/*!
		\return
		The number of of entries in the list.
	*/
	//-----------------------------------------------------------------------
	inline uint GetCardinality() const
	{	//number of elements
		return Count;
	}

	//-----------------------------------------------------------------------
	/*!
		Returns true if the list is empty. Operation is O(1).
		\return
		True if the array has no entries, false otherwise.
	*/
	//-----------------------------------------------------------------------
	inline bool IsEmpty() const
	{
		return (Count==0);
	}

	//-----------------------------------------------------------------------
	/*!
		Clear the array. Operation is O(1).
	*/
	//-----------------------------------------------------------------------
	inline void Clear()
	{
		Count = 0;
		FirstNode = NULL;
		LastNode = NULL;
	}

	//-----------------------------------------------------------------------
	/*!
		\return
		True if the list contains element a
	*/
	//-----------------------------------------------------------------------
	inline bool Contains(const ListNode *a) const
	{
		ListNode *tmp = FirstNode;
		while(tmp != NULL){
			if (tmp == a) return true;
			tmp = tmp->ENF_LIST_NEXT;
		}
		return false;
	}


	//-----------------------------------------------------------------------
	/*!
		Adds one element to the end of the list. Operation is O(1).
		\param a
		The element that will be added to this list.
	*/
	//-----------------------------------------------------------------------
	inline void Insert(ListNode *a)
	{
		if (LastNode) LastNode->ENF_LIST_NEXT = a;
		if (!FirstNode) FirstNode = a;
		a->ENF_LIST_NEXT = NULL;
		a->ENF_LIST_PREV = LastNode;
		LastNode = a;
		Count++;
	}


	//-----------------------------------------------------------------------
	/*!
	Remove an element from the list
	Operation is O(1)
	\param a
	The element that will be removed from this array.
	*/
	//-----------------------------------------------------------------------
	inline void Remove(ListNode *a)
	{
		if (a->ENF_LIST_NEXT) a->ENF_LIST_NEXT->ENF_LIST_PREV = a->ENF_LIST_PREV;
		else LastNode = a->ENF_LIST_PREV;
		if (a->ENF_LIST_PREV) a->ENF_LIST_PREV->ENF_LIST_NEXT = a->ENF_LIST_NEXT;
		else FirstNode = a->ENF_LIST_NEXT;
		a->ENF_LIST_NEXT = NULL;
		a->ENF_LIST_PREV = NULL;
		Count--;
	}

	inline ListNode *GetFirstNode() const
	{
		return FirstNode;
	}

	inline ListNode *GetLastNode() const
	{
		return LastNode;
	}

private:
	uint			Count;
	ListNode*	FirstNode;
	ListNode*	LastNode;
};


//-----------------------------------------------------------------------------
/*!
EListIterator
*/
//-----------------------------------------------------------------------------
template <class ListNode> class EListIterator
{
public:
	EListIterator(const EList<ListNode> *list)
	{
		Current = list->GetFirstNode();
	}

	EListIterator(const EList<ListNode> &list)
	{
		Current = list.GetFirstNode();
	}

	bool HasMoreElements() const
	{
		return Current != NULL;
	}

	ListNode *GetNext()
	{
		ListNode *tmp = Current;
		if (tmp) Current = Current->ENF_LIST_NEXT;
		else Current = NULL;
		return tmp;
	}

private:
	ListNode *Current;
};


//-----------------------------------------------------------------------------
/*!
RefCountObject
*/
//-----------------------------------------------------------------------------
class PUBLIC_API RefCountObject
{
protected:
	int	m_iRefs;
	virtual ~RefCountObject() {}

	RefCountObject(): m_iRefs(1)
	{
	}

public:

	//!Returns number of references on this object
	ENF_INLINE int RefCount() const
	{
		return m_iRefs;
	}

	/*!
	Adds reference on this object
	\return
	Current number of references
	*/
	ENF_INLINE int AddRef()
	{
		return ++m_iRefs;
	}

	/*!
	Removes one reference on object. Must be used very
	carefully as it doesn't release object when references==0!
	\return
	Current number of references
	*/
	ENF_INLINE int Unref()
	{
		return --m_iRefs;
	}

	/*!
	Sets number of references. Must be used very carefully.
	Used by deserialization only.
	\param refn
	Number of references
	*/
   ENF_INLINE void SetRef(int refn)
	{
		m_iRefs = refn;
	}

	/*!
	This default implementation removes one reference
	and calls destructor when we reach zero.
	\return
	Number of references, or zero, when object is deleted
	*/
	virtual int Release()
	{
		enf_assert(m_iRefs != 0);

		if(Unref() == 0)
		{
			delete this;
			return 0;
		}
		return m_iRefs;
	}
};

template<class T> class ComPtr
{
private:
	T* Ptr;

public:
	ComPtr()
	{
		Ptr = NULL;
	}

	ComPtr(T* p):
		Ptr(p)
	{
	}

	ENF_INLINE bool IsNull() const
	{
		return (Ptr == NULL);
	}

	ENF_INLINE T* operator->() const
	{
		return Ptr;
	}

	uint Release()
	{
		if(Ptr == NULL)
			return 0;

		uint refc = Ptr->Release();
		Ptr = NULL;
		return refc;
	}

	uint AddRef()
	{
		if(Ptr)
			return Ptr->AddRef();
		return 0;
	}
	void operator=(const ComPtr& ref)
	{
		T* src = ref.Ptr;
		if(src)
			src->AddRef();

		Release();
		Ptr = src;
	}

	ComPtr(const ComPtr& src):
		Ptr(src.Ptr)
	{
		if(Ptr) Ptr->AddRef();
	}

	ENF_INLINE operator T*() const
	{
		return Ptr;
	}

	ENF_INLINE ~ComPtr()
	{
		Release();
	}
};

class PUBLIC_API CDisableCopy
{
public:
  CDisableCopy(){}

private:
  CDisableCopy(const CDisableCopy &src);
  CDisableCopy &operator =( const CDisableCopy &src);
};

#endif //ENF_TYPES_H

