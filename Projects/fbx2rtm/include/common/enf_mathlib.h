//-----------------------------------------------------------------------------
// File: enf_mathlib.h
//
// Desc: Mathematics library
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#ifndef ENF_MATHLIB_H
#define ENF_MATHLIB_H

#include <float.h>
#include <math.h>
#include "enf_config.h"
#include "common/enf_types.h"

//-----------------------------------------------------------------------------
// Vector math
//-----------------------------------------------------------------------------

extern ALIGN16 uint fast_sqrt_table[];  // declare table of square roots

ALIGN16 const int g_imask[4] = {0xffffffff, 0xffffffff, 0xffffffff, 0};

#define BadVec(a) (!_finite(a[0]) || _isnan(a[0]) || !_finite(a[1]) || _isnan(a[1]) || !_finite(a[2]) || _isnan(a[2]))

class Vector3;
class Vector4;

const uint PITCH	= 0;
const uint YAW		= 1;
const uint ROLL	= 2;

const uint PLANE_X		= 0;
const uint PLANE_Y		= 4;
const uint PLANE_Z		= 8;
const uint PLANE_ANYX	= 12;
const uint PLANE_ANYY	= 16;
const uint PLANE_ANYZ	= 20;

const float ENF_PI		= 3.14159265358979323846f;					///< PI
const float ENF_PI_HALF	= (3.14159265358979323846f * 0.5f);		///< 0.5*PI
const float ENF_PI2		= 6.28318530717958647692f;					///< 2.0*PI

#define ENF_FSIGN2INDEX(a) (*(unsigned int *)&a >> 31)					///< return sign of the int of float (1 = negative, 0 = positive)
#define ENF_ISNEGATIVE(a) (*(unsigned int *)&a & 0x80000000)			///< return nonzero number (0x800000000) if float or integer is negative
#define ENF_FP_BITS(fp) (*(unsigned int *)&(fp))

#define ENF_RAD2DEG(rad) ((rad) * 180.0f / ENF_PI)						///< convert radians to degrees
#define ENF_DEG2RAD(deg) ((deg) * ENF_PI / 180.0f)						///< convert degrees to radians

const float	DIST_EPSILON = 0.03125f;

enum ProjType
{
	PT_PERSPECTIVE,
	PT_ORTHOGRAPHIC
};

ENF_INLINE int enf_log2 (long x)
{
#ifdef ENF_INTEL
	__asm
	{
		bsr	eax, x
		mov	x, eax
	}
	return x;
#else
	int l2;
	for (l2 = 0; x > 0; x >>=1)
	{
		++ l2;
	}
	return (l2);
#endif
}


//--------------------------------------------------------------
/*
Make square of number
\param n		number
\return		n*n
*/
//--------------------------------------------------------------
ENF_INLINEFUNC float sqr(float n)
{
	return n * n;
}

//--------------------------------------------------------------
/*
Compute fast square root of a number using lookup table
\param n		number
\return		sqrt(n)
*/
//--------------------------------------------------------------
ENF_INLINEFUNC float fastsqrt(float n)
{
  if (ENF_FP_BITS(n) == 0)
	 return 0.0;                 // check for square root of 0

  ENF_FP_BITS(n) = fast_sqrt_table[(ENF_FP_BITS(n) >> 8) & 0xFFFF] | ((((ENF_FP_BITS(n) - 0x3F800000) >> 1) + 0x3F800000) & 0x7F800000);

  return n;
}

//--------------------------------------------------------------
/*
Optimized SSE and normal conversion float to int by truncating
Timing : on Intel core2duo 3GHZ, 20mil conversions

(int) float		= ~385ms (does inside expensive FPU round state change)
SSE intrinsic	= ~113ms
int conversion = ~124ms
FPU directly   = ~126ms (24bit prec mode, without FPU state changing)

\param f		float number
\return		return converted number
*/
//--------------------------------------------------------------
ENF_INLINEFUNC int enf_floorint(float f)
{
#ifdef ENF_INTEL
	#ifdef ENF_SSE

		//use SSE intrinsic
		return _mm_cvttss_si32(_mm_set_ss(f));

	#else

		int FltInt = *(int *)&f; 
		int MyInt;

		int mantissa = (FltInt & 0x07fffff) | 0x800000; 
		int exponent = ((FltInt >> 23) & 0xff) - 0x7f; 

		int d = 23-exponent;

		if (d < 0) 
		{
			d = -d;
			MyInt = mantissa << d; 
		}
		else 
			MyInt = mantissa >> d; 

		//possible for very small numbers or very large numbers
		if (d > 31)
			MyInt = 0;

		//sign
		if (FltInt & 0x80000000)
			MyInt = -MyInt;

		return MyInt;

	#endif
#else

	//typical conversion
	return (int) f;

#endif
}

//--------------------------------------------------------------
/*
convert float to int by rounding (depends on FPU setting)
\param fl	float number
\return		return converted number
*/
//--------------------------------------------------------------
ENF_INLINEFUNC int enf_roundint(float fl)
{
#ifdef ENF_INTEL
#ifdef ENF_SSE
		return _mm_cvtss_si32(_mm_set_ss(fl));
#else
	int integer;

	__asm
	{
		fld		fl
		frndint
		fistp		dword ptr integer
	}
	return integer;
#endif
#else
	return (int)fl;
#endif
}


//-----------------------------------------------------------------------------
/*!
linear interpolation 
\param v1	value for t == 0
\param v2	value for t == 1
\param t weight between v1 and v2
*/
//-----------------------------------------------------------------------------
template<class T> ENF_INLINEFUNC T enf_lerp(const T &v1, const T &v2, float t) 
{
	return v1 + (v2 - v1) * t;
}

//-----------------------------------------------------------------------------
/*!
wrap number to specified interval
\param a		number
\param min	minimum
\param max	maximum
*/
//-----------------------------------------------------------------------------
template<class T> ENF_INLINEFUNC T enf_wrap(T a, T min, T max)
{
	enf_assert2(false, "Don't know now how to wrap this type");
	return T;
}

//-----------------------------------------------------------------------------
/*!
specialization of universal template to float, wrap number to specified interval
\param a		number
\param min	minimum
\param max	maximum
*/
//-----------------------------------------------------------------------------
template<> ENF_INLINEFUNC float enf_wrap<float>(float a, float min, float max) 
{
	float delta = max - min;

	enf_assert(delta != 0.0f);

	float flr	= floor(((a - min)/delta));
	return a - (min + delta*flr);
}

//-----------------------------------------------------------------------------
/*!
specialization of universal template to int, wrap number to specified interval, the 
result is from interval <min, max - 1>
\param a		number
\param min	minimum 
\param max	maximum
*/
//-----------------------------------------------------------------------------
template<> ENF_INLINEFUNC int enf_wrap<int>(int a, int min, int max)
{
	int x = a - min;

	if (x >= 0)
	{
		return min + x%(max - min);
	} else
	{
		return max + x%(max - min);
	}
}



//--------------------------------------------------------------
/*
Align the number
\param num		number to align
\param align	where to align
\return			aligned number
*/
//--------------------------------------------------------------
ENF_INLINEFUNC uint Align(uint num, uint align)
{
	return ((num + (align - 1)) / align) * align;
}

//--------------------------------------------------------------
/*
Align the number to 16 bytes
\param num		number to align
\return			aligned number
*/
//--------------------------------------------------------------
ENF_INLINEFUNC uint Align16(uint num)
{
	return ((num + 15) & 0xfffffff0);
}

//--------------------------------------------------------------
/*
Align the number to 8 bytes
\param num		number to align
\return			aligned number
*/
//--------------------------------------------------------------
ENF_INLINEFUNC uint Align8(uint num)
{
	return ((num + 7) & 0xfffffff8);
}



/*!
Convert string to vector
\param str
\return vector
*/
Vector3 strtovec(const char *str);


//-----------------------------------------------------------------------------
/*!
class Vector3
*/
//-----------------------------------------------------------------------------
class Vector3
{
public:

	//!vector data
	union
	{
		float v[3];				///< array of three floats
		
		struct
		{
			float x, y, z;		///< x, y, z of the vector
		};
	};


	//-----------------------------------------------------------------------------
	/*!
	Constructor, from three numbers
	\param ix	x
	\param iy	y
	\param iz	z
	*/
	//-----------------------------------------------------------------------------
	ENF_INLINE Vector3(float ix, float iy, float iz)
	{
		x = ix; y = iy; z = iz;
	}

	//-----------------------------------------------------------------------------
	/*!
	Constructor, 'f' is copied to all components	
	\param f		f,f,f
	*/
	//-----------------------------------------------------------------------------
	ENF_INLINE Vector3(float f)
	{
		x = y = z = f;
	}

	//-----------------------------------------------------------------------------
	/*!
	empty constructor
	*/
	//-----------------------------------------------------------------------------
	Vector3()
	{
	}


	ENF_INLINE Vector3(const Vector4& vec);
	ENF_INLINE Vector3& operator=(const Vector4& vec);

	//-----------------------------------------------------------------------------
	/*!
	constructor, from a pointer to float[3]
	\param ptr	pointer to floats
	*/
	//-----------------------------------------------------------------------------
	ENF_INLINE Vector3(const float *ptr)
	{
		x = ptr[0]; y = ptr[1]; z = ptr[2];
	}


	//--------------------------------------------------------------
	/*!
	Is vector zero
	\return true if vector has all components zeroed
	*/
	//--------------------------------------------------------------
	ENF_INLINE bool IsZero() const
	{
		return (x == 0 && y == 0 && z == 0);
	}


	//--------------------------------------------------------------
	/*!
	Is vector epsilon zero
	\param eps	epsilon for testing
	\return true if vector has all components zeroed
	*/
	//--------------------------------------------------------------
	ENF_INLINE bool IsZero(float eps) const
	{
		return (fabs(x) < eps && fabs(y) < eps && fabs(z) < eps);
	}


	//--------------------------------------------------------------
	/*!
	Zero vector
	\return reference
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3& Zero()
	{
		x = y = z = 0.0f;
		return *this;
	}


	//--------------------------------------------------------------
	/*!
	operator [], get component
	\return component
	*/
	//--------------------------------------------------------------
	ENF_INLINE float operator[](uint index) const
	{
		enf_assert(index <= 2);
		return v[index];
	}


	//--------------------------------------------------------------
	/*!
	operator ==, Are vectors equal?
	\return true if they are equal
	*/
	//--------------------------------------------------------------
	ENF_INLINE bool operator==(const Vector3& in) const
	{
		return (x == in.x && y == in.y && z == in.z);
	}


	//--------------------------------------------------------------
	/*!
	operator !, Are not vectors equal?
	\return true if they are not equal
	*/
	//--------------------------------------------------------------
	ENF_INLINE bool operator!=(const Vector3& in) const
	{
		return (x != in.x || y != in.y || z != in.z);
	}


	//--------------------------------------------------------------
	/*!
	operator <, return lesser components from both operands
	\return vector 
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 operator<(const Vector3& in)
	{
		return Vector3(x < in.x ? x : in.x, y < in.y ? y : in.y, z < in.z ? z : in.z);
	}


	//--------------------------------------------------------------
	/*!
	operator >, return bigger components from both operands
	\return vector 
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 operator>(const Vector3& in)
	{
		return Vector3(x > in.x ? x : in.x, y > in.y ? y : in.y, z > in.z ? z : in.z);
	}


	//--------------------------------------------------------------
	/*!
	operator =, copy from reference
	\return reference
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3& operator=(const Vector3& in)
	{
		x = in.x; y = in.y; z = in.z;
		return *this;
	}

	//--------------------------------------------------------------
	/*!
	operator =, copy from pointer to Vector3
	\return reference
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3& operator=(const Vector3* in)
	{
		x = in->x; y = in->y; z = in->z;
		return *this;
	}

	//--------------------------------------------------------------
	/*!
	operator =, copy from pointer to floats
	\return reference
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3& operator=(const float *ptr)
	{
		x = ptr[0]; y = ptr[1]; z = ptr[2];
		return *this;
	}

	//--------------------------------------------------------------
	/*!
	Linear interpolation between two vectors, lerp == 0 -> return this
	\param vec		second vector3 for interpolation as reference
	\param lerp		parameter
	\return interpolated vector
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 Lerp(const Vector3& vec, float lerp) const
	{
		return Vector3((vec.x - x) * lerp + x, (vec.y - y) * lerp + y, (vec.z - z) * lerp + z);
	}


	//--------------------------------------------------------------
	/*!
	operator *, scale vector
	\param fac		scale factor
	\return			Vector3(x * fac, y * fac, z * fac)
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 operator*(float fac) const
	{
		return Vector3(x * fac, y * fac, z * fac);
	}


	//--------------------------------------------------------------
	/*!
	operator *=, scale vector and set
	\param fac		scale factor
	\return			reference
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3& operator*=(float fac)
	{
		x *= fac; y *= fac; z *= fac;
		return *this;
	}

	//--------------------------------------------------------------
	/*!
	operator +, add two vectors
	\param in		vector to add to this one
	\return			new vector
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 operator+(const Vector3& in) const
	{
		return Vector3(x + in.x, y + in.y, z + in.z);
	}

	//--------------------------------------------------------------
	/*!
	Multiply and add
	\param add		vector to add
	\param mult		vector to multiply
	\return			new vector, add*mult + this
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 MA(const Vector3& add, float mult) const
	{
		if(mult == 0)
			return *this;

		if(mult == 1.0f)
			return *this + add;

		return add * mult + *this;
	}

	//--------------------------------------------------------------
	/*!
	operator +=, add vector and set
	\param in		vector to add to this one, pointer to float
	\return			reference
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3& operator+=(const Vector3& in)
	{
		x += in.x; y += in.y; z += in.z;
		return *this;
	}

	//--------------------------------------------------------------
	/*!
	operator -, subtract two vectors
	\param in		vector to subtract from this one
	\return			new vector
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 operator-(const Vector3 in) const
	{
		return Vector3(x - in.x, y - in.y, z - in.z);
	}

	//--------------------------------------------------------------
	/*!
	operator -, subtract two vectors and set
	\param in		vector to subtract from this one
	\return			reference
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3& operator-=(const Vector3& in)
	{
		x -= in.x; y -= in.y; z -= in.z;
		return *this;
	}

	//--------------------------------------------------------------
	/*!
	Dot product
	\param in	vector for dot product
	\return		dot product
	*/
	//--------------------------------------------------------------
	ENF_INLINE float Dot(const Vector3& in) const
	{
		return x * in.x + y * in.y + z * in.z;
	}

	//--------------------------------------------------------------
	/*!
	Dot product in double
	\param in	vector for dot product
	\return		dot product
	*/
	//--------------------------------------------------------------
	ENF_INLINE double Dotd(const Vector3& in) const
	{
		return x * in.x + y * in.y + z * in.z;
	}

	//--------------------------------------------------------------
	/*!
	operator -, make inverse of vector
	\return		new inverted vector
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 operator-() const
	{
		return Vector3(-x, -y, -z);
	}


	//--------------------------------------------------------------
	/*!
	operator ^, make cross product of two vectors
	\param in	second vector
	\return		new vector of cross product
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 operator^(const Vector3& in) const
	{
		return Vector3(y * in.z - z * in.y, z * in.x - x * in.z, x * in.y - y * in.x);
	}


	//--------------------------------------------------------------
	/*!
	Euclidean length of vector
	\return		euclidean length
	*/
	//--------------------------------------------------------------
	ENF_INLINE float Length() const
	{
		double d = x * x + y * y + z * z;

		if(d == 0.0)
			return 0.0f;

		if(d == 1.0)
			return 1.0f;

		return (float)sqrt(d);
	}


	//--------------------------------------------------------------
	/*!
	L1(Manhattan) length of vector (sum)
	\return		euclidean length
	*/
	//--------------------------------------------------------------
	ENF_INLINE float LengthL1() const
	{
		return fabs(x) + fabs(y) + fabs(z);
	}

	//--------------------------------------------------------------
	/*!
	Fast euclidean length of vector (fastsqrt lookup function used)
	\return		fast euclidean length
	*/
	//--------------------------------------------------------------
	ENF_INLINE float QLength() const
	{
		return fastsqrt(x * x + y * y + z * z);
	}

	//--------------------------------------------------------------
	/*!
	Euclidean square length of vector
	\return		square euclidean length
	*/
	//--------------------------------------------------------------
	ENF_INLINE float LengthSq() const
	{
		return (x * x + y * y + z * z);
	}

	//--------------------------------------------------------------
	/*!
	Distance from another point
	\param dest second point
	\return		euclidean distance between points
	*/
	//--------------------------------------------------------------
	ENF_INLINE float Distance(const Vector3& dest) const
	{
		return sqrtf(sqr(x - dest.x) + sqr(y - dest.y) + sqr(z - dest.z));
	}

	//--------------------------------------------------------------
	/*!
	L1(Manhattan) distance from another point
	\param dest second point
	\return		L1 distance between points
	*/
	//--------------------------------------------------------------
	ENF_INLINE float DistanceL1(const Vector3& dest) const
	{
		return fabs(x - dest.x) + fabs(y - dest.y) + fabs(z - dest.z);
	}

	//--------------------------------------------------------------
	/*!
	Euclidean square distance between two points
	\param dest second point
	\return		euclidean distance between points
	*/
	//--------------------------------------------------------------
	ENF_INLINE float SquaredDistance(const Vector3& dest) const
	{
		return sqr(x - dest.x) + sqr(y - dest.y) + sqr(z - dest.z);
	}

  //--------------------------------------------------------------
  /*!
  Normalize vector
  \return		length of vector
  */
  //--------------------------------------------------------------
  ENF_INLINE float Normalize()
  {
    float length = x * x + y * y + z * z;

    if(length != 1 && length > 0)
    {
      length = sqrtf(length);

      float ilength = 1.0f / length;
      x *= ilength;
      y *= ilength;
      z *= ilength;
    }

    return length;
  }

  //--------------------------------------------------------------
  /*!
  Normalize vector
  \return		length of vector
  */
  //--------------------------------------------------------------
  ENF_INLINE Vector3 Normalized() const
  {
    float length = x * x + y * y + z * z;
    
    if(length != 1 && length > 0)
    {
      float ilength = 1.0f / sqrtf(length);
      return Vector3(x * ilength,  y * ilength, z * ilength);
    }

    return *this;
  }


	//--------------------------------------------------------------
	/*!
	Normalize vector using fastsqrt
	\return		length of vector
	*/
	//--------------------------------------------------------------
	ENF_INLINE float QNormalize()
	{
		float length = x * x + y * y + z * z;

		if(length != 1 && length > 0)
		{
			length = fastsqrt(length);

			float ilength = 1.0f / length;
			x *= ilength;
			y *= ilength;
			z *= ilength;
		}

		return length;
	}


	//--------------------------------------------------------------
	/*!
	Clamp vector to specified interval
	\param minv	 minimum
	\param maxv	 maximum
	\return self clamped vector
	*/
	//--------------------------------------------------------------
	ENF_INLINE Vector3 Clamp(float minv, float maxv)
	{
		x = enf_limit(x, minv, maxv);
		y = enf_limit(y, minv, maxv);
		z = enf_limit(z, minv, maxv);
		return *this;
	}


	/*!
	Rotate point around vector
	\param point		which point
	\param degrees		the angle of rotation
	*/
	Vector3	RotatePointAroundVector(const Vector3& point, float degrees );


	/*!
	Make one perpendicular vector to this one
	\return	perpendicular vector
	*/
	Vector3 Perpendicular() const;


	//-----------------------------------------------------------------------------
	/*!
	convert from string
	\param instr	input string
	*/
	//-----------------------------------------------------------------------------
	void FromString(const char* instr)
	{
		*this = strtovec(instr);
	}
};




//--------------------------------------------------------------
/*!
class Vector4
*/
//--------------------------------------------------------------
class Vector4
{
public:

	union
	{
		float v[4];
		
		struct
		{
			float x, y, z, w;
		};
	};

//--------------------------------------------------------------
// Constructor
//--------------------------------------------------------------
	ENF_INLINE Vector4(float ix, float iy, float iz, float iw)
	{
		x = ix; y = iy; z = iz; w = iw;
	}

//--------------------------------------------------------------
	ENF_INLINE Vector4(float f)
	{
		x = y = z = w = f;
	}

//--------------------------------------------------------------
	Vector4()
	{
	}

//--------------------------------------------------------------
	Vector4(const Vector3& vec, float lw)
	{
		x = vec.x; y = vec.y; z = vec.z; w = lw;
	}

//--------------------------------------------------------------
	ENF_INLINE Vector4(const float *ptr)
	{
		x = ptr[0]; y = ptr[1]; z = ptr[2]; w = ptr[3];
	}

//--------------------------------------------------------------
	ENF_INLINE Vector4(const Vector3& vec)
	{
		x = vec.x; y = vec.y; z = vec.z; w = 0.0f;
	}

//--------------------------------------------------------------
	ENF_INLINE void Zero()
	{
		x = y = z = w = 0.0f;
	}

//--------------------------------------------------------------
//
//--------------------------------------------------------------
	ENF_INLINE float operator[](uint index) const
	{
		enf_assert(index <= 3);
		return v[index];
	}

//--------------------------------------------------------------
// copy/move/convert
//--------------------------------------------------------------
	ENF_INLINE Vector4& operator=(const Vector4& in)
	{
		x = in.x; y = in.y; z = in.z; w = in.w;
		return *this;
	}

	ENF_INLINE Vector4& operator=(const Vector3& in)
	{
		x = in.x; y = in.y; z = in.z; w = 0.0f;
		return *this;
	}

//--------------------------------------------------------------
	ENF_INLINE Vector4& operator=(const float *ptr)
	{
		x = ptr[0]; y = ptr[1]; z = ptr[2]; w = ptr[3];
		return *this;
	}

//--------------------------------------------------------------
	ENF_INLINE bool operator==(const Vector4& vec) const
	{
		return (x == vec.x && y == vec.y && z == vec.z && w == vec.w);
	}

//--------------------------------------------------------------
// Scale
//--------------------------------------------------------------
	ENF_INLINE Vector4 operator*(float fac) const
	{
		return Vector4(x * fac, y * fac, z * fac, w * fac);
	}

//--------------------------------------------------------------
// Scale
//--------------------------------------------------------------
	ENF_INLINE Vector4& operator*=(float fac)
	{
		x *= fac; y *= fac; z *= fac; w *= fac;
		return *this;
	}

//--------------------------------------------------------------
// Addition
//--------------------------------------------------------------
	ENF_INLINE Vector4 operator+(const Vector4& in) const
	{
		return Vector4(x + in.x, y + in.y, z + in.z, w + in.w);
	}

//--------------------------------------------------------------
// Addition
//--------------------------------------------------------------
	ENF_INLINE Vector4& operator+=(const Vector4& in)
	{
		x += in.x; y += in.y; z += in.z; w += in.w;
		return *this;
	}

//--------------------------------------------------------------
// Subtract
//--------------------------------------------------------------
	ENF_INLINE Vector4 operator-(const Vector4& in) const
	{
		return Vector4(x - in.x, y - in.y, z - in.z, w - in.w);
	}

	ENF_INLINE Vector4 operator-(const Vector3& in) const
	{
		return Vector4(x - in.x, y - in.y, z - in.z, w);
	}

//--------------------------------------------------------------
// Subtract
//--------------------------------------------------------------
	ENF_INLINE Vector4& operator-=(const Vector4& in)
	{
		x -= in.x; y -= in.y; z -= in.z; w -= in.w;
		return *this;
	}

//--------------------------------------------------------------
// Dot product
//--------------------------------------------------------------
	ENF_INLINE float operator*(const Vector4& in) const
	{
		return x * in.x + y * in.y + z * in.z + w * in.w;
	}

//--------------------------------------------------------------
// Dot product
//--------------------------------------------------------------
	ENF_INLINE float Dot(const Vector3& point) const
	{
		return (x * point.x + y * point.y + z * point.z);
	}

//--------------------------------------------------------------
// Reverse
//--------------------------------------------------------------
	ENF_INLINE Vector4 operator-() const
	{
		return Vector4(-x, -y, -z, -w);
	}

//--------------------------------------------------------------
// Cross product
//--------------------------------------------------------------
	ENF_INLINE Vector4 operator^(const Vector3& in) const
	{
		Vector4 res;
		res.x = y * in.z - z * in.y;
		res.y = z * in.x - x * in.z;
		res.z = x * in.y - y * in.x;
		res.w = 0.0f;
		return res;
	}

	ENF_INLINE float Normalize()
	{
		float length = x * x + y * y + z * z;

		if(length != 1 && length > 0)
		{
			length = sqrtf(length);

			float ilength = 1.0f / length;
			x = ilength * x;
			y = ilength * y;
			z = ilength * z;
		}

		return length;
	}

	ENF_INLINE float QLength() const
	{
		return fastsqrt(x * x + y * y + z * z + w * w);
	}

	ENF_INLINE float Length() const
	{
		return sqrtf(x * x + y * y + z * z + w * w);
	}

	ENF_INLINE void Clamp(float minv, float maxv)
	{
		x = enf_limit(x, minv, maxv);
		y = enf_limit(y, minv, maxv);
		z = enf_limit(z, minv, maxv);
		w = enf_limit(w, minv, maxv);
	}
};

ENF_INLINE Vector3::Vector3(const Vector4& vec)
{
	x = vec.x; y = vec.y; z = vec.z;
}

ENF_INLINE Vector3& Vector3::operator=(const Vector4& vec)
{
	x = vec.x; y = vec.y; z = vec.z;
	return *this;
}

//--------------------------------------------------------------
/*!
class Quaternion
*/
//--------------------------------------------------------------
class Quaternion: public Vector4
{
public:
	Quaternion(bool init = false)
	{
		if(init) Identity();
	}

	Quaternion(float xx, float yy, float zz, float ww)
	{
		x = xx; y = yy; z = zz; w = ww;
	}

	Quaternion operator=(const Vector4& vec)
	{
		x = vec.x; y = vec.y; z = vec.z; w = vec.w;
		return *this;
	}

	Quaternion Lerp(const Quaternion& q, float lerp) const;
	Quaternion Lerp(const Quaternion* q, float lerp) const;

	float Dot(const Quaternion& quat) const;
	float Dot(const Quaternion* quat) const;

	ENF_INLINE bool IsIdentity() const
	{
		return (x == 0.0f && y == 0.0f && z == 0.0f && w == 1.0f);
	}

	ENF_INLINE Quaternion& Identity()
	{
		x = y = z = 0.0f;
		w = 1.0f;
		return *this;
	}

	ENF_INLINE Quaternion operator*( const Quaternion& a ) const
	{
		return Quaternion(w * a.x + x * a.w + y * a.z - z * a.y,
								w * a.y + y * a.w + z * a.x - x * a.z,
								w * a.z + z * a.w + x * a.y - y * a.x,
								w * a.w - x * a.x - y * a.y - z * a.z);
	}

	Vector3 operator*( const Vector3& a ) const;

	ENF_INLINE float Length() const
	{
		return sqrtf(x * x + y * y + z * z + w * w);
	}

	float ToPitch() const;
	Vector3 ToMatrixRow(uint row) const;

	Quaternion& Normalize();
};

class Matrix43;

class Matrix44
{
private:
	static Matrix44 Ident;
public:
	
	static const Matrix44& GetIdent()
	{
		return Ident;
	}

	union
	{
		float	m[4][4];
		struct
		{
			float _11, _12, _13, _14;
			float	_21, _22, _23, _24;
			float	_31, _32, _33, _34;
			float	_41, _42, _43, _44;
		};
	};

	Matrix44& operator=(const Quaternion& quat);
	Matrix44& operator=(const Quaternion* quat);

	Matrix44& operator=(const Matrix43& mat);
	Matrix44& operator=(const Matrix43* mat);

	ENF_INLINE Matrix44& operator=(const Matrix44& mat)
	{
		CopyMem(m, mat.m, sizeof(m));
		return *this;
	}

	Matrix44& RollPitchYaw(float roll, float pitch, float yaw);
	Matrix44& Roll(float roll);
	Matrix44& Pitch(float pitch);
	Matrix44& Yaw(float yaw);

	Matrix44 MultiplyTranspose(const Matrix44& mat) const;

	Vector4 TransformPlane(const Vector4& plane) const;
	Vector3 TransformCoord(const Vector3& coord) const;
	Vector3 TransformVector(const Vector3& vec) const;
	Vector4 InvTransformCoord(const Vector4& coord) const;
	Vector4 InvTransformCoord(const Vector3& coord) const;
	Vector4 InvTransformVector(const Vector3& vec) const;

	Matrix44 PerspectiveFovLH(float fovy, float aspect, float zn, float zf);
	Matrix44 OrthoLH(float width, float height, float zn, float zf);
	Matrix44 OrthoRH(float width, float height, float zn, float zf);

	void SetScale(float xscale, float yscale, float zscale)
	{
		_11 = xscale; _12 = 0.0f; _13 = 0.0f; _14 = 0.0f;
		_21 = 0.0f; _22 = yscale; _23 = 0.0f; _24 = 0.0f;
		_31 = 0.0f; _32 = 0.0f; _33 = zscale; _34 = 0.0f;
		_41 = 0.0f; _42 = 0.0f; _43 = 0.0f; _44 = 1.0f;
	}

	Matrix44 GetInverse() const;
	Matrix44 GetTranspose() const;

	//solver for linear equations by LUP decomposition
	void SeparateLU(Matrix44 &L, Matrix44 &U) const;
	bool LUPDecomposition(Matrix44 &LU, uint *P) const;
	bool SolveEquationLUP(const Vector4 &b, Vector4 &x) const;


	Matrix44 operator*(const Matrix44& mat) const;
	Matrix44 operator*(const Matrix44* mat) const;
	Matrix44& operator*=(const Matrix44& mat);
	Matrix44& operator*=(const Matrix44* mat);

	ENF_INLINE  Vector4& operator[](uint row)
	{
		enf_assert(row <= 3);
		return *((Vector4*)&m[row][0]); //ultramegahumus
	}

	ENF_INLINE const Vector4& operator[](uint row) const
	{
		enf_assert(row <= 3);
		return *((Vector4*)&m[row][0]); //ultramegahumus
	}

	void SetRow(uint row, const Vector3& v)
	{
		enf_assert(row <= 3);
		m[row][0] = v.x;
		m[row][1] = v.y;
		m[row][2] = v.z;
		m[row][3] = (row == 3) ? 1.0f : 0.0f;
	}

	void SetRow(uint row, const Vector4& v)
	{
		enf_assert(row <= 3);
		m[row][0] = v.x;
		m[row][1] = v.y;
		m[row][2] = v.z;
		m[row][3] = v.w;
	}

	Matrix44& Identity()
	{
		_11 = 1.0f; _12 = 0.0f; _13 = 0.0f; _14 = 0.0f;
		_21 = 0.0f; _22 = 1.0f; _23 = 0.0f; _24 = 0.0f;
		_31 = 0.0f; _32 = 0.0f; _33 = 1.0f; _34 = 0.0f;
		_41 = 0.0f; _42 = 0.0f; _43 = 0.0f; _44 = 1.0f;
		return *this;
	}

	bool IsIdentity() const
	{
		if(_11 != 1.0f || _12 != 0.0f || _13 != 0.0f || _14 != 0.0f ||
			_21 != 0.0f || _22 != 1.0f || _23 != 0.0f || _24 != 0.0f ||
			_31 != 0.0f || _32 != 0.0f || _33 != 1.0f || _34 != 0.0f ||
			_41 != 0.0f || _42 != 0.0f || _43 != 0.0f || _44 != 1.0f)
			return false;

		return true;
	}


	Matrix44(bool ident = false)
	{
		if(ident) Identity();
	}

	Matrix44(const Vector4& row0,
				const Vector4& row1, 
				const Vector4& row2, 
				const Vector4& row3)
	{
		_11 = row0.x; _12 = row0.y; _13 = row0.z; _14 = row0.w;
		_21 = row1.x; _22 = row1.y; _23 = row1.z; _24 = row1.w;
		_31 = row2.x; _32 = row2.y; _33 = row2.z; _34 = row2.w;
		_41 = row3.x; _42 = row3.y; _43 = row3.z; _44 = row3.w;
	}

	Matrix44(float f11, float f12, float f13, float f14,
				float f21, float f22, float f23, float f24,
				float f31, float f32, float f33, float f34,
				float f41, float f42, float f43, float f44)
	{
		_11 = f11; _12 = f12; _13 = f13; _14 = f14;
		_21 = f21; _22 = f22; _23 = f23; _24 = f24;
		_31 = f31; _32 = f32; _33 = f33; _34 = f34;
		_41 = f41; _42 = f42; _43 = f43; _44 = f44;
	}

	Matrix44(const Matrix43& mat);
};

class Matrix43
{
private:

	static Matrix43 Ident;

public:
	
	static const Matrix43& GetIdent()
	{
		return Ident;
	}

	static void Matrix43::_multiply4(Matrix43& out, const Matrix43& a, const Matrix43& b);
	static void Matrix43::_multiply3(Matrix43& out, const Matrix43& a, const Matrix43& b);

	union
	{
		float	m[4][3];
		struct
		{
			float _11, _12, _13;
			float	_21, _22, _23;
			float	_31, _32, _33;
			float	_41, _42, _43;
		};
	};

//-----------------------------------------------------------------------
	Matrix43&	operator=(const Matrix44& mat);
	Matrix43&	operator=(const Matrix44* mat);
	Matrix43&	operator=(const Quaternion& quat);
	Matrix43&	operator=(const Quaternion* quat);

	//return just orientation part of matrix.
	Matrix43		GetOrientation() const
	{
		return Matrix43(_11, _12, _13,
							_21, _22, _23,
							_31, _32, _33,
							0.0f, 0.0f, 0.0f);
	}

	const Vector3& GetTranslation() const
	{
		return *(Vector3*)&_41;
	}

	Matrix43&	AroundVector( const Vector3 dir, float degrees );
	Vector3		TransformCoord(const Vector3& coord) const;
	Vector3		TransformVector(const Vector3& vec) const;
	Vector3		InvTransformCoord(const Vector3& coord) const;
	Vector3		InvTransformVector(const Vector3& vec) const;
	Matrix43		InvTransform(const Matrix43& mat) const;

	Vector4		TransformPlane(const Vector4& plane) const; 
	Matrix43&	RollPitchYaw(float roll, float pitch, float yaw);
	Matrix43&	Roll(float roll);
	Matrix43&	Pitch(float pitch);
	Matrix43&	Yaw(float yaw);

	Matrix43&	Reflect(const Vector4& plane);
	
	bool operator==(const Matrix43& mat) const;
	bool operator!=(const Matrix43& mat) const
	{
		return !(operator==(mat));
	}

//-----------------------------------------------------------------------
	bool IsOrthoNormal() const;
	bool IsOrthogonal() const;
	bool IsNormalized() const;

	bool IsRotated() const
	{
		if(_11 != 1.0f || _22 != 1.0f || _33 != 1.0f ||
			_12 != 0.0f || _13 != 0.0f ||
			_21 != 0.0f || _23 != 0.0f ||
			_31 != 0.0f || _32 != 0.0f)
			return true;

		return false;
	}

	bool IsIdentity() const;

//-----------------------------------------------------------------------
	ENF_INLINE  Matrix43& operator=(const Matrix43& mat)
	{
		CopyMem(m, mat.m, sizeof(m));
		return *this;
	}

//-----------------------------------------------------------------------
	void SetScale(float xscale, float yscale, float zscale)
	{
		_11 = xscale; _12 = 0.0f; _13 = 0.0f;
		_21 = 0.0f; _22 = yscale; _23 = 0.0f;
		_31 = 0.0f; _32 = 0.0f; _33 = zscale;
		_41 = 0.0f; _42 = 0.0f; _43 = 0.0f;
	}

//-----------------------------------------------------------------------
	ENF_INLINE  Matrix43 Multiply4(const Matrix43& mat)
	{
		Matrix43 result;

		_multiply4(result, *this, mat);
		return result;
	}

//-----------------------------------------------------------------------
	ENF_INLINE  Matrix43 Multiply3(const Matrix43& mat)
	{
		Matrix43 result;

		_multiply3(result, *this, mat);
		return result;
	}

//-----------------------------------------------------------------------
	ENF_INLINE  Matrix43 operator*(const Matrix43& mat) const
	{
		Matrix43 result;

		_multiply4(result, *this, mat);
		return result;
	}

//-----------------------------------------------------------------------
	ENF_INLINE  Matrix43& operator*=(const Matrix43& mat)
	{
		Matrix43 result;

		_multiply4(result, *this, mat);
		*this = result;
		return *this;
	}

//-----------------------------------------------------------------------
	ENF_INLINE  const Vector3& operator[](uint row) const 
	{
		enf_assert(row <= 3);
		return *((Vector3*)&m[row][0]);
	}

//-----------------------------------------------------------------------
	ENF_INLINE const Vector3& GetRow(uint row) const
	{
		enf_assert(row <= 3);
		return *((Vector3*)&m[row][0]);
	}

//-----------------------------------------------------------------------
	ENF_INLINE void SetRow(uint row, const Vector3& vec)
	{
		enf_assert(row <= 3);

		m[row][0] = vec.x;
		m[row][1] = vec.y;
		m[row][2] = vec.z;
	}

//-----------------------------------------------------------------------
	//! Returns determinant of 3x3 sub-matrix
	float GetDeterminant() const
	{
		return _11 * _22 * _33 + _12 * _23 * _31 + _13 * _21 * _32 -
				_13 * _22 * _31 - _12 * _21 * _33 - _11 * _23 * _32;
	}

	Matrix43 GetInverse() const;

//-----------------------------------------------------------------------
	Matrix43& Identity()
	{
		_11 = 1.0f; _12 = 0.0f; _13 = 0.0f;
		_21 = 0.0f; _22 = 1.0f; _23 = 0.0f;
		_31 = 0.0f; _32 = 0.0f; _33 = 1.0f;
		_41 = 0.0f; _42 = 0.0f; _43 = 0.0f;
		return *this;
	}

//-----------------------------------------------------------------------
	Matrix43(const Vector3& origin)
	{
		_11 = 1.0f; _12 = 0.0f; _13 = 0.0f;
		_21 = 0.0f; _22 = 1.0f; _23 = 0.0f;
		_31 = 0.0f; _32 = 0.0f; _33 = 1.0f;
		_41 = origin.x; _42 = origin.y; _43 = origin.z;
	}

//-----------------------------------------------------------------------
	Matrix43(const Vector3& row0,
				const Vector3& row1, 
				const Vector3& row2, 
				const Vector3& row3)
	{
		_11 = row0.x; _12 = row0.y; _13 = row0.z;
		_21 = row1.x; _22 = row1.y; _23 = row1.z;
		_31 = row2.x; _32 = row2.y; _33 = row2.z;
		_41 = row3.x; _42 = row3.y; _43 = row3.z;
	}

//-----------------------------------------------------------------------
	Matrix43(float f11, float f12, float f13,
				float f21, float f22, float f23,
				float f31, float f32, float f33,
				float f41, float f42, float f43)
	{
		_11 = f11; _12 = f12; _13 = f13;
		_21 = f21; _22 = f22; _23 = f23;
		_31 = f31; _32 = f32; _33 = f33;
		_41 = f41; _42 = f42; _43 = f43;
	}

//-----------------------------------------------------------------------
	Matrix43(bool ident = false)
	{
		if(ident) Identity();
	}

//-----------------------------------------------------------------------
	Matrix43(const Matrix44& mat)
	{
		_11 = mat._11; _12 = mat._12; _13 = mat._13;
		_21 = mat._21; _22 = mat._22; _23 = mat._23;
		_31 = mat._31; _32 = mat._32; _33 = mat._33;
		_41 = mat._41; _42 = mat._42; _43 = mat._43;
	}
};

//-----------------------------------------------------------------------
class Matrix33
{
private:

public:
	static void Matrix33::_multiply(Matrix33& out, const Matrix33& a, const Matrix33& b);

	union
	{
		float	m[3][3];
		struct
		{
			float _11, _12, _13;
			float	_21, _22, _23;
			float	_31, _32, _33;
		};
	};

//-----------------------------------------------------------------------
	Vector3		TransformVector(const Vector3& vec) const;
	Vector3		InvTransformVector(const Vector3& vec) const;
	Matrix33&	RollPitchYaw(const Vector3& angles);
	Matrix33&	Roll(float roll);
	Matrix33&	Pitch(float pitch);
	Matrix33&	Yaw(float yaw);

	//solver for linear equations by LUP decomposition
	void SeparateLU(Matrix33 &L, Matrix33 &U) const;
	bool LUPDecomposition(Matrix33 &LU, uint *P) const;
	bool SolveEquationLUP(const Vector3 &b, Vector3 &x) const;

//-----------------------------------------------------------------------
	bool IsIdentity() const
	{
		if(_11 != 1.0f || _12 != 0.0f || _13 != 0.0f ||
			_21 != 0.0f || _22 != 1.0f || _23 != 0.0f ||
			_31 != 0.0f || _32 != 0.0f || _33 != 1.0f)
			return false;

		return true;
	}

//-----------------------------------------------------------------------
	ENF_INLINE  Matrix33& operator=(const Matrix33& mat)
	{
		CopyMem(m, mat.m, sizeof(m));
		return *this;
	}

//-----------------------------------------------------------------------
	void SetScale(float xscale, float yscale, float zscale)
	{
		_11 = xscale; _12 = 0.0f; _13 = 0.0f;
		_21 = 0.0f; _22 = yscale; _23 = 0.0f;
		_31 = 0.0f; _32 = 0.0f; _33 = zscale;
	}

//-----------------------------------------------------------------------
	ENF_INLINE  Matrix33 operator*(const Matrix33& mat) const
	{
		Matrix33 result;

		_multiply(result, *this, mat);
		return result;
	}

//-----------------------------------------------------------------------
	ENF_INLINE  Matrix33& operator*=(const Matrix33& mat)
	{
		Matrix33 result;

		_multiply(result, *this, mat);
		*this = result;
		return *this;
	}

//-----------------------------------------------------------------------
	ENF_INLINE  const Vector3& operator[](uint row) const 
	{
		enf_assert(row <= 2);
		return *((Vector3*)&m[row][0]);
	}

//-----------------------------------------------------------------------
	ENF_INLINE void SetRow(uint row, const Vector3& vec)
	{
		enf_assert(row <= 2);

		m[row][0] = vec.x;
		m[row][1] = vec.y;
		m[row][2] = vec.z;
	}

//-----------------------------------------------------------------------
	Matrix33& Identity()
	{
		_11 = 1.0f; _12 = 0.0f; _13 = 0.0f;
		_21 = 0.0f; _22 = 1.0f; _23 = 0.0f;
		_31 = 0.0f; _32 = 0.0f; _33 = 1.0f;
		return *this;
	}

//-----------------------------------------------------------------------
	Matrix33(bool ident = false)
	{
		if(ident) Identity();
	}

//-----------------------------------------------------------------------
	Matrix33(const Vector3& row0, const Vector3& row1, const Vector3& row2)
	{
		_11 = row0.x; _12 = row0.y; _13 = row0.z;
		_21 = row1.x; _22 = row1.y; _23 = row1.z;
		_31 = row2.x; _32 = row2.y; _33 = row2.z;
	}
};

//-----------------------------------------------------------------------
//!Translation&Rotation key. Used for animations
//-----------------------------------------------------------------------
struct QKey
{
	Vector3		m_vTrans;
	Quaternion	m_qQuat;

	Matrix43 ToMatrix() const;
	QKey Lerp(const QKey& q2, float lerp) const;
	QKey LerpRot(const QKey& q, float lerp) const;
	QKey& Identity()
	{
		m_vTrans.Zero();
		m_qQuat.Identity();
		return *this;
	}
};


//-----------------------------------------------------------------------
/*!
MPlane, struct defining the plane in 3D
*/
//-----------------------------------------------------------------------
struct MPlane:public Vector4
{
	int		Type;		///< special type of plane, 0 -> general plane


	//-----------------------------------------------------------------------
	/*!
	empty constructor
	*/
	//-----------------------------------------------------------------------
	MPlane()
	{
	}

	//-----------------------------------------------------------------------
	/*!
	constructor
	\param normal	normal vector of the plane
	\param dist		distance to the origin
	*/
	//-----------------------------------------------------------------------
	MPlane(const Vector3& normal, float dist)
	{
		x = normal.x;
		y = normal.y;
		z = normal.z;
		w = dist;
		Type = 0;
	}


	//-----------------------------------------------------------------------
	/*!
	Compute distance from the point
	\param point	point
	\return			distance to the plane
	*/
	//-----------------------------------------------------------------------
	ENF_INLINE float Distance(const Vector3& point) const
	{
		return (x * point.x + y * point.y + z * point.z) - w;
	}

	//-----------------------------------------------------------------------
	/*!
	operator ==, are the planes equal
	\return	true is the planes are equal (the coeficients)
	*/
	//-----------------------------------------------------------------------
	bool operator==(const MPlane& mp) const
	{
		return (Type == mp.Type && x == mp.x && y == mp.y && z == mp.z && w == mp.w);
	}

	//-----------------------------------------------------------------------
	/*!
	operator =, copy
	\param mp	plane
	*/
	//-----------------------------------------------------------------------
	void operator=(const MPlane& mp)
	{
		x = mp.x; y = mp.y; z = mp.z; w = mp.w;
		Type = mp.Type;
	}

	//-----------------------------------------------------------------------
	/*!
	operator =, set normal vector of plane
	\param vec	normal vector
	\return		reference to plane
	*/
	//-----------------------------------------------------------------------
	MPlane& operator=(const Vector3& vec)
	{
		x = vec.x; y = vec.y; z = vec.z;
		return *this;
	}

	//-----------------------------------------------------------------------
	/*!
	operator =, set normal vector and distance
	\param vec	vector
	\return		reference to plane
	*/
	//-----------------------------------------------------------------------
	MPlane& operator=(const Vector4& vec)
	{
		x = vec.x; y = vec.y; z = vec.z; w = vec.w;
		Type = 0;
		return *this;
	}

	
	//-----------------------------------------------------------------------
	/*!
	Construct plane from three points
	\param p0	first point
	\param p1	second point
	\param p2	third point
	\return		reference to plane
	*/
	//-----------------------------------------------------------------------
	MPlane& FromPoints(const Vector3& p0, const Vector3& p1, const Vector3& p2)
	{
		Vector3 edge1, edge2, norm;

		edge1 = p1 - p0;
		edge2 = p2 - p0;

		norm = edge2 ^ edge1;
		norm.Normalize();
		x = norm.x;
		y = norm.y;
		z = norm.z;
		w = norm.Dot(p0);
		Type = 0;
		return *this;
	}

	
	//-----------------------------------------------------------------------
	/*!
	Project point to the plane
	\param pt	point
	\return		the projected point
	*/
	//-----------------------------------------------------------------------
	Vector3 Project(const Vector3& pt) const;


	//-----------------------------------------------------------------------
	/*!
	Make inversion of the plane
	\return		reference to plane
	*/
	//-----------------------------------------------------------------------
	MPlane &Invert()
	{
		x = -x; y = -y; z = -z; w = -w;
		return *this;
	}

	ENF_INLINE float DistanceOptimized(const Vector3& point)
	{
		int n = Type;
		if(n < 0)
			return *(float *)((char *)point.v + (n & 0xff)) - w;
		else
			return (point.x * x + point.y * y + point.z * z) - w;
	}

	//-----------------------------------------------------------------------
	ENF_INLINE uint IntersectLineOptimized(const Vector3& v1, const Vector3& v2, float& d1, float& d2)
	{
		int n;

		if((n = Type) < 0)
		{
			n &= 0xff;

			d1 = *(float *)((char *)v1.v + n) - w;
			d2 = *(float *)((char *)v2.v + n) - w;
		}
		else
		{
			d1 = (v1.x * x + v1.y * y + v1.z * z) - w;
			d2 = (v2.x * x + v2.y * y + v2.z * z) - w;
		}

		if(d1 >= -DIST_EPSILON && d2 >= -DIST_EPSILON)
			return 1;

		if(d1 < DIST_EPSILON && d2 < DIST_EPSILON)
			return 2;
		
		return 3;
	}
};

/*!
Axis aligned bounding volume. Bounding box
is defined by minimal and maximal bounds.
*/
class AAB
{
protected:
	Vector3	Mins; ///<Minimal bounds
	Vector3	Maxs; ///<Maximal bounds

public:
/*!
Tells whether is bounding volume valid. Valid AAB must
not be negative sized (minimums greater than maximums)
\return
True when AAB is valid, false otherwise
*/
	ENF_INLINE bool IsValid() const
	{
		return (Mins.x <= Maxs.x &&
				Mins.y <= Maxs.y &&
				Mins.z <= Maxs.z);
	}

/*!
Tells whether is bounding volume zero sized.
(Minimums are equal to maximums)
\return
True when AAB is zero sized, false otherwise
*/
	ENF_INLINE bool IsZero() const
	{
		return (Mins.IsZero() && Maxs.IsZero());
	}

/*!
Copy operator.
*/
	ENF_INLINE const AAB& operator=(const AAB& aabb)
	{
		Mins = aabb.Mins;
		Maxs = aabb.Maxs;
		return *this;
	}

/*!
Gets center of AAB. It's the middle between
maximums and minimums of volume.
\return
Point in the center of AAB
*/
	ENF_INLINE Vector3 GetCenter() const
	{
		return (Mins + Maxs) * 0.5f;
	}

/*!
Gets minimum of AAB
\return
Minimums.
*/
	ENF_INLINE const Vector3& GetMins() const
	{
		return Mins;
	}

/*!
Gets maximum of AAB
\return
Maximums.
*/
	ENF_INLINE const Vector3& GetMaxs() const
	{
		return Maxs;
	}

/*!
Gets maximum absolute extent in each axis
\return
Maximum extents
*/
	Vector3 GetExtents() const
	{
		return Vector3((-Mins.x > Maxs.x) ? -Mins.x : Maxs.x,
							(-Mins.y > Maxs.y) ? -Mins.y : Maxs.y,
							(-Mins.z > Maxs.z) ? -Mins.z : Maxs.z);		
	}

/*!
Gets size of AAB. It's distance from
minimum to maximum.
\return
Size of AAB
*/
	ENF_INLINE Vector3 GetSize() const
	{
		return (Maxs - Mins);
	}

/*!
Tells whether is AAB completelly inside
of this AAB.
\param bbox
AAB which we are testing whether is inside of this AAB
\return
True when is inside, false otherwise
*/
	bool Inside(const AAB& bbox)
	{
		return (Mins.x <= bbox.Mins.x &&
			Maxs.x >= bbox.Maxs.x &&
			Mins.y <= bbox.Mins.y &&
			Maxs.y >= bbox.Maxs.y &&
			Mins.z <= bbox.Mins.z &&
			Maxs.z >= bbox.Maxs.z);
	}

/*!
Tells whether is point inside of this AAB.
\param point Point we are testing.
\return
True when is inside, false otherwise.
*/
	bool Intersects(const Vector3& point) const
	{
		return !(Mins.x > point.x ||
			Mins.y > point.y ||
			Mins.z > point.z ||
			Maxs.x < point.x ||
			Maxs.y < point.y ||
			Maxs.z < point.z);
	}

/*!
Tells whether is AAB intersecting this AAB.
\param aabb AAB we are testing.
\return
True when is intersecting, false otherwise.
*/
	bool Intersects(const AAB& aabb) const
	{
		return !((Mins.x > aabb.Maxs.x) ||
			(Mins.y > aabb.Maxs.y) ||
			(Mins.z > aabb.Maxs.z) ||
			(Maxs.x < aabb.Mins.x) ||
			(Maxs.y < aabb.Mins.y) ||
			(Maxs.z < aabb.Mins.z));
	}

/*!
Tells whether is bounding volume intersecting this AAB.
\param mins
Minimums of volume we are testing.
\param maxs
Maximums of volume we are testing.
\return
True when is intersecting, false otherwise.
*/
	bool Intersects(const Vector3& mins, const Vector3& maxs) const
	{
		return !((Mins.x > maxs.x) ||
			(Mins.y > maxs.y) ||
			(Mins.z > maxs.z) ||
			(Maxs.x < mins.x) ||
			(Maxs.y < mins.y) ||
			(Maxs.z < mins.z));
	}

/*!
Updates minimums of this AAB in each axis
where point is lesser than minimum bounds
\param point
the point we are extending volume by
\return
Updated AAB
*/
	ENF_INLINE AAB& AddMin(const Vector3& point)
	{
		Mins.x = enf_min(Mins.x, point.x);
		Mins.y = enf_min(Mins.y, point.y);
		Mins.z = enf_min(Mins.z, point.z);
		return *this;
	}

/*!
Updates maximums of this AAB in each axis
where point is greater than maximum bounds
\param point
the point we are extending volume by
\return
Updated AAB
*/
	ENF_INLINE AAB& AddMax(const Vector3& point)
	{
		Maxs.x = enf_max(Maxs.x, point.x);
		Maxs.y = enf_max(Maxs.y, point.y);
		Maxs.z = enf_max(Maxs.z, point.z);
		return *this;
	}


/*!
Updates volume of this AAB by extents of
other AAB where maximums are greater than
maximums of this AAB and minimums are
lesser than minimums of this AAB.
\param bbox
The AAB we are extending volume by
\return
Updated AAB
*/
	ENF_INLINE AAB& Add(const AAB& bbox)
	{
		AddMin(bbox.Mins);
		AddMax(bbox.Maxs);
		return *this;
	}

/*!
Updates volume of this AAB where point axes
are greater than maximums of this AAB
or lesser than minimums of this AAB.
\param point
the point we are extending volume by
\return
Updated AAB
*/
	ENF_INLINE AAB& Add(const Vector3& point)
	{
		Mins.x = enf_min(Mins.x, point.x);
		Mins.y = enf_min(Mins.y, point.y);
		Mins.z = enf_min(Mins.z, point.z);

		Maxs.x = enf_max(Maxs.x, point.x);
		Maxs.y = enf_max(Maxs.y, point.y);
		Maxs.z = enf_max(Maxs.z, point.z);
		return *this;
	}

/*!
Updates volume of this AAB by point array,
where point axes are greater than maximums
of this AAB or lesser than minimums of this
AAB.
\param points
Array of points we are extending volume by
\param numpoints
Number of points in the array
\param stride
optional parameter, when stride of data in array
is different from sizeof(Vector3)
\return
Updated AAB
*/
	AAB& AddPoints(const Vector3* points, uint numpoints, uint stride = sizeof(Vector3));

/*!
Updates volume of this AAB where axes
are greater than maximums of this AAB
or lesser than minimums of this AAB.
\param x
Position on X axis
\param y
Position on Y axis
\param z
Position on Z axis
\return
Updated AAB
*/
	ENF_INLINE AAB& Add(float x, float y, float z)
	{
		Add(Vector3(x, y, z));
		return *this;
	}

/*!
Updates volume of this AAB where sphere
is exceeding bounds.
\param point
The origin of sphere
\param radius
The radius of sphere
\return
Updated AAB
*/
	ENF_INLINE AAB& AddSphere(const Vector3& point, float radius)
	{
		AddMin(Vector3(point.x - radius, point.y - radius, point.z - radius));
		AddMax(Vector3(point.x + radius, point.y + radius, point.z + radius));
		return *this;
	}

/*!
Updates volume of this AAB by extents of
other AAB (computed from oriented bounding box)
where maximums are greater than
maximums of this AAB and minimums are
lesser than minimums of this AAB.
\param matrix
Orientation of AAB
\param bbox
The AAB we are extending volume by
\return
Updated AAB
*/
	AAB& AddOBB(const Matrix43& matrix, const AAB& bbox);

/*!
Translates whole AAB by offset
\param offset
The offset we are tranaslating AAB by
\return
Updated AAB
*/
	ENF_INLINE AAB& Translate(const Vector3& offset)
	{
		Mins += offset;
		Maxs += offset;
		return *this;
	}

/*!
Zeroes the AAB. Minimums and maximums
are zero.
\return
Updated AAB
*/
	ENF_INLINE AAB& Zero()
	{
		Mins.Zero();
		Maxs.Zero();
		return *this;
	}

/*!
Initializes the AAB. Minimums are set to numeric maximum
and vice versa. The AAB is invalid until some point or volume
is added in.
\return
Updated AAB
*/
	ENF_INLINE AAB& Init()
	{
		Mins.x = Mins.y = Mins.z =  9999999.0f;
		Maxs.x = Maxs.y = Maxs.z = -9999999.0f;
		return *this;
	}

/*!
Initializes the AAB. Minimums and maximums are
set to defined extents.
\param mins
New minimums
\param maxs
New maximums
\return
Updated AAB
*/
	ENF_INLINE AAB& Init(const Vector3& mins, const Vector3& maxs)
	{
		Mins = mins;
		Maxs = maxs;
		return *this;
	}

/*!
Initializes the AAB. Minimums and maximums are
set to defined extents.
\param minx
New minimums in X axis
\param miny
New minimums in Y axis
\param minz
New minimums in Z axis
\param maxx
New maximums in X axis
\param maxy
New maximums in Y axis
\param maxz
New maximums in Z axis
\return
Updated AAB
*/
	ENF_INLINE AAB& Init(float minx, float miny, float minz, float maxx, float maxy, float maxz)
	{
		Mins.x = minx; Mins.y = miny; Mins.z = minz;
		Maxs.x = maxx; Maxs.y = maxy; Maxs.z = maxz;
		return *this;
	}

/*!
Returns corner based on signs in each axis. Returns maximum
for negative sign and minimum for positive sign
\param x
The sign on which is X axis of corner selected
\param y
The sign on which is Y axis of corner selected
\param z
The sign on which is Z axis of corner selected
\return
Selected corner of this AAB
*/
	ENF_INLINE Vector3 GetCornerBySigns(float x, float y, float z) const
	{
		return Vector3(x < 0 ? Maxs.x : Mins.x,
							y < 0 ? Maxs.y : Mins.y,
							z < 0 ? Maxs.z : Mins.z);
	}

/*!
Initializes the AAB. Minimums and maximums are
both set to defined point.
\param point
New minimums and maximums
\return
Updated AAB
*/
	ENF_INLINE AAB& Init(const Vector3& point)
	{
		Mins = Maxs = point;
		return *this;
	}

/*!
Initializes the AAB. Minimums and maximums are
defined from trajectory of moving sphere. Could
be used also as bounding volume of line, when
radius is set to zero.
\param from
Position where trajectory begins
\param to
Position where trajectory ends
\param radius
Radius of sphere
\return
Updated AAB
*/
	AAB& InitFromMovingSphere(const Vector3& from, const Vector3& to, float radius = 0.0f)
	{
		for(int i = 0; i < 3; i++)
		{
			if(to[i] > from[i])
			{
				Mins.v[i] = from[i] - radius;
				Maxs.v[i] = to[i] + radius;
			}
			else
			{
				Mins.v[i] = to[i] - radius;
				Maxs.v[i] = from[i] + radius;
			}
		}
		return *this;
	}

/*!
Initializes the AAB. Minimums and maximums are
defined from trajectory of moving AAB. 
\param from
Position where trajectory begins
\param to
Position where trajectory ends
\param box
AAB of moving box
\param epsilon
Optional epsilon by which is resulting bounding box enlarged
\return
Updated AAB
*/
	AAB& InitFromMovingBox(const Vector3& from, const Vector3& to, const AAB& box, float epsilon = 0.0f)
	{
		for(int i = 0; i < 3; i++)
		{
			if(to[i] > from[i])
			{
				Mins.v[i] = from.v[i] + box.Mins.v[i] - epsilon;
				Maxs.v[i] = to.v[i] + box.Maxs.v[i] + epsilon;
			}
			else
			{
				Mins.v[i] = to.v[i] + box.Mins.v[i] - epsilon;
				Maxs.v[i] = from.v[i] + box.Maxs.v[i] + epsilon;
			}
		}

		return *this;
	}

/*!
Initializes the AAB. Minimums and maximums are
defined by radius of sphere.
\param radius
Radius of sphere
\return
Updated AAB
*/
	AAB& InitFromSphere(float radius)
	{
		Mins.x = Mins.y = Mins.z = -radius;
		Maxs.x = Maxs.y = Maxs.z = radius;
		return *this;
	}

/*!
Initializes the AAB. Minimums and maximums are
defined by AAB translated by offset.
\param pos
Position where AAB is translated
\param box
AAB of box
\param epsilon
Optional epsilon by which is resulting bounding box enlarged
\return
Updated AAB
*/
	AAB& InitFromBox(const Vector3& pos, const AAB& box, float epsilon = 0.0f)
	{
		Mins = pos + box.Mins;
		Maxs = pos + box.Maxs;

		if(epsilon != 0.0f)
		{
			Mins.x -= epsilon;
			Mins.y -= epsilon;
			Mins.z -= epsilon;
			Maxs.x += epsilon;
			Maxs.y += epsilon;
			Maxs.z += epsilon;
		}
		return *this;
	}

/*!
Initializes the AAB. Minimums and maximums are
defined by oriented AAB.
\param matrix
Orientation of AAB
\param box
AAB of box
\return
Updated AAB
*/
	AAB& InitFromOBB(const Matrix43& matrix, const AAB& box);


//-----------------------------------------------------------------------
	float	PlaneDist(const MPlane *pl) const
	{
		Vector3 p;
		float d;

		p.x = (pl->x < 0) ? Mins.x : Maxs.x;
		p.y = (pl->y < 0) ? Mins.y : Maxs.y;
		p.z = (pl->z < 0) ? Mins.z : Maxs.z;

		int n = pl->Type;
		if(n < 0)
			d = *(float *)((char *)&p + (n & 0xff));
		else
			d = p.x * pl->x + p.y * pl->y + p.z * pl->z;

		return (d - pl->w);
	}

//-----------------------------------------------------------------------
/*!
Tells how is AAB related to plane or whether is intersected
\return
bitmask, where 1 means that AAB is inside plane, 2 means
that AAB is outside plane and 3 means that AAB is on both
sides (is crossed by plane).
*/
	ENF_INLINE int OnPlaneOptimized(const MPlane *pl) const
	{
		if(pl->Type < 0)
		{
			int t = 0;
			float *p;

			p = (float *)((char *)&Mins + (pl->Type & 0xff));

			if(p[3] >= pl->w)
				t = 1;

			if(p[0] < pl->w)
				t |= 2;

			return t;
		}
		else
		{
#if 1
			Vector3 center;
			float d1, d2;

			center = ( Mins + Maxs ) * 0.5f;

			d1 = pl->Distance(center);
			d2 = fabsf( ( Maxs.x - center.x ) * pl->x ) +
				fabsf( ( Maxs.y - center.y ) * pl->y ) +
				fabsf( ( Maxs.z - center.z ) * pl->z );

			if ( d1 - d2 > 0 )
			{
				return 1;
			}
			if ( d1 + d2 < 0 )
			{
				return 2;
			}
			return 3;

#else
			int t = 0;

			float d1, d2;

			float x1, y1, z1, x2, y2, z2;

			if(pl->x < 0)
			{
				x1 = Mins.x;
				x2 = Maxs.x;
			}
			else
			{
				x1 = Maxs.x;
				x2 = Mins.x;
			}

			if(pl->y < 0)
			{
				y1 = Mins.y;
				y2 = Maxs.y;
			}
			else
			{
				y1 = Maxs.y;
				y2 = Mins.y;
			}

			if(pl->z < 0)
			{
				z1 = Mins.z;
				z2 = Maxs.z;
			}
			else
			{
				z1 = Maxs.z;
				z2 = Mins.z;
			}

			d1 = pl->x * x1 + pl->y * y1 + pl->z * z1;
			d2 = pl->x * x2 + pl->y * y2 + pl->z * z2;

			if(d1 >= pl->w)
				t = 1;

			if(d2 < pl->w)
				t |= 2;

			return t;
#endif
		}
	}

	ENF_INLINE int OnPlane(const MPlane *pl) const
	{
#if 1
		Vector3 center;
		float d1, d2;

		center = ( Mins + Maxs ) * 0.5f;

		d1 = pl->Distance(center);
		d2 = fabsf( ( Maxs.x - center.x ) * pl->x ) +
			fabsf( ( Maxs.y - center.y ) * pl->y ) +
			fabsf( ( Maxs.z - center.z ) * pl->z );

		if ( d1 - d2 > 0 )
		{
			return 1;
		}
		if ( d1 + d2 < 0 )
		{
			return 2;
		}
		return 3;

#else
		int t = 0;

		float d1, d2;

		float x1, y1, z1, x2, y2, z2;

		if(pl->x < 0)
		{
			x1 = Mins.x;
			x2 = Maxs.x;
		}
		else
		{
			x1 = Maxs.x;
			x2 = Mins.x;
		}

		if(pl->y < 0)
		{
			y1 = Mins.y;
			y2 = Maxs.y;
		}
		else
		{
			y1 = Maxs.y;
			y2 = Mins.y;
		}

		if(pl->z < 0)
		{
			z1 = Mins.z;
			z2 = Maxs.z;
		}
		else
		{
			z1 = Maxs.z;
			z2 = Mins.z;
		}

		d1 = pl->x * x1 + pl->y * y1 + pl->z * z1;
		d2 = pl->x * x2 + pl->y * y2 + pl->z * z2;

		if(d1 >= pl->w)
			t = 1;

		if(d2 < pl->w)
			t |= 2;

		return t;
#endif
	}

/*!
Tells whether is AAB crossed by plane.
\param plane
Normal vector of plane.
\param pdist
Distance of plane to zero
\return
True when is intersected, false otherwise.
*/
	bool CrossingPlane(const Vector3& plane, float pdist) const
	{
		float x1, y1, z1, x2, y2, z2;

		if(plane.x < 0)
		{
			x1 = Mins.x;
			x2 = Maxs.x;
		}
		else
		{
			x1 = Maxs.x;
			x2 = Mins.x;
		}

		if(plane.y < 0)
		{
			y1 = Mins.y;
			y2 = Maxs.y;
		}
		else
		{
			y1 = Maxs.y;
			y2 = Mins.y;
		}

		if(plane.z < 0)
		{
			z1 = Mins.z;
			z2 = Maxs.z;
		}
		else
		{
			z1 = Maxs.z;
			z2 = Mins.z;
		}

		if((plane.x * x2 + plane.y * y2 + plane.z * z2) >= pdist)
			return false;

		return ((plane.x * x1 + plane.y * y1 + plane.z * z1) >= pdist);
	}

/*!
Gets position on, or inside plane which is nearest in each axis to
the specified point. Usefull for example for LOD distance of
object to camera.
\param pos
Position we are finding nearest position to
\return
the nearest position on box (or inside box, when source point
is inside too).
*/
	Vector3 GetNearestPosition(const Vector3& pos) const
	{
		//get nearest corner of AABB
		return Vector3((pos.x <= Mins.x) ? Mins.x : ((pos.x >= Maxs.x) ? Maxs.x : pos.x),
							(pos.y <= Mins.y) ? Mins.y : ((pos.y >= Maxs.y) ? Maxs.y : pos.y),
							(pos.z <= Mins.z) ? Mins.z : ((pos.z >= Maxs.z) ? Maxs.z : pos.z));
	}

/*!
Constructor creating AAB set to defined point
*/
	AAB(const Vector3& point)
	{
		Mins = point;
		Maxs = point;
	}

/*!
Constructor creating AAB with defined extents
*/
	AAB(const Vector3& mins, const Vector3& maxs)
	{
		Mins = mins;
		Maxs = maxs;
	}

	/*!
	Constructor creating AAB with defined extents
	*/
	AAB(float minx, float miny, float minz, float maxx, float maxy, float maxz)
	{
		Mins.x = minx; Mins.y = miny; Mins.z = minz;
		Maxs.x = maxx; Maxs.y = maxy; Maxs.z = maxz;
	}

/*!
Basic constructor.
\param init
When set to true, AAB is initialized with maximums
on numeric minimum and vice versa.
*/
	AAB(bool init = false)
	{
		if(init)
			Init();
	}
};

void __mathInit();

bool Colinear(const Vector3& a, const Vector3& b, const Vector3& c);

/*!
Sets inverse view matrix which corresponds with left-handed projection matrix.
TODO: figure out why must be Z axis flipped. Don't we need RH projection instead?
*/
Matrix44 SetupViewMatrix(const Matrix43& camera);

//-----------------------------------------------------------------------------------
/*!
Static class for linear equation solving 
*/
//-----------------------------------------------------------------------------------
template<class T> class EquationSolver
{
public:
	//-----------------------------------------------------------------------------------
	/*
	Make LUP decomposition of the matrix.
	\param M
	Which matrix to decompose.
	\param LU
	The result of decomposition.
	\param P
	The vector of pivots (each item is the position of row, where the original row was moved).
	\param N
	Size of square matrix (NxN).
	*/
	//-----------------------------------------------------------------------------------
	static bool LUPDecomposition(const T *M, T *LU, uint *P, const uint N);

	//-----------------------------------------------------------------------------------
	/*
	Solve the linear equation of N variables by LUP decomposition, Ax = b.
	\param A
	Matrix of variables.
	\param b
	Vector of right side.
	\param x
	The vector of unknown variables.
	\param N
	Size of square matrix (NxN).
	*/
	//-----------------------------------------------------------------------------------
	static bool Solve(const T *A, const T *b, T *x, const uint N);
};


#endif //ENF_MATHLIB_H

