#ifndef CSERIALIZE_H
#define CSERIALIZE_H

#include <stdio.h>
#include "common/enf_types.h"
#include "common/enf_mathlib.h"

class CSerialize
{
public:
	enum Mode {LITTLEENDIAN, BIGENDIAN};

	void SetMode(Mode mode)
	{
		m_Mode = mode;
	}

protected:
	Mode	m_Mode;
};

class CStream
{
protected:
	virtual bool IsOpen() const=0;
public:
};

class COutputStream
{
protected:
public:
	virtual bool IsOpen() const=0;
	virtual size_t Write(const void* data, size_t size)=0;
};

class CInputStream
{
protected:
public:
	virtual bool IsOpen() const=0;
	virtual size_t Read(void* data, size_t size)=0;
};

class CFileOutputStream: public COutputStream
{
protected:
	FILE*	m_pFile;

public:
	CFileOutputStream(): m_pFile(NULL)
	{
	}

	CFileOutputStream(const char* filename)
	{
		m_pFile = fopen(filename, "wb+");
	}

	size_t Write(const void* data, size_t size)
	{
		assert(data != NULL);
		assert(IsOpen());

		return fwrite(data, size, 1, m_pFile);
	}

	void Close()
	{
		if(m_pFile)
		{
			fclose(m_pFile);
			m_pFile = NULL;
		}
	}
	~CFileOutputStream()
	{
		Close();
	}
	bool IsOpen() const
	{
		return m_pFile != NULL;
	}
};

class CFileInputStream: public CInputStream
{
protected:
	FILE*	m_pFile;

public:
	CFileInputStream(): m_pFile(NULL)
	{
	}

	CFileInputStream(const char* filename)
	{
		m_pFile = fopen(filename, "rb+");
	}

	size_t Read(void* data, size_t size)
	{
		assert(data != NULL);
		assert(IsOpen());

		return fread(data, size, 1, m_pFile);
	}

	void Close()
	{
		if(m_pFile)
		{
			fclose(m_pFile);
			m_pFile = NULL;
		}
	}
	~CFileInputStream()
	{
		Close();
	}
	bool IsOpen() const
	{
		return m_pFile != NULL;
	}
};

template<class T> T SwapEndian(T val)
{
	union
	{
		T ret;
		ubyte bytes[sizeof(T)];
	};

	const ubyte* src = (const ubyte*)&val;
	for(uint n = 0; n < sizeof(T); n++)
	{
		bytes[n] = src[sizeof(T) - 1 - n];
	}
	return ret;
}

class CSerializeOutput: public CSerialize
{
protected:
	COutputStream*	m_pOStream;

	bool IsOpen() const
	{
		return m_pOStream != NULL && m_pOStream->IsOpen();
	}

public:
	CSerializeOutput(COutputStream* outstream): m_pOStream(outstream)
	{
		assert(outstream);
	}
	void WriteFloat(float value)
	{
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);

		m_pOStream->Write(&value, sizeof(value));
	}
	void WriteUInt32(uint value)
	{
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);

		m_pOStream->Write(&value, sizeof(value));
	}
	void WriteInt32(int value)
	{
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);

		m_pOStream->Write(&value, sizeof(value));
	}

	void WriteUInt16(unsigned short value)
	{
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);

		m_pOStream->Write(&value, sizeof(value));
	}
	void WriteInt16(short value)
	{
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);

		m_pOStream->Write(&value, sizeof(value));
	}
	void WriteUInt8(unsigned char value)
	{
		m_pOStream->Write(&value, sizeof(value));
	}
	void WriteInt8(char value)
	{
		m_pOStream->Write(&value, sizeof(value));
	}
	void Write(const void* data, size_t size)
	{
		m_pOStream->Write(data, size);
	}
};

class CSerializeInput: public CSerialize
{
protected:
	CInputStream*	m_pIStream;

	bool IsOpen() const
	{
		return m_pIStream != NULL && m_pIStream->IsOpen();
	}

public:
	CSerializeInput(CInputStream* instream): m_pIStream(instream)
	{
		assert(instream);
	}
	float ReadFloat()
	{
		float value;

		m_pIStream->Read(&value, sizeof(value));
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);
		return value;
	}

	uint ReadUInt32()
	{
		uint value;
		m_pIStream->Read(&value, sizeof(value));
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);
		return value;
	}
	int ReadInt32()
	{
		int value;
		m_pIStream->Read(&value, sizeof(value));
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);
		return value;
	}

	unsigned short ReadUInt16()
	{
		unsigned short value;

		m_pIStream->Read(&value, sizeof(value));
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);
		return value;
	}
	short ReadInt16()
	{
		short value;

		m_pIStream->Read(&value, sizeof(value));
		if(m_Mode == BIGENDIAN)
			value = SwapEndian(value);
		return value;
	}
	ubyte ReadUInt8()
	{
		ubyte value;
		m_pIStream->Read(&value, sizeof(value));
		return value;
	}
	char ReadInt8()
	{
		char value;
		m_pIStream->Read(&value, sizeof(value));
		return value;
	}
	void Read(void* data, size_t size)
	{
		m_pIStream->Read(data, size);
	}
};

#endif //CSERIALIZE_H