#ifndef CANIMATION_H
#define CANIMATION_H

#include "common/enf_types.h"
#include "common/enf_mathlib.h"

/*!
CAnimation hierarchy node.
*/
class CNode
{
protected:
	friend class CAnimation;

	CNode*		m_pParent;
	CNode*		m_pSibling;
	CNode*		m_pChildren;
	DString		m_Name;
	uint			m_iIndex;

public:
	CNode& operator=(const CNode& node)
	{
		m_pParent = node.m_pParent;
		m_pSibling = node.m_pSibling;
		m_pChildren = node.m_pChildren;
		m_Name = node.m_Name;
		m_iIndex = node.m_iIndex;
		return *this;
	}

	const CNode* GetParent() const
	{
		return m_pParent;
	}

	const CNode* GetChildren() const
	{
		return m_pChildren;
	}

	const CNode* GetSibling() const
	{
		return m_pSibling;
	}

	const char* GetName() const
	{
		return m_Name.c_str();
	}

	CNode(): m_pParent(NULL), m_pSibling(NULL), m_pChildren(NULL), m_iIndex(-1)
	{
	}

	CNode(const char* name): m_pParent(NULL), m_pSibling(NULL), m_pChildren(NULL), m_Name(name), m_iIndex(-1)
	{
	}
};

class CLogger;

//!Min/Max value range for normalize/quantization
struct SRange
{
	float Min, Max;

	void Init()
	{
		Min = FLT_MAX;
		Max = FLT_MIN;
	}

	SRange(): Min(FLT_MAX), Max(FLT_MIN)
	{
	}
};

/*!
Intermediate container of animation data, where all the fun happen.
*/
class CAnimation
{
protected:
	CLogger*	m_pLogger;

	struct SNodeHead
	{
		//uncompressed data
		//ATM we will store only global frame matrices, as RTM is stored this way too. Local ones can be easily restored
		EArray<Matrix43>	m_Keyframes;
		CNode*				m_pNode;

		//compressed data
		struct SKey
		{
			uint			k;
		};

		struct QKey: public SKey
		{
			Quaternion	q;
		};

		struct VKey: public SKey
		{
			Vector3		v;
		};

		EArray<QKey>	m_QuatKeys;
		SRange			m_QRange;
		EArray<VKey>	m_ScaleKeys;
		SRange			m_SRange;
		EArray<VKey>	m_TransKeys;
		SRange			m_TRange;

		SNodeHead(): m_pNode(NULL)
		{
		}

		~SNodeHead()
		{
		}
	};

	EArray<SNodeHead>	m_NodeHeads;

	void r_SetLocalFrames(uint node, const Matrix43& frame);

public:
	~CAnimation()
	{
		ClearAll();
	}
	CAnimation(CLogger* logger): m_pLogger(logger)
	{
	}

	const CNode* GetRootNode() const
	{
		if(m_NodeHeads.IsEmpty()) return NULL;

		return m_NodeHeads[0].m_pNode;
	}

	void Compress(float teps, float reps, float seps);

	//input methods
	void ClearAll();
	uint AppendNode(const char* name, uint parent);
	void Clear();
	uint AppendKeyframeGlobal(const Matrix43* nodes);
	uint AppendKeyframeGlobal(const Matrix43& node, uint nodenum);
	uint AppendKeyframeLocal(const Matrix43* nodes);

	//output methods
	uint GetNumNodes() const;
	uint GetNodeIndex(const char* name) const;
	const char* GetNodeName(uint node) const;
	uint GetNumFrames() const;
	uint GetNumCompressedFrames(uint node) const;
	bool IsNodeKeyframe(uint frame, uint node) const;
	bool IsKeyframe(uint frame) const;

	uint GetFrameFromCompressedFrame(uint cframe, uint node) const;
	/*
	Quaternion GetNodeR(uint frame, uint node) const;
	Vector3 GetNodeT(uint frame, uint node) const;
	Vector3 GetNodeS(uint frame, uint node) const;
	*/
	Matrix43 GetNodeLocalM(uint frame, uint node) const;
	Matrix43 GetNodeGlobalM(uint frame, uint node) const;
};

struct SExportParms
{
	float		m_fScaleX, m_fScaleY, m_fScaleZ;
	float		m_fTDelta, m_fRDelta;
	float		m_fFPS;
	DString	m_strCenterBone;
	bool		m_bNeutralizeMovement;

	SExportParms(): m_fScaleX(1.0f), m_fScaleY(1.0f), m_fScaleZ(1.0f), m_fTDelta(0.0f), m_fRDelta(0.0f), m_fFPS(0.0f), m_bNeutralizeMovement(false)
	{
	}
};

class CAnimExporter
{
protected:
	CLogger*	m_pLogger;

public:
	~CAnimExporter() {}
	CAnimExporter(CLogger* logger): m_pLogger(logger)
	{
	}
	bool Export(const char* name, const CAnimation* anim, const SExportParms* parms);
};


#endif //CANIMATION_H