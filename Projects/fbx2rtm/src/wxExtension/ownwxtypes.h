#ifndef OWN_GUI_TYPES_H
#define OWN_GUI_TYPES_H

#include "ControlsGlobal.h"

#include<string>
#include "wx/splitter.h"
#include "wx/button.h"
#include "wx/textctrl.h"
#include "wx/combobox.h"
#include "wx/panel.h"
#include "wx/dialog.h"
#include "wx/checklst.h"
#include "wx/gauge.h"
#include "wx/scrolwin.h"
#include "wx/statbox.h"
#include "common/enf_types.h"

class wxTextCtrl;
class wxTreeCtrl;
class wxTreeEvent;
class wxTreeItemId;
class wxTreeItemData;
class wxStaticText;
class wxListBox;
class wxSizerItem;
class wxStaticBoxSizer;
class UniversalBaseDialog;
class RichFileDialog;

DEFINE_EVENT_TYPE(wxEVT_PROPERTY_CHANGE)		//novy event ktory nastava ked niektory control zmeni svoju hodnotu

BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_PROPERTY_CHANGE, 7778)
END_DECLARE_EVENT_TYPES()

#define EVT_PROPERTY_CHANGE(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY( \
        wxEVT_PROPERTY_CHANGE, id, -1, \
        (wxObjectEventFunction)(wxEventFunction)(wxCommandEventFunction)&fn, \
        (wxObject *) NULL \
    ),
 

//====================================================================================
class RichButton: public wxButton
{
public:
	RichButton(wxWindow* parent, wxWindowID id, const wxString& label = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = "button");
	inline void* GetClientData(){return Data;};
	inline void	SetClientData(void* data){Data = data;};
protected:
	void* Data;
};

//---------------------------------------------------------------------------------------------
class RichSplitter : public wxSplitterWindow
{
public:
	RichSplitter(wxWindow* parent, wxWindowID id, int SashPos, bool FromRightSide = false, const wxPoint& point = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style=wxSP_3D);
	int GetSashPosForSplit();
	int GetSashPosFromSide();
private:
	wxWindow* Parent;
	int SashPosition;
	int NeededSashPos;
	bool FromRight;
	void OnSize(wxSizeEvent& event);
	void OnSplitterPosChanged(wxSplitterEvent& event);
	DECLARE_EVENT_TABLE()
};

//====================================================================================
//==== edit control with label and managed input =====================================
//====================================================================================
class LabeledTextCtrl: public wxTextCtrl
{
public:
	wxStaticText *Label;
	wxWindow *Parent;
	LabeledTextCtrl(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, int style, wxPoint& LabelOffset, wxString LabelText, const int InpType = VarType_String, wxString MinValue = "", wxString MaxValue = "");
	virtual bool Show(bool show = true);
	void SetLabelText(wxString label);
private:
	int InputType;
	wxPoint LblOffset;
	wxString MinVal, MaxVal;
	void OnMove(wxMoveEvent &event);
//	void OnTextChanging(wxCommandEvent &event);
	void OnTextEnter(wxCommandEvent &event);
	void OnLostFocus(wxFocusEvent& event);
	DECLARE_EVENT_TABLE()
};

//====================================================================================
//==== ComboBox with managed input ===================================================
//====================================================================================
class RichComboBox: public wxComboBox
{
public:
	inline void SetClientDataUsability(bool UsingClientData){this->UsingClientData = UsingClientData;};
	RichComboBox(wxWindow* parent, wxWindowID id, const wxString& value = wxT(""), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0, const int InpType = VarType_String, wxString MinValue = "", wxString MaxValue = "", bool ForceNicerBorder = true);
	RichComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style = 0, const int InpType = VarType_String, wxString MinValue = "", wxString MaxValue = "", const wxValidator& validator = wxDefaultValidator, const wxString& name = "comboBox", bool ForceNicerBorder = true);
	virtual ~RichComboBox();
private:
	bool UsingClientData;
	bool ForceNicerBorder;
	int InputType;
	wxSize InitSize;
	wxString MinVal, MaxVal;
	uint ClickTime;
//	bool LMB;
//	void OnSetFocus(wxFocusEvent& event);
	void OnLostFocus(wxFocusEvent& event);
	void OnLMBdown(wxMouseEvent& event);
//	void OnTextChanging(wxCommandEvent &event);
	void OnPaint(wxPaintEvent& event);
	DECLARE_EVENT_TABLE()
};

class PropEditComboBox: public RichComboBox
{
public:
	PropEditComboBox(wxWindow* parent, wxWindowID id, const wxString& value = wxT(""), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0, const int InpType = VarType_String, wxString MinValue = "", wxString MaxValue = "");
	~PropEditComboBox();
};

//====================================================================================
//==== edit control with filerequester ===============================================
//====================================================================================

//=========== custom event pre TextboxWithButton START ===============================
BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_FILETEXTBOX, 7777)
END_DECLARE_EVENT_TYPES()

// it may also be convenient to define an event table macro for this event type
#define EVT_FILETEXTBOX(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY( \
        wxEVT_FILETEXTBOX, id, -1, \
        (wxObjectEventFunction)(wxEventFunction)(wxCommandEventFunction)&fn, \
        (wxObject *) NULL \
    ),
//=========== custom event pre TextboxWithButton END ===============================

//====================================================================================
class TextboxWithButton: public wxPanel
{
public:
	wxWindow *Parent;
	wxTextCtrl* TextBox;
	RichComboBox* Combo;
	wxButton* button;
	wxWindow* GetEditControl();
	virtual wxString GetValue();
	virtual void SetValue(wxString value);
	bool IsAnyChoice(wxString value);
	TextboxWithButton(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, int TextBoxStyle, char** ButtonXPMBitmap);
	TextboxWithButton(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, wxArrayString& choices, int ComboBoxStyle, char** ButtonXPMBitmap);
	~TextboxWithButton();
private:
	wxBitmap* ButtonBitmap;
	virtual void OnButtonClick(wxCommandEvent &event);
	virtual void OnTextEnter(wxCommandEvent &event);
	virtual void OnSize(wxSizeEvent &event);
	DECLARE_EVENT_TABLE()
};

//====================================================================================
//base class for dialogs that can return string value
//====================================================================================
class UniversalBaseDialog : public wxDialog
{
public:
	UniversalBaseDialog(wxFrame* parent, wxWindowID id, wxString title, wxPoint pos, wxSize& size, long style = 0);
	virtual wxString GetResultValue();				//kazdy dialog bude mat stringovy vstup a vystup
	virtual void SetInputValue(wxString value);
	virtual int ShowAsModal();
protected:
	wxString InputValue;
	int ModalReturnCode;
	virtual void OnButtonClick(wxCommandEvent &event);
	DECLARE_EVENT_TABLE()
};

//====================================================================================
class RichSingleChoiceDialog : public UniversalBaseDialog
{
public:
	RichSingleChoiceDialog(wxFrame* parent, wxWindowID id, wxString title, wxArrayString& choices, wxPoint pos, wxSize size, long style = 0);
	virtual void SetInputValue(wxString value);
	virtual wxString GetResultValue();
	void SetSelection(int index);
	int GetSelection();
	void Clear();
private:
	wxListBox* ChoicesListBox;
	wxButton* OkButton;
	wxButton* CancelButton;
};

const int MCH_ADDBUTTON = 1;
const int MCH_REMOVEBUTTON = 2;

//====================================================================================
class MultiChoiceDialog : public UniversalBaseDialog
{
public:
	wxTreeCtrl* ChoicesThree;
	MultiChoiceDialog(wxFrame* parent, wxWindowID id, wxString title, wxPoint pos, wxSize& size, long style = 0);
	~MultiChoiceDialog();
	void AddChoice(wxString& category, wxString& choice);
	virtual void SetInputValue(wxString value); 
	virtual wxString GetResultValue();
	virtual int ShowAsModal();
	void SetSelections(wxString& CodedSelections);
	void UpdateButtonsAccesibility();
	void SelectUserChoicesInTree();
	void SortChoices();
	void Clear();
	void SetCloneTreeView(wxTreeCtrl* tree);
protected:
	wxTreeCtrl* CloneTree;
	wxListBox* ChoicesListBox;
	wxButton* AddButton;
	wxButton* RemoveButton;
	wxButton* OkButton;
	wxButton* CancelButton;
	wxFont* Font1;
	void OnSize(wxSizeEvent &event);
	virtual void OnButtonClick(wxCommandEvent &event);
	void OnTreeSelectionChanged(wxTreeEvent& event);
	void OnListboxSelectionChanged(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

//====================================================================================
class DialogCombo: public TextboxWithButton
{
public:
	UniversalBaseDialog* dialog;
	DialogCombo(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, char** ButtonXPMBitmap);
private:
	void OnButtonClick(wxCommandEvent& event);
//	void OnTextEnter(wxCommandEvent &event);
};


//====================================================================================
//==== flags edit ====================================================================
//====================================================================================
class FlagsDialog;
class FlagsEditBox;

class FlagsCheckListBox: public wxCheckListBox
{
public:
	FlagsCheckListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0);
private:
	FlagsDialog* Parent;
	void OnKeyDown(wxKeyEvent& event);
	DECLARE_EVENT_TABLE()
};

//====================================================================================
class FlagsDialog: public wxDialog
{
public:
	FlagsCheckListBox* FlagsCheckList;
	int ModalReturnCode;
	int Bits[32];
	void Close();
	FlagsDialog(wxWindow* parent, wxWindow* DialogFrameParent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
private:
	int Value;
	FlagsEditBox* Parent;
	void OnButtonClick(wxCommandEvent &event);
	DECLARE_EVENT_TABLE()
};

//====================================================================================
class FlagsEditBox: public TextboxWithButton
{
public:
	FlagsDialog* FlagsDlg;
	FlagsEditBox(wxWindow *parent, wxWindow* DialogFrameParent, wxWindowID id, wxPoint pos, wxSize& size);
private:
	void OnButtonClick(wxCommandEvent &event);
};

//====================================================================================
class EventEditButton: public wxButton
{
public:
	wxString KeyValue;
	EventEditButton(wxWindow *parent, wxWindowID id, wxString label = wxT(""), wxPoint pos = wxDefaultPosition, wxSize size = wxDefaultSize);
private:
};

//====================================================================================
class ProgressDialog: public wxDialog
{
public:
	ProgressDialog(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE, long BarStyle = wxGA_HORIZONTAL, int range = 1000);
	void SetProgress(int pos);	//pozicia v rangi
	void SetProgressText(wxString text);
private:
	wxGauge* ProgessBar;
	wxStaticText* ProgressText;
//	DECLARE_EVENT_TABLE()
};

class RichErrorDialog : public wxDialog
{
public:
	wxTextCtrl* TextWindow;
	wxButton* OkButton;
	int ModalRetCode;
	RichErrorDialog(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
private:
	void OnButtonClick(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

class ColorEdit : public TextboxWithButton
{
public:
	void SetCurrentColor(wxColour& color);
	ColorEdit(wxWindow *parent, wxWindow* DialogFrameParent, wxWindowID id, wxPoint pos, wxSize& size);
private:
	wxWindow* ParentForDialog;
	void OnButtonClick(wxCommandEvent& event);
};





//====================================================================================
//==== event edit =================================================
//====================================================================================

const int SashIDoffset = 2000;
class wxSashLayoutWindow;
class wxSashEvent;

enum
{
	EVENTEDIT_TEXTBOX0 = 1000,
	EVENTEDIT_TEXTBOX1,
	EVENTEDIT_TEXTBOX2,
	EVENTEDIT_TEXTBOX3,
	EVENTEDIT_TEXTBOX4,
	EVENTEDIT_TEXTBOX5,
	EVENTEDIT_TEXTBOX6,
	EVENTEDIT_TEXTBOX7
};

class EventEditDialog : public wxDialog
{
public:
	void EnableEdit(bool enable);
	EventEditDialog (wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE, wxString states = wxT(""), wxString* EventLine = NULL);
	~EventEditDialog();
	void WriteResult();
private:
	wxString* OutputStr;
	std::string AllStates[16];
	wxFont* font;
	wxPanel* MainPanel;
	wxSashLayoutWindow* SashWindows[18];
	wxTextCtrl* EditBoxs[18];
	wxStaticText* Labels[18];
	int NumStates;
	int NumEditBoxs;
	wxButton* OKButton;
	wxButton* CancelButton;
	void OnButtonPress(wxCommandEvent &event);
	void OnSize(wxSizeEvent &event);
	void OnSashDrag(wxSashEvent& event);
	void OnTextChanged(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};


//====================================================================================
//==== flexible multicomponent edit =================================================
//====================================================================================

//==========================================================================================
class PEditTextCtrl : public LabeledTextCtrl
{
public:
	PEditTextCtrl(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, wxPoint& LabelOffset, wxString& LabelText, const int InpType = VarType_String, wxString MinValue = "", wxString MaxValue = "");
private:
	 void OnLostFocus(wxFocusEvent& event); 
	 DECLARE_EVENT_TABLE()
};

const int PropLabelIDOffset = 1000;
const int PropControlIDOffset = 1100;
const int DefLabelWidth = 96;
const int DefLabelHeight = 16;
const int DefControlWidth = -1;
const int DefControlHeight = 16;
#define MaxPropLines 64

enum
{
	CtrlType_None = 0,
	CtrlType_EditBox,
	CtrlType_ComboBox,
	CtrlType_CheckBox,
	CtrlType_Slider,
	CtrlType_IncrementBox,
	CtrlType_FileRequester,
	CtrlType_FlagsEdit,
	CtrlType_EventButton,
	CtrlType_DialogCombo,
	CtrlType_ColorEdit
};

//==========================================================================================
class PropEditLine
{
public:
	wxString Key;
	wxString Value;
	int VarType;
	int ControlType;
	wxString* Choices;
	wxString* ChoicesParms;
	int NumChoices;
	wxString HelpText;
	wxString MinValue;
	wxString MaxValue;
	wxWindow* ExternControl;
	char** ExternBitmap;
	PropEditLine();
};

//==========================================================================================
class PropertiesEdit: public wxScrolledWindow
{
public:
	wxFrame* PrtForDialogs;		//parent pre dialogy
	wxArrayString States;
	wxStaticText* Labels[MaxPropLines];
	wxWindow* Controls[MaxPropLines];
	wxString Keys[MaxPropLines];
	wxString Values[MaxPropLines];
	int CtrlTypes[MaxPropLines];
	int VarTypes[MaxPropLines];
	wxString MinValues[MaxPropLines];
	wxString MaxValues[MaxPropLines];
	int LinesNum;
	void BeginSetup();
	void AddPropLine(PropEditLine& line);
	void EndSetup();
	void ChangeValue(wxString key, wxString val);
	void EnableLines(bool enable);
	int GetLineIndexFromID(int id);						//vrati lajnu na ktorej sa elemrnt nachadza
	bool CanKillFocus();
	PropertiesEdit(wxWindow *parent, wxFrame* ParentForDialogs, wxWindowID id, wxPoint pos, wxSize& size);
private:
	bool LinesEnabled;
	wxSize InitSize;
	
	void OnButtonPress(wxCommandEvent& event);
	void OnValueChanged(wxCommandEvent& event);		//custom event. bola zmenena hodnota v danej komponente
	void OnTextSet(wxCommandEvent& event);				//bolo nieco zadane do textoveho pola alebo do comba
	void OnSetFromCombo(wxCommandEvent& event);		//bolo nieco vybrane z listboxu comba
	void OnCheckboxSwitch(wxCommandEvent& event);	//bol prepnuty checkbox
	void OnSliderSwitch(wxScrollEvent& event);		//bol posunuty slider na inu poziciu
	void OnLostFocus(wxFocusEvent& event);				//lost focus ktorehokolvek prvku
	void OnSize(wxSizeEvent &event);
	DECLARE_EVENT_TABLE()
};

class DropdownContainer;

//==========================================================================================
class DropdownButton : public wxButton
{
public:
	DropdownButton(wxWindow* parent, DropdownContainer* container, wxWindowID id, const wxString& label = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = "button");
private:
	DropdownContainer* Container;
	void OnButtonPress(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

//==========================================================================================
struct DContainerItem
{
	wxObject* item;
	wxSizerItem* SizerItem;
	int flags;
	int border;
};

//==========================================================================================
class DropdownContainer : public wxStaticBox
{
public:
	DropdownContainer* NextContainer;
	wxStaticBoxSizer* sizer;
	DropdownContainer(wxWindow* parent, wxWindowID id, const wxString& label, bool StartExpanded = true, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0);
	~DropdownContainer();
	void AddItem(wxWindow* item, int flags, int border);
	void AddItem(wxSizer* item, int flags, int border);
	wxSizer* GetSizer();
	void UpdateItemSize(wxWindow* Item, wxSize& size);
	void UpdateButtonPos();
	void SetNextContainer(DropdownContainer* container);
	void OnDropButtonClick();
protected:
	wxButton* button;
	wxList items;
	bool expanded;
	wxWindow* Parent;
	wxSizerItem* FirstSpacer;
	void OnSize(wxSizeEvent &event);
	DECLARE_EVENT_TABLE()
};

class DescriptionDialog : public wxDialog
{
public:
	wxButton* OkButton;
	wxTextCtrl* TextArea;
	DescriptionDialog(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE, wxString title = "title", wxString DescText = "", wxFont* TextFont = NULL);
private:
	void OnButtonClick(wxCommandEvent& event);
	void OnKeyDown(wxKeyEvent& event);
	DECLARE_EVENT_TABLE()
};

//=============================================================================================
class ColorEditPanelPreviewBox : public wxPanel
{
public:
	ColorEditPanelPreviewBox(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL, const wxString& name = "panel");
	void SetColor(unsigned char red, unsigned char green, unsigned char blue);
private:
	unsigned char ColorR;
	unsigned char ColorG;
	unsigned char ColorB;
	void OnPaint(wxPaintEvent& event);
	DECLARE_EVENT_TABLE()
};

//=============================================================================================
class ColorEditPanelIntensityBox : public wxPanel
{
public:
	void SetPointer(int y);
	void UpdateCurrentColor();
	ColorEditPanelIntensityBox(wxWindow* parent, ColorEditPanelPreviewBox* PreviewBox, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL, const wxString& name = "panel");
	~ColorEditPanelIntensityBox();
private:
	unsigned char ColorR;
	unsigned char ColorG;
	unsigned char ColorB;
	int PointerY;
	wxImage* image;
	wxRect ScreenClipRect;
	ColorEditPanelPreviewBox* PreviewBox;
	void ClipCursorInside(bool OnOff);
	void OnPaint(wxPaintEvent& event);
	void OnMouseMove(wxMouseEvent& event);
	void OnLMBdown(wxMouseEvent& event);
	void OnLMBup(wxMouseEvent& event);
	DECLARE_EVENT_TABLE()
};

//=============================================================================================
class ColorEditPanelColorBox : public wxPanel
{
public:
	void SetPointer(int x, int y);
	ColorEditPanelColorBox(wxWindow* parent, ColorEditPanelIntensityBox* IntensityBox, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL, const wxString& name = "panel");
	~ColorEditPanelColorBox();
private:
	void GenerateSpectrumBitmap(int width, int height);
	wxImage* image;
	ColorEditPanelIntensityBox* IntensityBox;
	int PointerX;
	int PointerY;
	wxRect ScreenClipRect;
	void ClipCursorInside(bool OnOff);
	void OnPaint(wxPaintEvent& event);
	void OnMouseMove(wxMouseEvent& event);
	void OnLMBdown(wxMouseEvent& event);
	void OnLMBup(wxMouseEvent& event);
	DECLARE_EVENT_TABLE()
};

enum
{
	TEXTCTRL_RED = 100,
	TEXTCTRL_GREEN,
	TEXTCTRL_BLUE,
	TEXTCTRL_ALPHA,
	TEXTCTRL_HUE,
	TEXTCTRL_LIGHTNESS,
	TEXTCTRL_SATURATION
};

//=============================================================================================
class ColorEditPanel : public wxPanel
{
public:
	void SetHue(double val);
	void SetLightness(double val);
	void SetSaturation(double val);
	void SetHueAndSaturation(double hue, double sat);
	inline double GetHue(){return hue;};
	inline double GetLightness(){return lightness;};
	inline double GetSaturation(){return saturation;};

	void SetColor(double Hue, double Lightness, double Saturation);
	void SetColor(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha);
	void SetColor(unsigned char red, unsigned char green, unsigned char blue);
	wxString GetFRGBAColorAsString();
	virtual void OnColorChanged();
	virtual void OnColorChanging();
	ColorEditPanel(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL, const wxString& name = "panel");
	~ColorEditPanel();
private:
	double hue;
	double lightness;
	double saturation;
	ColorEditPanelColorBox* ColorBox;
	ColorEditPanelIntensityBox* IntensityBox;
	ColorEditPanelPreviewBox* PreviewBox;
	LabeledTextCtrl* RedEditBox;
	LabeledTextCtrl* GreenEditBox;
	LabeledTextCtrl* BlueEditBox;
	LabeledTextCtrl* AlphaEditBox;
	LabeledTextCtrl* HueEditBox;
	LabeledTextCtrl* LightnessEditBox;
	LabeledTextCtrl* SaturationEditBox;
	void UpdateCurrentColor();
	void OnTextEnter(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

//=============================================================================================
class SliderEdit : public wxPanel
{
public:
	bool IsAnyChoice(wxString value);
	virtual wxString SetValue(wxString value);
	SliderEdit(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, wxArrayString& choices, bool ComboBox, const int InpType = VarType_String, wxString value = "0", wxString MinValue = "", wxString MaxValue = "", wxString TickValue = "", long style = wxTAB_TRAVERSAL, const wxString& name = "panel");
	~SliderEdit();
protected:
	wxSlider* slider;
	RichComboBox* combo;
	LabeledTextCtrl* editbox;
	wxString MinValue;
	wxString MaxValue;
	wxString TickValue;
	int InpType;
	void UpdateFromTick();
	virtual void OnValueChanged(wxString value);
	virtual void OnValueChanging(wxString value);
	void OnSetFromCombo(wxCommandEvent& event);
	void OnTextEnter(wxCommandEvent& event);
	void OnSliderTick(wxScrollEvent& event);
	void OnSliderRelease(wxScrollEvent& event);
	DECLARE_EVENT_TABLE()
};

//=============================================================================================
class FlagsEditPanel : public wxPanel
{
public:
	wxString GetValue();
	FlagsEditPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, wxArrayString& flags, wxString value, long style = wxTAB_TRAVERSAL, const wxString& name = "panel");
private:
	int flags;
	virtual void OnValueChanged(wxString value);
	void OnCheckboxSwitch(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

#endif