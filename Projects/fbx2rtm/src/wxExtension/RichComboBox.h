#ifndef RICH_COMBOBOX_H
#define RICH_COMBOBOX_H

#include "ControlsGlobal.h"
#include "wx/combobox.h"
#include "wx/string.h"
#include "common/enf_types.h"

//====================================================================================
//==== ComboBox with managed input ===================================================
//====================================================================================
class RichComboBox: public wxComboBox
{
public:
	inline void SetClientDataUsability(bool UsingClientData){this->UsingClientData = UsingClientData;};
	RichComboBox(wxWindow* parent, wxWindowID id, const wxString& value = wxT(""), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0, const int InpType = VarType_String, wxString MinValue = "", wxString MaxValue = "", bool ForceNicerBorder = true);
	RichComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style = 0, const int InpType = VarType_String, wxString MinValue = "", wxString MaxValue = "", const wxValidator& validator = wxDefaultValidator, const wxString& name = "comboBox", bool ForceNicerBorder = true);
	virtual ~RichComboBox();
private:
	bool UsingClientData;
	bool ForceNicerBorder;
	int InputType;
	wxSize InitSize;
	wxString MinVal, MaxVal;
	uint ClickTime;
//	bool LMB;
//	void OnSetFocus(wxFocusEvent& event);
	void OnLostFocus(wxFocusEvent& event);
	void OnLMBdown(wxMouseEvent& event);
//	void OnTextChanging(wxCommandEvent &event);
	void OnPaint(wxPaintEvent& event);
	DECLARE_EVENT_TABLE()
};

#endif