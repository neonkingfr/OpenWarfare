#include "wbPrecomp.h"
#include "bitmaps/fileopen.xpm"
#include "bitmaps/help.xpm"
#include "bitmaps/flag.xpm"
#include "bitmaps/properties.xpm"
#include "bitmaps/sound.xpm"

#include "Common/NativeConvert.h"
#include "Common/ColorModelsConv.h"

#include "wxExtension/OwnWXTypes.h"
#include "wxExtension/FileTextBox.h"
#include "Common/WxConvert.h"

#include "Richedit.h"
#include "Common/NativeUtil.h"
#include "Common/TextFile.h"

//====================================================================================
//==== edit control with label and managed input =====================================
//====================================================================================

BEGIN_EVENT_TABLE(LabeledTextCtrl, wxTextCtrl)
    EVT_MOVE(LabeledTextCtrl::OnMove)
//	 EVT_TEXT(-1, LabeledTextCtrl::OnTextChanging)
	 EVT_TEXT_ENTER(-1, LabeledTextCtrl::OnTextEnter)
	 EVT_KILL_FOCUS(LabeledTextCtrl::OnLostFocus)
END_EVENT_TABLE()

//----------------------------------------------------------------------------------------
LabeledTextCtrl::LabeledTextCtrl(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, int style, wxPoint& LabelOffset, wxString LabelText, const int InpType, wxString MinValue, wxString MaxValue):wxTextCtrl(parent, id, wxString(wxT("")), pos, size, style|wxTE_PROCESS_ENTER)
{
	Parent = parent;
	Label = (wxStaticText*)NULL;
	InputType = InpType;
	MinVal = MinValue;
	MaxVal = MaxValue;

	if(!LabelText.IsEmpty())
	{
		LblOffset = LabelOffset;
		Label = new wxStaticText(parent, wxID_ANY, LabelText, wxPoint(pos + LabelOffset));
	}
}

//----------------------------------------------------------------------------------------
bool LabeledTextCtrl::Show(bool show)
{
	bool ret = wxTextCtrl::Show(show);

	if(Label)
		Label->Show(show);

	return ret;
}

//----------------------------------------------------------------------------------------
void LabeledTextCtrl::SetLabelText(wxString label)
{
	Label->SetLabel(label);
}

//----------------------------------------------------------------------------------------
void LabeledTextCtrl::OnMove(wxMoveEvent &event)
{
	if(Label)
		Label->Move(GetPosition() + LblOffset);

	event.Skip(true);
}
/*
//----------------------------------------------------------------------------------------
void LabeledTextCtrl::OnTextChanging(wxCommandEvent &event)
{
	wxString before = event.GetString();
	wxString after = CorrectizeStringFromInput(before, InputType);

	if(before != after)
	{
		SetValue(after);
		SetSelection(after.Length(), after.Length());
	}
	event.Skip();
}
*/
//----------------------------------------------------------------------------------------
void LabeledTextCtrl::OnTextEnter(wxCommandEvent &event)
{
	wxString before = event.GetString();
	wxString after;

	if(InputType == VarType_Int)
	{
		if(before.IsNumber())
			after = CorrectizeStringFromInput(before, InputType);
		else
		{
			if(MinVal.IsEmpty())
				after = "0";
			else
				after = MinVal;
		}

		int CurVal = AtoI(after);
		int MinVal = AtoI(this->MinVal);
		int MaxVal = AtoI(this->MaxVal);

		if(!this->MinVal.IsEmpty())
		{
			if(CurVal < MinVal)
			{
				CurVal = MinVal;
				after = ItoA(CurVal);
			}
		}

		if(!this->MinVal.IsEmpty())
		{
			if(CurVal > MaxVal)
			{
				CurVal = MaxVal;
				after = ItoA(CurVal);
			}
		}

		if(after.IsEmpty())
			after = "0";
	}

	if(InputType == VarType_Float)
	{
		if(IsFloatNumber(before.c_str()))
			after = CorrectizeStringFromInput(before, InputType);
		else
		{
			if(MinVal.IsEmpty())
				after = "0";
			else
				after = MinVal;
		}

		float CurVal = (float)atof(after.c_str());
		float MinVal = (float)atof(this->MinVal.c_str());
		float MaxVal = (float)atof(this->MaxVal.c_str());

		if(!this->MinVal.IsEmpty())
		{
			if(CurVal < MinVal)
			{
				CurVal = MinVal;
				after = FtoA(CurVal);
			}
		}

		if(!this->MaxVal.IsEmpty())
		{
			if(CurVal > MaxVal)
			{
				CurVal = MaxVal;
				after = FtoA(CurVal);
			}
		}

		if(after.IsEmpty())
			after = "0";
	}

	if(InputType == VarType_Int || InputType == VarType_Float)
	{
		if(before != after)
		{
			SetValue(after);
			SetSelection(after.Length(), after.Length());
		}
	}

	event.Skip();
}

//----------------------------------------------------------------------------------------
void LabeledTextCtrl::OnLostFocus(wxFocusEvent& event)
{
//	event.GetWindow();	//to co malo focus predtym alebo co ho dostane
/*	wxCommandEvent eventCustom(wxEVT_COMMAND);
	eventCustom.SetId(this->GetId());
	eventCustom.SetEventObject(this);
	wxPostEvent(this, eventCustom);*/

	//na lost focus
	long style = GetWindowStyle();

	if ( !(style & wxTE_READONLY) && (IsSingleLine() || (style & wxTE_PROCESS_ENTER)) )
	{
		wxCommandEvent MyEvent(wxEVT_COMMAND_TEXT_ENTER, GetId());
		InitCommandEvent(MyEvent);
		MyEvent.SetString(GetValue());
		GetEventHandler()->ProcessEvent(MyEvent);
	}

	event.Skip();
}

BEGIN_EVENT_TABLE(RichComboBox, wxComboBox)
//	EVT_SET_FOCUS(RichComboBox::OnSetFocus)
	EVT_KILL_FOCUS(RichComboBox::OnLostFocus)
	EVT_LEFT_DOWN(RichComboBox::OnLMBdown)
//	EVT_TEXT(-1, RichComboBox::OnTextChanging)
	EVT_PAINT(RichComboBox::OnPaint)
END_EVENT_TABLE()

//----------------------------------------------------------------------------------------
RichComboBox::RichComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style, const int InpType, wxString MinValue, wxString MaxValue, bool ForceNicerBorder) : wxComboBox(parent, id, value, pos, size, n, choices, style)
{
	this->ForceNicerBorder = ForceNicerBorder;
	InputType = InpType;
	MaxVal = MinValue;
	MaxVal = MaxValue;
	InitSize = size;
	ClickTime = 0;
//	LMB = false;
}

//----------------------------------------------------------------------------------------
RichComboBox::RichComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style, const int InpType, wxString MinValue, wxString MaxValue, const wxValidator& validator, const wxString& name, bool ForceNicerBorder)
	:wxComboBox(parent, id, value, pos, size, choices, style, validator, name)
{
	this->ForceNicerBorder = ForceNicerBorder;
	InputType = InpType;
	MaxVal = MinValue;
	MaxVal = MaxValue;
	InitSize = size;
	ClickTime = 0;
//	LMB = false;
}

//----------------------------------------------------------------------------------------
RichComboBox::~RichComboBox()
{
}

//----------------------------------------------------------------------------------------
void RichComboBox::OnLMBdown(wxMouseEvent& event)
{
	ClickTime = (uint)::timeGetTime();
	event.Skip();
}
/*
//----------------------------------------------------------------------------------------
void RichComboBox::OnSetFocus(wxFocusEvent& event)
{
	event.Skip();
}*/

//----------------------------------------------------------------------------------------
void RichComboBox::OnLostFocus(wxFocusEvent& event)
{
	long style = GetWindowStyle();

	if(!(style & wxCB_READONLY) /*&& (style & wxCB_DROPDOWN) /*&& (style & wxTE_PROCESS_ENTER)*/)
	{
		uint CurTime = (uint)::timeGetTime();
		uint diff = CurTime - ClickTime;

		if(diff > 100)	//pri rozkliknuti comba nastava lost focus takze tento event je v takom pripade neziaduci
		{
			wxCommandEvent MyEvent(wxEVT_COMMAND_TEXT_ENTER, m_windowId);
			const int sel = GetSelection();
			MyEvent.SetInt(sel);
			MyEvent.SetString(GetValue());
			InitCommandEventWithItems(MyEvent, sel);
			ProcessCommand(MyEvent);	//vola GetEventHandler()->ProcessEvent(MyEvent) a nic ineho v tejto verzii;
		}
	}

	event.Skip();
}
/*
//----------------------------------------------------------------------------------------
void RichComboBox::OnTextChanging(wxCommandEvent &event)
{
	wxString before = event.GetString();
	wxString after = CorrectizeStringFromInput(before, InputType);

	if(before != after)
	{
		SetValue(after);
		SetSelection(after.Length(), after.Length());
	}
	event.Skip();
}*/

//----------------------------------------------------------------------------------------
void RichComboBox::OnPaint(wxPaintEvent& event)
{
/*	if(ForceNicerBorder)
	{
		COMBOBOXINFO cbi;	//asi to neni prave spravne miesto ale jedine co zabera :(
		ZeroMemory(&cbi, sizeof(COMBOBOXINFO));
		cbi.cbSize = sizeof(COMBOBOXINFO);
		::GetComboBoxInfo((HWND)this->GetHWND(), &cbi);
		wxSize sz = GetClientSize();
		SendMessage(cbi.hwndCombo, CB_SETITEMHEIGHT, (WPARAM)-1, (LPARAM)InitSize.y - 4);	//nastavi skurvenu vysku komba ktora sa inac neda zmenit. -1 znamena combo. ine cislo je item po rozbaleni
		SetWindowPos(cbi.hwndItem, HWND_TOP, 1, 1, sz.x - 19, InitSize.y - 1, 0);						//vytiahneme editbox z comba do popredia a nastavime pos a size
	}*/
	event.Skip();
}

DEFINE_EVENT_TYPE(wxEVT_FILETEXTBOX)

BEGIN_EVENT_TABLE(TextboxWithButton, wxPanel)
	EVT_BUTTON(-1, TextboxWithButton::OnButtonClick)
	EVT_TEXT_ENTER(-1, TextboxWithButton::OnTextEnter)
	EVT_SIZE(TextboxWithButton::OnSize)
END_EVENT_TABLE()

//====================================================================================
//==== abstract class for other controls like FileTextBox ============================
//====================================================================================
TextboxWithButton::TextboxWithButton(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, int TextBoxStyle, char** ButtonXPMBitmap) : wxPanel(parent, id, pos, size)
{
	Parent = parent;
	this->SetMinSize(wxSize(60, size.y));

	Combo = NULL;
	TextBox = new wxTextCtrl(this, -1, wxT(""), wxPoint(0, 0), wxSize(size.x - size.y, size.y), TextBoxStyle);

	if(ButtonXPMBitmap)
	{
		ButtonBitmap = new wxBitmap( ButtonXPMBitmap );
		button = new wxBitmapButton(this, -1, *ButtonBitmap, wxPoint(size.x - 17, 0), wxSize(16, 16), wxBU_AUTODRAW, wxDefaultValidator, "");
	}
	else
	{
		ButtonBitmap = NULL;
		button = new wxButton(this, -1, wxEmptyString, wxPoint(size.x - size.y, 0), wxSize(size.y, size.y), 0);
	}
}

//----------------------------------------------------------------------------------------
TextboxWithButton::TextboxWithButton(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, wxArrayString& choices, int ComboBoxStyle, char** ButtonXPMBitmap) : wxPanel(parent, id, pos, size)
{
	Parent = parent;
	this->SetMinSize(wxSize(60, size.y));

	TextBox = NULL;
	Combo = new RichComboBox(this, -1, wxT(""), wxPoint(0, 0), wxSize(size.x - size.y, size.y), choices, ComboBoxStyle, VarType_String, "", "", wxDefaultValidator, "combobox", false);

	if(ButtonXPMBitmap)
	{
		ButtonBitmap = new wxBitmap( ButtonXPMBitmap );
		button = new wxBitmapButton(this, -1, *ButtonBitmap, wxPoint(size.x - 17, 0), wxSize(16, 16), wxBU_AUTODRAW, wxDefaultValidator, "");
	}
	else
	{
		ButtonBitmap = NULL;
		button = new wxButton(this, -1, wxEmptyString, wxPoint(size.x - size.y, 0), wxSize(size.y, size.y), 0);
	}
}

//----------------------------------------------------------------------------------------
TextboxWithButton::~TextboxWithButton()
{
	if(ButtonBitmap)
	{
		delete ButtonBitmap;
		ButtonBitmap = NULL;
	}
}

//----------------------------------------------------------------------------------------
wxString TextboxWithButton::GetValue()
{
	if(TextBox)
		return TextBox->GetValue();

	return Combo->GetValue();
}

//----------------------------------------------------------------------------------------
void TextboxWithButton::SetValue(wxString value)
{
	if(TextBox)
		TextBox->SetValue(value);
	else
		Combo->SetValue(value);
}

//----------------------------------------------------------------------------------------
wxWindow* TextboxWithButton::GetEditControl()
{
	if(TextBox)
		return TextBox;

	return Combo;
}

//----------------------------------------------------------------------------------------
bool TextboxWithButton::IsAnyChoice(wxString value)
{
	if(Combo && Combo->FindString(value) >= 0)
		return true;

	return false;
}

//----------------------------------------------------------------------------------------
void TextboxWithButton::OnSize(wxSizeEvent &event)
{
	wxSize size = GetClientSize();
	wxSize ButtonSize = button->GetSize();

	wxWindow* EditControl = GetEditControl();
	EditControl->SetSize(size.x - ButtonSize.y, ButtonSize.y);
	button->Move(size.x - ButtonSize.y, 0);

/*	TextBoxSizerItem->SetInitSize(size.x - 18, 16);
	TextBoxSizerItem->SetDimension(wxPoint(0, 0), wxSize(size.x - 18, 16));
	MainSizer->Layout();
	MainSizer->SetDimension(0, 0, size.x, size.y);
	MainSizer->RecalcSizes();
	MainSizer->Layout();*/
	event.Skip();
}

//----------------------------------------------------------------------------------------
void TextboxWithButton::OnButtonClick(wxCommandEvent& event)
{
}

//----------------------------------------------------------------------------------------
void TextboxWithButton::OnTextEnter(wxCommandEvent& event)
{
	wxCommandEvent eventCustom(wxEVT_FILETEXTBOX);
	eventCustom.SetId(this->GetId());
	eventCustom.SetEventObject(this);
	wxPostEvent(this, eventCustom);
//	event.Skip();
}

BEGIN_EVENT_TABLE(FlagsCheckListBox, wxCheckListBox)
	EVT_KEY_DOWN(FlagsCheckListBox::OnKeyDown)
END_EVENT_TABLE()

// ----------------------------------------------------------------------------
FlagsCheckListBox::FlagsCheckListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style) : wxCheckListBox(parent, id, pos, size, 0, choices, style)
{
	Parent = (FlagsDialog*)parent;
}

// ----------------------------------------------------------------------------
void FlagsCheckListBox::OnKeyDown(wxKeyEvent& event)
{
	int KeyCode = event.GetKeyCode();

   if(KeyCode == WXK_ESCAPE || KeyCode == WXK_RETURN)  //Esc
      Parent->Close();

	event.Skip();
}

BEGIN_EVENT_TABLE(FlagsDialog, wxDialog)
	EVT_BUTTON(-1, FlagsDialog::OnButtonClick)
END_EVENT_TABLE()

//----------------------------------------------------------------------------------------
void FlagsDialog::OnButtonClick(wxCommandEvent &event)
{
	Close();
	event.Skip();
}

//----------------------------------------------------------------------------------------
FlagsDialog::FlagsDialog(wxWindow* parent, wxWindow* DialogFrameParent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(DialogFrameParent, id, title, pos, size, style)
{
	Value = 0;
	Parent = (FlagsEditBox*)parent;
	FlagsCheckList = new FlagsCheckListBox(this, -1, wxDefaultPosition, size, 0, NULL, wxNO_BORDER);
	wxButton* CloseButton = new wxButton(this, wxID_ANY, _T("close"), wxDefaultPosition, wxSize(92, 20));
/*	FlagsCheckList->Append("flag 1");
	FlagsCheckList->Append("flag 2");
	FlagsCheckList->Append("flag 4");
	FlagsCheckList->Append("flag 8");
	FlagsCheckList->Append("flag 16");
	FlagsCheckList->Append("flag 32");*/

	wxBoxSizer* MainSizer = new wxBoxSizer(wxVERTICAL);
	MainSizer->Add(FlagsCheckList, 1, wxALIGN_LEFT | wxLEFT, 0);
	MainSizer->Add(CloseButton, 0, wxALIGN_LEFT | wxLEFT, 0);

	SetSizer(MainSizer);
	MainSizer->Fit(this);
//	MainSizer->RecalcSizes();
	MainSizer->Layout();
	SetAutoLayout(TRUE);
}

//----------------------------------------------------------------------------------------
void FlagsDialog::Close()
{
	int ItemsNum = FlagsCheckList->GetCount();
	int Result = 0;
	int current;

	for(int n = 0; n < ItemsNum; n++)
	{
		current = Bits[n];

		if(FlagsCheckList->IsChecked(n))
			Result |= current;
	}

	Parent->SetValue(ItoA(Result));

//	Parent->TextBox->SetFocus();
	EndModal(ModalReturnCode);
	Show(false);

	wxCommandEvent eventCustom(wxEVT_FILETEXTBOX);
	eventCustom.SetId(Parent->GetId());
	eventCustom.SetEventObject(this);
	wxPostEvent(Parent, eventCustom);
}

//====================================================================================
//==== flags edit ====================================================================
//====================================================================================

//----------------------------------------------------------------------------------------
FlagsEditBox::FlagsEditBox(wxWindow* parent, wxWindow* DialogFrameParent, wxWindowID id, wxPoint pos, wxSize& size) : TextboxWithButton(parent, id, pos, size, wxTE_READONLY | wxSTATIC_BORDER, flag_xpm)
{
	FlagsDlg = new FlagsDialog(this, DialogFrameParent, -1, _T("flags"), wxDefaultPosition, wxSize(size.x, 128), wxSYSTEM_MENU);
}

//----------------------------------------------------------------------------------------
void FlagsEditBox::OnButtonClick(wxCommandEvent &event)
{
	if(FlagsDlg->IsShown())
		FlagsDlg->Close();
	else
	{
		wxSize size = GetSize();
		wxPoint ButtonPos = wxPoint(0,size.y + 1);//button->GetPosition();
		ButtonPos = this->ClientToScreen(ButtonPos);

		int ItemsNum = FlagsDlg->FlagsCheckList->GetCount();
		int Flags = AtoI(this->GetValue());
		int CurFlag;

		for(int n = 0; n < ItemsNum; n++)
		{
			CurFlag = FlagsDlg->Bits[n];

			if(Flags & CurFlag)
				FlagsDlg->FlagsCheckList->Check(n, true);
			else
				FlagsDlg->FlagsCheckList->Check(n, false);
		}

		FlagsDlg->SetSize(ButtonPos.x, ButtonPos.y, size.x, -1);
		FlagsDlg->SetTitle(_T("flags"));
		FlagsDlg->Layout();
		FlagsDlg->FlagsCheckList->SetFocus();
//		FlagsDlg->ModalReturnCode = FlagsDlg->ShowModal();
		FlagsDlg->ShowModal();
	}
//	event.Skip();
}

//----------------------------------------------------------------------------------------
DialogCombo::DialogCombo(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, char** ButtonXPMBitmap) : TextboxWithButton(parent, id, pos, size, wxSTATIC_BORDER|wxTE_PROCESS_ENTER, ButtonXPMBitmap)
{
	dialog = NULL;
}

//----------------------------------------------------------------------------------------
void DialogCombo::OnButtonClick(wxCommandEvent& event)
{
	if(!dialog)
		return;

	dialog->SetInputValue(GetValue());
	dialog->Centre();

	if (dialog->ShowAsModal() == wxID_OK)
	{
		wxString res = dialog->GetResultValue();
		SetValue(dialog->GetResultValue());
		wxCommandEvent eventCustom(wxEVT_FILETEXTBOX);
		eventCustom.SetId(this->GetId());
		eventCustom.SetEventObject(this);
		wxPostEvent(this, eventCustom);
	}
}

//----------------------------------------------------------------------------------------
EventEditButton::EventEditButton(wxWindow *parent, wxWindowID id, wxString label, wxPoint pos, wxSize size):wxButton(parent, id, label, pos, size, wxSTATIC_BORDER)
{
}




//----------------------------------------------------------------------------------------
ProgressDialog::ProgressDialog(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, long BarStyle, int range):wxDialog(parent, id, wxT(""), pos, size, style)
{
	if(range <= 0)
		range = 1000;
	
	ProgressText = new wxStaticText(this, wxID_ANY, wxT("plase wait..."));
	ProgessBar = new wxGauge(this, wxID_ANY, range, wxDefaultPosition, wxSize(-1, 20), BarStyle);

	wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(ProgressText, 0, wxALIGN_LEFT|wxLEFT|wxRIGHT|wxTOP|wxBOTTOM, 3);
	sizer->Add(ProgessBar, 0, wxGROW|wxALIGN_LEFT|wxLEFT|wxRIGHT|wxTOP|wxBOTTOM, 3);

	SetSizer(sizer);
}

//----------------------------------------------------------------------------------------
void ProgressDialog::SetProgress(int pos)
{
	ProgessBar->SetValue(pos);
}

//----------------------------------------------------------------------------------------
void ProgressDialog::SetProgressText(wxString text)
{
	ProgressText->SetLabel(text);
}

BEGIN_EVENT_TABLE(RichErrorDialog, wxDialog)
	EVT_BUTTON(-1, RichErrorDialog::OnButtonClick)
END_EVENT_TABLE()

//----------------------------------------------------------------------------------------
RichErrorDialog::RichErrorDialog(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style):wxDialog(parent, id, wxT(""), pos, size, style)
{
	ModalRetCode = 0;
	TextWindow = new wxTextCtrl(this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(260, 200), wxTE_MULTILINE|wxTE_READONLY/*wxTE_RICH2|wxTE_DONTWRAP*/);
	OkButton = new wxButton(this, wxID_OK, _T("OK"), wxDefaultPosition, wxSize(80, 20));

	TextWindow->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_INACTIVEBORDER));

	wxBoxSizer* MainSizer = new wxBoxSizer(wxVERTICAL);
	MainSizer->Add(TextWindow, 0, wxALIGN_LEFT | wxLEFT, 0);
	MainSizer->Add(0, 10, 0, wxALIGN_CENTER, 0); // spacer
	MainSizer->Add(OkButton, 0, wxALIGN_CENTER | wxLEFT, 0);
	MainSizer->Add(0, 10, 0, wxALIGN_CENTER, 0); // spacer

	SetSizer(MainSizer);
	Layout();
	SetAutoLayout(TRUE);
	MainSizer->Fit(this);
	OkButton->SetFocus();
}

//----------------------------------------------------------------------------------------
void RichErrorDialog::OnButtonClick(wxCommandEvent &event)
{
	EndModal(ModalRetCode);
	event.Skip();
}



//----------------------------------------------------------------------------------------
ColorEdit::ColorEdit(wxWindow* parent, wxWindow* DialogFrameParent, wxWindowID id, wxPoint pos, wxSize& size) : TextboxWithButton(parent, id, pos, size, wxTE_READONLY|wxSTATIC_BORDER, NULL)
{
	ParentForDialog = DialogFrameParent;
}

//----------------------------------------------------------------------------------------
void ColorEdit::OnButtonClick(wxCommandEvent& event)
{
	wxColourData data;
	data.SetChooseFull(true);
	data.SetCustomColour(0, button->GetBackgroundColour());
	data.SetColour(button->GetBackgroundColour());
	wxColourDialog dialog(ParentForDialog, &data);
	dialog.Centre();

	if(dialog.ShowModal() == wxID_OK)
	{
		data = dialog.GetColourData();
		wxColour NewColor = data.GetColour();
		button->SetBackgroundColour(NewColor);
 
		float r = NewColor.Red();
		float g = NewColor.Green();
		float b = NewColor.Blue();

		wxString scol = FtoA(r / (float)255) + " " + FtoA(g / (float)255) + " " + FtoA(b / (float)255);
		SetValue(scol);
		wxCommandEvent eventCustom(wxEVT_FILETEXTBOX);
		eventCustom.SetId(this->GetId());
		eventCustom.SetEventObject(this);
		wxPostEvent(this, eventCustom);
	}
	event.Skip();
}

//----------------------------------------------------------------------------------------
void ColorEdit::SetCurrentColor(wxColour& color)
{
	button->SetBackgroundColour(color);
}







BEGIN_EVENT_TABLE(EventEditDialog, wxDialog)
	EVT_BUTTON(-1, EventEditDialog::OnButtonPress)
	EVT_SIZE(EventEditDialog::OnSize)
	EVT_SASH_DRAGGED_RANGE(-1, -1, EventEditDialog::OnSashDrag)
//	EVT_TEXT(-1, EventEditDialog::OnTextChanged)
END_EVENT_TABLE()

//----------------------------------------------------------------------------------------
void EventEditDialog::OnTextChanged(wxCommandEvent& event)
{
/*	int ID = event.GetId();

	wxTextCtrl* textbox = EditBoxs[ID - EVENTEDIT_TEXTBOX0];
	wxString ttt = GetValue();
	wxString sss = event.GetString();
	if(font)
	{
		wxTextAttr atr = wxTextAttr(wxColour(*wxBLACK), wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW), *font);
		EditBoxs[0]->SetDefaultStyle(atr);
	}*/
	event.Skip();
}

//----------------------------------------------------------------------------------------
void EventEditDialog::EnableEdit(bool enable)
{
	for(int n = 0; n < NumEditBoxs; n++)
		EditBoxs[n]->Enable(enable);

	OKButton->Enable(enable);
}

//----------------------------------------------------------------------------------------
EventEditDialog::EventEditDialog (wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style, wxString states, wxString* EventLine) : wxDialog(parent, id, title, pos, size, style)
{
	int n;
	OutputStr = EventLine;
	TextParser Parser;
	TxtLine line(&Parser);
	line.SetText(states.c_str());
	Parser.AddSplitSymbol(";", false);
	Parser.AddSplitSymbol(",", false);
	string Sline = line.GetText();
	NumStates = Parser.ParseLine(Sline, AllStates, NULL);

	NumEditBoxs = 0;

	if(NumStates == 0)
		NumEditBoxs = 1;
	else
		NumEditBoxs = NumStates + 2;

	wxString Texts[16];
	char TempChars[65536];
	wxString ToFind = ";";
	wxString ToReplace = wxString(char(13)) + wxString(char(10));
	string Tokens[16];
	int Spos, Spos2;

	for(n = 0; n < NumStates; n++)				//budeme hladat vyskyt ID pre jednotlive states tak si to pripravime
		Tokens[n] = "#" + wxString(AllStates[n].c_str()) + ":";

	Spos = EventLine->find_first_of('#', 0);	//kod "before" je vzdy po prvy znak # a ten mozeme kludne vrazit do prveho textboxu

	if(Spos != -1)
		Texts[0] = EventLine->SubString(0, Spos - 1);
	else
		Texts[0] = *EventLine;				//ked tam neni ziadny znak # tak pre ziadny state neni napisany kod. vsetko zostava v tomto "before"

	for(n = 0; n < NumStates; n++)		//vytiahneme kod pre jednotlive states a priradime textboxom ku ktorym patria
	{
		Spos = EventLine->Find(Tokens[n].c_str());

		if(Spos != -1)
		{
			Spos2 = EventLine->find_first_of('#', Spos + 1);	//od ID ktore sme nasli po dalsi znak # je nas kod pre tento state

			if(Spos2 != -1)
				Texts[n + 1] = EventLine->SubString(Spos + Tokens[n].length(), Spos2 - 1);			//bud po ten znak
			else
				Texts[n + 1] = EventLine->SubString(Spos + Tokens[n].length(), EventLine->length() - 1);	//alebo vsetko az do konca
		}
		else
			Texts[n + 1] = "";	//v EventLine neni ziadny kod pre tento state
	}

	if(NumEditBoxs >= 3)	//okrem states tam moze byt kod "after" a ten patri vzdy do posledneho editboxu
	{
		Spos = EventLine->find_last_of('#', EventLine->length());

		if(Spos != -1)
		{
			if((int)EventLine->length() > Spos + 3 && EventLine->SubString(Spos + 1, Spos + 3) == "end")
				Texts[NumEditBoxs - 1] = EventLine->SubString(Spos + 5, EventLine->length() - 1);
			else
				Texts[NumEditBoxs - 1] = "";
		}
	}

	for(n = 0; n < NumEditBoxs; n++)	//este musime zistit ci na zaciatku niesu zatvorky pretoze inac sa jedna o parametre funkcie
	{
		if(!Texts[n].IsEmpty() && Texts[n].First('(') == 0)
		{
			Spos = Texts[n].find_first_of(')', 1);

			if(Spos != -1)
				Texts[n].insert(Spos + 1, ToReplace);	//ked je na zaciatku nieco v zatvorkach tak to musime odriadkovat. vynimka
		}
	}

	wxString ToReplace2 = ";" + ToReplace;

	for(n = 0; n < NumEditBoxs; n++)	//vsetko co sme si pripravili ze priradime textboxom tak este vymenime znaky ";" za znaky konca riadka
	{
		strcpy(TempChars, Texts[n].c_str());
		ReplaceCharacters(TempChars, ToFind.c_str(), ToReplace2.c_str());
		Texts[n] = wxString(TempChars);
	}

	wxSize csize = GetClientSize();
//	csize.y = 600;
	int InitTextboxHeight = (csize.y - 60) / NumEditBoxs;
	wxSashLayoutWindow* win;
	RECT rc;
	font = new wxFont(8, wxFONTFAMILY_DEFAULT, wxNORMAL, wxNORMAL, false, _T("fixedsys"));
//	font->SetWeight(wxBOLD);

	MainPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(csize.x, csize.y - 60), wxTAB_TRAVERSAL | wxSTATIC_BORDER);

	win = new wxSashLayoutWindow(MainPanel, SashIDoffset, wxDefaultPosition, wxSize(200, 100), wxSW_BORDER | wxCLIP_CHILDREN);
	win->SetDefaultSize(wxSize(csize.x, InitTextboxHeight));
	win->SetOrientation(wxLAYOUT_HORIZONTAL);
	win->SetAlignment(wxLAYOUT_TOP);
	win->SetBackgroundColour(wxColour(255, 0, 0));
	win->SetSashVisible(wxSASH_BOTTOM, TRUE);
	win->SetMinimumSizeY(20);
	SashWindows[0] = win;
	EditBoxs[0] = new wxTextCtrl(win, EVENTEDIT_TEXTBOX0, wxT(""), wxDefaultPosition, wxSize(-1, -1), wxTE_MULTILINE|wxTE_RICH2|wxTE_DONTWRAP);

	SendMessage((HWND)EditBoxs[0]->GetHWND(), EM_GETRECT, 1, (LPARAM)&rc);
	rc.top += 16;
	rc.left++;
	SendMessage((HWND)EditBoxs[0]->GetHWND(), EM_SETRECT, 1, (LPARAM)&rc);
	wxString FirstLabel = "Always";

	SendMessage((HWND)EditBoxs[0]->GetHWND(), EM_SETTEXTMODE, (WPARAM)TM_PLAINTEXT, (LPARAM)0);	//vypneme formatovanie, farby a podobne veci

	if(NumStates > 0)
		FirstLabel = "Always Before";

	Labels[0] = new wxStaticText(EditBoxs[0], wxID_ANY, FirstLabel, wxPoint(0,0), wxSize(csize.x, 16));
	Labels[0]->SetBackgroundColour(wxColour(255, 127, 127));

	if(font)
	{
//		wxTextAttr atr = wxTextAttr(wxColour(*wxBLACK), wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW), *font);
//		EditBoxs[0]->SetDefaultStyle(atr);
		EditBoxs[0]->SetFont(*font);
	}

	EditBoxs[0]->SetValue(Texts[0]);
//----
	for(n = 0; n < NumStates; n++)
	{
		int StIndex = n + 1;
		win = new wxSashLayoutWindow(MainPanel, SashIDoffset + StIndex, wxDefaultPosition, wxSize(200, 100), wxSW_BORDER | wxCLIP_CHILDREN);
		win->SetDefaultSize(wxSize(csize.x, InitTextboxHeight));
		win->SetOrientation(wxLAYOUT_HORIZONTAL);
		win->SetAlignment(wxLAYOUT_TOP);
		win->SetBackgroundColour(wxColour(255, 127, 127));
		win->SetSashVisible(wxSASH_BOTTOM, TRUE);
		win->SetMinimumSizeY(20);
		SashWindows[StIndex] = win;
		EditBoxs[StIndex] = new wxTextCtrl(win, EVENTEDIT_TEXTBOX0 + StIndex, wxT(""), wxDefaultPosition, wxSize(-1, -1), wxTE_MULTILINE|wxTE_RICH2|wxTE_DONTWRAP);

		SendMessage((HWND)EditBoxs[StIndex]->GetHWND(), EM_GETRECT, 1, (LPARAM)&rc);
		rc.top += 16;
		rc.left++;
		SendMessage((HWND)EditBoxs[StIndex]->GetHWND(), EM_SETRECT, 1, (LPARAM)&rc);
		wxString Label = wxString(AllStates[n].c_str());//"state " + ItoA(n);
		SendMessage((HWND)EditBoxs[StIndex]->GetHWND(), EM_SETTEXTMODE, (WPARAM)TM_PLAINTEXT, (LPARAM)0);

		Labels[StIndex] = new wxStaticText(EditBoxs[StIndex], wxID_ANY, Label, wxPoint(0,0), wxSize(csize.x, 16));
		Labels[StIndex]->SetBackgroundColour(wxColour(255, 127, 127));

		if(font)
		{
//			wxTextAttr atr = wxTextAttr(wxColour(*wxBLACK), wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW), *font);
//			EditBoxs[StIndex]->SetDefaultStyle(atr);

			EditBoxs[StIndex]->SetFont(*font);
//			Labels[StIndex]->SetFont(*font);
//			Labels[StIndex]->SetForegroundColour(wxColour(*wxBLACK));
		}

		EditBoxs[StIndex]->SetValue(Texts[StIndex]);
	}

	if(NumStates > 0)
	{
		int LastIndex = n + 1;
		win = new wxSashLayoutWindow(MainPanel, SashIDoffset + LastIndex, wxDefaultPosition, wxSize(200, 100), wxSW_BORDER | wxCLIP_CHILDREN);
		win->SetDefaultSize(wxSize(csize.x, InitTextboxHeight));
		win->SetOrientation(wxLAYOUT_HORIZONTAL);
		win->SetAlignment(wxLAYOUT_TOP);
		win->SetBackgroundColour(wxColour(255, 0, 0));
		win->SetSashVisible(wxSASH_BOTTOM, TRUE);
		win->SetMinimumSizeY(20);
		SashWindows[LastIndex] = win;
		EditBoxs[LastIndex] = new wxTextCtrl(win, EVENTEDIT_TEXTBOX0 + LastIndex, wxT(""), wxDefaultPosition, wxSize(-1, -1), wxTE_MULTILINE|wxTE_RICH2|wxTE_DONTWRAP);

		SendMessage((HWND)EditBoxs[LastIndex]->GetHWND(), EM_GETRECT, 1, (LPARAM)&rc);
		rc.top += 16;
		rc.left++;
		SendMessage((HWND)EditBoxs[LastIndex]->GetHWND(), EM_SETRECT, 1, (LPARAM)&rc);
		wxString LastLabel = "Always After";
		SendMessage((HWND)EditBoxs[LastIndex]->GetHWND(), EM_SETTEXTMODE, (WPARAM)TM_PLAINTEXT, (LPARAM)0);

		Labels[LastIndex] = new wxStaticText(EditBoxs[LastIndex], wxID_ANY, LastLabel, wxPoint(0,0), wxSize(csize.x, 16));
		Labels[LastIndex]->SetBackgroundColour(wxColour(255, 127, 127));

		if(font)
		{
//			wxTextAttr atr = wxTextAttr(wxColour(*wxBLACK), wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW), *font);
//			EditBoxs[LastIndex]->SetDefaultStyle(atr);
			EditBoxs[LastIndex]->SetFont(*font);
		}
		EditBoxs[LastIndex]->SetValue(Texts[LastIndex]);
	}

	OKButton = new wxButton(this, wxID_OK, _T("OK"), wxPoint(10, csize.y - 50), wxSize(80, 40));
	CancelButton = new wxButton(this, wxID_CANCEL, _T("Cancel"), wxPoint(100, csize.y - 50), wxSize(80, 40));

/*	wxBoxSizer* ButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
	ButtonsSizer->Add(1, 0, 0, wxALIGN_LEFT | wxGROW, 0); // spacer
	ButtonsSizer->Add(OKButton, 0, wxALIGN_RIGHT | wxRIGHT, 5);
	ButtonsSizer->Add(CancelButton, 0, wxALIGN_RIGHT | wxRIGHT, 5);*/

/*	wxBoxSizer* MainPanelSizer = new wxBoxSizer(wxVERTICAL);
	MainPanelSizer->Add(MainPanel, 0, wxGROW | wxALIGN_TOP | wxRIGHT, 0);
	MainPanelSizer->Add(ButtonsSizer, 0, wxALIGN_TOP | wxALL, 5);
	this->SetSizer(MainPanelSizer);
	MainPanelSizer->SetMinSize(640, 480);
	MainPanelSizer->FitInside(this);
//	MainPanelSizer->Fit(this);
	MainPanelSizer->Layout();*/

   wxLayoutAlgorithm layout;
   layout.LayoutWindow(MainPanel);
}

//----------------------------------------------------------------------------------------
void EventEditDialog::WriteResult()
{
	wxString str;
	wxString result = "";
	wxString ToFind = wxString(char(13)) + wxString(char(10));
	wxString ToReplace = "";
	char TempChars[65536];
	bool AnyCode = false;

	for(int n = 0; n < NumEditBoxs; n++)
	{
		str = EditBoxs[n]->GetValue();
		strcpy(TempChars, str.c_str());
		ReplaceCharacters(TempChars, ToFind.c_str(), ToReplace.c_str());	//znaky konca riadka vyhodime
		str = wxString(TempChars);

		if(!str.IsEmpty())
			AnyCode = true;
/*		if(NumEditBoxs > 1 && str.length() > 0)	//ked sa tam nieco napise tak sa to musi ukoncit bodkociarkou
		{
			if(str.GetChar(str.length() - 1) != ';')
				str += ";";
		}*/

		if(NumEditBoxs >= 3)	//mame aspon jeden state
		{
			if(n > 0)
			{
				if(n < NumEditBoxs - 1)	//okna statov
				{
					if(!str.IsEmpty())
						str = "#" + wxString(AllStates[n - 1].c_str()) + ":" + str;
				}
				else
				{
					if(!str.IsEmpty())
						str = "#end:" + str;
				}
			}
		}

		result += str;
	}

	if(!AnyCode)
		result = "";

	*OutputStr = result;
}

//----------------------------------------------------------------------------------------
EventEditDialog::~EventEditDialog()
{
	if(font)
	{
		delete font;
		font = NULL;
	}
}
//----------------------------------------------------------------------------------------
void EventEditDialog::OnButtonPress(wxCommandEvent &event)
{
	int ID = event.GetId();

	if(ID == wxID_OK)
		WriteResult();

	event.Skip();
}

//----------------------------------------------------------------------------------------
void EventEditDialog::OnSize(wxSizeEvent &event)
{
	wxSize size = GetClientSize();

/*	for(int n = 0; n < NumEditBoxs; n++)
		SashWindows[n]->SetDefaultSize(wxSize(size.x, size.y / NumEditBoxs));*/

/*	MainPanel->SetSize(size);

    wxLayoutAlgorithm layout;
    layout.LayoutWindow(MainPanel);

	if(NumStates)	//su tam najmenej 3 editboxy a potrebuju prekreslit
	{
		for(int n = 0; n < NumEditBoxs; n++)
		{
			SashWindows[n]->SetDefaultSize(wxSize(size.x, 50));
			EditBoxs[n]->Refresh();
		}
	}
	event.Skip();*/
}

//----------------------------------------------------------------------------------------
void EventEditDialog::OnSashDrag(wxSashEvent& event)
{
    if (event.GetDragStatus() == wxSASH_STATUS_OUT_OF_RANGE)
        return;

	wxRect rect = event.GetDragRect();
	int ID = event.GetId();
	int WinIndex = ID - SashIDoffset;
	SashWindows[WinIndex]->SetDefaultSize(wxSize(event.GetDragRect().width, event.GetDragRect().height));

    wxLayoutAlgorithm layout;
    layout.LayoutWindow(MainPanel);

	if(NumStates)	//su tam najmenej 3 editboxy a potrebuju prekreslit
	{
		for(int n = 0; n < NumEditBoxs; n++)
			EditBoxs[n]->Refresh();
	}

    // Leaves bits of itself behind sometimes
//	 Refresh();
//	 Update();
//    GetClientWindow()->Refresh();
}


BEGIN_EVENT_TABLE(PEditTextCtrl, LabeledTextCtrl)
	EVT_KILL_FOCUS(PEditTextCtrl::OnLostFocus)
END_EVENT_TABLE()

//----------------------------------------------------------------------------------------
PEditTextCtrl::PEditTextCtrl(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, wxPoint& LabelOffset, wxString& LabelText, const int InpType, wxString MinValue, wxString MaxValue) : LabeledTextCtrl(parent, id, pos, size, wxSTATIC_BORDER, LabelOffset, LabelText, InpType, MinValue, MaxValue)
{
}

//----------------------------------------------------------------------------------------
void PEditTextCtrl::OnLostFocus(wxFocusEvent& event)
{
	PropertiesEdit* pe = (PropertiesEdit*)Parent;

	int ID = event.GetId();
	int Line = pe->GetLineIndexFromID(ID);
	wxString Key, Val;
	Key = pe->Labels[Line]->GetLabel();
	Val = this->GetValue();

	if(pe->VarTypes[Line] == VarType_Int)
	{
		int CurIntVal = AtoI(Val);
		int MinIntVal = AtoI(pe->MinValues[Line]);
		int MaxIntVal = AtoI(pe->MaxValues[Line]);

		if(!pe->MinValues[Line].IsEmpty() && CurIntVal < MinIntVal)
			Val = ItoA(MinIntVal);

		if(!pe->MaxValues[Line].IsEmpty() && CurIntVal > MaxIntVal)
			Val = ItoA(MaxIntVal);
	}

	if(pe->VarTypes[Line] == VarType_Float)
	{
		float CurIntVal = taAtoF(Val.c_str());
		float MinIntVal = taAtoF(pe->MinValues[Line].c_str());
		float MaxIntVal = taAtoF(pe->MaxValues[Line].c_str());

		if(!pe->MinValues[Line].IsEmpty() && CurIntVal < MinIntVal)
			Val = FtoA(MinIntVal);

		if(!pe->MaxValues[Line].IsEmpty() && CurIntVal > MaxIntVal)
			Val = FtoA(MaxIntVal);
	}

   wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED, pe->GetId());
   commandEvent.SetEventObject( pe );
	commandEvent.SetInt(Line);
	commandEvent.SetString(Val);
	wxPostEvent(pe, commandEvent);
	//	pe->PrtForDialogs->GetEventHandler()->ProcessEvent(commandEvent);
	event.Skip();
}

//---------------------------------------------------------------------------------------------
PropEditLine::PropEditLine()
{
	VarType = VarType_String;
	ControlType = CtrlType_EditBox;
	Choices = NULL;
	ChoicesParms = NULL;
	NumChoices = 0;
}

BEGIN_EVENT_TABLE(PropertiesEdit, wxScrolledWindow)
	EVT_BUTTON(-1, PropertiesEdit::OnButtonPress)
	EVT_FILETEXTBOX(-1, PropertiesEdit::OnValueChanged)					//bola zmenena hodnota controlu
	EVT_TEXT_ENTER(-1, PropertiesEdit::OnTextSet)							//user nieco zadal do textoveho pola alebo do comba
	EVT_COMBOBOX(-1, PropertiesEdit::OnSetFromCombo)						//bolo nieco vybrane z listboxu comba
	EVT_CHECKBOX(-1, PropertiesEdit::OnCheckboxSwitch)						//bol prepnuty checkbox
	EVT_COMMAND_SCROLL_ENDSCROLL(-1, PropertiesEdit::OnSliderSwitch)	//bol posunuty slider na inu poziciu
	EVT_SIZE(PropertiesEdit::OnSize)
//	EVT_KILL_FOCUS(PropertiesEdit::OnLostFocus)								//lost focus ktorehokolvek prvku
END_EVENT_TABLE()

/*
	PEditTextCtrl* TextCtrl;
	RichComboBox* RichCombo;
	wxCheckBox* CheckBox;
	wxSlider* Slider;
	FileTextBox* FileCtrl;
	FlagsEditBox* FlagsCtrl;
*/

//----------------------------------------------------------------------------------------
void PropertiesEdit::OnButtonPress(wxCommandEvent &event)
{
	int ID = event.GetId();
	int Line = GetLineIndexFromID(ID);

	if(CtrlTypes[Line] != CtrlType_EventButton)
		return;

	EventEditButton* EvButton = (EventEditButton*)Controls[Line];
	wxString Key = Labels[Line]->GetLabel();
	wxString Val = EvButton->KeyValue;
	wxString ValBefore = Val;
	wxString StateLine = "";

	unsigned int count = States.GetCount();
	for(unsigned int  n = 0; n < count; n++)
	{
		StateLine += States[n];

		if(n < count - 1)
			StateLine += ";";
	}

	EventEditDialog dialog(NULL, wxID_ANY, Key, wxDefaultPosition, wxSize(640, 640), wxDEFAULT_DIALOG_STYLE/* | wxMAXIMIZE_BOX*/, StateLine, &Val);
	dialog.EnableEdit(LinesEnabled);

	if(dialog.ShowModal() == wxID_OK)
	{
		EvButton->KeyValue = Val;

		if(Val != "")
			EvButton->SetLabel("contain code");
		else
			EvButton->SetLabel("unused");

		if(Val != ValBefore)
		{
			wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED/*wxEVT_PROPERTY_CHANGE*/, this->GetId());
			commandEvent.SetEventObject( this );
			commandEvent.SetInt(Line);
			commandEvent.SetString(Val);
			wxPostEvent(this, commandEvent);
		}
		else
		{
			wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED/*wxEVT_PROPERTY_CHANGE*/, this->GetId());
			commandEvent.SetInt(-1);		//tymto davame vediet ze sa nic nemeni
			wxPostEvent(this, commandEvent);
		}
	}
	else
	{
		wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED/*wxEVT_PROPERTY_CHANGE*/, this->GetId());
		commandEvent.SetInt(-1);		//tymto davame vediet ze sa nic nemeni
		wxPostEvent(this, commandEvent);
	}

	event.Skip();
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::OnValueChanged(wxCommandEvent& event)
{
	if(event.GetInt() == -1)	//len informujeme ze sa s tym nieco robilo
	{
		wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED/*wxEVT_PROPERTY_CHANGE*/, this->GetId());
		commandEvent.SetInt(-1);	//nebude sa s tym nic robit len sa v editore nastavi focus na render okno
		wxPostEvent(this, commandEvent);
		event.Skip();
		return;
	}

	int ID = event.GetId();
	int Line = GetLineIndexFromID(ID);
	int CtrlType = CtrlTypes[Line];

	if(CtrlType == CtrlType_FileRequester || CtrlType == CtrlType_FlagsEdit || CtrlType == CtrlType_DialogCombo || CtrlType == CtrlType_ColorEdit)
	{
		TextboxWithButton* Ctrl = (TextboxWithButton*)Controls[Line];
		wxString Key = Labels[Line]->GetLabel();
		wxString Val = Ctrl->GetValue();
		wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED/*wxEVT_PROPERTY_CHANGE*/, this->GetId());
		commandEvent.SetEventObject( this );
		commandEvent.SetInt(Line);
		commandEvent.SetString(Val);
		wxPostEvent(this, commandEvent);
	}

	event.Skip();
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::OnTextSet(wxCommandEvent& event)
{
	int ID = event.GetId();
	int Line = GetLineIndexFromID(ID);
	wxString Key, Val;

	if(CtrlTypes[Line] == CtrlType_EditBox)
	{
		PEditTextCtrl* TextCtrl = (PEditTextCtrl*)Controls[Line];
		Key = Labels[Line]->GetLabel();
		Val = TextCtrl->GetValue();
	}

	if(CtrlTypes[Line] == CtrlType_ComboBox)
	{
		PropEditComboBox* RichCombo = (PropEditComboBox*)Controls[Line];
		Key = Labels[Line]->GetLabel();
		Val = RichCombo->GetValue();		//este moze byt zmeneny

		int ItemIndex = RichCombo->FindString(Val);

		if(ItemIndex != -1)
		{
			wxString* ValToReplace = (wxString*)RichCombo->GetClientData(ItemIndex);
			if(ValToReplace)
				Val = *ValToReplace;
		}
	}

	if(VarTypes[Line] == VarType_Int)
	{
		int CurIntVal = AtoI(Val);
		int MinIntVal = AtoI(MinValues[Line]);
		int MaxIntVal = AtoI(MaxValues[Line]);

		if(!MinValues[Line].IsEmpty() && CurIntVal < MinIntVal)
			Val = ItoA(MinIntVal);

		if(!MaxValues[Line].IsEmpty() && CurIntVal > MaxIntVal)
			Val = ItoA(MaxIntVal);
	}

	if(VarTypes[Line] == VarType_Float)
	{
		float CurIntVal = taAtoF(Val.c_str());
		float MinIntVal = taAtoF(MinValues[Line].c_str());
		float MaxIntVal = taAtoF(MaxValues[Line].c_str());

		if(!MinValues[Line].IsEmpty() && CurIntVal < MinIntVal)
			Val = FtoA(MinIntVal);

		if(!MaxValues[Line].IsEmpty() && CurIntVal > MaxIntVal)
			Val = FtoA(MaxIntVal);
	}

   wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED/*wxEVT_PROPERTY_CHANGE*/, this->GetId());
   commandEvent.SetEventObject( this );
	commandEvent.SetInt(Line);
	commandEvent.SetString(Val);
	wxPostEvent(this, commandEvent);
//   this->GetEventHandler()->ProcessEvent(commandEvent);
	event.Skip(); 
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::OnSetFromCombo(wxCommandEvent& event)
{
	int ID = event.GetId();
	int Line = GetLineIndexFromID(ID);

	PropEditComboBox* RichCombo = (PropEditComboBox*)Controls[Line];
	wxString Key = Labels[Line]->GetLabel();
	wxString Val = event.GetString();//RichCombo->GetValue();

	int ItemIndex = RichCombo->FindString(Val);

	if(ItemIndex != -1)
	{
		wxString* ValToReplace = (wxString*)RichCombo->GetClientData(ItemIndex);
		if(ValToReplace)
			Val = *ValToReplace;
	}

   wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED/*wxEVT_PROPERTY_CHANGE*/, this->GetId());
   commandEvent.SetEventObject( this );
	commandEvent.SetInt(Line);
	commandEvent.SetString(Val);
	wxPostEvent(this, commandEvent);
	event.Skip();
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::OnCheckboxSwitch(wxCommandEvent& event)
{
	int ID = event.GetId();
	int Line = GetLineIndexFromID(ID);

	wxCheckBox* CheckBox = (wxCheckBox*)Controls[Line];
	wxString Key = Labels[Line]->GetLabel();
	wxString Val = ItoA(taBtoI(CheckBox->GetValue()));

	if(CheckBox->IsChecked())
		CheckBox->SetLabel("true");
	else
		CheckBox->SetLabel("false");

   wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED/*wxEVT_PROPERTY_CHANGE*/, this->GetId());
   commandEvent.SetEventObject( this );
	commandEvent.SetInt(Line);
	commandEvent.SetString(Val);
	wxPostEvent(this, commandEvent);
	event.Skip();
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::OnSliderSwitch(wxScrollEvent& event)
{
	int ID = event.GetId();
	int Line = GetLineIndexFromID(ID);

	wxSlider* Slider = (wxSlider*)Controls[Line];
	wxString Key = Labels[Line]->GetLabel();
	wxString Val = ItoA(Slider->GetValue());

   wxCommandEvent commandEvent(wxEVT_COMMAND_MENU_SELECTED/*wxEVT_PROPERTY_CHANGE*/, this->GetId());
   commandEvent.SetEventObject( this );
	commandEvent.SetInt(Line);
	commandEvent.SetString(Val);
	wxPostEvent(this, commandEvent);
	event.Skip();
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::OnLostFocus(wxFocusEvent& event)
{
	int ID = event.GetId();
	int Line = GetLineIndexFromID(ID);	//na ktorej lajne je tento prvok coz je index do Keys a Values
	event.Skip();
}

//----------------------------------------------------------------------------------------
bool PropertiesEdit::CanKillFocus()
{
	wxWindow* FocusedWindow = FindFocus();

	if(FocusedWindow)
	{
		for(int n = 0; n < LinesNum; n++)
		{
			if(Controls[n] == FocusedWindow)
			{
				if(CtrlTypes[n] == CtrlType_EventButton)
					return true;
			}
		}
	}

	return false;
}

//----------------------------------------------------------------------------------------
PropertiesEdit::PropertiesEdit(wxWindow *parent, wxFrame* ParentForDialogs, wxWindowID id, wxPoint pos, wxSize& size) : wxScrolledWindow(parent, id, pos, size, wxVSCROLL | wxSTATIC_BORDER /*| wxALWAYS_SHOW_SB | wxSTATIC_BORDER*/)
{
	PrtForDialogs = ParentForDialogs;
	LinesNum = 0;
	LinesEnabled = true;
	InitSize = size;
	SetScrollbars(0, 1, 0, 20);
	EnableScrolling(false, true);
}

//----------------------------------------------------------------------------------------
int PropertiesEdit::GetLineIndexFromID(int id)
{
	return id - PropControlIDOffset;
}

void PropertiesEdit::OnSize(wxSizeEvent &event)
{
	wxSize size = GetClientSize();
	int CtrlWidth = size.x - (DefLabelWidth + 1);
	int CtrlPosition = DefLabelWidth + 1;
	int CurPos = 0;

	for(int n = 0; n < LinesNum; n++)
	{
		CurPos = n * (DefControlHeight + 1);
		wxPoint CtrlPos = wxPoint(CtrlPosition, CurPos);
		wxSize CtrlSize = wxSize(CtrlWidth, DefControlHeight);
		Controls[n]->SetSize(CtrlSize);
	}
	event.Skip();
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::BeginSetup()
{
	wxSize size = GetClientSize();
	Scroll(0, 0);
	Freeze();

	for(int n = 0; n < LinesNum; n++)
	{
		RemoveChild(Labels[n]);
		RemoveChild(Controls[n]);
		delete Labels[n];
		delete Controls[n];
		Labels[n] = NULL;
		Controls[n] = NULL;
		Keys[n] = "";
		Values[n] = "";
	}
	LinesNum = 0;
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::AddPropLine(PropEditLine& line)
{
	int ExtraStyle = 0;
	int LabelID = PropLabelIDOffset + LinesNum;
	int ControlID = PropControlIDOffset + LinesNum;
	Keys[LinesNum] = line.Key;
	Values[LinesNum] = line.Value;			//zapametame si povodne vlozene hodnoty
	PEditTextCtrl* TextCtrl;
	PropEditComboBox* RichCombo;
	wxCheckBox* CheckBox;
	wxSlider* Slider;
	FileTextBox* FileCtrl;
	FlagsEditBox* FlagsCtrl;
	EventEditButton* EventButton;
	DialogCombo* DlgCombo;
	ColorEdit* ColorEd;
	int CheckVal;
	int n;
	wxString* DataVal;
	wxString NewComboVal;

	CtrlTypes[LinesNum] = line.ControlType;
	VarTypes[LinesNum] = line.VarType;
	MinValues[LinesNum] = line.MinValue;
	MaxValues[LinesNum] = line.MaxValue;

	wxSize size = GetClientSize();

	if(size == wxSize(0, 0))	//FIXME nulovu velkost to ma koli DropDown containeru ktory su sizuje na nulu. chcelo by to nejak inak
		size = wxSize(200,100);

	int CtrlWidth = size.x - (DefLabelWidth + 1);
	int CtrlPosition = DefLabelWidth + 1;
	int CurPos = LinesNum * (DefControlHeight + 1);

	wxPoint CtrlPos = wxPoint(CtrlPosition, CurPos);
	wxSize CtrlSize = wxSize(CtrlWidth, DefControlHeight);

	Labels[LinesNum] = new wxStaticText(this, LabelID, line.Key, wxPoint(0, CurPos), wxSize(DefLabelWidth, DefLabelHeight), 0);//	Labels[LinesNum++]->SetFont(*Font1)
	Labels[LinesNum]->SetBackgroundColour(wxColour(148,148,148));

	switch(line.ControlType)
	{
	case CtrlType_EditBox:
			TextCtrl = new PEditTextCtrl(this, ControlID, CtrlPos, CtrlSize, wxPoint(0,0), wxString(""), line.VarType, line.MinValue, line.MaxValue); 
			TextCtrl->SetValue(line.Value);
			TextCtrl->SetToolTip(line.HelpText);
			Controls[LinesNum] = TextCtrl;
		break;

	case CtrlType_ComboBox:
	{
		RichCombo = (PropEditComboBox*)new PropEditComboBox((wxWindow*)this, ControlID, wxT(""), CtrlPos, CtrlSize, line.NumChoices, line.Choices, wxCB_DROPDOWN | wxCB_SORT/*|wxCB_READONLY*/, line.VarType, line.MinValue, line.MaxValue);

		for(n = 0; n < line.NumChoices; n++)
		{
			if(!line.ChoicesParms[n].IsEmpty())
			{
				int ItemIndex = RichCombo->FindString(line.Choices[n]);

				if(ItemIndex != -1)
					RichCombo->SetClientData(ItemIndex, new wxString(line.ChoicesParms[n]));	//su pripady ked textovu polozku zamiename za inu hodnotu definovanu v XML
			}
		}

		NewComboVal = line.Value;

		if(line.VarType == VarType_Int || line.VarType == VarType_Float)
		{
			if(NewComboVal == "")
				NewComboVal = "0";
		}

		bool ReplacedFromChoice = false;
		for(n = 0; n < (int)RichCombo->GetCount(); n++)
		{
			DataVal = (wxString*)RichCombo->GetClientData(n);

			if(DataVal && *DataVal == NewComboVal)
			{
				NewComboVal = RichCombo->GetString(n);
				ReplacedFromChoice = true;
				break;
			}
		}

		if(line.VarType == VarType_Int || line.VarType == VarType_Float)
		{
			if(ReplacedFromChoice == false && NewComboVal == "0")
				NewComboVal = "";
		}

		RichCombo->SetValue(NewComboVal);
		RichCombo->SetToolTip(line.HelpText);
		Controls[LinesNum] = RichCombo;
		break;
	}
	case CtrlType_CheckBox:
		CheckBox = new wxCheckBox(this, ControlID, _T("false"), CtrlPos, CtrlSize);
		CheckBox->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));

		CheckVal = AtoI(line.Value);
		CheckBox->SetValue(taItoB(CheckVal));

		if(CheckVal)
			CheckBox->SetLabel("true");
		else
			CheckBox->SetLabel("false");

		CheckBox->SetToolTip(line.HelpText);
		Controls[LinesNum] = CheckBox;
		break;

	case CtrlType_Slider:
		Slider = new wxSlider(this, ControlID, AtoI(line.Value), AtoI(line.MinValue), AtoI(line.MaxValue), CtrlPos, CtrlSize, wxSL_HORIZONTAL | wxSL_AUTOTICKS/* | wxSL_LABELS*/, wxDefaultValidator, wxString("label"));
//		Slider->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
		Slider->SetValue(AtoI(line.Value));
		Slider->SetToolTip(line.HelpText);
		Controls[LinesNum] = Slider;
		break;

	case CtrlType_IncrementBox:
		break;

	case CtrlType_FileRequester:
		FileCtrl = new FileTextBox(this, ControlID, (RichFileDialog*)line.ExternControl, CtrlPos, CtrlSize, wxTE_PROCESS_ENTER | wxSTATIC_BORDER, fileopen_xpm);
		FileCtrl->SetValue(line.Value);
		FileCtrl->TextBox->SetToolTip(line.HelpText);
		FileCtrl->button->SetToolTip(line.HelpText);

		if(line.NumChoices > 0)
		{
			FileCtrl->FileMask = line.Choices[0];	//mame tam ulozenu masku
			FileCtrl->DefaultDir = /*line.Choices[2] + "/" + */line.Choices[1];	//default dir + relativne pokracovanie k tomu diru
/*			
			wxString Ch1 = line.Choices[1];
			wxString Ch2 = line.Choices[2];
			FileCtrl->DefaultDir = FileCtrl->DefaultDir;*/
		}

		Controls[LinesNum] = FileCtrl;
		break;

	case CtrlType_FlagsEdit:
		FlagsCtrl = new FlagsEditBox(this, PrtForDialogs, ControlID, CtrlPos, CtrlSize);
		FlagsCtrl->SetValue(line.Value);

		for(n = 0; n < line.NumChoices; n++)
		{
			FlagsCtrl->FlagsDlg->FlagsCheckList->Append(line.Choices[n]);
			FlagsCtrl->FlagsDlg->Bits[n] = AtoI(line.ChoicesParms[n]);
//			FlagsCtrl->FlagsDlg->FlagsCheckList->SetClientData(n, new wxString(line.ChoicesParms[n]));	//bit
		}

		FlagsCtrl->SetToolTip(line.HelpText);
		FlagsCtrl->button->SetToolTip(line.HelpText);
		Controls[LinesNum] = FlagsCtrl;
		break;
	
	case CtrlType_EventButton:
		EventButton = new EventEditButton(this, ControlID, _T("unused"), CtrlPos, CtrlSize);
		EventButton->KeyValue = line.Value;

		if(line.Value != "")
			EventButton->SetLabel("contain code");

		EventButton->SetToolTip(line.HelpText);
		Controls[LinesNum] = EventButton;
		break;
	case CtrlType_DialogCombo:
		{
			DlgCombo = new DialogCombo(this, ControlID, CtrlPos, CtrlSize, line.ExternBitmap);
			DlgCombo->dialog = (UniversalBaseDialog*)line.ExternControl;//UsingDialog;
			DlgCombo->SetValue(line.Value);
			DlgCombo->SetToolTip(line.HelpText);
			DlgCombo->button->SetToolTip(line.HelpText);
			Controls[LinesNum] = DlgCombo;
		}
		break;
	case CtrlType_ColorEdit:
		ColorEd = new ColorEdit(this, PrtForDialogs, ControlID, CtrlPos, CtrlSize);
		ColorEd->SetValue(line.Value);
		{
#ifdef ANDER_HOME
			taVector color(1,1,1);
#else
			Vector3 color(1,1,1);
#endif
			if(!line.Value.IsEmpty())
				color = StrToVec(line.Value);

			ColorEd->button->SetBackgroundColour(wxColour(color.x * (float)255, color.y * (float)255, color.z * (float)255));
		}
		ColorEd->SetToolTip(line.HelpText);
		ColorEd->button->SetToolTip(line.HelpText);
		Controls[LinesNum] = ColorEd;
		break;
	default:
		break;
	}

	Labels[LinesNum]->Freeze();
	Controls[LinesNum]->Freeze();
	LinesNum++;
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::EndSetup()
{
	wxSize size = GetClientSize();
	wxSize VirtualSize = size;

	if(LinesNum > 0)
		VirtualSize.y = (LinesNum * DefControlHeight) + LinesNum;
	else
		VirtualSize.y = InitSize.y;

	wxSize NewSize = VirtualSize;
	int MaxVisibleLines = 20;
	int MaxClientSize = MaxVisibleLines * (DefControlHeight + 1);

	if(NewSize.y > MaxClientSize)
		NewSize.y = MaxClientSize;

	SetClientSize(NewSize);
	SetVirtualSize(VirtualSize);
	
	for(int n = 0; n < LinesNum; n++)
	{
		Labels[n]->Thaw();
		Controls[n]->Thaw();
	}

	Thaw();
	Update();
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::EnableLines(bool enable)
{
	LinesEnabled = enable;

	for(int n = 0 ; n < LinesNum; n++)
	{
		if(CtrlTypes[n] != CtrlType_EventButton)
			Controls[n]->Enable(enable);
	}
}

//----------------------------------------------------------------------------------------
void PropertiesEdit::ChangeValue(wxString key, wxString val)
{
	if(key == "")
		return;

	PEditTextCtrl* TextCtrl;
	PropEditComboBox* RichCombo;
	wxCheckBox* CheckBox;
	wxSlider* Slider;
	FileTextBox* FileCtrl;
	FlagsEditBox* FlagsCtrl;
	EventEditButton* EventButton;
	DialogCombo* DlgCombo;
	ColorEdit* ColorEd;
	int CheckVal;

	for(int n = 0; n < LinesNum; n++)
	{
		if(Keys[n] == key)
		{
			switch(CtrlTypes[n])
			{
			case CtrlType_EditBox:
				TextCtrl = (PEditTextCtrl*)Controls[n];
				TextCtrl->SetValue(val);
				break;

			case CtrlType_ComboBox:
				RichCombo = (PropEditComboBox*)Controls[n];
				RichCombo->SetValue(val);
				break;

			case CtrlType_CheckBox:
				CheckBox = (wxCheckBox*)Controls[n];
				CheckVal = AtoI(val);
				CheckBox->SetValue(taItoB(CheckVal));

				if(CheckVal)
					CheckBox->SetLabel("true");
				else
					CheckBox->SetLabel("false");
				break;

			case CtrlType_Slider:
				Slider = (wxSlider*)Controls[n];
				Slider->SetValue(AtoI(val));
				break;

			case CtrlType_IncrementBox:
				break;

			case CtrlType_FileRequester:
				FileCtrl = (FileTextBox*)Controls[n];
				FileCtrl->SetValue(val);
				break;

			case CtrlType_FlagsEdit:
				FlagsCtrl = (FlagsEditBox*)Controls[n];
				FlagsCtrl->SetValue(val);
				break;
			
			case CtrlType_EventButton:
				EventButton = (EventEditButton*)Controls[n];
				EventButton->KeyValue = val;

				if(val != "")
					EventButton->SetLabel("contain code");
				break;
			case CtrlType_DialogCombo:
				DlgCombo = (DialogCombo*)Controls[n];
				DlgCombo->SetValue(val);
				break;
			case CtrlType_ColorEdit:
				ColorEd = (ColorEdit*)Controls[n];
				ColorEd->SetValue(val);
				break;
			}
			return;
		}
	}
}
 
BEGIN_EVENT_TABLE(UniversalBaseDialog, wxDialog)
    EVT_BUTTON(-1, UniversalBaseDialog::OnButtonClick)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------------
UniversalBaseDialog::UniversalBaseDialog(wxFrame* parent, wxWindowID id, wxString title, wxPoint pos, wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	ModalReturnCode = 0;
}

//-------------------------------------------------------------------------------------------------------
void UniversalBaseDialog::SetInputValue(wxString value)
{
	InputValue = value;
}

//-------------------------------------------------------------------------------------------------------
wxString UniversalBaseDialog::GetResultValue()
{
	return InputValue;
}

//-------------------------------------------------------------------------------------------------------
int UniversalBaseDialog::ShowAsModal()
{
	ModalReturnCode = ShowModal();
	return ModalReturnCode;
}

//-------------------------------------------------------------------------------------------------------
void UniversalBaseDialog::OnButtonClick(wxCommandEvent &event)
{
	int ID = event.GetId();

	if(ID == wxID_OK)
	{
		EndModal(ModalReturnCode);
	}
	else if (ID == wxID_CANCEL)
	{
		EndModal(ModalReturnCode);
	}
	event.Skip();
}

//-------------------------------------------------------------------------------------------------------
RichSingleChoiceDialog::RichSingleChoiceDialog(wxFrame* parent, wxWindowID id, wxString title, wxArrayString& choices, wxPoint pos, wxSize size, long style) : UniversalBaseDialog(parent, id, title, pos, size, style)
{
	ChoicesListBox = new wxListBox(this, -1, wxDefaultPosition, wxDefaultSize, choices, wxLB_NEEDED_SB|wxLB_SORT);
	OkButton = new wxButton(this, wxID_OK, "Ok", wxDefaultPosition, wxSize(80, 30));
	CancelButton = new wxButton(this, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(80, 30));
	ChoicesListBox->SetMinSize(wxSize(size.x - 20, size.y - 80));

	wxBoxSizer* ButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
	ButtonsSizer->Add(OkButton, 0, wxALIGN_TOP|wxALL, 0);
	ButtonsSizer->Add(10, 0, 0, wxALIGN_TOP | wxTOP, 10); // spacer
	ButtonsSizer->Add(CancelButton, 0, wxALIGN_TOP|wxALL, 0);

	wxBoxSizer* PanelSizer = new wxBoxSizer(wxVERTICAL);
	PanelSizer->Add(ChoicesListBox, 0, wxGROW|wxALIGN_LEFT|wxALL, 10);
//	PanelSizer->Add(1, 0, 0, wxALIGN_CENTER | wxGROW | wxTOP, 10); // spacer
	PanelSizer->Add(ButtonsSizer, 0, wxALIGN_LEFT|wxLEFT| wxBOTTOM, 10);

	SetSizer(PanelSizer);
	PanelSizer->Fit(this);
	PanelSizer->Layout();
	SetAutoLayout(TRUE);
}
//-------------------------------------------------------------------------------------------------------
void RichSingleChoiceDialog::SetInputValue(wxString value)
{
	InputValue = value;
	int sel = ChoicesListBox->FindString(value);

	if(sel != -1)
		ChoicesListBox->SetSelection(sel);
}

//-------------------------------------------------------------------------------------------------------
wxString RichSingleChoiceDialog::GetResultValue()
{
	wxString result = InputValue;

	int sel = ChoicesListBox->GetSelection();

	if(sel != -1)
		result = ChoicesListBox->GetString(sel);

	return result;
}

//-------------------------------------------------------------------------------------------------------
void RichSingleChoiceDialog::SetSelection(int index)
{
	ChoicesListBox->SetSelection(index);
}

//-------------------------------------------------------------------------------------------------------
int RichSingleChoiceDialog::GetSelection()
{
	return ChoicesListBox->GetSelection();
}

//-------------------------------------------------------------------------------------------------------
void RichSingleChoiceDialog::Clear()
{
	ChoicesListBox->Clear();
}


BEGIN_EVENT_TABLE(MultiChoiceDialog, UniversalBaseDialog)
    EVT_SIZE(MultiChoiceDialog::OnSize)
	 EVT_TREE_SEL_CHANGED(-1, MultiChoiceDialog::OnTreeSelectionChanged)
	 EVT_LISTBOX(-1, MultiChoiceDialog::OnListboxSelectionChanged)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------------
MultiChoiceDialog::MultiChoiceDialog(wxFrame* parent, wxWindowID id, wxString title, wxPoint pos, wxSize& size, long style) : UniversalBaseDialog(parent, id, title, pos, size, style)
{
	CloneTree = NULL;
	Font1 = new wxFont(10, wxFONTFAMILY_DEFAULT, wxNORMAL, wxFONTWEIGHT_BOLD, false, _T("fixedsys"));
//	Font1->SetWeight(wxBOLD);
	ChoicesThree = new wxTreeCtrl(this, -1, wxDefaultPosition, wxDefaultSize, wxTR_HIDE_ROOT|wxTR_HAS_BUTTONS|wxTR_LINES_AT_ROOT|wxTR_MULTIPLE);
	ChoicesListBox = new wxListBox(this, -1, wxDefaultPosition, wxDefaultSize, 0, NULL, wxLB_NEEDED_SB|wxLB_EXTENDED);
	AddButton = new wxButton(this, MCH_ADDBUTTON, "<--", wxDefaultPosition, wxSize(80, 30));
	RemoveButton = new wxButton(this, MCH_REMOVEBUTTON, "-->", wxDefaultPosition, wxSize(80, 30));
	OkButton = new wxButton(this, wxID_OK, "Ok", wxDefaultPosition, wxSize(80, 30));
	CancelButton = new wxButton(this, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(80, 30));

	ChoicesListBox->SetMinSize(wxSize((size.x / 2) - 60, size.y - 80));
	ChoicesThree->SetMinSize(wxSize((size.x / 2) - 60, size.y - 80));
	AddButton->SetFont(*Font1);
	RemoveButton->SetFont(*Font1);

	wxBoxSizer* ButtonsSizer2 = new wxBoxSizer(wxVERTICAL);
	ButtonsSizer2->Add(AddButton, 0, wxALIGN_LEFT|wxALL, 0);
	ButtonsSizer2->Add(0, 10, 0, wxALIGN_LEFT | wxTOP, 0); // spacer
	ButtonsSizer2->Add(RemoveButton, 0, wxALIGN_LEFT|wxALL, 0);

	wxBoxSizer* FrameBoxSizer = new wxBoxSizer(wxHORIZONTAL);
	FrameBoxSizer->Add(ChoicesListBox, 0, wxALIGN_TOP|wxALL, 10);
	FrameBoxSizer->Add(ButtonsSizer2, 0, wxALIGN_CENTER_VERTICAL|wxTOP, 0);
	FrameBoxSizer->Add(ChoicesThree, 0, wxALIGN_TOP|wxALL, 10);

	wxBoxSizer* ButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
	ButtonsSizer->Add(OkButton, 0, wxALIGN_TOP|wxALL, 0);
	ButtonsSizer->Add(10, 0, 0, wxALIGN_TOP | wxTOP, 0); // spacer
	ButtonsSizer->Add(CancelButton, 0, wxALIGN_TOP|wxALL, 0);

	wxBoxSizer* PanelSizer = new wxBoxSizer(wxVERTICAL);
	PanelSizer->Add(FrameBoxSizer, 0, wxALIGN_TOP|wxALL, 0);
//	PanelSizer->Add(0, 0, 0, wxALIGN_CENTER | wxGROW | wxTOP, 10); // spacer
	PanelSizer->Add(ButtonsSizer, 0, wxALIGN_BOTTOM|wxLEFT|wxBOTTOM, 10);

	SetSizer(PanelSizer);
	PanelSizer->Fit(this);
	PanelSizer->Layout();
	SetAutoLayout(TRUE);
}

//-------------------------------------------------------------------------------------------------------
MultiChoiceDialog::~MultiChoiceDialog()
{
	if(Font1)
	{
		delete Font1;
		Font1 = NULL;
	}
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::SetCloneTreeView(wxTreeCtrl* tree)
{
	CloneTree = tree;
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::SetInputValue(wxString value)
{
	InputValue = value;
	
//	ChoicesThree->SelectItem(const wxTreeItemId& item, bool select = true)
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::SelectUserChoicesInTree()
{
	wxTreeItemId TreeRoot = ChoicesThree->GetRootItem();

	if(!TreeRoot)
		return;

	wxTreeItemIdValue cookie;
	wxTreeItemId Child = ChoicesThree->GetFirstChild(TreeRoot, cookie);
	wxString ListboxItem;
	wxString TreeItem;
	wxTreeItemId TypeChild;

	while(Child)
	{
		TypeChild = ChoicesThree->GetFirstChild(Child, cookie);

		while(TypeChild)
		{
			TreeItem = ChoicesThree->GetItemText(TypeChild);
			bool AnyItem = false;
			for(int n = 0; n < (int)ChoicesListBox->GetCount(); n++)
			{
				ListboxItem = ChoicesListBox->GetString(n);

				if(TreeItem == ListboxItem)
				{
					ChoicesThree->SelectItem(TypeChild, true);
//					ChoicesThree->Expand(TypeChild);
					ChoicesThree->EnsureVisible(TypeChild);
					AnyItem = true;
					break;
				}
			}

/*			if(AnyItem == false)
			{
				if(ChoicesThree->IsExpanded(TypeChild))
					ChoicesThree->Toggle(TypeChild);
//				ChoicesThree->Collapse(TypeChild);
			}*/

			TypeChild = ChoicesThree->GetNextSibling(TypeChild);
		}

		Child = ChoicesThree->GetNextSibling(Child);
	}	
}

//-------------------------------------------------------------------------------------------------------
int MultiChoiceDialog::ShowAsModal()
{
	if(CloneTree)
	{
		ChoicesThree->DeleteAllItems();
		wxTreeItemId CloneTreeRoot = CloneTree->GetRootItem();
		wxTreeItemId TreeRoot = ChoicesThree->AddRoot("root_name", -1, -1, NULL);

		wxTreeItemIdValue cookie;
		wxTreeItemId Child = CloneTree->GetFirstChild(CloneTreeRoot, cookie);
		wxTreeItemId SubChild;

		while(Child)
		{
			wxTreeItemId item = ChoicesThree->AppendItem(TreeRoot, CloneTree->GetItemText(Child), -1, -1, NULL);
			ChoicesThree->SetItemBold(item, true);

			SubChild = CloneTree->GetFirstChild(Child, cookie);
			while(SubChild)
			{
				ChoicesThree->AppendItem(item, CloneTree->GetItemText(SubChild), -1, -1, NULL);
				SubChild = CloneTree->GetNextSibling(SubChild);
			}

			Child = CloneTree->GetNextSibling(Child);
		}
	}

	ChoicesListBox->Clear();
	wxString temp = InputValue;
	wxString item;

	while(!temp.IsEmpty())
	{
		item = temp.BeforeFirst(',');

		if(!item.IsEmpty())
			ChoicesListBox->Append(item);

		temp = temp.AfterFirst(',');
	}

	SelectUserChoicesInTree();
	return UniversalBaseDialog::ShowAsModal();
}

//-------------------------------------------------------------------------------------------------------
wxString MultiChoiceDialog::GetResultValue()
{
	wxString result;
	int count = ChoicesListBox->GetCount();

	for(int n = 0; n < count; n++)
	{
		result += ChoicesListBox->GetString(n);

		if(n < count - 1)
			result += ",";
	}

	return result;
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::SetSelections(wxString& CodedSelections)
{
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::Clear()
{
	wxTreeItemId TreeRoot = ChoicesThree->GetRootItem();
	ChoicesThree->DeleteAllItems();
	ChoicesListBox->Clear();
	AddButton->Enable(false);
	RemoveButton->Enable(false);
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::AddChoice(wxString& category, wxString& choice)
{
	wxTreeItemId TreeRoot = ChoicesThree->GetRootItem();

	if(!TreeRoot)
		TreeRoot = ChoicesThree->AddRoot("root_name", -1, -1, NULL);

	wxTreeItemIdValue cookie;
	wxTreeItemId Child = ChoicesThree->GetFirstChild(TreeRoot, cookie);
	bool added = false;

	while(Child)
	{
		if(ChoicesThree->GetItemText(Child) == category)
		{
			ChoicesThree->AppendItem(Child, choice, -1, -1, NULL);
			added = true;
			break;
		}

		Child = ChoicesThree->GetNextSibling(Child);
	}

	if(!added)
	{
		Child = ChoicesThree->AppendItem(TreeRoot, category, -1, -1, NULL);
		ChoicesThree->AppendItem(Child, choice, -1, -1, NULL);

		if(!ChoicesThree->IsBold(Child))
			ChoicesThree->SetItemBold(Child, true);
	}
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::SortChoices()
{
	wxTreeItemId TreeRoot = ChoicesThree->GetRootItem();

	if(!TreeRoot)
		return;

	ChoicesThree->SortChildren(TreeRoot);
	wxTreeItemIdValue cookie;
	wxTreeItemId Child = ChoicesThree->GetFirstChild(TreeRoot, cookie);

	while(Child)
	{
		ChoicesThree->SortChildren(Child);
		Child = ChoicesThree->GetNextSibling(Child);
	}
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::OnButtonClick(wxCommandEvent& event)
{
	int ID = event.GetId();

	if(ID == MCH_ADDBUTTON)
	{
		wxArrayTreeItemIds selections;
		size_t NumSelections = ChoicesThree->GetSelections(selections);
		wxString ItemText;

		for(size_t n = 0; n < NumSelections; n++)
		{
			ItemText = ChoicesThree->GetItemText(selections[n]);

			if(ChoicesThree->IsBold(selections[n]) == false && ChoicesListBox->FindString(ItemText) == wxNOT_FOUND)
				ChoicesListBox->Append(ItemText);
		}

//		UpdateButtonsAccesibility();
		ChoicesThree->SetFocus();
	}
	else if(ID == MCH_REMOVEBUTTON)
	{
		wxArrayInt selections;
		int NumSelections = ChoicesListBox->GetSelections(selections);
		int LastDeleted = -1;
		
		while(NumSelections)
		{
			LastDeleted = selections[0];
			ChoicesListBox->Delete(LastDeleted);
			NumSelections = ChoicesListBox->GetSelections(selections);
		}

		if(ChoicesListBox->GetCount() > 0)
		{
			if(LastDeleted < (int)ChoicesListBox->GetCount())
				ChoicesListBox->Select(LastDeleted);
			else
				ChoicesListBox->Select(LastDeleted - 1);
		}

//		UpdateButtonsAccesibility();
		ChoicesListBox->SetFocus();
	}
	
	UniversalBaseDialog::OnButtonClick(event);
	event.Skip();
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::UpdateButtonsAccesibility()
{
	wxArrayInt selections;
	int NumSelections = ChoicesListBox->GetSelections(selections);
	RemoveButton->Enable(!(NumSelections == 0));

	wxArrayTreeItemIds sel;
	size_t NumSel = ChoicesThree->GetSelections(sel);
	AddButton->Enable(!(NumSel == 0));
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::OnTreeSelectionChanged(wxTreeEvent& event)
{
	wxTreeItemId SelectedItem = event.GetItem();

	wxArrayTreeItemIds sel;
	size_t NumSel = ChoicesThree->GetSelections(sel);
	bool EnableAdd = false;

	for(size_t n = 0; n < NumSel; n++)
	{
		if(ChoicesThree->IsBold(sel[n]) == false)
		{
			EnableAdd = true;
			break;
		}
	}

	AddButton->Enable(EnableAdd);
	RemoveButton->Enable(false);
	event.Skip();
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::OnListboxSelectionChanged(wxCommandEvent& event)
{
	AddButton->Enable(false);
	RemoveButton->Enable(true);
	event.Skip();
}

//-------------------------------------------------------------------------------------------------------
void MultiChoiceDialog::OnSize(wxSizeEvent &event)
{
	wxSizer* sizer = this->GetSizer();
	wxSize size = GetClientSize();

	ChoicesListBox->SetMinSize(wxSize((size.x / 2) - 60, size.y - 80));
	ChoicesThree->SetMinSize(wxSize((size.x / 2) - 60, size.y - 80));
	sizer->Layout();
	event.Skip();
}

BEGIN_EVENT_TABLE(DropdownButton, wxButton)
	EVT_BUTTON(-1, DropdownButton::OnButtonPress)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------------
DropdownButton::DropdownButton(wxWindow* parent, DropdownContainer* container, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name) : wxButton(parent, id, label, pos, size, style, validator, name)
{
	Container = container;
}

//-------------------------------------------------------------------------------------------------------
void DropdownButton::OnButtonPress(wxCommandEvent& event)
{
	Container->OnDropButtonClick();
	event.Skip();
}

BEGIN_EVENT_TABLE(DropdownContainer, wxStaticBox)
   EVT_SIZE(DropdownContainer::OnSize)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------------
DropdownContainer::DropdownContainer(wxWindow* parent, wxWindowID id, const wxString& label, bool StartExpanded, const wxPoint& pos, const wxSize& size, long style) : wxStaticBox(parent, id, wxString(""), pos, size, style)
{
	expanded = StartExpanded;
	Parent = parent;
	NextContainer = NULL;
	button = new DropdownButton(parent, this, id, label, wxDefaultPosition, wxDefaultSize, wxNO_BORDER);
	sizer = new wxStaticBoxSizer(this, wxVERTICAL);

	if(expanded)
	{
		FirstSpacer = sizer->Add(0, 11, wxALIGN_LEFT | wxALL, 0);
//		button->SetBackgroundColour(wxColour(200,200,200));
	}
	else
	{
		FirstSpacer = sizer->Add(0, 0, wxALIGN_LEFT | wxALL, 0);
//		button->SetBackgroundColour(wxColour(148,148,148));
	}
}

//-------------------------------------------------------------------------------------------------------
void DropdownContainer::SetNextContainer(DropdownContainer* container)
{
	NextContainer = container;
}

//-------------------------------------------------------------------------------------------------------
void DropdownContainer::UpdateButtonPos()
{
	wxSize size = GetClientSize();
	wxPoint pos = GetPosition();
	button->Move(pos.x + 5, pos.y - 3);
	button->SetSize(size.x - 10, 20);
}

//-------------------------------------------------------------------------------------------------------
void DropdownContainer::OnSize(wxSizeEvent &event)
{
	UpdateButtonPos();
	event.Skip();
}

//-------------------------------------------------------------------------------------------------------
void DropdownContainer::AddItem(wxWindow* item, int flags, int border)
{
	DContainerItem* ItemData = new DContainerItem;
	ItemData->item = item;
	ItemData->flags = flags;
	ItemData->border = border;
	items.Append((wxObject*)ItemData);

	if(expanded)
		ItemData->SizerItem = sizer->Add(item, 0, flags, border);
	else
	{
		ItemData->SizerItem = sizer->Add(item, 0, flags, border);
		ItemData->SizerItem->SetDimension(wxDefaultPosition, wxSize(0, 0));
		item->Show(false);
	}
}

//-------------------------------------------------------------------------------------------------------
void DropdownContainer::AddItem(wxSizer* item, int flags, int border)
{
	DContainerItem* ItemData = new DContainerItem;
	ItemData->item = item;
	ItemData->flags = flags;
	ItemData->border = border;
	items.Append((wxObject*)ItemData);

	if(expanded)
		ItemData->SizerItem = sizer->Add(item, 0, flags, border);
	else
	{
		ItemData->SizerItem = sizer->Add(item, 0, flags, border);
		ItemData->SizerItem->SetDimension(wxDefaultPosition, wxSize(0, 0));
		item->Show(false);
	}
}

//-------------------------------------------------------------------------------------------------------
wxSizer* DropdownContainer::GetSizer()
{
	return sizer;
}

//-------------------------------------------------------------------------------------------------------
void DropdownContainer::UpdateItemSize(wxWindow* Item, wxSize& size)
{
	DContainerItem* ItemData;
	for(wxNode* node = items.GetFirst(); node; node = node->GetNext())
	{
		ItemData = (DContainerItem*)node->GetData();

		if(ItemData->item == Item)
		{
			ItemData->SizerItem->SetInitSize(size.x, size.y);
			break;
		}
	}

//	GetSizer()->Layout();

	wxSizer* ParentSizer = Parent->GetSizer();
	if(ParentSizer)
	{
		ParentSizer->Layout();

		for(DropdownContainer* Next = NextContainer; Next; Next = Next->NextContainer)
			Next->UpdateButtonPos();

		Parent->Refresh(false);
	}
}

//-------------------------------------------------------------------------------------------------------
void DropdownContainer::OnDropButtonClick()
{
	expanded = !expanded;
	DContainerItem* ItemData;
	wxWindow* window;
	wxSizer* Sizer;

	for(wxNode* node = items.GetFirst(); node; node = node->GetNext())
	{
		ItemData = (DContainerItem*)node->GetData();

		if(expanded)
		{
			if(ItemData->item->IsKindOf(CLASSINFO(wxSizer)))
			{
				Sizer = (wxSizer*)ItemData->item;
				Sizer->Show(true);
			}
			else if(ItemData->item->IsKindOf(CLASSINFO(wxWindow)))
			{
				window = (wxWindow*)ItemData->item;
				window->Show(true);
			}
		}
		else
		{
			if(ItemData->item->IsKindOf(CLASSINFO(wxSizer)))
			{
				Sizer = (wxSizer*)ItemData->item;
				Sizer->Show(false);
				ItemData->SizerItem->SetDimension(wxDefaultPosition, wxSize(0, 0));
			}
			else if(ItemData->item->IsKindOf(CLASSINFO(wxWindow)))
			{
				window = (wxWindow*)ItemData->item;
				window->Show(false);
				ItemData->SizerItem->SetDimension(wxDefaultPosition, wxSize(0, 0));
			}
		}
	}

	if(expanded)
		FirstSpacer->SetInitSize(0, 11);
	else
		FirstSpacer->SetInitSize(0, 0);

	wxSizer* ParentSizer = Parent->GetSizer();

	if(ParentSizer)
	{
		ParentSizer->Layout();

		for(DropdownContainer* Next = NextContainer; Next; Next = Next->NextContainer)
			Next->UpdateButtonPos();

		Parent->Refresh(false);
	}

	if(!expanded)
	{
		wxSize size = GetClientSize();
		SetSize(wxSize(size.x, 14));
	}

   wxCommandEvent commandEvent(wxEVT_COMMAND_BUTTON_CLICKED, this->GetId());
   commandEvent.SetEventObject( this );
	commandEvent.SetInt(taBtoI(expanded));
	wxPostEvent(this, commandEvent);
}

//-------------------------------------------------------------------------------------------------------
DropdownContainer::~DropdownContainer()
{
	if(button)
	{
		delete button;
		button = NULL;
	}

	for(wxNode* node = items.GetFirst(); node; node = node->GetNext())
	{
		DContainerItem* ItemData = (DContainerItem*)node->GetData();
		delete ItemData;
	}
}

BEGIN_EVENT_TABLE(DescriptionDialog, wxDialog)
   EVT_BUTTON(-1, DescriptionDialog::OnButtonClick)
	EVT_KEY_DOWN(DescriptionDialog::OnKeyDown)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------------
DescriptionDialog::DescriptionDialog(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, wxString title, wxString DescText, wxFont* TextFont) : wxDialog(parent, id, title, pos, size, style)
{
	TextArea = new wxTextCtrl(this, -1, DescText, wxDefaultPosition, wxDefaultSize, wxNO_BORDER|wxTE_READONLY|wxTE_MULTILINE);
//	OkButton = new wxButton(this, wxID_ANY, "OK", wxDefaultPosition, wxDefaultSize);

	if(TextFont)
		TextArea->SetFont(*TextFont);

	wxBoxSizer* DialogSizer = new wxBoxSizer(wxVERTICAL);
	DialogSizer->Add(TextArea, 1, wxGROW | wxALIGN_LEFT | wxALL, 0);
//	DialogSizer->Add(OkButton, 0, wxALIGN_CENTER | wxLEFT, 0);

	SetSizer(DialogSizer);
//	DialogSizer->Fit(this);
//	DialogSizer->Layout();
	SetAutoLayout(TRUE);
}

//-------------------------------------------------------------------------------------------------------
void DescriptionDialog::OnButtonClick(wxCommandEvent& event)
{
	this->Show(false);
}

//-------------------------------------------------------------------------------------------------------
void DescriptionDialog::OnKeyDown(wxKeyEvent& event)
{
	int KeyCode = event.GetKeyCode();

   if(KeyCode == WXK_ESCAPE)  //Esc
      this->Show(false);

	event.Skip();
}

//-------------------------------------------------------------------------------------------------------
RichButton::RichButton(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name) : wxButton(parent, id, label, pos, size, style, validator, name)
{
	Data = (void*)NULL;
}


//-------------------------------------------------------------------------------------------------
BEGIN_EVENT_TABLE(RichSplitter, wxSplitterWindow)
	EVT_SIZE(RichSplitter::OnSize)
	EVT_SPLITTER_SASH_POS_CHANGED(-1, RichSplitter::OnSplitterPosChanged)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------
RichSplitter::RichSplitter(wxWindow* parent, wxWindowID id, int SashPos, bool FromRightSide, const wxPoint& point, const wxSize& size, long style) : wxSplitterWindow(parent, id, point, size, style)
{
	Parent = parent;
	FromRight = FromRightSide;
	NeededSashPos = abs(SashPos);

	if(FromRight)
	{
		if(GetSplitMode() == wxSPLIT_VERTICAL)
			SashPosition = size.x - NeededSashPos;
		else
			SashPosition = size.y - NeededSashPos;
	}
	else
		SashPosition = SashPos;
}

//-------------------------------------------------------------------------------------------------
void RichSplitter::OnSize(wxSizeEvent& event)
{
	if(event.GetId() != GetId())
	{
		event.Skip();
		return;
	}

	wxSize size = GetClientSize();

	if(FromRight)
	{
		if(GetSplitMode() == wxSPLIT_VERTICAL)
			SashPosition = size.x - NeededSashPos;
		else
			SashPosition = size.y - NeededSashPos;
	}
	else
		SashPosition = NeededSashPos;

	SetSashPosition(SashPosition, false);
	event.Skip();
}

//-------------------------------------------------------------------------------------------------
void RichSplitter::OnSplitterPosChanged(wxSplitterEvent& event)
{
	if(event.GetId() != GetId())	//ked je jeden splitter v druhom tak sa mi to stalo
	{
		event.Skip();
		return;
	}

	int NewPos = event.GetSashPosition();
	wxSize size = GetClientSize();
	int offset;

	if(GetSplitMode() == wxSPLIT_VERTICAL)
		offset = size.x - NewPos;
	else
		offset = size.y - NewPos;

	if(FromRight)
		NeededSashPos = offset;
	else
		NeededSashPos = NewPos;

	event.Skip();
}

//-------------------------------------------------------------------------------------------------
int RichSplitter::GetSashPosForSplit()
{
	if(FromRight)
		return -NeededSashPos;

	return NeededSashPos;
}

//-------------------------------------------------------------------------------------------------
int RichSplitter::GetSashPosFromSide()
{
	return NeededSashPos;
}

BEGIN_EVENT_TABLE(ColorEditPanelColorBox, wxPanel)
	 EVT_PAINT(ColorEditPanelColorBox::OnPaint)
	 EVT_MOTION(ColorEditPanelColorBox::OnMouseMove)
	 EVT_LEFT_DOWN(ColorEditPanelColorBox::OnLMBdown)
	 EVT_LEFT_UP(ColorEditPanelColorBox::OnLMBup)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------
ColorEditPanelColorBox::ColorEditPanelColorBox(wxWindow* parent, ColorEditPanelIntensityBox* IntensityBox, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	this->IntensityBox = IntensityBox;
	image = NULL;
	PointerX = 0;
	PointerY = 0;
//	SetBackgroundColour(wxColour(127, 127, 255));
	RECT CursorRect;
	GetClipCursor(&CursorRect); 
	ScreenClipRect.x = CursorRect.left;
	ScreenClipRect.y = CursorRect.top;
	ScreenClipRect.width = CursorRect.right - CursorRect.left;
	ScreenClipRect.height = CursorRect.bottom - CursorRect.top;
	GenerateSpectrumBitmap(size.x, size.y);
}

//-------------------------------------------------------------------------------------------------
ColorEditPanelColorBox::~ColorEditPanelColorBox()
{
	if(image)
	{
		delete image;
		image = NULL;
	}
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelColorBox::GenerateSpectrumBitmap(int width, int height)
{
	image = new wxImage(width, height);
	unsigned char* data = image->GetData();

	for(int x = 0; x < width; x++)
	{
		for(int y = 0; y < height; y++)
		{
			unsigned char* PixelR = data + ((3 * y * width) + x * 3);
			unsigned char* PixelG = PixelR + 1;
			unsigned char* PixelB = PixelG + 1;

			FRGB color = HLSMap((float)x / (float)width, (float)y / (float)height);

			color.r *= 255.0f;
			color.g *= 255.0f;
			color.b *= 255.0f;
			color.r = RoundUp2(color.r);
			color.g = RoundUp2(color.g);
			color.b = RoundUp2(color.b);

			*PixelR = (unsigned char)(color.r);
			*PixelG = (unsigned char)(color.g);
			*PixelB = (unsigned char)(color.b);
		}
	}
}

//---------------------------------------------------------------------------------------------
void ColorEditPanelColorBox::ClipCursorInside(bool OnOff)
{
	if(OnOff)
	{
		int left = 0;
		int top = 0;
		int right, bottom;
		ClientToScreen(&left, &top);
		GetClientSize(&right, &bottom);
		ClientToScreen(&right, &bottom);

		RECT rect;
		rect.left = left;
		rect.top = top;
		rect.right = right;
		rect.bottom = bottom;
		ClipCursor(&rect);
		return;
	}

	wxRect& OrgClipRect = ScreenClipRect;

	RECT rect;
	rect.left = OrgClipRect.x;
	rect.top = OrgClipRect.y;
	rect.right = rect.left + OrgClipRect.width;
	rect.bottom = rect.top + OrgClipRect.height;
	ClipCursor(&rect);
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelColorBox::SetPointer(int x, int y)
{
	PointerX = x;
	PointerY = y;
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelColorBox::OnPaint(wxPaintEvent& event)
{
	if(image && IsEnabled())
	{
 		wxPaintDC dc(this);
			wxBitmap bitmap(*image);
			dc.DrawBitmap(bitmap, wxPoint(0,0));

			dc.DrawLine(PointerX - 10, PointerY, PointerX + 11, PointerY);
			dc.DrawLine(PointerX, PointerY - 10, PointerX, PointerY + 11);
	}

	event.Skip();
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelColorBox::OnMouseMove(wxMouseEvent& event)
{
	if(event.ButtonIsDown(wxMOUSE_BTN_LEFT))
	{
		ColorEditPanel* MainPanel = (ColorEditPanel*)GetParent();
		wxSize size = GetClientSize();
		double hue = (double)event.GetX() / (double)(size.x - 1);
		double sat = (double)event.GetY() / (double)(size.y - 1);
		sat = 1.0 - sat;
		MainPanel->SetHueAndSaturation(hue, sat);
		SetPointer(event.GetX(), event.GetY());
		MainPanel->OnColorChanging();
		Refresh(false);
	}

	event.Skip();
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelColorBox::OnLMBdown(wxMouseEvent& event)
{
	ClipCursorInside(true);
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelColorBox::OnLMBup(wxMouseEvent& event)
{
	ClipCursorInside(false);
	ColorEditPanel* MainPanel = (ColorEditPanel*)GetParent();
	MainPanel->OnColorChanged();
}



BEGIN_EVENT_TABLE(ColorEditPanelIntensityBox, wxPanel)
	 EVT_PAINT(ColorEditPanelIntensityBox::OnPaint)
	 EVT_MOTION(ColorEditPanelIntensityBox::OnMouseMove)
	 EVT_LEFT_DOWN(ColorEditPanelIntensityBox::OnLMBdown)
	 EVT_LEFT_UP(ColorEditPanelIntensityBox::OnLMBup)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------
ColorEditPanelIntensityBox::ColorEditPanelIntensityBox(wxWindow* parent, ColorEditPanelPreviewBox* PreviewBox, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	this->PreviewBox = PreviewBox;
	image = NULL;
	ColorR = 0;
	ColorG = 0;
	ColorB = 0;
	PointerY = size.y / 2;
	UpdateCurrentColor();
	RECT CursorRect;
	GetClipCursor(&CursorRect); 
	ScreenClipRect.x = CursorRect.left;
	ScreenClipRect.y = CursorRect.top;
	ScreenClipRect.width = CursorRect.right - CursorRect.left;
	ScreenClipRect.height = CursorRect.bottom - CursorRect.top;
//	SetBackgroundColour(wxColour(255, 127, 127));
}

//-------------------------------------------------------------------------------------------------
ColorEditPanelIntensityBox::~ColorEditPanelIntensityBox()
{
	if(image)
	{
		delete image;
		image = NULL;
	}
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelIntensityBox::UpdateCurrentColor()
{
	if(image)
	{
		delete image;
		image = NULL;
	}

	ColorEditPanel* MainPanel = (ColorEditPanel*)GetParent();
	wxSize size = GetClientSize();
	image = new wxImage(size.x, size.y);
	unsigned char* data = image->GetData();

	double Hue = MainPanel->GetHue();
	double Lightness;
	double Saturation = MainPanel->GetSaturation();

	FRGB color;

	for(int y = 0; y < size.y; y++)
	{
		Lightness = (double)y / (double)size.y;
		Lightness = 1.0f - Lightness;

		color = HLS2RGB(Hue, Lightness, Saturation);

		if(color.r > 1.0f || color.g > 1.0f || color.b > 1.0f)
			color.r = color.r;

		if(color.r < 0.0f || color.g < 0.0f || color.b < 0.0f)
			color.r = color.r;

		color.r *= 255.0f;
		color.g *= 255.0f;
		color.b *= 255.0f;
		color.r = RoundUp2(color.r);
		color.g = RoundUp2(color.g);
		color.b = RoundUp2(color.b);

		unsigned char red = (unsigned char)(color.r);
		unsigned char green = (unsigned char)(color.g);
		unsigned char blue = (unsigned char)(color.b);

		for(int x = 0; x < size.x; x++)
		{
			unsigned char* PixelR = data + ((3 * y * size.x) + x * 3);
			unsigned char* PixelG = PixelR + 1;
			unsigned char* PixelB = PixelG + 1;

			*PixelR = red;
			*PixelG = green;
			*PixelB = blue;
		}
	}
	Refresh(false);
}

//---------------------------------------------------------------------------------------------
void ColorEditPanelIntensityBox::ClipCursorInside(bool OnOff)
{
	if(OnOff)
	{
		int left = 0;
		int top = 0;
		int right, bottom;
		ClientToScreen(&left, &top);
		GetClientSize(&right, &bottom);
		ClientToScreen(&right, &bottom);

		RECT rect;
		rect.left = left;
		rect.top = top;
		rect.right = right;
		rect.bottom = bottom;
		ClipCursor(&rect);
		return;
	}

	wxRect& OrgClipRect = ScreenClipRect;

	RECT rect;
	rect.left = OrgClipRect.x;
	rect.top = OrgClipRect.y;
	rect.right = rect.left + OrgClipRect.width;
	rect.bottom = rect.top + OrgClipRect.height;
	ClipCursor(&rect);
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelIntensityBox::SetPointer(int y)
{
	PointerY = y;
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelIntensityBox::OnPaint(wxPaintEvent& event)
{
	if(image && IsEnabled())
	{
		wxSize size = GetClientSize();

 		wxPaintDC dc(this);
		wxBitmap bitmap(*image);
		dc.DrawBitmap(bitmap, wxPoint(0,0));
		dc.DrawLine(0, PointerY, size.x, PointerY);
	}

	event.Skip();
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelIntensityBox::OnMouseMove(wxMouseEvent& event)
{
	if(event.ButtonIsDown(wxMOUSE_BTN_LEFT))
	{
		ColorEditPanel* MainPanel = (ColorEditPanel*)GetParent();
		wxSize size = GetClientSize();
		double Lightness = (double)event.GetY() / (double)(size.y - 1);
		Lightness = 1.0 - Lightness;
		MainPanel->SetLightness(Lightness);
		SetPointer(event.GetY());
		MainPanel->OnColorChanging();
		Refresh(false);
	}

	event.Skip();
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelIntensityBox::OnLMBdown(wxMouseEvent& event)
{
	ClipCursorInside(true);
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelIntensityBox::OnLMBup(wxMouseEvent& event)
{
	ClipCursorInside(false);
	ColorEditPanel* MainPanel = (ColorEditPanel*)GetParent();
	MainPanel->OnColorChanged();
}

BEGIN_EVENT_TABLE(ColorEditPanelPreviewBox, wxPanel)
	 EVT_PAINT(ColorEditPanelPreviewBox::OnPaint)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------
ColorEditPanelPreviewBox::ColorEditPanelPreviewBox(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	ColorR = 0;
	ColorG = 0;
	ColorB = 0;
//	SetBackgroundColour(wxColour(255, 127, 127));
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelPreviewBox::SetColor(unsigned char red, unsigned char green, unsigned char blue)
{
	ColorR = red;
	ColorG = green;
	ColorB = blue;
	Refresh(false);
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanelPreviewBox::OnPaint(wxPaintEvent& event)
{
	if(IsEnabled())
	{
		wxSize size = GetClientSize();

 		wxPaintDC dc(this);
		dc.SetBrush(wxBrush(wxColour(ColorR, ColorG, ColorB), wxSOLID));
		dc.DrawRectangle(wxPoint(0, 0), size);
	}

	event.Skip();
}

BEGIN_EVENT_TABLE(ColorEditPanel, wxPanel)
	 EVT_TEXT_ENTER(-1, ColorEditPanel::OnTextEnter)
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------
ColorEditPanel::ColorEditPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	hue = 0.0;
	lightness = 0.0;
	saturation = 0.0;

//	SetBackgroundColour(wxColour(200, 200, 200));

	PreviewBox = new ColorEditPanelPreviewBox(this, -1, wxDefaultPosition, wxSize(64, 64), wxTAB_TRAVERSAL|wxSTATIC_BORDER);
	IntensityBox = new ColorEditPanelIntensityBox(this, PreviewBox, -1, wxDefaultPosition, wxSize(32, 258), wxTAB_TRAVERSAL|wxSTATIC_BORDER);
	ColorBox = new ColorEditPanelColorBox(this, IntensityBox, -1, wxDefaultPosition, wxSize(258, 258), wxTAB_TRAVERSAL|wxSTATIC_BORDER);	
	RedEditBox = new LabeledTextCtrl(this, TEXTCTRL_RED, wxDefaultPosition, wxSize(52, 18), wxSTATIC_BORDER, wxPoint(-10, 1), wxString("R"), VarType_Int, "0", "255");
	GreenEditBox = new LabeledTextCtrl(this, TEXTCTRL_GREEN, wxDefaultPosition, wxSize(52, 18), wxSTATIC_BORDER, wxPoint(-10, 1), wxString("G"), VarType_Int, "0", "255");
	BlueEditBox = new LabeledTextCtrl(this, TEXTCTRL_BLUE, wxDefaultPosition, wxSize(52, 18), wxSTATIC_BORDER, wxPoint(-10, 1), wxString("B"), VarType_Int, "0", "255");
	AlphaEditBox = new LabeledTextCtrl(this, TEXTCTRL_ALPHA, wxDefaultPosition, wxSize(52, 18), wxSTATIC_BORDER, wxPoint(-10, 1), wxString("A"), VarType_Int, "0", "255");

	HueEditBox = new LabeledTextCtrl(this, TEXTCTRL_HUE, wxDefaultPosition, wxSize(52, 18), wxSTATIC_BORDER, wxPoint(-10, 1), wxString("H"), VarType_Float, "0", "360");
	LightnessEditBox = new LabeledTextCtrl(this, TEXTCTRL_LIGHTNESS, wxDefaultPosition, wxSize(52, 18), wxSTATIC_BORDER, wxPoint(-10, 1), wxString("L"), VarType_Float, "0", "1");
	SaturationEditBox = new LabeledTextCtrl(this, TEXTCTRL_SATURATION, wxDefaultPosition, wxSize(52, 18), wxSTATIC_BORDER, wxPoint(-10, 1), wxString("S"), VarType_Float, "0", "1");

	wxBoxSizer* VertSizer = new wxBoxSizer(wxVERTICAL);
	VertSizer->Add(PreviewBox, 0, wxALIGN_LEFT | wxLEFT, 0);
	VertSizer->AddSpacer(10);
	VertSizer->Add(RedEditBox, 0, wxALIGN_LEFT | wxLEFT, 10);
	VertSizer->AddSpacer(5);
	VertSizer->Add(GreenEditBox, 0, wxALIGN_LEFT | wxLEFT, 10);
	VertSizer->AddSpacer(5);
	VertSizer->Add(BlueEditBox, 0, wxALIGN_LEFT | wxLEFT, 10);
	VertSizer->AddSpacer(5);
	VertSizer->Add(AlphaEditBox, 0, wxALIGN_LEFT | wxLEFT, 10);
	VertSizer->AddSpacer(32);
	VertSizer->Add(HueEditBox, 0, wxALIGN_LEFT | wxLEFT, 10);
	VertSizer->AddSpacer(5);
	VertSizer->Add(LightnessEditBox, 0, wxALIGN_LEFT | wxLEFT, 10);
	VertSizer->AddSpacer(5);
	VertSizer->Add(SaturationEditBox, 0, wxALIGN_LEFT | wxLEFT, 10);

	wxBoxSizer* MainSizer = new wxBoxSizer(wxHORIZONTAL);
	MainSizer->Add(ColorBox, 0, wxALIGN_TOP | wxLEFT, 0);
	MainSizer->AddSpacer(5);
	MainSizer->Add(IntensityBox, 0, wxALIGN_TOP | wxLEFT, 0);
	MainSizer->AddSpacer(5);
	MainSizer->Add(VertSizer, 0, wxALIGN_TOP | wxLEFT, 0);

	SetSizer(MainSizer);
	Layout();
//	MainSizer->Fit(this);
}

//-------------------------------------------------------------------------------------------------
ColorEditPanel::~ColorEditPanel()
{

}

//-------------------------------------------------------------------------------------------------
void ColorEditPanel::UpdateCurrentColor()
{
	IntensityBox->UpdateCurrentColor();

	FRGB color = HLS2RGB(hue, lightness, saturation);

	color.r *= 255.0f;
	color.g *= 255.0f;
	color.b *= 255.0f;
	color.r = RoundUp2(color.r);
	color.g = RoundUp2(color.g);
	color.b = RoundUp2(color.b);

	unsigned char red = (unsigned char)(color.r);
	unsigned char green = (unsigned char)(color.g);
	unsigned char blue = (unsigned char)(color.b);

	PreviewBox->SetColor(red, green, blue);

	RedEditBox->SetValue(ItoA(red));
	GreenEditBox->SetValue(ItoA(green));
	BlueEditBox->SetValue(ItoA(blue));

	HueEditBox->SetValue(FtoA(hue * 360.0));
	LightnessEditBox->SetValue(FtoA(lightness));
	SaturationEditBox->SetValue(FtoA(saturation));
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanel::SetHue(double val)
{
	hue = val;
	UpdateCurrentColor();
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanel::SetLightness(double val)
{
	lightness = val;
	UpdateCurrentColor();
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanel::SetSaturation(double val)
{
	saturation = val;
	UpdateCurrentColor();
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanel::SetHueAndSaturation(double hue, double sat)
{
	this->hue = hue;
	this->saturation = sat;
	UpdateCurrentColor();
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanel::SetColor(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
{
/*	red = 43;
	green = 158;
	blue = 111;*/

	HLS col = RGB2HLS(red, green, blue);	//prevod na HLS model

	hue = col.Hue / 360.0;
	lightness = col.Lightness;
	saturation = col.Saturation;
/*
	FRGB color = HLS2RGB(hue, lightness, saturation);
	float fred = color.r * 255.0f;
	float fgreen = color.g * 255.0f;
	float fblue = color.b * 255.0f;
*/
	wxSize size = ColorBox->GetClientSize();
	int x = (int)(hue * (double)size.x);
	int y = (int)(saturation * (double)size.y);
	y = size.y - y;
	int z = (int)(lightness * (double)size.y);
	z = size.y - z;

	ColorBox->SetPointer(x, y);
	IntensityBox->SetPointer(z);
	UpdateCurrentColor();
	AlphaEditBox->SetValue(ItoA(alpha));
}

//-------------------------------------------------------------------------------------------------
void ColorEditPanel::SetColor(double Hue, double Lightness, double Saturation)
{
	hue = Hue;
	lightness = Lightness;
	saturation = Saturation;

	wxSize size = ColorBox->GetClientSize();
	int x = (int)(hue * (double)size.x);
	int y = (int)(saturation * (double)size.y);
	y = size.y - y;
	int z = (int)(lightness * (double)size.y);
	z = size.y - z;

	ColorBox->SetPointer(x, y);
	IntensityBox->SetPointer(z);
	UpdateCurrentColor();
//	AlphaEditBox->SetValue(ItoA(alpha));
}

//----------------------------------------------------------------------------------------
void ColorEditPanel::SetColor(unsigned char red, unsigned char green, unsigned char blue)
{
	unsigned char alpha = AtoI(AlphaEditBox->GetValue());
	SetColor(red, green, blue, alpha);
}
/*
//----------------------------------------------------------------------------------------
FRGB ColorEditPanel::GetColor()
{
	FRGB color = HLS2RGB(hue, lightness, saturation);
	unsigned char alpha = AtoI(AlphaEditBox->GetValue());

	FRGBA res;
	res.r = color.r;
	res.g = color.g;
	res.b = color.b;
	res.a = (float)alpha / 255.0f; 
	return res;
}*/

//----------------------------------------------------------------------------------------
wxString ColorEditPanel::GetFRGBAColorAsString()
{
	FRGB color = HLS2RGB(hue, lightness, saturation);
	unsigned char alpha = AtoI(AlphaEditBox->GetValue());

	return FtoA(color.r) + " " + FtoA(color.g) + " " + FtoA(color.b) + " " + FtoA((float)alpha / 255.0f);
}

//----------------------------------------------------------------------------------------
void ColorEditPanel::OnColorChanging()
{
}

//----------------------------------------------------------------------------------------
void ColorEditPanel::OnColorChanged()
{
}

//----------------------------------------------------------------------------------------
void ColorEditPanel::OnTextEnter(wxCommandEvent& event)
{
	wxString val = event.GetString();
	int id = event.GetId();
	wxString bb = BlueEditBox->GetValue();

	if(id == TEXTCTRL_RED || id == TEXTCTRL_GREEN || id == TEXTCTRL_BLUE)
	{
		unsigned char red = AtoI(RedEditBox->GetValue());
		unsigned char green = AtoI(GreenEditBox->GetValue());
		unsigned char blue = AtoI(BlueEditBox->GetValue());
		SetColor(red, green, blue);
		ColorBox->Refresh(false);
	}

	if(id == TEXTCTRL_HUE || id == TEXTCTRL_LIGHTNESS || id == TEXTCTRL_SATURATION)
	{
		double HUE, LIGHTNESS, SATURATION;
		HueEditBox->GetValue().ToDouble(&HUE);
		LightnessEditBox->GetValue().ToDouble(&LIGHTNESS);
		SaturationEditBox->GetValue().ToDouble(&SATURATION);
		SetColor(HUE / 360.0, LIGHTNESS, SATURATION);
		ColorBox->Refresh(false);
	}
	
	OnColorChanged();


/*	switch(event.GetId())
	{
	case TEXTCTRL_RED:
		break;
	case TEXTCTRL_GREEN:
		break;
	case TEXTCTRL_BLUE:
		break;
	case TEXTCTRL_ALPHA:
		break;
	}*/
}

BEGIN_EVENT_TABLE(SliderEdit, wxPanel)
	EVT_TEXT_ENTER(-1, SliderEdit::OnTextEnter)							//user nieco zadal do textoveho pola alebo do comba
	EVT_COMBOBOX(-1, SliderEdit::OnSetFromCombo)						//bolo nieco vybrane z listboxu comba
	EVT_COMMAND_SCROLL_THUMBTRACK(-1, SliderEdit::OnSliderTick)	//frekventovane eventy pocas toho co user taha slider
	EVT_COMMAND_SCROLL_ENDSCROLL(-1, SliderEdit::OnSliderRelease)	// - iba ked user pusti slider alebo ho posuva sipkami. koli sipkam tu musi byt
END_EVENT_TABLE()

//-------------------------------------------------------------------------------------------------------------------
SliderEdit::SliderEdit(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, wxArrayString& choices, bool ComboBox, const int InpType, wxString value, wxString MinValue, wxString MaxValue, wxString TickValue, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	slider = NULL;
	combo = NULL;
	editbox = NULL;

	this->MinValue = MinValue;
	this->MaxValue = MaxValue;
	this->InpType = InpType;
	this->TickValue = TickValue;

	int MinVal, MaxVal, val;

	if(InpType == VarType_Int)
	{
		MinVal = AtoI(MinValue);
		MaxVal = AtoI(MaxValue);
		val = AtoI(value);
	}
	else if(InpType == VarType_Float)
	{
		float min = taAtoF(MinValue.c_str());
		float max = taAtoF(MaxValue.c_str());
		float tick = taAtoF(TickValue.c_str());

		float range = max - min;
		float fNumTicks = range / tick;
		float fval = AtoF(value) - min;
		fval /= range;
		MinVal = 0;
		MaxVal = (int)fNumTicks;
		val = int(fval * fNumTicks);
	}

	slider = new wxSlider(this, wxID_ANY, val, MinVal, MaxVal, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL | wxSL_AUTOTICKS/* | wxSL_LABELS*/, wxDefaultValidator, wxString("label"));

	if(ComboBox)
		combo = new RichComboBox(this, wxID_ANY, "", wxDefaultPosition, wxSize(-1, 18), choices, wxCB_DROPDOWN | wxCB_SORT, InpType, MinValue, MaxValue, wxDefaultValidator, "combobox", false);
	else
		editbox = new LabeledTextCtrl(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 20), 0, wxPoint(0, 0), wxString(""), InpType, MinValue, MaxValue);

	wxBoxSizer* MainSizer = new wxBoxSizer(wxVERTICAL);
	if(combo)
		MainSizer->Add(combo, 0, wxGROW | wxALIGN_LEFT | wxALL, 0);
	else
		MainSizer->Add(editbox, 0, wxGROW | wxALIGN_LEFT | wxALL, 0);

	MainSizer->AddSpacer(4);
	MainSizer->Add(slider, 0, wxGROW | wxALIGN_LEFT | wxALL, 0);

	SetSizer(MainSizer);
	Layout();
}

//-------------------------------------------------------------------------------------------------------------------
SliderEdit::~SliderEdit()
{

}

//-------------------------------------------------------------------------------------------------------------------
bool SliderEdit::IsAnyChoice(wxString value)
{
	if(combo && combo->FindString(value) >= 0)
		return true;

	return false;
}

//-------------------------------------------------------------------------------------------------------------------
wxString SliderEdit::SetValue(wxString value)
{
	if(InpType == VarType_Int)
	{
		if(value.IsNumber() == false)
			value = MinValue;
		else
			ClampIntValue(value, MinValue, MaxValue);

		slider->SetValue(AtoI(value));
	}
	else if(InpType == VarType_Float)
	{
		if(IsFloatNumber(value.c_str()) == false)
			value = MinValue;
		else
			ClampFloatValue(value, MinValue, MaxValue);

		int MinVal, MaxVal, val;

		float min = taAtoF(MinValue.c_str());
		float max = taAtoF(MaxValue.c_str());
		float tick = taAtoF(TickValue.c_str());

		float range = max - min;
		float fNumTicks = range / tick;
		float fval = taAtoF(value.c_str()) - min;
		fval /= range;
		MinVal = 0;
		MaxVal = (int)fNumTicks;
		val = int(fval * fNumTicks);

		slider->SetRange(MinVal, MaxVal);
		slider->SetValue(val);

		/*float val = (float)atof(value);
		float min = taAtoF(MinValue.c_str());
		float max = taAtoF(MaxValue.c_str());
		float tick = taAtoF(TickValue.c_str());

		float range = max - min;
		float RangeCenter = range * 0.5f;
		val += RangeCenter;
		float fNumTicks = range / tick;
		int MinVal = 0;
		int MaxVal = (int)fNumTicks;

		float ratepos = val / range;
		int CurPos = int(ratepos * fNumTicks);

		slider->SetRange(MinVal, MaxVal);
		slider->SetValue(CurPos);
*/
	}

	if(combo)
		combo->SetValue(value);
	else
		editbox->SetValue(value);

	return value;
}

//--------------------------------------------------------------------------------------------------------------
void SliderEdit::OnSetFromCombo(wxCommandEvent& event)
{
	slider->SetValue(0);
	event.Skip();
	OnValueChanged(event.GetString());
}

//--------------------------------------------------------------------------------------------------------------
void SliderEdit::OnTextEnter(wxCommandEvent& event)
{
	wxString OrginalString;

	if(combo)
		OrginalString = combo->GetValue();
	else
		OrginalString = editbox->GetValue();

	wxString HandledValue = SetValue(OrginalString);
	event.SetString(HandledValue);
	OnValueChanged(HandledValue);
	event.Skip();
}

//--------------------------------------------------------------------------------------------------------------
void SliderEdit::UpdateFromTick()
{
	wxString value = ItoA(slider->GetValue());

	if(InpType == VarType_Int)
	{
		ClampIntValue(value, MinValue, MaxValue);

		if(combo)
			combo->SetValue(value);
		else
			editbox->SetValue(value);
	}
	else if(InpType == VarType_Float)
	{
		float rate = (float)atof(value) / (float)slider->GetMax();
		float MinVal = atof(MinValue.c_str());
		float MaxVal = atof(MaxValue.c_str());
		float range = MaxVal - MinVal;

		float val = MinVal + (rate * range);
		value = FtoA(val);

		if(combo)
			combo->SetValue(value);
		else
			editbox->SetValue(value);
	}
}

//--------------------------------------------------------------------------------------------------------------
void SliderEdit::OnSliderTick(wxScrollEvent& event)
{
	UpdateFromTick();
	wxString value;

	if(combo)
		value = combo->GetValue();
	else
		value = editbox->GetValue();

	OnValueChanging(value);
	event.Skip();
}

//--------------------------------------------------------------------------------------------------------------
void SliderEdit::OnSliderRelease(wxScrollEvent& event)
{
	UpdateFromTick();
	wxString value;

	if(combo)
		value = combo->GetValue();
	else
		value = editbox->GetValue();

	OnValueChanged(value);
	event.Skip();
}

//--------------------------------------------------------------------------------------------------------------
void SliderEdit::OnValueChanged(wxString value)
{
	int ff = 0;
}

//--------------------------------------------------------------------------------------------------------------
void SliderEdit::OnValueChanging(wxString value)
{
}

BEGIN_EVENT_TABLE(FlagsEditPanel, wxPanel)
	EVT_CHECKBOX(-1, FlagsEditPanel::OnCheckboxSwitch)						//bol prepnuty checkbox
END_EVENT_TABLE()

//--------------------------------------------------------------------------------------------------------------
FlagsEditPanel::FlagsEditPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, wxArrayString& flags, wxString value, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	this->flags = AtoI(value);
	int CurFlag = 1;
//	SetBackgroundColour(wxColour(255, 0, 0));
	wxBoxSizer* MainSizer = new wxBoxSizer(wxVERTICAL);

	for(unsigned int n = 0; n < flags.GetCount(); n++)
	{
		wxCheckBox* checkbox = new wxCheckBox(this, n + 1, flags[n], wxDefaultPosition, wxDefaultSize, 0);

		if(this->flags & CurFlag)
			checkbox->SetValue(true);

		MainSizer->Add(checkbox, 0, wxALIGN_TOP|wxTOP, 5);
		CurFlag *= 2;
	}

	SetSizer(MainSizer);
	Layout();
}

//--------------------------------------------------------------------------------------------------------------
wxString FlagsEditPanel::GetValue()
{
	return ItoA(flags);
}

//--------------------------------------------------------------------------------------------------------------
void FlagsEditPanel::OnValueChanged(wxString value)
{
}

//--------------------------------------------------------------------------------------------------------------
void FlagsEditPanel::OnCheckboxSwitch(wxCommandEvent& event)
{
	int id = event.GetId();
	int flag = id - 1;

	flag = (1 << flag);

	if(event.GetInt())
		flags |= flag;
	else
		flags = flags & ~flag;

	OnValueChanged(ItoA(flags));
//	event.Skip();
}


//--------------------------------------------------------------------------------------------------------------
PropEditComboBox::PropEditComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style, const int InpType, wxString MinValue, wxString MaxValue) : RichComboBox(parent, id, value, pos, size, n, choices, style, InpType, MinValue, MaxValue, true)
{

}

//--------------------------------------------------------------------------------------------------------------
PropEditComboBox::~PropEditComboBox()
{
	for(int n = 0; n < (int)this->GetCount(); n++)
	{
		wxString* Val = (wxString*)this->GetClientData(n);

		if(Val)
		{
			delete Val;
			Val = NULL;
		}
	}
}