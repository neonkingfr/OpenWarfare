
#include "TextboxWithButton.h"
#include "wx/bitmap.h"
#include "wx/bmpbuttn.h"

DEFINE_EVENT_TYPE(wxEVT_FILETEXTBOX)

BEGIN_EVENT_TABLE(TextboxWithButton, wxPanel)
	EVT_BUTTON(-1, TextboxWithButton::OnButtonClick)
	EVT_TEXT_ENTER(-1, TextboxWithButton::OnTextEnter)
	EVT_SIZE(TextboxWithButton::OnSize)
END_EVENT_TABLE()

//====================================================================================
//==== abstract class for other controls like FileTextBox ============================
//====================================================================================
TextboxWithButton::TextboxWithButton(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, int TextBoxStyle, char** ButtonXPMBitmap) : wxPanel(parent, id, pos, size)
{
	Parent = parent;
	this->SetMinSize(wxSize(60, size.y));

	Combo = NULL;
	TextBox = new wxTextCtrl(this, -1, wxT(""), wxPoint(0, 0), wxSize(size.x - size.y, size.y), TextBoxStyle);

	if(ButtonXPMBitmap)
	{
		ButtonBitmap = new wxBitmap( ButtonXPMBitmap );
		button = new wxBitmapButton(this, -1, *ButtonBitmap, wxPoint(size.x - 17, 0), wxSize(16, 16), wxBU_AUTODRAW, wxDefaultValidator, "");
	}
	else
	{
		ButtonBitmap = NULL;
		button = new wxButton(this, -1, wxEmptyString, wxPoint(size.x - size.y, 0), wxSize(size.y, size.y), 0);
	}
}

//----------------------------------------------------------------------------------------
TextboxWithButton::TextboxWithButton(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, wxArrayString& choices, int ComboBoxStyle, char** ButtonXPMBitmap) : wxPanel(parent, id, pos, size)
{
	Parent = parent;
	this->SetMinSize(wxSize(60, size.y));

	TextBox = NULL;
	Combo = new RichComboBox(this, -1, wxT(""), wxPoint(0, 0), wxSize(size.x - size.y, size.y), choices, ComboBoxStyle, VarType_String, "", "", wxDefaultValidator, "combobox", false);

	if(ButtonXPMBitmap)
	{
		ButtonBitmap = new wxBitmap( ButtonXPMBitmap );
		button = new wxBitmapButton(this, -1, *ButtonBitmap, wxPoint(size.x - 17, 0), wxSize(16, 16), wxBU_AUTODRAW, wxDefaultValidator, "");
	}
	else
	{
		ButtonBitmap = NULL;
		button = new wxButton(this, -1, wxEmptyString, wxPoint(size.x - size.y, 0), wxSize(size.y, size.y), 0);
	}
}

//----------------------------------------------------------------------------------------
TextboxWithButton::~TextboxWithButton()
{
	if(ButtonBitmap)
	{
		delete ButtonBitmap;
		ButtonBitmap = NULL;
	}
}

//----------------------------------------------------------------------------------------
wxString TextboxWithButton::GetValue()
{
	if(TextBox)
		return TextBox->GetValue();

	return Combo->GetValue();
}

//----------------------------------------------------------------------------------------
void TextboxWithButton::SetValue(wxString value)
{
	if(TextBox)
		TextBox->SetValue(value);
	else
		Combo->SetValue(value);
}

//----------------------------------------------------------------------------------------
wxWindow* TextboxWithButton::GetEditControl()
{
	if(TextBox)
		return TextBox;

	return Combo;
}

//----------------------------------------------------------------------------------------
bool TextboxWithButton::IsAnyChoice(wxString value)
{
	if(Combo && Combo->FindString(value) >= 0)
		return true;

	return false;
}

//----------------------------------------------------------------------------------------
void TextboxWithButton::OnSize(wxSizeEvent &event)
{
	wxSize size = GetClientSize();
	wxSize ButtonSize = button->GetSize();

	wxWindow* EditControl = GetEditControl();
	EditControl->SetSize(size.x - ButtonSize.y, ButtonSize.y);
	button->Move(size.x - ButtonSize.y, 0);

/*	TextBoxSizerItem->SetInitSize(size.x - 18, 16);
	TextBoxSizerItem->SetDimension(wxPoint(0, 0), wxSize(size.x - 18, 16));
	MainSizer->Layout();
	MainSizer->SetDimension(0, 0, size.x, size.y);
	MainSizer->RecalcSizes();
	MainSizer->Layout();*/
	event.Skip();
}

//----------------------------------------------------------------------------------------
void TextboxWithButton::OnButtonClick(wxCommandEvent& event)
{
}

//----------------------------------------------------------------------------------------
void TextboxWithButton::OnTextEnter(wxCommandEvent& event)
{
	wxCommandEvent eventCustom(wxEVT_FILETEXTBOX);
	eventCustom.SetId(this->GetId());
	eventCustom.SetEventObject(this);
	wxPostEvent(this, eventCustom);
//	event.Skip();
}