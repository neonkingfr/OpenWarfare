
#include "Validators.h"
#include "Common/WxConvert.h"
#include "Common/NativeUtil.h"
#include "wx/wx.h"

//----------------------------------------------------------------------------------------
IntValidator::IntValidator(int MinValue, int MaxValue)
{
	this->MinValue = MinValue;
	this->MaxValue = MaxValue;
}

//----------------------------------------------------------------------------------------
wxObject* IntValidator::Clone() const
{
	return new IntValidator(MinValue, MaxValue);
}

//----------------------------------------------------------------------------------------
bool IntValidator::Validate(wxWindow* parent)
{
	wxWindow* win = GetWindow();

	//TODO: potom dopisat aj pre ine typy nez wxTextCtrl
	if(win->IsKindOf(CLASSINFO(wxTextCtrl)))
	{
		wxTextCtrl* TextCtrl = (wxTextCtrl*)win;

		wxString sval = TextCtrl->GetValue();

		if(IsIntNumber(sval.c_str()) == false)
			return false;

		int val = AtoI(sval);

		if(val > MaxValue || val < MinValue)
			return false;
	}

	return true;	//bud bolo odvalidovano ze hodnota je v poriadku alebo typ okna je neznamy tak sa nevaliduje vobec
}

//=========================================================================================
EnumIntValidator::EnumIntValidator(int MinValue, int MaxValue)
	:IntValidator(MinValue, MaxValue)
{
}

//----------------------------------------------------------------------------------------
void EnumIntValidator::AddEnum(const char* str)
{
	assert(str);

	if(str)
		enums.Add(str);
}

//----------------------------------------------------------------------------------------
wxObject* EnumIntValidator::Clone() const
{
	EnumIntValidator* val = new EnumIntValidator(MinValue, MaxValue);

	for(uint n = 0; n < enums.GetCount(); n++)
		val->AddEnum(enums[n].c_str());

	return val;
}

//----------------------------------------------------------------------------------------
bool EnumIntValidator::Validate(wxWindow* parent)
{
	wxWindow* win = GetWindow();

	//TODO: potom dopisat aj pre ine typy nez wxTextCtrl
	if(win->IsKindOf(CLASSINFO(wxTextCtrl)))
	{
		wxTextCtrl* TextCtrl = (wxTextCtrl*)win;

		wxString sval = TextCtrl->GetValue();

		for(uint n = 0; n < enums.GetCount(); n++)
		{
			if(enums[n] == sval)
				return true;
		}
	}

	return IntValidator::Validate(parent);
}

//----------------------------------------------------------------------------------------
FloatValidator::FloatValidator(float MinValue, float MaxValue)
{
	this->MinValue = MinValue;
	this->MaxValue = MaxValue;
}

//----------------------------------------------------------------------------------------
wxObject* FloatValidator::Clone() const
{
	return new FloatValidator(MinValue, MaxValue);
}

//----------------------------------------------------------------------------------------
bool FloatValidator::Validate(wxWindow* parent)
{
	wxWindow* win = GetWindow();

	//TODO: potom dopisat aj pre ine typy nez wxTextCtrl
	if(win->IsKindOf(CLASSINFO(wxTextCtrl)))
	{
		wxTextCtrl* TextCtrl = (wxTextCtrl*)win;

		wxString sval = TextCtrl->GetValue();

		if(IsFloatNumber(sval.c_str()) == false)
			return false;

		float val = AtoF(sval);

		if(val > MaxValue || val < MinValue)
			return false;
	}

	return true;	//bud bolo odvalidovano ze hodnota je v poriadku alebo typ okna je neznamy tak sa nevaliduje vobec
}

/*
//----------------------------------------------------------------------------------------
FileValidator::FileValidator(bool DirectoryFiles)
{
	this->DirectoryFiles = DirectoryFiles;
}

//----------------------------------------------------------------------------------------
wxObject* FileValidator::Clone() const
{
	FileValidator* fv = new FileValidator(DirectoryFiles);

	for(uint n = 0; n < extensions.GetCount(); n++)
		fv->AddExtension(extensions[n].c_str());

	return fv;
}

//----------------------------------------------------------------------------------------
bool FileValidator::Validate(wxWindow* parent)
{
	wxWindow* win = GetWindow();

	//TODO: potom dopisat aj pre ine typy nez wxTextCtrl
	if(win->IsKindOf(CLASSINFO(wxTextCtrl)))
	{
		wxTextCtrl* TextCtrl = (wxTextCtrl*)win;
		wxString file = TextCtrl->GetValue();

		if(g_EngineImpl->FileOrDirExist(file.c_str()) == false)
			return false;

		bool IsDirectory = g_EngineImpl->IsDirectory(file.c_str());

		if(IsDirectory)
		{
			if(DirectoryFiles == false)
				return false;
		}
		else
		{
			if(!extensions.IsEmpty())	//fajl musi mat nejaku koncovku
			{
				wxString ext = file.AfterLast('.');
			
				if(ext.IsEmpty())
					return false;

				return (extensions.Index(ext.c_str()) >= 0);
			}
		}
	}
	return true;
}

//----------------------------------------------------------------------------------------
void FileValidator::AddExtension(const char* str)
{
	assert(str);

	if(str)
		extensions.Add(str);
}
*/