
#ifndef WB_PROPERTY_GRID_H
#define WB_PROPERTY_GRID_H

#ifndef WORKBENCH
	#include "common/enf_mathlib.h"
#endif

#include "wx/propgrid/propgrid.h"
#include <wx/propgrid/propdev.h>
#include "wx/propgrid/advprops.h"

//#include "wx/clntdata.h"

class RichFileDialog;
class UniversalBaseDialog;

#ifndef WORKBENCH
WX_PG_DECLARE_STRING_PROPERTY(WBFileProperty)			//custom property na vyber a validaciu fajlu s dialogom na buttone
WX_PG_DECLARE_STRING_PROPERTY(WBCustomDialogProperty)	//podobne ale pre univerzalne dialogy so stringovym vstupom a vystupom. read only
#endif

//--------- EVT_WORKBENCH_PROPGRID event declaration START ------------------
BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_WORKBENCH_PROPGRID, 7780)
END_DECLARE_EVENT_TYPES()


#define EVT_WORKBENCH_PROPGRID(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY( \
        wxEVT_WORKBENCH_PROPGRID, id, -1, \
        (wxObjectEventFunction)(wxEventFunction)(wxCommandEventFunction)&fn, \
        (wxObject *) NULL \
    ),
//--------- EVT_WORKBENCH_PROPGRID event declaration END ------------------

enum PGCustomPropertyType
{
	CPT_VectorProperty = 1,
	CPT_FileProperty,
	CPT_CustomDialogProperty,
	CPT_ComboBoxProperty,
	CPT_EditBoxProperty
};

enum PGPropertyValueType
{
	PVT_UNKNOWN = 0,
	PVT_BOOL,
	PVT_INT,
//	PVT_INT_ARRAY,
	PVT_FLOAT,
	PVT_STRING,
//	PVT_STRING_ARRAY,
	PVT_VECTOR,
	PVT_COLOR,
};



//=====================================================================================
//v buducnu by tu mohli byt rozne values typu ArrayString a podobne takze s hodnotami zostaneme otvoreni lubovolnym novym
class PGPropertyValue
{
public:
	PGPropertyValue(PGPropertyValueType type)
	{
		this->type = type;
	}
	inline PGPropertyValueType GetType() const {return type;}
private:
	PGPropertyValueType type;
};

//=====================================================================================
class PGPropertyValueBool : public PGPropertyValue
{
public:
	PGPropertyValueBool(bool value) : PGPropertyValue(PVT_BOOL)
	{
		this->value = value;
	}
	inline bool GetValue() const {return value;}
private:
	bool value;
};

//=====================================================================================
class PGPropertyValueInt : public PGPropertyValue
{
public:
	PGPropertyValueInt(int value) : PGPropertyValue(PVT_INT)
	{
		this->value = value;
	}
	inline int GetValue() const {return value;}
private:
	int value;
};

//=====================================================================================
class PGPropertyValueFloat : public PGPropertyValue
{
public:
	PGPropertyValueFloat(int value) : PGPropertyValue(PVT_FLOAT)
	{
		this->value = value;
	}
	inline float GetValue() const {return value;}
private:
	float value;
};

//=====================================================================================
class PGPropertyValueString : public PGPropertyValue
{
public:
	PGPropertyValueString(wxString& value) : PGPropertyValue(PVT_STRING)
	{
		this->value = value;
	}
	inline const char* GetValue() const {return value;}
private:
	wxString value;
};

//=====================================================================================
class PGPropertyValueVector : public PGPropertyValue
{
public:
	PGPropertyValueVector(const Vector3& value) : PGPropertyValue(PVT_VECTOR)
	{
		this->value = value;
	}
	inline  const Vector3& GetValue() const {return value;}
private:
	Vector3 value;
};

//=====================================================================================
class PGPropertyValueColor : public PGPropertyValue
{
public:
	PGPropertyValueColor(const wxColour& value) : PGPropertyValue(PVT_COLOR)
	{
		this->value = value;
	}
	inline  const wxColour& GetValue() const {return value;}
private:
	wxColour value;
};

//=====================================================================================
//client data k eventu property gridu
class WBPropGridEventData : public wxClientData
{
public:
	WBPropGridEventData(wxPGProperty* property, const PGPropertyValue* value) : wxClientData()
	{
		this->property = property;
		this->value = value;
	}
	inline PGPropertyValueType		GetValueType() const {return value->GetType();}
	inline const PGPropertyValue* GetValue() const {return value;}
	inline wxPGProperty* GetProperty() const {return property;}
	wxString GetValueAsString();
private:
	wxPGProperty* property;
	const PGPropertyValue* value;
};

//=====================================================================================
class PGPropertyClientData
{
public:
	PGPropertyClientData(PGCustomPropertyType type, void* CustomData)
	{
		this->type = type;
		this->CustomData = CustomData;
		UserData = NULL;
	}

	inline PGCustomPropertyType GetType() const {return type;}
	inline void* GetCustomData() const {return CustomData;}
	inline void* GetUserData() const {return UserData;}
	inline void SetUserData(void* data) {UserData = data;}
private:
	PGCustomPropertyType type;
	void* CustomData;	//pre interne ucely
	void* UserData;	//pre potreby usera pri finalmom pouziti
};

//=====================================================================================
class WBPropertyGrid : public wxPropertyGrid
{
public:
				WBPropertyGrid(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,  long style = wxPG_DEFAULT_STYLE, const wxChar* name = wxPropertyGridNameStr);
	virtual ~WBPropertyGrid();

	wxPGProperty* AppendEditBoxProperty(const char* name, const char* label, int value, const wxValidator& validator);
	wxPGProperty* AppendEditBoxProperty(const char* name, const char* label, float value, const wxValidator& validator);
	wxPGProperty* AppendEditBoxProperty(wxPGId ParentID, const char* name, const char* label, const char* value, const wxValidator& validator);

	wxPGProperty* InsertEditBoxProperty(wxPGId ParentID, int pos, const char* name, const char* label, const char* value, const wxValidator& validator);

	wxPGProperty* AppendComboBoxProperty(const char* name, const char* label, int value, const wxValidator& validator, wxPGChoices& choices, bool ReadOnly);
	wxPGProperty* AppendComboBoxProperty(const char* name, const char* label, const char* value, const wxValidator& validator, wxPGChoices& choices, bool ReadOnly);

	wxPGProperty* AppendCheckBoxProperty(const char* name, const char* label, bool value);
	wxPGProperty* AppendVectorProperty(const char* name, const char* label, Vector3& value);
#ifndef WORKBENCH
	wxPGProperty* AppendFileProperty(const char* name, const char* label, const char* value, const wxValidator& validator, RichFileDialog* dialog);
	wxPGProperty* AppendCustomDialogProperty(const char* name, const char* label, const char* value, const wxValidator& validator, UniversalBaseDialog* dialog);
#endif
	wxPGProperty* AppendColorProperty(const char* name, const char* label, wxColour& value);
	wxPGProperty* AppendFlagsProperty(const char* name, const char* label, wxPGChoices& BitNamesAndOrders);

	void SetUserDataToProperty(wxPGProperty* prop, void* data);	//pre konecne pouzite
	void* GetUserDataFromProperty(wxPGProperty* prop);	//pre konecne pouzite

	wxPGProperty* SetPropertyValueEx(const char* name, const Vector3& value);
	wxPGProperty* SetPropertyValueEx(const char* name, float value);
	void RedrawProperty(wxPGProperty* prop);
	void RedrawProperty(wxPGId& id);
	wxPGId FindProperty(const char* name);
	wxPGId FindCategory(const char* name);
	wxPGId FindCategory(void* ClientData);

	virtual void OnPropertyChanged(const char* name, int value){};
	virtual void OnPropertyChanged(const char* name, float value){};
	virtual void OnPropertyChanged(const char* name, const char* value){};
	virtual void OnPropertyChanged(const char* name, bool value){};
	virtual void OnPropertyChanged(const char* name, const Vector3& value){};

private:
	EArray<PGPropertyClientData*> ClientDatas;
	void SetClientDataToProperty(wxPGProperty* prop, PGPropertyClientData* data);
	void OnPropertyGridChange( wxPropertyGridEvent& event );
	void OnRMBDown(wxMouseEvent& event);
	void OnRMBUp(wxMouseEvent& event);
	DECLARE_EVENT_TABLE()
};

#endif