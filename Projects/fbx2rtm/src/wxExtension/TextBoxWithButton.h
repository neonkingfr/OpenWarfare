
#ifndef TEXTBOX_WITH_BUTTON_H
#define TEXTBOX_WITH_BUTTON_H

#include "ControlsGlobal.h"
#include "RichComboBox.h"
#include "wx/button.h"
#include "wx/textctrl.h"
#include "wx/combobox.h"
#include "wx/panel.h"

//=========== custom event pre TextboxWithButton START ===============================
BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_FILETEXTBOX, 7777)
END_DECLARE_EVENT_TYPES()

// it may also be convenient to define an event table macro for this event type
#define EVT_FILETEXTBOX(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY( \
        wxEVT_FILETEXTBOX, id, -1, \
        (wxObjectEventFunction)(wxEventFunction)(wxCommandEventFunction)&fn, \
        (wxObject *) NULL \
    ),
//=========== custom event pre TextboxWithButton END ===============================

//====================================================================================
class TextboxWithButton: public wxPanel
{
public:
	wxWindow *Parent;
	wxTextCtrl* TextBox;
	RichComboBox* Combo;
	wxButton* button;
	wxWindow* GetEditControl();
	virtual wxString GetValue();
	virtual void SetValue(wxString value);
	bool IsAnyChoice(wxString value);
	TextboxWithButton(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, int TextBoxStyle, char** ButtonXPMBitmap);
	TextboxWithButton(wxWindow *parent, wxWindowID id, wxPoint pos, wxSize& size, wxArrayString& choices, int ComboBoxStyle, char** ButtonXPMBitmap);
	~TextboxWithButton();
private:
	wxBitmap* ButtonBitmap;
	virtual void OnButtonClick(wxCommandEvent &event);
	virtual void OnTextEnter(wxCommandEvent &event);
	virtual void OnSize(wxSizeEvent &event);
	DECLARE_EVENT_TABLE()
};

#endif