//-----------------------------------------------------------------------------
// File: enf_mathlib.cpp
//
// Desc: Mathematics library
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "common/enf_memory.h"
#include "common/enf_mathlib.h"
#ifndef ENF_XENON
#include <d3dx9math.h>
#endif

//-----------------------------------------------------------------------
Vector3 strtovec(const char *str) 
{ 
	Vector3 out(0.0, 0.0, 0.0); 

	//return zeroes if str is null 
	if (!str)  
		return out; 

	char tmp[50]; 
	int i = 0, j = 0; 

	//for each character 
	while (j < 3) 
	{
		if (!*str || *str == ',' || *str == ' ' || *str == '\t') 
		{ 
			//more spaces, commas,...
			if (i == 0 && *str)
			{
				str++;
				continue;
			}

			tmp[i] = '\0'; 
			out.v[j++] = atof(tmp); 

			if (!*str) 
				break; 

			i = 0; 
			str++; 
		}
		else 
		{ 
			tmp[i++] = *str; 
			str++; 
		} 
	}
	return out; 
} 

//-----------------------------------------------------------------------
ALIGN16 uint fast_sqrt_table[0x10000];  // declare table of square roots 

typedef union FastSqrtUnion
{
  float f;
  unsigned int i;
} FastSqrtUnion;

void  build_sqrt_table()
{
  unsigned int i;
  FastSqrtUnion s;
  
  for (i = 0; i <= 0x7FFF; i++)
  {
    // Build a float with the bit pattern i as mantissa
    //  and an exponent of 0, stored as 127

    s.i = (i << 8) | (0x7F << 23);
    s.f = (float)sqrt(s.f);

    // Take the square root then strip the first 7 bits of
    //  the mantissa into the table
    fast_sqrt_table[i + 0x8000] = (s.i & 0x7FFFFF);

    // Repeat the process, this time with an exponent of 1, 
    //  stored as 128

    s.i = (i << 8) | (0x80 << 23);
    s.f = (float)sqrt(s.f);

    fast_sqrt_table[i] = (s.i & 0x7FFFFF);
  }
}

//-----------------------------------------------------------------------
void __mathInit()
{
	build_sqrt_table();
}

//-----------------------------------------------------------------------
AAB& AAB::AddPoints(const Vector3* points, uint numpoints, uint stride)
{
	enf_assert(numpoints != 0);

	//let the compiler to optimize memory accesses
	float minsx = enf_min(Mins.x, points->x);
	float minsy = enf_min(Mins.y, points->y);
	float minsz = enf_min(Mins.z, points->z);
	float maxsx = enf_max(Maxs.x, points->x);
	float maxsy = enf_max(Maxs.y, points->y);
	float maxsz = enf_max(Maxs.z, points->z);

	for(uint n = 1; n < numpoints; n++)
	{
		points = (Vector3*)((const char*)points + stride);
		minsx = enf_min(minsx, points->x);
		maxsx = enf_max(maxsx, points->x);
		minsy = enf_min(minsy, points->y);
		maxsy = enf_max(maxsy, points->y);
		minsz = enf_min(minsz, points->z);
		maxsz = enf_max(maxsz, points->z);
	}

	Mins.x = minsx;
	Mins.y = minsy;
	Mins.z = minsz;
	Maxs.x = maxsx;
	Maxs.y = maxsy;
	Maxs.z = maxsz;

	return *this;
}

//-----------------------------------------------------------------------
AAB& AAB::AddOBB(const Matrix43& matrix, const AAB& bbox)
{
	Vector3 mins(matrix.m[3]), maxs(matrix.m[3]);

	float   a, b; 

	for (int j = 0;  j < 3; j++) 
	{
		for (int i = 0; i < 3; i++)
		{
			a = matrix.m[i][j] * bbox.Mins[i];
			b = matrix.m[i][j] * bbox.Maxs[i];

			if (a < b)
			{
				mins.v[j] += a;
				maxs.v[j] += b;
			}	else
			{
				mins.v[j] += b;
				maxs.v[j] += a;
			}
		}
	}

	AddMin(mins);
	AddMax(maxs);

	enf_assert(IsValid());
	return *this;
}

//-----------------------------------------------------------------------
AAB& AAB::InitFromOBB(const Matrix43& matrix, const AAB& box)
{
	Vector3 mins2, maxs2;
  	
	mins2 = maxs2 = matrix.m[3];

	float   a, b; 

	for (int j = 0;  j < 3; j++) 
	{
		for (int i = 0; i < 3; i++)
		{
			a = matrix.m[i][j] * box.Mins[i];
			b = matrix.m[i][j] * box.Maxs[i];

			if (a < b)
			{
				mins2.v[j] += a;
				maxs2.v[j] += b;
			}
			else
			{
				mins2.v[j] += b;
				maxs2.v[j] += a;
			}
		}
	}

	Mins = mins2;
	Maxs = maxs2;

	enf_assert(IsValid());
	return *this;
}

//-----------------------------------------------------------------------------
Vector3 Quaternion::operator*( const Vector3& a ) const
{
	float xxzz = x * x - z * z;
	float wwyy = w * w - y * y;

	float xw2 = x * w * 2.0f;
	float xy2 = x * y * 2.0f;
	float xz2 = x * z * 2.0f;
	float yw2 = y * w * 2.0f;
	float yz2 = y * z * 2.0f;
	float zw2 = z * w * 2.0f;

	return Vector3((xxzz + wwyy) * a.x		+ (xy2 + zw2) * a.y		+ (xz2 - yw2) * a.z,
						(xy2 - zw2) * a.x			+ (xxzz - wwyy) * a.y		+ (yz2 + xw2) * a.z,
						(xz2 + yw2) * a.x			+ (yz2 - xw2) * a.y		+ (wwyy - xxzz) * a.z);
}

//-----------------------------------------------------------------------------
Quaternion& Quaternion::Normalize()
{
	float len;
	float ilength;

	len = Length();
	if (len)
	{
		ilength = 1 / len;
		x *= ilength;
		y *= ilength;
		z *= ilength;
		w *= ilength;
	}
	return *this;
}

//-----------------------------------------------------------------------------
Vector3 Quaternion::ToMatrixRow(uint row) const
{
	enf_assert(row < 3);

	float fTX  = 2.0f * x;
	float fTY  = 2.0f * y;
	float fTZ  = 2.0f * z;

	Vector3 r;

	switch(row)
	{
	case 0:
		r.x  = 1.0f - ((fTY * y) + (fTZ * z));
		r.y  = (fTY * x) - (fTZ * w);
		r.z  = (fTZ * x) + (fTY * w);
		break;

	case 1:
		r.x  = (fTY * x) + (fTZ * w);
		r.y  = 1.0f - ((fTX * x) + (fTZ * z));
		r.z  = (fTZ * y) - (fTX * w);
		break;
		
	case 2:
		r.x  = (fTZ * x) - (fTY * w);
		r.y  = (fTZ * y) + (fTX * w);
		r.z = 1.0f - ( (fTX * x) + (fTY * y) );
		break;
	}
	return r;
}

//-----------------------------------------------------------------------------
float Quaternion::ToPitch() const
{
	Vector3 r;

	float fTX  = 2.0f * x;
	float fTY  = 2.0f * y;
	float fTZ  = 2.0f * z;
	float fTWY = fTY * w;
	float fTWZ = fTZ * w;
	float fTXY = fTY * x;
	float fTXZ = fTZ * x;
	float fTYY = fTY * y;
	float fTZZ = fTZ * z;

	r.x = 1.0f - ( fTYY + fTZZ );
	r.y = fTXY - fTWZ;
	r.z = fTXZ + fTWY;
	
	float ang = ENF_RAD2DEG(atan2(r.x, r.z));
	while(ang < 0)
		ang += 360.0f;

	while(ang >= 360.0f)
		ang -= 360.0f;

	return ang;
}

//-----------------------------------------------------------------------
Quaternion Quaternion::Lerp(const Quaternion& q, float lerp) const
{
	Quaternion res;

	D3DXQuaternionSlerp(
			(D3DXQUATERNION*)res.v,
			(D3DXQUATERNION*)v,
			(D3DXQUATERNION*)q.v,
			lerp);

	return res;
}

//-----------------------------------------------------------------------
Quaternion Quaternion::Lerp(const Quaternion* q, float lerp) const
{
	Quaternion res;
	Quaternion temp;
	float	omega, cosom, sinom, scale0, scale1;

	if ( lerp <= 0.0f )
	{
		res = *this;
		return res;
	}

	if ( lerp >= 1.0f )
	{
		res = *q;
		return res;
	}

	if ( *this == *q )
	{
		res = *this;
		return res;
	}

	cosom = x * q->x + y * q->y + z * q->z + w * q->w;
	if ( cosom < 0.0f )
	{
		temp = -(*q);
		cosom = -cosom;
	}
	else
	{
		temp = *q;
	}

	if ( ( 1.0f - cosom ) > 1e-6f )
	{
		scale0 = 1.0f - cosom * cosom;
		sinom = 1.0f / sqrtf( scale0 );
		omega = atan2f( scale0 * sinom, cosom );
		scale0 = sinf( ( 1.0f - lerp ) * omega ) * sinom;
		scale1 = sinf( lerp * omega ) * sinom;
	}
	else
	{
		scale0 = 1.0f - lerp;
		scale1 = lerp;
	}

	res = ( (Vector4)(*this) * scale0) + ( (Vector4)temp * scale1 );
	/*
	D3DXQuaternionSlerp(
	(D3DXQUATERNION*)res.v,
	(D3DXQUATERNION*)v,
	(D3DXQUATERNION*)q->v,
	lerp);
	*/
	return res; 
}

//-----------------------------------------------------------------------
float Quaternion::Dot(const Quaternion& quat) const
{
	return D3DXQuaternionDot((CONST D3DXQUATERNION*)v, (CONST D3DXQUATERNION*)quat.v);
}

//-----------------------------------------------------------------------
float Quaternion::Dot(const Quaternion* quat) const
{
	return D3DXQuaternionDot((CONST D3DXQUATERNION*)v, (CONST D3DXQUATERNION*)quat->v);
}

//-----------------------------------------------------------------------------
Matrix43 QKey::ToMatrix() const
{
	Matrix43 mat;
	mat = m_qQuat;
	mat._41 = m_vTrans[0];
	mat._42 = m_vTrans[1];
	mat._43 = m_vTrans[2];
	return mat;
}

//-----------------------------------------------------------------------------
QKey QKey::Lerp(const QKey& q, float lerp) const
{
	if(lerp == 0 || (m_qQuat == q.m_qQuat && m_vTrans == q.m_vTrans))
		return *this;

	if(lerp == 1)
		return q;

	QKey qout;
	qout.m_qQuat = m_qQuat.Lerp(q.m_qQuat, lerp);
	qout.m_vTrans = m_vTrans.Lerp(q.m_vTrans, lerp);
	return qout;
}

//-----------------------------------------------------------------------------
QKey QKey::LerpRot(const QKey& q, float lerp) const
{
	if(lerp == 0 || m_qQuat == q.m_qQuat)
		return *this;

	if(lerp == 1)
		return q;

	QKey qout;
	qout.m_qQuat = m_qQuat.Lerp(q.m_qQuat, lerp);
	return qout;
}


//-----------------------------------------------------------------------
Vector3 Vector3::Perpendicular() const
{
	const Vector3 left(1.0f, 0.0f, 0.0f);

	float d = Dot(left);

	if(fabs(d) < 0.5f)
	{
		return (*this) ^ left;
	}

	const Vector3 forward(0.0f, 1.0f, 0.0f);

	d = Dot(forward);

	if(fabs(d) < 0.5f)
	{
		return (*this) ^ forward;
	}

	const Vector3 up(0.0f, 0.0f, 1.0f);

	return (*this) ^ up;
}

//-----------------------------------------------------------------------
Vector3 Vector3::RotatePointAroundVector(const Vector3& point, float degrees )
{
	float xx = x*point.x;
	float xy = x*point.y;
	float xz = x*point.z;
	float yx = y*point.x;
	float yy = y*point.y;
	float yz = y*point.z;
	float zx = z*point.x;
	float zy = z*point.y;
	float zz = z*point.z;
	float sa = sinf(ENF_DEG2RAD(degrees));
	float ca = cosf(ENF_DEG2RAD(degrees));

	return Vector3(
        x*(xx+yy+zz) + (point.x*(y*y+z*z)-x*(yy+zz))*ca+(-zy+yz)*sa,
		y*(xx+yy+zz) + (point.y*(x*x+z*z)-y*(xx+zz))*ca+(zx-xz)*sa,
		z*(xx+yy+zz) + (point.z*(x*x+y*y)-z*(xx+yy))*ca+(-yx+xy)*sa
		);
}

//-----------------------------------------------------------------------
Vector3 MPlane::Project(const Vector3& pt) const
{
	float d = -Distance(pt);

	return Vector3(x * d + pt.x, y * d + pt.y, z * d + pt.z);
}

#define COLINEAR_EPSILON 0.01

//-----------------------------------------------------------------------
bool Colinear(const Vector3& a, const Vector3& b, const Vector3& c)
{
Vector3 v1, v2;

	v1 = a - b;
	v2 = b - c;
	v1.Normalize();
	v2.Normalize();
   float f = v1.Dot(v2);

	return (f > (1.0 - COLINEAR_EPSILON) && f < (1.0 + COLINEAR_EPSILON));
}

#define FP_ABS_BITS(fp) (FP_BITS(fp)&0x7FFFFFFF)
#define FP_SIGN_BIT(fp) (FP_BITS(fp)&0x80000000)
#define FP_ONE_BITS 0x3F800000


// r = 1/p
#define FP_INV(r,p)                                                          \
{                                                                            \
    int _i = 2 * FP_ONE_BITS - *(int *)&(p);                                 \
    r = *(float *)&_i;                                                       \
    r = r * (2.0f - (p) * r);                                                \
}

/////////////////////////////////////////////////
// The following comes from Vincent Van Eeckhout
// Thanks for sending us the code!
// It's the same thing in assembly but without this C-needed line:
//    r = *(float *)&_i;

float   two = 2.0f;

#define FP_INV2(r,p)                     \
{                                        \
    __asm { mov     eax,0x7F000000    }; \
    __asm { sub     eax,dword ptr [p] }; \
    __asm { mov     dword ptr [r],eax }; \
    __asm { fld     dword ptr [p]     }; \
    __asm { fmul    dword ptr [r]     }; \
    __asm { fsubr   [two]             }; \
    __asm { fmul    dword ptr [r]     }; \
    __asm { fstp    dword ptr [r]     }; \
}

/////////////////////////////////////////////////


#define FP_EXP(e,p)                                                          \
{                                                                            \
    int _i;                                                                  \
    e = -1.44269504f * (float)0x00800000 * (p);                              \
    _i = (int)e + 0x3F800000;                                                \
    e = *(float *)&_i;                                                       \
}

#define FP_NORM_TO_BYTE(i,p)                                                 \
{                                                                            \
    float _n = (p) + 1.0f;                                                   \
    i = *(int *)&_n;                                                         \
    if (i >= 0x40000000)     i = 0xFF;                                       \
    else if (i <=0x3F800000) i = 0;                                          \
    else i = ((i) >> 15) & 0xFF;                                             \
}



ENF_INLINE unsigned long FP_NORM_TO_BYTE2(float p)                                                 
{                                                                            
  float fpTmp = p + 1.0f;                                                      
  return ((*(unsigned *)&fpTmp) >> 15) & 0xFF;  
}


ENF_INLINE unsigned long FP_NORM_TO_BYTE3(float p)     
{
  float ftmp = p + 12582912.0f;                                                      
  return ((*(unsigned long *)&ftmp) & 0xFF);
}

