//-----------------------------------------------------------------------------
// File: enf_memory.cpp
//
// Desc: Memory allocator
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "common/enf_types.h"
#include "common/enf_strings.h"
#include "common/enf_memory.h"
//#include "enf_mutex.h"

#ifndef ENF_XENON
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#endif

//#include "enf_main.h"

#ifdef _DEBUG
#define ENF_DEBUG_ALLOC
#endif

#ifdef ENF_LEAK_DETECTOR
	#include "common/enf_memoryreport.h"
	#define DEBUG
#endif

#define ENF_BEGIN_GUARD 0xfeedf1d0
#define ENF_END_GUARD 0xdeadf1d0

size_t MemStats::m_iAnims = 0;
size_t MemStats::m_iTotalIndices = 0;
size_t MemStats::m_iTotalVertices = 0;
size_t MemStats::m_iObjectIndices = 0;
size_t MemStats::m_iObjectVertices = 0;
size_t MemStats::m_iRenderTargets = 0;
size_t MemStats::m_iTextures = 0;
size_t MemStats::m_iLevelData = 0;
size_t MemStats::m_iParticles = 0;
size_t MemStats::m_iObjects = 0;

size_t	MemoryManager::m_iTotalAllocatedBytes = 0;
size_t	MemoryManager::m_iTotalAllocatedBlocks = 0;
//CMutex	MemoryManager::m_Mutex;

struct MemoryManager::MemoryChunk
{
	const char*		File;
	int				Line;
	MemoryChunk*	Next;
	MemoryChunk**	Prev;
	size_t			Size;
	uint				Pattern;
};

MemoryManager::MemoryChunk*	MemoryManager::m_pChunks;

MemoryManager::MemoryManager()
{
	m_iTotalAllocatedBytes = 0;
	m_iTotalAllocatedBlocks = 0;
}

//-----------------------------------------------------------------------
void MemoryManager::Init()
{
}

#ifdef ENF_LEAK_DETECTOR

void AllocationSet::Dump()
{
	for(uint a = 0; a < GetCardinality(); a++)
	{
		const AllocLocation& loc = (*this)[a];

		SString<256> temp;

		_OutputDebugString(temp.format("%s(%d) %8dx %8d total bytes\n", loc.File, loc.Line, loc.Count, loc.Size));

		char databuff[256] = "Data: <";
		uint p = 7;
		for(uint b = 0; b < enf_min(16U, loc.Size); b++)
		{
			char c = ((const char*)loc.Data)[b];
			if(c < ' ') c = ' ';
			databuff[p++] = c;
		}
		databuff[p++] = '>';
		databuff[p++] = ' ';

		for(uint b = 0; b < enf_min(16U, loc.Size); b++)
		{
			sprintf(databuff + p, "%02x ", (int)((const char*)loc.Data)[b]);
			p += 3;
		}
		databuff[p++] = '\n';
		databuff[p++] = 0;
		_OutputDebugString(databuff);
	}
}

//---------------------------------------------------------------------------
void MemoryManager::Dump(const char* title)
{
	if(title)
		_OutputDebugString(title);

	int total = 0, named = 0;

	AllocationSet chunks;

	for(MemoryChunk *m = m_pChunks; m; m = m->Next)
	{
		total += m->Size;

		if(m->File && m->Size != 0)
		{
			named += m->Size;
			//enf_assert(IsBadStringPtr(m->File, 64) == false);
			
			chunks.Insert(m->File, m->Line, m->Size, m + 1);
		}
	}

	chunks.Dump();

	SString<256> temp;
	_OutputDebugString(temp.format("Total %dkb  Named %dkb\n", total / 1024, named / 1024));
}

//---------------------------------------------------------------------------
void *MemoryManager::Alloc(size_t cb, const char* name, int line)
{
	//SString<512> buff;
	//OutputDebugString(buff.format("%s(%d) Alloc %d\n", name, line, cb));

#ifdef ENF_MEMORYVALIDATE
	::HeapValidate(::GetProcessHeap(), 0, NULL);
#endif

	MemoryChunk *ptr = (MemoryChunk*)::HeapAlloc(::GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, cb + sizeof(MemoryChunk));

//	CMutexLock lock(m_Mutex);

	ptr->Prev = &m_pChunks;
	ptr->Next = m_pChunks;
	if(m_pChunks)
		m_pChunks->Prev = &ptr->Next;
	m_pChunks = ptr;
	ptr->Size = cb;
	ptr->Pattern = ENF_BEGIN_GUARD;
	ptr->File = NULL;
	if(name)
	{
		ptr->File = name;
	}

	ptr->Line = line;
	ptr++;

	FillMemory32(ptr, cb / 4, 0xbaadf00d);

	m_iTotalAllocatedBytes += cb;
	m_iTotalAllocatedBlocks++;

   return ptr;
}

//---------------------------------------------------------------------------
void *MemoryManager::ReAlloc(void* ptr, size_t size)
{
	if(ptr == NULL)
	{
		return Alloc(size, NULL, 0);
	}

#ifdef ENF_MEMORYVALIDATE
	::HeapValidate(::GetProcessHeap(), 0, NULL);
#endif

//	CMutexLock lock(m_Mutex);

	MemoryChunk *chunk = (MemoryChunk*)ptr - 1;
	enf_assert(chunk->Pattern == ENF_BEGIN_GUARD);
	if(chunk->Next)
		chunk->Next->Prev = chunk->Prev;
	*chunk->Prev = chunk->Next;
	chunk->Pattern = 0;

	m_iTotalAllocatedBytes -= chunk->Size;
	m_iTotalAllocatedBlocks--;

	const char* file = chunk->File;
	int line = chunk->Line;

	//char buff[512];
	//sprintf(buff, "%s(%d) Realloc\n", file, line);
	//OutputDebugString(buff);

	chunk = (MemoryChunk*)::HeapReAlloc(::GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, chunk, size + sizeof(MemoryChunk));

	chunk->Prev = &m_pChunks;
	chunk->Next = m_pChunks;
	if(m_pChunks)
		m_pChunks->Prev = &chunk->Next;
	m_pChunks = chunk;
	chunk->Size = size;
	chunk->Pattern = ENF_BEGIN_GUARD;
	chunk->File = file;
	chunk->Line = line;

	chunk++;

	m_iTotalAllocatedBytes += size;
	m_iTotalAllocatedBlocks++;

   return chunk;
}

//---------------------------------------------------------------------------
void MemoryManager::Free(void *p)
{
#ifdef ENF_MEMORYVALIDATE
	::HeapValidate(::GetProcessHeap(), 0, NULL);
#endif

//	CMutexLock lock(m_Mutex);

	MemoryChunk *ptr = (MemoryChunk*)p - 1;

	enf_assert(ptr->Pattern == ENF_BEGIN_GUARD);

	if(ptr->Next)
		ptr->Next->Prev = ptr->Prev;

	*ptr->Prev = ptr->Next;
	ptr->Pattern = 0;

	m_iTotalAllocatedBytes -= ptr->Size;
	m_iTotalAllocatedBlocks--;

	FillMemory32(ptr, ptr->Size / 4, 0xfeeefeee);

	::HeapFree(::GetProcessHeap(), 0, ptr);
}

//---------------------------------------------------------------------------
size_t MemoryManager::GetSize(void* ptr)
{
	if(ptr == NULL)
		return 0;

	return (((MemoryChunk*)ptr) - 1)->Size;
}

//---------------------------------------------------------------------------
void* MemoryManager::Alloc16(size_t cb, const char *src, int line)
{
	ubyte* ptr = (ubyte*)Alloc(cb + 15 + 1, src, line);
	ubyte* aligned = (ubyte*)((unsigned __int64)(ptr + 15 + 1) & (~0x0fui64));
	aligned[-1] = (ubyte)(aligned - ptr);

	return aligned;
}

//---------------------------------------------------------------------------
void MemoryManager::Free16(void *ptr)
{
	ubyte* aligned = (ubyte*)ptr;
	Free((void*)(aligned - aligned[-1]));
}

//---------------------------------------------------------------------------
void *operator new[](size_t cb, const char* file, int line)
{
	if(cb == 0)
		cb=cb;
	return MemoryManager::Alloc(cb, file, line);
}

//---------------------------------------------------------------------------
void *operator new(size_t cb, const char* file, int line)
{
	if(cb == 0)
		cb=cb;
	return MemoryManager::Alloc(cb, file, line);
}

//---------------------------------------------------------------------------
void operator delete[](void *p, const char* file, int line)
{
	if(p == NULL)
		return;

	MemoryManager::Free(p);
}

//---------------------------------------------------------------------------
void operator delete(void *p, const char* file, int line)
{
	if(p == NULL)
		return;

	MemoryManager::Free(p);
}

#else

//---------------------------------------------------------------------------
void *MemoryManager::Alloc(size_t cb)
{
//	CMutexLock lock(m_Mutex);

#ifdef ENF_DEBUG_ALLOC
	//in debug mode append some guard band
	char *ptr = (char*)::HeapAlloc(::GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, cb + 16);
	*(int*)ptr = ENF_BEGIN_GUARD;
	*(int*)(ptr + 4) = ENF_BEGIN_GUARD;

	ptr += 8;

	FillMemory32(ptr, cb / 4, 0xbaadf00d);

	*(int*)(ptr + cb) = ENF_END_GUARD;
	*(int*)(ptr + cb + 4) = ENF_END_GUARD;

	m_iTotalAllocatedBytes += cb + 16;

#else

	void *ptr = ::HeapAlloc(::GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, cb);

	m_iTotalAllocatedBytes += cb;

#endif

	m_iTotalAllocatedBlocks++;
   return ptr;
}

//---------------------------------------------------------------------------
void *MemoryManager::ReAlloc(void* ptr, size_t size)
{
	if(ptr == NULL)
	{
		return Alloc(size);
	}

//	CMutexLock lock(m_Mutex);

#ifdef ENF_DEBUG_ALLOC
	char* cp = ((char*)ptr) - 8;
	enf_assert(*(int*)(cp + 0) == ENF_BEGIN_GUARD);
	enf_assert(*(int*)(cp + 4) == ENF_BEGIN_GUARD);

	size_t cb = ::HeapSize(::GetProcessHeap(), 0, cp);

	m_iTotalAllocatedBytes -= cb;
	cb -= 16;

	enf_assert(*(int*)(cp + 8 + cb) == ENF_END_GUARD);
	enf_assert(*(int*)(cp + 12 + cb) == ENF_END_GUARD);

	cp = (char*)::HeapReAlloc(::GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, cp, size + 16);

	//if we allocated bigger block, fill rest of memory with pattern
	if(size > cb)
	{
		FillMemory32(cp + 8 + cb, (size - cb) / 4, 0xbaadf00d);
	}

	*(int*)(cp + 8 + size) = ENF_END_GUARD;
	*(int*)(cp + 12 + size) = ENF_END_GUARD;

	ptr = cp + 8;

	m_iTotalAllocatedBytes += size + 16;

#else
	m_iTotalAllocatedBytes -= ::HeapSize(::GetProcessHeap(), 0, ptr);

	ptr = ::HeapReAlloc(::GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, ptr, size);

	m_iTotalAllocatedBytes += size;
#endif

   return ptr;
}

//---------------------------------------------------------------------------
void MemoryManager::Free(void *p)
{
//	CMutexLock lock(m_Mutex);

#ifdef ENF_DEBUG_ALLOC
	char* cp = ((char*)p) - 8;
	enf_assert(*(int*)(cp + 0) == ENF_BEGIN_GUARD);
	enf_assert(*(int*)(cp + 4) == ENF_BEGIN_GUARD);

	size_t cb = ::HeapSize(::GetProcessHeap(), 0, cp);
	m_iTotalAllocatedBytes -= cb;

	cb -= 16;

	FillMemory32(cp + 8, cb / 4, 0xfeeefeee);

	enf_assert(*(int*)(cp + 8 + cb) == ENF_END_GUARD);
	enf_assert(*(int*)(cp + 12 + cb) == ENF_END_GUARD);

	::HeapFree(::GetProcessHeap(), 0, cp);

#else

	m_iTotalAllocatedBytes -= ::HeapSize(::GetProcessHeap(), 0, p);

	::HeapFree(::GetProcessHeap(), 0, p);
#endif

	m_iTotalAllocatedBlocks--;
}

//---------------------------------------------------------------------------
size_t MemoryManager::GetSize(void* ptr)
{
	if(ptr == NULL)
		return 0;

#ifdef ENF_DEBUG_ALLOC
	return ::HeapSize(::GetProcessHeap(), 0, ((char*)ptr) - 8) - 16;
#else
	return ::HeapSize(::GetProcessHeap(), 0, ptr);
#endif
}

//---------------------------------------------------------------------------
void* MemoryManager::Alloc16(size_t cb)
{
	ubyte* ptr = (ubyte*)Alloc(cb + 15 + 1);
	ubyte* aligned = (ubyte*)((unsigned __int64)(ptr + 15 + 1) & (~0x0fui64));
	aligned[-1] = (ubyte)(aligned - ptr);

	return aligned;
}

//---------------------------------------------------------------------------
void MemoryManager::Free16(void *ptr)
{
	ubyte* aligned = (ubyte*)ptr;
	Free((void*)(aligned - aligned[-1]));
}
#endif


#ifdef ENF_LEAK_DETECTOR
#ifndef ENF_STATIC
//---------------------------------------------------------------------------
void *operator new[](size_t cb)
{
	if(cb == 0)
		cb=cb;
	return MemoryManager::Alloc(cb);
}

//---------------------------------------------------------------------------
void *operator new(size_t cb)
{
	if(cb == 0)
		cb=cb;
	return MemoryManager::Alloc(cb);
}

//---------------------------------------------------------------------------
void operator delete[](void *p)
{
	if(p == NULL)
		return;

	try {
	MemoryManager::Free(p);
	} catch(...)
	{
		p = p;
	}
}

//---------------------------------------------------------------------------
void operator delete(void *p)
{
	if(p == NULL)
		return;

	MemoryManager::Free(p);
}
#endif
#endif
