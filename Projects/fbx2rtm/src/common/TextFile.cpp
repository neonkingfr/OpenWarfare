//ked je nadefinovane UNICODE tak ho oddefinujeme ale na konci fajlu ho pripadne znova nadefinujeme
#ifdef UNICODE
#undef UNICODE
#define UNICODE_BACK
#endif

#ifdef _UNICODE
#undef _UNICODE
#define _UNICODE_BACK
#endif


#include "wbPrecomp.h"
//#include "stdafx.h"
#include "TextFile.h"
#include "windows.h"
//#include "WindowsUndef.h"



//TextParser TxtParser;
const char EndLineChars[3] = {13, 10, '\0'};
const char Quotes[2] = {34, '\0'};
const int MaxLineLength = 1024;

//-------------------------------------------------------------------------------------------------
TextParser::TextParser()
{
	NumSplitSymbols = 0;
	AddSplitSymbol(string(Quotes), false);	//uvodzovky
	AddSplitSymbol(" ", false);				//medzera
	AddSplitSymbol("	", false);				//tabulator
	AddSplitSymbol("/*", true);				//poznamka start
	AddSplitSymbol("*/", true);				//poznamka end
	AddSplitSymbol("//", true);				//poznamka
}

//-------------------------------------------------------------------------------------------------
bool TextParser::AddSplitSymbol(string symbol, bool AsToken)
{
	if(NumSplitSymbols >= (sizeof(SplitSymbols) / sizeof(string)) - 1)
		return false;

	for(int n = 0; n < NumSplitSymbols; n++)
	{
		if(SplitSymbols[n] == symbol)	//nedavame ich tam duplicitne
		{
			IsTokenSymbol[n] = AsToken;	//ale prepiseme aktualnu poziadavku tohto
			return true;
		}
	}

	SplitSymbols[NumSplitSymbols] = symbol;
	IsTokenSymbol[NumSplitSymbols++] = AsToken;
	return true;
}

//-------------------------------------------------------------------------------------------------
bool TextParser::RemoveSplitSymbol(string symbol)
{
	for(int n = 0; n < NumSplitSymbols; n++)
	{
		if(SplitSymbols[n] == symbol)
		{
			NumSplitSymbols--;

			for(int k = n; k < NumSplitSymbols; k++)
			{
				SplitSymbols[k] = SplitSymbols[k + 1];
				IsTokenSymbol[k] = IsTokenSymbol[k + 1];
			}
			return true;
		}
	}
	return false;
}

//-------------------------------------------------------------------------------------------------
void TextParser::RemoveAllSplitSymbols()
{
	for(int n = 0; n < NumSplitSymbols; n++)
		SplitSymbols[n] = "";

	NumSplitSymbols = 0;
}

//-------------------------------------------------------------------------------------------------
int TextParser::ParseLine(string Line, string* tokens, int* PosArray)
{
	int lng = (int)Line.length();
	char CurChar;
	int NumTokens = 0;
	string Temp = "";
	string Splitter = "";
	int SplitterIndex = 0;
	int Status = 0;
	int k;
	int Spos;
	int n = -1;
	bool Running = true;

	if(lng == 0)
		return 0;

	while(Running)
	{
		switch(Status)
		{
		case 0:	//bufferujeme znaky a hladame v bufferi rozdelovacie znaky
			if(n >= lng - 1)
			{
				Running = false;	//toto sa stane ked si na konci lajny medzery
				continue;
			}
				
			CurChar = Line[++n];

			if(Temp.empty() && (CurChar == ' ' || CurChar == char(9)))	//medzery a tabulatory na zaciatku ignorujeme
				continue;

			Temp += CurChar;

			for(k = 0; k < NumSplitSymbols; k++)
			{
				Spos = (int)Temp.find(SplitSymbols[k]);
				if(Spos != -1)
				{
					Status = 1;
					SplitterIndex = k;
					break;
				}
			}

			if(n == lng - 1 && Status == 0)	//posledny znak a v Temp nieco mame takze zapiseme token
			{
				Spos = (int)Temp.length();
				Status = 1;
				SplitterIndex = 0;	//su tam uvodzovky ktore nemozu byt zaroven token
				break;
			}
			break;

		case 1:
			if(n >= lng - 1)
				Running = false;	//sme na konci stringu. tento token bude posledny

			if(Spos > 0)	//rozdelovaci znak by mohol byt hned na zaciatku alebo dva po sebe takze pred nim ziadny token neni
			{
				tokens[NumTokens] = Temp.substr(0, Spos);
				Temp.erase(0, Spos);	//v Temp nam zostal splitter

				//ked sme na konci a v tempe uz nic neni tak sa musi decrementovat
				//rovnako v pripade ze ide o token v uvodzovkach
				if((!Running && Temp.empty()) || (CurChar == char(34) && tokens[NumTokens][0] == char(34)))
					Spos--;

				if(PosArray != NULL)
					PosArray[NumTokens] = n - Spos;	//ulozime token
					
				NumTokens++;
			}

			//oddelovaci znak je zaroven token takze ho pridame k tokenom. za poslednym tokenom uz nic neni takze Temp moze byt aj prazdny !
			if(IsTokenSymbol[SplitterIndex] && !Temp.empty())
			{
				tokens[NumTokens] = Temp;
				if(PosArray != NULL)
					PosArray[NumTokens] = n;
					
				NumTokens++;
			}

			if(CurChar == char(34) && Temp.length() == 1)	//vsetko v uvodzovkach je jeden token. bez vynimky. toto su pociatocne uvodzovky ak to plati
			{
				Status = 2;	//Temp neclearujeme nech tam zostanu uvodzovky
				break;
			}
			else
			{
				Temp.clear();
				Status = 0;	//ideme citat dalsie znaky
			}
			break;

		case 2:
			CurChar = Line[++n];
			Temp += CurChar;	//bez vynimky skladame string az kym neskoncia uvodzovky

			if(CurChar == char(34))
			{
				Spos = (int)Temp.length();
				SplitterIndex = 0;	//su tam natvrdo uvodzovky !
				Status = 1;				//normalne to posleme na status 1 akoze sa nasiel oddelovaci znak na konci
				break;
			}
			break;
		}
	}

	return NumTokens;
}

/*
TxtLine TxtLine::operator+(const TxtLine& Line1, const TxtLine& Line2)
{
  TxtLine line = Line1;

  for(int n=0; n < Line2.GetNumTokens(); n++)
		line.InsertToken(666, Line2.GetToken(n);

  return line;
}*/

//-------------------------------------------------------------------------------------------------
void TxtFile::Clear()
{
	bTest = false;
	CurrentLine = -1;
	PosInFile = 0;
	FullFile.clear();
}

//-------------------------------------------------------------------------------------------------
TxtFile::TxtFile(TextParser* parser)
{
	Parser = parser;
	Clear();
}

//-------------------------------------------------------------------------------------------------
TxtFile::~TxtFile()
{
}

//-------------------------------------------------------------------------------------------------
int TxtFile::AddBuffer(const char *array)
{
	int BufferLines = 0;
	for(int n = 0; n < (int)strlen(array); n++)
	{
		if(array[n] == char(10))
			BufferLines++;
	}

	FullFile.insert(PosInFile, array);
	PosInFile += (int)strlen(array);
	CurrentLine += BufferLines;
	return CurrentLine;
}

//-------------------------------------------------------------------------------------------------
int TxtFile::AddLine(const char *array)
{
	int TempSize = 3;	//na dvojznak konca riadka + ukoncenie stringu

	if(array != NULL)
		TempSize += (int)strlen(array);
	
	char* Temp = (char*)calloc(TempSize, sizeof(char));

	if(array != NULL)
	{
		strcpy(Temp, array);
		strcat(Temp, EndLineChars);
	}
	else
		strcpy(Temp, EndLineChars);

	CurrentLine++;
	int Lng = (int)strlen(Temp);
	FullFile.insert(PosInFile, Temp);
	PosInFile += Lng;		//pocitame dlzku pre zapis
//	PosInFile += 2;		//koli EndLineChars
	free(Temp);
   return CurrentLine;
}

//-------------------------------------------------------------------------------------------------
int TxtFile::AddLine(string& Line)
{
	return AddLine(Line.c_str());
}

//-------------------------------------------------------------------------------------------------
int TxtFile::AddLine(TxtLine& Line)
{
	return AddLine(Line.GetText());
}

//-------------------------------------------------------------------------------------------------
int TxtFile::GetNumLines()
{
	int CurLine = 0;
	int CurPos = 0;
	int LastPos = 0;
	int FileLng = (int)FullFile.length();

	if(FileLng == 0)
		return -1;

	while(CurPos < FileLng)
	{
		LastPos = CurPos;
		CurPos = (int)FullFile.find_first_of(char(10), LastPos) + 1;
		CurLine++;

		if(CurPos == 0)
			break;
	}
	return CurLine;
}

//-------------------------------------------------------------------------------------------------
bool TxtFile::GetLinePos(int Line, int* Start, int* End)
{
	int CurLine = 0;
	int CurPos = 0;
	int LastPos = 0;

	while(CurLine <= Line)
	{
		LastPos = CurPos;
		CurPos = (int)FullFile.find_first_of(char(10), LastPos) + 1;

		if(CurPos > 0)
			CurLine++;
		else
			break;
	}

	bool LastLineWithoutEndLineChars = false;

	if(CurPos == 0)
	{
		CurPos = (int)FullFile.length();
		LastLineWithoutEndLineChars = true;
	}

	*Start = LastPos;
	*End = CurPos;
	return LastLineWithoutEndLineChars;
}

//-------------------------------------------------------------------------------------------------
int TxtFile::RemoveLine(int Line)
{
	if(Line < 0 || Line >= GetNumLines())
		return -1;

	int LineStart, LineEnd;
	GetLinePos(Line, &LineStart, &LineEnd);
	FullFile.erase(LineStart, LineEnd - LineStart);

	PosInFile = LineStart;
	return --CurrentLine;
}

//-------------------------------------------------------------------------------------------------
int TxtFile::GetAll(char *array)
{
	int Lng = (int)FullFile.length();
	strcpy(array, FullFile.c_str());
	return Lng; 
}

//-------------------------------------------------------------------------------------------------
const char* TxtFile::GetAll()
{
	return FullFile.c_str();
}

//-------------------------------------------------------------------------------------------------
int TxtFile::GetFirstLine(char *array)
{
	strcpy(array, GetFirstLine().c_str());
	return CurrentLine;
}

//-------------------------------------------------------------------------------------------------
string TxtFile::GetFirstLine()
{
	if(GetNumLines() < 1)
	{
		CurrentLine = -1;
		return "";
	}

	CurrentLine = 0;
	PosInFile = 0;
	int LastPosInFile = PosInFile;
	PosInFile = (int)FullFile.find_first_of(char(10), LastPosInFile + 1) + 1;

	int lng = PosInFile - LastPosInFile;
	string ln = FullFile.substr(0, lng - 2);
	return ln;
}

//-------------------------------------------------------------------------------------------------
TxtLine TxtFile::GetFirstLineEx()
{
	return TxtLine(Parser, GetFirstLine());
}

//-------------------------------------------------------------------------------------------------
int TxtFile::GetLastLine(char *array)
{
	strcpy(array, GetLastLine().c_str());
	return CurrentLine;
}

//-------------------------------------------------------------------------------------------------
TxtLine TxtFile::GetLastLineEx()
{
	return TxtLine(Parser, GetLastLine());
}

//-------------------------------------------------------------------------------------------------
string TxtFile::GetLastLine()
{
	int NumL = GetNumLines();
	string ln = "";

	if(NumL <= 0)
	{
		CurrentLine = -1;
		return ln;
	}

	int Line = NumL - 1;
	int LineStart, LineEnd;
	bool NoEndLineChars = GetLinePos(Line, &LineStart, &LineEnd);
	int lng = LineEnd - LineStart;

	if(NoEndLineChars == false)
		lng -= 2;

	ln = FullFile.substr(LineStart, lng);
	PosInFile = LineStart + (int)ln.length();
	CurrentLine = Line;
	return ln;
}

//-------------------------------------------------------------------------------------------------
int TxtFile::GetNextLine(char *array)
{
	strcpy(array, GetNextLine().c_str());
	return CurrentLine;
}

//-------------------------------------------------------------------------------------------------
string TxtFile::GetNextLine()
{
	string ln = "";
	int lng;

	if(PosInFile >= FullFile.length())	//koniec suboru
	{
		CurrentLine = -1;
		return ln;
	}

	CurrentLine++;
	int LastPosInFile = PosInFile;
	PosInFile = (int)FullFile.find_first_of(char(10), LastPosInFile + 1) + 1;

	if(PosInFile > 0)	//koniec lajny
	{
		lng = PosInFile - LastPosInFile;
		ln = FullFile.substr(LastPosInFile, lng - 2);
		return ln;
	}
	else	//vynimka pre koniec suboru
	{
		if(FullFile.length() <= 0)	//otvorili sme prazdny subor
		{
			CurrentLine = -1;
			return ln;
		}

		lng = (int)FullFile.length() - LastPosInFile;
		ln = FullFile.substr(LastPosInFile, lng);
		PosInFile = LastPosInFile + lng;
		return ln;
	}
}

//-------------------------------------------------------------------------------------------------
TxtLine TxtFile::GetNextLineEx()
{
	return TxtLine(Parser, GetNextLine());
}

//-------------------------------------------------------------------------------------------------
bool TxtFile::SetLine(int Line, const char* array)
{
	if(Line < 0 || Line >= GetNumLines())
		return false;

	int LineStart, LineEnd;
	bool LastLine = GetLinePos(Line, &LineStart, &LineEnd);
	FullFile.erase(LineStart, LineEnd - LineStart);
	char temp[MaxLineLength];
	strcpy(temp, array);
	strcat(temp, EndLineChars);
	FullFile.insert(LineStart, temp);
	PosInFile = LineStart + (int)strlen(temp);
	return true;
}

//-------------------------------------------------------------------------------------------------
bool TxtFile::SetLine(int Line, string& NewText)
{
	return SetLine(Line, NewText.c_str());
}

//-------------------------------------------------------------------------------------------------
bool TxtFile::SetLine(int Line, TxtLine& NewLine)
{
	char temp[MaxLineLength];
	NewLine.GetText(temp);
	return SetLine(Line, temp);
}

//-------------------------------------------------------------------------------------------------
int TxtFile::GetPreviousLine(char *array)
{
	if(CurrentLine <= 0)
		return -1;

	PosInFile = (int)FullFile.find_last_of(char(10), PosInFile - 2) + 1;
	PosInFile = (int)FullFile.find_last_of(char(10), PosInFile - 2) + 1;
	CurrentLine -= 2;	//odpocitame dva lajny pretoze GetNextLine jednu prida
	return GetNextLine(array);
}

//-------------------------------------------------------------------------------------------------
string TxtFile::GetPreviousLine()
{
	if(CurrentLine <= 0)
		return "";

	PosInFile = (int)FullFile.find_last_of(char(10), PosInFile - 2) + 1;
	PosInFile = (int)FullFile.find_last_of(char(10), PosInFile - 2) + 1;
	CurrentLine -= 2;	//odpocitame dva lajny pretoze GetNextLine jednu prida
	return GetNextLine();
}

//-------------------------------------------------------------------------------------------------
TxtLine TxtFile::GetPreviousLineEx()
{
	if(CurrentLine <= 0)
		return TxtLine(Parser);

	PosInFile = (int)FullFile.find_last_of(char(10), PosInFile - 2) + 1;
	PosInFile = (int)FullFile.find_last_of(char(10), PosInFile - 2) + 1;
	CurrentLine -= 2;	//odpocitame dva lajny pretoze GetNextLine jednu prida
	return GetNextLineEx();
}

//-------------------------------------------------------------------------------------------------
int TxtFile::GetCurrentLine(char *array)
{
	if(array != NULL)
		strcpy(array, GetCurrentLine().c_str());

	return CurrentLine;
}

//-------------------------------------------------------------------------------------------------
string TxtFile::GetCurrentLine()
{
	if(CurrentLine < 0)
		return "";

	if(PosInFile >= FullFile.length())	//koniec suboru
	{
		CurrentLine = -1;
//		return "";
	}

	int LineStart = (int)FullFile.find_last_of(char(10), PosInFile - 2) + 1;
	int lng = PosInFile - LineStart;
	string ln = FullFile.substr(LineStart, lng - 2);
	return ln;
}

//-------------------------------------------------------------------------------------------------
TxtLine TxtFile::GetCurrentLineEx()
{
	return TxtLine(Parser, GetCurrentLine());
}

//-------------------------------------------------------------------------------------------------
int TxtFile::SetCurrentLine(int Line)
{
	if(Line < -1 || Line >= GetNumLines())
		return -1;

	if(Line == -1)
	{
		PosInFile = 0;
		CurrentLine = Line;
		return CurrentLine;
	}

	int LineStart, LineEnd;
	bool WithoutEndLineChars = GetLinePos(Line, &LineStart, &LineEnd);

	PosInFile = LineEnd;
	CurrentLine = Line;
	return CurrentLine;
}

//-------------------------------------------------------------------------------------------------
int TxtFile::RemoveComments()
{
//----- Vyhodime zo stringu viacriadkove poznamky
	string MLCStart = "/*";
	string MLCEnd = "*/";
	int CommentStart, CommentEnd;
	const char *Array, *start, *end;
	bool MLCAvailable = true;
	int NumComments = 0;

	while(MLCAvailable)	//viacriadkove
	{
		Array = FullFile.c_str();
		start = strstr(Array, MLCStart.c_str());
		end = strstr(Array, MLCEnd.c_str());

		if(start && end)
		{
			CommentStart = (int)(start - Array);
			CommentEnd = (int)(end - Array) + 2;
			FullFile.erase(CommentStart, CommentEnd - CommentStart);
			NumComments++;
			MLCAvailable = true;
		}
		else
			MLCAvailable = false;
	}

	string SLC = "//";
	bool SLCAvailable = true;	//jednoriadkove
	while(SLCAvailable)
	{
		Array = FullFile.c_str();
		start = strstr(Array, SLC.c_str());

		if(start)
		{
			CommentStart = (int)(start - Array);
			CommentEnd = (int)FullFile.find_first_of(char(10), CommentStart) - 1;
			FullFile.erase(CommentStart, CommentEnd - CommentStart);
			NumComments++;
			SLCAvailable = true;
		}
		else
			SLCAvailable = false;
	}
	return NumComments;
}

bool TxtFile::Save(const char *FileName)
{
	HANDLE hFile = CreateFile(FileName,GENERIC_WRITE,FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if(hFile == INVALID_HANDLE_VALUE) 
	{ 
		printf("Could not open file (error %d)\n", GetLastError());
		return false;
	}

	unsigned int NumToWrite = (int)FullFile.length();
	unsigned int NumWrited = 0;
	bTest = (WriteFile(hFile, FullFile.c_str(), NumToWrite, (LPDWORD)&NumWrited, NULL) != 0);

	if(!bTest)
		return false;

	bTest = (CloseHandle(hFile) == TRUE);
	
	if(bTest)
		return true;

	return false;
}

//-------------------------------------------------------------------------------------------------
bool TxtFile::Open(const char *FileName)
{
	Clear();
	//skontrolujeme si ci subor vobec exituje a potrebujeme od toho aj handle pre GetFileSize 
	HANDLE hFile = CreateFile(FileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile == INVALID_HANDLE_VALUE) 
	{ 
		printf("Could not open file (error %d)\n", GetLastError());
		return false;
	}

	unsigned int FileSize = GetFileSize(hFile, NULL);
	if(FileSize == INVALID_FILE_SIZE)
		return false;

	unsigned int dwNumRead = 0;
	//naalokujeme do toho pamet pre subor podla dlzky suboru + 1 byte na znak '\0' ktory tam prida ReadFile
	char* dwBuffer = (char*)calloc(FileSize + 1, sizeof(char));
	bTest = (ReadFile(hFile, dwBuffer, FileSize, (LPDWORD)&dwNumRead,NULL) != 0);	//nacitame subor

	if(dwNumRead != FileSize)													//dlzka musi sediet inac je nieco v prdeli
		return false;

	bTest = (CloseHandle(hFile) == TRUE);

	if(!bTest)
		return false;																//ak sa nepodari uzavriet subor tak je nieco v prdeli a nepokracujeme dalej

	FullFile = dwBuffer;
	free(dwBuffer);
	return true;
}


//-------------------------------------------------------------------------------------------------
TxtLine::TxtLine(TextParser* parser)
{
	Parser = parser;
	Clear();
}

//-------------------------------------------------------------------------------------------------
TxtLine::TxtLine(TextParser* parser, const char* array)
{
	Parser = parser;
	Clear();
	SetText(array);
}

//-------------------------------------------------------------------------------------------------
TxtLine::TxtLine(TextParser* parser, string& text)
{
	Parser = parser;
	Clear();
	SetText(text);
}

//-------------------------------------------------------------------------------------------------
TxtLine::~TxtLine()
{
}

//-------------------------------------------------------------------------------------------------
void TxtLine::Clear()
{
	NumTokens = 0;
	Buffer.clear();

	for(int n = 0; n < (sizeof(tokens) / sizeof(string)); n++)
		tokens[n].clear();
}

//-------------------------------------------------------------------------------------------------
void TxtLine::ReparseTokens()
{
	NumTokens = Parser->ParseLine(Buffer, tokens, TokensPos);
}

//-------------------------------------------------------------------------------------------------
bool TxtLine::SetText(string& text)
{
	Buffer = text;
	ReparseTokens();
	return true;
}

//-------------------------------------------------------------------------------------------------
bool TxtLine::SetText(const char* array)
{
	Buffer.clear();
	Buffer.append(array);
	ReparseTokens();
	return true;
}

//-------------------------------------------------------------------------------------------------
void TxtLine::GetText(char* array)
{
	strcpy(array, Buffer.c_str());
}

//-------------------------------------------------------------------------------------------------
string TxtLine::GetText()
{
	return Buffer;
}

//-------------------------------------------------------------------------------------------------
int TxtLine::GetNumTokens()
{
	return NumTokens;
}

//-------------------------------------------------------------------------------------------------
bool TxtLine::GetToken(int index, char* array)
{
	strcpy(array, GetToken(index).c_str());
	return true;
}

//-------------------------------------------------------------------------------------------------
string TxtLine::GetToken(int index)
{
	if(index < 0)
		index = 0;
	
	if(index >= NumTokens)
		index = NumTokens - 1;

	return tokens[index];
}

//-------------------------------------------------------------------------------------------------
bool TxtLine::SetToken(int index, string& NewText)
{
	if(index < 0 || index >= NumTokens)
		return false;

	string str = tokens[index];
	int OldSize = (int)tokens[index].length();
	tokens[index] = NewText;

	int StartPos = TokensPos[index];
	int EndPos = StartPos + OldSize;
	Buffer.erase(StartPos, OldSize);
	string ToInsert = tokens[index];
	Buffer.insert(StartPos, ToInsert);

	int Offset = (int)tokens[index].length() - OldSize;

	for(int n = index + 1; n < NumTokens; n++)
		TokensPos[n] += Offset;

	return true;
}

//-------------------------------------------------------------------------------------------------
bool TxtLine::RemoveToken(int index)
{
	if(index < 0 || index >= NumTokens)
		return false;

	int Size = (int)tokens[index].length();
	int StartPos = TokensPos[index];
	int EndPos = StartPos + Size;

	if((int)Buffer.length() > EndPos && Buffer[EndPos] == ' ')
		Size++;	//zmazeme medzeru za tym

	Buffer.erase(StartPos, Size);
	string hh = Buffer;
	int Offset = Size;
	NumTokens--;

	for(int n = index; n < NumTokens; n++)
	{
		TokensPos[n] = TokensPos[n + 1] - Offset;
		tokens[n] = tokens[n + 1];
	}
	return true;
}

//-------------------------------------------------------------------------------------------------
bool TxtLine::InsertToken(int index, string& Token)
{
	if(index < 0)
		return false;

	if(index > NumTokens)
		index = NumTokens;

	string ToInsert = "";
	int StartPos = 0;

	if(NumTokens > 0)
	{
		if(index == NumTokens)	//pridavame novy token na koniec
		{
			TokensPos[index] = TokensPos[index - 1] + (int)tokens[index - 1].length() + 1;
			ToInsert += " ";
			StartPos = (int)Buffer.length();
		}
		else
			StartPos = TokensPos[index];
	}

	ToInsert += Token;

	if(index < NumTokens)	//neni to novy token na konci
		ToInsert += " ";

	Buffer.insert(StartPos, ToInsert);
	NumTokens++;

	int Offset = (int)ToInsert.length();

	for(int n = NumTokens - 1; n > index; n--)
	{
		TokensPos[n] = TokensPos[n - 1] + Offset;
		tokens[n] = tokens[n - 1];
	}

	tokens[index] = Token;
	return true;
}

int TxtLine::FindSingleLineComment(int FromToken)			//vracia index tokenu ktory je "//"
{
	for(int n = FromToken; n < NumTokens; n++)
	{
		if(tokens[n] == "//")
			return n;
	}
	return -1;
}

int TxtLine::FindMultiLineCommentStart(int FromToken)	//vracia index tokenu ktory je "/*"
{
	for(int n = FromToken; n < NumTokens; n++)
	{
		if(tokens[n] == "/*")
			return n;
	}
	return -1;
}

int TxtLine::FindMultiLineCommentEnd(int FromToken)		//vracia index tokenu ktory je "*/"
{
	for(int n = FromToken; n < NumTokens; n++)
	{
		if(tokens[n] == "*/")
			return n;
	}
	return -1;
}

//ak bolo predtym nadefinovane unicode tak ho nadefinujeme znova
#ifdef UNICODE_BACK
#undef UNICODE_BACK
#define UNICODE
#endif

#ifdef _UNICODE_BACK
#undef _UNICODE_BACK
#define _UNICODE
#endif