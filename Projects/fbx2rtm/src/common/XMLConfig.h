
#ifndef XML_CONFIG_H
#define XML_CONFIG_H

#include "common/enf_mathlib.h"
//#include "common/enf_hashmap.h"
#ifdef ENF_LEAK_DETECTOR
	#include "common/enf_memory.h"
#endif
#include "wx/string.h"

class TiXmlDocument;
class TiXmlElement;
class XMLConfig;

#ifdef WORKBENCH
	class FileSystem;
#endif

//========================================================================================
class XMLConfigNode
{
public:
						XMLConfigNode(XMLConfig* config, const char* path, TiXmlElement* element);
						~XMLConfigNode();
	XMLConfigNode*	GetChildNode(const char* name, bool CreateIfNotExist = false);
	XMLConfigNode*	GetChildNodeWithAttr(const char* name, const char* AttrName, const char* AttrValue, bool CreateIfNotExist);
	XMLConfigNode*	GetFirstChildNode();
	XMLConfigNode*	GetNextSiblingNode();
	XMLConfigNode*	AppendChildNode(const char* name);
	const char*		GetPath();
	const char*		GetName();
	const char*		GetStringValue(const char* property);
	int				GetIntValue(const char* property);
	float				GetFloatValue(const char* property);
	bool				GetValue(const char* property, char* value);
	bool			 	GetValue(const char* property, int& val);
	bool				GetValue(const char* property, float& val);
	bool				GetValue(const char* property, bool& val);
	bool				GetValue(const char* property, wxString& val);
	bool				GetValue(const char* property, Vector3& val);

	bool				GetAttributeValue(const char* attribute, char* value);
	bool			 	GetAttributeValue(const char* attribute, int& val);
	bool				GetAttributeValue(const char* attribute, float& val);
	bool				GetAttributeValue(const char* attribute, bool& val);
	bool				GetAttributeValue(const char* attribute, wxString& val);
	bool				GetAttributeValue(const char* attribute, Vector3& val);

	void				SetAttributeValue(const char* attribute, char* value);
	void			 	SetAttributeValue(const char* attribute, int& val);
	void				SetAttributeValue(const char* attribute, float& val);
	void				SetAttributeValue(const char* attribute, bool& val);
	void				SetAttributeValue(const char* attribute, wxString& val);
	void				SetAttributeValue(const char* attribute, Vector3& val);

	bool				SetValue(const char* property, const char* value);
	bool			 	SetValue(const char* property, int val);
	bool				SetValue(const char* property, float val);
	bool				SetValue(const char* property, bool val);
	bool				SetValue(const char* property, wxString& val);
	bool				SetValue(const char* property, Vector3& val);
#ifdef ENF_LEAK_DETECTOR
	ENF_DECLARE_MMOBJECT(XMLConfigNode);
#endif
private:
	XMLConfig*		config;
	TiXmlElement*	element;
	char*				path;
};

//========================================================================================
class XMLConfig
{
	friend class XMLConfigNode;
public:
						XMLConfig(const char* file);
						~XMLConfig();
	bool				Load();
	bool				Save();
	const char*		GetStringValue(const char* path);
	int				GetIntValue(const char* path);
	float				GetFloatValue(const char* path);
	bool				SetValue(const char* path, const char* value);
	bool			 	SetValue(const char* path, int val);
	bool				SetValue(const char* path, float val);
	XMLConfigNode*	GetNode(const char* path, bool CreateIfNotExist = false);
	XMLConfigNode*	AddNode(const char* path);

#ifdef ENF_LEAK_DETECTOR
	ENF_DECLARE_MMOBJECT(XMLConfig);
#endif

private:
#ifdef WORKBENCH
	FileSystem*		FSystem;
#endif
	TiXmlDocument* doc;
	TiXmlElement*	InsertEmptyPath(const char* path);
	TiXmlElement*	FindElement(const char* path);
	TiXmlElement*	FindChildElement(TiXmlElement* parent, const char* name);
	TiXmlElement*	FindChildElementWithAttr(TiXmlElement* parent, const char* name, const char* AttrName, const char* AttrValue);
	TiXmlElement*	InsertChildElement(TiXmlElement* parent, const char* name);
	XMLConfigNode*	FindRequestedNode(const char* path);
//	void				ParseElementPaths(TiXmlElement* element, const char* RootPath);
//	EHashMap<const char*, TiXmlElement*, StringHashFunc> PathsHash;
	EArray<XMLConfigNode*>	RequestedNodes;
	char FileName[256];
};

#endif