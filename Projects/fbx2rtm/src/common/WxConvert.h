
#ifndef OWN_WXCONVERT_H
#define OWN_WXCONVERT_H

#include "WxNativeConvert.h"
#include "common/enf_mathlib.h"

class wxImage;

Vector3		StrToVec(const wxString& str);
wxString		VecToStr(const Vector3& vec, unsigned char NumFloatUnits = 6);

#endif