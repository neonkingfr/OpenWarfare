//-----------------------------------------------------------------------------
// File: enf_matrix.cpp
//
// Desc: Mathematics library, Matrix classes
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "common/enf_memory.h"
#include "common/enf_mathlib.h"
#ifndef ENF_XENON
#include <d3dx9math.h>
#endif

//-----------------------------------------------------------------------
Matrix44 SetupViewMatrix(const Matrix43& viewmatrix)
{
	Matrix44 mmview;

	Vector3 from(viewmatrix[3].x, viewmatrix[3].y, -viewmatrix[3].z);
	Vector3 pat = viewmatrix[0];
	Vector3 pright = -viewmatrix[1];
	Vector3 pup = -viewmatrix[2];

	mmview._11 = pright.x;  mmview._12 = pup.x;  mmview._13 = pat.x;  mmview._14 = 0;
	mmview._21 = pright.y;  mmview._22 = pup.y;  mmview._23 = pat.y;  mmview._24 = 0;
	mmview._31 = -pright.z;  mmview._32 = -pup.z;  mmview._33 = -pat.z;  mmview._34 = 0;

	mmview._41 = -pright.Dot(from);
	mmview._42 = -pup.Dot(from);
	mmview._43 = -pat.Dot(from);
	mmview._44 = 1.0f;
	
	return mmview;
}

//-----------------------------------------------------------------------
Matrix44::Matrix44(const Matrix43& mat)
{
	_11 = mat._11; _12 = mat._12; _13 = mat._13; _14 = 0.0f;
	_21 = mat._21; _22 = mat._22; _23 = mat._23; _24 = 0.0f;
	_31 = mat._31; _32 = mat._32; _33 = mat._33; _34 = 0.0f;
	_41 = mat._41; _42 = mat._42; _43 = mat._43; _44 = 1.0f;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::RollPitchYaw(float roll, float pitch, float yaw)
{
	if(roll == 0.0f && pitch == 0.0f && yaw == 0.0f)
	{
		_11 = 1.0f;
		_12 = 0.0f;
		_13 = 0.0f;
		_14 = 0.0f;

		_21 = 0.0f;
		_22 = 1.0f;
		_23 = 0.0f;
		_24 = 0.0f;

		_31 = 0.0f;
		_32 = 0.0f;
		_33 = 1.0f;
		_34 = 0.0f;
	}
	else
	{
		float		angle;
		static float		sr, sp, sy, cr, cp, cy;

		angle = ENF_DEG2RAD(yaw);
		sy = sinf(angle);
		cy = cosf(angle);
		angle = ENF_DEG2RAD(pitch);
		sp = sinf(angle);
		cp = cosf(angle);
		angle = ENF_DEG2RAD(roll);
		sr = sinf(angle);
		cr = cosf(angle);
		
		_11 = cp * cy;
		_12 = cp * sy;
		_13 = -sp;
		_14 = 0.0f;

		_21 = -(-1*sr*sp*cy+-1*cr*-sy);
		_22 = -(-1*sr*sp*sy+-1*cr*cy);
		_23 = sr*cp;
		_24 = 0.0f;

		_31 = (cr*sp*cy+-sr*-sy);
		_32 = (cr*sp*sy+-sr*cy);
		_33 = cr*cp;
		_34 = 0.0f;
	}

///////////
	return *this;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::Roll(float roll)
{
	roll = ENF_DEG2RAD(roll);
	float sr = sinf(roll);
	float cr = cosf(roll);

	_11 = 1.0f;
	_12 = 0.0f;
	_13 = 0.0f;
	_14 = 0.0f;

	_21 = 0.0f;
	_22 = cr;
	_23 = sr;
	_24 = 0.0f;

	_31 = 0.0f;
	_32 = -sr;
	_33 = cr;
	_34 = 0.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::Yaw(float yaw)
{
	yaw = ENF_DEG2RAD(yaw);
	float sy = sinf(yaw);
	float cy = cosf(yaw);

	_11 = cy;
	_12 = sy;
	_13 = 0.0f;
	_14 = 0.0f;

	_21 = -sy;
	_22 = cy;
	_23 = 0.0f;
	_24 = 0.0f;

	_31 = 0.0f;
	_32 = 0.0f;
	_33 = 1.0f;
	_34 = 0.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::Pitch(float pitch)
{
	pitch = ENF_DEG2RAD(pitch);
	float sp = sinf(pitch);
	float cp = cosf(pitch);

	_11 = cp;
	_12 = 0.0f;
	_13 = -sp;
	_14 = 0.0f;

	_21 = 0.0f;
	_22 = 1.0f;
	_23 = 0.0f;
	_24 = 0.0f;

	_31 = sp;
	_32 = 0.0f;
	_33 = cp;
	_34 = 0.0f;

///////////
	return *this;
}

//-----------------------------------------------------------------------
Vector4 Matrix44::TransformPlane(const Vector4& in) const
{
	Vector4 out;

	out.x = in.x * _11 + in.y * _21 + in.z * _31;
	out.y = in.x * _12 + in.y * _22 + in.z * _32;
	out.z = in.x * _13 + in.y * _23 + in.z * _33;
	out.w = in.w + (out.x * _41 + out.y * _42 + out.z * _43);

	return out;
}

//-----------------------------------------------------------------------
Matrix44 Matrix44::PerspectiveFovLH(float fovy, float aspect, float zn, float zf)
{
	float yScale = 1.0f / tan(fovy / 2);
	float xScale = yScale / aspect;
	_11 = xScale; _12 = 0; _13 = 0; _14 = 0;
	_21 = 0; _22 = yScale; _23 = 0; _24 = 0;
	_31 = 0; _32 = 0; _33 = zf / (zf - zn); _34 = 1.0f;
	_41 = 0; _42 = 0; _43 = -zn * zf / (zf - zn); _44 = 0;
	return *this;
}

//-----------------------------------------------------------------------
Matrix44 Matrix44::OrthoLH(float width, float height, float zn, float zf)
{
	_11 = 2.0f / width; _12 = 0.0f; _13 = 0.0f; _14 = 0.0f;
	_21 = 0.0f; _22 = 2.0f / height; _23 = 0.0f; _24 = 0.0f;
	_31 = 0.0f; _32 = 0.0f; _33 = 1.0f /(zn - zf); _34 = 0.0f;
	_41 = 0.0f; _42 = 0.0f; _43 = zn / (zn - zf); _44 = 1.0f;
	return *this;
}

//-----------------------------------------------------------------------
Matrix44 Matrix44::OrthoRH(float width, float height, float zn, float zf)
{
	_11 = 2.0f / width; _12 = 0.0f; _13 = 0.0f; _14 = 0.0f;
	_21 = 0.0f; _22 = 2.0f / height; _23 = 0.0f; _24 = 0.0f;
	_31 = 0.0f; _32 = 0.0f; _33 = 1.0f /(zf - zn); _34 = 0.0f;
	_41 = 0.0f; _42 = 0.0f; _43 = -zn / (zf - zn); _44 = 1.0f;
	return *this;
}

//-----------------------------------------------------------------------
Matrix44 Matrix44::Ident(
	 1.0f, 0.0f, 0.0f, 0.0f,
	 0.0f, 1.0f, 0.0f, 0.0f,
	 0.0f, 0.0f, 1.0f, 0.0f,
	 0.0f, 0.0f, 0.0f, 1.0f);

//-----------------------------------------------------------------------
Matrix44 Matrix44::GetInverse() const
{
	ALIGN16 Matrix44 result;

	D3DXMatrixInverse((D3DXMATRIX *)&result, NULL, (D3DXMATRIX *)this);
	return result;
}

//-----------------------------------------------------------------------
Matrix44 Matrix44::GetTranspose() const
{
	ALIGN16 Matrix44 result;

#ifdef ENF_SSE
	__m128 a0 = _mm_loadu_ps(&_11);
	__m128 a1 = _mm_loadu_ps(&_21);
	__m128 a2 = _mm_loadu_ps(&_31);
	__m128 a3 = _mm_loadu_ps(&_41);

	_MM_TRANSPOSE4_PS(a0, a1, a2, a3);
	
	_mm_storeu_ps(&result._11, a0);
	_mm_storeu_ps(&result._21, a1);
	_mm_storeu_ps(&result._31, a2);
	_mm_storeu_ps(&result._41, a3);
#else
	D3DXMatrixTranspose((D3DXMATRIX *)&result, (D3DXMATRIX *)this);
#endif
	return result;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::operator=(const Quaternion& quat)
{
	if(quat.v[0] == 0.0f && quat.v[1] == 0.0f && quat.v[2] == 0.0f && quat.v[3] == 1.0f)
	{
		Identity();
		return *this;
	}

	float	wx, wy, wz;
	float	xx, yy, yz;
	float	xy, xz, zz;
	float	x2, y2, z2;

	x2 = quat.x + quat.x;
	y2 = quat.y + quat.y;
	z2 = quat.z + quat.z;

	xx = quat.x * x2;
	xy = quat.x * y2;
	xz = quat.x * z2;

	yy = quat.y * y2;
	yz = quat.y * z2;
	zz = quat.z * z2;

	wx = quat.w * x2;
	wy = quat.w * y2;
	wz = quat.w * z2;

	_11 = 1.0f - ( yy + zz );
	_12 = xy + wz;
	_13 = xz - wy;
	_14 = 0.0f;

	_21 = xy - wz;
	_22 = 1.0f - ( xx + zz );
	_23 = yz + wx;
	_24 = 0.0f;

	_31 = xz + wy;
	_32 = yz - wx;
	_33 = 1.0f - ( xx + yy );
	_34 = 0.0f;

	_41 = 0.0f;
	_42 = 0.0f;
	_43 = 0.0f;
	_44 = 1.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::operator=(const Quaternion* quat)
{
	if(quat->v[0] == 0.0f && quat->v[1] == 0.0f && quat->v[2] == 0.0f && quat->v[3] == 1.0f)
	{
		Identity();
		return *this;
	}

	float	wx, wy, wz;
	float	xx, yy, yz;
	float	xy, xz, zz;
	float	x2, y2, z2;

	x2 = quat->x + quat->x;
	y2 = quat->y + quat->y;
	z2 = quat->z + quat->z;

	xx = quat->x * x2;
	xy = quat->x * y2;
	xz = quat->x * z2;

	yy = quat->y * y2;
	yz = quat->y * z2;
	zz = quat->z * z2;

	wx = quat->w * x2;
	wy = quat->w * y2;
	wz = quat->w * z2;

	_11 = 1.0f - ( yy + zz );
	_12 = xy + wz;
	_13 = xz - wy;
	_14 = 0.0f;

	_21 = xy - wz;
	_22 = 1.0f - ( xx + zz );
	_23 = yz + wx;
	_24 = 0.0f;

	_31 = xz + wy;
	_32 = yz - wx;
	_33 = 1.0f - ( xx + yy );
	_34 = 0.0f;

	_41 = 0.0f;
	_42 = 0.0f;
	_43 = 0.0f;
	_44 = 1.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::operator*=(const Matrix44& mat)
{
	D3DXMatrixMultiply((D3DXMATRIX *)this,
		(CONST D3DXMATRIX *)this,
		(CONST D3DXMATRIX *)&mat);

	return *this;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::operator*=(const Matrix44* mat)
{
	D3DXMatrixMultiply((D3DXMATRIX *)this,
		(CONST D3DXMATRIX *)this,
		(CONST D3DXMATRIX *)mat);

	return *this;
}

//-----------------------------------------------------------------------
Matrix44 Matrix44::operator *(const Matrix44& mat) const
{
	Matrix44 result;

	D3DXMatrixMultiply((D3DXMATRIX *)&result,
		(CONST D3DXMATRIX *)this,
		(CONST D3DXMATRIX *)&mat);

	return result;
}

//-----------------------------------------------------------------------
Matrix44 Matrix44::operator *(const Matrix44* mat) const
{
	Matrix44 result;

	D3DXMatrixMultiply((D3DXMATRIX *)&result,
		(CONST D3DXMATRIX *)this,
		(CONST D3DXMATRIX *)mat);

	return result;
}

//-----------------------------------------------------------------------
Matrix44 Matrix44::MultiplyTranspose(const Matrix44& mat) const
{
	Matrix44 result;

	D3DXMatrixMultiplyTranspose((D3DXMATRIX *)&result,
		(CONST D3DXMATRIX *)this,
		(CONST D3DXMATRIX *)&mat);

	return result;
}

//-----------------------------------------------------------------------
Vector3 Matrix44::TransformCoord(const Vector3& in) const
{
	Vector3 out;
	D3DXVec3TransformCoord((D3DXVECTOR3 *)&out, (CONST D3DXVECTOR3 *)&in, (CONST D3DXMATRIX *)this);

/*
	out.x = in.x * _11 + in.y * _21 + in.z * _31 + _41;
	out.y = in.x * _12 + in.y * _22 + in.z * _32 + _42;
	out.z = in.x * _13 + in.y * _23 + in.z * _33 + _43;
*/
	return out;
}

//-----------------------------------------------------------------------
Vector3 Matrix44::TransformVector(const Vector3& vec) const
{
	Vector3 result;
	
	D3DXVec3TransformNormal((D3DXVECTOR3 *)&result, (CONST D3DXVECTOR3 *)&vec, (CONST D3DXMATRIX *)this);

	return result;
}

//-----------------------------------------------------------------------------
Vector4 Matrix44::InvTransformCoord(const Vector3& in) const
{
	float tmpx = in.x - _41;
	float tmpy = in.y - _42;
	float tmpz = in.z - _43;

	return Vector4(tmpx * _11 + tmpy * _12 + tmpz * _13, tmpx * _21 + tmpy * _22 + tmpz * _23, tmpx * _31 + tmpy * _32 + tmpz * _33, 0.0f);
}

//-----------------------------------------------------------------------------
Vector4 Matrix44::InvTransformCoord(const Vector4& in) const
{
	float tmpx = in.x - _41;
	float tmpy = in.y - _42;
	float tmpz = in.z - _43;

	return Vector4(tmpx * _11 + tmpy * _12 + tmpz * _13, tmpx * _21 + tmpy * _22 + tmpz * _23, tmpx * _31 + tmpy * _32 + tmpz * _33, 0.0f);
}

//-----------------------------------------------------------------------------
Vector4 Matrix44::InvTransformVector(const Vector3& in) const
{
	Vector4 out;

	out.x = in.x * _11 + in.y * _12 + in.z * _13;
	out.y = in.x * _21 + in.y * _22 + in.z * _23;
	out.z = in.x * _31 + in.y * _32 + in.z * _33;
	out.w = 0.0f;

	return out;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::operator=(const Matrix43& mat)
{
	_11 = mat._11;	_12 = mat._12;	_13 = mat._13;	_14 = 0.0f;
	_21 = mat._21;	_22 = mat._22;	_23 = mat._23;	_24 = 0.0f;
	_31 = mat._31;	_32 = mat._32;	_33 = mat._33;	_34 = 0.0f;
	_41 = mat._41;	_42 = mat._42;	_43 = mat._43;	_44 = 1.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix44& Matrix44::operator=(const Matrix43* mat)
{
	_11 = mat->_11; _12 = mat->_12; _13 = mat->_13; _14 = 0.0f;
	_21 = mat->_21; _22 = mat->_22; _23 = mat->_23; _24 = 0.0f;
	_31 = mat->_31; _32 = mat->_32; _33 = mat->_33; _34 = 0.0f;
	_41 = mat->_41; _42 = mat->_42; _43 = mat->_43; _44 = 1.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix43 Matrix43::Ident(
	 1.0f, 0.0f, 0.0f,
	 0.0f, 1.0f, 0.0f,
	 0.0f, 0.0f, 1.0f,
	 0.0f, 0.0f, 0.0f);

//-----------------------------------------------------------------------
bool Matrix43::IsOrthogonal() const
{
	return true;
}

const float ENF_NORMALIZED_EPSILON = 0.00001f;
const float ENF_COLINEAR_EPSILON = 0.0001f;

//-----------------------------------------------------------------------
bool Matrix43::IsNormalized() const
{
	return (fabs(1.0f - GetRow(0).LengthSq()) < ENF_NORMALIZED_EPSILON &&
		fabs(1.0f - GetRow(1).LengthSq()) < ENF_NORMALIZED_EPSILON &&
		fabs(1.0f - GetRow(2).LengthSq()) < ENF_NORMALIZED_EPSILON);
}

//-----------------------------------------------------------------------
bool Matrix43::IsOrthoNormal() const
{
	if(IsNormalized())
	{
		Vector3 cr = GetRow(0) ^ GetRow(1);
		return (fabs(1.0f - cr.Dot(GetRow(2))) < ENF_COLINEAR_EPSILON);
	}
	return false;
}

//-----------------------------------------------------------------------
bool Matrix43::operator==(const Matrix43& mat) const
{
	return (_11 == mat._11 && _12 == mat._12 && _13 == mat._13 &&
			_21 == mat._21 && _22 == mat._22 && _23 == mat._23 &&
			_31 == mat._31 && _32 == mat._32 && _33 == mat._33 &&
			_41 == mat._41 && _42 == mat._42 && _43 == mat._43);
}

//-----------------------------------------------------------------------
bool Matrix43::IsIdentity() const
{
	return (_11 == 1.0f && _12 == 0.0f && _13 == 0.0f &&
			_21 == 0.0f && _22 == 1.0f && _23 == 0.0f &&
			_31 == 0.0f && _32 == 0.0f && _33 == 1.0f &&
			_41 == 0.0f && _42 == 0.0f && _43 == 0.0f);
}

//-----------------------------------------------------------------------
Matrix43 Matrix43::GetInverse() const
{
	float det = GetDeterminant();

	if(det == 0.0f)
	{
		return Matrix43(
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			-_41, -_42, -_43);
	}

	Matrix43 tmp;

	float detInv = 1 / det;

	tmp._11 = (_22 * _33 - _32 * _23) * detInv;
	tmp._21 = -1 * (_21 * _33 - _31 * _23) * detInv;
	tmp._31 = (_21 * _32 - _31 * _22) * detInv;
	tmp._12 = -1 * (_12 * _33 - _32 * _13) * detInv;
	tmp._22 = (_11 * _33 - _31 * _13) * detInv;
	tmp._32 = -1 * (_11 * _32 - _31 * _12) * detInv;
	tmp._13 = (_12 * _23 - _22 * _13) * detInv;
	tmp._23 = -1 * (_11 * _23 - _21 * _13) * detInv;
	tmp._33 = (_11 * _22 - _21 * _12) * detInv;
	tmp._41 = -_41 * tmp._11 + -_42 * tmp._21 + -_43 * tmp._31;
	tmp._42 = -_41 * tmp._12 + -_42 * tmp._22 + -_43 * tmp._32;
	tmp._43 = -_41 * tmp._13 + -_42 * tmp._23 + -_43 * tmp._33;

	return tmp;
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::operator=(const Matrix44& mat)
{
	_11 = mat._11;	_12 = mat._12;	_13 = mat._13;
	_21 = mat._21;	_22 = mat._22;	_23 = mat._23;
	_31 = mat._31;	_32 = mat._32;	_33 = mat._33;
	_41 = mat._41;	_42 = mat._42;	_43 = mat._43;
	return *this;
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::operator=(const Matrix44* mat)
{
	_11 = mat->_11; _12 = mat->_12; _13 = mat->_13;
	_21 = mat->_21; _22 = mat->_22; _23 = mat->_23;
	_31 = mat->_31; _32 = mat->_32; _33 = mat->_33;
	_41 = mat->_41; _42 = mat->_42; _43 = mat->_43;
	return *this;
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::operator=(const Quaternion& quat)
{
	if(quat.x == 0.0f && quat.y == 0.0f && quat.z == 0.0f && (quat.w == 1.0f || quat.w == 0.0f))
	{
		Identity();
		return *this;
	}

	float	wx, wy, wz;
	float	xx, yy, yz;
	float	xy, xz, zz;
	float	x2, y2, z2;

	x2 = quat.x + quat.x;
	y2 = quat.y + quat.y;
	z2 = quat.z + quat.z;

	xx = quat.x * x2;
	xy = quat.x * y2;
	xz = quat.x * z2;

	yy = quat.y * y2;
	yz = quat.y * z2;
	zz = quat.z * z2;

	wx = quat.w * x2;
	wy = quat.w * y2;
	wz = quat.w * z2;

	_11 = 1.0f - ( yy + zz );
	_12 = xy + wz;
	_13 = xz - wy;

	_21 = xy - wz;
	_22 = 1.0f - ( xx + zz );
	_23 = yz + wx;

	_31 = xz + wy;
	_32 = yz - wx;
	_33 = 1.0f - ( xx + yy );

	_41 = 0.0f;
	_42 = 0.0f;
	_43 = 0.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::operator=(const Quaternion* quat)
{
	if(quat->v[0] == 0.0f && quat->v[1] == 0.0f && quat->v[2] == 0.0f && quat->v[3] == 1.0f)
	{
		Identity();
		return *this;
	}

	float	wx, wy, wz;
	float	xx, yy, yz;
	float	xy, xz, zz;
	float	x2, y2, z2;

	x2 = quat->x + quat->x;
	y2 = quat->y + quat->y;
	z2 = quat->z + quat->z;

	xx = quat->x * x2;
	xy = quat->x * y2;
	xz = quat->x * z2;

	yy = quat->y * y2;
	yz = quat->y * z2;
	zz = quat->z * z2;

	wx = quat->w * x2;
	wy = quat->w * y2;
	wz = quat->w * z2;

	_11 = 1.0f - ( yy + zz );
	_12 = xy + wz;
	_13 = xz - wy;

	_21 = xy - wz;
	_22 = 1.0f - ( xx + zz );
	_23 = yz + wx;

	_31 = xz + wy;
	_32 = yz - wx;
	_33 = 1.0f - ( xx + yy );

	_41 = 0.0f;
	_42 = 0.0f;
	_43 = 0.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::RollPitchYaw(float roll, float pitch, float yaw)
{
	if(roll == 0.0f && pitch == 0.0f && yaw == 0.0f)
	{
		_11 = 1.0f;
		_12 = 0.0f;
		_13 = 0.0f;

		_21 = 0.0f;
		_22 = 1.0f;
		_23 = 0.0f;

		_31 = 0.0f;
		_32 = 0.0f;
		_33 = 1.0f;
	}
	else
	{
		float		angle;
		static float		sr, sp, sy, cr, cp, cy;

		angle = ENF_DEG2RAD(yaw);
		sy = sinf(angle);
		cy = cosf(angle);
		angle = ENF_DEG2RAD(pitch);
		sp = sinf(angle);
		cp = cosf(angle);
		angle = ENF_DEG2RAD(roll);
		sr = sinf(angle);
		cr = cosf(angle);
		
		_11 = cp * cy;
		_12 = cp * sy;
		_13 = -sp;

		_21 = -(-1*sr*sp*cy+-1*cr*-sy);
		_22 = -(-1*sr*sp*sy+-1*cr*cy);
		_23 = sr*cp;

		_31 = (cr*sp*cy+-sr*-sy);
		_32 = (cr*sp*sy+-sr*cy);
		_33 = cr*cp;
	}

///////////
	return *this;
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::Roll(float roll)
{
	roll = ENF_DEG2RAD(roll);
	float sr = sinf(roll);
	float cr = cosf(roll);

	_11 = 1.0f;
	_12 = 0.0f;
	_13 = 0.0f;

	_21 = 0.0f;
	_22 = cr;
	_23 = sr;

	_31 = 0.0f;
	_32 = -sr;
	_33 = cr;

	return *this;
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::Yaw(float yaw)
{
	yaw = ENF_DEG2RAD(yaw);
	float sy = sinf(yaw);
	float cy = cosf(yaw);

	_11 = cy;
	_12 = sy;
	_13 = 0.0f;

	_21 = -sy;
	_22 = cy;
	_23 = 0.0f;

	_31 = 0.0f;
	_32 = 0.0f;
	_33 = 1.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::Pitch(float pitch)
{
	pitch = ENF_DEG2RAD(pitch);
	float sp = sinf(pitch);
	float cp = cosf(pitch);

	_11 = cp;
	_12 = 0.0f;
	_13 = -sp;

	_21 = 0.0f;
	_22 = 1.0f;
	_23 = 0.0f;

	_31 = sp;
	_32 = 0.0f;
	_33 = cp;

///////////
	return *this;
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::Reflect(const Vector4& plane)
{
	_11 = 2.0f * plane.x * -plane.x + 1.0f;
	_12 = 2.0f * plane.y * -plane.x;
	_13 = 2.0f * plane.z * -plane.x;

	_21 = 2.0f * plane.x * -plane.y;
	_22 = 2.0f * plane.y * -plane.y + 1.0f;
	_23 = 2.0f * plane.z * -plane.y;

	_31 = 2.0f * plane.x * -plane.z;
	_32 = 2.0f * plane.y * -plane.z;
	_33 = 2.0f * plane.z * -plane.z + 1.0f;

	_41 = 2.0f * plane.x * plane.w;
	_42 = 2.0f * plane.y * plane.w;
	_43 = 2.0f * plane.z * plane.w;

	return *this;
}

//-----------------------------------------------------------------------
Vector4 Matrix43::TransformPlane(const Vector4& in) const
{
	Vector4 out;

	out.x = in.x * _11 + in.y * _21 + in.z * _31;
	out.y = in.x * _12 + in.y * _22 + in.z * _32;
	out.z = in.x * _13 + in.y * _23 + in.z * _33;
	out.w = in.w + (out.x * _41 + out.y * _42 + out.z * _43);

	return out;
}

//-----------------------------------------------------------------------
Vector3 Matrix43::TransformCoord(const Vector3& in) const
{
	Vector3 out;
	out.x = in.x * _11 + in.y * _21 + in.z * _31 + _41;
	out.y = in.x * _12 + in.y * _22 + in.z * _32 + _42;
	out.z = in.x * _13 + in.y * _23 + in.z * _33 + _43;
	return out;
}

//-----------------------------------------------------------------------------
Vector3 Matrix43::TransformVector(const Vector3& in) const
{
	Vector3 out;
	out.x = in.x * _11 + in.y * _21 + in.z * _31;
	out.y = in.x * _12 + in.y * _22 + in.z * _32;
	out.z = in.x * _13 + in.y * _23 + in.z * _33;
	return out;
}

//-----------------------------------------------------------------------
Vector3 Matrix43::InvTransformCoord(const Vector3& in) const
{
	Vector3 out, tmp;

	tmp.x = in.x - _41;
	tmp.y = in.y - _42;
	tmp.z = in.z - _43;

	out.x = tmp.x * _11 + tmp.y * _12 + tmp.z * _13;
	out.y = tmp.x * _21 + tmp.y * _22 + tmp.z * _23;
	out.z = tmp.x * _31 + tmp.y * _32 + tmp.z * _33;

	return out;
}

//-----------------------------------------------------------------------------
Matrix43 Matrix43::InvTransform(const Matrix43& mat) const
{
	Matrix43 res;

	res.SetRow(0, InvTransformVector(mat[0]));
	res.SetRow(1, InvTransformVector(mat[1]));
	res.SetRow(2, InvTransformVector(mat[2]));
	res.SetRow(3, InvTransformCoord(mat[3]));
	return res;
}

//-----------------------------------------------------------------------------
Vector3 Matrix43::InvTransformVector(const Vector3& in) const
{
	Vector3 out;

	out.x = in.x * _11 + in.y * _12 + in.z * _13;
	out.y = in.x * _21 + in.y * _22 + in.z * _23;
	out.z = in.x * _31 + in.y * _32 + in.z * _33;

	return out;
}

//-----------------------------------------------------------------------
void Matrix43::_multiply3(Matrix43& out, const Matrix43& a, const Matrix43& b)
{
	out._11 = a._11 * b._11 + a._21 * b._12 + a._31 * b._13;
	out._21 = a._11 * b._21 + a._21 * b._22 + a._31 * b._23;
	out._31 = a._11 * b._31 + a._21 * b._32 + a._31 * b._33;
	out._12 = a._12 * b._11 + a._22 * b._12 + a._32 * b._13;
	out._22 = a._12 * b._21 + a._22 * b._22 + a._32 * b._23;
	out._32 = a._12 * b._31 + a._22 * b._32 + a._32 * b._33;
	out._13 = a._13 * b._11 + a._23 * b._12 + a._33 * b._13;
	out._23 = a._13 * b._21 + a._23 * b._22 + a._33 * b._23;
	out._33 = a._13 * b._31 + a._23 * b._32 + a._33 * b._33;
}

//-----------------------------------------------------------------------
void Matrix43::_multiply4(Matrix43& out, const Matrix43& a, const Matrix43& b)
{
	out._11 = a._11 * b._11 + a._21 * b._12 + a._31 * b._13;
	out._21 = a._11 * b._21 + a._21 * b._22 + a._31 * b._23;
	out._31 = a._11 * b._31 + a._21 * b._32 + a._31 * b._33;
	out._41 = a._11 * b._41 + a._21 * b._42 + a._31 * b._43 + a._41;
	out._12 = a._12 * b._11 + a._22 * b._12 + a._32 * b._13;
	out._22 = a._12 * b._21 + a._22 * b._22 + a._32 * b._23;
	out._32 = a._12 * b._31 + a._22 * b._32 + a._32 * b._33;
	out._42 = a._12 * b._41 + a._22 * b._42 + a._32 * b._43 + a._42;
	out._13 = a._13 * b._11 + a._23 * b._12 + a._33 * b._13;
	out._23 = a._13 * b._21 + a._23 * b._22 + a._33 * b._23;
	out._33 = a._13 * b._31 + a._23 * b._32 + a._33 * b._33;
	out._43 = a._13 * b._41 + a._23 * b._42 + a._33 * b._43 + a._43;	
}

//-----------------------------------------------------------------------
Matrix43& Matrix43::AroundVector( const Vector3 dir, float degrees )
{
	Vector3 vr, vup;

	vr = dir.Perpendicular();

	vup = vr ^ dir;

	if(degrees == 0)
	{
		_11 = dir[0];
		_21 = dir[1];
		_31 = dir[2];

		_12 = vr[0];
		_22 = vr[1];
		_32 = vr[2];

		_13 = vup[0];
		_23 = vup[1];
		_33 = vup[2];
	}
	else
	{
		Matrix43	mat;
		Matrix43	im;
		Matrix43	zrot;
		Matrix43	tmpmat;

		mat._11 = vr[0];
		mat._21 = vr[1];
		mat._31 = vr[2];

		mat._12 = vup[0];
		mat._22 = vup[1];
		mat._32 = vup[2];

		mat._13 = dir[0];
		mat._23 = dir[1];
		mat._33 = dir[2];

		im.Identity();

		im._12 = mat._21;
		im._13 = mat._31;
		im._21 = mat._12;
		im._23 = mat._32;
		im._31 = mat._13;
		im._32 = mat._23;

		zrot.Identity();

		degrees = ENF_DEG2RAD(degrees);
		zrot._11 = zrot._22 = cosf( degrees );
		zrot._12 = sinf( degrees );
		zrot._21 = -zrot._12;
		zrot._33 = 1.0F;

		_multiply3(tmpmat, mat, zrot);
		_multiply3(*this, tmpmat, im);
	}

	return *this;
}

//-----------------------------------------------------------------------
Matrix33& Matrix33::RollPitchYaw(const Vector3& angles)
{
	if(angles.IsZero())
	{
		_11 = 1.0f;
		_12 = 0.0f;
		_13 = 0.0f;

		_21 = 0.0f;
		_22 = 1.0f;
		_23 = 0.0f;

		_31 = 0.0f;
		_32 = 0.0f;
		_33 = 1.0f;
	}
	else
	{
		float		angle;
		static float		sr, sp, sy, cr, cp, cy;
		// static to help MS compiler fp bugs

		angle = angles[YAW] * (ENF_PI / 180);
		sy = sinf(angle);
		cy = cosf(angle);
		angle = angles[PITCH] * (ENF_PI / 180);
		sp = sinf(angle);
		cp = cosf(angle);
		angle = angles[ROLL] * (ENF_PI / 180);
		sr = sinf(angle);
		cr = cosf(angle);
		
		_11 = cp * cy;
		_12 = cp * sy;
		_13 = -sp;

		_21 = -(-1*sr*sp*cy+-1*cr*-sy);
		_22 = -(-1*sr*sp*sy+-1*cr*cy);
		_23 = sr*cp;

		_31 = (cr*sp*cy+-sr*-sy);
		_32 = (cr*sp*sy+-sr*cy);
		_33 = cr*cp;
	}

///////////
	return *this;
}

//-----------------------------------------------------------------------
Matrix33& Matrix33::Roll(float roll)
{
	roll = ENF_DEG2RAD(roll);
	float sr = sinf(roll);
	float cr = cosf(roll);

	_11 = 1.0f;
	_12 = 0.0f;
	_13 = 0.0f;

	_21 = 0.0f;
	_22 = cr;
	_23 = sr;

	_31 = 0.0f;
	_32 = -sr;
	_33 = cr;

	return *this;
}

//-----------------------------------------------------------------------
Matrix33& Matrix33::Yaw(float yaw)
{
	yaw = ENF_DEG2RAD(yaw);
	float sy = sinf(yaw);
	float cy = cosf(yaw);

	_11 = cy;
	_12 = sy;
	_13 = 0.0f;

	_21 = -sy;
	_22 = cy;
	_23 = 0.0f;

	_31 = 0.0f;
	_32 = 0.0f;
	_33 = 1.0f;

	return *this;
}

//-----------------------------------------------------------------------
Matrix33& Matrix33::Pitch(float pitch)
{
	pitch = ENF_DEG2RAD(pitch);
	float sp = sinf(pitch);
	float cp = cosf(pitch);

	_11 = cp;
	_12 = 0.0f;
	_13 = -sp;

	_21 = 0.0f;
	_22 = 1.0f;
	_23 = 0.0f;

	_31 = sp;
	_32 = 0.0f;
	_33 = cp;

///////////
	return *this;
}

//-----------------------------------------------------------------------
void Matrix33::_multiply(Matrix33& out, const Matrix33& a, const Matrix33& b)
{
	out._11 = a._11 * b._11 + a._21 * b._12 + a._31 * b._13;
	out._21 = a._11 * b._21 + a._21 * b._22 + a._31 * b._23;
	out._31 = a._11 * b._31 + a._21 * b._32 + a._31 * b._33;
	out._12 = a._12 * b._11 + a._22 * b._12 + a._32 * b._13;
	out._22 = a._12 * b._21 + a._22 * b._22 + a._32 * b._23;
	out._32 = a._12 * b._31 + a._22 * b._32 + a._32 * b._33;
	out._13 = a._13 * b._11 + a._23 * b._12 + a._33 * b._13;
	out._23 = a._13 * b._21 + a._23 * b._22 + a._33 * b._23;
	out._33 = a._13 * b._31 + a._23 * b._32 + a._33 * b._33;
}


