
#include "CResult.h"
#include "string.h"

//--------------------------------------------------------------------
CResult::CResult()
{
	result = false;
	comment[0] = NULL;
}

//--------------------------------------------------------------------
CResult::CResult(bool result, const char* comment)
{
	this->result = result;
	this->comment[0] = NULL;
	SetComment(comment);
}

//--------------------------------------------------------------------
CResult::~CResult()
{
	SetComment(NULL);	//maze string
}

//--------------------------------------------------------------------
void CResult::Set(bool result, const char* comment)
{
	this->result = result;
	SetComment(comment);
}

//--------------------------------------------------------------------
const char* CResult::GetComment() const
{
	return comment ? comment : "";
}

//--------------------------------------------------------------------
bool CResult::IsCommented() const
{
	return (comment[0] != NULL);
}

//--------------------------------------------------------------------
void CResult::SetComment(const char* comment)
{
	if(comment)
		strcpy(this->comment, comment);
	else
		this->comment[0] = NULL;
}

//--------------------------------------------------------------------
void CResult::operator= (bool res)
{
	result = res;
}

//--------------------------------------------------------------------
void CResult::operator= (const CResult& res)
{
	result = res.result;
	SetComment(res.comment);
}

//--------------------------------------------------------------------
bool CResult::operator== (const CResult& res)
{
	return (result == res.result);
}

//--------------------------------------------------------------------
bool CResult::operator== (bool res)
{
	return (result == res);
}

//--------------------------------------------------------------------
bool CResult::operator!= (const CResult& res)
{
	return (result != res.result);
}

//--------------------------------------------------------------------
bool CResult::operator!= (bool res)
{
	return (result != res);
}