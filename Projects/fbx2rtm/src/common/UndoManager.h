#ifndef _UNDO_MANAGER_H
#define _UNDO_MANAGER_H


#include "common/enf_types.h"


//-------------------------------------------------------------------------------------------------
/*!
Undo class
*/
//-------------------------------------------------------------------------------------------------
class UndoState
{
protected:
	int			type;
	DString		m_sName;					///< name of state
	void*			ClientData;

	//-------------------------------------------------------------------------------------------------
	/*!
	empty destructor
	*/
	//-------------------------------------------------------------------------------------------------
	virtual ~UndoState()
	{}

	
public:
	//-------------------------------------------------------------------------------------------------
	/*!
	constructor
	*/
	//-------------------------------------------------------------------------------------------------
	UndoState(int type, enf::CStr name = "")
	{
		ClientData = NULL;
		this->type = type;
		m_sName.assign(name.cstr(), name.Length());
	}

	//-------------------------------------------------------------------------------------------------
	ENF_INLINE int GetType() const
	{
		return type;
	}

	//-------------------------------------------------------------------------------------------------
	ENF_INLINE const char* GetName() const
	{
		return m_sName.c_str();
	}

	//-------------------------------------------------------------------------------------------------
	void SetName(enf::CStr name)
	{
		assert(name.Length() > 0);
		m_sName.assign(name.cstr(), name.Length());
	}

	//-------------------------------------------------------------------------------------------------
	ENF_INLINE void SetClientData(void* data)
	{
		ClientData = data;
	}

	//-------------------------------------------------------------------------------------------------
	ENF_INLINE void* GetClientData() const
	{
		return ClientData;
	}

	//-------------------------------------------------------------------------------------------------
	/*!
	release
	*/
	//-------------------------------------------------------------------------------------------------
	virtual void Release()
	{
		delete this;
	}


	//-------------------------------------------------------------------------------------------------
	/*!
	undo step
	*/
	//-------------------------------------------------------------------------------------------------
	virtual bool Undo()
	{
		return true;
	}


	//-------------------------------------------------------------------------------------------------
	/*!
	redo step
	*/
	//-------------------------------------------------------------------------------------------------
	virtual bool Redo()
	{
		return true;
	}

#ifdef ENF_LEAK_DETECTOR
	ENF_DECLARE_MMOBJECT(UndoState);
#endif
};




//-------------------------------------------------------------------------------------------------
/*!
UndoManager
*/
//-------------------------------------------------------------------------------------------------
class UndoManager
{
protected:
	EArray<UndoState*>		m_UndoStack;				///< stack for undo states
	int							m_iActualState;			///< actual state 

public:

	//-------------------------------------------------------------------------------------------------
	/*!
	empty constructor
	*/
	//-------------------------------------------------------------------------------------------------
	UndoManager()
	{
		m_iActualState = -1;
	}


	//-------------------------------------------------------------------------------------------------
	/*!
	destructor
	*/
	//-------------------------------------------------------------------------------------------------
	~UndoManager()
	{
		Clear();
	}

	//-------------------------------------------------------------------------------------------------
	void Clear()
	{
		for (uint i = 0; i < m_UndoStack.GetCardinality(); i++)	
		{
			m_UndoStack[i]->Release();
		}

		m_UndoStack.Clear();
		m_iActualState = -1;
	}

	//-------------------------------------------------------------------------------------------------
	/*!
	UndoManager
	*/
	//-------------------------------------------------------------------------------------------------
	bool Undo()
	{
		if (!m_UndoStack.GetCardinality() || m_iActualState == -1 || m_iActualState >= (int) m_UndoStack.GetCardinality())
			return false;

		UndoState *state = m_UndoStack[m_iActualState];
		assert(state);

		bool res = state->Undo();

		if(res)
			m_iActualState--;

		return res;
	}


	//-------------------------------------------------------------------------------------------------
	/*!
	UndoManager
	*/
	//-------------------------------------------------------------------------------------------------
	bool Redo()
	{
		if (m_iActualState < (int)m_UndoStack.GetCardinality() - 1)
		{
			bool res = m_UndoStack[++m_iActualState]->Redo();

//			if(res)
//				m_iActualState++;

			return res;
		}

		return false;
	}


	//-------------------------------------------------------------------------------------------------
	/*!
	add undo state -> release all undos above current state and insert new undo
	\param undo		one undo state
	*/
	//-------------------------------------------------------------------------------------------------
	void Add(UndoState *undo)
	{
		assert(strlen(undo->GetName()) > 0);
		//release all states
		while (m_iActualState < (int) m_UndoStack.GetCardinality() - 1)
		{
			UndoState *state = m_UndoStack.Pop();
			state->Release();
		}

		m_iActualState = m_UndoStack.Insert(undo);
	}


	//-------------------------------------------------------------------------------------------------
	/*!
	number of states
	*/
	//-------------------------------------------------------------------------------------------------
	ENF_INLINE uint GetStatesCount() const
	{
		return m_UndoStack.GetCardinality();
	}

	//-------------------------------------------------------------------------------------------------
	/*!
	number of redo states
	*/
	//-------------------------------------------------------------------------------------------------
	ENF_INLINE int GetRedoStatesCount() const
	{
		if(m_UndoStack.GetCardinality() == 0)
			return 0;
		else
			return ((int)m_UndoStack.GetCardinality() - 1 - m_iActualState);
	}

	//-------------------------------------------------------------------------------------------------
	ENF_INLINE int GetUndoStatesCount() const
	{
		return m_iActualState + 1;//(m_iActualState >= 0) ? m_iActualState : 0;
	}

	//-------------------------------------------------------------------------------------------------
	int GetCurrentStateIndex()
	{
		return m_iActualState;
	}

//-------------------------------------------------------------------------------------------------
	const UndoState* GetState(int state)
	{
		assert(state >= 0);
		assert(state < (int)m_UndoStack.GetCardinality());
		return m_UndoStack[state];
	}

	//-------------------------------------------------------------------------------------------------
	bool SetCurrentState(int state)
	{
		assert(state >= 0);
		assert(state < (int)m_UndoStack.GetCardinality());

		if(state == m_iActualState)
			return false;

		bool res = true;

		if(state > m_iActualState)
		{
			while(state > m_iActualState && res == true)
				res = Redo();
		}
		else
		{
			while(state < m_iActualState && res == true)
				res = Undo();
		}

		return res;
	}

#ifdef ENF_LEAK_DETECTOR
	ENF_DECLARE_MMOBJECT(UndoManager);
#endif
};


#endif 