
#ifndef WX_UTIL_H
#define WX_UTIL_H

#include "wx/string.h"

class wxMenuItem;
class wxMenu;
class wxFrame;
class wxMenuBar;
class wxToolBar;

wxMenuItem* FindMenuItem(wxMenu* menu, wxString& ItemName);
bool			CheckMenuItemFromName(wxMenu* menu, wxString& CheckedItem);
int			GetLowestFreeMenuId(wxMenu* menu, int LowestID);
bool			RemoveSharedMenu(wxFrame* window, wxMenu* menu);
bool			AssignSharedMenu(wxFrame* window, wxMenu* menu, wxString name, int pos);
void			UpdateMenuBarOfFrame(wxFrame* window);
void			EnableToolBar(wxToolBar* toolbar, bool enable);

#endif