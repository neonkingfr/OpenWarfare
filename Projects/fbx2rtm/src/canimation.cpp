#include "stdafx.h"
#include "canimation.h"

//---------------------------------------------------------------------------
Matrix43 CAnimation::GetNodeGlobalM(uint frame, uint node) const
{
	return m_NodeHeads[node].m_Keyframes[frame];
}

//---------------------------------------------------------------------------
Matrix43 CAnimation::GetNodeLocalM(uint frame, uint node) const
{
	const Matrix43& gmat = m_NodeHeads[node].m_Keyframes[frame];
	if(m_NodeHeads[node].m_pNode->m_pParent == NULL)
		return gmat;

	const Matrix43& pmat = m_NodeHeads[m_NodeHeads[node].m_pNode->m_pParent->m_iIndex].m_Keyframes[frame];

	Matrix43 res;
	res = pmat.InvTransform(gmat);
	return res;
}


//---------------------------------------------------------------------------
uint CAnimation::GetNodeIndex(const char* name) const
{
	for(uint n = 0; n < m_NodeHeads.GetCardinality(); n++)
	{
		if(strcmp(m_NodeHeads[n].m_pNode->GetName(), name) == 0)
			return n;
	}

	return -1;
}

//---------------------------------------------------------------------------
uint CAnimation::AppendKeyframeGlobal(const Matrix43* nodes)
{
	assert(nodes);
	uint frame = m_NodeHeads[0].m_Keyframes.GetCardinality();

	for(uint n = 0; n < m_NodeHeads.GetCardinality(); n++)
	{
		uint iframe = m_NodeHeads[n].m_Keyframes.Insert(nodes[n]);
		assert(iframe == frame);
	}

	return frame;
}

//---------------------------------------------------------------------------
uint CAnimation::AppendKeyframeGlobal(const Matrix43& node, uint nodenum)
{
	uint frame = m_NodeHeads[nodenum].m_Keyframes.GetCardinality();
	m_NodeHeads[nodenum].m_Keyframes.Insert(node);
	return frame;
}

//---------------------------------------------------------------------------
void CAnimation::r_SetLocalFrames(uint node, const Matrix43& frame)
{
	if(m_NodeHeads[node].m_pNode->m_pParent)
	{
		uint pindex = m_NodeHeads[node].m_pNode->m_pParent->m_iIndex;
		//transform it by parent node (which is already transformed)
		Matrix43 gmat = m_NodeHeads[pindex].m_Keyframes[m_NodeHeads[node].m_Keyframes.GetCardinality() - 1] * frame;
		m_NodeHeads[node].m_Keyframes.Insert(gmat);
	}
	else
	{
		m_NodeHeads[node].m_Keyframes.Insert(frame);
	}
}

//---------------------------------------------------------------------------
uint CAnimation::AppendKeyframeLocal(const Matrix43* nodes)
{
	assert(nodes);
	uint frame = m_NodeHeads[0].m_Keyframes.GetCardinality();

	for(uint n = 0; n < m_NodeHeads.GetCardinality(); n++)
	{
		uint iframe = m_NodeHeads[n].m_Keyframes.Insert(nodes[n]);

		//all frames must be aligned!
		assert(iframe == frame);
	}

	return frame;
}

//---------------------------------------------------------------------------
const char* CAnimation::GetNodeName(uint node) const
{
	return m_NodeHeads[node].m_pNode->GetName();
}

//---------------------------------------------------------------------------
uint CAnimation::GetNumNodes() const
{
	return m_NodeHeads.GetCardinality();
}

//---------------------------------------------------------------------------
uint CAnimation::GetNumFrames() const
{
	if(m_NodeHeads.IsEmpty())
		return 0;

	return m_NodeHeads[0].m_Keyframes.GetCardinality();
}

//---------------------------------------------------------------------------
void CAnimation::ClearAll()
{
	Clear();
	for(uint n = 0; n < m_NodeHeads.GetCardinality(); n++)
	{
		delete m_NodeHeads[n].m_pNode;
	}
	m_NodeHeads.Clear();

}

//---------------------------------------------------------------------------
uint CAnimation::AppendNode(const char* name, uint parent)
{
	SNodeHead nhead;
	nhead.m_pNode = new CNode(name);
	if(parent != -1)
	{
		//set parent node
		nhead.m_pNode->m_pParent = m_NodeHeads[parent].m_pNode;

		const char* pname = nhead.m_pNode->m_pParent->GetName();

		//add to children list
		CNode** prev = &nhead.m_pNode->m_pParent->m_pChildren;
		while(*prev)
			prev = &(*prev)->m_pSibling;

		*prev = nhead.m_pNode;
	}

	nhead.m_pNode->m_iIndex = m_NodeHeads.GetCardinality();
	return m_NodeHeads.Insert(nhead);
}

//---------------------------------------------------------------------------
void CAnimation::Clear()
{
	for(uint n = 0; n < m_NodeHeads.GetCardinality(); n++)
	{
		m_NodeHeads[n].m_Keyframes.Clear();
		m_NodeHeads[n].m_QuatKeys.Clear();
		m_NodeHeads[n].m_QRange.Init();
		m_NodeHeads[n].m_ScaleKeys.Clear();
		m_NodeHeads[n].m_SRange.Init();
		m_NodeHeads[n].m_TransKeys.Clear();
		m_NodeHeads[n].m_TRange.Init();
	}
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
