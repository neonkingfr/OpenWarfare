
#ifndef CONFIG_ED_APP_H
#define CONFIG_ED_APP_H

#include "wx/app.h"
#include "common/CResult.h"

class TiXmlDocument;
class TiXmlElement;
class TiXmlNode;
class XMLConfig;

//-------------------------------------------------------------------------------------------------------
class GuiApp : public wxApp
{
public:
	GuiApp();
	inline XMLConfig* GetConfig(){return config;}
	inline TiXmlDocument* GetAnimDefConfig(){return AnimDefConfig;}
	bool				SaveConfigOnExit;
private:
	XMLConfig*		config;
	TiXmlDocument*	AnimDefConfig;
	CResult			CheckAnimDefData();
   virtual bool	OnInit();
	virtual int		OnExit();
	int				MainLoop();
};

DECLARE_APP(GuiApp)

extern GuiApp* g_app;

#endif