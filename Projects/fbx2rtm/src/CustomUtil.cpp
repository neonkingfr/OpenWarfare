
//#include <assert.h>
//#include <memory.h>
//#include <windows.h>
#include "CustomUtil.h"
#include "tinyxml.h"
#include "wx/arrstr.h"

//===============================================================================
PathIterator::PathIterator()
{
	pos = 0;
}

//-------------------------------------------------------------------------------
PathIterator::PathIterator(const char* path)
{
	assert(path);
	this->path = path;
	pos = this->path.length();
}

//-------------------------------------------------------------------------------
const char* PathIterator::AddLevel(const char* name)
{
#ifdef _DEBUG
	assert(name);
	int len = strlen(name);
	assert(len > 0);	
	const char* ptr = name;

	for(int n = 0; n < len; n++, ptr++)
	{
		if(*ptr == '\\')
		{
			assert(false);
			return path.c_str();
		}
	}
#endif

	if(path.length() > 0)
		path += '\\';

	path += name;
	pos = path.length();
	return path.c_str();
}

//-------------------------------------------------------------------------------
wxString PathIterator::GetFirstLevel() const
{
	pos = 0;
	const char* start = path.c_str() + pos;
	const char* end = start;

	while(*end != 0 && *end != '\\')
		end++;

	int lng = end - start;
	pos += lng;

	if(*end == '\\')
		pos++;

	return wxString(start, lng);
}

//-------------------------------------------------------------------------------
wxString PathIterator::GetNextLevel() const
{
	const char* start = path.c_str() + pos;
	const char* end = start;

	while(*end != 0 && *end != '\\')
		end++;

	int lng = end - start;
	pos += lng;

	if(*end == '\\')
		pos++;

	return wxString(start, lng);
}

//-------------------------------------------------------------------------------
const char* PathIterator::GetLastLevel() const
{
	int len = path.length();

	if(len == 0)
		return "";

	const char* start = path.c_str();
	const char* end = start + (len - 1);

	while(end > start && *end != '\\')
		end--;

	if(*end == '\\')
		end++;

	return end;
}

//-------------------------------------------------------------------------------
const char* PathIterator::GetPath() const
{
	return path.c_str();
}

//-------------------------------------------------------------------------------
bool PathIterator::operator> (const PathIterator& p)
{
	return (strcmp(GetPath(), p.GetPath()) > 0);
}

//-------------------------------------------------------------------------------
bool PathIterator::operator< (const PathIterator& p)
{
	return (strcmp(GetPath(), p.GetPath()) < 0);
}

//-------------------------------------------------------------------------------
bool PathIterator::operator== (const PathIterator& p)
{
	return (strcmp(GetPath(), p.GetPath()) == 0);
}

//-----------------------------------------------------------------------------------------
TiXmlElement* GetChildElement(TiXmlNode* parent, const char* name)
{
	for(TiXmlNode* node = parent->FirstChild(); node; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(elm && strcmp(elm->Value(), name) == 0)
			return elm;
	}
	return NULL;
}

//---------------------------------------------------------------------------
void r_LoadBoneNamesFromSkeleton(wxArrayString& BoneNames, const TiXmlElement* parent)
{
	if(parent == NULL)
		return;

	for(const TiXmlElement* bone = parent; bone; bone = bone->NextSiblingElement("bone"))
	{
		const char* name = bone->Attribute("name");
//		const char* fbxname = bone->Attribute("fbxname");

		if(name)
			BoneNames.Add(name);

		r_LoadBoneNamesFromSkeleton(BoneNames, bone->FirstChildElement("bone"));
	}
}

//---------------------------------------------------------------------------
void LoadBoneNamesFromSkeleton(wxArrayString& BoneNames, TiXmlDocument* doc, enf::CStr name)
{
	TiXmlHandle handle = TiXmlHandle(doc->FirstChildElement("skeletondefs")).FirstChildElement("skeletondef");

	for(const TiXmlElement* elem = handle.Element(); elem; elem = elem->NextSiblingElement("skeletondef"))
	{
		enf::CStr sname = elem->Attribute("name");
		
		if(sname.cmpi(name) == 0)
		{
			r_LoadBoneNamesFromSkeleton(BoneNames, elem->FirstChildElement("bone"));

			//append inherited skeletons
			const TiXmlElement* ielem = elem;
			while(ielem)
			{
				enf::CStr inherits = ielem->Attribute("inherits");
				if(inherits.IsEmpty())
					break;

				for(ielem = handle.Element(); elem; elem = elem->NextSiblingElement("skeletondef"))
				{
					if(inherits.cmpi(ielem->Attribute("name")) == 0)
					{
						r_LoadBoneNamesFromSkeleton(BoneNames, ielem->FirstChildElement("bone"));
						break;
					}
				}
			}
		}
	}
}


//---------------------------------------------------------------------------
void LoadSkeletonNames(wxArrayString& skeletons, TiXmlDocument* doc)
{
	TiXmlHandle handle = TiXmlHandle(doc->FirstChildElement("skeletondefs")).FirstChildElement("skeletondef");

	for(const TiXmlElement* elem = handle.Element(); elem; elem = elem->NextSiblingElement("skeletondef"))
	{
		const char* name = elem->Attribute("name");

		if(name && strlen(name) > 0)
			skeletons.Add(name);
	}
}