#include "stdafx.h"
#include "fbxmain.h"
#include <vector>

void DisplayString(char* pHeader, char* pValue = "", char* pSuffix = "")
{
	KString lString;

	lString = pHeader;
	lString += pValue;
	lString += pSuffix;
	lString += "\n";
	CLogger::Current()->printf(lString);
}

bool AddToRange(KFbxAnimCurve* pCurve, KTime& tmin, KTime& tmax)
{
	if (pCurve)
	{
		int lKeyCount = pCurve->KeyGetCount();

		//no anim
		if(lKeyCount == 0)
			return false;

		KTime ctime;

		ctime = pCurve->KeyGetTime(0);
		if(tmin > ctime)
			tmin = ctime;
		ctime = pCurve->KeyGetTime(lKeyCount - 1);
		if(tmax < ctime)
			tmax = ctime;

		return true;
	}
	return false;
}
void AnimationRange(KFbxNode* pNode,KFbxAnimLayer* pAnimLayer, KTime& tmin, KTime& tmax)
{
	AddToRange(pNode->LclTranslation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_T_X), tmin, tmax);
	AddToRange(pNode->LclTranslation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_T_Y), tmin, tmax);
	AddToRange(pNode->LclTranslation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_T_Z), tmin, tmax);
	AddToRange(pNode->LclRotation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_R_X), tmin, tmax);
	AddToRange(pNode->LclRotation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_R_Y), tmin, tmax);
	AddToRange(pNode->LclRotation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_R_Z), tmin, tmax);
	AddToRange(pNode->LclScaling.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_S_X), tmin, tmax);
	AddToRange(pNode->LclScaling.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_S_Y), tmin, tmax);
	AddToRange(pNode->LclScaling.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_S_Z), tmin, tmax);
}

void DisplayCurveDefault(KFCurve *pCurve)
{
	KString lOutputString;

	lOutputString = "            Default Value: ";
	lOutputString += static_cast<float> (pCurve->GetValue());
	lOutputString += "\n";
	CLogger::Current()->printf (lOutputString);
}

void DisplayListCurveDefault(KFCurve *pCurve, KFbxProperty*)
{
	DisplayCurveDefault(pCurve);
}


static int InterpolationFlagToIndex(int flags)
{
	if ((flags&KFCURVE_INTERPOLATION_CONSTANT)==KFCURVE_INTERPOLATION_CONSTANT)
		return 1;
	if ((flags&KFCURVE_INTERPOLATION_LINEAR)==KFCURVE_INTERPOLATION_LINEAR)
		return 2;
	if ((flags&KFCURVE_INTERPOLATION_CUBIC)==KFCURVE_INTERPOLATION_CUBIC)
		return 3;
	return 0;
}

static int ConstantmodeFlagToIndex(int flags)
{
	if ((flags&KFCURVE_CONSTANT_STANDARD)==KFCURVE_CONSTANT_STANDARD)
		return 1;
	if ((flags&KFCURVE_CONSTANT_NEXT)==KFCURVE_CONSTANT_NEXT)
		return 2;
	return 0;
}

static int TangeantmodeFlagToIndex(int flags)
{
	if ((flags&KFCURVE_TANGEANT_AUTO) == KFCURVE_TANGEANT_AUTO)
		return 1;
	if ((flags&KFCURVE_TANGEANT_AUTO_BREAK)==KFCURVE_TANGEANT_AUTO_BREAK)
		return 2;
	if ((flags&KFCURVE_TANGEANT_TCB) == KFCURVE_TANGEANT_TCB)
		return 3;
	if ((flags&KFCURVE_TANGEANT_USER) == KFCURVE_TANGEANT_USER)
		return 4;
	if ((flags&KFCURVE_GENERIC_BREAK) == KFCURVE_GENERIC_BREAK)
		return 5;
	if ((flags&KFCURVE_TANGEANT_BREAK) ==KFCURVE_TANGEANT_BREAK)
		return 6;
	return 0;
}

static int TangeantweightFlagToIndex(int flags)
{
	if ((flags&KFCURVE_WEIGHTED_NONE) == KFCURVE_WEIGHTED_NONE)
		return 1;
	if ((flags&KFCURVE_WEIGHTED_RIGHT) == KFCURVE_WEIGHTED_RIGHT)
		return 2;
	if ((flags&KFCURVE_WEIGHTED_NEXT_LEFT) == KFCURVE_WEIGHTED_NEXT_LEFT)
		return 3;
	return 0;
}

static int TangeantVelocityFlagToIndex(int flags)
{
	if ((flags&KFCURVE_VELOCITY_NONE) == KFCURVE_VELOCITY_NONE)
		return 1;
	if ((flags&KFCURVE_VELOCITY_RIGHT) == KFCURVE_VELOCITY_RIGHT)
		return 2;
	if ((flags&KFCURVE_VELOCITY_NEXT_LEFT) == KFCURVE_VELOCITY_NEXT_LEFT)
		return 3;
	return 0;
}

void DisplayCurveKeys(KFCurve *pCurve)
{
	static char* interpolation[] = { "?", "constant", "linear", "cubic"};
	static char* constantMode[] =  { "?", "Standard", "Next" };
	static char* cubicMode[] =     { "?", "Auto", "Auto break", "Tcb", "User", "Break", "User break" };
	static char* tangentWVMode[] = { "?", "None", "Right", "Next left" };

	KTime   lKeyTime;
	float   lKeyValue;
	char    lTimeString[256];
	KString lOutputString;
	int     lCount;

	int lKeyCount = pCurve->KeyGetCount();

	for(lCount = 0; lCount < lKeyCount; lCount++)
	{
		lKeyValue = static_cast<float>(pCurve->KeyGetValue(lCount));
		lKeyTime  = pCurve->KeyGetTime(lCount);

		lOutputString = "            Time: ";
		lOutputString += lKeyTime.GetTimeString(lTimeString);
		lOutputString += ".... Value: ";
		lOutputString += lKeyValue;
		/*
		lOutputString += " [ ";
		lOutputString += interpolation[ InterpolationFlagToIndex(pCurve->KeyGetInterpolation(lCount)) ];
		if ((pCurve->KeyGetInterpolation(lCount)&KFCURVE_INTERPOLATION_CONSTANT) == KFCURVE_INTERPOLATION_CONSTANT)
		{
		lOutputString += " | ";
		lOutputString += constantMode[ ConstantmodeFlagToIndex(pCurve->KeyGetConstantMode(lCount)) ];
		lOutputString += " ]";
		}
		else
		if ((pCurve->KeyGetInterpolation(lCount)&KFCURVE_INTERPOLATION_CUBIC) == KFCURVE_INTERPOLATION_CUBIC)
		{
		lOutputString += " | ";
		lOutputString += cubicMode[ TangeantmodeFlagToIndex(pCurve->KeyGetTangeantMode(lCount)) ];
		lOutputString += " | ";
		lOutputString += tangentWVMode[ TangeantweightFlagToIndex(pCurve->KeyGetTangeantWeightMode(lCount)) ];
		lOutputString += " | ";
		lOutputString += tangentWVMode[ TangeantVelocityFlagToIndex(pCurve->KeyGetTangeantVelocityMode(lCount)) ];
		lOutputString += " ]";
		}
		*/
		lOutputString += "\n";
		CLogger::Current()->printf (lOutputString);
	}
}

//---------------------------------------------------------------------------
void DisplayListCurveKeys(KFCurve *pCurve, KFbxProperty* pProperty)
{
	KTime   lKeyTime;
	int     lKeyValue;
	char    lTimeString[256];
	KString lListValue;
	KString lOutputString;
	int     lCount;

	int lKeyCount = pCurve->KeyGetCount();

	for(lCount = 0; lCount < lKeyCount; lCount++)
	{
		lKeyValue = static_cast<int>(pCurve->KeyGetValue(lCount));
		lKeyTime  = pCurve->KeyGetTime(lCount);

		lOutputString = "            Key Time: ";
		lOutputString += lKeyTime.GetTimeString(lTimeString);
		lOutputString += ".... Key Value: ";
		lOutputString += lKeyValue;
		lOutputString += " (";
		lOutputString += pProperty->GetEnumValue(lKeyValue);
		lOutputString += ")";

		lOutputString += "\n";
		CLogger::Current()->printf (lOutputString);
	}
}

//---------------------------------------------------------------------------
void DisplayChannels(KFbxNode* pNode, KFbxAnimLayer* pAnimLayer, void (*DisplayCurve) (KFCurve *pCurve), void (*DisplayListCurve) (KFCurve *pCurve, KFbxProperty* pProperty))
{
	KFbxAnimCurve* lCurve = NULL;

	KTime tmin = KTIME_INFINITE, tmax = KTIME_MINUS_INFINITE;

	AnimationRange(pNode, pAnimLayer,tmin, tmax);

	if(tmin >= tmax)
	{
		return;
	}

	CLogger::Current()->printf("Animation time-length:");
	CLogger::Current()->printf("%lf", tmin.GetSecondDouble());
	CLogger::Current()->printf(" - ");
	CLogger::Current()->printf("%lf\n", tmax.GetSecondDouble());

	//DisplayAnimationData(pTakeNode, tmin, tmax);

	// Display general curves.
	lCurve = pNode->LclTranslation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_T_X);
	if (lCurve)
	{
		CLogger::Current()->printf("        TX\n");
		DisplayCurve(lCurve->GetKFCurve());
	}
	lCurve = pNode->LclTranslation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_T_Y);
	if (lCurve)
	{
		CLogger::Current()->printf("        TY\n");
		DisplayCurve(lCurve->GetKFCurve());
	}
	lCurve = pNode->LclTranslation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_T_Z);
	if (lCurve)
	{
		CLogger::Current()->printf("        TZ\n");
		DisplayCurve(lCurve->GetKFCurve());
	}
	lCurve = pNode->LclRotation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_R_X);
	if (lCurve)
	{
		CLogger::Current()->printf("        RX\n");
		DisplayCurve(lCurve->GetKFCurve());
	}
	lCurve = pNode->LclRotation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_R_Y);
	if (lCurve)
	{
		CLogger::Current()->printf("        RY\n");
		DisplayCurve(lCurve->GetKFCurve());
	}
	lCurve = pNode->LclRotation.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_R_Z);
	if (lCurve)
	{
		CLogger::Current()->printf("        RZ\n");
		DisplayCurve(lCurve->GetKFCurve());
	}
	lCurve = pNode->LclScaling.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_S_X);
	if (lCurve)
	{
		CLogger::Current()->printf("        SX\n");
		DisplayCurve(lCurve->GetKFCurve());
	}
	lCurve = pNode->LclScaling.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_S_Y);
	if (lCurve)
	{
		CLogger::Current()->printf("        SY\n");
		DisplayCurve(lCurve->GetKFCurve());
	}
	lCurve = pNode->LclScaling.GetCurve<KFbxAnimCurve>(pAnimLayer, KFCURVENODE_S_Z);
	if (lCurve)
	{
		CLogger::Current()->printf("        SZ\n");
		DisplayCurve(lCurve->GetKFCurve());
	}
}

//---------------------------------------------------------------------------
void DisplayAnimation(KFbxAnimLayer* pAnimLayer, KFbxNode* pNode)
{
	int lModelCount;
	KString lOutputString;

	lOutputString = "     Node Name: ";
	lOutputString += pNode->GetName();
	lOutputString += "\n\n";
	printf(lOutputString);

	DisplayChannels(pNode, pAnimLayer, DisplayCurveKeys, DisplayListCurveKeys);
	printf ("\n");

	for(lModelCount = 0; lModelCount < pNode->GetChildCount(); lModelCount++)
	{
		DisplayAnimation(pAnimLayer, pNode->GetChild(lModelCount));
	}
}

void DisplayAnimation(KFbxAnimStack* pAnimStack, KFbxNode* pNode)
{
	int l;
	int nbAnimLayers = pAnimStack->GetMemberCount(FBX_TYPE(KFbxAnimLayer));
	KString lOutputString;

	lOutputString = "Animation stack contains ";
	lOutputString += nbAnimLayers;
	lOutputString += " Animation Layer(s)\n";
	printf(lOutputString);

	for (l = 0; l < nbAnimLayers; l++)
	{
		KFbxAnimLayer* lAnimLayer = pAnimStack->GetMember(FBX_TYPE(KFbxAnimLayer), l);

		lOutputString = "AnimLayer ";
		lOutputString += l;
		lOutputString += "\n";
		printf(lOutputString);

		DisplayAnimation(lAnimLayer, pNode);
	}
}

//---------------------------------------------------------------------------
void CFbxScene::r_FillNodes(const CNodeFilter* filter, KFbxNode* pNode, KFbxAnimLayer* pAnimLayer, const char* parentname, EArray<SNodeEntry>& nodes, KTime& tmin, KTime& tmax) const
{
	bool accept = true;

	KFbxNodeAttribute* lNodeAttribute = pNode->GetNodeAttribute();

	SNodeEntry node;

	if(lNodeAttribute)
	{
		if(lNodeAttribute->GetAttributeType() == KFbxNodeAttribute::eSKELETON)
		{
			KFbxSkeleton* lSkeleton = (KFbxSkeleton*) pNode->GetNodeAttribute();

			// Only draw the skeleton if it's a limb node and if 
			// the parent also has an attribute of type skeleton.
			if (lSkeleton->GetSkeletonType() == KFbxSkeleton::eLIMB_NODE/* &&
				pNode->GetParent() &&
				pNode->GetParent()->GetNodeAttribute() &&
				pNode->GetParent()->GetNodeAttribute()->GetAttributeType() == KFbxNodeAttribute::eSKELETON*/)
			{
				// Do nothing if the current take node points to default values.
				node.m_strName = pNode->GetName();
				node.m_pNode = pNode;
				node.m_pParent = pNode->GetParent();
				node.m_strParentName = parentname;

				//custom filter
				if(filter == NULL || filter->Filter(node.m_strName, &node.m_strName))
				{
					const char* pname = pNode->GetParent() ? pNode->GetParent()->GetName() : NULL;

					nodes.Insert(node);
					AnimationRange(pNode,pAnimLayer, tmin, tmax);
				}
			}
		}
		else if(lNodeAttribute->GetAttributeType() == KFbxNodeAttribute::eMESH)
		{
			node.m_strName = pNode->GetName();
			node.m_pNode = pNode;
			node.m_pParent = pNode->GetParent();
			node.m_strParentName = parentname;

			//custom filter
			if(filter == NULL || filter->Filter(node.m_strName, &node.m_strName))
			{
				const char* pname = pNode->GetParent() ? pNode->GetParent()->GetName() : NULL;

				nodes.Insert(node);
				AnimationRange(pNode,pAnimLayer, tmin, tmax);
			}
		}

		//filter everything else than eSkeleton??
		switch(lNodeAttribute->GetAttributeType())
		{
		case  KFbxNodeAttribute::eMARKER:
			lNodeAttribute=lNodeAttribute;
			break;
		case KFbxNodeAttribute::eSKELETON:
			lNodeAttribute=lNodeAttribute;
			break;
		case KFbxNodeAttribute::eMESH:
			lNodeAttribute=lNodeAttribute;
			break;
		case KFbxNodeAttribute::eCAMERA:
			lNodeAttribute=lNodeAttribute;
			break;
		case KFbxNodeAttribute::eLIGHT:
			lNodeAttribute=lNodeAttribute;
			break;
		}
	}		

	for(int lNodeCount = 0; lNodeCount < pNode->GetChildCount(); lNodeCount++)
	{
		r_FillNodes(filter, pNode->GetChild(lNodeCount), pAnimLayer,node.m_strName, nodes, tmin, tmax);
	}
}

//---------------------------------------------------------------------------
void CFbxScene::FillNodes(const CNodeFilter* filter, KFbxNode* pNode, KFbxAnimLayer* pAnimLayer, EArray<SNodeEntry>& nodes) const
{
	nodes.Clear();
	KTime tmin = KTIME_INFINITE;
	KTime tmax = KTIME_MINUS_INFINITE;

	r_FillNodes(filter, pNode,pAnimLayer, NULL, nodes, tmin, tmax);
}

//---------------------------------------------------------------------------
bool CFbxScene::Export(CAnimation* anim, const CNodeFilter& filter, float fps, int from, int to)
{
	assert(anim != NULL);

	KArrayTemplate<const char*> lTakeNameArray;

	for (int i = 0; i < m_pScene->GetSrcObjectCount(FBX_TYPE(KFbxAnimStack)); i++)
	{
		KFbxAnimStack* lAnimStack = KFbxCast<KFbxAnimStack>(m_pScene->GetSrcObject(FBX_TYPE(KFbxAnimStack), i));
		int nbAnimLayers = lAnimStack->GetMemberCount(FBX_TYPE(KFbxAnimLayer));
		for (int l = 0; l < nbAnimLayers; l++)
		{
			KFbxAnimLayer* lAnimLayer = lAnimStack->GetMember(FBX_TYPE(KFbxAnimLayer), l);
			lTakeNameArray.Add(lAnimLayer->GetName());
		}
	}
	/*
	int i;
	for(i = lTakeNameArray.GetCount() - 1; i >= 0 ; i--)
	{
	// It's useless to display the default animation because it is always empty.
	if(lTakeNameArray.GetAt(i)->Compare(KFBXTAKENODE_DEFAULT_NAME) == 0)
	{
	continue;
	}
	*/
	/*
	KString lOutputString = "Take Name: ";

	//m_pScene->SetCurrentTake(lTakeNameArray.GetAt(i)->Buffer());
	lOutputString += m_pScene->GetCurrentTakeName();
	lOutputString += "\n\n";
	CLogger::Current()->printf(lOutputString);
	*/
	if(strcmp(m_pScene->ActiveAnimStackName.Get().Buffer(), KFBXTAKENODE_DEFAULT_NAME) == 0)
	{
		CLogger::Current()->printf("ERROR: Cannot export default take!\n");
		return false;
	}
	KFbxAnimStack* pAnimStack=m_pScene->FindMember(FBX_TYPE(KFbxAnimStack),m_pScene->ActiveAnimStackName.Get().Buffer());
	KFbxTakeInfo* takeinfo=m_pScene->GetTakeInfo(m_pScene->ActiveAnimStackName.Get().Buffer());

	if(!pAnimStack)
	{
		CLogger::Current()->printf("ERROR: No Anim stack found!\n");
		return false;
	}

	EArray<SNodeEntry> nodes;
	KTime tmin, tmax;

	//fill scene nodes
	KFbxAnimLayer* pAnimLayer=pAnimStack->GetMember(FBX_TYPE(KFbxAnimLayer), 0);
	FillNodes(&filter, m_pScene->GetRootNode(), pAnimLayer,nodes);

	tmin = takeinfo->mReferenceTimeSpan.GetStart();
	tmax = takeinfo->mReferenceTimeSpan.GetStop();

	/*
	double duration = takeinfo->mLocalTimeSpan.GetDuration().GetSecondDouble();
	double dstart = takeinfo->mLocalTimeSpan.GetStart().GetSecondDouble();
	double dend = takeinfo->mLocalTimeSpan.GetStop().GetSecondDouble();


	tmin = takeinfo->mLocalTimeSpan.GetStart();
	tmax = takeinfo->mLocalTimeSpan.GetStop();
	*/
	//no known nodes! Get out of here
	if(nodes.IsEmpty())
	{
		return false;
	}

	//and copy them to CAnimation
	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		uint parent = (nodes[n].m_strParentName == NULL) ? 0xffffffff : anim->GetNodeIndex(nodes[n].m_strParentName);

		anim->AppendNode(nodes[n].m_strName, parent);
	}

	KTime timelength = tmax - tmin;

	double tl = timelength.GetSecondDouble();
	kLongLong frames;

	switch((int)fps)
	{
	case 0:
		frames = 1;
		break;

	case 8:
		frames = timelength.GetFrame(true, KTime::eCINEMA) / 3;
		break;

	case 10:
		frames = timelength.GetFrame(true, KTime::eFRAMES50) / 5;
		break;

	case 12:
		frames = timelength.GetFrame(true, KTime::eFRAMES48) / 4;
		break;

	case 15:
		frames = timelength.GetFrame(true, KTime::eFRAMES30) / 2;
		break;

	case 24:
		frames = timelength.GetFrame(true, KTime::eCINEMA);
		break;

	case 25:
		frames = timelength.GetFrame(true, KTime::ePAL);
		break;

	case 30:
		frames = timelength.GetFrame(true, KTime::eFRAMES30);
		break;

	case 48:
		frames = timelength.GetFrame(true, KTime::eFRAMES48);
		break;

	case 50:
		frames = timelength.GetFrame(true, KTime::eFRAMES50);
		break;

	case 60:
		frames = timelength.GetFrame(true, KTime::eFRAMES60);
		break;

	case 100:
		frames = timelength.GetFrame(true, KTime::eFRAMES100);
		break;

	case 120:
		frames = timelength.GetFrame(true, KTime::eFRAMES120);
		break;

	default:
		frames = timelength.GetSecondDouble() / fps;
		break;
	}

	Matrix43* keyframe = (Matrix43*)_alloca(sizeof(Matrix43) * nodes.GetCardinality());

	if(from < 0) from = 0;
	if(to < 0) to = frames;
	if(from > to)
		enf_swap(from, to);

	int nframes = to - from/* + 1*/;
	assert(nframes != 0);

	m_pScene->GetEvaluator()->SetContext(pAnimStack);
	for(kLongLong frame = from; frame <= to; frame++)
	{
		KTime tm;

		tm = tmin + (timelength * frame) / nframes;

		//presne na posledni frame
		if(frame == to && to == frames)
			tm = tmax;

		for(unsigned int n = 0; n < nodes.GetCardinality(); n++)
		{
			KFbxXMatrix mat = m_pScene->GetEvaluator()->GetNodeGlobalTransform(nodes[n].m_pNode,tm);
			//        CLogger::Current()->printf("frame %d node %s\n", (int)frame, nodes[n].m_strName);
			//			        KFbxVector4 ang = mat.GetT();
			//		        CLogger::Current()->printf("global %f %f %f\n", ang.GetAt(0), ang.GetAt(1), ang.GetAt(2));
			//        KFbxVector4 ang = mat.GetR();
			//        CLogger::Current()->printf("global %f %f %f\n", ang.GetAt(0), ang.GetAt(1), ang.GetAt(2));
			//        ang = nodes[n].m_pNode->GetLocalRFromCurrentTake(tm);
			//      CLogger::Current()->printf("local %f %f %f\n", ang.GetAt(0), ang.GetAt(1), ang.GetAt(2) );


			KFbxVector4 lT, lR, lS;
			KFbxXMatrix lGeometryOffset;

			lT = nodes[n].m_pNode->GetGeometricTranslation(KFbxNode::eSOURCE_SET);
			lR = nodes[n].m_pNode->GetGeometricRotation(KFbxNode::eSOURCE_SET);
			lS = nodes[n].m_pNode->GetGeometricScaling(KFbxNode::eSOURCE_SET);

			lGeometryOffset.SetT(lT);
			lGeometryOffset.SetR(lR);
			lGeometryOffset.SetS(lS);

			KFbxXMatrix lGlobalOffPosition = mat * lGeometryOffset;

			mat = lGlobalOffPosition;

			keyframe[n]._11 = mat.Get(0, 0); keyframe[n]._12 = mat.Get(0, 1); keyframe[n]._13 = mat.Get(0, 2);
			keyframe[n]._21 = mat.Get(1, 0); keyframe[n]._22 = mat.Get(1, 1); keyframe[n]._23 = mat.Get(1, 2);
			keyframe[n]._31 = mat.Get(2, 0); keyframe[n]._32 = mat.Get(2, 1); keyframe[n]._33 = mat.Get(2, 2);
			keyframe[n]._41 = mat.Get(3, 0); keyframe[n]._42 = mat.Get(3, 1); keyframe[n]._43 = mat.Get(3, 2);
		}

		anim->AppendKeyframeGlobal(keyframe);
	}
	//		::DisplayAnimation(m_pScene->GetRootNode());	
	//	}

	lTakeNameArray.Clear();
	return false;
}
