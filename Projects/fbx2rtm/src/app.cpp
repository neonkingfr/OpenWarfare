//#include "resource.h"

#include "app.h"
#include "MainWindow.h"
#include "CustomUtil.h"
#include "common/enf_memory.h"
#include "common/WinApiUtil.h"
#include "common/XMLConfig.h"
#include "tinyxml.h"

#include "wx/evtloop.h"
#include "wx/app.h"
#include "wx/msw/private.h"
#include "wx/msgdlg.h"

#ifdef _DEBUG
//#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

IMPLEMENT_APP(GuiApp)
extern GuiApp* g_app = NULL;

//----------------------------------------------------------------------------------------------------
class GuiAppEventLoop : public wxEventLoop
{
	bool PreProcessMessage(WXMSG* msg)
	{
		return wxEventLoop::PreProcessMessage(msg);
	}
};

//---------------------------------------------------------------------------------------------
int GuiApp::MainLoop()
{
	wxEventLoop* myloop = new GuiAppEventLoop;
	wxEventLoop* prevloop = m_mainLoop;
	m_mainLoop = myloop;

   int res = m_mainLoop->Run();
	m_mainLoop = prevloop;
	delete myloop;
	return res;
}

// ---------------------------------------------------------------------------
GuiApp::GuiApp()
{
	g_app = this;
	config = NULL;
	AnimDefConfig = NULL;
	SaveConfigOnExit = true;
}

// ---------------------------------------------------------------------------
bool GuiApp::OnInit()
{
#ifdef _DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );	//zariadi ze na uplnom konci aplikacie sa samo zavola _CrtDumpMemoryLeaks();
#endif
//	_CrtSetBreakAlloc(71097);

	//vytvorime config
	char AppDir[256];
	taGetAplicationDir(AppDir);

	wxString ConfigFile = AppDir;
	ConfigFile += "/config.xml";
	config = new XMLConfig(ConfigFile.c_str());	//FIXME TinyXML pouziva filesystem z enginu. coz je tu nevhodne. config by mal vzniknut minimalne pred vytvorenim hlavneho okna

	if(config->Load() == false)
	{
		wxMessageDialog dialog(NULL, "Config file load failed. Using default", "Warning", wxICON_INFORMATION | wxOK);
		dialog.ShowModal();
	}

	AnimDefConfig = new TiXmlDocument();

	wxString AnimDefFile = AppDir;
	AnimDefFile += "/animdef.xml";

	if(!taFileExists(AnimDefFile.c_str()))
	{
		wxMessageDialog dialog(NULL, "Missing animation system definition file animdef.xml \nMust be located at the executable file).", "Fbx2Rtm Init error", wxICON_ERROR | wxOK);
		dialog.ShowModal();
		SAFE_DELETE(AnimDefConfig);
		return false;
	}

	if(!AnimDefConfig->LoadFile(AnimDefFile.c_str()))
	{
		wxMessageDialog dialog(NULL, "Animation system definition file (animdef.xml)", "Fbx2Rtm Init error", wxICON_ERROR | wxOK);
		dialog.ShowModal();
		SAFE_DELETE(AnimDefConfig);
		return false;
	}

	CResult AnimDefCheck = CheckAnimDefData();
	
	if(!AnimDefCheck.IsOk())
	{
		if(AnimDefCheck.IsCommented())
		{
			wxMessageDialog dialog(NULL, AnimDefCheck.GetComment(), "Fbx2Rtm Init error", wxICON_ERROR | wxOK);
			dialog.ShowModal();
		}
		SAFE_DELETE(AnimDefConfig);
		return false;
	}

	config->Save();

	MainWindow* frame = new MainWindow(NULL, wxID_ANY, "Fbx2Rtm", wxDefaultPosition, wxSize(800, 540), wxMINIMIZE_BOX | wxSYSTEM_MENU | wxCAPTION | wxCLOSE_BOX | wxCLIP_CHILDREN, "Fbx2RtmMainWindow");
	frame->Show(true);
	SetTopWindow(frame);
	return true;
}

//---------------------------------------------------------------------------------------------
int GuiApp::OnExit()	//nezvratny koniec aplikacie
{
	if(SaveConfigOnExit)
	{
		bool saved = config->Save();

		if(!saved)
		{
			wxMessageDialog dialog(NULL, "Cannot save config file (config.xml)", "Error", wxICON_ERROR | wxOK);
			dialog.ShowModal();
		}
	}

	int res = wxApp::OnExit();	//zmaze sa veskere GUI, okna si ukladaju nastavenia a pod.

	SAFE_DELETE(AnimDefConfig);
	SAFE_DELETE(config);
	g_app = NULL;
#ifdef ENF_LEAK_DETECTOR
          MemoryManager::Dump("\n\nMemoryManager: Memory leaks report\n");
#endif
	return res;
}

//---------------------------------------------------------------------------------------------
CResult GuiApp::CheckAnimDefData()
{
	assert(AnimDefConfig);

	TiXmlElement* root = AnimDefConfig->RootElement();

	if(!root)
		return CResult(false, "Bad format of animation system definition file animdef.xml. \nCannot find root xml element");

	TiXmlElement* PrefixesElm = GetChildElement(root, "prefixes");

	if(!PrefixesElm)
		return CResult(true, "Bad format of animation system definition file animdef.xml. \nCannot find ""prefixes"" element");

	for(TiXmlNode* node = PrefixesElm->FirstChild(); node; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(elm && strcmp(elm->Value(), "prefixgroup") == 0)
			return CResult(true);
	}

	return CResult(true, "Bad format of animation system definition file animdef.xml. \nCannot find definitions of prefix groups (""prefixgroup"" elements)");
}

