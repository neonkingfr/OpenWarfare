
#include "MainWindow.h"
#include "CustomUtil.h"
#include "app.h"
#include "wxExtension/FileTextBox.h"
#include "wxExtension/Validators.h"
#include "common/XMLConfig.h"
#include "common/WinApiUtil.h"
#include "common/WxConvert.h"
#include "tinyxml.h"

#include "wx/menu.h"
#include "wx/statbox.h"
#include "wx/sizer.h"
#include "wx/settings.h"
#include "wx/stattext.h"
#include "wx/msgdlg.h"
#include "wx/checkbox.h"
#include "wx/arrstr.h"
#include "wx/msw/private.h"

MainWindow* g_MainFrame = NULL;

BEGIN_EVENT_TABLE(MainWindow, wxFrame)
	EVT_MENU(-1, MainWindow::OnSelectFromMenu)
	EVT_CLOSE(MainWindow::OnClose)
	EVT_FILETEXTBOX(-1, MainWindow::OnFileTextBoxChanged)
	EVT_COMBOBOX(-1, MainWindow::OnComboBoxChanged)
	EVT_TEXT_ENTER(-1, MainWindow::OnEditBoxChanged)
	EVT_TEXT(-1, MainWindow::OnEditBoxCharChanged)
	EVT_BUTTON(-1, MainWindow::OnButtonClick)
	EVT_CHECKBOX(-1, MainWindow::OnCheckBoxChanged)
	EVT_CHILD_FOCUS(MainWindow::OnChildFocus)
END_EVENT_TABLE()


//======================================================================
MainWindow::MainWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
	:wxFrame(parent, id, title, pos, size, style, name)
{
	assert(!g_MainFrame);
	g_MainFrame = this;

	FileMenu = NULL;
	InputFileBox = NULL;
	BindPoseFileBox = NULL;
	ModelConfigFileBox = NULL;
	SkeletonCombo = NULL;
	RootBoneCombo = NULL;
	FPSEditBox = NULL;
	ScaleEditBox = NULL;
	OutputDirBox = NULL;
	ConvertButton = NULL;
	ExitButton = NULL;
	CancelButton = NULL;
	OutputNameBox = NULL;
	SaveInPathCheck = NULL;
	StepCheckBox = NULL;
	PrevFocusedWin = NULL;
	FinalPathTextCtrl = NULL;
	InputToOutputButton = NULL;

	SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_INACTIVEBORDER));
//	SetMenuBar(CreateMenuBar());

	SkeletonCombo = new wxComboBox(this, COMBOBOX_SKELETON_NAME, wxEmptyString, wxDefaultPosition, wxSize(130, -1), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);
	RootBoneCombo = new wxComboBox(this, COMBOBOX_ROOT_BONE, wxEmptyString, wxDefaultPosition, wxSize(130, -1), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);
	StepCheckBox = new wxCheckBox(this, CHECKBOX_STEP, "step", wxDefaultPosition, wxDefaultSize);
	ModelConfigFileBox = new FileTextBox(this, FILEBOX_MODEL_CONFIG_FILE, wxDefaultPosition, wxSize(-1, 21), wxTE_PROCESS_ENTER);
	BindPoseFileBox = new FileTextBox(this, FILEBOX_BIND_POSE_FILE, wxDefaultPosition, wxSize(-1, 21), wxTE_PROCESS_ENTER);
	InputFileBox = new FileTextBox(this, FILEBOX_INPUT_FILE, wxDefaultPosition, wxSize(-1, 21), wxTE_PROCESS_ENTER, NULL, FTB_DIALOGTYPE_FILE | FTB_FILE_DROP_TARGET);
	FPSEditBox = new wxTextCtrl(this, EDITBOX_FPS, wxEmptyString, wxDefaultPosition, wxSize(60, 20), wxTE_PROCESS_ENTER, FloatValidator(0.0f, 10000.0f));
	ScaleEditBox = new wxTextCtrl(this, EDITBOX_SCALE, wxEmptyString, wxDefaultPosition, wxSize(60, 20), wxTE_PROCESS_ENTER, FloatValidator(0.0f, 10000.0f));
	OutputDirBox = new FileTextBox(this, EDITBOX_OUTPUT_DIR, wxDefaultPosition, wxSize(-1, 21), wxTE_PROCESS_ENTER, NULL, FTB_DIALOGTYPE_DIR);
	FinalPathTextCtrl = new wxTextCtrl(this, -1, "", wxDefaultPosition, wxDefaultSize, wxTE_READONLY/* | wxNO_BORDER*/);
	ConvertButton = new wxButton(this, BUTTON_CONVERT, _T("Convert"), wxDefaultPosition, wxSize(120, 30));
	ExitButton = new wxButton(this, BUTTON_EXIT, _T("Exit"), wxDefaultPosition, wxSize(120, 30));
	CancelButton = new wxButton(this, BUTTON_CANCEL, _T("Cancel"), wxDefaultPosition, wxSize(120, 30));
	OutputNameBox = new wxTextCtrl(this, EDITBOX_OUTPUT_FILENAME, wxEmptyString, wxDefaultPosition, wxSize(130, 20), wxTE_PROCESS_ENTER);
	InputToOutputButton = new wxButton(this, BUTTON_INPUT_TO_OUTPUT, _T("input filename to output name"), wxPoint(220, 50), wxSize(156, 20));

	wxStaticText* ModelConfigFileBoxLabel = new wxStaticText(this, -1, "model config", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* SkeletonComboLabel = new wxStaticText(this, -1, "skeleton", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* RootBoneComboLabel = new wxStaticText(this, -1, "root bone", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* BindPoseFileBoxLabel = new wxStaticText(this, -1, "bind pose", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* InputFileBoxLabel = new wxStaticText(this, -1, "input file", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* FPSEditBoxLabel = new wxStaticText(this, -1, "fps", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* ScaleEditBoxLabel = new wxStaticText(this, -1, "scale", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* OutputDirBoxLabel = new wxStaticText(this, -1, "output dir", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* OutputFileBoxLabel = new wxStaticText(this, -1, "output file", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* OutputNameBoxLabel = new wxStaticText(this, -1, "name", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	wxBoxSizer* PrefGroupsSizer = new wxBoxSizer(wxVERTICAL);
	PrefGroupsSizer->AddSpacer(12);
	assert(g_app->GetAnimDefConfig());

	TiXmlElement* root = g_app->GetAnimDefConfig()->RootElement();
	assert(root);

	TiXmlElement* PrefixesElm = GetChildElement(root, "prefixes");
	assert(PrefixesElm);

	int ComboCounter = 0;
	for(TiXmlNode* node = PrefixesElm->FirstChild(); node; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(elm && strcmp(elm->Value(), "prefixgroup") == 0)
		{
			const char* GroupName = elm->Attribute("name");

			if(!GroupName)
			{
				assert(false);
				continue;
			}

			wxStaticText* PefixComboLabel = new wxStaticText(this, -1, GroupName, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
			wxComboBox* PefixCombo = new wxComboBox(this, COMBOBOX_FIRST_PREFIX_GROUP + ComboCounter, wxEmptyString, wxDefaultPosition, wxSize(130, -1), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY, wxDefaultValidator, GroupName);
			ComboCounter++;

			int ItemCounter = 0;
			PefixCombo->Append("");
			PefixCombo->SetClientData(ItemCounter++, NULL);
			wxString item;

			for(TiXmlNode* snode = elm->FirstChild(); snode; snode = snode->NextSibling())
			{
				TiXmlElement* selm = snode->ToElement();

				if(selm && strcmp(selm->Value(), "prefix") == 0)
				{
					const char* GroupName = selm->Attribute("name");
					const char* GroupLabel = selm->Attribute("label");

					if(!GroupName)
					{
						assert(false);
						continue;
					}

					item = GroupName;

					if(GroupLabel && strlen(GroupLabel) > 0)
					{
						item += " (";
						item += GroupLabel;
						item += " )";
					}

					PefixCombo->Append(item);
					PefixCombo->SetClientData(ItemCounter++, new wxString(GroupName));
				}
			}

			PrefixCombos.Insert(PefixCombo);

			if(!PrefixCombos.IsEmpty())
				PrefGroupsSizer->AddSpacer(4);

			PrefGroupsSizer->Add(PefixComboLabel, 0, wxALIGN_TOP | wxALL, 0);
			PrefGroupsSizer->Add(PefixCombo, 0, wxALIGN_TOP | wxALL, 0);
		}
	}

	ModelConfigFileBox->button->SetLabel("...");
	BindPoseFileBox->button->SetLabel("...");
	InputFileBox->button->SetLabel("...");
	OutputDirBox->button->SetLabel("...");
	ScaleEditBox->SetValue("1.0");
	FPSEditBox->SetValue("30");
	ModelConfigFileBox->FileMask = "xml files (*.xml)|*.xml";
	BindPoseFileBox->FileMask = "fbx files (*.fbx)|*.fbx";
	InputFileBox->FileMask = "fbx files (*.fbx)|*.fbx";

	FinalPathTextCtrl->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_INACTIVEBORDER));


	wxStaticBox* InputBox = new wxStaticBox(this, -1, _T("Input"), wxDefaultPosition, wxDefaultSize, wxCLIP_CHILDREN | wxTAB_TRAVERSAL);
	wxStaticBoxSizer* InputSizer = new wxStaticBoxSizer(InputBox, wxVERTICAL);

	InputSizer->AddSpacer(32);
	InputSizer->Add(InputFileBoxLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	InputSizer->Add(InputFileBox, 0, wxGROW | wxALIGN_TOP | wxLEFT | wxRIGHT, 8);
	InputSizer->AddSpacer(4);
	InputSizer->Add(BindPoseFileBoxLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	InputSizer->Add(BindPoseFileBox, 0, wxGROW | wxALIGN_TOP | wxLEFT | wxRIGHT, 8);
	InputSizer->AddSpacer(4);
	InputSizer->Add(ModelConfigFileBoxLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	InputSizer->Add(ModelConfigFileBox, 0, wxGROW | wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	InputSizer->AddSpacer(4);
	InputSizer->Add(SkeletonComboLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	InputSizer->Add(SkeletonCombo, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	InputSizer->AddSpacer(4);
	InputSizer->Add(RootBoneComboLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	InputSizer->Add(RootBoneCombo, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	InputSizer->AddSpacer(4);
	InputSizer->Add(StepCheckBox, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);

	wxStaticBox* OutputBox = new wxStaticBox(this, -1, _T("Output"), wxDefaultPosition, wxDefaultSize, wxCLIP_CHILDREN | wxTAB_TRAVERSAL);
	wxStaticBoxSizer* OutputSizer = new wxStaticBoxSizer(OutputBox, wxVERTICAL);

	wxBoxSizer* FpsSizer = new wxBoxSizer(wxVERTICAL);
	FpsSizer->Add(FPSEditBoxLabel, 0, wxALIGN_TOP | wxALL, 0);
	FpsSizer->Add(FPSEditBox, 0, wxGROW | wxALIGN_TOP | wxALL, 0);

	wxBoxSizer* ScaleSizer = new wxBoxSizer(wxVERTICAL);
	ScaleSizer->Add(ScaleEditBoxLabel, 0, wxALIGN_TOP | wxALL, 0);
	ScaleSizer->Add(ScaleEditBox, 0, wxGROW | wxALIGN_TOP | wxALL, 0);

	wxBoxSizer* FpsAndScaleSizer = new wxBoxSizer(wxHORIZONTAL);
	FpsAndScaleSizer->Add(FpsSizer, 0, wxALIGN_TOP | wxALL, 0);
	FpsAndScaleSizer->AddSpacer(8);
	FpsAndScaleSizer->Add(ScaleSizer, 0, wxALIGN_TOP | wxALL, 0);
	FpsAndScaleSizer->AddStretchSpacer();

	wxBoxSizer* OutputNameBoxSizer = new wxBoxSizer(wxVERTICAL);
//	OutputNameBoxSizer->AddSpacer(16);
	OutputNameBoxSizer->Add(OutputNameBoxLabel, 0, wxALIGN_TOP | wxALL, 0);
	OutputNameBoxSizer->Add(OutputNameBox, 0, wxGROW | wxALIGN_TOP | wxALL, 0);
//	OutputNameBoxSizer->AddSpacer(16);

	wxStaticBox* OutputNameBox = new wxStaticBox(this, -1, "", wxDefaultPosition, wxDefaultSize);
	wxStaticBoxSizer* OutputNameSizer = new wxStaticBoxSizer(OutputNameBox, wxHORIZONTAL);
	OutputNameSizer->Add(PrefGroupsSizer, 0, wxGROW | wxALIGN_TOP | wxALL, 4);
//	OutputNameSizer->AddSpacer(4);
//	OutputNameSizer->Add(OutputNameBoxSizer, 1, wxGROW | wxALIGN_TOP | wxALL, 4);

	OutputSizer->AddSpacer(8);
	OutputSizer->Add(OutputDirBoxLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	OutputSizer->Add(OutputDirBox, 0, wxGROW | wxALIGN_TOP | wxLEFT | wxRIGHT, 8);
	OutputSizer->AddSpacer(16);
	OutputSizer->Add(OutputNameSizer, 0, wxGROW | wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	OutputSizer->AddSpacer(16);
	OutputSizer->Add(OutputNameBoxSizer, 0, wxGROW | wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	OutputSizer->AddSpacer(16);
//	OutputSizer->Add(OutputNameSizer, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	OutputSizer->Add(OutputFileBoxLabel, 0, wxGROW | wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	OutputSizer->Add(FinalPathTextCtrl, 0, wxGROW | wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	OutputSizer->AddSpacer(16);
	OutputSizer->Add(FpsAndScaleSizer, 0, wxGROW | wxALIGN_LEFT | wxLEFT | wxRIGHT, 8);
	OutputSizer->AddSpacer(8);

	wxBoxSizer* TopSizer = new wxBoxSizer(wxHORIZONTAL);
	TopSizer->Add(InputSizer, 1, wxGROW | wxALIGN_TOP | wxLEFT | wxRIGHT, 8);
	TopSizer->Add(OutputSizer, 1, wxGROW | wxALIGN_TOP | wxLEFT | wxRIGHT, 8);

	wxBoxSizer* ButSizer = new wxBoxSizer(wxHORIZONTAL);
	ButSizer->Add(ConvertButton, 0, wxALIGN_TOP | wxLEFT | wxRIGHT, 8);
	ButSizer->Add(ExitButton, 0, wxALIGN_TOP | wxLEFT | wxRIGHT, 8);
	ButSizer->Add(CancelButton, 0, wxALIGN_TOP | wxLEFT | wxRIGHT, 8);

	wxBoxSizer* MainSizer = new wxBoxSizer(wxVERTICAL);
	MainSizer->AddSpacer(16);
	MainSizer->Add(TopSizer, 1, wxGROW | wxALIGN_TOP | wxALL, 0);
	MainSizer->AddSpacer(4);
	MainSizer->Add(ButSizer, 0, wxALIGN_CENTER | wxALL, 0);
	MainSizer->AddSpacer(4);

	SaveInPathCheck = new wxCheckBox(this, CHECKBOX_GENERATE_OUTPUT_FILENAME, "save in path", wxPoint(430, 91), wxDefaultSize);
	SaveInPathCheck->SetValue(true);
	SetOutputGuiAccesibility(true);

	SkeletonCombo->Enable(false);
	RootBoneCombo->Enable(false);
	StepCheckBox->Enable(false);

	XMLConfig* cfg = g_app->GetConfig();
	XMLConfigNode* node = cfg->GetNode("defaults", false);

	if(node)
		SetGuiValuesFromConfig(node);

	GenerateOutputFilename();
	ConvertButton->SetFocus();
	SetSizer(MainSizer);

	SetMinSize(wxSize(800, -1));	//sirka okna fixna, vyska premenliva podla obsahu
	MainSizer->Fit(this);			//rozmer okna podla obsahu
}

//----------------------------------------------------------------------
MainWindow::~MainWindow()
{
	SaveGuiValuesForCurrentAnim();

	XMLConfig* cfg = g_app->GetConfig();
	XMLConfigNode* node = cfg->GetNode("defaults", false);

	if(node)
		StoreGuiValuesToConfig(node);

	//mazeme client data z comboboxov
	for(uint n = 0; n < PrefixCombos.GetCardinality(); n++)
	{
		wxComboBox* combo = PrefixCombos[n];

		for(uint i = 0; i < combo->GetCount(); i++)
		{
			wxString* str = (wxString*)combo->GetClientData(i);

			if(str)	//prvy item nema data
				delete str;
		}
	}
}

//----------------------------------------------------------------------------
void MainWindow::OnClose(wxCloseEvent& event)
{
/*	if ( event.CanVeto())
	{
		if(g_editor->ReadyToClose() == false)
		{
			event.Veto();
			return;
		}
	}*/

	event.Skip();
}

//---------------------------------------------------------------------------------------------
void MainWindow::OnChildFocus(wxChildFocusEvent& event)
{
	wxWindow* FocusedWin = event.GetWindow();

	if(FocusedWin == PrevFocusedWin)
		return;

	if(PrevFocusedWin == OutputNameBox)
		GenerateOutputFilename();

	PrevFocusedWin = FocusedWin;
	event.Skip();
}

//----------------------------------------------------------------------
wxMenuBar* MainWindow::CreateMenuBar()
{
	wxMenuBar* MenuBar = new wxMenuBar;										
	FileMenu = CreateFileMenu();
	MenuBar->Append(FileMenu, _T("&File"));
	return MenuBar;
}

//----------------------------------------------------------------------
wxMenu* MainWindow::CreateFileMenu()
{
   wxMenu* menu = new wxMenu;
	menu->Append(MENU_NEW, _T("New\tCtrl-N"), _T("create new param config"));
	menu->Append(MENU_LOAD, _T("Open\tCtrl-O"), _T("load param file config"));
	menu->Append(MENU_SAVE, _T("Save\tCtrl-S"), _T("save param file config"));
	menu->Append(MENU_SAVE_AS, _T("Save As"), _T("save param file config"));
	menu->Enable(MENU_SAVE, false);
	menu->Enable(MENU_SAVE_AS, false);
	return menu;
}

//----------------------------------------------------------------------
void MainWindow::OnSelectFromMenu(wxCommandEvent& event)
{
	int ID = event.GetId();
/*
	switch(ID)
	{
		case MENU_NEW:
			g_editor->New();
			break;
		case MENU_LOAD:
			g_editor->Load();
			break;
		case MENU_SAVE:
			g_editor->Save();
			break;
		case MENU_SAVE_AS:
			g_editor->SaveAs();
			break;
		case MENU_GENERATE_DEF_FILE:
			g_editor->SaveConfigDef();
			break;
		case MENU_UNDO:
			g_editor->Undo();
			break;
		case MENU_REDO:
			g_editor->Redo();
			break;
	}*/
	event.Skip();
}

//----------------------------------------------------------------------
void MainWindow::OnFileTextBoxChanged(wxCommandEvent& event)
{
	switch(event.GetId())
	{
		case FILEBOX_MODEL_CONFIG_FILE:
			LoadSkeletonCombo();

			if(SkeletonCombo->GetCount() > 0)
				SkeletonCombo->Select(0);

			SkeletonCombo->Enable(SkeletonCombo->GetCount() > 0);

			LoadBonesCombo();

			if(RootBoneCombo->GetCount() > 0)
				RootBoneCombo->Select(0);

			RootBoneCombo->Enable(RootBoneCombo->GetCount() > 0);
			StepCheckBox->Enable(RootBoneCombo->IsEnabled() && RootBoneCombo->GetSelection() != -1);

			break;
		case FILEBOX_INPUT_FILE:
		{
			wxString InputFile = InputFileBox->GetValue();

			if(taFileExists(InputFile.c_str()))
			{
				XMLConfig* cfg = g_app->GetConfig();
				XMLConfigNode* node = cfg->GetNode("animations", true);
				node = node->GetChildNodeWithAttr("anim", "path", InputFile.c_str(), false);

				if(node)
					SetGuiValuesFromConfig(node);
			}
			break;
		}
		case EDITBOX_OUTPUT_DIR:
		{
			GenerateOutputFilename();
			break;
		}
	}
	event.Skip();
}

//----------------------------------------------------------------------
void MainWindow::OnCheckBoxChanged(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case CHECKBOX_GENERATE_OUTPUT_NAME:
		GenerateOutputFilename();
		OutputNameBox->Enable(!event.IsChecked());
		break;
	case CHECKBOX_GENERATE_OUTPUT_FILENAME:
		SetOutputGuiAccesibility(event.IsChecked());
		GenerateOutputFilename();
		break;
	}
	event.Skip();
}

//----------------------------------------------------------------------
void MainWindow::GenerateOutputFilename()
{
	wxString OutputFile = OutputDirBox->GetValue();
	wxString name;
	bool GeneratePathAndName = SaveInPathCheck->IsChecked();

	if(OutputFile.Right(1) != "\\")
		OutputFile += '\\';

	if(GeneratePathAndName)
	{
		for(uint n = 0; n < PrefixCombos.GetCardinality(); n++)
		{
			wxComboBox* combo = PrefixCombos[n];
			int selected = combo->GetSelection();

			if(selected == -1)
				continue;

			wxString* prefix = (wxString*)combo->GetClientData(selected);

			if(!prefix || prefix->IsEmpty())
				continue;

			OutputFile += *prefix;

			if(!OutputFile.IsEmpty())
				OutputFile += '\\';

			name += *prefix;
		}

		if(!name.IsEmpty())
			name += '_';
	}

	wxString NameAppendString = OutputNameBox->GetValue();
	NameAppendString = NameAppendString.Trim(false);
	NameAppendString = NameAppendString.Trim(true);

	if(!NameAppendString.IsEmpty())
		name += NameAppendString;

	wxString extension = name.Right(3);
	extension.LowerCase();

	if(!OutputFile.IsEmpty())
	{
		if(extension != ".rtm")
			name += ".rtm";

//		OutputFile += '\\';
	}

	if(!name.IsEmpty())
		OutputFile += name;

	FinalPathTextCtrl->SetValue(OutputFile);
//	FinalPathTextCtrl->SetSelection(OutputFile.Length() - 1, OutputFile.Length() - 1);
}

//----------------------------------------------------------------------
void MainWindow::OnComboBoxChanged(wxCommandEvent& event)
{
	if(event.GetId() >= COMBOBOX_FIRST_PREFIX_GROUP)
	{
		GenerateOutputFilename();
	}
	else
	{
		switch(event.GetId())
		{
		case COMBOBOX_SKELETON_NAME:
			LoadBonesCombo();
			StepCheckBox->Enable(RootBoneCombo->IsEnabled() && RootBoneCombo->GetSelection() != -1);
			break;
		case COMBOBOX_ROOT_BONE:
			StepCheckBox->Enable(RootBoneCombo->IsEnabled() && RootBoneCombo->GetSelection() != -1);
			break;
		}
	}

	event.Skip();
}

//----------------------------------------------------------------------
void MainWindow::OnEditBoxChanged(wxCommandEvent& event)
{
//	FPSEditBox->
//	if(Validate();
//	FPSEditBox->TransferDataToWindow();

	switch(event.GetId())
	{
		case EDITBOX_OUTPUT_FILENAME:
			GenerateOutputFilename();
			break;
		case EDITBOX_FPS:
		{
			wxValidator* validator = FPSEditBox->GetValidator();

			if(!validator->Validate(FPSEditBox))
				FPSEditBox->SetValue("0");
		}
		case EDITBOX_SCALE:
		{
			wxValidator* validator = ScaleEditBox->GetValidator();

			if(!validator->Validate(ScaleEditBox))
				ScaleEditBox->SetValue("1.0");
			break;
		}
	}
	
	event.Skip();
}

//----------------------------------------------------------------------
void MainWindow::OnEditBoxCharChanged(wxCommandEvent& event)
{
	switch(event.GetId())
	{
		case EDITBOX_OUTPUT_FILENAME:
			GenerateOutputFilename();
			break;
		default:
			if(event.GetEventObject() == OutputDirBox->TextBox)
				GenerateOutputFilename();
	}
}

//----------------------------------------------------------------------
void MainWindow::OnButtonClick(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case BUTTON_CONVERT:
		StartConvertProcess();
		break;
	case BUTTON_EXIT:
		Close();
		break;
	case BUTTON_CANCEL:
		g_app->SaveConfigOnExit = false;
		Close();
		break;
	case BUTTON_INPUT_TO_OUTPUT:
		InputFilenameToOutput();
		break;
	}
//	event.Skip();
}

//----------------------------------------------------------------------
void MainWindow::SetGuiValuesFromConfig(XMLConfigNode* node)
{
	wxString value;
	bool BoolValue;

	if(node->GetValue("FPS", value))
		FPSEditBox->SetValue(value);

	if(node->GetValue("Scale", value))
		ScaleEditBox->SetValue(value);

	if(node->GetValue("InputFile", value)/* && taFileExists(value.c_str())*/)
		InputFileBox->SetValue(value);

	if(node->GetValue("BindPoseFile", value)/* && taFileExists(value.c_str())*/)
		BindPoseFileBox->SetValue(value);

	if(node->GetValue("OutputDir", value)/* && taFileExists(value.c_str())*/)
		OutputDirBox->SetValue(value);

	if(node->GetValue("ModelConfigFile", value)/* && taFileExists(value.c_str())*/)
		ModelConfigFileBox->SetValue(value);

	LoadSkeletonCombo();

	if(node->GetValue("Skeleton", value))
	{
		if(!value.IsEmpty())
			SkeletonCombo->SetStringSelection(value);
		else if(SkeletonCombo->GetCount() > 0)
			SkeletonCombo->Select(0);
	}

	SkeletonCombo->Enable(SkeletonCombo->GetCount() > 0);

	LoadBonesCombo();

	if(node->GetValue("RootBone", value))
		RootBoneCombo->SetStringSelection(value);

	RootBoneCombo->Enable(RootBoneCombo->GetCount() > 0);

	if(node->GetValue("StepCheck", BoolValue))
		StepCheckBox->SetValue(BoolValue);

	StepCheckBox->Enable(RootBoneCombo->IsEnabled() && RootBoneCombo->GetSelection() != -1);

	if(node->GetValue("OutputName", value))
		OutputNameBox->SetValue(value);

	if(node->GetValue("SaveInPathCheck", BoolValue))
	{
		SaveInPathCheck->SetValue(BoolValue);
		SetOutputGuiAccesibility(BoolValue);
	}

	node = node->GetChildNode("prefixes", true);

	for(uint n = 0; n < PrefixCombos.GetCardinality(); n++)
	{
		wxComboBox* combo = PrefixCombos[n];
	
		if(node->GetValue(combo->GetName(), value) == false)
			continue;

		int sel = -1;
		for(int i = 0; i < (int)combo->GetCount(); i++)
		{
			wxString* str = (wxString*)combo->GetClientData(i);

			if(str && *str == value)
			{
				sel = i;
				break;
			}
		}

		combo->SetSelection(sel);
	}
}

//----------------------------------------------------------------------
void MainWindow::StoreGuiValuesToConfig(XMLConfigNode* node)
{
	node->SetValue("InputFile", InputFileBox->GetValue());
	node->SetValue("BindPoseFile", BindPoseFileBox->GetValue());
	node->SetValue("ModelConfigFile", ModelConfigFileBox->GetValue());
	node->SetValue("OutputDir", OutputDirBox->GetValue());
	node->SetValue("FPS", FPSEditBox->GetValue());
	node->SetValue("Scale", ScaleEditBox->GetValue());
	node->SetValue("OutputName", OutputNameBox->GetValue());
	node->SetValue("Skeleton", SkeletonCombo->GetValue());
	node->SetValue("RootBone", RootBoneCombo->GetValue());
	node->SetValue("StepCheck", StepCheckBox->GetValue());
	node->SetValue("SaveInPathCheck", SaveInPathCheck->GetValue());

	node = node->GetChildNode("prefixes", true);

	for(uint n = 0; n < PrefixCombos.GetCardinality(); n++)
	{
		wxComboBox* combo = PrefixCombos[n];
		int sel = combo->GetSelection();
		wxString value;
		value.Clear();

		if(sel != -1)
		{
			wxString* str = (wxString*)combo->GetClientData(sel);

			if(str)
				value = *str;
		}
	
		node->SetValue(combo->GetName(), value);
	}
}

//----------------------------------------------------------------------
CResult MainWindow::ValidateGuiValues()
{
	if(taFileExists(InputFileBox->GetValue().c_str()) == false)
		return CResult(false, "Invalid (not existing) input file");

	if(taFileExists(BindPoseFileBox->GetValue().c_str()) == false)
		return CResult(false, "Invalid (not existing) bind pose file");

	if(taFileExists(ModelConfigFileBox->GetValue().c_str()) == false)
		return CResult(false, "Invalid (not existing) model config file");

	if(AtoF(FPSEditBox->GetValue()) < 0.0f)
		return CResult(false, "Invalid FPS value");

	if(AtoF(ScaleEditBox->GetValue()) < 0.0f)
		return CResult(false, "Invalid Scale value");

	wxString OutputFileDir = FinalPathTextCtrl->GetValue().c_str();
	OutputFileDir = OutputFileDir.BeforeLast('\\');
	PathIterator path(OutputFileDir.c_str());
	wxString CurDir;
	wxString FirstLevel = path.GetFirstLevel();

	for(wxString level = FirstLevel; !level.IsEmpty(); level = path.GetNextLevel())
	{
		if(!CurDir.empty())
			CurDir += "\\";

		CurDir += level.c_str();

		if(level == FirstLevel)	//prvy adresar resp. disk musi existovat
		{
			if(!taFileExists(CurDir.c_str()))
				return CResult(false, "Invalid (not existing) output file");
		}
		else	//zbytok sa da vytvorit
		{
			if(!taFileExists(CurDir.c_str()))
			{
				bool DirCreated = wxMkdir(CurDir);
				assert(DirCreated);

				if(!DirCreated)
				{
					wxString msg("Cannot create directory ");
					msg += CurDir;
					msg += " unknown error";
					return CResult(false, msg.c_str());
				}
			}
		}
	}

	if(taFileExists(OutputFileDir.c_str()) == false)	//uz musi existovat
		return CResult(false, "Invalid (not existing) output file");

//	FPSEditBox->GetValue();
//	ScaleEditBox->GetValue();
	return CResult(true);
}

//----------------------------------------------------------------------
void MainWindow::LoadSkeletonCombo()
{
	SkeletonCombo->Clear();
	RootBoneCombo->Clear();

	wxString ModelCfg = ModelConfigFileBox->GetValue();	//prave zmenena hodnota

	if(taFileExists(ModelCfg.c_str()))
	{
		TiXmlDocument ModelDoc;

		if(!ModelDoc.LoadFile(ModelCfg.c_str()))
		{
			wxMessageDialog dialog(NULL, "Cannot load model config file. bad format?", "Error", wxICON_ERROR | wxOK);
			dialog.ShowModal();
			return;
		}
		
		wxArrayString skeletons;
		LoadSkeletonNames(skeletons, &ModelDoc);

		SkeletonCombo->Freeze();

		for(uint n = 0; n < skeletons.GetCount(); n++)
			SkeletonCombo->Append(skeletons[n]);

		SkeletonCombo->Thaw();
	}
}

//----------------------------------------------------------------------
void MainWindow::LoadBonesCombo()
{
	wxString ModelCfg = ModelConfigFileBox->GetValue();	//prave zmenena hodnota

	if(taFileExists(ModelCfg.c_str()))
	{
		TiXmlDocument ModelDoc;

		if(!ModelDoc.LoadFile(ModelCfg.c_str()))
		{
			wxMessageDialog dialog(NULL, "Cannot load model config file. bad format?", "Error", wxICON_ERROR | wxOK);
			dialog.ShowModal();
			return;
		}

		RootBoneCombo->Freeze();
		RootBoneCombo->Clear();

		wxString SkeletonName = SkeletonCombo->GetValue();
		if(!SkeletonName.IsEmpty())
		{
			wxArrayString BoneNames;
			LoadBoneNamesFromSkeleton(BoneNames, &ModelDoc, SkeletonName.c_str());

			for(uint n = 0; n < BoneNames.GetCount(); n++)
				RootBoneCombo->Append(BoneNames[n]);

			if(RootBoneCombo->GetCount() > 0)
				RootBoneCombo->Select(0);
		}

		RootBoneCombo->Thaw();
	}
}

//extern GuiApp* g_app = NULL;
//----------------------------------------------------------------------
void MainWindow::StartConvertProcess()
{
	CResult result = ValidateGuiValues();

	if(result.IsOk())
	{
//			TODO: spustit convert
		SetCursor(wxCURSOR_WAIT);

		STARTUPINFO si;
		PROCESS_INFORMATION pi;
		GetStartupInfo(&si);

		SString<1024> parm;

		parm.format("%s -cfg \"%s\" -skeleton %s -bindpose \"%s\" -step %s %s -fps %d -scale %f -log \"export.log\" -output \"%s\" \"%s\"",
#ifdef _DEBUG
			"fbx2rtm_d.exe",
#else
			"fbx2rtm.exe",
#endif
			ModelConfigFileBox->GetValue().c_str(),
			SkeletonCombo->GetValue().c_str(),
			BindPoseFileBox->GetValue().c_str(),
			RootBoneCombo->GetValue().c_str(),
			StepCheckBox->GetValue() ? "-removestep" : "",
			AtoI(FPSEditBox->GetValue()),
			AtoF(ScaleEditBox->GetValue()),
			FinalPathTextCtrl->GetValue().c_str(),
			InputFileBox->GetValue().c_str());

//TODO: implemntovat
//			bool step = ;


//-cfg model.xml -skeleton ManSkeleton -step Hips -fps 30 -scale 0.01 -output C:\ArmA\o\arma\ca\Anims\Characters\data\Anim\sdr\UnarmedWalk_Fkostra.rtm  UnarmedWalk_F.fbx  >anim.log
		if(CreateProcess(NULL, (LPSTR)parm.cstr(), NULL, NULL, false, CREATE_DEFAULT_ERROR_MODE|CREATE_NO_WINDOW, NULL, NULL, &si, &pi))
		{
			DWORD retcode;
			BOOL res = false;
			while(true)
			{
				res = GetExitCodeProcess(pi.hProcess, &retcode);

				if(!res)
					break;
				if(retcode == STILL_ACTIVE)
				{
					::Sleep(50);
					continue;
				}
				break;
			}
			if(res && retcode != 0)
			{
				wxMessageDialog dialog(NULL, "Export error", "Error", wxICON_INFORMATION | wxOK);
				dialog.ShowModal();
			}

			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);
		}
		SetCursor(wxNullCursor);

		SaveGuiValuesForCurrentAnim();
	}
	else if(result.IsCommented())
	{
		wxMessageDialog dialog(NULL, result.GetComment(), "Error", wxICON_INFORMATION | wxOK);
		dialog.ShowModal();
	}
}

//----------------------------------------------------------------------
void MainWindow::SaveGuiValuesForCurrentAnim()
{
	wxString InputFile = InputFileBox->GetValue();

	if(!InputFile.IsEmpty() && taFileExists(InputFile.c_str()))
	{
		XMLConfig* cfg = g_app->GetConfig();
		XMLConfigNode* node = cfg->GetNode("animations", true);
		node = node->GetChildNodeWithAttr("anim", "path", InputFile.c_str(), true);

		if(node)
			StoreGuiValuesToConfig(node);
	}
}

//----------------------------------------------------------------------
void MainWindow::SetOutputGuiAccesibility(bool GenrateGui)
{
//	OutputDirBox->TextBox->SetEditable(!GenrateGui);
//	OutputDirBox->button->Enable(!GenrateGui);

//	if(GenrateGui)
//		OutputDirBox->TextBox->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_INACTIVEBORDER));
//	else
//		OutputDirBox->TextBox->SetBackgroundColour(OutputNameBox->GetBackgroundColour());

	for(uint n = 0; n < PrefixCombos.GetCardinality(); n++)
	{
		wxComboBox* combo = PrefixCombos[n];
		combo->Enable(GenrateGui);
	}
}

//----------------------------------------------------------------------
void MainWindow::InputFilenameToOutput()
{
	wxString name = InputFileBox->GetValue();
	name.Replace("/", "\\", true);
	name = name.AfterLast('\\');
	name = name.BeforeLast('.');
	OutputNameBox->SetValue(name);
	GenerateOutputFilename();
}