
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "wx/frame.h"
#include "common/enf_types.h"
#include "common/CResult.h"

class wxMenuBar;
class wxMenu;
class FileTextBox;
class wxComboBox;
class wxTextCtrl;
class wxButton;
class wxCheckBox;
class wxBoxSizer;
class XMLConfigNode;

enum GuiIDs
{
	MENU_NEW = 1,
	MENU_LOAD,
	MENU_SAVE,
	MENU_SAVE_AS,
	FILEBOX_INPUT_FILE,
	FILEBOX_BIND_POSE_FILE,
	FILEBOX_MODEL_CONFIG_FILE,
	BUTTON_CONVERT,
	BUTTON_EXIT,
	BUTTON_CANCEL,
	COMBOBOX_SKELETON_NAME,
	COMBOBOX_ROOT_BONE,
	COMBOBOX_FIRST_PREFIX_GROUP = 100,
	EDITBOX_OUTPUT_FILENAME,
	EDITBOX_OUTPUT_DIR,
	EDITBOX_FPS,
	EDITBOX_SCALE,
	CHECKBOX_GENERATE_OUTPUT_NAME,
	CHECKBOX_GENERATE_OUTPUT_FILENAME,
	CHECKBOX_STEP,
	BUTTON_INPUT_TO_OUTPUT,
};

//======================================================================
class MainWindow : public wxFrame
{
public:
	MainWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE, const wxString& name = "frame");
	~MainWindow();
	wxMenu*						GetFileMenu(){return FileMenu;}
protected:
	wxMenuBar*					CreateMenuBar();
	wxMenu*						CreateFileMenu();
	wxMenu*						FileMenu;
	FileTextBox*				InputFileBox;
	FileTextBox*				BindPoseFileBox;
	FileTextBox*				ModelConfigFileBox;
	wxComboBox*					SkeletonCombo;
	wxComboBox*					RootBoneCombo;
	wxTextCtrl*					FPSEditBox;
	wxTextCtrl*					ScaleEditBox;
	FileTextBox*				OutputDirBox;
	wxButton*					ConvertButton;
	wxButton*					ExitButton;
	wxButton*					CancelButton;
	wxButton*					InputToOutputButton;
	EArray<wxComboBox*>		PrefixCombos;
	wxTextCtrl*					OutputNameBox;
	wxCheckBox*					SaveInPathCheck;
	wxCheckBox*					StepCheckBox;
	wxTextCtrl*					FinalPathTextCtrl;
private:
	wxWindow* PrevFocusedWin;
	void StartConvertProcess();
	void SaveGuiValuesForCurrentAnim();
	void LoadSkeletonCombo();
	void LoadBonesCombo();
	void GenerateOutputFilename();
	void InputFilenameToOutput();
	CResult ValidateGuiValues();
	void SetOutputGuiAccesibility(bool GenrateGui);
	void SetGuiValuesFromConfig(XMLConfigNode* node);
	void StoreGuiValuesToConfig(XMLConfigNode* node);
	void OnClose(wxCloseEvent& event);
	void OnSelectFromMenu(wxCommandEvent& event);
	void OnFileTextBoxChanged(wxCommandEvent& event);
	void OnComboBoxChanged(wxCommandEvent& event);
	void OnEditBoxChanged(wxCommandEvent& event);
	void OnButtonClick(wxCommandEvent& event);
	void OnCheckBoxChanged(wxCommandEvent& event);
	void OnEditBoxCharChanged(wxCommandEvent& event);
	void OnChildFocus(wxChildFocusEvent& event);
	DECLARE_EVENT_TABLE();
};

extern MainWindow* g_MainFrame;

#endif