
#ifndef CUSTOM_UTIL_H
#define CUSTOM_UTIL_H

class TiXmlDocument;
class TiXmlElement;
class TiXmlNode;

//#include "common/enf_strings.h"
#include "wx/string.h"
#include "common/enf_types.h"

//===============================================================================
class PathIterator
{
public:
					PathIterator();
					PathIterator(const char* path);
	const char*	AddLevel(const char* name);
	wxString		GetFirstLevel() const;
	wxString		GetNextLevel() const;
	const char* GetLastLevel() const;
	const char* GetPath() const;
	bool			operator>(const PathIterator& p);
	bool			operator<(const PathIterator& p);
	bool			operator==(const PathIterator& p);
//					ENF_DECLARE_MMOBJECT(PathIterator);
private:
	wxString path;
	mutable int pos;
};

TiXmlElement* GetChildElement(TiXmlNode* parent, const char* name);
void LoadSkeletonNames(wxArrayString& skeletons, TiXmlDocument* doc);
void LoadBoneNamesFromSkeleton(wxArrayString& BoneNames, TiXmlDocument* doc, enf::CStr name);

#endif