#ifndef FBXMAIN_H
#define FBXMAIN_H

#include <fbxsdk.h>
#include <vector>
#include "common/enf_types.h"
#include "common/enf_mathlib.h"
#include "common/enf_eset.h"

class CLogger
{
protected:
	static CLogger*	m_pCLogger;

public:
	virtual void printf(const char* str, ...)=0;
	virtual CLogger& operator <<(const char* str)=0;
	static CLogger* Current()
	{
		return m_pCLogger;
	}
	CLogger()
	{
		m_pCLogger = this;
	}
	virtual ~CLogger()
	{
		if(this == m_pCLogger) m_pCLogger = NULL;
	}
};

class CFbxManager;

/*!
Filter for scene node iterator
Inherit this class and reimplement the Filter method to
involve your own filtering
*/
class CNodeFilter
{
public:
	CNodeFilter() {}
	~CNodeFilter() {}
	virtual bool Filter(const char* nodeName, const char** nodeName2) const=0;
};

//No case sensitive version
class DStringNC: public DString
{
public:
	DStringNC(const char* str): DString(str) {}
	DStringNC() {}
};

inline bool operator == (const DStringNC & a, const DStringNC & b)
{
	return    ( a.length() == b.length() )				// optimization on some platforms
		&& ( enf::strcmpi(a.c_str(), b.c_str()) == 0 );	// actual compare
}
inline bool operator < (const DStringNC & a, const DStringNC & b)
{
	return enf::strcmpi(a.c_str(), b.c_str()) < 0;
}

inline bool operator != (const DStringNC & a, const DStringNC & b) { return !(a == b); }
inline bool operator >  (const DStringNC & a, const DStringNC & b) { return b < a; }

class CfgNodeFilter: public CNodeFilter
{
	typedef EPair<DStringNC, DString> NameMapping;

	ESet<NameMapping> NodeMapping;

public:
	void AddMapping(const char* name, const char* name2)
	{
		NodeMapping.Insert(NameMapping(name, name2));
	}

	bool Filter(const char* nodeName, const char** nodeName2) const
	{
		//if(strcmpi(nodeName, "WPN_launcher") == 0)
		//	nodeName = nodeName;

		if(!NodeMapping.IsEmpty())
		{
			uint index = NodeMapping.GetIndexOf(NameMapping(nodeName, NULL));

			if(index == -1)
			{
				//try it again, with stripped prefixes
				const char* name = nodeName;

				//strip prefixes
				while(*name == ' ' || *name == '_')
					name++;

				index = NodeMapping.GetIndexOf(NameMapping(name, NULL));
				if(index == -1)
					return false;
			}
			*nodeName2 = NodeMapping[index].Data.c_str();
			return true;
		}

		*nodeName2 = nodeName;
		return true;
	}
};


class CAnimation;

/*!
Just for information about nodes in scene. Used by FbxScene::FillNodes
It's filled with names taken from CNodeFilter if supplied
*/
struct SNodeEntry
{
	const char*	m_strName;
	KFbxNode*	m_pNode;
	const char*	m_strParentName;
	KFbxNode*	m_pParent;

	SNodeEntry(): m_strName(NULL), m_pNode(NULL), m_strParentName(NULL), m_pParent(NULL)
	{
	}
};

class CFbxScene
{
protected:
	friend class CFbxManager;
	KFbxScene*		m_pScene;
	CFbxManager*	m_pManager;

	void r_FillNodes(const CNodeFilter* filter, KFbxNode* pNode, KFbxAnimLayer* pAnimLayer, const char* parentname, EArray<SNodeEntry>& nodes, KTime& tmin, KTime& tmax) const;

public:
	void FillNodes(const CNodeFilter* filter, KFbxNode* pNode, KFbxAnimLayer* pAnimLayer, EArray<SNodeEntry>& nodes) const;
	bool Save(const char* pFilename, int pFileFormat, bool pEmbedMedia);
	bool Export(CAnimation* anim, const CNodeFilter& filter, float fps, int from, int to);

	CFbxScene(CFbxManager* manager);
	~CFbxScene();
};

class CFbxManager
{
protected:
	friend class CFbxScene;
	KFbxSdkManager*	m_pSdkManager;
	CLogger*	m_pLogger;

public:
	CFbxScene* LoadScene(const char* name, const char* password, bool verbose = true);
	CFbxManager(CLogger* logger);
	~CFbxManager();
};


#endif