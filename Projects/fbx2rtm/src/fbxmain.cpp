#include "stdafx.h"
#include "fbxmain.h"


CLogger* CLogger::m_pCLogger = NULL;

//---------------------------------------------------------------------------
CFbxManager::CFbxManager(CLogger* logger):
m_pSdkManager(NULL),
m_pLogger(logger)
{
	assert(logger != NULL);

	m_pSdkManager = KFbxSdkManager::Create();//KFbxSdkManager::CreateKFbxSdkManager();

	if (!m_pSdkManager)
	{
		m_pLogger->printf("Unable to create the FBX SDK manager\n");
		exit(0);
	}

	// create an IOSettings object
	KFbxIOSettings * ios = KFbxIOSettings::Create(m_pSdkManager, IOSROOT );
	m_pSdkManager->SetIOSettings(ios);

	// Load plugins from the executable directory
	KString lPath = KFbxGetApplicationDirectory();
	KString lExtension = "dll";
	m_pSdkManager->LoadPluginsDirectory(lPath.Buffer(), lExtension.Buffer());
}

//---------------------------------------------------------------------------
CFbxManager::~CFbxManager()
{
	if (m_pSdkManager) m_pSdkManager->Destroy();//m_pSdkManager->DestroyKFbxSdkManager();
	m_pSdkManager = NULL;
}


//---------------------------------------------------------------------------
CFbxScene::CFbxScene(CFbxManager* manager): m_pManager(manager), m_pScene(NULL)
{
	m_pScene = KFbxScene::Create(manager->m_pSdkManager,"");
}

//---------------------------------------------------------------------------
CFbxScene::~CFbxScene()
{
	if(m_pScene)
	{
		m_pScene->Destroy();
		m_pScene = NULL;
	}
}

//---------------------------------------------------------------------------
bool CFbxScene::Save(const char* pFilename, int pFileFormat, bool pEmbedMedia)
{
	int lMajor, lMinor, lRevision;
	bool lStatus = true;

	// Create an exporter.
	KFbxExporter* lExporter = KFbxExporter::Create(m_pManager->m_pSdkManager, "");

	KFbxSdkManager::GetFileFormatVersion(lMajor, lMinor, lRevision);
	m_pManager->m_pLogger->printf("FBX version number for this version of the FBX SDK is %d.%d.%d\n\n", lMajor, lMinor, lRevision);


	if( pFileFormat < 0 || pFileFormat >= m_pManager->m_pSdkManager->GetIOPluginRegistry()->GetWriterFormatCount() )
	{
		// Write in fall back format if pEmbedMedia is true
		pFileFormat = m_pManager->m_pSdkManager->GetIOPluginRegistry()->GetNativeWriterFormat();

		if (!pEmbedMedia)
		{
			//Try to export in ASCII if possible
			int lFormatIndex, lFormatCount = m_pManager->m_pSdkManager->GetIOPluginRegistry()->GetWriterFormatCount();

			for (lFormatIndex=0; lFormatIndex<lFormatCount; lFormatIndex++)
			{
				if (m_pManager->m_pSdkManager->GetIOPluginRegistry()->WriterIsFBX(lFormatIndex))
				{
					KString lDesc = m_pManager->m_pSdkManager->GetIOPluginRegistry()->GetWriterFormatDescription(lFormatIndex);
					if (lDesc.Find("ascii")>=0)
					{
						pFileFormat = lFormatIndex;
						break;
					}
				}
			}
		}
	}

	KFbxIOSettings *settings=m_pManager->m_pSdkManager->GetIOSettings();
	settings->SetBoolProp(EXP_FBX_MATERIAL,true);
	settings->SetBoolProp(EXP_FBX_TEXTURE,true);
	settings->SetBoolProp(EXP_FBX_EMBEDDED, pEmbedMedia);
	settings->SetBoolProp(EXP_FBX_SHAPE, true);
	settings->SetBoolProp(EXP_FBX_GOBO, true);
	settings->SetBoolProp(EXP_FBX_ANIMATION, true);
	settings->SetBoolProp(EXP_FBX_GLOBAL_SETTINGS, true);

	// Initialize the exporter by providing a filename.
	if(lExporter->Initialize(pFilename,pFileFormat,settings) == false)
	{
		m_pManager->m_pLogger->printf("Call to KFbxExporter::Initialize() failed.\n");
		m_pManager->m_pLogger->printf("Error returned: %s\n\n", lExporter->GetLastErrorString());
		return false;
	}

	// Export the scene.
	lStatus = lExporter->Export(m_pScene); 

	// Destroy the exporter.
	lExporter->Destroy();
	return lStatus;
}

//---------------------------------------------------------------------------
CFbxScene* CFbxManager::LoadScene(const char* pFilename, const char* pPassword, bool verbose)
{
	int lFileMajor, lFileMinor, lFileRevision;
	int lSDKMajor,  lSDKMinor,  lSDKRevision;
	int lFileFormat = -1;
	int lTakeCount;
	KString lCurrentTakeName;
	bool lStatus;

	CFbxScene* pScene = new CFbxScene(this);

	// Get the file version number generate by the FBX SDK.
	KFbxSdkManager::GetFileFormatVersion(lSDKMajor, lSDKMinor, lSDKRevision);

	// Create an importer.
	KFbxImporter* lImporter = KFbxImporter::Create(m_pSdkManager,"");

	// Initialize the importer by providing a filename.
	KFbxIOSettings *settings=m_pSdkManager->GetIOSettings();
	const bool lImportStatus = lImporter->Initialize(pFilename,-1, m_pSdkManager->GetIOSettings());
	lImporter->GetFileVersion(lFileMajor, lFileMinor, lFileRevision);

	if( !lImportStatus )
	{
		m_pLogger->printf("Call to KFbxImporter::Initialize() failed.\n");
		m_pLogger->printf("Error returned: %s\n\n", lImporter->GetLastErrorString());

		if (lImporter->GetLastErrorID() == KFbxIO::eFILE_VERSION_NOT_SUPPORTED_YET ||
			lImporter->GetLastErrorID() == KFbxIO::eFILE_VERSION_NOT_SUPPORTED_ANYMORE)
		{
			m_pLogger->printf("FBX version number for this FBX SDK is %d.%d.%d\n", lSDKMajor, lSDKMinor, lSDKRevision);
			m_pLogger->printf("FBX version number for file %s is %d.%d.%d\n\n", pFilename, lFileMajor, lFileMinor, lFileRevision);
		}

		return false;
	}

	if(verbose)
		m_pLogger->printf("FBX version number for this FBX SDK is %d.%d.%d\n", lSDKMajor, lSDKMinor, lSDKRevision);

	if (lImporter->IsFBX())
	{
		lTakeCount = lImporter->GetAnimStackCount();

		if(verbose)
		{
			m_pLogger->printf("FBX version number for file %s is %d.%d.%d\n\n", pFilename, lFileMajor, lFileMinor, lFileRevision);

			// From this point, it is possible to access take information without
			// the expense of loading the entire file.

			m_pLogger->printf("Take Information\n");

			m_pLogger->printf("    Number of Animation Stacks: %d\n", lTakeCount);
			m_pLogger->printf("    Current Animation Stack: \"%s\"\n", lImporter->GetActiveAnimStackName().Buffer());
			m_pLogger->printf("\n");

			for(int i = 0; i < lTakeCount; i++)
			{
				KFbxTakeInfo* lTakeInfo = lImporter->GetTakeInfo(i);

				printf("    Animation Stack %d\n", i);
				printf("         Name: \"%s\"\n", lTakeInfo->mName.Buffer());
				printf("         Description: \"%s\"\n", lTakeInfo->mDescription.Buffer());

				// Change the value of the import name if the animation stack should be imported 
				// under a different name.
				printf("         Import Name: \"%s\"\n", lTakeInfo->mImportName.Buffer());

				// Set the value of the import state to false if the animation stack should be not
				// be imported. 
				printf("         Import State: %s\n", lTakeInfo->mSelect ? "true" : "false");
				printf("\n");
			}
		}

		// Set the import states. By default, the import states are always set to 
		// true. The code below shows how to change these states.
		//		settings->SetBoolProp(IMP_FBX_MATERIAL,true);
		//		settings->SetBoolProp(IMP_FBX_TEXTURE, true);
		settings->SetBoolProp(IMP_FBX_MATERIAL, false);
		settings->SetBoolProp(IMP_FBX_TEXTURE, false);
		//      settings->SetBoolProp(IMP_FBX_EMBEDDED, false);
		//		settings->SetBoolProp(IMP_FBX_LINK, true);
		//		settings->SetBoolProp(IMP_FBX_SHAPE, true);
		//		settings->SetBoolProp(IMP_FBX_GOBO, true);
		settings->SetBoolProp(IMP_FBX_LINK,false);
		settings->SetBoolProp(IMP_FBX_SHAPE, false);
		settings->SetBoolProp(IMP_FBX_GOBO, false);
		settings->SetBoolProp(IMP_FBX_ANIMATION, true);
		settings->SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, true);

	}


	// Import the scene.
	lStatus = lImporter->Import(pScene->m_pScene);

	if(lStatus == false && lImporter->GetLastErrorID() == KFbxIO::ePASSWORD_ERROR && pPassword)
	{
		KString lString(pPassword);
		settings->SetStringProp(IMP_FBX_PASSWORD, lString);
		settings->SetBoolProp(IMP_FBX_PASSWORD_ENABLE, true);
		lStatus = lImporter->Import(pScene->m_pScene);

		if(lStatus == false && lImporter->GetLastErrorID() == KFbxIO::ePASSWORD_ERROR)
		{
			m_pLogger->printf("\nPassword is wrong, import aborted.\n");
		}
	}

	//set current take
	if(lStatus)
		pScene->m_pScene->ActiveAnimStackName=lImporter->GetActiveAnimStackName();

	// Destroy the importer.
	if(lStatus == false)
	{
		delete pScene;
		pScene = NULL;
	}

	return pScene;
}

