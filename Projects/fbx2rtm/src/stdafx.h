// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#include <stdio.h>
#include <stdarg.h>
#include <tchar.h>



// TODO: reference additional headers your program requires here
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <vector>

// current version: fbx 2011.3
#include <fbxsdk.h>
#include <fbxfilesdk_nsuse.h>

#include "common/enf_types.h"
#include "common/enf_mathlib.h"
#include "cserialize.h"
#include "canimation.h"
