// Fbx2Rtm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdarg.h>
#include "fbxmain.h"
#include "canimation.h"
#include "cserialize.h"
#include "tinyxml.h"

#define Assert assert
#include <Es/Common/mathND.hpp>

//#define RTM_OPTIMIZE
#define RTM_MIRROR

//---------------------------------------------------------------------------
void ShowHelp()
{
	printf("\n");
	printf("--------------------------------\n");
	printf("--- FBX to RTM exporter v1.0 ---\n");
	printf("-- (c)2007                    --\n");
	printf("--------------------------------\n\n");
	printf("\nUsage:\n");
	printf("fbx2rtm [[-cfg model.xml] [-skeleton skeletonname]][-step bonename][-removestep][-bindpose bindpose.fbx][-fps framerate][-scale scale][-log logname][-output filename.rtm] filename.fbx\n");
	printf("Options:\n");
	printf("cfg\t\tXML config with skeleton definitions\n");
	printf("skeleton\tName of skeleton definition\n");
	printf("step\t\tName of root bone defining movement (Usually \"Hips\")\n");
	printf("removestep\tNeutralize movement (remove step)\n");
	printf("bindpose\tFbx scene with character in bind pose.\n\t\tWhen not defined, the first animation frame is taken as bind pose.\n");
	printf("fps\t\tDesired animation framerate. Default 30\n");
	printf("scale\t\tScale factor. Default is 0.01\n");
	printf("log\t\tName of log-file\n");
	printf("output\t\tOutput file name. Default is source name with .rtm extension\n\n");
}

//---------------------------------------------------------------------------
static void FindCoordSpace(int coord[4], const Vector3 *source)
{
  int i,j,k,l;
  int nSel=4;
  //if( nSel<1 ) return MIdentity; // no point
  // find four selected points
  // they should define coordinate space
  // i.e for any (i,j,k) min(begP[i]-begP[j])*(begP[k]-begP[j]) should be maximal
  float minMax=1e10;
  int iMax=-1,jMax=-1,kMax=-1,lMax=-1;
  for( i=0; i<nSel; i++ )
    for( j=0; j<nSel; j++ ) if( j!=i )
      for( k=0; k<nSel; k++ ) if( k!=i && k!=j )
        for( l=0; l<nSel; l++ ) if( l!=i && l!=j && l!=k )
        {
          // all possible combinations are tested here
          Vector3 jMi=(source[j]-source[i]).Normalized();
          Vector3 kMi=(source[k]-source[i]).Normalized();
          Vector3 lMi=(source[l]-source[i]).Normalized();
          //float jMiSize2=jMi.SquareSize();
          //float kMiSize2=kMi.SquareSize();
          //float lMiSize2=lMi.SquareSize();
          float jk=jMi.Dot(kMi);
          float jl=jMi.Dot(lMi);
          float kl=kMi.Dot(lMi);
          float maxCosA=enf_max(jk,enf_max(jl,kl));
          //float minSP=fabs(jk*jl*kl)/(jMiSize2*kMiSize2*lMiSize2);
          if( minMax>maxCosA )
          {
            minMax=maxCosA;
            iMax=i,jMax=j,kMax=k,lMax=l;
            // if we have some good solution, we need not to search for the best
            if( minMax<0.1 ) goto Solution;
          }
        }

  if( iMax<0 || minMax>0.8 )
  {
    // not enough points to define whole transformation matrix
    coord[0]=coord[1]=coord[2]=coord[3]=-1;
    return;
  }

  Solution:;
  coord[0]=iMax;
  coord[1]=jMax;
  coord[2]=kMax;
  coord[3]=lMax;
}
//---------------------------------------------------------------------------
static Matrix43 CalcTransform
(
 const Vector3 pBeg[4],
 const Vector3 pEnd[4]
)
{
	int i,j;

	// create a lin. equation system matrix
	// we search for a matrix M (3x4) (rows * columns):
	// M*pBeg[i]=pEnd[i]
	Matrix<12,12> left;
	Vector<12> right;
	left.InitZero();
	// M(i,j) corresponds to solution(i*4+j) (column index)
	for( int k=0; k<4; k++ )
	{
		// insert condition below into the system
		// M*pBeg[i]=pEnd[i]
		for( i=0; i<3; i++ )
		{
			int row=k*3+i;
			assert( row<12 );
			for( j=0; j<3; j++ )
			{
				int col=i*4+j;
				assert( col<12 );
				left(row,col)=pBeg[k][j];
			}
			left(row,i*4+3)=1;
			right[row]=pEnd[k][i];
		}
	}
	Vector<12> solution;
	// solve lin. equation system
	Matrix<12,12> invLeft;
	bool regular=invLeft.InitInversed(left);
	assert( regular );
	Multiply(solution,invLeft,right);

	// fill matrix with the solution
	Matrix43 transform;
	for( i=0; i<3; i++ )
	{
		for( j=0; j<3; j++ )
			transform.m[j][i] = solution[i * 4 + j];

		transform.m[3][i] = solution[i * 4 + 3];
	}
	return transform;
}

//---------------------------------------------------------------------------
void FourRefPoints(const Matrix43& mat, Vector3* p)
{
  //	static const Vector3 refp[4] = {Vector3(4,0,0),Vector3(0,4,0),Vector3(0,0,4),Vector3(0,0,0)};
  static const Vector3 refp[4] = {Vector3(-4,-4,-4), Vector3(4,-4,-4),Vector3(-4,4,-4),Vector3(-4,-4,4)};
//  static const Vector3 refp[4] = {Vector3(0,0,0), Vector3(1,0.5,0),Vector3(0,1,0.5),Vector3(0.5,0,-1)};

	for(int v = 0; v < 4; v++)
	{
		p[v] = mat.TransformCoord(refp[v]);
#ifdef RTM_MIRROR
		p[v].z *= -1.0f;
#endif
	}
}

//---------------------------------------------------------------------------
class CRTM101File: public CAnimExporter
{
protected:

	static const char Magic101[8];
	
	bool CompareMatrix(const Matrix43& mat1, const Matrix43& mat2, float rdelta, float tdelta)
	{
		float d;
		for(int r = 0; r < 3; r++)
		{
			d = mat1[r].Dot(mat2[r]);
			d = enf_limit(d, 0.0f, 1.0f);
			if(acos(d) > rdelta)
				return false;
		}
		d = mat1[3].Distance(mat2[3]);
		return d < tdelta;
	}

public:
	bool Export(const char* name, const CAnimation* anim, const CAnimation* bindpose, const SExportParms& parms, CLogger* logger)
	{
    if(anim->GetNumFrames() == 0 || anim->GetNumFrames() == 0)
    {
      m_pLogger->printf("\nNo frames/bones to export!\n\n", name);
      return false;
    }


    CFileOutputStream outstream(name);
		if(!outstream.IsOpen())
			return false;

		CSerializeOutput so(&outstream);

		so.SetMode(CSerialize::LITTLEENDIAN);
		
		float x = 0, y = 0, z = 0;

		uint nPhases;
		uint nStartPhase;
		uint nEndPhase;
		uint nBones = anim->GetNumNodes();


		if(bindpose == NULL)
		{
			nPhases = anim->GetNumFrames() - 1;
			nStartPhase = 1;
		}
		else
		{
			nPhases = anim->GetNumFrames();
			nStartPhase = 0;
		}
		nEndPhase = anim->GetNumFrames() - 1;

		uint rBones = 0; //real number of matrices
		uint* boneindices = (uint*)_alloca(sizeof(uint) * nBones); //indices to them

		int ihips = anim->GetNodeIndex(parms.m_strCenterBone.c_str());
		if (!parms.m_strCenterBone.empty() && ihips==-1)
			m_pLogger->printf("\nHips bone '%s' not found!\n\n", parms.m_strCenterBone.c_str());

		//Build anim to bind-pose node mapping
		uint* bone2bindmapping = (uint*)_alloca(sizeof(uint) * nBones);

		for(uint bone = 0; bone < nBones; bone++)
		{
			//skip hips!
		/*	if(bone == ihips)
			{
				bone2bindmapping[bone] = -1;
				continue;
			}*/

			if(bindpose)
				bone2bindmapping[bone] = bindpose->GetNodeIndex(anim->GetNodeName(bone));
			else
				bone2bindmapping[bone] = bone;
		}

		//filter out bones without movement
		const CAnimation* bind = (bindpose == NULL) ? anim : bindpose;
		for(uint b = 0; b < nBones; b++)
		{
			//there could be unknown bones! Filter them out!
			if(bone2bindmapping[b] == -1)
				continue;

			Matrix43 lmat = bind->GetNodeLocalM(0, bone2bindmapping[b]);
//			Matrix43 lmat = anim->GetNodeLocalM(nStartPhase, b);

			uint frame;

#if RTM_OPTIMIZE
			for(frame = nStartPhase; frame < nEndPhase; frame++)
			{
				//if their difference exceeds deltas, take this bone
				if(!CompareMatrix(lmat, anim->GetNodeLocalM(frame, b), parms.m_fRDelta, parms.m_fTDelta))
					break;
			}
#else
			frame = 0;
#endif
			if(frame < nEndPhase)
			{
				boneindices[rBones++] = b;
			}
		}

		//check absolute translation of hips and turn that to x/y/z/ step

		Matrix43 starthips;
		Matrix43 endhips;
		Matrix43 mat_zi;
		mat_zi.Identity();
		mat_zi._33=-1.0f;

		if(ihips != -1)
		{
			starthips = anim->GetNodeGlobalM(nStartPhase, ihips);
			starthips=mat_zi*starthips*mat_zi;
			endhips = anim->GetNodeGlobalM(nEndPhase, ihips);
			endhips=mat_zi*endhips*mat_zi;

			if(parms.m_bNeutralizeMovement)
			{
				x = (endhips._41 - starthips._41) * parms.m_fScaleX;
				y = (endhips._42 - starthips._42) * parms.m_fScaleY;
				z = (endhips._43 - starthips._43) * parms.m_fScaleZ;

        y = 0;
			}
		}

		m_pLogger->printf("\nOutputs RTM101 format file %s.\n\n", name);

		so.Write(Magic101, sizeof(Magic101));
		so.WriteFloat(x);
		so.WriteFloat(y);
		so.WriteFloat(z);

		if(parms.m_bNeutralizeMovement)
			m_pLogger->printf("Step offset %f %f %f\n", x, y, z);

		float speed = 1.0f / ((float)nPhases / parms.m_fFPS);

		char cname[256];
		char rtmname[256];

		strcpy(cname, strrchr(name, '\\') == NULL ? name : (strrchr(name, '\\') + 1));
		strcpy(rtmname, cname);

		if(strrchr(cname, '.'))
			*strrchr(cname, '.') = 0;

		m_pLogger->printf("\nclass %s\n{\n", cname);
		m_pLogger->printf("\tfile=%s;\n", name);
		m_pLogger->printf("\tspeed=%f;\n};\n\n", speed);

		if(logger)
		{
			logger->printf("\n%s - %f, frames %d\n", rtmname, speed, nPhases);
		}

		so.WriteUInt32(nPhases);
		so.WriteUInt32(rBones);

		m_pLogger->printf("Phases %d\n", nPhases);
		m_pLogger->printf("Bones %d\n", rBones);

		for(uint bone = 0; bone < rBones; bone++)
		{
			char bname[64];
			memset(bname, 0, 32);
			strcpy(bname, anim->GetNodeName(boneindices[bone]));
			so.Write(bname, 32);
		}

		//Build bind-pose
		Matrix43* fframe = (Matrix43*)_alloca(sizeof(Matrix43) * rBones);

		Vector3 tpose[256][4];
		assert(rBones<256);
		for(uint bone = 0; bone < rBones; bone++)
		{
			Matrix43 mat;

			if(bindpose)
			{
				mat = bindpose->GetNodeGlobalM(0, bone2bindmapping[boneindices[bone]]);
				mat=mat_zi*mat*mat_zi;
			}
			else
			{
				mat = anim->GetNodeGlobalM(0, boneindices[bone]);
				mat=mat_zi*mat*mat_zi;
			}

			fframe[bone] = mat.GetInverse();

			FourRefPoints(mat, &tpose[bone][0]);
      //int coord[4];
      //FindCoordSpace(coord, tpose[bone]);
		}

		//store frames
		for(uint frame = nStartPhase; frame <= nEndPhase; frame++)
		{
			char bname[64];

			//stored time as normalized 0...1 range
			float ftime = (double)(frame - nStartPhase) / (double)(nPhases - 1);

			if(frame == nEndPhase)
				ftime = 1.0f; //avoid tiny inaccuracies

			so.WriteFloat(ftime);

			for(uint bone = 0; bone < rBones; bone++)
			{
				memset(bname, 0, 32);
				strcpy(bname, anim->GetNodeName(boneindices[bone]));
				so.Write(bname, 32);

				Matrix43 mat = anim->GetNodeGlobalM(frame, boneindices[bone]);
				mat=mat_zi*mat*mat_zi;

				//TODO: when the Z-mirror bug disappear, we can replace this code by straightforward matrix way.
				//I have no idea why the mirror hack don't work directly with matrices :-(

#if 0
				Vector3 refp[4];
				FourRefPoints(mat, refp);
				mat = CalcTransform(tpose[bone], refp);
#else
				mat = mat * fframe[bone];
#endif

				Vector3 pos = mat[3];

				//fly through frames and put them onto linear movement
				if(ihips != -1)
				{
					if(parms.m_bNeutralizeMovement)
					{
            Vector3 lpos = starthips[3].Lerp(endhips[3], (float)frame / (float)(nPhases - 1));
						pos -= Vector3(lpos.x, 0, lpos.z);
					}
				}
				pos.x *= parms.m_fScaleX;
				pos.y *= parms.m_fScaleY;
				pos.z *= parms.m_fScaleZ;
				mat.SetRow(3, pos);
 
				so.WriteFloat(mat._11); so.WriteFloat(mat._12); so.WriteFloat(mat._13);
				so.WriteFloat(mat._21); so.WriteFloat(mat._22); so.WriteFloat(mat._23);
				so.WriteFloat(mat._31); so.WriteFloat(mat._32); so.WriteFloat(mat._33);
				so.WriteFloat(mat._41); so.WriteFloat(mat._42); so.WriteFloat(mat._43);
			}
		}
		return true;
	}
	CRTM101File(CLogger* logger): CAnimExporter(logger)
	{
	}
};

const char CRTM101File::Magic101[] = {'R', 'T', 'M', '_', '0', '1', '0', '1'};

class CFileLogger: public CLogger
{
protected:
	FILE*	m_pFile;

public:
	virtual void printf(const char* str, ...)
	{
		va_list argptr;

		va_start (argptr, str);
		vfprintf(m_pFile, str, argptr);
		va_end (argptr);
	}
	
	CFileLogger& operator <<(const char* str)
	{
		fputs(str, m_pFile);
		return *this;
	}

	CFileLogger(FILE* f): m_pFile(f) {assert(f);}
	~CFileLogger()
	{
		if(m_pFile != stdout)
			fclose(m_pFile);
	}
};

//---------------------------------------------------------------------------
void LoadSkeletonFromConfig(CAnimation& anim, const TiXmlDocument& doc)
{
}

//---------------------------------------------------------------------------
void r_LoadMappingFromConfig(CfgNodeFilter& filter, const TiXmlElement* parent)
{
	if(parent == NULL)
		return;

	for(const TiXmlElement* bone = parent; bone; bone = bone->NextSiblingElement("bone"))
	{
		const char* name = bone->Attribute("name");
		const char* fbxname = bone->Attribute("fbxname");

		if(fbxname && name)
			filter.AddMapping(fbxname, name);
		else
		if(fbxname == NULL && name)
		{
			//no alternative name
			filter.AddMapping(name, name);
		}
		r_LoadMappingFromConfig(filter, bone->FirstChildElement("bone"));
	}
}

//---------------------------------------------------------------------------
bool LoadMappingFromConfig(CfgNodeFilter& filter, TiXmlDocument& doc, enf::CStr name)
{
	bool found=false;
	TiXmlHandle handle = TiXmlHandle(doc.FirstChildElement("skeletondefs")).FirstChildElement("skeletondef");

	for(const TiXmlElement* elem = handle.Element(); elem; elem = elem->NextSiblingElement("skeletondef"))
	{
		enf::CStr sname = elem->Attribute("name");
		
		if(sname.cmpi(name) == 0)
		{
			found=true;

			r_LoadMappingFromConfig(filter, elem->FirstChildElement("bone"));

			//append inherited skeletons
			const TiXmlElement* ielem = elem;
			while(ielem)
			{
				enf::CStr inherits = ielem->Attribute("inherits");
				if(inherits.IsEmpty())
					break;

				for(ielem = handle.Element(); elem; elem = elem->NextSiblingElement("skeletondef"))
				{
					if(inherits.cmpi(ielem->Attribute("name")) == 0)
					{
						r_LoadMappingFromConfig(filter, ielem->FirstChildElement("bone"));
						break;
					}
				}
			}
		}
	}
	return found;
}

//---------------------------------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{
	int n;
	
	//defaults
	DString skeleton = "ManSkeleton";
	DString stepbone = "Hips";
	bool removestep = false;
	float fps = 30;
	float scale = 0.01f; //FBX has centimeters by default. We need meters
	DString outname;
	DString logname;
	DString cfgname;
	DString bindpose;

  int from = -1, to = -1;

	for(n = 1; n < argc; n++)
	{
		if(argv[n][0] != '-')
			break;

		if(!_stricmp(argv[n] + 1, "cfg"))
		{
			n++;
			cfgname = argv[n];
		}
		else
		if(!_stricmp(argv[n] + 1, "skeleton"))
		{
			n++;
			skeleton = argv[n];
		}
		else
		if(!_stricmp(argv[n] + 1, "step"))
		{
			n++;
			stepbone = argv[n];
		}
		else
		if(!_stricmp(argv[n] + 1, "removestep"))
		{
			removestep = true;
		}
		else
		if(!_stricmp(argv[n] + 1, "fps"))
		{
			n++;
			fps = ::atof(argv[n]);
		}
		else
		if(!_stricmp(argv[n] + 1, "scale"))
		{
			n++;
			scale = ::atof(argv[n]);
		}
    else
    if(!_stricmp(argv[n] + 1, "bindpose"))
    {
      n++;
      bindpose = argv[n];
    }
    else
    if(!_stricmp(argv[n] + 1, "range"))
    {
      n++;
      from = atol(argv[n]);
      n++;
      to = atol(argv[n]);
    }
		else
		if(!_stricmp(argv[n] + 1, "output"))
		{
			n++;
			outname = argv[n];
		}
		else
		if(!_stricmp(argv[n] + 1, "log"))
		{
			n++;
			logname = argv[n];
		}
		else
		{
			printf("bad option '%s'\n", argv[n]);
			n = argc;
			break;
		}
	}

	if(n == argc || argc <= 1)
	{
		ShowHelp();
		//getchar();
		return -1;
	}

	//output-name not supplied. Construct it from source file name
	if(outname.empty())
	{
		outname = argv[n];
		size_t pos = outname.find('.');
		if(pos != -1)
		{
			DString out;

			out.append(outname.data(), pos);
			outname = out;
		}
		outname.append(".rtm", 4);
	}

	FILE* log = NULL;

	if(!logname.empty())
	{
		log = fopen(logname.c_str(), "a+t");
	}

	if(log == NULL)
		log = stdout;

	//logging to stdout
	CFileLogger logger(log);

	FILE* speedlog = fopen("speeds.log", "a+t");
	CFileLogger speedlogger(speedlog);


	logger.printf("========== FBX2RTM ===========\n");
	logger.printf("Command line: ");
	for(int p = 0; p < argc; p++)
	{
		logger.printf("%s ", argv[p]);
	}
	logger.printf("\n");

	//Initialize and load FBX scene
	CFbxManager fbxmng(&logger);
	CFbxScene* scene = fbxmng.LoadScene(argv[n], NULL);

	if(scene == NULL)
	{
		logger.printf("Cannot load scene %s\n", argv[n]);
		return -1;
	}

	//Initialize and fill CAnimation with data
	CAnimation anim(&logger);

	CfgNodeFilter filter;

	if(!cfgname.empty() && !skeleton.empty())
	{
		TiXmlDocument doc;

		if(doc.LoadFile(cfgname.c_str()))
		{
			//LoadSkeletonFromConfig(anim, doc);
			if (!LoadMappingFromConfig(filter, doc, skeleton.c_str()))
				logger.printf("Could not find '%s' skeleton in '%s' config file.\n",skeleton.c_str(),cfgname.c_str());
		}
		else
			logger.printf("Error loading '%s' config file.\n",cfgname.c_str());
	}
	else
	{
		if (!skeleton.empty() && cfgname.empty())
			logger.printf("Specified config file, but no skeleton name. Config file ignored.\n");
		if (skeleton.empty() && !cfgname.empty())
			logger.printf("Specified skeleton name, but no config file. Skeleton ignored.\n");
	}
	
	//export FBX scene to CAnimation
	scene->Export(&anim, filter, fps, from, to);

	//Cleanup FBX scene
	if(scene)
	{
		delete scene;
		scene = NULL;
	}

	//Initialize and export to RTM format
	CRTM101File rtmfile(&logger);
	SExportParms parms;

	parms.m_fScaleX = scale;
	parms.m_fScaleY = scale;
	parms.m_fScaleZ = scale;
	parms.m_fRDelta = ENF_DEG2RAD(1.5f);
	parms.m_fTDelta = 0.0015f;
	parms.m_fScaleY = scale;
	parms.m_strCenterBone = stepbone;
	parms.m_bNeutralizeMovement = removestep;
	parms.m_fFPS = fps;

	if(!bindpose.empty())
	{
		CFbxScene* bindscene = fbxmng.LoadScene(bindpose.c_str(), NULL, false);
		if(bindscene == NULL)
		{
			logger.printf("Cannot load bindpose scene %s\n", bindpose.c_str());
			return -1;
		}
		CAnimation bindanm(&logger);
		//export FBX scene to CAnimation
		bindscene->Export(&bindanm, filter, 1, 0, 1);
		//Cleanup FBX scene
		if(bindscene)
		{
			delete bindscene;
			bindscene = NULL;
		}

    if(bindanm.GetNumFrames() == 0 || bindanm.GetNumNodes() == 0)
    {
      logger.printf("Export Failed! Bind-pose is empty!\n");
    }
    else
    {
		  if(!rtmfile.Export(outname.c_str(), &anim, &bindanm, parms, &speedlogger))
			  logger.printf("Export Failed! Cannot write to %s\n", outname.c_str());
		  else
			  logger.printf("Export OK!\n");
    }
	}
	else
	{
		if(!rtmfile.Export(outname.c_str(), &anim, NULL, parms, &speedlogger))
			logger.printf("Export Failed! Cannot write to %s\n", outname.c_str());
		else
			logger.printf("Export OK!\n");
	}
	return 0;
}

