#pragma once

#include <stdio.h>

//! Main implementation of the refcount
//! We're going to use intrusive reference counting technique
class RefCount
{
   int _count;
public:
   RefCount();
   virtual ~RefCount();
   
   void AddRef();
   void SubRef();
};

template<class TYPE>
class SmartRef
{
  TYPE *_type;

  void AddRef()
  {
    if (_type) _type->AddRef();
  }
  void SubRef()
  {
    if (_type) _type->SubRef();
  }

public:
  SmartRef():_type(0)
  {
  }

  SmartRef(TYPE *type):_type(type)
  {
    AddRef();
  }

  // Copy constructor will never be called
  // With our internal state being different!
  SmartRef(TYPE &other)
  {
    _type = &other;
    AddRef();
  }

  // Copy constructor will never be called
  // With our internal state being different!
  SmartRef(const SmartRef &ref)
  {
    _type = ref.GetRef();
    AddRef();
  }

  virtual ~SmartRef()
  {
    SubRef();
  }

  const SmartRef<TYPE> &operator=(const SmartRef& ref) 
  {
    TYPE *temp = ref.GetRef();
    if (temp) temp->AddRef();
    SubRef();
    _type = temp;


    return *this;
  }

  TYPE *GetRef() const
  {
    return _type;
  }

  TYPE *operator->()
  {
    return _type;
  }

  TYPE *operator&()
  {
    return _type;
  }

  operator TYPE *() const
  {
    return _type;
  }

  operator bool() const
  {
    return _type != NULL;
  }

  // Seems if both SmartRef point to the same object.
  bool operator ==(const SmartRef& ref)
  {
    if (_type == ref.GetRef()) return true;
    return false;
  }
};


class WeakLink;
class SoftLink : public RefCount
{
public:
  // object pointed too.
  WeakLink *_ptr;

  SoftLink(WeakLink *ptr)
  {
    _ptr = ptr;  
  }
};

// Object Inherits WeakLink, this enables 
// WeakRef to hold a weak reference to it
class WeakLink
{
  // Contain a reference count to the tracker
  SmartRef<SoftLink> _track;
public:
  WeakLink()  
  {
  }

  virtual ~WeakLink()
  {
    // Destroy the pointer, pointing to us
    // we're no longer in memory
    if (_track) _track->_ptr = NULL;
  }

  SoftLink *GetSoftTracker()
  {
    if (!_track) _track = new SoftLink(this);
    return _track;
  }
};


template<class TYPE>
class WeakRef
{
  SmartRef<SoftLink> _track;

public:
  WeakRef()
  {
  }

  // Constructor, get the types soft link
  WeakRef(TYPE *type)
  {
    if (type) _track = type->GetSoftTracker();
  }

  // Copy constructor will never be called
  // With our internal state being different!
  WeakRef(TYPE &other)
  {
    _track = other._track;
  }

  // Copy constructor will never be called
  // With our internal state being different!
  WeakRef(const WeakRef& weakRef)
  {
    _track = weakRef._track;
  }

  virtual ~WeakRef(){}

  const WeakRef<TYPE> &operator=(const WeakRef& ref) 
  {
    _track = ref._track;
     return *this;
  }

  TYPE *GetRef() const
  {
    if (_track) return (TYPE*) _track.GetRef()->_ptr;
    return NULL;
  }

  TYPE *operator->()
  {
    return GetRef();
  }

  TYPE *operator&()
  {
    return GetRef();
  }

  operator TYPE *() const
  {
    return GetRef();
  }

  operator bool() const
  {
    return GetRef() != NULL;
  }

  // Seems if both SmartRef point to the same object.
  bool operator ==(const WeakRef& ref)
  {
    if (GetRef() == ref.GetRef()) return true;
    return false;
  }
};
