#ifndef _MARINA_PROJECT 
#include <Poseidon/lib/wpch.hpp>
#else
#define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES

//#include "marina.hpp"
//#include "inc.h"
#include "RigidBody.h"


void IRigidBody::AddForceAtPointToBanks(const Vector3& cForce, const Vector3& cPoint)
{
	_forceBank += cForce;
	_torqueBank += CalculateTorgue(cForce,cPoint);
  Assert(_forceBank.IsFinite());
  Assert(_torqueBank.IsFinite());
}

/*void IRigidBody::AddCorrectionForceAtPointToBanks(const Vector3& cForce, const Vector3& cPoint)
{
	_cCorrectionForceBank += cForce;
	_cCorrectionTorgueBank += CalculateTorgue(cForce,cPoint);
}*/


void IRigidBody::PrepareIntegration()
{
	_posBackUp = GetMassCenter();
	_orientationBackUp = GetOrientation();

	_velocityBackUp = GetVelocity();
	_angularMomentumBackUp = GetAngularMomentum();

	_acceleration = _forceBank * GetInvMass();
	_torque = _torqueBank;	

	_orientationVelocity = QuaternionRB(GetAngularVelocity()) * GetOrientation() * 0.5f;
		
	_orientationAcceleration = QuaternionRB(GetActualInvInertiaTensor() * (_torqueBank - (GetAngularVelocity().CrossProduct(GetAngularMomentum())))) * GetOrientation() * 0.5f;
	_orientationAcceleration += GetOrientation() * (GetAngularVelocity().SquareSize() / 4.0f);		
}

/*bool IRigidBody::IsMoving() const
{
    return !IsNumericalZero(GetVelocity().Size()/10000.0f) || !IsNumericalZero(_acceleration.Size()/10000.0f) || 
        !IsNumericalZero(_orientationVelocity.Size()/10000.0f) || !IsNumericalZero(_orientationAcceleration.Size()/10000.0f);
    // TODO: Some real conditions
}*/

void IRigidBody::DoIntegration(rb_real fTime)
{
	SetMassCenter(_posBackUp + GetVelocity() * fTime
	+ _acceleration * fTime * fTime / 2);

  QuaternionRB orient = _orientationBackUp + _orientationVelocity * fTime;
	orient += _orientationAcceleration * (fTime * fTime / 2.0f);
	orient.Normalize();
  SetOrientationMassCenter(orient);

	SetVelocity(GetVelocity() + _acceleration * fTime);
	//MomentumFromVelocity();

	SetAngularMomentum(GetAngularMomentum() + _torque * fTime);
	ActualizeTensors();
	AngularVelocityFromAngularMomentum();
}

#endif
