#ifndef _MARINA_PROJECT 
#include <Poseidon/lib/wpch.hpp>
#else
#define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES


#include "marina.hpp"
#include "inc.h"
#include "RestContactResolver.hpp"
#include "Simulation.h"
#include "BodyGroup.h"
#include "BaseGeometry.h"

//marina_real SimulationRB::_fTolerance = TOLERANCE;
unsigned long g_iDebugID = 1;
unsigned long g_iStopID = 0;

void CollisionDetection(RBContactArray& cContactPoints);
BOOL IsIntersection();

void SimulationRB::ReleaseGroups(BOOL bWithFrozen = FALSE)
{
  if (!bWithFrozen)
  {    
    for( int i = 0; i < _cGroups.Size();)
    {
      if (!_cGroups[i]->IsFrozen())
      {
        for( int j = 0; j < _cGroups[i]->Bodies().Size(); j++)
        {
          _cGroups[i]->Bodies()[j]->_group = NULL;
        } 

        delete _cGroups[i];
        _cGroups.Delete(i);
      }
      else
        i++;
    }
  }
  else
  {
    for( int i = 0; i < _cGroups.Size();i++)
    {
      delete _cGroups[i];
    }

    for( int i = 0; i < _cBodies.Size();i++)
    {
      _cBodies[i]->_group = NULL;
    }

    _cGroups.Clear();
  }
}

BOOL SimulationRB::IsIntersection()
{
  PROFILE_SCOPE(marIs); 
  if (_cBodies.Size() <= 1)
    return FALSE;

  // Update geom.
  for( int i = 0; i < _cBodies.Size(); i++)
  {
     //PROFILE_SCOPE(Origin);
    if (!_cBodies[i]->_static && (_cBodies[i]->_group == NULL || !_cBodies[i]->_group->IsFrozen()))
    {    
      _cBodies[i]->_geometry->SetOrigin(_cBodies[i]->_physBody->GetPos(),
        _cBodies[i]->_physBody->GetOrientation());
      _cBodies[i]->_geometry->CalculateEnclosingRectangle();
    }
  }

  // Do  (very primitive algorythm)    
  for( int i = 0; i < _cBodies.Size(); i++)
  {
    bool staticOrFroze = _cBodies[i]->_static || (_cBodies[i]->_group != NULL &&_cBodies[i]->_group->IsFrozen());
    for( int j = i + 1; j < _cBodies.Size(); j++)
    {
      if (!staticOrFroze || 
        ( !_cBodies[j]->_static && (_cBodies[j]->_group == NULL || !_cBodies[j]->_group->IsFrozen())))                
      {
        if (_cBodies[i]->_geometry->IsIntersection(*_cBodies[j]->_geometry, _simCycle))
          return TRUE;
      }
    }        
  }

  return FALSE;
};

void SimulationRB::ForceInterFree()
{  
  if (_cBodies.Size() <= 1)
    return ;
  
  // Do  (very primitive algorythm)    
  for( int i = 0; i < _cBodies.Size(); i++)
  {
    bool staticOrFroze = _cBodies[i]->_static || (_cBodies[i]->_group != NULL &&_cBodies[i]->_group->IsFrozen());
    for( int j = i + 1; j < _cBodies.Size(); j++)
    {
      if (!staticOrFroze || 
        ( !_cBodies[j]->_static && (_cBodies[j]->_group == NULL || !_cBodies[j]->_group->IsFrozen())))                
      {
       _cBodies[i]->_geometry->ForceInterFree(*_cBodies[j]->_geometry, _simCycle);          
      }
    }        
  }

  return;
}

void SimulationRB::CollisionDetection(RBContactArray& cContacts)
{
  PROFILE_SCOPE(Coll); 
  // Update geom.
  for( int i = 0; i < _cBodies.Size(); i++)
  {
       
    if (!_cBodies[i]->_static&& (_cBodies[i]->_group == NULL || !_cBodies[i]->_group->IsFrozen()))
    {    
      _cBodies[i]->_geometry->SetOrigin(_cBodies[i]->_physBody->GetPos(),
        _cBodies[i]->_physBody->GetOrientation());
      _cBodies[i]->_geometry->CalculateEnclosingRectangle();
    }
  }

  // Do CD (very primitive algorythm)
  for( int i = 0; i < _cBodies.Size(); i++)
  {
    bool staticOrFroze = _cBodies[i]->_static || (_cBodies[i]->_group != NULL &&_cBodies[i]->_group->IsFrozen());
    for( int j = i + 1; j < _cBodies.Size(); j++)
    {
      
      if (!staticOrFroze || 
        ( !_cBodies[j]->_static && (_cBodies[j]->_group == NULL || !_cBodies[j]->_group->IsFrozen())))                
      {
       _cBodies[i]->_geometry->FindContacts(cContacts, *_cBodies[j]->_geometry, _simCycle);        
      }
    }        
  }
}

void SimulationRB::CollisionDetection()
{
  //PROFILE_SCOPE(Coll);
  RBContactArray cContacts;

  ReleaseGroups(FALSE);

  CollisionDetection(cContacts);

  unsigned int nNumberOfUnCollision = cContacts.Size();
 
  for(unsigned int iCollisionID = 0; iCollisionID < nNumberOfUnCollision; iCollisionID++)
  {
    // Get bodies groups			
    const RBContactPoint& tCollision = cContacts[iCollisionID];

    BodyGroup * pcGroup1 = NULL;
    if (!tCollision.pcBody1->_static)
      pcGroup1 = tCollision.pcBody1->_group;

    //	if (tCollision.pcBody1->GetInvMass() == 0.0f)
    //		pcGroup1 = 0;

    BodyGroup * pcGroup2 = NULL;
    if (!tCollision.pcBody2->_static)
      pcGroup2 = tCollision.pcBody2->_group;

    //	if (tCollision.pcBody2->GetInvMass() == 0.0f)
    //		dwGroup2 = 0;

    if (pcGroup1 == pcGroup2)
    {	// same group , just add tCollision
      if (pcGroup1 != NULL)
        pcGroup1->AddContact(tCollision);				
      else //Create new group
      {
        BodyGroup * pcGroup = new BodyGroup;
        _cGroups.Add(pcGroup);								
        pcGroup->AddContact(tCollision);
      }
    }		
    else
    {
      if (pcGroup1 == NULL)
      { // add into group2
        pcGroup2->AddContact(tCollision);							
      }
      else
        if (pcGroup2 == NULL)
        {    // add into group1
          pcGroup1->AddContact(tCollision);					
        }
        else
        { // join groups
          pcGroup1->JoinGroup(*pcGroup2);
          pcGroup1->AddContact(tCollision);					

          // find group ID;
           int i = 0;
          for(; i < _cGroups.Size(); i++)
          {
            if (_cGroups[i] == pcGroup2)
              break;
          }
          ASSERT(i < _cGroups.Size());

          _cGroups.Delete(i);

          delete pcGroup2;					
        }
    }
  }

  //dsPrint("Coll: %d, Groups: %d\n",nNumberOfUnCollision, _cGroups.Size());
  //for(int i = 0; i < _cGroups.Size(); i++)
  //{
  //     dsPrint("Group: %d, Coll: %d, Bodies: %d\n",i, _cGroups[i]->Contacts().Size(),  _cGroups[i]->Bodies().Size());
  //}

}

#if _ADD_ARROW_ENABLED

SimulationRB * g_Sim = NULL;
void AddArrow(Vector3Val pos,Vector3Val dir, float size, DWORD color)
{
  if (g_Sim)
    g_Sim->AddArrow( pos, dir,  size, color);
}

void ResetArrows()
{
  if (g_Sim)
    g_Sim->ResetArrows();
}
#endif


void SimulationRB::Simulate(marina_real fTime)
{
#if _ADD_ARROW_ENABLED
  g_Sim = this;
#endif

  PROFILE_SCOPE(marina);
  if (_cBodies.Size() == 0)
    return;

  marina_real fRest = fTime;

#if _ENABLE_BREAKABLE
  int breaksInRow = 0;
#endif

  while (fRest > 0.0f) 
  {
    _simCycle ++;
    //DebugPoint();
    marina_real fSimTime = floatMin(fRest, 0.02f);

    for( int i = 0; i < _cBodies.Size(); i++)
    {
      if (!_cBodies[i]->_static)
      {      
        _cBodies[i]->_physBody->ZeroBanks();
        if (_cBodies[i]->_physBody->GetInvMass() != 0.0f)
          _cBodies[i]->_physBody->AddForceToBanks( _externalAccel /_cBodies[i]->_physBody->GetInvMass());

        if (!_cBodies[i]->_physBody->GetSyncStatus())
        {
          if (_cBodies[i]->_group != NULL)
            _cBodies[i]->_group->Melt();

          _cBodies[i]->_physBody->ResetSyncStatus(); 
        }
      }
    }

#if _ENABLE_BREAKABLE
    AutoArray<SeparateGeom> geomSeparate;
    bool breakBodies = false;
#endif
      
    CollisionDetection();

    //LogF("bodies: %d", _cBodies.Size());
      
    if (_cGroups.Size() > 0)
    {
      for( int iGroup = 0; iGroup < _cGroups.Size(); iGroup++)
      {   
        if (!_cGroups[iGroup]->IsFrozen())
        {
          PROFILE_SCOPE_MARINA(phys);
#if _ENABLE_BREAKABLE
          BreakableImpulseContactResolver cImpulseResolver(*_cGroups[iGroup]);
          cImpulseResolver.Init(fSimTime);
          cImpulseResolver.Resolve();

          geomSeparate = cImpulseResolver.GetSeparateGeom();           

          if (!CanBreakBodies(geomSeparate) || breaksInRow > 0 || _cGroups[iGroup]->Bodies().Size() > 4)
          {                  
            cImpulseResolver.ApplyCalculatedForcesFin(1.0f);
            RestContactResolver cResolver( *_cGroups[iGroup]);
            cResolver.Init(fSimTime);
            cResolver.Resolve();
          }
          else
          {
            breaksInRow++;
            breakBodies = true;
            cImpulseResolver.ApplyCalculatedForcesFin(0.95f);
            BreakBodies(geomSeparate);
          }

#else
          {
            PROFILE_SCOPE_MARINA(impulseresolve);
            ImpulseContactResolver cImpulseResolver(*_cGroups[iGroup]);
            cImpulseResolver.Init(fSimTime);
            cImpulseResolver.Resolve();
          }
          
          {
            PROFILE_SCOPE_MARINA(forceresolve);
            RestContactResolver cResolver( *_cGroups[iGroup]);
            cResolver.Init(fSimTime);
            cResolver.Resolve();          
          }
#endif
        }
      }
    }    
#if _ENABLE_BREAKABLE
    if (!breakBodies)    
    {
      Integrate(fSimTime);
      fRest -= fSimTime;
      //LogF("Integrated, fSimTime = %f", fSimTime);
    }
#else
    
    Integrate(fSimTime);
    fRest -= fSimTime;
    //LogF("Integrated, fSimTime = %f", fSimTime);
#endif
  }
}

int SimulationRB::SimulateOneStep(marina_real fTime)
{
#if _ADD_ARROW_ENABLED
  g_Sim = this;
#endif

  PROFILE_SCOPE(marina);

  switch (_phase)
  {
  case 0:
    {    
      if (_cBodies.Size() == 0)
        return MARINA_OK;
     
      _simCycle ++;        

      for( int i = 0; i < _cBodies.Size(); i++)
      {
        if (!_cBodies[i]->_static)
        {      
          _cBodies[i]->_physBody->ZeroBanks();
          if (_cBodies[i]->_physBody->GetInvMass() != 0.0f)
            _cBodies[i]->_physBody->AddForceToBanks( _externalAccel /_cBodies[i]->_physBody->GetInvMass());
        }
      }

      CollisionDetection();

      if (_cGroups.Size() == 0)
        return MARINA_OK;
      _iGroup = 0;

      _phase = 1;
    }

  case 1:
    {
      // find unfrozen group 
      

      for( ; _iGroup < _cGroups.Size() && _cGroups[_iGroup]->IsFrozen(); _iGroup++);
      if (_iGroup == _cGroups.Size())
      {
        _phase = 5;
        return MARINA_NEXT;
      }

      PROFILE_SCOPE_MARINA(phys);
      
      PROFILE_SCOPE_MARINA(impulseresolve);
      _pcImpulseResolver = new ImpulseContactResolver(*_cGroups[_iGroup]);
      _pcImpulseResolver->Init(fTime);
      _phase = 2;
      
    }
  case 2:
    {
      PROFILE_SCOPE_MARINA(phys);
      PROFILE_SCOPE_MARINA(impulseresolve);
      if (MARINA_NEXT != _pcImpulseResolver->ResolveOneStep())
      {
        _phase = 3;
        delete _pcImpulseResolver;
        _pcImpulseResolver = NULL;

        return MARINA_NEXT; 
      }  

      return MARINA_NEXT; 
    }
  case 3:
    {
      PROFILE_SCOPE_MARINA(phys);
      PROFILE_SCOPE_MARINA(forceresolve);
         
      _pcRestResolver = new RestContactResolver( *_cGroups[_iGroup]);
      _pcRestResolver->Init(fTime);      
      _phase = 4;      
    }    
  case 4:
    {
      PROFILE_SCOPE_MARINA(phys);
      PROFILE_SCOPE_MARINA(forceresolve);
      if (MARINA_NEXT != _pcRestResolver->ResolveOneStep())
      {
        _phase = 1; // return to group 
        _iGroup++;
        delete _pcRestResolver;
        _pcRestResolver = NULL;
        return MARINA_NEXT; 
      } 

      return MARINA_NEXT; 
    }
  case 5:
    {
      // end
      Integrate(fTime);
      _phase = 0;
      return MARINA_OK;
    }
  default:
    {
      _phase = 0;
      return MARINA_FAILURE;
    }
  }
}

void SimulationRB::Integrate(marina_real& fTime)
{
  PROFILE_SCOPE_MARINA(marIt);
  for( int i = 0; i < _cGroups.Size(); i++)
  {
    _cGroups[i]->Temp() = TRUE;  // Temp means should be frozen???
  }

  for( int i = 0; i < _cBodies.Size(); i++)
  {
    if (!_cBodies[i]->_static && (_cBodies[i]->_group == NULL || !_cBodies[i]->_group->IsFrozen()))
    {        
     _cBodies[i]->_physBody->PrepareIntegration();
      if (_cBodies[i]->_group != NULL &&_cBodies[i]->_physBody->IsMoving())
       _cBodies[i]->_group->Temp() = FALSE;
    }
  }

  for( int i = 0; i < _cGroups.Size(); i++)
  {
#ifndef _MARINA_PROJECT
    if (_cGroups[i]->Temp() && !_cGroups[i]->IsFrozen())
      _cGroups[i]->Froze(); //TODO Frozing is switched off
#endif
  }


  BOOL bRecalculate;
  int iter = 0;
  do 
  {
    bRecalculate = FALSE;
    for( int i = 0; i < _cBodies.Size(); i++)
    {
      //PROFILE_SCOPE(Integ);
      if (!_cBodies[i]->_static && (_cBodies[i]->_group == NULL || !_cBodies[i]->_group->IsFrozen()))
       _cBodies[i]->_physBody->DoIntegration(fTime);
    }

    //RBContactArray cContactPoints;

    if (IsIntersection())
    {
      //dsPrint("Intersection: %f\n", fTime);     

      if (++iter >= 3)     
      {
        // force itersection free situation
        ForceInterFree();
        //dsPrint("ForceInterFree: %f\n", fTime);

      }
      else
      {
        fTime /= 2;
        bRecalculate = TRUE;
      }

      /*if (fTime < 0.0001f)
      {
        fTime = 0;
        bRecalculate = FALSE;
      }*/
    }

  } while(bRecalculate);
}

void SimulationRB::Reset()
{
  ReleaseGroups(TRUE);
  _cBodies.Clear();
  _externalAccel = VZero;
  _simCycle = 0;
}


#endif