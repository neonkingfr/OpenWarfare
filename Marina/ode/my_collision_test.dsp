# Microsoft Developer Studio Project File - Name="my_collision_test" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=my_collision_test - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "my_collision_test.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "my_collision_test.mak" CFG="my_collision_test - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "my_collision_test - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "my_collision_test - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Marina/ode-0.03/ode", DTKAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "my_collision_test - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "my_collision_test___Win32_Release"
# PROP BASE Intermediate_Dir "my_collision_test___Win32_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "test"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /O2 /I "..\include" /I "marina" /I "marina\mtl-2.1.2-20" /D "WIN32" /D "MSVC" /FR /GF /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Comctl32.lib kernel32.lib user32.lib gdi32.lib OpenGL32.lib Glu32.lib ode.lib drawstuff.lib /nologo /machine:IX86 /out:"test/my_collision_testR.exe" /libpath:"../lib"

!ELSEIF  "$(CFG)" == "my_collision_test - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "my_collision_test___Win32_Debug"
# PROP BASE Intermediate_Dir "my_collision_test___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "test"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\include" /I "marina" /I "marina\mtl-2.1.2-20" /D "WIN32" /D "_DEBUG" /D "_MSVC" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Comctl32.lib kernel32.lib user32.lib gdi32.lib OpenGL32.lib Glu32.lib odeD.lib drawstuffD.lib /nologo /subsystem:console /debug /machine:I386 /out:"test/my_collision_testD.exe" /pdbtype:sept /libpath:"..\lib"

!ENDIF 

# Begin Target

# Name "my_collision_test - Win32 Release"
# Name "my_collision_test - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\test\my_collision_test.cpp

!IF  "$(CFG)" == "my_collision_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_collision_test - Win32 Debug"

!ENDIF 

# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=..\drawstuff\src\resources.rc
# End Source File
# End Group
# Begin Group "marina"

# PROP Default_Filter ""
# Begin Group "src"

# PROP Default_Filter "cpp"
# Begin Source File

SOURCE=.\marina\BoxCollision.cpp

!IF  "$(CFG)" == "my_collision_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_collision_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\mtl_lu_solve.cpp

!IF  "$(CFG)" == "my_collision_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_collision_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\RestContactResolver.cpp

!IF  "$(CFG)" == "my_collision_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_collision_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\RigidBody.cpp

!IF  "$(CFG)" == "my_collision_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_collision_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\RigidBodyState.cpp

!IF  "$(CFG)" == "my_collision_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_collision_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\Simulation.cpp

!IF  "$(CFG)" == "my_collision_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_collision_test - Win32 Debug"

!ENDIF 

# End Source File
# End Group
# Begin Group "inc"

# PROP Default_Filter "h,hpp,inl"
# Begin Source File

SOURCE=.\marina\BoxCollision.h
# End Source File
# Begin Source File

SOURCE=.\marina\marina.hpp
# End Source File
# Begin Source File

SOURCE=.\marina\Math2D.h
# End Source File
# Begin Source File

SOURCE=.\marina\Math2D.inl
# End Source File
# Begin Source File

SOURCE=.\marina\Math3D.h
# End Source File
# Begin Source File

SOURCE=.\marina\Math3D.inl
# End Source File
# Begin Source File

SOURCE=.\marina\MatrixA.hpp
# End Source File
# Begin Source File

SOURCE=.\marina\MyArray.hpp
# End Source File
# Begin Source File

SOURCE=.\marina\Quaternion.h
# End Source File
# Begin Source File

SOURCE=.\marina\Quaternion.inl
# End Source File
# Begin Source File

SOURCE=.\marina\RestContactResolver.hpp
# End Source File
# Begin Source File

SOURCE=.\marina\RigidBody.h
# End Source File
# Begin Source File

SOURCE=.\marina\RigidBodyState.h
# End Source File
# Begin Source File

SOURCE=.\marina\Simulation.h
# End Source File
# End Group
# End Group
# End Target
# End Project
