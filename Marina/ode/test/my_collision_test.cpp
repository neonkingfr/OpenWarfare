/*************************************************************************
 *                                                                       *
 * Open Dynamics Engine, Copyright (C) 2001 Russell L. Smith.            *
 *   Email: russ@q12.org   Web: www.q12.org                              *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of the GNU Lesser General Public            *
 * License as published by the Free Software Foundation; either          *
 * version 2.1 of the License, or (at your option) any later version.    *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 * Lesser General Public License for more details.                       *
 *                                                                       *
 * You should have received a copy of the GNU Lesser General Public      *
 * License along with this library (see the file LICENSE.TXT); if not,   *
 * write to the Free Software Foundation, Inc., 59 Temple Place,         *
 * Suite 330, Boston, MA 02111-1307 USA.                                 *
 *                                                                       *
 *************************************************************************/

#include <windows.h>
#include <ode/ode.h>
#include <drawstuff/drawstuff.h>
#include <marina.hpp>
#include <RestContactResolver.hpp>
#include <Simulation.h>
#include <BoxCollision.h>

#ifdef MSVC
#pragma warning(disable:4244 4305)  // for VC++, no precision loss complaints
#endif

// select correct drawing functions

#ifdef dDOUBLE
#define dsDrawBox dsDrawBoxD
#define dsDrawSphere dsDrawSphereD
#define dsDrawCylinder dsDrawCylinderD
#define dsDrawCappedCylinder dsDrawCappedCylinderD
#endif


// some constants

#define NUM 20			// max number of objects
#define DENSITY (5.0)		// density of all objects
#define GPB 3			// maximum number of geometries per body
#define FRICTION 0.6


// dynamics and collision objects

struct MyObject {
  dBodyID body;			// the body
  dGeomID geom[GPB];		// geometries representing this body
  CRigidBodyBase my_body;
};

static int num=0;		// number of objects in simulation
static int nextobj=0;		// next object to recycle if num==NUM
static dWorldID world;
static dSpaceID space;
static MyObject obj[NUM];
static dJointGroupID contactgroup;
static int selected = -1;	// selected object
static M_BodyArray cBodies;
static g_bIsIntersection;
// this is called by dSpaceCollide when two objects in space are
// potentially colliding.

static void nearCallback (void *data, dGeomID o1, dGeomID o2)
{
  int i;
  // if (o1->body && o2->body) return;

  // exit without doing anything if the two bodies are connected by a joint
  //dBodyID b1 = dGeomGetBody(o1);
  //dBodyID b2 = dGeomGetBody(o2);
  //if (b1 && b2 && dAreConnected (b1,b2)) return;

  dContactGeom contact[16];			// up to 16 contacts per box
  
  if (int numc = dCollide ( o1, o2, 16, contact, sizeof(dContactGeom))) 
  {
	dBodyID body1 = dGeomGetBody(o1);
	dBodyID body2 = dGeomGetBody(o2);

	CRigidBodyBase* pcBody1 = NULL;
	CRigidBodyBase* pcBody2 = NULL;

	for(unsigned int i = 0; i < num; i++)
	{
		if (obj[i].body == body1)
			pcBody1 = &(obj[i].my_body);

		if (obj[i].body == body2)
			pcBody2 = &(obj[i].my_body);
	}

	//ASSERT(pcBody1 != NULL && pcBody2 != NULL);

	for(unsigned int i = 0; i < numc; i++)
	{
		M_ContactPoint tContactPoint;
		tContactPoint.pcBody1 = pcBody1;
		tContactPoint.pcBody2 = pcBody2;
		tContactPoint.fRestitutionCoef = pcBody1->GetRestitutionCoef();
		tContactPoint.fSlidingFrictionCoef = pcBody1->GetSlidingFrictionCoef();
		tContactPoint.pcPedestal = NULL;


		tContactPoint.pos = CVector3D(contact[i].pos[0], contact[i].pos[1], contact[i].pos[2]);
		tContactPoint.under =  contact[i].depth;

		tContactPoint.dir[0] = CVector3D( contact[i].normal[0], contact[i].normal[1], contact[i].normal[2]);

		tContactPoint.dir[0].Normalize();
		
		// Find pedestal axes
		tContactPoint.dir[1] = tContactPoint.dir[0] % CVector3D(1,0,0);
		if (tContactPoint.dir[1].GetLength() < 0.1f) // is new vector correct
			tContactPoint.dir[1] = tContactPoint.dir[0] % CVector3D(0,1,0);

		tContactPoint.dir[2] = tContactPoint.dir[0] % tContactPoint.dir[1];

		tContactPoint.dir[1].Normalize();
		tContactPoint.dir[2].Normalize();


		((M_ContactArray *) data)->Add(tContactPoint);
		
	}
  }
}


// start simulation - set viewpoint

static void start()
{
  static float xyz[3] = {2.1640f,-1.3079f,1.7600f};
  static float hpr[3] = {125.5000f,-17.0000f,0.0000f};
  dsSetViewpoint (xyz,hpr);
  printf ("To drop another object, press:\n");
  printf ("   b for box.\n");
  printf ("   s for sphere.\n");
  printf ("   c for cylinder.\n");
  printf ("   x for a composite object.\n");
  printf ("To select an object, press space.\n");
  printf ("To disable the selected object, press d.\n");
  printf ("To enable the selected object, press e.\n");
}


char locase (char c)
{
  if (c >= 'A' && c <= 'Z') return c - ('a'-'A');
  else return c;
}

void DestroyAllBodies()
{
    for(int i = 0; i < num; i++)
    {
        dBodyDestroy (obj[i].body);
        for (int k=0; k < GPB; k++) 
		{
		    if (obj[i].geom[k]) dGeomDestroy (obj[i].geom[k]);
		}

    }
}

void CreateBodiesFromContactPoints(M_ContactArray& cArray)
{
    dMass m;;
    for(unsigned int i = 0; i < cArray.GetSize(); i++)
    {
        if (num >= NUM)
        {
            return;
        }
        
        obj[num].body = dBodyCreate(world);
        dBodySetPosition (obj[num].body, cArray[i].pos[0],
             cArray[i].pos[1], cArray[i].pos[2]);

        float radius = 0.1;
        obj[num].geom[0] = dCreateSphere (space,radius);

                
        dGeomSetBody (obj[num].geom[0],obj[num].body);
        dMassSetSphere(&m,DENSITY,radius);
        dBodySetMass (obj[num].body,&m);

        num++;
    }
}

float CalculateAngleBetweenVectors(CVector3D cVec1, CVector3D cVec2)
{
    cVec1.Normalize();
    cVec2.Normalize();

    float sinus = (cVec1 % cVec2).GetLength();
    float cosinus = cVec1 * cVec2;

    float angle = asinf(sinus); //betwen -pi/2 and pi/2
    ASSERT(angle >= 0.0f);

    if (cosinus < 0.0f)
        angle = 3.1415 - angle; // result is between 0 and pi

    return angle;
}

void CreateBodiesFromPointPairs(CPointPairArray& cArray)
{
    dMass m;;
    for(unsigned int i = 0; i < cArray.GetSize(); i++)
    {
        if (num >= NUM)
        {
            return;
        }
        
        obj[num].body = dBodyCreate(world);
        CVector3D cPos = cArray[i].tPoint1.cWordCoord + cArray[i].tPoint2.cWordCoord;
        cPos *= 0.5;

        dBodySetPosition (obj[num].body, cPos[0],
             cPos[1], cPos[2]);

        CVector3D cDiff = cArray[i].tPoint1.cWordCoord - cArray[i].tPoint2.cWordCoord;
        CVector3D cRotationAxe = CVector3D(1,0,0) % cDiff;
        cRotationAxe.Normalize();

        float fAngle = CalculateAngleBetweenVectors(CVector3D(1,0,0), cDiff);

        dQuaternion q;
        q[0] = cosf(fAngle/2);
        float fSinus = sinf(fAngle/2);
        q[1] = cRotationAxe[0] * fSinus;
        q[2] = cRotationAxe[1] * fSinus;
        q[3] = cRotationAxe[2] * fSinus;
        //dQFromAxisAndAngle(q,cRotationAxe[0],cRotationAxe[1],cRotationAxe[2],fAngle );

        dBodySetQuaternion(obj[num].body,q);

        float length = cDiff.GetLength();

        obj[num].geom[0] = dCreateBox (space, length, 0.025, 0.025);
                
        dGeomSetBody (obj[num].geom[0],obj[num].body);
        dMassSetBox(&m,DENSITY,length, 0.025, 0.025);
        dBodySetMass (obj[num].body,&m);

        num++;
    }
}
// called when a key pressed

static void command (int cmd)
{
    int i,j,k;
    dReal sides[3];
    dMass m;
    
    cmd = locase (cmd);
    if (cmd == 'b' || cmd == 's' || cmd == 'c' || cmd == 'x' ) 
    {
        
       	DestroyAllBodies();
					
        i = 0;
        num = 1;
        
        obj[i].body = dBodyCreate (world);
        
        //CVector3D cBox1(dRandReal()*0.5+0.1,dRandReal()*0.5+0.1,dRandReal()*0.5+0.1);
        //CVector3D cPos1(dRandReal()*2-1,dRandReal()*2-1,dRandReal()+1);
        
        CVector3D cBox1( 0.2, 0.2, 0.2);
        CVector3D cPos1(0,0,1);

        dBodySetPosition (obj[i].body,
            cPos1[0],cPos1[1],cPos1[2]);
        
        obj[i].my_body.SetPos(cPos1);
        
        dQuaternion q;
        //dQFromAxisAndAngle (q,dRandReal()*2.0-1.0,dRandReal()*2.0-1.0,
        //   dRandReal()*2.0-1.0,dRandReal()*10.0-5.0);
        q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;
        
        CQuaternion cOrientation1(q[0], q[1], q[2], q[3]);
        
        dBodySetQuaternion (obj[i].body,q);
        obj[i].my_body.SetOrientation(cOrientation1);
        
        dBodySetData (obj[i].body,(void*) i);
                
        dMassSetBox (&m,DENSITY,cBox1[0]*2,cBox1[1]*2,cBox1[2]*2);
        obj[i].geom[0] = dCreateBox (space,cBox1[0]*2,cBox1[1]*2,cBox1[2]*2);
                
        for (k=0; k < GPB; k++) {
            if (obj[i].geom[k]) dGeomSetBody (obj[i].geom[k],obj[i].body);
        }
        
        dBodySetMass (obj[i].body,&m);
        
        obj[i].my_body.SetInvMass(1.0/m.mass);
        CMatrix3DRow cInertiaTensor(CVector3D(m.I[0], m.I[1], m.I[2]),
            CVector3D(m.I[4], m.I[5], m.I[6]),
            CVector3D(m.I[8], m.I[9], m.I[10]));
        
        obj[i].my_body.SetInertiaTensor(cInertiaTensor);
        obj[i].my_body.SetInvInertiaTensor(cInertiaTensor.GetInverse().GetInRows());
        obj[i].my_body.SetRestitutionCoef(0.0);
        obj[i].my_body.SetSlidingFrictionCoef(FRICTION);
        obj[i].my_body.SetAngularVelocity(CVector3D(10,0,0));
        obj[i].my_body.ActualizeTensors();
        
        obj[i].my_body.AngularMomentumFromAngularVelocity();
        
        cBodies.Add(&(obj[i].my_body));          
        
        if (num < NUM) 
        {
            i = num;
            num++;
        }
        else 
        {
            return;
        }
        
        obj[i].body = dBodyCreate (world);
        
        //CVector3D cBox2(dRandReal()*0.5+0.1,dRandReal()*0.5+0.1,dRandReal()*0.5+0.1);
        //CVector3D cPos2(dRandReal()*2-1,dRandReal()*2-1,dRandReal()+1);

        CVector3D cBox2( 0.2, 0.2, 0.2);
        CVector3D cPos2(0.45,0,1);
        
        dBodySetPosition (obj[i].body,
            cPos2[0],cPos2[1],cPos2[2]);
        
        obj[i].my_body.SetPos(cPos2);
        
        //dQuaternion q;
        dQFromAxisAndAngle (q,dRandReal()*2.0-1.0 ,dRandReal()*2.0-1.0,
          dRandReal()*2.0-1.0,dRandReal()*10.0-5.0);
        //dQFromAxisAndAngle (q, 1.0, 0.0,
        //  0, dRandReal()*10.0-6.0);
        //q[0] = cos(0.18f); q[1] = 0.0f; q[2] = 0.0f; q[3] = sin(0.18f);
        //q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;
        
        CQuaternion cOrientation2(q[0], q[1], q[2], q[3]);
        cOrientation2.Normalize();
        

        dBodySetQuaternion (obj[i].body,q);
        
        obj[i].my_body.SetOrientation(cOrientation2);
        
        dBodySetData (obj[i].body,(void*) i);
                
        dMassSetBox (&m,DENSITY,cBox2[0]*2,cBox2[1]*2,cBox2[2]*2);
        obj[i].geom[0] = dCreateBox (space,cBox2[0]*2,cBox2[1]*2,cBox2[2]*2);
                
        for (k=0; k < GPB; k++) {
            if (obj[i].geom[k]) dGeomSetBody (obj[i].geom[k],obj[i].body);
        }
        
        
        dBodySetMass (obj[i].body,&m);
        
        obj[i].my_body.SetInvMass(1.0/m.mass);
        CMatrix3DRow cInertiaTensor2(CVector3D(m.I[0], m.I[1], m.I[2]),
            CVector3D(m.I[4], m.I[5], m.I[6]),
            CVector3D(m.I[8], m.I[9], m.I[10]));
        
        obj[i].my_body.SetInertiaTensor(cInertiaTensor2);
        obj[i].my_body.SetInvInertiaTensor(cInertiaTensor2.GetInverse().GetInRows());
        obj[i].my_body.SetRestitutionCoef(0.0);
        obj[i].my_body.SetSlidingFrictionCoef(FRICTION);
        obj[i].my_body.SetAngularVelocity(CVector3D(10,0,0));
        obj[i].my_body.ActualizeTensors();
        
        obj[i].my_body.AngularMomentumFromAngularVelocity();
        
        cBodies.Add(&(obj[i].my_body));


        // Test
        //M_ContactArray cArray;
        CPointPairArray cArray;
        
        g_bIsIntersection = BoxIntersection(cBox1, cPos1, cOrientation1, cBox2, cPos2, cOrientation2);

        if (!g_bIsIntersection)
            BoxCollision(cArray,cBox1, cPos1, cOrientation1, cBox2, cPos2, cOrientation2);

        CreateBodiesFromPointPairs(cArray);
    }
}


// draw a geom

void drawGeom (dGeomID g, const dReal *pos, const dReal *R)
{
  if (!g) return;
  if (!pos) pos = dGeomGetPosition (g);
  if (!R) R = dGeomGetRotation (g);

  int type = dGeomGetClass (g);
  if (type == dBoxClass) {
    dVector3 sides;
    dGeomBoxGetLengths (g,sides);
    dsDrawBox (pos,R,sides);
  }
  else if (type == dSphereClass) {
    dsDrawSphere (pos,R,dGeomSphereGetRadius (g));
  }
  else if (type == dCCylinderClass) {
    dReal radius,length;
    dGeomCCylinderGetParams (g,&radius,&length);
    dsDrawCappedCylinder (pos,R,length,radius);
  }
  else if (type == dGeomTransformClass) {
    dGeomID g2 = dGeomTransformGetGeom (g);
    const dReal *pos2 = dGeomGetPosition (g2);
    const dReal *R2 = dGeomGetRotation (g2);
    dVector3 actual_pos;
    dMatrix3 actual_R;
    dMULTIPLY0_331 (actual_pos,R,pos2);
    actual_pos[0] += pos[0];
    actual_pos[1] += pos[1];
    actual_pos[2] += pos[2];
    dMULTIPLY0_333 (actual_R,R,R2);
    drawGeom (g2,actual_pos,actual_R);
  }
}


// simulation loop
void CollisionDetection(M_ContactArray& cContactPoints)
{
	cContactPoints.Release();

	for (int i=0; i<num; i++)
	{	 
		dBodySetPosition(obj[i].body,obj[i].my_body.GetPos().Getx(), obj[i].my_body.GetPos().Gety(),obj[i].my_body.GetPos().Getz());
		
		dQuaternion q;
		q[0] = obj[i].my_body.GetOrientation().Getw();
		q[1] = obj[i].my_body.GetOrientation().Getx();
		q[2] = obj[i].my_body.GetOrientation().Gety();
		q[3] = obj[i].my_body.GetOrientation().Getz();
		
		dBodySetQuaternion(obj[i].body, q);
	}
		
	dSpaceCollide(space,(void *) (&cContactPoints),&nearCallback);
}

static void simLoop (int pause)
{

  
  dsSetTexture (DS_WOOD);
  for (int i=0; i<num; i++) {
    int color_changed = 0;
    if (i == 0)
        dsSetColor (g_bIsIntersection ? 0.5 : 1,0,0);
    else
        if (i == 1)
            dsSetColor (0,g_bIsIntersection ? 0.5 : 1,0);
        else
            dsSetColor (0,0,1);

    
    for (int j=0; j < GPB; j++) 
		drawGeom (obj[i].geom[j],0,0);

    if (color_changed) dsSetColor (1,1,0);
  }
}


int main (int argc, char **argv)
{
  // setup pointers to drawstuff callback functions
  dsFunctions fn;
  fn.version = DS_VERSION;
  fn.start = &start;
  fn.step = &simLoop;
  fn.command = &command;
  fn.stop = 0;
  fn.path_to_textures = "../../drawstuff/textures";

  // create world

  world = dWorldCreate();
  space = dHashSpaceCreate();
  contactgroup = dJointGroupCreate (0);
  dWorldSetGravity (world,0,0,-0.5);
  dWorldSetCFM (world,1e-5);
  dCreatePlane (space,0,0,1,0);
  memset (obj,0,sizeof(obj));

  // run simulation
  dsSimulationLoop (argc,argv,352,288,&fn);

  dJointGroupDestroy (contactgroup);
  dSpaceDestroy (space);
  dWorldDestroy (world);

  return 0;
}
