/*************************************************************************
 *                                                                       *
 * Open Dynamics Engine, Copyright (C) 2001 Russell L. Smith.            *
 *   Email: russ@q12.org   Web: www.q12.org                              *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of the GNU Lesser General Public            *
 * License as published by the Free Software Foundation; either          *
 * version 2.1 of the License, or (at your option) any later version.    *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 * Lesser General Public License for more details.                       *
 *                                                                       *
 * You should have received a copy of the GNU Lesser General Public      *
 * License along with this library (see the file LICENSE.TXT); if not,   *
 * write to the Free Software Foundation, Inc., 59 Temple Place,         *
 * Suite 330, Boston, MA 02111-1307 USA.                                 *
 *                                                                       *
 *************************************************************************/

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <conio.h>
#include <ode/ode.h>
#include <drawstuff/drawstuff.h>
#include "HordubalClientDll.h"

#include <stdafx.h>
#include <marina.hpp>
#include <RestContactResolver.hpp>
#include <Simulation.h>
#include <BoxCollision.h>
#include <BodyGroup.h>
#include <MyMass.h>
#include <Geometry.h>

#ifdef MSVC
#pragma warning(disable:4244 4305)  // for VC++, no precision loss complaints
#endif

// select correct drawing functions

#ifdef dDOUBLE
#define dsDrawBox dsDrawBoxD
#define dsDrawSphere dsDrawSphereD
#define dsDrawCylinder dsDrawCylinderD
#define dsDrawCappedCylinder dsDrawCappedCylinderD
#endif


// some constants
#define DENSITY (5.0)		// density of all objects

#define FRICTION 0.8
#define RESTITUTION 0.3

#define D3DCOLOR_ARGB(a,r,g,b) \
    ((unsigned int)((((a)&0xff)<<24)|(((r)&0xff)<<16)|(((g)&0xff)<<8)|((b)&0xff)))
#define D3DCOLOR_XRGB(r,g,b)   D3DCOLOR_ARGB(0xff,r,g,b)

// dynamics and collision objects
static int num=0;		// number of objects in simulation
static int nextobj=0;		// next object to recycle if num==NUM
static dWorldID world;
static dSpaceID space;
static dJointGroupID contactgroup;
static int selected = -1;	// selected object
static unsigned int g_iLastUsedVladasId = 0;

static M_BodyArray g_cBodies;
static CSimulation g_cSimulation(g_cBodies);
static CHordubalClientDll * g_pcDrawingClient;
static BOOL g_bUseDrawstuff;
static BOOL g_bUseHordubal;
static char * g_pszHordubalIP;


float CalculateAngleBetweenVectors(Vector3 cVec1, Vector3 cVec2)
{
    cVec1.Normalize();
    cVec2.Normalize();

    float sinus = (cVec1.CrossProduct(cVec2)).Size();
    float cosinus = cVec1 * cVec2;

    float angle = asinf(sinus); //betwen -pi/2 and pi/2
    ASSERT(angle >= 0.0f);

    if (cosinus < 0.0f)
        angle = 3.1415 - angle; // result is between 0 and pi

    return angle;
}

void CreateBodiesFromContacts(M_ContactArray& cArray)
{
  /*  dMass m;
    for(unsigned int i = 0; i < cArray.Size(); i++)
    {
        if (num >= NUM)
        {
            return;
        }
        
       g_cBodies[num].body = dBodyCreate(world);
        Vector3 cPos = cArray[i].pos;        

        dBodySetPosition (cBodies[num]body, cPos[0],
             cPos[1], cPos[2]);

       g_cBodies[num].my_body.SetPos(cPos);


        
        Vector3 cRotationAxe = Vector3(1,0,0) % cArray[i].dir[0];
        cRotationAxe.Normalize();

        float fAngle = CalculateAngleBetweenVectors(Vector3(1,0,0), cArray[i].dir[0]);

        dQuaternion q;
        q[0] = cosf(fAngle/2);
        float fSinus = sinf(fAngle/2);
        q[1] = cRotationAxe[0] * fSinus;
        q[2] = cRotationAxe[1] * fSinus;
        q[3] = cRotationAxe[2] * fSinus;
        //dQFromAxisAndAngle(q,cRotationAxe[0],cRotationAxe[1],cRotationAxe[2],fAngle );

        dBodySetQuaternion(obj[num].body,q);
        obj[num].my_body.SetOrientation(CQuaternion(q[0], q[1], q[2], q[3]));

        
        obj[num].geom[0] = dCreateBox (space, 0.05, 0.025, 0.025);
                
        dGeomSetBody (obj[num].geom[0],obj[num].body);
        dMassSetBox(&m,DENSITY,0.05, 0.025, 0.025);
        dBodySetMass (obj[num].body,&m);

        num++;
    }
    */
}



// start simulation - set viewpoint

static void start()
{
  static float xyz[3] = {0.0f,-25.0f,10.0f};
  static float hpr[3] = {90.0000f,-17.0000f,0.0000f};
  dsSetViewpoint (xyz,hpr);
  printf ("To drop another object, press:\n");
  printf ("   o for scud.\n");
 // printf ("   s for sphere.\n");
 // printf ("   c for cylinder.\n");
  printf ("   i for mi24.\n");
 // printf ("To select an object, press space.\n");
 // printf ("To disable the selected object, press d.\n");
 // printf ("To enable the selected object, press e.\n");

  M_TBODY tBody;
  tBody._pcGroup = NULL;
  tBody._pcBodyPhys = new CRigidBodyBase();        
  tBody._bStaticBody = TRUE;
  
  Vector3 cPos(0,0,-0.6);
  tBody._pcBodyPhys->SetPos(cPos);

  dQuaternion q;        
  q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;        
  CQuaternion cQuat(q[0], q[1], q[2], q[3]);

  tBody._pcBodyPhys->SetOrientation(cQuat);
  CMass cMass;       

  CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);  
  pcGeometry->LoadFromP3D("marina/scenes/podlaha.p3d", cMass); 

  cMass.SetRelPoint(cMass.MassCenter());
  pcGeometry->TranslateChilds(-cMass.MassCenter());
  pcGeometry->SetOrigin(cPos, cQuat);

  // g_iLastUsedVladasId ++;
  pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));

  tBody._pcGeometry = pcGeometry;            

  //tBody._pcBodyPhys->SetInvMass(1.0f / cMass.Mass());
  tBody._pcBodyPhys->SetInertiaTensor(cMass.InertiaTensor());
  tBody._pcBodyPhys->SetInvInertiaTensor(cMass.InertiaTensor().GetInverse().GetInRows());
  tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
  tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);
  //tBody._pcBodyPhys->SetAngularVelocity(Vector3(10,0,0));
  tBody._pcBodyPhys->ActualizeTensors();

  tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();

  g_cBodies.Add(tBody);
  tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
}


void LoadBody(char * pszBodyPath, M_TBODY& tBody)
{
    FILE *fBody = fopen(pszBodyPath,"r");
    if (fBody == NULL)
        return;

    char pszMsg[256];
    float fTemp0, fTemp1, fTemp2, fTemp3;
    
    int nComponents;
    fscanf(fBody,"Components: %d\n", &nComponents);
    
    if (nComponents > 0)
    {              
        CComposeGeometry * pcCompGeometry = new CComposeGeometry(++LastUsedID);
        
        CMass cMass;

        int iBodyColor = D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256));
        
        for( int j = 0; j < nComponents; j++)
        {                          
            fscanf(fBody,"%s\n",pszMsg); // Type not used now
            
            Vector3 cRelPos;
            fscanf(fBody,"Position: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
            cRelPos = Vector3(fTemp0, fTemp1, fTemp2);
            
            CQuaternion cRelQuat;                          
            fscanf(fBody,"Orientation: %f,%f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2, &fTemp3);
            cRelQuat= CQuaternion(fTemp0, fTemp1, fTemp2, fTemp3);
            
            marina_real fMass;
            fscanf(fBody,"Mass: %f\n", &fTemp0);
            fMass = fTemp0;
            
            Vector3 cBox;
            fscanf(fBody,"Sizes: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
            cBox= Vector3(fTemp0, fTemp1, fTemp2);
            
            CBoxGeometry * pcGeometry = new CBoxGeometry(++LastUsedID);
            pcGeometry->SetBoxSizes(cBox);                        
            pcGeometry->SetRelOrigin(cRelPos, CQuaternion());
            
            CMass cMassRel;
            cMassRel.SetBox(fMass,cBox);                                ;
            cMassRel.Translate(cRelPos);
            
            cMass += cMassRel;
            
            g_iLastUsedVladasId++;
            pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,iBodyColor);  

            pcCompGeometry->Add(*pcGeometry);
        }
        
        fclose(fBody);
        
        pcCompGeometry->TranslateChilds(-cMass.MassCenter());
        cMass.SetRelPoint(cMass.MassCenter());
                                
        tBody._pcGeometry = pcCompGeometry;
                
        tBody._pcBodyPhys->SetInvMass(1.0/cMass.Mass());
        tBody._pcBodyPhys->SetInertiaTensor(cMass.InertiaTensor());
        tBody._pcBodyPhys->SetInvInertiaTensor(cMass.InertiaTensor().GetInverse().GetInRows());
    }
}


char locase (char c)
{
  if (c >= 'A' && c <= 'Z') return c - ('a'-'A');
  else return c;
}


// called when a key pressed

static void command (int cmd)
{
	//int i,j,k;
    dMass m;
	
    DWORD dwTime = GetTickCount();
    dRandSetSeed(dwTime);

	cmd = locase (cmd);

	if (cmd == 'b') 
	{
    dReal sides[3];

    M_TBODY tBody;
    tBody._pcGroup = NULL;
    tBody._pcBodyPhys = new CRigidBodyBase();        
    tBody._bStaticBody = FALSE;

    // FLY
    for (int k=0; k<3; k++) sides[k] = dRandReal()*2.5+0.1;// 0.3;                

    Vector3 cPos(dRandReal()*1-0.5,dRandReal()*1-0.5,dRandReal()+5);
    tBody._pcBodyPhys->SetPos(cPos);

    dQuaternion q;        

    dQFromAxisAndAngle (q,dRandReal()*2.0-1.0,dRandReal()*2.0-1.0,
      dRandReal()*2.0-1.0,dRandReal()*10.0-5.0);

    //q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;        
    CQuaternion cQuat(q[0], q[1], q[2], q[3]);

    tBody._pcBodyPhys->SetOrientation(cQuat);
    CMass cMass;
    cMass.SetBox(1.0f,Vector3(sides[0]/2,sides[1]/2,sides[2]/2));

    //dMassSetBox (&m,DENSITY,sides[0],sides[1],sides[2]);

    CBoxGeometry * pcGeometry = new CBoxGeometry(++LastUsedID);
    pcGeometry->SetBoxSizes(Vector3(sides[0]/2,sides[1]/2,sides[2]/2));            
    pcGeometry->SetOrigin(cPos, cQuat);

    g_iLastUsedVladasId ++;
    pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));

    tBody._pcGeometry = pcGeometry;                                    
    tBody._pcBodyPhys->SetInvMass(1.0f/cMass.Mass());

    tBody._pcBodyPhys->SetInertiaTensor(cMass.InertiaTensor());
    tBody._pcBodyPhys->SetInvInertiaTensor(cMass.InertiaTensor().GetInverse().GetInRows());
    tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
    tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);
    //tBody._pcBodyPhys->SetAngularVelocity(Vector3(10,0,0));
    tBody._pcBodyPhys->ActualizeTensors();

    tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();

    g_cBodies.Add(tBody);
    tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);     
  }
  else
    if (cmd == 'p') 
    {
      M_TBODY tBody;
      tBody._pcGroup = NULL;
      tBody._pcBodyPhys = new CRigidBodyBase();        
      tBody._bStaticBody = TRUE;
                  
      //Vector3 cPos(dRandReal()*1-0.5,dRandReal()*1-0.5,dRandReal()+2);
      //Vector3 cPos(0,0,1);
      Vector3 cPos(VZero);
      tBody._pcBodyPhys->SetPos(cPos);

      dQuaternion q;        

      //dQFromAxisAndAngle (q,dRandReal()*2.0-1.0,dRandReal()*2.0-1.0,
      //  dRandReal()*2.0-1.0,dRandReal()*10.0-5.0);
      //dQFromAxisAndAngle (q,0,1,0,45);

      q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;        
      CQuaternion cQuat(q[0], q[1], q[2], q[3]);

      tBody._pcBodyPhys->SetOrientation(cQuat);
      CMass cMass;       

      CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
      //pcGeometry->LoadFromP3D("marina/scenes/mi24_HIND_my.p3d", cMass);
      //pcGeometry->LoadFromP3D("marina/scenes/hranol.p3d", cMass); 
      //pcGeometry->LoadFromP3D("marina/scenes/krychle.p3d", cMass); 
      pcGeometry->LoadFromP3D("marina/scenes/podlaha.p3d", cMass); 
                
      cMass.SetRelPoint(cMass.MassCenter());
      pcGeometry->TranslateChilds(-cMass.MassCenter());
      pcGeometry->SetOrigin(cPos, cQuat);

     // g_iLastUsedVladasId ++;
      pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));

      tBody._pcGeometry = pcGeometry;            
      
      //tBody._pcBodyPhys->SetInvMass(1.0f / cMass.Mass());
      tBody._pcBodyPhys->SetInertiaTensor(cMass.InertiaTensor());
      tBody._pcBodyPhys->SetInvInertiaTensor(cMass.InertiaTensor().GetInverse().GetInRows());
      tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
      tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);
      //tBody._pcBodyPhys->SetAngularVelocity(Vector3(10,0,0));
      tBody._pcBodyPhys->ActualizeTensors();

      tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();

      g_cBodies.Add(tBody);
      tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
    }
  else
    if (cmd == 'o') 
    {      
      M_TBODY tBody;
      tBody._pcGroup = NULL;
      tBody._pcBodyPhys =   new CRigidBodyBase();        
      tBody._bStaticBody = FALSE;

      Vector3 cPos(dRandReal()*1-0.5,dRandReal()*1-0.5,dRandReal()+36);
      //Vector3 cPos(0,0,6.4);
      //Vector3 cPos(VZero);
      tBody._pcBodyPhys->SetPos(cPos);

      dQuaternion q;        

      dQFromAxisAndAngle (q,dRandReal()*2.0-1.0,dRandReal()*2.0-1.0,
        dRandReal()*2.0-1.0,dRandReal()*10.0-5.0);
      //dQFromAxisAndAngle (q,0,0,1,45);

      //q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;        
      CQuaternion cQuat(q[0], q[1], q[2], q[3]);

      tBody._pcBodyPhys->SetOrientation(cQuat);
      CMass cMass;           

      CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
      //pcGeometry->LoadFromP3D("marina/scenes/mi24_HIND_my.p3d", cMass); 
      //pcGeometry->LoadFromP3D("marina/scenes/hranol.p3d", cMass); 
      //pcGeometry->LoadFromP3D("marina/scenes/krychle.p3d", cMass); 
      pcGeometry->LoadFromP3D("marina/scenes/scud.p3d", cMass); 

      cMass.SetRelPoint(cMass.MassCenter());
      pcGeometry->TranslateChilds(-cMass.MassCenter());
      pcGeometry->SetOrigin(cPos, cQuat);

      // g_iLastUsedVladasId ++;
      pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));

      tBody._pcGeometry = pcGeometry;            

      tBody._pcBodyPhys->SetInvMass(1.0f / cMass.Mass());
      tBody._pcBodyPhys->SetInertiaTensor(cMass.InertiaTensor());
      tBody._pcBodyPhys->SetInvInertiaTensor(cMass.InertiaTensor().GetInverse().GetInRows());
      tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
      tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);
      //tBody._pcBodyPhys->SetAngularVelocity(Vector3(10,0,0));
      tBody._pcBodyPhys->ActualizeTensors();

      tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();

      g_cBodies.Add(tBody);
      tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
    }
    else
      if (cmd == 'i') 
      {
        M_TBODY tBody;
        tBody._pcGroup = NULL;
        tBody._pcBodyPhys = new CRigidBodyBase();        
        tBody._bStaticBody = FALSE;

        Vector3 cPos(dRandReal()*1-0.5,dRandReal()*1-0.5,dRandReal()+26);
        //Vector3 cPos(0,0,6.4);
        //Vector3 cPos(VZero);
        tBody._pcBodyPhys->SetPos(cPos);

        dQuaternion q;        

        dQFromAxisAndAngle (q,dRandReal()*2.0-1.0,dRandReal()*2.0-1.0,
          dRandReal()*2.0-1.0,dRandReal()*10.0-5.0);
        //dQFromAxisAndAngle (q,1.0,0,0,45);

        //q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;        
        CQuaternion cQuat(q[0], q[1], q[2], q[3]);

        tBody._pcBodyPhys->SetOrientation(cQuat);
        CMass cMass;
       
        CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
        pcGeometry->LoadFromP3D("marina/scenes/mi24_HIND.p3d", cMass); 
        //pcGeometry->LoadFromP3D("marina/scenes/hranol.p3d", cMass); 
        //pcGeometry->LoadFromP3D("marina/scenes/krychle.p3d", cMass);         
        //pcGeometry->LoadFromP3D("marina/scenes/rotor.p3d", cMass); 

        cMass.SetRelPoint(cMass.MassCenter());
        pcGeometry->TranslateChilds(-cMass.MassCenter());
        pcGeometry->SetOrigin(cPos, cQuat);

        // g_iLastUsedVladasId ++;
        pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));

        tBody._pcGeometry = pcGeometry;            

        tBody._pcBodyPhys->SetInvMass(1.0f / cMass.Mass());
        tBody._pcBodyPhys->SetInertiaTensor(cMass.InertiaTensor());
        tBody._pcBodyPhys->SetInvInertiaTensor(cMass.InertiaTensor().GetInverse().GetInRows());
        tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
        tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);
        //tBody._pcBodyPhys->SetAngularVelocity(Vector3(10,0,0));
        tBody._pcBodyPhys->ActualizeTensors();

        tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();

        g_cBodies.Add(tBody);
        tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
      }
      else
        if (cmd == 'u') 
        {
          M_TBODY tBody;
          tBody._pcGroup = NULL;
          tBody._pcBodyPhys = new CRigidBodyBase();        
          tBody._bStaticBody = FALSE;

          //Vector3 cPos(dRandReal()*1-0.5,dRandReal()*1-0.5,dRandReal()+26);
          Vector3 cPos(0,0,6.4);
          //Vector3 cPos(VZero);
          tBody._pcBodyPhys->SetPos(cPos);

          dQuaternion q;        

          //dQFromAxisAndAngle (q,dRandReal()*2.0-1.0,dRandReal()*2.0-1.0,
          //  dRandReal()*2.0-1.0,dRandReal()*10.0-5.0);
          //dQFromAxisAndAngle (q,1.0,0,0,45);

          q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;        
          CQuaternion cQuat(q[0], q[1], q[2], q[3]);

          tBody._pcBodyPhys->SetOrientation(cQuat);
          CMass cMass;

          CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
          pcGeometry->LoadFromP3D("marina/scenes/mi24_HIND.p3d", cMass); 
          //pcGeometry->LoadFromP3D("marina/scenes/hranol.p3d", cMass); 
          //pcGeometry->LoadFromP3D("marina/scenes/krychle.p3d", cMass);         
          //pcGeometry->LoadFromP3D("marina/scenes/rotor.p3d", cMass); 

          cMass.SetRelPoint(cMass.MassCenter());
          pcGeometry->TranslateChilds(-cMass.MassCenter());
          pcGeometry->SetOrigin(cPos, cQuat);

          // g_iLastUsedVladasId ++;
          pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));

          tBody._pcGeometry = pcGeometry;            

          tBody._pcBodyPhys->SetInvMass(1.0f / cMass.Mass());
          tBody._pcBodyPhys->SetInertiaTensor(cMass.InertiaTensor());
          tBody._pcBodyPhys->SetInvInertiaTensor(cMass.InertiaTensor().GetInverse().GetInRows());
          tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
          tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);
          //tBody._pcBodyPhys->SetAngularVelocity(Vector3(10,0,0));
          tBody._pcBodyPhys->ActualizeTensors();

          tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();

          g_cBodies.Add(tBody);
          tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);

          {
          
            M_TBODY tBody;
            tBody._pcGroup = NULL;
            tBody._pcBodyPhys = new CRigidBodyBase();        
            tBody._bStaticBody = FALSE;

            //Vector3 cPos(dRandReal()*1-0.5,dRandReal()*1-0.5,dRandReal()+26);
            Vector3 cPos(0,4,6.4);
            //Vector3 cPos(VZero);
            tBody._pcBodyPhys->SetPos(cPos);

            dQuaternion q;        

            //dQFromAxisAndAngle (q,dRandReal()*2.0-1.0,dRandReal()*2.0-1.0,
            //  dRandReal()*2.0-1.0,dRandReal()*10.0-5.0);
            dQFromAxisAndAngle (q,1.0,1,1,45);

            //q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;        
            CQuaternion cQuat(q[0], q[1], q[2], q[3]);

            tBody._pcBodyPhys->SetOrientation(cQuat);
            CMass cMass;

            CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
            pcGeometry->LoadFromP3D("marina/scenes/mi24_HIND.p3d", cMass); 
            //pcGeometry->LoadFromP3D("marina/scenes/hranol.p3d", cMass); 
            //pcGeometry->LoadFromP3D("marina/scenes/krychle.p3d", cMass);         
            //pcGeometry->LoadFromP3D("marina/scenes/rotor.p3d", cMass); 

            cMass.SetRelPoint(cMass.MassCenter());
            pcGeometry->TranslateChilds(-cMass.MassCenter());
            pcGeometry->SetOrigin(cPos, cQuat);

            // g_iLastUsedVladasId ++;
            pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));

            tBody._pcGeometry = pcGeometry;            

            tBody._pcBodyPhys->SetInvMass(1.0f / cMass.Mass());
            tBody._pcBodyPhys->SetInertiaTensor(cMass.InertiaTensor());
            tBody._pcBodyPhys->SetInvInertiaTensor(cMass.InertiaTensor().GetInverse().GetInRows());
            tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
            tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);
            //tBody._pcBodyPhys->SetAngularVelocity(Vector3(10,0,0));
            tBody._pcBodyPhys->ActualizeTensors();

            tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();

            g_cBodies.Add(tBody);
            tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
          }
        }
        else
  if (cmd == 'x') 
	{
    M_TBODY tBody;
    tBody._pcGroup = NULL;
    tBody._pcBodyPhys = new CRigidBodyBase();        
    tBody._bStaticBody = FALSE;

    Vector3 cPos(dRandReal()*1-0.5,dRandReal()*1-0.5,dRandReal()+5);    
    tBody._pcBodyPhys->SetPos(cPos);

    dQuaternion q;        

    dQFromAxisAndAngle (q,dRandReal()*2.0-1.0,dRandReal()*2.0-1.0,
      dRandReal()*2.0-1.0,dRandReal()*10.0-5.0);

    //q[0] = 1.0f; q[1] = q[2] = q[3] = 0.0f;        
    CQuaternion cQuat(q[0], q[1], q[2], q[3]);

    tBody._pcBodyPhys->SetOrientation(cQuat);

    //do qeometry
    CComposeGeometry * pcCompGeometry = new CComposeGeometry(++LastUsedID);
    CMass cMass;
    for(unsigned int i = 0; i < 3; i++)
    {        
      Vector3 cBoxSizes;

      cBoxSizes[0] = (dRandReal()*0.5+0.1) / 2;
      cBoxSizes[1] = (dRandReal()*0.5+0.1) / 2;
      cBoxSizes[2] = (dRandReal()*0.5+0.1) / 2;                        

      Vector3 cRelPos;
      cRelPos[0] = dRandReal()*0.8-0.4;
      cRelPos[1] = dRandReal()*0.8-0.4;
      cRelPos[2] = dRandReal()*0.8-0.4;

      CBoxGeometry * pcGeometry = new CBoxGeometry(++LastUsedID);
      pcGeometry->SetBoxSizes(cBoxSizes);                        
      pcGeometry->SetRelOrigin(cRelPos, CQuaternion());

      CMass cMassRel;
      cMassRel.SetBox(dRandReal() + 0.5,cBoxSizes);                                ;
      cMassRel.Translate(cRelPos);

      cMass += cMassRel;

      g_iLastUsedVladasId ++;
      pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));

      pcCompGeometry->Add(*pcGeometry);
    }

    pcCompGeometry->TranslateChilds(-cMass.MassCenter());
    cMass.SetRelPoint(cMass.MassCenter());

    tBody._pcGeometry = pcCompGeometry;            

    tBody._pcBodyPhys->SetInvMass(1.0f/cMass.Mass());

    tBody._pcBodyPhys->SetInertiaTensor(cMass.InertiaTensor());
    tBody._pcBodyPhys->SetInvInertiaTensor(cMass.InertiaTensor().GetInverse().GetInRows());
    tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
    tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);
    tBody._pcBodyPhys->SetAngularVelocity(Vector3(dRandReal() * 2,dRandReal() * 2,dRandReal() * 2));
    tBody._pcBodyPhys->ActualizeTensors();

    tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();

    g_cBodies.Add(tBody);
    tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
        
  }
  else
	 /* if (cmd == 'e')
	  {
          // load from file.

          FILE *fConf = fopen("w:\\c\\marina\\ode-0.03\\ode\\marina\\scenes\\scene1.sce","r");
          if (fConf == NULL)
              return;

          while(!feof(fConf))
          {
              char pszMsg[256];
              fscanf(fConf,"%s\n",pszMsg); // type not used now

              Vector3 cPos;
              float fTemp0, fTemp1, fTemp2;
              fscanf(fConf,"Position: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
              cPos = Vector3(fTemp0, fTemp1, fTemp2);

              CQuaternion cQuat;
              float fTemp3;
              fscanf(fConf,"Orientation: %f,%f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2, &fTemp3);
              cQuat = Vector3(fTemp0, fTemp1, fTemp2, fTemp3);

              Vector3 cVelocity;
              fscanf(fConf,"Velocity: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
              cVelocity = Vector3(fTemp0, fTemp1, fTemp2);

              Vector3 cAngularVelocity;
              fscanf(fConf,"AngularVelocity: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
              cAngularVelocity = Vector3(fTemp0, fTemp1, fTemp2);

              marina_real fMass;
              fscanf(fConf,"Mass: %f\n", &fTemp0);
              fMass = fTemp0;

              Vector3 cBox;
              fscanf(fConf,"Sizes: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
              cBox = Vector3(fTemp0, fTemp1, fTemp2);
              
              //i = num;
		      //num++;  
              
              M_TBODY tBody;
              tBody._pcGroup = NULL;
              tBody._pcBodyPhys = new CRigidBodyBase();
             			 			  
			  tBody._pcBodyPhys->SetPos(cPos);			  			  			  
			  tBody._pcBodyPhys->SetOrientation(cQuat);
			  
			  dMassSetBox (&m,DENSITY,cBox.Getx(),cBox.Gety(),cBox.Getz());
             // dMassAdjust (&m, fMass);
               // Create body in hordubal
              cBox /= 2.0f;

              CBoxGeometry * pcGeometry = new CBoxGeometry(cBox);
              pcGeometry->SetOrigin(cPos, cQuat);

              g_iLastUsedVladasId ++;
              pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));                                        

              tBody._pcGeometry = pcGeometry;
			  
			  tBody._pcBodyPhys->SetInvMass(1.0/m.mass);
			  CMatrix3DRow cInertiaTensor(Vector3(m.I[0], m.I[1], m.I[2]),
				  Vector3(m.I[4], m.I[5], m.I[6]),
				  Vector3(m.I[8], m.I[9], m.I[10]));
			  
			  tBody._pcBodyPhys->SetInertiaTensor(cInertiaTensor);
			  tBody._pcBodyPhys->SetInvInertiaTensor(cInertiaTensor.GetInverse().GetInRows());
			  tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
			  tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);		 
			  tBody._pcBodyPhys->ActualizeTensors();
              tBody._pcBodyPhys->SetAngularVelocity(cAngularVelocity);
              tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();
              tBody._pcBodyPhys->SetVelocity(cVelocity);
              tBody._pcBodyPhys->MomentumFromVelocity();
              			  			  
			  g_cBodies.Add(tBody);
              tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
		  }
          
          fclose(fConf);
	  }
      else
      if (cmd == 'w')
	  {
          // load from file.

          FILE *fConf = fopen("w:\\c\\marina\\ode-0.03\\ode\\marina\\scenes\\scene1.sc2","rb");
          if (fConf == NULL)
              return;

          while(!feof(fConf))
          {
              
              Vector3 cPos;              
              if (0 == fread(cPos.Getm(),sizeof(marina_real), 3, fConf))
                  break;
              
              CQuaternion cQuat;             
              if (0 == fread(cQuat.Getm(),sizeof(marina_real), 4, fConf))
                  break;
             

              Vector3 cVelocity;
              if (0 == fread(cVelocity.Getm(),sizeof(marina_real), 3, fConf))
                  break;

              Vector3 cAngularVelocity;
              if (0 == fread(cAngularVelocity.Getm(),sizeof(marina_real), 3, fConf))
                  break;

              Vector3 cBoxSize;
              if (0 == fread(cBoxSize.Getm(),sizeof(marina_real), 3, fConf))
                  break;   
              			  
              M_TBODY tBody;
              tBody._pcGroup = NULL;
              tBody._pcBodyPhys = new CRigidBodyBase();
             			 			  
			  tBody._pcBodyPhys->SetPos(cPos);			  			  			  
			  tBody._pcBodyPhys->SetOrientation(cQuat);
			  
			  dMassSetBox (&m,DENSITY,cBoxSize.Getx(),cBoxSize.Gety(),cBoxSize.Getz());
             // dMassAdjust (&m, fMass);
               // Create body in hordubal
              cBoxSize /= 2.0f;

              CBoxGeometry * pcGeometry = new CBoxGeometry(cBoxSize);
              pcGeometry->SetOrigin(cPos, cQuat);

              g_iLastUsedVladasId ++;
              pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseDrawstuff,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));                                        

              tBody._pcGeometry = pcGeometry;
			  
			  tBody._pcBodyPhys->SetInvMass(1.0/m.mass);
			  CMatrix3DRow cInertiaTensor(Vector3(m.I[0], m.I[1], m.I[2]),
				  Vector3(m.I[4], m.I[5], m.I[6]),
				  Vector3(m.I[8], m.I[9], m.I[10]));
			  
			  tBody._pcBodyPhys->SetInertiaTensor(cInertiaTensor);
			  tBody._pcBodyPhys->SetInvInertiaTensor(cInertiaTensor.GetInverse().GetInRows());
			  tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
			  tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);		 
			  tBody._pcBodyPhys->ActualizeTensors();
              tBody._pcBodyPhys->SetAngularVelocity(cAngularVelocity);
              tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();
              tBody._pcBodyPhys->SetVelocity(cVelocity);
              tBody._pcBodyPhys->MomentumFromVelocity();
              			  			  
			  g_cBodies.Add(tBody);
              tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
		  }
          fclose(fConf);
      }
      else*/
  if (cmd == 'q')
  {
  // load from file.

  char * pszPath = "marina\\scenes\\";
  char pszScriptPath[256];
  strcpy(pszScriptPath, pszPath);
  strcat(pszScriptPath, "scene1.sc3");

  FILE *fConf = fopen(pszScriptPath,"r");
  if (fConf == NULL)
    return;

  while(!feof(fConf))
  {
    char pszMsg[256];
    fscanf(fConf,"%s\n",pszMsg); // body

    Vector3 cPos;
    float fTemp0, fTemp1, fTemp2;
    fscanf(fConf,"Position: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
    cPos = Vector3(fTemp0, fTemp1, fTemp2);

    CQuaternion cQuat;
    float fTemp3;
    fscanf(fConf,"Orientation: %f,%f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2, &fTemp3);
    cQuat = CQuaternion(fTemp0, fTemp1, fTemp2, fTemp3);

    Vector3 cVelocity;
    fscanf(fConf,"Velocity: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
    cVelocity = Vector3(fTemp0, fTemp1, fTemp2);

    Vector3 cAngularVelocity;
    fscanf(fConf,"AngularVelocity: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
    cAngularVelocity = Vector3(fTemp0, fTemp1, fTemp2);

    char pszBody[256];
    fscanf(fConf,"%s\n",pszBody); // body

    char pszBodyPath[256];
    strcpy(pszBodyPath, pszPath);
    strcat(pszBodyPath, pszBody);

    M_TBODY tBody;
    tBody._pcGroup = NULL;
    tBody._pcBodyPhys = new CRigidBodyBase();  
    tBody._bStaticBody = FALSE;

    tBody._pcBodyPhys->SetPos(cPos);                  
    tBody._pcBodyPhys->SetOrientation(cQuat); 

    LoadBody(pszBodyPath, tBody);

    tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
    tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);		 
    tBody._pcBodyPhys->ActualizeTensors();
    tBody._pcBodyPhys->SetAngularVelocity(cAngularVelocity);
    tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();
    tBody._pcBodyPhys->SetVelocity(cVelocity);
    tBody._pcBodyPhys->MomentumFromVelocity();

    g_cBodies.Add(tBody);
    tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
  }
  fclose(fConf);
  }
  else         
    if (cmd == 'w')
    {
      // load from file.

      char * pszPath = "marina\\scenes\\";
      char pszScriptPath[256];
      strcpy(pszScriptPath, pszPath);
      strcat(pszScriptPath, "kino.sc3");

      FILE *fConf = fopen(pszScriptPath,"r");
      if (fConf == NULL)
        return;

      while(!feof(fConf))
      {
        char pszMsg[256];
        fscanf(fConf,"%s\n",pszMsg); // body

        Vector3 cPos;
        float fTemp0, fTemp1, fTemp2;
        fscanf(fConf,"Position: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
        cPos = Vector3(fTemp0, fTemp1, fTemp2);

        CQuaternion cQuat;
        float fTemp3;
        fscanf(fConf,"Orientation: %f,%f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2, &fTemp3);
        cQuat = CQuaternion(fTemp0, fTemp1, fTemp2, fTemp3);

        Vector3 cVelocity;
        fscanf(fConf,"Velocity: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
        cVelocity = Vector3(fTemp0, fTemp1, fTemp2);

        Vector3 cAngularVelocity;
        fscanf(fConf,"AngularVelocity: %f,%f,%f\n", &fTemp0, &fTemp1, &fTemp2);
        cAngularVelocity = Vector3(fTemp0, fTemp1, fTemp2);

        char pszBody[256];
        fscanf(fConf,"%s\n",pszBody); // body

        char pszBodyPath[256];
        strcpy(pszBodyPath, pszPath);
        strcat(pszBodyPath, pszBody);


        M_TBODY tBody;
        tBody._pcGroup = NULL;
        tBody._pcBodyPhys = new CRigidBodyBase();  
        tBody._bStaticBody = FALSE;

        tBody._pcBodyPhys->SetPos(cPos);                  
        tBody._pcBodyPhys->SetOrientation(cQuat); 

        LoadBody(pszBodyPath, tBody);

        tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
        tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);		 
        tBody._pcBodyPhys->ActualizeTensors();
        tBody._pcBodyPhys->SetAngularVelocity(cAngularVelocity);
        tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();
        tBody._pcBodyPhys->SetVelocity(cVelocity);
        tBody._pcBodyPhys->MomentumFromVelocity();

        g_cBodies.Add(tBody);
        tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
      }

      fclose(fConf);
    }
    else
      if (cmd == 'c')
      {
        // load from file.

        Vector3 cPos;                 
        cPos = Vector3(2, 0, 1);

        CQuaternion cQuat;                 
        cQuat = CQuaternion(1, dRandReal(), dRandReal(), dRandReal());

        cQuat.Normalize();

        Vector3 cVelocity;                  
        cVelocity = Vector3(-10, 0, 0);

        Vector3 cAngularVelocity;                  
        cAngularVelocity = Vector3(5,10, 5);


        M_TBODY tBody;
        tBody._pcGroup = NULL;
        tBody._pcBodyPhys = new CRigidBodyBase();  
        tBody._bStaticBody = FALSE;

        tBody._pcBodyPhys->SetPos(cPos);                  
        tBody._pcBodyPhys->SetOrientation(cQuat); 

        LoadBody("marina\\scenes\\chair.mg", tBody);

        tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
        tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);		 
        tBody._pcBodyPhys->ActualizeTensors();
        tBody._pcBodyPhys->SetAngularVelocity(cAngularVelocity);
        tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();
        tBody._pcBodyPhys->SetVelocity(cVelocity);
        tBody._pcBodyPhys->MomentumFromVelocity();

        g_cBodies.Add(tBody);
        tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);

      }
      else
        if (cmd == 't')
        {
          // load from file.

          Vector3 cPos;                 
          cPos = Vector3(2, 0, 1);

          CQuaternion cQuat;                 
          cQuat = CQuaternion(1, dRandReal(), dRandReal(), dRandReal());

          cQuat.Normalize();

          Vector3 cVelocity;                  
          cVelocity = Vector3(-10, 0, 0);

          Vector3 cAngularVelocity;                  
          cAngularVelocity = Vector3(5,10, 5);


          M_TBODY tBody;
          tBody._pcGroup = NULL;
          tBody._pcBodyPhys = new CRigidBodyBase();  
          tBody._bStaticBody = FALSE;

          tBody._pcBodyPhys->SetPos(cPos);                  
          tBody._pcBodyPhys->SetOrientation(cQuat); 

          LoadBody("marina\\scenes\\table.mg", tBody);

          tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
          tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);		 
          tBody._pcBodyPhys->ActualizeTensors();
          tBody._pcBodyPhys->SetAngularVelocity(cAngularVelocity);
          tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();
          tBody._pcBodyPhys->SetVelocity(cVelocity);
          tBody._pcBodyPhys->MomentumFromVelocity();

          g_cBodies.Add(tBody);
          tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);

        }
        else
          if (cmd == 'l')
          {
            // load from file.

            Vector3 cPos;                 
            cPos = Vector3(2, 0, 1);

            CQuaternion cQuat;                 
            cQuat = CQuaternion(1, dRandReal(), dRandReal(), dRandReal());

            cQuat.Normalize();

            Vector3 cVelocity;                  
            cVelocity = Vector3(-10, 0, 0);

            Vector3 cAngularVelocity;                  
            cAngularVelocity = Vector3(5,10, 5);


            M_TBODY tBody;
            tBody._pcGroup = NULL;
            tBody._pcBodyPhys = new CRigidBodyBase();  
            tBody._bStaticBody = FALSE;

            tBody._pcBodyPhys->SetPos(cPos);                  
            tBody._pcBodyPhys->SetOrientation(cQuat); 

            LoadBody("marina\\scenes\\lavica.mg", tBody);

            tBody._pcBodyPhys->SetRestitutionCoef(RESTITUTION);
            tBody._pcBodyPhys->SetSlidingFrictionCoef(FRICTION);		 
            tBody._pcBodyPhys->ActualizeTensors();
            tBody._pcBodyPhys->SetAngularVelocity(cAngularVelocity);
            tBody._pcBodyPhys->AngularMomentumFromAngularVelocity();
            tBody._pcBodyPhys->SetVelocity(cVelocity);
            tBody._pcBodyPhys->MomentumFromVelocity();

            g_cBodies.Add(tBody);
            tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);

          }
    /*  else
          if (cmd == 't')
          {
              CMass cMass;
              dMass tMass;
              
              cMass.SetBox(1,Vector3(0.05,0.05,0.05));
              //cMass.Translate(Vector3(100,100,0));
              CQuaternion cQuad(0.707,0.707,0,0);
              cQuad.Normalize();
              cMass.Rotate(cQuad);


              dMassSetBox(&tMass, 1, 0.1, 0.1, 0.1);
              dMassTranslate(&tMass,100,100,0);

              int i = 8;
          }
          */
      
}

int g_i = 0;


LARGE_INTEGER dwLast;
static void simLoop (int pause)
{
  //dsSetColor (0,0,2);
  //dSpaceCollide (space,0,&nearCallback);

  //CSimulation cSimulation(g_cBodies);

  LARGE_INTEGER dwFrequency;
  QueryPerformanceFrequency(&dwFrequency);
  LARGE_INTEGER dwStart;
  QueryPerformanceCounter(&dwStart);
  
  //float fRealFrameTime = (dwStart.LowPart - dwLast.LowPart) / (1.0f * dwFrequency.LowPart) * 1000.0f;
  //dsPrint("Frame Time: %lf\n",fRealFrameTime);
  //dwLast = dwStart;

  /*
if (g_i == 0)
  {
      M_ContactArray cContactPoints;
      CollisionDetection(cContactPoints);
      if (cContactPoints.Size() > 0)
      {
            CreateBodiesFromContacts(cContactPoints);
            g_i = 10;
      }
  }
*/  
  if (_kbhit())
    command(_getch());
 
  float fSimTime = 10.0f;

  g_cSimulation.Simulate(fSimTime / 1000.0f);
  /*g_cSimulation.Simulate(0.01f);
  g_cSimulation.Simulate(0.01f);
  g_cSimulation.Simulate(0.01f);
  g_cSimulation.Simulate(0.01f);
  g_cSimulation.Simulate(0.01f);*/

  LARGE_INTEGER dwEnd;
  QueryPerformanceCounter(&dwEnd);
  float fTimeIntegration = (dwEnd.LowPart - dwStart.LowPart) / (1.0f * dwFrequency.LowPart) * 1000.0f;


  //if (!pause) dWorldStep (world,0.05);

  // remove all contact joints
  //dJointGroupEmpty (contactgroup);  
  marina_real fEnergy = 0.0f;
  {
    //PROFILE_SCOPE(draw);

    if (g_pcDrawingClient != NULL)
      g_pcDrawingClient->BeginScene();

    for (int i = 1; i <g_cBodies.Size(); i++) 
    {
      const M_TBODY& tBody =g_cBodies[i];
      tBody._pcGeometry->Draw();

      fEnergy += tBody._pcBodyPhys->CalculateEnergy() + 10.0f / tBody._pcBodyPhys->GetInvMass() * tBody._pcBodyPhys->GetPos()[2] ;       
    }

    if (g_pcDrawingClient != NULL)
      g_pcDrawingClient->EndScene();
  }

  dsPrint("Sim Time: %lf",fTimeIntegration);

  for(int i = 0; i < GPerfProfilers.N(); i++)
  {
    dsPrint(", %s: %d", GPerfProfilers.Name(i), GPerfProfilers.Value(i));    
  }
  GPerfProfilers.Reset();

  /*for(int i = 0; i < GPerfCounters.N(); i++)
  {
    dsPrint(", %s: %d", GPerfCounters.Name(i), GPerfCounters.Value(i));    
  }*/

  GPerfCounters.Reset();
  dsPrint("\n");

  QueryPerformanceCounter(&dwEnd);
  float fRealTimeSim = (dwEnd.LowPart - dwStart.LowPart) / (1.0f * dwFrequency.LowPart) * 1000.0f;

 // if (fRealTimeSim < fSimTime)
 // Sleep((DWORD) (fRealTimeSim - fSimTime));

  while(fRealTimeSim < fSimTime)
  {
    QueryPerformanceCounter(&dwEnd);
    fRealTimeSim = (dwEnd.LowPart - dwStart.LowPart) / (1.0f * dwFrequency.LowPart) * 1000.0f;
  }


  //dsPrint("Energy: %f\n" , fEnergy);
}


int main (int argc, char **argv)
{

  // Parse commnad line parameters
  if (argc < 2)
  {
    g_bUseDrawstuff = TRUE;
  }
  {
    for( int i = 1; i < argc; i++)
    {
      if (strcmp( argv[i], "-hord") == 0)
      {        
        g_bUseHordubal = TRUE;

        if (i == argc - 1)
        {
          printf(" Please set up IP for hordubal client.\n");
          return 1;
        }

        i++;
        g_pszHordubalIP = argv[i];
      }
      else
        if (strcmp( argv[i], "-drawstuff") == 0)
          g_bUseDrawstuff = TRUE;
        else
        {
          printf(" Wrong parameter.\n");
          return 1;
        }
    }
  }

    // Initialize logging in marina
    InitLogging();

    if (g_bUseHordubal)
    {
        // Initialize Hordubal
        g_pcDrawingClient = new CHordubalClientDll(g_pszHordubalIP);

        if (!g_pcDrawingClient->IsConnected())
            printf(" Warning: Connection to Hordubal failed.\n");
    }

    // Create ground 
    g_cBodies.Reserve(128,128);
    
   /* M_TBODY tBody;
    tBody._pcGroup = NULL;
    tBody._pcBodyPhys = new CRigidBodyBase();
    
    CBoxGeometry * pcGeometry = new CBoxGeometry();
    // FLY
    pcGeometry->SetBoxSizes(Vector3(50,50,0.1));

    g_iLastUsedVladasId++;
    pcGeometry->CreateDrawClients(g_pcDrawingClient, g_iLastUsedVladasId, g_bUseHordubal,D3DCOLOR_XRGB(dRandInt(256), dRandInt(256), dRandInt(256)));

    tBody._pcGeometry = pcGeometry;
    tBody._bStaticBody = TRUE;

    g_cBodies.Add(tBody);
    tBody._pcGeometry->SetBody(&g_cBodies[g_cBodies.Size() - 1]);
*/
    // Start simulation.
    if (g_bUseDrawstuff)
    {
        // Initialize Drawstuff
        dsFunctions fn;
        fn.version = DS_VERSION;
        fn.start = &start;
        fn.step = &simLoop;
        fn.command = &command;
        fn.stop = 0;
        fn.path_to_textures = "../drawstuff/textures";

        world = dWorldCreate();
        space = dHashSpaceCreate();
        contactgroup = dJointGroupCreate (0);
        dWorldSetGravity (world,0,0,-0.5);
        dWorldSetCFM (world,1e-5);
        dCreatePlane (space,0,0,1,0);

        dsSimulationLoop (argc,argv,352,288,&fn);

        dJointGroupDestroy (contactgroup);
        dSpaceDestroy (space);
        dWorldDestroy (world);

        return 0;
    }
    else
    { 
        do 
        {
            simLoop(3);
        } while(TRUE);
    }

}


//"C:\\Work\\ode-0.03-2\\ode\\marina\\assert.sce"

void save_bodies(const char * pszPath)
{    
    // Save All bodies.
   /* FILE *fOutput = fopen(pszPath,"w");
    if (fOutput == NULL)
        return;
        
    for(unsigned int i = 1; i <g_cBodies.Size(); i++)
    {
        const M_TBODY& tBody =g_cBodies[i];

        fprintf(fOutput, "Box:\n");

        fprintf(fOutput,"Position: %f,%f,%f\n", tBody._pcBodyPhys->GetPos().Getx(), tBody._pcBodyPhys->GetPos().Gety(),
            tBody._pcBodyPhys->GetPos().Getz()); 
              
        fprintf(fOutput,"Orientation: %f,%f,%f,%f\n", tBody._pcBodyPhys->GetOrientation().Getw(), 
            tBody._pcBodyPhys->GetOrientation().Getx(), tBody._pcBodyPhys->GetOrientation().Gety(), 
            tBody._pcBodyPhys->GetOrientation().Getz()); 
              
        fprintf(fOutput,"Velocity: %f,%f,%f\n", tBody._pcBodyPhys->GetVelocity().Getx(), tBody._pcBodyPhys->GetVelocity().Gety(), 
            tBody._pcBodyPhys->GetVelocity().Getz()); 
        
        fprintf(fOutput,"AngularVelocity: %f,%f,%f\n", tBody._pcBodyPhys->GetAngularVelocity().Getx(), 
            tBody._pcBodyPhys->GetAngularVelocity().Gety(), 
            tBody._pcBodyPhys->GetAngularVelocity().Getz());
        
        fprintf(fOutput,"Mass: %f\n", 1.0 / tBody._pcBodyPhys->GetInvMass());
             
        dVector3 cBoxSize;
        dGeomBoxGetLengths((*tBody._pcGeomArray)[0].geom, cBoxSize);
        fprintf(fOutput,"Sizes: %f,%f,%f\n", cBoxSize[0], cBoxSize[1], cBoxSize[2]);              
    }

    fclose(fOutput);
    */
}

void save_bodies_binary(const char * pszPath)
{    
    // Save All bodies.
 /*   FILE *fOutput = fopen(pszPath,"wb");
    if (fOutput == NULL)
        return;
        
    for(unsigned int i = 1; i <g_cBodies.Size(); i++)
    {
        const M_TBODY& tBody =g_cBodies[i];
        
        fwrite(tBody._pcBodyPhys->GetPos().Getm(), sizeof(marina_real), 3, fOutput); 
        fwrite(tBody._pcBodyPhys->GetOrientation().Getm(), sizeof(marina_real), 4, fOutput); 
        fwrite(tBody._pcBodyPhys->GetVelocity().Getm(), sizeof(marina_real), 3, fOutput); 
        fwrite(tBody._pcBodyPhys->GetAngularVelocity().Getm(), sizeof(marina_real), 3, fOutput);
        
      
                           
        dVector3 cBoxSize;
        dGeomBoxGetLengths((*tBody._pcGeomArray)[0].geom, cBoxSize);
        fwrite(cBoxSize, sizeof(*cBoxSize), 3, fOutput);             
    }

    fclose(fOutput);
    */
}