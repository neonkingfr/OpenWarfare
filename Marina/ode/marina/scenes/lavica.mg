Components: 10
Box:
Position: 0.2,0.2,0.2
Orientation: 1,0,0,0
Mass: 0.5
Sizes: 0.05,0.05,0.2
Box:
Position: 0.2,-0.2,0.2
Orientation: 1,0,0,0
Mass: 0.5
Sizes: 0.05,0.05,0.2
Box:
Position: -0.2,0.2,0.5
Orientation: 1,0,0,0
Mass: 1
Sizes: 0.05,0.05,0.5
Box:
Position: -0.2,-0.2,0.5
Orientation: 1,0,0,0
Mass: 1
Sizes: 0.05,0.05,0.5
Box:
Position: 0.2,0.6,0.2
Orientation: 1,0,0,0
Mass: 0.5
Sizes: 0.05,0.05,0.2
Box:
Position: 0.2,-0.6,0.2
Orientation: 1,0,0,0
Mass: 0.5
Sizes: 0.05,0.05,0.2
Box:
Position: -0.2,0.6,0.5
Orientation: 1,0,0,0
Mass: 1
Sizes: 0.05,0.05,0.5
Box:
Position: -0.2,-0.6,0.5
Orientation: 1,0,0,0
Mass: 1
Sizes: 0.05,0.05,0.5
Box:
Position: 0,0,0.4
Orientation: 1,0,0,0
Mass: 3
Sizes: 0.2,0.6,0.03
Box:
Position: -0.2,0,0.8
Orientation: 1,0,0,0
Mass: 1
Sizes: 0.03,0.6,0.1