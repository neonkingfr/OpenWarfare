# Microsoft Developer Studio Project File - Name="ode" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=ode - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ode.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ode.mak" CFG="ode - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ode - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "ode - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Marina/ode-0.03/ode", DTKAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ode - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "..\include" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\lib\ode.lib"

!ELSEIF  "$(CFG)" == "ode - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\include" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"../lib\odeD.lib"

!ENDIF 

# Begin Target

# Name "ode - Win32 Release"
# Name "ode - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\src\array.cpp
# End Source File
# Begin Source File

SOURCE=.\src\error.cpp
# End Source File
# Begin Source File

SOURCE=.\src\fastdot.c
# End Source File
# Begin Source File

SOURCE=.\src\fastldlt.c
# End Source File
# Begin Source File

SOURCE=.\src\fastlsolve.c
# End Source File
# Begin Source File

SOURCE=.\src\fastltsolve.c
# End Source File
# Begin Source File

SOURCE=.\src\geom.cpp
# End Source File
# Begin Source File

SOURCE=.\src\joint.cpp
# End Source File
# Begin Source File

SOURCE=.\src\lcp.cpp
# End Source File
# Begin Source File

SOURCE=.\src\mass.cpp
# End Source File
# Begin Source File

SOURCE=.\src\mat.cpp
# End Source File
# Begin Source File

SOURCE=.\src\matrix.cpp
# End Source File
# Begin Source File

SOURCE=.\src\memory.cpp
# End Source File
# Begin Source File

SOURCE=.\src\misc.cpp
# End Source File
# Begin Source File

SOURCE=.\src\obstack.cpp
# End Source File
# Begin Source File

SOURCE=.\src\ode.cpp
# End Source File
# Begin Source File

SOURCE=.\src\odemath.cpp
# End Source File
# Begin Source File

SOURCE=.\src\rotation.cpp
# End Source File
# Begin Source File

SOURCE=.\src\space.cpp
# End Source File
# Begin Source File

SOURCE=.\src\step.cpp
# End Source File
# Begin Source File

SOURCE=.\src\testing.cpp
# End Source File
# Begin Source File

SOURCE=.\src\timer.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\src\array.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\common.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\config.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\contact.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\error.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\geom.h
# End Source File
# Begin Source File

SOURCE=.\src\geom_internal.h
# End Source File
# Begin Source File

SOURCE=.\src\joint.h
# End Source File
# Begin Source File

SOURCE=.\src\lcp.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\mass.h
# End Source File
# Begin Source File

SOURCE=.\src\mat.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\matrix.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\memory.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\misc.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\objects.h
# End Source File
# Begin Source File

SOURCE=.\src\objects.h
# End Source File
# Begin Source File

SOURCE=.\src\obstack.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\ode.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\odecpp.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\odemath.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\rotation.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\space.h
# End Source File
# Begin Source File

SOURCE=.\src\stack.h
# End Source File
# Begin Source File

SOURCE=.\src\step.h
# End Source File
# Begin Source File

SOURCE=.\src\testing.h
# End Source File
# Begin Source File

SOURCE=..\include\ode\timer.h
# End Source File
# End Group
# End Target
# End Project
