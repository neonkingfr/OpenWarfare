# Microsoft Developer Studio Project File - Name="test_boxstack" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=test_boxstack - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "test_boxstack.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "test_boxstack.mak" CFG="test_boxstack - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "test_boxstack - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "test_boxstack - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Marina/ode-0.03/ode", DTKAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "test_boxstack - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "test_boxstack___Win32_Release"
# PROP BASE Intermediate_Dir "test_boxstack___Win32_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "test"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /O2 /I "..\include" /D "WIN32" /D "MSVC" /FR /GF /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Comctl32.lib kernel32.lib user32.lib gdi32.lib OpenGL32.lib Glu32.lib ode.lib drawstuff.lib /nologo /machine:IX86 /out:"test/test_boxstackR.exe" /libpath:"../lib"

!ELSEIF  "$(CFG)" == "test_boxstack - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "test_boxstack___Win32_Debug"
# PROP BASE Intermediate_Dir "test_boxstack___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "test"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\include" /D "WIN32" /D "_DEBUG" /D "_MSVC" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Comctl32.lib kernel32.lib user32.lib gdi32.lib OpenGL32.lib Glu32.lib odeD.lib drawstuffD.lib /nologo /subsystem:console /debug /machine:I386 /out:"test/test_boxstackD.exe" /pdbtype:sept /libpath:"..\lib"

!ENDIF 

# Begin Target

# Name "test_boxstack - Win32 Release"
# Name "test_boxstack - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\test\test_boxstack.cpp
DEP_CPP_TEST_=\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	

!IF  "$(CFG)" == "test_boxstack - Win32 Release"

!ELSEIF  "$(CFG)" == "test_boxstack - Win32 Debug"

!ENDIF 

# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=..\drawstuff\src\resources.rc
# End Source File
# End Group
# End Target
# End Project
