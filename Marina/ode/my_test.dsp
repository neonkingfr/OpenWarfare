# Microsoft Developer Studio Project File - Name="my_test" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=my_test - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "my_test.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "my_test.mak" CFG="my_test - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "my_test - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "my_test - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Marina/ode-0.03/ode", DTKAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "my_test - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "my_test___Win32_Release"
# PROP BASE Intermediate_Dir "my_test___Win32_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "test"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /Zi /O2 /Op /I "..\include" /I "marina" /I "..\..\mtl-2.1.2-20" /I "..\..\..\HordubalClientDll\inc" /I "..\..\.." /D "WIN32" /D "MSVC" /D "_WINDOWS" /D "_MBCS" /FR /GF /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"test/my_testR.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Comctl32.lib kernel32.lib user32.lib gdi32.lib OpenGL32.lib Glu32.lib ode.lib drawstuff.lib HordubalClientDll.lib /nologo /debug /machine:IX86 /out:"test/my_testR.exe" /libpath:"../lib" /libpath:"../../../HordubalClientDll/lib"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "my_test___Win32_Debug"
# PROP BASE Intermediate_Dir "my_test___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "test"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\include" /I "marina" /I "..\..\mtl-2.1.2-20" /I "..\..\..\HordubalClientDll\inc" /I "..\..\.." /D "WIN32" /D "_DEBUG" /D "_MSVC" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"test/my_testD.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Comctl32.lib kernel32.lib user32.lib gdi32.lib OpenGL32.lib Glu32.lib odeD.lib drawstuffD.lib HordubalClientDllD.lib /nologo /subsystem:console /debug /machine:I386 /out:"test/my_testD.exe" /pdbtype:sept /libpath:"..\lib" /libpath:"../../../HordubalClientDll/lib"

!ENDIF 

# Begin Target

# Name "my_test - Win32 Release"
# Name "my_test - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\test\my_test.cpp
DEP_CPP_MY_TE=\
	"..\..\..\Hordubal\inc\message.h"\
	"..\..\..\HordubalClientDll\inc\HordubalClientDll.h"\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	".\marina\BodyGroup.h"\
	".\marina\BoxCollision.h"\
	".\marina\Geometry.h"\
	".\marina\Logging.h"\
	".\marina\marina.hpp"\
	".\marina\Math3D.h"\
	".\marina\Math3D.inl"\
	".\marina\MatrixA.hpp"\
	".\marina\my_assert.h"\
	".\marina\MyArray.hpp"\
	".\marina\MyArrayWithDelete.h"\
	".\marina\MyMass.h"\
	".\marina\Quaternion.h"\
	".\marina\Quaternion.inl"\
	".\marina\RestContactResolver.hpp"\
	".\marina\RigidBody.h"\
	".\marina\RigidBodyState.h"\
	".\marina\Simulation.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

# SUBTRACT CPP /YX

!ENDIF 

# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=..\drawstuff\src\resources.rc
# End Source File
# End Group
# Begin Group "marina"

# PROP Default_Filter ""
# Begin Group "src"

# PROP Default_Filter "cpp"
# Begin Source File

SOURCE=.\marina\BodyGroup.cpp
DEP_CPP_BODYG=\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	".\marina\BodyGroup.h"\
	".\marina\Logging.h"\
	".\marina\marina.hpp"\
	".\marina\Math3D.h"\
	".\marina\Math3D.inl"\
	".\marina\MatrixA.hpp"\
	".\marina\my_assert.h"\
	".\marina\MyArray.hpp"\
	".\marina\MyArrayWithDelete.h"\
	".\marina\Quaternion.h"\
	".\marina\Quaternion.inl"\
	".\marina\RigidBody.h"\
	".\marina\RigidBodyState.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\BoxCollision.cpp
DEP_CPP_BOXCO=\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	".\marina\BoxCollision.h"\
	".\marina\Logging.h"\
	".\marina\marina.hpp"\
	".\marina\Math2D.h"\
	".\marina\Math2D.inl"\
	".\marina\Math3D.h"\
	".\marina\Math3D.inl"\
	".\marina\MatrixA.hpp"\
	".\marina\my_assert.h"\
	".\marina\MyArray.hpp"\
	".\marina\MyArrayWithDelete.h"\
	".\marina\Quaternion.h"\
	".\marina\Quaternion.inl"\
	".\marina\RestContactResolver.hpp"\
	".\marina\RigidBody.h"\
	".\marina\RigidBodyState.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\Geometry.cpp
DEP_CPP_GEOME=\
	"..\..\..\Hordubal\inc\message.h"\
	"..\..\..\HordubalClientDll\inc\HordubalClientDll.h"\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	".\marina\BoxCollision.h"\
	".\marina\Geometry.h"\
	".\marina\Logging.h"\
	".\marina\marina.hpp"\
	".\marina\Math3D.h"\
	".\marina\Math3D.inl"\
	".\marina\MatrixA.hpp"\
	".\marina\my_assert.h"\
	".\marina\MyArray.hpp"\
	".\marina\MyArrayWithDelete.h"\
	".\marina\Quaternion.h"\
	".\marina\Quaternion.inl"\
	".\marina\RestContactResolver.hpp"\
	".\marina\RigidBody.h"\
	".\marina\RigidBodyState.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\Logging.cpp
DEP_CPP_LOGGI=\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	".\marina\Logging.h"\
	".\marina\my_assert.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\mtl_lu_solve.cpp
DEP_CPP_MTL_L=\
	"..\..\mtl-2.1.2-20\mtl\abs.h"\
	"..\..\mtl-2.1.2-20\mtl\array2D.h"\
	"..\..\mtl-2.1.2-20\mtl\banded_indexer.h"\
	"..\..\mtl-2.1.2-20\mtl\blais.h"\
	"..\..\mtl-2.1.2-20\mtl\block2D.h"\
	"..\..\mtl-2.1.2-20\mtl\compressed1D.h"\
	"..\..\mtl-2.1.2-20\mtl\compressed2D.h"\
	"..\..\mtl-2.1.2-20\mtl\compressed_iter.h"\
	"..\..\mtl-2.1.2-20\mtl\conj.h"\
	"..\..\mtl-2.1.2-20\mtl\dense1D.h"\
	"..\..\mtl-2.1.2-20\mtl\dense2D.h"\
	"..\..\mtl-2.1.2-20\mtl\dense_iterator.h"\
	"..\..\mtl-2.1.2-20\mtl\diagonal_indexer.h"\
	"..\..\mtl-2.1.2-20\mtl\dim_calc.h"\
	"..\..\mtl-2.1.2-20\mtl\dimension.h"\
	"..\..\mtl-2.1.2-20\mtl\entry.h"\
	"..\..\mtl-2.1.2-20\mtl\envelope2D.h"\
	"..\..\mtl-2.1.2-20\mtl\external_vector.h"\
	"..\..\mtl-2.1.2-20\mtl\fast.h"\
	"..\..\mtl-2.1.2-20\mtl\initialize.h"\
	"..\..\mtl-2.1.2-20\mtl\iterator_adaptor.h"\
	"..\..\mtl-2.1.2-20\mtl\light1D.h"\
	"..\..\mtl-2.1.2-20\mtl\light_matrix.h"\
	"..\..\mtl-2.1.2-20\mtl\linalg_vec.h"\
	"..\..\mtl-2.1.2-20\mtl\lu.h"\
	"..\..\mtl-2.1.2-20\mtl\matrix.h"\
	"..\..\mtl-2.1.2-20\mtl\matrix_implementation.h"\
	"..\..\mtl-2.1.2-20\mtl\matrix_stream.h"\
	"..\..\mtl-2.1.2-20\mtl\matrix_traits.h"\
	"..\..\mtl-2.1.2-20\mtl\meta_equal.h"\
	"..\..\mtl-2.1.2-20\mtl\meta_if.h"\
	"..\..\mtl-2.1.2-20\mtl\mtl.h"\
	"..\..\mtl-2.1.2-20\mtl\mtl_algo.h"\
	"..\..\mtl-2.1.2-20\mtl\mtl_complex.h"\
	"..\..\mtl-2.1.2-20\mtl\mtl_config.h"\
	"..\..\mtl-2.1.2-20\mtl\mtl_exception.h"\
	"..\..\mtl-2.1.2-20\mtl\mtl_iterator.h"\
	"..\..\mtl-2.1.2-20\mtl\mtl_limits.h"\
	"..\..\mtl-2.1.2-20\mtl\mtl_set.h"\
	"..\..\mtl-2.1.2-20\mtl\not_at.h"\
	"..\..\mtl-2.1.2-20\mtl\oned_part.h"\
	"..\..\mtl-2.1.2-20\mtl\orien.h"\
	"..\..\mtl-2.1.2-20\mtl\partition.h"\
	"..\..\mtl-2.1.2-20\mtl\rect_indexer.h"\
	"..\..\mtl-2.1.2-20\mtl\refcnt_ptr.h"\
	"..\..\mtl-2.1.2-20\mtl\reverse_iter.h"\
	"..\..\mtl-2.1.2-20\mtl\scale_iterator.h"\
	"..\..\mtl-2.1.2-20\mtl\scaled1D.h"\
	"..\..\mtl-2.1.2-20\mtl\scaled2D.h"\
	"..\..\mtl-2.1.2-20\mtl\sparse1D.h"\
	"..\..\mtl-2.1.2-20\mtl\sparse_iterator.h"\
	"..\..\mtl-2.1.2-20\mtl\strided1D.h"\
	"..\..\mtl-2.1.2-20\mtl\strided_iterator.h"\
	"..\..\mtl-2.1.2-20\mtl\transform_iterator.h"\
	"..\..\mtl-2.1.2-20\mtl\uplo.h"\
	"..\..\mtl-2.1.2-20\mtl\utils.h"\
	
NODEP_CPP_MTL_L=\
	"..\..\mtl-2.1.2-20\mtl\contrib\double_double\double_double.h"\
	"..\..\mtl-2.1.2-20\mtl\mtl\concept_checks.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\MyMass.cpp
DEP_CPP_MYMAS=\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	".\marina\Logging.h"\
	".\marina\marina.hpp"\
	".\marina\Math3D.h"\
	".\marina\Math3D.inl"\
	".\marina\MatrixA.hpp"\
	".\marina\my_assert.h"\
	".\marina\MyArray.hpp"\
	".\marina\MyArrayWithDelete.h"\
	".\marina\MyMass.h"\
	".\marina\Quaternion.h"\
	".\marina\Quaternion.inl"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\RestContactResolver.cpp
DEP_CPP_RESTC=\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	".\marina\BodyGroup.h"\
	".\marina\Logging.h"\
	".\marina\marina.hpp"\
	".\marina\Math3D.h"\
	".\marina\Math3D.inl"\
	".\marina\MatrixA.hpp"\
	".\marina\my_assert.h"\
	".\marina\MyArray.hpp"\
	".\marina\MyArrayWithDelete.h"\
	".\marina\Quaternion.h"\
	".\marina\Quaternion.inl"\
	".\marina\RestContactResolver.hpp"\
	".\marina\RigidBody.h"\
	".\marina\RigidBodyState.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\RigidBody.cpp
DEP_CPP_RIGID=\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	".\marina\Logging.h"\
	".\marina\marina.hpp"\
	".\marina\Math3D.h"\
	".\marina\Math3D.inl"\
	".\marina\MatrixA.hpp"\
	".\marina\my_assert.h"\
	".\marina\MyArray.hpp"\
	".\marina\MyArrayWithDelete.h"\
	".\marina\Quaternion.h"\
	".\marina\Quaternion.inl"\
	".\marina\RigidBody.h"\
	".\marina\RigidBodyState.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\RigidBodyState.cpp
DEP_CPP_RIGIDB=\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	".\marina\Logging.h"\
	".\marina\marina.hpp"\
	".\marina\Math3D.h"\
	".\marina\Math3D.inl"\
	".\marina\MatrixA.hpp"\
	".\marina\my_assert.h"\
	".\marina\MyArray.hpp"\
	".\marina\MyArrayWithDelete.h"\
	".\marina\Quaternion.h"\
	".\marina\Quaternion.inl"\
	".\marina\RigidBodyState.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\Simulation.cpp
DEP_CPP_SIMUL=\
	"..\..\..\Hordubal\inc\message.h"\
	"..\..\..\HordubalClientDll\inc\HordubalClientDll.h"\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	"..\include\ode\common.h"\
	"..\include\ode\config.h"\
	"..\include\ode\contact.h"\
	"..\include\ode\error.h"\
	"..\include\ode\geom.h"\
	"..\include\ode\mass.h"\
	"..\include\ode\matrix.h"\
	"..\include\ode\memory.h"\
	"..\include\ode\misc.h"\
	"..\include\ode\objects.h"\
	"..\include\ode\ode.h"\
	"..\include\ode\odecpp.h"\
	"..\include\ode\odemath.h"\
	"..\include\ode\rotation.h"\
	"..\include\ode\space.h"\
	"..\include\ode\timer.h"\
	".\marina\BodyGroup.h"\
	".\marina\Geometry.h"\
	".\marina\Logging.h"\
	".\marina\marina.hpp"\
	".\marina\Math3D.h"\
	".\marina\Math3D.inl"\
	".\marina\MatrixA.hpp"\
	".\marina\my_assert.h"\
	".\marina\MyArray.hpp"\
	".\marina\MyArrayWithDelete.h"\
	".\marina\Quaternion.h"\
	".\marina\Quaternion.inl"\
	".\marina\RestContactResolver.hpp"\
	".\marina\RigidBody.h"\
	".\marina\RigidBodyState.h"\
	".\marina\Simulation.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

# ADD CPP /Yu"stdafx.h"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\marina\stdafx.cpp
DEP_CPP_STDAF=\
	"..\include\drawstuff\drawstuff.h"\
	"..\include\drawstuff\version.h"\
	".\marina\Logging.h"\
	".\marina\my_assert.h"\
	".\marina\stdafx.h"\
	

!IF  "$(CFG)" == "my_test - Win32 Release"

# ADD CPP /Yc"stdafx.h"

!ELSEIF  "$(CFG)" == "my_test - Win32 Debug"

!ENDIF 

# End Source File
# End Group
# Begin Group "inc"

# PROP Default_Filter "h,hpp,inl"
# Begin Source File

SOURCE=.\marina\BodyGroup.h
# End Source File
# Begin Source File

SOURCE=.\marina\BoxCollision.h
# End Source File
# Begin Source File

SOURCE=.\marina\Geometry.h
# End Source File
# Begin Source File

SOURCE=.\marina\Logging.h
# End Source File
# Begin Source File

SOURCE=.\marina\marina.hpp
# End Source File
# Begin Source File

SOURCE=.\marina\Math2D.h
# End Source File
# Begin Source File

SOURCE=.\marina\Math2D.inl
# End Source File
# Begin Source File

SOURCE=.\marina\Math3D.h
# End Source File
# Begin Source File

SOURCE=.\marina\Math3D.inl
# End Source File
# Begin Source File

SOURCE=.\marina\MatrixA.hpp
# End Source File
# Begin Source File

SOURCE=.\marina\my_assert.h
# End Source File
# Begin Source File

SOURCE=.\marina\MyArray.hpp
# End Source File
# Begin Source File

SOURCE=.\marina\MyArrayWithDelete.h
# End Source File
# Begin Source File

SOURCE=.\marina\MyMass.h
# End Source File
# Begin Source File

SOURCE=.\marina\Quaternion.h
# End Source File
# Begin Source File

SOURCE=.\marina\Quaternion.inl
# End Source File
# Begin Source File

SOURCE=.\marina\RestContactResolver.hpp
# End Source File
# Begin Source File

SOURCE=.\marina\RigidBody.h
# End Source File
# Begin Source File

SOURCE=.\marina\RigidBodyState.h
# End Source File
# Begin Source File

SOURCE=.\marina\Simulation.h
# End Source File
# Begin Source File

SOURCE=.\marina\stdafx.h
# End Source File
# End Group
# End Group
# End Target
# End Project
