#include <stdio.h>

#if (_MSC_VER < 1300 )
#include "mtl/mtl.h"    
#include "mtl/lu.h"
#else
#include "mtl_vs7/mtl.h"
#include "mtl_vs7/lu.h"
#endif

//#include <mtl/matrix.h>
//#include <mtl/mtl.h>
//#include <mtl/utils.h>
//#include <mtl/lu.h>

using namespace mtl ;


typedef matrix<float, rectangle<>, dense<>, row_major>::type MatrixMTL;
typedef dense1D<float> VectorMTL;

void testLUSoln(const MatrixMTL &A, const VectorMTL &b, VectorMTL &x)
{
  // create LU decomposition
  MatrixMTL LU(A.nrows(), A.ncols());
  dense1D<int> pvector(A.nrows());

  copy(A, LU);
  lu_factorize(LU, pvector);
        
  // solve
  lu_solve(LU, pvector, b, x);
}

void SolveNxN(unsigned int n, unsigned int lda, const float * pMatrix, float * pVector)
{
	MatrixMTL cMatrix(n,n);
	VectorMTL cIn(n), cOut(n);
	
	for(unsigned int iX = 0; iX < n; iX ++)
	{
		cIn[iX] = pVector[iX];
		for(unsigned int iY = 0; iY < n; iY++)
			cMatrix(iX,iY) = pMatrix[iX * lda + iY];
	}
	
	testLUSoln(cMatrix, cIn, cOut);
	
	for(iX = 0; iX < n; iX ++)
	{
		pVector[iX] = cOut[iX];
	}
}
