#ifdef _MSC_VER
#pragma once
#endif

#ifndef __CHOLESKY_H__
#define __CHOLESKY_H__ 


#include "../marina.hpp"
#include "../inc.h"
#include "core.hpp"

#define _CALC_ZEROS 0
#if _CALC_ZEROS
extern int g_oper;
extern int g_zeros;
#endif

class CholeskyMatrix
{
protected:
  int _n;
  int _nmax;

  float * _data;
  float * _invsqrt;
  int * _indexes;
  
  void Destroy();

public:
  CholeskyMatrix():_n(0), _nmax(0), _data(NULL), _indexes(NULL), _invsqrt(NULL) {};
  ~CholeskyMatrix() {Destroy();};

  bool Init(int n);
  bool Enlarge(int n);
  void ValidSize(int n) {_n = n;};

  bool Decomp(int n, Matrix<float>& mat, int * indexes);

  bool AddIndex(int index, Matrix<float>& mat);
  void DelIndex(int coord, int *_indexes, int n, Matrix<float>& mat);

  void Solve(M_VECTOR& out, const M_VECTOR& in, Matrix<float>& mat);

  float& Set(int i, int j) {return _data[i*(i+1)/2 + j];};
  float Get(int i, int j) const {return _data[i*(i+1)/2 + j];};

  bool Test(Matrix<float>& mat);
};

void CholeskySoln(Vector<float>& out, int n, int * indexes, Matrix<float> matrix, Vector<float> b);

class LUMatrix
{
protected:
  int _n;
  int _nmax;

  float * _dataL;
  float * _dataU;

  float * _invdiag;
  int * _indexes;

  int * _firstZerosL;
  int * _firstZerosU;

  int _firstNonSymetry;

  void Destroy();

public:

#if _CALC_ZEROS
  int _oper;
  int _zeroOper;
#endif

  LUMatrix():_n(0), _nmax(0), _dataL(NULL), _dataU(NULL), _indexes(NULL), _invdiag(NULL), _firstZerosL(NULL), _firstZerosU(NULL) {};
  ~LUMatrix() {Destroy();};

  bool Init(int n);
  bool Enlarge(int n);
  void ValidSize(int n) {_n = n; if (_firstNonSymetry >= n) _firstNonSymetry = _nmax + 1;};

  bool Decomp(int n, Matrix<float>& mat, int * indexes);

  bool AddIndex(int index, Matrix<float>& mat);
  bool AddIndex2(int index, Matrix<float>& mat);
  bool AddIndex3(int index, int lastSymetric, Matrix<float>& mat, bool symetric);
  void DelIndex(int coord, int *_indexes, int n, Matrix<float>& mat);

  void Solve(M_VECTOR& out, const M_VECTOR& in, Matrix<float>& mat);
  void Solve(M_VECTOR& inout, Matrix<float>& mat);
  void Solve2(M_VECTOR& inout, Matrix<float>& mat);
  void Solve3(M_VECTOR& inout, int lastSymetric, Matrix<float>& mat);

  float& SetL(int i, int j) {return _dataL[i*(i+1)/2 + j];};
  float GetL(int i, int j) const {return _dataL[i*(i+1)/2 + j];};
  float * GetLRow(int i) const {return _dataL + i*(i+1)/2;};
  

  float& SetU(int i, int j) {return _dataU[j*(j+1)/2 + i];};
  float GetU(int i, int j) const {return _dataU[j*(j+1)/2 + i];};
  float * GetUColumn(int i) const {return _dataU + i*(i+1)/2;};

  bool Test(Matrix<float>& mat);
  bool TestSymetry(Matrix<float>& mat);
};

void LUNewSoln(Vector<float>& out, int n, int * indexes, Matrix<float> matrix, Vector<float> b);


struct SparseRowTraits
{
static float Get(const float * data, int i)
  {    
    int j = 0;
    const float * pData = data;
    while (true) {
      j+= *((int*) pData);
      if (i < j)
        return 0.0f; //zero

      int non = *((int*) ++pData);
      ++pData;
      if (i < j + non)      
        //nonzero;
        return pData[i - j]; //zero      
      j += non; 
      pData += non;
    }
  }

  static int Create(float * data, const float * val, int n)
  {
    PROFILE_SCOPE_MARINA(create);
    int i = 0;
    float * pDataBeg = data;   
    while( i < n)
    {
      //find zeros
      int j = i;
      while(j < n && *val++ == 0)
        j++;
      
      *data++ = j - i;

      i = j;      
      // find nonzeros     
      j++;
      const float * noZeroStart = val - 1;
      while(j < n && *val++ != 0)              
        j++;
      val--;

      j -= i;
      *data++ = j;      

      memcpy(data,noZeroStart, sizeof(*val) * j);      
      data += j;

      i += j;
    }
    return data - pDataBeg;
  }

static float TimesRow(const float * data1, const float * data2, int length)
  {    
    PROFILE_SCOPE_MARINA(timesRow);
    float ret = 0;

    const float * pData1 = data1;
    const float * pData2 = data2;

    int i1 = 0;
    int i2 = 0;

    while (true)
    {
      int zeroend1 = i1 + *((int *) pData1);
      int zeroend2 = i2 + *((int *) pData2);
      if (zeroend1 <= zeroend2)
      {
        //switch states
        int itemp = zeroend1;
        zeroend1 = zeroend2;
        zeroend2 = itemp;

        itemp = i1;
        i1 = i2;
        i2 = itemp;

        const float * ptemp = pData1;
        pData1 = pData2;
        pData2 = ptemp;
      }

      int nonzeros2 = *((int *) (pData2 + 1));
      if (zeroend2 + nonzeros2 <= zeroend1)
      {
        pData2 += nonzeros2 + 1;
        i2 = zeroend2 + nonzeros2;
        if (i2 >= length)
          return ret;

        continue;
      }        

      int nonzeroend1 = zeroend1 + *((int *) pData1 + 1);
      int nonzeroend2 = zeroend2 + *((int *) pData2 + 1);

      if (nonzeroend1 > nonzeroend2)
      {
        const float *pData1Temp = pData1 + 2;
        nonzeroend2 = max(nonzeroend2, length);
        const float *pData1TempEnd = pData1Temp + nonzeroend2 - zeroend1;

        ++pData2;

        for(; pData1Temp < pData1TempEnd; pData1Temp ++,pData2++)
          ret += *pData1Temp * *pData2;

        i2 = nonzeroend2;
        if (i2 >= length)
          return ret;

        continue;
      }
      else
      {
        const float *pData2Temp = pData2 + 2 + zeroend1 - zeroend2;
        nonzeroend1 = max(nonzeroend1, length);
        const float *pData2TempEnd = pData2Temp + nonzeroend1 - zeroend1;

        pData1++;

        for(; pData2Temp < pData2TempEnd; pData2Temp ++,pData1++)
          ret += *pData2Temp * *pData1;

        i1 = nonzeroend1;
        if (i1 >= length)
          return ret;

        continue;
      }
    }
  }
  
 static  float TimesVector(const float * data, const float * vec, int n)
  {
    //PROFILE_SCOPE_MARINA(timesVector);    
    float ret = 0;
    int i = 0;
    int j = 0;    

    while(j < n)
    {
      j += (int) data[i];
      i++;
      int jnext = j + (int) data[i];
      i++;
      if (jnext > n)
        jnext = n;
  
      for(; j < jnext;j++,i++)
        ret += data[i] * vec[j];      
    }

    return ret;
  }
  static int MaxLenght(int length) {return 2 * length + 1;};
  
};

template<class RowTraits>
class MatrixTemplate
{
protected:
  int _n;
  int _nmax;
  float ** _rows;

  int _nData;
  int _nmaxData;
  
  float * _data;
  
public:
  MatrixTemplate() : _n(0), _nmax(0), _rows(NULL), _nData(0), _nmaxData(0), _data(NULL) {};
  virtual ~MatrixTemplate() {Destroy();};

  void Init(int n);
  void Destroy();

  void Enlarge(int n);
  void EnlargeData(int n);

  void SetValidSize(int n); 

  float Get(int i, int j) const {return RowTraits::Get(_rows[i],j);};
  float * GetRow(int i) {return _rows[i];};
  float RowTimesRow(int i, const float * data, int length) {return RowTraits::TimesRow(_rows[i],data,length);};
  float RowTimesVector(int i, const float * data, int length) {return RowTraits::TimesVector(_rows[i],data,length);};

  void AddRow( const float *  data, int length);
};


template<class RowTraits>
void MatrixTemplate<RowTraits>::Destroy()
{
  delete [] _data;
  delete [] _rows;
  _n = _nmax = 0;
  _nData = _nmaxData = 0;  
}

template<class RowTraits>
void MatrixTemplate<RowTraits>::Init(int n)
{
  if (n > _nmax)
  {  
     Destroy();
    _data = new float[n * n];
    _rows = new float *[n];
        
    _nmax = n;
    _nmaxData = n * n;
  }

  _n = 0;
  _nData = 0;  

  return;
}

template<class RowTraits>
void MatrixTemplate<RowTraits>::EnlargeData(int n)
{
  if (n <= _nmaxData)
    return;

  float * data = new float[n];
  memcpy(data, _data, sizeof(*data) * _nData);

  delete [] _data;
  _data = data;
  _nmaxData = n;
}

  
template<class RowTraits>
void MatrixTemplate<RowTraits>::Enlarge(int n)
{
  if (n <= _nmax)
    return;

  float * dataOld = _data;
  EnlargeData(n * n);

  float ** rows = new float *[n];
  _nmax = _n;

  for(int i = 0; i < _n; i++)  
    rows[i] = _data + (_rows[i] - dataOld);

  delete [] _rows;
  _rows = rows;
}

template<class RowTraits>
void MatrixTemplate<RowTraits>::SetValidSize(int n)
{
  ASSERT(_n >= n);   
  if (_n > n)
  {
    _n = n;
    _nData = _rows[n] - _data;
  }
}

template<class RowTraits>
void MatrixTemplate<RowTraits>::AddRow(const float * val, int length)
{
  int addSize = RowTraits::MaxLenght(length);
  
  Enlarge(_n + 1);
  EnlargeData(_nData + addSize);
  
  addSize = RowTraits::Create(_data + _nData, val, length);
  _rows[_n++] = _data + _nData;
  _nData += addSize;
}

class LUSparseMatrix
{
protected:
  int _n;
  int _nmax;

  MatrixTemplate<SparseRowTraits> _matL;
  MatrixTemplate<SparseRowTraits> _matU;

  float * _invdiag;
  int * _indexes;

  void Destroy();

public:

  LUSparseMatrix():_n(0), _nmax(0), _indexes(NULL), _invdiag(NULL) {};
  ~LUSparseMatrix() {Destroy();};

  bool Init(int n); 
  bool Enlarge(int n);
  void ValidSize(int n) {_n = n;_matL.SetValidSize(n); _matU.SetValidSize(n);};

  bool Decomp(int n, Matrix<float>& mat, int * indexes);

  bool AddIndex(int index, Matrix<float>& mat);
  void DelIndex(int coord, int *_indexes, int n, Matrix<float>& mat);

  void Solve(M_VECTOR& out, const M_VECTOR& in, Matrix<float>& mat);
  void Solve(M_VECTOR& inout, Matrix<float>& mat);

  bool Test(Matrix<float>& mat);
};

#endif
