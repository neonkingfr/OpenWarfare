#ifndef _MARINA_PROJECT 
#include <Poseidon/lib/wpch.hpp>
#else
#define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES


#include "Cholesky.hpp"

#if _CALC_ZEROS
int g_oper;
int g_zeros;
#endif

void CholeskyMatrix::Destroy()
{
  delete [] _data;
  delete [] _indexes;
  delete [] _invsqrt;
  _data = NULL;
  _invsqrt = NULL;
  _indexes = NULL;
  _n = _nmax = 0;
}

bool CholeskyMatrix::Init(int n)
{
  if (n > _nmax)
  {  
    Destroy();
    _data = new float[n * (n + 1) / 2];
    _indexes = new int[n];
    _invsqrt = new float[n];

    if(_data == NULL || _indexes == NULL || _invsqrt == NULL)
    {
      Destroy();
      return false;
    }
  }
  _nmax = n;
  _n = 0;
  return true;
}

bool CholeskyMatrix::Enlarge(int n)
{
  if (n <= _nmax)
    return true;

  // alloc more
  _nmax = n;
  float * data = new float[_nmax * (_nmax + 1) / 2];
  int * indexes = new int[_nmax];
  float * sqrtTemp = new float[_nmax];

  if (data == NULL || indexes == NULL)
  {
    delete [] data;
    delete [] indexes;
    delete [] sqrtTemp;
    return false;
  }

  memcpy(sqrtTemp, _invsqrt, sizeof(*_invsqrt) * _n);
  memcpy(indexes, _indexes, sizeof(*_indexes) * _n);
  memcpy(data, _data, sizeof(*_data) * _n * (_n + 1) / 2);

  delete [] _data;
  delete [] _indexes;
  delete [] _invsqrt;

  _data = data;
  _indexes = indexes;
  _invsqrt = sqrtTemp;

   return true;
}

bool CholeskyMatrix::AddIndex(int index, Matrix<float>& mat)
{
  if (_n == _nmax)
  {
    if (!Enlarge(_nmax + 10))
      return false;    
  }

  if (_n == 0)
  {    
    Set(0,0) = sqrt(mat.GetMemberValue(index,index));
    _invsqrt[0] = 1/Get(0,0);
    _indexes[0] = index;
    _n = 1;
    return true;
  }
 
  for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = 0; j < i; j++)        
      temp -= Get(i,j) * Get(_n,j);
    
    temp += mat.GetMemberValue(index, _indexes[i]);
    temp *= _invsqrt[i];
    Set(_n,i) = temp;   
  }

  float temp = 0;
  for(int i = 0; i < _n; i++)
    temp -= Get(_n,i) * Get(_n,i);

  temp += mat.GetMemberValue(index, index);
  if (temp <= 0)
  {
    ASSERT(FALSE);
    temp -= temp;
  }
  temp = sqrt(temp);
  Set(_n,_n) = temp;
  _invsqrt[_n] = 1/temp;
  
  _indexes[_n] = index;
  _n++;
  return true;
}

void CholeskyMatrix::DelIndex(int coord, int *_indexes, int n, Matrix<float>& mat)
{
  if (coord < _n)
  {
    _n = coord;
    for(int i = _n; i < n; i++)
      AddIndex( _indexes[i], mat);
  }
}

bool CholeskyMatrix::Decomp(int n, Matrix<float>& mat, int * indexes)
{
  if (!Init(n))
    return false;

  for(int i = 0; i < n; i++)
    AddIndex(indexes[i], mat);

  Test(mat);
  return true;
}

bool CholeskyMatrix::Test(Matrix<float>& mat)
{
  float * test = new float[_n * (_n + 1) / 2];

  for(int i = 0; i < _n; i++)
  {
    for(int j = 0; j <= i; j++)
    {
      test[i * (i + 1) / 2 + j] = 0;
      int to = min(i,j);
      for(int k = 0; k <= to; k++)
      {
        test[i * (i + 1) / 2 + j] += Get(i,k)*Get(j,k);
      }

      float res = test[i * (i + 1) / 2 + j] - mat.GetMemberValue(_indexes[i],_indexes[j]);
      float res2 = test[i * (i + 1) / 2 + j] - mat.GetMemberValue(_indexes[j], _indexes[i]);

      if (res > 0.00001f)
      {
        ASSERT(FALSE);
      }

      if (res2 > 0.00001f)
      {
        ASSERT(FALSE);
      }

    }
  }

  delete [] test;
  return true;
}

void CholeskyMatrix::Solve(M_VECTOR& out, const M_VECTOR& in, Matrix<float>& mat)
{
  M_VECTOR out1;
  out1.Init(in.GetDim());

  for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = 0; j < i; j++)
      temp += out1[j]*Get(i,j);

    out1[i] = (in[i] - temp) / Get(i,i);
  }

  for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = 0; j <= i; j++)
      temp += out1[j] * Get(i,j);

    if (fabs(in[i] - temp) > 0.00001f)
    {
      ASSERT(FALSE);
    }
  }

  for(int i = _n - 1; i >= 0; i--)
  {
    float temp = 0;
    for(int j = _n - 1; j > i; j--)
      temp += out[j] * Get(j,i);

    out[i] = (out1[i] - temp) / Get(i,i);
  }

  for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = i; j < _n; j++)
      temp += out[j] * Get(j,i);

    if (fabs(out1[i] - temp) > 0.00001f)
    {
      ASSERT(FALSE);
    }
  }

  //sicher test
  // L*U*out;
  M_VECTOR out2;
  out2.Init(in.GetDim());
  for(int i = 0; i < _n; i++)
  {
    out2[i] = 0;
    for(int j = i; j < _n; j++)
      out2[i] += Get(j,i) * out[j];
  }


  M_VECTOR in2;
  in2.Init(in.GetDim());
  for(int i = 0; i < _n; i++)
  {
    in2[i] = 0;
    for(int j = 0; j <= i; j++)
      in2[i] += Get(i,j) * out2[j];

    ASSERT(fabs(in2[i] - in[i]) < 0.00001f);
  }
  return;
}

void LUMatrix::Destroy()
{
  delete [] _dataL;
  delete [] _dataU;
  delete [] _indexes;
  delete [] _invdiag;
  _dataL = NULL;
  _dataU = NULL;
  _invdiag = NULL;
  _indexes = NULL;
  _n = _nmax = 0;
}

bool LUMatrix::Init(int n)
{
  if (n > _nmax)
  {  
    Destroy();
    _dataL = new float[n * (n + 1) / 2];
    _dataU = new float[n * (n + 1) / 2];
    _indexes = new int[n];
    _invdiag = new float[n];
    _firstZerosU = new int[n];
    _firstZerosL = new int[n];

    
    _nmax = n;
  }
  
  _firstNonSymetry = _nmax + 1;
  _n = 0;

#if _CALC_ZEROS
  _oper = 0;
  _zeroOper = 0;
#endif

  return true;
}

bool LUMatrix::Enlarge(int n)
{
  if (n < _nmax)
    return true;

  // alloc more
  if (_firstNonSymetry == _nmax + 1)
    _firstNonSymetry = n + 1;

  _nmax = n;
  float * dataL = new float[_nmax * (_nmax + 1) / 2];
  float * dataU = new float[_nmax * (_nmax + 1) / 2];
  int * indexes = new int[_nmax];
  float * invTemp = new float[_nmax];
  int * firstZerosU = new int[n];
  int * firstZerosL = new int[n]; 

  memcpy(invTemp, _invdiag, sizeof(*_invdiag) * _n);
  memcpy(indexes, _indexes, sizeof(*_indexes) * _n);
  memcpy(dataL, _dataL, sizeof(*_dataL) * _n * (_n + 1) / 2);
  memcpy(dataU, _dataU, sizeof(*_dataU) * _n * (_n + 1) / 2);
  memcpy(firstZerosU, _firstZerosU, sizeof(*_firstZerosU) * _n);
  memcpy(firstZerosL, _firstZerosL, sizeof(*_firstZerosL) * _n);

  delete [] _dataL;
  delete [] _dataU;
  delete [] _indexes;
  delete [] _invdiag;
  delete [] _firstZerosU;
  delete [] _firstZerosL;

  _dataL = dataL;
  _dataU = dataU;
  _indexes = indexes;
  _invdiag = invTemp;
  _firstZerosL = firstZerosL;
  _firstZerosU = firstZerosU;

   return true;
}

bool LUMatrix::AddIndex(int index, Matrix<float>& mat)
{
  if (_n == _nmax)
  {
    if (!Enlarge(_nmax + 10))
      return false;    
  }

  if (_n == 0)
  {    
    SetU(0,0) = mat.GetMemberValue(index,index);
    SetL(0,0) = 1;
    _invdiag[0] = 1/GetU(0,0);
    _indexes[0] = index;    
    _n = 1;
    return true;
  }

  //find L

  
  for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = 0; j < i; j++)        
#if _CALC_ZEROS    
    {
      _oper++;
      temp -= GetU(j,i) * GetL(_n,j);
      if (GetU(j,i) == 0 || GetL(_n,j) == 0)      
        _zeroOper++;
    }
#else
      temp -= GetU(j,i) * GetL(_n,j);
#endif

    temp += mat.GetMemberValue(index, _indexes[i]);
    temp *= _invdiag[i];
    SetL(_n,i) = temp;   
  }
  SetL(_n,_n) = 1;

  //find U
 

  for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = 0; j < i; j++)      
#if _CALC_ZEROS    
    {
      _oper++;
      temp -= GetL(i,j) * GetU(j,_n);
      if (GetL(i,j) == 0 || GetU(j,_n) == 0)      
        _zeroOper++;
    }
#else
      temp -= GetL(i,j) * GetU(j,_n);
#endif

    temp += mat.GetMemberValue(_indexes[i], index);    
    SetU(i,_n) = temp;
  }

  float temp = 0;
  for(int i = 0; i < _n; i++)
#if _CALC_ZEROS    
  {
    _oper++;
    temp -= GetL(_n,i) * GetU(i,_n);
    if (GetL(_n,i) == 0 || GetU(i,_n) == 0)      
      _zeroOper++;
  }
#else
    temp -= GetL(_n,i) * GetU(i,_n);
#endif

  temp += mat.GetMemberValue(index, index);  
  SetU(_n,_n) = temp;
  _invdiag[_n] = 1/temp;
  ASSERT(!IsNumericalZero(temp));
  ASSERT(_finite(_invdiag[_n]) && !_isnan(_invdiag[_n]));

  _indexes[_n] = index;
  _n++;
  return true;
}

bool LUMatrix::AddIndex2(int index, Matrix<float>& mat)
{
  if (_n == _nmax)
  {
    if (!Enlarge(_nmax + 10))
      return false;    
  }

  if (_n == 0)
  {    
    SetU(0,0) = mat.GetMemberValue(index,index);
    SetL(0,0) = 1;
    _invdiag[0] = 1/GetU(0,0);
    _indexes[0] = index;
    _n = 1;
    _firstZerosL[0] = 0;
    _firstZerosU[0] = 0;
    return true;
  }

  //find L

  int iFirstZeroes = 0;
  for(;iFirstZeroes < _n && mat.GetMemberValue(index, _indexes[iFirstZeroes]) == 0 ;iFirstZeroes++)
    SetL(_n,iFirstZeroes) = 0;

  _firstZerosL[_n] = iFirstZeroes;
  

  float * row = GetLRow(_n);
  for(int i = iFirstZeroes; i < _n; i++)
  {
    float temp = 0;
    float * column = GetUColumn(i);
    
    for(int j = max(iFirstZeroes,_firstZerosU[i]) ; j < i; j++)        
      temp -= column[j] * row[j];

    temp += mat.GetMemberValue(index, _indexes[i]);
    temp *= _invdiag[i];
    row[i] = temp;   
  }
  row[_n] = 1;

  //find U
  iFirstZeroes = 0;
  for(;iFirstZeroes < _n && mat.GetMemberValue(_indexes[iFirstZeroes], index) == 0 ;iFirstZeroes++)
    SetU(iFirstZeroes,_n) = 0;

  _firstZerosU[_n] = iFirstZeroes;

  float * column = GetUColumn(_n);
  for(int i = iFirstZeroes; i < _n; i++)
  {
    float temp = 0;
    float * row = GetLRow(i);
    for(int j = max(iFirstZeroes,_firstZerosL[i]); j < i; j++)      
      temp -= row[j] * column[j];


    temp += mat.GetMemberValue(_indexes[i], index);    
    column[i] = temp;
  }

  float temp = 0;
  for(int i = 0; i < _n; i++)
    temp -= row[i] * column[i];

  temp += mat.GetMemberValue(index, index);  
  column[_n] = temp;
  _invdiag[_n] = 1/temp;
  ASSERT(_finite(_invdiag[_n]) && !_isnan(_invdiag[_n]));

  _indexes[_n] = index;
  _n++;
  return true;
}

bool LUMatrix::AddIndex3(int index, int lastSymetric, Matrix<float>& mat, bool symetric)
{
  if (_n == _nmax)
  {
    if (!Enlarge(_nmax + 10))
      return false;    
  }

  if (_n == 0)
  {    
    float val;
    if (lastSymetric > 0)
    {    
      val = sqrt(mat.GetMemberValue(index,index));
      SetU(0,0) = val;
      SetL(0,0) = val;
    }
    else
    {
      val = mat.GetMemberValue(index,index);
      SetU(0,0) = val;
      SetL(0,0) = 1;
    }

    _invdiag[0] = 1/val;

    _indexes[0] = index;
    _n = 1;
    _firstZerosL[0] = 0;
    _firstZerosU[0] = 0;
    return true;
  }
  
  int uselastSymetric = min(lastSymetric, _n);

  //find L

  START_PROFILE_MARINA(findnzero);

  int iFirstZeroes = 0;
  for(;iFirstZeroes < _n && mat.GetMemberValue(index, _indexes[iFirstZeroes]) == 0 ;iFirstZeroes++)
    SetL(_n,iFirstZeroes) = 0;

  _firstZerosL[_n] = iFirstZeroes;
  END_PROFILE_MARINA(findnzero);


  START_PROFILE_MARINA(calcrow);
  float * row = GetLRow(_n);
  for(int i = iFirstZeroes; i < _n; i++)
  {
    float temp = 0;
    int j0 = max(iFirstZeroes,_firstZerosU[i]);
    int jend = i - j0;
    float * column = GetUColumn(i) + j0;
    float * rowTemp = row + j0;

    for(int j = 0 ; j < jend; j++)        
      temp -= column[j] * rowTemp[j];

    temp += mat.GetMemberValue(index, _indexes[i]);
    temp *= _invdiag[i];
    row[i] = temp;   
  }
  END_PROFILE_MARINA(calcrow);
  

  //find U
  
  START_PROFILE_MARINA(calccolumn);
  float * column = GetUColumn(_n);
  {  
    START_PROFILE_MARINA(findnzero);
    if (!symetric)        
    {
      iFirstZeroes = 0;
      for(;iFirstZeroes < _n && mat.GetMemberValue(_indexes[iFirstZeroes], index) == 0 ;iFirstZeroes++)
        SetU(iFirstZeroes,_n) = 0;
    }

    memset(column,0x0, iFirstZeroes * sizeof(*column));
    _firstZerosU[_n] = iFirstZeroes;
    END_PROFILE_MARINA(findnzero);
  }


  int i = iFirstZeroes;
  if (symetric)
  {
    if (uselastSymetric > iFirstZeroes)
    {    
      memcpy(column + iFirstZeroes, row + iFirstZeroes, sizeof(*column) * (uselastSymetric - iFirstZeroes));
      i = uselastSymetric;
    }

    //for(; i < uselastSymetric; i++)  
    //  column[i] = row[i];  

    for(; i < _n; i++)
    {
      float temp = 0;
      int j0 = max(iFirstZeroes,_firstZerosL[i]); 
      int jend = i - j0;
      float * row = GetLRow(i) + j0;
      float * columntemp = column + j0;

      for(int j = 0; j < jend; j++)      
        temp -= row[j] * columntemp[j];

      temp += mat.GetMemberValue(_indexes[i], index);     
      column[i] = temp;
    }
  }
  else
  {
    for(; i < _n; i++)
    {
      float temp = 0;
      int j0 = max(iFirstZeroes,_firstZerosL[i]); 
      int jend = i - j0;
      float * row = GetLRow(i) + j0;
      float * columntemp = column + j0;

      for(int j = 0; j < jend; j++)      
        temp -= row[j] * columntemp[j];


      temp += mat.GetMemberValue(_indexes[i], index);     
      column[i] = temp;
      if (i < uselastSymetric)
        column[i] *= _invdiag[i];
    }
  }
  END_PROFILE_MARINA(calccolumn);

  START_PROFILE_MARINA(calcnn);

  float temp = 0;
  int iend = _n - iFirstZeroes;
  float * rowtemp = row + iFirstZeroes;
  float * columntemp = column + iFirstZeroes;
  for(int i =  0; i < iend; i++)
    temp -= rowtemp[i] * columntemp[i];

#if _DEBUG
  float tempBak = temp;
  (void) tempBak;
#endif
  temp += mat.GetMemberValue(index, index);  
  if (lastSymetric <= _n)
  {
    // nonsymetri case
    ASSERT(!IsNumericalZero(temp) || Test(mat));
    if (IsNumericalZero(temp))
    {
      //LogF("critical temp: %f", temp);
      return false;
    }
    column[_n] = temp;
    row[_n] = 1;
    _invdiag[_n] = 1/temp;
    ASSERT(_finite(_invdiag[_n]) && !_isnan(_invdiag[_n]));
    
  }
  else
  {
    //ASSERT(temp > 0.00001f);
    ASSERT(temp > 0 || (Test(mat) && TestSymetry(mat)));
    if (temp < 0.00001f)
    {
      //LogF("critical temp: %f", temp);
      return false;
    }
    

    temp = sqrt(temp);
    ASSERT(_finite(temp) && !_isnan(temp));
    column[_n] = temp;
    row[_n] = temp;
    _invdiag[_n] = 1/temp;
   ASSERT(_finite(_invdiag[_n]) && !_isnan(_invdiag[_n]));

  }
  
  _indexes[_n] = index;
  _n++;

  if (!Test(mat))
  {
    TestSymetry(mat);
  }

  END_PROFILE_MARINA(calcnn);
#if _DEBUG
 // Test(mat);
#endif
  return true;
}


void LUMatrix::DelIndex(int coord, int *_indexes, int n, Matrix<float>& mat)
{
  if (coord < _n)
  {
    _n = coord;
    for(int i = _n; i < n; i++)
      AddIndex( _indexes[i], mat);
  }
}

bool LUMatrix::Decomp(int n, Matrix<float>& mat, int * indexes)
{
  if (!Init(n))
    return false;

  for(int i = 0; i < n; i++)
    AddIndex(indexes[i], mat);

  //Test(mat);
  return true;
}

bool LUMatrix::Test(Matrix<float>& mat)
{
#ifdef _DEBUG
  float * test = new float[_n * _n];

  for(int i = 0; i < _n; i++)
  {
    for(int j = 0; j < _n; j++)
    {
      test[i * _n + j] = 0;
      int to = min(i,j);
      for(int k = 0; k <= to; k++)
      {
        test[i * _n + j] += GetL(i,k)*GetU(k,j);
      }

      float res = test[i * _n + j] - mat.GetMemberValue(_indexes[i],_indexes[j]);      
      if (fabs(res) > 0.0001f)
      {
        ASSERT(FALSE);
        delete [] test;
        return false;
      }
    }
  }

  delete [] test;
  
#endif
  return true;
}

bool LUMatrix::TestSymetry(Matrix<float>& mat)
{
#ifdef _DEBUG
  for(int i = 0; i < _n; i++)
  {
    for(int j = 0; j < _n; j++)
    {
      float res = mat.GetMemberValue(_indexes[j],_indexes[i]) - mat.GetMemberValue(_indexes[i],_indexes[j]);
      if (fabs(res) > 0.00001f)
      {
        ASSERT(FALSE);
      }

      float res2 = GetL(i,j) - GetU(j,i);
      if (fabs(res2) > 0.00001f)
      {
        ASSERT(FALSE);
      }
    }
  }

  
#endif
  return true;
}

void LUMatrix::Solve(M_VECTOR& out, const M_VECTOR& in, Matrix<float>& mat)
{  
  M_VECTOR out1;
  out1.Init(in.GetDim());

  START_PROFILE_MARINA(solveL);
  for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = 0; j < i; j++)
#if _CALC_ZEROS
    {
      _oper++;
      temp += out1[j]*GetL(i,j);
      if (GetL(i,j) == 0)
        _zeroOper++;
    }
#else
      temp += out1[j]*GetL(i,j);
#endif

    out1[i] = in[i] - temp;
  }
  END_PROFILE_MARINA(solveL);

  /*for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = 0; j <= i; j++)
      temp += out1[j] * GetL(i,j);

    if (fabs(in[i] - temp) > 0.00001f)
      ASSERT(FALSE);
  }*/

  START_PROFILE_MARINA(solveU);
  for(int i = _n - 1; i >= 0; i--)
  {
    float temp = 0;
    for(int j = _n - 1; j > i; j--)
#if _CALC_ZEROS
    {
      _oper++;
      temp += out[j] * GetU(i,j);
      if (GetU(i,j) == 0)
        _zeroOper++;
    }
#else
      temp += out[j] * GetU(i,j);
#endif


    out[i] = (out1[i] - temp) * _invdiag[i];
  }
  END_PROFILE_MARINA(solveU);

  /*for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = i; j < _n; j++)
      temp += out[j] * GetU(i,j);

    if (fabs(out1[i] - temp) > 0.00001f)
      ASSERT(FALSE);
  }

  //sicher test
  // L*U*out;
  M_VECTOR out2;
  out2.Init(in.GetDim());
  for(int i = 0; i < _n; i++)
  {
    out2[i] = 0;
    for(int j = i; j < _n; j++)
      out2[i] += GetU(i,j) * out[j];
  }


  M_VECTOR in2;
  in2.Init(in.GetDim());
  for(int i = 0; i < _n; i++)
  {
    in2[i] = 0;
    for(int j = 0; j <= i; j++)
      in2[i] += GetL(i,j) * out2[j];

    ASSERT(fabs(in2[i] - in[i]) < 0.00001f);
  }*/

  return;
}

void LUMatrix::Solve(M_VECTOR& inout, Matrix<float>& mat)
{    
  START_PROFILE_MARINA(solveL);
  for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    for(int j = 0; j < i; j++)
#if _CALC_ZEROS
    {
      _oper++;
      temp += inout[j]*GetL(i,j);
      if (GetL(i,j) == 0)
        _zeroOper++;
    }
#else
      temp += inout[j]*GetL(i,j);
#endif

    inout[i] -= temp;
  }
  END_PROFILE_MARINA(solveL);

  START_PROFILE_MARINA(solveU);
  for(int i = _n - 1; i >= 0; i--)
  {
    float temp = 0;
    for(int j = _n - 1; j > i; j--)
#if _CALC_ZEROS
    {
      _oper++;
      temp += inout[j] * GetU(i,j);
      if (GetU(i,j) == 0)
        _zeroOper++;
    }
#else
      temp += inout[j] * GetU(i,j);
#endif


    inout[i] = (inout[i] - temp) * _invdiag[i];
  }
  END_PROFILE_MARINA(solveU);

 
  return;
}

void LUMatrix::Solve2(M_VECTOR& inout, Matrix<float>& mat)
{    
  START_PROFILE_MARINA(solveL2);
  for(int i = 0; i < _n; i++)
  {
    float temp = 0;
    float * lRow = GetLRow(i);
    float * pInOut = inout.GetPointer();
    for(int j = _firstZerosL[i]; j < i; j++)    
      temp += lRow[j] * pInOut[j];

    inout[i] -= temp;
  }
  END_PROFILE_MARINA(solveL2);

  START_PROFILE_MARINA(solveU2);
  for(int i = _n - 1; i >= 0; i--)
  {
    float temp = 0;
    for(int j = _n - 1; j > i; j--)
      temp += inout[j] * GetU(i,j);

    inout[i] = (inout[i] - temp) * _invdiag[i];
  }
  END_PROFILE_MARINA(solveU2);


  return;
}

void LUMatrix::Solve3(M_VECTOR& inout, int lastSymetric, Matrix<float>& mat)
{    

  START_PROFILE_MARINA(nonzeroL3);
  int iFirstNonZero = 0;
  for(; iFirstNonZero < _n && inout[iFirstNonZero] == 0; iFirstNonZero++);  
  END_PROFILE_MARINA(nonzeroL3);

  START_PROFILE_MARINA(solveL3);
  float * pInOut = inout.GetPointer();
  for(int i = iFirstNonZero; i < _n; i++)
  {
    float temp = 0;
    int j0 = max(_firstZerosL[i], iFirstNonZero);
    int jend = i - j0;

    float * pInOutTemp = pInOut + j0;
    float * lRow = GetLRow(i) + j0;    
    for(int j = 0; j < jend; j++)    
      temp += lRow[j] * pInOutTemp[j];
    
    inout[i] -= temp;
    if (i < lastSymetric)
      inout[i] *= _invdiag[i];
  }
  END_PROFILE_MARINA(solveL3);

  START_PROFILE_MARINA(solveU3);
  static M_VECTOR temp;
  temp.Init(_n);
  float *pTemp = temp.GetPointer();
  for(int i = _n - 1; i >= 0; i--)
  {
    float res = inout[i] = (inout[i] - pTemp[i]) * _invdiag[i];    
    float * uColumn = GetUColumn(i) + _firstZerosU[i];
    float * pTempTemp = pTemp + _firstZerosU[i];
    int jend = i - _firstZerosU[i];
    for(int j = 0; j < jend; j++)          
      pTempTemp[j] += res * uColumn[j];
  }
  END_PROFILE_MARINA(solveU3);

  return;
}

void LUSparseMatrix::Destroy()
{ 
  delete [] _indexes;
  delete [] _invdiag;  
  _invdiag = NULL;
  _indexes = NULL;
  _n = _nmax = 0;
}

bool LUSparseMatrix::Init(int n)
{
  if (n > _nmax)
  {  
    Destroy();
    _matL.Init(n);
    _matU.Init(n);
    
    _indexes = new int[n];
    _invdiag = new float[n];
    
    _nmax = n;
  }

  _n = 0;

  return true;
}

bool LUSparseMatrix::Enlarge(int n)
{
  if (n < _nmax)
    return true;

  // alloc more
  _nmax = n;
  _matL.Enlarge(n);
  _matU.Enlarge(n);

  int * indexes = new int[_nmax];
  float * invTemp = new float[_nmax];


  memcpy(invTemp, _invdiag, sizeof(*_invdiag) * _n);
  memcpy(indexes, _indexes, sizeof(*_indexes) * _n);

  delete [] _indexes;
  delete [] _invdiag;

  _indexes = indexes;
  _invdiag = invTemp;

  return true;
}
M_VECTOR vec;
bool LUSparseMatrix::AddIndex(int index, Matrix<float>& mat)
{
  if (_n == _nmax)
  {
    if (!Enlarge(_nmax + 10))
      return false;    
  }

  if (_n == 0)
  {    
   
    vec.InitNonZero(1);
    vec[0] = mat.GetMemberValue(index,index);;
    _invdiag[0] = 1/vec[0];
    _matU.AddRow(vec.GetPointer(),1);

    vec[0] = 1;
    _matL.AddRow(vec.GetPointer(),1);    
    
    _indexes[0] = index;
    _n = 1;
    return true;
  }

  //find L
  vec.InitNonZero(_n + 1);
  int iFirstZeroes = 0;
  for(;iFirstZeroes < _n && mat.GetMemberValue(index, _indexes[iFirstZeroes]) == 0 ;iFirstZeroes++)
    vec[iFirstZeroes] = 0;


  for(int i = iFirstZeroes; i < _n; i++)
  {
    float temp = -_matU.RowTimesVector(i,vec.GetPointer(),i);

    temp += mat.GetMemberValue(index, _indexes[i]);
    temp *= _invdiag[i];
    vec[i] = temp;   
  }
  vec[_n] = 1;
  _matL.AddRow(vec.GetPointer(), _n + 1);  

  //find U
  iFirstZeroes = 0;
  for(;iFirstZeroes < _n && mat.GetMemberValue(_indexes[iFirstZeroes], index) == 0 ;iFirstZeroes++)
    vec[iFirstZeroes] = 0;

  for(int i = iFirstZeroes; i < _n; i++)
  {
    float temp = -_matL.RowTimesVector(i,vec.GetPointer(),i);
    temp += mat.GetMemberValue(_indexes[i], index);    
    vec[i] = temp;
  }


  float temp =-_matL.RowTimesVector(_n,vec.GetPointer(),_n);
  temp += mat.GetMemberValue(index, index);  
  vec[_n] = temp;

  _matU.AddRow(vec.GetPointer(), _n + 1);
  _invdiag[_n] = 1/temp;

  _indexes[_n] = index;
  _n++;
  return true;
}

void LUSparseMatrix::DelIndex(int coord, int *_indexes, int n, Matrix<float>& mat)
{
  if (coord < _n)
  {
    _n = coord;
    for(int i = _n; i < n; i++)
      AddIndex( _indexes[i], mat);
  }
}

bool LUSparseMatrix::Decomp(int n, Matrix<float>& mat, int * indexes)
{
  if (!Init(n))
    return false;

  for(int i = 0; i < n; i++)
    AddIndex(indexes[i], mat);

  //Test(mat);
  return true;
}

bool LUSparseMatrix::Test(Matrix<float>& mat)
{
  /*float * test = new float[_n * _n];

  for(int i = 0; i < _n; i++)
  {
    for(int j = 0; j < _n; j++)
    {
      test[i * _n + j] = 0;
      int to = min(i,j);
      for(int k = 0; k <= to; k++)
      {
        test[i * _n + j] += GetL(i,k)*GetU(k,j);
      }

      float res = test[i * _n + j] - mat.GetMemberValue(_indexes[i],_indexes[j]);      
      if (res > 0.00001f)
      {
        ASSERT(FALSE);
      }
    }
  }

  delete [] test;*/
  return true;
}

void LUSparseMatrix::Solve(M_VECTOR& out, const M_VECTOR& in, Matrix<float>& mat)
{  
  static M_VECTOR out1;
  out1.Init(in.GetDim());

  START_PROFILE_MARINA(sparsesolveL)
  for(int i = 0; i < _n; i++)
  {
    float temp = _matL.RowTimesVector(i,out1.GetPointer(),i);   
    out1[i] = in[i] - temp;
  }
  END_PROFILE_MARINA(sparsesolveL)

  /*for(int i = 0; i < _n; i++)
  {
  float temp = 0;
  for(int j = 0; j <= i; j++)
  temp += out1[j] * GetL(i,j);

  if (fabs(in[i] - temp) > 0.00001f)
  ASSERT(FALSE);
  }*/

  START_PROFILE_MARINA(sparsesolveU)
  for(int i = _n - 1; i >= 0; i--)
  {
    float temp = 0;
    for(int j = _n - 1; j > i; j--)
      temp += out[j] * _matU.Get(j,i);
    out[i] = (out1[i] - temp) * _invdiag[i];
  }
  END_PROFILE_MARINA(sparsesolveU)

  /*for(int i = 0; i < _n; i++)
  {
  float temp = 0;
  for(int j = i; j < _n; j++)
  temp += out[j] * GetU(i,j);

  if (fabs(out1[i] - temp) > 0.00001f)
  ASSERT(FALSE);
  }

  //sicher test
  // L*U*out;
  M_VECTOR out2;
  out2.Init(in.GetDim());
  for(int i = 0; i < _n; i++)
  {
  out2[i] = 0;
  for(int j = i; j < _n; j++)
  out2[i] += GetU(i,j) * out[j];
  }


  M_VECTOR in2;
  in2.Init(in.GetDim());
  for(int i = 0; i < _n; i++)
  {
  in2[i] = 0;
  for(int j = 0; j <= i; j++)
  in2[i] += GetL(i,j) * out2[j];

  ASSERT(fabs(in2[i] - in[i]) < 0.00001f);
  }*/

  return;
}


void LUSparseMatrix::Solve(M_VECTOR& inout, Matrix<float>& mat)
{  
  //static M_VECTOR out1;
  //out1.Init(in.GetDim());

  START_PROFILE_MARINA(sparsesolveL);
  for(int i = 0; i < _n; i++)
  {
    float temp = _matL.RowTimesVector(i,inout.GetPointer(),i);   
    inout[i] -= temp;
  }
  END_PROFILE_MARINA(sparsesolveL);

  START_PROFILE_MARINA(sparsesolveU);
  for(int i = _n - 1; i >= 0; i--)
  {
    float temp = 0;
    for(int j = _n - 1; j > i; j--)
      temp += inout[j] * _matU.Get(j,i);
    inout[i] = (inout[i] - temp) * _invdiag[i];
  }
  END_PROFILE_MARINA(sparsesolveU);

  return;
}

#endif