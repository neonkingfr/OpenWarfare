#ifndef _MARINA_PROJECT 
#include <Poseidon/lib/wpch.hpp>
#else
#define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES

#include "../marina.hpp"
#include "../inc.h"
#include "Cholesky.hpp"
//#include "Dantzig.hpp"


//#include "BodyGroup.h"
//#include <drawstuff/drawstuff.h>
//#include "f2c.h"

//#define DRAW_MARINA_FORCES
#undef DRAW_MARINA_FORCES

#define BOOL_TO_STRING(a) ((a)?"TRUE":"FALSE")
#define CONTACTID_TO_MATRIXINDEX(i) (_cColumnHeaders[(i) * 3].iMatrixAIndex)

//#define min(a,b) ((a) < (b) ? (a) : (b))

const float DESINGULATOR = 0.0000001f;

LUMatrix g_luMatrix;
LUMatrix g_luMatrix2;
LUMatrix g_luMatrix3;
LUSparseMatrix g_luSparseMatrix;
Matrix<marina_real> g_cMatrixA;


void SolveNxN(unsigned int n, unsigned int lda, const float * pMatrix, float * pVector);
void SolveNxNOld(float * pVector);
void SolvingDone();
//void SerializeColumnHeader(FILE *fArchive, TCOLUMNHEADER& tColumnHeader, BOOL bSave);


__forceinline marina_real m_sqr(marina_real x)
{
  return x*x;
}

// Solves ax^2 + b^x + c = 0
// returns the lowest nonegative solution in x1. If does not exist any it returns a negative value.
// x2 is the second nonnegative solution. If does not exist any it returns a negative value.

void QvadruSolve(marina_real &x1, marina_real &x2, marina_real a, marina_real b, marina_real c)
{
  if (a == 0.0f)
  {
    x1 = -c/b;
    x2 = -1.0f;
    return;
  }

  marina_real fDisc = b*b - 4*a*c;
  if (fDisc < 0)
  {
    x1 = -1.0f;
    x2 = -1.0f;
    return;
  }

  fDisc = sqrtf(fDisc);

  x1 = (-b - fDisc) / (2 * a);
  x2 = (-b + fDisc) / (2 * a);

  marina_real x = umin<marina_real>(x1,x2);
  if (x != x1)
  {
    x2 = x1;
    x1 = x;
  }

  return;
}




BOOL DantzigAlgorithm::ProjectBIntoA()
{ 
  return FALSE;
}

BOOL DantzigAlgorithm::ResolveFirstCollision()
{
  marina_real fMaxAccel = 0.0f;
  unsigned int iMaxAccelIndex = 0;
  for(unsigned int i = 0; i < _nNumberOfForces; i++)
  {
    marina_real fNewAccel = _cVectorB[i];

    const TCOLUMNHEADER& tColumnHeader = *_cMappingToColumnID[i];
    if (tColumnHeader.eForceType != EFT_FRICTION)
    {
      // normal force
      if (fNewAccel < 0.0f && fMaxAccel > fNewAccel)
      {
        fMaxAccel = fNewAccel;
        iMaxAccelIndex = i;       
      }
    } 
  }

  if (IsNumericalZero(fMaxAccel))
  {
    //  Immediately terminate, nothing to do
    return FALSE;
  }

  SwitchCollisions(0, iMaxAccelIndex);

  _cAccelerations[0] = 0.0f; 

  // clamped
  _cForces[0] = -_cVectorB[0]/ (g_cMatrixA.GetMemberValue(0,0));

  _numberOfClamped[_cMappingToColumnID[0]->iNumberOfClamped] = 1;
  if (_cMappingToColumnID[0]->pedestal)
    _cMappingToColumnID[0]->pedestal->AddClamped(_cMappingToColumnID[0]->iContactPointID);
  
  _pnLastIndexInZone[EMZ_CLAMPED] = 1;
  _pnLastIndexInZone[EMZ_NOTCLAMPED] = 1;
  _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] = 1;    
  _pnLastIndexInZone[EMZ_SOLVING] = 1;
  //_pnLastIndexInZone[EMZ_FRICTIONCONST] = 1;
  _pnLastIndexInZone[EMZ_UNSOLVED] = _nNumberOfForces;
  //_pnLastIndexInZone[EMZ_FRICTIONFROMNOTCLAMPED] = _nNumberOfForces;
  //_pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED2] = _nNumberOfForces;

  return TRUE;  
}

/*
void DantzigAlgorithm::BackUp()
{
_cMatrixABackUp = g_cMatrixA;
_cForcesBackUp = _cForces;
_cAccelerationsBackUp = _cAccelerations;
_cMappingToColumnIDBackUp = _cMappingToColumnID;
memcpy(_pnLastIndexInZoneBackUp, _pnLastIndexInZone, sizeof(*_pnLastIndexInZoneBackUp) * EMZ_NUMBER);    
}

void DantzigAlgorithm::Restore()
{
g_cMatrixA = _cMatrixABackUp;
_cForces = _cForcesBackUp;
_cAccelerations = _cAccelerationsBackUp;
_cMappingToColumnID = _cMappingToColumnIDBackUp;
memcpy(_pnLastIndexInZone, _pnLastIndexInZoneBackUp, sizeof(*_pnLastIndexInZoneBackUp) * EMZ_NUMBER);   
}*/


BOOL DantzigAlgorithm::ResolveNextCollision()
{
  //PROFILE_SCOPE_MARINA(next);
  // TODO: rewrite following part. 
#ifdef _ONE_BODY
  Vector<marina_real> cRealAccel1;
  cRealAccel1.Init(_nNumberOfForces);

  ASSERT(_pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] == _pnLastIndexInZone[EMZ_SOLVING]);

  for(unsigned int i = 0; i < _nNumberOfForces; i++)
  {
    cRealAccel1[i] = g_cMatrixA.RowTimesVector(i, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) + 
      _cVectorB[i];     
  }

  Vector<marina_real> cRealAccel2;
  cRealAccel2.Init(_nNumberOfForces);

  Vector3 cForce(0,0,0);
  Vector3 cTorque(0,0,0);
  CRigidBodyBase& cBody = *(_cGroup.Contacts()[0].pcBody1->_physBody);
  unsigned int nNormalClamped = 0;

  for(unsigned int iForce = 0; iForce < _nNumberOfForces; iForce ++)
  {
    const TCOLUMNHEADER & tHeaderTemp = *(_cMappingToColumnID[iForce]);
    const RBContactPoint& cContactPoint = _cGroup.Contacts()[tHeaderTemp.iContactPointID];

    cForce += cContactPoint.dir[tHeaderTemp.iDirIndex] * _cForces[iForce];
    cTorque += cBody.CalculateTorgue(cContactPoint.dir[tHeaderTemp.iDirIndex] * _cForces[iForce], cContactPoint.pos);

    if (tHeaderTemp.eForceType == EFT_STATIC && iForce < _pnLastIndexInZone[EMZ_CLAMPED])
      nNormalClamped++;

  }

  for(unsigned int iForce = 0; iForce < _nNumberOfForces; iForce ++)
  {
    const TCOLUMNHEADER & tHeaderTemp = *(_cMappingToColumnID[iForce]);
    const RBContactPoint& cContactPoint = _cGroup.Contacts()[tHeaderTemp.iContactPointID];

    Vector3 cAccel = cBody.DiffAccelerationAtPointOnForceAndTorque(cContactPoint.pos, cForce, cTorque);
    cRealAccel2[iForce] = cContactPoint.dir[tHeaderTemp.iDirIndex]*(cAccel)
      + _cVectorB[iForce];
  }

#endif
  /// Calculate acceleration at new collision
  /// Let's do pivoting and take the biggest one (It is not clear that it will help)
  /// The first it chooses all normal forces. When all of them are solved it starts with 
  /// friction forces.

  marina_real fMaxAccelNormal = 0.0f;
  marina_real fMaxAccelTangential = 0.0f;
  unsigned int iMaxAccelNormalIndex = 0;
  unsigned int iMaxAccelTangentialIndex = 0;
  bool foundNormal = false;

  ASSERT(_pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] == _pnLastIndexInZone[EMZ_SOLVING]);

  
  START_PROFILE_MARINA(findnext);
  START_PROFILE_MARINA(findnextnormal);
 /* if (_searchNormal)
  for(unsigned int i = _pnLastIndexInZone[EMZ_SOLVING]; i < _pnLastIndexInZone[EMZ_UNSOLVED]; i++)
  {

    const TCOLUMNHEADER& tColumnHeader = *_cMappingToColumnID[i];
    if (tColumnHeader.iNotUseInGlobalCycle < _iGlobalCycle  // TODO: If one of the friction was used refuse both
      && _numberOfClamped[tColumnHeader.iNumberOfClamped] < 6) // if two many clamped there is no space for next one.
    {
      // was not moved into unsolved in last run
      if (tColumnHeader.eForceType != EFT_FRICTION )
      {
        if (_numberOfClampedNormal[tColumnHeader.iNumberOfClampedNormal] >= 3)
          continue;

        PROFILE_SCOPE_MARINA(findnextnormalinner);
        marina_real fNewAccel = g_cMatrixA.RowTimesVector(i, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                 
          _cVectorB[i];

        // normal force
        if (fNewAccel < 0.0f && fMaxAccelNormal > fNewAccel)
        {
          fMaxAccelNormal = fNewAccel;
          iMaxAccelNormalIndex = i;   

          foundNormal = foundNormal || !IsNumericalZero(fMaxAccelNormal/10.0f);
          //if (foundNormal)
          //  break;
        }        
      }
    }
  }*/

  /*if (_searchNormal)
  {  
    int nsolving = _pnLastIndexInZone[EMZ_SOLVING];
    int foundNormalN = 0;
    int iLastUsedNormalTemp = _iLastUsedNormal;
    for(unsigned int i = _iLastUsedNormal + 3; true; i+= 3)
    {
      if (i >= _nNumberOfForces)
        i = 0;

      const TCOLUMNHEADER& tColumnHeader = _cColumnHeaders[i];

      ASSERT(tColumnHeader.eForceType == EFT_STATIC);

      if (tColumnHeader.iMatrixAIndex < nsolving ||
        _numberOfClampedNormal[tColumnHeader.iNumberOfClampedNormal] >= 3 || 
        _numberOfClamped[tColumnHeader.iNumberOfClamped] >= 6 || // if two many clamped there is no space for next one.
        tColumnHeader.iNotUseInGlobalCycle >= _iGlobalCycle)  
      {
        if (i == _iLastUsedNormal)
          break;
        continue;
      }

      // was not moved into unsolved in last run


      PROFILE_SCOPE_MARINA(findnextnormalinner);
      marina_real fNewAccel = g_cMatrixA.RowTimesVector(tColumnHeader.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                 
        _cVectorB[tColumnHeader.iMatrixAIndex];
      // normal force
      if (fNewAccel < 0.0f && !IsNumericalZero(fNewAccel/10.0f))
      {      
        ++foundNormalN;        

        if (fMaxAccelNormal > fNewAccel)
        {
          fMaxAccelNormal = fNewAccel;
          iMaxAccelNormalIndex = tColumnHeader.iMatrixAIndex;   
          iLastUsedNormalTemp = i;
            
          foundNormal = true;
          if (foundNormalN > 10)            
            break;          
        }
      }
      if (i == _iLastUsedNormal)
        break;
    }
    _iLastUsedNormal = iLastUsedNormalTemp;
  }*/

  if (_searchNormal)
  {  
    unsigned int nsolving = _pnLastIndexInZone[EMZ_SOLVING];
    //int foundNormalN = 0;
    unsigned int processingBody = _iLastUsedNormal;    
    while(true) 
    {
      AutoArray<int>& contacts = ContactsOfBody(processingBody);

      for(unsigned int i = 0; i < (unsigned int) contacts.Size(); i++)
      {
        const TCOLUMNHEADER& tColumnHeader = _cColumnHeaders[contacts[i] * 3];

        ASSERT(tColumnHeader.eForceType == EFT_STATIC);

        if (tColumnHeader.iMatrixAIndex < nsolving ||
          (tColumnHeader.pedestal && tColumnHeader.pedestal->nClamped >= 3) ||           
          _numberOfClamped[tColumnHeader.iNumberOfClamped] >= 6 || // if two many clamped there is no space for next one.
          tColumnHeader.iNotUseInGlobalCycle >= _iGlobalCycle)                
          continue;      

        PROFILE_SCOPE_MARINA(findnextnormalinner);
        marina_real fNewAccel = g_cMatrixA.RowTimesVector(tColumnHeader.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                 
          _cVectorB[tColumnHeader.iMatrixAIndex];
        // normal force
        if (fNewAccel < 0.0f && !IsNumericalZero(fNewAccel/10.0f))
        {     
          if (fMaxAccelNormal > fNewAccel)
          {
            fMaxAccelNormal = fNewAccel;
            iMaxAccelNormalIndex = tColumnHeader.iMatrixAIndex;   

            foundNormal = true;          
          }
        }
      }

      if (!foundNormal)
      {
        processingBody++;       
        if (processingBody == _nNumberOfBodies)
          processingBody = 0;
        if (processingBody == _iLastUsedNormal)
          break;
      }
      else
        break;
    }
    _iLastUsedNormal = processingBody;
  }

  END_PROFILE_MARINA(findnextnormal);
  START_PROFILE_MARINA(findnextfriction);

  /*if (!foundNormal)
  {        
    for(unsigned int i = 0; i < _pnLastIndexInZone[EMZ_CLAMPED]; i++)
    {
      const TCOLUMNHEADER& tColumnHeaderNormal = *_cMappingToColumnID[i];

      if (tColumnHeaderNormal.eForceType == EFT_STATIC  &&  tColumnHeaderNormal.iNotClampedIndex == 0
        && _numberOfClamped[tColumnHeaderNormal.iNumberOfClamped] < 6 && _cForces[tColumnHeaderNormal.iMatrixAIndex] > 0) // if two many clamped there is no space for next one.
      {

        const TCOLUMNHEADER& tColumnHeaderFriction1 = *tColumnHeaderNormal.pBrotherForcesHeaders[0];
        const TCOLUMNHEADER& tColumnHeaderFriction2 = *tColumnHeaderNormal.pBrotherForcesHeaders[1];

        // was not moved into unsolved in last run
        if (tColumnHeaderFriction1.iNotUseInGlobalCycle >= _iGlobalCycle || tColumnHeaderFriction2.iNotUseInGlobalCycle >= _iGlobalCycle)
          continue;

        PROFILE_SCOPE_MARINA(findnextfrictioninner);

        if (tColumnHeaderFriction1.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING])
        {                          
          marina_real fNewAccel = g_cMatrixA.RowTimesVector(tColumnHeaderFriction1.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                 
            _cVectorB[tColumnHeaderFriction1.iMatrixAIndex];

          marina_real fWholeAccel = fNewAccel * fNewAccel;
          if (tColumnHeaderFriction2.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING])
          {
            float fAccel2 = g_cMatrixA.RowTimesVector(tColumnHeaderFriction2.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                                
              _cVectorB[tColumnHeaderFriction2.iMatrixAIndex];

            fWholeAccel += fAccel2 * fAccel2;            

            if (fMaxAccelTangential < fWholeAccel) 
            {
              fMaxAccelTangential = fWholeAccel;
              if (fabs(fNewAccel) > fabs(fAccel2))
                iMaxAccelTangentialIndex = tColumnHeaderFriction1.iMatrixAIndex;
              else
                iMaxAccelTangentialIndex = tColumnHeaderFriction2.iMatrixAIndex;

              if (!IsNumericalZero(fMaxAccelTangential/100.0f))
                break;
            }
          }
          else
          {
            if (fMaxAccelTangential < fWholeAccel) 
            {
              fMaxAccelTangential = fWholeAccel;                
              iMaxAccelTangentialIndex = tColumnHeaderFriction1.iMatrixAIndex;

              if (!IsNumericalZero(fMaxAccelTangential/100.0f))
                break;
            }
          }
        }
      }
    }
  }*/

  if (!foundNormal)
  {        
    _iLastUsedFriction = min(_iLastUsedFriction, _pnLastIndexInZone[EMZ_CLAMPED] - 1);
    unsigned int i = _iLastUsedFriction;
    while (true)    
    {
      const TCOLUMNHEADER& tColumnHeaderNormal = *_cMappingToColumnID[i];

      if (tColumnHeaderNormal.eForceType != EFT_STATIC  ||  tColumnHeaderNormal.iNotClampedIndex != 0
        || _numberOfClamped[tColumnHeaderNormal.iNumberOfClamped] >= 6)
      {
        i++;
        if (i == _pnLastIndexInZone[EMZ_CLAMPED])
          i = 0;
        if (i == _iLastUsedFriction)
          break;
        continue;
      }

      // is it force from pedestal?
#if _USE_PEDESTALS
      if (tColumnHeaderNormal.pedestal && tColumnHeaderNormal.pedestal->nClamped == 3)
      {
        if (tColumnHeaderNormal.iContactPointID != tColumnHeaderNormal.pedestal->min)
        {
          i++;
          if (i == _pnLastIndexInZone[EMZ_CLAMPED])
            i = 0;
          if (i == _iLastUsedFriction)
            break;
          continue;
        }
        // use real IDs?

        for(int j = 0; j < 3; j++)
        {
          TCOLUMNHEADER& tHeaderNormal = _cColumnHeaders[tColumnHeaderNormal.pedestal->realClamped[j] * 3];
          const TCOLUMNHEADER& tColumnHeaderFriction1 = *tHeaderNormal.pBrotherForcesHeaders[0];
          const TCOLUMNHEADER& tColumnHeaderFriction2 = *tHeaderNormal.pBrotherForcesHeaders[1];

          if (tHeaderNormal.iNotClampedIndex > 0 || tColumnHeaderFriction1.iNotUseInGlobalCycle >= _iGlobalCycle || tColumnHeaderFriction2.iNotUseInGlobalCycle >= _iGlobalCycle)
            continue;

          PROFILE_SCOPE_MARINA(findnextfrictioninner);

          marina_real fAccel1 = 0;
          if (tColumnHeaderFriction1.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING])
          {                          
            fAccel1 = g_cMatrixA.RowTimesVector(tColumnHeaderFriction1.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +
              _cVectorB[tColumnHeaderFriction1.iMatrixAIndex];
          }

          marina_real fAccel2 = 0;
          if (tColumnHeaderFriction2.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING])
          {                          
            fAccel2 = g_cMatrixA.RowTimesVector(tColumnHeaderFriction2.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                 
              _cVectorB[tColumnHeaderFriction2.iMatrixAIndex];
          }

          marina_real fWholeAccel = fAccel1 * fAccel1 + fAccel2 * fAccel2;          
          if (fMaxAccelTangential < fWholeAccel) 
          {
            fMaxAccelTangential = fWholeAccel;
            if (fabs(fAccel1) > fabs(fAccel2))
              iMaxAccelTangentialIndex = tColumnHeaderFriction1.iMatrixAIndex;
            else
              iMaxAccelTangentialIndex = tColumnHeaderFriction2.iMatrixAIndex;

            if (!IsNumericalZero(fMaxAccelTangential/100.0f))
            {
              //_iLastUsedFriction = i;
              break;
            }
          }
        }

        if (!IsNumericalZero(fMaxAccelTangential/100.0f))
        {
          // we found friction
          _iLastUsedFriction = i;
          break;
        }
      }
      else
#endif
      {
        // not a pedestal
        ASSERT(_cForces[tColumnHeaderNormal.iMatrixAIndex] >= 0)       

        const TCOLUMNHEADER& tColumnHeaderFriction1 = *tColumnHeaderNormal.pBrotherForcesHeaders[0];
        const TCOLUMNHEADER& tColumnHeaderFriction2 = *tColumnHeaderNormal.pBrotherForcesHeaders[1];

        // was not moved into unsolved in last run
        if (tColumnHeaderFriction1.iNotUseInGlobalCycle >= _iGlobalCycle || tColumnHeaderFriction2.iNotUseInGlobalCycle >= _iGlobalCycle)
        {
          i++;
          if (i == _pnLastIndexInZone[EMZ_CLAMPED])
            i = 0;
          if (i == _iLastUsedFriction)
            break;
          continue;
        }

        PROFILE_SCOPE_MARINA(findnextfrictioninner);

        marina_real fAccel1 = 0;
        if (tColumnHeaderFriction1.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING])
        {                          
          fAccel1 = g_cMatrixA.RowTimesVector(tColumnHeaderFriction1.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +
            _cVectorB[tColumnHeaderFriction1.iMatrixAIndex];
        }

        marina_real fAccel2 = 0;
        if (tColumnHeaderFriction2.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING])
        {                          
          fAccel2 = g_cMatrixA.RowTimesVector(tColumnHeaderFriction2.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                 
            _cVectorB[tColumnHeaderFriction2.iMatrixAIndex];
        }

        marina_real fWholeAccel = fAccel1 * fAccel1 + fAccel2 * fAccel2;          
        if (fMaxAccelTangential < fWholeAccel) 
        {
          fMaxAccelTangential = fWholeAccel;
          if (fabs(fAccel1) > fabs(fAccel2))
            iMaxAccelTangentialIndex = tColumnHeaderFriction1.iMatrixAIndex;
          else
            iMaxAccelTangentialIndex = tColumnHeaderFriction2.iMatrixAIndex;

          if (!IsNumericalZero(fMaxAccelTangential/100.0f))
          {
            _iLastUsedFriction = i;
            break;
          }
        }
      }
      
      i++;
      if (i == _pnLastIndexInZone[EMZ_CLAMPED])
        i = 0;

      if (i == _iLastUsedFriction)
        break;
    }   
  }
  
  END_PROFILE_MARINA(findnextfriction);
  END_PROFILE_MARINA(findnext);


  //DebugPoint();
  ////Log("---------------Debug Point %d-----------------------",g_iDebugID);

#ifdef _DEBUG
  //VerifySituation();
#endif
  bool foundFriction = !IsNumericalZero(fMaxAccelTangential/100);
  //foundFriction = false;
  fMaxAccelTangential = sqrtf(fMaxAccelTangential);
  
  //if (!foundNormal)
  //  return FALSE;

  if (!foundNormal && !foundFriction && !_searchNormal)
  {
    _searchNormal = true;
    return TRUE;
  }

  if (!foundNormal)
  {
    

    if (!foundFriction || _iGlobalCycle >  _nNumberOfForces * 3 / 2 || 
      _pnLastIndexInZone[EMZ_CLAMPED] >= _nNumberOfBodies * 6 /*- 6*/)  // 6 degrees of freedom per freebody.
    {
      //  Immediately terminate

      //Log("qwerty: _iGlobalCycle %d, clamped %d, %f, Calculate diffs %d, %f, AverageMatrixSize %d, _iLineDep %d, %f", _iGlobalCycle, _pnLastIndexInZone[EMZ_CLAMPED], _iGlobalCycle / (float) _pnLastIndexInZone[EMZ_CLAMPED],
      //  _iCalledCalculateDiffs, _iCalledCalculateDiffs / (float) _pnLastIndexInZone[EMZ_CLAMPED],
      //  _iMatrixSize / (_iCalledCalculateDiffs == 0 ? 1 : _iCalledCalculateDiffs),
      //  _iLineDep, _iLineDep / (float) _pnLastIndexInZone[EMZ_CLAMPED]);
      return FALSE;
    }

    _searchNormal = false;
  }

  unsigned int nActual;
  if (foundNormal)
  {    
    nActual = iMaxAccelNormalIndex;
    _cAccelerations[iMaxAccelNormalIndex] = fMaxAccelNormal; 
  }
  else
  {    
    nActual = iMaxAccelTangentialIndex;

    // Check if the axes can be changed
    TCOLUMNHEADER& tHeaderFriction1 =  *(_cMappingToColumnID[iMaxAccelTangentialIndex]);
    const TCOLUMNHEADER& tHeaderFriction2 =  *(tHeaderFriction1.pBrotherForcesHeaders[1]);

    if (tHeaderFriction2.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING])
    {
      BOOL  bUseAccelDir = TRUE; 
      if (tHeaderFriction1.cStartDir != VZero)
      {
        bUseAccelDir = FALSE;
        if (tHeaderFriction1.iDirIndex == 1)
          ChangeFrictionAxes(tHeaderFriction1, tHeaderFriction1.cStartDir);
        else
          ChangeFrictionAxes(tHeaderFriction1, Vector3(tHeaderFriction1.cStartDir[1], tHeaderFriction1.cStartDir[0],0));

        _cAccelerations[iMaxAccelTangentialIndex] = g_cMatrixA.RowTimesVector(iMaxAccelTangentialIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                     
          _cVectorB[iMaxAccelTangentialIndex];

        //float fTemp = g_cMatrixA.RowTimesVector(tHeaderFriction2.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                     
        //   _cVectorB[tHeaderFriction2.iMatrixAIndex];

        float fTemp1 = _cAccelerations[iMaxAccelTangentialIndex]/fMaxAccelTangential;
        if (fabsf(fTemp1) < 0.4)
          bUseAccelDir = TRUE;
      }

      if (bUseAccelDir)
      {
        // change friction direction into accelerations direction 
        marina_real fAccel1 = g_cMatrixA.RowTimesVector(tHeaderFriction1.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                    
          _cVectorB[tHeaderFriction1.iMatrixAIndex];
        marina_real fAccel2 = g_cMatrixA.RowTimesVector(tHeaderFriction2.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                    
          _cVectorB[tHeaderFriction2.iMatrixAIndex];

        Vector3 cNewDir(fAccel1,fAccel2,0);
        cNewDir.Normalize();

        ChangeFrictionAxes(tHeaderFriction1, cNewDir);
        _cAccelerations[iMaxAccelTangentialIndex] = g_cMatrixA.RowTimesVector(iMaxAccelTangentialIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                    
          _cVectorB[iMaxAccelTangentialIndex];

#ifdef _DEBUG
        marina_real fTemp1 = g_cMatrixA.RowTimesVector(tHeaderFriction1.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) 
          + _cVectorB[tHeaderFriction1.iMatrixAIndex];
        marina_real fTemp2 = g_cMatrixA.RowTimesVector(tHeaderFriction2.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) 
          + _cVectorB[tHeaderFriction2.iMatrixAIndex];
        (void) fTemp1;
        (void) fTemp2;
#endif

        //  ASSERT(IsNumericalZero(fMaxAccelTangential - (g_cMatrixA.RowTimesVector(tHeaderFriction1.iMatrixAIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) 
        //     + _cVectorB[tHeaderFriction1.iMatrixAIndex])));
      }
    }
    else
      _cAccelerations[iMaxAccelTangentialIndex] = g_cMatrixA.RowTimesVector(iMaxAccelTangentialIndex, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +                
      _cVectorB[iMaxAccelTangentialIndex];
  }
  
  //  Serialize();
  _cForces[nActual] = 0.0f;

  _cReleaseList.Clear();
  if (DriveToZero(nActual,TRUE))
  {
#ifdef _DEBUG
    //IsMatrixARegular();
#endif

    for(int i = 0; i < _cReleaseList.Size(); i++)
    {
      _cReleaseList[i]->bDeadMouse = TRUE;
      DriveToZero(_cReleaseList[i]->iMatrixAIndex, FALSE);
      _cReleaseList[i]->bDeadMouse = FALSE;
    }

    return TRUE;
  }
  else
  {
#ifdef _DEBUG
    IsMatrixARegular();
#endif        
    // co ted ? umbounded found
    if (_bAllowDynamicContact)
    {
      _bAllowDynamicContact = !_bAllowDynamicContact;
      return FALSE;
    }

    // let's solve it later
    return TRUE;
  }
}

BOOL DantzigAlgorithm::DriveToZero(unsigned int iMatrixIndex, // Drive to zero  with force.
                                    BOOL bNewForce) // if FALSE it moves frictin force between unsolved
{
  //DebugPoint();
  
  PROFILE_SCOPE_MARINA(driveToZero);
  _iLocalCycle++;

  if (!bNewForce)
  {
    //Log("DriveToZeroFalse ");
  }

  TCOLUMNHEADER& tColumnHeader = *_cMappingToColumnID[iMatrixIndex];
  TCOLUMNHEADER * ptLastMoved = NULL;

  MoveToZoneEx(iMatrixIndex, EMZ_SOLVING, bNewForce ? EMZ_UNSOLVED : EMZ_CLAMPED);

  BOOL bRecalculate;

  do 
  {
    bRecalculate = FALSE;

    // TOOD: better handling
    if (_pnLastIndexInZone[EMZ_SOLVING] == _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED])
    {
      if (_cForces[tColumnHeader.iMatrixAIndex] != 0.0f) // something realy happend
        _iGlobalCycle++;
      
      return TRUE;
    }    

    unsigned int nActual = _pnLastIndexInZone[EMZ_SOLVING] - 1;
    static Vector<marina_real> cDiffForces;
    cDiffForces.Init(nActual + 1);

    static Vector<marina_real> cDiffAccel;
    cDiffAccel.Init(nActual + 1);

    START_PROFILE_MARINA(calcdiffs);
    int ret;
    if (bNewForce)
      ret = CalculateDiffs(cDiffForces, cDiffAccel, _cAccelerations[nActual] < 0.0);
    else
      ret = CalculateDiffs(cDiffForces, cDiffAccel, _cForces[nActual] < 0.0);
    END_PROFILE_MARINA(calcdiffs);

    if (ret >= 0)
    {
      //lineary dependent force throu it into unsolved
      TCOLUMNHEADER& tHeader = *_cMappingToColumnID[ret];
      tHeader.iNotUseInGlobalCycle = _iGlobalCycle + 100000; // never use it again;
      DriveToZeroRelease(tHeader);

      bRecalculate = TRUE;
      continue;
    }

    //Log("cDiffAccel[nActual] = %f, _cAccelerations[nActual] = %f",cDiffAccel[nActual],_cAccelerations[nActual]);
    //unsigned int iNormalForceIndex = 0;

    marina_real fSNew = -1.0f;
    BOOL bFrictionClamped = TRUE;

    if (!bNewForce)
    {
      fSNew = fabsf(_cForces[nActual]);
    }
    else
    {
      //if (IsNumericalZero(cDiffAccel[nActual]/1000.0f))
      if (fabsf(cDiffAccel[nActual]) * 1000.0f < fabsf(g_cMatrixA.GetMemberValue(nActual,nActual)))
      {
        _iLineDep++;
        // the new force are lineary dependent with other forces.
        // TODO: better handling
        if (_cForces[nActual] == 0.0f)
        {
          // just try another one
          MoveToZoneEx(tColumnHeader.iMatrixAIndex, EMZ_UNSOLVED, EMZ_SOLVING);
          tColumnHeader.iNotUseInGlobalCycle = _iGlobalCycle + 10000;

          // Linearize condition
          marina_real fTemp = _cAccelerations[nActual];
          (void) fTemp;
          _cVectorB[nActual] -= _cAccelerations[nActual];
          _cAccelerations[nActual] = 0.0f;
          
          return TRUE;
        }
        else
        {
          ASSERT(bNewForce);

          _iLocalCycle++;
          ASSERT(ptLastMoved != NULL);
          if (ptLastMoved->iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED])
          {
            // move it back between unclamped
            if (ptLastMoved->eForceType == EFT_STATIC)
            {
#if _USE_PEDESTALS
              if(ptLastMoved->pedestal && ptLastMoved->pedestal->nClamped == 3)
              {
                // return one force from pedestal into not clamped
                TPEDESTAL & ped = *ptLastMoved->pedestal;

                Vector3 forces(_cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
                  _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
                  _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

                Vector3 realForces = ped.toRealCalmped * forces;  

                // there must be one with zero realforce
                int releaseIndx = 0;
                for(; releaseIndx < 3; releaseIndx++)
                  if (IsNumericalZero(realForces[releaseIndx]))
                    break;

                ASSERT(releaseIndx < 3);

                unsigned int newRealClamped[2];
                int i = 0; 
                if (releaseIndx != 0)
                {
                  //newRealClapmedForce[i] = realForces[0]; 
                  newRealClamped[i++] = ped.realClamped[0];
                }
                if (releaseIndx != 1)
                {
                  //newRealClapmedForce[i] = realForces[1]; 
                  newRealClamped[i++] = ped.realClamped[1];
                }
                if (releaseIndx != 2)
                {
                  //newRealClapmedForce[i] = realForces[2]; 
                  newRealClamped[i++] = ped.realClamped[2];
                }
               
                unsigned int clamped[3];
                memcpy(clamped,ped.clamped,sizeof(*clamped) * 3);

                /// revert all notclamped
                for(int i = 0; i < 3; i++)
                {
                  TCOLUMNHEADER& tOldRealClamped = _cColumnHeaders[ped.realClamped[i] * 3];
                  if (tOldRealClamped.iNotClampedIndex > 0)
                    RevertColumnInMatrixA(tOldRealClamped.iMatrixAIndex);
                }

                // release friction for one that goes away
                TCOLUMNHEADER& tHeader = _cColumnHeaders[ped.realClamped[releaseIndx] * 3];

                if (tHeader.iNotClampedIndex > 0)
                {          
                  tHeader.iNotClampedIndex = 
                    tHeader.pBrotherForcesHeaders[0]->iNotClampedIndex =
                    tHeader.pBrotherForcesHeaders[1]->iNotClampedIndex = 0;
                }

                MoveToZoneEx(tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex, EMZ_UNSOLVED);
                MoveToZoneEx(tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex, EMZ_UNSOLVED);                  

                for(int i =0; i < 3; i++)
                {
                  if (clamped[i] == newRealClamped[0] || 
                    clamped[i] == newRealClamped[1])             
                    continue;

                  TCOLUMNHEADER& clampedHeader = _cColumnHeaders[clamped[i] * 3];
                  ASSERT(clampedHeader.iNotClampedIndex == 0);
                  MoveToZoneEx(clampedHeader.iMatrixAIndex, EMZ_NOTCLAMPED, EMZ_CLAMPED);
                  _cForces[clampedHeader.iMatrixAIndex] = 0;
                }

                for(int i = 0; i < 2; i++)
                  MoveToZoneEx(CONTACTID_TO_MATRIXINDEX(newRealClamped[i]), EMZ_CLAMPED, EMZ_CLAMPED);

                for(int i =0; i < 3; i++)
                  _cForces[CONTACTID_TO_MATRIXINDEX(ped.realClamped[i])] = realForces[i];

                for(int i = 0; i < 2; i++)
                {
                  // add friction if clamped
                  TCOLUMNHEADER& tNewRealClamped = _cColumnHeaders[newRealClamped[i] * 3];
                  if (tNewRealClamped.iNotClampedIndex > 0)
                  {            
                    AddFrictionIntoNormal(tNewRealClamped, 
                      tNewRealClamped.pBrotherForcesHeaders[tNewRealClamped.iNotClampedIndex - 1]->iMatrixAIndex, 
                      tNewRealClamped.fSlidingFrictionCoef);            
                  }
                }

                for(int i =0; i < 3; i++)
                {
                  unsigned  int indx = CONTACTID_TO_MATRIXINDEX(clamped[i]);
                  ASSERT(indx != tColumnHeader.iMatrixAIndex);
                  if (indx >= _pnLastIndexInZone[EMZ_CLAMPED])
                  {         
                    _cAccelerations[indx] = 0;                   
                  }
                }

                TestForces();        
                CheckPedestals();
              }
              else 
#endif
                MoveToZoneEx(ptLastMoved->iMatrixAIndex, EMZ_NOTCLAMPED, EMZ_CLAMPED);
            }
            else
            {              
#if _USE_PEDESTALS
               // is it from pedestal?
              TCOLUMNHEADER& tHeaderNormal = *ptLastMoved->pBrotherForcesHeaders[0];
              if (tHeaderNormal.pedestal && tHeaderNormal.pedestal->nClamped == 3)
              {
                PedestalFrictionCToNC(*ptLastMoved);
              }
              else
#endif
              {              
                MoveToZoneEx(ptLastMoved->iMatrixAIndex, EMZ_FRICTIONNOTCLAMPED, EMZ_CLAMPED);
                MoveToZoneEx(ptLastMoved->pBrotherForcesHeaders[1]->iMatrixAIndex, EMZ_UNSOLVED);
                ASSERT(!ptLastMoved->pBrotherForcesHeaders[1]->bDeadMouse);

                AddFrictionIntoNormal(*ptLastMoved->pBrotherForcesHeaders[0], 
                  ptLastMoved->iMatrixAIndex,
                  ptLastMoved->fSlidingFrictionCoef);              

                ptLastMoved->iNotClampedIndex = ptLastMoved->iDirIndex;
                ptLastMoved->pBrotherForcesHeaders[1]->iNotClampedIndex = 
                  ptLastMoved->pBrotherForcesHeaders[0]->iNotClampedIndex = ptLastMoved->iNotClampedIndex;
              }
            }

            /// change _cVectorB and move it into clamped

            marina_real fTemp = _cAccelerations[nActual];
            (void) fTemp;
            _cVectorB[nActual] -= _cAccelerations[nActual];
            _cAccelerations[nActual] = 0.0f;

            MoveToZoneEx(nActual, EMZ_CLAMPED, EMZ_SOLVING);

            _iGlobalCycle++;

            //Log("---------------------------------------------------------------");
            //Log("DriveToZero IsNumericalZero(cDiffAccel[nActual]/100.0),_cForces[nActual] != 0.0f cAccel[nActual] = %f",fTemp);
            //Log("---------------------------------------------------------------");
            return TRUE;
          }
        }
      }

      if (tColumnHeader.eForceType == EFT_FRICTION)
      {
        /// Step to make friction contact clamped
        fSNew = - _cAccelerations[nActual] / cDiffAccel[nActual];
       
        if (fSNew < 0.0f /*|| cDiffAccel[nActual] == 0.0f*/)
        {

          if (!_bAllowDynamicContact && _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] == _pnLastIndexInZone[EMZ_NOTCLAMPED])
          {
            // Numerical problem, solve it later
            ASSERT(FALSE);           
            return FALSE;
          }

          fSNew = -1.0f;
        }

        // The normal force must be clamped. Calculate the step till friction reaches maximum value.
        ASSERT(tColumnHeader.fSlidingFrictionCoef >= 0.0f);

        TCOLUMNHEADER & tColumnHeaderNormal = *(tColumnHeader.pBrotherForcesHeaders[0]);
        ASSERT(tColumnHeaderNormal.iNotClampedIndex == 0);

        marina_real fS;
#if _USE_PEDESTALS
        if (tColumnHeaderNormal.pedestal && tColumnHeaderNormal.pedestal->nClamped == 3)
        { 
          TPEDESTAL& ped = *tColumnHeaderNormal.pedestal;
          unsigned int indxped = ped.RealClampedIndx(tColumnHeaderNormal.iContactPointID);          

          ASSERT(indxped < 3);

          Vector3 forces(_cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
            _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
            _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

          Vector3 realForces = ped.toRealCalmped * forces;

          Vector3 diffForces(cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
            cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
            cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

          Vector3 realDiffForces = ped.toRealCalmped * diffForces;

          CalculateMaximalAllowedStepForFrictionPoint(fS, tColumnHeaderNormal, realForces[indxped], realDiffForces[indxped], cDiffForces);
        }
        else
#endif
          CalculateMaximalAllowedStepForFrictionPoint(fS, 
            tColumnHeaderNormal,
            _cForces[tColumnHeaderNormal.iMatrixAIndex],
            cDiffForces[tColumnHeaderNormal.iMatrixAIndex],
            cDiffForces);

        fSNew = umin<marina_real>(fSNew, fS);
        bFrictionClamped = (fS != fSNew); 
      }
      else
      {
        if (cDiffAccel[nActual]  <= 0.0f) 
        {
          if (!_bAllowDynamicContact && _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] == _pnLastIndexInZone[EMZ_NOTCLAMPED])
          {
            // Numerical problem
            ASSERT(FALSE);           
            return FALSE;
          }

          fSNew = -1.0f;
        }
        else
          fSNew = - _cAccelerations[nActual] / cDiffAccel[nActual];
      }
    }

    // Find if any collision need change between C <-> NC   
    marina_real fSNewRest = fSNew;
    do 
    {
#if _USE_PEDESTALS
      if (bNewForce && fSNewRest != fSNew  && tColumnHeader.eForceType == EFT_FRICTION)
      {

        TCOLUMNHEADER & tColumnHeaderNormal = *(tColumnHeader.pBrotherForcesHeaders[0]);
        ASSERT(tColumnHeaderNormal.iNotClampedIndex == 0);
        marina_real fS;

        if (tColumnHeaderNormal.pedestal && tColumnHeaderNormal.pedestal->nClamped == 3)
        { 
          // maybe it was changed
          TPEDESTAL& ped = *tColumnHeaderNormal.pedestal;
          unsigned int indxped = ped.RealClampedIndx(tColumnHeaderNormal.iContactPointID);          

          ASSERT(indxped < 3);

          Vector3 forces(_cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
            _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
            _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

          Vector3 realForces = ped.toRealCalmped * forces;

          Vector3 diffForces(cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
            cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
            cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

          Vector3 realDiffForces = ped.toRealCalmped * diffForces;

          CalculateMaximalAllowedStepForFrictionPoint(fS, tColumnHeaderNormal, realForces[indxped], realDiffForces[indxped], cDiffForces);
        }
        else
          CalculateMaximalAllowedStepForFrictionPoint(fS, 
          tColumnHeaderNormal,
          _cForces[tColumnHeaderNormal.iMatrixAIndex],
          cDiffForces[tColumnHeaderNormal.iMatrixAIndex],
          cDiffForces);

        fSNewRest = - _cAccelerations[nActual] / cDiffAccel[nActual];
        fSNewRest = umin<marina_real>(fSNewRest, fS);
        bFrictionClamped = (fS != fSNewRest); 
      }
#endif

      fSNew = fSNewRest;
      marina_real fSMin = -1.0f;
      unsigned int iCollision = _nNumberOfForces + 1;

      CalculateMaximalAllowedStep( fSMin, iCollision, cDiffForces, cDiffAccel);

      if (fSMin < 0.0f && fSNew < 0.0f)
      { 
        // Dynamic case, unbound case
        //ASSERT(fSMin >= 0.0f || fSNew >= 0.0f);
        tColumnHeader.iNotUseInGlobalCycle = _iGlobalCycle + 10000; //BEWARE It is more than needed        
        _cReleaseList.Add(&tColumnHeader);
        ASSERT(FALSE); 
        // It is not correctly solved

        return TRUE;
      }      

      if (fSNew != umin<marina_real>(fSNew, fSMin))
      {
        fSNewRest -= fSMin;
        fSNew = fSMin;       
      }                
      else
      {
        fSNewRest = 0;
        iCollision = _nNumberOfForces + 1;
      }

#if _USE_PEDESTALS
      if (!IsNumericalZero(fSNew))
#else
      if(fSNew != 0)
#endif
      {

        _iLocalCycle++;
        // Drive to zero by coefficient fSNew
        // Clamped
        for (unsigned i = 0; i < _pnLastIndexInZone[EMZ_CLAMPED]; i++)
          _cForces[i] += fSNew * cDiffForces[i];

        for (unsigned i = _pnLastIndexInZone[EMZ_NOTCLAMPED]; i < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]; i++)
          _cForces[i] += fSNew * cDiffForces[i];


        // Not clamped
        for(unsigned i = _pnLastIndexInZone[EMZ_CLAMPED]; i < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED];i++)
          _cAccelerations[i] += fSNew * cDiffAccel[i];

        _cForces[nActual] += fSNew * cDiffForces[nActual];
        _cAccelerations[nActual] += fSNew * cDiffAccel[nActual];
      }

      if (iCollision > _nNumberOfForces )
      {     
        if (!bNewForce)
        {
          ASSERT(IsNumericalZero(_cForces[nActual]));
          _cForces[nActual] = 0.0f;          

          DriveToZeroReleaseEnd(tColumnHeader);
          return TRUE;
        }

        return DriveToZeroEnd(tColumnHeader,bFrictionClamped);      
      }
      else
      {           
        bRecalculate = TRUE;
        // A contact need to be moved C <-> NC  


        TCOLUMNHEADER& tColumnHeaderMove = *(_cMappingToColumnID[iCollision]);
        ptLastMoved = _cMappingToColumnID[iCollision];

        ////Log("Change collision: %d, %d",tColumnHeaderTemp.iSourceMatrixAIndex, tColumnHeaderTemp.iLastChangeCycle);

        if ((fSNew == 0.0f) && (tColumnHeaderMove.iLastChangeLocalCycle == _iLocalCycle/* - 1*/))
        {
          // force must be moved between unsolved   
          _iRelease++;

          unsigned int iTemp = tColumnHeaderMove.iMatrixAIndex;
          (void) iTemp;
          tColumnHeaderMove.iNotUseInGlobalCycle = _iGlobalCycle + 100000; // never use it again;
          DriveToZeroRelease(tColumnHeaderMove);
          fSNewRest = 0; //recalculate with diffs
          TestForces();

          //Log("---------------------------------------------------------------");
          //Log("!DriveToZeroRelease(tColumnHeader) iMatrixAIndex = %d, iMatrixAIndexAfter = %d", iTemp, tColumnHeaderMove.iMatrixAIndex);
          //Log("---------------------------------------------------------------");                                                 
        }
        else
        {
          _iCtoNC++;
          TestForces();
          CheckPedestals();
          if (DriveToZeroCFromToNC( tColumnHeader, tColumnHeaderMove, cDiffForces))
          {
            fSNewRest = 0; //recalculate with diffs         
            _iCtoNCReal++;
          }   
          TestForces();
        }          
      }
      CheckPedestals();
      TestForces();
    } while (fSNewRest > 0);
    
  } while (bRecalculate);

  ////Log("DriveToZero: End");
  //Log("DriveToZeroFalse WrongEnd\n");
  //Log("---------------------------------------------------------------");
  //Log("DriveToZero");
  //Log("---------------------------------------------------------------");
  return TRUE;  
}

BOOL DantzigAlgorithm::DriveToZeroEnd(TCOLUMNHEADER &tColumnHeader, BOOL bFrictionClamped)
{
  if (_cForces[tColumnHeader.iMatrixAIndex] != 0.0f) // something realy happend
    _iGlobalCycle++;  

  // made contact fully something
  if (tColumnHeader.eForceType == EFT_FRICTION)
  { 
#if _USE_PEDESTALS
    TCOLUMNHEADER& tHeaderNormal = *tColumnHeader.pBrotherForcesHeaders[0];
    if (tHeaderNormal.pedestal && tHeaderNormal.pedestal->nClamped == 3)
    {
      if (tHeaderNormal.pedestal->RealClampedIndx(tHeaderNormal.iContactPointID) < 3)
      {
        // normal clamped
        if (!bFrictionClamped)
        {
          PedestalFrictionCToNC(tColumnHeader);
          _cAccelerations[tColumnHeader.iMatrixAIndex] = CalcAccel(tColumnHeader.iMatrixAIndex);
        }
        else
        {
          ASSERT(IsNumericalZero(_cAccelerations[tColumnHeader.iMatrixAIndex]/100.0f));
          _cAccelerations[tColumnHeader.iMatrixAIndex] = 0.0f;
          MoveToZoneEx(tColumnHeader.iMatrixAIndex, EMZ_CLAMPED, EMZ_SOLVING);                    
        }       
      }
      else
      {
        ASSERT(IsNumericalZero(_cForces[tColumnHeader.iMatrixAIndex]));
        _cForces[tColumnHeader.iMatrixAIndex] = 0.0f;
        MoveToZoneEx(tColumnHeader.iMatrixAIndex, EMZ_UNSOLVED, EMZ_SOLVING);                
      }
      return TRUE;
    }
#endif

    unsigned int iNormalForceIndex = tColumnHeader.pBrotherForcesHeaders[0]->iMatrixAIndex;
    if (iNormalForceIndex < _pnLastIndexInZone[EMZ_CLAMPED])
    {       
      if (!bFrictionClamped)
      {
        // move it into "friction not clamped". 
        // determine clamped direction
        TCOLUMNHEADER & tHeaderBrotherFriction = * tColumnHeader.pBrotherForcesHeaders[1];
        ASSERT (tHeaderBrotherFriction.iMatrixAIndex < _pnLastIndexInZone[EMZ_NOTCLAMPED] || 
          tHeaderBrotherFriction.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING]);
        ASSERT(!tHeaderBrotherFriction.bDeadMouse);

        // make friction not clamped
        TCOLUMNHEADER & tHeaderBrotherNormal = * tColumnHeader.pBrotherForcesHeaders[0];

        ASSERT(tHeaderBrotherNormal.iMatrixAIndex  < _pnLastIndexInZone[EMZ_CLAMPED]);               

        Vector3 cNewDir(0,0,0);

        cNewDir[0] = _cForces[tColumnHeader.iMatrixAIndex];
        if (tHeaderBrotherFriction.iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED])
          cNewDir[1] = _cForces[tHeaderBrotherFriction.iMatrixAIndex];
        else        
          _cForces[tHeaderBrotherFriction.iMatrixAIndex] = 0;       
        
        cNewDir.Normalize();

        ChangeFrictionAxes( tColumnHeader, cNewDir);                

        tColumnHeader.iNotClampedIndex = tColumnHeader.iDirIndex;
        tHeaderBrotherFriction.iNotClampedIndex = tColumnHeader.iDirIndex ;
        tHeaderBrotherNormal.iNotClampedIndex = tColumnHeader.iDirIndex;

        MoveToZoneEx(tColumnHeader.iMatrixAIndex, EMZ_FRICTIONNOTCLAMPED,EMZ_SOLVING);
        MoveToZoneEx(tHeaderBrotherFriction.iMatrixAIndex,EMZ_UNSOLVED);

        AddFrictionIntoNormal(tHeaderBrotherNormal,tColumnHeader.iMatrixAIndex,
          tHeaderBrotherNormal.fSlidingFrictionCoef);        

        _cAccelerations[tColumnHeader.iMatrixAIndex] = CalcAccel(tColumnHeader.iMatrixAIndex);

        //MoveToZoneEnd(tHeaderBrotherNormal.iMatrixAIndex, EMZ_CLAMPED);
      }
      else
      { 
        // move it into clamped                   
        ASSERT(IsNumericalZero(_cAccelerations[tColumnHeader.iMatrixAIndex]/100.0f));
        _cAccelerations[tColumnHeader.iMatrixAIndex] = 0.0f;
        MoveToZoneEx(tColumnHeader.iMatrixAIndex, EMZ_CLAMPED, EMZ_SOLVING);                    
      }       
      return TRUE;
    }
  
    //if (iNormalForceIndex >= _pnLastIndexInZone[EMZ_NOTCLAMPED])
    {
      //not clamped
      ASSERT(IsNumericalZero(_cForces[tColumnHeader.iMatrixAIndex]));
      _cForces[tColumnHeader.iMatrixAIndex] = 0.0f;
      MoveToZoneEx(tColumnHeader.iMatrixAIndex, EMZ_UNSOLVED, EMZ_SOLVING);  

      return TRUE;
    }
  }
  else
  {
    // normal forces
    // made contact fully clamped
    _cAccelerations[tColumnHeader.iMatrixAIndex] = 0.0f;
    MoveToZoneEx(tColumnHeader.iMatrixAIndex,EMZ_CLAMPED, EMZ_SOLVING);       

    return TRUE;
  }
}



//#define CONTACTID_TO_MATRIXINDEX(i) (_cColumnHeaders[(i) * 3].iMatrixAIndex)
void DantzigAlgorithm::CalculatePedestalMatrix(Matrix3& ret, unsigned int *clamped, unsigned int *realClamped)
{
  unsigned int clampedIndex[3];
  clampedIndex[0] = CONTACTID_TO_MATRIXINDEX(clamped[0]);
  clampedIndex[1] = CONTACTID_TO_MATRIXINDEX(clamped[1]);
  clampedIndex[2] = CONTACTID_TO_MATRIXINDEX(clamped[2]);

  unsigned int realClampedIndex[3];
  realClampedIndex[0] = CONTACTID_TO_MATRIXINDEX(realClamped[0]);
  realClampedIndex[1] = CONTACTID_TO_MATRIXINDEX(realClamped[1]);
  realClampedIndex[2] = CONTACTID_TO_MATRIXINDEX(realClamped[2]);

  Matrix3 cl; 
  for(int i = 0; i < 3; i++) for(int j = 0; j < 3; j++)
  cl(i,j) = g_cMatrixA.GetMemberValue(realClampedIndex[i], clampedIndex[j]);

  Matrix3 rcl; 
  for(int i = 0; i < 3; i++) for(int j = 0; j < 3; j++)
    rcl(i,j) =g_cMatrixA.GetMemberValue(realClampedIndex[i], realClampedIndex[j]);

  ret = rcl.InverseGeneral() * cl;
}

Vector3 fric2;
Vector3 fric3;
float mincez;
void DantzigAlgorithm::Calculate3to2PedestalFriction(Vector3& fric, unsigned int  & newindx2, unsigned int *clamped, unsigned int *realClamped, Vector3Val fn)
{
  TCOLUMNHEADER& normRealClamped1 = _cColumnHeaders[3 * realClamped[0]];
  TCOLUMNHEADER& normRealClamped2 = _cColumnHeaders[3 * realClamped[1]];

  unsigned int newindx[3];

  newindx[0] = normRealClamped1.pBrotherForcesHeaders[0]->iMatrixAIndex;
  newindx[1] = normRealClamped1.pBrotherForcesHeaders[1]->iMatrixAIndex;  
  newindx[2] = normRealClamped2.pBrotherForcesHeaders[0]->iMatrixAIndex;
  newindx2 = 0;

  Vector3 accel = VZero;
  for(int i = 0; i < 3; i++) 
  {
    TCOLUMNHEADER& normClamped = _cColumnHeaders[3 * clamped[i]];
    
    unsigned int indx = normClamped.pBrotherForcesHeaders[0]->iMatrixAIndex;      

    if (indx < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED])
    {
      float value;
      if (indx <  _pnLastIndexInZone[EMZ_CLAMPED])      
        value = _cForces[indx];
      else
        value = _cForces[normClamped.iMatrixAIndex] * normClamped.fSlidingFrictionCoef;     

      for(int j = 0; j < 3; j++)      
        accel[j] = g_cMatrixA.GetMemberValue(newindx[j], indx) * value;      
    }

    indx = normClamped.pBrotherForcesHeaders[1]->iMatrixAIndex;      

    if (indx < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED])
    {
      float value;
      if (indx <  _pnLastIndexInZone[EMZ_CLAMPED])      
        value = _cForces[indx];
      else
        value = _cForces[normClamped.iMatrixAIndex] * normClamped.fSlidingFrictionCoef;     

      for(int j = 0; j < 3; j++)      
        accel[j] = g_cMatrixA.GetMemberValue(newindx[j], indx) * value;
    }
  }
  
  // calc matrix.
  Matrix3 rcl;
  rcl(0,0) = g_cMatrixA.GetMemberValue(newindx[0], newindx[0]);
  rcl(0,2) = g_cMatrixA.GetMemberValue(newindx[0], newindx[2]);
  rcl(1,1) = g_cMatrixA.GetMemberValue(newindx[1], newindx[1]);
  rcl(1,2) = g_cMatrixA.GetMemberValue(newindx[1], newindx[2]);

  if (IsNumericalZero(rcl(1,2) - rcl(1,1)) || IsNumericalZero(rcl(0,2) - rcl(0,0)))
  {
    // it is probably lin dep
    newindx2 = 1;
    newindx[2] = normRealClamped2.pBrotherForcesHeaders[1]->iMatrixAIndex;
    rcl(1,2) = g_cMatrixA.GetMemberValue(newindx[1], newindx[2]);
    rcl(0,2) = g_cMatrixA.GetMemberValue(newindx[0], newindx[2]);
    ASSERT(!IsNumericalZero(rcl(1,2) - rcl(1,1)) && !IsNumericalZero(rcl(0,2) - rcl(0,0)));
  }

  rcl(0,1) = g_cMatrixA.GetMemberValue(newindx[0], newindx[1]);
  rcl(1,0) = g_cMatrixA.GetMemberValue(newindx[1], newindx[0]);
  rcl(2,0) = g_cMatrixA.GetMemberValue(newindx[2], newindx[0]);
  rcl(2,1) = g_cMatrixA.GetMemberValue(newindx[2], newindx[1]);
  rcl(2,2) = g_cMatrixA.GetMemberValue(newindx[2], newindx[2]);

  fric = rcl.InverseGeneral() * accel;

  if (accel == VZero)
    return;

  // try another case   
  Matrix3 rcl2 = rcl;  
  unsigned indx2 = normRealClamped2.pBrotherForcesHeaders[newindx2 == 1 ? 0 : 1]->iMatrixAIndex;
  rcl2(1,2) = g_cMatrixA.GetMemberValue(newindx[1], indx2);
  rcl2(2,2) = g_cMatrixA.GetMemberValue(newindx[2], indx2);
  rcl2(0,2) = g_cMatrixA.GetMemberValue(newindx[0], indx2);
  
  fric2 = rcl2.InverseGeneral() * accel;
  
  mincez = FLT_MAX;
  Matrix3 rcl3 = rcl;
  for(int i = 0; i < 99; i++)
  {       
    float c1 = sin(3.14f / 100 * i);
    float c2 = cos(3.14f / 100 * i);
    rcl3(1,2) = (rcl2(1,2) * c1 + rcl(1,2) * c2);
    rcl3(2,2) = (rcl2(2,2) * c1 + rcl(2,2) * c2);
    rcl3(0,2) = (rcl2(0,2) * c1 + rcl(0,2) * c2);

    fric3 = rcl3.InverseGeneral() * accel;

    float f1 = sqrt(fric3[0] * fric3[0] + fric3[1] * fric3[1]) / fn[0];
    float f2 = fabs(fric3[2]) / fn[1];

    float cez1 = f1 - normRealClamped1.fSlidingFrictionCoef;
    float cez2 = f2 - normRealClamped2.fSlidingFrictionCoef;
    float cez = ((cez1 > 0) ? cez1 : 0) + ((cez2 > 0) ? cez2 : 0);
    if (mincez > cez)
      mincez = cez;
  }
}

bool DantzigAlgorithm::DriveToZeroCFromToNC(TCOLUMNHEADER &tColumnHeaderActual, TCOLUMNHEADER &tColumnHeaderMove, Vector<marina_real>& cDiffForces)
{
  switch(tColumnHeaderMove.eForceType)
  {
  case EFT_STATIC:
    {
#if _USE_PEDESTALS
      if (tColumnHeaderMove.iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED] && tColumnHeaderMove.pedestal && tColumnHeaderMove.pedestal->nClamped == 3)
      {
        PROFILE_SCOPE_MARINA(pedestalnew);

        /// Test of correctness
       TestForces();

        // clamped pedestal
        TPEDESTAL & ped = *tColumnHeaderMove.pedestal;

        Vector3 forces(_cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
          _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
          _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

        Vector3 realForces = ped.toRealCalmped * forces;

        Vector3 diffForces(cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
          cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
          cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

        Vector3 realDiffForces = ped.toRealCalmped * diffForces;
        
        unsigned int newRealClamped[3];
        Vector3 newRealClapmedForce;
        int i = 0; 
        if (ped.susp != 0)
        {
          newRealClapmedForce[i] = realForces[0]; 
          newRealClamped[i++] = ped.realClamped[0];
        }
        if (ped.susp != 1)
        {
          newRealClapmedForce[i] = realForces[1]; 
          newRealClamped[i++] = ped.realClamped[1];
        }
        if (ped.susp != 2)
        {
          newRealClapmedForce[i] = realForces[2]; 
          newRealClamped[i++] = ped.realClamped[2];
        }

        Matrix3 maxTrans;
        unsigned int imax = 0; 
        float maxVal = 0; 
        for(unsigned int i = ped.from; i <= ped.to; i++)
        {
          if (i != ped.realClamped[0] && i != ped.realClamped[1] && i != ped.realClamped[2])
          {
            TCOLUMNHEADER & tNewHeader =  _cColumnHeaders[i * 3];
            if (tNewHeader.iLastChangeLocalCycle == _iLocalCycle) // do not use it twice per cycle
              continue;

            newRealClamped[2] = i;
            Matrix3 trans;
            CalculatePedestalMatrix(trans, ped.clamped, newRealClamped);
            Vector3 realDiffForcesB = trans * diffForces;
            Vector3 realForcesB = trans * forces;

            // realForcesB must be equal to realForces otherwise it is numerical instability
            if ((realForcesB - realForces).SquareSize() <  0.00001 && /*realForcesB[2] > 0 && realForcesB[0] > 0 && realForcesB[1] > 0 &&*/ realDiffForcesB[2] > 0 && !IsNumericalZero(realDiffForcesB[2]/10) && realDiffForcesB[2] > maxVal)
            {
              maxVal = realDiffForcesB[2];
              imax = i;
              maxTrans = trans;
            }                                      
          }
        }

        bool ret = false;
        if (maxVal > 0)
        {        
          // if there is any friction in released point move it between unsolved
          TCOLUMNHEADER& tHeader = _cColumnHeaders[ped.realClamped[ped.susp] * 3];

          if (tHeader.iNotClampedIndex > 0)
          {
            RevertColumnInMatrixA(tHeader.iMatrixAIndex);
            tHeader.iNotClampedIndex = 
              tHeader.pBrotherForcesHeaders[0]->iNotClampedIndex =
              tHeader.pBrotherForcesHeaders[1]->iNotClampedIndex = 0;

            MoveToZoneEx(tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex, EMZ_UNSOLVED);
            MoveToZoneEx(tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex, EMZ_UNSOLVED);
            ret = true;
          }
          else
          {
            if (tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED])
            {            
              MoveToZoneEx(tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex, EMZ_UNSOLVED);
              ret = true;
            }

            if (tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED])
            {            
              MoveToZoneEx(tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex, EMZ_UNSOLVED);
              ret = true;
            }
          }

          newRealClamped[2] = imax;
          ASSERT(_cColumnHeaders[imax * 3].iNotClampedIndex == 0);

          // It is needed to recreate not clamped system
          for(int i = 0; i < 2; i++)
          {
            TCOLUMNHEADER& tRealClamped = _cColumnHeaders[newRealClamped[i] * 3];
            if (tRealClamped.iNotClampedIndex > 0)
            {
              RevertColumnInMatrixA(tRealClamped.iMatrixAIndex);
              ret = true;
            }
          }          

          memcpy(ped.realClamped, newRealClamped, 3 * sizeof(*newRealClamped));
          ped.toRealCalmped = maxTrans;                    

          for(int i = 0; i < 2; i++)
          {
            // add friction if clamped
            TCOLUMNHEADER& tNewRealClamped = _cColumnHeaders[newRealClamped[i] * 3];
            if (tNewRealClamped.iNotClampedIndex > 0)
            {                 
              unsigned int frictionIndx = tNewRealClamped.pBrotherForcesHeaders[tNewRealClamped.iNotClampedIndex - 1]->iMatrixAIndex;
              for(unsigned int j = 0; j < 3; j++)
              {
                if (true)//!IsNumericalZero(ped.toRealCalmped(indx,i)))
                {      
                  AddFrictionIntoNormal(_cColumnHeaders[ped.clamped[j] * 3], frictionIndx,
                    tNewRealClamped.fSlidingFrictionCoef * ped.toRealCalmped(i,j));
                }
              }                            
            }
          }

          // add local cycle to visualize that new was moved
          _cColumnHeaders[imax * 3].iLastChangeLocalCycle = _iLocalCycle;          

          CheckPedestals();
          TestForces();  
          return ret;
        }

        // release pedestal contact
        // not third contact found... 
        unsigned int clamped[3];
        memcpy(clamped,ped.clamped,sizeof(*clamped) * 3);

        // find friction
        //unsigned int newindx2;
        //Vector3 fric;
        //Calculate3to2PedestalFriction(fric, newindx2, clamped, newRealClamped, newRealClapmedForce);
        for(int i = 0; i < 3; i++)
        {
          TCOLUMNHEADER& tOldRealClamped = _cColumnHeaders[ped.realClamped[i] * 3];
          if (tOldRealClamped.iNotClampedIndex > 0)
            RevertColumnInMatrixA(tOldRealClamped.iMatrixAIndex);
        }

        // release friction for one that goes away
        TCOLUMNHEADER& tHeader = _cColumnHeaders[ped.realClamped[ped.susp] * 3];

        if (tHeader.iNotClampedIndex > 0)
        {          
          tHeader.iNotClampedIndex = 
            tHeader.pBrotherForcesHeaders[0]->iNotClampedIndex =
            tHeader.pBrotherForcesHeaders[1]->iNotClampedIndex = 0;
        }

        MoveToZoneEx(tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex, EMZ_UNSOLVED);
        MoveToZoneEx(tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex, EMZ_UNSOLVED);                  

        for(int i =0; i < 3; i++)
        {
          if (clamped[i] == newRealClamped[0] || 
              clamped[i] == newRealClamped[1])             
              continue;

          TCOLUMNHEADER& clampedHeader = _cColumnHeaders[clamped[i] * 3];
          ASSERT(clampedHeader.iNotClampedIndex == 0);
          MoveToZoneEx(clampedHeader.iMatrixAIndex, EMZ_NOTCLAMPED, EMZ_CLAMPED);
          _cForces[clampedHeader.iMatrixAIndex] = 0;
        }

        for(int i = 0; i < 2; i++)
          MoveToZoneEx(CONTACTID_TO_MATRIXINDEX(newRealClamped[i]), EMZ_CLAMPED, EMZ_CLAMPED);
               
        for(int i =0; i < 3; i++)
          _cForces[CONTACTID_TO_MATRIXINDEX(ped.realClamped[i])] = realForces[i];

        for(int i = 0; i < 2; i++)
        {
          // add friction if clamped
          TCOLUMNHEADER& tNewRealClamped = _cColumnHeaders[newRealClamped[i] * 3];
          if (tNewRealClamped.iNotClampedIndex > 0)
          {            
            AddFrictionIntoNormal(tNewRealClamped, 
              tNewRealClamped.pBrotherForcesHeaders[tNewRealClamped.iNotClampedIndex - 1]->iMatrixAIndex, 
              tNewRealClamped.fSlidingFrictionCoef);            
          }
        }

        for(int i =0; i < 3; i++)
        {
          unsigned  int indx = CONTACTID_TO_MATRIXINDEX(clamped[i]);
          ASSERT(indx != tColumnHeaderActual.iMatrixAIndex);
          if (indx >= _pnLastIndexInZone[EMZ_CLAMPED])
          {         
            _cAccelerations[indx] = 0;

             /* 
            _cAccelerations[indx] = g_cMatrixA.RowTimesVector(indx, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) 
              + g_cMatrixA.GetMemberValue(indx, tColumnHeaderActual.iMatrixAIndex) * _cForces[tColumnHeaderActual.iMatrixAIndex] + _cVectorB[indx];
            if (_cAccelerations[indx] < 0)
            {
              ASSERT(IsNumericalZero(_cAccelerations[indx]/10));
              _cAccelerations[indx] = 0;
            }*/
          }
        }

        TestForces();        
        return true;        
      }
#endif

      // normal point
      _cAccelerations[tColumnHeaderMove.iMatrixAIndex] = 0.0f;
      _cForces[tColumnHeaderMove.iMatrixAIndex] = 0.0f;
      // Normal forces
      if (tColumnHeaderMove.iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED])      
        // C -> NC  
        MoveToZoneEx(tColumnHeaderMove.iMatrixAIndex, EMZ_NOTCLAMPED, EMZ_CLAMPED);              
      else      
        // NC -> C        
        MoveToZoneEx(tColumnHeaderMove.iMatrixAIndex, EMZ_CLAMPED, EMZ_NOTCLAMPED);           
       
      return true;
    }
  case EFT_FRICTION:
    {
      _iCtoFric++;
      TCOLUMNHEADER & tHeaderBrotherNormal = * tColumnHeaderMove.pBrotherForcesHeaders[0];
      TCOLUMNHEADER & tHeaderBrotherFriction = * tColumnHeaderMove.pBrotherForcesHeaders[1];

#if _USE_PEDESTALS
      // check if it is pedestal
      if (tHeaderBrotherNormal.pedestal && tHeaderBrotherNormal.pedestal->nClamped == 3)
      {
        // pedestal
        if (tColumnHeaderMove.iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED])
        {
          PedestalFrictionCToNC(tColumnHeaderMove);
          break;
        }
        else
        {
          PedestalFrictionNCToC(tColumnHeaderMove);
          break;
        }
      }
#endif

      if (tColumnHeaderMove.iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED])
      {
        // move it into NOTCLAMPED
        TCOLUMNHEADER & tHeaderBrotherNormal = * tColumnHeaderMove.pBrotherForcesHeaders[0];
        TCOLUMNHEADER & tHeaderBrotherFriction = * tColumnHeaderMove.pBrotherForcesHeaders[1];

        ASSERT(tHeaderBrotherNormal.iMatrixAIndex  < _pnLastIndexInZone[EMZ_CLAMPED]);
        ASSERT(tHeaderBrotherFriction.iMatrixAIndex  < _pnLastIndexInZone[EMZ_CLAMPED] || 
          tHeaderBrotherFriction.iMatrixAIndex >= _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]);

        ASSERT(!tHeaderBrotherNormal.bDeadMouse);
        ASSERT(!tHeaderBrotherFriction.bDeadMouse);

        Vector3 cNewDir(0,0,0);

        cNewDir[0] = _cForces[tColumnHeaderMove.iMatrixAIndex];  
        if (tHeaderBrotherFriction.iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED])
          cNewDir[1] = _cForces[tHeaderBrotherFriction.iMatrixAIndex];
        else
          _cForces[tHeaderBrotherFriction.iMatrixAIndex] = 0;

        cNewDir.Normalize();       

        ChangeFrictionAxes( tColumnHeaderMove, cNewDir);                

        tColumnHeaderMove.iNotClampedIndex = tColumnHeaderMove.iDirIndex;
        tHeaderBrotherFriction.iNotClampedIndex = tColumnHeaderMove.iDirIndex ;
        tHeaderBrotherNormal.iNotClampedIndex = tColumnHeaderMove.iDirIndex;

        MoveToZoneEx(tColumnHeaderMove.iMatrixAIndex, EMZ_FRICTIONNOTCLAMPED,EMZ_CLAMPED);
        MoveToZoneEx(tHeaderBrotherFriction.iMatrixAIndex,EMZ_UNSOLVED);

        AddFrictionIntoNormal(tHeaderBrotherNormal,
          tColumnHeaderMove.iMatrixAIndex,
          tHeaderBrotherNormal.fSlidingFrictionCoef);        

        _cAccelerations[tColumnHeaderMove.iMatrixAIndex] = 0.0f;

        ASSERT(IsNumericalZero(_cForces[tHeaderBrotherFriction.iMatrixAIndex]));
        _cForces[tHeaderBrotherFriction.iMatrixAIndex] = 0.0f;             

        break;
      }

      //if (iCollision < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED])

      {
        ASSERT(tColumnHeaderMove.iMatrixAIndex < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] || tColumnHeaderMove.iMatrixAIndex >= _pnLastIndexInZone[EMZ_NOTCLAMPED]);

        // move it into CLAMPED
      
        ASSERT(tHeaderBrotherNormal.iMatrixAIndex  < _pnLastIndexInZone[EMZ_CLAMPED]);                
        ASSERT(tHeaderBrotherFriction.iMatrixAIndex  >= _pnLastIndexInZone[EMZ_SOLVING]);

        ASSERT(!tHeaderBrotherNormal.bDeadMouse);
        ASSERT(!tHeaderBrotherFriction.bDeadMouse);

        RevertColumnInMatrixA(tHeaderBrotherNormal.iMatrixAIndex);

        // Create clamped friction force in the direction of accel, when the actual point get clamped
        tColumnHeaderMove.iNotClampedIndex = 0;
        tHeaderBrotherFriction.iNotClampedIndex = 0;
        tHeaderBrotherNormal.iNotClampedIndex = 0;  

        ASSERT(IsNumericalZero(_cAccelerations[tColumnHeaderMove.iMatrixAIndex]/10000));
        MoveToZoneEx(tColumnHeaderMove.iMatrixAIndex, EMZ_CLAMPED, EMZ_FRICTIONNOTCLAMPED);        
        _cAccelerations[tColumnHeaderMove.iMatrixAIndex] = 0.0f;

        ////Log("_cForces[tColumnHeaderMove.iMatrixAIndex] = %f,_cForces[tHeaderBrotherNormal.iMatrixAIndex] = %f", _cForces[tColumnHeaderMove.iMatrixAIndex],_cForces[tHeaderBrotherNormal.iMatrixAIndex]  );
        ASSERT(_cForces[tColumnHeaderMove.iMatrixAIndex] >= 0.0f || IsNumericalZero(_cForces[tColumnHeaderMove.iMatrixAIndex] / 100.0f));

        _cForces[tColumnHeaderMove.iMatrixAIndex] = _cForces[tHeaderBrotherNormal.iMatrixAIndex] * tHeaderBrotherNormal.fSlidingFrictionCoef;

        _cForces[tHeaderBrotherFriction.iMatrixAIndex] = 0.0f;
        /* if (_cForces[tHeaderBrotherFriction.iMatrixAIndex] != 0.0f)
        MoveToZoneEx(tHeaderBrotherFriction.iMatrixAIndex, EMZ_FRICTIONCONST, EMZ_FRICTIONNOTCLAMPED);
        else*/
        MoveToZoneEx(tHeaderBrotherFriction.iMatrixAIndex, EMZ_UNSOLVED);

      }
      break;
    }
  default:
    ASSERT(FALSE);
  }

  return true;
}

void DantzigAlgorithm::PedestalFrictionCToNC(TCOLUMNHEADER &tColumnHeaderMove)
{
  TCOLUMNHEADER& tHeaderBrotherNormal = *tColumnHeaderMove.pBrotherForcesHeaders[0];
  TCOLUMNHEADER& tHeaderBrotherFriction = *tColumnHeaderMove.pBrotherForcesHeaders[1];

  TPEDESTAL& ped = *tHeaderBrotherNormal.pedestal;

  // move it into NOTCLAMPED
  // find index 

  ASSERT(tHeaderBrotherNormal.iNotClampedIndex == 0);
  ASSERT(tHeaderBrotherFriction.iMatrixAIndex  < _pnLastIndexInZone[EMZ_CLAMPED] || 
    tHeaderBrotherFriction.iMatrixAIndex >= _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]);

  ASSERT(!tHeaderBrotherNormal.bDeadMouse);
  ASSERT(!tHeaderBrotherFriction.bDeadMouse);

  Vector3 cNewDir(VZero);

  cNewDir[0] = _cForces[tColumnHeaderMove.iMatrixAIndex];  
  if (tHeaderBrotherFriction.iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED])
    cNewDir[1] = _cForces[tHeaderBrotherFriction.iMatrixAIndex];
  else
    _cForces[tHeaderBrotherFriction.iMatrixAIndex] = 0;

  cNewDir.Normalize();          

  ChangeFrictionAxes( tColumnHeaderMove, cNewDir);                

  tColumnHeaderMove.iNotClampedIndex = tColumnHeaderMove.iDirIndex;
  tHeaderBrotherFriction.iNotClampedIndex = tColumnHeaderMove.iDirIndex ;
  tHeaderBrotherNormal.iNotClampedIndex = tColumnHeaderMove.iDirIndex;

  MoveToZoneEx(tColumnHeaderMove.iMatrixAIndex, EMZ_FRICTIONNOTCLAMPED,EMZ_CLAMPED);
  MoveToZoneEx(tHeaderBrotherFriction.iMatrixAIndex,EMZ_UNSOLVED);

  // combinate according to trans matrix.
  unsigned int indx = ped.RealClampedIndx(tColumnHeaderMove.iContactPointID);
  for(unsigned int i = 0; i < 3; i++)
  {
    if (true)//!IsNumericalZero(ped.toRealCalmped(indx,i)))
    {      
      AddFrictionIntoNormal(_cColumnHeaders[ped.clamped[i] * 3], tColumnHeaderMove.iMatrixAIndex,
        tHeaderBrotherNormal.fSlidingFrictionCoef * ped.toRealCalmped(indx,i));
    }
  }          
  

  _cAccelerations[tColumnHeaderMove.iMatrixAIndex] = 0.0f;

  ASSERT(IsNumericalZero(_cForces[tHeaderBrotherFriction.iMatrixAIndex]));
  _cForces[tHeaderBrotherFriction.iMatrixAIndex] = 0.0f;  
}

void DantzigAlgorithm::PedestalFrictionNCToC(TCOLUMNHEADER &tColumnHeaderMove)
{
  ASSERT(tColumnHeaderMove.iMatrixAIndex < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] || tColumnHeaderMove.iMatrixAIndex >= _pnLastIndexInZone[EMZ_NOTCLAMPED]);

  // move it into CLAMPED
  TCOLUMNHEADER& tHeaderBrotherNormal = *tColumnHeaderMove.pBrotherForcesHeaders[0];
  TCOLUMNHEADER& tHeaderBrotherFriction = *tColumnHeaderMove.pBrotherForcesHeaders[1];

  //ASSERT(tHeaderBrotherNormal.iMatrixAIndex  < _pnLastIndexInZone[EMZ_CLAMPED]);                
  ASSERT(tHeaderBrotherFriction.iMatrixAIndex  >= _pnLastIndexInZone[EMZ_SOLVING]);

  ASSERT(!tHeaderBrotherNormal.bDeadMouse);
  ASSERT(!tHeaderBrotherFriction.bDeadMouse);

  RevertColumnInMatrixA(tHeaderBrotherNormal.iMatrixAIndex);

  // Create clamped friction force in the direction of accel, when the actual point get clamped
  tColumnHeaderMove.iNotClampedIndex = 0;
  tHeaderBrotherFriction.iNotClampedIndex = 0;
  tHeaderBrotherNormal.iNotClampedIndex = 0;

  ASSERT(IsNumericalZero(_cAccelerations[tColumnHeaderMove.iMatrixAIndex]/10000));
  MoveToZoneEx(tColumnHeaderMove.iMatrixAIndex, EMZ_CLAMPED, EMZ_FRICTIONNOTCLAMPED);        
  _cAccelerations[tColumnHeaderMove.iMatrixAIndex] = 0.0f;

  TPEDESTAL & ped = *tHeaderBrotherNormal.pedestal;

  Vector3 forces(_cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
    _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
    _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

  Vector3 realForces = ped.toRealCalmped * forces;

  unsigned int indx = ped.RealClampedIndx(tColumnHeaderMove.iContactPointID);
  ASSERT(indx < 3);

  _cForces[tColumnHeaderMove.iMatrixAIndex] = realForces[indx] * tHeaderBrotherNormal.fSlidingFrictionCoef;
  _cForces[tHeaderBrotherFriction.iMatrixAIndex] = 0.0f; 

  MoveToZoneEx(tHeaderBrotherFriction.iMatrixAIndex, EMZ_UNSOLVED);
}

BOOL DantzigAlgorithm::DriveToZeroRelease(TCOLUMNHEADER &tColumnHeaderRelease)
{
  //tColumnHeaderRelease.iNotUseInGlobalCycle = _iGlobalCycle + 1;

  switch(tColumnHeaderRelease.eForceType)
  {
  case EFT_STATIC:
    {
      ASSERT(IsNumericalZero(_cAccelerations[tColumnHeaderRelease.iMatrixAIndex]/100));
      ASSERT(IsNumericalZero(_cForces[tColumnHeaderRelease.iMatrixAIndex]/100));      

      // is in pedestal?
#if _USE_PEDESTALS
      if (tColumnHeaderRelease.iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED] && 
        tColumnHeaderRelease.pedestal && tColumnHeaderRelease.pedestal->nClamped == 3)
      {
        // release pedestal.
        // calculate real forces.
        TPEDESTAL& ped = *tColumnHeaderRelease.pedestal; 
        unsigned int clamped[3];
        memcpy(clamped,ped.clamped,sizeof(*clamped) * 3);
        unsigned int newRealClamped[2];
        memcpy(newRealClamped,ped.realClamped,sizeof(*newRealClamped) * 2);
        
        for(int i = 0; i < 3; i++)
        {
          TCOLUMNHEADER& tOldRealClamped = _cColumnHeaders[ped.realClamped[i] * 3];
          if (tOldRealClamped.iNotClampedIndex > 0)
            RevertColumnInMatrixA(tOldRealClamped.iMatrixAIndex);
        }

        // release friction for one that goes away
        TCOLUMNHEADER& tHeader = _cColumnHeaders[ped.realClamped[2] * 3];

        if (tHeader.iNotClampedIndex > 0)
        {
          RevertColumnInMatrixA(tHeader.iMatrixAIndex);
          tHeader.iNotClampedIndex = 
            tHeader.pBrotherForcesHeaders[0]->iNotClampedIndex =
            tHeader.pBrotherForcesHeaders[1]->iNotClampedIndex = 0;
        }

        MoveToZoneEx(tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex, EMZ_UNSOLVED);
        MoveToZoneEx(tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex, EMZ_UNSOLVED);                  

        for(int i =0; i < 3; i++)
        {
          if (clamped[i] == newRealClamped[0] || 
            clamped[i] == newRealClamped[1])             
            continue;

          TCOLUMNHEADER& clampedHeader = _cColumnHeaders[clamped[i] * 3];
          ASSERT(clampedHeader.iNotClampedIndex == 0);
          MoveToZoneEx(clampedHeader.iMatrixAIndex, EMZ_NOTCLAMPED, EMZ_CLAMPED);
          _cForces[clampedHeader.iMatrixAIndex] = 0;
        }

        for(int i = 0; i < 2; i++)
          MoveToZoneEx(CONTACTID_TO_MATRIXINDEX(newRealClamped[i]), EMZ_CLAMPED, EMZ_CLAMPED);

        Vector3 forces(_cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
          _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
          _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

        Vector3 realForces = ped.toRealCalmped * forces;

        for(int i =0; i < 3; i++)
          _cForces[CONTACTID_TO_MATRIXINDEX(ped.realClamped[i])] = realForces[i];

        for(int i = 0; i < 2; i++)
        {
          // add friction if clamped
          TCOLUMNHEADER& tNewRealClamped = _cColumnHeaders[newRealClamped[i] * 3];
          if (tNewRealClamped.iNotClampedIndex > 0)
          {            
            AddFrictionIntoNormal(tNewRealClamped, 
              tNewRealClamped.pBrotherForcesHeaders[tNewRealClamped.iNotClampedIndex - 1]->iMatrixAIndex, 
              tNewRealClamped.fSlidingFrictionCoef);            
          }
        }

        for(int i = 0; i < 3; i++)
        {
          unsigned  int indx = CONTACTID_TO_MATRIXINDEX(clamped[i]);
          //ASSERT(indx != tColumnHeaderActual.iMatrixAIndex);
          if (indx >= _pnLastIndexInZone[EMZ_CLAMPED])                   
            _cAccelerations[indx] = 0;                   
        }

      }      
#endif
      _cAccelerations[tColumnHeaderRelease.iMatrixAIndex] = 0.0f;
      _cForces[tColumnHeaderRelease.iMatrixAIndex] = 0.0f;
      MoveToZoneEx(tColumnHeaderRelease.iMatrixAIndex, EMZ_UNSOLVED);

      return TRUE;      
      break;
    }
  case EFT_FRICTION:
    {
      TCOLUMNHEADER& tHeaderBrotherNormal = *(tColumnHeaderRelease.pBrotherForcesHeaders[0]);                            
      TCOLUMNHEADER& tHeaderBrotherFriction = * tColumnHeaderRelease.pBrotherForcesHeaders[1];

      ASSERT(_cForces[tHeaderBrotherFriction.iMatrixAIndex] == 0.0f);

      if (tColumnHeaderRelease.iMatrixAIndex >= _pnLastIndexInZone[EMZ_NOTCLAMPED])
      {
        ASSERT(tColumnHeaderRelease.iMatrixAIndex < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]);

        // Create force in direction of not clamped                
        RevertColumnInMatrixA(tHeaderBrotherNormal.iMatrixAIndex);                

        tColumnHeaderRelease.iNotClampedIndex = 0;
        tHeaderBrotherFriction.iNotClampedIndex = 0;
        tHeaderBrotherNormal.iNotClampedIndex = 0;                                        
#if _USE_PEDESTALS
        if (tHeaderBrotherNormal.pedestal && tHeaderBrotherNormal.pedestal->nClamped == 3)
        {
          TPEDESTAL & ped = *tHeaderBrotherNormal.pedestal;

          Vector3 forces(_cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
            _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
            _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

          Vector3 realForces = ped.toRealCalmped * forces;

          unsigned int indx = ped.RealClampedIndx(tColumnHeaderRelease.iContactPointID);
          ASSERT(indx < 3);

          _cForces[tColumnHeaderRelease.iMatrixAIndex] = realForces[indx] * tHeaderBrotherNormal.fSlidingFrictionCoef;
        }
        else
#endif
        {

          ASSERT(_cForces[tColumnHeaderRelease.iMatrixAIndex] >= 0.0f);
          _cForces[tColumnHeaderRelease.iMatrixAIndex] = _cForces[tHeaderBrotherNormal.iMatrixAIndex] * tColumnHeaderRelease.fSlidingFrictionCoef;                                
          _cForces[tHeaderBrotherFriction.iMatrixAIndex] = 0.0f;
        }

        
        MoveToZoneEx(tHeaderBrotherFriction.iMatrixAIndex, EMZ_UNSOLVED);
        //tHeaderBrotherFriction.iNotUseInGlobalCycle = _iGlobalCycle + 1;

        MoveToZoneEx(tColumnHeaderRelease.iMatrixAIndex, EMZ_UNSOLVED, EMZ_FRICTIONNOTCLAMPED);
        //tColumnHeaderRelease.iNotUseInGlobalCycle = _iGlobalCycle + 1;            
      }
      else
      {            
        ASSERT(tColumnHeaderRelease.pBrotherForcesHeaders[1]->iMatrixAIndex >= _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] &&
          tColumnHeaderRelease.pBrotherForcesHeaders[1]->iMatrixAIndex < _pnLastIndexInZone[EMZ_UNSOLVED]);

        MoveToZoneEx(tHeaderBrotherFriction.iMatrixAIndex, EMZ_UNSOLVED, EMZ_FRICTIONNOTCLAMPED);
        //tHeaderBrotherFriction.iNotUseInGlobalCycle = _iGlobalCycle + 1;

        MoveToZoneEx(tColumnHeaderRelease.iMatrixAIndex, EMZ_UNSOLVED, EMZ_CLAMPED);
        //tColumnHeaderRelease.iNotUseInGlobalCycle = _iGlobalCycle + 1;  
      }

      _cReleaseList.Add(&tColumnHeaderRelease);
      return TRUE;                
    }
  default:
    ASSERT(FALSE);
  }

  return TRUE;
}

void DantzigAlgorithm::DriveToZeroReleaseEnd(TCOLUMNHEADER &tColumnHeaderRelease)
{    
  MoveToZoneEx(tColumnHeaderRelease.iMatrixAIndex, EMZ_UNSOLVED, EMZ_SOLVING);
  return;
}

void DantzigAlgorithm::CalculateMaximalAllowedStepForFrictionPoint(marina_real& fSMin, const TCOLUMNHEADER &tHeaderNormal, float fNormalForce, float fNormalDiffForce, const Vector<marina_real>& cDiffForces)
{
  //PROFILE_SCOPE_MARINA(maxallowed);
  // Configuration is valid if:
  // ax^2 + bx + c <= 0

  //marina_real a,b,c;
  marina_real a,b,c;
  a = b = c = 0.0f;
  
  marina_real fFrictionCoefSqr = m_sqr(tHeaderNormal.fSlidingFrictionCoef);
  unsigned int iIndex = tHeaderNormal.iMatrixAIndex;

  a -= m_sqr(fNormalDiffForce) * fFrictionCoefSqr;
  b -= 2 * fNormalDiffForce * fNormalForce * fFrictionCoefSqr;
  c -= m_sqr(fNormalForce) * fFrictionCoefSqr;

  iIndex = tHeaderNormal.pBrotherForcesHeaders[0]->iMatrixAIndex;

  if ((iIndex < _pnLastIndexInZone[EMZ_CLAMPED] || iIndex == _pnLastIndexInZone[EMZ_SOLVING -1]) && (!tHeaderNormal.pBrotherForcesHeaders[0]->bDeadMouse))
  {    
    a += m_sqr(cDiffForces[iIndex]);
    b += 2 * cDiffForces[iIndex] * _cForces[iIndex];
    c += m_sqr(_cForces[iIndex]);
  }

  iIndex = tHeaderNormal.pBrotherForcesHeaders[1]->iMatrixAIndex;

  if ((iIndex < _pnLastIndexInZone[EMZ_CLAMPED] || iIndex == _pnLastIndexInZone[EMZ_SOLVING -1]) && !tHeaderNormal.pBrotherForcesHeaders[1]->bDeadMouse)
  { 
    a += m_sqr(cDiffForces[iIndex]);
    b += 2 * cDiffForces[iIndex] * _cForces[iIndex];
    c += m_sqr(_cForces[iIndex]);
  }

  marina_real fSMin1,fSMin2;
  QvadruSolve(fSMin1 , fSMin2, a, b, c);
  fSMin = fSMin1;



  if (fSMin >= 0.0f)
  {

    if (IsNumericalZero(fSMin/10000)) // Is it goes to or from zero ?
    {
      marina_real fTestPoint = fSMin + ( (fSMin2 < 0.0f ? 1.0f : fSMin2) - fSMin) / 4.0f; 
      // If in TestPoint configuration is correct it goes from zero.
      marina_real fFrac = 0.0f; 

      iIndex = tHeaderNormal.pBrotherForcesHeaders[1]->iMatrixAIndex;
      if ((iIndex < _pnLastIndexInZone[EMZ_CLAMPED] || iIndex == _pnLastIndexInZone[EMZ_SOLVING -1]) && !tHeaderNormal.pBrotherForcesHeaders[1]->bDeadMouse)
      {
        fFrac += m_sqr(_cForces[iIndex] + fTestPoint * cDiffForces[iIndex]);
      }

      iIndex = tHeaderNormal.pBrotherForcesHeaders[0]->iMatrixAIndex;
      if ((iIndex < _pnLastIndexInZone[EMZ_CLAMPED] || iIndex == _pnLastIndexInZone[EMZ_SOLVING -1]) && !tHeaderNormal.pBrotherForcesHeaders[0]->bDeadMouse)
      {
        fFrac += m_sqr(_cForces[iIndex] + fTestPoint * cDiffForces[iIndex]);
      }

      //iIndex = tHeaderNormal.iMatrixAIndex;
      fFrac /= m_sqr(fNormalForce + fTestPoint * fNormalDiffForce);

      if (fFrac <= fFrictionCoefSqr)
      {
        // yes it goes from zero
        fSMin = fSMin2;
      }
    }
    else
    {
      if (c > 0.0f) // situation is not valid 
        fSMin = 0.0f; 
    }
  }
  else       
  {
    if (c > 0.0f) // situation is not valid 
      fSMin = 0.0f;        
  }


}



void Solver2x2(marina_real * pMatrix, const unsigned int nDim, marina_real * pVector)
{
  if (fabs(pMatrix[0]) < fabs(pMatrix[nDim]))
  {
    pMatrix[1] = pMatrix[1] * pMatrix[nDim] - pMatrix[0] * pMatrix[nDim + 1];
    //  ASSERT(pMatrix[3] != 0,0f);


    float fTemp = pVector[1];
    pVector[1] = pVector[0] * pMatrix[nDim] - pVector[1] * pMatrix[0];

    pVector[1] /= pMatrix[1];

    pVector[0] = fTemp - pMatrix[nDim + 1] * pVector[1];
    pVector[0] /= pMatrix[nDim];
  }
  else
  {
    pMatrix[nDim + 1] = pMatrix[1] * pMatrix[nDim] - pMatrix[0] * pMatrix[nDim + 1];
    //  ASSERT(pMatrix[3] != 0,0f);

    pVector[1] = pVector[0] * pMatrix[nDim] - pVector[1] * pMatrix[0];

    pVector[1] /= pMatrix[nDim + 1];

    pVector[0] -= pMatrix[1] * pVector[1];
    pVector[0] /= pMatrix[0];
  }
}


int DantzigAlgorithm::CalculateDiffs(Vector<marina_real>& cDiffForces, Vector<marina_real>& cDiffAccel, BOOL bPositiveForce)
{     
  START_PROFILE_MARINA(CalcDiffsFindStart);
  _iCalledCalculateDiffs++;
  _iMatrixSize += _pnLastIndexInZone[EMZ_CLAMPED];
  if (_iMaxMatrixSize  < _pnLastIndexInZone[EMZ_CLAMPED])
    _iMaxMatrixSize = _pnLastIndexInZone[EMZ_CLAMPED]; 



  //Profiling
  //_matStepOld += _pnLastIndexInZone[EMZ_CLAMPED] * _pnLastIndexInZone[EMZ_CLAMPED] * _pnLastIndexInZone[EMZ_CLAMPED]* 2 /3;

  /*for(unsigned int i = 1; i <= _pnLastIndexInZone[EMZ_CLAMPED]; i++)
    _matStepOld += i*(i-1)/2;

  if (_decompositionValid + 1 == _pnLastIndexInZone[EMZ_CLAMPED])
  {
    for(unsigned int i = _decompositionValid + 1; i <= _pnLastIndexInZone[EMZ_CLAMPED]; i++)
      _matStepNew += i*(i-1)/2;
  }
  else
  {
    //delete row
    for(unsigned int i = _decompositionValid + 1; i <= _pnLastIndexInZone[EMZ_CLAMPED]; i++) 
    {
      _matStepNew += i *(i-1)/2 - max(0,(_decompositionValid * (_decompositionValid -1) /2));
      _matStepDel += i *(i-1)/2 - max(0,(_decompositionValid * (_decompositionValid -1) /2));
    }
  }*/

  //_decompositionValid = _pnLastIndexInZone[EMZ_CLAMPED];

  // Prepare rigth side
  unsigned int nActual = _pnLastIndexInZone[EMZ_SOLVING] - 1;
  START_PROFILE_MARINA(CalcDiffsGetColumn);
  g_cMatrixA.GetColumn(cDiffForces, nActual, nActual + 1 /* _pnLastIndexInZone[EMZ_CLAMPED]*/);
  END_PROFILE_MARINA(CalcDiffsGetColumn);

  //const TCOLUMNHEADER& tColumnHeader = *_cMappingToColumnID[nActual];
  if (bPositiveForce)
  {
    // In case with friction and positive acceleration the friction force must be negative
    cDiffForces.Negative(nActual + 1);
  }

  // Get copy of "forcies"
  static Vector<marina_real> cLastColumn;
  cLastColumn = cDiffForces;
  cLastColumn.Negative(nActual + 1);
  END_PROFILE_MARINA(CalcDiffsFindStart);

  switch (_pnLastIndexInZone[EMZ_CLAMPED])
  {
  case 0:
    break;
  //case 1: cDiffForces[0] /= g_cMatrixA.GetMemberValue(0,0);
  //  break;
  /*case 2:
    { 
      marina_real * pTempMatrix = new marina_real[g_cMatrixA.GetDim() * 2];

      memcpy( pTempMatrix, g_cMatrixA.GetPointer(), g_cMatrixA.GetDim() * 2 * sizeof(marina_real));     

      Solver2x2(pTempMatrix, g_cMatrixA.GetDim(), cDiffForces.GetPointer());      

      delete pTempMatrix;
    }
    break;
*/
  default:
    {
#define USE_CHOLESKY 1
//#ifdef  USE_CHOLESKY    
      //DebugPoint();
           
      //find first index where is matrix nonsymetrics
      START_PROFILE_MARINA(CalcDiffsFindSymetric);
      unsigned int iNonSymetry = 0;
      for(; iNonSymetry < _pnLastIndexInZone[EMZ_CLAMPED]; iNonSymetry++)
      {        
        if (_cMappingToColumnID[iNonSymetry]->unsymetric != 0)
          break;
      }
      END_PROFILE_MARINA(CalcDiffsFindSymetric);



      int decompositionValidOld = _decompositionValid;
      //M_VECTOR diffForcesOld;
      //diffForcesOld.Init(cDiffForces.GetDim());
     // diffForcesOld = cDiffForces;
     /* START_PROFILE_MARINA(LUDecom);

      int n = _pnLastIndexInZone[EMZ_CLAMPED];      

      g_luMatrix.ValidSize(_decompositionValid);

      START_PROFILE_MARINA(LUDecomAddIndex);
      for(int i = _decompositionValid; i < n; i++)
        //_luMatrix.AddIndex(g_cMatrixA.GetIndexes()[i],g_cMatrixA);
        g_luMatrix.AddIndex(i,g_cMatrixA);
      END_PROFILE_MARINA(LUDecomAddIndex);

      _decompositionValid = n;

      {
        
        START_PROFILE_MARINA(LUDecomSolve);
        g_luMatrix.Solve(diffForcesOld, g_cMatrixA);
        END_PROFILE_MARINA(LUDecomSolve);            
      }
      
      END_PROFILE_MARINA(LUDecom);*/

     /* {        
        M_VECTOR diffForcesOld;
        //diffForcesOld.Init(cDiffForces.GetDim());
        diffForcesOld = cDiffForces;
        START_PROFILE_MARINA(LUDecom2);

        int n = _pnLastIndexInZone[EMZ_CLAMPED];      

        g_luMatrix2.ValidSize(decompositionValidOld);

        if (iNonSymetry < n)
        {
          START_PROFILE_MARINA(LUDecomAddIndex2NonSym);        
          for(int i = decompositionValidOld; i < n; i++)        
            g_luMatrix2.AddIndex2(i,g_cMatrixA);
          END_PROFILE_MARINA(LUDecomAddIndex2NonSym);     

        }
        else
        {        
          START_PROFILE_MARINA(LUDecomAddIndex2Sym);        
          for(int i = decompositionValidOld; i < n; i++)        
            g_luMatrix2.AddIndex2(i,g_cMatrixA);
          END_PROFILE_MARINA(LUDecomAddIndex2Sym);        
        }

        _decompositionValid = n;

        {

          START_PROFILE_MARINA(LUDecomSolve2);
          g_luMatrix2.Solve2(diffForcesOld, g_cMatrixA);
          END_PROFILE_MARINA(LUDecomSolve2);            
        }

        END_PROFILE_MARINA(LUDecom2);
      }*/

      {        
        //M_VECTOR diffForcesOld;
        //diffForcesOld.Init(cDiffForces.GetDim());
        //diffForcesOld = cDiffForces;
        START_PROFILE_MARINA(LUDecom3);

        unsigned int n = _pnLastIndexInZone[EMZ_CLAMPED];      

        g_luMatrix3.ValidSize(decompositionValidOld);

        if (iNonSymetry < n)
        {
          START_PROFILE_MARINA(LUDecomAddIndex3NonSym);        
          for(unsigned int i = decompositionValidOld; i < n; i++) 
          {                                     
            if (!g_luMatrix3.AddIndex3(i,iNonSymetry,g_cMatrixA,_cMappingToColumnID[i]->unsymetric == 0))           
            {
              // force is numericaly lineary dependent
              _decompositionValid = i;              
              END_PROFILE_MARINA(LUDecomAddIndex3NonSym);  
              END_PROFILE_MARINA(LUDecom3);
              return i;
            }
          }
          END_PROFILE_MARINA(LUDecomAddIndex3NonSym);     

        }
        else
        {        
          START_PROFILE_MARINA(LUDecomAddIndex3Sym);        
          for(unsigned int i = decompositionValidOld; i < n; i++)        
            if (!g_luMatrix3.AddIndex3(i,iNonSymetry,g_cMatrixA, true))
            {
              // force is numericaly lineary dependent
              _decompositionValid = i;              
              END_PROFILE_MARINA(LUDecomAddIndex3Sym); 
              END_PROFILE_MARINA(LUDecom3);
              return i;
            }
          END_PROFILE_MARINA(LUDecomAddIndex3Sym);        
        }

        _decompositionValid = n;

        {
          START_PROFILE_MARINA(LUDecomSolve3);
          g_luMatrix3.Solve3(cDiffForces, iNonSymetry, g_cMatrixA);
          END_PROFILE_MARINA(LUDecomSolve3);            
        }

        END_PROFILE_MARINA(LUDecom3);
        if (_iMaxTime < (unsigned int) DiffI_LUDecom3/2000)
          _iMaxTime =  (unsigned int) DiffI_LUDecom3/2000;
      }
      
      /*{

        START_PROFILE_MARINA(LUSparseDecom);

        int n = _pnLastIndexInZone[EMZ_CLAMPED];       

        g_luSparseMatrix.ValidSize(decompositionValidOld);

        START_PROFILE_MARINA(LUSparseDecomAddIndex);
        for(int i = decompositionValidOld; i < n; i++)
          //_luMatrix.AddIndex(g_cMatrixA.GetIndexes()[i],g_cMatrixA);
          g_luSparseMatrix.AddIndex(i,g_cMatrixA);        
        END_PROFILE_MARINA(LUSparseDecomAddIndex);

        _decompositionValid = n;

        START_PROFILE_MARINA(LUSparseDecomSolve);
        g_luSparseMatrix.Solve(diffForcesOld, g_cMatrixA);                          
        END_PROFILE_MARINA(LUSparseDecomSolve);

        END_PROFILE_MARINA(LUSparseDecom);
      }*/

     /* for(int i = 0; i < n; i++)
      {
        ASSERT(_finite(cDiffForces[i]));
        ASSERT(!_isnan(cDiffForces[i]));
      }*/

 
/*
      if (_pnLastIndexInZone[EMZ_NOTCLAMPED] == _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED])
      {            
        PROFILE_SCOPE_MARINA(cholesky);
        CholeskyMatrix mat;
        mat.Decomp(n,g_cMatrixA,g_cMatrixA.GetIndexes());
                
        M_VECTOR out;
        out.Init(cDiffForces.GetDim());
        mat.Solve(out,cDiffForces, g_cMatrixA);
        for(int i = 0; i < n; i++)          
          cDiffForces[i] = out[i];
      }
      else
      {
        PROFILE_SCOPE_MARINA(LUDecom);
        LUMatrix mat;
        mat.Decomp(n,g_cMatrixA,g_cMatrixA.GetIndexes());
        //delete [] indexes;

        M_VECTOR out;
        out.Init(cDiffForces.GetDim());
        mat.Solve(out,cDiffForces, g_cMatrixA);
        for(int i = 0; i < n; i++)          
          cDiffForces[i] = out[i];
      }*/
//#else      
 //     if(!_bLUDecompositionValid)
     //{     
     //   PROFILE_SCOPE_MARINA(mtl);
     //   SolveNxN(_pnLastIndexInZone[EMZ_CLAMPED], g_cMatrixA.GetDim(), g_cMatrixA.GetPointer(), cDiffForces.GetPointer());
     //   _bLUDecompositionValid = TRUE;
     // }
        //for(int i = 0; i < n; i++)
        //  ASSERT(fabs((cDiffForces[i] - out[i])/out[1]) < 0.01f);
  //    }
  //    else
  //    {            
  //      SolveNxNOld(cDiffForces.GetPointer());
  //    }
//#endif
    }

  }


  START_PROFILE_MARINA(CalcDiffsFindEnd);
  // Nonclaped forces are 0, actual force is 1.
  // Calculate diff accelerations
  g_cMatrixA.MultiplyByShortVectorFromTo(cDiffAccel, cDiffForces, _pnLastIndexInZone[EMZ_CLAMPED], 
    _pnLastIndexInZone[EMZ_CLAMPED], _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] - 1);
  

  for(unsigned int i = _pnLastIndexInZone[EMZ_CLAMPED]; i < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]; i++)
    cDiffAccel[i] += cLastColumn[i];

  cDiffAccel[nActual] = g_cMatrixA.RowTimesVector(nActual,cDiffForces, _pnLastIndexInZone[EMZ_CLAMPED]);
  cDiffAccel[nActual] += cLastColumn[nActual];
  cDiffForces[nActual] = (bPositiveForce) ? 1.0f : -1.0f;

  END_PROFILE_MARINA(CalcDiffsFindEnd);
  // calculate DiffForces for not clamped 
  PROFILE_SCOPE_MARINA(CalcDiffsNotClampedFric);
  for(unsigned int i = 0; i < _pnLastIndexInZone[EMZ_CLAMPED]; i++)
  {
    const TCOLUMNHEADER &tHeaderNormal = *(_cMappingToColumnID[i]);
    if (tHeaderNormal.eForceType == EFT_STATIC && tHeaderNormal.iNotClampedIndex != 0)
    {

      ASSERT(tHeaderNormal.pBrotherForcesHeaders[tHeaderNormal.iNotClampedIndex - 1]->iMatrixAIndex >= _pnLastIndexInZone[EMZ_NOTCLAMPED] &&
        tHeaderNormal.pBrotherForcesHeaders[tHeaderNormal.iNotClampedIndex - 1]->iMatrixAIndex < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]);

      cDiffForces[tHeaderNormal.pBrotherForcesHeaders[tHeaderNormal.iNotClampedIndex - 1]->iMatrixAIndex] = 
        cDiffForces[i] * tHeaderNormal.fSlidingFrictionCoef;

    }
  }

  return -1;
#ifdef _DEBUG

  /*  if (_pnLastIndexInZone[EMZ_CLAMPED] > 0)
  {

  g_cMatrixA.MultiplyByShortVectorFromTo(cDiffAccel, cDiffForces, _pnLastIndexInZone[EMZ_CLAMPED], 
  0,_pnLastIndexInZone[EMZ_CLAMPED] - 1);

  for(unsigned int iii = 0; iii <_pnLastIndexInZone[EMZ_CLAMPED]; iii++)
  {
  cDiffAccel[iii] += cLastColumn[iii];
  if (!IsNumericalZero(cDiffAccel[iii]/10.0f))
  {
  int i = 3;
  }
  //  ASSERT(IsNumericalZero(cDiffAccel[iii]/10.0f));
  }
  }
  */
#endif
}

BOOL DantzigAlgorithm::CalculateMaximalAllowedStep( marina_real& fSMin, unsigned int &iMinIndex, 
                                                    Vector<marina_real>& cDiffForces, Vector<marina_real>& cDiffAccel)
{
  PROFILE_SCOPE_MARINA(maxallowed);
  // Clamped
  unsigned int iIndex = 0;
  for (; iIndex < _pnLastIndexInZone[EMZ_CLAMPED]; iIndex++)
  {
    TCOLUMNHEADER& tColumnHeaderTemp = *_cMappingToColumnID[iIndex];    

    marina_real fS = -1.0f;

    if (tColumnHeaderTemp.eForceType == EFT_FRICTION)
    {
      TCOLUMNHEADER& tColumnHeaderNormal = *(tColumnHeaderTemp.pBrotherForcesHeaders[0]);

#if _USE_PEDESTALS
      if (tColumnHeaderNormal.pedestal && tColumnHeaderNormal.pedestal->nClamped == 3)
        continue;
#endif

      // the second friction must be also clamped or unsolved
      ASSERT(tColumnHeaderTemp.pBrotherForcesHeaders[1]->iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED] ||
        tColumnHeaderTemp.pBrotherForcesHeaders[1]->iMatrixAIndex >= _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]);

      // the normal force must be clamped
      ASSERT(tColumnHeaderNormal.iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED]);

      CalculateMaximalAllowedStepForFrictionPoint( fS, 
        tColumnHeaderNormal, 
        _cForces[tColumnHeaderNormal.iMatrixAIndex],
        cDiffForces[tColumnHeaderNormal.iMatrixAIndex],
        cDiffForces);
    }
    else
    {
      // The normal forces
#if _USE_PEDESTALS      
      if (tColumnHeaderTemp.pedestal && tColumnHeaderTemp.pedestal->nClamped == 3)
      {
        if (tColumnHeaderTemp.pedestal->min == tColumnHeaderTemp.iContactPointID)
        {
          // Pedestal
          TPEDESTAL & ped = *tColumnHeaderTemp.pedestal;

//#define CONTACTID_TO_MATRIXINDEX(i) (_cColumnHeaders[(i) * 3].iMatrixAIndex)

          Vector3 forces(_cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
            _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
            _cForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

          Vector3 realForces = ped.toRealCalmped * forces;

          if (realForces[0] < 0)
          {
            realForces[0] = 0;          
            ASSERT(IsNumericalZero(realForces[0]));
          }
          if (realForces[1] < 0)
          {
            realForces[1] = 0;          
            ASSERT(IsNumericalZero(realForces[1]));
          }
          if (realForces[2] < 0)
          {
            realForces[2] = 0;          
            ASSERT(IsNumericalZero(realForces[2]));
          }

          Vector3 diffForces(cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[0])], 
            cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[1])],
            cDiffForces[CONTACTID_TO_MATRIXINDEX(ped.clamped[2])]);

          Vector3 realDiffForces = ped.toRealCalmped * diffForces;

          if (realDiffForces[0] < 0)
          {
            fS = - realForces[0] / realDiffForces[0];
            ped.susp = 0;
          }

          float fS2;
          if (realDiffForces[1] < 0)
          {
            fS2= - realForces[1] / realDiffForces[1];

            if (fS != umin<marina_real>(fS, fS2))
            {
              ped.susp = 1;
              fS = fS2;
            }
          }

          if (realDiffForces[2] < 0)
          {
            fS2 = - realForces[2] / realDiffForces[2];
            if (fS != umin<marina_real>(fS, fS2))
            {
              ped.susp = 2;
              fS = fS2;
            }
          }
        
        /* // if normal force is clamped and it will reach zero move friction between unsolved
        if (tColumnHeaderTemp.iNotClampedIndex != 0)
        {
          float fS2 = -_cForces[iIndex] / cDiffForces[iIndex];  
          if ( fS != umin<marina_real>(fS2, fS))
          {
            if (fSMin != umin<marina_real>(fS2, fSMin))
            {            
              fSMin = fS2;
              iMinIndex = tColumnHeaderTemp.pBrotherForcesHeaders[tColumnHeaderTemp.iNotClampedIndex -1]->iMatrixAIndex;
            }
          }
        }*/

          // Calculate friction
          for(int j = 0; j < 3; j++)
          {
            if (realForces[j] > 0)
            {
              TCOLUMNHEADER& tNormal = _cColumnHeaders[3 * ped.realClamped[j]];
              if (tNormal.iNotClampedIndex == 0 &&
                (tNormal.pBrotherForcesHeaders[0]->iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED] ||
                tNormal.pBrotherForcesHeaders[1]->iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED]))
              {
                CalculateMaximalAllowedStepForFrictionPoint(fS2, tNormal, realForces[j], realDiffForces[j], cDiffForces);
                if ( fS != umin<marina_real>(fS2, fS))
                {
                  if (fSMin != umin<marina_real>(fS2, fSMin))
                  {            
                    fSMin = fS2;
                    iMinIndex = tNormal.pBrotherForcesHeaders[0]->iMatrixAIndex < _pnLastIndexInZone[EMZ_CLAMPED] ? 
                      tNormal.pBrotherForcesHeaders[0]->iMatrixAIndex : tNormal.pBrotherForcesHeaders[1]->iMatrixAIndex;
                  }
                }
              }
            }
          }
        }
      }
      else 
#endif        
      if (cDiffForces[iIndex] < 0.0f)
      {        
        if (_cForces[iIndex] == 0)
        {
          // This collision was switch C<->NC last time 
          // Mark it like a unsolved, 
          fS = 0.0f;      
        }
        else
        {
          fS = -_cForces[iIndex] / cDiffForces[iIndex];
          if (fS < 0.0f)
            fS = 0.0f;
        }
      }
    }

    if ( fSMin != umin<marina_real>(fS, fSMin))
    {
      fSMin = fS;
      iMinIndex = iIndex;
    }
  }

  // Not clamped 
  for(; iIndex < _pnLastIndexInZone[EMZ_NOTCLAMPED];iIndex++)
  {
    TCOLUMNHEADER& tColumnHeaderNormal = *_cMappingToColumnID[iIndex];  
    
    ASSERT(tColumnHeaderNormal.eForceType == EFT_STATIC);

    if (tColumnHeaderNormal.pedestal && tColumnHeaderNormal.pedestal->nClamped == 3)
      continue;

    // here are only normal forces
    if (cDiffAccel[iIndex] < 0.0f && !IsNumericalZero(cDiffAccel[iIndex]/100.0f))
    {
      marina_real fS = -1.0f;
      if (_cAccelerations[iIndex] == 0.0f)
      {
        // This iIndex was switched C<->NC last time 
        // Mark it like a unsolved, 
        fS = 0.0f;        
      }
      else
      {           
        fS = -_cAccelerations[iIndex] / cDiffAccel[iIndex];
        if (fS < 0.0f)
          fS = 0.0f;
      }

      if ( fSMin != umin<marina_real>(fS, fSMin))
      {
        fSMin = fS;
        iMinIndex = iIndex;
      }
    }
  }

  // Not clamped friction forces
  for(; iIndex < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED];iIndex++)
  {
#ifdef _DEBUG   
    TCOLUMNHEADER& tColumnHeaderTemp = *_cMappingToColumnID[iIndex];  
    ASSERT(tColumnHeaderTemp.eForceType == EFT_FRICTION);

    const TCOLUMNHEADER& tBrotherFrictionHeader = *(tColumnHeaderTemp.pBrotherForcesHeaders[1]);
    unsigned int iBrotherFrictionForceIndex = tBrotherFrictionHeader.iMatrixAIndex;
    ASSERT(iBrotherFrictionForceIndex >= _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]); 
#endif

    marina_real fAccelInNotClampedDir = _cAccelerations[iIndex];
    marina_real fDiffAccelInNotClampedDir = cDiffAccel[iIndex];

    marina_real fS = -1.0f; 
    if (!IsNumericalZero(fDiffAccelInNotClampedDir/100.0f))    
      fS = - fAccelInNotClampedDir / fDiffAccelInNotClampedDir;          

    if ( fSMin != umin<marina_real>(fS, fSMin))
    {
      fSMin = fS;
      iMinIndex = iIndex;
    } 
  }

  return FALSE;
}

void DantzigAlgorithm::SwitchCollisions(unsigned int iColl1, unsigned int iColl2)
{
  if (iColl1 == iColl2)
    return;

  PROFILE_SCOPE_MARINA(SwitchCollisions);
  {
  PROFILE_SCOPE_MARINA(SwitchCollisionsMatrix);
  g_cMatrixA.SwitchCoordinates(iColl1, iColl2);
  }

  marina_real fTemp = _cVectorB[iColl1];
  _cVectorB[iColl1] = _cVectorB[iColl2];
  _cVectorB[iColl2] = fTemp;

  fTemp = _cForces[iColl1];
  _cForces[iColl1] = _cForces[iColl2];
  _cForces[iColl2] = fTemp;

  fTemp = _cAccelerations[iColl1];
  _cAccelerations[iColl1] = _cAccelerations[iColl2];
  _cAccelerations[iColl2] = fTemp;

  PTCOLUMNHEADER pcTemp = _cMappingToColumnID[iColl1];
  _cMappingToColumnID[iColl1] = _cMappingToColumnID[iColl2];
  _cMappingToColumnID[iColl2] = pcTemp;

  _cMappingToColumnID[iColl1]->iMatrixAIndex = iColl1;
  _cMappingToColumnID[iColl2]->iMatrixAIndex = iColl2;
}


void DantzigAlgorithm::MoveToZoneEx(unsigned int iIndex, unsigned int iToZone, unsigned int iFromZone /*= 0*/)
{
  //PROFILE_SCOPE_MARINA(movetozone);
  while( iIndex >= _pnLastIndexInZone[iFromZone])
    iFromZone ++;

  if (iFromZone == iToZone)
    return;  // trivial case

  TCOLUMNHEADER& tColumnHeader = *_cMappingToColumnID[iIndex];

  //Log("MoveToZoneEx: SourceMatrixIndex = %d, MatrixIndex = %d, iFromZone = %d, iToZone = %d",
  //  tColumnHeader.iSourceMatrixAIndex, tColumnHeader.iMatrixAIndex, iFromZone, iToZone);

  switch (tColumnHeader.eForceType)
  {
  case EFT_FRICTION:
    {     
      MoveToZone(iIndex, iToZone, iFromZone);
      tColumnHeader.iLastChangeLocalCycle = _iLocalCycle;
      break;
    }
  case EFT_STATIC:
    {
      if (iFromZone == EMZ_CLAMPED)
      {
        if (tColumnHeader.iNotClampedIndex != 0)
        {
          // not clamped friction in point
          RevertColumnInMatrixA(iIndex);
          tColumnHeader.iNotClampedIndex = 0;
          tColumnHeader.pBrotherForcesHeaders[0]->iNotClampedIndex = 0;
          tColumnHeader.pBrotherForcesHeaders[1]->iNotClampedIndex = 0;
        }

        /*if (!tColumnHeader.pBrotherForcesHeaders[0]->bDeadMouse)
        {
          int i = 0;
          for(; i < _cReleaseList.Size(); i++)
            if (_cReleaseList[i]->iMatrixAIndex == tColumnHeader.pBrotherForcesHeaders[0]->iMatrixAIndex)
              break;

          if (i != _cReleaseList.Size())
            _cForces[tColumnHeader.pBrotherForcesHeaders[0]->iMatrixAIndex] = 0.0f;
        }

        if (!tColumnHeader.pBrotherForcesHeaders[1]->bDeadMouse)
        {
          int i = 0;
          for(; i < _cReleaseList.Size(); i++)
            if (_cReleaseList[i]->iMatrixAIndex == tColumnHeader.pBrotherForcesHeaders[1]->iMatrixAIndex)
              break;

          if (i != _cReleaseList.Size())
            _cForces[tColumnHeader.pBrotherForcesHeaders[1]->iMatrixAIndex] = 0.0f;
        }*/
          

        if (iToZone >= EMZ_NOTCLAMPED)
        {
          if (!tColumnHeader.pBrotherForcesHeaders[0]->bDeadMouse)
            MoveToZone(tColumnHeader.pBrotherForcesHeaders[0]->iMatrixAIndex,EMZ_UNSOLVED);

          if (!tColumnHeader.pBrotherForcesHeaders[1]->bDeadMouse)
            MoveToZone(tColumnHeader.pBrotherForcesHeaders[1]->iMatrixAIndex,EMZ_UNSOLVED);
        }

        /*if (iToZone == EMZ_UNSOLVED)
        {
          if (!tColumnHeader.pBrotherForcesHeaders[0]->bDeadMouse)
            MoveToZone(tColumnHeader.pBrotherForcesHeaders[0]->iMatrixAIndex,EMZ_UNSOLVED);

          if (!tColumnHeader.pBrotherForcesHeaders[1]->bDeadMouse)
            MoveToZone(tColumnHeader.pBrotherForcesHeaders[1]->iMatrixAIndex,EMZ_UNSOLVED);
        }*/
      }

      /*if (iFromZone == EMZ_NOTCLAMPED)
      {
        if (!tColumnHeader.pBrotherForcesHeaders[0]->bDeadMouse)
          MoveToZone(tColumnHeader.pBrotherForcesHeaders[0]->iMatrixAIndex,EMZ_UNSOLVED);

        if (!tColumnHeader.pBrotherForcesHeaders[1]->bDeadMouse)
          MoveToZone(tColumnHeader.pBrotherForcesHeaders[1]->iMatrixAIndex,EMZ_UNSOLVED);
      }*/

      MoveToZone(tColumnHeader.iMatrixAIndex, iToZone, iFromZone);
      tColumnHeader.iLastChangeLocalCycle = _iLocalCycle;

      /*
      if (iToZone == EMZ_NOTCLAMPED)
      {
      ChooseNewPedestalPoint(tColumnHeader.iMatrixAIndex);  
      }*/


      break;
    }
  default:
    ASSERT(FALSE);
  }

  //Log("MovedToZoneEx: SourceMatrixIndex = %d, MatrixIndex = %d, iFromZone = %d, iToZone = %d, _pnLastIndexInZone[EMZ_CLAMPED] = %d",
  //  tColumnHeader.iSourceMatrixAIndex, tColumnHeader.iMatrixAIndex, iFromZone, iToZone, _pnLastIndexInZone[EMZ_CLAMPED]);

  return;
}

void DantzigAlgorithm::MoveToZone(unsigned int iIndex, unsigned int iToZone, unsigned int iFromZone /*= 0*/)
{
  START_PROFILE_MARINA(movetozone);
  while( iIndex >= _pnLastIndexInZone[iFromZone])
    iFromZone ++;

  if (iFromZone == iToZone)
  {
    END_PROFILE_MARINA(movetozone);
    return;  // trivial case
  }

  //Log("MoveToZone:   SourceMatrixIndex = %d, MatrixIndex = %d, iFromZone = %d, iToZone = %d",
  //  _cMappingToColumnID[iIndex]->iSourceMatrixAIndex, iIndex, iFromZone, iToZone);

  if (iFromZone == EMZ_CLAMPED || iToZone == EMZ_CLAMPED)
    _bLUDecompositionValid = FALSE;

  TCOLUMNHEADER & tHeader = *_cMappingToColumnID[iIndex];
  if (iFromZone == EMZ_CLAMPED)
  {
    _decompositionValid = min(_decompositionValid,iIndex);
    _numberOfClamped[tHeader.iNumberOfClamped]--;
    if (tHeader.eForceType == EFT_STATIC && tHeader.pedestal)
      tHeader.pedestal->RemoveClamped(tHeader.iContactPointID);

  }

  if (iToZone == EMZ_CLAMPED)
  {
    _numberOfClamped[tHeader.iNumberOfClamped]++;
    if (tHeader.eForceType == EFT_STATIC && tHeader.pedestal)
    {
      tHeader.pedestal->AddClamped(tHeader.iContactPointID);
#if _USE_PEDESTALS
      if (tHeader.pedestal->nClamped == 3)
      {
        TPEDESTAL& ped = *tHeader.pedestal;
        // add to unsymetric for each clamped.
        for(int i = 0; i < 3; i++)
        {
          TCOLUMNHEADER& tHeaderNormal = _cColumnHeaders[3 * ped.realClamped[i]];
          if (tHeaderNormal.iNotClampedIndex > 0)
          {
            for(int j = 0; j < 3; j++)
              if (i != j)
              {
                _cColumnHeaders[3 * ped.clamped[j]].unsymetric++;            
                _decompositionValid = min(_decompositionValid, _cColumnHeaders[3 * ped.clamped[j]].iMatrixAIndex);
              }
          }
        }
      }
#endif
    }
  }
  


  // semitrivial cases
  if (iFromZone + 1 == iToZone)
  {
    SwitchCollisions(iIndex, _pnLastIndexInZone[iFromZone] - 1);
    _pnLastIndexInZone[iFromZone]--;
    END_PROFILE_MARINA(movetozone);
    return;
  }

  if (iFromZone == iToZone + 1)
  {
    SwitchCollisions(iIndex, _pnLastIndexInZone[iToZone]);
    _pnLastIndexInZone[iToZone]++;
    END_PROFILE_MARINA(movetozone);
    return;
  }

  // nontrivial case
  if (iFromZone < iToZone)
  {
    // move down all form iIndex + 1 to _pnLastIndexInZone[iToZone]
    unsigned int iToIndex = _pnLastIndexInZone[iToZone] - 1;

    if (iIndex != iToIndex)
    {
      START_PROFILE_MARINA(movetozoneMove);
      _cVectorB.MoveUp(iIndex, iToIndex);
      _cForces.MoveUp(iIndex, iToIndex);
      _cAccelerations.MoveUp(iIndex, iToIndex);
      START_PROFILE_MARINA(movetozoneMatrix);
      g_cMatrixA.MoveUp(iIndex, iToIndex);
      END_PROFILE_MARINA(movetozoneMatrix);

      _cMappingToColumnID.MoveUp(iIndex, iToIndex);
      END_PROFILE_MARINA(movetozoneMove);
    }

    for(unsigned int iZone = iFromZone; iZone < iToZone; iZone++ )
      _pnLastIndexInZone[iZone]--;    

    for(unsigned int iIndexTemp = iIndex; iIndexTemp <= iToIndex; iIndexTemp++ )
      _cMappingToColumnID[iIndexTemp]->iMatrixAIndex = iIndexTemp;
  }
  else
  {
    // move up all form iIndex + 1 to _pnLastIndexInZone[iToZone]
    unsigned int iToIndex = _pnLastIndexInZone[iToZone];

    if (iIndex != iToIndex)
    {
      START_PROFILE_MARINA(movetozoneMove);
      _cVectorB.MoveDown(iIndex, iToIndex );
      _cForces.MoveDown(iIndex, iToIndex );
      _cAccelerations.MoveDown(iIndex, iToIndex );
      START_PROFILE_MARINA(movetozoneMatrix);
      g_cMatrixA.MoveDown(iIndex, iToIndex );
      END_PROFILE_MARINA(movetozoneMatrix);
      
      _cMappingToColumnID.MoveDown(iIndex, iToIndex);
      END_PROFILE_MARINA(movetozoneMove);
    }

    for(unsigned int iZone = iToZone; iZone < iFromZone; iZone++ )
      _pnLastIndexInZone[iZone]++;    


    for(unsigned int iIndexTemp = iToIndex; iIndexTemp <= iIndex; iIndexTemp++ )
      _cMappingToColumnID[iIndexTemp]->iMatrixAIndex = iIndexTemp;
  }
  END_PROFILE_MARINA(movetozone);
}

void DantzigAlgorithm::MoveToZoneEnd(unsigned int iIndex, unsigned int iZone)
{
  START_PROFILE_MARINA(movetozone);
  ASSERT( iIndex < _pnLastIndexInZone[iZone]);
  ASSERT(iZone == 0 || iIndex >= _pnLastIndexInZone[iZone - 1]);

  unsigned int iToIndex = _pnLastIndexInZone[iZone] - 1;
   
  _decompositionValid = min(_decompositionValid,iIndex);

  if (iIndex != iToIndex)
  {
    _cVectorB.MoveUp(iIndex, iToIndex);
    _cForces.MoveUp(iIndex, iToIndex);
    _cAccelerations.MoveUp(iIndex, iToIndex);
    g_cMatrixA.MoveUp(iIndex, iToIndex);

    _cMappingToColumnID.MoveUp(iIndex, iToIndex);
  }

  for(unsigned int iIndexTemp = iIndex; iIndexTemp <= iToIndex; iIndexTemp++ )
    _cMappingToColumnID[iIndexTemp]->iMatrixAIndex = iIndexTemp;
  
  END_PROFILE_MARINA(movetozone);
}

void DantzigAlgorithm::RevertColumnInMatrixA(unsigned int iIndex)
{
  TCOLUMNHEADER & header = *_cMappingToColumnID[iIndex];

  // MUST be normal, friction axes can be changed
  ASSERT(header.eForceType == EFT_STATIC);
  ASSERT(header.iNotClampedIndex > 0);


  unsigned int frictionIndex = header.pBrotherForcesHeaders[header.iNotClampedIndex - 1]->iMatrixAIndex;
  ASSERT(frictionIndex >= _pnLastIndexInZone[EMZ_NOTCLAMPED] && frictionIndex < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]);  

#if _USE_PEDESTALS
  // maybe is part of pedestal
  if (header.pedestal && header.pedestal->nClamped == 3)
  {
    TPEDESTAL & ped = *header.pedestal;
    unsigned int indx = ped.RealClampedIndx(header.iContactPointID);
    for(unsigned int i = 0; i < 3; i++)
    {    
      
      if (true) //!IsNumericalZero(ped.toRealCalmped(indx,i)))
      {
        int normIndx = CONTACTID_TO_MATRIXINDEX(ped.clamped[i]);
        g_cMatrixA.CombinateColumns(normIndx, frictionIndex, - header.fSlidingFrictionCoef * ped.toRealCalmped(indx,i));
        _decompositionValid = min(_decompositionValid,normIndx);
        ASSERT(_cMappingToColumnID[normIndx]->unsymetric <= 3 && _cMappingToColumnID[normIndx]->unsymetric > 0);
        _cMappingToColumnID[normIndx]->unsymetric--;
        
      }
    }
  }
  else
#endif
  {
    _decompositionValid = min(_decompositionValid,iIndex);

    g_cMatrixA.CombinateColumns(iIndex, frictionIndex, - header.fSlidingFrictionCoef);
    ASSERT(_cMappingToColumnID[iIndex]->unsymetric <= 3 && _cMappingToColumnID[iIndex]->unsymetric > 0);
    _cMappingToColumnID[iIndex]->unsymetric--;
    


#ifdef _DEBUG
    unsigned int iSourceIndex = _cMappingToColumnID[iIndex]->iSourceMatrixAIndex;
    for(unsigned int iIndexTemp = 0; iIndexTemp < _nNumberOfForces; iIndexTemp++)
    {
      float val = g_cMatrixA.GetMemberValue( iIndexTemp, iIndex) -
        _cSourceMatrixA.GetMemberValue(_cMappingToColumnID[iIndexTemp]->iSourceMatrixAIndex,iSourceIndex);
      if (!IsNumericalZero(val))
        ASSERT(FALSE);  
    }
 
#endif
  }
    
  _bLUDecompositionValid = FALSE;

}

void DantzigAlgorithm::AddFrictionIntoNormal(TCOLUMNHEADER& normal, unsigned int frictionIndx, float coef)
{
  _decompositionValid = min(_decompositionValid,normal.iMatrixAIndex);
  _bLUDecompositionValid = false;

  g_cMatrixA.CombinateColumns(normal.iMatrixAIndex, frictionIndx, coef);
    
  normal.unsymetric++;
}


void DantzigAlgorithm::ChangeFrictionAxes(TCOLUMNHEADER &tHeaderFriction1, const Vector3& cNewDir1)
{
  TCOLUMNHEADER &tHeaderFriction2 = *(tHeaderFriction1.pBrotherForcesHeaders[1]);

  // both are friction
  ASSERT(tHeaderFriction1.eForceType == EFT_FRICTION && tHeaderFriction2.eForceType == EFT_FRICTION)
  // the friction must not be clamped and both must be in the same zone
  //ASSERT((tHeaderFriction1.iMatrixAIndex < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] && tHeaderFriction2.iMatrixAIndex < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]) ||
  //    (tHeaderFriction1.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING] && tHeaderFriction2.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING]));

  // projection matrix
  marina_real pfProjectionMatrix[4];

  pfProjectionMatrix[0] = cNewDir1[0];
  pfProjectionMatrix[1] = cNewDir1[1];
  pfProjectionMatrix[2] = cNewDir1[1];
  pfProjectionMatrix[3] = -cNewDir1[0];

  unsigned int minIndexTouched = min(tHeaderFriction1.iMatrixAIndex,tHeaderFriction1.iMatrixAIndex);
  _decompositionValid = min(_decompositionValid, minIndexTouched);
  // update matrixes
  g_cMatrixA.CombineCoordinates(tHeaderFriction1.iMatrixAIndex, tHeaderFriction2.iMatrixAIndex, pfProjectionMatrix);
  

#ifdef _DEBUG  
  _cSourceMatrixA.CombineCoordinates(tHeaderFriction1.iSourceMatrixAIndex, tHeaderFriction2.iSourceMatrixAIndex, pfProjectionMatrix);
#endif
  
  //_bLUDecompositionValid = FALSE; BE WARE not needed but be ware.

  // update vectors
  marina_real fIn1, fIn2;

  fIn1 = _cVectorB[tHeaderFriction1.iMatrixAIndex];
  fIn2 = _cVectorB[tHeaderFriction2.iMatrixAIndex];

  _cVectorB[tHeaderFriction1.iMatrixAIndex] = pfProjectionMatrix[0] * fIn1 + pfProjectionMatrix[1] * fIn2;
  _cVectorB[tHeaderFriction2.iMatrixAIndex] = pfProjectionMatrix[2] * fIn1 + pfProjectionMatrix[3] * fIn2;

  fIn1 = _cForces[tHeaderFriction1.iMatrixAIndex];
  fIn2 = _cForces[tHeaderFriction2.iMatrixAIndex];

  _cForces[tHeaderFriction1.iMatrixAIndex] = pfProjectionMatrix[0] * fIn1 + pfProjectionMatrix[1] * fIn2;
  _cForces[tHeaderFriction2.iMatrixAIndex] = pfProjectionMatrix[2] * fIn1 + pfProjectionMatrix[3] * fIn2;

  fIn1 = _cAccelerations[tHeaderFriction1.iMatrixAIndex];
  fIn2 = _cAccelerations[tHeaderFriction2.iMatrixAIndex];

  _cAccelerations[tHeaderFriction1.iMatrixAIndex] = pfProjectionMatrix[0] * fIn1 + pfProjectionMatrix[1] * fIn2;
  _cAccelerations[tHeaderFriction2.iMatrixAIndex] = pfProjectionMatrix[2] * fIn1 + pfProjectionMatrix[3] * fIn2;

  // update ContactPoint

  Vector3 cNewRealDir = ContactPointDir(tHeaderFriction1.iContactPointID, tHeaderFriction1.iDirIndex) * pfProjectionMatrix[0] + 
    ContactPointDir(tHeaderFriction1.iContactPointID, tHeaderFriction2.iDirIndex) * pfProjectionMatrix[1];

  ContactPointDir(tHeaderFriction1.iContactPointID, tHeaderFriction2.iDirIndex) = ContactPointDir(tHeaderFriction1.iContactPointID, tHeaderFriction1.iDirIndex) * pfProjectionMatrix[2] +
    ContactPointDir(tHeaderFriction1.iContactPointID, tHeaderFriction2.iDirIndex) * pfProjectionMatrix[3];

  ContactPointDir(tHeaderFriction1.iContactPointID, tHeaderFriction1.iDirIndex) = cNewRealDir;

  // update ColumnHeaders

  ASSERT(tHeaderFriction1.iNotClampedIndex == 0);
  ASSERT(tHeaderFriction2.iNotClampedIndex == 0);
  ASSERT(tHeaderFriction1.cStartDir == tHeaderFriction2.cStartDir);

  marina_real fTemp = pfProjectionMatrix[0] * tHeaderFriction1.cStartDir[tHeaderFriction1.iDirIndex - 1] + pfProjectionMatrix[1] * tHeaderFriction1.cStartDir[tHeaderFriction2.iDirIndex - 1];
  tHeaderFriction1.cStartDir[tHeaderFriction2.iDirIndex - 1] = pfProjectionMatrix[2] * tHeaderFriction1.cStartDir[tHeaderFriction1.iDirIndex - 1] + pfProjectionMatrix[3] * tHeaderFriction1.cStartDir[tHeaderFriction2.iDirIndex - 1];
  tHeaderFriction1.cStartDir[tHeaderFriction1.iDirIndex - 1] = fTemp;

  tHeaderFriction2.cStartDir = tHeaderFriction1.cStartDir;
}

int DantzigAlgorithm::Resolve()
{
  PROFILE_SCOPE_MARINA(resolve);
  DebugPoint();
  //InitLogging();

  BOOL bAllowDynamic; 

  do 
  {

    PROFILE_SCOPE_MARINA(do);
    Done();

    BOOL bNothingToDo;
    {    
      PROFILE_SCOPE_MARINA(Initialize);
      if (MARINA_FAILURE == Initialize(bNothingToDo))
        return MARINA_FAILURE;
    }
    if (bNothingToDo)
    {
      Done();
      return MARINA_OK;
    }

     //PROFILE_SCOPE_MARINA(resolverest);

    bAllowDynamic = _bAllowDynamicContact;
    _bLUDecompositionValid = FALSE;
    _decompositionValid = 0;

    {
      PROFILE_SCOPE_MARINA(luinit);
      g_luMatrix.Init(_nNumberOfForces);
      g_luMatrix2.Init(_nNumberOfForces);
      g_luMatrix3.Init(_nNumberOfForces);
      g_luSparseMatrix.Init(_nNumberOfForces);
#if _CALC_ZEROS
      g_oper = 0;
      g_zeros = 0;
#endif
    }

    _matStepOld = 1;
    _matStepNew = 1;
    _matStepDel = 1;

    _iLocalCycle = _iGlobalCycle = 1;

    /// Vector B can lay out outside from the space defined by culumns of the matrix A.
    /// Here we present very simple and time consuming method how to project vector b into 
    /// Matrix A space. On other sidem if we will be lucy all forces can be possitiove
    //  if (! ProjectBIntoA())
    { 

      PROFILE_SCOPE_MARINA(loop);
      //// Calculation with first one. it must be solved by hand.
      if (!ResolveFirstCollision())
        return MARINA_OK; // nothing to do


      //// Calculate othrers
      while (/*_pnLastIndexInZone[EMZ_UNSOLVED] != _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] &&*/ ResolveNextCollision())
      {//ForcesToArrows();
      }      
    }

    //LogF("Dantzig:n %d  old %d, new %d, del %d, %lf",_nNumberOfForces, _matStepOld, _matStepNew, _matStepDel, _matStepNew/ (double) _matStepOld);
#if _CALC_ZEROS
    LogF("Dantzig:n oper %d  zeros %d, %lf",g_oper , g_zeros, g_zeros / (double) g_oper);
#endif
    //LogF("Dantzig: clamped %d  calcdiffs %d  lindep %d, %lf",_pnLastIndexInZone[EMZ_CLAMPED] , _iCalledCalculateDiffs , _iLineDep, _iLineDep / (double) _iCalledCalculateDiffs);

   
  } while /*(bAllowDynamic && !_bAllowDynamicContact)*/ (FALSE);

  //LogF("Dantzig: _iGlobalCycle %d, _iCalledCalculateDiffs %d, _clamped %d, _iMaxMatrixSize %d, _iMaxTime %d, _iLineDep %d",
  //  _iGlobalCycle,_iCalledCalculateDiffs, _pnLastIndexInZone[EMZ_CLAMPED], _iMaxMatrixSize, _iMaxTime, _iLineDep);
  //LogF("Dantzig: _iCtoNC %d, _iCtoNCReal %d, _iCtoFric %d, _iRelease %d",
  //  _iCtoNC,_iCtoNCReal, _iCtoFric, _iRelease);

  /// Say how much friction forces was be needed
  /*int Friction = 0;
  for(int i = 0; i < _pnLastIndexInZone[EMZ_CLAMPED]; i++)
  {
    if (_cMappingToColumnID[i]->eForceType == EFT_STATIC)
      Friction += 2;
  }

  Friction += (_pnLastIndexInZone[EMZ_NOTCLAMPED] - _pnLastIndexInZone[EMZ_CLAMPED]) * 2;
  LogF("Dantzig: friction %d, uberall %d, not clamped %d ", Friction, Friction + _nNumberOfForces/3, (_pnLastIndexInZone[EMZ_NOTCLAMPED] - _pnLastIndexInZone[EMZ_CLAMPED]));
  */


  //Serialize();

  /*  for(unsigned int ii = _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]; ii < _pnLastIndexInZone[EMZ_UNSOLVED]; ii = _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED])
  {
  const TCOLUMNHEADER& tColumnHeader = _cColumnHeaders[_cMappingToColumnID[ii]];
  tColumnHeader.pPedestal->MoveFrictionToClamped(_cMappingToColumnID[ii]);
  MoveToZone(ii, EMZ_CLAMPED, EMZ_UNSOLVED);
  }*/

  ForcesToArrows();
  RecalculateClamped();
  
  //Serialize();

  /*
  for(unsigned int i = 0; i < _pnLastIndexInZone[EMZ_CLAMPED]; i++)
  {
  marina_real fNewAccel = g_cMatrixA.RowTimesVector(i, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) + _cVectorB[i];
  if (!IsNumericalZero(fNewAccel))
  {
  RecalculateClamped();
  break;
  }
  }*/


  //RecalculateClamped();
  // Still some acceleration < 0, because of numerical fluctuations. 
  // Apply harmonic potencial to force bodies to stay in the middle of the tolerance.
  // ApplyBindForcesAtClamped();


  //// Apply forces to bodies
  PROFILE_SCOPE_MARINA(apply);
  ApplyCalculatedForces(_cForces);

  FinallyRevertMatrixA();

  //// Apply correction  forces
  //ApplyCorrectionForces(fSimulationTime); 

  //Done();
  //DoneLogging();

  return MARINA_OK;
}


int DantzigAlgorithm::ResolveOneStep()
{
  PROFILE_SCOPE_MARINA(resolve);
  DebugPoint();
  //InitLogging();
  
  switch(_phase)
  {
  case 0:
    {  
      PROFILE_SCOPE_MARINA(do);
      Done();

      BOOL bNothingToDo;
      {    
        PROFILE_SCOPE_MARINA(Initialize);
        if (MARINA_FAILURE == Initialize(bNothingToDo))
          return MARINA_FAILURE;
      }
      if (bNothingToDo)
      {
        Done();
        return MARINA_OK;
      }
    
      _bLUDecompositionValid = FALSE;
      _decompositionValid = 0;

      {
        PROFILE_SCOPE_MARINA(luinit);
        //g_luMatrix.Init(_nNumberOfForces);
        //g_luMatrix2.Init(_nNumberOfForces);
        g_luMatrix3.Init(_nNumberOfForces);
        //g_luSparseMatrix.Init(_nNumberOfForces);
#if _CALC_ZEROS
        g_oper = 0;
        g_zeros = 0;
#endif
      }

      _matStepOld = 1;
      _matStepNew = 1;
      _matStepDel = 1;

      _iLocalCycle = _iGlobalCycle = 1;

      if (!ResolveFirstCollision())
        return MARINA_OK; // nothing to do

      ForcesToArrows();
      _phase = 1;
      return MARINA_NEXT;
    }
  case 1: 
    {
      PROFILE_SCOPE_MARINA(loop);
      if (ResolveNextCollision())
      {
        ForcesToArrows();
        return MARINA_NEXT;
      }

      ForcesToArrows();

      //LogF("Dantzig:n %d  old %d, new %d, del %d, %lf",_nNumberOfForces, _matStepOld, _matStepNew, _matStepDel, _matStepNew/ (double) _matStepOld);
#if _CALC_ZEROS
        LogF("Dantzig:n oper %d  zeros %d, %lf",g_oper , g_zeros, g_zeros / (double) g_oper);
#endif
      //LogF("Dantzig: clamped %d  calcdiffs %d  lindep %d, %lf",_pnLastIndexInZone[EMZ_CLAMPED] , _iCalledCalculateDiffs , _iLineDep, _iLineDep / (double) _iCalledCalculateDiffs);

     /* LogF("Dantzig: _iGlobalCycle %d, _iCalledCalculateDiffs %d, _clamped %d, _iMaxMatrixSize %d, _iMaxTime %d, _iLineDep %d",
        _iGlobalCycle,_iCalledCalculateDiffs, _pnLastIndexInZone[EMZ_CLAMPED], _iMaxMatrixSize, _iMaxTime, _iLineDep);
      LogF("Dantzig: _iCtoNC %d, _iCtoNCReal %d, _iCtoFric %d, _iRelease %d",
        _iCtoNC,_iCtoNCReal, _iCtoFric, _iRelease);
*/
      RecalculateClamped();

      //// Apply forces to bodies
      PROFILE_SCOPE_MARINA(apply);
      ApplyCalculatedForces(_cForces);
      FinallyRevertMatrixA();
      _phase = 2;

      return MARINA_OK;
    }
  default:
    {
      ASSERT(FALSE);
      return MARINA_FAILURE;
    }
  }
}

void DantzigAlgorithm::RecalculateClamped()
{
  START_PROFILE_MARINA(RecalculateClamped);
  unsigned int iNonSymetry = 0;
  for(; iNonSymetry < _pnLastIndexInZone[EMZ_CLAMPED]; iNonSymetry++)
  {
    if (_cMappingToColumnID[iNonSymetry]->unsymetric != 0)
      break;
  } 
    
  {        
    //M_VECTOR diffForcesOld;
    //diffForcesOld.Init(cDiffForces.GetDim());
    //diffForcesOld = cDiffForces;
    
    unsigned int n = _pnLastIndexInZone[EMZ_CLAMPED];      

    g_luMatrix3.ValidSize(0);

    if (iNonSymetry < n)
    {
      //START_PROFILE_MARINA(LUDecomAddIndex3NonSym);        
      for(unsigned int i = 0; i < n; i++)        
        g_luMatrix3.AddIndex3(i,iNonSymetry,g_cMatrixA,
          _cMappingToColumnID[iNonSymetry]->unsymetric == 0);
    }
    else
    {              
      for(unsigned int i = 0; i < n; i++)        
        g_luMatrix3.AddIndex3(i,iNonSymetry,g_cMatrixA, true);        
    }
          
    g_luMatrix3.Solve3(_cVectorB, iNonSymetry, g_cMatrixA);      
    
    END_PROFILE_MARINA(RecalculateClamped);
  }
}

void DantzigAlgorithm::Done()
{
  _cColumnHeaders.Clear();
  _cMappingToColumnID.Clear(); // indexes are coordinates used in g_cMatrixA, values are columns IDs
  _numberOfClamped.Resize(0);
  for(int i = 0; i < _pedestals.Size(); i++)
    delete _pedestals[i];

  _pedestals.Resize(0);
  SolvingDone();
}

BOOL DantzigAlgorithm::IsMatrixARegular()
{
  //PROFILE_SCOPE_MARINA(regular);
  /*if (_pnLastIndexInZone[EMZ_CLAMPED] == 1)
    return TRUE;

  Vector<marina_real> cLeftVector;
  cLeftVector.Init(_pnLastIndexInZone[EMZ_CLAMPED] - 1);

  g_cMatrixA.GetColumn(cLeftVector,_pnLastIndexInZone[EMZ_CLAMPED] - 1,_pnLastIndexInZone[EMZ_CLAMPED] - 1);

  SolveNxN(_pnLastIndexInZone[EMZ_CLAMPED] - 1, g_cMatrixA.GetDim(), g_cMatrixA.GetPointer(), cLeftVector.GetPointer());

  marina_real fResult = g_cMatrixA.RowTimesVector(_pnLastIndexInZone[EMZ_CLAMPED] - 1,cLeftVector, _pnLastIndexInZone[EMZ_CLAMPED] - 1);

  fResult -= g_cMatrixA.GetMemberValue(_pnLastIndexInZone[EMZ_CLAMPED] - 1,_pnLastIndexInZone[EMZ_CLAMPED] - 1);
  //ASSERT(!IsNumericalZero(fResult));

  return !IsNumericalZero(fResult);
  */
  return TRUE;
}

BOOL DantzigAlgorithm::VerifySituation()
{
  // All clamped normal forces must be >= 0
  // All clamped friction forces must have thier normal forces clamped
  unsigned int i = 0;
  for(; i < _pnLastIndexInZone[EMZ_CLAMPED]; i++)
  {
    TCOLUMNHEADER tHeader = *(_cMappingToColumnID[i]);
    if (tHeader.eForceType == EFT_STATIC)
    {
      if (_cForces[i] < 0.0 && !IsNumericalZero(_cForces[i]))
      {            
        ASSERT(FALSE);
        return FALSE;
      }

      if (tHeader.iNotClampedIndex != 0)
      {

        if (tHeader.pBrotherForcesHeaders[tHeader.iNotClampedIndex - 1]->iMatrixAIndex >= _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] ||
          tHeader.pBrotherForcesHeaders[tHeader.iNotClampedIndex - 1]->iMatrixAIndex < _pnLastIndexInZone[EMZ_NOTCLAMPED])
        {
          ASSERT(FALSE);
          return FALSE;            
        }  


      }
    }
    else
      if (tHeader.eForceType == EFT_FRICTION)
      {
        if (tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex >=  _pnLastIndexInZone[EMZ_CLAMPED])
        {
          ASSERT(FALSE);
          return FALSE;
        }

        if (tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex <  _pnLastIndexInZone[EMZ_CLAMPED])
        {
          /*marina_real fFrictionForce = _cForces[i] *  _cForces[i] +
          _cForces[tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex] * _cForces[tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex];
          fFrictionForce = sqrt(fFrictionForce);

          if (fFrictionForce > _cForces[tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex] * tHeader.fSlidingFrictionCoef &&
          IsNumericalZero(fFrictionForce - _cForces[tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex] * tHeader.fSlidingFrictionCoef))
          {                    
          ASSERT(FALSE);
          return FALSE;
          }
          */
        }
        else
        {                
          if (tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED])
          {
            ASSERT(FALSE);
            return FALSE;
          }

          /* if (_cForces[i] > _cForces[tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex] * tHeader.fSlidingFrictionCoef 
          && !IsNumericalZero(_cForces[i] - _cForces[tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex] * tHeader.fSlidingFrictionCoef))
          {                    
          ASSERT(FALSE);
          return FALSE;
          }
          */
        }
      }

  }

  // All nonclamped normal accelerations must be >= 0
  for(; i < _pnLastIndexInZone[EMZ_NOTCLAMPED]; i++)
  {
    TCOLUMNHEADER tHeader = *(_cMappingToColumnID[i]);
    if (tHeader.eForceType != EFT_STATIC)// && _cAccelerations[i] < 0.0)
    {
      ASSERT(FALSE);
      return FALSE;
    }
  }

  for(; i < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]; i++)
  {
    TCOLUMNHEADER tHeader = *(_cMappingToColumnID[i]);
    if (tHeader.eForceType != EFT_FRICTION) 
    {
      ASSERT(FALSE);
      return FALSE;
    }

    if (tHeader.pBrotherForcesHeaders[0]->iNotClampedIndex == 0)
    {
      ASSERT(FALSE);
      return FALSE;
    }

    if (tHeader.pBrotherForcesHeaders[0]->iMatrixAIndex >= _pnLastIndexInZone[EMZ_CLAMPED])
    {
      ASSERT(FALSE);
      return FALSE;
    }

    if (tHeader.pBrotherForcesHeaders[1]->iMatrixAIndex < _pnLastIndexInZone[EMZ_SOLVING])
    {
      ASSERT(FALSE);
      return FALSE;
    }

  }

  return TRUE;
}

void DantzigAlgorithm::Serialize()
{
  FILE *fArchive = fopen("C:\\temp\\RestResolver.ser","w");

  SerializeChild(fArchive);

  fprintf(fArchive,"_cMappingToColumnID: %u\n", _cMappingToColumnID.Size());
  for(int i = 0; i < _cMappingToColumnID.Size(); i++)
  {
    fprintf(fArchive,"%u\n", _cMappingToColumnID[i]);
  }

  /*  fprintf(fArchive,"_cColumnHeaders: %u\n", _cColumnHeaders.GetSize());
  for(unsigned int i = 0; i < _cColumnHeaders.GetSize(); i++)
  {
  SerializeColumnHeader(fArchive, _cColumnHeaders[i], TRUE);    
  }
  */
  fprintf(fArchive,"_cForces: %u\n", _cForces.GetDim());
  for(unsigned int i = 0; i < _cForces.GetDim(); i++)
  {
    fprintf(fArchive,"%10.8lf\n", _cForces[i]); 
  }

  fclose(fArchive);
}

float DantzigAlgorithm::CalcAccel(unsigned int iMatIndx)
{
  float val = g_cMatrixA.RowTimesVector(iMatIndx, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +  _cVectorB[iMatIndx];

  
  if (_pnLastIndexInZone[EMZ_SOLVING] > _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED])
  {
    ASSERT(_pnLastIndexInZone[EMZ_SOLVING] - _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED] == 1);    
    val += g_cMatrixA.GetMemberValue(iMatIndx, _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]) * _cForces[_pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]];
  }

  for(int j = 0; j < _cReleaseList.Size(); j++)   
    if (_cReleaseList[j]->iMatrixAIndex != _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]) // not currently solving
      val += g_cMatrixA.GetMemberValue(iMatIndx, _cReleaseList[j]->iMatrixAIndex) * _cForces[_cReleaseList[j]->iMatrixAIndex];

  return val;
}

void DantzigAlgorithm::FinallyRevertMatrixA()
{

  // revert notclamped 
  for(unsigned int i = _pnLastIndexInZone[EMZ_NOTCLAMPED]; i < _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]; i++)
  {
    TCOLUMNHEADER& tHeaderFriction = *_cMappingToColumnID[i];
    TCOLUMNHEADER& tHeaderNormal = * tHeaderFriction.pBrotherForcesHeaders[0];
    if (tHeaderNormal.iNotClampedIndex > 0)
      RevertColumnInMatrixA(tHeaderNormal.iMatrixAIndex);
  }

  // fix friction dirs
  for(unsigned int i = 0; i < _nNumberOfForces / 3; i++)
  {
    if (ContactPointDir(i,1) != ContactPointDirSource(i,1) || ContactPointDir(i,2) != ContactPointDirSource(i,2))
    {
      TCOLUMNHEADER& tHeaderFriction1 = _cColumnHeaders[i * 3 + 1];
      TCOLUMNHEADER& tHeaderFriction2 = _cColumnHeaders[i * 3 + 2];

      // find conversion matrix
      float mat[4];

      mat[0] = ContactPointDir(i,1) * ContactPointDirSource(i,1);
      mat[1] = ContactPointDir(i,2) * ContactPointDirSource(i,1);
      mat[2] = ContactPointDir(i,1) * ContactPointDirSource(i,2);
      mat[3] = ContactPointDir(i,2) * ContactPointDirSource(i,2);

      g_cMatrixA.CombineCoordinates(tHeaderFriction1.iMatrixAIndex, tHeaderFriction2.iMatrixAIndex, mat);
    }
  }

  g_cMatrixA.NeutralizePermutation();

#ifdef _DEBUG_OFF
  for(unsigned int i = 0; i < _nNumberOfForces; i++)
  {
    for(unsigned int j = 0; j < _nNumberOfForces; j++)
    {
      float val = g_cMatrixA.GetMemberValue(i,j) - _cSuperSourceMatrixA.GetMemberValue(i,j);
      ASSERT(IsNumericalZero(val));
    }
  }
#endif
}

void DantzigAlgorithm::TestForces()
{
  /// Test of correctness
#ifdef _DEBUG_YY
  for(unsigned int i = 0; i < _pnLastIndexInZone[EMZ_NOTCLAMPED]; i++)
  {
    float val = g_cMatrixA.RowTimesVector(i, _cForces, _pnLastIndexInZone[EMZ_CLAMPED]) +  _cVectorB[i];

    if (_pnLastIndexInZone[EMZ_SOLVING] > _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED])
    {
      for(int j = _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]; j < _pnLastIndexInZone[EMZ_SOLVING]; j++)
        val += g_cMatrixA.GetMemberValue(i, j) * _cForces[j];
    }

    for(int j = 0; j < _cReleaseList.Size(); j++)   
      if (_cReleaseList[j]->iMatrixAIndex != _pnLastIndexInZone[EMZ_FRICTIONNOTCLAMPED]) // not currently solving
        val += g_cMatrixA.GetMemberValue(i, _cReleaseList[j]->iMatrixAIndex) * _cForces[_cReleaseList[j]->iMatrixAIndex];
    
    val -= _cAccelerations[i];
    ASSERT(IsNumericalZero(val  / 100));
     //_cVectorB[i] += val - _cAccelerations[i];
    
  }
#endif
}

void DantzigAlgorithm::CheckPedestals()
{
#if _USE_PEDESTALS
#ifdef _DEBUG
  for(unsigned int i = 0; i < _pnLastIndexInZone[EMZ_NOTCLAMPED]; i++)
  {
    TCOLUMNHEADER& tHeader = *_cMappingToColumnID[i];
    if (tHeader.eForceType == EFT_STATIC && tHeader.pedestal && 
      tHeader.pedestal->nClamped == 3 && tHeader.iContactPointID == tHeader.pedestal->min)
    {
      TPEDESTAL & ped = *tHeader.pedestal;
      unsigned int unsymetric = 0; 
      for(int j = 0; j < 3; j++)
      {
         TCOLUMNHEADER& tRealClamped = _cColumnHeaders[ped.realClamped[j] * 3];
         if (tRealClamped.iNotClampedIndex > 0)
           unsymetric++;
      }

      for(int j = 0; j < 3; j++)
      {
        TCOLUMNHEADER& tRealClamped = _cColumnHeaders[ped.clamped[j] * 3];
        ASSERT(tRealClamped.unsymetric  == unsymetric);
      }
    }
  }
#endif 
#endif
}
#endif