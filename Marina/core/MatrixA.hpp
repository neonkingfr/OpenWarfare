#ifdef _MSC_VER
#pragma once
#endif

#ifndef __MATRIXA_H__
#define __MATRIXA_H__

template <class Type>
class Vector 
{
protected:
	Type * _pVector;
	unsigned int _nDim;
  unsigned int _nDimMax;
public:
	Vector():_nDim(0), _nDimMax(0), _pVector(0) {};
	~Vector() {delete [] _pVector;};

	int Init(unsigned int nDim);
  int InitNonZero(unsigned int nDim);
	void Negative(unsigned int nDim);
	void MoveUp(unsigned int iFromIndex, unsigned int iToIndex);
	void MoveDown(unsigned int iFromIndex, unsigned int iToIndex);

	Type& operator[](unsigned int i) const {Assert(_nDim > i);return _pVector[i];};

	Type * GetPointer() {return _pVector;};
	unsigned int GetDim() const {return _nDim;};

	const Vector<Type>& operator=(const Vector<Type>& cVector);
};

template <class Type>
int Vector<Type>::Init(unsigned int nDim)
{
	if (_nDimMax < nDim)
  {  
	  delete [] _pVector;
    _nDimMax = nDim;
	  _pVector = new Type[_nDimMax]; 
    if (_pVector == NULL)
      return MARINA_FAILURE;
  }

  _nDim = nDim;

	memset(_pVector, 0x0, nDim * sizeof(Type));

	return MARINA_OK;
}

template <class Type>
int Vector<Type>::InitNonZero(unsigned int nDim)
{
  if (_nDimMax < nDim)
  {  
    delete [] _pVector;
    _nDimMax = nDim;
    _pVector = new Type[_nDimMax]; 
    if (_pVector == NULL)
      return MARINA_FAILURE;
  }
   _nDim = nDim;

  return MARINA_OK;
}


template <class Type>
void Vector<Type>::Negative(unsigned int nDim)
{
	Assert(_nDim >= nDim);

	for(unsigned int i = 0; i < nDim; i++)
		_pVector[i] *= -1;
}

template <class Type>
void Vector<Type>::MoveUp(unsigned int iFromIndex, unsigned int iToIndex)
{
	Assert(iToIndex > iFromIndex);
	Assert(iToIndex < _nDim);

	Type fTemp = _pVector[iFromIndex];

	memmove(_pVector + iFromIndex, _pVector + iFromIndex + 1, (iToIndex - iFromIndex) * sizeof(Type));
	
	_pVector[iToIndex] = fTemp;
}

template <class Type>
void Vector<Type>::MoveDown(unsigned int iFromIndex, unsigned int iToIndex)
{
	Assert(iToIndex < iFromIndex);
	Assert(iFromIndex < _nDim);

	Type fTemp = _pVector[iFromIndex];

	memmove(_pVector + iToIndex + 1, _pVector + iToIndex, (iFromIndex - iToIndex) * sizeof(Type));
	
	_pVector[iToIndex] = fTemp;
}

template <class Type>
const Vector<Type>& Vector<Type>::operator=(const Vector<Type>& cVector)
{
  if (_nDimMax < cVector._nDim)
  {  
	  delete [] _pVector;

	  _nDimMax = cVector._nDim;
	  _pVector = new Type[_nDimMax];
  }
  _nDim = cVector._nDim;

	memcpy(_pVector, cVector._pVector, sizeof(Type) * _nDim);

	return *this;
}

template <class Type>
class Matrix  // Symetric matrix !!! positive semidefinite.
{
protected:
  Type * _pMatrix;
  unsigned int _nDim;
  unsigned int _nDimMax;
  unsigned int * _indexes;
public:

  Matrix():_nDim(0), _nDimMax(0), _pMatrix(NULL), _indexes(NULL) {};
  ~Matrix() {delete [] _pMatrix; delete [] _indexes;};
  int Init(unsigned int nDim);
  void NeutralizePermutation();

  void SetMemberValue(unsigned int nRow, unsigned int nCollumn, const Type& Value) {_pMatrix[_indexes[nRow] * _nDim + _indexes[nCollumn]] = Value;}
  void SetMemberValueRaw(unsigned int nRow, unsigned int nCollumn, const Type& Value) {_pMatrix[nRow * _nDim + nCollumn] = Value;}
  void AddMemberValue(unsigned int nRow, unsigned int nCollumn, const Type& Value) {_pMatrix[_indexes[nRow] * _nDim + _indexes[nCollumn]] += Value;}
  void AddMemberValueRaw(unsigned int nRow, unsigned int nCollumn, const Type& Value) {_pMatrix[nRow * _nDim + nCollumn] += Value;}
  void SwitchCoordinates(unsigned int nCoord1, unsigned int nCoord2);
  void MoveDown(unsigned int iFromCoord1, unsigned int iToCoord1);
  void MoveUp(unsigned int iFromCoord1, unsigned int iToCoord1);
  void GetColumn(Vector<marina_real>& cColumn, unsigned int iColumn, unsigned int nLength);
  void CombinateColumns(unsigned int iDestColumn, unsigned int iSourceColumn, marina_real fCoef);
  void CombineCoordinates(unsigned int iCoord1, unsigned int iCoord2, marina_real * pfCombMatrix); // Combines two coordinates according to Matrix
  //void RemoveCoordinate(unsigned int iCoord);
  Type RowTimesVector(unsigned int iRow, Vector<marina_real>& cForces, unsigned int nRows);
  Type RowTimesVector(unsigned int iRow, Vector<marina_real>& cVector, unsigned int iFrom, unsigned int iTo);
  const Vector<marina_real>& MultiplyByShortVector(Vector<marina_real>& cOutput, Vector<marina_real>& cInput, unsigned int nDim);	
  const Vector<marina_real>& MultiplyByShortVectorFromTo(Vector<marina_real>& cOutput, Vector<marina_real>& cInput, unsigned int nDim,
    unsigned int iFromCoord, unsigned int iToCoord);	

  void Sqr(Matrix<Type>& cOutput, unsigned int nRows);

  //Type * GetPointer() {return _pMatrix;};
  unsigned int GetDim() const {return _nDim;};
  Type GetMemberValue(unsigned int nRow, unsigned int nColumn) const {return _pMatrix[_indexes[nRow] * _nDim + _indexes[nColumn]];};

  const Matrix<Type>& operator=(const Matrix<Type>& cMatrix);
};

template <class Type>
int Matrix<Type>::Init(unsigned int nDim)
{
  if (_nDimMax < nDim)
  {
	  delete [] _pMatrix;
    _pMatrix = new Type[nDim * nDim]; 
    delete [] _indexes;
    _indexes = new unsigned int[nDim]; 

    if (_pMatrix == NULL || _indexes == NULL)
      return MARINA_FAILURE;
    _nDimMax = nDim;
  }

	_nDim = nDim;	
	memset(_pMatrix, 0x0, nDim * nDim * sizeof(Type));

  for(unsigned int i = 0; i < nDim; i++)  
    _indexes[i] = i;

	return MARINA_OK;
}

template <class Type>
void Matrix<Type>::NeutralizePermutation()
{
  for(unsigned int i = 0; i < _nDim; i++)  
    _indexes[i] = i;
}

template <class Type>
void Matrix<Type>::SwitchCoordinates(unsigned int nCoord1, unsigned int nCoord2)
{
	Assert(nCoord1 < _nDim);
	Assert(nCoord2 < _nDim);

	// Switch rows
  unsigned int temp = _indexes[nCoord1];
  _indexes[nCoord1] = _indexes[nCoord2];
  _indexes[nCoord2] = temp;

	// Switch columns
	/*for(unsigned int i = 0; i < _nDim; i++)
	{
		Type Temp = _pMatrix[i * _nDim + nCoord1];
		_pMatrix[i * _nDim + nCoord1] = _pMatrix[i * _nDim + nCoord2];
		_pMatrix[i * _nDim + nCoord2] = Temp;
	}*/
}

template <class Type>
void Matrix<Type>::MoveUp(unsigned int iFromCoord, unsigned int iToCoord)
{
	Assert(iFromCoord < iToCoord);
	Assert(iToCoord < _nDim);

	// move rows
	unsigned int temp = _indexes[iFromCoord];
  memmove(_indexes + iFromCoord, _indexes + iFromCoord + 1, (iToCoord - iFromCoord) * sizeof(unsigned int));
  _indexes[iToCoord] = temp; 

	// move columns
	/*Type * pTemp = _pMatrix + iFromCoord;
	for(unsigned int i = 0; i < _nDim; i++)
	{
		Type Temp = *pTemp;
		memmove(pTemp, pTemp + 1, (iToCoord - iFromCoord) * sizeof(Type));
		pTemp[iToCoord - iFromCoord] = Temp;
		pTemp += _nDim;
	}*/
}

template <class Type>
void Matrix<Type>::MoveDown(unsigned int iFromCoord, unsigned int iToCoord)
{
	Assert(iFromCoord > iToCoord);
	Assert(iFromCoord < _nDim);

	// move rows	
  unsigned int temp = _indexes[iFromCoord];
  memmove(_indexes + iToCoord + 1, _indexes + iToCoord, (iFromCoord - iToCoord) * sizeof(unsigned int));
  _indexes[iToCoord] = temp; 

	// move columns
	/*Type * pTemp = _pMatrix + iToCoord;
	for(uns/*igned int i = 0; i < _nDim; i++)
	{
		Type Temp = pTemp[iFromCoord - iToCoord];
		memmove(pTemp + 1, pTemp, (iFromCoord - iToCoord) * sizeof(Type));
		*pTemp = Temp;
		pTemp += _nDim;
	}*/
}

template <class Type>
void Matrix<Type>::GetColumn(Vector<marina_real>& cColumn, unsigned int iColumn, unsigned int nLength)
{
	Assert(iColumn < _nDim);
	Assert(nLength <= _nDim);

  iColumn = _indexes[iColumn];

	for(unsigned int i = 0; i < nLength; i++)
	{
		cColumn[i] = _pMatrix[_indexes[i] * _nDim + iColumn];
	}
}

template <class Type>
Type Matrix<Type>::RowTimesVector(unsigned int iRow, Vector<marina_real>& cForces, unsigned int nRows)
{
	Type Output = 0;
	Type * pTemp = _pMatrix + _indexes[iRow] * _nDim;
	for(unsigned int i = 0; i < nRows; i++)	
		Output += pTemp[_indexes[i]] * cForces[i];				
	
	return Output;
}

template <class Type>
Type Matrix<Type>::RowTimesVector(unsigned int iRow, Vector<marina_real>& cVector, unsigned int iFrom, unsigned int iTo)
{
	Type Output = 0;
	Type * pTemp = _pMatrix + _indexes[iRow] * _nDim;
	for(unsigned int i = iFrom; i < iTo; i++)
		Output += pTemp[_indexes[i]] * cVector[i];			

	return Output;
}


template <class Type>
const Vector<marina_real>& Matrix<Type>::MultiplyByShortVector(Vector<marina_real>& cOutput, Vector<marina_real>& cInput, unsigned int nDim)
{
	Assert(cOutput.GetDim() >= nDim);
	Assert(cInput.GetDim() >= nDim);

	for(unsigned int i = 0; i < nDim; i++)
	{
		cOutput[i] = 0;
		Type * pTemp = _pMatrix + _indexes[i] * _nDim;
		for(unsigned int j = 0; j < nDim; j++)		
			cOutput[i] += pTemp[_indexes[j]] * cInput[j];		
	}

	return cOutput;
}

template <class Type>
const Vector<marina_real>& Matrix<Type>::MultiplyByShortVectorFromTo(Vector<marina_real>& cOutput, Vector<marina_real>& cInput, unsigned int nDim,
		unsigned int iFromCoord, unsigned int iToCoord)
{
	Assert(cOutput.GetDim() >= iToCoord);
	Assert(cInput.GetDim() >= nDim);

	for(unsigned int i = iFromCoord; i <= iToCoord; i++)
	{
		cOutput[i] = 0;
		Type * pTemp = _pMatrix + _indexes[i] * _nDim;
		for(unsigned int j = 0; j < nDim; j++)		
			cOutput[i] += pTemp[_indexes[j]] * cInput[j];
	}

	return cOutput;
}


template <class Type>
void Matrix<Type>::Sqr(Matrix<Type>& cOutput, unsigned int nRows)
{
	Assert(cOutput._nDim = nRows);

	/// Just for symetric matrix
	for(unsigned int i = 0; i < nRows; i ++)
		for(unsigned int j = i; j < nRows; j++)
		{
			Type * pTempI = _pMatrix + i * _nDim;
			Type * pTempJ = _pMatrix + j * _nDim;

			Type * pTemp = cOutput._pMatrix + i * nRows + j;
			*pTemp = 0;

			for(unsigned int k = 0; k < nRows; k++)
			{
				*pTemp += (*pTempI) * (*pTempJ);
				pTempI++; pTempJ++;
			}

			cOutput._pMatrix[j * nRows + i] = *pTemp;			
		}

}

template <class Type>
void Matrix<Type>::CombinateColumns(unsigned int iDestColumn, unsigned int iSourceColumn, marina_real fCoef)
{
	Assert(iDestColumn < _nDim);
	Assert(iSourceColumn < _nDim);
  iDestColumn = _indexes[iDestColumn];
  iSourceColumn = _indexes[iSourceColumn];

	Type * pTemp = _pMatrix + iDestColumn;
	for(unsigned int iRows = 0; iRows < _nDim; iRows++)
	{
		*pTemp += fCoef * pTemp[iSourceColumn - iDestColumn];
		pTemp += _nDim;
	}
}

template <class Type>
void Matrix<Type>::CombineCoordinates(unsigned int iCoord1, 
                                      unsigned int iCoord2, 
                                      marina_real * pfCombMatrix) //  must be unitary
{
  Assert(iCoord1 < _nDim);
  Assert(iCoord2 < _nDim);
  Assert(IsNumericalZero(pfCombMatrix[0] * pfCombMatrix[2] + pfCombMatrix[1] * pfCombMatrix[3]));//  must be unitary

  iCoord1 = _indexes[iCoord1];
  iCoord2 = _indexes[iCoord2];
  // Columns
  Type * pTemp = _pMatrix + iCoord1;
  Type * pTempEnd = _pMatrix + _nDim * _nDim;
  Type In1, In2;

  for(; pTemp < pTempEnd; pTemp += _nDim)
  {
    In1 = (*pTemp);
    In2 = pTemp[iCoord2 - iCoord1];
    (*pTemp) = pfCombMatrix[0] * In1 + pfCombMatrix[1] * In2;
    pTemp[iCoord2 - iCoord1] = pfCombMatrix[2] * In1 + pfCombMatrix[3] * In2;
  }

  // Rows
  pTemp = _pMatrix + iCoord1 * _nDim;
  pTempEnd = pTemp + _nDim;
  Type * pTemp2 = _pMatrix + iCoord2 * _nDim;

  for(; pTemp < pTempEnd; pTemp++,pTemp2++)
  {
    In1 = (*pTemp);
    In2 = (*pTemp2);

    (*pTemp) = pfCombMatrix[0] * In1 + pfCombMatrix[1] * In2;
    (*pTemp2) = pfCombMatrix[2] * In1 + pfCombMatrix[3] * In2;
  }
}

/*template <class Type>
void Matrix<Type>::RemoveCoordinate(unsigned int iCoord)
{
	Assert(iCoord < _nDim);

	if (iCoord + 1 == _nDim)
		return; // trivial case

	// move rows
	memmove(_pMatrix + _nDim * iCoord, _pMatrix + _nDim * (iCoord + 1), (_nDim - iCoord - 1) * _nDim * sizeof(Type));

	// move columns
	Type * pTemp = _pMatrix + iCoord;
	for(unsigned int iRows = 0; iRows < _nDim; iRows++)
	{
		memmove(pTemp, pTemp + 1, (_nDim - iCoord - 1) * sizeof(Type));
		pTemp += _nDim;
	}
}
*/
template <class Type>
const Matrix<Type>& Matrix<Type>::operator=(const Matrix<Type>& cMatrix)
{
	if (_nDimMax < cMatrix._nDim)
	{
    _nDimMax = cMatrix._nDim;
		delete [] _pMatrix;		
		_pMatrix = new Type[_nDimMax * _nDimMax];   
    delete [] _indexes;		
    _indexes = new unsigned int[_nDimMax];  
	}
  _nDim = cMatrix._nDim;
	memcpy(_pMatrix, cMatrix._pMatrix, sizeof(Type) * _nDim * _nDim);
  memcpy(_indexes, cMatrix._indexes, sizeof(*_indexes) * _nDim);

	return *this;
}
#endif

