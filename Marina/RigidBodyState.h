#ifdef _MSC_VER
#pragma once
#endif

#ifndef __RIGIDBODYSTATE_H__
#define __RIGIDBODYSTATE_H__ 

typedef float rb_real;

#include "Quaternion.h"

typedef enum {
	TB_TRUE = 1,
	TB_NONE = 0,
	TB_FALSE = -1
} TRIBOOL;

class IRigidBodyState //: virtual public RefCount
{
public:
	IRigidBodyState() {};	
  virtual ~IRigidBodyState(){};  

	/*-------------------
	  Get methods.
	  -------------------*/
	virtual rb_real GetInvMass() const = 0;
  virtual rb_real GetMass() const { return GetInvMass() == 0 ? 0 : 1.0f/ GetInvMass();};
  
	virtual const Matrix3& GetInvInertiaTensor() const  = 0;
	virtual const Matrix3& GetInertiaTensor() const  = 0;

	virtual const Matrix3& GetActualInvInertiaTensor() const  = 0;
	virtual const Matrix3& GetActualInertiaTensor() const  = 0;

	virtual const Vector3& GetPos() const  = 0;
	virtual const QuaternionRB& GetOrientation() const = 0;
  virtual const Vector3& GetMassCenter() const = 0;


	//virtual const Vector3& GetMomentum() const  = 0;
	virtual const Vector3& GetVelocity() const  = 0;

	virtual const Vector3& GetAngularMomentum() const  = 0;
	virtual const Vector3& GetAngularVelocity() const  = 0;
	
	/*-------------------
	  Set methods.
	  -------------------*/

	//virtual void SetInvMass(rb_real fInvMass)  = 0;
	//virtual void SetInvInertiaTensor(const Matrix3& cInvInertiaTensor)  = 0;
	//virtual void SetInertiaTensor(const Matrix3& cInertiaTensor)  = 0;
  virtual void SetActualInvInertiaTensor(const Matrix3& cInvInertiaTensor)  = 0;
  virtual void SetActualInertiaTensor(const Matrix3& cInertiaTensor)  = 0;

	virtual void SetPos(const Vector3& pos)  = 0;
  virtual void SetOrientationMassCenter(const QuaternionRB& orientation);
  virtual void SetOrientation(const QuaternionRB& orientation)  = 0;
  virtual void SetPosAndOrientation(const Vector3& pos, const QuaternionRB& orientation) {SetPos(pos); SetOrientation(orientation);};
  virtual void SetMassCenter(const Vector3& cPos)  = 0;

	//virtual void SetMomentum(const Vector3& cMomentum)  = 0;
	virtual void SetVelocity(const Vector3& cVelocity)  = 0;

	virtual void SetAngularMomentum(const Vector3& cAngularMomentum)  = 0;
	virtual void SetAngularVelocity(const Vector3& cAngularVelocity)  = 0;

	/*-------------------
	  Other methods.
	  -------------------*/
	//virtual void VelocityFromMomentum() {SetVelocity(GetMomentum() * GetInvMass());};
	//virtual void MomentumFromVelocity() {SetMomentum(GetVelocity() * (GetInvMass() == 0.0f ? 0.0f : 1.0f / GetInvMass()));};

	virtual void AngularVelocityFromAngularMomentum() {SetAngularVelocity(GetActualInvInertiaTensor()* GetAngularMomentum());};
  // beware this function is slow because of inverse
	virtual void AngularMomentumFromAngularVelocity() {SetAngularMomentum(GetActualInvInertiaTensor().InverseGeneral() * GetAngularVelocity());};

	virtual void ApplyImpulseAtPoint(const Vector3& cImpulse,
		const Vector3& cImpulsePoint);

	virtual Vector3 CalculateTorgue(const Vector3& cForce, const Vector3& cPoint) const;

	
	virtual Vector3 AccelerationAtPoint(const Vector3& cForceBank, 
								  const Vector3&  cTorgueBank, 
								  const Vector3& cPoint, 
								  TRIBOOL bPlus = TB_TRUE) const;

	virtual Vector3 DiffAccelerationAtPointOnForceAndTorque( const Vector3& cAccelPoint, 
													   const Vector3& cForce, 
													   const Vector3& cTorque) const;

	virtual void DiffAccelerationAtPointOnForceAtPoint(Vector3& cAccel, 
		    								const Vector3& cAccelPoint, 
											const Vector3& cForce, 
											const Vector3& cForcePoint) const;

	virtual Vector3 SpeedAtPoint(const Vector3& cPoint) const;

	virtual Vector3 DiffSpeedAtPointOnImpulseAndTorque( const Vector3& cSpeedPoint, 
												  const Vector3& cImpulse, 
												  const Vector3& cImpulseTorque) const;

	virtual void ActualizeTensors();

  virtual rb_real CalculateEnergy();
  void SlowDown(float coef);

  /*-------------------
  Sync methods. If Sync is true bodies local propeties (velocity atd...), is the same like ones in phys. e ngines
  -------------------*/

  virtual bool GetSyncStatus() const = 0;
  virtual void ResetSyncStatus() = 0;


	//virtual const IRigidBodyState& operator=(const IRigidBodyState& cState);
};

#endif
