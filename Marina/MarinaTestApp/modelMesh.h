
#pragma once

#include "model3D.h"

class CModelMesh :public CModel3D
{
public:
  CModelMesh( EModelTypes modelType,
    int id,
    Matrix4Par origin,
    IDirect3DDevice9 *pD3DDevice,
    D3DCOLOR color);

  void BeginInput() {};
  void AddTriangle(Vector3Val vertex1, Vector3Val vertex2, Vector3Val vertex3);
  void EndInput() {_ps->Prepare();}; 
};

