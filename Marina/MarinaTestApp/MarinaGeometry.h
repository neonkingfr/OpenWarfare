#pragma once

#include <marina.hpp>
#include <MyMass.h>
#include <cd/CDCache.hpp>
#include <d3d9.h>
#include "modelArray.h"
#include <RV/Tree/ObjShapeMat/objlod.hpp>
//#include <Poseidon/lib/paramArchiveExt.hpp>

typedef enum 
{
  EGT_COMPOSE = 0,
  //EGT_BOX = 1,
  //EGT_PLANE = 2,
  EGT_CONVEX = 3
} EGEOMTYPE;

class CGeometry : public IRBGeometry
{
protected:
  EGEOMTYPE _type;
  CMass _mass;

public:
  CGeometry(EGEOMTYPE type, int ID) : _type(type) ,IRBGeometry(ID) {};
  virtual ~CGeometry() {};


  EGEOMTYPE GetType() const {return _type;};

  const CMass& GetMass() const {return _mass;};
  virtual void InitModelForDraw(CModelArray& modelArray,IDirect3DDevice9 *pD3DDevice) const = 0;
  virtual void SetOriginForDraw(CModelArray& modelArray,IDirect3DDevice9 *pD3DDevice) const = 0;

  //virtual void SerializeParamFile(ParamArchive& ar);
};

TypeIsSimple(CGeometry *);
typedef AutoArray<CGeometry *> M_CGEOMARRAY;

class CComposeGeometry : public CGeometry
{
protected:
  M_CGEOMARRAY _cGeomArray;
  RString _sourcePath;

public:
  CComposeGeometry( int ID) : CGeometry(EGT_COMPOSE, ID) {};
  virtual ~CComposeGeometry();    

  bool LoadFromP3D(LPCSTR path, CMass& mass);

  void Add(CGeometry& cGeom) {_cGeomArray.Add(&cGeom);}; // Pointers will be cleaned in destructor
  int NumberOfChilds() const {return _cGeomArray.Size();};
  

  void TranslateChilds(const Vector3& cRelPos);
  CGeometry * DecomposeChild(int ID);

  virtual void SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation);
  virtual void CalculateEnclosingRectangle();
  virtual BOOL IsIntersection( IRBGeometry& cSecond, int simCycle);
  virtual BOOL ForceInterFree( IRBGeometry& cSecond, int simCycle);
  virtual void Scale(float scale) {};
  virtual void NormScale() {};
  virtual int FindContacts( RBContactArray& cContactPoints,  IRBGeometry& cSecond, int simCycle) ;

  virtual void SetBody(Ref<RigidBodyObject> ptBody);

  virtual void InitModelForDraw(CModelArray& modelArray,IDirect3DDevice9 *pD3DDevice) const;
  virtual void SetOriginForDraw(CModelArray& modelArray,IDirect3DDevice9 *pD3DDevice) const;

  //virtual void SerializeParamFile(ParamArchive& ar);

  //virtual void CreateDrawClients(CHordubalClientDll * pcDrawingClient,  unsigned int& iVladasID, BOOL bUseDrawstuff, int iBodyColor);
};

#include <Poseidon/lib/plane.hpp>
#define MAX_VERTEXES 4

class MPlane : public Plane
{
public:
  MPlane() : Plane() {};
  MPlane( Vector3Par normal, float d ) : Plane( normal, d ) {};
  MPlane( Vector3Par normal, Vector3Par point ) : Plane(normal,point) {};
  MPlane( const MPlane& plane) : Plane(plane) {};
};

typedef const MPlane& MPlaneVal;

class MFace
{
protected:
  int _n;
  int _vertexes[MAX_VERTEXES];
  MPlane _plane;

public:
  MFace(): _n(0) {};

  void CalcPlane(const AutoArray<Vector3>& vertexes);
  void CalcPlaneD(const AutoArray<Vector3>& vertexes);
  operator const MPlane& () const {return _plane;};

  int N() const {return _n;};
  int operator[](int i) const {Assert(i < MAX_VERTEXES); return _vertexes[i];};

  void AddVertex(int vertex) ;

  void Check(const AutoArray<Vector3>& verPos) const;

};
typedef const MFace& MFaceVal;
TypeIsBinary(MFace);

class MEdge
{
protected:
  int _vertexes[2];
  int _faces[2];
  Vector3 _dir;

public: 
  MEdge(int vertex1, int vertex2) : _dir(VZero) {
    _vertexes[0] = vertex1;_vertexes[1] = vertex2;_faces[0] = -1;_faces[1] = -1;};

    void CalcDir(const AutoArray<Vector3>& vertexes);

    int operator[](int i) const {Assert(i < 2); return _vertexes[i];};
    operator const Vector3& () const {return _dir;};

    int GetFace(int i) const {return _faces[i];};
    int& SetFace(int i) {return _faces[i];};
};

typedef const MEdge& MEdgeVal; 
TypeIsSimple(MEdge);

class CMeshGeometry : public CGeometry
{
protected:
  AutoArray<Vector3> _vertexOrig;
  AutoArray<Vector3> _vertex;
  AutoArray<int> _triangle;
  AutoArray<MFace> _face;
  AutoArray<MEdge> _edge; 

  // support for faster NormScale
  bool _validNormScale;
  AutoArray<Vector3> _vertexNormScale;

  Vector3 _geomCenter;
  Vector3 _geomCenterOrig;

  

  char _name[256];

  void FindEdges();
  void CalcDirs();
  void CalcGeomCenterOrig();
  void UpdateOnPos();

  void AdjustScaleAndSepPlaneOnDist(float &dist, float& scale, MPlane& SepPlane, CMeshGeometry& cSecond);

public:
  CMeshGeometry(int ID) ;// : CGeometry(EGT_CONVEX) {};
  virtual ~CMeshGeometry() {};  

  bool LoadFromP3D(LPCSTR path, CMass& mass);
  bool LoadFromP3DSel(const ObjectData& obj, int selIndex);

  virtual void SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation);
  virtual void CalculateEnclosingRectangle();
  virtual BOOL IsIntersection(IRBGeometry& cSecond, int simCycle);
  virtual BOOL ForceInterFree(IRBGeometry& cSecond, int simCycle);
  virtual int FindContacts( RBContactArray& cContactPoints,  IRBGeometry& cSecond, int simCycle) ;

  virtual void Scale(float scale);
  virtual void NormScale();

  virtual void InitModelForDraw(CModelArray& modelArray,IDirect3DDevice9 *pD3DDevice) const;
  virtual void SetOriginForDraw(CModelArray& modelArray,IDirect3DDevice9 *pD3DDevice) const ;
  //virtual void CreateDrawClients(CHordubalClientDll * pcDrawingClient, unsigned int& iVladasID, BOOL bUseDrawstuff, int iBodyColor);

  //virtual void SerializeParamFile(ParamArchive& ar);


  int NFaces() const {return _face.Size();};
  int NPoints() const {return _vertex.Size();};
  int NEdges() const {return _edge.Size();};
  Vector3Val Pos(int i) const {return _vertex[i];}; 
  MPlaneVal GetPlane(int i) const {return _face[i];};
  MFaceVal GetFace(int i) const {return _face[i];};
  MEdgeVal GetEdge(int i) const {return _edge[i];};
  const CMass& GetMass() const {return _mass;};

  Vector3Val GeomCenter() const {return _geomCenter;};

  void CheckFaces() const;
};

class CDCacheItemData : public RefCount
{
public:
  MPlane _sepPlane;
  float _dist;
  int _geom1ID;
  // information about where was plane found
  int _plane1; // if both are positive there are edge IDs otherwise. There plane IDs.
  int _plane2;  
  // scale ... how much should be bodies smaller. valid only if it is positive.
  float _scale; 

  int _lastUpdate; 

  CDCacheItemData() : _dist(-1), _geom1ID(-1), _plane1(-1), _plane2(-1),
    _scale(-1), _lastUpdate(-1) {};
};

extern CDCache<CDCacheItemData> cdCache; 

extern int LastUsedID;
