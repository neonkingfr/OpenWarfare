#include <El/elementpch.hpp>
#include "modelCube.h"
#include "modelBlock.h"
#include "modelString.h"
#include "modelArray.h"

CModelArray::CModelArray() {
  _array.Realloc(10);
  //_array.Resize(10);

  // Create radiosity lights
  //SLight *pLight;

/*
  pLight = new SLight();
  pLight->_id = 0;
  pLight->_position = Vector3(5, 5, 5);
  pLight->_color = Vector3(0.5, 0.5, 0.5);
  _radiosity.LightAdd(pLight);

  pLight = new SLight();
  pLight->_id = 1;
  pLight->_position = Vector3(0, 5, 5);
  pLight->_color = Vector3(0.5, 0.2, 0.2);
  _radiosity.LightAdd(pLight);
*/

 /*for (int j = 0; j < 5; j++) {
    for (int i = 0; i < 5; i++) {
      pLight = new SLight();
      pLight->_id = j * 5 + i;
      pLight->_position = Vector3(i * 1.0f, 5, j * 1.0f);
      pLight->_color = Vector3(1.0f/25.0f, 1.0f/25.0f, 1.0f/25.0f);
      _radiosity.LightAdd(pLight);
    }
  }

*/



}

void CModelArray::CreateDefaultScene(IDirect3DDevice9 *pD3DDevice) {

  _ps = new CPrimitiveStream(pD3DDevice);
  _ps->Init(sizeof(SModelVertex), D3DFVF_MODEL_VERTEX);

  HRESULT hr;
  
  // Register texture
/*
  hr = D3DXCreateTextureFromFile(pD3DDevice, "white.bmp", _pWhite.Init());
  if (SUCCEEDED(hr)) {
    _pWhite->AddRef();
    //_ps->RegisterTextures(0, _pTexture, RStringB("brick.bmp"), NULL, RStringB(""));
  }
*/
  hr = D3DXCreateTextureFromFile(pD3DDevice, "ground.bmp", _pTexture.Init());
  if (SUCCEEDED(hr)) {
    _pTexture->AddRef();
    _ps->RegisterTextures(0, _pTexture, RStringB("brick.bmp"), NULL, RStringB(""));
  }


/*
  CModel *model;
  Matrix4 matrix;

  matrix.SetIdentity();
  

  matrix.SetPosition(Vector3(0, -1, 0));
  model = new CModelBlock(
    pD3DDevice, 1, matrix,
    D3DCOLOR_ARGB(255,255,255,255),
    5.0f, 0.5, 5.0f);
  _array.Add(model);

  //matrix.SetPosition(Vector3(1.5, 1, 1));
  matrix.SetRotationY(H_PI * 0.25f);
  matrix.SetPosition(Vector3(0.5, 0, 0));
  //matrix.SetPosition(Vector3(0, 0, 0));
  model = new CModelBlock(
    pD3DDevice, 0, matrix,
    D3DCOLOR_ARGB(255,255,255,255),
    1, 1, 1);
  _array.Add(model);
*/

}

void CModelArray::CreateModel(IDirect3DDevice9 *pD3DDevice, SMessage *message) {
  CModel *model = NULL;
  Matrix4 matrix;
  memcpy(&matrix, &message->_createModel._origin, sizeof(matrix));
  SModel *pModel;
  switch (message->_createModel._modelType) {
  case modelType_Cube:
    model = new CModelCube(
      pD3DDevice,
      message->_createModel._id,
      matrix,
      message->_createModel._cube._color,
      message->_createModel._cube._size);
    break;
  case modelType_Block:
    model = new CModelBlock(
      pD3DDevice,
      message->_createModel._id,
      matrix,
      message->_createModel._block._color,
      message->_createModel._block._sizeX,
      message->_createModel._block._sizeY,
      message->_createModel._block._sizeZ);

    pModel = new SModel();
    pModel->_id = message->_createModel._id;
    pModel->_node._id = 0;
    pModel->_node._origin = matrix;
    pModel->_node._dimension = Vector3(message->_createModel._block._sizeX, message->_createModel._block._sizeY, message->_createModel._block._sizeZ);
    //_radiosity.ModelAdd(pModel);
    break;
  case modelType_String:
    model = new CModelString(
      message->_createModel._id,
      matrix,
      pD3DDevice,
      message->_createModel._string._color,
      message->_createModel._string._text);
    break;
  }
  _array.Add(model);
}

void CModelArray::DeleteModel(SMessage *message) {
  for (int i = 0; i < _array.Size(); i++) {
    if (_array[i]->GetId() == message->_deleteModel._id) {
      _array.Delete(i);
      break;
    }
  }
  //_radiosity.ModelRemove(message->_deleteModel._id);
}

void CModelArray::MoveModel(SMessage *message) {
  Matrix4 matrix;
  memcpy(&matrix, &message->_moveModel._origin, sizeof(matrix));
  for (int i = 0; i < _array.Size(); i++) {
    if (_array[i]->GetId() == message->_moveModel._id) {
      _array[i]->SetOrigin(matrix);
      break;
    }
  }
  //_radiosity.ModelMove(message->_moveModel._id, matrix);
}

bool CModelArray::MoveModel(int ID, Matrix4Par newOrigin)
{ 
  for (int i = 0; i < _array.Size(); i++) {
    if (_array[i]->GetId() == ID) {
      _array[i]->SetOrigin(newOrigin);
      return true;
    }
  }
  return false;
  //_radiosity.ModelMove(ID, newOrigin);
}

void CModelArray::Draw(IDirect3DDevice9 *pD3DDevice,
                       D3DXMATRIX &matView,
                       D3DXMATRIX &matProj,
                       float backBufferWidth,
                       float backBufferHeight) {
  _ps->RegisterTextures(0, _pTexture, RStringB("brick.bmp"), NULL, RStringB(""));
  for (int i = 0; i < _array.Size(); i++) {
    _array[i]->Draw(pD3DDevice, matView, matProj, backBufferWidth, backBufferHeight);
  }
}

void CModelArray::DrawScene(IDirect3DDevice9 *pD3DDevice,
                            D3DXMATRIX &matView,
                            D3DXMATRIX &matProj,
                            float backBufferWidth,
                            float backBufferHeight,
                            Vector3Par eyePosition) {
  // Register texture
  //_ps->RegisterTextures(0, _pTexture, RStringB("brick.bmp"), NULL, RStringB(""));

  // Set position of the player
  Matrix4 origin;
  origin.SetIdentity();
  origin.SetPosition(eyePosition);
  //_radiosity.SetOrigin(origin);

  // Refine the solution
/*  for (int i = 0; i < 100; i++)
    _radiosity.Refine(this);*/

  // Set the view matrix
  D3DXMATRIX MatTemp;
  D3DXMatrixTranspose(&MatTemp, &D3DXMATRIX(matView));
  pD3DDevice->SetVertexShaderConstantF(0, (float *) MatTemp, 4);

  // Convert patches to a geometry
  _ps->Clear();
  //_radiosity.GetGeometry(*_ps);

  // Draw the geometry
  _ps->Prepare();
  _ps->Draw();


/*
  // Create light
  CLight Light(0, Vector3(0.0f, 5.0f, 0.0f));

  // Add enlighted blocks to the light scope
  for (int i = 0; i < _array.Size(); i++) {
    Vector3 dimension = _array[i]->GetDimension();
    if (dimension != Vector3(0.0f, 0.0f, 0.0f)) {
      Light.AddBlock(this, _array[i]->GetOrigin(), dimension.X(), dimension.Y(), dimension.Z());
    }
  }

  // Split specified number of patches
  for (int i = 0; i < 500; i++) {
    Light.SplitPatch(this);
  }

  // Set the view matrix
  D3DXMATRIX MatTemp;
  D3DXMatrixTranspose(&MatTemp, &D3DXMATRIX(matView));
  pD3DDevice->SetVertexShaderConstant(0, &MatTemp, 4);

  // Convert patches to a geometry
  _ps->Clear();
  Light.GetGeometry(*_ps);

  // Draw the geometry
  _ps->Prepare();
  _ps->Draw();
*/
}

int CModelArray::IntersectsWithAbscissa(Vector3Par p, Vector3Par v) {
  for (int i = 0; i < _array.Size(); i++) {
    if (_array[i]->IntersectsWithAbscissa(p, v)) return 1;
  }
  return 0;
}
