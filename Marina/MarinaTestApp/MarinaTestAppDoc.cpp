// MarinaTestAppDoc.cpp : implementation of the CMarinaTestAppDoc class
//

#include "stdafx.h"
#include "MarinaTestApp.h"

#include "modelArray.h"
#include "modelArrow.h"
#include "MarinaTestAppDoc.h"
#include "MainFrm.h"
#include "MyMass.h"
#include "MarinaGeometry.h"
#include "MarinaTestAppBody.h"
#include ".\marinatestappdoc.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RESTITUTION 0.6f
#define FRICTION 0.7f

// CMarinaTestAppDoc

IMPLEMENT_DYNCREATE(CMarinaTestAppDoc, CDocument)

BEGIN_MESSAGE_MAP(CMarinaTestAppDoc, CDocument)
  ON_COMMAND(ID_TEST_CUBE, OnTestCube)
  ON_COMMAND(ID_TEST_MOVE, OnTestMove)
  ON_COMMAND(ID_BODY_CREATE_1, OnBodyCreate1)
  ON_COMMAND(ID_BODY_CREATE_2, OnBodyCreate2)
  ON_COMMAND(ID_BODY_CREATE, OnBodyCreate)
  ON_COMMAND(ID_ADD_IMPULSE, OnAddImpulse)
  ON_COMMAND(ID_BODY_CREATE_3, OnBodyCreate3)
  ON_COMMAND(ID_BODY_CREATE_4, OnBodyCreate4)
  ON_COMMAND(ID_BODY_CREATE_5, OnBodyCreate5)
  ON_COMMAND(ID_BODY_CREATE_6, OnBodyCreate6)
  ON_COMMAND(ID_BODY_CREATE_7, OnBodyCreate7)
  ON_COMMAND(ID_BODY_CREATE_8, OnBodyCreate8)
  ON_COMMAND(ID_BODY_CREATE_9, OnBodyCreate9)
  ON_COMMAND(ID_MODE_PER_STEP, OnModePerStep)
  ON_COMMAND(ID_NEXT_STEP, OnNextStep)
END_MESSAGE_MAP()


// CMarinaTestAppDoc construction/destruction

CMarinaTestAppDoc::CMarinaTestAppDoc() : m_cSimulation(*this), m_modePerStep(FALSE)
{
	// TODO: add one-time construction code here
  m_cSimulation.SetExternalAccel(Vector3(0,0,-9.8f));
}

CMarinaTestAppDoc::~CMarinaTestAppDoc()
{
}

BOOL CMarinaTestAppDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

  RigidBodyObject * tBody = new RigidBodyObject();
  tBody->_group = NULL;
  MarinaTestAppBody * body = new MarinaTestAppBody(); 
  tBody->_physBody = body;
  tBody->_static = TRUE;

  Vector3 cPos(0,0,-1.6);
  tBody->_physBody->SetPos(cPos);

  CQuaternion cQuat(1, 0, 0, 0);

  tBody->_physBody->SetOrientation(cQuat);
  CMass cMass;       

  CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);  
  pcGeometry->LoadFromP3D("x:\\prim\\podlaha.p3d", cMass); 

  cMass.SetRelPoint(cMass.MassCenter());
  pcGeometry->TranslateRelOrigin(-cMass.MassCenter());
  pcGeometry->SetOrigin(cPos, cQuat);
  pcGeometry->CalculateEnclosingRectangle();

  //pcGeometry->InitModelForDraw(m_models, ((CMainFrame *) AfxGetMainWnd())->m_pd3dDevice);

  tBody->_geometry = pcGeometry;            

  //tBody->_physBody->SetInvMass(1.0f / cMass.Mass());
  body->SetInertiaTensor(cMass.InertiaTensor());
  body->SetInvInertiaTensor(cMass.InertiaTensor().InverseGeneral());
  //body->SetRestitutionCoef(RESTITUTION);
  //body->SetSlidingFrictionCoef(FRICTION);
  //tBody->_physBody->SetAngularVelocity(Vector3(10,0,0));
  tBody->_physBody->ActualizeTensors();

  tBody->_physBody->AngularMomentumFromAngularVelocity();

  m_cSimulation.AddBody(tBody);  
  tBody->_geometry->SetBody(tBody);
  
	return TRUE;
}

// CMarinaTestAppDoc serialization
void CMarinaTestAppDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CMarinaTestAppDoc diagnostics

#ifdef _DEBUG
void CMarinaTestAppDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMarinaTestAppDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMarinaTestAppDoc commands

void CMarinaTestAppDoc::OnTestCube()
{
  SMessage msg;
  msg._messageType = messageType_CreateModel;
  msg._createModel._id = 1;
  memset(&msg._createModel._origin,0x0, sizeof(msg._createModel._origin));
  msg._createModel._origin._aside._x = 1;
  msg._createModel._origin._up._y = 1;
  msg._createModel._origin._direction._z = 1;

  msg._createModel._modelType = modelType_Cube;
  msg._createModel._cube._size = 1.0f;
  msg._createModel._cube._color = 0xff0000ff;

  CMainFrame * pFrm = ((CMainFrame *) AfxGetMainWnd());
  m_models.CreateModel(pFrm->m_pd3dDevice, &msg);

  UpdateAllViews(NULL);
}

void CMarinaTestAppDoc::OnTestMove()
{
  SMessage msg;
  msg._messageType = messageType_MoveModel;
  msg._moveModel._id = 1;
  memset(&msg._createModel._origin,0x0, sizeof(msg._createModel._origin));
  msg._moveModel._origin._aside._x = 1;
  msg._moveModel._origin._up._y = 1;
  msg._moveModel._origin._direction._z = 1;
  msg._moveModel._origin._position._x = 1;

  m_models.MoveModel(&msg);
  UpdateAllViews(NULL);
}

void CMarinaTestAppDoc::OnModePerStep() 
{
  if (m_modePerStep)
  {
    //first finish actual sim
    while (MARINA_NEXT == m_cSimulation.SimulateOneStep(0.01f));
  }
  m_modePerStep = !m_modePerStep;
};

void CMarinaTestAppDoc::OnNextStep()
{
  if (!m_modePerStep)
  {
    UpdateAllViews(NULL);
    return;
  }

   m_arrows.ReleaseModels();
   m_cSimulation.SimulateOneStep(0.01f);
   UpdateAllViews(NULL);
}

void CMarinaTestAppDoc::Simulate(float time)
{ 
  if (m_modePerStep)
  {
    UpdateAllViews(NULL);
    return;
  }

  /*m_cSimulation.SimulateOneStep(time);
    UpdateAllViews(NULL);
    return;
  }*/

  m_arrows.ReleaseModels();
  LARGE_INTEGER dwFrequency;
  QueryPerformanceFrequency(&dwFrequency);
  LARGE_INTEGER dwStart;
  QueryPerformanceCounter(&dwStart);

  START_PROFILE(all);
  m_cSimulation.Simulate(time);
  END_PROFILE(all);

  LARGE_INTEGER dwEnd;
  QueryPerformanceCounter(&dwEnd);
  float fTime = (dwEnd.LowPart - dwStart.LowPart) / (1.0f * dwFrequency.LowPart) * 1000.0f;
  
  LogF("==============%.4f ms ==================", fTime);
  for(int i = 0; i < GPerfProfilers.N(); i++)
  {
    LogF(", %s: %d", GPerfProfilers.Name(i), GPerfProfilers.Value(i));    
  }
  GPerfProfilers.Frame(time);
  UpdateAllViews(NULL);
}



void CMarinaTestAppDoc::PrepareModelsForDraw(LPDIRECT3DDEVICE9 pDevice)
{
  RigidBodyArray& bodies = m_cSimulation.GetBodies();

  for(int i = 0; i < bodies.Size(); i++)
  {
    ((CGeometry *) bodies[i]->_geometry)->SetOriginForDraw(m_models, pDevice);    
  }
}

void CMarinaTestAppDoc::OnBodyCreate1()
{
  /*RigidBodyObject * tBody = new RigidBodyObject();
  tBody->_group = NULL;
  MarinaTestAppBody * body = new MarinaTestAppBody(); 
  tBody->_physBody = body;       
  tBody->_static = FALSE;

  Vector3 cPos(0,0,-0.5);
  tBody->_physBody->SetPos(cPos);

  CQuaternion cQuat(1, 0, 0, 0);

  tBody->_physBody->SetOrientation(cQuat);
  CMass cMass;       

  CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
  pcGeometry->LoadFromP3D("X:\\prim\\hranol.p3d", cMass); 

  cMass.SetRelPoint(cMass.MassCenter());
  pcGeometry->TranslateRelOrigin(-cMass.MassCenter());
  pcGeometry->SetOrigin(cPos, cQuat);

  //pcGeometry->InitModelForDraw(m_models, ((CMainFrame *) AfxGetMainWnd())->m_pd3dDevice);

  tBody->_geometry = pcGeometry;            

  body->SetInvMass(1.0f / cMass.Mass());
  body->SetInertiaTensor(cMass.InertiaTensor());
  body->SetInvInertiaTensor(cMass.InertiaTensor().InverseGeneral());
  //body->SetRestitutionCoef(RESTITUTION);
  //body->SetSlidingFrictionCoef(FRICTION);
  //tBody->_physBody->SetAngularVelocity(Vector3(10,0,0));
  tBody->_physBody->ActualizeTensors();

  tBody->_physBody->AngularMomentumFromAngularVelocity();

  m_cSimulation.AddBody(tBody);
  tBody->_geometry->SetBody(tBody);  
  {
  
  RigidBodyObject * tBody = new RigidBodyObject();
  tBody->_group = NULL;
  MarinaTestAppBody * body = new MarinaTestAppBody(); 
  tBody->_physBody = body;       
  tBody->_static = FALSE;

  Vector3 cPos(0.3,0.3,0.8);
  tBody->_physBody->SetPos(cPos);

  CQuaternion cQuat(1, 0, 0, 0);

  tBody->_physBody->SetOrientation(cQuat);
  CMass cMass;       

  CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
  pcGeometry->LoadFromP3D("X:\\prim\\skrin.p3d", cMass); 

  cMass.SetRelPoint(cMass.MassCenter());
  pcGeometry->TranslateRelOrigin(-cMass.MassCenter());
  pcGeometry->SetOrigin(cPos, cQuat);

  //pcGeometry->InitModelForDraw(m_models, ((CMainFrame *) AfxGetMainWnd())->m_pd3dDevice);

  tBody->_geometry = pcGeometry;            

  body->SetInvMass(1.0f / cMass.Mass());
  body->SetInertiaTensor(cMass.InertiaTensor());
  body->SetInvInertiaTensor(cMass.InertiaTensor().InverseGeneral());
  //body->SetRestitutionCoef(RESTITUTION);
  //body->SetSlidingFrictionCoef(FRICTION);
  //tBody->_physBody->SetAngularVelocity(Vector3(10,0,0));
  tBody->_physBody->ActualizeTensors();

  tBody->_physBody->AngularMomentumFromAngularVelocity();

  m_cSimulation.AddBody(tBody);
  tBody->_geometry->SetBody(tBody);  
  }*/

#define D3DRGB(r, g, b) \
  (0xff000000L | ( ((long)((r) * 255)) << 16) | (((long)((g) * 255)) << 8) | (long)((b) * 255))

  AddArrow(Vector3(0,0,0), Vector3(0,1,0), 5, D3DRGB(1,0,0));
}

void CMarinaTestAppDoc::OnBodyCreate2()
{
  for(int i = 0; i < 2; i++ )
  {

    RigidBodyObject * tBody = new RigidBodyObject();
    tBody->_group = NULL;
    MarinaTestAppBody * body = new MarinaTestAppBody(); 
    tBody->_physBody = body;       
    tBody->_static = FALSE;

    Vector3 cPos(0,2,0.6 + i *3);
    tBody->_physBody->SetPos(cPos);

    CQuaternion cQuat(1, 0, 0, 0);

    tBody->_physBody->SetOrientation(cQuat);
    CMass cMass;       

    CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
    pcGeometry->LoadFromP3D("X:\\prim\\Hranol2.p3d", cMass); 

    cMass.SetRelPoint(cMass.MassCenter());
    pcGeometry->TranslateRelOrigin(-cMass.MassCenter());
    pcGeometry->SetOrigin(cPos, cQuat);

    //pcGeometry->InitModelForDraw(m_models, ((CMainFrame *) AfxGetMainWnd())->m_pd3dDevice);

    tBody->_geometry = pcGeometry;            

    body->SetInvMass(1.0f / cMass.Mass());
    body->SetInertiaTensor(cMass.InertiaTensor());
    body->SetInvInertiaTensor(cMass.InertiaTensor().InverseGeneral());
    //body->SetRestitutionCoef(RESTITUTION);
    //body->SetSlidingFrictionCoef(FRICTION);
    //tBody->_physBody->SetAngularVelocity(Vector3(10,0,0));
    tBody->_physBody->ActualizeTensors();

    tBody->_physBody->AngularMomentumFromAngularVelocity();

    m_cSimulation.AddBody(tBody); 
    tBody->_geometry->SetBody(tBody);  
  }
}

void CMarinaTestAppDoc::OnBodyCreate()
{
  CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
    _T("Models (*.p3d)|*.p3d"));

  if (IDOK != dlg.DoModal())
    return;

  CMass cMass;
  CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
  if (!pcGeometry->LoadFromP3D(dlg.GetPathName(), cMass))
  {
    // load failed
    CString text;
    text.Format("Failed load of %s", dlg.GetPathName());
    AfxMessageBox(text,MB_OK);
    delete pcGeometry;
    return;
  }

  RigidBodyObject * tBody = new RigidBodyObject();
  tBody->_group = NULL;
  MarinaTestAppBody * body = new MarinaTestAppBody(); 
  tBody->_physBody = body;      
  tBody->_static = FALSE;

  Vector3 cPos(0,2,10.6);
  tBody->_physBody->SetPos(cPos);

  /*
  CQuaternion cQuat(1, 1, 0, 0);
    cQuat.Normalize();*/

  CQuaternion cQuat(1, 0, 0, 0);
  

  tBody->_physBody->SetOrientation(cQuat);      
  
  cMass.SetRelPoint(cMass.MassCenter());
  pcGeometry->TranslateRelOrigin(-cMass.MassCenter());
  pcGeometry->SetOrigin(cPos, cQuat);

  //pcGeometry->InitModelForDraw(m_models, ((CMainFrame *) AfxGetMainWnd())->m_pd3dDevice);

  tBody->_geometry = pcGeometry;   
  

  body->SetInvMass(1.0f / cMass.Mass());
  body->SetInertiaTensor(cMass.InertiaTensor());
  body->SetInvInertiaTensor(cMass.InertiaTensor().InverseGeneral());
  //body->SetRestitutionCoef(RESTITUTION);
  //body->SetSlidingFrictionCoef(FRICTION);
  //tBody->_physBody->SetAngularVelocity(Vector3(10,0,0));
  tBody->_physBody->ActualizeTensors();

  tBody->_physBody->AngularMomentumFromAngularVelocity();

  m_cSimulation.AddBody(tBody); 
  tBody->_geometry->SetBody(tBody);  
}

BOOL CMarinaTestAppDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
  

  return CDocument::OnSaveDocument(lpszPathName);
}

void CMarinaTestAppDoc::OnAddImpulse()
{
  RigidBodyArray& bodies = m_cSimulation.GetBodies();
  int last = bodies.Size() - 1;
  Ref<RigidBodyObject> body = bodies[last]; 

  if (body->_group)
    body->_group->Melt();
  body->_physBody->SetVelocity(Vector3(-1, 0, -20));
  body->_physBody->SetAngularVelocity(Vector3(-1, -3, 0));
  body->_physBody->AngularMomentumFromAngularVelocity();
}

void CMarinaTestAppDoc::CreateBodyAtPos(CString modelName, const Vector3& pos, const CQuaternion& orient, const Vector3& speed, const Vector3& angularSpeed, bool staticBody )
{
  CMass cMass;   
  CComposeGeometry * pcGeometry = new CComposeGeometry(++LastUsedID);
  if (!pcGeometry->LoadFromP3D(modelName, cMass))
  {
    // load failed
    CString text;
    text.Format("Failed load of %s", modelName);
    AfxMessageBox(text,MB_OK);
    delete pcGeometry;
    return;
  }

  RigidBodyObject * tBody = new RigidBodyObject();
  tBody->_group = NULL;
  MarinaTestAppBody * body = new MarinaTestAppBody(); 
  tBody->_physBody = body;       
  tBody->_static = staticBody;
  tBody->_physBody->SetPos(pos);
  tBody->_physBody->SetOrientation(orient);      
  cMass.SetRelPoint(cMass.MassCenter());
  pcGeometry->TranslateRelOrigin(-cMass.MassCenter());

  pcGeometry->SetOrigin(pos, orient);  
  pcGeometry->CalculateEnclosingRectangle();

  tBody->_geometry = pcGeometry;            

  body->SetInvMass(1.0f / cMass.Mass());
  body->SetInertiaTensor(cMass.InertiaTensor());
  body->SetInvInertiaTensor(cMass.InertiaTensor().InverseGeneral());

  tBody->_physBody->ActualizeTensors();
  tBody->_physBody->SetVelocity(speed);
  tBody->_physBody->SetAngularVelocity(angularSpeed);
  tBody->_physBody->AngularMomentumFromAngularVelocity();

  m_cSimulation.AddBody(tBody); 
  tBody->_geometry->SetBody(tBody); 
}

void CMarinaTestAppDoc::OnBodyCreate3()
{
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0,0), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.85,0,0), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.85,0,0), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(1.7,0,0), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-1.7,0,0), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.425,0,1.2), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.425,0,1.2), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(1.275,0,1.2), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-1.275,0,1.2), CQuaternion(1,0,0,0));   
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0,2.4), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.85,0,2.4), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.85,0,2.4), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.425,0,3.6), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.425,0,3.6), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0,4.8), CQuaternion(1,0,0,0)); 

}

void CMarinaTestAppDoc::OnBodyCreate4()
{
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.425,0.24,0), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.425,0.24,0), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0.97,0), CQuaternion(1,0,0,0));

  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0,1.2), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.85,0,1.2), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.85,0,1.2), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.425,0.73,1.2), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.425,0.73,1.2), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,1.46,1.2), CQuaternion(1,0,0,0));  

  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.425,0.24,2.4), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.425,0.24,2.4), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0.97,2.4), CQuaternion(1,0,0,0));  

  //CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0.73,3.6), CQuaternion(1,0,0,0));   

 /* CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0,2.4), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.85,0,2.4), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.85,0,2.4), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.425,0,3.6), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.425,0,3.6), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0,4.8), CQuaternion(1,0,0,0));
  */
}

void CMarinaTestAppDoc::OnBodyCreate5()
{
  CreateBodyAtPos(CString("P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\BrickA\\BrickhouseA01\\SmallBrickhouseA01b.p3d"), Vector3(0,0,3.6318), CQuaternion(1,0,0,0), VZero, VZero, true);  
}


void CMarinaTestAppDoc::OnBodyCreate6()
{

  float offsetx = 0;
  float offsetz = 0;

  for(int i = 9; i > 0; i --)
  {
    offsetx += 0.425f;
    offsetz += 1.2f;
    for(int j = 0; j < i; j++ )
    {
      CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(offsetx + 0.85 * j,0,offsetz), CQuaternion(1,0,0,0));  
    }
  }
  /*
CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0,0), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.85,0,0), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.85,0,0), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(1.7,0,0), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-1.7,0,0), CQuaternion(1,0,0,0)); 

  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.425,0,1.2), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.425,0,1.2), CQuaternion(1,0,0,0)); 
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(1.275,0,1.2), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-1.275,0,1.2), CQuaternion(1,0,0,0));   

  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0,2.4), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.85,0,2.4), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.85,0,2.4), CQuaternion(1,0,0,0)); 

  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0.425,0,3.6), CQuaternion(1,0,0,0));  
  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(-0.425,0,3.6), CQuaternion(1,0,0,0)); 

  CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(0,0,4.8), CQuaternion(1,0,0,0)); 
*/

}

void CMarinaTestAppDoc::OnBodyCreate7()
{
  float offsetx = 0;
  float offsetz = 0;

  for(int i = 5; i > 0; i --)
  {
    offsetx += 1.8f;
    offsetz += 2.8f;
    for(int j = 0; j < i; j++ )
    {
      CreateBodyAtPos(CString("X:\\prim\\Hranol.p3d"), Vector3(offsetx + 3.0f * j,0,offsetz), CQuaternion(1,0,0,0));  
    }
  }
}

void CMarinaTestAppDoc::OnBodyCreate8()
{
  float offsetx = 0;
  float offsetz = 3;

  for(int i = 2; i > 0; i --)
  {
    offsetx += 0.425f;
    offsetz -= 1.2f;
    for(int j = 0; j < i; j++ )
    {
      CreateBodyAtPos(CString("X:\\prim\\barel1.p3d"), Vector3(offsetx + 0.85 * j,0,offsetz), CQuaternion(1,0,0,0));  
    }
  }
}

void CMarinaTestAppDoc::OnBodyCreate9()
{
  float offsetx = -20;
  float offsetz = 0;

  for(int i = 9; i > 0; i --)
  {
    offsetx += 1.8f;
    offsetz += 2.8f;
    for(int j = 0; j < i; j++ )
    {
      CreateBodyAtPos(CString("X:\\prim\\Hranol.p3d"), Vector3(offsetx + 3.0f * j,0,offsetz), CQuaternion(1,0,0,0));  
    }
  }
}

#define D3DRGB(r, g, b) \
  (0xff000000L | ( ((long)((r) * 255)) << 16) | (((long)((g) * 255)) << 8) | (long)((b) * 255))

void CMarinaTestAppDoc::AddArrow(Vector3Val pos, Vector3Val dir, float size, DWORD color)
{
  Matrix4 trans;
  Vector3 dirMat(1,0,0);
  Vector3 dirArrow(dir[0],dir[2],dir[1]);
  if (dir * dirArrow < 0.5)
    trans.SetDirectionAndUp(Vector3(1,0,0),Vector3(dir[0],dir[2],dir[1]));
  else
    trans.SetDirectionAndUp(Vector3(0,0,1),Vector3(dir[0],dir[2],dir[1]));

  trans.SetPosition(Vector3(pos[0],pos[2],pos[1]));
  
  //D3DCOLOR color = D3DRGB(1, 0, 0);

  CMainFrame * pFrm = ((CMainFrame *) AfxGetMainWnd());
  LPDIRECT3DDEVICE9  pd3dDevice = pFrm->GetD3DDevice();

  CModelArrow * pArrow = new CModelArrow(pd3dDevice, 0, trans, color, size/10);
  m_arrows.AddModel(pArrow);
  //UpdateAllViews(NULL);  
}