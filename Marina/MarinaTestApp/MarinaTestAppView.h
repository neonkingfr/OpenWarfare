// MarinaTestAppView.h : interface of the CMarinaTestAppView class
//


#pragma once

enum EMouseMode
{
  EMM_NORMAL = 0,
  EMM_CAMERA = 1
};


class CMarinaTestAppView : public CView
{
protected:
  
  Vector3                 m_EyePos;
  float                   m_EyeRotY;
  float                   m_EyeRotX;
  BOOL                    m_bWireframe;
  BOOL                    m_bTextures;
  EMouseMode m_mouseMode;

  CPoint m_lastMouse;

  BOOL                    m_bLoadingApp;          // TRUE, if the app is loading
  
  //LPDIRECTINPUT9          m_pDI;                  // DirectInput object
  //LPDIRECTINPUTDEVICE9    m_pKeyboard;            // DirectInput keyboard device
  //UserInput               m_UserInput;            // Struct for storing user input 


protected: // create from serialization only
	CMarinaTestAppView();
	DECLARE_DYNCREATE(CMarinaTestAppView)

// Attributes
public:
	CMarinaTestAppDoc* GetDocument() const;

	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
  virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
  virtual ~CMarinaTestAppView();
#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  HRESULT Render();


protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

	DECLARE_MESSAGE_MAP()
public:
  virtual void OnInitialUpdate();
  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnThrowBall();
  afx_msg void OnThrowBody();
};

#ifndef _DEBUG  // debug version in MarinaTestAppView.cpp
inline CMarinaTestAppDoc* CMarinaTestAppView::GetDocument() const
   { return reinterpret_cast<CMarinaTestAppDoc*>(m_pDocument); }
#endif

