#pragma once

#include "marina.hpp"

class MarinaTestAppBody : public IRigidBody
{
// data members
protected:
  marina_real _fInvMass;
  Matrix3 _cInvInertiaTensor;
  Matrix3 _cInertiaTensor;

  Matrix3 _cActualInertiaTensor;
  Matrix3 _cActualInvInertiaTensor;

  Vector3 _cPos;
  CQuaternion _cOrientation;

  Vector3 _cMomentum;
  Vector3 _cVelocity;

  Vector3 _cAngularMomentum;
  Vector3 _cAngularVelocity;

  Vector3 _cCenterPosBody;
  Vector3 _cCenterPos;

  Vector3 _cMassCenter;  
public:
  MarinaTestAppBody(): _fInvMass(0), _cPos(VZero), _cVelocity(VZero),
    _cMomentum(VZero),_cAngularVelocity(VZero), _cAngularMomentum(VZero), 
    _cCenterPosBody(VZero), _cCenterPos(VZero), _cMassCenter(VZero) {};

  virtual ~MarinaTestAppBody() {};

  /*-------------------
  Get methods.
  -------------------*/

  virtual marina_real GetInvMass() const {return _fInvMass;};
  virtual const Matrix3& GetInvInertiaTensor() const { return _cInvInertiaTensor;};
  virtual const Matrix3& GetInertiaTensor() const { return _cInertiaTensor;};

  virtual const Matrix3& GetActualInvInertiaTensor() const { return _cActualInvInertiaTensor;};
  virtual const Matrix3& GetActualInertiaTensor() const { return _cActualInertiaTensor;};

  virtual const Vector3& GetPos() const {return _cPos;};
  virtual const CQuaternion& GetOrientation() const {return _cOrientation;};

  virtual const Vector3& GetMomentum() const {return _cMomentum;};
  virtual const Vector3& GetVelocity() const {return _cVelocity;};
  virtual const Vector3& GetMassCenter() const {return _cPos;};

  virtual const Vector3& GetAngularMomentum() const {return _cAngularMomentum;};
  virtual const Vector3& GetAngularVelocity() const {return _cAngularVelocity;};
  
  /*-------------------
  Set methods.
  -------------------*/

  virtual void SetInvMass(marina_real fInvMass) { _fInvMass = fInvMass;};

  virtual void SetInvInertiaTensor(const Matrix3& cInvInertiaTensor) {  _cInvInertiaTensor = cInvInertiaTensor;};
  virtual void SetInertiaTensor(const Matrix3& cInertiaTensor) {  _cInertiaTensor = cInertiaTensor;};

  virtual void SetActualInvInertiaTensor(const Matrix3& mat) { _cActualInvInertiaTensor = mat;};
  virtual void SetActualInertiaTensor(const Matrix3& mat) { _cActualInertiaTensor = mat;};

  virtual void SetPos(const Vector3& cPos) {_cPos = cPos;};
  virtual void SetOrientation(const CQuaternion& cOrientation) {_cOrientation = cOrientation; _cOrientation.Normalize();};

  virtual void SetMomentum(const Vector3& cMomentum) {_cMomentum = cMomentum;};
  virtual void SetVelocity(const Vector3& cVelocity) {_cVelocity = cVelocity;};
  virtual void SetMassCenter(const Vector3& cPos) {_cPos = cPos;};

  virtual void SetAngularMomentum(const Vector3& cAngularMomentum) {_cAngularMomentum = cAngularMomentum;};
  virtual void SetAngularVelocity(const Vector3& cAngularVelocity) {_cAngularVelocity = cAngularVelocity;};
};