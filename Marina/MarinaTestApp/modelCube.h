#ifndef _modelCube_h_
#define _modelCube_h_

#include "model3D.h"

class CModelCube : public CModel3D {
private:
  ComRef<IDirect3DTexture9> _pTexture;
public:
  CModelCube(
    IDirect3DDevice9 *pD3DDevice,
    int id,
    Matrix4Par origin,
    D3DCOLOR color,
    float size);
  virtual int IntersectsWithAbscissa(Vector3Par p, Vector3Par v) {return 0;};
};

#endif