// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "MarinaTestApp.h"

#include "MainFrm.h"
#include "Model.h"
#include "d3dutil.h"
#include "MarinaTestAppDoc.h"
#include "MarinaTestAppView.h"
#include ".\mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_WM_CREATE()
  ON_COMMAND(ID_RUN_TIME_MODE, OnRunTimeMode)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame() : m_pD3D(NULL), m_pd3dDevice(NULL)
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

  /////////////////////////////
  // D3D

  HRESULT hr;

  // Create the Direct3D object
  m_pD3D = Direct3DCreate9( D3D_SDK_VERSION );
  if( m_pD3D == NULL )
  {
    DisplayErrorMsg( D3DAPPERR_NODIRECT3D, MSGERR_APPMUSTEXIT );
    return FALSE;
  }

  hr;
  if (FAILED(hr = DirectInput8Create(AfxGetInstanceHandle(), DIRECTINPUT_VERSION, 
    IID_IDirectInput8, (void **) m_pDI.Init(), NULL)))
  {
    LogF("Failed: DirectInput8Create");
    return FALSE;
  }

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}
#endif //_DEBUG

HRESULT CMainFrame::DisplayErrorMsg( HRESULT hr, DWORD dwType )
{
  TCHAR strMsg[512];

  switch( hr )
  {
  case D3DAPPERR_NODIRECT3D:
    _tcscpy( strMsg, _T("Could not initialize Direct3D. You may\n")
      _T("want to check that the latest version of\n")
      _T("DirectX is correctly installed on your\n")
      _T("system.  Also make sure that this program\n")
      _T("was compiled with header files that match\n")
      _T("the installed DirectX DLLs.") );
    break;

  case D3DAPPERR_NOCOMPATIBLEDEVICES:
    _tcscpy( strMsg, _T("Could not find any compatible Direct3D\n")
      _T("devices.") );
    break;

  case D3DAPPERR_NOWINDOWABLEDEVICES:
    _tcscpy( strMsg, _T("This sample cannot run in a desktop\n")
      _T("window with the current display settings.\n")
      _T("Please change your desktop settings to a\n")
      _T("16- or 32-bit display mode and re-run this\n")
      _T("sample.") );
    break;

  case D3DAPPERR_NOHARDWAREDEVICE:
    _tcscpy( strMsg, _T("No hardware-accelerated Direct3D devices\n")
      _T("were found.") );
    break;

  case D3DAPPERR_HALNOTCOMPATIBLE:
    _tcscpy( strMsg, _T("This sample requires functionality that is\n")
      _T("not available on your Direct3D hardware\n")
      _T("accelerator.") );
    break;

  case D3DAPPERR_NOWINDOWEDHAL:
    _tcscpy( strMsg, _T("Your Direct3D hardware accelerator cannot\n")
      _T("render into a window.\n")
      _T("Press F2 while the app is running to see a\n")
      _T("list of available devices and modes.") );
    break;

  case D3DAPPERR_NODESKTOPHAL:
    _tcscpy( strMsg, _T("Your Direct3D hardware accelerator cannot\n")
      _T("render into a window with the current\n")
      _T("desktop display settings.\n")
      _T("Press F2 while the app is running to see a\n")
      _T("list of available devices and modes.") );
    break;

  case D3DAPPERR_NOHALTHISMODE:
    _tcscpy( strMsg, _T("This sample requires functionality that is\n")
      _T("not available on your Direct3D hardware\n")
      _T("accelerator with the current desktop display\n")
      _T("settings.\n")
      _T("Press F2 while the app is running to see a\n")
      _T("list of available devices and modes.") );
    break;

  case D3DAPPERR_MEDIANOTFOUND:
    _tcscpy( strMsg, _T("Could not load required media." ) );
    break;

  case D3DAPPERR_RESIZEFAILED:
    _tcscpy( strMsg, _T("Could not reset the Direct3D device." ) );
    break;

  case D3DAPPERR_NONZEROREFCOUNT:
    _tcscpy( strMsg, _T("A D3D object has a non-zero reference\n")
      _T("count (meaning things were not properly\n")
      _T("cleaned up).") );
    break;

  case D3DAPPERR_NULLREFDEVICE:
    _tcscpy( strMsg, _T("Warning: Nothing will be rendered.\n")
      _T("The reference rendering device was selected, but your\n")
      _T("computer only has a reduced-functionality reference device\n")
      _T("installed.  Install the DirectX SDK to get the full\n")
      _T("reference device.\n") );
    break;

  case E_OUTOFMEMORY:
    _tcscpy( strMsg, _T("Not enough memory.") );
    break;

  case D3DERR_OUTOFVIDEOMEMORY:
    _tcscpy( strMsg, _T("Not enough video memory.") );
    break;

  default:
    _tcscpy( strMsg, _T("Generic application error. Enable\n")
      _T("debug output for detailed information.") );
  }

  if( MSGERR_APPMUSTEXIT == dwType )
  {
    _tcscat( strMsg, _T("\n\nThis sample will now exit.") );
    //MessageBox( NULL, strMsg, m_strWindowTitle, MB_ICONERROR|MB_OK );
    ::MessageBox( NULL, strMsg, _T("Error"), MB_ICONERROR|MB_OK );

    // Close the window, which shuts down the app
    if( m_hWnd )
      ::SendMessage( m_hWnd, WM_CLOSE, 0, 0 );
  }
  else
  {
    if( MSGWARN_SWITCHEDTOREF == dwType )
      _tcscat( strMsg, _T("\n\nSwitching to the reference rasterizer,\n")
      _T("a software device that implements the entire\n")
      _T("Direct3D feature set, but runs very slowly.") );
    ::MessageBox( NULL, strMsg, _T("Error"), MB_ICONWARNING|MB_OK );
  }

  return hr;
}

void CMainFrame::OnViewCreated(CWnd * pView)
{
  if (m_pd3dDevice)
    return; 

  D3DPRESENT_PARAMETERS d3dpp; 
  ZeroMemory( &d3dpp, sizeof(d3dpp) );
  d3dpp.Windowed = TRUE;
  d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
  d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
  d3dpp.hDeviceWindow = pView->m_hWnd;
  CRect wndRect;
  pView->GetWindowRect(wndRect);
  //d3dpp.BackBufferWidth = 1152;
  //d3dpp.BackBufferHeight = 864;
  d3dpp.EnableAutoDepthStencil = TRUE;
  d3dpp.AutoDepthStencilFormat = D3DFMT_D16;


  HRESULT hr;
  if( FAILED( hr = m_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, pView->m_hWnd,
    D3DCREATE_SOFTWARE_VERTEXPROCESSING,
    &d3dpp, m_pd3dDevice.Init())))
    return;

  LPDIRECT3DSURFACE9 pBackBuffer;
  //UNSAFE
  m_pd3dDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer );
  pBackBuffer->GetDesc( &m_d3dsdBackBuffer );
  pBackBuffer->Release();

  if (FAILED( hr = InitDeviceObjects()))
    return;

  if (FAILED( hr = RestoreDeviceObjects()))
    return;

  //DirectInput
  if (FAILED(hr = m_pDI->CreateDevice(GUID_SysKeyboard, m_pKeyb.Init(), NULL)))
  {
    LogF("Failed: CreateDevice(GUID_SysKeyboard, m_pKeyb.Init(), NULL)");
    return;
  }

  hr = m_pKeyb->SetDataFormat(&c_dfDIKeyboard);
  hr = m_pKeyb->SetCooperativeLevel(m_hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
  m_pKeyb->Acquire();

  if (FAILED(hr = m_pDI->CreateDevice(GUID_SysMouse, m_pMouse.Init(), NULL)))
  {
    LogF("Failed: CreateDevice(GUID_SysKeyboard, m_pMouse.Init(), NULL)");
    return;
  }

  hr = m_pMouse->SetDataFormat(&c_dfDIMouse);
  hr = m_pMouse->SetCooperativeLevel(m_hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);



  DIPROPDWORD dipdw;
  // the header
  dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
  dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
  dipdw.diph.dwObj        = 0;
  dipdw.diph.dwHow        = DIPH_DEVICE;
  // the data
  dipdw.dwData            = SAMPLE_BUFFER_SIZE;
 
  hr = m_pMouse->SetProperty(DIPROP_BUFFERSIZE, &dipdw.diph);

  m_pMouse->Acquire();
  MDIMaximize(pView->GetParentFrame());

}

//-----------------------------------------------------------------------------
// Name: InitDeviceObjects()
// Desc: Initialize scene objects.
//-----------------------------------------------------------------------------

D3DVERTEXELEMENT9 vertexDeclBasic[] =
{
  {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
  {0, 12, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   0},
  {0, 24, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
  {0, 28, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
  D3DDECL_END()
};

HRESULT CMainFrame::InitDeviceObjects()
{
  //m_ModelArray.CreateDefaultScene(m_pd3dDevice);
  // Create shaders
  ID3DXBuffer *pCode;
  ComRef<IDirect3DVertexDeclaration9> pVertDecl;

  FAILED_HR(m_pd3dDevice->CreateVertexDeclaration(vertexDeclBasic, pVertDecl.Init()));
  FAILED_HR(m_pd3dDevice->SetVertexDeclaration(pVertDecl));
   
  FAILED_HR(D3DXAssembleShaderFromFile("Model.vsh", 0, NULL, 0 /*D3DXSHADER_DEBUG*/, &pCode, NULL));
  FAILED_HR(m_pd3dDevice->CreateVertexShader((DWORD*)pCode->GetBufferPointer(), m_VSBranch.Init()));
  pCode->Release();
  FAILED_HR(D3DXAssembleShaderFromFile("Model.psh", 0, NULL, 0 /*D3DXSHADER_DEBUG*/, &pCode, NULL ));
  FAILED_HR(m_pd3dDevice->CreatePixelShader((DWORD*)pCode->GetBufferPointer(),m_PSBranch.Init()));
  pCode->Release();
  return S_OK;
}

//-----------------------------------------------------------------------------
// Name: RestoreDeviceObjects()
// Desc: Restores scene objects.
//-----------------------------------------------------------------------------
HRESULT CMainFrame::RestoreDeviceObjects()
{
  // TODO: setup render states
  HRESULT hr;

  // Setup a material
  D3DMATERIAL9 mtrl;
  D3DUtil_InitMaterial( mtrl, 1.0f, 0.0f, 0.0f );
  FAILED_HR(m_pd3dDevice->SetMaterial( &mtrl ));

  // Set p the textures
  FAILED_HR(m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE ));
  FAILED_HR(m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE ));
  FAILED_HR(m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE ));
  
  FAILED_HR(m_pd3dDevice->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR ));
  FAILED_HR(m_pd3dDevice->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR ));

  // Set miscellaneous render states
  FAILED_HR(m_pd3dDevice->SetRenderState( D3DRS_DITHERENABLE,   FALSE ));
  FAILED_HR(m_pd3dDevice->SetRenderState( D3DRS_SPECULARENABLE, FALSE ));
  FAILED_HR(m_pd3dDevice->SetRenderState( D3DRS_ZENABLE,        TRUE ));
  FAILED_HR(m_pd3dDevice->SetRenderState( D3DRS_AMBIENT,        0x000F0F0F ));
  
  //m_pd3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

  /*
  m_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
  m_pd3dDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
  m_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  m_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
  */





  // Set the projection matrix
  /*
  D3DXMATRIX matProj;
  FLOAT fAspect = ((FLOAT)m_d3dsdBackBuffer.Width) / m_d3dsdBackBuffer.Height;
  D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, fAspect, 1.0f, 100.0f );
  m_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
  */

  // Create a D3D font using D3DX     
  if( FAILED( hr = D3DXCreateFont( m_pd3dDevice, 20,20,FW_BOLD, 3, FALSE, ANSI_CHARSET,OUT_DEFAULT_PRECIS,ANTIALIASED_QUALITY, 
    FF_DONTCARE,"Arial", m_pD3DXFont.Init() ) ) )
    return DXTRACE_ERR_MSGBOX( "D3DXCreateFont", hr );

  return S_OK;
}

HRESULT CMainFrame::UpdateInput()
{
  ZeroMemory( &m_UserInput.diks, sizeof(m_UserInput.diks) );
  m_UserInput.mouseDataFilled = 0;

  HRESULT hr = m_pKeyb->GetDeviceState( sizeof(m_UserInput.diks), &m_UserInput.diks );
  if( FAILED(hr) ) 
  {
    m_pKeyb->Acquire();
    return hr; 
  }

  m_UserInput.bRotateLeft   = ( (m_UserInput.diks[DIK_LEFT] & 0x80)   == 0x80 );
  m_UserInput.bRotateRight  = ( (m_UserInput.diks[DIK_RIGHT] & 0x80)  == 0x80 );
  m_UserInput.bRotateUp     = ( (m_UserInput.diks[DIK_PGUP] & 0x80)   == 0x80 );
  m_UserInput.bRotateDown   = ( (m_UserInput.diks[DIK_PGDN] & 0x80)   == 0x80 );

  m_UserInput.bMoveForward  = ( (m_UserInput.diks[DIK_UP] & 0x80)     == 0x80 );
  m_UserInput.bMoveBackward = ( (m_UserInput.diks[DIK_DOWN] & 0x80)   == 0x80 );

  m_UserInput.bMoveUp       = ( (m_UserInput.diks[DIK_Q] & 0x80)      == 0x80 );
  m_UserInput.bMoveDown     = ( (m_UserInput.diks[DIK_A] & 0x80)      == 0x80 );

  m_UserInput.bWireframe    = ( (m_UserInput.diks[DIK_1] & 0x80)      == 0x80 );
  m_UserInput.bTextures     = ( (m_UserInput.diks[DIK_2] & 0x80)      == 0x80 );

  m_UserInput.bRunTimeOff     = ( (m_UserInput.diks[DIK_SPACE] & 0x80)      == 0x80 );
  

  /// Mouse
  m_UserInput.mouseDataFilled = SAMPLE_BUFFER_SIZE;
  hr = m_pMouse->GetDeviceData(sizeof(DIDEVICEOBJECTDATA), 
    m_UserInput.mouseData, &m_UserInput.mouseDataFilled, 0);

  if ( FAILED(hr) ) 
  {
    m_pMouse->Acquire();
    return hr; 
  }

  return S_OK; 
}

void CMainFrame::OnIdle()
{
  /*CFrameWnd * pAcFrm = GetActiveFrame();
  if (pAcFrm)
  {
    CMarinaTestAppDoc * pDoc = (CMarinaTestAppDoc *) pAcFrm->GetActiveDocument();
    pDoc->Simulate();
  }*/
  PostMessage(WM_COMMAND, ID_RUN_TIME_MODE,0);
}

void CMainFrame::OnRunTimeMode()
{
  CFrameWnd * pAcFrm = GetActiveFrame();
  if (pAcFrm)
  {
    CMarinaTestAppDoc * pDoc = (CMarinaTestAppDoc *) pAcFrm->GetActiveDocument();
    if (pDoc != NULL)
      pDoc->Simulate(0.01f);
    //pView->Render();
  }
}


