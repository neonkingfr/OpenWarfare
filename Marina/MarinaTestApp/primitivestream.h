#ifndef _primitivestream_h_
#define _primitivestream_h_

#include <d3d9.h>
#include <Es/Containers/array.hpp>
#include <Es/Strings/rString.hpp>

#define PRIMITIVESTAGE_COUNT 512
//#define VB_SIZE 65536
//#define IB_SIZE (65536*4)
//#define VB_SIZE 32768
//#define IB_SIZE (32768*4)

#define VB_SIZE 512
#define IB_SIZE 512

//! Set of primitives represented as a indices to vertex buffer.
/*!
  Type of index is set to D3DFMT_INDEX16. Type of primitive is D3DPT_TRIANGLELIST.
*/
class CPrimitiveStage {
private:
  //! List of triangles associated with this stage. All of them will be drawn as a single drawindexedprimitive.
  AutoArray<WORD> _Indices;
  //! Minimum index of a vertex associated with this stage.
  UINT _MinVertexIndex;
  //! Maximum index of a vertex associated with this stage.
  UINT _MaxVertexIndex;
public:
  //! Texture on stage 0
  //IDirect3DTexture9 *_pTextureS0;
  ComRef<IDirect3DTexture9> _pTextureS0;
  //! Name of the texture on stage 0
  RStringB _TS0Name;
  //! Texture on stage 1
  //IDirect3DTexture9 *_pTextureS1;
  ComRef<IDirect3DTexture9> _pTextureS1;
  //! Name of the texture on stage 1
  RStringB _TS1Name;
  //! Constructor
  CPrimitiveStage();
  //! Clears this stage.
  void Clear();
  //! Add specified index. Array will be resized if necessary.
  void AddIndex(const WORD &Index);
  //! Returns number of indices in the array.
  int Size();
  //! Returns pointer to stage data.
  const WORD *Data();
  //! Returns minimal vertex index associated with this stage.
  UINT GetMinVertexIndex();
  //! Returns size of the vertex block associated with this stage.
  UINT GetNumVertices();
};

//! Stream of primitives to draw.
/*!
  Type of vertex is specified by _VertexFVF and its size by _VertexSize.
*/
class CPrimitiveStream : public RefCount {
private:
  //! 3D Device.
  ComRef<IDirect3DDevice9> _pD3DDevice;
  //! Vertex buffer.
  ComRef<IDirect3DVertexBuffer9> _pVB;
  //! Index buffer.
  ComRef<IDirect3DIndexBuffer9> _pIB;
  //! List of primitive stages.
  CPrimitiveStage _PS[PRIMITIVESTAGE_COUNT];
  //! Array of vertices.
  AutoArray<BYTE> _Vertices;
  //! Size of one vertex in bytes.
  int _VertexSize;
  //! Either handle for the vertex shader or FVF code.
  //DWORD _VertexHandle;
  //! Handle for the pixel shader.
  //DWORD _PixelHandle;
  //! Positions of stages in index buffer. This is set by Prepare() method.
  UINT _StageIndexPos[PRIMITIVESTAGE_COUNT];
  //! Number of indices in all stages together.
  int _IndexCount;
public:
  //! Constructor.
  CPrimitiveStream(IDirect3DDevice9 *pD3DDevice);
  //! Copy constructor.
  /*!
    This is very quick copy constructor. Both streams will share their VB and IB.
    \param PrimitiveStream Source stream.
  */
  CPrimitiveStream(CPrimitiveStream &PrimitiveStream);
  //! Initialization.
  /*!
    \param VertexSize Size of single vertex.
    \param VertexHandle Either handle for the vertex shader or FVF code.
    \param PixelHandle Handle for the pixel shader.
    \param FVF FVF code which should correspond to VertexHandle.
  */
  void Init(int VertexSize, DWORD FVF);
  //! Clears all vertices and indices.
  void Clear();
  //! Add single vertex to vertex buffer.
  /*!
    \param pData Pointer to vertex data.
    \return Index of new vertex in vertex buffer.
  */
  WORD AddVertex(void *pData);
  //! Add single index to index buffer at specified stage.
  /*!
    \param Stage Index of stage to add index to.
    \param Index Index to add.
  */
  void AddIndex(int Stage, WORD Index);
  //! Gets index of the new vertex in the vertex buffer.
  WORD GetNewVertexIndex();
  //! Registers textures on specified stage.
  void RegisterTextures(int Stage, IDirect3DTexture9 *pTextureS0, RStringB TS0Name, IDirect3DTexture9 *pTextureS1, RStringB TS1Name);
  //! Prepares vertex and index buffers.
  /*!
    Besides, this method set up the _StageIndexPos array which will be used
    in Draw method.
  */
  void Prepare();
  //! Draws primitives on the surface.
  void Draw(BOOL SetTextures = TRUE);
  //! Returns array of vertices
  AutoArray<BYTE> GetVertices();
  //! Returns specified stage
  CPrimitiveStage *GetPrimitiveStage(int i);
  //! Returns number of indices
  int GetIndexCount();
};

#endif