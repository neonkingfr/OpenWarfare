#include <El/elementpch.hpp>
#include "modelArrow.h"

CModelArrow::CModelArrow
(
 IDirect3DDevice9 *pD3DDevice,
 int id,
 Matrix4Par origin,
 D3DCOLOR color,
 float size) 
 : CModel3D(modelType_Arrow, id, origin, pD3DDevice, color) 
{  

  float size2 = size * 0.05f;
  SModelVertex mv;
  WORD VIndex;

  // bottom side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(size2, 0, size2);
  mv._normal = Vector3(0, -1, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-size2, 0, size2);
  mv._normal = Vector3(0, -1, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-size2, 0, -size2);
  mv._normal = Vector3(0, -1, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(size2, 0, -size2);
  mv._normal = Vector3(0, -1, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 3);


  // Front side 1

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(size2, 0, size2);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-size2, 0, size2);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(0, size, 0);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);  

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);

  // Front side 2

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(-size2, 0, size2);
  mv._normal = Vector3(-1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-size2, 0, -size2);
  mv._normal = Vector3(-1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(0, size, 0);
  mv._normal = Vector3(-1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);  

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);

  // Front side 3

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(-size2, 0,-size2);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(size2, 0, -size2);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(0, size, 0);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);  

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);

  // Front side 4

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(size2, 0, -size2);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(size2, 0, size2);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(0, size, 0);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);  

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);

  _ps->Prepare();

}