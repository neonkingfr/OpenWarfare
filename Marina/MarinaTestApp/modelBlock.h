#ifndef _modelBlock_h_
#define _modelBlock_h_

#include "model3D.h"

class CModelBlock : public CModel3D {
private:
  ComRef<IDirect3DTexture9> _pTexture;
  float _sizeX;
  float _sizeY;
  float _sizeZ;
  //! Intersection of a face (a, u, v) and abscissa (p, w)
  int FaceIntersectsWithAbscissa(
    Vector3Par a,
    Vector3Par u,
    Vector3Par v,
    Vector3Par p,
    Vector3Par w);
public:
  CModelBlock(
    IDirect3DDevice9 *pD3DDevice,
    int id,
    Matrix4Par origin,
    D3DCOLOR color,
    float sizeX,
    float sizeY,
    float sizeZ);
    virtual int IntersectsWithAbscissa(Vector3Par p, Vector3Par w);
    virtual Vector3 GetDimension();
};

#endif