// MarinaTestAppView.cpp : implementation of the CMarinaTestAppView class
//

#include "stdafx.h"
#include "MarinaTestApp.h"

#include "modelArray.h"
#include "MarinaTestAppDoc.h"

#include "MarinaTestAppView.h"
#include ".\marinatestappview.h"
#include "MainFrm.h"
#include "Model.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMarinaTestAppView

IMPLEMENT_DYNCREATE(CMarinaTestAppView, CView)

BEGIN_MESSAGE_MAP(CMarinaTestAppView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
  ON_WM_ERASEBKGND()
  ON_WM_LBUTTONDOWN()
  ON_WM_MOUSEMOVE()
  ON_WM_RBUTTONDOWN()
  ON_WM_LBUTTONUP()
  ON_WM_RBUTTONUP()
  ON_COMMAND(ID_THROW_BALL, OnThrowBall)
  ON_COMMAND(ID_THROW_BODY, OnThrowBody)
END_MESSAGE_MAP()

// CMarinaTestAppView construction/destruction
#define GRAD_TO_RAD(a) (a/180.0 * 3.1415)

CMarinaTestAppView::CMarinaTestAppView()
{
	// TODO: add construction code here
  m_EyePos = Vector3(0,15,-25);
  m_bWireframe = FALSE;
  m_EyeRotY = 0;//GRAD_TO_RAD(-60);
  m_EyeRotX =GRAD_TO_RAD(20);
  m_mouseMode = EMM_NORMAL;
}

CMarinaTestAppView::~CMarinaTestAppView()
{
}

BOOL CMarinaTestAppView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMarinaTestAppView drawing

void CMarinaTestAppView::OnDraw(CDC* /*pDC*/)
{
	CMarinaTestAppDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

  Render();

	// TODO: add draw code for native data here
}


// CMarinaTestAppView printing

BOOL CMarinaTestAppView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMarinaTestAppView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMarinaTestAppView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CMarinaTestAppView diagnostics

#ifdef _DEBUG
void CMarinaTestAppView::AssertValid() const
{
	CView::AssertValid();
}

void CMarinaTestAppView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMarinaTestAppDoc* CMarinaTestAppView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMarinaTestAppDoc)));
	return (CMarinaTestAppDoc*)m_pDocument;
}
#endif //_DEBUG


// CMarinaTestAppView message handlers

void CMarinaTestAppView::OnInitialUpdate()
{
  CView::OnInitialUpdate();

  ((CMainFrame *) AfxGetMainWnd())->OnViewCreated(this);
  
  

}

HRESULT  CMarinaTestAppView::Render()
{
 
  // Set the wireframe
  CMainFrame * pFrm = ((CMainFrame *) AfxGetMainWnd());
  //pFrm->UpdateInput();

  LPDIRECT3DDEVICE9  pd3dDevice = pFrm->GetD3DDevice();        // The D3D rendering device

  //if (m_bWireframe)
    pd3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
  //else
  //  pd3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

  // Clear the viewport
  pd3dDevice->Clear( 0L, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,
    0xfff0f0f0, 1.0f, 0L );

  // Set shaders
  pd3dDevice->SetVertexShader(pFrm->m_VSBranch);
  pd3dDevice->SetPixelShader(pFrm->m_PSBranch);

  // Set shader constants
  D3DXMATRIX matProj;
  FLOAT fAspect = ((FLOAT)pFrm->m_d3dsdBackBuffer.Width) / pFrm->m_d3dsdBackBuffer.Height;
  D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, fAspect, 1.0f, 100.0f );
  //pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
  D3DXMATRIX MatTemp;
  D3DXMatrixTranspose(&MatTemp, &matProj);
  pd3dDevice->SetVertexShaderConstantF(4,(const float *) MatTemp, 4);

  float BranchC8[] = {0.58f, -0.58f, 0.58f, 0.0f};
  pd3dDevice->SetVertexShaderConstantF(8, BranchC8, 1);
  float BranchC9[] = {0.5f, 1.0f, 0.0f, 0.0f};
  pd3dDevice->SetVertexShaderConstantF(9, BranchC9, 1);
  float BranchpC0[] = {0.4f, 0.4f, 0.4f, 0.0f}; // Ambient light color
  pd3dDevice->SetPixelShaderConstantF(0, BranchpC0, 1);

  // Set the View matrix
  //m_EyeRotY += 0.1;
  Matrix4 matRotY;
  matRotY.SetRotationY(m_EyeRotY);
  Matrix4 matRotX;
  matRotX.SetRotationX(m_EyeRotX);
  Matrix4 matPos;
  matPos.SetIdentity();
  matPos.SetPosition(m_EyePos);
  Matrix4 mat;
  //mat = matPos * matRotY * matRotX;
  mat = (matPos * matRotY * matRotX).InverseGeneral();

  D3DXMATRIX matView;
  ConvertMatrix(matView, mat);

  // Begin the scene
  if( SUCCEEDED( pd3dDevice->BeginScene() ) )
  {
    // Drawing the models
    /*
    m_ModelArray.Draw(
    pd3dDevice,
    matView,
    matProj,
    (float)m_d3dsdBackBuffer.Width,
    (float)m_d3dsdBackBuffer.Height);
    */

    CMainFrame * pFrm = ((CMainFrame *) AfxGetMainWnd());
    GetDocument()->PrepareModelsForDraw(pd3dDevice);
    GetDocument()->GetModels().Draw(
      pd3dDevice,
      matView,
      matProj,
      (float)pFrm->m_d3dsdBackBuffer.Width,
      (float)pFrm->m_d3dsdBackBuffer.Height);

    pd3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

    GetDocument()->GetArrows().Draw(
      pd3dDevice,
      matView,
      matProj,
      (float)pFrm->m_d3dsdBackBuffer.Width,
      (float)pFrm->m_d3dsdBackBuffer.Height);

    /*m_ModelArray.DrawScene(
      pd3dDevice,
      matView,
      matProj,
      (float)m_d3dsdBackBuffer.Width,
      (float)m_d3dsdBackBuffer.Height,
      m_EyePos);
*/

    // Render stats and help text  
    //RenderText();

    // End the scene.
    pd3dDevice->EndScene();
    pd3dDevice->Present(NULL, NULL, m_hWnd, NULL);
  }

  return S_OK;
}


BOOL CMarinaTestAppView::OnEraseBkgnd(CDC* pDC)
{
  // TODO: Add your message handler code here and/or call default

  //return CView::OnEraseBkgnd(pDC);
  return FALSE;
}

void CMarinaTestAppView::OnMouseMove(UINT nFlags, CPoint point)
{
  if (m_mouseMode == EMM_CAMERA)
  {
    CPoint diff = point - m_lastMouse;
    m_lastMouse = point;

    if  (nFlags & MK_LBUTTON)
    {
      if (nFlags & MK_RBUTTON)
      {
#define TO_SCENE_COORD  0.01f
        Matrix4 matRotY;
        matRotY.SetRotationY(m_EyeRotY);
        Matrix4 matRotX;
        matRotX.SetRotationX(m_EyeRotX);        
        Matrix4 mat;
        //mat = matPos * matRotY * matRotX;
        mat = (matRotY * matRotX);

        m_EyePos += mat * Vector3(-diff.x * TO_SCENE_COORD, 0 ,diff.y * TO_SCENE_COORD);
        //m_EyePos[2] += matdiff.y * TO_SCENE_COORD;
        //m_EyePos[0] -= diff.x * TO_SCENE_COORD;
      }
      else
      {
        //rotate camera

        m_EyeRotY += diff.x * TO_SCENE_COORD;
        m_EyeRotX -= diff.y * TO_SCENE_COORD;
      }
    }
    else
    {
      if (nFlags & MK_RBUTTON)
      {
        Matrix4 matRotY;
        matRotY.SetRotationY(m_EyeRotY);
        Matrix4 matRotX;
        matRotX.SetRotationX(m_EyeRotX);        
        Matrix4 mat;
        //mat = matPos * matRotY * matRotX;
        mat = (matRotY * matRotX);

        m_EyePos += mat * Vector3(- diff.x * TO_SCENE_COORD, diff.y * TO_SCENE_COORD ,0 );
        //m_EyePos[1] += diff.y * TO_SCENE_COORD;
        //m_EyePos[0] -= diff.x * TO_SCENE_COORD;
      }
    }
  }
 
  CView::OnMouseMove(nFlags, point);
}

void CMarinaTestAppView::OnLButtonDown(UINT nFlags, CPoint point)
{
  // change view properties
  SetCapture();
  m_mouseMode = EMM_CAMERA;
  m_lastMouse = point;

  CView::OnLButtonDown(nFlags, point);
}

void CMarinaTestAppView::OnRButtonDown(UINT nFlags, CPoint point)
{
  SetCapture();
  m_mouseMode = EMM_CAMERA;
  m_lastMouse = point;

  CView::OnRButtonDown(nFlags, point);
}

void CMarinaTestAppView::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (!(nFlags & MK_RBUTTON))
  {
    ReleaseCapture();
    m_mouseMode = EMM_NORMAL;
  }

  CView::OnLButtonUp(nFlags, point);
}

void CMarinaTestAppView::OnRButtonUp(UINT nFlags, CPoint point)
{
  if (!(nFlags & MK_LBUTTON))
  {
    ReleaseCapture();
    m_mouseMode = EMM_NORMAL;
  }

  CView::OnRButtonUp(nFlags, point);
}

void CMarinaTestAppView::OnThrowBall()
{
  Matrix4 matRotY;
  matRotY.SetRotationY(m_EyeRotY);
  Matrix4 matRotX;
  matRotX.SetRotationX(m_EyeRotX);        
  Matrix4 mat;
  //mat = matPos * matRotY * matRotX;
  mat = (matRotY * matRotX);

  Vector3 speed = mat * Vector3(0,0,100);

  GetDocument()->CreateBodyAtPos(_T("x:\\prim\\krychle_tezka.p3d"), Vector3(m_EyePos[0],m_EyePos[2],m_EyePos[1]) , CQuaternion(1,0,0,0),Vector3(speed[0], speed[2], speed[1]));
}

void CMarinaTestAppView::OnThrowBody()
{
  Matrix4 matRotY;
  matRotY.SetRotationY(m_EyeRotY);
  Matrix4 matRotX;
  matRotX.SetRotationX(m_EyeRotX);        
  Matrix4 mat;
  //mat = matPos * matRotY * matRotX;
  mat = (matRotY * matRotX);

  Vector3 speed = mat * Vector3(0,0,20);

  GetDocument()->CreateBodyAtPos(_T("x:\\prim\\barel1.p3d"), Vector3(m_EyePos[0],m_EyePos[2],m_EyePos[1]) , CQuaternion(1,0,0,0),Vector3(speed[0], speed[2], speed[1]));
}
