#include <El/elementpch.hpp>
#include "modelCube.h"

CModelCube::CModelCube(IDirect3DDevice9 *pD3DDevice,
                       int id,
                       Matrix4Par origin,
                       D3DCOLOR color,
                       float size) : CModel3D(modelType_Cube, id, origin, pD3DDevice, color) {

  // Create and register texture
  HRESULT hr = D3DXCreateTextureFromFile(pD3DDevice, "brick.bmp", _pTexture.Init());
  if (SUCCEEDED(hr)) {
    _ps->RegisterTextures(0, _pTexture, RStringB("brick.bmp"), NULL, RStringB(""));
  }

  float halfSize = size * 0.5f;
  SModelVertex mv;
  WORD VIndex;

  // Back side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSize, halfSize, halfSize);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSize, -halfSize, halfSize);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, -halfSize, halfSize);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, halfSize, halfSize);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 3);
  _ps->AddIndex(0, VIndex + 2);


  // Front side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSize, halfSize, -halfSize);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSize, -halfSize, -halfSize);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, -halfSize, -halfSize);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, halfSize, -halfSize);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 3);



  // Right side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSize, halfSize, halfSize);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSize, -halfSize, halfSize);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSize, -halfSize, -halfSize);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSize, halfSize, -halfSize);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 3);


  // Left side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(-halfSize, halfSize, halfSize);
  mv._normal = Vector3(-1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, -halfSize, halfSize);
  mv._normal = Vector3(-1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, -halfSize, -halfSize);
  mv._normal = Vector3(-1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, halfSize, -halfSize);
  mv._normal = Vector3(-1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 3);
  _ps->AddIndex(0, VIndex + 2);


  // Top side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSize, halfSize, halfSize);
  mv._normal = Vector3(0,1,0 );
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSize, halfSize, -halfSize);
  mv._normal = Vector3(0,1,0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, halfSize, -halfSize);
  mv._normal = Vector3(0,1,0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, halfSize, halfSize);
  mv._normal = Vector3(0, 1, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 3);


  // Bottom side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSize, -halfSize, halfSize);
  mv._normal = Vector3(0, -1, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSize, -halfSize, -halfSize);
  mv._normal = Vector3(0, -1, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, -halfSize, -halfSize);
  mv._normal = Vector3(0, -1, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSize, -halfSize, halfSize);
  mv._normal = Vector3(0, -1, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 3);
  _ps->AddIndex(0, VIndex + 2);


  _ps->Prepare();
}