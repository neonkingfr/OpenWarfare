#include "stdafx.h"

#include <Es/essencepch.hpp>
#include <Es/Framework/appFrame.hpp>

#if _MSC_VER && !defined INIT_SEG_COMPILER
#pragma warning(disable:4074)
#pragma init_seg(compiler)
#define INIT_SEG_COMPILER
#endif


class FileAppFrameFunctions : public AppFrameFunctions
{
public:
  virtual void LogF(const char *format, va_list argptr);
};


void FileAppFrameFunctions::LogF(const char *format, va_list argptr)
{
  char  buf[512];
  vsprintf(buf,format,argptr);
  strcat(buf,"\n");

  OutputDebugString(buf);

  FILE * f = fopen("c:\\temp\\marina.log","a");
  fputs(buf,f);
  fclose(f);
}

static FileAppFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;

