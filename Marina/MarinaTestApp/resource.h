//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MarinaTestApp.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_MarinaTestAppTYPE           129
#define ID_TEST_CUBE                    32771
#define ID_TEST_MOVE                    32773
#define ID_RUN_TIME_MODE                32775
#define ID_BODY_CREATE_1                32777
#define ID_BODY_CREATE_2                -32756
#define ID_BODY_CREATE                  32781
#define ID_COMP_CREATE                  32784
#define ID_ACCELERATOR32786             32786
#define ID_ADD_IMPULSE                  32786
#define ID_BODY_CREATE_3                32788
#define ID_BODY_CREATE_4                32790
#define ID_BODY_CREATE_5                32792
#define ID_THROW_BALL                   32793
#define ID_BODY_CREATE_6                32794
#define ID_THROW_BODY                   -32738
#define ID_BODY_CREATE_7                32799
#define ID_BODY_CREATE_8                32801
#define ID_BODY_CREATE_9                32802
#define ID_MODE_PER_STEP                32804
#define ID_NEXT_STEP                    32806

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32808
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
