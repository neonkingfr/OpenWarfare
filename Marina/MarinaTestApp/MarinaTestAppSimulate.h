#pragma once

#include <marina.hpp>


class CMarinaTestAppDoc;
class CMarinaTestAppSimulation : public SimulationRB
{
protected:
  CMarinaTestAppDoc& _doc;

public:
  CMarinaTestAppSimulation(CMarinaTestAppDoc& doc) : _doc(doc), SimulationRB() {};

  virtual bool BreakBodies(SeparateGeomArray & sep);
  virtual bool CanBreakBodies(SeparateGeomArray & sep);
  virtual void AddArrow(Vector3Val pos,Vector3Val dir, float size, DWORD color);
  virtual void ResetArrows();
};

