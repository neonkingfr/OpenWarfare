#include "stdafx.h"
#include <marina.hpp>

#include <cd/IntersectConvex.hpp>
#include <cd/ContactConvex.hpp>
#include "MarinaGeometry.h"
#include <RigidBody.h>
#include "modelMesh.h"

marina_real g_fTolerance = 0.01f;

#define IGNORANCE_TRY_STEP 5
#define IGNORANCE_SCALE 0.6f

int LastUsedID = 0;

/*LSError Serialize(ParamArchive& ar, const RStringB &name, CQuaternion& value, int minVersion)
{
  if (ar.GetArVersion() < minVersion) return LSOK;
  if (ar.IsSaving())
  {
    AUTO_STATIC_ARRAY(float, array, 4);
    array.Resize(4);
    for (int i=0; i<4; i++) array[i] = value[i];
    CHECK_INTERNAL(ar.SerializeArray(name, array, minVersion))
  }
  else
  {
    if (ar.GetPass() != ParamArchive::PassFirst) return LSOK;
    AUTO_STATIC_ARRAY(float, array, 4);
    CHECK_INTERNAL(ar.SerializeArray(name, array, minVersion))
      if (array.Size() == 0) return LSNoEntry; // default value
    Assert(array.Size() == 4);
    for (int i=0; i<4; i++) value[i] = array[i];
  }
  return LSOK;
}

void CGeometry::SerializeParamFile(ParamArchive& ar)
{
  ar.Serialize("id", (int) _type, 0);
  ::Serialize(ar, "relPos", _cRelPos, 0);
  ::Serialize(ar, "relOrientation", _cRelOrientation, 0);

}*/

void CComposeGeometry::SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation)
{
  _cPos = cPos + cOrientation.GetMatrix() * _cRelPos;
  _cOrientation = cOrientation * _cRelOrientation;

  for( int i = 0; i < _cGeomArray.Size(); i++)  
    _cGeomArray[i]->SetOrigin(_cPos, _cOrientation);  
}

void CComposeGeometry::InitModelForDraw(CModelArray& modelArray,IDirect3DDevice9 *pD3DDevice) const
{
  for( int i = 0; i < _cGeomArray.Size(); i++)
    _cGeomArray[i]->InitModelForDraw(modelArray, pD3DDevice);
}

void CComposeGeometry::SetOriginForDraw(CModelArray& modelArray,IDirect3DDevice9 *pD3DDevice) const 
{
  for( int i = 0; i < _cGeomArray.Size(); i++)
    _cGeomArray[i]->SetOriginForDraw(modelArray, pD3DDevice);
}

void CComposeGeometry::CalculateEnclosingRectangle()
{
  if (_cGeomArray.Size() == 0)
    return;

  //_cGeomArray[0]->SetOrigin(_cPos, _cOrientation);
  _cGeomArray[0]->CalculateEnclosingRectangle();

  _cEnclosingRectangle = _cGeomArray[0]->GetEnclosingRectangle();

  for( int i = 1; i < _cGeomArray.Size(); i++)
  {
    //_cGeomArray[i]->SetOrigin(_cPos, _cOrientation);
    _cGeomArray[i]->CalculateEnclosingRectangle();

    _cEnclosingRectangle += _cGeomArray[i]->GetEnclosingRectangle();
  }
}

int CComposeGeometry::FindContacts( RBContactArray& cContactPoints, IRBGeometry& cSecond, int simCycle)
{
  CRectangle cEnlargedEnclRectangle(_cEnclosingRectangle);
  cEnlargedEnclRectangle.Enlarge(g_fTolerance);

  if (!cEnlargedEnclRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return MARINA_OK; // enclosing rectagles doen not overlap
  }

  // Do every it with every member
  Ref<CDCacheItemData> data;
  if (cdCache.Get(GetID(), cSecond.GetID(), data))
  {
    if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
    {            
      return MARINA_OK; //ignore
    }
  }
  for( int i = 0; i < _cGeomArray.Size(); i++)
  {
    _cGeomArray[i]->FindContacts(cContactPoints, cSecond, simCycle);
  }

  return MARINA_OK;
}

BOOL CComposeGeometry::IsIntersection( IRBGeometry& cSecond, int simCycle) 
{
  if (!_cEnclosingRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return FALSE; // enclosing rectagles doen not overlap
  }

  // Do every it with every member
  Ref<CDCacheItemData> data;
  if (cdCache.Get(GetID(), cSecond.GetID(), data))
  {
    if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE && data->_lastUpdate + IGNORANCE_TRY_STEP >= simCycle)
    {            
      return FALSE;
    }
  }

  for( int i = 0; i < _cGeomArray.Size(); i++)
  {
    if (_cGeomArray[i]->IsIntersection(cSecond, simCycle))
    {
      if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE && data->_lastUpdate + IGNORANCE_TRY_STEP < simCycle)
      { 
        // it was just a try
        return FALSE;
      }

      return TRUE;
    }
  }

  if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
  {
    // no need to ignore
    cdCache.Delete(GetID(), cSecond.GetID());    
  }

  return FALSE;
}

BOOL CComposeGeometry::ForceInterFree( IRBGeometry& cSecond, int simCycle)
{
  if (!_cEnclosingRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return TRUE; // enclosing rectagles doen not overlap
  }

  // Do every it with every member

  int i = 0;
  Ref<CDCacheItemData> data;
  if (cdCache.Get(GetID(), cSecond.GetID(), data))
  {
    if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
      return TRUE;
  }

  for(; i < _cGeomArray.Size(); i++)
  {
    if (!_cGeomArray[i]->ForceInterFree(cSecond, simCycle))
      break;
  }

  if (i < _cGeomArray.Size())
  {
    if (data.IsNull())
    {
      data = new CDCacheItemData;
      cdCache.Add(GetID(), cSecond.GetID(), data);
    }

    data->_scale = IGNORANCE_SCALE;
    data->_lastUpdate = simCycle;
    return FALSE;
  }  

  return TRUE;
}

void CComposeGeometry::SetBody(Ref<RigidBodyObject> ptBody)
{
  // distribute ptBody to the childs
  _ptBody = ptBody;

  for( int i = 0; i < _cGeomArray.Size(); i++)
  {
    _cGeomArray[i]->SetBody(ptBody);
  }
}

void CComposeGeometry::TranslateChilds(const Vector3& cRelPos)
{
  for( int i = 0; i < _cGeomArray.Size(); i++)
  {
    _cGeomArray[i]->TranslateRelOrigin(cRelPos);
  }
}

CComposeGeometry::~CComposeGeometry()
{
  for( int i = 0; i < _cGeomArray.Size(); i++)
  {
    delete _cGeomArray[i];
  }
}

CGeometry * CComposeGeometry::DecomposeChild(int ID)
{
  for(int i = 0; i < _cGeomArray.Size(); i++)
  {
    switch (_cGeomArray[i]->GetType())
    {
    case EGT_CONVEX:      
      if (_cGeomArray[i]->GetID() == ID)
      {
        CGeometry * ret = _cGeomArray[i];
        _cGeomArray.Delete(i);

        cdCache.Delete(_ID);
        _mass -= ret->GetMass();
        return ret;          
      }
      break;
    case EGT_COMPOSE:
      {
        CGeometry * ret = ((CComposeGeometry *)_cGeomArray[i])->DecomposeChild(ID);
        if (ret != NULL)
        {
          cdCache.Delete(_ID);
          _mass -= ret->GetMass();
          return ret;
        }
      } 
    }
  }
  return NULL;
}

/*
void CComposeGeometry::SerializeParamFile(ParamArchive& ar)
{
  ar.Serialize("sourceFile", _sourcePath, 0);
  ar.Serialize("sourceFile", _sourcePath, 0);

}
*/

CMeshGeometry::CMeshGeometry(int ID) : CGeometry(EGT_CONVEX, ID),_geomCenter(VZero),_validNormScale(false)
{
  _cEnclosingRectangle = CRectangle(Vector3(0,0,0),Vector3(1,1,1));
}

/*void CMeshGeometry::CreateDrawClients(CHordubalClientDll * pcDrawingClient, unsigned int& iVladasID, BOOL bUseDrawstuff, int iBodyColor)
{
  _bUseDrawstuff = bUseDrawstuff;

  _iBodyColor = iBodyColor;
}*/

static void MatrixFromQuaternionAndPos(Matrix4& tMatrix, const CQuaternion& cOrientation, const Vector3& cPos)
{
  Matrix3 mat = cOrientation.GetMatrix();

  tMatrix.SetDirectionAside(Vector3(mat(0,0), mat(2,0), mat(1,0)));
  tMatrix.SetDirectionUp(Vector3(mat(0,2), mat(2,2), mat(1,2)));
  tMatrix.SetDirection(Vector3(mat(0,1), mat(2,1), mat(1,1)));
  tMatrix.SetPosition(Vector3(cPos[0], cPos[2], cPos[1])); 
};

#define D3DRGB(r, g, b) \
  (0xff000000L | ( ((long)((r) * 255)) << 16) | (((long)((g) * 255)) << 8) | (long)((b) * 255))

void CMeshGeometry::InitModelForDraw(CModelArray& modelArray, IDirect3DDevice9 *pD3DDevice) const
{
  Matrix4 pos;
  MatrixFromQuaternionAndPos(pos, _cOrientation, _cPos);
  srand(_ID);
  D3DCOLOR color = D3DRGB(rand()/(float) RAND_MAX, rand()/(float) RAND_MAX, rand()/(float) RAND_MAX);
  
  CModelMesh * pModel = new CModelMesh(modelType_Mesh, _ID, pos, pD3DDevice, color);

  pModel->BeginInput();

  int n = _triangle.Size() / 3;
  for(int i = 0; i < n; i ++)
  {
    float temp;
    Vector3 ver0 = _vertexOrig[_triangle[3*i]];
    temp = ver0[1];
    ver0[1] = ver0[2];
    ver0[2] = temp;

    Vector3 ver1 = _vertexOrig[_triangle[3*i+1]];
    temp = ver1[1];
    ver1[1] = ver1[2];
    ver1[2] = temp;

    Vector3 ver2 = _vertexOrig[_triangle[3*i+2]];
    temp = ver2[1];
    ver2[1] = ver2[2];
    ver2[2] = temp;

    pModel->AddTriangle(ver0, ver1, ver2);
  }

  pModel->EndInput();

  modelArray.AddModel(pModel);
}

void CMeshGeometry::SetOriginForDraw(CModelArray& modelArray, IDirect3DDevice9 *pD3DDevice) const
{
  Matrix4 pos;

  MatrixFromQuaternionAndPos(pos, _cOrientation, _cPos);
  if (!modelArray.MoveModel(_ID, pos))
  {
    InitModelForDraw(modelArray, pD3DDevice);
    modelArray.MoveModel(_ID, pos);
  }
}


#include "Poseidon/lib/SpecLods.hpp"

class FaceDataRobber : public FaceT
{
public:
  FaceDataRobber(const FaceT& face) : FaceT(face) {};
  int GetN() const {return n;};
  const DataVertex * GetDataVertex() const {return vs;};
};

void SetMassFromLevel(CMass& mass, const ObjectData & geometryLevel)
{
  for(int i = 0; i < geometryLevel.NPoints(); i++)
  {   
    const Vector3 &point = geometryLevel.Point(i);    
    mass.AddMassPoint(geometryLevel.GetPointMass(i), Vector3(point[0], point[2], point[1]));    
  }  
}

bool CMeshGeometry::LoadFromP3D(LPCSTR path, CMass& mass)
{
  // Load form p3d.
  LODObject lodObj;
  lodObj.Load(WFilePath(path), NULL, NULL);
  int gIndex=lodObj.FindLevelExact(GEOMETRY_SPEC);

  if (gIndex == -1)
    return false;

  lodObj.SelectLevel(gIndex);
  ObjectData & geometryLevel = lodObj;

  _vertex.Resize(0);
  _vertex.Reserve(geometryLevel.NPoints(), geometryLevel.NPoints());

  for(int i = 0; i < geometryLevel.NPoints(); i++)
  {
    const Vector3 &point = geometryLevel.Point(i);
    _vertex.Add(Vector3(point[0], point[2], point[1]));   
  }

  _triangle.Resize(0);
  _triangle.Reserve(geometryLevel.NFaces(), geometryLevel.NFaces());

  _face.Resize(0);
  _face.Reserve(geometryLevel.NFaces(), geometryLevel.NFaces());

  for(int i = 0; i < geometryLevel.NFaces(); i++)
  {
    const FaceT& face = geometryLevel.Face(i);
    FaceDataRobber rob(face);

    const int n = rob.GetN();
    const DataVertex  * vertexData =  rob.GetDataVertex();
    for(int j = 0; j < n - 2; j++)
    {
      _triangle.Add(vertexData[j].point);
      _triangle.Add(vertexData[j + 1].point);       
      _triangle.Add(vertexData[n - 1].point);   
    }

    Assert(n < MAX_VERTEXES);
    for(int j = 0; j < n; j++)
    {
      MFace& face = _face.Append();
      face.AddVertex(vertexData[j].point);
    }
  }

  FindEdges();
  CalcDirs();
  CalcGeomCenterOrig();
  _vertexOrig = _vertex;

  SetMassFromLevel(mass, geometryLevel);
  _mass = mass;

  return true;
}

bool CMeshGeometry::LoadFromP3DSel(const ObjectData& obj, int selIndex)
{
  const NamedSelection * sel = obj.GetNamedSel(selIndex);

  strcpy(_name, sel->Name());
  AutoArray<int> mapping;
  mapping.Resize(obj.NPoints());
  _vertex.Resize(0);

  for(int i = 0; i < obj.NPoints(); i++)
  {
    if (sel->PointSelected(i))
    {
      mapping[i] = _vertex.Size();
      const Vector3 &point = obj.Point(i);
      _vertex.Add(Vector3(point[0], point[2], point[1]));
      _mass.AddMassPoint(obj.GetPointMass(i), Vector3(point[0], point[2], point[1]));
    }
    else
    {
      mapping[i] = -1;
    }
  }

  if (_vertex.Size() == 0)
    return false;

  _triangle.Resize(0);

  for(int i = 0; i < obj.NFaces(); i++)
  {
    if (sel->FaceSelected(i))
    {
      const FaceT& face = obj.Face(i);
      FaceDataRobber rob(face);

      const int n = rob.GetN();
      const DataVertex  * vertexData =  rob.GetDataVertex();
      for(int j = 0; j < n - 2; j++)
      {
        _triangle.Add(mapping[vertexData[j].point]);          
        _triangle.Add(mapping[vertexData[j + 1].point]);
        _triangle.Add(mapping[vertexData[n - 1].point]); 

        // teselate faces
        MFace& mface = _face.Append();
        mface.AddVertex(mapping[vertexData[j].point]);
        mface.AddVertex(mapping[vertexData[j + 1].point]);
        mface.AddVertex(mapping[vertexData[n - 1].point]);

      }

      /*Assert(n <= MAX_VERTEXES);
      MFace& mface = _face.Append();
      for(int j = 0; j < n; j++)
      {        
      mface.AddVertex(mapping[vertexData[j].point]);
      } */     
    }
  }

  if (_mass.Mass() == 0)
  {
    CString text;
    text.Format(_T("Zero mass in selection %s"),_name);
    AfxMessageBox(text, MB_OK); 
  }

  LogF("mass: %s %lf", _name, _mass.Mass());

  FindEdges();
  CalcDirs();
  _vertexOrig = _vertex;
  CalcGeomCenterOrig();

  //CheckFaces();

  return true;
}

bool CComposeGeometry::LoadFromP3D(LPCSTR path, CMass& mass)
{
  // Load form p3d.
  LODObject lodObj;
  lodObj.Load(WFilePath(path), NULL, NULL);
  int gIndex=lodObj.FindLevelExact(GEOMETRY_SPEC);

  if (gIndex == -1)
    return false;

  lodObj.SelectLevel(gIndex);
  ObjectData & geometryLevel = lodObj;

  NamedSelection * sel;
  for(int i = 0; sel = geometryLevel.GetNamedSel(i); i++)
  {
    if (strncmp(sel->Name(), "Component", 9) == 0)
    {
      CMeshGeometry * meshGeom = new CMeshGeometry(++LastUsedID);
      if (meshGeom->LoadFromP3DSel(geometryLevel,i))
      {      
        meshGeom->SetRelOrigin(VZero, CQuaternion());    
        Add(*meshGeom);    
      }
      else
        delete meshGeom;
    }
  }

  SetMassFromLevel(mass, geometryLevel);
  _mass = mass;

  // set _sourcePath
  return true;
}


void CMeshGeometry::FindEdges()
{
  _edge.Resize(0);
  for(int i = 0; i < _face.Size(); i++)
  {
    MFace & face = _face[i];
    for(int j = 0; j < face.N(); j++)
    {
      int vert1 = min(face[(j == 0) ? face.N()-1: j-1], face[j]);
      int vert2 = max(face[(j == 0) ? face.N()-1: j-1], face[j]);

      // maybe this edge exist?
      int l = 0;
      for(; l < _edge.Size(); l++)
      {
        if (_edge[l][0] == vert1 && _edge[l][1] == vert2)
          break;
      }

      if ( l == _edge.Size())
      {
        MEdge edge(vert1, vert2);
        edge.SetFace(0) = i;
        _edge.Add(edge);
      }
      else
        _edge[l].SetFace(1) = i;
    }
  }
}

void CMeshGeometry::CalcDirs()
{
  {  
    //PROFILE_SCOPE(face); 
    for(int i = 0; i < _face.Size(); i++)
    {
      _face[i].CalcPlane(_vertex);
    }
  }

  {
    //PROFILE_SCOPE(edge); 
    for(int i = 0; i < _edge.Size(); i++)
    {
      _edge[i].CalcDir(_vertex);
    }
  }
}

void CMeshGeometry::CalcGeomCenterOrig()
{
  _geomCenterOrig = VZero;
  marina_real invSize = 1.0f / _vertexOrig.Size();

  for(int i = 0; i < _vertexOrig.Size(); i++)
  {
    _geomCenterOrig += invSize * _vertexOrig[i];
  }
}

void CMeshGeometry::CheckFaces() const
{
  for(int i = 0; i < _face.Size(); i++)
  {
    _face[i].Check(_vertex);
  }
}

void CMeshGeometry::UpdateOnPos()
{
  {
    //PROFILE_SCOPE(vertex); 
    _validNormScale = false;

    Matrix3 rot = _cOrientation.GetMatrix();

    for(int i = 0; i < _vertexOrig.Size(); i++)  
      _vertex[i] = rot * _vertexOrig[i] + _cPos;

    _geomCenter = rot * _geomCenterOrig + _cPos;
  }

  CalcDirs();
}

void CMeshGeometry::SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation)
{

  _cPos = cPos + cOrientation.GetMatrix() * _cRelPos;
  _cOrientation = cOrientation * _cRelOrientation;

  UpdateOnPos();
}

void CMeshGeometry::Scale(float scale)
{
  // scale distance from vertex to geometry center by scale.
  if (!_validNormScale)
  {
    _vertexNormScale = _vertex; //backUp;
    _validNormScale = true;
  }

  // scale vertexes
  Vector3 scaleCenter = (1 - scale) * _geomCenter;
  for(int i = 0; i < _vertex.Size(); i++)
  {
    _vertex[i] = scaleCenter + scale * _vertexNormScale[i];
  }

  //scale planes
  for(int i = 0; i < _face.Size(); i++)
  {
    _face[i].CalcPlaneD(_vertex);
  }
}

void CMeshGeometry::NormScale()
{
  if (_validNormScale)
  {
    _vertex = _vertexNormScale;

    //scale planes
    for(int i = 0; i < _face.Size(); i++)
    {
      _face[i].CalcPlaneD(_vertex);
    }
  }
}



BOOL CMeshGeometry::IsIntersection( IRBGeometry& cSecond, int simCycle) 
{    
  if (!_cEnclosingRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return FALSE; // enclosing rectagles doen not overlap
  }

  switch (((CGeometry&)cSecond).GetType())
  {
  case EGT_COMPOSE:
    return cSecond.IsIntersection(*this, simCycle);  
  case EGT_CONVEX:
    {
      //PROFILE_SCOPE(inter2);
      FindSeparationPlane<CMeshGeometry, MPlane, MEdge> intersect;
      MPlane sepPlane;
      float dist;
      int plane1 = -1, plane2 = -1;
      float scale = -1;

      Ref<CDCacheItemData> data;
      if (cdCache.Get(GetID(), cSecond.GetID(), data))
      {
        if (GetID() == data->_geom1ID)
        {
          plane1 = data->_plane1;
          plane2 = data->_plane2;
        }
        else
        {
          plane2 = data->_plane1;
          plane1 = data->_plane2;
        }

        if (data->_scale > 0)
        {
          if (data->_scale <= IGNORANCE_SCALE)
          {
            if (data->_lastUpdate + 5 <= simCycle)
            {
              // try it maybe there is allready space
              scale = IGNORANCE_SCALE + 0.01;
            }
            else
              return FALSE;
          }
          else
            scale = data->_scale;

          Scale(data->_scale);
          cSecond.Scale(data->_scale);
        }
      }

      BOOL found = intersect(sepPlane, *this,(const CMeshGeometry&) cSecond, dist, plane1, plane2);      

      if (scale > 0)
      {
        if (found && dist > g_fTolerance)
          AdjustScaleAndSepPlaneOnDist(dist, scale, sepPlane, (CMeshGeometry&) cSecond);

        NormScale();
        cSecond.NormScale();
      }

      if (found)
      {        
        if (data.IsNull())
        {
          //not found
          data = new CDCacheItemData;
          cdCache.Add(GetID(), cSecond.GetID(), data);
        }

        data->_sepPlane = sepPlane;
        data->_lastUpdate = simCycle; 
        data->_dist = dist;
        data->_geom1ID = GetID();
        data->_plane1 = plane1;
        data->_plane2 = plane2;
        data->_scale = scale;
      }
      else
      { 
        if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
          // it was just a try geometries should be iqnored.
        {
          data->_lastUpdate = simCycle;
          return FALSE;
        }        
      }
      return !found;
    }
  default:
    Assert(FALSE);
  }
  return FALSE;  
}

BOOL CMeshGeometry::ForceInterFree(IRBGeometry& cSecond, int simCycle)
{    
  if (!_cEnclosingRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return TRUE; // enclosing rectagles does not overlap
  }

  switch (((CGeometry&)cSecond).GetType())
  {
  case EGT_COMPOSE:
    return cSecond.ForceInterFree(*this, simCycle);  
  case EGT_CONVEX:
    {
      //PROFILE_SCOPE(fintfree);
      FindSeparationPlane<CMeshGeometry, MPlane, MEdge> intersect;
      MPlane sepPlane;
      float dist;
      int plane1 = -1, plane2 = -1;
      float scale = 1;

      Ref<CDCacheItemData> data;
      if (cdCache.Get(GetID(), cSecond.GetID(), data))
      {
        if (GetID() == data->_geom1ID)
        {
          plane1 = data->_plane1;
          plane2 = data->_plane2;
        }
        else
        {
          plane2 = data->_plane1;
          plane1 = data->_plane2;
        }

        if (data->_scale > 0)
        {
          if (data->_scale <= IGNORANCE_SCALE)
            return TRUE; 
          scale = data->_scale;
          Scale(data->_scale);
          cSecond.Scale(data->_scale);
        }
      }
      else
      {        
        //not found
        data = new CDCacheItemData;
        cdCache.Add(GetID(), cSecond.GetID(), data);
      }

      BOOL found = intersect(sepPlane, *this,(const CMeshGeometry&) cSecond, dist, plane1, plane2);      
      if (found)  
      {
        if(data->_scale > 0)
        {
          if (dist > g_fTolerance)
            AdjustScaleAndSepPlaneOnDist(dist, scale, sepPlane, (CMeshGeometry&) cSecond);

          NormScale();
          cSecond.NormScale();
        }

        data->_sepPlane = sepPlane;
        data->_lastUpdate = simCycle; 
        data->_dist = dist;
        data->_geom1ID = GetID();
        data->_plane1 = plane1;
        data->_plane2 = plane2;

        return TRUE;
      }

      // later effectove size can be bounding sphere radius etc...
      Vector3 diff = _cEnclosingRectangle.Max() - _cEnclosingRectangle.Min();
      float efectiveSize = max(diff[0], diff[1]);
      efectiveSize = max(efectiveSize, diff[2]);

      while(!found && scale > IGNORANCE_SCALE)
      {
        scale *= 0.90;

        Scale(scale);
        cSecond.Scale(scale);

        found = intersect(sepPlane, *this,(const CMeshGeometry&) cSecond, dist, plane1, plane2);
      }

      if (scale <= IGNORANCE_SCALE)
      {
        //ignore collisions
        data->_scale = scale;
        data->_lastUpdate = simCycle;

        NormScale();
        cSecond.NormScale();

        return FALSE;
      }
      else
      {
        if (dist > g_fTolerance)
          AdjustScaleAndSepPlaneOnDist(dist, scale, sepPlane, (CMeshGeometry&) cSecond);

        data->_sepPlane = sepPlane;
        data->_lastUpdate = simCycle; 
        data->_dist = dist;
        data->_geom1ID = GetID();
        data->_plane1 = plane1;
        data->_plane2 = plane2;
        data->_scale = scale;

        NormScale();
        cSecond.NormScale();

        return TRUE;
      }
    }
  default:
    Assert(FALSE);
  }
  return FALSE;  
}

void FilterPairs(RBContactArray& cFinalContacts, const CDContactArray& cFoundPairs)
{
  if (cFoundPairs.Size() == 0)
    return;

  // Find closest par.
  marina_real fSmallestDist = g_fTolerance * 2;
  Vector3 cSmallestDistNorm;

  for( int i = 0; i < cFoundPairs.Size(); i++)
  {
    if (fSmallestDist > cFoundPairs[i]._dist)
    {
      fSmallestDist = cFoundPairs[i]._dist;
      cSmallestDistNorm = cFoundPairs[i]._norm;
    }
  }

  for( int i = 0; i < cFoundPairs.Size(); i++)
  {
    if (cFoundPairs[i]._norm * cSmallestDistNorm > 0.98)
    {
      // Add to contacts;
      RBContactPoint tContactPoint;

      tContactPoint.pos = cFoundPairs[i]._pos;
      tContactPoint.under =  g_fTolerance  - cFoundPairs[i]._dist;

      tContactPoint.dir[0] = -cSmallestDistNorm;

      // Find pedestal axes
      tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(1,0,0));
      if (tContactPoint.dir[1].Size() < 0.1f) // is new vector correct
        tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(0,1,0));

      tContactPoint.dir[2] = tContactPoint.dir[0].CrossProduct(tContactPoint.dir[1]);

      tContactPoint.dir[1].Normalize();
      tContactPoint.dir[2].Normalize();

      cFinalContacts.Add(tContactPoint);
    }
  }
}

int CMeshGeometry::FindContacts( RBContactArray& cContactPoints, IRBGeometry& cSecond, int simCycle)
{
  CRectangle cEnlargedEnclRectangle(_cEnclosingRectangle);
  cEnlargedEnclRectangle.Enlarge(g_fTolerance);

  if (!cEnlargedEnclRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return 0; // enclosing rectagles doen not overlap
  }

  switch (((CGeometry&)cSecond).GetType())
  {
  case EGT_COMPOSE:
    return cSecond.FindContacts(cContactPoints, *this, simCycle);  
  case EGT_CONVEX:
    {     
      //PROFILE_SCOPE(conv);
      ADD_COUNTER(conv2,1);

      Ref<CDCacheItemData> data;
      MPlane sepPlane(VZero,0);
      float scale = -1;

      if (cdCache.Get(GetID(), cSecond.GetID(), data))
      {    
        if (data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
          return 0; // ignore collision        

        if (data->_lastUpdate == simCycle-1) 
        {
          if (data->_dist > g_fTolerance)
          {
            ADD_COUNTER(nconv1,1);
            return 0;
          }
          else
          {
            sepPlane = data->_sepPlane;
            if (data->_geom1ID != GetID())
            {
              //ASSERT(FALSE);
              sepPlane.SetNormal(-sepPlane.Normal(), -sepPlane.D() /*+ data->_dist*/);
            }

            if (data->_scale > 0)
            {              
              scale = data->_scale;
              Scale(data->_scale);
              cSecond.Scale(data->_scale);
            }
          }
        }
      }

      CDContactArray res;
      ContactConvex<CMeshGeometry, MPlane, MFace, MEdge> cont;
      cont( res, *this, (const CMeshGeometry&) cSecond, g_fTolerance, sepPlane);

      if (scale > 0)
      {
        NormScale();
        cSecond.NormScale();
      }

      if (res.Size() == 0)
      {
        ADD_COUNTER(ncov,1);
      }
      else
      {
        ADD_COUNTER(ycov,1);
      }

      int nContacts = cContactPoints.Size();
      FilterPairs(cContactPoints, res);

      RigidBodyObject * pcBody1 = /*(_ptBody->_static) ? NULL :*/ _ptBody;
      RigidBodyObject * pcBody2 = /*(cSecond.GetBody()->_static) ? NULL : */cSecond.GetBody();

      float fRestitutionCoef = 0.3f;
      float fFrictionCoef = 0.7f;

      for(;nContacts < cContactPoints.Size(); nContacts++)
      {
        cContactPoints[nContacts].geomID1 = GetID();
        cContactPoints[nContacts].geomID2 = cSecond.GetID();
        cContactPoints[nContacts].pcBody1 = pcBody1;
        cContactPoints[nContacts].pcBody2 = pcBody2;
        cContactPoints[nContacts].fRestitutionCoef = fRestitutionCoef;
        cContactPoints[nContacts].fSlidingFrictionCoef = fFrictionCoef;
      }

      return nContacts;

    }
  default:
    ASSERT(FALSE);
  }
  return -1;  
}


void CMeshGeometry::CalculateEnclosingRectangle() 
{
  if (_vertex.Size() == 0)
    return;

  Vector3 minv(_vertex[0]);
  Vector3 maxv(_vertex[0]);

  for(int i = 0; i < _vertex.Size(); i++)
  {
    saturateMin(minv[0], _vertex[i][0]);
    saturateMin(minv[1], _vertex[i][1]);
    saturateMin(minv[2], _vertex[i][2]);

    saturateMax(maxv[0], _vertex[i][0]);
    saturateMax(maxv[1], _vertex[i][1]);
    saturateMax(maxv[2], _vertex[i][2]);
  }
  _cEnclosingRectangle = CRectangle(minv,maxv);
};


void CMeshGeometry::AdjustScaleAndSepPlaneOnDist(float &dist, float& scale, MPlane& SepPlane, CMeshGeometry& cSecond)
{
  return; //TODO: uknoen error
  float distWanted = g_fTolerance * 0.75;
  float centerDist = -SepPlane.Distance(GeomCenter()) + SepPlane.Distance(cSecond.GeomCenter()) - dist;
  ASSERT(centerDist > 0);

  float rescale = 1 +  (dist - distWanted) / centerDist;
  scale *= rescale;

  if (scale > 1)
  {
    rescale /= scale; 
    scale = -1;
  }

  float sepDDiff = (1 - rescale) * SepPlane.Distance(GeomCenter());
  SepPlane.SetD(SepPlane.D() - sepDDiff);

  dist -= (rescale - 1) * centerDist;
}
////////////////////////////////////////////////////////////////////
//

void MFace::CalcPlane(const AutoArray<Vector3>& verPos)
{
  Assert(_n > 2);
  Vector3 normal = (verPos[_vertexes[1]] -  verPos[_vertexes[0]]).CrossProduct(verPos[_vertexes[2]] - verPos[_vertexes[0]]);

  normal.Normalize(); 

  _plane.SetNormal(normal, -normal * verPos[_vertexes[0]]);
}

void MFace::CalcPlaneD(const AutoArray<Vector3>& verPos)
{
  Assert(_n > 2);   
  _plane.SetNormal(_plane.Normal(), -_plane.Normal() * verPos[_vertexes[0]]);
}

void MFace::AddVertex(int vertex)
{
  Assert(_n < MAX_VERTEXES);
  _vertexes[_n++] = vertex;
}

void MEdge::CalcDir(const AutoArray<Vector3>& verPos)
{
  _dir = verPos[_vertexes[1]] -  verPos[_vertexes[0]];  
  _dir.Normalize();
}

void MFace::Check(const AutoArray<Vector3>& verPos)  const
{
  if (_n > 3)
  {
    Vector3 normal1 = (verPos[_vertexes[1]] -  verPos[_vertexes[0]]).CrossProduct(verPos[_vertexes[2]] - verPos[_vertexes[0]]);
    normal1.Normalize();
    Vector3 normal2 = (verPos[_vertexes[2]] -  verPos[_vertexes[0]]).CrossProduct(verPos[_vertexes[3]] - verPos[_vertexes[0]]);
    normal2.Normalize();

    ASSERT(normal1 * normal2 > 0.999);
  }  
}

CDCache<CDCacheItemData> cdCache; 