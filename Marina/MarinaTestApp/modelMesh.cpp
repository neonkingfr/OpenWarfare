#include <El/elementpch.hpp>
#include <d3dx9.h>
#include "modelMesh.h"

CModelMesh::CModelMesh(EModelTypes modelType,
                       int id,
                       Matrix4Par origin,
                       IDirect3DDevice9 *pD3DDevice,
                       D3DCOLOR color):
CModel3D(modelType, id, origin, pD3DDevice, color)
{}

void CModelMesh::AddTriangle(Vector3Val vertex0, Vector3Val vertex1, Vector3Val vertex2)
{
  SModelVertex mv;
  WORD VIndex;
  Vector3 normal = (vertex2 - vertex0).CrossProduct(vertex1 - vertex0);
  normal.Normalize();

  VIndex = _ps->GetNewVertexIndex();

  mv._position = vertex0;
  mv._normal = normal;
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = vertex1;
  mv._normal = normal;
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = vertex2;
  mv._normal = normal;
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);
}