#ifndef _modelString_h_
#define _modelString_h_

#include "model.h"

#define TEXT_SIZE 256

class CModelString : public CModel {
private:
  D3DCOLOR _color;
  ComRef<ID3DXFont> _pD3DXFont;
  char _text[TEXT_SIZE];
public:
  CModelString(
    int id,
    Matrix4Par origin,
    IDirect3DDevice9 *pD3DDevice,
    D3DCOLOR color,
    char *text);
  virtual void Draw(
    IDirect3DDevice9 *pD3DDevice,
    D3DXMATRIX &matView,
    D3DXMATRIX &matProj,
    float backBufferWidth,
    float backBufferHeight);
  virtual int IntersectsWithAbscissa(Vector3Par p, Vector3Par v) {return 0;};
};

#endif