#include <El/elementpch.hpp>
#include <d3dx9.h>
#include "model3D.h"

CModel3D::CModel3D(EModelTypes modelType,
                   int id,
                   Matrix4Par origin,
                   IDirect3DDevice9 *pD3DDevice,
                   D3DCOLOR color) : CModel(modelType, id, origin) {
  _color = color;
  _ps = new CPrimitiveStream(pD3DDevice);
  _ps->Init(sizeof(SModelVertex), D3DFVF_MODEL_VERTEX);
}

void CModel3D::Draw(IDirect3DDevice9 *pD3DDevice,
                    D3DXMATRIX &matView,
                    D3DXMATRIX &matProj,
                    float backBufferWidth,
                    float backBufferHeight) {
  D3DXMATRIX matWorld;
  ConvertMatrix(matWorld, _origin);
  D3DXMATRIX MatTemp;
  D3DXMatrixTranspose(&MatTemp, &D3DXMATRIX(matWorld * matView));

  pD3DDevice->SetVertexShaderConstantF(0,(float *) MatTemp, 4);

  _ps->Draw();
}
