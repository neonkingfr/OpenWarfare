#include <El/elementpch.hpp>
#include "modelBlock.h"

int CModelBlock::FaceIntersectsWithAbscissa(Vector3Par a,
                                            Vector3Par u,
                                            Vector3Par v,
                                            Vector3Par p,
                                            Vector3Par w) {

  float divisor =
    - u.X()*v.Z()*w.Y()
    - v.X()*u.Y()*w.Z()
    + v.X()*u.Z()*w.Y()
    + u.X()*w.Z()*v.Y()
    + w.X()*u.Y()*v.Z()
    - w.X()*u.Z()*v.Y();

  // Face and abscissa are collinear
  if (fabs(divisor) < FLT_MIN) return 0;

  float uu = -(
      v.Z()*a.Y()*w.X()
    + v.Z()*w.Y()*p.X()
    - v.Z()*w.Y()*a.X()
    + a.Z()*v.X()*w.Y()
    - a.Z()*w.X()*v.Y()
    + p.Z()*w.X()*v.Y()
    + w.Z()*a.X()*v.Y()
    + w.Z()*v.X()*p.Y()
    - w.Z()*v.X()*a.Y()
    - p.Z()*v.X()*w.Y()
    - v.Z()*p.Y()*w.X()
    - w.Z()*p.X()*v.Y()) / divisor;
  if ((uu < 0.0f) || (uu > 1.0f)) return 0;

  float vv = (
      u.Y()*p.Z()*w.X()
    - u.Y()*w.Z()*p.X()
    - u.Y()*a.Z()*w.X()
    + u.Y()*w.Z()*a.X()
    - w.Y()*u.Z()*a.X()
    + w.Y()*u.Z()*p.X()
    + w.Z()*u.X()*p.Y()
    - w.Y()*u.X()*p.Z()
    - w.Z()*u.X()*a.Y()
    + a.Y()*u.Z()*w.X()
    + w.Y()*u.X()*a.Z()
    - p.Y()*u.Z()*w.X()) / divisor;
  if ((vv < 0.0f) || (vv > 1.0f)) return 0;


  float ww = (
    - u.X()*v.Z()*a.Y()
    + u.X()*v.Z()*p.Y()
    + v.X()*u.Z()*a.Y()
    - v.X()*u.Y()*a.Z()
    + v.X()*u.Y()*p.Z()
    - v.X()*u.Z()*p.Y()
    + a.X()*u.Y()*v.Z()
    - a.X()*u.Z()*v.Y()
    + u.X()*a.Z()*v.Y()
    - u.X()*p.Z()*v.Y()
    - p.X()*u.Y()*v.Z()
    + p.X()*u.Z()*v.Y()) / divisor;
  if ((ww < 0.0f) || (ww > 1.0f)) return 0;

  return 1;

/*
  float divisor;

  // vv
  divisor = 
    u.Z() * (w.Y() * v.X() - w.X() * v.Y()) + 
    w.Z() * (u.X() * v.Y() - u.Y() * v.X()) + 
    v.Z() * (u.Y() * w.X() - u.X() * w.Y());

  if (fabs(divisor) < FLT_MIN) return 0;

  float Ax_M_Px = a.X() - p.X(); // A-J
  float Ay_M_Py = a.Y() - p.Y(); // B-K
  float Az_M_Pz = a.Z() - p.Z(); // C-L

  float vv = 
    (u.Z() * (w.X() * Ay_M_Py - w.Y() * Ax_M_Px) +
     u.Y() * (w.Z() * Ax_M_Px - w.X() * Az_M_Pz) +
     u.X() * (w.Y() * Az_M_Pz - w.Z() * Ay_M_Py)) / divisor;
  if ((vv < 0.0f) || (vv > 1.0f)) return 0;

  // uu
  divisor = (w.X() * u.Y() - w.Y() * u.X());
  if (fabs(divisor) < FLT_MIN) return 0;
  float uu = 
    (w.X() * (p.Y() - a.Y() - v.Y() * vv) +
     w.Y() * (a.X() - p.X() + v.X() * vv)) / divisor;
  if ((uu < 0.0f) || (uu > 1.0f)) return 0;

  // ww
  divisor = w.X();
  if (fabs(divisor) < FLT_MIN) return 0;
  float ww = 
    (a.X() - p.X() + u.X() * uu + v.X() * vv) / divisor;
  if ((ww < 0.0f) || (ww > 1.0f)) return 0;

  return 1;
*/
}


CModelBlock::CModelBlock(IDirect3DDevice9 *pD3DDevice,
                         int id,
                         Matrix4Par origin,
                         D3DCOLOR color,
                         float sizeX,
                         float sizeY,
                         float sizeZ) : CModel3D(modelType_Cube, id, origin, pD3DDevice, color) {

  _sizeX = sizeX;
  _sizeY = sizeY;
  _sizeZ = sizeZ;

  // Create and register texture
  HRESULT hr = D3DXCreateTextureFromFile(pD3DDevice, "brick.bmp", _pTexture.Init());
  if (SUCCEEDED(hr)) {
    _pTexture->AddRef();
    _ps->RegisterTextures(0, _pTexture, RStringB("brick.bmp"), NULL, RStringB(""));
  }

  float halfSizeX = sizeX * 0.5f;
  float halfSizeY = sizeY * 0.5f;
  float halfSizeZ = sizeZ * 0.5f;

  SModelVertex mv;
  WORD VIndex;

  // Back side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSizeX, halfSizeY, halfSizeZ);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSizeX, -halfSizeY, halfSizeZ);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, -halfSizeY, halfSizeZ);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, halfSizeY, halfSizeZ);
  mv._normal = Vector3(0, 0, 1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 3);
  _ps->AddIndex(0, VIndex + 2);

  // Front side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSizeX, halfSizeY, -halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSizeX, -halfSizeY, -halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, -halfSizeY, -halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, halfSizeY, -halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 3);

  // Right side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSizeX, halfSizeY, halfSizeZ);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSizeX, -halfSizeY, halfSizeZ);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSizeX, -halfSizeY, -halfSizeZ);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSizeX, halfSizeY, -halfSizeZ);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 3);

  // Left side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(-halfSizeX, halfSizeY, halfSizeZ);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, -halfSizeY, halfSizeZ);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, -halfSizeY, -halfSizeZ);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, halfSizeY, -halfSizeZ);
  mv._normal = Vector3(1, 0, 0);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 3);
  _ps->AddIndex(0, VIndex + 2);

  // Top side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSizeX, halfSizeY, halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSizeX, halfSizeY, -halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, halfSizeY, -halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, halfSizeY, halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 3);

  // Bottom side

  VIndex = _ps->GetNewVertexIndex();

  mv._position = Vector3(halfSizeX, -halfSizeY, halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(halfSizeX, -halfSizeY, -halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 0.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, -halfSizeY, -halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 1.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  mv._position = Vector3(-halfSizeX, -halfSizeY, halfSizeZ);
  mv._normal = Vector3(0, 0, -1);
  mv._diffuse = _color;
  mv._u0 = 0.0f;
  mv._v0 = 1.0f;
  _ps->AddVertex(&mv);

  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 2);
  _ps->AddIndex(0, VIndex + 1);
  _ps->AddIndex(0, VIndex);
  _ps->AddIndex(0, VIndex + 3);
  _ps->AddIndex(0, VIndex + 2);

  _ps->Prepare();
}

int CModelBlock::IntersectsWithAbscissa(Vector3Par p, Vector3Par w) {

  float halfSizeX = _sizeX * 0.5f;
  float halfSizeY = _sizeY * 0.5f;
  float halfSizeZ = _sizeZ * 0.5f;

  Vector3 a;

  // +X
  a = _origin.Position() + _origin.DirectionAside() * halfSizeX + _origin.DirectionUp() * halfSizeY + _origin.Direction() * halfSizeZ;
  if (FaceIntersectsWithAbscissa(a, -_origin.DirectionUp() * _sizeY, -_origin.Direction() * _sizeZ, p, w)) return 1;
  
  // -X
  a = _origin.Position() - _origin.DirectionAside() * halfSizeX + _origin.DirectionUp() * halfSizeY + _origin.Direction() * halfSizeZ;
  if (FaceIntersectsWithAbscissa(a, -_origin.DirectionUp() * _sizeY, -_origin.Direction() * _sizeZ, p, w)) return 1;

  // +Y
  a = _origin.Position() + _origin.DirectionAside() * halfSizeX + _origin.DirectionUp() * halfSizeY + _origin.Direction() * halfSizeZ;
  if (FaceIntersectsWithAbscissa(a, -_origin.DirectionAside() * _sizeX, -_origin.Direction() * _sizeZ, p, w)) return 1;

  // -Y
  a = _origin.Position() + _origin.DirectionAside() * halfSizeX - _origin.DirectionUp() * halfSizeY + _origin.Direction() * halfSizeZ;
  if (FaceIntersectsWithAbscissa(a, -_origin.DirectionAside() * _sizeX, -_origin.Direction() * _sizeZ, p, w)) return 1;

  // +Z
  a = _origin.Position() + _origin.DirectionAside() * halfSizeX + _origin.DirectionUp() * halfSizeY + _origin.Direction() * halfSizeZ;
  if (FaceIntersectsWithAbscissa(a, -_origin.DirectionAside() * _sizeX, -_origin.DirectionUp() * _sizeY, p, w)) return 1;

  // -Z
  a = _origin.Position() + _origin.DirectionAside() * halfSizeX + _origin.DirectionUp() * halfSizeY -_origin.Direction() * halfSizeZ;
  if (FaceIntersectsWithAbscissa(a, -_origin.DirectionAside() * _sizeX, -_origin.DirectionUp() * _sizeY, p, w)) return 1;

  return 0;
}

Vector3 CModelBlock::GetDimension() {
  return Vector3(_sizeX, _sizeY, _sizeZ);
}
