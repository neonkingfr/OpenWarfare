#ifndef _modelArrow_h_
#define _modelArrow_h_

#include "model3D.h"

class CModelArrow : public CModel3D 
{
private:
  ComRef<IDirect3DTexture9> _pTexture;
public:
  CModelArrow(
    IDirect3DDevice9 *pD3DDevice,
    int id,
    Matrix4Par origin,
    D3DCOLOR color,
    float size);

  virtual int IntersectsWithAbscissa(Vector3Par p, Vector3Par v) {return 0;};
};

#endif