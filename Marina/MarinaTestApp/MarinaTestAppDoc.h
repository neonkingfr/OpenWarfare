// MarinaTestAppDoc.h : interface of the CMarinaTestAppDoc class

#pragma once

#include <d3d9.h>
#include "modelArray.h"
#include <marina.hpp>
#include "MarinaTestAppSimulate.h"


class CMarinaTestAppDoc : public CDocument
{
protected:
  CModelArray m_models;  
  CModelArray m_arrows; 
  CMarinaTestAppSimulation m_cSimulation;

  BOOL m_modePerStep;

protected: // create from serialization only
	CMarinaTestAppDoc();
	DECLARE_DYNCREATE(CMarinaTestAppDoc)


public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

  virtual ~CMarinaTestAppDoc();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

  CModelArray& GetModels() {return m_models;};
  CModelArray& GetArrows() {return m_arrows;};
  void Simulate(float);
  

  // Arrows;
  void PrepareModelsForDraw(LPDIRECT3DDEVICE9 pDevice);  
  void AddArrow(Vector3Val pos, Vector3Val dir, float size, DWORD color);
  void ResetArrows() {UpdateAllViews(NULL); m_arrows.ReleaseModels();};

  void CreateBodyAtPos(CString modelName, const Vector3& pos, const CQuaternion& orient
    , const Vector3& speed = VZero, const Vector3& angularSpeed = VZero, bool staticBody = false );


protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnTestCube();
  afx_msg void OnTestMove();
  afx_msg void OnBodyCreate1();
  afx_msg void OnBodyCreate2();
  afx_msg void OnBodyCreate();
  virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
  afx_msg void OnAddImpulse();
  afx_msg void OnBodyCreate3();
  afx_msg void OnBodyCreate4();
  afx_msg void OnBodyCreate5();
  afx_msg void OnBodyCreate6();
  afx_msg void OnBodyCreate7();
  afx_msg void OnBodyCreate8();
  afx_msg void OnBodyCreate9();
  afx_msg void OnModePerStep();
  afx_msg void OnNextStep();
};



