#include <El/elementpch.hpp>
#include "modelString.h"

CModelString::CModelString(int id,
                           Matrix4Par origin,
                           IDirect3DDevice9 *pD3DDevice,
                           D3DCOLOR color,
                           char *text) : CModel(modelType_Cube, id, origin) {
  
  // Create font
  HRESULT hr;
  if( FAILED( hr = D3DXCreateFont( pD3DDevice, 20,20,FW_BOLD, 3, FALSE, ANSI_CHARSET,OUT_DEFAULT_PRECIS,ANTIALIASED_QUALITY, 
     FF_DONTCARE,"Arial", _pD3DXFont.Init()) ) )
    LogF("Error while creating the d3d font.");;
  

  // Save the color
  _color = color;

  // Save the text
  strcpy(_text, text);
}

void CModelString::Draw(IDirect3DDevice9 *pD3DDevice,
                        D3DXMATRIX &matView,
                        D3DXMATRIX &matProj,
                        float backBufferWidth,
                        float backBufferHeight) {

  D3DXMATRIX matWorld;
  D3DXMATRIX final;
  ConvertMatrix(matWorld, _origin);
  final = matWorld * matView * matProj;

  //D3DCOLOR fontColor        = D3DCOLOR_ARGB(255,255,255,255);
  RECT rct;
  ZeroMemory(&rct, sizeof(rct));       

  float halfBBW = backBufferWidth * 0.5f;
  float halfBBH = backBufferHeight * 0.5f;

 /* _pD3DXFont->Begin();
  rct.left   = halfBBW + final._41 * halfBBW / final._44;
  rct.right  = halfBBW + final._41 * halfBBW / final._44 + 1000;
  rct.top = halfBBH - final._42 * halfBBH / final._44;
  rct.bottom = halfBBH - final._42 * halfBBH / final._44 + 20;    
  _pD3DXFont->DrawText(_text, -1, &rct, 0, _color);
  _pD3DXFont->End();
*/

}
