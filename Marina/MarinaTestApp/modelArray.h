#ifndef _modelArray_h_
#define _modelArray_h_

#include <Es/Containers/array.hpp>
//#include "radiosity/radiosity.h"
#include "model.h"

//class CRadiosity;

template Ref<CModel>;

//! Array of models
class CModelArray {
private:
  //! Data
  RefArray<CModel> _array;
  ComRef<IDirect3DTexture9> _pWhite;
  ComRef<IDirect3DTexture9> _pTexture;
  Ref<CPrimitiveStream> _ps;
  //CRadiosity _radiosity;
public:
  CModelArray();
  void ReleaseModels() {_array.Clear();};
  void CreateDefaultScene(IDirect3DDevice9 *pD3DDevice);
  void CreateModel(IDirect3DDevice9 *pD3DDevice, SMessage *message);
  void AddModel(CModel * model) {_array.Add(model);};
  void DeleteModel(SMessage *message);
  //void DeleteModel(int ID);
  void MoveModel(SMessage *message);
  bool MoveModel(int ID, Matrix4Par newOrigin);

  void Draw(
    IDirect3DDevice9 *pD3DDevice,
    D3DXMATRIX &matView,
    D3DXMATRIX &matProj,
    float backBufferWidth,
    float backBufferHeight);
  void DrawScene(
    IDirect3DDevice9 *pD3DDevice,
    D3DXMATRIX &matView,
    D3DXMATRIX &matProj,
    float backBufferWidth,
    float backBufferHeight,
    Vector3Par eyePosition);
  int IntersectsWithAbscissa(Vector3Par p, Vector3Par v);
};

#endif