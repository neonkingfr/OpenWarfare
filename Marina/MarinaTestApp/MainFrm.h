
// MainFrm.h : interface of the CMainFrame class
//
#pragma once

#define SAMPLE_BUFFER_SIZE  16
// Struct to store the current input state
struct UserInput
{
  BYTE diks[256];   // DirectInput keyboard state buffer 
  DIDEVICEOBJECTDATA mouseData[SAMPLE_BUFFER_SIZE];

  DWORD mouseDataFilled;

  // TODO: change as needed
  BOOL bRotateUp;
  BOOL bRotateDown;
  BOOL bRotateLeft;
  BOOL bRotateRight;

  BOOL bMoveForward;
  BOOL bMoveBackward;

  BOOL bMoveUp;
  BOOL bMoveDown;

  BOOL bWireframe;
  BOOL bTextures;

  BOOL bRunTimeOff; 
};




class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
  
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
public:
  D3DPRESENT_PARAMETERS m_d3dpp;         // Parameters for CreateDevice/Reset
  HWND              m_hWnd;              // The main app window
  HWND              m_hWndFocus;         // The D3D focus window (usually same as m_hWnd)
  ComRef<IDirect3D9>       m_pD3D;              // The main D3D object
  ComRef<IDirect3DDevice9>  m_pd3dDevice;        // The D3D rendering device
  D3DSURFACE_DESC   m_d3dsdBackBuffer;   // Surface desc of the backbuffer

  ComRef<IDirect3DVertexShader9>   m_VSBranch;
  ComRef<IDirect3DPixelShader9>   m_PSBranch;
  ComRef<ID3DXFont>              m_pD3DXFont;            // D3DX font   

  // DirectInput
  ComRef<IDirectInput8>  m_pDI;
  ComRef<IDirectInputDevice8> m_pKeyb;
  ComRef<IDirectInputDevice8> m_pMouse;

  UserInput m_UserInput;

  HRESULT DisplayErrorMsg( HRESULT hr, DWORD dwType );
  HRESULT InitDeviceObjects();
  HRESULT RestoreDeviceObjects();
  HRESULT UpdateInput();
  void OnIdle();

public:
  void OnViewCreated(CWnd * pView);
  LPDIRECT3DDEVICE9 GetD3DDevice() const {return m_pd3dDevice;};


// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnRunTimeMode();
};


