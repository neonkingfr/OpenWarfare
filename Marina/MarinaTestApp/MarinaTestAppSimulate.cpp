#include "stdafx.h"
#include "MarinaTestAppSimulate.h"
#include "MarinaTestAppDoc.h"

#include <cd/IntersectConvex.hpp>
#include <cd/ContactConvex.hpp>
#include "MarinaGeometry.h"

#include "MarinaTestAppBody.h"

bool CMarinaTestAppSimulation::BreakBodies(SeparateGeomArray & sep)
{
  bool ret = false;
  for(int i = 0; i < sep.Size(); i++)
  {
    CGeometry * geom = (CGeometry *) (sep[i].obj->_geometry);
    if (geom->GetType() != EGT_COMPOSE)
      continue;

    CComposeGeometry * geomComp = (CComposeGeometry *) geom;
    if (geomComp->NumberOfChilds() < 2)
      continue;

    CGeometry * geomToSeparate = geomComp->DecomposeChild(sep[i].geomID);
    if (geomToSeparate != NULL)
    {
      ret = true;
      RigidBodyObject * tBody = new RigidBodyObject();
      tBody->_group = NULL;
      MarinaTestAppBody * body = new MarinaTestAppBody(); 
      tBody->_physBody = body;       
      tBody->_static = FALSE;
    
      CMass mass = geomToSeparate->GetMass();

      mass.SetRelPoint(mass.MassCenter());
      geomToSeparate->SetRelOrigin(-mass.MassCenter(), CQuaternion(1,0,0,0));
      //geomToSeparate->TranslateRelOrigin(-mass.MassCenter());

      tBody->_physBody->SetPos(geomToSeparate->Pos() 
        + geomToSeparate->Orientation().GetMatrix() * mass.MassCenter());
      tBody->_physBody->SetOrientation(geomToSeparate->Orientation());

      tBody->_geometry = geomToSeparate;            

      body->SetInvMass(1.0f / mass.Mass());
      body->SetInertiaTensor(mass.InertiaTensor());
      body->SetInvInertiaTensor(mass.InertiaTensor().InverseGeneral());
      //body->SetRestitutionCoef(RESTITUTION);
      //body->SetSlidingFrictionCoef(FRICTION);

      tBody->_physBody->SetVelocity(sep[i].obj->_physBody->SpeedAtPoint(body->GetPos()));
      tBody->_physBody->SetAngularVelocity(sep[i].obj->_physBody->GetAngularVelocity());
      tBody->_physBody->ActualizeTensors();

      tBody->_physBody->AngularMomentumFromAngularVelocity();

      AddBody(tBody);
      tBody->_geometry->SetBody(tBody);

      // mass of the old body changed
      RigidBodyObject * oldBody = sep[i].obj;
      CMass massOld = ((CGeometry *) oldBody->_geometry)->GetMass();

      massOld.SetRelPoint(massOld.MassCenter());
      oldBody->_geometry->SetRelOrigin(-massOld.MassCenter(), CQuaternion(1,0,0,0));
      //geomToSeparate->TranslateRelOrigin(-mass.MassCenter());

      oldBody->_physBody->SetPos(oldBody->_geometry->Pos() 
        + oldBody->_geometry->Orientation().GetMatrix() * massOld.MassCenter());
      oldBody->_physBody->SetOrientation(oldBody->_geometry->Orientation());
          
      ((MarinaTestAppBody *) oldBody->_physBody)->SetInvMass(1.0f / massOld.Mass());
      ((MarinaTestAppBody *) oldBody->_physBody)->SetInertiaTensor(massOld.InertiaTensor());
      ((MarinaTestAppBody *) oldBody->_physBody)->SetInvInertiaTensor(massOld.InertiaTensor().InverseGeneral());
      //body->SetRestitutionCoef(RESTITUTION);

      oldBody->_physBody->ActualizeTensors();
      oldBody->_physBody->AngularMomentumFromAngularVelocity();
    }
  }

  return ret;
}

bool CMarinaTestAppSimulation::CanBreakBodies(SeparateGeomArray & sep)
{ 
  for(int i = 0; i < sep.Size(); i++)
  {
    CGeometry * geom = (CGeometry *) (sep[i].obj->_geometry);
    if (geom->GetType() != EGT_COMPOSE)
      continue;

    CComposeGeometry * geomComp = (CComposeGeometry *) geom;
    if (geomComp->NumberOfChilds() < 2)
      continue;  

    return true;
  }

  return false;
}

void CMarinaTestAppSimulation::AddArrow(Vector3Val pos,Vector3Val dir, float size, DWORD color) 
{_doc.AddArrow(pos, dir, size, color);};

void CMarinaTestAppSimulation::ResetArrows() 
{_doc.ResetArrows();};