#ifdef _MSC_VER
#pragma once
#endif

#ifndef __BODYGROUP_H__
#define __BODYGROUP_H__
#include "core/MatrixA.hpp"


typedef AutoArray<AutoArray<int> > AutoIntArray2D;
typedef AutoArray<RigidBodyObject *> RBpBodyArray;
typedef Matrix<marina_real> M_MATRIX;


class BodyGroup
{
protected:
  RBpBodyArray _cBodies;  
  RBContactArray _cContacts;
  AutoIntArray2D _contactsOfBodies;

  //static M_MATRIX _cMatrixA;
  BOOL _bMatrixReady;

  BOOL _bFrozen;
  BOOL _bTemp;

  marina_real _normalizationMass; 

  void CalcNormMass();
  void CalculateMatrix();

public:
  BodyGroup(): _bMatrixReady(FALSE), _bFrozen(FALSE) {};
  ~BodyGroup() {};

  //void AddBody(RigidBodyObject * ptBody) {ptBody->_group = this; _cBodies.Add(ptBody);_bMatrixReady = FALSE;_bFrozen = FALSE;};
  void AddContact(const RBContactPoint& tContact);

  void JoinGroup(BodyGroup & cBodyGroup);

  void Froze() {_bFrozen = TRUE;};
  void Melt() {_bFrozen = FALSE;}; 
  BOOL IsFrozen() const {/*ASSERT(_bFrozen == TRUE || _bFrozen == FALSE);*/ return _bFrozen;};

  const M_MATRIX& GetMatrix();

  BOOL& Temp() {return _bTemp;};
  RBContactArray& Contacts() {return _cContacts;};
  const RBContactArray& Contacts() const {return _cContacts;};   

  AutoIntArray2D& ContactsOfBodies() {return _contactsOfBodies;};
  const AutoIntArray2D& ContactsOfBodies() const {return _contactsOfBodies;}; 

  RBpBodyArray& Bodies() {return _cBodies;};
  const RBpBodyArray& Bodies() const {return _cBodies;};  

  marina_real NormMass() const {return _normalizationMass;}
};

#endif //__BODYGROUP_H__