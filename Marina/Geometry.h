#ifndef __GEOMETRY_H__
#define __GEOMETRY_H__


//#include <ode/ode.h>
//#include <drawstuff/drawstuff.h>
#include "Quaternion.h"
#include "Math3D.h"
//#include "HordubalClientDll.h"
#include "RV/Tree/ObjShapeMat/objlod.hpp"
#include "MyMass.h"
#include "CD/CDCache.hpp"


class CRectangle
{
protected:
    Vector3 _cMax;
    Vector3 _cMin;

public:
    CRectangle() {};
    CRectangle(const Vector3& cMin, const Vector3& cMax) {_cMax = cMax; _cMin = cMin;};
    CRectangle(const CRectangle& cRect) {_cMax = cRect._cMax; _cMin = cRect._cMin;};

    BOOL IsIntersection(const CRectangle& cSecond) const;
    void Enlarge(marina_real fSize);

    const Vector3& Min() const {return _cMin;};
    const Vector3& Max() const {return _cMax;};

    CRectangle operator+(const CRectangle& cSecond) const;
    const CRectangle& operator+=(const CRectangle& cSecond);
    const CRectangle& operator=(const CRectangle& cSecond) {_cMax = cSecond._cMax; _cMin = cSecond._cMin; return *this;};

    // Adding vector means to minimaly enlarge rectangle so that it contains vector's end point.
    CRectangle operator+(const Vector3& cSecond) const;
    const CRectangle& operator+=(const Vector3& cSecond);

    // The rectangle with zerosize is created.
    const CRectangle& operator=(const Vector3& cSecond) {_cMax = cSecond; _cMin = cSecond; return *this;};
};

typedef enum 
{
    EGT_COMPOSE = 0,
    EGT_BOX = 1,
    EGT_PLANE = 2,
    EGT_CONVEX = 3
} EGEOMTYPE;

class CGeometry
{
protected:
    const EGEOMTYPE _eType;
    int _ID; // unique ID of the body.

    CRectangle _cEnclosingRectangle;
    Vector3 _cPos;
    CQuaternion _cOrientation;

    Vector3 _cRelPos;
    CQuaternion _cRelOrientation;

    M_TBODY * _ptBody; //corresponding body.

    int _iVladasID;
    CHordubalClientDll * _pcDrawingClient;

    BOOL _bUseDrawstuff;

    unsigned int _iBodyColor;

public:
    CGeometry(const EGEOMTYPE eType, int ID) : _eType(eType), _ID(ID),  _ptBody(NULL), _iVladasID(0), _bUseDrawstuff(FALSE), _pcDrawingClient(NULL),
    _cPos(VZero), _cRelPos(VZero) {};

    EGEOMTYPE GetType() const {return _eType;};
    int GetID() const {return _ID;};
    M_TBODY * GetBody() const {return _ptBody;};

    //int GetVladasID() const {return _iVladasID;};
    
    virtual void SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation);
    void SetRelOrigin(const Vector3& cRelPos, const CQuaternion& cRelQuaternion);
    void TranslateRelOrigin(const Vector3& cRelPos);

    //void SetVladasID(int iVladasID) {_iVladasID = iVladasID;};
    

    const CRectangle& GetEnclosingRectangle() const {return _cEnclosingRectangle;};

    virtual void CalculateEnclosingRectangle() = 0;

    virtual BOOL IsIntersection( CGeometry& cSecond, int simCycle)  = 0;
    virtual BOOL ForceInterFree(CGeometry& cSecond, int simCycle) = 0; // return FALSE if bodies must be ignored.
    virtual void Scale(float scale) = 0;
    virtual void NormScale() = 0;

    virtual int FindContacts( M_ContactArray& cContactPoints, CGeometry& cSecond, int simCycle) = 0;    

    virtual void SetBody(M_TBODY * ptBody) {_ptBody = ptBody;};

    virtual void Draw() const = 0;

    virtual void CreateDrawClients(CHordubalClientDll * pcDrawingClient, unsigned int& iVladasID, BOOL bUseDrawstuff, int iBodyColor) = 0;
    
};

class CBoxGeometry : public CGeometry
{
protected:
    Vector3 _cBoxSizes;

public:
    CBoxGeometry(int ID) : CGeometry(EGT_BOX,ID) {};
    CBoxGeometry( const Vector3& cBoxSizes, int ID) : CGeometry(EGT_BOX, ID), _cBoxSizes(cBoxSizes) {};

    void SetBoxSizes(const Vector3& cBoxSizes) {_cBoxSizes = cBoxSizes;};
    Vector3 GetBoxSizes() const {return _cBoxSizes;};

    virtual void CalculateEnclosingRectangle();
    virtual BOOL IsIntersection( CGeometry& cSecond, int simCycle) ;
    virtual BOOL ForceInterFree(CGeometry& cSecond, int simCycle){return TRUE;};
    virtual void Scale(float scale) {};
    virtual void NormScale() {};

    virtual int FindContacts( M_ContactArray& cContactPoints,  CGeometry& cSecond, int simCycle);   

    virtual void Draw() const;
    virtual void CreateDrawClients(CHordubalClientDll * pcDrawingClient, unsigned int& iVladasID, BOOL bUseDrawstuff, int iBodyColor);
};

TypeIsSimple(CGeometry *)
typedef AutoArray<CGeometry *> M_CGEOMARRAY;

class CComposeGeometry : public CGeometry
{
protected:
    M_CGEOMARRAY _cGeomArray;

public:
    CComposeGeometry(int ID) : CGeometry(EGT_COMPOSE, ID) {};
    ~CComposeGeometry();    
 
    void LoadFromP3D(LPCSTR path, CMass& mass);

    void Add(CGeometry& cGeom) {_cGeomArray.Add(&cGeom);}; // Pointers will be cleaned in destructor
    
    void TranslateChilds(const Vector3& cRelPos);

    virtual void SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation);
    virtual void CalculateEnclosingRectangle();
    virtual BOOL IsIntersection( CGeometry& cSecond, int simCycle);
    virtual BOOL ForceInterFree( CGeometry& cSecond, int simCycle);
    virtual void Scale(float scale) {};
    virtual void NormScale() {};
    virtual int FindContacts( M_ContactArray& cContactPoints,  CGeometry& cSecond, int simCycle) ;

    virtual void SetBody(M_TBODY * ptBody);

    virtual void Draw() const;
    virtual void CreateDrawClients(CHordubalClientDll * pcDrawingClient,  unsigned int& iVladasID, BOOL bUseDrawstuff, int iBodyColor);
};

#include <Poseidon/lib/plane.hpp>
#define MAX_VERTEXES 4

class MPlane : public Plane
{
public:
  MPlane() : Plane() {};
  MPlane( Vector3Par normal, float d ) : Plane( normal, d ) {};
  MPlane( Vector3Par normal, Vector3Par point ) : Plane(normal,point) {};
  MPlane( const MPlane& plane) : Plane(plane) {};
};

typedef const MPlane& MPlaneVal;

class MFace
{
protected:
  int _n;
  int _vertexes[MAX_VERTEXES];
  MPlane _plane;

public:
  MFace(): _n(0) {};

  void CalcPlane(const AutoArray<Vector3>& vertexes);
  void CalcPlaneD(const AutoArray<Vector3>& vertexes);
  operator const MPlane& () const {return _plane;};

  int N() const {return _n;};
  int operator[](int i) const {Assert(i < MAX_VERTEXES); return _vertexes[i];};
  
  void AddVertex(int vertex) ;

  void Check(const AutoArray<Vector3>& verPos) const;

};
typedef const MFace& MFaceVal;
TypeIsBinary(MFace);

class MEdge
{
protected:
  int _vertexes[2];
  int _faces[2];
  Vector3 _dir;

public: 
  MEdge(int vertex1, int vertex2) : _dir(VZero) {
    _vertexes[0] = vertex1;_vertexes[1] = vertex2;_faces[0] = -1;_faces[1] = -1;};

  void CalcDir(const AutoArray<Vector3>& vertexes);

  int operator[](int i) const {Assert(i < 2); return _vertexes[i];};
  operator const Vector3& () const {return _dir;};

  int GetFace(int i) const {return _faces[i];};
  int& SetFace(int i) {return _faces[i];};
};

typedef const MEdge& MEdgeVal; 
TypeIsSimple(MEdge);

class CMeshGeometry : public CGeometry
{
protected:
  AutoArray<Vector3> _vertexOrig;
  AutoArray<Vector3> _vertex;
  AutoArray<int> _triangle;
  AutoArray<MFace> _face;
  AutoArray<MEdge> _edge; 

  // support for faster NormScale
  bool _validNormScale;
  AutoArray<Vector3> _vertexNormScale;
  

  Vector3 _geomCenter;
  Vector3 _geomCenterOrig;

  char _name[256];

  void FindEdges();
  void CalcDirs();
  void CalcGeomCenterOrig();
  void UpdateOnPos();

  void AdjustScaleAndSepPlaneOnDist(float &dist, float& scale, MPlane& SepPlane, CMeshGeometry& cSecond);

 

public:
  CMeshGeometry(int ID) ;// : CGeometry(EGT_CONVEX) {};
  ~CMeshGeometry() {};  

  void LoadFromP3D(LPCSTR path, CMass& mass);
  bool LoadFromP3DSel(const ObjectData& obj, int selIndex);
  
  virtual void SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation);
  virtual void CalculateEnclosingRectangle();
  virtual BOOL IsIntersection(CGeometry& cSecond, int simCycle);
  virtual BOOL ForceInterFree(CGeometry& cSecond, int simCycle);
  virtual int FindContacts( M_ContactArray& cContactPoints,  CGeometry& cSecond, int simCycle) ;

  virtual void Scale(float scale);
  virtual void NormScale();
  
  virtual void Draw() const;
  virtual void CreateDrawClients(CHordubalClientDll * pcDrawingClient, unsigned int& iVladasID, BOOL bUseDrawstuff, int iBodyColor);

  int NFaces() const {return _face.Size();};
  int NPoints() const {return _vertex.Size();};
  int NEdges() const {return _edge.Size();};
  Vector3Val Pos(int i) const {return _vertex[i];}; 
  MPlaneVal GetPlane(int i) const {return _face[i];};
  MFaceVal GetFace(int i) const {return _face[i];};
  MEdgeVal GetEdge(int i) const {return _edge[i];};
 
  Vector3Val GeomCenter() const {return _geomCenter;};

   void CheckFaces() const;
};

class CDCacheItemData : public RefCount
{
public:
  MPlane _sepPlane;
  float _dist;
  int _geom1ID;
  // information about where was plane found
  int _plane1; // if both are positive there are edge IDs otherwise. There plane IDs.
  int _plane2;  
  // scale ... how much should be bodies smaller. valid only if it is positive.
  float _scale; 

  int _lastUpdate; 

  CDCacheItemData() : _dist(-1), _geom1ID(-1), _plane1(-1), _plane2(-1),
    _scale(-1), _lastUpdate(-1) {};
};

extern CDCache<CDCacheItemData> cdCache; 

extern int LastUsedID;

#endif //__GEOMETRY_H__