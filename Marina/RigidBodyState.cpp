#ifndef _MARINA_PROJECT 
#include <Poseidon/lib/wpch.hpp>
#else
#define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES

#include "RigidBodyState.h"

void IRigidBodyState::SetOrientationMassCenter(const QuaternionRB& orientation)
{
  Vector3 massCenter = GetMassCenter();
  SetOrientation(orientation);
  SetMassCenter(massCenter);
}

Vector3 IRigidBodyState::CalculateTorgue(const Vector3& cForce, const Vector3& cPoint) const
{
	return (cPoint - GetMassCenter()).CrossProduct(cForce);
}

void IRigidBodyState::ApplyImpulseAtPoint(const Vector3& cImpulse,
										  const Vector3& cImpulsePoint) 
{
	if (GetInvMass() == 0)
		return;
		
	SetVelocity(GetVelocity() + cImpulse * GetInvMass());

	SetAngularMomentum(GetAngularMomentum() - cImpulse.CrossProduct(cImpulsePoint - GetMassCenter()));
	SetAngularVelocity(GetActualInvInertiaTensor() * GetAngularMomentum());
}

Vector3 IRigidBodyState::SpeedAtPoint(const Vector3& cPoint) const
{
/*	Log("SpeedAtPoint: _cMomentum(%f,%f), fInvMass(%f), cPoint(%f,%f), cPod(%f,%f), fL(%f), fInvInertia(%f,%f)\n",
		_cMomentum.Getx(), _cMomentum.Gety(), fInvMass, cPoint.Getx(), cPoint.Gety(),
		GetPos().Getx(), GetPos().Gety(), _fL, fInvInertiaTensor);*/
	return GetVelocity() - ((cPoint - GetMassCenter()).CrossProduct(GetAngularVelocity()));
}

Vector3 IRigidBodyState::AccelerationAtPoint(const Vector3& cForceBank, const Vector3& cTorgueBank, const Vector3& cPoint, TRIBOOL bPlus) const
{		
	Vector3 cAccel = cForceBank * GetInvMass() +  // Force
		(GetActualInvInertiaTensor() * (cTorgueBank - GetAngularVelocity().CrossProduct(GetAngularMomentum()))).CrossProduct(cPoint - GetMassCenter()); // Torgue
    
	switch (bPlus)
	{
	case TB_TRUE:
		cAccel += ((cPoint - GetMassCenter()).CrossProduct(GetAngularVelocity())).CrossProduct(GetAngularVelocity()); // dostredive acceleration
		break;
	case TB_FALSE:
		cAccel += - ((cPoint - GetMassCenter()).CrossProduct(GetAngularVelocity())).CrossProduct(GetAngularVelocity()); // dostredive acceleration
		break;
	default:
		;
	}

	return cAccel;
}

Vector3 IRigidBodyState::DiffAccelerationAtPointOnForceAndTorque( const Vector3& cAccelPoint, 
																   const Vector3& cForce, 
																   const Vector3& cTorque) const
{
	Vector3 cAccel = cForce * GetInvMass() + (GetActualInvInertiaTensor() * cTorque).CrossProduct(cAccelPoint - GetMassCenter());
	return cAccel;
}

Vector3 IRigidBodyState::DiffSpeedAtPointOnImpulseAndTorque( const Vector3& cSpeedPoint, 
															  const Vector3& cImpulse, 
															  const Vector3& cImpulseTorque) const
{	
	return cImpulse * GetInvMass() + (GetActualInvInertiaTensor() * cImpulseTorque).CrossProduct(cSpeedPoint - GetMassCenter());	
}

void IRigidBodyState::DiffAccelerationAtPointOnForceAtPoint(Vector3& cAccel, 
															const Vector3& cAccelPoint, 
															const Vector3& cForce, 
															const Vector3& cForcePoint) const
{
	cAccel = cForce * GetInvMass() + (GetActualInvInertiaTensor() * ((cForcePoint - GetMassCenter()).CrossProduct(cForce) )).CrossProduct(cAccelPoint - GetMassCenter());
}

void IRigidBodyState::ActualizeTensors()
{
  Matrix3 rot = GetOrientation().GetMatrix();
 	//Matrix3 cTest = rot * rot.InverseRotation();
   
  Matrix3 rotTrans = rot.InverseRotation();

  //SetActualInertiaTensor( rot * (GetInertiaTensor() * rotTrans));
	//CMatrix3DColumn cRotationMatrixColumn  = cRotationMatrixRow.GetInColumns();
	SetActualInvInertiaTensor(rot * (GetInvInertiaTensor() * rotTrans));
  SetActualInertiaTensor(rot * (GetInertiaTensor() * rotTrans));
}

rb_real IRigidBodyState::CalculateEnergy()
{
   return (GetVelocity().SquareSize() * GetMass() + (GetAngularVelocity() * GetAngularMomentum())) / 2;
}

void IRigidBodyState::SlowDown(float coef)
{
  SetVelocity(GetVelocity() * coef);
  SetAngularVelocity(GetAngularVelocity() * coef);
  AngularMomentumFromAngularVelocity();
}
#endif
