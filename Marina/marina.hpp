#ifdef _MSC_VER
#pragma once
#endif

#ifndef __MARINA_HPP__
#define __MARINA_HPP__

#include <El/elementpch.hpp>
#include <El/Math/Math3d.hpp>
#include <Es/Containers/array.hpp>

typedef float marina_real;

class IRigidBody;
class IRBGeometry;
class BodyGroup;

class RigidBodyObject : public RefCount
{      
public:
  IRBGeometry * _geometry;
  IRigidBody * _physBody;  
  bool _static;
  bool _ignoreCollWithLandscape;
  void * _userData;

  // reserved for use in library
  BodyGroup * _group;  
};

TypeIsSimple(RigidBodyObject)
TypeIsSimple(RigidBodyObject *)

typedef RefArray<RigidBodyObject> RigidBodyArray;

// define geometry to separate
typedef struct 
{
  RigidBodyObject * obj;
  int geomID;
} SeparateGeom;

TypeIsSimple(SeparateGeom);

typedef AutoArray<SeparateGeom> SeparateGeomArray;

#if _MARINA_PROJECT
#define _ENABLE_BREAKABLE 0
#else
#define _ENABLE_BREAKABLE 0
#endif

// return value
#define MARINA_NEXT       2
#define MARINA_OK       1
#define MARINA_FAILURE  0

#include "BaseGeometry.h"
#include "RigidBody.h"
//#include "core/core.hpp"
#include "core/Dantzig.hpp"
#include "Simulation.h"

#include "BodyGroup.h"


#endif