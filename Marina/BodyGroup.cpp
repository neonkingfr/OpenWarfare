#ifndef _MARINA_PROJECT 
#include <Poseidon/lib/wpch.hpp>
#else
#define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES

#include "marina.hpp"
#include "inc.h"
#include "BodyGroup.h"

extern Matrix<marina_real> g_cMatrixA;  //< Matrix A. 


void BodyGroup::CalcNormMass()
{
  // Set minimal mass
  _normalizationMass = FLT_MAX;
  for(int i = 0; i < _cBodies.Size(); i++)
  {
    if (!_cBodies[i]->_static)
    {
      float mass = _cBodies[i]->_physBody->GetMass();
      if (mass < _normalizationMass)
        _normalizationMass = mass;
    }
  }
}
/*
void BodyGroup::CalculateMatrix()
{
  PROFILE_SCOPE_MARINA(calcmatrix);
  CalcNormMass();

  unsigned int nNumberOfPoints = _cContacts.Size();
  g_cMatrixA.Init(3 * nNumberOfPoints);

  for( int i = 0; i < _cContacts.Size(); i++)
  {
    const RBContactPoint &tContact1 = _cContacts[i];

    if (tContact1.pcBody1 != NULL)
    {
      for( int j = 0; j < nNumberOfPoints; j++)
      {
        const RBContactPoint &tContact2 = _cContacts[j];

        if (tContact1.pcBody1 == tContact2.pcBody1 || tContact1.pcBody1 == tContact2.pcBody2)
        {
          Vector3 cAccel;
          for( int jj = 0; jj < 3; jj++)
          {
            tContact1.pcBody1->_physBody->DiffAccelerationAtPointOnForceAtPoint(cAccel, tContact1.pos, 
              tContact2.dir[jj] * (tContact1.pcBody1 == tContact2.pcBody1 ? 1.0f : -1.0f) , tContact2.pos);

            cAccel *= _normalizationMass;
            for( int ii = 0; ii < 3; ii++)
            {

              g_cMatrixA.AddMemberValue(3 * i + ii, 3 * j + jj, cAccel * tContact1.dir[ii]);
              //	if (3 * i + ii != 3 * j + jj)
              //		g_cMatrixA.SetMemberValue(3 * j + jj, 3 * i + ii, cAccel * tContact1.dir[ii]);
            }
          }
        } 
      }
    }

    if (tContact1.pcBody2 != NULL)
    {

      for(unsigned int j = 0; j < nNumberOfPoints; j++)
      {
        const RBContactPoint &tContact2 = _cContacts[j];
        if (tContact1.pcBody2 == tContact2.pcBody1 || tContact1.pcBody2 == tContact2.pcBody2)
        {
          Vector3 cAccel;

          for(unsigned int jj = 0; jj < 3; jj++)
          {
            tContact1.pcBody2->_physBody->DiffAccelerationAtPointOnForceAtPoint(cAccel, tContact1.pos, 
              tContact2.dir[jj] * (tContact1.pcBody2 == tContact2.pcBody1 ? 1.0f : -1.0f) , tContact2.pos);

            cAccel *= _normalizationMass;
            for(unsigned int ii =  0; ii < 3; ii++)
            {

              g_cMatrixA.AddMemberValue(3 * i + ii, 3 * j + jj, -cAccel * tContact1.dir[ii]);
              //	if (3 * i + ii != 3 * j + jj)
              //		g_cMatrixA.SetMemberValue(3 * j + jj, 3 * i + ii, -cAccel * tContact1.dir[ii]);
            }
          }
        }
      }
    }        
  }
}
*/

//M_MATRIX BodyGroup::_cMatrixA;

/*void BodyGroup::CalculateMatrix()
{
  PROFILE_SCOPE_MARINA(calcmatrix);
 
  CalcNormMass();

  unsigned int nNumberOfPoints = _cContacts.Size();
  g_cMatrixA.Init(3 * nNumberOfPoints);

  PROFILE_SCOPE_MARINA(calcmatrixcore);
 
  for( int i = 0; i < _cContacts.Size(); i++)
  {
    const RBContactPoint &tContact1 = _cContacts[i];

    if (!tContact1.pcBody1->_static)
    {
      IRigidBody * physBody = tContact1.pcBody1->_physBody;
      Vector3 massCenter = physBody->GetMassCenter();
      for( int ii = 0; ii < 3; ii++)
      {        
        Vector3 cAccelPart1 =  tContact1.dir[ii] * physBody->GetInvMass() * _normalizationMass;
        Vector3 cAccelPart2 = (physBody->GetActualInvInertiaTensor() * (( tContact1.pos - massCenter).CrossProduct(tContact1.dir[ii]))) * _normalizationMass;        

        int index = 3 * i + ii;
         
        for(unsigned int j = i; j < nNumberOfPoints; j++)
        {          
          const RBContactPoint &tContact2 = _cContacts[j];

          if (tContact1.pcBody1 == tContact2.pcBody1 || tContact1.pcBody1 == tContact2.pcBody2)
          {    
            PROFILE_SCOPE_MARINA(calcmatrixinner);
            Vector3 cAccel = cAccelPart1 + cAccelPart2.CrossProduct( tContact2.pos - massCenter);
            //tContact1.pcBody1->_physBody->DiffAccelerationAtPointOnForceAtPoint(cAccel, tContact2.pos, 
            //  tContact1.dir[ii] , tContact1.pos);             

            if (tContact1.pcBody1 != tContact2.pcBody1)
              cAccel *= -1.0f;            

            for( int jj = (i == j) ? ii : 0; jj < 3; jj++)
            {   
              float val = cAccel * tContact2.dir[jj];
              g_cMatrixA.AddMemberValue(3 * j + jj, index, val);
              if (i != j || ii != jj)
                g_cMatrixA.AddMemberValue(index, 3 * j + jj, val);
            }
          }
        } 
      }
    }

    if (!tContact1.pcBody2->_static)
    {
      IRigidBody * physBody = tContact1.pcBody2->_physBody;
      Vector3 massCenter = physBody->GetMassCenter();
      for( int ii = 0; ii < 3; ii++)
      {
        Vector3 cAccelPart1 =  tContact1.dir[ii] * physBody->GetInvMass() * _normalizationMass;
        Vector3 cAccelPart2 = (physBody->GetActualInvInertiaTensor() * (( tContact1.pos - massCenter).CrossProduct(tContact1.dir[ii]))) * _normalizationMass;

        int index = 3 * i + ii;
        for(unsigned int j = i; j < nNumberOfPoints; j++)
        {
          const RBContactPoint &tContact2 = _cContacts[j];
          if (tContact1.pcBody2 == tContact2.pcBody1 || tContact1.pcBody2 == tContact2.pcBody2)
          {    
            PROFILE_SCOPE_MARINA(calcmatrixinner);
            Vector3 cAccel = cAccelPart1 + cAccelPart2.CrossProduct( tContact2.pos - massCenter);;            
            //tContact1.pcBody2->_physBody->DiffAccelerationAtPointOnForceAtPoint(cAccel, tContact2.pos, 
            //  tContact1.dir[ii] , tContact1.pos);  
            if (tContact1.pcBody2 != tContact2.pcBody1)
              cAccel *= -1.0f;             
            
            for(unsigned int jj = (i == j) ? ii : 0; jj < 3; jj++)
            {          
              float val = -cAccel * tContact2.dir[jj];
              g_cMatrixA.AddMemberValue(3 * j + jj, index, val);
              if (i != j || ii != jj)
                g_cMatrixA.AddMemberValue(index, 3 * j + jj, val);
            }
          }
        }
      }
    }        
  }
  _bMatrixReady = TRUE;
  //LogF("Matrix size: %d", 3 * nNumberOfPoints);
}*/

void BodyGroup::CalculateMatrix()
{
  PROFILE_SCOPE_MARINA(calcmatrix);

  CalcNormMass();

  int nonzeros = 0;

  unsigned int nNumberOfPoints = _cContacts.Size();
  g_cMatrixA.Init(3 * nNumberOfPoints);

  PROFILE_SCOPE_MARINA(calcmatrixcore);

  for( int i = 0; i < _cBodies.Size(); i++)
  {
    IRigidBody * physBody = _cBodies[i]->_physBody;
    Vector3 massCenter = physBody->GetMassCenter();

    const AutoArray<int>& contacts = _contactsOfBodies[i];
    for(int j = 0; j < contacts.Size(); j++)
    {
      int jContact = contacts[j];
      const RBContactPoint &tContact1 = _cContacts[jContact];

      for( int jj = 0; jj < 3; jj++)
      {        
        Vector3 cAccelPart1 =  tContact1.dir[jj] * physBody->GetInvMass() * _normalizationMass;
        Vector3 cAccelPart2 = (physBody->GetActualInvInertiaTensor() * (( tContact1.pos - massCenter).CrossProduct(tContact1.dir[jj]))) * _normalizationMass;        

        if (tContact1.pcBody1 != _cBodies[i])
        {
          cAccelPart1 *= -1;
          cAccelPart2 *= -1;
        }

        int index = 3 * jContact + jj;

        for(int k = j; k < contacts.Size(); k++)
        {         
          int kContact = contacts[k];
          const RBContactPoint &tContact2 = _cContacts[kContact];
         
          Vector3 cAccel = cAccelPart1 + cAccelPart2.CrossProduct( tContact2.pos - massCenter);
          //tContact1.pcBody1->_physBody->DiffAccelerationAtPointOnForceAtPoint(cAccel, tContact2.pos, 
          //  tContact1.dir[ii] , tContact1.pos);             

          if (_cBodies[i] != tContact2.pcBody1)
            cAccel *= -1.0f;            

          for( int kk = (j == k) ? jj : 0; kk < 3; kk++)
          {   
            float val = cAccel * tContact2.dir[kk];
            g_cMatrixA.AddMemberValueRaw(3 * kContact + kk, index, val);
            nonzeros++;
            if (j != k || jj != kk)
            {
              g_cMatrixA.AddMemberValueRaw(index, 3 * kContact + kk, val);
              nonzeros++;
            }
          }
        }
      } 
    }
  }
  _bMatrixReady = TRUE;
  nonzeros -= ( nonzeros - 3 * nNumberOfPoints)/2;
  //LogF("Matrix size: %d nonzeros %d, %lf", 3 * nNumberOfPoints, nonzeros, nonzeros/(float)(9 * nNumberOfPoints * nNumberOfPoints));
}

const M_MATRIX& BodyGroup::GetMatrix()
{
    if (!_bMatrixReady)
        CalculateMatrix();

   // g_cMatrixA.NeutralizePermutation();
    
    return g_cMatrixA;
}

void BodyGroup::JoinGroup(BodyGroup & cBodyGroup)
{
  _bMatrixReady = FALSE;
  _bFrozen = FALSE;

  int lastContact = _cContacts.Size();
  for( int i = 0; i < cBodyGroup._cBodies.Size(); i++)
  {    
    cBodyGroup._cBodies[i]->_group = this;
    _cBodies.Add(cBodyGroup._cBodies[i]);
    _contactsOfBodies.Add( cBodyGroup._contactsOfBodies[i]);  
    AutoArray<int>& contacts = _contactsOfBodies[_contactsOfBodies.Size() - 1];
    for(int j = 0; j < contacts.Size(); j++)
      contacts[j] += lastContact;
  }

  for( int i = 0; i < cBodyGroup._cContacts.Size(); i++)  
    _cContacts.Add(cBodyGroup._cContacts[i]);          
}

void BodyGroup::AddContact(const RBContactPoint& tContact)
{
  _bMatrixReady = FALSE;
  _bFrozen = FALSE;

  int iContact = _cContacts.Add(tContact);

  // Add Bodies if they are not in list,
  if (!tContact.pcBody1->_static)
  {
    tContact.pcBody1->_group = this;
    int i = 0;
    for(; i < _cBodies.Size(); i++)    
      if (_cBodies[i] == tContact.pcBody1)
        break;    

    if (i == _cBodies.Size())
    {
      _cBodies.Add(tContact.pcBody1);

      AutoArray<int>& contacts = _contactsOfBodies.Append();
      contacts.Add(iContact);
    }
    else    
     _contactsOfBodies[i].Add(iContact); 
  }

  if (!tContact.pcBody2->_static)
  {
    tContact.pcBody2->_group = this;
    int i = 0;
    for(; i < _cBodies.Size(); i++)    
      if (_cBodies[i] == tContact.pcBody2)
        break;    

    if (i == _cBodies.Size())
    {
      _cBodies.Add(tContact.pcBody2);

      AutoArray<int>& contacts = _contactsOfBodies.Append();
      contacts.Add(iContact);
    }
    else    
      _contactsOfBodies[i].Add(iContact); 
  }
}

#endif 

