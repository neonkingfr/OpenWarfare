#ifdef _MSC_VER
#pragma once
#endif

#ifndef __BASE_GEOMETRY_H__
#define __BASE_GEOMETRY_H__

typedef float rb_real;

#include "Quaternion.h"
#include <El/bb/boundingbox.hpp>
class RigidBodyObject;

struct RBContactPoint // contact point used by marina physical engine
{	
  Vector3 pos; // world space position
  Vector3 dir[3]; // direction out from pedestal.pcBody1, direction x and y from pedestal

  float under; // how much under surface

  float fSlidingFrictionCoef;
  float fRestitutionCoef;

  int geomID1;
  int geomID2;

  RigidBodyObject * pcBody1;
  RigidBodyObject * pcBody2;

  // just for debugging
  int reserved;
};

TypeIsSimple(RBContactPoint)
typedef AutoArray<RBContactPoint> RBContactArray;

class AABox
{
protected:
  Vector3 _max;
  Vector3 _min;

public:
  AABox(): _max(VZero), _min(VZero) {};
  AABox(const Vector3& amin, const Vector3& amax) {_max = amax; _min = amin;};
  AABox(const AABox& rect) {_max = rect._max; _min = rect._min;};

  bool IsIntersection(const AABox& second) const;
  void Enlarge(rb_real size);

  const Vector3& Min() const {return _min;};
  const Vector3& Max() const {return _max;};
  Vector3 Center() const {return (_min + _max) * 0.5f;};
  bool IsPointIn(Vector3Val pos) const;

  AABox operator+(const AABox& second) const;
  const AABox& operator+=(const AABox& second);
  const AABox& operator=(const AABox& second) {_max = second._max; _min = second._min; return *this;};

  // Adding vector means to minimaly enlarge rectangle so that it contains vector's end point.
  AABox operator+(const Vector3& second) const;
  const AABox& operator+=(const Vector3& second);

  // The rectangle with zerosize is created.
  const AABox& operator=(const Vector3& second) {_max = second; _min = second; return *this;};
};

class IRBGeometry : public ShapeReference
{
protected:  
  int _ID; // unique ID of the body.

  AABox _enclosingRectangle;
  Vector3 _pos;
  QuaternionRB _orientation;

  Vector3 _relPos;
  QuaternionRB _relOrientation;

  Ref<RigidBodyObject> _ptBody; //corresponding body. not very elegant, later will be removed

public:
  IRBGeometry( int ID) : _ID(ID),  /*_ptBody(NULL),*/
    _pos(VZero), _relPos(VZero) {};
  virtual ~IRBGeometry() {};

  int GetID() const {return _ID;};
  RigidBodyObject * GetBody() const {return _ptBody;};

  //int GetVladasID() const {return _iVladasID;};

  virtual void SetOrigin(const Vector3& pos, const QuaternionRB& orientation);
  void SetRelOrigin(const Vector3& relPos, const QuaternionRB& relQuaternion);
  void TranslateRelOrigin(const Vector3& relPos);

  const Vector3& RelPos() const {return _relPos;};
  const QuaternionRB& RelOrientation() const {return _relOrientation;};

  const Vector3& Pos() const {return _pos;};
  const QuaternionRB& Orientation() const {return _orientation;};
  
  const AABox& GetEnclosingRectangle() const {return _enclosingRectangle;};

  virtual void CalculateEnclosingRectangle() = 0;

  virtual bool IsIntersection( IRBGeometry& second, int simCycle)  = 0;
  virtual bool ForceInterFree( IRBGeometry& second, int simCycle) = 0; // return FALSE if bodies must be ignored.
  virtual void Scale(float scale) = 0;
  virtual void NormScale() = 0;

  virtual int FindContacts( RBContactArray& contactPoints, IRBGeometry& second, int simCycle) = 0;    

  bool AreInProximity(const AABox& test) const { return _enclosingRectangle.IsIntersection(test);};

  virtual void SetBody(Ref<RigidBodyObject> ptBody) {_ptBody = ptBody;};

  /// Get geometry vertexes. Usefull for OOBB 
  //virtual int NVerteces() const {return 0};
  //virtual Vector3Val VertexPos(int i) const {return VZero;};

};
#endif