
#include "inc.h"
#include "marina.hpp"
#include "Geometry.h"
#include "BoxCollision.h"
#include "BodyGroup.h"
#include "cd/IntersectConvex.hpp"
#include "cd/ContactConvex.hpp"

extern marina_real g_fTolerance;

#define IGNORANCE_TRY_STEP 5
#define IGNORANCE_SCALE 0.6f

int LastUsedID = 0;

// select correct drawing functions

#ifdef dDOUBLE
#define dsDrawBox dsDrawBoxD
#define dsDrawSphere dsDrawSphereD
#define dsDrawCylinder dsDrawCylinderD
#define dsDrawCappedCylinder dsDrawCappedCylinderD
//#define dsDrawTriangle dsDrawTriangleD
#endif

#define GL_TRIANGLES 0x0004


BOOL CRectangle::IsIntersection(const CRectangle& cSecond) const
{
   return (max(_cMin[0],cSecond._cMin[0]) < min(_cMax[0],cSecond._cMax[0]) &&
       max(_cMin[1],cSecond._cMin[1]) < min(_cMax[1],cSecond._cMax[1]) &&
       max(_cMin[2],cSecond._cMin[2]) < min(_cMax[2],cSecond._cMax[2]));
}

void CRectangle::Enlarge(marina_real fSize)
{
    _cMin[0] -= fSize;
    _cMin[1] -= fSize;
    _cMin[2] -= fSize;

    _cMax[0] += fSize;
    _cMax[1] += fSize;
    _cMax[2] += fSize;
}

CRectangle CRectangle::operator+(const CRectangle& cSecond) const
{
    Vector3 cNewMin;
    Vector3 cNewMax;

    cNewMin[0] = (min(_cMin[0], cSecond._cMin[0]));
    cNewMin[1] = (min(_cMin[1], cSecond._cMin[1]));
    cNewMin[2] = (min(_cMin[2], cSecond._cMin[2]));

    cNewMax[0] = (max(_cMax[0], cSecond._cMax[0]));
    cNewMax[1] = (max(_cMax[1], cSecond._cMax[1]));
    cNewMax[2] = (max(_cMax[2], cSecond._cMax[2]));

    return CRectangle( cNewMin, cNewMax);
}

const CRectangle& CRectangle::operator+=(const CRectangle& cSecond)
{
    _cMin[0] = (min(_cMin[0], cSecond._cMin[0]));
    _cMin[1] = (min(_cMin[1], cSecond._cMin[1]));
    _cMin[2] = (min(_cMin[2], cSecond._cMin[2]));

    _cMax[0] = (max(_cMax[0], cSecond._cMax[0]));
    _cMax[1] = (max(_cMax[1], cSecond._cMax[1]));
    _cMax[2] = (max(_cMax[2], cSecond._cMax[2]));
    
    return *this;
}

CRectangle CRectangle::operator+(const Vector3& cSecond) const
{
    Vector3 cNewMin;
    Vector3 cNewMax;

    cNewMin[0] = (min(_cMin[0], cSecond[0]));
    cNewMin[1] = (min(_cMin[1], cSecond[1]));
    cNewMin[2] = (min(_cMin[2], cSecond[2]));

    cNewMax[0] = (max(_cMax[0], cSecond[0]));
    cNewMax[1] = (max(_cMax[1], cSecond[1]));
    cNewMax[2] = (max(_cMax[2], cSecond[2]));

    return CRectangle( cNewMin, cNewMax);
}

const CRectangle& CRectangle::operator+=(const Vector3& cSecond)
{
    _cMin[0] = (min(_cMin[0], cSecond[0]));
    _cMin[1] = (min(_cMin[1], cSecond[1]));
    _cMin[2] = (min(_cMin[2], cSecond[2]));

    _cMax[0] = (max(_cMax[0], cSecond[0]));
    _cMax[1] = (max(_cMax[1], cSecond[1]));
    _cMax[2] = (max(_cMax[2], cSecond[2]));
    
    return *this;
}

static void SMatrixFromQuaternionAndPos(SMatrix4& tMatrix, const CQuaternion& cOrientation, const Vector3& cPos)
{
    CMatrix3DRow cMatrix = cOrientation.GetMatrix().GetInColumns().GetTransposed();

    tMatrix._aside._x = cMatrix[0][0];
    tMatrix._aside._y = cMatrix[0][2];
    tMatrix._aside._z = cMatrix[0][1];

    tMatrix._up._x = cMatrix[2][0];
    tMatrix._up._y = cMatrix[2][2];
    tMatrix._up._z = cMatrix[2][1];

    tMatrix._direction._x = cMatrix[1][0];
    tMatrix._direction._y = cMatrix[1][2];
    tMatrix._direction._z = cMatrix[1][1];

    tMatrix._position._x = cPos[0];
    tMatrix._position._y = cPos[2];
    tMatrix._position._z = cPos[1];
};


void CGeometry::SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation)
{
    _cPos = cPos + cOrientation.GetMatrix() * _cRelPos;
    _cOrientation = cOrientation * _cRelOrientation;
}

void CGeometry::SetRelOrigin(const Vector3& cRelPos, const CQuaternion& cRelOrientation)
{
    _cRelPos = cRelPos;
    _cRelOrientation = cRelOrientation;
}

void CGeometry::TranslateRelOrigin(const Vector3& cRelPos)
{
    _cPos = _cPos + cRelPos;
    _cRelPos += cRelPos;
}

void CBoxGeometry::CalculateEnclosingRectangle()
{    
    const CMatrix3DRow cToWorld(_cOrientation.GetMatrix());
        
    Vector3 cBodyPoint;
    for(int ix = -1; ix < 2; ix+= 2)
    {
        cBodyPoint[0] = (_cBoxSizes[0] * ix);

        for(int iy = -1; iy < 2; iy+= 2)
        {
            cBodyPoint[1] = (_cBoxSizes[1] * iy);

            for(int iz = -1; iz < 2; iz+= 2)
            {
                cBodyPoint[2] = (_cBoxSizes[2] * iz);
                Vector3 cWorldPoint = cToWorld * cBodyPoint + _cPos;

                if (ix == -1 && iy == -1 && iz == -1)
                {
                    _cEnclosingRectangle = cWorldPoint;
                }
                else
                {
                    _cEnclosingRectangle += cWorldPoint;

                }           
            }
        }
    }        
}

int CBoxGeometry::FindContacts( M_ContactArray& cContactPoints, CGeometry& cSecond, int simCycle)
{
  CRectangle cEnlargedEnclRectangle(_cEnclosingRectangle);
  cEnlargedEnclRectangle.Enlarge(g_fTolerance);

  if (!cEnlargedEnclRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return 0; // enclosing rectagles doen not overlap
  }

  switch (cSecond.GetType())
  {
  case EGT_COMPOSE:
    return cSecond.FindContacts(cContactPoints, *this, simCycle);
  case EGT_BOX:
    {    
      const CBoxGeometry& cSecondBox = static_cast<const CBoxGeometry&>(cSecond);
      int nContacts = cContactPoints.Size();

      BoxCollision(cContactPoints, _cBoxSizes, _cPos, _cOrientation, 
        cSecondBox._cBoxSizes, cSecondBox._cPos, cSecondBox._cOrientation); 

      M_TBODY * pcBody1 = (_ptBody->_pcBodyPhys->GetInvMass() == 0) ? NULL : _ptBody;
      M_TBODY * pcBody2 = (cSecondBox.GetBody()->_pcBodyPhys->GetInvMass() == 0) ? NULL : cSecondBox.GetBody();

      float fRestitutionCoef = (pcBody1 == NULL) ? 
        ((pcBody2 == NULL) ? 0: pcBody2->_pcBodyPhys->GetRestitutionCoef()) : pcBody1->_pcBodyPhys->GetRestitutionCoef();

      float fFrictionCoef = (pcBody1 == NULL) ? 
        ((pcBody2 == NULL) ? 0: pcBody2->_pcBodyPhys->GetSlidingFrictionCoef()) : pcBody1->_pcBodyPhys->GetSlidingFrictionCoef();

      for(;nContacts < cContactPoints.Size(); nContacts++)
      {
        cContactPoints[nContacts].pcBody1 = pcBody1;
        cContactPoints[nContacts].pcBody2 = pcBody2;
        cContactPoints[nContacts].fRestitutionCoef = fRestitutionCoef;
        cContactPoints[nContacts].fSlidingFrictionCoef = fFrictionCoef;
      }

      return MARINA_OK;
    }
  case EGT_CONVEX:
    {
      return 0;
    }
  default:
    ASSERT(FALSE);
  }
  return -1;  
}

BOOL CBoxGeometry::IsIntersection( CGeometry& cSecond, int simCycle) 
{    
    if (!_cEnclosingRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
    {
        return FALSE; // enclosing rectagles doen not overlap
    }

    switch (cSecond.GetType())
    {
    case EGT_COMPOSE:
        return cSecond.IsIntersection(*this, simCycle);
    case EGT_BOX:
    {    
        const CBoxGeometry& cSecondBox = static_cast<const CBoxGeometry&>(cSecond);

        return BoxIntersection(_cBoxSizes, _cPos, _cOrientation, 
            cSecondBox._cBoxSizes, cSecondBox._cPos, cSecondBox._cOrientation);        
    }
    case EGT_CONVEX:
    {
      return FALSE;
    }
    default:
        ASSERT(FALSE);
    }
    return FALSE;  
}


void CBoxGeometry::CreateDrawClients(CHordubalClientDll * pcDrawingClient, unsigned int& iVladasID, BOOL bUseDrawstuff, int iBodyColor)
{
  _bUseDrawstuff = bUseDrawstuff;
  _iBodyColor = iBodyColor;

  if (pcDrawingClient != NULL)
  {
    ASSERT(iVladasID != 0);

    _pcDrawingClient = pcDrawingClient;
    _iVladasID = iVladasID; 

    SMatrix4 tMatrix;
    SMatrixFromQuaternionAndPos( tMatrix, _cOrientation, _cPos);
    _pcDrawingClient->CreateBlock(iVladasID, tMatrix, 2 * _cBoxSizes[0], 2 * _cBoxSizes[2], 2 * _cBoxSizes[1], _iBodyColor);
  }
}

void CBoxGeometry::Draw() const
{
  if (_bUseDrawstuff)
  {

    dMatrix3 R;
    dQuaternion q;
    q[0] = _cOrientation[0];
    q[1] = _cOrientation[1];
    q[2] = _cOrientation[2];
    q[3] = _cOrientation[3];
    dQtoR(q,R);

    dVector3 pos;
    pos[0] = _cPos[0];
    pos[1] = _cPos[1];
    pos[2] = _cPos[2];

    dVector3 sides;
    sides[0] = 2 * _cBoxSizes[0];
    sides[1] = 2 * _cBoxSizes[1];
    sides[2] = 2 * _cBoxSizes[2];

    dsSetColor (((_iBodyColor&(0xff<<16)) >> 16)/256.0f,
      ((_iBodyColor&(0xff<<8)) >> 8)/256.0f,
      (_iBodyColor&(0xff))/256.0f);

    dsDrawBox (pos,R,sides);
  }

  if (_iVladasID != 0 && (_ptBody->_pcGroup == NULL || !_ptBody->_pcGroup->IsFrozen()))
  {
    SMatrix4 tMatrix;
    SMatrixFromQuaternionAndPos( tMatrix, _cOrientation, _cPos);
    _pcDrawingClient->MoveModel(_iVladasID, tMatrix); 
  }
}



void CComposeGeometry::SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation)
{
  _cPos = cPos + cOrientation.GetMatrix() * _cRelPos;
  _cOrientation = cOrientation * _cRelOrientation;

  for( int i = 0; i < _cGeomArray.Size(); i++)  
    _cGeomArray[i]->SetOrigin(_cPos, _cOrientation);  
}

void CComposeGeometry::CreateDrawClients(CHordubalClientDll * pcDrawingClient, unsigned int& iVladasID, BOOL bUseDrawstuf,int iBodyColor)
{
  for(int i = 0; i < _cGeomArray.Size(); i++)
  {
    iVladasID++;
    iBodyColor += 0x223344;
    _cGeomArray[i]->CreateDrawClients(pcDrawingClient,iVladasID,bUseDrawstuf,iBodyColor);    
  }
}

void CComposeGeometry::Draw() const
{
  for( int i = 0; i < _cGeomArray.Size(); i++)
  {
    _cGeomArray[i]->Draw();
  }
}

void CComposeGeometry::CalculateEnclosingRectangle()
{
  if (_cGeomArray.Size() == 0)
    return;

  //_cGeomArray[0]->SetOrigin(_cPos, _cOrientation);
  _cGeomArray[0]->CalculateEnclosingRectangle();

  _cEnclosingRectangle = _cGeomArray[0]->GetEnclosingRectangle();

  for( int i = 1; i < _cGeomArray.Size(); i++)
  {
    //_cGeomArray[i]->SetOrigin(_cPos, _cOrientation);
    _cGeomArray[i]->CalculateEnclosingRectangle();

    _cEnclosingRectangle += _cGeomArray[i]->GetEnclosingRectangle();
  }
}

int CComposeGeometry::FindContacts( M_ContactArray& cContactPoints, CGeometry& cSecond, int simCycle)
{
  CRectangle cEnlargedEnclRectangle(_cEnclosingRectangle);
  cEnlargedEnclRectangle.Enlarge(g_fTolerance);

  if (!cEnlargedEnclRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return MARINA_OK; // enclosing rectagles doen not overlap
  }

  // Do every it with every member
  Ref<CDCacheItemData> data;
  if (cdCache.Get(GetID(), cSecond.GetID(), data))
  {
    if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
    {            
      return MARINA_OK; //ignore
    }
  }
  for( int i = 0; i < _cGeomArray.Size(); i++)
  {
    _cGeomArray[i]->FindContacts(cContactPoints, cSecond, simCycle);
  }

  return MARINA_OK;
}

BOOL CComposeGeometry::IsIntersection( CGeometry& cSecond, int simCycle) 
{
  if (!_cEnclosingRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return FALSE; // enclosing rectagles doen not overlap
  }

  // Do every it with every member
  Ref<CDCacheItemData> data;
  if (cdCache.Get(GetID(), cSecond.GetID(), data))
  {
    if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE && data->_lastUpdate + IGNORANCE_TRY_STEP >= simCycle)
    {            
      return FALSE;
    }
  }

  for( int i = 0; i < _cGeomArray.Size(); i++)
  {
    if (_cGeomArray[i]->IsIntersection(cSecond, simCycle))
    {
      if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE && data->_lastUpdate + IGNORANCE_TRY_STEP < simCycle)
      { 
        // it was just a try
        return FALSE;
      }

      return TRUE;
    }
  }

  if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
  {
    // no need to ignore
    cdCache.Delete(GetID(), cSecond.GetID());    
  }

  return FALSE;
}

BOOL CComposeGeometry::ForceInterFree( CGeometry& cSecond, int simCycle)
{
  if (!_cEnclosingRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return TRUE; // enclosing rectagles doen not overlap
  }

  // Do every it with every member

  int i = 0;
  Ref<CDCacheItemData> data;
  if (cdCache.Get(GetID(), cSecond.GetID(), data))
  {
    if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
      return TRUE;
  }

  for(; i < _cGeomArray.Size(); i++)
  {
    if (!_cGeomArray[i]->ForceInterFree(cSecond, simCycle))
      break;
  }

  if (i < _cGeomArray.Size())
  {
    if (data.IsNull())
    {
      data = new CDCacheItemData;
      cdCache.Add(GetID(), cSecond.GetID(), data);
    }

    data->_scale = IGNORANCE_SCALE;
    data->_lastUpdate = simCycle;
    return FALSE;
  }  

  return TRUE;
}

void CComposeGeometry::SetBody(M_TBODY * ptBody)
{
    // distribute ptBody to the childs
    _ptBody = ptBody;

    for( int i = 0; i < _cGeomArray.Size(); i++)
    {
       _cGeomArray[i]->SetBody(ptBody);
    }
}

void CComposeGeometry::TranslateChilds(const Vector3& cRelPos)
{
    for( int i = 0; i < _cGeomArray.Size(); i++)
    {
       _cGeomArray[i]->TranslateRelOrigin(cRelPos);
    }
}

CComposeGeometry::~CComposeGeometry()
{
    for( int i = 0; i < _cGeomArray.Size(); i++)
    {
        delete _cGeomArray[i];
    }
}

CMeshGeometry::CMeshGeometry(int ID) : CGeometry(EGT_CONVEX, ID),_geomCenter(VZero),_validNormScale(false)
{
  _cEnclosingRectangle = CRectangle(Vector3(0,0,0),Vector3(1,1,1));
}

void CMeshGeometry::CreateDrawClients(CHordubalClientDll * pcDrawingClient, unsigned int& iVladasID, BOOL bUseDrawstuff, int iBodyColor)
{
  _bUseDrawstuff = bUseDrawstuff;

  _iBodyColor = iBodyColor;
}

void CMeshGeometry::Draw() const
{
  if (_bUseDrawstuff)
  {    
    dMatrix3 R;
    dQuaternion q;
    /*q[0] = _cOrientation[0];
    q[1] = _cOrientation[1];
    q[2] = _cOrientation[2];
    q[3] = _cOrientation[3];*/
    q[0] = 1.0f;
    q[1] = q[2] = q[3] = 0.0f;
    dQtoR(q,R);

    dVector3 pos;
    pos[0] = 0;
    pos[1] = 0;
    pos[2] = 0;
   
    dsSetColor (((_iBodyColor&(0xff<<16)) >> 16)/256.0f,
      ((_iBodyColor&(0xff<<8)) >> 8)/256.0f,
      (_iBodyColor&(0xff))/256.0f);

    if (false)
    {
      const Vector3 minS = _cEnclosingRectangle.Min();
      const Vector3 maxS = _cEnclosingRectangle.Max();

      pos[0] = (minS[0] + maxS[0]) * 0.5f;
      pos[1] = (minS[1] + maxS[1]) * 0.5f;
      pos[2] = (minS[2] + maxS[2]) * 0.5f;

      dReal size[3];
      size[0] = maxS[0] - minS[0];
      size[1] = maxS[1] - minS[1];
      size[2] = maxS[2] - minS[2];

      dsDrawBox(pos, R, size);
    }
    else
    {
      int _numberOfTriangles = _triangle.Size() / 3;
      for(int i = 0; i < _numberOfTriangles; i++)
      {
        dVector3 pos1;
        const Vector3& vertex1 = _vertex[_triangle[3 * i]];
        pos1[0] = vertex1[0];
        pos1[1] = vertex1[1];
        pos1[2] = vertex1[2];

        dVector3 pos2;
        const Vector3& vertex2 = _vertex[_triangle[3 * i + 1]];
        pos2[0] = vertex2[0];
        pos2[1] = vertex2[1];
        pos2[2] = vertex2[2];

        dVector3 pos3;
        const Vector3& vertex3 = _vertex[_triangle[3 * i + 2]];
        pos3[0] = vertex3[0];
        pos3[1] = vertex3[1];
        pos3[2] = vertex3[2];

        dsDrawTriangle(pos, R, pos1, pos2, pos3,GL_TRIANGLES);
      }
    }
  }
}


#include "Poseidon/lib/SpecLods.hpp"
class FaceDataRobber : public FaceT
{
public:
  FaceDataRobber(const FaceT& face) : FaceT(face) {};
  int GetN() const {return n;};
  const DataVertex * GetDataVertex() const {return vs;};
};

void SetMassFromLevel(CMass& mass, const ObjectData & geometryLevel)
{
  for(int i = 0; i < geometryLevel.NPoints(); i++)
  {   
    const Vector3 &point = geometryLevel.Point(i);    
    mass.AddMassPoint(geometryLevel.GetPointMass(i), Vector3(point[0], point[2], point[1]));    
  }  
}

void CMeshGeometry::LoadFromP3D(LPCSTR path, CMass& mass)
{
  // Load form p3d.
  LODObject lodObj;
  lodObj.Load(WFilePath(path), NULL, NULL);
  int gIndex=lodObj.FindLevelExact(GEOMETRY_SPEC);

  if (gIndex == -1)
    return;

  lodObj.SelectLevel(gIndex);
  ObjectData & geometryLevel = lodObj;

  _vertex.Resize(0);
  _vertex.Reserve(geometryLevel.NPoints(), geometryLevel.NPoints());

  for(int i = 0; i < geometryLevel.NPoints(); i++)
  {
    const Vector3 &point = geometryLevel.Point(i);
    _vertex.Add(Vector3(point[0], point[2], point[1]));   
  }

  _triangle.Resize(0);
  _triangle.Reserve(geometryLevel.NFaces(), geometryLevel.NFaces());

  _face.Resize(0);
  _face.Reserve(geometryLevel.NFaces(), geometryLevel.NFaces());

  for(int i = 0; i < geometryLevel.NFaces(); i++)
  {
     const FaceT& face = geometryLevel.Face(i);
     FaceDataRobber rob(face);

     const int n = rob.GetN();
     const DataVertex  * vertexData =  rob.GetDataVertex();
     for(int j = 0; j < n - 2; j++)
     {
       _triangle.Add(vertexData[j].point);
       _triangle.Add(vertexData[j + 1].point);       
       _triangle.Add(vertexData[n - 1].point);   
     }

     Assert(n < MAX_VERTEXES);
     for(int j = 0; j < n; j++)
     {
       MFace& face = _face.Append();
       face.AddVertex(vertexData[j].point);
     }
  }

  FindEdges();
  CalcDirs();
  CalcGeomCenterOrig();
  _vertexOrig = _vertex;

  SetMassFromLevel(mass, geometryLevel);
}

bool CMeshGeometry::LoadFromP3DSel(const ObjectData& obj, int selIndex)
{
  const NamedSelection * sel = obj.GetNamedSel(selIndex);

  strcpy(_name, sel->Name());
  AutoArray<int> mapping;
  mapping.Resize(obj.NPoints());
  _vertex.Resize(0);

  for(int i = 0; i < obj.NPoints(); i++)
  {
    if (sel->PointSelected(i))
    {
      mapping[i] = _vertex.Size();
      const Vector3 &point = obj.Point(i);
      _vertex.Add(Vector3(point[0], point[2], point[1]));
    }
    else
    {
      mapping[i] = -1;
    }
  }

  if (_vertex.Size() == 0)
    return false;

  _triangle.Resize(0);
 
  for(int i = 0; i < obj.NFaces(); i++)
  {
    if (sel->FaceSelected(i))
    {
      const FaceT& face = obj.Face(i);
      FaceDataRobber rob(face);

      const int n = rob.GetN();
      const DataVertex  * vertexData =  rob.GetDataVertex();
      for(int j = 0; j < n - 2; j++)
      {
        _triangle.Add(mapping[vertexData[j].point]);          
        _triangle.Add(mapping[vertexData[j + 1].point]);
        _triangle.Add(mapping[vertexData[n - 1].point]); 
        
        // teselate faces
        MFace& mface = _face.Append();
        mface.AddVertex(mapping[vertexData[j].point]);
        mface.AddVertex(mapping[vertexData[j + 1].point]);
        mface.AddVertex(mapping[vertexData[n - 1].point]);

      }

      /*Assert(n <= MAX_VERTEXES);
      MFace& mface = _face.Append();
      for(int j = 0; j < n; j++)
      {        
        mface.AddVertex(mapping[vertexData[j].point]);
      } */     
    }
  }

  FindEdges();
  CalcDirs();
   _vertexOrig = _vertex;
  CalcGeomCenterOrig();

  //CheckFaces();

  return true;
}

void CComposeGeometry::LoadFromP3D(LPCSTR path, CMass& mass)
{
  // Load form p3d.
  LODObject lodObj;
  lodObj.Load(WFilePath(path), NULL, NULL);
  int gIndex=lodObj.FindLevelExact(GEOMETRY_SPEC);

  if (gIndex == -1)
    return;

  lodObj.SelectLevel(gIndex);
  ObjectData & geometryLevel = lodObj;
 
  NamedSelection * sel;
  for(int i = 0; sel = geometryLevel.GetNamedSel(i); i++)
  {
    if (strncmp(sel->Name(), "Component", 9) == 0)
    {
      CMeshGeometry * meshGeom = new CMeshGeometry(++LastUsedID);
      if (meshGeom->LoadFromP3DSel(geometryLevel,i))
      {      
        meshGeom->SetRelOrigin(VZero, CQuaternion());    
        Add(*meshGeom);    
      }
      else
        delete meshGeom;
    }
  }

  SetMassFromLevel(mass, geometryLevel);
}


void CMeshGeometry::FindEdges()
{
  _edge.Resize(0);
  for(int i = 0; i < _face.Size(); i++)
  {
    MFace & face = _face[i];
    for(int j = 0; j < face.N(); j++)
    {
      int vert1 = min(face[(j == 0) ? face.N()-1: j-1], face[j]);
      int vert2 = max(face[(j == 0) ? face.N()-1: j-1], face[j]);

      // maybe this edge exist?
      int l = 0;
      for(; l < _edge.Size(); l++)
      {
        if (_edge[l][0] == vert1 && _edge[l][1] == vert2)
         break;
      }

      if ( l == _edge.Size())
      {
        MEdge edge(vert1, vert2);
        edge.SetFace(0) = i;
        _edge.Add(edge);
      }
      else
        _edge[l].SetFace(1) = i;
    }
  }
}

void CMeshGeometry::CalcDirs()
{
  {  
    PROFILE_SCOPE(face); 
    for(int i = 0; i < _face.Size(); i++)
    {
      _face[i].CalcPlane(_vertex);
    }
  }

  {
    PROFILE_SCOPE(edge); 
    for(int i = 0; i < _edge.Size(); i++)
    {
      _edge[i].CalcDir(_vertex);
    }
  }
}

void CMeshGeometry::CalcGeomCenterOrig()
{
  _geomCenterOrig = VZero;
  marina_real invSize = 1.0f / _vertexOrig.Size();

  for(int i = 0; i < _vertexOrig.Size(); i++)
  {
    _geomCenterOrig += invSize * _vertexOrig[i];
  }
}

void CMeshGeometry::CheckFaces() const
{
  for(int i = 0; i < _face.Size(); i++)
  {
    _face[i].Check(_vertex);
  }
}

void CMeshGeometry::UpdateOnPos()
{
  {
    PROFILE_SCOPE(vertex); 
    _validNormScale = false;

    CMatrix3DRow rot = _cOrientation.GetMatrix();

    for(int i = 0; i < _vertexOrig.Size(); i++)  
      _vertex[i] = rot * _vertexOrig[i] + _cPos;

    _geomCenter = rot * _geomCenterOrig + _cPos;
  }

  CalcDirs();
}

void CMeshGeometry::SetOrigin(const Vector3& cPos, const CQuaternion& cOrientation)
{

  _cPos = cPos + cOrientation.GetMatrix() * _cRelPos;
  _cOrientation = cOrientation * _cRelOrientation;

  UpdateOnPos();
}

void CMeshGeometry::Scale(float scale)
{
  // scale distance from vertex to geometry center by scale.
  if (!_validNormScale)
  {
    _vertexNormScale = _vertex; //backUp;
    _validNormScale = true;
  }

  // scale vertexes
  Vector3 scaleCenter = (1 - scale) * _geomCenter;
  for(int i = 0; i < _vertex.Size(); i++)
  {
    _vertex[i] = scaleCenter + scale * _vertexNormScale[i];
  }

  //scale planes
  for(int i = 0; i < _face.Size(); i++)
  {
    _face[i].CalcPlaneD(_vertex);
  }
}

void CMeshGeometry::NormScale()
{
  if (_validNormScale)
  {
    _vertex = _vertexNormScale;

    //scale planes
    for(int i = 0; i < _face.Size(); i++)
    {
      _face[i].CalcPlaneD(_vertex);
    }
  }
}



BOOL CMeshGeometry::IsIntersection( CGeometry& cSecond, int simCycle) 
{    
  if (!_cEnclosingRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return FALSE; // enclosing rectagles doen not overlap
  }

  switch (cSecond.GetType())
  {
  case EGT_COMPOSE:
    return cSecond.IsIntersection(*this, simCycle);
  case EGT_BOX:
    {    
      return FALSE;       
    }
  case EGT_CONVEX:
    {
      //PROFILE_SCOPE(inter2);
      FindSeparationPlane<CMeshGeometry, MPlane, MEdge> intersect;
      MPlane sepPlane;
      float dist;
      int plane1 = -1, plane2 = -1;
      float scale = -1;

      Ref<CDCacheItemData> data;
      if (cdCache.Get(GetID(), cSecond.GetID(), data))
      {
        if (GetID() == data->_geom1ID)
        {
          plane1 = data->_plane1;
          plane2 = data->_plane2;
        }
        else
        {
          plane2 = data->_plane1;
          plane1 = data->_plane2;
        }

        if (data->_scale > 0)
        {
          if (data->_scale <= IGNORANCE_SCALE)
          {
            if (data->_lastUpdate + 5 <= simCycle)
            {
              // try it maybe there is allready space
              scale = IGNORANCE_SCALE + 0.01;
            }
            else
              return FALSE;
          }
          else
            scale = data->_scale;

          Scale(data->_scale);
          cSecond.Scale(data->_scale);
        }
      }

      BOOL found = intersect(sepPlane, *this,(const CMeshGeometry&) cSecond, dist, plane1, plane2);      

      if (scale > 0)
      {
        if (found && dist > g_fTolerance)
          AdjustScaleAndSepPlaneOnDist(dist, scale, sepPlane, (CMeshGeometry&) cSecond);

        NormScale();
        cSecond.NormScale();
      }

      if (found)
      {        
        if (data.IsNull())
        {
          //not found
          data = new CDCacheItemData;
          cdCache.Add(GetID(), cSecond.GetID(), data);
        }

        data->_sepPlane = sepPlane;
        data->_lastUpdate = simCycle; 
        data->_dist = dist;
        data->_geom1ID = GetID();
        data->_plane1 = plane1;
        data->_plane2 = plane2;
        data->_scale = scale;
      }
      else
      { 
        if (data.NotNull() && data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
          // it was just a try geometries should be iqnored.
        {
          data->_lastUpdate = simCycle;
          return FALSE;
        }        
      }
      return !found;
    }
  default:
    Assert(FALSE);
  }
  return FALSE;  
}

BOOL CMeshGeometry::ForceInterFree(CGeometry& cSecond, int simCycle)
{    
  if (!_cEnclosingRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return TRUE; // enclosing rectagles does not overlap
  }

  switch (cSecond.GetType())
  {
  case EGT_COMPOSE:
    return cSecond.ForceInterFree(*this, simCycle);
  case EGT_BOX:
    {    
      return  TRUE;       
    }
  case EGT_CONVEX:
    {
      //PROFILE_SCOPE(fintfree);
      FindSeparationPlane<CMeshGeometry, MPlane, MEdge> intersect;
      MPlane sepPlane;
      float dist;
      int plane1 = -1, plane2 = -1;
      float scale = 1;

      Ref<CDCacheItemData> data;
      if (cdCache.Get(GetID(), cSecond.GetID(), data))
      {
        if (GetID() == data->_geom1ID)
        {
          plane1 = data->_plane1;
          plane2 = data->_plane2;
        }
        else
        {
          plane2 = data->_plane1;
          plane1 = data->_plane2;
        }

        if (data->_scale > 0)
        {
          if (data->_scale <= IGNORANCE_SCALE)
            return TRUE; 
          scale = data->_scale;
          Scale(data->_scale);
          cSecond.Scale(data->_scale);
        }
      }
      else
      {        
        //not found
        data = new CDCacheItemData;
        cdCache.Add(GetID(), cSecond.GetID(), data);
      }

      BOOL found = intersect(sepPlane, *this,(const CMeshGeometry&) cSecond, dist, plane1, plane2);      
      if (found)  
      {
        if(data->_scale > 0)
        {
          if (dist > g_fTolerance)
            AdjustScaleAndSepPlaneOnDist(dist, scale, sepPlane, (CMeshGeometry&) cSecond);

          NormScale();
          cSecond.NormScale();
        }

        data->_sepPlane = sepPlane;
        data->_lastUpdate = simCycle; 
        data->_dist = dist;
        data->_geom1ID = GetID();
        data->_plane1 = plane1;
        data->_plane2 = plane2;
        
        return TRUE;
      }

      // later effectove size can be bounding sphere radius etc...
      Vector3 diff = _cEnclosingRectangle.Max() - _cEnclosingRectangle.Min();
      float efectiveSize = max(diff[0], diff[1]);
      efectiveSize = max(efectiveSize, diff[2]);
      
      while(!found && scale > IGNORANCE_SCALE)
      {
        scale *= 0.90;

        Scale(scale);
        cSecond.Scale(scale);

        found = intersect(sepPlane, *this,(const CMeshGeometry&) cSecond, dist, plane1, plane2);
      }

      if (scale <= IGNORANCE_SCALE)
      {
        //ignore collisions
        data->_scale = scale;
        data->_lastUpdate = simCycle;

        NormScale();
        cSecond.NormScale();

        return FALSE;
      }
      else
      {
        if (dist > g_fTolerance)
          AdjustScaleAndSepPlaneOnDist(dist, scale, sepPlane, (CMeshGeometry&) cSecond);

        data->_sepPlane = sepPlane;
        data->_lastUpdate = simCycle; 
        data->_dist = dist;
        data->_geom1ID = GetID();
        data->_plane1 = plane1;
        data->_plane2 = plane2;
        data->_scale = scale;
     
        NormScale();
        cSecond.NormScale();

        return TRUE;
      }
    }
  default:
    Assert(FALSE);
  }
  return FALSE;  
}

void FilterPairs(M_ContactArray& cFinalContacts, const ContactArray& cFoundPairs)
{
  if (cFoundPairs.Size() == 0)
    return;

  // Find closest par.
  marina_real fSmallestDist = g_fTolerance * 2;
  Vector3 cSmallestDistNorm;

  for( int i = 0; i < cFoundPairs.Size(); i++)
  {
    if (fSmallestDist > cFoundPairs[i]._dist)
    {
      fSmallestDist = cFoundPairs[i]._dist;
      cSmallestDistNorm = cFoundPairs[i]._norm;
    }
  }
  
  for( int i = 0; i < cFoundPairs.Size(); i++)
  {
    if (cFoundPairs[i]._norm * cSmallestDistNorm > 0.98)
    {
      // Add to contacts;
      M_ContactPoint tContactPoint;
   
      tContactPoint.pos = cFoundPairs[i]._pos;
      tContactPoint.under =  g_fTolerance  - cFoundPairs[i]._dist;

      tContactPoint.dir[0] = -cSmallestDistNorm;

      // Find pedestal axes
      tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(1,0,0));
      if (tContactPoint.dir[1].Size() < 0.1f) // is new vector correct
        tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(0,1,0));

      tContactPoint.dir[2] = tContactPoint.dir[0].CrossProduct(tContactPoint.dir[1]);

      tContactPoint.dir[1].Normalize();
      tContactPoint.dir[2].Normalize();

      cFinalContacts.Add(tContactPoint);
    }
  }
}

int CMeshGeometry::FindContacts( M_ContactArray& cContactPoints, CGeometry& cSecond, int simCycle)
{
  CRectangle cEnlargedEnclRectangle(_cEnclosingRectangle);
  cEnlargedEnclRectangle.Enlarge(g_fTolerance);

  if (!cEnlargedEnclRectangle.IsIntersection(cSecond.GetEnclosingRectangle()))
  {
    return 0; // enclosing rectagles doen not overlap
  }

  switch (cSecond.GetType())
  {
  case EGT_COMPOSE:
    return cSecond.FindContacts(cContactPoints, *this, simCycle);
  case EGT_BOX:
    {
      return 0;
    }
  case EGT_CONVEX:
    {     
      //PROFILE_SCOPE(conv);
      ADD_COUNTER(conv2,1);

      Ref<CDCacheItemData> data;
      MPlane sepPlane(VZero,0);
      float scale = -1;

      if (cdCache.Get(GetID(), cSecond.GetID(), data))
      {    
        if (data->_scale > 0 && data->_scale <= IGNORANCE_SCALE)
            return 0; // ignore collision        

        if (data->_lastUpdate == simCycle-1) 
        {
          if (data->_dist > g_fTolerance)
          {
            ADD_COUNTER(nconv1,1);
            return 0;
          }
          else
          {
            sepPlane = data->_sepPlane;
            if (data->_geom1ID != GetID())
            {
              ASSERT(FALSE);
              sepPlane.SetNormal(-sepPlane.Normal(), -sepPlane.D());
            }

            if (data->_scale > 0)
            {              
              scale = data->_scale;
              Scale(data->_scale);
              cSecond.Scale(data->_scale);
            }
          }
        }
      }

      ContactArray res;
      ContactConvex<CMeshGeometry, MPlane, MFace, MEdge> cont;
      cont( res, *this, (const CMeshGeometry&) cSecond, g_fTolerance, sepPlane);

      if (scale > 0)
      {
        NormScale();
        cSecond.NormScale();
      }

      if (res.Size() == 0)
      {
        ADD_COUNTER(ncov,1);
      }
      else
      {
        ADD_COUNTER(ycov,1);
      }

      int nContacts = cContactPoints.Size();
      FilterPairs(cContactPoints, res);

      M_TBODY * pcBody1 = (_ptBody->_pcBodyPhys->GetInvMass() == 0) ? NULL : _ptBody;
      M_TBODY * pcBody2 = (cSecond.GetBody()->_pcBodyPhys->GetInvMass() == 0) ? NULL : cSecond.GetBody();

      float fRestitutionCoef = (pcBody1 == NULL) ? 
        ((pcBody2 == NULL) ? 0: pcBody2->_pcBodyPhys->GetRestitutionCoef()) : pcBody1->_pcBodyPhys->GetRestitutionCoef();

      float fFrictionCoef = (pcBody1 == NULL) ? 
        ((pcBody2 == NULL) ? 0: pcBody2->_pcBodyPhys->GetSlidingFrictionCoef()) : pcBody1->_pcBodyPhys->GetSlidingFrictionCoef();

      for(;nContacts < cContactPoints.Size(); nContacts++)
      {
        cContactPoints[nContacts].pcBody1 = pcBody1;
        cContactPoints[nContacts].pcBody2 = pcBody2;
        cContactPoints[nContacts].fRestitutionCoef = fRestitutionCoef;
        cContactPoints[nContacts].fSlidingFrictionCoef = fFrictionCoef;
      }

      return nContacts;

    }
  default:
    ASSERT(FALSE);
  }
  return -1;  
}


void CMeshGeometry::CalculateEnclosingRectangle() 
{
  if (_vertex.Size() == 0)
    return;

  Vector3 minv(_vertex[0]);
  Vector3 maxv(_vertex[0]);

  for(int i = 0; i < _vertex.Size(); i++)
  {
    saturateMin(minv[0], _vertex[i][0]);
    saturateMin(minv[1], _vertex[i][1]);
    saturateMin(minv[2], _vertex[i][2]);

    saturateMax(maxv[0], _vertex[i][0]);
    saturateMax(maxv[1], _vertex[i][1]);
    saturateMax(maxv[2], _vertex[i][2]);
  }
  _cEnclosingRectangle = CRectangle(minv,maxv);
};


void CMeshGeometry::AdjustScaleAndSepPlaneOnDist(float &dist, float& scale, MPlane& SepPlane, CMeshGeometry& cSecond)
{
  float distWanted = g_fTolerance * 0.75;
  float centerDist = -SepPlane.Distance(GeomCenter()) + SepPlane.Distance(cSecond.GeomCenter()) - dist;
  ASSERT(centerDist > 0);

 float rescale = 1 +  (dist - distWanted) / centerDist;
 scale *= rescale;

 if (scale > 1)
 {
   rescale /= scale; 
   scale = -1;
 }

 float sepDDiff = (1 - rescale) * SepPlane.Distance(GeomCenter());
 SepPlane.SetD(SepPlane.D() - sepDDiff);

 dist -= (rescale - 1) * centerDist;
}
////////////////////////////////////////////////////////////////////
//

void MFace::CalcPlane(const AutoArray<Vector3>& verPos)
{
  Assert(_n > 2);
  Vector3 normal = (verPos[_vertexes[1]] -  verPos[_vertexes[0]]).CrossProduct(verPos[_vertexes[2]] - verPos[_vertexes[0]]);
  
  normal.Normalize(); 
  
  _plane.SetNormal(normal, -normal * verPos[_vertexes[0]]);
}

void MFace::CalcPlaneD(const AutoArray<Vector3>& verPos)
{
  Assert(_n > 2);   
  _plane.SetNormal(_plane.Normal(), -_plane.Normal() * verPos[_vertexes[0]]);
}

void MFace::AddVertex(int vertex)
{
  Assert(_n < MAX_VERTEXES);
  _vertexes[_n++] = vertex;
}

void MEdge::CalcDir(const AutoArray<Vector3>& verPos)
{
  _dir = verPos[_vertexes[1]] -  verPos[_vertexes[0]];  
  _dir.Normalize();
}

void MFace::Check(const AutoArray<Vector3>& verPos)  const
{
  if (_n > 3)
  {
    Vector3 normal1 = (verPos[_vertexes[1]] -  verPos[_vertexes[0]]).CrossProduct(verPos[_vertexes[2]] - verPos[_vertexes[0]]);
    normal1.Normalize();
    Vector3 normal2 = (verPos[_vertexes[2]] -  verPos[_vertexes[0]]).CrossProduct(verPos[_vertexes[3]] - verPos[_vertexes[0]]);
    normal2.Normalize();

    ASSERT(normal1 * normal2 > 0.999);
  }  
}
 
CDCache<CDCacheItemData> cdCache; 


