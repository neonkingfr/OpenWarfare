#ifndef __MASS_H__
#define __MASS_H__

#include "Quaternion.h"

class CMass 
{
protected:
    marina_real _fMass;
    Vector3 _cMassCenter;
    Vector3 _cRelPoint;
    Matrix3 _cInertiaTensor;
public:
    CMass(): _fMass(0.0f), _cMassCenter(VZero), _cRelPoint(VZero), _cInertiaTensor(M3Zero) {};
    CMass(float fMass): _fMass(fMass), _cMassCenter(VZero), _cRelPoint(VZero), _cInertiaTensor(M3Zero) {};
    CMass(const CMass& cSecond) : _fMass(cSecond._fMass), _cMassCenter(cSecond._cMassCenter), 
        _cInertiaTensor(cSecond._cInertiaTensor), _cRelPoint(cSecond._cRelPoint) {};

    marina_real Mass() const {return _fMass;};
    const Vector3& MassCenter() const {return _cMassCenter;};
    const Vector3& RelPoint() const {return _cRelPoint;};
    const Matrix3& InertiaTensor() const {return _cInertiaTensor;};

    void SetBox(const marina_real fMass, const Vector3& cBox); // cBox represents half sizes of the box
    void SetRelPoint(const Vector3& newRelPos);
    void AddMassPoint(const marina_real mass, const Vector3& point);

    void Translate(const Vector3& delta);

    //void Rotate(const CQuaternion& cOrientation);
    
    const CMass& operator+=(const CMass& cSecond);
    const CMass& operator-=(const CMass& cSecond);
};

#endif //__MASS_H__