#ifdef _MSC_VER
#pragma once
#endif

#ifndef __QUATERNION_H__
#define __QUATERNION_H__

class QuaternionRB 
{
private:
	union 
	{
		struct {
			rb_real _w,_x,_y,_z; // TODO nice representation could be rb_real w, CVector3 v
		};
		rb_real _m[4];
	};
public:
	QuaternionRB(const QuaternionRB& cQuat): _w(cQuat._w), _x(cQuat._x), _y(cQuat._y), _z(cQuat._z) {};
	QuaternionRB(rb_real w, rb_real x, rb_real y, rb_real z): _w(w), _x(x), _y(y), _z(z) {};
	QuaternionRB(): _w(1.0f), _x(0.0f), _y(0.0f), _z(0.0f) {};
	QuaternionRB(const Vector3& cVector): _w(0.0f), _x(cVector[0]), _y(cVector[1]), _z(cVector[2]) {};
  QuaternionRB(Matrix3Par mat) {FromMatrixRotation(mat);};  
  QuaternionRB(float angleRad, const Vector3& axis) {angleRad /= 2; _w = cosf(angleRad); float s = sinf(angleRad); 
  _x = axis[0] * s; _y = axis[1] * s; _z = axis[2] * s;};


	// Set Functions
	void Set(rb_real *m) {memcpy(_m, m, 4 * sizeof(m[1]));};
	void Set(rb_real w,rb_real x,rb_real y,rb_real z) {_w = w; _x = x; _y = y; _z = z;};

	// Get Fuctions
	rb_real * Getm() {return _m;};
    const rb_real * Getm() const {return _m;};
	rb_real Getw() const {return _w;};
	rb_real Getx() const {return _x;};
	rb_real Gety() const {return _y;};
	rb_real Getz() const {return _z;};

	__forceinline void Normalize();
  __forceinline rb_real Size();

	// Operators
	__inline const QuaternionRB& operator=( const QuaternionRB& cSecond);
	__inline const QuaternionRB& operator=(  Vector3& cSecond);

	__inline QuaternionRB operator+( const QuaternionRB& cSecond) const;
	__inline QuaternionRB operator-( const QuaternionRB& cSecond) const;
	__inline QuaternionRB operator*( const QuaternionRB& cSecond) const;
	__inline QuaternionRB operator*( rb_real fSecond) const;

	__inline const QuaternionRB& operator+=( const QuaternionRB& cSecond);
	__inline const QuaternionRB& operator-=( const QuaternionRB& cSecond);
	__inline const QuaternionRB& operator*=( const QuaternionRB& cSecond);
	__inline const QuaternionRB& operator*=( rb_real fSecond);
  __inline bool operator==( const QuaternionRB& cSecond) const;

  __inline rb_real& operator[](unsigned int i) {return _m[i];};
  __inline rb_real operator[](unsigned int i) const {return _m[i];};

	// Functions
	__inline Matrix3 GetMatrix() const;
  __inline void FromMatrixRotation(Matrix3Par m);
  __inline QuaternionRB GetTransposed() const {return QuaternionRB(_w, -_x, -_y, -_z);};
};

#include "Quaternion.inl"


#endif //__QUATERNION_H__