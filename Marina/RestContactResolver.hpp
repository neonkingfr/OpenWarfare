#ifdef _MSC_VER
#pragma once
#endif

#ifndef __RESTCONTACTRESOLVER_H__
#define __RESTCONTACTRESOLVER_H__

#include "core/core.hpp"
#include "core/Dantzig.hpp"
#include "RigidBody.h"
#include "BodyGroup.h"
//#define TOLERANCE 0.01f

class RestContactResolver : public DantzigAlgorithm
{
protected:
  marina_real _fSimulationTime; 
  BodyGroup& _cGroup; 

  AutoArray<Vector3> _dirs; // Hack just till I rewrite whole marina to El/Es headres

  /** 
  * Function prepares A matrix and B vector and it initializates all staff needed in algorithm.
  */
  virtual int Initialize(BOOL & doNothing); 

  /** 
  * Not used at the moment.
  */
  virtual BOOL ChooseNewPedestalPoint(unsigned int iIndex);

  /** 
  * Function applies calculated forces onto bodies.
  * @param cForces - (in) Forces.
  */
  virtual void ApplyCalculatedForces( const Vector<marina_real>& cForces);

  virtual Vector3& ContactPointDir(const unsigned int iContactPointID, const unsigned int iIndex)
    {return _dirs[3 * iContactPointID + iIndex];};

  virtual Vector3& ContactPointDirSource(const unsigned int iContactPointID, const unsigned int iIndex)
  {return _cGroup.Contacts()[iContactPointID].dir[iIndex];};

  virtual AutoArray<int>& ContactsOfBody(const unsigned int iIndex) {return _cGroup.ContactsOfBodies()[iIndex];};

  // Debug function
  virtual void SerializeChild(FILE *fArchive);
  virtual void ForcesToArrows();

public:
  RestContactResolver(BodyGroup& cGroup): DantzigAlgorithm(), _cGroup(cGroup) {};
  void Init( marina_real fSimulationTime) { _fSimulationTime = fSimulationTime;};
};


class ImpulseContactResolver : public DantzigAlgorithm
{
protected:
  marina_real _fSimulationTime; 
  BodyGroup& _cGroup;
  AutoArray<Vector3> _dirs; // Hack just till I rewrite whole marina to El/Es headres

  /** 
  * Function prepares A matrix and B vector and it initializates all staff needed in algorithm.
  */
  virtual int Initialize(BOOL & doNothing); 

  /** 
  * Not used at the moment.
  */
  BOOL ChooseNewPedestalPoint(unsigned int iIndex);

  /** 
  * Function applies calculated impulses onto bodies.
  * @param cForces - (in) Impulses.
  */
  virtual void ApplyCalculatedForces( const Vector<marina_real>& cForces);

  virtual Vector3& ContactPointDir(const unsigned int iContactPointID, const unsigned int iIndex)
    {return _dirs[3 * iContactPointID + iIndex];};

  virtual Vector3& ContactPointDirSource(const unsigned int iContactPointID, const unsigned int iIndex)
  {return _cGroup.Contacts()[iContactPointID].dir[iIndex];};

  virtual AutoArray<int>& ContactsOfBody(const unsigned int iIndex) {return _cGroup.ContactsOfBodies()[iIndex];};
  
  // Debug function
  virtual void SerializeChild(FILE *fArchive);
  virtual void ForcesToArrows() {};

public:
  ImpulseContactResolver(BodyGroup& cGroup): DantzigAlgorithm(), _cGroup(cGroup){};
  void Init( marina_real fSimulationTime) { _fSimulationTime = fSimulationTime;};
};

// Allows contact breaking

class BreakableImpulseContactResolver : public ImpulseContactResolver
{
protected:

  SeparateGeomArray separateGeom;

  /** 
  * Function applies calculated impulses onto bodies.
  * @param cForces - (in) Impulses.
  */
  virtual void ApplyCalculatedForces( const Vector<marina_real>& cForces);

 
public:
  BreakableImpulseContactResolver(BodyGroup& cGroup): ImpulseContactResolver(cGroup) {};

  SeparateGeomArray& GetSeparateGeom() {return separateGeom;};

  virtual void ApplyCalculatedForcesFin(float coef);
};

#endif //__RESTCONTACTRESOLVER_H__