#ifndef __CDCACHE_HPP__
#define __CDCACHE_HPP__

#include <Es/Containers/sortedArray.hpp>

template <class Type>
struct CDSortedArrayTraits
{
  typedef const int &KeyType;

  //! get key
  static KeyType GetKey(const Type &type) {return type._objID;}
  //! compare two key values
  static int Compare(KeyType a, KeyType b)
  {
    if (a>b) return +1;
    if (a<b) return -1;
    return 0;
  }
};

template <class Type>
class CDCacheItem
{
public:
  int _objID;
  Ref<Type> _data;

  ClassIsMovable(CDCacheItem);
};

template <class Type>
class CDSubCacheItem
{
public:
  int _objID;
  SortedArray<CDCacheItem<Type>,MemAllocD, CDSortedArrayTraits<CDCacheItem<Type> > > * _array;

  ClassIsSimple(CDSubCacheItem);
};

template <class Type>
class CDCache : public SortedArray<CDSubCacheItem<Type>, MemAllocD, CDSortedArrayTraits<CDSubCacheItem<Type> > >
{
  typedef SortedArray<CDCacheItem<Type>,MemAllocD, CDSortedArrayTraits<CDCacheItem<Type> > > CacheItemArray;
  typedef SortedArray<CDSubCacheItem<Type>, MemAllocD, CDSortedArrayTraits<CDSubCacheItem<Type> > > base;
public:
  CDCache() {};
  virtual ~CDCache();

  void Clear();
  void Add(int ID1, int ID2, Ref<Type> data);
  bool Get(int ID1, int ID2, Ref<Type>& data);
  void Delete(int ID1, int ID2);
  void Delete(int ID);
  
};

template <class Type>
bool CDCache<Type>::Get(int ID1, int ID2, Ref<Type>& data)
{
  PROFILE_SCOPE(odeCG);
  int i = FindKey(min(ID1,ID2));
  if (i < 0)
    return false;

  CacheItemArray * array = base::operator[](i)._array;

  PROFILE_SCOPE(odeCG2);
  int j = array->FindKey(max(ID1,ID2));
  if (j < 0)
    return false;

  data = array->operator[](j)._data;
  return true;
}

template <class Type>
void CDCache<Type>::Add(int ID1, int ID2, Ref<Type> data)
{  
  CDCacheItem<Type> item;
  item._objID = max(ID1,ID2);
  item._data = data;

  int i = FindKey( min(ID1,ID2));
  if (i < 0)
  {
    // create new
    CDSubCacheItem<Type> subItem;
    subItem._objID = min(ID1,ID2);
    subItem._array = new CacheItemArray;

    subItem._array->Add(item);
    base::Add(subItem);
  }
  else
  {
    base::operator[](i)._array->Add(item);
  }
}

template <class Type>
void CDCache<Type>::Delete(int ID1, int ID2)
{
  int i = FindKey(min(ID1,ID2));
  if (i < 0)
    return;

  CacheItemArray * array = base::operator[](i)._array;
  
  array->DeleteKey(max(ID1,ID2));

  if (array->Size() == 0)
  {
    // delete row.
    delete array; 
    DeleteKey(min(ID1,ID2));  
  }
  return;
}

template <class Type>
void CDCache<Type>::Delete(int ID)
{
  int i = FindKey(ID);
  if (i > 0)
  {
    CacheItemArray * array = base::operator[](i)._array;

    // delete row.
    delete array; 
    base::DeleteKey(ID);  
  }

  for(int i = 0; i < Size(); i++)
  {
    const CDSubCacheItem<Type> & subItem = base::operator[](i);
    if (subItem._objID < ID)
    {
      subItem._array->DeleteKey(ID);
      /*if (subItem._array->Size() == 0) //TODO find elegant wayhow to go through field and clean some item
      {
        // delete row.
        delete subItem._array; 
        base::DeleteKey(subItem._objID); 
        i--;
      }*/
    }
  }
  return;
}

template <class Type>
CDCache<Type>::~CDCache()
{
  Clear();
}

template <class Type>
void CDCache<Type>::Clear()
{
//delete all arrays;
for(int i = 0; i < base::Size(); i++)
delete base::operator[](i)._array;

base::Clear();
}

#endif //__CDCACHE_HPP__