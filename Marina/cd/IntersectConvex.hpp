#ifndef __INTERSECTCONVEX_HPP_
#define __INTERSECTCONVEX_HPP_

#define __DEBUG_INTERSECTION 0
//#define NUMERICAL_ERROR 0.00001

template <class Geom, class Plane, class Edge>
class FindSeparationPlane
{
protected:
  bool IsSeparationPlane(typename const Plane& sepPlane, const typename Geom& geom1, float& dist);
public:
  bool operator () (typename Plane& sepPlane, const typename Geom& geom1, const typename Geom& geom2, float& dist, int& plane1, int& plane2);
};

template <class Geom, class Plane, class Edge>
bool FindSeparationPlane<Geom,Plane,Edge>::IsSeparationPlane(typename const Plane& sepPlane,  const typename Geom& geom, float& dist)
{
  
  dist = FLT_MAX;
  for(int i = 0; i < geom.NumVertices(); i++)
  {
    float actDist = sepPlane.Distance(geom.GetVertex(i));
    if (actDist <= 0)
    {
      dist = actDist;
      return false;
    }
    else
      if (actDist < dist)
        dist = actDist;
  }
  return true;
}


template <class Geom, class Plane, class Edge>
bool FindSeparationPlane<Geom,Plane,Edge>::operator () (typename Plane& sepPlane,  const typename Geom& geom1,  const typename Geom& geom2, float& dist, int& plane1, int& plane2)
{
#if __DEBUG_INTERSECTION
  bool bIsSeparated = false;
#endif

  Vector3 centerdiff = geom1.GeomCenter() - geom2.GeomCenter();
  PROFILE_SCOPE(odeMIs1);

  {
    //PROFILE_SCOPE(odeMIs1);
  
  // first chceck if old one is valid
  if (plane1 >= 0 )
  {
    if (plane2 < 0)
    {
      // test plane1 as a separation plane
      const typename Plane& candidate = geom1.GetPlane(plane1);
      if (candidate.Normal() * centerdiff < 0 && IsSeparationPlane(candidate, geom2, dist) && dist > NUMERICAL_ERROR)
      {
        sepPlane = candidate;
        return true;
      }
    }
    else
    {
      // edges
      const typename Edge& cand1 = geom1.GetEdge(plane1);
      Vector3Val v1 = cand1;

      const typename Edge& cand2 = geom2.GetEdge(plane2);  
      Vector3Val v2 = cand2;

      Vector3 normal = (v2).CrossProduct(v1);
      if (normal.SquareSize() > NUMERICAL_ERROR) // numerical zero...        
      {
        normal.Normalize();

        sepPlane.SetNormal(normal, -normal * geom1.GetVertex(cand1[0]));
        float dist1 = sepPlane.Distance(geom1.GeomCenter());
        if (dist1 > 0)
          sepPlane.SetNormal(-normal, -sepPlane.D());

        //dist = sepPlane.Distance(geom2.GetVertex(cand2[0]));
        float temp1, temp2;
        (void) temp1;
        (void) temp2;
        if (IsSeparationPlane(Plane(sepPlane.Normal(), sepPlane.D()), geom2, dist) && dist > NUMERICAL_ERROR && 
          IsSeparationPlane(Plane(-sepPlane.Normal(), -sepPlane.D()), geom1,  temp2))
        {
          return true;
        }
      }
    }
  }
  else
  {
    if (plane2 >= 0)
    {
      // test plane2 as a separation plane
      const typename Plane& candidate = geom2.GetPlane(plane2);
      if (candidate.Normal() * centerdiff > 0 && IsSeparationPlane(candidate, geom1, dist) && dist > NUMERICAL_ERROR)
      {
        sepPlane.SetNormal(-candidate.Normal(), -candidate.D() + dist);
        return true;
      }
    }
  }
  }

  //PROFILE_SCOPE(odeMis2);

  // check faces first
  for(int i = 0; i < geom1.NFaces(); i++)
  {
    const typename Plane& candidate = geom1.GetPlane(i);
    if (candidate.Normal() * centerdiff < 0 && IsSeparationPlane(candidate, geom2, dist) && dist > NUMERICAL_ERROR)
    {
      plane1 = i;
      plane2 = -1;
      sepPlane = candidate;
      return true;
    }
  }

  for(int i = 0; i < geom2.NFaces(); i++)
  {
    const typename Plane& candidate = geom2.GetPlane(i);
    if (candidate.Normal() * centerdiff > 0 && IsSeparationPlane(candidate, geom1, dist) && dist > NUMERICAL_ERROR)
    {
      plane1 = -1;
      plane2 = i;
      sepPlane.SetNormal(-candidate.Normal(), -candidate.D() + dist);
      return true;
    }
  }


  //PROFILE_SCOPE(odeMis3);
  //ADD_COUNTER(odeMis31,1);
  //OK no face is separation plane check edge2edge
  for(int i = 0; i < geom1.NEdges(); i++)
  {
    const typename Edge& cand1 = geom1.GetEdge(i);
    
    Vector3Val v1 = cand1;
    Vector3Val norm11 = geom1.GetPlane(cand1.GetFace(0)).Normal();
    Vector3Val norm12 = geom1.GetPlane(cand1.GetFace(1)).Normal();

    if (norm11 * centerdiff >=0 && norm12 * centerdiff >=0)
      continue;

    if (norm11 * norm12 > 0.999f)
      continue; //not the real edge.

    //Vector3 planeVec1 = ((Vector3Val) cand1).CrossProduct(norm1);
    //Vector3 planeVec2 = -((Vector3Val) cand1).CrossProduct(norm2);

    for(int j = 0; j < geom2.NEdges(); j++)
    {
      const typename Edge& cand2 = geom2.GetEdge(j);  

      Vector3Val norm21 = geom2.GetPlane(cand2.GetFace(0)).Normal();
      Vector3Val norm22 = geom2.GetPlane(cand2.GetFace(1)).Normal();

      if (norm21 * norm22 > 0.999f)
        continue; //not the real edge.

#if __DEBUG_INTERSECTION
      {

        //whaw maybe separation plane
        Vector3 normal = ((Vector3Val) cand2).CrossProduct(cand1);
        if (normal.SquareSize() < 0.000001f) // numerical zero...        
          continue;

        normal.Normalize();

        sepPlane.SetNormal(normal, -normal * geom1.GetVertex(cand1[0]));
        float dist1 = sepPlane.Distance(geom1.GeomCenter());
        if (dist1 > 0)
          sepPlane.SetNormal(-normal, -sepPlane.D());
      }
#endif 
      Vector3Val v2 = cand2;
      //Vector3 w2 = v2 - v1 * (v1 * v2);
      //w2.Normalize();

      //float val = (w2 - planeVec1) * (w2 - planeVec2);
      float val = (v2 * norm11) * (v2 * norm12);
      if (val > 0) //val < 0 /*|| (val == 0 && planeVec1 * planeVec2 < 0)*/)
      {
#if __DEBUG_INTERSECTION
        float temp;
        if (!bIsSeparated)
          bIsSeparated = IsSeparationPlane(sepPlane, geom2, temp) && temp > NUMERICAL_ERROR && 
          IsSeparationPlane(Plane(-sepPlane.Normal(), -sepPlane.D()+0.0001), geom1,  temp) && temp > NUMERICAL_ERROR;
#endif
        continue;
      }

      if ((v1 * norm21) * (v1 * norm22) > 0) //val < 0 /*|| (val == 0 && planeVec1 * planeVec2 < 0)*/)
        continue;

      /*val = (w2 + planeVec1) * (w2 + planeVec2);
      if (val < 0 )
      {
#if __DEBUG_INTERSECTION
        float temp;
        if (!bIsSeparated)
          bIsSeparated = IsSeparationPlane(sepPlane, geom2, temp) && temp > NUMERICAL_ERROR &&
          IsSeparationPlane(Plane(-sepPlane.Normal(), -sepPlane.D()+0.0001), geom1, temp) && temp > NUMERICAL_ERROR;
#endif
        continue;
      }*/

      //Vector3 planeVec21 = ((Vector3Val) cand2).CrossProduct(norm21);
      //Vector3 planeVec22 = -((Vector3Val) cand2).CrossProduct(norm22);

      //Vector3 w1 = v1 - v2 * (v1 * v2);
      //w1.Normalize();
      //val = (w1 - planeVec21) * (w1 -  planeVec22);

      //if (val >= 0 && (w1 + planeVec21) * (w1 + planeVec22) >= 0/*|| (val == 0 && planeVec21 * planeVec22 >= 0)*/)
      {
        //whaw maybe separation plane
        Vector3 normal = ((Vector3Val) cand2).CrossProduct(cand1);
        if (normal.SquareSize() < 0.000001f) // numerical zero...        
          continue;
        
        normal.Normalize();
         
        sepPlane.SetNormal(normal, -normal * geom1.GetVertex(cand1[0]));       

        float dist1 = sepPlane.Distance(geom1.GeomCenter());
        if (dist1 > 0)
          sepPlane.SetNormal(-normal, -sepPlane.D());

        dist = sepPlane.Distance(geom2.GetVertex(cand2[0]));
        //if (dist < 0)
        //{
        //  sepPlane.SetNormal(-normal, -sepPlane.D());
        //  dist *= -1;
        //}

        if ( dist > NUMERICAL_ERROR && dist <= sepPlane.Distance(geom2.GeomCenter()))          
        {          
#if __DEBUG_INTERSECTION
          float temp1,temp2;
          (void) temp1; (void) temp2;
          Assert(IsSeparationPlane(sepPlane, geom2, temp1) && temp1 > NUMERICAL_ERROR);
          //geom2.CheckFaces();
          //IsSeparationPlane(sepPlane, geom2, temp1);
          Assert(IsSeparationPlane(Plane(-sepPlane.Normal(), -sepPlane.D() + dist), geom1, temp2) && temp2 > NUMERICAL_ERROR);          
#endif
          plane1 = i;
          plane2 = j;
          return true;
        }
      }
   /*   else
      {
#if __DEBUG_INTERSECTION
        float temp;
        if (!bIsSeparated)
          bIsSeparated = IsSeparationPlane(sepPlane, geom2, temp) && temp > NUMERICAL_ERROR &&
          IsSeparationPlane(Plane(-sepPlane.Normal(), -sepPlane.D()+0.0001), geom1, temp) && temp > NUMERICAL_ERROR;
#endif
      }*/
    }
  }  
#if __DEBUG_INTERSECTION
  Assert(!bIsSeparated);
#endif
  //ADD_COUNTER(odeMis32,1);
  return false;
}
#endif