#ifndef __CONTACTCONVEX_HPP__
#define __CONTACTCONVEX_HPP__

struct CDContactPair
{
  Vector3 _pos;
  Vector3 _norm;
  float _dist;
  int _triangle[2]; //used just for landcontacts
};
TypeIsSimple(CDContactPair)
typedef AutoArray<CDContactPair> CDContactArray;

#define __USE_2D_BB 0
#if __USE_2D_BB
// 2D Axis Aligned box 
class AABox2D
{
protected:
  Vector3 _cMax;
  Vector3 _cMin;

public:  
  AABox2D() {}; 

  void Create(const Vector3& v1, const Vector3& v2) 
  {
    if (v1[0] < v2[0])
    {
      _cMin[0] = v1[0];
      _cMax[0] = v2[0];
    }
    else
    {
      _cMin[0] = v2[0];
      _cMax[0] = v1[0];
    }

    if (v1[1] < v2[1])
    {
      _cMin[1] = v1[1];
      _cMax[1] = v2[1];
    }
    else
    {
      _cMin[1] = v2[1];
      _cMax[1] = v1[1];
    }
  }

  bool IsIntersection(const AABox2D& cSecond) const
  {
    return !(_cMax[0] < cSecond._cMin[0] || _cMin[0] > cSecond._cMax[0] || 
      _cMax[1] < cSecond._cMin[1] || _cMin[1] > cSecond._cMax[1]);
  }

  void Enlarge(float fSize)
  {
    _cMin[0] -= fSize;
    _cMin[1] -= fSize;

    _cMax[0] += fSize;
    _cMax[1] += fSize;
  }

  const Vector3& Min() const {return _cMin;};
  const Vector3& Max() const {return _cMax;};  
};

#endif
typedef struct 
{
#if __USE_2D_BB
  AABox2D _bb;
#endif
  int _i;
} EdgeDesc;
TypeIsSimple(EdgeDesc);

template <class Geom, class Plane, class Face, class Edge>
class ContactConvex
{
protected:
public:
  void operator() (CDContactArray& found,const Geom& geom1,const  Geom& geom2, float epsilon, Plane& sepPlane);
  void operator() (CDContactArray& found,const Geom& geom1,const  Geom& geom2, float epsilon, Plane& sepPlane, int plane1, int& plane2);
};

template <class Geom, class Plane, class Face, class Edge>
void ContactConvex<Geom, Plane, Face, Edge>::operator() (CDContactArray& found, const Geom& geom1, const Geom& geom2, float epsilon, Plane& sepPlane)
{
  START_PROFILE(odeCC);

  bool useSepPlane = sepPlane.Normal() != VZero;
  //useSepPlane = false;
#ifdef _DEBUG
  float f = sepPlane.Distance(geom2.GetVertex(0));
  (void) f;
  Assert(!useSepPlane || sepPlane.Distance(geom2.GetVertex(0)) > 0); //num barier
#endif

  //START_PROFILE(cdcCP);
  static AutoArray<bool> closePoints1;
  //closePoints1.Reserve(geom1.NumVertices());
  closePoints1.Resize(geom1.NumVertices());

  static AutoArray<bool> closePoints2;  
  //closePoints2.Realloc(geom2.NumVertices());
  closePoints2.Resize(geom2.NumVertices());

  for (int i = 0; i < geom2.NumVertices(); i++)
  {
    if (useSepPlane)
    {
      Vector3Val point = geom2.GetVertex(i);      
      closePoints2[i] = sepPlane.Distance(point) < epsilon;        
    }
    else
      closePoints2[i] = true;
  }
   
  for (int i = 0; i < geom1.NumVertices(); i++)
  {
    if (useSepPlane)
    {
      Vector3Val point = geom1.GetVertex(i);      
      closePoints1[i] = (-sepPlane.Distance(point)) < epsilon;        
    }
    else
      closePoints1[i] = true;
  }
  //END_PROFILE(cdcCP);
  // Point to face
  //START_PROFILE(cdcP1);
  for(int i = 0; i < geom1.NFaces(); i++)
  {
    //PROFILE_SCOPE(cdcP1);
    typename const Face& face = geom1.GetFace(i);
    typename const Plane& plane = face;

    if (useSepPlane)
    {
      // do not take planes from other side.
      if (sepPlane.Normal() * plane.Normal() < 0)
        continue;

      /*// plane must have point at least epsilon close to sepPlane.
      int l = 0;
      for(; l < face.N(); l++)
      {
        if (- sepPlane.Distance(geom1.GetVertex(face[l])) < epsilon)
          break;
      }

      if (l == face.N())
        continue;
        */
      
    }
    Vector3Val normal = plane.Normal();

    for (int j = 0; j < geom2.NumVertices(); j++)
    {
      if (!closePoints2[j])
        continue;

      Vector3Val point = geom2.GetVertex(j);
      float dist = plane.Distance(point);
      if (dist < 0)
        break;
      
      if (dist > epsilon)
        continue;

      // sepPlane 
      //if (useSepPlane && sepPlane.Distance(point) > epsilon)
      //  continue; 

      // is point over face?      
      int l = 0;
      const Vector3 * st = &geom1.GetVertex(face[face.N()-1]);
      
      for(; l < face.N(); l++)
      {
        Vector3Val en = geom1.GetVertex(face[l]);
        Vector3 edgeDir = en - *st;
        st = &en;
        Vector3 normalEdge = normal.CrossProduct(edgeDir);
        
        if (normalEdge * (point - en) <= 0)
          break;
      }

      if (l == face.N())
      { // found point
        CDContactPair& pair = found.Append(); 
        pair._norm = normal;
        pair._pos = point;
        pair._dist = dist; 
        pair._triangle[0] = 0; // just for debbuging
      }
    }
  }
  //END_PROFILE(cdcP1);
  //START_PROFILE(cdcP2);

  for(int i = 0; i < geom2.NFaces(); i++)
  {
    //PROFILE_SCOPE(cdcP2);
    typename const Face& face = geom2.GetFace(i);
    typename const Plane& plane = face;

    if (useSepPlane)
    {
      // do not take planes from other side.
      if (sepPlane.Normal() * plane.Normal() > 0)
        continue;

      // plane must have point at least g_fTolerance close to sepPlane.
      /*int l = 0;
      for(; l < face.N(); l++)
      {
      if ( sepPlane.Distance(geom1.GetVertex(face[l])) < epsilon)
      break;
      }

      if (l == face.N())
      continue;
      */
    }
    Vector3Val normal = plane.Normal();

    for (int j = 0; j < geom1.NumVertices(); j++)
    {      
      if (!closePoints1[j])
        continue;

      Vector3Val point = geom1.GetVertex(j);
      float dist = plane.Distance(point);
      if (dist < 0)
        break;
        
      if (dist > epsilon)
        continue;

      //if (useSepPlane && -sepPlane.Distance(point) > epsilon)
      //  continue; ;

      // is point over face?
      int l = 0;
      const Vector3 * st = &geom2.GetVertex(face[face.N()-1]);

      for(; l < face.N(); l++)
      {
        Vector3Val en = geom2.GetVertex(face[l]);
        Vector3 edgeDir = en - *st;
        st = &en;
        Vector3 normalEdge = normal.CrossProduct(edgeDir);

        if (normalEdge * (point - en) <= 0)
          break;
      }     

      if (l == face.N())
      { // found point
        CDContactPair& pair = found.Append(); 
        pair._norm = -normal;
        pair._pos = point + normal * dist;
        pair._dist = dist;  
        pair._triangle[0] = 0; 
      }
    }
  }
  //END_PROFILE(cdcP2);
  //START_PROFILE(cdcEE);
    
  // Edge to edge
  //PROFILE_SCOPE(cdcE1);
  //AutoArray<int> edges2; // suspected edges from geom2
  static AutoArray<EdgeDesc> edges2;  
  edges2.Reserve(geom2.NEdges(),geom2.NEdges());
  edges2.Resize(0);

  //PROFILE_SCOPE(cdcE2);

#if __USE_2D_BB  
  Vector3 sepPlaneDir1;
  Vector3 sepPlaneDir2;

  if (useSepPlane)
  {
    Vector3Val normal = sepPlane.Normal();
    Vector3 vec(1,0,0);
    float coherence = vec * sepPlaneDir1; 
    if (fabs(coherence) > 0.9f) 
      sepPlaneDir1 = normal.CrossProduct(vec);
    else
      sepPlaneDir1 = normal.CrossProduct(Vector3(0,1,0));

    sepPlaneDir1.Normalize();
    sepPlaneDir2 = normal.CrossProduct(sepPlaneDir1);
  }
#endif
  for(int j = 0; j < geom2.NEdges(); j++)
  {
    typename const Edge& cand2 = geom2.GetEdge(j);
    if (!closePoints2[cand2[0]] || !closePoints2[cand2[1]])
      continue;

    //Vector3Val pos21 = geom2.GetVertex(cand2[0]);
    //Vector3Val pos22 = geom2.GetVertex(cand2[1]);


    //if (useSepPlane && sepPlane.Distance(pos22) > epsilon && sepPlane.Distance(pos21) > epsilon)
    //  continue;

    /*Vector3Val norm21 = geom2.GetPlane(cand2.GetFace(0)).Normal();
    Vector3Val norm22 = geom2.GetPlane(cand2.GetFace(1)).Normal();
    if (norm21 * norm22 > 0.999f)
      continue; // not real edge

    if (useSepPlane)
    {
      if (norm21 * sepPlane.Normal() > 0 && norm22 * sepPlane.Normal() > 0)
        continue;
    }*/

    EdgeDesc& desc = edges2.Append();
    desc._i = j;
#if __USE_2D_BB 
    if (useSepPlane)
      desc._bb.Create(Vector3(pos21 * sepPlaneDir1, pos21 * sepPlaneDir2, 0), Vector3(pos22 * sepPlaneDir1, pos22 * sepPlaneDir2, 0));
#endif
  }

  if (edges2.Size() == 0)
    return;

  //ADD_COUNTER(cdcEA,geom1.NEdges() * geom2.NEdges());

  float epsilon2 = epsilon * epsilon;
  for(int i = 0; i < geom1.NEdges(); i++)
  {    
    typename const Edge& cand1 = geom1.GetEdge(i);
    if (!closePoints1[cand1[0]] || !closePoints1[cand1[1]])
      continue;

    Vector3Val pos11 = geom1.GetVertex(cand1[0]);
    Vector3Val pos12 = geom1.GetVertex(cand1[1]);

    /*if (useSepPlane)
    {
      if (-sepPlane.Distance(pos12) > epsilon && -sepPlane.Distance(pos11) > epsilon)
        continue;
    }*/

    //START_PROFILE(cdcEHT);
    //START_PROFILE(cdcEH);

    /*Vector3Val norm11 = geom1.GetPlane(cand1.GetFace(0)).Normal();
    Vector3Val norm12 = geom1.GetPlane(cand1.GetFace(1)).Normal();
    if (norm11 * norm12 > 0.999f)
      continue; // not real edge

    if (useSepPlane)
    {
      if (norm11 * sepPlane.Normal() < 0 && norm12 * sepPlane.Normal() < 0)
        continue;
    }*/

    Vector3Val v1 = cand1;
#if __USE_2D_BB 
    AABox2D bb2;    
    if (useSepPlane)
    {
      bb2.Create(Vector3(pos11 * sepPlaneDir1, pos11 * sepPlaneDir2, 0), Vector3(pos12 * sepPlaneDir1, pos12 * sepPlaneDir2, 0));
      bb2.Enlarge(epsilon);
    }
#endif

    /*float v1pos11 = v1 * pos11;
    float v1pos12 = v1 * pos12;*/

    float v1pos11 = v1 * pos11;
    float v1pos12 = v1 * pos12;
    

    for(int j = 0; j < edges2.Size(); j++)
    {
      //ADD_COUNTER(cdcE1,1);
      typename const Edge& cand2 = geom2.GetEdge(edges2[j]._i);

#if __USE_2D_BB 
      if (!bb2.IsIntersection(edges2[j]._bb))
      {
        //ADD_COUNTER(cdcE8,1);
        continue;
      }
#endif
      //Vector3Val norm21 = geom2.GetPlane(cand2.GetFace(0)).Normal();
      //Vector3Val norm22 = geom2.GetPlane(cand2.GetFace(1)).Normal();      

      /*if (useSepPlane)
      {
        if (norm21 * sepPlane.Normal() > 0 && norm22 * sepPlane.Normal() > 0)
          continue;
      }*/

      Vector3Val pos21 = geom2.GetVertex(cand2[0]);
      Vector3Val pos22 = geom2.GetVertex(cand2[1]);

      /*if (useSepPlane)
      {
        if (sepPlane.Distance(pos22) > epsilon && sepPlane.Distance(pos21) > epsilon)
          continue;
      }*/

      //PROFILE_SCOPE(hearte2e);

      Vector3Val v2 = cand2;
      
      /*if ((v1 * (pos21 - pos11) <= 0 && v1 * (pos22 - pos11) <= 0) ||
        (v1 * (pos21 - pos12) >= 0 && v1 * (pos22 - pos12) >= 0))
        continue;*/
      float v1pos21 = v1 * pos21;
      float v1pos22 = v1 * pos22;

      if ((v1pos21 <= v1pos11 && v1pos22 <= v1pos11) ||
        (v1pos21 >= v1pos12 && v1pos22 >= v1pos12))
      {
        //ADD_COUNTER(cdcE7,1);
        continue;
      }

      float v2pos11 = v2 * pos11;
      float v2pos12 = v2 * pos12;
      float v2pos21 = v2 * pos21;
      float v2pos22 = v2 * pos22;
      
      /*if ((v2*(pos11 - pos21) <= 0 && v2 * (pos12 - pos21) <= 0) ||
        (v2 * (pos11 - pos22) >= 0 && v2 * (pos12 - pos22) >= 0))*/
      if ((v2pos11 <= v2pos21 && v2pos12 <= v2pos21) ||
        (v2pos11 >= v2pos22 && v2pos12 >= v2pos22))
      {
        //ADD_COUNTER(cdcE7,1);
        continue;
      }


      //PROFILE_SCOPE(hearte3e);
      //PROFILE_SCOPE(hearte4e);

      //ADD_COUNTER(cdcE2,1);

      //START_PROFILE(cdcEH);

      Vector3 normal = v1.CrossProduct(v2);
      float sqrSize = normal.SquareSize();
      if (sqrSize > 0.0001f) // numerical barier vectors are paralel
      {
        //normal.Normalize();
        float dist = normal * (pos22 - pos11);
        if (dist * dist > epsilon2 * sqrSize)
        {
          //ADD_COUNTER(cdcE9,1);
          // distance between lines is more than epsilon
          continue;
        }            

        // diffnorm * v1 = 0 && diffnorm belongs to plane def by v1 and v2
        /*Vector3 diffNorm = v2 - v1 * (v1 * v2);

        float diffNormPos11 = diffNorm * pos11;
        float t1 = diffNorm * pos21 - diffNormPos11;
        float t2 = diffNorm * pos22 - diffNormPos11;*/
        
        float v1v2 = v1 * v2;        
        float t1 = (v2pos21 - v2pos11) - (v1pos21 - v1pos11) * v1v2;
        float t2 = (v2pos22 - v2pos11) - (v1pos22 - v1pos11) * v1v2;
        
        if (t1 * t2 > 0)
        {
          // both points on the same side
          //ADD_COUNTER(cdcE6,1);
          continue;
        }   
      
        float invT = 1.0f/ (t1 - t2);

       // Vector3 resPos2 = (-t2 * invT * pos21 + t1 * invT * pos22) ;        
       // float posOnV1 = v1 * resPos2;
        float posOnV1 = (t1 * v1pos22 - t2 * v1pos21) * invT ;

        if ( posOnV1 <= v1pos11 || posOnV1 >= v1pos12)
        {
          //ADD_COUNTER(cdcE6,1);
          continue;
        }
       
        //ADD_COUNTER(cdcE4,1);
        CDContactPair& pair = found.Append(); 
        float invSize = 1.0f / normal.Size();
        if (dist < 0)
        {
          invSize *= -1;          
        }

        pair._norm = normal * invSize;
        pair._pos = (-t2 * invT * pos21 + t1 * invT * pos22);
        pair._dist = dist * invSize;
        pair._triangle[0] = 1; // just for debugging
      }
      else
      {
        //parallel
        normal = (pos21 - pos11);
        normal = normal - v1 * (normal * v1);

        float dist = normal.Size();

        normal /= dist;
        if (dist > epsilon)
          continue;

        //ADD_COUNTER(cdcE3,1);

        float proj11 = v1 * pos11;
        float proj12 = v1 * pos12;

        float proj21 = v1 * pos21;
        float proj22 = v1 * pos22;

        if (proj21 < proj22)
        {
          if (proj11 != proj21)
          {
            CDContactPair& pair = found.Append(); 
            pair._norm = normal;        
            pair._dist = dist;
            pair._triangle[0] = 2; // just for debugging

            if (proj21 > proj11)
              pair._pos = pos21 - normal * dist + pos11;
            else
              pair._pos = pos11;
          }

          CDContactPair& pair2 = found.Append(); 
          pair2._norm = normal;        
          pair2._dist = dist;
          pair2._triangle[0] = 2; // just for debugging

          if (proj22 < proj12)
            pair2._pos = pos22 - normal * dist;
          else
            pair2._pos = pos12;
        }
        else
        {
          if (proj11 != proj22)
          {
            CDContactPair& pair = found.Append(); 
            pair._norm = normal;        
            pair._dist = dist;
            pair._triangle[0] = 2; // just for debugging

            if (proj22 > proj11)
              pair._pos = pos22 - normal * dist;
            else
              pair._pos = pos11;
          }

          CDContactPair& pair2 = found.Append(); 
          pair2._norm = normal;        
          pair2._dist = dist;
          pair2._triangle[0] = 2; // just for debugging

          if (proj21 < proj12)
            pair2._pos = pos21 - normal * dist;
          else
            pair2._pos = pos12;
        }
      }      
    }
    //END_PROFILE(cdcEH);
    //END_PROFILE(cdcEHT);
  }

  //END_PROFILE(cdcEE);
  END_PROFILE(odeCC);

  return;
}


template <class Geom, class Plane, class Face, class Edge>
void ContactConvex<Geom, Plane, Face, Edge>::operator() (CDContactArray& found, const Geom& geom1, const Geom& geom2, float epsilon, Plane& sepPlane, int plane1, int& plane2)
{
  START_PROFILE(odeCC2);

  //START_PROFILE(cdcCP);
  static AutoArray<bool> closePoints1;
  //closePoints1.Reserve(geom1.NumVertices());
  closePoints1.Resize(geom1.NumVertices());

  static AutoArray<bool> closePoints2;  
  //closePoints2.Realloc(geom2.NumVertices());
  closePoints2.Resize(geom2.NumVertices());

  if (plane1 >= 0 && plane2 < 0)
  {
    // plane1 is separation plane... 
    for (int i = 0; i < geom2.NumVertices(); i++)
    {     
      Vector3Val point = geom2.GetVertex(i);      
      closePoints2[i] = sepPlane.Distance(point) < epsilon;             
    }
      
    // is any point over face plane1
    typename const Face& face1 = geom1.GetFace(plane1);
    typename const Plane& plane = face1;
      
    Vector3Val normal = plane.Normal();

    for (int j = 0; j < geom2.NumVertices(); j++)
    {
      if (!closePoints2[j])
        continue;

      Vector3Val point = geom2.GetVertex(j);
      float dist = plane.Distance(point);
      if (dist < 0)
        break;

      if (dist > epsilon)
        continue;

      // is point over face?      
      int l = 0;
      const Vector3 * st = &geom1.GetVertex(face1[face1.N()-1]);

      for(; l < face1.N(); l++)
      {
        Vector3Val en = geom1.GetVertex(face1[l]);
        Vector3 edgeDir = en - *st;
        st = &en;
        Vector3 normalEdge = normal.CrossProduct(edgeDir);

        if (normalEdge * (point - en) <= 0)
          break;
      }

      if (l == face1.N())
      { // found point
        CDContactPair& pair = found.Append(); 
        pair._norm = normal;
        pair._pos = point;
        pair._dist = dist; 
        pair._triangle[0] = 0; // just for debbuging
      }
    }

    // are points from plane1 over faces from 2
    for(int i = 0; i < geom2.NFaces(); i++)
    {
      //PROFILE_SCOPE(cdcP2);
      typename const Face& face = geom2.GetFace(i);
      typename const Plane& plane = face;
    
      // do not take planes from other side.
      if (sepPlane.Normal() * plane.Normal() > 0)
        continue;

      Vector3Val normal = plane.Normal();

      for (int j = 0; j < face1.N(); j++)
      {              
        Vector3Val point = geom1.GetVertex(face1[j]);
        float dist = plane.Distance(point);
        if (dist < 0)
          break;

        if (dist > epsilon)
          continue;

        // is point over face?
        int l = 0;
        const Vector3 * st = &geom2.GetVertex(face[face.N()-1]);

        for(; l < face.N(); l++)
        {
          Vector3Val en = geom2.GetVertex(face[l]);
          Vector3 edgeDir = en - *st;
          st = &en;
          Vector3 normalEdge = normal.CrossProduct(edgeDir);

          if (normalEdge * (point - en) <= 0)
            break;
        }     

        if (l == face.N())
        { // found point
          CDContactPair& pair = found.Append(); 
          pair._norm = -normal;
          pair._pos = point + normal * dist;
          pair._dist = dist;  
          pair._triangle[0] = 0; 
        }
      }
    }

    // edges... 
    float epsilon2 = epsilon * epsilon;
    for(int i = 0; i < face1.NEdges(); i++)
    {
      typename const Edge& cand1 = geom1.GetEdge(face1.Edge(i));     

      Vector3Val pos11 = geom1.GetVertex(cand1[0]);
      Vector3Val pos12 = geom1.GetVertex(cand1[1]);

      Vector3Val v1 = cand1;
      float v1pos11 = v1 * pos11;
      float v1pos12 = v1 * pos12;

      for(int j = 0; j < geom2.NEdges(); j++)
      {
        //ADD_COUNTER(cdcE1,1);
        typename const Edge& cand2 = geom2.GetEdge(j);

        if (!closePoints2[cand2[0]] || !closePoints2[cand2[1]])
          continue;

        Vector3Val pos21 = geom2.GetVertex(cand2[0]);
        Vector3Val pos22 = geom2.GetVertex(cand2[1]);

        Vector3Val v2 = cand2;
        
        float v1pos21 = v1 * pos21;
        float v1pos22 = v1 * pos22;

        if ((v1pos21 <= v1pos11 && v1pos22 <= v1pos11) ||
          (v1pos21 >= v1pos12 && v1pos22 >= v1pos12))
        {
          continue;
        }

        float v2pos11 = v2 * pos11;
        float v2pos12 = v2 * pos12;
        float v2pos21 = v2 * pos21;
        float v2pos22 = v2 * pos22;


        if ((v2pos11 <= v2pos21 && v2pos12 <= v2pos21) ||
          (v2pos11 >= v2pos22 && v2pos12 >= v2pos22))
        {
          //ADD_COUNTER(cdcE7,1);
          continue;
        }

        Vector3 normal = v1.CrossProduct(v2);
        float sqrSize = normal.SquareSize();
        if (sqrSize > 0.0001f) // numerical barier vectors are paralel
        {
          //normal.Normalize();
          float dist = normal * (pos22 - pos11);
          if (dist * dist > epsilon2 * sqrSize)
          {            
            // distance between lines is more than epsilon
            continue;
          }            
          float v1v2 = v1 * v2;        
          float t1 = (v2pos21 - v2pos11) - (v1pos21 - v1pos11) * v1v2;
          float t2 = (v2pos22 - v2pos11) - (v1pos22 - v1pos11) * v1v2;

          if (t1 * t2 > 0)
          {
            // both points on the same side
            //ADD_COUNTER(cdcE6,1);
            continue;
          }   

          float invT = 1.0f/ (t1 - t2);
          
          float posOnV1 = (t1 * v1pos22 - t2 * v1pos21) * invT ;

          if ( posOnV1 <= v1pos11 || posOnV1 >= v1pos12)
          {
            //ADD_COUNTER(cdcE6,1);
            continue;
          }

          //ADD_COUNTER(cdcE4,1);
          CDContactPair& pair = found.Append(); 
          float invSize = 1.0f / normal.Size();
          if (dist < 0)
          {
            invSize *= -1;          
          }

          pair._norm = normal * invSize;
          pair._dist = dist * invSize;
          pair._pos = (-t2 * invT * pos21 + t1 * invT * pos22);         
          pair._triangle[0] = 1; // just for debugging
        }
        else
        {
          //parallel
          normal = (pos21 - pos11);
          normal = normal - v1 * (normal * v1);

          float dist = normal.Size();

          normal /= dist;
          if (dist > epsilon)
            continue;

          //ADD_COUNTER(cdcE3,1);

          float proj11 = v1 * pos11;
          float proj12 = v1 * pos12;

          float proj21 = v1 * pos21;
          float proj22 = v1 * pos22;

          if (proj21 < proj22)
          {
            if (proj11 != proj21)
            {
              CDContactPair& pair = found.Append(); 
              pair._norm = normal;        
              pair._dist = dist;
              pair._triangle[0] = 2; // just for debugging

              if (proj21 > proj11)
                pair._pos = pos21 - normal * dist;
              else
                pair._pos = pos11;
            }

            CDContactPair& pair2 = found.Append(); 
            pair2._norm = normal;        
            pair2._dist = dist;
            pair2._triangle[0] = 2; // just for debugging

            if (proj22 < proj12)
              pair2._pos = pos22 - normal * dist;
            else
              pair2._pos = pos12;
          }
          else
          {
            if (proj11 != proj22)
            {
              CDContactPair& pair = found.Append(); 
              pair._norm = normal;        
              pair._dist = dist;
              pair._triangle[0] = 2; // just for debugging

              if (proj22 > proj11)
                pair._pos = pos22 - normal * dist;
              else
                pair._pos = pos11;
            }

            CDContactPair& pair2 = found.Append(); 
            pair2._norm = normal;        
            pair2._dist = dist;
            pair2._triangle[0] = 2; // just for debugging

            if (proj21 < proj12)
              pair2._pos = pos21 - normal * dist;
            else
              pair2._pos = pos12;
          }
        }      
      }
    }

    return;
  }

  if (plane1 < 0 && plane2 >= 0)
  {
    for (int i = 0; i < geom1.NumVertices(); i++)
    {
      Vector3Val point = geom1.GetVertex(i);      
      closePoints1[i] = (-sepPlane.Distance(point) ) < epsilon;        
    }

    // plane2 is separation plane... 
    typename const Face& face2 = geom2.GetFace(plane2);
    typename const Plane& plane2 = face2;

    for(int i = 0; i < geom1.NFaces(); i++)
    {
      //PROFILE_SCOPE(cdcP1);
      typename const Face& face = geom1.GetFace(i);
      typename const Plane& plane = face;

      // do not take planes from other side.
      if (sepPlane.Normal() * plane.Normal() < 0)
        continue;      

      Vector3Val normal = plane.Normal();
      for (int j = 0; j < face2.N(); j++)
      {
       
        Vector3Val point = geom2.GetVertex(face2[j]);
        float dist = plane.Distance(point);

        if (dist < 0)
          break;

        if (dist > epsilon)
          continue;         

        // is point over face?      
        int l = 0;
        const Vector3 * st = &geom1.GetVertex(face[face.N()-1]);

        for(; l < face.N(); l++)
        {
          Vector3Val en = geom1.GetVertex(face[l]);
          Vector3 edgeDir = en - *st;
          st = &en;
          Vector3 normalEdge = normal.CrossProduct(edgeDir);

          if (normalEdge * (point - en) <= 0)
            break;
        }

        if (l == face.N())
        { // found point
          CDContactPair& pair = found.Append(); 
          pair._norm = normal;
          pair._pos = point;
          pair._dist = dist; 
          pair._triangle[0] = 0; // just for debbuging
        }
      }
    }

    Vector3Val normal = plane2.Normal();

    for (int j = 0; j < geom1.NumVertices(); j++)
    { 
      if (!closePoints1[j])
        continue;

      Vector3Val point = geom1.GetVertex(j);
      float dist = plane2.Distance(point);
      if (dist < 0)
        break;

      if (dist > epsilon)
        continue;

      // is point over face?
      int l = 0;
      const Vector3 * st = &geom2.GetVertex(face2[face2.N()-1]);

      for(; l < face2.N(); l++)
      {
        Vector3Val en = geom2.GetVertex(face2[l]);
        Vector3 edgeDir = en - *st;
        st = &en;
        Vector3 normalEdge = normal.CrossProduct(edgeDir);

        if (normalEdge * (point - en) <= 0)
          break;
      }     

      if (l == face2.N())
      { // found point
        CDContactPair& pair = found.Append(); 
        pair._norm = -normal;
        pair._pos = point + normal * dist;
        pair._dist = dist;  
        pair._triangle[0] = 0; 
      }
    } 

    // edges... 
    float epsilon2 = epsilon * epsilon;
    for(int i = 0; i < geom1.NEdges(); i++)
    {
      typename const Edge& cand1 = geom1.GetEdge(i);   

      if (!closePoints1[cand1[0]] || !closePoints1[cand1[1]])
        continue;

      Vector3Val pos11 = geom1.GetVertex(cand1[0]);
      Vector3Val pos12 = geom1.GetVertex(cand1[1]);

      Vector3Val v1 = cand1;
      float v1pos11 = v1 * pos11;
      float v1pos12 = v1 * pos12;

      for(int j = 0; j < face2.NEdges(); j++)
      {
        //ADD_COUNTER(cdcE1,1);
        typename const Edge& cand2 = geom2.GetEdge(face2.Edge(j));

        Vector3Val pos21 = geom2.GetVertex(cand2[0]);
        Vector3Val pos22 = geom2.GetVertex(cand2[1]);

        Vector3Val v2 = cand2;

        float v1pos21 = v1 * pos21;
        float v1pos22 = v1 * pos22;

        if ((v1pos21 <= v1pos11 && v1pos22 <= v1pos11) ||
          (v1pos21 >= v1pos12 && v1pos22 >= v1pos12))
        {
          continue;
        }

        float v2pos11 = v2 * pos11;
        float v2pos12 = v2 * pos12;
        float v2pos21 = v2 * pos21;
        float v2pos22 = v2 * pos22;


        if ((v2pos11 <= v2pos21 && v2pos12 <= v2pos21) ||
          (v2pos11 >= v2pos22 && v2pos12 >= v2pos22))
        {
          //ADD_COUNTER(cdcE7,1);
          continue;
        }

        Vector3 normal = v1.CrossProduct(v2);
        float sqrSize = normal.SquareSize();
        if (sqrSize > 0.0001f) // numerical barier vectors are paralel
        {
          //normal.Normalize();
          float dist = normal * (pos22 - pos11);
          if (dist * dist > epsilon2 * sqrSize)
          {            
            // distance between lines is more than epsilon
            continue;
          }            
          float v1v2 = v1 * v2;        
          float t1 = (v2pos21 - v2pos11) - (v1pos21 - v1pos11) * v1v2;
          float t2 = (v2pos22 - v2pos11) - (v1pos22 - v1pos11) * v1v2;

          if (t1 * t2 > 0)
          {
            // both points on the same side
            //ADD_COUNTER(cdcE6,1);
            continue;
          }   

          float invT = 1.0f/ (t1 - t2);

          float posOnV1 = (t1 * v1pos22 - t2 * v1pos21) * invT ;

          if ( posOnV1 <= v1pos11 || posOnV1 >= v1pos12)
          {
            //ADD_COUNTER(cdcE6,1);
            continue;
          }

          //ADD_COUNTER(cdcE4,1);
          CDContactPair& pair = found.Append(); 
          float invSize = 1.0f / normal.Size();
          if (dist < 0)
          {
            invSize *= -1;          
          }

          pair._norm = normal * invSize;
          pair._dist = dist * invSize;
          pair._pos = (-t2 * invT * pos21 + t1 * invT * pos22);         
          pair._triangle[0] = 1; // just for debugging
        }
        else
        {
          //parallel
          normal = (pos21 - pos11);
          normal = normal - v1 * (normal * v1);

          float dist = normal.Size();

          normal /= dist;
          if (dist > epsilon)
            continue;

          //ADD_COUNTER(cdcE3,1);

          float proj11 = v1 * pos11;
          float proj12 = v1 * pos12;

          float proj21 = v1 * pos21;
          float proj22 = v1 * pos22;

          if (proj21 < proj22)
          {
            if (proj11 != proj21)
            {
              CDContactPair& pair = found.Append(); 
              pair._norm = normal;        
              pair._dist = dist;
              pair._triangle[0] = 2; // just for debugging

              if (proj21 > proj11)
                pair._pos = pos21 - normal * dist + pos11;
              else
                pair._pos = pos11;
            }

            CDContactPair& pair2 = found.Append(); 
            pair2._norm = normal;        
            pair2._dist = dist;
            pair2._triangle[0] = 2; // just for debugging

            if (proj22 < proj12)
              pair2._pos = pos22 - normal * dist;
            else
              pair2._pos = pos12;
          }
          else
          {
            if (proj11 != proj22)
            {
              CDContactPair& pair = found.Append(); 
              pair._norm = normal;        
              pair._dist = dist;
              pair._triangle[0] = 2; // just for debugging

              if (proj22 > proj11)
                pair._pos = pos22 - normal * dist;
              else
                pair._pos = pos11;
            }

            CDContactPair& pair2 = found.Append(); 
            pair2._norm = normal;        
            pair2._dist = dist;
            pair2._triangle[0] = 2; // just for debugging

            if (proj21 < proj12)
              pair2._pos = pos21 - normal * dist;
            else
              pair2._pos = pos12;
          }
        }      
      }
    }

    return;
  }

  // both plane1 >=0 && plane2 >= 0

  if (plane1 >=0 && plane2 >= 0)
  {
    // edges... 
    float epsilon2 = epsilon * epsilon;

    typename const Edge& cand1 = geom1.GetEdge(plane1);   

    Vector3Val pos11 = geom1.GetVertex(cand1[0]);
    Vector3Val pos12 = geom1.GetVertex(cand1[1]);

    Vector3Val v1 = cand1;
    float v1pos11 = v1 * pos11;
    float v1pos12 = v1 * pos12;


    //ADD_COUNTER(cdcE1,1);
    typename const Edge& cand2 = geom2.GetEdge(plane2);

    Vector3Val pos21 = geom2.GetVertex(cand2[0]);
    Vector3Val pos22 = geom2.GetVertex(cand2[1]);

    Vector3Val v2 = cand2;

    float v1pos21 = v1 * pos21;
    float v1pos22 = v1 * pos22;

    if ((v1pos21 <= v1pos11 && v1pos22 <= v1pos11) ||
      (v1pos21 >= v1pos12 && v1pos22 >= v1pos12))
    {
      return;
    }

    float v2pos11 = v2 * pos11;
    float v2pos12 = v2 * pos12;
    float v2pos21 = v2 * pos21;
    float v2pos22 = v2 * pos22;


    if ((v2pos11 <= v2pos21 && v2pos12 <= v2pos21) ||
      (v2pos11 >= v2pos22 && v2pos12 >= v2pos22))
    {
      //ADD_COUNTER(cdcE7,1);
      return;
    }

    Vector3 normal = v1.CrossProduct(v2);
    float sqrSize = normal.SquareSize();
    if (sqrSize > 0.0001f) // numerical barier vectors are paralel
    {
      //normal.Normalize();
      float dist = normal * (pos22 - pos11);
      if (dist * dist > epsilon2 * sqrSize)
      {            
        // distance between lines is more than epsilon
        return;
      }            
      float v1v2 = v1 * v2;        
      float t1 = (v2pos21 - v2pos11) - (v1pos21 - v1pos11) * v1v2;
      float t2 = (v2pos22 - v2pos11) - (v1pos22 - v1pos11) * v1v2;

      if (t1 * t2 > 0)
      {
        // both points on the same side
        //ADD_COUNTER(cdcE6,1);
        return;
      }   

      float invT = 1.0f/ (t1 - t2);

      float posOnV1 = (t1 * v1pos22 - t2 * v1pos21) * invT ;

      if ( posOnV1 <= v1pos11 || posOnV1 >= v1pos12)
      {
        //ADD_COUNTER(cdcE6,1);
        return;
      }

      //ADD_COUNTER(cdcE4,1);
      CDContactPair& pair = found.Append(); 
      float invSize = 1.0f / normal.Size();
      if (dist < 0)
      {
        invSize *= -1;          
      }

      pair._norm = normal * invSize;
      pair._dist = dist * invSize;
      pair._pos = (-t2 * invT * pos21 + t1 * invT * pos22);         
      pair._triangle[0] = 1; // just for debugging
    }
    else
    {
      //parallel
      normal = (pos21 - pos11);
      normal = normal - v1 * (normal * v1);

      float dist = normal.Size();

      normal /= dist;
      if (dist > epsilon)
        return;

      //ADD_COUNTER(cdcE3,1);

      float proj11 = v1 * pos11;
      float proj12 = v1 * pos12;

      float proj21 = v1 * pos21;
      float proj22 = v1 * pos22;

      if (proj21 < proj22)
      {
        if (proj11 != proj21)
        {
          CDContactPair& pair = found.Append(); 
          pair._norm = normal;        
          pair._dist = dist;
          pair._triangle[0] = 2; // just for debugging

          if (proj21 > proj11)
            pair._pos = pos21 - normal * dist + pos11;
          else
            pair._pos = pos11;
        }

        CDContactPair& pair2 = found.Append(); 
        pair2._norm = normal;        
        pair2._dist = dist;
        pair2._triangle[0] = 2; // just for debugging

        if (proj22 < proj12)
          pair2._pos = pos22 - normal * dist;
        else
          pair2._pos = pos12;
      }
      else
      {
        if (proj11 != proj22)
        {
          CDContactPair& pair = found.Append(); 
          pair._norm = normal;        
          pair._dist = dist;
          pair._triangle[0] = 2; // just for debugging

          if (proj22 > proj11)
            pair._pos = pos22 - normal * dist;
          else
            pair._pos = pos11;
        }

        CDContactPair& pair2 = found.Append(); 
        pair2._norm = normal;        
        pair2._dist = dist;
        pair2._triangle[0] = 2; // just for debugging

        if (proj21 < proj12)
          pair2._pos = pos21 - normal * dist;
        else
          pair2._pos = pos12;
      }
    }      
  }

  END_PROFILE(odeCC2);

  return;
}

#endif
