#ifndef _MARINA_PROJECT 
#include <Poseidon/lib/wpch.hpp>
#else
#define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES

#include "marina.hpp"
#include "inc.h"
#include "core/core.hpp"
#include "core/Dantzig.hpp"

#include "RestContactResolver.hpp"

//#include <drawstuff/drawstuff.h>

//#define DRAW_MARINA_FORCES
#undef DRAW_MARINA_FORCES

//#define min(a,b) ((a) < (b) ? (a) : (b))

const float DESINGULATOR = 0.0000001f;
const float MAXIMAL_DYN_SPEED = 0.000001f;

//const Vector3 VZero3D(0,0,0);


void SolveNxN(unsigned int n, unsigned int lda, const float * pMatrix, float * pVector);

__forceinline marina_real m_sqr(marina_real x)
{
  return x*x;
}

// Solves ax^2 + b^x + c = 0
// returns the lowest nonegative solution in x1. If does not exist any it returns a negative value.
// x2 is the second nonnegative solution. If does not exist any it returns a negative value.


int RestContactResolver::Initialize(BOOL & doNothing)
{	
  doNothing = FALSE;
  //// create matrix A. Use first three contact point from current pedestal	
  //ASSERT(_cContactPedestals.Size() == 1);
  unsigned int nNumberOfPoints = _cGroup.Contacts().Size();
  _nNumberOfForces = 3 * nNumberOfPoints;
  _nNumberOfBodies = _cGroup.Bodies().Size();

  {
    PROFILE_SCOPE_MARINA(columns);
  //g_cMatrixA.Init(_nNumberOfForces);
  //_cColumnHeaders = new AutoArray<TCOLUMNHEADER>(_nNumberOfForces);
  _cColumnHeaders.Resize(_nNumberOfForces);
  _dirs.Resize(_nNumberOfForces);

  RigidBodyObject * pCurBody1 = NULL; //I suspose that contacts of two bodies are in _cGroup.Contacts 
                                      // stored sequentialy.
  RigidBodyObject * pCurBody2 = NULL;

  Vector3 curNormal = VZero;

  int indexInClamped = -1;
  unsigned int iFromPedestal = 0; 

  for(unsigned int i = 0; i < nNumberOfPoints; i++)
  {
    //_cContactPedestals[0].piActiveIndexes[i] = i;

    TCOLUMNHEADER& tHeaderNormal = _cColumnHeaders[3 * i];
    TCOLUMNHEADER& tHeaderFriction1 = _cColumnHeaders[3 * i + 1];
    TCOLUMNHEADER& tHeaderFriction2 = _cColumnHeaders[3 * i + 2];

    memset(&tHeaderNormal, 0x0, sizeof(tHeaderNormal));
    memset(&tHeaderFriction1, 0x0, sizeof(tHeaderFriction1));
    memset(&tHeaderFriction2, 0x0, sizeof(tHeaderFriction2));

    //tHeaderFriction2.iContactPedestalID = tHeaderFriction1.iContactPedestalID = 
    //  tHeaderNormal.iContactPedestalID = 0;
    tHeaderFriction2.iContactPointID = tHeaderFriction1.iContactPointID = 
      tHeaderNormal.iContactPointID = i;

    tHeaderNormal.eForceType = EFT_STATIC;		
    tHeaderNormal.iMatrixAIndex = 3 * i;
#ifdef _DEBUG
    tHeaderNormal.iSourceMatrixAIndex = 3 * i;
#endif 
    tHeaderNormal.iDirIndex = 0;
    tHeaderNormal.pBrotherForcesHeaders[0] = &tHeaderFriction1;
    tHeaderNormal.pBrotherForcesHeaders[1] = &tHeaderFriction2;

    tHeaderFriction1.eForceType = EFT_FRICTION;		
    tHeaderFriction1.iMatrixAIndex = 3 * i + 1;
#ifdef _DEBUG
    tHeaderFriction1.iSourceMatrixAIndex = 3 * i + 1;
#endif
    tHeaderFriction1.iDirIndex = 1;
    tHeaderFriction1.pBrotherForcesHeaders[0] = &tHeaderNormal;
    tHeaderFriction1.pBrotherForcesHeaders[1] = &tHeaderFriction2;

    tHeaderFriction2.eForceType = EFT_FRICTION;			
    tHeaderFriction2.iMatrixAIndex = 3 * i + 2;
#ifdef _DEBUG
    tHeaderFriction2.iSourceMatrixAIndex = 3 * i + 2;
#endif
    tHeaderFriction2.iDirIndex = 2;
    tHeaderFriction2.pBrotherForcesHeaders[0] = &tHeaderNormal;
    tHeaderFriction2.pBrotherForcesHeaders[1] = &tHeaderFriction1;

    const RBContactPoint &tContact1 = _cGroup.Contacts()[i];

    if ((pCurBody1 != tContact1.pcBody1 || pCurBody2 != tContact1.pcBody2) && 
      (pCurBody1 != tContact1.pcBody2 || pCurBody2 != tContact1.pcBody1))
    {
      indexInClamped++;     
      
      pCurBody1 = tContact1.pcBody1;
      pCurBody2 = tContact1.pcBody2;
      curNormal = tContact1.dir[0];
      
      _numberOfClamped.Add(0);
      if (i - iFromPedestal > 3)
      {
        // create new pedestal 
        TPEDESTAL * ped = new TPEDESTAL;
        ped->nClamped = 0; 
        ped->from = iFromPedestal;
        ped->to = i - 1;
        for(unsigned int j = iFromPedestal; j < i; j++)        
          _cColumnHeaders[3 * j].pedestal = ped;

        _pedestals.Add(ped);
      }
      iFromPedestal = i;
    }
    else
    {
      if (curNormal != tContact1.dir[0])
      {
        if (i - iFromPedestal > 3)
        {
          // create new pedestal 
          TPEDESTAL * ped = new TPEDESTAL;
          ped->nClamped = 0; 
          ped->from = iFromPedestal;
          ped->to = i;
          for(unsigned int j = iFromPedestal; j < i; j++)        
            _cColumnHeaders[3 * j].pedestal = ped;  

          _pedestals.Add(ped);
        }
        iFromPedestal = i;
        curNormal = tContact1.dir[0];
      }
    }

    tHeaderNormal.iNumberOfClamped = indexInClamped;   
    tHeaderFriction1.iNumberOfClamped = indexInClamped;
    tHeaderFriction2.iNumberOfClamped = indexInClamped;

    tHeaderFriction2.fSlidingFrictionCoef = tHeaderFriction1.fSlidingFrictionCoef = 
      tHeaderNormal.fSlidingFrictionCoef = tContact1.fSlidingFrictionCoef;

    _dirs[3 * i] = tContact1.dir[0];
    _dirs[3 * i + 1] = tContact1.dir[1];
    _dirs[3 * i + 2] = tContact1.dir[2];
  }
  }
  {
    PROFILE_SCOPE_MARINA(matrixcopy);
  //g_cMatrixA = _cGroup.GetMatrix();
    _cGroup.GetMatrix();
#ifdef _DEBUG
  _cSourceMatrixA = g_cMatrixA;
  _cSuperSourceMatrixA = g_cMatrixA;
#endif
  }

  //// create vector B and Ball  
  _cVectorB.Init(_nNumberOfForces);
  //_cVectorBAll.Init(3 * _cContactPoints.Size());

  START_PROFILE_MARINA(initvectorB);
  unsigned int i = 0;
  for(; i < nNumberOfPoints; i++)
  {
    const RBContactPoint &tContact = _cGroup.Contacts()[i];
    Vector3 cSpeed = VZero;

    if (!tContact.pcBody1->_static)
    {
      cSpeed = tContact.pcBody1->_physBody->SpeedAtPoint(tContact.pos);
    }

    if (!tContact.pcBody2->_static)
    {
      cSpeed -= tContact.pcBody2->_physBody->SpeedAtPoint(tContact.pos);
    }

    Vector3 cAccel = VZero;
    if (IsNumericalZero(cSpeed * tContact.dir[0]))
    {

      if (!tContact.pcBody1->_static)
      {
        cAccel = tContact.pcBody1->_physBody->AccelerationAtPoint(tContact.pos,TB_FALSE);			
      }

      if (!tContact.pcBody2->_static)
      {
        cAccel -= tContact.pcBody2->_physBody->AccelerationAtPoint(tContact.pos,TB_FALSE);			
      }
    }       
    else
    {
      if (!tContact.pcBody1->_static)
      {
        cAccel = tContact.pcBody1->_physBody->AccelerationAtPoint(tContact.pos,TB_NONE);
      }

      if (!tContact.pcBody2->_static)
      {
        cAccel -= tContact.pcBody2->_physBody->AccelerationAtPoint(tContact.pos,TB_NONE);

      }
    }


    for(unsigned int ii = 0; ii < 3; ii++)
    {
      marina_real fSpeed = tContact.dir[ii] * cSpeed;

      {
        /* _cVectorBAll[3 * i + ii] = */_cVectorB[3 * i + ii] = tContact.dir[ii]*(cAccel);
        //_cVectorBAll[3 * i + ii] += fSpeed / _fSimulationTime;
        _cVectorB[3 * i + ii] += fSpeed / _fSimulationTime;
      }
    }

    // Check if contacs are dynamical.
    //   marina_real fSpeed1 = cContact.dir[1]*(cSpeed);
    //        marina_real fSpeed2 = cContact.dir[2]*(cSpeed);

    if (_cVectorB[3 * i + 1] * _cVectorB[3 * i + 1] + _cVectorB[3 * i + 2] * _cVectorB[3 * i + 2] > MAXIMAL_DYN_SPEED) // if dynamical contact;
    {
      Vector3 cDir(_cVectorB[3 * i + 1],_cVectorB[3 * i + 2],0);
      cDir.Normalize();

      _cColumnHeaders[3 * i + 1].cStartDir = cDir;
      _cColumnHeaders[3 * i + 2].cStartDir = cDir;
    }
  }	
  END_PROFILE_MARINA(initvectorB);

  //// Draw vectorsB
#ifdef DRAW_MARINA_FORCES
  for(unsigned int i = 0; i < nNumberOfPoints; i++)
  {
    const RBContactPoint &cContact = _cContactPoints[i];
    cBody.AddForce(cContact.pos, - cContact.dir[0] * _cVectorB[3 * i] , Color(1,1,1));
    cBody.AddForce(cContact.pos, - cContact.dir[1] * _cVectorB[3 * i + 1], Color(1,1,1));
    cBody.AddForce(cContact.pos, - cContact.dir[2] * _cVectorB[3 * i + 2], Color(1,1,1));
    cBody.AddForce(cContact.pos, cContact.dir[0] * (cBody.GetCollisionTolerance() - cContact.under)/cBody.GetCollisionTolerance() * 20, Color(0,1,1));
  }
#endif

  //// Prepare forces and accelerations
  _cForces.Init(_nNumberOfForces);
  _cAccelerations.Init(_nNumberOfForces);

  //// Prepare mapping
  _cMappingToColumnID.Resize(0);
  _cMappingToColumnID.Reserve(_nNumberOfForces,_nNumberOfForces);
  for(unsigned int i = 0; i < _nNumberOfForces; i++)
  {
    PTCOLUMNHEADER pTemp = &(_cColumnHeaders[i]);
    _cMappingToColumnID.Add(pTemp);
  }

  return MARINA_OK;
}

void RestContactResolver::ApplyCalculatedForces(const Vector<marina_real>& cForces)
{
  marina_real normMass = _cGroup.NormMass();
  for(unsigned int iForce = 0; iForce < _nNumberOfForces; iForce++)
  {
    const TCOLUMNHEADER &tColumn = *(_cMappingToColumnID[iForce]);
    const RBContactPoint &tPoint = _cGroup.Contacts()[tColumn.iContactPointID];

    if (!tPoint.pcBody1->_static)
      tPoint.pcBody1->_physBody->AddForceAtPointToBanks(
      _dirs[3 * tColumn.iContactPointID + tColumn.iDirIndex] * (cForces[iForce]) * normMass, tPoint.pos);

    if (!tPoint.pcBody2->_static)
      tPoint.pcBody2->_physBody->AddForceAtPointToBanks(
      -_dirs[3 * tColumn.iContactPointID + tColumn.iDirIndex] * (cForces[iForce]) * normMass, tPoint.pos);
#if _ADD_ARROW_ENABLED 
    //AddArrow(tPoint.pos, tPoint.dir[tColumn.iDirIndex], (cForces[iForce]));
#endif
  }	
}

#define D3DRGB(r, g, b) \
  (0xff000000L | ( ((long)((r) * 255)) << 16) | (((long)((g) * 255)) << 8) | (long)((b) * 255))

void RestContactResolver::ForcesToArrows()
{
#if _ADD_ARROW_ENABLED 
  //ResetArrows();

  marina_real normMass = _cGroup.NormMass();
  for(unsigned int iForce = 0; iForce < _pnLastIndexInZone[EMZ_SOLVING]; iForce++)
  {
    const TCOLUMNHEADER &tColumn = *(_cMappingToColumnID[iForce]);
    const RBContactPoint &tPoint = _cGroup.Contacts()[tColumn.iContactPointID];

    if (tColumn.eForceType != EFT_STATIC)
      AddArrow(tPoint.pos, tPoint.dir[tColumn.iDirIndex], (_cForces[iForce]), D3DRGB(1,0,0));
    else
      AddArrow(tPoint.pos, tPoint.dir[tColumn.iDirIndex], (_cForces[iForce]), D3DRGB(0,1,0));

  }	
#endif
}

int ImpulseContactResolver::Initialize(BOOL & doNothing)
{	
  doNothing = FALSE;
  //// create matrix A. Use first three contact point from current pedestal	
  //	ASSERT(_cContactPedestals.Size() == 1);
  unsigned int nNumberOfPoints =_cGroup.Contacts().Size();
  _nNumberOfForces = 3 * nNumberOfPoints; 
  _nNumberOfBodies = _cGroup.Bodies().Size();

  {
  PROFILE_SCOPE_MARINA(columns);
  
  _cColumnHeaders.Resize(_nNumberOfForces);
  _dirs.Resize(_nNumberOfForces);

  RigidBodyObject * pCurBody1 = NULL;
  RigidBodyObject * pCurBody2 = NULL;

  Vector3 curNormal = VZero;

  int indexInClamped = -1;
  unsigned int iFromPedestal = 0;

  for(unsigned int i = 0; i < nNumberOfPoints; i++)
  {
    //_cContactPedestals[0].piActiveIndexes[i] = i;

    TCOLUMNHEADER& tHeaderNormal = (_cColumnHeaders)[3 * i];
    TCOLUMNHEADER& tHeaderFriction1 = (_cColumnHeaders)[3 * i + 1];
    TCOLUMNHEADER& tHeaderFriction2 = (_cColumnHeaders)[3 * i + 2];

    memset(&tHeaderNormal, 0x0, sizeof(tHeaderNormal));
    memset(&tHeaderFriction1, 0x0, sizeof(tHeaderFriction1));
    memset(&tHeaderFriction2, 0x0, sizeof(tHeaderFriction2));

    //tHeaderFriction2.iContactPedestalID = tHeaderFriction1.iContactPedestalID = 
    //  tHeaderNormal.iContactPedestalID = 0;    

    tHeaderFriction2.iContactPointID = tHeaderFriction1.iContactPointID = 
      tHeaderNormal.iContactPointID = i;


    tHeaderNormal.eForceType = EFT_STATIC;		
    tHeaderNormal.iMatrixAIndex = 3 * i;
#ifdef _DEBUG
    tHeaderNormal.iSourceMatrixAIndex = 3 * i;
#endif
    tHeaderNormal.iDirIndex = 0;
    tHeaderNormal.pBrotherForcesHeaders[0] = &tHeaderFriction1;
    tHeaderNormal.pBrotherForcesHeaders[1] = &tHeaderFriction2;

    tHeaderFriction1.eForceType = EFT_FRICTION;		
    tHeaderFriction1.iMatrixAIndex = 3 * i + 1;
#ifdef _DEBUG
    tHeaderFriction1.iSourceMatrixAIndex = 3 * i + 1;
#endif
    tHeaderFriction1.iDirIndex = 1;
    tHeaderFriction1.pBrotherForcesHeaders[0] = &tHeaderNormal;
    tHeaderFriction1.pBrotherForcesHeaders[1] = &tHeaderFriction2;

    tHeaderFriction2.eForceType = EFT_FRICTION;			
    tHeaderFriction2.iMatrixAIndex = 3 * i + 2;
#ifdef _DEBUG
    tHeaderFriction2.iSourceMatrixAIndex = 3 * i + 2;
#endif
    tHeaderFriction2.iDirIndex = 2;
    tHeaderFriction2.pBrotherForcesHeaders[0] = &tHeaderNormal;
    tHeaderFriction2.pBrotherForcesHeaders[1] = &tHeaderFriction1;

    const RBContactPoint &tContact1 = _cGroup.Contacts()[i];

    if ((pCurBody1 != tContact1.pcBody1 || pCurBody2 != tContact1.pcBody2) && 
      (pCurBody1 != tContact1.pcBody2 || pCurBody2 != tContact1.pcBody1))
    {
      indexInClamped++;      
      pCurBody1 = tContact1.pcBody1;
      pCurBody2 = tContact1.pcBody2;
      curNormal = tContact1.dir[0];
      _numberOfClamped.Add(0);

      if (i - iFromPedestal > 3)
      {
        // create new pedestal 
        TPEDESTAL * ped = new TPEDESTAL;
        ped->nClamped = 0; 
        ped->from = iFromPedestal;
        ped->to = i - 1;
        for(unsigned int j = iFromPedestal; j < i; j++)        
          _cColumnHeaders[3 * j].pedestal = ped;

        _pedestals.Add(ped);
      }
      iFromPedestal = i;
    }
    else
    {
      if (curNormal != tContact1.dir[0])
      {
        curNormal = tContact1.dir[0];
        if (i - iFromPedestal > 3)
        {
          // create new pedestal 
          TPEDESTAL * ped = new TPEDESTAL;
          ped->nClamped = 0; 
          ped->from = iFromPedestal;
          ped->to = i;
          for(unsigned int j = iFromPedestal; j < i; j++)        
            _cColumnHeaders[3 * j].pedestal = ped;

          _pedestals.Add(ped);
        }
        iFromPedestal = i;
      }
    }

    tHeaderNormal.iNumberOfClamped = indexInClamped;
    tHeaderFriction1.iNumberOfClamped = indexInClamped;
    tHeaderFriction2.iNumberOfClamped = indexInClamped;

    tHeaderFriction2.fSlidingFrictionCoef = tHeaderFriction1.fSlidingFrictionCoef = 
      tHeaderNormal.fSlidingFrictionCoef = tContact1.fSlidingFrictionCoef;

    _dirs[3 * i] = tContact1.dir[0];
    _dirs[3 * i + 1] = tContact1.dir[1];
    _dirs[3 * i + 2] = tContact1.dir[2];
    ASSERT(IsNumericalZero(_dirs[3 * i] * _dirs[3 * i + 1]));
    ASSERT(IsNumericalZero(_dirs[3 * i] * _dirs[3 * i + 2]));
    ASSERT(IsNumericalZero(_dirs[3 * i + 1] * _dirs[3 * i + 2]));

    ASSERT(IsNumericalZero(_dirs[3 * i ].Size() - 1));
    ASSERT(IsNumericalZero(_dirs[3 * i + 1].Size() - 1));
    ASSERT(IsNumericalZero(_dirs[3 * i + 2].Size() - 1));
  }
  }
 
  {
    PROFILE_SCOPE_MARINA(matrixcopy);
  
    //g_cMatrixA = 
    _cGroup.GetMatrix();
#ifdef _DEBUG
    _cSourceMatrixA = g_cMatrixA;
    _cSuperSourceMatrixA = g_cMatrixA;
#endif
  }

  //// create vector B and Ball
  //START_PROFILE_MARINA(initvectorB);
  _cVectorB.Init(_nNumberOfForces);
  //_cVectorBAll.Init(3 * _cContactPoints.Size());

  START_PROFILE_MARINA(initvectorB);
  unsigned int i = 0;
  float fMaxSpeed = 0;
  for(; i < nNumberOfPoints; i++)
  {
    const RBContactPoint &tContact = _cGroup.Contacts()[i];
    Vector3 cSpeed = VZero;

    if (!tContact.pcBody1->_static)
    {
      cSpeed = tContact.pcBody1->_physBody->SpeedAtPoint(tContact.pos);
    }

    if (!tContact.pcBody2->_static)
    {			
      cSpeed -= tContact.pcBody2->_physBody->SpeedAtPoint(tContact.pos);
    }

    if (cSpeed.Size() > fMaxSpeed)
      fMaxSpeed = cSpeed.Size();

    for(unsigned int ii = 0; ii < 3; ii++)
    {
      //_cVectorBAll[3 * i + ii] = _cVectorB[3 * i + ii] = cContact.dir[ii]*(cAccel); 

      marina_real fSpeed = tContact.dir[ii]*(cSpeed);
      if (ii == 0 )
      {
        // estimate outgoing speed 
        marina_real fEpsilon = 1.0f + tContact.fRestitutionCoef;
        if(fSpeed >= 0.0f)
        {
          _cVectorB[3*i] = fSpeed; // am I right ?
        }
        else
        {
          //if (fSpeed > -0.10f)
          //    fEpsilon = 1.0f; 

          _cVectorB[3 * i] = fSpeed * fEpsilon;
        }

        //_cVectorBAll[3 * i] = _cVectorB[3 * i];
      }
      else
      {
        //_cVectorBAll[3 * i + ii] = fSpeed ;
        _cVectorB[3 * i + ii] = fSpeed ;
      }
    }

    // Check if contacs are dynamical.

    if (_cVectorB[3 * i + 1] * _cVectorB[3 * i + 1] + _cVectorB[3 * i + 2] * _cVectorB[3 * i + 2] > MAXIMAL_DYN_SPEED) // if dynamical contact;
    {
      Vector3 cDir(_cVectorB[3 * i + 1],_cVectorB[3 * i + 2],0);
      cDir.Normalize();

      (_cColumnHeaders)[3 * i + 1].cStartDir = cDir;
      (_cColumnHeaders)[3 * i + 2].cStartDir = cDir;
    }
  }
  END_PROFILE_MARINA(initvectorB);

  //// Prepare forces and accelerations
  _cForces.Init(_nNumberOfForces);
  _cAccelerations.Init(_nNumberOfForces);

  //// Prepare mapping
  _cMappingToColumnID.Resize(0);
  _cMappingToColumnID.Reserve(_nNumberOfForces,_nNumberOfForces);
  for(unsigned int i = 0; i < _nNumberOfForces; i++)
  {
    PTCOLUMNHEADER pTemp = &(_cColumnHeaders[i]);
    _cMappingToColumnID.Add(pTemp);
  }

  return MARINA_OK;
}

void ImpulseContactResolver::ApplyCalculatedForces(const Vector<marina_real>& cForces)
{
  marina_real normMass = _cGroup.NormMass();
  for(unsigned int iForce = 0; iForce < _nNumberOfForces; iForce++)
  {
    const TCOLUMNHEADER &tColumn = *(_cMappingToColumnID[iForce]);
    const RBContactPoint &tPoint = _cGroup.Contacts()[tColumn.iContactPointID];

    if (!tPoint.pcBody1->_static)
      tPoint.pcBody1->_physBody->ApplyImpulseAtPoint(
      _dirs[3 * tColumn.iContactPointID + tColumn.iDirIndex] * (cForces[iForce]) * normMass, tPoint.pos);

    if (!tPoint.pcBody2->_static)
      tPoint.pcBody2->_physBody->ApplyImpulseAtPoint(
      -_dirs[3 * tColumn.iContactPointID + tColumn.iDirIndex] * (cForces[iForce]) * normMass, tPoint.pos);        
  }
}

#define IMPULSE_TO_BREAK 500000
void BreakableImpulseContactResolver::ApplyCalculatedForces(const Vector<marina_real>& cForces)
{
  marina_real normMass = _cGroup.NormMass();
  for(unsigned int iForce = 0; iForce < _nNumberOfForces; iForce++)
  {
    const TCOLUMNHEADER &tColumn = *(_cMappingToColumnID[iForce]);
    const RBContactPoint &tPoint = _cGroup.Contacts()[tColumn.iContactPointID];

    if (fabs(cForces[iForce] * normMass) > IMPULSE_TO_BREAK)
    {          
      SeparateGeom& sepGeom = separateGeom.Append();
      sepGeom.geomID = tPoint.geomID1;
      sepGeom.obj = tPoint.pcBody1;          
      
      SeparateGeom& sepGeom2 = separateGeom.Append();
      sepGeom2.geomID = tPoint.geomID2;
      sepGeom2.obj = tPoint.pcBody2;           
    }
  }

  /*if (separateGeom.Size() != 0)
    for(unsigned int iForce = 0; iForce < _nNumberOfForces; iForce++)
      cForces[iForce] *= 0.5;

  ImpulseContactResolver::ApplyCalculatedForces(cForces);*/
}

void BreakableImpulseContactResolver::ApplyCalculatedForcesFin(float coef)
{
  for(unsigned int iForce = 0; iForce < _nNumberOfForces; iForce++)
    _cForces[iForce] *= coef;

  ImpulseContactResolver::ApplyCalculatedForces(_cForces);


}


BOOL RestContactResolver::ChooseNewPedestalPoint(unsigned int iIndex)
{
  /*
  TCOLUMNHEADER& tColumnHeader = *(_cMappingToColumnID[iIndex]);
  ASSERT(tColumnHeader.eForceType == EFT_STATIC)

  M_ContactPedestal& cPedestal = _cContactPedestals[tColumnHeader.iContactPedestalID];
  if ((cPedestal.iEndIndex - cPedestal.iStartIndex) <= 3)
  return FALSE; // no new points aviable.

  // Find point with the largest negative acceleration.
  //// Calculate force and torque currently on body from forces
  Entity& cBody = *cPedestal.pcBody1;
  Vector3 cForce(VZero3D);
  //cBody.GetForceBank(cForce);

  Vector3 cTorque(VZero3D);
  //cBody.GetTorqueBank(cTorque);

  //// TODO: in future choose just forces for the body. 
  for(unsigned int iForce = 0; iForce < _nNumberOfForces; iForce ++)
  {
  const TCOLUMNHEADER & tHeaderTemp = *(_cMappingToColumnID[iForce]);
  const RBContactPoint& cContactPoint = _cContactPoints[tHeaderTemp.iContactPointID];

  cForce += cContactPoint.dir[tHeaderTemp.iDirIndex] * _cForces[iForce];
  cTorque += cBody.TorqueFromForce(cContactPoint.dir[tHeaderTemp.iDirIndex] * _cForces[iForce], cContactPoint.pos);
  }

  //if (_pnLastIndexInZone[EMZ_NOTCLAMPED] != _pnLastIndexInZone[EMZ_SOLVING])
  //	{
  //		const RBContactPoint& cContactPoint = _cContactPoints[_cColumnHeaders[_cMappingToColumnID[_pnLastIndexInZone[EMZ_NOTCLAMPED]]].iContactPointID];
  //		cForce += cContactPoint.dirOut * _cForces[_pnLastIndexInZone[EMZ_NOTCLAMPED]];
  //		cTorque += cBody.TorqueFromForce(cContactPoint.dirOut * _cForces[_pnLastIndexInZone[EMZ_NOTCLAMPED]], cContactPoint.pos);
  //	}


  marina_real fMaxAccel = 0.0f;
  unsigned int iMaxIndex = 0;
  for(unsigned int iPointID = cPedestal.iStartIndex; iPointID < cPedestal.iEndIndex; iPointID++)
  {
  const RBContactPoint& cContactPoint = _cContactPoints[iPointID];

  if (iPointID == cPedestal.piActiveIndexes[0] || iPointID == cPedestal.piActiveIndexes[1] ||
  iPointID == cPedestal.piActiveIndexes[2])
  {
  }
  else
  {
  marina_real fAccel = cContactPoint.dir[0]*(
  cBody.AccelerationAtPointOnForceAndTorque(cContactPoint.pos, cForce, cTorque, TB_NONE))
  + _cVectorBAll[3 * iPointID];

  if (fAccel < fMaxAccel)
  {
  fMaxAccel = fAccel;
  iMaxIndex = iPointID;
  }
  }
  }

  if (fMaxAccel < 0.0f && !IsNumericalZero(fMaxAccel))
  {
  // new candidate is found, move point into unsolved

  if (cPedestal.piActiveIndexes[0] == tColumnHeader.iContactPointID)
  cPedestal.piActiveIndexes[0] = iMaxIndex;
  else
  if (cPedestal.piActiveIndexes[1] == tColumnHeader.iContactPointID)
  cPedestal.piActiveIndexes[1] = iMaxIndex;
  else
  if (cPedestal.piActiveIndexes[2] == tColumnHeader.iContactPointID)
  cPedestal.piActiveIndexes[2] = iMaxIndex;
  else
  ASSERT(FALSE);

  TCOLUMNHEADER & tHeaderBrotherFriction1 =  * tColumnHeader.pBrotherForcesHeaders[0];
  TCOLUMNHEADER & tHeaderBrotherFriction2 =  * tColumnHeader.pBrotherForcesHeaders[1];

  // Move point between unsolved
  MoveToZoneEx(iIndex, EMZ_UNSOLVED);

  ASSERT(tHeaderBrotherFriction1.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING]);
  ASSERT(tHeaderBrotherFriction2.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING]);


  _cVectorB[tColumnHeader.iMatrixAIndex] = _cVectorBAll[3 * iMaxIndex];
  _cVectorB[tHeaderBrotherFriction1.iMatrixAIndex] = _cVectorBAll[3 * iMaxIndex + 1];
  _cVectorB[tHeaderBrotherFriction2.iMatrixAIndex] = _cVectorBAll[3 * iMaxIndex + 2];

  if (_cVectorBAll[3 * iMaxIndex + 1] * _cVectorBAll[3 * iMaxIndex + 1] + 
  _cVectorBAll[3 * iMaxIndex + 2] * _cVectorBAll[3 * iMaxIndex + 2] > MAXIMAL_DYN_SPEED)
  {
  Vector3 cDir(_cVectorBAll[3 * iMaxIndex + 1],_cVectorBAll[3 * iMaxIndex + 2],0);
  cDir.Normalize();

  tHeaderBrotherFriction1.cStartDir = cDir;
  tHeaderBrotherFriction2.cStartDir = cDir;
  }

  const RBContactPoint& cContactPoint = _cContactPoints[iMaxIndex];

  tColumnHeader.iContactPointID = iMaxIndex;
  tColumnHeader.iLastChangeCycle = _iCycleLast; // It was moved, but it is brand new so can be used
  tColumnHeader.fSlidingFrictionCoef = cContactPoint.fSlidingFrictionCoef;

  tHeaderBrotherFriction1.iContactPointID = iMaxIndex;
  tHeaderBrotherFriction1.iLastChangeCycle = _iCycleLast;
  tHeaderBrotherFriction1.fSlidingFrictionCoef = cContactPoint.fSlidingFrictionCoef;

  tHeaderBrotherFriction2.iContactPointID = iMaxIndex;
  tHeaderBrotherFriction2.iLastChangeCycle = _iCycleLast;
  tHeaderBrotherFriction2.fSlidingFrictionCoef = cContactPoint.fSlidingFrictionCoef;

  for(unsigned int i = 0; i < _nNumberOfForces; i++)
  {
  Vector3 cAccel;
  const TCOLUMNHEADER& tColumnTemp = *(_cMappingToColumnID[i]);
  const RBContactPoint& cContactPoint2 = _cContactPoints[tColumnTemp.iContactPointID];

  cBody.AccelerationAtPointOnForceAtPoint(cAccel, cContactPoint.pos, 
  cContactPoint2.dir[tColumnTemp.iDirIndex], cContactPoint2.pos);

  g_cMatrixA.SetMemberValue( tColumnHeader.iMatrixAIndex, i, cAccel*(cContactPoint.dir[0]));
  g_cMatrixA.SetMemberValue( i, tColumnHeader.iMatrixAIndex, cAccel*(cContactPoint.dir[0]));

  g_cMatrixA.SetMemberValue( tHeaderBrotherFriction1.iMatrixAIndex, i, cAccel*(cContactPoint.dir[1]));
  g_cMatrixA.SetMemberValue( i, tHeaderBrotherFriction1.iMatrixAIndex, cAccel*(cContactPoint.dir[1]));

  g_cMatrixA.SetMemberValue( tHeaderBrotherFriction2.iMatrixAIndex, i, cAccel*(cContactPoint.dir[2]));
  g_cMatrixA.SetMemberValue( i, tHeaderBrotherFriction2.iMatrixAIndex, cAccel*(cContactPoint.dir[2]));
  }


  // some of the columns are combinated
  for(unsigned int i = 0; i < _pnLastIndexInZone[EMZ_CLAMPED]; i++)
  {
  const TCOLUMNHEADER& tHeaderTemp = *(_cMappingToColumnID[i]);
  if (tHeaderTemp.eForceType == EFT_STATIC && tHeaderTemp.cNotClampedDir != VZero3D)
  {
  unsigned int iFriction1Index = tHeaderTemp.pBrotherForcesHeaders[0]->iMatrixAIndex;
  unsigned int iFriction2Index = tHeaderTemp.pBrotherForcesHeaders[1]->iMatrixAIndex;

  g_cMatrixA.AddMemberValue(tColumnHeader.iMatrixAIndex,i,
  tHeaderTemp.fSlidingFrictionCoef * 
  (tHeaderTemp.cNotClampedDir[0] * g_cMatrixA.GetMemberValue(tColumnHeader.iMatrixAIndex, iFriction1Index) + 
  tHeaderTemp.cNotClampedDir[1] * g_cMatrixA.GetMemberValue(tColumnHeader.iMatrixAIndex, iFriction2Index)));

  g_cMatrixA.AddMemberValue(tHeaderBrotherFriction1.iMatrixAIndex,i,
  tHeaderTemp.fSlidingFrictionCoef * 
  (tHeaderTemp.cNotClampedDir[0] * g_cMatrixA.GetMemberValue(tHeaderBrotherFriction1.iMatrixAIndex, iFriction1Index) + 
  tHeaderTemp.cNotClampedDir[1] * g_cMatrixA.GetMemberValue(tHeaderBrotherFriction1.iMatrixAIndex, iFriction2Index)));

  g_cMatrixA.AddMemberValue(tHeaderBrotherFriction2.iMatrixAIndex,i,
  tHeaderTemp.fSlidingFrictionCoef * 
  (tHeaderTemp.cNotClampedDir[0] * g_cMatrixA.GetMemberValue(tHeaderBrotherFriction2.iMatrixAIndex, iFriction1Index) + 
  tHeaderTemp.cNotClampedDir[1] * g_cMatrixA.GetMemberValue(tHeaderBrotherFriction2.iMatrixAIndex, iFriction2Index)));
  }
  }

  // desingulator
  //g_cMatrixA.AddMemberValue(tColumnHeader.iMatrixAIndex, tColumnHeader.iMatrixAIndex, - DESINGULATOR);
  g_cMatrixA.AddMemberValue(tHeaderBrotherFriction1.iMatrixAIndex, tHeaderBrotherFriction1.iMatrixAIndex, - DESINGULATOR);
  g_cMatrixA.AddMemberValue(tHeaderBrotherFriction2.iMatrixAIndex, tHeaderBrotherFriction2.iMatrixAIndex, - DESINGULATOR);

  return TRUE;
  }
  */

  return FALSE;
}

BOOL ImpulseContactResolver::ChooseNewPedestalPoint(unsigned int iIndex)
{
  /*
  TCOLUMNHEADER& tColumnHeader = *(_cMappingToColumnID[iIndex]);
  ASSERT(tColumnHeader.eForceType == EFT_STATIC)

  M_ContactPedestal& cPedestal = _cContactPedestals[tColumnHeader.iContactPedestalID];
  if ((cPedestal.iEndIndex - cPedestal.iStartIndex) <= 3)
  return FALSE; // no new points aviable.

  // Find point with the largest negative acceleration.
  //// Calculate force and torque currently on body from forces
  Entity& cBody = *cPedestal.pcBody1;
  Vector3 cForce(VZero3D);
  //cBody.GetForceBank(cForce);

  Vector3 cTorque(VZero3D);
  //cBody.GetTorqueBank(cTorque);

  //// TODO: in future choose just forces for the body. 
  for(unsigned int iForce = 0; iForce < _nNumberOfForces; iForce ++)
  {
  const TCOLUMNHEADER & tHeaderTemp = *(_cMappingToColumnID[iForce]);
  const RBContactPoint& cContactPoint = _cContactPoints[tHeaderTemp.iContactPointID];

  cForce += cContactPoint.dir[tHeaderTemp.iDirIndex] * _cForces[iForce];
  cTorque += cBody.TorqueFromForce(cContactPoint.dir[tHeaderTemp.iDirIndex] * _cForces[iForce], cContactPoint.pos);
  }

  //if (_pnLastIndexInZone[EMZ_NOTCLAMPED] != _pnLastIndexInZone[EMZ_SOLVING])
  //	{
  //		const RBContactPoint& cContactPoint = _cContactPoints[_cColumnHeaders[_cMappingToColumnID[_pnLastIndexInZone[EMZ_NOTCLAMPED]]].iContactPointID];
  //		cForce += cContactPoint.dirOut * _cForces[_pnLastIndexInZone[EMZ_NOTCLAMPED]];
  //		cTorque += cBody.TorqueFromForce(cContactPoint.dirOut * _cForces[_pnLastIndexInZone[EMZ_NOTCLAMPED]], cContactPoint.pos);
  //	}


  marina_real fMaxAccel = 0.0f;
  unsigned int iMaxIndex = 0;
  for(unsigned int iPointID = cPedestal.iStartIndex; iPointID < cPedestal.iEndIndex; iPointID++)
  {
  const RBContactPoint& cContactPoint = _cContactPoints[iPointID];

  if (iPointID == cPedestal.piActiveIndexes[0] || iPointID == cPedestal.piActiveIndexes[1] ||
  iPointID == cPedestal.piActiveIndexes[2])
  {
  }
  else
  {
  marina_real fAccel = cContactPoint.dir[0]*(
  cBody.AccelerationAtPointOnForceAndTorque(cContactPoint.pos, cForce, cTorque, TB_NONE))
  + _cVectorBAll[3 * iPointID];

  if (fAccel < fMaxAccel)
  {
  fMaxAccel = fAccel;
  iMaxIndex = iPointID;
  }
  }
  }

  if (fMaxAccel < 0.0f && !IsNumericalZero(fMaxAccel))
  {
  // new candidate is found, move point into unsolved

  if (cPedestal.piActiveIndexes[0] == tColumnHeader.iContactPointID)
  cPedestal.piActiveIndexes[0] = iMaxIndex;
  else
  if (cPedestal.piActiveIndexes[1] == tColumnHeader.iContactPointID)
  cPedestal.piActiveIndexes[1] = iMaxIndex;
  else
  if (cPedestal.piActiveIndexes[2] == tColumnHeader.iContactPointID)
  cPedestal.piActiveIndexes[2] = iMaxIndex;
  else
  ASSERT(FALSE);

  TCOLUMNHEADER & tHeaderBrotherFriction1 =  * tColumnHeader.pBrotherForcesHeaders[0];
  TCOLUMNHEADER & tHeaderBrotherFriction2 =  * tColumnHeader.pBrotherForcesHeaders[1];

  // Move point between unsolved
  MoveToZoneEx(iIndex, EMZ_UNSOLVED, EMZ_NOTCLAMPED);

  ASSERT(tHeaderBrotherFriction1.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING]);
  ASSERT(tHeaderBrotherFriction2.iMatrixAIndex >= _pnLastIndexInZone[EMZ_SOLVING]);


  _cVectorB[tColumnHeader.iMatrixAIndex] = _cVectorBAll[3 * iMaxIndex];
  _cVectorB[tHeaderBrotherFriction1.iMatrixAIndex] = _cVectorBAll[3 * iMaxIndex + 1];
  _cVectorB[tHeaderBrotherFriction2.iMatrixAIndex] = _cVectorBAll[3 * iMaxIndex + 2];

  if (_cVectorBAll[3 * iMaxIndex + 1] * _cVectorBAll[3 * iMaxIndex + 1] + 
  _cVectorBAll[3 * iMaxIndex + 2] * _cVectorBAll[3 * iMaxIndex + 2] > MAXIMAL_DYN_SPEED)
  {
  Vector3 cDir(_cVectorBAll[3 * iMaxIndex + 1],_cVectorBAll[3 * iMaxIndex + 2],0);
  cDir.Normalize();

  tHeaderBrotherFriction1.cStartDir = cDir;
  tHeaderBrotherFriction2.cStartDir = cDir;
  }

  const RBContactPoint& cContactPoint = _cContactPoints[iMaxIndex];

  tColumnHeader.iContactPointID = iMaxIndex;
  tColumnHeader.iLastChangeCycle = _iCycleLast; // It was moved, but it is brand new so can be used
  tColumnHeader.fSlidingFrictionCoef = cContactPoint.fSlidingFrictionCoef;

  tHeaderBrotherFriction1.iContactPointID = iMaxIndex;
  tHeaderBrotherFriction1.iLastChangeCycle = _iCycleLast;
  tHeaderBrotherFriction1.fSlidingFrictionCoef = cContactPoint.fSlidingFrictionCoef;

  tHeaderBrotherFriction2.iContactPointID = iMaxIndex;
  tHeaderBrotherFriction2.iLastChangeCycle = _iCycleLast;
  tHeaderBrotherFriction2.fSlidingFrictionCoef = cContactPoint.fSlidingFrictionCoef;

  for(unsigned int i = 0; i < _nNumberOfForces; i++)
  {
  Vector3 cAccel;
  const TCOLUMNHEADER& tColumnTemp = *(_cMappingToColumnID[i]);
  const RBContactPoint& cContactPoint2 = _cContactPoints[tColumnTemp.iContactPointID];

  cBody.AccelerationAtPointOnForceAtPoint(cAccel, cContactPoint.pos, 
  cContactPoint2.dir[tColumnTemp.iDirIndex], cContactPoint2.pos);

  g_cMatrixA.SetMemberValue( tColumnHeader.iMatrixAIndex, i, cAccel*(cContactPoint.dir[0]));
  g_cMatrixA.SetMemberValue( i, tColumnHeader.iMatrixAIndex, cAccel*(cContactPoint.dir[0]));

  g_cMatrixA.SetMemberValue( tHeaderBrotherFriction1.iMatrixAIndex, i, cAccel*(cContactPoint.dir[1]));
  g_cMatrixA.SetMemberValue( i, tHeaderBrotherFriction1.iMatrixAIndex, cAccel*(cContactPoint.dir[1]));

  g_cMatrixA.SetMemberValue( tHeaderBrotherFriction2.iMatrixAIndex, i, cAccel*(cContactPoint.dir[2]));
  g_cMatrixA.SetMemberValue( i, tHeaderBrotherFriction2.iMatrixAIndex, cAccel*(cContactPoint.dir[2]));
  }


  // some of the columns are combinated
  for(unsigned int i = 0; i < _pnLastIndexInZone[EMZ_CLAMPED]; i++)
  {
  const TCOLUMNHEADER& tHeaderTemp = *(_cMappingToColumnID[i]);
  if (tHeaderTemp.eForceType == EFT_STATIC && tHeaderTemp.cNotClampedDir != VZero3D)
  {
  unsigned int iFriction1Index = tHeaderTemp.pBrotherForcesHeaders[0]->iMatrixAIndex;
  unsigned int iFriction2Index = tHeaderTemp.pBrotherForcesHeaders[1]->iMatrixAIndex;

  g_cMatrixA.AddMemberValue(tColumnHeader.iMatrixAIndex,i,
  tHeaderTemp.fSlidingFrictionCoef * 
  (tHeaderTemp.cNotClampedDir[0] * g_cMatrixA.GetMemberValue(tColumnHeader.iMatrixAIndex, iFriction1Index) + 
  tHeaderTemp.cNotClampedDir[1] * g_cMatrixA.GetMemberValue(tColumnHeader.iMatrixAIndex, iFriction2Index)));

  g_cMatrixA.AddMemberValue(tHeaderBrotherFriction1.iMatrixAIndex,i,
  tHeaderTemp.fSlidingFrictionCoef * 
  (tHeaderTemp.cNotClampedDir[0] * g_cMatrixA.GetMemberValue(tHeaderBrotherFriction1.iMatrixAIndex, iFriction1Index) + 
  tHeaderTemp.cNotClampedDir[1] * g_cMatrixA.GetMemberValue(tHeaderBrotherFriction1.iMatrixAIndex, iFriction2Index)));

  g_cMatrixA.AddMemberValue(tHeaderBrotherFriction2.iMatrixAIndex,i,
  tHeaderTemp.fSlidingFrictionCoef * 
  (tHeaderTemp.cNotClampedDir[0] * g_cMatrixA.GetMemberValue(tHeaderBrotherFriction2.iMatrixAIndex, iFriction1Index) + 
  tHeaderTemp.cNotClampedDir[1] * g_cMatrixA.GetMemberValue(tHeaderBrotherFriction2.iMatrixAIndex, iFriction2Index)));
  }
  }

  // desingulator
  //g_cMatrixA.AddMemberValue(tColumnHeader.iMatrixAIndex, tColumnHeader.iMatrixAIndex, - DESINGULATOR);
  g_cMatrixA.AddMemberValue(tHeaderBrotherFriction1.iMatrixAIndex, tHeaderBrotherFriction1.iMatrixAIndex, - DESINGULATOR);
  g_cMatrixA.AddMemberValue(tHeaderBrotherFriction2.iMatrixAIndex, tHeaderBrotherFriction2.iMatrixAIndex, - DESINGULATOR);

  return TRUE;
  }
  */

  return FALSE;
}

void RestContactResolver::SerializeChild(FILE *fArchive)
{
  //	_pcCollGroup->SerializeCollisions( fArchive, TRUE);
}

void ImpulseContactResolver::SerializeChild(FILE *fArchive)
{
  // 	_cCollGroupRestricted.SerializeCollisions( fArchive, TRUE);
}

#endif

