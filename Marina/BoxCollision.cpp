#include "inc.h"
#include "marina.hpp"
#include "BoxCollision.h"
#include "Math2D.h"

//const Vector3 VZero3D(0,0,0);
const CVector3DInt VZeroInt(0,0,0);
 marina_real g_fTolerance = TOLERANCE;



__forceinline int ClosestPointOnLine2D(marina_real& tCoef, marina_real& fDistance, const CVector2D& cPoint,  const CVector2D& cFrom, const CVector2D& cTo) // Do not takes line endpoints
{
    CVector2D cDir = cTo - cFrom;
    CVector2D cDiff = cFrom - cPoint;
    tCoef = - (cDir * cDiff / cDir.GetSqrLength());

    if (tCoef > 0.0f && tCoef < 1.0f)
    {
        fDistance = (cDir * tCoef + cDiff).GetLength();
        return 1;
    }

    return -1;
}

__forceinline int LineToPointx(CPointPairArray& cFoundPairs, const Vector3& cBox1, const CVector2D& cPoint12D,const CVector3DInt& cBodyRelCoord1,
                 const Vector3& cFrom3D, const Vector3& cTo3D, const CVector3DInt& cBodyRelCoord2)
{
    CVector2D cFrom2D(cFrom3D[1], cFrom3D[2]);
    CVector2D cTo2D(cTo3D[1], cTo3D[2]);

    if (cFrom2D != cTo2D)
    {
        marina_real tCoef;
        marina_real fDistance;
        
        if (0 < ClosestPointOnLine2D( tCoef, fDistance, cPoint12D, cFrom2D, cTo2D))
        {
            if (fDistance <=  g_fTolerance)
            {                
                //point found
                BC_TPOINTPAIR tPointPair;

                tPointPair.tPoint2.cWordCoord = cFrom3D + (cTo3D - cFrom3D) * tCoef;

                if (tPointPair.tPoint2.cWordCoord[0] > -cBox1[0] && tPointPair.tPoint2.cWordCoord[0] < cBox1[0])
                {
                
                    tPointPair.fDistance = fDistance;

                    tPointPair.tPoint1.cBodyRelCoord = cBodyRelCoord1;
                    tPointPair.tPoint1.cWordCoord = Vector3(tPointPair.tPoint2.cWordCoord[0],cPoint12D[0], cPoint12D[1]);

                    tPointPair.tPoint2.cBodyRelCoord = cBodyRelCoord2;

                    cFoundPairs.Add(tPointPair);
                }                
            }
        }
    }

    return MARINA_OK;
}

__forceinline int LineToPointy(CPointPairArray& cFoundPairs, const Vector3& cBox1, const CVector2D& cPoint12D,const CVector3DInt& cBodyRelCoord1,
                 const Vector3& cFrom3D, const Vector3& cTo3D, const CVector3DInt& cBodyRelCoord2)
{
    CVector2D cFrom2D(cFrom3D[0], cFrom3D[2]);
    CVector2D cTo2D(cTo3D[0], cTo3D[2]);

    if (cFrom2D != cTo2D)
    {
        marina_real tCoef;
        marina_real fDistance;
        
        if (0 < ClosestPointOnLine2D( tCoef, fDistance, cPoint12D, cFrom2D, cTo2D))
        {
            if (fDistance <=  g_fTolerance)
            {                
                //point found
                BC_TPOINTPAIR tPointPair;

                tPointPair.tPoint2.cWordCoord = cFrom3D + (cTo3D - cFrom3D) * tCoef;

                if (tPointPair.tPoint2.cWordCoord[1] > -cBox1[1] && tPointPair.tPoint2.cWordCoord[1] < cBox1[1])
                {
                
                    tPointPair.fDistance = fDistance;

                    tPointPair.tPoint1.cBodyRelCoord = cBodyRelCoord1;
                    tPointPair.tPoint1.cWordCoord = Vector3(cPoint12D[0],tPointPair.tPoint2.cWordCoord[1], cPoint12D[1]);

                    tPointPair.tPoint2.cBodyRelCoord = cBodyRelCoord2;

                    cFoundPairs.Add(tPointPair);
                }                
            }
        }
    }

    return MARINA_OK;
}

__forceinline int LineToPointz(CPointPairArray& cFoundPairs, const Vector3& cBox1, const CVector2D& cPoint12D,const CVector3DInt& cBodyRelCoord1,
                 const Vector3& cFrom3D, const Vector3& cTo3D, const CVector3DInt& cBodyRelCoord2)
{
    CVector2D cFrom2D(cFrom3D[0], cFrom3D[1]);
    CVector2D cTo2D(cTo3D[0], cTo3D[1]);

    if (cFrom2D != cTo2D)
    {
        marina_real tCoef;
        marina_real fDistance;
        
        if (0 < ClosestPointOnLine2D( tCoef, fDistance, cPoint12D, cFrom2D, cTo2D))
        {
            if (fDistance <=  g_fTolerance)
            {                
                //point found
                BC_TPOINTPAIR tPointPair;

                tPointPair.tPoint2.cWordCoord = cFrom3D + (cTo3D - cFrom3D) * tCoef;

                if (tPointPair.tPoint2.cWordCoord[2] > -cBox1[2] && tPointPair.tPoint2.cWordCoord[2] < cBox1[2])
                {
                
                    tPointPair.fDistance = fDistance;

                    tPointPair.tPoint1.cBodyRelCoord = cBodyRelCoord1;
                    tPointPair.tPoint1.cWordCoord = Vector3(cPoint12D[0], cPoint12D[1],tPointPair.tPoint2.cWordCoord[2]);

                    tPointPair.tPoint2.cBodyRelCoord = cBodyRelCoord2;

                    cFoundPairs.Add(tPointPair);
                }                
            }
        }
    }

    return MARINA_OK;
}
int LinesToLines(CPointPairArray& cFoundPairs, const Vector3& cBox1, const Vector3& cBox2, const CMatrix3DRow& cToWorld2, const Vector3& cPos2)
{
    Vector3 pppcPoints2[2][2][2];

    Vector3 cBodyPoint;
    for(int ix = -1; ix < 2; ix+= 2)
    {
        cBodyPoint[0] = (cBox2[0] * ix);

        for(int iy = -1; iy < 2; iy+= 2)
        {
            cBodyPoint[1] = (cBox2[1] * iy);

            for(int iz = -1; iz < 2; iz+= 2)
            {
                cBodyPoint[2] = (cBox2[2] * iz);
                pppcPoints2[(ix + 1)/2][(iy + 1)/2][(iz + 1)/2] = cToWorld2 * cBodyPoint + cPos2;
            }
        }
    }

    // Just stupid algorithm everybody with everybody, aproved by flyman
    // in x - dir
    for(int iy1 = -1; iy1 < 2; iy1+= 2)
    {
        for(int iz1 = -1; iz1 < 2; iz1+= 2)
        {
            CVector2D cPoint12D(cBox1[1] * iy1, cBox1[2] * iz1);
            CVector3DInt cRelBodyCoord1(0,iy1,iz1);

            for(int ix2 = -1; ix2 < 2; ix2+=2)
            {
                for(int iy2 = -1; iy2 < 2; iy2+=2)
                {
                    for(int iz2 = -1; iz2 < 2; iz2+=2)
                    {
                        const Vector3& cFrom3D = pppcPoints2[(ix2 + 1)/2][(iy2 + 1)/2][(iz2 + 1)/2];
                        
                        if (ix2 == -1)
                        {
                            const Vector3& cTo3D = pppcPoints2[1][(iy2 + 1)/2][(iz2 + 1)/2];                                                                                   
                            CVector3DInt cRelBodyCoord2(0,iy2,iz2);
                            
                            LineToPointx(cFoundPairs, cBox1, cPoint12D, cRelBodyCoord1, cFrom3D, cTo3D,cRelBodyCoord2);                                
                        }

                        if (iy2 == -1)
                        {
                            const Vector3& cTo3D = pppcPoints2[(ix2 + 1)/2][1][(iz2 + 1)/2];
                            CVector3DInt cRelBodyCoord2(ix2,0,iz2);
                            LineToPointx(cFoundPairs, cBox1, cPoint12D, cRelBodyCoord1, cFrom3D, cTo3D,cRelBodyCoord2);                                
                        }

                        if (iz2 == -1)
                        {
                            const Vector3& cTo3D = pppcPoints2[(ix2 + 1)/2][(iy2 + 1)/2][1];                                                                                   
                            CVector3DInt cRelBodyCoord2(ix2,iy2,0);
                            LineToPointx(cFoundPairs, cBox1, cPoint12D, cRelBodyCoord1, cFrom3D, cTo3D,cRelBodyCoord2);                                
                        }
                    }
                }
            }
        }
    }

    // in y - dir
    for(int ix1 = -1; ix1 < 2; ix1+= 2)
    {
        for(int iz1 = -1; iz1 < 2; iz1+= 2)
        {
            CVector2D cPoint12D(cBox1[0] * ix1, cBox1[2] * iz1);
            CVector3DInt cRelBodyCoord1(ix1,0,iz1);

            for(int ix2 = -1; ix2 < 2; ix2+=2)
            {
                for(int iy2 = -1; iy2 < 2; iy2+=2)
                {
                    for(int iz2 = -1; iz2 < 2; iz2+=2)
                    {
                       const Vector3& cFrom3D = pppcPoints2[(ix2 + 1)/2][(iy2 + 1)/2][(iz2 + 1)/2];
                        
                        if (ix2 == -1)
                        {
                            const Vector3& cTo3D = pppcPoints2[1][(iy2 + 1)/2][(iz2 + 1)/2];                                                                                  
                            CVector3DInt cRelBodyCoord2(0,iy2,iz2);
                            
                            LineToPointy(cFoundPairs, cBox1, cPoint12D, cRelBodyCoord1, cFrom3D, cTo3D,cRelBodyCoord2);                                
                        }

                        if (iy2 == -1)
                        {
                            const Vector3& cTo3D = pppcPoints2[(ix2 + 1)/2][1][(iz2 + 1)/2];
                            CVector3DInt cRelBodyCoord2(ix2,0,iz2);
                            LineToPointy(cFoundPairs, cBox1, cPoint12D, cRelBodyCoord1, cFrom3D, cTo3D,cRelBodyCoord2);                                
                        }

                        if (iz2 == -1)
                        {
                            const Vector3& cTo3D = pppcPoints2[(ix2 + 1)/2][(iy2 + 1)/2][1];                                                                                   
                            CVector3DInt cRelBodyCoord2(ix2,iy2,0);
                            LineToPointy(cFoundPairs, cBox1, cPoint12D, cRelBodyCoord1, cFrom3D, cTo3D,cRelBodyCoord2);                                
                        }
                    }
                }
            }
        }
    }

    // in z - dir
    for(int ix1 = -1; ix1 < 2; ix1+= 2)
    {
        for(int iy1 = -1; iy1 < 2; iy1+= 2)
        {
            CVector2D cPoint12D(cBox1[0] * ix1, cBox1[1] * iy1);
            CVector3DInt cRelBodyCoord1(ix1,iy1,0);

            for(int ix2 = -1; ix2 < 2; ix2+=2)
            {
                for(int iy2 = -1; iy2 < 2; iy2+=2)
                {
                    for(int iz2 = -1; iz2 < 2; iz2+=2)
                    {
                        const Vector3& cFrom3D = pppcPoints2[(ix2 + 1)/2][(iy2 + 1)/2][(iz2 + 1)/2];
                        
                        if (ix2 == -1)
                        {
                            const Vector3& cTo3D = pppcPoints2[1][(iy2 + 1)/2][(iz2 + 1)/2];                                                                                  
                            CVector3DInt cRelBodyCoord2(0,iy2,iz2);
                            
                            LineToPointz(cFoundPairs, cBox1, cPoint12D, cRelBodyCoord1, cFrom3D, cTo3D,cRelBodyCoord2);                                
                        }

                        if (iy2 == -1)
                        {
                            const Vector3& cTo3D = pppcPoints2[(ix2 + 1)/2][1][(iz2 + 1)/2];                                                                                  
                            CVector3DInt cRelBodyCoord2(ix2,0,iz2);
                            LineToPointz(cFoundPairs, cBox1, cPoint12D, cRelBodyCoord1, cFrom3D, cTo3D,cRelBodyCoord2);                                
                        }

                        if (iz2 == -1)
                        {
                            const Vector3& cTo3D = pppcPoints2[(ix2 + 1)/2][(iy2 + 1)/2][1];                                                                                   
                            CVector3DInt cRelBodyCoord2(ix2,iy2,0);
                            LineToPointz(cFoundPairs, cBox1, cPoint12D, cRelBodyCoord1, cFrom3D, cTo3D,cRelBodyCoord2);                                
                        }
                    }
                }
            }
        }
    }

    return cFoundPairs.Size();
}
   
void FilterPairs(M_ContactArray& cFinalContacts, const CPointPairArray& cFoundPairs)
{
    if (cFoundPairs.Size() == 0)
        return;

    // Find closest par.
    marina_real fSmallestDist = g_fTolerance * 2;
    Vector3 cSmallestDistNorm;

    for( int i = 0; i < cFoundPairs.Size(); i++)
    {
        if (fSmallestDist > cFoundPairs[i].fDistance)
        {
            fSmallestDist = cFoundPairs[i].fDistance;
            cSmallestDistNorm = cFoundPairs[i].tPoint1.cWordCoord - cFoundPairs[i].tPoint2.cWordCoord;
        }
    }

    cSmallestDistNorm.Normalize();
    for( int i = 0; i < cFoundPairs.Size(); i++)
    {
        Vector3 cNorm = cFoundPairs[i].tPoint1.cWordCoord - cFoundPairs[i].tPoint2.cWordCoord;

        cNorm.Normalize();

        if (cNorm * cSmallestDistNorm > 0.98)
        {
            // Add to contacts;
            M_ContactPoint tContactPoint;
		    //tContactPoint.pcBody1 = pcBody1;
		    //tContactPoint.pcBody2 = pcBody2;
		    //tContactPoint.fRestitutionCoef = pcBody1->GetRestitutionCoef();
		    //tContactPoint.fSlidingFrictionCoef = pcBody1->GetSlidingFrictionCoef();
		    //tContactPoint.pcPedestal = NULL;


		    tContactPoint.pos = (cFoundPairs[i].tPoint2.cWordCoord + cFoundPairs[i].tPoint1.cWordCoord) * 0.5;
		    tContactPoint.under =  g_fTolerance  - cFoundPairs[i].fDistance;

		    tContactPoint.dir[0] = cSmallestDistNorm;
	
	    	// Find pedestal axes
		    tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(1,0,0));
		    if (tContactPoint.dir[1].Size() < 0.1f) // is new vector correct
			    tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(0,1,0));

		    tContactPoint.dir[2] = tContactPoint.dir[0].CrossProduct(tContactPoint.dir[1]);

		    tContactPoint.dir[1].Normalize();
		    tContactPoint.dir[2].Normalize();
        
            cFinalContacts.Add(tContactPoint);
        }
    }
}

int PointsInBox(CPointArray& cFoundPoints, const Vector3& cBox1, const Vector3& cBox2, const CMatrix3DRow& cToWorld2, const Vector3& cPos2)
{

    Vector3 cBodyCoord2;
	for(int ix = -1; ix < 2; ix += 2)
	{
        cBodyCoord2[0] = (cBox2[0] * ix);
        for(int iy = -1; iy < 2; iy += 2)
        {
            cBodyCoord2[1] = (cBox2[1] * iy);
            for(int iz = -1; iz < 2; iz += 2)
	        {
                cBodyCoord2[2] = (cBox2[2] * iz);
                Vector3 cPoint = cToWorld2 * cBodyCoord2 + cPos2;
;
                if (cPoint[0] <= cBox1[0] && cPoint[0] >= -cBox1[0] &&
                    cPoint[1] <= cBox1[1] && cPoint[1] >= -cBox1[1] &&
                    cPoint[2] <= cBox1[2] && cPoint[2] >= -cBox1[2])
                {
                    // Point is in
                    BC_TPOINT tPoint;
                    tPoint.cWordCoord = cPoint;
                    tPoint.cBodyRelCoord.Set(ix,iy,iz);

                    cFoundPoints.Add(tPoint);
                }
            }
        }
	}

    return cFoundPoints.Size();
}

int PointPairs(CPointPairArray& cFoundPairs, const CPointArray& cFoundPoints, const Vector3& cBox1, BOOL bTakePointToPoint = TRUE)
{
    BC_TPOINTPAIR tPointPair;

    for( int i = 0; i < cFoundPoints.Size(); i++)
    {
        tPointPair.tPoint2 = cFoundPoints[i];
        const Vector3& cWorldCoord2 = tPointPair.tPoint2.cWordCoord;
               
        // Find vorony subspace
        for(unsigned int j = 0; j < 3; j++)
        {
            if (cWorldCoord2[j] <= -cBox1[j])
                tPointPair.tPoint1.cBodyRelCoord[j] = -1;
            else
                if (cWorldCoord2[j] <= cBox1[j])            
                    tPointPair.tPoint1.cBodyRelCoord[j] = 0;
                else
                    tPointPair.tPoint1.cBodyRelCoord[j] = 1;
        }
        
        //ASSERT(tPointPair.tPoint1.cBodyRelCoord != VZero3D);
        if (tPointPair.tPoint1.cBodyRelCoord == VZeroInt)
            continue;

        // Find corresponding point on body 1
        TimesByCoord(tPointPair.tPoint1.cWordCoord,cBox1, tPointPair.tPoint1.cBodyRelCoord);
        
        BOOL bPointToPoint = TRUE;
        for(unsigned int j = 0; j < 3; j++)
        {
            if (tPointPair.tPoint1.cBodyRelCoord[j] == 0)
            {   
                tPointPair.tPoint1.cWordCoord[j] = cWorldCoord2[j];
                bPointToPoint = FALSE;
            }

        }

        if (bTakePointToPoint || !bPointToPoint)
        {            
            tPointPair.fDistance = (tPointPair.tPoint1.cWordCoord - cWorldCoord2).Size();
            
            if (tPointPair.fDistance <= g_fTolerance)
            {           
                cFoundPairs.Add(tPointPair);
            }
        }
    }    

    return cFoundPairs.Size();
}


int BoxCollision(M_ContactArray& cFinalContacts, const Vector3& cBox1, const Vector3& cPos1, const CQuaternion& cOrientation1,
                const Vector3& cBox2, const Vector3& cPos2, const CQuaternion& cOrientation2)
{
    CPointArray cFoundPoints;
    cFoundPoints.Reserve(16,16);
    
    CPointPairArray cFoundPairs;
    cFoundPairs.Reserve(16,16);
    
    
    CQuaternion cRelOrientation2 = cOrientation1.GetTransposed() * cOrientation2;
    CQuaternion cRelOrientation1 = cOrientation2.GetTransposed() * cOrientation1;
    
    Vector3 cRelPos2 = cOrientation1.GetTransposed().GetMatrix() * (cPos2 - cPos1);
    Vector3 cRelPos1 = cOrientation2.GetTransposed().GetMatrix() * (cPos1 - cPos2);
    
    Vector3 cBoxTemp(cBox1);
    cBoxTemp += Vector3(g_fTolerance, g_fTolerance, g_fTolerance);
    
    
    
    PointsInBox(cFoundPoints, cBoxTemp, cBox2, cRelOrientation2.GetMatrix(), cRelPos2);
    PointPairs(cFoundPairs, cFoundPoints, cBox1);
    
    CMatrix3DRow cRotMatrix1 = cOrientation1.GetMatrix();
    CMatrix3DRow cRotMatrix2 = cOrientation2.GetMatrix();
    
     int i = 0;
    
    for(;i < cFoundPairs.Size(); i++)
    {  
        cFoundPairs[i].tPoint1.cWordCoord = cRotMatrix1 * cFoundPairs[i].tPoint1.cWordCoord + cPos1;
        cFoundPairs[i].tPoint2.cWordCoord = cRotMatrix2 * TimesByCoord(cBox2,cFoundPairs[i].tPoint2.cBodyRelCoord) + cPos2;        
        
    }
    
    cBoxTemp = cBox2;
    cBoxTemp += Vector3(g_fTolerance, g_fTolerance, g_fTolerance);
    cFoundPoints.Clear();
    
    PointsInBox(cFoundPoints, cBoxTemp, cBox1, cRelOrientation1.GetMatrix(), cRelPos1);    
    PointPairs(cFoundPairs, cFoundPoints, cBox2, FALSE);
    
    for(;i < cFoundPairs.Size(); i++)
    {
        BC_TPOINT tPoint  = cFoundPairs[i].tPoint1;
        cFoundPairs[i].tPoint1 = cFoundPairs[i].tPoint2;
        cFoundPairs[i].tPoint2 = tPoint;
        
        cFoundPairs[i].tPoint2.cWordCoord = cRotMatrix2 * cFoundPairs[i].tPoint2.cWordCoord + cPos2;
        cFoundPairs[i].tPoint1.cWordCoord = cRotMatrix1 * TimesByCoord(cBox1,cFoundPairs[i].tPoint1.cBodyRelCoord) + cPos1;        
    }
    

    LinesToLines(cFoundPairs, cBox1, cBox2, cRelOrientation2.GetMatrix(), cRelPos2);
    for(;i < cFoundPairs.Size(); i++)
    {  
        cFoundPairs[i].tPoint1.cWordCoord = cRotMatrix1 * cFoundPairs[i].tPoint1.cWordCoord + cPos1;
        cFoundPairs[i].tPoint2.cWordCoord = cRotMatrix1 * cFoundPairs[i].tPoint2.cWordCoord + cPos1;        
        
    }

    // Filter Pairs
    //cFinalPairs.Release();
    FilterPairs(cFinalContacts,cFoundPairs);
    
    return MARINA_OK;
}


BOOL ExistSeparationPlane(const Vector3& cBox1, const Vector3& cBox2, const CMatrix3DRow& cToWorld2, const Vector3& cPos2)
{
    Vector3 pcPoints2[8];
    Vector3 cBodyPoint;
    for(int ix = -1; ix < 2; ix+= 2)
    {
        cBodyPoint[0] = (cBox2[0] * ix);

        for(int iy = -1; iy < 2; iy+= 2)
        {
            cBodyPoint[1] = (cBox2[1] * iy);

            for(int iz = -1; iz < 2; iz+= 2)
            {
                cBodyPoint[2] = (cBox2[2] * iz);
                pcPoints2[(ix + 1)*2 + (iy + 1) + (iz + 1)/2] = cToWorld2 * cBodyPoint + cPos2;
            }
        }
    }

    for(unsigned int i = 0; i < 3; i++)
    {
        for(int j = -1; j < 2; j+=2)
        {
            marina_real fPlane = cBox1[i] * j;
            

            unsigned int k = 0;
            for(; k < 8; k++)
            {
                if ((j == 1 && pcPoints2[k][i] <= fPlane) ||
                    (j == -1 && pcPoints2[k][i] >= fPlane))
                    break;
            }

            if ( k == 8)
            {
                // separation plane found
                return TRUE;
            }
        }
    }
    
    return FALSE;
}

BOOL ExistSeparationPlane3(unsigned int iIndexX, unsigned int iIndexY, const CVector2DInt& cLine, const Vector3& cBox1, Vector3* pcPoints2, CVector3DInt* pcRelPoints2) 
{
    //ASSERT(iIndexX < 3);
    //ASSERT(iIndexY < 3);

    marina_real fMinTang1 = -1.0f;
    marina_real fMinTang2 = -1.0f;

    for(unsigned int k = 0; k < 8; k++)
    {
        if (pcRelPoints2[k][iIndexX] != cLine[0] && pcRelPoints2[k][iIndexY] != cLine[1])
           return FALSE;

        if (pcRelPoints2[k][iIndexX] == cLine[0] && pcRelPoints2[k][iIndexY] == cLine[1])
           continue;

        if (pcRelPoints2[k][iIndexX] == cLine[0])
        {
            marina_real fActualTang1 = fabsf((pcPoints2[k][iIndexX] - cLine[0] * cBox1[iIndexX])/(pcPoints2[k][iIndexY] - cLine[1] * cBox1[iIndexY]));
            if (fActualTang1 < fMinTang1 || fMinTang1 == -1.0f)
            {
                fMinTang1 = fActualTang1;                
            }
        }
        else
        {
            ASSERT(pcRelPoints2[k][iIndexY] == cLine[1]);
                        
            /*marina_real fActualTang2 = pcPoints2[k][iIndexY] - cLine[1] * cBox1[iIndexY];
            fActualTang2 /= pcPoints2[k][iIndexX] - cLine[0] * cBox1[iIndexX];
            fActualTang2 = fabsf(fActualTang2);*/

            marina_real fActualTang2 = fabsf((pcPoints2[k][iIndexY] - cLine[1] * cBox1[iIndexY])/(pcPoints2[k][iIndexX] - cLine[0] * cBox1[iIndexX]));
            if (fActualTang2 < fMinTang2 || fMinTang2 == -1.0f)
            {
               fMinTang2 = fActualTang2;                
            }
            
        }

    }

    ASSERT(fMinTang1 != -1);
    ASSERT(fMinTang2 != -1);

    if (fMinTang1 < 1.0/fMinTang2)
        return FALSE;

    return TRUE;
}

BOOL ExistSeparationPlane2(const Vector3& cBox1, const Vector3& cBox2, const CMatrix3DRow& cToWorld2, const Vector3& cPos2)
{
    Vector3 pcPoints2[8];
    Vector3 cBodyPoint;
    for(int ix = -1; ix < 2; ix+= 2)
    {
        cBodyPoint[0] = (cBox2[0] * ix);
        
        for(int iy = -1; iy < 2; iy+= 2)
        {
            cBodyPoint[1] = (cBox2[1] * iy);
            
            for(int iz = -1; iz < 2; iz+= 2)
            {
                cBodyPoint[2] = (cBox2[2] * iz);
                pcPoints2[(ix + 1)*2 + (iy + 1) + (iz + 1)/2] = cToWorld2 * cBodyPoint + cPos2;
            }
        }
    }
    
    CVector3DInt pcRelPoints2[8];
    
    for(unsigned int k = 0; k < 8; k++)
    {
        // Find vorony subspace
        for(unsigned int j = 0; j < 3; j++)
        {
            if (pcPoints2[k][j] <= -cBox1[j])
                pcRelPoints2[k][j] = -1;
            else
                if (pcPoints2[k][j] <= cBox1[j])            
                    pcRelPoints2[k][j] = 0;
                else
                    pcRelPoints2[k][j] = 1;
        }

        if (pcRelPoints2[k] == VZeroInt)
            return FALSE;
    }

    // Find separation plane from body if exist.
    CVector3DInt cPotPlane = pcRelPoints2[0];

    CVector3DInt cTemp;
    for(unsigned int k = 1; k < 8; k++)
    {
        cTemp = cPotPlane & pcRelPoints2[k];

        if (cTemp[0] == -1)
            cTemp[0] = 0;

        if (cTemp[1] == -1)
            cTemp[1] = 0;

        if (cTemp[2] == -1)
            cTemp[2] = 0;

        cPotPlane &= cTemp;

        if (cPotPlane == VZeroInt)
            break;        
    }

    if (cPotPlane != VZeroInt)
        return TRUE;

    // Find plane going through body's line    
    CVector2DInt cLine;

    for( int ix = -1; ix < 2; ix += 2)
    {
        cLine[0] = (ix);
        for( int iy = -1; iy < 2; iy += 2)
        {
            cLine[1] = (iy);

            if (ExistSeparationPlane3( 0, 1, cLine, cBox1, pcPoints2, pcRelPoints2))
                return TRUE;

            if (ExistSeparationPlane3( 0, 2, cLine, cBox1, pcPoints2, pcRelPoints2))
                return TRUE;

            if (ExistSeparationPlane3( 1, 2, cLine, cBox1, pcPoints2, pcRelPoints2))
                return TRUE;
        }
    }


    return FALSE;
}


BOOL BoxIntersection( const Vector3& cBox1, const Vector3& cPos1, const CQuaternion& cOrientation1,
                const Vector3& cBox2, const Vector3& cPos2, const CQuaternion& cOrientation2)
{
    CQuaternion cRelOrientation2 = cOrientation1.GetTransposed() * cOrientation2;
    CQuaternion cRelOrientation1 = cOrientation2.GetTransposed() * cOrientation1;
    
    Vector3 cRelPos2 = cOrientation1.GetTransposed().GetMatrix() * (cPos2 - cPos1);
    Vector3 cRelPos1 = cOrientation2.GetTransposed().GetMatrix() * (cPos1 - cPos2);
 
    if (ExistSeparationPlane(cBox1, cBox2, cRelOrientation2.GetMatrix(), cRelPos2))
        return FALSE;

    return !ExistSeparationPlane2(cBox2, cBox1, cRelOrientation1.GetMatrix(), cRelPos1);
}

int BoxPlaneCollision(M_ContactArray& cFinalContacts, const Vector3& cPlaneNormal, marina_real fPlaneCoef, const Vector3& cBox, const Vector3& cPos, const CQuaternion& cOrientation)
{

    //Vector3 cPlaneOrigin = cPlaneNormal * ( - fPlaneCoef / cPlaneNormal.GetSquareLength());

    Vector3 cBodyPoint;
    Vector3 cWordCoord;
    CMatrix3DRow cRotMatrix = cOrientation.GetMatrix();

    for(int ix = -1; ix < 2; ix+= 2)
    {
        cBodyPoint[0] = (cBox[0] * ix);
        
        for(int iy = -1; iy < 2; iy+= 2)
        {
            cBodyPoint[1] = (cBox[1] * iy);
            
            for(int iz = -1; iz < 2; iz+= 2)
            {
                cBodyPoint[2] = (cBox[2] * iz);
                cWordCoord = cRotMatrix * cBodyPoint + cPos;

                marina_real fDistance = cPlaneNormal * cWordCoord + fPlaneCoef;

                if (fDistance < g_fTolerance)
                {
                    M_ContactPoint tContactPoint;
		            //tContactPoint.pcPedestal = NULL;

                    tContactPoint.pos = cWordCoord;
                    tContactPoint.under =  g_fTolerance  - fDistance;
                    
                    tContactPoint.dir[0] = cPlaneNormal;
                    
                    // Find pedestal axes
                    tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct(Vector3(1,0,0));
                    if (tContactPoint.dir[1].Size() < 0.1f) // is new vector correct
                        tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct(Vector3(0,1,0));
                    
                    tContactPoint.dir[2] = tContactPoint.dir[0].CrossProduct(tContactPoint.dir[1]);
                    
                    tContactPoint.dir[1].Normalize();
                    tContactPoint.dir[2].Normalize();
                    
                    cFinalContacts.Add(tContactPoint);
                }
            }
        }
    }

    return cFinalContacts.Size();
}

BOOL BoxPlaneIntersection(const Vector3& cPlaneNormal, marina_real fPlaneCoef, const Vector3& cBox, const Vector3& cPos, const CQuaternion& cOrientation)
{

    //Vector3 cPlaneOrigin = cPlaneNormal * ( - fPlaneCoef / cPlaneNormal.GetSquareLength());

    Vector3 cBodyPoint;
    Vector3 cWordCoord;
    CMatrix3DRow cRotMatrix = cOrientation.GetMatrix();

    for(int ix = -1; ix < 2; ix+= 2)
    {
        cBodyPoint[0] = (cBox[0] * ix);
        
        for(int iy = -1; iy < 2; iy+= 2)
        {
            cBodyPoint[1] = (cBox[1] * iy);
            
            for(int iz = -1; iz < 2; iz+= 2)
            {
                cBodyPoint[2] = (cBox[2] * iz);
                cWordCoord = cRotMatrix * cBodyPoint + cPos;

                if (0 >= cPlaneNormal * cWordCoord + fPlaneCoef)
                    return TRUE;                
            }
        }
    }

    return FALSE;
}