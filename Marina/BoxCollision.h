#ifndef __BOXCOLLISION_H__
#define __BOXCOLLISION_H__

#include "RestContactResolver.hpp"


typedef struct
{
	Vector3 cWordCoord;
	CVector3DInt cBodyRelCoord;
} BC_TPOINT;

TypeIsSimple(BC_TPOINT)

typedef struct
{
    BC_TPOINT tPoint1;
    BC_TPOINT tPoint2;
    marina_real fDistance;
   // Vector3 cNormala;
} BC_TPOINTPAIR;
TypeIsSimple(BC_TPOINTPAIR)

typedef AutoArray<BC_TPOINT> CPointArray;
typedef AutoArray<BC_TPOINTPAIR> CPointPairArray;

//int PointsInBox(CPointArray& cFoundPoints, const Vector3& cBox1, const Vector3& cBox2, 
//                const CMatrix3DRow& cToWorld2, const Vector3& cPos2);

//int PointPairs(CPointPairArray& cFoundPairs, const CPointArray& cFoundPoints, const Vector3& cBox1, BOOL bTakePointToPoint = TRUE);
//

int BoxCollision(M_ContactArray& cContactPoints, const Vector3& cBox1, const Vector3& cPos1, const CQuaternion& cOrientation1,
                const Vector3& cBox2, const Vector3& cPos2, const CQuaternion& cOrientation2);

BOOL BoxIntersection( const Vector3& cBox1, const Vector3& cPos1, const CQuaternion& cOrientation1,
                const Vector3& cBox2, const Vector3& cPos2, const CQuaternion& cOrientation2);

int BoxPlaneCollision(M_ContactArray& cFinalContacts, const Vector3& cPlaneNormal, marina_real fPlaneCoef, const Vector3& cBox, const Vector3& cPos, const CQuaternion& cOrientation);
BOOL BoxPlaneIntersection(const Vector3& cPlaneNormal, marina_real fPlaneCoef, const Vector3& cBox, const Vector3& cPos, const CQuaternion& cOrientation);

#endif //__BOXCOLLISION_H__