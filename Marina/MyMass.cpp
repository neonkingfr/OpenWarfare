#ifndef _MARINA_PROJECT 
#include <Poseidon/lib/wpch.hpp>
#else
#define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES

#include "marina.hpp"
#include "inc.h"
#include "MyMass.h"

void CMass::SetBox(const marina_real fMass, const Vector3& cBox)
{
    _fMass = fMass;

    _cMassCenter = VZero;
    _cRelPoint = VZero;

    _cInertiaTensor = M3Zero;

    _cInertiaTensor(0,0) = fMass * (cBox[1] * cBox[1] + cBox[2] * cBox[2]) / 12.0f;
    _cInertiaTensor(1,1) = fMass * (cBox[0] * cBox[0] + cBox[2] * cBox[2]) / 12.0f;
    _cInertiaTensor(2,2) = fMass * (cBox[0] * cBox[0] + cBox[1] * cBox[1]) / 12.0f;
}

void CMass::Translate(const Vector3& cDelta)
{
    //_cMassCenter = cDelta;
    _cInertiaTensor(0,0) += _fMass * ( 2 * cDelta[1] * _cMassCenter[1] + 2 * cDelta[2] * _cMassCenter[2] + 
        cDelta[1] * cDelta[1] + cDelta[2] * cDelta[2] );

    _cInertiaTensor(1,1) += _fMass * ( 2 * cDelta[0] * _cMassCenter[0] + 2 * cDelta[2] * _cMassCenter[2] + 
        cDelta[0] * cDelta[0] + cDelta[2] * cDelta[2] );

    _cInertiaTensor(2,2) += _fMass * ( 2 * cDelta[0] * _cMassCenter[0] + 2 * cDelta[1] * _cMassCenter[1] + 
        cDelta[0] * cDelta[0] + cDelta[1] * cDelta[1] );

    _cInertiaTensor(1,0) = (_cInertiaTensor(0,1) += _fMass * (cDelta[1] * _cMassCenter[0] + cDelta[0] * _cMassCenter[1] + 
        cDelta[1] * cDelta[0]));

    _cInertiaTensor(1,2) = (_cInertiaTensor(2,1) += _fMass * (cDelta[1] * _cMassCenter[2] + cDelta[2] * _cMassCenter[1] + 
        cDelta[1] * cDelta[2]));

    _cInertiaTensor(2,0) = (_cInertiaTensor(0,2) += _fMass * (cDelta[2] * _cMassCenter[0] + cDelta[0] * _cMassCenter[2] + 
        cDelta[2] * cDelta[0]));

    _cMassCenter += cDelta;
}

/*void CMass::Rotate(const CQuaternion& cOrientation)
{    
  Matrix3 cRotationMatrixRow = cOrientation.GetMatrix();    

  _cInertiaTensor =  cRotationMatrixRow * (_cInertiaTensor * cRotationMatrixRow.GetTransposed());
  _cMassCenter = cRotationMatrixRow * _cMassCenter;
}*/

const CMass& CMass::operator+=(const CMass& cSecond)
{
  CMass add(cSecond);

  add.SetRelPoint(_cRelPoint);

  _cInertiaTensor += cSecond._cInertiaTensor;  

  float totalMass = _fMass + cSecond._fMass;
  _cMassCenter = (_cMassCenter * _fMass + cSecond._cMassCenter * cSecond._fMass) / totalMass;    
  _fMass = totalMass;

  return *this;
}

const CMass& CMass::operator-=(const CMass& cSecond)
{
  CMass remove(cSecond);

  remove.SetRelPoint(_cRelPoint);

  _cInertiaTensor -= cSecond._cInertiaTensor;  

  float totalMass = _fMass - cSecond._fMass;
  _cMassCenter = (_cMassCenter * _fMass - cSecond._cMassCenter * cSecond._fMass) / totalMass;    
  _fMass = totalMass;

  return *this;
}
void CMass::SetRelPoint(const Vector3& newRelPos)
{
  Vector3 cDelta = _cRelPoint - newRelPos;
  _cInertiaTensor(0,0) += _fMass * (2 * cDelta[1] * _cMassCenter[1] + 2 * cDelta[2] * _cMassCenter[2] + 
    cDelta[1] * cDelta[1] + cDelta[2] * cDelta[2] );
  _cInertiaTensor(1,1) += _fMass * (2 * cDelta[0] * _cMassCenter[0] + 2 * cDelta[2] * _cMassCenter[2] + 
    cDelta[0] * cDelta[0] + cDelta[2] * cDelta[2] );
  _cInertiaTensor(2,2) += _fMass * (2 * cDelta[0] * _cMassCenter[0] + 2 * cDelta[1] * _cMassCenter[1] + 
    cDelta[0] * cDelta[0] + cDelta[1] * cDelta[1] );

  _cInertiaTensor(1,0) = (_cInertiaTensor(0,1) += _fMass * (cDelta[1] * _cMassCenter[0] + cDelta[0] * _cMassCenter[1] + 
    cDelta[1] * cDelta[0]));
  _cInertiaTensor(1,2) = (_cInertiaTensor(2,1) += _fMass * (cDelta[1] * _cMassCenter[2] + cDelta[2] * _cMassCenter[1] + 
    cDelta[1] * cDelta[2]));
  _cInertiaTensor(2,0) = (_cInertiaTensor(0,2) += _fMass * (cDelta[2] * _cMassCenter[0] + cDelta[0] * _cMassCenter[2] + 
    cDelta[2] * cDelta[0]));

  _cRelPoint = newRelPos;
}

void CMass::AddMassPoint(const marina_real mass, const Vector3& point)
{
  float totalMass = _fMass + mass;

  _cMassCenter = (_cMassCenter * _fMass + point * mass) / totalMass;
  _fMass = totalMass;

  Vector3 r = point - _cRelPoint;
  _cInertiaTensor(0,0) += mass * (r[1] * r[1] + r[2] * r[2]);
  _cInertiaTensor(1,1) += mass * (r[0] * r[0] + r[2] * r[2]);
  _cInertiaTensor(2,2) += mass * (r[0] * r[0] + r[1] * r[1]);

  _cInertiaTensor(1,0) = (_cInertiaTensor(0,1) += mass * r[1] * r[0]);
  _cInertiaTensor(1,2) = (_cInertiaTensor(2,1) += mass * r[1] * r[2]);
  _cInertiaTensor(2,0) = (_cInertiaTensor(0,2) += mass * r[2] * r[0]); 
}
#endif