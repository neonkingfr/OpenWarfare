#ifdef _MSC_VER
#pragma once
#endif

#ifndef __SIMULATION_H__
#define __SIMULATION_H__

//#include "inc.h"
#include "RestContactResolver.hpp"

TypeIsSimple(BodyGroup *);

class SimulationRB
{
protected:
  //RBContactArray _cContactPoints;
  typedef AutoArray<BodyGroup *> M_PGroupArray;
  M_PGroupArray _cGroups;
  RigidBodyArray _cBodies; 
  //static marina_real _fTolerance;
  Vector3 _externalAccel; //typicaly gravitational acceleration.

  int _simCycle; 
  // used in SimmulateOneStep
  int _iGroup; 
  int _phase;

  ImpulseContactResolver * _pcImpulseResolver;
  RestContactResolver * _pcRestResolver;

  void Integrate(marina_real& fTime);
  void ReleaseGroups(BOOL bWithFrozen /*= FALSE*/);
  void CollisionDetection();
  void CollisionDetection(RBContactArray& cContacts);
  BOOL IsIntersection();
  void ForceInterFree();

#if _ENABLE_BREAKABLE
  virtual bool BreakBodies(SeparateGeomArray & sep) = 0;
  virtual bool CanBreakBodies(SeparateGeomArray & sep) = 0;  
#endif

public:
  SimulationRB():  _simCycle(0), _externalAccel(VZero), _iGroup(0), _phase(0) {};
  ~SimulationRB() {ReleaseGroups(TRUE);};

  RigidBodyArray& GetBodies() {return _cBodies;};
  void AddBody(Ref<RigidBodyObject> body) {_cBodies.Add(body);};  
  void SetExternalAccel(Vector3Val vec) {_externalAccel = vec;};
  void Simulate(marina_real fTime);
  int SimulateOneStep(marina_real fTime);

  void Reset();

  // for debuging
  virtual void AddArrow(Vector3Val pos,Vector3Val dir, float size, DWORD color) {};
  virtual void ResetArrows() {};
};


#endif //__SIMULATION_H__