#ifdef _MSC_VER
#pragma once
#endif

#ifndef __RIGIDBODY_H__
#define __RIGIDBODY_H__

//#include <Es/essencepch.hpp>
#include <Es/Containers/array.hpp>
#include <El/Math/math3d.hpp>
#include <El/ParamArchive/paramArchive.hpp>
#include "RigidBodyState.h"


/** 
  * Class for rigid body implementation.
  */
class IRigidBody : public IRigidBodyState 
{
protected:

	Vector3 _forceBank; // Force applied into body.
	Vector3 _torqueBank;

	//Vector3 _cCorrectionForceBank;    // Correction forces  applied into body. 
	//Vector3 _cCorrectionTorgueBank; // They are handled by integrator separately (no speed withou force)	
	
	//Vector3 _cNextStepForceBank; // Force that will be applied in next step
	//Vector3 _cNextStepTorgueBank;
	
private:
	// BackUp for integration
	Vector3 _posBackUp;
	QuaternionRB _orientationBackUp;

	Vector3 _velocityBackUp;
	
	Vector3 _angularMomentumBackUp;

	// Steps for integration
	Vector3 _acceleration;
	Vector3 _torque;

	QuaternionRB _orientationVelocity;
	QuaternionRB _orientationAcceleration;

  unsigned long _marker; // Marker used for instance in division of the collisions into collision groups.
	
public:
	//-----------------------------
	// Constructors
	//-----------------------------	
  IRigidBody() : _forceBank(VZero),  _torqueBank(VZero) {};
  virtual ~IRigidBody() {};

	//-----------------------------
	// Methods
	//-----------------------------

	Vector3 AccelerationAtPoint(const Vector3& cPoint, 
		TRIBOOL bPlus = TB_TRUE) const { return IRigidBodyState::AccelerationAtPoint( _forceBank, _torqueBank, cPoint, bPlus);};

	void PrepareIntegration();
	void DoIntegration(rb_real fTime);	
  virtual bool IsMoving() const = 0;

  virtual void SimulateInnerProcesses(rb_real fTime) {};
  virtual float GetMaxSimTime() const  = 0; 
  virtual void Enable(bool enable = true) {}; 
  virtual bool IsBodyRef(ParamArchive& ar) const = 0; 
  virtual LSError SaveBodyRef(ParamArchive& ar) = 0; 

	//-----------------------------
	// Get Methods
	//-----------------------------		
	
	unsigned long GetMarker() const {return _marker;};

	const Vector3& GetTorgueBank() const {return _torqueBank;};
	const Vector3& GetForceBank() const {return _forceBank;};
	/*const Vector3& GetNextStepTorgueBank() const {return _cNextStepTorgueBank;};
	const Vector3& GetNextStepForceBank() const {return _cNextStepForceBank;};
	const Vector3& GetCorrectionTorgueBank() const {return _cCorrectionTorgueBank;};
	const Vector3& GetCorrectionForceBank() const {return _cCorrectionForceBank;};*/	

	//-----------------------------
	// Set Methods
	//-----------------------------
	void ZeroBanks() {_forceBank = VZero; _torqueBank = VZero;
			/*_cCorrectionForceBank = VZero; _cCorrectionTorgueBank = VZero; */};
	void ZeroNextStepBanks() {/*cNextStepForceBank = VZero; _cNextStepTorgueBank = VZero;*/};
	void AddForceAtPointToBanks(const Vector3& cForce, const Vector3& cPoint);
	//void AddCorrectionForceAtPointToBanks(const Vector3& cForce, const Vector3& cPoint);
  void AddForceToBanks(const Vector3& force) {_forceBank += force; Assert(_forceBank.IsFinite());};
  void AddTorqueToBanks(const Vector3& torque) {_torqueBank += torque; Assert(_torqueBank.IsFinite());};


	//void AddNextStepForceToBanks(const Vector3& cForce) {_cNextStepForceBank += cForce;};
	
	void SetMarker(unsigned long dwMarker) {_marker = dwMarker;};	
  //-----------------------------
  // Transforms 
  //-----------------------------

  virtual Vector3 DirectionBodyToWorld(Vector3Val dirBody) const = 0;
  virtual Vector3 DirectionWorldToBody(Vector3Val dirWorld) const = 0;
  virtual Vector3 PositionBodyToWorld(Vector3Val posBody) const = 0;
  virtual Vector3 PositionWorldToBody(Vector3Val posWorld) const = 0;
};
 
#define NUMERICAL_ERROR 0.00001f

__forceinline bool IsNumericalZero(float fValue)
{
  return ((fValue <= NUMERICAL_ERROR) && (fValue >= -NUMERICAL_ERROR));
}
#endif