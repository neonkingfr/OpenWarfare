#ifndef __MY_ASSERT_H__
#define __MY_ASSERT_H__

//#include <drawstuff/drawstuff.h>

void save_bodies(const char* pszPath);
void save_bodies_binary(const char* pszPath);

#ifdef _DEBUG

#ifdef ASSERT
#undef ASSERT
#endif

#define ASSERT(a) {if (!(a)) {\
                    /*dsPrint(""#a"");*/ \
                    /*save_bodies("marina\\scenes\\assert.sce");*/ \
                    /*save_bodies_binary("marina\\scenes\\assert.sc2");*/ \
                    /*while(true){};*/ DebugBreak(); }}
#else

#ifdef ASSERT
#undef ASSERT
#endif

#define ASSERT(a) {if (!(a)) {\
                    /*dsPrint(""#a"");*/ \
                    /*save_bodies("marina\\scenes\\assert.sce"); */\
                    /*save_bodies_binary("marina\\scenes\\assert.sc2");*/ \
                    /*while(true){}; */DebugBreak(); }}
#endif                   

#endif //__MY_ASSERT_H__