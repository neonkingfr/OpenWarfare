@ECHO OFF

REM Report in news only if regular build fails

IF [%1] == [] GOTO badparam
IF [%2] == [] GOTO badparam
IF [%3] == [] GOTO badparam


ECHO Reporting to newsgroup...

c:\bia\blat\blat.exe %2 -to news.bistudio.com -u biainternal -groups %3 -s %1


GOTO ok

:badparam
ECHO.  
ECHO Batch 'toNews.bat' posting to news. Usage: toNews SUBJECT "MESSAGE TO BE SENT" GROUP
ECHO.  


:ok




