@echo off
REM %1 is for name of computer

time /T
title "[ %1 ] Start building VBS3"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Start building VBS3"
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS3 started"

echo Getting tools...
title "Getting tools..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Getting tools..."
call W:\c\BuildServer\BIA\Build\GetTools.bat

echo Getting sources...
title "Getting sources..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Getting sources..."
call W:\c\BuildServer\BIA\Build\GetSourcesVBS3.bat

echo Update configs on O...
rem xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.cpp o:\fp\Bin /R /Y
rem xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.csv o:\fp\Bin /R /Y
rem xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.h* o:\fp\Bin /R /Y
rem xcopy w:\c\Archive\VBS2\Poseidon\cfg\*.hpp o:\fp\Bin /R /Y
rem xcopy w:\c\Archive\VBS2\Poseidon\lib\dikcodes.h o:\fp\Bin /R /Y
rem xcopy w:\c\Archive\VBS2\Poseidon\lib\languages.hpp o:\fp\Bin /R /Y
rem xcopy W:\c\Archive\VBS2\Poseidon\lib\UI\resincl.hpp o:\fp\Bin /R /Y

rem Disable incremental linking on the build server
rem del O:\fp\VBS2*.exe O:\fp\VBS2*.ilk

echo Building VBS3 Alpha ...
title "[ %1 ] VBS Std: (1/4) Building VBS3 Alpha ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (1/7) Building VBS3 Alpha ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS3\ArmA2Ext.sln /build /prj=lib2005 /cfg="VBS3_Alpha|Win32"
if errorlevel 1 goto buildfail

echo Distributing VBS3 Alpha ...
title "[ %1 ] VBS3 Std: (2/4) Distributing VBS3 Alpha ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (4/7) Distributing VBS3 Alpha ..."
call W:\c\BuildServer\BIA\Build\distribute_VBS3_Alpha.bat

python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building and sending of VBS3 succeeded"

echo Building Binarization ...
title "[ %1 ] VBS3 Std: (3/4) Building Binarization"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (7/7) Building Binarization"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Archive\VBS3\binarize\binarize.sln /build /prj=binarize /cfg="VBS3_Alpha|Win32"
if errorlevel 1 goto buildbinarizefail
rem xcopy W:\c\Archive\VBS2\Poseidon\binarize\Release\*.exe P:\tools_dev\Latest\binarize /R /Y
rem svn.exe commit -m "[DEV] #define BUILD_NO XXXXX" "P:\tools_dev\Latest\binarize\binarize.exe"

echo Distributing VBS3 Binarization Alpha ...
title "[ %1 ] VBS3 Std: (4/4) Distributing VBS3 Binarization Alpha ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (4/4) Distributing VBS3 Binarization Alpha ..."
call W:\c\BuildServer\BIA\Build\distribute_VBS3_Binarization_Alpha.bat

python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building standard versions succeeded"

time /T
title "All Done! Hungry for new tasks."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] All Done! Hungry for new tasks."
goto end

:buildfail
time /T
title "Building of VBS3 failed"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Building of VBS3 failed."
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS3 failed"
goto end

:buildbinarizefail
time /T
title "Building of binarization failed"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Building of VBS3 binarization failed."
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS3 binarization failed"
goto end

:end
