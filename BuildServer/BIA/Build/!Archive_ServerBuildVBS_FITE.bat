@echo off
REM %1 is for name of computer

time /T
title "[ %1 ] Start building VBS Standard Versions"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Start building VBS FITE"
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS FITE started"

echo Getting sources...
title "Getting sources..."
svn update --non-interactive W:\c\Archive\VBS2_FITE

echo Building VBS Developer ...
title "[ %1 ] VBS Std: (1/9) Building VBS Developer ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (1/9) Building VBS Developer ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_FITE\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_Rel_USMC|Win32"
if errorlevel 1 goto buildfail

python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building standard versions succeeded"

time /T
title "All Done! Hungry for new tasks."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] All Done! Hungry for new tasks."
goto end

:buildfail
time /T
title "Building of VBS failed"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Building of VBS failed."
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS failed"
goto end

:end
