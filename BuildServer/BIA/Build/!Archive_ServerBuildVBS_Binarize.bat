@echo off
REM %1 is for name of computer
SET TOTALSTEPS=6

SET SVNPATH="C:\Program Files\SlikSvn\bin\svn.exe"

time /T
title "[ %1 ] Start building VBS All Versions"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] Start building VBS Binarize"
python W:\c\BuildServer\BIA\Build\netsend.py All "[ %1 ] Building of VBS Binarize started"

echo Getting tools...
title "Getting tools..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] Getting tools..."
call W:\c\BuildServer\BIA\Build\GetTools.bat

echo Getting sources...
title "Getting sources..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] Getting sources..."
call W:\c\BuildServer\BIA\Build\GetSourcesVBS2.bat

echo Update configs on O...
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.cpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.csv o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.h* o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\*.hpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\dikcodes.h o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\languages.hpp o:\fp\Bin /R /Y
xcopy W:\c\Archive\VBS2\Poseidon\lib\UI\resincl.hpp o:\fp\Bin /R /Y

rem Disable incremental linking on the build server
del O:\fp\VBS2*.exe O:\fp\VBS2*.ilk

echo Building Binarization Alpha ...
title "[ %1 ] VBS All: (1/%TOTALSTEPS%) Building Binarization Alpha"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (1/%TOTALSTEPS%) Building Binarization Alpha"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /rebuild /prj=binarize /cfg="Alpha|Win32"
if errorlevel 1 goto buildbinarizefail
xcopy W:\c\Archive\VBS2\Poseidon\binarize\Alpha\binarize_Alpha.exe P:\tools\binarize /R /Y

echo Building Binarization Beta ...
title "[ %1 ] VBS All: (2/%TOTALSTEPS%) Building Binarization Beta"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (2/%TOTALSTEPS%) Building Binarization Beta"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /rebuild /prj=binarize /cfg="Beta|Win32"
if errorlevel 1 goto buildbinarizefail
xcopy W:\c\Archive\VBS2\Poseidon\binarize\Beta\binarize_Beta.exe P:\tools\binarize /R /Y

echo Building Binarization Release ...
title "[ %1 ] VBS All: (3/%TOTALSTEPS%) Building Binarization Release"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (3/%TOTALSTEPS%) Building Binarization Release"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /rebuild /prj=binarize /cfg="Release|Win32"
if errorlevel 1 goto buildbinarizefail
xcopy W:\c\Archive\VBS2\Poseidon\binarize\Release\binarize.exe P:\tools\binarize /R /Y

echo Building Binarization Alpha64 ...
title "[ %1 ] VBS All: (4/%TOTALSTEPS%) Building Binarization Alpha64"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (4/%TOTALSTEPS%) Building Binarization Alpha64"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /rebuild /prj=binarize /cfg="Alpha|x64"
if errorlevel 1 goto buildbinarizefail
xcopy W:\c\Archive\VBS2\Poseidon\binarize\Alpha64\binarize_Alpha64.exe P:\tools\binarize /R /Y

echo Building Binarization Beta64 ...
title "[ %1 ] VBS All: (5/%TOTALSTEPS%) Building Binarization Beta64"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (5/%TOTALSTEPS%) Building Binarization Beta64"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /rebuild /prj=binarize /cfg="Beta|x64"
if errorlevel 1 goto buildbinarizefail
xcopy W:\c\Archive\VBS2\Poseidon\binarize\Beta64\binarize_Beta64.exe P:\tools\binarize /R /Y

echo Building Binarization Release64 ...
title "[ %1 ] VBS All: (6/%TOTALSTEPS%) Building Binarization Release"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (6/%TOTALSTEPS%) Building Binarization Release64"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /rebuild /prj=binarize /cfg="Release|x64"
if errorlevel 1 goto buildbinarizefail
xcopy W:\c\Archive\VBS2\Poseidon\binarize\Release64\binarize64.exe P:\tools\binarize /R /Y
%SVNPATH% commit P:\tools\binarize\*.exe -F w:\c\Archive\VBS2\Poseidon\lib\appInfoRev.h --force-log


python W:\c\BuildServer\BIA\Build\netsend.py All "[ %1 ] Building binarization succeeded"

time /T
title "All Done! Hungry for new tasks."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] All Done! Hungry for new tasks."
goto end

:buildfail
time /T
title "Building of VBS failed"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] Building of VBS failed."
python W:\c\BuildServer\BIA\Build\netsend.py All "[ %1 ] Building of VBS failed"
goto end

:buildbinarizefail
time /T
title "Building of binarization failed"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] Building of VBS binarization failed."
python W:\c\BuildServer\BIA\Build\netsend.py All "[ %1 ] Building of VBS binarization failed"
goto end

:end
