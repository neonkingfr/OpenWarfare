@echo off
REM %1 is for name of computer

echo Getting tools...
call w:\bis\Build\GetTools.bat

echo Update configs on O...
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.cpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.csv o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.h* o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\*.hpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\dikcodes.h o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\languages.hpp o:\fp\Bin /R /Y

echo Building VBS Dev ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /SILENT /build /prj=lib2005 /cfg="VBS2|Win32"
if errorlevel 1 goto buildfail

echo Creating shaders ...
call o:\fp\VBS2.exe -nosplash -window -generateShaders

echo Distributing VBS ...
cmd /c w:\tools\hasp\envelope.com -p w:\tools\hasp\dev.cfgx

echo Packing bin.pbo ...
p:\tools\oxygen\O2Script.exe -d -a "p:\tools\PBOPacker\PBOPacker.bio2s" O:\fp\Bin 8 -export=Q:\vbs2_latest_builds\dev\engine\dta 

net send %1 Building and sending of Dev VBS succeeded

echo Building VBS Release ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /SILENT /build /prj=lib2005 /cfg="VBS2_Release|Win32"
if errorlevel 1 goto buildfail

echo Building VBS Dev LVC ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /SILENT /build /prj=lib2005 /cfg="VBS2 LVC|Win32"
if errorlevel 1 goto buildfail

echo Building VBS Release LS ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /SILENT /build /prj=lib2005 /cfg="VBS2_Rel_LS|Win32"
if errorlevel 1 goto buildfail

echo Building VBS Release LVC ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /SILENT /build /prj=lib2005 /cfg="VBS2_Rel_LVC|Win32"
if errorlevel 1 goto buildfail

goto end

:buildfail
net send %1 Building of VBS failed
goto end

:end