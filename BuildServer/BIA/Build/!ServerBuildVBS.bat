@echo off
REM %1 is for name of computer

echo Getting tools...
call w:\bis\Build\GetTools.bat

echo Getting sources...
call w:\bis\Build\GetSourcesSAW.bat

echo Update configs on O...
xcopy w:\c\Poseidon\cfg\Bin o:\VBS2X\Bin /E /I /R /Y
xcopy w:\c\Poseidon\cfg\*.hpp o:\VBS2X\Bin /R /Y
xcopy w:\c\Poseidon\lib\dikcodes.h o:\VBS2X\Bin /R /Y
xcopy w:\c\Poseidon\lib\languages.hpp o:\VBS2X\Bin /R /Y

echo Building VBS (ArmA2) ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Poseidon\ArmA2.sln /build /prj=lib2005 /cfg="VBS2|Win32"
if errorlevel 1 goto buildfail

echo Creating shaders ...
call o:\VBS2X\VBS2.exe -nosplash -window -generateShaders

echo Distributing VBS (ArmA2) ...
cmd /c w:\tools\hasp\envelope.com -p w:\tools\hasp\dev_vbs2x.cfgx

echo Packing bin.pbo ...
p:\tools\oxygen\O2Script.exe -d -a "p:\tools\PBOPacker\PBOPacker.bio2s" O:\VBS2X\Bin 8 -export=Q:\vbs2_latest_builds\vbs2x\engine\dta -tools=P:\tools_dev\vbs2x

net send %1 Building of VBS (ArmA2) succeeded

echo Building ArmA2 ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Poseidon\ArmA2.sln /build /prj=lib2005 /cfg="Release|Win32"
if errorlevel 1 goto buildarma2fail

echo Building Binarization (ArmA2) ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Poseidon\binarize\binarize.sln /build /prj=binarize /cfg="Release|Win32"
if errorlevel 1 goto buildbinarizefail

goto end

:buildfail
net send %1 Building of VBS (ArmA2) failed
goto end

:buildarma2fail
net send %1 Building of ArmA2 failed
goto end

:buildbinarizefail
net send %1 Building of Binarize (ArmA2) failed
goto end

:end