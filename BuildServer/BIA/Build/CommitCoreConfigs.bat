@echo Copying core configs to P: drive
@xcopy w:\C\Archive\VBS2\Poseidon\cfg\Bin p:\vbs2\Doc\Core /i /y
@ren p:\vbs2\Doc\Core\*.txt *.nepripona_909
@ren p:\vbs2\Doc\Core\*.cpp *.txt
@ren p:\vbs2\Doc\Core\*.nepripona_909 *.txt
@del p:\vbs2\Doc\Core\*.nepripona_909

@echo Commiting core configs to SVN
@p:
@cd p:\vbs2\doc\core
@svn commit -m "Engine core configs" .