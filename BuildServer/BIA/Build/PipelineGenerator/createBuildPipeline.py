import sys
import os
import string
import shutil
import os
from os.path import join, getsize
import fileinput

if sys.version_info < (2, 6):
	print "This script requires python version 2.6 and later.\nVersion installed is " + str(sys.version_info)
	sys.exit()

from shutil import copytree, ignore_patterns

DATA_DIRECTORY = "!Data"
PLACEHOLDER_STRING = "$$$$$$"
PLACEHOLDER_STRING_DOT = "$$##$$"
SVN_SUBDIRECTORY = ".svn"

##########################################################################

def ChangeFileNames(dirPath, buildNumber):
	for root, dirs, files in os.walk(dirPath):
		for name in files:
			if string.find(name, PLACEHOLDER_STRING) >= 0:
				newName = string.replace(name, PLACEHOLDER_STRING, buildNumber)
				os.rename(os.path.join(root, name), os.path.join(root, newName))
				print "File " + name + " renamed to " + newName

##########################################################################

def ChangeDirNames(dirPath, buildNumber):
	for root, dirs, files in os.walk(dirPath):
		for name in dirs:
			if string.find(name, PLACEHOLDER_STRING) >= 0:
				newName = string.replace(name, PLACEHOLDER_STRING, buildNumber)
				os.rename(os.path.join(root, name), os.path.join(root, newName))
				print "Directory " + name + " renamed to " + newName

##########################################################################

def ChangeFileContent(filePath, buildNumber, buildNumberOrig):
	for line in fileinput.FileInput(filePath, inplace=1):
		line = line.replace(PLACEHOLDER_STRING, buildNumber)
		line = line.replace(PLACEHOLDER_STRING_DOT, buildNumberOrig)
		print line,

##########################################################################

def ChangeFilesContent(dirPath, buildNumber, buildNumberOrig):
	for root, dirs, files in os.walk(dirPath):
		for name in files:
			ChangeFileContent(os.path.join(root, name), buildNumber, buildNumberOrig)
			print "File " + name + " content changed."

##########################################################################

def main():
	if len(sys.argv) < 2:
		print "Usage: createBuildPipeline.py build_number\nExample: createBuildPipeline.py 1.40\n";
		raw_input('Press enter to end the script...')
		sys.exit()

	BUILD_NUMBER = sys.argv[1];
	BUILD_NUMBER_INTERNAL = string.replace(BUILD_NUMBER, ".", "_")

	print "Creating pipeline for build " + BUILD_NUMBER + "...\n"

	copytree(DATA_DIRECTORY, BUILD_NUMBER_INTERNAL, ignore=ignore_patterns(SVN_SUBDIRECTORY))
	print "Directory " + BUILD_NUMBER_INTERNAL + " created.\n"

	ChangeDirNames(BUILD_NUMBER_INTERNAL, BUILD_NUMBER_INTERNAL)
	ChangeFileNames(BUILD_NUMBER_INTERNAL, BUILD_NUMBER_INTERNAL)
	ChangeFilesContent(BUILD_NUMBER_INTERNAL+"/Q", BUILD_NUMBER_INTERNAL, BUILD_NUMBER)
	ChangeFilesContent(BUILD_NUMBER_INTERNAL+"/W", BUILD_NUMBER_INTERNAL, BUILD_NUMBER)

	print "###############################################################################"
	print "What has to be done yet:\n\n"
	print "1.  Add line 'ArchiveBuildVBS_" + BUILD_NUMBER_INTERNAL + " W:\\c\\BuildServer\\BIA\\Build\\!Archive_ServerBuildVBS_" + BUILD_NUMBER_INTERNAL + ".bat' into file W:\\c\\BuildServer\\BIA\\RemEx\\appList.txt."
	print "2. Restart Remex."
	print "3.  Checkout https://dev.bistudio.com/svn/pgm/labels/VBS2/VBS2_" + BUILD_NUMBER_INTERNAL + " into W:\\c\\Archive\\VBS2_" + BUILD_NUMBER_INTERNAL + "."
	print "4.  Change macro ProjectRoot in engdd9.cpp to W:\\c\\Archive\\VBS2_" + BUILD_NUMBER_INTERNAL + "\\Poseidon\\lib."
	print "5.  Change linker output of project configurations VBS2_Release and VBS2_Diag to O:\\" + BUILD_NUMBER_INTERNAL + "\\VBS2.exe and O:\\" + BUILD_NUMBER_INTERNAL + "\\VBS2_Diag.exe."
	print "6.  Update appInfoRev.h to current build. (e.g. 73213)."
	print "7.  Update buildDate.h to current date."
	print "8.  Change version defines in appInfo.h of main branch to next version."
	print "9.  Commit the changed files into SVN."
	print "10. Copy created files and directories of new pipeline to their respective paths."
	print "11. Fire the new build using Q:\\!BuildVBS2_" + BUILD_NUMBER_INTERNAL + ".bat." 
	print "###############################################################################"

	print "Pipeline for build " + BUILD_NUMBER + " created.\n"
	raw_input('Press enter to end the script...')

##########################################################################
	
if __name__ == "__main__":
    main()
	
