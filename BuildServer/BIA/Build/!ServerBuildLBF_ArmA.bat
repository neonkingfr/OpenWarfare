@echo off
REM %1 is for name of computer

python W:\c\BuildServer\BIA\Build\netsend.py LBF_ARMA "[ %1 ] Building of LBF ArmA started"

echo Getting tools...
call W:\c\BuildServer\BIA\Build\GetTools.bat

echo Getting sources...
call W:\c\BuildServer\BIA\Build\GetSourcesLBF.bat

echo Building LandBuilder2 ArmA...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\LandBuilder\LandBuilder2\LandBuilder2.sln /build /prj="*" /cfg="ReleaseArmA|Win32"
if errorlevel 1 goto buildfail

echo Building Roads ArmA...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\LandBuilder\Plugins\Roads\Roads.sln /build /prj=Roads /cfg="Release|Win32"
if errorlevel 1 goto buildfail

echo Building Buildings ArmA...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\LandBuilder\Plugins\Buildings\Buildings.sln /build /prj=Buildings /cfg="ReleaseArmA|Win32"
if errorlevel 1 goto buildfail

echo Building Rivers ArmA...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\LandBuilder\Plugins\Rivers\Rivers.sln /build /prj=Rivers /cfg="ReleaseArmA|Win32"
if errorlevel 1 goto buildfail

echo Building VisualLandBuilder ArmA...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\VisualLandBuilder\VisualLandBuilder.sln /build /prj=VisualLandBuilder /cfg="Release|Win32"
if errorlevel 1 goto buildfail

echo Distributing LandBuilder2 ArmA dlls...
xcopy W:\c\Projects\LandBuilder\LandBuilder2\BinArmA\BuildingPlacers.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\BuildingPlacers.dll -m "New version"
xcopy W:\c\Projects\LandBuilder\LandBuilder2\BinArmA\Erasers.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\Erasers.dll -m "New version"
xcopy W:\c\Projects\LandBuilder\LandBuilder2\BinArmA\ExamplePlugin.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\ExamplePlugin.dll -m "New version"
xcopy W:\c\Projects\LandBuilder\LandBuilder2\BinArmA\GenericPlacers.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\GenericPlacers.dll -m "New version"
xcopy W:\c\Projects\LandBuilder\LandBuilder2\BinArmA\RandomPlacers.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\RandomPlacers.dll -m "New version"
xcopy W:\c\Projects\LandBuilder\LandBuilder2\BinArmA\Terrain.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\Terrain.dll -m "New version"
xcopy W:\c\Projects\LandBuilder\LandBuilder2\BinArmA\Transformations.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\Transformations.dll -m "New version"
xcopy W:\c\Projects\LandBuilder\plugins\Buildings\binArmA\Buildings.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\Buildings.dll -m "New version"
xcopy W:\c\Projects\LandBuilder\plugins\Roads\binArmA\Roads.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\Roads.dll -m "New version"
xcopy W:\c\Projects\LandBuilder\plugins\Rivers\binArmA\Rivers.dll P:\tools\landbuilder_arma /R /Y
svn commit P:\tools\landbuilder_arma\Rivers.dll -m "New version"

echo Distributing LandBuilder ArmA Family...
cmd /c w:\tools\hasp\envelope.com -p w:\tools\hasp\LandBuilder_ArmA.prjx

python W:\c\BuildServer\BIA\Build\netsend.py LBF_ARMA "[ %1 ] Building and sending of LBF ArmA succeeded"

goto end

:buildfail
python W:\c\BuildServer\BIA\Build\netsend.py LBF_ARMA "[ %1 ] Building of LBF ArmA failed"
goto end

:end