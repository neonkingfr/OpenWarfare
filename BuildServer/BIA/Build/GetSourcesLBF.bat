@echo off
set REVISION=%1
set TARGET=LBF
set ROOTDIR=W:\c
set REVNODIR=%ROOTDIR%\Projects
call W:\c\BuildServer\BIA\Build\GetRevNo.bat %REVNODIR% %REVISION%
echo Updating %TARGET% sources (Projects, Es, El, img, extern) to revision %REVISION%...
svn update --non-interactive -r %REVISION% %ROOTDIR%\Projects
svn update --non-interactive -r %REVISION% %ROOTDIR%\Es
svn update --non-interactive -r %REVISION% %ROOTDIR%\El
svn update --non-interactive -r %REVISION% %ROOTDIR%\img
svn update --non-interactive -r %REVISION% %ROOTDIR%\extern