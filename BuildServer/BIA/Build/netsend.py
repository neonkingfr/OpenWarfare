#!/usr/bin/env python
#
# some skype messages from the build server
#
# the used skype user is vbs3.com
# with password kjef0l5xmnciba
# the same is for gmail contact
#

import sys
import Skype4Py

if 3 != len(sys.argv):
  print("wrong number of parameters")
  exit()

mtype = sys.argv[1]
mvalue = sys.argv[2]

# we don't want to see that mtype (normally All or Std) anymore (i.e. the first parameter to this script is completely ignored now)
#message = str(mtype) + ": " + mvalue
message = mvalue

#just a testing group
#chatID = "wP9vKQEpw57nbFSbBr3tPfV2CQ31YLN0YlHKN23qQRWaY1bAeI4t33KvvXnHo2C75b-HEG6VxnI"
#build messages group
chatID = "BbHf9YEpQ8V89yQi-dAyQr3E1nQWo0JHUQhHmXYMpOYBNOIneNiG5DpSXQehgF4Dih3hdw"

skype = Skype4Py.Skype()
skype.Attach()

buildChat = skype.FindChatUsingBlob(chatID)
buildChat.SendMessage(message)




