@echo off
REM %1 is for name of computer

echo Getting tools...
call w:\bis\Build\GetTools.bat

echo Update configs on O...
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.cpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.csv o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.h* o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\*.hpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\dikcodes.h o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\languages.hpp o:\fp\Bin /R /Y
xcopy W:\c\Archive\VBS2\Poseidon\lib\UI\resincl.hpp o:\fp\Bin /R /Y

echo Building VBS ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2|Win32"
if errorlevel 1 goto buildfail
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_Release|Win32"
if errorlevel 1 goto buildfail

echo Creating shaders ...
call o:\fp\VBS2.exe -nosplash -window -generateShaders

echo Distributing VBS ...
cmd /c w:\tools\hasp\envelope.com -p w:\tools\hasp\dev.cfgx

echo Packing bin.pbo ...
p:\tools\oxygen\O2Script.exe -d -a "p:\tools\PBOPacker\PBOPacker.bio2s" O:\fp\Bin 8 -export=Q:\vbs2_latest_builds\dev\engine\dta 

net send %1 Building of VBS succeeded

echo Building Binarization (ArmA2) ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /build /prj=binarize /cfg="Release|Win32"
if errorlevel 1 goto buildbinarizefail

goto end

:buildfail
net send %1 Building of VBS failed
goto end

:buildbinarizefail
net send %1 Building of VBS binarization failed
goto end

:end