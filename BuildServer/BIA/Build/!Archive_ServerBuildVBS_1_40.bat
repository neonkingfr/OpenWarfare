@echo off
REM %1 is for name of computer

time /T
title "[ %1 ] Start building VBS Standard Versions"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Start building VBS Release"
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS Release started"

echo Getting tools...
title "Getting tools..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] Getting tools..."
call W:\c\BuildServer\BIA\Build\GetTools.bat

echo Getting sources...
title "Getting sources..."
svn update --non-interactive W:\c\Archive\VBS2_1_40

echo Getting and incrementing patch no...
title "Getting and incrementing patch no..."
set BUILDNUMBERFILE=w:\c\Archive\VBS2_1_40\buildNumber.bat
if exist %BUILDNUMBERFILE% call %BUILDNUMBERFILE%
set /a BUILDNUMBER=BUILDNUMBER+1
echo set /a BUILDNUMBER=%BUILDNUMBER% >%BUILDNUMBERFILE%

echo Reporting build number...
title "Reporting build number..."
rem set MSG=Build number is set to X.X.0.%BUILDNUMBER%
python W:\c\BuildServer\BIA\Build\netsend.py All "Build number is set to 1.40.0.%BUILDNUMBER%"

echo Updating the project patch no...
title "Updating the project patch no..."
set REVNOHDR=w:\c\Archive\VBS2_1_40\Poseidon\lib\appInfoRev.h
echo #define APP_BREV %BUILDNUMBER% >%REVNOHDR%
echo #define APP_BREVHI 0 >>%REVNOHDR%
echo #define APP_BREVLO %BUILDNUMBER% >>%REVNOHDR%

echo Update configs on O...
xcopy w:\c\Archive\VBS2_1_40\Poseidon\cfg\Bin\*.cpp o:\1_40\Bin /R /Y
xcopy w:\c\Archive\VBS2_1_40\Poseidon\cfg\Bin\*.csv o:\1_40\Bin /R /Y
xcopy w:\c\Archive\VBS2_1_40\Poseidon\cfg\Bin\*.h* o:\1_40\Bin /R /Y
xcopy w:\c\Archive\VBS2_1_40\Poseidon\cfg\*.hpp o:\1_40\Bin /R /Y
xcopy w:\c\Archive\VBS2_1_40\Poseidon\lib\dikcodes.h o:\1_40\Bin /R /Y
xcopy w:\c\Archive\VBS2_1_40\Poseidon\lib\languages.hpp o:\1_40\Bin /R /Y
xcopy W:\c\Archive\VBS2_1_40\Poseidon\lib\UI\resincl.hpp o:\1_40\Bin /R /Y

echo Building VBS Release ...
title "[ %1 ] VBS All: (0.5/9) Building VBS Release ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (0.5/9) Building VBS Release ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_1_40\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_Release|Win32"
if errorlevel 1 goto buildfail
W:\Tools\HASP\envelope -p w:\Tools\HASP\1_40\Release_VBS2_WOCRM.prjx
rem w:\Tools\server\md5.exe /dp=n:\bia_partners\release_archive\1_40\release_Update\release_customer\WOCRM /f=VBS2.exe /S
W:\Tools\HASP\envelope -p w:\Tools\HASP\1_40\Release_VBS2_YYMEA.prjx
rem w:\Tools\server\md5.exe /dp=n:\bia_partners\release_archive\1_40\release_Update\release_customer\YYMEA /f=VBS2.exe /S

echo Creating shaders ...
title "[ %1 ] VBS All: (0.75/9) Creating shaders ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (0.75/9) Creating shaders ..."
call o:\1_40\VBS2.exe -nosplash -window -generateShaders

echo Packing bin.pbo ...
title "[ %1 ] VBS All: (0.8/9) Packing bin.pbo ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (0.8/9) Packing bin.pbo ..."
p:\tools\oxygen\O2Script.exe -d -a "p:\tools\PBOPacker\PBOPacker.bio2s" O:\1_40\Bin 8 -export=O:\1_40\dta -tools=P:\tools_dev\latest
p:\tools\filebank\pbotozbo.exe O:\1_40\dta\bin.pbo n:\bia_partners\release_archive\1_40\release_Update\release\dta
w:\Tools\server\md5.exe /dp=n:\bia_partners\release_archive\1_40\release_Update\release /f=dta\bin.zbo /S

echo Building VBS Diag ...
title "[ %1 ] VBS All: (4/9) Building VBS Diag ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (4/9) Building VBS Diag ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_1_40\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_Diag|Win32"
if errorlevel 1 goto buildfail
W:\Tools\HASP\envelope -p w:\Tools\HASP\1_40\Release_VBS2_Diag_WOCRM.prjx
rem w:\Tools\server\md5.exe /dp=n:\bia_partners\release_archive\1_40\release_Update\release_customer\WOCRM /f=VBS2_Diag.exe /S

echo Building Binarization Release ...
title "[ %1 ] VBS All: (1/%TOTALSTEPS%) Building Binarization Release"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (1/%TOTALSTEPS%) Building Binarization Release"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_1_40\Poseidon\binarize\binarize.sln /build /prj=binarize /cfg="Release|Win32"
if errorlevel 1 goto buildfail
rem xcopy W:\c\Archive\VBS2_1_40\Poseidon\binarize\Release\binarize.exe P:\tools\binarize /R /Y
rem %SVNPATH% commit P:\tools\binarize\*.exe -F w:\c\Archive\VBS2\Poseidon\lib\appInfoRev.h --force-log


python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building release version succeeded"

time /T
title "All Done! Hungry for new tasks."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] All Done! Hungry for new tasks."
goto end

:buildfail
time /T
title "Building of VBS failed"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Building of VBS failed."
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS failed"
goto end

:end
