@echo off
REM %1 is for name of computer

time /T
title "[ %1 ] Start building VBS Standard Versions"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Start building VBS GAA"
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS GAA started"

echo Getting tools...
title "Getting tools..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] Getting tools..."
call W:\c\BuildServer\BIA\Build\GetTools.bat

echo Getting sources...
title "Getting sources..."
svn update --non-interactive W:\c\Archive\VBS2_GAA

echo Update configs on O...
xcopy w:\c\Archive\VBS2_GAA\Poseidon\cfg\Bin\*.cpp o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2_GAA\Poseidon\cfg\Bin\*.csv o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2_GAA\Poseidon\cfg\Bin\*.h* o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2_GAA\Poseidon\cfg\*.hpp o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2_GAA\Poseidon\lib\dikcodes.h o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2_GAA\Poseidon\lib\languages.hpp o:\fp_stable\Bin /R /Y
xcopy W:\c\Archive\VBS2_GAA\Poseidon\lib\UI\resincl.hpp o:\fp_stable\Bin /R /Y

echo Building VBS GAA Developer ...
title "[ %1 ] VBS All: (0.5/9) Building VBS GAA ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (0.5/9) Building VBS Developer ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_GAA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2|Win32"
if errorlevel 1 goto buildfail

echo Creating shaders ...
title "[ %1 ] VBS All: (0.75/9) Creating shaders ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (0.75/9) Creating shaders ..."
call o:\fp_stable\VBS2_Dev.exe -nosplash -window -generateShaders

echo Packing bin.pbo ...
title "[ %1 ] VBS All: (0.8/9) Packing bin.pbo ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (0.8/9) Packing bin.pbo ..."
p:\tools\oxygen\O2Script.exe -d -a "p:\tools\PBOPacker\PBOPacker.bio2s" O:\fp_stable\Bin 8 -export=O:\fp_stable\dta 
"C:\Program Files\Aladdin\HASP HL\VendorTools\VendorCenter\dfcrypt.exe" -e -c:w:\tools\hasp\dev.hvc -k:9lzoMtOH -o O:\fp_stable\dta\bin.pbo N:\bia_developers\_Build_stable\dta\bin.ebo

echo Building VBS NL_RIOT ...
title "[ %1 ] VBS Std: (3/9) Building VBS NL_RIOT ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (3/9) Building VBS NL_RIOT ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_GAA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_Release|Win32"
if errorlevel 1 goto buildfail
W:\Tools\HASP\envelope -p W:\Tools\HASP\VBS2_NL_RIOT.prjx

echo Building VBS GAA PR ...
title "[ %1 ] VBS Std: (1/9) Building VBS GAA PR ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (1/9) Building VBS GAA PR ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_GAA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_Rel_USMC|Win32"
if errorlevel 1 goto buildfail
"c:\Program Files\Aladdin\HASP SRM\VendorTools\VendorSuite\envelope.com" -p W:\Tools\HASP\VBS2_SRM_GAA_PR.prjx

echo Building VBS GAA PR RC ...
title "[ %1 ] VBS Std: (2/9) Building VBS GAA PR RC ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (2/9) Building VBS GAA PR ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_GAA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_RC_USMC|Win32"
if errorlevel 1 goto buildfail
"c:\Program Files\Aladdin\HASP SRM\VendorTools\VendorSuite\envelope.com" -p W:\Tools\HASP\VBS2_SRM_GAA_PR_RC.prjx
"c:\Program Files\Aladdin\HASP SRM\VendorTools\VendorSuite\envelope.com" -p W:\Tools\HASP\VBS2_GAA_Fusion.prjx


rem NATO versions VVVVVVVVVVVVVVVVVVVVVVVV


echo Building VBS NATO server ...
title "[ %1 ] VBS All: (2.1/9) Building VBS NATO server ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (2.1/9) Building VBS NATO server ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_GAA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_NATO_Server|Win32"
if errorlevel 1 goto buildfail

echo DistributingNATO server  ...
title "[ %1 ] VBS All: (2.2/9) Encrypting NATO server  ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (2.2/9) Encrypting NATO server  ..."
cmd /c W:\Tools\HASP\envelope -p W:\Tools\HASP\NatoServer_1_32.prjx


echo Encrypt the NATO server bin.pbo ...

p:\tools\filebank\PBOTOXBONATO.exe O:\fp_stable\dta\bin.pbo N:\bia_developers\_NATOLite_1.32\toTest\dta

echo Building Building VBS2 NATO CLIENT
title "[ %1 ] VBS All: (2.3/9) Building VBS2 NATO CLIENT"
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (2.3/9) Building VBS2 NATO CLIENT"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_GAA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_NATO_Client|Win32"
if errorlevel 1 goto buildfail
xcopy o:\fp\Release\GAA\NATO\VBS2_NATO_Client.exe N:\bia_developers\_NATOLite_1.32\toTest\ /y

rem NATO versions ^^^^^^^^^^^^^^^^^^^^^^^^^^

echo Building VBS LITE US ...
title "[ %1 ] VBS Std: (4/9) Building VBS LITE US ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (4/9) Building VBS LITE US ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_GAA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_LITE_US|Win32"
if errorlevel 1 goto buildfail
rem W:\Tools\HASP\envelope -p W:\Tools\HASP\VBS2_NL_RIOT.prjx

rem echo Building VBS GAA Rel UK ...
rem title "[ %1 ] VBS Std: (2/9) Building VBS GAA Rel UK ..."
rem python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (2/9) Building VBS Rel UK ..."
rem call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2_GAA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_Rel_UK|Win32"
rem if errorlevel 1 goto buildfail
rem "c:\Program Files\Aladdin\HASP SRM\VendorTools\VendorSuite\envelope.com" -p W:\Tools\HASP\VBS2_SRM_GAA_Rel_UK.prjx


python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building standard versions succeeded"

time /T
title "All Done! Hungry for new tasks."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] All Done! Hungry for new tasks."
goto end

:buildfail
time /T
title "Building of VBS failed"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Building of VBS failed."
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS failed"
goto end

:end
