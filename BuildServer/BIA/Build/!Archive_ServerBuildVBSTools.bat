@echo off
REM %1 is for name of computer

time /T
title "[ %1 ] Start building VBS Standard Versions"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Start building VBS Standard Versions"
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS started"

echo Getting tools...
title "Getting tools..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Getting tools..."
call W:\c\BuildServer\BIA\Build\GetTools.bat

echo Getting sources...
title "Getting sources..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Getting sources..."
call W:\c\BuildServer\BIA\Build\GetSourcesVBS2.bat
call W:\c\BuildServer\BIA\Build\GetSourcesVBS2Tools.bat

echo Update configs on O...
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.cpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.csv o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.h* o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\*.hpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\dikcodes.h o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\languages.hpp o:\fp\Bin /R /Y

echo Building Binarization ...
title "[ %1 ] VBS Std: (1/9) Building Binarization"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (1/9) Building Binarization"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /build /prj=binarize /cfg="Release|Win32"
if errorlevel 1 goto buildfail
rem xcopy W:\c\Archive\VBS2\Poseidon\binarize\Release\*.exe P:\tools_dev\Latest\binarize /R /Y
rem svn.exe commit -m "[TOOLS] #define BUILD_NO XXXXX" "P:\tools_dev\Latest\binarize\binarize.exe"

title "[ %1 ] VBS Std: (1.5/9) Building FBX convertor"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (1.5/9) Building FBX convertor"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\v\FBX\Converter\Converter.sln /build /prj=Converter /cfg="Release|Win32"
if errorlevel 1 goto buildfail
call "w:\c\BuildServer\BIA\Build\distribute_FBXConverter.bat"

title "[ %1 ] VBS Std: (1.75/9) Building VBS Launcher"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (1.75/9) Building VBS Launcher"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\v\VBSLauncher\VBSLauncher.sln /build /prj=VBSLauncher /cfg="Release|Win32"
if errorlevel 1 goto buildfail
call "w:\c\BuildServer\BIA\Build\distribute_VBSLauncher.bat"

title "[ %1 ] VBS Std: (2/9) Building PBO2XBO"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (2/9) Building PBO2XBO"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\PboToXbo\PboToXbo.sln /build /prj=PboToXbo /cfg="Dev_Rel|Win32"
if errorlevel 1 goto buildfail
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\PboToXbo\PboToXbo.sln /build /prj=PboToXbo /cfg="Lite_Rel_AU|Win32"
if errorlevel 1 goto buildfail
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\PboToXbo\PboToXbo.sln /build /prj=PboToXbo /cfg="Lite_Rel_UK|Win32"
if errorlevel 1 goto buildfail
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\PboToXbo\PboToXbo.sln /build /prj=PboToXbo /cfg="Lite_Rel_US|Win32"
if errorlevel 1 goto buildfail
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\PboToXbo\PboToXbo.sln /build /prj=PboToXbo /cfg="NATO_Rel|Win32"
if errorlevel 1 goto buildfail
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\PboToEbo\PboToEbo.sln /build /prj=PboToEbo /cfg="Release|Win32"
if errorlevel 1 goto buildfail

title "[ %1 ] VBS Std: (3/9) Building Filebank"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (3/9) Building Filebank"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\FileBank\FileBank.sln /build /prj=FileBank /cfg="VBS2Rel|Win32"
if errorlevel 1 goto buildfail

title "[ %1 ] VBS Std: (4/9) Building p3dview"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (4/9) Building p3dview"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\v\Projects\p3dview\p3dview.sln /build /prj=p3dview /cfg="ReleaseVBS|Win32"
if errorlevel 1 goto buildfail
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\v\Projects\p3dview\p3dview.sln /build /prj=p3dview /cfg="Release (CLI)|Win32"
if errorlevel 1 goto buildfail

python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building VBS tools succeeded"

time /T
title "All Done! Hungry for new tasks."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] All Done! Hungry for new tasks."
goto end

:buildfail
time /T
title "Building of VBS tools failed"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Building of VBS tools failed."
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS tools failed"
goto end

:end
