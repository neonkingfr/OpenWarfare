set DESTINATION="p:\_toolsCommit\Sdk\"
set DESTINATION_COMMON="p:\toolsCommit\Sdk\Samples\Binary\"

title Local Distributing SDK...

rmdir W:\sdk /q /s
mkdir W:\sdk

:: copied all include headers to p tools
svn export w:\c\Archive\VBS2\Tools\SDK\Distribution\include W:\SDK\include --force

::copies all generates dlls
mkdir W:\sdk\Lib
mkdir W:\sdk\Lib\BISDKLBModules
xcopy /y /q /f w:\c\Archive\VBS2\Tools\SDK\Distribution\lib\*.dll W:\sdk\Lib\
xcopy /y /q /f w:\c\Archive\VBS2\Tools\SDK\Distribution\lib\*.lib W:\sdk\Lib\
xcopy /y /q /f w:\c\Archive\VBS2\Tools\SDK\Distribution\lib\BISDKLBModules\*.dll W:\sdk\Lib\BISDKLBModules

echo "Copying Distribution"
::here comes all copying where should modelSDK be distributed
xcopy  W:\sdk %DESTINATION%  /f /S /Y
xcopy  W:\sdk\Lib %DESTINATION_COMMON%  /f /S /Y
:: commiting to the project that nneed modelSDK
xcopy  w:\sdk\Lib\BISDKModel.dll p:\_toolsCommit\oxygen /f /S /Y

::rmdir W:\sdk /q /s

title Distributing done
echo Distributing done
goto OK

:buildfail 
echo build failed!;
pause;

:OK
GOTO end;

:not Found
echo "somwhitng was not foudn"

:end