@echo off
REM %1 user
set NETSEND=python W:\c\BuildServer\BIA\Build\netsend.py O2
set BUILD="c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" /rebuild
SET SVNPATH="C:\Program Files\SlikSvn\bin\svn.exe"
SET TOOLS_PATH=w:\c\Archive\VBS2\Tools\

title Preparing O2 build...
%NETSEND% "[ %1 ] Building of O2 started"

:: this will update p:\tools drive so there will be no problems with later commit
echo Getting tools...
call W:\c\BuildServer\BIA\Build\GetTools.bat

:: this will update all the sources tools are compiled from
echo Getting sources...
call W:\c\BuildServer\BIA\Build\GetSourcesO2.bat
REM %REVISION% and %REVNOHDR% are now set properly

:: Building SDK - so far without the packer because of its compilation problems
%BUILD% %TOOLS_PATH%SDK\SDKModel\SDKModelSolution\SDKModelSolution.sln  /all /cfg="Release|Win32"
%BUILD% %TOOLS_PATH%SDK\SDKLandBuilder\build\SDKLandBuilder.sln /all /cfg="Release|Win32"
%BUILD% %TOOLS_PATH%SDK\SDKMultiWorld\build\SDKMultiWorld.sln  /all /cfg="Release|Win32"
%BUILD% %TOOLS_PATH%SDK\SDKWorld\build\SDKWorld.sln /all /cfg="Release|Win32"

echo Distributing SDK...
call w:\c\BuildServer\BIA\Build\DistriButeModelSdkLocal.bat 

set DOXYFILE=w:\c\Archive\VBS2\Tools\SDK\Doxygen\
set EXAMPLESPATH="p:\_toolsCommit\SDK\Samples"
set BINARYPATH="p:\_toolsCommit\SDK\Samples\Binary"

title Rebuilding all SDK examples...
FOR /R %EXAMPLESPATH% %%f in (*.sln) DO (
	pushd "%%f"
	%BUILD% /all %%f /cfg="Release|Win32"
	if errorlevel 1 goto buildfail 
	popd
 )

title Generating documentation

:: Generate new documentation
pushd "%DOXYFILE%"
doxygen "%DOXYFILE%Doxyfile"
popd

:: Building dev versions

:: O2s are build on the new build server now
:: title Building O2_dev...
:: echo Building O2_dev...
:: %BUILD% %TOOLS_PATH%Projects\Objektiv2\Objektiv2.sln /prj=Objektiv2 /cfg="RELEASE|Win32"
:: if errorlevel 1 goto buildfail

:: Building release versions
:: title Building O2...
:: echo Building O2...
:: %BUILD% %TOOLS_PATH%Projects\Objektiv2\Objektiv2.sln /prj=Objektiv2 /cfg="Release VBS|Win32"
:: if errorlevel 1 goto buildfail

title Building p3d viewer...
echo Building p3d viewer...
%BUILD% %TOOLS_PATH%Projects\p3dView\p3dview.sln /prj=p3dview /cfg="ReleaseVBS|Win32"
if errorlevel 1 goto buildfail

title Building p3d comparer...
echo Building p3d compare...
%BUILD% p:\_toolsCommit\Sdk\Samples\Model\p3dCompare\build\p3dCompare.sln /prj=p3dcompare /cfg="Release|Win32"
if errorlevel 1 goto buildfail

::p3dRender is p3dView but in command line mode
title Building p3d renderer...
echo Building p3d renderer...
%BUILD% %TOOLS_PATH%Projects\p3dview\p3dview.sln /prj=p3dview /cfg="Release (CLI)|Win32"
if errorlevel 1 goto buildfail

title Building SoundConverter...
echo Building SoundConverter...
%BUILD% %TOOLS_PATH%Projects\SoundConverter\SoundConverter.sln /prj=SoundConverter /cfg="Release|Win32"
if errorlevel 1 goto buildfail

title Building UIDebugger...
echo Building UIDebugger...
%BUILD% %TOOLS_PATH%Projects\Objektiv2\Objektiv2.sln /prj=UIDebugger /cfg="Release VBS|Win32"
if errorlevel 1 goto buildfail

title Building texView...
echo Building texView...
%BUILD% %TOOLS_PATH%img\img.sln /prj=texView /cfg="Release|Win32"
if errorlevel 1 goto buildfail

title Building Pal2PacE...
echo Building Pal2PacE...
%BUILD% %TOOLS_PATH%img\img.sln /prj=Pal2PacE /cfg="Release|Win32"
if errorlevel 1 goto buildfail

title Building Pal2Pac.dll...
echo Building Pal2Pac.dll...
%BUILD% %TOOLS_PATH%img\img.sln /prj=Pal2Pac /cfg="Release|Win32"
if errorlevel 1 goto buildfail

title Building p3d2bmp...
echo Building p3d2bmp...
%BUILD% %TOOLS_PATH%img\img.sln /prj=P3D2BMP /cfg="Release|Win32"
if errorlevel 1 goto buildfail

::title Building OxygeneSDK.. -> instead modelSDK was built.
::echo Building OxygeneSDK...
::%BUILD% w:\c\Projects\ObjektivLib\ObjektivSDK\oxygeneSDK.sln /rebuild /prj=ObjektivLib /cfg="Release|Win32"
::if errorlevel 1 goto buildfail

title All builds OK, post-processing...
echo All builds OK, post-processing...

title Encryption...
%SVNPATH% update -non-interactive "P:\_toolsCommit"
:: Files get encrypted to _toolsCommit as the packing server is running O2Script and might prevent overwriting
:: I see no reason to single commit those.. so I changed it to one for all commit.
W:\tools\hasp\envelope.com -p W:\tools\hasp\Tools_noObjectiv2.prjx
xcopy W:\c\Archive\VBS2\Tools\Projects\Objektiv2\bin\FBX.dll P:\_toolsCommit\oxygen\ /R /Y
xcopy W:\c\Archive\VBS2\Tools\Projects\Objektiv2\Bin\UIDebugger.dll P:\_toolsCommit\oxygen\tools\ /R /Y
xcopy w:\c\Archive\VBS2\Tools\Projects\Objektiv2\bin\O2Script.exe P:\_toolsCommit\oxygen\ /R /Y
xcopy W:\c\Archive\VBS2\Tools\Projects\ObjektivLib\SpecialLod.hpp P:\_toolsCommit\oxygen\std\ /R /Y
xcopy W:\c\Archive\VBS2\Tools\img\texView\Release\texView.exe P:\_toolsCommit\tex\ /R /Y
xcopy W:\c\Archive\VBS2\Tools\img\Pal2PacE\Release\Pal2PacE.exe P:\_toolsCommit\tex\ /R /Y
xcopy W:\c\Archive\VBS2\Tools\img\p3d2bmp\Release\p3d2bmp.exe P:\_toolsCommit\oxygen\tools\ /R /Y
::p3dComparer should be build in the correct place
title Commiting...
:: Do not commit until confirmed
%SVNPATH% commit P:\_toolsCommit -m "[TOOLS] Updated Tools"

title Build of O2 done
echo Build of O2 done
%NETSEND% "[ %1 ] Building of O2 (%REVISION%) succeeded"
goto end

:buildfail
title Build of O2 failed
echo Build of O2 failed
%NETSEND% "[ %1 ] Building of O2 failed"
goto end

:end
