svn.exe update p:\tools\VBSLauncher
xcopy w:\v\VBSLauncher\Release\VBSLauncher.exe p:\tools\VBSLauncher\ /R /Y
xcopy w:\v\VBSLauncher\Release\Launcher\rc p:\tools\VBSLauncher\Launcher\rc /R /Y
xcopy w:\v\VBSLauncher\Release\Launcher\rc\pictures p:\tools\VBSLauncher\Launcher\rc\pictures /R /Y
xcopy w:\v\VBSLauncher\Release\Launcher\presets p:\tools\VBSLauncher\Launcher\presets /R /Y
xcopy w:\v\VBSLauncher\Release\Launcher\Bin p:\tools\VBSLauncher\Launcher\Bin /R /Y
xcopy w:\v\VBSLauncher\Release\Launcher\ReadMe.txt p:\tools\VBSLauncher\Launcher\ /R /Y
xcopy w:\v\VBSLauncher\Release\Launcher\RemoteServer p:\tools\VBSLauncher\Launcher\RemoteServer /R /Y
svn.exe commit p:\tools\VBSLauncher\*.* -m "Launcher updated"
