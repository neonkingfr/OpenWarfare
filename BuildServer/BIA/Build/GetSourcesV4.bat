@echo off
set REVISION=%1
set TARGET=Visitor4
set ROOTDIR=W:\t\Visitor4\trunk
set REVNODIR=%ROOTDIR%\build
call W:\c\BuildServer\BIA\Build\GetRevNo.bat %REVNODIR% %REVISION%
echo Updating %TARGET% sources to revision %REVISION%...
svn update --non-interactive -r %REVISION% %ROOTDIR%
