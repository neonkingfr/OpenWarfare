@echo off
set REVISION=%1
set TARGET=O2
set ROOTDIR=W:\v
set REVNODIR=%ROOTDIR%\Projects
call W:\c\BuildServer\BIA\Build\GetRevNo.bat %REVNODIR% %REVISION%
echo Updating %TARGET% sources to revision %REVISION%...
svn update --non-interactive -r %REVISION% %ROOTDIR%\Projects
svn update --non-interactive -r %REVISION% %ROOTDIR%\img
svn update --non-interactive -r %REVISION% %ROOTDIR%\El
svn update --non-interactive -r %REVISION% %ROOTDIR%\Es
svn update --non-interactive -r %REVISION% %ROOTDIR%\extern
svn update --non-interactive -r %REVISION% %ROOTDIR%\SDK
REM FIXME: why we need these folders from different repository (W:\v before and W\c now) ? Note that we can't use REVISION here, as that repo has a different one!
REM svn update --non-interactive W:\c\Projects
REM svn update --non-interactive W:\c\img
svn update --non-interactive W:\c\Visitor