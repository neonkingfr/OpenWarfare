@echo off
REM %1 is for name of computer
SET TOTALSTEPS=11

time /T
title "[ %1 ] Start building VBS Standard Versions"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Start building VBS Standard Versions"
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS started"

echo Getting tools...
title "Getting tools..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Getting tools..."
call W:\c\BuildServer\BIA\Build\GetTools.bat

echo Getting sources...
title "Getting sources..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Getting sources..."
call W:\c\BuildServer\BIA\Build\GetSourcesVBS2.bat

echo Copying core configs...
title "Copying core configs..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Copying core configs..."
call W:\c\BuildServer\BIA\Build\CommitCoreConfigs.bat

echo Update configs on O:\alpha...
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.cpp o:\alpha\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.csv o:\alpha\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.h* o:\alpha\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\*.hpp o:\alpha\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\dikcodes.h o:\alpha\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\languages.hpp o:\alpha\Bin /R /Y
xcopy W:\c\Archive\VBS2\Poseidon\lib\UI\resincl.hpp o:\alpha\Bin /R /Y

echo Update configs on O:\beta...
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.cpp o:\beta\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.csv o:\beta\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.h* o:\beta\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\*.hpp o:\beta\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\dikcodes.h o:\beta\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\languages.hpp o:\beta\Bin /R /Y
xcopy W:\c\Archive\VBS2\Poseidon\lib\UI\resincl.hpp o:\beta\Bin /R /Y

echo Update configs on O:\fp_stable...
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.cpp o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.csv o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.h* o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\*.hpp o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\dikcodes.h o:\fp_stable\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\languages.hpp o:\fp_stable\Bin /R /Y
xcopy W:\c\Archive\VBS2\Poseidon\lib\UI\resincl.hpp o:\fp_stable\Bin /R /Y

rem Disable incremental linking on the build server
del O:\fp\VBS2*.exe O:\fp\VBS2*.ilk

echo Building VBS Alpha ...
title "[ %1 ] VBS Std: (1/%TOTALSTEPS%) Building VBS Alpha ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (1/%TOTALSTEPS%) Building VBS Alpha ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /rebuild /prj=lib2005 /cfg="VBS2_Alpha|Win32"
if errorlevel 1 goto buildfail

echo Creating alpha shaders /Distributing alpha bin.pbo...
title "[ %1 ] VBS Std: (2/%TOTALSTEPS%) Creating shaders ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (2/%TOTALSTEPS%) Creating alpha shaders ..."
call o:\alpha\VBS2_Alpha.exe -nosplash -window -generateShaders
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (2/%TOTALSTEPS%) Packing alpha bin.pbo ..."
p:\tools\oxygen\O2Script.exe -d -a "p:\tools\PBOPacker\PBOPacker.bio2s" O:\alpha\Bin 8 -export=o:\alpha\dta 
p:\tools\filebank\pboToZBO.exe O:\alpha\dta\Bin.pbo n:\bia_developers\_builds\alpha\dta
w:\Tools\server\md5.exe /dp=n:\bia_developers\_builds\alpha /f=dta\bin.zbo /S

echo Distributing VBS Alpha ...
title "[ %1 ] VBS Std: (3/%TOTALSTEPS%) Distributing VBS2 Alpha ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (3/%TOTALSTEPS%) Distributing VBS Alpha ..."
W:\Tools\HASP\envelope -p W:\Tools\HASP\VBS2_Alpha.prjx
w:\Tools\server\md5.exe /dp=n:\bia_developers\_builds\alpha /f=VBS2_Alpha.exe /S


echo Building VBS Beta ...
title "[ %1 ] VBS All: (4/%TOTALSTEPS%) Building VBS2 Beta ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (4/%TOTALSTEPS%) Building VBS Beta ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /rebuild /prj=lib2005 /cfg="VBS2_Beta|Win32"
if errorlevel 1 goto buildfail

echo Creating beta shaders /Distributing beta bin.pbo...
title "[ %1 ] VBS Std: (5/%TOTALSTEPS%) Creating shaders ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (5/%TOTALSTEPS%) Creating beta shaders ..."
call o:\beta\VBS2_Beta.exe -nosplash -window -generateShaders
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (5/%TOTALSTEPS%) Packing beta bin.pbo ..."
p:\tools\oxygen\O2Script.exe -d -a "p:\tools\PBOPacker\PBOPacker.bio2s" O:\beta\Bin 8 -export=o:\beta\dta 
p:\tools\filebank\pboToZBO.exe O:\beta\dta\Bin.pbo n:\bia_developers\_builds\beta\dta
w:\Tools\server\md5.exe /dp=n:\bia_developers\_builds\beta /f=dta\bin.zbo /S

echo Distributing VBS Beta ...
title "[ %1 ] VBS All: (6/%TOTALSTEPS%) Distributing VBS Beta ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (6/%TOTALSTEPS%) Distributing VBS Beta ..."
cmd /c W:\Tools\HASP\envelope -p W:\Tools\HASP\VBS2_Beta.prjx
w:\Tools\server\md5.exe /dp=n:\bia_developers\_builds\beta /f=VBS2_Beta.exe /S

call W:\c\BuildServer\BIA\Build\CopyCompiledShaders.bat

echo Building VBS Release ...
title "[ %1 ] VBS All: (7/%TOTALSTEPS%) Building VBS Release ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (7/%TOTALSTEPS%) Building VBS Release ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /rebuild /prj=lib2005 /cfg="VBS2_ReleaseLatest|Win32"
if errorlevel 1 goto buildfail

echo Creating release shaders /Distributing release bin.pbo...
title "[ %1 ] VBS Std: (8/%TOTALSTEPS%) Creating shaders ..."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (8/%TOTALSTEPS%) Creating release shaders ..."
call o:\fp_stable\VBS2.exe -nosplash -window -generateShaders
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (8/%TOTALSTEPS%) Packing release bin.pbo ..."
p:\tools\oxygen\O2Script.exe -d -a "p:\tools\PBOPacker\PBOPacker.bio2s" O:\fp_stable\Bin 8 -export=o:\fp_stable\dta
p:\tools\filebank\pboToZBO.exe O:\fp_stable\dta\Bin.pbo n:\bia_developers\_builds\ReleaseExe\dta\

echo Building VBS2 Release FP64 ...
title "[ %1 ] VBS All: (9/%TOTALSTEPS%) Building VBS2 Release FP64 ..."
python W:\c\BuildServer\BIA\Build\nettitle.py All "[ %1 ] (9/%TOTALSTEPS%) Building VBS2 Release FP64 ..."
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /rebuild /prj=lib2005 /cfg="VBS2_Release_FP64|Win32"
if errorlevel 1 goto buildfail

W:\Tools\HASP\envelope -p W:\Tools\HASP\VBS2_Release_WOCRM.prjx
W:\Tools\HASP\envelope -p W:\Tools\HASP\VBS2_Release_YYMEA.prjx

rem echo BackUp Dev on Q ...
rem call W:\c\BuildServer\BIA\Build\Backup.bat Q:\vbs2_latest_builds\dev\engine_backup Q:\vbs2_latest_builds\dev\base\VBS2_Alpha.exe

python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building and sending of VBS Alpha succeeded"

echo Building Binarization ...
title "[ %1 ] VBS Std: (10/%TOTALSTEPS%) Building Binarization"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (10/%TOTALSTEPS%) Building Binarization"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /rebuild /prj=binarize /cfg="Release|Win32"
if errorlevel 1 goto buildbinarizefail

echo Building Binarization64 ...
title "[ %1 ] VBS Std: (11/%TOTALSTEPS%) Building Binarization64"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] (11/%TOTALSTEPS%) Building Binarization64"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\binarize\binarize.sln /rebuild /prj=binarize /cfg="Release|x64"
if errorlevel 1 goto buildbinarizefail
rem xcopy W:\c\Archive\VBS2\Poseidon\binarize\Release\*.exe P:\tools_dev\Latest\binarize /R /Y
rem svn.exe commit -m "[DEV] #define BUILD_NO XXXXX" "P:\tools_dev\Latest\binarize\binarize.exe"

python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building standard versions succeeded"

time /T
title "All Done! Hungry for new tasks."
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] All Done! Hungry for new tasks."
goto end

:buildfail
time /T
title "Building of VBS failed"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Building of VBS failed."
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS failed"
goto end

:buildbinarizefail
time /T
title "Building of binarization failed"
python W:\c\BuildServer\BIA\Build\nettitle.py Std "[ %1 ] Building of VBS binarization failed."
python W:\c\BuildServer\BIA\Build\netsend.py Std "[ %1 ] Building of VBS binarization failed"
goto end

:end
