#!/usr/bin/env python
#
# sets the mood-text/topic according to the given parameters
#

import sys
import Skype4Py

if 3 != len(sys.argv):
  print("wrong number of parameters")
  exit()

mtype = sys.argv[1]
mvalue = sys.argv[2]
moodtext = str(mtype) + ": " + mvalue
mtopic = "vbs2 build: " + str(mtype) + ": " + mvalue

skype = Skype4Py.Skype()
profile = Skype4Py.profile.IProfile
profile(skype)._SetMoodText(moodtext)

#just a testing group
#chatID = "wP9vKQEpw57nbFSbBr3tPfV2CQ31YLN0YlHKN23qQRWaY1bAeI4t33KvvXnHo2C75b-HEG6VxnI"
#build messages group
#chatID = "BbHf9YEpQ8V89yQi-dAyQr3E1nQWo0JHUQhHmXYMpOYBNOIneNiG5DpSXQehgF4Dih3hdw"
#
#buildChat = skype.FindChatUsingBlob(chatID)
#buildChat._SetTopic(mtopic)


