@echo off
REM %1 is for name of computer

net send %1 Building of VBS started

del O:\fp\Release\Lite\*.* /F /Q

echo Getting tools...
call w:\bis\Build\GetTools.bat

echo Getting sources...
call w:\bis\Build\Archive_GetSourcesSVN.bat

echo Update configs on O...
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.cpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.csv o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\Bin\*.h* o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\cfg\*.hpp o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\dikcodes.h o:\fp\Bin /R /Y
xcopy w:\c\Archive\VBS2\Poseidon\lib\languages.hpp o:\fp\Bin /R /Y
xcopy W:\c\Archive\VBS2\Poseidon\lib\UI\resincl.hpp o:\fp\Bin /R /Y

echo Building VBS ...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="VBS2_LITE|Win32"
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\VBS2\Poseidon\Poseidon2005.sln /rebuild /prj=lib2005 /cfg="VBS2_LITE|Win32"
if errorlevel 1 goto buildfail

echo Packing bin.pbo ...
p:\tools\oxygen\O2Script.exe -d -a "p:\tools\PBOPacker\PBOPacker.bio2s" O:\fp\Bin 8 -export=Q:\vbs2_latest_builds\dev\engine\dta 

echo Copy exe to the Q ...
xcopy O:\fp\Release\lite\VBS2_LITE.exe Q:\vbs2_latest_builds\dev\engine_Lite /R /Y 

net send %1 Building and sending of Dev VBS succeeded

goto end

:buildfail
net send %1 Building of VBS failed
goto end

:buildbinarizefail
net send %1 Building of VBS binarization failed
goto end

:end