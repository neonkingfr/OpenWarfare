@echo off
set PROJECTDIR=%1
set REVISION=%2
set REVNOHDR=%PROJECTDIR%\appInfoRev.h
if exist %REVNOHDR% goto :valid_header
REM header doesn't exist (probably due to wrong spec), , do nothing, just warn and exit
set MSG=Can't locate header %REVNOHDR%, wrong project dir specified?
goto :finalization

:valid_header
REM to prevent eventual collision if revNo.h is locally modified (it shouldn't)
svn revert -q %REVNOHDR%
REM get the revision number of the whole repository (could be higher than the last project-related commit, but that's no problem)
for /F "tokens=2 skip=4" %%G in ('svn info -r HEAD %PROJECTDIR%') do if not defined REVISION set REVISION=%%G
REM in case of any problems
if [%REVISION%] == [] set REVISION=0000
set /a REVISIONHI=REVISION/1000
set /a REVISIONLO=REVISION%%1000
REM update the revno in the header
echo #define APP_BREV %REVISION% >%REVNOHDR%
echo #define APP_BREVHI %REVISIONHI% >>%REVNOHDR%
echo #define APP_BREVLO %REVISIONLO% >>%REVNOHDR%
set MSG=Revision number in %REVNOHDR% set to %REVISION%

:finalization
echo %MSG%
python W:\c\BuildServer\BIA\Build\netsend.py All "%MSG%"
