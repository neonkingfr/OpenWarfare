@echo off
set REVISION=%1
set TARGET=VBS2
set ROOTDIR=W:\c\Archive\VBS2
set REVNODIR=%ROOTDIR%\Poseidon\lib
call W:\c\BuildServer\BIA\Build\GetRevNo.bat %REVNODIR% %REVISION%
echo Updating %TARGET% sources to revision %REVISION%...
svn update --non-interactive -r %REVISION% %ROOTDIR%
svn update --non-interactive w:\Plugins