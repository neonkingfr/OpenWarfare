@echo off
REM %1 user; %2 version (YY.MM)
set VERSION=%2
set NETSEND=python W:\c\BuildServer\BIA\Build\netsend.py V4
set BUILD="c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\t\Visitor4\trunk\build\visitor4.sln /rebuild /prj="*"
set BUILDDIR=W:\t\Visitor4\trunk\bin
set SVNDIR=P:\tools\Visitor4
set OUTROOT=P:\tools_archive\Visitor4
set V4NAME=Visitor4 %VERSION%

if [%VERSION%] == [] goto :buildfail

title Preparing %V4NAME% build...
%NETSEND% "[ %1 ] Building of %V4NAME% started"

echo Getting tools...
call W:\c\BuildServer\BIA\Build\GetTools.bat

echo Getting sources...
call W:\c\BuildServer\BIA\Build\GetSourcesV4.bat
REM %REVISION% and %REVNOHDR% are now set properly

REM provide build name suffix (10.07.RC etc.)
echo #define APP_BSUFF "%VERSION%" >>%REVNOHDR%

echo Preparing release area...
svn update %SVNDIR%
svn revert %SVNDIR%\Visitor4.exe


title Building %V4NAME% internal...
echo Building %V4NAME% internal...
%BUILD% /cfg="internal|Win32"
if errorlevel 1 goto buildfail

REM title Building %V4NAME% Arma...
REM echo Building %V4NAME% Arma...
REM %BUILD% /cfg="Arma|Win32"
REM if errorlevel 1 goto buildfail

title Building %V4NAME% customer...
echo Building %V4NAME% customer...
%BUILD% /cfg="customer|Win32"
if errorlevel 1 goto buildfail


set OUTDIR=%OUTROOT%\%VERSION%.%REVISION%
set OUTDIR_RAW=%OUTDIR%\raw
title Post-processing %V4NAME% exes (%OUTDIR%)
echo All builds OK, storing results to %OUTDIR%

mkdir %OUTDIR_RAW%
copy /y %BUILDDIR%\Visitor4.exe %OUTDIR_RAW%
copy /y %BUILDDIR%\Visitor4.pdb %OUTDIR_RAW%
copy /y %BUILDDIR%\Visitor4_i.exe %OUTDIR_RAW%
copy /y %BUILDDIR%\Visitor4_i.pdb %OUTDIR_RAW%
REM copy /y %BUILDDIR%\Visitor4_Arma2.exe %OUTDIR_RAW%
REM copy /y %BUILDDIR%\Visitor4_Arma2.pdb %OUTDIR_RAW%

echo Encryption...
REM encrypted customer will be at P:\tools\visitor4\Visitor4.exe
W:\tools\hasp\envelope.com -p W:\tools\hasp\Visitor4.prjx
copy /y P:\tools\visitor4\Visitor4.exe %OUTDIR%

REM encrypted Arma will be at Q:\toolsPipeline\Arma\tools\visitor4\Visitor4_Arma2.exe
REM "C:\Program Files\Aladdin\HASP SRM\VendorTools\VendorSuite\envelope.com" -p W:\tools\hasp\Visitor4_Arma2.prjx
REM copy /y Q:\toolsPipeline\Arma\tools\visitor4\Visitor4_Arma2.exe %OUTDIR%

echo Comitting customer version to SVN (%SVNDIR%)...
svn commit %SVNDIR% -m "Changed: [TOOLS] Visitor4 %VERSION% version %REVISION%"


title Build of %V4NAME% done
echo Build of %V4NAME% done
%NETSEND% "[ %1 ] Building of Visitor4 %VERSION% (%REVISION%) succeeded"
goto end

:buildfail
title Build of %V4NAME% failed
echo Build of %V4NAME% failed
%NETSEND% "[ %1 ] Building of Visitor4 %VERSION% failed"
goto end

:end