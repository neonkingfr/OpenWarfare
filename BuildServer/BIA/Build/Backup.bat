@echo off
rem Backup.bat dstFolder srcFile1 srcFile2 srcFile3 srcFile4

if exist %1\050 rmdir /S /Q %1\050
if exist %1\049 ren %1\049 050
if exist %1\048 ren %1\048 049
if exist %1\047 ren %1\047 048
if exist %1\046 ren %1\046 047
if exist %1\045 ren %1\045 046
if exist %1\044 ren %1\044 045
if exist %1\043 ren %1\043 044
if exist %1\042 ren %1\042 043
if exist %1\041 ren %1\041 042
if exist %1\040 ren %1\040 041
if exist %1\039 ren %1\039 040
if exist %1\038 ren %1\038 039
if exist %1\037 ren %1\037 038
if exist %1\036 ren %1\036 037
if exist %1\035 ren %1\035 036
if exist %1\034 ren %1\034 035
if exist %1\033 ren %1\033 034
if exist %1\032 ren %1\032 033
if exist %1\031 ren %1\031 032
if exist %1\030 ren %1\030 031
if exist %1\029 ren %1\029 030
if exist %1\028 ren %1\028 029
if exist %1\027 ren %1\027 028
if exist %1\026 ren %1\026 027
if exist %1\025 ren %1\025 026
if exist %1\024 ren %1\024 025
if exist %1\023 ren %1\023 024
if exist %1\022 ren %1\022 023
if exist %1\021 ren %1\021 022
if exist %1\020 ren %1\020 021
if exist %1\019 ren %1\019 020
if exist %1\018 ren %1\018 019
if exist %1\017 ren %1\017 018
if exist %1\016 ren %1\016 017
if exist %1\015 ren %1\015 016
if exist %1\014 ren %1\014 015
if exist %1\013 ren %1\013 014
if exist %1\012 ren %1\012 013
if exist %1\011 ren %1\011 012
if exist %1\010 ren %1\010 011
if exist %1\009 ren %1\009 010
if exist %1\008 ren %1\008 009
if exist %1\007 ren %1\007 008
if exist %1\006 ren %1\006 007
if exist %1\005 ren %1\005 006
if exist %1\004 ren %1\004 005
if exist %1\003 ren %1\003 004
if exist %1\002 ren %1\002 003
if exist %1\001 ren %1\001 002
md %1\001
if exist %2 xcopy %2 %1\001 /R /Y
if exist %3 xcopy %3 %1\001 /R /Y
if exist %4 xcopy %4 %1\001 /R /Y
if exist %5 xcopy %5 %1\001 /R /Y
