@echo off
REM %1 is for name of computer

python W:\c\BuildServer\BIA\Build\netsend.py LBF "[ %1 ] Building of LBF started"

echo Getting tools...
call W:\c\BuildServer\BIA\Build\GetTools.bat

echo Getting sources...
call W:\c\BuildServer\BIA\Build\GetSourcesLBF.bat

svn update "p:\tools\landbuilder"

echo Building LandBuilder2...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\LandBuilder\LandBuilder2\LandBuilder2.sln /rebuild /prj="*" /cfg="Release|Win32"
if errorlevel 1 goto buildfail

echo Building Roads...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\LandBuilder\Plugins\Roads\Roads.sln /rebuild /prj=Roads /cfg="ReleaseVBS|Win32"
if errorlevel 1 goto buildfail

echo Building Buildings...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\LandBuilder\Plugins\Buildings\Buildings.sln /rebuild /prj=Buildings /cfg="Release|Win32"
if errorlevel 1 goto buildfail

echo Building Rivers...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\LandBuilder\Plugins\Rivers\Rivers.sln /rebuild /prj=Rivers /cfg="Release|Win32"
if errorlevel 1 goto buildfail

echo Building VisualLandBuilder...
call "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Projects\VisualLandBuilder\VisualLandBuilder.sln /rebuild /prj=VisualLandBuilder /cfg="ReleaseVBS|Win32"
if errorlevel 1 goto buildfail

echo Distributing LandBuilder2 dlls...
xcopy W:\c\Projects\LandBuilder\LandBuilder2\Bin\BuildingPlacers.dll P:\tools\landbuilder /R /Y
xcopy W:\c\Projects\LandBuilder\LandBuilder2\Bin\Erasers.dll P:\tools\landbuilder /R /Y
xcopy W:\c\Projects\LandBuilder\LandBuilder2\Bin\ExamplePlugin.dll P:\tools\landbuilder /R /Y
xcopy W:\c\Projects\LandBuilder\LandBuilder2\Bin\GenericPlacers.dll P:\tools\landbuilder /R /Y
xcopy W:\c\Projects\LandBuilder\LandBuilder2\Bin\RandomPlacers.dll P:\tools\landbuilder /R /Y
xcopy W:\c\Projects\LandBuilder\LandBuilder2\Bin\Terrain.dll P:\tools\landbuilder /R /Y
xcopy W:\c\Projects\LandBuilder\LandBuilder2\Bin\Transformations.dll P:\tools\landbuilder /R /Y
xcopy W:\c\Projects\LandBuilder\plugins\Buildings\bin\Buildings.dll P:\tools\landbuilder /R /Y
xcopy W:\c\Projects\LandBuilder\plugins\Roads\bin\Roads.dll P:\tools\landbuilder /R /Y
xcopy W:\c\Projects\LandBuilder\plugins\Rivers\bin\Rivers.dll P:\tools\landbuilder /R /Y


echo Distributing LandBuilder Family...
cmd /c w:\tools\hasp\envelope.com -p w:\tools\hasp\LandBuilder.prjx

for /F "tokens=3" %%G in (W:\c\Projects\appInfoRev.h) do set COMMITMSG="Changed: [TOOLS] Landbuilder version: %%G"

svn commit "p:\tools\LandBuilder" -m %COMMITMSG%

python W:\c\BuildServer\BIA\Build\netsend.py LBF "[ %1 ] Building and sending of LBF succeeded"

goto end

:buildfail
python W:\c\BuildServer\BIA\Build\netsend.py LBF "[ %1 ] Building of LBF failed"
goto end

:end