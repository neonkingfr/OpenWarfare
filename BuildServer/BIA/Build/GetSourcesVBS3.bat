@echo off
set REVISION=%1
set TARGET=VBS3
set ROOTDIR=W:\c\Archive\VBS3
set REVNODIR=%ROOTDIR%\Poseidon\lib
rem call W:\c\BuildServer\BIA\Build\GetRevNo.bat %REVNODIR% %REVISION%
echo Updating %TARGET% sources to revision %REVISION%...
rem svn update --non-interactive -r %REVISION% %ROOTDIR%
svn update --non-interactive %ROOTDIR%