This tool can remotely execute application from the list specified on the server. The server must be running the application ExeSrv.exe. On the client you can use the application ExeCln.exe to run specified command and arguments. In order to be able to run particular application on the server, the application must be registered in application list in file "appList.txt". Each row of the file specifies one application. First word is the command name, the local path of the application follows. One line of the "appList.txt" file can look like this:

BuildArmaIB c:\BIS\Build\!ServerBuildArmaIB.bat

Usage:
ExeSrv.exe (server side - file appList.txt must be present in the current folder)
ExeCln.exe ServerName ApplicationName [[Argument1], [Argument2], ...]

Example: execln albunda buildarmaib bigamd2
