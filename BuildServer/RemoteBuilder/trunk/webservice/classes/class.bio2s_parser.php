<?

/*
	Parses given bio2s script and returns array of variables and values found inside the script.
	NOTE: Written specially for parsing of the 'projectSettings.txt' file, to get some settings needed for PackBot.
	      There is absolutely no guarantee it will work with ANY bio2s script - for simple ones it might work,
		  but most probably it wont.
*/
class Bio2sParser {

	private static $debug = false;
	private static $content = "";
	private static $buffer = "";
	private static $data = array();
	private static $i = 0;
	private static $contentLen = 0;

	public static function parse($file) {
		self::$content = @file_get_contents($file);
		if (self::$content===false) {
			throw new Exception(__CLASS__ ."->". __FUNCTION__ .": Couldn't read content of file '{$file}'");
		}
		self::$data = array();
		self::$contentLen = strLen(self::$content);
		for (self::$i; self::$i < self::$contentLen; self::$i++) {
			$char = self::$content[self::$i];
			self::eecho("^".$char);
			switch($char) {
				case '=': self::assignment(); break;
				case ';': break;
				case '/':
					if (@self::$content[self::$i -1]!="\\" && @self::$content[self::$i +1]=='/') {
						self::skipComment();
						break;
					}
				default: self::addToBuffer($char);
			}
		}
		return self::$data;
	}

	private static function skipComment() {
		for (self::$i; self::$i < self::$contentLen; self::$i++) {
			$char = self::$content[self::$i];
			if ( substr(self::$content,self::$i,2)=="\r\n" ) {
				self::$i++;
				break;
			} else if (ord($char)==13 || ord($char)==10 ) {
				break;
			}
		}
	}
	
	private static function assignment() {
		$varName = trim(self::$buffer);
		if (!strLen($varName)) {
			throw new Exception(__CLASS__ ."->". __FUNCTION__ .": Content pos ".self::$i.": Assignment found, but variable name string is empty.");
		}
		self::$i++;
		self::$data[$varName] = self::parseVariableValue();
	}
	private static function parseVariableValue() {
		$result = NULL;
		self::$buffer = "";
		for (self::$i; self::$i < self::$contentLen; self::$i++) {
			$char = self::$content[self::$i];
			self::eecho(".".$char);
			switch($char) {
				case '"':
				case "'":
					$result = self::parseString($char);
					break 2;
				case '[': $result = self::parseArray(); break 2;
				case '{': $result = self::parseCode(); break 2;
				default : if (trim($char)!="") { $result = self::parseValue($char); break 2; }
			}
		}
		return $result;
	}

/*
	private static function parseCode() {
		$str = "";
		$init_i = self::$i++;
		$brackets = 1;
		for (self::$i; self::$i < self::$contentLen; self::$i++) {
			$char = self::$content[self::$i];
			self::eecho("_".$char);
			switch($char) {
				case '}':
					$brackets--;
					if ($brackets==0) {
						return array('code' => $str);
					}
					break;
				case '{':
					$brackets++;
				default:
					$str .= $char;
			}
		}
		var_export(self::$data);
		throw new Exception(__CLASS__ ."->". __FUNCTION__ .": Parse error: code at pos {$init_i} is messed up.".PHP_EOL."string:".PHP_EOL.substr(self::$content, $init_i));
	}
*/
	private static function parseCode() {
		$arr = array();
		$init_i = self::$i++;
		self::$buffer = "";
		for (self::$i; self::$i < self::$contentLen; self::$i++) {
			$char = self::$content[self::$i];
			self::eecho("^".$char);
			switch($char) {
				case '=':
					$varName = trim(self::$buffer);
					if (!strLen($varName)) {
						throw new Exception(__CLASS__ ."->". __FUNCTION__ .": Content pos ".self::$i.": Assignment found, but variable name string is empty.");
					}
					self::$i++;
					$arr[$varName] = self::parseVariableValue();
					break;
				case ';': break;
				case '/':
					if (@self::$content[self::$i -1]!="\\" && @self::$content[self::$i +1]=='/') {
						self::skipComment();
						break;
					}
				case '{':
					$arr[] = self::parseCode();
					break;
				case '}':
					return array('code' => $arr);
				default: self::addToBuffer($char);
			}
		}
		return array('code' => $arr);
	}

	private static function parseArray() {
		$arr = array();
		$init_i = self::$i++;
		for (self::$i; self::$i < self::$contentLen; self::$i++) {
			$char = self::$content[self::$i];
			self::eecho(",".$char);
			switch($char) {
				case '"':
				case "'":
					$arr[] = self::parseString($char);
					break;
				case '{':
					$arr[] = self::parseCode();
					break;
				case '[':
					$arr[] = self::parseArray();
					break;
				case ']':
					return $arr;
			}
		}
		//var_export(self::$data,1);
		throw new Exception(__CLASS__ ."->". __FUNCTION__ .": array at pos {$init_i} is messed up.".PHP_EOL."string:".PHP_EOL.substr(self::$content, $init_i));
	}

	private static function parseValue($char) {
		$str = $char;
		$init_i = self::$i++;
		for (self::$i; self::$i < self::$contentLen; self::$i++) {
			$char = self::$content[self::$i];
			self::eecho("*".$char);
			switch($char) {
				case ";":
					self::eecho(PHP_EOL ."VALUE[".$str."]". PHP_EOL);
					// Convert to int or float if it looks like it
					if (preg_match('/^[\-\+\d\.]+$/', $str)) { return (float)$str; }
					if (preg_match('/^[\-\+\d]+$/', $str)) { return (int)$str; }
					// Return variable value if it looks like variable and we can find it in $data array
					$str = trim($str);
					reset(self::$data); // just in case
					if (array_key_exists($str, self::$data)) {
						return self::$data[$str];
					}
					return trim($str);
				default:
					$str .= $char;
			}
		}
		throw new Exception(__CLASS__ ."->". __FUNCTION__ .": assignment value at pos {$init_i} is messed up (not ending with semicolon?).");
	}

	private static function parseString($quote) {
		$str = "";
		$init_i = self::$i++;
		for (self::$i; self::$i < self::$contentLen; self::$i++) {
			$char = self::$content[self::$i];
			self::eecho("`".$char);
			switch($char) {
				case $quote:
					// Look behind to see if previous char slash, or look ahead if it will be another double quote,
					// to add current quote to string. Otherwise we reached the end of string, so stop parsing and return the string.
					if (self::$content[self::$i -1] == "\\") {
						$str .= $quote;
					} else if (@self::$content[self::$i +1] == $quote) {
						$str .= $quote;
						self::$i++;
					} else {
						return $str;
					}
				default: $str .= $char;
			}
		}
		throw new Exception(__CLASS__ ."->". __FUNCTION__ .": string starting at pos {$init_i} is not closed.");
	}
	private static function addToBuffer($char) {
		self::$buffer .= $char;
		return 0;
	}
	private static function eecho($str) {
		if (self::$debug) echo $str;
	}

}
