<?

class Curl {
	public $handle = null; // curl handle
	public $result = null; // holds HTTP response results from latest curl_exec calls
	public $info = null; // array returned by curl_getinfo(), set after every curl_exec call
	public $options = array(); // array of options set through setOpt() method (some values may be incorrect due to curl setting related options internaly without any notification)

	public function __construct ($options = false) {
		$this->handle = curl_init();
		if ($options) $this->SetOpt($options);
		return $this;
	}

	/*	Dummy function being set as CURL progress callback when CURL operation is finished.
		Used when we need to stop calling the real callback func, because there is no way to clear assigned curl callbacks.
	*/
	public static function void() {}

	public function Open ($url) {
		if (!curl_setopt($this->handle, CURLOPT_URL, $url)) {
			// TODO: throw exception
			die(
				__METHOD__ ." line #". __LINE__
				." curl_setopt() failed with error #". curl_errno($this->handle) .": ".curl_error($this->handle)
				.", passed url: '". @$url .'"'
			);
		}
		$this->options[CURLOPT_URL] = $url;
	}

	public function Close () {
		if ($this->handle) curl_close($this->handle);
		$this->handle = false;
	}

	public function Exec () {
		$this->result = curl_exec($this->handle);
		$this->info = curl_getinfo($this->handle);
		if ($this->result===false) {
			// TODO: throw exception
			die(
				__METHOD__ ." line #". __LINE__
				." curl_exec() failed with error #". curl_errno($this->handle) .": ".curl_error($this->handle)
			);
		}
		return $this->result;
	}

	public function SetOpt ($option = null, $value = null) {
		if (is_array($option)) return $this->SetOptArray($option);
		if (!curl_setopt($this->handle, $option, $value)) {
			// TODO: throw exception
			die(
				__METHOD__ ." line #". __LINE__ 
				." curl_setopt($option, $value) failed with error #". curl_errno($this->handle) .": ".curl_error($this->handle)
			);
		}
		$this->options[$option] = $value;
		return true;
	}

	public function SetOptArray ($options = array()) {
		forEach($options as $option => $value) {
			$this->SetOpt($option, $value);
		}
	}
}
