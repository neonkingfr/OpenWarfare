<?

class SenderQueue {

	public static $curl;

	public static function getRequest() {
		global $l;
		if (!file_exists(CFG::$sender_queue_file)) { return false; }
		$f = new File(CFG::$sender_queue_file, 'r', true, 60); // open file with exclusive lock, with 60 seconds timeout to succeed
		$line = @fgets($f->f, 4096); // get first line from file
		$f->close();
		if (!$line) { return false; }
		$request = unserialize($line);
		if ($request === FALSE) {
			$l->log("ERROR: couldn't unserialize request in queue: ".$line);
			self::removeRequest();
			return false;
		}
		return $request;
	}
	public static function removeRequest() {
		if (!file_exists(CFG::$sender_queue_file)) { return false; }
		$f = new File(CFG::$sender_queue_file, 'c+', true, 60); // open file with exclusive lock, with 60 seconds timeout to succeed
		$requests = "";
		$line = @fgets($f->f, 4096); // ignore first line
		while (($line = fgets($f->f, 4096)) !== false) {
			$requests .= $line;
		}
		ftruncate($f->f,0);
		if (!empty($requests)) {
			fseek($f->f, 0, SEEK_SET);
			$f->write($requests);
		}
		$f->close();
	}
	public static function useServer( $request ) {
		global $l;
		$srv = @CFG::$servers[strToLower($request['server'])];
		if (!$srv) {
			$l->log("ERROR: unknown server name '".$request['server']."'",1);
			die;
		}
		forEach ($srv as $key => $value) {
			$request[$key] = $value;
		}
		$request['url'] = $request['protocol'] ."://". $request['hostname'] .':'. $request['port'] .'/' . $request['uri'];
	}
	public static function prepareRequest($request) {
		// Set which remote server we'll connect to, to initialize some additional CFG variables
		self::useServer(&$request);
		// prepare the request
		//$request['xml'] = xmlrpc_encode_request($request['service'], array($request['method'], $request['data']), array('encoding'=>'utf8'));
		$request['xml'] = xmlrpc_encode_request($request['service'], $request, array('encoding'=>'utf8'));
		// Custom header block for cURL
		$request['header'] = array ( "Host: ".$request['hostname'], "Content-type: text/xml", "Content-length: ".strLen($request['xml']) );

		self::$curl = curl_init();
		curl_setopt(self::$curl, CURLOPT_URL, $request['url']);
		curl_setopt(self::$curl, CURLOPT_PORT , $request['port']);
		curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, 1); // save result into a variable instead of printing
		curl_setopt(self::$curl, CURLOPT_HTTPHEADER, $request['header']); // use custom headers (see $header)
		curl_setopt(self::$curl, CURLOPT_CUSTOMREQUEST, 'POST'); // using custom POST with its own specific Content-type
		curl_setopt(self::$curl, CURLOPT_POST, 1);
		curl_setopt(self::$curl, CURLOPT_POSTFIELDS, $request['xml']);
		curl_setopt(self::$curl, CURLOPT_CONNECTTIMEOUT, 10); // number of seconds to wait while trying to connect
		curl_setopt(self::$curl, CURLOPT_TIMEOUT, 15); // maximum number of seconds to allow cURL functions to execute
		
		if ($request['protocol']=='https') {
			curl_setopt(self::$curl, CURLOPT_SSLVERSION, 3);
			//curl_setopt(self::$curl, CURLOPT_SSLCERT, realpath(CFG::$certs_dir.'/client_'.CFG::$client_name.'.cer'));
			//curl_setopt(self::$curl, CURLOPT_SSLKEY,  realpath(CFG::$certs_dir.'/client_'.CFG::$client_name.'.key'));
			//curl_setopt(self::$curl, CURLOPT_CAINFO,  realpath(CFG::$certs_dir.'/ca.cer'));
			//curl_setopt(self::$curl, CURLOPT_SSL_VERIFYPEER, 1);
			//curl_setopt(self::$curl, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt(self::$curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt(self::$curl, CURLOPT_SSL_VERIFYHOST, 0);
		}
		return self::$curl;
	}
	public static function sendRequest($request) {
		global $l;
		$retry = 0;
		$wait = CFG::$wait_on_retry[0];
		while (true) {
			if ($retry==0) {
				echo date("Y-m-d H:i:s"), PHP_EOL;
				$l->log("Sending request to ". $request['url'] . PHP_EOL ."Service: ". $request['service'].".".$request['method'] . PHP_EOL ."Data: ". var_export($request['data'],1), 1);
			} else {
				$l->log("Retry # ".$retry, 1);
			}
			$response = curl_exec( self::$curl ); // sends the request
			if (curl_errno(self::$curl)) {
				echo date("Y-m-d H:i:s"), "  ";
				$l->log("Failed sending request, ERROR: ". curl_errno(self::$curl).": ".curl_error(self::$curl), 1);
				echo "                     Waiting ".$wait." secs before another try." . PHP_EOL;
				usleep( $wait * 1000000 );
				$retry++;
				if (count(CFG::$wait_on_retry)>$retry) {
					$wait = CFG::$wait_on_retry[$retry];
				}
				continue;
			}
			break;
		}

		$curl_info = curl_getinfo(self::$curl);
		curl_close(self::$curl);

		if ($response === false) {
			throw new Exception("Connection failed, check validity of client and server certificates, and make sure they are properly configured - ie. matching the server hostname, etc.");
		}

		// handle the response
		$data = xmlrpc_decode($response);
		if (!is_array($data)) { // an array is expected, if it is not an array, something went wrong.
			// throw new Exception("Malformed response, was expecting an array: ". PHP_EOL . var_export($response,1));
			$l->log("ERROR: Malformed response, array expected", 1);
		}

		// error response
		if (xmlrpc_is_fault($data)) {
			//throw new Exception("Error response: ".$data['faultString'],$data['faultCode']);
			$l->log("ERROR RESPONSE: ".$data['faultCode'].": ".$data['faultString'], 1);
		}

		// responded with OK
		if (@$data['response']=='OK') {
			// It is important to print the response message to STDOUT, so the parent bio2s script can use it to determine result of the request
			$l->log("Response: ". $data['response'] . (@$data['message']!="" ? ", ".$data['message'] : "") , 1);
			$l->log("DATA: ". $response);
			$l->log("Took ". $curl_info['total_time'] ." seconds");
		} else {
			// anything else?
			$l->log(var_export($data,1), 1);
		}

		return $response;
	}
}
