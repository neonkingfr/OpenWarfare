<?

class File {

	public function __construct($path, $mode='a', $exclusive=false, $timeout=2) {
		$this->f = false;
		$this->path = $path;
		$this->mode = $mode;
		$this->exclusive = $exclusive;
		$this->timeout = $timeout;

		$this->open();
		if ($this->f && $exclusive) {
			$this->lock($timeout);
		}
		return $this;
	}

	public function open() {
		$timeout = $this->timeout + time();
		do {
			$this->f = fopen($this->path, $this->mode);
			if (!$this->f) {usleep(100000);}
		} while (!$this->f || $timeout<time());
		if ( !$this->f ) {
			throw new Exception("Failed to open file '{$this->path}' (mode:{$this->mode}) in given time ({$this->timeout} seconds)");
		}
		return $this->f;
	}

	public function lock($timeout = false) {
		if ($timeout) { $this->timeout = $timeout; }
		$timeout = $this->timeout + time();
		do {
			$lock = flock($this->f, LOCK_EX);
			if (!$lock) {usleep(100000);}
		} while (!$lock || $timeout<time());
		if (!$lock) {
			$this->close();
			throw new Exception("Failed to acquire exclusive lock on file '$path' in given time ($timeout seconds)");
		}
		$this->exclusive = true;
		return $lock;
	}
	public function unlock() {
		if ($this->exclusive) {
			flock($this->f, LOCK_UN);
		}
		$this->exclusive = false;
	}

	public function write($data) {
		$written = fwrite($this->f, $data, strlen($data));
		if (!$written) {
			$this->close();
			throw new Exception("Couldn't write to file: ". $this->path);
		}
		return $written;
	}

	public function close() {
		$this->unlock();
		fclose($this->f);
	}

}
