<?
require_once "cfg/_common.php";
require_once "classes/class.logger.php";
require_once "classes/class.file.php";
require_once "classes/class.bio2s_parser.php";
$l = new Logger('xmlrpc_'.CFG::$client_name.' - server.log');

// autoloads the appropriate service class
function __autoload($class) {
	global $l; // instance of logger class

	// load service configuration class first
	$dir = realpath(CFG::$cfg_dir);
	$cfgpath = CFG::$cfg_dir.'/'.$class.'.php';
	if (file_exists($cfgpath)) {
		// make sure the path is safe, before including the file
		$cfgpath = realpath($cfgpath);
		if (substr($cfgpath,0,strLen($dir)) !== $dir) {
			throw new Exception("Service configuration for '$class' cannot be loaded.");
		}
		// load the config file
		include_once $cfgpath;
	} else {
		$l->log("NOTICE: Class '$class' has no configuration file.");
	}

	// load the service class
	$dir = realpath(CFG::$svc_dir);
	$classpath = CFG::$svc_dir.'/class.'.$class.'.php';
	if (file_exists($classpath)) {
		// make sure the path is safe, before including the file
		$classpath = realpath($classpath);
		if (substr($classpath,0,strLen($dir)) !== $dir) {
			throw new Exception("Service '$class' cannot be loaded.");
		}
		// load the class file
		include_once $classpath;
	} else {
		throw new Exception("Service '$class' not found.");
	}
	
}

// Get the class name from request
$l->log("RECEIVED REQUEST: ". $HTTP_RAW_POST_DATA);
$decoded_request = xmlrpc_decode_request($HTTP_RAW_POST_DATA, $class);
$method = @$decoded_request[0]['method'];
if (!$method) {
	throw new Exception("Invalid request: ". var_export($decoded_request,1));
}

// Load the requested class (autoloading function will be triggered by method_exists())
$l->log("Loading service class '$class'");
if (!method_exists($class,$method)) {
	throw new Exception("Service '$class' has no method '$method'.");
}

// setup the XML-RPC "server"
$server = xmlrpc_server_create();
xmlrpc_server_register_method($server, $class, array($class, $method));

// Call the requested method, and return its response
$l->log("Calling $class.$method");
if ($response = xmlrpc_server_call_method($server, $HTTP_RAW_POST_DATA, null, array('encoding'=>'utf8'))) {
	$l->log("RESPONSE: ". $response);
	header('Content-Type: text/xml');
	echo $response;
}

xmlrpc_server_destroy($server); 
die;
