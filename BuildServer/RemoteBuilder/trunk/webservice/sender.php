<?
require_once "cfg/_common.php";
require_once "classes/class.logger.php";
require_once "classes/class.file.php";
require_once "classes/class.sender_queue.php";
$l = new Logger('xmlrpc_'.CFG::$client_name.' - sender.log');

// Loop until exit signalled via existence of special file (configured in common config)
while (true) {
	sleep(1);
	if (file_exists(CFG::$sender_signal_file)) {
		@unlink(CFG::$sender_signal_file);
		$l->log("Signal to exit via ".CFG::$sender_signal_file.", exiting.", 1);
		break;
	}

	// Get request from queue, and prepare XML data
	if ( !$request = SenderQueue::getRequest() ) { continue; }
	if ( !$curl = SenderQueue::prepareRequest(&$request) ) { continue; };

	// Send the request
	SenderQueue::sendRequest(&$request);
	SenderQueue::removeRequest();
}
