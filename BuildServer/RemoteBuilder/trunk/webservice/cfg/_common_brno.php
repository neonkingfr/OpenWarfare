<?
/* -----------------------------------------------------------------------------
	This is only a sample of configuration file for MEGAPACKER server in BRNO.
	The real config file IS UNVERSIONED (not in SVN), because each server has
	different config, but both servers are using same SVN repository.

	In case of accidantaly deleting the real "_common.php" config file, use the
	appropriate sample (ie. "_common_brno.php" for Megapacker server in Brno) 
	to make a new one.
	Simply copy the "_common_brno.php" to "_common.php", then make any desired
	changes in the new "_common.php" file (and do not add it to SVN!).
----------------------------------------------------------------------------- */

// Class with common configuration parameters
class CFG {

	// Determines which certificate to use, and some other things.
	// Valid options are 'lhota' or 'brno'
	public static $client_name = "brno";

	// List of remex "server names" and their addresses and other params
	public static $servers = array(
		'builder_lhota' => array( 'hostname'=>"rpc1.bistudio.com", 'protocol'=>'https', 'port'=>9002, 'uri'=>"webservice/server.php" ),
		'builder_brno'  => array( 'hostname'=>"rpc2.bistudio.com", 'protocol'=>'https', 'port'=>9002, 'uri'=>"webservice/server.php" )
	);

	// Path to the configuration files (used only by the server scripts)
	public static $cfg_dir = "c:/XMLRPC/apache/htdocs/webservice/cfg";

	// Path to where the client certificates are stored
	public static $certs_dir = "c:/XMLRPC/certs";

	// Path to the service classes (used only by the server scripts)
	public static $svc_dir = "c:/XMLRPC/apache/htdocs/webservice/services";

	// Path to the logfiles (used by both client and server scripts)
	public static $log_dir = "c:/XMLRPC/apache/logs";

	/* How many seconds to wait for another try after failing to send a request.
	   First try uses first value in array, second try uses second value, etc.
	   When the process runs out of array elements (during retries for same request),
	   the last one in the array will be used for any subsequent retries. */
	public static $wait_on_retry = array(0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

	// File containing queued requests to forward to remote server
	public static $sender_queue_file = "c:/XMLRPC/apache/logs/xmlrpc - sender_queue.txt";

	// Files which existence serves as a signal to stop the sender service
	public static $sender_signal_file = "c:/XMLRPC/signal.sender_exit";

};
