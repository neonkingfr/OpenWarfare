<?
/* -----------------------------------------------------------------------------
	This is only a sample of configuration file for Sleeper's local test server.
	DO NOT USE THIS CONFIG ON PRODUCTION SERVERS!
----------------------------------------------------------------------------- */

class PackBot_config {

	// Path to 'requests.lst'
	// in bio2s it was (pathToServers + "\"+ selServer +"\requests.lst")
	public static $pack_requests_queue_file = 'c:/XMLRPC/apache/logs/xmlrpc_lhota - requests.lst';

	public static $remote_server = "builder_brno"; // which of the CFG::$servers should be used as a remote server to which the incomming LAN requests will be forwarded

	// Used by PackBot::getUsablePath() to translate given path to something we can actually read files from.
	// ie. for virtual drives mounted via subst, we must translate these drives to the actual real directories,
	// and mounted network shares must be translated to the real newtork path (\\server\shared_dir).
	// Each item in $fixPaths must be associative array( 'pattern' => '...', 'replacement' => '...', 'limit' => -1 )
	// 'limit' sets the constraint on maximum possible replacements (-1 means no limit)
	// NOTE: all $fixPaths items are processed in order as they are defined here.
	public static $fixPaths = array(
		array('pattern' => '/^P:\\\\/i', 'replacement' => 'D:\\\\P\\\\', 'limit' => 1) // Real path to the subst-ed P: drive
	);

	// lists of addons, used to lookup the correct case-sensitive name to be used in svn URL
	public static $addonsLists = array(
		'addonFiles.lst',
		'addonFiles_altar.lst',
		'addonFiles_su.lst'
	);

}