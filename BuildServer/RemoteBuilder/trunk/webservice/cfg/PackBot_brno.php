<?
/* -----------------------------------------------------------------------------
	This is only a sample of configuration file for MEGAPACKER server in BRNO.
	The real config file IS UNVERSIONED (not in SVN), because each server has
	different config, but both servers are using same SVN repository.

	In case of accidantaly deleting the real "PackBot.php" config file, use the
	appropriate sample (ie. "PackBot_brno.php" for Megapacker server in Brno)
	to make a new one.
	Simply copy the "PackBot_brno.php" to "PackBot.php", then make any desired
	changes in the new "PackBot.php" file (and do not add it to SVN!).
----------------------------------------------------------------------------- */

class PackBot_config {

	// Path to 'requests.lst'
	// in bio2s it was (pathToServers + "\"+ selServer +"\requests.lst")
	public static $pack_requests_queue_file = '\\\\velryba\Armatools\packserver\Packer\requests.lst';

	public static $remote_server = "builder_lhota"; // which of the CFG::$servers should be used as a remote server to which the incomming LAN requests will be forwarded

	// Used by PackBot::getUsablePath() to translate given path to something we can actually read files from.
	// ie. for virtual drives mounted via subst, we must translate these drives to the actual real directories,
	// and mounted network shares must be translated to the real newtork path (\\server\shared_dir).
	// Each item in $fixPaths must be associative array( 'pattern' => '...', 'replacement' => '...', 'limit' => -1 )
	// 'limit' sets the constraint on maximum possible replacements (-1 means no limit)
	// NOTE: all $fixPaths items are processed in order as they are defined here.
	public static $fixPaths = array(
		array('pattern' => '/^P:\\\\/i', 'replacement' => 'd:\\\\DevP\\\\', 'limit' => 1) // Real path to the subst-ed P: drive
	);

	// lists of addons, used to lookup the correct case-sensitive name to be used in svn URL
	public static $addonsLists = array(
		'addonFiles.lst',
		'addonFiles_altar.lst',
		'addonFiles_su.lst'
	);

	// Addons not to be synchronized (ie. when packing Limnos island in Brno, we don't want to sync the request to Mnisek)
	// Should contain a list of directories, same as in addonFiles.lst except here it is not case sensitive.
	public static $noSyncAddons = array(
		'ca\map_limnos\data\layers',
		'ca\map_stratis\data\layers'
	);

	// Addons currently forbidden to pack.
	// These addons will not be added to the queue.
	// Use regexp patterns to match addon dirs, ie. '/addon_(f|acr)/'
	public static $forbiddenAddons = array(
	);

}