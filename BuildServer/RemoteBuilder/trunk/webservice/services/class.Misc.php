<?

// class is loaded in the autoloading function.
class Misc extends Misc_config {

	/*
		Stores upload request in a queue.
		Queued requests will be processed later by other process (probably from Task Scheduler).
	*/
	static function queueUpload($method, $params) {

		// Convert input parameters values to lowercase
		$source = strToLower(trim($params[0]['data']['source']));

		// Open queue file for read/write with exclusive lock
		$queue = new File(self::$upload_queue_file, 'c+', true, 60); // open file with exclusive lock, with 60 seconds timeout to succeed

		// Search for same request already in queue, and if found, skip adding it and return OK response
		while (($line = fgets($queue->f)) !== false) {
			if ( strToLower(trim($line)) == $source ) {
				return array("response" => "OK", "message" => "Same request already in queue, no need to add it again.");
			}
		}

		// Write the new request into the queue
		$queue->write($source.PHP_EOL);
		$queue->close();

		return array("response" => "OK", "message" => "Upload request added to queue.");
	}

}