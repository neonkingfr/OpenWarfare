<?

// class is loaded in the autoloading function.
class Test {

	/*
		Does nothing, and notifies the client about it.
		Just for testing purposes.
	*/
	static function doNothing($method = false, $params = false) {
		return array("response" => "OK", "message" => "Test request received, will do nothing.");
	}

}