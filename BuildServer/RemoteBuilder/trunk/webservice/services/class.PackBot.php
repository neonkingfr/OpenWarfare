<?
/*
	Adds given pack request to the local pack requests queue ('requests.lst' file),
	and also to the queue of requests to be sent to remote server (from Mnisek to Brno, or vice versa).
	
	To add request to both queues, use the pack() method.
	To add request to specific queue only, use the add_to_local_queue() or add_to_server_queue()
*/

// class PackBot_config is loaded in the autoloading function.
class PackBot extends PackBot_config {

	/*
		Translates given path according to regular expressions in $fixPaths in PackBot configuration
	*/
	public function fixPath($path) {
		forEach (self::$fixPaths as $r) {
			$path = preg_replace($r['pattern'], $r['replacement'], $path, $r['limit']);
		}
		return $path;
	}

	// project settings loaded from parsing P:\<project>\Doc\<prefix>projectSettings.txt
	private static $project = array();

	/*
		Prevents processing of forbidden addons (see \cfg\PackBot.php -> class PackBot_config -> $forbiddenAddons)
	*/
	private static function exitIfForbiddenAddon($data) {
global $l;
		if (!isset($data['directory']) || empty($data['directory'])) {
			throw new Exception("Addon directory '".$dir."' not set or empty, exiting.");
		}
		// Temporarily, Sleeper can pack forbidden addons
		if (isset($data['kecal']) && preg_match('/kecal\s*\:\s*sleeper/i', $data['kecal'])) {
			return;
		}
		$dir = $data['directory'];
		$dir = strtr($dir, "/","\\");
$l->log("Is '".$dir."' forbidden addon?");
		forEach (self::$forbiddenAddons as $fadir) {
			if (preg_match($fadir, $dir)) {
$l->log("'".$dir."' matched by fadir '".$fadir."'");
				throw new Exception("Trying to pack forbidden addon '".$dir."', exiting.");
			}
		}
	}

	/*
		Loads file '...projectSettings.txt' and parses it for some configuration variables.
		$data: associative array of request parameters - [method, directory, kecal, forcerebin, cleanserver, cmdline, revision]
			...only the 'directory' and 'cmdline' is mandatory in this case.
	*/
	private static function initProjectSettings($data) {
		if (!@$data['directory']) {
			throw new Exception("parseProjectSettings(): Couldn't load project settings". PHP_EOL ."'directory' not found in passed data array: ". PHP_EOL . var_export($data,1));
		}
		if (!@$data['cmdline']) {
			throw new Exception("parseProjectSettings(): Couldn't load project settings". PHP_EOL ."'cmdline' not found in passed data array: ". PHP_EOL . var_export($data,1));
		}
		// Reset project settings
		self::$project = array(
			'name' => "ca",
			'path' => "p:\\ca",
			'platform' => "",
			'settings' => array(),
		);
		// Determine which project to use (which directory to load the 'projectSettings.txt' from)
		$bslashPos = strPos($data['directory'], "\\", 3);
		if ($bslashPos) {
			self::$project['name'] = substr($data['directory'], 3, $bslashPos-3);
			self::$project['path'] = substr($data['directory'], 0, $bslashPos);
		}
		// Determine project platform (added in front of 'projectSettings.txt' filename)
		$isPC = striPos($data['cmdline'], 'pc')!==false;
		$isXBox = striPos($data['cmdline'], 'xbox')!==false;
		$isDemo = striPos($data['cmdline'], 'demo')!==false;
		$isForceExp = striPos($data['cmdline'], 'ForceExp')!==false;
		$isForceACR = striPos($data['cmdline'], 'ForceAcr')!==false;
		if ($isForceACR) {
			self::$project['platform'] = "forceacr";
		} else {
			if ($isForceExp) {
				self::$project['platform'] = "forceexp";
			} else {
				if ($isDemo) {
					self::$project['platform'] = "demo";
				} else {
					if (!$isPC and $isXBox) {
						self::$project['platform'] = "xbox";
					}
				}
			}
		}
		// Update projectsettings file from SVN
		$svn_update_path = self::fixPath(self::$project['path'] ."\\doc");
		exec( "svn update ". $svn_update_path ." 2>&1", $output, $result);
		if (!preg_match('/(at|updated to) revision \d+/i', join(" ",$output))) {
			throw new Exception("Unable to perform SVN update of project settings directory '".$svn_update_path."': ". PHP_EOL . PHP_EOL .join(PHP_EOL,$output));
		}
		// Load and parse the settings file
		$file = self::fixPath(self::$project['path'] ."\\doc\\". self::$project['platform'] ."projectSettings.txt");
		self::$project['settings'] = Bio2sParser::parse($file);

		// Lookup given addon directory in project settings 'specialFolders' array, and initialize addonconfig settings according to what is found in there.
		self::$project['addonconfig'] = array();
		$specialFolders = @self::$project['settings']['specialFolders'];
		if ($specialFolders && count($specialFolders)) {
			// We receive the directory to be packed as "P:\ca\ui_f\data", but in the specialFolders array it is written
			// as "ui_f_data", so we must explode the string into array by '\', ditch the first two items ("P:" and "ca"),
			// and then start searching for the rest with slash replaced by underscore (ie. "ui_f_data").
			$dir_tokens = explode("\\", $data['directory']);
			array_shift($dir_tokens); // remove the first item ("P:")
			array_shift($dir_tokens); // remove the second item ("Ca")
			// now glue the rest back together with underscores
			$addon = strToLower(implode("_", $dir_tokens));
			forEach ($specialFolders as $item) {
				if (strToLower($item[0]) == $addon) { // addon found
					if (@$item[1]['code']) {
						self::$project['addonconfig'] = $item[1]['code'];
						break;
					}
				}
			}
		}
	}

	/*
		Requests received from LAN client (send by client.php) must be written to both "packing queue" and "sender queue",
		while WAN requests (send by sender.php) must be written only to the local (on the remote) "packing queue", which
		is the reason for splitting the actual process into the two functions "add_to_local_queue" and "add_to_sender_queue".
	*/
	static function pack($method, $params) {
		global $l;

		// Find latest revision number for given directory, and add it to the request in order to make sure the remote server will pack same revision
		if (@$params[0]['data']['directory'] != "") {

			// Ignore forbidden addons
			self::exitIfForbiddenAddon($params[0]['data']);
		
			self::initProjectSettings($params[0]['data']); // Determine which project settings file to load, and get some settings from it (self::$project['settings'])

			// Find the given directory in addon lists, to get its proper case sensitive name to be used in svn URL to ask for latest revision number.
			$search_str = strToLower(substr(trim($params[0]['data']['directory']),3)); // string to search for in the addonslist (ie. hsim\air_h)
			$addon_uri = false; // Proper case-sensitive addon URI for given $search_str (ie HSim/Air_H)
			forEach (self::$addonsLists as $addonsLstFile) {
				$file = self::fixPath(self::$project['path'] ."\\doc\\". self::$project['platform'] .$addonsLstFile);
$l->log("Searching for '$search_str' in addons list ".$file);
				if (!file_exists($file)) {
					//throw new Exception("Couldn't open addons list file '".$file."'");
					continue;
				}
				$list = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
				forEach ($list as $item) {
//$l->log("   ".strToLower(trim($item)));
					if (strToLower(trim($item))==$search_str) {
						$addon_uri = strtr(escapeshellarg($item),"\\", "/");
$l->log("Found '$search_str': ".strToLower(trim($item)));
						break 2;
					}
				}
			}
//$l->log("Done searching in addons lists");
			if (!$addon_uri) {
//$l->log("Couldn't find '$search_str' in addons lists");
				throw new Exception("Couldn't find given path '".$params[0]['data']['directory']."' in addonslists".PHP_EOL."Searching for '".$search_str."'");
			}
			// Ask for latest revision number only if addon is in SVN
			// First find out which SVN url to use:
			$svnUrl = false;
			//    ...by default, use the global SVNPath parametr from projectSettings
			if (array_key_exists('SVNPath', self::$project['settings']) && is_array(self::$project['settings']['SVNPath']) && count(self::$project['settings']['SVNPath']) > 0) {
				$svnUrl = @self::$project['settings']['SVNPath'][0];
			}
			//    ...and if exists, use the SVNPath parameter of given addon in specialFolders array
			if (array_key_exists('SVNPath', self::$project['addonconfig']) && is_array(self::$project['addonconfig']['SVNPath']) && count(self::$project['addonconfig']['SVNPath']) > 0) {
				$svnUrl = @self::$project['addonconfig']['SVNPath'][0];
			}
	
			// Even thought the addon is NOT in SVN, we might still have $svnUrl set, so we need to check also for NonSSSource.
			// NonSSSource is defined only for addons that are not in SVN.
			if ($svnUrl && !@self::$project['addonconfig']['NonSSSource']) {
				//exec( "svn info -r HEAD https://dev.bistudio.com/svn/addons/".$addon_uri." 2>&1", $output, $result);
				exec( "svn info --non-interactive -r HEAD ".$svnUrl.$addon_uri." 2>&1", $output, $result);
				// Add the revision number to the request
				if (preg_match('/Revision: (\d+)/i', join(" ",$output), $matches)) {
					$params[0]['data']['revision'] = $matches[1];
				} else if (preg_match('/\(not a valid url\)/i', join(" ",$output))) {
					throw new Exception("Unable to find addon '".$addon_uri."' in SVN: ". PHP_EOL .join(PHP_EOL,$output));
				}
			} else {
				// If found in addonslist, but not in SVN (ie. the "languagecore" addon), pack anyway
				$params[0]['data']['revision'] = 0; // since the addon is not in SVN, it doesn't matter what we set the revnum to
			}
		} else {
			throw new Exception("Missing 'directory' parameter in passed data array: ".var_export($params,1));
		}

		// Write the request to the local queue file
		self::add_to_local_queue($method, $params);

		// Write the request to the sender queue file (used to send requests from Lhota to Brno and vice versa).
		// ...skip addons which are NOT to be synchronized (see \cfg\Packbot.php configuration file).
		$syncIt = !in_array($search_str, self::$noSyncAddons);
		// ...skip non-Arma3 addons (its "tools" variable does not contain "Futura")
		$tools = array_key_exists('tools', self::$project['addonconfig']) ? self::$project['addonconfig']['tools'] : self::$project['settings']['tools'];
		$syncIt = $syncIt && (stripos($tools, "futura") !== FALSE);
		// ...skip addons with non-empty NonSSSource
		$syncIt = $syncIt && empty($project['addonconfig']['NonSSSource']);
		
		if ($syncIt) {
			self::add_to_sender_queue($method, $params);
		}
		return array("response" => "OK", "message" => "Request has been added to the queue.");
	}

	/*
		Save request to local pack server queue file.
		On remote server it is called directly, instead from the "pack" function which is used only for requests comming from within LAN.
	*/
	static function add_to_local_queue($method, $params) {
		global $l;

		// Ignore forbidden addons
		self::exitIfForbiddenAddon(@$params[0]['data']);

		// seems stupid converting to string and back to array, but the first conversion takes care of sanitizing some of the data
		$out = self::requestArrayToString(@$params[0]['data']);
		$req_new = self::requestStringToArray($out);
		if ($req_new === false) {
			throw new Exception("PackBot.requestStringToArray() failed? \$req_new is FALSE (as if it is autoupdate request) \$out = ".var_export($out,1));
		}
		/*
			Had to rewrite this process of adding requests to local queue:
			- Existing requests with same parameters, but older revision, should be replaced by new request with newer revision.
			- Existing requests same as new, but requested by different user(s), should be put together into one request
			  with <kecal> variable expanded accordingly.
		*/
		// Open queue file for read/write with exclusive lock
		$file = new File(self::$pack_requests_queue_file, 'c+', true, 60); // open file with exclusive lock, with 60 seconds timeout to succeed
		// Read queue into array, while trying to merge the new request with any of the existing ones
		$queue = array();
		$merged = false;
		while (($line = fgets($file->f)) !== false) {
			if (trim($line)=="") continue; // ignore possible blank lines
			$req_old = self::requestStringToArray($line);
			if ($req_old !== false) {
				if (!$merged) {
					$merged = self::mergeRequests($req_old, $req_new);
					if ($merged) {
						$queue[] = self::requestArrayToString($merged);
						continue;
					}
				}
			}
			$queue[] = trim($line);
		}
		// If the new request wasn't merged with any of the existing ones, add it to end of the array
		if (!$merged) {
			$queue[] = $out;
		}
		// Replace old queue with the new one stored in the array
		ftruncate($file->f, 0);
		fseek($file->f, 0);
		forEach ($queue as $i => $line) {
			$file->write($line.PHP_EOL);
		}
		$file->close();
		if ($merged) {
			$l->log("Request merged into local queue: ".trim(print_r($merged, true)));
			return array("response" => "OK", "message" => "Request has been merged into another already in queue.");
		} else {
			$l->log("Request added to local queue: ".trim($out));
			return array("response" => "OK", "message" => "Request has been added to the queue.");
		}
/*		$f = new File(self::$pack_requests_queue_file, 'a', true, 60); // open file with exclusive lock, with 60 seconds timeout to succeed
		$f->write($out);
		$f->close();
		$l->log("Request added to local queue: ".trim($out));
		return array("response" => "OK", "message" => "Request has been added to the queue.");
*/
	}

	/*
		Tries to merge two requests.
		The conditions for merging requests are:
			1. All parameters must be same, except for 'kecal' and 'revision'.
			2. If revisions differ, the latest one is used.
			3. Arrays of kecal user names are joined and the result is used.
		Returns the merged request, or False if the requests cannot be merged.
	*/
	static function mergeRequests($r1, $r2) {
		// 1. All parameters must be same, except for 'kecal' and 'revision'.
		if (strToLower($r1['method']) != strToLower($r2['method'])) return false;
		if (strToLower($r1['directory']) != strToLower($r2['directory'])) return false;
		if (strToLower($r1['forcerebin']) != strToLower($r2['forcerebin'])) return false;
		if (strToLower($r1['cleanserver']) != strToLower($r2['cleanserver'])) return false;
		if (strToLower($r1['cmdline']) != strToLower($r2['cmdline'])) return false;
		// we'll merge $r2 into $r1 and return it
		if ($r1['revision'] < $r2['revision']) $r1['revision'] = $r2['revision'];
		forEach ($r2['kecal'] as $user) {
			if (!in_array($user, $r1['kecal'])) {
				$r1['kecal'][] = $user;
			}
		}
		return $r1;
	}

	/*
		Takes associative array of request parameters and returns string which can be written into request queue.
	*/
	static function requestArrayToString($array) {
		$out = '["<method>", "<directory>", [<kecal>], <forcerebin>, <cleanserver>, <revision>, "<cmdline>"]';

		// Kecal can be a string with single username, or an array of usernames.
		// In any case, before replacing placeholder in $out, every username must be enclosed in double quotes and the array imploded to string.
		if (is_array($array['kecal'])) {
			forEach ($array['kecal'] as &$user) {
				$user = '"'.$user.'"';
			}
			$array['kecal'] = implode(",", $array['kecal']);
		} else {
			$array['kecal'] = '"'.$array['kecal'].'"';
		}

		// Replace the placeholders in $out with actual values
		if ($array && is_array($array)) {
			forEach ($array as $key => $val) {
				$out = str_replace('<'.$key.'>', $val, $out);
			}
		} else {
			throw new Exception("PackBot.requestArrayToString() failed, invalid input (array expected): ".var_export($array,1));
		}

		return $out;
	}

	/*
		Parses given string (a line taken from the pack requests queue) into associative array(method,directory,kecal,forcerebin,cleanserver,revision,cmdline).
		Returns array with request values, or boolean FALSE for autoupdate requests.
		NOTE: 'kecal' item is returned as an array of user names.
	*/
	static function requestStringToArray($str) {
		if (preg_match('/^\[\"update\".*$/', trim($str), $matches)) {
			return false;
		} else {
			if (!preg_match('/^\[\"(.*)\",\s*\"(.*)\",\s*\[(.*)\],\s*(true|false),\s*(true|false),\s*(\d+),\s*\"(.*)\"\][\s\r\n]*$/', trim($str), $matches)) {
				throw new Exception("PackBot.requestStringToArray() failed, invalid input data: ".var_export($str,1));
			}
			return array(
				'method' => $matches[1],
				'directory' => $matches[2],
				'kecal' => explode(',', str_replace("\"", "", str_replace("'", "", $matches[3]))),
				'forcerebin' => $matches[4],
				'cleanserver' => $matches[5],
				'revision' => (int)$matches[6],
				'cmdline' => $matches[7]
			);
		}
	}

	/*
		Saves request to "sender queue" to forward it to the remote pack server (ie. from Lhota to Brno).
		Supposed to be called only from "pack" function, which is being used for requests comming from within the LAN.
	*/
	static function add_to_sender_queue($method, $params) {
		global $l;

		// Ignore forbidden addons
		self::exitIfForbiddenAddon(@$params[0]['data']);

		$request = array(
			'server' => self::$remote_server,
			'service' => __CLASS__,
			'method' => 'add_to_local_queue',
			'data' => @$params[0]['data']
		);
		$request['data']['kecal'] = 'noreply'; // replace the 'kecal' parameter, as there are different users on the remote server.
		$out = serialize($request) .PHP_EOL;
		$f = new File(CFG::$sender_queue_file, 'a', true, 60); // open file with exclusive lock, with 60 seconds timeout to succeed
		$f->write($out);
		$f->close();
		$l->log("Request added to sender queue: ".trim($out));
		return array("response" => "OK", "message" => "Request has been added to the queue.");
	}
}