<?php
require_once 'commonFunctions.php';

$hostName = strtolower(gethostname());

if (($hostName == "newbuilder") || ($hostName == "hozki-pc")) {
    $location = "Mníšek";
    $serverPrefix = "\\\\server\\diskx\\";
}
else if ($hostName == "megapacker") {
    $location = "Brno";
    $serverPrefix = "\\\\velryba\\armatools\\";
}
else throwFatalError("Couldn't find the location of the server!");

readSettings();
?>
<!DOCTYPE html>
<html>
    <head>
        <script>
            function confirmAddon() {
                var selectElement = document.getElementById("addonSelect");
                var selectedAddon = selectElement.options[selectElement.selectedIndex].value;

                return confirm('Are you sure to send ' + selectedAddon + ' to steam publishing queue?');
            }

        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>SteamPublisher</title>
    </head>
    <body>
        <form action="sendToSteam.php" method="post" onSubmit="return confirmAddon();">
            <div id="wrapperDiv">
                <div id="upperDiv">
                    <h1>SteamPublisher <sup>||<?php echo $location; ?>||</sup></h1>
                </div>
                <div id="leftDiv">
                    <ul>
                        <li><a href="#">Some link 0</a></li>
                        <li><a href="#">Some link 1</a></li>
                        <li><a href="#">Some link 2</a></li>
                    </ul>
                </div>
                <div id="mainDiv">
                    <table id="selectAddonTable">
                        <tr>
                            <td>Select addon:&nbsp;</td>
                            <td>
                                <select id="addonSelect" name="addonKey">
                                    <?php
                                    $addonsKeys = array_keys($addons);

                                    if (array_key_exists("selected",$_GET)) {
                                        $selectedAddonKey = $_GET["selected"];
                                    } else {
                                        $selectedAddonKey = "";
                                    }

                                    foreach ($addonsKeys as $addonKey) {
                                        if (strcasecmp($addonKey, $selectedAddonKey) == 0) {
                                            echo '<option value="' . $addonKey . '" selected>' . $addonKey . '</option>';                                    
                                        }
                                        else {
                                            echo '<option value="' . $addonKey . '">' . $addonKey . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="hidden" name="serverPrefix" value="<?php echo $serverPrefix; ?>" />
                                <input type="submit" value="Add to queue"/>                                  
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </body>
</html>
