<?php
require_once 'commonFunctions.php';

function throwError($msg) {
    echo '<script>alert("Error: \\n\\n ' . $msg . '");</script>';
    redirectToMain();
}
function redirectToMain($additionalValues = "") {
    echo "<script>window.location=\"index.php" . $additionalValues . "\";</script>";
    die();
}

readSettings();
$addonKey = $_POST["addonKey"];

if (!array_key_exists($addonKey, $addons)) {
    throwError($addonKey . " not found in settings\\\\settings.json");
}

$serverPrefix = $_POST["serverPrefix"];
$addonRelativePath = $addons[$addonKey][0];
$addonPath = $serverPrefix . $addonRelativePath;

$filesToUpload = array($addonPath, $addonPath . ".a3.bisign");

//check if files exist
foreach ($filesToUpload as $fileToUpload) {
    if (!file_exists($fileToUpload)) {
        throwError(addslashes($fileToUpload) . " not found!");
    }
}

//check addon size
$addonSize = filesize($addonPath) / 1024 / 1024;
$addonSizeLimit = $addons[$addonKey][1];

if ($addonSize > $addonSizeLimit) {
    throwError("File size of " .  addslashes($addonKey) . " is bigger than allowed!");
}

//write to steam requests.lst
$outputString = implode(PHP_EOL,$filesToUpload) . PHP_EOL;
$requestsFile = file_put_contents($serverPrefix . "Steam\\Development\\requests.lst", $outputString, FILE_APPEND | LOCK_EX);

if ($requestsFile === FALSE) {
    throwError("Failed to open file " . addslashes($serverPrefix) . "Steam\\\\Development\\\\requests.lst");
}

echo "<script>alert(\"Request Sent Successfully\");</script>";
redirectToMain("?selected=" . addslashes($addonKey));

