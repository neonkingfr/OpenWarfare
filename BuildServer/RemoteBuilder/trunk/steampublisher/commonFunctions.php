<?php
function throwFatalError($msg) {
    header("Location: error.php?msg=" . urlencode($msg));
    die();
}

function readSettings() {
    global $addons;

    @$fileContent = file_get_contents("settings\\settings.json");
    if ($fileContent !== FALSE) {
        $addons = json_decode($fileContent, TRUE);
    }
    else {
        throwFatalError("Unable to read settings\\settings.json,<br/>contact Hozki for help.");
    }        
}