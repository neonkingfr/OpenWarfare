@echo off
echo Sending history...

pushd .
c:
cd \bis\bedar\_output

REM echo %1 %2 %3 %4 %5 %6>sendHistory.log

md x:\verhistory\bin 2>nul

xcopy el.history x:\verhistory\bin /D /Y /Q
xcopy es.history x:\verhistory\bin /D /Y /Q
xcopy rv.history x:\verhistory\bin /D /Y /Q
xcopy physicallibs.history x:\verhistory\bin /D /Y /Q
xcopy poseidon.history x:\verhistory\bin /D /Y /Q
xcopy extern.history x:\verhistory\bin /D /Y /Q

popd



