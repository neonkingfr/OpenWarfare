@echo off
echo Sending history...

pushd .
c:
cd \bis\bedar\_output

md x:\verhistory 2>nul
md x:\verhistory\file 2>nul

REM echo %1 %2 %3 %4 %5 %6>sendHistory.log
xcopy *.history x:\verhistory /Q /D /Y 

cd file
echo xcopy c:\bis\bedar\_output\file\*.history x:\verhistory\file /Q /D /Y 
xcopy *.history x:\verhistory\file /Q /D /Y 

echo sendHistory.bat finished.
popd
