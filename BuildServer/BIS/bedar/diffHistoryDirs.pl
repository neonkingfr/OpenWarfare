#Warning - due to case sensitive Perl, compare may work badly
#Warning - copy doesnt alternate case on filenames

use File::Copy;
use File::Compare;




@newhistbeforediffremove = <o:\\arma\\verhistory\\new\\*.*>;
@oldhist = <o:\\arma\\verhistory\\*.*>;
@diffs = <o:\\arma\\verhistory\\*_diff.*>;

$cnt=0;
foreach $file (@newhistbeforediffremove)
{
  if ($file =~ /_diff./)
  {#last pack difference files from x:\verhistory\ab_packlog\
    unlink($file);
    #print "\nDeleting new\*_diff.* file: $file";
    next;
  }
  
  $newhist[$cnt]=$file;
  $cnt++;
}



foreach $file(@diffs)
{#delete all old diffs
  
  if ($file =~ /ArmA2Int_diff.rpt/) 
  {} #do not delete ArmA2Int_diff.rpt - report difference file that originate in extract_rpt.bat
  else
  {
    unlink("$file");
    #print "\nDeleting diff file: $file";
  }
} 


$cnt=0;
foreach $file(@newhist)
{  
  $file =~ /\\([^\\]+)$/; #name of file from full path 
  $newhist[$cnt] = $1;   
  $cnt++;
}

$cnt=0;
foreach $file(@oldhist)
{  
  $file =~ /\\([^\\]+)$/; #name of file from full path 
  $oldhist[$cnt] = $1;   
  $cnt++;
}

#print "@newhist\n";
#print "@oldhist\n";

@hash{@oldhist}=();

foreach $file(@newhist)
{#foreach newly copied history file
  #print "\n$file";  
    
  $file =~ /^(.+)\.(.+)/;
  $difffile = "$1_diff.$2";
  #print "difffile: $difffile";    
  
  if (!exists $hash{$file})
  {#new file is not in old files
    copy("o:\\arma\\verhistory\\new\\$file", "o:\\arma\\verhistory\\$file") or die "File cannot be copied."; #copy to old        
    copy("o:\\arma\\verhistory\\new\\$file", "o:\\arma\\verhistory\\$difffile") or die "File cannot be copied."; #copy to old                
    unlink("o:\\arma\\verhistory\\new\\$file"); 
    print "\nNew history file: $file";
  }
  else
  {
    #print"\ncomparing.. $file vs \\new\\$file";
    if (compare("o:\\arma\\verhistory\\$file","o:\\arma\\verhistory\\new\\$file") == 0) 
    {#same files, no change, delete new
      unlink("o:\\arma\\verhistory\\new\\$file"); 
    }
    else
    {#derive difference - filter new file by old, store into o:\arma\verhistory\*_diff.history   
      system("c:\\bis\\bedar\\filter\\filter.pl -l o:\\arma\\verhistory\\new\\$file o:\\arma\\verhistory\\$file>o:\\arma\\verhistory\\$difffile");           
      copy("o:\\arma\\verhistory\\new\\$file", "o:\\arma\\verhistory\\$file") or die "File cannot be copied."; #copy to old
      unlink "o:\\arma\\verhistory\\new\\$file";
    }    
  }
  
}




