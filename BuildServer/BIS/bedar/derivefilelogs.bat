@echo off
pushd .


call c:\bis\bedar\setaddonname-temp.bat

c:
md \log 2>nul
md \log\filelog 2>nul

cd \log\filelog



perl c:\bis\bedar\taggedToFiles.pl \temp\log\%addonname%.log



REM noduplicity dir, remove duplicities from files
md \log\filelog\noduplicity 2>nul
for %%f in (*.*) do perl c:\bis\bedar\filter\sortfreq.pl -nosort -sn "%%f">"noduplicity\%%f"




md x:\verhistory 2>nul
md x:\verhistory\ab_filelog 2>nul

REM xcopy p:\log\filelog\*.* x:\verhistory\ab_filelog /Y /D /Q

echo xcopy p:\log\filelog\noduplicity\*.* x:\verhistory\ab_filelog /Y /D /Q
xcopy p:\log\filelog\noduplicity\*.* x:\verhistory\ab_filelog /Y /D /Q

echo derivefilelogs.bat finished.
popd