#Preparing send for autotestresult
#
#warning: message -No autotest failure information found- needs to be the same in autotestresult.bat to work well
open(FSOURCE,"tempresult2.bat") || die("Error: Cannot open.");

@lines = <FSOURCE>;


#if @lines =~ /^(\s+\n+)*$/
	#{
	
	#}

foreach $line (@lines)
{
	$_ = $line;
	$line =~ s/\"\"-No autotest failure information found-(.*<.*)\"\"/\"$1\"/g;	
	 #substitute from ""-No autotest failure found-.blah.blah.<.blah."" to ".blah.blah.<.blah."
	 #purpose: delete -No autotest failure found- msg when < (leading bracket from <Autotest) found
	 
	print "$line";
}

