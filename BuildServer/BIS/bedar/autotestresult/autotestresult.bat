@echo off
REM AUTOTESTRESULT - by vilem
REM Bunch of scripts to find autotest result in report (<autotest...> </autotest> tags)
REM and convert them into batch file: c:\bis\kecal\kecal.exe %1 info..about..autotest..from..report.
REM
REM Function: if some autotest tags found in .rpt, c:\bis\kecal\kecal.exe will send everything that is
REM           between them, including them.
REM           If there is no autotest info found (no tags in .rpt, .rpt not found), net
REM           send will send "-No autotest failure information found- " message.
REM
REM Uses Perl scripts.

REM how much lines from bottom of .rpt to display in window (<15 is reasonable)
set howmuchlines=10

echo Extracting info from report...

c:
cd \bis\bedar\autotestresult\
echo c:\bis\kecal\kecal.exe %1 ""-No autotest failure information found->tempresult.bat
perl at-result.pl "C:\Documents and Settings\%USERNAME%\Local Settings\Application Data\ArmA 2\arma2int.rpt" 1>>tempresult.bat 2>&1
echo ________________________________________________________________________________________________________________________________________________________________________________________________---Last-%howmuchlines%-lines-of-.rpt-(without-linebreaks):----------------------------------------------------------------->>tempresult.bat

perl lastlines.pl "C:\Documents and Settings\%USERNAME%\Local Settings\Application Data\ArmA 2\arma2int.rpt" "%howmuchlines%" 1>>tempresult.bat 2>&1
echo "">>tempresult.bat

REM echo ""                                                                                      Last lines of report:                                                                                                                        "">>tempresult.bat


perl tooneline.pl tempresult.bat>tempresult2.bat
perl prepareforsend.pl>tempresult3.bat
call tempresult3.bat


REM delete intermediate files - rem this line for debug:
REM del tempresult*.bat