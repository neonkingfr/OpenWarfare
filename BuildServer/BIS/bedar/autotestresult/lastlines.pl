#prints number of last lines given by a parameter

$source = shift;
while ($source =~ /^-(.+)/) {$source = shift;} #

open(FSOURCE,$source) || die("Error: Cannot open source file: \"$source\"\nUse: -help\nDied");

$howmuchlines=shift;

if ($howmuchlines) {} else {die("Give number of lines to print.");};
 
@lines = <FSOURCE>;
$numberoflines = "".@lines."";

if ($howmuchlines>$numberoflines) {$howmuchlines = $numberoflines;}

for ($i=0;$i<$howmuchlines;$i++)
{
  #print "$numberoflines-$howmuchlines+$i: ";
  print "$lines[$numberoflines-$howmuchlines+$i]";
}
