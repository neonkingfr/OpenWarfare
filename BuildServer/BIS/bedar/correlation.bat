REM creates scripts for determining difference btw histories (addons, bins)
REM aggregates to output
@echo off

echo Deriving differences in history...
perl c:\bis\bedar\diffHistoryDirs.pl

md o:\arma\verHistory\output 2>nul
perl c:\bis\bedar\diffAggregateToOutput.pl %computername%
