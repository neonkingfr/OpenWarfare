
#Vilem - FILTROVÁNÍ TEXTU SOUBORU / HLEDÁNÍ V TEXTU SOUBORU
#
# hleda ve zdrojovem souboru nacitanem po radcich slova a slovni spojeni 
# - radky, ktere odpovidaji pozadavkum se bud filtruji nebo vypisuji
#
#case insensitive!
#
#

#take arguments (ex: -help) and search for "help"
$filtrujeme=1;
$linefiltermode=0;
$comlinetokens=0;

@args = @ARGV;
argum();

#the first item on the command line is source
#the second item on the command line is filtering file

$source = shift;
while ($source =~ /^-(.+)/) {$source = shift;} #
open(FSOURCE,$source) || die("Error: Cannot open source file: \"$source\"\nUse: -help\nDied");



if ($comlinetokens == 0)
  {
  $filterby = shift;
  open(FFILTERBY,$filterby) || die("Error: Cannot open filterby file: \"$filterby\"\nUse: -help\nDied");
  @matchthis = <FFILTERBY>; #all lines of filterby file are taken
  } 
  else
  {
    @prac = @ARGV;
        
    foreach $pom(@prac)
    {#command line tokens are on "next line each" -> we need to build one line of them 
      $pom =~ s/\"/\\\"/g;# substitute " -> \" (will be undone later)
      #print "\npom: $pom";
       
      if ($pom eq "'") {$i++; next;}  
      if ($pom =~ /^.+\s.+$/)
      {#"a b c" is seen as _a b c_ line, we need " to put back if there are space between (we have more words)
      $matchthis[$i] .= " \"";
      $matchthis[$i] .= $pom;
      $matchthis[$i] .= "\"";
      }
      else
      {
      $matchthis[$i] .= " ";
      $matchthis[$i] .= $pom;       
      }
      
      
    }
        
  }
  
  #print "\n\n@matchthis\n";
  #$i=0; for($i=0;$i<20;$i++) {print "\n$matchthis[$i]";};
  #exit;  
  
  
#exit;
  
  
  if ($linefiltermode==0)
  {  
    foreach $line(@matchthis)
    {#perl-regular expressions-specific-characters need to pe preceded by \ to take no effect in regular expression          
          $_ = $line;
          #this chars: / \ ( ) [ ] ^ $ # . * + ? { } | , we need to have like \/ \\ \( etc.
          # \/ \\ \( \) \[ \] \^ \$ \# \. \* \+ \? \{ \} \| \,
          # \/|\\|\(|\)|\[|\]|\^|\$|\#|\.|\*|\+|\?|\{|\}|\||\,  
          $line =~ s/(\\|\/|\(|\)|\[|\]|\^|\$|\#|\.|\*|\+|\?|\{|\}|\||\,)/\\$1/g;  #put \ before perl-regular-expression-specific characters    
      #warning: ultraedit shows line commented ^ here (not understanding \# is not a comment)
          #print ("\n2.: $line");    
    }
    
  
  }
  
    
    
  
  
  while (<FSOURCE>) 
    {
      $sourceline = $_;
      
            
      next if $sourceline =~ /^\s*$/;              
      next if $sourceline =~ /^\s*\n$/;              
      next if $sourceline eq "\n";              
      
      $linematched = 0;
      
      foreach $line (@matchthis)
        {        
        #print "\nsourceline: $sourceline";
        #print "filterby line: $line";      
        
          
        
        
          if ($linefiltermode == 0)
          {#parse keywords in filterbyfile  
            
           #START - tokenization
            next if $line =~ /^\s*$/;              
            next if $line eq "\n";              
          
            $line2=$line;
            $line1=$line;
  
            $_ = $line1;
            $line1 =~ s/\"[^\"]*\"//g; #not in " "
            
            $_ = $line2;
            $line2 = "" if $line2 !~ /[^\"]*\"([^\"]+)\"[^\"]*/; #line2 used only if " " are present
            #$line2 =~ s/[^\"]*\"([^\"]+)\"[^\"]*/\"$1\" /g;    #line2 holds only what was in " "            
            
            #print "line2bef: $line2\n";              
            $line2 =~ s/[^\"]*\"([^\"]+)\"[^\"]*/\"$1\" /g;    #line2 holds only what was in " "            
            $line2 =~ s/(\".*\").+/$1/g;    #what isnt between "" and is at the end of line is discarded
            $line2 =~ s/\"\"//g; #empty "" are deleted
    
    
            #print "line1: $line1\n";
            #print "line2: $line2\n";              
      
            @tokens = split (/\s+/,$line1);
            @tokens2 = split (/\"/,$line2);
      
            $i=0;
            foreach $token (@tokens) {$i++;}
            foreach $token (@tokens2) {$tokens[$i]=$token; $i++;} #adding "" tokens to one word tokens
          #print "new tokens (after adding \"*\"): @tokens\n"; #by vilem
          
          
          $isthere=1;         
          foreach $token (@tokens)  
            {#match tokens from one filterbyfile row          
              $cnt++;
              next if $token =~ /^$/; #no empty tokens
              next if $token =~ /^ $/; #no onespacetokens
              
              if (!$linefiltermode)
              {#match whole lines when in line mode - including \n
                chop($token) if $token =~ /.*\n/; #kill possible \n at the end
              }
              
              #print "$cnt.$token\n";
                        
              #print ">>$token in: $sourceline" if $sourceline =~ /$token/i;
              #print ">>$token not in: $sourceline" if $sourceline !~ /$token/i;
              
              #print "+\"$token\" " if $sourceline =~ /$token/i;
              #print "-\"$token\" " if $sourceline !~ /$token/i;
              
              $_ = $line;
              
              
              #print ("token: $token");

              #print "\nsource: $sourceline";
              #print "\ntoken: $token";

              
              $token = lc($token);          
              $sourcelineLC = lc($sourceline);    
              $isthere = 0 if $sourcelineLC !~ /$token/i; #if $token not in $sourceline set isthere to 0
              
            }      
            
            #print "\nFOUND: $sourceline" if $isthere == 1;

            if ($isthere == 1)
            {#if we found matching token row (with tokens)             
              print "$sourceline" if (!$filtrujeme); 
              $linematched=1;                        
              last;
            }
            
            
          }
          else
          {#matchwhole lines ($linefiltermode == 1)
            $line = lc($line);
            $sourcelineLC = lc($sourceline);
            if ($line eq $sourcelineLC)
            {                         
              print "$sourceline" if (!$filtrujeme);                         
              $linematched=1;
              last;             
            }                                 
          }    
        }
                         
        
      #printing line if   
      if (!$linematched)
        {
          print "$sourceline" if ($filtrujeme);  
        }   
    }

sub argum
  { 
    while ($args[0] =~ /^-(.+)/) 
    {       
    shift @args;
    $switch = $1;      
    
    if (($switch eq "help") || ($switch eq "h") || ($switch eq "?"))
      {
      help();
      exit;
      }
     
    if (($switch eq "line") || ($switch eq "l"))
      {
      $linefiltermode=1;
      next;      
      }
    
    
        
    if (($switch eq "find") || ($switch eq "f"))
      {
      $filtrujeme=0;
      next;
      }
      
    if ($switch eq "match")
      {
      $filtrujeme=0;
      next;
      }
    
      
    if ($switch eq "tokenshere")
      {
      $comlinetokens=1;
      next;
      }
      
    if ($switch eq "t")
      {
      $comlinetokens=1;
      next;
      }
    
      
      
    die "Unknown parameter -$switch";
    }
  }

#describe how this script is used
sub help 
  {
  print "
Filters text. Case insensitive. Source is taken line by line, if a line contains ALL tokens from AT LEAST ONE row of filter file, it is discarded (resp. printed in find mode). Rest of lines is printed (resp. discarded).

FILTER.PL [-help | -find | -line] source filter
FILTER.PL -tokenshere [..] source token1 token2 ' token3 \"token4 token5\" token6

  source            Sourcefile.
  filter            File with tokens (text used to filter_out/find lines).
  
  -find or -f       Lines from sourcefile that were matched are printed.
  -line or -l       Whole line in filter is taken as one token. Match if 
                    filter is the same as source line (not subset).
  -help or -h       Displays help.
  -tokenshere or -t Not using filter file. Takes command line parameters 
                    after source file as tokens for filtering. Use ' as
                    OR (same as newline in filter file).
                    
  Tokens (either in filter file or command line) are separated by spaces. Text between \" \" is considered to be one token.
  ";
  }

