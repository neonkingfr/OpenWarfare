$subnum=0;
$sorting=1;
$printcnt=0;
@args = @ARGV;
argum();

$fin = shift;

while ($fin =~ /^-(.+)/) {$fin = shift;} #

open(FIN, $fin) || die("Cannot open \"$fin\"\n");
while($line=<FIN>)
{
  if ($line=~/\n/) {} else {$line=$line."\n"} #adding \n to last line in file

  $original = $line if $subnum == 1;
  $line =~ s/(\d)+/|NUMBER|/g if $subnum == 1; #substitute numbers with MYTEMPNo
  $origline{$line}=$original if $subnum == 1; #origline field containing nonsubstituted lines

  if ($caseinsensitive) {$line = lc ($line);};

#  print "origline: $origline[$line] key: $line";
 # print "\n all @origline";
  $freq{$line}++; #@freq - frequencies of lines indexed by line

  if ($freq{$line}==1)
  {
    $li[$cnt]=$line; #@li - all unique lines, indexed by no. of row
    $cnt++;
  }
}


close(FIN);

#print "\n";print "\n";
#print @origline;


#print %freq;
#print "\n\n\nOrigline:\n";
#print %origline;
#print "\n\n\n";



if ($sorting) {@all =
  sort
  {
    $freq{$b}<=> $freq{$a}
  }keys %freq}
else {@all = @li}; #@li - array that has non duplicating lines - either normal or substituted ($subnum==1)

foreach $key (@all)
	{
  	print("$freq{$key}\t") if $printcnt;

    #if (($subnum==1) && ($printcnt == 0))
    if ($subnum==1)
    {
      print "$origline{$key}"; #we need original line
      #print " key: $key";
    }
    else {print("$key");}

  }




sub argum
  {
    while ($args[0] =~ /^-(.+)/)
    {
    shift @args;
    $switch = $1;

    if (($switch eq "help") || ($switch eq "h"))
      {
      help();
      exit;
      }

    if (($switch eq "cnt") || ($switch eq "c"))
      {
      $printcnt=1;
      next;
      }

    if (($switch eq "nosort") || ($switch eq "n"))
      {
      $sorting=0;
      next;
      }

    if (($switch eq "caseinsensitive") || ($switch eq "ci"))
      {
      $caseinsensitive=1;
      next;
      }

    if (($switch eq "substitutenumbers") || ($switch eq "subnum") || ($switch eq "sn") || ($switch eq "s"))
      {
      $subnum=1;
      next;
      }


    die "Unknown parameter -$switch";
    }
  }

  sub help
  {
  	print "sortfreq.pl [-help|-cnt|-nosort|-substitutenumbers|-caseinsensitive] infile";
  }