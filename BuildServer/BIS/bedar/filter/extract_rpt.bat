@echo off
SET ERRLVL = %ERRORLEVEL%
echo Extracting info from .rpt...




C:
cd \Documents and Settings\%username%\Local Settings\Application Data\ArmA 2\
md extract 2>nul
cd extract


perl c:\bis\bedar\filter\filter.pl -f ../ArmA2Int.RPT c:\bis\bedar\filter\trash.filter>trash.rpt
perl c:\bis\bedar\filter\filter.pl ../ArmA2Int.RPT c:\bis\bedar\filter\trash.filter>ArmA2IntI.rpt 2>nul
perl c:\bis\bedar\filter\filter.pl ../ArmA2Int_old.RPT c:\bis\bedar\filter\trash.filter>ArmA2IntI_old.rpt 2>nul


perl c:\bis\bedar\filter\filter.pl -f ArmA2IntI.rpt c:\bis\bedar\filter\error.filter>errors.rpt
perl c:\bis\bedar\filter\filter.pl -f ArmA2IntI.rpt c:\bis\bedar\filter\warning.filter>warnings.rpt

perl c:\bis\bedar\filter\filter.pl -line ArmA2IntI.rpt ArmA2IntI_old.RPT >problems_diff_temp.rpt
perl c:\bis\bedar\filter\filter.pl -line ArmA2IntI_old.RPT ArmA2IntI.rpt >problems_solved.rpt

REM Only unique lines to have in problems_diff.rpt...
perl c:\bis\bedar\filter\sortfreq.pl -sn -nosort problems_diff_temp.rpt>problems_diff.rpt
del problems_diff_temp.rpt

echo _____________________________________________________________________>>problems_new.history
date /T >> problems_new.history
type problems_diff.rpt >> problems_new.history

echo _____________________________________________________________________>>problems_solved.history
date /T >> problems_solved.history
type problems_solved.rpt >> problems_solved.history




REM Building stat.txt file

REM echo ----BEDAR Report2: Stats, Frequency of unique lines in .rpt ------->stat.txt

echo ----BEDAR2: ArmA2Int.rpt: Stats; Line freq ------------------------>stat.txt
echo --->>stat.txt


perl c:\bis\bedar\filter\sortfreq.pl -sn -cnt Arma2IntI.rpt>rptLineFrequency.rpt
perl c:\bis\bedar\filter\linecnt.pl rptLineFrequency.rpt>>stat.txt
echo  problems in report! >>stat.txt

echo --->>stat.txt

perl c:\bis\bedar\filter\sortfreq.pl -sn -cnt problems_diff.rpt >problems_new_freq.rpt
perl c:\bis\bedar\filter\linecnt.pl problems_new_freq.rpt>>stat.txt
echo  new problems in report. >>stat.txt

echo --->>stat.txt

perl c:\bis\bedar\filter\sortfreq.pl -sn -cnt errors.rpt>ErrorsFrequency.rpt
perl c:\bis\bedar\filter\linecnt.pl ErrorsFrequency.rpt>>stat.txt
echo  errors. >>stat.txt

perl c:\bis\bedar\filter\sortfreq.pl -sn -cnt warnings.rpt>WarningsFrequency.rpt
perl c:\bis\bedar\filter\linecnt.pl WarningsFrequency.rpt>>stat.txt
echo  warnings. >>stat.txt

perl c:\bis\bedar\filter\linecnt.pl ArmA2IntI.rpt >>stat.txt
echo  total significant lines in report (includes duplicite lines). >>stat.txt


echo _____________________________________________________________________>>stat.txt

echo Count (total) of problems (unique*) in .rpt - descending line freq: >>stat.txt
type rptLineFrequency.rpt>>stat.txt


md o:\arma 2>nul
md o:\arma\verhistory 2>nul
md o:\arma\verhistory\output 2>nul

perl c:\bis\bedar\filter\sortfreq.pl -sn -n problems_diff.rpt>o:\arma\verhistory\ArmA2Int_diff.rpt
type stat.txt> o:\arma\verhistory\output\bedar_stat.rpt

REM exitting while reseting errorlevel:
EXIT /B %ERRLVL%




