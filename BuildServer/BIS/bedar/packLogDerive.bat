@echo off
REM copies binarize log to according file on X:, derives and copies difference onto X:
REM
REM compares against binarize_old.log
REM addonname must be set as env variable

REM call c:\bis\bedar\setaddonname-temp.bat

pushd .

md p:\log 2>nul

C:
cd \temp\log

perl c:\bis\bedar\filter\filter.pl %addonname%.log c:\bis\bedar\filter\trash.filter > temp.log
perl c:\bis\bedar\filter\filter.pl %addonname%_old.log c:\bis\bedar\filter\trash.filter > temp_old.log

REM type %addonname%.log > temp.log
REM type %addonname%_old.log > temp_old.log


perl c:\bis\bedar\filter\sortfreq.pl -sn -nosort temp.log > temp2.log
perl c:\bis\bedar\filter\sortfreq.pl -sn -nosort temp_old.log > temp2_old.log


type temp2.log>p:\log\%addonname%.packlog
perl c:\bis\bedar\filter\filter.pl -l temp2.log temp2_old.log > p:\log\%addonname%_diff.packlog


popd