@args = @ARGV;
argum();



#$outputfile= "o:\\arma\\verHistory\\output\\bedar_fps.rpt";
#$inputfile= shift;
mkdir "o:\\arma\\verHistory\\output";



$source = shift;

#print "\nsource: $source";

while ($source =~ /^-(.+)/) {$source = shift;}

open(FIN,$source) || die("Error: Cannot open source file: \"$source\"\nUse: -help\nDied");
$output = shift;


$output = ">>".$output;
#print "\noutput: $output";

open (FOUT,$output) || die "Error: Cannot open output file: $output";

print FOUT "----BEDAR3: FPS Statistics ------------------------\n";

while($line=<FIN>)
{
	if ($line =~ /FPS-STAT (.*)\"/) 
	{
		print FOUT "$1\n";
	}
}










sub argum
  { 
    while ($args[0] =~ /^-(.+)/) 
    {       
    shift @args;
    $switch = $1;      
    
    if (($switch eq "help") || ($switch eq "h") || ($switch eq "?"))
      {
      help();
      exit;
      }
     
  
      
    die "Unknown parameter -$switch";
    }
  }

#describe how this script is used
sub help 
  {
  print "
Derives FPS log from arma debug.log. Use mission _TEST_FPS.Chernarus to write fps statistics to debug.log.
DERIVEFPS.PL [path]debug.log output
  ";
  }

