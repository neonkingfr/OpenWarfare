#old: aggregates diff files and report into output (used by Albunda and Albunda2)
#new:
# aggregates diff files from Albunda2\\o:\arma\verhistory and x:\verhistory\AB_daydiff_binarize_log(diff of binarize logs from Albunda)
#
#
#
use File::Copy;

$outputfile= "o:\\arma\\verHistoryF\\output\\buildInfo2.txt";
$linecntLimit = 1000; #how much lines to output?

mkdir "o:\\arma\\verHistoryF\\output";
$out = $outputfile;

$out = ">".$out; #write to file flag
open (FOUT,$out) || die "Error: Cannot open output file: $out.";

$machine = shift;
@timeData = localtime(time);
$year = $timeData[5]+1900;$month = $timeData[4]+1;$day = $timeData[3];$hour = $timeData[2];$min = $timeData[1];


#if ($machine eq "ALBUNDA")
#{
  #print (FOUT "---- obsolete ---------------------------------------------------------");                                                                          
# 
#} elsif ($machine eq "ALBUNDA2")
#{
  #print (FOUT "----BEDAR1: .rpt .packlog <= .history differences ----------------------");                                                                          
#} else
#{
  #print (FOUT "----BEDAR report from: $machine ----------------------------------------");                                                                          
#}

#print (FOUT "\n$day.$month.$year ");if ($hour<10) {print FOUT "0"};
#print (FOUT "$hour:");if ($min<10) {print FOUT "0";};print FOUT "$min";

$reportlines=0;
$packloglines=0;
$historylines=0;
synopsis(); #list of users, number of lines in report, in pack logs


#if ($reportlines+$packloglines==0)
#{
#  close(FOUT);
#  unlink($outputfile);
#  print "\nPerfect: No problems found in difference file ($outputfile) discarded.";
#  exit;
#} 


#aggregating all non-empty diff files into output




if ($reportlines>0)
{
	#print FOUT "\n\n=======================================================================";
  #print FOUT "\n=== PROBLEMS IN REPORT (total: $reportlines) =====================================";
  #@filestogo = <o:\\arma\\verhistoryF\\*_diff.rpt>;
  #processFiles();
}

if ($packloglines>0)
{
  #print FOUT "\n\n=======================================================================";
  #print FOUT "\n=== PROBLEMS IN PACK LOGS (total: $packloglines) =============================";
  #@filestogo = <o:\\arma\\verhistoryF\\*_diff.packlog>;
  #processFiles();
}
                
print FOUT "\n\n-----------------------------------------------------------------------";
print FOUT "\n-- CHANGES IN SS/SVN HISTORY (total: $historylines) ---------------------------";
@filestogo = <o:\\arma\\verhistoryF\\*_diff.history>;
processFiles();

print FOUT "\n\n\n\n";

exit;











sub processFiles
{
  foreach $file(@filestogo)
  {
    #print "\n$file";
    
    $filesize = -s $file;
    if ($filesize<10) {next;} #empty and very small diff files are not considered
    
    $file =~ /\\([^\\]+)_diff\.(.+)/; 
    print FOUT "\n____$1 (.$2)__________________________\n";
    
    open(FIN,$file) || next;
    
    $linecnt=0;
    $skip=0;
    while ($line=<FIN>)
    {
      #print "\n$line";
      if ($linecnt >= $linecntLimit) {$skip=1;}    
      print FOUT "$line" if $skip == 0;
      $linecnt++;   
    }
    
    $skippedlines=$linecnt - $linecntLimit;
    if ($skip==1) {print FOUT "---skipped: $skippedlines (unique) lines ---\n"};
  
    
    
    close(FIN);
    
  } 
}






sub synopsis
{#output users that changed (read from .history): USER (33 changes) in (ADDON1, ADDON2, ...)
 #store $reportlines, $packloglines linecounts for respective files

  
  #print FOUT "=== SS changers list ===";
  #@diffs = <o:\\arma\\verhistory\\*_diff.*>;
  
  #
  
  #push(@difffiles, <o:\\arma\\verhistory\\*_diff.rpt>);
  #push(@difffiles, <o:\\arma\\verhistory\\*_diff.history>);
  #push(@difffiles, <x:\\verhistory\\AB_daydiff_Binarize_log\\*_diff.binarizelog>);              
  #print @difffiless;
  
  @difffiles = <o:\\arma\\verhistoryF\\*_diff.*>;


  foreach $file(@difffiles)
  {#remove comments using trash-comment.filter from files which filename contains _diff (*.temp skipped)
    print "\nRemove comments: $file";
    $file =~ /(.+\.)([^\.]+)$/;    
    next if $2 =~ /temp/;
    $filetemp = $1 . "temp";
    copy($file,$filetemp);
    system("c:\\bis\\bedar\\filter\\filter.pl $filetemp c:\\bis\\bedar\\filter\\trash-comment.filter>$file");
    unlink($filetemp);
  }

  
  foreach $file(@difffiles)
  {
    
    $filesize = -s $file;
    if ($filesize<10) {next;} #empty and very small diff files are not considered
    
    $file =~ /\\([^\\]+)_diff\.(.+)/;         
    $addonname = $1;
    
    #print FOUT "\n=== $1 (.$2) ===\n";
    
    open(FIN,$file) || next;
    
    $linecnt=0;
    $skip=0;
    
    #print "\n$file";
    while ($line=<FIN>)
    {    
      
      #counting lines    
      if ($file =~ /.rpt/) {$reportlines++;next;} 
      if ($file =~ /.packlog/) {$packloglines++;next;} 
      if ($file=~ /.history/) {} else {next;}
    
      
      
      $historylines++;
      #file is of type .history, find username
      $line =~ /:\d\d ([^\s]+) /; #match username (after time: ":02 USER")      
      $user = lc($1);      
      $users{$user}++;
      
      if ($usersAddons{$user} =~ /$addonname/) {}
      else {$usersAddons{$user} .= $addonname.", ";} #add addonname if not already there
      #print FOUT "$1\n";
      $linecnt++;   
    }
        
    close(FIN);  
  } 
  
  
    
  @all = sort {lc($a) cmp lc($b)} keys %users;
  #print @all;
  
  
  #print FOUT "\n\nproblems in report: $reportlines";
  #print FOUT "\nproblems in pack logs: $packloglines";
   
  print FOUT "\n\nUsers changing:";
  foreach $key (@all)
	{
	  $usersAddons{$key} =~ s/, $//g; #destroy last ", " after addon names
	  print FOUT "\n\t$key \t($users{$key} changes) in ($usersAddons{$key})"
	}
 
  print FOUT "\n";
  
}








  