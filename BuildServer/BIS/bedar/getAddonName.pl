#Get addon name: reads addonSSform.txt - on first line:
#format $/slak/sgoij/soitj/addonname or https://mm.xdev.bistudio.com/svn/CA/Missions/ or $/neco/neco.p3d
#
#prints addonname
#prepares setaddonname.bat (!)

$fin = "addonSSform.txt";

open(FIN, $fin) || die ("Cannot open input file.");

$line = <FIN>;

$line =~ s/\n//g;

if ($line =~ /^\$(\/(\w)+)*\/(\w+)$/)
{
  print "REM created by getAddonName.pl (from: $line) - 1";
  $name=$3;
}
else
{
  #https://mm.xdev.bistudio.com/svn/CA/Missions/
  if ($line=~/([^\/]+)\/$/)
  {    
    print "REM created by getAddonName.pl (from: $line) - 2";
    $name=$1;
  }
  else
  {
    #https://mm.xdev.bistudio.com/svn/CA/Missions
    #$/neco/neco.p3d
    if ($line=~/([^\/]+)[\s]*$/)
    {
      print "REM created by getAddonName.pl (from: $line) - 3";
      $name =$1;
    }
    else
      {      
      $name="unknown";
      }
    
   }
}


$name = lc($name);


#print "$name";
#system("echo set addonname=$name 1>setaddonname-temp.bat");

#open (FOUT, "setaddonname-temp.bat") || die("Cannot create setaddonname-temp.bat");
#print FOUT "set addonname=$name";
#close FOUT;

print "\nset addonname=$name";

print "\nset storehistoryoutputdir=_output";

print "\\file" if $name =~ /\./; #if name has dot in it, it is stored in _output\file\ directory (see storeHistory.bat)

