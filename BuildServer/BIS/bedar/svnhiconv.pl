#  Conversion of SVN history 
#   by Vilem        
#
#   Input data created by: svn.exe log p:\ca\missions -v
#   Function: 
#      mode 0 (default, parameter -log): writes files + comments
#      mode 1 (parameter -patch): writes found patch names (eg. "Patch 1.043) + patch comments (Patch: comment\n comment\n comment)
#  
#  Usage: svnhiconv.pl o:\svn_log.txt [-log|-patch] 
#

$fin = shift;

while ($fin =~ /^-(.+)/) {$fin = shift;} #
open(FIN, $fin) || die("Cannot open \"$fin\"\n Usage:\n  svnhiconv.pl o:\\svn_log.txt [-log|-patch]\n");

$arg = shift;


$writeDiffLog = 1; if ($arg eq '-log') {$writeDiffLog = 1;}; #default
$writePatchLog = 0; if ($arg eq '-patch') {$writePatchLog = 1; $writeDiffLog = 0;}; #write only patch numbers and patch comments

$printSVNrevisionNumbers = 1; #display revision numbers in -patch changelog


#printMy "arg: $arg difflog: $writeDiffLog patchlog: $writePatchLog\n";

$part_comment = 0;
$part_files = 0;

while($line=<FIN>)
{ 
  
  #printMy "LINE ($part_files $part_comment): $line";
  
  if ($line =~ /^[\s]*\n/) {next;} #line containing just spaces...

  
  if ($line =~ /\s*r(\d+)[^|]*\| (\w+) \| ([\d]+)-([\d]+)-([\d]+) ([\d]+):([\d]+):([\d]+) [^|]*\|/)
  {#found line of a type: r274 | joris | 2007-08-16 14:08:59 +0200 (_t, 16 VIII 2007) | 1 line
    $revision = $1; $user = $2; $year = $3; $month=$4; $day = $5; $hour = $6; $min=$7;         
    #print "\n reading... $day.$month.$year $hour:$min $user $revision\n";    
    #printMy "\n$day.$month.$year $hour:$min $user $revision";    
  }
  
  if ($line =~ /\s+([\w]+) (\/.*)/)
  {#found line with spaces, word char(s), space and / (<- part of path)
    if ($writeDiffLog) {printMy ("\n$revision $day.$month.$year $hour:$min $user $1 $2");};
    $part_files = 1;
    
  }
  else
  {
    #printMy "\nCHECK ($part_files $part_comment)";
    if ($part_files)
    {#after files, we have comment
      $part_files = 0;
      $part_comment = 1;
      
      #printMy "\nCHECK - 1 ($part_files $part_comment)";
    };    
  };
   
  
  #printMy "LINE2 ($part_files $part_comment): $line";
    
  if ($line =~/^-+$/)
  {#entry ending line detected (-------------------------)
    
    if (@comment > 1) 
    {
      if ($writeDiffLog) {printMy ("\n\nComment: \n@comment");};
    };
    
    
    
    #if ($printSVNrevisionNumbers) {unshift(@commentPatch, "[$revision - $user]");}; unshift(@commentPatch, "\n  ");
    
    if ($part_commentPatch)
    {      
      if ($writeDiffLog) {
        printMy ("\n\nPatch Comment: \n@commentPatch");        
        #print " Comment-patch-print @commentPatch";   
        };
      if ($writePatchLog) 
      {
        printMy ("@commentPatch");     
        #print " Comment-patch-print @commentPatch";   
      };
    };
        
    printMy ($commentPatchName);
    $commentPatchName = '';
    
    $part_comment = 0;
    $part_commentPatch = 0;
    
    @comment = "";
    
    @commentPatch = "";
    #if ($printSVNrevisionNumbers) {push(@commentPatch, "[$revision]");} 
    
    
    
    $commentPatchName = '';
    
    if ($writeDiffLog) {printMy ("\n------------------------------------------------------------------------");};
  }
  else
  {
    if ($part_comment) 
    {
      if ($line =~/(.*)Patch:(.*)/) 
      {        
        if ($part_commentPatch) {} else {push(@comment, $1);}; #take part before Patch: as normal comment (if no Patch: before)        
        $part_commentPatch = 1; #1 == true
        
        $part_afterPatchWithoutWhiteBeg = $2;
        $part_afterPatchWithoutWhiteBeg =~ /\s*(.*)/;
        $part_afterPatchWithoutWhiteBeg = $1;
        #push(@commentPatch, " ".$part_afterPatchWithoutWhiteBeg);    
        
        push(@commentPatch, "\n  ");            
        if ($printSVNrevisionNumbers) {push(@commentPatch, "[$revision]");};
        
        push(@commentPatch, " ".$part_afterPatchWithoutWhiteBeg);        
      }
      else
      {
        if ($part_commentPatch) 
        {
          $lineWithoutPatchName = $line;
          $lineWithoutPatchName =~ s/(Patch \d+\.\d+)//gi;
          chomp($lineWithoutPatchName);
          if ($lineWithoutPatchName !~ m/^\s*$/) {push(@commentPatch, " ".$lineWithoutPatchName);}; #line is not empty, lets include in patch comment
        } 
        else 
        {push (@comment, $line);};
      };
      
      if ($line =~/(Patch \d+\.\d+)/) 
      {
        if ($writePatchLog) {$commentPatchName = "\n$1";};
      };
      
      
    };
  };
  
} 


if ($writePatchLog) {printMy ("\n(current patch candidate)");};

print (@outPut);


sub printMy
{#patch log printed in reversed order
  $text = shift;    
  #print $text;
  
  if ($text !~/^\s*$/)
  {      
    if ($writeDiffLog) {push (@outPut, $text);}
    if ($writePatchLog) {unshift(@outPut, $text)}; #reversed order  
  };
};

