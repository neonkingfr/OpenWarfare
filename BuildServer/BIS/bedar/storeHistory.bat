@echo off
REM echo Getting history from SS...
REM \\core1\vssdatap\cfg
REM \\new_server\vss\pgm
REM uses autocreated setaddonname-temp.bat to get outputdir and addonname
REM
REM
REM Examples:
REM     * call c:\bis\bedar\storeHistory.bat \\New_server\vss\Pgm\srcsafe.ini $/Poseidon
REM     * call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/Poseidon -svn
REM
REM

pushd .

md c:\bis\bedar\_output 2>nul
md c:\bis\bedar\_output\file 2>nul

c:
cd \bis\bedar


echo Storing history (%1 %2 7-days %3)...


if [%1] == [-svn] echo   1st parameter: -svn, we need SS database/empty, exitting.
if [%2] == [-svn] echo   2nd parameter: -svn, we need SS/SVN project, exitting.

if [%1] == [-svn] goto end
if [%2] == [-svn] goto end


echo %2>addonSSform.txt

REM getaddonname.pl creates setaddonname-temp.bat
perl getaddonname.pl>setaddonname-temp.bat
call setaddonname-temp.bat

if errorlevel 1 echo Error: Empty addon name.
if errorlevel 1 goto end



REM (albunda) make binarize.log difference and send to x:\verHistory\AB_difference_log\
REM ...uses %addonname% set in setaddonname-temp.bat
REM if [%computername%] == [albunda]
REM call ab_binarizelog_to_old.bat 2>nul



if [%3] == [-svn] goto svn
call sshi.bat %1 %2 "7" %3> %storehistoryoutputdir%\%addonname%.history
goto end


:svn
REM svn log -r {2006-11-20}:{2006-11-29}
call svnhi.bat %1 %2 "7"> %storehistoryoutputdir%\%addonname%.history


:end
popd