if /I [%1] == [EXP] goto :Expansion
if /I [%1] == [Expansion] goto :Expansion
if /I [%1] == [TKOH] goto :TKOH

echo Sending to NG...
call c:\bis\blat\toNewsFile.bat "BEDAR Differences" o:\arma\verHistory\output\bedar_corr.rpt bistudio.fp.ca.bedar
call c:\bis\blat\toNewsFile.bat "BEDAR Stats; LineFreq" o:\arma\verHistory\output\bedar_stat.rpt bistudio.fp.ca.bedar
call c:\bis\blat\toNewsFile.bat "BEDAR FPS" o:\arma\verHistory\output\bedar_fps.rpt bistudio.fp.ca.bedar

call c:\bis\blat\toNewsFile.bat "BEDAR FPS PERFTEST" o:\arma\verHistory\output\bedar_fpsPerftest.rpt bistudio.fp.ca.bedar o:\arma\verHistory\output\bedar_slowFrames.rpt

goto :end

:Expansion
echo Sending to NG...
REM call c:\bis\blat\toNewsFile.bat "BEDAR Differences" O:\Arma2_Expansion\verHistory\output\bedar_corr.rpt bistudio.fp.ca.expansion.bedar
REM call c:\bis\blat\toNewsFile.bat "BEDAR Stats; LineFreq" O:\Arma2_Expansion\verHistory\output\bedar_stat.rpt bistudio.fp.ca.expansion.bedar
REM call c:\bis\blat\toNewsFile.bat "BEDAR FPS" O:\Arma2_Expansion\verHistory\output\bedar_fps.rpt bistudio.fp.ca.expansion.bedar

call c:\bis\blat\toNewsFile.bat "BEDAR FPS PERFTEST" O:\Arma2_Expansion\verHistory\output\bedar_fpsPerftest.rpt bistudio.fp.ca.expansion.bedar

goto :end 

:TKOH
echo Sending to NG...
call c:\bis\blat\toNewsFile.bat "BEDAR FPS PERFTEST" O:\TakeOnH\verHistory\output\bedar_fpsPerftest.rpt bistudio.fp.helisim.bedar

goto :end 

:end