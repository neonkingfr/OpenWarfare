#taggedToFiles.pl
#Seeks for tags, creates files. Takes source file line by line. If tag (<model = \"...\">, <world...>) is encountered, lines between this tags are stored in file with name derived from the name of the model/world.
#parses argument: \ca\temp\log\signs.log into addonname
#stores output files to current dir 

#warning: in ultraedit call to storeHistory.bat not working

#print "\nExecuted taggedToFiles.pl @ARGV...";

$ssdb = "\\\\core1\\vssdatap\\cfg";
$ssprojdir = "\$\/ca";


@args = @ARGV;
argum();

#the first item on the command line is source
$source = shift;
while ($source =~ /^-(.+)/) {$source = shift;} #
open(FSOURCE,$source) || die("Error: Cannot open source file: \"$source\"\nUse: -help\nDied");


#if ($source =~ /^.+\\(.+)$/) {print "$1";}

if ($source =~ /^.+\\(.+)$/) {$filename = $1;}
if ($source =~ /^.+\/(.+)$/) {$filename = $1;}

if ($filename) {if ($filename =~ /([^\.]+)\.*/) {$addonname = $1;}}
  
$header = "";
$linecnt=-1;
$write=0;
while ($line=<FSOURCE>)
{  
  $linecnt++;
  
  if ($linecnt==0)
  {
    if ($line =~ /\/\/\/\/.+\..+\..+\:.+\:.+/)
    {
      $header = $line;
    }    
  }
  
  
  
  if (
  ($line =~ /<model = \".*\\([^\\]+)\">/i)
  ||
  ($line =~ /<world = \".*\\([^\\]+)\">/i)
  )  
  {         
    close FOUT; #no closing tag, but opening tag found
    #$1 obsahuje jm�no souboru
    $taggedfilename = $1;
    
    next if $line =~ /road_/;
    
    #$out = ">>".$taggedfilename.".log";    
    $out = ">".$taggedfilename.".log";
    $out = lc($out);
    open (FOUT,$out) || die "Error: Cannot open output file: $taggedfilename.";
    print FOUT $header;
    printf FOUT "//// addon log: $source";
    printf FOUT "\n////\n";
    
    #system "echo: $taggedfilename";    
    $line =~ /p:(.+)\"/i; #ssproj   
    $ssprojfile = "\$".$1;    
    
    $ssprojfile =~ s/\\/\//g; #substitution \ -> /
    #print $ssprojfile;
    
    #print "\n taggedToFiles.pl: system \"c:\\bis\\bedar\\storeHistory.bat $ssdb $ssprojfile\" \n";
    
    system "c:\\bis\\bedar\\storeHistory.bat $ssdb $ssprojfile";
        
    $write=1;
    next;
  }
  
  if ($line =~ /<\/model>/) {$write=0;close FOUT};
  if ($line =~ /<\/world>/) {$write=0;close FOUT};
  
  print FOUT "$line" if ($write==1);  
  #print $line;
}








sub argum
  {    
    while ($args[0] =~ /^-(.+)/) 
    {       
    shift @args;
    $switch = $1;      
    
    if (($switch eq "help") || ($switch eq "h") || ($switch eq "?"))
      {
      help();
      exit;
      }
            
    die "Unknown parameter -$switch";
    }
  }

#describe how this script is used
sub help 
  {
  print "
Seeks for tags, creates files. Takes source file line by line. If tag (<model = \"...\">, <world...>) is encountered, lines between this tags are stored in file with name derived from the name of the model/world.


TAGGEDTOFILES.PL [-help] source 

  source            Sourcefile.
  
  -help or -h       Displays help.
  ";
  }

#print "\ntaggedToFiles.pl end.";