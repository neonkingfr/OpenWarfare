@echo off
REM Executed after addon packing on Albunda

set addonname=%1

echo Executed ab_postpack.bat %1 %2 %3 %4 %5 %6 %7 %8 %9

echo Deleting... (del /F /Q p:\log\filelog\*.*; p:\log\filelog\noduplicity\*.*; C:\bis\bedar\_output\file\*.*)
del /F /Q p:\log\filelog\*.*
del /F /Q p:\log\filelog\noduplicity\*.*
del /F /Q C:\bis\bedar\_output\file\*.*

echo c:\bis\bedar\packLogDerive.bat...
call c:\bis\bedar\packLogDerive.bat

echo c:\bis\bedar\derivefilelogs.bat...
call c:\bis\bedar\derivefilelogs.bat


REM ECHO Test log here...

echo REM c:\bis\bedar\sendHistory.bat...
call c:\bis\bedar\sendHistory.bat

echo c:\bis\bedar\ab_sendPackLogs.bat
call c:\bis\bedar\ab_sendPackLogs.bat

echo ab_postpack.bat finished.
