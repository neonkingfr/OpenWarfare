$fin='w:\c\Poseidon\lib\versionNo.h';
open(FIN, $fin) || die("ERROR: Cannot open \"$fin\".\n");

while (<FIN>)
{#read file, remove \n, store into $file
  #print $_
  $line = $_;
  chomp ($line);  
  $file = $file.$line;  
};

$file =~ /ArmA version((?!APP_VERSION_TEXT).)*APP_VERSION_TEXT[^"]*"([^"]*)"/i;  #find "Arma version string & next APP_VERSION_TEXT
print $2;