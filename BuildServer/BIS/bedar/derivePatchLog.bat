@echo off
REM
REM directory outputFile revision_start revision_to
REM


pushd .

set outPutFile="c:\bis\bedar\derivePatchLog_logPatch.txt"
set revision_start=0
set revision_to=HEAD

if not [%2] == [] set outPutFile=%2
if [%2] == [] echo ERROR: derivePatchLog.bat - no output file! Usage: derivePatchLog.bat directory outputfile [revision_start [revision_to]]
if not [%3]==[] set revision_start=%3
if not [%4]==[] set revision_to=%4

cd /d %1

svn.exe log -v -r %revision_start%:%revision_to% > c:\bis\bedar\derivePatchLog_log.txt
perl c:\bis\bedar\svnhiconv.pl c:\bis\bedar\derivePatchLog_log.txt -patch > %outPutFile%

REM TEST-COMMENT Poseidion- interesting revisions: 58000 to HEAD

popd