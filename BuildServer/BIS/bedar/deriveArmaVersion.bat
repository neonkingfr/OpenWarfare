﻿
pushd .

set revision=HEAD
if not [%1]==[] set revision=%1

echo Getting Arma 2 version from w:\c\Poseidon\lib\versionNo.h (SVN revision: %revision%).

cd /d w:\c\Poseidon\lib
svn up versionNo.h -r %revision%
call perl c:\bis\bedar\deriveArmaVersion.pl

popd