@args = @ARGV;
argum();

mkdir "o:\\arma\\verHistory\\output";

$source = shift;
while ($source =~ /^-(.+)/) {$source = shift;}
open(FIN,$source) || die("Error: Cannot open source file: \"$source\"\nUse: -help\nDied");

$output = shift;
$output = ">".$output;
open (FOUT,$output) || die "Error: Cannot open output file: $output";

$inside = 0;

while ($line=<FIN>)
{
	if ($line =~ /\<AutoTestInfo (.*)\>/) 
	{
		$inside = 1;		
	}
	if ($inside == 1)
	{
		print FOUT "$line";
	}
	if ($line =~ /\<\/AutoTestInfo\>/) 
	{
		$inside = 0;		
	}
}


sub argum
  { 
    while ($args[0] =~ /^-(.+)/) 
    {       
    shift @args;
    $switch = $1;      
    
    if (($switch eq "help") || ($switch eq "h") || ($switch eq "?"))
      {
      help();
      exit;
      }
     
  
      
    die "Unknown parameter -$switch";
    }
  }

#describe how this script is used
sub help 
  {
  print "
Derives slow frames report from arma debug.log. Use mission _TEST_FPS.Chernarus to write fps statistics to debug.log.
DERIVESLOWFRAMES.PL [path]debug.log output
  ";
  }

