@echo off
REM Uploads executables and other built engine files to mm.xdev.bistudio.com
REM from: o:\arma\ (exe, bin.pbo) <- build server Albunda2
REM
REM usage: upload-mm_xdev_bistudio_Com.bat
REM   
REM by Vilem

pushd . 

set sourceProduct=ArmA2Int
REM set sourceProduct2=ArmA2_Server

set sourceDir=o:\Arma
set source=%sourceDir%\%sourceProduct%
REM set source2=%sourceDir%\%sourceProduct2%

set ftpUpload=c:\bin\curl\curl.exe -f -k -u packbot:stupidbot -T
set ftpTarget=https://mm.xdev.bistudio.com/files/a2/


rem Deploy protected files to the FTP
echo Deploying to FTP server
%ftpUpload% %source%.map %ftpTarget%/
%ftpUpload% %source%.exe %ftpTarget%/
%ftpUpload% %source%.exe.cfg %ftpTarget%/

%ftpUpload% %sourceDir%\ijl15.dll %ftpTarget%/
%ftpUpload% %sourceDir%\IFC22.dll %ftpTarget%/


REM SecuROM files
%ftpUpload% %sourceDir%\lang.ini     %ftpTarget%/
%ftpUpload% %sourceDir%\paul.dll     %ftpTarget%/
%ftpUpload% %sourceDir%\logo-paul.bmp     %ftpTarget%/
%ftpUpload% %sourceDir%\unicows.dll    %ftpTarget%/


REM %ftpUpload% %sourceDir%\xlive.dll.cat %ftpTarget%/
REM %ftpUpload% %sourceDir%\xlivefnt.dll %ftpTarget%/
REM %ftpUpload% %sourceDir%\xlive.dll %ftpTarget%/

REM %ftpUpload% %source2%.exe %ftpTarget%/

popd