# 
# getRevisionNumber.pl
#
# Gets SVN revision nuber from SVN info file
#  by Vilem
#

# Note: in current build pipeline (https://dev.bistudio.com/svn/pgm/trunk/BuildServer/BIS/Build)
#       this file c:\bis\Build\svninfoProg_uncensored.txt contains SVN info

@args = @ARGV;
argum();
$fin = shift;
while ($fin =~ /^-(.+)/) {$fin = shift;} #
open(FIN, $fin) || die("Cannot open \"$fin\"\n");

@lines = <FIN>;

#print @lines;

for (@lines)
{
  #print $_;
  
  #taking this line: "Revision: 58028", printing SVN revision
  if ($_ =~/Revision:\s*(\d*)/) {print $1};
};







sub argum
  {    
    while ($args[0] =~ /^-(.+)/) 
    {       
    shift @args;
    $switch = $1;      
    
    if ($switch eq "help") 
      {
      help();
      exit;
      }    
        
    if ($switch eq "cnt")
      {
      $printcnt=1;
      next;
      }
      
    die "Unknown parameter -$switch";
    }
  }
  
  sub help
  {
  }