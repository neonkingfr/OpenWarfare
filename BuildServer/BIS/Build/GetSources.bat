if not [%1]==[] set REVISION=%1

@echo off
REM TODO: next line weird (should get highest revision number?)
FOR /F "tokens=2 skip=4" %%G IN ('svn info -r HEAD https://dev.bistudio.com/svn/pgm') DO IF NOT DEFINED REVISION SET REVISION=%%G

SET /A REVISION_HI=%REVISION%/1000
SET /A REVISION_LO=((%REVISION%)%%1000)

echo Getting sources... (rev. %REVISION% - storing to SVNrevisionWanted env. var.)
@ECHO #define BUILD_NO %REVISION% > W:\c\buildNo.h
REM @ECHO #define BUILD_NO_HI %REVISION_HI% >> W:\c\buildNo.h
REM @ECHO #define BUILD_NO_LO %REVISION_LO% >> W:\c\buildNo.h

@ECHO #define BUILD_NO %REVISION% > w:\C_branch\Poseidon\buildNo.h
REM @ECHO #define BUILD_NO_HI %REVISION_HI% >> W:\C_branch\Poseidon\buildNo.h
REM @ECHO #define BUILD_NO_LO %REVISION_LO% >> W:\C_branch\Poseidon\buildNo.h


set SVNrevisionWanted=%REVISION%

Echo Updating w:\c from SVN...
svn update --non-interactive -r %REVISION% w:\c
if not [%errorlevel%] == [0] exit /B %errorlevel% 

Echo Updating w:\c_branch from SVN...
svn update --non-interactive -r %REVISION% w:\c_branch
if not [%errorlevel%] == [0] exit /B %errorlevel% 