@echo off
echo Reporttoold... (arma2int.rpt, arma2.rpt, arma2intUnprotected.rpt)
pushd .
c:
cd c:\documents and settings\packbot\local settings\application data\arma 2\

type arma2int_old.rpt>>reports.rpt
type arma2int.rpt>arma2int_old.rpt
del arma2int.rpt

type arma2SR_old.rpt>>reportsSR.rpt
type arma2.rpt>arma2SR_old.rpt
del arma2.rpt

type arma2intUnprotected_old.rpt>>reportsUnprotected.rpt
type arma2intUnprotected.rpt>arma2intUnprotected_old.rpt
del arma2intUnprotected.rpt

popd