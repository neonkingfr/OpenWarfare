@echo off
set user=%1
set dateBuild=%date%
set timeBuild=%time%

echo ----------------------------------------------------------------------
echo -- TKOH FPS measurement ----------------------------------
echo ----------------------------------------%dateBuild% %timeBuild%---
echo initiated by: %user%


REM call W:\C_Branch\Poseidon\Arrowhead\lib\Tools\ArmA\autotestFPSveh.bat
REM exit /B

call W:\C_Branch\Poseidon\HeliSim\lib\Tools\ArmA\autotestFPSpc.bat
copy /Y /B O:\TakeOnH\verhistory\output\bedar_fpsPerftest.rpt x:\verHistory\AB2_fps\bedar_fpsPerftest_TKOH.rpt

c:\bis\kecal\kecal.exe %user% (FPS measurement %dateBuild% %timeBuild% by %user%) TKOH FPS measurement complete. file:///X:/verHistory/AB2_fps/bedar_fpsPerftest_TKOH.rpt
call c:\bis\bedar\sendNG.bat TKOH