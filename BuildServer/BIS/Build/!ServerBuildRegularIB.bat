@echo off
echo ====================================================================
echo == REGULAR BUILD (!ServerBuildArmaRegularIB.bat) ===================
echo ====================================================================

call c:\bis\Build\dbg.bat ====================================================================2>nul
call c:\bis\Build\dbg.bat == REGULAR BUILD (!ServerBuildArmaRegularIB.bat) ===================2>nul
call c:\bis\Build\dbg.bat === changed back to: 10 s wait between remote client execution======2>nul
call c:\bis\Build\dbg.bat ====================================================================2>nul

REM moving contents of Arma2Int.rpt to Arma2Int_old.rpt
call c:\bis\Build\dbg.bat %0 call x:\update\DtaArmaUpd.bat 2>nul
call x:\update\DtaArmaUpd.bat

REM Build X360 PGInstrument - PREREGULAR (creates .pgd file that is used on another machine that then calls regular build with profiled data)

REM XBOX start earlier 2.10 to prepaire PBOInstrument files
REM call c:\bis\Build\dbg.bat %0 B:\RemEx\ExeCln.exe Albunda2 BuildArmaX360IB %computername% PGInstrument
REM B:\RemEx\ExeCln.exe Albunda2 BuildArmaX360IB NOREPLY PREREGULAR PGInstrument

rem Simulation of "Sleep" command via ping - wait 10 seconds to make sure the binarize will be first
@ping 127.0.0.1 -n 10 -w 1000 > nul


REM Regular build (+ autotest) - will be launched after profiling on XBOX from Builder
REM   call c:\bis\Build\dbg.bat %0 B:\RemEx\ExeCln.exe Albunda2 BuildArmaX360IB %computername%
REM   B:\RemEx\ExeCln.exe Albunda2 BuildArmaX360IB NOREPLY regular

call c:\bis\Build\dbg.bat %0 B:\RemEx\ExeCln.exe Albunda2 BuildBinarize NOREPLY regular 2>nul
B:\RemEx\ExeCln.exe Albunda2 BuildBinarize NOREPLY regular


rem Simulation of "Sleep" command via ping - wait 10 seconds to make sure the binarize will be first
@ping 127.0.0.1 -n 10 -w 1000 > nul

call c:\bis\Build\dbg.bat %0 B:\RemEx\ExeCln.exe Albunda2 BuildArmaIB NOREPLY regular 2>nul
B:\RemEx\ExeCln.exe Albunda2 BuildArmaIB NOREPLY regular


REM report to old need to be here - after BEDAR reporting (for BEDAR-differences to handle correctly not-regular build reports)
call c:\bis\Build\reporttoold.bat


rem Simulation of "Sleep" command via ping - wait 10 seconds to make sure the binarize will be first
@ping 127.0.0.1 -n 10 -w 1000 > nul

call c:\bis\Build\dbg.bat %0 B:\RemEx\ExeCln.exe Albunda2 BuildArmaIB NOREPLY noregular SuperRelease 2>nul
::B:\RemEx\ExeCln.exe Albunda2 BuildArmaIB NOREPLY noregular SuperRelease

B:\RemEx\ExeCln.exe Albunda2 BuildArmaRegularIB_OA NOREPLY regular
B:\RemEx\ExeCln.exe Albunda2 BuildArmaRegularIB_HeliSim NOREPLY regular
