REM TODO: parametrize (or delete) history version for expansion
@echo off

if [%1] == [] goto unknownUser
set user=%1
if /I [%1] == [bxbx] set user=Kuba
goto knownUser
:unknownUser
set user=UNKNOWN
:knownUser



REM %1 is for name of computer
REM Parameter regular must be in %2
REM Configuration type must be in %3
REM Wanted configuration (see contents of .sln) must be in %4

echo Executed: %0 %1 %2 %3 %4 %5 %6
echo %date% %time% %0 %1 %2 %3 %4 %5 %6 >> c:\bis\build\buildLog.txt

REM Note: using "Release|Win32" as configuration to build shader generating version (Arma2IntUnprotected.exe / Arma2ExpIntUnprotected.exe)

REM Default settings
set errorlvl=0
set protection=SECUROM
set autotest=1
set mapOrderProtection=none
set mapOrderProtectionResult=" Success"
set buildType=NotRegular
set shaderExe=Arma2Int.exe
set buildXoreaxSwitch=BUILD
set buildXoreaxClean=
set BuildAllocators=0

if /I [%2] == [regular] set buildType=Regular
if /I [%2] == [regular] set buildXoreaxSwitch=REBUILD


REM SVNrevisionWanted ... revision that was wanted to be updated from SVN (used for w:\c and w:\c_branch - both are in the same SVN; TODO: parametrize; see c:\Bis\build\GetSources.bat for setting this parameter)
set SVNrevisionWanted=0

REM w:\C_branch\Poseidon\Arrowhead\ArmA2Ext.sln
REM w:\c\Poseidon\ArmA2.sln

set sourceSVN=w:\c\Poseidon\
set solution=w:\c\Poseidon\ArmA2.sln
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2

REM Build configurations...
if /I [%3] == [] goto :default
if /I [%3] == [Default] goto :default
if /I [%3] == [Quick] goto :Quick
if /I [%3] == [TestApp] goto :TestApp
if /I [%3] == [Preview] goto :Preview
if /I [%3] == [Retail] goto :Retail
if /I [%3] == [Demo] goto :Demo
if /I [%3] == [A2Free] goto :A2Free
if /I [%3] == [SuperRelease] goto :SuperRelease
if /I [%3] == [Steam] goto :Steam
if /I [%3] == [Server] goto :Server
if /I [%3] == [Expansion] goto :Expansion
if /I [%3] == [ExpansionRetail] goto :ExpansionRetail
if /I [%3] == [ExpansionRetailReview] goto :ExpansionRetailReview
if /I [%3] == [ExpansionSteam] goto :ExpansionSteam
if /I [%3] == [ExpansionRFT] goto :ExpansionRFT
if /I [%3] == [ExpansionDemo] goto :ExpansionDemo
if /I [%3] == [ExpansionServer] goto :ExpansionServer

if /I [%3] == [HeliSim] goto :TakeOnH
if /I [%3] == [TakeOnH] goto :TakeOnH
if /I [%3] == [HeliSimRetail] goto :TakeOnHRetail
if /I [%3] == [TakeOnHRetail] goto :TakeOnHRetail
if /I [%3] == [TakeOnHSteam]  goto :TakeOnHSteam
if /I [%3] == [TakeOnHServer]  goto :TakeOnHServer
if /I [%3] == [TakeOnHPublicBeta]  goto :TakeOnHPublicBeta
if /I [%3] == [TakeOnHPublicBetaRFT]  goto :TakeOnHPublicBetaRFT
if /I [%3] == [TakeOnHDemo]  goto :TakeOnHDemo
if /I [%3] == [TakeOnHDemoSteam]  goto :TakeOnHDemoSteam

if /I [%3] == [Liberation]  goto :Liberation




ECHO unknown build rule %3
c:\bis\kecal\kecal.exe %user% unknown build rule %3

exit /B

:default
REM only this build version can generate shaders!
set buildVer=DefaultVersion
set buildCfg="Release|Win32"
set buildDir=o:\arma\
set outputDir=X:\fp
set outputExe=ArmA2Int
set Binarize=0
goto :start

:Quick
REM Like default, but skip protection and autotest
set buildVer=DefaultVersion
set buildCfg="Release|Win32"
set buildDir=o:\arma\
set outputDir=X:\fp
set outputExe=ArmA2Int
set Binarize=0
set protection=none
set autotest=0
goto :start

:Expansion
set solution=w:\C_branch\Poseidon\Arrowhead\ArmA2Ext.sln
set sourceSVN=w:\C_branch\Poseidon\Arrowhead\
set buildVer=Expansion
set buildCfg="Release|Win32"
set buildDir=o:\Arma2_Expansion\
set outputDir=X:\Arma2_Expansion
set outputExe=ArmA2ExpInt
set shaderExe=Arma2ExpInt.exe
set autotest=1
set Binarize=EXP
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA
set BuildAllocators=1
goto :start

:ExpansionRetail
set solution=w:\C_branch\Poseidon\Arrowhead\ArmA2Ext.sln
set sourceSVN=w:\C_branch\Poseidon\Arrowhead\
set buildVer=ExpansionRetail
set buildCfg="Retail|Win32"
set buildDir=o:\Arma2_Expansion\
set outputDir=X:\ARMA2Public\Expansion\ARMA2ExtFull
set outputExe=ArmA2Exp
set shaderExe=Arma2ExpInt.exe
set autotest=none
set buildXoreaxSwitch=REBUILD
set Binarize=EXP
set mapOrderProtection=1
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA
set BuildAllocators=1
goto :start

:ExpansionRetailReview
set solution=w:\C_branch\Poseidon\Arrowhead\ArmA2Ext.sln
set sourceSVN=w:\C_branch\Poseidon\Arrowhead\
set buildVer=ExpansionRetailReview
set buildCfg="Retail|Win32"
set buildDir=o:\Arma2_Expansion\
set outputDir=X:\ARMA2Public\Expansion\ARMA2ExtFull-Review
set outputExe=ArmA2Exp
set shaderExe=Arma2ExpInt.exe
set autotest=none
set buildXoreaxSwitch=REBUILD
set Binarize=EXP
set mapOrderProtection=1
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA
set BuildAllocators=1
goto :start

:ExpansionServer
set solution=w:\C_branch\Poseidon\Arrowhead\ArmA2Ext.sln
set sourceSVN=w:\C_branch\Poseidon\Arrowhead\
set buildDir=o:\Arma2_Expansion\
set outputDir=X:\ARMA2Public\Expansion\ARMA2ExtServer
set outputExe=ArmA2ExpServer
set buildVer=ExpansionServer
set buildCfg="Retail Server|Win32"
set mapOrderProtection=none
set protection=none
set autotest=none
set buildXoreaxSwitch=REBUILD
set shaderExe=Arma2ExpInt.exe
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA
set BuildAllocators=1
goto :start

:ExpansionSteam
set solution=w:\C_branch\Poseidon\Arrowhead\ArmA2Ext.sln
set sourceSVN=w:\C_branch\Poseidon\Arrowhead\
set buildVer=ExpansionSteam
set buildCfg="Retail Steam|Win32"
set buildDir=o:\Arma2_Expansion\
set outputDir=X:\ARMA2Public\Expansion\ARMA2ExtSteam
set outputExe=ArmA2ExpSteam
set shaderExe=Arma2ExpInt.exe
set mapOrderProtection=1
set protection=none
set buildXoreaxSwitch=REBUILD
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA
set BuildAllocators=1
goto :start

:ExpansionRFT
set solution=w:\C_branch\Poseidon\Arrowhead\ArmA2Ext.sln
set sourceSVN=w:\C_branch\Poseidon\Arrowhead\
set buildVer=ExpansionRFT
set buildCfg="Retail REINFORCEMENTS|Win32"
set buildDir=o:\Arma2_Expansion\
set outputDir=X:\ARMA2Public\Expansion\ARMA2ExtREINFORCEMENTS
set outputExe=ArmA2ExpRFT
set shaderExe=Arma2ExpInt.exe
set mapOrderProtection=1
set protection=none
set autotest=none
set buildXoreaxSwitch=REBUILD
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA
set BuildAllocators=1
goto :start

:ExpansionDEMO
set solution=w:\C_branch\Poseidon\Arrowhead\ArmA2Ext.sln
set sourceSVN=w:\C_branch\Poseidon\Arrowhead\
set buildVer=ExpansionDemo
set buildCfg="Retail Demo|Win32"
set buildDir=o:\Arma2_Expansion\
set outputDir=X:\ARMA2Public\Expansion\ARMA2ExtDemo
set outputExe=ArmA2ExpDemo
set shaderExe=Arma2ExpInt.exe
set mapOrderProtection=none
set protection=none
set autotest=none
set buildXoreaxSwitch=REBUILD
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA
set BuildAllocators=1
goto :start

:TestApp
REM obsolete, now demo
set buildVer=TestApp
set buildCfg="Retail demo|Win32"
set buildDir=o:\ArmADemo\
set outputDir=X:\ARMA2Public\ARMA2TestApp
set outputExe=ArmA2Demo
set autotest=none
goto :start

:Demo
set buildVer=Demo
set buildCfg="Retail demo|Win32"
set buildDir=o:\arma\
set outputDir=X:\ARMA2Public\ARMA2Demo
set outputExe=ArmA2Demo
set protection=none
set autotest=none
goto :start

:A2Free
set buildVer=Demo
set buildCfg="Retail free|Win32"
set buildDir=o:\arma2_free\
set outputDir=X:\ARMA2Public\ARMA2free
set shaderExe=ARMA2free.exe
set outputExe=ArmA2Free
set protection=none
set autotest=none
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 Free
goto :start

:Preview
REM obsolete, now demo
set buildVer=Preview
set buildCfg="Retail demo|Win32"
set buildDir=o:\ArmADemo\
set outputDir=X:\ARMA2Public\ARMA2Preview
set outputExe=ArmA2Demo
set autotest=none
goto :start

:Retail
set buildVer=Retail
set buildCfg="Retail|Win32"
set buildDir=o:\arma\
set outputDir=X:\ARMA2Public\ARMA2Full
set outputExe=ArmA2
set mapOrderProtection=1
set buildXoreaxSwitch=REBUILD
set Binarize=Normal
goto :start

:SuperRelease
set buildVer=SuperRelease
set buildCfg="Retail|Win32"
set buildDir=o:\arma\
set outputDir=X:\ARMA2Public\ARMA2SuperRelease
set outputExe=ArmA2
set mapOrderProtection=1
set buildXoreaxSwitch=REBUILD
goto :start

:Steam
set buildVer=Steam
set buildCfg="Retail Steam|Win32"
set buildDir=o:\arma\
set outputDir=X:\ARMA2Public\ARMA2Steam
set outputExe=ArmA2Steam
set mapOrderProtection=1
set protection=none
set buildXoreaxSwitch=REBUILD
goto :start

:Server
set buildVer=Server
set buildCfg="Retail Server|Win32"
set buildDir=o:\arma\
set outputDir=X:\ARMA2Public\ARMA2Server
set outputExe=ArmA2Server
set mapOrderProtection=none
set protection=none
set autotest=none
set buildXoreaxSwitch=REBUILD
goto :start


:TakeOnH
set buildVer=TakeOnH
set buildCfg="Release|Win32"
set outputExe=TakeOnHInt
set autotest=1
set buildXoreaxSwitch=REBUILD
set BuildAllocators=1
goto :TakeOnHCommon

:TakeOnHRetail
set buildVer=TakeOnHRetail
set buildCfg="Retail|Win32"
set outputExe=TakeOnH
set buildXoreaxSwitch=REBUILD
set mapOrderProtection=1
set BuildAllocators=1
goto :TakeOnHCommon

:TakeOnHSteam
set buildVer=TakeOnHSteam
set buildCfg="Retail Steam|Win32"
set outputExe=TakeOnHSteam
set protection=none
set buildXoreaxSwitch=REBUILD
set mapOrderProtection=1
set BuildAllocators=1
goto :TakeOnHCommon

:TakeOnHServer
set outputExe=TakeOnHServer
set buildVer=TakeOnHServer
set buildCfg="Retail Server|Win32"
set mapOrderProtection=none
set protection=none
set autotest=none
set buildXoreaxSwitch=REBUILD
set buildDir=o:\TakeOnH\
set outputDir=X:\HeliSim
set solution=w:\C_branch\Poseidon\HeliSim\TakeOnH.sln
set sourceSVN=w:\C_branch\Poseidon\HeliSim\
set Binarize=heli
set shaderExe=TakeOnHInt.exe
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\Take On Helicopters
set buildXoreaxClean=/CLEAN
set BuildAllocators=1
goto :start

:TakeOnHDemoSteam
set protection=none
set outputExe=TakeOnHdemoSteam
set outputDir=x:\HeliSim\demosteam\
set buildCfg="Retail Demo Steam|Win32"
set shaderExe=TakeOnHdemoSteam.exe
set BuildAllocators=1
goto :TakeOnHDemoCommon 

:TakeOnHDemo
set protection=none
set outputExe=TakeOnHdemo
set outputDir=x:\HeliSim\demo\
set buildCfg="Retail Demo|Win32"
set shaderExe=TakeOnHdemo.exe
set BuildAllocators=1
goto :TakeOnHDemoCommon 

:TakeOnHDemoCommon
set buildVer=TakeOnHdemo
set mapOrderProtection=none
set autotest=none
set buildXoreaxSwitch=REBUILD
set buildDir=o:\TakeOnH_demo\
set outputDir=X:\HeliSim\demo
set solution=w:\C_branch\Poseidon\HeliSim\TakeOnH.sln
set sourceSVN=w:\C_branch\Poseidon\HeliSim\
set Binarize=0
set shaderExe=TakeOnHdemo.exe
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\Take On Helicopters
set buildXoreaxClean=/CLEAN
set BuildAllocators=1
goto :start

:TakeOnHPublicBeta
set buildVer=TakeOnHPublicBeta
set buildCfg="Retail Public Beta|Win32"
set outputDir=x:\HeliSim\Public\Public_Beta\
set outputExe=TakeOnHPublicBeta
set mapOrderProtection=1
set buildXoreaxSwitch=REBUILD
set autotest=none
set buildDir=o:\TakeOnH\
set solution=w:\C_branch\Poseidon\HeliSim\TakeOnH.sln
set sourceSVN=w:\C_branch\Poseidon\HeliSim\
set Binarize=heli
set protection=none
set shaderExe=TakeOnHPublicBeta.exe
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\Take On Helicopters
set buildXoreaxClean=/CLEAN
set BuildAllocators=1
goto :start

:TakeOnHPublicBetaRFT
set buildVer=TakeOnHPublicBetaRFT
set buildCfg="Retail Public Beta RFT|Win32"
set outputDir=x:\HeliSim\Public\Public_Beta_RFT\
set outputExe=TakeOnHPublicBetaRFT
set mapOrderProtection=1
set buildXoreaxSwitch=REBUILD
set autotest=none
set buildDir=o:\TakeOnH\
set solution=w:\C_branch\Poseidon\HeliSim\TakeOnH.sln
set sourceSVN=w:\C_branch\Poseidon\HeliSim\
set Binarize=heli
set protection=none
set shaderExe=TakeOnHPublicBetaRFT.exe
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\Take On Helicopters
set buildXoreaxClean=/CLEAN
set BuildAllocators=1
goto :start

:TakeOnHCommon
set buildDir=o:\TakeOnH\
set outputDir=X:\HeliSim
set solution=w:\C_branch\Poseidon\HeliSim\TakeOnH.sln
set sourceSVN=w:\C_branch\Poseidon\HeliSim\
set Binarize=heli
set shaderExe=TakeOnHInt.exe
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\Take On Helicopters
set buildXoreaxClean=/CLEAN
set BuildAllocators=1
goto :start

:Liberation
set solution=w:\C_branch\Poseidon\Arrowhead\ArmA2Ext.sln
set sourceSVN=w:\C_branch\Poseidon\Arrowhead\
set buildVer=Liberation
set buildCfg="Retail Liberation|Win32"
set buildDir=o:\Arma2_Expansion\
set outputDir=X:\ARMA2PublicExpansion\ARMA2Liberation
set shaderExe=ironfront.exe
set outputExe=ironfront
set protection=none
set autotest=none
set buildXoreaxSwitch=REBUILD
set logFolder=c:\Documents and Settings\%username%\Local Settings\Application Data\ironfront
set BuildAllocators=1
goto :start


:start
del %buildDir%\*.sec


set historyDir=%buildDir%
md %outputDir% 2>nul
md %outputDir%\dta 2>nul
md %outputDir%\bin 2>nul
md %buildDir% 2>nul
md %buildDir%\bin 2>nul

set dateBuild=%date%
set timeBuild=%time%

call c:\bis\Build\GetSources.bat
if not [%errorlevel%] == [0] (
  c:\bis\kecal\kecal.exe %user% unable to update from SVN: er%errorlevel% 
  exit /B %errorlevel%
)

pushd
svn info w:\c > c:\bis\build\svninfo_uncensored.txt

REM SVN revision - used for archiving directory names
c:\Bis\build\getRevisionNumber.pl c:\Bis\build\svninfo_uncensored.txt > c:\Bis\build\svninfoRevNo.txt
set /p SVNrevisionNo=<c:\bis\build\svninfoRevNo.txt

REM SVN revision - used in reports (kecal, NG etc.) - can be assembled from svninfoRevNo.txt
filter.pl -f -t c:\bis\build\svninfo_uncensored.txt revision > c:\bis\build\svninfo.txt

echo Getting sources revision... - revision %SVNrevisionNo%
popd

echo ----------------------------------------------------------------------
echo -- Arma2 (%buildVer%) build ----------------------------------
echo ----------------------------------------%dateBuild% %timeBuild%---
echo initiated by: %user% (%buildType% build)
echo parameters:
echo   - buildVer: %buildVer%
echo   - solution: %solution%
echo   - buildCfg: %buildCfg%
echo   - buildDir: %buildDir%
echo   - outputExe: %outputExe%
echo   - outputDir: %outputDir%
echo   - protection: %protection%
echo   - maporderProtection: %mapOrderProtection%
echo   - autotest: %autotest%
echo   - SVN Revision: %SVNrevisionNo% (archive directory name: %SVNrevisionNo%)
echo   - build switch: /%buildXoreaxSwitch%
echo ----------------------------------------------------------------------
call c:\bis\Build\dbg.bat %0 ----Arma2 (%buildVer%) build - by  %user% (%buildType% build) - buildCfg: %buildCfg% 2>nul



if [%SVNrevisionWanted%]==[0] echo ERROR: SVNrevisionWanted not set! Wrong SVN update (current revision: %SVNrevisionNo%, wanted: ?).
if [%SVNrevisionWanted%]==[0] goto :buildFail


if not [%SVNrevisionWanted%]==[%SVNrevisionNo%] echo ERROR: Wrong SVN update (current revision: %SVNrevisionNo%, wanted: %SVNrevisionWanted%).
if not [%SVNrevisionWanted%]==[%SVNrevisionNo%] goto :buildFail

echo Deleting eventual bin.pbo in dta...
del /q /f %buildDir%dta\bin.* 2>nul

echo Storing history...
call c:\bis\Build\dbg.bat %0 Storing history 2>nul
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/Poseidon -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/El -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/Es -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/RV -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/extern  -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/PhysicalLibs -svn



REM frequent verhistory (each build)
md %historyDir%verhistoryF 2>nul
md %historyDir%verhistoryF\new 2>nul
md %historyDir%verhistoryF\output 2>nul
xcopy c:\bis\bedar\_output\*.history %historyDir%verhistoryF\new /D /Y /Q
echo --> %historyDir%verhistoryF\output\buildInfo3.txt
echo -- SUCCESSFUL BUILD >> %historyDir%verhistoryF\output\buildInfo3.txt
echo -->> %historyDir%verhistoryF\output\buildInfo3.txt

md %historyDir%verHistoryF\output 2>nul
echo =======================================================================> %historyDir%verHistoryF\output\buildInfo.txt
echo ---------------------------------------------------------------------->> %historyDir%verHistoryF\output\buildInfo.txt
echo -- Arma2 (%buildVer%) build ---------------------------------->> %historyDir%verHistoryF\output\buildInfo.txt
echo ----------------------------------------%dateBuild% %timeBuild%--->> %historyDir%verHistoryF\output\buildInfo.txt
echo initiated from: %user% (%buildType% build)>> %historyDir%verHistoryF\output\buildInfo.txt
echo parameters:>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - buildVer: %buildVer%>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - solution: %solution%>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - buildCfg: %buildCfg%>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - buildDir: %buildDir%>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - outputExe: %outputExe%>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - outputDir: %outputDir%>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - protection: %protection%>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - autotest: %autotest%>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - SVN Revision: %SVNrevisionNo% (archive directory name: %SVNrevisionNo%)>> %historyDir%verHistoryF\output\buildInfo.txt
echo   - build switch: /%buildXoreaxSwitch%>> %historyDir%verHistoryF\output\buildInfo.txt
echo ---------------------------------------------------------------------->> %historyDir%verHistoryF\output\buildInfo.txt

echo Deriving differences...
c:\bis\bedar\diffHistoryDirsF.pl

echo Aggregating differences to output...
c:\bis\bedar\diffAggregateToOutputF.pl

REM Each configuration deletes .exe it is building (caution: some vers needs default ver exe to generate shaders)
echo Deleting %buildDir%%outputExe%.exe, ren %outputExe%Unprotected.exe to %outputExe%.exe...
del /q /f %buildDir%%outputExe%.exe 2>nul
rem del shader exe and if shaderexe = outputExe then pipeline do not build it again
del /q /f %buildDir%%shaderExe% 2>nul 

REM TODO: test and enable skip if needed
REM Build acceleration - use unprotected .exe (as build target) - if none changed in sources, no need to build that exe; if mapOrderProtection, exe version not matching (doublebuild)
REM if [%mapOrderProtection%]==[none] goto :skipBuildAccelByExeCopy
ren %buildDir%%outputExe%Unprotected.exe %outputExe%.exe
REM :skipBuildAccelByExeCopy

set TBB3=w:\C_branch\Poseidon\Arrowhead\Allocators\TBB3\tbb\build\vsproject\makefile.sln
set TBB4=w:\C_branch\Poseidon\Arrowhead\Allocators\TBB4\tbb\build\vsproject\makefile.sln
set TCMalloc=w:\C_branch\Poseidon\Arrowhead\Allocators\TCMalloc\TCMalloc.sln
set JEMalloc=w:\C_branch\Poseidon\Arrowhead\Allocators\JEMalloc\JEMalloc_bi.sln
set NedMalloc=w:\C_branch\Poseidon\Arrowhead\Allocators\NedMalloc\NedMalloc_bi.sln


IF [%BuildAllocators%] == [1] (

  call:BuildAlloc TBB3 %TBB3% tbbmalloc "Release|Win32" tbb3malloc_bi https://dev.bistudio.com/svn/pgm/branches/Poseidon/Arrowhead/Allocators/TBB3
  call:BuildAlloc TBB4 %TBB4% tbbmalloc "Release|Win32" tbb4malloc_bi https://dev.bistudio.com/svn/pgm/branches/Poseidon/Arrowhead/Allocators/TBB4
  call:BuildAlloc TCMalloc %TCMalloc% TCMalloc "Release|Win32" TCMalloc_bi https://dev.bistudio.com/svn/pgm/branches/Poseidon/Arrowhead/Allocators/TCMalloc
  call:BuildAlloc JEMalloc %JEMalloc% JEMalloc_bi "Release|Win32" JEMalloc_bi https://dev.bistudio.com/svn/pgm/branches/Poseidon/Arrowhead/Allocators/JEMalloc
  call:BuildAlloc NedMalloc %NedMalloc% NedMalloc_bi "Release|Win32" NedMalloc_bi https://dev.bistudio.com/svn/pgm/branches/Poseidon/Arrowhead/Allocators/NedMalloc
)

:building
echo Building...
call c:\bis\Build\dbg.bat %0 Building...
if not [buildXoreaxClean] == [] "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% %buildXoreaxClean% /prj=lib2005 /cfg=%buildCfg%
echo "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /%buildXoreaxSwitch% /prj=lib2005 /cfg=%buildCfg%  /log="c:\bis\build\BuildLogIB.txt"
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /%buildXoreaxSwitch% /prj=lib2005 /cfg=%buildCfg%  /log="c:\bis\build\BuildLogIB.txt"
if errorlevel 1 goto buildfail

set buildXoreaxClean=  

if [%mapOrderProtection%]==[none] goto :skipMapOrderProtection

REM if mapOrderProtection then force build only an delete exe
set buildXoreaxSwitch=BUILD
del %buildDir%%outputExe%.exe

echo Applying mapOrder protection (c:\bis\build\mapOrder.bat)
call c:\bis\Build\dbg.bat %0 call c:\bis\build\mapOrder.bat %buildDir% %outputExe%
call c:\bis\build\mapOrder.bat %buildDir% %outputExe%
if errorlevel 1 goto :mapOrderProtectionFail
set mapOrderProtection=none
call c:\bis\Build\dbg.bat %0 del /q /f %buildDir%%outputExe%.exe 2>nul
del /q /f %buildDir%%outputExe%.exe 2>nul
goto :building
:skipMapOrderProtection


echo Applying protection (%sourceSVN%lib\Tools\ArmA\protectInternal.bat)...
ren %builddir%%outputExe%.exe %outputExe%Unprotected.exe

if [%protection%]==[none] goto :skipProtection
if [%protection%]==[0] goto :skipProtection


REM Apply protection (TODO: move below if it takes too long time (after store history, before autotest))
call c:\bis\Build\dbg.bat %0 call %sourceSVN%lib\Tools\ArmA\protectInternal.bat %user% %buildVer%

REM dir o:\arma > %buildDir%dir_before_protect.txt
call %sourceSVN%lib\Tools\ArmA\protectInternal.bat %user% %buildVer%

for /F %%i in ('echo %builddir%%outputExe%.exe') do if [%%~zi]==[0] goto securomFail

goto :afterProtection


:skipProtection

echo Protection skipped (by parameter).
copy /Y /B %builddir%%outputExe%Unprotected.exe %builddir%%outputExe%.exe

:afterProtection

echo sign exe by bis certificate
call c:\BIS\sign\sign.bat %builddir%%outputExe%.exe

REM Copy history files right to %buildDir%verhistory\new (not thru x:\verhistory)
md o:\arma 2>nul
md %historyDir%verhistory 2>nul
md %historyDir%verhistory\new 2>nul
xcopy c:\bis\bedar\_output\*.history %historyDir%verhistory\new /D /Y /Q

rem Deleting FPS report file
del /q /f %historyDir%verhistory\output\bedar_fps.rpt 2>nul
del /q /f %historyDir%verhistory\output\bedar_fpsPerftest.rpt 2>nul


REM .csv depricated, localization db is used - must be done before autotest
del /q /f %buildDir%bin\*.csv 2>nul

REM Get autotest dir - SVN
call %buildDir%autotest\GetautotestDir.bat

if [%autotest%] == [none] goto :skipAutotest
if [%autotest%] == [0] goto :skipAutotest

call c:\bis\Build\dbg.bat %0 call %sourceSVN%lib\Tools\ArmA\autotest.bat %2 2>nul
call %sourceSVN%lib\Tools\ArmA\autotest.bat %buildDir% %outputExe% %buildDir%autotest\ %2 %buildVer%
if errorlevel 1 goto autotestfail
:skipAutotest

if [%2] == [regular] goto regular
if [%buildVer%] == [SuperRelease] goto :SuperReleaseAutotest
if [%buildVer%] == [Retail] goto :RetailAutotest
goto distribute

:SuperReleaseAutotest
:RetailAutotest
REM Public versions always full autotest...
REM
REM
REM if errorlevel 1 goto autotestfail
goto distribute

:regular
REM Note: following restoring of errorlevel - to avoid confusion of result with files that process text outputs (BEDAR processing files) - leaving only errorlevels set by game runs

call c:\bis\Build\dbg.bat %0 call %sourceSVN%lib\Tools\ArmA\autotestFunct.bat %buildDir% %outputExe% %2 2>nul
call %sourceSVN%lib\Tools\ArmA\autotestFunct.bat %buildDir% %outputExe% %2

REM sets errorlvl if %errorlevel% is equal or greater (if %errorlevel% is 0 then all ok (%errorlvl% set to 0))
call c:\bis\Build\dbg.bat %0 if errorlevel %errorlvl% set errorlvl=%errorlevel%
if errorlevel %errorlvl% set errorlvl=%errorlevel%

REM deriving fps from debug.log - stored by funct. autotest mission _TEST_FPS
c:\bis\bedar\deriveFPS.pl "C:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2\debug.log" %historyDir%verhistory\output\bedar_fps.rpt

REM Testing unprotected version
call c:\bis\Build\dbg.bat %0 call %sourceSVN%lib\Tools\ArmA\autotestFunct.bat %buildDir% %outputExe%Unprotected %2
call %sourceSVN%lib\Tools\ArmA\autotestFunct.bat %buildDir% %outputExe%Unprotected %2
call c:\bis\Build\dbg.bat %0 if errorlevel %errorlvl% set errorlvl=%errorlevel%
if errorlevel %errorlvl% set errorlvl=%errorlevel%

REM more FPS measurements during regular build - needed is correct errorlevel
call c:\bis\bedar\seterrorlevel.bat %errlvl%
call c:\bis\Build\dbg.bat %0 call %sourceSVN%lib\Tools\ArmA\autotestFPSpc.bat
call %sourceSVN%lib\Tools\ArmA\autotestFPSpc.bat

REM Evaluate errorlevel after all testing
call c:\bis\Build\dbg.bat %0 call c:\bis\bedar\seterrorlevel.bat %errlvl%
call c:\bis\bedar\seterrorlevel.bat %errlvl%
if errorlevel 1 goto autotestfail


:distribute
echo Distributing...

rem Generate shaders
echo +Generate shaders...

REM shared exe already exist do not need to build agaim
if exist %buildDir%%shaderExe% goto :skipBuildForShaders

echo Building %shaderExe% to have shaders

echo "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /BUILD /prj=lib2005 /cfg="Release|Win32"  /log="c:\bis\build\BuildLogIB.txt"
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /BUILD /prj=lib2005 /cfg="Release|Win32"  /log="c:\bis\build\BuildLogIB.txt"
if errorlevel 1 goto buildfail


:skipBuildForShaders

o:
cd %buildDir%
echo   ---errorlevel before generating shaders: %errorlevel%
REM call %buildDir%ArmA2Int.exe -nosplash -window -GenerateShaders
REM %buildDir%ArmA2Int.exe -nosplash -window -GenerateShaders
%buildDir%%shaderExe% -nosplash -window -GenerateShaders
echo   ---errorlevel after generating shaders: %errorlevel%
if errorlevel 1 goto generateShaderFail

rem Binarize configs
echo +Binarize configs...

cd bin
b:\cfgconvert\cfgConvert -bin -dst config.bin config.cpp
del /q /f %buildDir%bin\*.?pp 2>nul
del /q /f %buildDir%bin\*.h 2>nul


rem Create the PBO
echo +Create the PBO...
b:\filebank\filebank -exclude b:\FileBank\exclude.lst -property prefix=bin %buildDir%bin
b:\security\dsSignFile.exe b:\security\bi.biprivatekey %buildDir%bin.pbo
b:\security\dsSignFile.exe b:\security\bi2.biprivatekey %buildDir%bin.pbo
b:\security\dsSignFile.exe "b:\Security\TKOH_Demo.biprivatekey" %buildDir%bin.pbo
b:\security\dsSignFile.exe b:\security\a2free.biprivatekey %buildDir%bin.pbo


rem copy PBO to X
echo +copy PBO to X...
xcopy  /Y /R /D /I /K "%buildDir%bin.pbo*" %outputDir%\dta

REM create changeLog
call c:\Bis\bedar\derivePatchLog.bat %sourceSVN% %buildDir%changeLog.txt 79385 %SVNrevisionNo%

rem Bin distribution
call c:\bis\Build\dbg.bat %0 call %sourceSVN%lib\Tools\ArmA\putBinSimple.bat %computername%-%user% %SVNrevisionNo% %outputDir% %buildDir%%outputExe% %outputExe% %buildDir% %sourceSVN%cfg\
call %sourceSVN%lib\Tools\ArmA\putBinSimple.bat %computername%-%user% %SVNrevisionNo% %outputDir% %buildDir%%outputExe% %outputExe% %buildDir% %sourceSVN%cfg\

if /I not [%2] == [regular] goto afterRegularArchiving
if /I not [%buildVer%] == [DefaultVersion] goto afterRegularArchiving

REM archiving default (internal) version to regular build archive directory
call %sourceSVN%lib\Tools\ArmA\putBinSimple.bat REGULAR %SVNrevisionNo% %outputDir% %buildDir%%outputExe% %outputExe% %sourceSVN%cfg\

:afterRegularArchiving
type %historyDir%verHistoryF\output\buildInfo3.txt >> %historyDir%verHistoryF\output\buildInfo.txt
type %historyDir%verHistoryF\output\buildInfo2.txt >> %historyDir%verHistoryF\output\buildInfo.txt
copy "%historyDir%verHistoryF\output\buildInfo.txt" x:\verHistory\buildInfo\buildInfo_%buildVer%_%SVNrevisionNo%.txt /Y

c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user% SVN Revision:%SVNrevisionNo% protection:%protection% mapOrderProtection:%mapOrderProtection% autotest:%autotest%) %outputExe% has been distributed. file:///X:/verHistory/buildInfo/buildInfo_%buildVer%_%SVNrevisionNo%.txt 
call c:\bis\bedar\sendHistoryAB2.bat

if /I [%2] == [regular] echo Whole A2 backup to o:\arma_bup ...
if /I [%2] == [regular] md o:\arma_bup 2>nul
if /I [%2] == [regular] xcopy /y /r /e /d /i /k %buildDir%*.* o:\arma_bup 1>nul

goto end

:securomFail
echo --FAILED! Securom protection. %date% %time% %1 %2 %3 %4 %5 %6 >> c:\bis\build\buildLog.txt
if [%2] == [regular] C:\BIS\blat\toNewsFile.bat "Securom protection of %outputExe% failed!" "file size after protection is 0B" bistudio.engine.task-log
c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user% SVN Revision: %SVNrevisionNo%) Securom protection of %outputExe% failed! (%0 %1 %2 %3 %4 %5 %6) 
goto afterBinarization

:mapOrderProtectionFail
set mapOrderProtectionResult=" Failure! (.sec file probably not created)"

:buildfail
echo --FAILED! Build failed. %date% %time% %1 %2 %3 %4 %5 %6 >> c:\bis\build\buildLog.txt
if [%mapOrderProtection%] neq [none] echo mapOrderProtection result: %mapOrderProtectionResult% >> c:\bis\build\buildLog.txt
echo --> %historyDir%verhistoryF\output\buildInfo3.txt
echo -- BUILD FAILED! %date% %time% %1 %2 %3 %4 %5 %6 >> %historyDir%verhistoryF\output\buildInfo3.txt
echo -->> %historyDir%verhistoryF\output\buildInfo3.txt

REM if [%2] == [regular] C:\BIS\blat\toNews.bat "Regular build failed." "Building of %solution% failed." bistudio.engine.task-log
if [%2] == [regular] C:\BIS\blat\toNewsFile.bat "Regular build of %outputExe% failed!" "c:\bis\build\BuildLogIB.txt" bistudio.engine.task-log

type %historyDir%verHistoryF\output\buildInfo3.txt >> %historyDir%verHistoryF\output\buildInfo.txt
type %historyDir%verHistoryF\output\buildInfo2.txt >> %historyDir%verHistoryF\output\buildInfo.txt
copy "%historyDir%verHistoryF\output\buildInfo.txt" x:\verHistory\buildInfo\buildInfo_%buildVer%_%SVNrevisionNo%.txt /Y

call c:\bis\Build\dbg.bat %0 "Building of %solution% failed." 2>nul
c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user% SVN Revision: %SVNrevisionNo%) Building of %outputExe% failed! (%0 %1 %2 %3 %4 %5 %6) file:///X:/verHistory/buildInfo/buildInfo_%buildVer%_%SVNrevisionNo%.txt file:///X:/verHistory/buildInfo/buildLogIB.txt
type "c:\BIS\Build\BuildLogIB.txt" > x:\verhistory\buildInfo\buildLogIB.txt
goto afterBinarization

:autotestfail
echo --FAILED! Autotesting failed. %date% %time% %1 %2 %3 %4 %5 %6 >> c:\bis\build\buildLog.txt
echo --> %historyDir%verhistoryF\output\buildInfo3.txt
echo -- AUTOTEST FAILED! %date% %time% %1 %2 %3 %4 %5 %6 >> %historyDir%verhistoryF\output\buildInfo3.txt
call c:\bis\Build\dbg.bat %0 :autotestfail - errorlevel: %errorlevel% 2>nul
echo Autotest Failed... (errorlevel: %errorlevel%)

type %historyDir%verHistoryF\output\buildInfo3.txt >> %historyDir%verHistoryF\output\buildInfo.txt
type %historyDir%verHistoryF\output\buildInfo2.txt >> %historyDir%verHistoryF\output\buildInfo.txt
copy "%historyDir%verHistoryF\output\buildInfo.txt" x:\verHistory\buildInfo\buildInfo_%buildVer%_%SVNrevisionNo%.txt /Y

REM News message sent in autotest.bat / autotestFunct.bat
c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user% SVN Revision: %SVNrevisionNo%) Autotest of %outputExe% failed! file:///X:/verHistory/buildInfo/buildInfo.txt file:///X:/verHistory/AB2_rpt/%outputExe%.RPT

call c:\bis\bedar\autotestresult\autotestresult %1
goto afterBinarization

:generateShaderFailBuild
type %historyDir%verHistoryF\output\buildInfo3.txt >> %historyDir%verHistoryF\output\buildInfo.txt
type %historyDir%verHistoryF\output\buildInfo2.txt >> %historyDir%verHistoryF\output\buildInfo.txt
copy "%historyDir%verHistoryF\output\buildInfo.txt" x:\verHistory\buildInfo\buildInfo_%buildVer%_%SVNrevisionNo%.txt /Y

echo Build of %shaderExe% for shader generation failed!
type "c:\BIS\Build\BuildLogIB.txt" > x:\verhistory\buildInfo\buildLogIB.txt
c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user% SVN Revision: %SVNrevisionNo%) Building of %shaderExe% failed (Shader generating %shaderExe%)! (%0 %1 %2 %3 %4 %5 %6) file:///X:/verHistory/buildInfo/buildInfo_%buildVer%_%SVNrevisionNo%.txt file:///X:/verHistory/buildInfo/buildLogIB.txt


:generateShaderFail
type %historyDir%verHistoryF\output\buildInfo3.txt >> %historyDir%verHistoryF\output\buildInfo.txt
type %historyDir%verHistoryF\output\buildInfo2.txt >> %historyDir%verHistoryF\output\buildInfo.txt
copy "%historyDir%verHistoryF\output\buildInfo.txt" x:\verHistory\buildInfo\buildInfo_%buildVer%_%SVNrevisionNo%.txt /Y

echo Shader generation failed!
c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user% SVN Revision: %SVNrevisionNo%) Building of %shaderExe% failed (Shader generation failed)! (%0 %1 %2 %3 %4 %5 %6) file:///X:/verHistory/buildInfo/buildInfo_%buildVer%_%SVNrevisionNo%.txt file:///X:/verHistory/buildInfo/buildLogIB.txt
goto afterBinarization


:end


if /I  [%binarize%]==[none] goto skipbinarize
if /I  [%binarize%]==[0] goto skipbinarize
  call c:\bis\Build\dbg.bat %0 echo Building the Binarization... 2>nul
  call c:\bis\Build\dbg.bat %0 "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %sourceSVN%\binarize\binarize.sln /%buildXoreaxSwitch% /prj=binarize /cfg="Release|Win32" /log="c:\bis\build\BuildLogIB.txt" 2>nul
  echo Building the Binarization...
  "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %sourceSVN%\binarize\binarize.sln /%buildXoreaxSwitch% /prj=binarize /cfg="Release|Win32" /log="c:\bis\build\BuildLogIB.txt"
  if errorlevel 1 goto binarizationfail
:skipbinarize

goto afterBinarization

:binarizationfail
echo --FAILED! Binarization failed. %date% %time% %1 %2 %3 %4 %5 %6 >> c:\bis\build\buildLog.txt

echo --> %historyDir%verhistoryF\output\buildInfo3.txt
echo -- BINARIZATION BUILD FAILED! %date% %time% %1 %2 %3 %4 %5 %6 >> %historyDir%verhistoryF\output\buildInfo3.txt
echo -->> %historyDir%verhistoryF\output\buildInfo3.txt

type %historyDir%verHistoryF\output\buildInfo3.txt >> %historyDir%verHistoryF\output\buildInfo.txt
type %historyDir%verHistoryF\output\buildInfo2.txt >> %historyDir%verHistoryF\output\buildInfo.txt
copy "%historyDir%verHistoryF\output\buildInfo.txt" x:\verHistory\buildInfo\buildInfo_%buildVer%_%SVNrevisionNo%.txt /Y

call c:\bis\Build\dbg.bat %0 Building of binarization failed 2>nul
c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user% SVN Revision: %SVNrevisionNo%) Building of binarization failed! file:///X:/verHistory/buildInfo/buildInfo_%buildVer%_%SVNrevisionNo%.txt file:///X:/verHistory/buildInfo/buildLogIB.txt
type "c:\BIS\Build\BuildLogIB.txt" > x:\verhistory\buildInfo\buildLogIB.txt
rem No need to report fail to NG - it will be reported by separate binarization build

:afterBinarization

if [%2] == [regular] goto regularafterbin
goto totalafterbin
0

:regularafterbin

REM if failed, no upload
if errorlevel 1 goto :regularafterbinNoUpload

REM Upload .EXE + bin.pbo to https://beta2.bistudio.com/files/a2setup/
call c:\bis\build\upload-beta2_bistudio_com.bat > c:\bis\build\upload.log

:regularafterbinNoUpload
call c:\bis\Build\dbg.bat %0 regularafterbin 2>nul
REM Create differences btw old new report, copy difference file to %historyDir%verHistory
call c:\bis\bedar\filter\extract_rpt.bat


REM derive differences in history files, aggregate for output, send to NG
call c:\bis\bedar\correlation.bat
call c:\bis\bedar\sendNG.bat

call c:\bis\Build\dbg.bat %0 regularafterbin -end 2>nul

:totalafterbin
echo Sending .rpt to x:\verhistory\ab2_rpt
xcopy "%logFolder%\%outputExe%.rpt" x:\verhistory\ab2_rpt\ /D /Y /R
REM send eventual dumps
xcopy "%logFolder%\%outputExe%.*"   x:\verhistory\ab2_rpt\ /D /Y /R

echo Sending build notification...
md x:\verHistory\buildInfo 2>nul
type %historyDir%verHistoryF\output\buildInfo3.txt >> %historyDir%verHistoryF\output\buildInfo.txt
type %historyDir%verHistoryF\output\buildInfo2.txt >> %historyDir%verHistoryF\output\buildInfo.txt
copy "%historyDir%verHistoryF\output\buildInfo.txt" x:\verHistory\buildInfo\buildInfo_last.txt /Y

type "%historyDir%verHistoryF\output\buildInfo.txt" >> %historyDir%verHistoryF\output\buildInfo_history.txt
xcopy "%historyDir%verHistoryF\output\buildInfo_history.txt" x:\verHistory\buildInfo /D /Y /R

REM if setup in parameters send makesetup request to Builder
if /I [%4] == [SETUP] B:\RemEx\ExeCln.exe Builder MakeSetup %2 %1 %3 %4 %5 %6 %7 %8 %9

ECHO all done
exit /b


:BuildAlloc
  echo Building %~1      
  "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %~2 /%buildXoreaxSwitch% /prj=%~3 /cfg="%~4" /log="c:\bis\build\BuildLogIB.txt"
  
  md %buildDir%Dll   
  call c:\BIS\sign\sign.bat W:\C_branch\Poseidon\Arrowhead\Allocators\%~5.dll   
  copy /y W:\C_branch\Poseidon\Arrowhead\Allocators\%~5.* %buildDir%Dll
  
  pushd W:\pack\Allocators
  svn co %~6
  if exist %~1_source.7z  del %~1_source.7z 
  B:\7zip\7za.exe a -xr!Release -xr!.svn -xr!Debug -xr!*.ncb %~1_source.7z %~1
  cmd /c "c:\Program Files\Putty\PSCP.EXE" -pw VeliceTajneHeslo -p %~1_source.7z  apache@ws1.bistudio.com:/data/web/downloads.bistudio.com/content/arma2.com/update/Allocs
  popd  
goto:eof 