@echo off
REM Uploads executables and other built engine files to beta2.bistudio.com
REM from: o:\arma\ (exe, bin.pbo) <- build server Albunda2
REM
REM usage: upload-beta2_bistudio_com.bat
REM
REM by Vilem & Bx2


CALL b:\build\sendAddons.bat

EXIT /B


REM !!!!! OBSOLOTE, USE BUILDER INSTEAD

pushd .

set sourceProduct=ArmA2Int
REM set sourceProduct2=ArmA2_Server

set sourceDir=o:\Arma
set source=%sourceDir%\%sourceProduct%
REM set source2=%sourceDir%\%sourceProduct2%

set ftpUpload=c:\bin\curl\curl.exe -f -k -u packbot:stupidbot -T
set ftpTarget=https://beta2.bistudio.com/files/a2setup/a2/



rem Deploy protected files to the FTP
echo Deploying to FTP server
REM %ftpUpload% %source%.map %ftpTarget%/
%ftpUpload% %source%.exe %ftpTarget%/
%ftpUpload% %source%.exe.cfg %ftpTarget%/

%ftpUpload% %sourceDir%\ijl15.dll %ftpTarget%/


REM SecuROM files
%ftpUpload% %sourceDir%\lang.ini     %ftpTarget%/
%ftpUpload% %sourceDir%\paul.dll     %ftpTarget%/
%ftpUpload% %sourceDir%\logo-paul.bmp     %ftpTarget%/
%ftpUpload% %sourceDir%\unicows.dll    %ftpTarget%/


%ftpUpload% %sourceDir%\xlive.dll.cat %ftpTarget%/
%ftpUpload% %sourceDir%\xlivefnt.dll %ftpTarget%/
%ftpUpload% %sourceDir%\xlive.dll %ftpTarget%/

%ftpUpload% %sourceDir%\bin.pbo %ftpTarget%/dta/
%ftpUpload% %sourceDir%\bin.pbo.bi.bisign %ftpTarget%/dta/





REM %ftpUpload% %source2%.exe %ftpTarget%/

popd