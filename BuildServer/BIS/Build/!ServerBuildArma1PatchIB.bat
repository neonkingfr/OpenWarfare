@echo off

c:
cd c:\bis\Build

echo Getting sources...
call GetSources.bat
call W:\C\Poseidon\lib\Tools\ArmA\updCfg.bat

echo Building...
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\ArmA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="Release|Win32"
if errorlevel 1 goto fail

echo Distributing...
call W:\C\Archive\ArmA\Poseidon\lib\Tools\ArmA\putBin.bat %1

c:\bis\kecal\kecal.exe %1 Arma has been distributed
goto end

:fail

c:\bis\kecal\kecal.exe %1 Building of arma failed
goto afterBinarization

:end

echo Building the Binarization...
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Archive\ArmA\Poseidon\binarize\binarize.sln /build /prj=binarize /cfg="Release|Win32"
if errorlevel 1 c:\bis\kecal\kecal.exe %1 Building of binarization failed

:afterBinarization