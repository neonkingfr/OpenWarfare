REM
REM 
REM

pushd .
Echo Maporder.bat: %1 + %2
set maporderoutputDir=%1
if [%maporderoutputDir%]==[] Echo   --default dir: o:\arma\
if [%maporderoutputDir%]==[] set maporderoutputDir=o:\arma\

set exe=%2
if [%exe%]==[] Echo   --default exe: Arma2
if [%exe%]==[] set exe=arma2


cd /D %maporderoutputDir%
del /q /f %exe%.sec
b:\protect\mapOrder.exe %exe%.map

popd
if EXIST %maporderoutputDir%%exe%.sec exit /B 0
exit /B 1


