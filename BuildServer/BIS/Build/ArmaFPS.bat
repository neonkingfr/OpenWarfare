@echo off
set user=%1
set dateBuild=%date%
set timeBuild=%time%

echo ----------------------------------------------------------------------
echo -- Arma2 FPS measurement ----------------------------------
echo ----------------------------------------%dateBuild% %timeBuild%---
echo initiated by: %user%


call w:\c\Poseidon\lib\Tools\ArmA\autotestFPSpc.bat 

copy /Y /B o:\arma\verhistory\output\bedar_fpsPerftest.rpt x:\verHistory\AB2_fps\bedar_fpsPerftest.rpt 
copy /Y /B o:\arma\verhistory\output\bedar_slowFrames.rpt x:\verHistory\AB2_fps\bedar_slowFrames.rpt 

c:\bis\kecal\kecal.exe %user% (FPS measurement %dateBuild% %timeBuild% by %user%) Arma FPS measurement complete. file:///X:/verHistory/AB2_fps/bedar_fpsPerftest.rpt