@echo off

c:
cd c:\bis\Build

echo Getting sources...
call GetSources.bat

echo Getting configs...
call X:\update\getCfg.bat
call W:\C\Poseidon\lib\Tools\ArmA\updCfg.bat

rem Demo is not FADE protected now

echo Building...
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="Retail Demo|Win32"
if errorlevel 1 goto fail

c:\bis\kecal\kecal.exe %1 Arma Retail Demo build OK, protecting

rem Get the build version without incrementing it
echo Build version retrieving...
w:
cd w:\c\Poseidon\lib
b:\buildNo\buildNo -n buildNo.h
set build=%errorlevel%

rem bin.pbo packing and moving to u:\mapfiles\arma
echo Packing of bin.pbo and moving into u:\mapfiles\arma folder...
b:\ssMan\ssMan.exe \\CORE1\vssdatap\cfg\srcsafe.ini $/Fp/Bin
b:\cfgconvert\cfgConvert -bin -dst o:\fp\bin\config.bin o:\fp\bin\config.cpp
del /q /f o:\fp\bin\*.*pp
del /q /f o:\fp\bin\*.h
b:\filebank\filebank -exclude b:\FileBank\exclude.lst -property prefix=bin o:\fp\bin
xcopy o:\fp\bin.* U:\mapfiles\ArmA\%build%\Bin\ /Y /R
b:\security\dsSignFile.exe c:\bis\security\armademo.biprivatekey U:\mapfiles\ArmA\%build%\Bin\bin.pbo

echo Distributing...
call W:\C\Poseidon\lib\Tools\fp\putDemo.bat 
if errorlevel 1 goto distFail

c:\bis\kecal\kecal.exe %1 Arma Demo has been distributed
goto end

:fail
c:\bis\kecal\kecal.exe %1 Building of Arma Demo failed

goto end

:distFail
c:\bis\kecal\kecal.exe %1 Distribution of Arma Demo failed

:end
