rem Arma1 archive branch build pipeline <- Vilem

@echo off
pushd . 


c:
cd c:\bis\Build

md o:\arma1 2>nul
md o:\arma1\bin 2>nul
md u:\mapfiles 2>nul
rem next line is not working if arma1 doesn't exist (because of rights)
md x:\arma1 2>nul

echo Getting sources...
call GetSources.bat

REM del /F /Q O:\Arma1\*.*
del /F /Q O:\Arma1\*.map
del /F /Q O:\Arma1\*.pdb
del /F /Q O:\Arma1\*.prf
del /F /Q O:\Arma1\*.sec
del /F /Q O:\Arma1\*.lib
del /F /Q O:\Arma1\*.ord
del /F /Q o:\Arma1\bin\config.bin
REM we need to let .dll there to run arma to generate shaders

echo First building...
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\ArmA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="RetailInternal|Win32,Retail Server|Win32"
if errorlevel 1 goto fail

B:\Protect\MapOrder.exe O:\Arma1\ArmA.map
if errorlevel 1 goto fail

del /q /f O:\Arma1\ArmA.exe

echo Second building (applying Arma.sec)... 
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Archive\ArmA\Poseidon\Poseidon2005.sln /build /prj=lib2005 /cfg="RetailInternal|Win32,Retail Server|Win32"
if errorlevel 1 goto fail


c:\bis\kecal\kecal.exe %1 Arma1 Retail + Retail Server built.

rem Get the build version without incrementing it
echo Build version retrieving...
w:
cd w:\c\archive\arma\Poseidon\lib
b:\buildNo\buildNo -n buildNo.h
set build=%errorlevel%
echo version number: %errorlevel%





rem bin.pbo packing and moving to u:\mapfiles\arma1
echo Packing of bin.pbo and moving into u:\mapfiles\arma1 folder...


xcopy /Y /R /E /D /I /K w:\c\archive\Arma\poseidon\cfg\bin\*.* o:\arma1\bin
o:
cd \arma1\bin

b:\Arma1tools\cfgconvert\cfgConvert -bin -dst config.bin config.cpp

del /q /f o:\Arma1\bin\*.?pp 2>nul
del /q /f o:\Arma1\bin\*.h 2>nul

o:
cd \arma1\
rem Generate shaders
echo +Generate shaders...
arma.exe -nosplash -window -GenerateShaders
if errorlevel 1 goto failshaders

cd bin

b:\Arma1tools\filebank\filebank -exclude b:\FileBank\exclude.lst -property prefix=bin o:\Arma1\bin
b:\security\dsSignFile.exe c:\bis\security\bi.biprivatekey o:\arma1\bin.pbo

rem Arma_server_beforeappendingtable.exe will be used for updating server (appending table at end) with new hacked functions info
rem Update will be done by remex call to Albunda2

copy /Y /B o:\arma1\arma_server.exe o:\arma1\arma_server_beforeappendingtable.exe

b:\crccheck\FunctionNametoaddr.exe W:\c\Archive\ArmA\Poseidon\Protect\hacked.txt o:\arma1\hacked.bin o:\arma1\arma.exe o:\arma1\arma.map
if errorlevel 1 goto crcFail

copy o:\arma1\arma_server_beforeappendingtable.exe /B + o:\arma1\hacked.bin /B o:\arma1\arma_server.exe
if errorlevel 1 goto crcFail
del /q /f o:\arma1\hacked.bin

echo Distributing...
call W:\c\archive\arma\Poseidon\lib\Tools\Arma1\putArma1Internal.bat 
if errorlevel 1 goto distFail

c:\bis\kecal\kecal.exe %1 Arma Retail + Retail Server distributed.
goto end

:fail
echo Building of Arma Retail failed!
c:\bis\kecal\kecal.exe %1 Building of Arma Retail failed!
goto afterend

:failshaders
echo Shader generation for Arma Retail failed!
c:\bis\kecal\kecal.exe %1 Shader generation for Arma Retail failed!
goto afterend

:crcFail
echo Adding CRC to Arma_server.exe failed!
c:\bis\kecal\kecal.exe %1 Adding CRC to Arma_server.exe failed!
goto afterend


:distFail
echo Distribution of Arma Retail failed!
c:\bis\kecal\kecal.exe %1 Distribution of Arma Retail failed!
goto afterend

:end

echo All ok, have a nice day.

:afterend

popd