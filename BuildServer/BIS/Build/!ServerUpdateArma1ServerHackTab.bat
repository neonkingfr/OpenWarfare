@echo off
pushd .

echo Updating Arma_server.exe with latest info from hacked.txt.
echo Retrieve build version...
rem parameter -n tells not to increment version number

w:
cd w:\c\archive\arma\Poseidon\lib
b:\buildNo\buildNo -n buildNo.h
set build=%errorlevel%

echo Applying...

"c:\Program Files\Subversion\bin\svn.exe" checkout --non-interactive https://dev.bistudio.com/svn/pgm/trunk/Poseidon/Protect w:\c\Poseidon\Protect

b:\crccheck\FunctionNametoaddr.exe W:\c\Archive\ArmA\Poseidon\Protect\hacked.txt o:\arma1\hacked.bin o:\arma1\arma.exe o:\arma1\arma.map
if errorlevel 1 goto crcFail
REM  arma_server_beforeappendingtable.exe <- using this file (created in !ServerBuildArma1SRIB.bat) to prevent multiple adding 
copy o:\arma1\arma_server_beforeappendingtable.exe /B + o:\arma1\hacked.bin /B o:\arma1\arma_server.exe
if errorlevel 1 goto crcFail
del /q /f o:\arma1\hacked.bin

c:\bis\kecal\kecal.exe %1 Arma1 server version %build% updated with new hacked.txt.

echo Distributing...
call W:\c\Archive\ArmA\Poseidon\lib\Tools\Arma1\putArma1.bat -noincrement
if errorlevel 1 goto distFail

goto end


:distFail
echo Distribution of Arma Retail failed!
c:\bis\kecal\kecal.exe %1 Distribution of Arma Retail ver. %build% failed!
goto afterend

:crcFail
echo Creating CRC / adding CRC to Arma_server.exe failed!
c:\bis\kecal\kecal.exe %1 Adding CRC to Arma_server.exe ver. %build% failed!
goto afterend

:end
echo End: Success.

:afterend
popd



