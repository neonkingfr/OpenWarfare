@echo off

c:
cd c:\bis\Build

echo Getting sources...
call GetSources.bat

echo Getting configs...
call X:\update\getCfg.bat
call W:\C\Poseidon\lib\Tools\ArmA\updCfg.bat

rem ord copyis now integrated in the project as pre-link step

echo Building...
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Poseidon\ArmA2.sln /build /prj=lib2005 /cfg="Retail|Win32,Retail SecuROM|Win32"
if errorlevel 1 goto fail

B:\Protect\MapOrder.exe O:\FPSuperRelease\ArmA.map
if errorlevel 1 goto fail
B:\Protect\MapOrder.exe O:\ArmASecuROM\ArmA.map
if errorlevel 1 goto fail

del O:\FPSuperRelease\ArmA.exe
del O:\ArmASecuROM\ArmA.exe

"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Poseidon\ArmA2.sln /build /prj=lib2005 /cfg="Retail|Win32,Retail SecuROM|Win32,Retail Server|Win32"
if errorlevel 1 goto fail

rem SetCRC is now integrated in the project as post-build step

c:\bis\kecal\kecal.exe %1 Arma Retail build OK, protecting

rem Get the build version without incrementing it
echo Build version retrieving...
w:
cd w:\c\Poseidon\lib
b:\buildNo\buildNo -n buildNo.h
set build=%errorlevel%

rem bin.pbo packing and moving to u:\mapfiles\arma
echo Packing of bin.pbo and moving into u:\mapfiles\arma folder...
b:\ssMan\ssMan.exe \\CORE1\vssdatap\cfg\srcsafe.ini $/Fp/Bin
b:\cfgconvert\cfgConvert -bin -dst o:\fp\bin\config.bin o:\fp\bin\config.cpp
del /q /f o:\fp\bin\*.*pp
del /q /f o:\fp\bin\*.h
b:\filebank\filebank -exclude b:\FileBank\exclude.lst -property prefix=bin o:\fp\bin
xcopy o:\fp\bin.* U:\mapfiles\ArmA\%build%\Bin\ /Y /R
b:\security\dsSignFile.exe c:\bis\security\bi.biprivatekey U:\mapfiles\ArmA\%build%\Bin\bin.pbo

echo Distributing...
call W:\C\Poseidon\lib\Tools\fp\putSR.bat 
if errorlevel 1 goto distFail

c:\bis\kecal\kecal.exe %1 Arma Retail has been distributed
goto end

:fail
c:\bis\kecal\kecal.exe %1 Building of Arma Retail failed
goto end

:distFail
c:\bis\kecal\kecal.exe %1 Distribution of Arma Retail failed

:end