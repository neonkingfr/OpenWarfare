@echo off

set batchDir=%~dp0
set jdkbin="C:\Program Files\Java\jdk1.7.0_03\bin"
set JNIScripting=O:\TakeOnH\JNIScripting
set genSources=O:\TakeOnH\JNI\src
set dest=O:\TakeOnH

o:
cd %JNIScripting%\src\com\bistudio\JNIScripting
xcopy %genSources%\*.* %JNIScripting%\src\com\bistudio\JNIScripting\ /y
md %JNIScripting%\build
md %JNIScripting%\build\classes

echo JNIScripting build started... > %batchDir%\JNIBuildResult.txt
%jdkbin%\javac.exe -sourcepath %JNIScripting%\src -d %JNIScripting%\build\classes Vector3.java ClassReloader.java Entity.java FilesAccess.java FSMBase.java NativeObject.java PerfTest.java RVDirectories.java RVEngine.java Serialization.java GUI.java Logging.java >> %batchDir%\JNIBuildResult.txt
if errorlevel 1 goto compileFailed
echo ...build finished successfully >> %batchDir%\JNIBuildResult.txt

echo jar started... >> %batchDir%\JNIBuildResult.txt
md %JNIScripting%\dist
%jdkbin%\jar.exe cf %JNIScripting%\dist\JNIScripting.jar -C %JNIScripting%\build\classes com >> %batchDir%\JNIBuildResult.txt
if errorlevel 1 goto jarFailed
echo ...jar finished successfully >> %batchDir%\JNIBuildResult.txt

echo javadoc started... >> %batchDir%\JNIBuildResult.txt
%jdkbin%\javadoc.exe -d %JNIScripting%\dist\javadoc -sourcepath %JNIScripting%\src -splitindex com.bistudio.JNIScripting >> %batchDir%\JNIBuildResult.txt
if errorlevel 1 goto javadocFailed
echo ...javadoc finished successfully >> %batchDir%\JNIBuildResult.txt

xcopy %JNIScripting%\dist\JNIScripting.jar %dest%\jre\lib\ext\ /y

goto end

:compileFailed
echo ...build failed >> %batchDir%\JNIBuildResult.txt
goto end

:jarFailed
echo ...jar failed >> %batchDir%\JNIBuildResult.txt
goto end

:javadocFailed
echo ...javadoc failed >> %batchDir%\JNIBuildResult.txt
goto end


:end

echo Finished with errorlevel %errorlevel%