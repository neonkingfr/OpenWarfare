@echo off

if [%1] == [] goto unknownUser
set user=%1
goto knownUser
:unknownUser
set user=UNKNOWN
:knownUser

c:
cd c:\bis\Build
set dst=w:\Colabo\publish

echo Getting sources...
svn update --non-interactive w:\Colabo\src
svn update --non-interactive %dst%

echo Building...
rem "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\Colabo\src\Colabo\Colabo.sln /build /prj=Colabo /cfg="Release|Any CPU" /log="c:\bis\build\BuildLogIB.txt"
if errorlevel 1 goto fail
rem "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\Colabo\src\Colabo\Colabo.sln /build /prj=ColaboUpdate /cfg="Release|Any CPU" /log="c:\bis\build\BuildLogIB.txt"
if errorlevel 1 goto fail
set build=C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe
%build% W:\Colabo\src\Colabo\Colabo.sln /p:Configuration="Release" /p:Platform="Any CPU" >"c:\bis\build\BuildLogIB.txt"
if errorlevel 1 goto fail

echo Deploying...
set src=W:\Colabo\src\Colabo\bin\Release
copy %src%\Colabo.exe %dst% /y
copy %src%\*.txt %dst% /y
copy %src%\*.dll %dst% /y
copy %src%\*.svnExe %dst% /y
mkdir %dst%\af
copy %src%\af\*.dll %dst%\af /y

set src2=W:\Colabo\src\ColaboUpdate\bin\Release
copy %src2%\ColaboUpdate.exe %dst% /y

svn add %dst%\*.*
svn commit --non-interactive -m "Colabo Distribution" %dst%

FOR /F %%i in (%dst%/revision.txt) do set revision=%%i

c:\bis\kecal\kecal.exe %user% (Build by %user%) Colabo (revision %revision%) has been distributed.
goto end

:fail
c:\bis\kecal\kecal.exe %user% (Build by %user%) Building of Colabo (revision %revision%) failed! file:///c:\bis\build\BuildLogIB.txt
goto :end

:failDeploy
c:\bis\kecal\kecal.exe %user% (Build by %user%) Deploying of Colabo (revision %revision%) failed!
goto :end

:end