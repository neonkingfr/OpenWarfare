:: bat for building ARMA2GDF.DLL for MS Games For Windows


set user=%1
if [%1] == [bxbx] set user=Kuba

@ECHO OFF
ECHO building ARMA2GDF

if exist w:\ARMA2GDF goto :update

:: check out
 svn checkout https://mm.xdev.bistudio.com/svn/Setup/GDF w:\ARMA2GDF
goto updateDone

:update
 svn update --non-interactive  w:\ARMA2GDF
:updateDone


"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\ARMA2GDF\ARMA2GDF.sln /build /prj=ARMA2GDF /cfg="Release|Win32" /log="c:\bis\build\BuildLogIB.txt"
if errorlevel 1 goto buildfail

IF not exist x:\ca\GDF MD x:\ca\GDF
Xcopy w:\ARMA2GDF\dll\ARMA2GDF.dll x:\ca\GDF /Y

copy c:\bis\build\BuildLogIB.txt X:\verHistory\buildInfo\GDFbuildInfo.txt /Y
if not [%1] == [] c:\bis\kecal\kecal.exe %user% Build of ARMA2GDF.DLL has been distributed: Destination: x:\ca\GDF report: file:///X:/verHistory/buildInfo/GDFbuildInfo.txt

goto end

:buildfail
copy c:\bis\build\BuildLogIB.txt X:\verHistory\buildInfo\GDFbuildInfo.txt /Y
if not [%1] == [] c:\bis\kecal\kecal.exe %user% Build of ARMA2GDF.DLL FAIL file:///X:/verHistory/buildInfo//GDFbuildInfo.txt

:end