@echo off
REM %1 is for name of computer
REM Parameter regular must be in %2
REM Configuration type must be in %3
REM Wanted configuration (see contents of .sln) must be in %4

set dateBuild=%date%
set timeBuild=%time%

REM echo Executed: %0 %1 %2 %3 %4 %5 %6
echo %dateBuild% %timeBuild% %0 %1 %2 %3 %4 %5 %6 >> c:\bis\build\X360buildLog.txt

set buildType=NotRegular
if [%2] == [regular] set buildType=regular
if [%2] == [preregular] set buildType=preregular
if [%2] == [REGULAR] set buildType=regular
if [%2] == [PREREGULAR] set buildType=preregular


if [%3] == [] goto :default
if [%3] == [XBOXFull] goto :XBOXFull
if [%3] == [XBOXDemo] goto :XBOXDemo
if [%3] == [PGInstrument] goto :PGInstrument


:default
:XBOXRelease
set buildVer=XBOXRelease
set buildCfg="Release|Xbox 360,X360ShaderGen|Win32"
set buildDir=o:\ArmA\
set outputDir=X:\fpX360
goto :start

:PGInstrument
set buildVer=XBOXRelease PGInstrument
set buildCfg="Retail PGInstrument|Xbox 360,Release PGInstrument|Xbox 360,X360ShaderGen|Win32"
REM set buildCfg="Release PGInstrument|Xbox 360"
set buildDir=o:\ArmA\
set outputDir=X:\fpX360
goto :start


:XBOXFull
set buildVer=XBOXFull
set buildCfg="Retail|Xbox 360,X360ShaderGen|Win32"
set buildDir=o:\ArmA\
set outputDir=X:\fpX360
goto :start

:XBOXDemo
set buildVer=XBOXDemo
set buildCfg="Retail Demo|Xbox 360,X360ShaderGen|Win32"
set buildDir=o:\ArmA\
set outputDir=X:\verhistory\test\fpX360
goto :start

:start

if [%1] == [] goto unknownUser
set user=%1

if [%1] == [bxbx] set user=Kuba

goto knownUser
:unknownUser
set user=UNKNOWN
:knownUser


echo ----------------------------------------------------------------------
echo -- X360 Arma2 (%buildVer%) build ------------------------------
echo ----------------------------------------%dateBuild% %timeBuild%---
echo initiated by: %user% (%buildType% build)
echo parameters:
echo   - buildVer: %buildVer%
echo   - buildCfg: %buildCfg%
echo   - buildDir: %buildDir%
echo   - outputDir: %outputDir%
echo ----------------------------------------------------------------------




c:
cd c:\bis\Build

echo Getting sources...
call GetSources.bat
call W:\C\Poseidon\lib\Tools\ArmA\updCfg.bat

REM .csv depricated, localization db is used - must be done before autotest
del /q /f o:\Arma\bin\*.csv 2> nul

REM Deleting old XBOX files...
del /q /f o:\arma\XArma_*.* 2>nul

echo Storing history...
call dbg.bat %0 Storing history 2>nul
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/Poseidon -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/El -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/Es -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/RV -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/extern  -svn
call c:\Bis\bedar\storeHistory.bat empty https://dev.bistudio.com/svn/pgm/trunk/PhysicalLibs -svn

REM frequent X360 verhistory (each build)
md o:\arma\verhistoryFX360 2>nul
md o:\arma\verhistoryFX360\new 2>nul
md o:\arma\verhistoryFX360\output 2>nul
xcopy c:\bis\bedar\_output\*.history o:\arma\verhistoryFX360\new /D /Y /Q
echo --> o:\arma\verhistoryFX360\output\X360buildInfo3.txt
echo -- SUCCESSFUL BUILD >> o:\arma\verhistoryFX360\output\X360buildInfo3.txt
echo -->> o:\arma\verhistoryFX360\output\X360buildInfo3.txt

md o:\arma\verhistoryFX360\output 2>nul
echo =======================================================================> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo ---------------------------------------------------------------------->> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo -- X360 Arma2 (%buildVer%) build ------------------------------>> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo ----------------------------------------%dateBuild% %timeBuild%--->> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo initiated by: %user% (%buildType% build)>> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo parameters:>> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo   - buildVer: %buildVer%>> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo   - buildCfg: %buildCfg%>> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo   - buildDir: %buildDir%>> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo   - outputDir: %outputDir%>> o:\arma\verhistoryFX360\output\X360buildInfo.txt
echo ---------------------------------------------------------------------->> o:\arma\verhistoryFX360\output\X360buildInfo.txt

echo Deriving differences...
c:\bis\bedar\diffHistoryDirsFX360.pl

echo Aggregating differences to output...
c:\bis\bedar\diffAggregateToOutputFX360.pl


echo Building %buildCfg%...
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" w:\c\Poseidon\ArmA2.sln /build /prj=lib2005 /cfg=%buildCfg% /log="c:\bis\build\X360BuildLogIB.txt" 2>nul
if errorlevel 1 goto fail


echo Distributing...
call W:\C\Poseidon\lib\Tools\ArmAX360\putBin.bat %1

c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user%) ArmA X360 has been distributed. file:///X:/verHistory/buildInfo/X360buildInfo.txt
type "c:\BIS\Build\X360BuildLogIB.txt" > x:\verhistory\buildInfo\X360buildLogIB.txt




if [%2] == [regular] goto :XBOXtestRegular
if [%2] == [REGULAR] goto :XBOXtestRegular

if [%2] == [preregular] goto :XBOXprePregular
if [%2] == [PREREGULAR] goto :XBOXprePregular



REM Unit test not used
REM B:\RemEx\ExeCln.exe Builder X360AutotestUnit %1  

goto :XBOXtestEND

:XBOXtestRegular
B:\RemEx\ExeCln.exe Builder X360AutotestRegular noreply

:XBOXtestEND

goto end

:XBOXprePregular

echo Executing profiling step on Builder machine... (Builder will then trigger regular build via remEx.)
B:\RemEx\ExeCln.exe Builder X360_MAKE_PG NOREPLY REGULAR
goto end

:fail
echo --FAILED! Building failed. %dateBuild% %timeBuild% %1 %2 %3 %4 %5 %6 >> c:\bis\build\X360buildLog.txt

echo --> o:\arma\verhistoryFX360\output\X360buildInfo3.txt
echo -- BUILD FAILED! %date% %time% %1 %2 %3 %4 %5 %6 >> o:\arma\verhistoryFX360\output\X360buildInfo3.txt
echo -->> o:\arma\verhistoryFX360\output\X360buildInfo3.txt

c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user%) Building of X360 Arma 2 failed. file:///X:/verHistory/buildInfo/X360buildInfo.txt file:///X:/verHistory/buildInfo/X360buildLogIB.txt
type "c:\BIS\Build\X360BuildLogIB.txt" > x:\verhistory\buildInfo\X360buildLogIB.txt

if [%buildType%] == [regular] C:\BIS\blat\toNewsFile.bat "Regular build of X360 arma failed!" "c:\BIS\Build\X360BuildLogIB.txt" bistudio.engine.task-log
if [%buildType%] == [preregular] C:\BIS\blat\toNewsFile.bat "Regular build (pre-regular) of X360 arma failed!" "c:\BIS\Build\X360BuildLogIB.txt" bistudio.engine.task-log


goto afterBinarization

:end

echo Building the Binarization...
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\c\Poseidon\binarize\binarize.sln /build /prj=binarize /cfg="Release|Win32" /log="c:\bis\build\X360BuildLogIB.txt" 2>nul
if errorlevel 1 c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user%) Building of binarization failed. file:///X:/verHistory/buildInfo/X360buildInfo.txt file:///X:/verHistory/buildInfo/X360buildLogIB.txt
type "c:\BIS\Build\X360BuildLogIB.txt" >> x:\verhistory\buildInfo\X360buildLogIB.txt



:afterBinarization

echo Sending build notification...
md x:\verHistory\buildInfo 2>nul
type o:\arma\verHistoryFX360\output\X360buildInfo3.txt >> o:\arma\verHistoryFX360\output\X360buildInfo.txt
type o:\arma\verHistoryFX360\output\X360buildInfo2.txt >> o:\arma\verHistoryFX360\output\X360buildInfo.txt
xcopy "o:\arma\verHistoryFX360\output\X360buildInfo.txt" x:\verHistory\buildInfo /D /Y /R
type "o:\arma\verHistoryFX360\output\X360buildInfo.txt" >> o:\arma\verHistoryFX360\output\X360buildInfo_history.txt
xcopy "o:\arma\verHistoryFX360\output\X360buildInfo_history.txt" x:\verHistory\buildInfo /D /Y /R





