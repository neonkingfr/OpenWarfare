@echo off
REM %1 is for name of computer
REM Parameter regular must be in %2
REM Configuration type must be in %3
REM Wanted configuration (see contents of .sln) must be in %4

REM Self update and restart  if new version in SVN
@FOR /F "delims=" %%i in ('"svn diff -r HEAD:BASE %~f0| FIND /I "%~nx0""') do svn update %%~dpi && call %~f0 %1 %2 %3 %4 %5 %6 %7 %8 %9 && goto:eof


echo Executed: %0 %1 %2 %3 %4 %5 %6
echo %date% %time% %0 %1 %2 %3 %4 %5 %6 >> c:\bis\build\buildLogTools.txt

set buildType=NotRegular
if /I [%2] == [regular] set buildType=Regular

if [%1] == [] goto set user=Unknown
set user=%1
if [%1] == [bxbx] set user=Kuba


if [%3] == [] goto :default
if /I [%3] == [Default] goto :default
if /I [%3] == [FSMEditor] goto :FSMEditor
if /I [%3] == [newsreader] goto :NewsReader
if /I [%3] == [Oxygen] goto :Oxygen
if /I [%3] == [setup] goto :setup

:default

echo unknowk build request %3
exit /B

:NewsReader
REM buildVer is also .exe name!
set buildVer=NewsReader
set buildCfg="Release|Win32"
set outputDir=o:\NewsReader\
set outputExe=%outputDir%%buildVer%
set solution="w:\c\AITools\GraphTest\GraphTest.sln"
set project=GraphTest

goto :start

:FSMEditor
REM buildVer is also .exe name!
set buildVer=FSMEditor
set buildCfg="Release|Win32"
set outputDir=o:\FSMEditor\
set outputExe=%outputDir%%buildVer%
set solution="w:\c\AITools\GraphTest\GraphTest.sln"
set project=GraphTest
goto :start

:Oxygen
REM buildVer is also .exe name!
set buildVer=Objektiv2
set buildCfg="Release|Win32"
set outputDir=o:\Oxygen\
set outputExe=%outputDir%%buildVer%
set solution="w:\C\Projects\Objektiv2\Objektiv2.sln"
set project=Objektiv2
goto :start



:setup
call c:\bis\Build\GetSources.bat
set solution="W:\C_Branch\Poseidon\Arrowhead\Setup\Setup2010.sln"
set buildCfg="Release|Win32"
set buildCfgSteam="Release Steam DLC|Win32"

set project=setup
MD W:\C_Branch\Poseidon\Arrowhead\Setup\Setup\ReleaseSteam
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /build /prj=%project% /cfg=%buildCfgSteam%  /log="c:\bis\build\BuildLogIBtools.txt"
if errorlevel 1 goto buildsetupfail

set project=setup
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /build /prj=%project% /cfg=%buildCfg%  /log="c:\bis\build\BuildLogIBtools.txt"
if errorlevel 1 goto buildsetupfail

set project=autorun
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /build /prj=%project% /cfg=%buildCfg%  /log="c:\bis\build\BuildLogIBtools.txt"
if errorlevel 1 goto buildsetupfail

set project=uninstall
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /build /prj=%project% /cfg=%buildCfg%  /log="c:\bis\build\BuildLogIBtools.txt"
if errorlevel 1 goto buildsetupfail

set solution="w:\c\Drobnosti\SetupConvert\SetupConvert.sln"
set project=SetupConvert
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /build /prj=%project% /cfg=%buildCfg%  /log="c:\bis\build\BuildLogIBtools.txt"
if errorlevel 1 goto buildsetupfail

call c:\BIS\sign\sign.bat W:\C_Branch\Poseidon\Arrowhead\Setup\AutoRun\Release\AutoRun.exe
call c:\BIS\sign\sign.bat W:\C_Branch\Poseidon\Arrowhead\Setup\Setup\Release\Setup.exe
call c:\BIS\sign\sign.bat "W:\C_Branch\Poseidon\Arrowhead\Setup\Setup\ReleaseSteam\datacachepreprocessor.exe"
call c:\BIS\sign\sign.bat W:\C_Branch\Poseidon\Arrowhead\Setup\UnInstall\Release\UnInstall.exe

c:\bis\kecal\kecal.exe %user% Building of (setup,datacachepreprocessor,uninstall,autorun,setupconvert) done!

MD o:\setup
copy /Y W:\C_Branch\Poseidon\Arrowhead\Setup\AutoRun\Release\AutoRun.exe o:\setup
copy /Y W:\C_Branch\Poseidon\Arrowhead\Setup\Setup\Release\Setup.exe o:\setup
copy /Y W:\C_Branch\Poseidon\Arrowhead\Setup\Setup\ReleaseSteam\datacachepreprocessor.exe o:\setup
copy /Y W:\C_Branch\Poseidon\Arrowhead\Setup\UnInstall\Release\UnInstall.exe o:\setup
copy /Y w:\c\Drobnosti\SetupConvert\Release\SetupConvert.exe o:\setup

MD U:\Tools\Setup\AB_Build

del U:\Tools\Setup\AB_Build\"setup_%REVISION%.7z" /f /q
b:\7zip\7za.exe a U:\Tools\Setup\AB_Build\"setup_%REVISION%.7z" o:\setup\AutoRun.exe o:\setup\UnInstall.exe o:\setup\Setup.exe o:\setup\datacachepreprocessor.exe o:\setup\SetupConvert.exe


echo build of setup is done.
exit /B

:buildsetupfail
REM if /I [%2] == [regular] C:\BIS\blat\toNewsFile.bat "Regular build of %project% failed!" "c:\bis\build\BuildLogIB.txt" bistudio.engine.task-log

copy /Y "c:\bis\build\BuildLogIBtools.txt" "X:\verHistory\buildInfo\buildSetupLogIB.txt"
c:\bis\kecal\kecal.exe %user% Building of %project% failed! file:///X:/verHistory/buildInfo/buildSetupLogIB.txt

echo build of setup failed.
exit /B






:start
md %outputDir% 2>nul
REM md %outputDir%\dta 2>nul
REM md %buildDir% 2>nul
REM md %buildDir%\bin 2>nul





set dateBuild=%date%
set timeBuild=%time%

if [%1] == [] goto unknownUser
set user=%1
if /I [%1] == [bxbx] set user=Kuba
goto knownUser
:unknownUser
set user=UNKNOWN
:knownUser

echo ----------------------------------------------------------------------
echo -- Tools (%buildVer%) build ----------------------------------
echo ----------------------------------------%dateBuild% %timeBuild%---
echo initiated by: %user% (%buildType% build)
echo parameters:
echo   - buildVer: %buildVer%
echo   - solution: %solution%
echo   - project: %project%
echo   - buildCfg: %buildCfg%
echo   - buildDir: %buildDir%
echo   - outputExe: %outputExe%
echo   - outputDir: %outputDir%
echo   - protection: %protection%
echo   - maporderProtection: %mapOrderProtection%
echo   - autotest: %autotest%
echo   - SVN Revision: %SVNrevisionNo% (archive directory name: %SVNrevisionNo%)
echo   - build switch: /%buildXoreaxSwitch%
echo ----------------------------------------------------------------------


if /I [%buildVer%] == [NewsReader] goto :NewsReaderBuild

echo Getting sources...
call c:\bis\Build\GetSources.bat

REM echo Storing history...
REM call dbg.bat %0 Storing history 2>nul
REM call c:\bis\bedar\storeHistory.bat \\New_server\vss\Pgm\srcsafe.ini $/Poseidon
REM call c:\bis\bedar\storeHistory.bat \\New_server\vss\Pgm\srcsafe.ini $/El
REM call c:\bis\bedar\storeHistory.bat \\New_server\vss\Pgm\srcsafe.ini $/Es
REM call c:\bis\bedar\storeHistory.bat \\New_server\vss\Pgm\srcsafe.ini $/rv
REM call c:\bis\bedar\storeHistory.bat \\New_server\vss\Pgm\srcsafe.ini $/extern
REM call c:\bis\bedar\storeHistory.bat \\New_server\vss\Pgm\srcsafe.ini $/physicallibs


REM frequent verhistory (each build)
REM md %outputDir%verhistoryF 2>nul
REM md o:\arma\verhistoryF\new 2>nul
REM md o:\arma\verhistoryF\output 2>nul
REM xcopy c:\bis\bedar\_output\*.history o:\arma\verhistoryF\new /D /Y /Q

REM echo --> o:\arma\verhistoryF\output\buildInfo3.txt
REM echo -- SUCCESSFUL BUILD >> o:\arma\verhistoryF\output\buildInfo3.txt
REM echo -->> o:\arma\verhistoryF\output\buildInfo3.txt

REM md o:\arma\verHistoryF\output 2>nul
REM echo =======================================================================> o:\arma\verHistoryF\output\buildInfo.txt
REM echo ---------------------------------------------------------------------->> o:\arma\verHistoryF\output\buildInfo.txt
REM echo -- Arma2 (%buildVer%) build ---------------------------------->> o:\arma\verHistoryF\output\buildInfo.txt
REM echo ----------------------------------------%dateBuild% %timeBuild%--->> o:\arma\verHistoryF\output\buildInfo.txt
REM echo initiated from: %user% (%buildType% build)>> o:\arma\verHistoryF\output\buildInfo.txt
REM echo parameters:>> o:\arma\verHistoryF\output\buildInfo.txt
REM echo   - buildVer: %buildVer%>> o:\arma\verHistoryF\output\buildInfo.txt
REM echo   - buildCfg: %buildCfg%>> o:\arma\verHistoryF\output\buildInfo.txt
REM echo   - buildDir: %buildDir%>> o:\arma\verHistoryF\output\buildInfo.txt
REM echo   - outputDir: %outputDir%>> o:\arma\verHistoryF\output\buildInfo.txt
REM echo ---------------------------------------------------------------------->> o:\arma\verHistoryF\output\buildInfo.txt

REM echo Deriving differences...
REM c:\bis\bedar\diffHistoryDirsF.pl

REM echo Aggregating differences to output...
REM c:\bis\bedar\diffAggregateToOutputF.pl


REM echo Deleting %buildVer%.exe, ren %outputExe%Unprotected.exe to %outputExe%.exe...
REM del /q /f %outputExe%.exe 2>nul
REM ren %outputExe%Unprotected.exe %outputExe%.exe



if /I [%buildVer%] == [NewsReader] goto :NewsReaderBuild

echo Building (%solution%)...
echo "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /build /prj=%project% /cfg=%buildCfg%  /log="c:\bis\build\BuildLogIBtools.txt"
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" %solution% /build /prj=%project% /cfg=%buildCfg%  /log="c:\bis\build\BuildLogIBtools.txt"
if errorlevel 1 goto buildfail

REM TODO: move to post-build step?
copy w:\c\AITools\GraphTest\Release\FSMEditor.exe o:\FSMEditor\FSMEditorUnprotected.exe
goto :buildEnd

:NewsReaderBuild
copy u:\Tools\NewsReader\dist\NewsReader.exe o:\NewsReader\NewsReaderUnprotected.exe
goto :buildEnd

:buildEnd


echo Applying protection (w:\c\Poseidon\lib\Tools\ArmA\protectInternal.bat)...
REM Rename to unprotected
REM (done before) del /q /f o:\arma\ArmA2IntUnprotected.exe 2>nul
REM ren %outputExe%.exe %outputExe%Unprotected.exe

REM Apply protection
call dbg.bat %0 call w:\c\Poseidon\lib\Tools\ArmA\protectInternal.bat %user% %buildVer%
dir %outputDir% > %outputDir%dir_before_protect.txt
call w:\c\Poseidon\lib\Tools\ArmA\protectInternal.bat %user% %buildVer%

REM Copy history files right to o:\arma\verhistory\new (not thru x:\verhistory)
REM md o:\arma 2>nul
REM md o:\arma\verhistory 2>nul
REM md o:\arma\verhistory\new 2>nul
REM xcopy c:\bis\bedar\_output\*.history o:\arma\verhistory\new /D /Y /Q

REM rem Deleting FPS report file
REM del /q /f o:\arma\verhistory\output\bedar_fps.rpt 2>nul

REM REM .csv depricated, localization db is used - must be done before autotest
REM del /q /f o:\Arma\bin\*.csv 2>nul

REM call dbg.bat %0 call W:\C\Poseidon\lib\Tools\ArmA\autotest.bat %2 2>nul
REM call W:\C\Poseidon\lib\Tools\ArmA\autotest.bat %2
REM if errorlevel 1 goto autotestfail

REM if /I [%2] == [regular] goto regular
goto distribute

REM :regular
REM call dbg.bat %0 call W:\C\Poseidon\lib\Tools\ArmA\autotestFunct.bat %2 2>nul
REM call W:\C\Poseidon\lib\Tools\ArmA\autotestFunct.bat %2
REM set errlvl=%errorlevel%

REM deriving fps from debug.log - stored by funct. autotest mission _TEST_FPS
REM c:\bis\bedar\deriveFPS.pl "C:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2\debug.log" o:\arma\verhistory\output\bedar_fps.rpt


REM call c:\bis\bedar\seterrorlevel.bat %errlvl%
REM if errorlevel 1 goto autotestfail



:distribute
echo Distributing...

REM Bin distribution
REM call W:\C\Poseidon\lib\Tools\ArmA\putBinSimple.bat %1 noparam %outputDir% %outputExe%



copy %outputExe%.exe u:\Tools\%buildVer%\dist\%buildVer%Protected.exe


if not /I [%2] == [regular] goto afterVersionArchiving

echo Archiving version...
md u:\mapfiles\%buildVer% 2> nul
set mapdir=u:\mapfiles\%buildVer%\
rmdir /S /Q "%mapdir%recent-14"
rename "%mapdir%recent-13" "recent-14"
rename "%mapdir%recent-12" "recent-13"
rename "%mapdir%recent-11" "recent-12"
rename "%mapdir%recent-10" "recent-11"
rename "%mapdir%recent-9" "recent-10"
rename "%mapdir%recent-8" "recent-9"
rename "%mapdir%recent-7" "recent-8"
rename "%mapdir%recent-6" "recent-7"
rename "%mapdir%recent-5" "recent-6"
rename "%mapdir%recent-4" "recent-5"
rename "%mapdir%recent-3" "recent-4"
rename "%mapdir%recent-2" "recent-3"
rename "%mapdir%recent-1" "recent-2"
rename "%mapdir%recent" "recent-1"
mkdir "%mapdir%recent"

copy "%outputExe%*.*" "%mapdir%recent"

:afterVersionArchiving

REM c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user%) %buildVer% has been distributed. file:///X:/verHistory/buildInfo/buildInfo.txt
c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user%) %buildVer% has been distributed.
REM call c:\bis\bedar\sendHistoryAB2.bat

goto :totalend


:buildfail
REM echo --FAILED! Build failed. %date% %time% %1 %2 %3 %4 %5 %6 >> c:\bis\build\buildLog.txt
REM echo --> o:\arma\verhistoryF\output\buildInfo3.txt
REM echo -- BUILD FAILED! %date% %time% %1 %2 %3 %4 %5 %6 >> o:\arma\verhistoryF\output\buildInfo3.txt
REM echo -->> o:\arma\verhistoryF\output\buildInfo3.txt

REM if /I [%2] == [regular] C:\BIS\blat\toNewsFile.bat "Regular build of arma failed!" "c:\bis\build\BuildLogIB.txt" bistudio.engine.task-log
call dbg.bat %0 "Building of %buildVer% (%solution%) failed." 2>nul

c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user%) Building of %buildVer% failed! (%0 %1 %2 %3 %4 %5 %6) file:///X:/verHistory/buildInfo/buildInfo.txt file:///X:/verHistory/buildInfo/buildLogIBtools.txt
type "c:\BIS\Build\BuildLogIBtools.txt" > x:\verhistory\buildInfo\buildLogIBtools.txt
goto afterBinarization

REM :autotestfail
REM echo --FAILED! Autotesting failed. %date% %time% %1 %2 %3 %4 %5 %6 >> c:\bis\build\buildLog.txt
REM echo --> o:\arma\verhistoryF\output\buildInfo3.txt
REM echo -- AUTOTEST FAILED! %date% %time% %1 %2 %3 %4 %5 %6 >> o:\arma\verhistoryF\output\buildInfo3.txt
REM call dbg.bat %0 :autotestfail - errorlevel: %errorlevel% 2>nul
REM echo Autotest Failed... (errorlevel: %errorlevel%)

REM REM News message sent in autotest.bat / autotestFunct.bat
REM c:\bis\kecal\kecal.exe %user% (Build %dateBuild% %timeBuild% by %user%) Autotest of arma failed! file:///X:/verHistory/buildInfo/buildInfo.txt file:///X:/verHistory/AB2_rpt/ArmA2Int.RPT
REM call c:\bis\bedar\autotestresult\autotestresult %1
REM goto afterBinarization
REM :end


:afterBinarization

if /I [%2] == [regular] goto regularafterbin
goto totalafterbin


:regularafterbin
call dbg.bat %0 regularafterbin 2>nul
REM Create differences btw old new report, copy difference file to o:\arma\verHistory
call c:\bis\bedar\filter\extract_rpt.bat

REM Upload .EXE to https://beta2.bistudio.com/files/a2setup/
call c:\bis\build\upload-beta2_bistudio_com.bat > c:\bis\build\upload.log

REM Upload dta/*.pbo to https://beta2.bistudio.com/files/a2setup/dta <- (RemEx call to builder)
call b:\build\sendBin.bat

REM derive differences in history files, aggregate for output, send to NG
call c:\bis\bedar\correlation.bat
call c:\bis\bedar\sendNG.bat

call dbg.bat %0 regularafterbin -end 2>nul

:totalafterbin
REM echo Sending .rpt to x:\verhistory\ab2_rpt
REM xcopy "c:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2\arma2int*.rpt" x:\verhistory\ab2_rpt\ /D /Y /R

REM echo Sending build notification...
REM md x:\verHistory\buildInfo 2>nul
REM type o:\arma\verHistoryF\output\buildInfo3.txt >> o:\arma\verHistoryF\output\buildInfo.txt
REM type o:\arma\verHistoryF\output\buildInfo2.txt >> o:\arma\verHistoryF\output\buildInfo.txt
REM xcopy "o:\arma\verHistoryF\output\buildInfo.txt" x:\verHistory\buildInfo /D /Y /R
REM type "o:\arma\verHistoryF\output\buildInfo.txt" >> x:\verHistory\buildInfo\buildInfo_history.txt


:totalend
echo End. Have a nice day!