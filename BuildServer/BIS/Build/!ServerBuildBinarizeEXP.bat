@echo off

if [%1] == [] goto unknownUser
set user=%1
goto knownUser
:unknownUser
set user=UNKNOWN
:knownUser

c:
cd c:\bis\Build

call dbg.bat %0 2>nul
echo Getting sources...
call GetSources.bat

echo Getting configs...
call W:\C_Branch\Poseidon\Arrowhead\lib\Tools\ArmA\updCfg.bat

call dbg.bat %0 "c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\C_Branch\Poseidon\Arrowhead\binarize\binarize.sln /build /prj=binarize /cfg="Release|Win32" /log="c:\bis\build\BuildLogIB.txt" 2>nul
echo Building...
"c:\Program Files\Xoreax\IncrediBuild\BuildConsole.exe" W:\C_Branch\Poseidon\Arrowhead\binarize\binarize.sln /build /prj=binarize /cfg="Release|Win32" /log="c:\bis\build\BuildLogIB.txt"
if errorlevel 1 goto fail

REM call dbg.bat %0 Distributing... 2>nul
REM echo Distributing...
REM call dbg.bat %0 call W:\C_Branch\Poseidon\Arrowhead\binarize\putFP2Bin.bat 2>nul
REM call W:\C_Branch\Poseidon\Arrowhead\binarize\putFP2Bin.bat
REM call dbg.bat %0 ---returned with errorlevel: %errorlevel% 2>nul
REM if errorlevel 1 goto failDeploy

echo Deploying...
call dbg.bat %0 call W:\C_Branch\Poseidon\Arrowhead\binarize\deploy.bat 2>nul
call W:\C_Branch\Poseidon\Arrowhead\binarize\deploy.bat
call dbg.bat %0 ---returned with errorlevel: %errorlevel% 2>nul
if errorlevel 1 goto failDeploy

c:\bis\kecal\kecal.exe %user% (Build by %user%) Binarize EXP has been distributed.
goto end

:fail
call dbg.bat %0 "Building EXP of binarization failed" 2>nul
c:\bis\kecal\kecal.exe %user% (Build by %user%) Building of Binarize failed! file:///X:/verHistory/buildInfo/buildLogIB.txt
REM if /I [%2] == [regular] C:\BIS\blat\toNews.bat "Building EXP of binarization failed" "Building of binarization EXP failed" bistudio.engine.task-log
if /I [%2] == [regular] C:\BIS\blat\toNewsFile.bat "Building EXP of binarization failed" "c:\bis\build\BuildLogIB.txt" bistudio.engine.task-log
type "c:\BIS\Build\BuildLogIB.txt" > x:\verhistory\buildInfo\buildLogIB.txt

goto :end

:failDeploy
call dbg.bat %0 "Deploying of binarization EXP failed" 2>nul
c:\bis\kecal\kecal.exe %user% (Build by %user%) Deploying of Binarize EXP failed!
if /I [%2] == [regular] C:\BIS\blat\toNews.bat "Deploying of binarization EXP failed" "Binarize EXP deploy failed! Msg sent from: C:\bis\build\!ServerBuildBinarize.bat." bistudio.fp.ca.bedar
goto :end

:end