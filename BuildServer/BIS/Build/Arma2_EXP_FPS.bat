@echo off
set user=%1
set dateBuild=%date%
set timeBuild=%time%

echo ----------------------------------------------------------------------
echo -- Arma2 FPS measurement ----------------------------------
echo ----------------------------------------%dateBuild% %timeBuild%---
echo initiated by: %user%


REM call W:\C_Branch\Poseidon\Arrowhead\lib\Tools\ArmA\autotestFPSveh.bat
REM exit /B

call W:\C_Branch\Poseidon\Arrowhead\lib\Tools\ArmA\autotestFPSpc.bat
copy /Y /B O:\Arma2_Expansion\verhistory\output\bedar_fpsPerftest.rpt x:\verHistory\AB2_fps\bedar_fpsPerftest_EXP.rpt

c:\bis\kecal\kecal.exe %user% (FPS measurement %dateBuild% %timeBuild% by %user%) Arma2 EXP FPS measurement complete. file:///X:/verHistory/AB2_fps/bedar_fpsPerftest_EXP.rpt
call c:\bis\bedar\sendNG.bat Expansion