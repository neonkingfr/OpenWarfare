import os
from tempfile import NamedTemporaryFile

from helpers import printError

class BuildError(Exception):
	def __init__(self, message, info, logFile, value):
		#raise buildError.BuildError("Some error", "Info text", "Log file Path", errorlevel)
		self.message = message
		self.info = info
		self.logFile = logFile
		self.value = value
		
		
	def __str__(self):
		return repr(self.message)


	def reportError(self):
		printError("=======================================================================")
		printError("Error message: "+self.message)
		printError(self.info)
		printError("Error value: "+str(self.value))
		printError("=======================================================================")
