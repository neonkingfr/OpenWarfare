import os
from subprocess import call

def createDir(path):
	if not os.path.exists(path):
		os.makedirs(path)


def copy(src, dst):
	call("xcopy /Y \"{}\" \"{}\"".format(src, dst))


def delete(path):
	call("del /Q /F \"{}\" 2>nul".format(path), shell=True)


def rename(src, reldst):
	fullDst = os.path.dirname(src) + "\\" + reldst
	call("del /Q /F \"{}\" 2>nul".format(fullDst), shell=True)
	call("ren \"{}\" \"{}\"".format(src, reldst), shell=True)


def move(src, dst):
	call("move /Y \"{}\" \"{}\"".format(src, dst), shell=True)