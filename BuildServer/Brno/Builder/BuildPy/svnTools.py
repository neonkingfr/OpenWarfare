from subprocess import Popen
from subprocess import PIPE
from re import search

import buildError


class SvnTools:
	def __init__(self):
		self.svnPath = r"c:\Program Files\TortoiseSVN\bin\svn.exe"
		self.author = ""
		self.revNumber = 0


	def update(self, path):
		p = Popen("\"{}\" update --non-interactive \"{}\"".format(self.svnPath, path), stderr=PIPE)

		# communicate() returns a tuple (stdoutdata, stderrdata).
		output, errors = p.communicate()
		errorlevel = p.returncode
		if(errorlevel):
			raise buildError.BuildError("SVN Update FAILED!", errors, "", errorlevel)


	def updateSvnInfo(self, path):
		p = Popen("\"{}\" info \"{}\"".format(self.svnPath, path), stdout=PIPE, stderr=PIPE)
		
		# communicate() returns a tuple (stdoutdata, stderrdata).
		output, errors = p.communicate()
		errorlevel = p.returncode
		if(errorlevel):
			raise buildError.BuildError("SVN Info FAILED!", errors, "", errorlevel)
		
		m = search(r".*Last Changed Author: (?P<author>\w+)[ ^\S\r\n]+Last Changed Rev: (?P<rev>\d+)", output)
		if(m!=None):
			self.author = m.group("author")
			self.revNumber = int(m.group("rev"))

		print(output)