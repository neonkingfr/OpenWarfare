import time
import os.path
from sys import stderr

def formattedHeaderText(productName, timeBuild):
	#tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec, tm_wday, tm_yday, tm_isdst
	text = "   {} build\t{}.{}.{} {}:{}:{}".format(productName,
		timeBuild.tm_mday, timeBuild.tm_mon, timeBuild.tm_year,
		timeBuild.tm_hour, timeBuild.tm_min, timeBuild.tm_sec)
	return text


def formattedDateTime(timeBuild):
	text = "{}.{}.{} {:02d}:{:02d}:{:02d}".format(
		timeBuild.tm_mday, timeBuild.tm_mon, timeBuild.tm_year,
		timeBuild.tm_hour, timeBuild.tm_min, timeBuild.tm_sec)
	return text


def msgInfoBegin(productName, branch, revNumber):
	if(branch):
		text = "{} {} (r. {})".format(productName, branch, revNumber)
	else:
		text = "{} (r. {})".format(productName, revNumber)
	return text


def reportDurationOfBuild(fileName, timeBuild):
	#Get difference between build start and now in seconds
	diff = int(time.time() - time.mktime(timeBuild))

	file = open(fileName, 'w')
	#Write duration to file
	file.write(str(diff))
	file.close()


def formatSecToStr(timeDiff):
	if(timeDiff >= 0):
		minutes = timeDiff / 60
		secs = timeDiff % 60 #The remainder of the division
		return "{} min {} sec".format(int(minutes), int(secs))
	else:
		minutes = (-timeDiff / 60)
		secs = (-timeDiff % 60) #The remainder of the division
		return "-{} min {} sec".format(int(minutes), int(secs))


def getEstimateOfFinish(fileName, timeBuild):
	#Get difference between build start and now in seconds
	diff = int(time.time() - time.mktime(timeBuild))

	if(os.path.exists(fileName)):
		file = open(fileName, 'r')
		file_contents = file.readlines()
		file.close()

		#Read last build time from file and compute ETA
		lastBuildTime = 0
		if(len(file_contents) > 0):
			lastBuildTime = int(file_contents[0])
			lastBuildTime = lastBuildTime - diff

		return formatSecToStr(lastBuildTime)
	else:
		return "Unknown"


def printError(text):
	stderr.write(text)