from subprocess import call

import diskTools
import buildError

class BuildTools:
	def __init__(self):
		self.signTool = r"c:\Bis\sign\sign.bat"
		self.devenv = r"c:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.com"
		self.devenv2005 = r"c:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.com"
		self.incredibuild = r"c:\Program Files (x86)\Xoreax\IncrediBuild\BuildConsole.exe"


	def build(self, name, slnPath, buildCfg, buildLog, devenvNew=True):
		print("Building {}".format(name))

		errorlevel = 0
		diskTools.delete(buildLog)
		if(devenvNew):
			errorlevel = call("\"{}\" \"{}\" /rebuild \"{}\" /Out \"{}\"".format(self.devenv, slnPath, buildCfg, buildLog))
		else:
			errorlevel = call("\"{}\" \"{}\" /rebuild \"{}\" /Out \"{}\"".format(self.devenv2005, slnPath, buildCfg, buildLog))
		
		if(errorlevel):
			raise buildError.BuildError("Build FAILED!", slnPath, buildLog, errorlevel)
		print("Build of {} finished successfully.".format(name))


	def buildIB(self, slnPath):
		print("NOT YET")


	def buildIBPreset(self, slnPath):
		print("NOT YET")