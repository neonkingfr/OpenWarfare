from subprocess import call

import buildError

class SignTools:
	def __init__(self):
		self.signTool = r"c:\Bis\sign\sign.bat"


	def sign(self, path):
		print("Signing file: {}".format(path))
		errorlevel = call("\"{}\" \"{}\"".format(self.signTool, path))
		if(errorlevel):
			raise buildError.BuildError("Sign FAILED!", path, "", errorlevel)