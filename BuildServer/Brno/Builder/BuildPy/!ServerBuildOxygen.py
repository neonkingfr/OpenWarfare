import argparse
import os
from sys import exit
import time
from traceback import format_exc
from glob import glob

import buildError
import svnTools
import signTools
import diskTools
import buildTools
import helpers
from helpers import printError


class BuildScript:
	def __init__(self):
		# Script constants
		self.batchDir = os.getcwd()
		self.batchLog = os.path.splitext(__file__)[0]+".txt"
		self.timeBuild = time.localtime(time.time())
		#tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec, tm_wday, tm_yday, tm_isdst

		# Set product constants
		self.productName = "Oxygen"

		#Product global variables
		self.branch = ""
		self.revNumber = 0

		# Build params
		self.sourcePath = r"c:\Tools\Oxygen\c\projects\Objektiv2"
		self.sourcePathProjects = r"c:\Tools\Oxygen\c\projects"
		self.sourcePathEl = r"c:\Tools\Oxygen\c\El"
		self.sourcePathEs = r"c:\Tools\Oxygen\c\Es"
		self.sourcePathImg = r"c:\Tools\Oxygen\c\img"
		self.toolsln = r"{}\Objektiv2.sln".format(self.sourcePath)
		self.outBuildExePath = r"{}\Bin".format(self.sourcePath)
		self.outBuildPdbPath = r"{}\pdb".format(self.sourcePath)
		self.buildCfg = r"Release Public|Win32"
		self.outputDir = r"c:\O\Tools\{}".format(self.productName)
		self.buildLog = r"{}\BuildLog{}.txt".format(self.batchDir, self.productName)
		self.logsDstPath = r"c:\O\Tools.Logs\{}".format(self.productName)

		# Build constants
		self.deploy = 1


	def run(self):
		parser = argparse.ArgumentParser(description = self.productName+" build script")
		parser.add_argument("-a", "--attach", action='store_true', help="Pause application so it can be attached by VS")
		args = parser.parse_args()

		if(args.attach):
			raw_input("PRESS ENTER TO CONTINUE.")

		#=============================================================================
		# Start script
		#=============================================================================
		print("=======================================================================")
		print(helpers.formattedHeaderText(self.productName, self.timeBuild))
		print("=======================================================================")
		print("parameters:")
		print("  - buildCfg: "+self.buildCfg)
		print("  - outputDir: "+self.outputDir)
		print("=======================================================================")
		print("");

		#=============================================================================
		# Update sources from svn
		#=============================================================================
	
		print("Update sources and get revision number ...")
		svn = svnTools.SvnTools()
		svn.update(self.sourcePathProjects)
		svn.update(self.sourcePathEl)
		svn.update(self.sourcePathEs)
		svn.update(self.sourcePathImg)

		svn.updateSvnInfo(self.sourcePathProjects)
		if(svn.revNumber > self.revNumber):
			self.revNumber = svn.revNumber
		svn.updateSvnInfo(self.sourcePathEl)
		if(svn.revNumber > self.revNumber):
			self.revNumber = svn.revNumber
		svn.updateSvnInfo(self.sourcePathEs)
		if(svn.revNumber > self.revNumber):
			self.revNumber = svn.revNumber
		svn.updateSvnInfo(self.sourcePathImg)
		if(svn.revNumber > self.revNumber):
			self.revNumber = svn.revNumber

		logDirName = "{}-{}.{:02d}.{:02d}-{:02d}.{:02d}".format(self.revNumber, self.timeBuild.tm_year, self.timeBuild.tm_mon, self.timeBuild.tm_mday, self.timeBuild.tm_hour, self.timeBuild.tm_min)
		logsDstFullPath = self.logsDstPath+"\\"+logDirName
		if(self.deploy):
			diskTools.createDir(logsDstFullPath)

		#=============================================================================
		# Build configurations
		#=============================================================================

		build = buildTools.BuildTools()
		build.build(self.productName, self.toolsln, self.buildCfg, self.buildLog, False)

		#=============================================================================
		# Sign app .exe
		#=============================================================================

		files = glob(self.outBuildExePath+r"\*.exe")
		files = files + glob(self.outBuildExePath+r"\*.dll")
		for file in files:
			signTools.SignTools().sign(file)

		#=============================================================================
		# Deploying
		#=============================================================================

		if(self.deploy):
			print("Deploying ...")

			#Delete files in output directory in case something changed
			diskTools.delete(self.outputDir+r"\*.exe")
			diskTools.delete(self.outputDir+r"\*.dll")
			diskTools.copy(self.outBuildExePath+r"\*.exe", self.outputDir)
			diskTools.copy(self.outBuildExePath+r"\*.dll", self.outputDir)

			print("Backup .exe , .dll and .pdb")
			diskTools.copy(self.outBuildExePath+r"\*.exe", logsDstFullPath)
			diskTools.copy(self.outBuildExePath+r"\*.dll", logsDstFullPath)
			diskTools.copy(self.outBuildPdbPath+r"\*.pdb", logsDstFullPath)

		#=============================================================================
		# Report After build info
		#=============================================================================

		helpers.reportDurationOfBuild(self.batchLog, self.timeBuild)

		#=============================================================================
		# End script
		#=============================================================================

		print("");
		print("=======================================================================")
		print("SERVER BUILD "+self.productName+" ENDED")
		print("=======================================================================")
		print("");
		return 0


def main():
	bs = BuildScript()
	try:
		bs.run()
	except buildError.BuildError as e:
		e.reportError()
		exit(e.value)
	except:
		printError("=======================================================================")
		text = format_exc()
		printError(text)
		error = 1
		printError("Exit with errorlevel "+str(error))
		printError("=======================================================================")
		exit(error)

if __name__ == "__main__":
	main()
