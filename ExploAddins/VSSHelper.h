/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// VSSHelper.h: interface for the CVSSHelper class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VSSHELPER_H__264FF49B_81C6_4866_A515_62CC0FAFD006__INCLUDED_)
#define AFX_VSSHELPER_H__264FF49B_81C6_4866_A515_62CC0FAFD006__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#import "SourceSafe6.tlb" no_namespace

#include "RuleProfile.h"

class CVSSHelper
{
	HRESULT hr;
	IVSSDatabasePtr m_objVSSDb;
public:
	//bool SafeAddFile(IVSSItemPtr objItemParent, vss_item* pItem);
	//bool SafeAddFolder(IVSSItemPtr objItemParent, vss_item* pSubFolder, bool bRecursive);
	
	bool Add(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive=false);
	bool Get(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive=false);
	bool CheckOut(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive=false);
	bool CheckIn(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive=false);
	bool UndoCheckOut(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive=false);
	// Set to deleted items
	bool Delete(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive=false);

	bool IsCheckedOut(IVSSItemPtr objItem);  
  //returns true iff current user is checkouting + fill in outName by user name of the checkouter
  bool GetCheckedOuterName(IVSSItemPtr objItem, LPWSTR &outName); 
  bool IsDifferent(IVSSItemPtr objItemParent, vss_item* pItem);

	void RecursiveCmd(int nCmdID, IVSSItemPtr objItemParent, LPCTSTR lpcsParentPath, int nProfileID);

	// Get a temporary version to work
	bool GetTempVersion(IVSSItemPtr objItemParent, vss_item* pItem, LPTSTR lpsFilePath);

	CVSSHelper(LPCTSTR lpcsDatabase, LPCTSTR lpcsUser, LPCTSTR lpcsPwd);
	virtual ~CVSSHelper();

	inline bool IsValidVSS()
	{ return m_objVSSDb!=NULL; }

	// Create if not found
	IVSSItemPtr GetSafeFolder(LPCTSTR lpcsVSSItem, bool bCreateIfNotExist=true);
	IVSSItemPtr GetSafeItem(IVSSItemPtr objItemParent, LPCTSTR lpcsVSSItem);
  LPWSTR GetUserName() {if (m_objVSSDb) return m_objVSSDb->GetUsername(); else return NULL; };
	
	VSSString m_strLastError;
};

#endif // !defined(AFX_VSSHELPER_H__264FF49B_81C6_4866_A515_62CC0FAFD006__INCLUDED_)
