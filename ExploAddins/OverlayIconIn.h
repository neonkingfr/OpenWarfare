/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// OverlayIconIn.h : Declaration of the COverlayIconIn

#ifndef __OVERLAYICONIN_H_
#define __OVERLAYICONIN_H_

#include "resource.h"       // main symbols
#include "OverlayIconBase.h"

/////////////////////////////////////////////////////////////////////////////
// COverlayIconIn

class ATL_NO_VTABLE COverlayIconIn : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<COverlayIconIn, &CLSID_OverlayIconIn>,
	public IOverlayIconIn,
	public IShellIconOverlayIdentifier,
	public COverlayIconBase
{
public:
	COverlayIconIn()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_OVERLAYICONIN)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(COverlayIconIn)
	COM_INTERFACE_ENTRY(IOverlayIconIn)
	COM_INTERFACE_ENTRY(IShellIconOverlayIdentifier)
END_COM_MAP()

public:
	void FinalRelease()
	{
		//MessageBox(NULL, "release", "", MB_OK);
	}

	HRESULT FinalConstruct()
	{
		//MessageBox(NULL, "construct", "", MB_OK);
		return S_OK;
	}

// IOverlayIconIn
public:

	// IShellIconOverlayIdentifier methods
	STDMETHODIMP GetOverlayInfo(LPWSTR pwszIconFile, int cchMax, int *pIndex, DWORD *pdwFlags);
	STDMETHODIMP GetPriority(int *pPriority);	
	STDMETHODIMP IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib);
};

#endif //__OVERLAYICONIN_H_
