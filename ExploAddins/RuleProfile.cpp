/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// RuleProfile.cpp: implementation of the CRuleProfile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RuleProfile.h"

#include "ESSUtils.h"
#include "VSSHelper.h"


bool CFileRule::MatchPattern (const TCHAR *pat, const TCHAR *str)
{
	switch (*pat)
	{
	case '\0':
		return !*str;
		
	case '*' :
		return MatchPattern(pat+1, str) || *str && MatchPattern(pat, str+1);
		
	case '?' :
		return *str && MatchPattern(pat+1, str+1);
		
	default  :
		return (*pat == *str) && MatchPattern(pat+1, str+1);
	}
}
CFileRule::CFileRule()
{
	m_ruleList.clear();
}

void CFileRule::AddRule(LPCTSTR lpcsRule)
{
	TCHAR szRule[MAX_PATH];
	lstrcpy(szRule, lpcsRule);
	CharLower(szRule);
	VSSString strRule(szRule);
	//std::transform(strRule.begin(), strRule.end(), strRule.begin(), tolower);
	
	m_ruleList.push_back(strRule);
}

BOOL CFileRule::CheckFile(LPCTSTR lpcsFileName, LPCTSTR lpcsFilePath)
{
	TCHAR szTemp[MAX_PATH];
	
	lstrcpy(szTemp, lpcsFileName);
	CharLower(szTemp);
	VSSString strFileName(szTemp);

	lstrcpy(szTemp, lpcsFilePath);
	CharLower(szTemp);
	VSSString strFilePath(szTemp);
	
	//std::transform(strFileName.begin(), strFileName.end(), strFileName.begin(), tolower);
	//std::transform(strFilePath.begin(), strFilePath.end(), strFilePath.begin(), tolower);
	
	VSSStringList::const_iterator it, itEnd;
	for ( it = m_ruleList.begin(), itEnd = m_ruleList.end(); it != itEnd; it++ )
	{
		if (it->find("\\") == VSSString::npos)
		{
			if (MatchPattern(it->c_str(), strFileName.c_str()))
				return TRUE;
		}else
		{
			if (MatchPattern(it->c_str(), strFilePath.c_str()))
				return TRUE;
		}
	}
	return FALSE;
}

void CFileRule::Clear()
{
	m_ruleList.clear();
}




CRuleProfile::CRuleProfile(LPCTSTR lpcsVSSData, LPCTSTR lpcsUser, LPCTSTR lpcsPwd
						   , LPCTSTR lpcsLocalRoot, LPCTSTR lpcsVSSRoot)
{
	strVSSData = lpcsVSSData;
	strUser = lpcsUser;
	strPwd = lpcsPwd;				
	strLocalRoot = lpcsLocalRoot;
	if (strLocalRoot.length()>0 && strLocalRoot.at(strLocalRoot.length()-1)!='\\')
		strLocalRoot += "\\";
	strVSSRoot = lpcsVSSRoot;
	if (strVSSRoot.length()>0 && strVSSRoot.at(strVSSRoot.length()-1)!='\\')
		strVSSRoot += "\\";

	m_pVSSHelper = NULL;
	nRef = 0;
	bErrMsgDone = false;
}

CRuleProfile::~CRuleProfile()
{
	if (m_pVSSHelper!=NULL)
		delete m_pVSSHelper;
	m_pVSSHelper = NULL;
}

CVSSHelper* CRuleProfile::QueryVSSHelper()
{
	if (m_pVSSHelper == NULL)
	{
		m_pVSSHelper = new CVSSHelper(strVSSData.c_str(),strUser.c_str(),strPwd.c_str());

		if (!bErrMsgDone && !m_pVSSHelper->IsValidVSS() && !m_pVSSHelper->m_strLastError.empty())
		{
			MessageBox(NULL, m_pVSSHelper->m_strLastError.c_str(), 
				"Error when connecting to SourceSafe", MB_OK|MB_ICONERROR);
			bErrMsgDone = true;
		}
		ATLTRACE("Create new CVSSHelper\n");
	}
	nRef++;
	return m_pVSSHelper;
}

void CRuleProfile::ReleaseVSSHelper()
{
	if (m_pVSSHelper == NULL)
	{
		nRef = 0;
		return;
	}

	nRef--;

	//FreeUnusedVSSHelper();
	/*
	if (nRef<=0)
	{
		delete m_pVSSHelper;
		m_pVSSHelper = NULL;
		ATLTRACE("Delete CVSSHelper\n");
	}*/
}

bool CRuleProfile::FreeUnusedVSSHelper()
{
	if (m_pVSSHelper!=NULL && nRef<=0)
	{
		delete m_pVSSHelper;
		m_pVSSHelper = NULL;
		nRef = NULL;
		ATLTRACE("Delete CVSSHelper\n");
		return true;
	}

	if (m_pVSSHelper==NULL)
		return true;
	return false;
}

// Return false if the path is outside the root folder
bool CRuleProfile::ParseFromCurrentPath(LPCTSTR/*in*/ lpcsCurrentPath, LPTSTR/*out*/ lpsVSSPath)
{
	VSSString strCurrentPath = lpcsCurrentPath;
	
	if (strCurrentPath.length()<strLocalRoot.length())
	{
		// current path is not inside the local root
		// Check if the current path if the parent of the local root
		return false;
	}else
	{
		// Compare if the current path is inside the local root
		if (!CompareStringNI(&strCurrentPath, &strLocalRoot, strLocalRoot.length()))
			return false;
		
		// Now we are sure that current path is inside the local root
		// get the corresponding vss root
		TCHAR szVSSPath[MAX_PATH], szTemp[MAX_PATH];
		lstrcpy(szVSSPath, strVSSRoot.c_str());
		lstrcpy(szTemp, strCurrentPath.c_str()+strLocalRoot.length());
		lstrcat(szVSSPath, szTemp);
		for (int i=0; i<lstrlen(szVSSPath); i++)
		{
			if (szVSSPath[i] == '\\')
				szVSSPath[i] = '/';
		}					
		
		lstrcpy(lpsVSSPath, szVSSPath);
		
		return true;
	}
}

void CRuleProfile::InitRules(CProfileManager* pProfile, LPCTSTR lpcsSection)
{
	TCHAR szRule[64];
	VSSString strRuleVal;
	int n;
	ruleIncludes.Clear();
	ruleExcludes.Clear();
	
	// Init include rules
	n = 1;
	sprintf(szRule, "Include_%d", n);
	strRuleVal = pProfile->GetValue(lpcsSection, szRule);
	while (strRuleVal.length()>0)
	{
		ruleIncludes.AddRule(strRuleVal.c_str());						
		sprintf(szRule, "Include_%d", ++n);
		strRuleVal = pProfile->GetValue(lpcsSection, szRule);
	}
	
	// Init exclude rules
	n = 1;
	sprintf(szRule, "Exclude_%d", n);
	strRuleVal = pProfile->GetValue(lpcsSection, szRule);
	while (strRuleVal.length()>0)
	{
		ruleExcludes.AddRule(strRuleVal.c_str());
		
		sprintf(szRule, "Exclude_%d", ++n);
		strRuleVal = pProfile->GetValue(lpcsSection, szRule);
	}
}

bool CRuleProfile::CheckItem(LPCTSTR lpcsItemPath)
{
	if (lstrlen(lpcsItemPath)<=strLocalRoot.length())
		return false;
				
	TCHAR sFname[_MAX_FNAME];
	SplitPath(lpcsItemPath, NULL, sFname);

	VSSString strRelativePath;
	strRelativePath = lpcsItemPath + strLocalRoot.length();
				
	DWORD attriCode=GetFileAttributes(lpcsItemPath);
	if ((attriCode!=-1)&&(attriCode&FILE_ATTRIBUTE_DIRECTORY))
	{
	}else
	{
		if (!ruleIncludes.CheckFile(sFname, strRelativePath.c_str()))
			return false;
		// Now check if it matches the excludes rules
		if (ruleExcludes.CheckFile(sFname, strRelativePath.c_str()))
			return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRuleProfileManager::CRuleProfileManager()
{
	m_nIDOffset = 0;
	m_cs.Init();
	
	DWORD nThreadId = 0;
	m_hThreadFileMonitor=CreateThread(0,0,ThreadFileMonitor,
		(PVOID)NULL, CREATE_SUSPENDED, &nThreadId);
	if (m_hThreadFileMonitor!=NULL)
	{
		SetThreadPriority(m_hThreadFileMonitor, THREAD_PRIORITY_IDLE);
		ResumeThread(m_hThreadFileMonitor);
	}
}

CRuleProfileManager::~CRuleProfileManager()
{
	CleanupThread(m_hThreadFileMonitor, true);
	CleanupThread(m_hThreadFreeUnusedItems, true);

	m_cs.Term();
}

void CRuleProfileManager::CleanupThread(HANDLE& hThread, bool bForceTerminate)
{
	m_cs.Lock();
	if (hThread!=NULL)
	{
		if (bForceTerminate)
			TerminateThread(hThread, 0);

		DWORD dwExitCode;
		GetExitCodeThread(hThread, &dwExitCode);
		if (dwExitCode!=STILL_ACTIVE)
			CloseHandle(hThread);
		hThread = NULL;
	}
	m_cs.Unlock();
}

void CRuleProfileManager::Term()
{
	CleanupThread(m_hThreadFreeUnusedItems, true);
	FreeUnusedVSSHelper();
	m_cs.Lock();
	m_profileVector.clear();
	m_cs.Unlock();
}

void CRuleProfileManager::Init(bool bIsReload)
{
	CleanupThread(m_hThreadFreeUnusedItems, true);
	FreeUnusedVSSHelper();

	m_cs.Lock();

	m_nIDOffset += m_profileVector.size();

	CProfileManager m_profileMan;
	m_profileVector.clear();
	
	int profile_count = 0;
	
	TCHAR sFilename[MAX_PATH];
	TCHAR sPath[MAX_PATH];
	GetModuleFileName(_Module.GetModuleInstance(), sFilename, MAX_PATH);
	SplitPath(sFilename, sPath, NULL);
	lstrcat(sPath, "essshell.ini");
	m_profileMan.ReadIniFile(sPath);
	profile_count = m_profileMan.GetValueInt("Common", "profiles");

	m_strCompareCmd = m_profileMan.GetValue("Common", "CompareCmd");
	m_strCompareParam = m_profileMan.GetValue("Common", "CompareParam");
	if (m_strCompareParam.empty())
		m_strCompareParam = "%1 %2";
	if (m_strCompareCmd.empty())
		m_strCompareCmd = "WinDiff.exe";

	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	if(!GetVersionEx(&osv) || (osv.dwMajorVersion == 4 && osv.dwMinorVersion < 90 || osv.dwMajorVersion < 4))
	{
		m_bEnableOverlay = false;
		m_bEnableVSSCaching = false;
	}else
	{
		TCHAR sFilename[MAX_PATH];
		GetModuleFileName(NULL, sFilename, MAX_PATH);
		CharLower(sFilename);
		std::string strFilename(sFilename);
		if (strFilename.find("explorer", 0) != std::string::npos)
		{
			m_bEnableOverlay = m_profileMan.GetValueBool("Common", "OverlayIcons");
			m_bEnableVSSCaching = m_bEnableOverlay;
		}else
		{
			m_bEnableOverlay = m_profileMan.GetValueBool("Common", "NonExplOIcons");
			m_bEnableVSSCaching = false;
		}
	}

	for (int i=0; i<profile_count; i++)
	{
		TCHAR szItem[64];
		sprintf(szItem, "Profile_%d", i+1);
		
		// Remove "" and \\ 
		{
			TCHAR szTemp[MAX_PATH];
			lstrcpy(szTemp, m_profileMan.GetValue(szItem, "LocalRoot"));
			if (lstrlen(szTemp)>0 && szTemp[0]=='\"')
				lstrcpy(szTemp, m_profileMan.GetValue(szItem, "LocalRoot")+1);
			if (lstrlen(szTemp)>0 && szTemp[lstrlen(szTemp)-1] == '\"')
				szTemp[lstrlen(szTemp)-1] = '\0';
			if (lstrlen(szTemp)>0 && szTemp[lstrlen(szTemp)-1] == '\\')
				szTemp[lstrlen(szTemp)-1] = '\0';
			m_profileMan.SetValue(szItem, "LocalRoot", szTemp);
			
			lstrcpy(szTemp, m_profileMan.GetValue(szItem, "VSSRoot"));
			if (lstrlen(szTemp)>0 && szTemp[0]=='\"')
				lstrcpy(szTemp, m_profileMan.GetValue(szItem, "VSSRoot")+1);
			if (lstrlen(szTemp)>0 && szTemp[lstrlen(szTemp)-1] == '\"')
				szTemp[lstrlen(szTemp)-1] = '\0';
			if (lstrlen(szTemp)>0 && szTemp[lstrlen(szTemp)-1] == '/')
				szTemp[lstrlen(szTemp)-1] = '\0';
			m_profileMan.SetValue(szItem, "VSSRoot", szTemp);
		}

		CRuleProfile p(
			m_profileMan.GetValue(szItem, "VSSData"),
			m_profileMan.GetValue(szItem, "User"),
			m_profileMan.GetValue(szItem, "Pwd"),				
			m_profileMan.GetValue(szItem, "LocalRoot"),
			m_profileMan.GetValue(szItem, "VSSRoot"));
		
		p.InitRules(&m_profileMan, szItem);
		m_profileVector.push_back(p);
	}
	m_cs.Unlock();

	//if (bIsReload)
	//	MessageBox(NULL, "The Effective SourceSafe profile has been reloaded.", "Profile reloaded", MB_OK|MB_ICONINFORMATION);
}
/*
const char* CRuleProfileManager::GetValue(const char* section, const char* item)
{	
	m_cs.Lock();
	const char* c = m_profileMan.GetValue(section, item);
	m_cs.Unlock();
	return c;
}

int CRuleProfileManager::GetValueInt(const char* section, const char* item)
{
	m_cs.Lock();
	int i = m_profileMan.GetValueInt(section, item);
	m_cs.Unlock();
	return i;
}
*/
int CRuleProfileManager::Count()
{
	m_cs.Lock();
	int i = m_profileVector.size();
	m_cs.Unlock();
	return i;
}

int CRuleProfileManager::GetProfileID(int iPos)
{
	int count = Count();
	if (iPos<0 || iPos>=Count())
		return -1;

	return iPos+m_nIDOffset;
}

// Return false if the path is outside the root folder
bool CRuleProfileManager::ParseFromCurrentPath(int nProfileID, LPCTSTR/*in*/ lpcsCurrentPath, LPTSTR/*out*/ lpsVSSPath)
{
	m_cs.Lock();

	bool b;
	int iPos = nProfileID-m_nIDOffset;
	if (iPos<0 || iPos>=m_profileVector.size())
		b = false;
	else
		b = m_profileVector.at(iPos).ParseFromCurrentPath(lpcsCurrentPath, lpsVSSPath);
	m_cs.Unlock();
	return b;
}

// Check if an item matches the rule
bool CRuleProfileManager::CheckItem(int nProfileID, LPCTSTR lpcsItemPath)
{
	m_cs.Lock();

	bool b;
	int iPos = nProfileID-m_nIDOffset;
	if (iPos<0 || iPos>=m_profileVector.size())
		b = false;
	else
		b = m_profileVector.at(iPos).CheckItem(lpcsItemPath);
	m_cs.Unlock();
	return b;
}

CVSSHelper* CRuleProfileManager::QueryVSSHelper(int nProfileID)
{
	m_cs.Lock();

	int iPos = nProfileID-m_nIDOffset;
	if (iPos<0 || iPos>=m_profileVector.size())
	{
		m_cs.Unlock();
		return false;
	}
	
	CVSSHelper* pVSS = m_profileVector.at(iPos).QueryVSSHelper();

	DWORD dwExitCode = 0;

	if (IsVSSCachingEnabled())
	{
		m_cs.Unlock();
		CleanupThread(m_hThreadFreeUnusedItems, false);
		m_cs.Lock();
		if (dwExitCode == 0 || m_hThreadFreeUnusedItems == NULL)
		{
			DWORD nThreadId = 0;
			m_hThreadFreeUnusedItems=CreateThread(0,0,ThreadFreeUnusedItems,
				(PVOID)NULL, CREATE_SUSPENDED, &nThreadId);
			if (m_hThreadFreeUnusedItems!=NULL)
			{
				SetThreadPriority(m_hThreadFreeUnusedItems, THREAD_PRIORITY_IDLE);
				ResumeThread(m_hThreadFreeUnusedItems);
			}
		}
	}
	m_cs.Unlock();
	
	return pVSS;
}

void CRuleProfileManager::ReleaseVSSHelper(int nProfileID)
{
	m_cs.Lock();

	int iPos = nProfileID-m_nIDOffset;
	if (iPos<0 || iPos>=m_profileVector.size())
	{
	}else
	{
		m_profileVector.at(iPos).ReleaseVSSHelper();
		if (!IsVSSCachingEnabled())
			m_profileVector.at(iPos).FreeUnusedVSSHelper();
	}
	m_cs.Unlock();
}

bool CRuleProfileManager::FreeUnusedVSSHelper()
{
	m_cs.Lock();

	bool bAllCleaned=true;
	for (int p=0; p<m_profileVector.size(); p++)
	{
		if (!m_profileVector.at(p).FreeUnusedVSSHelper())
			bAllCleaned = false;
	}
	m_cs.Unlock();

	return bAllCleaned;
}

DWORD WINAPI CRuleProfileManager::ThreadFileMonitor(LPVOID pParam)
{
	TCHAR sFilename[MAX_PATH];
	TCHAR sPath[MAX_PATH];
	FILETIME ftLastModified, ftModified;
	GetModuleFileName(_Module.GetModuleInstance(), sFilename, MAX_PATH);
	SplitPath(sFilename, sPath, NULL);
	
	HANDLE hHandle;
	HWND hwnd = (HWND)pParam;
	hHandle = ::FindFirstChangeNotification(sPath, TRUE, 
		FILE_NOTIFY_CHANGE_LAST_WRITE);
	if (hHandle == INVALID_HANDLE_VALUE)
		return -1;

	lstrcat(sPath, "essshell.ini");

	HANDLE hFind;
	WIN32_FIND_DATA fd;
	if ((hFind=::FindFirstFile (sPath,&fd))!=INVALID_HANDLE_VALUE)
		ftLastModified = fd.ftLastWriteTime;
	FindClose (hFind);	

	while (1)
	{
		::WaitForSingleObject(hHandle, INFINITE);

		if ((hFind=::FindFirstFile (sPath,&fd))!=INVALID_HANDLE_VALUE)
		{
			ftModified = fd.ftLastWriteTime;
			if (::CompareFileTime(&ftModified, &ftLastModified) != 0)
			{
				g_profiles.Init(true);
				ftModified = ftLastModified;
			}
		}
		FindClose (hFind);	
		
		::FindNextChangeNotification(hHandle);//Reset;
	}

	::FindCloseChangeNotification(hHandle);
	return 0;
}

DWORD WINAPI CRuleProfileManager::ThreadFreeUnusedItems(LPVOID pParam)
{
	while (1)
	{
		if (g_profiles.FreeUnusedVSSHelper())
			ExitThread(0);
		
		Sleep(300);
	}
	return 0;
}