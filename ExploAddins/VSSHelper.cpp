/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// VSSHelper.cpp: implementation of the CVSSHelper class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VSSHelper.h"
#include "EssUtils.h"

enum VSSCmd
{
	VSS_CMD_ADD,
	VSS_CMD_GET,
	VSS_CMD_CHECKOUT,
	VSS_CMD_CHECKIN,
	VSS_CMD_UNDOOUT,
};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVSSHelper::CVSSHelper(LPCTSTR lpcsDatabase, LPCTSTR lpcsUser, LPCTSTR lpcsPwd)
{
	m_objVSSDb = NULL;
	m_strLastError = "";

	CLSID clsVSS;
	if (FAILED(CLSIDFromString(L"SourceSafe", &clsVSS)))
	{
		m_strLastError = "Microsoft Visual SourceSafe is not installed in this computer";
		return;
	}

	if (FAILED(m_objVSSDb.CreateInstance(clsVSS)))
	{
		m_strLastError = "Microsoft Visual SourceSafe is not installed in this computer";
		return;
	}
	
	try
	{
		hr = m_objVSSDb->Open(_bstr_t(lpcsDatabase), _bstr_t(lpcsUser), _bstr_t(lpcsPwd));
		if (FAILED(hr))
		{
			m_strLastError = "The database could not be opened. Please verify the path, user name and password.";
			m_objVSSDb = NULL;
		}
	}catch(...)
	{
		m_strLastError = "The database could not be opened. Please verify the path, user name and password.";
		m_objVSSDb = NULL;
	}
}

CVSSHelper::~CVSSHelper()
{
	m_objVSSDb = NULL;	
}

void PumpMessages()
{
	MSG msg;
	// Handle dialog messages
	while(::PeekMessage(&msg,NULL,0,0,PM_REMOVE))
	{
		::TranslateMessage(&msg);
		::DispatchMessage(&msg);
	}
}

IVSSItemPtr CVSSHelper::GetSafeFolder(LPCTSTR lpcsVSSItem, bool bCreateIfNotExist)
{
	if (!IsValidVSS())
		return NULL;

	IVSSItemPtr objItem = NULL;
	try
	{
		objItem = m_objVSSDb->GetVSSItem(_bstr_t(lpcsVSSItem), FALSE);
	}catch (...)
	{
	}
	if (objItem == NULL && bCreateIfNotExist)
	{
		TCHAR szItem[MAX_PATH];
		TCHAR szParentPath[MAX_PATH];
		TCHAR szItemName[_MAX_FNAME];
		lstrcpy(szItem, lpcsVSSItem);
		if (lstrlen(szItem)>0 && szItem[lstrlen(szItem)-1] == '/')
			szItem[lstrlen(szItem)-1] = '\0';
		SplitPath(szItem, szParentPath, szItemName);

		if (lstrlen(szParentPath)<2)
			return NULL;

		IVSSItemPtr pParent = GetSafeFolder(szParentPath);
		if (pParent)
		{
			try
			{
				objItem = pParent->NewSubproject(_bstr_t(szItemName), _bstr_t(""));
			}catch(...)
			{
				objItem = NULL;
			}
		}
	}

	return objItem;
}

IVSSItemPtr CVSSHelper::GetSafeItem(IVSSItemPtr objItemParent, LPCTSTR lpcsVSSItem)
{
	USES_CONVERSION;
	_bstr_t bstrVSSItem = objItemParent->GetSpec();
	VSSString strVSSItem = W2T((LPWSTR)bstrVSSItem);
	
	if (strVSSItem.length()<2)
		return NULL;
	if (strVSSItem.at(strVSSItem.length()-1) != '/')
		strVSSItem += "/";
	strVSSItem += lpcsVSSItem;

	IVSSItemPtr objItem = NULL;
	try
	{
		objItem = m_objVSSDb->GetVSSItem(_bstr_t(strVSSItem.c_str()), FALSE);
	}
  catch (_com_error& e) {
    #ifdef _DEBUG
    ATLTRACE(" *** GetSafeItem exception: com error: %d - %s\n", e.Error(), (const char*)e.Description());
    #endif
  }  
  catch (...)
	{
	}
	return objItem;
}

void CVSSHelper::RecursiveCmd(int nCmdID, IVSSItemPtr objItemParent, LPCTSTR lpcsParentPath, int nProfileID)
{
	//VSSString strPath = lpcsParentPath;
	if (lstrlen(lpcsParentPath)<=0)
		return;
	
	TCHAR szParentPath[MAX_PATH], szVSSParentPath[MAX_PATH];
	lstrcpy(szParentPath, lpcsParentPath);
	if (szParentPath[lstrlen(szParentPath)-1] != '\\')
		lstrcat(szParentPath, "\\");

	VSSString strMask(szParentPath);
	strMask+=_T("*.*");


	if (!g_profiles.ParseFromCurrentPath(nProfileID, 
		szParentPath, szVSSParentPath))
		return;
	
	HANDLE hFind;
	WIN32_FIND_DATA fd;

	if ((hFind=::FindFirstFile (strMask.c_str(),&fd))==
		INVALID_HANDLE_VALUE)
	{
		FindClose (hFind);
		return;
	}

	do
	{
		if (lstrcmp(fd.cFileName, ".")==0)
			continue;
		if (lstrcmp(fd.cFileName, "..")==0)
			continue;
		vss_item me;
		me.localpath = szParentPath;
		me.vsspath = szVSSParentPath;
		me.filename = fd.cFileName;
		me.nProfileID = nProfileID;
		
		strMask = szParentPath;
		strMask += fd.cFileName;
		
		if (!g_profiles.CheckItem(nProfileID, strMask.c_str()))
			continue;

		switch (nCmdID)
		{
		case VSS_CMD_ADD:
			Add(objItemParent, &me, true);
			break;
		case VSS_CMD_GET:
			Get(objItemParent, &me, true);
			break;
		case VSS_CMD_CHECKOUT:
			CheckOut(objItemParent, &me, true);
			break;
		case VSS_CMD_CHECKIN:
			CheckIn(objItemParent, &me, true);
			break;
		case VSS_CMD_UNDOOUT:
			UndoCheckOut(objItemParent, &me, true);
			break;
		}
	} while (::FindNextFile (hFind,&fd));

	FindClose (hFind);	
}

bool CVSSHelper::Add(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive)
{
	VSSString strPath = pItem->localpath;
	strPath += pItem->filename;
	
	DWORD attriCode=GetFileAttributes(strPath.c_str());
	BOOL bIsFolder = ((attriCode!=-1)&&(attriCode&FILE_ATTRIBUTE_DIRECTORY));

	IVSSItemPtr objItem = GetSafeItem(objItemParent, pItem->filename.c_str());
	if (objItem == NULL)
	{
		try
		{
			if (bIsFolder)
				objItem = objItemParent->NewSubproject(_bstr_t(pItem->filename.c_str()), _bstr_t(""));
			else
				objItem = objItemParent->Add(_bstr_t(strPath.c_str()), _bstr_t(""), NULL);
		}catch (_com_error e)
		{
			return false;
		}
	}

	PumpMessages();

	if (bIsFolder && bRecursive)
		RecursiveCmd(VSS_CMD_ADD, objItem, strPath.c_str(), pItem->nProfileID);
	
	return true;
}

bool CVSSHelper::Get(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive)
{
	VSSString strPath = pItem->localpath;
	strPath += pItem->filename;
	
	DWORD attriCode=GetFileAttributes(strPath.c_str());
	BOOL bIsFolder = ((attriCode!=-1)&&(attriCode&FILE_ATTRIBUTE_DIRECTORY));

	IVSSItemPtr objItem = GetSafeItem(objItemParent, pItem->filename.c_str());
	if (objItem==NULL)
		return false;

	hr = S_FALSE;
	try
	{
		CComBSTR path = strPath.c_str();
		BSTR b_path = path.Detach();
		if (bRecursive)
			hr = objItem->Get(&b_path, VSSFLAG_RECURSYES|VSSFLAG_FORCEDIRNO);
		else
			hr = objItem->Get(&b_path, NULL);
		::SysFreeString(b_path);
	}catch(...)
	{}

	//PumpMessages();

	//if (bIsFolder && bRecursive)
//		RecursiveCmd(ESS_CMD_GET, objItem, strPath.c_str(), pItem->nProfileID);


	return SUCCEEDED(hr);
}

//
// Example usage of IEnumVARIANT (loop through items of container)
// ... list all users, who checkouts this vssitem
//
#include <list>
#include <string>
std::list<std::string> GetCheckouters(IVSSItemPtr &vssitem)
{
//  ASSERT (vssitem->GetType() != 0l);

  IUnknownPtr lpunk;
  IVSSCheckoutsPtr objCheckouts;
  IVSSCheckoutPtr objCheckout;

  IEnumVARIANTPtr ppvobj;
  ULONG fetched;
  VARIANT st;

  std::list<std::string> usernameList;

  try // ssapi spits sometimes
  {
    objCheckouts = vssitem->GetCheckouts();
    // Prepare to loop through the items
    lpunk = objCheckouts->_NewEnum();
    lpunk.QueryInterface(IID_IEnumVARIANT, (void **)&ppvobj);
    // Loop through the checkouts
    do
    {
      ppvobj->Next(1UL, &st, &fetched);
      if(fetched != 0)
      {
        // Try to get the item
        if(!FAILED(st.punkVal->QueryInterface(__uuidof(IVSSCheckout), (void**)&objCheckout))){
          char chname[256]; sprintf(chname, "%s", (LPCTSTR)objCheckout->GetUsername());
          //username. Format("%s", (LPCTSTR)objCheckout->GetUsername());
          std::string username(chname);
          usernameList.push_back(username);
          st.punkVal->Release();
        }
      }
    }
    while (fetched != 0);

    ppvobj.Release();
    // something very strange can happen here when the pointers are the same
    if (lpunk != ppvobj)
      lpunk.Release();
  }
  catch (...)
  {
    // do nothing
  } 
  return usernameList;
}
//
//USAGE:
//
//std::list<std::string> chlist = GetCheckouters(objItem);
//std::list<std::string>::iterator it = chlist.begin(), end = chlist.end();
//for (; it!=end; it++) 
//{
//  std::string name = *it;
//  OutputDebugString("XXX Checked Out by: ");
//  OutputDebugString(name.data());
//  OutputDebugString("\n");
//}

bool CVSSHelper::GetCheckedOuterName(IVSSItemPtr objItem, LPWSTR &outName)
{
	USES_CONVERSION;
  bool retval=false;  
  static wchar_t checkoutingUserName[256];
  LPWSTR name = NULL;
  if (objItem == NULL)
		return false;
#ifdef _DEBUG
  _bstr_t nm = objItem->GetName();
  if (!wcscmp(nm, L"tankrb.cpp"))
    _asm nop;
#endif
	try
	{
//    bool itIsCheckedOuted = objItem->GetIsCheckedOut()!=NULL;
//    std::list<std::string> chlist = GetCheckouters(objItem);
//    std::list<std::string>::iterator it = chlist.begin(), end = chlist.end();
//    for (; it!=end; it++) 
//    {
//      std::string name = *it;
//      OutputDebugString("XXX Checked Out by: ");
//      OutputDebugString(name.data());
//      OutputDebugString("\n");
//    }
//    if (itIsCheckedOuted)
//    {
    IVSSCheckoutsPtr chptr = objItem->Checkouts;
      if (chptr!=NULL) 
      {
        int count = chptr->GetCount();
        if (count > 0) 
        {
          VARIANT sItem;
          for (int i=1; i<=count; i++)
          {
            VariantInit(&sItem);
            sItem.vt = VT_INT;
            sItem.intVal = 1;
            IVSSCheckoutPtr chOutPtr = objItem->Checkouts->GetItem(sItem);
            VariantClear(&sItem);
            name = chOutPtr->GetUsername();
            wcscpy(checkoutingUserName, name);
            name = m_objVSSDb->GetUsername();
            if (!wcscmp(checkoutingUserName, name)) 
            {
              retval = true;
              break;
            }
          }
          #ifdef _DEBUG
            CW2A nameA(checkoutingUserName);
            OutputDebugString("*** Checked Out by: ");
            OutputDebugString(nameA);
            OutputDebugString("\n");
          #endif
          //wcscpy(checkoutingUserName, name);
          if (count>1) wcscat(checkoutingUserName, L"...");
          outName = checkoutingUserName;
        }
      }
//    }
	}
  catch (_com_error& e) {
    #ifdef _DEBUG
    ATLTRACE(" *** GetCheckedOuterName exception: com error: %d - %s\n", e.Error(), (const char*)e.Description());
    #endif
  }
  catch( ... )
	{
	}	
	return retval;
}

bool CVSSHelper::IsDifferent(IVSSItemPtr objItemParent, vss_item* pItem)
{
  USES_CONVERSION;
  VSSString strPath = pItem->localpath;
  strPath += pItem->filename;
  LPWSTR localFile = A2W(strPath.c_str());
  IVSSItemPtr objItem = GetSafeItem(objItemParent, pItem->filename.c_str());
  if (objItem->GetIsDifferent(localFile)) return true;
  return false;
}

bool CVSSHelper::IsCheckedOut(IVSSItemPtr objItem)
{
  if (objItem == NULL)
    return false;
  try
  {
    return objItem->GetIsCheckedOut()!=NULL;
  } catch (_com_error& e) {
    #ifdef _DEBUG
    ATLTRACE(" *** IsCheckedOut exception: com error: %d - %s\n", e.Error(), (const char*)e.Description());
    #endif
  } catch(...)
  {
  }	
  return false;
}

bool CVSSHelper::CheckOut(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive)
{
  IVSSItemPtr objItem = GetSafeItem(objItemParent, pItem->filename.c_str());
  if (objItem==NULL)
    return false;

  VSSString strPath = pItem->localpath;
  strPath += pItem->filename;
  DWORD attriCode=GetFileAttributes(strPath.c_str());
  BOOL bIsFolder = ((attriCode!=-1)&&(attriCode&FILE_ATTRIBUTE_DIRECTORY));

	hr = S_OK;
  if (!bIsFolder)
  {
	  hr = S_FALSE;
	  try
	  {
      LPWSTR chname;
      if (!IsCheckedOut(objItem) || !GetCheckedOuterName(objItem, chname))
      {
        long iFlags=NULL;
        if (IsDifferent(objItemParent, pItem))
        {
          char messageText[512];
          sprintf(messageText, "Folder contains different copy of file %s.\nDo you want to LEAVE your local copy?\n(Replace otherwise, or Cancel CheckOut)", pItem->filename.c_str());
          int answer = MessageBox
            (
            NULL, 
            messageText,
            "CheckOut",
            MB_YESNOCANCEL|MB_ICONQUESTION
            );
          switch (answer) 
          {
          case IDYES: iFlags = VSSFLAG_REPSKIP | VSSFLAG_GETNO; 
            break;
          case IDNO: iFlags = VSSFLAG_REPREPLACE | VSSFLAG_GETYES; 
            break;
          default:
            goto NO_CHECKOUT;
          }
        }
        hr = objItem->Checkout(_bstr_t(""), _bstr_t(strPath.c_str()), iFlags);
  NO_CHECKOUT: ;
      }
    }catch (_com_error& e) {
      #ifdef _DEBUG
      ATLTRACE(" *** CheckOut exception: com error: %d - %s\n", e.Error(), (const char*)e.Description());
      #endif
    }catch(...)
	  {}
  }

	PumpMessages();

	if (bIsFolder && bRecursive)
		RecursiveCmd(VSS_CMD_CHECKOUT, objItem, strPath.c_str(), pItem->nProfileID);

	return SUCCEEDED(hr);
}

bool CVSSHelper::CheckIn(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive)
{
	VSSString strPath = pItem->localpath;
	strPath += pItem->filename;
	
	DWORD attriCode=GetFileAttributes(strPath.c_str());
	BOOL bIsFolder = ((attriCode!=-1)&&(attriCode&FILE_ATTRIBUTE_DIRECTORY));

	IVSSItemPtr objItem = GetSafeItem(objItemParent, pItem->filename.c_str());
	if (objItem==NULL)
		return false;
	
	hr = S_FALSE;
	try
	{
		if (IsCheckedOut(objItem))
    {
      LPWSTR name;
      if (GetCheckedOuterName(objItem, name)) //I'm checkouter too
      {
        IVSSCheckoutsPtr chptr = objItem->Checkouts;
        if (chptr!=NULL) 
        {
          if (chptr->GetCount() > 1) //but I'm not alone
          {
            char messageText[512];
            sprintf(messageText,"File %s is also checkOuted by another USER.\nDo you really want to CheckIn?", pItem->filename.c_str());
            int answer = MessageBox
              (
              NULL, 
              messageText,
              "CheckIn",
              MB_YESNOCANCEL|MB_ICONQUESTION
              );
            switch (answer) 
            {
            case IDYES:
              break;
            case IDNO: 
            default:
              goto NO_CHECKIN;
            }
          }
        }
        hr = objItem->Checkin(_bstr_t(""), _bstr_t(strPath.c_str()), NULL);		
NO_CHECKIN: ;
      }
    }
	}catch(...)
	{}

	PumpMessages();

	if (bIsFolder && bRecursive)
		RecursiveCmd(VSS_CMD_CHECKIN, objItem, strPath.c_str(), pItem->nProfileID);

	return SUCCEEDED(hr);
}

bool CVSSHelper::UndoCheckOut(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive)
{
  IVSSItemPtr objItem = GetSafeItem(objItemParent, pItem->filename.c_str());
  if (objItem==NULL)
    return false;

  VSSString strPath = pItem->localpath;
	strPath += pItem->filename;
	DWORD attriCode=GetFileAttributes(strPath.c_str());
	BOOL bIsFolder = ((attriCode!=-1)&&(attriCode&FILE_ATTRIBUTE_DIRECTORY));
	
  hr = S_OK;
  if (!bIsFolder)
  {
	  try
	  {
		  if (IsCheckedOut(objItem))
      {
        long iFlags = NULL;
        if (IsDifferent(objItemParent, pItem))
        {
          char messageText[512];
          sprintf(messageText,"Your local copy of file %s has changed.\nDo you want to LEAVE your local copy?\n(Replace otherwise, or Cancel UndoCheckOut)", pItem->filename.c_str());
          int answer = MessageBox
            (
            NULL, 
            messageText,
            "Undo CheckOut",
            MB_YESNOCANCEL|MB_ICONQUESTION
            );
          switch (answer) 
          {
          case IDYES: iFlags = VSSFLAG_DELNOREPLACE; 
            break;
          case IDNO: iFlags = VSSFLAG_DELNO; 
            break;
          default:
            goto NO_UNDO;
          }
        }
        hr = objItem->UndoCheckout(_bstr_t(strPath.c_str()), iFlags);
      }
  NO_UNDO: ;
    }
    catch (_com_error& e) {
      #ifdef _DEBUG
      ATLTRACE(" *** UndoCheckOut exception: com error: %d - %s\n", e.Error(), (const char*)e.Description());
      #endif
    } catch(...)
	  {
      #ifdef _DEBUG
      ATLTRACE(" *** UndoCheckOut exception: UNKNOWN Error\n");
      #endif
    }
  }

	PumpMessages();

	if (bIsFolder && bRecursive)
		RecursiveCmd(VSS_CMD_UNDOOUT, objItem, strPath.c_str(), pItem->nProfileID);

	return SUCCEEDED(hr);
}

bool CVSSHelper::Delete(IVSSItemPtr objItemParent, vss_item* pItem, bool bRecursive)
{
	IVSSItemPtr objItem = GetSafeItem(objItemParent, pItem->filename.c_str());
	if (objItem==NULL)
		return false;
	
	try
	{
		hr = objItem->put_Deleted(TRUE);
		return SUCCEEDED(hr);
	}catch(...)
	{}
	return false;
}

bool CVSSHelper::GetTempVersion(IVSSItemPtr objItemParent, vss_item* pItem, LPTSTR lpsFilePath)
{
	USES_CONVERSION;
	
	IVSSItemPtr objItem = GetSafeItem(objItemParent, pItem->filename.c_str());
	if (objItem==NULL)
		return false;

	hr = S_FALSE;
	try
	{
		TCHAR szTempFolder[MAX_PATH] = "C:\\";
		GetTempPath(MAX_PATH, szTempFolder);
		GetTempFileName(szTempFolder, "VSS", 0, lpsFilePath);

		CComBSTR path(lpsFilePath);
		BSTR b_path = path.Detach();
		hr = objItem->Get(&b_path, VSSFLAG_FORCEDIRNO|VSSFLAG_REPREPLACE);
		::SysFreeString(b_path);

		return true;
	}catch(...)
	{}
	return false;	
}