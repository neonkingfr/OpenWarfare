/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/

// OverlayIconNew.h : Declaration of the COverlayIconNew

#ifndef __OverlayIconNew_H_
#define __OverlayIconNew_H_

#include "resource.h"       // main symbols
#include "OverlayIconBase.h"

/////////////////////////////////////////////////////////////////////////////
// COverlayIconNew

class ATL_NO_VTABLE COverlayIconNew : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<COverlayIconNew, &CLSID_OverlayIconNew>,
	public IOverlayIconNew,
	public IShellIconOverlayIdentifier,
	public COverlayIconBase
{
public:
	COverlayIconNew()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_OVERLAYICONNEW)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(COverlayIconNew)
	COM_INTERFACE_ENTRY(IOverlayIconNew)
	COM_INTERFACE_ENTRY(IShellIconOverlayIdentifier)
END_COM_MAP()

// IOverlayIconNew
public:

	// IShellIconOverlayIdentifier methods
	STDMETHODIMP GetOverlayInfo(LPWSTR pwszIconFile, int cchMax, int *pIndex, DWORD *pdwFlags);
	STDMETHODIMP GetPriority(int *pPriority);	
	STDMETHODIMP IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib);
};

#endif //__OverlayIconNew_H_
