/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/

// OverlayIconNew.cpp : Implementation of COverlayIconNew
#include "stdafx.h"
#include "EssShell.h"
#include "OverlayIconNew.h"

#include "EssUtils.h"

/////////////////////////////////////////////////////////////////////////////
// COverlayIconNew

STDMETHODIMP COverlayIconNew::GetOverlayInfo(LPWSTR pwszIconFile, int cchMax, int *pIndex, DWORD *pdwFlags)
{
	COverlayIconBase::QueryIcon(pwszIconFile, cchMax, "new.ico");
	*pIndex = 0;
	*pdwFlags = ISIOI_ICONFILE;
	
	return S_OK;
};

STDMETHODIMP COverlayIconNew::GetPriority(int *pPriority)
{
	*pPriority = 12;
	return S_OK;
}

STDMETHODIMP COverlayIconNew::IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib )
{
  //if (!wcscmp(pwszPath, L"W:\\C\\Poseidon\\lib\\tankrb.cpp")) OutputDebugString("COverlayIconNew\n");
  //if (!wcscmp(pwszPath, L"W:\\C\\AITools\\FSMCompiler\\FSMGraph.cpp")) OutputDebugString("COverlayIconNew\n");
  int type = COverlayIconBase::IsMemberOf(pwszPath, dwAttrib);
	if (type == IconNew)
		return S_OK;
	return S_FALSE;
}