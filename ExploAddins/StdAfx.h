// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__69703C1C_1A74_407E_B337_1ABB40E8CF0C__INCLUDED_)
#define AFX_STDAFX_H__69703C1C_1A74_407E_B337_1ABB40E8CF0C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0400
#endif
#define _ATL_APARTMENT_THREADED

#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>

#include <string>
#include <list>
#include <algorithm>
#include <locale>
#include <functional>
#include <vector>
#include <map>

#include "profileman.h"

typedef std::basic_string<TCHAR> VSSString;
typedef std::basic_string<wchar_t> WideString;
typedef std::list<VSSString> VSSStringList;


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__69703C1C_1A74_407E_B337_1ABB40E8CF0C__INCLUDED)
