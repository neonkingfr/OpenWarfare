//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by EssShell.rc
//
#define IDS_PROJNAME                    100
#define IDR_MENUEXT                     101
#define IDR_OVERLAYICONIN               102
#define IDR_OVERLAYICONOTHEROUT         103
#define IDB_ADD                         202
#define IDB_CHECKIN                     203
#define IDB_CHECKOUT                    204
#define IDB_HISTORY                     205
#define IDR_OVERLAYICONNEW              206
#define IDB_PROPERTIES                  206
#define IDR_OVERLAYICONOUT              207

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        208
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
