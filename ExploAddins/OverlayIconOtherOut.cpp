// OverlayIconOtherOut.cpp : Implementation of COverlayIconOtherOut

#include "stdafx.h"
#include "EssShell.h"
#include "OverlayIconOtherOut.h"
#include "EssUtils.h"

/////////////////////////////////////////////////////////////////////////////
// OverlayIconOtherOut

STDMETHODIMP COverlayIconOtherOut::GetOverlayInfo(LPWSTR pwszIconFile, int cchMax, int *pIndex, DWORD *pdwFlags)
{
  COverlayIconBase::QueryIcon(pwszIconFile, cchMax, "otherout.ico");
  *pIndex = 0;
  *pdwFlags = ISIOI_ICONFILE;

  return S_OK;
};

STDMETHODIMP COverlayIconOtherOut::GetPriority(int *pPriority)
{
  *pPriority = 8;
  return S_OK;
}

STDMETHODIMP COverlayIconOtherOut::IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib )
{
  //if (!wcscmp(pwszPath, L"W:\\C\\Poseidon\\lib\\tankrb.cpp")) OutputDebugString("COverlayIconOtherOut\n");
  //if (!wcscmp(pwszPath, L"W:\\C\\AITools\\FSMCompiler\\FSMGraph.cpp")) OutputDebugString("COverlayIconOtherOut\n");
  int type = COverlayIconBase::IsMemberOf(pwszPath, dwAttrib);
  if (type == IconOtherOut)
    return S_OK;
  return S_FALSE;
}
