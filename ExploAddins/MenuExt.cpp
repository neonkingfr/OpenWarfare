/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// MenuExt.cpp : Implementation of CMenuExt
#include "stdafx.h"
#include "EssShell.h"

#include "MenuExt.h"

#include "VSSHelper.h"
#include "ESSUtils.h"
#include "RuleProfile.h"

enum ESSCmd
{
	ESS_CMD_FIRST,
	//Add to source control
	ESS_CMD_ADD,
	//Get latest version 
	ESS_CMD_GET,
	//Check out 
	ESS_CMD_CHECKOUT,
	//Check in 
	ESS_CMD_CHECKIN,
	//Undo check out 
	ESS_CMD_UNDOOUT,
	// submenu
	ESS_CMD_SUBMENU,
	ESS_CMD_ADD_R,
	//Get latest version 
	ESS_CMD_GET_R,
	//Check out 
	ESS_CMD_CHECKOUT_R,
	//Check in 
	ESS_CMD_CHECKIN_R,
	//Undo check out 
	ESS_CMD_UNDOOUT_R,
	ESS_CMD_SEP_1,
	//Show history
	ESS_CMD_HISTORY,
	// Compare with an older version
	ESS_CMD_COMPARE,
	//Delete at source control
	ESS_CMD_DELETE,
	//Exclude from source control
	ESS_CMD_EXCLUDE,	
	ESS_CMD_PROPERTIES,
	ESS_CMD_SEP_2,
	ESS_CMD_VSS,
	ESS_CMD_VSSADMIN,
	ESS_CMD_SEP_3,
	ESS_CMD_CONFIG,
	ESS_CMD_SEP_4,	
	ESS_CMD_ABOUT,
	ESS_CMD_LAST
};

/////////////////////////////////////////////////////////////////////////////
// CMenuExt

HRESULT CMenuExt::Initialize (LPCITEMIDLIST pidlFolder,
							  LPDATAOBJECT pDataObj,
							  HKEY hProgID )
{
	m_itemList.clear();
	m_cmdMap.clear();
	
	VSSStringList itemList;
	// Get the selected file list
	m_uItemCount = DecodeDropedFileList(pDataObj, &itemList);
	if (m_uItemCount<=0)
		return E_INVALIDARG;
	
	int profile_count = g_profiles.Count();
	int nProfileID = -1;
	
	m_bListContainsFolder = false;
	//m_bRecursive = false;
	
	VSSStringList::const_iterator itItem, itEndItem;
	TCHAR szCurrentPath[MAX_PATH];
	TCHAR szFileName[_MAX_FNAME];
	TCHAR szVSSPath[MAX_PATH];
	for ( itItem = itemList.begin(), itEndItem = itemList.end(); itItem != itEndItem; itItem++ )
	{		
		SplitPath(itItem->c_str(), szCurrentPath, szFileName);

		for (int p=0; p<profile_count; p++)
		{
			nProfileID = g_profiles.GetProfileID(p);
			if (!g_profiles.ParseFromCurrentPath(nProfileID, szCurrentPath, szVSSPath))
				continue;
			if (!g_profiles.CheckItem(nProfileID, itItem->c_str()))
				continue;

			vss_item me;
			me.localpath = szCurrentPath;
			me.vsspath = szVSSPath;
			me.filename = szFileName;
			me.nProfileID = nProfileID;
			DWORD attriCode=GetFileAttributes(itItem->c_str());
			if ((attriCode!=-1)&&(attriCode&FILE_ATTRIBUTE_DIRECTORY))
				m_bListContainsFolder = true;
			
			m_itemList.push_back(me);
		}
	}
	
	if (!m_itemList.empty())
		return S_OK;
	return E_INVALIDARG;
}

HRESULT CMenuExt::QueryContextMenu (HMENU hmenu,
									UINT  uMenuIndex, 
									UINT  uidFirstCmd,
									UINT  uidLastCmd,
									UINT  uFlags )
{
  USES_CONVERSION;
  int iItemCountMain = 0, iItemCountSub = 0;
    // If the flags include CMF_DEFAULTONLY then we shouldn't do anything.
    if ( uFlags & CMF_DEFAULTONLY )
        return MAKE_HRESULT ( SEVERITY_SUCCESS, FACILITY_NULL, 0 );

	//BOOL bShiftPressed = (GetKeyState(VK_SHIFT)  & 0x8000);
	
	bool bHasItemToAdd=false;
	bool bHasItemCheckedOut=false;
	bool bHasItemCheckedIn=false;
	bool bHasItemInVSS=false;
  bool bCheckedOutedByMe=false;
  LPWSTR checkouterName=NULL;
  bool grayIt=false;
	
	// Find the status
	int profile_count = g_profiles.Count();
	for (int p=0; p<profile_count && 
		(!(bHasItemToAdd && bHasItemCheckedOut && bHasItemCheckedIn && bHasItemInVSS)); 
		p++)
	{
		int nProfileID = g_profiles.GetProfileID(p);
		
		CVSSHelper* pVss = g_profiles.QueryVSSHelper(nProfileID);
		if (pVss == NULL || !pVss->IsValidVSS())
		{
			TCHAR szMessage[128];
			sprintf(szMessage, 
				"Cound not open database at\n%s\n\nPlease verify the database location, user id and password.\n",
				"");
			//MessageBox(NULL, szMessage, "Error", MB_OK|MB_ICONERROR);
			ATLTRACE(szMessage);
			g_profiles.ReleaseVSSHelper(nProfileID);
			continue;
		}

		/*IVSSItemPtr objParent = pVss->GetSafeFolder(itProfile->strVSSPath.c_str(), false);
		if (objParent == NULL)
		{
		bHasItemToAdd = true;
		continue;
		}*/
		
		VSSItemList::iterator itItem, itEndItem;
		for ( itItem = m_itemList.begin(), itEndItem = m_itemList.end();
			itItem != itEndItem; 
			itItem++ )
		{
			if (itItem->nProfileID != nProfileID)
				continue;
			vss_item* pItem = &*itItem;

			IVSSItemPtr objParent = pVss->GetSafeFolder(pItem->vsspath.c_str(), false);
			if (objParent == NULL)
			{
				bHasItemToAdd = true;
				continue;
			}

			VSSString strPath(itItem->localpath);
			strPath+=itItem->filename;
			
			DWORD attriCode=GetFileAttributes(strPath.c_str());
			BOOL bIsFolder = ((attriCode!=-1)&&(attriCode&FILE_ATTRIBUTE_DIRECTORY));
			if (bIsFolder)
			{
				/*if (bShiftPressed)
				{
					bHasItemCheckedIn = true;
					bHasItemCheckedOut = true;
					bHasItemToAdd = true;
					bHasItemInVSS = true;
				}*/
			}
			else
			{
				IVSSItemPtr objItem = pVss->GetSafeItem(objParent, pItem->filename.c_str());
				if (objItem == NULL)
				{
					/*if (!bHasItemToAdd) */bHasItemToAdd = true;
				}else
				{
					/*if (!bHasItemInVSS) */bHasItemInVSS = true;
					if (pVss->IsCheckedOut(objItem))
					{
            /*if (!bHasItemCheckedOut) */bHasItemCheckedOut = true;
            bCheckedOutedByMe = pVss->GetCheckedOuterName(objItem, checkouterName);
					}else
					{
						/*if (!bHasItemCheckedIn) */bHasItemCheckedIn = true;
					}					
				}
			}
		}
		g_profiles.ReleaseVSSHelper(nProfileID);
	}
	
	InsertMenu ( hmenu, uMenuIndex+iItemCountMain, MF_BYPOSITION|MF_SEPARATOR, 
		uidFirstCmd+iItemCountMain+iItemCountSub, _T("") );
	iItemCountMain++;
	
	//m_bRecursive = bShiftPressed && m_bListContainsFolder;
	HMENU hSubMenu = CreateMenu();
	
	HBITMAP hBitmap;
	int nCmdID;
	VSSString strText;
	for (nCmdID=ESS_CMD_FIRST+1; nCmdID<=ESS_CMD_LAST-1; nCmdID ++)
	{
		hBitmap = NULL;
		switch(nCmdID)
		{
		case ESS_CMD_ADD:
			if (!bHasItemToAdd) continue;
			strText = "&Add to Source Control";
			hBitmap = m_hbmpAdd;
			break;
		case ESS_CMD_GET:
			if (!bHasItemInVSS) continue;
			strText = "&Get Latest Version";
			break;
		case ESS_CMD_CHECKOUT:
			if (!bHasItemCheckedIn && 
         (bHasItemInVSS && bHasItemCheckedOut && bCheckedOutedByMe)) continue;
      if ( m_bListContainsFolder) continue;
			strText = "Check &Out";
			hBitmap = m_hBmpOut;
			break;
		case ESS_CMD_CHECKIN:
			if (!bHasItemCheckedOut || !bCheckedOutedByMe) continue;
			strText = "Check &In";
			hBitmap = m_hbmpIn;
			break;
		case ESS_CMD_UNDOOUT:
      {
        if (!bHasItemCheckedOut) continue;
			  strText = "&Undo Check Out (by "+VSSString(W2A(checkouterName))+")";
        grayIt = !bCheckedOutedByMe;
      }
			break;
		case ESS_CMD_SUBMENU:
			strText = "Sou&rce Control";
			break;
		case ESS_CMD_ADD_R:
			if (!m_bListContainsFolder) continue;
			strText = "&Add to Source Control (R)";
			hBitmap = m_hbmpAdd;
			break;
		case ESS_CMD_GET_R:
			if (!m_bListContainsFolder) continue;
			strText = "&Get Latest Version (R)";
			break;
		case ESS_CMD_CHECKOUT_R:
			if (!m_bListContainsFolder) continue;
			strText = "Check &Out (R)";
			hBitmap = m_hBmpOut;
			break;
		case ESS_CMD_CHECKIN_R:
			if (!m_bListContainsFolder) continue;
			strText = "Check &In (R)";
			hBitmap = m_hbmpIn;
			break;
		case ESS_CMD_UNDOOUT_R:
			if (!m_bListContainsFolder) continue;
			strText = "&Undo Check Out (R)";
			break;
		case ESS_CMD_SEP_1:
			if (!m_bListContainsFolder) continue;
			strText = "";
			break;
		case ESS_CMD_HISTORY:
			if (!bHasItemInVSS) continue;
			strText = "Show &History ...";
			hBitmap = m_hBmpHistory;
			break;
		case ESS_CMD_COMPARE:
			if (!bHasItemInVSS) continue;
			strText = "&Compare Versions";
			break;
		case ESS_CMD_EXCLUDE:
			if (!bHasItemInVSS) continue;
			strText = "&Exclude from Source Control";
			break;
		case ESS_CMD_DELETE:
			if (!bHasItemInVSS) continue;
			strText = "&Delete at Source Control";
			break;
		case ESS_CMD_PROPERTIES:
			if (!bHasItemInVSS) continue;
			strText = "SouceSafe &Properties";
			hBitmap = m_hBmpProperties;
			break;
		case ESS_CMD_SEP_2:
			if (!bHasItemInVSS) continue;
			strText = "";
			break;
		case ESS_CMD_CONFIG:
			strText = "Configure E&ffective SourceSafe";
			break;
		case ESS_CMD_VSS:
			strText = "&Load Visual SourceSafe Explorer";
			break;		
		case ESS_CMD_VSSADMIN:
			strText = "&Load Visual SourceSafe Admin";
			break;
		case ESS_CMD_SEP_3:
			strText = "";
			break;
		case ESS_CMD_SEP_4:
			strText = "";
			break;
		case ESS_CMD_ABOUT:
			strText = "A&bout Effective SourceSafe ...";
			break;
		default:
			strText = "Unknown commands...";
			break;
		}

		if (nCmdID < ESS_CMD_SUBMENU)
		{
			InsertMenu ( hmenu, uMenuIndex+iItemCountMain, MF_BYPOSITION, 
				uidFirstCmd+iItemCountMain+iItemCountSub, strText.c_str());
      if (grayIt)
        EnableMenuItem(hmenu, uMenuIndex+iItemCountMain, MF_BYPOSITION|MF_DISABLED|MF_GRAYED);
			if (hBitmap)
				SetMenuItemBitmaps (hmenu, uMenuIndex+iItemCountMain, 
				MF_BYPOSITION, hBitmap, NULL);
			
			iItemCountMain++;
		}else if (nCmdID == ESS_CMD_SUBMENU)
		{
			InsertMenu(hmenu, uMenuIndex+iItemCountMain, MF_BYPOSITION|MF_POPUP, 
				(UINT)hSubMenu, strText.c_str());
			if (hBitmap)
				SetMenuItemBitmaps (hmenu, uMenuIndex+iItemCountMain, 
				MF_BYPOSITION, hBitmap, NULL);
			
			iItemCountMain++;
		}else
		{
			if (nCmdID == ESS_CMD_SEP_1 || nCmdID == ESS_CMD_SEP_2 || nCmdID == ESS_CMD_SEP_3
				 || nCmdID == ESS_CMD_SEP_4)
			{
				InsertMenu ( hSubMenu, iItemCountSub, MF_BYPOSITION|MF_SEPARATOR, 
					uidFirstCmd+iItemCountMain+iItemCountSub, _T("") );
			}
			else
			{
				InsertMenu ( hSubMenu, iItemCountSub, MF_BYPOSITION, 
					uidFirstCmd+iItemCountMain+iItemCountSub, strText.c_str());
				if (hBitmap)
					SetMenuItemBitmaps (hSubMenu, iItemCountSub, 
					MF_BYPOSITION, hBitmap, NULL);

				if (m_uItemCount!=1 && (nCmdID == ESS_CMD_HISTORY || 
					nCmdID == ESS_CMD_COMPARE || nCmdID == ESS_CMD_PROPERTIES))
					EnableMenuItem(hSubMenu, iItemCountSub, MF_BYPOSITION|MF_DISABLED|MF_GRAYED);
			}
			
			iItemCountSub++;
		}

		m_cmdMap.insert(VSSMapInt2Int::value_type(iItemCountMain+iItemCountSub-1, nCmdID));
	}
	
	InsertMenu ( hmenu, uMenuIndex+iItemCountMain, MF_BYPOSITION|MF_SEPARATOR, 
		uidFirstCmd+iItemCountMain+iItemCountSub, _T("") );
	iItemCountMain++;	
	
	
    return MAKE_HRESULT ( SEVERITY_SUCCESS, FACILITY_NULL, iItemCountMain+iItemCountSub );
}

HRESULT CMenuExt::GetCommandString (
									UINT  idCmd,
									UINT  uFlags,
									UINT* pwReserved,
									LPSTR pszName,
									UINT  cchMax )
{
    USES_CONVERSION;
	
	// Check idCmd, it must be 0 since we have only one menu item.
    //if ( 1 != idCmd )
	//  return E_INVALIDARG;
	
    // If Explorer is asking for a help string, copy our string into the
    // supplied buffer.
    if ( uFlags & GCS_HELPTEXT )
	{
		int nCmdID;
		VSSMapInt2Int::iterator theIterator;
		theIterator = m_cmdMap.find((int)idCmd);
        if(theIterator != m_cmdMap.end() )
			nCmdID = (*theIterator).second;
		
		VSSString strText;
		switch(nCmdID)
		{
		case ESS_CMD_ADD:
			strText = "Add to source control";
			break;
		case ESS_CMD_GET:
			strText = "Get latest version";
			break;
		case ESS_CMD_CHECKOUT:
			strText = "Check out";
			break;
		case ESS_CMD_CHECKIN:
			strText = "Check in";
			break;
		case ESS_CMD_UNDOOUT:
			strText = "Undo check out";
			break;
		case ESS_CMD_EXCLUDE:
			strText = "Exclude from source control";
			break;
		case ESS_CMD_DELETE:
			strText = "Delete at source control";
			break;
		case ESS_CMD_ABOUT:
			strText = "About Effective SourceSafe ...";
			break;
			case ESS_CMD_SUBMENU:
			strText = "Sou&rce Control";
			break;
		case ESS_CMD_ADD_R:
			strText = "Add to Source Control recursively";
			break;
		case ESS_CMD_GET_R:
			strText = "Get Latest Version recursively";
			break;
		case ESS_CMD_CHECKOUT_R:
			strText = "Check Out recursively";
			break;
		case ESS_CMD_CHECKIN_R:
			strText = "Check In recursively";
			break;
		case ESS_CMD_UNDOOUT_R:
			strText = "Undo Check Out recursively";
			break;
		case ESS_CMD_HISTORY:
			strText = "Show History";
			break;
		case ESS_CMD_COMPARE:
			strText = "Compare Versions";
			break;
		case ESS_CMD_PROPERTIES:
			strText = "SouceSafe Properties";
			break;
		case ESS_CMD_CONFIG:
			strText = "Configure Effective SourceSafe";
			break;
		case ESS_CMD_VSS:
			strText = "Load Visual SourceSafe Explorer";
			break;		
		case ESS_CMD_VSSADMIN:
			strText = "Load Visual SourceSafe Admin";
			break;
		}
		
        if ( uFlags & GCS_UNICODE )
		{
            // We need to cast pszName to a Unicode string, and then use the
            // Unicode string copy API.
            lstrcpynW ( (LPWSTR) pszName, A2CW(strText.c_str()), cchMax );
		}
        else
		{
            // Use the ANSI string copy API to return the help string.
            lstrcpynA ( pszName, T2CA(strText.c_str()), cchMax );
		}
		
        return S_OK;
	}
	
    return E_INVALIDARG;
}


static BOOL CALLBACK enumChildUpdateListCtrl(HWND hwnd, LPARAM lParam) 
{
	TCHAR szInfo[256] = "";
	
	GetWindowText(hwnd, szInfo, 256);
	if (lstrcmpi(szInfo, _T("FolderView")) != 0)
		return TRUE;
	
	GetClassName(hwnd, szInfo, 256);
	if (lstrcmpi(szInfo, _T("SysListView32")) != 0)
		return TRUE;
	
	int i = ListView_GetNextItem(hwnd, -1, LVNI_SELECTED|LVNI_ALL);
	while (i!=-1)
	{
		ListView_RedrawItems(hwnd, i, i);
		i = ListView_GetNextItem(hwnd, i, LVNI_SELECTED|LVNI_ALL);
	}
	return TRUE;
}

HRESULT CMenuExt::InvokeCommand ( LPCMINVOKECOMMANDINFO pCmdInfo )
{
  USES_CONVERSION;
  // If lpVerb really points to a string, ignore this function call and bail out.
    if ( 0 != HIWORD( pCmdInfo->lpVerb ))
        return E_INVALIDARG;
	
	// Get the command index - from 1 (0 is the separator).
	int nIndex = (int)LOWORD( pCmdInfo->lpVerb );
	
	VSSMapInt2Int::iterator theIterator;
	theIterator = m_cmdMap.find(nIndex);
	if(theIterator == m_cmdMap.end() )
		return E_INVALIDARG;
	
	int nCmdID = (*theIterator).second;
	
	switch (nCmdID)
	{
	case ESS_CMD_ABOUT:
		{
			VSSString strInfo;
			
			strInfo+="Effective SourceSafe\n";
			strInfo+="Version 0.1\n";
			strInfo+="Written by David Y. Zhao\n";
			strInfo+="\n";
			strInfo+="Latest information about this extension is available online at\n";
			strInfo+="http://www.ministars.com/programming/ess/\n";
			strInfo+="\n";
			strInfo+="Do you wish to visit the website now?\n";
			
			if (MessageBox(pCmdInfo->hwnd, strInfo.c_str(), "About Effective SourceSafe", 
				MB_YESNO|MB_ICONINFORMATION)==IDYES)
			{
				GotoURL("http://www.ministars.com/programming/ess/");
			}
			return S_OK;
		}
	case ESS_CMD_CONFIG:
		{
			TCHAR sFilename[MAX_PATH];
			TCHAR sPath[MAX_PATH];
			GetModuleFileName(_Module.GetModuleInstance(), sFilename, MAX_PATH);
			SplitPath(sFilename, sPath, NULL);
			//lstrcat(sPath, "config.exe");
			lstrcat(sPath, "essshell.ini");
			if (GetFileAttributes(sPath) == -1)
				MessageBox(pCmdInfo->hwnd, "The configure application could not be found", "Error", MB_OK|MB_ICONERROR);
			ShellExecute(pCmdInfo->hwnd, NULL, sPath, NULL, NULL, SW_SHOW);
		}
		return S_OK;
	case ESS_CMD_VSSADMIN:
	case ESS_CMD_VSS:
		{
			CLSID clsid;
			LPOLESTR pwszClsid;
			CHAR  szKey[128];
			CHAR  szCLSID[60];
			HKEY hKey;		
			
			TCHAR szPath[MAX_PATH];
			unsigned long cSize = MAX_PATH;
			
			// Get the CLSID using ProgID
			HRESULT hr = CLSIDFromProgID(L"SourceSafe", &clsid);
			if (FAILED(hr))
			{
				MessageBox(pCmdInfo->hwnd, 
					"Microsoft SourceSafe application could not be found in this system. CLSIDFromProgID failure.", 
					"Error", MB_OK|MB_ICONERROR);
				return S_OK;
			}
			
			// Convert CLSID to String
			hr = StringFromCLSID(clsid, &pwszClsid);
			if (FAILED(hr))
			{
				MessageBox(pCmdInfo->hwnd, 
					"Microsoft SourceSafe application could not be found in this system. StringFromCLSID failure.", 
					"Error", MB_OK|MB_ICONERROR);
				return S_OK;
			}
			
			// Convert result to ANSI
			WideCharToMultiByte(CP_ACP, 0, pwszClsid, -1, szCLSID, 60, NULL, NULL);
			
			// Free memory used by StringFromCLSID
			CoTaskMemFree(pwszClsid);
			
			// Format Registry Key string
			wsprintf(szKey, "CLSID\\%s\\InprocServer32", szCLSID);
			
			// Open key to find path of application
			LONG lRet = RegOpenKeyEx(HKEY_CLASSES_ROOT, szKey, 0, KEY_ALL_ACCESS, &hKey);
			if (lRet != ERROR_SUCCESS) 
			{
				MessageBox(pCmdInfo->hwnd, 
					"Microsoft SourceSafe application could not be found in this system. InprocServer32 not found.", 
					"Error", MB_OK|MB_ICONERROR);
				return S_OK;
			}
			
			// Query value of key to get Path and close the key
			lRet = RegQueryValueEx(hKey, NULL, NULL, NULL, (BYTE*)szPath, &cSize);
			RegCloseKey(hKey);
			if (lRet != ERROR_SUCCESS)
			{
				MessageBox(pCmdInfo->hwnd, 
						"Microsoft SourceSafe application could not be found in this system. key to Path not found.", 
						"Error", MB_OK|MB_ICONERROR);
				return S_OK;
			}


			TCHAR szAppPath[MAX_PATH];
			TCHAR szParam[MAX_PATH*3] ="";
			SplitPath(szPath, szAppPath, NULL);
			if (nCmdID == ESS_CMD_VSS)
			{
				lstrcat(szAppPath, "SSEXP.EXE");

				/*if (m_itemList.begin() != m_itemList.end())
				{
					vss_item* pItem = &*m_itemList.begin();
					
					pItem->nProfileID
				}*/
			}else if (nCmdID == ESS_CMD_VSSADMIN)
				lstrcat(szAppPath, "SSADMIN.EXE");

			ShellExecute(pCmdInfo->hwnd, NULL, szAppPath, NULL, NULL, SW_SHOW);				
		}
		return S_OK;		
	}
	
	
	int profile_count = g_profiles.Count();
	for (int p=0; p<profile_count; p++)
	{
		int nProfileID = g_profiles.GetProfileID(p);
		
		CVSSHelper* pVss = g_profiles.QueryVSSHelper(nProfileID);
		if (pVss == NULL || !pVss->IsValidVSS())
		{
			TCHAR szMessage[128];
			sprintf(szMessage, 
				"Cound not open database at\n%s\n\nPlease verify the database location, user id and password.\n",
				"");
			//MessageBox(NULL, szMessage, "Error", MB_OK|MB_ICONERROR);
			ATLTRACE(szMessage);
			g_profiles.ReleaseVSSHelper(nProfileID);
			continue;
		}
		
		VSSItemList::iterator itItem, itEndItem;
		for ( itItem = m_itemList.begin(), itEndItem = m_itemList.end(); itItem != itEndItem; itItem++ )
		{
			if (itItem->nProfileID != nProfileID)
				continue;
			vss_item* pItem = &*itItem;

			IVSSItemPtr objParent = pVss->GetSafeFolder(pItem->vsspath.c_str());
			if (objParent == NULL)
			{
				TCHAR szMessage[128];
				sprintf(szMessage, 
					"Cound not create project: \n%s\n\nPlease verify that you have right to create project in this location.",
					pItem->vsspath.c_str());
				MessageBox(pCmdInfo->hwnd, szMessage, "Error", MB_OK|MB_ICONERROR);
				continue;
			}
			switch(nCmdID)
			{
			case ESS_CMD_ADD:
				pVss->Add(objParent, pItem, false);
				break;
			case ESS_CMD_ADD_R:
				pVss->Add(objParent, pItem, true);
				break;
			case ESS_CMD_GET:
				pVss->Get(objParent, pItem, false);
				break;
			case ESS_CMD_GET_R:
				pVss->Get(objParent, pItem, true);
				break;
			case ESS_CMD_CHECKOUT:
        pVss->CheckOut(objParent, pItem, false);
        break;
			case ESS_CMD_CHECKOUT_R:
				pVss->CheckOut(objParent, pItem, true);
				break;
			case ESS_CMD_CHECKIN:
				pVss->CheckIn(objParent, pItem, false);
				break;
			case ESS_CMD_CHECKIN_R:
				pVss->CheckIn(objParent, pItem, true);
				break;
			case ESS_CMD_UNDOOUT:
        pVss->UndoCheckOut(objParent, pItem, false);
				break;
			case ESS_CMD_UNDOOUT_R:
				pVss->UndoCheckOut(objParent, pItem, true);
				break;
			case ESS_CMD_HISTORY:
				MessageBox(pCmdInfo->hwnd, "History is not yet implemented!", "Info", MB_OK|MB_ICONINFORMATION);
				break;
			case ESS_CMD_COMPARE:
				{
					TCHAR szVSSVersionPath[MAX_PATH];
					if (!pVss->GetTempVersion(objParent, pItem, szVSSVersionPath) || 
						GetFileAttributes(szVSSVersionPath) == -1)
						MessageBox(pCmdInfo->hwnd, "The version at SourceSafe could not be loaded!", 
						"Error", MB_OK|MB_ICONERROR);
					else
					{
						TCHAR szApp[MAX_PATH], szParam[MAX_PATH];
						g_profiles.GetCompareCmd(szApp);
						g_profiles.GetCompareParam(szParam);						
						VSSString strParam = szParam;
						VSSString str1, str2;
						str1+="\"";
						str1+=pItem->localpath;
						str1+=pItem->filename;
						str1+="\"";

						str2+="\"";
						str2+=szVSSVersionPath;
						str2+="\"";

						if (strParam.find("%1") != std::string::npos)
							strParam.replace(strParam.find("%1"), 2, str1);
						if (strParam.find("%2") != std::string::npos)
							strParam.replace(strParam.find("%2"), 2, str2);

						if ((int)ShellExecute(pCmdInfo->hwnd, NULL, szApp, strParam.c_str(), NULL, SW_SHOW)<=32)
						{
							VSSString strError("Comparison failed! Command\n\t");
							strError += szApp;
							strError += " ";
							strError += strParam;
							strError += "\ncould not be executed. Please check up the command.";
							MessageBox(pCmdInfo->hwnd, strError.c_str(), 
									"Error", MB_OK|MB_ICONERROR);
						}
					}
				}
				break;
			case ESS_CMD_EXCLUDE:
				MessageBox(pCmdInfo->hwnd, "Exclude is not yet implemented!", "Info", MB_OK|MB_ICONINFORMATION);
				break;
			case ESS_CMD_DELETE:
				pVss->Delete(objParent, pItem);
				break;
			case ESS_CMD_PROPERTIES:
				MessageBox(pCmdInfo->hwnd, "Properties is not yet implemented!", "Info", MB_OK|MB_ICONINFORMATION);
				break;
			}
		}
		g_profiles.ReleaseVSSHelper(nProfileID);
	}
	
	EnumChildWindows(pCmdInfo->hwnd, enumChildUpdateListCtrl, NULL);
	
	
	// Simulate a key press
	/*keybd_event( VK_F5, 0x45, KEYEVENTF_EXTENDEDKEY | 0, 0 );
	// Simulate a key release
	keybd_event( VK_F5, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
	*/
	
	return S_OK;
}



