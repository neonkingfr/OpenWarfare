/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// OverlayIconOut.h : Declaration of the COverlayIconOut

#ifndef __OverlayIconOut_H_
#define __OverlayIconOut_H_

#include "resource.h"       // main symbols
#include "OverlayIconBase.h"

/////////////////////////////////////////////////////////////////////////////
// COverlayIconOut

class ATL_NO_VTABLE COverlayIconOut : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<COverlayIconOut, &CLSID_OverlayIconOut>,
	public IOverlayIconOut,
	public IShellIconOverlayIdentifier,
	public COverlayIconBase
{
public:
	COverlayIconOut()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_OVERLAYICONOUT)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(COverlayIconOut)
	COM_INTERFACE_ENTRY(IOverlayIconOut)
	COM_INTERFACE_ENTRY(IShellIconOverlayIdentifier)
END_COM_MAP()

// IOverlayIconOut
public:

	// IShellIconOverlayIdentifier methods
	STDMETHODIMP GetOverlayInfo(LPWSTR pwszIconFile, int cchMax, int *pIndex, DWORD *pdwFlags);
	STDMETHODIMP GetPriority(int *pPriority);	
	STDMETHODIMP IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib);
};

#endif //__OverlayIconOut_H_
