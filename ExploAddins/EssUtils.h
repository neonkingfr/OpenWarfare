/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


#ifndef _VSS_UTILS_H_
#define _VSS_UTILS_H_

#pragma once

// Decode file list from an droped data object
// return size
int DecodeDropedFileList(LPDATAOBJECT pDataObj,
						  VSSStringList* pItemList);

// Break a fullpath to path and file name/or folder name
void SplitPath(LPCTSTR lpcsFullPath, LPTSTR lpsPath, LPTSTR lpsFileName);

// compare two strings in lower case,
// make sure that pstr1.length()<=size and pstr2.length()<=size 
// return true if match, false if not
bool CompareStringNI(VSSString* pstr1, VSSString* pstr2, int size);


LONG GetRegKey(HKEY key,LPCTSTR subkey,LPTSTR retdata);
void GotoURL(LPCTSTR url);
#endif // _VSS_UTILS_H_
