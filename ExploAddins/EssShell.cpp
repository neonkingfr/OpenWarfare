/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/

 // EssShell.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f EssShellps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "EssShell.h"

#include "EssShell_i.c"
#include "MenuExt.h"
#include "OverlayIconIn.h"
#include "OverlayIconOut.h"
#include "OverlayIconNew.h"
#include "OverlayIconOtherOut.h"


CComModule _Module;
CRuleProfileManager g_profiles;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(CLSID_MenuExt, CMenuExt)
OBJECT_ENTRY(CLSID_OverlayIconIn, COverlayIconIn)
OBJECT_ENTRY(CLSID_OverlayIconOut, COverlayIconOut)
OBJECT_ENTRY(CLSID_OverlayIconNew, COverlayIconNew)
OBJECT_ENTRY(CLSID_OverlayIconOtherOut, COverlayIconOtherOut)
END_OBJECT_MAP()

/////////////////////////////////////////////////////////////////////////////
// DLL Entry Point

extern "C"
BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID /*lpReserved*/)
{
    if (dwReason == DLL_PROCESS_ATTACH)
    {
        _Module.Init(ObjectMap, hInstance, &LIBID_ESSSHELLLib);
        DisableThreadLibraryCalls(hInstance);

		g_profiles.Init();
    }
    else if (dwReason == DLL_PROCESS_DETACH)
	{
		g_profiles.Term();
        _Module.Term();
	}
    return TRUE;    // ok
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
	if (_Module.GetLockCount()==0)
	{
		g_profiles.FreeUnusedVSSHelper();
		return S_OK;
	}else
		return S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    return _Module.RegisterServer(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
    return _Module.UnregisterServer(TRUE);
}
