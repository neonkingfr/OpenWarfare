// OverlayIconOtherOut.h : Declaration of the COverlayIconOtherOut

#ifndef __OverlayIconOtherOut_H_
#define __OverlayIconOtherOut_H_

#include "resource.h"       // main symbols
#include "OverlayIconBase.h"


// COverlayIconOtherOut

class ATL_NO_VTABLE COverlayIconOtherOut : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<COverlayIconOtherOut, &CLSID_OverlayIconOtherOut>,
  public IShellIconOverlayIdentifier, 
  public IDispatchImpl<IOverlayIconOtherOut, &IID_IOverlayIconOtherOut, &LIBID_ESSSHELLLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
  public COverlayIconBase
{
public:
	COverlayIconOtherOut()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_OVERLAYICONOTHEROUT)


BEGIN_COM_MAP(COverlayIconOtherOut)
	COM_INTERFACE_ENTRY(IOverlayIconOtherOut)
	COM_INTERFACE_ENTRY(IDispatch)
  COM_INTERFACE_ENTRY(IShellIconOverlayIdentifier)  
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IShellIconOverlayIdentifier methods
  STDMETHODIMP GetOverlayInfo(LPWSTR pwszIconFile, int cchMax, int *pIndex, DWORD *pdwFlags);
  STDMETHODIMP GetPriority(int *pPriority);	
  STDMETHODIMP IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib);

};

OBJECT_ENTRY_AUTO(__uuidof(OverlayIconOtherOut), COverlayIconOtherOut)

#endif
