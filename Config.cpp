#include <string>
#include <vector>
#include <fstream>

# if defined(_MSC_VER)
# ifndef _CRT_SECURE_NO_DEPRECATE
# define _CRT_SECURE_NO_DEPRECATE (1)
# endif
# pragma warning(disable : 4996)
# endif

#include "RefCount.hpp"
#include "LexicalAnalyser.hpp"
#include "SyntaxAnyaliser.hpp"
#include "Config.hpp"
#include "AppFrameWork.hpp"

ConfigEntry::ConfigEntry(std::string name):_name(name){}
ConfigEntry::ConfigEntry(std::string name, IConfigType *entry):_name(name),_entry(entry){}

// Check to see if this is a class, if not then just return
ConfigEntry* ConfigEntry::FindEntry(const std::string name)
{
  if (IsClass())
  {
    ConfigClass *cfgClass = GetClass();
    return cfgClass->FindEntry(name);
  }

  return NULL;
}

void ConfigEntry::SetEntry(IConfigType *entry)
{
  _entry = entry;
}

ConfigDataType ConfigEntry::GetType()
{
  if (_entry) return _entry->GetType();
  return DTUnknown;
}

ConfigEntry *ConfigClass::FindEntry(const std::string name)
{
  ConfigEntry* entry = NULL;
  for (unsigned int i=0; i<size(); ++i)
  {
    std::string entryName = (*this)[i]->GetEntryName();
    if (!stricmp(name.data(),entryName.data())) return (*this)[i];
  }

  return NULL;
}

void ConfigClass::Add(ConfigEntry *entry)
{
  push_back(entry);
}

void ConfigClass::Add(SmartRef<ConfigEntry> &entry)
{
  push_back(entry);
}

// Return this is a class.
bool ConfigClass::IsClass(){ return true; }
bool ConfigClass::IsEntry(){ return false; }

ConfigClass  *IConfigType::GetClass(){ return dynamic_cast<ConfigClass*>(this); }
ConfigArray  *IConfigType::GetArray(){ return dynamic_cast<ConfigArray*>(this); }

const char *GNewLineFeed = "\r\n";

void ParamaFileConfig::LoadFile(const char *path)
{
  // Clear everything...
  Clear();

  ifstream inputFile;
  inputFile.open(path,ios::in | ios::binary);
  LoadFile(inputFile);
}

void ParamaFileConfig::LoadFile(ifstream &inputStream)
{
  // Clear everything...
  Clear();

  if (inputStream)
  {   
    std::string fullFile;
    char buffer[1024] = {0};

    while (inputStream.good())
    {
      inputStream.read(buffer,1024);
      fullFile.append(buffer,inputStream.gcount());
    }

    SmartRef<LexicalAnalyser> lexier = new LexicalAnalyser(fullFile.data());
    SmartRef<SyntaxAnyaliser> syntax = new SyntaxAnyaliser;
    syntax->Parse(lexier,this);
  }
}

void ParamaFileConfig::SaveFile(ofstream &outputStream, int indent)
{
  // Config file does not have a name but it does have entry's
  for (size_t i=0; i<size(); i++)
      (*this)[i]->SaveFileConfig(outputStream,indent);
}

void ConfigClass::SaveFileConfig(ofstream &outputStream, int indent)
{
  std::string space(" ",indent);
  
  std::string buffer;
  buffer += space + "class " + GetName() + "\r\n";
  buffer += space + "{" + GNewLineFeed;
  outputStream.write(buffer.data(), buffer.length());
  
  // For each entry save their value
  for (size_t i=0; i<size(); i++)
    (*this)[i]->SaveFileConfig(outputStream,indent+3);
  
  buffer = space + "};" + GNewLineFeed;
  outputStream.write(buffer.data(), buffer.length());
}


void ConfigEntry::SaveFileConfig(ofstream &outputStream, int indent)
{
  // Call the entry
  if (_entry)
  {
    // Class have a special routine that they write out
    // their class leader
    std::string spaces;spaces.append(indent,' ');
    if (_entry->IsClass()) 
    {
      _entry->SaveFileConfig(outputStream,indent);
    }
    else if (_entry->IsArray())
    {
      std::string entryAssignment = spaces + _name + "[]=";

      // Write out the entry name. the [] and assignment is done by the value..
      outputStream.write(entryAssignment.data(),entryAssignment.length());
      _entry->SaveFileConfig(outputStream,indent+3);

      // Every entry is expected to be ended with ;
      const std::string endLine =  std::string(";") + GNewLineFeed;
      outputStream.write(endLine.data(),endLine.size());
    }
    else
    {
      std::string entryAssignment = spaces + _name + "=";
      
      // This is a entry, so every entry is expected to have a assignment =
      outputStream.write(entryAssignment.data(),entryAssignment.length());

      _entry->SaveFileConfig(outputStream,indent+3);

      // Every entry is expected to be ended with ;
      const std::string endLine =  std::string(";") + GNewLineFeed;
      outputStream.write(endLine.data(),endLine.size());
    }   
  }
}

void ConfigArray::SaveFileConfig(ofstream &outputStream, int indent)
{
  std::string arrayStart = "{";
  std::string delimiter = ",";
  std::string arrayEnd = "}";

  outputStream.write(arrayStart.data(),arrayStart.length());
  for (size_t i=0; i<size(); i++)
  {
    // no need for indent
    (*this)[i]->SaveFileConfig(outputStream, indent);

    // Write out the delimiter
    if (i<size()-1) outputStream.write(delimiter.data(),delimiter.size());
  }
  outputStream.write(arrayEnd.data(),arrayEnd.length());
}

// Called, for base types that are pretty straight forward
void IConfigType::SaveFileConfig(ofstream &outputStream, int indent)
{
  // Write out bool
  if (IsBool())
  {
    bool value = false;
    GetVal(value);
    std::string output = ::Format("%s",value ? "true" : "false");  
    outputStream.write(output.data(),output.size());
  }
  // Write out string
  if (IsString())
  {
    std::string value;
    GetVal(value);
    std::string output = ::Escape(value.data());
    output = ::Format("\"%s\"",output.data());  
    outputStream.write(output.data(),output.size());
  }
  // Write out scalar
  if (IsScalar())
  {
    float value;
    GetVal(value);
    std::string output = ::Format("%f",value);  
    outputStream.write(output.data(),output.size());
  }
  // Write out integer
  if (IsInteger())
  {
    int value;
    GetVal(value);
    std::string output = ::Format("%i",value);
    outputStream.write(output.data(),output.size());
  }
};