# Microsoft Developer Studio Project File - Name="EssShell" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=EssShell - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "EssShell.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "EssShell.mak" CFG="EssShell - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "EssShell - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "EssShell - Win32 Release MinDependency" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/ExploAddins/Ess", EGTAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "EssShell - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x41d /d "_DEBUG"
# ADD RSC /l 0x41d /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# Begin Custom Build - Performing registration
OutDir=.\Debug
TargetPath=.\Debug\EssShell.dll
InputPath=.\Debug\EssShell.dll
SOURCE="$(InputPath)"

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	copy             /y             files\*.*             debug\  
	
# End Custom Build

!ELSEIF  "$(CFG)" == "EssShell - Win32 Release MinDependency"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "ReleaseMinDependency"
# PROP BASE Intermediate_Dir "ReleaseMinDependency"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "ReleaseMinDependency"
# PROP Intermediate_Dir "ReleaseMinDependency"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "_ATL_STATIC_REGISTRY" /D "_ATL_MIN_CRT" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "_ATL_STATIC_REGISTRY" /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x41d /d "NDEBUG"
# ADD RSC /l 0x41d /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386 /out:"../output/EssShell.dll"
# Begin Custom Build - Performing registration
OutDir=.\ReleaseMinDependency
TargetPath=\C\ExploAddins\output\EssShell.dll
InputPath=\C\ExploAddins\output\EssShell.dll
SOURCE="$(InputPath)"

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	rem regsvr32 /s /c "$(TargetPath)" 
	rem echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	copy             /y             files\*.ico             ..\Output\  
	copy             /y             files\*.ini             ..\Output\  
	copy             /y             files\*.htm             ..\Output\  
	copy             /y             files\*.gif             ..\Output\  
	
# End Custom Build

!ENDIF 

# Begin Target

# Name "EssShell - Win32 Debug"
# Name "EssShell - Win32 Release MinDependency"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\EssShell.cpp
# End Source File
# Begin Source File

SOURCE=.\EssShell.def
# End Source File
# Begin Source File

SOURCE=.\EssShell.idl
# ADD MTL /tlb ".\EssShell.tlb" /h "EssShell.h" /iid "EssShell_i.c" /Oicf
# End Source File
# Begin Source File

SOURCE=.\EssShell.rc
# End Source File
# Begin Source File

SOURCE=.\EssUtils.cpp
# End Source File
# Begin Source File

SOURCE=.\MenuExt.cpp
# End Source File
# Begin Source File

SOURCE=.\OverlayIconBase.cpp
# End Source File
# Begin Source File

SOURCE=.\OverlayIconIn.cpp
# End Source File
# Begin Source File

SOURCE=.\OverlayIconNew.cpp
# End Source File
# Begin Source File

SOURCE=.\OverlayIconOut.cpp
# End Source File
# Begin Source File

SOURCE=.\RuleProfile.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\VSSHelper.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\EssUtils.h
# End Source File
# Begin Source File

SOURCE=.\MenuExt.h
# End Source File
# Begin Source File

SOURCE=.\OverlayIconBase.h
# End Source File
# Begin Source File

SOURCE=.\OverlayIconIn.h
# End Source File
# Begin Source File

SOURCE=.\OverlayIconNew.h
# End Source File
# Begin Source File

SOURCE=.\OverlayIconOut.h
# End Source File
# Begin Source File

SOURCE=.\ProfileMan.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\RuleProfile.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\VSSHelper.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\add.bmp
# End Source File
# Begin Source File

SOURCE=.\res\checkin.bmp
# End Source File
# Begin Source File

SOURCE=.\res\checkout.bmp
# End Source File
# Begin Source File

SOURCE=.\res\history.bmp
# End Source File
# Begin Source File

SOURCE=.\files\in.ico
# End Source File
# Begin Source File

SOURCE=.\MenuExt.rgs
# End Source File
# Begin Source File

SOURCE=.\files\new.ico
# End Source File
# Begin Source File

SOURCE=.\files\out.ico
# End Source File
# Begin Source File

SOURCE=.\OverlayIconIn.rgs
# End Source File
# Begin Source File

SOURCE=.\OverlayIconNew.rgs
# End Source File
# Begin Source File

SOURCE=.\OverlayIconOut.rgs
# End Source File
# Begin Source File

SOURCE=.\res\properti.bmp
# End Source File
# Begin Source File

SOURCE=.\files\screen.gif
# End Source File
# End Group
# Begin Source File

SOURCE=.\files\EssShell.ini
# End Source File
# Begin Source File

SOURCE=.\files\info.htm
# End Source File
# Begin Source File

SOURCE=.\SourceSafe6.tlb
# End Source File
# End Target
# End Project
