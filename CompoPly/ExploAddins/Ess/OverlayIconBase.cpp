/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// OverlayIconBase.cpp: implementation of the OverlayIconBase class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OverlayIconBase.h"

#include "EssUtils.h"
#include "RuleProfile.h"
#include "VSSHelper.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COverlayIconBase::COverlayIconBase()
{

}

COverlayIconBase::~COverlayIconBase()
{
}

void COverlayIconBase::QueryIcon(LPWSTR pwszIconFile, int cchMax, LPCTSTR lpcsIcoName)
{
	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	if(!GetVersionEx(&osv))
		return;
	// Only >=me and 2000
	if (osv.dwMajorVersion == 4 && osv.dwMinorVersion < 90 || osv.dwMajorVersion < 4)
		return;

	TCHAR sFilename[_MAX_PATH];
	TCHAR sPath[_MAX_PATH];
	GetModuleFileName(_Module.GetModuleInstance(), sFilename, _MAX_PATH);
	SplitPath(sFilename, sPath, NULL);
	lstrcat(sPath, lpcsIcoName);

	wchar_t widePath[_MAX_PATH * 2+10];
	int wLength = MultiByteToWideChar(CP_ACP, 0, sPath, -1, widePath, _MAX_PATH*2+10-1);
	widePath[wLength] = 0;

	wcsncpy(pwszIconFile, widePath, cchMax);
}

int COverlayIconBase::IsMemberOf(LPCWSTR pwszPath, DWORD /* dwAttrib */)
{
	if (!g_profiles.IsOverlayEnabled())
		return Unknown;

	char szFilePath[MAX_PATH], szParentPath[MAX_PATH], szFileName[_MAX_FNAME];
	TCHAR szVSSPath[MAX_PATH];

	int nLength = WideCharToMultiByte(CP_ACP, 0, pwszPath, -1, szFilePath, MAX_PATH - 1, NULL, NULL);
	szFilePath[nLength] = 0;

	SplitPath(szFilePath, szParentPath, szFileName);

	int profile_count = g_profiles.Count();
	for (int p=0; p<profile_count; p++)
	{
		int nProfileID = g_profiles.GetProfileID(p);
		
		if (!g_profiles.ParseFromCurrentPath(nProfileID, szParentPath, szVSSPath))
			continue;

		if (g_profiles.CheckItem(nProfileID, szFilePath))
		{
			CVSSHelper* pVss = g_profiles.QueryVSSHelper(nProfileID);
			if (pVss==NULL || !pVss->IsValidVSS())
			{
				g_profiles.ReleaseVSSHelper(nProfileID);
				continue;
			}

      int nIcon = Unknown;
			IVSSItemPtr objParent = pVss->GetSafeFolder(szVSSPath, false);
			if (objParent == NULL)
				nIcon = IconNew; // New items to add
			else
			{			
        IVSSItemPtr objItem = pVss->GetSafeItem(objParent, szFileName);
				if (objItem == NULL)
					nIcon = IconNew; // New items to add
				else if (pVss->IsCheckedOut(objItem))
        {
          LPWSTR chname=NULL;           
          //LPWSTR userName = pVss->GetUserName();
          if (pVss->GetCheckedOuterName(objItem, chname)) nIcon = IconOut; // out
          else nIcon = IconOtherOut; // out by another user
        }
				else
					nIcon = IconIn;
			}
      g_profiles.ReleaseVSSHelper(nProfileID);
			return nIcon;
		}
	}
	return Unknown;
}