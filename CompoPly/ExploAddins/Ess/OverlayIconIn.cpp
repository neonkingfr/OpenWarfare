/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// OverlayIconIn.cpp : Implementation of COverlayIconIn
#include "stdafx.h"
#include "EssShell.h"
#include "OverlayIconIn.h"

#include "EssUtils.h"

/////////////////////////////////////////////////////////////////////////////
// COverlayIconIn

STDMETHODIMP COverlayIconIn::GetOverlayInfo(LPWSTR pwszIconFile, int cchMax, int *pIndex, DWORD *pdwFlags)
{
	COverlayIconBase::QueryIcon(pwszIconFile, cchMax, "in.ico");
	*pIndex = 0;
	*pdwFlags = ISIOI_ICONFILE;
	
	return S_OK;
};

STDMETHODIMP COverlayIconIn::GetPriority(int *pPriority)
{
	*pPriority = 14;
	return S_OK;
}

STDMETHODIMP COverlayIconIn::IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib )
{
  //if (!wcscmp(pwszPath, L"W:\\C\\Poseidon\\lib\\tankrb.cpp")) OutputDebugString("COverlayIconIn\n");
  //if (!wcscmp(pwszPath, L"W:\\C\\AITools\\FSMCompiler\\FSMGraph.cpp")) OutputDebugString("COverlayIconIn\n");
  int type = COverlayIconBase::IsMemberOf(pwszPath, dwAttrib);
	if (type == IconIn)
		return S_OK;
	return S_FALSE;
}