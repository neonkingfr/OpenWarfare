/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// OverlayIconOut.cpp : Implementation of COverlayIconOut
#include "stdafx.h"
#include "EssShell.h"
#include "OverlayIconOut.h"

#include "EssUtils.h"

/////////////////////////////////////////////////////////////////////////////
// COverlayIconOut

STDMETHODIMP COverlayIconOut::GetOverlayInfo(LPWSTR pwszIconFile, int cchMax, int *pIndex, DWORD *pdwFlags)
{
	COverlayIconBase::QueryIcon(pwszIconFile, cchMax, "out.ico");
	*pIndex = 0;
	*pdwFlags = ISIOI_ICONFILE;
	
	return S_OK;
};

STDMETHODIMP COverlayIconOut::GetPriority(int *pPriority)
{
	*pPriority = 10;
	return S_OK;
}

STDMETHODIMP COverlayIconOut::IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib )
{
  //if (!wcscmp(pwszPath, L"W:\\C\\Poseidon\\lib\\tankrb.cpp")) OutputDebugString("COverlayIconOut\n");
  //if (!wcscmp(pwszPath, L"W:\\C\\AITools\\FSMCompiler\\FSMGraph.cpp")) OutputDebugString("COverlayIconOut\n");
  int type = COverlayIconBase::IsMemberOf(pwszPath, dwAttrib);
	if (type == IconOut)
		return S_OK;
	return S_FALSE;
}