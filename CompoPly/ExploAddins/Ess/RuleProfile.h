/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// RuleProfile.h: interface for the CRuleProfile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RULEPROFILE_H__7F56550D_C2B4_4908_95CA_4651F8E45AF2__INCLUDED_)
#define AFX_RULEPROFILE_H__7F56550D_C2B4_4908_95CA_4651F8E45AF2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRuleProfileManager;
class CVSSHelper;

class CFileRule
{
	VSSStringList m_ruleList;

	bool MatchPattern (const TCHAR *pat, const TCHAR *str);
public:
	CFileRule();	
	void AddRule(LPCTSTR lpcsRule);	
	BOOL CheckFile(LPCTSTR lpcsFileName, LPCTSTR lpcsFilePath);

	void Clear();
};

class CRuleProfile
{
public:
	~CRuleProfile();
private:
	friend CRuleProfileManager;

	bool bErrMsgDone;
	VSSString strVSSData;
	VSSString strUser;
	VSSString strPwd;
	
	// Root path
	// E.g $/ = E:\Projects\ 
	VSSString strVSSRoot;
	VSSString strLocalRoot;
	
	// Current path
	// E.g. $/SafeClean/SafeCln/ = E:\projects\SafeClean\SafeCln\ 
	//VSSString strCurrentPath;
	//VSSString strVSSPath;
	
	CFileRule ruleIncludes;
	CFileRule ruleExcludes;

	CVSSHelper* m_pVSSHelper;
	int nRef;	
	CVSSHelper* QueryVSSHelper();
	void ReleaseVSSHelper();
	bool FreeUnusedVSSHelper();

	CRuleProfile(LPCTSTR lpcsVSSData, LPCTSTR lpcsUser, LPCTSTR lpcsPwd
		, LPCTSTR lpcsLocalRoot, LPCTSTR lpcsVSSRoot);

	void InitRules(CProfileManager* pProfile, LPCTSTR lpcsSection);

	// Return false if the path is outside the root folder
	bool ParseFromCurrentPath(LPCTSTR/*in*/ lpcsCurrentPath, LPTSTR/*out*/ lpsVSSPath);

	// Check if an item matches the rule
	bool CheckItem(LPCTSTR lpcsItemPath);
};


struct vss_item
{
	// C:\hej\ 
	VSSString localpath;
	// $/hej/ 
	VSSString vsspath;
	// file name
	VSSString filename;
	//CRuleProfile* pProfile;
	int nProfileID;
};

typedef std::vector<CRuleProfile> VSSProfileVector;
typedef std::list<vss_item> VSSItemList;
typedef std::map<int, int, std::less<int> > VSSMapInt2Int;

class CRuleProfileManager
{
	CComCriticalSection m_cs;
	VSSProfileVector m_profileVector;
	int m_nIDOffset;
	bool m_bEnableOverlay;
	bool m_bEnableVSSCaching;
	VSSString m_strCompareCmd, m_strCompareParam;

	HANDLE	m_hThreadFileMonitor;
	HANDLE	m_hThreadFreeUnusedItems;
	static DWORD WINAPI ThreadFileMonitor(LPVOID pParam);
	static DWORD WINAPI ThreadFreeUnusedItems(LPVOID pParam);

	void CleanupThread(HANDLE& hThread, bool bForceTerminate);
public:	
	CRuleProfileManager();
	virtual ~CRuleProfileManager();

	void Init(bool bIsReload=false);
	void Term();

	//const char* GetValue(const char* section, const char* item);
	//int GetValueInt(const char* section, const char* item);
	inline bool IsOverlayEnabled()
	{
		return m_bEnableOverlay;
	}
	inline bool IsVSSCachingEnabled()
	{
		return m_bEnableVSSCaching;
	}

	inline void GetCompareCmd(LPTSTR lpsVal)
	{
		m_cs.Lock();
		lstrcpy(lpsVal, m_strCompareCmd.c_str());
		m_cs.Unlock();
	}

	inline void GetCompareParam(LPTSTR lpsVal)
	{
		m_cs.Lock();
		lstrcpy(lpsVal, m_strCompareParam.c_str());
		m_cs.Unlock();
	}

	int Count();
	int GetProfileID(int iPos);

public:
	// Return false if the path is outside the root folder
	bool ParseFromCurrentPath(int nProfileID, LPCTSTR/*in*/ lpcsCurrentPath, LPTSTR/*out*/ lpsVSSPath);

	// Check if an item matches the rule
	bool CheckItem(int nProfileID, LPCTSTR lpcsItemPath);

	CVSSHelper* QueryVSSHelper(int nProfileID);
	void ReleaseVSSHelper(int nProfileID);	
	bool FreeUnusedVSSHelper();
};

extern CRuleProfileManager g_profiles;

#endif // !defined(AFX_RULEPROFILE_H__7F56550D_C2B4_4908_95CA_4651F8E45AF2__INCLUDED_)
