/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// MenuExt.h : Declaration of the CMenuExt

#ifndef __MENUEXT_H_
#define __MENUEXT_H_

#include "resource.h"       // main symbols

struct __declspec(uuid("000214e4-0000-0000-c000-000000000046")) IContextMenu;


#include "FileRule.h"
#include <shlobj.h>
#include <comdef.h>

#include "ruleprofile.h"
extern CRuleProfileManager g_profiles;


/////////////////////////////////////////////////////////////////////////////
// CMenuExt
class ATL_NO_VTABLE CMenuExt : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CMenuExt, &CLSID_MenuExt>,
	public IMenuExt,
	public IShellExtInit,
	public IContextMenu
{
private:
	VSSItemList m_itemList;
	bool m_bListContainsFolder;

	VSSMapInt2Int m_cmdMap;
	UINT	m_uItemCount;
	HBITMAP m_hbmpAdd, m_hbmpIn, m_hBmpOut, m_hBmpHistory, m_hBmpProperties;
public:
	CMenuExt()
	{
		m_hbmpAdd = LoadBitmap(_Module.GetModuleInstance(),
			MAKEINTRESOURCE(IDB_ADD));
		m_hbmpIn = LoadBitmap(_Module.GetModuleInstance(),
			MAKEINTRESOURCE(IDB_CHECKIN));
		m_hBmpOut = LoadBitmap(_Module.GetModuleInstance(),
			MAKEINTRESOURCE(IDB_CHECKOUT));
		m_hBmpHistory = LoadBitmap(_Module.GetModuleInstance(),
			MAKEINTRESOURCE(IDB_HISTORY));
		m_hBmpProperties = LoadBitmap(_Module.GetModuleInstance(),
			MAKEINTRESOURCE(IDB_PROPERTIES));
	}
	~CMenuExt()
	{
		if (m_hbmpAdd != NULL)
			DeleteObject(m_hbmpAdd);
		if (m_hbmpIn != NULL)
			DeleteObject(m_hbmpIn);		
		if (m_hBmpOut != NULL)
			DeleteObject(m_hBmpOut);		
		if (m_hBmpHistory != NULL)
			DeleteObject(m_hBmpHistory);
		if (m_hBmpProperties != NULL)
			DeleteObject(m_hBmpProperties);
	}

DECLARE_REGISTRY_RESOURCEID(IDR_MENUEXT)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CMenuExt)
	COM_INTERFACE_ENTRY(IMenuExt)
	COM_INTERFACE_ENTRY(IShellExtInit)
	COM_INTERFACE_ENTRY(IContextMenu)
END_COM_MAP()

// IMenuExt
public:
	void FinalRelease()
	{
		g_profiles.FreeUnusedVSSHelper();
	}
	
	// IShellExtInit
    STDMETHOD(Initialize)(LPCITEMIDLIST, LPDATAOBJECT, HKEY);

	// IContextMenu
    STDMETHOD(GetCommandString)(UINT, UINT, UINT*, LPSTR, UINT);
    STDMETHOD(InvokeCommand)(LPCMINVOKECOMMANDINFO);
    STDMETHOD(QueryContextMenu)(HMENU, UINT, UINT, UINT, UINT);
};

#endif //__MENUEXT_H_
