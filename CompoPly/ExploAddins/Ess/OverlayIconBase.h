/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


// OverlayIconBase.h: interface for the OverlayIconBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OVERLAYICONBASE_H__149CB97A_64D8_45E1_A83F_FF4E2C4B7CA7__INCLUDED_)
#define AFX_OVERLAYICONBASE_H__149CB97A_64D8_45E1_A83F_FF4E2C4B7CA7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ruleprofile.h"
#include <shlobj.h>
#include <comdef.h>

class COverlayIconBase  
{
protected:
	enum IconType
	{
		Unknown,
		IconIn,
		IconOut,
		IconNew,
    IconOtherOut
	};
public:
	COverlayIconBase();
	virtual ~COverlayIconBase();

	void QueryIcon(LPWSTR pwszIconFile, int cchMax, LPCTSTR lpcsIcoName);

	int IsMemberOf(LPCWSTR pwszPath, DWORD /* dwAttrib */);
};

#endif // !defined(AFX_OVERLAYICONBASE_H__149CB97A_64D8_45E1_A83F_FF4E2C4B7CA7__INCLUDED_)
