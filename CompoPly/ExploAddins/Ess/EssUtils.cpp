/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/

#include "stdafx.h"
#include "EssUtils.h"



int DecodeDropedFileList(LPDATAOBJECT pDataObj,
						  VSSStringList* pItemList)
{
	pItemList->clear();

	TCHAR     szFile    [MAX_PATH];
	HDROP     hdrop;
	FORMATETC etc = { CF_HDROP, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
	STGMEDIUM stg = { TYMED_HGLOBAL };
	bool      bChangedDir = false;
	// Read the list of folders from the data object.  They're stored in HDROP
	// form, so just get the HDROP handle and then use the drag 'n' drop APIs
	// on it.
	if ( FAILED( pDataObj->GetData ( &etc, &stg )))
		return E_INVALIDARG;
	
	// Get an HDROP handle.
	hdrop = (HDROP) GlobalLock ( stg.hGlobal );
	
	if ( NULL == hdrop )
	{
		ReleaseStgMedium ( &stg );
		return E_INVALIDARG;
	}
	
	// Determine how many files are involved in this operation.
	int uItemCount = DragQueryFile ( hdrop, 0xFFFFFFFF, NULL, 0 );
	
	for ( UINT uFile = 0; uFile < uItemCount; uFile++ )
	{
		// Get the next filename.
		if ( 0 == DragQueryFile ( hdrop, uFile, szFile, MAX_PATH ))
			continue;
		
		
		// Remove the \\ if there is any
		if (szFile[lstrlen(szFile)-1] == '\\')
			szFile[lstrlen(szFile)-1] = '\0';
		
		pItemList->push_back ( szFile );		
	}
	
	// Release resources.
	GlobalUnlock ( stg.hGlobal );
	ReleaseStgMedium ( &stg );

	return uItemCount;
}

void SplitPath(LPCTSTR lpcsFullPath, LPTSTR lpsPath, LPTSTR lpsFileName)
{
	if (lstrlen(lpcsFullPath)==0)
	{
		lstrcpy(lpsPath, "");
		lstrcpy(lpsFileName, "");
		return;
	}
	
	TCHAR sDrive[_MAX_DRIVE];
	TCHAR sDir[_MAX_DIR];
	TCHAR sFname[_MAX_FNAME];
	TCHAR sExt[_MAX_EXT];

	if (lpcsFullPath[lstrlen(lpcsFullPath)-1] == '\\')
	{
		TCHAR szFullPath[MAX_PATH];
		lstrcpy(szFullPath, lpcsFullPath);
		szFullPath[lstrlen(szFullPath)-1] = '\0';
		
		_tsplitpath(szFullPath,sDrive,sDir,sFname,sExt);
	}else
		_tsplitpath(lpcsFullPath,sDrive,sDir,sFname,sExt);

	if (lpsPath != NULL)
	{
		lstrcpy(lpsPath, sDrive);
		lstrcat(lpsPath, sDir);
	}

	if (lpsFileName != NULL)
	{
		lstrcpy(lpsFileName, sFname);
		lstrcat(lpsFileName, sExt);	
	}
}

bool CompareStringNI(VSSString* pstr1, VSSString* pstr2, int size)
{
	VSSString::iterator it, end;
	int compared;
	for (it=pstr1->begin(), end=pstr1->end(), compared=0; 
		compared<size && it!=end; compared++, it++)
	{
		TCHAR sz1[2] = "0";
		TCHAR sz2[2] = "0";
		sz1[0] = pstr1->at(compared);
		sz2[0] = pstr2->at(compared);
		if (lstrcmpi(sz1, sz2)!=0)
			return false;
	}
	return true;
}

LONG GetRegKey(HKEY key,LPCTSTR subkey,LPTSTR retdata)
{
	HKEY hkey;
	LONG retval=RegOpenKeyEx(key,subkey,0,KEY_QUERY_VALUE,&hkey);

	if (retval==ERROR_SUCCESS)
	{
			long datasize=MAX_PATH;
		TCHAR data[MAX_PATH];
		RegQueryValue(hkey,NULL,data,&datasize);
		lstrcpy(retdata,data);
		RegCloseKey(hkey);
	}

	return retval;
}

void GotoURL(LPCTSTR url)
{
	TCHAR key[MAX_PATH+MAX_PATH];

	// First try ShellExecute()
	HINSTANCE result=ShellExecute(NULL,_T("open"),url,NULL,NULL,SW_SHOW);

	// If it failed, get the .htm regkey and lookup the program
	if ((UINT)result<=HINSTANCE_ERROR)
	{

		if (GetRegKey(HKEY_CLASSES_ROOT,_T(".htm"),key)==ERROR_SUCCESS)
		{
				lstrcat(key,_T("\\shell\\open\\command"));

			if (GetRegKey(HKEY_CLASSES_ROOT,key,key)==ERROR_SUCCESS)
			{
					TCHAR*pos;
				pos=_tcsstr(key,_T("\"%1\""));
				if (pos==NULL)
				{
						// No quotes found
						pos=strstr(key,_T("%1"));	// Check for %1, without quotes
					if (pos==NULL)				// No parameter at all...
						pos=key+lstrlen(key)-1;
					else
						*pos='\0';				// Remove the parameter
				}
				else
					*pos='\0';					// Remove the parameter

				lstrcat(pos,_T(" "));
				lstrcat(pos,url);
				result=(HINSTANCE) WinExec(key,SW_SHOW);
			}
		}
	}

	if ((UINT)result<=HINSTANCE_ERROR)
	{

	}
}