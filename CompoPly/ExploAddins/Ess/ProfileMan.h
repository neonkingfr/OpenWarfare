/*---------------------------------------------------------------------------*
 *                         Effective SourceSafe                              *
 *---------------------------------------------------------------------------*
 * Copyright (c) 2001-2003 by David Y. Zhao                                  *
 *                                                                           *
 * Permission to use, copy, modify, and distribute this software and its     *
 * documentation under the terms of the GNU General Public License is hereby *
 * granted. No representations are made about the suitability of this        *
 * software for any purpose. It is provided "as is" without expressed or     *
 * implied warranty. See the GNU General Public License for more details.    *
 *---------------------------------------------------------------------------*/


#ifndef _PROFILEMAN_H_
#define _PROFILEMAN_H_

/***********************************************************
	Things that can be optimized
	- Read from ini file, read line by line or 
	  80 bytes each time
	- Each start node takes the unnessary space
	  try to remove
***********************************************************/
 
class CProfileManager
{
public:
	class CSectionNode
	{
	private:
		char* m_pchSection;
		CSectionNode*	m_pNextNode;
	public:
		CSectionNode()
		{
			m_pchSection = 0;
			m_pNextNode = NULL;
		}
		
		CSectionNode(const char* pchSection)
		{
			m_pchSection = new char[lstrlen(pchSection)+1];
			if (m_pchSection)
				lstrcpy(m_pchSection, pchSection);
			m_pNextNode = NULL;
		}
		
		~CSectionNode()
		{
			CleanUp();
		}
		
		void CleanUp()
		{
			if (m_pNextNode)
			{
				m_pNextNode->CleanUp();				
				delete m_pNextNode;
			}				
			
			if (m_pchSection)
				delete[] m_pchSection;
			m_pchSection = 0;
			m_pNextNode = NULL;
		}
		
		CSectionNode* GetNextNode() {return m_pNextNode;}
		bool	HasNextNode()	{return m_pNextNode!=NULL;}
		void	AddTailNode(CSectionNode* pNode)
		{
			if (m_pNextNode)
				m_pNextNode->AddTailNode(pNode);
			else
				m_pNextNode = pNode;
		}
		CSectionNode* FindSection(const char* pchSection)
		{
			if (pchSection == NULL)
				return NULL;
			
			if (lstrcmpi(pchSection, m_pchSection) == 0)
				return this;
			
			CSectionNode* pNext = GetNextNode();
			if (pNext)
				return pNext->FindSection(pchSection);
			else
				return NULL;
		}
		
		// Remove the node 
		// Important this does not check items that link to this section
		bool DeleteSection(const char* pchSection, CSectionNode *pLastNode=NULL)
		{
			if (pchSection == NULL)
				return false;
			if (pLastNode == NULL)
				return DeleteSection(pchSection, this);
			
			if (m_pchSection && lstrcmpi(pchSection, m_pchSection) == 0)
			{
				pLastNode->m_pNextNode = m_pNextNode;				
				m_pNextNode = NULL;
				CleanUp();
				delete this;
				return true;
			}
			
			CSectionNode* pNext = GetNextNode();
			if (pNext)
				return pNext->DeleteSection(pchSection, pLastNode);
			else
				return false;
		}
		int	GetLength()
		{
			if (m_pNextNode)
				return m_pNextNode->GetLength()+1;
			else
				return 1;
		}
		char* GetSection()	{return m_pchSection;}
	};
	
	class CItemNode
	{
	private:
		CSectionNode* m_pNodeSection;
		char* m_pchItem;
		char* m_pchValue;
		CItemNode*	m_pNextNode;
		
	public:
		CItemNode()
		{
			m_pNodeSection = 0;
			m_pchItem = 0;
			m_pchValue = 0;
			m_pNextNode = NULL;
		}
		
		CItemNode(CSectionNode* pNodeSection, const char* pchItem, const char* pchValue)
		{
			m_pNodeSection = pNodeSection;
			
			m_pchItem = new char[lstrlen(pchItem)+1];
			if (m_pchItem)
				lstrcpy(m_pchItem, pchItem);
			
			m_pchValue = new char[lstrlen(pchValue)+1];
			if (m_pchValue)
				lstrcpy(m_pchValue, pchValue);
			
			m_pNextNode = NULL;
		}
		
		~CItemNode()
		{
			CleanUp();
		}
		
		void CleanUp()
		{
			if (m_pNextNode)
			{
				m_pNextNode->CleanUp();
				
				delete m_pNextNode;
			}			
			if (m_pchItem)
				delete[] m_pchItem;
			
			if (m_pchValue)
				delete[] m_pchValue;
			
			m_pNodeSection = 0;
			m_pchItem = 0;
			m_pchValue = 0;
			m_pNextNode = NULL;
		}

		CItemNode* GetNextNode() {return m_pNextNode;}
		bool	HasNextNode()	{return m_pNextNode!=NULL;}
		CItemNode* FindItem(const char* pchSection, const char* pchItem)
		{
			if (pchSection == NULL || pchItem == NULL)
				return NULL;

			if (m_pNodeSection && m_pchItem && 
				lstrcmpi(pchSection, m_pNodeSection->GetSection()) == 0 &&
				lstrcmpi(pchItem, m_pchItem) == 0)
				return this;

			CItemNode* pNext = GetNextNode();
			if (pNext)
				return pNext->FindItem(pchSection, pchItem);
			else
				return NULL;
		}
		void	AddTailNode(CItemNode* pNode)
		{
			if (m_pNextNode)
				m_pNextNode->AddTailNode(pNode);
			else
				m_pNextNode = pNode;
		}
		int	GetLength(){
			if (m_pNextNode)
				return m_pNextNode->GetLength()+1;
			else
				return 1;
		}
		char* GetSection()	{return m_pNodeSection->GetSection();}
		char* GetItem()	{return m_pchItem;}
		char* GetValue()	{return m_pchValue;}
		void  SetValue(const char* chNewValue)	
		{
			if (m_pchValue)
				delete[] m_pchValue;
			
			m_pchValue = new char[lstrlen(chNewValue)+1];
			if (m_pchValue)
				lstrcpy(m_pchValue, chNewValue);
		}
		
		// Remove the node and make sure that the list is still working
		bool DeleteItem(const char* pchSection, const char* pchItem, 
			CItemNode *pLastNode=NULL)
		{
			if (pchSection == NULL || pchItem == NULL)
				return false;
			if (pLastNode == NULL)
				return DeleteItem(pchSection, pchItem, this);
			
			if (m_pNodeSection && m_pchItem && 
				lstrcmpi(pchSection, m_pNodeSection->GetSection()) == 0 &&
				lstrcmpi(pchItem, m_pchItem) == 0)
			{
				pLastNode->m_pNextNode = m_pNextNode;
				
				m_pNextNode = NULL;
				CleanUp();
				delete this;
				return true;
			}
			
			CItemNode* pNext = GetNextNode();
			if (pNext)
				return pNext->DeleteItem(pchSection, pchItem, this);
			else
				return false;
		}

		// Remove the node from a section
		void DeleteItem(const char* pchSection, CItemNode *pLastNode=NULL)
		{
			if (pchSection == NULL)
				return;
			if (pLastNode == NULL)
			{
				DeleteItem(pchSection, this);
				return;
			}
			
			CItemNode* pNext = GetNextNode();
			if (pNext)
				pNext->DeleteItem(pchSection, this);

			if (m_pNodeSection && 
				lstrcmpi(pchSection, m_pNodeSection->GetSection()) == 0)
			{
				pLastNode->m_pNextNode = m_pNextNode;
				
				m_pNextNode = NULL;
				CleanUp();
				delete this;
			}
		}
	};
	
	CItemNode m_nodeTable[70];
	CSectionNode m_sectionTable[70];
public:	
	int GetNrOfSections()
	{
		int i=0;
		for (int p=0; p<70; p++)
		{			
			CSectionNode* pNode =  m_sectionTable[p].GetNextNode();
			
			while (pNode)
			{
				i++;
				pNode = pNode->GetNextNode();
			}
		}
		
		return i;
	}
	
	int GetNrOfItems()
	{
		int i=0;
		for (int p=0; p<70; p++)
		{			
			CItemNode* pNode =  m_nodeTable[p].GetNextNode();
			
			while (pNode)
			{
				i++;
				pNode = pNode->GetNextNode();
			}
		}
		
		return i;			
	}

	// The callback function parse should return false if to interupt
	bool EnumAllValues(bool (*Parse)(LPCTSTR lptrSection,LPCTSTR lptrItem,LPCTSTR lptrValue,DWORD lParam1,DWORD lParam2),DWORD lParam1,DWORD lParam2)
	{
		char* strSection;
		char* strItem;
		char* strValue;

		for (int i=0; i<70; i++)
		{
			CItemNode* pNode =  m_nodeTable[i].GetNextNode();

			while (pNode)
			{
				strSection = pNode->GetSection();
				strItem = pNode->GetItem();
				strValue = pNode->GetValue();

				if (!Parse(strSection,strItem,strValue,lParam1,lParam2))
					return false;
				
				pNode = pNode->GetNextNode();
			}
		}
		return true;
	}
public:
	
	CProfileManager()
	{
	}

	~CProfileManager()
	{
		ClearAll();
	}

	// lpSubKey = e,g.Software\\Ministars Software\\ExplorerX
	bool WriteToRegistry(HKEY hKeyRoot, LPCTSTR lpSubKey)
	{
		char chKeyRoot[MAX_PATH], chKeyCurrent[MAX_PATH];
		lstrcpy(chKeyRoot, lpSubKey);
		lstrcat(chKeyRoot, "\\");

		HKEY hKey = NULL;

		char* strSection;
		char* strItem;
		char* strValue;

		for (int i=0; i<70; i++)
		{
			CItemNode* pNode =  m_nodeTable[i].GetNextNode();

			while (pNode)
			{
				strSection = pNode->GetSection();
				strItem = pNode->GetItem();
				strValue = pNode->GetValue();

				lstrcpy(chKeyCurrent, chKeyRoot);
				lstrcat(chKeyCurrent, strSection);
				try
				{
					if (RegOpenKeyEx (hKeyRoot, chKeyCurrent, 0L, KEY_ALL_ACCESS, &hKey)
						!= ERROR_SUCCESS)
					{
						// Create Key
						if (RegCreateKeyEx (hKeyRoot, chKeyCurrent, 0L, NULL,
							REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, 
							&hKey, NULL) != ERROR_SUCCESS)
							throw ("Problem creating the key");
					}
					
					RegSetValueEx(hKey, strItem, 0, REG_SZ,
						reinterpret_cast<unsigned const char*>(strValue), 
						lstrlen(strValue)+1);
				}catch (...)
				{
				}
				
				if (hKey) RegCloseKey (hKey);
				hKey = NULL;
				
				pNode = pNode->GetNextNode();
			}
		}

		


		return true;
	}

	bool ReadFromRegistry(HKEY hKeyRoot, LPCTSTR lpSubKey)
	{
		HKEY hKey = NULL;
		HKEY hKeyChild = NULL;
		bool bSuccessful = true;
		int nNrChild = 0;

		TCHAR szValueName[ MAX_PATH ];
		DWORD cbValueNameSize = MAX_PATH;
		TCHAR szValue[ MAX_PATH ];
		DWORD cbValueSize = MAX_PATH;
		DWORD type;
		
		try
		{			
			LONG lRes = RegOpenKeyEx(hKeyRoot, lpSubKey, 0,
				KEY_ALL_ACCESS, &hKey);
			if (lRes != ERROR_SUCCESS)
				return false;
			
			char szSection[MAX_PATH];
			DWORD dwSize = MAX_PATH;
			while (RegEnumKeyEx(hKey, nNrChild, szSection, &dwSize, NULL,
				NULL, NULL, NULL) == S_OK)
			{
				if (lRes != ERROR_SUCCESS)
					throw ("");
				//MessageBox(NULL, CString(szBuffer), "", 0);

				int nNrChildKey = 0;
				if (RegOpenKeyEx (hKey, szSection, 0L, KEY_ALL_ACCESS, &hKeyChild)
					!= ERROR_SUCCESS)
					throw ("");
				
				while ( RegEnumValue(hKeyChild, nNrChildKey,
					szValueName, &cbValueNameSize,
					0, &type, (LPBYTE)szValue, &cbValueSize ) == S_OK )
				{
					nNrChildKey ++;

					cbValueNameSize = MAX_PATH;
					cbValueSize = MAX_PATH;

					if ( REG_SZ != type )
						continue;

					SetValue(szSection, szValueName, szValue);
				}
				if (hKeyChild) RegCloseKey (hKeyChild);		
				hKeyChild = NULL;

				
				dwSize = MAX_PATH;
				nNrChild ++;
			}			
		}catch(...)
		{
			bSuccessful = false;
		}
		if (hKeyChild) RegCloseKey (hKeyChild);
		if (hKey) RegCloseKey (hKey);

		return bSuccessful;
	}

	bool ReadIniFile(const char* filename, 
		bool bOverwrite=true /*if to overwrite existing values*/)
	{
		bool bResult = false;

		try
		{
			HANDLE hFile = CreateFile(filename,   
				GENERIC_READ,
				0,           
				NULL,        
				OPEN_EXISTING, 
				FILE_ATTRIBUTE_NORMAL,        // normal file 
				NULL);                        // no attr. template 
			
			if (hFile != INVALID_HANDLE_VALUE) 
			{ 
				
				DWORD dwSize = GetFileSize(hFile, NULL);
				char* strFile = new char[dwSize+1];
				DWORD dwRead = 0;
				ReadFile(hFile, strFile, dwSize, &dwRead, NULL);
				strFile[dwSize] = '\0';

				char strSection[80] = "??????";
				char strItem[80] = "";
				char strValue[256] = "";
				
				// Now parse the ini file
				DWORD pos = 0;
				char* buffer = strFile;
				while (pos<=dwSize)
				{
					if (strFile[pos] == '\r' || strFile[pos] == '\n' || pos==dwSize)
					{
						strFile[pos] = '\0';

						// Remove white space in the beginning
						while (IsWhiteChar(*buffer) && (*buffer) != '\0')
							buffer ++;

						int nLen = lstrlen(buffer);
						// Remove white space in the end
						while (nLen>1 && IsWhiteChar(buffer[nLen-1]))
							nLen --;
						buffer[nLen] = '\0';

						nLen = lstrlen(buffer);
						if (nLen>0)
						{
							// Now parse the line
							if (buffer[0] == ';')
							{
								// Comment
							}else if (buffer[0]=='[' && buffer[nLen-1]==']')
							{
								// Section
								buffer[nLen-1] = '\0';
								buffer ++;								

								lstrcpy(strSection, buffer);
								//TRACE("Find section %s\n", strSection);
							}else
							{
								int p = 0;
								int pos = 0;
								strItem[0] = '\0';

								while (p<nLen)
								{
									if (buffer[p] == '=')
									{
										buffer[pos+1] = '\0';
										lstrcpy(strItem, buffer);
										buffer += p+1;
										break;
									}
									
									if (!IsWhiteChar(buffer[p]))
										pos = p;

									p ++;
								}

								if (lstrlen(strItem)>0)
								{
									// Remove white space in the beginning
									while (IsWhiteChar(*buffer) && (*buffer) != '\0')
										buffer ++;
									
									lstrcpy(strValue, buffer);

									if (bOverwrite || FindNode(strSection, strItem)==NULL)
										SetValue(strSection, strItem, strValue);
								}
							}
						}

						if (pos<dwSize)
							buffer = strFile+pos+1;
					}
					pos ++;
				}

				delete []strFile;
				CloseHandle(hFile);
				bResult = true;

				/*
				int nMax = 0;
				for (int i=0; i<70; i++)
				{
					if (m_nodeTable[i].GetLength()-1>nMax)
						nMax = m_nodeTable[i].GetLength()-1;
					TRACE("%d ", m_nodeTable[i].GetLength()-1);
				}
				TRACE("\nMax: %d\n", nMax);
				 	*/
			}
		}catch(...)
		{}

		return bResult;
	}

	bool WriteIniFile(const char* filename)
	{
		// Reconstruct the list from the hash
		struct Item{
			CItemNode* pNode;
			Item* pNext;
			Item* pPrev;
		};

		struct Section{
			CSectionNode* pSection;
			Item* pItemList;
		};
		int nNrSections = GetNrOfSections();
		Section* pSectionArray = new Section[nNrSections];
		
		int i=0;
		int p=0;
		for (p=0; p<70; p++)
		{			
			CSectionNode* pNode =  m_sectionTable[p].GetNextNode();
			
			while (pNode)
			{
				pSectionArray[i].pSection = pNode;
				pSectionArray[i].pItemList = NULL;
				i++;
				pNode = pNode->GetNextNode();
			}
		}

		// Now loop the items and populate values
		char* strSection;

		for (p=0; p<70; p++)
		{
			CItemNode* pNode =  m_nodeTable[p].GetNextNode();

			while (pNode)
			{
				strSection = pNode->GetSection();
				
				// Find the section in the section array
				for (int z=0; z<nNrSections; z++)
				{
					if (pSectionArray[z].pSection && 
						lstrcmpi(pSectionArray[z].pSection->GetSection(), strSection)==0)
					{
						Item*  pItem = pSectionArray[z].pItemList;
						Item* pItemLast = pSectionArray[z].pItemList;
						while (pItem!=NULL)
						{
							pItemLast = pItem;
							pItem = pItem->pNext;
						}
						if (pItemLast)
						{
							pItemLast->pNext = new Item();
							pItem = pItemLast->pNext;
							pItem->pNext = NULL;
							pItem->pPrev = pItemLast;
							pItem->pNode = pNode;
						}else
						{
							pSectionArray[z].pItemList = new Item();
							pItem = pSectionArray[z].pItemList;
							pItem->pNext = NULL;
							pItem->pPrev = pItemLast;
							pItem->pNode = pNode;
						}

						break;
					}
				}
				
				pNode = pNode->GetNextNode();
			}
		}

		// now write
		try
		{
			HANDLE hFile = CreateFile(filename,   // open TWO.TXT 
				GENERIC_WRITE,                // open for writing 
				0,                            // do not share 
				NULL,                         // no security 
				CREATE_ALWAYS,                  // open or create 
				FILE_ATTRIBUTE_NORMAL,        // normal file 
				NULL);                        // no attr. template 
			
			if (hFile != INVALID_HANDLE_VALUE) 
			{ 
				char szBuffer[256];
				bool bFirstLine = true;
				DWORD dwBytesWritten;
				
				for (p=0; p<nNrSections; p++)
				{
					Item* pItem = pSectionArray[p].pItemList;
					CSectionNode* pSection = pSectionArray[p].pSection;
					
					if (pItem != NULL && pSection != NULL)
					{
						szBuffer[0] = '\0';
						if (!bFirstLine)
							lstrcat(szBuffer, "\r\n");
						else
							bFirstLine = false;
						lstrcat(szBuffer, "[");
						lstrcat(szBuffer, pSection->GetSection());
						lstrcat(szBuffer, "]\r\n");
						WriteFile(hFile, szBuffer, lstrlen(szBuffer), 
							&dwBytesWritten, NULL); 
					}

					while (pItem!=NULL)
					{
						szBuffer[0] = '\0';
						lstrcat(szBuffer, pItem->pNode->GetItem());
						lstrcat(szBuffer, " = ");
						lstrcat(szBuffer, pItem->pNode->GetValue());
						lstrcat(szBuffer, "\r\n");
						WriteFile(hFile, szBuffer, lstrlen(szBuffer), 
							&dwBytesWritten, NULL); 

						//MessageBox(NULL, pItem->pNode->GetItem(), pItem->pNode->GetValue(), 0);
						pItem = pItem->pNext;
					}
				}
			
				CloseHandle(hFile);
			}			
		}catch(...)
		{
		}
		

		// now clean it up
		for (p=0; p<nNrSections; p++)
		{
			Item* pItem = pSectionArray[p].pItemList;
			while (pItem!=NULL && pItem->pNext!=NULL)
			{
				pItem = pItem->pNext;		
			}

			while (pItem!=NULL)
			{
				Item* pTemp = pItem;
				pItem = pItem->pPrev;
				delete pTemp;
				pTemp = NULL;
			}
		}

		delete []pSectionArray;

		return true;
	}
	
	void ClearAll()
	{
		int i;
		for (i=0; i<70; i++)
		{
			m_nodeTable[i].CleanUp();
		}
		
		for (i=0; i<70; i++)
		{
			m_sectionTable[i].CleanUp();
		}
	}
	
	CItemNode* FindNode(const char* section, const char* item)
	{
		unsigned char hashCode = GetHashCode(item);
		
		return m_nodeTable[hashCode].FindItem(section, item);
	}
	
	CSectionNode* FindSectionNode(const char* section)
	{
		unsigned char hashCode = GetHashCode(section);
		
		return m_sectionTable[hashCode].FindSection(section);
	}
	
	// Warning: Untested code
	bool DeleteItem(const char* section, const char* item)
	{
		unsigned char hashCode = GetHashCode(item);
		
		return m_nodeTable[hashCode].DeleteItem(section, item);
	}

	// Warning: Untested code
	bool DeleteSection(const char* section)
	{
		unsigned char hashCode = GetHashCode(section);

		for (int p=0; p<70; p++)
			m_nodeTable[p].DeleteItem(section);
		
		return m_sectionTable[hashCode].DeleteSection(section);
	}


	const char* GetValue(const char* section, const char* item)
	{
		CItemNode* pNode = FindNode(section, item);
		if (pNode)
			return pNode->GetValue();
		else
			return "";
	}

	void SetValue(const char* section, const char* item, const char* value)
	{
		CItemNode* pNode = FindNode(section, item);
		
		if (pNode)
			pNode->SetValue(value);
		else
		{
			CSectionNode* pSection = FindSectionNode(section);
			if (pSection == NULL)
			{
				pSection = new CSectionNode(section);
				
				unsigned char hashSection = GetHashCode(section);
				m_sectionTable[hashSection].AddTailNode(pSection);				
			}
			
			unsigned char hashItem = GetHashCode(item);
			CItemNode* pNode = new CItemNode(pSection, item, value);
			m_nodeTable[hashItem].AddTailNode(pNode);
		}		
	}


	bool GetValueBool(const char* section, const char* item)
	{
		const char* pChar = GetValue(section, item);
		return lstrcmpi(pChar, "true") == 0 || lstrcmp(pChar, "1") == 0;
	}

	void SetValueBool(const char* section, const char* item, bool bVal)
	{
		char temp[20];
		if (bVal)
			lstrcpy(temp, "true");
		else
			lstrcpy(temp, "false");
		SetValue(section, item, (const char*)temp);
	}
	
	int GetValueInt(const char* section, const char* item)
	{
		const char* pChar = GetValue(section, item);
		return AtoI(pChar);
	}
	
	void SetValueInt(const char* section, const char* item, int nVal)
	{
		char chVal[MAX_PATH];
		wsprintf(chVal, "%d", nVal);
		SetValue(section, item, chVal);
	}
	
	unsigned int GetValueUnsignedInt(const char* section, const char* item)
	{
		const char* pChar = GetValue(section, item);
		return AtoU(pChar);
	}
	
	void SetValueUnsignedInt(const char* section, const char* item, unsigned int nVal)
	{
		char chVal[MAX_PATH];
		wsprintf(chVal, "%u", nVal);
		SetValue(section, item, chVal);
	}
	
	void SetValueLogFont(const char* section, const char* item, LOGFONT* pfont)
	{
		char chValue[256];
		wsprintf(chValue, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s",							
			pfont->lfHeight, 
			pfont->lfWidth, 
			pfont->lfEscapement, 
			pfont->lfOrientation, 
			pfont->lfWeight, 
			pfont->lfItalic, 
			pfont->lfUnderline, 
			pfont->lfStrikeOut, 						
			pfont->lfCharSet, 
			pfont->lfOutPrecision, 
			pfont->lfClipPrecision, 
			pfont->lfQuality, 
			pfont->lfPitchAndFamily, 
			pfont->lfFaceName);
		SetValue(section, item, chValue);
	}

	void GetValueLogFont(const char* section, const char* item, LOGFONT* pfont)
	{
		const char* pChar = GetValue(section, item);
		memset(pfont, 0, sizeof(LOGFONT));

		char chValue[256];
		lstrcpy(chValue, pChar);
		char* pchToken = chValue;
		char* pchPos = chValue;
		
		int pos = 0;
		while (*pchPos != '\0')
		{
			if (*pchPos == ',')
			{
				*pchPos = '\0';
				
				switch (pos)
				{
				case 0:
					pfont->lfHeight = AtoI(pchToken);
					break;
				case 1:
					pfont->lfWidth = AtoI(pchToken);
					break;
				case 2:
					pfont->lfEscapement = AtoI(pchToken);
					break;
				case 3:
					pfont->lfOrientation = AtoI(pchToken);
					break;
				case 4:
					pfont->lfWeight = AtoI(pchToken);
					break;
				case 5:
					pfont->lfItalic = AtoI(pchToken);
					break;
				case 6:
					pfont->lfUnderline = AtoI(pchToken);
					break;
				case 7:
					pfont->lfStrikeOut = AtoI(pchToken);
					break;
				case 8:
					pfont->lfCharSet = AtoI(pchToken);
					break;
				case 9:
					pfont->lfOutPrecision = AtoI(pchToken);
					break;
				case 10:
					pfont->lfClipPrecision = AtoI(pchToken);
					break;
				case 11:
					pfont->lfQuality = AtoI(pchToken);
					break;
				case 12:
					pfont->lfPitchAndFamily = AtoI(pchToken);
					break;
				}
				
				pchToken = pchPos+1;
				pos++;
				
			}
			pchPos ++;
		}
		
		lstrcpy(pfont->lfFaceName, pchToken);
	}
	
private:
	// Help functions
	bool IsWhiteChar(char ch)
	{
		return (ch==' ') || (ch=='\t');
	}
	
	bool IsDigit(char c)
	{
		return c>='0' && c<='9';
	}

	unsigned char GetHashCode(const char* text){
		int nItemLen = lstrlen(text);

		char chTest[5];
		chTest[4] = '\0';

		switch (nItemLen)
		{
		case 0:
			return 0;
		case 1:
			chTest[0] = *(text);
			chTest[1] = *(text);
			chTest[2] = *(text);
			chTest[3] = *(text);
			break;
		case 2:
			chTest[0] = *(text);
			chTest[1] = *(text);
			chTest[2] = *(text+1);
			chTest[3] = *(text+1);
			break;
		case 3:
			chTest[0] = *(text);
			chTest[1] = *(text);
			chTest[2] = *(text+1);
			chTest[3] = *(text+2);
			break;
		default:
			chTest[0] = *(text);
			chTest[1] = *(text+1);
			chTest[2] = *(text+nItemLen-1);
			chTest[3] = *(text+nItemLen-2);
			break;
		}		

		CharUpper(chTest);

		unsigned int nMaxCode = chTest[0]*chTest[1]*chTest[2]*chTest[3];
		
		return nMaxCode % 67; // This gives very good hash result
	}

	int AtoI(const char *nptr)
	{
        char c;              /* current char */
        long total;         /* current total */
        int sign;           /* if '-', then negative, otherwise positive */

        /* skip whitespace */
        while ( IsWhiteChar(*nptr) )
            ++nptr;

        c = (char)*nptr++;
        sign = c;           /* save sign indication */
        if (c == '-' || c == '+')
            c = (char)*nptr++;    /* skip sign */

        total = 0;

        while (IsDigit(c)) {
            total = 10 * total + (c - '0');     /* accumulate digit */
            c = (char)*nptr++;    /* get next char */
        }

        if (sign == '-')
            return -total;
        else
            return total;   /* return result, negated if necessary */
	}

	unsigned int AtoU(const char *nptr)
	{
        char c;              /* current char */
        unsigned long total;         /* current total */
		
        /* skip whitespace */
        while ( IsWhiteChar(*nptr) )
            ++nptr;

		c = (char)*nptr++;
		
        total = 0;		
        while (IsDigit(c)) {
            total = 10 * total + (c - '0');     /* accumulate digit */
            c = (char)*nptr++;    /* get next char */
        }
		
		return total;   /* return result, negated if necessary */
	}
};


#endif