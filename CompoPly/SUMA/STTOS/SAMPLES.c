/* zpracovani AVR souboru */
/* SUMA 9/1993 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <sndbind.h>
#include <macros.h>
#include <stdc\fileutil.h>
#include <stdc\memspc.h>

#include <sttos\samples.h>
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <assert.h>

long DelkaSam( avr_t *h )
{
	int nt=h->avr_mode!=AVR_MONO ? 2 : 1;
	return h->avr_length*nt;
}

long DelkaCSam( avr_t *h ){return h->avr_length;}
void SetDelkaCSam( avr_t *h, long l ){h->avr_length=l;}

long LoopFirstSam( avr_t *h ){return h->avr_loop_first;}
long LoopEndSam( avr_t *h ){return h->avr_loop_end;}
void SetLoopFirstSam( avr_t *h, long l ){h->avr_loop_first=l;}
void SetLoopEndSam( avr_t *h, long l ){h->avr_loop_end=l;}

long CompoFreqSam( avr_t *H )
{
	long p=H->avr_frequency;
	p&=0xffffffl;
	if( p<100 ) p=100; /* nesmyslne hodnoty trochu pooprav */
	if( *(long *)H->avr_user!=MULTICHAR('SUMA') )
	{
		p*=1000;
	}
	else
	{
		long c;
		c=((long *)H->avr_user)[1];
		if( labs(c/1000-p)<=2 ) p=c; /* jen, je-li to zpresneni */
		else p*=1000;
	}
	if( p==33880L*1000 ) p=ACT_CLK33K*1000L;
	ef( p==32880L*1000 ) p=ACT_CLK33K*1000L;
	ef( p==20770L*1000 ) p=ACT_CLK20K*1000L;
	ef( p==16490L*1000 ) p=ACT_CLK16K*1000L;
	/* oprava chyb v dokumentaci */
	return p;
}

void SetCompoFreqSam( avr_t *H, long f )
{
	*(long *)H->avr_user=MULTICHAR('SUMA');
	((long *)H->avr_user)[1]=f;
	H->avr_frequency=(f/1000)|0xff000000L;
}

int GetHWord( avr_t *h, void *Buf, long i )
{
	if( h->avr_resolution==8 ) return (int)(*((signed char *)Buf+i))<<8;
	else return *((int *)Buf+i);
}

void NegSign( avr_t *h, char *b )
{
	int lin=h->avr_resolution>>3;
	long ln=h->avr_length;
	long i;
	if( lin==2 ) for( i=0; i<(ln>>1); i++ )
	{
		(*(int *)(b+i*2))+=0x8000;
	}
	ef( lin==1 ) for( i=0; i<ln; i++ )
	{
		(*(char *)(b+i))+=0x80;
	}
	else Plati( False );
}

static Flag SpravnyAVR( avr_t *H )
{
	if( ((long *)H->avr_user)[0]!=MULTICHAR('SUMA') ) return True;
	if( ((long *)H->avr_user)[2]==MULTICHAR('A16B') ) return True;
	return False;
}

void *AVRLoad( avr_t *H, const char *FName )
{
	int F=open(FName,O_RDONLY|O_BINARY);
	void *Buf;
	long il,it;
	if( F<0 )
	{
		errno=ENOENT;
		return NULL;
	}
	if( sizeof(avr_t)!=read(F,H,sizeof(avr_t)) )
	{
		errno=EIO;
		ErrHlava: close(F);return NULL;
	}

  SwapEndianL(&H->avr_magic);
	SwapEndianW(&H->avr_mode);				
	SwapEndianW(&H->avr_resolution);
	SwapEndianW(&H->avr_signed);			
	SwapEndianW(&H->avr_looping);		
	SwapEndianW(&H->avr_midinote);		
	SwapEndianL(&H->avr_frequency);
	SwapEndianL(&H->avr_length);
	SwapEndianL(&H->avr_loop_first);	
	SwapEndianL(&H->avr_loop_end);

	SwapEndianL(&((long *)H->avr_user)[0]);
	SwapEndianL(&((long *)H->avr_user)[1]);
	SwapEndianL(&((long *)H->avr_user)[2]);

	if( H->avr_magic!=AVR_MAGIC )
	{
		errno=EINVAL;
		goto ErrHlava;
	}
	if( H->avr_resolution!=8 && H->avr_resolution!=16 )
	{
		errno=EINVAL;
		goto ErrHlava;
	}
	it=H->avr_resolution>>3;
	if( H->avr_mode!=AVR_MONO ) it*=2;
	if( !SpravnyAVR(H) )
	{
		H->avr_length/=it;
		H->avr_loop_first/=it;
		H->avr_loop_end/=it;
	}
	il=H->avr_length*it;
	Buf=mallocSpc(il,MULTICHAR('AVRL'));
	if( !Buf ) {errno=ENOMEM;goto ErrHlava;}
	if( il!=read(F,Buf,il) )
	{
		close(F);
		freeSpc(Buf);
		errno=EINVAL;
		return NULL;
	}
  if (H->avr_resolution==16)
  {
    int s;
    for (s=0; s<H->avr_length; s++) 
    {
      SwapEndianW((short *)Buf+s);
    }
  }
	if( H->avr_signed==AVR_UNSIGNED ) NegSign(H,Buf);
	H->avr_signed=AVR_SIGNED;
	close(F);
	return Buf;
}

int AVRSave( const char *FName, const avr_t *H, const void *Buf )
{
	int F=creat(FName, _S_IREAD | _S_IWRITE);
	long il,ll;
	if( F<0 ) return -1;
	((long *)H->avr_user)[0]=MULTICHAR('SUMA');
	((long *)H->avr_user)[2]=MULTICHAR('A16B');
	if( sizeof(avr_t)!=write(F,H,sizeof(avr_t)) )
	{
		Err: close(F);return -1;
	}
	il=H->avr_resolution>>3;
	if( H->avr_mode!=AVR_MONO ) il*=2;
	ll=H->avr_length*il;
	if( ll!=write(F,Buf,ll) ) goto Err;
	if( close(F)<0 ) return -1;
	return 0;
}

typedef struct
{ /* podmno�ina avr_t */
	signed char mode;
	signed char resolution;
	signed char looping;
	signed char midinote;
	unsigned long frequency;
	unsigned long length,loop_first,loop_end;
} sss_t;

void *SSSLoad( avr_t *H, const char *FName )
{ /* nekompatibilni samply - pro hry (Gravon) */
	int F=open(FName,O_RDONLY|O_BINARY);
	void *Buf;
	sss_t h;
	long il,it;
	if( F<0 )
	{
		errno=ENOENT;
		return NULL;
	}
	if( sizeof(h)!=read(F,&h,sizeof(h)) )
	{
		errno=EIO;
		ErrHlava: close(F);return NULL;
	}
  SwapEndianL(&h.frequency);
  SwapEndianL(&h.length);
  SwapEndianL(&h.loop_first);
  SwapEndianL(&h.loop_end);
	memset(H,0,sizeof(*H));
	H->avr_mode=h.mode;
	H->avr_resolution=h.resolution;
	H->avr_looping=h.looping;
	H->avr_midinote=h.midinote;
	H->avr_frequency=h.frequency;
	H->avr_length=h.length;
	H->avr_loop_first=h.loop_first;
	H->avr_loop_end=h.loop_end;
	H->avr_magic=AVR_MAGIC;
	if( H->avr_resolution!=8 && H->avr_resolution!=16 )
	{
		errno=EINVAL;
		goto ErrHlava;
	}
	it=H->avr_resolution>>3;
	if( H->avr_mode!=AVR_MONO ) it*=2;
	H->avr_name[0]=H->avr_xname[0]=0;
	H->avr_signed=AVR_SIGNED;
	il=H->avr_length*it;
	Buf=mallocSpc(il,MULTICHAR('AVRL'));
	if( !Buf ) {errno=ENOMEM;goto ErrHlava;}
	if( il!=read(F,Buf,il) )
	{
		close(F);
		freeSpc(Buf);
		errno=EINVAL;
		return NULL;
	}
  if (h.resolution>8)
  {
    int i;
    assert(h.resolution==16);
    for (i=0; i<il/2; i++)
    {
      SwapEndianW((word *)Buf+i);
    }

  }
	close(F);
	return Buf;
}

int SSSSave( const char *FName, const avr_t *H, const void *Buf )
{
	int F=creat(FName, _S_IREAD | _S_IWRITE);
	long il,ll;
	sss_t h;
	if( F<0 ) return -1;
	h.mode=H->avr_mode;
	h.resolution=H->avr_resolution;
	h.looping=H->avr_looping;
	h.midinote=H->avr_midinote;
	h.frequency=H->avr_frequency;
	h.length=H->avr_length;
	h.loop_first=H->avr_loop_first;
	h.loop_end=H->avr_loop_end;
	if( sizeof(h)!=write(F,&h,sizeof(h)) )
	{
		Err: close(F);return -1;
	}
	il=H->avr_resolution>>3;
	if( H->avr_mode!=AVR_MONO ) il*=2;
	ll=H->avr_length*il;
	if( ll!=write(F,Buf,ll) ) goto Err;
	if( close(F)<0 ) return -1;
	return 0;
}

static int *DVSoffset;
static int InitDVSDekomp( void )
{
	int i;
	if( DVSoffset ) return 0;
	DVSoffset=malloc(256*sizeof(int));
	if( !DVSoffset ) return -1;
	DVSoffset[0]=-pow(+1.084618362,128);
	for( i=1; i<128; i++ )
	{
		int v=pow(+1.084618362,+i);
		DVSoffset[128+i]=+v;
		DVSoffset[128-i]=-v;
		/*
		       / -1.084618362^-x  for x<0  (-128 to -1)
		f(x)= {   0               for x=0  (0)
		       \  1.084618362^x   for x>0  (1 to 127)
		*/
	}
	return 0;
}

const static long DVSsam_freq[8]={ACT_CLK8K,ACT_CLK10K,ACT_CLK12K,ACT_CLK16K,ACT_CLK20K,ACT_CLK25K,ACT_CLK33K,ACT_CLK50K};
typedef struct
{
	char magic[6];     /* "DVSM" */
	int headlen;       /* Headlen in Bytes*/
	int freq;		   /* Samplefreqenz 0=8kHz 7=50kHz*/
	char pack;		   /* 0 ungepackt, 2=DVS Packmethode*/
	char mode;         /* 0=Stereo 8Bit,1=Stereo 16Bit,2=Mono 8Bit*/
	long blocklen;     /* falls pack=1: L�nge eines gepackten Blocks*/ 
} DVSMHEAD;

#define getword(b)		( (*((int *)(b)))++ )

static int sat16( long val )
{
	if( val>32767 ) return (int)32767;
	ef( val<-32768L ) return (int)-32768L;
	return (int)val;
}

void *DVSLoad( avr_t *H, const char *FName )
{
	DVSMHEAD dh;
	int F;
	void *Buf=NULL,*B;
  long len;
	F=open(FName,O_RDONLY|O_BINARY);
	if( !F ) return NULL;
	len=filelength(F);
	if( len<=0 ) {close(F);return NULL;}
	if( sizeof(dh)!=read(F,&dh,sizeof(dh)) ) goto Error;
	len-=dh.headlen;
	if( len<=0 ) goto Error;
	if( memcmp(dh.magic,"DVSM\0\0",6) ) goto Error;
	if( lseek(F,dh.headlen-sizeof(dh),SEEK_CUR)<0 ) goto Error;
	/* zalo� AVR hlavi�ku */
	memset(H,0,sizeof(*H));
	H->avr_magic=AVR_MAGIC;
	H->avr_mode=(dh.mode&2) ? AVR_MONO : AVR_STEREO;
	H->avr_resolution=(dh.mode&1) ? 16 : 8;
	H->avr_signed=AVR_SIGNED;
	H->avr_looping=AVR_NON_LOOPING;
	H->avr_midinote=AVR_NOTE_NONE;
	if( dh.freq>=0 && dh.freq<8 ) H->avr_frequency=DVSsam_freq[dh.freq];
	else H->avr_frequency=dh.freq;
	if( !dh.pack )
	{
		B=mallocSpc(len,MULTICHAR('DVSL'));
		if( !B ) goto Error;
		if( len!=read(F,B,len) ) {freeSpc(B);goto Error;}
		Buf=B;
	}
	ef( dh.pack==2 && dh.mode==1 ) /* 16 bit� stereo */
	{
		long cnt=len;
		int *dst;
		signed char *ibuf;
		long lval,rval; /* o�ez�v� se a� nakonec */
		if( InitDVSDekomp()<0 ) goto Error;
		ibuf=malloc(cnt);
		if( !ibuf ) goto Error;
		len=0;
		B=mallocSpc(cnt*2,MULTICHAR('DVSL')); /* komprese ne v�ce ne� 2x */
		if( !B ) {freeSpc(ibuf);goto Error;}
		dst=B;
		if( cnt!=read(F,ibuf,cnt) ) {freeSpc(ibuf);freeSpc(B);goto Error;}
		while( cnt>0 ) /* �ti a dekomprimuj bloky */
		{
			int bcnt;
			lval=getword(ibuf);
			rval=getword(ibuf);
			cnt-=4;
			*dst++=(int)lval;
			*dst++=(int)rval;
			len+=4;
			for( bcnt=0; bcnt<dh.blocklen && cnt>0; bcnt++ )
			{
				int rd=*ibuf++;
				int ld=*ibuf++;
				rval+=DVSoffset[rd+128];
				lval+=DVSoffset[ld+128];
				cnt-=2;
				*dst++=sat16(lval);
				*dst++=sat16(rval);
				len+=4;
			}
		}
		Buf=B;
	}
	/* d�lku zn�� a� nakonec */
	H->avr_length=len;
	if( H->avr_mode!=AVR_MONO ) H->avr_length>>=1;
	if( H->avr_resolution==16 ) H->avr_length>>=1;
	H->avr_loop_first=0;
	H->avr_loop_end=H->avr_length;
	Error:
	close(F);
	(void)H;
	return Buf;
}

void *RAWLoad( avr_t *H, const char *FName )
{
	long len;
	int F;
	void *Buf;
	char Naz[16];
	char *E;
	F=open(FName,O_RDONLY|O_BINARY);
	if( !F ) return NULL;
	len=filelength(F);
	if( len<=0 ) {close(F);return NULL;}
	Buf=mallocSpc(len,MULTICHAR('RAWL'));
	if( !Buf ) {close(F);return NULL;}
	if( len!=read(F,Buf,len) ) {close(F);freeSpc(Buf);return NULL;}
	close(F);
	memset(H,0,sizeof(avr_t));
	strlncpy(Naz,NajdiNazev(FName),sizeof(Naz));
	E=NajdiExt(Naz);
	if( E ) *E=0;
	H->avr_magic=AVR_MAGIC;
	strncpy(H->avr_name,Naz,8);
	H->avr_mode=AVR_MONO;
	H->avr_resolution=8; /* vetsinou je RAW 8 bit */
	H->avr_signed=AVR_SIGNED; /* to vetsina je */
	H->avr_looping=AVR_NON_LOOPING;
	H->avr_midinote=AVR_NOTE_NONE;
	/* import Amiga MOD samplu - RAW data */
	H->avr_frequency=12500|0xff000000L;
	H->avr_length=len;
	H->avr_loop_first=0;
	H->avr_loop_end=len;
	return Buf;
}

void *ZvukLoad( avr_t *H, const char *FName )
{ /* mo�n� by bylo lep� ignorovat extender? */
	const char *E=NajdiPExt(NajdiNazev(FName));
	if( !strcmpi(E,".AVR") ) return AVRLoad(H,FName);
	ef( !strcmpi(E,".DVS") ) return DVSLoad(H,FName);
	ef( !strcmpi(E,".SSS") ) return SSSLoad(H,FName);
	else return RAWLoad(H,FName);
}

int ZvukSave( const char *FName, const avr_t *H, const void *Buf )
{
	const char *E=NajdiPExt(NajdiNazev(FName));
	if( !strcmpi(E,".SSS") ) return SSSSave(FName,H,Buf);
	ef( !strcmpi(E,".DVS") ) return -1;
	else return AVRSave(FName,H,Buf);
}

/* operace na samplech */

void *ZmenRozl( avr_t *h, void *b, int nb )
{
	long i;
	long l=DelkaSam(h);
	int nsi=nb>>3;
	long nl=DelkaSam(h)*nsi;
	char *NB=mallocSpc(nl,MULTICHAR('AVRR'));
	if( !NB ) return NB;
	Plati( nb==8 || nb==16 );
	for( i=0; i<l; i++ )
	{
		int r=GetHWord(h,b,i);
		if( nb==16 ) *(int *)(NB+i*2)=r;
		ef( nb==8 ) *(char *)(NB+i)=r>>8;
		else Plati( False );
	}
	h->avr_resolution=nb;
	h->avr_signed=AVR_SIGNED;
	freeSpc(b);
	return NB;
}

void ZmenPuvRozl( avr_t *h, void *b, int nb )
{
	int ob=h->avr_resolution;
	h->avr_resolution=nb;
	SetDelkaCSam(h,DelkaCSam(h)*ob/nb);
	SetLoopFirstSam(h,LoopFirstSam(h)*ob/nb);
	SetLoopEndSam(h,LoopEndSam(h)*ob/nb);
	(void)b;
}

void *ZmenStereo( avr_t *h, char *b )
{
	long i;
	int li=h->avr_resolution>>3;
	int nm=h->avr_mode!=AVR_MONO ? AVR_MONO : AVR_STEREO;
	int osi=h->avr_mode!=AVR_MONO ? 2 : 1;
	int nsi=3-osi;
	long l=DelkaCSam(h);
	char *B=mallocSpc(l*nsi*li,MULTICHAR('AVRS'));
	if( !B ) return NULL;
	if( nm!=AVR_MONO )
	{ /* -> Strereo */
		for( i=0; i<l; i++ )
		{
			int v=GetHWord(h,b,i);
			if( li==1 )
			{
				(*(char *)(B+i*2))=
				(*(char *)(B+i*2+1))=v>>8;
			}
			else
			{
				(*(int *)(B+i*4))=
				(*(int *)(B+i*4+2))=v;
			}
		}
	}
	else
	{ /* -> Mono */
		for( i=0; i<l; i++ )
		{
			int v=(GetHWord(h,b,i*2)>>1)+(GetHWord(h,b,i*2+1)>>1);
			if( li==1 ) (*(char *)(B+i))=v>>8;
			ef( li==2 ) (*(int *)(B+i*2))=v;
		}
	}
	freeSpc(b);
	h->avr_mode=nm;
	return B;
}

void ZmenPuvStereo( avr_t *h, char *b )
{
	int ot=h->avr_mode!=AVR_MONO ? 2 : 1;
	int nt=3-ot;
	int nm=h->avr_mode!=AVR_MONO ? AVR_MONO : AVR_STEREO;
	h->avr_mode=nm;
	SetDelkaCSam(h,DelkaCSam(h)/nt*ot);
	SetLoopFirstSam(h,LoopFirstSam(h)/nt*ot);
	SetLoopEndSam(h,LoopEndSam(h)/nt*ot);
	(void)b;
}

/* obecne rutiny */

#define FORCE_ST  0 /* predstirej, ze jsi ST */
#define FORCE_STE 0 /* predstirej, ze jsi STE */
#define FORCE_SHIFT 1

enum zvuksys Zvuky( void )
{
	static Flag Tested=False;
	static enum zvuksys Vysledek=SndST;
	if( Tested ) return Vysledek;
	Tested=True;
	return Vysledek;
}
