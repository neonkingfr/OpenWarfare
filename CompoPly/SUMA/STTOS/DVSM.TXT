
A DVSM sample file has the following structure:

typedef struct
{
	char magic[6];     /* "DVSM" */
	int headlen;       /* Headlen in Bytes*/
	int freq;		   /* Sample freqency 0=8kHz 7=50kHz*/
	char pack;		   /* 0 unpacked, 2=DVS packmethod*/
	char mode;         /* 0=Stereo 8Bit,1=Stereo 16Bit,2=Mono 8Bit*/
	long blocklen;     /* if pack=1: Length of a packed block*/ 
} DVSMHEAD;

followed by the 16 Bit stereo data (or DVS 8 Bit data).


Pack format deltapack (CPU):
(At the moment only for 16 bit stereo samples)


A block looks like that (length defined by 'blocklen'):

 1. Sound word left    (16 Bit)
 1. Sound word right   (16 Bit)
 1. Distance value left  (8 Bit)
 1. Distance value right (8 Bit)
 				.
 				.
 				.
 n. Distance value left  (8 Bit)
 n. Distance value right (8 Bit)

The distance values are 'pointers' to a table of specially scaled real 16
bit distance values (16 bit). The table contains the function values of the following
function:
				   / -1.084618362^-x  for x<0  (-128 to -1)
			f(x)= {   0               for x=0  (0)
				   \  1.084618362^x   for x>0  (1 to 127)

The table has a length of 256 entries. 				   


Andreas Binner
Harald Sch�nfeld