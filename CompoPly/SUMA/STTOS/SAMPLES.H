#ifndef __SAMPLES
#define __SAMPLES

#pragma pack(push,1)
typedef struct
{
#define AVR_MAGIC	0x32424954L
	long avr_magic;					/* '2BIT' */
	char avr_name[8];				/* sample name */
#define AVR_MONO	0
#define AVR_STEREO	-1
	short avr_mode;					/* mono/stereo */
	short avr_resolution;			/* sample resolution */
#define AVR_SIGNED	-1
#define AVR_UNSIGNED	0
	short avr_signed;				/* signed/unsigned sample */
#define AVR_NON_LOOPING	0
#define AVR_LOOPING	-1
	short avr_looping;				/* looping sample control */
#define AVR_NOTE_NONE	-1
	short avr_midinote;				/* assigned midi note */
	unsigned long avr_frequency;	/* sample frequency */
	unsigned long avr_length;		/* sample length */
	unsigned long avr_loop_first;	/* offset of first loop */
	unsigned long avr_loop_end;		/* offset of end loop */
	char avr_reserved[6];			/* reserved */
	char avr_xname[20];				/* sample name extension area */
	char avr_user[64];				/* user commitable area */
} avr_t;
#pragma pack(pop)

/* je-li resolution 1, znamena neznamy pocet bitu */
/* programy by mely zachazet s 1 vzdycky jako s 8 */

/* AVR je pouzivan jako interni format */
/* ma nejuplnejsi hlavicku */

long CompoFreqSam( avr_t *H ); /* dotaz na presnou frekvenci v 1/1000 Hz */
void SetCompoFreqSam( avr_t *H, long f ); /* jeji nastaveni */

long DelkaSam( avr_t *H ); /* delka vyj. v dat. slovech */
long DelkaCSam( avr_t *H ); /* delka vyj. v cas. jednotkach (i stereo) */
void SetDelkaCSam( avr_t *H, long del );

long LoopFirstSam( avr_t *H );
void SetLoopFirstSam( avr_t *H, long lf );

long LoopEndSam( avr_t *H );
void SetLoopEndSam( avr_t *H, long lf );

int GetHWord( avr_t *h, void *Buf, long i );

#define GetHWord88(Buf,i) ( *((signed char *)(Buf)+(i)) )
#define GetHWord8(Buf,i) ( ( (int)*((signed char *)(Buf)+(i)) )<<8 )
#define GetHWord16(Buf,i) ( *((int *)(Buf)+(i)) )

#define GetHWordM(h,Buf,i) \
	( \
		(h)->avr_resolution==8 ? \
		(int)(*((signed char *)(Buf)+(i)))<<8 : \
		*((int *)(Buf)+(i)) \
	)

void NegSign( avr_t *h, char *b ); /* +  0x8000 */

void *AVRLoad( avr_t *H, const char *FName );
int AVRSave( const char *FName, const avr_t *H, const void *Buf );

void *DVSLoad( avr_t *H, const char *FName );

void *SSSLoad( avr_t *H, const char *FName );
int SSSSave( const char *FName, const avr_t *H, const void *Buf );

void *ZvukLoad( avr_t *H, const char *FName );
int ZvukSave( const char *FName, const avr_t *H, const void *Buf );
/* cteni s autodetekci formatu */
/* samo nastavuje default hodnoty nezn. parametru */

/* operace na samplech */

/* zmeny formatu */

void *ZmenRozl( avr_t *h, void *b, int nb );
void *ZmenStereo( avr_t *h, char *b );

/* zmeny chapani dat */
/* uzitecne po importu RAW dat */

void ZmenPuvRozl( avr_t *h, void *b, int nb );
void ZmenPuvStereo( avr_t *h, char *b );

/* obecne rutiny */ 

enum zvuksys {SndST,SndSTE,SndFalcon};

/* Falcon zarucuje 16-bit DMA, DSP, Codec a 68020 */
/* STE zarucuje 8-bit DMA */
/* ST zarucuje 68000, a AY- */

enum zvuksys Zvuky( void );

#endif
