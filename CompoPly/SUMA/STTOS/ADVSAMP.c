/* slozitejsi oprace na samplech */
/* SUMA 1993-5/1994 */

#include <macros.h>
#include <sttos\samples.h>
#include <sttos\advsamp.h>

void VystredAVR( avr_t *h, char *b, int tr )
{
	long Str=0;
	int nt=( h->avr_mode!=AVR_MONO ? 2 : 1 );
	long len=DelkaCSam(h);
	{
		long i;
		long n;
		int ex=0;
		long lp=len;
		if( tr>nt-1 ) tr=nt-1;
		while( lp>0x4000 ) ex++,lp>>=1;/* hrozi preteceni */
		for( i=0; i<len; i++ )
		{
			Str+=(long)GetHWord(h,b,i*nt+tr)>>ex;
		}
		n=len>>ex;
		Str/=n;
	} /* strida je urcena */
	{ /* proved posuv */
		long ln=len;
		int lin=h->avr_resolution>>3;
		long i;
		if( lin==2 ) for( i=0; i<ln; i++ )
		{
			long iff=i*nt+tr;
			long W=GetHWord16(b,iff);
			W-=Str;
			if( W<(int)-0x8000 ) W=(int)-0x8000;
			ef( W>(int)0x7fff ) W=(int)0x7fff;
			*(int *)(b+iff*2)=(int)W;
		}
		else for( i=0; i<ln; i++ )
		{
			long iff=i*nt+tr;
			long W=GetHWord8(b,iff);
			W-=Str;
			if( W<(int)-0x8000 ) W=(int)-0x8000;
			ef( W>(int)0x7fff ) W=(int)0x7fff;
			*(char *)(b+iff)=(int)(W>>8);
		}
		h->avr_signed=AVR_SIGNED;
	}
}

void ZhorsiAVR( avr_t *h, char *b, int tr )
{
	int nt=( h->avr_mode!=AVR_MONO ? 2 : 1 );
	long len=DelkaCSam(h);
	long ln=len;
	int lin=h->avr_resolution>>3;
	long i;
	if( lin==2 ) for( i=0; i<ln-1; i+=2 )
	{
		long iff=i*nt+tr;
		long W1=GetHWord16(b,iff);
		long W2=GetHWord16(b,iff+nt);
		W1=(W1+W2)>>1;
		if( W1<(int)-0x8000 ) W1=(int)-0x8000;
		ef( W1>(int)0x7fff ) W1=(int)0x7fff;
		iff=(i>>1)*nt+tr; /* zahustujeme */
		*(int *)(b+iff*2)=(int)W1;
	}
	else for( i=0; i<ln; i++ )
	{
		long iff=i*nt+tr;
		long W1=GetHWord8(b,iff);
		long W2=GetHWord8(b,iff+nt);
		W1=(W1+W2)>>1;
		if( W1<(int)-0x8000 ) W1=(int)-0x8000;
		ef( W1>(int)0x7fff ) W1=(int)0x7fff;
		iff=(i>>1)*nt+tr; /* zahustujeme */
		*(char *)(b+iff)=(int)(W1>>8);
	}
	h->avr_signed=AVR_SIGNED;
	lin*=nt;
	SetDelkaCSam(h,DelkaCSam(h)/2);
	SetLoopFirstSam(h,LoopFirstSam(h)/2);
	SetLoopEndSam(h,LoopEndSam(h)/2);
	SetCompoFreqSam(h,CompoFreqSam(h)/2);
}

void ScaleRozsah( avr_t *h, char *b, long Min, long Max )
{
	long i;
	long Rng=Max+1-Min;
	int lin=h->avr_resolution>>3;
	long ln=DelkaSam(h);
	Max-=Min;
	Max>>=1;
	Min=-Max;
	if( lin==2 ) for( i=0; i<ln; i++ )
	{
		long W=GetHWord16(b,i);
		if( W<Min ) W=Min;
		ef( W>Max ) W=Max;
		if( Rng<0x10000L ) W=((lword)(W-Min)<<16)/Rng-0x8000;
		else W=((lword)(W-Min)<<12)/(Rng>>4)-0x8000;
		if( W<(int)-0x8000 ) W=(int)-0x8000;
		ef( W>(int)0x7fff ) W=(int)0x7fff;
		*(int *)(b+i*2)=(int)W;
	}
	else for( i=0; i<ln; i++ )
	{
		long W=GetHWord8(b,i);
		if( W<Min ) W=Min;
		ef( W>Max ) W=Max;
		if( Rng<0x10000L ) W=((lword)(W-Min)<<16)/Rng-0x8000;
		else W=((lword)(W-Min)<<12)/(Rng>>4)-0x8000;
		if( W<(int)-0x8000 ) W=(int)-0x8000;
		ef( W>(int)0x7fff ) W=(int)0x7fff;
		*(char *)(b+i)=(int)(W>>8);
	}
	h->avr_signed=AVR_SIGNED;
}

#define lsgn(l) ( (l)>0 ? +1 : (l)<0 ? -1 : 0 )

void NabehyAVR( avr_t *h, char *b, int t, long DelNab )
{
	int nt=( h->avr_mode!=AVR_MONO ? 2 : 1 );
	long len=DelkaCSam(h);
	if( len<DelNab*3 ) return;
	{
		int lin=h->avr_resolution>>3;
		long i;
		long vb=GetHWord(h,b,t);
		long ve=GetHWord(h,b,(len-1)*nt+t);
		if( lin==2 )
		{
			for( i=0; i<DelNab; i++ )
			{
				long iff=i*nt+t;
				int W=GetHWord16(b,iff);
				if( lsgn(vb)==lsgn(W) ) *(int *)(b+iff*2)=0;
				else break;
			}
			for( i=0; i<DelNab; i++ )
			{
				long iff=(len-1-i)*nt+t;
				int W=GetHWord16(b,iff);
				if( lsgn(ve)==lsgn(W) ) *(int *)(b+iff*2)=0;
				else break;
			}
		}
		else
		{
			for( i=0; i<DelNab; i++ )
			{
				long iff=i*nt+t;
				int W=GetHWord8(b,iff);
				if( lsgn(vb)==lsgn(W) ) *(char *)(b+iff)=0;
				else break;
			}
			for( i=0; i<DelNab; i++ )
			{
				long iff=(len-1-i)*nt+t;
				int W=GetHWord8(b,iff);
				if( lsgn(ve)==lsgn(W) ) *(char *)(b+iff)=0;
				else break;
			}
		}
		h->avr_signed=AVR_SIGNED;
	}
}
