;
; This program originally available on the Motorola DSP bulletin board.
; It is provided under a DISCLAMER OF WARRANTY available from
; Motorola DSP Operation, 6501 Wm. Cannon Drive W., Austin, Tx., 78735.
; 
; Motorola Standard I/O Equates.
; 
; Last Update 25 Aug 87   Version 1.1   fixed OF
;
;************************************************************************
;
;       EQUATES for DSP56000 I/O registers and ports
;
;************************************************************************

ioequ   ident   1,0

;------------------------------------------------------------------------
;
;       EQUATES for I/O Port Programming
;
;------------------------------------------------------------------------

;       Register Addresses

BCR   EQU     $FFFE           ; Port A Bus Control Register
PBC   EQU     $FFE0           ; Port B Control Register
PBDDR EQU     $FFE2           ; Port B Data Direction Register
PBD   EQU     $FFE4           ; Port B Data Register
PCC   EQU     $FFE1           ; Port C Control Register
PCDDR EQU     $FFE3           ; Port C Data Direction Register
PCD   EQU     $FFE5           ; Port C Data Register


;------------------------------------------------------------------------
;
;       EQUATES for Host Interface
;
;------------------------------------------------------------------------

;       Register Addresses

HCR   EQU     $FFE8           ; Host Control Register
HSR   EQU     $FFE9           ; Host Status Register
HRX   EQU     $FFEB           ; Host Receive Data Register
HTX   EQU     $FFEB           ; Host Transmit Data Register

;       Host Control Register Bit Flags

HRIE  EQU     0               ; Host Receive Interrupt Enable
HTIE  EQU     1               ; Host Transmit Interrupt Enable
HCIE  EQU     2               ; Host Command Interrupt Enable
HF2   EQU     3               ; Host Flag 2
HF3   EQU     4               ; Host Flag 3

;       Host Status Register Bit Flags

HRDF  EQU     0               ; Host Receive Data Full
HTDE  EQU     1               ; Host Transmit Data Empty
HCP   EQU     2               ; Host Command Pending
HF    EQU     $18             ; Host Flag Mask
HF0   EQU     3               ; Host Flag 0
HF1   EQU     4               ; Host Flag 1
DMA   EQU     7               ; DMA Status

;------------------------------------------------------------------------
;
;       EQUATES for Synchronous Serial Interface (SSI)
;
;------------------------------------------------------------------------

;       Register Addresses

RX    EQU     $FFEF           ; Serial Receive Data Register
TX    EQU     $FFEF           ; Serial Transmit Data Register
CRA   EQU     $FFEC           ; SSI Control Register A
CRB   EQU     $FFED           ; SSI Control Register B
SSR    EQU     $FFEE           ; SSI Status Register
TSR   EQU     $FFEE           ; SSI Time Slot Register

;       SSI Control Register A Bit Flags

PM    EQU     $FF             ; Prescale Modulus Select Mask
DC    EQU     $1F00           ; Frame Rate Divider Control Mask
WL    EQU     $6000           ; Word Length Control Mask
WL0   EQU     13              ; Word Length Control 0
WL1   EQU     14              ; Word Length Control 1
PSR   EQU     15              ; Prescaler Range

;       SSI Control Register B Bit Flags

OF    EQU     $3              ; Serial Output Flag Mask
OF0   EQU     0               ; Serial Output Flag 0
OF1   EQU     1               ; Serial Output Flag 1
SCD   EQU     $1C             ; Serial Control Direction Mask
SCD0  EQU     2               ; Serial Control 0 Direction
SCD1  EQU     3               ; Serial Control 1 Direction
SCD2  EQU     4               ; Serial Control 2 Direction
SCKD  EQU     5               ; Clock Source Direction
FSL   EQU     8               ; Frame Sync Length
SYN   EQU     9               ; Sync/Async Control
GCK   EQU     10              ; Gated Clock Control
MOD   EQU     11              ; Mode Select
STE   EQU     12              ; SSI Transmit Enable
SRE   EQU     13              ; SSI Receive Enable
STIE  EQU     14              ; SSI Transmit Interrupt Enable
SRIE  EQU     15              ; SSI Receive Interrupt Enable

;       SSI Status Register Bit Flags

IF    EQU     $2              ; Serial Input Flag Mask
IF0   EQU     0               ; Serial Input Flag 0
IF1   EQU     1               ; Serial Input Flag 1
TFS   EQU     2               ; Transmit Frame Sync
RFS   EQU     3               ; Receive Frame Sync
TUE   EQU     4               ; Transmitter Underrun Error
ROE   EQU     5               ; Receiver Overrun Error
TDE   EQU     6               ; Transmit Data Register Empty
RDF   EQU     7               ; Receive Data Register Full

;------------------------------------------------------------------------
;
;       EQUATES for Exception Processing
;
;------------------------------------------------------------------------

;       Register Addresses

IPR   EQU     $FFFF           ; Interrupt Priority Register

;       Interrupt Priority Register Bit Flags

IAL   EQU     $7              ; IRQA Mode Mask
IAL0  EQU     0               ; IRQA Mode Interrupt Priority Level (low)
IAL1  EQU     1               ; IRQA Mode Interrupt Priority Level (high)
IAL2  EQU     2               ; IRQA Mode Trigger Mode
IBL   EQU     $38             ; IRQB Mode Mask
IBL0  EQU     3               ; IRQB Mode Interrupt Priority Level (low)
IBL1  EQU     4               ; IRQB Mode Interrupt Priority Level (high)
IBL2  EQU     5               ; IRQB Mode Trigger Mode
HPL   EQU     $C00            ; Host Interrupt Priority Level Mask
HPL0  EQU     10              ; Host Interrupt Priority Level Mask (low)
HPL1  EQU     11              ; Host Interrupt Priority Level Mask (high)
SSLM   EQU     $3000           ; SSI Interrupt Priority Level Mask
SSL0  EQU     12              ; SSI Interrupt Priority Level Mask (low)
SSL1  EQU     13              ; SSI Interrupt Priority Level Mask (high)
SCLM   EQU     $C000           ; SCI Interrupt Priority Level Mask
SCL0  EQU     14              ; SCI Interrupt Priority Level Mask (low)
SCL1  EQU     15              ; SCI Interrupt Priority Level Mask (high)
