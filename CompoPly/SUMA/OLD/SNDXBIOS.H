/*
* SNDBIND.H	Low level Sound function bindings for use with "C"
*		and the added Sound XBIOS calls.
*
*		Mike Schmal		7/7/92
*
*
* Updates:
*
* 7/13/92 MS  Added buffptr function.
* 7/24/92 MF  Added equates for function parameters
*
*/



/*
 * soundcmd() 'mode' parameter values
*/

#define	LTATTEN		0
#define	RTATTEN		1
#define	LTGAIN		2
#define	RTGAIN		3
#define	ADDERIN		4
#define	ADCINPUT	5
#define	SETPRESCALE	6


/*
 * Added SOUND BIOS Errors.
*/

#define	SNDNOTLOCK	-128
#define	SNDLOCKED	-129



/*
 * ADDERIN bit masks
*/

#define ADDERIN_ADC	1
#define ADDERIN_MATRIX	2


/*
 * SETPRESCALE values
*/

#define STE_6K		0	/* Invalid... won't do 6.25khz samples */
#define STE_12K		1	/* 12.5KHz STE/TT compatible */
#define STE_25K		2	/* 25KHz STE/TT compatible */
#define STE_50K		3	/* 50KHz STE/TT compatible */


/*
 * setmode() parameter values
*/

#define _8BIT_STEREO	0
#define _16BIT_STEREO	1
#define _8BIT_MONO	2


/*
 * devconnect() source/destination device parameter values
*/

#define SRC_ADC		3	/* source parameter is one of these */
#define SRC_EXTERNAL	2
#define SRC_DSPXMIT	1
#define SRC_DMAPLAY	0

#define DEST_DAC	8	/* destination parameter is any combination */
#define DEST_EXTERNAL	4	/* of these... */
#define	DEST_DSPRECV	2
#define DEST_DMAREC	1	/* e.g.  DEST_DAC|DEST_DSPRECV|DEST_DMAREC */


/*
 * devconnect() clock input parameter values
*/

#define INTERNAL_25175	0	/* internal 25.175MHz clock */
#define	EXT_CLOCK	1	/* external clock input */
#define	INTERNAL_32	2	/* internal 32Mhz clock (DSP Dependent) */


/*
 * devconnect() clock prescale parameter values
*/

#define CLK_OLD		0	/* STE/TT prescale values... see SETPRESCALE stuff above. */
#define CLK50K		1
#define CLK33K		2
#define CLK25K		3
#define CLK20K		4
#define CLK16K		5
#define CLK12K		7
#define CLK10K		9
#define CLK8K		11

/*
 * SOUND trap calling routine.
*/

typedef struct
{
	void *play,*rec;
	void *res1,*res2;
} snd_buf;

#define SOUND xbios

#define	locksnd()		SOUND(0x80)
#define	unlocksnd()		SOUND(0x81)
#define	soundcmd(a,b)		SOUND(0x82,a,b)
#define	setbuffer(a,b,c)	SOUND(0x83,a,b,c)
#define	setmode(a)		SOUND(0x84,a)
#define	settracks(a,b)		SOUND(0x85,a,b)
#define	setmontracks(a)		SOUND(0x86,a)
#define	setinterrupt(a,b)	SOUND(0x87,a,b)
#define	buffoper(a)		SOUND(0x88,a)
#define	dsptristate(a,b)	SOUND(0x89,a,b)
#define	gpio(a,b)		SOUND(0x8A,a,b)
#define	devconnect(a,b,c,d,e)	SOUND(0x8B,a,b,c,d,e)
#define	sndstatus(a)		SOUND(0x8C,a)
#define	buffptr(a)		SOUND(0x8D,a)
