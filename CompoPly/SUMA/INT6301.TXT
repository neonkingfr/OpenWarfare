 2.1.1       My�

            Konstrukce tohoto za��zen� je velmi jednoduch� a  p�itom 
 efektivn�.  Cel�  my� je slo�en� ze dvou  fotobun�k,  dvou  kole�ek 
 pln�c�ch  funkci  dekod�ru a ��dic�ho mechanismu.  �kolem  my�i  je 
 vys�lat  do  po��ta�e informace o jejim pohybu (pohybu  kursoru  na 
 obrazovce). Tuto informaci tvo�� sm�r pohybu v sou�adnic�ch X a Y a 
 dr�ha  pohybu  v t�chto sou�adnic�ch.  Transformace dr�hy  my�i  na 
 sou�adnice  X a Y je zaji�t�na pomoci kuli�ky s  gumov�m  povlakem, 
 kter�  ��d� 2 dekodovac� kole�ka (X a Y).  Osy t�chto kole�ek  jsou 
 navz�jem kolm� k sob�.  Ka�d� z t�chto kole�ek (nebo jedno z nich v 
 z�vislosti  na sm�ru pohybu my�i) se ot��i v�ce nebo  m�n�  dop�edu 
 nebo dozadu podle dr�hy pohybu my�i.  P�e�ten� sm�ru pohybu  ne�ini 
 ��dn�  pot��e.  B�hem pohybu dekodovac� kole�ka zakr�vaj�  sv�teln� 
 paprsek vys�lany do fotobu�ky. Sta�i se��st impulsy ka�d�ho kole�ka 
 pro ur�eni dr�hy pohybu. 


 2.1.2       P��kazy obslu�n�ho procesoru kl�vesnice

             Procesor pro obsluhu kl�vesnice obsahuje  sadu  p��kaz�, 
 kter�  slou��  nap�.  pro obsluhu  my�i,  nastaven�  p�esn�ho  �asu 
 re�ln�ch  hodin  tohoto procesoru �ten�m jeho vnit�n�  pam�ti  atd. 
 Uk�zkov� aplikace s listingem je pops�na v t�to kapitole p�i popisu 
 p��kazu  $21.  Norm�ln�  pr�ce  tohoto  procesoru  je  zalo�en�  na 
 nep�etr�it�m  �ten� kl�vesnice a hl��en� hlavn�mu  procesoru  ka�d� 
 zma�knut�  kl�vesnice.  Je  to realizov�no  pomoci  p�enosu  p�edem 
 definovan�ch  k�dov�ch ��sel jednotliv�ch kl�ves v okam�iku  jejich 
 stisku.  Po uvoln�n� zma�knut� kl�vesy,  k�d t�to kl�vesy je  znovu 
 pos�lan do hlavn�ho procesoru, ale s nastaven�m (logick� hodnota 1) 
 bitem �.7.  V d�sledku toho nem��eme obdr�et k�d v�t�� ne� 127  pro 
 jakoukoliv  kl�vesu.  K�dy  jednotliv�ch kl�ves jsou  zobrazeny  na 
 obr�zku 2.1.2-1.  Ve skute�nosti tyto k�dy jsou z intervalu ohrani-
 �en�ho  zprava  hodnotou  117,  proto�e k�dy  vy���  ne�  $F6  jsou 
 rezervov�ny  pro jin� ��ely.  Jedna se zde o to,  �e  n�kdy  vznik� 
 pot�eba vysl�n� do hlavn�ho procesoru nejenom k�du kl�vesy,  ale  i 
 jin�ch  informac�  - nap�.  aktu�ln� stav hodin nebo  poloha  my�i. 
 Proto�e tyto informace se nevejdou do jednoho bytu,  mus�me pos�lat 
 skupiny byt�.  P�en��en� t�chto skupin p�edch�z� vysl�n� k�du  $F6. 
 Popis,  kter�  k�d  p�edch�z�  ur�it�  skupin�  byt�  je  pops�n  u 
 jednotliv�ch  p��kaz�.  Samostatn� p��kaz procesoru  kl�vesnice  je 
 slo�en  z  k�du p��kazu (1 byte)  a  po�adovan�ch  parametr�.  N��e 
 uveden� popis je se�azen podle ��sel k�dovan�ch p��kaz�. 
 

 $07 
 Informuje o stisku jednoho nebo obou tla�itek my�i. Po�aduje se 
 tak�  vysl�n� bytu obsahuj�c�ho parametry podle n�sleduj�c�ho  for-
 m�tu:

 Bit  0  -  1=ud�v� absolutn� pozici v okam�iku  stla�en�  tla�itka 
            my�i. Bit 2 mus� obsahovat logickou 0.
 Bit  1  -  1=ud�v� absolutn� polohu v okam�iku,  kdy tla�itko  nen� 
            zma�knuto. Bit 2 mus� obsahovat logickou 0.
 Bit  2  -  1=tla�itka my�i jsou pova�ov�na za b��n�  kl�vesy.  Lev� 
            tla�itko m� hodnotu $74 a prav� $75.
 Bity 3-7 musej� m�t v�dy logickou hodnotu 0.

 $08
 Ud�v� relativn� polohu my�i a informuje procesor kl�vesnice o d�lce 
 posuvu od star� polohy do polohy sou�asn�,  v�dy v okam�iku posuvu. 
 Tato informace je vys�l�na v p��pad�, �e po�et impuls� generovan�ch 
 dekodujic�mi  kole�ky  dos�hne ur�itou prahovou hodnotu  (viz  tak� 
 p��kaz $0B).  Tato data jsou p�en��en� jako skupina byt� v n�sledu-
 j�c�m po�ad�:

 1. byte  -  hlavi�ka v podob� k�dov�ho ��sla $F8-$FB
 2. byte  -  relativn� poloha podle osy X (se znam�nkem!)
 3. byte  -  relativn� poloha podle osy Y (se znam�nkem!)

 V  p��pad�,  �e  relativn� poloha se m�n� natolik,  �e  nem��e  b�t 
 vyjad�en�  touto  skupinou  byt�,  automaticky  je  tato  informace 
 p�en��en�  pomoci dal�� skupiny,  kter� je roz���en�m t�to  skupiny 
 byt�. 

 $09
 Ud�v� absolutn� polohu my�i (vzd�lenost p�esunu) a ur�uje maxim�ln� 
 hodnoty  sou�adnic.  Vnit�n�  ukazatele t�chto  sou�adnic  jsou  ve 
 stejn� dob� nulov�ny. Po�aduje se ud�n� n�sleduj�c�ch parametr�:

 1. slovo  -  maxim�ln� sou�adnice X
 2. slovo  -  maxim�ln� sou�adnice Y

 Poloha  my�i ur�ena sou�adnicemi men��mi od nuly anebo  v�t��mi  od 
 maxim�ln�ch hodnot nen� ud�van�.

 $0A
 Pomoci  tohoto p��kazu m��eme ��st kl�vesy kursoru  m�sto  my�i.  V 
 tomto  p��pad� stisknut� kl�vesy kursoru je br�no jako pohyb  my�i. 
 Po�aduje se vysl�n� t�chto byt�:

 1.  byte  -  po�et impuls� (X),  po kter�ch bude vyslan k�d kl�vesy 
              kurzoru vlevo nebo vpravo.
 2.  byte  -  po�et impuls� (Y),  po kter�ch bude vyslan k�d kl�vesy 
              kurzoru nahoru nebo dolu.

 $0B
 Tento  p��kaz  ur�uje prah p�epnut�  (citlivost  my�i),  po  kter�m 
 n�sleduje vysl�n� informace o zm�n�ch polohy my�i.  Z tohoto d�vodu 
 je  vys�lan ur�it� po�et �id�c�ch impuls� z  dekodovac�ch  kole�ek, 
 kter� p�edch�zej� p�enosu vlastn� skupiny byt� - funguje to pouze v 
 opera�n�m re�imu RELATIVE. Parametry jsou n�sleduj�c�:

 1.  byte  -  prah,  po p�ekro�en� kter�ho je vyslan sign�l o pohybu 
              X-ov� sou�adnice
 2. byte  -   stejn� v�znam pro sou�adnici Y

 $0C
 Umo��uje stupnicov�n� pohybu my�i. Tento sign�l n�m umo��uje ur�it, 
 kolik  musi  b�t p�ijato impuls� z dekodovac�ch  kole�ek  my�i  pro 
 zv�t�en�  �ita�e  sou�adnic o 1.  Tento p��kaz je  spojen  pouze  s 
 absolutn� polohou my�i. Jsou po�adov�ny tyto parametry:

 1. byte  -  stupnicov�n� polohy ve sm�ru X
 2. byte  -  stupnicov�n� polohy ve sm�ru Y

 $0D
 �te absolutn� polohu my�i. Tento p��kaz nepo�aduje vysil�n� ��dn�ch 
 parametr�.  Form�t  skupiny  byt�  vys�lan�ch p�i  tomto  �ten�  je 
 n�sleduj�c�:

 1. byte  -  hlavi�ka v podob� k�du $F7
 2. byte  -  stav zma�knut� tla�itka my�i
             bit 0=1 - bylo zma�knuto prav�  tla�itko  od  momentu        
                       posledn�ho �ten�
             bit 1=1 - nebylo zma�knuto prav� tla�itko
             bit 2=1 - bylo zma�knuto lev�  tla�itko  od  momentu     
                       posledn�ho �ten�
             bit 3=1 - nebylo zma�knuto lev� tla�itko

 Tato trochu podivn� informace n�m umo��uje ur�it pomoci 2 vzta�n�ch 
 byt� (logick� hodnota 0),  zda stav jednoho nebo obou tla�itek my�i 
 se zm�nil od posledn�ho �ten� t�chto hodnot.

 1 slovo  -  absolutn� poloha X
 1 slovo  -  absolutn� poloha Y

 $0E
 Nastavuje vnit�n� ��ta� sou�adnic.  Po�aduje vysl�n�  n�sleduj�c�ch 
 parametr�:

 1 byte  -  dopl�kov� byte
 1 slovo -  poloha X
 1 slovo -  poloha Y

 $0F
 Nastavuje za��tek osy Y (dole).

 $10
 Nastavuje za��tek osy X (naho�e).

 $11
 P�enos dat do hlavn�ho procesoru je op�t umo�n�n (viz  $13).  Ka�d� 
 jin� p��kaz (krom� $13) tak� obnovuje p�enos dat.

 $12
 Vyp�n�  my�.  Pou�it� jak�hokoliv p��kazu odvol�vaj�c�ho se na  my� 
 ($08,  $09,  $10),  op�t zap�n� my�.  Pracuje-li my� v re�imu  $0A, 
 tento p��kaz nem� ��dn� v�znam.

 $13
 Zastavuje p�enos dat do hlavn�ho procesoru.
 POZOR!  Hodnoty  ur�uj�c� pohyb my�i nebo k�dy zma�knut�ch  kurzor-
 ov�ch  kl�ves  jsou v procesoru 6301 zaps�ny do  t�  doby,  ne�  je 
 zapln�n jeho bufer pro �schovu t�chto dat.  V�echny hodnoty zapsan� 
 po jeho napln�n� jsou ignorov�ny. 

 $14
 Informuje  o pohybu joysticku.  Skupina byt� ur�uj�c� zm�nu  polohy 
 (pohyb) joysticku m� n�sleduj�c� form�t:

 1 byte  -  k�dov� ��slo hlavi�ky ($FE pro  joystick  0,  $FF  pro       
            joystick 1)
 1 byte  -  bity 0-3 ur�uj� sm�ry pohybu,  bit 7 zma�knut�  tla�itka            
            joysticku (fire)

 $15
 Vyp�n� automatick� re�im �ten� polohy joysticku. V tomto p��pad� je 
 nutno ��dat o vysl�n� skupiny byt� ur�en�ch p��kazem $16.

 $16
 �te polohu joysticku.  Po proveden� tohoto p��kazu procesor  kl�ve-
 snice vys�l� v��e popsanou skupinu byt�.

 $17
 Informace o dob� trv�n� pohybu joysticku.  Je po�adov�n pouze jeden 
 parametr:

 1 byte  -  �as mezi dv�ma sign�ly v setin�ch sekundy

 Od  t�to chvile jsou vys�lany skupiny byt� do doby ne� bude  zvolen 
 jin� pracovn� re�im:

 1 byte  -  bit 0 - stav p�epina�e joysticku 1
            bit 1 - stav p�epina�e joysticku 0
 1 byte  -  bity 0-3 - poloha joysticku 1
            bity 4-7 - poloha joysticku 0

 POZOR!  P�e�ten� �asov� interval nem� b�t krat�� ne� je  po�adov�no 
 pro vysl�n� dvou byt� tvo��c�ch tuto skupinu ur�en�m kan�lem.

 $18
 Informace o d�lce trv�n� zma�knut� tla�itka FIRE joysticku 1. Tento 
 stav  je testov�n spojit� a v�sledek je p�en��en cel�m bytem  tzn., 
 �e je prov�d�no 8 t�chto test�; nejaktualn�j�� hodnota je zaps�na v 
 bitu 7.  Kontroler  kl�vesnice ur�uje �as mezi zm�nami pomoci  hla-
 vn�ho  procesoru.  Tento �as je potom rozd�len na 8  interval�,  ve 
 kter�ch  je  �ten stav tla�itka FIRE - toto testov�n� je  co  mo�na 
 nepravideln�j��.  Tento  p��kaz je prov�d�n a� do obdr�en�  dal��ho 
 p��kazu, kter� m� b�t proveden.

 $19
 Re�im simulace joysticku 0 pomoci kurzorov�ch kl�ves.  Informace  o 
 aktu�ln�  poloze joysticku je p�en��en� do hlavn�ho procesoru  jako 
 stla�ov�n� kurzorov�ch kl�ves (tak �asto, jak je to nutn�). Abychom 
 se vyhnuli opakov�n� po n�kolikat� nezbytn�ch parametr�, p�ejdeme k 
 v�ci  nejd�le�it�j��:  v�echny  �asy  jsou  p�ij�many  jakoby  byly 
 vys�lany v desetin�ch sekundy. R ur�uje �asov� okam�ik, po uplynut� 
 kter�ho  je vysl�na informace o zm�n� kurzoru v �asov�m odstupu  T, 
 jako interval je br�na potom hodnota V.  V p��pad�, �e R=0, pouze V 
 je zodpov�dn� za interval. Samoz�ejm�, �e cel� tento mechanismus je 
 zapojen  pouze  v p��pad�,  �e joystick nem�n� svou polohu  v  �ase 
 del��m ne� T nebo R. 
 1 byte  -  RX
 1 byte  -  RY
 1 byte  -  TX
 1 byte  -  TY
 1 byte  -  VX
 1 byte  -  VY

 $1A
 Vyp�n� oba joysticky; mohou b�t op�t zapnuty proveden�m jak�hokoliv 
 p��kazu odvol�vaj�c�ho se k nim.

 $1B
 Nastavuje  �as vnit�n�ch hodin realn�ho �asu,  kter� se  nach�z�  v 
 procesoru kl�vesnice.  �asov� hodnoty jsou uchov�vany v k�du BCD  - 
 jednotliv� �islice v rozsahu 0-9 jsou zaps�ny v polovin�ch byt�  (1 
 byte = 2 �islice). Po�aduje se vysl�n� t�chto parametr�:

 1 byte  -  rok     - 2 ��slice (nap�. 88, 89)
 1 byte  -  m�sic   - 2 ��slice (nap�. 01)
 1 byte  -  den     - 2 ��slice
 1 byte  -  hodina  - 2 ��slice
 1 byte  -  minuty  - 2 ��slice 
 1 byte  -  vte�iny - 2 ��slice

 V p��pad�,  �e v polovin� bytu se objevi hodnota,  kter� neodpovid� 
 k�du BCD (nap�.  F),  je ignorov�na.  Umo��uje to zm�nu pouze ��sti 
 datumu.

 $1C
 Hodiny realn�ho �asu. Po p�ijet� tohoto p��kazu procesor kl�vesnice 
 vys�l�  skupinu byt� ve stejn�m form�tu jako u  p��kazu  $1B.  Tuto 
 skupinu byt� p�edch�z� k�dov� ��slo $FC.

 $20
 Na�ten�  dat do pam�ti.  Jedn� se o vnit�n� pam�t procesoru  kl�ve-
 snice (pam�� RAM od adresy $80 do $FF),  do kter� m��eme  zapisovat 
 data.  Neni n�m ale prozatim jasn� k �emu se to m��e hodit, proto�e 
 podle na��ch informaci (DATA BECKER dissasembloval opera�n�  syst�m 
 procesoru  6301)  tuto  pam��  nelze  vyu�it  tak,  jak  by  si  to 
 predstavoval u�ivatel.  Je pravd�podobn�,  �e t�mto zp�sobem m��eme 
 m�nit ur�it� parametry, kter� jin�m, "leg�ln�m" zp�sobem nen� mo�no 
 m�nit. Jsou po�adov�ny tyto parametry:

 1 slovo  -  po��te�n� adresa
 1 byte   -  po�et byt� (maximaln� 128)
 data     -  datov� byty podle uveden�ho po�tu

 �as pro p�enos t�chto dat musi b�t krat�� ne� 20 ms. 

 $21
 �ten� vnit�n� pam�ti procesoru kl�vesnice - opak p��kazu $20.  Jsou 
 po�adov�ny tyto parametry:

 1 slovo  -  adresa, od kter� se m� ��st

 Tato skupina byt� (kterou obdr�ime) m� tento form�t:

 1  byte   -  hlavi�ka 1 (k�dov� ��slo)=$F6.  Toto je  st�l�  k�dov� 
              ��slo,  kter� p�edch�z� ka�dou skupinu byt� obsahuj�c� 
              jak�koliv opera�n� parametry pro procesor kl�vesnice - 
              k tomuto t�matu se je�t� budeme d�le vracet. 
 1  byte   -  hlavi�ka 2 (k�dov� ��slo)=$20.  Identifikuje  skupinu 
              byt�, kter� p�en��� obsah pam�ti.
 6  bytu   -  obsah pam�ti od adresy,  kter� byla ur�ena jako parametr 
              tohoto p��kazu.

 Uv�d�me zde tak� kr�tk� program,  kter� n�m umo�nil p�e�ten� obsahu 
 pam�ti ROM procesoru kl�vesnice 6301 a v�pis na tisk�rn�.  Na tomto 
 p��klad�  uvidi�  zp�sob p�enosu skupiny byt�,  kter�  ur�uj�  stav 
 kl�vesnice,  a  kter� jsou ignorov�ny opera�n�m  syst�mem  hlavn�ho 
 procesoru.

 1                             prt      equ      0
 2                             chout    equ      3
 3                             gemdos   equ      1
 4                             bios     equ      13
 5                             xbios    equ      14
 6                             stvec    equ      12
 7                             rdm      equ      $21
 8                             wrkbd    equ      25
 9                             kbdvec   equ      34
 10                            term     equ      0
 11                            START:
 12 00000000 3F3C0022                   move.w   #kbdvec,-(a7)
 13 00000004 4E4E                       trap     #xbios
 14 00000006 548F                       addq.l   #2, a7
 15 00000008 41F900000000               lea      0, a0
 16 0000000E 43F9000000D6               lea      keyin,a1
 17 00000014 23C000000104               move.l   d0, savea
 18 0000001A 23F0000C00000100           move.l   stvec(a0,d0), save
 19 00000022 2189000C                   move.l   a1,stvec(a0,d0)
 20 00000026 383CF000                   move.w   #$F000,d4
 21                            LOOP:
 22 0000002A 33C400000010A              move.w   d4,tbuf+1
 23 00000030 61000084                   brs      keyout
 24                            WAIT:
 25 00000034 0C390000000000F8           cmpi.b   #0,rbuf
 26 0000003C 67F6                       beq      wait
 27 0000003E 3C3C0006                   move.w   #6,d6
 28 00000042 610A                       bsr      bufout
 29 00000044 5C44                       addq.w   #6,d4
 30 00000046 0C44FFFF                   cmpi.w   #$ffff,d4
 31 0000004A 6DDE                       blt      loop
 32 0000004C 6052                       bra      exit
 33                            BUFOUT:
 34 0000004E 49F9000000F9               lea      rbuf+1,a4
 35                            BYTOUT:
 36 00000054 101C                       move.b   (a4)+,d0
 37 00000056 6106                       bsr      hexout
 38 00000058 5306                       subq.b   #1,d6
 39 0000005A 66F8                       bne      bytout
 40 0000005C 4E75                       rts
 41                            HEXOUT:
 42 0000005E 3240                       movea.w  d0,a1
 43 00000060 E808                       lsr.b    #4,d0
 44 00000062 02800000000F               andi.l   #15,d0
 45 00000068 47F9000000E8               lea      table,a3
 46 0000006E 14330000                   move.b   0(a3,d0),d2
 47 00000072 E14A                       lsl.w    #8,d2
 48 00000074 3009                       move.w   a1,d0
 49 00000076 02800000000F               andi.l   #15,d0
 50 0000007C 14330000                   move.b   0(a3,d0),d2
 51 00000080 3002                       move.w   d2,d0
 52 00000082 3F02                       move.w   d2,-(a7)
 53 00000084 E048                       lsr.w    #8,d0
 54 00000086 6108                       bsr      chrout
 55 00000088 301F                       move.w   (a7)+,d0
 56 0000008A 6104                       bsr      chrout
 57 0000008C 103C0020                   move.b   #" ",do
 58                            CHROUT:
 59 00000090 3F00                       move.w   d0,-(a7)
 60 00000092 3F3C0000                   move.w   #prt,-(a7)
 61 00000096 3F3C0003                   move.w   #chout,-(a7)
 62 0000009A 4E4D                       trap     #bios
 63 0000009C 5C8F                       addq.l   #,a7
 64 0000009E 4E75                       rts
 65                            EXIT:
 66 000000A0 307900000104               movea    savea,a0
 67 000000A6 203900000100               move.l   save,d0
 68 000000AC 2140000C                   move.l   d0,stvec(a0)
 69 000000B0 3F3C0000                   move.w   #term,-(a7)
 70 000000B4 4E41                       trap     #gemdos
 71                            KEYOUT:
 72 000000B6 13FC00000000000F8          move.b   #0,rbuf
 73 000000BE 487900000109               pea      tbuf
 74 000000C4 3F3C0002                   move.w   #2,-(a7)
 75 000000C8 3F3C0019                   move.w   #wrkbd,-(a7)
 76 000000CC 4E4E                       trap     #xbios
 77 000000CE DFFC00000008               adda.l   #8,a7
 78 000000D4 4E75                       rts
 79                            KEYIN:
 80 000000D6 103C0008                   move.b   #8,d0
 81 000000DA 43F9000000F8               lea      rbuf,a1
 82                            REPIN:
 83 000000E0 12D8                       move.b   (a0)+,(a1)+
 84 000000E2 5300                       subq.b   "1,d0
 85 000000E4 66FA                       bne      repin
 86 000000E6 4E75                       rts
 87                            TABLE:
 88 000000E8 3031323334353637           dc.b     "0123456789ABCDEF"
 88 000000F0 3839414243444546
 89 000000F8                   RBUF:    ds.b     8
 90 00000100                   SAVE:    ds.l     1
 91 00000104                   SAVEA:   ds.l     1
 92 00000108                   DUMMY:   ds.b     1
 93 00000109 21                TBUF:    dc.b     rdm
 94 0000010A                            ds.b     2
 95 0000010C                            .end

 $22
 Prov�d�c� procedura. Pomoci tohoto p��kazu m��e� prov�d�t procedury 
 obsa�en� v pam�ti ROM obvodu 6301. Samoz�ejm�, �e  nejprve je nutno 
 v�d�t k �emu dana procedura slou�i a kde se nach�z�,  v p��pad�, �e 
 nebyla  p�enesen�  do  pam�ti  RAM  pomoci  p��kazu  $20  (jestli�e 
 p�edpoklad�me, �e je tam trochu voln�ho m�sta). Jedin�m po�adovan�m 
 parametrem je:

 1 slovo  -  po��te�n� adresa.


 2.1.3       Hl��en� o stavu procesoru kl�vesnice

             V  ka�d�m  okam�iku m��e�  p�e��st  opera�n�  parametry 
 kl�vesnice pomoci jednoduch�ho p�id�n� hodnoty $80 k bytu  p��kazu, 
 pomoci kter�ho budeme volit opera�n� re�im (kter�ho parametry chce� 
 p�e��st).  Jako  v�sledek  obdr��� skupinu  byt�  (kter�  p�edch�z� 
 k�dov� ��slo $F6), kter� ur�uj� parametry odpov�daj�c� p�esn� t�om, 
 kter�  budeme  pot�ebovat  pro zapnut�  tohoto  opera�n�ho  re�imu. 
 P��klad  v�t�inou odstrani nejasnosti:  p�ipus�me,  �e chce�  v�d�t  
 jak� stupnice plat� pro pohyb my�i. V tomto p��pad� m�l bys prov�st 
 p��kaz $8C ($0C+$80;  $0C umo��uje definovat stupnici pohybu my�i). 
 Jako v�sledek obdr���:

 1 byte  -  k�dov� ��slo pro tuto skupinu byt�
 1 byte  -  stupnici podle plochy X
 1 byte  -  stupnici podle plochy Y

 Jak  je  mo�no vid�t je to stejn� form�t jako u  p��kazu  $0C.  P�i 
 pokusu  zji�t�n� parametr� u p��kaz�,  u kter�ch nejsou  vy�adovany 
 ��dn� parametry,  bude proveden tento p��kaz.  Nap�.  p�ipus�me, �e 
 chce� v�d�t v jak�m re�imu je obsluhov�n joystick (p��kaz $14  nebo 
 $15).  V  tomto p��pad� prov�d�� p��kaz $94 (anebo $95 - je to  bez 
 rozd�lu).  Jako  v�sledek obdr��� skupinu  byt�,  kterou  p�edch�z� 
 k�dov�  ��slo pro p��kaz ($14 nebo $15) a tak� proveden�  samotn�ch 
 p��kaz� podle toho v jak�m re�imu se nach�z� joystick.
 P��kazy p��pustn� pro ur�en� stavu jsou:  $87,  $88, $89, $8A, $8B, 
 $8C, $8F, $90, $92, $94, $99 a $9A. Chceme tak� n�co nab�dnout t�m, 
 kter�m se zdaj� funkce procesoru moc slo�it� a tak� t�m,  kte��  by 
 cht�li vid�t trochu inteligence v procesoru 6301. Tento procesor m� 
 tak�  sv�ho dvojn�ka s ozna�en�m 63P01 (HITACHI).  V  tomto  modelu 
 m�sto  zabudovan� pam�ti ROM je patice pro p�ipojen�  pam�ti  EPROM 
 2732 nebo 2764 (8 KB!).  Po t�to zm�n� m��e� realizovat sv� vlastn� 
 c�le,  nap�.  vyu�it  dva  konektory pro p�ipojen�  joysticku  jako 
 univers�ln� 4-bitov� port vstup/v�stup,  pro kter� m��e� vytvo�it a 
 roz���it  existuj�c� soubor p��kaz� anebo z�skat p��stup k  funkc�m 
 obsa�en�m v XBIOS. 


