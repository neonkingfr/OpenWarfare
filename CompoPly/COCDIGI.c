/* digitalni hrani na AY a Falconu */
/* SUMA 3/1993-2/1994 */
/* GEM/Componium zapouzdreni */

#include <macros.h>
#include <string.h>
#include <sndbind.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sttos\samples.h>

#include "compo.h"
#include "llhraj.h"
#include "digill.h"
#include "utlcompo.h"
#include "ramcompo.h"
#include "cocload.h"
#include "STDC\FILEUTIL.H"
#include <windows.h>

PathName OrcNam;
Flag Rezerva=False;
LLHraj *AktHraj=&AYBufFalc; /* glob. promenna, urcuje, kdo prave hraje */

const char *JmenoOrc( const char *Nam ){return Nam;}

/*
*/

NastrDef *NastrAcc( DynPole *P, int NN )
{
	if( NN&0x4000 )
	{
		Plati( P );
		return AccDyn(P,NN&0x3fff);
	}
	return AccDyn(&NDef,NN);
}

static int NajdiOptSoub( NastrDef *D, int Ton )
{
	int ni;
	int mindif=FyzOkt*12*2,dif;
	int mini=-1;
	Flag FF=False;
	for( ni=1; ni<NMSoub; ni++ )
	{
		const char *N=D->Soubory[ni];
		if( *N ) FF=True;
	}
	if( !FF ) return 0;
	for( ni=0; ni<NMSoub; ni++ )
	{
		const char *N=D->Soubory[ni];
		if( *N )
		{
			NastrSoub *NS;
			long S=ZavedSoub(D->Soubory[ni],2);
			if( S<0 ) return -1;
			NS=AccDyn(&NSou,S);
			dif=abs(Ton-NS->BasNota);
			if( dif<mindif ) mindif=dif,mini=ni;
		}
	}
	return mini;
}

int DefHNastr( const char *NNam, int Ton, int Tip, Pisen *P )
{
	int I=NajdiDef(NNam,Tip,&P->LocDigi);
	if( I>=EOK )
	{
    int noteOffset = HoverFormat ? -12 : 0;
		NastrDef *D=NastrAcc(&P->LocDigi,I);
		long S;
		int FTon=F_FyzTon(Ton);
		int ni=NajdiOptSoub(D,Ton);
		if( ni<0 ) return KON;
		S=ZavedSoub(D->Soubory[ni],2);
		if( S==ERR ) /*AlertRF(CENASFA,1,NajdiNazev(D->Soubory[ni])),*/ S=ECAN;
		if( S<EOK ) return (int)S;
		UzijSoub(S);
		D->IMSoubory[FTon+noteOffset]=S;
		D->Uzit=True; /* potrebujeme vedet, koho mame ukladat do COC */
	}
	else return KON;
	return I;
}

#if 0
int DefHNastr( const char *NNam, int Tip, int *KL, int *KH, Flag *Rytm, Pisen *P )
{
	int I=NajdiDef(NNam,Tip,&P->LocDigi);
	if( I>=EOK )
	{
		NastrDef *D=NastrAcc(&P->LocDigi,I);
		long S;
		if( AktHraj==&AYBufFalc ) S=ZavedSoub(D->Soubor,2);
		else S=ZavedSoub(D->Soubor,1);
		if( S==ERR ) S=ECAN;
		if( S<EOK ) return (int)S;
		UzijSoub(S);
		D->ISoub=S;
	}
	else return KON;
	*KL=0,*KH=0x7fff; /* bereme jakykoliv kanal */
	*Rytm=False; /* rytmika taky zabira cas */
	return I;
  return 0;
}
#endif

/* ---------------- editace nastroje */

/* LLHRAJ zapouzdreni sluzeb */

/* uroven orchestru - NastrDef */

const char *EditSNastr( const char *NNam, const WindRect *Orig, Pisen *P )
{
	(void)Orig;
	(void)P;
	return NNam;
}

void MazSNastr( void )
{
}

/* uroven orchestru */

const char *NastrInfo( const char *NNam, Pisen *P )
{
	(void)P;
	return NNam;
}

Err SaveHNastr( FILE *f, Pisen *P, const char *A ) /* uloz vsechny pouzite nastroje */
{
	(void)f,(void)A;
	(void)P;
	return EFIL;
}

void AktHNastr( void )
{
}

Err VymazNastr( char *N, Pisen *P )
{
	(void)N,(void)P;
	return EFIL;
}

Err KonSNastr( void )
{
	return EFIL;
}

Err CtiOrchestr( void )
{
	return EFIL;
}
Err PisOrchestr( void )
{
	return EFIL;
}

const char **SoupisSNastr( Pisen *P )
{
	(void)P;
	return NULL;
}

/* sekce DSP */

Err ZacDsp( void )
{
	return EOK;
}

static long Hlasitost;
static Flag JeLod=False;

void HlasHudba( long VOut ) /* 0..1000 */
{
	(void)VOut;
}

static long FadeOutT;
static Flag JeFadeOut=False;

void ZacFadeOutHudba(int cas)
{
	if( JeLod && !TestHraje() )
	{
		FadeOutT=SystemCas()+1070;
		JeFadeOut=True;
		F_Povel=FPFade;
	}
}
void KonFadeOutHudba()
{
	if( JeFadeOut )
	{
		while( SystemCas()<FadeOutT ){}
		JeFadeOut=False;
	}
}

void FadeOutHudba( int cas )
{
	ZacFadeOutHudba(cas);
	KonFadeOutHudba();
}

Err ZacLod( void )
{
	Hlasitost=0x7FFFFFL;
	F_Povel=FPNic;
	JeLod=True;
	return EOK;
}
void KonDsp( void )
{
}
void KonLod( void )
{
	JeLod=False;
}

static int PrazdnyNastr( Pisen *P, Flag ForceL )
{
	NastrDef n,*N=&n;
	Flag Global;
	int NN;
	int ni;
	strcpy(N->Nazev,"");
	for( ni=0; ni<NMSoub; ni++ )
	{
		strcpy(N->Soubory[ni],"");
	}
	for( ni=0; ni<FyzOkt*12; ni++ )
	{
		N->IMSoubory[ni]=-1;
	}
	if( !P ) Global=True;
	ef( ForceL ) Global=False;
	else Global=False /*AlertRF(LOKGLOBA,1)==2*/;
	if( Global )
	{
		if( NDyn(&NDef)>=0x4000 ) return ERAM;
		NN=(int)InsDyn(&NDef,N);
		if( NN<0 ) return ERAM;
	}
	else
	{
		if( NDyn(&P->LocDigi)>=0x4000 ) return ERAM;
		NN=(int)InsDyn(&P->LocDigi,N);
		if( NN<0 ) return ERAM;
		NN|=0x4000;
	}
	return NN;
}

#define strlncpy strncpy

static Err CtiSNastr( const char *NNaz )
{
  char basePath[1024];
  char NasPath[1024];
	Err ret;
	FILE *f;
	if( !*NNaz ) return EOK;
  GetModuleFileName(NULL,basePath,sizeof(basePath));
  strcpy(NajdiNazev(basePath),"");
  strcpy(NasPath,basePath);
  strcat(NasPath,NNaz);
	MazSNastr();
	ZacDyn(&NDef);
	InitNastrP(NasPath);
	f=fopen(NasPath,"r");
	if( !f ) return EOK;
	if( !Rezerva ) if( setvbuf(f,NULL,_IOFBF,9*1024)==EOF ) {ret=ERAM;goto Konec;}
	if( AlokDyn(&NDef,32)<0 ) {ret=ERAM;goto Konec;}
	for(;;)
	{
		int NH;
		int ni;
		NastrDef *N;
		char *R,*S;
		R=CtiRadek(f);if( !R ) break;
		S=SCtiSlovo(&R);
		NH=PrazdnyNastr(NULL,False);if( NH<EOK ) {ret=(Err)NH;goto Konec;}
		N=NastrAcc(NULL,NH); /* urcite globalni */
		strlncpy(N->Nazev,S,sizeof(N->Nazev));
		for( ni=0; ni<4; ni++ )
		{
			S=SCtiSlovo(&R);
			if( S ) strlncpy(N->Soubory[ni],S,sizeof(N->Soubory[ni]));
			else strcpy(N->Soubory[ni],"");
		}
	}
	ret=EOK;
	Konec:
	fclose(f);
	return ret;
}


/* nejvyssi uroven */
void AYCtiCFG( const Info *Inf )
{
  CtiSNastr("SAMPLES\\COMPO.ATV");
	F_Divis=CLK33K;
	InitFFreq(44100);
}

Flag AYUlozCFG( Info *Inf )
{
	(void)Inf;
	return False;
}

Err DigiZacNastr( LLHraj *H, double Fr )
{
	FreqIs=Fr;
	AktHraj=H;
	return EOK;
}

Err OJinSNastr( LLHraj *Na, LLHraj *Z )
{
	(void)Na,(void)Z;
	return ECAN;
}
