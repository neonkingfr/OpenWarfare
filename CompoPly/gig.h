#ifndef gig_h__
#define gig_h__

#ifdef __cplusplus
extern "C"
{
#endif

HANDLE init_gig(HWND hwnd, const char *currentDirectory);
void shutDown_gig();

/**
@param buffer destination buffer to fill. Data are expected as stereo (2 channels) 16b signed integer
@param bufferSize size of the destination buffer
@param bufferFreq frequency which will be used for playback
@return number of bytes filled. Game should fill as much as possible, buf does not have to fill the complete buffer. Game must always fill complete 2 channels samples.
*/
size_t sound_gig(void *buffer, size_t bufferSize, int bufferFreq);

#ifdef __cplusplus
};
#endif

#endif // gig_h__
