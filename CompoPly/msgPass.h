#ifndef msgPass_h__
#define msgPass_h__

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

void SendMsg(void *data, size_t size);
size_t GetMsg(void *data, size_t maxSize);

#ifdef __cplusplus
};
#endif

#endif // msgPass_h__
