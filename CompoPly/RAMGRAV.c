/* Gravon- akcni 3D hra */
/* SUMA 9/1994-2/1995 */

#include <windows.h>
#include <malloc.h>

#undef SetProp

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <io.h>
#include <limits.h>
#include <macros.h>
#include <stdc\imgload.h>
#include <stdc\memspc.h>
#include <stdc\fileutil.h>
#include <stdc\infosoub.h>
#include "..\..\compo\cocload.h"
#include "ramgrav.h"
#include <fcntl.h>
#include <assert.h>
#include <float.h>
#include <math.h>
#include <winnt.h>
#if _MSC_VER
#include <crtdbg.h>
#endif
#include "sndWaveOut.h"
#include "gig.h"

#pragma comment(lib,"winmm.lib")

#if _DEBUG
#define __DEBUG _DEBUG
#endif

#if __DEBUG
#define PROFIL 0
#else
#define PROFIL 0
#endif

static HWND g_hostWindow;


static HANDLE gameThread;
static DWORD gameThreadId;

/// terminate requested by the host
static Flag TerminatedFlag;

int RandomMod( int Mod )
{
	int c=(int)(rand()&0x7ff)%20;
	unsigned long Seed=rand()+timeGetTime();
	int rnd;
	while( --c>=0 ) KRand(&Seed);
	rnd=KRand(&Seed);
	return rnd%Mod;
}

DWORD WINAPI GameThread(void *context)
{
  const char **argv = __argv;
  int argc = __argc;
  const char *file = argc>1 ? argv[1] : "GRAVON.DTA\\MUSIC\\GRAVON.COC";

  timeBeginPeriod(1);

	ZacHudba(file);
	while( !TestHraje() && !Terminated() )
  {
  }
	KonHudba();
  timeEndPeriod(1);
  return 0;
}

char GiGDirectory[1024];

HANDLE init_gig(HWND hwnd, const char *currentDirectory)
{
  HANDLE ret = NULL;

  g_hostWindow = hwnd;

  strcpy(GiGDirectory,currentDirectory);

  // dbmain spawned as a thread?
  gameThread = CreateThread(NULL, 128*1024, GameThread, NULL, CREATE_SUSPENDED, &gameThreadId);

  ResumeThread(gameThread);

  DuplicateHandle(GetCurrentProcess(),gameThread,GetCurrentProcess(),&ret,0,FALSE,DUPLICATE_SAME_ACCESS);

  return ret;
}

Flag Terminated(void)
{
  return TerminatedFlag;
}

void shutDown_gig()
{
  // any waiting for the key should terminate
  if (gameThread)
  {
    TerminatedFlag = True;
    // caution: gameThread might request the rendering to be done
    WaitForSingleObject(gameThread,INFINITE);
    CloseHandle(gameThread),gameThread = NULL;
  }

}

void SwapEndianL(int *xx)
{
  int x = *xx;
  *xx = (((x)>>24)&0xff) | (((x)>>8)&0xff00) | (((x)<<8)&0xff0000) | (((x)<<24)&0xff000000);
}
void SwapEndianW(short *xx)
{
  int x = *xx;
  *xx = (((x)>>8)&0xff) | (((x)<<8)&0xff00);
}


void *mallocSpc( size_t size, long Idtf )
{
  long *mem = malloc(size+sizeof(Idtf));
//   memset(mem,0,size);
  //_RPTF3(_CRT_WARN,"malloc %c%c%c%c, size %d ->%p\n",IDTF4CC(Idtf),size,mem);
  *mem++ = Idtf;
  return mem;
}
void freeSpc( void *Com )
{
  if (Com)
  {
    long *mem = Com;
    long Idtf = *--mem;
    size_t size = _msize(mem)-sizeof(Idtf);
    //_RPTF3(_CRT_WARN,"free %c%c%c%c, size %d -> %p\n",IDTF4CC(Idtf),size,mem);
    free(mem);
  }
}
void *reallocSpc( void *Co, size_t size, long Idtf )
{
  long *mem = realloc((char *)Co-sizeof(Idtf),size+sizeof(Idtf));
  *mem++ = Idtf;
  return mem;
}
size_t coreleftSpc( void ){return 10*1024*1024;} // we can assume plenty of RAM on PC

#if !__RELEASE
void DebugError( int Line, const char *File )
{
  char buf[1024];
	if( Line ) sprintf(buf,"Error: %s %d\n",File,Line);
	else sprintf(buf,"%s\n",File);
  OutputDebugString(buf);
#if _MSC_VER
  __debugbreak();
#endif
}
#endif

static long Zpozdeni;


long SystemCas( void )
{
  return timeGetTime()-Zpozdeni;
}

void Vsync(){}

int KRand( unsigned long *seed )
{ /* 0..0x7fff */
	unsigned long r;
	r=(*seed)*22696501L+1;
	/* to je asi prvocislo */
	*seed=r;
	return (int)(r>>16)&0x7fff;
}

