/* Gravon- akcni 3D hra */
/* SUMA 9/1994-9/1994 */

#include <windows.h>
#include <mmsystem.h>
#include <stdio.h>
#include <macros.h>
#include <stdc\memspc.h>
#include "ramgrav.h"
#include "..\..\compo\cocload.h"
#include "STDC\FILEUTIL.H"
#include "COMPO\UTLCOMPO.H"
#include "COMPO\RAMCOMPO.H"

void *myalloc( long a, Flag F ){(void)F;return mallocSpc(a,MULTICHAR('Musi'));}
void myfree( void *a ){freeSpc(a);}
void *myallocSpc( long a, Flag F, long Idtf ){(void)F;return mallocSpc(a,Idtf);}
void myfreeSpc( void *a ){freeSpc(a);}

static Flag Hraje=False;

Flag HudbaHraje( void )
{
	return Hraje;
}

static void ZacHudbaF( const char *Nam )
{
	FILE *f=fopen(Nam,"rb");
	Plati( !Hraje );
	F_Display=False;
	if( f )
	{
    int freq = 44100;
    char musicRoot[256],musicRootAlt[256];
    strcpy(musicRoot,Nam);
    *NajdiNazev(musicRoot)=0;
    strcpy(musicRootAlt,musicRoot);
    strcat(musicRoot,"SSS\\");
		if( NactiPisen(f,musicRoot,musicRootAlt,freq)>=0 )
		{
			int ret=ZacHrajPisen(freq);
			if( ret>=0 ) Hraje=True;
			else
			{
				#if __DEBUG
					char Buf[80];
					sprintf(Buf,"No music <=%d",ret);
					DebugError(0,Buf);
				#endif
				PustPisen();
			}
		}
		else Plati( False );
		fclose(f);
	}
	else Plati( False );
}

void KonHudba( void )
{
	if( Hraje )
	{
		Hraje=False;
		/* mel bys fade-outovat */
		KonHrajPisen();
		PustPisen();
	}
}

void ZacHudba(const char *name)
{
  const char *ext = NajdiPExt(NajdiNazev(name));
	F_Mode2=False;
	AutoRepeat=False;
  if (!stricmp(ext,".coch"))
  {
    HoverFormat = True;
    AutoEffects = True;
	  ZacHudbaF(name);
  }
  else if (!stricmp(ext,".coc"))
  {
    HoverFormat = False;
    AutoEffects = True;
	  ZacHudbaF(name);
  }
  else
  {
    Pisen *P = CtiPisen(name);
    if (P)
    {
			Sekvence *S=NajdiSekv(P,MainMel,0);
      void NactiLocDef( DynPole *nastr);
      NactiLocDef(&P->LocDigi);
      if (S)
      {
        void ZacKontext( Kontext *K );
        Kontext kont;
        CompoErr CE;
        ZacKontext(&kont);
        HoverFormat = False;
      	AutoRepeat=P->Repeat;
        AutoEffects = False;
        CompoCom(True,S,0,&kont,&CE);
      }
    }
  }
}
