/* Gravon- akcni 3D hra */
/* SUMA 9/1994-2/1995 */

#include <windows.h>

//#include "resource.h"

#include <float.h>
#include <math.h>
#include <assert.h>
#include "sndWaveOut.h"

#include "gig.h"
#include <mmsystem.h>
//#include <XInput.h>
#include "sndWaveOut.h"

HWND  g_hWnd = NULL;

BOOL AppActive;

HANDLE gigTerminated;

static bool GameInit()
{

  gigTerminated = init_gig(g_hWnd,".");
  if (gigTerminated)
  {
    return true;
  }
  shutDown_gig();

  return false;
}

LRESULT CALLBACK WindowProc( HWND   hWnd,  UINT   msg,  WPARAM wParam, LPARAM lParam )
{
  switch( msg )
  {
  case WM_ACTIVATE:
    {
      BOOL active = LOWORD(wParam)!=WA_INACTIVE;
      BOOL minimized = HIWORD(wParam)!=0;

      if (active && !minimized)
      {	
        AppActive = TRUE;
      }			
  else
  {
        AppActive = FALSE;				
  }
    }

    return DefWindowProc( hWnd, msg, wParam, lParam );
  case WM_SIZE:
    break;

  case WM_CLOSE:
    PostMessage(hWnd,WM_DESTROY,0,0);
    return TRUE;
  case WM_DESTROY:
    shutDown_gig();
    PostQuitMessage(0);
    g_hWnd = NULL;
    break;

  default:
    return DefWindowProc( hWnd, msg, wParam, lParam );
  }

  return 0;
}


int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )
{
  WNDCLASSEX winClass; 
  //int b[1];
  MSG        uMsg;
  const char WindowClass[]="CompoPly";
  const char AppName[]="Componium Player";
  RECT rect;

  memset(&uMsg,0,sizeof(uMsg));

  winClass.lpszClassName = WindowClass;
  winClass.cbSize        = sizeof(WNDCLASSEX);
  winClass.style         = CS_HREDRAW | CS_VREDRAW;
  winClass.lpfnWndProc   = WindowProc;
  winClass.hInstance     = hInstance;
  winClass.hIcon	       = NULL;
  winClass.hIconSm	     = NULL;
  winClass.hIcon	       = NULL;
  winClass.hIconSm	     = NULL;
  winClass.hCursor       = LoadCursor(NULL, IDC_ARROW);
  winClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  winClass.lpszMenuName  = NULL;
  winClass.cbClsExtra    = 0;
  winClass.cbWndExtra    = 0;

  if( !RegisterClassEx(&winClass) )
    return E_FAIL;

  rect.top = 0;
  rect.left = 0;
  rect.right = 640;
  rect.bottom = 480;

  AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW | WS_VISIBLE, FALSE);

  g_hWnd = CreateWindowEx( 0, WindowClass,  AppName, WS_OVERLAPPEDWINDOW | WS_VISIBLE, 0, 0, rect.right-rect.left, rect.bottom-rect.top, NULL, NULL, hInstance, NULL );

  if( g_hWnd == NULL )
    return E_FAIL;

  ShowWindow( g_hWnd, nShowCmd );
  UpdateWindow( g_hWnd );


  timeBeginPeriod(1);
  if (GameInit())
  {
    bool closed = false;
    while( uMsg.message != WM_QUIT )
    {
      DWORD wait;
      HANDLE handles[1];
      handles[0] = gigTerminated; // TODO: detect the game has terminated itself
      wait = MsgWaitForMultipleObjects(sizeof(handles)/sizeof(*handles),handles,FALSE,INFINITE,QS_ALLEVENTS);
      if( PeekMessage( &uMsg, NULL, 0, 0, PM_REMOVE ) )
      {
        TranslateMessage( &uMsg );
        DispatchMessage( &uMsg );
      }
      if (wait==WAIT_OBJECT_0 || WaitForSingleObject(gigTerminated,0)==WAIT_OBJECT_0)
      {
        // also pretend the rendering was done
        if (!closed)
        {
          PostMessage(g_hWnd,WM_CLOSE,0,0);
          closed = true;
        }
      }
    }
    EndSoundWaveOut();
  }

  UnregisterClass( WindowClass, winClass.hInstance );
  timeEndPeriod(1);

  return uMsg.wParam;
}
