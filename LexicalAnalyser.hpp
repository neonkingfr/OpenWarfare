#include "StreamTokenizer.hpp"

#ifndef LEXICALANALYSER_HEADER
#define LEXICALANALYSER_HEADER

#define TOKEN_STRINGS(XX) \
XX(TIMES,"*"),\
XX(TIMES_ASSIGN,"*="),\
XX(PLUS,"+"),\
XX(PLUS_ASSIGN,"+="),\
XX(MINUS,"-"),\
XX(MINUS_ASSIGN,"-="),\
XX(DIVIDE,"/"),\
XX(DIVIDE_ASSIGN,"/="),\
XX(FULLSTOP,"."),\
XX(LEFTBRACKET,"("),\
XX(RIGHTBRACKET,")"),\
XX(LEFTBRACE,"{"),\
XX(RIGHTBRACE,"}"),\
XX(GREATERTHAN,">"),\
XX(GREATERTHAN_EQUALTO,">="),\
XX(LESSTHAN,"<"),\
XX(LESSTHAN_EQUALTO,"<="),\
XX(MOD,"%"),\
XX(ASSIGN,"="),\
XX(EQUALITY,"=="),\
XX(NOT,"!"),\
XX(NOT_ASSIGN,"!="),\
XX(SEMICOLON,";"),\
XX(COMMA,","),\
XX(COLON,":"),\
XX(RSBRACKET,"]"),\
XX(LSBRACKET,"["),\
XX(VERTICALBAR,"|"),\
XX(BOOLTRUE,"true"),\
XX(BOOLFALSE,"false"),\
XX(CLASS,"class"),\
XX(INTCONST,"INTCONST"),\
XX(FLOATCONST,"FLOATCONST"),\
XX(BOOLEANCONST,"BOOLEANCONST"),\
XX(STRINGLITERAL,"STRINGLITERAL"),\
XX(IDENTIFIER,"IDENTIFIER"),\
XX(UNKNOWN,"UNKNOWN"),\
XX(SOF,"SOF"),\
XX(EOF,"EOF"),\
XX(END,"END")

#define TOKEN_ENUM(IDENT,DELIMITOR_CHAR) T_##IDENT
#define TOKEN_STRING(IDENT,DELIMITOR_CHAR) DELIMITOR_CHAR

extern char* GTokenString[];

enum GTokenEnum
{
  TOKEN_STRINGS(TOKEN_ENUM)
};

struct Token
{
  GTokenEnum  _type;
  
  std::string _identifier;
  std::string _stringLiteralVal;
  int         _intConst;
  float       _floatConst;
  bool        _boolConst;
  
  Token() 
  {
    _intConst = 0;
    _floatConst = 0.0f;
    _boolConst = false;
    _type = T_END;
  }
};

// Main responsibility of the lexical analyzer is to
// convert the unknown symbols from the stream tokenizer
// into recognizable tokens with extra meaning.
// 
// This is so a syntax analyzer can do error checking
// and report to the user.
class LexicalAnalyser : public StreamTokenizer
{
  typedef StreamTokenizer base;

  // Current token we're working with.
  Token _currentToken;

  // Determine the token from the unknown string
  bool DetermineReserveToken(std::string unknown);
  void ProccessToken(const TokenTypes foundType,Token& myNewToken);
public:

  // Typical constructors.
  LexicalAnalyser(ifstream &inputFile):StreamTokenizer(inputFile){};
  LexicalAnalyser(const char *data):StreamTokenizer(data){};

  const Token& NextToken();
  const Token& PreviousToken();

  const Token& GetCurrentToken()
  {
    return _currentToken;
  }
};

#endif