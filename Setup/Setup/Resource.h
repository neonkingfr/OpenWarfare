//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Setup.rc
//
#define IDD_POSSETUP_DIALOG             102
#define IDD_WIZARD_PAGE_DIR             106
#define IDD_WIZARD_PAGE_DRX             108
#define IDD_WIZARD_PAGE_RES             109
#define IDD_WIZARD_PAGE_STP             110
#define IDR_MAINFRAME                   128
#define IDD_RESULT_OK                   137
#define IDD_WIZARD_PAGE_CDKEY           138
#define IDD_INSERT_CDROM_DIALOG         140
#define IDD_WIZARD_PAGE_ALL_USERS       141
#define IDD_WIZARD_PAGE_LICENCE         142
#define IDD_WIZARD_PAGE_INFO            143
#define IDC_ANIMATE_FIRST               1002
#define IDC_DIRECTX_DETECT              1003
#define IDC_DIRECTX_SETUP               1004
#define IDC_DIRECTORY                   1005
#define IDC_CHANGE_DIR                  1006
#define IDC_PROGRESS_COPY               1008
#define IDC_COPY_FROM                   1009
#define IDC_COPY_TO                     1010
#define IDC_SETUP_TEXT                  1010
#define IDC_EDIT_DIRECTORY              1011
#define IDC_ANIMATE_COPY                1013
#define IDC_PICTURE_COPY                1014
#define IDD_SETUP_TITLE                 1015
#define IDC_SETUP_TITLE                 1015
#define IDC_SETUP_TITLE_POS             1017
#define IDC_PICTURE_COPY_POS            1018
#define IDC_README                      1019
#define IDC_RUN_APP                     1020
#define IDC_CDKEY                       1020
#define IDC_CDKEY1                      1020
#define IDC_CDKEY2                      1021
#define IDC_REGISTRATION                1021
#define IDC_CDKEY3                      1022
#define IDC_GAMESPY                     1022
#define IDC_CDKEY4                      1023
#define IDC_FINACTION0                  1023
#define IDC_CDKEY5                      1024
#define IDC_FINACTION1                  1024
#define IDC_FINACTION2                  1025
#define IDC_FINACTION3                  1026
#define IDC_FINACTION4                  1027
#define IDC_FINACTION5                  1028
#define IDC_FINACTION6                  1029
#define IDC_FINACTION7                  1030
#define IDC_FINACTION9                  1031
#define IDC_FINACTION8                  1032
#define IDC_STATICMESSAGE               1033
#define IDC_INSERT_CDROM_MESSAGE        1035
#define IDC_ALL_USERS                   1036
#define IDC_WELCOME_PICT_POS            1037
#define IDC_WELCOME_PICT                1038
#define IDC_LICENCE                     1047
#define IDC_LICENCE2                    1048
#define IDC_LICENCE_TEXT                1049
#define IDC_DISK_SPACE                  1050
#define IDC_INFO_TEXT                   1051

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1051
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
