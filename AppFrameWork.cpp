#include <Windows.h>
#include <WinBase.h>

#include <string>
#include <sstream>
#include "AppFrameWork.hpp"


// Format the arguments into a std::string
// Pre-condition: fmt cannot be null.
std::string STDStringFormat(const char *fmt, va_list args)
{
  Assert(fmt);
  int result = -1;
  int length = 256;
  char *buffer = NULL;

  while (result == -1)
  {
    if (buffer) delete [] buffer;
    buffer = new char [length];
    Assert(buffer);

    // Failed to succeed memory allocation request.
    if (!buffer) return std::string();
    ZeroMemory(buffer,length);
    result = _vsnprintf_s(buffer, length-1, _TRUNCATE, fmt, args);
    length *= 2;
  }

  std::string s(buffer);
  delete [] buffer;
  return s;
}

std::string Format(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  std::string outString = STDStringFormat(format,args); 
  va_end(args);

  return outString;
}

std::string Escape(const char *str)
{
  std::stringstream out;
  while (*str != 0)
  {
    switch (*str)
    {
      case '\\' : out << "\\\\"; break;
      case '\t' : out << "\\t"; break;
      case '"'  : out << "\\\""; break;
      case '\r' : out << "\\\r"; break;
      case '\n' : out << "\\n"; break;
      default: out << *str;
    }
    ++str;
  }
  return out.str();
}

// Normal message
void LogF(const char* format, ...)
{
  va_list args;
  va_start(args, format);
  std::string outString = STDStringFormat(format,args); 
  va_end(args);

  if (GFrameWork) GFrameWork->LogF(outString.data());
  else OutputDebugStringA(outString.data());
}

// Warning message dialog box appearance.
void WarningMessage(const char* format, ...)
{
  va_list args;
  va_start(args, format);
  std::string outString = STDStringFormat(format,args); 
  va_end(args);

  if (GFrameWork) GFrameWork->WarningMessage(outString.data());
}

// Error message program cannot continue operation.
void ErrorMessage(const char* format, ...)
{
  va_list args;
  va_start(args, format);
  std::string outString = STDStringFormat(format,args); 
  va_end(args);

  if (GFrameWork) GFrameWork->ErrorMessage(outString.data());
}

