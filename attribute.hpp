#ifdef _MSC_VER
#  pragma once
#endif

/*
    attribute.hpp

    Global definitions for attribute-mechanism
    @since 28.10.2001

    13.11.2001, PE

    Copyright (C) 2001 by BIStudio (www.bistudio.com)
*/

#ifndef _ATTRIBUTE_H
#define _ATTRIBUTE_H

/**
    @name   AttrType
    Attribute types.
*/
//@{
#define AtTInt           0                  /// 32-bit signed integer type
#define AtTFloat         1                  /// 32-bit floating point type
#define AtTBoolean       2                  /// boolean type
#define AtTString        3                  /// string type (RString, RStringB ???)
#define AtTPoint3D       4                  /// [x,y,z] float triple
#define AtTPoint4D       5                  /// [x,y,z,w] float quadruple
#define AtTMatrix4x3     6                  /// 4x3 homogeneous transformation matrix
// ...
#define AtTNode      0x010                  /// reference to another hierarchy node
// ...
#define AtTArray     0x100                  /// general array (AutoArray?, 1-dimensional, integer indices 0..N-1)
// ...
//@}

/**
    @name   AttrInherit
    Attribute inheritance methods.
*/
//@{
#define AtIAncestor         0x0001          /// ancestor-only inheritance
#define AtIType             0x0002          /// type-only inheritance
#define AtIReference        0x0004          /// assignment through an abstract reference
// ...
#define AtIPriority         0x1000          /// invert the default priority
#define AtICompound         0x2000          /// compound value
#define AtIShared           0x4000          /// the value is shared (only one instance exists)
//@}

/**
    General ancestor of all attribute value types.
*/
class AttributeValue {

public:

    /**
        Locks the attribute value for R/O access.
            Invokes watch-dog pre-synchronization.
        @return NULL in case of invalid access
        @see    lockValueRW()
        @see    unlockValue()
    */
    virtual const void* lockValueRO () { return NULL; };

    /**
        Locks one item of the attribute value array for R/O access.
            Invokes watch-dog pre-synchronization.
        @param  i zero-based array index
        @return NULL in case of invalid access (scalar attribute, index out of range)
        @see    lockValueRW(int)
        @see    unlockValue(int)
    */
    virtual const void* lockValueRO ( int i ) { return NULL; };

    /**
        Locks the attribute value for R/W access.
            Invokes watch-dog pre-synchronization.
        @return NULL in case of invalid access (R/O attribute)
        @see    lockValueRO()
        @see    unlockValue()
    */
    virtual void* lockValueRW ();

    /**
        Locks one item of the attribute value array for R/W access.
            Invokes watch-dog pre-synchronization.
        @param  i zero-based array index
        @return NULL in case of invalid access (scalar attribute, R/O attribute, index out of range)
        @see    lockValueRO(int)
        @see    unlockValue(int)
    */
    virtual void* lockValueRW ( int i );

    /**
        Unlocks the attribute value.
            If the value was locked in R/W mode, assumes that it was changed.
            Invokes watch-dog post-synchronization.
    */
    virtual void unlockValue ();

    /**
        Unlocks one item of the attribute value array.
            If the value was locked in R/W mode, assumes that it was changed.
            Invokes watch-dog post-synchronization.
        @param  i zero-based array index
    */
    virtual void unlockValue ( int i );

    /**
        Value size in bytes.
        @return Value size in bytes
    */
    virtual int valueSize () const;

    /**
        Value size of one array item in bytes.
        @param  i zero-based array index
        @return Value size in bytes
    */
    virtual int valueSize ( int i ) const;

    /**
        Retrieves the actual attribute value.
        @param  dest Destination pointer (able to hold "valueSize" bytes of data).
    */
    void get ( void *dest )
    {
        Assert(dest);
        const void *ptr = lockValueRO();
        Assert(ptr);
        memcpy(dest,ptr,valueSize());
        unlockValue();
    }

    /**
        Changes the actual attribute value.
        @param  src Source pointer (contains "valueSize" bytes of data).
    */
    void put ( void *src )
    {
        Assert(src);
        void *ptr = lockValueRW();
        Assert(ptr);
        memcpy(ptr,dest,valueSize());
        unlockValue();
    }

    /**
        Retrieves value-type flags.
        @return Value-type flags (see AtT* constants)
        @see    arraySize()
    */
    virtual int valueType () const;

    /**
        Retrieves the array size.
        @return Array size (in items). -1 for scalar attributes.
        @see    valueType()
    */
    virtual int arraySize () const { return -1; };

    /**
        Retrieves value-inheritance flags.
        @return Value-inheritance flags (see AtI* constants). Only limited information can be used
                (e.g. AtIShared).
        @see    valueType()
        @see    AtIShared
    */
    virtual int valueInheritance () const;

    };

#endif
