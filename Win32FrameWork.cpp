#include <Windows.h>
#include <WinBase.h>
#include "AppFrameWork.hpp"

class wxTextCtrl;

class Win32FrameWork : public AppFrameWorkBase
{
public:
  Win32FrameWork()
  {
  }

  void SetTextControl(wxTextCtrl *ctrl)
  {
  }

  // System status
  void LogF(const char *msg)
  {
    OutputDebugString(msg);
  }
  // Dialog box pops up
  void WarningMessage(const char *msg)
  {
    OutputDebugString(msg);
  }
  // Dialog pops up and the program does a memory dump and call stack.
  void ErrorMessage(const char *msg)
  {
    OutputDebugString(msg);
  }
};

Win32FrameWork GWin32FrameWrok;
AppFrameWorkBase* GFrameWork = &GWin32FrameWrok;