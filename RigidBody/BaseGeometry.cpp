#ifndef _MARINA_PROJECT 
  #include <Poseidon/lib/wpch.hpp>
#else
  #define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES

//#include "marina.hpp"
//#include "inc.h"
#include <El/Math/math3d.hpp>
#include "BaseGeometry.h"
#include <PhysicalLibs/RigidBody/RBSimBase.hpp>

#ifndef max
#undef  min
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))
#endif

bool AABox::IsIntersection(const AABox& second) const
{
  return (max(_min[0],second._min[0]) < min(_max[0],second._max[0]) &&
    max(_min[1],second._min[1]) < min(_max[1],second._max[1]) &&
    max(_min[2],second._min[2]) < min(_max[2],second._max[2]));
}

void AABox::Enlarge(float size)
{
  _min[0] -= size;
  _min[1] -= size;
  _min[2] -= size;

  _max[0] += size;
  _max[1] += size;
  _max[2] += size;
}

AABox AABox::operator+(const AABox& second) const
{
  Vector3 newMin;
  Vector3 newMax;

  newMin[0] = (min(_min[0], second._min[0]));
  newMin[1] = (min(_min[1], second._min[1]));
  newMin[2] = (min(_min[2], second._min[2]));

  newMax[0] = (max(_max[0], second._max[0]));
  newMax[1] = (max(_max[1], second._max[1]));
  newMax[2] = (max(_max[2], second._max[2]));

  return AABox( newMin, newMax);
}

const AABox& AABox::operator+=(const AABox& second)
{
  _min[0] = (min(_min[0], second._min[0]));
  _min[1] = (min(_min[1], second._min[1]));
  _min[2] = (min(_min[2], second._min[2]));

  _max[0] = (max(_max[0], second._max[0]));
  _max[1] = (max(_max[1], second._max[1]));
  _max[2] = (max(_max[2], second._max[2]));

  return *this;
}

AABox AABox::operator+(const Vector3& second) const
{
  Vector3 newMin;
  Vector3 newMax;

  newMin[0] = (min(_min[0], second[0]));
  newMin[1] = (min(_min[1], second[1]));
  newMin[2] = (min(_min[2], second[2]));

  newMax[0] = (max(_max[0], second[0]));
  newMax[1] = (max(_max[1], second[1]));
  newMax[2] = (max(_max[2], second[2]));

  return AABox( newMin, newMax);
}

const AABox& AABox::operator+=(const Vector3& second)
{
  _min[0] = (min(_min[0], second[0]));
  _min[1] = (min(_min[1], second[1]));
  _min[2] = (min(_min[2], second[2]));

  _max[0] = (max(_max[0], second[0]));
  _max[1] = (max(_max[1], second[1]));
  _max[2] = (max(_max[2], second[2]));

  return *this;
}

bool AABox::IsPointIn(Vector3Val pos) const
{
  for(int i = 0; i < 3; i++)
    if (pos[i] < _min[i] || pos[i] > _max[i])
      return false;
  
  return true;
}

/////////////////////////////////////////////////////////

void IRBGeometry::SetOrigin(const Vector3& pos, const QuaternionRB& orientation)
{
  _pos = pos + orientation.GetMatrix() * _relPos;
  _orientation = orientation * _relOrientation;
}

void IRBGeometry::SetRelOrigin(const Vector3& relPos, const QuaternionRB& relOrientation)
{
  _relPos = relPos;
  _relOrientation = relOrientation;
}

void IRBGeometry::TranslateRelOrigin(const Vector3& relPos)
{
  _pos = _pos + relPos;
  _relPos += relPos;
}

#endif _ENABLE_RIGID_BODIES
