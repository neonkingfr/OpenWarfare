#ifdef _MSC_VER
#pragma once
#endif

#ifndef __JOINTRB_HPP__
#define __JOINTRB_HPP__

#if _ENABLE_RIGID_BODIES
#include <Poseidon/lib/object.hpp>
#include <Poseidon/lib/objectHierarchyExt.hpp>

#include <Es/Memory/fastAlloc.hpp>


/* Joints in physical simulation points of view are virtual structures. thats represent constrains of the system.
 * Constrains are reducion of degrees of freedom (DOFs). In contact of the two bodies movement of bodies into each other
 * is restricted...etc. So joint is definition which DOFs will be restricted and how. 
 *
 * Joints connect two bodies or body to void (connect body to unknown static entity). The second option is not prefered 
 * rather connect bidy to some invisible static body. In lot of joints is impossible to define limits 
 * and do error correction if second body is not defined. But still if we do not care much about error corrections 
 * it can be usefull.  
 */ 

struct RBContactPoint;
struct dxWorld;


//------------------------------------
// IDtoIRigidBody
//------------------------------------


class IDtoIRigidBody
{
public:
  virtual IRigidBody * GetIRigidBody(HierarchyIDVal id) = 0; 
};

//------------------------------------
// JointWithIRigidBody
//------------------------------------

class JointWithIRigidBody : public JointRB
{
  typedef JointRB base;
protected:
  InitPtr<IRigidBody> _body[2]; // should be link
public:
  //! Transfers bodies IDs to IRigidBody objects.
  virtual void SetIRigidBodies(IDtoIRigidBody& src) = 0;

  //! Returns colliding bodies. i can be 0 or 1.
  Ref<IRigidBody> GetIRigidBody(int i) const {return _body[i].GetRef();};

  //! Returns true if IRigidBody represents the same body 
  bool HasIRigidBody(const IRigidBody& body, int i) const {return _body[i] ? body == *_body[i] : false;};  
  USE_CASTING(base);
};


//------------------------------------
// ContactJointRB
//------------------------------------
// Structure is big but it is allocated quite often so use fast alloc
#include <Es/Memory/normalNew.hpp>
//! Contact joint created by detection collision algorithm. It is not part of the hierarchy
class ContactJointRB: public JointWithIRigidBody
{
  typedef JointWithIRigidBody base;
protected:
  RBContactPoint _contact; //< definition of contact
  int _the_m; //< number of DOF reduced
  dxWorld * _world; //< global phys configuration
  bool _bounce;  //< is contact bounce/impulse (bodies colliding with speed -> leading to impulse transfer)
  float _lambda[3]; //< result from last step 

public:
  ContactJointRB() {}; 
  ContactJointRB(dxWorld * world, const RBContactPoint& contact) : _world(world), _contact(contact), _bounce(false) {};
  ~ContactJointRB() {};
 
  //! Initialization.
  void Create(dxWorld * world, const RBContactPoint& contact);    

  //! Returns basic info about joint (number of DOF etc...)
  void GetInfo1 (Info1 *info);

  //! Returns Jacobian, cfm... 
  void GetInfo2 (Info2 *info);

  //! Returns position of joint. 
  Vector3 GetPos() const {return _contact.pos;};

  //! Transfers bodies IDs to IRigidBody objects.
  void SetIRigidBodies(IDtoIRigidBody& src);

  //! Is contact bounce/impulse-like (bodies colliding with speed -> leading to impulse transfer)
  bool IsImpulse() const {return _bounce;};

  //! Informs joint about result forces in joint.
  void SetResult(float * lambda); 
  //! Returns force in world coordinate that was applied by joint. 
  Vector3 GetForceInJoint() const ; // from body 0 to body 1
  //! Returns torque in world coordinate that was applied by joint to body. 
  Vector3 GetTorqueOnBody(int i) const ;

#if _ENABLE_CHEATS
  void DiagDraw();
#endif
  USE_FAST_ALLOCATOR;
  USE_CASTING(base);
};
#include <Es/Memory/debugNew.hpp>
//------------------------------------
// JointLimitMotor
//------------------------------------
/** Joints usually does not restrict all 6 DOFs, there are some free moving and rotation axes. 
  * In many applications there is need to set limits of the movement or add motor to the movement. 
  * Limits simply adds restriction to the corresponding DOF if moving bodies reaches some limit. 
  * Motors forces bodies to have defined speed of movements (also different type of motors can be written, f.e.
  * one stats force bodies to have some distance etc...).
  *
  * The following class defined interface for limits and motors that can be added to free DOFs of joints.
  */

class JointLimitMotorBase : public RefCount
{
protected:
  int _dof; //< id of DOF influenced by the limitMotor. Each joint defines its ids. 
public:
  JointLimitMotorBase(int dof) : _dof(dof) {};

  int Dof() const {return _dof;};
  //! Returns true if structure corresponds to motor.
  virtual bool IsPowered() const {return false;};  
  //! Returns true if structure corresponds to limit.
  virtual bool IsLimit() const {return false;}; 
  //! Returns true if value is at or beyond the limit.
  virtual bool IsAtLimit(float val) const {return false;};
  //! Modifies Info2 set constrains into LPC. BE WARE Jacobians are not filled. by this function.
  virtual void Apply(Info2& info, float val, int row) const = 0; 
};

//! Motor is trying to keep velocity in dof by applying force or torque there 
class JointMotor : public JointLimitMotorBase
{
  typedef JointLimitMotorBase base;
protected:
  float _vel;  //< velocity to keep. 
  float _fmax;		//< maximal force or torque that can be applied
  float _normal_cfm;		//< cfm to use when not at a stop

public:
  JointMotor(int dof, float vel, float fmax = -1, float normal_cfm = -1) : base(dof), _vel(vel), _fmax(fmax), _normal_cfm(normal_cfm) {};
  bool IsPowered() const  {return true;};
  void Apply(Info2& info, float val, int row) const;
  
};

//! Joint limit.
class JointLimit: public JointLimitMotorBase
{
  typedef JointLimitMotorBase base;
protected:
  float _lostop;
  float _histop;		//< joint limits
  float _stop_erp;
  float _stop_cfm;	//< erp and cfm for when at joint limit  

public:
  JointLimit(int dof, float lostop, float histop, float stop_cfm = -1, float stop_erp = -1)
    : base(dof), _lostop(lostop), _histop(histop), _stop_erp(stop_erp), _stop_cfm(stop_cfm) {};

  bool IsLimit() const {return true;}; 
  bool IsAtLimit(float val) const {return val <= _lostop || val >= _histop;}; 
  void Apply(Info2& info, float val, int row) const;
};

//! Joint limit. It assumes that values corresponds to angles in radians.
class JointLimitAngular : public JointLimit
{
  typedef JointLimit base;
public:
  JointLimitAngular(int dof, float lostop, float histop, float stop_cfm = -1, float stop_erp = -1, float restitution = 0) :
  JointLimit( dof, lostop, histop, stop_cfm, stop_erp) {};
  
  bool IsAtLimit(float val) const; 
  void Apply(Info2& info, float val, int row) const;
};

//------------------------------------
// HierarchyJointRB
//------------------------------------
class HierarchyJointRBWrap;

/** Parent class for joints in hierarchy.*/
class HierarchyJointRB : public HierarchyItemNoShape
{
protected:
  //< relative ids to bodies joined by joint. if id is not valid bodies is static. 
  HierarchyIDRelative _id[2]; //< relative ids to bodies joined by joint. if id[1] is not valid body is static.
  RefArray<JointLimitMotorBase> _limitMotors;  

  float _cfm; //< CFM for joint
  float _erp; //< ERP for joint

  HierarchyJointRB(const HierarchyJointRB& src);

  //! Returns basic info about joint (number of DOF etc...)
  virtual void GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info) = 0;
  //! Returns Jacobian, cfm...
  virtual void GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info) = 0;
  //! Returns position of joint. 
  virtual Vector3 GetPos(IRigidBody * body0) const = 0;
  //! Returns force in world coordinate that was applied by joint.
  virtual Vector3 GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const = 0;  // from body 0 to body 1
  //! Returns torque in world coordinate that was applied by joint to body. 
  virtual Vector3 GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const = 0;
  //! Returns colliding bodies. i can be 0 or 1.
  Ref<IRigidBody> GetIRigidBody(HierarchyIDVal scope, int i) const;

  //!Returns true if joint is breakable and is breaking
  virtual bool IsBreaking(IRigidBody * body0,IRigidBody* body1, const float * lambda) const {return false;};
public:
  HierarchyJointRB() : _cfm(-1), _erp(-1) {}; 

  //! Creates join. If body1Path is zero, it will be attached to undef static object   
  void Create(const HierarchyItem& parent, ConstHierarchyPathRef myPath, HierarchyPathVal body0Path, ConstHierarchyPathRef body1Path); 
  //! Destroy ids.
  void OnRemove(HierarchyIDVal scope); 
  //! Return if a joint has physical meaning (it introduces a constrain). For exmaple joint than only disables collision for bodies
  virtual bool IsPhysicalJoint() const {return true;};
  //! Is contact bounce/impulse-like (bodies colliding with speed -> leading to impulse transfer).
  virtual bool IsImpulse() const {return false;};
  //! Returns colliding bodies. i can be 0 or 1.
  HierarchyID GetBodyID(HierarchyIDVal scope, int i) const; 
  //! If true, bodies connected by this joint will be not tested for collision detection
  virtual bool BodiesNotCollide() const {return false;};
  //! appends new structure for limit
  virtual void AddLimitMotor(JointLimitMotorBase * limitMotor) {_limitMotors.Add(limitMotor);_limitMotors.Compact();};

  // manipulation of ids
  bool NeedChangeForItemMoved(const HierarchyID& scope, HierarchyPathVal iD) const;
  void ItemMoved(const HierarchyID& scope, HierarchyPathVal iDfrom, HierarchyPathVal iDto);  
  void ItemRemoved(const HierarchyID& scope, HierarchyPathVal removed); 

  void SetCfmErp(float cfm, float erp) {_cfm = cfm; _erp = erp;};
#if _ENABLE_CHEATS
  virtual void DiagDraw(HierarchyIDVal scope, float * lambda) = 0;
#endif 

  friend HierarchyJointRBWrap;
};

//------------------------------------
// HierarchyJointRBWrap
//------------------------------------
#include <Es/Memory/normalNew.hpp>
/** Temporary representation of HierarchyJointRB in phys library. It exists only during one simulation frame.
  * It wraps hierarchy specific interface and provides hierarchy independent JointRB interface.
  */
class HierarchyJointRBWrap : public JointWithIRigidBody
{
  typedef JointWithIRigidBody base;
protected:
  HierarchyID _id;//< id of source joint
  HierarchyID _bodyID[2];//< id of source joint

  HierarchyJointRB * _joint; //< source joint object
  float _lambda[6];   //< result from last step 

public:
  HierarchyJointRBWrap(HierarchyIDVal _id);

  //! Returns basic info about joint (number of DOF etc...)
  void GetInfo1 (Info1 *info) {_joint->GetInfo1(_body[0],_body[1],info);};
  //! Returns Jacobian, cfm...
  void GetInfo2 (Info2 *info) {_joint->GetInfo2(_body[0],_body[1],info);};
  //! Transfers bodies IDs to IRigidBody objects.
  void SetIRigidBodies(IDtoIRigidBody& src);  
  //! Returns true if IRigidBody represents the same body 
  bool HasIRigidBody(const IRigidBody& body, int i) const;
  //! Returns colliding bodies. i can be 0 or 1.
  HierarchyID GetBodyID(int i) const {return _bodyID[i];};
  //! Returns id of joint 
  HierarchyIDVal GetJointID() const {return _id;};

  //! Returns position of joint. 
  Vector3 GetPos() const {return _joint->GetPos(_body[0]);};

  //! Is contact bounce/impulse-like (bodies colliding with speed -> leading to impulse transfer).
  bool IsImpulse() const {return _joint->IsImpulse();};
  //! If true, bodies connected by this joint will be not tested for collision detection
  bool BodiesNotCollide() const {return _joint->BodiesNotCollide();};
 
  //! Informs joint about result forces in joint.
  void SetResult(float * lambda);
  //! Returns force in world coordinate that was applied by joint.
  Vector3 GetForceInJoint() const;  // from body 0 to body 1
  //! Returns torque in world coordinate that was applied by joint to body.
  Vector3 GetTorqueOnBody(int i) const;

  /** Function is called after simulation and allows the joint to react on force and impulses. 
  * The typical example will be breakable joints, that will be breaking here.
  */
  void RunReactors(float deltaT, bool impulseJoints);

#if _ENABLE_CHEATS
  void DiagDraw();
#endif
  USE_FAST_ALLOCATOR;
  USE_CASTING(base);
};

#include <Es/Memory/debugNew.hpp>


//------------------------------------
// BallJointRB
//------------------------------------
//! Ball joint. Two bodies connected by their anchors. It has 3 free rotational DOFs.
class BallJointRB : public HierarchyJointRB 
{
  typedef HierarchyJointRB base; 
protected:

  /** Positions of the anchors in local(model) coord of bodies. _pos[0] in the local coordinates of the body 0 and
    * _pos[1] in the local coord of the body 1. If the body 1 is static _pos[1] is in world coord. */    
  Vector3 _pos[2]; 
  Vector3 _dir[3]; //< coordination system used for forces in local coord of body 0

  bool _bodiesNotCollide; //< of true bodies connected by the joint will not collide 

  //! Returns basic info about joint (number of DOF etc...)
  void GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info);
  //! Returns Jacobian, cfm...
  void GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info);
  //! Returns force in world coordinate that was applied by joint.
  Vector3 GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const;  // from body 0 to body 1
  //! Returns torque in world coordinate that was applied by joint to body. 
  Vector3 GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const;
  //! Returns position of joint. 
  Vector3 GetPos(IRigidBody * body0) const {return body0->PositionBodyToWorld(_pos[0]);};

  BallJointRB(const BallJointRB& src);
public:
  BallJointRB();  

  HierarchyItem * Clone() const;

  //! Initialization
  void Init(HierarchyIDVal scope, Vector3Val pos, Vector3 * dir = NULL);  

  /** Appends limit or motor. 
    * Ball joints has free 3 rotational DOFs. They are numbered for 0 - 2 and corresponds to rotation in axes _dir[0] -> _dir[2].
    * BE WARE the rotations cannot be limited by limits. Only motors are allowed. For cretation of limited rotations,
    * Use Hinge2 + Hinge configuration. */
  void AddLimitMotor(JointLimitMotorBase * limitMotor);
  //! If true, bodies connected by this joint will be not tested for collision detection
  bool BodiesNotCollide() const {return _bodiesNotCollide;};
  //! Enables/disable collision between bodies connected by joint
  void EnableCollision(bool enable) {_bodiesNotCollide = !enable;};
  

#if _ENABLE_CHEATS
protected:
  PackedColor _color;
public:
  void DiagDraw(HierarchyIDVal scope, float * lambda);
#endif
};

//------------------------------------
// FixedJointRB
//------------------------------------
/** Fixed joint. The relative position and orientation of two bodies will be fixed. In fact it glues two bodies.
  */
class FixedJointRB : public BallJointRB
{
  typedef BallJointRB base;
protected:
  QuaternionRB _qrel;		//< initial relative rotation body0 -> body1

  //! Returns basic info about joint (number of DOF etc...)
  void GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info);
  //! Returns Jacobian, cfm...
  void GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info); 
  //! Returns torque in world coordinate that was applied by joint to body. 
  Vector3 GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const;

  FixedJointRB(const FixedJointRB& src);
public:
  FixedJointRB() : base() {};

  HierarchyItem * Clone() const;

  //! If true, bodies connected by this joint will be not tested for collision detection
  bool BodiesNotCollide() const {return true;};

  // in parent from scoop coordinate system
  void Init(HierarchyIDVal scope, Vector3Val pos, Vector3 * dir = NULL);  

  //! No free DOF so cannot add limit or motor
  void AddLimitMotor(JointLimitMotorBase * limitMotor);  

};

//------------------------------------
// BreakableFixedJointRB
//------------------------------------
/** The same as FixedJointRB, but it can apply only limited force and torque. If applied forces and
  * torque is not sufficient to prevent bodies moving joints os broken -> removed. */
class BreakableFixedJointRB : public FixedJointRB
{
  typedef FixedJointRB base;
protected: 
  Vector3 _forceBoundHi; //< max allowed force in coord system BallJointRB::_dir
  Vector3 _forceBoundLo; //< min allowed force in coord system BallJointRB::_dir

  Vector3 _torqueBoundHi; //< max allowed torque in coord system BallJointRB::_dir
  Vector3 _torqueBoundLo; //< min allowed torque in coord system BallJointRB::_dir

  //! Returns Jacobian, cfm...
  void GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info); 

  //!Returns true if joint is breakable and is breaking
  bool IsBreaking(IRigidBody * body0,IRigidBody* body1, const float * lambda) const;

  // can  be used only during clone
  BreakableFixedJointRB(const BreakableFixedJointRB& src);
public:  
  BreakableFixedJointRB() : base() {};
  //! Clones joint. 
  HierarchyItem * Clone() const;  
  //! Returns Jacobian, cfm...
  void GetInfo2 (HierarchyIDVal scope, Info2 *info);

  //! Set limits for force in coord system BallJointRB::_dir
  void SetBreakForce(Vector3Val lo, Vector3Val hi) {_forceBoundLo = lo; _forceBoundHi = hi;};
  //! Set limits for torque in coord system BallJointRB::_dir
  void SetBreakTorque(Vector3Val lo, Vector3Val hi) {_torqueBoundLo = lo; _torqueBoundHi = hi;};
#if _ENABLE_CHEATS
  void DiagDraw(HierarchyIDVal scope, float * lambda);
#endif  
};

//------------------------------------
// SliderJointRB
//------------------------------------
/** Slider joint. Connected bodies can move only in one direction. 
*/
class SliderJointRB : public HierarchyJointRB 
{
  typedef HierarchyJointRB base; 
protected:
  /** Positions of the anchors in local (model) coord of bodies. _pos[0] in the local coordinates of the body 0 and
  * _pos[1] in the local coord of the body 1. If the body 1 is static _pos[1] is in world coord. */    
  Vector3 _pos[2];
  Vector3 _dir[3]; //< coordination system used for forces in local coord of body 0, _dir[0] is axe of joint
  QuaternionRB _qrel; //< relative orientation of the bodies

  bool _bodiesNotCollide; //< of true bodies connected by the joint will not collide 

  //! Returns basic info about joint (number of DOF etc...)
  void GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info);
  //! Returns Jacobian, cfm...
  void GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info); 
  //! Returns force in world coordinate that was applied by joint.
  Vector3 GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const;  // from body 0 to body 1
  //! Returns torque in world coordinate that was applied by joint to body. 
  Vector3 GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const;
  Vector3 GetPos(IRigidBody * body0) const {return body0->PositionBodyToWorld(_pos[0]);};

  SliderJointRB(const SliderJointRB& src);
public:
  SliderJointRB();  

  HierarchyItem * Clone() const;

  /** Initialization.
    * @param scope -  joint ID.
    * @param pos - position of the anchor in world coord.
    * @param axe - direction in which can be body move in world coord. 
    * @param dir - array of two direction in world coord. Together with axe define coordination system of the joint.
    */
  void Init(HierarchyIDVal scope, Vector3Val pos, Vector3Val axe, Vector3 * dir = NULL);  

  //! Appends limit or motor. Only one DOF is free. 
  void AddLimitMotor(JointLimitMotorBase * limitMotor);
  //! If true, bodies connected by this joint will be not tested for collision detection
  bool BodiesNotCollide() const {return _bodiesNotCollide;};
  //! Enables/disable collision between bodies connected by joint
  void EnableCollision(bool enable) {_bodiesNotCollide = !enable;};

#if _ENABLE_CHEATS
protected:
  PackedColor _color;
public:
  void DiagDraw(HierarchyIDVal scope, float * lambda);
#endif
};

//------------------------------------
// HingeJointRB
//------------------------------------
//! Hinge joint. Two bodies can only rotate in one axe. 
class HingeJointRB: public BallJointRB
{
  typedef BallJointRB base;
protected:
  Vector3 _axe1; //< Axe in body 1 coordinate system, _dir[0] is axe in body0 coordinate system. 
  QuaternionRB _qrel; //< relative orientation of bodies

  //! Returns basic info about joint (number of DOF etc...)
  void GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info);
  //! Returns Jacobian, cfm...
  void GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info);   
  //! Returns force in world coordinate that was applied by joint.
  Vector3 GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const;  // from body 0 to body 1
  //! Returns torque in world coordinate that was applied by joint to body. 
  Vector3 GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const;
  //! Returns relative rotation in joint
  float GetRotAngle(IRigidBody * body0,IRigidBody* body1) const; 

  HingeJointRB(const HingeJointRB& src);
public:
  HingeJointRB();

  HierarchyItem * Clone() const;

  /** Initialization.
  * @param scope -  joint ID.
  * @param pos - position of the anchor in world coord.
  * @param axe - axe of rotation in world coord. 
  * @param dir - array of two direction in world coord. Together with axe define coordination system of the joint.
  */
  void Init(HierarchyIDVal scope, Vector3Val pos, Vector3 axe, Vector3 * dir = NULL);

  //! Appends limit or motor. Only one DOF is free. 
  void AddLimitMotor(JointLimitMotorBase * limitMotor);
  //! If true, bodies connected by this joint will be not tested for collision detection
  bool BodiesNotCollide() const {return _bodiesNotCollide;};
  //! Enables/disable collision between bodies connected by joint
  void EnableCollision(bool enable) {_bodiesNotCollide = !enable;};
};


//------------------------------------
// Hinge2JointRB
//------------------------------------

// Hinge2Joint is combination of two Hinge joints, but the axes of joints are not parallel. 
class Hinge2JointRB: public HierarchyJointRB
{
  typedef HierarchyJointRB base;
protected:
  /** Positions of the anchors in local(model) coord of bodies. _pos[0] in the local coordinates of the body 0 and
  * _pos[1] in the local coord of the body 1. If the body 1 is static _pos[1] is in world coord. */    
  Vector3 _pos[2]; 
  Vector3 _dir[3]; //< coordination system for forces in body1 

  Vector3 _axe0; //< Rotation axe of body0 in body0 coordinate system
  Vector3 _axe1; //< Rotation axe of body1 in body1 coordinate system


  Vector3 _coord0; //< Auxialiary values , used for determination of angle in axes.
  Vector3 _coord1; //< Auxialiary values , used for determination of angle in axes. 

  float _c,_s; //< Auxialiary values, used for determination of angle in axes.

  bool _bodiesNotCollide; //< of true bodies connected by the joint will not collide 

  //! Returns basic info about joint (number of DOF etc...)
  void GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info);
  //! Returns Jacobian, cfm...
  void GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info);   
  //! Returns force in world coordinate that was applied by joint.
  Vector3 GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const;  // from body 0 to body 1
  //! Returns torque in world coordinate that was applied by joint to body. 
  Vector3 GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const;
  //! Returns relative rotation in axe0
  float GetRotAngle0(IRigidBody * body0,IRigidBody* body1) const; 
  //! Returns relative rotation in axe1
  float GetRotAngle1(IRigidBody * body0,IRigidBody* body1) const; 
  //! Returns position of joint. 
  Vector3 GetPos(IRigidBody * body0) const {return body0->PositionBodyToWorld(_pos[0]);};

  Hinge2JointRB(const Hinge2JointRB& src);
public:
  Hinge2JointRB();

  HierarchyItem * Clone() const;
  /** Initialization.
  * @param scope -  joint ID.
  * @param pos - position of the anchor in world coord.
  * @param axe0 - the first axe of rotation in world coord. 
  * @param axe1 - the second axe of rotation in world coord. 
  */
  void Init(HierarchyIDVal scope, Vector3Val pos, Vector3Val axe0, Vector3Val axe1);  
  //! Appends limit or motor. Two DOFs are free, 0 - the first ace of rotation, 1 - the second axe of rotation,
  void AddLimitMotor(JointLimitMotorBase * limitMotor); 
  //! If true, bodies connected by this joint will be not tested for collision detection
  bool BodiesNotCollide() const {return _bodiesNotCollide;};
  //! Enables/disable collision between bodies connected by joint
  void EnableCollision(bool enable) {_bodiesNotCollide = !enable;};

#if _ENABLE_CHEATS
protected:
  PackedColor _color;
public:
  void DiagDraw(HierarchyIDVal scope, float * lambda);
#endif
};

//------------------------------------
// Universal Joint
//------------------------------------

//! Universal joint allows user to choose with DOFs will be constrained. 
class UniversalJointRB : public HierarchyJointRB
{
  typedef HierarchyJointRB base;
protected:
  /** Positions of the anchors in local(model) coord of bodies. _pos[0] in the local coordinates of the body 0 and
  * _pos[1] in the local coord of the body 1. If the body 1 is static _pos[1] is in world coord. */    
  Vector3 _pos[2]; 
  Vector3 _dirPos[3]; //< coordination system for forces in body1 coord
  Vector3 _dirRot[3]; //< coordination system for forces in body1 coord 

  QuaternionRB _qrel; //< relative rotation between bodies

  bool _bodiesNotCollide; //< of true bodies connected by the joint will not collide 
  unsigned char _used; //< lowest 6 bits are 6 dofs. if bit is 1 dof is constrained. the most lowest three are linear dofs the rest is angular dofs

  //! Returns basic info about joint (number of DOF etc...)
  void GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info);
  //! Returns Jacobian, cfm...
  void GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info);   
  //! Returns force in world coordinate that was applied by joint.
  Vector3 GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const;  // from body 0 to body 1
  //! Returns torque in world coordinate that was applied by joint to body. 
  Vector3 GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const;
  //! Returns position of joint. 
  Vector3 GetPos(IRigidBody * body0) const {return body0->PositionBodyToWorld(_pos[0]);};

  UniversalJointRB(const UniversalJointRB& src);
public:
  UniversalJointRB();

  HierarchyItem * Clone() const;

  /** Initialization,
    * @param scope - joint ID.
    * @param frozen -  constrained dofs. see UniversalJointRB::_used
    * @param pos - position of the anchor in world coord.
    * @param dirPos - 3 vectors define axes for linear movement.
    * @param dirRot - 3 vectors define axes for rotational movement.
    */  
  void Init(HierarchyIDVal scope, unsigned char frozen, Vector3Val pos, Vector3* dirPos = NULL, Vector3* dirRot = NULL);

  //! Appends limit or motor. Add limits and motors to free DOFs. BE WARE rotational DOFs cannot be limited. 
  void AddLimitMotor(JointLimitMotorBase * limitMotor);
  //! If true, bodies connected by this joint will be not tested for collision detection
  bool BodiesNotCollide() const {return _bodiesNotCollide;};
  //! Enables/disable collision between bodies connected by joint
  void EnableCollision(bool enable) {_bodiesNotCollide = !enable;};

#if _ENABLE_CHEATS
protected:
  PackedColor _color;
public:
  void DiagDraw(HierarchyIDVal scope, float * lambda);
#endif
};
#endif //_ENABLE_RIGID_BODIES
#endif //__JOINTRB_HPP__