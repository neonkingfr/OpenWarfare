#include <Poseidon/lib/wpch.hpp>



//#include <Es/Common/global.hpp>
//#include <Es/Framework/potime.hpp>

#include <El/Common/perfProf.hpp>
#include <El/Common/perfLog.hpp>
#include <El/HiResTime/hiResTime.hpp>

#include <Poseidon/lib/world.hpp>
#include <Poseidon/lib/scene.hpp>
#include <Poseidon/lib/diagmodes.hpp>

#include <El/bb/obb.hpp>

#if _ENABLE_RIGID_BODIES
#include <Poseidon/lib/landscape.hpp>
#include "ODESim.hpp"
#include <PhysicalLibs/RigidBody/cd/CDCache.hpp>
#include "include/ode.h"



//--------------------------------------
//  GetCost
//--------------------------------------
float CalculatePhysSimulationCost(float maxSimStep, float nbodies, float ncontactjoints, float permanentjoints)
{
  return 0.05f / maxSimStep * (nbodies + 0.5 * permanentjoints + ncontactjoints);
}

//--------------------------------------
// RigidBodyGroup
//--------------------------------------

DEFINE_CASTING(RigidBodyGroup);

HierarchyItem * RigidBodyGroup::Clone() const 
{
  RigidBodyGroup * clone = new RigidBodyGroup();
  AfterClone(clone);
  return clone; 
}

void RigidBodyGroup::AfterClone(HierarchyItem * src) const
{
  base::AfterClone(src);
  RigidBodyGroup * clone = static_cast<RigidBodyGroup *>(src);
  clone->_disabled = _disabled;
  clone->_forceActualize = _forceActualize;
}

void RigidBodyGroup::Disable(HierarchyIDVal id)
{
  if (!_disabled)
  {
    GWorld->StopSimulationRBGroup(id);
    _disabled = true;
  }
};
void RigidBodyGroup::Enable(HierarchyIDVal id)
{
  if (_disabled)
  {  
    GWorld->StartSimulationRBGroup(id);
    _disabled = false;
  }
};

void RigidBodyGroup::OnRemove(HierarchyIDVal id)
{
  if (!_disabled)
    GWorld->StopSimulationRBGroup(id);
}

float RigidBodyGroup::SimulationCost() const
{
  if (_simulationCost < 0)
  {
    // make estimation
    return CalculatePhysSimulationCost(0.01f, _grpItems.Size(), 0, _grpItems.Size() * 10); //< pessimistic guess
  }
  return _simulationCost;
}

void RigidBodyGroup::RemoveObjectFromGroup(const HierarchyItem& parent, ConstHierarchyPathRef myPath, HierarchyPathVal objPath)
{
  if (_simulationCost > 0)
  {
    _simulationCost *= (_grpItems.Size() - 1.0f) / _grpItems.Size();
  }

  base::RemoveObjectFromGroup( parent, myPath,  objPath);
}

//--------------------------------------
// ODESim
//--------------------------------------

ODESim::ODESim() : _maxFreeResource(300)
{
  _world = 0;
  _simCycle = 0;
  _simTime = 0;
}

bool ODESim::Init()
{
  g_cdCache.Clear(); // TODO: move clearing of cache to the different position
  _world = dWorldCreate();  
  _simCycle = 0;
  _simTime = 0;

  _freeResource = _maxFreeResource;
  _blockedResource = 0;

  dWorldSetGravity(_world, 0, -9.8f , 0);
  return true;
}

/* Physics does not contain its representation of the world. It is working directly with items from hierarchy.
 * The items corresponding to simulated rigid bodies must not to be individualized (unique). Usually they will be 
 * and after first move they will be individualized. But still there is a chance that it will be not needed. 
 * The first implementation works with IDs. When it needs a item from hierarchy it gets from ID. That was very robust 
 * system, but unfortunately unusable slow. Now simulation "caches" items from hierarchy. So it creates objects witch 
 * usually contains id, corresponding item and transformation till that object in hierarchy etc... Such objects are valid
 * only during one Simulate call. 
 *
 * These increases the performance but has a big disadvantage. What if somebody individualizes (make CopyOnWrite) the item 
 * or individualizes some of objects under it in hierarchy, or deletes the object (for example joint). 
 * Pointer is not more valid. So If during simulation is made anything with item the objects must to be 
 * updated/actualizated. That is really the BIG point. Always think about it, when write rectors, joints, simulation 
 * or anything connected to phys. 
 *
 * The simulation has several parts:
 * 1.) Create temporary representation of active groups
 * 2.) Detect collisions for group. Join groups if needed. Create joints.
 * 3.) Simulate inner processes of bodies in group. It means prepare bodies for simulation. DO NOT delete them, only 
 *   apply forces, impulses, create extra joints etc...
 * 4.) Solve LCP problem, move bodies. Calculation of next state
 * 5.) Run reactors. Most "dangerous" part. Reactors can do anything, they can delete items, individualize them etc,
     f.e. remove joints, destroy body etc. There is a system which allows to do such a things see RigidBodyGroup::SetForceActualize, but
     still preferred way is to only notice it to items and do any heavy operations on hierarchy during SimulatePost step.
 * 6.) Disable not moving groups.
 * 7.) SimulationPost. Simulation is over let items to do what they want. But still verify that any operation on hierarchy will not lead to crash during 
 *   call of SimulationPost for rest of bodies. 
 */
  
bool ODESim::Simulate(const FindArray<HierarchyID>& activeGroupIDs, float time)
{
  PROFILE_SCOPE_EX(odeSi,*);

#ifdef _XBOX
  return true;
#endif
  _freeResource= _maxFreeResource - _blockedResource;

  if (_world == NULL)
    return false; // probably not initialized yet.

  if (time == 0 || activeGroupIDs.Size() == 0)
    return true; // nothing to do
  
  //unsigned64 spendTime = getSystemTime();
#if _ENABLE_CHEATS
  SectionTimeHandle startTime = StartSectionTime();
#endif
  float timeToSim = time;  

  // Handle slow frames --> slow phys time
  if (timeToSim > 0.1f)
  {
    timeToSim = 0.1f;     
  }  

  // 1.) Create temporary representation of active groups
  ActiveGroupArray activeGroups;
  activeGroups.Realloc(activeGroupIDs.Size());

  AUTO_FIND_ARRAY(HierarchyID, staticBodiesIDs, 64);  
  RefArray<HierarchyIRigidBody> staticBodiesRB;

  float maxSimTime = FLT_MAX; // maximal allowed simulation step... minimum from all simulation steps
  for(int i = 0; i < activeGroupIDs.Size(); i++)
  {
    PROFILE_SCOPE(odeLP);    
    Ref<ActiveGroup> newGrp = new ActiveGroup(staticBodiesIDs, staticBodiesRB);
    activeGroups.AddFast(newGrp);
    newGrp->Create(activeGroupIDs[i],0, _world);    
    _freeResource -= newGrp->SimulationCost(); // better will be to get simulation cost... 
    saturateMin(maxSimTime, newGrp->GetSimStep());
    ADD_COUNTER(odeBo, newGrp->GetIRigidBodies().Size());
  }

  float actTime = 0;
  bool lastCycle = false;
  do 
  {
    // the same simulation time for all groups    
    saturateMin(maxSimTime, timeToSim);
    timeToSim -= maxSimTime;
    actTime += maxSimTime;

    _simCycle++;
    _simTime += toIntCeil(maxSimTime * 100); // step 0.01 s 

    if (timeToSim <= 0.0001f)
    {
      lastCycle = true;    
    }


    for(int i = 0; NextGroup(activeGroups, i, actTime, lastCycle); i++)
    {
      // 2.) Detect collision for group. Join groups if needed
      CollisionDetection(activeGroups, i, maxSimTime, staticBodiesIDs, staticBodiesRB); 

      PROFILE_SCOPE_EX(odeQS,*);
      ActiveGroup *  grp = activeGroups[i];   

      // 3.) Simulate inner processes of bodies in group... 
      grp->SimulateInnerProcesses(grp->GetSimStep());

      // 4.) Solve LCP problem, move bodies
      if (grp->GetJoints().Size() >0)
        dxQuickStepper ( _world, grp->GetIRigidBodies(), grp->GetJoints(), grp->GetSimStep());  
      else
        dxStepper(_world,grp->GetIRigidBodies(), grp->GetSimStep());
      
#if _ENABLE_CHEATS
      // diagnostic draw
      if (timeToSim <= 0.0001f)
        grp->DiagDraw(); 
#endif
      // 5.) Run reactors.      
      grp->RunReactors(grp->GetSimStep());             

      // 6.) Disable not moving groups      
      grp->MadeDisable(_simTime);      
    }      
  } while (!lastCycle); 

  // 7.) Reactors only changes the bodies now let bodies react to those changes.
  _freeResource = _maxFreeResource - _blockedResource;
  for(int i = 0; i < activeGroups.Size(); i++)
  {
    PROFILE_SCOPE(odeSP);
    _freeResource -= activeGroups[i]->SimulatePost(); 
  }
  // 7.1.) Also for static bodies

  for(int i = 0; i < staticBodiesRB.Size(); i++)
  {
    PROFILE_SCOPE(odeSP);
    staticBodiesRB[i]->SimulatePost(); 
  }

#if _ENABLE_CHEATS
  if(CHECK_DIAG(DEPhys))
  {  
    float timeSpend = GetSectionTime(startTime);
    BString<256> text;
    sprintf(text, "Have to: %lf \n Spend: %lf \n Percent: %lf", time * 1000,timeSpend * 1000, timeSpend/time * 100 );
    const int nMsg = 20;
    static InitVal<int,-1> handlesMsg[nMsg];
    GlobalShowMessage(100, text, handlesMsg, nMsg );
  };
#endif

  return true;
}

// search for next group for simulation 
bool ODESim::NextGroup(ActiveGroupArray& grps, int &iGrp, float time, bool takeAll)
{
  time += 0.0001f; // numerical barrier
  for(; iGrp < grps.Size(); iGrp++)
  {  
    ActiveGroup& grp = *grps[iGrp];
    if (grp._grp->IsDisabled())
      continue;

    if (grp._grp->IsForcedActualize())
    {
      grp.Actualize();
      // if grp is empty
      if (grp._bodiesIDs.Size() == 0)
      {
        // delete group from hierarchy
        unconst_cast(grp._idGrp.GetParent())->RemoveSubItem(*grp._idGrp.GetPath());
        grps.DeleteAt(iGrp);
        iGrp--;
        continue;
      }
    }    
    if (takeAll)
    {
      grp.SetSimStep(time - grp.GetSimTime());
      return true;
    }

    if (grp.NextTimeOfSimlation() <= time)
      return true;
  }
  return false;
}

void ODESim::DeInit()
{  
  _world = NULL;
 
  g_cdCache.Clear();
}

/** Function is not implemented yet */
float ODESim::GetFreeResource() const
{
  return _freeResource;
};

/** Detects collisions for group. If group collides with other group merge them together. 
  * @param grps - array with active group. It will be change if any groups will be merged.
  * @param iGrp - position of the processed group in grps array.
  */
void ODESim::CollisionDetection(ActiveGroupArray& grps, int &iGrp, float& minSimTime, FindArray<HierarchyID, MemAllocSA>& staticBodiesIDs, RefArray<HierarchyIRigidBody>& staticBodiesRB)
{
  PROFILE_SCOPE_EX(odeCo,*);
  Ref<ActiveGroup> grp = grps[iGrp];

  AUTO_FIND_ARRAY(HierarchyID,touchedGroups, 32)  
  
  // Detect collisions for group. 
  grp->CollisionDetection(touchedGroups, _simCycle);

  if (touchedGroups.Size() == 0)
  {
    grp->ProcessJoints();
    // find sub groups if exists and divide group to them
    grp->DivideIntoSubGroups(grps);
    return;
  }

  touchedGroups.Insert(0, grp->GetID());

  
  // If any it collides with any other group, detect collisions for thouse group too. 
  int n;
  int i = 1;
  do 
  {
    n = touchedGroups.Size();
    for(;i < n; i++)
    {
      HierarchyID& idTG = touchedGroups[i];     
      int iTG = -1;
      // search between found groups
      iTG = grps.FindKey(idTG);             

      ActiveGroup * grpNew;
      if (iTG < 0)
      {
        // create new active group
        iTG = grps.Size();

        grpNew = new ActiveGroup(staticBodiesIDs,staticBodiesRB);
        grps.Add(grpNew);
        grpNew->Create(idTG, grp->GetSimTime(), _world);  
        saturateMin(minSimTime, grpNew->GetSimStep());
      }
      else
      {
        grpNew = grps[iTG];        
        grpNew->_grp->Enable(idTG);
        if (grpNew->_grp->IsForcedActualize())        
          grpNew->Actualize();       
      }

      // collide new group
      grpNew = grps[iTG];
      grpNew->CollisionDetection(touchedGroups,_simCycle);
    }
  } while (touchedGroups.Size() > n);


  // merge groups
  for(int i = 1; i < touchedGroups.Size(); i++)
  {
    // join groups...
    int iToJoin = grps.FindKey(touchedGroups[i]);
    ActiveGroup * grpToJoin = grps[iToJoin];
    grp->JoinWith(*grpToJoin);    
    grps.DeleteAt(iToJoin);
  }

  iGrp = grps.FindKey(grp->GetID());

  grps[iGrp]->ProcessJoints();

  // find sub groups if exists and divide group to them
  grps[iGrp]->DivideIntoSubGroups(grps);

}

void ODESim::UpdateFreeResourceInfo(const FindArray<HierarchyID>& grps)
{
  _freeResource = _maxFreeResource - _blockedResource;
  for(int i = 0; i < grps.Size(); i++)
  {
    if (grps[i])
    {
      RigidBodyGroup * grp = static_cast<RigidBodyGroup *>(grps[i].Get());
      _freeResource -= grp->SimulationCost();
    }
  }
}

//--------------------------------------
//  ActiveGroup
//--------------------------------------

ActiveGroup::ActiveGroup(FindArray<HierarchyID, MemAllocSA>& staticBodiesIDs, RefArray<HierarchyIRigidBody>& staticBodiesRB)
: _staticBodiesIDs(staticBodiesIDs), _staticBodiesRB(staticBodiesRB), _nContactJoints(0), _nPermanentJoints(0) {};

void ActiveGroup::PrepareForDC()
{
  for(int i =0; i < _bodiesRB.Size(); i++)
  {
    _bodiesRB[i]->PrepareForDC();
  }
  for(int i =0; i < _staticBodiesRB.Size(); i++)
  {
    _staticBodiesRB[i]->PrepareForDC();
  }
}

void ActiveGroup::DCDone()
{
  for(int i =0; i < _bodiesRB.Size(); i++)
  {
    _bodiesRB[i]->DCDone();
  }
  for(int i =0; i < _staticBodiesRB.Size(); i++)
  {
    _staticBodiesRB[i]->DCDone();
  }
}

/** Filter used during detection collision in hierarchy */
class FilterColl : public HierarchyIntersectionFilter
{
protected:
  // the first part are bodies from same group that already made collision testing
  AutoArray<HierarchyID>& _bodies; 
  int _n;
  // the second part are bodies that will do not collide from some other reasons (f.e.: they are connected by joint)
#define OTHRERS_SIZE 32
  AUTO_STORAGE(HierarchyID, _othersData, OTHRERS_SIZE);
  StaticArrayAuto<HierarchyID> _others; 

public:
  FilterColl(AutoArray<HierarchyID>& bodies): _bodies(bodies), _n(0), _others(_othersData,sizeof(_othersData)) {};
  void SetN(int n) {_n = n; Assert(n <= _bodies.Size());}; 

  void ClearOthers() {_others.Resize(0);};
  StaticArrayAuto<HierarchyID>& AddOthers() {return _others;};
  
  bool operator()(HierarchyIDVal id) const 
  {
    for(int i = 0; i < _n; i++)
      if (_bodies[i] == id)
        return false; // do not collide with

    for(int i = 0; i < _others.Size(); i++)
      if (_others[i] == id)
        return false; // do not collide with
      
    return true;
  };
};

void LandIntersectionRB(RBCollisionBuffer& ret, const GeomShapeAnimatedBase& shape0, float mass, Matrix4Val trans, const Landscape& land);

/** Detects collisions for group
  * @param touchedGroups - groups colliding with this. If any new will be find it will be added.
  */
int g_numColl = 0; 
void ActiveGroup::CollisionDetection(FindArray<HierarchyID, MemAllocSA>& touchedGroups, int simCycle)
{
  PROFILE_SCOPE(odeCo0);
  PrepareForDC();

  Ref<HierarchyHead> head = GWorld->GetHierarchyHead();

  int nTouchedGroups = touchedGroups.Size();
  FilterColl filter(_bodiesIDs);   

  /// Allocate some space for joints... 2048 should be enough
  _joints.Reserve(2048);

  _nContactJoints = 0;
  _nPermanentJoints = 0;

  int nJoints = 0;

  PROFILE_SCOPE(odeCo1);
  // for each object find collisions
  for(int i = 0; i < _bodiesIDs.Size(); i++)
  {   
    PROFILE_SCOPE(odeCo12);
    _bodiesRB[i]->ClearStoredJoints();
    // Add pernament joints from body
#if 0
    int n = _joints.Size();
    _bodiesRB[i]->GetPermanentJoints(_joints);
    for(; n <  _joints.Size(); n++)
    {
      HierarchyID body0 = static_cast<HierarchyJointRBWrap*>(_joints[n].GetRef())->GetBodyID(0); 
      HierarchyID body1 = static_cast<HierarchyJointRBWrap*>(_joints[n].GetRef())->GetBodyID(1); 

      if (body0)
      {
        HierarchyID idsecGrp = ((HierarchyItem *) body0)->GetRBGroup(body0);
        Assert(idsecGrp == _idGrp);
      }

      if (body1)
      {
        HierarchyID idsecGrp = ((HierarchyItem *) body1)->GetRBGroup(body1);
        Assert(idsecGrp == _idGrp);
      }
    }
#else
    _bodiesRB[i]->GetPermanentJoints(_joints);
#endif
    _nPermanentJoints += _joints.Size() - nJoints;
    nJoints = _joints.Size();

    GeomIntersectionRBContext context;
      
    PROFILE_SCOPE(odeCo3);
    // it can be object or model part       
    HierarchyIRigidBody * bodyRB = _bodiesRB[i];

    if (!bodyRB->CollideWithObjs() && !bodyRB->CollideWithLand())
      continue;

    context.with = bodyRB->CreateGeomShape(); 
    if (!context.with)
      continue; 

    context.withItem =  bodyRB->GetItem();

    context.withMass = bodyRB->GetMass();
    context.withPos = bodyRB->GetTransform(); 

    HierarchyIDVal id = _bodiesIDs[i];

    if (bodyRB->CollideWithObjs())
    {    
      // do not collide with bodies that already have tested collisions 
      context.filter = &filter; 
      filter.SetN(i+1);
      filter.ClearOthers();

      PROFILE_SCOPE(odeCo4); 
      bodyRB->IgnoreCollisionWith(filter.AddOthers());

      context.center = context.withPos * context.with->BoundingCenter();
      context.radius = context.with->BoundingSphere();

      context.minMax[0] = context.with->Min();
      context.minMax[1] = context.with->Max();

      const Vector3 epsilon(0.02f,0.02f,0.02f);
      context.minMax[0] -= epsilon; 
      context.minMax[1] += epsilon; 
      
      context.id = id;

      Assert(id == bodyRB->GetItemID());
      context.simCycle = simCycle;  
    }

#if _ENABLE_CHEATS
    if (CHECK_DIAG(DEBBTree))
    { 
      context.with->DrawDiag(context.withPos);
    }
#endif

    RBCollisionBuffer ret; 
    {
      PROFILE_SCOPE(odeCoH);
      ADD_COUNTER(odeCoH,1);
      g_numColl = 0;
      if (bodyRB->CollideWithObjs())
        head->GeomIntersectionRB(ret, context);

      if (bodyRB->CollideWithLand())
        ::LandIntersectionRB(ret,*context.with, context.withMass, context.withPos, *GLandscape );
    };
    

    PROFILE_SCOPE(odeCo5);
    _joints.Reserve(_joints.Size() + ret.Size());

    // if contact is in different group, the goup will be joined.
    for(int j = 0; j < ret.Size(); j++)
    {    
      RBContactPoint& pt = ret[j];
      if (pt.body2)
      {      
        PROFILE_SCOPE(odeCo7);
        // probably there be in ret a sequence of contact points with the same bodies
        int toJ = j + 1;         
        for(;toJ < ret.Size() && pt.body2 == ret[toJ].body2; toJ++);

        // compare groups
        HierarchyID idsecGrp = ((HierarchyItem *) pt.body2)->GetRBGroup(pt.body2);
        if ((bool)idsecGrp)
        {
          if (!(idsecGrp == _idGrp))
          {
            int ii = touchedGroups.Find(idsecGrp);
            if (ii < 0)
            {
              // new one
              touchedGroups.Add(idsecGrp);
            }
            else if (ii < nTouchedGroups)
            {
              continue; // such contact was already found
            }
          }
        }
        else
        {

          // maybe start simulation will create a new group for him
          HierarchyItem * item = pt.body2.Get();
          if (item->IsIRigidBody(pt.body2))
          {
            if (!item->Static())
            {            
              pt.body2.MakeIndividual();
              item = pt.body2.Get();

              item->StartSimulation(pt.body2);
              idsecGrp = item->GetRBGroup(pt.body2);
              if (idsecGrp)
              { 
                Assert(touchedGroups.Find(idsecGrp) < 0);
                touchedGroups.Add(idsecGrp);
              }             
            }            
          }
          else
            pt.body2 = HierarchyID();
        }
        
        PROFILE_SCOPE(odeCo8);
        if (pt.body2)
        {
          // body 2 was not changed        
          for(; j < toJ; j++)
          {
            RBContactPoint& ptAct = ret[j];
            ptAct.body1 = id;
           
            PROFILE_SCOPE(odeCo6);
            // create contact joint                                 
            _joints.AddFast(new ContactJointRB(_world, ptAct));       
          }
        }
        else
        {
          // body 2 was changed to HierarchyID()
          for(; j < toJ; j++)
          {
            RBContactPoint& ptAct = ret[j];
            ptAct.body1 = id;
            ptAct.body2 = HierarchyID();
            
            PROFILE_SCOPE(odeCo6);
            // create contact joint                  
            _joints.AddFast(new ContactJointRB(_world, ptAct));       
          }
        }
        continue;
      }

      pt.body1 = id;      
      PROFILE_SCOPE(odeCo6);
      // create contact joint                
      _joints.AddFast(new ContactJointRB(_world, pt));        
    }  

    ADD_COUNTER(odeCJ,_joints.Size() - nJoints);
    _nContactJoints += _joints.Size() - nJoints;
    nJoints = _joints.Size();
  } 

  DCDone();
}

/** Merges groups. 
  * @param src - bodies from group will be added to this one. Its source RigidBodyGroup will be deleted then. 
  */
void ActiveGroup::JoinWith(ActiveGroup& src)
{
  Assert(this != &src);
  _idGrp.MakeIndividual(); // must be individual
  _grp = static_cast<RigidBodyGroup *>((HierarchyItem *)_idGrp); 

  int n = _bodiesIDs.Size() + src._bodiesIDs.Size();
  _bodiesIDs.Realloc(n);

  for(int i = 0; i < src._bodiesIDs.Size(); i++)
  {
    // add bodies
    HierarchyID& id = src._bodiesIDs[i];
    id.MakeIndividual();

    Assert(id.GetParent() == _idGrp.GetParent());

    HierarchyItem * obj = id.Get();
    obj->SetRBGroup(*id.GetParent(), id.GetPath(), *_idGrp.GetPath());

    Assert(_idGrp.GetParent() == id.GetParent());
    _grp->AddObjectInGroup(*_idGrp.GetParent(), _idGrp.GetPath(), *id.GetPath());
    _bodiesIDs.AddFast(id);

    // if object was individualized,update IRigidBody
    src._bodiesRB[i]->Actualize();   
  }

  // set maxSimTime
  saturateMin(_maxTimeStep, src._maxTimeStep);
  _timeStep = _maxTimeStep;

  // add joints
  _joints.Append(src._joints);
  src._joints.Clear();

  _nPermanentJoints += src._nPermanentJoints;
  _nContactJoints += src._nContactJoints;

  // add IRigidBodise
  _bodiesRB.Append(src._bodiesRB);
  src._bodiesRB.Clear();

  // remove unused group
  src._idGrp.MakeIndividual();
  static_cast<RigidBodyGroup *>((HierarchyItem *)src._idGrp)->ClearObjects(src._idGrp);
  unconst_cast(src._idGrp.GetParent())->RemoveSubItem(*src._idGrp.GetPath());
}
/** We can interpret group like a graph where nodes are bodies and edges are joints. 
  * Ideal group corresponds to connected graph. But as bodies moves it sometimes creates disconnected graph 
  * (connected subgraphs). Such groups should be divided into subgroups. It is only matter of optimalization. If 
  * one body in group is enabled whole group must be enabled. And ideal groups prevents simulation of bodies 
  * where it is not needed.
  * To find connected subgraphs is probably expensive operation. On other side it can be done once  for several frames.
  * 
  * The following function do not search for a connected subgraphs. It only searches for disconnected single bodies. 
  * In lot of applications it is sufficient.
  */
void ActiveGroup::DivideIntoSubGroups(ActiveGroupArray& activeGroups)
{  
  if (_bodiesRB.Size() < 2)
    return;

  // now does not searches for sub groups only for isolated bodies.
  for(int i = 0; i < _bodiesRB.Size(); i++)
  {
    const RefArray<JointRB>& joints = _bodiesRB[i]->GetStoredJoints();

    bool isIsolated = (joints.Size() == 0);
    if (!isIsolated)
    {
      // maybe the joints are only with static bodies
      int j = 0;
      for(; j < joints.Size(); j++)
      {
       IRigidBody * body = joints[j]->GetIRigidBody(1);
        if (body && !body->IsStatic())
          break;
      }

      isIsolated = (j == joints.Size());
    }

    if (isIsolated)
    {
      // isolated body create a separate group for him      

      // where to create new group? let's suppose that parent of this group is multiplacer
      MultiPlacerIdentity * placer = dyn_cast<MultiPlacerIdentity>(_idGrp.GetSubObject(_idGrp.Length() - 1));

      if (!placer)
        continue;

      HierarchyIDVal objID = _bodiesIDs[i];
      Assert(objID.GetParent() == _idGrp.GetParent());

      // create new group in hierarchy
      Ref<RigidBodyGroup> grp = new RigidBodyGroup();
      HierarchyPathRef grpPathEnd = placer->AddSubObject(grp);
      HierarchyPathRef grpPath = _idGrp.GetPath()->CloneDeep(_idGrp.Length() - 1);
      if (grpPath)
        grpPath->Append(grpPathEnd);
      else
        grpPath = grpPathEnd; 

      grp->Individual();
      grp->Enable(HierarchyID(_idGrp.GetParent(), grpPath.GetRef()));

      _grp->RemoveObjectFromGroup(*objID.GetParent(), _idGrp.GetPath(), *objID.GetPath());
      grp->AddObjectInGroup(*objID.GetParent(), grpPath.GetRef(), *objID.GetPath());

      HierarchyItem * item = _bodiesIDs[i].Get();
      item->SetRBGroup(*objID.GetParent(), objID.GetPath(), *grpPath);

      // create new ActiveGroups
      HierarchyID newGrpID(objID.GetParent(),grpPath.GetRef());

      Ref<ActiveGroup> newGrp = new ActiveGroup(_staticBodiesIDs, _staticBodiesRB);
      activeGroups.Add(newGrp);
      newGrp->Create(newGrpID, _simTime, _world);

      // remove joints
      for(int j = 0; j < joints.Size(); j++)
      {
        if (dyn_cast<ContactJointRB>(joints[j].GetRef()))
        {
          newGrp->_nContactJoints++;
          _nContactJoints--;
        }
        else
        {
          newGrp->_nPermanentJoints++;
          _nPermanentJoints--;
        }

        int ii = _joints.Find(joints[j]);
        Assert(ii >= 0);
        _joints.DeleteAt(ii);
      }

      // remove body from this group
      _bodiesIDs.DeleteAt(i);
      _bodiesRB.DeleteAt(i);

      i--;
    }
  }
}

/** Initialization of groups.
  * @param id - id of source group in hierarchy
  * @param timeToSim - amount of time, that must be simulated
  * @param world = global phys configuration
  */
void ActiveGroup::Create(HierarchyIDVal id, float actTime, dxWorld * world)
{
  _world = world;
  _simTime = actTime; 
  _idGrp = id;  
  _grp = static_cast<RigidBodyGroup *>((HierarchyItem *)id); 

  _nContactJoints = 0;
  _nPermanentJoints = 0;

  Actualize();  
}

/** Reload group from source group in hierarchy.*/
void ActiveGroup::Actualize()
{
  _bodiesIDs.Resize(0);
  _bodiesRB.Resize(0);

  if (_idGrp)
  {  
    _grp = static_cast<RigidBodyGroup *>((HierarchyItem *)_idGrp); 

    // get bodies from group
    _bodiesIDs.Resize(0);
    _grp->GetObjects(_idGrp, _bodiesIDs);      

    _bodiesRB.Resize(0);
    _bodiesRB.Realloc(_bodiesIDs.Size());
    
    _maxTimeStep = FLT_MAX;
    for(int j = 0; j < _bodiesIDs.Size(); j++)
    {

      HierarchyItem * item = _bodiesIDs[j].Get();
      if (item)
      {      
        Ref<HierarchyIRigidBody> rb = item->GetIRigidBody( _bodiesIDs[j]);        
        // register all ids it will be easier than to search in them
        _bodiesIDs[j].Register();

        // the rb must be always nonzero... but somehow it happens and produce crash so rather solve it
        if (!rb)
        {
          Assert(rb);
          RptF("Body with no IRigidBody interface in ODESim.");
          _bodiesIDs.Delete(j);
          j--;
          continue;
        }
        
        _bodiesRB.AddFast(rb);
        saturateMin(_maxTimeStep, rb->GetMaxSimTime());
        Assert(!rb->IsStatic());
      }
      else
      {
        _bodiesIDs.Delete(j);
        j--;
      }
    }
    _timeStep = _maxTimeStep;
  }
  for(int i = 0; i < _myStaticBodiesRB.Size(); i++)
  {
    _myStaticBodiesRB[i]->Actualize();
  }
  _grp->SetForceActualize(false);
}

void ActiveGroup::ClearStoredJoints()
{
  _joints.Resize(0);
}

void ActiveGroup::ProcessJoints()
{  
  _myStaticBodiesRB.Resize(0);
  // 1.) Set IRigidBodies into corresponding items in hierarchy, it makes step 2.) much more faster
  for(int i =0; i < _bodiesRB.Size(); i++)
  {
    _bodiesRB[i]->PrepareForProcessJoints();
  }
  for(int i =0; i < _staticBodiesRB.Size(); i++)
  {
    _staticBodiesRB[i]->PrepareForProcessJoints();
  }

  // 2.) Set IRigidBodies
  for(int i = 0; i < _joints.Size(); i++)
  {    
    // Optimalization: Set IRigidBodies form groups to joint. (Joint will not get IRigidBody interfaces from bodies.) 
    JointRB * jnt = _joints[i];
    static_cast<JointWithIRigidBody *>(jnt)->SetIRigidBodies(*this);   
  } 

  // 3.) Clean temporary storages in items
  for(int i =0; i < _bodiesRB.Size(); i++)
  {
    _bodiesRB[i]->ProcessJointsDone();
  }
  for(int i =0; i < _staticBodiesRB.Size(); i++)
  {
    _staticBodiesRB[i]->ProcessJointsDone();
  }
}

bool ActiveGroup::MadeDisable(unsigned int time)
{
  // If none of bodies is moving disable the group.
  bool disable = true;

  for(int j = 0; j < _bodiesRB.Size(); j++)
    disable = _bodiesRB[j]->CanBeStopped(time) && disable;      


  if (disable)
  {
    // disable group    
    _idGrp.MakeIndividual();
    _grp = static_cast<RigidBodyGroup *>((HierarchyItem *)_idGrp);
    _grp->Disable(_idGrp);
    return true;  
  }

  return false; 
}

HierarchyIRigidBody * ActiveGroup::GetIRigidBody(HierarchyIDVal id)
{
  // if it is individual maybe it has IRigidBody in temp
  HierarchyItem * item = id.Get();
  Assert(item);
  if (item->IsIndividual() && item->Temp())
  {  
    HierarchyIRigidBody * ret = static_cast<HierarchyIRigidBody *>(item->Temp());
    if (ret->IsStatic())      
    {
      _myStaticBodiesRB.AddUnique(ret); // is between my? 
    }    
    return ret;
  }

  int i = _bodiesIDs.Find(id);
  
  if (i >= 0)
    return _bodiesRB[i]; 
  else
  {
     // interaction with static body
    Assert(id.Get()->IsIRigidBody(id) && id.Get()->Static());
    
    i = _staticBodiesIDs.Find(id);
    if (i >= 0)
    {
      _myStaticBodiesRB.AddUnique(_staticBodiesRB[i]); // is between my? 
      return _staticBodiesRB[i];
    }
    else
    {
      i = _staticBodiesIDs.Add(id);
      _staticBodiesIDs[i].Register(); // must be registered
      _staticBodiesRB.Add(id.Get()->GetIRigidBody(id));
      Assert(_staticBodiesRB[i]);
      _myStaticBodiesRB.Add(_staticBodiesRB[i]);
      return _staticBodiesRB[i];
    }
  }
}

void ActiveGroup::RunReactors(float deltaT)
{ 
  // Check if contacts in group are impulse like
  bool isImpulse = false;
  for(int i = 0; i < _joints.Size(); i++)
  {        
    if (_joints[i]->IsImpulse())
    {
      isImpulse = true;
      break;
    }
  }

  // Inform all bodies that the contact are impluse-like. Such information will be used by reactors.

  for(int i = 0; i < _bodiesRB.Size(); i++)  
  {      
    _bodiesRB[i]->RunReactors(deltaT, isImpulse);
  }

  // run reactors also for attached static bodies
  for(int i = 0; i < _myStaticBodiesRB.Size(); i++)  
  {
    _myStaticBodiesRB[i]->RunReactors(deltaT, isImpulse);
  }
 
  // run also reactors attached to joints
  for(int i = 0; i < _joints.Size();i++)
    _joints[i]->RunReactors(deltaT, isImpulse);

 ClearStoredJoints(); // joints are not more needed
}

float ActiveGroup::SimulatePost()
{ 
  if (_grp->IsForcedActualize())
  {
    Actualize();
    if (_bodiesRB.Size() == 0)
    {
      // delete group from hierarchy
      unconst_cast(_idGrp.GetParent())->RemoveSubItem(*_idGrp.GetPath());
      return 0;
    }
  }
  
  float cost = CalculatePhysSimulationCost(_maxTimeStep, _bodiesRB.Size(), _nContactJoints, _nPermanentJoints);
  if (_grp->IsIndividual())
    _grp->SetSimulationCost(cost);

  for(int i =0; i < _bodiesRB.Size(); i++)
  {
    _bodiesRB[i]->SimulatePost();
  } 


  return cost;
}

void ActiveGroup::SimulateInnerProcesses(float deltaT)
{
  _simTime += deltaT;

  for(int i =0; i < _bodiesRB.Size(); i++)
  {
    _bodiesRB[i]->SimulateInnerProcesses(deltaT);
  }   
  for(int i = 0; i < _myStaticBodiesRB.Size(); i++)  
  {
    _myStaticBodiesRB[i]->SimulateInnerProcesses(deltaT);
  }
}

#if _ENABLE_CHEATS
void ActiveGroup::DiagDraw()
{
  for(int i = 0; i < _joints.Size();i++)
    _joints[i]->DiagDraw();
}
#endif



//--------------------------------------
// DamageReactor
//--------------------------------------

void DamageReactor::operator()(RefArray<JointRB>& joints, bool impulseJoints, HierarchyIRigidBody *  bodyRB, float deltaT) const
{
  if (joints.Size() == 0)
    return;  

  float forceSize = 0;   
  Vector3 avgPos = VZero;

  // calculate sum of force in all joints 
  if (impulseJoints)
  {
    float deltaTSq = deltaT * deltaT;
    for(int i = 0; i < joints.Size(); i++)
    {
      avgPos += (joints[i]->GetPos() - avgPos) / (i+1); // numerically stable avgPoint 
      forceSize += joints[i]->GetForceInJoint().SquareSize() * deltaTSq;             
    }

    // transfer to model coord 
    avgPos = bodyRB->GetInvTransform() * avgPos ;

    // apply dammage if needed  
    HierarchyItemWithShape::DoDamageResult result;
    HierarchyItemWithShape *body = static_cast<HierarchyItemWithShape *>(bodyRB->GetItem());
    // TODO: who is owner? probably body with higher impulse in joint... For now lets say its self damage.
    body->SimulateImpulseDamage(bodyRB->GetItemID(),result, dyn_cast<EntityAI>(body), forceSize * InvSqrt(forceSize), avgPos);
    if (result.damage > 0)
    {
      if (!body->IsIndividual())
      {
        // make individual
        bodyRB->MakeIndividual();
        body = static_cast<HierarchyItemWithShape *>(bodyRB->GetItem());
      }

      body->ApplyDoDamage(bodyRB->GetItemID(), result, dyn_cast<EntityAI>(body), RString());
    }       
  }
  else
  {   
    // Currently nobody is using force dammage but code is prepared here.
    /*for(int i = 0; i < joints.Size(); i++)
    {
      avgPos += (joints[i]->GetPos() - avgPos) / (i+1);
      forceSize += joints[i]->GetForceInJoint().SquareSize();            
    }

    // transfer to model coord 
    avgPos =bodyRB->GetInvTransform() * avgPos;

    HierarchyItemWithShape::DoDamageResult result;
    HierarchyItemWithShape *body = static_cast<HierarchyItemWithShape *>(bodyRB->GetItem());
    body->SimulateForceDamage(bodyRB->GetItemID(),result, dyn_cast<EntityAI>(body), forceSize * InvSqrt(forceSize), avgPos);
    if (result.damage > 0)
    {
      if (!body->IsIndividual())
      {
        // make individual
        bodyRB->MakeIndividual();
        body = static_cast<HierarchyItemWithShape *>(bodyRB->GetItem());
      }

      body->ApplyDoDamage(bodyRB->GetItemID(), result, NULL, RString());
    }*/
  }
}
#endif //_ENABLE_RIGID_BODIES 
//--------------------------------------
//  OBBNodeS
//--------------------------------------
#if _ENABLE_CHEATS 
#include <El/Color/colors.hpp>
void DiagOBBox(const Vector3 *vec, Vector3Val center,const Frame &pos, PackedColor color);

void OBBNodeS::DrawDiag(Color color1,Color color2, float t, float step, const Frame &pos) const
{  
  Vector3 vec[3];
  vec[0] = _u * _hu;
  vec[1] = _v * _hv;
  vec[2] =  _w * _hw;

  Color color = color1 * t + color2 * (1-t);   

  t += step;  

  if (_son) 
  {    
    _son->DrawDiag(color1, color2, t, step, pos);  

    if (_son->Brother())   
    {      
      _son->Brother()->DrawDiag(color1, color2, t, step, pos);
    }
  }

  DiagOBBox(vec,_obbC,pos,PackedColor(color));
}
#endif
