/*************************************************************************
 *                                                                       *
 * Open Dynamics Engine, Copyright (C) 2001,2002 Russell L. Smith.       *
 * All rights reserved.  Email: russ@q12.org   Web: www.q12.org          *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of EITHER:                                  *
 *   (1) The GNU Lesser General Public License as published by the Free  *
 *       Software Foundation; either version 2.1 of the License, or (at  *
 *       your option) any later version. The text of the GNU Lesser      *
 *       General Public License is included with this library in the     *
 *       file LICENSE.TXT.                                               *
 *   (2) The BSD-style license that is included with this library in     *
 *       the file LICENSE-BSD.TXT.                                       *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files    *
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.                     *
 *                                                                       *
 *************************************************************************/

#ifndef _ODE_ODE_H_
#define _ODE_ODE_H_

#include <PhysicalLibs/RigidBody/jointrb.hpp>

typedef float dReal;
typedef dReal dVector3[4];
typedef dReal dMatrix3[4*3];

#define dInfinity FLT_MAX


void dxQuickStepper (dxWorld *world, const RefArray<HierarchyIRigidBody>& bodies,
                     const RefArray<JointRB>& _joint,  dReal stepsize);

void dxStepper (dxWorld *world, const RefArray<HierarchyIRigidBody>& bodies,
                dReal stepsize);


// quick-step parameters
struct dxQuickStepParameters {
  int num_iterations;		// number of SOR iterations to perform
  dReal w;			// the SOR over-relaxation parameter
};


// contact generation parameters
struct dxContactParameters {
  dReal max_vel;		// maximum correcting velocity
  dReal min_depth;		// thickness of 'surface layer'
};

struct dxWorld  { 
  Vector3 gravity;		// gravity vector (m/s/s)
  dReal global_erp;		// global error reduction parameter
  dReal global_cfm;		// global costraint force mixing parameter  
  dxQuickStepParameters qs;
  dxContactParameters contactp;
};

dxWorld * dWorldCreate();
void dWorldSetGravity (dxWorld *, dReal x, dReal y, dReal z);

#endif
