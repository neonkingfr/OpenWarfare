/*************************************************************************
 *                                                                       *
 * Open Dynamics Engine, Copyright (C) 2001,2002 Russell L. Smith.       *
 * All rights reserved.  Email: russ@q12.org   Web: www.q12.org          *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of EITHER:                                  *
 *   (1) The GNU Lesser General Public License as published by the Free  *
 *       Software Foundation; either version 2.1 of the License, or (at  *
 *       your option) any later version. The text of the GNU Lesser      *
 *       General Public License is included with this library in the     *
 *       file LICENSE.TXT.                                               *
 *   (2) The BSD-style license that is included with this library in     *
 *       the file LICENSE-BSD.TXT.                                       *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files    *
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.                     *
 *                                                                       *
 *************************************************************************/

#include <Poseidon/lib/wpch.hpp>
#if _ENABLE_RIGID_BODIES

#include "include/ode.h"
#include "util.h"



void dxStepBody (IRigidBody *body, dReal h,dReal * work)
{
  // handle linear velocity

  
  if (body->IsMoving() < 1)    
  {     
    body->SetVelocity(VZero);
    body->SetAngularVelocity(VZero);
    //body->SetAngularMomentum(VZero);

    return;
  }

  
  Vector3 massCenter = body->GetMassCenter() + h * body->GetVelocity(); 

  float anglediffSq = body->GetAngularVelocity().SquareSize() * h * h * 0.25f;
  /// for big angle changes use better precision in rotation
  if (anglediffSq < 0.01f)
  {  
    QuaternionRB orient = body->GetOrientation();
    QuaternionRB diff = QuaternionRB(body->GetAngularVelocity()) * orient * 0.5f * h;
    orient += diff;
    orient.Normalize();
    body->SetOrientationMassCenter(massCenter, orient); 
  }
  else
  {
    // create rotation quaternion from angular velocity
   float angVelSize = body->GetAngularVelocity().Size();
   float theta = angVelSize * h;
   QuaternionRB rot(theta, body->GetAngularVelocity() / angVelSize);
   QuaternionRB orient = rot * body->GetOrientation();
   orient.Normalize();
   body->SetOrientationMassCenter(massCenter,orient); 
  }

  if (work)
  {
    *work += h * body->GetVelocity() * body->GetForceBank();      
    *work += body->GetAngularVelocity()* h * body->GetTorqueBank();
  }

  body->NotifyAboutMove();
}


void dSetZero (dReal *a, int n)
{
  Assert (a && n >= 0);
  while (n > 0) {
    *(a++) = 0;
    n--;
  }
}


void dSetValue (dReal *a, int n, dReal value)
{
  Assert (a && n >= 0);
  while (n > 0) {
    *(a++) = value;
    n--;
  }
}

#endif