#include <Poseidon/lib/wpch.hpp>
#if _ENABLE_RIGID_BODIES
#include <El/Common/perfProf.hpp>

#include "../include/ode.h"

void dWorldSetGravity (dxWorld * w, dReal x, dReal y, dReal z)
{
  Assert (w);
  w->gravity[0] = x;
  w->gravity[1] = y;
  w->gravity[2] = z;
}

dxWorld * dWorldCreate()
{
  dxWorld *w = new dxWorld;
  w->gravity = VZero;

  w->global_cfm = 1e-5f;  
  w->global_erp = 0.01f;

  w->qs.num_iterations = 20;
  w->qs.w = 1.3f;

  w->contactp.max_vel = dInfinity;
  w->contactp.min_depth = 0;

  return w;
}

#endif