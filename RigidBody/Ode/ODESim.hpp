#ifdef _MSC_VER
#pragma once
#endif

#ifndef __ODESIM_HPP__
#define __ODESIM_HPP__

#include "include/ode.h"
#include <PhysicalLibs/RigidBody/TriggerRB.hpp>
#include <PhysicalLibs/RigidBody/JointRB.hpp>

//--------------------------------------
//  RigidBodyGroup
//--------------------------------------
/** Group is set of referencies to the rigid bodies. It is a part of hierarchy.
  * It can be active (simulated/enabled) or disabled. If it is active it MUST be individual. */
class RigidBodyGroup : public ItemGroup
{
  typedef ItemGroup base;
protected:
  bool _disabled; //< if true group is not simulated
  bool _forceActualize; //< group is probably currently simulated in physical engine. Actualize its engine ActualGroupCopy

  float _simulationCost; //< cost of simulation for group, if -1, group cost is unknown so make a guess

public:  
  RigidBodyGroup() : _disabled(true), _forceActualize(false), _simulationCost(-1) {};

  //! Called during remove from hierarchy
  void OnRemove(HierarchyIDVal scope); 
  //! Creates the object clone. It is using AfterClone function to copy the date. 
  HierarchyItem * Clone() const; 
  void AfterClone(HierarchyItem * src) const;  

  void RemoveObjectFromGroup(const HierarchyItem& parent, ConstHierarchyPathRef myPath, HierarchyPathVal objPath);

  bool IsDisabled() const {return _disabled;}; 
  bool IsEnabled() const {return !_disabled;}; 

  void Disable(HierarchyIDVal id);
  void Enable(HierarchyIDVal id);

  /** If this flag is set it means, that group or one of its bodies was modified in hieararchy (maybe individualized). 
    * If group is currently simulated by physics its temporary representative in physics will be updated */
  void SetForceActualize(bool act) {_forceActualize = act;};
  bool IsForcedActualize() const {return _forceActualize;};

  void SetSimulationCost(float cost) {_simulationCost = cost;};
  float SimulationCost() const;

  USE_CASTING(base);
};

//--------------------------------------
//  GetCost
//--------------------------------------
//! Calculates estimation of simulation cost for group or body. 
float CalculatePhysSimulationCost(float maxSimStep, float nbodies, float ncontactjoints, float permanentjoints);

//--------------------------------------
//  ActiveGroup
//--------------------------------------
class ActiveGroup;
class ODESim;

template<class TypeWithID>
struct IDFindArrayKey
{
  typedef const HierarchyID &KeyType;
  //! check if two keys are equal
  static bool IsEqual(KeyType a, KeyType b) {return a==b;}
  //! get a key from an item
  static KeyType GetKey(const TypeWithID &a) {return a->GetID();}
};

typedef FindArrayKey<Ref<ActiveGroup>, IDFindArrayKey< Ref<ActiveGroup> > > ActiveGroupArray;

/** ActiveGroup is temporary representation of RigidBodyGroup in physical library. It exists only during one simulation frame.
  * If RigidBodyGroup could be changed, call Actualize() to refresh the data in ActiveGroup.
  */

class ActiveGroup : public IDtoIRigidBody, public RefCount
{
protected:
  dxWorld * _world;
  float _simTime;  //< rest of simulation time for group
  float _maxTimeStep; //< max time step allowed for group
  float _timeStep; //< current timeStep
  RigidBodyGroup * _grp; //< source RigidBodyGroup
  HierarchyID _idGrp; //< id of source RigidBodyGroup
  RefArray<JointRB> _joints; //< joints detected in group
  int _nPermanentJoints; //< Number of permanent joints. It is used for calculation of simulation cost
  int _nContactJoints; //< Number of contact joints. It is used for calculation of simulation cost
  FindArray<HierarchyID> _bodiesIDs; //< ids of bodies in group
  RefArray<HierarchyIRigidBody> _bodiesRB; //< IrigidBody interfaces of bodies in group
  
  /* Static bodies are not part of the group. On the other hand static bodies can be connected by joints with bodies in this group.
     That's why for joints in this group we need IRigidBody interfaces of static bodies. Further static bodies can have reactors (like 
     destroy on impulse). The reactors will be called at the same time like reactors for group bodies. */      

  FindArray<HierarchyID, MemAllocSA>& _staticBodiesIDs; //< ids of static bodies shared between all groups
  RefArray<HierarchyIRigidBody>& _staticBodiesRB; //< IrigidBody interfaces of static bodies shared between all groups

  RefArray<HierarchyIRigidBody> _myStaticBodiesRB; //< IrigidBody interfaces of static bodies. static bodies are not part of the group. Its bodies with which group members are connected by joints


  //! Get IRigidBody from _bodiesRB array by id.
  HierarchyIRigidBody * GetIRigidBody(HierarchyIDVal id);
 
  //! Refresh data in group according to current status of source RigidBodyGroup
  void Actualize();

  //! Prepare RigidBodies for detection collision
  void PrepareForDC();
  //! Detection collision finished
  void DCDone();


public:
  ActiveGroup(FindArray<HierarchyID, MemAllocSA>& staticBodiesIDs, RefArray<HierarchyIRigidBody>& staticBodiesRB);
  ~ActiveGroup() {ClearStoredJoints();};

  //! Initialization.
  void Create(HierarchyIDVal id, float actTime,dxWorld * world); 

  //! Removes all joints from group (pernament joints will stays in bodies).
  void ClearStoredJoints();

  //! Shares IRigidBody interfaces with joints
  void ProcessJoints();

  //! Performs detection collision.
  void CollisionDetection(FindArray<HierarchyID, MemAllocSA>& touchedGroups, int simCycle);

  //! Appends bodies from the source group and deletes the source group from hierarchy
  void JoinWith(ActiveGroup& src);

  //! If group consists of sub groups it will be divided to them. 
  void DivideIntoSubGroups(ActiveGroupArray& src);
  
  //! Runs reactors for all bodies in group.
  void RunReactors(float deltaT);

  //! Calls SimulatePost for all bodies in group. Returns cost of simulation
  float SimulatePost(); 

  //! Calls SimulatePost for all bodies in group.
  void SimulateInnerProcesses(float deltaT);

  const RefArray<HierarchyIRigidBody>& GetIRigidBodies() const {return _bodiesRB;}
  const RefArray<JointRB>& GetJoints() const {return _joints; };
  HierarchyIDVal GetID() const {return _idGrp;};  

  float NextTimeOfSimlation() const {return _simTime + _timeStep ;};
  float GetSimStep() const {return _timeStep;};
  void SetSimStep(float step) {_timeStep = step;};  
  float GetSimTime() const {return _simTime;};

  //! Checks conditions to disable group are valid and if yes it disables group. 
  bool MadeDisable(unsigned int time);
  
  float SimulationCost() const {return _grp->SimulationCost();};
  
#if _ENABLE_CHEATS
  //! Draws diagnostic informations. 
  void DiagDraw();
#endif

  friend class ODESim;
};

//--------------------------------------
//  ODESim
//--------------------------------------

//! Main class providing rigid body simulation. 
class ODESim
{
protected:
  SRef<dxWorld> _world; //< configuration of the library
  int _simCycle; //< number of sim cycles done during library lifetime
  unsigned int _simTime; //< amount of time simulated during library lifetime in 0.01s
  float _freeResource; //< resources available to be used
  float _blockedResource; //< reserved resources 
  const float _maxFreeResource; //< maximum resources that can be used 

  //! Detects collisions. 
  void CollisionDetection(ActiveGroupArray& _grps, int &_iGrp, float & minSimTime, FindArray<HierarchyID, MemAllocSA>& staticBodiesIDs, RefArray<HierarchyIRigidBody>& staticBodiesRB);
  //! Searches for next group for simulation 
  bool NextGroup(ActiveGroupArray& grps, int &iGrp, float actTime, bool takeAll);
public:
  ODESim();
  ~ODESim() {DeInit();};

  //! Initialize library
  bool Init();  

  //! De initialize library (release all data).
  void DeInit();

  //! Performs simulation step. 
  bool Simulate(const FindArray<HierarchyID>& grps, float time);

  //! Returns number of bodies that can be simulated around position. The performance of library limits number of simulated bodies at once
  float GetFreeResource() const;

  void BlockResource(float block) {_blockedResource += block; _freeResource -= block;};
  void UnblockResource(float block) {_blockedResource -= block;};

  // Recalculate info about amount of free resources. The result is not very exact. If it is not really needed wait next frame till phys will calc exact values
  void UpdateFreeResourceInfo(const FindArray<HierarchyID>& grps);  
};


//--------------------------------------
//  DamageReactor
//--------------------------------------
//! Example of reactors. 
class DamageReactor : public BodyReactor
{
protected:
  void operator()(RefArray<JointRB>& joints, bool impulseJoints, HierarchyIRigidBody * bodyRB, float deltaT) const;

  //! Object does not contains data. Do not clone it. 
  HierarchyItem * Clone() const {return const_cast<DamageReactor *>(this);};
};

#endif //__ODESIM_HPP__


