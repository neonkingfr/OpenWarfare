#ifdef _MSC_VER
#pragma once
#endif

#ifndef __RIGIDBODY_H__
#define __RIGIDBODY_H__

//#include <Es/essencepch.hpp>
#include <Es/Containers/array.hpp>
#include <El/Math/math3d.hpp>
#include <Poseidon/lib/cast.hpp>

//#include <El/ParamArchive/paramArchive.hpp>
//#include <El/bb/boundingbox.hpp>
//#include "BaseGeometry.h"

#include "Quaternion.h"

#define NUMERICAL_ERROR 0.00001f

//--------------------------------------
//  JointRB
//--------------------------------------

struct Info1 
{
  int m;
};

// info returned by getInfo2 function

struct Info2 
{
  // integrator parameters: frames per second (1/stepsize), default error
  // reduction parameter (0..1).
  float fps,erp,cfmCoef;

  // for the first and second body, pointers to two (linear and angular)
  // n*3 jacobian sub matrices, stored by rows. these matrices will have
  // been initialized to 0 on entry. if the second body is zero then the
  // J2xx pointers may be 0.
  float *J1l,*J1a,*J2l,*J2a;

  // elements to jump from one row to the next in J's
  int rowskip;

  // right hand sides of the equation J*v = c + cfm * lambda. cfm is the
  // "constraint force mixing" vector. c is set to zero on entry, cfm is
  // set to a constant value (typically very small or zero) value on entry.
  float *c,*cfm;

  // lo and hi limits for variables (set to -/+ infinity on entry).
  float *lo,*hi;

  // findex vector for variables. see the LCP solver interface for a
  // description of what this does. this is set to -1 on entry.
  // note that the returned indexes are relative to the first index of
  // the constraint.
  int *findex;
};
class IRigidBody;
class JointRB : public RefCount
{
public:  
  virtual ~JointRB() {};

  //virtual void Create(HierarchyIDVal body1, HierarchyIDVal body2) = 0; 

  virtual void GetInfo1 (Info1 *info) = 0;
  virtual void GetInfo2 (Info2 *info) = 0;

  /// Is bounce contact ? 
  virtual bool IsImpulse() const = 0;

  virtual Ref<IRigidBody> GetIRigidBody(int i) const = 0;
  /// Returns true if IRigidBody represents the same body 
  virtual bool HasIRigidBody(const IRigidBody& body, int i) const = 0;  

  virtual void SetResult(float * lambda) = 0; 

  virtual Vector3 GetPos() const = 0;
  virtual Vector3 GetForceInJoint() const = 0;  // from body 0 to body 1
  virtual Vector3 GetTorqueOnBody(int i) const = 0;  

  /// If true, bodies connected by this joint will be not tested for collision detection
  virtual bool BodiesNotCollide() const {return false;};

  /** Function is called after simulation and allows the joint to react on force and impulses. 
    * The typical example will be breakable joints, that will be breaking here.
    */
  virtual void RunReactors(float  deltaT, bool impulseJoints) {};
 
#if _ENABLE_CHEATS
  virtual void DiagDraw() = 0;
#endif

  USE_CASTING_ROOT;

};

TypeIsSimple(JointRB *); 


//--------------------------------------
//  IRigidBodyState
//--------------------------------------

typedef enum {
  TB_TRUE = 1,
  TB_NONE = 0,
  TB_FALSE = -1
} TRIBOOL;

class IRigidBodyState : public RefCount
{
public:
  IRigidBodyState() {};	
  virtual ~IRigidBodyState(){};  

  /*-------------------
  Get methods.
  -------------------*/
  virtual float GetInvMass() const = 0;
  virtual float GetMass() const { return GetInvMass() == 0 ? 0 : 1.0f/ GetInvMass();};

  virtual Matrix3 GetInvInertiaTensor() const  = 0;
  virtual Matrix3 GetInertiaTensor() const  = 0;

  virtual Matrix3 GetActualInvInertiaTensor() const  = 0;
  virtual Matrix3 GetActualInertiaTensor() const  = 0;

  virtual Vector3 GetPos() const  = 0;
  virtual QuaternionRB GetOrientation() const = 0;
  virtual Vector3 GetMassCenter() const = 0;
  virtual Matrix4 GetTransform() const = 0;
  virtual Matrix4 GetInvTransform() const = 0;


  //virtual const Vector3& GetMomentum() const  = 0;
  virtual Vector3 GetVelocity() const  = 0;  
protected:
  Vector3 GetAngularMomentum() const {return GetActualInertiaTensor() * GetAngularVelocity();};
public:
  virtual Vector3 GetAngularVelocity() const  = 0;

  /*-------------------
  Set methods.
  -------------------*/
  

  virtual void SetPos(const Vector3& pos)  = 0;
  /// SetMass center and orientation around it... 
  virtual void SetOrientationMassCenter(const Vector3& pos, const QuaternionRB& orientation);
  virtual void SetOrientation(const QuaternionRB& orientation)  = 0;
  virtual void SetPosAndOrientation(const Vector3& pos, const QuaternionRB& orientation) {SetPos(pos); SetOrientation(orientation);};
  virtual void SetMassCenter(const Vector3& cPos)  = 0;

  virtual void SetVelocity(const Vector3& cVelocity)  = 0;
  virtual void SetAngularVelocity(const Vector3& cAngularVelocity)  = 0;

  /*-------------------
  Other methods.
  -------------------*/
  //virtual void AngularVelocityFromAngularMomentum() {SetAngularVelocity(GetActualInvInertiaTensor()* GetAngularMomentum());};
  // beware this function is slow because of inverse
  //virtual void AngularMomentumFromAngularVelocity() {SetAngularMomentum(GetActualInvInertiaTensor().InverseGeneral() * GetAngularVelocity());};
//protected:
  //Vector3 CalculateTorgue(const Vector3& cForce, const Vector3& cPoint) const;

public:
  Vector3 AccelerationAtPoint(const Vector3& cForceBank, 
    const Vector3&  cTorgueBank, 
    const Vector3& cPoint, 
    TRIBOOL bPlus = TB_TRUE) const;

  Vector3 DiffAccelerationAtPointOnForceAndTorque( const Vector3& cAccelPoint, 
    const Vector3& cForce, 
    const Vector3& cTorque) const;

  void DiffAccelerationAtPointOnForceAtPoint(Vector3& cAccel, 
    const Vector3& cAccelPoint, 
    const Vector3& cForce, 
    const Vector3& cForcePoint) const;

  Vector3 SpeedAtPoint(const Vector3& cPoint) const;

  Vector3 DiffSpeedAtPointOnImpulseAndTorque( const Vector3& cSpeedPoint, 
    const Vector3& cImpulse, 
    const Vector3& cImpulseTorque) const;  

  float CalculateEnergy();
  void SlowDown(float coef);  

  //-----------------------------
  // Transforms 
  //-----------------------------
  virtual Vector3 DirectionBodyToWorld(Vector3Val dirBody) const = 0;
  virtual Vector3 DirectionWorldToBody(Vector3Val dirWorld) const = 0;
  virtual Vector3 PositionBodyToWorld(Vector3Val posBody) const = 0;
  virtual Vector3 PositionWorldToBody(Vector3Val posWorld) const = 0;
};


//--------------------------------------
//  IRigidBody
//--------------------------------------

/** 
* Class for rigid body implementation.
*/
class IRigidBody : public IRigidBodyState 
{
typedef IRigidBodyState base;
protected:
  int _tag;
public:
  //-----------------------------
  // Constructors
  //-----------------------------	
  IRigidBody(): _tag(-1) {}; 
  virtual ~IRigidBody() {};

  //-----------------------------
  // Methods
  //-----------------------------

  virtual bool operator==(const IRigidBody& src) const = 0; 

  // return - 0 not moving return 1 moving a bit, 2 strongly moving
  virtual int IsMoving() const = 0;  

  virtual bool IsStatic() const {return true;};

  virtual bool CanBeStopped(unsigned int time) = 0;
  virtual float GetMaxSimTime() const  = 0; 
   
  void SetTag(int tag) {_tag = tag;};
  int GetTag() const {return _tag;};

  /// function is called after physical library change bodies position or orientation 
  virtual void NotifyAboutMove() {};

  virtual bool CollideWithObjs() const {return true;};
  virtual bool CollideWithLand() const {return true;};

  //-----------------------------
  // Force & Torque Banks
  //-----------------------------
  virtual void ZeroBanks() = 0; 
  virtual void AddForceToBanks(const Vector3& force) = 0;
  virtual void AddTorqueToBanks(const Vector3& torque) = 0;
  virtual Vector3 GetForceBank() const = 0;
  virtual Vector3 GetTorqueBank() const = 0;

  //-----------------------------
  // Joints
  //-----------------------------
  virtual void GetPermanentJoints(RefArray<JointRB>& joints) {};
  virtual void RemovePermanentJoint(JointRB * jnt) {};

  virtual void AddJointToStorage(JointRB * joint) = 0;
  virtual const RefArray<JointRB>&  GetStoredJoints() const = 0;

  // notifies body if during last calculation was calculated impulse configuration. Default is force configuration  
  virtual void ClearStoredJoints() = 0; 

  //-----------------------------
  // Joints
  //-----------------------------
  // reactor can write to body info, later processed by SimulatePost
  virtual void RunReactors(float deltaT, bool wasImpulseJoints) {};
  virtual void SimulatePost() {};
  virtual void SimulateInnerProcesses(float deltaT) {};

  //-----------------------------
  // Detection collision
  //-----------------------------
  virtual void PrepareForDC() {};
  virtual void DCDone() {};
  virtual void PrepareForProcessJoints() {};
  virtual void ProcessJointsDone() {};


  USE_CASTING_ROOT;
};

#endif