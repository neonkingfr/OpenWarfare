#ifdef _MSC_VER
#pragma once
#endif

#ifndef __TRIGGERRB_HPP__
#define __TRIGGERRB_HPP__
 
#include <Poseidon/lib/objectHierarchyExt.hpp>
//------------------------------------
// BallJointRB
//------------------------------------

/** Body reactor is object that works more like a functor. Its input are joints applied to body and the body. 
  * According to input it can perform changes on body (it cannot delete body etc, but it can set up flag for deletion etc...)
  */
class BodyReactor : public HierarchyItemNoShape
{
public:
  virtual ~BodyReactor() {};  
  //! Performs operation.
  virtual void operator()(RefArray<JointRB>& joints, bool impulseJoints, HierarchyIRigidBody * bodyRB, float deltaT) const = 0;   
};

//------------------------------------
// MaxForceBodyReactor
//------------------------------------
/** Reactor calculates maximal force and torque applied on body and 
  * if any of them reaches critical value it launches React function. */
class MaxForceBodyReactor: public BodyReactor
{
protected:
  float _maxForceSq;
  float _maxTorqueSq;

  float _maxImpulseSq;
  float _maxAngMomentumSq;
  
public: 
  void Init(float force, float torque, float impulse, float angMomentum);
  void operator()(RefArray<JointRB>& joints, bool impulseJoints, HierarchyIRigidBody * bodyRB, float deltaT) const;

  //! React can only change the body (it cannot delete it). If it is needed body can be made individual.
  virtual void React(HierarchyIRigidBody * bodyRB) const = 0;  
};

#endif //__TRIGGERRB_HPP__