#include <Poseidon/lib/wpch.hpp>

#if _ENABLE_RIGID_BODIES

#include <Poseidon/lib/object.hpp>
#include "triggerRB.hpp"

//------------------------
// MaxForceBodyReactor
//------------------------

void MaxForceBodyReactor::Init(float force, float torque, float impulse, float angMomentum)
{
  if (force == FLT_MAX)
    _maxForceSq = FLT_MAX;
  else    
    _maxForceSq = force * force;

  if (torque == FLT_MAX)
    _maxTorqueSq = FLT_MAX;
  else    
    _maxTorqueSq = torque * torque;

  if (impulse == FLT_MAX)
    _maxImpulseSq = FLT_MAX;
  else    
    _maxImpulseSq = impulse * impulse;

  if (angMomentum == FLT_MAX)
    _maxAngMomentumSq = FLT_MAX;
  else    
    _maxAngMomentumSq = angMomentum * angMomentum;
}
void MaxForceBodyReactor::operator()(RefArray<JointRB>& joints, bool impulseJoints, HierarchyIRigidBody * bodyRB, float deltaT) const
{
  if (joints.Size() == 0)
    return;  

  float forceSize = 0; 
  float torqueSize = 0; 
  
  if (impulseJoints)
  {
    float deltaTSq = deltaT * deltaT;
    for(int i = 0; i < joints.Size(); i++)
    {
      forceSize += joints[i]->GetForceInJoint().SquareSize() * deltaTSq;
      if (forceSize > _maxImpulseSq)
      {
        return React(bodyRB);
      }

      int j = joints[i]->HasIRigidBody(*bodyRB,0) ? 0 : 1;
      torqueSize += joints[i]->GetTorqueOnBody(j).SquareSize() * deltaTSq;
      if (torqueSize > _maxAngMomentumSq)
      {
        return React(bodyRB);      
      }    
    }
  }
  else
  {   
    for(int i = 0; i < joints.Size(); i++)
    {

      forceSize += joints[i]->GetForceInJoint().SquareSize();
      if (forceSize > _maxForceSq)    
        return React(bodyRB);   

      int j = joints[i]->HasIRigidBody(*bodyRB,0)  ? 0 : 1;     
      torqueSize += joints[i]->GetTorqueOnBody(j).SquareSize();
      if (torqueSize > _maxTorqueSq)
        return React(bodyRB);      
    }
  }
}

#endif