#ifdef _MSC_VER
#pragma once
#endif

#ifndef __INC_H__
#define __INC_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <math.h>
#include <memory.h>

#include <Es/Containers/array.hpp>
#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>

#include <assert.h>
#ifndef ASSERT
#define ASSERT Assert
#endif

typedef int BOOL;
#define FALSE 0
#define TRUE 1

#define _PROFILE_MARINA 1

#if _PROFILE_MARINA
#define PROFILE_SCOPE_MARINA PROFILE_SCOPE
#define START_PROFILE_MARINA START_PROFILE
#define END_PROFILE_MARINA END_PROFILE
#else
#define PROFILE_SCOPE_MARINA(a) 
#define START_PROFILE_MARINA(a)
#define END_PROFILE_MARINA(a)
#endif

#define _ADD_ARROW_ENABLED 0

#define _USE_PEDESTALS 0

#if _ADD_ARROW_ENABLED
void AddArrow(Vector3Val pos,Vector3Val dir, float size, DWORD color);
void ResetArrows();
#endif
//#define START_PROFILE_MARINA START_PROFILE
//#define END_PROFILE_MARINA END_PROFILE


#if _MSC_VER
	// make for iterator variable local
	#define for if( false ) {} else for
#endif

#define NUMERICAL_ERROR 0.00001f

__forceinline BOOL IsNumericalZero(float fValue)
{
	return ((fValue <= NUMERICAL_ERROR) && (fValue >= -NUMERICAL_ERROR));
}

// Function returns unsigned minimum. Negative values mean infinity.
template <class Type> Type umin(Type n, Type m)
{
	if (n < 0) 
		return m;
	
	if (m < 0)
		return n;

	return n < m ? n : m;
};

// Debugging tools
extern unsigned long g_iDebugID;
extern unsigned long g_iStopID;

inline void DebugPoint(void)
{
	if (g_iStopID == g_iDebugID)
	{
		ASSERT(FALSE);
		//int i = 3;
	}
	
	//Log("DebugPoint %d", g_iDebugID);

	g_iDebugID++;
	
}

#ifndef max
#undef  min
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))
#endif

#include "core/MatrixA.hpp"

typedef Vector<marina_real> M_VECTOR;
#endif