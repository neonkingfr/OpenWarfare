#ifndef __INTERSECTCONVEX_HPP_
#define __INTERSECTCONVEX_HPP_

template <class Geom, class Plane, class Edge>
class FindSeparationPlane
{
protected:
  bool IsSeparationPlane(typename const Plane& sepPlane, const typename Geom& geom1, float& dist);
public:
  bool operator () (typename Plane& sepPlane, const typename Geom& geom1, const typename Geom& geom2, Matrix4Val trans2to1, Matrix4Val trans1to2, float& dist, int& plane1, int& plane2);
};

template <class Geom, class Plane, class Edge>
bool FindSeparationPlane<Geom,Plane,Edge>::IsSeparationPlane(typename const Plane& sepPlane,  const typename Geom& geom, float& dist)
{  
  dist = FLT_MAX;
  for(int i = 0; i < geom.Size(); i++)
  {
    float actDist = sepPlane.Distance(geom.Get(i));
    if (actDist <= 0)
    {
      dist = actDist;
      return false;
    }
    else
      if (actDist < dist)
        dist = actDist;
  }
  return true;
}


template <class Geom, class Plane, class Edge>
bool FindSeparationPlane<Geom,Plane,Edge>::operator () (typename Plane& sepPlane,  const typename Geom& geom1,  const typename Geom& geom2, Matrix4Val trans2to1, Matrix4Val trans1to2, float& dist, int& plane1, int& plane2)
{
  PROFILE_SCOPE(odeMIs1);
  Vector3 centerdiff = geom1.GetCenter() - trans2to1 * geom2.GetCenter();

  // first check if old one is valid
  ADD_COUNTER(odeMIs1,1);
  if (plane1 >= 0 )
  {
    if (plane2 < 0)
    {
      
      // test plane1 as a separation plane 
      // BE WARE normals has opposite direction
      const typename Plane& candidate = geom1.GetPlane(plane1);
      if (candidate.Normal() * centerdiff > 0)
      {
        Plane candidate2(-candidate.Normal(), -candidate.D());
        candidate2.Transform(trans2to1);

        if (IsSeparationPlane(candidate2, geom2, dist) && dist > NUMERICAL_ERROR)
        {
          ADD_COUNTER(odeMIs11,1);
          sepPlane.SetNormal(-candidate.Normal(), -candidate.D());
          return true;
        }
      }
    }
    else
    {
      // edges

      const typename Edge& cand1 = geom1.GetEdge(plane1);

      Vector3 v1 = geom1.Get(cand1.GetVertex(1)) - geom1.Get(cand1.GetVertex(0));

      const typename Edge& cand2 = geom2.GetEdge(plane2);  
      Vector3 v2 = geom2.Get(cand2.GetVertex(1)) - geom2.Get(cand2.GetVertex(0));
      v2 = trans2to1.Rotate(v2);

      Vector3 normal = (v2).CrossProduct(v1);
      if (normal.SquareSize() > NUMERICAL_ERROR) // numerical zero...        
      {
        normal.Normalize();

        sepPlane.SetNormal(normal, -normal * geom1.Get(cand1.GetVertex(0)));
        float dist1 = sepPlane.Distance(geom1.GetCenter());
        if (dist1 > 0)
          sepPlane.SetNormal(-normal, -sepPlane.D());

        //dist = sepPlane.Distance(geom2.GetVertex(cand2[0]));
        float temp2;       
        Plane pl2 = sepPlane;
        pl2.Transform(trans2to1);

        if (IsSeparationPlane(pl2, geom2, dist) && dist > NUMERICAL_ERROR && 
          IsSeparationPlane(Plane(-sepPlane.Normal(), -sepPlane.D()), geom1,  temp2))
        {
          ADD_COUNTER(odeMIs12,1);
          return true;
        }
      }
    }
  }
  else
  {
    if (plane2 >= 0)
    {
       
      // test plane2 as a separation plane
      // BE WARE normals has opposite direction
      const Plane& pl = geom2.GetPlane(plane2);
      Plane candidate(-pl.Normal(), -pl.D());
      candidate.Transform(trans1to2);

      if (candidate.Normal() * centerdiff > 0 && IsSeparationPlane(candidate, geom1, dist) && dist > NUMERICAL_ERROR)
      {
        sepPlane.SetNormal(-candidate.Normal(), -candidate.D() + dist);
        ADD_COUNTER(odeMIs13,1);
        return true;
      }
    }
  }
  
  // check faces first
  for(int i = 0; i < geom1.NPlanes(); i++)
  {
    // BE WARE normals has opposite direction
    const typename Plane& candidate = geom1.GetPlane(i);
    Plane candidateIn2(-candidate.Normal(), -candidate.D());
    candidateIn2.Transform(trans2to1);

    if (candidate.Normal() * centerdiff < 0 && IsSeparationPlane(candidateIn2, geom2, dist) && dist > NUMERICAL_ERROR)
    {
      plane1 = i;
      plane2 = -1;
      sepPlane.SetNormal( -candidate.Normal(), -candidate.D());
      return true;
    }
  }
  
  for(int i = 0; i < geom2.NPlanes(); i++)
  {
    // BE WARE normals has opposite direction
    const Plane& pl = geom2.GetPlane(i);
    Plane candidate(-pl.Normal(), -pl.D());    
    candidate.Transform(trans1to2);
    if (candidate.Normal() * centerdiff > 0 && IsSeparationPlane(candidate, geom1, dist) && dist > NUMERICAL_ERROR)
    {
      plane1 = -1;
      plane2 = i;
      sepPlane.SetNormal(-candidate.Normal(), -candidate.D() + dist);
      return true;
    }
  }
  
  //OK no face is separation plane check edge2edge
  for(int i = 0; i < geom1.NEdges(); i++)
  {
    const typename Edge& cand1 = geom1.GetEdge(i);

    Assert(cand1.GetFace(1) >= 0);
    if (cand1.GetFace(1) < 0) 
      continue; // wrong convex component
   
    // BE WARE normals has opposite direction
    Vector3Val norm11 = geom1.GetPlane(cand1.GetFace(0)).Normal();   
    Vector3Val norm12 = geom1.GetPlane(cand1.GetFace(1)).Normal();

    if (norm11 * centerdiff <=0 && norm12 * centerdiff <=0)
      continue;

    if (norm11 * norm12 > 0.999f)
      continue; //not the real edge.  

    Vector3 v1 = geom1.Get(cand1.GetVertex(1)) - geom1.Get(cand1.GetVertex(0));   

    for(int j = 0; j < geom2.NEdges(); j++)
    {     
      const typename Edge& cand2 = geom2.GetEdge(j);  

      if (cand2.GetFace(1) < 0) 
        continue; // wrong convex component

      // BE WARE normals has opposite direction
      Vector3 norm21 = geom2.GetPlane(cand2.GetFace(0)).Normal();
      Vector3 norm22 = geom2.GetPlane(cand2.GetFace(1)).Normal();

      if (norm21 * norm22 > 0.999f)
        continue; //not the real edge.

      Vector3 v2 = geom2.Get(cand2.GetVertex(1)) - geom2.Get(cand2.GetVertex(0));
      v2 = trans2to1.Rotate(v2);     

      float val = (v2 * norm11) * (v2 * norm12);
      if (val > 0)       
        continue;      

      norm21 = trans2to1.Rotate(norm21);
      norm22 = trans2to1.Rotate(norm22);

      if ((v1 * norm21) * (v1 * norm22) > 0) 
        continue;

      //whaw maybe separation plane
      Vector3 normal = v2.CrossProduct(v1);
      if (normal.SquareSize() < 0.000001f) // numerical zero...        
        continue;

      normal.Normalize();

      sepPlane.SetNormal(normal, -normal * geom1.Get(cand1.GetVertex(0)));       

      float dist1 = sepPlane.Distance(geom1.GetCenter());
      if (dist1 > 0)
        sepPlane.SetNormal(-normal, -sepPlane.D());       

      dist = sepPlane.Distance(trans2to1 * geom2.Get(cand2.GetVertex(0)));        

      if ( dist > NUMERICAL_ERROR && dist <= sepPlane.Distance(trans2to1 * geom2.GetCenter()))          
      {        
#if 0 // _DEBUG 
        float distTemp;
        Plane sepPlane1;
        sepPlane1.SetNormal(-sepPlane.Normal(), -sepPlane.D());

        Assert(IsSeparationPlane(sepPlane1, geom1, distTemp) || fabs(distTemp) < NUMERICAL_ERROR);

        Plane sepPlane2(sepPlane);
        sepPlane2.Transform(trans2to1);
        Assert(IsSeparationPlane(sepPlane2, geom2, distTemp));
#endif
        plane1 = i;
        plane2 = j;
        return true;
      }        
    }
  }    
  return false;
}
#endif