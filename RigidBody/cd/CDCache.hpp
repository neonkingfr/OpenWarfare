#ifndef __CDCACHE_HPP__
#define __CDCACHE_HPP__

//#include <Es/Containers/sortedArray.hpp>

template <class Type, class KeyTypeTraits>
struct FindArrayKey2DTraits
{
  typedef typename const KeyTypeTraits::KeyType& KeyType;
  //! get key
  static KeyType GetKey(const Type &type) {return type._ID;}
  /// check if two keys are equal
  static bool IsEqual(KeyType a, KeyType b) {return KeyTypeTraits::IsEqual(a,b);}

};

template <class Type, class KeyTypeTraits>
struct ArrayKey2DItem 
{  
  typedef typename KeyTypeTraits::KeyType KeyType;
  ArrayKey2DItem() {_data = new Type();};
  KeyType _ID;
  Ref<Type> _data;

  ClassIsMovable(ArrayKey2DItem)
};

template  <class Type, class KeyTypeTraits>
class FindArrayKey2DRowArray : public RemoveLinks , 
  private FindArrayKey<ArrayKey2DItem<Type,KeyTypeTraits>, FindArrayKey2DTraits<ArrayKey2DItem<Type,KeyTypeTraits>, KeyTypeTraits > >
{
  typedef FindArrayKey<ArrayKey2DItem<Type,KeyTypeTraits>, FindArrayKey2DTraits<ArrayKey2DItem<Type,KeyTypeTraits>, KeyTypeTraits > > base;
  typedef ArrayKey2DItem<Type,KeyTypeTraits> Item;

public:
  typedef typename KeyTypeTraits::KeyType KeyType;
  typedef const KeyType& KeyTypeVal;

  FindArrayKey2DRowArray() {};
  ~FindArrayKey2DRowArray() {};

  void Compact(){ base::Compact();};

  Type * Add(KeyTypeVal ID1);
  Type * GetOrAdd(KeyTypeVal ID1);
  Type * Get(KeyTypeVal ID1) const;  

  void Delete(KeyTypeVal ID) {DeleteKey(ID);};
};

template <class Type, class KeyTypeTraits>
Type * FindArrayKey2DRowArray<Type,KeyTypeTraits>::Add(KeyTypeVal ID)
{
  Item& item = Append();
  item._ID = ID;
  return item._data;
};

template <class Type, class KeyTypeTraits>
Type * FindArrayKey2DRowArray<Type,KeyTypeTraits>::GetOrAdd(KeyTypeVal ID)
{
  Type * ret = Get(ID);
  if (ret) 
    return ret;

  Item& item = Append();
  item._ID = ID;
  return item._data;
};

template <class Type, class KeyTypeTraits>
Type * FindArrayKey2DRowArray<Type,KeyTypeTraits>::Get(KeyTypeVal ID) const
{
  int i = FindKey(ID);
  if (i < 0)
    return NULL;

  return operator[](i)._data;
};

template <class Type, class KeyTypeTraits>
class FindArrayKey2D : private FindArrayKey<ArrayKey2DItem<FindArrayKey2DRowArray<Type, KeyTypeTraits>, KeyTypeTraits >, FindArrayKey2DTraits<ArrayKey2DItem<FindArrayKey2DRowArray<Type, KeyTypeTraits>, KeyTypeTraits >, KeyTypeTraits > > 
{
  typedef FindArrayKey<ArrayKey2DItem<FindArrayKey2DRowArray<Type, KeyTypeTraits>, KeyTypeTraits >, FindArrayKey2DTraits<ArrayKey2DItem<FindArrayKey2DRowArray<Type, KeyTypeTraits>, KeyTypeTraits >, KeyTypeTraits > >  base;
public:
  typedef FindArrayKey2DRowArray<Type, KeyTypeTraits> TypeArray; 
  typedef typename KeyTypeTraits::KeyType KeyType;
  typedef const KeyType& KeyTypeVal;
  typedef ArrayKey2DItem<FindArrayKey2DRowArray<Type, KeyTypeTraits>, KeyTypeTraits > Item;

  FindArrayKey2D() {}; 

  void Clear() {base::Clear();};  
  void Compact();
  int Size() const {return base::Size();};
  TypeArray * operator[](int i) const {return base::operator[](i)._data;};  

  Type * Add(KeyTypeVal ID1, KeyTypeVal ID2, bool& inverse);
  Type * Get(KeyTypeVal ID1, KeyTypeVal ID2, bool& inverse);
  Type * GetOrAdd(KeyTypeVal ID1, KeyTypeVal ID2, bool& inverse);
  TypeArray * Get(KeyTypeVal ID);

  void Delete(KeyTypeVal ID1, KeyTypeVal ID2);
  void Delete(KeyTypeVal ID);
};

template <class Type, class KeyTypeTraits>
void FindArrayKey2D<Type,KeyTypeTraits>::Compact()
{
  base::Compact();

  for(int i = 0; i < Size(); i++)
  {
    TypeArray * subArray = operator[](i);    
    subArray.Compact();
  }
};

template <class Type, class KeyTypeTraits>
typename FindArrayKey2D<Type,KeyTypeTraits>::TypeArray * FindArrayKey2D<Type,KeyTypeTraits>::Get(KeyTypeVal ID)
{
  int i = FindKey(ID);
  if (i >= 0)
    return operator[](i);
  else
    return NULL;
};

template <class Type, class KeyTypeTraits>
Type * FindArrayKey2D<Type,KeyTypeTraits>::Get(KeyTypeVal ID1, KeyTypeVal ID2, bool& inverse)
{
  PROFILE_SCOPE(odeCG);
  TypeArray * array = Get(ID1);
  if (array == NULL)
  {
    array = Get(ID2);
    if (array == NULL)
      return NULL;
    else
    {      
      Type * ret = array->Get(ID1);
      if (ret)
        inverse = !inverse;

      return ret;
    }
  }  
  
  return array->Get(ID2);
};

template <class Type, class KeyTypeTraits>
Type * FindArrayKey2D<Type,KeyTypeTraits>::Add(KeyTypeVal ID1, KeyTypeVal ID2, bool& inverse)
{  
  PROFILE_SCOPE(odeCG);
  TypeArray * array = Get(ID1);
  if (array == NULL)
  {
    array = Get(ID2);
    if (array)
    {
      inverse = !inverse;
      return array->Add(ID1); 
    }
    else
    {
      // create new 
      int i = Size();
      Item& item = Append();
      item._ID = ID1; 

      array = operator[](i);
      return array->Add(ID2);
    }
  }  

  return array->Add(ID2);
};

template <class Type, class KeyTypeTraits>
Type * FindArrayKey2D<Type,KeyTypeTraits>::GetOrAdd(KeyTypeVal ID1, KeyTypeVal ID2, bool& inverse)
{  
  PROFILE_SCOPE(odeCG);
  TypeArray * array = Get(ID1);
  if (array == NULL)
  {
    array = Get(ID2);
    if (array)
    {
      inverse = !inverse;
      return array->GetOrAdd(ID1); 
    }
    else
    {
      // create new 
      int i = Size();
      Item& item = Append();
      item._ID = ID1; 

      array = operator[](i);
      return array->Add(ID2);
    }
  }  

  return array->GetOrAdd(ID2);
};


template <class Type, class KeyTypeTraits>
void FindArrayKey2D<Type,KeyTypeTraits>::Delete(KeyTypeVal ID1, KeyTypeVal ID2)
{
  TypeArray * array = Get(ID1);
  if (array == NULL)
  {
    array = Get(ID2);
    if (array == NULL)
      return;
    else
      array->Delete(ID1)
  }  

  return array->Delete(ID2);
};

template <class Type, class KeyTypeTraits>
void FindArrayKey2D<Type,KeyTypeTraits>::Delete(KeyTypeVal ID)
{
  DeleteKey(ID);
 
  for(int i = 0; i < Size(); i++)
  {
    operator[](i)->Delete(ID);    
  }
  return;
};


//------------------------
// Cache
//------------------------


struct IntKeyTypeTraits
{
  typedef int KeyType;
  /// check if two keys are equal
  static bool IsEqual(KeyType a, KeyType b) {return a == b;}
};

struct HierarchyIDKeyTypeTraits
{
  typedef HierarchyID KeyType;
  /// check if two keys are equal
  static bool IsEqual(const KeyType& a, const KeyType& b) {return a == b;}
};

struct CDConvexComponentCacheData : public RemoveLinks
{
public:
  // information about where was plane found
  int _compID;
  int _plane1; // if both are positive there are edge IDs otherwise. There plane IDs.
  int _plane2;  
  // scale ... how much should be bodies smaller. valid only if it is positive.
  float _scale; 

  CDConvexComponentCacheData() : _plane1(-1), _plane2(-1),
    _scale(1), _compID(-1) {};
};

typedef FindArrayKey2D<CDConvexComponentCacheData, IntKeyTypeTraits> CDCacheCC; 

struct CDBodyCacheData : public RemoveLinks
{
public: 
  CDBodyCacheData() : _lastCycleWithCollision(-1) {};

  typedef FindArrayKey2D<CDConvexComponentCacheData, IntKeyTypeTraits> CDCacheCC; 
  HierarchyID _id;
  CDCacheCC _cacheCC;   // cache of for convex componets 
  int _lastCycleWithCollision; 
};

typedef FindArrayKey2D<CDBodyCacheData, HierarchyIDKeyTypeTraits> CDCache;

extern CDCache g_cdCache;



#endif //__CDCACHE_HPP__