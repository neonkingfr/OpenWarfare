#include <Poseidon/lib/wpch.hpp>


#include <Poseidon/lib/Shape/shape.hpp>
#include <Poseidon/lib/object.hpp>
#include <Poseidon/lib/landscape.hpp>
#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>

#if _ENABLE_RIGID_BODIES
#include "ContactConvex.hpp"
#include "ContactLandConvex.hpp"
#include "IntersectConvex.hpp"
#include "IntersectLandConvex.hpp"
#include "CDCache.hpp"


const float g_cfm = 0.0001f;
const float g_erp = 0.002f;
const float g_frictionCoef = 0.7f;
const float g_restitutionCoef = 0.3f;
const float g_scaleMin = 0.5f;
const float g_scaleCoef = 0.1f;

const float g_epsilon = 0.02f;

CDCache g_cdCache;


CDContactArray::CDContactArray()
{
  static StaticStorage<CDContactPair> storage;
  SetStorage(storage.Init(128));
};
CDContactArray::~CDContactArray() {};

//-----------------------------------------
// CCwithShape
//-----------------------------------------

//! Wrapper around convex component. convex component does not contains shape. This wrapper does.
class CCwithShape
{
protected:
  const ConvexComponent& _cc; 
  const Shape& _shape;

public:
  const CCwithShape(const ConvexComponent& cc ,const Shape& shape) : _cc(cc) , _shape(shape) {};

  float GetRadius() const {return _cc.GetRadius();};
  int Size() const {return _cc.Size();};
  Vector3Val Get(int i) const {return _shape.Pos(_cc[i]);};
  Vector3Val GetVertex(int i) const {return _shape.Pos(i);};

  int NPlanes() const {return _cc.NPlanes();};
  // BE WARE! in convex components has plane normals opposite values as is expected
  const Plane& GetPlane(int i) const {return _cc.GetPlane(&_shape, i);}; 
  const Poly& GetFace(int i) const {return _cc.GetFace(&_shape, i);};

  int NEdges() const {return _cc.NGeomEdges();}
  GeomEdgeVal GetEdge(int i) const {return _cc.GetGeomEdge(i);};
  Vector3Val GetCenter() const {return _cc.GetCenter();};  
};

/** Detection collision searches for all contacts within distance epsilon. It can find contacts with 
  * several different normals. But because we are colliding convex components result collisions all must to have the 
  * same normal. This filter will find the closes contact (contact points are closest), take its normal and then use
  * only the contact with the same normal.
  */

void FilterPairs(RBCollisionBuffer& cFinalContacts, CDContactArray& foundPairs, 
                 float scale, float epsilon, 
                 Vector3Val geomCenter0, Vector3 geomCenter1)
{
  if (foundPairs.Size() == 0)
    return;

  // calculate correct under with respect to scale
  if (scale < 1)
  {  
    Matrix4 scaleMat0(MScale, scale, geomCenter0); // all values are in 0 coord so not scaled spaces
    geomCenter1 = scaleMat0 * geomCenter1;

    for( int i = 0; i < foundPairs.Size(); i++)
    {
      CDContactPair& contactPair = foundPairs[i];
      contactPair._pos = scaleMat0 * contactPair._pos;
      contactPair._dist *= scale;

      Vector3 diff2 = contactPair._pos - geomCenter1;      
      Vector3 diff1 = contactPair._pos + (-contactPair._norm * contactPair._dist) - geomCenter0;  

      contactPair._dist -= (1 - scale) / scale * (contactPair._norm * (diff1 - diff2)) + epsilon;
      contactPair._dist *= -1;      
    }
  }
  else
  {
    for( int i = 0; i < foundPairs.Size(); i++)
    {
      CDContactPair& contactPair = foundPairs[i];      
      contactPair._dist = epsilon - contactPair._dist;      
    }
  }


  // Find closest par.
  float maxUnder = 0;
  Vector3 maxUnderNorm;

  for( int i = 0; i < foundPairs.Size(); i++)
  {
    if (maxUnder < foundPairs[i]._dist)
    {
      maxUnder = foundPairs[i]._dist;
      maxUnderNorm = foundPairs[i]._norm;
    }
  }

  if (maxUnder <= 0)
    return;

  // find points with the same normal
  for( int i = 0; i < foundPairs.Size(); i++)
  {
    if (foundPairs[i]._norm * maxUnderNorm > 0.98f && foundPairs[i]._dist > 0)
    {
      // Add to contacts;
      RBContactPoint& tContactPoint = cFinalContacts.Append();

      tContactPoint.pos = foundPairs[i]._pos;
      tContactPoint.under = foundPairs[i]._dist;

      /*if (foundPairs[i]._dist > 0.10f)
      {
      LogF("big dist %f", foundPairs[i]._dist);
      }*/

      tContactPoint.dir[0] = maxUnderNorm;
      
      tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(1,0,0));
      if (tContactPoint.dir[1].Size() < 0.1f) // is new vector correct
        tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(0,1,0));
   
      tContactPoint.dir[1].Normalize();
  
#if __DEBUG_CONTACTS
      tContactPoint.reserved = foundPairs[i]._triangle[0];
#endif


    }
  }
}

void GeomIntersectionRB(RBCollisionBuffer& ret, const ConvexComponent& cc0, const Shape& shape0, Matrix4Val trans0to1, const ConvexComponent& cc1, const Shape& shape1, Matrix4Val trans1to0, CDConvexComponentCacheData * cache, bool inverse)
{  
  PROFILE_SCOPE(odeGIIn);
  ADD_COUNTER(odeGIIn1,1);
  CCwithShape geom0(cc0, shape0);
  CCwithShape geom1(cc1, shape1);

  // find scale till convex component are not overlapping
  FindSeparationPlane<CCwithShape, Plane, GeomEdge> doFindSP;

  Plane sepPlane(VZero, 0);

  Matrix4 scaledTrans0to1 = trans0to1;
  Matrix4 scaledTrans1to0 = trans1to0;

  // temporary values will be used for setting up scaleTrans
  Vector3 centerDiff = trans0to1.Rotate(cc0.GetCenter()) - cc1.GetCenter();
  Vector3 centerDiffInv = -trans1to0.Rotate(centerDiff);

  float dist; 
  int plane1 = -1;
  int plane2 = -1;

  float scale = 1; 
  if (cache)
  {
    if (inverse)
    {    
      plane1 = cache->_plane2;
      plane2 = cache->_plane1;
    }
    else
    {
      plane1 = cache->_plane1;
      plane2 = cache->_plane2;
    }

    scale = cache->_scale;    

    if (scale < 1.0f)
    {
      // scale convex components
      /*
      The following code was replaced by the faster code
      Matrix4 scaleMat0(MScale, scale, cc0.GetCenter());
      Matrix4 scaleMat1(MScale, scale, cc1.GetCenter());
      scaledTrans0to1 = scaleMat1.InverseScaled() *  trans0to1 * scaleMat0;
      scaledTrans1to0 = scaledTrans0to1.InverseGeneral();
      */
      float invScale = 1.0f/scale;
      scaledTrans0to1.SetPosition(((1 - scale) * centerDiff + trans0to1.Position())*invScale);      
      scaledTrans1to0.SetPosition(((1 - scale) * centerDiffInv + trans1to0.Position())*invScale);
    }
    else
      scale = 1;
  }

  if (!doFindSP(sepPlane, geom0, geom1, scaledTrans1to0, scaledTrans0to1, dist, plane1, plane2))
  {  
    float scaleCoef = g_scaleCoef;
    saturateMin(scaleCoef, 0.05f / geom0.GetRadius());
    saturateMin(scaleCoef, 0.05f / geom1.GetRadius());
    scaleCoef = 1 - scaleCoef;

    do 
    {    
      ADD_COUNTER(odeGIIn3,1);
      scale *= scaleCoef;
      /*
      The following code was replaced by the faster code
      Matrix4 scaleMat0(MScale, scale, cc0.GetCenter());
      Matrix4 scaleMat1(MScale, scale, cc1.GetCenter());
      scaledTrans0to1 = scaleMat1.InverseScaled() *  trans0to1 * scaleMat0;
      scaledTrans1to0 = scaledTrans0to1.InverseGeneral();
      */

      float invScale = 1.0f/scale;
      scaledTrans0to1.SetPosition(((1 - scale) * centerDiff + trans0to1.Position())*invScale);      
      scaledTrans1to0.SetPosition(((1 - scale) * centerDiffInv + trans1to0.Position())*invScale);              

    } while (scale > g_scaleMin && !doFindSP(sepPlane, geom0, geom1, scaledTrans1to0, scaledTrans0to1, dist, plane1, plane2));
  }

  if (cache)
  { 
    if (inverse)
    { 
      cache->_plane1 = plane2;
      cache->_plane2 = plane1;
    }
    else
    {
      cache->_plane1 = plane1;
      cache->_plane2 = plane2;
    }
    cache->_scale = scale;
  }

  dist *= scale; // dist is in 0 coord so it is inversely scaled; 
  
  float epsilonScaled = g_epsilon + (cc0.GetRadius() + cc1.GetRadius()) * (1 - scale);
  if (scale < g_scaleMin || dist > epsilonScaled)
    return; // to big scale ignore collision

  epsilonScaled /= scale; // translate it into 0 coord 

  ContactConvex<CCwithShape, Plane, Poly, GeomEdge> doColl;
  CDContactArray found;

  doColl(found, geom0, geom1, scaledTrans1to0, scaledTrans0to1, epsilonScaled, sepPlane);
  FilterPairs(ret, found, scale, g_epsilon, cc0.GetCenter(), scaledTrans1to0 * cc1.GetCenter());
}

bool HasIntersection(Vector3 minMax0[], Matrix4Par trans0to1,Vector3 minMax1[]);

void GeomIntersectionRB(RBCollisionBuffer& ret, const GeomShapeAnimatedBase& cc0, Matrix4Val trans0to1, const GeomShapeAnimatedBase& cc1, float mass, CDBodyCacheData * cache, bool inverse)
{
  PROFILE_SCOPE(odeGI);
  ADD_COUNTER(odeGI,1);
  int n = ret.Size();
  int nn = n; 

/*#if _ENABLE_REPORT
  if (cc0.Size() * cc1.Size() > 20)
  {
    LogF("Many test: %s %d %s %d", cc0.DebugName(), cc0.Size(), cc1.DebugName(), cc1.Size()); 
  }
#endif*/

  Matrix4 trans1to0 = trans0to1.InverseGeneral();
  // test every x every component for now. 
  for(int i = 0; i < cc0.NGroups(); i++)
  {      
    if (!cc0.IsEnabled(i))
      continue; 

    Matrix4 trans0Anim = trans0to1 * cc0.GetTransform(i);    
    Matrix4 trans0AnimInv = cc0.GetInvTransform(i) * trans1to0;    

    for(int j = 0; j < cc1.NGroups(); j++)
    {
      if (!cc1.IsEnabled(j))
        continue; 

      Matrix4 trans0to1Anim = cc1.GetInvTransform(j) * trans0Anim;
      Matrix4 trans1to0Anim = trans0AnimInv * cc1.GetTransform(j);

      // BBTree vs BBTree
      AUTO_STATIC_ARRAY(const OBBNodeS * ,colliding, 128); 

      OBBNodeS * bbTree0 = static_cast<OBBNodeS *>(cc0.BoundingTree(i));
      OBBNodeS * bbTree1 = static_cast<OBBNodeS *>(cc1.BoundingTree(j));

      bbTree0->DeepVsDeep(bbTree1, trans0to1Anim, colliding, g_epsilon);

      int nColliding = colliding.Size() / 2;
      for(int k = 0; k < nColliding; k++)
      {              
        const ConvexComponent& ccc0 = *(colliding[2 * k]->GetComponnent());
        const ConvexComponent& ccc1 = *(colliding[2 * k + 1]->GetComponnent());

        if (!ccc0.IsEnabled() || !ccc1.IsEnabled() 
          || (ccc0.GetSurfaceInfo() && ccc0.GetSurfaceInfo()->_isWater) ||(ccc1.GetSurfaceInfo() && ccc1.GetSurfaceInfo()->_isWater)) // avoid water components
          continue;

        CDConvexComponentCacheData * ccCache = NULL; 

        bool thisInversed = false;
        if (cache)
        {        
          if (inverse)
            ccCache = cache->_cacheCC.GetOrAdd(ccc1.GetNameIndex(), ccc0.GetNameIndex() + 1000, thisInversed);            
          else
            ccCache = cache->_cacheCC.GetOrAdd(ccc0.GetNameIndex(), ccc1.GetNameIndex() + 1000, thisInversed);            

          Assert(!thisInversed);      
        }

        GeomIntersectionRB(ret, ccc0, cc0.GetShape(), trans0to1Anim, ccc1, cc1.GetShape(), trans1to0Anim, ccCache,inverse);      
        if (n == ret.Size()) 
        {
          ADD_COUNTER(odeGIIn2,1);
          continue;
        }

        Matrix4Val trans0 = cc0.GetTransform(i);
        float friction; 
        float restitution;  

        // set friction and restitution like a average value between two surfaces
        const SurfaceInfo * surf0 = ccc0.GetSurfaceInfo();
        if (surf0)
        {
          friction = surf0->_friction;
          restitution = surf0->_restitution;
        }
        else
        {
          // use default
          friction = g_frictionCoef;   
          restitution = g_restitutionCoef; 
        }

        const SurfaceInfo * surf1 = ccc1.GetSurfaceInfo();
        if (surf1)
        {
          friction += surf1->_friction;
          restitution += surf1->_restitution;
        }
        else
        {
          // use default
          friction += g_frictionCoef;   
          restitution += g_restitutionCoef; 
        }

        friction *= 0.5f;
        restitution *= 0.5f;

        for(; n< ret.Size(); n++)
        {        
          RBContactPoint& pt = ret[n]; 

          // recalculate according to animation          
          pt.pos = trans0 * pt.pos;
          pt.dir[0] = trans0.Rotate(pt.dir[0]);
          pt.dir[1] = trans0.Rotate(pt.dir[1]);

          pt.surface.mode = dContactBounce | dContactSoftCFM | dContactApprox1_1 | dContactApprox1_2 | dContactSlip1 | dContactSlip2 | dContactSoftERP ;

          pt.surface.mu = friction;   
          pt.surface.bounce = restitution; 
          pt.surface.bounce_vel = 0.1f;    

          pt.surface.soft_cfm = g_cfm / mass;
          //pt.surface.soft_erp = g_erp / mass;
          pt.surface.soft_erp = g_erp;
          pt.surface.slip1 =  g_cfm / mass;
          pt.surface.slip2 = g_cfm/ mass;
        }
      }
    }
  }
    
  if (nn == n)
  {
    ADD_COUNTER(odeGI2,1);
  }
}

TypeIsSimple(Poly);

class LandscapeWithColl
{
protected:
  const Landscape& _land;

  int _xb;
  int _xSize;
  int _zb;
  int _zSize;

  float _maxHeight; // the most height point in _pos field... 

  // prepare data for 4x4, this class is constructed quite often 
  AUTO_STORAGE(Poly, _facesData, 32);
  StaticArrayAuto<Poly> _faces; 

  AUTO_STORAGE(Poly, _edgesData, 96); // TODE later better calculation
  StaticArrayAuto<GeomEdge> _edges;

  AUTO_STORAGE(Vector3, _posData, 25);
  StaticArrayAuto<Vector3> _pos;

  AUTO_STORAGE(Plane, _planeData,32);  
  StaticArrayAuto<Plane> _plane;
  
  Plane CalcPlane(int i) const;
public:
  LandscapeWithColl(const Landscape& land);
    
  bool Create(Vector3Val minBB, Vector3Val maxBB);
  float MaxHeight() const {return _maxHeight;};  

  Vector3Val GetVertex(int i) const {return _pos[i];}

  int NPlanes() const {return _faces.Size();};
  Plane GetPlane(int i) const {return _plane[i];}
  const Poly& GetFace(int i) const {return _faces[i];};

  int NEdges() const {return _edges.Size();}  
  GeomEdgeVal GetEdge(int i) const {return _edges[i];};

  float SurfaceY(float x, float z) const {return _land.SurfaceY( x,  z);};
};

Plane LandscapeWithColl::CalcPlane(int i) const
{
  /// create plane corresponding to face 
  const Poly& face = GetFace(i);
  Assert(face.N() == 3);
   
  Vector3 normal = (GetVertex(face.GetVertex(2)) - GetVertex(face.GetVertex(1))).CrossProduct(GetVertex(face.GetVertex(0)) - GetVertex(face.GetVertex(1)));
  normal.Normalize();

  return Plane(normal, GetVertex(face.GetVertex(1)));
}

/*Vector3Val LandscapeWithColl::GetVertex(int i) const 
{
  int xx = _xb + i / _zSize;
  int zz = _zb + i % _zSize;

  float l=_land.ClippedData(zz,xx);

  return  Vector3(_land.GetTerrainGrid() * xx, l, _land.GetTerrainGrid() * zz);
}*/

// prepare data for 4x4, this class is constructed quite often 

LandscapeWithColl::LandscapeWithColl(const Landscape& land):
_land(land), _faces(_facesData,sizeof(_facesData)),  _edges(_edgesData, sizeof(_edgesData)), _pos(_posData,sizeof(_posData)), _plane(_planeData,sizeof(_planeData)) {};


bool LandscapeWithColl::Create(Vector3Val minBB, Vector3Val maxBB)
{
  _xb = toIntFloor(minBB[0] * _land.GetInvTerrainGrid());
  _zb = toIntFloor(minBB[2] * _land.GetInvTerrainGrid());  
  
  _xSize = toIntCeil(maxBB[0] * _land.GetInvTerrainGrid()) + 1 - _xb;
  _zSize = toIntCeil(maxBB[2] * _land.GetInvTerrainGrid()) + 1 - _zb; 

  // is trivially over?   
  for(int xx = 0; xx < _xSize ; xx++)
  {
    for(int zz = 0; zz < _zSize; zz++)
      if (_land.ClippedData(_zb + zz ,_xb + xx) >= minBB[1])
        goto out;
  }
  return true;

out:

  // set vertexes
  _pos.Reserve(_xSize * _zSize);
  _pos.Resize(_xSize * _zSize);

  int i = 0;
  _maxHeight = -FLT_MIN;
  int xe = _xSize + _xb;
  int ze = _zSize + _zb;
  for(int xx = _xb; xx < xe; xx++)
    for(int zz = _zb; zz < ze; zz++,i++)
    {          
      _pos[i] = Vector3(_land.GetTerrainGrid() * xx, _land.ClippedData(zz, xx), 
        _land.GetTerrainGrid() * zz);
      if (_pos[i][1] > _maxHeight)
        _maxHeight = _pos[i][1];
    }   

  // set faces
  _faces.Reserve((_xSize - 1) * (_zSize - 1) * 2);

  for(int xx = 0; xx < _xSize - 1; xx++)
    for(int zz = 0; zz < _zSize - 1; zz++)
    {
    
      Poly& face1 = _faces.AppendFast();
      face1.SetN(3);
      face1.Set(0,_zSize * xx + zz);
      face1.Set(1,_zSize * xx + (zz + 1));
      face1.Set(2,_zSize * (xx + 1) + zz);

      Poly& face2 = _faces.AppendFast();
      face2.SetN(3);
      face2.Set(0,_zSize * xx + (zz + 1));
      face2.Set(1,_zSize * (xx + 1) + (zz + 1));
      face2.Set(2,_zSize * (xx + 1) + zz);    
    }  

   // planes  
   _plane.Reserve(_faces.Size());
   _plane.Resize(_faces.Size());
   for(int i = 0; i < _faces.Size(); i++)
     _plane[i] = CalcPlane(i);

   // find edges. Algorithm is very ineffective ... later write faster one
  _edges.Reserve(3 *  _faces.Size()); //TODO: Its to much add later better calculation
  for(int i = 0; i < _faces.Size(); i++)
  {
    Poly & face = _faces[i];
    for(int j = 0; j < face.N(); j++)
    {
      int vert0 = face.GetVertex((j == 0) ? face.N()-1: j-1);
      int vert1 = face.GetVertex(j);

      // maybe this edge exist?
      int l = 0;
      for(; l < _edges.Size(); l++)
      {
        if (_edges[l].GetVertex(0) == vert1 && _edges[l].GetVertex(1) == vert0)
          break;
      }

      if ( l == _edges.Size())
      {
        // create new edge
        GeomEdge& edge = _edges.AppendFast();
        edge._vertexes[0] = vert0;
        edge._vertexes[1] = vert1;
        edge._faces[0] = i;
        edge._faces[1] = -1;                
      }
      else
        _edges[l]._faces[1] = i;
    }
  }

  /// remove edges borders of region _faces[1] == -1
  for(int i = 0; i < _edges.Size(); i++)
  {
    if (_edges[i]._faces[1] == -1)
    {
      _edges.Delete(i);
      i--;       
    }
  }
  
  return false;


}

void FilterLandPairs(RBCollisionBuffer& finalContacts, CDContactArray& foundPairs, float shift, float epsilon, 
                     Vector3Val geomCenter, int nFaces)
{
  if (foundPairs.Size() == 0)
    return;

  if (shift > 0)
  {  
    for( int i = 0; i < foundPairs.Size(); i++)
    {
      CDContactPair& contactPair = foundPairs[i];
      Vector3 diff = contactPair._pos  - shift * VUp;      
      contactPair._dist =  epsilon - contactPair._dist + contactPair._norm * VUp * shift;      
    }
  }
  else
  {
    for( int i = 0; i < foundPairs.Size(); i++)
    {
      CDContactPair& contactPair = foundPairs[i];      
      contactPair._dist = epsilon - contactPair._dist;      
    }
  }

  for(int t = 0; t < nFaces; t++)
  {
    // Find closest par.
    float maxUnder = -1;
    Vector3 maxUnderNorm;

    for( int i = 0; i < foundPairs.Size(); i++)
    {
      if ((foundPairs[i]._triangle[0] == t || foundPairs[i]._triangle[1] == t) && maxUnder < foundPairs[i]._dist)
      {      
        maxUnder = foundPairs[i]._dist;
        maxUnderNorm = foundPairs[i]._norm;
      }
    }

    if (maxUnder <= 0)
      continue;

    for( int i = 0; i < foundPairs.Size(); i++)
    {
      if ((foundPairs[i]._triangle[0] == t || foundPairs[i]._triangle[1] == t) && foundPairs[i]._norm * maxUnderNorm > 0.98 && foundPairs[i]._dist > 0)
      {
        // Add to contacts;
        RBContactPoint& tContactPoint = finalContacts.Append();

        tContactPoint.pos = foundPairs[i]._pos;
        tContactPoint.under = foundPairs[i]._dist;

        /*if (foundPairs[i]._dist > 0.10f)
        {
        LogF("Landscape big dist %f", foundPairs[i]._dist);
        } */ 

        tContactPoint.dir[0] = -maxUnderNorm;

        // Find pedestal axes
        tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(1,0,0));
        if (tContactPoint.dir[1].Size() < 0.1f) // is new vector correct
          tContactPoint.dir[1] = tContactPoint.dir[0].CrossProduct( Vector3(0,1,0));        

        tContactPoint.dir[1].Normalize();        

        foundPairs.Delete(i); //allready used
        i--;
      }
    }
  }
}

const float epsilonWithGround = 0.02f;

void LandIntersectionRB(RBCollisionBuffer& ret, const ConvexComponent& cc0, const Shape& shape0, Matrix4Val trans, const LandscapeWithColl& geomLand)
{
  CCwithShape geom0(cc0, shape0);

  Vector3 center = trans * cc0.GetCenter();  
  float bRadius = cc0.GetRadius() + epsilonWithGround;

  if (geomLand.MaxHeight() < center[1] - bRadius)
    return; //no collision

  // find shift till is in collision
  IsIntersectionLandConvex<LandscapeWithColl, CCwithShape, Plane, Poly, GeomEdge> doInter;

  Plane sepPlane(VZero, 0);
  float dist; 
  Matrix4 shiftedTrans = trans; 

  float shiftVal = cc0.GetRadius() * 0.1f;

  // start shift maybe it is under land  
  float shift = 0;

  float landY = geomLand.SurfaceY(center[0], center[2]);
  if (landY > center[1])
    shift = landY - center[1];

  shiftedTrans.SetPosition(trans.Position() + shift * VUp);

  if (doInter(geomLand, geom0, shiftedTrans, sepPlane, dist))
  {      
    do 
    {      
      shift += shiftVal;
      shiftedTrans.SetPosition(trans.Position() + shift * VUp); // put it higher 
    } while (doInter(geomLand, geom0, shiftedTrans, sepPlane, dist));
  }
 
  if (sepPlane.Normal().SquareSize() != 0 && dist > epsilonWithGround + shift)
    return; // to big scale ignore collision

  ContactLandConvex<LandscapeWithColl, CCwithShape, Plane, Poly, GeomEdge> doColl;
  CDContactArray found;  
  doColl(found, geomLand, geom0, shiftedTrans, epsilonWithGround + shift);

  FilterLandPairs(ret, found, shift, epsilonWithGround, center, geomLand.NPlanes());
}

void LandIntersectionRB(RBCollisionBuffer& ret, const GeomShapeAnimatedBase& shape, float mass, Matrix4Val trans, const Landscape& land)
{
  PROFILE_SCOPE_EX(odeLI,*);

  Vector3 center = trans * shape.BoundingCenter();  
  float bRadius = shape.BoundingSphere() + epsilonWithGround;
  Vector3 corner(bRadius, bRadius, bRadius);
  LandscapeWithColl geomLand(land);
  if (geomLand.Create( center - corner, center + corner))
    return;

  int n = ret.Size();
  for(int i = 0; i < shape.NGroups(); i++)
  {
    if (!shape.IsEnabled(i))
      continue; 

    Matrix4 transAnim =  shape.GetTransform(i);
    Matrix4 transWhole = transAnim * trans;

    int nGrp = shape.GroupSize(i);
    for(int ii = 0; ii < nGrp; ii++)
    {     
      const ConvexComponent& cc = shape.GetConvexComp(i,ii);
      if (cc.IsEnabled())
      {      
        LandIntersectionRB(ret, shape.GetConvexComp(i,ii), shape.GetShape(), transWhole, geomLand);

        if (n == ret.Size())
          continue;

        float friction; 
        float restitution;  

        // set friction and restitution like a average value between two surfaces
        const SurfaceInfo * surf = cc.GetSurfaceInfo();
        if (surf)
        {
          friction = surf->_friction;
          restitution = surf->_restitution;
        }
        else
        {
          // use default
          friction = g_frictionCoef;   
          restitution = g_restitutionCoef; 
        }

        /// set up surface properties
        for(; n< ret.Size(); n++)
        {
          RBContactPoint& pt = ret[n];
          pt.pos = transAnim * pt.pos;

          pt.dir[0] = transAnim.Rotate(pt.dir[0]);
          pt.dir[1] = transAnim.Rotate(pt.dir[1]);          

          pt.surface.mode = dContactBounce | dContactSoftCFM | dContactApprox1_1 | dContactApprox1_2 | dContactSlip1 | dContactSlip2 | dContactSoftERP ;

          int x = toIntFloor(pt.pos.X() * land.GetInvLandGrid());
          int z = toIntFloor(pt.pos.Z() * land.GetInvLandGrid());
          const Landscape::TextureInfo& info = land.ClippedTextureInfo(z,x);

          pt.surface.mu = (friction + info.Friction()) * 0.5f;
          pt.surface.bounce = (restitution + info.Restitution()) * 0.5f;
          pt.surface.bounce_vel = 0.1f;    

          pt.surface.soft_cfm = g_cfm / mass;
          pt.surface.soft_erp = g_erp ;
          pt.surface.slip1 =  g_cfm / mass;
          pt.surface.slip2 = g_cfm/ mass;
        }
      }
    }
  }
}
#endif
//---------------------------
// Line vs convex component
//---------------------------
#if _ENABLE_OBJECTS_HIERARCHY

void LineIntersection(CollisionBuffer& result, const Shape& shape, const ConvexComponent& cThis, Vector3Val beg, Vector3Val end)
{
  // check intersection will all convex components
  // check line beg..end with all cThis faces
  Vector3 b=beg,e=end;
  float bt=0,et=1;
  // clip the b..e line with all planes of cThis
  Assert( cThis.NPlanes()>=4 );
  Vector3 dirOut = VUp;

  for( int i=0; i<cThis.NPlanes(); i++ )
  {
    const Plane &plane=cThis.GetPlane(&shape,i);
    float distE=plane.Distance(e);
    float distB=plane.Distance(b);
    // dist<0 means point is in outer space
    if( distB<0 && distE<0 )
    {
#if LOG_ISECT
      LogF("  NotInside");
#endif
      return;
    }
    // dist>=0 means point is in inner space
    if( distB>=0 && distE>=0 ) continue; // no clip
    Vector3Val normal=plane.Normal();
    Vector3 bme=b-e;
    float denom=bme*normal;
    if( fabs(denom)<1e-6 )
    {
#if _ENABLE_REPORT
      if( bme.Normalized()*normal<1e-6 )
      {
        RptF(
          "Object::Intersect bme %.2f,%.2f,%.2f normal %.2f,%.2f,%.2f",
          bme.X(),bme.Y(),bme.Z(),
          normal.X(),normal.Y(),normal.Z()
          );
      }
#endif
#if LOG_ISECT
      LogF("  Parallel");
#endif
      continue; // parallel - no clip
    }
    float t=(normal*b+plane.D())/denom;

    // there must be some intersection
    Assert( t>=-1e-3 );
    Assert( t<=1+1e-3 );
    //Vector3 pt=(b-e)*t+e;
    Vector3 pt=(e-b)*t+b;
    Assert( plane.Distance(pt)<1e-3 );
    // note: t is between bt and et
    float tt = bt+t*(et-bt);

    if( distB<0 )
    {
      // b is outside 
      //Assert(t<=et);
      b=pt,bt=tt;
#if LOG_ISECT
      LogF("  setb %.3f",tt);
#endif
      dirOut=normal;
    }
    else
    {
      // e is outside
      Assert(distE<0);
      //Assert(bt<=t);
      e=pt,et=tt;
#if LOG_ISECT
      LogF("  sete %.3f",tt);
#endif
    }

#if 0 //SHOT_STARS
    if( CHECK_DIAG(DECollision) )
    {
      GScene->DrawCollisionStar
        (
        PositionModelToWorld(pt),0.05,PackedColor(Color(0.25,0,0.25))
        );
    }
#endif
  }
  
  // b,e interval is intersection
  /**/
  if (bt>et)
  {
    // there must be some degenerate planes?
    // probably non-convex component?
#if LOG_ISECT
    LogF("  t %.3f..%.3f no hit",bt,et);
#endif
    //continue;
  }
  /**/

  // return collision information
  CollisionInfo &ret=result.Append();
  // check nearest point of intersections
  Vector3 tPoint=b;
  float t=bt;
#if 1
  float maxDist=shape.BSphereRadius()*2;
  if( tPoint.SquareSize()>Square(maxDist) )
  {
#if LOG_ISECT
    LogF("Impossible hit %.3f,%.3f,%.3f",tPoint[0],tPoint[1],tPoint[2]);
#endif
    tPoint=tPoint.Normalized()*maxDist;
  }
#endif

  ret.pos=tPoint;

  // get texture from any face of the component

  ret.texture = cThis.GetTexture();
  /// caution: dirOut and under have different meaning here
  ret.dirOut = e-b;
  ret.dirOutNotNorm = -dirOut;
  ret.under = t;
  // transform into world coord, it can be scaled
  ret.pos = ret.pos; 
  ret.component = cThis.GetNameIndex();
  ret.geomLevel = -1;

#if 0
  Log("  t %.3f..%.3f hit %s",bt,et,(const char *)GetDebugName());
  // calculate distance from beg
  Log("  dist %.3f, %s",tPoint.Distance(begThis)/endThis.Distance(begThis),GetShape()->Name());
#endif
};

//---------------------------
// Line vs shape
//---------------------------
void LineIntersection(CollisionBuffer& ret, const GeomShapeAnimatedBase& collShapeThis, Vector3Val begl, Vector3Val endl, bool onlyWater)
{
  for(int i = 0; i < collShapeThis.NGroups(); i++)
  {
    if (!collShapeThis.IsEnabled(i))
      continue; 

    Matrix4 invTransAnim = collShapeThis.GetInvTransform(i);

    Vector3 beg = invTransAnim * begl; 
    Vector3 end = invTransAnim * endl; 

    int n = ret.Size();
    // test against group obb
    RefArray<OBBNodeS> who;
    collShapeThis.BoundingTree(i)->RayDeep(beg,end,who);

    for(int j = 0; j < who.Size(); j++)
    {
      const ConvexComponent& cc = *(who[j]->GetComponnent());
      if (!cc.IsEnabled())
        continue;

#if _ENABLE_WALK_ON_GEOMETRY
      if (onlyWater && (!cc.GetTexture() || (!cc.GetTexture()->IsWater() && cc.GetTexture() != GLandscape->GetSeaTexture())))
        continue; 
#endif

      // collide convex component
      LineIntersection(ret,collShapeThis.GetShape(), cc,beg,end);
    }
 
    if (n < ret.Size())
    {    
      Matrix4 transAnim = collShapeThis.GetTransform(i);
      for(;n < ret.Size();n++)
      {
        CollisionInfo &info = ret[n];
        info.pos = transAnim * info.pos;
        info.dirOut = transAnim.Rotate(info.dirOut);
        info.dirOutNotNorm = transAnim.Rotate(info.dirOutNotNorm);
      }
    }
  }
}

//---------------------------
// // AABB vs OBB
//---------------------------

bool HasIntersection(Vector3 minMax0[], Matrix4Par trans0to1,Vector3 minMax1[])
{
  Vector3 center0 = trans0to1 * ((minMax0[1] + minMax0[0])* 0.5f);
  Vector3 center1 = (minMax1[1] + minMax1[0])* 0.5f;

  Vector3 h0 = (minMax0[1] - minMax0[0]) * 0.5f;
  Vector3 h1 = (minMax1[1] - minMax1[0]) * 0.5f;

  Vector3 vD = center1 - center0; 

  float R, Rab;

  // Separation this.u:
  float uu = trans0to1(0,0);//float uu = trans0to1.DirectionAside() * Vector3(1,0,0);
  float uv = trans0to1(1,0); //float uv = trans0to1.DirectionAside() * Vector3(0,1,0);
  float uw = trans0to1(2,0); //float uw = trans0to1.DirectionAside() * Vector3(0,0,1);

  float auu = abs(uu);
  float auv = abs(uv);
  float auw = abs(uw);

  R = abs(trans0to1.DirectionAside() * vD);
  Rab = h1[0] * auu + h1[1] * auv + h1[2] * auw;
  if ( R > h0[0] + Rab ) return false;

  // Separation this.v:
  float vu = trans0to1(0,1);
  float vv = trans0to1(1,1);
  float vw = trans0to1(2,1);

  float avu = abs(vu);
  float avv = abs(vv);
  float avw = abs(vw);

  R = abs(trans0to1.DirectionUp() * vD);
  Rab = h1[0] * avu + h1[1] * avv + h1[2] * avw;
  if ( R > h0[1] + Rab ) return false;

  // Separation this.w:
  float wu = trans0to1(0,2);
  float wv = trans0to1(1,2);
  float ww = trans0to1(2,2);

  float awu = abs(wu);
  float awv = abs(wv);
  float aww = abs(ww);

  R = abs(trans0to1.Direction() * vD);
  Rab = h1[0] * awu + h1[1] * awv + h1[2] * aww;
  if ( R > h0[2] + Rab ) return false;

  // Separation b.u:
  R = abs(vD[0]);
  Rab = h0[0] * auu + h0[1] * avu + h0[2] * awu;
  if ( R > h1[0] + Rab ) return false;

  // Separation b.v:
  R = abs(vD[1]);
  Rab = h0[0] * auv + h0[1] * avv + h0[2] * awv;
  if ( R > h1[1] + Rab ) return false;

  // Separation b.w:
  R = abs(vD[2]);
  Rab = h0[0] * auw + h0[1] * avw + h0[2] * aww;
  if ( R > h1[2] + Rab ) return false;

  // b->u -> b->v -> b->w -> b->u
  // Separating this.u x b.u:

  R = abs(vD * trans0to1.DirectionAside().CrossProduct(Vector3(1,0,0)));
  Rab = h0[1] * awu + h0[2] * avu + h1[1] * auw + h1[2] * auv;  
  if ( R > Rab ) return false;

  // Separating this.u x b.v:
  R = abs(vD * trans0to1.DirectionAside().CrossProduct(Vector3(0,1,0)));
  Rab = h0[1] * awv + h0[2] * avv + h1[0] * auw + h1[2] * auu;  
  if ( R > Rab ) return false;

  // Separating this.u x b.w:
  R = abs(vD * trans0to1.DirectionAside().CrossProduct(Vector3(0,0,1)));
  Rab = h0[1] * aww + h0[2] * avw + h1[0] * auv + h1[1] * auu;  
  if ( R > Rab ) return false;

  // Separating this.v x b.u:
  R = abs(vD * trans0to1.DirectionUp().CrossProduct(Vector3(1,0,0)));
  Rab = h0[0] * awu + h0[2] * auu + h1[1] * avw + h1[2] * avv;  
  if ( R > Rab ) return false;

  // Separating this.v x b.v:
  R = abs(vD * trans0to1.DirectionUp().CrossProduct(Vector3(0,1,0)));
  Rab = h0[0] * awv + h0[2] * auv + h1[0] * avw + h1[2] * avu;  
  if ( R > Rab ) return false;

  // Separating this.v x b.w:
  R = abs(vD * trans0to1.DirectionUp().CrossProduct(Vector3(0,0,1)));
  Rab = h0[0] * aww + h0[2] * auw + h1[0] * avv + h1[1] * avu;  
  if ( R > Rab ) return false;

  // Separating this.w x b.u:
  R = abs(vD * trans0to1.Direction().CrossProduct(Vector3(1,0,0)));
  Rab = h0[0] * avu + h0[1] * auu + h1[1] * aww + h1[2] * awv;  
  if ( R > Rab ) return false;

  // Separating this.w x b.v:
  R = abs(vD * trans0to1.Direction().CrossProduct(Vector3(0,1,0)));
  Rab = h0[0] * avv + h0[1] * auv + h1[0] * aww + h1[2] * awu;  
  if ( R > Rab ) return false;

  // Separating this.w x b.w:
  R = abs(vD * trans0to1.Direction().CrossProduct(Vector3(0,0,1)));
  Rab = h0[0] * avw + h0[1] * auw + h1[0] * awv + h1[1] * awu;  
  if ( R > Rab ) return false;
  return true;
}

//---------------------------
// Object vs Object
//---------------------------

bool CalculateIntersectionsExactEx
(
 CollisionBuffer &result, const Object *thisObj,
 const Shape *sThis, const Shape *sWith,
 const ConvexComponent &cThis, const ConvexComponent &cWith,
 Matrix4Val thisToWith, Matrix4Val withToThis,
 Matrix4Val thisToWorld,
 int hierLevel
 );

bool CalculateIntersectionsExact
(
 CollisionBuffer &result, const Object *thisObj,
 const Shape *sThis, const Shape *sWith,
 const ConvexComponent &cThis, const ConvexComponent &cWith,
 Matrix4Val thisToWith, Matrix4Val withToThis,
 Matrix4Val thisToWorld,
 int hierLevel
 );

void GeomIntersection(CollisionBuffer& ret,const GeomShapeAnimatedBase& cc0, Matrix4Par trans0to1, const GeomShapeAnimatedBase& cc1, bool person)
{
  int n = ret.Size();
 
  Matrix4 trans1to0 = trans0to1.InverseGeneral();
  // test every x every component for now. 
  for(int i = 0; i < cc0.NGroups(); i++)
  {      
    if (!cc0.IsEnabled(i))
      continue; 

    Matrix4Val trans0 = cc0.GetTransform(i);
    Matrix4 trans0Anim = trans0to1 * trans0;    
    Matrix4 trans0AnimInv = cc0.GetInvTransform(i) * trans1to0;    

    for(int j = 0; j < cc1.NGroups(); j++)
    {
      if (!cc1.IsEnabled(j))
        continue; 

      Matrix4 trans0to1Anim = cc1.GetInvTransform(j) * trans0Anim;
      Matrix4 trans1to0Anim = trans0AnimInv * cc1.GetTransform(j);

      // BBTree vs BBTree
      AUTO_STATIC_ARRAY(const OBBNodeS * ,colliding, 128);

      OBBNodeS * bbTree0 = cc0.BoundingTree(i);
      OBBNodeS * bbTree1 = cc1.BoundingTree(j);

      bbTree0->DeepVsDeep(bbTree1, trans0to1Anim, colliding);

      int nColliding = colliding.Size() / 2;
      for(int k = 0; k < nColliding; k++)
      {              
        const ConvexComponent& ccc0 = *(colliding[2 * k]->GetComponnent());
        const ConvexComponent& ccc1 = *(colliding[2 * k + 1]->GetComponnent());

        if (!ccc0.IsEnabled() || !ccc1.IsEnabled())
          continue;
                   
        if ( person )
        {
          // change cc0 <-> cc1 person must be always second

          CalculateIntersectionsExactEx(
          ret,NULL,
          &cc1.GetShape(),&cc0.GetShape(),
          ccc1,ccc0,trans1to0Anim, trans0to1Anim,
          M4Zero,0
          );


          if ( n < ret.Size() )
          {
            int component = ccc1.GetNameIndex() * 100000 +  ccc0.GetNameIndex();  
            Matrix4  trans = trans1to0 * cc1.GetTransform(j);

            for(; n < ret.Size(); n++)
            {
              CollisionInfo& info = ret[n];
              info.component = component;
              info.geomLevel = -1;

              info.dirOut *= info.under;
              info.dirOut = -trans.Rotate(info.dirOut);
              info.under = info.dirOut.Size();
              info.dirOut /= (info.under != 0) ? info.under : 1;
              info.pos = trans * info.pos;+
            }
          } 
        }
        else
        {

          CalculateIntersectionsExact(
          ret,NULL,
          &cc0.GetShape(),&cc1.GetShape(),
          ccc0,ccc1,trans0to1Anim,trans1to0Anim,
          M4Zero,0
          );

          if ( n < ret.Size() )
          {
            int component = ccc0.GetNameIndex() * 100000 +  ccc1.GetNameIndex();  
           
            for(; n < ret.Size(); n++)
            {
              CollisionInfo& info = ret[n];
              info.component = component;
              info.geomLevel = -1;

              info.dirOut *= info.under;
              info.dirOut = -trans0.Rotate(info.dirOut);
              info.under = info.dirOut.Size();
              info.dirOut /= (info.under != 0) ? info.under : 1;
              info.pos = trans0 * info.pos;
            }
          } 
        }
      }
    }
  }
}

//---------------------------
// Object vs Object how long distance till first touch
//---------------------------
int CalculateFirstTouchExact
(
 float &fDoneFrac, Vector3& cNormal, /*const Object *thisObj,*/
 const Shape *sThis, const Shape *sWith,
 const ConvexComponent &cThis, const ConvexComponent &cWith,
 Matrix4Val thisToWith, Matrix4Val withToThis,
 Vector3Val cDeltaPosWith 
 );

int FirstTouch(float &doneFrac, Vector3& normal,const GeomShapeAnimatedBase& cc0, Matrix4Par trans0to1, const GeomShapeAnimatedBase& cc1, Vector3Val delta)
{
  int ret = 0;

  Matrix4 trans1to0 = trans0to1.InverseGeneral();
  // test every x every component for now. 
  for(int i = 0; i < cc0.NGroups(); i++)
  {      
    if (!cc0.IsEnabled(i))
      continue; 

    Matrix4Val trans0 = cc0.GetTransform(i);
    Matrix4 trans0Anim = trans0to1 * trans0;    
    Matrix4 trans0AnimInv = cc0.GetInvTransform(i) * trans1to0;  

    Vector3 delta0 = cc0.GetInvTransform(i) * delta;
    // create bounding sphere of both current and shifted body by delta
    OBBNodeS * bbTree0 = cc0.BoundingTree(i);
  
    for(int j = 0; j < cc1.NGroups(); j++)
    {
      if (!cc1.IsEnabled(j))
        continue; 

      Matrix4 trans0to1Anim = cc1.GetInvTransform(j) * trans0Anim;
      Matrix4 trans1to0Anim = trans0AnimInv * cc1.GetTransform(j);

      // BBTree vs BBTree
      // I have no test for BBTree Vs BBTree so I will simple test between sphere

      RefArray<OBBNodeS> colliding;      
      OBBNodeS * bbTree1 = cc1.BoundingTree(j);

      bbTree0->DeepStrechedVsDeep(delta0, bbTree1, trans0to1Anim, colliding);

      int nColliding = colliding.Size() / 2;
      for(int k = 0; k < nColliding; k++)
      {              
        const ConvexComponent& ccc0 = *(colliding[2 * k]->GetComponnent());
        const ConvexComponent& ccc1 = *(colliding[2 * k + 1]->GetComponnent());

        if (!ccc0.IsEnabled() || !ccc1.IsEnabled())
          continue;

        float doneFracNew = 1;
        Vector3 normalNew;

        int returnTemp = CalculateFirstTouchExact(
          doneFracNew, normalNew,
          &cc1.GetShape(),&cc0.GetShape(),
          ccc1,ccc0,trans1to0Anim,trans0to1Anim,
          delta0
          );

        if (doneFrac > doneFracNew)
        {
          doneFrac = doneFracNew;
          normal = trans0 * normalNew;
          ret = returnTemp;
        }
      }
    }
  }

  return ret;
}


#endif //_ENABLE_OBJECTS_HIERARCHY