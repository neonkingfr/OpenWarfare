#ifndef __INTERSECTLANDCONVEX_HPP_
#define __INTERSECTLANDCONVEX_HPP_

template <class Land, class Geom, class Plane, class Face, class Edge>
class IsIntersectionLandConvex
{
protected:
  bool IsSeparationPlane(typename const Plane& sepPlane, Matrix4Val trans, const typename Geom& geom1, float& dist);
public:
  bool operator () ( const typename Land& land, const typename Geom& geom, Matrix4 trans, typename Plane& sepPlane, float& dist);
};

template <class Land, class Geom, class Plane, class Face, class Edge>
bool IsIntersectionLandConvex<Land,Geom,Plane,Face,Edge>::IsSeparationPlane(typename const Plane& sepPlane, Matrix4Val trans, const typename Geom& geom, float& dist)
{
  Plane sepPlaneTrans(sepPlane);
  sepPlaneTrans.Transform(trans);

  dist = FLT_MAX;
  for(int i = 0; i < geom.Size(); i++)
  {
    float actDist = sepPlaneTrans.Distance(geom.Get(i));
    if (actDist <= 0)
    {
      dist = actDist;
      return false;
    }
    else
      if (actDist < dist)
        dist = actDist;
  }
  return true;
}

template <class Land, class Geom, class Plane, class Face, class Edge>
bool IsIntersectionLandConvex<Land,Geom,Plane,Face,Edge>::operator () ( const typename Land& land, const typename Geom& geom, Matrix4 trans, typename Plane& sepPlane, float& dist)
{
  // are land faces separation planes?
  dist = FLT_MAX;
  int i = 0;
  for(; i < land.NPlanes(); i++)
  {
    const typename Plane& candidate = land.GetPlane(i);
    float distL;
    if (!IsSeparationPlane(candidate, trans, geom, distL) || distL < NUMERICAL_ERROR)
    {      
      break;
    }

    if (distL < dist)
    {
      dist = distL;
      sepPlane = candidate;
    }
  }

  if (i == land.NPlanes())
    return false;

  dist = -1;

  // check edges vs triangle
  for(; i < land.NPlanes(); i++)
  {
    const typename Plane& plane = land.GetPlane(i);
    const typename Face& face = land.GetFace(i);
    float dist;
    if (IsSeparationPlane(plane, trans, geom, dist) && dist > NUMERICAL_ERROR)      
      continue;

    for(int j = 0; j < geom.NEdges(); j++)
    {
      const typename Edge& edge = geom.GetEdge(j);   
      Vector3 pos0 = trans * geom.Get(edge.GetVertex(0));
      Vector3 pos1 = trans * geom.Get(edge.GetVertex(1));
      float fDist0 = plane.Distance(pos0);
      float fDist1 = plane.Distance(pos1);

      if (fDist0 * fDist1 > 0)
        continue; // edge is on one side of the plane

      Vector3 intersectionPoint = fabs(fDist1) * pos0 + fabs(fDist0) * pos1;
      intersectionPoint /= fabs(fDist0) + fabs(fDist1);

      // is point over face?            
      int l = 0;
      for(; l < face.N(); l++)
      {
        Vector3Val vert1 = land.GetVertex(face.GetVertex(l));
        Vector3 edgeDir = vert1 - land.GetVertex(face.GetVertex((l==0)?face.N()-1:l-1));
        Vector3 normal = plane.Normal().CrossProduct(edgeDir);

        if (normal * (intersectionPoint - vert1) <= 0)
          break;
      }

      // intersection point found
      if (l == face.N())      
        return true;      
    }
  }

  // TODO still can be total under
  return false;
};


#endif