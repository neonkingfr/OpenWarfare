#ifndef __CONTACTLANDCONVEX_HPP_
#define __CONTACTLANDCONVEX_HPP_

#include "ContactConvex.hpp"

template <class Land, class Geom, class Plane, class Face, class Edge>
class ContactLandConvex
{
protected:
public:
  void operator() (CDContactArray& found, const Land& land, const Geom& geom, Matrix4 trans, float epsilon);
};

template <class Land, class Geom, class Plane, class Face, class Edge>
void ContactLandConvex<Land, Geom, Plane, Face, Edge>::operator() 
(CDContactArray& found, const Land& land, const Geom& geom, Matrix4 trans, float epsilon)
{
  // Point to face
  for(int i = 0; i < land.NPlanes(); i++)
  {
    //PROFILE_SCOPE(plane1);
    typename const Face& face = land.GetFace(i);
    typename Plane plane =  land.GetPlane(i);

    for (int j = 0; j < geom.Size(); j++)
    {
      Vector3 point = trans * geom.Get(j);
      float dist = plane.Distance(point);
      if (dist < 0 || dist > epsilon)
        continue;
      
      // is point over face?      
      int l = 0;
      for(; l < face.N(); l++)
      {
        Vector3Val pos1 = land.GetVertex(face.GetVertex(l));
        Vector3 edgeDir = pos1 - land.GetVertex(face.GetVertex((l==0)?face.N()-1:l-1));
        Vector3 normal = plane.Normal().CrossProduct(edgeDir);

        if (normal * (point - pos1) <= 0)
          break;
      }

      if (l == face.N())
      { // found point
        CDContactPair& pair = found.Append(); 
        pair._norm = plane.Normal();
        pair._pos = point - plane.Normal() * dist;
        pair._dist = dist;  
        pair._triangle[0] = i;
        pair._triangle[1] = -1;
      }
    }
  }

  // Edge to edge
  for(int i = 0; i < land.NEdges(); i++)
  {
    //PROFILE_SCOPE(edge);
    typename const Edge& cand1 = land.GetEdge(i);

    const typename Plane& plane11 = land.GetPlane(cand1.GetFace(0));

    if (plane11.Normal() * land.GetPlane(cand1.GetFace(1)).Normal() > 0.999f)    
      continue; // not real edge    

    Vector3Val pos11 = land.GetVertex(cand1.GetVertex(0));
    Vector3Val pos12 = land.GetVertex(cand1.GetVertex(1));  

    Vector3 v1 = pos12 - pos11;
    v1.Normalize();

    for(int j = 0; j < geom.NEdges(); j++)
    {
      typename const Edge& cand2 = geom.GetEdge(j);
      const Plane& plane21 = geom.GetPlane(cand2.GetFace(0));      
      const Plane& plane22 = geom.GetPlane(cand2.GetFace(1));

      if (plane21.Normal() * plane22.Normal() > 0.999f)
        continue; // not real edge      

      Vector3Val pos21 = trans * geom.Get(cand2.GetVertex(0));
      Vector3Val pos22 = trans * geom.Get(cand2.GetVertex(1));
      
      float dist1 = plane11.Distance(pos21);
      float dist2 = plane11.Distance(pos22);

      if (dist1 * dist2 > 0 && fabs(dist1) > epsilon && fabs(dist2) > epsilon)
        continue;      

      Vector3 v2 = pos22 - pos21;
      v2.Normalize();

      if ((v1 * (pos21 - pos11) <= 0 && v1 * (pos22 - pos11) <= 0) ||
        (v1 * (pos21 - pos12) >= 0 && v1 * (pos22 - pos12) >= 0))
        continue;

      if ((v2 * (pos11 - pos21) <= 0 && v2 * (pos12 - pos21) <= 0) ||
        (v2 * (pos11 - pos22) >= 0 && v2 * (pos12 - pos22) >= 0))
        continue;

      Vector3 normal = (v1).CrossProduct(v2);
      float sqrSize = normal.SquareSize();
      if (sqrSize < 0.000001f) // numerical barier vectors are paralel
      {
        //parallel
        normal = (pos21 - pos11);
        normal = normal - v1 * (normal * v1);

        float dist = normal.Size();

        normal /= dist;
        if (dist > epsilon)
          continue;        

        float proj11 = v1 * pos11;
        float proj12 = v1 * pos12;

        float proj21 = v1 * pos21;
        float proj22 = v1 * pos22;

        if (proj21 < proj22)
        {
          if (proj11 != proj21)
          {
            CDContactPair& pair = found.Append(); 
            pair._norm = normal;        
            pair._dist = dist;
            pair._triangle[0] = cand1.GetFace(0);
            pair._triangle[1] = cand1.GetFace(1);

            if (proj21 > proj11)
              pair._pos = pos21 - normal * dist;
            else
              pair._pos = pos11;
          }

          CDContactPair& pair2 = found.Append(); 
          pair2._norm = normal;        
          pair2._dist = dist;
          pair2._triangle[0] = cand1.GetFace(0);
          pair2._triangle[1] = cand1.GetFace(1);

          if (proj22 < proj12)
            pair2._pos = pos22 - normal * dist;
          else
            pair2._pos = pos12;
        }
        else
        {
          if (proj11 != proj22)
          {
            CDContactPair& pair = found.Append(); 
            pair._norm = normal;        
            pair._dist = dist;
            pair._triangle[0] = cand1.GetFace(0);
            pair._triangle[1] = cand1.GetFace(1);

            if (proj22 > proj11)
              pair._pos = pos22 - normal * dist;
            else
              pair._pos = pos11;
          }

          CDContactPair& pair2 = found.Append(); 
          pair2._norm = normal;        
          pair2._dist = dist;
          pair2._triangle[0] = cand1.GetFace(0);
          pair2._triangle[1] = cand1.GetFace(1);

          if (proj21 < proj12)
            pair2._pos = pos21 - normal * dist;
          else
            pair2._pos = pos12;
        }

        continue;
      }

      normal.Normalize();
      float dist = normal * (pos21 - pos11);
      if (dist < 0)
      {
        normal *= -1;
        dist *= -1;
      }

      if (dist > epsilon)
        continue;

      Vector3 pos2 = pos21 - normal * dist;

      // diffnorm * v1 = 0 && diffnorm belongs to plane def by v1 and v2
      Vector3 diffNorm = v2 - v1 * (v1 * v2);

      float t = (pos11 - pos2) * diffNorm / (v2 * diffNorm);

      if (t < 0 || t > (pos22 - pos21) * v2)
        continue; // out of range

      Vector3 resPos1 = v2 * t + pos2;
      float t1 = (resPos1 - pos11) * v1;

      if (t1 < 0 || t1 > (pos12 - pos11) * v1)
        continue; // out of range

      CDContactPair& pair = found.Append(); 
      pair._norm = normal;
      pair._pos = resPos1;
      pair._dist = dist; 
      pair._triangle[0] = cand1.GetFace(0);
      pair._triangle[1] = cand1.GetFace(1);
    }
  }
}
#endif