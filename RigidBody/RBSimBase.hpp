#ifdef _MSC_VER
#pragma once
#endif

#ifndef __RBSIMBASE_HPP__
#define __RBSIMBASE_HPP__

//class IRBGeometry;
//class IRigidBody;
#include <El/Math/math3d.hpp>
//#include "BaseGeometry.h"
#include "RigidBody.h"
#include <El/ParamArchive/paramArchive.hpp>


/*class RigidBodyObject : public RefCount
{      
public:
  IRBGeometry * _geometry; // TODO: redundancy later will be only _treeNode;
  IRigidBody * _physBody;  
  bool _static;
  bool _collideWithLandscape;
  bool _collideWithBodies;

  RigidBodyObject(): _geometry(NULL), _physBody(NULL), _static(false), _collideWithLandscape(true), _collideWithBodies(true) {};
  RigidBodyObject(const RigidBodyObject& src) {*this = src;};

  virtual ~RigidBodyObject() {};
};*/

enum CollEnableType
{ 
  CET_LAND,
  CET_BODIES
};

class LODShape;
class Landscape;

//#include "jointrb.hpp"
class HierarchyID;
template <class TSSim>
class RBSimBase: private TSSim /*: public RefCount*/ // RBSimBase is referenced only in World by SRef
{
public:
  virtual ~RBSimBase() {};
  // init & deinit
  bool Init() {return TSSim::Init();};
  void DeInit() {TSSim::DeInit();};

  // How many bodies can be added without performance loss
  float GetFreeResource(Vector3Val pos) const {return TSSim::GetFreeResource(pos);};  

 // bool AddLandscape(const Landscape& land) {return TSSim::AddLandscape(land);};

  // simulate
  bool Simulate(FindArray<HierarchyID> grps, float time) {return TSSim::Simulate(grps,time);};

  // Serialize
 // LSError Serialize(ParamArchive& ar) {return TSSim::Serialize(ar);};
  
};

#endif // __RBSIMBASE_HPP__