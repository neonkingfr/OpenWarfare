
#include <Poseidon/lib/wpch.hpp>

#if _ENABLE_RIGID_BODIES
#include <Poseidon/lib/global.hpp>

#include "RBSimBase.hpp"
//#include "Meqon/MeqonSim.hpp"
#include "NovodeX/NovodexSim.hpp"
#include "Marina/MarinaSim.hpp"
#include "ODE/ODESim.hpp"


RBSimBase * AllocRigidBodySim()
{
  if (Glob.config.physEngine == RString("NovodeX"))
    return new NovodexSim();
  if (Glob.config.physEngine == RString("NovodeXBox"))
    return new NovodexBoxSim();
  if (Glob.config.physEngine == RString("Marina"))
    return new MarinaSim();
  //if (Glob.config.physEngine == RString("MeqonBox"))
  //  return new MeqonBoxSim();
  //if (Glob.config.physEngine == RString("Meqon"))
  //  return new MeqonSim();
  if (Glob.config.physEngine == RString("ODE"))
    return new ODESim();

  return new ODESim();
}

#endif