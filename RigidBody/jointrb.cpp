#include <Poseidon/lib/wpch.hpp>

#if _ENABLE_RIGID_BODIES

#include <El/Common/randomGen.hpp>
#include <Poseidon/lib/object.hpp>
#include <Poseidon/lib/scene.hpp>
#include <Poseidon/lib/diagmodes.hpp>
#include "jointrb.hpp"
#include "ode/include/ode.h"
#include "ode/include/odemath.h"


DEFINE_CASTING(JointWithIRigidBody);

//------------------------------------
// ContactJointRB
//------------------------------------
DEFINE_FAST_ALLOCATOR(ContactJointRB);
DEFINE_CASTING(ContactJointRB)

void ContactJointRB::Create(dxWorld * world, const RBContactPoint& contact)
{
  _bounce = false; 
  _world = world;
  _contact = contact;
  Assert(_contact.body1.IsRegistered());
  Assert(!(bool)_contact.body2 || _contact.body2.IsRegistered());    
}

void ContactJointRB::SetIRigidBodies(IDtoIRigidBody& src)
{
  Assert((bool)_contact.body1);
  _body[0] = src.GetIRigidBody(_contact.body1);
  _body[0]->AddJointToStorage(this);

  if (_contact.body2)  
  {
    _body[1] = src.GetIRigidBody(_contact.body2);
    _body[1]->AddJointToStorage(this);
  }
}

void ContactJointRB::GetInfo1(Info1 *info)
{
  // make sure mu's >= 0, then calculate number of constraint rows and number
  // of unbounded rows.
  int m = 0, nub=0;
  if (!(_contact.surface.mode & dContactNormalConst)) m++;
  if (_contact.surface.mu < 0) _contact.surface.mu = 0;
  if (_contact.surface.mode & dContactMu2) 
  {
    if (_contact.surface.mu > 0) m++;
    if (_contact.surface.mu2 < 0) _contact.surface.mu2 = 0;
    if (_contact.surface.mu2 > 0) m++;
    if (_contact.surface.mu  == FLT_MAX) nub ++;
    if (_contact.surface.mu2 == FLT_MAX) nub ++;
  }
  else 
  {
    if (_contact.surface.mu > 0) m += 2;
    if (_contact.surface.mu == FLT_MAX) nub += 2;
  }
  
  _the_m = m;
  info->m = m;
}

void ContactJointRB::GetInfo2(Info2 *info)
{
  int s = info->rowskip;
  int s2 = 2*s;
  if (_contact.surface.mode & dContactNormalConst)
  {
    s = 0;
    s2 = info->rowskip;
  }
  int index = 0; 

  Vector3Val normal = -_contact.dir[0];

  Ref<IRigidBody> body1 = GetIRigidBody(0);
  Ref<IRigidBody> body2 = GetIRigidBody(1);
  bool nonstaticBody2 = body2 && !body2->IsStatic();
   
  Vector3 c1 = _contact.pos - body1->GetMassCenter();
  //Vector3Val massCenter = node[0].body->_physBody->GetMassCenter();  
  Vector3 c2;

  if (!(_contact.surface.mode & dContactNormalConst))
  {      
    // set Jacobian for normal

    info->J1l[0] = normal[0];
    info->J1l[1] = normal[1];
    info->J1l[2] = normal[2];
    dCROSS (info->J1a,=,c1,normal);
    if (nonstaticBody2) {
      c2 = _contact.pos - body2->GetMassCenter();      

      info->J2l[0] = -normal[0];
      info->J2l[1] = -normal[1];
      info->J2l[2] = -normal[2];
      dCROSS (info->J2a,= -,c2,normal);
    }

    // set right hand side and cfm value for normal
    float erp = info->erp;
    if (_contact.surface.mode & dContactSoftERP)
      erp = _contact.surface.soft_erp;
    float k = info->fps * erp;
    float depth = _contact.under - _world->contactp.min_depth;
    if (depth < 0) depth = 0;
    float maxvel = _world->contactp.max_vel;
    if (k*depth > maxvel) info->c[0] = maxvel; else info->c[0] = k*depth;
    if (_contact.surface.mode & dContactSoftCFM)
      info->cfm[0] = _contact.surface.soft_cfm;   

    // deal with bounce
    if (_contact.surface.mode & dContactBounce) 
    {    
      // calculate outgoing velocity (-ve for incoming _contact)
      Vector3 lvel0 = body1->GetVelocity();
      Vector3 avel0 = body1->GetAngularVelocity();   

      float outgoing = dDOT(info->J1l,lvel0) +
        dDOT(info->J1a,avel0);

      if (nonstaticBody2) 
      {
        Vector3Val lvel1 = body2->GetVelocity();
        Vector3Val avel1 = body2->GetAngularVelocity();        

        outgoing += dDOT(info->J2l,lvel1) +
          dDOT(info->J2a,avel1);
      }
      // only apply bounce if the outgoing velocity is greater than the
      // threshold, and if the resulting c[0] exceeds what we already have.
      if (_contact.surface.bounce_vel >= 0 &&
        (-outgoing) > _contact.surface.bounce_vel) 
      {
          _bounce = true;
          float newc = - _contact.surface.bounce * outgoing;
          if (newc > info->c[0])      
            info->c[0] = newc;
        }
    }

    // set LCP limits for normal
    info->lo[0] = 0;
    info->hi[0] = dInfinity;

    index++;
  }

  // now do Jacobian for tangential forces
  Vector3 t1,t2;	// two vectors tangential to normal

  // first friction direction
  if (_the_m >= 2 || (_the_m >= 1 && _contact.surface.mode & dContactNormalConst))
  {   
    t1 = -_contact.dir[1];
    t2 = -_contact.dir[0].CrossProduct(_contact.dir[1]);
    
    info->J1l[s+0] = t1[0];
    info->J1l[s+1] = t1[1];
    info->J1l[s+2] = t1[2];
    dCROSS (info->J1a+s,=,c1,t1);
    if (nonstaticBody2) 
    {
      info->J2l[s+0] = -t1[0];
      info->J2l[s+1] = -t1[1];
      info->J2l[s+2] = -t1[2];
      dCROSS (info->J2a+s,= -,c2,t1);
    }
    // set right hand side
    if (_contact.surface.mode & dContactMotion1) {
      info->c[index] = _contact.surface.motion1;
    }
    // set LCP bounds and friction index. this depends on the approximation
    // mode
    if ( _contact.surface.mode & dContactNormalConst)
    {
      info->findex[index] = -1;
      info->lo[index] = -_contact.surface.normalF * _contact.surface.mu;
      info->hi[index] = _contact.surface.normalF * _contact.surface.mu;
    }
    else
    {
      info->lo[index] = -_contact.surface.mu;
      info->hi[index] = _contact.surface.mu;
      if (_contact.surface.mode & dContactApprox1_1) info->findex[index] = 0;
    }

    // set slip (constraint force mixing)
    if (_contact.surface.mode & dContactSlip1)
      info->cfm[index] = _contact.surface.slip1;
    index++;
  }

  // second friction direction
  if (_the_m >= 3 || (_the_m >= 2 && _contact.surface.mode & dContactNormalConst)) {
    info->J1l[s2+0] = t2[0];
    info->J1l[s2+1] = t2[1];
    info->J1l[s2+2] = t2[2];
    dCROSS (info->J1a+s2,=,c1,t2);
    if (nonstaticBody2) {
      info->J2l[s2+0] = -t2[0];
      info->J2l[s2+1] = -t2[1];
      info->J2l[s2+2] = -t2[2];
      dCROSS (info->J2a+s2,= -,c2,t2);
    }
    // set right hand side
    if (_contact.surface.mode & dContactMotion2) {
      info->c[index] = _contact.surface.motion2;
    }
    // set LCP bounds and friction index. this depends on the approximation
    // mode
    if ( _contact.surface.mode & dContactNormalConst)
    {
      info->findex[index] = -1;
      if (_contact.surface.mode & dContactMu2) {                
        info->lo[index] = -_contact.surface.normalF * _contact.surface.mu2;
        info->hi[index] = _contact.surface.normalF * _contact.surface.mu2;
      } else {
        info->lo[index] = -_contact.surface.normalF * _contact.surface.mu;
        info->hi[index] = _contact.surface.normalF * _contact.surface.mu;
      }
    }
    else
    {
      if (_contact.surface.mode & dContactMu2) {
        info->lo[index] = -_contact.surface.mu2;
        info->hi[index] = _contact.surface.mu2;
      }
      else {
        info->lo[index] = -_contact.surface.mu;
        info->hi[index] = _contact.surface.mu;
      }
      if (_contact.surface.mode & dContactApprox1_2) info->findex[index] = 0;
    }

    // set slip (constraint force mixing)
    if (_contact.surface.mode & dContactSlip2)
      info->cfm[index] = _contact.surface.slip2;
  }


  //backup current Jacobian
  /*for(int i = 0; i < _the_m; i++)
  {  
  memcpy(_jacobian + i * 12, info->J1l + i * s, 3 * sizeof(*info->J1l));
  memcpy(_jacobian + 3 + i * 12, info->J1a + i * s, 3 * sizeof(*info->J1l));
  memcpy(_jacobian + 6 + i * 12, info->J2l + i * s, 3 * sizeof(*info->J1l));
  memcpy(_jacobian + 9 + i * 12, info->J2a + i * s, 3 * sizeof(*info->J1l));
  }*/

  //optimalized version
  //memcpy(_jacobian, info->J1l, sizeof(*info->J1l) * _the_m * info->rowskip);
}


void ContactJointRB::SetResult(float * lambda)
{
  memcpy(_lambda, lambda,_the_m * sizeof(*lambda));  
}

// from body 0 to body 1
Vector3 ContactJointRB::GetForceInJoint() const
{
  Vector3 res(VZero);
  int index = 0; 
  if (_contact.surface.mode & dContactNormalConst)
  {
    res = _contact.dir[0] * _contact.surface.normalF;    
  }
  else
  {
    res = _contact.dir[0] * _lambda[0];
    index++;
  }
  
  if (_the_m - index > 0)
  {
    res += _contact.dir[1] * _lambda[index];
    index++;
  }

  if (_the_m - index > 0)
  {
    res += _contact.dir[0].CrossProduct(_contact.dir[1]) * _lambda[index];
    index++;
  }

  return res;
}

Vector3 ContactJointRB::GetTorqueOnBody(int i) const
{
  // torque is created by force in joint at contact
  Ref<IRigidBody> body = GetIRigidBody(i);
  if (!body)  
    return VZero;   

  Vector3 force = GetForceInJoint();
  if ( i == 0)  
    force *= -1;  

  Vector3 c = _contact.pos - body->GetMassCenter();
  return c.CrossProduct(force);
}

#if _ENABLE_CHEATS
void ContactJointRB::DiagDraw()
{  
  if (CHECK_DIAG(DEContactJoints))
  {  
    if (IsImpulse())
    {  
      GScene->DrawDiagArrow(_contact.pos, _contact.dir[0],  1, PackedColor(Color(1,0,0)));
      GScene->DrawDiagArrow(_contact.pos, -_contact.dir[0],  1, PackedColor(Color(1,0,0)));
    }
    else
    {
      GScene->DrawDiagArrow(_contact.pos, _contact.dir[0], 1, PackedColor(Color(0,1,0)));
      GScene->DrawDiagArrow(_contact.pos, -_contact.dir[0],  1, PackedColor(Color(0,1,0)));
    }
  }
}
#endif

//------------------------------------
// JointLimitMotor
//------------------------------------

void JointMotor::Apply(Info2& info, float val, int row) const
{
  // only motor
  info.cfm[row] = _normal_cfm >= 0 ? _normal_cfm : info.cfmCoef;   
  info.c[row] = _vel;
  if (_fmax >= 0)
  {
    info.lo[row] = -_fmax;
    info.hi[row] = _fmax; 
  }
  else
  {  
    info.lo[row] = -dInfinity;
    info.hi[row] = dInfinity; 
  }
}

void JointLimit::Apply(Info2& info, float val, int row) const
{
  int limit = 0; 
  float limit_err = 0; 
  if (val <= _lostop )
  {
    limit = -1;
    limit_err = val - _lostop;
  }

  if (val >= _histop )
  {
    limit = 1;
    limit_err = val - _histop;
  }

  if (limit == 0)  
    return;  
  
  // joint is at limit
  float k = info.fps * (_stop_erp >= 0 ? _stop_erp : info.erp);
  info.c[row] = k * limit_err;
  info.cfm[row] = _stop_cfm >= 0? _stop_cfm : info.cfmCoef;

  if (_lostop == _histop) {
    // limited low and high simultaneously
    info.lo[row] = -dInfinity;
    info.hi[row] = dInfinity;
  }
  else 
  {
    if (limit == 1) 
    {
      // low limit
      info.lo[row] = 0;
      info.hi[row] = dInfinity;
    }
    else 
    {
      // high limit
      info.lo[row] = -dInfinity;
      info.hi[row] = 0;
    }
  }
}
bool JointLimitAngular::IsAtLimit(float val) const
{
  if (_lostop > _histop)  
     // valid intervals are (_lostop. 2 * pi),(0,_histop)
    return (val >= _histop && val <= _lostop);
  else
    return ! (val > _lostop && val < _histop);
}

void JointLimitAngular::Apply(Info2& info, float val, int row) const
{
  int limit = 0; 
  float limit_err = 0; 
  // get limit and error . mind that all values are in interval (0,2 * pi)
  if (_lostop > _histop)
  {
    float avgStop = (_lostop + _histop) * 0.5f;  
    if (val <= _lostop && val >= avgStop)
    {
      limit = -1;
      limit_err = val - _lostop;
    }
    else if (val >= _histop &&  val < avgStop)
    {
      limit = 1;
      limit_err = val - _histop;
    }
  }
  else
  {  
    if (val > _lostop && val < _histop)
      return; // no limit

    // relative to _lostop
    float avgStop = (_histop - _lostop) * 0.5f + PI;
    if (avgStop > 2 * PI)
      avgStop -= 2* PI;

    val -= _lostop;
    if (val < 0)
      val += 2* PI;

    if (val > avgStop)
    { 
      limit = -1;
      limit_err = -val;
    }    
    else
    {
      limit = 1;
      limit_err = val - (_histop - _lostop);        
    }
  }

  if (limit == 0)  
    return;  

  // joint is at limit
  float k = info.fps * (_stop_erp >= 0 ? _stop_erp : info.erp);
  info.c[row] = k * limit_err;
  info.cfm[row] = _stop_cfm >= 0? _stop_cfm : info.cfmCoef;

  if (_lostop == _histop) {
    // limited low and high simultaneously
    info.lo[row] = -dInfinity;
    info.hi[row] = dInfinity;
  }
  else 
  {
    if (limit == 1) 
    {
      // low limit
      info.lo[row] = 0;
      info.hi[row] = dInfinity;
    }
    else 
    {
      // high limit
      info.lo[row] = -dInfinity;
      info.hi[row] = 0;
    }
  }
}

//------------------------------------
// HierarchyJointRB
//------------------------------------

HierarchyJointRB::HierarchyJointRB(const HierarchyJointRB& src)
{
  _id[0] = src._id[0].Clone();
  _id[1] = src._id[1].Clone();
  _cfm = src._cfm;
  _erp = src._erp;
}

/** Creates joint between two bodies. 
  * @param parent - all paths are from him 
  * @param myPath - path to the joint
  * @param body0Path - path to the first object (it must exist)
  * @param body1Path - path to the seconf object. If it is zero, the second object is undefined static object .
  */
void HierarchyJointRB::Create(const HierarchyItem& parent, ConstHierarchyPathRef myPath, HierarchyPathVal body0Path, ConstHierarchyPathRef body1Path)
{
  _id[0].Create(parent, myPath, body0Path);
  if (body1Path)
    _id[1].Create(parent, myPath, *body1Path);
}

void HierarchyJointRB::OnRemove(HierarchyIDVal scope)
{
  _id[0].Destroy(scope);
  _id[1].Destroy(scope);
}

Ref<IRigidBody> HierarchyJointRB::GetIRigidBody(HierarchyIDVal scope, int i) const
{  
  if (! _id[i].IsNotNull())
    return NULL;

  HierarchyID objID = _id[i].GetID(scope);
  if (objID)
    return objID.Get()->GetIRigidBody(objID).GetRef();
  else
    return NULL;
}

HierarchyID HierarchyJointRB::GetBodyID(HierarchyIDVal scope, int i) const
{
  if (!_id[i].IsNotNull())
    return HierarchyID();

  return _id[i].GetID(scope);
}

// manipulation of ids
bool HierarchyJointRB::NeedChangeForItemMoved(const HierarchyID& scope, HierarchyPathVal path) const
{
  return _id[0].NeedChangeForItemMoved(scope, path) && _id[1].NeedChangeForItemMoved(scope, path);
}

void HierarchyJointRB::ItemMoved(const HierarchyID& scope, HierarchyPathVal pathFrom, HierarchyPathVal pathTo)
{
  _id[0].ItemMoved(scope, pathFrom, pathTo);
  _id[1].ItemMoved(scope, pathFrom, pathTo);
}

void HierarchyJointRB::ItemRemoved(const HierarchyID& scope, HierarchyPathVal removed)
{
  _id[0].ItemRemoved(scope, removed);
  _id[1].ItemRemoved(scope, removed);
}

//------------------------------------
// HierarchyJointRBWrap
//------------------------------------
DEFINE_FAST_ALLOCATOR(HierarchyJointRBWrap);
DEFINE_CASTING(HierarchyJointRBWrap);

HierarchyJointRBWrap::HierarchyJointRBWrap( HierarchyIDVal id)
{
  _id = id;   
  _joint = static_cast<HierarchyJointRB *>((HierarchyItem *)id);
  _bodyID[0] = _joint->GetBodyID(_id,0);
  _bodyID[0].Register();
  _bodyID[1] = _joint->GetBodyID(_id,1);
  _bodyID[1].Register();
}

void HierarchyJointRBWrap::SetResult(float * lambda)
{
  Info1 info;
  GetInfo1(&info);

  memcpy(_lambda, lambda,info.m * sizeof(*lambda));  
}

Vector3 HierarchyJointRBWrap::GetForceInJoint() const
{
  return _joint->GetForceInJoint(_body[0],_body[1],_lambda);
}
Vector3 HierarchyJointRBWrap::GetTorqueOnBody(int i) const
{
  return _joint->GetTorqueOnBody(_body[0],_body[1],i,_lambda);
}

void HierarchyJointRBWrap::SetIRigidBodies(IDtoIRigidBody& src)
{
  Assert((bool)_bodyID[0]);
  _body[0] = src.GetIRigidBody(_bodyID[0]);
  _body[0]->AddJointToStorage(this);
  
  if (_bodyID[1])  
  {
    _body[1] = src.GetIRigidBody(_bodyID[1]);    
    _body[1]->AddJointToStorage(this);
  }
}

bool HierarchyJointRBWrap::HasIRigidBody(const IRigidBody& body, int i) const
{ 
  return base::HasIRigidBody(body, i);
}

void HierarchyJointRBWrap::RunReactors(float deltaT, bool impulseJoints)
{
  if (impulseJoints)
    return;

  if (_joint->IsBreaking(_body[0],_body[1],_lambda))
  {
    // remove joint from hierarchy
    for(int i = 0; i < 2; i++)
    {     
      // remove joint from body

      if (_body[i])
        _body[i]->RemovePermanentJoint(this);         
    }

    unconst_cast(_id.GetParent())->RemoveSubItem(*_id.GetPath());
  }
}

#if _ENABLE_CHEATS
void HierarchyJointRBWrap::DiagDraw()
{
  if (CHECK_DIAG(DEPermanentJoints))
  { 
    _joint->DiagDraw(_id, _lambda);
  }
}
#endif
//------------------------------------
// BallJointRB
//------------------------------------

BallJointRB::BallJointRB() 
#if _ENABLE_CHEATS
: _color(Color(GRandGen.RandomValue(),GRandGen.RandomValue(),GRandGen.RandomValue()))
#endif
{
  _bodiesNotCollide = true;
};

BallJointRB::BallJointRB(const BallJointRB& src)
: base(src)
{
  _pos[0] = src._pos[0];
  _pos[1] = src._pos[1];

  _dir[0] = src._dir[0];
  _dir[1] = src._dir[1];
  _dir[2] = src._dir[2];

  _bodiesNotCollide = src._bodiesNotCollide;

#if _ENABLE_CHEATS
  _color = src._color;
#endif

}

HierarchyItem * BallJointRB::Clone() const
{
  BallJointRB * clone = new BallJointRB(*this);
  AfterClone(clone);
  return clone;
}

/** Initializes ball joint. 
* @param scope - id of joint
* @param pos - position of the anchor in world coord. 
* @param dir - coordinate system of joint in world coord. If NULL arbitrary coord will be used.
*/

void BallJointRB::Init(HierarchyIDVal scope, Vector3Val pos, Vector3 * dir)
{
  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Assert(body0);

  _pos[0] = body0->PositionWorldToBody(pos);
  if (body1)
    _pos[1] = body1->PositionWorldToBody(pos);

  if (dir)
  {
    for(int i = 0; i < 3; i++)
      _dir[i] = body0->DirectionWorldToBody(dir[i]);    
  }
  else
  {
    _dir[0] = VForward;
    _dir[1] = VUp;
    _dir[2] = _dir[0].CrossProduct(_dir[1]);
  }

}

void BallJointRB::AddLimitMotor(JointLimitMotorBase * limitMotor)
{
  /* BallJoint cannot be limited in current version. It can be only motorized. 
    If you want to create limited BallJoint use combination of Hinge2 and Hinge joint.

    Problem with implementation of limited BallJoint is decomposition of rotation into 3 axes. 
    Further about topic:
    http://www.euclideanspace.com/maths/geometry/rotations/euler/index.htm

    The second option is to use AMotor joint from ODE. 
  */

  if (limitMotor->IsLimit())
  {
    RptF("Cannot set limit for BallJoint.");
    return;
  }

  Assert(limitMotor->IsPowered())

  if (limitMotor->Dof() < 0 || limitMotor->Dof() > 3)
  {
    RptF("Cannot set motor for DOF %f for BallJoint.", limitMotor->Dof());
    return; // no such limit for reduction
  }

  base::AddLimitMotor(limitMotor);
}

void BallJointRB::GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info)
{
  info->m = 3;

  // check the limits and motors if there is more constrains
  for(int i = 0; i < _limitMotors.Size(); i++)
  {
    JointLimitMotorBase* limit = _limitMotors[i];    
    if (limit->IsPowered())
    {
      info->m++;
      continue;
    }

    // ball joint cannot be limited... Limited ball joint can be created like a combination of Hinge2 body Hinge joint    
  }
}

void BallJointRB::GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info)
{
  int s = info->rowskip;

  Vector3 wdir[3];
  for(int i = 0 ; i < 3; i++)
    wdir[i] = body0->DirectionBodyToWorld( _dir[i]);

  float invMass = body0->GetMass();

  // set linear part of J1l and J2l
  for(int i = 0; i < 3; i++)
  {
    for(int j = 0; j < 3; j++)
    {
      info->J1l[i * s + j] = wdir[i][j];        
    }
  }

  bool nonstaticBody1 = body1 && !body1->IsStatic();

  if (nonstaticBody1)
  {
    for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
      {
        info->J2l[i * s + j] = -wdir[i][j];   
      }
    }

    invMass += body1->GetMass();
  }
  invMass = 1.0f / invMass;

  Vector3 wpos[2];
  wpos[0] = body0->PositionBodyToWorld(_pos[0]);

  if (body1)
    wpos[1] = body1->PositionBodyToWorld(_pos[1]);
  

  Vector3 c1,c2;

  c1 = wpos[0] - body0->GetMassCenter();  
  if (body1)
    c2 = -(wpos[1] - body1->GetMassCenter()); // negative c2

  for (int i = 0; i < 3; i++)
  {
    dCROSS (info->J1a + i * s,=,c1,wdir[i]);   
  }

  if (nonstaticBody1)
  {
    for (int i = 0; i < 3; i++)
    {        
      dCROSS (info->J2a + i * s,=,c2,wdir[i]);
    }
  }

  // now found errors
  // set right hand side for the first three rows (linear)  
  if (body1)
  { 
    // without body1 correction is not possible
    float erp = _erp >= 0 ? _erp : info->erp;      
    float k = info->fps * erp;

    Vector3 diff;
    diff = wpos[1] - wpos[0];

    for (int j=0; j<3; j++)
    {
      float error = wdir[j] * diff;      
      info->c[j] = k * error;
    }
  }

  // set cfm factor 
  float cfm = _cfm >= 0 ? _cfm : info->cfmCoef; 
  cfm *= invMass;
  for(int i = 0; i < 3; i++)    
    info->cfm[i] = cfm;

  // if there are also limits add angular part
  for(int i = 0; i < _limitMotors.Size(); i++)
  {
    JointLimitMotorBase * limit = _limitMotors[i];  
    if (limit->IsPowered())
    {        
      int row = 3 +  i;
      for (int j = 0; j < 3; j++)
      {       
        info->J1a[j + row * s] = wdir[limit->Dof()][j];        
      }      

      if (nonstaticBody1)
      {           
        for (int j = 0; j < 3; j++)
        {          
          info->J2a[j + row * s] = -wdir[limit->Dof()][j];
        }            
      }
      limit->Apply(*info, 0, row);
    }
  }
}

// from body 0 to body 1
Vector3 BallJointRB::GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const
{
  Vector3 res(VZero);
  for(int i = 0 ; i < 3; i++)
    res += body0->DirectionBodyToWorld( _dir[i]) * lambda[i];

  return res;
}

Vector3 BallJointRB::GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const  float * lambda) const
{
  // torque is created by force in joint at contact 
  Vector3 force = GetForceInJoint( body0, body1, lambda);
  if ( i == 0)
    force *= -1;

  IRigidBody * body = (i == 0) ? body0 : body1;

  Vector3 c = body->PositionBodyToWorld(_pos[i]) - body->GetMassCenter();
  return c.CrossProduct(force);
}

#if _ENABLE_CHEATS
void BallJointRB::DiagDraw(HierarchyIDVal scope, float * lambda)
{
  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Vector3 pos0 = body0->PositionBodyToWorld(_pos[0]);
  Vector3 massCenter0 = body0->GetMassCenter();

  Vector3 dir = pos0 - massCenter0;
  float size = dir.Size();

  GScene->DrawDiagArrow(massCenter0, dir / size, size, _color);

  if (body1)
  {
    Vector3 pos1 = body1->PositionBodyToWorld(_pos[1]);
    Vector3 massCenter1 = body1->GetMassCenter();
    Vector3 dir = pos1 - massCenter1;
    float size = dir.Size();

    GScene->DrawDiagArrow(massCenter1, dir / size, size, _color);
  }  
}
#endif
//------------------------------------
// FixedJointRB
//------------------------------------

FixedJointRB::FixedJointRB(const FixedJointRB& src)
: base(src)
{
  _qrel = src._qrel;
}

HierarchyItem * FixedJointRB::Clone() const
{
  FixedJointRB * clone = new FixedJointRB(*this);
  AfterClone(clone);
  return clone;
}

void FixedJointRB::Init(HierarchyIDVal scope, Vector3Val pos, Vector3 * dir)
{
  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Assert(body0);

  _pos[0] = body0->PositionWorldToBody(pos);
  if (body1)
    _pos[1] = body1->PositionWorldToBody(pos);
 

  // compute the relative orient between the bodies   
  _qrel = body0->GetOrientation().GetTransposed();
  if (body1)
    _qrel *= body1->GetOrientation();  

  if (dir)
  {
    for(int i = 0; i < 3; i++)
      _dir[i] = body0->DirectionWorldToBody(dir[i]);    
  }
  else
  {
    _dir[0] = VForward;
    _dir[1] = VUp;
    _dir[2] = _dir[0].CrossProduct(_dir[1]);
  }
}

void FixedJointRB::AddLimitMotor(JointLimitMotorBase * limitMotor)
{
  RptF("Cannot set limit or motor for FixedJoint.");
  return; // all degrees of freedom are frozen, nothing to limit or motorized.
}


void FixedJointRB::GetInfo1(IRigidBody * body0,IRigidBody* body1, Info1 *info)
{
  info->m = 6;
}

static void SetFixedOrientation(IRigidBody * body0, IRigidBody* body1, Info2 *info,  Vector3 wdir[], QuaternionRB qrel, float epsK, int start_row)
{
  int s = info->rowskip;
  for (int i = 0; i < 3; i++)
  {    
    for (int j = 0; j < 3; j++)
    {       
      info->J1a[j + (start_row + i) * s] = wdir[i][j];        
    }      
  }

  if (body1 && !body1->IsStatic())
  {
    for (int i = 0; i < 3; i++)
    {           
      for (int j = 0; j < 3; j++)
      {          
        info->J2a[j + (start_row + i) * s] = -wdir[i][j];
      }      
    }
  }

  // compute the right hand side. the first three elements will result in
  // relative angular velocity of the two bodies - this is set to bring them
  // back into alignment. the correcting angular velocity is
  //   |angular_velocity| = angle/time = erp*theta / stepsize
  //                      = (erp*fps) * theta
  //    angular_velocity  = |angular_velocity| * u
  //                      = (erp*fps) * theta * u
  // where rotation along unit length axis u by theta brings body 2's frame
  // to qrel with respect to body 1's frame. using a small angle approximation
  // for sin(), this gives
  //    angular_velocity  = (erp*fps) * 2 * v
  // where the quaternion of the relative rotation between the two bodies is
  //    q = [cos(theta/2) sin(theta/2)*u] = [s v]

  // get qerr = relative rotation (rotation error) between two bodies
  QuaternionRB qq = body0->GetOrientation().GetTransposed(); 
  if (body1) 
    qq *=  body1->GetOrientation();

  QuaternionRB qerr = qq *  qrel.GetTransposed();

  if (qerr[0] < 0) {
    qerr[1] = -qerr[1];		// adjust sign of qerr to make theta small
    qerr[2] = -qerr[2];
    qerr[3] = -qerr[3];
  }

  Vector3 eLoc(qerr.Getx(), qerr.Gety(), qerr.Getz());
  Vector3 e = body0->DirectionBodyToWorld(eLoc);

  for(int i = 0; i < 3; i++)  
    info->c[start_row + i] += 2 * epsK * wdir[i] * e;  
}

void FixedJointRB::GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info)
{  
  int s = info->rowskip;
 
  Vector3 wdir[3];
  for(int i = 0 ; i < 3; i++)
    wdir[i] = body0->DirectionBodyToWorld( _dir[i]);

  // set linear part of J1l and J2l
  for(int i = 0; i < 3; i++)
  {
    for(int j = 0; j < 3; j++)
    {
      info->J1l[i * s + j] = wdir[i][j];        
    }
  }

  bool nonstaticBody1 = body1 && !body1->IsStatic();
  if (nonstaticBody1)
  {
    for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
      {
        info->J2l[i * s + j] = -wdir[i][j];   
      }
    }
  }

  Vector3 wpos[2];
  wpos[0] = body0->PositionBodyToWorld(_pos[0]);

  if (body1)
    wpos[1] = body1->PositionBodyToWorld(_pos[1]);

  Vector3 c1,c2;

  c1 = wpos[0] - body0->GetMassCenter();  
  if (nonstaticBody1)
    c2 = -(wpos[1] - body1->GetMassCenter()); // negative c2

  for (int i = 0; i < 3; i++)
  {
    dCROSS (info->J1a + i * s,=,c1,wdir[i]);         
  }

  if (nonstaticBody1)
  {
    for (int i = 0; i < 3; i++)
    {        
      dCROSS (info->J2a + i * s,=,c2,wdir[i]);      
    }
  }

  // now found errors
  // set right hand side for the first three rows (linear) 
  float erp = _erp >= 0 ? _erp : info->erp;  
  float k = info->fps * erp;

  if (body1)
  {
    // no correction without body1
    Vector3 diff;
    diff = wpos[1] - wpos[0];

    for (int j=0; j<3; j++)
      info->c[j] = k * (wdir[j] * diff);  
  }

  SetFixedOrientation(body0, body1, info, wdir, _qrel, k, 3);

  // set cfm factor 
  float cfm = _cfm >= 0 ? _cfm : info->cfmCoef; 
  for(int i = 0; i < 6; i++)    
    info->cfm[i] = cfm;
}

Vector3 FixedJointRB::GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const
{
  // torque is created by force in joint at contact 
  // plus torque in joint
  Vector3 force = GetForceInJoint(body0,body1,lambda);
  
  IRigidBody * body = (i == 0) ? body0 : body1;  

  Vector3 c = body->PositionBodyToWorld(_pos[i]) - body->GetMassCenter();
  Vector3 res = VZero;

  // torque in joint 
  for(int j = 0 ; j < 3; j++)
    res += body->DirectionBodyToWorld( _dir[j]) * lambda[j+3];

  // torque generated by force
  res += c.CrossProduct(force);  

  if (i == 0)
    res *= -1;

  return res;
}

//------------------------------------
// BreakableFixedJointRB
//------------------------------------
 
BreakableFixedJointRB::BreakableFixedJointRB(const BreakableFixedJointRB& src)
: base(src)
{
  _forceBoundHi = src._forceBoundHi;
  _forceBoundLo = src._forceBoundLo;
  _torqueBoundHi = src._torqueBoundHi;
  _torqueBoundLo = src._torqueBoundLo; 
}

HierarchyItem * BreakableFixedJointRB::Clone() const
{
  BreakableFixedJointRB * clone = new BreakableFixedJointRB(*this);
  AfterClone(clone);
  return clone;
}

void BreakableFixedJointRB::GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info)
{
  base::GetInfo2( body0, body1, info);

  // hi lo 
  for(int i = 0; i < 3; i++)
  {    
    info->hi[i] = _forceBoundHi[i];
    info->lo[i] = _forceBoundLo[i];
  }

  for(int i = 0; i < 3; i++)
  {    
    info->hi[i + 3] = _torqueBoundHi[i];
    info->lo[i + 3] = _torqueBoundLo[i];
  }
}

//!Returns true if joint is breakable and is breaking
bool BreakableFixedJointRB::IsBreaking(IRigidBody * body0,IRigidBody* body1, const float * lambda) const
{
  Vector3 wPos = body0->PositionBodyToWorld(_pos[0]);

  // get velocity
  Vector3 vel = body0->SpeedAtPoint(wPos);
  if (body1)
    vel -= body1->SpeedAtPoint(wPos); 

  // check forces
  for(int i = 0; i < 3; i++)
  {       
    if (lambda[i] == _forceBoundHi[i])
    {
      float velInDir = vel * body0->DirectionBodyToWorld(_dir[i]);
      if (velInDir < -0.01f) // numerical barrier
      {
        // force is on maximum but bodies are still moving
        return true;
      }
    }
    if (lambda[i] == _forceBoundLo[i])
    {
      float velInDir = vel * body0->DirectionBodyToWorld(_dir[i]);
      if (velInDir > 0.01f) // numerical barrier
      {
        // force is on maximum but bodies are still moving
        return true;
      }
    }
  }

  // check momentum

  // get angVelocity
  Vector3 angVel = body0->GetAngularVelocity();
  if (body1)
    angVel -= body1->GetAngularVelocity(); 

  for(int i = 0; i < 3; i++)
  {       
    if (lambda[i + 3] == _torqueBoundHi[i])
    {
      float velInDir = angVel * body0->DirectionBodyToWorld(_dir[i]);
      if (velInDir < -0.01f) // numerical barrier
      {
        // force is on maximum but bodies are still moving
        return true;
      }
    }
    if (lambda[i + 3] == _torqueBoundLo[i])
    {
      float velInDir = angVel * body0->DirectionBodyToWorld(_dir[i]);
      if (velInDir > 0.01f) // numerical barrier
      {
        // force is on maximum but bodies are still moving
        return true;
      }
    }
  }

  return false;
}

#if _ENABLE_CHEATS
void BreakableFixedJointRB::DiagDraw(HierarchyIDVal scope, float * lambda)
{
  base::DiagDraw(scope, lambda);

  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Vector3 pos0 = body0->PositionBodyToWorld(_pos[0]);
  for(int i = 0; i < 3; i++)
  {
    Vector3 wDir = body0->DirectionBodyToWorld(_dir[i]);  
    float sign = lambda[i] > 0 ? 1 : -1;
    // red will be forces, blue will be torque
    if (lambda[i] == _forceBoundHi[i] || lambda[i] == _forceBoundLo[i])      
      GScene->DrawDiagArrow(pos0, sign *wDir, sign *lambda[i] / _forceBoundHi[i], PackedColor(Color(1,0,0)));    
    else
      GScene->DrawDiagArrow(pos0, sign *wDir,sign * lambda[i] / _forceBoundHi[i], PackedColor(Color(1,0,0, 0.5f))); 

    sign = lambda[3+i] > 0 ? 1 : -1;
    if (lambda[3+i] == _torqueBoundHi[i] || lambda[3+i] == _torqueBoundLo[i])      
      GScene->DrawDiagArrow(pos0, sign *wDir, sign *lambda[3+i] / _torqueBoundHi[i], PackedColor(Color(0,0,1)));    
    else
      GScene->DrawDiagArrow(pos0, sign *wDir, sign *lambda[3+i] / _torqueBoundHi[i], PackedColor(Color(0,0,1, 0.5f))); 
  }  
}
#endif

//------------------------------------
// SliderJointRB
//------------------------------------
SliderJointRB::SliderJointRB()
#if _ENABLE_CHEATS
: _color(Color(GRandGen.RandomValue(),GRandGen.RandomValue(),GRandGen.RandomValue()))
#endif
{
  _bodiesNotCollide = true;
};

SliderJointRB::SliderJointRB(const SliderJointRB& src)
: base(src)
{
  _pos[0] = src._pos[0];
  _pos[1] = src._pos[1];

  _dir[0] = src._dir[0];
  _dir[1] = src._dir[1];
  _dir[2] = src._dir[2]; 

  _bodiesNotCollide = src._bodiesNotCollide;

  _qrel = src._qrel;
}

HierarchyItem * SliderJointRB::Clone() const
{
  SliderJointRB * clone = new SliderJointRB(*this);
  AfterClone(clone);
  return clone;
}

void SliderJointRB::Init(HierarchyIDVal scope, Vector3Val pos, Vector3Val axe, Vector3 * dir )
{
  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Assert(body0);

  _pos[0] = body0->PositionWorldToBody(pos);
  if (body1)
    _pos[1] = body1->PositionWorldToBody(pos);
 
  _dir[0] =  body0->DirectionWorldToBody(axe);

  if (dir)
  {
    for(int i = 0; i < 2; i++)
      _dir[i + 1] = body0->DirectionWorldToBody(dir[i]);    
  }
  else
  { 
    if (fabs(_dir[0] * VUp) < 0.9f)
      _dir[1] = _dir[0].CrossProduct(VUp);
    else
      _dir[1] = _dir[0].CrossProduct(VForward);

    _dir[1].Normalize();   
    _dir[2] = _dir[0].CrossProduct(_dir[1]);
  }
}

void SliderJointRB::AddLimitMotor(JointLimitMotorBase * limitMotor)
{
  if (_limitMotors.Size() == 1)
  {
    RptF("Cannot set more than 1 limit or motors for SliderJoint");
    return;
  }
  HierarchyJointRB::AddLimitMotor(limitMotor);
}

void SliderJointRB::GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info)
{
  info->m = 5;  

  if (_limitMotors.Size() > 0 )
  {
    Assert(_limitMotors.Size() == 1);
    JointLimitMotorBase * limitMotor =  _limitMotors[0];
    if (limitMotor->IsPowered())
    {
      info->m = 6;
      return;
    }

    if (limitMotor->IsLimit() && body1)
    {      
      float val = (body1->PositionBodyToWorld(_pos[1]) - body0->PositionBodyToWorld(_pos[0])) *  body0->DirectionBodyToWorld( _dir[0]);
      if (limitMotor->IsAtLimit(val))
      {
        info->m = 6;
        return;
      }    
    }

  }
}
//! Returns Jacobian, cfm...
void SliderJointRB::GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info)
{
  int s = info->rowskip;
  
  // transfer coordinate system and anchors to world coord
  Vector3 wdir[3];
  for(int i = 0 ; i < 3; i++)
    wdir[i] = body0->DirectionBodyToWorld( _dir[i]);

  Vector3 wpos[2];
  wpos[0] = body0->PositionBodyToWorld(_pos[0]);

  if (body1)
    wpos[1] = body1->PositionBodyToWorld(_pos[1]);
 

  Vector3 c1,c2;
  c1 = wpos[0] - body0->GetMassCenter();  
  for (int i = 0; i < 2; i++)
  {
    dCROSS (info->J1a + i * s,=,c1,wdir[i + 1]);         
    for(int j = 0; j < 3; j++)
    {
      info->J1l[i * s + j] = wdir[i + 1][j];        
    }
  }

  bool nonstaticBody1 = body1 && !body1->IsStatic();
  if (nonstaticBody1)
  {    
    c2 = -(wpos[0] - body1->GetMassCenter()); // negative c2
    for (int i = 0; i < 2; i++)
    {        
      dCROSS (info->J2a + i * s,=,c2,wdir[i + 1]);
      for(int j = 0; j < 3; j++)
      {
        info->J2l[i * s + j] = -wdir[i + 1][j];
      }
    }
  }

  // set right side according to error  
  float k = info->fps * info->erp;
  if (body1)
  {
    // no correction without body1
    Vector3 diff;
    diff = wpos[1] - wpos[0];

    for (int i=0; i<2; i++)
      info->c[i] = k * (wdir[i + 1] * diff);
  }

  // 3 rows to make body rotations equal
  SetFixedOrientation(body0, body1, info, wdir, _qrel, k, 2);  

  // handle limits and motors, cannot calculate value without body1
  if (_limitMotors.Size() > 0 )
  {
    Assert(_limitMotors.Size() == 1);
    JointLimitMotorBase * limitMotor =  _limitMotors[0];

    float val = 0; 
    if (body1 && limitMotor->IsLimit())    
       val = (wpos[1] - wpos[0]) * wdir[0];
    
    if (limitMotor->IsPowered() || (body1 && limitMotor->IsLimit() && limitMotor->IsAtLimit(val)))
    {       
      int row = 5 * s;
      dCROSS (info->J1a + row,=,c1,wdir[0]);         
      for(int j = 0; j < 3; j++)
      {
        info->J1l[row + j] = wdir[0][j];        
      }
   
      if (nonstaticBody1)
      {    
        dCROSS (info->J2a + row,=,c2,wdir[0]);
        for(int j = 0; j <3; j++)
        {
          info->J2l[row + j] = -wdir[0][j];
        }
      }
      limitMotor->Apply(*info,val,5);
    }
  }
}

Vector3 SliderJointRB::GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const
{
  Vector3 res(VZero);
  for(int i = 0 ; i < 2; i++)
    res += body0->DirectionBodyToWorld( _dir[i + 1]) * lambda[i];

  return res;
}

// Returns torque in world coordinate that was applied by joint to body. 
Vector3 SliderJointRB::GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const
{
  // torque is created by force in joint at contact 
  // plus torque in joint
  Vector3 force = GetForceInJoint(body0,body1,lambda);

  IRigidBody * body = (i == 0) ? body0 : body1;  

  Vector3 c = body->PositionBodyToWorld(_pos[i]) - body->GetMassCenter();
  Vector3 res = VZero;

  // torque in joint 
  for(int j = 0 ; j < 3; j++)
    res += body->DirectionBodyToWorld( _dir[j]) * lambda[j+3];

  // torque generated by force
  res += c.CrossProduct(force);  

  if (i == 0)
    res *= -1;

  return res;
}

#if _ENABLE_CHEATS
void SliderJointRB::DiagDraw(HierarchyIDVal scope, float * lambda)
{
  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Vector3 pos0 = body0->PositionBodyToWorld(_pos[0]);
  Vector3 massCenter0 = body0->GetMassCenter();

  Vector3 dir = pos0 - massCenter0;
  float size = dir.Size();

  GScene->DrawDiagArrow(massCenter0, dir / size, size, _color);

  if (body1)
  {
    Vector3 pos1 = body1->PositionBodyToWorld(_pos[1]);
    Vector3 massCenter1 = body1->GetMassCenter();
    Vector3 dir = pos1 - massCenter1;
    float size = dir.Size();

    GScene->DrawDiagArrow(massCenter1, dir / size, size, _color);
  }
  //#endif
}
#endif

//------------------------------------
// HingeJointRB
//------------------------------------

HingeJointRB::HingeJointRB(const HingeJointRB& src) : base(src)
{};

HingeJointRB::HingeJointRB() 
{};

HierarchyItem * HingeJointRB::Clone() const
{
  HingeJointRB * clone = new HingeJointRB(*this);
  AfterClone(clone);
  return clone;
};

// Initialization
void HingeJointRB::Init(HierarchyIDVal scope, Vector3Val pos, Vector3 axe, Vector3 * dir )
{
  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Assert(body0);

  _pos[0] = body0->PositionWorldToBody(pos);
  if (body1)
    _pos[1] = body1->PositionWorldToBody(pos);
 

  _dir[0] =  body0->DirectionWorldToBody(axe);
  if (body1)
    _axe1 = body1->DirectionWorldToBody(axe);

  // compute the relative orient between the bodies   
  _qrel = body0->GetOrientation().GetTransposed();
  if (body1)
    _qrel *= body1->GetOrientation();  


  if (dir)
  {
    for(int i = 0; i < 2; i++)
      _dir[i + 1] = body0->DirectionWorldToBody(dir[i]);    
  }
  else
  { 
    if (fabs(_dir[0] * VUp) < 0.9f)
      _dir[1] = _dir[0].CrossProduct(VUp);
    else
      _dir[1] = _dir[0].CrossProduct(VForward);

    _dir[1].Normalize();   
    _dir[2] = _dir[0].CrossProduct(_dir[1]);
  }
};

void HingeJointRB::AddLimitMotor(JointLimitMotorBase * limitMotor)
{
  if (_limitMotors.Size() == 1)
  {
    RptF("Cannot set more than 1 limit or motors for HingeJoint");
    return;
  }
  HierarchyJointRB::AddLimitMotor(limitMotor);
}

float HingeJointRB::GetRotAngle(IRigidBody * body0,IRigidBody* body1) const
{
  // the quaternion of the relative rotation between the two bodies is
  //  q = [cos(theta/2) sin(theta/2)*u] = [s v]
  
  QuaternionRB qq = body0->GetOrientation().GetTransposed(); 
  if (body1) 
  {
    // a correction cannot be done without body 1
    qq *=  body1->GetOrientation();
  }

  qq *= _qrel.GetTransposed();

  Vector3 wdir = body0->DirectionBodyToWorld(_dir[0]);

  float sinTheta = qq.Getx() * wdir[0] + qq.Gety() * wdir[1] + qq.Getz() * wdir[2];
  if (sinTheta > 0)
  {
    float theta = asin(sinTheta); 
    if (qq.Getw() < 0)    
      theta = PI - theta;

    theta *= 2; 
    return theta;
  }
  else
  {
    float theta = asin(-sinTheta); 
    if (qq.Getw() > 0)    
      theta = PI - theta;

    theta *= 2;
    return theta;
  }  
}

void HingeJointRB::GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info)
{
  info->m = 5;  
  if (_limitMotors.Size() >= 1)
  {
    JointLimitMotorBase * limit = _limitMotors[0];
    if (limit->IsPowered() || (limit->IsLimit() && limit->IsAtLimit(GetRotAngle(body0, body1)) ))
    {
      info->m = 6;
      return;
    } 
  }
}

void HingeJointRB::GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info)
{  
  int s = info->rowskip;

  Vector3 wdir[3];
  for(int i = 0 ; i < 3; i++)
    wdir[i] = body0->DirectionBodyToWorld( _dir[i]);

  Vector3 wpos[2];
  wpos[0] = body0->PositionBodyToWorld(_pos[0]);

  if (body1)
    wpos[1] = body1->PositionBodyToWorld(_pos[1]);

  bool nonstaticBody1 = body1 && !body1->IsStatic();
  
  // set part for forces
  Vector3 c1 = wpos[0] - body0->GetMassCenter();  
  for(int i = 0; i < 3; i++)
  {
    dCROSS (info->J1a + i * s,=,c1,wdir[i]); 
    for(int j = 0; j < 3; j++)
    {
      info->J1l[i * s + j] = wdir[i][j];        
    }
  }

  if (nonstaticBody1)
  {
    Vector3 c2 = -(wpos[1] - body1->GetMassCenter()); // negative c2
    for(int i = 0; i < 3; i++)
    {
      dCROSS (info->J2a + i * s,=,c2,wdir[i]); 
      for(int j = 0; j < 3; j++)
      {
        info->J2l[i * s + j] = -wdir[i][j];   
      }
    }    
  }

  // fixed two rotations except one in _axe   
  for (int i = 0; i < 2; i++)
  {    
    for (int j = 0; j < 3; j++)
    {       
      info->J1a[j + (3 + i) * s] = wdir[i + 1][j];
    }      
  }

  if (nonstaticBody1)
  {
    for (int i = 0; i < 2; i++)
    {           
      for (int j = 0; j < 3; j++)
      {          
        info->J2a[j + (3 + i) * s] = -wdir[i + 1][j];
      }      
    }
  }

  // compute the right hand side.  
  if (body1)
  {
    // no correction without body1
    float erp = _erp >= 0 ? _erp : info->erp;  
    float k = info->fps * erp;

    // set right hand side for the first three rows (linear)  
    Vector3 diff;
    diff = wpos[1] - wpos[0];

    for (int j=0; j<3; j++)
      info->c[j] = k * (wdir[j] * diff); 

    
   // set right hand side for the angular part
    Vector3 waxe1 = body1->DirectionBodyToWorld(_axe1);
    Vector3 e = wdir[0].CrossProduct(waxe1);

    for(int i = 0; i < 2; i++)  
      info->c[3 + i] = k * wdir[i+1] * e;   
  }

  // set cfm factor 
  float cfm = _cfm >= 0 ? _cfm : info->cfmCoef; 
  for(int i = 0; i < 5; i++)    
    info->cfm[i] = cfm;

  // handle limits and motors
  if (_limitMotors.Size() > 0)
  {
    Assert(_limitMotors.Size() == 1);
    JointLimitMotorBase * limitMotor =  _limitMotors[0];
    float rotAngle = GetRotAngle(body0, body1);

    if (limitMotor->IsPowered() || (limitMotor->IsLimit() && limitMotor->IsAtLimit(rotAngle)))
    {        
      for (int j = 0; j < 3; j++)
      {       
        info->J1a[j + 5 * s] = wdir[0][j];
      }      
      
      if (nonstaticBody1)
      {      
        for (int j = 0; j < 3; j++)
        {          
          info->J2a[j + 5 * s] = -wdir[0][j];
        }             
      }

      limitMotor->Apply(*info,rotAngle,5);
    }
  }
}
// from body 0 to body 1
Vector3 HingeJointRB::GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const
{
  Vector3 res(VZero);
  for(int i = 0 ; i < 3; i++)
    res += body0->DirectionBodyToWorld( _dir[i]) * lambda[i];

  return res;
}

// Returns torque in world coordinate that was applied by joint to body. 
Vector3 HingeJointRB::GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const
{
  // torque is created by force in joint at contact 
  // plus torque in joint
  Vector3 force = GetForceInJoint(body0,body1,lambda);

  IRigidBody * body = (i == 0) ? body0 : body1;  

  Vector3 c = body->PositionBodyToWorld(_pos[i]) - body->GetMassCenter();
  Vector3 res = VZero;

  // torque in joint 
  for(int j = 0 ; j < 2; j++)
    res += body->DirectionBodyToWorld( _dir[j]) * lambda[j+3];

  // torque generated by force
  res += c.CrossProduct(force);  

  if (i == 0)
    res *= -1;

  return res;
}



//------------------------------------
// Hinge 2 Joint
//------------------------------------
Hinge2JointRB::Hinge2JointRB()
#if _ENABLE_CHEATS
: _color(Color(GRandGen.RandomValue(),GRandGen.RandomValue(),GRandGen.RandomValue()))
#endif
{
  _bodiesNotCollide = true;
}

Hinge2JointRB::Hinge2JointRB(const Hinge2JointRB& src) : base(src)
{
  _pos[0] = src._pos[0];
  _pos[1] = src._pos[1];

  _dir[0] = src._dir[0];
  _dir[1] = src._dir[1];
  _dir[2] = src._dir[2];  

  _axe0 = src._axe0;
  _axe1 = src._axe1;

  _coord0 = src._coord0;
  _coord1 = src._coord1;

  _c = src._c;
  _s = src._s;

  _bodiesNotCollide = src._bodiesNotCollide;
}

HierarchyItem * Hinge2JointRB::Clone() const
{
  Hinge2JointRB * clone = new Hinge2JointRB(*this);
  AfterClone(clone);
  return clone;
};

void Hinge2JointRB::Init(HierarchyIDVal scope, Vector3Val pos, Vector3Val axe0, Vector3Val axe1)
{
  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Assert(body0);
  Assert(body1); // must have second body

  _pos[0] = body0->PositionWorldToBody(pos);  
  _pos[1] = body1->PositionWorldToBody(pos);

  _c = axe0 * axe1;
  Vector3 wcoord = axe0.CrossProduct(axe1);
  _s = wcoord.Size();
  wcoord /= _s;

  _axe0 = body0->DirectionWorldToBody(axe0);  
  _axe1 = body1->DirectionWorldToBody(axe1);

  _coord0 = body0->DirectionWorldToBody(wcoord);
  _coord0.Normalize(); 

  _coord1 = -body1->DirectionWorldToBody(wcoord);;
  _coord1.Normalize();

  // set some fixed coordinates for now... in future here can 
  _dir[0] = VForward;
  _dir[1] = VUp;
  _dir[2] = VAside;
}

void Hinge2JointRB::AddLimitMotor(JointLimitMotorBase * limitMotor)
{
  if (_limitMotors.Size() == 2)
  {
    RptF("Cannot set more than 2 limit or motors for HingeJoint");
    return;
  }

  if (limitMotor->Dof() < 0 || limitMotor->Dof() > 1)
  {
    RptF("Cannot set limit or motor for %d for HingeJoint", limitMotor->Dof());
    return;
  }

  HierarchyJointRB::AddLimitMotor(limitMotor);
}

void Hinge2JointRB::GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info)
{
  info->m = 4; 

  for(int i = 0; i < _limitMotors.Size(); i++)
  {
    JointLimitMotorBase& limitMotor = * _limitMotors[i];
    if (limitMotor.IsPowered())
    {
      info->m++;
      continue;
    }

    if (limitMotor.IsLimit())
    {
      float val;
      if (limitMotor.Dof() == 0)
      {
        val = GetRotAngle0(body0, body1);
      }
      else
      {
        val = GetRotAngle1(body0, body1);
      }

      if (limitMotor.IsAtLimit(val))
        info->m++;      
    }
  }
}

void Hinge2JointRB::GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info)
{
  int s = info->rowskip;

  //-------------------------
  // Fix anchors ... 3 dof
  //-------------------------

  Vector3 wdir[3];
  for(int i = 0 ; i < 3; i++)
    wdir[i] = body0->DirectionBodyToWorld( _dir[i]);  

  // set linear part of J1l and J2l
  for(int i = 0; i < 3; i++)
  {
    for(int j = 0; j < 3; j++)
    {
      info->J1l[i * s + j] = wdir[i][j];        
    }
  }

  bool nonstaticBody1 = !body1->IsStatic();

  if (nonstaticBody1)
  {
    for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
      {
        info->J2l[i * s + j] = -wdir[i][j];   
      }
    }  
  }
  
  Vector3 wpos[2];
  wpos[0] = body0->PositionBodyToWorld(_pos[0]); 
  wpos[1] = body1->PositionBodyToWorld(_pos[1]);


  Vector3 c1,c2;
  c1 = wpos[0] - body0->GetMassCenter();    
  c2 = -(wpos[1] - body1->GetMassCenter()); // negative c2

  for (int i = 0; i < 3; i++)
  {
    dCROSS (info->J1a + i * s,=,c1,wdir[i]);   
  }

  if (nonstaticBody1)
  {
    for (int i = 0; i < 3; i++)
    {        
      dCROSS (info->J2a + i * s,=,c2,wdir[i]);
    }
  }

  // solve errors
  float erp = _erp >= 0 ? _erp : info->erp;  
  float k = info->fps * erp;

  Vector3 diff;
  diff = wpos[1] - wpos[0];

  for (int j=0; j<3; j++)
  {    
    info->c[j] = k * diff * wdir[j];
  }

  //-------------------------
  // Fix rotation ... 1 dof
  //-------------------------
  Vector3 axe0 = body0->DirectionBodyToWorld(_axe0);
  Vector3 axe1 = body1->DirectionBodyToWorld(_axe1);
  Vector3 rotAxe = axe0.CrossProduct(axe1);
  float sin = rotAxe.Size();
  rotAxe /= sin;
 
  for (int j = 0; j < 3; j++)
  {       
    info->J1a[j + 3 * s] = rotAxe[j];        
  }      

  if (nonstaticBody1)
  {  
    for (int j = 0; j < 3; j++)
    {          
      info->J2a[j + 3 * s] = -rotAxe[j];
    }
  }
  // compute the right hand side for the constrained rotational DOF.
  // axis 1 and axis 2 are separated by an angle `theta'. the desired
  // separation angle is theta0. sin(theta0) and cos(theta0) are recorded
  // in the joint structure. the correcting angular velocity is:
  //   |angular_velocity| = angle/time = erp*(theta0-theta) / stepsize
  //                      = (erp*fps) * (theta0-theta)
  // (theta0-theta) can be computed using the following small-angle-difference
  // approximation:
  //   theta0-theta ~= tan(theta0-theta)
  //                 = sin(theta0-theta)/cos(theta0-theta)
  //                 = (c*s0 - s*c0) / (c*c0 + s*s0)
  //                 = c*s0 - s*c0         assuming c*c0 + s*s0 ~= 1
  // where c = cos(theta), s = sin(theta)
  //       c0 = cos(theta0), s0 = sin(theta0)
  info->c[3] = k * (_c * sin - _s * (axe0 * axe1)); 

  // set cfm factor 
  float cfm = _cfm >= 0 ? _cfm : info->cfmCoef; 
  for(int i = 0; i < 4; i++)    
    info->cfm[i] = cfm;

  //-------------------------
  // Limits if needed rotation ... 1 dof
  //-------------------------
  int row = 4;
  for(int i = 0; i < _limitMotors.Size(); i++)
  {
    JointLimitMotorBase& limitMotor = * _limitMotors[i];
    float val;
    if (limitMotor.Dof() == 0)    
      val = GetRotAngle0(body0, body1);    
    else    
      val = GetRotAngle1(body0, body1);    

    if (limitMotor.IsPowered() || (limitMotor.IsLimit() && limitMotor.IsAtLimit(val)))
    {
      Vector3 axe;
      if (limitMotor.Dof() == 0)    
        axe = axe0;    
      else    
        axe = axe1;

      for (int j = 0; j < 3; j++)
      {       
        info->J1a[j + row * s] = axe[j];        
      }      

      if (nonstaticBody1)
      {  
        for (int j = 0; j < 3; j++)
        {          
          info->J2a[j + row * s] = -axe[j];
        }
      }

      limitMotor.Apply(*info,val,row);
      row++;
    }    
  }
}

float AngleFromCoord(float x, float y)
{
  float angl = asin(y);
  if (x < 0)
    angl = PI - angl;

  if (angl < 0)
    angl += 2* PI;

  return angl;
}

//! Returns relative rotation in axe0
float Hinge2JointRB::GetRotAngle0(IRigidBody * body0,IRigidBody* body1) const
{  
  Vector3 currentPos = _axe0.CrossProduct(body0->DirectionWorldToBody(body1->DirectionBodyToWorld(_axe1)));
  currentPos.Normalize();

  return AngleFromCoord(currentPos * _coord0, currentPos * (_axe0.CrossProduct(_coord0)));
}


//! Returns relative rotation in axe1
float Hinge2JointRB::GetRotAngle1(IRigidBody * body0,IRigidBody* body1) const
{
  Vector3 currentPos = _axe1.CrossProduct(body1->DirectionWorldToBody(body0->DirectionBodyToWorld(_axe0)));
  currentPos.Normalize();

  return AngleFromCoord(currentPos * _coord1, currentPos * (_axe1.CrossProduct(_coord1)));
}

// from body 0 to body 1
Vector3 Hinge2JointRB::GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const
{
  Vector3 res(VZero);
  for(int i = 0 ; i < 3; i++)
    res += body0->DirectionBodyToWorld( _dir[i]) * lambda[i];

  return res;
}

// Returns torque in world coordinate that was applied by joint to body. 
Vector3 Hinge2JointRB::GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const
{
  // torque is created by force in joint at contact 
  // plus torque in joint
  Vector3 force = GetForceInJoint(body0,body1,lambda);

  IRigidBody * body = (i == 0) ? body0 : body1;  

  Vector3 c = body->PositionBodyToWorld(_pos[i]) - body->GetMassCenter();
  Vector3 res = VZero;

  // torque in joint 
  res += body0->DirectionBodyToWorld(_axe0).CrossProduct(body1->DirectionBodyToWorld( _axe1)) * lambda[3];

  // torque generated by force
  res += c.CrossProduct(force);  

  if (i == 0)
    res *= -1;

  return res;
}

#if _ENABLE_CHEATS
void Hinge2JointRB::DiagDraw(HierarchyIDVal scope, float * lambda)
{
  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Vector3 pos0 = body0->PositionBodyToWorld(_pos[0]);
  Vector3 massCenter0 = body0->GetMassCenter();

  Vector3 dir = pos0 - massCenter0;
  float size = dir.Size();

  GScene->DrawDiagArrow(massCenter0, dir / size, size, _color);

  if (body1)
  {
    Vector3 pos1 = body1->PositionBodyToWorld(_pos[1]);
    Vector3 massCenter1 = body1->GetMassCenter();
    Vector3 dir = pos1 - massCenter1;
    float size = dir.Size();

    GScene->DrawDiagArrow(massCenter1, dir / size, size, _color);
  }  
}
#endif


//------------------------------------
// Universal Joint
//------------------------------------

#define IS_USED(used,i) ((used) & (0x1 << (i)))

UniversalJointRB::UniversalJointRB() 
#if _ENABLE_CHEATS
: _color(Color(GRandGen.RandomValue(),GRandGen.RandomValue(),GRandGen.RandomValue()))
#endif
{
  _bodiesNotCollide = true;
};

UniversalJointRB::UniversalJointRB(const UniversalJointRB& src)
: base(src)
{
  _pos[0] = src._pos[0];
  _pos[1] = src._pos[1];

  _dirPos[0] = src._dirPos[0];
  _dirPos[1] = src._dirPos[1];
  _dirPos[2] = src._dirPos[2];

  _dirRot[0] = src._dirRot[0];
  _dirRot[1] = src._dirRot[1];
  _dirRot[2] = src._dirRot[2];

  _qrel = src._qrel;
  _used = src._used;

  _bodiesNotCollide = src._bodiesNotCollide;

#if _ENABLE_CHEATS
  _color = src._color;
#endif
}

HierarchyItem * UniversalJointRB::Clone() const
{
  UniversalJointRB * clone = new UniversalJointRB(*this);
  AfterClone(clone);
  return clone;
};

//! Initialization pos, posDir, posRot in world coord
void UniversalJointRB::Init(HierarchyIDVal scope, unsigned char frozen, Vector3Val pos, Vector3* dirPos, Vector3* dirRot)
{
  const unsigned char all = 0x3f;
  if ((frozen & all) == 0)
  {
    // TODO: constrains can be also introduced by limits or motors. But still then can be sometimes
    // number of constrains 0. quickstep is now not able to handle that situation, but it can be easily changed. 
    RptF("Al least one dof must be constrained in UniversalJoint");
    return; 
  }

  _used = frozen;

  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Assert(body0);

  _pos[0] = body0->PositionWorldToBody(pos);
  if (body1)
    _pos[1] = body1->PositionWorldToBody(pos);


  // compute the relative orient between the bodies   
  _qrel = body0->GetOrientation().GetTransposed();
  if (body1)
    _qrel *= body1->GetOrientation();  

  if (dirPos)
  {
    for(int i = 0; i < 3; i++)
      _dirPos[i] = body0->DirectionWorldToBody(dirPos[i]);    
  }
  else
  {
    _dirPos[0] = VForward;
    _dirPos[1] = VUp;
    _dirPos[2] = _dirPos[0].CrossProduct(_dirPos[1]);
  }

  if (dirRot)
  {
    for(int i = 0; i < 3; i++)
      _dirRot[i] = body0->DirectionWorldToBody(dirRot[i]);    
  }
  else
  {
    _dirRot[0] = _dirPos[0];
    _dirRot[1] = _dirPos[1];
    _dirRot[2] = _dirPos[2];
  }
}

//! Appends limit or motor
void UniversalJointRB::AddLimitMotor(JointLimitMotorBase * limitMotor)
{
  if (IS_USED(_used,limitMotor->Dof()))
  {
    RptF("Cannot add limit or motor to constrained dof %d.",limitMotor->Dof());
    return;
  }

  if (limitMotor->IsLimit() && limitMotor->Dof() > 2)
  {
    // reason is the same like for BallJoint. Currently there is no code to determine value of rotation in axe... 
    RptF("Cannot set limit to rotational dof %d.",limitMotor->Dof());
    return;
  }

  HierarchyJointRB::AddLimitMotor(limitMotor);
}


void UniversalJointRB::GetInfo1 (IRigidBody * body0,IRigidBody* body1, Info1 *info)
{
  info->m = 0; 
  for(int i = 0;i < 6; i++)
  {
    if (IS_USED(_used,i))
      info->m++;
  }

  for(int i = 0; i < _limitMotors.Size(); i++)
  {
    const JointLimitMotorBase& limit = *_limitMotors[i];
    if (limit.IsPowered())
    {
      Assert(!IS_USED(_used, limit.Dof()));
      info->m++;
      continue;
    }

    if (limit.IsLimit())
    {
      Assert(!IS_USED(_used, limit.Dof()));
      if (limit.Dof() < 3)
      {
        // linear
        if (!body1)
          continue; // cannot calculate diff without body1

        Vector3 wdiff = body1->PositionBodyToWorld(_pos[1]) - body0->PositionBodyToWorld(_pos[0]);
        float val = wdiff * body0->DirectionBodyToWorld(_dirPos[limit.Dof()]);
        if (limit.IsAtLimit(val))
          info->m++;
      }
      else
      {
        // limit in rotations is not implemented see BallJointRB::AddLimitMotor       
      }      
    }
  }
}
//! Returns Jacobian, cfm...
void UniversalJointRB::GetInfo2 (IRigidBody * body0,IRigidBody* body1, Info2 *info)
{
  int s = info->rowskip;
  int row = 0; 

  //-----------------
  // Set linear dofs
  //-----------------

  // dir to world coord
  Vector3 wdirPos[3];
  for(int i = 0 ; i < 3; i++)
    wdirPos[i] = body0->DirectionBodyToWorld( _dirPos[i]);

  bool nonstaticBody1 = body1 && !body1->IsStatic();

  Vector3 wpos[2];
  wpos[0] = body0->PositionBodyToWorld(_pos[0]);
  if (body1)
    wpos[1] = body1->PositionBodyToWorld(_pos[1]);

  Vector3 c1,c2;
  c1 = wpos[0] - body0->GetMassCenter();  
  if (nonstaticBody1)
    c2 = -(wpos[1] - body1->GetMassCenter()); // negative c2

  // now found errors
  // set right hand side for the first three rows (linear) 
  float erp = _erp >= 0 ? _erp : info->erp;  
  float k = info->fps * erp;
  // set cfm factor 
  float cfm = _cfm >= 0 ? _cfm : info->cfmCoef; 

  // set linear part of J1l and J2l
  for(int i = 0; i < 3; i++)
  {
    if (IS_USED(_used, i))
    {    
      dCROSS (info->J1a + row * s,=,c1,wdirPos[i]);  
      for(int j = 0; j < 3; j++)
      {
        info->J1l[row * s + j] = wdirPos[i][j];        
      }

      if (nonstaticBody1)
      {
        dCROSS (info->J2a + row * s,=,c2,wdirPos[i]); 
        for(int j = 0; j < 3; j++)
        {
          info->J2l[row * s + j] = -wdirPos[i][j];   
        }     
      }

      if (body1)
      {
        // no correction without body1
        Vector3 diff;
        diff = wpos[1] - wpos[0];
        
        info->c[row] = k * (wdirPos[i] * diff);  
      }

      info->cfm[row] = cfm;
      row++;
    }
  }

  //-----------------
  // Set angular/rotational dofs
  //-----------------
  // error correction for angular part can be done only if all degrees are frozen !!!
  // TODO: make decomposition into Euler angles... and do error correction on them 
  // TODO: for reduction in two axes do Hinge2 like error correction...
  // TODO: for reduction in one axe do Hinge like error correction...
  Vector3 e(VZero);

  const unsigned char allRotations = 0x38;
  if (body1 && ((_used & allRotations) ==  allRotations))
  {

    // compute the right hand side. the first three elements will result in
    // relative angular velocity of the two bodies - this is set to bring them
    // back into alignment. the correcting angular velocity is
    //   |angular_velocity| = angle/time = erp*theta / stepsize
    //                      = (erp*fps) * theta
    //    angular_velocity  = |angular_velocity| * u
    //                      = (erp*fps) * theta * u
    // where rotation along unit length axis u by theta brings body 2's frame
    // to qrel with respect to body 1's frame. using a small angle approximation
    // for sin(), this gives
    //    angular_velocity  = (erp*fps) * 2 * v
    // where the quaternion of the relative rotation between the two bodies is
    //    q = [cos(theta/2) sin(theta/2)*u] = [s v]

    // get qerr = relative rotation (rotation error) between two bodies  
    QuaternionRB qq = body0->GetOrientation().GetTransposed(); 
    // a correction cannot be done without body 1
    qq *=  body1->GetOrientation();

    QuaternionRB qerr = qq *  _qrel.GetTransposed();

    if (qerr[0] < 0) {
      qerr[1] = -qerr[1];		// adjust sign of qerr to make theta small
      qerr[2] = -qerr[2];
      qerr[3] = -qerr[3];
    }

    Vector3 eLoc(qerr.Getx(), qerr.Gety(), qerr.Getz());
    e = body0->DirectionBodyToWorld(eLoc);
  }

  // dir to world coord
  Vector3 wdirRot[3];
  for(int i = 0 ; i < 3; i++)
    wdirRot[i] = body0->DirectionBodyToWorld( _dirRot[i]);

  for (int i = 0; i < 3; i++)
  {   
    if (IS_USED(_used, i + 3))
    {
      for (int j = 0; j < 3; j++)
      {       
        info->J1a[j + row * s] = wdirRot[i][j];        
      }      

      if (nonstaticBody1)
      {
        for (int j = 0; j < 3; j++)
        {          
          info->J2a[j + row * s] = -wdirRot[i][j];
        }      
      }

      info->c[row] += 2 * k * wdirRot[i] * e;
      info->cfm[row] = cfm;
      row++;
    }
  }

  //----------------------
  // Apply limit and motors if any
  //--------------------
  for(int i = 0; i < _limitMotors.Size(); i++)
  {
    JointLimitMotorBase& limitMotor = *_limitMotors[i];
    float val; 
    if (!limitMotor.IsPowered())
    {    
      if (limitMotor.IsLimit() && body1) // cannot limit wothout body1
      {
        Assert(limitMotor.Dof() < 3); // cannot limit rotation parts
        val = (wpos[1] - wpos[0]) * wdirPos[limitMotor.Dof()];
        if (!limitMotor.IsAtLimit(val))
          continue;
      }
      else
      { 
        continue;
      }

      if (limitMotor.Dof() < 3)
      {
        // linear part
        dCROSS (info->J1a + row * s,=,c1,wdirPos[limitMotor.Dof()]);  
        for(int j = 0; j < 3; j++)
        {
          info->J1l[row * s + j] = wdirPos[limitMotor.Dof()][j];        
        }

        if (nonstaticBody1)
        {
          dCROSS (info->J2a + row * s,=,c2,wdirPos[limitMotor.Dof()]); 
          for(int j = 0; j < 3; j++)
          {
            info->J2l[row * s + j] = -wdirPos[limitMotor.Dof()][j];   
          }     
        }
      }
      else
      {
        // angular rotational part
        for (int j = 0; j < 3; j++)
        {       
          info->J1a[j + row * s] = wdirRot[limitMotor.Dof() - 3][j];        
        }      

        if (nonstaticBody1)
        {
          for (int j = 0; j < 3; j++)
          {          
            info->J2a[j + row * s] = -wdirRot[limitMotor.Dof() - 3][j];
          }      
        }
      }

      limitMotor.Apply(*info,val, row);
      row++;
      Assert(row <= 6);
    }
  }
}

// from body 0 to body 1
Vector3 UniversalJointRB::GetForceInJoint(IRigidBody * body0,IRigidBody* body1, const float * lambda) const
{
  Vector3 res(VZero);
  for(int i = 0 ; i < 3; i++)
  {
    if (IS_USED(_used, i))
    {    
      res += body0->DirectionBodyToWorld( _dirPos[i]) * lambda[i];
    }
  }

  return res;
}

Vector3 UniversalJointRB::GetTorqueOnBody(IRigidBody * body0,IRigidBody* body1, int i,const float * lambda) const
{
  // torque is created by force in joint at contact 
  // plus torque in joint
  Vector3 force = GetForceInJoint(body0,body1,lambda);

  IRigidBody * body = (i == 0) ? body0 : body1;  

  Vector3 c = body->PositionBodyToWorld(_pos[i]) - body->GetMassCenter();
  Vector3 res = VZero;

  // torque in joint 
  for(int j = 0 ; j < 3; j++)
  {
    if (IS_USED(_used,j + 3))
      res += body->DirectionBodyToWorld( _dirRot[j]) * lambda[j+3];
  }

  // torque generated by force
  res += c.CrossProduct(force);  

  if (i == 0)
    res *= -1;

  return res;
}

#if _ENABLE_CHEATS
void UniversalJointRB::DiagDraw(HierarchyIDVal scope, float * lambda)
{
  Ref<IRigidBody> body0 = GetIRigidBody(scope, 0);
  Ref<IRigidBody> body1 = GetIRigidBody(scope, 1);

  Vector3 pos0 = body0->PositionBodyToWorld(_pos[0]);
  Vector3 massCenter0 = body0->GetMassCenter();

  Vector3 dir = pos0 - massCenter0;
  float size = dir.Size();

  GScene->DrawDiagArrow(massCenter0, dir / size, size, _color);

  if (body1)
  {
    Vector3 pos1 = body1->PositionBodyToWorld(_pos[1]);
    Vector3 massCenter1 = body1->GetMassCenter();
    Vector3 dir = pos1 - massCenter1;
    float size = dir.Size();

    GScene->DrawDiagArrow(massCenter1, dir / size, size, _color);
  }  
}
#endif
#endif //_ENABLE_RIGID_BODIES

