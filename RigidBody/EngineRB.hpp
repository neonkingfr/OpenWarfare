#ifdef _MSC_VER
#pragma once
#endif

#ifndef __ENGINERB_HPP__
#define __ENGINERB_HPP__
#if _ENABLE_RIGID_BODIES

#include <Poseidon/lib/objectHierarchy.hpp>
#include <Poseidon/lib/objectHierarchyExt.hpp>
#include "ODE/ODESim.hpp"
#include "jointrb.hpp"
#include "TriggerRB.hpp"

typedef ODESim EngineRB;
#endif
#endif //__ENGINERB_HPP__
