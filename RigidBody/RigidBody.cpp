#ifndef _MARINA_PROJECT 
#include <Poseidon/lib/wpch.hpp>
#else
#define _ENABLE_RIGID_BODIES 1
#endif

#if _ENABLE_RIGID_BODIES

#include "RigidBody.h"
#include "RBSimBase.hpp"

DEFINE_CASTING(JointRB);
//--------------------------------------
//  IRigidBodyState
//--------------------------------------

void IRigidBodyState::SetOrientationMassCenter(const Vector3& pos,const QuaternionRB& orientation)
{ 
  SetOrientation(orientation);
  SetMassCenter(pos);
}

/*Vector3 IRigidBodyState::CalculateTorgue(const Vector3& cForce, const Vector3& cPoint) const
{
  return (cPoint - GetMassCenter()).CrossProduct(cForce);
}*/

Vector3 IRigidBodyState::SpeedAtPoint(const Vector3& cPoint) const
{
  /*	Log("SpeedAtPoint: _cMomentum(%f,%f), fInvMass(%f), cPoint(%f,%f), cPod(%f,%f), fL(%f), fInvInertia(%f,%f)\n",
  _cMomentum.Getx(), _cMomentum.Gety(), fInvMass, cPoint.Getx(), cPoint.Gety(),
  GetPos().Getx(), GetPos().Gety(), _fL, fInvInertiaTensor);*/
  return GetVelocity() - ((cPoint - GetMassCenter()).CrossProduct(GetAngularVelocity()));
}

Vector3 IRigidBodyState::AccelerationAtPoint(const Vector3& cForceBank, const Vector3& cTorgueBank, const Vector3& cPoint, TRIBOOL bPlus) const
{		
  Vector3 cAccel = cForceBank * GetInvMass() +  // Force
    (GetActualInvInertiaTensor() * (cTorgueBank - GetAngularVelocity().CrossProduct(GetAngularMomentum()))).CrossProduct(cPoint - GetMassCenter()); // Torgue

  switch (bPlus)
  {
  case TB_TRUE:
    cAccel += ((cPoint - GetMassCenter()).CrossProduct(GetAngularVelocity())).CrossProduct(GetAngularVelocity()); // dostredive acceleration
    break;
  case TB_FALSE:
    cAccel += - ((cPoint - GetMassCenter()).CrossProduct(GetAngularVelocity())).CrossProduct(GetAngularVelocity()); // dostredive acceleration
    break;
  default:
    ;
  }

  return cAccel;
}

Vector3 IRigidBodyState::DiffAccelerationAtPointOnForceAndTorque( const Vector3& cAccelPoint, 
                                                                 const Vector3& cForce, 
                                                                 const Vector3& cTorque) const
{
  Vector3 cAccel = cForce * GetInvMass() + (GetActualInvInertiaTensor() * cTorque).CrossProduct(cAccelPoint - GetMassCenter());
  return cAccel;
}

Vector3 IRigidBodyState::DiffSpeedAtPointOnImpulseAndTorque( const Vector3& cSpeedPoint, 
                                                            const Vector3& cImpulse, 
                                                            const Vector3& cImpulseTorque) const
{	
  return cImpulse * GetInvMass() + (GetActualInvInertiaTensor() * cImpulseTorque).CrossProduct(cSpeedPoint - GetMassCenter());	
}

void IRigidBodyState::DiffAccelerationAtPointOnForceAtPoint(Vector3& cAccel, 
                                                            const Vector3& cAccelPoint, 
                                                            const Vector3& cForce, 
                                                            const Vector3& cForcePoint) const
{
  cAccel = cForce * GetInvMass() + (GetActualInvInertiaTensor() * ((cForcePoint - GetMassCenter()).CrossProduct(cForce) )).CrossProduct(cAccelPoint - GetMassCenter());
}



float IRigidBodyState::CalculateEnergy()
{
  return (GetVelocity().SquareSize() * GetMass() + (GetAngularVelocity() * GetAngularMomentum())) / 2;
}

void IRigidBodyState::SlowDown(float coef)
{
  SetVelocity(GetVelocity() * coef);
  SetAngularVelocity(GetAngularVelocity() * coef);  
}


//--------------------------------------
//  IRigidBody
//--------------------------------------
DEFINE_CASTING(IRigidBody);
/*
Ref<BBNode> IRigidBody::AnimateBBTree(IRBGeometry& geom, bool createcopy)
{
  Ref<BBNode> node = geom.GetBBTree(); 
  if (createcopy)
    node = node->clone();

  Matrix4 trans;
  trans.SetOrientation(GetOrientation().GetMatrix());
  trans.SetPosition(GetPos());
  node->RecomputeFromSrc(trans);

  return node;
}

IRBGeometry * IRigidBody::AnimateGeom(IRBGeometry& geom, bool createcopy)
{
  IRBGeometry * retGeom = &geom;
  if (createcopy)
    retGeom = geom.Clone();

  retGeom->SetOrigin(GetPos(), GetOrientation().GetMatrix()); 
  retGeom->SetBaseID(GetBaseID());
  return retGeom;
}

void IRigidBody::AddForceAtPointToBanks(const Vector3& cForce, const Vector3& cPoint)
{
  _forceBank += cForce;
  _torqueBank += CalculateTorgue(cForce,cPoint);
  Assert(_forceBank.IsFinite());
  Assert(_torqueBank.IsFinite());
}
*/

/*
void IRigidBody::AddCorrectionForceAtPointToBanks(const Vector3& cForce, const Vector3& cPoint)
{
_cCorrectionForceBank += cForce;
_cCorrectionTorgueBank += CalculateTorgue(cForce,cPoint);
}*/


/*void IRigidBody::PrepareIntegration()
{
_posBackUp = GetMassCenter();
_orientationBackUp = GetOrientation();

_velocityBackUp = GetVelocity();
_angularMomentumBackUp = GetAngularMomentum();

_acceleration = _forceBank * GetInvMass();
_torque = _torqueBank;	

_orientationVelocity = QuaternionRB(GetAngularVelocity()) * GetOrientation() * 0.5f;

_orientationAcceleration = QuaternionRB(GetActualInvInertiaTensor() * (_torqueBank - (GetAngularVelocity().CrossProduct(GetAngularMomentum())))) * GetOrientation() * 0.5f;
_orientationAcceleration += GetOrientation() * (GetAngularVelocity().SquareSize() / 4.0f);		
}*/

/*bool IRigidBody::IsMoving() const
{
return !IsNumericalZero(GetVelocity().Size()/10000.0f) || !IsNumericalZero(_acceleration.Size()/10000.0f) || 
!IsNumericalZero(_orientationVelocity.Size()/10000.0f) || !IsNumericalZero(_orientationAcceleration.Size()/10000.0f);
// TODO: Some real conditions
}*/
/*
void IRigidBody::DoIntegration(float fTime)
{
SetMassCenter(_posBackUp + GetVelocity() * fTime
+ _acceleration * fTime * fTime / 2);

QuaternionRB orient = _orientationBackUp + _orientationVelocity * fTime;
orient += _orientationAcceleration * (fTime * fTime / 2.0f);
orient.Normalize();
SetOrientationMassCenter(orient);

SetVelocity(GetVelocity() + _acceleration * fTime);
//MomentumFromVelocity();

SetAngularMomentum(GetAngularMomentum() + _torque * fTime);
ActualizeTensors();
AngularVelocityFromAngularMomentum();
}*/
#endif
