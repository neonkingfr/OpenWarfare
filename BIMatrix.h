#ifndef __BI_MATRIX__
#define __BI_MATRIX__

#include "BISdkCommon.h"
#include "BIVector.h"

/** 4D BI_Matrix consisting only of zeros 
*/
#define BI_ZeroMatrix4 BI_TMatrix4<float> (  0,0,0,	\
   								   0,0,0,	\
							       0,0,0,	\
							       0,0,0)	

#define BI_ZeroMatrix4d BI_TMatrix4<double> (  0,0,0,	\
  0,0,0,	\
  0,0,0,	\
  0,0,0)	

/** Identity 3D BI_Matrix with las column filled with zeroes
*/
#define BI_IdentityMatrix4 BI_TMatrix4<float>(  1,0,0,	\
									  0,1,0,	\
									  0,0,1,	\
									  0,0,0)

#define BI_IdentityMatrix4d BI_TMatrix4<double>(  1,0,0,	\
  0,1,0,	\
  0,0,1,	\
  0,0,0)
/** Identity 3D BI_Matrix 
*/
#define BI_IdentityMatrix3 BI_TMatrix3<float> (  1,0,0,	\
									   0,1,0,	\
									   0,0,1);
#define BI_IdentityMatrix3d BI_TMatrix3<double> (  1,0,0,	\
  0,1,0,	\
  0,0,1);
/* 3D BI_Matrix consisting only of zeros */
#define BI_ZeroMatrix3 BI_TMatrix3<float> (  0,0,0,	\
								   0,0,0,	\
								   0,0,0)

#define BI_ZeroMatrix3d BI_TMatrix3<double> (  0,0,0,	\
  0,0,0,	\
  0,0,0)


// Euler rotation orders 
// These rotation schemes assume a static coordinate frame and no repeating axes
enum { 
  XYZ = 0,     // Rxyz = Rz * Ry * Rx
  XZY = 4,     // Rxzy = Ry * Rz * Rx
  YZX = 8,     // Ryzx = Rx * Rz * Ry
  YXZ = 12,    // Ryxz = Rz * Rx * Ry
  ZXY = 16,    // Rzxy = Ry * Rx * Rz
  ZYX = 20     // Rzyx = Rx * Ry * Rz 
};

// Helper function to get info about euler rotation order for the various 
// conversion functions between euler angles and matrices or quaternions.
static int EulerFirst[] = { 0, 1, 2, 0 };
static int EulerNext[] = { 1, 2, 0, 1 };

/** \brief Class handling operation with 3D matrix */
/** Matrix is defined by axis up, direction and aside. 
 * Class cooperates with BI_Vectors
 */
template<class Real>
class __declspec( dllexport ) BI_TMatrix3
{
protected:
	/** Columns of matrix 
	*/
	BI_TVector3<Real> _columns[3];

	/** Rows is represented by a vector */
	typedef BI_TVector3<Real> Row;
	typedef BI_TVector3<Real> Column;

public:
	/** \brief Enum deciding where are located axis */
	enum Axis { _Aside, _Up, _Direction };

	/**  Constructor */
	/** Constructs Matrix from values. Every triple value in sequence represents column.
	 */
	BI_TMatrix3<Real>(Real x1 =0, Real x2 =0, Real x3 =0, Real y1 =0, Real y2 =0, Real y3=0, Real z1 =0, Real z2 =0, Real z3 =0)
	{
		Set(0, 0) = x1;		Set(0, 1) = x2;		Set(0, 2) = x3;
		Set(1, 0) = y1;		Set(1, 1) = y2;		Set(1, 2) = y3;
		Set(2, 0) = z1;		Set(2, 1) = z2;		Set(2, 2) = z3;
	}
	/** \brief Contructor via _columns */
	/** Constructs matrix when there are given columns
	 */
	BI_TMatrix3<Real>(const BI_TVector3<Real>& column1, const BI_TVector3<Real> & column2, const BI_TVector3<Real> & column3)
	{
		memcpy( &_columns[0], &column1, sizeof(Real)*3 ); 
		memcpy( &_columns[1], &column2, sizeof(Real)*3 );
		memcpy( &_columns[2], &column3, sizeof(Real)*3 );
	}
	/** \brief Copy contructor  */
	/** Copies all data from source
	 */
	BI_TMatrix3<Real>(const BI_TMatrix3<Real> &src)	{ memcpy( _columns, src._columns, sizeof(BI_TVector3<Real>)*3); }

  /** \brief Access to element of matrix */
	/** \return reference to matrix element for further modifying
	 */
  BI_TVector3<Real>& Set(int axis, BI_TVector3<Real> value) { return _columns[axis] = value; }

  /** \brief  get  the value of corresponding axis */
  BI_TVector3<Real> Get(int axis) const { return _columns[axis]; }

  BI_TMatrix3<Real> InvertedRotation()const
  {
    return Transposed();
  }
  /** \brief create orthogonal matrix from given axis */
  /** \param fixedAxis will is the axis that will be only normalized
   *  \param otherAxis will be changed so that it will be perpendicular to the fixedAxis 
   *  \return reference to created matrix 
   */
  BI_TMatrix3<Real>& CreateOrthogonal(Axis fixedAxis, Axis otherAxis )
  {
    Get(fixedAxis).Normalize();
    double cosx = Get(fixedAxis).DotProduct(Get(otherAxis));
    Set(otherAxis,Get(otherAxis) - Get(fixedAxis)*cosx);
    int lastIndex = 3 - fixedAxis - otherAxis;
    Set(lastIndex,Get(fixedAxis).CrossProduct(Get(otherAxis)));
    return *this;
  }

	/** \brief Access to element of matrix */
	/** \return reference to matrix element for further modifying
	 */
	Real& Set(int row, int column) { return _columns[column][row]; }

	/** \brief Setting element value */
	/** Sets value at row and column. There is no return value and thus no further modifying 
	 */
	void Set(int row, int column, Real value)
	{
		Set(row, column) = value;
	}

	/** \brief Setting element value */
	/** Sets value at row and column. There is no return value and thus no further modifying 
	 */
	Real Get(int row, int column)const
	{
		return _columns[column][row];
	}
	/** \brief Sets identity */
	/** Changes to identity matrix */
  void SetIdentity()
  { 
    *this = BI_TMatrix3<Real>(1,0,0,
                              0,1,0,
                              0,0,1);
  } 

	/** \brief Sets rotation in X axis */
	/** \param Angle is angle between vector and X axis
	 */
	void SetRotationX( Real angle )
	{
		SetIdentity();
		Real s = sin(angle), c = cos(angle);
		Set(1,1)=+c,Set(1,2)=-s;
		Set(2,1)=+s,Set(2,2)=+c;
	}
	/** \brief Creates scaling matrix */
	void SetScale(Real x, Real y, Real z)
	{
		*this = BI_ZeroMatrix3;
		_columns[0][0] = x;
		_columns[1][1] = y;
		_columns[2][2] = z;
	}
	/** \brief Sets rotation in Y axis */
	/** \param angle is angle between vector and Y axis
	 * \note To achieve the same effect in VBS2, please use he negative angle
	 */
	void SetRotationY( Real angle )
	{
		SetIdentity();
		Real s=(Real)sin(angle),c=(Real)cos(angle);
		Set(0,0)=+c,Set(0,2)=-s;
		Set(2,0)=+s,Set(2,2)=+c;
	}

	/** \brief Sets rotation in Z axis */
	/** \param angle is angle between vector and Z axis
	 */
	void SetRotationZ( Real angle )
	{
		SetIdentity();
		Real s=(Real)sin(angle),c=(Real)cos(angle);
		Set(0,0)=+c,Set(0,1)=-s;
		Set(1,0)=+s,Set(1,1)=+c;
	}

	/** \brief Sets rotation according to arbitrary axis */
	/** \param axis 
	 * \param angle is angle between vector and  
	 */
	void SetRotationByAxis(const BI_TVector3<Real> & axis, Real angle)
	{
		// Normalize input vector
		BI_TVector3<Real> axisN = axis;
		axisN.Normalize();

		// Convert axis and angle into a quaternion (w, x, y, z)
		Real halfAngle = angle * 0.5f;
		Real w = cos(halfAngle);
		Real sinHalfAngle = sin(halfAngle);
		Real x = sinHalfAngle * axisN.X();
		Real y = sinHalfAngle * axisN.Y();
		Real z = sinHalfAngle * axisN.Z();

		// Convert the quaternion into the matrix
		Real wx = w*x*2;
		Real wy = w*y*2;
		Real wz = w*z*2;
		Real xx = x*x*2;
		Real xy = x*y*2;
		Real xz = x*z*2;
		Real yy = y*y*2;
		Real yz = y*z*2;
		Real zz = z*z*2;

		Set(0, 0) = 1 - yy - zz;
		Set(0, 1) = xy - wz;
		Set(0, 2) = xz + wy;
		Set(1, 0) = xy + wz;
		Set(1, 1) = 1 - xx - zz;
		Set(1, 2) = yz - wx;
		Set(2, 0) = xz - wy;
		Set(2, 1) = yz + wx;
		Set(2, 2) = 1 - xx - yy;
	}


	/** \brief Transpose matrix  */
	/** \return transposed matrix
	 */
	BI_TMatrix3<Real> Transposed()const 
	{
		BI_TMatrix3<Real> m(*this);
		for (int i = 0; i < 3; i++)
			for ( int j =0; j < 3; j++)
			{
				m._columns[j][i] = _columns[i][j];
			}
			return m;
	}

  BI_TMatrix3<Real> Transpose()
  {
    *this = Transposed();
    return *this;
  }
	/** \brief Fetch column of matrix */
	/** \return BI_Vector3 representing column of the matrix
	 */
	const BI_TVector3<Real> & GetColumn(int idx)const { return _columns[idx]; }
	BI_TVector3<Real>& GetColumn(int idx) { return _columns[idx]; }

	/** \brief Fetch rows of the matrix  */
	BI_TVector3<Real> GetRow(int idx)const { return BI_TVector3<Real>(_columns[0][idx],_columns[1][idx],_columns[2][idx]); }

	/** \brief Computes determinant */
	/** \return float result
	 */
	Real Determinant()const
	{ 
		return Get(0,0)*( Get(1,1)*Get(2,2) - Get(1,2)*Get(2,1) )
			 + Get(0,1)*( Get(1,2)*Get(2,0) - Get(1,0)*Get(2,2) )
			 + Get(0,2)*( Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0) );
	}

	/** \brief Checks, if the matrix can be inverted */
	 /** \return true, if the natrix is invertible, false otherwise
	 */
	bool Invertible()const {	return Determinant() != 0; }

	/** \brief Gets up axis */
	const BI_TVector3<Real>& GetUp()const { return _columns[_Up]; }

	/** \brief Gets up axis */
	BI_TVector3<Real>& GetUp() { return _columns[_Up]; }

	/** \brief Gets aside axis */
	const BI_TVector3<Real>& GetAside()const { return _columns[_Aside]; }
	BI_TVector3<Real>& GetAside() { return _columns[_Aside]; }

	/** \brief Gets direction axis */
	const BI_TVector3<Real>& GetDirection()const { return _columns[_Direction]; }
	BI_TVector3<Real>& GetDirection() { return _columns[_Direction]; }

  void  SetDirection (const BI_TVector3<Real>& vDir)
  {
    _columns[_Direction] = vDir;
  }

  void  SetUp (const BI_TVector3<Real>& vUp)
  {
    _columns[_Up] = vUp;
  }

  void  SetAside (const BI_TVector3<Real>& vAside)
  {
    _columns[_Aside] = vAside;
  }

  /** \brief Sets orientation */
  void SetDirectionAndUp(const BI_TVector3<Real>& vDir, const BI_TVector3<Real>& vUp)
  {
    _columns[_Direction] = vDir.Normalized();    
    // Project into the plane
    float t = vUp.DotProduct(_columns[_Direction]);
    _columns[_Up] = (vUp-GetDirection()*t).Normalized();
    // Calculate the vector pointing along the x axis (i.e. aside)
    _columns[_Aside] = GetUp().CrossProduct(GetDirection());
  }

  /** \brief Sets orientation */
  void SetDirectionAndAside(const BI_TVector3<Real>& vDir, const BI_TVector3<Real>& vAside)
  {
    _columns[_Direction] = vDir.Normalized();    
    // Project into the plane
    float t = vAside.DotProduct(_columns[_Direction]);
    _columns[_Aside] = (vAside-GetDirection()*t).Normalized();
    // Calculate the vector pointing along the x axis (i.e. aside)
    _columns[_Up] = GetDirection().CrossProduct(GetAside());
  }

	/** \brief Sets rotation matrix */
	void SetRotationAxis(const BI_TVector3<Real> & axis, Real angle)
	{
		// Normalize input vector
		BI_TVector3<Real> axisN = axis;
		axisN.Normalize();

		// Convert axis and angle into a quaternion (w, x, y, z)
		Real halfAngle = angle * 0.5f;
		Real w = cos(halfAngle);
		Real sinHalfAngle = sin(halfAngle);
		Real x = sinHalfAngle * axisN.X();
		Real y = sinHalfAngle * axisN.Y();
		Real z = sinHalfAngle * axisN.Z();

		// Convert the quaternion into the matrix
		Real wx = w*x*2;
		Real wy = w*y*2;
		Real wz = w*z*2;
		Real xx = x*x*2;
		Real xy = x*y*2;
		Real xz = x*z*2;
		Real yy = y*y*2;
		Real yz = y*z*2;
		Real zz = z*z*2;

		Set(0, 0) = 1 - yy - zz;
		Set(0, 1) = xy - wz;
		Set(0, 2) = xz + wy;
		Set(1, 0) = xy + wz;
		Set(1, 1) = 1 - xx - zz;
		Set(1, 2) = yz - wx;
		Set(2, 0) = xz - wy;
		Set(2, 1) = yz + wx;
		Set(2, 2) = 1 - xx - yy;
	}
	/** \brief Invert matrix */
	/** \return valid BI_Matrix3. If matrix is invertible, returns inverted matrix, 
	 * otherwise return null matrix, which seems to be invalid the same way like input...
	 */
	BI_TMatrix3<Real> Inverted() const
	{
		BI_TMatrix3<Real> m(*this);
		return m.Invert();
	};

	/** \brief Invert matrix */
	/** \return valid BI_Matrix3. If matrix is invertible, returns inverted matrix, 
	 * otherwise return null matrix, which seems to be invalid the same way like input...
	 */
	BI_TMatrix3<Real>& Invert()
	{
		if (!Invertible())
		{
			(*this) = BI_ZeroMatrix3;
			return *this;
		}
		Row rv1(Get(1,1)* Get(2,2) - Get(1,2)*Get(2,1), Get(1,0)*Get(2,1) - Get(1, 1)* Get(1,0),Get(1,0)*Get(2,1) - Get(1,1)* Get(1,0));
		Row rv2(Get(1,2)*Get(2,0) - Get(1,0)*Get(2,2), Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0),Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0));
		Row rv3(Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0), Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0),Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0));
		BI_TMatrix3<Real> inv(rv1,rv2,rv3);
		(*this) = inv/Determinant();
		return (*this);
	};

  /** \brief Set general inverted matrix */
  void SetInvertGeneral3x3(BI_TMatrix3<Real>& op)
  {
    // calculate inversion using Gauss-Jordan elimination
    BI_TMatrix3<Real> a=op;
    // load result with identity
    SetIdentity();
    int row,col;
    // construct result by pivoting
    // pivot column
    for( col=0; col<3; col++ )
    {
      // use maximal number as pivot
      float max=0;
      int maxRow=col;
      for( row=col; row<3; row++ )
      {
        float mag=fabs(a.Get(row,col));
        if( max<mag ) max=mag,maxRow=row;
      }
      if( max<=0.0 ) continue; // no pivot exists
      // swap lines col and maxRow
      swap(a.Set(col,0),a.Set(maxRow,0));
      swap(a.Set(col,1),a.Set(maxRow,1));
      swap(a.Set(col,2),a.Set(maxRow,2));
      swap(Set(col,0),Set(maxRow,0));
      swap(Set(col,1),Set(maxRow,1));
      swap(Set(col,2),Set(maxRow,2));
      // use a(col,col) as pivot
      float quotient=1/a.Get(col,col);
      // make pivot 1
      a.Set(col,0)*=quotient,a.Set(col,1)*=quotient,a.Set(col,2)*=quotient;
      Set(col,0)*=quotient,Set(col,1)*=quotient,Set(col,2)*=quotient;
      // use pivot line to zero all other lines
      for( row=0; row<3; row++ ) if( row!=col )
      {
        float factor=a.Get(row,col);
        a.Set(row,0)-=a.Get(col,0)*factor;
        a.Set(row,1)-=a.Get(col,1)*factor;
        a.Set(row,2)-=a.Get(col,2)*factor;
        Set(row,0)-=Get(col,0)*factor;
        Set(row,1)-=Get(col,1)*factor;
        Set(row,2)-=Get(col,2)*factor;
      }
    }
  }

  /** \brief Computes sum of square distances in all direction*/
	Real Distance2(const BI_TMatrix3<Real> &mat) const
	{
		return _columns[0].Distance2(mat._columns[0])+_columns[1].Distance2(mat._columns[1])+_columns[2].Distance2(mat._columns[2]);
	}

  inline void EulerGetOrder(int o, int &i, int &j, int &k, int &p) const
  { 
    o >>= 2;
    p = o & 1;
    o >>= 1;

    i = EulerFirst[o & 3];
    j = EulerNext[i + p];
    k = EulerNext[i + 1 - p]; 

    p = (p == 0) ? 1 : -1;
  }

  void GetEulerAngles(Real &angleX, Real &angleY, Real &angleZ, int rotScheme = 0) const
  {
    // Code taken from Tmath library (Patricia Denbrook)
    // Converts rotation matrix to euler angles, using the rotation order specified by 
    // the euler object.  The conversion assumes a fixed coordinate frame; if the 
    // application requires a rotating reference frame, transpose the input matrix
    // prior to calling this function.
    // This algorithm is adapted from code by Ken Shoemake, Graphics Gems IV, pp 222-229.

    int i, j, k;	// permutation of X, Y, Z
    int p;			  // parity: 1 if order follows XYZXY, else -1

    EulerGetOrder(rotScheme, i, j, k, p);

    float e[3];
    float cy = sqrt(Get(i, i) * Get(i, i) + Get(j, i) * Get(j, i));
    if (cy > 0.000001)
    {
      e[i] = atan2( (float)Get(k, j), (float)Get(k, k));
      e[j] = atan2(-(float)Get(k, i), (float)cy);
      e[k] = atan2( (float)Get(j, i), (float)Get(i, i));
    }
    else
    {
      e[i] = atan2(-(float)Get(j, k), (float) Get(j, j));
      e[j] = atan2(-(float)Get(k, i), (float)cy);
      e[k] = 0.0f;
    }

    angleX = e[0] * p;
    angleY = e[1] * p;
    angleZ = e[2] * p;
  }

  void SetEulerAngles(Real angleX, Real angleY, Real angleZ, int rotScheme = 0)
  {
    // Code taken from Tmath library (Patricia Denbrook)
    // Converts euler angles to a rotation matrix, using the rotation order specified by 
    // the euler object.  The conversion assumes a fixed coordinate frame; if the 
    // applications requires a rotating reference frame, the resulting matrix will need 
    // to be transposed upon return from this function.
    // This algorithm is adapted from code by Ken Shoemake, Graphics Gems IV, pp 222-229.

    int i, j, k;	// permutation of X, Y, Z
    int p;			  // parity: 1 if order follows XYZXY, else -1

    EulerGetOrder(rotScheme, i, j, k, p);

    float e[3] = {angleX, angleY, angleZ};

    double ti = e[i] * p;
    double tj = e[j] * p;
    double tk = e[k] * p;

    double ci = cos(ti);
    double cj = cos(tj);
    double ck = cos(tk);

    double si = sin(ti);
    double sj = sin(tj);
    double sk = sin(tk);

    double cc = ci * ck;
    double cs = ci * sk;
    double sc = si * ck;
    double ss = si * sk;

    Set(i, i) = cj * ck;
    Set(i, j) = sj * sc - cs;
    Set(i, k) = sj * cc + ss;

    Set(j, i) = cj * sk;
    Set(j, j) = sj * ss + cc;
    Set(j, k) = sj * cs - sc;

    Set(k, i) = -sj;
    Set(k, j) = cj * si;
    Set(k, k) = cj * ci;
  }

  void SetInvertRotation( const BI_TMatrix3<Real>& op )
  {
    for( int i=0; i<3; i++ )
    {
      Set(i,0)=op.Get(0,i);
      Set(i,1)=op.Get(1,i);
      Set(i,2)=op.Get(2,i);
    }
  }

  inline Real Inv( Real c ) { return 1 / c; }

  inline Real InvSquareSize( Real x, Real y, Real z )
  {
    return Inv(x * x + y * y + z * z);
  }

  void SetInvertScaled( const BI_TMatrix3<Real>& op )
  {
    // matrix inversion is calculated based on these prepositions:
    // matrix is S*R, where S is scale, R is rotation
    // inversion of such matrix is Inv(R)*Inv(S)
    // Inv(R) is Transpose(R), Inv(S) is C: C(i,i)=1/S(i,i)
    // sizes of row(i) are scale coeficients a,b,c
    // all member are set
    //SetIdentity();

    // calculate scale
    Real invScale0=InvSquareSize(op.Get(0,0),op.Get(0,1),op.Get(0,2));
    Real invScale1=InvSquareSize(op.Get(1,0),op.Get(1,1),op.Get(1,2));
    Real invScale2=InvSquareSize(op.Get(2,0),op.Get(2,1),op.Get(2,2));
    // invert rotation and scale
    for( int i=0; i<3; i++ ) //for( int j=0; j<3; j++ )
    {
      Set(i,0)=op.Get(0,i)*invScale0;
      Set(i,1)=op.Get(1,i)*invScale1;
      Set(i,2)=op.Get(2,i)*invScale2;
    }
  }

	/** \name operators on BI_Matrix3 alone */	
	/** \{ */
	/** \brief Adding to BI_Matrix3 */
	/** Values in BI_Matrix3 elements will be increased by corresponding elements from src
	 * \param src source matrix
	 * \return reference to this changed matrix
	 */
	BI_TMatrix3<Real> & operator+=(const BI_TMatrix3<Real>& src)
	{
		for ( int i =0; i<3;i++)
				_columns[i] += src._columns[i];
		return *this;
	}

	/** \brief Operation plus */
	/** \return BI_Matrix3 whose elements are increased by source matrix
	 */
	const BI_TVector3<Real>& operator[](int idx)const  { return _columns[idx]; }
  BI_TVector3<Real>& operator[](int idx) { return _columns[idx]; }
	BI_TMatrix3<Real> operator+(const BI_TMatrix3<Real>& src)
	{
		BI_TMatrix3<Real> m(*this);
		m+=src;
		return m;
	}

	/** \brief Operation minus */
	/** \return BI_Matrix3 whose elements are decreased by source matrix
	 */
	BI_TMatrix3<Real> & operator-=(const BI_TMatrix3<Real>& src)
	{
		for ( int i =0; i<3;i++)
				_columns[i] -= src._columns[i];
		return *this;
	}

	/** \brief Operation minus */
	/** \return BI_Matrix3 whose elements are decreased by source matrix
	 */
	BI_TMatrix3<Real> operator-(const BI_TMatrix3<Real>& src)const
	{
		BI_TMatrix3<Real> m(*this);
		m-=src;
		return m;
	}
	
	/** \brief Multiplying matrix */
	/** \param src parameter that every element of matrix will be multiplied by
	 * \return reference to this changed matrix
	 */
	BI_TMatrix3<Real> & operator*=(const Real & src)
	{
		for ( int i =0; i<3; i++)
				_columns[i]*=src;
		return *this;
	}

	/** \brief Operation multiply */
	/** \param f parameter that every element of the matrix will be multiplied by
	 * \return BI_Matrix3 whose elements are multiplied by src. BI_Matrix3 itself remains untouched
	 */
	BI_TMatrix3<Real> & operator*(const Real & f)const
	{
		BI_TMatrix3<Real> m(*this);
		return m*=f;
	}
	/** \brief BI_Matrix3 multiplication */
	/** \param src source matrix
	  * \return changed matrix 
	  */
	BI_TMatrix3<Real> & operator*=( const BI_TMatrix3<Real> & src)
	{
		BI_TMatrix3<Real> hlp(*this);
		for ( int i = 0; i< 3; i++)
		{
			BI_TVector3<Real> v = src.GetColumn(i);
			v = hlp*v;
			memcpy(&_columns[i],&v, sizeof(Real)*3);
		}
		return (*this);
	}
	/** \brief BI_Matrix3 multiplication  */
	/** \param src source matrix
	  * \return changed matrix 
	  */
	BI_TMatrix3<Real> operator*(const BI_TMatrix3<Real> & src)const
	{
		BI_TMatrix3<Real> m(*this);
		m*=src;
		return m;
	}

	/** \brief Matrix multiplication by BI_Vector3 */
	/** \param v vector that matrix will be multiplicated by
	 */
	BI_TVector3<Real> operator*(const BI_TVector3<Real> & v)const
	{
		BI_TVector3<Real> r1 = GetRow(0);
		BI_TVector3<Real> r2 = GetRow(1);
		BI_TVector3<Real> r3 = GetRow(2);
		return BI_TVector3<Real>(r1.DotProduct(v), r2.DotProduct(v), r3.DotProduct(v));
	}

	/** \brief Division BI_Matrix3 &*/
	/** \param f parameter that divides every element of the matrix
	 * \return reference to this changed matrix
	 */
	BI_TMatrix3<Real> operator/=(const Real & f)
	{
		Real t = 1/f;
		return (*this)*t;
	}

	/** \brief Division BI_Matrix3 */
	/** \return matrix whose every element is divided by \param f
	 */
	BI_TMatrix3<Real> operator/(const Real & f)const
	{
		BI_TMatrix3<Real> m(*this);
		return m*(1/f);
	}

  bool operator==( const BI_TMatrix3<Real> other )const
  {
    bool res = true;
    for ( int i =0; i < 3; i++)
      res&= other.GetColumn(i) == this->GetColumn(i);
    return res;
  }
  bool operator!=( const BI_TMatrix3<Real> other )const
  {
    return !(*this == other);
  }

	/** \} */

  template<class MatrixClass>
  MatrixClass Convert()
  {
    MatrixClass s(Get(0,0),Get(0,1),Get(0,2),
                  Get(1,0),Get(1,1),Get(1,2),
                  Get(2,0),Get(2,1),Get(2,2));
    return s;
  }
  template <class MatrixClass>
  void Convert(MatrixClass m)
  {
    for ( int i =0; i < 0; i++)
      for ( int j=0; j< 3; j++)
        Set(i,j,m(i,j));
  }
};

/** \brief 4D matrix with position */
/** This matrix is similar to 3D matrix, but contains also another vector, which defined position */

template<class Real>
class __declspec( dllexport ) BI_TMatrix4 : public BI_TMatrix3<Real>
{
	typedef BI_TMatrix3<Real> Base;

protected:
	/** position of the matrix */
	BI_TVector3<Real> _position;

public:
	/** \brief Contructor */
	BI_TMatrix4<Real>(const BI_TMatrix3<Real>& matrix, const BI_TVector3<Real>& pos = BI_TVector3<Real>(0,0,0) ) : Base(matrix), _position(pos) {}

	/** \brief Contructor */
	BI_TMatrix4<Real>(Real x1 = 0, Real x2 = 0, Real x3 = 0,
			Real y1 = 0, Real y2 =0, Real y3 =0,
			Real z1 =0 , Real z2 =0, Real z3 =0,
			Real p1 =0, Real p2 =0, Real p3 =0) : Base(x1,x2,x3,y1,y2,y3,z1,z2,z3), _position(p1,p2,p3) {}

	/** \brief Gets position vector */
	/** \param i i<sup>th</sup> item if the position vector */
	Real GetPos(int i) const { return _position[i]; }

	/** \brief Gets position coordinate vector */
	/** \param i i<sup>th</sup> item if the position vector */
	Real & GetPos(int i) { return _position[i]; }

  void Set(int axis, BI_TVector3<Real> vector)
  {
    Base::Set(axis,vector);
  }
	/** \brief Set method */
	Real& Set(int row, int column) 
	{  
		if (row == 3)
			return SetPos(column);
		else
			return Base::Set(row,column);
	}

	/** \brief Set cell */
	void Set(int row, int column, Real value) 
	{
		Set(row,column) = value;
	}

	/** \brief Sets position coordinate vector */
	Real& SetPos(int i) { return _position[i]; }

  void SetPosition(BI_TVector3<Real> pos) { _position = pos; }

  void SetPosition(Real posX, Real posY, Real posZ) { _position[0] = posX; _position[1] = posY; _position[2] = posZ; }

  BI_TVector3<Real>  GetPosition() const { return _position; }
	
  void SetPos(BI_TVector3<Real> vector) { _position = vector; }

	/** \brief Gets cell */
	Real Get(int row, int column)const
	{
		return row == 3? GetPos(row):Base::Get(row,column);
	}

	/** \brief Sets position offset*/
	void SetTranslation( const BI_TVector3<Real> & offset ) { _position = offset; }
  /** the same as setPos() */
  BI_TVector3<Real> GetTranslation( ) { return _position; }

  BI_TMatrix3<Real>& Orientation()
  {
    return *this; //automatic conversion
  }
	BI_TMatrix3<Real> Orientation()const
	{
		return *this; //automatic conversion
	}

	/** \brief Matrix multiplication according to perspective */
	BI_TMatrix4<Real>& MultiplyByPerspective(const BI_TMatrix4<Real> & m)
	{
		BI_TMatrix4<Real> hlp(*this);
		Base::operator *=(m);
		for ( int i =0; i < 3; i++ ) //set perspective
			Set(i,2) += m.GetPos(i);
		for( int i=0; i<3; i++ )
			SetPos(i) = ( hlp.Get(i,0)*m.GetPos(0) + hlp.Get(i,1)*m.GetPos(1) + hlp.Get(i,2)*m.GetPos(2));
		return *this;
	}

	/** \brief Gets position vector */
	const BI_TVector3<Real>& GetPos() const { return _position; }

	BI_TVector3<Real>& GetPos() { return _position; }

	/** \brief Count square distance in Matrix4*/
	Real Distance4( const BI_TMatrix4<Real> & m ) const { return Distance2(m) + _position.Distance(m._position); }

	/** \name Operators */
	/* \{ */

	/** \brief Assign operator */
	BI_TMatrix4<Real> & operator += (const BI_TMatrix4<Real> & matrix) //we should inherit operator += for BIMatrix3
	{
		(Base)(*this) +=  (Base) matrix;
		_position += matrix._position;
		return *this;
	}
	/** \brief Plus operator */
	BI_TMatrix4<Real> operator+(const BI_TMatrix4<Real> & matrix) const
	{
		BI_TMatrix4<Real> m (*this);
		m += matrix;
		return m;
	}

  BI_TMatrix4<Real> operator*(const BI_TMatrix3<Real> & matrix) const
  {
    Base::operator *(matrix);
  }
  /** \brief multiply operator */
  /** This follows this behavior : \n
    Orientation matrix will be result of multiplication original and parameter orientation matrix
    The position vector will be modifies by adding the result of multiplication of old orientation matrix and  the other's position vector
  */
  BI_TMatrix4<Real> operator*(const BI_TMatrix4<Real> & matrix) const
  {
    BI_TMatrix4<Real> m(*this);
    m *= matrix;
    return m;
  }
  BI_TMatrix4<Real>& operator*=(const BI_TMatrix4<Real> & matrix) 
  {
    BI_TMatrix3<Real> oldOrientation = Orientation();
    BI_TMatrix3<Real>::operator *=(matrix);
    SetTranslation(oldOrientation*matrix.GetPos()+GetPos());
    return *this;
  }
	/** \brief Minus operator */
	BI_TMatrix4<Real> & operator -= (const BI_TMatrix4<Real> & matrix) 
	{
		(Base)(*this) -= (Base) matrix;
		_position -= matrix._position;
		return *this;
	}
	/** \brief Minus operator */
	BI_TMatrix4<Real> operator-( const BI_TMatrix4<Real> & matrix ) const
	{
		BI_TMatrix4<Real> m (*this);
		m -= matrix;
		return m;
	}
	/** \brief Multiplication by vector */
	BI_TVector3<Real> operator*( const BI_TVector3<Real>& f )const
	{
		BI_TVector3<Real> v = Base::operator *(f);
		v += _position;
		return v;
	}

  /** \brief Multiplication by Matrix */
  /** \note Only orientation is affected */
  BI_TMatrix4<Real>& operator*=( const BI_TMatrix3<Real>& f )
  {
    Base::operator *(f);
    return *this;
  }
  
  /** \brief Addition by matrix */
  /** \note Only orientation is affected */
  BI_TMatrix4<Real>& operator+=( const BI_TMatrix3<Real>& f )
  {
    Base::operator +(f);
    return *this;
  }

	/** \brief Multiplication operator */
	BI_TMatrix4<Real> & operator*=( const Real f ) 
	{
		(Base)(*this) *= f;
		_position *=f;
		return *this;
	}

  void SetMultiply( const BI_TMatrix4<Real> &a, Real b )
  {
    _columns[_Aside] = a._columns[_Aside] * b;
    _columns[_Up] = a._columns[_Up] * b;
    _columns[_Direction] = a._columns[_Direction] * b;
    _position = a._position * b;
  }

  BI_TMatrix4<Real>operator*( const Real f ) const
  {
    BI_TMatrix4<Real> m;
    //(Base)m = (Base)(*this) * f;
    m.SetMultiply((*this), f);
    return m;
  }

	/** \brief Plus operator */
	BI_TMatrix4<Real> operator+( const Real f )const
	{
		BI_TMatrix4<Real> m (*this);
		m *= f;
		return m;
	}
  bool operator==( const BI_TMatrix4<Real> other )const
  {
    return Base::operator==(other) && _position == other.GetPos();
  }
  bool operator!=( const BI_TMatrix4<Real> other )const
  {
    return Base::operator!=(other) && _position != other.GetPos();
  }
  BI_TMatrix4<Real> InvertedRotation()const
  {
    BI_TMatrix4<Real> m = Base::InvertedRotation();
    m.SetPos(m.Orientation()*GetPos());
    return m;
  }

  /** \brief Set inverse general matrix */
  void SetInvertGeneral4x4(const BI_TMatrix4<Real>& op)
  {
    //invert rot matrix
    BI_TMatrix3<Real> invRotMat;
    invRotMat.SetInvertGeneral3x3(op.Orientation()); 

    _columns[_Aside] = invRotMat.GetAside();
    _columns[_Up] = invRotMat.GetUp();
    _columns[_Direction] = invRotMat.GetDirection();

    //invert position
    SetTranslation(this->Orientation() * (-op.GetPos()));
  }

  BI_TMatrix4<Real> GetInvertGeneral() const
  {
    BI_TMatrix4<Real> m;
    // invert orientation
    m.SetInvertGeneral3x3(Orientation());
    // invert translation
    m.SetPosition(m.GetRotated(-GetPosition()));
    return m;
  }


  /** \brief Set matrix multiplication result */
  void SetMultiply( const BI_TMatrix4<Real> &a, const BI_TMatrix4<Real> &b )
  {
    // result cannot be one of the sources
    //Assert(&a!=this);
    //Assert(&b!=this);
    // matrix multiplication
    int i,j;
    // b(3,0)=0, b(3,1)=0, b(3,2)=0, b(3,3)=1
    // a(3,0)=0, a(3,1)=0, a(3,2)=0, a(3,3)=1
    for( i=0; i<3; i++ ) for( j=0; j<3; j++ )
    {
      Set(i,j)=
        (
        a.Get(i,0)*b.Get(0,j)+
        a.Get(i,1)*b.Get(1,j)+
        a.Get(i,2)*b.Get(2,j)
        );
    }
    for( i=0; i<3; i++ )
    {
      SetPos(i)=
        (
        a.Get(i,0)*b.GetPos(0)+
        a.Get(i,1)*b.GetPos(1)+
        a.Get(i,2)*b.GetPos(2)+
        a.GetPos(i)
        );
    }
    //for( j=0; j<3; j++ ) Set(3,j)=0;
    //Set(3,3)=1;
  }

	BI_TVector3<Real> TransformPosition(const BI_TVector3<Real>& vc)const
	{
		BI_TVector3<Real> m= Base::operator *(vc);
		m+= _position;
		return m;
	}
	BI_TVector3<Real> TransformVector(const BI_TVector3<Real>& vc)const
	{
		BI_TVector3<Real> extr = Base::operator *(vc);
		return extr;
	}
	/** \} */
  template<class MatrixClass>
  MatrixClass Convert()
  {
    MatrixClass s(Get(0,0),Get(0,1),Get(0,2),GetPos(0),
      Get(1,0),Get(1,1),Get(1,2),GetPos(1),
      Get(2,0),Get(2,1),Get(2,2),GetPos(2));
    return s;
  }
  template <class MatrixClass>
  void Convert(MatrixClass m)
  {
    for ( int i =0; i < 3; i++)
      for ( int j=0; j< 3; j++)
        Set(i,j,m(i,j));
    _position.Convert(m.Position());
  }

  void SwitchYZ()
  {
    _columns[_Aside] = -_columns[_Aside];
    Real tmp;
    tmp = Get(1, 0); Set(1, 0) = Get(2, 0); Set(2, 0) = tmp;
    tmp = Get(1, 1); Set(1, 1) = Get(2, 1); Set(2, 1) = tmp;
    tmp = Get(1, 2); Set(1, 2) = Get(2, 2); Set(2, 2) = tmp;
    tmp = GetPos(1); SetPos(1) = GetPos(2); SetPos(2) = tmp;
  }

  BI_TMatrix4<Real> GetInvertRotation() const
  {
    BI_TMatrix4<Real> m;
    // invert orientation
    m.SetInvertRotation(Orientation());
    // invert translation
    m.SetPosition(m.GetRotated(-GetPosition()));
    return m;
  }

  BI_TMatrix4<Real> GetInvertScaled() const
  {
    BI_TMatrix4<Real> m;
    // invert orientation
    m.SetInvertScaled(Orientation());
    // invert translation
    m.SetPosition(m.Orientation() * -GetPosition());
    return m;
  }

  BI_TMatrix4<Real> GetRotatedX(Real angle)
  {
    SetRotationX(angle);
    return *this;
  }

  BI_TMatrix4<Real> GetRotatedY(Real angle)
  {
    SetRotationY(angle);
    return *this;
  }

  BI_TMatrix4<Real> GetRotatedZ(Real angle)
  {
    SetRotationZ(angle);
    return *this;
  }

  BI_TVector3<Real> GetRotated(BI_TVector3<Real> &o) const
  {
    BI_TVector3<Real> v;
    v.SetRotate(*this, o);
    return v;
  }
	/** \} */
};

typedef BI_TMatrix3<float> BI_Matrix3;
typedef BI_TMatrix3<double> BI_Matrix3d;
typedef BI_TMatrix4<float> BI_Matrix4;
typedef BI_TMatrix4<double> BI_Matrix4d;
#endif