#include <stdio.h>
#include <tchar.h>

#include "MemDump.hpp"

char MiniDumper::_dumpFullPath[_MAX_PATH] = {0};

MiniDumper::MiniDumper()
{
  // By default we dont set the unhandled exception filter
}

void MiniDumper::SetMyUnhandledExceptionFilter(const char *dumpFilePath)
{
  strcpy_s(_dumpFullPath,_MAX_PATH-1,dumpFilePath);
  ::SetUnhandledExceptionFilter( TopLevelFilter );
}


LONG MiniDumper::TopLevelFilter( struct _EXCEPTION_POINTERS *pExceptionInfo )
{
	LONG retval = EXCEPTION_CONTINUE_SEARCH;
	HWND hParent = NULL;						// find a better value for your app

	// firstly see if dbghelp.dll is around and has the function we need
	// look next to the EXE first, as the one in System32 might be old 
	// (e.g. Windows 2000)
	HMODULE hDll = NULL;
	char szDbgHelpPath[_MAX_PATH];

	if (GetModuleFileName( NULL, szDbgHelpPath, _MAX_PATH ))
	{
		char *pSlash = _tcsrchr( szDbgHelpPath, '\\' );
		if (pSlash)
		{
			_tcscpy( pSlash+1, "DBGHELP.DLL" );
			hDll = ::LoadLibrary( szDbgHelpPath );
		}
	}

  // load any version we can
	if (hDll==NULL) hDll = ::LoadLibrary( "DBGHELP.DLL" );


	LPCTSTR szResult = NULL;
	if (hDll)
	{
		MINIDUMPWRITEDUMP pDump = (MINIDUMPWRITEDUMP)::GetProcAddress( hDll, "MiniDumpWriteDump" );
		if (pDump)
		{
			// create the file
			HANDLE hFile = ::CreateFile
      ( 
        _dumpFullPath, 
        GENERIC_WRITE, 
        FILE_SHARE_WRITE, NULL, 
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL, 
        NULL 
      );

			if (hFile!=INVALID_HANDLE_VALUE)
			{
				_MINIDUMP_EXCEPTION_INFORMATION ExInfo;

				ExInfo.ThreadId = ::GetCurrentThreadId();
				ExInfo.ExceptionPointers = pExceptionInfo;
				ExInfo.ClientPointers = NULL;

				// write the dump
				BOOL bOK = pDump( GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &ExInfo, NULL, NULL );
				if (bOK) retval = EXCEPTION_EXECUTE_HANDLER;
				
				::CloseHandle(hFile);
			}
		}
	}

  char buffer[256] = {0};
  sprintf_s(buffer,sizeof(buffer),"Something broke.\r\nDebug information written to:\r\n%s",_dumpFullPath);

  ::MessageBox(NULL,buffer,"Ops",MB_OK);
	return retval;
}
