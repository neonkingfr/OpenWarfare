#include <El/elementpch.hpp>
#include <Es/Framework/consoleBase.h>
#include <El/QStream/QStdStream.hpp>
#include <El/QStream/QStream.hpp>
#include <io.h>

int consoleMain( int argc, const char *argv[] )
{
  QStdInStream in;

  char line[1024];
  while (in.readLine(line,sizeof(line)))
  {
    puts(line);
  }
#if _DEBUG
  wasError = true;
#endif
  return 0;
}
