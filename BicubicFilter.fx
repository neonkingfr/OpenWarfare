uniform float4 sourceSize;	// resampled/filtered texture size
uniform float4 texelSize;	// size of texels in format (texelX, 0, texelY, 0)

sampler s0 : register(s0);
sampler s1 : register(s1);

void bicubic_filter_vs(float3 iPos : POSITION0,
						float2 iUV	: TEXCOORD0,
						out float4 oPos	: POSITION0,
						out float2 oUV	: TEXCOORD0)
{
	oPos = float4(iPos.xyz, 1.0);
	oUV.xy = iUV.xy;
}

float4 bicubic_filter_ps(float2 iUV : TEXCOORD0) : COLOR0
{
	//return tex2D(s0, iUV.xy);

	// determination of source texture coordinates
	float2 coeffCoord = iUV.xy * sourceSize.xy - float2(0.5, 0.5);

	// offsets and weights from coefficient texture
	float3 hgX = tex1D(s1, coeffCoord.x).xyz;
	float3 hgY = tex1D(s1, coeffCoord.y).xyz;

	// sampling coordinates 
	float2 coordSource10 = iUV.xy + hgX.x * texelSize.xy;
	float2 coordSource00 = iUV.xy - hgX.y * texelSize.xy;

	float2 coordSource11 = coordSource10 + hgY.x * texelSize.zw;
	float2 coordSource01 = coordSource00 + hgY.x * texelSize.zw;

	coordSource10 = coordSource10 - hgY.y * texelSize.zw;
	coordSource00 = coordSource00 - hgY.y * texelSize.zw;

	// four linear source texture fetches
	float4 texSource00 = tex2D(s0, coordSource00);
	float4 texSource10 = tex2D(s0, coordSource10);
	float4 texSource01 = tex2D(s0, coordSource01);
	float4 texSource11 = tex2D(s0, coordSource11);

	// lin. interpolation along y
	texSource00 = lerp(texSource00, texSource01, hgY.z);
	texSource10 = lerp(texSource10, texSource11, hgY.z);

	// lin. interpolation along x
	texSource00 = lerp(texSource00, texSource10, hgX.z);

	return texSource00;
}
