#ifndef _ES_CACHELIST_HPP
#define _ES_CACHELIST_HPP

#include "listBidir.hpp"

#define CLTLink TLinkBidir
#define CLDLink TLinkBidirD
#define CLList TListBidir

#define CLRefList TListBidirRef
#define CLRefLink TLinkBidirRef

// note: this file is obsolete and it is here for old code compatibility only
// you should use TXxxxBidir naming scheme instead

#endif


