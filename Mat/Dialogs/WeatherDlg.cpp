// WeatherDlg.cpp : implementation file
//

#include <stdio.h>

#include "..\stdafx.h"
#include "..\Mat.h"
#include "..\View\ddeviewer.hpp"
#include "..\MatDoc.h"
#include "..\UpdateViewer.h"
#include "..\MatView.h"
#include "WeatherDlg.h" 



IMPLEMENT_DYNAMIC(CWeatherDlg, CDialog)
CWeatherDlg::CWeatherDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWeatherDlg::IDD, pParent)
  , _label_overcast(_T(""))
  , _label_fog(_T(""))
{
}


CWeatherDlg::~CWeatherDlg()
{
  if ( IsWindow(m_hWnd) )
    DestroyWindow();
}

void CWeatherDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_W_TIME, _dtp_time);
  DDX_Control(pDX, IDC_SLD_OVERCAST, _sld_overcast);
  DDX_Control(pDX, IDC_SLD_FOG, _sld_fog);
  DDX_Text(pDX, IDC_S_OVERCAST, _label_overcast);
  DDX_Text(pDX, IDC_S_FOG, _label_fog);
}


BEGIN_MESSAGE_MAP(CWeatherDlg, CDialog)
  ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLD_OVERCAST, OnNMReleasedcaptureSldOvercast)
  ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLD_FOG, OnNMReleasedcaptureSldFog)
  ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
  ON_BN_CLICKED(IDOK, OnBnClickedOk)
  ON_BN_CLICKED(IDC_B_Apply, OnBnClickedBApply)
  ON_WM_CLOSE()
END_MESSAGE_MAP()


//-------------------------------------   public:   -----------------------------------------


void CWeatherDlg::SetScriptName(RString script_name)
{
  _script_name= script_name;
}


void CWeatherDlg::SetMainWnd(HWND main_wnd)
{
  _main_wnd= main_wnd;
}


void CWeatherDlg::SetTime(CTime time)
{
  _dtp_time.SetTime(&time);
}


//-------------------------------------   Controls   ---------------------------------------


BOOL CWeatherDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  CTime wtt= view->GetTime();
  _dtp_time.SetTime(&wtt);
  _sld_overcast.SetRange(0, 100);
  _sld_overcast.SetPos(0);
  _sld_fog.SetRange(0, 100);
  _sld_fog.SetPos(0);

  _script_name= "";
  _main_wnd= NULL;

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CWeatherDlg::OnNMReleasedcaptureSldOvercast(NMHDR *pNMHDR, LRESULT *pResult)
{
  _label_overcast.Format("%.2f", 0.01*_sld_overcast.GetPos());
  UpdateData(FALSE);         //update dialog

  *pResult = 0;
}

void CWeatherDlg::OnNMReleasedcaptureSldFog(NMHDR *pNMHDR, LRESULT *pResult)
{
  _label_fog.Format("%.2f", 0.01*_sld_fog.GetPos());
  UpdateData(FALSE);

  *pResult = 0;
}


void CWeatherDlg::OnBnClickedCancel()
{
  OnCancel();
  ::SendMessage(_main_wnd, WM_CLOSE_WEATHER_DLG, 0, 0);     //musi byt nutne posledni
}


void CWeatherDlg::OnBnClickedOk()
{
  CTime tm;
  _dtp_time.GetTime(tm);
  if ( SetWeather(_script_name, tm, 0.01*_sld_overcast.GetPos(), 0.01*_sld_fog.GetPos()) )
  {
    view->SetTimeOvercastFog(tm, 0.01*_sld_overcast.GetPos(), 0.01*_sld_fog.GetPos());
    view->UpdateLights();
  }

  OnOK();
  ::SendMessage(_main_wnd, WM_CLOSE_WEATHER_DLG, 0, 0);     //musi byt nutne posledni
}


void CWeatherDlg::OnBnClickedBApply()
{
  CTime tm;
  _dtp_time.GetTime(tm);
  if ( SetWeather(_script_name, tm, 0.01*_sld_overcast.GetPos(), 0.01*_sld_fog.GetPos()) )
  {
    view->SetTimeOvercastFog(tm, 0.01*_sld_overcast.GetPos(), 0.01*_sld_fog.GetPos());
    view->UpdateLights();
  }
}

void CWeatherDlg::OnClose()
{
  CDialog::OnClose();
  ::SendMessage(_main_wnd, WM_CLOSE_WEATHER_DLG, 0, 0);     //musi byt nutne posledni
}


//---------------------------   global   ----------------------------------------


//create the script
BOOL CreateScript(RString script_name, CTime tm, float overcast, float fog)
{
  FILE* f= fopen(script_name, "w");
  if ( !f )
    return FALSE;
  int h= tm.GetHour();
  int m= tm.GetMinute();
  float t= h + max(0, min(1, 0.0166666666666666*m));
  fprintf(f, "skiptime (%.2f-daytime)\n", t);
  fprintf(f, "0 setOvercast %.2f\n", overcast);
  fprintf(f, "0 setFog %.2f\n", fog);
  fclose(f);
  return TRUE;
}


//set the weather
BOOL SetWeather(RString script_name, CTime tm, float overcast, float fog)
{
  if ( !CreateScript(script_name, tm, overcast, fog) )
  {
    ::MessageBox(NULL, "The weather was not set.", "Weather", MB_OK|MB_ICONWARNING);
    return FALSE;
  }
  if ( !ViewerStartScript(script_name) )
  {
    ::MessageBox(NULL, "The weather was not set.", "Weather", MB_OK|MB_ICONWARNING);
    DeleteFile(script_name);
    return FALSE;
  }
  view->Update();

  DeleteFile(script_name);
  return TRUE;
}



