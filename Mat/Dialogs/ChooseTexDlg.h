#pragma once


#include	"..\Codex\cdxCDynamicDialog.h"


// CChooseTexDlg dialog

class CChooseTexDlg : public cdxCDynamicDialog
{
	DECLARE_DYNAMIC(CChooseTexDlg)

public:
	CChooseTexDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChooseTexDlg();

// Dialog Data
	enum { IDD = IDD_DLG_CHOOSE_TEX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	DECLARE_DYNAMIC_MAP()
public:
  afx_msg void OnBnClickedBOk();
  afx_msg void OnBnClickedBCancel();
};
