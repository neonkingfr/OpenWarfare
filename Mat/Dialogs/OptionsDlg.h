#pragma once
#include "afxcmn.h"


// COptionsDlg dialog

class COptionsDlg : public CDialog
{
	DECLARE_DYNAMIC(COptionsDlg)

public:
	COptionsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionsDlg();

// Dialog Data
	enum { IDD = IDD_DLG_OPTIONS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString _edit_buld_path;
  CString _edit_material_path;
  CString _edit_ps;
  CString _edit_vs;
  CString _edit_uvTex;
  CString _edit_editor;
  CString _edit_texview;
  CString _edit_default_materials;
  CString _edit_ss_server_name;
  CSpinButtonCtrl _spin_coef_gray;
  BOOL _check_buld_start;
  BOOL _check_buld_end;
  BOOL _disable_unselect_mats;
  //disable materials color
  int _radio_dmc;
  float _edit_coef_gray;

  virtual BOOL OnInitDialog();

  afx_msg void OnEnChangeEditOptBuldpath();
  afx_msg void OnEnChangeEditOptMaterialPath();
  afx_msg void OnEnChangeEditOptCps();
  afx_msg void OnEnChangeEditOptCvs();
  afx_msg void OnEnChangeEditOptUvtex();
  afx_msg void OnEnChangeEditOptEditor();
  afx_msg void OnBnClickedBOptEditor();
  afx_msg void OnEnChangeEditOptTexview();
  afx_msg void OnBnClickedBOptTexview();
  afx_msg void OnEnChangeEditOptDfmat();
  afx_msg void OnBnClickedBOptDfmat();
  afx_msg void OnEnChangeEditOptCoefgray();
  afx_msg void OnDeltaposSpinOptCg(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedCheckBuldStart();
  afx_msg void OnBnClickedCheckBuldEnd();
  afx_msg void OnBnClickedCheckDisUnsMat();
  afx_msg void OnEnChangeEditOptSsServerName();
  afx_msg void OnBnClickedRadioGray();
  afx_msg void OnBnClickedRadioRed();
  afx_msg void OnBnClickedRadioGreen();
  afx_msg void OnBnClickedRadioBlue();
};
#pragma once


