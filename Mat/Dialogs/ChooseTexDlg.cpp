// Dialogs\ChooseTexDlg.cpp : implementation file
//

#include "..\stdafx.h"
#include "..\Mat.h"
#include "ChooseTexDlg.h"
#include ".\choosetexdlg.h"


// CChooseTexDlg dialog

IMPLEMENT_DYNAMIC(CChooseTexDlg, cdxCDynamicDialog)
CChooseTexDlg::CChooseTexDlg(CWnd* pParent /*=NULL*/)
	: cdxCDynamicDialog(CChooseTexDlg::IDD, pParent)
{
}

CChooseTexDlg::~CChooseTexDlg()
{
}

void CChooseTexDlg::DoDataExchange(CDataExchange* pDX)
{
	cdxCDynamicDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CChooseTexDlg, cdxCDynamicDialog)
  ON_BN_CLICKED(IDC_B_OK, OnBnClickedBOk)
  ON_BN_CLICKED(IDC_B_CANCEL, OnBnClickedBCancel)
END_MESSAGE_MAP()


BEGIN_DYNAMIC_MAP(CChooseTexDlg,cdxCDynamicDialog)
	DYNAMIC_MAP_ENTRY(IDC_LIST_TEX,	mdResize,	mdResize)
  DYNAMIC_MAP_ENTRY(IDC_B_OK, mdRelative, mdRepos)
  DYNAMIC_MAP_ENTRY(IDC_B_CANCEL, mdRelative, mdRepos)
END_DYNAMIC_MAP()


// CChooseTexDlg message handlers

void CChooseTexDlg::OnBnClickedBOk()
{
  OnOK();
}

void CChooseTexDlg::OnBnClickedBCancel()
{
  OnCancel();
}
