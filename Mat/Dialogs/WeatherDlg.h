#pragma once
#include "afxdtctl.h"
#include "afxcmn.h"
#include "Es/Strings/rString.hpp"


// CWeatherDlg dialog

class CWeatherDlg : public CDialog
{
	DECLARE_DYNAMIC(CWeatherDlg)

public:
	CWeatherDlg(CWnd* pParent = NULL);    // standard constructor
	virtual ~CWeatherDlg();

// Dialog Data
	enum { IDD = IDD_DLG_WEATHER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
private:
  RString _script_name;
  HWND _main_wnd;
  //controls
  CDateTimeCtrl _dtp_time;
  CSliderCtrl _sld_overcast;
  CSliderCtrl _sld_fog;
  CString _label_overcast;
  CString _label_fog;

public:
  void SetScriptName(RString script_name);
  void SetMainWnd(HWND main_wnd);
  void SetTime(CTime time);

  virtual BOOL OnInitDialog();
  afx_msg void OnNMReleasedcaptureSldOvercast(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReleasedcaptureSldFog(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedCancel();
  afx_msg void OnBnClickedOk();
  afx_msg void OnBnClickedBApply();
  afx_msg void OnClose();
};


BOOL SetWeather(RString script_name, CTime tm, float overcast, float fog);



