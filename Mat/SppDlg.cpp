// CSppDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Mat.h"
#include "SppDlg.h"
#include "SppDlg.h"

#include <math.h>
#include ".\sppdlg.h"

#define DEGREE                   0.017453292519943295769
#define START_X                  0            //nemenit
#define START_Y                  170
#define MAX_X                    180          //nemenit
#define MAX_Y                    150          
#define CENTER_X                 110


// CSppDlg dialog

IMPLEMENT_DYNAMIC(CSppDlg, CDialog)
CSppDlg::CSppDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSppDlg::IDD, pParent)
  , _edit_spp(0)
{
}

CSppDlg::~CSppDlg()
{
}

void CSppDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_EDIT_SPP, _edit_spp);
  DDX_Control(pDX, IDC_SLIDER_SPP, _slider_spp);
}


BEGIN_MESSAGE_MAP(CSppDlg, CDialog)
  ON_WM_PAINT()
  ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_SPP, OnNMReleasedcaptureSliderSpp)
  ON_EN_CHANGE(IDC_EDIT_SPP, OnEnChangeEditSpp)
END_MESSAGE_MAP()


BOOL CSppDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  _slider_spp.SetRange(0, 100);
  _slider_spp.SetPos(99);
  _slider_spp.SetPos(100-_edit_spp);
  UpdateData(FALSE);


  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}


void CSppDlg::OnPaint()
{
  CPaintDC dc(this);         // device context for painting
  float x, y;

  dc.MoveTo(START_X, START_Y);
  for ( x= 0; x <= 90; x++ )
  {
    y= cos(x * DEGREE);
    y= pow(y, _edit_spp);
    dc.MoveTo(CENTER_X + x, START_Y);
    dc.LineTo(CENTER_X + x, START_Y - y*MAX_Y);

    dc.MoveTo(CENTER_X - x, START_Y);
    dc.LineTo(CENTER_X - x, START_Y - y*MAX_Y);
  }
}


void CSppDlg::OnNMReleasedcaptureSliderSpp(NMHDR *pNMHDR, LRESULT *pResult)
{
  _edit_spp= 100-_slider_spp.GetPos();
  UpdateData(FALSE);
  Invalidate();

  *pResult = 0;
}

void CSppDlg::OnEnChangeEditSpp()
{
  UpdateData(TRUE);
  _slider_spp.SetPos(100-_edit_spp);
  Invalidate();
}
