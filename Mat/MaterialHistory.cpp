/*
  MateialHistory.cpp
  ------------------

  Data structur for history of material.
*/


#include "Material.hpp"


#define H_MAX                 H_LEN-1                        //index of maximal possible list member
#define P_UNDF                -1                             //undefined position
#define ADD_NORMAL            0                              //status normal
#define ADD_START             1                              //status first
#define ADD_END               2                              //status last
#define ADD_FL                3                              //both first and last


void MaterialHistory::Inc(int& a)
{
  if ( a != H_MAX )
    a++;
  else
    a= 0;
}


void MaterialHistory::Dec(int& a)
{
  if ( a )
    a--;
  else
    a= H_MAX;
}


//history is full, must remove first change
void MaterialHistory::RemoveFirstChange()
{
  Inc(_first);                          //uvolnim misto
  while ( !_stat[_first]&ADD_START)     //zrusim prvnim zmenu
    Inc(_first);
}


//---------------------------   public   ------------------------------------------


MaterialHistory::MaterialHistory()
{
  Reset();
}


//reset
void MaterialHistory::Reset()
{
  _first= _last= _cur= P_UNDF;
  _adding= _undoing= _redoing= FALSE;
  _lha= LHA_NO;
  for ( int i= 0; i < H_LEN; i++ )
    _stat[i]= S_NORMAL;
}


BOOL MaterialHistory::Empty()
{
  //Empty() se testuje na _cur, pokud se pri 'Undo' odebere prvni mateial _cur se nastavi na P_UNDF
  if ( _cur == P_UNDF )
    return TRUE;
  else
    return FALSE;
}


//if list is full return TRUE
BOOL MaterialHistory::Full()
{
  //the last one catched the first one
  int l= _last;
  Inc(l);
  if ( l == _first )
    return TRUE;
  else
    return FALSE;
}


//return TRUE if it can not do Undo
BOOL MaterialHistory::FirstPos()
{
  if (  _cur == _first  &&  !Empty()  )
    return TRUE;
  else
    return FALSE;
}


//return TRUE if it can not do Redo
BOOL MaterialHistory::LastPos()
{
  if (  _cur == _last  &&  !Empty()  )
    return TRUE;
  else
    return FALSE;
}


//return the last history action
int MaterialHistory::GetLastAction()
{
  return _lha;
}


//return the position in the history
int MaterialHistory::GetPos()
{
  return _cur;
}


//start adding materials
//typical call (add one change):
//StartAdd();                 //start adding one change to history
//for (i= 0; i<_Nmat; i++)
//  Add(_mat[i]);
//EndAdd();                   //stop adding this change
void MaterialHistory::StartAdd()
{
  //no body, adding is detected by first call of Add()
}


//add vzdycky skoci na pozici _cur, ktera je rovna _last pokud se nedelalo Undo
void MaterialHistory::Add(Material& mat)
{
  if ( Empty() )                //pridavam 1. udalost
  {
    _first= _last= _cur= 0;
  }
  else
  {
    Inc(_cur);
    _last= _cur;                //zrusi se tim moznost Redo, pokud se delalo Undo (_last!=_cur)
    //pokud je historie plna umazese prvni zaznam
    if ( Full() )
      RemoveFirstChange();
  }
  _mat[_last]= mat;
  //pridavam 1. material teto udalosti?
  if ( !_adding )
  {
    _stat[_last]|= ADD_START;             //oznacit prvniho
    _adding= TRUE;
  }
}


//stop adding materials
void MaterialHistory::EndAdd()
{
  if ( _adding )             //pridaly se vubec nejake zmeny?
  {
    _stat[_last]|= ADD_END;              //oznacit posledniho
    _adding= FALSE;
    _lha= LHA_ADD;
  }
}


//start Undo
void MaterialHistory::StartUndo()
{
  if ( !Empty() )
    _undoing= TRUE;
  else
    _undoing= FALSE;
}


//Undo
//typical call (Undo of one change):
//StartUndo();
//wile ( !EndUndo() ) {
//  Undo(mat)
//  _mat[mat._id]= mat;                 //! casem vylepsit (2x kopirovani)
//}
void MaterialHistory::Undo(Material& mat)
{
  mat= _mat[_cur];
  _lha= LHA_UNDO;
  //prvni mat udalosti?
  if ( _stat[_cur]&ADD_START )
  {
    _undoing= FALSE;              //koncim s 'Undo' teto udalosti, dalsi 'Undo' se uz nebude delat
    //uplne prvni mat?
    if ( FirstPos() )
    {
      _cur= P_UNDF;               //history is empty
      return;
    }
  }
  Dec(_cur);       //_cur ma ukazovat na material, ktery se ma vratit pri dalsim Undo
}


//end Undo
BOOL MaterialHistory::EndUndo()
{
  if ( !_undoing || Empty() )
  {
    return TRUE;
  }
  if ( FirstPos() )
    _undoing= FALSE;
  return FALSE;
}


//start Redo
void MaterialHistory::StartRedo()
{
  if (  LastPos()  &&  _stat[_cur]&ADD_END  )
    _redoing= FALSE;
  else
    _redoing= TRUE;
}


//Redo
//typical call (Redo of one change):
//StartRedo();
//wile ( !EndRedo() ) {
//  Rddo(mat)
//  _mat[mat._id]= mat;                 //! casem vylepsit (2x kopirovani)
//}
void MaterialHistory::Redo(Material& mat)
{
  //bud jsem jiz ukoncil Redo pro jednu zmenu, nebo Redo jiz nelze provadet
  if ( !_redoing || LastPos() )
  {
    _redoing= FALSE;              
    return;                        //Redo() nesmim udelat
  }
  if ( Empty() )                   //uz nelze delat Undo
    _cur= _first;
  else
    Inc(_cur);                       //_cur ma ukazovat na material, ktery se ma vratit pri dalsim Undo
  mat= _mat[_cur];
  _lha= LHA_REDO;
  if ( _stat[_cur]&ADD_END )
    _redoing= FALSE;
}


//end Redo
BOOL MaterialHistory::EndRedo()
{
//  return !_redoing;
  if ( !_redoing || Full() )
    return TRUE;
  if ( LastPos() )
    _redoing= FALSE;             //pristi kolo Redo uz nebude
  return FALSE;
}
