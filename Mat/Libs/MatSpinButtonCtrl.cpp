// Libs\MatSpinButtonCtrl.cpp : implementation file
//

#include "..\stdafx.h"
#include "..\Mat.h"
#include "..\MatView.h"
#include ".\matspinbuttonctrl.h"

// CMatSpinButtonCtrl

IMPLEMENT_DYNAMIC(CMatSpinButtonCtrl, CSpinButtonCtrl)
CMatSpinButtonCtrl::CMatSpinButtonCtrl()
{
  _starting= _ending= FALSE;
}

CMatSpinButtonCtrl::~CMatSpinButtonCtrl()
{
}


BEGIN_MESSAGE_MAP(CMatSpinButtonCtrl, CSpinButtonCtrl)
  ON_WM_LBUTTONDOWN()
  ON_WM_LBUTTONUP()
END_MESSAGE_MAP()


void CMatSpinButtonCtrl::RemoveStartEndFlag()
{
  _starting= _ending= FALSE;
}


BOOL CMatSpinButtonCtrl::Starting()
{
  return _starting;
}


BOOL CMatSpinButtonCtrl::Ending()
{
  return _ending;
}


void CMatSpinButtonCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
  _starting= TRUE;
  view->ForbidUpdate(TRUE);

  CSpinButtonCtrl::OnLButtonDown(nFlags, point);
}


void CMatSpinButtonCtrl::OnLButtonUp(UINT nFlags, CPoint point)
{
  _ending= TRUE;
  view->ForbidUpdate(FALSE);

  CSpinButtonCtrl::OnLButtonUp(nFlags, point);
}

