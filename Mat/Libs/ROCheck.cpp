#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <malloc.h>
#include <io.h>
#include <windows.h>
#include <tchar.h>
#include "ROCheck.h"

#undef IDC_RUNEXPLORER
#define IDC_RUNEXPLORER 1001

#undef IDC_FILENAME 
#define IDC_FILENAME 1002

#undef IDC_DLGICON
#define IDC_DLGICON 1003


struct RODialogInfo
  {
  const _TCHAR *szFilename;
  int flags;
  HICON icon;
  };

static void RunExplorer(HWND hOwner,_TCHAR *fname)
  {
  _TCHAR *params=(_TCHAR *)alloca((_tcslen(fname)+100)*sizeof(_TCHAR));
  _stprintf(params,_T("explorer.exe /select,\"%s\""),fname);
  STARTUPINFO strinfo;
  PROCESS_INFORMATION pinfo;

  memset(&strinfo,0,sizeof(strinfo));
  strinfo.cb=sizeof(strinfo);
  strinfo.dwFlags=0;  
  BOOL ret=CreateProcess(NULL,params,NULL,NULL,FALSE,CREATE_DEFAULT_ERROR_MODE|NORMAL_PRIORITY_CLASS,
    NULL,NULL,&strinfo,&pinfo);
  if (ret==FALSE)
    {
    _TCHAR buff[256];_TCHAR *appname;
    ::GetModuleFileName(NULL,buff,sizeof(buff)/sizeof(_TCHAR));
    appname=_tcsrchr(buff,'\\');
    if (appname==NULL) appname=buff;else ++appname;
    MessageBox(hOwner,_T("Failed create process"),appname,MB_OK|MB_ICONEXCLAMATION);
    }
  else
    {
    SetCursor(LoadCursor(NULL,IDC_WAIT));
    WaitForInputIdle(pinfo.hProcess,30000);
    CloseHandle(pinfo.hProcess);
    CloseHandle(pinfo.hThread);
    }
  }

static void CenterWindow(HWND hDlg)
  {
  HWND hParent=GetParent(hDlg);
  if (hParent==NULL) hParent=GetDesktopWindow();
  RECT parentRc;
  RECT curRc;
  POINT pt;
  GetWindowRect(hParent,&parentRc);
  ClientToScreen(hParent,(POINT *)(&parentRc));
  ClientToScreen(hParent,(POINT *)(&parentRc)+1);
  GetWindowRect(hDlg,&curRc);
  pt.x=(parentRc.left+parentRc.right)>>1;
  pt.y=(parentRc.top+parentRc.bottom)>>1;
  pt.x-=(curRc.right+curRc.left)>>1;
  pt.y-=(curRc.top+curRc.bottom)>>1;
  SetWindowPos(hDlg,NULL,pt.x,pt.y,0,0,SWP_NOZORDER|SWP_NOSIZE);
  }

static LRESULT CALLBACK TestFileRODlg(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
  {
  switch (uMsg)
    {
    case WM_INITDIALOG:
      {
      RODialogInfo *info=(RODialogInfo *)lParam;
      SetDlgItemText(hDlg,IDC_FILENAME,info->szFilename);
      if (info->flags & ROCHF_DisableSaveAs) EnableWindow(GetDlgItem(hDlg,IDOK),FALSE);
      if (info->flags & ROCHF_DisableExploreButton) EnableWindow(GetDlgItem(hDlg,IDC_RUNEXPLORER),FALSE);
      if (info->icon==NULL)
        SendDlgItemMessage(hDlg,IDC_DLGICON,STM_SETIMAGE,IMAGE_ICON,(LPARAM)LoadIcon(NULL,IDI_EXCLAMATION));
      else
        SendDlgItemMessage(hDlg,IDC_DLGICON,STM_SETIMAGE,IMAGE_ICON,(LPARAM)info->icon);
      CenterWindow(hDlg);
      MessageBeep(MB_ICONEXCLAMATION);
      return 0;
      }      
    case WM_COMMAND:
      {
      switch (LOWORD(wParam))
        {
        case IDOK: EndDialog(hDlg,IDOK);return 1;
        case IDCANCEL: EndDialog(hDlg,IDCANCEL);return 1;
        case IDC_RUNEXPLORER:
          {
          HWND hFileName=GetDlgItem(hDlg,IDC_FILENAME);
          int len=GetWindowTextLength(hFileName)+1;
          _TCHAR *fname=(_TCHAR *)alloca(len*sizeof(_TCHAR));
          GetWindowText(hFileName,fname,len);
          RunExplorer(hDlg,fname);
          return 1;
          }
        }
      }
    }
  return 0;
  }

ROCheckResult TestFileRO(HWND hWndOwner, const _TCHAR *szFilename, int flags,HICON hIcon, HINSTANCE hInst)
  {
  RODialogInfo nfo;
  nfo.flags=flags;
  nfo.szFilename=szFilename;
  nfo.icon=hIcon;
  if (_access(szFilename,0)!=0) return ROCHK_FileOK;  //Not Exists, it is OK
  if (_access(szFilename,06)==0) return ROCHK_FileOK; //File is R/W, it is OK
  if (hInst==NULL) hInst=GetModuleHandle(NULL);
  if (hWndOwner==NULL) hWndOwner=GetActiveWindow();
  if (GetParent(hWndOwner)!=NULL) hWndOwner=GetParent(hWndOwner);
  int ret=::DialogBoxParam(hInst,"READONLYCHECKDIALOG",hWndOwner,(DLGPROC)TestFileRODlg,(LPARAM)&nfo);
  if (ret==-1) 
    {
    MessageBox(hWndOwner,_T("Application error: ROCheck dialog creation failed"),NULL,MB_OK|MB_ICONSTOP);
    return ROCHK_FileRO;
    }
  if (ret==IDOK) return ROCHK_FileSaveAs;
  if (_access(szFilename,0)!=0) return ROCHK_FileOK;  //Not Exists, it is OK
  if (_access(szFilename,06)==0) return ROCHK_FileOK; //File is R/W, it is OK
  return ROCHK_FileRO;
  }
