#pragma once


// CMatListBox

class CMatListBox : public CListBox
{
	DECLARE_DYNAMIC(CMatListBox)

public:
	CMatListBox();
	virtual ~CMatListBox();

protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
};


