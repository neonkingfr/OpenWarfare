#pragma once

/*
#define SPIN_NO        0
#define SPIN_AMB_R     1
#define SPIN_AMB_G     2
#define SPIN_AMB_B     3
#define SPIN_AMB_A     4
#define SPIN_DIF_R     11
#define SPIN_DIF_G     12
#define SPIN_DIF_B     13
#define SPIN_DIF_A     14
#define SPIN_FOD_R     21
#define SPIN_FOD_G     22
#define SPIN_FOD_B     23
#define SPIN_FOD_A     24
#define SPIN_EMM_R     31
#define SPIN_EMM_G     32
#define SPIN_EMM_B     33
#define SPIN_EMM_A     34
#define SPIN_SPC_R     41
#define SPIN_SPC_G     42
#define SPIN_SPC_B     43
#define SPIN_SPC_A     44
*/

class CMatSpinButtonCtrl : public CSpinButtonCtrl
{
	DECLARE_DYNAMIC(CMatSpinButtonCtrl)

public:
	CMatSpinButtonCtrl();
	virtual ~CMatSpinButtonCtrl();

protected:
	DECLARE_MESSAGE_MAP()

private:
  BOOL _starting;
  BOOL _ending;

public:
  void RemoveStartEndFlag();
  BOOL Starting();
  BOOL Ending();

  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
};


