#pragma once


#define SLD_NO          0
#define SLD_AMB         1
#define SLD_DIF         2
#define SLD_FOD         3
#define SLD_EMM         4
#define SLD_SPC         5


class CMatSliderCtrl : public CSliderCtrl
{
	DECLARE_DYNAMIC(CMatSliderCtrl)

public:
	CMatSliderCtrl();
	virtual ~CMatSliderCtrl();

protected:
	DECLARE_MESSAGE_MAP()

private:
  int _type;

public:
  void SetType(int type);

  afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
};


