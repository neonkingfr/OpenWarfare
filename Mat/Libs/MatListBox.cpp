// Libs\MatListBox.cpp : implementation file
//

#include "..\stdafx.h"
#include "..\Mat.h"
#include "..\MatView.h"
#include ".\matlistbox.h"


// CMatListBox

IMPLEMENT_DYNAMIC(CMatListBox, CListBox)
CMatListBox::CMatListBox()
{
}

CMatListBox::~CMatListBox()
{
}


BEGIN_MESSAGE_MAP(CMatListBox, CListBox)
  ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()



// CMatListBox message handlers

void CMatListBox::OnRButtonDown(UINT nFlags, CPoint point)
{
  view->OnRButtonDown();

  CListBox::OnRButtonDown(nFlags, point);
}

