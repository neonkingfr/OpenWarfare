#ifndef __READONLYCHECK_HEADER_
#define __READONLYCHECK_HEADER_

#include <tchar.h>
enum ROCheckResult {ROCHK_FileOK,ROCHK_FileRO,ROCHK_FileSaveAs};
enum ROCheckFlags  {ROCHF_DisableSaveAs=1,ROCHF_DisableExploreButton=2};

ROCheckResult TestFileRO(HWND hWndOwner, const _TCHAR *szFilename, int flags=0,HICON hIcon=NULL, HINSTANCE hInst=NULL);

#endif