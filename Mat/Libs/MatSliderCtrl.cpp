// Libs\MatSliderCtrl.cpp : implementation file
//


#include "..\stdafx.h"
#include "..\Mat.h"
#include "..\MatView.h"
#include ".\matsliderctrl.h"



IMPLEMENT_DYNAMIC(CMatSliderCtrl, CSliderCtrl)
CMatSliderCtrl::CMatSliderCtrl()
{
  _type= SLD_NO;
}

CMatSliderCtrl::~CMatSliderCtrl()
{
}


BEGIN_MESSAGE_MAP(CMatSliderCtrl, CSliderCtrl)
  ON_WM_KEYUP()
END_MESSAGE_MAP()



void CMatSliderCtrl::SetType(int type)
{
  _type= type;
}


void CMatSliderCtrl::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  if ( nChar == VK_LEFT || nChar == VK_RIGHT )
  {
    NMHDR par_NMHDR;
    LRESULT par_Result;
    switch ( _type )
    {
      case SLD_AMB:
        view->OnNMReleasedcaptureSliderAmb(&par_NMHDR, &par_Result);
      break;
      case SLD_DIF:
        view->OnNMReleasedcaptureSliderDif(&par_NMHDR, &par_Result);
      break;
      case SLD_FOD:
        view->OnNMReleasedcaptureSliderFod(&par_NMHDR, &par_Result);
      break;
      case SLD_EMM:
        view->OnNMReleasedcaptureSliderEmm(&par_NMHDR, &par_Result);
      break;
      case SLD_SPC:
        view->OnNMReleasedcaptureSliderSpc(&par_NMHDR, &par_Result);
      break;
    } 
  }

  CSliderCtrl::OnKeyUp(nChar, nRepCnt, nFlags);
}

