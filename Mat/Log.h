/*
  Log.h
  -----

  October 2003, Tonda Malik
*/


#ifndef _log_h_
#define _log_h_


#include "Es\Framework\appFrame.hpp"


//log
class MatFrameFunctions : public AppFrameFunctions {
private:
  FILE* _f;

public:

//constructor
MatFrameFunctions()
{
  _f= fopen("Mat.log", "wc");
};

//destructor
~MatFrameFunctions()
{
  if ( _f ) fclose(_f);
};

#if _ENABLE_REPORT
virtual void LogF(const char *format, va_list argptr)
{
  if (_f)
  {
    char buf[1024];
    vsprintf(buf,format,argptr);
    fprintf(_f, "%s\n", buf);
    fflush(_f);
  }
}
#endif

};

static MatFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;


#endif