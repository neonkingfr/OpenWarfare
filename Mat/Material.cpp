/*
  MATERIAL.CPP
  ------------

  August 2003, Tonda Malik

  Data structure for material. Load, save of material.
*/


#include "Material.hpp"
#include "Es/Types/memtype.h"
#include "El/ParamFile/paramFile.hpp"
#include "Es/Common/global.hpp"
#include <io.h>


#define TEMP_DIR             "temp\\"
#define HARMLESS_MAT         "harmless.rvmat"
#define NO_ID                -1
#define NONAME               "NONAME"


//global data & functions
int   g_unselected_mats;
float g_coef_gray;


BOOL FileExist(RString& file)
{
  WIN32_FIND_DATA FindFileData;
  HANDLE h= FindFirstFile((LPCTSTR) file, &FindFileData);

  if ( h != INVALID_HANDLE_VALUE )
  {
    FindClose(h);
    return TRUE;
  }
  else
    return FALSE;
}


//***************************   Stage   *******************************************************


Stage::Stage()
{
  _enable= TRUE;
  _name= "";
  _protected= FALSE;
  _texture= "";
  _uvSource= "";
  _uv_none= FALSE;
  _aside[1]= _aside[2]= _up[0]= _up[2]= _dir[0]= _dir[1]=
    _pos[0]= _pos[1]= _pos[2]= 0;
  _aside[0]= _up[1]= _dir[2]= 1;
}


BOOL Stage::Enable() const
{
  return _enable;
}


void Stage::Enable(BOOL enable)
{
  _enable= enable;
}


BOOL Stage::Protected() const
{
  return _protected;
}


void Stage::Protected(BOOL prt)
{
  _protected= prt;
}


RString Stage::GetName() const
{
  return _name;
}


void Stage::SetName(const RString& name)
{
  _name= name;
}


RString Stage::GetTexture() const
{
  return _texture;
}


void Stage::SetTexture(const RString& texture)
{
  _texture= texture;
}


RString Stage::GetUVSource() const
{
  return _uvSource;
}


void Stage::SetUVSource(const RString& uvSource)
{
  _uvSource= uvSource;
  if ( _uvSource == (RString)"none" )
    _uv_none= TRUE;
  else
    _uv_none= FALSE;
}


BOOL Stage::UVNone()
{
  return _uv_none;
}


void Stage::UVNone(BOOL uv_none)
{
  _uv_none= uv_none;
  if ( _uv_none )
    _uvSource= "none";
}


Vector3& Stage::GetAside()
{
  return _aside;
}


Vector3& Stage::GetUp()
{
  return _up;
}


Vector3& Stage::GetDir()
{
  return _dir;
}


Vector3& Stage::GetPos()
{
  return _pos;
}


//***************************   Material   ****************************************************


//constructor
Material::Material()
{
  Reset();
}


Material::~Material()
{
}


//set an initial values to material
void Material::Reset()
{
  _id= NO_ID;
  _ambient._r= _ambient._g= _ambient._b= _ambient._a= AMBIENT;
  _diffuse._r= _diffuse._g= _diffuse._b= _diffuse._a= DIFFUSE;
  _forcedDiffuse._r= _forcedDiffuse._g= _forcedDiffuse._b= _forcedDiffuse._a= FORCEDDIFFUSE;
  _emmisive._r= _emmisive._g= _emmisive._b= _emmisive._a= EMMISIVE;
  _specular._r= _specular._g= _specular._b= _specular._a= SPECULAR;
  _specularPower= SPECULARPOWER;
  _Nstage= 1;
	_cur_stage= 0;
  _PixelShaderID= "";
  _VertexShaderID= "";

  for ( int i= 0; i<MAX_STAGES; i++ )
  {
    _stage[i]._name= "Stage";
    _stage[i]._texture= "";
    _stage[i]._uvSource= "";
    for ( int j= 0; j<3; j++ )
    {
      _stage[i]._aside[j]= _stage[i]._up[j]= _stage[i]._dir[j]= _stage[i]._pos[j]= 0;
    }
    //set the diagonal
    _stage[i]._aside[0]= _stage[i]._up[1]= _stage[i]._dir[2]= 1;
  };

  _file_name= _mat_name= _temp= "";
  _changed= _to_save= _disable= _selected= _ro= _ss_checked_out= _ss_controled= _ss_deleted= FALSE;
}


//kopiruje se
/*
Pouziti:

- pri ukladani do histori
- pri kopirovani z modelu na primitiva
- pri kopirovani z primitiva na primitivum
- pri kopirovani z primitiva na model
- pri kopirovani materialu pomoci "Copy", "Paste"

Poznamky:

- Vzdy se kopiruji pouze parametry materialu a neskodne flagy. Nekopiruje se
  cesta k souboru nebo struktury !
*/
Material& Material::operator = (const Material& mat)
{
  _id= mat._id;
  _ambient= mat._ambient;
  //_ambient_disable;                           
  _diffuse= mat._diffuse;
  //_diffuse_disable;
  _forcedDiffuse= mat._forcedDiffuse;
  _emmisive= mat._emmisive;
  _specular= mat._specular;
  _specularPower= mat._specularPower;
  _PixelShaderID= mat._PixelShaderID;;
  _VertexShaderID= mat._VertexShaderID;
  _Nstage= mat._Nstage;
	_cur_stage= mat._cur_stage;
  for ( int i= 0; i < _Nstage; i++ )
  {
    _stage[i]._enable= mat._stage[i]._enable;
    _stage[i]._name= mat._stage[i]._name;
    _stage[i]._protected= mat._stage[i]._protected;
    _stage[i]._texture= mat._stage[i]._texture;
    _stage[i]._uvSource= mat._stage[i]._uvSource;
    _stage[i]._uv_none= mat._stage[i]._uv_none;
    _stage[i]._aside= mat._stage[i]._aside;
    _stage[i]._up= mat._stage[i]._up;
    _stage[i]._dir= mat._stage[i]._dir;
    _stage[i]._pos= mat._stage[i]._pos;
  }
//! mozne problemy - co kopirovat a co ne?
  //_file_name= mat._file_name;  //to ne, chci vzdy kopirovat pouze parametry z jednoho souboru do druheho
  //_mat_name= mat._mat_name;    //to ne, chci vzdy kopirovat pouze parametry z jednoho souboru do druheho
  //_temp= mat._temp;            //nesmim predavat! 
  _disable= mat._disable;        //toto asi ano, ve vetsine pripadu TRUE
  //_changed= mat._changed;      //toto ne
  _changed= _to_save= TRUE;      //toto je hodne rozumne
  //_to_save= mat._to_save;      //toto asi ne
  //_selected= mat._selected;    //toto skoro urcite ne, mozny vznik nekonzistence s view->_mat_list
  //_ro, _ss_checked_out, _ss_controled, _ss_deleted   //to ne, v zadnem pripade

  //structures
  //_texture= NULL;                //netreba kopirovat

  return *this;
}


//---------------------------   Get & Set   --------------------------------------------------


Color& Material::GetAmbient()
{
  return _ambient;
}


Color& Material::GetDiffuse()
{
  return _diffuse;
}


Color& Material::GetForcedDiffuse()
{
  return _forcedDiffuse;
}


Color& Material::GetEmmisive()
{
  return _emmisive;
}


Color& Material::GetSpecular()
{
  return _specular;
}


float Material::GetSpecularPower() const
{
  return _specularPower;
}


void Material::SetSpecularPower(float specularPower)
{
  if ( specularPower>=0 )
    _specularPower= specularPower;
}


RString Material::GetPixelShaderID() const
{
  return _PixelShaderID;
}


void Material::SetPixelShaderID(const RString& PixelShaderID)
{
  _PixelShaderID= PixelShaderID;
}


RString Material::GetVertexShaderID() const
{
  return _VertexShaderID;
}


void Material::SetVertexShaderID(const RString& VertexShaderID)
{
  _VertexShaderID= VertexShaderID;
}


int Material::GetNstage()
{
	return _Nstage;
}


BOOL Material::SetCurStage(int pos)
{
	if ( pos >= 0 && pos < _Nstage )
	{
		_cur_stage= pos;
		return TRUE;
	}
	else
		return FALSE;
}


int Material::GetIndCurStage()
{
	return _cur_stage;
}


Stage& Material::GetCurStage()
{
  Assert(_Nstage);
  if ( !_Nstage )
  {
    LogF("No stage.");
    return _stage[0];             //_cur_stage muze byt -1
  }
	return _stage[_cur_stage];
}


Stage& Material::GetStage(int pos)
{
	Assert(pos>=0 && pos<_Nstage);
  if ( pos >= 0 && pos < _Nstage )
    return _stage[pos];
  else
  {
    Stage s;
    return s;
  }
}


//add a new stage at 'position'
BOOL Material::AddStage(Stage stage, int pos)
{
  if ( _Nstage > MAX_STAGES  || pos > _Nstage || pos < 0 )    //muzu pridat i na pozici _Nstage
  {
    LogF("The stage can not be added. The number of stages is maximum or the stage position is out of range.");
    return FALSE;
  }
  for ( int i= _Nstage; i>pos; i-- )
    _stage[i]= _stage[i-1];
  _stage[pos]= stage;
  _cur_stage= pos;
  _Nstage++;

  return TRUE;
}


//add a new stage on the end of stage list
BOOL Material::AddStage(Stage stage)
{
  return AddStage(stage, _Nstage);
}


BOOL Material::RemoveStage(int pos)
{
  if ( pos >= _Nstage || pos < 0 || !_Nstage ) 
  {
    LogF("No stages or the stage position is out of range.");
    return FALSE;
  }
  for ( int i= pos; i<_Nstage-1; i++ )
    _stage[i]= _stage[i+1];
  _cur_stage= 0;             //pokud byla smazana stage s ukazatelem _cur_stage, _cur_stage se nemeni a tim automaticky prejde na dalsiho
  _Nstage--;
  if ( !_Nstage )
    _cur_stage= -1;

  return TRUE;
}


//remove actual stage
BOOL Material::RemoveStage()
{
  return RemoveStage(_cur_stage);
}


//set the protected flag for all stages
void Material::ProtectStages(BOOL prt)
{
  for ( int i= 0; i < _Nstage; i++ )
    _stage[i].Protected(prt);
}


BOOL Material::Changed() const
{
  return _changed;
}


void Material::Changed(BOOL changed)
{
  _changed= changed;
  _to_save= TRUE;
}


BOOL Material::ToSave() const
{
  return _to_save;
}


void Material::ToSave(BOOL tosave)
{
  _to_save= tosave;
}


BOOL Material::Selected() const
{
  return _selected;
}


void Material::Selected(BOOL selected)
{
  _selected= selected;
}


BOOL Material::RO() const
{
  return _ro;
}


void Material::RO(BOOL ro)
{
  _ro= ro;
}


void Material::SSCheckedOut(BOOL co)
{
  _ss_checked_out= co;
}


BOOL Material::SSCheckedOut() const
{
  return _ss_checked_out;
}


void Material::SSControlled(BOOL ssc)
{
  _ss_controled= ssc;
}


BOOL Material::SSDeleted() const
{
  return _ss_deleted;
}


void Material::SSDeleted(BOOL deleted)
{
  _ss_deleted= deleted;
}


BOOL Material::SSControlled() const
{
  return _ss_controled;
}


RString Material::GetMatName() const
{
  return _mat_name;
}


RString Material::GetFileName() const
{
  return _file_name;
}


void Material::SetFileName(const RString& file_name)
{
  _file_name= file_name;
  //change _mat_name too
  WFilePath m_name= _mat_name;
  m_name.SetFilename(((WFilePath)_file_name).GetFilename());
  _mat_name= (RString)(m_name.GetDirectory()+m_name.GetFilename());
}


RString Material::GetTempName() const
{
  return _temp;
}


//set material by file_name
BOOL Material::SetMat(const RString& file_name)
{
  RString fn= _file_name;
  BOOL ret= Load(file_name);
  _file_name= fn;

  ProtectStages(TRUE);

  return ret;
}


//---------------------------   temp files   -------------------------------------------------


//create temp. file number 'num'
void Material::CreateTemp(int num, const RString& mat_path)
{
  char str[4];

  itoa(num, str, 10);
//  _temp= "temp";
  _temp= mat_path + TEMP_DIR + "temp" + str + ".rvmat";
  _changed= TRUE;         //SaveTemp() save only changed materials
  SaveTemp();
}


//save to temporary file
BOOL Material::SaveTemp()
{
  BOOL r= TRUE;
  if ( _changed )
  {
    r= Save(_temp, TRUE);
    _changed= FALSE;
  }
  return r;
}


//---------------------------   Load & Save   -------------------------------------------------


//load from file
BOOL Material::Load( RString file_name )
{
  ParamFile f; 
  ConstParamEntryPtr entry;
  int entry_size;

  f.Parse(file_name);
  if ( !f.GetEntryCount() ) 
  {
    LogF("No entry in %s.", file_name);
    return FALSE;
  }
  //ambient
  entry= f.FindEntry("ambient");
  if ( !entry )
    LogF("Entry 'ambient' doesn't exist.");
  else
  {
    if ( !entry->IsArray() )
      LogF("Entry 'ambient' is not an array.");
    else 
    {
      if ( entry->GetSize() < 3 )
        LogF("Entry 'ambient' has less than 3 parameters.");
      else 
      {
        _ambient._r= entry->operator[](0);
        _ambient._g= entry->operator[](1);
        _ambient._b= entry->operator[](2);
        _ambient._a= entry->operator[](3);
      }
    }
  }
  //diffuse
  entry= f.FindEntry("diffuse");
  if ( !entry )
    LogF("Entry 'diffuse' doesn't exist.");
  else
  {
    if ( !entry->IsArray() )
      LogF("Entry 'diffuse' is not an array.");
    else 
    {
      if ( entry->GetSize() < 3 )
        LogF("Entry 'diffuse' has less than 3 parameters.");
      else
      {
        _diffuse._r= entry->operator[](0);
        _diffuse._g= entry->operator[](1);
        _diffuse._b= entry->operator[](2);
        _diffuse._a= entry->operator[](3);
      }
    }
  }
  //forcedDiffuse
  entry= f.FindEntry("forcedDiffuse");
  if ( !entry )
    LogF("Entry 'forcedDiffuse' doesn't exist.");
  else
  {
    if ( !entry->IsArray() )
      LogF("Entry 'forcedDiffuse' is not an array.");
    else
    {
      if ( entry->GetSize() < 3 )
        LogF("Entry 'forcedDiffuse' has less than 3 parameters.");
      else
      {
        _forcedDiffuse._r= entry->operator[](0);
        _forcedDiffuse._g= entry->operator[](1);
        _forcedDiffuse._b= entry->operator[](2);
        _forcedDiffuse._a= entry->operator[](3);
      }
    }
  }
  //emmisive
  entry= f.FindEntry("emmisive");
  if ( !entry )
    LogF("Entry 'emmisive' doesn't exist.");
  else
  {
    if ( !entry->IsArray() )
      LogF("Entry 'emmisive' is not an array.");
    else
    {
      if ( entry->GetSize() < 3 )
        LogF("Entry 'emmisive' has less than 3 parameters.");
      else
      {
        _emmisive._r= entry->operator[](0);
        _emmisive._g= entry->operator[](1);
        _emmisive._b= entry->operator[](2);
        _emmisive._a= entry->operator[](3);
      }
    }
  }
  //specular
  entry= f.FindEntry("specular");
  if ( !entry )
    LogF("Entry 'specular' doesn't exist.");
  else
  {
    if ( !entry->IsArray() )
      LogF("Entry 'specular' is not an array.");
    else
    {
      if ( entry->GetSize() < 3 )
        LogF("Entry 'specular' has less than 3 parameters.");
      else
      {
        _specular._r= entry->operator[](0);
        _specular._g= entry->operator[](1);
        _specular._b= entry->operator[](2);
        _specular._a= entry->operator[](3);
      }
    }
  }
  //specularPower
  entry= f.FindEntry("specularPower");
  if ( !entry )
    LogF("Entry 'SpecularPower' doesn't exist.");
  else
    _specularPower= *entry;
  //PixelShaderID
  entry= f.FindEntry("PixelShaderID");
  if ( !entry )
    LogF("Entry 'PixelShaderID' doesn't exist.");
  else
    _PixelShaderID= entry->GetValue();
  //VertexShaderID
  entry= f.FindEntry("VertexShaderID");
  if ( !entry )
    LogF("Entry 'VertexShaderID' doesn't exist.");
  else
    _VertexShaderID= entry->GetValue();
  //Stages
  int i, j;
  entry_size= f.GetEntryCount();
  for ( i= 0, j= 0; i<entry_size; i++ )
  {
    if ( j>=MAX_STAGES ) {
      LogF("Too many stages in the file! Only %d are accepted.", MAX_STAGES);
      break;
    };
    ParamEntryVal subentry= f.GetEntry(i);
    if ( subentry.IsClass() ) {
      _stage[j]._name= subentry.GetName();
      if ( _stage[j]._name == (RString)NONAME )
        _stage[j]._name= "";
      //texture
      entry= subentry.FindEntry("texture");
      if ( !entry )
        LogF("Subentry 'texture' doesn't exist.");
      else
        _stage[j]._texture= entry->GetValue();
      //uvSource
      entry= subentry.FindEntry("uvSource");
      if ( !entry ) 
        LogF("Subentry 'uvSource' doesn't exist.");
      else
        _stage[j]._uvSource= entry->GetValue();
      if ( _stage[j]._uvSource == (RString)"none" )
        _stage[j]._uv_none= TRUE;                       
      else
        _stage[j]._uv_none= FALSE;                      //must
      if ( !_stage[j]._uv_none )                        //uvTransform exist
      {
        //uvTransform
        int subentry_size= subentry.GetEntryCount();
        for ( int k= 0; k<subentry_size; k++ ) {
          ParamEntryVal subsubentry= subentry.GetEntry(k);
          if ( subsubentry.IsClass() ) {
            //aside
            entry= subsubentry.FindEntry("aside");
            if ( !entry )
              LogF("Subentry 'aside' of 'uvTransform' doesn't exist.");
            else
            {
              if ( !entry->IsArray() )
                LogF("Subentry 'aside' of 'uvTransform' is not an array.");
              else
              {
                if ( entry->GetSize() < 3 )
                  LogF("Entry 'aside' has less than 3 parameters.");
                else
                {
                  _stage[j]._aside[0]= entry->operator[](0);
                  _stage[j]._aside[1]= entry->operator[](1);
                  _stage[j]._aside[2]= entry->operator[](2);
                }
              }
            }
            //up
            entry= subsubentry.FindEntry("up");
            if ( !entry )
              LogF("Subentry 'up' of 'uvTransform' doesn't exist.");
            else
            {
              if ( !entry->IsArray() )
                LogF("Subentry 'up' of 'uvTransform' is not an array.");
              else
              {
                if ( entry->GetSize() < 3 )
                  LogF("Entry 'up' has less than 3 parameters.");
                else
                {
                  _stage[j]._up[0]= entry->operator[](0);
                  _stage[j]._up[1]= entry->operator[](1);
                  _stage[j]._up[2]= entry->operator[](2);
                }
              }
            }
            //dir
            entry= subsubentry.FindEntry("dir");
            if ( !entry )
              LogF("Subentry 'dir' of 'uvTransform' doesn't exist.");
            else
            {
              if ( !entry->IsArray() )
                LogF("Subentry 'dir' of 'uvTransform' is not an array.");
              else
              {
                if ( entry->GetSize() < 3 )
                  LogF("Entry 'dir' has less than 3 parameters.");
                else
                {
                  _stage[j]._dir[0]= entry->operator[](0);
                  _stage[j]._dir[1]= entry->operator[](1);
                  _stage[j]._dir[2]= entry->operator[](2);
                }
              }
            }
            //pos
            entry= subsubentry.FindEntry("pos");
            if ( !entry )
              LogF("Subentry 'pos' of 'uvTransform' doesn't exist.");
            else
            {
              if ( !entry->IsArray() )
                LogF("Subentry 'pos' of 'uvTransform' is not an array.");
              else
              {
                if ( entry->GetSize() < 3 )
                  LogF("Entry 'pos' has less than 3 parameters.");
                else
                {
                  _stage[j]._pos[0]= entry->operator[](0);
                  _stage[j]._pos[1]= entry->operator[](1);
                  _stage[j]._pos[2]= entry->operator[](2);
                }
              }
            }
          }
        }  //for
      }  // if ( !_uv_none )
      j++;          //number of stages
    }  //if
  }  //for 
  _Nstage= j;
  //file_name
  _file_name= file_name;
  //ro
//  if ( !_access(file_name, 02) )
//    RO(TRUE);
//  else 
//    RO(FALSE);
  
  return TRUE;
}


BOOL Material::Load()
{
  //return Load(_mat_name);              
  //! nalezena chyba, driv to fungoval, protoze bud se Load() nikdy (bez parametru nevolal), nebo
  //volal, ale aktualni adresar byl p:\
  
  return Load(_file_name);
}


BOOL Material::Save( RString file_name, BOOL temp )
{
  int i, j;
  ParamFile f;
  ParamClassPtr stage;
  ParamClassPtr uvTransform;

  //ambient
  ParamEntryPtr ambient_array= f.AddArray("ambient");
  ambient_array->ReserveArrayElements(4);
  if ( temp && _disable )
  {
    ambient_array->AddValue(_ambient_disable._r);
    ambient_array->AddValue(_ambient_disable._g);
    ambient_array->AddValue(_ambient_disable._b);
    ambient_array->AddValue(_ambient_disable._a);
  }
  else
  {
    ambient_array->AddValue(_ambient._r);
    ambient_array->AddValue(_ambient._g);
    ambient_array->AddValue(_ambient._b);
    ambient_array->AddValue(_ambient._a);
  }
  //diffuse
  ParamEntryPtr diffuse_array= f.AddArray("diffuse");
  diffuse_array->ReserveArrayElements(4);
  if ( temp && _disable )
  {
    diffuse_array->AddValue(_diffuse_disable._r);
    diffuse_array->AddValue(_diffuse_disable._g);
    diffuse_array->AddValue(_diffuse_disable._b);
    diffuse_array->AddValue(_diffuse_disable._a);
  }
  else
  {
    diffuse_array->AddValue(_diffuse._r);
    diffuse_array->AddValue(_diffuse._g);
    diffuse_array->AddValue(_diffuse._b);
    diffuse_array->AddValue(_diffuse._a);
  }
  //forcedDiffuse
  ParamEntryPtr forcedDiffuse_array= f.AddArray("forcedDiffuse");
  forcedDiffuse_array->ReserveArrayElements(4);
  forcedDiffuse_array->AddValue(_forcedDiffuse._r);
  forcedDiffuse_array->AddValue(_forcedDiffuse._g);
  forcedDiffuse_array->AddValue(_forcedDiffuse._b);
  forcedDiffuse_array->AddValue(_forcedDiffuse._a);
  //emmisive
  ParamEntryPtr emmisive_array= f.AddArray("emmisive");
  emmisive_array->ReserveArrayElements(4);
  emmisive_array->AddValue(_emmisive._r);
  emmisive_array->AddValue(_emmisive._g);
  emmisive_array->AddValue(_emmisive._b);
  emmisive_array->AddValue(_emmisive._a);
  //specular
  ParamEntryPtr specular_array= f.AddArray("specular");
  specular_array->ReserveArrayElements(4);
  specular_array->AddValue(_specular._r);
  specular_array->AddValue(_specular._g);
  specular_array->AddValue(_specular._b);
  specular_array->AddValue(_specular._a);
  f.Add("specularPower", _specularPower);
  //shaders
  if ( _PixelShaderID != RString("") )
    f.Add("PixelShaderID", _PixelShaderID);
  if ( _VertexShaderID != RString("") )
    f.Add("VertexShaderID", _VertexShaderID);
  //stages
  for ( i= 0; i<_Nstage; i++ )
  {
    //pokud ukladam temp a stage je disable, tak stage neulozim
    if ( temp && !GetStage(i).Enable() )
      break;
    if ( _stage[i]._name == (RString)"" )
      stage= f.AddClass(NONAME);
    else
      stage= f.AddClass(_stage[i]._name);   
    stage->Add("texture", _stage[i]._texture);
    stage->Add("uvSource", _stage[i]._uvSource);
    if ( !_stage[i]._uv_none )
    {
      uvTransform= stage->AddClass("uvTransform");
      //aside, up, dir, pos
      ParamEntryPtr aside_arr= uvTransform->AddArray("aside");
      aside_arr->ReserveArrayElements(3);
      ParamEntryPtr up_arr= uvTransform->AddArray("up");
      up_arr->ReserveArrayElements(3);
      ParamEntryPtr dir_arr= uvTransform->AddArray("dir");
      dir_arr->ReserveArrayElements(3);
      ParamEntryPtr pos_arr= uvTransform->AddArray("pos");
      pos_arr->ReserveArrayElements(3);
      for ( j= 0; j<3; j++ ) 
      {
        aside_arr->AddValue(_stage[i]._aside[j]);
        up_arr->AddValue(_stage[i]._up[j]);
        dir_arr->AddValue(_stage[i]._dir[j]);
        pos_arr->AddValue(_stage[i]._pos[j]);
      }
    }
  }  //for all stages
  if ( f.Save(file_name) != LSOK )
  {
    LogF("Can not save %s", file_name);
    return FALSE;
  }
  if ( !f.GetEntryCount() )
  {
    LogF("No entry.");
    return FALSE;
  }
  if ( !temp )
    _to_save= FALSE;
  return TRUE;
}


//save
BOOL Material::Save(const RString& file_name)
{
  BOOL save= _to_save;

  BOOL r= Save( file_name, FALSE);         
  _to_save= save;                        //save in the other file
  return r;
}


//save
int Material::Save()
{
  if ( _to_save )
    return Save(_file_name, FALSE);
  else
    return 1;          //ok - neni treba ukladat
}


//---------------------------   Material changes   ------------------------------------------


//back to the normal material
void Material::Enable()
{
  if ( _disable )
  {
    _disable= FALSE;
    _changed= TRUE;
  }
  //if material is normal (not gray) do nothing
}


//get greyer
void Material::Disable()
{
  if ( !_disable )
  {
    switch ( g_unselected_mats )
    {
      case DMC_GRAY:
      {
        _ambient_disable= _ambient;
        _ambient_disable._r*= g_coef_gray;
        _ambient_disable._g*= g_coef_gray;
        _ambient_disable._b*= g_coef_gray;
        _diffuse_disable= _diffuse;
        _diffuse_disable._r*= g_coef_gray;
        _diffuse_disable._g*= g_coef_gray;
        _diffuse_disable._b*= g_coef_gray;
      }
      break;
      case DMC_RED:
      {
        _ambient_disable._r= 1;
        _ambient_disable._g= 0;
        _ambient_disable._b= 0;
        _ambient_disable._a= _ambient._a;
        _diffuse_disable._r= 1;
        _diffuse_disable._g= 0;
        _diffuse_disable._b= 0;
        _diffuse_disable._a= _diffuse._a;
      }
      break;
      case DMC_GREEN:
      {
        _ambient_disable._r= 0;
        _ambient_disable._g= 1;
        _ambient_disable._b= 0;
        _ambient_disable._a= _ambient._a;
        _diffuse_disable._r= 0;
        _diffuse_disable._g= 1;
        _diffuse_disable._b= 0;
        _diffuse_disable._a= _diffuse._a;
      }
      break;
      case DMC_BLUE:
      {
        _ambient_disable._r= 0;
        _ambient_disable._g= 0;
        _ambient_disable._b= 1;
        _ambient_disable._a= _ambient._a;
        _diffuse_disable._r= 0;
        _diffuse_disable._g= 0;
        _diffuse_disable._b= 1;
        _diffuse_disable._a= _diffuse._a;
      }
      break;
      default:
      {
        _ambient_disable= _ambient;
        _ambient_disable._r*= g_coef_gray;
        _ambient_disable._g*= g_coef_gray;
        _ambient_disable._b*= g_coef_gray;
        _diffuse_disable= _diffuse;
        _diffuse_disable._r*= g_coef_gray;
        _diffuse_disable._g*= g_coef_gray;
        _diffuse_disable._b*= g_coef_gray;
      }
    }
    _disable= TRUE;
    _changed= TRUE;
  }
  //if material is gray, it doesn't need get grayer
}


BOOL Material::Enabled() const
{
  return !_disable;
}


//***************************   Materials   *******************************************************************


//inicializace _lods
void Materials::CreateLodsMat()
{
  int* cur= (int*) malloc(_Nlod*sizeof(int));

  for ( int c= 0; c<_Nlod; c++ )
    _lods[c]._mat= (int*) malloc(_lods[c]._Nmat*sizeof(int));
  for ( int c= 0; c<_Nlod; c++ )
    cur[c]= 0;
  for ( int j= 0; j<_Nmat; j++ )
  {
    for ( int c= 0; c<_Nlod; c++ )
    {
      BOOL find= FALSE;
      for ( int i= 0; i<_lods[c]._Nface && !find; i++ )
      {
        if ( _lods[c]._face[i] == j )
        {
          _lods[c]._mat[cur[c]++]= j;
          find= TRUE;
        }
      }
    }
  }
  free(cur);
}


//find out the number of "normal" LODs in _obj
void Materials::SetNlod()
{
  float res;
  int i, n= 0;

  if ( _obj == NULL )
  {
    _Nlod= 0;
    return;
  }

  for ( i= 0; i<_obj->NLevels(); i++ )
  {
    res= _obj->Resolution(i);
    //zachyti obycejne lody, Gunner, Pilot, Commander
    if ( res < 1500 )           
      n++;
    else
      break;
  }
  _Nlod= n;
}


//load all material from p3d-object
BOOL Materials::LoadMaterialsFromFiles()
{
  if ( _Nmat )
  {
    BOOL find;
    _mat= new Material[_Nmat];
    _sel_mat= (int*) malloc(_Nmat*sizeof(int));
    _Nsel_mat= 0;
    for ( int j= 0; j<_Nmat; j++ )
    {
      //find the first face with material j
      find= FALSE;
      for ( int c= 0; c<_Nlod && !find; c++ )             
      {
        for ( int i= 0; i<_lods[c]._Nface && !find; i++ )
        {
          //material 'j' is in lod 'c' in face 'i'
          if ( _lods[c]._face[i] == j )
          {
            /* 
            //load the material
            if ( strcspn( (const char*)_obj->Level(c)->Face(i).vMaterial, ":" ) < strlen((const char*)_obj->Level(c)->Face(i).vMaterial) )
            {
              //sem by to nikdy nemelo jit, je to tu jen pro jistotu
              if ( !_mat[j].Load((const char*) (_obj->Level(c)->Face(i).vMaterial)) ) {
                LogF("No rvmat file: %s.", _obj->Level(c)->Face(i).vMaterial);
                return FALSE;
              }
            }
            else
            {
            */
            if ( !_mat[j].Load((const char*) (_mat_path+_obj->Level(c)->Face(i).vMaterial)) )
            {
              LogF("No rvmat file: %s.", _obj->Level(c)->Face(i).vMaterial);
              return FALSE;
            }
          //}
            _mat[j]._mat_name= _obj->Level(c)->Face(i).vMaterial;
            _mat[j].CreateTemp(j, _mat_path);
            find= TRUE;
          }
        }
      }
    }    //for
  }   //if
  else
    return FALSE;
  return TRUE;
}


//load all materials from the current Lod
//create _face
BOOL Materials::LoadMaterialsFromObj()
{
  if ( _obj == NULL )
    return FALSE;
  SetNlod();                //compute the number of lods
  _lods= (Lod*) malloc(_Nlod*sizeof(Lod));
  for ( int c= 0; c<_Nlod; c++ )
  {
    _lods[c]._Nface= _obj->Level(c)->NFaces();
    _lods[c]._Nmat= 0;
    _lods[c]._face= (int*) malloc(_lods[c]._Nface*sizeof(int));
    for ( int i= 0; i<_lods[c]._Nface; i++ )
      _lods[c]._face[i]= UNKNOWN_MAT;
  }
  //index materials
  _Nmat= 0;
  //projdu vsechny lody
  for ( int c= 0; c<_Nlod; c++ )
  {
    //pro kazdy lod projdu vsechny facy
    for ( int i= 0; i<_lods[c]._Nface; i++ )
    {
      if ( _lods[c]._face[i] == UNKNOWN_MAT )
      {
        if ( _obj->Level(c)->Face(i).vMaterial == RString("") )
          _lods[c]._face[i]= NO_MAT;
        else
        {
          if ( FileExist(_mat_path+_obj->Level(c)->Face(i).vMaterial) )
            _lods[c]._face[i]= _Nmat++;
          else
          {
            _lods[c]._face[i]= NO_MAT;
            RString str(_obj->Level(c)->Face(i).vMaterial, " can not be loaded");
            LogF(str);
          }
        }
        //vyhledam a oznacim vsechny facy (ve vsech lodech) se stejnym jmenem
        //zacnu od aktualniho lodu 'c', predchozi jsou jiz oznaceny
        for ( int fc= c; fc<_Nlod; fc++ )
        {
          BOOL first_in_lod= TRUE;
          int fi;
          if ( fc == c )
            fi= i;              //v aktualnim lodu nemusim prochazet vsechny facy, ale jen od aktualniho dal
          else
            fi= 0;
          for ( ; fi<_lods[fc]._Nface; fi++ )
          {
            if ( _obj->Level(fc)->Face(fi).vMaterial == _obj->Level(c)->Face(i).vMaterial )
            {
              _lods[fc]._face[fi]= _lods[c]._face[i];
              //v kazdem lodu si pamatuju pocet materialu
              if ( first_in_lod && _lods[c]._face[i] != NO_MAT )
              {
                _lods[fc]._Nmat++;
                first_in_lod= FALSE;
              }
            }
          }
        }
      }
    }
  }
  CreateLodsMat();
  //load all materials
  return LoadMaterialsFromFiles();
}


void Materials::SetMatID()
{
  for ( int i= 0; i < _Nmat; i++ )
    _mat[i]._id= i;
}


//unselect selected materials
void Materials::ResetSelMat()
{
  _Nsel_mat= 0;
  for ( int i= 0; i < _Nmat; i++ )
    _mat[i].Selected(FALSE);
}


//create harmless material
int Materials::CreateHsMat()
{
  if ( !_hs_mat )
    delete _hs_mat;
  _hs_mat= new Material;
  _hs_mat->_file_name= _hs_mat->_mat_name= _hs_mat->_temp= _temp_path + HARMLESS_MAT;
  if ( !_hs_mat->Save(_hs_mat->_file_name) )
    return 0;
  return 1;
}


//set p3d-object to temporary rvmat-files
void Materials::ConnectObjTemp()
{
  //all lods
  for ( int c= 0; c<_Nlod; c++ )
  {
    //all faces in the lod
    for ( int i= 0; i<_lods[c]._Nface; i++ )
    {
      int ind= _lods[c]._face[i];
      if ( ind != NO_MAT )
        _obj->Level(c)->Face(i).vMaterial= _mat[ind]._temp;
      else
        _obj->Level(c)->Face(i).vMaterial= _hs_mat->_temp;
    }
  }
}


//remove temporary materials from object
void Materials::DisconnectObjTemp()
{
  for ( int c= 0; c<_Nlod; c++ )
  {
    for ( int i= 0; i<_lods[c]._Nface; i++ )
    {
      int ind= _lods[c]._face[i];
      if ( ind != NO_MAT )
        _obj->Level(c)->Face(i).vMaterial= _mat[ind]._mat_name;
      else
        _obj->Level(c)->Face(i).vMaterial= "";
    }
  }
}


//delete temporary files
void Materials::DeleteTemps()
{
  int i;

  for ( i= 0; i<_Nmat; i++ )
    DeleteFile(_mat[i]._temp);
  if ( _hs_mat )
    DeleteFile(_hs_mat->_file_name);
  RemoveDirectory((const char*) _temp_path);
}


//destruct all materials and delete all temporary files
void Materials::Destruct()
{
  DeleteTemps();
  if ( _mat )
  {
    delete []_mat;
    _mat= NULL;
  }
  if ( _sel_mat )
  {
    free(_sel_mat);
    _sel_mat= NULL;
  }
  if ( _hs_mat )
  {
    delete _hs_mat;
    _hs_mat= NULL;
  }
  if ( _lods )
  {
    for ( int i= 0; i<_Nlod; i++ )
    {
      free(_lods[i]._face);
      free(_lods[i]._mat);
    }
    free(_lods);
    _lods= NULL;
  }
  _Nmat= _Nsel_mat= _Nlod= 0;
  _cur_lod= -1;
  _mat_path= _temp_path= "";
  //Do not delete _obj!
}


//---------------------------   public   ------------------------------------------------
 

//constructor
Materials::Materials()
{
  _mat= NULL;
  _sel_mat= NULL;
  _hs_mat= NULL;
  _lods= NULL;
  _Nmat= _Nsel_mat= _Nlod= 0;
  _cur_lod= NULL;
  _obj= NULL;
  //paths
  _mat_path= _temp_path= "";
  //flags
  _changed= FALSE;
};


//destructor
Materials::~Materials()
{
  Destruct();
};


//nastavi _obj
//pozor, muze dojit k zahniti pameti
void Materials::SetObj(LODObject* obj)
{
  _obj= obj;
}


LODObject* Materials::GetObj()
{
  return _obj;
}



//load the list of materials from p3d object
BOOL Materials::Load(LODObject* obj, const RString& mat_path)
{
  if ( !obj )
    return FALSE;
  Destruct();
  _obj= obj;
  if ( !SetMatPath(mat_path) )
    return FALSE;
  _cur_lod= 0;
  if ( !LoadMaterialsFromObj() )
    return FALSE;
  if ( !SetSelMatCurLod(0) )
    return FALSE;
  //set p3d-object to temporary rvmat-files
  ConnectObjTemp();
  SetMatID();

  return TRUE;
}


//save changed selected materials
BOOL Materials::SaveSel()
{
  BOOL r= TRUE;
  for ( int i= 0; i<_Nmat; i++ )
  {
    if ( _mat[i]._selected )
      if ( !GetMat(i).Save() )
        r= FALSE;
  }
  return r;
}


//save all materials
BOOL Materials::SaveAll()
{
  BOOL r= TRUE;
  for ( int i= 0; i<_Nmat; i++ )
  {
    if ( !_mat[i].Save() )
      r= FALSE;
  }
  return r;
}


//create temp files
BOOL Materials::SaveTemps()
{
  BOOL r= TRUE;
  for ( int i= 0; i<_Nmat; i++ )
  {
    if ( !_mat[i].SaveTemp() )
      r= FALSE;
  }
  if (_hs_mat==NULL || !_hs_mat->SaveTemp() )
    r= FALSE;
  return r;
}


int Materials::GetNlod() const
{
  return _Nlod;
}


//change the LOD
BOOL Materials::SetCurLod(int lod)
{
  if ( lod < 0 && lod > _Nlod )
    return FALSE;
  _cur_lod= lod;
  ResetSelMat();                              //unselect selected materials
  SetSelMatCurLod(0);
  EnableAllMat();
  if ( _obj == NULL )
    return FALSE;
  _obj->SelectLevel(lod);

  return TRUE;
}


//get current LOD
int Materials::GetCurLod() const
{
  return _cur_lod;
}


//get name of LOD
float Materials::GetNameLod(int lod) const
{
  return _obj->Resolution(lod);
}


//set the i-th material in the '_cur_lod'
BOOL Materials::SetSelMatCurLod(int i)
{
  if ( i < 0 || i >= _lods[_cur_lod]._Nmat )
    return FALSE;

  int c= _lods[_cur_lod]._mat[i];           //c - absolut index of material
  if ( _mat[c]._selected )
    return TRUE;                              //uz byl oznaceny

  _mat[c]._selected= TRUE;
  _sel_mat[_Nsel_mat++]= c;

  return TRUE;
}


//unselect material, i - order in current lod
BOOL Materials::UnsetSelMatCurLod(int i)
{
  if ( i < 0 || i >= _lods[_cur_lod]._Nmat )
    return FALSE;

  int c= _lods[_cur_lod]._mat[i];           //c - absolut index of material
  if ( !_mat[c]._selected )
    return TRUE;                            //uz byl odznaceny

  _mat[c]._selected= FALSE;
  for ( int j= 0; j < _Nsel_mat; j++ )
  {
    if ( _sel_mat[j] == c )
    {
      _sel_mat[j]= _sel_mat[_Nsel_mat-1];    //posledniho prehodim na misto mazaneho 
      _Nsel_mat--;
      break;
    }
  }
  return TRUE;
}


//unselect all materials
void Materials::UnsetAllSelMatCurLod()
{
  for ( int i= 0; i < _Nsel_mat; i++ )
    _mat[ _sel_mat[i] ]._selected= FALSE;
  _Nsel_mat= 0;
}


//get current materials from current lod
//return an array of pointers to materials
const int* Materials::GetSelMatCurLod() const
{
  if ( !_Nlod )
    return NULL;

  return _sel_mat;              //velikost pole je _lods[_cur_lod]._Nsel_mat
}


//get number of materials in current lod
int Materials::GetNmatCurLod() const
{
  if ( !_lods )
    return 0;
  return _lods[_cur_lod]._Nmat;
}


//get number of materials from 'lod'
int Materials::GetNmat(int lod) const
{
  if ( lod < 0 && lod >= _Nlod )
    return 0;
  return _lods[lod]._Nmat;
}


//get number of all materials
int Materials::GetNmatAll() const
{
  return _Nmat;
}


//get number of all selected materials
int Materials::GetNselmat() const
{
  return _Nsel_mat;
}


//get material (i is absolut index)
Material& Materials::GetMat(int i) const
{
  Assert(i>=0 && i<_Nmat);
	if ( i >= 0 && i < _Nmat )
    return _mat[i];
}


//get first material from current lod
Material& Materials::GetFirstSelMatCurLod() const
{
  Assert(_Nmat && _Nsel_mat);

  return _mat[_sel_mat[0]];
}


//get i-th material from current lod
Material& Materials::GetMatCurLod(int i) const
{
  Assert(i>=0 && i<_Nmat);
  Assert(i < _lods[_cur_lod]._Nmat);
	if ( i >= 0 && i < _Nmat )
    if ( i < _lods[_cur_lod]._Nmat )
      return _mat[ _lods[_cur_lod]._mat[i] ];
}


//get i-th material from 'lod'
Material& Materials::GetMat(int lod, int i) const
{
  Assert(lod>=0 && lod<_Nlod);
  Assert(i>=0 && i<_lods[lod]._Nmat);
  if ( lod >= 0 && lod < _Nlod )
		if ( i >= 0 && i < _lods[lod]._Nmat )
      return _mat[ _lods[lod]._mat[i] ];
}


//return the array of all materials
Material* Materials::GetAllMaterials()
{
  return _mat;
}


//return "harmless" material
Material& Materials::GetHsMat()
{
  return *_hs_mat;
}


BOOL Materials::ExistMat() const
{
  if ( !_Nmat )
    return FALSE;
  else
    return TRUE;
}


//exist any material in current lod?
BOOL Materials::ExistMatCurLod() const
{
  if ( _lods[_cur_lod]._Nmat )
    return TRUE;
  else
    return FALSE;
}


BOOL Materials::SetMatPath(const RString& mat_path)
{
  _mat_path= mat_path;
  //create temporary directory
  _temp_path= _mat_path;
  _temp_path= _temp_path + TEMP_DIR;
 
  if ( !PathIsDirectory((const char*) _temp_path) )
  {
    if ( !CreateDirectory((const char*) _temp_path, NULL) )
    {
      LogF("Temporary directory was not create.");
      return FALSE;
    }
  }
  //create harmless material
  if ( !CreateHsMat() )
    return FALSE;
  return TRUE;
}


//return the variable _mat_path
RString Materials::GetMatPath() const
{
  return _mat_path;
}


//return TRUE if a material was changed
BOOL Materials::Changed()
{
  if ( _changed )
    return TRUE;

  for ( int i= 0; i < _Nmat; i++ )
  {
    if ( _mat[i].Changed() )
    {
      _changed= TRUE;
      return TRUE;
    }
  }
  return FALSE;
}


//set the flag changed
void Materials::Changed(BOOL changed)
{
  _changed= changed;
}


//set the i-th material according to file_name
BOOL Materials::SetMat(int i, const RString& file_name)
{
  return GetMat(i).SetMat(file_name);
}


//enable selected materials, other material will be disabled (greyer)
void Materials::EnableSelMat()
{
  for ( int j= 0, j2= 0; j<_Nmat; j++ )
  {
    if (  j2 < _Nsel_mat  &&  j == _sel_mat[j2]  )
    {
      _mat[j].Enable();
      j2++;
    }
    else
      _mat[j].Disable();
  }
  _hs_mat->Disable();
}


//unselect all materials
void Materials::EnableAllMat()
{
  for ( int j= 0; j<_Nmat; j++ )
    _mat[j].Enable();
  _hs_mat->Enable();
}


//change gray coeficient
void Materials::ChangeGrayCoef()
{
  for ( int j= 0; j < _Nmat; j++ )
  {
    if ( !_mat[j].Enabled() )
    {
      _mat[j].Enable();
      _mat[j].Disable();
    }
  }
  _hs_mat->Disable();
}


//---------------------------   History   -----------------------------------------------


//history reset
void Materials::HistoryReset()
{
  _history.Reset();
}


//add all changed materials to history 
void Materials::HistoryAdd()
{
  int s= sizeof(Material);

  _history.StartAdd();
  for ( int i= 0; i < _Nmat; i++ )
  {
    if ( _mat[i]._changed )
      _history.Add(_mat[i]);
  }
  _history.EndAdd();
}


//make 'Undo'
void Materials::HistoryUndo()
{
  Material mat;

  _history.StartUndo();
  while ( !_history.EndUndo() ) {
    _history.Undo(mat);
    _mat[mat._id]= mat;                 
  }
}


//make 'Redo'
void Materials::HistoryRedo()
{
  Material mat;

  _history.StartRedo();
  while ( !_history.EndRedo() ) {
    _history.Redo(mat);
    _mat[mat._id]= mat;                 
  }
}


//return the last history action
int Materials::HistoryGetLastAction()
{
  return _history.GetLastAction();
}


//return the position in the history
int Materials::HistoryGetPos()
{
  return _history.GetPos();
}


//return TRUE if history is empty
BOOL Materials::HistoryEmpty()
{
  return _history.Empty();
}

  
//return TRUE if the current position in history is the last one, Redo is not avalible
BOOL Materials::HistoryLastPos()
{
  return _history.LastPos();
}


//---------------------------   Report&Info   -----------------------------------------


//write information about p3d file
void Materials::ObjReport()
{
  //vypise info o p3d
  FILE* f= fopen("p:\\report_obj.txt", "w");

  int Nface= 0;
  for ( int c= 0; c < _Nlod; c++ )
    Nface+= _lods[c]._Nface;
  fprintf(f, "Number of Lods: %d\nNumber of Faces: %d\nNumber of Materials: %d\n\n", _Nlod, Nface, _Nmat);

  for ( int c= 0; c < _Nlod; c++ )
    fprintf(f, "Lod %2d, %8.3f, number of faces: %4d, number of materials: %2d\n",
            c, _obj->Resolution(c), _lods[c]._Nface, _lods[c]._Nmat);
  fprintf(f, "\n");
  for ( int c= 0; c < _Nlod; c++ )
  {
    fprintf(f, "Lod %2d, %8.3f, number of faces: %d\n", c, _obj->Resolution(c), _lods[c]._Nface);
    for ( int j= 0; j < _lods[c]._Nface; j++ )
      fprintf(f, "lod: %2d, face: %4d, mat: %2d\n", c, j, _lods[c]._face[j]);
  }
  fclose(f);
}


