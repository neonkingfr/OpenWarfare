/*
  UpdateViewer.cpp
  ----------------

  October 2003

  Update Buldozer with current Lod.
*/


#include "stdafx.h"
#include "UpdateViewer.h"
#include "View\ddeviewer.hpp"
#include "MainFrm.h"
#include <afxmt.h>


//nezapojovat UPDATE_IN_THREAD ! Pak dochazi obcas k padum

//#define UPDATE_IN_THREAD

#define LODBIAS              1.0f


LODObject upd_obj;


#ifdef UPDATE_IN_THREAD

BOOL g_read= FALSE;
CMutex mut;


UINT UpdateThread(LPVOID pParam)
{
  ObjectData* lod= (ObjectData*) pParam;
  if (mut.Lock(0)==FALSE) 
  {
    g_read= TRUE;
    return 0;
  }
  LODObject upd_obj;
  upd_obj.AddLevel(*lod, 1.0f);
  g_read= TRUE;
  upd_obj.DeleteLevel(0);
  UpdateViewer(*frame, &upd_obj, LODBIAS);
  mut.Unlock();
  return 0;
}

#endif // UPDATE_IN_THREAD


//update Buldozer with current Lod
int UpdateViewer(LODObject* obj)
{
#ifdef UPDATE_IN_THREAD

  g_read= FALSE;
  AfxBeginThread(UpdateThread, obj->Active(), THREAD_PRIORITY_BELOW_NORMAL);
  while ( g_read ) Sleep(1);

#else

  //v upd_obj je vzdy jen jeden lod
  upd_obj.AddLevel(*(obj->Active()), 1.0f);
  upd_obj.DeleteLevel(0);
  UpdateViewer(*frame, &upd_obj, LODBIAS);

#endif

  return 1;
}


//close Buldozer
void CloseViewer()
{
  CloseViewer(NULL);
}
