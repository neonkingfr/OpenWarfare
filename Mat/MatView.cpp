// MatView.cpp : implementation of the CMatView class
//
 
#include "stdafx.h"
#include "Mat.h"
#include "MatDoc.h"
#include "MainFrm.h"

#include "View\ddeviewer.hpp"
#include "Material.hpp"
#include "UpdateViewer.h"
#include "Dialogs\OptionsDlg.h"
#include "Dialogs\SppDlg.h"
#include "Libs\ROCheck.h"
#include "El\ParamFile\paramFile.hpp"
#include "El\Scc\Scc.hpp"
#include ".\matview.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define NO_VAL            -1
#define NO_STR            "???"

#define F_ALL             1
#define F_SELECTED        2

#define FL_CHECKOUT       "v "
#define FL_NCHECKOUT      "   "               //2 spaces insted of 'v'
#define FL_RO             "!"
#define FL_NRO            " "
#define FL_SAVE           "* "
#define FL_NSAVE          "   "               //2 spaces insted of '*'

//timers
#define UPD_TIMER         1
#define UPD_TIME          500                 //0.5s
#define WEATHER_TIMER     2 
#define WEATHER_TIME      60000               //60s

//type of model
#define P3D_NO            0
#define P3D_EMPTY         1
#define P3D_NORMAL        2
#define P3D_PRIMITIVE     3

#define SCRIPT_FILE       "weather.sqs"

//type of special lods
#define SL_UNKNOWN        "Error - ???"
#define SL_GUNNER         "View - Gunner"
#define SL_PILOT          "View - Pilot"
#define SL_CARGO          "View - Cargo"

#define SS_PROJ_NAME      "$/"                 //default SS project name

//lights update
#define BULD_CLASS_NAME   "Operation Flashpoint"
#define BULD_WINDOW_NAME  "Buldozer"

#define LIST_LEN          78


//global
CMatView* view;

HANDLE g_thread_editor;
RString g_com_line;
HWND g_handle_view;

MsSccFunctions SccFcs;


//zaokrouhli na 2 desetina mista
float Round2(float f)          
{
  f= f*100;
  int i= toInt(f);
  f= (float) i;
  f= 0.01 * f;
  return f;
  //return 0.01 * (float) toInt(f*100+0.5);
}


//zkrati 'str' na delku 'n'
RString ClipText(RString& str, int n)
{
  int len= str.GetLength();

  if ( n > len ) 
    return str; 

  RString ret; //("...", str.Substring(len-n+3,len));
  
  ret= str.Substring(0, 20);
  ret= ret + "...";
  ret= ret + str.Substring(len-n+23, len);
    
  return ret;
}


// CMatView

IMPLEMENT_DYNCREATE(CMatView, cdxCDynamicFormView)

BEGIN_MESSAGE_MAP(CMatView, cdxCDynamicFormView)
  ON_WM_DESTROY()
  //edits
  ON_EN_CHANGE(IDC_EDIT_AMB_R, OnEnChangeEditAmbR)
  ON_EN_CHANGE(IDC_EDIT_AMB_G, OnEnChangeEditAmbG)
  ON_EN_CHANGE(IDC_EDIT_AMB_B, OnEnChangeEditAmbB)
  ON_EN_CHANGE(IDC_EDIT_AMB_A, OnEnChangeEditAmbA)
  ON_EN_CHANGE(IDC_EDIT_DIF_R, OnEnChangeEditDifR)
  ON_EN_CHANGE(IDC_EDIT_DIF_G, OnEnChangeEditDifG)
  ON_EN_CHANGE(IDC_EDIT_DIF_B, OnEnChangeEditDifB)
  ON_EN_CHANGE(IDC_EDIT_DIF_A, OnEnChangeEditDifA)
  ON_EN_CHANGE(IDC_EDIT_FOD_R, OnEnChangeEditFodR)
  ON_EN_CHANGE(IDC_EDIT_FOD_G, OnEnChangeEditFodG)
  ON_EN_CHANGE(IDC_EDIT_FOD_B, OnEnChangeEditFodB)
  ON_EN_CHANGE(IDC_EDIT_FOD_A, OnEnChangeEditFodA)
  ON_EN_CHANGE(IDC_EDIT_EMM_R, OnEnChangeEditEmmR)
  ON_EN_CHANGE(IDC_EDIT_EMM_G, OnEnChangeEditEmmG)
  ON_EN_CHANGE(IDC_EDIT_EMM_B, OnEnChangeEditEmmB)
  ON_EN_CHANGE(IDC_EDIT_EMM_A, OnEnChangeEditEmmA)
  ON_EN_CHANGE(IDC_EDIT_SPC_R, OnEnChangeEditSpcR)
  ON_EN_CHANGE(IDC_EDIT_SPC_G, OnEnChangeEditSpcG)
  ON_EN_CHANGE(IDC_EDIT_SPC_B, OnEnChangeEditSpcB)
  ON_EN_CHANGE(IDC_EDIT_SPC_A, OnEnChangeEditSpcA)
  ON_EN_CHANGE(IDC_EDIT_SPP, OnEnChangeEditSpp)
	ON_CBN_EDITCHANGE(IDC_COMBO_PSI, OnCbnEditchangeComboPsi)
	ON_CBN_SELCHANGE(IDC_COMBO_PSI, OnCbnSelchangeComboPsi)
	ON_CBN_EDITCHANGE(IDC_COMBO_VSI, OnCbnEditchangeComboVsi)
	ON_CBN_SELCHANGE(IDC_COMBO_VSI, OnCbnSelchangeComboVsi)
  //stage
  ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_STAGES, OnTcnSelchangeTabStages)
  ON_BN_CLICKED(IDC_CHECK_ENABLE_STAGE, OnBnClickedCheckEnableStage)
  ON_BN_CLICKED(IDC_B_NEW_STAGE, OnBnClickedBNewStage)
  ON_BN_CLICKED(IDC_B_DELETE_STAGE, OnBnClickedBDeleteStage)
  ON_EN_CHANGE(IDC_EDIT_STAGE_NAME, OnEnChangeEditStageName)
  ON_EN_CHANGE(IDC_EDIT_TEX, OnEnChangeEditTex)
  ON_BN_CLICKED(IDC_B_BROWS_TEXTURE, OnBnClickedBBrowsTexture)
  ON_BN_CLICKED(IDC_B_VIEW_TEXTURE, OnBnClickedBViewTexture)
  ON_BN_CLICKED(IDC_B_EXPLORE_TEXTURE, OnBnClickedBExploreTexture)
  ON_CBN_EDITCHANGE(IDC_COMBO_UVS, OnCbnEditchangeComboUvs)
  ON_CBN_SELCHANGE(IDC_COMBO_UVS, OnCbnSelchangeComboUvs)
  ON_EN_CHANGE(IDC_EDIT_AS0, OnEnChangeEditAs0)
  ON_EN_CHANGE(IDC_EDIT_AS1, OnEnChangeEditAs1)
  ON_EN_CHANGE(IDC_EDIT_AS2, OnEnChangeEditAs2)
  ON_EN_CHANGE(IDC_EDIT_UP0, OnEnChangeEditUp0)
  ON_EN_CHANGE(IDC_EDIT_UP1, OnEnChangeEditUp1)
  ON_EN_CHANGE(IDC_EDIT_UP2, OnEnChangeEditUp2)
  ON_EN_CHANGE(IDC_EDIT_DI0, OnEnChangeEditDi0)
  ON_EN_CHANGE(IDC_EDIT_DI1, OnEnChangeEditDi1)
  ON_EN_CHANGE(IDC_EDIT_DI2, OnEnChangeEditDi2)
  ON_EN_CHANGE(IDC_EDIT_PS0, OnEnChangeEditPs0)
  ON_EN_CHANGE(IDC_EDIT_PS1, OnEnChangeEditPs1)
  ON_EN_CHANGE(IDC_EDIT_PS2, OnEnChangeEditPs2)
  //spins
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_AMB_R, OnDeltaposSpinAmbR)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_AMB_G, OnDeltaposSpinAmbG)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_AMB_B, OnDeltaposSpinAmbB)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_AMB_A, OnDeltaposSpinAmbA)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_DIF_R, OnDeltaposSpinDifR)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_DIF_G, OnDeltaposSpinDifG)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_DIF_B, OnDeltaposSpinDifB)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_DIF_A, OnDeltaposSpinDifA)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FOD_R, OnDeltaposSpinFodR)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FOD_G, OnDeltaposSpinFodG)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FOD_B, OnDeltaposSpinFodB)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FOD_A, OnDeltaposSpinFodA)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_EMM_R, OnDeltaposSpinEmmR)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_EMM_G, OnDeltaposSpinEmmG)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_EMM_B, OnDeltaposSpinEmmB)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_EMM_A, OnDeltaposSpinEmmA)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SPC_R, OnDeltaposSpinSpcR)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SPC_G, OnDeltaposSpinSpcG)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SPC_B, OnDeltaposSpinSpcB)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SPC_A, OnDeltaposSpinSpcA)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SPP, OnDeltaposSpinSpp)
  //big spins
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_AMB, OnDeltaposSpinAmb)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_DIF, OnDeltaposSpinDif)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FOD, OnDeltaposSpinFod)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_EMM, OnDeltaposSpinEmm)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SPC, OnDeltaposSpinSpc)
  //checks
  ON_BN_CLICKED(IDC_CHECK_AMB_R, OnBnClickedCheckAmbR)
  ON_BN_CLICKED(IDC_CHECK_AMB_G, OnBnClickedCheckAmbG)
  ON_BN_CLICKED(IDC_CHECK_AMB_B, OnBnClickedCheckAmbB)
  ON_BN_CLICKED(IDC_CHECK_AMB_A, OnBnClickedCheckAmbA)
  ON_BN_CLICKED(IDC_CHECK_LOCK_AMBDIF, OnBnClickedCheckLockAmbDif)
  ON_BN_CLICKED(IDC_CHECK_DIF_R, OnBnClickedCheckDifR)
  ON_BN_CLICKED(IDC_CHECK_DIF_G, OnBnClickedCheckDifG)
  ON_BN_CLICKED(IDC_CHECK_DIF_B, OnBnClickedCheckDifB)
  ON_BN_CLICKED(IDC_CHECK_DIF_A, OnBnClickedCheckDifA)
  ON_BN_CLICKED(IDC_CHECK_FOD_R, OnBnClickedCheckFodR)
  ON_BN_CLICKED(IDC_CHECK_FOD_G, OnBnClickedCheckFodG)
  ON_BN_CLICKED(IDC_CHECK_FOD_B, OnBnClickedCheckFodB)
  ON_BN_CLICKED(IDC_CHECK_FOD_A, OnBnClickedCheckFodA)
  ON_BN_CLICKED(IDC_CHECK_EMM_R, OnBnClickedCheckEmmR)
  ON_BN_CLICKED(IDC_CHECK_EMM_G, OnBnClickedCheckEmmG)
  ON_BN_CLICKED(IDC_CHECK_EMM_B, OnBnClickedCheckEmmB)
  ON_BN_CLICKED(IDC_CHECK_EMM_A, OnBnClickedCheckEmmA)
  ON_BN_CLICKED(IDC_CHECK_SPC_R, OnBnClickedCheckSpcR)
  ON_BN_CLICKED(IDC_CHECK_SPC_G, OnBnClickedCheckSpcG)
  ON_BN_CLICKED(IDC_CHECK_SPC_B, OnBnClickedCheckSpcB)
  ON_BN_CLICKED(IDC_CHECK_SPC_A, OnBnClickedCheckSpcA)
  //sliders
  //ON_NOTIFY(NM_THEMECHANGED, IDC_SLIDER_AMB, OnNMThemeChangedSliderAmb)
  ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_AMB, OnNMReleasedcaptureSliderAmb)
  ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_DIF, OnNMReleasedcaptureSliderDif)
  ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_FOD, OnNMReleasedcaptureSliderFod)
  ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_EMM, OnNMReleasedcaptureSliderEmm)
  ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_SPC, OnNMReleasedcaptureSliderSpc)
  //buttons
  ON_BN_CLICKED(IDC_B_COL_AMB, OnBnClickedBColAmb)
  ON_BN_CLICKED(IDC_B_COL_DIF, OnBnClickedBColDif)
  ON_BN_CLICKED(IDC_B_COL_FOD, OnBnClickedBColFod)
  ON_BN_CLICKED(IDC_B_COL_EMM, OnBnClickedBColEmm)
  ON_BN_CLICKED(IDC_B_COL_SPC, OnBnClickedBColSpc)
  ON_BN_CLICKED(IDC_B_SPP, OnBnClickedBSpp)
  ON_BN_CLICKED(IDC_B_LIGHT_AMB, OnBnClickedBLightAmb)
  ON_BN_CLICKED(IDC_B_LIGHT_DIF, OnBnClickedBLightDif)
  //menu & toolbar
  ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
  ON_COMMAND(ID_FILE_SAVE, OnFileSave)
  ON_COMMAND(ID_FILE_SAVEAS, OnFileSaveas)
  ON_COMMAND(ID_FILE_SAVEALL, OnFileSaveall)
  ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
  ON_UPDATE_COMMAND_UI(ID_FILE_SAVEAS, OnUpdateFileSaveas)
  ON_UPDATE_COMMAND_UI(ID_FILE_SAVEALL, OnUpdateFileSaveall)
  ON_COMMAND(ID_FILE_GET_LV, OnFileGetLatestVersion)
  ON_UPDATE_COMMAND_UI(ID_FILE_GET_LV, OnUpdateFileGetLatestVersion)
  ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
  ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
  ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
  ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)
  ON_COMMAND(ID_BULD_VIEW, OnViewBuldozer)
  ON_COMMAND(ID_VIEW_EXTERNALEDITOR, OnViewExternalEditor)
  ON_UPDATE_COMMAND_UI(ID_VIEW_EXTERNALEDITOR, OnUpdateViewExternaleditor)
  ON_COMMAND(ID_PRIM_SPHERE, OnPrimSphere)
  ON_COMMAND(ID_PRIM_SPHERE2, OnPrimSphere2)
  ON_COMMAND(ID_PRIM_BOX, OnPrimBox)
  ON_COMMAND(ID_PRIM_CONE, OnPrimCone)
  ON_COMMAND(ID_PRIM_CONE2, OnPrimCone2)
  ON_COMMAND(ID_VIEW_MAINMODEL, OnViewMainModel)
  ON_UPDATE_COMMAND_UI(ID_VIEW_MAINMODEL, OnUpdateViewMainmodel)
  ON_COMMAND(ID_OPTIONS_CONFIGURATION, OnOptionsConfiguration)
  ON_COMMAND(ID_WEATHER, OnWeather)
  ON_UPDATE_COMMAND_UI(ID_WEATHER, OnUpdateWeather)
  //lod & material list
  ON_CBN_SELCHANGE(IDC_COMBO_LOD, OnCbnSelchangeComboLod)
  ON_LBN_SELCHANGE(IDC_LIST_MAT, OnLbnSelchangeListMat)
  //other controls
  ON_BN_CLICKED(IDC_CHECK_DIS_UNS_MAT, OnBnClickedCheckDisUnsMat)
  //timer
  ON_WM_TIMER()
  //popup menu
  ON_COMMAND(ID_PM_COPY, OnPmCopy)
  ON_UPDATE_COMMAND_UI(ID_PM_COPY, OnUpdatePmCopy)
  ON_COMMAND(ID_PM_PASTE, OnPmPaste)
  ON_UPDATE_COMMAND_UI(ID_PM_PASTE, OnUpdatePmPaste)
  ON_COMMAND(ID_PM_LOAD, OnPmLoad)
  ON_UPDATE_COMMAND_UI(ID_PM_LOAD, OnUpdatePmLoad)
  ON_COMMAND(ID_PM_LOAD_DEFAULT, OnPmLoadDefault)
  ON_UPDATE_COMMAND_UI(ID_PM_LOAD_DEFAULT, OnUpdatePmLoadDefault)
  ON_COMMAND(ID_PM_EXPLORE, OnPmExplore)
  ON_UPDATE_COMMAND_UI(ID_PM_EXPLORE, OnUpdatePmExplore)
  ON_COMMAND(ID_PM_GET_LATEST_VERSION, OnPmGetLatestVersion)
  ON_UPDATE_COMMAND_UI(ID_PM_GET_LATEST_VERSION, OnUpdatePmGetLatestVersion)
  ON_COMMAND(ID_PM_CHECK_OUT, OnPmCheckOut)
  ON_UPDATE_COMMAND_UI(ID_PM_CHECK_OUT, OnUpdatePmCheckOut)
  ON_COMMAND(ID_PM_CHECK_IN, OnPmCheckIn)
  ON_UPDATE_COMMAND_UI(ID_PM_CHECK_IN, OnUpdatePmCheckIn)
  ON_COMMAND(ID_PM_UNDO_CHECK_OUT, OnPmUndoCheckOut)
  ON_UPDATE_COMMAND_UI(ID_PM_UNDO_CHECK_OUT, OnUpdatePmUndoCheckOut)
  ON_COMMAND(ID_PM_HISTORY, OnPmHistory)
  ON_UPDATE_COMMAND_UI(ID_PM_HISTORY, OnUpdatePmHistory)
  ON_COMMAND(ID_PM_DIFF, OnPmDiff)
  ON_UPDATE_COMMAND_UI(ID_PM_DIFF, OnUpdatePmDiff)
  ON_COMMAND(ID_PM_PROPERTIES, OnPmProperties)
  ON_UPDATE_COMMAND_UI(ID_PM_PROPERTIES, OnUpdatePmProperties)
  ON_COMMAND(ID_PM_READ_ONLY, OnPmReadOnly)
  ON_UPDATE_COMMAND_UI(ID_PM_READ_ONLY, OnUpdatePmReadOnly)
  //size
  ON_WM_SIZE()
END_MESSAGE_MAP()


//---------------------------   Resizing   ----------------------------------------------


BEGIN_DYNAMIC_MAP(CMatView, cdxCDynamicFormView)
  DYNAMIC_MAP_ENTRY(IDC_LIST_MAT, mdResize, mdResize)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_DIS_UNS_MAT, mdRepos)
  //stages
  DYNAMIC_MAP_YENTRY(IDC_TAB_STAGES, mdRepos)
  DYNAMIC_MAP_ENTRY(IDC_BOX_GSTAGE, mdResize, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_S_STAGE, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_CHECK_ENABLE_STAGE, mdRepos)
  DYNAMIC_MAP_ENTRY(IDC_B_NEW_STAGE, mdRepos, mdRepos)
  DYNAMIC_MAP_ENTRY(IDC_B_DELETE_STAGE, mdRepos, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_S_NAME, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_STAGE_NAME, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_S_TEX, mdRepos)
  DYNAMIC_MAP_ENTRY(IDC_EDIT_TEX, mdResize, mdRepos)
  DYNAMIC_MAP_ENTRY(IDC_B_BROWS_TEXTURE, mdRepos, mdRepos)
  DYNAMIC_MAP_ENTRY(IDC_B_VIEW_TEXTURE, mdRepos, mdRepos)
  DYNAMIC_MAP_ENTRY(IDC_B_EXPLORE_TEXTURE, mdRepos, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_S_UV, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_COMBO_UVS, mdRepos)
  DYNAMIC_MAP_ENTRY(IDC_BOX_UV, mdResize, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_S_ASIDE, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_AS0, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_AS1, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_AS2, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_S_UP, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_UP0, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_UP1, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_UP2, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_S_DIR, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_DI0, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_DI1, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_DI2, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_S_POS, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_PS0, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_PS1, mdRepos)
  DYNAMIC_MAP_YENTRY(IDC_EDIT_PS2, mdRepos)
  //lights
  DYNAMIC_MAP_XENTRY(IDC_BOX_LIGHTS, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_LIGHT_AMB_LABEL, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_LIGHT_AMB_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_LIGHT_AMB_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_LIGHT_AMB_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_B_LIGHT_AMB, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_LIGHT_DIF_LABEL, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_LIGHT_DIF_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_LIGHT_DIF_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_LIGHT_DIF_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_B_LIGHT_DIF, mdRepos)
  //ambient
  DYNAMIC_MAP_XENTRY(IDC_BOX_AMB, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_AMB, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_AMB_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_AMB_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_AMB_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_AMB_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_AMB_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_AMB_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_AMB_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_AMB_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_AMB_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_AMB_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_AMB_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_AMB_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_B_COL_AMB, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SLIDER_AMB, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_AMB_0, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_AMB_1, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_LOCK_AMBDIF, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_AMB, mdRepos)
	//diffuse
  DYNAMIC_MAP_XENTRY(IDC_BOX_DIF, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_DIF, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_DIF_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_DIF_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_DIF_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_DIF_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_DIF_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_DIF_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_DIF_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_DIF_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_DIF_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_DIF_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_DIF_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_DIF_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_B_COL_DIF, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SLIDER_DIF, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_DIF_0, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_DIF_1, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_DIF, mdRepos)
	//forced diffuse
  DYNAMIC_MAP_XENTRY(IDC_BOX_FOD, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_FOD, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_FOD_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_FOD_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_FOD_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_FOD_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_FOD_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_FOD_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_FOD_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_FOD_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_FOD_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_FOD_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_FOD_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_FOD_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_B_COL_FOD, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SLIDER_FOD, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_FOD_0, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_FOD_1, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_FOD, mdRepos)
	//emmisive
  DYNAMIC_MAP_XENTRY(IDC_BOX_EMM, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_EMM, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_EMM_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_EMM_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_EMM_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_EMM_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_EMM_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_EMM_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_EMM_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_EMM_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_EMM_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_EMM_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_EMM_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_EMM_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_B_COL_EMM, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SLIDER_EMM, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_EMM_0, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_EMM_1, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_EMM, mdRepos)
	//specular
  DYNAMIC_MAP_XENTRY(IDC_BOX_SPC, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_SPC, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_SPC_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_SPC_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_SPC_R, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_SPC_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_SPC_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_SPC_G, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_SPC_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_SPC_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_SPC_B, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_SPC_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_SPC_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_CHECK_SPC_A, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_B_COL_SPC, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SLIDER_SPC, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_SPC_0, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_SPC_1, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_SPP, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_EDIT_SPP, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_SPP, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_B_SPP, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_SPIN_SPC, mdRepos)
	//shaders
  DYNAMIC_MAP_XENTRY(IDC_BOX_SHD, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_PSI, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_COMBO_PSI, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_S_VSI, mdRepos)
  DYNAMIC_MAP_XENTRY(IDC_COMBO_VSI, mdRepos)
END_DYNAMIC_MAP()


CMatView::CMatView()
	: cdxCDynamicFormView(CMatView::IDD)
  {
  g_handle_view= m_hWnd;
}

CMatView::~CMatView()
{
}

void CMatView::DoDataExchange(CDataExchange* pDX)
{
  cdxCDynamicFormView::DoDataExchange(pDX);
  //informations, configurations
  DDX_Check(pDX, IDC_CHECK_DIS_UNS_MAT, _disable_unselect_mats);
  //ambient
  DDX_Text(pDX, IDC_EDIT_AMB_R, _ambient_r);
  DDX_Text(pDX, IDC_EDIT_AMB_G, _ambient_g);
  DDX_Text(pDX, IDC_EDIT_AMB_B, _ambient_b);
  DDX_Text(pDX, IDC_EDIT_AMB_A, _ambient_a);
  DDX_Check(pDX, IDC_CHECK_AMB_R, _check_amb_r);
  DDX_Check(pDX, IDC_CHECK_AMB_G, _check_amb_g);
  DDX_Check(pDX, IDC_CHECK_AMB_B, _check_amb_b);
  DDX_Check(pDX, IDC_CHECK_AMB_A, _check_amb_a);
  DDX_Control(pDX, IDC_SPIN_AMB_R, _spin_amb_r);
  DDX_Control(pDX, IDC_SPIN_AMB_G, _spin_amb_g);
  DDX_Control(pDX, IDC_SPIN_AMB_B, _spin_amb_b);
  DDX_Control(pDX, IDC_SPIN_AMB_A, _spin_amb_a);
  DDX_Control(pDX, IDC_SLIDER_AMB, _slider_amb);
  DDX_Control(pDX, IDC_B_COL_AMB, _bcol_amb);
  DDX_Check(pDX, IDC_CHECK_LOCK_AMBDIF, _check_lock_ambdif);
  DDX_Control(pDX, IDC_CHECK_LOCK_AMBDIF, _checkbutton_lock_ambdif);
  DDX_Control(pDX, IDC_SPIN_AMB, _spin_amb);
  //diffuse
  DDX_Text(pDX, IDC_EDIT_DIF_R, _diffuse_r);
  DDX_Text(pDX, IDC_EDIT_DIF_G, _diffuse_g);
  DDX_Text(pDX, IDC_EDIT_DIF_B, _diffuse_b);
  DDX_Text(pDX, IDC_EDIT_DIF_A, _diffuse_a);
  DDX_Check(pDX, IDC_CHECK_DIF_R, _check_dif_r);
  DDX_Check(pDX, IDC_CHECK_DIF_G, _check_dif_g);
  DDX_Check(pDX, IDC_CHECK_DIF_B, _check_dif_b);
  DDX_Check(pDX, IDC_CHECK_DIF_A, _check_dif_a);
  DDX_Control(pDX, IDC_SPIN_DIF_R, _spin_dif_r);
  DDX_Control(pDX, IDC_SPIN_DIF_G, _spin_dif_g);
  DDX_Control(pDX, IDC_SPIN_DIF_B, _spin_dif_b);
  DDX_Control(pDX, IDC_SPIN_DIF_A, _spin_dif_a);
  DDX_Control(pDX, IDC_SLIDER_DIF, _slider_dif);
  DDX_Control(pDX, IDC_B_COL_DIF, _bcol_dif);
  DDX_Control(pDX, IDC_SPIN_DIF, _spin_amb);
  //forcedDiffuse
  DDX_Text(pDX, IDC_EDIT_FOD_R, _forcedDiffuse_r);
  DDX_Text(pDX, IDC_EDIT_FOD_G, _forcedDiffuse_g);
  DDX_Text(pDX, IDC_EDIT_FOD_B, _forcedDiffuse_b);
  DDX_Text(pDX, IDC_EDIT_FOD_A, _forcedDiffuse_a);
  DDX_Check(pDX, IDC_CHECK_FOD_R, _check_fod_r);
  DDX_Check(pDX, IDC_CHECK_FOD_G, _check_fod_g);
  DDX_Check(pDX, IDC_CHECK_FOD_B, _check_fod_b);
  DDX_Check(pDX, IDC_CHECK_FOD_A, _check_fod_a);
  DDX_Control(pDX, IDC_SPIN_FOD_R, _spin_fod_r);
  DDX_Control(pDX, IDC_SPIN_FOD_G, _spin_fod_g);
  DDX_Control(pDX, IDC_SPIN_FOD_B, _spin_fod_b);
  DDX_Control(pDX, IDC_SPIN_FOD_A, _spin_fod_a);
  DDX_Control(pDX, IDC_SLIDER_FOD, _slider_fod);
  DDX_Control(pDX, IDC_B_COL_FOD, _bcol_fod);
  DDX_Control(pDX, IDC_SPIN_FOD, _spin_amb);
  //emmisive
  DDX_Text(pDX, IDC_EDIT_EMM_R, _emmisive_r);
  DDX_Text(pDX, IDC_EDIT_EMM_G, _emmisive_g);
  DDX_Text(pDX, IDC_EDIT_EMM_B, _emmisive_b);
  DDX_Text(pDX, IDC_EDIT_EMM_A, _emmisive_a);
  DDX_Check(pDX, IDC_CHECK_EMM_R, _check_emm_r);
  DDX_Check(pDX, IDC_CHECK_EMM_G, _check_emm_g);
  DDX_Check(pDX, IDC_CHECK_EMM_B, _check_emm_b);
  DDX_Check(pDX, IDC_CHECK_EMM_A, _check_emm_a);
  DDX_Control(pDX, IDC_SPIN_EMM_R, _spin_emm_r);
  DDX_Control(pDX, IDC_SPIN_EMM_G, _spin_emm_g);
  DDX_Control(pDX, IDC_SPIN_EMM_B, _spin_emm_b);
  DDX_Control(pDX, IDC_SPIN_EMM_A, _spin_emm_a);
  DDX_Control(pDX, IDC_SLIDER_EMM, _slider_emm);
  DDX_Control(pDX, IDC_B_COL_EMM, _bcol_emm);
  DDX_Control(pDX, IDC_SPIN_EMM, _spin_amb);
  //specular
  DDX_Text(pDX, IDC_EDIT_SPC_R, _specular_r);
  DDX_Text(pDX, IDC_EDIT_SPC_G, _specular_g);
  DDX_Text(pDX, IDC_EDIT_SPC_B, _specular_b);
  DDX_Text(pDX, IDC_EDIT_SPC_A, _specular_a);
  DDX_Check(pDX, IDC_CHECK_SPC_R, _check_spc_r);
  DDX_Check(pDX, IDC_CHECK_SPC_G, _check_spc_g);
  DDX_Check(pDX, IDC_CHECK_SPC_B, _check_spc_b);
  DDX_Check(pDX, IDC_CHECK_SPC_A, _check_spc_a);
  DDX_Control(pDX, IDC_SPIN_SPC_R, _spin_spc_r);
  DDX_Control(pDX, IDC_SPIN_SPC_G, _spin_spc_g);
  DDX_Control(pDX, IDC_SPIN_SPC_B, _spin_spc_b);
  DDX_Control(pDX, IDC_SPIN_SPC_A, _spin_spc_a);
  DDX_Control(pDX, IDC_SLIDER_SPC, _slider_spc);
  DDX_Control(pDX, IDC_B_COL_SPC, _bcol_spc);
  DDX_Control(pDX, IDC_SPIN_SPC, _spin_amb);
  //specularPower
  DDX_Text(pDX, IDC_EDIT_SPP, _specularPower);
  DDX_Control(pDX, IDC_SPIN_SPP, _spin_spp);
  //PixelShaderID
  DDX_Text(pDX, IDC_COMBO_PSI, _PixelShaderID);
  DDX_Control(pDX, IDC_COMBO_PSI, _combo_PSI);
  //VertexShaderID
  DDX_Text(pDX, IDC_COMBO_VSI, _VertexShaderID);
  DDX_Control(pDX, IDC_COMBO_VSI, _combo_VSI);
  //stage
  DDX_Check(pDX, IDC_CHECK_ENABLE_STAGE, _check_stage);   
  DDX_Text(pDX, IDC_EDIT_STAGE_NAME, _stage_name);
  DDX_Text(pDX, IDC_EDIT_TEX, _texture);
  DDX_Text(pDX, IDC_COMBO_UVS, _uvSource);
  DDX_Control(pDX, IDC_COMBO_UVS, _combo_uvTex);
  //aside
  DDX_Text(pDX, IDC_EDIT_AS0, _aside_0);
  DDX_Text(pDX, IDC_EDIT_AS1, _aside_1);
  DDX_Text(pDX, IDC_EDIT_AS2, _aside_2);
  //up
  DDX_Text(pDX, IDC_EDIT_UP0, _up_0);
  DDX_Text(pDX, IDC_EDIT_UP1, _up_1);
  DDX_Text(pDX, IDC_EDIT_UP2, _up_2);
  //dir
  DDX_Text(pDX, IDC_EDIT_DI0, _dir_0);
  DDX_Text(pDX, IDC_EDIT_DI1, _dir_1);
  DDX_Text(pDX, IDC_EDIT_DI2, _dir_2);
  //pos
  DDX_Text(pDX, IDC_EDIT_PS0, _pos_0);
  DDX_Text(pDX, IDC_EDIT_PS1, _pos_1);
  DDX_Text(pDX, IDC_EDIT_PS2, _pos_2);
  //lod list
  DDX_Control(pDX, IDC_COMBO_LOD, _list_lod);
  //mat list
  DDX_Control(pDX, IDC_LIST_MAT, _list_mat);
  //stages
  DDX_Control(pDX, IDC_TAB_STAGES, _tab_stage);
  //lights from Buldozer
  DDX_Text(pDX, IDC_S_LIGHT_AMB_R, _light_amb_r);
//  DDX_Control(pDX, IDC_S_LIGHT_AMB_R, _label_light_amb_r);
  DDX_Text(pDX, IDC_S_LIGHT_AMB_G, _light_amb_g);
//  DDX_Control(pDX, IDC_S_LIGHT_AMB_G, _label_light_amb_g);
  DDX_Text(pDX, IDC_S_LIGHT_AMB_B, _light_amb_b);
//  DDX_Control(pDX, IDC_S_LIGHT_AMB_B, _label_light_amb_b);
  DDX_Text(pDX, IDC_S_LIGHT_DIF_R, _light_dif_r);
//  DDX_Control(pDX, IDC_S_LIGHT_DIF_R, _label_light_dif_r);
  DDX_Text(pDX, IDC_S_LIGHT_DIF_G, _light_dif_g);
//  DDX_Control(pDX, IDC_S_LIGHT_DIF_G, _label_light_dif_g);
  DDX_Text(pDX, IDC_S_LIGHT_DIF_B, _light_dif_b);
//  DDX_Control(pDX, IDC_S_LIGHT_DIF_B, _label_light_dif_b);
//  DDX_Control(pDX, IDC_S_LIGHT_AMB, _label_light_amb);
//  DDX_Control(pDX, IDC_S_LIGHT_DIF, _label_light_dif);
  DDX_Control(pDX, IDC_B_LIGHT_AMB, _bcol_light_amb);
  DDX_Control(pDX, IDC_B_LIGHT_DIF, _bcol_light_dif);
}


//---------------------------   Private   -----------------------------------------------


//model backup
//create the back-up of model and materials
//called by ResetObj()
void CMatView::BackupObj()
{
  //mats
  SaveMaterial();
  if ( doc->_mats_backup )
    delete doc->_mats_backup;
  doc->_mats_backup= doc->_mats;
  //obj
  if ( doc->_obj_backup )
    delete doc->_obj_backup;
  doc->_obj_backup= doc->_obj;
  if ( doc->_mats_backup && doc->_obj_backup )
    _backup= TRUE;
  else
    _backup= FALSE;
}


//restore the backuped model and materials
void CMatView::RestoreBackupObj()
{
  if ( !_backup || !doc->_obj_backup || !doc->_mats_backup )     //backup is not corect
    return;                                          

  //delete the current model
  if ( doc->_obj )
    delete doc->_obj;
  if ( doc->_mats )
    delete doc->_mats;

  //restore backup model
  doc->_obj= doc->_obj_backup;
  doc->_mats= doc->_mats_backup;
  doc->_mats->SetObj(doc->_obj);

  //restore model in interface
  //musim ulozit vsechny! materialy do tempu (stacilo by teoreticky ulozit
  //jen ty co jsou oznaceny a ten prvni, ten byl zmenen materialem primitiva
  for ( int i= 0; i < doc->_mats->GetNmatAll(); i++ )
  {
    BOOL to_save= doc->_mats->GetMat(i).ToSave();
    doc->_mats->GetMat(i).Changed(TRUE);
    doc->_mats->GetMat(i).ToSave(to_save);
  }
  doc->_mats->SaveTemps();

  //delete backup
  doc->_obj_backup= NULL;
  doc->_mats_backup= NULL;
  _backup= FALSE;
}


//zrusi objekt a seznam materialu a vytvori oboje nove (prazdne)
//vola se pred load
//backup = TRUE - create backup of model
void CMatView::ResetObj(BOOL backup)
{
  //pokud je objekt primitivum, prazdny nebo zadny, backup nesmim provest
  if (  backup  &&  _p3d_type == P3D_NORMAL  )
    BackupObj();

  //mats
  if (  doc->_mats != NULL  )
  {
    //pokud je vytvoren backup, nesmim mazat (backup byl vytvoren pouze prepointrovanim)
    if ( !backup )
      delete doc->_mats;
    doc->_mats= new Materials;
  }
  //obj
  if (  doc->_obj != NULL  )
  {
    //pokud je vytvoren backup, nesmim mazat (backup byl vytvoren pouze prepointrovanim)
    if ( !backup )
      delete doc->_obj;
    doc->_obj= new LODObject;
    doc->_mats->SetObj(doc->_obj);
    _p3d_type= P3D_EMPTY;
  }
  Disable();
}


//delete backup model (obj & mats)
void CMatView::DeleteBackup()
{
  if ( doc->_obj_backup )
  {
    delete doc->_obj_backup;
    doc->_obj_backup= NULL;
  }
  if ( doc->_mats_backup )
  {
    delete doc->_mats_backup;
    doc->_mats_backup= NULL;
  }
  _backup= FALSE;
}


//ask user to load backup mat, if yes than load it
void CMatView::AskToLoadFromBackupMat()
{
  if ( _backup )
  {
//    char str[256];
//    sprintf(str, "Map the selected material: %s\n on the primitive?", (const char*)doc->_mats_backup->GetFirstSelMatCurLod().GetMatName());
//    if ( MessageBox(str, "Material transfer", MB_ICONINFORMATION|MB_YESNO) == IDYES )
//    {
      //load the new material from parameter-material to all selected materials
      LoadMatFromMat(doc->_mats_backup->GetFirstSelMatCurLod());
//    }
  }
}


//save actual material to all selected materials from backup model if user say yes
//destroy the backup model
BOOL CMatView::AskToSaveToBackupMat()
{
  //tento if neni deluzety
  char str[256];
  sprintf(str, "Map the current material: %s\n on the main model?", (const char*)doc->_mats->GetFirstSelMatCurLod().GetMatName() );
  if ( MessageBox(str, "Material transfer", MB_ICONINFORMATION|MB_YESNO) == IDYES )
    return TRUE;
  else
    return FALSE;
}


BOOL CMatView::OpenObj(const char* file_name)
{
  BOOL ret= TRUE;

  WFilePath p(file_name);
  _last_p3d_path= (RString) (p.GetDrive()+p.GetDirectory());
  StatusBar("Loading");
  ResetObj();
  if ( doc->_obj->Load(WFilePath(file_name),NULL,NULL) == 0 )    //nazacatku se provede destrukce stareho
  {
    if ( doc->_mats->Load(doc->_obj, _mat_path) )
    {
      SetListLod();                              //nastavi i seznam materialu
      SetFlags();
      ResetControlsAfterLoad();
      ProtectStages();
      CheckStageEnable();
      SetColors();
			if ( doc->_mats->ExistMat() )
			  Enable();
      _obj_path= file_name;
      SetTitle(_obj_path);
      _p3d_type= P3D_NORMAL;
    }
    else
    {
      LogF("Materials can't load from %s.", file_name);
      MessageBox("Materials can not load from file.", 0, MB_ICONEXCLAMATION | MB_OK);
      ret= FALSE;
    }
    Update();
  }
  else
  {
    LogF("%s: This file can not load.", file_name);
    MessageBox("This file can not load.", 0, MB_ICONEXCLAMATION | MB_OK);
    ret= FALSE;
  }
  SaveCfg();                 
  StatusBar();

  return TRUE;
}


//vypis na status bar
void CMatView::StatusBar(const char* str)
{
  status_bar->SetPaneText(0, str);
}


//nastavi titulek aplikace
void CMatView::SetTitle(const char* title)
{
  char str[1024];
  
  sprintf(str, "Mat:   %s", title);

  ::SetWindowText(GetParentFrame()->m_hWnd, str);
}


//enable or disable controls
void CMatView::EnableControls()
{
  //material list
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_LIST_MAT), _enable);
	//ambient
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AMB_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AMB_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AMB_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AMB_A), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_AMB_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_AMB_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_AMB_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_AMB_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_AMB_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_AMB_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_AMB_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_AMB_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SLIDER_AMB), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_COL_AMB), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_AMB), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_LOCK_AMBDIF), _enable);
  //diffuse
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DIF_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DIF_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DIF_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DIF_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_DIF_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_DIF_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_DIF_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_DIF_A), _enable);
 	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_DIF_R), _enable);
 	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_DIF_G), _enable);
 	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_DIF_B), _enable);
 	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_DIF_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SLIDER_DIF), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_COL_DIF), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_DIF), _enable);
  //forced diffuse
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_FOD_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_FOD_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_FOD_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_FOD_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_FOD_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_FOD_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_FOD_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_FOD_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_FOD_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_FOD_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_FOD_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_FOD_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SLIDER_FOD), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_COL_FOD), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_FOD), _enable);
  //emmisive
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_EMM_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_EMM_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_EMM_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_EMM_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_EMM_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_EMM_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_EMM_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_EMM_A), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_EMM_R), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_EMM_G), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_EMM_B), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_EMM_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SLIDER_EMM), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_COL_EMM), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_EMM), _enable);
  //specular
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_SPC_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_SPC_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_SPC_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_SPC_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_SPC_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_SPC_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_SPC_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_SPC_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_SPC_R), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_SPC_G), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_SPC_B), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_SPC_A), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SLIDER_SPC), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_COL_SPC), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_SPC), _enable);
  //specular power
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_SPP), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_SPIN_SPP), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_SPP), _enable);
  //shaders
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_COMBO_PSI), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_COMBO_VSI), _enable);
	//stage
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_ENABLE_STAGE), _enable);  
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_DELETE_STAGE), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_STAGE_NAME), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_TEX), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_BROWS_TEXTURE), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_VIEW_TEXTURE), _enable);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_EXPLORE_TEXTURE), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_COMBO_UVS), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AS0), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AS1), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AS2), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_UP0), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_UP1), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_UP2), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DI0), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DI1), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DI2), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_PS0), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_PS1), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_PS2), _enable);
  //lights
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_LIGHT_AMB), _enable);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_LIGHT_DIF), _enable);
  //other
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_DIS_UNS_MAT), _enable);
}


//enable all controls
void CMatView::Enable()
{
  if ( !_enable )
  {
	  _enable= TRUE;
    EnableControls();
  }
}


//disable all controls
void CMatView::Disable()
{
  if ( _enable )
  {
    _enable= FALSE;
    EnableControls();
  }
}


//zapne vse ve stage
void CMatView::EnableStage()
{
  //no material
  if ( !doc->_mats->ExistMatCurLod() )
    return;
	//stage
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_TAB_STAGES), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_ENABLE_STAGE), TRUE);    
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_DELETE_STAGE), TRUE);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_STAGE_NAME), TRUE);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_TEX), TRUE);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_BROWS_TEXTURE), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_VIEW_TEXTURE), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_EXPLORE_TEXTURE), TRUE);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_COMBO_UVS), TRUE);
  EnableUVTransform();
}


//vypne vse ve stage
void CMatView::DisableStage()
{
	//stage
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_TAB_STAGES), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_ENABLE_STAGE), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_DELETE_STAGE), FALSE);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_STAGE_NAME), FALSE);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_TEX), FALSE);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_BROWS_TEXTURE), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_VIEW_TEXTURE), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_EXPLORE_TEXTURE), FALSE);
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_COMBO_UVS), FALSE);
  DisableUVTransform();
}


void CMatView::EnableUVTransform()
{
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AS0), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AS1), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AS2), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_UP0), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_UP1), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_UP2), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DI0), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DI1), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DI2), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_PS0), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_PS1), TRUE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_PS2), TRUE);
}


void CMatView::DisableUVTransform()
{
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AS0), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AS1), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_AS2), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_UP0), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_UP1), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_UP2), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DI0), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DI1), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_DI2), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_PS0), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_PS1), FALSE);
  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_PS2), FALSE);
}


void CMatView::ProtectStages()
{
  UpdateData(TRUE);

  for ( int i= 0; i < doc->_mats->GetNmatAll(); i++ )
  {
    int ind_cfg_stage= InCfgStage(doc->_mats->GetMat(i).GetPixelShaderID());
    if ( ind_cfg_stage > -1 )
      doc->_mats->GetMat(i).SetMat(_cfg_stage_file[ind_cfg_stage]);
    else
      doc->_mats->GetMat(i).ProtectStages(FALSE);
  }
}


//---------------------------   Inicialization   -------------------------------------------


//init spins
void CMatView::InitControls()
{
  //spins
	UDACCEL u[2];
  UDACCEL v[1];
	u[0].nInc= 1;
	u[0].nSec= 0;
	u[1].nInc= 5;
	u[1].nSec= 3;
  v[0].nInc= 1;
  v[0].nSec= 0;
  //ambient
	_spin_amb_r.SetRange(0,100);
	_spin_amb_r.SetAccel(2, u);
	_spin_amb_r.SetPos(100*AMBIENT);
	_ambient_r= AMBIENT;
	_spin_amb_g.SetRange(0,100);
	_spin_amb_g.SetAccel(2, u);
	_spin_amb_g.SetPos(100*AMBIENT);
	_ambient_g= AMBIENT;
	_spin_amb_b.SetRange(0,100);
	_spin_amb_b.SetAccel(2, u);
	_spin_amb_b.SetPos(100*AMBIENT);
	_ambient_b= AMBIENT;
	_spin_amb_a.SetRange(0,100);
	_spin_amb_a.SetAccel(2, u);
	_spin_amb_a.SetPos(100*AMBIENT);
	_ambient_a= AMBIENT;
	_spin_amb.SetAccel(1, v);              //nutne, aby skakal po 1
  //diffuse
	_spin_dif_r.SetRange(0,100);
	_spin_dif_r.SetAccel(2, u);
	_spin_dif_r.SetPos(100*DIFFUSE);
	_diffuse_r= DIFFUSE;
	_spin_dif_g.SetRange(0,100);
	_spin_dif_g.SetAccel(2, u);
	_spin_dif_g.SetPos(100*DIFFUSE);
	_diffuse_g= DIFFUSE;
	_spin_dif_b.SetRange(0,100);
	_spin_dif_b.SetAccel(2, u);
	_spin_dif_b.SetPos(100*DIFFUSE);
	_diffuse_b= DIFFUSE;
	_spin_dif_a.SetRange(0,100);
	_spin_dif_a.SetAccel(2, u);
	_spin_dif_a.SetPos(100*DIFFUSE);
	_diffuse_a= DIFFUSE;
  //forced diffuse
	_spin_fod_r.SetRange(0,100);
	_spin_fod_r.SetAccel(2, u);
	_spin_fod_r.SetPos(100*FORCEDDIFFUSE);
	_forcedDiffuse_r= FORCEDDIFFUSE;
	_spin_fod_g.SetRange(0,100);
	_spin_fod_g.SetAccel(2, u);
	_spin_fod_g.SetPos(100*FORCEDDIFFUSE);
	_forcedDiffuse_g= FORCEDDIFFUSE;
	_spin_fod_b.SetRange(0,100);
	_spin_fod_b.SetAccel(2, u);
	_spin_fod_b.SetPos(100*FORCEDDIFFUSE);
	_forcedDiffuse_b= FORCEDDIFFUSE;
	_spin_fod_a.SetRange(0,100);
	_spin_fod_a.SetAccel(2, u);
	_spin_fod_a.SetPos(100*FORCEDDIFFUSE);
	_forcedDiffuse_a= FORCEDDIFFUSE;
  //emmisive
  _spin_emm_r.SetRange(0,100);
	_spin_emm_r.SetAccel(2, u);
	_spin_emm_r.SetPos(100*EMMISIVE);
	_emmisive_r= EMMISIVE;
  _spin_emm_g.SetRange(0,100);
	_spin_emm_g.SetAccel(2, u);
	_spin_emm_g.SetPos(100*EMMISIVE);
	_emmisive_g= EMMISIVE;
  _spin_emm_b.SetRange(0,100);
	_spin_emm_b.SetAccel(2, u);
	_spin_emm_b.SetPos(100*EMMISIVE);
	_emmisive_b= EMMISIVE;
  _spin_emm_a.SetRange(0,100);
	_spin_emm_a.SetAccel(2, u);
	_spin_emm_a.SetPos(100*EMMISIVE);
	_emmisive_a= EMMISIVE;
  //specular
  _spin_spc_r.SetRange(0,100);
	_spin_spc_r.SetAccel(2, u);
	_spin_spc_r.SetPos(100*SPECULAR);
	_specular_r= SPECULAR;
  _spin_spc_g.SetRange(0,100);
	_spin_spc_g.SetAccel(2, u);
	_spin_spc_g.SetPos(100*SPECULAR);
	_specular_g= SPECULAR;
  _spin_spc_b.SetRange(0,100);
	_spin_spc_b.SetAccel(2, u);
	_spin_spc_b.SetPos(100*SPECULAR);
	_specular_b= SPECULAR;
  _spin_spc_a.SetRange(0,100);
	_spin_spc_a.SetAccel(2, u);
	_spin_spc_a.SetPos(100*SPECULAR);
	_specular_a= SPECULAR;
  //specular power
	UDACCEL w[2];
	w[0].nInc= 100;
	w[0].nSec= 0;
  w[1].nInc= 500;
  w[1].nSec= 2;
  _spin_spp.SetRange(0, UD_MAXVAL);
	_spin_spp.SetAccel(2, w);
	_spin_spp.SetPos(100*SPECULARPOWER);
	_specularPower= SPECULARPOWER;

  //sliders
  _slider_amb.SetRange(0, 100);
  _slider_amb.SetPos(100);
  //_slider_amb.SetTicFreq(20);   //nastavi carkovani, musi byt zapnute 'Tick Marks'
  _slider_dif.SetRange(0, 100);
  _slider_dif.SetPos(100);
  _slider_fod.SetRange(0, 100);
  _slider_fod.SetPos(0);
  _slider_emm.SetRange(0, 100);
  _slider_emm.SetPos(0);
  _slider_spc.SetRange(0, 100);
  _slider_spc.SetPos(0);

  //buttons
  ((CButton *)GetDlgItem(IDC_B_EXPLORE_TEXTURE))->SetIcon(
    (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_ICON_IE),IMAGE_ICON,12,12,LR_DEFAULTCOLOR));
  ((CButton *)GetDlgItem(IDC_B_VIEW_TEXTURE))->SetIcon(
    (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_ICON_TEXVIEW),IMAGE_ICON,12,12,LR_DEFAULTCOLOR));
  //lock amb&dif
  ((CButton *)GetDlgItem(IDC_CHECK_LOCK_AMBDIF))->SetIcon(
    (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_ICON_LOCK),IMAGE_ICON,20,20,LR_DEFAULTCOLOR));
}


//reset controls after load - checks, sliders - controls independent on loaded variables
void CMatView::ResetControlsAfterLoad()
{
  //checks
  _check_amb_r= _check_amb_g= _check_amb_b= TRUE;
  _check_amb_a= FALSE;
  _check_dif_r= _check_dif_g= _check_dif_b= TRUE;
  _check_dif_a= FALSE;
  _check_fod_r= _check_fod_g= _check_fod_b= TRUE;
  _check_fod_a= FALSE;
  _check_emm_r= _check_emm_g= _check_emm_b= TRUE;
  _check_emm_a= FALSE;
  _check_spc_r= _check_spc_g= _check_spc_b= TRUE;
  _check_spc_a= FALSE;
  _check_lock_ambdif= FALSE;
  //sliders
  _slider_amb.SetPos(100);
  _slider_dif.SetPos(100);
  _slider_fod.SetPos(0);
  _slider_emm.SetPos(0);
  _slider_spc.SetPos(0);
  //stage
  _check_stage= TRUE;

  UpdateData(FALSE);
}


//set all variables to default values
void CMatView::ResetValues()
{
  //ambient
  _ambient_r= _ambient_g= _ambient_b= _ambient_a= 1;
  _check_amb_r= _check_amb_g= _check_amb_b= TRUE;
  _check_amb_a= FALSE;
  _check_lock_ambdif= FALSE;
  //diffuse
  _diffuse_r= _diffuse_g= _diffuse_b= _diffuse_a= 1;
  _check_dif_r= _check_dif_g= _check_dif_b= TRUE;
  _check_dif_a= FALSE;
  //forcedDiffuse
  _forcedDiffuse_r= _forcedDiffuse_g= _forcedDiffuse_b= _forcedDiffuse_a= 0;
  _check_fod_r= _check_fod_g= _check_fod_b= TRUE;
  _check_fod_a= FALSE;
  //emmisive
  _emmisive_r= _emmisive_g= _emmisive_b= _emmisive_a= 0;
  _check_emm_r= _check_emm_g= _check_emm_b= TRUE;
  _check_emm_a= FALSE;
   //specular
  _specular_r= _specular_g= _specular_b= _specular_a= 0;
  _check_spc_r= _check_spc_g= _check_spc_b= TRUE;
  _check_spc_a= FALSE;
  //specularPower
  _specularPower= 0;
  //PixelShaderID
  _PixelShaderID= "";
  //VertexShaderID
  _VertexShaderID= "";
  //stage
  _check_stage= TRUE;
  _texture= "";
  _uvSource= "";
  //aside
  _aside_0= 1;
  _aside_1= 0;
  _aside_2= 0;
  //up
  _up_0= 0;
  _up_1= 1;
  _up_2= 0;
  //dir
  _dir_0= 0;
  _dir_1= 0;
  _dir_2= 1;
  //pos
  _pos_0= _pos_1= _pos_2= 0;
}


//set the current row in the material list
//u zmenenych materialu vypisuje na zacatku '*'
//u RO materialu vypise '!'
//u check-outnutych materialu vypise 'v'
void CMatView::SetFlags(BOOL fast)
{
  if  ( !doc->_mats->GetNmatCurLod() )
    return;

  int n= _list_mat.GetSelCount();
  RString flag;
  if ( !fast )
  {
    SetROFlags();
    SetSSFlags();
  }
  //get lenght of list box
  RECT rect;
  _list_mat.GetWindowRect(&rect);
  int len= 0.175 * (rect.right-rect.left);

  _list_mat.GetSelItems(MAX_MAT, _sel);
  for ( int i= 0; i<_list_mat.GetCount(); i++ )
  {
    _list_mat.DeleteString(i);
    //checkout flag
    if ( doc->_mats->GetMatCurLod(i).SSCheckedOut() )
      flag= FL_CHECKOUT;
    else
      flag= FL_NCHECKOUT;
    //ro flag
    if ( doc->_mats->GetMatCurLod(i).RO() )
      flag= flag+FL_RO;
    else
      flag= flag+FL_NRO;
    //save flag
    if ( doc->_mats->GetMatCurLod(i).ToSave()  &&  _p3d_type != P3D_PRIMITIVE )
      flag= flag+FL_SAVE;   //"!* "
    else
      flag= flag+FL_NSAVE;    //"!   "
    //add
    _list_mat.InsertString(i, (const char*)(flag + ClipText(doc->_mats->GetMatCurLod(i).GetMatName(), len/*LIST_LEN*/)));
    //77 je napevno, casem zavedu scalovani
    _list_mat.SetCurSel(i);
  }
  for ( int i= 0; i < n; i++ )
    _list_mat.SetSel(_sel[i]);
}


//set the list '_list_mat'
void CMatView::SetListMat(const int sel)
{
  //add materials to list
  _list_mat.ResetContent();
  for ( int j= 0; j<doc->_mats->GetNmatCurLod(); j++ )
  {
    //_list_mat.InsertString(-1,(const char*)("    " + doc->_mats->GetMatCurLod(j).GetMatName()));
    RString flag= FL_NRO;
    flag= flag+FL_NSAVE;
    _list_mat.InsertString(-1,(const char*)(flag + doc->_mats->GetMatCurLod(j).GetMatName()));
  }
  SetFlags();
  //choose the first material from the list
  if ( _list_mat.GetCount() )
  {
    _list_mat.SetSel(sel);
    SetStages();
    LoadMaterial();
    doc->_mats->SetSelMatCurLod(sel);
    if ( _disable_unselect_mats )
      doc->_mats->EnableSelMat();
    doc->_mats->SaveTemps();
    Enable();
  }
  else
  {
    ResetValues();
    Disable();
    UpdateData(FALSE);           //from variables to interface
		doc->_mats->EnableAllMat();
		doc->_mats->SaveTemps();
  }
  SetEditChangeMat();
}


//set the edit control according to selected materials in the _list
void CMatView::SetEditChangeMat()
{
  int n_sel= doc->_mats->GetNselmat();

  if ( !n_sel )
    _edit_chmat= "";
  else
  {
    if ( n_sel == 1 )
    {
      _edit_chmat= doc->_mats->GetFirstSelMatCurLod().GetMatName();    
    }
    else                   //n_sel > 1
    {
      _edit_chmat= NO_STR;
    }
  }
  UpdateData(FALSE);        //from variables to controls
}


//set RO flag to each material
void CMatView::SetROFlags()
{
  for ( int i= 0; i < doc->_mats->GetNmatAll(); i++ )
  {
    if ( _access(doc->_mats->GetMat(i).GetFileName(), 02) )   //_acces return 0 if file si writeable
      doc->_mats->GetMat(i).RO(TRUE);
    else 
      doc->_mats->GetMat(i).RO(FALSE);
  }
}


void CMatView::SetSSFlags()
{
  if ( !CheckOpenSS() )
    return;

  for ( int i= 0; i < doc->_mats->GetNmatAll(); i++ )
  {
    SCCSTAT status= SccFcs.Status(doc->_mats->GetMat(i).GetFileName());
    //SS controlled
    if ( status & SCC_STATUS_CONTROLLED )
      doc->_mats->GetMat(i).SSControlled(TRUE);
    else
      doc->_mats->GetMat(i).SSControlled(FALSE);
    //SS deleted
    if ( status & SCC_STATUS_DELETED )
      doc->_mats->GetMat(i).SSDeleted(TRUE);
    else
      doc->_mats->GetMat(i).SSDeleted(FALSE);
    //SS checked out
    if ( status & SCC_STATUS_OUTBYUSER )
      doc->_mats->GetMat(i).SSCheckedOut(TRUE);
    else
      doc->_mats->GetMat(i).SSCheckedOut(FALSE);
  }
}

//load the new material from file to materials
void CMatView::LoadMatFromFile(RString mat_name)
{
  int n_sel= doc->_mats->GetNselmat();
  const int* sel= doc->_mats->GetSelMatCurLod();

  for ( int i= 0; i < n_sel; i++ )
  {
    doc->_mats->GetMat(sel[i]).Load(_mat_path+mat_name);
    doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  LoadMaterial();                     //z dat do promenych prostredi
  ProtectStages();
  CheckStageEnable();
  SetColors();
  UpdateData(FALSE);                  //z promenych prostredi do controlu
}


//load the new material from material 'm' to all selected materials
void CMatView::LoadMatFromMat(Material& m)
{
  int n_sel= doc->_mats->GetNselmat();
  const int* sel= doc->_mats->GetSelMatCurLod();

  for ( int i= 0; i < n_sel; i++ )
  {
    doc->_mats->GetMat(sel[i])= m;// .Load(_mat_path+mat_name);
    doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  SetListLod();
  SetFlags();
  ResetControlsAfterLoad();
  ProtectStages();
  CheckStageEnable();
  UpdateData(FALSE);                  //z promenych prostredi do controlu
}


//disable or enable unselected materials according to _disable_unselect_mats
void CMatView::DisableUnselectedMaterials()
{
  StatusBar("Updating");
  if ( _disable_unselect_mats )
    doc->_mats->EnableSelMat();
  else
    doc->_mats->EnableAllMat();

  Update();

  SaveCfg();
  StatusBar();
}


void CMatView::SelectRowLod(int sel)
{
  doc->_mats->SetCurLod(sel);
}


//set 
//sel_mat - the row in the list of materials
void CMatView::SetListLod(const int sel_mat)
{
  char str[32];

  _list_lod.ResetContent();
  for ( int i= 0; i<doc->_mats->GetNlod(); i++ )
  {
    float lod_resol= doc->_mats->GetNameLod(i);

    if ( lod_resol < 999 )
      sprintf(str, "%.3f", lod_resol);
    else if (  lod_resol > 999  &&  lod_resol < 1001  )
      strcpy(str, SL_GUNNER);
    else if (  lod_resol > 1099  &&  lod_resol < 1101  )
      strcpy(str, SL_PILOT);
    else if (  lod_resol > 1199  &&  lod_resol < 1201  )
      strcpy(str, SL_CARGO);
    else 
      strcpy(str, SL_UNKNOWN);

    _list_lod.InsertString(_list_lod.GetCount(), str);
  }
  _list_lod.SetCurSel(0);
  SelectRowLod(0);
  SetListMat(sel_mat);
}


void CMatView::CheckUVTransformEnable()
{
  if ( _check_stage )
  {
    if ( doc->_mats->GetNselmat() )
    {
      const int* sel= doc->_mats->GetSelMatCurLod();
      BOOL uv_none= TRUE;
      for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
      {
        if ( !doc->_mats->GetMat(sel[i]).GetCurStage().UVNone() )
        {
          uv_none= FALSE;
          break;
        }
      }
      if  ( !uv_none )
        EnableUVTransform();
      else
        DisableUVTransform();
    }
    else 
      DisableUVTransform();
  }
  else 
    DisableUVTransform();
}


//provede spravne enable nebo disable na stage
void CMatView::CheckStageEnable()
{
  UpdateData(TRUE);
  if ( _check_stage )                             //stage is enabled
  {
    EnableStage();
    if ( doc->_mats->GetNselmat() > 1 )               //je oznaceno vice materialu
    {
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_TAB_STAGES), FALSE);
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), FALSE);
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_DELETE_STAGE), FALSE);
    }
    else                                             //je oznacen jen jeden material
    {
      if ( _tab_stage.GetItemCount() == MAX_STAGES )
        ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), FALSE);
    }
  }
  else                                             //stage is disabled
  {
    DisableStage();
    ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_ENABLE_STAGE), TRUE);
    if ( doc->_mats->GetNselmat() == 1 )
    {
      if ( _tab_stage.GetItemCount() < MAX_STAGES )
        ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), TRUE);
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_DELETE_STAGE), TRUE);
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_TAB_STAGES), TRUE);
    }
  }
  
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetCurStage().Protected() )
    {
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), FALSE);
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_DELETE_STAGE), FALSE);
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_CHECK_ENABLE_STAGE), FALSE);
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_EDIT_STAGE_NAME), FALSE);
      break;
    }
  }
 
  CheckUVTransformEnable();
}


//vytvori stage
//zodpovida za to, zda jak bude nastavena stage a taky zda bude disable nebo enable
void CMatView::SetStages()
{
  GetDlgItem(IDC_EDIT_TEX)->SendMessage(EM_LINESCROLL, 100, 0);

  const int* sel= doc->_mats->GetSelMatCurLod();
  int n= doc->_mats->GetNselmat();
  BOOL disable= FALSE;

  for ( int i= 0; i < n; i++ )
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage() )
    {
      disable= TRUE;
      break;
    }
  }
  if ( disable )
  {
    DisableStage();
    return;
  }
  if ( n == 1 )             //oznacen je pouze jeden material
  {
    int Nstage= doc->_mats->GetFirstSelMatCurLod().GetNstage();
    _tab_stage.DeleteAllItems();
    if ( !Nstage )
    {
      DisableStage();
      return;
    }
    for ( int i= 0; i < Nstage; i++ )
      _tab_stage.InsertItem(i, doc->_mats->GetFirstSelMatCurLod().GetStage(i).GetName());
    _tab_stage.SetCurSel(doc->_mats->GetFirstSelMatCurLod().GetIndCurStage());
  }
  else                    //oznaceno je vice materialu
  {
    _stage_name= doc->_mats->GetMat(sel[0]).GetCurStage().GetName();
    for ( int i= 1; i < n; i++ )  
    {
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetName() != _stage_name )
      {
        _stage_name= NO_STR;
        break;
      }
    }
    _tab_stage.DeleteAllItems();
    _tab_stage.InsertItem(0, (const char*)_stage_name);
    _tab_stage.SetCurSel(0);
  }
  CheckStageEnable();
}


template <class T> int CMatView::Assign(T& dest, T& src)
{
  if ( dest != src )
  {
    dest= src;
    return 1;
  }
  else
    return 0;
}


//load material from doc to view variables
//natahne data z oznacenych materialu do prostredi
void CMatView::LoadMaterial()
{
  int n= doc->_mats->GetNselmat();
  if ( !n )
    return;                                  //neni vybran zadny material
  const const int* sel= doc->_mats->GetSelMatCurLod();

  //vezmu prvni vybrany material
  _ambient_r= doc->_mats->GetMat(sel[0]).GetAmbient()._r;
  _ambient_g= doc->_mats->GetMat(sel[0]).GetAmbient()._g;
  _ambient_b= doc->_mats->GetMat(sel[0]).GetAmbient()._b;
  _ambient_a= doc->_mats->GetMat(sel[0]).GetAmbient()._a;
  _diffuse_r= doc->_mats->GetMat(sel[0]).GetDiffuse()._r;
  _diffuse_g= doc->_mats->GetMat(sel[0]).GetDiffuse()._g;
  _diffuse_b= doc->_mats->GetMat(sel[0]).GetDiffuse()._b;
  _diffuse_a= doc->_mats->GetMat(sel[0]).GetDiffuse()._a;
  _forcedDiffuse_r= doc->_mats->GetMat(sel[0]).GetForcedDiffuse()._r;
  _forcedDiffuse_g= doc->_mats->GetMat(sel[0]).GetForcedDiffuse()._g;
  _forcedDiffuse_b= doc->_mats->GetMat(sel[0]).GetForcedDiffuse()._b;
  _forcedDiffuse_a= doc->_mats->GetMat(sel[0]).GetForcedDiffuse()._a;
  _emmisive_r= doc->_mats->GetMat(sel[0]).GetEmmisive()._r;
  _emmisive_g= doc->_mats->GetMat(sel[0]).GetEmmisive()._g;
  _emmisive_b= doc->_mats->GetMat(sel[0]).GetEmmisive()._b;
  _emmisive_a= doc->_mats->GetMat(sel[0]).GetEmmisive()._a;
  _specular_r= doc->_mats->GetMat(sel[0]).GetSpecular()._r;
  _specular_g= doc->_mats->GetMat(sel[0]).GetSpecular()._g;
  _specular_b= doc->_mats->GetMat(sel[0]).GetSpecular()._b;
  _specular_a= doc->_mats->GetMat(sel[0]).GetSpecular()._a;
  _specularPower= doc->_mats->GetMat(sel[0]).GetSpecularPower();
  _PixelShaderID= doc->_mats->GetMat(sel[0]).GetPixelShaderID();
  _VertexShaderID= doc->_mats->GetMat(sel[0]).GetVertexShaderID();

  //najdu 1. material, ktery ma aspon jednu stage
  BOOL mat_with_stage= FALSE;
  for ( int i= 0; i < n; i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
    {
      _check_stage= doc->_mats->GetMat(sel[i]).GetCurStage().Enable();
      _stage_name= doc->_mats->GetMat(sel[i]).GetCurStage().GetName();
      _texture= doc->_mats->GetMat(sel[i]).GetCurStage().GetTexture();
      _uvSource= doc->_mats->GetMat(sel[i]).GetCurStage().GetUVSource();
      _aside_0= doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[0];
      _aside_1= doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[1];
      _aside_2= doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[2];
      _up_0= doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[0];
      _up_1= doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[1];
      _up_2= doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[2];
      _dir_0= doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[0];
      _dir_1= doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[1];
      _dir_2= doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[2];
      _pos_0= doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[0];
      _pos_1= doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[1];
      _pos_2= doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[2];
      mat_with_stage= TRUE;
      break;
    }
  }

  //nejsou vybrany zadne dalsi materialy, nastavim spiny
  if ( n == 1 )
  {
    _spin_amb_r.SetPos32((int)(100*_ambient_r+0.5));
    _spin_amb_g.SetPos32((int)(100*_ambient_g+0.5));
    _spin_amb_b.SetPos32((int)(100*_ambient_b+0.5));
    _spin_amb_a.SetPos32((int)(100*_ambient_a+0.5));
    _spin_dif_r.SetPos32((int)(100*_diffuse_r+0.5));
    _spin_dif_g.SetPos32((int)(100*_diffuse_g+0.5));
    _spin_dif_b.SetPos32((int)(100*_diffuse_b+0.5));
    _spin_dif_a.SetPos32((int)(100*_diffuse_a+0.5));
    _spin_fod_r.SetPos32((int)(100*_forcedDiffuse_r+0.5));
    _spin_fod_g.SetPos32((int)(100*_forcedDiffuse_g+0.5));
    _spin_fod_b.SetPos32((int)(100*_forcedDiffuse_b+0.5));
    _spin_fod_a.SetPos32((int)(100*_forcedDiffuse_a+0.5));
    _spin_emm_r.SetPos32((int)(100*_emmisive_r+0.5));
    _spin_emm_g.SetPos32((int)(100*_emmisive_g+0.5));
    _spin_emm_b.SetPos32((int)(100*_emmisive_b+0.5));
    _spin_emm_a.SetPos32((int)(100*_emmisive_a+0.5));
    _spin_spc_r.SetPos32((int)(100*_specular_r+0.5));
    _spin_spc_g.SetPos32((int)(100*_specular_g+0.5));
    _spin_spc_b.SetPos32((int)(100*_specular_b+0.5));
    _spin_spc_a.SetPos32((int)(100*_specular_a+0.5));
    _spin_spp.SetPos32((int)(100*_specularPower+0.5));
    UpdateData(FALSE);           //from variables to interface
    return;
  }

  int i;
  //vezmu dalsi vybrane materialy
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetAmbient()._r != _ambient_r )
    {
      _ambient_r= NO_VAL;              //vsechny vybrane materialy nemaji stejnou hodnotu
      _spin_amb_r.SetPos32(1);
      break;
    }
  if ( _ambient_r != NO_VAL )
    _spin_amb_r.SetPos32((int)(100*_ambient_r));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetAmbient()._g != _ambient_g )
    {
      _ambient_g= NO_VAL;
      _spin_amb_g.SetPos32(1);
      break;
    }
  if ( _ambient_g != NO_VAL )
    _spin_amb_g.SetPos32((int)(100*_ambient_g));

  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetAmbient()._b != _ambient_b )
    {
      _ambient_b= NO_VAL;
      _spin_amb_b.SetPos32(1);
      break;
    }
  if ( _ambient_b != NO_VAL )
    _spin_amb_b.SetPos32((int)(100*_ambient_b));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetAmbient()._a != _ambient_a )
    {
      _ambient_a= NO_VAL;
      _spin_amb_a.SetPos32(1);
      break;
    }
  if ( _ambient_a != NO_VAL )
    _spin_amb_a.SetPos32((int)(100*_ambient_a));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._r != _diffuse_r )
    {
      _diffuse_r= NO_VAL;
      _spin_dif_r.SetPos32(1);
      break;
    }
  if ( _diffuse_r != NO_VAL )
    _spin_dif_r.SetPos32((int)(100*_diffuse_r));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._g != _diffuse_g )
    {
      _diffuse_g= NO_VAL;
      _spin_dif_g.SetPos32(1);
      break;
    }
  if ( _diffuse_g != NO_VAL )
    _spin_dif_g.SetPos32((int)(100*_diffuse_g));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._b != _diffuse_b )
    {
      _diffuse_b= NO_VAL;
      _spin_dif_b.SetPos32(1);
      break;
    }
  if ( _diffuse_b != NO_VAL )
    _spin_dif_b.SetPos32((int)(100*_diffuse_b));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._a != _diffuse_a )
    {
      _diffuse_a= NO_VAL;
      _spin_dif_a.SetPos32(1);
      break;
    }
  if ( _diffuse_a != NO_VAL )
    _spin_dif_a.SetPos32((int)(100*_diffuse_a));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r != _forcedDiffuse_r )
    {
      _forcedDiffuse_r= NO_VAL;
      _spin_fod_r.SetPos32(0);
      break;
    }
  if ( _forcedDiffuse_r != NO_VAL )
    _spin_fod_r.SetPos32((int)(100*_forcedDiffuse_r));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g != _forcedDiffuse_g )
    {
      _forcedDiffuse_g= NO_VAL;
      _spin_fod_g.SetPos32(0);
      break;
    }
  if ( _forcedDiffuse_g != NO_VAL )
    _spin_fod_g.SetPos32((int)(100*_forcedDiffuse_g));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b != _forcedDiffuse_b )
    {
      _forcedDiffuse_b= NO_VAL;
      _spin_fod_b.SetPos32(0);
      break;
    }
  if ( _forcedDiffuse_b != NO_VAL )
    _spin_fod_b.SetPos32((int)(100*_forcedDiffuse_b));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._a != _forcedDiffuse_a )
    {
      _forcedDiffuse_a= NO_VAL;
      _spin_fod_a.SetPos32(0);
      break;
    }
  if ( _forcedDiffuse_a != NO_VAL )
    _spin_fod_a.SetPos32((int)(100*_forcedDiffuse_a));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._r != _emmisive_r )
    {
      _emmisive_r= NO_VAL;
      _spin_emm_r.SetPos32(0);
      break;
    }
  if ( _emmisive_r != NO_VAL )
    _spin_emm_r.SetPos32((int)(100*_emmisive_r));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._g != _emmisive_g )
    {
      _emmisive_g= NO_VAL;
      _spin_emm_g.SetPos32(0);
      break;
    }
  if ( _emmisive_g != NO_VAL )
    _spin_emm_g.SetPos32((int)(100*_emmisive_g));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._b != _emmisive_b )
    {
      _emmisive_b= NO_VAL;
      _spin_emm_b.SetPos32(0);
      break;
    }
  if ( _emmisive_b != NO_VAL )
    _spin_emm_b.SetPos32((int)(100*_emmisive_b));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._a != _emmisive_a )
    {
      _emmisive_a= NO_VAL;
      _spin_emm_a.SetPos32(0);
      break;
    }
  if ( _emmisive_a != NO_VAL )
    _spin_emm_a.SetPos32((int)(100*_emmisive_a));
  //specular
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetSpecular()._r != _specular_r )
    {
      _specular_r= NO_VAL;
      _spin_spc_r.SetPos32(0);
      break;
    }
  if ( _specular_r != NO_VAL )
    _spin_spc_r.SetPos32((int)(100*_specular_r));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetSpecular()._g != _specular_g )
    {
      _specular_g= NO_VAL;
      _spin_spc_g.SetPos32(0);
      break;
    }
  if ( _specular_g != NO_VAL )
    _spin_spc_g.SetPos32((int)(100*_specular_g));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetSpecular()._b != _specular_b )
    {
      _specular_b= NO_VAL;
      _spin_spc_b.SetPos32(0);
      break;
    }
  if ( _specular_b != NO_VAL )
    _spin_spc_b.SetPos32((int)(100*_specular_b));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetSpecular()._a != _specular_a )
    {
      _specular_a= NO_VAL;
      _spin_spc_a.SetPos32(0);
      break;
    }
  if ( _specular_a != NO_VAL )
    _spin_spc_a.SetPos32((int)(100*_specular_a));
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetSpecularPower() != _specularPower )
    {
      _specularPower= NO_VAL;
      _spin_spc_a.SetPos32(0);
      break;
    }
  if ( _specularPower != NO_VAL )
    _spin_spp.SetPos32((int)(100*_specularPower));
  //shaders
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetPixelShaderID() != _PixelShaderID )
    {
      _PixelShaderID= NO_STR;
      break;
    }
  for ( i= 1; i < n; i++ )  
    if ( doc->_mats->GetMat(sel[i]).GetVertexShaderID() != _VertexShaderID )
    {
      _VertexShaderID= NO_STR;
      break;
    }

  //zadny material nema stage (promene jsou nastaveny postaru)
  if ( !mat_with_stage )
  {
    UpdateData(FALSE);           //from variables to interface
    return;
  }

  //stages
  for ( i= 0; i < n; i++ ) 
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  || 
         doc->_mats->GetMat(sel[i]).GetCurStage().Enable() != _check_stage )
    {
      _check_stage= FALSE;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetName() != _stage_name )
    {
      _stage_name= NO_STR;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
		if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetTexture() != _texture )
    {
      _texture= NO_STR;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetUVSource() != _uvSource )
    {
      _uvSource= NO_STR;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[0] != _aside_0 )
    {
      _aside_0= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[1] != _aside_1 )
    {
      _aside_1= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[2] != _aside_2 )
    {
      _aside_2= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[0] != _up_0 )
    {
      _up_0= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[1] != _up_1 )
    {
      _up_1= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[2] != _up_2 )
    {
      _up_2= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[0] != _dir_0 )
    {
      _dir_0= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[1] != _dir_1 )
    {
      _dir_1= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[2] != _dir_2 )
    {
      _dir_2= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[0] != _pos_0 )
    {
      _pos_0= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[1] != _pos_1 )
    {
      _pos_1= NO_VAL;
      break;
    }
  }
  for ( i= 0; i < n; i++ )  
  {
    if ( !doc->_mats->GetMat(sel[i]).GetNstage()  ||  
         doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[2] != _pos_2 )
    {
      _pos_2= NO_VAL;
      break;
    }
  }
  UpdateData(FALSE);           //from variables to interface
}


//save material from view to doc variables
void CMatView::SaveMaterial()
{
  int n= doc->_mats->GetNselmat();        //pocet oznacenych materialu v aktualnim lodu
  if ( !n )
    return;
  const const int* sel= doc->_mats->GetSelMatCurLod();
  int i;

  for ( i= 0; i < n; i++ )
  {
    if ( Assign(doc->_mats->GetMat(sel[i]).GetAmbient()._r, _ambient_r) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetAmbient()._g, _ambient_g) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetAmbient()._b, _ambient_b) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetAmbient()._a, _ambient_a) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    //diffuse
    if ( Assign(doc->_mats->GetMat(sel[i]).GetDiffuse()._r, _diffuse_r) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetDiffuse()._g, _diffuse_g) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetDiffuse()._b, _diffuse_b) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetDiffuse()._a, _diffuse_a) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    //forcedDiffuse
    if ( Assign(doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r, _forcedDiffuse_r) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g, _forcedDiffuse_g) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b, _forcedDiffuse_b) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._a, _forcedDiffuse_a) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    //emmisive
    if ( Assign(doc->_mats->GetMat(sel[i]).GetEmmisive()._r, _emmisive_r) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetEmmisive()._g, _emmisive_g) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetEmmisive()._b, _emmisive_b) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetEmmisive()._a, _emmisive_a) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    //specular
    if ( Assign(doc->_mats->GetMat(sel[i]).GetSpecular()._r, _specular_r) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetSpecular()._g, _specular_g) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetSpecular()._b, _specular_b) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    if ( Assign(doc->_mats->GetMat(sel[i]).GetSpecular()._a, _specular_a) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    //specularPower
    if ( doc->_mats->GetMat(sel[i]).GetSpecularPower() != _specularPower )
    {
      doc->_mats->GetMat(sel[i]).SetSpecularPower(_specularPower);
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    }
    //PixelShaderID
    if ( Assign(doc->_mats->GetMat(sel[i]).GetPixelShaderID(), (RString)_PixelShaderID) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    //VertexShaderID
    if ( Assign(doc->_mats->GetMat(sel[i]).GetVertexShaderID(), (RString)_VertexShaderID) )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }    //for - prochazeni vsech vybranych materialu  

  //stage menim, jen pokud je vybran pouze jeden material
  if ( n == 1 )
  {
    if ( doc->_mats->GetMat(sel[0]).GetNstage() )         //existuje vubec nejaka stage?
    {
      if ( doc->_mats->GetMat(sel[0]).GetCurStage().Enable() != _check_stage )
      {
        doc->_mats->GetMat(sel[0]).GetCurStage().Enable(_check_stage);
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      }
      //stage name
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetName(), (RString)_stage_name) )
      {
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
        doc->_mats->GetMat(sel[0]).ToSave(FALSE);
      }
      //texture
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetTexture(), (RString)_texture) )
      {
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
        doc->_mats->GetMat(sel[0]).ToSave(FALSE);
      }
      //uvSource
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetUVSource(), (RString)_uvSource ) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      //aside
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetAside()[0], _aside_0) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetAside()[1], _aside_1) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetAside()[2], _aside_2) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      //up
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetUp()[0], _up_0) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetUp()[1], _up_1) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetUp()[2], _up_2) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      //dir
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetDir()[0], _dir_0) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetDir()[1], _dir_1) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetDir()[2], _dir_2) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      //pos
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetPos()[0], _pos_0) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetPos()[1], _pos_1) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
      if ( Assign(doc->_mats->GetMat(sel[0]).GetCurStage().GetPos()[2], _pos_2) )
        doc->_mats->GetMat(sel[0]).Changed(TRUE);
    }       //if - exist any stage
  }     //if - pocet vybranych materialu je 1

  doc->_mats->SaveTemps();
}


//zkontroluje soubor pred ulozeni, zda nejsou nahodou RO
void CMatView::CheckFileBeforeSave(Material& m)
{
  ROCheckResult r= TestFileRO(m_hWnd, m.GetFileName());

  if ( r == ROCHK_FileSaveAs )
  {
    TCHAR file_name[256]= {0};
    OPENFILENAME ofd;
    WFilePath path= (const char*) m.GetFileName();
    strcpy(file_name, path.GetFilename());
    ZeroMemory(&ofd, sizeof(OPENFILENAME));
    ofd.lStructSize= sizeof(OPENFILENAME);
    ofd.lpstrFile= file_name;
    ofd.hwndOwner= m_hWnd;
    ofd.nMaxFile= sizeof(file_name);
    ofd.Flags= OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST;
    ofd.lpstrInitialDir= path.GetDirectory();
    ofd.lpstrFilter= "rvmat file (*.rvmat)\0*.rvmat\0";               //filtr
    ofd.lpstrDefExt= "rvmat";
    if ( !GetSaveFileName(&ofd) ) 
      return;
    StatusBar("Saving");
    SaveMaterial();
    RString fn= m.GetFileName();
    m.SetFileName(file_name);
    CheckFileBeforeSave(m);
    m.SetFileName(fn);
    if ( !m.Save(file_name) )
      MessageBox("File can not be saved.", "Save file:", MB_ICONWARNING);
    m.ToSave(FALSE);
    SetFlags();
    StatusBar();
  }
}


//zkontroluje soubory pred ulozeni, zda nejsou nahodou RO
void CMatView::CheckFilesBeforeSave(int flag)
{
  if ( flag == F_ALL )
  {
    for ( int i= 0; i < doc->_mats->GetNmatAll(); i++ )
      if ( doc->_mats->GetMat(i).ToSave() )
        CheckFileBeforeSave(doc->_mats->GetMat(i));
  }
  if ( flag == F_SELECTED )
  {
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
      if ( doc->_mats->GetMat(sel[i]).ToSave() )
        CheckFileBeforeSave(doc->_mats->GetMat(sel[i]));
  }
}


//zatim se tato funkce nikde nevola, neni jasne, co presne ma delat
void CMatView::GUIRefresh()
{
  const int* sel= doc->_mats->GetSelMatCurLod();     
  SetListLod(sel[0]);                                
  SetFlags();
  ResetControlsAfterLoad();
  ProtectStages();
  CheckStageEnable();
  SetColors();
	if ( doc->_mats->ExistMat() )
		Enable();
  SetTitle(_obj_path);
  UpdateData(FALSE);             //nutne
}


//the real update, called by timer
//if you want to start update call doc->_mats->Changed(TRUE) and StartUpdate()
void CMatView::StartUpdate()
{
  if ( _updating )
    return;
  if ( _update_forbiddance )
    return;

  if ( doc->_mats->Changed() )
  {
    doc->_mats->SaveTemps();
    SetFlags(TRUE);
    if ( _run_buld )
    {
      StatusBar("Updating Buldozer (START)");
      UpdateViewer(doc->_obj);
      _updating= TRUE;
      LogF("Update Buldozer: Start");
    }
  }
}


//end of update
void CMatView::EndUpdate()
{
  if ( _update_forbiddance )
    return;

  doc->_mats->Changed(FALSE);
  _updating= FALSE;
  LogF("Update Buldozer: End");
  StatusBar();
}


//---------------------------   Set button color   ------------------------------------------------


void CMatView::SetColorAmb()
{
  int n= doc->_mats->GetNselmat();
  if (  !n  )
  {
    _bcol_amb.ResetColour();
    return;
  }

  const int* sel= doc->_mats->GetSelMatCurLod();
  COLORREF first_col= RGB((int)255*doc->_mats->GetMat(sel[0]).GetAmbient()._r,
                          (int)255*doc->_mats->GetMat(sel[0]).GetAmbient()._g,
                          (int)255*doc->_mats->GetMat(sel[0]).GetAmbient()._b);
  COLORREF col= first_col;
  for ( int i= 1; i < n; i++ )
  {
    col= RGB((int)255*doc->_mats->GetMat(sel[i]).GetAmbient()._r,
             (int)255*doc->_mats->GetMat(sel[i]).GetAmbient()._g,
             (int)255*doc->_mats->GetMat(sel[i]).GetAmbient()._b);
    if ( col != first_col )
    {
      _bcol_amb.ResetColour();
      return;
    }
  }
  _bcol_amb.SetColor(col, col);
}


void CMatView::SetColorDif()
{
  int n= doc->_mats->GetNselmat();
  if (  !n  )
  {
    _bcol_dif.ResetColour();
    return;
  }

  const int* sel= doc->_mats->GetSelMatCurLod();
  COLORREF first_col= RGB((int)255*doc->_mats->GetMat(sel[0]).GetDiffuse()._r,
                          (int)255*doc->_mats->GetMat(sel[0]).GetDiffuse()._g,
                          (int)255*doc->_mats->GetMat(sel[0]).GetDiffuse()._b);
  COLORREF col= first_col;
  for ( int i= 1; i < n; i++ )
  {
    col= RGB((int)255*doc->_mats->GetMat(sel[i]).GetDiffuse()._r,
             (int)255*doc->_mats->GetMat(sel[i]).GetDiffuse()._g,
             (int)255*doc->_mats->GetMat(sel[i]).GetDiffuse()._b);
    if ( col != first_col )
    {
      _bcol_dif.ResetColour();
      return;
    }
  }
  _bcol_dif.SetColor(col, col);
}


void CMatView::SetColorFod()
{
  int n= doc->_mats->GetNselmat();
  if (  !n  )
  {
    _bcol_fod.ResetColour();
    return;
  }

  const int* sel= doc->_mats->GetSelMatCurLod();
  COLORREF first_col= RGB((int)255*doc->_mats->GetMat(sel[0]).GetForcedDiffuse()._r,
                          (int)255*doc->_mats->GetMat(sel[0]).GetForcedDiffuse()._g,
                          (int)255*doc->_mats->GetMat(sel[0]).GetForcedDiffuse()._b);
  COLORREF col= first_col;
  for ( int i= 1; i < n; i++ )
  {
    col= RGB((int)255*doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r,
             (int)255*doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g,
             (int)255*doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b);
    if ( col != first_col )
    {
      _bcol_fod.ResetColour();
      return;
    }
  }
  _bcol_fod.SetColor(col, col);
}


void CMatView::SetColorEmm()
{
  int n= doc->_mats->GetNselmat();
  if (  !n  )
  {
    _bcol_emm.ResetColour();
    return;
  }

  const int* sel= doc->_mats->GetSelMatCurLod();
  COLORREF first_col= RGB((int)255*doc->_mats->GetMat(sel[0]).GetEmmisive()._r,
                          (int)255*doc->_mats->GetMat(sel[0]).GetEmmisive()._g,
                          (int)255*doc->_mats->GetMat(sel[0]).GetEmmisive()._b);
  COLORREF col= first_col;
  for ( int i= 1; i < n; i++ )
  {
    col= RGB((int)255*doc->_mats->GetMat(sel[i]).GetEmmisive()._r,
             (int)255*doc->_mats->GetMat(sel[i]).GetEmmisive()._g,
             (int)255*doc->_mats->GetMat(sel[i]).GetEmmisive()._b);
    if ( col != first_col )
    {
      _bcol_emm.ResetColour();
      return;
    }
  }
  _bcol_emm.SetColor(col, col);
}


void CMatView::SetColorSpc()
{
  int n= doc->_mats->GetNselmat();
  if (  !n  )
  {
    _bcol_spc.ResetColour();
    return;
  }

  const int* sel= doc->_mats->GetSelMatCurLod();
  COLORREF first_col= RGB((int)255*doc->_mats->GetMat(sel[0]).GetSpecular()._r,
                          (int)255*doc->_mats->GetMat(sel[0]).GetSpecular()._g,
                          (int)255*doc->_mats->GetMat(sel[0]).GetSpecular()._b);
  COLORREF col= first_col;
  for ( int i= 1; i < n; i++ )
  {
    col= RGB((int)255*doc->_mats->GetMat(sel[i]).GetSpecular()._r,
             (int)255*doc->_mats->GetMat(sel[i]).GetSpecular()._g,
             (int)255*doc->_mats->GetMat(sel[i]).GetSpecular()._b);
    if ( col != first_col )
    {
      _bcol_spc.ResetColour();
      return;
    }
  }
  _bcol_spc.SetColor(col, col);
}


void CMatView::SetColors()
{
  SetColorAmb();
  SetColorDif();
  SetColorFod();
  SetColorEmm();
  SetColorSpc();
}


//nastavi barevne svetla ziskana z Buldozeru
void CMatView::SetLights()
{
  COLORREF amb_col= RGB((int)255*_light_amb_r, (int)255*_light_amb_g, (int)255*_light_amb_b);
  COLORREF dif_col= RGB((int)255*_light_dif_r, (int)255*_light_dif_g, (int)255*_light_dif_b);

  _bcol_light_amb.SetColor(amb_col, amb_col);    
  _bcol_light_dif.SetColor(dif_col, dif_col);

  UpdateData(FALSE);
}


//---------------------------   SS functions   ----------------------------------


//check SS initalization
BOOL CMatView::CheckOpenSS()
{
  if ( !SccFcs.Opened() )
    if ( !IS_SCC_SUCCESS(SccFcs.Init(_ss_server_name, SS_PROJ_NAME,
                                      doc->_mats->GetMatPath(), m_hWnd)) )
      return FALSE;

  return TRUE;
}


//get latest version of all selected files
BOOL CMatView::GetLatestVersion()
{
  if ( !CheckOpenSS() )
    return FALSE;

  BOOL ret= TRUE;
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( !IS_SCC_SUCCESS(SccFcs.GetLatestVersion((const char*) doc->_mats->GetMat(sel[i]).GetFileName())) )
    {
      char str[1024];
      sprintf(str, "The file %s\ncan not be updated.", (const char*) doc->_mats->GetMat(sel[i]).GetFileName());
      MessageBox(str, "Check out:", MB_ICONWARNING);
      ret= FALSE;
      break;
    }
    else         //got the latest version and must load it
    {
      doc->_mats->GetMat(sel[i]).Load();
      LoadMaterial();
    }
  }

  return ret;
}


//check out all selected files
BOOL CMatView::CheckOut()
{
  if ( !CheckOpenSS() )
    return FALSE;

  BOOL ret= TRUE;
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    SCCRTN scc_ret= SccFcs.CheckOut((const char*) doc->_mats->GetMat(sel[i]).GetFileName());
    if ( scc_ret != SCC_OK  &&  scc_ret != SCC_E_ALREADYCHECKEDOUT )
    {
      char str[1024];
      sprintf(str, "The file %s\ncan not be checked out.", (const char*) doc->_mats->GetMat(sel[i]).GetFileName());
      MessageBox(str, "Check out:", MB_ICONWARNING);
      ret= FALSE;
    }
    else
      doc->_mats->GetMat(sel[i]).SSCheckedOut(TRUE);
  }
  SetFlags();

  return ret;
}


//check in all selected files
BOOL CMatView::CheckIn()
{
  if ( !CheckOpenSS() )
    return FALSE;

  BOOL ret= TRUE;
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    //must firstly save the rvmat file
    doc->_mats->GetMat(sel[i]).ToSave(TRUE);
    doc->_mats->GetMat(sel[i]).Save();
    //check in
    SCCRTN scc_ret= SccFcs.CheckIn((const char*) doc->_mats->GetMat(sel[i]).GetFileName());
    if ( scc_ret != SCC_OK )
    {
      char str[1024];
      sprintf(str, "The file %s\ncan not be checked in.", (const char*) doc->_mats->GetMat(sel[i]).GetFileName());
      MessageBox(str, "Check in:", MB_ICONWARNING);
      ret= FALSE;
    }
  }
  SetFlags();

  return ret;
}

//make undo chech out, load the old version from SS
BOOL CMatView::UndoCheckOut()
{
  if ( !CheckOpenSS() )
    return FALSE;

  BOOL ret= TRUE;
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    //undo check out
    SCCRTN scc_ret= SccFcs.UndoCheckOut((const char*) doc->_mats->GetMat(sel[i]).GetFileName());
    if ( scc_ret != SCC_OK )
    {
      char str[1024];
      sprintf(str, "The file %s\ncan not be unchecked out.", (const char*) doc->_mats->GetMat(sel[i]).GetFileName());
      MessageBox(str, "Uncheck out:", MB_ICONWARNING);
      ret= FALSE;
    }
    else
    {
      doc->_mats->GetMat(sel[i]).Load();
      LoadMaterial();
    }
  }
  SetFlags();

  return ret;
}


//show SS history
BOOL CMatView::History()
{
  //je urcite oznacen prave jeden material (zarucuje enable, disable v menu)
  if ( !CheckOpenSS() )
    return FALSE;

  SCCRTN scc_ret= SccFcs.History((const char*) doc->_mats->GetFirstSelMatCurLod().GetFileName());
  if ( scc_ret != SCC_OK )
  {
    MessageBox("Mat can not show the history of this material.", "Show history", MB_ICONWARNING);
    return FALSE;
  }
  else 
    return TRUE;
}


//show differences between local file and the version in SS
BOOL CMatView::Diff()
{
  //je urcite oznacen prave jeden material (zarucuje enable, disable v menu)
  if ( !CheckOpenSS() )
    return FALSE;

  SCCRTN scc_ret= SccFcs.Diff((const char*) doc->_mats->GetFirstSelMatCurLod().GetFileName());
  if ( scc_ret != SCC_OK )
  {
    MessageBox("Mat can not show the differences of this material.", "Show differences", MB_ICONWARNING);
    return FALSE;
  }
  else 
    return TRUE;
}


//show SS properties of the material
BOOL CMatView::Properties()
{
  //je urcite oznacen prave jeden material (zarucuje enable, disable v menu)
  if ( !CheckOpenSS() )
    return FALSE;

  SCCRTN scc_ret= SccFcs.Properties((const char*) doc->_mats->GetFirstSelMatCurLod().GetFileName());
  if ( scc_ret != SCC_OK )
  {
    MessageBox("Mat can not show the properties of this material.", "Show history", MB_ICONWARNING);
    return FALSE;
  }
  else 
    return TRUE;
}


//change 'read-only' flag
BOOL CMatView::ReadOnly()
{
  BOOL ret= TRUE;
  BOOL change= FALSE;
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    //zmen RO atribut souboru
    if ( _chmod(doc->_mats->GetMat(sel[i]).GetFileName(), 0000200) )     //no success
    {
      MessageBox("The Read Only acces to file can not be changed", "Read only", MB_ICONWARNING);
      ret= FALSE;
    }
    else
    {
       //zmen RO flag
       doc->_mats->GetMat(sel[i]).RO( !doc->_mats->GetMat(sel[i]).RO() );
       change= TRUE;
    }
  }
  if ( change )
    SetFlags();

  return ret;
}


//---------------------------   enable&disalbe popup menu   ---------------------


//set flags in the material list and set the variable _all_ss_controlled and _all_checked_out
void CMatView::GetPopupMenuInfo()
{
  SetFlags();     //provedu pouze u prvniho v menu
                  //nastavi pro kazdy mateial flagy '_checked_out' a '_ss_controled'
  _all_ss_controlled= TRUE;
  _all_checked_out= TRUE;
  const int* sel= doc->_mats->GetSelMatCurLod();

  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( !doc->_mats->GetMat(sel[i]).SSControlled() )
      _all_ss_controlled= FALSE;
    if ( !doc->_mats->GetMat(sel[i]).SSCheckedOut() )
      _all_checked_out= FALSE;
  }
}


BOOL CMatView::EnablePmCopy()
{
  if ( _p3d_type != P3D_NO  &&  _p3d_type != P3D_EMPTY  &&  doc->_mats->GetNselmat() == 1 )
    return TRUE;
  else
    return FALSE;
}


BOOL CMatView::EnablePmPaste()
{
  if ( _p3d_type != P3D_NO  &&  _p3d_type != P3D_EMPTY  &&  doc->_mats->GetNselmat() == 1  &&  _mat_copied )
    return TRUE;  
  else
    return FALSE;  
}


BOOL CMatView::EnablePmLoad()
{
  if ( _p3d_type == P3D_NO  ||  _p3d_type == P3D_EMPTY  ||  !doc->_mats->GetNselmat() )
    return FALSE;
  else
    return TRUE;
}


BOOL CMatView::EnablePmLoadDefault()
{
  return EnablePmLoad();
}


BOOL CMatView::EnablePmSave()
{
  switch ( _p3d_type )
  {
    case P3D_NO:
    case P3D_EMPTY:
    case P3D_PRIMITIVE:
      return FALSE;
    break;
    case P3D_NORMAL:
      return TRUE;
    break;
    default:
      return FALSE;
  }
}


BOOL CMatView::EnablePmSaveAs()
{
  switch ( _p3d_type )
  {
  case P3D_NO:
  case P3D_EMPTY:
    return FALSE;
  break;
  case P3D_PRIMITIVE:
  case P3D_NORMAL:
    return TRUE;
  break;
  default:
    return FALSE;
  }
}


BOOL CMatView::EnablePmExplore()
{
  return EnablePmLoad();
}


BOOL CMatView::EnablePmGetLatestVersion()
{
  GetPopupMenuInfo();          //zavolam jen u prvniho

  if ( _p3d_type == P3D_NO  ||  _p3d_type == P3D_EMPTY  ||   _p3d_type == P3D_PRIMITIVE  ||
       !doc->_mats->GetNselmat()  ||  !_all_ss_controlled )
    return FALSE;
  else
    return TRUE;
}


BOOL CMatView::EnablePmCheckOut()
{
  if ( _p3d_type == P3D_NO  ||  _p3d_type == P3D_EMPTY  ||  _p3d_type == P3D_PRIMITIVE  ||
       !doc->_mats->GetNselmat()  ||  !_all_ss_controlled )
    return FALSE;
  else
    //1. varianta
    //povolim check out, ikdyz uz to je jedenkrat checkoutnuty
    return TRUE;
    //2. varianta
    //jestlize neni ani jeden z oznacenych materialu checkoutnuty muzu nastavit na TRUE (povolit CheckOut)
    /*
    //v GetPopupMenuInfo() je treba nastavit _all_not_checked_out
    if ( _all_not_checked_out )
      return TRUE;
    else
      return FALSE;
    */
}


//it's faster if you use the last call
BOOL CMatView::EnablePmCheckIn()     // BOOL lc= FALSE
{
  if ( _p3d_type == P3D_NO  ||  _p3d_type == P3D_EMPTY  ||   _p3d_type == P3D_PRIMITIVE  ||
       !doc->_mats->GetNselmat()  ||  !_all_ss_controlled  ||  !_all_checked_out )
    return FALSE;
  else
    return TRUE;  
}


BOOL CMatView::EnablePmUndoCheckOut()
{
  return EnablePmCheckIn();              //faster with last call
}


BOOL CMatView::EnablePmHistory()
{
  if ( _p3d_type == P3D_NO  ||  _p3d_type == P3D_EMPTY  ||   _p3d_type == P3D_PRIMITIVE  ||
       doc->_mats->GetNselmat() != 1  ||  !_all_ss_controlled )
    return FALSE;
  else
    //je oznaceny prave jeden material
    return TRUE;
}


BOOL CMatView::EnablePmDiff()
{
  return EnablePmHistory();
}


BOOL CMatView::EnablePmProperties()
{
  return EnablePmHistory();
}


BOOL CMatView::EnablePmReadOnly(BOOL& check)
{
  if ( _p3d_type == P3D_NO  ||  _p3d_type == P3D_EMPTY  ||   _p3d_type == P3D_PRIMITIVE  ||
       !doc->_mats->GetNselmat() )
  {
    check= FALSE;
    return FALSE;
  }
  else
  {
    BOOL enable;
    BOOL all_ro= TRUE;
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( !doc->_mats->GetMat(sel[i]).RO() )
      {
        all_ro= FALSE;
        break;
      }
    }
    if ( all_ro )      
    {
      enable= TRUE;
      check= TRUE;
    }
    else
    {
      enable= FALSE;
      check= FALSE;
    }
    return enable;
  }
}


void CMatView::StartBuldozer()
{
  StartViewer(*frame, doc->_obj, _buld_path, 1.0, false, _shared_mem);
  UpdateLights();
  _run_buld= TRUE;
}


//---------------------------   Public:   ---------------------------------------


//zepta se uzivatale, zda ulozit vsechny rozdelane matiraly a provede
void CMatView::AskToSave(const BOOL rep_no_changes)
{
  if ( doc->_mats == NULL )
    return;
  if ( _p3d_type == P3D_PRIMITIVE )
    return;
  StatusBar("Saving");
  Material* mats= doc->_mats->GetAllMaterials();
  RString str= "Do you want to save";
  BOOL save= FALSE;

  for ( int i= 0; i<doc->_mats->GetNmatCurLod(); i++ )
  {
    if ( mats[i].ToSave() )
    {
      str= str + "\n" + mats[i].GetMatName();
      save= TRUE;
    }
  }
  str= str + " ?";
  if ( !save )
  {
    if ( rep_no_changes )
      MessageBox("No changes.", 0, MB_ICONINFORMATION | MB_OK);
    StatusBar();
    return;
  }
  if ( MessageBox((const char*) str, "Save Material", MB_ICONWARNING | MB_YESNO) == IDYES )
  {
    SaveMaterial();
    CheckFilesBeforeSave(F_ALL);
    if ( !doc->_mats->SaveAll() )
      MessageBox("File can not be saved.", "Save file:", MB_ICONWARNING);
    SetFlags();
  }
  StatusBar();
}


void CMatView::SetTimeOvercastFog(CTime time, float overcast, float fog)
{
  _keep_weather= TRUE;
  _time= time;
  _overcast= overcast;
  _fog= fog;
}


void CMatView::SetTime(float time)
{
  int n_sec= (int) (time*86400);

  int hour= n_sec / 3600;
  int min = (n_sec % 3600) / 60;
  int sec = (n_sec % 3600) % 60;
  CTime t(1978, 6, 1, hour, min, sec);
  _time= t;
}


CTime CMatView::GetTime() const
{
  return _time;
}


//update lights from Buldozer
void CMatView::UpdateLights()
{
  HWND buld_window= ::FindWindow(BULD_CLASS_NAME, BULD_WINDOW_NAME);
  if ( buld_window )
  {
    _run_buld= TRUE;
    ::PostMessage(buld_window, USER_WINDOW_HANDLE, 0, (LPARAM) m_hWnd);        //pro jistotu
    LogF("Message sent: USER_WINDOW_HANDLE");
    ::PostMessage(buld_window, USER_LIGHT_REQUEST, 0, 0);
    LogF("Message sent: USER_LIGHT_REQUEST");
  }
  else
  {
    _run_buld= FALSE;
    _updating= FALSE;
  }
}


void CMatView::UpdateTime()
{
  HWND buld_window= ::FindWindow(BULD_CLASS_NAME, BULD_WINDOW_NAME);
  if ( buld_window )
  {
    _run_buld= TRUE;
    ::PostMessage(buld_window, USER_WINDOW_HANDLE, 0, (LPARAM) m_hWnd);        //pro jistotu
    LogF("Message sent: USER_WINDOW_HANDLE");

    ::PostMessage(buld_window, USER_TIME_REQUEST, 0, 0);
    LogF("Message sent: USER_TIME_REQUEST");
  }
  else
  {
    _run_buld= FALSE;
    _updating= FALSE;
  }
}


void CMatView::Update()
{
  doc->_mats->Changed(TRUE);   //chci skutecne update
  StartUpdate();               //provadi update jen kdyz doslo ke zmene
                               //po provedeni updatu posle Buldozer zpravu a Mat nani odpovi EndUpdate()
}


void CMatView::ForbidUpdate(BOOL fu)
{
  if ( fu )
    LogF("Update forbiddance: TRUE");
  else
    LogF("Update forbiddance: FALSE");

  _update_forbiddance= fu;
}


//---------------------------   Overrides   ------------------------------------------------


void CMatView::OnInitialUpdate()
{
	cdxCDynamicFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//TO DO:
  view= this;
  char str[256];
  GetCurrentDirectory(256, str);
  _cfg_file= str;
  _cfg_file= _cfg_file + "\\" + CFG_FILE;
  ReadCfg();
  ReadCfgStage();
	if ( _automatic_buldozer_start )
    StartBuldozer();
  else
    _run_buld= FALSE;         //ikdyz je Buldozer nahodou spusteny, tak se nepripojuju

  ResetValues();
	UpdateData(FALSE);
  InitControls();
  _list_mat.SetTabStops(8);              //vyhrazeny prostor na zobrazovani flagu
  SetTimer(UPD_TIMER, UPD_TIME, 0);
  SetTimer(WEATHER_TIMER, WEATHER_TIME, 0);
  //flags
	_enable= FALSE;
  _p3d_type= P3D_NO;
  _backup= FALSE;
  _mat_copied= FALSE;
  _updating= FALSE;
  _update_forbiddance= FALSE;
  //buldozer weather
  _keep_weather= FALSE;
  _weather_dlg= NULL;
  _time= CTime(1985,6,15,12,0,0);
  _overcast= _fog= 0;
  //popup menu info
  _all_ss_controlled= _all_checked_out= FALSE;
  //buldozer light
  _light_amb_r= _light_amb_g= _light_amb_b= _light_dif_r= _light_dif_g= _light_dif_b= 0;
  //spins
  _last_spin_pos= 0;
  _last_spin_move= 0;          //no move

  UpdateData(FALSE);   //potreba
}


// CMatView diagnostics

#ifdef _DEBUG
void CMatView::AssertValid() const
{
	cdxCDynamicFormView::AssertValid();
}

void CMatView::Dump(CDumpContext& dc) const
{
	cdxCDynamicFormView::Dump(dc);
}

CMatDoc* CMatView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMatDoc)));
	return (CMatDoc*)m_pDocument;
}
#endif //_DEBUG


//funguje to i bez teto funkce (vygenerovana MFC)
//create window
int CMatView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  //MFC
  if (cdxCDynamicFormView::OnCreate(lpCreateStruct) == -1)
    return -1;
  return 0;
}


//destroy window
void CMatView::OnDestroy()
{
  cdxCDynamicFormView::OnDestroy();

  // TODO: Add your message handler code here
	if ( _automatic_buldozer_end )
    CloseViewer();
  KillTimer(UPD_TIMER);
  KillTimer(WEATHER_TIMER);
}


//---------------------------   Menu & Toolbar   -------------------------------------------


//load p3d object
void CMatView::OnFileOpen()
{
  // TODO: Add your command handler code here
  TCHAR file_name[256]= {0};
  OPENFILENAME ofd;

  ZeroMemory(&ofd, sizeof(OPENFILENAME));
  ofd.lStructSize= sizeof(OPENFILENAME);
  ofd.lpstrFile= file_name;
  ofd.hwndOwner= m_hWnd;
  ofd.nMaxFile= sizeof(file_name);
  ofd.Flags= OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST;
  ofd.lpstrFilter= "p3d file (*.p3d)\0*.p3d\0";               //filtr
  ofd.lpstrInitialDir= _last_p3d_path;
  ofd.lpstrDefExt= "p3d";
  AskToSave(FALSE);
  if ( !GetOpenFileName(&ofd) ) 
    return;
  
  OpenObj(file_name);
}


//ze vsech oznacenych matu ulozi ty, ktere byly zmeneny
void CMatView::OnFileSave()
{
  if ( !doc->_mats->GetNmatCurLod() )
  {
    MessageBox( "No material.", 0, MB_ICONINFORMATION | MB_OK);
    return;
  }
  //zjistim vsechny oznacene materialy v aktualnim lodu
  int n= doc->_mats->GetNselmat();
  if ( !n )
  {
    MessageBox( "No selected material.", 0, MB_ICONINFORMATION | MB_OK);
    return;
  }

  RString str= "Do you want to save";
  BOOL save= FALSE;
  const const int* sel= doc->_mats->GetSelMatCurLod();

  //projdu vsechny vybrane soubory
  for ( int i= 0; i < n; i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).ToSave() )
    {
      str= str + "\n" + doc->_mats->GetMat(sel[i]).GetMatName();
      save= TRUE;
    }
  }
  str= str + " ?";
  if ( !save )
  {
    MessageBox("No changes in selected materials.", 0, MB_ICONINFORMATION | MB_OK);
    StatusBar();
    return;
  }
  StatusBar("Saving");
  if ( MessageBox((const char*) str, "Save Material", MB_ICONWARNING | MB_YESNO) == IDYES )
  {
    SaveMaterial();
    CheckFilesBeforeSave(F_SELECTED);
    if ( !doc->_mats->SaveSel() )               //ulozi oznacene soubory u nich doslo ke zmene
    {
      MessageBox("File can not be saved.", "Save file:", MB_ICONWARNING);
      return;
    }
    SetFlags();
  }
  StatusBar();
}


//pokud je oznaceny jeden material tak ho ulozi, jinak zahlasi
void CMatView::OnFileSaveas()
{
  if ( !doc->_mats->GetNmatCurLod() )
  {
    MessageBox( "No material." );
    return;
  }
  //zjistim vsechny oznacene materialy v aktualnim lodu
  int n= doc->_mats->GetNselmat();
  if ( !n )
  {
    MessageBox( "No selected material.", 0, MB_ICONINFORMATION | MB_OK);
    return;
  }
  if ( n > 1 )
  {
    MessageBox( "Select only one material.", 0, MB_ICONINFORMATION | MB_OK);
    return;
  }

  TCHAR file_name[256]= {0};
  OPENFILENAME ofd;
  WFilePath path= (const char*) doc->_mats->GetFirstSelMatCurLod().GetFileName();
  strcpy(file_name, path.GetFilename());
  ZeroMemory(&ofd, sizeof(OPENFILENAME));
  ofd.lStructSize= sizeof(OPENFILENAME);
  ofd.lpstrFile= file_name;
  ofd.hwndOwner= m_hWnd;
  ofd.nMaxFile= sizeof(file_name);
  ofd.Flags= OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST;
  ofd.lpstrInitialDir= path.GetDirectory();
  ofd.lpstrFilter= "rvmat file (*.rvmat)\0*.rvmat\0";               //filtr
  ofd.lpstrDefExt= "rvmat";
  if ( !GetSaveFileName(&ofd) ) 
    return;
  StatusBar("Saving");
  SaveMaterial();                     //from view to variables
  WIN32_FIND_DATA file_data;
  HANDLE h= FindFirstFile((LPCSTR)file_name, &file_data);
  if ( h != INVALID_HANDLE_VALUE )              //file exist
  {
    char str[6256];
    sprintf(str, "The file %s exist. Overwrite?", file_name);
    if ( MessageBox(str, "Overwrite", MB_ICONWARNING|MB_OKCANCEL) != IDOK )
    {
      FindClose(h);
      StatusBar();
      return;
    }
  }
  if ( !doc->_mats->GetFirstSelMatCurLod().Save(file_name) )
  {
    MessageBox("File can not be saved.", "Save file:", MB_ICONWARNING);
    StatusBar();
    return;
  }
  doc->_mats->GetFirstSelMatCurLod().ToSave(FALSE);
  SetFlags();
  StatusBar();
}


void CMatView::OnFileSaveall()
{
  AskToSave();  //TRUE
}


//zapina a vypina polozku 'Save' v menu a prislusny buttonek
void CMatView::OnUpdateFileSave(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmSave());
}


void CMatView::OnUpdateFileSaveas(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmSaveAs());
}


void CMatView::OnUpdateFileSaveall(CCmdUI *pCmdUI)
{
  switch ( _p3d_type )
  {
  case P3D_NO:
  case P3D_EMPTY:
  case P3D_PRIMITIVE:
    pCmdUI->Enable(FALSE);
  break;
  case P3D_NORMAL:
    pCmdUI->Enable(TRUE);
  break;
  }
}


void CMatView::OnFileGetLatestVersion()
{
  if ( !CheckOpenSS() )
    return;
  if ( !_obj_path )
    return;

  if ( !SccFcs.UnderSSControl(_obj_path) )
  {
    char str[1024];
    sprintf(str, "Directory %s\n is not under SourceSafe control.", (const char*)_obj_path);
    LogF(str);
    MessageBox(str, "Get latest version:", MB_ICONWARNING);
    return;
  }
  if ( !IS_SCC_SUCCESS(SccFcs.GetLatestVersionDir(_obj_path, TRUE)) )
  {
    char str[1024];
    sprintf(str, "Can not get latest version of the model\n %s.", (const char*)_obj_path);
    LogF(str);
    MessageBox(str, "Get latest version:", MB_ICONWARNING);
  }

  GetLatestVersion();          //get latest version of all materials
  OpenObj(_obj_path);
}


void CMatView::OnUpdateFileGetLatestVersion(CCmdUI *pCmdUI)
{
  if ( _p3d_type != P3D_NO  &&  _p3d_type != P3D_EMPTY  &&  _p3d_type != P3D_PRIMITIVE )
    pCmdUI->Enable(TRUE);
  else
    pCmdUI->Enable(FALSE);
}


void CMatView::OnEditUndo()
{
  int lha= doc->_mats->HistoryGetLastAction();

  //pred provedenim Undo je treba ulozit aktualni stav materialu do historie
  if ( lha == LHA_ADD )
  {
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    }
    doc->_mats->HistoryAdd();
    doc->_mats->HistoryUndo();          //je treba udelat Undo na aktualnim stavu
  }
  if ( lha == LHA_REDO )
  {
    doc->_mats->HistoryUndo();          //je treba udelat Undo na aktualnim stavu
  }
  doc->_mats->HistoryUndo();

  Update();

  LoadMaterial();
}


void CMatView::OnEditRedo()
{
  int lha= doc->_mats->HistoryGetLastAction();

  if ( lha == LHA_UNDO )
  {
    doc->_mats->HistoryRedo();    
  }
  doc->_mats->HistoryRedo();

  Update();

  LoadMaterial();
}


void CMatView::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
  switch ( doc->_mats->HistoryGetLastAction() )
  {
    case LHA_NO   :
      pCmdUI->Enable(FALSE);
    break;
    case LHA_ADD  :
      pCmdUI->Enable(TRUE);
    break;
    case LHA_UNDO :
      if ( !doc->_mats->HistoryEmpty() )
        pCmdUI->Enable(TRUE);
      else
        pCmdUI->Enable(FALSE);
    break;
    case LHA_REDO :
      pCmdUI->Enable(TRUE);
    break;
  }
}


void CMatView::OnUpdateEditRedo(CCmdUI *pCmdUI)
{
  switch ( doc->_mats->HistoryGetLastAction() )
  {
    case LHA_NO   :
      pCmdUI->Enable(FALSE);
    break;
    case LHA_ADD  :
      pCmdUI->Enable(FALSE);
    break;
    case LHA_UNDO :
      pCmdUI->Enable(TRUE);
    break;
    case LHA_REDO :
      if ( doc->_mats->HistoryLastPos() )
        pCmdUI->Enable(FALSE);
      else
        pCmdUI->Enable(TRUE);
    break;
  }
}


//run buldozer
void CMatView::OnViewBuldozer()
{
  if ( _run_buld )
    CloseViewer();
  StartBuldozer();

  Update();
}


//vytvori proces, ceka na jeho ukonce, pak posle zpravu Matu
int ProcExternEditor(LPVOID lpParameter)
{
  //DWORD dwExitCode;
  STARTUPINFO sui;
  PROCESS_INFORMATION pi;
  ZeroMemory(&sui, sizeof(sui));
  ZeroMemory(&pi, sizeof(pi));
  sui.dwFlags= STARTF_USESHOWWINDOW;
  sui.wShowWindow= SW_SHOW;
  CreateProcess(NULL, (LPSTR)(const char*) g_com_line, NULL, NULL, FALSE, 0, NULL, NULL, &sui, &pi);
  WaitForSingleObject(pi.hProcess, INFINITE);
  //GetExitCodeProcess(pi.hProcess, &dwExitCode);  - zbytecne
  CloseHandle(g_thread_editor);
  SendMessage(g_handle_view, WM_EXTERN_EDITOR, 0, 0);

  return 0;
}


//spusti externi editor
void CMatView::OnViewExternalEditor()
{
  int n= doc->_mats->GetNselmat();
  if ( !n )
  {
    MessageBox( "No selected material.", 0, MB_ICONINFORMATION | MB_OK);
    return;
  }
  if ( n > 1 )
  {
    MessageBox( "Select please only one material.", 0, MB_ICONINFORMATION | MB_OK);
    return;
  }
  _ext_mat= doc->_mats->GetSelMatCurLod()[0];

  DWORD thread_id;

  g_com_line= _extern_editor + " " + doc->_mats->GetFirstSelMatCurLod().GetTempName();
  g_handle_view= m_hWnd;
  g_thread_editor= CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ProcExternEditor, (LPVOID)5, 0, &thread_id);
  if ( !g_thread_editor )
  {
    LogF("External editor can not start.");
    MessageBox("External editor can not start.", "", MB_ICONWARNING | MB_OK);
  }
}


void CMatView::OnUpdateViewExternaleditor(CCmdUI *pCmdUI)
{
  switch ( _p3d_type )
  {
  case P3D_NO:
  case P3D_EMPTY:
    pCmdUI->Enable(FALSE);
    break;
  case P3D_PRIMITIVE:
  case P3D_NORMAL:
    pCmdUI->Enable(TRUE);
    break;
  }
}


void CMatView::OnPrimSphere()
{
  StatusBar("Loading");
  ResetObj(TRUE);              //TRUE - create backup of model
  if ( doc->_obj->Load(WFilePath(_mat_path+_sphere_path), NULL, NULL) == 0 )    //nazacatku se provede destrukce stareho
  {
    if ( doc->_mats->Load(doc->_obj, _mat_path) )
    {
      SetListLod();
      SetTitle(_sphere_path);
      ResetControlsAfterLoad();
      if ( doc->_mats->ExistMat() )
        Enable();
      _p3d_type= P3D_PRIMITIVE;
      AskToLoadFromBackupMat();
      ProtectStages();
      CheckStageEnable();
      SetColors();

      Update();
    }
    else
    {
      LogF("Materials can't load from %s.",(const char*)_sphere_path);
      MessageBox("Materials can not load from file.", 0, MB_ICONEXCLAMATION | MB_OK);
    }
  }
  else
  {
    LogF("%s: This file can not load.",(const char*)_sphere_path);
    MessageBox("This file can not load.", 0, MB_ICONEXCLAMATION | MB_OK);
  }
  StatusBar();
}


void CMatView::OnPrimSphere2()
{
  StatusBar("Loading");
  ResetObj(TRUE);
  if ( doc->_obj->Load(WFilePath(_mat_path+_sphere2_path), NULL, NULL) == 0 )    //nazacatku se provede destrukce stareho
  {
    if ( doc->_mats->Load(doc->_obj, _mat_path) )
    {
      SetListLod();
      SetTitle(_sphere2_path);
      ResetControlsAfterLoad();
      if ( doc->_mats->ExistMat() )
        Enable();
      _p3d_type= P3D_PRIMITIVE;
      AskToLoadFromBackupMat();
      ProtectStages();
      CheckStageEnable();
      SetColors();

      Update();
    }
    else
    {
      LogF("Materials can't load from %s.",(const char*)_sphere2_path);
      MessageBox("Materials can not load from file.", 0, MB_ICONEXCLAMATION | MB_OK);
    }
  }
  else
  {
    LogF("%s: This file can not load.",(const char*)_sphere2_path);
    MessageBox("This file can not load.", 0, MB_ICONEXCLAMATION | MB_OK);
  }
  StatusBar();
}


void CMatView::OnPrimBox()
{
  StatusBar("Loading");
  ResetObj(TRUE);
  if ( doc->_obj->Load(WFilePath(_mat_path+_box_path), NULL, NULL) == 0 )    //nazacatku se provede destrukce stareho
  {
    if ( doc->_mats->Load(doc->_obj, _mat_path) )
    {
      SetListLod();
      SetTitle(_box_path);
      ResetControlsAfterLoad();
      if ( doc->_mats->ExistMat() )
        Enable();
      _p3d_type= P3D_PRIMITIVE;
      AskToLoadFromBackupMat();
      ProtectStages();
      CheckStageEnable();
      SetColors();

      Update();
    }
    else
    {
      LogF("Materials can't load from %s.",(const char*)_box_path);
      MessageBox("Materials can not load from file.", 0, MB_ICONEXCLAMATION | MB_OK);
    }
  }
  else
  {
    LogF("%s: This file can not load.",(const char*)_box_path);
    MessageBox("This file can not load.", 0, MB_ICONEXCLAMATION | MB_OK);
  }
  StatusBar();
}


void CMatView::OnPrimCone()
{
  StatusBar("Loading");
  ResetObj(TRUE);
  if ( doc->_obj->Load(WFilePath(_mat_path+_cone_path), NULL, NULL) == 0 )    //nazacatku se provede destrukce stareho
  {
    if ( doc->_mats->Load(doc->_obj, _mat_path) )
    {
      SetListLod();
      SetTitle(_cone_path);
      ResetControlsAfterLoad();
      if ( doc->_mats->ExistMat() )
        Enable();
      _p3d_type= P3D_PRIMITIVE;
      AskToLoadFromBackupMat();
      ProtectStages();
      CheckStageEnable();
      SetColors();

      Update();
    }
    else
    {
      LogF("Materials can't load from %s.",(const char*)_cone_path);
      MessageBox("Materials can not load from file.", 0, MB_ICONEXCLAMATION | MB_OK);
    }
  }
  else
  {
    LogF("%s: This file can not load.",(const char*)_cone_path);
    MessageBox("This file can not load.", 0, MB_ICONEXCLAMATION | MB_OK);
  }
  StatusBar();
}


void CMatView::OnPrimCone2()
{
  StatusBar("Loading");
  ResetObj(TRUE);               //TRUE - create backup of model
  if ( doc->_obj->Load(WFilePath(_mat_path+_cone2_path), NULL, NULL) == 0 )    //nazacatku se provede destrukce stareho
  {
    if ( doc->_mats->Load(doc->_obj, _mat_path) )
    {
      SetListLod();
      SetTitle(_cone2_path);
      ResetControlsAfterLoad();
      if ( doc->_mats->ExistMat() )
        Enable();
      _p3d_type= P3D_PRIMITIVE;
      AskToLoadFromBackupMat();
      ProtectStages();
      CheckStageEnable();
      SetColors();

      Update();
    }
    else
    {
      LogF("Materials can't load from %s.",(const char*)_cone2_path);
      MessageBox("Materials can not load from file.", 0, MB_ICONEXCLAMATION | MB_OK);
    }
  }
  else
  {
    LogF("%s: This file can not load.",(const char*)_cone2_path);
    MessageBox("This file can not load.", 0, MB_ICONEXCLAMATION | MB_OK);
  }
  StatusBar();
}


//back to the backuped model
void CMatView::OnViewMainModel()
{
  if ( !_backup )
  {
    DeleteBackup();        //uklid (pro jistotu)
    return;                //zadna zaloha neexistuje
  }
  if ( AskToSaveToBackupMat() )
  {
    //zmenim backup materialy
    int n= doc->_mats_backup->GetNselmat();
    const int* sel= doc->_mats_backup->GetSelMatCurLod();

    for ( int i= 0; i < n; i++ )
      doc->_mats_backup->GetMat(sel[i])= doc->_mats->GetFirstSelMatCurLod();
  }
  RestoreBackupObj();  

  const int* sel= doc->_mats->GetSelMatCurLod();
  if ( doc->_mats->GetNselmat() )
    SetListLod(sel[0]);
  else
    SetListLod();                    //select the 1. row

  ResetControlsAfterLoad();
  ProtectStages();
  CheckStageEnable();
  SetTitle(_obj_path);
  SetColors();
  if ( doc->_mats->ExistMat() )
    Enable();
  _p3d_type= P3D_NORMAL;         //nutne pred SetFlags()
  SetFlags();
  UpdateData(FALSE);             //nutne

  Update();

}


void CMatView::OnUpdateViewMainmodel(CCmdUI *pCmdUI)
{
  switch ( _p3d_type )
  {
  case P3D_NO:
  case P3D_EMPTY:
  case P3D_NORMAL:
    pCmdUI->Enable(FALSE);
  break;
  case P3D_PRIMITIVE:
    if ( _backup )
      pCmdUI->Enable(TRUE);
    else
      pCmdUI->Enable(FALSE);
  break;
  }
}


//reaguje na kliknuti na 'Configuration', spusti options dialog
void CMatView::OnOptionsConfiguration()
{
  int i;
  CString str;
	COptionsDlg options;

  //nacpe options udaji z promenych z CMatView
  options._edit_buld_path= _buld_path;
  options._edit_material_path= _mat_path;
  options._edit_ps= options._edit_vs= "";
  for ( i= 0; i < _combo_PSI.GetCount(); i++ )
  {
    _combo_PSI.GetLBText(i, str);
    if ( i )
      options._edit_ps+= ", ";
    options._edit_ps+= str;
  }
  for ( i= 0; i < _combo_VSI.GetCount(); i++ )
  {
    _combo_VSI.GetLBText(i, str);
    if ( i )
      options._edit_vs+= ", ";
    options._edit_vs+= str;
  }
  for ( i= 0; i < _combo_uvTex.GetCount(); i++ )
  {
    _combo_uvTex.GetLBText(i, str);
    if ( i )
      options._edit_uvTex+= ", ";
    options._edit_uvTex+= str;
  }
  options._edit_editor= _extern_editor;
  options._edit_texview= _texview_path;
  options._edit_default_materials= _default_mat_path;
  options._edit_ss_server_name= _ss_server_name;
  options._edit_coef_gray= g_coef_gray;
  options._check_buld_start= _automatic_buldozer_start;
  options._check_buld_end= _automatic_buldozer_end;
  options._disable_unselect_mats= _disable_unselect_mats;
  switch ( _dis_mats_col )
  {
    case DMC_GRAY : options._radio_dmc= DMC_GRAY;
    break;
    case DMC_RED  : options._radio_dmc= DMC_RED;
    break;
    case DMC_GREEN: options._radio_dmc= DMC_GREEN;
    break;
    case DMC_BLUE : options._radio_dmc= DMC_BLUE;
    break;
    default       : options._radio_dmc= DMC_GRAY;
  }

  //run options dialog
  if ( options.DoModal() == IDOK )
  {
    //naplni data z options do CMatView
    _buld_path= (const char*) options._edit_buld_path;
    _mat_path= (const char*) options._edit_material_path;
    int pos= 0;
    _combo_PSI.ResetContent();
    CString str= options._edit_ps.Tokenize(", ", pos);
    while ( str != "" )
    {
       _combo_PSI.InsertString(-1, str);
       str= options._edit_ps.Tokenize(", ", pos);       
    }
    pos= 0;
    _combo_VSI.ResetContent();
    str= options._edit_vs.Tokenize(", ", pos);
    while ( str != "" )
    {
       _combo_VSI.InsertString(-1, str);
       str= options._edit_vs.Tokenize(", ", pos);       
    }
    pos= 0;
    _combo_uvTex.ResetContent();
    str= options._edit_uvTex.Tokenize(", ", pos);
    while ( str != "" )
    {
       _combo_uvTex.InsertString(-1, str);
       str= options._edit_uvTex.Tokenize(", ", pos);       
    }
    _extern_editor= (const char*) options._edit_editor;
    _texview_path= (const char*) options._edit_texview;
    _default_mat_path= (const char*) options._edit_default_materials;
    _ss_server_name= (const char*) options._edit_ss_server_name;
    if (  options._edit_coef_gray >= 0  &&  options._edit_coef_gray <= 1  )
    {
      if ( g_coef_gray != options._edit_coef_gray )
      {
        g_coef_gray= options._edit_coef_gray;
        if ( _disable_unselect_mats )
          doc->_mats->ChangeGrayCoef();      

        Update();
      }
      else
        g_coef_gray= options._edit_coef_gray;
    }
    _automatic_buldozer_start= options._check_buld_start;
    _automatic_buldozer_end= options._check_buld_end;
    if ( _disable_unselect_mats != options._disable_unselect_mats )
    {
      _disable_unselect_mats= options._disable_unselect_mats;
      DisableUnselectedMaterials();
    }
    g_unselected_mats= _dis_mats_col= options._radio_dmc;

    UpdateData(FALSE);
    SaveCfg();                  //ulozi data do mat.cfg

    doc->_mats->EnableAllMat();
    doc->_mats->EnableSelMat();
    Update();
  }
}


//change the weather
void CMatView::OnWeather()
{
  UpdateTime();

  if ( _weather_dlg )
    return;

  _weather_dlg= new CWeatherDlg;
  _weather_dlg->Create(IDD_DLG_WEATHER);
  _weather_dlg->SetScriptName(_mat_path+SCRIPT_FILE);
  _weather_dlg->SetMainWnd(m_hWnd);
  _weather_dlg->ShowWindow(SW_SHOW);
}


void CMatView::OnUpdateWeather(CCmdUI *pCmdUI)
{
  if ( _run_buld )
    pCmdUI->Enable(TRUE);
  else
    pCmdUI->Enable(FALSE);
}


//---------------------------   Popup menu   -----------------------------------------------


//reakce na kliknuti pravym tlacitkem do seznamu materialu
void CMatView::OnRButtonDown()
{
  if ( doc->_mats->GetNselmat() != 1 )
    LogF("Popup menu can not be loaded because more than one (or none) materials are selected.");

  CWnd* pMain= AfxGetMainWnd();
  CMenu* mm= pMain->GetMenu();
  CMenu* sm= mm->GetSubMenu(1);     //edit

  CMenu pm;
  pm.CreatePopupMenu();
  int state;
  int id;
  CString name;
  BOOL add;
  BOOL enable;
  BOOL check;

  //create menu
  for ( int i= 0; i < sm->GetMenuItemCount(); i++ )
  {
    state= sm->GetMenuState(i, MF_BYPOSITION);
    id= sm->GetMenuItemID(i);
    sm->GetMenuString(i, name, MF_BYPOSITION);
    if ( i > 2 )
    {
      add= TRUE;
      enable= FALSE;
      check= FALSE;
      switch ( i )
      {
        //Copy
        case 3: enable= EnablePmCopy();        //32832
        break;
        //Paste
        case 4: enable= EnablePmPaste();
        break;
        //Load
        case 6: enable= EnablePmLoad();
        break;
        //Load default
        case 7: enable= EnablePmLoadDefault();
        break;
        //Save
        case 8: enable= EnablePmSave();
        break;
        //Save as
        case 9: enable= EnablePmSaveAs();
        break;
        //Explore
        case 10: enable= EnablePmExplore();
        break;
        //Get latest version
        case 12: enable= EnablePmGetLatestVersion();
        break;
        //Check out
        case 13: enable= EnablePmCheckOut();
        break;
        //Check in
        case 14: enable= EnablePmCheckIn();
        break;
        //Undo check out
        case 15: enable= EnablePmUndoCheckOut();
        break;
        //History
        case 16: enable= EnablePmHistory();
        break;
        //Diff
        case 17: enable= EnablePmDiff();
        break;
        //Properties
        case 18: enable= EnablePmProperties();
        break;
        //Read only
        case 20: enable= EnablePmReadOnly(check);
        break;
      }        //switch
    }
    else
    {
      add= FALSE;
    }
    if ( add )
    {
      pm.AppendMenu(state, id, name);
      //set enable
      if ( enable )
        pm.EnableMenuItem(pm.GetMenuItemCount()-1, MF_BYPOSITION | MF_ENABLED);
      else
        pm.EnableMenuItem(pm.GetMenuItemCount()-1, MF_BYPOSITION | MF_DISABLED | MF_GRAYED);
      /*
      //set check
      if ( check )
        pm.CheckMenuItem(pm.GetMenuItemCount()-1, MF_BYPOSITION | MF_CHECKED);
      else
        pm.CheckMenuItem(pm.GetMenuItemCount()-1, MF_BYPOSITION | MF_UNCHECKED);
      */
    }
  }   //for

  CPoint pt;
  GetCursorPos(&pt);                    //vrati absolutni souradnice
  pm.TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON, pt.x, pt.y, this);
}


//funguje pro hlavni i popup menu
void CMatView::OnPmCopy()
{
  //je oznacen prave jeden material (jinak je menu disable)
  _mat_copy= doc->_mats->GetFirstSelMatCurLod();
  _mat_copied= TRUE;
}


//funguje jen pro hlavni menu, ne pro popup menu!
void CMatView::OnUpdatePmCopy(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmCopy());
}


//funguje pro hlavni i popup menu
void CMatView::OnPmPaste()
{
  //je oznacen prave jeden material (jinak je menu disable)
  if ( _mat_copied )          //zbytecna podminka, slo by se spolehnout na "disable"
  {
     doc->_mats->GetFirstSelMatCurLod()= _mat_copy;
     doc->_mats->GetFirstSelMatCurLod().Changed(TRUE);   //zbytecne, to dela operator =
     LoadMaterial();
  }
}


//funguje jen pro hlavni menu, ne pro popup menu!
void CMatView::OnUpdatePmPaste(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmPaste());
}


//nacte material
void CMatView::OnPmLoad()
{
  TCHAR file_name[256]= {0};
  OPENFILENAME ofd;

  ZeroMemory(&ofd, sizeof(OPENFILENAME));
  ofd.lStructSize= sizeof(OPENFILENAME);
  ofd.lpstrFile= file_name;          //return the name of selected materials
  ofd.hwndOwner= m_hWnd;
  ofd.nMaxFile= sizeof(file_name);
  ofd.Flags= OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST;
  ofd.lpstrFilter= "rvmat file (*.rvmat)\0*.rvmat\0";               //filtr
  ofd.lpstrDefExt= "rvmat";
  ofd.lpstrInitialDir= (const char*) _last_mat_path;             
  if ( !GetOpenFileName(&ofd) ) 
    return;
  WFilePath p(file_name); 
  _last_mat_path= (RString)(p.GetDrive()+p.GetDirectory());
  SaveCfg();             
  RString mat_name= (RString)(p.GetDirectory()+p.GetFilename() );
  mat_name= mat_name.Substring(1, mat_name.GetLength());
  //mat_name.Lower();
  _edit_chmat= mat_name;             
  LoadMatFromFile(mat_name);         //set the name to the selected materials

  Update();
}


void CMatView::OnUpdatePmLoad(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmLoad());
}


void CMatView::OnPmLoadDefault()
{
  TCHAR file_name[256]= {0};
  OPENFILENAME ofd;

  ZeroMemory(&ofd, sizeof(OPENFILENAME));
  ofd.lStructSize= sizeof(OPENFILENAME);
  ofd.lpstrFile= file_name;          //return the name of selected materials
  ofd.hwndOwner= m_hWnd;
  ofd.nMaxFile= sizeof(file_name);
  ofd.Flags= OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST;
  ofd.lpstrFilter= "rvmat file (*.rvmat)\0*.rvmat\0";               //filtr
  ofd.lpstrDefExt= "rvmat";
  ofd.lpstrInitialDir= (const char*) _default_mat_path;             
  if ( !GetOpenFileName(&ofd) ) 
    return;
  WFilePath p(file_name); 
  _last_mat_path= (RString)(p.GetDrive()+p.GetDirectory());
  SaveCfg();             
  RString mat_name= (RString)(p.GetDirectory()+p.GetFilename() );
  mat_name= mat_name.Substring(1, mat_name.GetLength());
  //mat_name.Lower();
  _edit_chmat= mat_name;             
  LoadMatFromFile(mat_name);          //set the name to the selected materials

  Update();
}


void CMatView::OnUpdatePmLoadDefault(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmLoadDefault());
}


void CMatView::OnPmExplore()
{
  STARTUPINFO sui;
  PROCESS_INFORMATION pi;
  ZeroMemory(&sui, sizeof(sui));
  ZeroMemory(&pi, sizeof(pi));
  sui.dwFlags= STARTF_USESHOWWINDOW;
  sui.wShowWindow= SW_SHOW;
  char str[256];
  sprintf(str, "explorer.exe /select, \"%s\"", (const char*) doc->_mats->GetFirstSelMatCurLod().GetFileName());

  CreateProcess(NULL, (LPSTR) str, NULL, NULL, FALSE, 0, NULL, NULL, &sui, &pi);
}


void CMatView::OnUpdatePmExplore(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmExplore());
}


void CMatView::OnPmGetLatestVersion()
{
  if ( GetLatestVersion() )
    LogF("GetLatestVersion OK");
  else
    LogF("GetLatestVersion Error");
}


void CMatView::OnUpdatePmGetLatestVersion(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmGetLatestVersion());
}


void CMatView::OnPmCheckOut()
{
  if ( CheckOut() )
    LogF("CheckOut OK");
  else
    LogF("CheckOut Error");
}


void CMatView::OnUpdatePmCheckOut(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmCheckOut());
}


void CMatView::OnPmCheckIn()
{
  if ( CheckIn() )
    LogF("CheckIn OK");
  else
    LogF("CheckIn Error");
}


void CMatView::OnUpdatePmCheckIn(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmCheckIn());
}


void CMatView::OnPmUndoCheckOut()
{
  if ( UndoCheckOut() )
    LogF("UndoCheckOut OK");
  else
    LogF("UndoCheckOut Error");
}


void CMatView::OnUpdatePmUndoCheckOut(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmUndoCheckOut());
}


void CMatView::OnPmHistory()
{
  if ( History() )
    LogF("Show History OK");
  else
    LogF("Show History Error");
}


void CMatView::OnUpdatePmHistory(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmHistory());
}


void CMatView::OnPmDiff()
{
  if ( Diff() )
    LogF("Show Differences OK");
  else
    LogF("Show Differences Error");
}


void CMatView::OnUpdatePmDiff(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmDiff());
}


void CMatView::OnPmProperties()
{
  if ( Properties() )
    LogF("Show Properties OK");
  else
    LogF("Show Properties Error");
}


void CMatView::OnUpdatePmProperties(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(EnablePmProperties());
}


void CMatView::OnPmReadOnly()
{
  //vsechny oznacene materialy jsou read-only
  if ( ReadOnly() )
    LogF("Change Read-only OK");
  else
    LogF("Change Read-only Error");
}


void CMatView::OnUpdatePmReadOnly(CCmdUI *pCmdUI)
{
  BOOL check;
  pCmdUI->Enable(EnablePmReadOnly(check));
//  pCmdUI->SetCheck(check);
}


//---------------------------   Edits   ----------------------------------------------------


void CMatView::OnEnChangeEditAmbR()
{
  UpdateData(TRUE);

	_ambient_r= Round2(_ambient_r);
  if ( _check_lock_ambdif )
    _diffuse_r= _ambient_r;

  if ( _ambient_r >= 0 && _ambient_r <= 1 )
	{
    //proc +0.0001? _ambient_r s hodnotou 0.24 se obcas reprezentuje jako 0.2400001 neob 0.239999
    //2. pripad (0.23999) je spatny, spin se totiz nastavi na 0.23, 1. pripad je OK
    _spin_amb_r.SetPos32((int)(100*_ambient_r+0.0001));
    if ( _check_lock_ambdif ) 
      _spin_dif_r.SetPos32((int)(100*_diffuse_r+0.0001));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetAmbient()._r != _ambient_r )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._r != _diffuse_r )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;   //_ambient_r == _diffuse_r
      }
		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_DIF_R, _diffuse_r);
    }
	}
}


void CMatView::OnEnChangeEditAmbG()
{
  UpdateData(TRUE);
	_ambient_g= Round2(_ambient_g);
  if ( _check_lock_ambdif )
    _diffuse_g= _ambient_g;
  if ( _ambient_g >= 0 && _ambient_g <= 1 )
	{
    _spin_amb_g.SetPos32((int)(100*_ambient_g+0.0001));
    if ( _check_lock_ambdif ) 
      _spin_dif_g.SetPos32((int)(100*_diffuse_g+0.0001));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetAmbient()._g != _ambient_g )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._g != _diffuse_g )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
      }
		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_DIF_G, _diffuse_g);
    }
	}
}


void CMatView::OnEnChangeEditAmbB()
{
  UpdateData(TRUE);
	_ambient_b= Round2(_ambient_b);
  if ( _check_lock_ambdif )
    _diffuse_b= _ambient_b;
  if ( _ambient_b >= 0 && _ambient_b <= 1 )
	{
    _spin_amb_b.SetPos32((int)(100*_ambient_b+0.0001));
    if ( _check_lock_ambdif ) 
      _spin_dif_b.SetPos32((int)(100*_diffuse_b+0.0001));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetAmbient()._b != _ambient_b )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._b != _diffuse_b )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
      }
		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_DIF_B, _diffuse_b);
    }
	}
}


void CMatView::OnEnChangeEditAmbA()
{
  UpdateData(TRUE);
	_ambient_a= Round2(_ambient_a);
  if ( _check_lock_ambdif )
    _diffuse_a= _ambient_a;
  if ( _ambient_a >= 0 && _ambient_a <= 1 )
	{
    _spin_amb_a.SetPos32((int)(100*_ambient_a+0.0001));
    if ( _check_lock_ambdif ) 
      _spin_dif_a.SetPos32((int)(100*_diffuse_a+0.0001));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetAmbient()._a != _ambient_a )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._a != _diffuse_a )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._a= _ambient_a;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetDiffuse()._a= _diffuse_a;
      }
		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_DIF_A, _diffuse_a);
    }
	}
}


void CMatView::OnEnChangeEditDifR()
{
  UpdateData(TRUE);
	_diffuse_r= Round2(_diffuse_r);
  if ( _check_lock_ambdif )
    _ambient_r= _diffuse_r;
  if ( _diffuse_r >= 0 && _diffuse_r <= 1 )
	{
    _spin_dif_r.SetPos32((int)(100*_diffuse_r+0.0001));
    if ( _check_lock_ambdif ) 
      _spin_amb_r.SetPos32((int)(100*_ambient_r+0.0001));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._r != _diffuse_r )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetAmbient()._r != _ambient_r )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
      }
		}
    SetColorDif();
    if ( _check_lock_ambdif )
    {
      SetColorAmb();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_AMB_R, _ambient_r);
    }
	}
}


void CMatView::OnEnChangeEditDifG()
{
  UpdateData(TRUE);
	_diffuse_g= Round2(_diffuse_g);
  if ( _check_lock_ambdif )
    _ambient_g= _diffuse_g;
  if ( _diffuse_g >= 0 && _diffuse_g <= 1 )
	{
    _spin_dif_g.SetPos32((int)(100*_diffuse_g+0.0001));
    if ( _check_lock_ambdif ) 
      _spin_amb_g.SetPos32((int)(100*_ambient_g+0.0001));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._g != _diffuse_g )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetAmbient()._g != _ambient_g )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
      }
		}
    SetColorDif();
    if ( _check_lock_ambdif )
    {
      SetColorAmb();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_AMB_G, _ambient_g);
    }
	}
}


void CMatView::OnEnChangeEditDifB()
{
  UpdateData(TRUE);
	_diffuse_b= Round2(_diffuse_b);
  if ( _check_lock_ambdif )
    _ambient_b= _diffuse_b;
  if ( _diffuse_b >= 0 && _diffuse_b <= 1 )
	{
    _spin_dif_b.SetPos32((int)(100*_diffuse_b+0.0001));
    if ( _check_lock_ambdif ) 
      _spin_amb_b.SetPos32((int)(100*_ambient_b+0.0001));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._b != _diffuse_b )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetAmbient()._b != _ambient_b )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
      }
		}
    SetColorDif();
    if ( _check_lock_ambdif )
    {
      SetColorAmb();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_AMB_B, _ambient_b);
    }
	}
}


void CMatView::OnEnChangeEditDifA()
{
  UpdateData(TRUE);
	_diffuse_a= Round2(_diffuse_a);
  if ( _check_lock_ambdif )
    _ambient_a= _diffuse_a;
  if ( _diffuse_a >= 0 && _diffuse_a <= 1 )
	{
    _spin_dif_a.SetPos32((int)(100*_diffuse_a+0.0001));
    if ( _check_lock_ambdif ) 
      _spin_amb_a.SetPos32((int)(100*_ambient_a+0.0001));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._a != _diffuse_a )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetAmbient()._a != _ambient_a )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._a= _diffuse_a;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetAmbient()._a= _ambient_a;
      }
		}
    SetColorDif();
    if ( _check_lock_ambdif )
    {
      SetColorAmb();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_AMB_A, _ambient_a);
    }
	}
}


void CMatView::OnEnChangeEditFodR()
{
  UpdateData(TRUE);
	_forcedDiffuse_r= Round2(_forcedDiffuse_r);
  if ( _forcedDiffuse_r >= 0 && _forcedDiffuse_r <= 1 )
	{
    _spin_fod_r.SetPos32((int)(100*_forcedDiffuse_r));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r != _forcedDiffuse_r )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r= _forcedDiffuse_r;
      }
		}
    SetColorFod();
	}
}


void CMatView::OnEnChangeEditFodG()
{
  UpdateData(TRUE);
	_forcedDiffuse_g= Round2(_forcedDiffuse_g);
  if ( _forcedDiffuse_g >= 0 && _forcedDiffuse_g <= 1 )
	{
    _spin_fod_g.SetPos32((int)(100*_forcedDiffuse_g));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g != _forcedDiffuse_g )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g= _forcedDiffuse_g;
      }
		}
    SetColorFod();
	}
}


void CMatView::OnEnChangeEditFodB()
{
  UpdateData(TRUE);
	_forcedDiffuse_b= Round2(_forcedDiffuse_b);
  if ( _forcedDiffuse_b >= 0 && _forcedDiffuse_b <= 1 )
	{
    _spin_fod_b.SetPos32((int)(100*_forcedDiffuse_b));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b != _forcedDiffuse_b )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b= _forcedDiffuse_b;
      }
		}
    SetColorFod();
	}
}


void CMatView::OnEnChangeEditFodA()
{
  UpdateData(TRUE);
	_forcedDiffuse_a= Round2(_forcedDiffuse_a);
  if ( _forcedDiffuse_a >= 0 && _forcedDiffuse_a <= 1 )
	{
    _spin_fod_a.SetPos32((int)(100*_forcedDiffuse_a));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._a != _forcedDiffuse_a )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._a= _forcedDiffuse_a;
      }
		}
	}
}


void CMatView::OnEnChangeEditEmmR()
{
  UpdateData(TRUE);
	_emmisive_r= Round2(_emmisive_r);
  if ( _emmisive_r >= 0 && _emmisive_r <= 1 )
	{
    _spin_emm_r.SetPos32((int)(100*_emmisive_r));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._r != _emmisive_r )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetEmmisive()._r= _emmisive_r;
      }
		}
    SetColorEmm();
	}
}


void CMatView::OnEnChangeEditEmmG()
{
  UpdateData(TRUE);
	_emmisive_g= Round2(_emmisive_g);
  if ( _emmisive_g >= 0 && _emmisive_g <= 1 )
	{
    _spin_emm_g.SetPos32((int)(100*_emmisive_g));
    const const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._g != _emmisive_g )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetEmmisive()._g= _emmisive_g;
      }
		}
    SetColorEmm();
	}
}


void CMatView::OnEnChangeEditEmmB()
{
  UpdateData(TRUE);
	_emmisive_b= Round2(_emmisive_b);
  if ( _emmisive_b >= 0 && _emmisive_b <= 1 )
	{
    _spin_emm_b.SetPos32((int)(100*_emmisive_b));
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._b != _emmisive_b )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetEmmisive()._b= _emmisive_b;
      }
		}
    SetColorEmm();
	}
}


void CMatView::OnEnChangeEditEmmA()
{
  UpdateData(TRUE);
	_emmisive_a= Round2(_emmisive_a);
  if ( _emmisive_a >= 0 && _emmisive_a <= 1 )
	{
    _spin_emm_a.SetPos32((int)(100*_emmisive_a));
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._a != _emmisive_a )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetEmmisive()._a= _emmisive_a;
      }
		}
	}
}


void CMatView::OnEnChangeEditSpcR()
{
  UpdateData(TRUE);
	_specular_r= Round2(_specular_r);
  if ( _specular_r >= 0 && _specular_r <= 1 )
	{
    _spin_spc_r.SetPos32((int)(100*_specular_r));
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecular()._r != _specular_r )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetSpecular()._r= _specular_r;
      }
		}
    SetColorSpc();
	}
}


void CMatView::OnEnChangeEditSpcG()
{
  UpdateData(TRUE);
	_specular_g= Round2(_specular_g);
  if ( _specular_g >= 0 && _specular_g <= 1 )
	{
    _spin_spc_g.SetPos32((int)(100*_specular_g));
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecular()._g != _specular_g )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetSpecular()._g= _specular_g;
      }
		}
    SetColorSpc();
	}
}


void CMatView::OnEnChangeEditSpcB()
{
  UpdateData(TRUE);
	_specular_b= Round2(_specular_b);
  if ( _specular_b >= 0 && _specular_b <= 1 )
	{
    _spin_spc_b.SetPos32((int)(100*_specular_b));
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecular()._b != _specular_b )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetSpecular()._b= _specular_b;
      }
		}
    SetColorSpc();
	}
}


void CMatView::OnEnChangeEditSpcA()
{
  UpdateData(TRUE);
	_specular_a= Round2(_specular_a);
  if ( _specular_a >= 0 && _specular_a <= 1 )
	{
    _spin_spc_a.SetPos32((int)(100*_specular_a));
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecular()._a != _specular_a )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).GetSpecular()._a= _specular_a;
      }
		}
	}
}


void CMatView::OnEnChangeEditSpp()
{
  UpdateData(TRUE);
	_specularPower= Round2(_specularPower);
  if ( _specularPower >= 0 )
	{
    _spin_spp.SetPos32((int)(100*_specularPower));
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecularPower() != _specular_r )
    	{
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
    	{
        doc->_mats->GetMat(sel[i]).SetSpecularPower(_specularPower);
      }
		}
	}
}


void CMatView::OnCbnEditchangeComboPsi()
{
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetPixelShaderID() != (RString)_PixelShaderID )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    }
  }
  doc->_mats->HistoryAdd();

  int ind_cfg_stage= InCfgStage((RString)_PixelShaderID);
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      if ( ind_cfg_stage > -1 )
        doc->_mats->GetMat(sel[i]).SetMat(_cfg_stage_file[ind_cfg_stage]);
      else
      {
        doc->_mats->GetMat(sel[i]).SetPixelShaderID((RString)_PixelShaderID);
        doc->_mats->GetMat(sel[i]).ProtectStages(FALSE);
        CheckStageEnable();
      }
    }
  }
  if ( ind_cfg_stage > -1 )
  {
    LoadMaterial();
    SetStages();
  }
}


void CMatView::OnCbnSelchangeComboPsi()
{
  _combo_PSI.GetLBText(_combo_PSI.GetCurSel(),_PixelShaderID);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    //if ( doc->_mats->GetMat(sel[i]).GetPixelShaderID() != (RString)_PixelShaderID )
    //{
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    //}
  }
  doc->_mats->HistoryAdd();

  int ind_cfg_stage= InCfgStage((RString)_PixelShaderID);
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      if ( ind_cfg_stage > -1 )
        doc->_mats->GetMat(sel[i]).SetMat(_cfg_stage_file[ind_cfg_stage]);
      else
      {
        doc->_mats->GetMat(sel[i]).SetPixelShaderID((RString)_PixelShaderID);
        doc->_mats->GetMat(sel[i]).ProtectStages(FALSE);
        CheckStageEnable();
      }
    }
  }
  if ( ind_cfg_stage > -1 )
    LoadMaterial();
  SetStages();
}


void CMatView::OnCbnEditchangeComboVsi()
{
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetVertexShaderID() != (RString)_VertexShaderID )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    }
  }
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      doc->_mats->GetMat(sel[i]).SetVertexShaderID((RString)_VertexShaderID);
    }
  }
}


void CMatView::OnCbnSelchangeComboVsi()
{
  _combo_VSI.GetLBText(_combo_VSI.GetCurSel(),_VertexShaderID);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetVertexShaderID() != (RString)_VertexShaderID )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    }
  }
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      doc->_mats->GetMat(sel[i]).SetVertexShaderID((RString)_VertexShaderID);
    }
  }
}


//---------------------------   Stage   ------------------------------------------------


//prekliknuti na jiny tab
void CMatView::OnTcnSelchangeTabStages(NMHDR *pNMHDR, LRESULT *pResult)
{
  //sem se dostanu, jen pokud je vybran prave jeden material, v jinem pripade to je disable
	if ( doc->_mats->GetNselmat() != 1 )
		return;
  int cur= _tab_stage.GetCurSel();

  doc->_mats->GetFirstSelMatCurLod().SetCurStage(cur);
  LoadMaterial();
  CheckStageEnable();
  *pResult = 0;
}


void CMatView::OnBnClickedCheckEnableStage()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
    {
      if (  doc->_mats->GetMat(sel[i]).GetCurStage().Enable() != _check_stage  )
  	  {
        doc->_mats->GetMat(sel[i]).GetCurStage().Enable(_check_stage);
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
    }
  }
  CheckStageEnable();          //zapne nebo vypne stage podle _check_stage
  //Update();
}


//add new stage
void CMatView::OnBnClickedBNewStage()
{
  //sem se dostanu, jen pokud je vybran prave jeden material, v jinem pripade to je disable
	if ( doc->_mats->GetNselmat() != 1 )
		return;

  Stage stage;

  //add old state to history
  doc->_mats->GetFirstSelMatCurLod().Changed(TRUE);
  doc->_mats->HistoryAdd();

  stage.SetName("Stage");
  if ( doc->_mats->GetFirstSelMatCurLod().AddStage(stage) )
	{
    _tab_stage.InsertItem(_tab_stage.GetItemCount(), stage.GetName());  //doc->_mats->GetFirstSelMatCurLod().GetNstage()-1
    //doc->_mats->GetFirstSelMatCurLod().Changed(TRUE);
    LoadMaterial();

    _tab_stage.SetCurSel(_tab_stage.GetItemCount()-1);
    //enable, disable buttons
    EnableStage();
    if ( doc->_mats->GetFirstSelMatCurLod().GetNstage() == MAX_STAGES )
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), FALSE);
    if ( doc->_mats->GetFirstSelMatCurLod().GetNstage() == 1 )
      EnableStage();

    Update();
	}
  else
    MessageBox("The number of stages is maximum.");
}


void CMatView::OnBnClickedBDeleteStage()
{
  //sem se dostanu, jen pokud je vybran prave jeden material, v jinem pripade to je disable
	if ( doc->_mats->GetNselmat() != 1 )
		return;

  //add old state to history
  doc->_mats->GetFirstSelMatCurLod().Changed(TRUE);
  doc->_mats->HistoryAdd();

	if ( doc->_mats->GetFirstSelMatCurLod().RemoveStage() ) 
	{
    _tab_stage.DeleteItem(_tab_stage.GetCurSel());
    //doc->_mats->GetFirstSelMatCurLod().Changed(TRUE);
		LoadMaterial();
    _tab_stage.SetCurSel(0);        //nastavuje se na 1. stage, to same se deje u RemoveStage()
    //enable, disable buttons
    if ( doc->_mats->GetFirstSelMatCurLod().GetNstage() == 0 )
    {
      DisableStage();
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), TRUE);   //DisableStage() vypne i buton na pridavani
    }
    if ( doc->_mats->GetFirstSelMatCurLod().GetNstage() == MAX_STAGES-1 )
      ::EnableWindow(::GetDlgItem(m_hWnd, IDC_B_NEW_STAGE), TRUE);

    Update();
  }
	else
		MessageBox("No stages.");
}


void CMatView::OnEnChangeEditStageName()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();

  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
    {
      if (  doc->_mats->GetMat(sel[i]).GetCurStage().Enable()  &&
            doc->_mats->GetMat(sel[i]).GetCurStage().GetName() != _stage_name
         )
  	  {
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
    }
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
        doc->_mats->GetMat(sel[i]).GetCurStage().SetName((RString)_stage_name);
    }
  }
  doc->_mats->SaveTemps();
  SetFlags(TRUE);

  //tab-name update
  TCITEM item;

  if ( _tab_stage.GetItem(_tab_stage.GetCurSel(), &item) )
  {
    item.mask = TCIF_TEXT;
    item.pszText= _stage_name.GetBuffer(256);
    _tab_stage.SetItem(_tab_stage.GetCurSel(), &item);
  }
}


void CMatView::OnEnChangeEditTex()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();

  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
    {
      if (  doc->_mats->GetMat(sel[i]).GetCurStage().Enable()  &&
            doc->_mats->GetMat(sel[i]).GetCurStage().GetTexture() != _texture
         )
  	  {
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
    }
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
        doc->_mats->GetMat(sel[i]).GetCurStage().SetTexture((RString)_texture);
    }
  }
}


void CMatView::OnBnClickedBBrowsTexture()
{
  TCHAR texture_name[256]= {0};
  OPENFILENAME ofd;

  ZeroMemory(&ofd, sizeof(OPENFILENAME));
  ofd.lStructSize= sizeof(OPENFILENAME);
  ofd.lpstrFile= texture_name;
  ofd.hwndOwner= m_hWnd;
  ofd.nMaxFile= sizeof(texture_name);
  ofd.Flags= OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST;
  ofd.lpstrFilter= "paa file (*.paa)\0*.paa\0";               //filtr
  ofd.lpstrInitialDir= (const char*) _last_tex_path;             
  if ( !GetOpenFileName(&ofd) ) 
    return;
  WFilePath p(texture_name);
  _last_tex_path= (RString) (p.GetDrive()+p.GetDirectory());
  SaveCfg();                 //zbytecne narocne, smazat az budu hotove ukladani na konci
  _texture= texture_name;
  _texture.MakeLower();
  _texture= _texture.Right(_texture.GetLength()-3);
  UpdateData(FALSE);
  GetDlgItem(IDC_EDIT_TEX)->SendMessage(EM_LINESCROLL, 100, 0);
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
    {
      if (  doc->_mats->GetMat(sel[i]).GetCurStage().Enable()  &&
            doc->_mats->GetMat(sel[i]).GetCurStage().GetTexture() != _texture
         )
  	  {
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
    }
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
        doc->_mats->GetMat(sel[i]).GetCurStage().SetTexture((RString)_texture);
    }
  }
}


void CMatView::OnBnClickedBViewTexture()
{
  RString path(_mat_path, (const char*)_texture);
  ShellExecute(m_hWnd, "open", "c:\\bis\\tex\\texView.exe", path, NULL, SW_SHOW);
}


void CMatView::OnBnClickedBExploreTexture()
{
  STARTUPINFO sui;
  PROCESS_INFORMATION pi;
  ZeroMemory(&sui, sizeof(sui));
  ZeroMemory(&pi, sizeof(pi));
  sui.dwFlags= STARTF_USESHOWWINDOW;
  sui.wShowWindow= SW_SHOW;
  char str[256];
  sprintf(str, "explorer.exe /select, \"%s\"", 
    (const char*) RString (_mat_path, doc->_mats->GetFirstSelMatCurLod().GetCurStage().GetTexture()));

  CreateProcess(NULL, (LPSTR) str, NULL, NULL, FALSE, 0, NULL, NULL, &sui, &pi);
}


void CMatView::OnCbnEditchangeComboUvs()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
    {
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() && doc->_mats->GetMat(sel[i]).GetCurStage().GetUVSource() != (RString)_uvSource )
  	  {
        if ( _uvSource != (RString)"none" )
        {
          doc->_mats->GetMat(sel[i]).GetCurStage().UVNone(FALSE);
          EnableUVTransform();
        }
        else
        {
          doc->_mats->GetMat(sel[i]).GetCurStage().UVNone(TRUE);
          DisableUVTransform();
        }
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
    }
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
        doc->_mats->GetMat(sel[i]).GetCurStage().SetUVSource((RString)_uvSource);
    }
  }
}


void CMatView::OnCbnSelchangeComboUvs()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  _combo_uvTex.GetLBText(_combo_uvTex.GetCurSel(),_uvSource);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
    {
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() && doc->_mats->GetMat(sel[i]).GetCurStage().GetUVSource() != (RString)_uvSource )
      {
        if ( _uvSource != (RString)"none" )
        {
          doc->_mats->GetMat(sel[i]).GetCurStage().UVNone(FALSE);
          EnableUVTransform();
        }
        else
        {
          doc->_mats->GetMat(sel[i]).GetCurStage().UVNone(TRUE);
          DisableUVTransform();
        }
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
    }
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      doc->_mats->GetMat(sel[i]).GetCurStage().SetUVSource((RString)_uvSource);
    }
  }
}


void CMatView::OnEnChangeEditAs0()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[0] != _aside_0 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[0]= _aside_0;
  }
}


void CMatView::OnEnChangeEditAs1()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[1] != _aside_1 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[1]= _aside_1;
  }
}


void CMatView::OnEnChangeEditAs2()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[2] != _aside_2 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetAside()[2]= _aside_2;
  }
}


void CMatView::OnEnChangeEditUp0()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[0] != _up_0 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[0]= _up_0;
  }
}


void CMatView::OnEnChangeEditUp1()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[1] != _up_1 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[1]= _up_1;
  }
}


void CMatView::OnEnChangeEditUp2()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[2] != _up_2 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).Changed() )
          doc->_mats->GetMat(sel[i]).GetCurStage().GetUp()[2]= _up_2;
  }
}


void CMatView::OnEnChangeEditDi0()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[0] != _dir_0 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[0]= _dir_0;
  }
}


void CMatView::OnEnChangeEditDi1()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[1] != _dir_1 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[1]= _dir_1;
  }
}


void CMatView::OnEnChangeEditDi2()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[2] != _dir_2 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetDir()[2]= _dir_2;
  }
}


void CMatView::OnEnChangeEditPs0()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[0] != _pos_0 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[0]= _pos_0;
  }
}


void CMatView::OnEnChangeEditPs1()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[1] != _pos_1 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[1]= _pos_1;
  }
}


void CMatView::OnEnChangeEditPs2()
{
  //zmena probehne u vsech oznacenych materialu u aktualni stage, pokud je enable
  UpdateData(TRUE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).GetNstage() )
      if ( doc->_mats->GetMat(sel[i]).GetCurStage().Enable() )
        if ( doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[2] != _pos_2 )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);
  }
  //add old state to history
  doc->_mats->HistoryAdd();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
      doc->_mats->GetMat(sel[i]).GetCurStage().GetPos()[2]= _pos_2;
  }
}


//---------------------------   Spins   ---------------------------------------------------


void CMatView::OnDeltaposSpinAmbR(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_ambient_r= s;
    if ( _check_lock_ambdif )
    {
      _diffuse_r= _ambient_r;
      _spin_dif_r.SetPos32((int)(100*_diffuse_r+0.0001));
    }
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetAmbient()._r != _ambient_r )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);            
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._r != _diffuse_r )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);         
 		}
    if (  _spin_amb_r.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
      }
 		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_DIF_R, _diffuse_r);
    }
  }
  _spin_amb_r.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinAmbG(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_ambient_g= s;
    if ( _check_lock_ambdif )
    {
      _diffuse_g= _ambient_g;
      _spin_dif_g.SetPos32((int)(100*_diffuse_g+0.0001));
    }
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetAmbient()._g != _ambient_g )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);            
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._g != _diffuse_g )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);         
 		}
    if (  _spin_amb_g.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
      }
 		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_DIF_G, _diffuse_g);
    }
  }
  _spin_amb_g.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinAmbB(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_ambient_b= s;
    if ( _check_lock_ambdif )
    {
      _diffuse_b= _ambient_b;
      _spin_dif_b.SetPos32((int)(100*_diffuse_b+0.0001));
    }
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetAmbient()._b != _ambient_b )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);            
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._b != _diffuse_b )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);         
 		}
    if (  _spin_amb_b.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
      }
 		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_DIF_B, _diffuse_b);
    }
  }
  _spin_amb_b.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinAmbA(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_ambient_a= s;
    if ( _check_lock_ambdif )
    {
      _diffuse_a= _ambient_a;
      _spin_dif_a.SetPos32((int)(100*_diffuse_a+0.0001));
    }
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetAmbient()._a != _ambient_a )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);            
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._a != _diffuse_a )
          doc->_mats->GetMat(sel[i]).Changed(TRUE);         
 		}
    if (  _spin_amb_a.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._a= _ambient_a;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetDiffuse()._a= _diffuse_a;
      }
 		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_DIF_A, _diffuse_a);
    }
  }
  _spin_amb_a.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinDifR(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_diffuse_r= s;
    if ( _check_lock_ambdif )
    {
      _ambient_r= _diffuse_r;
      _spin_amb_r.SetPos32((int)(100*_ambient_r+0.0001));
    }
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._r != _diffuse_r )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetAmbient()._r != _ambient_r )
           doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_dif_r.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
      }
 		}
    SetColorDif();
    if ( _check_lock_ambdif )
    {
      SetColorAmb();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_AMB_R, _ambient_r);
    }
  }
  _spin_dif_r.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinDifG(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_diffuse_g= s;
    if ( _check_lock_ambdif )
    {
      _ambient_g= _diffuse_g;
      _spin_amb_g.SetPos32((int)(100*_ambient_g));
    }
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._g != _diffuse_g )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetAmbient()._g != _ambient_g )
           doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_dif_g.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
      }
 		}
    SetColorDif();
    if ( _check_lock_ambdif )
    {
      SetColorAmb();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_AMB_G, _ambient_g);
    }
  }
  _spin_dif_g.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinDifB(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_diffuse_b= s;
    if ( _check_lock_ambdif )
    {
      _ambient_b= _diffuse_b;
      _spin_amb_b.SetPos32((int)(100*_ambient_b));
    }
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._b != _diffuse_b )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetAmbient()._b != _ambient_b )
           doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_dif_b.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
      }
 		}
    SetColorDif();
    if ( _check_lock_ambdif )
    {
      SetColorAmb();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_AMB_B, _ambient_b);
    }
  }
  _spin_dif_b.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinDifA(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_diffuse_a= s;
    if ( _check_lock_ambdif )
    {
      _ambient_a= _diffuse_a;
      _spin_amb_a.SetPos32((int)(100*_ambient_a));
    }
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetDiffuse()._a != _diffuse_a )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_lock_ambdif )
        if ( doc->_mats->GetMat(sel[i]).GetAmbient()._a != _ambient_a )
           doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_dif_a.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._a= _diffuse_a;
        if ( _check_lock_ambdif )
          doc->_mats->GetMat(sel[i]).GetAmbient()._a= _ambient_a;
      }
 		}
    SetColorDif();
    if ( _check_lock_ambdif )
    {
      SetColorAmb();
      CDataExchange dx(this, FALSE);
      DDX_Text(&dx, IDC_EDIT_AMB_A, _ambient_a);
    }
  }
  _spin_dif_a.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinFodR(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_forcedDiffuse_r= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if (doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r != _forcedDiffuse_r )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_fod_r.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r= _forcedDiffuse_r;
 		}
    SetColorFod();
  }
  _spin_fod_r.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinFodG(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_forcedDiffuse_g= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g != _forcedDiffuse_g )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_fod_g.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g= _forcedDiffuse_g;
 		}
    SetColorFod();
  }
  _spin_fod_g.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinFodB(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_forcedDiffuse_b= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b != _forcedDiffuse_b )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_fod_b.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b= _forcedDiffuse_b;
 		}
    SetColorFod();
  }
  _spin_fod_b.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinFodA(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_forcedDiffuse_a= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._a != _forcedDiffuse_a )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_fod_a.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._a= _forcedDiffuse_a;
 		}
  }
  _spin_fod_a.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinEmmR(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_emmisive_r= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._r != _emmisive_r )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_emm_r.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetEmmisive()._r= _emmisive_r;
 		}
    SetColorEmm();
  }
  _spin_emm_r.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinEmmG(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_emmisive_g= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._g != _emmisive_g )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_emm_g.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetEmmisive()._g= _emmisive_g;
 		}
    SetColorEmm();
  }
  _spin_emm_g.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinEmmB(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_emmisive_b= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._b != _emmisive_b )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if (  _spin_emm_b.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetEmmisive()._b= _emmisive_b;
 		}
    SetColorEmm();
  }
  _spin_emm_b.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinEmmA(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_emmisive_a= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetEmmisive()._a != _emmisive_a )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if ( _spin_emm_a.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
        doc->_mats->GetMat(sel[i]).GetEmmisive()._a= _emmisive_a;
 		}
  }
  _spin_emm_a.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinSpcR(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_specular_r= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecular()._r != _specular_r )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if ( _spin_spc_r.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetSpecular()._r= _specular_r;
 		}
    SetColorSpc();
  }
  _spin_spc_r.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinSpcG(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_specular_g= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecular()._g != _specular_g )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if ( _spin_spc_g.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetSpecular()._g= _specular_g;
 		}
    SetColorSpc();
  }
  _spin_spc_g.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinSpcB(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_specular_b= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecular()._b != _specular_b )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if ( _spin_spc_b.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetSpecular()._b= _specular_b;
 		}
    SetColorSpc();
  }
  _spin_spc_b.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinSpcA(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
  	_specular_a= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecular()._b != _specular_a )
         doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if ( _spin_spc_a.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
         doc->_mats->GetMat(sel[i]).GetSpecular()._a= _specular_a;
 		}
  }
  _spin_spc_a.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinSpp(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 )
  {
  	_specularPower= s;
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecularPower() != _specularPower )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
 		}
    if ( _spin_spp.Starting() )
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
        doc->_mats->GetMat(sel[0]).SetSpecularPower(_specularPower);
 		}
  }
  _spin_spp.RemoveStartEndFlag();
  *pResult = 0;
}


//---------------------------   Big spins   -------------------------------------------------


void CMatView::OnDeltaposSpinAmb(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  int move_i  = -pNMUpDown->iDelta;          //naopak, doprava zvetsovat
  float move_f= 0.01 * move_i;
  BOOL change= FALSE;

  if ( _check_amb_r )
  {
    if (  !( (_ambient_r > 0.9999  &&  move_i == 1)  ||  (_ambient_r < 0.0001  &&  move_i == -1) )  )
    {
      _ambient_r+= move_f;
      _ambient_r= Round2(_ambient_r);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_amb_r.SetPos(_spin_amb_r.GetPos()+move_i);
      change= TRUE;
    }
    if ( _check_lock_ambdif  &&  _check_dif_r )
    {
      _diffuse_r= _ambient_r;
      _spin_dif_r.SetPos(_spin_amb_r.GetPos());
      change= TRUE;
    }
  }
  if ( _check_amb_g )
  {
    if (  !( (_ambient_g > 0.9999  &&  move_i == 1)  ||  (_ambient_g < 0.0001  &&  move_i == -1) )  )
    {
      _ambient_g+= move_f;
      _ambient_g= Round2(_ambient_g);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_amb_g.SetPos(_spin_amb_g.GetPos()+move_i);
      change= TRUE;
    }
    if ( _check_lock_ambdif  &&  _check_dif_g )
    {
      _diffuse_g= _ambient_g;
      _spin_dif_g.SetPos(_spin_amb_g.GetPos());
      change= TRUE;
    }
  }
  if ( _check_amb_b )
  {
    if (  !( (_ambient_b > 0.9999  &&  move_i == 1)  ||  (_ambient_b < 0.0001  &&  move_i == -1) )  )
    {
      _ambient_b+= move_f;
      _ambient_b= Round2(_ambient_b);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_amb_b.SetPos(_spin_amb_b.GetPos()+move_i);
      change= TRUE;
    }
    if ( _check_lock_ambdif  &&  _check_dif_b )
    {
      _diffuse_b= _ambient_b;
      _spin_dif_b.SetPos(_spin_amb_b.GetPos());
      change= TRUE;
    }
  }
  if ( _check_amb_a )
  {
    if (  !( (_ambient_a > 0.9999  &&  move_i == 1)  ||  (_ambient_a < 0.0001  &&  move_i == -1) )  )
    {
      _ambient_a+= move_f;
      _ambient_a= Round2(_ambient_a);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_amb_a.SetPos(_spin_amb_a.GetPos()+move_i);
      change= TRUE;
    }
    if ( _check_lock_ambdif  &&  _check_dif_a )
    {
      _diffuse_a= _ambient_a;
      _spin_dif_a.SetPos(_spin_amb_a.GetPos());
      change= TRUE;
    }
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();

    if ( _spin_amb.Starting() )
    {
      for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    }

    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
      doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
      doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
      doc->_mats->GetMat(sel[i]).GetAmbient()._a= _ambient_a;
      if ( _check_lock_ambdif )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
        doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
        doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
        doc->_mats->GetMat(sel[i]).GetDiffuse()._a= _diffuse_a;
      }
    }
    SetColorAmb();
    if ( _check_lock_ambdif )
      SetColorDif();
  }
  _spin_amb.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinDif(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  int move_i  = -pNMUpDown->iDelta;          //naopak, doprava zvetsovat
  float move_f= 0.01 * move_i;
  BOOL change= FALSE;

  if ( _check_dif_r )
  {
    if (  !( (_diffuse_r > 0.9999  &&  move_i == 1)  ||  (_diffuse_r < 0.0001  &&  move_i == -1) )  )
    {
      _diffuse_r+= move_f;
      _diffuse_r= Round2(_diffuse_r);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_dif_r.SetPos(_spin_dif_r.GetPos()+move_i);
      change= TRUE;
    }
    if ( _check_lock_ambdif  &&  _check_amb_r )
    {
      _ambient_r= _diffuse_r;
      _spin_amb_r.SetPos(_spin_dif_r.GetPos());
      change= TRUE;
    }
  }
  if ( _check_dif_g )
  {
    if (  !( (_diffuse_g > 0.9999  &&  move_i == 1)  ||  (_diffuse_g < 0.0001  &&  move_i == -1) )  )
    {
      _diffuse_g+= move_f;
      _diffuse_g= Round2(_diffuse_g);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_dif_g.SetPos(_spin_dif_g.GetPos()+move_i);
      change= TRUE;
    }
    if ( _check_lock_ambdif  &&  _check_amb_g )
    {
      _ambient_g= _diffuse_g;
      _spin_amb_g.SetPos(_spin_dif_g.GetPos());
      change= TRUE;
    }
  }
  if ( _check_dif_b )
  {
    if (  !( (_diffuse_b > 0.9999  &&  move_i == 1)  ||  (_diffuse_b < 0.0001  &&  move_i == -1) )  )
    {
      _diffuse_b+= move_f;
      _diffuse_b= Round2(_diffuse_b);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_dif_b.SetPos(_spin_dif_b.GetPos()+move_i);
      change= TRUE;
    }
    if ( _check_lock_ambdif  &&  _check_amb_b )
    {
      _ambient_b= _diffuse_b;
      _spin_amb_b.SetPos(_spin_dif_b.GetPos());
      change= TRUE;
    }
  }
  if ( _check_dif_a )
  {
    if (  !( (_diffuse_a > 0.9999  &&  move_i == 1)  ||  (_diffuse_a < 0.0001  &&  move_i == -1) )  )
    {
      _diffuse_a+= move_f;
      _diffuse_a= Round2(_diffuse_a);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_dif_a.SetPos(_spin_dif_a.GetPos()+move_i);
      change= TRUE;
    }
    if ( _check_lock_ambdif  &&  _check_amb_a )
    {
      _ambient_a= _diffuse_a;
      _spin_amb_a.SetPos(_spin_dif_a.GetPos());
      change= TRUE;
    }
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();

    if ( _spin_dif.Starting() )
    {
      for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    }

    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
      doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
      doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
      doc->_mats->GetMat(sel[i]).GetDiffuse()._a= _diffuse_a;
      if ( _check_lock_ambdif )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
        doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
        doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
        doc->_mats->GetMat(sel[i]).GetAmbient()._a= _ambient_a;
      }
    }
    SetColorDif();
    if ( _check_lock_ambdif )
      SetColorAmb();
  }
  _spin_dif.RemoveStartEndFlag();
  *pResult = 0;
}

void CMatView::OnDeltaposSpinFod(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  int move_i  = -pNMUpDown->iDelta;          //naopak, doprava zvetsovat
  float move_f= 0.01 * move_i;
  BOOL change= FALSE;

  if ( _check_fod_r )
  {
    if (  !( (_forcedDiffuse_r > 0.9999  &&  move_i == 1)  ||  (_forcedDiffuse_r < 0.0001  &&  move_i == -1) )  )
    {
      _forcedDiffuse_r+= move_f;
      _forcedDiffuse_r= Round2(_forcedDiffuse_r);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_fod_r.SetPos(_spin_fod_r.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( _check_fod_g )
  {
    if (  !( (_forcedDiffuse_g > 0.9999  &&  move_i == 1)  ||  (_forcedDiffuse_g < 0.0001  &&  move_i == -1) )  )
    {
      _forcedDiffuse_g+= move_f;
      _forcedDiffuse_g= Round2(_forcedDiffuse_g);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_fod_g.SetPos(_spin_fod_g.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( _check_fod_b )
  {
    if (  !( (_forcedDiffuse_b > 0.9999  &&  move_i == 1)  ||  (_forcedDiffuse_b < 0.0001  &&  move_i == -1) )  )
    {
      _forcedDiffuse_b+= move_f;
      _forcedDiffuse_b= Round2(_forcedDiffuse_b);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_fod_b.SetPos(_spin_fod_b.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( _check_fod_a )
  {
    if (  !( (_forcedDiffuse_a > 0.9999  &&  move_i == 1)  ||  (_forcedDiffuse_a < 0.0001  &&  move_i == -1) )  )
    {
      _forcedDiffuse_a+= move_f;
      _forcedDiffuse_a= Round2(_forcedDiffuse_a);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_fod_a.SetPos(_spin_fod_a.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();

    if ( _spin_amb.Starting() )
    {
      for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    }

    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r= _forcedDiffuse_r;
      doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g= _forcedDiffuse_g;
      doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b= _forcedDiffuse_b;
      doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._a= _forcedDiffuse_a;
    }
    SetColorFod();
  }
  _spin_amb.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinEmm(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  int move_i  = -pNMUpDown->iDelta;          //naopak, doprava zvetsovat
  float move_f= 0.01 * move_i;
  BOOL change= FALSE;

  if ( _check_emm_r )
  {
    if (  !( (_emmisive_r > 0.9999  &&  move_i == 1)  ||  (_emmisive_r < 0.0001  &&  move_i == -1) )  )
    {
      _emmisive_r+= move_f;
      _emmisive_r= Round2(_emmisive_r);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_emm_r.SetPos(_spin_emm_r.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( _check_emm_g )
  {
    if (  !( (_emmisive_g > 0.9999  &&  move_i == 1)  ||  (_emmisive_g < 0.0001  &&  move_i == -1) )  )
    {
      _emmisive_g+= move_f;
      _emmisive_g= Round2(_emmisive_g);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_emm_g.SetPos(_spin_emm_g.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( _check_emm_b )
  {
    if (  !( (_emmisive_b > 0.9999  &&  move_i == 1)  ||  (_emmisive_b < 0.0001  &&  move_i == -1) )  )
    {
      _emmisive_b+= move_f;
      _emmisive_b= Round2(_emmisive_b);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_emm_b.SetPos(_spin_emm_b.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( _check_emm_a )
  {
    if (  !( (_emmisive_a > 0.9999  &&  move_i == 1)  ||  (_emmisive_a < 0.0001  &&  move_i == -1) )  )
    {
      _emmisive_a+= move_f;
      _emmisive_a= Round2(_emmisive_a);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_emm_a.SetPos(_spin_emm_a.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();

    if ( _spin_amb.Starting() )
    {
      for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    }

    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).GetEmmisive()._r= _emmisive_r;
      doc->_mats->GetMat(sel[i]).GetEmmisive()._g= _emmisive_g;
      doc->_mats->GetMat(sel[i]).GetEmmisive()._b= _emmisive_b;
      doc->_mats->GetMat(sel[i]).GetEmmisive()._a= _emmisive_a;
    }
    SetColorEmm();
  }
  _spin_amb.RemoveStartEndFlag();
  *pResult = 0;
}


void CMatView::OnDeltaposSpinSpc(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  int move_i  = -pNMUpDown->iDelta;          //naopak, doprava zvetsovat
  float move_f= 0.01 * move_i;
  BOOL change= FALSE;

  if ( _check_spc_r )
  {
    if (  !( (_specular_r > 0.9999  &&  move_i == 1)  ||  (_specular_r < 0.0001  &&  move_i == -1) )  )
    {
      _specular_r+= move_f;
      _specular_r= Round2(_specular_r);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_spc_r.SetPos(_spin_spc_r.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( _check_spc_g )
  {
    if (  !( (_specular_g > 0.9999  &&  move_i == 1)  ||  (_specular_g < 0.0001  &&  move_i == -1) )  )
    {
      _specular_g+= move_f;
      _specular_g= Round2(_specular_g);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_spc_g.SetPos(_spin_spc_g.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( _check_spc_b )
  {
    if (  !( (_specular_b > 0.9999  &&  move_i == 1)  ||  (_specular_b < 0.0001  &&  move_i == -1) )  )
    {
      _specular_b+= move_f;
      _specular_b= Round2(_specular_b);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_spc_b.SetPos(_spin_spc_b.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( _check_spc_a )
  {
    if (  !( (_specular_a > 0.9999  &&  move_i == 1)  ||  (_specular_a < 0.0001  &&  move_i == -1) )  )
    {
      _specular_a+= move_f;
      _specular_a= Round2(_specular_a);              //casem se kumuluje chyba, proto prubezne zaokrouhluju
      _spin_spc_a.SetPos(_spin_spc_a.GetPos()+move_i);
      change= TRUE;
    }
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();

    if ( _spin_amb.Starting() )
    {
      for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      doc->_mats->HistoryAdd();                                 //ulozim do historie
    }

    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).GetSpecular()._r= _specular_r;
      doc->_mats->GetMat(sel[i]).GetSpecular()._g= _specular_g;
      doc->_mats->GetMat(sel[i]).GetSpecular()._b= _specular_b;
      doc->_mats->GetMat(sel[i]).GetSpecular()._a= _specular_a;
    }
    SetColorSpc();
  }
  _spin_amb.RemoveStartEndFlag();
  *pResult = 0;
}


//---------------------------   Checks   ------------------------------------------------------


void CMatView::OnBnClickedCheckAmbR()
{
  UpdateData(TRUE);
  if ( _check_lock_ambdif )
    _check_dif_r= _check_amb_r;
  UpdateData(FALSE);
}


void CMatView::OnBnClickedCheckAmbG()
{
  UpdateData(TRUE);
  if ( _check_lock_ambdif )
    _check_dif_g= _check_amb_g;
  UpdateData(FALSE);
}


void CMatView::OnBnClickedCheckAmbB()
{
  UpdateData(TRUE);
  if ( _check_lock_ambdif )
    _check_dif_b= _check_amb_b;
  UpdateData(FALSE);
}


void CMatView::OnBnClickedCheckAmbA()
{
  UpdateData(TRUE);
  if ( _check_lock_ambdif )
    _check_dif_a= _check_amb_a;
  UpdateData(FALSE);
}


void CMatView::OnBnClickedCheckLockAmbDif()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckDifR()
{
  UpdateData(TRUE);
  if ( _check_lock_ambdif )
    _check_amb_r= _check_dif_r;
  UpdateData(FALSE);
}


void CMatView::OnBnClickedCheckDifG()
{
  UpdateData(TRUE);
  if ( _check_lock_ambdif )
    _check_amb_g= _check_dif_g;
  UpdateData(FALSE);
}


void CMatView::OnBnClickedCheckDifB()
{
  UpdateData(TRUE);
  if ( _check_lock_ambdif )
    _check_amb_b= _check_dif_b;
  UpdateData(FALSE);
}


void CMatView::OnBnClickedCheckDifA()
{
  UpdateData(TRUE);
  if ( _check_lock_ambdif )
    _check_amb_a= _check_dif_a;
  UpdateData(FALSE);
}


void CMatView::OnBnClickedCheckFodR()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckFodG()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckFodB()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckFodA()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckEmmR()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckEmmG()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckEmmB()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckEmmA()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckSpcR()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckSpcG()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckSpcB()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckSpcA()
{
  UpdateData(TRUE);
}


void CMatView::OnBnClickedCheckDisUnsMat()
{
  UpdateData(TRUE);
  DisableUnselectedMaterials();
}


//---------------------------   Sliders   -----------------------------------------------------


/*
void CMatView::OnNMThemeChangedSliderAmb(NMHDR *pNMHDR, LRESULT *pResult)
{
  // This feature requires Windows XP or greater.
  // The symbol _WIN32_WINNT must be >= 0x0501.

  //sem to nikdy nejde ikdyz nastavim _WIN32_WINNT 0x0501

  BOOL change= FALSE;
  int pos_i= _slider_amb.GetPos();
  float pos_f= 0.01 * (float) pos_i;

  if ( _check_amb_r )
  {
    _ambient_r= pos_f;
    if ( _check_lock_ambdif  &&  _check_dif_r )
      _diffuse_r= _ambient_r;
    change= TRUE;
  }
  if ( _check_amb_g )
  {
    _ambient_g= pos_f;
    if ( _check_lock_ambdif  &&  _check_dif_g )
      _diffuse_g= _ambient_g;
    change= TRUE;
  }
  if ( _check_amb_b )
  {
    _ambient_b= pos_f;
    if ( _check_lock_ambdif  &&  _check_dif_b )
      _diffuse_b= _ambient_b;
    change= TRUE;
  }
  if ( _check_amb_a )
  {
    _ambient_a= pos_f;
    if ( _check_lock_ambdif  &&  _check_dif_a )
      _diffuse_a= _ambient_a;
    change= TRUE;
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
		}

    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
        doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
        doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
        doc->_mats->GetMat(sel[i]).GetAmbient()._a= _ambient_a;
        if ( _check_lock_ambdif )
        {
          doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
          doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
          doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
          doc->_mats->GetMat(sel[i]).GetDiffuse()._a= _diffuse_a;
        }
      }
		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      _slider_dif.SetPos(pos_i);
    } 
  }
  *pResult = 0;
}
*/


void CMatView::OnNMReleasedcaptureSliderAmb(NMHDR *pNMHDR, LRESULT *pResult)
{
  BOOL change= FALSE;
  int pos_i= _slider_amb.GetPos();
  float pos_f= 0.01 * (float) pos_i;

  if ( _check_amb_r )
  {
    _ambient_r= pos_f;
    _spin_amb_r.SetPos(pos_i);
    if ( _check_lock_ambdif  &&  _check_dif_r )
    {
      _diffuse_r= _ambient_r;
      _spin_dif_r.SetPos(pos_i);
    }
    change= TRUE;
  }
  if ( _check_amb_g )
  {
    _ambient_g= pos_f;
    _spin_amb_g.SetPos(pos_i);
    if ( _check_lock_ambdif  &&  _check_dif_g )
    {
      _diffuse_g= _ambient_g;
      _spin_dif_g.SetPos(pos_i);
    }
    change= TRUE;
  }
  if ( _check_amb_b )
  {
    _ambient_b= pos_f;
    _spin_amb_b.SetPos(pos_i);
    if ( _check_lock_ambdif  &&  _check_dif_b )
    {
      _diffuse_b= _ambient_b;
      _spin_dif_b.SetPos(pos_i);
    }
    change= TRUE;
  }
  if ( _check_amb_a )
  {
    _ambient_a= pos_f;
    _spin_amb_a.SetPos(pos_i);
    if ( _check_lock_ambdif  &&  _check_dif_a )
    {
      _diffuse_a= _ambient_a;
      _spin_dif_a.SetPos(pos_i);
    }
    change= TRUE;
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
      //zbytecne moc testu, ktere vetsinou vyjdou TRUE, protoze napr. _ambient_r je 0.2300001
      //nejaka zmena nastala, to je zrejme z promene 'change'
      /*
      if ( _check_amb_r && (doc->_mats->GetMat(sel[i]).GetAmbient()._r != _ambient_r) )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_amb_g && (doc->_mats->GetMat(sel[i]).GetAmbient()._g != _ambient_g) )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_amb_b && (doc->_mats->GetMat(sel[i]).GetAmbient()._b != _ambient_b) )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_amb_a && (doc->_mats->GetMat(sel[i]).GetAmbient()._a != _ambient_a) )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      */
		}
    doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
        doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
        doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
        doc->_mats->GetMat(sel[i]).GetAmbient()._a= _ambient_a;
        if ( _check_lock_ambdif )
        {
          doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
          doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
          doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
          doc->_mats->GetMat(sel[i]).GetDiffuse()._a= _diffuse_a;
        }
      }
		}
    SetColorAmb();
    if ( _check_lock_ambdif )
    {
      SetColorDif();
      _slider_dif.SetPos(pos_i);
    } 
  }
  *pResult = 0;
}


void CMatView::OnNMReleasedcaptureSliderDif(NMHDR *pNMHDR, LRESULT *pResult)
{
  BOOL change= FALSE;
  int pos_i= _slider_dif.GetPos();
  float pos_f= 0.01 * (float) pos_i;

  if ( _check_dif_r )
  {
    _diffuse_r= pos_f;
    _spin_dif_r.SetPos(pos_i);
    if ( _check_lock_ambdif  &&  _check_amb_r )
    {
      _ambient_r= _diffuse_r;
      _spin_amb_r.SetPos(pos_i);
    }
    change= TRUE;
  }
  if ( _check_dif_g )
  {
    _diffuse_g= pos_f;
    _spin_dif_g.SetPos(pos_i);
    if ( _check_lock_ambdif  &&  _check_amb_g )
    {
      _ambient_g= _diffuse_g;
      _spin_amb_g.SetPos(pos_i);
    }
    change= TRUE;
  }
  if ( _check_dif_b )
  {
    _diffuse_b= pos_f;
    _spin_dif_b.SetPos(pos_i);
    if ( _check_lock_ambdif  &&  _check_amb_b )
    {
      _ambient_b= _diffuse_b;
      _spin_amb_b.SetPos(pos_i);
    }
    change= TRUE;
  }
  if ( _check_dif_a )
  {
    _diffuse_a= pos_f;
    _spin_dif_a.SetPos(pos_i);
    if ( _check_lock_ambdif  &&  _check_amb_a )
    {
      _ambient_a= _diffuse_a;
      _spin_amb_a.SetPos(pos_i);
    }
    change= TRUE;
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
      //zbytecne moc testu, ktere vetsinou vyjdou TRUE, protoze napr. _ambient_r je 0.2300001
      //nejaka zmena nastala, to je zrejme z promene 'change'
      /*
      if ( _check_dif_r && doc->_mats->GetMat(sel[i]).GetDiffuse()._r != _diffuse_r )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_dif_g && doc->_mats->GetMat(sel[i]).GetDiffuse()._g != _diffuse_g )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_dif_b && doc->_mats->GetMat(sel[i]).GetDiffuse()._b != _diffuse_b )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_dif_a && doc->_mats->GetMat(sel[i]).GetDiffuse()._a != _diffuse_a )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      */
		}
    doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
        doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
        doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
        doc->_mats->GetMat(sel[i]).GetDiffuse()._a= _diffuse_a;
        if ( _check_lock_ambdif )
        {
          doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
          doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
          doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
          doc->_mats->GetMat(sel[i]).GetAmbient()._a= _ambient_a;
        }
      }
		}
    SetColorDif();
    if ( _check_lock_ambdif )
    {
      SetColorAmb();
      _slider_amb.SetPos(pos_i);
    }
  }
  *pResult = 0;
}


void CMatView::OnNMReleasedcaptureSliderFod(NMHDR *pNMHDR, LRESULT *pResult)
{
  BOOL change= FALSE;
  int pos_i= _slider_fod.GetPos();
  float pos_f= 0.01 * (float) pos_i;

  if ( _check_fod_r )
  {
    _forcedDiffuse_r= pos_f;
    _spin_fod_r.SetPos(pos_i);
    change= TRUE;
  }
  if ( _check_fod_g )
  {
    _forcedDiffuse_g= pos_f;
    _spin_fod_g.SetPos(pos_i);
    change= TRUE;
  }
  if ( _check_fod_b )
  {
    _forcedDiffuse_b= pos_f;
    _spin_fod_b.SetPos(pos_i);
    change= TRUE;
  }
  if ( _check_fod_a )
  {
    _forcedDiffuse_a= pos_f;
    _spin_fod_a.SetPos(pos_i);
    change= TRUE;
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
      /*
      if ( _check_fod_r && doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r != _forcedDiffuse_r )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_fod_g && doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g != _forcedDiffuse_g )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_fod_b && doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b != _forcedDiffuse_b )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_fod_a && doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._a != _forcedDiffuse_a )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      */
		}
    doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r= _forcedDiffuse_r;
        doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g= _forcedDiffuse_g;
        doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b= _forcedDiffuse_b;
        doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._a= _forcedDiffuse_a;
      }
		}
    SetColorFod();
    //Update();
  }
  *pResult = 0;
}


void CMatView::OnNMReleasedcaptureSliderEmm(NMHDR *pNMHDR, LRESULT *pResult)
{
  BOOL change= FALSE;
  int pos_i= _slider_emm.GetPos();
  float pos_f= 0.01 * (float) pos_i;

  if ( _check_emm_r )
  {
    _emmisive_r= pos_f;
    _spin_emm_r.SetPos(pos_i);
    change= TRUE;
  }
  if ( _check_emm_g )
  {
    _emmisive_g= pos_f;
    _spin_emm_g.SetPos(pos_i);
    change= TRUE;
  }
  if ( _check_emm_b )
  {
    _emmisive_b= pos_f;
    _spin_emm_b.SetPos(pos_i);
    change= TRUE;
  }
  if ( _check_emm_a )
  {
    _emmisive_a= pos_f;
    _spin_emm_a.SetPos(pos_i);
    change= TRUE;
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
      /*
      if ( _check_emm_r && doc->_mats->GetMat(sel[i]).GetEmmisive()._r != _emmisive_r )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_emm_g && doc->_mats->GetMat(sel[i]).GetEmmisive()._g != _emmisive_g )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_emm_b && doc->_mats->GetMat(sel[i]).GetEmmisive()._b != _emmisive_b )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_emm_a && doc->_mats->GetMat(sel[i]).GetEmmisive()._a != _emmisive_a )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      */
		}
    doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetEmmisive()._r= _emmisive_r;
        doc->_mats->GetMat(sel[i]).GetEmmisive()._g= _emmisive_g;
        doc->_mats->GetMat(sel[i]).GetEmmisive()._b= _emmisive_b;
        doc->_mats->GetMat(sel[i]).GetEmmisive()._a= _emmisive_a;
      }
		}
    SetColorEmm();
    //Update();
  }
  *pResult = 0;
}


void CMatView::OnNMReleasedcaptureSliderSpc(NMHDR *pNMHDR, LRESULT *pResult)
{
  BOOL change= FALSE;
  int pos_i= _slider_spc.GetPos();
  float pos_f= 0.01 * (float) pos_i;

  if ( _check_spc_r )
  {
    _specular_r= pos_f;
    _spin_spc_r.SetPos(pos_i);
    change= TRUE;
  }
  if ( _check_spc_g )
  {
    _specular_g= pos_f;
    _spin_spc_g.SetPos(pos_i);
    change= TRUE;
  }
  if ( _check_spc_b )
  {
    _specular_b= pos_f;
    _spin_spc_b.SetPos(pos_i);
    change= TRUE;
  }
  if ( _check_spc_a )
  {
    _specular_a= pos_f;
    _spin_spc_a.SetPos(pos_i);
    change= TRUE;
  }
  if ( change )
  {
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
      /*
      if ( _check_spc_r && doc->_mats->GetMat(sel[i]).GetSpecular()._r != _specular_r )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_spc_g && doc->_mats->GetMat(sel[i]).GetSpecular()._g != _specular_g )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_spc_b && doc->_mats->GetMat(sel[i]).GetSpecular()._b != _specular_b )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      if ( _check_spc_a && doc->_mats->GetMat(sel[i]).GetSpecular()._a != _specular_a )
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      */
		}
    doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).GetSpecular()._r= _specular_r;
        doc->_mats->GetMat(sel[i]).GetSpecular()._g= _specular_g;
        doc->_mats->GetMat(sel[i]).GetSpecular()._b= _specular_b;
        doc->_mats->GetMat(sel[i]).GetSpecular()._a= _specular_a;
      }
		}
    SetColorSpc();
    //Update();
  }
  *pResult = 0;
}


//---------------------------   Buttons   ----------------------------------------------------


void CMatView::OnBnClickedBColAmb()
{
  _color_dlg.m_cc.Flags|= CC_FULLOPEN | CC_RGBINIT | CC_SOLIDCOLOR;
  _color_dlg.m_cc.rgbResult= RGB(_ambient_r*255, _ambient_g*255, _ambient_b*255);
  if ( _color_dlg.DoModal() != IDOK )
    return;
  _ambient_r= Round2((float)GetRValue(_color_dlg.m_cc.rgbResult)/255); 
  _ambient_g= Round2((float)GetGValue(_color_dlg.m_cc.rgbResult)/255);
  _ambient_b= Round2((float)GetBValue(_color_dlg.m_cc.rgbResult)/255);
  _spin_amb_r.SetPos32((int)(_ambient_r*100));
  _spin_amb_g.SetPos32((int)(_ambient_g*100));
  _spin_amb_b.SetPos32((int)(_ambient_b*100));
  if ( _check_lock_ambdif )
  {
    _diffuse_r= Round2((float)GetRValue(_color_dlg.m_cc.rgbResult)/255); 
    _diffuse_g= Round2((float)GetGValue(_color_dlg.m_cc.rgbResult)/255);
    _diffuse_b= Round2((float)GetBValue(_color_dlg.m_cc.rgbResult)/255);
    _spin_dif_r.SetPos32((int)(_diffuse_r*100));
    _spin_dif_g.SetPos32((int)(_diffuse_g*100));
    _spin_dif_b.SetPos32((int)(_diffuse_b*100));
  }
  UpdateData(FALSE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( (doc->_mats->GetMat(sel[i]).GetAmbient()._r != _ambient_r) ||
         (doc->_mats->GetMat(sel[i]).GetAmbient()._g != _ambient_g) ||
         (doc->_mats->GetMat(sel[i]).GetAmbient()._b != _ambient_b)
       )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    }
    if ( _check_lock_ambdif )
    {
      if ( (doc->_mats->GetMat(sel[i]).GetDiffuse()._r != _diffuse_r) ||
           (doc->_mats->GetMat(sel[i]).GetDiffuse()._g != _diffuse_g) ||
           (doc->_mats->GetMat(sel[i]).GetDiffuse()._b != _diffuse_b)
         )
      {
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
    }
  }
  doc->_mats->HistoryAdd();                                 //ulozim do historie
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
      doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
      doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
      if ( _check_lock_ambdif )
      {
        doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
        doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
        doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
      }
    }
  }
  SetColorAmb();
  if ( _check_lock_ambdif )
    SetColorDif();
}


void CMatView::OnBnClickedBColDif()
{
  // TODO: Add your control notification handler code here
  _color_dlg.m_cc.Flags|= CC_FULLOPEN | CC_RGBINIT | CC_SOLIDCOLOR;
  _color_dlg.m_cc.rgbResult= RGB(_diffuse_r*255, _diffuse_g*255, _diffuse_b*255);
  _color_dlg.DoModal();
  _diffuse_r= Round2((float)GetRValue(_color_dlg.m_cc.rgbResult)/255); 
  _diffuse_g= Round2((float)GetGValue(_color_dlg.m_cc.rgbResult)/255);
  _diffuse_b= Round2((float)GetBValue(_color_dlg.m_cc.rgbResult)/255);
  _spin_dif_r.SetPos32((int)(_diffuse_r*100));
  _spin_dif_g.SetPos32((int)(_diffuse_g*100));
  _spin_dif_b.SetPos32((int)(_diffuse_b*100));
  if ( _check_lock_ambdif )
  {
    _ambient_r= Round2((float)GetRValue(_color_dlg.m_cc.rgbResult)/255); 
    _ambient_g= Round2((float)GetGValue(_color_dlg.m_cc.rgbResult)/255);
    _ambient_b= Round2((float)GetBValue(_color_dlg.m_cc.rgbResult)/255);
    _spin_amb_r.SetPos32((int)(_ambient_r*100));
    _spin_amb_g.SetPos32((int)(_ambient_g*100));
    _spin_amb_b.SetPos32((int)(_ambient_b*100));
  }
  UpdateData(FALSE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( (doc->_mats->GetMat(sel[i]).GetDiffuse()._r != _diffuse_r) ||
         (doc->_mats->GetMat(sel[i]).GetDiffuse()._g != _diffuse_g) ||
         (doc->_mats->GetMat(sel[i]).GetDiffuse()._b != _diffuse_b)
       )
    {
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
    }
    if ( _check_lock_ambdif )
    {
      if ( (doc->_mats->GetMat(sel[i]).GetAmbient()._r != _ambient_r) ||
           (doc->_mats->GetMat(sel[i]).GetAmbient()._g != _ambient_g) ||
           (doc->_mats->GetMat(sel[i]).GetAmbient()._b != _ambient_b)
         )
      {
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
    }
  }
  doc->_mats->HistoryAdd();                                 //ulozim do historie
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      doc->_mats->GetMat(sel[i]).GetDiffuse()._r= _diffuse_r;
      doc->_mats->GetMat(sel[i]).GetDiffuse()._g= _diffuse_g;
      doc->_mats->GetMat(sel[i]).GetDiffuse()._b= _diffuse_b;
      if ( _check_lock_ambdif )
      {
        doc->_mats->GetMat(sel[i]).GetAmbient()._r= _ambient_r;
        doc->_mats->GetMat(sel[i]).GetAmbient()._g= _ambient_g;
        doc->_mats->GetMat(sel[i]).GetAmbient()._b= _ambient_b;
      }
    }
  }
  SetColorDif();
  //Update();
}


void CMatView::OnBnClickedBColFod()
{
  // TODO: Add your control notification handler code here
  _color_dlg.m_cc.Flags|= CC_FULLOPEN | CC_RGBINIT | CC_SOLIDCOLOR;
  _color_dlg.m_cc.rgbResult= RGB(_forcedDiffuse_r*255, _forcedDiffuse_g*255, _forcedDiffuse_b*255);
  _color_dlg.DoModal();
  _forcedDiffuse_r= Round2((float)GetRValue(_color_dlg.m_cc.rgbResult)/255); 
  _forcedDiffuse_g= Round2((float)GetGValue(_color_dlg.m_cc.rgbResult)/255);
  _forcedDiffuse_b= Round2((float)GetBValue(_color_dlg.m_cc.rgbResult)/255);
  _spin_fod_r.SetPos32((int)(_forcedDiffuse_r*100));
  _spin_fod_g.SetPos32((int)(_forcedDiffuse_g*100));
  _spin_fod_b.SetPos32((int)(_forcedDiffuse_b*100));
  UpdateData(FALSE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( (doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r != _forcedDiffuse_r) ||
         (doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g != _forcedDiffuse_g) ||
         (doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b != _forcedDiffuse_b)
       )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
     //a se nemeni
  }
  doc->_mats->HistoryAdd();                                 //ulozim do historie
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._r= _forcedDiffuse_r;
      doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._g= _forcedDiffuse_g;
      doc->_mats->GetMat(sel[i]).GetForcedDiffuse()._b= _forcedDiffuse_b;
    }
  }
  SetColorFod();
  //Update();
}


void CMatView::OnBnClickedBColEmm()
{
  // TODO: Add your control notification handler code here
  _color_dlg.m_cc.Flags|= CC_FULLOPEN | CC_RGBINIT | CC_SOLIDCOLOR;
  _color_dlg.m_cc.rgbResult= RGB(_emmisive_r*255, _emmisive_g*255, _emmisive_b*255);
  _color_dlg.DoModal();
  _emmisive_r= Round2((float)GetRValue(_color_dlg.m_cc.rgbResult)/255); 
  _emmisive_g= Round2((float)GetGValue(_color_dlg.m_cc.rgbResult)/255);
  _emmisive_b= Round2((float)GetBValue(_color_dlg.m_cc.rgbResult)/255);
  _spin_emm_r.SetPos32((int)(_emmisive_r*100));
  _spin_emm_g.SetPos32((int)(_emmisive_g*100));
  _spin_emm_b.SetPos32((int)(_emmisive_b*100));
  UpdateData(FALSE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( (doc->_mats->GetMat(sel[i]).GetEmmisive()._r != _emmisive_r) ||
         (doc->_mats->GetMat(sel[i]).GetEmmisive()._g != _emmisive_g) ||
         (doc->_mats->GetMat(sel[i]).GetEmmisive()._b != _emmisive_b)
       )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
     //a se nemeni
  }
  doc->_mats->HistoryAdd();                                 //ulozim do historie
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      doc->_mats->GetMat(sel[i]).GetEmmisive()._r= _emmisive_r;
      doc->_mats->GetMat(sel[i]).GetEmmisive()._g= _emmisive_g;
      doc->_mats->GetMat(sel[i]).GetEmmisive()._b= _emmisive_b;
    }
  }
  SetColorEmm();
  //Update();
}


void CMatView::OnBnClickedBColSpc()
{
  // TODO: Add your control notification handler code here
  _color_dlg.m_cc.Flags|= CC_FULLOPEN | CC_RGBINIT | CC_SOLIDCOLOR;
  _color_dlg.m_cc.rgbResult= RGB(_specular_r*255, _specular_g*255, _specular_b*255);
  _color_dlg.DoModal();
  _specular_r= Round2((float)GetRValue(_color_dlg.m_cc.rgbResult)/255); 
  _specular_g= Round2((float)GetGValue(_color_dlg.m_cc.rgbResult)/255);
  _specular_b= Round2((float)GetBValue(_color_dlg.m_cc.rgbResult)/255);
  _spin_spc_r.SetPos32((int)(_specular_r*100));
  _spin_spc_g.SetPos32((int)(_specular_g*100));
  _spin_spc_b.SetPos32((int)(_specular_b*100));
  UpdateData(FALSE);
  const int* sel= doc->_mats->GetSelMatCurLod();
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( (doc->_mats->GetMat(sel[i]).GetSpecular()._r != _specular_r) ||
         (doc->_mats->GetMat(sel[i]).GetSpecular()._g != _specular_g) ||
         (doc->_mats->GetMat(sel[i]).GetSpecular()._b != _specular_b)
       )
      doc->_mats->GetMat(sel[i]).Changed(TRUE);
     //a se nemeni
  }
  doc->_mats->HistoryAdd();                                 //ulozim do historie
  for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
  {
    if ( doc->_mats->GetMat(sel[i]).Changed() )
    {
      doc->_mats->GetMat(sel[i]).GetSpecular()._r= _specular_r;
      doc->_mats->GetMat(sel[i]).GetSpecular()._g= _specular_g;
      doc->_mats->GetMat(sel[i]).GetSpecular()._b= _specular_b;
    }
  }
  SetColorSpc();
  //Update();
}


void CMatView::OnBnClickedBSpp()
{
  CSppDlg spp;

  spp._edit_spp= _specularPower;
  if ( spp.DoModal() == IDOK )
  {
    _specularPower= spp._edit_spp;
    _spin_spp.SetPos32((int)(100*_specularPower));
    UpdateData(FALSE);
    const int* sel= doc->_mats->GetSelMatCurLod();
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).GetSpecularPower() != spp._edit_spp )
      {
        doc->_mats->GetMat(sel[i]).Changed(TRUE);
      }
		}
    doc->_mats->HistoryAdd();                                 //ulozim do historie
    for ( int i= 0; i < doc->_mats->GetNselmat(); i++ )
    {
      if ( doc->_mats->GetMat(sel[i]).Changed() )
      {
        doc->_mats->GetMat(sel[i]).SetSpecularPower(_specularPower);
      }
		}
  }
}


void CMatView::OnBnClickedBLightAmb()
{
  UpdateLights();
}

void CMatView::OnBnClickedBLightDif()
{
  UpdateLights();
}


//---------------------------   Other Events   ------------------------------------------------


//the change of lod
void CMatView::OnCbnSelchangeComboLod()
{
  doc->_mats->SetCurLod(_list_lod.GetCurSel());
  SetListMat();

  Update();
}


//the change of material
void CMatView::OnLbnSelchangeListMat()
{
  int n= _list_mat.GetSelCount();

  //upravi oznaceni materialu podle _list_mat
  _list_mat.GetSelItems(MAX_MAT, _sel);
  doc->_mats->UnsetAllSelMatCurLod();
  for ( int i= 0; i < n; i++ )
    doc->_mats->SetSelMatCurLod(_sel[i]);

  //neni oznacen zadny material
  if ( !n && _enable )
  {
    Disable();
	  ::EnableWindow(::GetDlgItem(m_hWnd, IDC_LIST_MAT), TRUE);
  }
  else 
  {
    if ( n && !_enable )
      Enable();
    StatusBar("Updating");
    LoadMaterial();
    SetStages();
    SetFlags(TRUE);
  }
  if ( _disable_unselect_mats )
    doc->_mats->EnableSelMat();
  SetEditChangeMat();
  SetColors();
  StatusBar();
}


//on timer
void CMatView::OnTimer(UINT nIDEvent)
{
  switch ( nIDEvent )
  {
  case UPD_TIMER:
    //update model for Buldozer
    StartUpdate();
  break;
  case WEATHER_TIMER:
    //update weather
    if ( _keep_weather )
      SetWeather(_mat_path+SCRIPT_FILE, _time, _overcast, _fog);
    //update lights
    UpdateLights();
  break;
  }
  //base class
  cdxCDynamicFormView::OnTimer(nIDEvent);
}


//---------------------------   messages   -----------------------------------------------


//reaguje na zavriti externiho editoru
BOOL CMatView::OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
  switch ( message )
  {
    case WM_EXTERN_EDITOR:
      doc->_mats->GetMat(_ext_mat).Load(doc->_mats->GetMat(_ext_mat).GetTempName());
      doc->_mats->GetMat(_ext_mat).Changed(TRUE);
      LoadMaterial();
      SetFlags();
      Update();
    break;
    case WM_CLOSE_WEATHER_DLG:
      if ( _weather_dlg )
      {
        delete _weather_dlg;
        _weather_dlg= NULL;
      }
    break;
/*    case ( USER_RELOAD ):                  //USER_RELOAD
    {
      LogF("Message get: USER_RELOAD");
      EndUpdate();
    }
    break; */
    case USER_LIGHT_AMBIENT_R:
    {
      _light_amb_r= Round2(*((float*)&lParam));
    }
    break;
    case USER_LIGHT_AMBIENT_G:
    {
      _light_amb_g= Round2(*((float*)&lParam));
    }
    break;
    case USER_LIGHT_AMBIENT_B:
    {
      _light_amb_b= Round2(*((float*)&lParam));
    }
    break;
    case USER_LIGHT_DIFFUSE_R:
    {
      _light_dif_r= Round2(*((float*)&lParam));
    }
    break;
    case USER_LIGHT_DIFFUSE_G:
    {
      _light_dif_g= Round2(*((float*)&lParam));
    }
    break;
    case USER_LIGHT_DIFFUSE_B:
    {
      _light_dif_b= Round2(*((float*)&lParam));
      LogF("Message get: USER_LIGHT_B");
      SetLights();                       //staci zavolat u posledniho
    }
    break;
    case USER_TIME_REQUEST:
    {
      float t= *((float*)&lParam);
      SetTime(t);
      if ( _weather_dlg )      
      {
        _weather_dlg->SetTime(_time);
        _weather_dlg->UpdateData(FALSE);
      }
    }
    break;
  }

  return cdxCDynamicFormView::OnWndMsg(message, wParam, lParam, pResult); 
}


void CMatView::OnSize(UINT nType, int cx, int cy)
{
  cdxCDynamicFormView::OnSize(nType, cx, cy);

  if ( _p3d_type != P3D_NO ) SetFlags(TRUE);       //updatne listbox (rychla verze, bez zjistovani flagu)
}


