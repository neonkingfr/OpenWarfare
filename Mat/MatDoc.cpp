// MatDoc.cpp : implementation of the CMatDoc class
//

#include "stdafx.h"
#include "Mat.h"

#include "MatDoc.h"
#include "MatView.h"
//#include ".\Matdoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMatDoc

IMPLEMENT_DYNCREATE(CMatDoc, CDocument)

BEGIN_MESSAGE_MAP(CMatDoc, CDocument)
END_MESSAGE_MAP()

CMatDoc* doc;

// CMatDoc construction/destruction

CMatDoc::CMatDoc()
{
	// TODO: add one-time construction code here
  doc= this;
  _obj= new LODObject;
  _obj_backup= NULL;
  _mats= new Materials;
  _mats_backup= NULL;
}

CMatDoc::~CMatDoc()
{
  if ( _obj )
    delete _obj;
  if ( _obj_backup )
    delete _obj_backup;
  if ( _mats )
    delete _mats;
  if ( _mats_backup )
    delete _mats_backup;
}

BOOL CMatDoc::OnNewDocument()
{
  // TODO: Add your specialized code here and/or call the base class

  return CDocument::OnNewDocument();
}

void CMatDoc::Serialize(CArchive& ar)
{
  if (ar.IsStoring())
  {	// storing code
  }
  else
  {	// loading code
  }
}

// CMatDoc diagnostics

#ifdef _DEBUG
void CMatDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMatDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


void CMatDoc::OnCloseDocument()
{
  view->AskToSave(FALSE);

  CDocument::OnCloseDocument();
}
