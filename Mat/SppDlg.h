#pragma once
#include "afxcmn.h"


// CSppDlg dialog

class CSppDlg : public CDialog
{
	DECLARE_DYNAMIC(CSppDlg)

public:
	CSppDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSppDlg();

// Dialog Data
	enum { IDD = IDD_DLG_SPP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
  CSliderCtrl _slider_spp;

public:
  float _edit_spp;

  virtual BOOL OnInitDialog();
  afx_msg void OnPaint();
  afx_msg void OnNMReleasedcaptureSliderSpp(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnEnChangeEditSpp();
};
