// MatView.h : interface of the CMatView class
//
#pragma once


#include "afxwin.h"
#include "resource.h"

#include "Es/Strings/rString.hpp"
#include "Libs/ClrButton.h"
#include "Libs/MatSliderCtrl.h"
#include "Libs/MatListBox.h"
#include "Libs/MatSpinButtonCtrl.h"
#include "Libs/ColorStatic.h"
#include "Dialogs/WeatherDlg.h"
#include "Codex/cdxCDynamicFormView.h"

#include "MatDoc.h"
#include "afxcmn.h"


#define MAX_MAT             64
#define CFG_FILE            "mat.cfg"


//comunication with Buldozer
#define USER_WINDOW_HANDLE   ( WM_APP+4 )
#define USER_LIGHT_AMBIENT_R ( WM_APP+8 )
#define USER_LIGHT_AMBIENT_G ( WM_APP+9 )
#define USER_LIGHT_AMBIENT_B ( WM_APP+10 )
#define USER_LIGHT_DIFFUSE_R ( WM_APP+11 )
#define USER_LIGHT_DIFFUSE_G ( WM_APP+12 )
#define USER_LIGHT_DIFFUSE_B ( WM_APP+13 )
#define USER_LIGHT_REQUEST   ( WM_APP+14 )
#define USER_TIME_REQUEST    ( WM_APP+15 )


//other messages
#define WM_EXTERN_EDITOR              ( WM_APP+50 )
#define WM_CLOSE_WEATHER_DLG          ( WM_APP+51 )               


class CMatView : public cdxCDynamicFormView //pouze kvuli roztahovani
{
protected: // create from serialization only
	CMatView();
	DECLARE_DYNCREATE(CMatView)

public:
	enum{ IDD = IDD_Mat_FORM };

// Attributes
public:
	CMatDoc* GetDocument() const;

// Overrides
	public:
//virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CMatView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
	DECLARE_DYNAMIC_MAP()

  //flags & help variables
	BOOL _enable;                    //nastaveno podle posledniho volani Enable(), Disable()
  int _sel[MAX_MAT];               //pomocne pole
  int _ext_mat;                    //pomocna promena pro externi editor
  int _dis_mats_col;               //color to draw unselected materials
  BOOL _disable_unselect_mats;
  int _p3d_type;                   //P3D_EMPTY, P3D_NORMAL, P3D_PRIMITIVE
  BOOL _backup;                    //TRUE if model is backuped
  BOOL _run_buld;                  //TRUE if run Buldozer
  BOOL _shared_mem;                //TRUE if Mat has to share memory with other clients (O2)
  Material _mat_copy;              //material created by "Copy", "Paste"
  BOOL _mat_copied;                //true if a material was copied
  BOOL _updating;                
  BOOL _update_forbiddance;        //if true no updating
  //buldozer weather
  BOOL _keep_weather;
  CTime _time;        
  float _overcast;                 //0..1
  float _fog;                      //0..1
  //lights from Buldozer
  float _light_amb_r;
  float _light_amb_g;
  float _light_amb_b;
  float _light_dif_r;
  float _light_dif_g;
  float _light_dif_b;
  CClrButton _bcol_light_amb;
  CClrButton _bcol_light_dif;
  //edits
  float _ambient_r;
  float _ambient_g;
  float _ambient_b;
  float _ambient_a;
  float _diffuse_r;
  float _diffuse_g;
  float _diffuse_b;
  float _diffuse_a;
  float _forcedDiffuse_r;
  float _forcedDiffuse_g;
  float _forcedDiffuse_b;
  float _forcedDiffuse_a;
  float _emmisive_r;
  float _emmisive_g;
  float _emmisive_b;
  float _emmisive_a;
  float _specular_r;
  float _specular_g;
  float _specular_b;
  float _specular_a;
  float _specularPower;
  CString _PixelShaderID;
  CString _VertexShaderID;
	CComboBox _combo_PSI;
	CComboBox _combo_VSI;
  //stage
  BOOL _check_stage;                           //umoznuje uzivateli disable stage
  CString _stage_name;
  CString _texture;
  CString _uvSource;
  CComboBox _combo_uvTex;
  float _aside_0, _aside_1, _aside_2;
  float _up_0, _up_1, _up_2;
  float _dir_0, _dir_1, _dir_2;
  float _pos_0, _pos_1, _pos_2;
  //spins
  CMatSpinButtonCtrl _spin_amb_r;
  CMatSpinButtonCtrl _spin_amb_g;
  CMatSpinButtonCtrl _spin_amb_b;
  CMatSpinButtonCtrl _spin_amb_a;
  CMatSpinButtonCtrl _spin_dif_r;
  CMatSpinButtonCtrl _spin_dif_g;
  CMatSpinButtonCtrl _spin_dif_b;
  CMatSpinButtonCtrl _spin_dif_a;
  CMatSpinButtonCtrl _spin_fod_r;
  CMatSpinButtonCtrl _spin_fod_g;
  CMatSpinButtonCtrl _spin_fod_b;
  CMatSpinButtonCtrl _spin_fod_a;
  CMatSpinButtonCtrl _spin_emm_r;
  CMatSpinButtonCtrl _spin_emm_g;
  CMatSpinButtonCtrl _spin_emm_b;
  CMatSpinButtonCtrl _spin_emm_a;
  CMatSpinButtonCtrl _spin_spc_r;
  CMatSpinButtonCtrl _spin_spc_g;
  CMatSpinButtonCtrl _spin_spc_b;
  CMatSpinButtonCtrl _spin_spc_a;
  CMatSpinButtonCtrl _spin_spp;
  //big spins
  CMatSpinButtonCtrl _spin_amb;
  CMatSpinButtonCtrl _spin_dif;
  CMatSpinButtonCtrl _spin_fod;
  CMatSpinButtonCtrl _spin_emm;
  CMatSpinButtonCtrl _spin_spc;
  //checks
  BOOL _check_amb_r;
  BOOL _check_amb_g;
  BOOL _check_amb_b;
  BOOL _check_amb_a;
  BOOL _check_lock_ambdif;
  CButton _checkbutton_lock_ambdif;
  BOOL _check_dif_r;
  BOOL _check_dif_g;
  BOOL _check_dif_b;
  BOOL _check_dif_a;
  BOOL _check_fod_r;
  BOOL _check_fod_g;
  BOOL _check_fod_b;
  BOOL _check_fod_a;
  BOOL _check_emm_r;
  BOOL _check_emm_g;
  BOOL _check_emm_b;
  BOOL _check_emm_a;
  BOOL _check_spc_r;
  BOOL _check_spc_g;
  BOOL _check_spc_b;
  BOOL _check_spc_a;
  //sliders
  CMatSliderCtrl _slider_amb;
  CMatSliderCtrl _slider_dif;
  CMatSliderCtrl _slider_fod;
  CMatSliderCtrl _slider_emm;
  CMatSliderCtrl _slider_spc;
  //color buttons
  CClrButton _bcol_amb;
  CClrButton _bcol_dif;
  CClrButton _bcol_fod;
  CClrButton _bcol_emm;
  CClrButton _bcol_spc;
  //cfg & ConfigStage
  RString _cfg_file;                      //for config save
  RString _cfg_stage_path;                //for Stage config files
  AutoArray<RString> _cfg_stage_name;
  AutoArray<RString> _cfg_stage_file;
  //paths
  RString _buld_path;
  RString _mat_path;
  RString _default_mat_path;
  RString _last_p3d_path;
  RString _last_mat_path;
  RString _last_tex_path;
	BOOL _automatic_buldozer_start;
	BOOL _automatic_buldozer_end;
  RString _extern_editor;
  RString _texview_path;
  RString _sphere_path;
  RString _sphere2_path;
  RString _box_path;
  RString _cone_path;
  RString _cone2_path;
  RString _obj_path;
  RString _ss_server_name;
  //controls
  CMatListBox _list_mat;
  CString _edit_chmat;
  CComboBox _list_lod;
  CColorDialog _color_dlg;
  CTabCtrl _tab_stage;
  //umodal dialogs                                   //modalni vytvarim na zasobniku
  CWeatherDlg* _weather_dlg;                         //weather dialog
  //popup menu info about selected materials
  BOOL _all_ss_controlled;
  BOOL _all_checked_out;

  //model
  void BackupObj();                                  //called by ResetObj()
  void RestoreBackupObj();                           //restore the backuped model and materials
  void ResetObj(BOOL backup= FALSE);
  void DeleteBackup();
  void AskToLoadFromBackupMat();                    //ask user to load backup mat, if yes than load it
  BOOL AskToSaveToBackupMat();                      //save actual material to all selected materials from backup model if user say yes
  BOOL OpenObj(const char* file_name);
  //status bar
  void StatusBar(const char* str= "");
  void SetTitle(const char* title);
	//enable&disable
  void EnableControls();
	void Enable();
  void Disable();
  void EnableStage();
  void DisableStage();
  void EnableUVTransform();
  void DisableUVTransform();
  void ProtectStages();
  //inicialization
  void InitControls();
  void ResetControlsAfterLoad();                    //reset controls after load - checks, sliders
  void ResetValues();                               //set all variables to default values 
  //mat
  void SetFlags(BOOL fast= FALSE);                  //set the current row in the material list
  void SetListMat(const int sel= 0);                //set the list '_list_mat'
  void SetEditChangeMat();
  void SetROFlags();
  void SetSSFlags();
  void LoadMatFromFile(RString mat_name);           //load the new material from file to materials
  void LoadMatFromMat(Material& m);                 //load the new material from file to materials
  void DisableUnselectedMaterials();                //disable or enable unselected materials according to _disable_unselect_mats
  //lod
  void SelectRowLod(int sel);
  void SetListLod(const int sel_mat= 0); 
  //stages
  void CheckUVTransformEnable();                    //set the UVTransform
  void CheckStageEnable();                          //nastavi stage podle _check_stage
  void SetStages();
  //variables: load & save
  void LoadMaterial();                              //load material from doc to view variables
  template <class T> int Assign(T& dest, T& src);
  void SaveMaterial();                              //save material from view to doc variables
  void CheckFileBeforeSave(Material& m);
  void CheckFilesBeforeSave(int flag);
  //config
  void CreateCfg();                                 //create config file
  void ReadCfg();                                   //read config file
	void SaveCfg();                                   //save config file
  void ReadCfgStage();                              //read stage configs for pixel shaders
  int InCfgStage(RString PixelShaderID);
	//update
  void GUIRefresh();
  void StartUpdate();
  void EndUpdate();
  //set colors
  void SetColorAmb();
  void SetColorDif();
  void SetColorFod();
  void SetColorEmm();
  void SetColorSpc();
  void SetColors();
  void SetLights();
  //SS functions
  BOOL CheckOpenSS();                                //check SS initalization
  BOOL GetLatestVersion();                           //get latest version of all selected files, load the latest version from SS
  BOOL CheckOut();                                   //check out all selected files
  BOOL CheckIn();                                    //check in all selected files
  BOOL UndoCheckOut();                               //make undo chech out, load the old version from SS
  BOOL History();                                    //show SS history
  BOOL Diff();                                       //show differences between local file and the version in SS
  BOOL Properties();                                 //show SS properties of the material
  BOOL ReadOnly();                                   //change 'read-only' flag
  //enable&disable popup menu
  void GetPopupMenuInfo();
  BOOL EnablePmCopy();
  BOOL EnablePmPaste();
  BOOL EnablePmLoad();
  BOOL EnablePmLoadDefault();
  BOOL EnablePmSave();
  BOOL EnablePmSaveAs();
  BOOL EnablePmExplore();
  BOOL EnablePmGetLatestVersion();
  BOOL EnablePmCheckOut();
  BOOL EnablePmCheckIn();
  BOOL EnablePmUndoCheckOut();
  BOOL EnablePmHistory();
  BOOL EnablePmDiff();
  BOOL EnablePmProperties();
  BOOL EnablePmReadOnly(BOOL& check);
  //Buldozer
  void StartBuldozer();

public:
  void AskToSave(const BOOL rep_no_changes= TRUE);  //zepta se uzivatale, zda ulozit vsechny rozdelane matiraly a provede
  void SetTimeOvercastFog(CTime time, float overcast, float fog);
  void SetTime(float time);
  CTime GetTime() const;

  //update
  void UpdateLights();                              //update lights from Buldozer
  void UpdateTime();
  void Update();
  void ForbidUpdate(BOOL fu);

  //release spin
  float _last_spin_pos;
  int _last_spin_move;                              //indicate last spin move, 0- no move (on start), 1- move up, -1- move down
  void OnStartSpinAmbR();
  void OnEndSpinAmbR();

  //edits
  afx_msg void OnEnChangeEditAmbR();
  afx_msg void OnEnChangeEditAmbG();
  afx_msg void OnEnChangeEditAmbB();
  afx_msg void OnEnChangeEditAmbA();
  afx_msg void OnEnChangeEditDifR();
  afx_msg void OnEnChangeEditDifG();
  afx_msg void OnEnChangeEditDifB();
  afx_msg void OnEnChangeEditDifA();
  afx_msg void OnEnChangeEditFodR();
  afx_msg void OnEnChangeEditFodG();
  afx_msg void OnEnChangeEditFodB();
  afx_msg void OnEnChangeEditFodA();
  afx_msg void OnEnChangeEditEmmR();
  afx_msg void OnEnChangeEditEmmG();
  afx_msg void OnEnChangeEditEmmB();
  afx_msg void OnEnChangeEditEmmA();
  afx_msg void OnEnChangeEditSpcR();
  afx_msg void OnEnChangeEditSpcG();
  afx_msg void OnEnChangeEditSpcB();
  afx_msg void OnEnChangeEditSpcA();
  afx_msg void OnEnChangeEditSpp();
	afx_msg void OnCbnEditchangeComboPsi();
	afx_msg void OnCbnSelchangeComboPsi();
	afx_msg void OnCbnEditchangeComboVsi();
	afx_msg void OnCbnSelchangeComboVsi();
  //stage
  afx_msg void OnTcnSelchangeTabStages(NMHDR *pNMHDR, LRESULT *pResult);     //prekliknuti na jiny tab
  afx_msg void OnBnClickedCheckEnableStage();
  afx_msg void OnBnClickedBNewStage();
  afx_msg void OnBnClickedBDeleteStage();
  afx_msg void OnEnChangeEditStageName();
  afx_msg void OnEnChangeEditTex();
  afx_msg void OnBnClickedBBrowsTexture();
  afx_msg void OnBnClickedBViewTexture();
  afx_msg void OnBnClickedBExploreTexture();
  afx_msg void OnCbnEditchangeComboUvs();
  afx_msg void OnCbnSelchangeComboUvs();
  afx_msg void OnEnChangeEditAs0();
  afx_msg void OnEnChangeEditAs1();
  afx_msg void OnEnChangeEditAs2();
  afx_msg void OnEnChangeEditUp0();
  afx_msg void OnEnChangeEditUp1();
  afx_msg void OnEnChangeEditUp2();
  afx_msg void OnEnChangeEditDi0();
  afx_msg void OnEnChangeEditDi1();
  afx_msg void OnEnChangeEditDi2();
  afx_msg void OnEnChangeEditPs0();
  afx_msg void OnEnChangeEditPs1();
  afx_msg void OnEnChangeEditPs2();
  //spins
  afx_msg void OnDeltaposSpinAmbR(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinAmbG(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinAmbB(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinAmbA(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinDifR(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinDifG(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinDifB(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinDifA(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinFodR(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinFodG(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinFodB(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinFodA(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinEmmR(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinEmmG(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinEmmB(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinEmmA(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinSpcR(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinSpcG(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinSpcB(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinSpcA(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinSpp(NMHDR *pNMHDR, LRESULT *pResult);
  //big spin
  afx_msg void OnDeltaposSpinAmb(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinDif(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinFod(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinEmm(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDeltaposSpinSpc(NMHDR *pNMHDR, LRESULT *pResult);
  //spin buttons
  afx_msg void OnBnClickedBAmbL();
  afx_msg void OnBnClickedBAmbR();
  //checks
  afx_msg void OnBnClickedCheckAmbR();
  afx_msg void OnBnClickedCheckAmbG();
  afx_msg void OnBnClickedCheckAmbB();
  afx_msg void OnBnClickedCheckAmbA();
  afx_msg void OnBnClickedCheckLockAmbDif();
  afx_msg void OnBnClickedCheckDifR();
  afx_msg void OnBnClickedCheckDifG();
  afx_msg void OnBnClickedCheckDifB();
  afx_msg void OnBnClickedCheckDifA();
  afx_msg void OnBnClickedCheckFodR();
  afx_msg void OnBnClickedCheckFodG();
  afx_msg void OnBnClickedCheckFodB();
  afx_msg void OnBnClickedCheckFodA();
  afx_msg void OnBnClickedCheckEmmR();
  afx_msg void OnBnClickedCheckEmmG();
  afx_msg void OnBnClickedCheckEmmB();
  afx_msg void OnBnClickedCheckEmmA();
  afx_msg void OnBnClickedCheckSpcR();
  afx_msg void OnBnClickedCheckSpcG();
  afx_msg void OnBnClickedCheckSpcB();
  afx_msg void OnBnClickedCheckSpcA();
  afx_msg void OnBnClickedCheckDisUnsMat();
  //sliders
  afx_msg void OnNMReleasedcaptureSliderAmb(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReleasedcaptureSliderDif(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReleasedcaptureSliderFod(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReleasedcaptureSliderEmm(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReleasedcaptureSliderSpc(NMHDR *pNMHDR, LRESULT *pResult);
  //buttons
  afx_msg void OnBnClickedBColAmb();
  afx_msg void OnBnClickedBColDif();
  afx_msg void OnBnClickedBColFod();
  afx_msg void OnBnClickedBColEmm();
  afx_msg void OnBnClickedBColSpc();
  afx_msg void OnBnClickedBSpp();
  afx_msg void OnBnClickedBLightAmb();
  afx_msg void OnBnClickedBLightDif();
  //start & end
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnDestroy();
  //menu & toolbar
  afx_msg void OnFileOpen();
  afx_msg void OnFileSave();
  afx_msg void OnFileSaveas();
  afx_msg void OnFileSaveall();
  afx_msg void OnUpdateFileSave(CCmdUI *pCmdUI);
  afx_msg void OnUpdateFileSaveas(CCmdUI *pCmdUI);
  afx_msg void OnUpdateFileSaveall(CCmdUI *pCmdUI);
  afx_msg void OnFileGetLatestVersion();
  afx_msg void OnUpdateFileGetLatestVersion(CCmdUI *pCmdUI);
  afx_msg void OnEditUndo();
  afx_msg void OnEditRedo();
  afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
  afx_msg void OnUpdateEditRedo(CCmdUI *pCmdUI);
  afx_msg void OnViewBuldozer();
  afx_msg void OnViewExternalEditor();
  afx_msg void OnUpdateViewExternaleditor(CCmdUI *pCmdUI);
  afx_msg void OnPrimSphere();
  afx_msg void OnPrimSphere2();
  afx_msg void OnPrimBox();
  afx_msg void OnPrimCone();
  afx_msg void OnPrimCone2();
  afx_msg void OnViewMainModel();
  afx_msg void OnUpdateViewMainmodel(CCmdUI *pCmdUI);
  afx_msg void OnOptionsConfiguration();
  afx_msg void OnWeather();
  afx_msg void OnUpdateWeather(CCmdUI *pCmdUI);
  //popup menu
  void OnRButtonDown();
  afx_msg void OnPmCopy();
  afx_msg void OnPmPaste();
  afx_msg void OnUpdatePmPaste(CCmdUI *pCmdUI);
  afx_msg void OnUpdatePmCopy(CCmdUI *pCmdUI);
  afx_msg void OnPmLoad();
  afx_msg void OnUpdatePmLoad(CCmdUI *pCmdUI);
  afx_msg void OnPmLoadDefault();
  afx_msg void OnUpdatePmLoadDefault(CCmdUI *pCmdUI);
  afx_msg void OnPmExplore();
  afx_msg void OnUpdatePmExplore(CCmdUI *pCmdUI);
  afx_msg void OnPmGetLatestVersion();
  afx_msg void OnUpdatePmGetLatestVersion(CCmdUI *pCmdUI);
  afx_msg void OnPmCheckOut();
  afx_msg void OnUpdatePmCheckOut(CCmdUI *pCmdUI);
  afx_msg void OnPmCheckIn();
  afx_msg void OnUpdatePmCheckIn(CCmdUI *pCmdUI);
  afx_msg void OnPmUndoCheckOut();
  afx_msg void OnUpdatePmUndoCheckOut(CCmdUI *pCmdUI);
  afx_msg void OnPmHistory();
  afx_msg void OnUpdatePmHistory(CCmdUI *pCmdUI);
  afx_msg void OnPmDiff();
  afx_msg void OnUpdatePmDiff(CCmdUI *pCmdUI);
  afx_msg void OnPmProperties();
  afx_msg void OnUpdatePmProperties(CCmdUI *pCmdUI);
  afx_msg void OnPmReadOnly();
  afx_msg void OnUpdatePmReadOnly(CCmdUI *pCmdUI);
  //lod & material list
  afx_msg void OnCbnSelchangeComboLod();
  afx_msg void OnLbnSelchangeListMat();
  //timer
  afx_msg void OnTimer(UINT nIDEvent);
  //on resize
  afx_msg void OnSize(UINT nType, int cx, int cy);
  //test
  afx_msg void OnBnClickedBTest();

protected:
  virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);
};

#ifndef _DEBUG  // debug version in MatView.cpp
inline CMatDoc* CMatView::GetDocument() const
   { return reinterpret_cast<CMatDoc*>(m_pDocument); }
#endif

extern CMatView* view;

