/*
  MateialHistory.h
  ----------------

  Data structur for history of material.
*/


#ifndef _material_history_h_
#define _material_history_h_


#include "Material.hpp"


#define H_LEN           100                  //delka historie


class MaterialHistory
{
private:
  Material _mat[H_LEN];
  int _first;
  int _last;
  int _cur;                                  //material 

  void Inc(int& a);
  void Dec(int& a);
  BOOL Empty();
  BOOL Full();                               //if list is full return TRUE
  void AddFirst(Material& mat);              //add first member
  void RemoveLast(Material& mat);

public:
  MaterialHistory();
  BOOL FirstPos();
  BOOL LastPos();
  void Add(Material& mat);
  BOOL Undo(Material& mat);
  BOOL Redo(Material& mat);
};


#endif