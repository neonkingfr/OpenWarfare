// Mat.h : main header file for the Mat application
// 
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols


// CMatApp:
// See Mat.cpp for the implementation of this class
//

class CMatApp : public CWinApp
{
public:
	CMatApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CMatApp theApp;
