// MatDoc.h : interface of the CMatDoc class
//


#pragma once
#include "view/objlod.hpp"
#include "material.hpp"

class CMatDoc : public CDocument
{
protected: // create from serialization only
	CMatDoc();
	DECLARE_DYNCREATE(CMatDoc)

// Implementation
public:
	virtual ~CMatDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

public:
  LODObject* _obj;                  
  LODObject* _obj_backup;
  Materials* _mats;                 //the list of materials in _obj
  Materials* _mats_backup;

  virtual BOOL OnNewDocument();
  virtual void Serialize(CArchive& ar);
  virtual void OnCloseDocument();
};


extern CMatDoc* doc;

