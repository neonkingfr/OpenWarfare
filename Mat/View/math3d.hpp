#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MATH3D_HPP
#define _MATH3D_HPP

#include <Es/essencePch.hpp>
#include <Es/Containers/array.hpp>

#ifdef _KNI
#include "math3dK.hpp"
// force K types
#define Vector3 Vector3K
#define Point3 Vector3K
#define Vector3Val Vector3KVal
#define Vector3Par Vector3KPar
#define Matrix3 Matrix3K
#define Matrix4 Matrix4K
#define Matrix3Val const Matrix3K &
#define Matrix4Val const Matrix4K &
#define ConvertToM(x) ConvertPToK(x)
#define ConvertToP(x) ConvertKToP(x)
#define VZero VZeroK
#define VUp VUpK
#define VForward VForwardK
#define VAside VAsideK
/*
#elif defined _PIII
	#include "math3dP3.hpp"
	// force K types
	#define Vector3 Vector3P3
	#define Point3 Vector3P3
	#define Vector3Val Vector3P3Val
	#define Vector3Par Vector3P3Par
	#define Matrix3 Matrix3P3
	#define Matrix4 Matrix4P3
	#define ConvertToM(x) ConvertPToP3(x)
	#define ConvertToP(x) ConvertP3ToP(x)
	#define VZero VZeroP3
	#define VUp VUpP3
	#define VForward VForwardP3
	#define VAside VAsideP3
	#define MIdentity M4IdentityP3
	#define MZero M4ZeroP3
	#define M4Identity M4IdentityP3
	#define M4Zero M4ZeroP3
	#define M3Identity M3IdentityP3
	#define M3Zero M3ZeroP3
*/
#else
#include "math3dP.hpp"
// force P types
typedef Vector3P Vector3;
typedef Vector3P Point3;
typedef Matrix3P Matrix3;
typedef Matrix4P Matrix4;

#define Vector3Val Vector3PVal
#define Vector3Par Vector3PPar
#define Matrix3Val Matrix3PVal
#define Matrix3Par Matrix3PPar
#define Matrix4Val Matrix4PVal
#define Matrix4Par Matrix4PPar
#define ConvertToM(x) (x)
#define ConvertToP(x) (x)
#define VZero VZeroP
#define VUp VUpP
#define VForward VForwardP
#define VAside VAsideP
#define MIdentity M4IdentityP
#define MZero M4ZeroP
#define M4Identity M4IdentityP
#define M4Zero M4ZeroP
#define M3Identity M3IdentityP
#define M3Zero M3ZeroP
#endif

#endif
