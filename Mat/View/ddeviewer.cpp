#include "..\stdafx.h"
#include "wpch.hpp"
#include <Es/Strings/RString.hpp>
#include <El/QStream/QStream.hpp>
#include <process.h>
#include "optima2mfc.h"

#include "DdeViewer.hpp"

#define USER_RELOAD ( WM_APP+0 )
#define USER_CLOSE ( WM_APP+1 )
#define USER_FILE_HANDLE ( WM_APP+2 )
#define USER_MUTEX_HANDLE ( WM_APP+3 )
#define USER_WINDOW_HANDLE ( WM_APP+4 )
#define USER_ANIM_PHASE ( WM_APP+5 )
#define USER_START_SCRIPT ( WM_APP+6 ) //LPARAM atom name

#define SHARED_LEN ( 24*1024*1024 ) // objects are usually max 32 KB
// some large objects (people...) may be quite large

#define SHARDED_MUTEX_NAME "BohemiaInteractiveStudio@FileMutex@Objektiv2@Buldozer@12ad581"
#define SHARDED_MEMORY_NAME "BohemiaInteractiveStudio@FileShared@Objektiv2@Buldozer@12be587"

// implementation

//static HANDLE ViewerThread;
static HANDLE ViewerProcess;
static HWND ViewerWindow;

static DWORD ViewerThreadId;

static HANDLE FileMap;
static HANDLE FileMutex;
static void *MemMap;

static bool sharedMemory;

static bool InitSharing()
  {
  if( FileMutex && FileMap && MemMap ) return true;
  // create security descriptor for inheritable handle
  SECURITY_ATTRIBUTES inherit;
  inherit.nLength=sizeof(inherit);
  inherit.bInheritHandle=true;
  inherit.lpSecurityDescriptor=NULL;
  // create mutex
  if ( sharedMemory )
    FileMutex=::CreateMutex(&inherit,FALSE,SHARDED_MUTEX_NAME);
  else
    FileMutex=::CreateMutex(&inherit,FALSE,NULL);
  if( !FileMutex ) return false;
  // create shared memory object
  if ( sharedMemory )
    FileMap=::CreateFileMapping((HANDLE)-1, &inherit, PAGE_READWRITE, 0, SHARED_LEN, SHARDED_MEMORY_NAME);
  else
    FileMap=::CreateFileMapping((HANDLE)-1, &inherit, PAGE_READWRITE, 0, SHARED_LEN, NULL);
  if( !FileMap ) return false;
  MemMap=MapViewOfFile(FileMap,FILE_MAP_WRITE,0,0,SHARED_LEN);
  if( !MemMap ) return false;
  return true;
  }

//--------------------------------------------------

static void CloseSharing()
  {
  if( MemMap ) UnmapViewOfFile(MemMap),MemMap=NULL;
  if( FileMap ) CloseHandle(FileMap),FileMap=NULL;
  if( FileMutex ) CloseHandle(FileMutex),FileMutex=NULL;
  }

//--------------------------------------------------

static bool PostViewerMessage( UINT msg, WPARAM wp, LPARAM lp )
  {
  if( ViewerWindow )
    {
    return ::PostMessage(ViewerWindow,msg,wp,lp)!=FALSE;
    }
  else if( ViewerThreadId )
    {
    return ::PostThreadMessage(ViewerThreadId,msg,wp,lp)!=FALSE;;
    }
  return false;
  }

//--------------------------------------------------

static void CloseViewerHandles()
  {
  // invalidate handles
  if( ViewerProcess ) ::CloseHandle(ViewerProcess);
  //if( ViewerThread ) ::CloseHandle(ViewerThread);
  ViewerProcess=NULL;
  //ViewerThread=NULL;
  ViewerWindow=NULL;
  ViewerThreadId=NULL;
  }

//--------------------------------------------------

static void SaveSharedData( const void *buf, int size, float lodBias, float phase )
  {
  if( WaitForSingleObject(FileMutex,INFINITE)==WAIT_OBJECT_0 )
    {
    if (size>SHARED_LEN)
      {
      MessageBox(ViewerWindow,"Out of memory while updating Buldozer - Buffer is too small","SaveSharedData",MB_OK);
      CloseSharing();
      CloseViewerHandles();      
      }
    else if( MemMap)
      {
      memcpy(MemMap,buf,size);
      if( !PostViewerMessage(USER_RELOAD,WPARAM(lodBias*100),size) )
        {
        CloseViewerHandles();
        }
      else
        {
        ::PostViewerMessage(USER_ANIM_PHASE,0,LPARAM(phase*65536));
        }
      }
    ReleaseMutex(FileMutex);
    }
  }

//--------------------------------------------------

static bool CheckViewer()
  {
  if( !ViewerProcess ) return false;
  DWORD terminated=WaitForSingleObject(ViewerProcess,0);
  if( terminated==WAIT_OBJECT_0 )
    {
    CloseViewerHandles();
    return false;
    }
  else if( terminated==WAIT_TIMEOUT )
    {
    return true; // viewer is running
    }
  else
    {
    WMessageBox::Message(NULL,WMsgBSOk,"Viewer","Thread status unknown");
    return false;
    }
  }

//--------------------------------------------------

struct SearchViewerContext
  {
  // in:
  
  // out:
  HWND window;
  DWORD threadID;
  HANDLE process; // process handle
  //HANDLE thread; // process handle
  };

//--------------------------------------------------

static BOOL CALLBACK SearchViewer( HWND hwnd, LPARAM lParam )
  {
  SearchViewerContext *context=(SearchViewerContext *)lParam;
  // get process attached to hwnd
  // we have to scan window title
  // search for Buldozer
  
  char text[512];
  *text=0;    
  GetWindowText(hwnd,text,sizeof(text));
  if( !strstr(text,"Buldozer") ) return TRUE;
  
  context->window=hwnd;
  DWORD processID;
  DWORD threadID=::GetWindowThreadProcessId(hwnd,&processID);
  HANDLE processHandle=::OpenProcess(STANDARD_RIGHTS_REQUIRED,FALSE,processID);
  //    HANDLE threadHandle=::OpenThread(STANDARD_RIGHTS_REQUIRED,FALSE,threadID);
  if( processHandle )
    {
    // get process command line
    context->process=processHandle;
    context->threadID=threadID;
    return FALSE;
    }
  return TRUE;
  }

//--------------------------------------------------


static bool FindViewerWindow()
  {
  if( ViewerWindow ) return true;
  
  ViewerWindow=::FindWindow("Operation Flashpoint","Buldozer");
  if (!ViewerWindow) return false;
  DWORD temp;
  ViewerThreadId=GetWindowThreadProcessId(ViewerWindow,&temp);
  if (ViewerProcess==NULL)
    {
    ViewerProcess=OpenProcess(SYNCHRONIZE|PROCESS_DUP_HANDLE|PROCESS_TERMINATE,TRUE,temp);
    }
  return true;
  }

//--------------------------------------------------

bool IsViewerRunning()
  {
  return CheckViewer();  
  }

//--------------------------------------------------

static bool RunViewer( HWND hWnd, const char *exeCommand, bool twoMon )
  {
  // extract command fromt the path
  // skip any leading spaces
  while (isspace(*exeCommand)) exeCommand++;
  // take care of double quotes
  RString exeName;
  if (*exeCommand=='"')
    {
    const char *startExe = ++exeCommand;
    while (*exeCommand!='"' && *exeCommand)
      {
      exeCommand++;
      }
    // everything from startExe to exeCommand is exe name
    exeName = RString(startExe,exeCommand-startExe);
    if (*exeCommand) exeCommand++;
    
    }
  else
    {
    const char *startExe = exeCommand;
    while (!isspace(*exeCommand) && *exeCommand)
      {
      exeCommand++;
      }
    // everything from startExe to exeCommand is exe name
    exeName = RString(startExe,exeCommand-startExe);
    if (*exeCommand) exeCommand++;
    }
  // skip any trailing spaces
  while (isspace(*exeCommand)) exeCommand++;
  
  WFilePath exePath = exeName;
  
  // if viewer is already running, use it
  if( CheckViewer() ) return true;
  
  if( !InitSharing() ) return false;
  
  if( !FindViewerWindow() )
    {
    
    WFilePath exeDir;
    exeDir.SetDrive(exePath.GetDrive());
    exeDir.SetDirectory(exePath.GetDirectory());
    exeDir.SetFilename("");
    
    STARTUPINFO sInfo;
    PROCESS_INFORMATION pInfo;
    
    sInfo.cb=sizeof(sInfo); 
    sInfo.lpReserved=NULL;
    sInfo.lpDesktop=NULL;
    sInfo.lpTitle=NULL;
    sInfo.dwFlags=0;
    // wShowWindow; 
    sInfo.cbReserved2=NULL;
    sInfo.lpReserved2=NULL;
    
    WString pars = exePath + WString(" ") + WString(exeCommand);
    if( twoMon ) pars+=" -twomon";
    //::MessageBox(NULL,parsChar,"Viewer pars",MB_OK);
    if
      (
        ::CreateProcess
          (
            NULL, // file name
            (char *)pars.GetText(), // command line
            NULL,NULL, // pointers to security attributes 
            TRUE, // handle inheritance flag 
            0,	// creation flags 
            NULL, // pointer to new environment block 
            exeDir, // pointer to current directory name 
            &sInfo, // pointer to STARTUPINFO 
            &pInfo // pointer to PROCESS_INFORMATION  
            )
              )
                {
                ::WaitForInputIdle(pInfo.hProcess,INFINITE);
                //if( !FindViewerWindow() )
                ViewerProcess=pInfo.hProcess;
                CloseHandle(pInfo.hThread); // thread handle not required
                ViewerThreadId=pInfo.dwThreadId;
                //::MessageBox(NULL,(char *)pars.GetText(),"Viewer started: pars ",MB_OK);
                }
    }
  
  if( ViewerProcess )
    {
    HANDLE dupFileMap,dupFileMutex;
    ::DuplicateHandle
      (
        ::GetCurrentProcess(),FileMap,
    ViewerProcess,&dupFileMap,
    STANDARD_RIGHTS_REQUIRED,TRUE,DUPLICATE_SAME_ACCESS
      );
    ::DuplicateHandle
      (
        ::GetCurrentProcess(),FileMutex,
    ViewerProcess,&dupFileMutex,
    STANDARD_RIGHTS_REQUIRED,TRUE,DUPLICATE_SAME_ACCESS
      );
    if( dupFileMap && dupFileMutex )
      {
      PostViewerMessage(USER_FILE_HANDLE,0,(DWORD)dupFileMap);
      PostViewerMessage(USER_MUTEX_HANDLE,0,(DWORD)dupFileMutex);
      PostViewerMessage(USER_WINDOW_HANDLE,0,(DWORD)hWnd);
      }
    }
  return ViewerProcess!=NULL;
  }

//--------------------------------------------------

static int buffersize=4*1024*1024;
static bool ViewerSendOpen( HWND hWnd, LODObject *objSave, float lodBias )
  {

  if( !CheckViewer() ) return false;
  // viewer is running
  // save object into memory

  QOStrStream f;
  f.setbuffer(buffersize);
  objSave->Save(f,9999,true);
  int commandLen= f.tellp();
  if (commandLen>buffersize) buffersize*=2;
  const char *commandData= f.str();
  if( commandData )
    {
    // save into the shared memory
    ObjectData *obj=objSave->Active();
    int pIndex=obj->CurrentAnimation();
    float phase=0;
    if( pIndex>=0 ) phase=obj->GetAnimation(pIndex)->GetTime();
    SaveSharedData(commandData,commandLen,lodBias,phase);
    return true;
    }
  return false;
  }

//--------------------------------------------------

static void ViewerSendClose( HWND hWnd )
  {
  PostViewerMessage(USER_CLOSE,0,0);
  WaitForSingleObject(ViewerProcess,10000);
  CloseHandle(ViewerProcess);
  ViewerProcess=NULL;
  ViewerWindow=NULL;
  }

//--------------------------------------------------

bool StartViewer(HWND hWnd, LODObject *obj, const char *exe, float lodBias, bool twoMon, bool sharedMem)
{
  sharedMemory= sharedMem;
  if( !InitSharing() )
    return false;
  if( !RunViewer(hWnd,exe,twoMon) )
    return false;
  if( ViewerSendOpen(hWnd,obj,lodBias) )
    return true;
  return true;
}

//--------------------------------------------------

bool UpdateViewer( HWND hWnd, LODObject *obj, float lodBias )
  {
  return ViewerSendOpen(hWnd,obj,lodBias);
  }

//--------------------------------------------------

void CloseViewer( HWND hWnd )
  {
  ViewerSendClose(hWnd);
  CloseSharing();
  }

//--------------------------------------------------

void SwitchToViewer()
  {
  if (FindViewerWindow()) 
    {
    ::SetForegroundWindow(ViewerWindow);
    ::ShowWindow(ViewerWindow,SW_RESTORE);
    }
  }

//--------------------------------------------------

bool ViewerHasFocus()
  {
  return GetForegroundWindow()==ViewerWindow;
  }

//--------------------------------------------------

HWND GetExternalWindow()
  {
  if (FindViewerWindow()) return ViewerWindow;
  return NULL;
  }

//--------------------------------------------------

bool ViewerStartScript(const char *scriptname)
  {
  bool out;
  ATOM atm=GlobalAddAtom(scriptname);
  FindViewerWindow();
  //SendMessageTimeout(ViewerWindow, USER_START_SCRIPT , (WPARAM)atm, (LPARAM)atm,SMTO_ABORTIFHUNG|SMTO_NORMAL,5000,&result);
  PostMessage(ViewerWindow, USER_START_SCRIPT, 0,(LPARAM)atm);
  for (int i=0;i<20;i++)
    {
    out=GlobalFindAtom(scriptname)!=atm;
    if (out) break;
    Sleep(100);
    }
  GlobalDeleteAtom(atm);
  return out;
  }

//--------------------------------------------------

//2531 2919