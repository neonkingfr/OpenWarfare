#include "..\stdafx.h"
// homogenous vector and matrix arithmetics
// (C) 1997, SUMA
#include "wpch.hpp"

#if _RELEASE && defined _MSC_VER
// assume no aliasing
#pragma optimize("a",on)
#endif

#if _DEBUG
#include <Es/Framework/optEnable.hpp>
#endif

#include "math3dP.hpp"
#include <Es/Framework/debugLog.hpp>

const Vector3P VZeroP(0,0,0);
const Vector3P VUpP(0,1,0);
const Vector3P VForwardP(0,0,1);
const Vector3P VAsideP(1,0,0);

const Matrix3P M3ZeroP
  (
  0,0,0,
  0,0,0,
  0,0,0
  );
const Matrix3P M3IdentityP
  (
  1,0,0,
  0,1,0,
  0,0,1
  );

const Matrix4P M4ZeroP
  (
  0,0,0,
  0,0,0,
  0,0,0,
  0,0,0
  );
const Matrix4P M4IdentityP
  (
  1,0,0,0,
  0,1,0,0,
  0,0,1,0
  );


#if _MSC_VER
bool Matrix4P::IsFinite() const
  {
  for( int i=0; i<3; i++ )
    {
    if( !_finite(Get(i,0)) ) return false;
    if( !_finite(Get(i,1)) ) return false;
    if( !_finite(Get(i,2)) ) return false;
    if( !_finite(GetPos(i)) ) return false;
    }
  return true;
  }

//--------------------------------------------------

bool Matrix3P::IsFinite() const
  {
  for( int i=0; i<3; i++ )
    {
    if( !_finite(Get(i,0)) ) return false;
    if( !_finite(Get(i,1)) ) return false;
    if( !_finite(Get(i,2)) ) return false;
    }
  return true;
  }

//--------------------------------------------------

#endif

float Matrix4P::Characteristic() const
  {
  // used in fast comparison
  // sum of all data members
  float sum1=0; // lower dependecies
  float sum2=0;
  for( int i=0; i<3; i++ )
    {
    sum1+=Get(i,0);
    sum2+=Get(i,1);
    sum1+=Get(i,2);
    sum2+=GetPos(i);
    }
  return sum1+sum2;
  }

//--------------------------------------------------

void Matrix4P::SetIdentity()
  {
  *this=M4IdentityP;
  }

//--------------------------------------------------

void Matrix4P::SetZero()
  {
  *this=M4ZeroP;
  }

//--------------------------------------------------

void Matrix4P::SetTranslation( Vector3PPar offset )
  {
  SetIdentity();
  SetPosition(offset);
  }

//--------------------------------------------------

void Matrix4P::SetRotationX( Coord angle )
  {
  SetIdentity();
  Coord s=(Coord)sin(angle),c=(Coord)cos(angle);
  Set(1,1)=+c,Set(1,2)=-s;
  Set(2,1)=+s,Set(2,2)=+c;
  }

//--------------------------------------------------

void Matrix4P::SetRotationY( Coord angle )
  {
  SetIdentity();
  Coord s=(Coord)sin(angle),c=(Coord)cos(angle);
  Set(0,0)=+c,Set(0,2)=-s;
  Set(2,0)=+s,Set(2,2)=+c;
  }

//--------------------------------------------------

void Matrix4P::SetRotationZ( Coord angle )
  {
  SetIdentity();
  Coord s=(Coord)sin(angle),c=(Coord)cos(angle);
  Set(0,0)=+c,Set(0,1)=-s;
  Set(1,0)=+s,Set(1,1)=+c;
  }

//--------------------------------------------------

void Matrix4P::SetScale( Coord x, Coord y, Coord z )
  {
  SetZero();
  Set(0,0)=x;
  Set(1,1)=y;
  Set(2,2)=z;
  //Set(3,3)=1;
  }

//--------------------------------------------------

void Matrix4P::SetPerspective( Coord cLeft, Coord cTop )
  {
  SetZero();
  // xg=x*near/right :: <-1,+1>
  Set(0,0)=1.0f/cLeft;
  // yg=y*near/top :: <-1,+1>
  Set(1,1)=1.0f/cTop;
  
  // zg=-w*1
  SetPos(2)=-1;
  
  // wg=z
  //Set(3,2)=1; 
  
  // this gives actual z result (suppose w=1) = zg/wg =
  // zRes(z)=-1/z;
  }

//--------------------------------------------------

void Matrix4P::Orthogonalize()
  {
  Vector3PVal dir=Direction();
  Vector3PVal up=DirectionUp();
  SetDirectionAndUp(dir,up);
  }

//--------------------------------------------------

void Matrix3P::Orthogonalize()
  {
  Vector3PVal dir=Direction();
  Vector3PVal up=DirectionUp();
  SetDirectionAndUp(dir,up);
  }

//--------------------------------------------------

void Matrix4P::SetOriented( Vector3PPar dir, Vector3PPar up )
  {
  SetIdentity();
  SetDirectionAndUp(dir,up);
  }

//--------------------------------------------------

void Matrix4P::SetView( Vector3PPar point, Vector3PPar dir, Vector3PPar up )
  {
  Matrix4P translate(MTranslation,-point);
  Matrix4P direction(MDirection,dir,up);
  SetMultiply(direction,translate);
  }

//--------------------------------------------------

void Matrix4P::SetMultiply( const Matrix4P &a, const Matrix4P &b )
  {
  // matrix multiplication
  int i,j;
  // b(3,0)=0, b(3,1)=0, b(3,2)=0, b(3,3)=1
  // a(3,0)=0, a(3,1)=0, a(3,2)=0, a(3,3)=1
  for( i=0; i<3; i++ ) for( j=0; j<3; j++ )
    {
    Set(i,j)=
      (
        a.Get(i,0)*b.Get(0,j)+
          a.Get(i,1)*b.Get(1,j)+
            a.Get(i,2)*b.Get(2,j)
              );
    }
  for( i=0; i<3; i++ )
    {
    SetPos(i)=
      (
        a.Get(i,0)*b.GetPos(0)+
          a.Get(i,1)*b.GetPos(1)+
            a.Get(i,2)*b.GetPos(2)+
              a.GetPos(i)
                );
    }
  //for( j=0; j<3; j++ ) Set(3,j)=0;
  //Set(3,3)=1;
  }

//--------------------------------------------------

void Matrix4P::SetMultiply( const Matrix4P &a, float b )
  {
  _orientation.SetMultiply(a._orientation,b);
  _position=a._position*b;
  }

//--------------------------------------------------

void Matrix4P::SetMultiplyByPerspective( const Matrix4P &a, const Matrix4P &b )
  {
  // matrix multiplication
  int i,j;
  // b is perspective projection
  // b(3,0)=0, b(3,1)=0, b(3,2)=1, b(3,3)=0
  // a(3,0)=0, a(3,1)=0, a(3,2)=0, a(3,3)=1
  for( i=0; i<3; i++ ) for( j=0; j<3; j++ )
    {
    Set(i,j)=
      (
        a.Get(i,0)*b.Get(0,j)+
          a.Get(i,1)*b.Get(1,j)+
            a.Get(i,2)*b.Get(2,j)+
              //a.Get(i,3)*b.Get(3,j)
              a.GetPos(i)*(j==2)
                );
    }
  for( i=0; i<3; i++ )
    {
    SetPos(i)=
      (
        a.Get(i,0)*b.GetPos(0)+
          a.Get(i,1)*b.GetPos(1)+
            a.Get(i,2)*b.GetPos(2)
              );
    }
  //for( j=0; j<3; j++ ) Set(3,j)=b.Get(3,j);
  //Set(3,3)=0;
  }

//--------------------------------------------------

inline float InvSquareSize( float x, float y, float z )
  {
  return Inv(x*x+y*y+z*z);
  }

//--------------------------------------------------

Vector3P Vector3P::Normalized() const
  {
  Coord size2=SquareSize();
  if( size2==0 ) return *this;
  Coord invSize=InvSqrt(size2);
  return Vector3P(X()*invSize,Y()*invSize,Z()*invSize);
  }

//--------------------------------------------------

void Vector3P::Normalize() // no return to avoid using instead of Normalized
  {
  Coord size2=SquareSize();
  if( size2==0 ) return;
  Coord invSize=InvSqrt(size2);
  Set(0)*=invSize,Set(1)*=invSize,Set(2)*=invSize;
  }

//--------------------------------------------------

Vector3P Vector3P::CrossProduct( Vector3PPar op ) const
  {
  //Vector3P ret;
  float x=Y()*op.Z()-Z()*op.Y();
  float y=Z()*op.X()-X()*op.Z();
  float z=X()*op.Y()-Y()*op.X();
  return Vector3P(x,y,z);
  }

//--------------------------------------------------

float Vector3P::CosAngle( Vector3PPar op ) const
  {
  return DotProduct(op)*InvSqrt(op.SquareSize()*SquareSize());
  }

//--------------------------------------------------

float Vector3P::Distance( Vector3PPar op ) const
  {
  return (*this-op).Size();
  }

//--------------------------------------------------

float Vector3P::Distance2( Vector3PPar op ) const
  {
  return (*this-op).SquareSize();
  }

//--------------------------------------------------

float Vector3P::DistanceXZ( Vector3PPar op ) const
  {
  return (*this-op).SizeXZ();
  }

//--------------------------------------------------

float Vector3P::DistanceXZ2( Vector3PPar op ) const
  {
  return (*this-op).SquareSizeXZ();
  }

//--------------------------------------------------

Vector3P Vector3P::Project( Vector3PPar op ) const
  {
  return op*DotProduct(op)*op.InvSquareSize();
  }

//--------------------------------------------------

#if _MSC_VER
bool Vector3P::IsFinite() const
  {
  if( !_finite(Get(0)) ) return false;
  if( !_finite(Get(1)) ) return false;
  if( !_finite(Get(2)) ) return false;
  return true;
  }

//--------------------------------------------------

#endif

#define NO_ASM 0
#define ASM_VC5 1
#define ASM_ICL45 2

#if !_PIII // special optimization for PIII

#if _MSC_VER
#define TRANSFORM_VER ASM_ICL45 // select which implementation should we use
#endif

#if TRANSFORM_VER==ASM_ICL45

// ICL 4.5 generated code - 47 cycles per Test project iteration

void __declspec(naked) Vector3P::SetFastTransform( const Matrix4P &a, Vector3PPar o )
  {
  __asm
    {
    mov         edx,dword ptr [esp+8]
      mov         eax,dword ptr [esp+4]
        fld         dword ptr [eax+18h]
          fmul        dword ptr [edx+8]
            fadd        dword ptr [eax+24h]
              fld         dword ptr [eax+0Ch]
                fmul        dword ptr [edx+4]
                  faddp       st(1),st
                    fld         dword ptr [eax]
                      fmul        dword ptr [edx]
                        faddp       st(1),st
                          fld         dword ptr [edx+8]
                            fmul        dword ptr [eax+20h]
                              fadd        dword ptr [eax+2Ch]
                                fld         dword ptr [edx+8]
                                  fmul        dword ptr [eax+1Ch]
                                    fadd        dword ptr [eax+28h]
                                      fld         dword ptr [eax+4]
                                        fmul        dword ptr [edx]
                                          fld         dword ptr [eax+10h]
                                            fmul        dword ptr [edx+4]
                                              faddp       st(2),st
                                                faddp       st(1),st
                                                  fld         dword ptr [edx+4]
                                                    fmul        dword ptr [eax+14h]
                                                      faddp       st(2),st
                                                        fld         dword ptr [edx]
                                                          fmul        dword ptr [eax+8]
                                                            faddp       st(2),st
                                                              fstp        dword ptr [ecx+4]
                                                                fstp        dword ptr [ecx+8]
                                                                  fstp        dword ptr [ecx]
                                                                    ret         8
    }
  }

//--------------------------------------------------

#elif TRANSFORM_VER==ASM_VC5

// VC++ 5 generated code - 53 cycles per iteration

void __declspec(naked) Vector3P::SetFastTransform( const Matrix4P &a, Vector3PPar o )
  {
  __asm
    {
    mov         eax,dword ptr [esp+4]
      fld         dword ptr [eax+18h]
        mov         edx,dword ptr [esp+8]
          fmul        dword ptr [edx+8]
            fld         dword ptr [eax+0Ch]
              fmul        dword ptr [edx+4]
                faddp       st(1),st
                  fld         dword ptr [edx]
                    fmul        dword ptr [eax]
                      faddp       st(1),st
                        fadd        dword ptr [eax+24h]
                          fld         dword ptr [eax+1Ch]
                            fmul        dword ptr [edx+8]
                              fld         dword ptr [eax+10h]
                                fmul        dword ptr [edx+4]
                                  faddp       st(1),st
                                    fld         dword ptr [eax+4]
                                      fmul        dword ptr [edx]
                                        faddp       st(1),st
                                          fadd        dword ptr [eax+28h]
                                            fstp        dword ptr [esp+8]
                                              fld         dword ptr [eax+14h]
                                                fmul        dword ptr [edx+4]
                                                  fld         dword ptr [eax+20h]
                                                    fmul        dword ptr [edx+8]
                                                      faddp       st(1),st
                                                        fld         dword ptr [eax+8]
                                                          fmul        dword ptr [edx]
                                                            faddp       st(1),st
                                                              fadd        dword ptr [eax+2Ch]
                                                                mov         eax,dword ptr [esp+8]
                                                                  fstp        dword ptr [esp+4]
                                                                    mov         edx,dword ptr [esp+4]
                                                                      fstp        dword ptr [ecx]
                                                                        mov         dword ptr [ecx+4],eax
                                                                          mov         dword ptr [ecx+8],edx
                                                                            ret         8
    }
  }

//--------------------------------------------------

#else

// no handoptimized assembly - use C version
// VC++6 - 64 cycles per iteration

void Vector3P::SetFastTransform( const Matrix4P &a, Vector3PPar o )
  {
  float r0=a.Get(0,0)*o[0]+a.Get(0,1)*o[1]+a.Get(0,2)*o[2]+a.GetPos(0);
  float r1=a.Get(1,0)*o[0]+a.Get(1,1)*o[1]+a.Get(1,2)*o[2]+a.GetPos(1);
  float r2=a.Get(2,0)*o[0]+a.Get(2,1)*o[1]+a.Get(2,2)*o[2]+a.GetPos(2);
  Set(0)=r0;
  Set(1)=r1;
  Set(2)=r2;
  }

//--------------------------------------------------

#endif

#endif

void Vector3P::SetMultiplyLeft( Vector3PPar o, const Matrix3P &a )
  { // vector rotation - only 3x3 matrix is used, translation is ignored
  // u=M*v
  float o0=o[0],o1=o[1],o2=o[2];
  for( int i=0; i<3; i++ )
    {
    Set(i)=a.Get(0,i)*o0+a.Get(1,i)*o1+a.Get(2,i)*o2;
    }
  }

//--------------------------------------------------

void Matrix4P::SetInvertRotation( const Matrix4P &op )
  {
  // invert orientation
  _orientation.SetInvertRotation(op.Orientation());
  // invert translation
  SetPosition(Rotate(-op.Position()));
  }

//--------------------------------------------------

void Matrix4P::SetInvertScaled( const Matrix4P &op )
  {
  // invert orientation
  _orientation.SetInvertScaled(op.Orientation());
  // invert translation
  SetPosition(Rotate(-op.Position()));
  }

//--------------------------------------------------

void Matrix4P::SetInvertGeneral( const Matrix4P &op )
  {
  // invert orientation
  _orientation.SetInvertGeneral(op.Orientation());
  // invert translation
  SetPosition(Rotate(-op.Position()));
  }

//--------------------------------------------------

void Matrix3P::SetNormalTransform( const Matrix3P &op )
  {
  // normal transformation for scale matrix (a,b,c) is (1/a,1/b,1/c)
  // all matrices we use are rotation combined with scale
  SetIdentity();
  int j;
  float invRow0size2=InvSquareSize(op.Get(0,0),op.Get(0,1),op.Get(0,2));
  float invRow1size2=InvSquareSize(op.Get(1,0),op.Get(1,1),op.Get(1,2));
  float invRow2size2=InvSquareSize(op.Get(2,0),op.Get(2,1),op.Get(2,2));
  for( j=0; j<3; j++ )
    {
    Set(0,j)=op.Get(0,j)*invRow0size2;
    Set(1,j)=op.Get(1,j)*invRow1size2;
    Set(2,j)=op.Get(2,j)*invRow2size2;
    }
  }

//--------------------------------------------------

// implementation of 3x3 matrices

void Matrix3P::SetIdentity()
  {
  *this=M3IdentityP;
  }

//--------------------------------------------------

void Matrix3P::SetZero()
  {
  *this=M3ZeroP;
  }

//--------------------------------------------------

void Matrix3P::SetRotationX( Coord angle )
  {
  Coord s=sin(angle),c=cos(angle);
  SetIdentity();
  Set(1,1)=+c,Set(1,2)=-s;
  Set(2,1)=+s,Set(2,2)=+c;
  }

//--------------------------------------------------

void Matrix3P::SetRotationY( Coord angle )
  {
  Coord s=sin(angle),c=cos(angle);
  SetIdentity();
  Set(0,0)=+c,Set(0,2)=-s;
  Set(2,0)=+s,Set(2,2)=+c;
  }

//--------------------------------------------------

void Matrix3P::SetRotationZ( Coord angle )
  {
  Coord s=(Coord)sin(angle),c=(Coord)cos(angle);
  SetIdentity();
  Set(0,0)=+c,Set(0,1)=-s;
  Set(1,0)=+s,Set(1,1)=+c;
  }

//--------------------------------------------------

void Matrix3P::SetScale( Coord x, Coord y, Coord z )
  {
  SetZero();
  Set(0,0)=x;
  Set(1,1)=y;
  Set(2,2)=z;
  //Set(3,3)=1;
  }

//--------------------------------------------------

void Matrix3P::SetScale( float scale )
  {
  // note: old scale may be zero
  float invOldScale=InvScale();
  if( invOldScale>0 )
    {
    //Matrix3P rescale(MScale,scale*invOldScale);
    //(*this)=(*this)*rescale;
    (*this)*=scale*invOldScale;
    }
  else
    {
    SetScale(scale,scale,scale);
    }
  // note: any SetDirection will remove scale
  }

//--------------------------------------------------

float Matrix3P::Scale() const
  {
  // Frame transformation is composed from
  // rotation*scale
  // current scale can be determined as
  // orient * transpose orient
  
  //Matrix3P invR=InverseRotation();
  //Matrix3P oTo=(*this)*invR;
  // note all but [i][i] should be zero
  //float sx2=oTo(0,0);
  //float sy2=oTo(1,1);
  //float sz2=oTo(2,2);
  
  // optimized matrix transposition + multiplication
  Vector3P sv;
  for( int i=0; i<3; i++ )
    {
    sv[i]=Square(Get(i,0))+Square(Get(i,1))+Square(Get(i,2));
    }
  
  // calculate average scale
  float scale2=(sv[0]+sv[1]+sv[2])*(1.0/3);
  return scale2*InvSqrt(scale2);
  }

//--------------------------------------------------

float Matrix3P::InvScale() const
  {
  // Frame transformation is composed from
  // translation*rotation*scale
  // current scale can be determined as
  // orient * transpose orient
  /*
	Matrix3P oTo=(*this)*InverseRotation();
	// note all but [i][i] should be zero
	float sx2=oTo(0,0);
	float sy2=oTo(1,1);
	float sz2=oTo(2,2);
	// calculate average scale
	float scale2=(sx2+sy2+sz2)*(1.0/3);
	*/
  
  Vector3P sv;
  for( int i=0; i<3; i++ )
    {
    sv[i]=Square(Get(i,0))+Square(Get(i,1))+Square(Get(i,2));
    }
  
  // calculate average scale
  float scale2=(sv[0]+sv[1]+sv[2])*(1.0/3);
  
  if( scale2<=0 ) return 0; // singular matrix
  return InvSqrt(scale2);
  }

//--------------------------------------------------

void Matrix3P::SetDirectionAndUp( Vector3PPar dir, Vector3PPar up )
  {
  SetDirection(dir.Normalized());
  // Project into the plane
  Coord t=up*Direction();
  SetDirectionUp((up-Direction()*t).Normalized());
  // Calculate the vector pointing along the x axis (i.e. aside)
  // sign is experimental - I never know what is right and what is left
  // no need to normalize - cross product of two perpendicular unit vectors is unit vector
  SetDirectionAside(DirectionUp().CrossProduct(Direction()));
  }

//--------------------------------------------------

void Matrix3P::SetDirectionAndAside( Vector3PPar dir, Vector3PPar aside )
  {
  SetDirection(dir.Normalized());
  // Project into the plane
  Coord t=aside*Direction();
  SetDirectionAside((aside-Direction()*t).Normalized());
  // Calculate the vector pointing along the x axis (i.e. aside)
  // sign is experimental - I never know what is right and what is left
  // no need to normalize - cross product of two perpendicular unit vectors is unit vector
  SetDirectionUp(DirectionAside().CrossProduct(Direction()));
  }

//--------------------------------------------------

void Matrix3P::SetUpAndAside( Vector3PPar up, Vector3PPar aside )
  {
  SetDirectionUp(up.Normalized());
  // Project into the plane
  Coord t=DirectionUp()*aside;
  SetDirectionAside((aside-DirectionUp()*t).Normalized());
  // Calculate the vector pointing along the x axis (i.e. aside)
  // sign is experimental - I never know what is right and what is left
  // no need to normalize - cross product of two perpendicular unit vectors is unit vector
  SetDirection(DirectionAside().CrossProduct(DirectionUp()));
  }

//--------------------------------------------------

void Matrix3P::SetUpAndDirection( Vector3PPar up, Vector3PPar dir )
  {
  SetDirectionUp(up.Normalized());
  // Project into the plane
  Coord t=DirectionUp()*dir;
  // Calculate the vector pointing along the x axis (i.e. aside)
  // sign is experimental - I never know what is right and what is left
  // no need to normalize - cross product of two perpendicular unit vectors is unit vector
  SetDirection((dir-DirectionUp()*t).Normalized());
  SetDirectionAside(DirectionUp().CrossProduct(Direction()));
  }

//--------------------------------------------------

void Matrix3P::SetTilda( Vector3PPar a )
  {
  SetZero();
  Set(0,1)=-a[2],Set(0,2)=+a[1];
  Set(1,0)=+a[2],Set(1,2)=-a[0];
  Set(2,0)=-a[1],Set(2,1)=+a[0];
  //Assert( ((*this)*a).SquareSize()<1e-10 );
  }

//--------------------------------------------------

void Matrix3P::operator *= ( float op )
  {
  _aside*=op;
  _up*=op;
  _dir*=op;
  }

//--------------------------------------------------

void Matrix3P::SetMultiply( const Matrix3P &a, const Matrix3P &b )
  {
  // matrix multiplication
  for( int i=0; i<3; i++ ) for( int j=0; j<3; j++ )
    {
    Set(i,j)=
      (
        a.Get(i,0)*b.Get(0,j)+
          a.Get(i,1)*b.Get(1,j)+
            a.Get(i,2)*b.Get(2,j)
              );
    }
  }

//--------------------------------------------------

void Matrix3P::SetMultiply( const Matrix3P &a, float op )
  {
  // matrix multiplication
  _aside=a._aside*op;
  _up=a._up*op;
  _dir=a._dir*op;
  }

//--------------------------------------------------

Matrix3P Matrix3P::operator + ( const Matrix3P &a ) const
  {
  Matrix3P res;
  res._aside=_aside+a._aside;
  res._up=_up+a._up;
  res._dir=_dir+a._dir;
  return res;
  }

//--------------------------------------------------

Matrix3P Matrix3P::operator - ( const Matrix3P &a ) const
  {
  Matrix3P res;
  res._aside=_aside-a._aside;
  res._up=_up-a._up;
  res._dir=_dir-a._dir;
  return res;
  }

//--------------------------------------------------

Matrix3P &Matrix3P::operator -= ( const Matrix3P &a )
  {
  _aside-=a._aside;
  _up-=a._up;
  _dir-=a._dir;
  return *this;
  }

//--------------------------------------------------

Matrix3P &Matrix3P::operator += ( const Matrix3P &a )
  {
  _aside+=a._aside;
  _up+=a._up;
  _dir+=a._dir;
  return *this;
  }

//--------------------------------------------------

Matrix4P Matrix4P::operator + ( const Matrix4P &a ) const
  {
  Matrix4P res;
  res._orientation._aside=_orientation._aside+a._orientation._aside;
  res._orientation._up=_orientation._up+a._orientation._up;
  res._orientation._dir=_orientation._dir+a._orientation._dir;
  res._position=_position+a._position;
  return res;
  }

//--------------------------------------------------

Matrix4P Matrix4P::operator - ( const Matrix4P &a ) const
  {
  Matrix4P res;
  res._orientation._aside=_orientation._aside-a._orientation._aside;
  res._orientation._up=_orientation._up-a._orientation._up;
  res._orientation._dir=_orientation._dir-a._orientation._dir;
  res._position=_position-a._position;
  return res;
  }

//--------------------------------------------------

void Matrix4P::operator += ( const Matrix4P &op )
  {
  _orientation._aside+=op._orientation._aside;
  _orientation._up+=op._orientation._up;
  _orientation._dir+=op._orientation._dir;
  _position+=op._position;
  }

//--------------------------------------------------

void Matrix4P::operator -= ( const Matrix4P &op )
  {
  _orientation._aside-=op._orientation._aside;
  _orientation._up-=op._orientation._up;
  _orientation._dir-=op._orientation._dir;
  _position-=op._position;
  }

//--------------------------------------------------

void Matrix3P::SetInvertRotation( const Matrix3P &op )
  {
  // matrix inversion is calculated based on these prepositions:
  SetIdentity();
  for( int i=0; i<3; i++ ) //for( int j=0; j<3; j++ )
    {
    Set(i,0)=op.Get(0,i);
    Set(i,1)=op.Get(1,i);
    Set(i,2)=op.Get(2,i);
    }
  }

//--------------------------------------------------

//#define FailF(format,args) DebF("%s(%d) : Format",__FILE__,__LINE__,args)

void Matrix3P::SetInvertScaled( const Matrix3P &op )
  {
  #if 0
  // TODO: correct bug
    {
    // verify matrix is S*R
    // scaled matrix has form S*R, where S is scale matrix and R is rotation
    // (S*R)*(RT*ST)=S*ST=S*S
    Matrix3P t;
    for( int i=0; i<3; i++ )
      {
      t(0,i)=op(i,0),t(1,i)=op(i,1),t(2,i)=op(i,2);
      }
    Matrix3PVal se=op*t;
    // check if se is diagonal
    const float max=1e-4;
    float maxNSR=0;
    saturateMax(maxNSR,fabs(se(0,1)));
    saturateMax(maxNSR,fabs(se(0,2)));
    saturateMax(maxNSR,fabs(se(1,0)));
    saturateMax(maxNSR,fabs(se(1,2)));
    saturateMax(maxNSR,fabs(se(2,0)));
    saturateMax(maxNSR,fabs(se(2,1)));
    if( maxNSR>max )
      {
      LogF("%s(%d) : %f",__FILE__,__LINE__,maxNSR);
      }
    }
  #endif
  // matrix inversion is calculated based on these prepositions:
  // matrix is S*R, where S is scale, R is rotation
  // inversion of such matrix is Inv(S)*Inv(R)
  // Inv(R) is Transpose(R), Inv(S) is C: C(i,i)=1/S(i,i)
  // sizes of row(i) are scale coeficients a,b,c
  // all member are set
  //SetIdentity();
  // calculate scale
  float invScale0=InvSquareSize(op.Get(0,0),op.Get(0,1),op.Get(0,2));
  float invScale1=InvSquareSize(op.Get(1,0),op.Get(1,1),op.Get(1,2));
  float invScale2=InvSquareSize(op.Get(2,0),op.Get(2,1),op.Get(2,2));
  // invert rotation and scale
  for( int i=0; i<3; i++ ) //for( int j=0; j<3; j++ )
    {
    Set(i,0)=op.Get(0,i)*invScale0;
    Set(i,1)=op.Get(1,i)*invScale1;
    Set(i,2)=op.Get(2,i)*invScale2;
    }
  }

//--------------------------------------------------

#define swap(a,b) {float p;p=a;a=b;b=p;}

void Matrix3P::SetInvertGeneral( const Matrix3P &op )
  {
  #if 1
  // TODO: detect general matrices as soon as possible
  // check if they are really necessary
  // check if matrix is really general
  // if not, we can use scaled version
  // scaled matrix has form S*R, where S is scale matrix and R is rotation
  // (S*R)*(RT*ST)=S*ST=S*S
  // 
  Matrix3P t;
  for( int i=0; i<3; i++ )
    {
    t(0,i)=op(i,0),t(1,i)=op(i,1),t(2,i)=op(i,2);
    }
  Matrix3PVal se=op*t;
  // check if se is diagonal
  #if 0
  static int diag=0;
  static int unit=0;
  static int total=0;
  if( total>1000 )
    {
    LogF("G Diagonal: %.1f %% matrices",diag*100.0/total);
    total=0;
    diag=0;
    }
  total++;
  #endif
  const float max=1e-6;
  if
    (
      fabs(se(0,1))<max && fabs(se(0,2))<max &&
        fabs(se(1,0))<max && fabs(se(1,2))<max &&
          fabs(se(2,0))<max && fabs(se(2,1))<max
            )
              {
              // matrix is diagonal - use special case inversion
              //diag++;
              SetInvertScaled(op);
              return ;
              }
  #endif
  
  
  // calculate inversion using Gauss-Jordan elimination
  Matrix3P a=op;
  // load result with identity
  SetIdentity();
  int row,col;
  // construct result by pivoting
  // pivot column
  for( col=0; col<3; col++ )
    {
    // use maximal number as pivot
    float max=0;
    int maxRow=col;
    for( row=col; row<3; row++ )
      {
      float mag=fabs(a.Get(row,col));
      if( max<mag ) max=mag,maxRow=row;
      }
    if( max<=0.0 ) continue; // no pivot exists
    // swap lines col and maxRow
    swap(a.Set(col,0),a.Set(maxRow,0));
    swap(a.Set(col,1),a.Set(maxRow,1));
    swap(a.Set(col,2),a.Set(maxRow,2));
    swap(Set(col,0),Set(maxRow,0));
    swap(Set(col,1),Set(maxRow,1));
    swap(Set(col,2),Set(maxRow,2));
    // use a(col,col) as pivot
    float quotient=1/a.Get(col,col);
    // make pivot 1
    a.Set(col,0)*=quotient,a.Set(col,1)*=quotient,a.Set(col,2)*=quotient;
    Set(col,0)*=quotient,Set(col,1)*=quotient,Set(col,2)*=quotient;
    // use pivot line to zero all other lines
    for( row=0; row<3; row++ ) if( row!=col )
      {
      float factor=a.Get(row,col);
      a.Set(row,0)-=a.Get(col,0)*factor;
      a.Set(row,1)-=a.Get(col,1)*factor;
      a.Set(row,2)-=a.Get(col,2)*factor;
      Set(row,0)-=Get(col,0)*factor;
      Set(row,1)-=Get(col,1)*factor;
      Set(row,2)-=Get(col,2)*factor;
      }
    }
  // result constructed
  }

//--------------------------------------------------

// general calculations

void SaturateMin( Vector3P &min, Vector3PPar val )
  {
  saturateMin(min[0],val[0]);
  saturateMin(min[1],val[1]);
  saturateMin(min[2],val[2]);
  }

//--------------------------------------------------

void SaturateMax( Vector3P &max, Vector3PPar val )
  {
  saturateMax(max[0],val[0]);
  saturateMax(max[1],val[1]);
  saturateMax(max[2],val[2]);
  }

//--------------------------------------------------

void CheckMinMax( Vector3P &min, Vector3P &max, Vector3PPar val )
  {
  saturateMin(min[0],val[0]),saturateMax(max[0],val[0]);
  saturateMin(min[1],val[1]),saturateMax(max[1],val[1]);
  saturateMin(min[2],val[2]),saturateMax(max[2],val[2]);
  }

//--------------------------------------------------

Vector3P VectorMin( Vector3PPar a, Vector3PPar b )
  {
  return Vector3P
    (
      floatMin(a[0],b[0]),floatMin(a[1],b[1]),floatMin(a[2],b[2])
        );
  }

//--------------------------------------------------

Vector3P VectorMax( Vector3PPar a, Vector3PPar b )
  {
  return Vector3P
    (
      floatMax(a[0],b[0]),floatMax(a[1],b[1]),floatMax(a[2],b[2])
        );
  }

//--------------------------------------------------

