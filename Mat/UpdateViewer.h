/*
  UpdateViewer.h
  --------------

  October 2003

  Update Buldozer with current Lod.
*/


#ifndef _update_viewer_h_
#define _update_viewer_h_


#include "View\objlod.hpp"


//update Buldozer with current Lod
int UpdateViewer(LODObject* obj);

//close Buldozer
void CloseViewer();


#endif
