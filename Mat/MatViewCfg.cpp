/*
  Cfg.cpp
  -------

  Antonin Malik, February 2004

  Operation with mat.cfg and /ConfigStage
*/


#include "stdafx.h"
#include "MatDoc.h"
#include "MatView.h"
#include "Material.hpp"
#include "El\ParamFile\paramFile.hpp"


//default paths
#define BULD_PATH         "p:\\buldozer.exe -noland -dx -window -cfg=p:\\Buldozer.cfg"
#define MAT_PATH          "p:\\" 
#define EXT_EDITOR        "notepad.exe"
#define COEF_GRAY         0.2
#define SPHERE_PATH       "data3d\\Sphere.p3d"
#define SPHERE2_PATH      "data3d\\Sphere2.p3d"
#define BOX_PATH          "data3d\\Box.p3d"
#define CONE_PATH         "data3d\\Cone.p3d"
#define CONE2_PATH        "data3d\\Cone2.p3d"
#define DEFAULT_MAT_PATH  "p:\\DefaultMaterials\\"
#define CFG_STAGE_PATH    "c:\\Bis\\Mat\\ConfigStage\\"
#define TEXVIEW_PATH      "c:\\Bis\\Tex\\texView.exe"
#define SS_SERVER_NAME    "\\\\new_server\\vssdatap\\dataP"


//---------------------------------   cfg   -------------------------------------------------------


//create config file
//this function is called only if mat.cfg does not exist
void CMatView::CreateCfg()
{
  ParamFile f;
  f.Add("BuldozerPath", BULD_PATH);
  f.Add("MaterialPath", MAT_PATH);
  f.Add("AutomaticStartWithBuldozer", "yes");
  f.Add("CloseBuldozerAtEnd", "yes");
  ParamEntryPtr ps= f.AddArray("ComboPixelShaderID");
  ps->ReserveArrayElements(6);
  ps->AddValue("Normal");
  ps->AddValue("NormalDXTA");
  ps->AddValue("NormalMap");
  ps->AddValue("NormalMapThrough");
  ps->AddValue("Detail");
  ps->AddValue("Water");
  ParamEntryPtr vs= f.AddArray("ComboVertexShaderID");
  vs->ReserveArrayElements(4);
  vs->AddValue("Basic");
  vs->AddValue("NormalMap");
  vs->AddValue("ShadowVolume");
  vs->AddValue("Water");
  ParamEntryPtr uvTex= f.AddArray("ComboUVTexture");
  uvTex->ReserveArrayElements(2);
  uvTex->AddValue("tex");
  uvTex->AddValue("none");
  f.Add("ExternalEditor", EXT_EDITOR);
  f.Add("TexViewPath", TEXVIEW_PATH);
  f.Add("DisabledMaterialColor", "gray");
  f.Add("CoefGray", (float) COEF_GRAY);
  f.Add("DisableUnselectedMaterials", "no");
  f.Add("Sphere", SPHERE_PATH);
  f.Add("Sphere2", SPHERE2_PATH);
  f.Add("Box", BOX_PATH);
  f.Add("Cone", CONE_PATH);
  f.Add("Cone2", CONE2_PATH);
  f.Add("PathToDefaultMaterials", DEFAULT_MAT_PATH);
  f.Add("LastP3dPath", MAT_PATH);
  f.Add("LastMaterialPath", MAT_PATH);
  f.Add("LastTexturePath", MAT_PATH);
  f.Add("ConfigStagePath", CFG_STAGE_PATH);
  f.Add("SharedMemory", "yes");
  f.Add("SourceSafeServerName", SS_SERVER_NAME);
  f.Save(_cfg_file);
  ReadCfg();
  OnOptionsConfiguration();
};


//read "config.cfg"
//vola pouze OnInitialUpdate()
void CMatView::ReadCfg()
{
  ParamFile f; 
  ConstParamEntryPtr entry;
  RString str;
  int entry_size;
  BOOL run_configuration= FALSE;

  if ( f.Parse(_cfg_file) != LSOK )
  {
    LogF("Cfg is in wrong format, a new one cfg was created.");
    MessageBox("Cfg is in wrong format, a new one cfg was created.", 0, MB_ICONINFORMATION | MB_OK);
    CreateCfg();         //vola v sobe ReadCfg() a spusti konfiguracni dialog
    return;
  }
  if ( !f.GetEntryCount() ) {
    LogF("No config file.");
		MessageBox("No config file, a new one was created.", 0, MB_ICONINFORMATION | MB_OK);
    CreateCfg();          //vola v sobe ReadCfg() a spusti konfiguracni dialog
    return;
  };
  //BuldozerPath
  entry= f.FindEntry("BuldozerPath");
  if ( !entry )
  {
    _buld_path= BULD_PATH;
    run_configuration= TRUE;
    LogF("Entry 'BuldozerPath' doesn't exist.");
  }
  else
    _buld_path= entry->GetValue();
  //MaterialPath
  entry= f.FindEntry("MaterialPath");
  if ( !entry )
  {
    _mat_path= MAT_PATH;
    run_configuration= TRUE;
    LogF("Entry 'MaterialPath' doesn't exist.");
  }
  else
    _mat_path= _last_p3d_path= entry->GetValue();
  doc->_mats->SetMatPath(_mat_path);
	//info about Bulodozer starting and ending
	entry= f.FindEntry("AutomaticStartWithBuldozer");
  if ( !entry )
  {
    LogF("Entry 'AutomaticStartWithBuldozer' doesn't exist.");
    _automatic_buldozer_start= TRUE;
    run_configuration= TRUE;
  }
	else
	{
	  str= entry->GetValue(); 
		str.Lower();
		if ( str == (RString) "yes" )
      _automatic_buldozer_start= TRUE;
		else
		{
			if ( str == (RString) "no" )
        _automatic_buldozer_start= FALSE;
			else
				_automatic_buldozer_start= TRUE;
		}
	}
	entry= f.FindEntry("CloseBuldozerAtEnd");
  if ( !entry )
  {
    LogF("Entry 'CloseBuldozerAtEnd' doesn't exist.");
    _automatic_buldozer_end= TRUE;
    run_configuration= TRUE;
  }
	else
	{
	  str= entry->GetValue(); 
		str.Lower();
		if ( str == (RString) "yes" )
      _automatic_buldozer_end= TRUE;
		else
		{
			if ( str == (RString) "no" )
        _automatic_buldozer_end= FALSE;
			else
				_automatic_buldozer_end= TRUE;
		}
	}
	//combos - PixelShaderID & VertexShaderID
	entry= f.FindEntry("ComboPixelShaderID");
  if ( !entry )
  {
    LogF("Entry 'ComboPixelShaderID' doesn't exist.");
    run_configuration= TRUE;
  	_combo_PSI.ResetContent();
		_combo_PSI.InsertString(-1, "Normal");
		_combo_PSI.InsertString(-1, "NormalDXTA");
		_combo_PSI.InsertString(-1, "NormalMap");
		_combo_PSI.InsertString(-1, "NormalMapThrough");
		_combo_PSI.InsertString(-1, "Detail");
		_combo_PSI.InsertString(-1, "Water");
  }
  else
  {
    if ( !entry->IsArray() )
      LogF("Entry 'ComboPixelShaderID' is not an array.");
    else 
    {
			_combo_PSI.ResetContent();
			for ( int i= 0; i < entry->GetSize(); i++ )
				_combo_PSI.InsertString(-1, (const char*) (RString) entry->operator[](i) );
		}
	}
	entry= f.FindEntry("ComboVertexShaderID");
  if ( !entry )
  {
    LogF("Entry 'ComboVertexShaderID' doesn't exist.");
    run_configuration= TRUE;
		_combo_VSI.ResetContent();
		_combo_VSI.InsertString(-1, "Basic");
		_combo_VSI.InsertString(-1, "NormalMap");
		_combo_VSI.InsertString(-1, "ShadowVolume");
		_combo_VSI.InsertString(-1, "Water");
  }
  else
  {
    if ( !entry->IsArray() )
      LogF("Entry 'ComboVertexShaderID' is not an array.");
    else 
    {
			_combo_VSI.ResetContent();
			for ( int i= 0; i < entry->GetSize(); i++ )
				_combo_VSI.InsertString(-1, (const char*) (RString) entry->operator[](i) );
		}
	}
  //uvTexture
	entry= f.FindEntry("ComboUVTexture");
  if ( !entry )
  {
    LogF("Entry 'ComboUVTexture' doesn't exist.");
    run_configuration= TRUE;
  	_combo_uvTex.ResetContent();
  	_combo_uvTex.InsertString(-1, "tex");
  	_combo_uvTex.InsertString(-1, "none");
  }
  else
  {
    if ( !entry->IsArray() )
      LogF("Entry 'ComboUVTexture' is not an array.");
    else 
    {
			_combo_uvTex.ResetContent();
			for ( int i= 0; i < entry->GetSize(); i++ )
				_combo_uvTex.InsertString(-1, (const char*) (RString) entry->operator[](i) );
		}
	}
  //external editor
  entry= f.FindEntry("ExternalEditor");
  if ( !entry )
  {
    LogF("Entry 'ExternalEditor' doesn't exist.");
    _extern_editor= EXT_EDITOR;
    run_configuration= TRUE;
  }
  else
    _extern_editor= entry->GetValue();
  //TexViewPath
  entry= f.FindEntry("TexViewPath");
  if ( !entry )
  {
    LogF("Entry 'TexViewPath' doesn't exist.");
    _texview_path= TEXVIEW_PATH;
    run_configuration= TRUE;
  }
  else
    _texview_path= entry->GetValue();
  //disable material color
  entry= f.FindEntry("DisabledMaterialColor");
  if ( !entry )
  {
    LogF("Entry 'DisabledMaterialColor' doesn't exist.");
    _dis_mats_col= DMC_GRAY;
  }
  else
  {
    RString str= entry->GetValue();
    str.Lower();
    if ( !strcmp(str, "gray") )
      _dis_mats_col= DMC_GRAY;
    else if ( !strcmp(str, "red") )
      _dis_mats_col= DMC_RED;
    else if ( !strcmp(str, "green") )
      _dis_mats_col= DMC_GREEN;
    else if ( !strcmp(str, "blue") )
      _dis_mats_col= DMC_BLUE;
    else 
      _dis_mats_col= DMC_GRAY;
  }
  g_unselected_mats= _dis_mats_col;
  //coef gray
  entry= f.FindEntry("CoefGray");
  if ( !entry )
  {
    LogF("Entry 'CoefGray' doesn't exist.");
    g_coef_gray= COEF_GRAY;
    run_configuration= TRUE;
  }
  else
    g_coef_gray= *entry;
  //disable unselected materials
	entry= f.FindEntry("DisableUnselectedMaterials");
  if ( !entry )
  {
    LogF("Entry 'DisableUnselectedMaterials' doesn't exist.");
    _disable_unselect_mats= TRUE;
    run_configuration= TRUE;
  }
	else
	{
	  str= entry->GetValue(); 
		str.Lower();
		if ( str == (RString) "yes" )
      _disable_unselect_mats= TRUE;
		else
		{
			if ( str == (RString) "no" )
        _disable_unselect_mats= FALSE;
			else
				_disable_unselect_mats= TRUE;
		}
	}
  //primitives
  entry= f.FindEntry("Sphere");
  if ( !entry )
  {
    LogF("Entry 'Sphere' doesn't exist.");
    _sphere_path= SPHERE_PATH;
  }
  else
    _sphere_path= entry->GetValue();
  entry= f.FindEntry("Sphere2");
  if ( !entry )
  {
    LogF("Entry 'Sphere2' doesn't exist.");
    _sphere2_path= SPHERE2_PATH;
  }
  else
    _sphere2_path= entry->GetValue();
  entry= f.FindEntry("Box");
  if ( !entry )
  {
    LogF("Entry 'Box' doesn't exist.");
   _box_path= BOX_PATH;
  }
  else
    _box_path= entry->GetValue();
  entry= f.FindEntry("Cone");
  if ( !entry )
  {
    LogF("Entry 'Cone' doesn't exist.");
   _cone_path= CONE_PATH;
  }
  else
    _cone_path= entry->GetValue();
  entry= f.FindEntry("Cone2");
  if ( !entry )
  {
    LogF("Entry 'Cone2' doesn't exist.");
   _cone2_path= CONE2_PATH;
  }
  else
    _cone2_path= entry->GetValue();
  //path to the default materials
  entry= f.FindEntry("PathToDefaultMaterials");
  if ( !entry )
  {
    LogF("Entry 'PathToDefaultMaterials' doesn't exist.");
    _default_mat_path= DEFAULT_MAT_PATH;
    run_configuration= TRUE;
  }
  else
    _default_mat_path= entry->GetValue();
  //_last_p3d_path
  entry= f.FindEntry("LastP3dPath");
  if ( !entry )
  {
    LogF("Entry 'LastP3dPath' doesn't exist.");
    _last_p3d_path= _mat_path;
    run_configuration= TRUE;
  }
  else
    _last_p3d_path= entry->GetValue();
  //_last_mat_path
  entry= f.FindEntry("LastMaterialPath");
  if ( !entry )
  {
    LogF("Entry 'LastMaterialPath' doesn't exist.");
    _last_mat_path= _mat_path;
    run_configuration= TRUE;
  }
  else
    _last_mat_path= entry->GetValue();
  //_last_tex_path
  entry= f.FindEntry("LastTexturePath");
  if ( !entry )
  {
    LogF("Entry 'LastTexturePath' doesn't exist.");
    _last_tex_path= _mat_path;
    run_configuration= TRUE;
  }
  else
    _last_tex_path= entry->GetValue();
  //_cfg_stage_path
  entry= f.FindEntry("ConfigStagePath");
  if ( !entry )
  {
    LogF("Entry 'ConfigStagePath' doesn't exist.");
    _cfg_stage_path= CFG_STAGE_PATH;
    if ( _cfg_stage_path[_cfg_stage_path.GetLength()-1] != '\\' )
      _cfg_stage_path= _cfg_stage_path + "\\";
  }
  else
    _cfg_stage_path= entry->GetValue();
  entry= f.FindEntry("SharedMemory");
  if ( !entry )
  {
    LogF("Entry 'SharedMemory' doesn't exist.");
    _shared_mem= TRUE;
  }
  else
  {
    str= entry->GetValue(); 
    str.Lower();
    if ( str == (RString) "no" )
      _shared_mem= FALSE;
    else
      _shared_mem= TRUE;
  }
  //SS server name
  entry= f.FindEntry("SourceSafeServerName");
  if ( !entry )
  {
    LogF("Entry 'SourceSafeServerName' doesn't exist.");
    _ss_server_name= SS_SERVER_NAME;
  }
  else
    _ss_server_name= entry->GetValue();

  SaveCfg();             //pro pripad, ze v Cfg neco chybelo, tak se to doplnilo defaultne a ted se to savne. Mozna zbytecne, ale aspon bude cfg uplny (vhodne pro prohlizeni cfg)
  //pokud v configu neco chybelo
  if ( run_configuration )
    OnOptionsConfiguration();        //vola save jen po kliknuti na <OK>
}


//save config file
void CMatView::SaveCfg()
{
  CString str;
  ParamFile f;

  f.Add("BuldozerPath", _buld_path);
  f.Add("MaterialPath", _mat_path);
  if ( _automatic_buldozer_start )
    f.Add("AutomaticStartWithBuldozer", "yes");
  else
    f.Add("AutomaticStartWithBuldozer", "no");
  if ( _automatic_buldozer_end )
    f.Add("CloseBuldozerAtEnd", "yes");
  else
    f.Add("CloseBuldozerAtEnd", "no");
  //shaders
  ParamEntryPtr cps= f.AddArray("ComboPixelShaderID");
  cps->ReserveArrayElements(_combo_PSI.GetCount());
  for ( int i= 0; i<_combo_PSI.GetCount(); i++ )
  {
    _combo_PSI.GetLBText(i, str);
    cps->AddValue((RString) str);
  }
  ParamEntryPtr cvs= f.AddArray("ComboVertexShaderID");
  cvs->ReserveArrayElements(_combo_VSI.GetCount());
  for ( int i= 0; i<_combo_VSI.GetCount(); i++ )
  {
    _combo_VSI.GetLBText(i, str);
    cvs->AddValue((RString) str);
  }
  ParamEntryPtr uvTex= f.AddArray("ComboUVTexture");
  uvTex->ReserveArrayElements(_combo_uvTex.GetCount());
  for ( int i= 0; i<_combo_uvTex.GetCount(); i++ )
  {
    _combo_uvTex.GetLBText(i, str);
    uvTex->AddValue((RString) str);
  }
  f.Add("ExternalEditor", _extern_editor);
  f.Add("TexViewPath", _texview_path);
  switch ( _dis_mats_col )
  {
    case DMC_GRAY : f.Add("DisabledMaterialColor", "gray");  
    break;
    case DMC_RED  : f.Add("DisabledMaterialColor", "red");  
    break;
    case DMC_GREEN: f.Add("DisabledMaterialColor", "green");  
    break;
    case DMC_BLUE : f.Add("DisabledMaterialColor", "blue");  
    break;
    default       : f.Add("DisabledMaterialColor", "gray");  
  }
  f.Add("CoefGray", g_coef_gray);
  if ( _disable_unselect_mats )
    f.Add("DisableUnselectedMaterials", "yes" );
  else
    f.Add("DisableUnselectedMaterials", "no" );
  //primitives
  f.Add("Sphere", _sphere_path);
  f.Add("Sphere2", _sphere2_path);
  f.Add("Box", _box_path);
  f.Add("Cone", _cone_path);
  f.Add("Cone2", _cone2_path);
  //path to default materials
  f.Add("PathToDefaultMaterials", _default_mat_path);
  //last mat  & tex path
  f.Add("LastP3dPath", _last_p3d_path);
  f.Add("LastMaterialPath", _last_mat_path);
  f.Add("LastTexturePath", _last_tex_path);
  f.Add("ConfigStagePath", _cfg_stage_path);
  if ( _shared_mem )
    f.Add("SharedMemory", "yes");
  else
    f.Add("SharedMemory", "no");
  //ss server name
  f.Add("SourceSafeServerName", _ss_server_name);

  f.Save(_cfg_file);
}


//---------------------------------------   ConfigStage   -----------------------------------


//read stage configs for pixel shaders
void CMatView::ReadCfgStage()
{
  WIN32_FIND_DATA file_data;
  HANDLE hFindFile;
  BOOL next= TRUE;

  RString files= _cfg_stage_path + "*.rvmat";
  hFindFile= FindFirstFile((const char*)files, &file_data);
  if ( hFindFile == INVALID_HANDLE_VALUE )
    return;
  while ( next )
  {
    char name[128];
    int len= strrchr(file_data.cFileName, '.')-file_data.cFileName;
    strncpy(name, file_data.cFileName, len);
    name[len]= '\0';
    RString nm(name);
    nm.Lower();
    _cfg_stage_name.Add(nm); 
    _cfg_stage_file.Add(_cfg_stage_path + file_data.cFileName);  

    next= FindNextFile(hFindFile, &file_data);
  }
  FindClose(hFindFile);
}


//return integet >-1 if PixelShaderID is in _cfg_stage
int CMatView::InCfgStage(RString PixelShaderID)
{
  for ( int i= 0; i < _cfg_stage_name.Size(); i++ )
  {
    PixelShaderID.Lower();
    if ( PixelShaderID == _cfg_stage_name.Get(i) )
      return i;
  }
  return -1;
}


