// OptionsDlg.cpp : implementation file
//
 
#include "stdafx.h"
#include "Mat.h"
#include "OptionsDlg.h"
#include ".\optionsdlg.h"


// COptionsDlg dialog

IMPLEMENT_DYNAMIC(COptionsDlg, CDialog)
COptionsDlg::COptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDlg::IDD, pParent)
  , _edit_buld_path(_T(""))
  , _edit_material_path(_T(""))
  , _check_buld_start(FALSE)
  , _check_buld_end(FALSE)
  , _edit_ps(_T(""))
  , _edit_vs(_T(""))
  , _edit_uvTex(_T(""))
  , _edit_editor(_T(""))
  , _edit_coef_gray(0)
  , _disable_unselect_mats(FALSE)
{
}

COptionsDlg::~COptionsDlg()
{
}

void COptionsDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_EDIT_OPT_BULD_PATH, _edit_buld_path);
  DDX_Text(pDX, IDC_EDIT_OPT_MATERIAL_PATH, _edit_material_path);
  DDX_Check(pDX, IDC_CHECK_BULD_START, _check_buld_start);
  DDX_Check(pDX, IDC_CHECK_BULD_END, _check_buld_end);
  DDX_Text(pDX, IDC_EDIT_OPT_CPS, _edit_ps);
  DDX_Text(pDX, IDC_EDIT_OPT_CVS, _edit_vs);
  DDX_Text(pDX, IDC_EDIT_OPT_UVTEX, _edit_uvTex);
  DDX_Text(pDX, IDC_EDIT_OPT_EDITOR, _edit_editor);
  DDX_Text(pDX, IDC_EDIT_OPT_COEFGRAY, _edit_coef_gray);
  DDX_Control(pDX, IDC_SPIN_OPT_CG, _spin_coef_gray);
  DDX_Check(pDX, IDC_CHECK_DIS_UNS_MAT, _disable_unselect_mats);
}


BEGIN_MESSAGE_MAP(COptionsDlg, CDialog)
  ON_EN_CHANGE(IDC_EDIT_OPT_BULD_PATH, OnEnChangeEditOptBuldpath)
  ON_EN_CHANGE(IDC_EDIT_OPT_MATERIAL_PATH, OnEnChangeEditOptMaterialPath)
  ON_BN_CLICKED(IDC_CHECK_BULD_START, OnBnClickedCheckBuldStart)
  ON_BN_CLICKED(IDC_CHECK_BULD_END, OnBnClickedCheckBuldEnd)
  ON_EN_CHANGE(IDC_EDIT_OPT_CPS, OnEnChangeEditOptCps)
  ON_EN_CHANGE(IDC_EDIT_OPT_CVS, OnEnChangeEditOptCvs)
  ON_EN_CHANGE(IDC_EDIT_OPT_UVTEX, OnEnChangeEditOptUvtex)
  ON_EN_CHANGE(IDC_EDIT_OPT_EDITOR, OnEnChangeEditOptEditor)
  ON_BN_CLICKED(IDC_B_OPT_EDITOR, OnBnClickedBOptEditor)
  ON_EN_CHANGE(IDC_EDIT_OPT_COEFGRAY, OnEnChangeEditOptCoefgray)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_OPT_CG, OnDeltaposSpinOptCg)
  ON_BN_CLICKED(IDC_CHECK_DIS_UNS_MAT, OnBnClickedCheckDisUnsMat)
END_MESSAGE_MAP()


BOOL COptionsDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  // TODO:  Add extra initialization here
	UDACCEL u[1];
	u[0].nInc= 5;
	u[0].nSec= 0;

	_spin_coef_gray.SetRange(0,100);
	_spin_coef_gray.SetAccel(2, u);
	_spin_coef_gray.SetPos(100*_edit_coef_gray);


  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}


// COptionsDlg message handlers

void COptionsDlg::OnEnChangeEditOptBuldpath()
{
  UpdateData(TRUE);
}


void COptionsDlg::OnEnChangeEditOptMaterialPath()
{
  UpdateData(TRUE);
}


void COptionsDlg::OnBnClickedCheckBuldStart()
{
  UpdateData(TRUE);
}


void COptionsDlg::OnBnClickedCheckBuldEnd()
{
  UpdateData(TRUE);
}


void COptionsDlg::OnEnChangeEditOptCps()
{
  UpdateData(TRUE);
}


void COptionsDlg::OnEnChangeEditOptCvs()
{
  UpdateData(TRUE);
}


void COptionsDlg::OnEnChangeEditOptUvtex()
{
  UpdateData(TRUE);
}


void COptionsDlg::OnEnChangeEditOptEditor()
{
  UpdateData(TRUE);
}


void COptionsDlg::OnBnClickedBOptEditor()
{
  char file_name[256]={0};
  OPENFILENAME ofd;

  ZeroMemory(&ofd, sizeof(OPENFILENAME));
  ofd.lStructSize= sizeof(OPENFILENAME);
  ofd.lpstrFile= file_name;
  ofd.hwndOwner= m_hWnd;
  ofd.nMaxFile= sizeof(file_name);
  ofd.lpstrTitle= "Open external editor";
  ofd.Flags= OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST;
  ofd.lpstrFilter= "Executable file (*.exe)\0*.exe\0All files (*.*)\0*.*\0";               //filtr
  if ( GetOpenFileName(&ofd) ) 
  {
    _edit_editor= file_name;
    UpdateData(FALSE);
  }
}


void COptionsDlg::OnEnChangeEditOptCoefgray()
{
  UpdateData(TRUE);
  if ( _edit_coef_gray>=0 && _edit_coef_gray<=1 )
    _spin_coef_gray.SetPos32((int)(100*_edit_coef_gray));
}


void COptionsDlg::OnDeltaposSpinOptCg(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  // TODO: Add your control notification handler code here
  float s= (float) 0.01 * (float) (pNMUpDown->iPos + pNMUpDown->iDelta);
  if ( s >= 0 && s <= 1 )
  {
    _edit_coef_gray= s;
    UpdateData(FALSE);    
  }
  *pResult = 0;
}


void COptionsDlg::OnBnClickedCheckDisUnsMat()
{
  UpdateData(TRUE);
}
