/*
  MATERIAL.HPP
  ------------

  Data structure for material.
*/


#ifndef _material_h_
#define _material_h_


#include "view/objlod.hpp"
#include "Es/Strings/rString.hpp"


#define MAX_STAGES           4                     //max. number of stages
#define UNKNOWN_MAT          -1
#define NO_MAT               -2
#define AMBIENT              1
#define DIFFUSE              1
#define FORCEDDIFFUSE        0
#define EMMISIVE             0
#define SPECULAR             0
#define SPECULARPOWER        0
#define H_LEN                1000                  //delka historie
//last history action
#define LHA_NO               0
#define LHA_ADD              1
#define LHA_UNDO             2
#define LHA_REDO             3
//disabled material color
#define DMC_GRAY             0
#define DMC_RED              1
#define DMC_GREEN            2
#define DMC_BLUE             3


//global data
extern int   g_unselected_mats;
extern float g_coef_gray;


//color
typedef struct Color 
{
  float _r, _g, _b, _a;
} Color;


//stage
class Stage
{
private:
  friend class Material;

  //flags & names
  BOOL _enable;
  RString _name;
  BOOL _protected;
  //parameters
  RString _texture;
  RString _uvSource;
  //uvTransform
  BOOL _uv_none;                          //if TRUE, uv-transform does not exist and _uvSource is "none"
  Vector3 _aside;
  Vector3 _up;
  Vector3 _dir;
  Vector3 _pos;

public:
  Stage();

  //Get & Set
  BOOL Enable() const;
  void Enable(BOOL enable);
  BOOL Protected() const;
  void Protected(BOOL prt);
  RString GetName() const;
  void SetName(const RString& name);
  RString GetTexture() const;
  void SetTexture(const RString& texture);
  RString GetUVSource() const;
  void SetUVSource(const RString& uvSource);
  BOOL UVNone();
  void UVNone(BOOL uv_none);
  Vector3& GetAside();
  Vector3& GetUp();
  Vector3& GetDir();
  Vector3& GetPos();
};


//material
class Material
{
private:
  friend class Material;
  friend class Materials;

  int _id;
  //parameters
  Color _ambient;
  Color _ambient_disable;                           
  Color _diffuse;
  Color _diffuse_disable;
  Color _forcedDiffuse;
  Color _emmisive;
  Color _specular;
  float _specularPower;
  RString _PixelShaderID;
  RString _VertexShaderID;
  //stages
  int _Nstage;
	int _cur_stage;
  Stage _stage[MAX_STAGES];              
  //file paths
  RString _file_name;                            //rvmat file of the material (the real path)
  RString _mat_name;                             //material name (the relativ path to the rvmat-file)
  RString _temp;                                 //temporary file for material
  //flags
  BOOL _disable;                                 //urcuje, zda se ma v Buldozeru zobrazovat jako 'disable'
  BOOL _changed;                                 //jen proto, abych pri kazdem update nesavoval, kdyz se nic nezmenilo
  BOOL _to_save;                                 //narozdil od _change se nastavuje, jen kdyz nastanou skutecne zmeny (ne treba disable stage), odpovida flagu '*'
  BOOL _selected;                                //urcuje, zda je v Matu vybrany pro editaci
  BOOL _ro;                                      //TRUE if material file (rvmat) is "read only"
  BOOL _ss_checked_out;                          //TRUE if material file is checked-out from SS
  BOOL _ss_controled;                            //TRUE if material is under SS control
  BOOL _ss_deleted;                              //TRUE if material is deleted in SS

  int Save( RString file_name, BOOL temp );

public:
  Material();
  ~Material();
  void Reset();                                  //reset all variables
  Material& operator = (const Material& mat);    //kopiroje do sebe

  //get & set atributes
  Color& GetAmbient();
  Color& GetDiffuse();
  Color& GetForcedDiffuse();
  Color& GetEmmisive();
  Color& GetSpecular();
  float GetSpecularPower() const;
  void SetSpecularPower(float specularPower);
  RString GetPixelShaderID() const;
  void SetPixelShaderID(const RString& PixelShaderID);
  RString GetVertexShaderID() const;
  void SetVertexShaderID(const RString& VertexShaderID);
   
	//stages
	int GetNstage();
  BOOL SetCurStage(int pos);
	int GetIndCurStage();
	Stage& GetCurStage();
  Stage& GetStage(int pos);
  BOOL AddStage(Stage stage, int pos);
  BOOL AddStage(Stage stage);
  BOOL RemoveStage(int pos);
	BOOL RemoveStage();
  void ProtectStages(BOOL prt);                  //set the protected flag for all stages

  //get & set flags & names
  BOOL Changed() const;
  void Changed(BOOL changed);
  BOOL ToSave() const;
  void ToSave(BOOL tosave);
  BOOL Selected() const;
  void Selected(BOOL selected);
  BOOL RO() const;
  void RO(BOOL ro);
  BOOL SSCheckedOut() const;
  void SSCheckedOut(BOOL co);
  BOOL SSControlled() const;
  void SSDeleted(BOOL deleted);
  BOOL SSDeleted() const;
  void SSControlled(BOOL ssc);

  RString GetMatName() const;
  RString GetFileName() const;
  void SetFileName(const RString& file_name);
  RString GetTempName() const;
  BOOL SetMat(const RString& file_name);

  //temp files
  void CreateTemp(int num, const RString& mat_path);    //create temp. file number 'num'
  BOOL SaveTemp();

  //Load & Save
  BOOL Load( RString file_name );                 //load material from file
  BOOL Load();
  BOOL Save(const RString& file_name);
  BOOL Save();

  //enable & disable
  void Enable();
  void Disable();
  BOOL Enabled() const;
};


//list of materials
class MaterialHistory
{
private:
  Material _mat[H_LEN];
  int _stat[H_LEN];
  int _first;
  int _last;
  int _cur;                                     //_cur ma ukazovat na material, ktery se ma vratit pri dalsim Undo
  BOOL _adding;                                 //TRUE if adding
  BOOL _undoing;
  BOOL _redoing;
  int _lha;                                     //last history action: , nothing, add, undo, redo

  void Inc(int& a);
  void Dec(int& a);
  void MaterialHistory::RemoveFirstChange();    //history is full, must remove first change

public:
  //init
  MaterialHistory();
  void Reset();
  //state of history
  BOOL Empty();
  BOOL Full();                                  //if list is full return TRUE
  BOOL FirstPos();                              //return TRUE if it can not do Undo
  BOOL LastPos();                               //return TRUE if it can not do Redo
  int GetLastAction();                          //return the last history action
  int GetPos();                                 //return the position in the history
  //add a change (more materials or parameters can be changed in one change)
  void StartAdd();                              //start adding materials
  void Add(Material& mat);                      //add one materia
  void EndAdd();                                //stop adding materials
  //Undo
  void StartUndo();
  void Undo(Material& mat);
  BOOL EndUndo();
  //Redo
  void StartRedo();
  void Redo(Material& mat);
  BOOL EndRedo();
};


//the array of pointers to the material list
typedef struct Lod {
  //faces - koresponduje s facy v _obj!
  int* _face;                   //array of pointers from face to material
  int _Nface;                   //number of faces in this lod
  //materials
  int* _mat;                    //the array of pointers to material, which belongs to this lod
  int _Nmat;                    //number of material in lod
} Lod;


//list of materials of p3d file
class Materials {
private:
  //materials
  Material* _hs_mat;
  Material* _mat;
  int _Nmat;
  int* _sel_mat;
  int _Nsel_mat;                                //number of selected materials, is set to 0 if the lod has been changed

  //paths
  RString _mat_path;                            //the path to materials
  RString _temp_path;

  //lods
  LODObject* _obj;
  Lod* _lods;
  int _Nlod; 
  int _cur_lod;

  //history
  MaterialHistory _history;

  //flags
  BOOL _changed;

  void CreateLodsMat();
  void SetNlod();

  BOOL LoadMaterialsFromFiles();
  BOOL LoadMaterialsFromObj();
  void SetMatID();
  void ResetSelMat();                              //unselect selected materials

  int CreateHsMat();
  void ConnectObjTemp();
  void DisconnectObjTemp();
  void DeleteTemps();
  void Destruct();

public:

  //constructor & destructor
  Materials();
  ~Materials();

  //obj
  void SetObj(LODObject* obj);
  LODObject* GetObj();

  //load & save ...
  BOOL Load(LODObject* obj, const RString& mat_path);   //load materials from 'obj'
  BOOL SaveSel();                                //save selected materials
  BOOL SaveAll();                                //save all materials
  BOOL SaveTemps();                              //save materials to temporary files, 'obj' can be update

  //LODs
  int GetNlod() const;                           //get number of lods
  BOOL SetCurLod(int lod);                       //change current lod to 'lod', select 1. material in 'lod', change lod in object
  int GetCurLod() const;                         //get current lod
  float GetNameLod(int lod) const;               //get name of 'lod'

  //material selection
  BOOL SetSelMatCurLod(int i);                   //select material, i - order in current lod
  BOOL UnsetSelMatCurLod(int i);                 //unselect material, i - order in current lod
  void UnsetAllSelMatCurLod();                   //unselect all materials
  const int* GetSelMatCurLod() const;            //get current mat in current lod

  //number of materials
  int GetNmatCurLod() const;                     //get number of materials in current lod 
  int GetNmat(int lod) const;                    //get number of materials in 'lod'
  int GetNmatAll() const;                        //get number of all materials
  int GetNselmat() const;                        //get number of selected materials in 'lod'

  //get material
  Material& GetFirstSelMatCurLod() const;        //get first selected material of currunt lod
  Material& GetMat(int i) const;                 //get material (i is absolut index)
  Material& GetMatCurLod(int i) const;           //get i-th material of current lod
  Material& GetMat(int lod, int i) const;        //get i-th material of 'lod'
  Material* GetAllMaterials();                   //return the array of all materials
  Material& GetHsMat();                          //return "harmless" material

  //get&set flags, paths
  BOOL ExistMat() const;                         //exist any material in object?    
  BOOL ExistMatCurLod() const;                   //exist any material in current lod?
  BOOL SetMatPath(const RString& mat_path);             //set path to materials, the name of materials is relativ path
  RString GetMatPath() const;                    //return the variable _mat_path
  BOOL Changed();                                //return TRUE if a material was changed
  void Changed(BOOL changed);                    //set the flag changed

  BOOL SetMat(int i, const RString& file_name);         //set the i-th material according to file_name

  //Enable&Disable materials
  void EnableSelMat();                           //enable selected materials, other material will be disabled (greyer)
  void EnableAllMat();                           //unselect all materials, all materials will be enable
  void ChangeGrayCoef();                         //change gray coeficient

  //history
  void HistoryReset();                           //history reset
  void HistoryAdd();                             //add all changed materials to history 
  void HistoryUndo();                            //make 'Undo'
  void HistoryRedo();                            //make 'Redo'
  int HistoryGetLastAction();                    //return the last history action
  int HistoryGetPos();                           //return the position in the history
  BOOL HistoryEmpty();                           //return TRUE if history is empty
  BOOL HistoryLastPos();                         //return TRUE if the current position in history is the last one, Redo is not avalible

  //info & report
  void ObjReport();                              //write information about p3d file
};


#endif
