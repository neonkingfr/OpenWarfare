#include <stdio.h>
#include <stdlib.h>

int batch = 1000000;

const int maxLine = 1024;

char *inputFile = "";

char *outputTemplate = "split%03d.txt";

char outputFile[256];

char buf[maxLine+1];

int main ( int argc, char **argv )
{
    if ( argc < 2 ) {
        printf("Usage: split <input> [ <output-template> [ <lines> ] ]\n");
        return 1;
        }
    inputFile = argv[1];
    if ( argc > 2 ) {
        outputTemplate = argv[2];
        if ( argc > 3 )
            batch = atoi(argv[3]);
        }
    FILE *in = fopen(inputFile,"rt");
    if ( !in ) {
        printf("Error opening '%s'\n",inputFile);
        return 1;
        }
    FILE *out = NULL;
    int fragment = 1;
    int lineNo;
    bool eof = false;
    do {                                    // one fragment
        lineNo = 0;
        do {                                // one line
            if ( !fgets(buf,maxLine,in) ) {
                eof = true;
                break;
                }
            if ( !out ) {
                sprintf(outputFile,outputTemplate,fragment);
                out = fopen(outputFile,"wt");
                if ( !out ) {
                    printf("Cannot write to '%s'\n",outputFile);
                    fclose(in);
                    return 1;
                    }
                }
            fputs(buf,out);
            } while ( ++lineNo < batch );
        if ( out ) {
            fclose(out);
            out = NULL;
            }
        fragment++;
        } while ( !eof );
    fclose(in);
    return 0;
}
