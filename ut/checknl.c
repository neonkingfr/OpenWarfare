#include <stdio.h>
#include <stdlib.h>

int main ( int argc, char *argv[] )
{
    FILE *input;
    int ch, ch2, lastchar, linenum;
    if ( !(input = fopen(argv[1],"rb")) )
    {
        printf("Cannot open input file %s\n",argv[1]);
        return -1;
    }

    lastchar = 0;
    linenum = 1;
    while ( 1 ) {       // one input character
        ch = getc(input);
        if ( ch == EOF || ch == '\x1A' ) 
        {
            break;
        }
        if ( ch == '\x0D' ) {
          if (lastchar == '\x0D') {
            printf("file %s obsahuje 0x0d 0x0d, line %d\n",argv[1], linenum);
          }
        }
        if ( ch == '\x0A' ) {
          if (lastchar == '\x0A') {
            printf("file %s obsahuje 0x0a 0x0a, line %d\n",argv[1], linenum);
            break;
          }
          linenum++;
        }
        lastchar = ch;
    }
    fclose(input);

    return 1;
}
