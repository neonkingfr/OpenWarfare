// River Raid - wave.cpp
// Loading of wav files
// (C) 1996, SUMA

#include <El/elementpch.hpp>
#include <Es/Common/fltopts.hpp>
#include <Es/Framework/consoleBase.h>
#include <Es/Types/pointers.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Strings/bString.hpp>
#include <Es/Common/win.h>
#include <El/SoundFile/soundFile.hpp>
#include <stdio.h>
#include <direct.h>
#include <math.h>
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>

const int WSSMagic='0SSW';
static int resample = 0; //no resample by default
static int resampleQuality = 10; //default quality for resample purposes
//driven by commandline param -convert, which forces wss file to be converted instead of played. (We need automatic play of wss files as the waveToSSS.exe should work as a media player for these files)
static bool forceConvert = false; 
static bool enableStereoEncoding = false;

struct WSSHeader
{
	int magic;
	char deltaPack;
	char resvd[3];
};

const int MaxDelta4Inv=7;
const int MaxDelta4=8192;
const static int Delta4[MaxDelta4Inv*2+1]=
{
	-8192,-4096,-2048,-1024,-512,-256,-64,
	0, // 7
	64,256,512,1024,2048,4096,8192
};
static char Delta4Inv[MaxDelta4*2+1];

const int MaxDelta8=32768;
const int MaxDelta8Inv=127;
static int Delta8[MaxDelta8Inv*2+1];
static char Delta8Inv[MaxDelta8*2+1];

static void InitDelta8()
{
	double base=pow(MaxDelta8,1.0f/MaxDelta8Inv);
	Delta8[MaxDelta8Inv]=0;
	for( int i=1; i<MaxDelta8Inv+1; i++ )
	{
		//        / -1.084618362^-x  for x<0  (-128 to -1)
		// f(x)= {   0               for x=0  (0)
		//        \  1.084618362^x   for x>0  (1 to 127)
		int v=toInt(0.5f+pow(base,i));
		Delta8[MaxDelta8Inv+i]=+v;
		Delta8[MaxDelta8Inv-i]=-v;
	}
	double invLogBase=1/log(base);
	Delta8Inv[MaxDelta8]=0;
	for( int i=1; i<=MaxDelta8; i++ )
	{
		int v=toInt(0.5f+log(float(i))*invLogBase);
		if( v>127 ) v=MaxDelta8Inv;
		Delta8Inv[MaxDelta8+i]=+v;
		Delta8Inv[MaxDelta8-i]=-v;
	}
}

static void InitDelta4()
{
	int o;
	for( o=1; o<=MaxDelta4; o++ )
	{
		// calculate inverse of Delta4
		int bigger;
		for( bigger=0; bigger<MaxDelta4Inv; bigger++ ) if( Delta4[MaxDelta4Inv+bigger]>=o ) break;
		if( bigger>0 )
		{
			// check if we are closer to lower or bigger
			int lower=bigger-1;
			// interpolate betweeen lower and bigger accordingly
			int lVal=Delta4[lower+MaxDelta4Inv];
			int bVal=Delta4[bigger+MaxDelta4Inv];
			if( o-lVal<bVal-o ) bigger=lower;
		}
		Delta4Inv[MaxDelta4+o]=bigger;
		Delta4Inv[MaxDelta4-o]=-bigger;
	}
}

static void InitTables()
{
	InitDelta4();
	InitDelta8();
}

inline int SatDelta4( int val )
{
	if( val>+8192 ) val=+8192;
	else if( val<-8192 ) val=-8192;
	return val;
}

inline int SatDelta8( int val )
{
	if( val>+32767 ) return val=+32767;
	else if( val<-32767 ) return val=-32767;
	return val;
}

template<unsigned N>
struct DeltaPack4 
{
  SRef< Buffer<char> > operator()( SRef< Buffer<char> > &source )
  {
    // assume source is 16-b
    Buffer<char> *ret=new Buffer<char>;
    short *data=(short *)source->Data();
    int len=source->Size()/2;
    int offset=0,dOffset=0;
    int actVal[N]= {0};
    int end=len&~1;
    ret->Init(end/2); // alloc return storage
    memset(ret->Data(),0,ret->Size());
    while( offset<end )
    {
      for (unsigned i = 0; i < N; i++)
      {
        int pd0;
        int a0=data[offset++];
        // take sample
        int ss0=SatDelta4(a0-actVal[i]);
        pd0=Delta4Inv[MaxDelta4+ss0];
        pd0+=MaxDelta4Inv;
        actVal[i]+=Delta4[pd0];
        
        if (i % 2 == 0)
        {
          (*ret)[dOffset]=pd0<<4;
        }
        else
        {
          (*ret)[dOffset++]|=(pd0&0xf);
        }
      }
    }

    return ret;
  }

};

template<>
struct DeltaPack4<1>
{
  SRef< Buffer<char> > operator()( SRef< Buffer<char> > &source )
  {
    // assume source is 16-b
    Buffer<char> *ret=new Buffer<char>;
    short *data=(short *)source->Data();
    int len=source->Size()/2;
    int offset=0,dOffset=0;
    int actVal=0;
    int end=len&~1;
    ret->Init(end/2); // alloc return storage
    memset(ret->Data(),0,ret->Size());
    while( offset<end )
    {
      // two samples a time
      int pd0,pd1;
      int a0=data[offset++];
      int a1=data[offset++];
      // first sample
      int ss0=SatDelta4(a0-actVal);
      pd0=Delta4Inv[MaxDelta4+ss0];
      pd0+=MaxDelta4Inv;
      actVal+=Delta4[pd0];
      // second sample
      int ss1=SatDelta4(a1-actVal);
      pd1=Delta4Inv[MaxDelta4+ss1];
      pd1+=MaxDelta4Inv;
      actVal+=Delta4[pd1];
      (*ret)[dOffset++]=(pd0<<4)|(pd1&0xf);
    }

    return ret;
  }

};

template<unsigned N>
struct DeltaPack8 
{
  SRef< Buffer<char> > operator()( SRef< Buffer<char> > &source ) const
  {
    // assume source is 16-b
    Buffer<char> *ret=new Buffer<char>;
    int len=source->Size()/2;
    int offset=0,dOffset=0;
    int actVal[N] = {0};
    int end=len;
    ret->Init(end); // alloc return storage
    const short *data=(const short *)source->Data();
    memset(ret->Data(),0,ret->Size());
    while( offset<end )
    {
      // forach channel
      for (unsigned i = 0; i < N; i++) 
      {
        // one samples a time
        int pd0;
        int a0=data[offset++];
        // first sample
        int ss0=SatDelta8(a0-actVal[i]);
        pd0=Delta8Inv[ss0+MaxDelta8];
        actVal[i]+=Delta8[pd0+MaxDelta8Inv];
        (*ret)[dOffset++]=pd0;
      }
    }

    return ret;
  }
};

template<>
struct DeltaPack8<1>
{
  SRef< Buffer<char> > operator()( SRef< Buffer<char> > &source ) const
  {
    // assume source is 16-b
    Buffer<char> *ret=new Buffer<char>;
    int len=source->Size()/2;
    int offset=0,dOffset=0;
    int actVal=0;
    int end=len;
    ret->Init(end); // alloc return storage
    const short *data=(const short *)source->Data();
    memset(ret->Data(),0,ret->Size());
    while( offset<end )
    {
      // one samples a time
      int pd0;
      int a0=data[offset++];
      // first sample
      int ss0=SatDelta8(a0-actVal);
      pd0=Delta8Inv[ss0+MaxDelta8];
      actVal+=Delta8[pd0+MaxDelta8Inv];
      (*ret)[dOffset++]=pd0;
    }
    return ret;
  }
};

/**
Implement PlaySound on our own, using low-level API
Adapted from WINE playsound.c
*/

struct playsound_data
{
  HANDLE	hEvent;
};

static void CALLBACK PlaySound_Callback(
  HWAVEOUT hwo, UINT uMsg, DWORD dwInstance, DWORD dwParam1, DWORD dwParam2
)
{
  struct playsound_data*	s = (struct playsound_data*)dwInstance;
  switch (uMsg)
  {
  case WOM_OPEN:
  case WOM_CLOSE:
    break;
  case WOM_DONE:
    SetEvent(s->hEvent);
    break;
  default:
    LogF("Unknown uMsg=%d\n", uMsg);
  }
}

static int proc_PlaySound(LPWAVEFORMATEX lpWaveFormat, void *data, int dataSize)
{
  HWAVEOUT		hWave = 0;
  struct playsound_data	s;

  s.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

  if (
    waveOutOpen(&hWave, WAVE_MAPPER, lpWaveFormat, (DWORD)PlaySound_Callback, (DWORD)&s, CALLBACK_FUNCTION) == MMSYSERR_NOERROR
  )
  {
    LPWAVEHDR waveHdr = new WAVEHDR;
    waveHdr[0].lpData = (LPSTR)data;
    waveHdr[0].dwUser = 0;
    waveHdr[0].dwLoops = 0;
    waveHdr[0].dwFlags = 0;
    waveHdr[0].dwBufferLength = dataSize;
    if (waveOutPrepareHeader(hWave, &waveHdr[0], sizeof(WAVEHDR))==MMSYSERR_NOERROR)
    {
      waveHdr[0].dwFlags &= ~WHDR_DONE;
      if (waveOutWrite(hWave, &waveHdr[0], sizeof(WAVEHDR)) == MMSYSERR_NOERROR)
      {
      }

      WaitForSingleObject(s.hEvent, INFINITE);
      waveOutReset(hWave);

      waveOutUnprepareHeader(hWave, &waveHdr[0], sizeof(WAVEHDR));
    }
    waveOutClose(hWave);
  }
  CloseHandle(s.hEvent);

  return 0;
}

const float PI = 3.1415926535f;
void Resample(WAVEFORMATEX &header, SRef< Buffer<char> > &buf, int resample, int quality=10)
{
  WAVEFORMATEX headerOut = header; //but we change something to fit new resample rate
  const short *samplesIn = (const short *)buf->Data();
  const int lenIn = buf->Size()/2;

  headerOut.nSamplesPerSec = resample;
  headerOut.nAvgBytesPerSec = resample * header.nBlockAlign;

  unsigned int lenOut = lenIn * resample / header.nSamplesPerSec;
  float dx = double(lenIn)/lenOut;
  SRef< Buffer<char> > bufOut = new Buffer<char>;
  bufOut->Init(lenOut*2);
  short *samplesOut = (short *)bufOut->Data();

    // fmax : nyqist half of destination sampleRate
  // fmax / fsr = 0.5;
  float fmaxDivSR = 0.5;
  float r_g = 2 * fmaxDivSR; //1

  // Quality is half the window width
  int wndWidth2 = quality;
  int wndWidth = quality*2;

  float x = 0;
  for (unsigned i=0;i<lenOut;++i)
  {
    float r_y = 0.0;
    for (int tau=-wndWidth2;tau < wndWidth2;++tau)
    {
        // input sample index
        int j = (int)(x+tau);

        // Hann Window. Scale and calculate sinc
        float r_w = 0.5 - 0.5 * cos(2*PI*(0.5 + (j-x)/wndWidth));
        float r_a = 2*PI*(j-x)*fmaxDivSR;
        float r_snc = 1.0;
        if (r_a != 0)
            r_snc = sin(r_a)/r_a;

        if ((j >= 0) && (j < lenIn))
        {
            r_y += r_g * r_w * r_snc * samplesIn[j];
        }
    }
    samplesOut[i] = r_y;
    x += dx;
  }
  // rewrite the input values to new output
  header = headerOut;
  buf = bufOut;
}

/**
@param play instead of storing the file play it
*/
static int ConvertFile( const char *dest, const char *source, int deltaPack, bool play )
{
	// TODO: check time stamps

  Ref<WaveStream> input;
  SoundRequestFile(source,input,true);
  if (input.IsNull())
  {
    printf("Cannot read file %s\n", cc_cast(source));
    return 1;
  }

  WAVEFORMATEX header;
  input->GetFormat(header);
  if (enableStereoEncoding) 
  {
    if (header.wBitsPerSample != 16 || (header.nChannels != 1 && header.nChannels != 2))
    {
      printf("Warning: %s is not 16b mono or stereo.\n",source);
      deltaPack=0; // cannot deltaPack
    }
  }
  else
  {
    if (header.wBitsPerSample != 16 || header.nChannels != 1) 
    {
      printf("Warning: %s is not 16b mono, for 16b stereo samples use param: stereo.\n",source);
      deltaPack=0; // cannot deltaPack
    }
  }
  
  // TODO: use streaming instead
  int totalSize = input->GetUncompressedSize();
  SRef< Buffer<char> > buf = new Buffer<char>;
  buf->Realloc(totalSize);
  buf->Resize(totalSize);
  int offset = 0;
  int actualRead = input->GetData(buf->Data(),offset,buf->Size());
  if (actualRead<totalSize)
  {
    printf("Error while reading %s at %d",cc_cast(source),offset);
    return 1;
  }
  offset += actualRead;
  if (play)
  {
    // only play the file, no conversion
    printf("Playing %s\n",cc_cast(source));
    return proc_PlaySound(&header,buf->Data(),buf->Size());
  }

  if (resample)
  {
    Resample(header, buf, resample, resampleQuality);
  }

  WSSHeader wHeader;
	memset(&wHeader,0,sizeof(wHeader));
  printf("%s->%s\n", source, dest);
	int handle=_open(dest,_O_CREAT|_O_TRUNC|_O_BINARY|_O_WRONLY,_S_IREAD|_S_IWRITE);
	if( handle>=0 )
	{
		switch( deltaPack )
		{
			case 4:
        if (header.nChannels == 1) {
          buf=DeltaPack4<1>()(buf);
        } else if (header.nChannels == 2) {
          buf=DeltaPack4<2>()(buf);
        }
				header.cbSize=buf->Size()*4;
			break;
			case 8:
        if (header.nChannels == 1) {
          buf=DeltaPack8<1>()(buf);
        } else if (header.nChannels == 2) {
          buf=DeltaPack8<2>()(buf);
        }
			break;
			default: // no deltaPack
				deltaPack=0;
			break;
		}
		wHeader.magic=WSSMagic;
		wHeader.deltaPack=deltaPack;
		write(handle,(char *)&wHeader,sizeof(wHeader));
		write(handle,(char *)&header,sizeof(header));
		write(handle,buf->Data(),buf->Size());
		close(handle);
	}
	return 0;
}


/**
@param play instead of storing the file play it
*/
static int ConvertDir( const char *src, int deltaPack, bool play, const char *dst = NULL )
{
	int test;
	
	test=open(src,O_RDONLY);
	if( test>=0 )
	{
		close(test);
		// single file - decide automatically what to do
    const char *sExt=GetFileExt(GetFilenameExt(src));
    if (!strcmpi(sExt,".ogg") || !strcmpi(sExt,".wss"))
    {
      // converting from ogg or wss has little sense - assume user wanted to play back the sound
      // New: it has sense now to resample wss files when creating LITE addons, mainly for server purpose
      play = !forceConvert;
    }
    const char *ext = strrchr(src, '.');
    if (!ext) ext = src+strlen(src);
		RString dest;
    if (dst) 
    {
      dest = RString(dst);
	    const char *destExt = strrchr(dest, '.');
	    if (!destExt)
      {
        if (dest[dest.GetLength()-1]!='\\') dest = dest + "\\";
        const char *fullname = strrchr(src, '\\');
        if (!fullname) dest = dest +RString(src,ext-src) + RString(".wss");
        else dest = dest + RString(fullname + 1,ext - fullname -1) + RString(".wss");
      }
    }
    else
    {
      dest = RString(src, ext - src) + RString(".wss");
    }

    int ret=ConvertFile(dest,src,deltaPack,play);
		//remove(src);
		return ret;
	}
	
	printf("Scanning folder %s\n",src);

	BString<512> wPath;
	strcpy(wPath,src);
	const char *N=GetFilenameExt(wPath);
	if( !strchr(N,'?') && !strchr(N,'*') )
	{
	  TerminateBy(wPath,'\\');
		strcat(wPath,"*.*");
	}

	_finddata_t c_file;
	long hFile;
	if( (hFile=_findfirst(wPath,&c_file)) != -1L )
	{
		for(;;)
		{
			BString<512> DPath;
			BString<512> SPath;
			if( strcmp(c_file.name,".") && strcmp(c_file.name,"..") )
			{

				strcpy(SPath,wPath);

				LString N=GetFilenameExt(SPath);
				strcpy(N,c_file.name);
				if( c_file.attrib&_A_SUBDIR )
				{
					ConvertDir(SPath,deltaPack,play);
				}

				if (!strcmpi(GetFileExt(c_file.name),".wav"))
				{
					if (dst)
          {
            strcpy(DPath,dst);
            if (dst[strlen(dst)-1]!='\\') strcat(DPath,"\\"); 
            strcat(DPath,N);
          }
          else
          {
            strcpy(DPath,SPath);
          };
					LString dExt=GetFileExt(GetFilenameExt(DPath));
					strcpy(dExt,".wss");
					if( ConvertFile(DPath,SPath,deltaPack,play)<0 ) wasError=TRUE;
				}
			}
			if( _findnext(hFile,&c_file)!=0 ) break;
		}
		_findclose(hFile);
	}
	return 0;
}

int consoleMain( int argc, const char *argv[] )
{
	const char *src=NULL;
  const char *dst=NULL;
	
	const char **argv0=argv;
	int deltaPack=0;
	bool play = false;
	while( argc>1 )
	{
		const char *arg=argv[1];
		if( *arg=='-' || *arg=='/' )
		{
			arg++;
			if (!strcmpi(arg,"play"))
			{
			  play = true;
			}
      else if (!strcmpi(arg,"convert"))
			{
			  forceConvert = true;
			}
      else if (!strcmpi(arg,"stereo"))
      {
        enableStereoEncoding = true;
      }
      else if( *arg=='d' )
			{
				arg++;
				deltaPack=atoi(arg);
				if( deltaPack!=4 && deltaPack!=8 ) goto Usage;
			}
			else if( *arg=='r' ) //resample
			{
				arg++;
				resample=atoi(arg);
			}
			else if( *arg=='q' ) //resampleQuality
			{
				arg++;
				resampleQuality=atoi(arg);
			}
			else goto Usage;
		}
		else
		{
			if( !src ) src=arg;
      else if (!dst) dst=arg;
			else goto Usage;
		}
		argc--,argv++;
	}
	if( !src )
	{
		Usage:
		printf("Arguments used:\n");
		for( int i=1; argv0[i]; i++ )
		{
			printf("'%s'\n",argv0[i]);
		}
		
		printf("Usage:\tWaveToSSS <options> <source> [<destination>]\n");
		printf("Options:-d8 8b deltapack\n");
		printf("\t-d4 4b deltapack (voice)\n");
		printf("\t-r8000 resample to 8kHz (or other value specified)\n");
		printf("\t-q10 resample quality (the width of Hann window applied)\n");
    printf("\t-play play the sound instead converting it\n");
    printf("\t-convert convert the wss file instead of playing it\n");
    printf("\t-stereo enable stereo samples encoding\n");
		return 1;
	}

	int ret=-1;
	InitTables();
	if( src )
	{
	  ret=ConvertDir(src,deltaPack,play,dst);
	}
	if( ret==0 ) wasError=FALSE;
	if( !wasError ) return 0;
	return 1;
}

