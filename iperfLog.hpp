class IPerfCounters
{
public:
  virtual int N() const = NULL;
  virtual const char *Name( int i ) const = NULL;
  virtual int CurrentValue( int i ) const = NULL;
  virtual int LastValue( int i ) const = NULL;
  virtual int LastCount(int i) const = NULL;
  
  virtual int SmoothValue(int i) const = NULL;
  virtual int SlowValue(int i) const = NULL;
  virtual int SlowCount(int i) const = NULL;
  virtual int SumValue(int i) const = NULL;
  virtual int LastValue(int i, int frame) const = NULL;
  virtual float Correlation(int i) const = NULL;
  virtual bool Show( int i ) const = NULL;
  virtual bool WasNonZero( int i, int frames=30 ) const = NULL;
};