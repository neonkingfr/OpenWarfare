
#include <El/ParamArchive/paramArchive.hpp>

/// single pass info (material and alpha blending)
struct SinglePassInfo
{
  RString material;
  RString texture;
  /// alpha on u=0, v=0
  float tlAlpha;
  /// alpha on u=1, v=0
  float trAlpha;
  /// alpha on u=0, v=1
  float blAlpha;
  /// alpha on u=1, v=1
  float brAlpha;
  /// save content
  LSError Serialize(ParamArchive &ar);
};

TypeIsMovable(SinglePassInfo)

/// extension of TexMaterial - multipass rendering
class MultipassMaterial: public RefCount
{
public:
  AutoArray<SinglePassInfo> _passes;

public:
  MultipassMaterial();

  LSError Serialize(ParamArchive &ar);

  int PassCount() const {return _passes.Size();}
  const SinglePassInfo &Pass(int i) const {return _passes[i];}
  void AddPass(const SinglePassInfo &pass);
  /// call when all passes are ready
  void Close();
};

