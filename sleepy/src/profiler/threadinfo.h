/*=====================================================================
threadinfo.h
------------
File created by ClassTemplate on Sun Mar 20 03:22:37 2005

Copyright (C) Nicholas Chapman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html..
=====================================================================*/
#ifndef __THREADINFO_H_666_
#define __THREADINFO_H_666_


#include <windows.h>
#include <string>


/*=====================================================================
ThreadInfo
----------
Info about a thread running on the system
=====================================================================*/
class ThreadInfo
{
public:
	/*=====================================================================
	ThreadInfo
	----------
	
	=====================================================================*/
	// DE: 20090325 Threads now have a name
	ThreadInfo(DWORD id, const std::string& name);

	~ThreadInfo();

	// DE: 20090325 Threads now have a name
	const std::string& getName() const { return name; }
	const DWORD getID() const { return id; }

  FILETIME prevKernelTime, prevUserTime;
	// DE: 20090325 Threads now have CPU usage
  int cpuUsage;

private:
	// DE: 20090325 Threads now have a name
	std::string name;
	DWORD id;
};



#endif //__THREADINFO_H_666_




