/*=====================================================================
profiler.cpp
------------
File created by ClassTemplate on Thu Feb 24 19:00:30 2005

Copyright (C) Nicholas Chapman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html..
=====================================================================*/
#include "profiler.h"


#include "../utils/stringutils.h"
#include <process.h>
#include <iostream>
#include <assert.h>
#include <winnt.h>
#include <dbghelp.h>

// DE: 20090325: Profiler no longer owns callstack and flatcounts since it is shared between multipler profilers

Profiler::Profiler(HANDLE target_process_, HANDLE target_thread_, std::map<CallStack, int>& callstacks_, std::map<PROFILER_ADDR, int>& flatcounts_)
:	target_process(target_process_),
	target_thread(target_thread_),
	callstackPipe(NULL),
	callstacks(callstacks_),
	flatcounts(flatcounts_),
	slowFrameLimit(0),
	skipSlowFrames(4)
{
	
}

Profiler::Profiler(HANDLE pipe, float slowFrameLimit, std::map<CallStack, int>& callstacks, std::map<PROFILER_ADDR, int>& flatcounts)
:	target_process(NULL),
	target_thread(NULL),
	callstackPipe(pipe),
	callstacks(callstacks),
	flatcounts(flatcounts),
	slowFrameLimit(slowFrameLimit),
	skipSlowFrames(4)
{
	
}

// DE: 20090325: Need copy constructor since it is put in a std::vector

Profiler::Profiler(const Profiler& iOther)
:	target_process(iOther.target_process),
	target_thread(iOther.target_thread),
	callstackPipe(iOther.callstackPipe),
	callstacks(iOther.callstacks),
	flatcounts(iOther.flatcounts),
	toProcess(iOther.toProcess),
	slowFrameLimit(iOther.slowFrameLimit),
	skipSlowFrames(iOther.skipSlowFrames)
{
}

// DE: 20090325: Need copy assignement since it is put in a std::vector

Profiler& Profiler::operator=(const Profiler& iOther)
{
	target_process = iOther.target_process;
	target_thread = iOther.target_thread;
	callstacks = iOther.callstacks;
	flatcounts = iOther.flatcounts;
	toProcess = iOther.toProcess;
	slowFrameLimit = iOther.slowFrameLimit;
	skipSlowFrames = iOther.skipSlowFrames;

	return *this;
}

Profiler::~Profiler()
{
	
}


void Profiler::reset()
{
  
  flatcounts.swap(std::map<PROFILER_ADDR, int>());
  callstacks.swap(std::map<CallStack, int>());
  
}

void Profiler::sampleTarget()
{
  if (callstackPipe)
  {
    samplePipe();
    return;
  }
	// DE: 20090325: Moved declaration of stack variables to reduce size of code inside Suspend/Resume thread
	int stacklevels = 0;
	PROFILER_ADDR walk[256];

	STACKFRAME64 frame;
	memset(&frame, 0, sizeof(frame));

	CONTEXT threadcontext;
	threadcontext.ContextFlags = CONTEXT_i386 | CONTEXT_CONTROL;

	// An open question is whther or not this routine can be called
	// reentrantly by the multi-media timer support.

	HRESULT result = SuspendThread(target_thread);

	if(result == 0xffffffff)
		throw ProfilerExcep("SuspendThread failed.");

  // instead of using StackWalk64, we may read the data provided by the profiled application via pipe
  
	result = GetThreadContext(target_thread, &threadcontext);
	if(!result){
		// DE: 20090325: If GetThreadContext fails we must be sure to resume thread again
		ResumeThread(target_thread);
		throw ProfilerExcep("GetThreadContext failed.");
	}

	walk[stacklevels++] = threadcontext.Eip;
	frame.AddrStack.Offset = threadcontext.Esp;
	frame.AddrPC.Offset = threadcontext.Eip;
	frame.AddrFrame.Offset = threadcontext.Ebp;
	frame.AddrStack.Mode = frame.AddrPC.Mode = frame.AddrFrame.Mode = AddrModeFlat;

	while(true)
	{
		BOOL result = StackWalk64(
			IMAGE_FILE_MACHINE_I386,
			target_process,
			target_thread,
			&frame,
			&threadcontext,
			NULL,
			&SymFunctionTableAccess64,
			&SymGetModuleBase64,
			NULL
		);

		if (!result || stacklevels >= 256)
			break;

		walk[stacklevels++] = (PROFILER_ADDR)frame.AddrReturn.Offset;
	}

	//std::cout << "addr: " << addr << std::endl;

	result = ResumeThread(target_thread);
	if(!result)
		throw ProfilerExcep("ResumeThread failed.");

	//NOTE: this has to go after ResumeThread.  Otherwise mem allocation needed by std::map
	//may hit a lock held by the suspended thread.

	flatcounts[walk[0]]++;

	CallStack stack;
	for (int n=0;n<stacklevels;n++)
		stack.addr.push_back(walk[n]);
	callstacks[stack]++;
}

static bool ReadFromPipe(HANDLE pipe, void *buffer, size_t toRead)
{
  DWORD read = 0;
  while (toRead>0)
  {
    BOOL ok = ReadFile(pipe,buffer,toRead,&read,NULL);
    if (!ok)
    {
      HRESULT hr = GetLastError();
      switch (hr)
      {
        case ERROR_ACCESS_DENIED:
        case ERROR_BAD_PIPE:
        case ERROR_NO_DATA:
        case ERROR_PIPE_NOT_CONNECTED:
          return false;
      }
    }
    toRead -= read;
  }
  return true;

}

enum SleepyMsg
{
  SleepyFrame, // 0
};

struct SleepyFrameMsg
{
  DWORD id;
  DWORD duration;
  SleepyFrameMsg():id(SleepyFrame){}
};

void Profiler::samplePipe()
{
	int stacklevels = 0;
	PROFILER_ADDR walk[256];
	
  // TODO: read the callstack from the pipe
  ReadFromPipe(callstackPipe,&stacklevels,sizeof(stacklevels));
  
  if (stacklevels>0)
  {
    assert(stacklevels<=256);
    DWORD toRead = sizeof(*walk)*stacklevels;
    
    ReadFromPipe(callstackPipe,walk,sizeof(*walk)*stacklevels);
    
	  flatcounts[walk[0]]++;

	  CallStack stack;
	  for (int n=0;n<stacklevels;n++)
		  stack.addr.push_back(walk[n]);
		  
	  // when frame detection is disabled, process each call-stack
	  if (slowFrameLimit<=0)
	  {
    	callstacks[stack]++;
	  }
	  else
	  {
	    // otherwise record callstacks for the frame for later processing
  		toProcess.push_back(stack);
	  }
  }
  else
  {
    // zero or negative - a special message
    switch (-stacklevels)
    {
      case SleepyFrame:
        {
          // frame edge - process or discard the frame
          DWORD duration;
          ReadFromPipe(callstackPipe,&duration,sizeof(duration));
          static DWORD minFrameTime = slowFrameLimit;
          if (slowFrameLimit>0 && duration>minFrameTime && --skipSlowFrames<0)
          {
            // slow frame detected - process all accumulated data
            for (std::vector<CallStack>::iterator it=toProcess.begin();  it!=toProcess.end(); ++it)
            {
              const CallStack &stack = *it;
          	  callstacks[stack]++;
            }
          }
          else
          {
            // frame not slow enough to be interesting - skip it
            toProcess.swap(std::vector<CallStack>());
          }
	      }
        
        break;
    }
    
  }
  
}


// returns true if the target thread has finished
bool Profiler::targetExited() const
{
	DWORD code = WaitForSingleObject(target_thread, 100);
	return (code == WAIT_OBJECT_0);
}


//void Profiler::saveIPs(std::ostream& stream)
//{
//	for(std::map<Sample, int>::const_iterator i = counts.begin(); 
//		i != counts.end(); ++i)
//	{
//		const Sample &sample = i->first;
//		int count = i->second;
//		stream << ::toHexString(sample.addr) << " " << count << "\n";
//	}
//
//	stream.flush();
//}
//
