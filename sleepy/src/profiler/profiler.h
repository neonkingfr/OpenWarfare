/*=====================================================================
profiler.h
----------
File created by ClassTemplate on Thu Feb 24 19:00:30 2005

Copyright (C) Nicholas Chapman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html..
=====================================================================*/
#ifndef __PROFILER_H_666_
#define __PROFILER_H_666_


#pragma warning(disable : 4786)//disable long debug name warning


#include <windows.h>
#include <map>
#include <iostream>
#include <string>
#include <vector>

//64 bit mode:
//typedef unsigned long long PROFILER_ADDR;
//32 bit mode:
typedef unsigned int PROFILER_ADDR;

//use 32 bit mode for now 'cause CONTEXT::eip is only 32 bits.

class CallStack
{
public:
  std::vector<PROFILER_ADDR> addr;

	bool operator < (const CallStack &other) const {
    if (addr.size() != other.addr.size())
      return (addr.size() < other.addr.size());

    for (size_t n=0;n<addr.size();n++)
    {
      if (addr[n] < other.addr[n])
        return true;
      if (addr[n] > other.addr[n])
        return false;
    }

    return false;
	}
};

class ProfilerExcep
{
public:
	ProfilerExcep(const std::string& s_) : s(s_) {}
	~ProfilerExcep(){}	

	const std::string& what() const { return s; }
private:
	std::string s;
};

/*=====================================================================
Profiler
--------
does the EIP sampling
=====================================================================*/
class Profiler
{
public:
	/*=====================================================================
	Profiler
	--------
	
	=====================================================================*/
	// DE: 20090325: Profiler no longer owns callstack and flatcounts since it is shared between multipler profilers
	Profiler(HANDLE target_process, HANDLE target_thread, std::map<CallStack, int>& callstacks, std::map<PROFILER_ADDR, int>& flatcounts);

	Profiler(HANDLE pipe, float slowFrameLimit, std::map<CallStack, int>& callstacks, std::map<PROFILER_ADDR, int>& flatcounts);
	
	// DE: 20090325: Need copy constructor since it is put in a std::vector
	Profiler(const Profiler& iOther);
	// DE: 20090325: Need copy assignement since it is put in a std::vector
	Profiler& operator=(const Profiler& iOther);

	~Profiler();
	
	// DE: 20090325: Profiler no longer owns callstack and flatcounts since it is shared between multipler profilers
	std::map<CallStack, int>& callstacks;
	std::map<PROFILER_ADDR, int>& flatcounts;

	void sampleTarget();//throws ProfilerExcep
	
	/// discard all data collected so far
	void reset();
	
	bool targetExited() const;

	//void saveIPs(std::ostream& stream);//write IP values to a stream

	HANDLE getTarget(){ return target_thread; }
	
protected:
  void samplePipe();
  
private:
	HANDLE target_process, target_thread;
	HANDLE callstackPipe;

  float slowFrameLimit;
  
	int skipSlowFrames;
	
	std::vector<CallStack> toProcess;
};



#endif //__PROFILER_H_666_




