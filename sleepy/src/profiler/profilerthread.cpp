/*=====================================================================
profilerthread.cpp
------------------
File created by ClassTemplate on Thu Feb 24 19:29:41 2005

Copyright (C) Nicholas Chapman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html..
=====================================================================*/
#include "profilerthread.h"
#include <wx/wx.h>
#include <wx/wfstream.h>
#include <wx/zipstrm.h>
#include <wx/txtstrm.h>

#include "../utils/stringutils.h"
#include <fstream>
#include <assert.h>
#include <algorithm>
#include "symbolinfo.h"

#pragma comment(lib, "winmm.lib")


void startProfiling(int num_samples)
{
	//get handle of the current thread
	/*const HANDLE thisthread = getProcessWideHandle();//OpenThread(THREAD_ALL_ACCESS, 0, GetCurrentThreadId());

	//------------------------------------------------------------------------
	//create profiler thread
	//------------------------------------------------------------------------
	ProfilerThread* profilerthread = new ProfilerThread(
	thisthread, //thread to target(this thread)
	num_samples, //do N samples
	false);//don't kill target when completed

	//------------------------------------------------------------------------
	//start the thread
	//------------------------------------------------------------------------
	profilerthread->launch();*/
}


const HANDLE getProcessWideHandle()
{
	HANDLE processWideThreadHandle;

	BOOL result = DuplicateHandle(
		GetCurrentProcess(),
		GetCurrentThread(),
		GetCurrentProcess(),
		&processWideThreadHandle,
		0,
		FALSE, // not inheritable
		DUPLICATE_SAME_ACCESS);

	assert(result);
	return processWideThreadHandle;
}




// DE: 20090325: Profiler has a list of threads to profile
ProfilerThread::ProfilerThread(HANDLE target_process_, const std::vector<HANDLE>& target_threads, HANDLE pipeHandle, float slowFrameLimit)
:	profilers(),
target_process(target_process_),callstackPipe(pipeHandle),slowFrameLimit(slowFrameLimit)
{
	if (pipeHandle)
	{
    // TODO: allow multiple pipes (threads) to be sampled at the same time
	  profilers.push_back(Profiler(pipeHandle,slowFrameLimit, callstacks, flatcounts));
	}
	else
	{
  	// DE: 20090325: Profiler has a list of threads to profile, one Profiler instance per thread
	  profilers.reserve(target_threads.size());
	  for(std::vector<HANDLE>::const_iterator it = target_threads.begin(); it != target_threads.end(); ++it){
		  profilers.push_back(Profiler(target_process_, *it, callstacks, flatcounts));
	  }
	}
	numsamplessofar = 0;
	failed = false;
	symbolsPercent = 0;

	filename = wxFileName::CreateTempFileName(wxEmptyString);
	
}

ProfilerThread::~ProfilerThread()
{

}


void CALLBACK sprof_callback(UINT wTimerID, UINT msg,
							 DWORD dwUser, DWORD dw1, DWORD dw2)
{
	ProfilerThread* thread = reinterpret_cast<ProfilerThread*>(dwUser);

	thread->sample();

}

bool ProfilerThread::sample()
{
	// DE: 20090325: Profiler has a list of threads to profile, one Profiler instance per thread
	for(std::vector<Profiler>::iterator it = profilers.begin(); it != profilers.end(); ++it){
		Profiler& profiler(*it);
		profiler.sampleTarget();
	}
	numsamplessofar++;
  return false;
  	

}

class ProcPred
{
public:
	bool operator () (std::pair<std::string, int>& a, std::pair<std::string, int>& b)
	{
		return a.second > b.second;
	}
};

void ProfilerThread::run()
{

	if(!SetThreadPriority(getHandle(), THREAD_PRIORITY_TIME_CRITICAL))
	{
		error("failed to set thread priority to highest.");
		return;
	}


	try 
	{
	  struct Timer
	  {
	    Timer(){timeBeginPeriod(1);}
	    ~Timer(){timeEndPeriod(1);}
	  } scopedTimer;
	  
		//------------------------------------------------------------------------
		//load up the debug info for it
		//------------------------------------------------------------------------
		SymbolInfo syminfo(target_process);

		//there are two ways to do this... either to Sleep() 1 ms between samples,
		//or to use the Windows multimedia timer.
		//The Sleep method is simpler, and seems to work alright.

		const bool use_mm_timer = false;

		if(use_mm_timer)
		{
			HRESULT rc;

			HRESULT htimer = timeSetEvent(5, 1, sprof_callback, reinterpret_cast<DWORD>(this),
				TIME_PERIODIC | TIME_CALLBACK_FUNCTION);
			assert(htimer);

			while(!this->commit_suicide)//while(numsamplessofar < numsamples)
			{
				Sleep(250);
			}

			rc = timeKillEvent(htimer);
			if(rc != TIMERR_NOERROR)
			{
				//error
			}
		}
		else
		{
			while(!this->commit_suicide)//numsamplessofar < numsamples)
			{
				Sleep(1);

//         DWORD start = timeGetTime();
				bool terminate = sample();
//         int duration = timeGetTime()-start;
//         char buf[10];
//         sprintf(buf,"%d\n",duration);
//         OutputDebugString(buf);
        
				if (terminate) break;
			}
		}

	std::cout << "computing and saving profile results..." << std::endl;

	int numsymbols = 0;
	std::map<PROFILER_ADDR, int> symbols;
	std::map<std::string, int> symbolidtable;

		//get process id of the process the target thread is running in
		//const DWORD process_id = GetProcessIdOfThread(profiler.getTarget());

		wxFFileOutputStream out(filename);
		wxZipOutputStream zip(out);
		wxTextOutputStream txt(zip);

		if (!out.IsOk() || !zip.IsOk())
		{
			error("Error writing to file");
			return;
		}

		//SymbolInfo syminfo(target_process);

		//------------------------------------------------------------------------
		//save instruction pointer count results
		//------------------------------------------------------------------------

		zip.PutNextEntry(_T("IPcounts.txt"));

		txt << wxUint32(flatcounts.size()) << "\n";

		for(std::map<PROFILER_ADDR, int>::const_iterator i = flatcounts.begin(); 
			i != flatcounts.end(); ++i)
		{
			PROFILER_ADDR addr = i->first;

			std::string addr_file;
			int addr_line;
			syminfo.getLineForAddr(addr, addr_file, addr_line);

			int count = i->second;

			txt << ::toHexString(addr) << " " << count << 
				"         \"" << addr_file << "\" " << addr_line << "\n";
		}

		zip.PutNextEntry(_T("symbols.txt"));

		// Build up addr->procedure symbol table.
		std::map<PROFILER_ADDR, bool> used_addresses;
		for(std::map<PROFILER_ADDR, int>::const_iterator i = flatcounts.begin(); 
			i != flatcounts.end(); ++i)
		{
			PROFILER_ADDR addr = i->first;
			used_addresses[addr] = true;
		}

		for(std::map<CallStack, int>::const_iterator i = callstacks.begin(); 
			i != callstacks.end(); ++i)
		{
			const CallStack &callstack = i->first;
			for (size_t n=0;n<callstack.addr.size();n++)
			{
				used_addresses[callstack.addr[n]] = true;
			}
		}

		int done = 0;
		int used_total = static_cast<int>(used_addresses.size());
		for (std::map<PROFILER_ADDR, bool>::iterator i = used_addresses.begin(); i != used_addresses.end(); i++)
		{
			int proclinenum;
			std::string procfile;
			PROFILER_ADDR addr = i->first;

			const std::string proc_name = "\"" + syminfo.getProcForAddr(addr, procfile, proclinenum) + "\"";
			const std::string full_proc_name = "\"" + syminfo.getModuleNameForAddr(addr) + "\" " + 
				proc_name + " \"" + procfile + "\"" + " " + ::toString(proclinenum);
			
			if (symbolidtable.find(full_proc_name) == symbolidtable.end())
				symbolidtable[full_proc_name] = numsymbols++;

			symbols[addr] = symbolidtable[full_proc_name];

			symbolsPercent = MulDiv(done, 100, used_total);
			done++;
		}

		for(std::map<std::string, int>::const_iterator i = symbolidtable.begin(); i != symbolidtable.end(); ++i)
		{
			std::string str = i->first;
			int id = i->second;

			txt << "sym" << id << " " << str << "\n";
		}

		//------------------------------------------------------------------------
		//write callstack counts to disk
		//------------------------------------------------------------------------
		zip.PutNextEntry(_T("callstacks.txt"));

		for(std::map<CallStack, int>::const_iterator i = callstacks.begin(); 
			i != callstacks.end(); ++i)
		{
			const CallStack &callstack = i->first;
			int count = i->second;

			txt << count;
			for (std::vector<PROFILER_ADDR>::const_iterator i = callstack.addr.begin(); i != callstack.addr.end(); i++)
			{
				txt << " sym" << symbols[*i];
			}
			txt << "\n";
		}

		if (!out.IsOk() || !zip.IsOk())
		{
			error("Error writing to file");
			return;
		}
	} 
	catch(ProfilerExcep& e) {
		// see if it's an actual error, or did the thread just finish naturally
		for(std::vector<Profiler>::const_iterator it = profilers.begin(); it != profilers.end(); ++it){
			const Profiler& profiler(*it);
			if (!profiler.targetExited())
			{
				error("ProfilerExcep: " + e.what());
				return;
			}
		}
	} catch(SymbolInfoExcep& e) {
		error("SymbolInfoExcep: " + e.what());
		return;
	}

	symbolsPercent = 100;

	std::cout << "Profiling successfully completed." << std::endl;
	//::MessageBox(NULL, "Profiling successfully completed.", "Profiler", MB_OK);
}


void ProfilerThread::error(const std::string& what)
{
	failed = true;
	std::cerr << "ProfilerThread Error: " << what << std::endl;

	::MessageBox(NULL, std::string("Error: " + what).c_str(), "Profiler Error", MB_OK);
}
