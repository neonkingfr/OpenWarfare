/*=====================================================================
sortlist.cpp
------------

Copyright (C) Dan Engelbrecht

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html.
=====================================================================*/
#include "osutils.h"

int GetCPUCores(){
	SYSTEM_INFO systemInfo;
	::GetSystemInfo(&systemInfo);
	__int64 cpuCount = systemInfo.dwNumberOfProcessors;
	return static_cast<int>(cpuCount);
}

int GetCountFromBitMask(DWORD bitMask){
	int count = 0;
	while(bitMask != 0){
		if((bitMask & 1) != 0){
			++count;
		}
		bitMask = bitMask >> 1;
	}
	return count;
}

int GetCoresForProcess(HANDLE process){
	DWORD processAffinityMask = 0;
	DWORD systemAffinityMask = 0;
	BOOL okFlag = ::GetProcessAffinityMask(process, &processAffinityMask, &systemAffinityMask);
	if((okFlag == FALSE) || (processAffinityMask == 0)){
		return GetCPUCores();
	}
	else{
		return GetCountFromBitMask(processAffinityMask);
	}
}

