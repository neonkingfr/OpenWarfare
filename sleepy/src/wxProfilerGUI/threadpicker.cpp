/*=====================================================================
threadpicker.cpp
----------------
File created by ClassTemplate on Sun Mar 20 17:12:56 2005

Copyright (C) Nicholas Chapman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html.
=====================================================================*/
#include "threadpicker.h"
#include "mainwin.h"
#include "../profiler/profilerthread.h"
#include "../profiler/symbolinfo.h"
#include <dbghelp.h>

// IDs for the controls and the menu commands
enum
{
	// menu items
	ProcWin_Exit = 1,
	ProcWin_Refresh,

	// it is important for the id corresponding to the "About" command to have
	// this standard value as otherwise it won't be handled properly under Mac
	// (where it is special and put into the "Apple" menu)
	ProcWin_About = wxID_ABOUT
};

BEGIN_EVENT_TABLE(ThreadPicker, wxFrame)
EVT_BUTTON(wxID_OK, ThreadPicker::OnLaunchProfiler)
EVT_MENU(wxID_OPEN, ThreadPicker::OnOpen)
EVT_LIST_ITEM_ACTIVATED(PROCESS_LIST, ThreadPicker::OnDoubleClicked)
EVT_MENU(ProcWin_Exit, ThreadPicker::OnQuit)
EVT_MENU(ProcWin_Refresh, ThreadPicker::OnRefresh)
EVT_MENU(ProcWin_About, ThreadPicker::OnAbout)	
EVT_BUTTON(ProcWin_Refresh, ThreadPicker::OnRefresh)
EVT_BUTTON(ProcWin_Exit, ThreadPicker::OnQuit)
END_EVENT_TABLE()

ThreadPicker::ThreadPicker()
:	wxFrame(NULL, -1, wxString(_T("Sleepy")), 
			 wxDefaultPosition, wxDefaultSize,
			 wxDEFAULT_FRAME_STYLE)
{
    SetIcon(sleepy_icon);

	wxMenu *menuFile = new wxMenu;

	// the "About" item should be in the help menu
	wxMenu *helpMenu = new wxMenu;
	helpMenu->Append(ProcWin_About, _T("&About...\tF1"), _T("Show about dialog"));

	menuFile->Append(wxID_OPEN, _T("&Open..."), _T("Opens an existing profile"));
	menuFile->Append(ProcWin_Refresh, _T("&Refresh"), _T("Refreshes the process list"));
	menuFile->Append(ProcWin_Exit, _T("E&xit\tAlt-X"), _T("Quit this program"));

	// now append the freshly created menu to the menu bar...
	wxMenuBar *menuBar = new wxMenuBar();
	menuBar->Append(menuFile, _T("&File"));
	menuBar->Append(helpMenu, _T("&Help"));

	// ... and attach this menu bar to the frame
	SetMenuBar(menuBar);

	wxBoxSizer *rootsizer = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *panelsizer = new wxBoxSizer(wxVERTICAL);

	wxPanel *panel = new wxPanel(this);

	// DE: 20090325 one list for processes and one list for selected process threads
	threadlist = new ThreadList(panel, wxDefaultPosition, wxDefaultSize);
	processlist = new ProcessList(panel, wxDefaultPosition, wxDefaultSize, threadlist);

	rootsizer->Add(panel, 1, wxEXPAND | wxALL);
	panelsizer->Add(new wxStaticText(panel, -1, "Select a process to profile:"), 0, wxLEFT|wxRIGHT|wxTOP, 10);
	panelsizer->Add(processlist, 1, wxEXPAND | wxALL, 10);

	// DE: 20090325 title for thread list
	panelsizer->Add(new wxStaticText(panel, -1, "Select thread(s) to profile:"), 0, wxLEFT|wxRIGHT, 10);
	panelsizer->Add(threadlist, 1, wxEXPAND | wxALL, 10);

  { // add slow frame limit
	  wxSizer *region = new wxBoxSizer(wxHORIZONTAL);
	  
	  slowFrameLimit = new wxTextCtrl(panel, -1,	"100", wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_NUMERIC) );
	  region->Add(new wxStaticText(panel, -1,	"Slow frame threshold:"), 0, wxLEFT| wxALIGN_BOTTOM, 10);
	  region->Add(slowFrameLimit,		0, wxALIGN_BOTTOM | wxLEFT, 10);

	  panelsizer->Add(region, 0, wxLEFT|wxRIGHT|wxBOTTOM|wxEXPAND, 10);
	}
	
	{ // add buttons
	  int border = ConvertDialogToPixels(wxSize(2, 0)).x;
	  wxSizer *buttons = new wxBoxSizer(wxHORIZONTAL);
	  buttons->Add(new wxButton(panel, ProcWin_Refresh, "Refresh"),	0, wxALIGN_LEFT  | wxRIGHT,			border);
	  buttons->AddStretchSpacer();
	  buttons->Add(new wxButton(panel, wxID_OK),						0, wxALIGN_RIGHT | wxLEFT|wxRIGHT,	border);
	  buttons->Add(new wxButton(panel, ProcWin_Exit,	"E&xit"),		0, wxALIGN_RIGHT | wxLEFT,			border);

  	
	  panelsizer->Add(buttons, 0, wxLEFT|wxRIGHT|wxBOTTOM|wxEXPAND, 10);
	}

	panel->SetSizer(panelsizer);
	panel->SetAutoLayout(TRUE);

	SetSizer(rootsizer);
	rootsizer->SetSizeHints(this);
	SetAutoLayout(TRUE);

	SetSize(wxSize(400, 500));
	Centre();
}

void ThreadPicker::OnOpen(wxCommandEvent& event)
{
	filename = ProfilerGUI::PromptOpen(this);
	if (!filename.empty())
	{
		Hide();
		LoadProfileData(filename);
	}
}

void ThreadPicker::OnLaunchProfiler()
{
	Hide();
	std::string value = slowFrameLimit->GetValue();
	float slowLimit = atof(value.c_str());
	std::string filename;
	if (LaunchProfiler(filename, slowLimit))
		LoadProfileData(filename);
	else
		Show();
}

void ThreadPicker::OnLaunchProfiler(wxCommandEvent& event)
{
	OnLaunchProfiler();
}

void ThreadPicker::OnDoubleClicked(wxListEvent& event)
{
	OnLaunchProfiler();
}

void ThreadPicker::OnQuit(wxCommandEvent& event)
{
	wxExit();
}

void ThreadPicker::OnRefresh(wxCommandEvent& event)
{
	processlist->updateProcesses();
}

void ThreadPicker::OnAbout(wxCommandEvent& event)
{
	ProfilerGUI::ShowAboutBox();
}


ThreadPicker::~ThreadPicker()
{

}

/*
unsigned int ThreadPicker::getSelectedThread()
{
	const ProcessInfo* info = processlist->getSelectedProcess();

	if(info)
	{
		return info->threads[0].getID();
	}

	return 0;
}
*/

#define SleepyAPINameBase "BIS_{B8D1E291-55CA-49a6-A1DD-EFB9C32582DB}"
#define SleepyAPIName(XXX) SleepyAPINameBase #XXX

// DE: 20090325 attaches to specific thread
bool ThreadPicker::AttachToProcess(
  const ProcessInfo* procinfo, const ThreadInfo* threadinfo,
  HANDLE &processHandle, HANDLE &threadHandle, HANDLE &pipeHandle,
  std::string &filename
)
{
	DWORD err;

	processHandle = NULL;
	threadHandle = NULL;
	pipeHandle = NULL;
	
	filename = "";

  DWORD startConnect = timeGetTime();
  Retry:
  HANDLE pipe = CreateFile("\\\\.\\pipe\\" SleepyAPIName(Callstacks),GENERIC_READ,0,NULL,OPEN_EXISTING,0,NULL);
  if (pipe==NULL || pipe==INVALID_HANDLE_VALUE)
  {
    HRESULT err = GetLastError();
    if (err==ERROR_PIPE_BUSY)
    {
      // pipe is busy: attempt waiting for a while, hoping we may connect
      Sleep(0);
      DWORD now = timeGetTime();
      const DWORD timeout = 10000;
      if (now-startConnect<timeout)
      {
        goto Retry;
      }
    }
  }
  else
  {
    DWORD read = 0;
    // read the protocol version
    DWORD version = 0;
    BOOL ok = ReadFile(pipe,&version,sizeof(version),&read,NULL);
    if (ok && read==sizeof(version))
    {
      const int myVersion = 2;
      if (version!=myVersion)
      {
    		wxLogError("Pipe version mismatch: Sleepy version %d, app version %d.",myVersion,version);
        return false;
      }
    }
    // read the process ID from the pipe and convert it to process handle 
    DWORD pid = 0;
    ok = ReadFile(pipe,&pid,sizeof(pid),&read,NULL);
    HANDLE process = INVALID_HANDLE_VALUE;
    if (ok && read==sizeof(pid))
    {
      process = OpenProcess(READ_CONTROL|PROCESS_QUERY_INFORMATION|PROCESS_VM_READ,false,pid);
    }
    
    if (process && process!=INVALID_HANDLE_VALUE)
    {
      pipeHandle = pipe;
      processHandle = process;
    }
    else
    {
      HRESULT ret = GetLastError();
      (void)ret;
    }

    return true;
  }

  if (!threadinfo || !procinfo)
  {
		wxLogError("Profiling pipe not found, no thread selected.");
    return false;
  }
	// DE: 20090325 attaches to specific thread
	DWORD target_thread_id = threadinfo->getID();
	DWORD target_process_id = procinfo->getID();

	//------------------------------------------------------------------------
	//get a handle to the target thread
	//------------------------------------------------------------------------
	threadHandle = OpenThread(	THREAD_ALL_ACCESS,			//DWORD dwDesiredAccess,
								FALSE,						//BOOL bInheritHandle,
								target_thread_id			//DWORD dwThreadId
								);
	err = GetLastError();
	if (threadHandle == NULL)
	{
		return false;
	}

	//------------------------------------------------------------------------
	//Get handle to target process
	//------------------------------------------------------------------------
	processHandle = OpenProcess (PROCESS_ALL_ACCESS, FALSE, target_process_id); 
	err = GetLastError();

	if ( processHandle == NULL )
	{
		CloseHandle(threadHandle);
		return false;
	}

	return true;
}

// DE: 20090325 attaches to specific a list of threads
bool ThreadPicker::LaunchProfiler(std::string &filename, float slowFrameLimit)
{
	HANDLE processHandle = NULL;
	HANDLE pipeHandle = NULL;
	filename = "";

	const ProcessInfo* processInfo = processlist->getSelectedProcess();
	std::vector<const ThreadInfo*> selectedThreads = threadlist->getSelectedThreads();

	// DE: 20090325 attaches to specific a list of threads
	std::vector<HANDLE> threadHandles;
	
	if (selectedThreads.empty())
	{
		HANDLE threadHandle = NULL;
		if (!AttachToProcess(NULL, NULL, processHandle, threadHandle, pipeHandle, filename))
		{
			wxLogError("Cannot attach to pipe.");
			return false;
		}
		if (pipeHandle==0)
		{
		  wxLogError("Profiling pipe not found or connection failed.");
		  return false;
		}
	}
	else
	{
		for(std::vector<const ThreadInfo*>::const_iterator it = selectedThreads.begin(); it != selectedThreads.end(); ++it){
			HANDLE threadHandle = NULL;
			const ThreadInfo* threadInfo(*it);
			if (!AttachToProcess(processInfo, threadInfo, processHandle, threadHandle, pipeHandle, filename))
			{
				wxLogError("Cannot attach to running process.");
			}
			else{
				threadHandles.push_back(threadHandle);
			}
		}
	  // DE: 20090325 attaches to specific a list of threads
	  if(threadHandles.size() == 0){
		  wxLogError("Cannot attach to any running threads.");
		  return false;
	  }
	}
	//------------------------------------------------------------------------
	//create the profiler thread
	//------------------------------------------------------------------------
	// DE: 20090325 attaches to specific a list of threads
	ProfilerThread* profilerthread = new ProfilerThread(processHandle,threadHandles, pipeHandle, slowFrameLimit);
  bool aborted = PerformProfiling(profilerthread);

	{
		// DE: 20090325 attaches to specific a list of threads
		for(std::vector<HANDLE>::const_iterator it = threadHandles.begin(); it != threadHandles.end(); ++it){
			HANDLE threadHandle(*it);
			CloseHandle(threadHandle);
		}
		threadHandles.clear();
	}
	CloseHandle(processHandle);

	bool failed = profilerthread->getFailed();
	filename = profilerthread->getFilename();

	delete profilerthread;
	profilerthread = NULL;

	if (failed)
		return false;

	if (aborted)
		return false;

	if (filename.empty())
	{
		wxLogError("There was a problem creating the profile data.");
		return false;
	}

	return true;
}

void ThreadPicker::LoadProfileData(const std::string &filename)
{
	MainWin *frame = new MainWin(_T("Sleepy"), filename);

	frame->Show(TRUE);
	frame->Update();
	frame->Reset();
}

bool PerformProfiling(ProfilerThread* profilerthread)
{
  //------------------------------------------------------------------------
  //start the profiler thread
  //------------------------------------------------------------------------
  profilerthread->launch(false);

  wxMessageDialog okdialog(NULL, 
    "Press OK to stop profiling and display collected results.", 
    "Profiling...",
    wxOK|wxCANCEL);

  bool aborted = (okdialog.ShowModal() == wxID_CANCEL);

  profilerthread->commit_suicide = true;

  {
    wxProgressDialog dlg("Sleepy", "Gathering symbols...", 100);
    while(true)
    {
      int percent = profilerthread->getSymbolsPercent();
      if (percent >= 100 || profilerthread->getFailed())
        break;
      WaitForSingleObject(profilerthread->getHandle(), 100);
    }
    WaitForSingleObject(profilerthread->getHandle(), INFINITE);
  }
  return aborted;
}
