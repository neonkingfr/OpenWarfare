/*=====================================================================
mainwin.cpp
-----------
File created by ClassTemplate on Sun Mar 13 21:12:40 2005

Copyright (C) Nicholas Chapman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html.
=====================================================================*/
#include "mainwin.h"
#include "database.h"

#include "../utils/stringutils.h"

// IDs for the controls and the menu commands
enum
{
	// menu items
	MainWin_Quit = 1,
	MainWin_Open,
	MainWin_SaveAs,

	// it is important for the id corresponding to the "About" command to have
	// this standard value as otherwise it won't be handled properly under Mac
	// (where it is special and put into the "Apple" menu)
	MainWin_About = wxID_ABOUT
};

MainWin::MainWin(const wxString& title, 
				 const std::string& profilepath,
				 //const std::string& procpath, const std::string& symbolspath, const std::string& ipcountspath,
				 long style)
				 :	wxFrame(),
				 	aui(this, wxAUI_MGR_RECTANGLE_HINT|wxAUI_MGR_ALLOW_FLOATING)
{
	wxConfig config(REG_APPNAME, REG_VENDORNAME);
	wxString str;

	wxPoint pos(50, 50);
	wxSize size = wxGetDisplaySize();
	size.Scale(0.75f, 0.75f);

	if (config.Read("MainWinMaximized", 0L))
		style |= wxMAXIMIZE;
	pos.x = config.Read("MainWinX", (long)pos.x);
	pos.y = config.Read("MainWinY", (long)pos.x);
	size.x = config.Read("MainWinW", (long)size.x);
	size.y = config.Read("MainWinH", (long)size.y);

	Create(NULL, -1, title, pos, size, style);
				
	panel = NULL;
	proclist = NULL;
	sourceview = NULL;
	this->profilepath = profilepath;

	// set the frame icon
    SetIcon(sleepy_icon);

#if wxUSE_MENUS
	// create a menu bar
	wxMenu *menuFile = new wxMenu;

	// the "About" item should be in the help menu
	wxMenu *helpMenu = new wxMenu;
	helpMenu->Append(MainWin_About, _T("&About...\tF1"), _T("Show about dialog"));

	menuFile->Append(MainWin_Open, _T("&Open..."), _T("Opens an existing profile"));
	menuFile->Append(MainWin_SaveAs, _T("Save &As..."), _T("Saves the profile data to a file"));
	menuFile->Append(MainWin_Quit, _T("E&xit\tAlt-X"), _T("Quit this program"));

	// now append the freshly created menu to the menu bar...
	wxMenuBar *menuBar = new wxMenuBar();
	menuBar->Append(menuFile, _T("&File"));
	menuBar->Append(helpMenu, _T("&Help"));

	// ... and attach this menu bar to the frame
	SetMenuBar(menuBar);
#endif // wxUSE_MENUS

	CreateStatusBar(2);

	database = new Database();
	database->loadFromPath(profilepath);

	sourceview = new SourceView(this, this);

	// Create the windows
	proclist = new ProcList(this, LIST_CTRL,
		wxDefaultPosition, wxDefaultSize,
		wxLC_EDIT_LABELS, 
		sourceview, database, true);

	callers = new ProcList(this, LIST_CTRL,
		wxDefaultPosition, wxDefaultSize,
		wxLC_EDIT_LABELS, 
		NULL, database, false);

	callees = new ProcList(this, LIST_CTRL,
		wxDefaultPosition, wxDefaultSize,
		wxLC_EDIT_LABELS, 
		NULL, database, false);

	// Construct the docking panes
	wxSize clientSize = GetClientSize();

	aui.AddPane(proclist, wxAuiPaneInfo()
		.Name(wxT("Functions"))
		.CentrePane()
		.Caption(wxT("Functions"))
		.CaptionVisible(true)
		);

	aui.AddPane(sourceview, wxAuiPaneInfo()
		.Name(wxT("Source"))
		.Caption(wxT("Source"))
		.CloseButton(false)
		.Bottom()
		.Layer(0)
		.BestSize(clientSize.GetWidth() * 2/3, clientSize.GetHeight() * 1/3)
		);

	aui.AddPane(callers, wxAuiPaneInfo()
		.Name(wxT("CalledFrom"))
		.Caption(wxT("Called From"))
		.CloseButton(false)
		.Right()
		.Layer(1)
		.BestSize(clientSize.GetWidth() * 1/3, clientSize.GetHeight() * 1/2)
		);

	aui.AddPane(callees, wxAuiPaneInfo()
		.Name(wxT("ChildCalls"))
		.Caption(wxT("Child Calls"))
		.CloseButton(false)
		.Right()
		.Layer(1)
		.BestSize(clientSize.GetWidth() * 1/3, clientSize.GetHeight() * 1/2)
		);

	
	aui.Update();

	if (config.Read("MainWinLayout", &str) ) {
		aui.LoadPerspective(str);
	}

	aui.Update();

	// Tie it all together
	proclist->setCallersView(callers);
	proclist->setCalleesView(callees);
	callers->setParentView(proclist);
	callees->setParentView(proclist);
}

void MainWin::Reset()
{
	proclist->showRoot(NULL);
}


MainWin::~MainWin()
{
	wxConfig config(REG_APPNAME, REG_VENDORNAME);

	config.Write("MainWinMaximized", IsMaximized());

	wxString str = aui.SavePerspective();
	config.Write("MainWinLayout", str);
	config.Write("MainWinX", GetScreenRect().x);
	config.Write("MainWinY", GetScreenRect().y);
	config.Write("MainWinW", GetScreenRect().width);
	config.Write("MainWinH", GetScreenRect().height);

	aui.UnInit();
	delete database;	
}


// the event tables connect the wxWindows events with the functions (event
// handlers) which process them. It can be also done at run-time, but for the
// simple menu events like this the static method is much simpler.
BEGIN_EVENT_TABLE(MainWin, wxFrame)
EVT_CLOSE(MainWin::OnClose)
EVT_MENU(MainWin_Quit,  MainWin::OnQuit)
EVT_MENU(MainWin_Open,  MainWin::OnOpen)
EVT_MENU(MainWin_SaveAs,  MainWin::OnSaveAs)
EVT_MENU(MainWin_About, MainWin::OnAbout)	
END_EVENT_TABLE()

void MainWin::OnClose(wxCloseEvent& WXUNUSED(event))
{
	wxExit();
}

void MainWin::OnQuit(wxCommandEvent& WXUNUSED(event))
{
	wxExit();
}

void MainWin::OnOpen(wxCommandEvent& WXUNUSED(event))
{
	wxString filename = ProfilerGUI::PromptOpen(this);
	if (filename.empty())
		return;

	database->loadFromPath(filename.c_str());
	Reset();
}

void MainWin::OnSaveAs(wxCommandEvent& WXUNUSED(event))
{
	wxFileDialog dlg(this, "Save File As", "", "capture.sleepy", "Sleepy Profiles (*.sleepy)|*.sleepy", 
		wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
	if (dlg.ShowModal() != wxID_CANCEL)
	{
		if (!CopyFile(profilepath.c_str(), dlg.GetPath(), FALSE))
		{
			wxLogSysError("Could not save profile data.");
		}
	}
}

void MainWin::OnAbout(wxCommandEvent& WXUNUSED(event))
{
	ProfilerGUI::ShowAboutBox();
}

void MainWin::setCurrent(const std::string& currentfile_, int currentline_)
{
	if(currentfile != currentfile_ || currentline != currentline_)
	{
		currentfile = currentfile_;
		currentline = currentline_;

		SetStatusText(std::string("Source file: " + currentfile).c_str(), 0);
		SetStatusText(std::string("Line " + ::toString(currentline)).c_str(), 1);
	}
}
