/*=====================================================================
profilergui.cpp
---------------
File created by ClassTemplate on Sun Mar 13 18:16:34 2005

Copyright (C) Nicholas Chapman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html.
=====================================================================*/
#include "profilergui.h"

#include "threadpicker.h"
#include "../utils/stringutils.h"
#include "../profiler/profilerthread.h"

// DE: 20090325 Linking fails in debug target under visual studio 2005
// RJM: works for me :-/
//#include <wx/apptrait.h>
//#if wxUSE_STACKWALKER && defined( __WXDEBUG__ )
//// silly workaround for the link error with debug configuration:
//// \src\common\appbase.cpp
//wxString wxAppTraitsBase::GetAssertStackTrace()
//{
//   return wxT("");
//}
//#endif

wxIcon sleepy_icon;

ProfilerGUI::ProfilerGUI()
{
	
}


ProfilerGUI::~ProfilerGUI()
{
	
}


IMPLEMENT_APP(ProfilerGUI)

void ProfilerGUI::ShowAboutBox()
{
    wxAboutDialogInfo info;
    info.SetName(_T("Very Sleepy"));
    info.SetVersion(_T("0.5"));
    info.SetDescription(_T(
		"Open-source CPU profiler\n"
		"\n"
		"Maintained by Richard Mitton\n"
		"\n"
		"Authors:\n"
		"- Nicholas Chapman\n"
		"- Richard Mitton\n"
		"- Dan Engelbrecht\n"
		));
	//info.SetCopyright(_T("(c) 2008 Nicholas Chapman && Richard Mitton"));
    info.SetWebSite(_T("http://www.codersnotes.com/sleepy"), _T("Very Sleepy web site"));


    wxAboutBox(info);
}

wxString ProfilerGUI::PromptOpen(wxWindow *parent)
{
	wxFileDialog dlg(parent, "Open File", "", "", "Sleepy Profiles (*.sleepy)|*.sleepy", 
		wxFD_OPEN);
	if (dlg.ShowModal() != wxID_CANCEL)
		return dlg.GetPath();
	else
		return wxEmptyString;
}

bool ProfilerGUI::OnInit()
{
	::doStringUtilsUnitTests();

	sleepy_icon = wxICON(sleepy);

	threadpicker = new ThreadPicker();
  
  
  
	threadpicker->Show();

	return true;
}

