/*=====================================================================
database.h
----------

Copyright (C) Nicholas Chapman
Copyright (C) Richard Mitton

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html.
=====================================================================*/
#ifndef __DATABASE_H_666_
#define __DATABASE_H_666_

#include "profilergui.h"
#include <string>
#include <vector>
#include <map>
#include "lineinfo.h"

/*=====================================================================
Database
--------

=====================================================================*/
class Database
{
public:
  struct Symbol
  {
    std::string id;
    std::string module;
    std::string procname;
    std::string sourcefile;
    int         sourceline;
  };

  struct Item
  {
    const Symbol *symbol;
    int inclusive, exclusive;
  };

  struct List
  {
    List() { totalcount = 0; }

    std::vector<Item> items;
    int totalcount;
  };

  Database();
	virtual ~Database();
  void clear();

  void loadFromPath(const std::string& profilepath);
  void scanRoot();

  const List &getRoot() const { return rootList; }
  List getCallers(const Symbol *symbol) const;
  List getCallees(const Symbol *symbol) const;

  const LINEINFOMAP *getLineInfo(const std::string &srcfile) const;

private:
  struct CallStack
  {
    std::vector<const Symbol *> stack;
    int samplecount;
  };

  std::map<std::string, Symbol *> symbols;
  std::vector<CallStack> callstacks;
	std::map<std::string, LINEINFOMAP > fileinfo;
	List rootList;

  void loadSymbols(wxInputStream &file);
  void loadProcList(wxInputStream &file);
  void loadIpCounts(wxInputStream &file);
};

#endif //__DATABASE_H_666_
