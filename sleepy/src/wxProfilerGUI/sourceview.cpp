/*=====================================================================
sourceview.cpp
--------------
File created by ClassTemplate on Tue Mar 15 21:38:06 2005

Copyright (C) Nicholas Chapman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html.
=====================================================================*/
#include "sourceview.h"


#include <fstream>
#include "../utils/stringutils.h"
#include "mainwin.h"
//#undef min
//#undef max
#include <algorithm>

BEGIN_EVENT_TABLE(SourceView, wxTextCtrl)
	EVT_PAINT(SourceView::OnPaint)
	EVT_UPDATE_UI(SOURCE_VIEW, SourceView::OnUpdateUI)
END_EVENT_TABLE()


SourceView::SourceView(wxWindow *parent, MainWin* mainwin_)
:	wxTextCtrl(parent, SOURCE_VIEW, wxEmptyString,
               wxDefaultPosition, wxDefaultSize,
               wxTE_MULTILINE | wxSUNKEN_BORDER | wxTE_READONLY | wxTE_DONTWRAP | wxTE_RICH2  ),
	mainwin(mainwin_)
{
	AppendText("Select a procedure from the list above.");
}


SourceView::~SourceView()
{
	
}


void SourceView::showFile(const std::string& path, int proclinenum, const LINEINFOMAP& lineinfomap)
{
	currentfile = path;

	SetValue("");//clear text
	SetDefaultStyle(wxTextAttr(*wxBLACK));

	std::ifstream file(path.c_str());

	if(!file)
	{
		AppendText(std::string("[ Could not open file '" + path + "'. ]").c_str());
		return;
	}

	Show(false);//hide window temporarily

	std::string displaytext;
	int linenum = 1;//1-based counting
	//int showpos = 0;//index of character to make visible in order to scroll to line where proc is defined.
	size_t textsize = 0;
	const int MARGIN_WIDTH = 5;
	while(file)
	{
		std::string line;
		std::getline(file, line);
		
		if(!file)
			break;

		std::string lineprefix;
		LINEINFOMAP::const_iterator result = lineinfomap.find(linenum);
		
		if(result == lineinfomap.end())
		{
			lineprefix = std::string(MARGIN_WIDTH, ' ');
		}
		else
		{
			lineprefix = ::floatToString((*result).second.percentage, 2);
			while(lineprefix.size() < MARGIN_WIDTH)
				::concatWithChar(lineprefix, ' ');
		}

		lineprefix += " ";


		const std::string printline = lineprefix + line + "\n";//form line to display
		//AppendText(printline.c_str());//write to control
		displaytext += printline;

		//highlight the start of it
		/*bool res = SetStyle(textsize, textsize + lineprefix.length(), wxTextAttr(wxNullColour, *wxLIGHT_GREY));
		assert(res);

		res = SetStyle(textsize + lineprefix.length(), textsize + printline.length(), wxTextAttr(wxNullColour, wxNullColour));
		assert(res);*/

		/*if(linenum == proclinenum)
		{
			showpos = textsize - 1;
			if(showpos < 0) showpos = 0;
		}*/

		textsize += printline.length();


		//displaytext += lineprefix + line + "\n";

		
	
		linenum++;
	}

	AppendText(displaytext.c_str());

	//TEMP:
	const int showpos = std::max((int)XYToPosition(0, std::max(proclinenum - 2, 0)), 0);

	const bool res = SetStyle(0, (long)textsize, wxTextAttr(wxNullColour, wxNullColour, 
						wxFont(8, wxMODERN , wxNORMAL, wxNORMAL)));
	assert(res);
	//SetValue(displaytext.c_str());

	//------------------------------------------------------------------------
	//Go thru and highlight after new lines
	//------------------------------------------------------------------------
	for(size_t i=0; i<displaytext.size() - 3; ++i)
	{
		if(displaytext[i] == '\n')
		{
			if(displaytext[i+1] != ' ' || displaytext[i+2] != ' ' || displaytext[i+3] != ' ')
			{
				const size_t highlightend = std::min(i+1 + MARGIN_WIDTH, displaytext.size());
				bool res = SetStyle((long)(i+1), (long)highlightend, wxTextAttr(*wxWHITE, *wxRED));
				assert(res);
			}

			SetDefaultStyle(wxTextAttr(*wxBLACK));
		}
	}
			


	Show(true);//show window again

	ShowPosition(showpos);
}

void SourceView::OnPaint(wxPaintEvent& event)
{

	{
	//wxPaintDC dc(this);
	//dc.BeginDrawing();
	//dc.SetBrush( *wxRED_BRUSH );
	//dc.DrawLine(100, 0, 100, 300);
	//dc.EndDrawing();
	}
	event.Skip();

	//SetDefaultStyle(wxTextAttr(*wxBLACK, *wxWHITE));
	
}

void SourceView::OnUpdateUI(wxUpdateUIEvent& event)
{
	long col, line;
	const bool result = PositionToXY(GetInsertionPoint(), &col, &line);
	if(!result)
		line = -1;
	else
		line += 1;//convert to 1-based line numbering

	mainwin->setCurrent(currentfile, line);

	event.Skip();
}

