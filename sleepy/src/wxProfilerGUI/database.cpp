/*=====================================================================
Database.cpp
------------

Copyright (C) Nicholas Chapman
Copyright (C) Richard Mitton

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html.
=====================================================================*/
#include "database.h"

#include "../utils/stringutils.h"
#include "sourceview.h"
#include <fstream>
#include "globals.h"

Database::Database()
{
}

Database::~Database()
{
	clear();
}

void Database::clear()
{
	for (std::map<std::string, Symbol *>::iterator i = symbols.begin(); i != symbols.end(); i++)
	{
		delete i->second;
	}

	symbols.clear();
	callstacks.clear();
	fileinfo.clear();
	rootList.items.clear();
	rootList.totalcount = 0;
}

void Database::loadFromPath(const std::string& profilepath)
{
	clear();

	wxFFileInputStream in(profilepath);
	wxZipInputStream zip(in);

	if (!in.IsOk() || !zip.IsOk())
	{
		wxLogError("The profile data cannot be read.");
		return;
	}

	while(true)
	{
		wxZipEntry *entry = zip.GetNextEntry();
		if ( !entry )
			break;
		 
		wxString name = entry->GetInternalName();

			 if (name == "symbols.txt")		loadSymbols(zip);
		else if (name == "callstacks.txt")	loadProcList(zip);
		else if (name == "IPcounts.txt")	loadIpCounts(zip);
		else {
			wxLogWarning("Other fluff found in capture file (%s)", name.c_str());
		}
	}

	scanRoot();
}

// read symbol table
void Database::loadSymbols(wxInputStream &file)
{
	wxTextInputStream str(file);
	int c = 0;
	while(!file.Eof())
	{
		wxString line = str.ReadLine();
		if (line.IsEmpty())
			break;

		std::istringstream stream(line.c_str());
		Symbol *sym = new Symbol;

		stream >> sym->id;
		::readQuote(stream, sym->module);
		::readQuote(stream, sym->procname);
		::readQuote(stream, sym->sourcefile);
		stream >> sym->sourceline;
		symbols[sym->id] = sym;
	}
}

// read callstacks
void Database::loadProcList(wxInputStream &file)
{
	wxTextInputStream str(file);

	while(!file.Eof())
	{
		wxString line = str.ReadLine();
		if (line.IsEmpty())
			break;

		std::istringstream stream(line.c_str());

		CallStack callstack;
		stream >> callstack.samplecount;

		while(true)
		{
			std::string id;
			stream >> id;
			if (id.empty())
				break;

			const Symbol *sym = symbols[id];
			callstack.stack.push_back(sym);
		}

		callstacks.push_back(callstack);
	}
}

void Database::loadIpCounts(wxInputStream &file)
{
	int totallinecount = 0;
	wxTextInputStream str(file);

	str >> totallinecount;

	int c = 0;
	while(!file.Eof())
	{
		wxString line = str.ReadLine();
		if (line.IsEmpty())
			break;

		std::istringstream stream(line.c_str());

		std::string memaddr;
		stream >> memaddr;

		int count;
		stream >> count;

		std::string srcfile;
		int linenum;

		::readQuote(stream, srcfile);
		stream >> linenum;

		LineInfo& lineinfo = (fileinfo[srcfile])[linenum];
		lineinfo.count += count;
		lineinfo.percentage += 100.0f * ((float)count / (float)totallinecount);
	}
}

void Database::scanRoot()
{
	std::map<const Symbol *, int> exclusive, inclusive;

	wxProgressDialog progressdlg("Sleepy", "Scanning database...", (int)callstacks.size(), NULL,
		wxPD_APP_MODAL|wxPD_REMAINING_TIME);

	rootList.items.clear();
	rootList.totalcount = 0;

	int progress = 0;
	for (std::vector<CallStack>::const_iterator i = callstacks.begin(); i != callstacks.end(); i++)
	{  
		exclusive[i->stack[0]] += i->samplecount;
		std::map<const Symbol *, bool> seen;
		for (size_t n=0;n<i->stack.size();n++)
		{
			// we filter out duplicates, to avoid getting funny numbers when 
			// using recursive functions.
			if (!seen[i->stack[n]])
			{
				inclusive[i->stack[n]] += i->samplecount;
				seen[i->stack[n]] = true;
			} 
		}
		rootList.totalcount += i->samplecount;

		progressdlg.Update(progress++);
	}

	for (std::map<const Symbol *, int>::const_iterator i = inclusive.begin(); i != inclusive.end(); i++)
	{
		Item item;
		item.symbol = i->first;
		item.exclusive = exclusive[item.symbol];
		item.inclusive = i->second;
		rootList.items.push_back(item);
	}
}

Database::List Database::getCallers(const Database::Symbol *symbol) const
{
	List list;
	std::map<const Symbol *, int> counts;
	for (std::vector<CallStack>::const_iterator i = callstacks.begin(); i != callstacks.end(); i++)
	{ 
		// Only include callstacks that have our symbol in.
		for (size_t n=0;n<i->stack.size()-1;n++)
		{
			if (i->stack[n] == symbol)
			{
				const Symbol *caller = i->stack[n+1];
				counts[caller] += i->samplecount;
				list.totalcount += i->samplecount;
			}
		}
	}

	for (std::map<const Symbol *, int>::const_iterator i = counts.begin(); i != counts.end(); i++)
	{
		Item item;
		item.symbol = i->first;
		item.inclusive = i->second;
		item.exclusive = i->second;
		list.items.push_back(item);
	}

	return list;
}

Database::List Database::getCallees(const Database::Symbol *symbol) const
{
	List list;
	std::map<const Symbol *, int> counts;
	for (std::vector<CallStack>::const_iterator i = callstacks.begin(); i != callstacks.end(); i++)
	{ 
		// Only include callstacks that have our symbol in.
		int foundat = -1;
		for (size_t n=1;n<i->stack.size();n++)
		{
			if (i->stack[n] == symbol)
			{
				foundat = (int)n; // fixme - is there an issue here with recursive functions?
				break;
			}
		}
		if (foundat == -1)
			continue;

		counts[i->stack[foundat-1]] += i->samplecount;
		list.totalcount += i->samplecount;
	}

	for (std::map<const Symbol *, int>::const_iterator i = counts.begin(); i != counts.end(); i++)
	{
		Item item;
		item.symbol = i->first;
		item.inclusive = i->second;
		item.exclusive = i->second;
		list.items.push_back(item);
	}

	return list;
}

const LINEINFOMAP *Database::getLineInfo(const std::string &srcfile) const
{
	std::map<std::string, LINEINFOMAP >::const_iterator i = fileinfo.find(srcfile);
	if (i != fileinfo.end())
	{
		return &i->second;
	} else {
		return NULL;
	}
}
