/*=====================================================================
threadpicker.h
--------------
File created by ClassTemplate on Sun Mar 20 17:12:56 2005

Copyright (C) Nicholas Chapman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

http://www.gnu.org/copyleft/gpl.html.
=====================================================================*/
#ifndef __THREADPICKER_H_666_
#define __THREADPICKER_H_666_

#include "profilergui.h"
#include "processlist.h"

// DE: 20090325 Include for list to pick thread(s)
#include "threadlist.h"

class ThreadPicker : public wxFrame
{
public:
	ThreadPicker();
	virtual ~ThreadPicker();

	//unsigned int getSelectedThread();

	// DE: 20090325 Attaches to a specific thread
	bool AttachToProcess(
	  const ProcessInfo *procinfo, const ThreadInfo* threadinfo,
	  HANDLE &processHandle, HANDLE &threadHandle, HANDLE &pipeHandle,
	  std::string &filename
	);
	void LoadProfileData(const std::string &filename);
	bool LaunchProfiler(std::string &filename, float slowFrameLimit);

  void UpdateSorting();

	void OnOpen(wxCommandEvent& event);
	void OnQuit(wxCommandEvent& event);
	void OnRefresh(wxCommandEvent& event);
	void OnAbout(wxCommandEvent& event);
	void OnLaunchProfiler();
	void OnLaunchProfiler(wxCommandEvent& event);
	void OnDoubleClicked(wxListEvent& event);

private:
	ProcessList* processlist;

	// DE: 20090325 Include for list to pick thread(s)
	ThreadList* threadlist;
	wxBitmap* bitmap;
	std::string filename;
	wxTextCtrl *slowFrameLimit;

	DECLARE_EVENT_TABLE()
};

#endif //__THREADPICKER_H_666_




