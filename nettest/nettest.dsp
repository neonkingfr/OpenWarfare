# Microsoft Developer Studio Project File - Name="nettest" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 60000
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=nettest - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "nettest.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "nettest.mak" CFG="nettest - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "nettest - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "nettest - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "nettest - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /G6 /W3 /GR /GX- /O2 /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "NET_TEST" /YX /FD /c
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib largeint.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "nettest - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "nettest___Win32_Debug"
# PROP BASE Intermediate_Dir "nettest___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ  /c
# ADD CPP /nologo /G6 /W3 /Gm /GR /GX- /ZI /Od /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "NET_TEST" /YX /FD /GZ  /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib largeint.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "nettest - Win32 Release"
# Name "nettest - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\bitmask.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Algorithms\crc32.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\maps.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netapi.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netchannel.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\netlog.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netmessage.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netpeer.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netpool.cpp
# End Source File
# Begin Source File

SOURCE=.\nettest.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\peerfactory.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\pocritical.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\pothread.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\potime.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\randomJames.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Statistics\statistics.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\useAppFrameDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\useMallocMemFunctions.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Poseidon\lib\appFrameExt.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\bitmask.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Strings\bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\cachelist.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Algorithms\crc32.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\elementpch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\global.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\iQFBank.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\list.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\listBidir.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\logflags.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\maps.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\FreeOnDemand\memFreeReq.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Poseidon\lib\MemGrow.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Poseidon\lib\MemHeap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\memtype.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\multisync.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netapi.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netchannel.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netglobal.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\netlog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netmessage.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netpch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netpeer.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netXboxConfig.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\normalConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\normalNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\peerfactory.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\platform.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\pocritical.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\posemaphore.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\pothread.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\potime.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Algorithms\qsort.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\randomJames.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Strings\rString.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\scopeLock.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\smallArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Statistics\statistics.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\stdIncludes.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeOpts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\win.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
