#ifdef _MSC_VER
#  pragma once
#endif

/**
    @file   pothread.hpp
    @brief  Portable threads.

    Implemented by CreateThread() (WIN32) OR pthread_create() (pthreads).
    <p>Copyright &copy; 2001-2002 by BIStudio (www.bistudio.com)
    @author PE
    @since  29.11.2001
    @date   8.7.2002
    @todo   More routines for "portable threads" will be needed?
*/

#ifndef _POTHREAD_H
#define _POTHREAD_H

#include "pocritical.hpp"

//------------------------------------------------------------
//  Portability macros:

#ifdef _WIN32

   /// Thread identifier.
   typedef HANDLE ThreadId;

   /// Return type of thread main routine.
#  define THREAD_PROC_RETURN DWORD
   /// Calling mode of thread main routine.
#  define THREAD_PROC_MODE   WINAPI

#else

   /// Thread identifier.
   typedef pthread_t ThreadId;

   /// Return type of thread main routine.
#  define THREAD_PROC_RETURN void*
   /// Calling mode of thread main routine.
#  define THREAD_PROC_MODE

#endif

//------------------------------------------------------------
//  Thread-manipulating functions:

/**
    Creates and starts a new thread.
    The new thread will be executed concurrently with the calling thread
    and will start immediately (in "runnable" mode).
    @param  id Pointer to new thread's id [out]. Can be <code>NULL</code>.
    @param  stackSize Initial stack size (or <code>0</code> for default/inherited size).
    @param  threadProc Address of the thread-routine.
    @param  arg Argument to be passed to the thread-routine.
    @return <code>true</code> if succeeded.
*/
extern bool poThreadCreate ( ThreadId *id, long stackSize, THREAD_PROC_RETURN (THREAD_PROC_MODE *threadProc)(void*), void *arg );

/**<
    The <code>threadProc</code> should be declared as follows:

    <code>
    <pre>
    THREAD_PROC_RETURN THREAD_PROC_MODE proc ( void *param )
    {
        ...
        return (THREAD_PROC_RETURN)anything;
    }
    </pre>
    </code>

*/

/**
    Joins the given thread with me.
    Suspends the execution of the calling thread (waits) until the thread identified
    by <code>id</code> terminates.
    @param  id Thread's identifier.
    @param  result Optional address of a buffer to receive thread return value.
    @return <code>true</code> if succeeded.
*/
extern bool poThreadJoin ( ThreadId id, THREAD_PROC_RETURN *result );

/**
    Current thread's identifier.
    @param  id Thread's identifier [out].
*/
extern void poThreadId ( ThreadId &id );

/**
    Sets priority of the given thread.
    @param  id Thread's identifier [in].
    @param  delta Relative thread priority (from <code>-2</code> /lowest/ to <code>2</code> /highest/) [in].
    @return <code>true</code> if succeeded.
*/
extern bool poSetPriority ( ThreadId id, int delta );

/**
    Sets priority of the running (calling) thread.
    @param  delta Relative thread priority (from <code>-2</code> /lowest/ to <code>2</code> /highest/) [in].
    @return <code>true</code> if succeeded.
*/
extern bool poSetMyPriority ( int delta );

//------------------------------------------------------------
//  Private MT-safe support:

#ifdef _WIN32

class MemAllocSafe {

public:

#if ALLOC_DEBUGGER

    static void *Alloc ( int &size, const char *file, int line, const char *postfix )
    {
        return safeNew(size);
    }

#else

    static void *Alloc ( int &size )
    {
        return safeNew(size);
    }

#endif

    static void Free ( void *mem, int size )
    {
        safeDelete(mem);
    }

    static void Free ( void *mem )
    {
        safeDelete(mem);
    }

    static void Unlink ( void *mem )
    {}

    };

#else

#  define MemAllocSafe MemAllocD

#endif

//------------------------------------------------------------
//  MT-safe RefCount:

class RefCountSafe {

private:

    /// Number of references to this object.
    mutable int _count;

protected:

    /// Critical section to lock _count. It can be used for general object locking..
    mutable PoCriticalSection lock;

public:

    //! Default constructor.
    RefCountSafe ()
    { _count = 0; }

    //! Copy constructor (_count attribute will not be copied).
    /*!
        \param src Reference to source object.
    */
    RefCountSafe ( const RefCountSafe &src )
    { _count = 0; }

    //! Copying of object (_count attribute will not be copied).
    void operator = ( const RefCountSafe &src )
    { _count = 0; }

    //! Destructor
    virtual ~RefCountSafe ();               // use destructor of derived class

    /// Enter the object's critical section.
    inline void enter () const
    { lock.enter(); }

    /// Leave the object's critical section.
    inline void leave () const
    { lock.leave(); }

    //! Adding of reference to this object.
    /*!
        \return Number of references.
    */
    int AddRef () const
    {
        enter();
        int result = ++_count;
        leave();
        return result;
    }

    //! Releasing of reference to this object.
    /*!
        \return Number of references.
    */
    int Release () const
    {
        enter();
        int ret=--_count;
        leave();
        if( ret == 0 )
            delete (RefCountSafe*)this;
        return ret;
    }

    //! Determines number of references to this object.
    /*!
        \return Number of references.
    */
    int RefCounter () const
    { return _count; }

    //! get memory used by this object
    //! not including memory used to hold this object itself,
    //! which is covered by sizeof
    virtual double GetMemoryUsed () const;

    };

#endif
