#ifndef _CHECKMEM_HPP
#define _CHECKMEM_HPP

#if defined MFC_NEW
	// MFC application needs to use MFC new

	#define safeNew(size) malloc(size)
	#define safeDelete(mem) free(mem)

	inline size_t MemoryUsed() {return 1024*1024;}

	inline void SafeMemoryCleanUp() {}
	inline void MemoryCleanUp() {}
	inline int MemoryFreeBlocks() {return 1;}
	inline bool IsOutOfMemory() {return false;}

	// if desired, use MS VC++ memory debuging
	//#define _CRTDBG_MAP_ALLOC 1
	//#include <crtdbg.h>

#else

	class MemFunctions
	{
		public:
		virtual void *New(size_t size) {return malloc(size);}
		virtual void *New(size_t size, const char *file, int line) {return malloc(size);}
		virtual void Delete(void *mem){free(mem);}
		virtual void Delete(void *mem, const char *file, int line) {free(mem);}


		//! base of memory allocation space, NULL is unknown/undefined
		virtual void *HeapBase() {return NULL;}

#ifdef _WIN32
		//! approximate total ammount of commited memory
		virtual size_t HeapUsed() {return 0;}
#else
		//! approximate total ammount of commited memory
		virtual size_t HeapUsed() {return linuxMemoryUsage();}
#endif

		//! approximate total count of free memory blocks (used to determine fragmentation)
		virtual int FreeBlocks() {return 1;}

		//! approximate total count of allocated memory blocks (used to determine fragmentation)
		virtual int MemoryAllocatedBlocks() {return 1;}

		//! list all allocated blocks to debug log
		virtual void Report() {}

		//! check heap integrity
		virtual bool CheckIntegrity() {return true;}
		//! check if out of memory is signalized
		//! if true, application should limit memory allocation as much as possible
		virtual bool IsOutOfMemory() {return false;}

		//! clean-up any cached memory
		virtual void CleanUp() {}
	};

	extern MemFunctions *GMemFunctions;
	extern MemFunctions *GSafeMemFunctions;

	inline void * CCALL operator new (size_t size, int addr, char const *file, int line)
	{ // placement new
		return (void *)addr;
	}
	#ifdef _CPPUNWIND
		inline void CCALL operator delete (void *mem, int addr, char const *file, int line)
		{ // placement delete
		}
	#endif

	// note: global array versions ([]) are ignored in VC5.0

	inline void * CCALL operator new ( size_t size ) {return GMemFunctions->New(size);}
	inline void * CCALL operator new[] ( size_t size ) {return GMemFunctions->New(size);}

	inline void * CCALL operator new ( size_t size, const char *file, int line )
	{
		return GMemFunctions->New(size,file,line);
	}
	inline void * CCALL operator new[] ( size_t size, const char *file, int line )
	{
		return GMemFunctions->New(size,file,line);
	}
	inline void CCALL operator delete ( void *ptr )
	{
		GMemFunctions->Delete(ptr);
	}
	inline void CCALL operator delete[] ( void *ptr )
	{
		GMemFunctions->Delete(ptr);
	}

	#ifdef _CPPUNWIND
		inline void CCALL operator delete ( void *ptr, const char *file, int line )
		{
			GMemFunctions->Delete(ptr,file,line);
		}
	#endif

	#if !_RELEASE

		#define ALLOC_DEBUGGER 1

		#ifndef CCALL
			#define CCALL
		#endif

		// memory errors tracking for custom new handlers


		#define debugNew new(__FILE__,__LINE__)
		#define debugNewAddr(addr) new(addr,__FILE__,__LINE__)

		#define stdNew new()
		#define stdNewAddr new(addr)

		#define newAddr(addr) new(addr)

		#include <Es/Memory/debugNew.hpp>
	#endif

	// required interface for MT safe heap
	inline void *safeNew ( size_t size ){return GSafeMemFunctions->New(size);}
	inline void safeDelete ( void *mem ){GSafeMemFunctions->Delete(mem);}

	inline bool IsOutOfMemory() {return GMemFunctions->IsOutOfMemory();}
	inline size_t MemoryUsed() {return GMemFunctions->HeapUsed();}

	inline void SafeMemoryCleanUp() {GSafeMemFunctions->CleanUp();}
	//inline void MemoryCleanUp() {GMemFunctions->CleanUp();}
	inline int MemoryFreeBlocks() {return GMemFunctions->FreeBlocks();}

#endif

#endif
