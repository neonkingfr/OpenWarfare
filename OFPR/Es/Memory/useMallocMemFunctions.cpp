#include <Es/essencepch.hpp>
#include "checkMem.hpp"

#pragma warning(disable:4074)
#pragma init_seg(compiler)

static MemFunctions MemFunctionsMalloc INIT_PRIORITY_URGENT;

MemFunctions *GMemFunctions = &MemFunctionsMalloc;
MemFunctions *GSafeMemFunctions = &MemFunctionsMalloc;
