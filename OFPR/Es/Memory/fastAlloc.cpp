#pragma warning(disable:4073)
#pragma init_seg(lib)

#include <Es/essencepch.hpp>
#include <Es/platform.hpp>
#include "fastAlloc.hpp"


// should be included in source file with #pragma init_seg(lib)
// to guarantee initialization before all user variables

//////////////////////////////////////////////////////////////////////////////

FastAlloc::FastAlloc( size_t n, const char *name, int alignOffset )
:esize( n+4<sizeof(Link) ? sizeof(Link) : n+4 ),
_name(name),
_alignOffset(alignOffset),
//head(0),
allocated(1)
{
	//#ifdef _KNI
	//const int alignMem=16;
	//#else
	// if structure is smaller than 32 it is very likely
	// it does not have any special alignment requirements
	// to conserve memory we will align it to 4B only
	const int alignMem=esize>32 ? 16 : 4;
	//#endif
	esize=(esize+alignMem-1)&~(alignMem-1);

	int chSize = Chunk::size-_alignOffset;
	_nElemPerChunk = chSize/esize;
}
/*
CLList<FastAlloc::Chunk>::~CLList()
{
	if (First())
	{
		Log("Non-empty link list destruct avoided.");
	}
}*/

FastAlloc::~FastAlloc()
{
	// note: automatic destruction of links takes place
	//Log("Destruct %s",_name);

	// check if all chunks may be freed now
	if( --allocated==0 )
	{
		FreeChunks();
		Assert(!chunksBusy.First());
		Assert(!chunksFree.First());
	}
	else
	{
		LogF("FastAlloc %s not free when destructed.",Name());
	}
}

#if !_RELEASE
	#define FA_DIAGS 0
#endif

#include "normalNew.hpp"

FastAlloc::Chunk *FastAlloc::NewChunk()
{
	#if ALLOC_DEBUGGER && defined _CPPRTTI
		Chunk *chunk=new(FileLine(_name),esize) Chunk;
	#else
		Chunk *chunk=new Chunk;
	#endif
	//LogF("%s: NewChunk %x",_name,chunk);
	return chunk;

}
void FastAlloc::DeleteChunk( Chunk *chunk )
{
	delete chunk;
}

#include "debugNew.hpp"

void FastAlloc::FreeChunks()
{
	//Log("FreeChunks %s",_name);
	#if FA_DIAGS
		int size=0;
	#endif
	// free all free chunks
	Chunk *n;
	while ((n=chunksFree.First())!=NULL)
	{
		// all chunks should be empty
		Assert(n->allocated==0);
		#if FA_DIAGS
			size+=sizeof(*n);
		#endif
		chunksFree.Delete(n);
		DeleteChunk(n); // use chunk delete
	}

	// note: there should be no busy chunks now
	Assert(chunksBusy.First()==NULL);
	// free all busy chunks
	while ((n=chunksBusy.First())!=NULL)
	{
		#if FA_DIAGS
			size+=sizeof(*n);
		#endif
		chunksBusy.Delete(n);
		DeleteChunk(n); // use chunk delete
	}

	#if FA_DIAGS
		LogF("FastAlloc of type %s (%d) - %d KB",_name,esize,(size+1023)/1024);
	#endif
}

void *FastAlloc::AllocCounted(size_t n)
{
	allocated++; // count allocation

	Assert( n<=esize );
	if( chunksFree.First()==NULL ) Grow();
	Chunk *ch = chunksFree.First();

	if (!ch) return NULL;

	Link *p = ch->head;

	ch->head = p->next;
	ch->allocated++;

	if (!ch->head)
	{
		// move this chunk to busy chunk list
		Assert (ch->allocated==_nElemPerChunk);
		chunksFree.Delete(ch);
		chunksBusy.Insert(ch);
		//Log("%s: Chunk %x moved to busy",_name,ch);
	}

	*(Chunk **)p=ch;
	// mark chunk to link
	return p;
}

void FastAlloc::FreeCounted( void*pAlloc, bool fast)
{
	if (!pAlloc) return;

	Chunk *ch=*(Chunk **)pAlloc;

	if (ch->null1)
	{
		Fail("FastAlloc chunk header corrupted.");
		RptF("Chunk %x, null %x",ch,ch->null1);
		return;
	}
	Link *p=static_cast<Link*>(pAlloc);
	//p--; // link is skipped

	// add to head of free links in this chunk
	p->next=ch->head;
	bool wasBusy = ch->head==NULL;
	ch->head=p;

	// mark which chunk do we belong to
	p->chunk=ch;

	if (wasBusy)
	{
		// was in busy list
		// move it to free list
		chunksBusy.Delete(ch);
		chunksFree.Insert(ch);
		//Log("%s: Chunk %x moved to free",_name,ch);
	}

	--ch->allocated;
	if (ch->allocated==0)
	{
		if (!fast)
		{
			ChunkRemove(ch);
		}
	}

	// check if all chunks may be freed now
	if( --allocated==0 ) FreeChunks();
}

void *FastAlloc::Alloc( size_t n )
{
	Assert( n+4<=esize );
	char *counted = (char *)AllocCounted(n);
	if (!counted) return NULL;
	return counted+sizeof(int); // skip header
}

void FastAlloc::Free( void*pAlloc )
{
	if (!pAlloc) return;
	char *counted = (char *)pAlloc-sizeof(int);
	// free but do not clean chunks yet
	FreeCounted(counted);
}

FastAlloc *FastAlloc::WhichAllocator( void*pAlloc )
{
	if (!pAlloc) return NULL;
	Chunk *ch=*(Chunk **)pAlloc;
	if( ch->null1!=0 ) return NULL;
	return ch->allocator;
}

// free unused chunks
void FastAlloc::CleanUp()
{
	// clean-up this one allocator
	// if we have reference count zero, remove the chunk
	// no need to check busyChunks
	for (Chunk *ch = chunksFree.First(); ch; )
	{
		Chunk *next = chunksFree.Next(ch);
		if( ch->allocated<=0 )
		{
			Assert( ch->allocated==0 );
			ChunkRemove(ch);
		}
		ch = next;
	}
	// sort chunks by filled
	// most filled should be first
}

/*!
Used in genuinity check when test failed.
Dammages internal structures of TrackLLink allocator.
This usually leads to strange error or program crashes, but not very soon.
It should be therefore difficult to track the place causing the dammage.
*/

bool FastAlloc::CheckIfFree(Chunk *chunk, void *data) const
{
	Chunk *chunkVerify = *(Chunk **)data;
	// if item does not contain pointer back to chunk, it must be free
	//if (chunkVerify!=chunk) return true;
	// check if data are from this chunk
	// traverse free list and check if data is in it
	Link *p = chunk->head;
	while (p)
	{
		if (data>=p && data<(char *)p+esize) return true;
		p = p->next;
	}
	// item is not free: chunk must match
	/*
	if (chunkVerify!=chunk)
	{
		__asm nop;
		return true;
	}
	*/
	return false;
}

//! free first used chunk in alloc
void Dammage_FastAlloc(FastAlloc *alloc)
{
	FastAlloc::Chunk *chunk = alloc->chunksFree.First(); // check chunk with some free links
	if (!chunk || chunk->allocated==0)
	{
		// there is no free chunk or has no items allocated
		chunk = alloc->chunksBusy.First(); // check busy chunk
	}
	if (!chunk) return;

	// we have to find some non-free block of the chunk
	// use first busy block of the chunk
	// it will become free, but the memory will still be used,
	// which should lead to strange effects soon
	void *p = chunk->mem;
	while (alloc->CheckIfFree(chunk,p))
	{
		// note: freeing free block is not possible, it is not linked back to chunk
		p = (char *)p + alloc->ItemSize();
		if (p>=chunk->mem+FastAlloc::Chunk::size)
		{
			// no dammage can be done
			return;
		}
	}
	alloc->FreeCounted(p);
}

void FastAlloc::ChunkRemove( Chunk *chunk )
{
	// chunk may be released
	#if !_RELEASE
	bool found = true;
	for (Chunk *ch=chunksFree.Start(); chunksFree.NotEnd(ch); ch = chunksFree.Advance(ch))
	{
		if (ch==chunk) 
		{
			found = true;
			break;
		}
	}
	Assert(found);
	#endif

	chunksFree.Delete(chunk);
	// remove all items from this chunk from free list
	// no need to remove links - they are not stored anywhere

	DeleteChunk(chunk);
}

void FastAlloc::Grow()
{
	Chunk *n=NewChunk(); // use chunk new
	// if we are out of memory, we cannot create a new chunk
	if (!n) return;
	n->null1=0;

	n->allocated=0;
	n->allocator=this;

	chunksFree.Insert(n);

	char *start=n->mem+_alignOffset;

	const int nelem= _nElemPerChunk;

	char *last=&start[(nelem-1)*esize];
	for( char*p = start; p<last; p+=esize )
	{
		Link *link = reinterpret_cast<Link*>(p);

		link->next=reinterpret_cast<Link*>(p+esize);
		link->chunk=n;
	}
	Link *lastLink = reinterpret_cast<Link*>(last);
	lastLink->next=0;
	lastLink->chunk=n;

	n->head=reinterpret_cast<Link*>(start);
}

#include <Es/Containers/smallArray.hpp>

// keep track of all FastCAlloc
static VerySmallArray
<
	InitPtr<FastCAlloc>, 1024*sizeof(InitPtr<FastCAlloc>)
> AllInstances INIT_PRIORITY_URGENT;


FastCAlloc::FastCAlloc( size_t n, const char *name )
// note: base class is always constructed before members
:base(n,name,12)
{
	// insert FastCAlloc into FastCAlloc list
	AllInstances.Add(this);
	//Log("Constructed %s",name);
}

FastCAlloc::~FastCAlloc()
{
	
	for( int i=0; i<AllInstances.Size(); i++ )
	{
		FastCAlloc *ii=AllInstances[i];
		if( ii==this )
		{
			AllInstances.Delete(i);
			return;
		}
	}
}

void *FastCAlloc::CAlloc( size_t n )
{
	Assert( n+4<=esize );
	char *counted = (char *)FastAlloc::AllocCounted(n);
	if (!counted) return NULL;
	return counted+sizeof(int); // skip header
}

void FastCAlloc::CFree( void*pAlloc )
{
	if (!pAlloc) return;
	char *counted = (char *)pAlloc-sizeof(int);
	// free but do not clean chunks yet
	FastAlloc::FreeCounted(counted);
}



void FastCAlloc::CleanUpAll()
{
	for( int i=0; i<AllInstances.Size(); i++ )
	{
		AllInstances[i]->CleanUp();
	}
}
