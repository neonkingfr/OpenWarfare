#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SCOPE_LOCK_HPP
#define _SCOPE_LOCK_HPP

/*!
	\file
	Scope locking related templates
*/

//! lock object passed as argument during lifetime of ScopeLock object
template<class Lock>
class ScopeLock
{
	protected:
	const Lock *_lock;
	public:
	ScopeLock( const Lock &lock ) {_lock=&lock;_lock->Lock();}
	~ScopeLock() {_lock->Unlock();}
};

//! unlock object passed as argument during lifetime of ScopeLock object
template<class Lock>
class ScopeUnlock
{
	protected:
	Lock *_lock;
	public:
	ScopeUnlock( const Lock &lock ) {_lock=&lock;_lock->Unlock();}
	~ScopeUnlock() {_lock->Lock();}
};

#endif
