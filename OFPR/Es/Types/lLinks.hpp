#ifdef _MSC_VER
#pragma once
#endif

#ifndef _LLINKS_HPP
#define _LLINKS_HPP

#include <Es/Types/pointers.hpp>
#include <Es/Memory/fastAlloc.hpp>
#include <Es/Containers/array.hpp>

class TrackLLinks;
class RemoveLLinks;

class RemoveLLinks: public RefCount
{
	friend class TrackLLinks;
	TrackLLinks *_track;

	public:
	RemoveLLinks() {_track=NULL;}
	RemoveLLinks( const RemoveLLinks &src ){_track=NULL;}
	void operator =( const RemoveLLinks &src ){_track=NULL;}

	~RemoveLLinks();

	__forceinline TrackLLinks *Track() const {return _track;}
};

#include <Es/Memory/normalNew.hpp>

class TrackLLinks: public RefCount
{
	friend class RemoveLLinks;

	RemoveLLinks *_remove;

	public:
	TrackLLinks()
	{
	}
	explicit TrackLLinks( RemoveLLinks *obj )
	{
		_remove=obj;
		if( obj )
		{
			Assert( !obj->_track );
			obj->_track=this;
		}
	}
	~TrackLLinks()
	{
		if( _remove ) _remove->_track=NULL;
		_remove=NULL;
	}

	__forceinline RemoveLLinks *GetObject() const {return _remove;}

	USE_FAST_ALLOCATOR_ID(TrackLLinks)
};

#include <Es/Memory/debugNew.hpp>

inline RemoveLLinks::~RemoveLLinks()
{
	if( _track ) _track->_remove=NULL;
	_track=NULL;
}

extern Ref<TrackLLinks> LLinkNil;

template <class Type>
class LLink
{
	mutable Ref<TrackLLinks> _tracker;

	public:
	LLink():_tracker(LLinkNil){}
	LLink( Type *ref )
	:_tracker( ref ? ( ref->Track() ? ref->Track() : new TrackLLinks(ref) ) : (TrackLLinks*)LLinkNil )
	{}

	__forceinline Type *GetLink() const
	{
		return static_cast<Type *>(_tracker->GetObject());
	}
	__forceinline operator Type *() const {return GetLink();}
	__forceinline Type * operator ->() const {return GetLink();}

	ClassIsMovable(LLink);
};

template <class Type>
class LLinkArray: public FindArray< LLink<Type> >
{
	public:
	int Count() const;
	void Compact();
	ClassIsMovable(LLinkArray)
};

template <class Type>
int LLinkArray<Type>::Count() const
{
	int i, n = 0;
	for (i=0; i<Size(); i++)
		if (Get(i)!=NULL) n++;
	return n;
}

template <class Type>
void LLinkArray<Type>::Compact()
{
	int d=0;
	for( int s=0; s<Size(); s++ )
	{
		if( Get(s)!=NULL )
		{
			if( s!=d ) Set(d)=Get(s);
			d++;
		}
	}
	Resize(d);
}

#endif


