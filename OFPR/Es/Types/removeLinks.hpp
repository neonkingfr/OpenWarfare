#ifdef _MSC_VER
#pragma once
#endif

#ifndef _REMOVELINKS_HPP
#define _REMOVELINKS_HPP

#include <Es/Containers/list.hpp>

// simple application of double-linked list
// when object of class derived from RemoveLinks is destructed,
// all links (pointers) to that object are set to NULL

#if !_RELEASE
	#define DO_LINK_DIAGS 1
#endif

class BaseLink;
class RemoveLinks;

class BaseLink: public SLink
{
	private:
	RemoveLinks *_ref;

	protected:
	void DoConstruct( RemoveLinks *ref );
	void DoDestruct();

	public:
	RemoveLinks *GetRef() const;

	BaseLink(){_ref=NULL;}
	BaseLink( RemoveLinks *src ){DoConstruct(src);}
	BaseLink( const BaseLink &src ){DoConstruct(src.GetRef());}
	BaseLink &operator = ( const BaseLink &src )
	{
		// note: src may be *this
		RemoveLinks *ref=src.GetRef();
		DoDestruct();
		DoConstruct(ref);
		return *this;
	}

	__forceinline bool NotNull() const {return _ref!=NULL;}
	__forceinline bool IsNull() const {return _ref==NULL;}
	//bool operator ! () const {return _ref==NULL;}
	
	__forceinline void Remove(){DoDestruct();}
	~BaseLink() {DoDestruct();}
};

class RemoveLinks: public RefCount, public SList<BaseLink>
{
	#if DO_LINK_DIAGS
		friend class BaseLink;
		int _nLinks;
	#endif

	public:
	RemoveLinks()
	{
		#if DO_LINK_DIAGS
			_nLinks=0;
		#endif
	}

	// on copy of linked object do not copy links
	RemoveLinks( const RemoveLinks &src )
	{
		#if DO_LINK_DIAGS
			_nLinks=0;
		#endif
	}
	void operator = ( const RemoveLinks &src )
	{
		#if DO_LINK_DIAGS
			_nLinks=0;
		#endif
		SLink::Reset();
	}

	~RemoveLinks();

	#if DO_LINK_DIAGS
		bool VerifyStructure();
	#endif
};

typedef RemoveLinks RefCountWithLinks;


inline RemoveLinks::~RemoveLinks()
{ // set all existing references to NULL
	#if DO_LINK_DIAGS
		Assert( VerifyStructure() );
	#endif
	while( First() )
	{
		First()->Remove();
	}
	#if DO_LINK_DIAGS
		Assert( _nLinks==0 );
	#endif
}	

#if DO_LINK_DIAGS
	inline bool RemoveLinks::VerifyStructure()
	{
		BaseLink *link=First();
		if( !link )
		{
			return( _nLinks==0 );
		}
		for( int i=0; i<_nLinks; i++ )
		{
			//Assert( link->next );
			//Assert( link->prev );
			//Assert( link->next->prev==link );
			link=Next(link);
			if( !link )
			{
				return ( _nLinks==i+1 );
			}
		}
		return false;
	}
#endif

inline void BaseLink::DoConstruct( RemoveLinks *ref )
{
	_ref=ref;
	if( _ref )
	{
		#if DO_LINK_DIAGS
			Assert( _ref->VerifyStructure() );
		#endif
		_ref->RemoveLinks::Insert(this);
		#if DO_LINK_DIAGS
			_ref->_nLinks++;
		#endif
	}
}
inline void BaseLink::DoDestruct()
{
	if( _ref )
	{
		#if DO_LINK_DIAGS
			Assert( _ref->VerifyStructure() );
		#endif
		_ref->Delete(this);
		#if DO_LINK_DIAGS
			_ref->_nLinks--;
		#endif
	}
	_ref=NULL;
}

__forceinline RemoveLinks *BaseLink::GetRef() const {return _ref;}

template <class Type>
class Link: public BaseLink
{
	#if _DEBUG
		Type *_refT; // this enable looking at variable

		public:
		Link():_refT(NULL){}
		Link( Type *src ):BaseLink(src),_refT(src){}
		Link( const Link &src ):BaseLink(src),_refT(src._refT){}

	#else
		public:
		Link(){}
		Link( Type *src ):BaseLink(src){}
		Link( const Link &src ):BaseLink(src){}
	#endif
	
	__forceinline Type *GetTypeRef() const {return static_cast<Type *>(GetRef());}
	__forceinline operator Type *() const {return GetTypeRef();}
	__forceinline Type &operator *() const {return *GetTypeRef();}
	__forceinline Type *operator ->() const {return GetTypeRef();}

	ClassIsGeneric(Link);
};

template <class Type>
class LinkArray: public FindArray< Link<Type> >
{
	public:
	int Count() const;
	void Compact();
	ClassIsMovable(LinkArray)
};

template <class Type>
int LinkArray<Type>::Count() const
{
	int i, n = 0;
	for (i=0; i<Size(); i++)
		if ((*this)[i]) n++;
	return n;
}

template <class Type>
void LinkArray<Type>::Compact()
{
	int d=0;
	for( int s=0; s<Size(); s++ )
	{
		if( Get(s) )
		{
			if( s!=d ) Set(d)=Get(s);
			d++;
		}
	}
	Resize(d);
}

#endif
