// template class for list
// fast o(1) insertion/deletion

#ifndef _CACHELIST_HPP
#define _CACHELIST_HPP

#include <Es/Framework/debugLog.hpp>

//! base class for defining elements of CLList
//! int template will allow multiple inheritance for single object

/*!
\patch_internal 1.45 Date 2/15/2002 by Ondra
- Fixed: More robust handling of CLTLink destrution
when still in list.
*/

template <int i=0> class CLTLink
{
	protected:
	void Reset()
	{
		prev=next=this;
	} // reset without any clean up

	public:
	CLTLink *prev;
	CLTLink *next;
	
	CLTLink(){prev=next=NULL;}
	~CLTLink()
	{
		if (prev || next)
		{
			Assert(prev);
			Assert(next);
			Delete();
		}
	}

	void Delete()
	{
		// check it is in list
		Assert( prev );
		Assert( next );
		// remove element from list

		next->prev=prev;
		prev->next=next;
	
		// not linked
		prev=next=NULL;
	}
	bool IsInList() const {return prev!=NULL;}
	
	private: // disable copy
	CLTLink( const CLTLink &src );
	void operator =( const CLTLink &src );
};

typedef CLTLink<0> CLDLink;

// circular list - any item can be used as root

template <class Type,class Base=CLDLink >
class CLList: public Base
{
	protected:
	void Reset(){Base::Reset();}

	public:
	CLList(){Reset();}
	~CLList()
	{
		Clear();
		// prepare for CLTLink destruction
		Assert (prev==this);
		Assert (next==this);
		prev = next = NULL;
	}
	void Insert( Type *element )
	{
		// insert before first element
		Base::next->Base::prev=element;
		element->Base::next=Base::next;
		
		element->Base::prev=this;
		Base::next=element;
	}
	void Add( Type *element )
	{
		// insert after the last element
		Base::prev->Base::next=element;
		element->Base::prev=Base::prev;
		
		element->Base::next=this;
		Base::prev=element;
	}
	static void InsertBefore( Type *after, Type *element )
	{
		// insert before the element
		after->Base::prev->Base::next=element;
		element->Base::prev=after->Base::prev;
		
		element->Base::next=after;
		after->Base::prev=element;
	}
	static void InsertAfter( Type *before, Type *element )
	{
		// insert after the element
		before->Base::next->Base::prev=element;
		element->Base::next=before->Base::next;
		
		element->Base::prev=before;
		before->Base::next=element;
	}

	void Delete( Type *element )
	{
		element->Base::Delete();
	}
	
	// use in for loops
	Type *Start() const
	{
		return static_cast<Type *>(Base::next);
	}
	static Type *Advance( Type *item )
	{
		return static_cast<Type *>(item->Base::next);
	}
	bool NotEnd( Type *item ) const
	{
		return item!=(Base *)this;
	}
	// sample for loop
	// for( Type *i=list.Begin(); list.NotEnd(i); i=list.Advance(i) ) {}
	
	// safe access
	Type *First() const
	{
		if( Base::next==const_cast<CLList *>(this) ) return NULL;
		Assert( Base::next );
		return static_cast<Type *>(Base::next);
	}

	Type *Next( Type *item ) const
	{
		if( item->Base::next==const_cast<CLList *>(this) ) return NULL;
		Assert( item->Base::next );
		return static_cast<Type *>(item->Base::next);
	}
	Type *Prev( Type *item ) const
	{
		if( item->Base::prev==const_cast<CLList *>(this) ) return NULL;
		Assert( item->Base::prev );
		return static_cast<Type *>(item->Base::prev);
	}
	
	Type *Last() const
	{
		if( Base::prev==const_cast<CLList *>(this) ) return NULL;
		Assert( Base::prev );
		return static_cast<Type *>(Base::prev);
	}

	void Move( CLList &src );
	void Clear();

	int Size() const;

	private: // disable copy
	CLList( const CLList &src );
	void operator = ( const CLList &src );
};

template <class Type,class Base>
void CLList<Type,Base>::Move( CLList &src )
{
	// take whole list src and move it into this
	// result: this contains merged list, src is empty
	// take first and last members of src
	Base *srcFirst=src.First();
	Base *srcLast=src.Last();
	if( !srcFirst ) return; // nothing to merge with
	// srcLast is not NULL
	// insert first..last before first member of this
	Base *first=Base::next; // first member of this - can be prev (if no members)
	// link last of src with first of this
	first->Base::prev=srcLast,srcLast->Base::next=first;
	// make first of src to be first of this
	Base::next=srcFirst,srcFirst->Base::prev=this;
	src.Base::prev=src.Base::next=&src;
}

template <class Type,class Base>
void CLList<Type,Base>::Clear()
{
	while( First() ) Delete(First());
}

template <class Type,class Base>
int CLList<Type,Base>::Size() const
{
	// note: size may be slow - use it carefully
	int count=0;
	for( Type *obj=First(); obj; obj=Next(obj) ) count++;
	return count;
}

typedef CLTLink<1> CLRefLink;

template <class Type>
class CLRefList: public CLList<Type,CLRefLink>
{
	typedef CLList<Type,CLRefLink> base;

	public:
	void Clear();
	~CLRefList(){Clear();}

	void Insert( Type *element )
	{
		element->AddRef();
		base::Insert(element);
	}
	void Add( Type *element )
	{
		element->AddRef();
		base::Add(element);
	}

	void Delete( Type *element )
	{
		base::Delete(element);
		element->Release();
	}

};

template <class Type>
void CLRefList<Type>::Clear()
{
	while( First() ) Delete(First());
}

#endif


