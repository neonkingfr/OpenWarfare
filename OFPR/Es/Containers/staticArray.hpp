#ifdef _MSC_VER
#pragma once
#endif

#ifndef _STATIC_ARRAY_HPP
#define _STATIC_ARRAY_HPP

#include <Es/Framework/debugLog.hpp>
#include <Es/Containers/array.hpp>


template <class Type>
class StaticArray: public AutoArray<Type,MemAllocSS>
{
	public:

	StaticArray(){}
	explicit StaticArray( const MemAllocSS &alloc )
	{
		SetStorage(alloc);
	}
};

#define StaticStorage MStorage

template <class Type>
class StaticArrayAuto: public AutoArray< Type, MemAllocSA >
{
	public:

	StaticArrayAuto(){} // no init - use dynamic allocation
	explicit StaticArrayAuto( void *mem, int size )
	{
		_alloc.Init(mem,size);
	}
};

// different alignement requirements

// default - 4 B

#define AUTO_STORAGE_ALIGNED(Type,name,size,atype) \
	const int name##_minGrow = StaticArrayAuto< Type >::MinGrow; \
	const int name##_size=size>name##_minGrow ? size : name##_minGrow; \
	atype name[(name##_size*sizeof(Type)+sizeof(atype)-1)/sizeof(atype)];

#define AUTO_STATIC_ARRAY_ALIGNED(Type,name,size,atype) \
	const int name##_minGrow = StaticArrayAuto< Type >::MinGrow; \
	const int name##_size=size>name##_minGrow ? size : name##_minGrow; \
	atype name##_storage[(name##_size*sizeof(Type)+sizeof(atype)-1)/sizeof(atype)]; \
	StaticArrayAuto< Type > name(name##_storage,sizeof(name##_storage)); \
	name.Realloc(name##_size);

#define AUTO_STATIC_ARRAY_1(Type,name,size) \
	AUTO_STATIC_ARRAY_ALIGNED(Type,name,size,char)

#define AUTO_STATIC_ARRAY_2(Type,name,size) \
	AUTO_STATIC_ARRAY_ALIGNED(Type,name,size,short)

#define AUTO_STATIC_ARRAY_4(Type,name,size) \
	AUTO_STATIC_ARRAY_ALIGNED(Type,name,size,int)

#define AUTO_STATIC_ARRAY_8(Type,name,size) \
	AUTO_STATIC_ARRAY_ALIGNED(Type,name,size,double)

#ifdef _KNI
#define AUTO_STATIC_ARRAY_16(Type,name,size) \
	AUTO_STATIC_ARRAY_ALIGNED(Type,name,size,__m128)

#define AUTO_STATIC_ARRAY AUTO_STATIC_ARRAY_16
#else
#define AUTO_STATIC_ARRAY AUTO_STATIC_ARRAY_8
#endif


#define AUTO_FIND_ARRAY_ALIGNED(Type,name,size,atype) \
	const int name##_minGrow = FindArray<Type,MemAllocSA>::MinGrow; \
	const int name##_size=size>name##_minGrow ? size : name##_minGrow; \
	atype name##_storage[(name##_size*sizeof(Type)+sizeof(atype)-1)/sizeof(atype)]; \
	MemAllocSA name##_alloc; \
	name##_alloc.Init(name##_storage,sizeof(name##_storage)); \
	FindArray<Type,MemAllocSA> name; \
	name.SetStorage(name##_alloc); \
	name.Realloc(name##_size);

#define AUTO_FIND_ARRAY_1(Type,name,size) \
	AUTO_FIND_ARRAY_ALIGNED(Type,name,size,char)

#define AUTO_FIND_ARRAY_2(Type,name,size) \
	AUTO_FIND_ARRAY_ALIGNED(Type,name,size,short)

#define AUTO_FIND_ARRAY_4(Type,name,size) \
	AUTO_FIND_ARRAY_ALIGNED(Type,name,size,int)

#define AUTO_FIND_ARRAY_8(Type,name,size) \
	AUTO_FIND_ARRAY_ALIGNED(Type,name,size,double)

#ifdef _KNI
#define AUTO_FIND_ARRAY_16(Type,name,size) \
	AUTO_FIND_ARRAY_ALIGNED(Type,name,size,__m128)

#define AUTO_FIND_ARRAY AUTO_FIND_ARRAY_16
#else
#define AUTO_FIND_ARRAY AUTO_FIND_ARRAY_8
#endif


#endif
