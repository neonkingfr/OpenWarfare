#ifdef _MSC_VER
#pragma once
#endif

#ifndef __SUMA_CLASS_SORTED_ARRAY_HPP
#define __SUMA_CLASS_SORTED_ARRAY_HPP

#include <Containers/array.hpp>
#include <Algorithms/qsort.hpp>
#include <Algorithms/bSearch.hpp>

template <class Type>
struct SortedArrayTraits
{
	typedef const Type &KeyType;

	//! get key
	static KeyType GetKey(const Type &type) {return type;}
	//! compare two key values
	static int Compare(KeyType a, KeyType b)
	{
		if (a>b) return +1;
		if (a<b) return -1;
		return 0;
	}
};

template <class Type,class Allocator=MemAllocD,class Traits=SortedArrayTraits<Type> >
class SortedArray: private AutoArray<Type,Allocator>
{
	typedef typename Traits::KeyType KeyType;
	typedef typename AutoArray<Type,Allocator> base;

	public:
	SortedArray();

  //! Add item without sorting.
	//! Sort is required before any function that require searching may be used.
	//! (Add, AddUnique, Find, FindKey, Delete, DeleteKey)
	void AddDirty(const Type &src) {base::Add(src);}

  //! Adding of an item with resizing the field.
	void Add(const Type &src);
	//! add item if item with the same key does not exist
	void AddUnique(const Type &src)
	{
		if( Find(src)>=0 ) return false;
		Add(src);
		return true;
	}
	//! replace value with another value with the same key
	void Replace(int index, const Type &src)
	{
		Assert(Traits::Compare(Traits::GetKey(Get(index)),Traits::GetKey(src)));
		_data[index] = src;
	}
	//! find if item already exists
	int Find( const Type &src ) const
	{
		return FindKey(Traits::GetKey(src));
	}
	//! find if key already exists
	int FindKey(KeyType key) const;

	//! Delete given element
	bool Delete(const Type &src)
	{
		return DeleteKey(Traits::GetKey(src));
	}
	//! Delete given element
	bool DeleteKey(KeyType src);

	//! allow access to selected functions from base class interface
	__forceinline const Type *Data() const {return base::Data();}
	__forceinline int Size() const {return base::Size();}
	__forceinline const Type &operator [] (int i) const {return base::Get(i);}
	__forceinline void Compact() {base::Compact();}
	__forceinline void Clear() {base::Clear();}
	__forceinline void Realloc(int sizeNeed, int sizeWant)
	{
		base::Realloc(sizeNeed,sizeWant);
	}
	__forceinline void Realloc(int size){base::Realloc(size);}
	__forceinline void Reserve(int sizeNeed, int sizeWant)
	{
		base::Reserve(sizeNeed,sizeWant);
	}

	//! sort array after adding elements with AddDirty
	void Sort();

	ClassIsMovable(SortedArray);
};

template<class Type,class Allocator, class Traits>
SortedArray<Type,Allocator,Traits>::SortedArray()
{
}

template<class Type,class Allocator, class Traits>
bool SortedArray<Type,Allocator,Traits>::DeleteKey(KeyType key)
{
	int index=FindKey(key);
	if( index<0 ) return false;
	base::Delete(index);
	return true;
}

//! functor - to be used as QSort parameter
template <class Type,class Traits>
struct SortedArrayCompare
{
	int operator () (const Type *a, const Type *b, void *)
	{
		// use Traits compare
		return Traits::Compare(Traits::GetKey(*a),Traits::GetKey(*b));
	}
};

template <class Type, class Traits=SortedArrayTraits<Type> >
struct CompareWithKey
{
	int operator () (Traits::KeyType b, const Type &a)
	{
		return Traits::Compare(b,Traits::GetKey(a));
	}
};

template<class Type,class Allocator, class Traits>
void SortedArray<Type,Allocator,Traits>::Add( const Type &src )
{
	KeyType key = Traits::GetKey(src);
	int index = BSearchPos
	(
		Data(),Size(),CompareWithKey<Type,Traits>(),key
	);
	// verify found position is correct
	#if _DEBUG
		Assert(index<=Size());
		Assert(index>=0);
		if (index==Size())
		{
			Assert(Size()==0 || Traits::Compare(key,Traits::GetKey(Get(Size()-1)))>0);
		}
		else
		{
			Assert(Traits::Compare(key,Traits::GetKey(Get(index)))<=0);
			Assert(index<=0 || Traits::Compare(key,Traits::GetKey(Get(index-1)))>=0);
		}
	#endif
	base::Insert(index,src);
}

template<class Type,class Allocator, class Traits>
void SortedArray<Type,Allocator,Traits>::Sort()
{
	// perform quick sort on whole array
	SortedArrayCompare<Type,Traits> compare;
	QSortWithContext
	(
		base::Data(),Size(),(void *)NULL,
		compare
	);
}

template<class Type,class Allocator, class Traits>
int SortedArray<Type,Allocator,Traits>::FindKey(KeyType key) const
{
	return BSearch
	(
		Data(),Size(),CompareWithKey<Type,Traits>(),key
	);
	return -1;
}

#endif
