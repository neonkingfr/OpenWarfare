#ifdef _MSC_VER
#pragma once
#endif

#ifndef __ARRAY_2D_HPP
#define __ARRAY_2D_HPP

//! two-dimensional dynamic array
/*!
AutoArray is used as underlying type
2D access is done on top of ir
*/

template
<
	class Type,class Allocator=MemAllocD,
	class Array1D=AutoArray<Type,Allocator>
>
class Array2D
{
	//! actual data storage
	Array1D _data;
	//! array dimensions
	int _xRng;
	//! array dimensions
	int _yRng;

	public:
	Array2D();
	~Array2D();

	//! resize array
	void Dim(int x, int y);
	//! get array size
	int GetXRange() const {return _xRng;}
	//! get array size
	int GetYRange() const {return _yRng;}

	//! clear array
	void Clear()
	{
		_xRng=_yRng=0;
		_data.Clear();
	}
	//! read and write access
	__forceinline Type &Set(int x, int y)
	{
		return _data[y*_xRng+x];
	}
	//! read only access
	__forceinline const Type &Get(int x, int y) const
	{
		return _data[y*_xRng+x];
	}
	//! read and write access
	__forceinline Type &operator ()(int x, int y)
	{
		return Set(x,y);
	}
	//! read only access
	__forceinline const Type &operator ()(int x, int y) const
	{
		return Get(x,y);
	}
	const void *RawData() const {return _data.Data();}
	void *RawData() {return _data.Data();}
	int RawSize() const {return _data.Size()*sizeof(Type);}
};

template <class Type,class Allocator, class Array1D>
void Array2D<Type,Allocator,Array1D>::Dim(int x, int y)
{
	_xRng = x;
	_yRng = y;
	_data.Realloc(x*y);
	_data.Resize(x*y);
}

template <class Type,class Allocator, class Array1D>
Array2D<Type,Allocator,Array1D>::Array2D()
{
	_xRng = _yRng = 0;
}

template <class Type,class Allocator, class Array1D>
Array2D<Type,Allocator,Array1D>::~Array2D()
{
	Clear();
}

#endif

