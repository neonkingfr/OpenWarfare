#ifdef _MSC_VER
#pragma once
#endif

#ifndef _RSTRING_HPP
#define _RSTRING_HPP

#include <string.h>
#include <ctype.h>


//class RString;
//class RStringB;

#include <Es/Types/pointers.hpp>
#include <Es/Memory/fastAlloc.hpp>

#include <Es/Containers/compactBuf.hpp>

#include <Es/Memory/normalNew.hpp>

typedef CompactBuffer<char> String;

inline String *CreateString( const char *str, int len )
{
	String *string = String::New(len+1);
	strncpy(string->Data(),str,len);
	string->Data()[len]=0;
	return string;
}

inline String *CreateString( const char *str )
{
	int len = strlen(str)+1;
	return String::Copy(str,len);
}

inline String *CreateString(const char *a, const char *b)
{
	int len = strlen(a)+strlen(b)+1;
	String *string = String::New(len);
	strcpy(string->Data(),a);
	strcat(string->Data(),b);
	return string;
}

//! This class represents a string.
/*!
  Using this class you can represent a string with the smart way. Using this class
  you need not take care of allocating or releasing the space of the string unlike
  the usual way. Moreover in case you are about to represent 2 same strings, it will
  allocate just single space for them and split them easily right before you're
  about to change one of them.
*/
class RString // temporary - general class
{
protected:
  //! Reference to string data.
	Ref<String> _ref;
public:
  //! Default constructor.
	RString(){}
  //! Constructor which will set text according to source.
  /*!
    \param text Reference to memory to get text from.
  */
	RString( const char *text )
	{
		if( text ) _ref=CreateString(text);
	}
  //! Constructor which will set text according to source and given number of chars.
  /*!
    \param text Reference to memory to get text from.
    \param len Number of characters to get.
  */
	RString( const char *text, int len )
	{
		if( text ) _ref=CreateString(text,len);
	}
  //! Constructor which will set text with concatenation of given strings.
  /*!
    \param a The first part of string.
    \param b The second part of string.
  */
	RString( const char *a, const char *b )
	{
		_ref=CreateString(a,b);
	}
  //! This method will return pointer to data of the string.
  /*!
    Characters are finished with (0x00) char as usually. Note that method will return
    valid pointer to data even in case the data have not been set yet. So it will
    never return NULL pointer.
    \return Pointer to chars.
  */
	const char *Data() const
	{
		if( _ref ) return _ref->Data();
		else return ""; // always return valid string
	}
  //! This method will return pointer to string which will represent the key of this string.
  /*!
    This method serves for hashing class implementation. For simplicity we return
    just the represented string itself.
    \sa MapStringToClass
    \return Key value.
  */
	__forceinline const char *GetKey() const {return Data();}

  //! This method makes this string independent on another instances pointing to
  //! the same place and consequently returns pointer to data of independent string.
  /*!
    In case pointer is uninitialized this method returns NULL.
    \return Pointer to chars.
  */
	char *MutableData()
	{
		if( !_ref ) return NULL;
		MakeMutable();
		return _ref->Data();
	}
  //! Using this operator you can retrieve directly pointer to characters of the string.
	__forceinline operator const char *() const {return Data();}
  //! Returns length of the represented string.
  /*!
    The last char (0x00) is not involved.
    \return The length of string in characters.
  */
	int GetLength() const {return _ref ? strlen(_ref->Data()) : 0;}
  //! Returns number of references to this string.
  /*!
    \return Number of references.
  */
	int GetRefCount() const {return _ref->RefCounter();}
  //! This operator is suitable for reading single chars of the string.
  /*!
    The first character has the index 0.
    \return Single character from specified position.
  */
	char operator [] ( int i ) const {return Data()[i];}

  //! This method makes this string independent on another instances pointing to
  //! the same place.
  /*!
    You can change the string right after calling this method. In case there are
    more references pointing to this string it will simply create another copy
    of the string and redirect the pointer to it.
  */
	void MakeMutable()
	{
		if( _ref && _ref->RefCounter()>1 )
		{
			// it is shared - we must split it
			_ref=CreateString(_ref->Data());
		}
	}
  //! Converts this string to lowercase.
  /*!
    Before converting it will of course call MakeMutable() method  to not to affect
    another instances.
  */
	void Lower()
	{
		MakeMutable();
		if( _ref ) strlwr(_ref->Data());
	}
  //! Converts this string to uppercase.
  /*!
    Before converting it will of course call MakeMutable() method  to not to affect
    another instances.
  */
	void Upper()
	{
		MakeMutable();
		if( _ref ) strupr(_ref->Data());
	}
  //! Returns substring of this string.
  /*!
    \param from Index of first character to return.
    \param to Index of the character after last character to return.
  */
	RString Substring( int from, int to ) const
	{
		int len=GetLength();
		if( from>len ) from=len;
		if( to>len ) to=len;
		// special case: return whole string (avoid creating a new copy)
		if (from==0 && to==len) return *this;
		// return substring
		return RString(Data()+from,to-from);
	}

	// TODO: disable functions by making them private
	//private:
	//! RString comparison is not possible - use RStringI, RStringS or strcmp, strcmpi instead
	bool operator == (const RString &with) const
	{
		return strcmp(*this,with)==0;
	}
	//! RString comparison is not possible - use RStringI, RStringS or strcmp, strcmpi instead
	bool operator != (const RString &with) const
	{
		return strcmp(*this,with)!=0;
	}
};

TypeIsMovableZeroed(RString);

//! RString with added comparison
template <class Compare>
class RStringT: public RString // temporary - general class
{
public:
  //! Default constructor.
	RStringT(){}
  //! Constructor which will set text according to source.
  /*!
    \param text Reference to memory to get text from.
  */
	RStringT(const RString &text)
	:RString(text)
	{
	}
	RStringT( const char *text )
	:RString(text)
	{
	}
  //! Constructor which will set text according to source and given number of chars.
  /*!
    \param text Reference to memory to get text from.
    \param len Number of characters to get.
  */
	RStringT( const char *text, int len )
	:RString(text,len)
	{
	}
  //! Constructor which will set text with concatenation of given strings.
  /*!
    \param a The first part of string.
    \param b The second part of string.
  */
	RStringT( const char *a, const char *b )
	:RString(a,b)
	{
	}
	//! string comparison
	bool operator == (const RStringT &with) const
	{
		Compare cmp;
		return cmp(*this,with)==0;
	}
	//! string comparison
	bool operator != (const RStringT &with) const
	{
		Compare cmp;
		return cmp(*this,with)!=0;
	}

	private:
	void Lower();
	void Upper();
public:
  ClassIsMovableZeroed(RStringT)
};

//! functor - compare string, case sensitive
/*!
Suitable as template argument for RStringT
*/

class CmpStrCS
{
	public:
	int operator () (const char *a, const char *b)
	{
		return strcmp(a,b);
	}
};

//! functor - compare string, case insensitive
/*!
Suitable as template argument for RStringT
*/
class CmpStrCI
{
	public:
	int operator () (const char *a, const char *b)
	{
		return strcmpi(a,b);
	}
};

//! case sensitive string
typedef RStringT<CmpStrCS> RStringS;
//! case insensitive string
typedef RStringT<CmpStrCI> RStringI;

#include <Es/Memory/debugNew.hpp>

#include <Es/Containers/hashMap.hpp>

template <>
struct MapClassTraits<RStringI>
{
	//! key type
	typedef const char *KeyType;
	//! calculate hash value
	static unsigned int CalculateHashValue(KeyType key)
	{
		unsigned int nHash = 0;
		while (*key)
			nHash = nHash*32 + nHash + tolower(*key++);
		return nHash;
	}

	//! compare keys, return negative when k1<k2, positive when k1>k2, zero when equal
	static int CmpKey(KeyType k1, KeyType k2)
	{
		return strcmpi(k1,k2);
	}

  static KeyType GetKey(const RStringI &item) {return item.GetKey();}
};

// string pool implementation
template <class RStringType>
class RStringBankT
{
	// TODO: keep strings sorted
	// maintain index table (at least for all first letters (256 entries)
	typedef MapStringToClass<RStringType, AutoArray<RStringType> > RStringBankType;

	RStringBankType _bank;

	// note: items are reference countet
	// if any item with reference count 1 is detected, if must be removed

	public:
	RStringBankT();
	~RStringBankT();
	void Clear();

	static bool IsNull(const RStringType &value) {return RStringBankType::IsNull(value);}
	static bool NotNull(const RStringType &value) {return RStringBankType::NotNull(value);}
	static RStringType &Null() {return RStringBankType::Null();}

	const RStringType &Get(const char *t);
	const RStringType &Add(const char *t);

	void Unregister(const char *str);

	void Compact();
};


// Banked string
template <class RStringType, RStringBankT<RStringType> *bank>
class RStringBT: public RStringType // banked string
{
	typedef RStringBankT<RStringType> RStringBankType;

	void Free();
	void Copy( const RStringBT &src ) {_ref=src._ref;}

	public:
	RStringBT() {}
	RStringBT( RString str );
	RStringBT( RStringType str );
	RStringBT( const char *str );

	RStringBT( const RStringBT &src ){Copy(src);}
	void operator =( const RStringBT &src )
	{
		RStringBT temp=src;
		Free();
		Copy(temp);
	}
	~RStringBT(){Free();}

	bool operator == ( const RStringBT &with ) const {return _ref==with._ref;}
	bool operator != ( const RStringBT &with ) const {return _ref!=with._ref;}

	ClassIsMovableZeroed(RStringBT);
};


#if 1
typedef RStringBankT<RStringS> RStringBank;
extern RStringBank StringBank; // global string bank
typedef RStringBT<RStringS,&StringBank> RStringB;
extern RStringB RStringBEmpty;
#endif

#if 1
typedef RStringBankT<RStringI> RStringBankI;
extern RStringBankI StringBankI; // global string bank
typedef RStringBT<RStringI,&StringBankI> RStringIB;
extern RStringIB RStringIBEmpty;
#endif


template <class RStringType, RStringBankT<RStringType> *bank>
RStringBT<RStringType,bank>::RStringBT( const char *str )
{
	// search for string
	if( !str ) return;
	const RStringType &rstr = bank->Get(str);
	if (bank->NotNull(rstr))
	{
		RStringType::operator =(rstr);
	}
	else
	{
		const RStringType &rstr = bank->Add(str);
		Assert(bank->NotNull(rstr));
		RStringType::operator =(rstr);
	}
}

template <class RStringType, RStringBankT<RStringType> *bank>
RStringBT<RStringType,bank>::RStringBT( RString str )
{
	// search for string
	if( !str ) return;
	const RStringType &rstr = bank->Get(str);
	if (bank->NotNull(rstr))
	{
		RStringType::operator =(rstr);
	}
	else
	{
		const RStringType &rstr = bank->Add(str);
		Assert(bank->NotNull(rstr));
		RStringType::operator =(rstr);
	}
}

template <class RStringType, RStringBankT<RStringType> *bank>
RStringBT<RStringType,bank>::RStringBT( RStringType str )
{
	// search for string
	if( !str ) return;
	const RStringType &rstr = bank->Get(str);
	if (RStringBank::NotNull(rstr))
	{
		RStringType::operator =(rstr);
	}
	else
	{
		const RStringType &rstr = bank->Add(str);
		Assert(RStringBank::NotNull(rstr));
		RStringType::operator =(rstr);
	}
}


template <class RStringType, RStringBankT<RStringType> *bank>
void RStringBT<RStringType,bank>::Free()
{
	if( _ref && _ref->RefCounter()==2 )
	{
		bank->Unregister(*this);
	}
	_ref.Free();
}

template <class RStringType, class RStringBankType>
void CheckRefsWeak(RStringType &str, RStringBankType *bank, void *context )
{
	if (str.GetRefCount() > 1)
	{
		LogF
		(
			"Error: String %s late release (%d)",
			(const char *)str, str.GetRefCount()
		);
		// do not delete, bank will be cleared
	}
}

template <class RStringType, class RStringBankType>
void CheckRefsStrong(RStringType &str, RStringBankType *bank, void *context )
{
	if (str.GetRefCount() > 0)
	{
		LogF
		(
			"Warning: String %s late release (%d)",
			(const char *)str, str.GetRefCount()
		);
		// do not delete, bank will be cleared
	}
}

template <class RStringType>
RStringBankT<RStringType>::RStringBankT()
{
}

template <class RStringType>
RStringBankT<RStringType>::~RStringBankT()
{
	LogF("RStringBank destruct");
	_bank.ForEach(CheckRefsWeak);
	_bank.Clear();
}

template <class RStringType>
void RStringBankT<RStringType>::Clear()
{
	LogF("RStringBank clear");
	_bank.ForEach(CheckRefsWeak);
	_bank.Clear();
}

template <class RStringType>
const RStringType &RStringBankT<RStringType>::Add( const char *t )
{
	// we are sure string is not in bank
	const RStringType &str = _bank[t];
	if (NotNull(str)) return str;
	RStringType newstr(t);
	_bank.Add(newstr);
	return _bank[newstr];
}

template <class RStringType, class RStringBankType>
static void DeleteUnused(RStringType &str, RStringBankType *bank, void *context )
{
	if (str.GetRefCount() == 1)
	{
		LogF("String %s not released",(const char *)str);
		bank->Remove(str);
		// ForEach will restart (this array)
	}
}

template <class RStringType>
void RStringBankT<RStringType>::Compact()
{
	_bank.ForEach(DeleteUnused);
}


#define RStringBVal const RStringB &
#define RStringIBVal const RStringIB &


// define static RStringB
#define DEF_RSB(x) static RStringB str_##x(#x);
#define RSB(x) str_##x

#define DEF_RSBI(x) static RStringIB stri_##x(#x);
#define RSBI(x) stri_##x

// basic operators
#if 1
inline RString operator + ( const RString &a, const RString &b ) {return RString(a,b);}
#endif

template <class RStringType>
inline const RStringType &RStringBankT<RStringType>::Get(const char *t)
{
	return _bank[t];
}
template <class RStringType>
inline void RStringBankT<RStringType>::Unregister( const char *str)
{
	_bank.Remove(str);
}


#endif
