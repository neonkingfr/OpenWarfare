#ifdef _MSC_VER
#pragma once
#endif

#ifndef _BSTRING_HPP
#define _BSTRING_HPP

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <Es/Framework/debugLog.hpp>


__forceinline void strcpyLtd(char *dst, const char *src, int size)
{
	strncpy(dst,src,size-1);
}

__forceinline void strcatLtd(char *dst, const char *src, int size)
{
	while (size>0 && *dst)
	{
		dst++;
		size--;
	}
	strcpyLtd(dst,src,size);
}

//! template for automatical character buffer limitation handling
template <int Size>
class BString
{
	char _data[Size];
	
	//! safe copy from raw string
	void Copy(const char *src)
	{
		strcpyLtd(_data,src,sizeof(_data));
	}

	public:
	BString()
	{
		_data[0]=0;
		// note: last character will always stay zero
		// if not, overflow occured
		_data[Size-1]=0;
	}
	~BString()
	{
		Assert(IsValid());
	}

	//! check is string is still valid (last char must be zero)
	bool IsValid() const
	{
		if (_data[sizeof(_data)-1])
		{
			Fail("BString overflow.");
			return false;
		}
		return true;
	}

	//! conversion to const char *
	operator const char *() const {return _data;}
	//! conversion to const char *
	const char *cstr() const {return _data;}

	//! conversion from const char *
	BString(const char *src)
	{
		Copy(src);
	}
	//! conversion from const char *
	const BString &operator = (const char *src)
	{
		Copy(src);
		return *this;
	}

	//! array-like access
	char &operator [] (int i)
	{
		Assert(i>=0);
		Assert(i<sizeof(_data));
		return _data[i];
	}
	//! array-like access
	const char &operator [] (int i) const
	{
		Assert(i>=0);
		Assert(i<sizeof(_data));
		return _data[i];
	}

	//! simple case assignment: no overflow guaranteed
	const BString &operator = (const BString &src)
	{
		strcpy(_data,src._data);
		return *this;
	}
	//! simple case assignment: no overflow guaranteed
	BString(const BString &src)
	{
		strcpy(_data,src._data);
	}
	//! string concatentation
	const BString &operator += (const char *src)
	{
		strcatLtd(_data,src,sizeof(_data));
		return *this;
	}
	//! sprintf replacement
	int PrintF(const char *format, ...)
	{
		va_list va;
		va_start(va,format);
		int ret = vsnprintf(_data,sizeof(_data)-1,format,va);
		va_end(va);
		return ret;
	}
	//! vsprintf replacement
	int VPrintF(const char *format, va_list va)
	{
		return vsnprintf(_data,sizeof(_data)-1,format,va);
	}
	//! strncpy replacement
	const BString &StrNCpy(const char *src, int n)
	{
		if (n<sizeof(_data)-1)
		{
			strncpy(_data,src,n);
		}
		else
		{
			strcpyLtd(_data,src,sizeof(_data));
		}
		return *this;
	}
	BString &StrLwr()
	{
		strlwr(_data);
		return *this;
	}
	BString &StrUpr()
	{
		strupr(_data);
		return *this;
	}

};

//! overloads that make conversion of C code easier 
template <int Size>
inline const BString<Size> & strcpy(BString<Size> &dst, const char *src)
{
	return dst = src;
}

//! overloads that make conversion of C code easier 
template <int Size>
inline const BString<Size> & strncpy(BString<Size> &dst, const char *src, int n)
{
	dst.StrNCpy(src,n);
	return dst;
}

//! overloads that make conversion of C code easier 
template <int Size>
inline const BString<Size> & strcat(BString<Size> &dst, const char *src)
{
	return dst += src;
}

//! overloads that make conversion of C code easier 
template <int Size>
inline BString<Size> & strlwr(BString<Size> &dst)
{
	return dst.StrLwr();
}

//! overloads that make conversion of C code easier 
template <int Size>
inline BString<Size> & strupr(BString<Size> &dst)
{
	return dst.StrUpr();
}

//! overloads that make conversion of C code easier 
#ifdef _WIN32
template <int Size>
inline int sprintf(BString<Size> &dst, const char *format, ...)
#else
template <int Size>
int sprintf(BString<Size> &dst, const char *format, ...)
#endif
{
	va_list va;
	va_start(va,format);
	int ret = dst.VPrintF(format,va);
	va_end(va);
	return ret;
}

#ifdef _WIN32
template <int Size>
inline int vsprintf(BString<Size> &dst, const char *format, va_list argptr)
#else
template <int Size>
int vsprintf(BString<Size> &dst, const char *format, va_list argptr)
#endif
{
	return dst.VPrintF(format,argptr);
}

#endif
