// should be included in source file with #pragma init_seg(lib)
// to guarantee initialization before all user variables

#pragma warning(disable:4073)
#pragma init_seg(lib)

#include <Es/essencepch.hpp>
#include <Es/Strings/rString.hpp>

#if 1
RStringBank StringBank INIT_PRIORITY_HIGH; // global string bank
RStringB RStringBEmpty INIT_PRIORITY_HIGH ="";
#endif

#if 1
RStringBankI StringBankI INIT_PRIORITY_HIGH; // global string bank
RStringIB RStringIBEmpty INIT_PRIORITY_HIGH ="";
#endif
