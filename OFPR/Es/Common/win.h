#ifndef _EL_WIN_H
#define _EL_WIN_H

#ifdef _XBOX

	// note: Verify is function name in XBox Direct3D, but we use it as macro
  #undef Verify
  #include <xtl.h>
  #ifdef NDEBUG
    #define Verify( expr ) (expr)
  #else
    #define Verify( expr ) DoAssert(expr)
  #endif

#elif defined(_WIN32)

	#if defined _WINDOWS_ && !defined _MFC_VER
        #ifndef NET_TEST
		    #error Windows used in non-MFC application.
        #endif
	#endif
  #include <windows.h>
#else
  #undef Verify
  #define Verify( expr ) (expr)
#endif

#undef SearchPath
// #undef LoadString
#undef DrawText
#undef GetObject

#endif
