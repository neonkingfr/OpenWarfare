// declare vertex shader constant mapping
// transform matrix
// world-view-projection matrix (clip space)
#define TRANS_MAT_0 0
#define TRANS_MAT_1 1
#define TRANS_MAT_2 2
#define TRANS_MAT_3 3


#define LIGHT_SUN_DIRECTION   4
#define LIGHT_SUN_DIFFUSE     5
#define LIGHT_SUN_AMBIENT     6

// constant: should be 32,?,?,0
#define CONST_32_05_1_0 7

// world-view matrix (camera space)
#define CAMERA_MAT_0 8
#define CAMERA_MAT_1 9
#define CAMERA_MAT_2 10
#define CAMERA_MAT_3 11

// beg, end -> linear?
#define FOG_PARAM    12
#define GRASS_PARAM 13
// vertex inputs

#define vPosIn v0
#define vNormIn v3
#define vTexIn v7
