// implementation of performance monitor + logging

#include "wpch.hpp"
#include <El/Common/perfLog.hpp>

#if _ENABLE_PERFLOG

#include <Es/Common/win.h>
#include "keyInput.hpp"
#include "dikCodes.h"
#include <Es/Strings/bstring.hpp>

void PerfCounters::Enable( bool value )
{
	if( !_enabled && value )
	{
		Reset();
		Reinit();
	}
	_enabled=value;
	if( !_enabled ) _first=true;
}

struct RegCounter
{
	const char *name;
	const char *sname;
	int lastValue;
	int scale;

	RegCounter( const char *n, const char *s, int c )
	:name(n),sname(s),lastValue(0),scale(c)
	{}
};

static RegCounter CounterNames[]=
{
	//RegCounter("VFAT\\BReadsSec","vfatR",1024),
	//RegCounter("VFAT\\BWritesSec","vfatW",1024),
	RegCounter("VMM\\cPageIns","vmmPI",1),
	//RegCounter("VMM\\cPageOuts","vmmPO",1),
	//RegCounter("VMM\\cPageFaults","vmmPF",1),
};

//#include "psapi.h"
// search for "psapi"
// Structure for GetProcessMemoryInfo()

typedef struct _PROCESS_MEMORY_COUNTERS {
    DWORD cb;
    DWORD PageFaultCount;
    SIZE_T PeakWorkingSetSize;
    SIZE_T WorkingSetSize;
    SIZE_T QuotaPeakPagedPoolUsage;
    SIZE_T QuotaPagedPoolUsage;
    SIZE_T QuotaPeakNonPagedPoolUsage;
    SIZE_T QuotaNonPagedPoolUsage;
    SIZE_T PagefileUsage;
    SIZE_T PeakPagefileUsage;
} PROCESS_MEMORY_COUNTERS;
typedef PROCESS_MEMORY_COUNTERS *PPROCESS_MEMORY_COUNTERS;

#ifdef _WIN32

typedef BOOL WINAPI GetProcessMemoryInfoF
(
  HANDLE Process,
  PPROCESS_MEMORY_COUNTERS ppsmemCounters,
  DWORD cb
);

static GetProcessMemoryInfoF *GetProcessMemoryInfo;

#endif

SIZE_T GetMemoryUsedSize()
{
#ifdef _WIN32
	if (!GetProcessMemoryInfo) return 0;
	PROCESS_MEMORY_COUNTERS pc;
	pc.cb = sizeof(pc);
	if (GetProcessMemoryInfo(GetCurrentProcess(),&pc,sizeof(pc)))
	{
		return pc.WorkingSetSize;
	}
#endif
	return 0;
}

SIZE_T GetMemoryCommitedSize()
{
#ifdef _WIN32
	if (!GetProcessMemoryInfo) return 0;
	PROCESS_MEMORY_COUNTERS pc;
	pc.cb = sizeof(pc);
	if (GetProcessMemoryInfo(GetCurrentProcess(),&pc,sizeof(pc)))
	{
		return pc.PagefileUsage;
	}
#endif
	return 0;
}

static struct InitPSAPI
{
	InitPSAPI()
	{
		static bool once = true;
		if (!once) return;
		once = false;
		#if defined _WIN32 && !defined _XBOX
		HINSTANCE module = LoadLibrary("psapi.dll");
		if (!module) return;
		void *pa = GetProcAddress(module,"GetProcessMemoryInfo");
		if (pa)
		{
			GetProcessMemoryInfo = (GetProcessMemoryInfoF	*)pa;
		}
		#endif
	}
} SInitPSAPI; 

void PerfCounters::SavePCHeaders()
{
	_logFile <<"  wset";
	//_logFile <<"  mcom";


	#if defined _WIN32 && !defined _XBOX
	HKEY key;
	if
	(
		::RegOpenKeyEx(HKEY_DYN_DATA, "PerfStats\\StatData", 0, KEY_READ, &key) == ERROR_SUCCESS
	)
	{
		for( int i=0; i<sizeof(CounterNames)/sizeof(*CounterNames); i++ )
		{
			RegCounter &ctr=CounterNames[i];
			_logFile << " " << ctr.sname;
		}
		::RegCloseKey(key);
	}
	#endif
}

void PerfCounters::SavePCValues()
{
	_logFile << (int)(GetMemoryUsedSize()/(1024*1024));
	//_logFile << (int)(GetMemoryCommitedSize()/(1024*1024));
	// note: works only for Win9x
	// TODO: WinNT performance counters, see HKEY_PERFORMANCE_DATA
	#if defined _WIN32 && !defined _XBOX
	HKEY key;
	if
	(
		::RegOpenKeyEx(HKEY_DYN_DATA, "PerfStats\\StatData", 0, KEY_READ, &key) == ERROR_SUCCESS
	)
	{
		for( int i=0; i<sizeof(CounterNames)/sizeof(*CounterNames); i++ )
		{
			RegCounter &ctr=CounterNames[i];
			DWORD value,size=sizeof(value);
			if( ::RegQueryValueEx(key, ctr.name, NULL, NULL, (BYTE *)&value, &size) !=  ERROR_SUCCESS )
			{
				value=0;
			}

			if( ctr.lastValue==0 ) ctr.lastValue = value;
			_logFile << (int)( value - ctr.lastValue )/ctr.scale;
			ctr.lastValue = value;
		}
		::RegCloseKey(key);
	}
	#endif
}


void PerfCounters::Save( float fps )
{
	if( !_enabled ) return;
	if( _skip>0 )
	{
		--_skip;
		return;
	}
	// check if save is enabled
	// repeat headers
	if( _lines++>=30 ) _first=true;
	// auto-enable counters
	for( int i=0; i<_counters.Size(); i++ )
	{
		PerfCounterSlot &slot=_counters[i];
		if( slot.disabled ) continue;
		if( !slot.enabled )
		{
			if( slot.value==0 ) continue;
			slot.enabled=true;
		}
	}
	if( _first )
	{
		_first=false;
		_lines=0;
		_logFile << "   fps";
		SavePCHeaders();
		_logFile << " Alloc";
		_logFile << " NFree";
		//_logFile << " NB100";
		for( int i=0; i<_counters.Size(); i++ )
		{
			const PerfCounterSlot &slot=_counters[i];
			if( slot.disabled || !slot.enabled ) continue;
			char name[256];
			sprintf(name,"%6s",slot.name);
			name[6]=0;
			_logFile<<name;
		}
		_logFile << "\n";
	}
	if( _line )
	{
		_line=false;
		_logFile << "----------------------------------------------------";
		_logFile << "-----------------------------------------\n";
	}
	_logFile << floatMin(fps,999);
	SavePCValues();
	_logFile << (int)MemoryUsed()/(1024*1024);
	_logFile << (int)MemoryFreeBlocks();
	//_logFile << MemoryAllocatedBlocks()/100;
	for( int i=0; i<_counters.Size(); i++ )
	{
		const PerfCounterSlot &slot=_counters[i];
		if( slot.disabled || !slot.enabled ) continue;
		_logFile<<(slot.value/slot.scale);
	}

	_logFile << "\n";
}

PerfCounters GPerfCounters;
PerfCounters GPerfProfilers;

//PerfCounters GPerfCounters("events.spf");
//PerfCounters GPerfProfilers("timing.spf");

void OpenPerfCounters()
{
	GPerfCounters.Open("events.spf");
	GPerfProfilers.Open("timing.spf");
}
void ClosePerfCounters()
{
	GPerfCounters.Close();
	GPerfProfilers.Close();
}

void PerfCounter::operator +=( int value )
{
	if( !_slot ) _slot=_bank->New(_name,_scale);
	if( _slot ) _slot->value+=value;
}
int PerfCounter::GetValue() const
{
	if( _slot ) return _slot->value;
	return 0;
}

PerfCounters::PerfCounters()
{
	_constructed=true;
	_enabled=false;
	_skip=5; // skip some frames - to reach stable environment
	Reinit();
	// some default setting
	DEF_COUNTER(mSize,1024);
	DEF_COUNTER(tHeap,1024);
	DEF_COUNTER(tGRAM,1024);
}

void PerfCounters::Open(const char *name)
{
	_logFile.Open(name);
	_skip=3; // skip some frames - to reach stable environment
}

void PerfCounters::Close()
{
	_logFile.Close();
}

/*
PerfCounters::PerfCounters( const char *name )
:_logFile(name)
{
	_constructed=true;
	_enabled=false;
	_skip=10; // skip some frames - to reach stable environment
	Reinit();
	// some default setting
	DEF_COUNTER(mSize,1024);
	DEF_COUNTER(tHeap,1024);
	DEF_COUNTER(tGRAM,1024);
}
*/

PerfCounters::~PerfCounters()
{
	// make sure all columns have corresponding header
	_first=true;
	Save(0);
}

void PerfCounters::Reinit()
{
	_first=true;
	_line=true;
	_lines=0;
}

void PerfCounters::Reset()
{
	for( int i=0; i<_counters.Size(); i++ )
	{
		_counters[i].Frame();
		_counters[i].value=0;
	}
}

int PerfCounters::N() const {return _counters.Size();}

const char *PerfCounters::Name( int i ) const
{
	return _counters[i].name;
}

int PerfCounters::Value( int i ) const
{
	return _counters[i].value/_counters[i].scale;
}

bool PerfCounters::Show( int i ) const
{
	return !_counters[i].disabled && _counters[i].enabled;
}

bool PerfCounters::WasNonZero( int i, int frames ) const
{
	return _counters[i].lastNonZero<frames;
}


int PerfCounters::Find( const char *name )
{
	for( int i=0; i<_counters.Size(); i++ )
	{
		if( !strcmp(_counters[i].name,name) ) return i;
	}
	return -1;
}

PerfCounterSlot::PerfCounterSlot()
{
	strcpy(name,"");
	value=0;
	scale=1;
	enabled=true;
	disabled=false;
}

PerfCounterSlot *PerfCounters::New( const char *name, int scale )
{
	static PerfCounterSlot dummy;
	if( !_constructed ) return NULL;
	int index=Find(name);
	if( index>=0 ) return &_counters[index];
	index=_counters.Add();
	if( index<0 )
	{
		Fail("No more counters enabled");
		return &dummy;
	}
	PerfCounterSlot &slot=_counters[index];
	strncpy(slot.name,name,sizeof(slot.name)-1);
	slot.name[sizeof(slot.name)-1]=0;
	slot.scale = scale;
	slot.lastNonZero = 10000;
	//LogF("New perf slot %s",slot.name);
	return &slot;
}

void PerfCounters::Enable( const char *name )
{
	int index=Find(name);
	if( index>=0 ) _counters[index].enabled=true;
}

void PerfCounters::SetScale( const char *name, int scale )
{
	int index=Find(name);
	if( index>=0 ) _counters[index].scale=scale;
}

void PerfCounters::Disable( const char *name )
{
	int index=Find(name);
	if( index>=0 ) _counters[index].disabled=true;
}


void LogFile::Open( const char *name )
{
	Close();
	if (!name || !*name) return; // empty name - no logging
	_file=fopen(name,"w");
	if( !_file ) return;
	setvbuf(_file,NULL,_IOFBF,128*1024);
}
void LogFile::Close()
{
	if( !_file ) return;
	fclose(_file);
	_file=NULL;
}

LogFile::LogFile()
{
	_file=NULL;
}

LogFile::LogFile( const char *name )
{
	Open(name);
}

LogFile::~LogFile()
{
	Close();
}


void LogFile::Append( const char *text )
{
	if( !_file ) return;
	fwrite(text,strlen(text),1,_file);
}

void LogFile::PrintF( const char *text, ... )
{
	BString<1024> buf;
	va_list arglist;
	va_start( arglist, text );
	vsprintf( buf, text, arglist );
	va_end( arglist );
	Append(buf);
}

LogFile &LogFile::operator << ( int i )
{
	char buf[80];
	sprintf(buf,"%6d",i);
	Append(buf);
	return *this;
}


LogFile &LogFile::operator << ( float f )
{
	char buf[80];
	sprintf(buf,"%6.2f",f);
	Append(buf);
	return *this;
}

LogFile &LogFile::operator << ( const char *txt )
{
	Append(txt);
	return *this;
}



static bool perfFirst=true;
static bool perfLine=true;
//LogFile GLogFile("perf.log");

#endif
