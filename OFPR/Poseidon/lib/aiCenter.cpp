// AI - implementation of AI and AICenter

#include "wpch.hpp"

#include "ai.hpp"
#include "aiDefs.hpp"
#include "global.hpp"
#include "scene.hpp"
//#include "loadStream.hpp"
#include "person.hpp"

#include "landscape.hpp"
#include "world.hpp"
#include "dynSound.hpp"
#include "detector.hpp"
#include "shots.hpp"

//#include "strIncl.hpp"
#include "stringtableExt.hpp"
#include <El/Common/randomGen.hpp>
#include "camEffects.hpp"

#include "languages.hpp"
#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"

#include <Es/Algorithms/qsort.hpp>

#include "arcadeTemplate.hpp"
#include "paramArchive.hpp"

#include "gameStateExt.hpp"

#include "network.hpp"
#include "chat.hpp"

#include <El/Common/enumNames.hpp>

#include "uiActions.hpp"

#include "mbcs.hpp"
#include <Es/Strings/bstring.hpp>
#include "strFormat.hpp"

extern SRef<EntityAI> GDummyVehicle;

///////////////////////////////////////////////////////////////////////////////
// Parameters

// no of updates in AICenter::Think
#define FieldsPerCycle		10
#define GroupsPerCycle		1

#define TargetsPerCycle		8

#define EXPOSURE_COEF			50.0F
#define INV_EXPOSURE_COEF	(1.0F / EXPOSURE_COEF)

const float AccuracyLast=7200;			// how long we can remmember the information
const float ExposureTimeout = 450;	// how long exposure persist when target is not visible

bool IsEnemy(const AICenter *center, TargetSide side)
{
	return center->IsEnemy(side);
}

bool IsUnknown(const AICenter *center, TargetSide side)
{
	return side == TSideUnknown;
}


#define LOG_TARGETS 0

///////////////////////////////////////////////////////////////////////////////
// union AITimeRelative

union AITimeRelative
{
	private:
	WORD _packed;
	struct
	{
		signed _mantisa:7; // singed mantisa,r range -0x3f, +0x3f
		unsigned _exponent:3; // binary exponent divided by 3
		// max. represented value is (0x3f)<<(7*3) = 132120576
		// max. valid AITime value is 365*24*60*60 = 31536000
		unsigned _owner:6;
	};

	public:
	AITimeRelative():_mantisa(0),_exponent(0),_owner(0x3f){}
	explicit AITimeRelative( AITime time, int owner=0 );
	operator AITime() const {return _mantisa<<(_exponent*4);}

	int GetOwner() const {return _owner;}
	void SetOwner( int owner ) {_owner=owner;}
};

AITimeRelative::AITimeRelative( AITime delta, int owner )
{
	_owner=owner;
	// delta can be up to 25 bits (secPerYear<2^25)
	bool negative=delta<0;
	if( negative ) delta=-delta;
	int exponent=0;
	while( delta>=0x40 ) delta>>=4,exponent++;
	Assert( exponent<8 );
	Assert( delta<0x40 );
	if( negative ) delta=-delta;
	_exponent=exponent;
	_mantisa=delta;
}

///////////////////////////////////////////////////////////////////////////////
// class AI

static const EnumName SemaphoreNames[]=
{
	EnumName(-1, "NO CHANGE"),
	EnumName(AI::SemaphoreBlue, "BLUE"),
	EnumName(AI::SemaphoreGreen, "GREEN"),
	EnumName(AI::SemaphoreWhite, "WHITE"),
	EnumName(AI::SemaphoreYellow, "YELLOW"),
	EnumName(AI::SemaphoreRed, "RED"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AI::Semaphore dummy)
{
	return SemaphoreNames;
}

static const EnumName FormationNames[]=
{
	EnumName(-1, "NO CHANGE"),
	EnumName(AI::FormColumn, "COLUMN"),
	EnumName(AI::FormStaggeredColumn, "STAG COLUMN"),
	EnumName(AI::FormWedge, "WEDGE"),
	EnumName(AI::FormEcholonLeft, "ECH LEFT"),
	EnumName(AI::FormEcholonRight, "ECH RIGHT"),
	EnumName(AI::FormVee, "VEE"),
	EnumName(AI::FormLine, "LINE"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AI::Formation dummy)
{
	return FormationNames;
}

static const EnumName RankNames[]=
{
	EnumName(RankUndefined, "UNDEFINED"),
	EnumName(RankPrivate, "PRIVATE"),
	EnumName(RankCorporal, "CORPORAL"),
	EnumName(RankSergeant, "SERGEANT"),
	EnumName(RankLieutnant, "LIEUTNANT"),
	EnumName(RankCaptain, "CAPTAIN"),
	EnumName(RankMajor, "MAJOR"),
	EnumName(RankColonel, "COLONEL"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(Rank dummy)
{
	return RankNames;
}

AITime AI::GetActualTime()
{
	// static member function
	float time=Glob.clock.GetTimeInYear();
	// convert in-day time and in-year time to single int represetation
	const float secPerYear=365*24*60*60;
	return toIntFloor(secPerYear*time);
}

float AI::ExpForRank(Rank rank, bool ingame)
{
	if (rank < RankPrivate)
		return 0;

	if (rank > RankColonel)
		return FLT_MAX;

	if (ingame)
		return 0.8 * ExperienceTable[rank];

	return ExperienceTable[rank];
}

Rank AI::RankFromExp(float exp, bool ingame)
{
	int i;
	for (i=RankPrivate+1; i<=RankColonel; i++)
		if (exp < ExpForRank((Rank)i, ingame))
			return (Rank)(i - 1);
	return RankColonel;
}

AutoArray<float> ExperienceTable;
AutoArray<ExperienceDestroyInfo> ExperienceDestroyTable;

float ExperienceDestroyEnemy;
float ExperienceDestroyFriendly;
float ExperienceDestroyCivilian;

float ExperienceKilled;

float ExperienceRenegadeLimit;
	
// for subordinate soldier only
float ExperienceCommandCompleted;
float ExperienceCommandFailed;
float ExperienceFollowMe;

// for leadership only
float ExperienceDestroyYourUnit; 
float ExperienceMissionCompleted;
float ExperienceMissionFailed;

void AI::InitTables()
{
	const ParamEntry &cls = Pars >> "CfgExperience";
	
	int n = (cls >> "ranks").GetSize(); 
	Assert(n == NRanks);

	ExperienceTable.Resize(n);
	for (int i=0; i<n; i++)
	{
		ExperienceTable[i] = (cls >> "ranks")[i];
	}

	n = (cls >> "destroyUnit").GetSize();
	ExperienceDestroyTable.Resize(n);
	for (int i=0; i<n; i++)
	{
		RString name = (cls >> "destroyUnit")[i];
		ExperienceDestroyTable[i].maxCost = (cls >> name)[0];
		ExperienceDestroyTable[i].exp = (cls >> name)[1];
	}

	ExperienceDestroyEnemy = cls >> "destroyEnemy";
	ExperienceDestroyFriendly = cls >> "destroyFriendly";
	ExperienceDestroyCivilian = cls >> "destroyCivilian";

	ExperienceRenegadeLimit = cls >> "renegadeLimit";

	ExperienceKilled = cls >> "playerKilled";
		
	// for subordinate soldier only
	ExperienceCommandCompleted = cls >> "commandCompleted";
	ExperienceCommandFailed = cls >> "commandFailed";
	ExperienceFollowMe = cls >> "followMe";

	// for leadership only
	ExperienceDestroyYourUnit = cls >> "destroyYourUnit"; 
	ExperienceMissionCompleted = cls >> "missionCompleted";
	ExperienceMissionFailed = cls >> "missionFailed";
}

///////////////////////////////////////////////////////////////////////////////
// AIStats classes

static const EnumName EventTypeNames[]=
{
	EnumName(SETUnitLost, "LOST"),
	EnumName(SETKillsEnemyInfantry, "ENEMY INF"),
	EnumName(SETKillsEnemySoft, "ENEMY SOFT"),
	EnumName(SETKillsEnemyArmor, "ENEMY ARMOR"),
	EnumName(SETKillsEnemyAir, "ENEMY AIR"),
	EnumName(SETKillsFriendlyInfantry, "FRIEND INF"),
	EnumName(SETKillsFriendlySoft, "FRIEND SOFT"),
	EnumName(SETKillsFriendlyArmor, "FRIEND ARMOR"),
	EnumName(SETKillsFriendlyAir, "FRIEND AIR"),
	EnumName(SETKillsCivilInfantry, "CIVIL INF"),
	EnumName(SETKillsCivilSoft, "CIVIL SOFT"),
	EnumName(SETKillsCivilArmor, "CIVIL ARMOR"),
	EnumName(SETKillsCivilAir, "CIVIL AIR"),
	EnumName(SETUser, "USER"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AIStatsEventType dummy)
{
	return EventTypeNames;
}

static const EnumName StatsKillsNames[]=
{
	EnumName(SKEnemyInfantry, "ENEMY INF"),
	EnumName(SKEnemySoft, "ENEMY SOFT"),
	EnumName(SKEnemyArmor, "ENEMY ARMOR"),
	EnumName(SKEnemyAir, "ENEMY AIR"),
	EnumName(SKFriendlyInfantry, "FRIEND INF"),
	EnumName(SKFriendlySoft, "FRIEND SOFT"),
	EnumName(SKFriendlyArmor, "FRIEND ARMOR"),
	EnumName(SKFriendlyAir, "FRIEND AIR"),
	EnumName(SKCivilianInfantry, "CIVIL INF"),
	EnumName(SKCivilianSoft, "CIVIL SOFT"),
	EnumName(SKCivilianArmor, "CIVIL ARMOR"),
	EnumName(SKCivilianAir, "CIVIL AIR"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AIStatsKills dummy)
{
	return StatsKillsNames;
}

AIStatsMPRow::AIStatsMPRow()
{
	order = 0;
	side = TSideUnknown;
	killsInfantry = 0;
	killsSoft = 0;
	killsArmor = 0;
	killsAir = 0;
	killsPlayers = 0;
	customScore = 0;
	killsTotal = 0;
	killed = 0;
}

void AIStatsMPRow::RecalculateTotal()
{
	killsTotal = killsInfantry + 2 * killsSoft + 3 * killsArmor + 5 * killsAir + customScore;
}

AIStatsMission::AIStatsMission()
{
	//Clear();

	_size = 0; 
}

void AIStatsMission::Init()
{
	const ParamEntry &cls = Pars >> "CfgInGameUI" >> "MPTable";
	_color = GetPackedColor(cls >> "color");
	_colorBg = GetPackedColor(cls >> "colorBg");
	_colorSelected = GetPackedColor(cls >> "colorSelected");
	_colorWest = GetPackedColor(cls >> "colorWest");
	_colorEast = GetPackedColor(cls >> "colorEast");
	_colorCiv = GetPackedColor(cls >> "colorCiv");
	_colorRes = GetPackedColor(cls >> "colorRes");
}

const int maxMPTableRows = 10;

/*!
\patch 1.11 Date 07/30/2001 by Jirka
- Fixed: score does not work if squad id entered
\patch_internal 1.11 Date 07/30/2001 by Jirka
- different names used in initialization of table and searching in table
*/
void AIStatsMission::Clear()
{
	_events.Clear();
	_tableMP.Clear();
#if _ENABLE_VBS
	_vbsEvents.Clear();
#endif
	_lives = -1;
	_start = Glob.time;

	// caution: GNetworkManager must be initialized
	#if 1
/*
	AUTO_STATIC_ARRAY(NetPlayerInfo, players, 32)
	GetNetworkManager().GetPlayers(players);
	int n = players.Size();
	if (n > 0)
	{
		_tableMP.Resize(n);
		for (int i=0; i<n; i++)
		{
			_tableMP[i].player = players[i].name;
			_tableMP[i].order = i + 1;
		}
		// ensure local player statistics are visible
		int index = -1;
		for (int i=0; i<_tableMP.Size(); i++)
		{
			if (_tableMP[i].player == GetLocalPlayerName())
			{
				index = i; break;
			}
		}
		if (index >= maxMPTableRows) swap(_tableMP[index], _tableMP[maxMPTableRows - 1]);
	}
*/
	// FIX
	const AutoArray<PlayerIdentity> *identities = GetNetworkManager().GetIdentities();
	if (identities)
	{
		int n = identities->Size();
		if (n > 0)
		{
			_tableMP.Resize(n);
			for (int i=0; i<n; i++)
			{
				_tableMP[i].player = identities->Get(i).name;
				_tableMP[i].order = i + 1;
				int dpnid = identities->Get(i).dpnid;
				for (int j=0; j<GetNetworkManager().NPlayerRoles(); j++)
				{
					const PlayerRole *info = GetNetworkManager().GetPlayerRole(j);
					if (info && info->player == dpnid)
					{
						_tableMP[i].side = info->side;
						break;
					}
				}
			}
			// ensure local player statistics are visible
			int index = -1;
			for (int i=0; i<_tableMP.Size(); i++)
			{
				if (_tableMP[i].player == GetLocalPlayerName())
				{
					index = i; break;
				}
			}
			if (index >= maxMPTableRows) swap(_tableMP[index], _tableMP[maxMPTableRows - 1]);
		}
	}
	#endif
}

void AIStatsMission::AddEvent
(
	AIStatsEventType type, Vector3Par position, RString message,
	const VehicleType *killedType, RString killedName, bool killedPlayer,
	const VehicleType *killerType, RString killerName, bool killerPlayer
)
{
	int index = _events.Add();
	_events[index].type = type;
	_events[index].time = Glob.clock.GetTimeInYear();
	_events[index].position = position;
	_events[index].message = message;

	_events[index].killedType = killedType;
	_events[index].killedName = killedName;
	_events[index].killedPlayer = killedPlayer;
	_events[index].killerType = killerType;
	_events[index].killerName = killerName;
	_events[index].killerPlayer = killerPlayer;
}

Person *GetPlayerPerson(EntityAI *vehicle)
{
	if (!vehicle) return NULL;
	Person *person = dyn_cast<Person>(vehicle);
	if (person) return person;
	Transport *trans = dyn_cast<Transport>(vehicle);
	if (!trans) return NULL; // may be Thing or Building

	// check if player is inside
	Person *commander = trans->Commander();
	if (commander && commander->IsNetworkPlayer()) return commander;
	Person *gunner = trans->Gunner();
	if (gunner && gunner->IsNetworkPlayer()) return gunner;
	Person *driver = trans->Driver();
	if (driver && driver->IsNetworkPlayer()) return driver;

	// check if anybody is inside
	if (commander) return commander;
	if (gunner) return gunner;
	return driver;
}

#if _ENABLE_VBS
/*!
\patch_internal 1.78 Date 7/16/2002 by Jirka
- Added: Multiplayer report for VBS
*/

void AIStatsMission::VBSKilled(EntityAI *killed, EntityAI *killer)
{
	Assert(killed);
	Assert(killer);
	
	Person *killedPerson = GetPlayerPerson(killed);
	if (!killedPerson) return;
	Person *killerPerson = GetPlayerPerson(killer);
	if (!killerPerson) return;

	int index = _vbsEvents.Add();
	VBSStatsEvent &event = _vbsEvents[index];
	
	event.type = VBSETKill;
	event.t = Glob.time;

	// killed
	event.unit1.name = killedPerson->GetInfo()._name;
	event.unit1.type = killed->GetType()->GetName();
	event.unit1.position = killed->Position();
	event.unit1.side = killedPerson->Vehicle::GetTargetSide();
	event.unit1.player = killedPerson->IsNetworkPlayer();

	// killer
	event.unit2.name = killerPerson->GetInfo()._name;
	event.unit2.type = killer->GetType()->GetName();
	event.unit2.position = killer->Position();
	event.unit2.side = killerPerson->Vehicle::GetTargetSide();
	event.unit2.player = killerPerson->IsNetworkPlayer();
}

void AIStatsMission::VBSInjured(EntityAI *injured, EntityAI *killer, float damage, RString ammo)
{
	Assert(injured);
	Person *person = GetPlayerPerson(injured);
	if (!person) return;
	Person *killerPerson = GetPlayerPerson(killer);
	if (!killerPerson) return;

	int index = _vbsEvents.Add();
	VBSStatsEvent &event = _vbsEvents[index];
	
	event.type = VBSETInjury;
	event.t = Glob.time;

	// injured
	event.unit1.name = person->GetInfo()._name;
	event.unit1.type = injured->GetType()->GetName();
	event.unit1.position = injured->Position();
	event.unit1.side = person->Vehicle::GetTargetSide();
	event.unit1.player = person->IsNetworkPlayer();

	// killer
	event.unit2.name = killerPerson->GetInfo()._name;
	event.unit2.type = killer->GetType()->GetName();
	event.unit2.position = killer->Position();
	event.unit2.side = killerPerson->Vehicle::GetTargetSide();
	event.unit2.player = killerPerson->IsNetworkPlayer();

	event.value = damage;
	event.text = ammo;
}

void AIStatsMission::VBSAddHeader(RString text)
{
	_vbsHeader.Add(text);
}

void AIStatsMission::VBSAddEvent(RString text)
{
	int index = _vbsEvents.Add();
	VBSStatsEvent &event = _vbsEvents[index];
	
	event.type = VBSETUser;
	event.t = Glob.time;
	event.text = text;
}

void AIStatsMission::VBSAddFooter(RString text)
{
	_vbsFooter.Add(text);
}

#include <Es/Common/win.h>

void ReportOutput(HANDLE report, const char *text)
{
	DWORD cbWritten;
	WriteFile(report, text, strlen(text), &cbWritten, 0);
	WriteFile(report, "\r\n", 2, &cbWritten, 0);
}

void AIStatsMission::WriteVBSStatistics()
{
	HANDLE report = CreateFile
	(
		"mpreport.txt",
		GENERIC_WRITE,
		0,
		0,
		OPEN_ALWAYS, //CREATE_ALWAYS,
		FILE_FLAG_WRITE_THROUGH,
		0
	);
	if (!report) return;
	
	SetFilePointer(report, 0, 0, FILE_END);

	BString<512> buffer;

	// Header
  ReportOutput(report, "----------**Start**----------");
	sprintf
	(
		buffer, "Mission: %s.%s",
		(const char *)Glob.header.filenameReal, (const char *)Glob.header.worldname
	);
  ReportOutput(report, buffer);

	RString sides, players;
	for (int i=0; i<TSideUnknown; i++)
	{
		int PlayersNumber(TargetSide side);
		int count = PlayersNumber((TargetSide)i);
		if (count == 0) continue;
		
		if (sides.GetLength() > 0) sides = sides + RString("/");
		if (players.GetLength() > 0) players = players + RString("/");

		switch (i)
		{
		case TEast:
			sides = sides + LocalizeString(IDS_EAST); break;
		case TWest:
			sides = sides + LocalizeString(IDS_WEST); break;
		case TGuerrila:
			sides = sides + LocalizeString(IDS_GUERRILA); break;
		case TCivilian:
			sides = sides + LocalizeString(IDS_CIVILIAN); break;
		}
		
		players = players + FormatNumber(count);
	}
	sprintf
	(
		buffer, "Players: %s (%s)",
		(const char *)sides, (const char *)players
	);
  ReportOutput(report, buffer);
	
	struct tm *time = localtime(&_time);
	sprintf
	(
		buffer, "Start mission: %d/%d/%d, %d:%02d:%02d",
		time->tm_year + 1900, time->tm_mon + 1, time->tm_mday,
		time->tm_hour, time->tm_min, time->tm_sec
	);
  ReportOutput(report, buffer);

	for (int i=0; i<_vbsHeader.Size(); i++) ReportOutput(report, _vbsHeader[i]);

	// Events
  ReportOutput(report, "----------**Events**----------");
	int totalKills[TSideUnknown], totalKilled[TSideUnknown];
	for (int i=0; i<TSideUnknown; i++)
	{
		totalKills[i] = 0; totalKilled[i] = 0;
	}
	for (int i=0; i<_vbsEvents.Size(); i++)
	{
		VBSStatsEvent &event = _vbsEvents[i];

		int time = toInt(event.t.toFloat());
		int h = time / 3600;
		time -= 3600 * h;
		int m = time / 60;
		time -= 60 * m;
		int s = time;
		
		switch (event.type)
		{
		case VBSETKill:
			{
				sprintf
				(
					buffer, "%2d:%02d:%02d : %s%s killed by %s%s at [%.0f, %.0f]",
					h, m, s,
					(const char *)event.unit1.name, event.unit1.player ? "" : " (AI)",
					(const char *)event.unit2.name, event.unit2.player ? "" : " (AI)",
					event.unit1.position.X(), event.unit1.position.Z()
				);
				ReportOutput(report, buffer);
				totalKills[event.unit2.side]++;
				totalKilled[event.unit1.side]++;
			}
			break;
		case VBSETInjury:
			{
				BString<256> ammo;
				if (event.text.GetLength() > 0) sprintf(ammo, ", ammo %s", (const char *)event.text);
				sprintf
				(
					buffer, "%2d:%02d:%02d : %s%s injured by %s%s at [%.0f, %.0f], damage %.0f%%%s",
					h, m, s,
					(const char *)event.unit1.name, event.unit1.player ? "" : " (AI)",
					(const char *)event.unit2.name, event.unit2.player ? "" : " (AI)",
					event.unit1.position.X(), event.unit1.position.Z(),
					100.0f * event.value, (const char *)ammo
				);
				ReportOutput(report, buffer);
			}
			break;
		case VBSETUser:
			{
				sprintf
				(
					buffer, "%2d:%02d:%02d : %s",
					h, m, s,
					(const char *)event.text
				);
				ReportOutput(report, buffer);
			}
			break;
		}
	}

	// Footer
  ReportOutput(report, "----------**Summary**----------");
	RString kills, killed;
	for (int i=0; i<TSideUnknown; i++)
	{
		int PlayersNumber(TargetSide side);
		int count = PlayersNumber((TargetSide)i);
		if (count == 0) continue;
		
		if (kills.GetLength() > 0) kills = kills + RString("/");
		if (killed.GetLength() > 0) killed = killed + RString("/");

		kills = kills + FormatNumber(totalKills[i]);
		killed = killed + FormatNumber(totalKilled[i]);
	}
	sprintf
	(
		buffer, "Total amount of deaths: %s (%s)",
		(const char *)sides, (const char *)killed
	);
  ReportOutput(report, buffer);
	sprintf
	(
		buffer, "Total amount of kills: %s (%s)",
		(const char *)sides, (const char *)kills
	);
  ReportOutput(report, buffer);
	for (int i=0; i<_vbsHeader.Size(); i++) ReportOutput(report, _vbsFooter[i]);

  ReportOutput(report, "----------**End**----------");
  ReportOutput(report, "");

	CloseHandle(report);
}

#endif

void AIStatsMission::OnMissionStart()
{
	time(&_time);
}

int CmpMPTableRows(const AIStatsMPRow *row1, const AIStatsMPRow *row2)
{
	int value = row2->killsTotal - row1->killsTotal;
	if (value != 0) return value;
	value = row1->killed - row2->killed;
	if (value != 0) return value;
	return stricmp(row1->player, row2->player);
}

void AIStatsMission::AddMPKill(RString playerName, TargetSide playerSide, const VehicleType *killedType, bool killedPlayer, bool isEnemy)
{
	// 1. find row in table
	int index = -1;
	for (int i=0; i<_tableMP.Size(); i++)
	{
		if (strcmp(_tableMP[i].player, playerName) == 0)
		{
			index = i; break;
		}
	}
	if (index < 0)
	{
//		return;
		index = _tableMP.Add();
		_tableMP[index].player = playerName;
		_tableMP[index].side = playerSide;
	}
	AIStatsMPRow &row = _tableMP[index];

	if (killedType)
	{
		// 2. add killed vehicle
		if (killedType->IsKindOf(GWorld->Preloaded(VTypeStatic))) return;
		if (killedType->IsKindOf(GWorld->Preloaded(VTypeMan)))
		{
			if (isEnemy)
				row.killsInfantry++;
			else
				row.killsInfantry--;
		}
		else switch (killedType->GetKind())
		{
		case VAir:
			if (isEnemy)
				row.killsAir++;
			else
				row.killsAir--;
			break;
		case VArmor:
			if (isEnemy)
				row.killsArmor++;
			else
				row.killsArmor--;
			break;
		case VSoft:
		default:
			if (isEnemy)
				row.killsSoft++;
			else
				row.killsSoft--;
			break;
		}

		if (killedPlayer)
		{
			if (isEnemy)
				row.killsPlayers++;
			else
				row.killsPlayers--;
		}

		row.RecalculateTotal();

		// 3. sort 
		QSort(_tableMP.Data(), _tableMP.Size(), CmpMPTableRows);
		for (int i=0; i<_tableMP.Size(); i++) _tableMP[i].order = i + 1;

		// 4. ensure local player statistics are visible
		index = -1;
		for (int i=0; i<_tableMP.Size(); i++)
		{
			if (_tableMP[i].player == GetLocalPlayerName())
			{
				index = i; break;
			}
		}
		if (index >= maxMPTableRows) swap(_tableMP[index], _tableMP[maxMPTableRows - 1]);
	}
	else
	{
		// 2b. player killed
		row.killed++;
	}
}

void AIStatsMission::AddMPScore(RString playerName, TargetSide playerSide, int score)
{
	// 1. find row in table
	int index = -1;
	for (int i=0; i<_tableMP.Size(); i++)
	{
		if (strcmp(_tableMP[i].player, playerName) == 0)
		{
			index = i; break;
		}
	}
	if (index < 0)
	{
		const MissionHeader *header = GetNetworkManager().GetMissionHeader();
		if (header && header->aiKills)
		{
			index = _tableMP.Add();
			_tableMP[index].player = playerName;
			_tableMP[index].side = playerSide;
		}
		else return;
	}
	AIStatsMPRow &row = _tableMP[index];

	// 2. add score
	row.customScore += score;
	row.RecalculateTotal();

	// 3. sort 
	QSort(_tableMP.Data(), _tableMP.Size(), CmpMPTableRows);
	for (int i=0; i<_tableMP.Size(); i++) _tableMP[i].order = i + 1;

	// 4. ensure local player statistics are visible
	index = -1;
	for (int i=0; i<_tableMP.Size(); i++)
	{
		if (_tableMP[i].player == GetLocalPlayerName())
		{
			index = i; break;
		}
	}
	if (index >= maxMPTableRows) swap(_tableMP[index], _tableMP[maxMPTableRows - 1]);
}

#define CX(x) (toInt((x) * w) + 0.5)
#define CY(y) (toInt((y) * h) + 0.5)

/*!
\patch 1.22 Date 08/27/2001 by Jirka
- Improved: MP Statistics table
*/
void AIStatsMission::DrawMPTable(float alpha)
{
	bool isEast = false, isWest = false, isRes = false, isCiv = false;
	AIStatsMPRow rowEast, rowWest, rowRes, rowCiv;
	rowEast.side = TEast;
	rowWest.side = TWest;
	rowRes.side = TGuerrila;
	rowCiv.side = TCivilian;
	for (int i=0; i<_tableMP.Size(); i++)
	{
		AIStatsMPRow &row = _tableMP[i];
		switch (row.side)
		{
		case TEast:
			isEast = true;
			rowEast.player = LocalizeString(IDS_EAST);
			rowEast.killsInfantry += row.killsInfantry;
			rowEast.killsSoft += row.killsSoft;
			rowEast.killsArmor += row.killsArmor;
			rowEast.killsAir += row.killsAir;
			rowEast.killsPlayers += row.killsPlayers;
			rowEast.killsTotal += row.killsTotal;
			rowEast.killed += row.killed;
			break;
		case TWest:
			isWest = true;
			rowWest.player = LocalizeString(IDS_WEST);
			rowWest.killsInfantry += row.killsInfantry;
			rowWest.killsSoft += row.killsSoft;
			rowWest.killsArmor += row.killsArmor;
			rowWest.killsAir += row.killsAir;
			rowWest.killsPlayers += row.killsPlayers;
			rowWest.killsTotal += row.killsTotal;
			rowWest.killed += row.killed;
			break;
		case TGuerrila:
			isRes = true;
			rowRes.player = LocalizeString(IDS_GUERRILA);
			rowRes.killsInfantry += row.killsInfantry;
			rowRes.killsSoft += row.killsSoft;
			rowRes.killsArmor += row.killsArmor;
			rowRes.killsAir += row.killsAir;
			rowRes.killsPlayers += row.killsPlayers;
			rowRes.killsTotal += row.killsTotal;
			rowRes.killed += row.killed;
			break;
		case TCivilian:
			isCiv = true;
			rowCiv.player = LocalizeString(IDS_CIVILIAN);
			rowCiv.killsInfantry += row.killsInfantry;
			rowCiv.killsSoft += row.killsSoft;
			rowCiv.killsArmor += row.killsArmor;
			rowCiv.killsAir += row.killsAir;
			rowCiv.killsPlayers += row.killsPlayers;
			rowCiv.killsTotal += row.killsTotal;
			rowCiv.killed += row.killed;
			break;
		}
	}
	int nSides = 0;
	if (isEast) nSides++;
	if (isWest) nSides++;
	if (isRes) nSides++;
	if (isCiv) nSides++;

	static Ref<Font> font;
	if (!font)
	{
		font = GEngine->LoadFont(GetFontID("tahomaB48"));
		_size = 0.032;
	}

	const float xb = 0.05;
	const float xe = 0.95;
	const float yb = 0.15;
	const float ye = 0.85;
	const float rowHeight = (ye - yb) / (maxMPTableRows + 5.5);
	const int columns = 9;
	const float colWidths[columns] =
	{
		0.05,		// order
		0.15,		// player
		0.10,		// killsInfantry
		0.10,		// killsSoft
		0.10,		// killsArmor
		0.10,		// killsAir
		0.10,		// killsPlayers
		0.10,		// killed
		0.10,		// killsTotal
	}; // total (xe - xb) == 0.9
	const bool doubleLines[columns] =
	{
		false,	// order
		true,		// player
		false,	// killsInfantry
		false,	// killsSoft
		false,	// killsArmor
		true,		// killsAir
		true,		// killsPlayers
		true,		// killed
		false,	// killsTotal
	};

	PackedColor color(PackedColorRGB(_color, toIntFloor(_color.A8() * alpha)));
	PackedColor bgColor(PackedColorRGB(_colorBg, toIntFloor(_colorBg.A8() * alpha)));
	PackedColor colorSelected(PackedColorRGB(_colorSelected, toIntFloor(_colorSelected.A8() * alpha)));
	PackedColor colorWest(PackedColorRGB(_colorWest, toIntFloor(_colorWest.A8() * alpha)));
	PackedColor colorEast(PackedColorRGB(_colorEast, toIntFloor(_colorEast.A8() * alpha)));
	PackedColor colorRes(PackedColorRGB(_colorRes, toIntFloor(_colorRes.A8() * alpha)));
	PackedColor colorCiv(PackedColorRGB(_colorCiv, toIntFloor(_colorCiv.A8() * alpha)));

	const float w = GEngine->Width2D();
	const float h = GEngine->Height2D();

	const float cxb = CX(xb);
	const float cxe = CX(xe);
	const float cyb = CY(yb);

	float fontHeight = _size;

	int nRows = _tableMP.Size();
	saturateMin(nRows, maxMPTableRows);

	// background
	MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
	GEngine->Draw2D(mip, bgColor, Rect2DPixel(cxb, cyb, cxe-cxb, CY(yb + (nRows + 1) * rowHeight) - cyb));

	// lines
	float bottom = yb;
	float cbottom = CY(bottom);
	GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
	for (int i=0; i<nRows+1; i++)
	{
		bottom += rowHeight;
		cbottom = CY(bottom);
		GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
	}
	float left = xb;
	float cleft = cxb;
	GEngine->DrawLine(Line2DPixel(cleft, cyb, cleft, cbottom), color, color);
	for (int i=0; i<columns; i++)
	{
		left += colWidths[i];
		cleft = CX(left);
		if (doubleLines[i])
		{
			GEngine->DrawLine(Line2DPixel(cleft - 1, cyb, cleft - 1, cbottom), color, color);
			GEngine->DrawLine(Line2DPixel(cleft + 1, cyb, cleft + 1, cbottom), color, color);
		}
		else
			GEngine->DrawLine(Line2DPixel(cleft, cyb, cleft, cbottom), color, color);
	}

	// titles
	float top = yb + 0.5 * (rowHeight - fontHeight);
	left = xb + colWidths[0];
	RString str = LocalizeString(IDS_MPTABLE_NAME);
	float x = left + 0.5 * (colWidths[1] - GEngine->GetTextWidth(_size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), _size, font, color, str);
	left += colWidths[1];
	str = LocalizeString(IDS_MPTABLE_INFANTRY);
	x = left + 0.5 * (colWidths[2] - GEngine->GetTextWidth(_size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), _size, font, color, str);
	left += colWidths[2];
	str = LocalizeString(IDS_MPTABLE_SOFT);
	x = left + 0.5 * (colWidths[3] - GEngine->GetTextWidth(_size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), _size, font, color, str);
	left += colWidths[3];
	str = LocalizeString(IDS_MPTABLE_ARMORED);
	x = left + 0.5 * (colWidths[4] - GEngine->GetTextWidth(_size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), _size, font, color, str);
	left += colWidths[4];
	str = LocalizeString(IDS_MPTABLE_AIR);
	x = left + 0.5 * (colWidths[5] - GEngine->GetTextWidth(_size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), _size, font, color, str);
	left += colWidths[5];
	str = LocalizeString(IDS_MPTABLE_PLAYERS);
	x = left + 0.5 * (colWidths[6] - GEngine->GetTextWidth(_size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), _size, font, color, str);
	left += colWidths[6];
	str = LocalizeString(IDS_MPTABLE_KILLED);
	x = left + 0.5 * (colWidths[7] - GEngine->GetTextWidth(_size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), _size, font, color, str);
	left += colWidths[7];
	str = LocalizeString(IDS_MPTABLE_TOTAL);
	x = left + 0.5 * (colWidths[8] - GEngine->GetTextWidth(_size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), _size, font, color, str);

	// values
	TargetSide playerSide = TSideUnknown;
	for (int i=0; i<nRows; i++)
	{
		AIStatsMPRow &row = _tableMP[i];
		if (row.player == GetLocalPlayerName())
		{
			float top = yb + (i + 1) * rowHeight;
			GEngine->Draw2D(mip, colorSelected, Rect2DPixel(cxb, CY(top), cxe-cxb, CY(top + rowHeight) - CY(top)));
			playerSide = row.side;
		}
		PackedColor colorAct;
		switch (row.side)
		{
		case TWest:
			colorAct = colorWest;
			break;
		case TEast:
			colorAct = colorEast;
			break;
		case TGuerrila:
			colorAct = colorRes;
			break;
		case TCivilian:
			colorAct = colorCiv;
			break;
		default:
			colorAct = color;
			break;
		}

		top += rowHeight;

		left = xb;
		x = left + 0.5 * (colWidths[0] - GEngine->GetTextWidthF(_size, font, "%d", row.order));
		GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.order);

		left += colWidths[0];
		float sizeName = 0.8 * _size;
		x = left + 0.5 * (colWidths[1] - GEngine->GetTextWidth(sizeName, font, row.player));
		GEngine->DrawText
		(
			Point2DFloat(x, top), sizeName,
			Rect2DFloat(left, top, colWidths[1], top + rowHeight),
			font, colorAct, row.player
		);

		left += colWidths[1];
		if (row.killsInfantry > 0)
		{
			x = left + 0.5 * (colWidths[2] - GEngine->GetTextWidthF(_size, font, "%d", row.killsInfantry));
			GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsInfantry);
		}
		left += colWidths[2];
		if (row.killsSoft > 0)
		{
			x = left + 0.5 * (colWidths[3] - GEngine->GetTextWidthF(_size, font, "%d", row.killsSoft));
			GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsSoft);
		}
		left += colWidths[3];
		if (row.killsArmor > 0)
		{
			x = left + 0.5 * (colWidths[4] - GEngine->GetTextWidthF(_size, font, "%d", row.killsArmor));
			GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsArmor);
		}
		left += colWidths[4];
		if (row.killsAir > 0)
		{
			x = left + 0.5 * (colWidths[5] - GEngine->GetTextWidthF(_size, font, "%d", row.killsAir));
			GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsAir);
		}
		left += colWidths[5];
		if (row.killsPlayers > 0)
		{
			x = left + 0.5 * (colWidths[6] - GEngine->GetTextWidthF(_size, font, "%d", row.killsPlayers));
			GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsPlayers);
		}
		left += colWidths[6];
		if (row.killed > 0)
		{
			x = left + 0.5 * (colWidths[8] - GEngine->GetTextWidthF(_size, font, "%d", row.killed));
			GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killed);
		}
		left += colWidths[7];
		if (row.killsTotal > 0)
		{
			x = left + 0.5 * (colWidths[7] - GEngine->GetTextWidthF(_size, font, "%d", row.killsTotal));
			GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsTotal);
		}
	}

	// summary for sides
	if (nSides > 1)
	{
		const float yb = ye - 4 * rowHeight;
		const float cyb = CY(yb);

		// background
		GEngine->Draw2D(mip, bgColor, Rect2DPixel(cxb, cyb, cxe-cxb, CY(yb + nSides * rowHeight) - cyb));

		// lines
		float bottom = yb;
		float cbottom = CY(bottom);
		GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
		for (int i=0; i<nSides; i++)
		{
			bottom += rowHeight;
			cbottom = CY(bottom);
			GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
		}
		float left = xb;
		float cleft = cxb;
		GEngine->DrawLine(Line2DPixel(cleft, cyb, cleft, cbottom), color, color);
		for (int i=0; i<columns; i++)
		{
			left += colWidths[i];
			cleft = CX(left);
			if (doubleLines[i])
			{
				GEngine->DrawLine(Line2DPixel(cleft - 1, cyb, cleft - 1, cbottom), color, color);
				GEngine->DrawLine(Line2DPixel(cleft + 1, cyb, cleft + 1, cbottom), color, color);
			}
			else
				GEngine->DrawLine(Line2DPixel(cleft, cyb, cleft, cbottom), color, color);
		}

		// values
		float top = yb + 0.5 * (rowHeight - fontHeight);
		int r = 0;
		for (int i=0; i<nSides; i++)
		{
			PackedColor colorAct;
			AIStatsMPRow *pRow = NULL;
			switch (i)
			{
			case 0:
				if (!isWest) continue;
				pRow = &rowWest;
				colorAct = colorWest;
				break;
			case 1:
				if (!isEast) continue;
				pRow = &rowEast;
				colorAct = colorEast;
				break;
			case 2:
				if (!isRes) continue;
				pRow = &rowRes;
				colorAct = colorRes;
				break;
			case 3:
				if (!isCiv) continue;
				pRow = &rowCiv;
				colorAct = colorCiv;
				break;
			}

			AIStatsMPRow &row = *pRow;
			if (row.side == playerSide)
			{
				float top = yb + r * rowHeight;
				GEngine->Draw2D(mip, colorSelected, Rect2DPixel(cxb, CY(top), cxe-cxb, CY(top + rowHeight) - CY(top)));
			}

			left = xb + colWidths[0];
			float sizeName = 0.8 * _size;
			x = left + 0.5 * (colWidths[1] - GEngine->GetTextWidth(sizeName, font, row.player));
			GEngine->DrawText
			(
				Point2DFloat(x, top), sizeName,
				Rect2DFloat(left, top, colWidths[1], top + rowHeight),
				font, colorAct, row.player
			);

			left += colWidths[1];
			if (row.killsInfantry > 0)
			{
				x = left + 0.5 * (colWidths[2] - GEngine->GetTextWidthF(_size, font, "%d", row.killsInfantry));
				GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsInfantry);
			}
			left += colWidths[2];
			if (row.killsSoft > 0)
			{
				x = left + 0.5 * (colWidths[3] - GEngine->GetTextWidthF(_size, font, "%d", row.killsSoft));
				GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsSoft);
			}
			left += colWidths[3];
			if (row.killsArmor > 0)
			{
				x = left + 0.5 * (colWidths[4] - GEngine->GetTextWidthF(_size, font, "%d", row.killsArmor));
				GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsArmor);
			}
			left += colWidths[4];
			if (row.killsAir > 0)
			{
				x = left + 0.5 * (colWidths[5] - GEngine->GetTextWidthF(_size, font, "%d", row.killsAir));
				GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsAir);
			}
			left += colWidths[5];
			if (row.killsPlayers > 0)
			{
				x = left + 0.5 * (colWidths[6] - GEngine->GetTextWidthF(_size, font, "%d", row.killsPlayers));
				GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsPlayers);
			}
			left += colWidths[6];
			if (row.killed > 0)
			{
				x = left + 0.5 * (colWidths[8] - GEngine->GetTextWidthF(_size, font, "%d", row.killed));
				GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killed);
			}
			left += colWidths[7];
			if (row.killsTotal > 0)
			{
				x = left + 0.5 * (colWidths[7] - GEngine->GetTextWidthF(_size, font, "%d", row.killsTotal));
				GEngine->DrawTextF(Point2DFloat(x, top), _size, font, colorAct, "%d", row.killsTotal);
			}

			top += rowHeight;
			r++;
		}
	}
}

LSError AIStatsEvent::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeEnum("type", type, 1))
	CHECK(ar.Serialize("time", time, 1))
	CHECK(ar.Serialize("position", position, 1))
	CHECK(ar.Serialize("message", message, 1, ""))

	CHECK(::Serialize(ar, "killedType", killedType, 1, GWorld->Preloaded(VTypeAllVehicles)))
	CHECK(ar.Serialize("killedName", killedName, 1, ""))
	CHECK(ar.Serialize("killedPlayer", killedPlayer, 1, false))
	CHECK(::Serialize(ar, "killerType", killerType, 1, GWorld->Preloaded(VTypeAllVehicles)))
	CHECK(ar.Serialize("killerName", killerName, 1, ""))
	CHECK(ar.Serialize("killerPlayer", killerPlayer, 1, false))
	return LSOK;
}

LSError AIStatsMission::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("Events", _events, 1))
	CHECK(ar.Serialize("lives", _lives, 1, -1))
	CHECK(ar.Serialize("start", _start, 1))
	return LSOK;
}

LSError AIUnitHeader::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("index", index, 1))
	CHECK(ar.Serialize("experience", experience, 1, 0))
	CHECK(ar.SerializeEnum("rank", rank, 1, RankUndefined))
	CHECK(ar.SerializeRef("Unit", unit, 1))
	CHECK(ar.Serialize("used", used, 1, false))
	return LSOK;
}

/*
LSError AIUnitPool::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("Units", _units, 1))
	CHECK(ar.SerializeEnum("side", _side, 1))
	CHECK(ar.Serialize("nextSoldier", _nextSoldier, 1))
	return LSOK;
}

void AIUnitPool::Clear()
{
	_units.Clear();
	_nextSoldier = 0;
	Update();
}

void AIUnitPool::Update()
{
	for (int i=0; i<_units.Size();)
	{
		AIUnitHeader &unit = _units[i];
		if (!unit.used) {i++; continue;}
		if (unit.unit)
		{
			unit.experience = unit.unit->GetPerson()->GetExperience();
			unit.rank = AI::RankFromExp(unit.experience);
			unit.unit = NULL;
			unit.used = false;
			i++;
		}
		else
			_units.Delete(i);
	}

	while (_units.Size() < MAX_UNITS_PER_GROUP - 1)
	{
		int index = _units.Add();
		AIUnitHeader &unit = _units[index];
		unit.index = _nextSoldier++;
		unit.experience = 0;
		unit.rank = RankUndefined;
	}
}
*/

AIStatsCampaign::AIStatsCampaign()
//	: _westUnits(TWest), _eastUnits(TEast)
{
	Clear();
}

void AIStatsCampaign::Clear()
{
	_playerInfo.index = -1;
	_playerInfo.experience = 0;
	_playerInfo.rank = RankPrivate;
	_playerInfo.unit = NULL;
/*
	_westUnits.Clear();
	_eastUnits.Clear();
*/
	_variables.Clear();
	_casualties = 0;
	_inCombat = 0;
	for (int i=0; i<SKN; i++) _kills[i] = 0;

	_score = 0;
	_lastScore = 0;

	_awards.Clear();
	_penalties.Clear();

	_date = 0;
}

void AIStatsCampaign::AddVariable(GameVariable &var)
{
	for (int i=0; i<_variables.Size(); i++)
	{
		if (_variables[i]._name == var._name)
		{
			_variables[i] = var;
			return;
		}
	}
	_variables.Add(var);
}

void AIStatsCampaign::Update(AIStatsMission &mission)
{
	_lastScore = _score;

	if (_playerInfo.unit)
	{
		float experience = _playerInfo.unit->GetPerson()->GetExperience();
		float score = experience - _playerInfo.experience;
		_playerInfo.experience = experience;
		_playerInfo.rank = _playerInfo.unit->GetPerson()->GetRank();

		// score
		if
		(
			ExtParsMission.FindEntry("minScore") &&
			ExtParsMission.FindEntry("avgScore") &&
			ExtParsMission.FindEntry("maxScore")
		)
		{
			float minScore = ExtParsMission >> "minScore";
			float avgScore = ExtParsMission >> "avgScore";
			float maxScore = ExtParsMission >> "maxScore";

			int points;
			if (score >= avgScore)
			{
				points = toInt(4 * (score - avgScore) / (maxScore - avgScore));
				saturateMin(points, 4);
			}
			else
			{
				points = toInt(3 * (score - avgScore) / (avgScore - minScore));
				saturateMax(points, -3);
			}
			_score += points;
		}
	}

	// FIX - _playerInfo.unit is not set in MP
	else if (GWorld->GetRealPlayer())
	{
		_playerInfo.experience = GWorld->GetRealPlayer()->GetExperience();
		_playerInfo.rank = GWorld->GetRealPlayer()->GetRank();
	}

	_inCombat += Glob.time - mission._start;
	
	time(&_date);
	
	for (int i=0; i<mission._events.Size(); i++)
	{
		switch (mission._events[i].type)
		{
		case SETUnitLost:
			_casualties++;
			break;
		case SETKillsEnemyInfantry:
			_kills[SKEnemyInfantry]++;
			break;
		case SETKillsEnemySoft:
			_kills[SKEnemySoft]++;
			break;
		case SETKillsEnemyArmor:
			_kills[SKEnemyArmor]++;
			break;
		case SETKillsEnemyAir:
			_kills[SKEnemyAir]++;
			break;
		case SETKillsFriendlyInfantry:
			_kills[SKFriendlyInfantry]++;
			break;
		case SETKillsFriendlySoft:
			_kills[SKFriendlySoft]++;
			break;
		case SETKillsFriendlyArmor:
			_kills[SKFriendlyArmor]++;
			break;
		case SETKillsFriendlyAir:
			_kills[SKFriendlyAir]++;
			break;
		case SETKillsCivilInfantry:
			_kills[SKCivilianInfantry]++;
			break;
		case SETKillsCivilSoft:
			_kills[SKCivilianSoft]++;
			break;
		case SETKillsCivilArmor:
			_kills[SKCivilianArmor]++;
			break;
		case SETKillsCivilAir:
			_kills[SKCivilianAir]++;
			break;
		case SETUser:
			break;
		}
	}
}

struct KillsItem
{
	AIStatsKills index;
	int value;

	LSError Serialize(ParamArchive &ar);
};
TypeIsMovableZeroed(KillsItem)

LSError KillsItem::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeEnum("index", index, 1))
	CHECK(ar.Serialize("value", value, 1, 0))
	return LSOK;
}

LSError AIStatsCampaign::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("PlayerInfo", _playerInfo, 1))
//	CHECK(ar.Serialize("WestUnits", _westUnits, 1))
//	CHECK(ar.Serialize("EastUnits", _eastUnits, 1))
	ar.SetParams(GWorld->GetGameState());
	CHECK(ar.Serialize("Variables", _variables, 1))

	CHECK(ar.Serialize("casualities", _casualties, 1, 0))
	CHECK(ar.Serialize("inCombat", _inCombat, 1, 0))

	CHECK(ar.Serialize("score", _score, 1, 0))
	CHECK(ar.Serialize("lastScore", _lastScore, 1, 0))

	CHECK(ar.SerializeArray("awards", _awards, 1))
	CHECK(ar.SerializeArray("penalties", _penalties, 1))

	AutoArray<KillsItem> kills;
	if (ar.IsSaving())
	{
		kills.Resize(SKN);
		for (int i=0; i<SKN; i++)
		{
			kills[i].index = (AIStatsKills)i;
			kills[i].value = _kills[i];
		}
		CHECK(ar.Serialize("Kills", kills, 1))
	}
	else if (ar.GetPass() == ParamArchive::PassFirst)
	{
		CHECK(ar.Serialize("Kills", kills, 1))
		for (int i=0; i<kills.Size(); i++)
		{
			int index = kills[i].index;
			if (index >=0 && index < SKN) _kills[index] = kills[i].value;
		}
	}

	CHECK(ar.Serialize("date", (int &)_date, 1, 0))

	return LSOK;
}

LSError AIStatsCampaign::CampaignSerialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("PlayerInfo", _playerInfo, 1))
//	CHECK(ar.Serialize("WestUnits", _westUnits, 1))
//	CHECK(ar.Serialize("EastUnits", _eastUnits, 1))
	ar.SetParams(GWorld->GetGameState());
	CHECK(ar.Serialize("Variables", _variables, 1))

	CHECK(ar.Serialize("casualities", _casualties, 1))
	CHECK(ar.Serialize("inCombat", _inCombat, 1))

	CHECK(ar.Serialize("score", _score, 1, 0))
	CHECK(ar.Serialize("lastScore", _lastScore, 1, 0))

	CHECK(ar.SerializeArray("awards", _awards, 1))
	CHECK(ar.SerializeArray("penalties", _penalties, 1))

	if (ar.GetArVersion() >= 3)
	{
		AutoArray<KillsItem> kills;
		if (ar.IsSaving())
		{
			kills.Resize(SKN);
			for (int i=0; i<SKN; i++)
			{
				kills[i].index = (AIStatsKills)i;
				kills[i].value = _kills[i];
			}
			CHECK(ar.Serialize("Kills", kills, 3))
		}
		else if (ar.GetPass() == ParamArchive::PassFirst)
		{
			CHECK(ar.Serialize("Kills", kills, 3))
			for (int i=0; i<kills.Size(); i++)
			{
				int index = kills[i].index;
				if (index >=0 && index < SKN) _kills[index] = kills[i].value;
			}
		}
	}

	CHECK(ar.Serialize("date", (int &)_date, 1, 0))

	return LSOK;
}

void PositionToAA11(Vector3Val pos, char *buffer);

void AIStats::OnMPMissionEnd()
{
#if _ENABLE_VBS
	bool IsVBS();
	if
	(
		IsVBS() && GetNetworkManager().IsServer()
	)
	{
		_mission.WriteVBSStatistics();
	}
#endif
}

void AIStats::OnMissionStart()
{
	_mission.OnMissionStart();
}

/*!
\patch 1.22 Date 8/30/2001 by Jirka
- Added: enable calculate all playable units into MP (I key) score
- add aiKills = true; into mission's description.ext
*/
void AIStats::OnVehicleDestroyed(EntityAI *killed, EntityAI *killer)
{
	if (!killed) return;

	Person *killedSoldier = dyn_cast<Person>(killed);
	Person *killerPerson = GetPlayerPerson(killer);

#if _ENABLE_VBS
	bool IsVBS();
	if (IsVBS() && killed && killer)
	{
		_mission.VBSKilled(killed, killer);
	}
#endif

	// MP statistics
	bool isEnemy;
	if (killer == killed) isEnemy = false;
//	else if (killed->GetTargetSide() == TEnemy) isEnemy = true;
	else if (killed->CommanderUnit() && killed->CommanderUnit()->GetPerson()->GetExperience() < ExperienceRenegadeLimit) isEnemy = true;
	else
	{
		if (killer)
		{
			AICenter *center = GWorld->GetCenter(killer->Vehicle::GetTargetSide());
			isEnemy = center && center->IsEnemy(killed->Vehicle::GetTargetSide());
		}
		else
		{
			isEnemy = true;
		}
	}

	const MissionHeader *header = GetNetworkManager().GetMissionHeader();
	if (header && header->aiKills)
	{
		if (killer)
		{
			if (killerPerson && killerPerson->Brain() && killerPerson->Brain()->IsPlayable())
			{
				RString playerName = killerPerson->GetInfo()._name;
				TargetSide playerSide = TSideUnknown;
				AIGroup *grp = killerPerson->Brain()->GetGroup();
				if (grp)
				{
					AICenter *center = grp->GetCenter();
					if (center) playerSide = center->GetSide();
				}
				_mission.AddMPKill
				(
					playerName, playerSide, killed->GetType(),
					killedSoldier && killedSoldier->IsNetworkPlayer(), isEnemy
				);
			}
		}
		if (killedSoldier && killedSoldier->Brain() && killedSoldier->Brain()->IsPlayable())
		{
			TargetSide playerSide = TSideUnknown;
			AIGroup *grp = killedSoldier->Brain()->GetGroup();
			if (grp)
			{
				AICenter *center = grp->GetCenter();
				if (center) playerSide = center->GetSide();
			}
			_mission.AddMPKill
			(
				killedSoldier->GetInfo()._name, playerSide, NULL, true, isEnemy
			);
		}
	}
	else
	{
		if (killer)
		{
			if (killerPerson && killerPerson->IsNetworkPlayer())
			{
				RString playerName = killerPerson->GetInfo()._name;
				TargetSide playerSide = TSideUnknown;
				AIUnit *brain = killerPerson->Brain();
				if (brain)
				{
					AIGroup *grp = brain->GetGroup();
					if (grp)
					{
						AICenter *center = grp->GetCenter();
						if (center) playerSide = center->GetSide();
					}
				}
				_mission.AddMPKill
				(
					playerName, playerSide, killed->GetType(),
					killedSoldier && killedSoldier->IsNetworkPlayer(), isEnemy
				);
			}
		}
		if (killedSoldier && killedSoldier->Brain() && killedSoldier->IsNetworkPlayer())
		{
			TargetSide playerSide = TSideUnknown;
			AIGroup *grp = killedSoldier->Brain()->GetGroup();
			if (grp)
			{
				AICenter *center = grp->GetCenter();
				if (center) playerSide = center->GetSide();
			}
			_mission.AddMPKill
			(
				killedSoldier->GetInfo()._name, playerSide, NULL, true, isEnemy
			);
		}
	}

	Person *player = GWorld->GetRealPlayer();
	if (!player) return;
	AIUnit *me = player->Brain();
	AIGroup *myGroup = me ? me->GetGroup() : NULL;
	AICenter *myCenter = myGroup ? myGroup->GetCenter() : NULL;
	if (!myCenter)
	{
		switch (player->Vehicle::GetTargetSide())
		{
		case TEast:
			myCenter = GWorld->GetEastCenter(); break;
		case TWest:
			myCenter = GWorld->GetWestCenter(); break;
		case TGuerrila:
			myCenter = GWorld->GetGuerrilaCenter(); break;
		case TCivilian:
			myCenter = GWorld->GetCivilianCenter(); break;
		}
	}

	AIUnit *killedUnit = killedSoldier ? killedSoldier->Brain() : NULL;

	if
	(
		killedSoldier == player ||
		myGroup && killedUnit && killedUnit->GetGroup() == myGroup
	)
	{
		char time[32], position[32];
		float tod = 24.0 * Glob.clock.GetTimeOfDay();
		int hour = toIntFloor(tod);
		int minute = toIntFloor(60.0 * (tod - hour));
		sprintf(time, "% 2d:%02d", hour, minute);
		PositionToAA11(killed->Position(), position);

		RString message = LocalizeString(IDS_STAT_UNIT_LOST);
		char buffer[256];
		sprintf
		(
			buffer, message, time,
			(const char *)LocalizeString(IDS_PRIVATE + killedSoldier->GetRank()),
			(const char *)killedSoldier->GetInfo()._name,
			position
		);
		if (killerPerson)
		{
			_mission.AddEvent
			(
				SETUnitLost, killed->Position(), buffer,
				killed->GetType(), killedSoldier->GetInfo()._name, killedSoldier == player,
				killer->GetType(), killerPerson->GetInfo()._name, killerPerson->IsNetworkPlayer()
			);
		}
		else
		{
			_mission.AddEvent
			(
				SETUnitLost, killed->Position(), buffer,
				killed->GetType(), killedSoldier->GetInfo()._name, killed == player,
				GWorld->Preloaded(VTypeAllVehicles), "", false
			);
		}
//		return; // avoid message duplication
	}

	if (killerPerson == player && killed->GetType()->GetCost() > 2000)
	{
		char time[32], position[32];
		float tod = 24.0 * Glob.clock.GetTimeOfDay();
		int hour = toIntFloor(tod);
		int minute = toIntFloor(60.0 * (tod - hour));
		sprintf(time, "% 2d:%02d", hour, minute);
		PositionToAA11(killed->Position(), position);

		// get original target side
//		TargetSide side = killed->Vehicle::GetTargetSide();
		TargetSide side = killed->GetType()->GetTypicalSide();
		bool isEnemy = killed->CommanderUnit() && killed->CommanderUnit()->GetPerson()->GetExperience() < ExperienceRenegadeLimit;
//		bool isEnemy = killed->GetTargetSide() == TEnemy;
		if (!isEnemy && myCenter) isEnemy = myCenter->IsEnemy(side);

		if (killedSoldier)
		{
			RString message;
			AIStatsEventType type;
			if (isEnemy)
			{
				message = LocalizeString(IDS_STAT_KILL_ENEMY_SOLDIER);
				type = SETKillsEnemyInfantry;
			}
			else if (side == TCivilian)
			{
				message = LocalizeString(IDS_STAT_KILL_CIVIL);
				type = SETKillsCivilInfantry;
			}
			else
			{
				message = LocalizeString(IDS_STAT_KILL_FRIENDLY_SOLDIER);
				type = SETKillsFriendlyInfantry;
			}
			char buffer[256];
			sprintf(buffer, message, time, position);
			if (killedSoldier)
				_mission.AddEvent
				(
					type, killed->Position(), buffer,
					killed->GetType(), killedSoldier->GetInfo()._name, killedSoldier->IsRemotePlayer(),
					killer->GetType(), killerPerson->GetInfo()._name, true
				);
			else
				_mission.AddEvent
				(
					type, killed->Position(), buffer,
					killed->GetType(), "", false,
					killer->GetType(), killerPerson->GetInfo()._name, true
				);
		}
		else
		{
			RString message;
			if (isEnemy)
				message = LocalizeString(IDS_STAT_KILL_ENEMY_UNIT);
			else if (side == TCivilian)
				message = LocalizeString(IDS_STAT_KILL_CIVILIAN_UNIT);
			else
				message = LocalizeString(IDS_STAT_KILL_FRIENDLY_UNIT);
			char buffer[256];
			sprintf
			(
				buffer, message, time,
				(const char *)killed->GetType()->GetDisplayName(),
				position
			);
			AIStatsEventType type;
			switch (killed->GetType()->GetKind())
			{
			default:
			case VSoft:
				if (isEnemy)
					type = SETKillsEnemySoft;
				else if (side == TCivilian)
					type = SETKillsCivilSoft;
				else
					type = SETKillsFriendlySoft;
				break;
			case VArmor:
				if (isEnemy)
					type = SETKillsEnemyArmor;
				else if (side == TCivilian)
					type = SETKillsCivilArmor;
				else
					type = SETKillsFriendlyArmor;
				break;
			case VAir:
				if (isEnemy)
					type = SETKillsEnemyAir;
				else if (side == TCivilian)
					type = SETKillsCivilAir;
				else
					type = SETKillsFriendlyAir;
				break;
			}
			_mission.AddEvent
			(
				type, killed->Position(), buffer,
				killed->GetType(), "", false,
				killer->GetType(), killerPerson->GetInfo()._name, true
			);
		}
	}
}

#if _ENABLE_VBS
void AIStats::OnVehicleDamaged(EntityAI *injured, EntityAI *killer, float damage, RString ammo)
{
	if (injured) _mission.VBSInjured(injured, killer, damage, ammo);
}

void AIStats::VBSAddHeader(RString text)
{
	_mission.VBSAddHeader(text);
}

void AIStats::VBSAddEvent(RString text)
{
	_mission.VBSAddEvent(text);
}

void AIStats::VBSAddFooter(RString text)
{
	_mission.VBSAddFooter(text);
}

#endif

void AIStats::AddUserEvent(RString event, Vector3Par pos)
{
	_mission.AddEvent
	(
		SETUser, pos, event,
		GWorld->Preloaded(VTypeAllVehicles), "", false,
		GWorld->Preloaded(VTypeAllVehicles), "", false
	);
}

LSError AIStats::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("Mission", _mission, 1))
	CHECK(ar.Serialize("Campaign", _campaign, 1))
	return LSOK;
}

AIStats GStats;

///////////////////////////////////////////////////////////////////////////////
// Strategic database classes

AITargetInfo::AITargetInfo()
{
	_idExact = NULL;
	_side = TSideUnknown;
	_type = NULL;
	_x = 0;
	_z = 0;
	
	_accuracySide = 0;
	_timeSide = TIME_MIN;
	_accuracyType = 0;
	_timeType = TIME_MIN;

	_time = TIME_MIN;
	_precisionPos=0;
	
	_exposure = false;
	_vanished = false;
	_destroyed = false;
	_dir = VZero; // unknown 
}



LSError AITargetInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("IdExact", _idExact, 1))
	CHECK(ar.SerializeEnum("side", _side, 1))
	CHECK(::Serialize(ar, "type", _type, 1))

	CHECK(ar.Serialize("realPos", _realPos, 1))
	CHECK(ar.Serialize("pos", _pos, 1))
	CHECK(ar.Serialize("x", _x, 1))
	CHECK(ar.Serialize("z", _z, 1))
	CHECK(ar.Serialize("dir", _dir, 1, VZero))
	CHECK(ar.Serialize("accuracySide", _accuracySide, 1, 4.0))
	CHECK(ar.Serialize("timeSide", _timeSide, 1))
	CHECK(ar.Serialize("accuracyType", _accuracyType, 1, 4.0))
	CHECK(ar.Serialize("timeType", _timeType, 1))
	CHECK(ar.Serialize("time", _time, 1))
	CHECK(ar.Serialize("precisionPos", _precisionPos, 1, 0))

	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		_exposure = false;	// AIMap is not serialized
	}
	return LSOK;
}

float AITargetInfo::FadingSideAccuracy() const
{
	if (_timeSide<Glob.time-AccuracyLast-2) return 0;
	const float invT=1.0/AccuracyLast;
	float fade = 1 - (Glob.time - _timeSide - 2) * invT;
	saturate(fade,0,1);
	return _accuracySide * fade;
}

float AITargetInfo::FadingTypeAccuracy() const
{
	if (_timeType<Glob.time-AccuracyLast-2) return 0;
	const float invT=1.0/AccuracyLast;
	float fade = 1 - (Glob.time - _timeType - 2) * invT;
	saturate(fade,0,1);
	return _accuracyType * fade;
}

float AITargetInfo::FadingPositionAccuracy() const
{
	if (_time<Glob.time-AccuracyLast-2) return 0;
	const float invT=1.0/AccuracyLast;
	float fade = 1 - (Glob.time - _time - 2) * invT;
	saturate(fade,0,1);
	return fade;
}

LSError AIGuardingGroup::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Group", group, 1))
	CHECK(ar.Serialize("guarding", guarding, 1, false))
	return LSOK;
}

LSError AIGurdedVehicle::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Vehicle", vehicle, 1))
	CHECK(ar.SerializeRef("By", by, 1))
	return LSOK;
}

LSError AIGurdedPoint::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("position", position, 1))
	CHECK(ar.SerializeRef("By", by, 1))
	return LSOK;
}

LSError AISupportTarget::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Group", group, 1))
	CHECK(ar.Serialize("pos", pos, 1))
	CHECK(ar.Serialize("heal", heal, 1))
	CHECK(ar.Serialize("repair", repair, 1))
	CHECK(ar.Serialize("rearm", rearm, 1))
	CHECK(ar.Serialize("refuel", refuel, 1))
	return LSOK;
}

LSError AISupportGroup::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Group", group, 1))
	CHECK(ar.SerializeRef("Assigned", assigned, 1))
	CHECK(ar.Serialize("heal", heal, 1))
	CHECK(ar.Serialize("repair", repair, 1))
	CHECK(ar.Serialize("rearm", rearm, 1))
	CHECK(ar.Serialize("refuel", refuel, 1))
	return LSOK;
}

AICheckPointInfo::AICheckPointInfo()
{
	_missions = 0;
	_time = Time(0);
}

AICheckPointInfo::AICheckPointInfo(Vector3Val pos, Time time)
{
	_pos = pos;
	_x = toIntFloor(pos.X() * InvLandGrid);
	_z = toIntFloor(pos.Z() * InvLandGrid);
	_missions = 0;
	_time = time;
}

#define DIRTY_ENEMY		0x01
#define DIRTY_UNKNOWN	0x10

struct AITargetChange
{
	bool erase;
	bool enemy;
	TargetId idExact;
	const VehicleType *type;
	int x;
	int z;
	//Vector3 pos;
	Time time;
};
TypeContainsOLink(AITargetChange)

#include <Es/Containers/array2D.hpp>

class AIMap
{
public:
	Array2D<BYTE> _dirty;
	AutoArray<BYTE> _dirtyRow;
	AutoArray<AITargetChange> _changesQueue;
	Array2D<AIThreat> _exposureEnemy;
	Array2D<AIThreat> _exposureUnknown;

	AIMap() {Init();}
	void Init();
	void AddChange(bool erase, bool enemy, const AITargetInfo &info);
	bool ProcessChange();
protected:
	void SetDirty(int x, int z, BYTE dirty);
};

void AIMap::Init()
{
	// realloc all arrays
	_dirty.Dim(LandRange,LandRange);
	_dirtyRow.Realloc(LandRange);
	_dirtyRow.Resize(LandRange);
	_exposureEnemy.Dim(LandRange,LandRange);
	_exposureUnknown.Dim(LandRange,LandRange);

	for (int i=0; i<LandRange; i++)
	{
		for (int j=0; j<LandRange; j++)
		{
			_exposureEnemy(j,i).packed = 0;
			_exposureUnknown(j,i).packed = 0;
			_dirty(j,i) = 0;
		}
		_dirtyRow[i] = 0;
	}
}

void AIMap::SetDirty(int x, int z, BYTE dirty)
{
	_dirty(x,z) |= dirty;
	_dirtyRow[z] |= dirty;
}

void AIMap::AddChange(bool erase, bool enemy, const AITargetInfo &info)
{
	if (erase && info._idExact!=NULL)
	{
		// try to eliminate pairs insert - delete
		int i;
		for (i=_changesQueue.Size() - 1; i>=0; i--)
		{
			AITargetChange &change = _changesQueue[i];
			if (change.idExact == info._idExact)
			{
				if (!change.erase && change.enemy == enemy && change.type == info._type)
				{
					// pair insert - delete found
					// delete after insert - unit destroyed sooner insert applied
					_changesQueue.Delete(i);
					return;
				}
				else
					break;
			}
		}
	}

	int index = _changesQueue.Add();
	AITargetChange &change = _changesQueue[index];
	change.erase = erase;
	change.enemy = enemy;
	change.idExact = info._idExact;
	// here must be info._pos (not info._realPos)
	//change.pos = info._pos;
	change.type = info._type;
	change.x = info._x;
	change.z = info._z;
//	change.time = Glob.time + 5.0;
	change.time = Glob.time;
}

bool AIMap::ProcessChange()
{
	Assert(_dirty.GetXRange()==LandRange);
	Assert(_dirty.GetYRange()==LandRange);

	if (_changesQueue.Size() < 1) return false; // done
	AITargetChange &info = _changesQueue[0];
	if (Glob.time < info.time) return false;		// cannot continue
#if 0
Log
(
 "Update exposure: erase %d, enemy %d, id %x, type %s, x %d, z %d, [%.1f, %.1f %.1f]",
 info.erase, info.enemy, (Object *)info.idExact,
 (const char *)info.type->GetDisplayName(), info.x, info.z,
 info.pos.X(), info.pos.Y(), info.pos.Z()
);
#endif
	int x = info.x;
	int z = info.z;
	const VehicleType *type = info.type;
	BYTE dirty;
	if (info.enemy) dirty = DIRTY_ENEMY;
	else dirty = DIRTY_UNKNOWN;
	int i, j, r;
	int xMin, xMax, zMin, zMax;

	float maxRange = -FLT_MAX;
	for (int w=0; w<type->NWeaponSystems(); w++)
	{
		const WeaponType *weapon = type->GetWeaponSystem(w);
		for (int i=0; i<weapon->_muzzles.Size(); i++)
		{
			const MuzzleType *muzzle = weapon->_muzzles[i];
			const MagazineType *magazine = muzzle->_typicalMagazine;
			if (!magazine) continue;
			for (int j=0; j<magazine->_modes.Size(); j++)
			{
				const AmmoType *ammo = magazine->_modes[j]->_ammo;
				if (ammo) saturateMax(maxRange, ammo->maxRange);
			}
		}
	}
	if (maxRange < 0) goto ExitProcessChange;
	r = toIntCeil(maxRange * InvLandGrid);

	xMin = x - r;
	xMax = x + r;
	zMin = z - r;
	zMax = z + r;
	if (xMin >= LandRange) goto ExitProcessChange;
	if (xMax < 0) goto ExitProcessChange;
	if (zMin >= LandRange) goto ExitProcessChange;
	if (zMax < 0) goto ExitProcessChange;
	saturateMax(xMin, 0);
	saturateMin(xMax, LandRangeMask);
	saturateMax(zMin, 0);
	saturateMin(zMax, LandRangeMask);
	
	for (i=zMin; i<=zMax; i++)
		for (j=xMin; j<=xMax; j++)
		{
			float dist2 = Square(j - x) + Square(i - z);
			if (dist2 > r * r) continue;
			dist2 *= Square(LandGrid);
			//if (dist2 > Square(TACTICAL_VISIBILITY)) continue;
			//if (dist2 > Square(maxRange)) continue;

			AIThreat *expInMap;
			if (info.enemy)
				expInMap = &_exposureEnemy(j,i);
			else
				expInMap = &_exposureUnknown(j,i);
			SetDirty(j, i, dirty);
		}
ExitProcessChange:
	_changesQueue.Delete(0);
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// class AICenter

#pragma warning( disable: 4355 )

AICenter::AICenter(TargetSide side, Mode mode)
	: _radio(CCSide, this, RNRadio)
{
	_nSoldier = 0;
	_nWoman = 0;

	_side = side;

	_mode = mode;

	_row = 0;
	_column = 0;
	_resources = 0;

	if (side < TSideUnknown)
		_map = new AIMap();
	else
		_map = NULL;

	_guardingValid = Glob.time - 1.0f;
	_supportValid = Glob.time - 1.0f;
}

static const EnumName EndModeNames[]=
{
	EnumName(EMContinue,"CONTINUE"),
	EnumName(EMKilled,"KILLED"),
	EnumName(EMLoser,"LOSER"),
	EnumName(EMEnd1,"END1"),
	EnumName(EMEnd2,"END2"),
	EnumName(EMEnd3,"END3"),
	EnumName(EMEnd4,"END4"),
	EnumName(EMEnd5,"END5"),
	EnumName(EMEnd6,"END6"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(EndMode dummy)
{
	return EndModeNames;
}

static const EnumName CenterModeNames[]=
{
	EnumName(AICMDisabled, "DISABLED"),
	EnumName(AICMArcade, "ARCADE"),
	EnumName(AICMIntro, "INTRO"),
	EnumName(AICMNetwork, "NETWORK"),
	EnumName(AICMArcade, "NORMAL"),
	EnumName(AICMArcade, "TRAINING"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AICenter::Mode dummy)
{
	return CenterModeNames;
}

LSError AICenter::Serialize(ParamArchive &ar)
{
	if (ar.GetArVersion() >= 6)
	{
		CHECK(ar.Serialize("Groups", _groups, 6))
	}
	else if (ar.IsLoading())
	{
		_groups.Resize(28);
		for (int i=0; i<28; i++)
		{
			char buffer[256];
			sprintf(buffer, "Group%d", i);
			CHECK(ar.Serialize(buffer, _groups[i], 1))
		}
	}
	else
	{
		return LSStructure;
	}
	CHECK(ar.Serialize("Radio", _radio, 1))

	CHECK(ar.SerializeEnum("mode", _mode, 1, (AICenterMode)AICMArcade))
	CHECK(ar.Serialize("lastUpdateTime", _lastUpdateTime, 1))
	CHECK(ar.SerializeEnum("side", _side, 1))
	CHECK(ar.Serialize("resources", _resources, 1, 0))

	for (int i=0; i<TSideUnknown; i++)
	{
		char buffer[256];
		sprintf(buffer, "friends%d", i);
		CHECK(ar.Serialize(buffer, _friends[i], 1, 1.0))
	}

	CHECK(ar.Serialize("Targets", _targets, 1))

	CHECK(ar.Serialize("soldier", _nSoldier, 1))
	CHECK(ar.Serialize("woman", _nWoman, 1, 0))

	CHECK(ar.Serialize("GuardingGroups", _guardingGroups, 1))
	CHECK(ar.Serialize("GuardingVehicles", _guardedVehicles, 1))
	CHECK(ar.Serialize("GuardingPoints", _guardedPoints, 1))
	CHECK(ar.Serialize("guardingValid", _guardingValid, 1))

	CHECK(ar.Serialize("SupportGroups", _supportGroups, 1))
	CHECK(ar.Serialize("SupportTargets", _supportTargets, 1))
	CHECK(ar.Serialize("supportValid", _supportValid, 1, Glob.time))

	// Recreate strategic map
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
	{
		_row = 0;
		_column = 0;
		if (_side < TSideUnknown)
		{
			_map = new AIMap();
			_map->Init();
			AddNewExposures();
			while (_map->ProcessChange());
		}
		else
			_map = NULL;
	}

	return LSOK;
}

AICenter *AICenter::LoadRef(ParamArchive &ar)
{
	TargetSide side = TSideUnknown;
	if (ar.SerializeEnum("side", side, 1) != LSOK) return NULL;
	switch (side)
	{
		case TWest:
			return GWorld->GetWestCenter();
		case TEast:
			return GWorld->GetEastCenter();
		case TGuerrila:
			return GWorld->GetGuerrilaCenter();
		case TCivilian:
			return GWorld->GetCivilianCenter();
		case TLogic:
			return GWorld->GetLogicCenter();
	}
	return NULL;
}

LSError AICenter::SaveRef(ParamArchive &ar) const
{
	TargetSide side = GetSide();
	CHECK(ar.SerializeEnum("side", side, 1))
	return LSOK;
}

NetworkMessageType AICenter::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateAICenter;
	case NMCUpdateGeneric:
		return NMTUpdateAICenter;
	default:
		return NMTNone;
	}
}

//! network message indices for AICenter class
class IndicesCreateAICenter : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;
public:
	//! index of field in message format
	int side;

	//! Constructor
	IndicesCreateAICenter();
	NetworkMessageIndices *Clone() const {return new IndicesCreateAICenter;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesCreateAICenter::IndicesCreateAICenter()
{
	side = -1;
}

void IndicesCreateAICenter::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(side)
}

//! Create network message indices for AICenter class
NetworkMessageIndices *GetIndicesCreateAICenter() {return new IndicesCreateAICenter();}

//! network message indices for AICenter class
class IndicesUpdateAICenter : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;
public:
	//! index of field in message format
	int friends[TSideUnknown];

	//! Constructor
	IndicesUpdateAICenter();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateAICenter;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateAICenter::IndicesUpdateAICenter()
{
	for (int i=0; i<TSideUnknown; i++)
	{
		friends[i] = -1;
	}
}

void IndicesUpdateAICenter::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	for (int i=0; i<TSideUnknown; i++)
	{
		char buffer[256];
		sprintf(buffer, "friends%d", i);
		friends[i] = format->FindIndex(buffer);
	}
}

//! Create network message indices for AICenter class
NetworkMessageIndices *GetIndicesUpdateAICenter() {return new IndicesUpdateAICenter();}

NetworkMessageFormat &AICenter::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCCreate:
		NetworkObject::CreateFormat(cls, format);
		format.Add("side", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, TEast), DOC_MSG("Side of center"));
		break;
	case NMCUpdateGeneric:
		NetworkObject::CreateFormat(cls, format);
		for (int i=0; i<TSideUnknown; i++)
		{
			char buffer[256];
			sprintf(buffer, "friends%d", i);
			format.Add(buffer, NDTFloat, NCTNone, DEFVALUE(float, 1.0), DOC_MSG("Friendship to other side"));
		}
		break;
	default:
		NetworkObject::CreateFormat(cls, format);
		break;
	}
	return format;
}

AICenter *AICenter::CreateObject(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesCreateAICenter *>(ctx.GetIndices()))
	const IndicesCreateAICenter *indices = static_cast<const IndicesCreateAICenter *>(ctx.GetIndices());

	TargetSide side;
	if (ctx.IdxTransfer(indices->side, (int &)side) != TMOK) return NULL;
	AICenter *center = new AICenter(side, AICMNetwork);
	GWorld->AddCenter(center);

	NetworkId objectId;
	if (ctx.IdxTransfer(indices->objectCreator, objectId.creator) != TMOK) return NULL;
	if (ctx.IdxTransfer(indices->objectId, objectId.id) != TMOK) return NULL;
	center->SetNetworkId(objectId);
	center->SetLocal(false);

	center->TransferMsg(ctx);

	return center;
}

void AICenter::DestroyObject()
{
	DoAssert(NGroups() == 0);
	GWorld->RemoveCenter(this);
}

TMError AICenter::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCCreate:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		if (ctx.IsSending())
		{
			Assert(dynamic_cast<const IndicesCreateAICenter *>(ctx.GetIndices()))
			const IndicesCreateAICenter *indices = static_cast<const IndicesCreateAICenter *>(ctx.GetIndices());

			ITRANSF_ENUM(side)
		}
		break;
	case NMCUpdateGeneric:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateAICenter *>(ctx.GetIndices()))
			const IndicesUpdateAICenter *indices = static_cast<const IndicesUpdateAICenter *>(ctx.GetIndices());

			for (int i=0; i<TSideUnknown; i++)
			{
				TMCHECK(ctx.IdxTransfer(indices->friends[i], _friends[i]))
			}
		}
		break;
	default:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		break;
	}
	return TMOK;
}

float AICenter::CalculateError(NetworkMessageContext &ctx)
{
	// TODO: Not implemented
	return 0.0;
}

AICenter *CreateCenter(ArcadeTemplate &t, TargetSide side, AICenter::Mode mode)
{
	switch (mode)
	{
		case AICMNetwork:
			return new AICenter(side, mode);
		case AICMArcade:
		case AICMIntro:
		{
			int n = t.groups.Size();
			for (int i=0; i<n; i++)
			{
				if (t.groups[i].side == side)
					 return new AICenter(side, mode);
			}
			return NULL;
		}
	}
	return NULL;
}

int AICenter::GetLanguage() const
{
	switch (_side)
	{
		case TWest:
			return English;
		case TEast:
			return Czech;
		case TGuerrila:
			return English;
		case TCivilian:
			return English;
		case TLogic:
			return English;
		default:
			Fail("Side");
			return English;
	}
}

bool AICenter::IsFriendly( TargetSide side) const
{
	if (side==TFriendly) return true;
	if (side==TEnemy) return false;
	if (side < 0 || side >= TSideUnknown)
		return false;

	return _friends[side] >= 0.6;
}

bool AICenter::IsNeutral( TargetSide side) const
{
	return false;
}

bool AICenter::IsEnemy( TargetSide side) const
{
	if (side==TFriendly) return false;
	if (side==TEnemy) return true;
	if (side < 0 || side >= TSideUnknown)
		return false;

//	return _friends[side] <= 0.4;
	return _friends[side] < 0.6;
}

bool AICenter::IsUnknown( TargetSide side) const
{
	return side==TSideUnknown;
}

bool AICenter::IsCivilian( TargetSide side) const
{
	return side==TCivilian;
}

bool AICenter::IsWest( TargetSide side) const
{
	return side==TWest;
}

bool AICenter::IsEast( TargetSide side) const
{
	return side==TEast;
}

bool AICenter::IsResistance( TargetSide side) const
{
	return side==TGuerrila;
}

void AICenter::UpdateSupport()
{
	_supportValid = Glob.time + 5.0f + 2.0f * GRandGen.RandomValue();

	// 1. remove destroyed groups
	for (int i=0; i<_supportTargets.Size();)
	{
		AISupportTarget &target = _supportTargets[i];
		if (!target.group)
			_supportTargets.Delete(i);
		else if (target.group->NUnits() == 0)
			SupportDone(target.group);
		else i++;
	}

	// 2. assign groups to targets
	for (int i=0; i<_supportGroups.Size(); i++)
	{
		AISupportGroup &group = _supportGroups[i];
		if (!group.group || !group.group->Leader()) continue;
		if (group.assigned) continue;
		Vector3Val posG = group.group->Leader()->Position();
		
		// find target to assign
		AISupportTarget *best = NULL;
		float minDist2 = FLT_MAX;
		for (int j=0; j<_supportTargets.Size(); j++)
		{
			AISupportTarget &target = _supportTargets[j];
			if (!target.group->Leader()) continue;
			if
			(
				target.heal && group.heal ||
				target.repair && group.repair ||
				target.refuel && group.refuel ||
				target.rearm && group.rearm
			)
			{
				float dist2 = target.group->Leader()->Position().Distance2(posG);
				if (dist2 < minDist2)
				{
					best = &target;
					minDist2 = dist2;
				}
			}
		}

		if (best)
		{
			// assign target
			group.assigned = best->group;
			if (group.heal) best->heal = false;
			if (group.repair) best->repair = false;
			if (group.refuel) best->refuel = false;
			if (group.rearm) best->rearm = false;
			group.group->Support(group.assigned, group.assigned->Leader()->Position());
		}
	}
}

void AICenter::ReadyForSupport(AIGroup *grp)
{
	if (!grp->NUnits()) return;

	int index = -1;
	for (int i=0; i<_supportGroups.Size(); i++)
	{
		if (_supportGroups[i].group == grp)
		{
			index = i;
			break;
		}
	}

	if (index < 0)
	{
		index = _supportGroups.Add();
		_supportGroups[index].group = grp;
	}

	_supportGroups[index].heal = false;
	_supportGroups[index].rearm = false;
	_supportGroups[index].refuel = false;
	_supportGroups[index].repair = false;

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = grp->UnitWithID(i + 1);
		if (!unit || unit->GetLifeState() != AIUnit::LSAlive) continue;

		Transport *transport = unit->GetVehicleIn();
		if (transport && !transport->IsDammageDestroyed())
		{
			if (transport->IsAttendant())
				_supportGroups[index].heal = true;
			if (transport->GetRepairCargo() > 10)
				_supportGroups[index].repair = true;
			if (transport->GetFuelCargo() > 10)
				_supportGroups[index].refuel = true;
			if (transport->GetAmmoCargo() > 10)
				_supportGroups[index].rearm = true;
		}
		if (unit->GetPerson()->GetType()->IsAttendant())
			_supportGroups[index].heal = true;
	}
}

void AICenter::AskSupport(AIGroup *grp, UIActionType type)
{
	if (!grp->Leader()) return;

	int index = -1;
	for (int i=0; i<_supportTargets.Size(); i++)
	{
		if (_supportTargets[i].group == grp)
		{
			index = i;
			break;
		}
	}

	if (index < 0)
	{
		index = _supportTargets.Add();
		_supportTargets[index].group = grp;
		_supportTargets[index].heal = false;
		_supportTargets[index].repair = false;
		_supportTargets[index].refuel = false;
		_supportTargets[index].rearm = false;
	}
	
	_supportTargets[index].pos = grp->Leader()->Position();
	switch (type)
	{
	case ATHeal:
		_supportTargets[index].heal = true;
		break;
	case ATRepair:
		_supportTargets[index].repair = true;
		break;
	case ATRefuel:
		_supportTargets[index].refuel = true;
		break;
	case ATRearm:
		_supportTargets[index].rearm = true;
		break;
	default:
		Fail("Action");
		break;
	}
}

void AICenter::SupportDone(AIGroup *grp)
{
	for (int i=0; i<_supportTargets.Size(); i++)
	{
		if (_supportTargets[i].group == grp)
		{
			_supportTargets.Delete(i);
			break;
		}
	}

	for (int i=0; i<_supportGroups.Size(); i++)
	{
		if (_supportGroups[i].assigned == grp)
		{
			_supportGroups[i].assigned = NULL;
			_supportGroups[i].group->CancelSupport();
		}
	}
}

bool AICenter::IsSupported(AIGroup *grp, UIActionType type)
{
	for (int i=0; i<_supportGroups.Size(); i++)
	{
		if (_supportGroups[i].assigned == grp && _supportGroups[i].group && _supportGroups[i].group->NUnits() > 0)
		{
			switch (type)
			{
			case ATHeal:
				if (_supportGroups[i].heal) return true;
				break;
			case ATRepair:
				if (_supportGroups[i].repair) return true;
				break;
			case ATRefuel:
				if (_supportGroups[i].refuel) return true;
				break;
			case ATRearm:
				if (_supportGroups[i].rearm) return true;
				break;
			case ATNone:
				if (_supportGroups[i].heal) return true;
				if (_supportGroups[i].repair) return true;
				if (_supportGroups[i].refuel) return true;
				if (_supportGroups[i].rearm) return true;
				break;
			}
		}
	}
	return false;
}

bool AICenter::WaitingForSupport(AIGroup *grp, UIActionType type)
{
	for (int i=0; i<_supportTargets.Size(); i++)
	{
		if (_supportTargets[i].group == grp)
		{
			switch (type)
			{
			case ATHeal:
				if (_supportTargets[i].heal) return true;
				break;
			case ATRepair:
				if (_supportTargets[i].repair) return true;
				break;
			case ATRefuel:
				if (_supportTargets[i].refuel) return true;
				break;
			case ATRearm:
				if (_supportTargets[i].rearm) return true;
				break;
			case ATNone:
				if (_supportTargets[i].heal) return true;
				if (_supportTargets[i].repair) return true;
				if (_supportTargets[i].refuel) return true;
				if (_supportTargets[i].rearm) return true;
				break;
			}
		}
	}
	return false;
}

bool AICenter::CanSupport(UIActionType type)
{
	for (int i=0; i<_supportGroups.Size(); i++)
	{
		if (_supportGroups[i].group && _supportGroups[i].group->NUnits() > 0)
		{
			switch (type)
			{
			case ATHeal:
				if (_supportGroups[i].heal) return true;
				break;
			case ATRepair:
				if (_supportGroups[i].repair) return true;
				break;
			case ATRefuel:
				if (_supportGroups[i].refuel) return true;
				break;
			case ATRearm:
				if (_supportGroups[i].rearm) return true;
				break;
			}
		}
	}
	return false;
}

int CmpGuardedVehicles
(
	const AITargetInfo * const *i1,
	const AITargetInfo * const *i2
)
{
	const AITargetInfo *info1 = *i1;
	const AITargetInfo *info2 = *i2;
	if (info1->_side == TSideUnknown)
	{
		if (info2->_side != TSideUnknown) return 1;
	}
	else if (info2->_side == TSideUnknown)
	{
		return -1;
	}

	float diff = info2->_type->GetCost() - info1->_type->GetCost();
	if (diff != 0)
		return sign(diff);
	else
		return info1->_idExact - info2->_idExact;
}

TypeIsSimple(const AITargetInfo *);

void AICenter::UpdateGuarding()
{
	// algorithm for assigning groups to targets
	_guardingValid = Glob.time + 25.0f + 10.0f * GRandGen.RandomValue();

	// 1. remove (temporary) groups with no units
	int nGroups = 0;
	for (int i=0; i<_guardingGroups.Size(); i++)
	{
		if (_guardingGroups[i].group && _guardingGroups[i].group->NUnits() > 0)
		{
			_guardingGroups[i].guarding = false;
			nGroups++;
		}
		else
		{
			_guardingGroups[i].guarding = true;
		}
	}
	if (nGroups == 0) return;

	// 2. reset points
	for (int i=0; i<_guardedPoints.Size(); i++)
	{
		_guardedPoints[i].by = NULL;
	}

	// 3. prepare list of guarded vehicles
	AutoArray<const AITargetInfo *> targets;
	for (int i=0; i<NTargets(); i++)
	{
		const AITargetInfo &info = GetTarget(i);
		if (!info._idExact) continue;
		if (info._idExact->IsDammageDestroyed()) continue;
		if (info._type->IsKindOf(GWorld->Preloaded(VTypeStatic))) continue;
		// TODO: check unknown targets handling
		if (info._side == TSideUnknown)
			targets.Add(&info);
		else if (IsEnemy(info._side))
			targets.Add(&info);
		/*
		if (IsEnemy(info._side))
			targets.Add(&info);
		*/
	}

	// 4. sort list
	QSort(targets.Data(), targets.Size(), CmpGuardedVehicles);

	// 5. assign vehicles to groups
	int n = targets.Size();
	_guardedVehicles.Resize(n);
	for (int i=0; i<n; i++)
	{
		AIGurdedVehicle &info = _guardedVehicles[i];
		info.vehicle = targets[i]->_idExact;
		info.by = NULL;
		// TODO: better solution
		// try to cover instead of 1:1 assignment
		if (nGroups > 0)
		{
			float minDist2 = FLT_MAX;
			int jBest = -1;
			Vector3Val posD = targets[i]->_realPos;
			for (int j=0; j<_guardingGroups.Size(); j++)
			{
				if (_guardingGroups[j].guarding) continue;
				AIGroup *grp = _guardingGroups[j].group;
				Assert(grp);
				Assert(grp->Leader());
				Vector3Val posL = grp->Leader()->Position();
				float dist2 = (posD - posL).SquareSizeXZ();
				if (dist2 < minDist2)
				{
					minDist2 = dist2;
					jBest = j;
				}
			}
			Assert(jBest >= 0);
			info.by = _guardingGroups[jBest].group;
			_guardingGroups[jBest].guarding = true;
			nGroups--;
		}
	}

	// 6. assign points to groups
	// TODO: sort _guardedPoints
	for (int i=0; i<_guardedPoints.Size() && nGroups > 0; i++)
	{
		AIGurdedPoint &info = _guardedPoints[i];
		float minDist2 = FLT_MAX;
		int jBest = -1;
		Vector3Val posD = info.position;
		for (int j=0; j<_guardingGroups.Size(); j++)
		{
			if (_guardingGroups[j].guarding) continue;
			AIGroup *grp = _guardingGroups[j].group;
			Assert(grp);
			Assert(grp->Leader());
			Vector3Val posL = grp->Leader()->Position();
			float dist2 = (posD - posL).SquareSizeXZ();
			if (dist2 < minDist2)
			{
				minDist2 = dist2;
				jBest = j;
			}
		}
		Assert(jBest >= 0);
		info.by = _guardingGroups[jBest].group;
		_guardingGroups[jBest].guarding = true;
		nGroups--;
	}	
}

AIGuardTarget AICenter::GetGuardTarget(AIGroup *grp)
{
	AIGuardTarget result;
	result.type = GTTNothing;
	
	Assert(grp);
	if (!grp) return result;

	int found = -1;
	for (int i=0; i<_guardingGroups.Size();)
	{
		if (!_guardingGroups[i].group)
		{
			_guardingGroups.Delete(i);
			continue;
		}
		
		if (_guardingGroups[i].group == grp)
		{
			found = i;
			// continue - delete obsolete items
		}
		i++;
	}

	if (found < 0)
	{
		found = _guardingGroups.Add();
		_guardingGroups[found].group = grp;
		_guardingGroups[found].guarding = false;
		UpdateGuarding();	// for new group recalculate guarding immediately
	}

	if (_guardingGroups[found].guarding)
	{
		for (int i=0; i<_guardedVehicles.Size(); i++)
		{
			if (_guardedVehicles[i].by == grp)
			{
				result.type = GTTVehicle;
				result.vehicle = _guardedVehicles[i].vehicle;
				return result;
			}
		}
		for (int i=0; i<_guardedPoints.Size(); i++)
		{
			if (_guardedPoints[i].by == grp)
			{
				result.type = GTTPoint;
				result.position = _guardedPoints[i].position;
				return result;
			}
		}
	}
	return result;
}

int AICenter::AddGuardedPoint(Vector3Par pos)
{
	int index = _guardedPoints.Add();
	_guardedPoints[index].position = pos;
	return index;
}

int AICenter::FindTargetIndex(TargetType *id) const
{
	for (int i=0; i<_targets.Size(); i++)
	{
		const AITargetInfo &item = _targets[i];
		if (id == item._idExact)
		{
			// target found
			return i;
		}
	}
	// target not found
	return -1;
}

const AITargetInfo *AICenter::FindTargetInfo(TargetType *id) const
{
	int index;
	index = FindTargetIndex(id);
	if (index < 0) return NULL;
	return &_targets[index];
}

AITargetInfo *AICenter::FindTargetInfo(TargetType *id)
{
	int index;
	index = FindTargetIndex(id);
	if (index < 0) return NULL;
	return &_targets[index];
}

void AICenter::InitPreview( const ArcadeUnitInfo &info )
{
	_lastUpdateTime = Glob.time;

	if (_map) _map->Init();
	_row = 0;
	_column = 0;
	BeginPreviewUnit(info);
}

void AICenter::Init(ArcadeTemplate &t, AutoArray<VehicleInitCmd, MemAllocSA> &inits)
{
	_lastUpdateTime = Glob.time;

	if (_map) _map->Init();
	_row = 0;
	_column = 0;

	BeginArcade(t, inits);

	if (!AssertValid())
	{
		Fail("Structure invalid after init");
	}
}

void AICenter::InitSensors(bool initialize)
{
	for (int i=0; i<NGroups(); i++)
	{
		AIGroup *grp = GetGroup(i);
		if (!grp) continue;
		grp->CreateTargetList(initialize, false);
		if (grp->NEnemiesDetected()>0 && !grp->IsAnyPlayerGroup())
		{
			grp->ReactToEnemyDetected();
		}
	}

	if (initialize)
	{
		if (_map)
		{
			AddNewExposures();
			while (_map->ProcessChange());
		}
		// setup combat mode accordingly


	}
}

#define MIN_DIST2		50.0F * 50.0F
bool ValidPos(Vector3Val pos, AutoArray<Point3> &forbidden)
{
	int i;
	for (i=0; i<forbidden.Size(); i++)
		if ((forbidden[i] - pos).SquareSizeXZ() < MIN_DIST2)
			return false;
	return true;
}

static Vector3 FindStartPosition(Vector3Val center, float radius, AutoArray<Point3> *avoid = NULL)
{
	if (radius <= 0.1) return center;

	//AutoArray<Point3> points;
	// use paper car - it is cheap in terms of textures and memory
	// it is defined so that it gives most constraints on position of all all vehicles
	Vector3 bestPoint = center;
	bool bestPointValid = false;
	int j;
	float minCost=1e10;
	for (j=0; j<100; j++)
	{
		float xr = (2.0 * GRandGen.RandomValue() - 1.0) * radius;
		float zr = (2.0 * GRandGen.RandomValue() - 1.0) * radius;
		if (Square(xr) + Square(zr) > radius * radius) continue;
		Point3 pos;		
		pos.Init();
		pos[0] = center[0] + xr;
		pos[1] = 0;
		pos[2] = center[2] + zr;
		int xtest = toIntFloor(pos.X() * InvLandGrid);
		int ztest = toIntFloor(pos.Z() * InvLandGrid);
		for (int x=-1; x<=1; x++)
			for (int z=-1; z<=1; z++)
			{
				GeographyInfo info = GLOB_LAND->GetGeography(xtest + x, ztest + z);
				if (GDummyVehicle->GetCost(info) > 1e10)
					goto Break;
			}
		if (avoid && !ValidPos(pos, *avoid))
				continue;

		{
			GeographyInfo info = GLOB_LAND->GetGeography(xtest, ztest);
			float cost = GDummyVehicle->GetCost(info);
			if (cost < minCost)
			{
				minCost = cost;
				bestPoint = pos;
				bestPointValid = true;
			}
		}

Break:
		continue;
	}
	if( !bestPointValid )
	{
		//Fail("No valid start position.");
		//TODO: correct bug 
		LogF("No valid start position.");
	}
	return bestPoint;
}

Vector3 FindWaypointPosition(Vector3Par center, float radius)
{
	// use paper car - it is cheap in terms of textures and memory
	// it is defined so that it gives most constraints on position of all all vehicles
	Vector3 normal = VUp;
	for (int j=0; j<100; j++)
	{
		float xr = (2.0 * GRandGen.RandomValue() - 1.0) * radius;
		float zr = (2.0 * GRandGen.RandomValue() - 1.0) * radius;
		if (Square(xr) + Square(zr) > radius * radius) continue;
		Vector3 pos;		
		pos.Init();
		pos[0] = center[0] + xr;
		pos[1] = 0;
		pos[2] = center[2] + zr;
		int xtest = toIntFloor(pos.X() * InvLandGrid);
		int ztest = toIntFloor(pos.Z() * InvLandGrid);
		for (int x=-1; x<=1; x++)
			for (int z=-1; z<=1; z++)
			{
				GeographyInfo info = GLOB_LAND->GetGeography(xtest + x, ztest + z);
				if (GDummyVehicle->GetCost(info) >= 1e10)
					goto BadPosition;
			}

		if (AIUnit::FindFreePosition(pos, normal, false, GDummyVehicle))
			return pos;

BadPosition:
		continue;
	}

	Vector3 pos = center;		
	AIUnit::FindFreePosition(pos, normal, false, GDummyVehicle);
	return pos;
}

OLinkArray<EntityAI> vehiclesMap;
OLinkArray<Vehicle> sensorsMap;
AutoArray<ArcadeMarkerInfo> markersMap;
AutoArray<SynchronizedItem> synchronized;

void SynchronizedItem::Add(AIGroup *grp)
{
	int index = groups.Add();
	groups[index].group = grp;
	groups[index].active = true;
}

void SynchronizedItem::Add(Vehicle *sensor)
{
	int index = sensors.Add();
	sensors[index].sensor = sensor;
	sensors[index].active = true;
}

void SynchronizedItem::SetActive(AIGroup *grp, bool active)
{
	int i, n = groups.Size();
	for (i=0; i<n; i++)
	{
		if (groups[i].group == grp)
		{
			groups[i].active = active;
		}
	}
}

void SynchronizedItem::SetActive(Vehicle *sensor, bool active)
{
	int i, n = sensors.Size();
	for (i=0; i<n; i++)
	{
		if (sensors[i].sensor == sensor)
		{
			sensors[i].active = active;
		}
	}
}

bool SynchronizedItem::IsActive(AIGroup *grp)
{
	// two items in one synchronization
	// return true <=> some is active
	int n = groups.Size();
	for (int i=0; i<n; i++)
	{
		if
		(
			groups[i].group && groups[i].group != grp &&
			groups[i].group->NUnits() > 0 && groups[i].active
		) return true;
	}
	n = sensors.Size();
	for (int i=0; i<n; i++)
	{
		if (sensors[i].sensor && sensors[i].active)
			return true;
	}
	return false;
}

// Init / load / save
void AIGlobalInit()
{
	synchronized.Clear();
	vehiclesMap.Clear();
	sensorsMap.Clear();
	markersMap.Clear();
}

LSError SynchronizedGroup::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Group", group, 1))
	CHECK(ar.Serialize("active", active, 1, false))
	return LSOK;
}

LSError SynchronizedSensor::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Sensor", sensor, 1))
	CHECK(ar.Serialize("active", active, 1, false))
	return LSOK;
}

	LSError SynchronizedItem::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("Groups", groups, 1))
	CHECK(ar.Serialize("Sensors", sensors, 1))
	return LSOK;
}

LSError AIGlobalSerialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("Synchronized", synchronized, 1))
	CHECK(ar.SerializeRefs("VehiclesMap", vehiclesMap, 1))
	CHECK(ar.SerializeRefs("SensorsMap", sensorsMap, 1))
	CHECK(ar.Serialize("Stats", GStats, 1))
	CHECK(ar.Serialize("Markers", markersMap, 1))
	return LSOK;
}

const float MinHealth=0.03;

/*
static void PlaceObject( EntityAI *veh, Matrix4 &transform )
{
	Vector3 pos=transform.Position();
	Matrix3 orient=transform.Orientation();
	
	if( veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeMan)) )
	{
		pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
	}
	else
	{
		pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0], pos[2]);
	}

	if( veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeStatic)) )
	{
		// static vehicle - place as in buldozer
		pos += orient * veh->GetShape()->BoundingCenter();

	}
	else
	{
		// dynamic vehicle
		pos[1] -= veh->GetShape()->Min().Y();
	}

	transform.SetPosition(pos);
}
*/

static Vehicle *CreateSoundSource(ArcadeUnitInfo &info, ArcadeTemplate &t, bool multiplayer)
{
	RString sim = Pars >> "CfgVehicles" >> info.vehicle >> "sound";
	DynSoundSource *vehicle = new DynSoundSource(sim);
	Vector3 pos = info.position;
	int n = info.markers.Size();
	if (n > 0)
	{	// randomized placement
		int i = toIntFloor((n + 1) * GRandGen.RandomValue());
		if (i < n)
		{
			RString name = info.markers[i];
			int m = t.markers.Size();
			for (int j=0; j<m; j++)
			{
				ArcadeMarkerInfo &mInfo = t.markers[j];
				if (stricmp(mInfo.name, name) == 0)
				{
					pos = mInfo.position;
					break;
				}
			}
		}
	}
	float xr, zr;
	do
	{
		xr = (2.0 * GRandGen.RandomValue() - 1.0) * info.placement;
		zr = (2.0 * GRandGen.RandomValue() - 1.0) * info.placement;
	} while (Square(xr) + Square(zr) > info.placement * info.placement);
	pos[0] += xr;
	pos[2] += zr;
	pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0], pos[2]);
	vehicle->SetPosition(pos);
	GWorld->AddBuilding(vehicle);
	if (multiplayer)
		GetNetworkManager().CreateVehicle(vehicle, VLTBuilding, "", -1);
	return vehicle;
}

static Vehicle *CreateMine(ArcadeUnitInfo &info, ArcadeTemplate &t, bool multiplayer)
{
	RString typeName = Pars >> "CfgVehicles" >> info.vehicle >> "ammo";
	Ref<VehicleNonAIType> type = VehicleTypes.New(typeName);
	AmmoType *aType = dynamic_cast<AmmoType *>(type.GetRef());
	Vehicle *vehicle = NewShot(NULL, aType, NULL);
	Vector3 pos = info.position;
	int n = info.markers.Size();
	if (n > 0)
	{	// randomized placement
		int i = toIntFloor((n + 1) * GRandGen.RandomValue());
		if (i < n)
		{
			RString name = info.markers[i];
			int m = t.markers.Size();
			for (int j=0; j<m; j++)
			{
				ArcadeMarkerInfo &mInfo = t.markers[j];
				if (stricmp(mInfo.name, name) == 0)
				{
					pos = mInfo.position;
					break;
				}
			}
		}
	}
	float xr, zr;
	do
	{
		xr = (2.0 * GRandGen.RandomValue() - 1.0) * info.placement;
		zr = (2.0 * GRandGen.RandomValue() - 1.0) * info.placement;
	} while (Square(xr) + Square(zr) > info.placement * info.placement);
	pos[0] += xr;
	pos[2] += zr;
	pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0], pos[2]);
	vehicle->SetPosition(pos);
	GWorld->AddFastVehicle(vehicle);
	if (multiplayer)
		GetNetworkManager().CreateVehicle(vehicle, VLTFast, "", -1);
	return vehicle;
}

struct MagazineInfo
{
	const MagazineType *type;
	int n;
};
TypeIsSimple(MagazineInfo)

static EntityAI *CreateVehicle
(
	const ArcadeUnitInfo &info, ArcadeTemplate &t, TargetSide side, bool multiplayer,
	bool disableC, bool disableD, bool disableG
)
{
	// avoid sounds
	RString vehClass;
	if( info.type )
	{
		vehClass=info.type->_simName;
	}
	else
	{
		vehClass = Pars >> "CfgVehicles" >> info.vehicle >> "vehicleClass";
	}
	if (stricmp(vehClass, "Sounds") == 0) return NULL;

	Vector3 pos = info.position, normal;
	int n = info.markers.Size();
	if (n > 0)
	{	// randomized placement
		int i = toIntFloor((n + 1) * GRandGen.RandomValue());
		if (i < n)
		{
			RString name = info.markers[i];
			int m = t.markers.Size();
			for (int j=0; j<m; j++)
			{
				ArcadeMarkerInfo &mInfo = t.markers[j];
				if (stricmp(mInfo.name, name) == 0)
				{
					pos = mInfo.position;
					break;
				}
			}
		}
	}
	pos[1] = GLOB_LAND->RoadSurfaceY(pos[0], pos[2]);
	pos = FindStartPosition(pos, info.placement);
	float azimut = -HDegree(info.azimut);

	Ref<EntityAI> veh;
	if( info.type )
	{
		veh = NewVehicle(info.type);
	}
	else
	{
		veh = NewVehicle(info.vehicle);
	}
	if (!veh) return NULL;

	Person *soldier = dyn_cast<Person>(veh.GetRef());
	if (soldier && disableD) return NULL;

/*
	int has = 0, enable = 0;
	if (veh->GetType()->HasDriver())
	{
		has++;
		if (!disableD) enable++;
	}
	if (veh->GetType()->HasCommander())
	{
		has++;
		if (!disableC) enable++;
	}
	if (veh->GetType()->HasGunner())
	{
		has++;
		if (!disableG) enable++;
	}
	if (has > 0 && enable == 0) return NULL;
*/

	if (side!=TSideUnknown) veh->SetTargetSide(side);

	if (info.name.GetLength() > 0)
	{
		veh->SetVarName(info.name);
		GWorld->GetGameState()->VarSet(info.name, GameValueExt(veh.GetRef()), true);
	}
	
	// Add into World
//	veh->Repair(floatMax(MinHealth, info.health) - 1 - veh->GetTotalDammage());
	veh->SetDammage(1 - floatMax(MinHealth, info.health));
	veh->Refuel(info.fuel * veh->GetType()->GetFuelCapacity() - veh->GetFuel());

	// ammo
	AUTO_STATIC_ARRAY(MagazineInfo, infos, 32);
	for (int i=0; i<veh->NMagazines(); i++)
	{
		Magazine *magazine = veh->GetMagazine(i);
		if (!magazine) continue;
		MagazineType *type = magazine->_type;
		if (type->_maxAmmo == 0) continue;
		int index = -1;
		for (int j=0; j<infos.Size(); j++)
		{
			if (infos[j].type == type)
			{
				index = j;
				infos[j].n++;
				break;
			}
		}
		if (index < 0)
		{
			index = infos.Add();
			infos[index].type = type;
			infos[index].n = 1;
		}
		// magazine->_ammo = toInt(info.ammo * magazine->_type->_maxAmmo);
	}
	for (int k=0; k<infos.Size(); k++)
	{
		const MagazineType *type = infos[k].type;
		int ammo = toInt(info.ammo * infos[k].n * type->_maxAmmo);
		for (int i=0; i<veh->NMagazines(); i++)
		{
			Magazine *magazine = veh->GetMagazine(i);
			if (!magazine) continue;
			if (magazine->_type != type) continue;
			if (ammo == 0)
			{
				veh->RemoveMagazine(magazine);
				i--;
			}
			else if (ammo >= type->_maxAmmo)
			{
				magazine->_ammo = type->_maxAmmo;
				ammo -= type->_maxAmmo;
			}
			else
			{
				magazine->_ammo = ammo;
				ammo = 0;
			}
		}
	}

	Matrix3 rotY(MRotationY, azimut);

	if (info.placement > 0 && !AIUnit::FindFreePosition(pos, normal, soldier!=NULL, veh))
	{
		Fail("Bad position");
		normal=VUp;
	}
	else
	{
		float dx,dz;
		pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0], pos[2], &dx, &dz);
		
		if (soldier)
		{
			// enable placing soldier onto object road
			pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2], &dx, &dz);
			normal=VUp;
		}
		else
		{
			normal=Vector3(-dx,1,-dz);
		}
	}

	Matrix3 dir;
	Matrix4 transform;


	if
	(
		info.special == ASpFlying &&
		veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir))
	)
	{
		dir.SetUpAndDirection(VUp,VForward);
		transform.SetOrientation(dir*rotY);
		veh->SetTransform(transform);

		pos[1] += veh->MakeAirborne();
		normal=VUp;

		transform.SetPosition(pos);
	}
	else
	{
		transform.SetPosition(pos);
		dir.SetUpAndDirection(normal,VForward);
		transform.SetOrientation(dir*rotY);

		veh->PlaceOnSurface(transform);
	}

	veh->SetTransform(transform);
	veh->Init(transform);

	// if vehicle is static, add it to building list

	if( veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeStatic)) )
	{
		GWorld->AddBuilding(veh);
		if (multiplayer)
			GetNetworkManager().CreateVehicle(veh, VLTBuilding, info.name, info.id);
	}
	else
	{
		GWorld->AddVehicle(veh);
		if (multiplayer)
			GetNetworkManager().CreateVehicle(veh, VLTVehicle, info.name, info.id);
	}

	if (info.id >= vehiclesMap.Size())
		vehiclesMap.Resize(info.id + 1);
	vehiclesMap[info.id] = veh.GetRef();

	// Add into target list
	if (side != Glob.header.playerSide)
	{
		AICenter *center = GWorld->GetCenter((TargetSide)Glob.header.playerSide);
		if(center)
		{
			float time = 0;
			switch (info.age)
			{
				case AAActual:
					time = 0;
					break;
				case AA5Min:
					time = 5 * 60;
					break;
				case AA10Min:
					time = 10 * 60;
					break;
				case AA15Min:
					time = 15 * 60;
					break;
				case AA30Min:
					time = 30 * 60;
					break;
				case AA60Min:
					time = 60 * 60;
					break;
				case AA120Min:
					time = 120 * 60;
					break;
				case AAUnknown:
					time = AccuracyLast;
					break;
			}
			center->InitTarget(veh, time);
		}
	}

	Transport *transport = dyn_cast<Transport>(veh.GetRef());
	if (transport)
	{
//		transport->SetLocked(info.locked);
		transport->SetLock(info.lock);

		// generate plate number from "regulations"
		const ParamEntry &world=Pars>>"CfgWorlds">> Glob.header.worldname;
		RString format = world>>"plateFormat";
		RString letters = world>>"plateLetters";

		int seed = info.id*4586 + t.randomSeed*871;

		int plateId = toLargeInt(GRandGen.RandomValue(seed)*0x1000000);

		int nLetters = letters.GetLength();

		char plate[256];
		char *p=plate;
		for (const char *f=format; *f; f++)
		{
			if(*f=='$')
			{
				*p++ = letters[plateId%nLetters];
				plateId/=nLetters;
			}
			else if (*f=='#')
			{
				*p++ = '0'+plateId%10;
				plateId/=10;
			}
			else
			{
				*p++=*f;
			}
		}
		*p=0;
		// offer the plate to the vehicle
		transport->SetPlateNumber(plate);
	}

	return veh;
}

Vehicle *CreateSensor(const ArcadeSensorInfo &info, AIGroup *grp, bool multiplayer)
{
	Vector3 pos = info.position, normal = VUp;
	pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);

	AICenter *center = NULL;
	switch (info.type)	// special cases - guarded by ...
	{
		case ASTEastGuarded:
			center = GWorld->GetEastCenter();
			goto Guarded;
		case ASTWestGuarded:
			center = GWorld->GetWestCenter();
			goto Guarded;
		case ASTGuerrilaGuarded:
			center = GWorld->GetGuerrilaCenter();
			goto Guarded;
		Guarded:
			if (!center) return NULL;
			if (grp)
			{
				Assert(grp->Leader());
				if (grp->Leader()) pos = grp->Leader()->Position();
			}
			else
			{
				if (info.idStatic >= 0)
				{
					Object *obj = GLandscape->FindObject(info.idStatic);
					if (obj) pos = obj->Position();
				}
				else
				{
					if (info.idVehicle >= 0 && info.idVehicle < vehiclesMap.Size())
					{
						Vehicle *veh = vehiclesMap[info.idVehicle];
						if (veh) pos = veh->Position(); 
					}
				}
			}
			{
				int index = center->_guardedPoints.Add();
				center->_guardedPoints[index].position = pos;
			}
			return NULL;
	}

	//RString objectName = (Pars >> "CfgDetectors" >> "objects")[info.object];
	//Ref<EntityAI> vehicle = NewVehicle(objectName);
	Ref<Vehicle> vehicle = NewNonAIVehicle(info.object);
	if (!vehicle) return NULL;

	if (info.name.GetLength() > 0)
	{
		GWorld->GetGameState()->VarSet(info.name, GameValueExt(vehicle.GetRef()), true);
	}

	// position is on sea level now
	if( vehicle->GetShape() )
	{
		pos += vehicle->GetShape()->BoundingCenter();
	}
	Matrix4 transform;
	// TODO: random azimut
	transform.SetUpAndDirection(normal, Vector3(0,0,1));
	transform.SetPosition(pos);
	vehicle->SetTransform(transform);

	Assert(dyn_cast<Detector>(vehicle.GetRef()));
	Detector *sensor = static_cast<Detector *>(vehicle.GetRef());
	sensor->FromTemplate(info);
	if (grp)
	{
		Assert(sensor->GetActivationBy() == ASAGroup);
		sensor->AssignGroup(grp);
	}

	// Add into World
	GWorld->AddBuilding(vehicle);
	if (multiplayer)
		GetNetworkManager().CreateVehicle(vehicle, VLTBuilding, info.name, -1);

	sensorsMap.Add(vehicle.GetRef());

	center = GWorld->GetCenter((TargetSide)Glob.header.playerSide);
	if(center)
	{
		float time = 0;
		switch (info.age)
		{
			case AAActual:
				time = 0;
				break;
			case AA5Min:
				time = 5 * 60;
				break;
			case AA10Min:
				time = 10 * 60;
				break;
			case AA15Min:
				time = 15 * 60;
				break;
			case AA30Min:
				time = 30 * 60;
				break;
			case AA60Min:
				time = 60 * 60;
				break;
			case AA120Min:
				time = 120 * 60;
				break;
			case AAUnknown:
				time = AccuracyLast;
				break;
		}

		// TODO: non-AI detectors InitTarget ???
		//center->InitTarget(sensor, time);
	}

	for (int k=0; k<sensor->NSynchronizations(); k++)
	{
		int sync = sensor->GetSynchronization(k);
		Assert(sync >= 0);
		if (sync >= synchronized.Size())
			synchronized.Resize(sync + 1);
		synchronized[sync].Add(sensor);
	}

	return vehicle;
}

void CreateMarker(const ArcadeMarkerInfo &info, bool multiplayer)
{
	markersMap.Add(info);
}

void InitNoCenters(ArcadeTemplate &t, AutoArray<VehicleInitCmd, MemAllocSA> &inits, bool multiplayer)
{
	// empty vehicles
	int m = t.emptyVehicles.Size();
	for (int j=0; j<m; j++)
	{
		ArcadeUnitInfo &uInfo = t.emptyVehicles[j];
		if (GRandGen.RandomValue() > uInfo.presence) continue;
		if (!GWorld->GetGameState()->EvaluateBool(uInfo.presenceCondition)) continue;

		RString vehClass = Pars >> "CfgVehicles" >> uInfo.vehicle >> "vehicleClass";
		if (stricmp(vehClass, "Sounds") == 0)
			CreateSoundSource(uInfo, t, multiplayer);
		else if (stricmp(vehClass, "Mines") == 0)
			CreateMine(uInfo, t, multiplayer);
		else
		{
			EntityAI *veh = CreateVehicle(uInfo, t, TSideUnknown, multiplayer, false, false, false);
			if (veh && uInfo.init.GetLength() > 0)
			{
				int index = inits.Add();
				inits[index].vehicle = veh;
				inits[index].init = uInfo.init;
			}
		}
	}
	// create sensors - must be after empty vehicles
	m = t.sensors.Size();
	for (int j=0; j<m; j++)
	{
		ArcadeSensorInfo &sInfo = t.sensors[j];
		CreateSensor(sInfo, NULL, multiplayer);
	}
	// markers
	m = t.markers.Size();
	for (int j=0; j<m; j++)
	{
		ArcadeMarkerInfo &mInfo = t.markers[j];
		CreateMarker(mInfo, multiplayer);
	}
}

bool IsIdentityDead(RString identity);
void EmptyDeadIdentities();

const ParamEntry &AICenter::NextSoldierIdentity(bool woman)
{
	const char *nameSide = NULL;
	switch (_side)
	{
		case TEast:
			nameSide = "East";
			break;
		case TWest:
			nameSide = "West";
			break;
		case TCivilian:
			nameSide = "Civilian";
			break;
		case TGuerrila:
		case TLogic:
			nameSide = "Guerrila";
			break;
		default:
			Fail("No such side");
			break;
	}
	Assert(nameSide);

	const char *nameType = woman ? "Women" : "Soldiers";
	int &index = woman ? _nWoman : _nSoldier;

	const ParamEntry &cfgSide = Pars >> "CfgWorlds" >> nameSide;
	int nNames = (cfgSide >> nameType).GetEntryCount();

	while (true)
	{
		if (index >= nNames)
		{
			// soldier database exhausted, start again from the beginning
			_nSoldier = 0;
			_nWoman = 0;
			EmptyDeadIdentities();
		}
		const ParamEntry &entry = (cfgSide >> nameType).GetEntry(index++);
		if (!IsIdentityDead(entry.GetContext())) return entry;
	}
	Fail("Unaccessible");
}

extern void ApplyEffects(AIGroup *group, int index);

AIUnit *AICenter::CreateSoldier(Transport *transport, int rank, const ParamEntry &cfgSide, bool multiplayer)
{
	RString nameType = transport->Type()->GetCrew();
	Person *soldier = dyn_cast<Person>(NewVehicle(nameType));
	// FIX: better handling of invalid crew specification
	if (!soldier)
	{
		ErrorMessage("Invalid crew %s",(const char*)nameType);
	}
	soldier->SetTargetSide(_side);

	// Add into World
	// maintain correct position in hierarchy
	soldier->SetPosition(transport->Position());

	// we are creating the soldier inside of some vehicle
	GWorld->AddOutVehicle(soldier);
	soldier->SetMoveOutDone(transport);
	//GWorld->AddVehicle(soldier);

	AIUnit *unit = soldier->Brain();
	unit->Load(NextSoldierIdentity(soldier->IsWoman()));

	AIUnitInfo &info = soldier->GetInfo();
	info._rank = (Rank)rank;
	info._initExperience = info._experience = ExpForRank((Rank)rank);
	return unit;
}

/*
void CheckPosition(Object *obj, const Vector3 &newPos)
{
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (veh && stricmp(veh->GetVarName(), "com1") == 0)
	{
		Vector3 dif = obj->Position() - newPos;
		if (dif.SquareSize() > Square(0.01))
		{
			LogF("Move to: %.3f, %.3f, %.3f", newPos.X(), newPos.Y(), newPos.Z());
		}
	}
}
*/

AIUnit *AICenter::CreateUnit
(
	const ArcadeUnitInfo &info, ArcadeTemplate &t, bool multiplayer, AIGroup *grp,
	bool disableC, bool disableD, bool disableG
)
{
	if (info.player == APNonplayable)
	{
		if (GRandGen.RandomValue() > info.presence) return NULL;
		if (!GWorld->GetGameState()->EvaluateBool(info.presenceCondition)) return NULL;
	}

	const char *nameSide = NULL;
	switch (_side)
	{
		case TEast:
			nameSide = "East";
			break;
		case TWest:
			nameSide = "West";
			break;
		case TGuerrila:
		case TCivilian:
		case TLogic:
			nameSide = "Guerrila";
			break;
		default:
			Fail("No such side");
			return NULL;
	}
	Assert(nameSide);
	const ParamEntry &cfgSide = Pars >> "CfgWorlds" >> nameSide;

	Ref<EntityAI> veh = CreateVehicle(info, t, _side, multiplayer, disableC, disableD, disableG);
	if (!veh) return NULL;

	Assert(veh->RefCounter()>1);

	Person *soldier = dyn_cast<Person>(veh.GetRef());
	if (soldier)
	{
		if (info.special == ASpCargo)
		{
			for (int u=0; u<MAX_UNITS_PER_GROUP; u++)
			{
				AIUnit *unit = grp->_units[u];
				if (!unit || !unit->IsUnit()) continue;
				Transport *veh = unit->GetVehicleIn();
				if (!veh) continue;
				if (veh->GetFreeManCargo() > 0)
				{
					veh->GetInCargo(soldier,false);
					soldier->Brain()->SetState(AIUnit::InCargo);
					soldier->Brain()->AssignAsCargo(veh);
					soldier->Brain()->OrderGetIn(true);
					// TODO: if (multiplayer) ... 
					break;
				}
			}
		}
		else if (_side != TLogic)
		{
			// soldier is free soldier - we should add sensor
			GWorld->AddSensor(soldier);
		}

		AIUnit *unit = soldier->Brain();
		unit->Load(NextSoldierIdentity(soldier->IsWoman()));

		AIUnitInfo &aiInfo = soldier->GetInfo();
		aiInfo._rank = (Rank)info.rank;
		aiInfo._initExperience = aiInfo._experience = ExpForRank((Rank)info.rank);
		unit->SetAbility(info.skill);

		// Add into AIGroup
		grp->AddUnit(unit);
/*
if (stricmp(info.name, "com1") == 0)
{
	LogF("Init: %.3f, %.3f, %.3f", soldier->Position().X(), soldier->Position().Y(), soldier->Position().Z());
}	
*/
		return unit;
	}
	else
	{
		Transport *transport = dyn_cast<Transport>(veh.GetRef());
		if( !transport )
		{
			RptF("%s is not soldier nor transport.",(const char *)veh->GetDebugName());
			Fail("No transport");	
			return NULL;
		}

		Assert(transport);
		// Add transport into group
		grp->AddVehicle(transport);

		AIUnit *unit = NULL;

		// Create crew
		if (transport->GetType()->HasDriver() && !disableD)
		{
		
			AIUnit *driver = CreateSoldier(transport, info.rank, cfgSide, multiplayer);

			RString name;
			if (info.name.GetLength() > 0)
			{
				name = info.name + RString("d");
				driver->GetPerson()->SetVarName(name);
				GWorld->GetGameState()->VarSet
				(
					name, GameValueExt(driver->GetPerson()), true
				);
			}
			if (multiplayer)
				GetNetworkManager().CreateVehicle(driver->GetPerson(), VLTVehicle, name, -1);
			transport->GetInDriver(driver->GetPerson(),false);
			driver->AssignAsDriver(transport);
			driver->OrderGetIn(true);
			grp->AddUnit(driver);
			unit = driver;
			driver->SetAbility(info.skill);
		}

		int commanderOffset = 1;
		int gunnerOffset = -1;
		// for vehicle where gunner is commander gunner rank
		// should be higher that driver
		if (!transport->Type()->DriverIsCommander())
		{
			commanderOffset = 2;
			gunnerOffset = 1;
		}

		if (transport->GetType()->HasCommander() && !disableC)
		{
			int commanderRank = info.rank + commanderOffset;
			saturate(commanderRank, 0, NRanks - 1);
			AIUnit *commander = CreateSoldier(transport, (Rank)commanderRank, cfgSide, multiplayer);
			RString name;
			if (info.name.GetLength() > 0)
			{
				name = info.name + RString("c");
				commander->GetPerson()->SetVarName(name);
				GWorld->GetGameState()->VarSet
				(
					name, GameValueExt(commander->GetPerson()), true
				);
			}
			else
				name = "";
			if (multiplayer)
				GetNetworkManager().CreateVehicle(commander->GetPerson(), VLTVehicle, name, -1);
			transport->GetInCommander(commander->GetPerson(),false);
			commander->AssignAsCommander(transport);
			commander->OrderGetIn(true);
			grp->AddUnit(commander);
			unit = commander;
			commander->SetAbility(info.skill);
		}

		if (transport->GetType()->HasGunner() && !disableG)
		{
			int gunnerRank = info.rank + gunnerOffset;
			saturate(gunnerRank, 0, NRanks - 1);
			AIUnit *gunner = CreateSoldier(transport, (Rank)gunnerRank, cfgSide, multiplayer);
			RString name;
			if (info.name.GetLength() > 0)
			{
				name = info.name + RString("g");
				gunner->GetPerson()->SetVarName(name);
				GWorld->GetGameState()->VarSet
				(
					name, GameValueExt(gunner->GetPerson()), true
				);
			}
			else
				name = "";
			if (multiplayer)
				GetNetworkManager().CreateVehicle(gunner->GetPerson(), VLTVehicle, name, -1);
			transport->GetInGunner(gunner->GetPerson(),false);
			gunner->AssignAsGunner(transport);
			gunner->OrderGetIn(true);
			grp->AddUnit(gunner);

			if (!unit) unit = gunner;
			gunner->SetAbility(info.skill);
		}

		if (unit)
		{
			if (_side != TLogic) GWorld->AddSensor(unit->GetPerson());
		}
		return unit;
	}
}

void AICenter::BeginArcade(ArcadeTemplate &t, AutoArray<VehicleInitCmd, MemAllocSA> &inits)
{
	_nSoldier = 0;
	_nWoman = 0;
	SetResources(0); // resources are not used

	bool multiplayer = _mode == AICMNetwork;

	// create groups
	AIUnit *playerUnit = NULL;
	if (_side == TCivilian)
	{
		_friends[TCivilian] = 1.0f;
		
		// friendship of military units is equal to resistance
		_friends[TGuerrila] = 1.0f;
		_friends[TWest] = t.intel.friends[TGuerrila][TWest];
		_friends[TEast] = t.intel.friends[TGuerrila][TEast];
	}
	else if (_side >= TSideUnknown)
	{
		// logic etc
		_friends[TEast] = 1.0;
		_friends[TWest] = 1.0;
		_friends[TGuerrila] = 1.0;
		_friends[TCivilian] = 1.0;
	}
	else
	{
		_friends[TEast] = t.intel.friends[_side][TEast];
		_friends[TWest] = t.intel.friends[_side][TWest];
		_friends[TGuerrila] = t.intel.friends[_side][TGuerrila];
		_friends[TCivilian] = 1.0;
	}

	int n = t.groups.Size();
	int group = -1;
	for (int i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = t.groups[i];
		if (gInfo.side != _side) continue;
		group++;

		AIGroup *grp = new AIGroup();
		AddGroup(grp);
		AIUnit *leaderUnit = NULL;
		OLinkArray<AIUnit> inFormation;
		// create vehicles pass 1
		int m = gInfo.units.Size();
		for (int j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.special == ASpCargo) continue;

			bool disableC = false;
			bool disableD = false;
			bool disableG = false;
			if (multiplayer)
			{
				for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
				{
					const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
					if
					(
						role->player == NO_PLAYER &&
						role->side == _side &&
						role->group == group &&
						role->unit == j
					)
					{
						switch (role->position)
						{
						case PRPCommander:
							disableC = true; break;
						case PRPNone:
						case PRPDriver:
							disableD = true; break;
						case PRPGunner:
							disableG = true; break;
						}
					}
				}
			}

			AIUnit *unit = CreateUnit(uInfo, t, multiplayer, grp, disableC, disableD, disableG);
			if (!unit) continue;

			EntityAI *veh = unit->GetVehicle();
			if (multiplayer)
			{
				for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
				{
					const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
					if
					(
						role->player != NO_PLAYER &&
						role->side == _side &&
						role->group == group &&
						role->unit == j
					)
					{
						AIUnit *player = NULL;
						switch (role->position)
						{
						case PRPNone:
						case PRPCommander:
							player = veh->CommanderUnit(); break;
						case PRPDriver:
							player = veh->PilotUnit(); break;
						case PRPGunner:
							player = veh->GunnerUnit(); break;
						}
						Assert(player);
						player->SetPlayable();
						if (role->player != AI_PLAYER)
						{
							player->GetPerson()->SetRemotePlayer(role->player);
							const PlayerIdentity *identity = GetNetworkManager().FindIdentity(role->player);
							if (identity)
							{
								AIUnitInfo &info = player->GetPerson()->GetInfo();
								info._identityContext = RString();
								info._name = identity->name;
								info._face = identity->face;
								info._glasses = identity->glasses;
								info._speaker = identity->speaker;
								info._pitch = identity->pitch;
								if (identity->squad)
								{
									info._squadTitle = identity->squad->title;
									if (identity->squad->picture.GetLength() > 0)
									{
										RString picture = RString("tmp\\squads\\") + identity->squad->nick + RString("\\") + identity->squad->picture;
										if (QIFStream::FileExists(picture))
										{
											info._squadPicture = GlobLoadTexture(picture);
										}
									}
								}
								player->GetPerson()->SetFace(info._face, info._name);
								player->GetPerson()->SetGlasses(info._glasses);
								player->SetSpeaker(info._speaker, info._pitch);
							}
						}
					}
				}
			}
			else
			{
				switch (uInfo.player)
				{
				case APPlayerCommander:
					playerUnit = veh->CommanderUnit(); goto PlayerFound;
				case APPlayerDriver:
					playerUnit = veh->PilotUnit(); goto PlayerFound;
				case APPlayerGunner:
					playerUnit = veh->GunnerUnit(); goto PlayerFound;
				PlayerFound:
					{
						playerUnit->GetPerson()->SetRemotePlayer(0);
						AIUnitInfo &info = playerUnit->GetPerson()->GetInfo();
						info._identityContext = RString();
						info._name = Glob.header.playerName;
						info._face = Glob.header.playerFace;
						info._glasses = Glob.header.playerGlasses;
						info._speaker = Glob.header.playerSpeaker;
						info._pitch = Glob.header.playerPitch;
						
						playerUnit->GetPerson()->SetFace(info._face);
						playerUnit->GetPerson()->SetGlasses(info._glasses);
						playerUnit->SetSpeaker(info._speaker, info._pitch);
					}
					break;
				}
			}

			if (uInfo.leader) leaderUnit = unit;
			if (uInfo.special == ASpForm)
				inFormation.Add(unit);

			if (uInfo.init.GetLength() > 0)
			{
				int index = inits.Add();
				inits[index].vehicle = veh;
				inits[index].init = uInfo.init;
			}
		}
		// create vehicles pass 2
		// m is still # of units
		for (int j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.special != ASpCargo) continue;
			
			if (multiplayer)
			{
				bool disable = false;
				for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
				{
					const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
					if
					(
						role->side == _side &&
						role->group == group &&
						role->unit == j
					)
					{
						disable = role->player == NO_PLAYER;
						break;
					}
				}
				if (disable) continue;
			}

			AIUnit *unit = CreateUnit(uInfo, t, multiplayer, grp);
			if (!unit) continue;

			if (multiplayer)
			{
				for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
				{
					const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
					if
					(
						role->player != NO_PLAYER &&
						role->side == _side &&
						role->group == group &&
						role->unit == j
					)
					{
						unit->SetPlayable();
						if (role->player != AI_PLAYER)
						{
							unit->GetPerson()->SetRemotePlayer(role->player);
							const PlayerIdentity *identity = GetNetworkManager().FindIdentity(role->player);
							if (identity)
							{
								AIUnitInfo &info = unit->GetPerson()->GetInfo();
								info._identityContext = RString();
								info._name = identity->name;
								info._face = identity->face;
								info._glasses = identity->glasses;
								info._speaker = identity->speaker;
								info._pitch = identity->pitch;
								if (identity->squad)
								{
									info._squadTitle = identity->squad->title;
									if (identity->squad->picture.GetLength() > 0)
									{
										RString picture = RString("tmp\\squads\\") + identity->squad->nick + RString("\\") + identity->squad->picture;
										if (QIFStream::FileExists(picture))
										{
											info._squadPicture = GlobLoadTexture(picture);
										}
									}
								}
								unit->GetPerson()->SetFace(info._face, info._name);
								unit->GetPerson()->SetGlasses(info._glasses);
								unit->SetSpeaker(info._speaker, info._pitch);
							}
						}
					}
				}
			}
			else
			{
				switch (uInfo.player)
				{
				case APPlayerCommander:
				case APPlayerDriver:
				case APPlayerGunner:
					playerUnit = unit;
					playerUnit->GetPerson()->SetRemotePlayer(0);
					{
						AIUnitInfo &info = playerUnit->GetPerson()->GetInfo();
						info._identityContext = RString();
						info._name = Glob.header.playerName;
						info._face = Glob.header.playerFace;
						info._glasses = Glob.header.playerGlasses;
						info._speaker = Glob.header.playerSpeaker;
						info._pitch = Glob.header.playerPitch;
						
						playerUnit->GetPerson()->SetFace(info._face);
						playerUnit->GetPerson()->SetGlasses(info._glasses);
						playerUnit->SetSpeaker(info._speaker, info._pitch);
					}
					break;	// in cargo
				}
			}
			if (uInfo.leader) leaderUnit = unit;

			if (uInfo.init.GetLength() > 0)
			{
				int index = inits.Add();
				inits[index].vehicle = unit->GetPerson();
				inits[index].init = uInfo.init;
			}
		}
		
		if (grp->NUnits() == 0)
		{
			grp->RemoveFromCenter();
			continue;
		}

		grp->CalculateMaximalStrength();
		if (leaderUnit)
		{
			grp->SelectLeader(leaderUnit);
			grp->SortUnits();
		}
		else
		{
			grp->SortUnits();
			SelectLeader(grp);
			leaderUnit = grp->Leader();
			Assert(leaderUnit);
		}

		// create waypoints
		m = gInfo.waypoints.Size();
		grp->_wp.Resize(m + 1);
		ArcadeWaypointInfo &wInfo = grp->_wp[0];
		wInfo.Init();
		wInfo.position = leaderUnit->Position();
		wInfo.type = ACMOVE;
		for (int j=0; j<m; j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			grp->_wp[j + 1].ArcadeWaypointInfo::operator =(wInfo);
			// Add synchronization into map
			int o = wInfo.synchronizations.Size();
			for (int k=0; k<o; k++)
			{
				int sync = wInfo.synchronizations[k];
				Assert(sync >= 0);
				if (sync >= synchronized.Size())
					synchronized.Resize(sync + 1);
				synchronized[sync].Add(grp);
			}

			if (wInfo.placement > 0)
			// set random position
				grp->_wp[j + 1].position =
					FindWaypointPosition(wInfo.position, wInfo.placement);
		}
		// create sensors - must be after selecting leader
		m = gInfo.sensors.Size();
		for (int j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			CreateSensor(sInfo, grp, multiplayer);
		}

		// set formation and semaphores
		if (grp->NWaypoints() > 1)
		{
			const ArcadeWaypointInfo &wInfo = grp->GetWaypoint(1);
			if (wInfo.formation >= 0)
			{
				AI::Formation f = (AI::Formation)wInfo.formation;
				grp->MainSubgroup()->SetFormation(f);
			}
			if (wInfo.combatMode >= 0)
			{
				AI::Semaphore s = (AI::Semaphore)wInfo.combatMode;
				grp->SetSemaphore(s);
				for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					AIUnit *unit = grp->UnitWithID(i + 1);
					if (unit) unit->SetSemaphore(s);
				}
			}
			if (wInfo.speed != SpeedUnchanged)
				grp->MainSubgroup()->SetSpeedMode(wInfo.speed);
			if (wInfo.combat != CMUnchanged)
				grp->SetCombatModeMajor(wInfo.combat);
		}

		// force units into formation
		AIUnit *leader = grp->MainSubgroup()->Leader();
		Vector3 direction = leader->Direction();
		direction[1] = 0;
		direction.Normalize();
		grp->MainSubgroup()->SetDirection(direction);
		grp->MainSubgroup()->UpdateFormationPos();
		m = inFormation.Size();
		for (int j=0; j<m; j++)
		{
			AIUnit *unit = inFormation[j];
			Assert(unit);
			if (unit == leader) continue;
			if (!unit->IsUnit()) continue;

			EntityAI *veh = unit->GetVehicle();
			Vector3Val oldPos = veh->Position();

			Vector3 pos = unit->GetFormationAbsolute();
			Vector3 normal;
			if (!AIUnit::FindFreePosition(pos, normal, unit->IsSoldier(), veh))
			{
				LogF("Bad position");
				normal=VUp;
			}

			if( veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)) )
			{
				float yAbove = oldPos[1] - GLOB_LAND->RoadSurfaceYAboveWater(oldPos[0], oldPos[2]);
				pos[1] += yAbove;
			}

			Matrix4 transform;
			transform.SetPosition(pos);
			transform.SetDirectionAndUp(direction, normal);
			veh->PlaceOnSurface(transform);
			veh->Move(transform);
		}
		grp->MainSubgroup()->UpdateFormationCoef();

		// send mission
		Mission mis;
		mis._action = Mission::Arcade;
		SendMission(grp, mis);
	}
	
	if (playerUnit)
	{
//		AIGroup *grp = playerUnit ? playerUnit->GetGroup() : NULL;
		if (_mode == AICMIntro)
		{
			if (playerUnit)
				GWorld->SwitchCameraTo(playerUnit->GetVehicle(), CamExternal);
		}
		else
		{
			GStats._campaign._playerInfo.unit = playerUnit;

			// keep rank and exp. from editor
			if (_mode != AICMNetwork)
			{
				PlayerInfo().rank = playerUnit->GetPerson()->GetInfo()._rank;
				PlayerInfo().experience = playerUnit->GetPerson()->GetInfo()._experience;
			}

			GWorld->SwitchCameraTo(playerUnit->GetVehicle(), CamInternal);
			GWorld->SetPlayerManual(true);
			GWorld->SwitchPlayerTo(playerUnit->GetPerson());
			GWorld->SetRealPlayer(playerUnit->GetPerson());
		}
	}
}

void AICenter::BeginPreviewUnit( const ArcadeUnitInfo &info )
{
	_lastUpdateTime = Glob.time;

	if (_map) _map->Init();
	_row = 0;
	_column = 0;

	ArcadeTemplate t;

	_nSoldier = 0;
	_nWoman = 0;
	SetResources(0); // resources are not used

	// create groups
	for (int i=0; i<TSideUnknown; i++) _friends[i] = 1.0f;
	
	AIGroup *grp = new AIGroup();
	AddGroup(grp);
	OLinkArray<AIUnit> inFormation;
	// create vehicles pass 1

	const ArcadeUnitInfo &uInfo = info;
	
	AIUnit *unit = CreateUnit(uInfo, t, false, grp);
	if (!unit) return;

	EntityAI *veh = unit->GetVehicle();
	GWorld->GetGameState()->VarSet("bis_buldozer_zero", GameValueExt(veh), true);

	AIUnit *playerUnit = veh->CommanderUnit();

	AIUnit *leaderUnit = playerUnit;

	if (grp->NUnits() == 0)
	{
		grp->RemoveFromCenter();
		return;
	}

	grp->SortUnits();
	grp->CalculateMaximalStrength();
	if (leaderUnit)
		grp->SelectLeader(leaderUnit);
	else
	{
		SelectLeader(grp);
		leaderUnit = grp->Leader();
		Assert(leaderUnit);
	}

	grp->MainSubgroup()->UpdateFormationCoef();

	if (playerUnit)
	{
		playerUnit->GetPerson()->GetInfo()._name = "Dummy Bummy";

		GWorld->SwitchCameraTo(playerUnit->GetVehicle(), CamInternal);
		GWorld->GetGameState()->VarSet("bis_buldozer_cursor", GameValueExt(playerUnit->GetVehicle()), true);
		GWorld->SetPlayerManual(true);
		GWorld->SwitchPlayerTo(playerUnit->GetPerson());
		GWorld->SetRealPlayer(playerUnit->GetPerson());

//		playerUnit->_speaker = -1;
	}
}


void AICenter::SelectLeader(AIGroup *grp)
{
	AIUnit *unit = grp->LeaderCandidate(NULL);
	if (unit)
	{
		grp->SelectLeader(unit);
	}
	/*
	AIUnit *unit = NULL;
	Rank bestRank = RankPrivate;
	float bestExp = -FLT_MAX;
	// find unit with the highest rank
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = grp->UnitWithID(i + 1);
//		group leader can be in cargo now
//		if (u && !u->GetInCargo())
		if (u)
		{
			Rank rank = u->GetPerson()->GetRank();
			if (rank > bestRank)
			{
				bestRank = rank;
				bestExp = u->GetPerson()->GetExperience();
				unit = u;
			}
			else if (rank == bestRank)
			{
				float exp = u->GetPerson()->GetExperience();
				if (exp > bestExp)
				{
					bestExp = exp;
					unit = u;
				}
			}
		}
	}

	if (!unit)
	{
		// no units
		grp->_leader = NULL;
		return;
	}

	if (bestRank >= RankCaptain)
	{
		grp->SelectLeader(unit);
		return;
	}
	grp->SelectLeader(unit);
	return;
	*/
}

void AICenter::AddGroup(AIGroup *grp, int id)
{
	if (id == 0)
	{	// attach new id
		id = 1;
		for (int i=0; i<_groups.Size(); i++)
		{
			AIGroup *grp = _groups[i];
			if (!grp) continue;
			saturateMax(id, grp->_id + 1);
		}
	}
	_groups.Add(grp);
	grp->_center = this;
	grp->_id = id;
	grp->Init();
}

void AICenter::GroupRemoved(AIGroup *grp)
{
	Assert(grp->NUnits() == 0);
	Assert(grp->ID() > 0);
	for (int i=0; i<NGroups(); i++)
	{
		if (grp == _groups[i])
		{
			_groups.Delete(i);
			return;
		}
	}
	Fail("Not found.");
}

float AICenter::RecalculateExposure(int x, int z, AIThreat &result, SideFunction func)
{
	// return value - change of exposure
	float change = 0;
	Threat exposure;

	Vector3 fieldPos
	(
		x * LandGrid + LandGrid * 0.5,
		0,
		z * LandGrid + LandGrid * 0.5
	);
	fieldPos[1] = GLOB_LAND->SurfaceY(fieldPos[0], fieldPos[2]);
	saturateMax(fieldPos[1], 0); // zero - normal sea level
	fieldPos[1] += 3;

	//LogF("Update %.1f,%.1f",(float)x*LandGrid,(float)z*LandGrid);

	for (int i=0; i<_targets.Size(); i++)
	{
		AITargetInfo &info = _targets[i];
		if (!info._exposure) continue;
		if (info._vanished) continue;
		if (info._destroyed) continue;
		if (!func(this, info._side)) continue;
		const VehicleType *type=info._type;
		// consider weapons of target
		if (type->NWeaponSystems() <= 0) continue;

		float maxRange = -FLT_MAX;
		for (int w=0; w<type->NWeaponSystems(); w++)
		{
			const WeaponType *weapon = type->GetWeaponSystem(w);
			for (int i=0; i<weapon->_muzzles.Size(); i++)
			{
				const MuzzleType *muzzle = weapon->_muzzles[i];
				const MagazineType *magazine = muzzle->_typicalMagazine;
				if (!magazine) continue;
				for (int j=0; j<magazine->_modes.Size(); j++)
				{
					const AmmoType *ammo = magazine->_modes[j]->_ammo;
					if (ammo) saturateMax(maxRange, ammo->maxRange);
				}
			}
		}
		if (maxRange <= 0) continue;

		float dist2 = Square(info._x - x) + Square(info._z - z);
		dist2 *= Square(LandGrid);
		// check maxRange of vehicle
		if (dist2 <= Square(maxRange))
		{
			// check strategic visibility
			// here must be info._pos (not info._realPos)
			float dirCoef = info._dir.CosAngle(fieldPos - info._pos);
			float visibility = GLOB_LAND->VisibleStrategic(info._pos,fieldPos);
			if (visibility > 0.5)
			{
				//LogF("  pos %.1f,%.1f",info._pos.X(),info._pos.Z());
				exposure += info._type->GetStrategicThreat(dist2, visibility, dirCoef);
			}
			// check azimut

			ADD_COUNTER(exMap,1);
		}
	}

	GeographyInfo geog = GLandscape->GetGeography(x,z);
	// consider unit will be cover by objects
	float coverFactor = 1-geog.u.howManyObjects*0.3;
	exposure = exposure*coverFactor;
	int expInt;
	expInt = toInt(exposure[VSoft] * INV_EXPOSURE_COEF);
	saturate(expInt, 0, 255);
	change += EXPOSURE_COEF * abs(expInt - result.u.soft);
	result.u.soft = expInt;
	expInt = toInt(exposure[VArmor] * INV_EXPOSURE_COEF);
	saturate(expInt, 0, 255);
	change += EXPOSURE_COEF * abs(expInt - result.u.armor);
	result.u.armor = expInt;
	expInt = toInt(exposure[VAir] * INV_EXPOSURE_COEF);
	saturate(expInt, 0, 255);
	change += EXPOSURE_COEF * abs(expInt - result.u.air);
	result.u.air = expInt;

	return change;
}

void AICenter::UpdateField(int x, int z)
{
	float changeEnemy = 0;
	float changeUnknown = 0;
	BYTE &dirty = _map->_dirty(x,z);
	if (dirty & DIRTY_ENEMY)
	{
		changeEnemy = RecalculateExposure
		(
			x, z, _map->_exposureEnemy(x,z), ::IsEnemy
		);
	}
	if (dirty & DIRTY_UNKNOWN)
	{
		changeUnknown = RecalculateExposure
		(
			x, z, _map->_exposureUnknown(x,z), ::IsUnknown
		);
	}

	dirty = 0;
	
	if
	(
		changeEnemy > 0.5 ||
		changeUnknown > 0.5
	)
	{
		int g;
		for (g=0; g<NGroups(); g++)
		{
			AIGroup *grp = GetGroup(g);
			if (!grp)
				continue;
			for (int sg=0; sg<grp->NSubgroups(); sg++)
			{
				AISubgroup *subgrp = grp->GetSubgroup(sg);
				if (!subgrp)
					continue;
				subgrp->ExposureChanged(x, z, changeEnemy, changeEnemy + changeUnknown);
			}
		}
	}
}

void AICenter::UpdateMap()
{
	//const int nc = LandRange*LandRange;
	// due to bus limitation it is not possible to check all fields
	// for dirty flag in every simulation cycle
	// (it took 10-15% for 20fps when there were no dirty fields)
	// limit dirty flag checking is important when there are little dirty fields
	int i;
	int n = TargetsPerCycle;
	for (i=0; i<n; i++)
		_map->ProcessChange();
	
	n = FieldsPerCycle;
	int nRows = 16;
	int nAll = LandRange * LandRange;
	
	// finish row
	if (_column != 0)
	{
		bool dirty = false;
		for (i=0; i<_column; i++)
			if (_map->_dirty(i,_row) != 0)
				dirty = true;
		
		while (_column != 0)
		{
			if (n <= 0 || nAll <= 0)
				goto EndOfUpdateMap;
			if (_map->_dirty(_column,_row) != 0)
			{
				UpdateField(_column, _row);
				//GWorld->SecondaryAllowSwitch();
				n--;
			}
			_column = (_column + 1) & (LandRangeMask);
			nAll--;
		}
		if (!dirty)
			_map->_dirtyRow[_row] = 0;
		_row = (_row + 1) & (LandRangeMask);
		nRows--;
	}

	while (true)
	{
		Assert(_column == 0);
		if (n <= 0 || nRows <= 0 || nAll <= 0)
			goto EndOfUpdateMap;

		while (_map->_dirtyRow[_row] == 0)
		{
			_row = (_row + 1) & (LandRangeMask);
			nAll -= LandRange;
			if (nAll <= 0)
				goto EndOfUpdateMap;
		}
		for (i=0; i<LandRange; i++)
		{
			if (n <= 0 || nAll <= 0)
				goto EndOfUpdateMap;
			if (_map->_dirty(_column,_row) != 0)
			{
				UpdateField(_column, _row);
				//GWorld->SecondaryAllowSwitch();
				n--;
			}
			_column = (_column + 1) & (LandRangeMask);
			nAll--;
		}
		_map->_dirtyRow[_row] = 0;
		_row = (_row + 1) & (LandRangeMask);
		nRows--;
	}

EndOfUpdateMap:;

	#if 0
		// if there are many cycles left, use them to random map update
		// this should help removing any artifacts
		for( ;n>FieldsPerCycle/4; n-- )
		{
			int row=toIntFloor(GRandGen.RandomValue()*LandRange);
			int col=toIntFloor(GRandGen.RandomValue()*LandRange);
			if( !InRange(row,col) ) continue;
			_map->_dirtyRow[row]=true;
			_map->_dirty[row][col]=true;
		}
	#endif

#if 0
	switch (GetSide())
	{
		case TWest:
			Log("*** WEST ***");
			break;
		case TEast:
			Log("*** EAST ***");
			break;
		case TGuerrila:
			Log("*** GUERRILA ***");
			break;
	}
	int dirty = 0;
	int dirtyRows = 0;
	for (i=0; i<LandRange; i++)
	{
		for (int j=0; j<LandRange; j++)
		{
/*
			BYTE exposure = _map._exposureUnknown(j,i);
			if (exposure > 0)
				Log("Exposure [%d, %d] = %d", i, j, exposure);
*/
			if (_map._dirty(j,i) != 0)
				dirty ++;
		}
		if (_map._dirtyRow[i] != 0)
			dirtyRows ++;
	}
	Log("Dirty fields: %d, dirty rows: %d", dirty, dirtyRows);
#endif
}

// TODO: better interface for PerformanceCounter

typedef __int64 SectionTimeHandle;

SectionTimeHandle StartSectionTime();
float GetSectionTime(SectionTimeHandle section);
bool CompareSectionTimeGE(SectionTimeHandle section, float time);
__int64 GetSectionResolution();

void AICenter::UpdateGroup()
{
	// maintain CPU load under certain limit
	// get current time (QueryPerformanceCounter)
	SectionTimeHandle sectionTime = StartSectionTime();
	float maxTimePerCenter = 0.020;

	for (int i=0; i<GroupsPerCycle; ) 
	{
		// search for oldest non-updated group
		float maxAge = 0;
		AIGroup *maxGrp = NULL;
		for (int j=0; j<NGroups(); j++)
		{
			AIGroup *grp = GetGroup(j);
			if (!grp)
				continue;
			float age = _lastUpdateTime - grp->_lastUpdateTime;
			if (maxAge<age)
			{
				maxAge = age;
				maxGrp = grp;
			}
		}
		if (!maxGrp)
		{
			// Not found
			_lastUpdateTime = Glob.time;
			break; // no more group to update
		}
		bool think = maxGrp->Think();

		if (think)
		{
			i++;
		}
		maxGrp->_lastUpdateTime = _lastUpdateTime;
		// guarantee no center takes too much CPU time
		if (CompareSectionTimeGE(sectionTime,maxTimePerCenter)) break;
	}
	/*
	LogF
	(
		"Center %s, time %g, F: %I64d, T: %I64d,%I64d",
		(const char *)GetDebugName(),
		GetSectionTime(sectionTime),
		GetSectionResolution(),
		sectionTime,StartSectionTime()
	);
	*/
}

void AICenter::Think()
{
	PROFILE_SCOPE(aiCnt);
	DoAssert( AssertValid() );

	if (_side == TLogic)
	{
		UpdateGroup();
		DoAssert( AssertValid() );
		return;
	}

	// Update database - remove targets older than AccuracyLast
	for (int i=0; i<_targets.Size();)
	{
		AITargetInfo &info = _targets[i];
		if (info._exposure)
		{
			i++;
			continue; // cannot delete
		}

		if (Glob.time > info._time + AccuracyLast)
			_targets.Delete(i);
		else
			i++;
	}
	
	RemoveOldExposures();
	UpdateMap();
	if (Glob.time >= _guardingValid) UpdateGuarding();
	if (Glob.time >= _supportValid) UpdateSupport();
	UpdateGroup();
	AddNewExposures();

	DoAssert( AssertValid() );
}

void AICenter::SendMission(AIGroup *to, Mission &mis)
{
	Assert(to);

#if LOG_COMM
	Log("Send Mission: Group %s: Mission %d",
	(const char *)to->GetDebugName(), mis._action);
#endif

	to->ReceiveMission(mis);
}

void AICenter::ReceiveAnswer(AIGroup *from, Answer answer)
{
	if (!from)
		return;

#if LOG_COMM
	Log("Receive answer: Group %s: Answer %d",
	(const char *)from->GetDebugName(), answer);
#endif

	switch (answer)
	{
		case AI::MissionCompleted:
		case AI::WorkCompleted:
		{
			AIUnit *leader = from->Leader();
			if (leader)
			{
				leader->AddExp(ExperienceMissionCompleted);
			}
//			from->_missionStatus = MSCompleted;
		}
		break;
		case AI::MissionFailed:
		case AI::WorkFailed:
		{
			AIUnit *leader = from->Leader();
			if (leader)
			{
				leader->AddExp(ExperienceMissionFailed);
			}
//			from->_missionStatus = MSFailed;
		}
		break;
	}
}

#if LOG_COMM
static const char * sideNames[] =
{
	"East",
	"West",
	"Guerrila",
	"Civilian",
	"Unknown"
};
#endif	

void AICenter::RemoveOldExposures()
{
	for (int i=0; i<_targets.Size(); i++)
	{
		AITargetInfo &info = _targets[i];
		if (!info._exposure) continue;
		
		if (Glob.time > info._time + ExposureTimeout)
		{
			if (info._side == TSideUnknown)
				_map->AddChange(true, false, info);
			else if (IsEnemy(info._side))
				_map->AddChange(true, true, info);
			else
				Fail("Illegal exposure");
			info._exposure = false;
		}
	}
}

void AICenter::AddNewExposures()
{
	for (int i=0; i<_targets.Size(); i++)
	{
		AITargetInfo &info = _targets[i];
		if (info._exposure) continue;
		if (info._vanished) continue;
		if (info._destroyed) continue;
		
		if (Glob.time <= info._time + ExposureTimeout)
		{
			if (info._side == TSideUnknown)
			{
				_map->AddChange(false, false, info);
				info._exposure = true;
			}
			else if (IsEnemy(info._side))
			{
				_map->AddChange(false, true, info);
				info._exposure = true;
			}
		}
	}
}

void AICenter::InitTarget(EntityAI *veh, float age)
{
	// InitTarget can be called only once for every target
	// age is in seconds
	int index = FindTargetIndex(veh);
	Assert(index < 0);
	if (index >= 0) return;

	index = _targets.Add();
	AITargetInfo &item = _targets[index];
	item._idExact = veh;
	// nasty trick: avoid detecting as empty
	item._side = veh->EntityAI::GetTargetSide();
	item._type = veh->GetType();
	item._realPos = veh->Position();
	item._pos = veh->Position();
	item._x = toIntFloor(item._pos.X() * InvLandGrid);
	item._z = toIntFloor(item._pos.Z() * InvLandGrid);
	item._accuracySide = 1.0;
	item._timeSide = Glob.time - age;
	item._accuracyType = 1.0;
	item._timeType = Glob.time - age;
	item._time = Glob.time - age;
	item._exposure = false;
	if (age<=5*60)
	{
		item._dir = veh->Direction();
	}

	#if LOG_TARGETS
		LogF
		(
			"%s target inserted (init) %s (side %d, age %.0f)",
			(const char *)GetDebugName(),(const char *)item._idExact->GetDebugName(),
			item._side, age
		);
	#endif
}

void AICenter::DeleteTarget(TargetType *id)
{
	int index = FindTargetIndex(id);
	if (index < 0) return;

	AITargetInfo &item = _targets[index];
	Assert(item._idExact == id);

	#if LOG_TARGETS
		LogF
		(
			"%s target deleted %s (side %d)",
			(const char *)GetDebugName(),(const char *)id->GetDebugName(),
			item._side
		);
	#endif

	if (item._exposure)
	{
		if (item._side == TSideUnknown)
			_map->AddChange(true, false, item);
		else if (IsEnemy(item._side))
			_map->AddChange(true, true, item);
		else
			Fail("Illegal exposure");
		// item._exposure = false;
	}
	_targets.Delete(index);
}

void AICenter::UpdateTarget(Target &target)
{
	int index = FindTargetIndex(target.idExact);
	if (index >= 0)
	{
		AITargetInfo &item = _targets[index];
		Assert(item._idExact == target.idExact);
		float oldAccuracyType = item.FadingTypeAccuracy();
		float oldAccuracySide = item.FadingSideAccuracy();
		float newAccuracyType = target.FadingAccuracy();
		float newAccuracySide = target.FadingSideAccuracy();
		TargetSide newSide;

		bool change = false;
		if (newAccuracySide >= floatMin(oldAccuracySide, 2.1))
		{
			newSide = target.side;
			item._timeSide = Glob.time;
			item._accuracySide = newAccuracySide;
			change = true;
		}
		else
			newSide = item._side;
		const VehicleType *newType;
		if (newAccuracyType > oldAccuracyType)
		{
			newType = target.type;
			item._timeType = Glob.time;
			item._accuracyType = newAccuracyType;
			change = true;
		}
		else
			newType = item._type;

		float errSize2 = target.posError.SquareSize();
		if
		(
			target.lastSeen>item._time &&
			errSize2<=item._precisionPos
		)
		{ // change only if target information is more recent
			item._time = target.lastSeen;
			item._realPos = target.AimingPosition();
			item._precisionPos = target.posError.SquareSize();
			change = true;
		}

		bool forceUpdate = false;
		if (errSize2<=Square(2) ) // we see it very precisely
		{
			Vector3Val dir = target.idExact->GetEyeDirection();
			// limitDist = 2 * sin(alfa/2)
			// for alfa = 45 deg limitDist = 0.76536686473
			const float limitDist = 0.76;
			if (dir.Distance2(item._dir)>Square(limitDist))
			{
				//LogF("%s: Target rotated",(const char *)target.idExact->GetDebugName());
				item._dir = dir;
				change = true;
				forceUpdate = true;
			}
		}

		if (target.vanished!=item._vanished)
		{
			item._vanished = target.vanished;
			forceUpdate = true;
		}

		if (target.destroyed!=item._destroyed)
		{
			item._destroyed = target.destroyed;
			newSide = target.side;
			forceUpdate = true;
		}

		if( change ) // some change in target information
		{
			int x = toIntFloor(item._realPos.X() * InvLandGrid);
			int z = toIntFloor(item._realPos.Z() * InvLandGrid);
			if
			(
				forceUpdate ||
				item._x != x || item._z != z ||
				item._side != newSide ||
				item._type != newType
			)
			{
				if (item._exposure)
				{
					if (item._side == TSideUnknown)
						_map->AddChange(true, false, item);
					else if (IsEnemy(item._side))
						_map->AddChange(true, true, item);
					else
						Fail("Illegal exposure");
					item._exposure = false;
				}
				item._pos = item._realPos;
				item._x = x;
				item._z = z;
				item._type = newType;
				item._side = newSide;
			}
		}
	}
	else
	{
		// target not found
		index = _targets.Add();
		AITargetInfo &item = _targets[index];
		item._idExact = target.idExact;
		item._side = target.side;
		item._type = target.type;
		item._realPos = target.position;
		item._precisionPos = target.posError.SquareSize();
		item._pos = target.position;
		item._x = toIntFloor(item._pos.X() * InvLandGrid);
		item._z = toIntFloor(item._pos.Z() * InvLandGrid);
		item._accuracySide = target.FadingSideAccuracy();
		item._timeSide = Glob.time;
		item._accuracyType = target.FadingAccuracy();
		item._timeType = Glob.time;
		item._time = target.lastSeen;
		item._exposure = false;

		#if LOG_TARGETS
			LogF
			(
				"%s target inserted (update) %s (side %d)",
				(const char *)GetDebugName(),(const char *)target.idExact->GetDebugName(),
				item._side
			);
		#endif
	}
}

void AICenter::ReceiveReport(AIGroup *from, ReportSubject subject, Target &target)
{
	UpdateTarget(target);
}

float AICenter::GetExposurePessimistic(int x, int z) const
{
	if (!_map) return 0;
	if( !InRange(x,z) ) return 0;
	AIThreat &expE=_map->_exposureEnemy(x,z);
	AIThreat &expU=_map->_exposureUnknown(x,z);
	int exposureE = expE.u.soft+expE.u.armor+expE.u.air;
	int exposureU = expU.u.soft+expU.u.armor+expU.u.air;
	return EXPOSURE_COEF * 0.33 * (exposureE+exposureU);
}

float AICenter::GetExposureOptimistic(int x, int z) const
{
	if (!_map) return 0;
	if( !InRange(x,z) ) return 0;
	AIThreat &expE=_map->_exposureEnemy(x,z);
	int exposureE = expE.u.soft+expE.u.armor+expE.u.air;
	return EXPOSURE_COEF * 0.33 * exposureE;
}

float AICenter::GetExposureUnknown(int x, int z) const
{
	if (!_map) return 0;
	if( !InRange(x,z) ) return 0;
	AIThreat &expU=_map->_exposureUnknown(x,z);
	int exposureU = expU.u.soft+expU.u.armor+expU.u.air;
	return EXPOSURE_COEF * 0.33 * exposureU;
}

float AICenter::GetExposurePessimistic(Vector3Par pos) const
{
	return GetExposurePessimistic
	(
		toIntFloor(pos.X()*InvLandGrid),toIntFloor(pos.Z()*InvLandGrid)
	);
}
float AICenter::GetExposureOptimistic(Vector3Par pos) const
{
	return GetExposureOptimistic
	(
		toIntFloor(pos.X()*InvLandGrid),toIntFloor(pos.Z()*InvLandGrid)
	);
}
float AICenter::GetExposureUnknown(Vector3Par pos) const
{
	return GetExposureUnknown
	(
		toIntFloor(pos.X()*InvLandGrid),toIntFloor(pos.Z()*InvLandGrid)
	);
}

RString AICenter::GetDebugName() const
{
	switch (_side)
	{
		case TEast:
			return "EAST";
		case TWest:
			return "WEST";
		case TGuerrila:
			return "GUER";
		case TCivilian:
			return "CIVL";
		case TLogic:
			return "LOGIC";
		default:
			Fail("Side");
			return NULL;
	}
}

bool AICenter::AssertValid() const
{
	bool result = true;
	for (int i=0; i<NGroups(); i++)
	{
		AIGroup *grp = GetGroup(i);
		if (!grp) continue;
		if (grp->GetCenter() == NULL)
		{
			Fail("group with no center");
			result = false;
		}
		else if (grp->GetCenter() != this)
		{
			Fail("group with other center");
			result = false;
		}
		if (!grp->AssertValid()) result = false;
	}
	return result;
}

void AICenter::Dump(int indent) const
{
}

ArcadeTemplate CurrentTemplate;

int GetTemplateSeed()
{
	return CurrentTemplate.randomSeed;
}

RString CurrentCampaign;
RString CurrentBattle;
RString CurrentMission;
RString CurrentWorld;
RString CurrentFile;
