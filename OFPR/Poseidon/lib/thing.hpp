#ifdef _MSC_VER
#pragma once
#endif

#ifndef _THING_HPP
#define _THING_HPP

#include "transport.hpp"

class ThingType: public TransportType
{
	protected:
	friend class Thing;
	friend class ThingEffectLight;

	typedef VehicleTransportType base;
	float _submerged; // some thing submerge slowly under ground
	// _submerged<=0 should be interpreted as 0, it just make submerging
	// to start later
	float _submergeSpeed; // how fast do we submerge
	float _timeToLive;
	bool _disappearAtContact;

	public:
	ThingType( const ParamEntry *param );
	~ThingType();

	void Load(const ParamEntry &par);

};

class Thing: public VehicleSupply
{
	protected:
	enum CrashType {CrashNone,CrashLand,CrashWater,CrashObject};
	CrashType _doCrash;
	float _crashVolume;
	// time of last crash sound (will not repeat for some time)
	Time _timeCrash;
	float _submerged; // some thing submerge - see ThingType
	bool _isCloudlet;

	typedef VehicleSupply base;

	public:
	Thing( VehicleType *name );
	
	const ThingType *Type() const
	{
		return static_cast<const ThingType *>(GetType());
	}
	Vector3 Friction( Vector3Par speed );

	void Simulate( float deltaT, SimulationImportance prec );

	float Rigid() const {return 1;} // how much energy is transfered in collision

	void CrashDammage( float ammount, const Vector3 &pos=VZero );

	// building are usually empty
	void Sound( bool inside, float deltaT );
	void UnloadSound();

	void DrawDiags();
	bool QIsManual() const {return false;}

	void SetCloudlet(bool val) {_isCloudlet = val;}

	Matrix4 InsideCamera( CameraType camType ) const;
	int InsideLOD( CameraType camType ) const;
	
	// no get-in to buildings
	bool QCanIGetIn( Person *who = NULL ) const {return false;}

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	/*
	void DrawProxies
	(
		int level, ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights
	);
	*/

	//TargetSide GetTargetSide() const {return TCivilian;}
	USE_CASTING(base)
};

class ThingEffect: public Thing
{
	typedef Thing base;

	public:
	ThingEffect( VehicleType *name );

	USE_CASTING(base)
};

#include <Es/Memory/normalNew.hpp>

class ThingEffectLight: public Vehicle
{
	protected:
	enum CrashType {CrashNone,CrashLand,CrashWater,CrashObject};
	CrashType _doCrash;
	float _crashVolume;
	// time of last crash sound (will not repeat for some time)
	Time _timeCrash;
	float _submerged; // some thing submerge - see ThingType
	float _timeToLive;

	bool _isCloudlet;

	typedef Vehicle base;

	public:
	ThingEffectLight( ThingType *name );


	const ThingType *Type() const
	{
		return static_cast<const ThingType *>(GetNonAIType());
	}
	Vector3 Friction( Vector3Par speed );

	void Simulate( float deltaT, SimulationImportance prec );

	float Rigid() const {return 1;} // how much energy is transfered in collision

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	void SetCloudlet(bool val) {_isCloudlet = val;}

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

enum ThingEffectKind
{
	TEGround,
	TEArmor,
	TEHouse,
	TECartridge, // TODO: right word
	NThingEffectKind
};

//! create thing fo given type - exact class name is randomized
//! also adds it to World and Landscape
Entity *CreateThingEffect
(
	ThingEffectKind kind, // kind
	Matrix4Val pos, Vector3Val vel // position and velocity
);

//! create thing with a known type
//! also adds it to World and Landscape

Entity *CreateThing
(
	VehicleType *type, // kind
	Matrix4Val pos, Vector3Val vel // position and velocity
);

// we also might want to set angular velocity
// but this can be accomplised via AddImpulse()

#endif
