#ifdef _MSC_VER
#pragma once
#endif

#ifndef _DETECTOR_HPP
#define _DETECTOR_HPP

#define LOG_FLAG_CHANGES	0

//#include "house.hpp"
#include "ArcadeWaypoint.hpp"
#include "ai.hpp"
#include "rtAnimation.hpp"

class SoundObject;
class DynSoundObject;
class GameValue;

#define NON_AI_DETECTOR 1
#if NON_AI_DETECTOR
	typedef Vehicle DetectorBase;
#else
	typedef EntityAI DetectorBase;
#endif

class Detector : public DetectorBase
{
	friend class CStaticMapMain; // drawing
protected:
	typedef DetectorBase base;

	float _a;
	float _b;
	float _e;
	float _sinAngle;
	float _cosAngle;
	bool _rectangular;

	ArcadeSensorActivation _activationBy;
	ArcadeSensorActivationType _activationType;
	OLink<AIGroup> _assignedGroup;
	int _assignedStatic;
	int _assignedVehicle;
	bool _repeating;
	bool _interruptable;
	float _timeoutMin;
	float _timeoutMid;
	float _timeoutMax;

	ArcadeSensorType _action;
	RString _text;

	RString _expCond;
	RString _expActiv;
	RString _expDesactiv;

	AutoArray<int> _synchronizations;

	ArcadeEffects _effects;

	Time _nextCheck;
	Time _countdown;
	bool _active;
	bool _activeCountdown;

	//OLinkArray<EntityAI> _vehicles;
	SRef<GameValue> _vehicles;	

	Ref<DynSoundObject> _dynSound;
	Ref<SoundObject> _sound;
	Ref<SoundObject> _voice;

#if NON_AI_DETECTOR
	// TODO: create VehicleWithType
	Ref<const VehicleNonAIType> _type;
	const VehicleNonAIType *GetType() const {return _type;}
#endif

public:
	Detector(EntityType *name, int id);
	~Detector();

	void FromTemplate(const ArcadeSensorInfo &info);
	void AssignGroup(AIGroup *group);
	void AssignStatic(int id);
	void AssignVehicle(int id);
	
	void SetArea(float a, float b, float angle, bool rectangular);
	void SetActivation(ArcadeSensorActivation by, ArcadeSensorActivationType type, bool repeating);
	void SetTriggerType(ArcadeSensorType type);
	void SetTimeout(float min, float mid, float max, bool interruptable);
	void AttachVehicle(EntityAI *vehicle);
	void SetStatements(RString cond, RString activ, RString desactiv);
	void SetSynchronizations(AutoArray<int> synchronizations) {_synchronizations = synchronizations;}
	ArcadeEffects &GetEffects() {return _effects;}

	void Simulate( float deltaT, SimulationImportance prec );
	bool QIsManual()const {return false;}

	TargetSide GetTargetSide() const {return _targetSide;}

	int GetActivationBy() const {return _activationBy;}
	int GetActivationType() const {return _activationType;}
	int GetAction() const {return _action;}
	int NSynchronizations() const {return _synchronizations.Size();}
	int GetSynchronization(int i) const {return _synchronizations[i];}
	float GetCountdown();
	RString GetText() const {return _text;}
	void SetText(RString text) {_text = text;}

	bool IsActive() const {return _active;}
	bool IsCountdown() const {return _activeCountdown;}
	bool IsRepeating() const {return _repeating;}

	Object *GetActiveVehicle();
	int NVehicles() const;
	const EntityAI *GetVehicle(int i) const;
	EntityAI *GetVehicle(int i);

	const GameValue &GetGameValue() const;

	void DoActivate();

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	static Detector *CreateObject(NetworkMessageContext &ctx);
	void DestroyObject();
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

protected:
	bool IsInside(Vector3Par pos, Vector3Par f1, Vector3Par f2);
	bool TestSide(EntityAI *vehicle);
	bool TestSide(const AITargetInfo &target);
	AICenter *GetCenter() const;	// for detected by <center>
	bool TestVehicle(Vehicle *veh, Vector3Par f1, Vector3Par f2);
	bool TestVehicle(AICenter *center, Vehicle *veh, Vector3Par f1, Vector3Par f2);
	void Scan();
	void OnActivate(Object *obj);	// who is activating
	void OnDesactivate();
	
	/*
	const BuildingType *Type() const
	{
		return static_cast<const BuildingType *>(GetType());
	}
	*/

	USE_CASTING(base)
};

class FlagCarrier: public VehicleSupply
{
	typedef VehicleSupply base;

protected:
	Ref<Texture> _flagTexture;
	TargetSide _flagSide;
	OLink<Person> _flagOwner;
	OLink<Person> _flagOwnerWanted;

	bool _down;
	bool _up;
	Time _animStart;
	Ref<AnimationRT> _animation;
	WeightInfo _weights;
	Ref<Skeleton> _skeleton;

	float _phase;
	float _animSpeed;
	float _invAnimSpeed;

public:
	FlagCarrier(VehicleType *type);

	void AnimateMatrix(Matrix4 &mat,int level, int selection) const;
	bool IsAnimated(int level) const {return true;}

	void GetActions(UIActions &actions, AIUnit *unit, bool now);

	Texture *GetFlagTexture();
	Texture *GetFlagTextureInternal();
	void SetFlagTexture(RString name);
	Person *GetFlagOwner() {return _flagOwner;}
	void SetFlagOwner(Person *veh);
	TargetSide GetFlagSide() const;
	void SetFlagSide(TargetSide side);
	
	bool QIsManual() const {return false;}

	void Simulate( float deltaT, SimulationImportance prec );

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

	USE_CASTING(base)
};

#endif
