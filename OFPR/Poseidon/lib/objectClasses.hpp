#ifdef _MSC_VER
#pragma once
#endif

#ifndef _OBJECTCLASSES_HPP
#define _OBJECTCLASSES_HPP

#include <El/ParamFile/paramFile.hpp>

#include "object.hpp"
#include "transport.hpp"
#include "lights.hpp"
#include "house.hpp"

//! type of entity with hitpoint support
class EntityHitType: public EntityType
{
	typedef EntityType base;
	friend class EntityHit;

	protected:
	
	HitPointList _hitPoints;
	float _structuralDammageCoef;

	public:
	EntityHitType( const ParamEntry *param );
	~EntityHitType();

	virtual void Load(const ParamEntry &par);
	virtual void InitShape(); // after shape is loaded
	virtual void DeinitShape(); // before shape is unloaded

	__forceinline float GetStructuralDammageCoef() const {return _structuralDammageCoef;}
	__forceinline const HitPointList &GetHitPoints() const {return _hitPoints;}
	__forceinline HitPointList &GetHitPoints() {return _hitPoints;}

};

//! entity with hitpoint support
class EntityHit: public Entity
{
	typedef Entity base;
	protected:
	AutoArray<float> _hit; //!< Hitpoints (local dammage)

	public:
	EntityHit
	(
		LODShapeWithShadow *shape, const EntityHitType *type, int id
	);
	~EntityHit();
	//! apply hit at given point to hitpoints
	float LocalHit( Vector3Par pos, float val, float valRange );
	//! Get dammage state of given hitpoint (0 or 1)
	float GetHit( const HitPoint &hitpoint ) const;
	//! Get dammage state of given hitpoint (continuos) 
	float GetHitCont( const HitPoint &hitpoint ) const; // used for indication

	void ResetStatus();

	__forceinline const EntityHitType *GetType() const
	{
		return static_cast<const EntityHitType *>(_type.GetRef());
	}
};

class StreetLampType: public EntityHitType
{
	typedef EntityHitType base;
	friend class StreetLamp;

	protected:
	HitPoint _bulbHit;
	
	float _brightness;
	Color _colorDiffuse;
	Color _colorAmbient;

	public:
	StreetLampType( const ParamEntry *param );
	~StreetLampType();

	virtual void Load(const ParamEntry &par);
	virtual void InitShape(); // after shape is loaded
	virtual void DeinitShape(); // before shape is unloaded

};

class StreetLamp: public EntityHit
{
public:
	enum LightState
	{
		LSOff,
		LSOn,
		LSAuto
	};
	typedef EntityHit base;
protected:
	LightState _lightState;
	bool _pilotLight; // switch the light on/off
	Ref<LightPointVisible> _light;
	Vector3 _lightPos;
	
public:
	StreetLamp( LODShapeWithShadow *shape, StreetLampType *type, int id );

	__forceinline const StreetLampType *Type() const
	{
		return static_cast<const StreetLampType *>(_type.GetRef());
	}

	LightState GetLightState() const {return _lightState;}
	void SwitchLight(LightState state);

	void Init( Matrix4Par pos );
	void SimulateSwitch();
	void ResetStatus();
	void OnTimeSkipped();
	void CreateLight(Matrix4Par pos);
	void Simulate( float deltaT, SimulationImportance prec );

	void HitBy( EntityAI *killer, float howMuch, RString ammo );

	LSError Serialize(ParamArchive &ar);
};

class RoadType: public RefCount
{
	friend class Road;
	Ref<LODShapeWithShadow> _shape;

	public:
	const char *GetName() const {return _shape->Name();}

	RoadType();
	RoadType( const char *name );
	~RoadType();
};

TypeIsMovable(RoadType);

class RoadTypeBank: public BankArray<RoadType>
{	
};

extern RoadTypeBank RoadTypes;

#include <Es/Memory/normalNew.hpp>

class Road: public Object
{
	typedef Object base;

	InitPtr<RoadType> _roadType;

	public:
	Road( LODShapeWithShadow *shape, int id );

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate

	float GetArmor() const;
	float GetInvArmor() const;
	float GetLogArmor() const;

	void DrawDiags();

	USE_FAST_ALLOCATOR
	USE_CASTING(base)
};

#define FOREST_PROXY_ENABLE 0

class ForestPlain: public Object
{
	typedef Object base;
	bool _singleMatrixT1; // TODO: different class
	bool _singleMatrixT2; // TODO: different class

	float _skewX,_skewZ,_offsetY;

	public:
	ForestPlain( LODShapeWithShadow *type, int id );

	virtual void InitSkew( Landscape *land ); // call to prepare skew matrix

	Matrix4 GetInvTransform() const;

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	Vector3 AnimatePoint( int level, int index ) const;
	void Deanimate( int level );

	float ViewDensity() const;

	void Draw( int forceLOD, ClipFlags clipFlags, const FrameBase &pos );

	#if !FOREST_PROXY_ENABLE
	// disabled proxies
	void DrawProxies
	(
		int level, ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights
	){}
	virtual int GetProxyComplexity
	(
		int level, const FrameBase &pos, float dist2
	) const {return 0;}

	// proxy access
	virtual int GetProxyCount(int level) const {return 0;}
	#endif


	USE_CASTING(base)

	USE_FAST_ALLOCATOR
};


#define FOREST_PATHS 0

class Forest: public ForestPlain
#if FOREST_PATHS
,public IPaths
#endif
{
	Ref<BuildingType> _type;
	typedef ForestPlain base;
	
	public:
	Forest( BuildingType *type, int id );
	~Forest();

	#if FOREST_PATHS // forest are working without paths much faster
		virtual const BuildingType *GetBType() const {return _type;}
		virtual const IPaths *GetIPaths() const {return this;}
		virtual const Object *GetObject() const {return this;}
	#endif

	USE_FAST_ALLOCATOR
};


#include <Es/Memory/debugNew.hpp>

#endif
