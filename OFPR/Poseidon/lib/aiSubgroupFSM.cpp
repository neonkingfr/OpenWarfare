///////////////////////////////////////////////////////////////////////////////
// Implementation of AISubgroup as an Abstract AI Machine

#include "wpch.hpp"

#pragma warning(disable:4100 4511 4512 4201 4127)

#include "ai.hpp"
#include "aiRadio.hpp"
#include "fsm.hpp"
#include "global.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "person.hpp"
#include "house.hpp"

#include "shots.hpp"
#include "weapons.hpp"
#include "uiActions.hpp"

DEFINE_FAST_ALLOCATOR(AISubgroupFSM)

AISubgroupFSM::AISubgroupFSM
(
	const StateInfo *states, int n,
	const pStateFunction *functions, int nFunc
)
:base(states,n,functions,nFunc)
{
}

AISubgroupFSM::~AISubgroupFSM()
{
}

#define LOST_UNIT_MIN							45.0F
#define LOST_UNIT_MAX							300.0F

#define SUBGROUP_REFRESH					20.0F

///////////////////////////////////////////////////////////////////////////////
// Generic Command Functions

#if !_RELEASE
	#define LOG_FSM 0
#else
	#define LOG_FSM 0
#endif

#define DIAG_ATTACK 0

#include "engine.hpp"

inline void CheckDistance(AIUnit *unit, Vector3Val dest)
{
/*
	if (!unit) return;

	EntityAI *veh = unit->GetVehicle();
	float prec = 3.0 * veh->GetPrecision();
	saturateMax(prec, 10.0);
	if (veh->Position().Distance2(dest) > Square(prec))
	{
		GEngine->ShowMessage
		(
			2000,"%.3f, %s (%s): Far destination completed - distance %.0f m",
			Glob.time.toFloat(), (const char *)unit->GetDebugName(),
			(const char *)veh->GetType()->GetName(), veh->Position().Distance(dest)
		);
		RptF
		(
			"%.3f, %s (%s): Far destination completed",
			Glob.time.toFloat(), (const char *)unit->GetDebugName(),
			(const char *)veh->GetType()->GetName()
		);
		RptF
		(
			"  - position %.0f, %.0f, %.0f", veh->Position().X(), veh->Position().Y(), veh->Position().Z()
		);
		RptF
		(
			"  - destination %.0f, %.0f, %.0f", dest.X(), dest.Y(), dest.Z()
		);
		RptF
		(
			"  - FSM state: %d %s",
			unit->GetSubgroup()->GetCommand()->_message,
			unit->GetSubgroup()->GetCurrent()->_fsm->GetStateName()
		);
	}
*/
}

inline void LogFSM(AISubgroup *subgrp, const char *cmd, const char *state)
{
#if LOG_FSM
	if (subgrp && subgrp->GetGroup())
		LogF
		(
			"FSM: %s %s - state %s, %d tasks",
			(const char *)subgrp->GetDebugName(),
			cmd,
			state, 
			subgrp->StackSize()
		);
#endif
}

#define INIT_PREFIX \
	AISubgroup *subgrp = context->_subgroup; \
	Assert(subgrp); \
	(void)subgrp;

#define STATE_PREFIX \
	AISubgroup *subgrp = context->_subgroup; \
	Assert(subgrp); \
	Command *cmd = context->_task; \
	Assert(cmd); \
	(void)cmd; \
	if (subgrp->NUnits() <= 0) return; \
	Assert(subgrp->Leader()); \
	if (!subgrp->Leader()) return;
/*!
\patch 1.34 Date 12/6/2001 by Ondra
- Fixed: MP: Player character auto reported "Ready" when completed command.
*/

void CommandSucceed(AISubgroupContext *context)
{
	AISubgroup *subgroup = context->_subgroup;
	Assert(subgroup);
	Command *cmd = context->_task;
	Assert(cmd);
	LogFSM(subgroup, "Command", "SUCCEED");

	subgroup->DoNotGo();
	subgroup->ClearPlan();

	AIGroup *grp = subgroup->GetGroup();
	if (subgroup->IsAnyPlayerSubgroup())
	{
		grp->ReceiveAnswer(subgroup,AI::CommandCompleted);
	}
	else
	{
		subgroup->SendAnswer(AI::CommandCompleted);
	}
	if (grp->IsPlayerGroup())
		SetCommandState(cmd->_id, CSSucceed, subgroup);

	if (cmd->_joinToSubgroup && cmd->_joinToSubgroup != subgroup && subgroup != grp->MainSubgroup())
	{
		subgroup->JoinToSubgroup(cmd->_joinToSubgroup);
	}
}

void CheckCommandSucceed(AISubgroupContext *context)
{
	context->_fsm->SetState(FSM::FinalState, context);
}

void CommandFailed(AISubgroupContext *context)
{
	AISubgroup *subgroup = context->_subgroup;
	Assert(subgroup);
	Command *cmd = context->_task;
	Assert(cmd);
	LogFSM(subgroup, "Command", "FAILED");

	subgroup->DoNotGo();
	subgroup->ClearPlan();
	AIGroup *grp = subgroup->GetGroup();
	if (subgroup->IsAnyPlayerSubgroup())
	{
		grp->ReceiveAnswer(subgroup,AI::CommandFailed);
	}
	else
	{
		subgroup->SendAnswer(AI::CommandFailed);
	}
	if (cmd->_id >= 0)
		SetCommandState(cmd->_id, CSFailed, subgroup);

	if (cmd->_joinToSubgroup && cmd->_joinToSubgroup != subgroup && subgroup != grp->MainSubgroup())
	{
		subgroup->JoinToSubgroup(cmd->_joinToSubgroup);
	}
}

void CheckCommandFailed(AISubgroupContext *context)
{
	context->_fsm->SetState(FSM::FinalState, context);
}

///////////////////////////////////////////////////////////////////////////////
// No Command FSM

static void NoCommandWait(AISubgroupContext *context)
{
	if (!context) return;

	AISubgroup *subgrp = context->_subgroup;
	if(!subgrp ) return;

	LogFSM(subgrp, "NoCommand", "WAIT");
}

static void CheckNoCommandWait(AISubgroupContext *context)
{
}

/*
static void NoCommandEnter(AISubgroupContext *context)
{
}

static void NoCommandExit(AISubgroupContext *context)
{
}
*/

static AISubgroupFSM::StateInfo noCommandStates[] =
{
	AISubgroupFSM::StateInfo("Wait", NoCommandWait, CheckNoCommandWait)
};

/*
static AISubgroupFSM::pStateFunction noCommandSpecial[] =
{
	NoCommandEnter,NoCommandExit
};
*/

///////////////////////////////////////////////////////////////////////////////
// Command Wait FSM

enum
{
	SWaitInit,
	SWaitWait,
	SWaitDone,SWaitFail
};

static void WaitInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Wait", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckWaitInit(AISubgroupContext *context)
{
	Assert(context->_subgroup);

	context->_fsm->SetState(SWaitWait, context);
}

static void WaitWait(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Wait", "WAIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckWaitWait(AISubgroupContext *context)
{
	STATE_PREFIX

	if (cmd->_time >= Time(0) && Glob.time > cmd->_time)
		context->_fsm->SetState(SWaitDone, context);
}

static AISubgroupFSM::StateInfo waitStates[] =
{
	AISubgroupFSM::StateInfo("Init", WaitInit, CheckWaitInit),
	AISubgroupFSM::StateInfo("Wait", WaitWait, CheckWaitWait),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};


///////////////////////////////////////////////////////////////////////////////
// Command Attack FSM

enum
{
	SAttackInit,
	SAttackFarMove,SAttackMove,
	SAttackAttack,
	SAttackWait,SAttackRunAway,
	SAttackDone,SAttackFail
};

// TODO: correct handling of cmd->_target
// use always _targetE

static Target *GetAttackTarget( const Command *cmd, AIGroup *group )
{
	Target *tgt = cmd->_targetE;
	if (!tgt) tgt = group->FindTarget(cmd->_target);
	if (!tgt) return tgt;
	if (tgt->destroyed) return NULL;
	return tgt;
}

static bool IsAttackTarget( const Command *cmd, AIGroup *group )
{	
	Target *tgt = cmd->_targetE;
	if (tgt)
	{
		return !tgt->destroyed;
	}
	return cmd->_target!=NULL;
}

static bool AttackReplan(AISubgroupContext *context)
{
	AISubgroup *subgrp = context->_subgroup;
	Assert(subgrp);
	Command *cmd = context->_task;
	Assert(cmd);

	EntityAI *lVehicle=subgrp->Leader()->GetVehicle();
	Target *tgt = GetAttackTarget(cmd,subgrp->GetGroup());

	// call BeginAttack to be sure vehicle is engaged
	if( tgt ) lVehicle->BegAttack(tgt);

	Vector3 attackPos;
	FireResult result;

	#if DIAG_ATTACK
	LogF("AttackThink");
	#endif
	if( lVehicle->AttackThink(result,attackPos) )
	{
		if( attackPos.Distance2(cmd->_destination)>Square(5) )
		{
			if( attackPos.SquareSize()<1 )
			{
				Fail("Bad attack pos");
			}
			// Refreshing when attack position changed
			subgrp->GoPlanned(attackPos);
		}
		return true;
	}
	return false;
}

static bool AttackReplanIfLost(AISubgroupContext *context)
{
	AISubgroup *subgrp = context->_subgroup;
	Assert(subgrp);
	Command *cmd = context->_task;
	Assert(cmd);

	//AIUnit *leader = subgrp->Leader();

	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( !tgt->IsKnown() )
	{
		Vector3 attackPos=tgt->AimingPosition();
		if( attackPos.Distance2(cmd->_destination)>Square(5) )
		{
			if( attackPos.SquareSize()<1 )
			{
				Fail("Bad attack pos");
			}
			// Refreshing when attack position changed
			subgrp->GoPlanned(attackPos);
		}
		return true;
	}
	return false;
}

static bool CanAttack( AISubgroup *subgrp, Target &tar )
{
	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (!unit) continue;
		EntityAI *veh;
		if (unit->IsInCargo()) veh = unit->GetPerson();
		else if (unit->IsUnit()) veh = unit->GetVehicle();
		else continue;

		FireResult result;
		float bestDist, minRange, maxRange;
		bool potential = veh->BestFireResult
		(
			result, tar, bestDist, minRange, maxRange, 30, true
		);
		if (potential)
		{
			#if DIAG_ATTACK
			LogF("CanAttack true");
			#endif
			return true;
		}
	}
	#if DIAG_ATTACK
	LogF("CanAttack false");
	#endif
	return false;
}

static void TryAttack
(
	bool &somePossible, bool &somePotential, AISubgroup *subgrp, Target &tar
)
{
	somePossible=somePotential=false;
	AIUnit *leader = subgrp->Leader();
	EntityAI *lVehicle = leader->GetVehicle();
	Shot *shot = dyn_cast<Shot>(lVehicle->GetLastShot());
	if (shot)
	{
		// if we have already placed a bomb, wait until it explodes
		switch (shot->Type()->_simulation)
		{
			case AmmoShotPipeBomb:
			case AmmoShotTimeBomb:
				// no fire possible until bomb exploded
				return;
		}
	}
	//Assert( subgrp->HasAI() );
	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (!unit) continue;
		FireResult result;
		if (unit->IsUnit())
		{
			EntityAI *veh=unit->GetVehicle();
			bool possible=veh->WhatFireResult(result, tar, 30.0);
			if( possible )
			{
				somePossible = true;
				somePotential = true;
			}
			else
			{
				float bestDist,minRange,maxRange;
				if( veh->BestFireResult(result, tar, bestDist, minRange, maxRange,30, true) )
				{
					somePotential = true;
				}
			}
		}
		else if( unit->IsInCargo() )
		{
			// unit may be cargo
			float bestDist,minRange,maxRange;
			EntityAI *veh=unit->GetPerson();
			if( veh->BestFireResult(result, tar, bestDist, minRange, maxRange,30, true) )
			{
				somePotential = true;
			}
		}
	}
	#if DIAG_ATTACK
	LogF("TryAttack %d %d",somePossible,somePotential);
	#endif
}


static void AttackInit(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	AIUnit *leader = subgrp->Leader();
	cmd->_destination = leader->Position();

	LogFSM(subgrp, "Attack", "INIT");

	subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
	subgrp->DoNotGo();
	if( subgrp!=subgrp->GetGroup()->MainSubgroup() )
	{
		//subgroup->GetGroup()->SendFormation();
		subgrp->SetFormation(AI::FormLine);
	}
}

static void CheckAttackInit(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp->GetGroup()))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}
	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( tgt && !CanAttack(subgrp,*tgt) )
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackWait, context);
		return;
	}
	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SAttackAttack, context);
		subgrp->ClearPlan();
		return;
	}
	else
	{
		if( tgt )
		{
			subgrp->Leader()->GetVehicle()->BegAttack(tgt);
			#if DIAG_ATTACK
				LogF("Engage - attack command.");
			#endif
		}
		else
		{
			const AITargetInfo *info = NULL;
			AIGroup *grp = subgrp->GetGroup();
			AICenter *center = grp ? grp->GetCenter() : NULL;
			if (center)
			{
				info = center->FindTargetInfo(cmd->_target);
			}
			if (info)
			{
				cmd->_destination = info->_realPos;
			}
			else
			{
				subgrp->Leader()->GetVehicle()->EndAttack();
				context->_fsm->SetState(SAttackFail,context);	// target lost
				subgrp->ClearPlan();
				return;
			}
		}

		context->_fsm->SetState(SAttackFarMove, context);
		subgrp->ClearPlan();
		return;
	}
}

static void AttackFarMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Attack", "FARMOVE");

	subgrp->SetRefreshTime(Glob.time + 180); // no refresh

	subgrp->GoPlanned(cmd->_destination);
	context->_fsm->SetTimeOut(30);

	Vector3Val posL = subgrp->Leader()->Position();
	Vector3Val posD = cmd->_destination;
	float dist = 300;
	if ((posL - posD).SquareSizeXZ() > Square(dist))
	{
		for (int i=0; i<subgrp->NUnits(); i++)
		{
			AIUnit *unit = subgrp->GetUnit(i);
			if (unit)
				unit->OrderGetIn(true);
		}
	}
}


static void CheckAttackFarMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp->GetGroup()))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}
	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( tgt && !CanAttack(subgrp,*tgt) )
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackWait, context);
		return;
	}
	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SAttackAttack, context);	// Attack
		return;
	}

	// refreshing when target position changed
	if (!AttackReplan(context))
	{
		// try to find some attack position
		// timeout when no attack possible
		if( context->_fsm->TimedOut() )
		{
			#if DIAG_ATTACK
			LogF("Far Move Replan Failed.");
			#endif
			context->_fsm->SetState(SAttackWait, context);
		}
		//return;
	}
	EntityAI *lai=subgrp->Leader()->GetVehicle();
	Vector3Val posL = lai->Position();
	Vector3Val posD = cmd->_destination;
	float dist = 200;
	// Precision is FormationX()*0.25 by default
	// for tank it is 5 m
	saturateMax(dist, lai->GetPrecision()*10);
	saturateMin(dist, 1000);

	#if LOG_FSM
	if (subgrp && subgrp->GetGroup())
	LogF
	(
		"FSM: %s %s - state %s, dist %.1f posL %.1f,%.1f,%.1f posD %.1f,%.1f,%.1f",
		(const char *)subgrp->GetDebugName(),
		"ATTACK","MOVE",
		(posL - posD).SizeXZ(),
		posL[0],posL[1],posL[2], posD[0],posD[1],posD[2]
	);
	#endif

	if ((posL - posD).SquareSizeXZ() <= Square(dist) )
	{
		context->_fsm->SetState(SAttackMove, context);
		return;
	}

	if (subgrp->HasAI() && subgrp->AllUnitsCompleted())
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);

		LogFSM(subgrp,"Attack","far move refresh");
		context->_fsm->Refresh(context);
		return;
	}
}

static void AttackMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Attack", "MOVE");

	// near attack position - get out from cargo
	AIGroup *grp=subgrp->GetGroup();

	PackedBoolArray list;
	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (!unit) continue;
		if (!unit->IsInCargo()) continue;
		Transport *veh = unit->GetVehicleIn();
		if (!veh) continue;
		AIUnit *driver = veh->DriverBrain();
		if (!driver || !driver->IsPlayer())
		{
			Target *tgt=GetAttackTarget(cmd,grp);
			if( tgt ) unit->AssignTarget(tgt);
			unit->OrderGetIn(false);
		}
	}

//	subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination);
	context->_fsm->SetTimeOut(30);
}

// how long should we wait when we have no ammo

const float PosTimeOut=5;

static void CheckAttackMove(AISubgroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = subgrp->Leader();
	if (!IsAttackTarget(cmd,subgrp->GetGroup()))
	{
		leader->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}
	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SAttackAttack, context);	// Attack
		subgrp->ClearPlan();
		return;
	}

	#if DIAG_ATTACK
	LogF("FindTarget");
	#endif
	Target *tar=GetAttackTarget(cmd,subgrp->GetGroup());
	if( tar )
	{
		#if DIAG_ATTACK
		LogF("FindTarget OK");
		#endif
		bool somePossible,somePotential;
		TryAttack(somePossible,somePotential,subgrp, *tar);
		if( !somePotential )
		{
			context->_fsm->SetState(SAttackWait, context);
			return;
		}
	}

	// update attack position if neccessary
	if (!AttackReplan(context))
	{
		// try to find some attack position
		// timeout when no attack possible
		if( context->_fsm->TimedOut() )
		{
			#if DIAG_ATTACK
			LogF("Move Replan Failed.");
			#endif
			context->_fsm->SetState(SAttackWait, context);
		}
		return;
	}

	EntityAI *lai=subgrp->Leader()->GetVehicle();
	Vector3Val posL = lai->Position();
	Vector3Val posD = cmd->_destination;
	if ((posL - posD).SquareSizeXZ() <= Square(lai->GetPrecision()*2))
	{
		if( tar && tar->IsKnown() )
		{
			// check if we have something planned
			EntityAI *lVehicle=subgrp->Leader()->GetVehicle();	
			if (lVehicle->AttackReady())
			{
				// target seen recently  - and path is planned
				#if DIAG_ATTACK
				LogF("Target seen - Attack");
				#endif
				context->_fsm->SetState(SAttackAttack, context);
			}
		}
		else
		{
			// target not seen, we are in position - fail
			context->_fsm->SetState(SAttackWait, context);
		}
		//subgrp->ClearPlan();
		return;
	}

	#if LOG_FSM
	if (subgrp && subgrp->GetGroup())
	LogF
	(
		"FSM: %s %s - state %s, dist %.1f posL %.1f,%.1f,%.1f posD %.1f,%.1f,%.1f",
		(const char *)subgrp->GetDebugName(),
		"ATTACK","MOVE",
		(posL - posD).SizeXZ(),
		posL[0],posL[1],posL[2], posD[0],posD[1],posD[2]
	);
	#endif
	if (subgrp->HasAI() && subgrp->AllUnitsCompleted())
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);

		LogFSM(subgrp,"Attack","move refresh");
		context->_fsm->Refresh(context);
		return;
	}
}


static void AttackAttack(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Attack", "ATTACK");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();

	context->_fsm->SetTimeOut(10);
	// unit in position - place bomb if neccessary

}

static void CheckAttackAttack(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp->GetGroup()))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}

	//EntityAI *lVehicle=subgrp->Leader()->GetVehicle();	
	#if DIAG_ATTACK
		LogF("Check fire %.1f,%.1f",lVehicle->Position().X(),lVehicle->Position().Z());
	#endif

	Target *tar=GetAttackTarget(cmd,subgrp->GetGroup());
	if (tar)
	{
		if( !tar->IsKnown() )
		{
			// target has not been seen for some time by any unit
			if (subgrp->HasAI())
			{
				// target disappeared - move to its last known position
				// check AI Center database

				AIGroup *grp = subgrp->GetGroup();
				AICenter *center = grp ? grp->GetCenter() : NULL;
				if (center)
				{
					const AITargetInfo *info = center->FindTargetInfo(cmd->_target);
					if (info)
					{
						cmd->_destination = info->_realPos;
					}
				}

				context->_fsm->SetState(SAttackMove, context);
				return;			
			}
			else
			{ // non-AI attack - target not known
				// if target is not seen for long time, fail
				if( tar->lastSeen<Glob.time-60 )
				{
					context->_fsm->SetState(SAttackWait, context);
					return;
				}
			}

		}
		bool somePossible=false,somePotential=false;
		TryAttack(somePossible,somePotential,subgrp,*tar);
		if (!somePossible)
		{
			if( !somePotential )
			{
				// wait until last shot is gone
				subgrp->Leader()->GetVehicle()->EndAttack();
				LogFSM(subgrp,"Attack","Attack fail");
				context->_fsm->SetState(SAttackWait, context); // Fail
				subgrp->ClearPlan();
				return;
			}
			if( subgrp->HasAI() )
			{
				// fire not possible
				if( context->_fsm->TimedOut() )
				{
					LogFSM(subgrp, "Attack", "Retry - no fire possible.");
					context->_fsm->SetState(SAttackInit, context); // Retry
					//subgrp->ClearPlan();
					return;
				}
			}
		}
	}
	else
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		LogFSM(subgrp, "Attack", "Failed - target out of range.");
		context->_fsm->SetState(SAttackWait, context); // Fail
		//subgrp->ClearPlan();
		return;
	}
}

static void AttackWait(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;
}

static void CheckAttackWait(AISubgroupContext *context)
{
	STATE_PREFIX

	// TODO: if we fired bomb, we should go away quickly
	// check if target is destroyed
	if (!IsAttackTarget(cmd,subgrp->GetGroup()))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}

	// wait until last shot at target is gone
	EntityAI *lVehicle = subgrp->Leader()->GetVehicle();
	Vehicle *lastShot = lVehicle->GetLastShot();
	if (subgrp->HasAI())
	{
		// check for a special case - last shot is mine or bomb
		Shot *shot = dyn_cast<Shot>(lastShot);
		if (shot)
		{
			switch (shot->Type()->_simulation)
			{
				case AmmoShotMine:
				case AmmoShotPipeBomb:
				case AmmoShotTimeBomb:
					context->_fsm->SetState(SAttackRunAway, context);	// failed
					subgrp->ClearPlan();
					return;
			}
		}
	}
	if
	(
		lVehicle->GetLastShotAtAssignedTarget()<Glob.time-20
	)
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackFail, context);	// failed
		subgrp->ClearPlan();
		return;
	}
	
}

static void AttackRunAway(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *lVehicle = leader->GetVehicle();
	// run away from target
	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( tgt )
	{

		Vector3 tgtPos = tgt->LandAimingPosition();
		Vector3 pos = lVehicle->Position();
		AIUnit *gLeader = subgrp->GetGroup()->Leader();
		if (gLeader!=leader)
		{
			// prefer running in group leader direction
			pos = gLeader->Position();
		}
		else
		{
			// TODO: if leader is me, select some safe place
			// (perhaps direction to first waypoint
		}
		Vector3 dir = pos-tgtPos;
		dir.Normalize();

		Vector3 posD = tgtPos+dir*100;
		leader->FindNearestEmpty(posD);
	
		subgrp->SetRefreshTime(Glob.time + 60);
		subgrp->GoPlanned(posD);
	}
}

static void CheckAttackRunAway(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackFail, context);	// failed
		subgrp->ClearPlan();
		return;
	}
	// wait until leader is at given position

	if (!IsAttackTarget(cmd,subgrp->GetGroup()))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}

	AIUnit *leader = subgrp->Leader();
	EntityAI *lVehicle=leader->GetVehicle();

	if (subgrp->AllUnitsCompleted())
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);

		Vehicle *lastShot = lVehicle->GetLastShot();
		// TODO: if pipe bomb - fire touch off
		Shot *shot = dyn_cast<Shot>(lastShot);
		if (shot)
		{
			if (shot->Type()->_simulation==AmmoShotPipeBomb)
			{
				// find touch off
				UIAction action;
				action.type = ATTouchOff;
				// other params not used:
				action.target = lVehicle;
				action.param = 0;
				action.param2 = 0;
				action.hideOnUse = true;
				action.showWindow = false;
				action.priority = 0;
				lVehicle->StartActionProcessing(action,leader);
			}
		}

		// start over again
		context->_fsm->SetState(SAttackInit, context);
		subgrp->ClearPlan();
		return;
	}
}

static AISubgroupFSM::StateInfo attackStates[] =
{
	AISubgroupFSM::StateInfo("Init", AttackInit, CheckAttackInit),
	AISubgroupFSM::StateInfo("FarMove", AttackFarMove, CheckAttackFarMove),
	AISubgroupFSM::StateInfo("Move", AttackMove, CheckAttackMove),
	AISubgroupFSM::StateInfo("Attack", AttackAttack, CheckAttackAttack),
	AISubgroupFSM::StateInfo("Wait", AttackWait, CheckAttackWait),
	AISubgroupFSM::StateInfo("RunAway", AttackRunAway, CheckAttackRunAway),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

/*!
\patch 1.53 Date 3/8/2002 by Ondra
- Fixed: AI units when both Engage at will and Hold fire were given.
*/

static void AttackEnterEnableFire(AISubgroupContext *context, bool fire)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( !tgt ) return;

	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (unit)
		{
			unit->AssignTarget(tgt);
			if (fire) unit->EnableFireTarget(tgt);
		}
	}
}

static void AttackExitEnableFire(AISubgroupContext *context, bool fire)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( !tgt ) return;

	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (unit)
		{
			unit->AssignTarget(NULL);
			if (fire) unit->EnableFireTarget(NULL);
		}
	}
}

static void AttackFireEnter(AISubgroupContext *context)
{
	AttackEnterEnableFire(context,true);
}
static void AttackFireExit(AISubgroupContext *context)
{
	AttackExitEnableFire(context,true);
}

static void AttackEnter(AISubgroupContext *context)
{
	AttackEnterEnableFire(context,false);
}

static void AttackExit(AISubgroupContext *context)
{
	AttackExitEnableFire(context,false);
}

static AISubgroupFSM::pStateFunction attackSpecial[] =
{
	AttackEnter,
	AttackExit
};

static AISubgroupFSM::pStateFunction attackFireSpecial[] =
{
	AttackFireEnter,
	AttackFireExit
};

///////////////////////////////////////////////////////////////////////////////
// Command Hide FSM

enum
{
	SHideInit,
	SHideFail
};

static void HideInit(AISubgroupContext *context)
{
}

static void CheckHideInit(AISubgroupContext *context)
{
	STATE_PREFIX

	EntityAI *lVehicle=subgrp->Leader()->GetVehicle();
	lVehicle->HideThink();
}

static void HideEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if( !unit ) continue;
		if( !unit->IsUnit() ) continue;
		unit->GetVehicle()->BegHide();
	}
}

static void HideExit(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if( !unit ) continue;
		if( !unit->IsUnit() ) continue;
		unit->GetVehicle()->EndHide();
	}
}


static AISubgroupFSM::StateInfo hideStates[] =
{
	AISubgroupFSM::StateInfo("Init", HideInit, CheckHideInit),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};


static AISubgroupFSM::pStateFunction hideSpecial[] =
{
	HideEnter,
	HideExit
};

///////////////////////////////////////////////////////////////////////////////
// Command Fire FSM

enum
{
	SFireInit,
	SFireFire,
	SFireDone,SFireFail
};

static void FireInit(AISubgroupContext *context)
{
	INIT_PREFIX
	Command *cmd = context->_task; \
	Assert(cmd); \
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Fire", "INIT");

	subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
	subgrp->DoNotGo();

	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (!unit || !unit->IsUnit()) 
			continue;
		Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
		if( tgt ) unit->AssignTarget(tgt);
	}
}

static void CheckFireInit(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp->GetGroup()))
	{
		context->_fsm->SetState(SFireDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}
	context->_fsm->SetState(SFireFire, context);
	subgrp->ClearPlan();
	return;
}

static void FireFire(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Fire", "FIRE");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckFireFire(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp->GetGroup()))
	{
		context->_fsm->SetState(SFireDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}

	Target *tar=GetAttackTarget(cmd,subgrp->GetGroup());
	bool somePossible = false;
	if (tar)
	{
		if (!subgrp->HasAI())
		{
			// check out of ammo
			bool outOfAmmo=true;
			for (int u=0; u<subgrp->NUnits(); u++)
			{
				AIUnit *unit = subgrp->GetUnit(u);
				if (!unit || !unit->IsUnit()) 
					continue;
				EntityAI *vehicle=unit->GetVehicle();
				if( cmd->_param>=0 )
				{
					const Magazine *magazine = vehicle->GetMagazineSlot(cmd->_param)._magazine;
					if (magazine && magazine->_ammo>0 ) outOfAmmo=false;
				}
				else
				{
					for (int w=0; w<vehicle->NMagazineSlots(); w++)
					{
						const Magazine *magazine = vehicle->GetMagazineSlot(w)._magazine;
						if (magazine && magazine->_ammo>0 ) outOfAmmo=false;
					}
				}
			}
			if( outOfAmmo )
			{
				context->_fsm->SetState(SFireFail, context); // Fail
				subgrp->ClearPlan();
			}
			return;
		}
		for (int u=0; u<subgrp->NUnits(); u++)
		{
			AIUnit *unit = subgrp->GetUnit(u);
			if (!unit || !unit->IsUnit()) 
				continue;
			if( cmd->_param>=0  )
			{
				EntityAI *vehicle=unit->GetVehicle();
				const Magazine *magazine = vehicle->GetMagazineSlot(cmd->_param)._magazine;
				if (magazine && magazine->_ammo>0 )
				{
					//vehicle->FireAt(cmd->_param, *tar);
					somePossible = true;
				}
			}
			else
			{
				FireResult result;
				bool possible=unit->GetVehicle()->WhatFireResult(result, *tar, 30.0);
				if( possible )
				{
					somePossible = true;
				}
			}
		}
		if (!somePossible)
		{
			// no unit can fire - fail
			LogFSM(subgrp, "Fire", "Failed - no fire possible.");
			context->_fsm->SetState(SFireFail, context); // Fail
			subgrp->ClearPlan();
			return;
		}
	}
	else
	{
		LogFSM(subgrp, "Fire", "Failed - target out of range.");
		context->_fsm->SetState(SFireFail, context); // Fail
		subgrp->ClearPlan();
		return;
	}
}

static AISubgroupFSM::StateInfo fireStates[] =
{
	AISubgroupFSM::StateInfo("Init", FireInit, CheckFireInit),
	AISubgroupFSM::StateInfo("Fire", FireFire, CheckFireFire),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Command Move FSM

enum
{
	SMoveInit,SMoveMove,SMoveDone,SMoveFail
};

static void MoveInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Move", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckMoveInit(AISubgroupContext *context)
{
	STATE_PREFIX
	
	context->_fsm->SetState(SMoveMove, context);
	subgrp->ClearPlan();
}

static void MoveMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Move", "MOVE");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination);

	Vector3Val posL = subgrp->Leader()->Position();
	Vector3Val posD = cmd->_destination;
	float dist = 200;
	if ((posL - posD).SquareSizeXZ() > Square(dist))
	{
		for (int i=0; i<subgrp->NUnits(); i++)
		{
			AIUnit *unit = subgrp->GetUnit(i);
			if (unit)
				unit->OrderGetIn(true);
		}
	}
}

static void CheckMoveMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		Vector3Val posL = subgrp->Leader()->Position();
		Vector3Val posD = cmd->_destination;
		float maxDistance=10;
		if( subgrp->Leader()->IsFreeSoldier() ) maxDistance=2.5;
		if ((posL - posD).SquareSizeXZ() <= Square(maxDistance))
		{
			context->_fsm->SetState(SMoveDone, context);
			subgrp->ClearPlan();
		}
		return;
	}

	Assert(subgrp->GetMode() == AISubgroup::PlanAndGo);
	if (subgrp->AllUnitsCompleted())
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);

		context->_fsm->SetState(SMoveDone, context);
		subgrp->ClearPlan();
	}
}

static AISubgroupFSM::StateInfo moveStates[] =
{
	AISubgroupFSM::StateInfo("Init", MoveInit, CheckMoveInit),
	AISubgroupFSM::StateInfo("Move", MoveMove, CheckMoveMove),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Command Stop FSM

enum
{
	SStopInit,SStopStop,SStopDone,SStopFail
};

static void StopEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	// set leader direction

	subgrp->DoNotGo();
}

static void StopExit(AISubgroupContext *context)
{
	STATE_PREFIX
}

static void StopInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Stop", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckStopInit(AISubgroupContext *context)
{
	STATE_PREFIX
	
	context->_fsm->SetState(SStopStop, context);
	subgrp->ClearPlan();
}

static void StopStop(AISubgroupContext *context)
{
	// do nothing
}

static void CheckStopStop(AISubgroupContext *context)
{
}


static AISubgroupFSM::StateInfo stopStates[] =
{
	AISubgroupFSM::StateInfo("Init", StopInit, CheckStopInit),
	AISubgroupFSM::StateInfo("Stop", StopStop, CheckStopStop),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Command Expect FSM

enum
{
	SExpectInit,SExpectExpect,SExpectDone,SExpectFail
};

static void ExpectEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	// set leader direction

	subgrp->DoNotGo();
}

static void ExpectExit(AISubgroupContext *context)
{
	STATE_PREFIX
}

static void ExpectInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Expect", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckExpectInit(AISubgroupContext *context)
{
	STATE_PREFIX
	
	context->_fsm->SetState(SExpectExpect, context);
	subgrp->ClearPlan();
}

static void ExpectExpect(AISubgroupContext *context)
{
	// do nothing
}

static void CheckExpectExpect(AISubgroupContext *context)
{
	// check if we are in formation

}


static AISubgroupFSM::StateInfo expectStates[] =
{
	AISubgroupFSM::StateInfo("Init", ExpectInit, CheckExpectInit),
	AISubgroupFSM::StateInfo("Expect", ExpectExpect, CheckExpectExpect),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Commands Heal, Repair, Refuel, Rearm, TakeWeapon, TakeMagazine FSM

static Vector3 GetSupplyPoint(VehicleSupply *veh)
{
	const VehicleType *type = veh->GetType();
	return veh->PositionModelToWorld
	(
		type->GetSupplyPoint()
	);
}

static Vector3 GetActionPoint(VehicleSupply *veh, const Command *cmd)
{
	switch (cmd->_action)
	{
		case ATLadderOnUp:
		case ATLadderOnDown:
		case ATLadderUp:
		case ATLadderDown:
		{
			// check corresponding ladder point
			Building *house = dyn_cast<Building>(veh);
			if (!house) break;
			Vector3 pos = house->GetLadderPos(cmd->_param,cmd->_param2!=0);
			return pos;
		}
		break;
	}
	return GetSupplyPoint(veh);
}

static Vector3 GetSupplyPointFar(VehicleSupply *veh)
{
	const VehicleType *type = veh->GetType();
	float distance = veh->GetRadius() * 1.4 + 3.5;
	Vector3 offset = type->GetSupplyPoint().Normalized() * distance;
	Vector3 res = veh->PositionModelToWorld(offset);
	res[1] = GLandscape->RoadSurfaceYAboveWater(res[0],res[2]);

	return res;
}


static Vector3 GetActionPointFar(VehicleSupply *veh, const Command *cmd)
{
	Vector3 res = GetActionPoint(veh,cmd);
	Vector3 pos = veh->Position();

	float distance = veh->GetRadius() * 1.4 + 3.5;
	Vector3 norm = res-pos;
	res = norm.Normalized() * distance + pos;

	res[1] = GLandscape->RoadSurfaceYAboveWater(res[0],res[2]);
	return res;
}


enum SupplyFSMStates
{
	SSupplyInit,
	SSupplyAlloc,
	SSupplyMove,
	SSupplyDirect,
	SSupplySupply,
	SSupplySucceed,
	SSupplyFail
};

static void ReportStatus(AIUnit *unit,Command::Message msg)
{
	AIGroup *grp = unit->GetGroup();
	if (!grp) return;
	switch (msg)
	{
		case Command::Heal:
			grp->SetHealthStateReported(unit,AIUnit::RSNormal);
			break;
		case Command::Rearm:
			grp->SetAmmoStateReported(unit,AIUnit::RSNormal);
			break;
		case Command::Repair:
			grp->SetDammageStateReported(unit,AIUnit::RSNormal);
			break;
		case Command::Refuel:
			grp->SetFuelStateReported(unit,AIUnit::RSNormal);
			break;
	}
}

static Command::Message ActionToCommand(UIActionType action)
{
	switch (action)
	{
		case ATHeal: return Command::Heal;
		case ATRepair: return  Command::Repair;
		case ATRefuel: return  Command::Refuel;
		case ATRearm: return  Command::Rearm;
//		case ATTakeWeapon: return  Command::Rearm;
//		case ATTakeMagazine: return  Command::Rearm;
		default: return Command::Action;
	}
}

/*
static void ReportStatus(AIUnit *unit,UIActionType action)
{
	Command::Message msg = ActionToCommand(action);
	ReportStatus(unit,msg);
}
*/

// 
static bool CheckAbleToRearm(EntityAI *veh,VehicleSupply *transport)
{
	// adapted from Man::Supply, action == ATRearm
	Person *man = dyn_cast<Person>(veh);

	// check which magazines we need
	const MuzzleType *muzzle1 = NULL;
	const MuzzleType *muzzle2 = NULL;
	int slots1 = 0, slots2 = 0, slots3 = 0;
	man->CheckAmmo(muzzle1, muzzle2, slots1, slots2, slots3);
	for (int i=0; i<transport->NMagazines();)
	{
		Ref<const Magazine> magazine = transport->GetMagazine(i);
		if (!magazine || magazine->_ammo == 0)
		{
			i++;
			continue;
		}
		if (transport->IsMagazineUsed(magazine))
		{
			i++;
			continue;
		}
		// check if magazine can be used
		const MagazineType *type = magazine->_type;
		int slots = GetItemSlotsCount(type->_magazineType);

		bool add = false;
		if (muzzle1 && muzzle1->CanUse(type))
		{
			if (slots <= slots1)
			{
				slots1 -= slots;
				add = true;
			}
		}
		else if (muzzle2 && muzzle2->CanUse(type))
		{
			if (slots <= slots2)
			{
				slots2 -= slots;
				add = true;
			}
		}
		else if (man->IsMagazineUsable(type))
		{
			if (slots <= slots3)
			{
				slots3 -= slots;
				add = true;
			}
		}
		if (add)
		{
			// we are able to take magazine
			return true;
		}
		else i++;
	}
	return false;
}

/*!
\patch 1.34 Date 10/22/2001 by Ondra.
- Fixed: When AI soldiers wanted to take riffle ammo
but there was no suitable avaiable, they looped kneeling and standing forever.
*/

static bool CheckSupplyDone(AISubgroupContext *context)
{
	AISubgroup *subgrp = context->_subgroup;
	Command *cmd = context->_task;

	EntityAI *veh = subgrp->Leader()->GetVehicle();
	Assert(veh);


	#define UNALLOC() if( transport->GetAllocSupply()==veh ) transport->SetAllocSupply(NULL);
	VehicleSupply *transport = dyn_cast<VehicleSupply,Object>(cmd->_target);
	if (Glob.time > cmd->_time)
	{ // timeout
		context->_fsm->SetState(SSupplyFail, context);
		subgrp->ClearPlan();
		UNALLOC(); // unalloc supply
		return true;
	}

	// wait until we are fully filled or source is exhausted
//	if (!transport || transport->GetTotalDammage() >= 1.0)
	if (!transport)
	{
		// unalloc supply
		context->_fsm->SetState(SSupplyFail, context);
		if( transport ) UNALLOC(); // unalloc supply
		return true;
	}

	Command::Message msg = cmd->_message;
	if (msg == Command::Action) msg = ActionToCommand(cmd->_action);

	switch (msg)
	{
		case Command::Heal:
			if (veh->NeedsAmbulance() <= 0.01)
			{
				context->_fsm->SetState(SSupplySucceed, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(),cmd->_message);
				return true;
			}
			if (!transport->GetType()->IsAttendant() || transport->IsDammageDestroyed())
			{
				context->_fsm->SetState(SSupplyFail, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(),cmd->_message);
				return true;
			}
			return false;
		case Command::Repair:
			if (veh->NeedsRepair() <= 0.05)
			{
				context->_fsm->SetState(SSupplySucceed, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(),cmd->_message);
				return true;
			}
			if (transport->GetRepairCargo() <= 10 || transport->IsDammageDestroyed())
			{
				context->_fsm->SetState(SSupplyFail, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(),cmd->_message);
				return true;
			}
			return false;
		case Command::Refuel:
			if (veh->NeedsRefuel() <= 0.01)
			{
				context->_fsm->SetState(SSupplySucceed, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(),cmd->_message);
				return true;
			}
			if (transport->GetFuelCargo() <= 10 || transport->IsDammageDestroyed())
			{
				context->_fsm->SetState(SSupplyFail, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(),cmd->_message);
				return true;
			}
			return false;
		case Command::Rearm:
			if (subgrp->Leader()->IsFreeSoldier())
			{
				// check if we are able to take something from the supply
				// TODO: improve conditions
				if 
				(
					veh->NeedsInfantryRearm() <= 0.01 ||
					!CheckAbleToRearm(veh,transport)
				)
				{
					context->_fsm->SetState(SSupplySucceed, context);
					UNALLOC(); // unalloc supply
					ReportStatus(subgrp->Leader(),cmd->_message);
					return true;
				}
			}
			else
			{
				if (veh->NeedsRearm() <= 0.01)
				{
					context->_fsm->SetState(SSupplySucceed, context);
					UNALLOC(); // unalloc supply
					ReportStatus(subgrp->Leader(),cmd->_message);
					return true;
				}
				if
				(
					transport->GetAmmoCargo() <= 10 || transport->IsDammageDestroyed()
				)
				{
					context->_fsm->SetState(SSupplyFail, context);
					UNALLOC(); // unalloc supply
					ReportStatus(subgrp->Leader(),cmd->_message);
					return true;
				}
			}
			return false;
/*
		case Command::TakeWeapon:
			{
				Ref<WeaponType> weapon = WeaponTypes.New(cmd->_param3);
				for (int i=0; i<veh->NWeaponSystems(); i++)
				{
					if (veh->GetWeaponSystem(i) == weapon)
					{
						context->_fsm->SetState(SSupplySucceed, context);
						UNALLOC(); // unalloc supply
						return true;
					}
				}
			}
			return false;
		case Command::TakeMagazine:
			{
				int creator = cmd->_param;
				int id = cmd->_param2;
				for (int i=0; i<veh->NMagazines(); i++)
				{
					const Magazine *magazine = veh->GetMagazine(i);
					if (magazine->_creator == creator && magazine->_id == id)
					{
						context->_fsm->SetState(SSupplySucceed, context);
						UNALLOC(); // unalloc supply
						return true;
					}
				}
			}
			return false;
*/
		case Command::Action:
			return false;
		default:
			Fail("Command");
			context->_fsm->SetState(SSupplyFail, context);
			UNALLOC(); // unalloc supply
			return true;
	}
}

static bool SupplyDoneImmediatelly(AISubgroupContext *context)
{
	Command *cmd = context->_task;
	Command::Message msg = cmd->_message;
	if (msg == Command::Action) msg = ActionToCommand(cmd->_action);

	switch (msg)
	{
		case Command::Heal:
		case Command::Refuel:
		case Command::Repair:
			return false;
		case Command::Rearm:
			{
				AISubgroup *subgrp = context->_subgroup;
				Assert(subgrp);
				AIUnit *leader = subgrp->Leader();
				Assert(leader);
				return leader->IsFreeSoldier(); 
			}
		default:
			return true;
	}
}

static void SupplyInit(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "Supply", "INIT");

	if (!subgrp->HasAI())
	{
		VehicleSupply *transport = dyn_cast<VehicleSupply, Object>(cmd->_target);
		if (transport) cmd->_destination = GetSupplyPoint(transport);
		return;
	}

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckSupplyInit(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SSupplySupply, context);
	}
	else
	{
		context->_fsm->SetState(SSupplyMove, context);
		subgrp->ClearPlan();
	}
}

static void SupplyAlloc(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Supply", "ALLOC");

	// stop and wait
	subgrp->Stop();
}

const float SupplyWaitDistance=70;

static void CheckSupplyAlloc(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}
	if (CheckSupplyDone(context)) return;

	EntityAI *vehicle = subgrp->Leader()->GetVehicle();

	VehicleSupply *transport = dyn_cast<VehicleSupply, Object>(cmd->_target);
	Assert(transport);	// checked in CheckSupplyDone

	// if supply is allocated, we cannot go direct
	EntityAI *alloc=transport ? transport->GetAllocSupply() : NULL;
	if( !alloc || alloc==vehicle )
	{
		// start moving - supply is free
		context->_fsm->SetState(SSupplyMove, context);
		return;
	}

	// if supply is far (moved), we can get nearer
	if( transport->Position().Distance2(vehicle->Position())>Square(SupplyWaitDistance*1.5) )
	{
		context->_fsm->SetState(SSupplyMove, context);
		return;
	}
}

static bool IsDropAction(Command *cmd)
{
	return
		cmd->_message == Command::Action &&
		(cmd->_action == ATDropWeapon || cmd->_action == ATDropMagazine) &&
		cmd->_target == NULL;
}

static void SupplyMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Supply", "MOVE");

	// update command destination
	VehicleSupply *transport = dyn_cast<VehicleSupply, Object>(cmd->_target);
	if (transport)
	{
		cmd->_destination = GetActionPointFar(transport,cmd);
	}

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination);
}

static void CheckSupplyMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}
	if (IsDropAction(context->_task))
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}

	if (CheckSupplyDone(context)) return;

	EntityAI *vehicle = subgrp->Leader()->GetVehicle();
	Vector3Val posL = vehicle->Position();
	Vector3Val posD = cmd->_destination;

	VehicleSupply *transport = dyn_cast<VehicleSupply, Object>(cmd->_target);
	Assert(transport);	// checked in CheckSupplyDone

	if( transport && transport->Position().Distance2(vehicle->Position())<Square(SupplyWaitDistance) )
	{
		// if supply is allocated, we cannot go direct
		EntityAI *alloc=transport->GetAllocSupply();
		if( alloc && alloc!=vehicle )
		{
			context->_fsm->SetState(SSupplyAlloc, context);
			return;
		}
		// allocate - we are quite near
		transport->SetAllocSupply(vehicle);
		#if LOG_FSM
			LogF
			(
				"%s allocated %s",
				(const char *)vehicle->GetDebugName(),(const char *)transport->GetDebugName()
			);
		#endif
	}


	Point3 posDNew = GetActionPointFar(transport,cmd);
	float radius = transport->GetRadius() * 1.4 + 3.5;
	if ((posL - posDNew).SquareSizeXZ() <= Square(radius))
	{
		bool ok = subgrp->AllUnitsCompleted();
		if (!ok)
		{
			Vector3 posDExact = GetActionPoint(transport,cmd);
			if ((posL - posDExact).CosAngle(posDExact - transport->Position()) > cos(0.25 * H_PI))
			{
				CollisionBuffer collision;
				GLandscape->ObjectCollision(collision, vehicle, NULL, posL, posDExact, 0, ObjIntersectGeom);
				ok = collision.Size() == 0;
				#if _ENABLE_CHEATS
				if (!ok)
				{
					for (int i=0; i<collision.Size(); i++)
					{
						const CollisionInfo &info = collision[i];
						
						LogF
						(
							"%s at %s: collision with %s",
							(const char *)vehicle->GetDebugName(),
							(const char *)transport->GetDebugName(),
							(const char *)info.object->GetDebugName()
						);
					}
				}
				#endif
			}
		}
		if (ok)
		{
			context->_fsm->SetState(SSupplyDirect, context);
			subgrp->ClearPlan();
			return;
		}
	}

	float limit = 0.01 * (posL - posD).SquareSizeXZ();
	saturateMax(limit, Square(0.5 * radius));
	if ((posD - posDNew).SquareSizeXZ() > limit)
	{
		// update target position
		subgrp->GoPlanned(posDNew);
	}
}

static void SupplyDirect(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Supply", "DIRECT");

	VehicleSupply *transport = dyn_cast<VehicleSupply, Object>(cmd->_target);
	if (transport)
	{
		// direct go
		subgrp->GoDirect(GetSupplyPoint(transport));
	}
}

static void CheckSupplyDirect(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}
	if (IsDropAction(context->_task))
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}
	if (CheckSupplyDone(context)) return;

	VehicleSupply *transport = dyn_cast<VehicleSupply, Object>(cmd->_target);
	Assert(transport);	// checked in CheckSupplyDone
	// check if supply is allocated
	AIUnit *leader = subgrp->Leader();
	EntityAI *vehicle = leader->GetVehicle();

	Vector3Val posL = vehicle->Position();
	Vector3Val posD = cmd->_destination;
	Point3 posDNew = GetActionPoint(transport,cmd);
	float radius = transport->GetType()->GetSupplyRadius() + vehicle->CollisionSize() * 1.4f + 1;
	if ((posL - posDNew).SquareSizeXZ() <= Square(radius))
	{
		context->_fsm->SetState(SSupplySupply, context);
		subgrp->ClearPlan();
		return;
	}

	float limit = Square(0.5 * radius);
	if ((posD - posDNew).SquareSizeXZ() > limit)
	{
		// target moved
		context->_fsm->SetState(SSupplyMove, context);
		return;
	}
	#if 1
	// if we are not moving any longer and our supply is not moving as well
	// it is dead-lock
	// we have to recover somehow
	if 
	(
		transport->Speed().SquareSize()<1 &&
		leader->HasAI() && leader->GetPath().Size()<=0 
	)
	{
		if (posDNew.Distance2(vehicle->Position())<Square(50))
		{
			LogF("%s: supply dead-lock recovery - OK.",(const char *)vehicle->GetDebugName());
			context->_fsm->SetState(SSupplySupply, context);
			subgrp->ClearPlan();
			return;
		}
		else
		{
			Time maxTimeout = Glob.time+30;
			if (cmd->_time>maxTimeout)
			{
				LogF("%s: supply dead-lock recovery - Fail.",(const char *)vehicle->GetDebugName());
				cmd->_time = maxTimeout;
			}
		}
	}
	#endif

}

/*!
\patch 1.28 Date 10/22/2001 by Ondra.
- Fixed: AI soldiers with AI leader were able to rearm
without corresponding animation.
*/
static void ProcessSupply(VehicleSupply *transport, AIUnit *unit, Command *cmd)
{
	EntityAI *vehicle = unit->GetVehicle();

	UIActionType action;
	// translate message to action
	switch (cmd->_message)
	{
	default:
		Fail("Unknown action");
	case Command::Heal:
		action = ATHeal;
		break;
	case Command::Repair:
		action = ATRepair;
		break;
	case Command::Refuel:
		action = ATRefuel;
		break;
	case Command::Rearm:
		action = ATRearm;
		break;
/*
	case Command::TakeWeapon:
		action = ATTakeWeapon;
		break;
	case Command::TakeMagazine:
		action = ATTakeMagazine;
		break;
*/
	case Command::Action:
		action = cmd->_action;
		break;
	}
	if (!vehicle->CheckActionProcessing(action,unit))
	{
		LogF
		(
			"%s ProcessSupply %s",
			(const char *)vehicle->GetDebugName(),transport ? (const char *)transport->GetDebugName() : "NULL"
		);
		UIAction uiAction;
		uiAction.type = action;
		uiAction.target = cmd->_target;
		uiAction.param = cmd->_param;
		uiAction.param2 = cmd->_param2;
		uiAction.param3 = cmd->_param3;
		uiAction.priority = 0;
		uiAction.showWindow = false;
		uiAction.hideOnUse = false;
		vehicle->StartActionProcessing(uiAction,unit);
	}
}

static void SupplySupply(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "Supply", "SUPPLY");

	if (!subgrp->HasAI()) return;

	cmd->_time = Glob.time+60;
	subgrp->Stop();

	if (IsDropAction(context->_task))
	{
		ProcessSupply(NULL, subgrp->Leader(), cmd);
		context->_fsm->SetState(SSupplySucceed, context);
		return;
	}

	VehicleSupply *transport = dyn_cast<VehicleSupply, Object>(cmd->_target);
	if (transport)
	{
		ProcessSupply(transport, subgrp->Leader(), cmd);
		if (SupplyDoneImmediatelly(context))
		{
			// supply command succeed immediatelly
			EntityAI *veh = subgrp->Leader()->GetVehicle();

			#define UNALLOC() if( transport->GetAllocSupply()==veh ) transport->SetAllocSupply(NULL);
			VehicleSupply *transport = dyn_cast<VehicleSupply,Object>(cmd->_target);
			if (transport)
			{
				UNALLOC();
			}

			context->_fsm->SetState(SSupplySucceed, context);
		}
	}
}

static void CheckSupplySupply(AISubgroupContext *context)
{
	STATE_PREFIX
	
	if (CheckSupplyDone(context)) return;

	if (!subgrp->HasAI())
	{
		VehicleSupply *transport = dyn_cast<VehicleSupply, Object>(cmd->_target);
		if (transport) cmd->_destination = GetSupplyPoint(transport);
		return;
	}

	EntityAI *vehicle = subgrp->Leader()->GetVehicle();
	VehicleSupply *transport = dyn_cast<VehicleSupply, Object>(cmd->_target);
	Assert(transport);	// checked in CheckSupplyDone
	Vector3Val posL = vehicle->Position();
	Point3 posD = GetSupplyPoint(transport);
	float radius = transport->GetType()->GetSupplyRadius() + vehicle->CollisionSize() * 1.2 + 1;

	if ((posL - posD).SquareSizeXZ() > Square(radius))
	{
		context->_fsm->SetState(SSupplyMove, context);
		subgrp->ClearPlan();
		return;
	}
	if (transport->GetSupplying() != vehicle)
	{
		// refresh supply
		ProcessSupply(transport, subgrp->Leader(), cmd);
	}
}

static void SupplyEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	VehicleSupply *vehInto = dyn_cast<VehicleSupply,Object>(cmd->_target);
	if (vehInto)
	{
/*
		if (!vehInto->GetType()->IsKindOf(GWorld->Preloaded(VTypeStatic)))
		{
			AIUnit *unit = vehInto->CommanderUnit();
			AIGroup *grp = subgrp->GetGroup();
			AICenter *center = grp->GetCenter();
			if (unit && unit->GetLifeState() == AIUnit::LSAlive)
			{
				if (unit->GetGroup() == grp)
				{
					grp->GetRadio().Transmit
					(
						new RadioMessageAskSupply(leader, cmd->_message),
						center->GetLanguage()
					);
				}
				else
				{
					center->GetRadio().Transmit
					(
						new RadioMessageAskSupply(leader, cmd->_message),
						center->GetLanguage()
					);
				}
			}
		}
*/
		vehInto->WaitForSupply(leader);
	}
}

static void SupplyExit(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	VehicleSupply *vehInto = dyn_cast<VehicleSupply,Object>(cmd->_target);
	if (vehInto)
	{
		vehInto->SupplyFinished(leader);
	}
}

static AISubgroupFSM::StateInfo supplyStates[] =
{
	AISubgroupFSM::StateInfo("Init", SupplyInit, CheckSupplyInit),
	AISubgroupFSM::StateInfo("Alloc", SupplyAlloc, CheckSupplyAlloc),
	AISubgroupFSM::StateInfo("Move", SupplyMove, CheckSupplyMove),
	AISubgroupFSM::StateInfo("Direct", SupplyDirect, CheckSupplyDirect),
	AISubgroupFSM::StateInfo("Supply", SupplySupply, CheckSupplySupply),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

static AISubgroupFSM::pStateFunction supplySpecial[] =
{
	SupplyEnter,
	SupplyExit
};

///////////////////////////////////////////////////////////////////////////////
// Command Join FSM

/*

enum
{
	SJoinInit,
	SJoinMove,
	SJoinDone,SJoinFail
};

static void JoinInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Join", "INIT");

	subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
	subgrp->DoNotGo();
}

static void CheckJoinInit(AISubgroupContext *context)
{
	STATE_PREFIX

	context->_fsm->SetState(SJoinMove, context);
	// subgrp may not be valid
	// subgrp->ClearPlan(); 
}

static void JoinMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
	{
		context->_fsm->SetTimeOut(20);
		return;
	}

	LogFSM(subgrp, "Join", "MOVE");

	// update destination
	AIGroup *grp = subgrp->GetGroup();
	Assert(grp);
	if (subgrp == grp->MainSubgroup())
	{
		context->_fsm->SetState(SJoinFail, context);
		subgrp->ClearPlan();
		return;
	}
	else
	{
		AISubgroup *subgroup = cmd->_joinToSubgroup;
		if (!subgroup)
			subgroup = grp->MainSubgroup();
		Assert(subgroup);
		Assert(subgrp != subgroup);
		if (subgroup->NUnits() == 0)
		{
			// do not join now - wait for update phase
			return;	// join now
		}

		if (!subgroup->Leader())
		{
			Fail("Leader");
			subgroup->SelectLeader();
			if (!subgroup->Leader())
			{
				Fail("Leader cannot be selected");
				return;
			}
		}

		AIUnit *tgtLeader = subgroup->Leader();

		AIUnit *leader = subgrp->Leader();

		cmd->_destination = leader->GetFormationAbsolute(tgtLeader);
		
		subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
		subgrp->GoPlanned(cmd->_destination);
	}
}

static void CheckJoinMove(AISubgroupContext *context)
{
	STATE_PREFIX

	// TODO: BUG
	if (subgrp->HasAI() && subgrp->GetMode() != AISubgroup::PlanAndGo)
	{
		subgrp->GoPlanned(cmd->_destination);
	}

	AIGroup *grp = subgrp->GetGroup();
	Assert(grp);
	// update destination
	AISubgroup *subgroup = cmd->_joinToSubgroup;
	if (!subgroup)
		subgroup = grp->MainSubgroup();
	Assert(subgroup);
	Assert(subgrp != subgroup);
	if (subgroup->NUnits() == 0)
	{
		context->_fsm->SetState(SJoinDone, context);
		subgrp->ClearPlan();
		subgrp->JoinToSubgroup(subgroup);
		return;	// join now
	}

	AIUnit *leader = subgrp->Leader();
	AIUnit *tgtLeader = subgroup->Leader();
	Assert(leader);

	EntityAI *veh = leader->GetVehicle();

	bool away = veh->IsAway(tgtLeader,0.75);
	if (!away)
	{
		context->_fsm->SetState(SJoinDone, context);
		subgrp->ClearPlan();
		// note: subgrp may become MainSubgroup if MainSubgroup is destroyed
		if( grp->MainSubgroup() != subgrp )
		{
			subgrp->JoinToSubgroup(subgroup);
		}
		return;
	}

	if (subgrp->HasAI() )
	{
		if( subgrp->AllUnitsCompleted())
		{
			context->_fsm->Refresh(context);
			return;
		}
	}
	else
	{
		if( context->_fsm->TimedOut() )
		{
			context->_fsm->SetState(SJoinFail, context);
		}
	}
}

static AISubgroupFSM::StateInfo joinStates[] =
{
	AISubgroupFSM::StateInfo("Init", JoinInit, CheckJoinInit),
	AISubgroupFSM::StateInfo("Move", JoinMove, CheckJoinMove),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

*/

enum
{
	SJoinInit,
	SJoinDone,SJoinFail
};

static void JoinInit(AISubgroupContext *context)
{
	STATE_PREFIX

	AISubgroup *joinTo = cmd->_joinToSubgroup;
	if (!joinTo) joinTo = subgrp->GetGroup()->MainSubgroup();

	if (joinTo && joinTo != subgrp && subgrp != subgrp->GetGroup()->MainSubgroup())
	{
		subgrp->JoinToSubgroup(joinTo);
	}
}

static void CheckJoinInit(AISubgroupContext *context)
{
	context->_fsm->SetState(SJoinDone, context);
}

static AISubgroupFSM::StateInfo joinStates[] =
{
	AISubgroupFSM::StateInfo("Init", JoinInit, CheckJoinInit),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Command GetIn FSM

static Vector3 GetGetInPoint(AIUnit *unit, Transport *veh)
{
	Assert(veh == unit->VehicleAssigned());
/*
	const VehicleType *type = veh->GetType();
	Vector3 pos = VZero;
	if (veh->GetDriverAssigned() == unit)
	{
		// get in as driver
		pos = veh->PositionModelToWorld
		(
			type->GetDriverGetInPos()
		);
	}
	else
	{
		// get in as cargo
		pos = veh->PositionModelToWorld
		(
			type->GetCargoGetInPos()
		);
	}
	pos[1] = GLandscape->RoadSurfaceYAboveWater(pos[0], pos[2]);
*/
	Vector3 pos = veh->GetUnitGetInPos(unit);
	pos[1] = GLandscape->RoadSurfaceYAboveWater(pos[0], pos[2]);
	return pos;
}


static Vector3 GetGetInPointFar(AIUnit *unit, Transport *veh)
{
	Vector3 res;

	Assert(veh == unit->VehicleAssigned());
//	const VehicleType *type = veh->GetType();

	if (veh->IsStopped())
	{
		res = veh->GetUnitGetInPos(unit);
/*
		if (veh->GetDriverAssigned() == unit)
		{
			// get in as driver
			pt = type->GetDriverGetInPos();
		}
		else
		{
			// get in as cargo
			pt = type->GetCargoGetInPos();
		}
		veh->PositionModelToWorld(res,pt);
*/
	}
	else
	{
		res = veh->GetStopPosition();
		res += Vector3(30,0,0);

	}

	// TODO: FindNearestEmpty may fail - use alternative GetOut point
	unit->FindNearestEmpty(res);

	res[1] = GLandscape->RoadSurfaceYAboveWater(res[0],res[2]);

	return res;
}

enum
{
	SGetInInit,
	SGetInMove,
	SGetInGetOut,
	SGetInWalk,
	SGetInDirect,
	SGetInGetIn,
	SGetInDone, SGetInFail
};

bool CheckGetInDone(AISubgroupContext *context)
{
	AISubgroup *subgrp = context->_subgroup;
	Assert(subgrp);
	Command *cmd = context->_task;
	Assert(cmd);

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);

	if (!vehInto || !vehInto->IsPossibleToGetIn())
	{
		context->_fsm->SetState(SGetInFail, context);
		subgrp->ClearPlan();
		return true;	// state changed
	}

	if (leader->GetVehicle() == vehInto)
	{
		context->_fsm->SetState(SGetInDone, context);
		subgrp->ClearPlan();
		return true;	// state changed
	}

	if (subgrp->HasAI() && leader->VehicleAssigned() != vehInto)
	{
		context->_fsm->SetState(SGetInFail, context);
		subgrp->ClearPlan();
		return true;	// state changed
	}

	return false;	// continue with other tests
}

static void GetInInit(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "GetIn", "INIT");

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport,Object>(cmd->_target);
	if (vehInto)
	{
		if (!subgrp->HasAI())
		{
			cmd->_destination = GetGetInPoint(leader, vehInto);
		}
	}

	if (!subgrp->HasAI())
		return;

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckGetInInit(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
	}
	else
	{
		context->_fsm->SetState(SGetInMove, context);
		subgrp->ClearPlan();
	}
}

static void GetInMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "GetIn", "MOVE");

	// update command destination
	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	if (vehInto)
	{
		cmd->_destination = GetGetInPointFar(leader, vehInto);
	}

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination);

	Vector3Val posL = leader->Position();
	Vector3Val posD = cmd->_destination;
	float dist = 200;
	if ((posL - posD).SquareSizeXZ() > Square(dist))
	{
		for (int i=0; i<subgrp->NUnits(); i++)
		{
			AIUnit *unit = subgrp->GetUnit(i);
			if (unit)
				unit->OrderGetIn(true);
		}
	}
}

static void CheckGetInMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
		return;
	}
	if (CheckGetInDone(context)) return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *vehicle = leader->GetVehicle();
	Vector3Val posL = vehicle->Position();
	Vector3Val posD = cmd->_destination;

	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	Assert(vehInto);	// checked in CheckGetInDone
	Point3 posDNew = GetGetInPointFar(leader, vehInto);
	float radius = 2.5 * vehInto->GetRadius() + 15.0;

	bool completed = subgrp->AllUnitsCompleted();
	if (completed) CheckDistance(subgrp->Leader(), cmd->_destination);
	
	if ((posL - posDNew).SquareSizeXZ() <= Square(radius) || completed)
	{
		context->_fsm->SetState(SGetInGetOut, context);
		return;
	}

	float limit = 0.01 * (posL - posD).SquareSizeXZ();
	saturateMax(limit, Square(0.5 * radius));
	if ((posD - posDNew).SquareSizeXZ() > limit)
	{
		// update target position
		subgrp->GoPlanned(posDNew);
	}
}

static void GetInGetOut(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "GetIn", "GETOUT");

	AIUnit *unit = subgrp->Leader();
	Assert(unit);

	unit->OrderGetIn(true);
	if (!unit->IsFreeSoldier())
	{
		PackedBoolArray list;
		list.Set(unit->ID() - 1, true);
		//subgrp->GetGroup()->SendGetOut(list);
		Command cmd;
		cmd._message = Command::GetOut;
		cmd._context = Command::CtxAutoSilent;
		cmd._id = subgrp->GetGroup()->GetNextCmdId();
		subgrp->GetGroup()->IssueCommand(cmd,list);
	}
}

static void CheckGetInGetOut(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
		return;
	}
	if (CheckGetInDone(context)) return;

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);

	if (!vehInto || !vehInto->QCanIGetInAny(leader->GetPerson()) )
	{
		// fail - unable to get in
		context->_fsm->SetState(SGetInFail, context);
		subgrp->ClearPlan();
		return;
	}

	if (subgrp->Leader()->IsFreeSoldier())
	{
		context->_fsm->SetState(SGetInWalk, context);
		return;
	}

	if (!subgrp->GetGroup()->CommandSent(subgrp->Leader(), Command::GetOut))
	{
		// get out from current vehicle failed
		context->_fsm->SetState(SGetInFail, context);
		return;
	}
}

static void GetInWalk(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "GetIn", "GETIN");

	if (!subgrp->HasAI())
		return;

	// update command destination
	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	if (vehInto)
	{
		cmd->_destination = GetGetInPointFar(leader, vehInto);
	}

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination);
}

static void CheckGetInWalk(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
		return;
	}
	if (CheckGetInDone(context)) return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *vehicle = leader->GetVehicle();
	Vector3Val posL = vehicle->Position();
	Vector3Val posD = cmd->_destination;

	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	Assert(vehInto);	// checked in CheckGetInDone
	Point3 posDNew = GetGetInPointFar(leader, vehInto);
	float radius = vehInto->GetRadius() * 1.4 + 3.5;

	if (subgrp->AllUnitsCompleted())
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);
		
		if (vehInto->IsStopped())
		{
			if (vehInto->QCanIGetInAny(leader->GetPerson()))
			{
				context->_fsm->SetState(SGetInDirect, context);
			}
			else
			{
				// fail - unable to get in
				context->_fsm->SetState(SGetInFail, context);
			}
			subgrp->ClearPlan();
			return;
		}
	}

	if ((posL - posDNew).SquareSizeXZ() <= Square(radius))
	{
		// check if vehicle is stopped
		if (vehInto->IsStopped())
		{
			CollisionBuffer collision;
			Vector3 posDExact = GetGetInPoint(leader, vehInto);
			if ((posL - posDExact).CosAngle(posDExact - vehInto->Position()) > cos(0.25 * H_PI))
			{
				GLandscape->ObjectCollision(collision, vehicle, NULL, posL, posDExact, 0, ObjIntersectGeom);
				if (collision.Size() == 0)
				{
					if (vehInto->QCanIGetInAny(leader->GetPerson()))
					{
						context->_fsm->SetState(SGetInDirect, context);
					}
					else
					{
						// fail - unable to get in
						context->_fsm->SetState(SGetInFail, context);
					}
					subgrp->ClearPlan();
					return;
				}
			}
		}
	}

	bool completed = subgrp->AllUnitsCompleted();
	if (completed) CheckDistance(subgrp->Leader(), cmd->_destination);

	float limit = 0.01 * (posL - posD).SquareSizeXZ();
	saturateMax(limit, Square(0.5 * radius));
	if ((posD - posDNew).SquareSizeXZ() > limit || completed)
	{
		// update target position
		subgrp->GoPlanned(posDNew);
	}
}

static void GetInDirect(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "GetIn", "DIRECT");

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	if (vehInto)
	{
		// direct go
		subgrp->GoDirect(GetGetInPoint(leader, vehInto));
	}
}

static void CheckGetInDirect(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
		return;
	}
	if (CheckGetInDone(context)) return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *vehicle = leader->GetVehicle();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	Assert(vehInto);	// checked in CheckGetInDone
	Vector3Val posL = vehicle->Position();
	Point3 posD = GetGetInPoint(leader, vehInto);

	if( cmd->_destination.Distance2(GetGetInPoint(leader, vehInto))>=Square(3) )
	{
		// vehicle moved - go back to non-direct state
		context->_fsm->SetState(SGetInWalk, context);
		return;
	}

	// if soldier crashes into the vehicle, GetIn
	CollisionBuffer bump;
	vehicle->Intersect(bump,vehInto);
	//float radius = /*vehInto->GetType()->GetGetInRadius()*/2.0 + vehicle->CollisionSize() * 1.2 + 1;
	//float radius = vehicle->GetPrecision() * 0.55;
	float radius = vehicle->GetPrecision();
	//if ((posL - posD).SquareSizeXZ() <= Square(radius))

//	if( bump.Size()>0 || (posL - posD).SquareSizeXZ() <= Square(radius) )
// avoid changes of destination
	if( bump.Size()>0 || (posL - cmd->_destination).SquareSizeXZ() <= Square(radius) )
	{
		context->_fsm->SetState(SGetInGetIn, context);
		subgrp->ClearPlan();
		return;
	}

	if ((posL - cmd->_destination).SquareSizeXZ() >= Square(100))
	{
		// vehicle is now very far - go back to non-direct state
		context->_fsm->SetState(SGetInWalk, context);
		return;
	}
}

static void GetInGetIn(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "GetIn", "GETIN");

	AIUnit *leader = subgrp->Leader();
	Assert(leader->IsFreeSoldier());
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	if (!vehInto)
	{
		context->_fsm->SetState(SGetInFail, context);
		return;
	}
	if (!leader->ProcessGetIn(*vehInto))
		context->_fsm->SetState(SGetInFail, context);
	// else subgroup dies out
}

static void CheckGetInGetIn(AISubgroupContext *context)
{
	STATE_PREFIX
	CheckGetInDone(context);
}

static void GetInEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport,Object>(cmd->_target);
	if (vehInto) vehInto->WaitForGetIn(leader);
}

static void GetInExit(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport,Object>(cmd->_target);
	if (vehInto) vehInto->GetInFinished(leader);
}

static AISubgroupFSM::StateInfo getinStates[] =
{
	AISubgroupFSM::StateInfo("Init", GetInInit, CheckGetInInit),
	AISubgroupFSM::StateInfo("Move", GetInMove, CheckGetInMove),
	AISubgroupFSM::StateInfo("GetOut", GetInGetOut, CheckGetInGetOut),
	AISubgroupFSM::StateInfo("Walk", GetInWalk, CheckGetInWalk),
	AISubgroupFSM::StateInfo("Direct", GetInDirect, CheckGetInDirect),
	AISubgroupFSM::StateInfo("GetIn", GetInGetIn, CheckGetInGetIn),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

static AISubgroupFSM::pStateFunction getinSpecial[] =
{
	GetInEnter,
	GetInExit
};

///////////////////////////////////////////////////////////////////////////////
// Command GetOut FSM

enum
{
	SGetOutInit,
	SGetOutGetOut,
	SGetOutMove,
	SGetOutDone, SGetOutFail
};

static void GetOutInit(AISubgroupContext *context)
{
	INIT_PREFIX
	LogFSM(subgrp, "GetOut", "INIT");
	subgrp->SetRefreshTime(TIME_MAX);
}

static void CheckGetOutInit(AISubgroupContext *context)
{
	INIT_PREFIX
	context->_fsm->SetState(SGetOutGetOut, context);
}

static void GetOutGetOut(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "GetOut", "GETOUT");
}

static void CheckGetOutGetOut(AISubgroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = subgrp->Leader();

	if (leader->IsFreeSoldier())
	{
		if (subgrp->HasAI())
			context->_fsm->SetState(SGetOutMove, context);
		else
			context->_fsm->SetState(SGetOutDone, context);
	}
}

static void GetOutMove(AISubgroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = subgrp->Leader();

	LogFSM(subgrp, "GetOut", "Move");

	if (!cmd->_target)
	{
		context->_fsm->SetState(SGetOutDone, context);
		return;
	}

	Vector3Val posL = subgrp->Leader()->Position();
	Vector3Val posV = cmd->_target->Position();
	float distance = (cmd->_target->GetRadius() * 0.9f + 1.0f);

	Vector3 norm = (posL - posV);
	Point3 posD = posV + norm.Normalized() * distance;
	Vector3 normal;
	leader->FindNearestEmpty(posD);
	
	subgrp->GoDirect(posD);
}

static void CheckGetOutMove(AISubgroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = subgrp->Leader();

	EntityAI *lVehicle=leader->GetVehicle();

	if( !cmd->_target )
	{
		// target destroyed - 
		context->_fsm->SetState(SGetOutDone, context);
		return;
	}
	const float tgtRadius = cmd->_target->GetRadius();
	const float radius = lVehicle->GetPrecision()+tgtRadius*0.5;
	Vector3Val posL = lVehicle->Position();
	if ((posL - cmd->_destination).SquareSizeXZ() <= Square(radius))
	{
		context->_fsm->SetState(SGetOutDone, context);
	}
}

static void GetOutEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	Assert( leader );
	if (leader->IsFreeSoldier()) return;

	Transport *veh = leader->GetVehicleIn();
	Assert(veh);
	cmd->_target = veh;
	if (veh->Driver())
	{
		AIUnit *driver = veh->DriverBrain();
		if (driver)
		{
			veh->WaitForGetOut(leader);
			return;
		}
	}
	if (veh->GetGetOutTime()>Glob.time+2)
	{
		veh->SetGetOutTime(Glob.time+2);
	}
	veh->GetOutStarted(leader);
}

static void GetOutExit(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	Transport *veh = dyn_cast<Transport,Object>(cmd->_target);
	if (veh) veh->GetOutFinished(leader);
}

static AISubgroupFSM::StateInfo getoutStates[] =
{
	AISubgroupFSM::StateInfo("Init", GetOutInit, CheckGetOutInit),
	AISubgroupFSM::StateInfo("GetOut", GetOutGetOut, CheckGetOutGetOut),
	AISubgroupFSM::StateInfo("Move", GetOutMove, CheckGetOutMove),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

static AISubgroupFSM::pStateFunction getoutSpecial[] =
{
	GetOutEnter,
	GetOutExit
};

///////////////////////////////////////////////////////////////////////////////
// Command Support FSM

enum
{
	SSupportInit,SSupportMove,SSupportDone,SSupportFail
};

static bool CheckSupportDone(AISubgroupContext *context)
{
	AISubgroup *subgrp = context->_subgroup;
	Command *cmd = context->_task;

	VehicleSupply *veh = dyn_cast<VehicleSupply>(subgrp->Leader()->GetVehicle());
	if (!veh)
	{
		// vehicle changed
		context->_fsm->SetState(SSupportFail, context);
		subgrp->ClearPlan();
		return true;
	}

	VehicleWithAI *target = cmd->_target;
	if (!target)
	{
		// target destroyed
		context->_fsm->SetState(SSupportFail, context);
		subgrp->ClearPlan();
		return true;
	}

	if (Glob.time > cmd->_time)
	{
		// timeout
		context->_fsm->SetState(SSupportFail, context);
		subgrp->ClearPlan();
		return true;
	}

	// target allocated - continue
	if (veh->GetAllocSupply() == target) return false;

	if (veh->GetAllocSupply())
	{
		// allocated by other unit
		context->_fsm->SetState(SSupportFail, context);
		subgrp->ClearPlan();
		return true;
	}

	const OLinkArray<AIUnit> &supplyUnits = veh->GetSupplyUnits();
	for (int i=0; i<supplyUnits.Size(); i++)
	{
		if (!supplyUnits[i]) continue;
		if (supplyUnits[i]->GetVehicle() == target) return false; // continue
	}

	// done
	context->_fsm->SetState(SSupportDone, context);
	subgrp->ClearPlan();
	return true;
}

static void SupportInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Support", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckSupportInit(AISubgroupContext *context)
{
	STATE_PREFIX
	
	context->_fsm->SetState(SSupportMove, context);
	subgrp->ClearPlan();
}

static void SupportMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Support", "MOVE");

	// update command destination
	EntityAI *veh = cmd->_target;
	if (veh) cmd->_destination = veh->Position();

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination);
}

/*!
\patch 1.28 Date 10/22/2001 by Ondra.
- Fixed: AI driving and coordination errors when using supply trucks fixed.
*/

static void CheckSupportMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (CheckSupportDone(context)) return;

	EntityAI *vehicle = subgrp->Leader()->GetVehicle();
	Vector3Val posL = vehicle->Position();
	Vector3Val posD = cmd->_destination;

	EntityAI *target = cmd->_target;
	Assert(target);	// checked in CheckSupportDone
	Vector3Val posDNew = target->Position();
	float distanceToTarget2 = posL.DistanceXZ2(posD);
	float limit = Square(0.1f) * distanceToTarget2;
	saturateMax(limit, Square(5));
	float precision = vehicle->GetPrecision();
	saturateMax(limit, Square(precision));
	if (posD.DistanceXZ2(posDNew)> limit)
	{
		if (distanceToTarget2>Square(precision*4))
		{
			// update target position
			subgrp->GoPlanned(posDNew);
		}
	}
	else if (distanceToTarget2<Square(precision*2))
	{
		// check if target is in good position for supplying
		subgrp->Stop();
	}
}

static AISubgroupFSM::StateInfo supportStates[] =
{
	AISubgroupFSM::StateInfo("Init", SupportInit, CheckSupportInit),
	AISubgroupFSM::StateInfo("Move", SupportMove, CheckSupportMove),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Registration of FSMs

FSM *AbstractAIMachine<Command, AISubgroupContext>::CreateFSM(int taskType)
{
	int itemSize = sizeof(AISubgroupFSM::StateInfo);
	int funcSize = sizeof(AISubgroupFSM::pStateFunction);

	Assert( sizeof(waitStates) / itemSize == SWaitFail+1 );
	Assert( sizeof(attackStates) / itemSize == SAttackFail+1 );
	Assert( sizeof(hideStates) / itemSize == SHideFail+1 );
	Assert( sizeof(moveStates) / itemSize == SMoveFail+1 );
	Assert( sizeof(getoutStates) / itemSize == SGetOutFail+1 );
	Assert( sizeof(joinStates) / itemSize == SJoinFail+1 );
	Assert( sizeof(getinStates) / itemSize == SGetInFail+1 );
	Assert( sizeof(supplyStates) / itemSize == SSupplyFail+1 );
	Assert( sizeof(supportStates) / itemSize == SSupportFail+1 );
	Assert( sizeof(fireStates) / itemSize == SFireFail+1 );
	Assert( sizeof(stopStates) / itemSize == SStopFail+1 );

	switch (taskType)
	{
		case Command::NoCommand:
			return new AISubgroupFSM
			(
				noCommandStates,
				sizeof(noCommandStates) / itemSize
				//noCommandSpecial,
				//sizeof(noCommandSpecial) / itemSize
			);
		case Command::Wait:
			return new AISubgroupFSM
			(
				waitStates,
				sizeof(waitStates) / itemSize
			);
		case Command::Attack:
			return new AISubgroupFSM
			(
				attackStates,
				sizeof(attackStates) / itemSize,
				attackSpecial,
				sizeof(attackSpecial) / funcSize
			);
	#if ENABLE_HOLDFIRE_FIX
		case Command::AttackAndFire:
			return new AISubgroupFSM
			(
				attackStates,
				sizeof(attackStates) / itemSize,
				attackFireSpecial,
				sizeof(attackFireSpecial) / funcSize
			);
	#endif
		case Command::Hide:
			return new AISubgroupFSM
			(
				hideStates,
				sizeof(hideStates) / itemSize,
				hideSpecial,
				sizeof(hideSpecial) / funcSize
			);
		case Command::Move:
			return new AISubgroupFSM
			(
				moveStates,
				sizeof(moveStates) / itemSize
			);
		case Command::Heal:
		case Command::Repair:
		case Command::Refuel:
		case Command::Rearm:
		case Command::Action:
			return new AISubgroupFSM
			(
				supplyStates,
				sizeof(supplyStates) / itemSize,
				supplySpecial,
				sizeof(supplySpecial) / funcSize
			);
		case Command::Support:
			return new AISubgroupFSM
			(
				supportStates,
				sizeof(supportStates) / itemSize
			);
		case Command::Join:
			return new AISubgroupFSM
			(
				joinStates,
				sizeof(joinStates) / itemSize
			);
		case Command::GetIn:
			return new AISubgroupFSM
			(
				getinStates,
				sizeof(getinStates) / itemSize,
				getinSpecial,
				sizeof(getinSpecial) / funcSize
			);
		case Command::GetOut:
			return new AISubgroupFSM
			(
				getoutStates,
				sizeof(getoutStates) / itemSize,
				getoutSpecial,
				sizeof(getoutSpecial) / funcSize
			);
		case Command::Fire:
			return new AISubgroupFSM
			(
				fireStates,
				sizeof(fireStates) / itemSize
			);
		case Command::Stop:
			return new AISubgroupFSM
			(
				stopStates,
				sizeof(stopStates) / itemSize
			);
		case Command::Expect:
			return new AISubgroupFSM
			(
				expectStates,
				sizeof(expectStates) / itemSize
			);
	}
	Fail("Unknown command type");
	return NULL;
}

void FailCommand(AISubgroupContext *context)
{
	Assert(context->_subgroup);
	Command *cmd = context->_task;
	Assert(cmd);
	FSM *fsm = context->_fsm;
	Assert(fsm);

	if (fsm)
	{
		switch (cmd->_message)
		{
		case Command::NoCommand:
			break;
		case Command::Wait:
			fsm->SetState(SWaitFail, context);
			break;
		case Command::Attack:
	#if ENABLE_HOLDFIRE_FIX
		case Command::AttackAndFire:
			fsm->SetState(SAttackFail, context);
			break;
	#endif
		case Command::Hide:
			fsm->SetState(SHideFail, context);
			break;
		case Command::Move:
			fsm->SetState(SMoveFail, context);
			break;
		case Command::Heal:
		case Command::Repair:
		case Command::Refuel:
		case Command::Rearm:
/*
		case Command::TakeWeapon:
		case Command::TakeMagazine:
*/
		case Command::Action:
			fsm->SetState(SSupplyFail, context);
			break;
		case Command::Support:
			fsm->SetState(SSupportFail, context);
			break;
		case Command::Join:
			fsm->SetState(SJoinFail, context);
			break;
		case Command::GetIn:
			fsm->SetState(SGetInFail, context);
			break;
		case Command::Fire:
			fsm->SetState(SFireFail, context);
			break;
		case Command::GetOut:
			fsm->SetState(SGetOutFail, context);
			break;
		case Command::Stop:
			fsm->SetState(SStopFail, context);
			break;
		case Command::Expect:
			fsm->SetState(SExpectFail, context);
			break;
/*
		case Command::Action:
			fsm->SetState(SActionFailed, context);
			break;
*/
		}
	}
}
