#include "wpch.hpp"
#include "dynSound.hpp"
#include <El/Common/randomGen.hpp>

#include "global.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "titEffects.hpp"

#include "ai.hpp"
#include "camera.hpp"
#include "engine.hpp"
#include "mbcs.hpp"

//#include "soldierOld.hpp"

void FindSFX(RString name, SoundEntry &emptySound, AutoArray<SoundEntry> &sounds);
const ParamEntry *FindSound(RString name, SoundPars &pars);

DynSound::DynSound()
{
}

DynSound::DynSound(const char *name)
{
	
	Load(name);
}

void DynSound::Load(const char *name)
{
	_name = name;
	FindSFX(name, _emptySound, _sounds);
}

SoundEntry DynSound::LoadEntry( const ParamEntry &entry )
{
	// load single sound parameter
	SoundEntry sound;
	GetValue(sound, entry);
	sound._probab=entry[3];
	sound._min_delay=entry[4],
	sound._mid_delay=entry[5];
	sound._max_delay=entry[6];
	return sound;
}

bool DynSound::IsOneLoopingSound() const
{
	if (_sounds.Size()!=1) return false;
	if (_sounds[0]._max_delay>0) return false;
	if (_emptySound._max_delay>0) return false;
	// in some cu
	return true;
}

const SoundEntry &DynSound::SelectSound( float probab ) const
{
	for( int i=0; i<_sounds.Size(); i++ )
	{
		probab-=_sounds[i]._probab;
		if( probab<=0 ) return _sounds[i];
	}
	return _emptySound;
}

void SoundObject::StartSound()
{

	SoundPars pars;
	FindSound(_soundName, pars);

	if (_hasObject)
	{
		Object *source = _object;
		if (_looped)
			_sound = GSoundScene->OpenAndPlay
			(
				pars.name, source->WorldPosition(), source->WorldSpeed(),
				pars.vol, pars.freq
			);
		else
			_sound = GSoundScene->OpenAndPlayOnce
			(
				pars.name, source->WorldPosition(), source->WorldSpeed(),
				pars.vol, pars.freq
			);
		source->AttachWave(_sound, pars.freq);
	}
	else
	{
		if (_looped)
			_sound = GSoundScene->OpenAndPlay2D
			(
				pars.name, pars.vol, pars.freq, false
			);
		else
			_sound = GSoundScene->OpenAndPlayOnce2D
			(
				pars.name, pars.vol, pars.freq, false
			);
	}
}

void SoundObject::LoadSound()
{
	SoundPars pars;
	const ParamEntry *cls = FindSound(_soundName, pars);

	if (cls)
	{
		ParamEntry *entry = cls->FindEntry("forceTitles");
		if (entry) _forceTitles = *entry;
		if (_forceTitles || Glob.config.showTitles)
		{
			entry = cls->FindEntry("titlesFont");
			if (entry)
			{
				_titlesFont = GEngine->LoadFont(GetFontID(*entry)); 
				_titlesSize = (*cls) >> "titlesSize";
			}
		
			const ParamEntry &cfg = (*cls) >> "titles";
			int n = cfg.GetSize() / 2;
			for (int i=0; i<n; i++)
			{
				int index = _titles.Add();
				float t = cfg[2 * i];
				_titles[index].time = Glob.time + t;
				_titles[index].text = cfg[2 * i + 1];
			}
		}
	}
}

static OLinkArray<SoundObject> SpeakingObjects;

bool IsSpeakingRadio(AIUnit *sender);

bool IsSpeakingDirect(AIUnit *sender)
{
	if (!sender) return false;

	for (int i=0; i<SpeakingObjects.Size();)
	{
		SoundObject *obj = SpeakingObjects[i];
		if (!obj) {SpeakingObjects.Delete(i); continue;}
		if (!obj->IsWaiting() && obj->GetSender() == sender) return true;
		i++;
	}
	return false;
}

SoundObject::SoundObject(RString name, Object *source, bool looped, float maxTitlesDistance, float speed)
{
	_index = 0;
	_object = source;
	_soundName = name;

	_hasObject = source != NULL;
	_looped = looped;
	_paused = false;

	_forceTitles = false;
	_titlesSize = 0;
	
	_maxTitlesDistance = maxTitlesDistance;
	_speed = speed;

	_waiting = false;
	if (source)
	{
		Person *person = dyn_cast<Person>(source);
		if (person && person->Brain())
		{
			_waiting = true;
			_sender = person->Brain();
			SpeakingObjects.Add(this);
		}
	}
	if (!_waiting) LoadSound();
	
/*
	SoundPars pars;
	const ParamEntry *cls = FindSound(name, pars);

	if (Glob.config.showTitles && cls)
	{
		const ParamEntry &cfg = (*cls) >> "titles";
		int n = cfg.GetSize() / 2;
		for (int i=0; i<n; i++)
		{
			int index = _titles.Add();
			float t = cfg[2 * i];
			_titles[index].time = Glob.time + t;
			_titles[index].text = cfg[2 * i + 1];
		}
		SimulateTitles();
	}
*/
}

bool SoundObject::Simulate(float deltaT, SimulationImportance prec)
{
	if (_hasObject && (!_object || _object->IsDammageDestroyed()))
	{
		_sound = NULL;
		return false;
	}

	if (_waiting)
	{
		if (IsSpeakingDirect(_sender) || IsSpeakingRadio(_sender)) return true;
		_waiting = false;
		LoadSound();
	}

	if (_forceTitles || Glob.config.showTitles) SimulateTitles();

	if (_paused)
	{
		if (_sound) _sound = NULL;
	}
	else
	{
		if (_soundName.GetLength()>0 && !_sound)
		{
			StartSound();
		}
		if (_hasObject&& _sound)
		{
			if (_object)
				_sound->SetPosition
				(
					_object->WorldPosition(), _object->WorldSpeed()
				);
			else
			{
				// object destroyed - stop sound
				_sound = NULL;
			}
		}
		if (_sound && _sound->IsTerminated()) _sound = NULL;
	}
	
	return
	(
		_sound != NULL || ((_forceTitles || Glob.config.showTitles) && _index < _titles.Size())
	);
}

//! Show titles
/*!
	\patch 1.01 Date 06/12/2001 by Jirka
	- Fixed: titles was shown on unlimited distance
	- distance is limited to 100 m now
\patch 1.50 Date 4/2/2002 by Jirka
- Added: Enable different fonts for titles
*/

void SoundObject::SimulateTitles()
{
	if (_index >= _titles.Size()) return;
	if (Glob.time >= _titles[_index].time)
	{
		// FIX titles are shown only on limited distance
		if
		(
			!_object || !GScene->GetCamera() || _maxTitlesDistance == 0 ||
			GScene->GetCamera()->Position().Distance2(_object->WorldTransform().Position()) <= Square(_maxTitlesDistance)
		)
		{
			TitleEffect *effect = NULL;
			if (_titlesFont) effect = CreateTitleEffect(TitPlainDown, _titles[_index].text, _speed, _titlesFont, _titlesSize);
			else effect = CreateTitleEffect(TitPlainDown, _titles[_index].text, _speed);
			GWorld->SetTitleEffect(effect);
		}
		_index++;
	}
}

SoundOnVehicle::SoundOnVehicle(const char *name, Object *source, float maxTitlesDistance, float speed)
: Vehicle
	(
		Shapes.New
		(
			"data3d\\empty.p3d",
			false,true
		), VehicleTypes.New("SoundOnVehicle"), -1
	)
{
	_sound = new SoundObject(name, source, false, maxTitlesDistance, speed);
}

void SoundOnVehicle::Simulate( float deltaT, SimulationImportance prec )
{
	if (!_sound || !_sound->Simulate(deltaT, prec))
		SetDelete();
}

/*!
\patch 1.57 Date 5/16/2002 by Ondra
- Fixed: When one sound with no pauses is played in CfgSFX, it is now correctly looped.
*/

void DynSoundObject::Simulate( Object *source, float deltaT, SimulationImportance prec )
{
	if (prec>SimulateInvisibleNear)
	{
		if (_sound) _sound->LastLoop();
		return;
	}
	_timeToLive-=deltaT;
	if( _timeToLive>0 ) return; // continue playing or pause
	// select sound source
	float rand=GRandGen.RandomValue();
	const SoundEntry &pars=_dynSound->SelectSound(rand);
	bool loop = _dynSound->IsOneLoopingSound();
	// play selected sound
	AbstractWave *wave=NULL;
	if( pars.name[0] )
	{
		float rndFreq=1;
		float rndVol=1;
		if( pars.freqRnd ) rndFreq=1+GRandGen.RandomValue()*pars.freqRnd-pars.freqRnd*0.5;
		if( pars.volRnd ) rndVol=1+GRandGen.RandomValue()*pars.volRnd-pars.volRnd*0.5;
		if (loop)
		{
			wave=GSoundScene->OpenAndPlay
			(
				pars.name,
				source->WorldPosition(),source->WorldSpeed(),
				pars.vol*rndVol,pars.freq*rndFreq
			);
		}
		else
		{
			wave=GSoundScene->OpenAndPlayOnce
			(
				pars.name,
				source->WorldPosition(),source->WorldSpeed(),
				pars.vol*rndVol,pars.freq*rndFreq
			);
		}
		if( wave )
		{
			GSoundScene->AddSound(wave);
		}
	}
	// random time to live
	if (!loop)
	{
		float delay = GRandGen.Gauss(pars._min_delay, pars._mid_delay, pars._max_delay);
		if( wave ) delay+=wave->GetLength();
		_timeToLive=delay;
	}
	else
	{
		_timeToLive = 1e10;
	}
	if (_sound) _sound->LastLoop();
	_sound = wave;
}


DynSoundBank DynSounds;

// dynamic sounds:
// some technology to place dynamic sounds in landscape

DynSoundObject::DynSoundObject( const char *name )
:_timeToLive(0) // how long current sound will be played
{
	_dynSound=DynSounds.New(name);
}

void DynSoundObject::StopSound()
{
	if (_sound) _sound->LastLoop();
}

DynSoundObject::~DynSoundObject()
{
	StopSound();
}

DynSoundSource::DynSoundSource( const char *name )
:Vehicle(NULL,VehicleTypes.New("DynamicSound"),-1)
{
	// TODO: regular creation for serialization, network transport
	if (name) _dynSound=new DynSoundObject(name);
}

DynSoundSource::~DynSoundSource()
{
}

void DynSoundSource::StopSound()
{
	if (_dynSound) _dynSound->StopSound();
}

void DynSoundSource::Simulate( float deltaT, SimulationImportance prec )
{
	if (_dynSound) _dynSound->Simulate(this,deltaT,prec);
}

template Link<DynSound>;
