#ifdef _MSC_VER
#pragma once
#endif

#ifndef _AI_HPP
#define _AI_HPP

#include "types.hpp"
#include "aiTypes.hpp"
#include "transport.hpp"
//#include "house.hpp"
//#include "loadsave.hpp"
//#include "loadstream.hpp"
#include "paramArchive.hpp"
#include "speaker.hpp"

#include "time.hpp"

#include "camEffects.hpp"
#include "titEffects.hpp"

#include "ArcadeWaypoint.hpp"

#include "fsm.hpp"

#include "scripts.hpp"

#include <time.h>

///////////////////////////////////////////////////////////////////////////////
// class AI - base class for all classes in hierarchy

extern int MaxGroups;

struct ExperienceDestroyInfo
{
	float maxCost;
	float exp;
};
TypeIsSimple(ExperienceDestroyInfo)

extern AutoArray<float> ExperienceTable;
extern AutoArray<ExperienceDestroyInfo> ExperienceDestroyTable;

extern float ExperienceDestroyEnemy;
extern float ExperienceDestroyFriendly;
extern float ExperienceDestroyCivilian;

extern float ExperienceRenegadeLimit;

extern float ExperienceKilled;
	
// for subordinate soldier only
extern float ExperienceCommandCompleted;
extern float ExperienceCommandFailed;
extern float ExperienceFollowMe;

// for leadership only
extern float ExperienceDestroyYourUnit; 
extern float ExperienceMissionCompleted;
extern float ExperienceMissionFailed;

struct SynchronizedGroup
{
	OLink<AIGroup> group;
	bool active;

	LSError Serialize(ParamArchive &ar);
};
TypeIsGeneric(SynchronizedGroup);

struct SynchronizedSensor
{
	OLink<Vehicle> sensor;
	bool active;

	LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(SynchronizedSensor);

struct SynchronizedItem
{
	AutoArray<SynchronizedGroup> groups;
	AutoArray<SynchronizedSensor> sensors;

	void Add(AIGroup *grp);
	void Add(Vehicle *sensor);

	void SetActive(AIGroup *grp, bool active = true);
	void SetActive(Vehicle *sensor, bool active = true);

	bool IsActive(AIGroup *grp = NULL);

	LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(SynchronizedItem);

extern OLinkArray<EntityAI> vehiclesMap;
extern OLinkArray<Vehicle> sensorsMap;
extern AutoArray<ArcadeMarkerInfo> markersMap;
extern AutoArray<SynchronizedItem> synchronized;

///////////////////////////////////////////////////////////////////////////////
// class AbstractAIMachine

template <class Task, class ContextType>
class AbstractAIMachine
{
protected:
	struct Item
	{
		SRef<FSM> _fsm;
		Ref<Task> _task;
		LSError Serialize(ParamArchive &ar)
		{
			CHECK(ar.Serialize("Task", _task, 1))
			if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
				_fsm = AbstractAIMachine<Task, ContextType>::CreateFSM(_task->GetType());
			CHECK(ar.Serialize("FSM", *_fsm, 1))
			return LSOK;
		}
		ClassIsGeneric(Item);
	};
	AutoArray<Item> _stack;
	SRef<FSM> _noTaskFSM;
public:
	AbstractAIMachine(ContextType *context = NULL)
	{
		_noTaskFSM = CreateFSM(0);
		if (context)
		{
			context->_task = NULL;
			context->_fsm = _noTaskFSM;
		}
		_noTaskFSM->Enter(context);
		_noTaskFSM->SetState(0, context);
	}
	~AbstractAIMachine() {}

	LSError Serialize(ParamArchive &ar);
	
	static FSM *CreateFSM(int taskType);
	void PushTask(Task &task,ContextType *context);									// add task to top of stack and make it current
	void EnqueueTask(Task &task,ContextType *context);							// add task to bottom of stack
	void PopTask(ContextType *context, bool doRefresh = true);			// remove task from top of stack
	void Delete(int i, ContextType *context, bool doRefresh = true);// remove task from top of stack
	void Clear(ContextType *context = NULL);												// remove all tasks from stack
	bool Update(ContextType *context);
	void UpdateAndRefresh(ContextType *context);
	Item *GetCurrent()
	{int n = _stack.Size(); return n == 0 ? NULL : &_stack[n - 1];}
	const Item *GetCurrent() const
	{int n = _stack.Size(); return n == 0 ? NULL : &_stack[n - 1];}

	virtual void OnTaskCreated(int index, Task &task) {};
	virtual void OnTaskDeleted(int index, Task &task) {};
protected:
	void Enter(ContextType *context);
	void Exit(ContextType *context);
};

template <class Task, class ContextType>
LSError AbstractAIMachine<Task, ContextType>::Serialize(ParamArchive &ar)
{
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
		_noTaskFSM = CreateFSM(0);
	CHECK(ar.Serialize("NoTaskFSM", *_noTaskFSM, 1))
	CHECK(ar.Serialize("Stack", _stack, 1))
	return LSOK;
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::PushTask(Task &task, ContextType *context)
{
	Assert(task.GetType() > 0);	// Task 0 is used when no task in stack
	if (task.GetType() <= 0)
		return;
	Exit(context);
	int index = _stack.Add();
	Assert(GetCurrent());
	_stack[index]._task = new Task(task);
	_stack[index]._fsm = CreateFSM(task.GetType());
	OnTaskCreated(index, *GetCurrent()->_task);
	context->_task = GetCurrent()->_task;
	context->_fsm = GetCurrent()->_fsm;
	GetCurrent()->_fsm->Enter(context);
	GetCurrent()->_fsm->SetState(0, context);	// State 0 is starting state
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::EnqueueTask(Task &task, ContextType *context)
{
	Assert(task.GetType() > 0);	// Task 0 is used when no task in stack
	if (task.GetType() <= 0)
		return;
	if (_stack.Size() <= 0)
	{
		PushTask(task, context);
		return;
	}
	_stack.Insert(0);
	_stack[0]._task = new Task(task);
	_stack[0]._fsm = CreateFSM(task.GetType());
	OnTaskCreated(0, *_stack[0]._task);
	_stack[0]._fsm->SetState(0); // State 0 is starting state
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::PopTask(ContextType *context, bool doRefresh)
{
	int n = _stack.Size() - 1;
	if (n < 0)
	{
		return;
	}

	Exit(context);
	OnTaskDeleted(n, *_stack[n]._task);
	_stack.Delete(n);

	Enter(context);
	if (doRefresh) UpdateAndRefresh(context);
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::Delete(int i,ContextType *context, bool doRefresh)
{
	if (GetCurrent())
	{
		context->_task = GetCurrent()->_task;
		context->_fsm = GetCurrent()->_fsm;
	}
	else
	{
		context->_task = NULL;
		context->_fsm = _noTaskFSM;
	}

	if( i==_stack.Size()-1 ) PopTask(context, doRefresh);
	else
	{
		OnTaskDeleted(i, *_stack[i]._task);
		_stack.Delete(i);
	}
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::Clear(ContextType *context)
{
	if (_stack.Size() == 0)
	{
		return;
	}

	Exit(context);
	for (int i=_stack.Size()-1; i>=0; i--)
		OnTaskDeleted(i, *_stack[i]._task);
	_stack.Clear();

	if (context)
	{
		Enter(context);
		UpdateAndRefresh(context);
	}
}

template <class Task, class ContextType>
bool AbstractAIMachine<Task, ContextType>::Update(ContextType *context)
{
	if (GetCurrent())
	{
		context->_task = GetCurrent()->_task;
		context->_fsm = GetCurrent()->_fsm;
		if (GetCurrent()->_fsm->Update(context))
		{
			PopTask(context);
			return true;
		}
	}
	else
	{
		context->_task = NULL;
		context->_fsm = _noTaskFSM;
		_noTaskFSM->Update(context);
	}
	return false;
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::UpdateAndRefresh(ContextType *context)
{
	if (GetCurrent())
	{
		context->_task = GetCurrent()->_task;
		context->_fsm = GetCurrent()->_fsm;
	}
	else
	{
		context->_task = NULL;
		context->_fsm = _noTaskFSM;
	}
	context->_fsm->Refresh(context);
	Update(context);
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::Enter(ContextType *context)
{
	if (GetCurrent())
	{
		context->_task = GetCurrent()->_task;
		context->_fsm = GetCurrent()->_fsm;
	}
	else
	{
		context->_task = NULL;
		context->_fsm = _noTaskFSM;
	}
	context->_fsm->Enter(context);
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::Exit(ContextType *context)
{
	if (GetCurrent())
	{
		context->_task = GetCurrent()->_task;
		context->_fsm = GetCurrent()->_fsm;
	}
	else
	{
		context->_task = NULL;
		context->_fsm = _noTaskFSM;
	}
	context->_fsm->Exit(context);
}

///////////////////////////////////////////////////////////////////////////////
// class AILocker

class RoadLink;
//TypeIsSimpleZeroed(InitPtr<RoadLink>);

struct BuildingLockInfo
{
	OLink<Object> house;
	int index;
};
TypeContainsOLink(BuildingLockInfo)

class AILocker : public RefCount
{
	AutoArray< InitPtr<LockField> > _fields;	// locked fields
	AutoArray< InitPtr<RoadLink> > _roads;	// locked roads
	AutoArray<BuildingLockInfo> _buildings;	// locked building positions
public:
	AILocker();
	~AILocker();

	void LockPosition( Vector3Val pos, float radius, bool soldier, float size);
	void UnlockPosition( Vector3Val pos, float radius, bool soldier);

	void LockPositionMan( Vector3Val pos, float radius);
	void UnlockPositionMan( Vector3Val pos, float radius);

protected:
	// implementation
	void LockItem(int x, int z, bool soldier);
	void UnlockItem(int x, int z, bool soldier);
};

///////////////////////////////////////////////////////////////////////////////
// class AIUnit

extern const Vector3 VUndefined;

#include "pathSteer.hpp"

//! network message indices for AIUnit class
class IndicesCreateAIUnit : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;

public:
	//@{
	//! index of field in message format
	int person;
	int subgroup;
	int id;
	int name;
	int face;
	int glasses;
	int speaker;
	int pitch;
	int rank;
	int experience;
	int initExperience;
	int playable;
	int squadPicture;
	int squadTitle;
	//@}

	//! Constructor
	IndicesCreateAIUnit();
	NetworkMessageIndices *Clone() const {return new IndicesCreateAIUnit;}
	void Scan(NetworkMessageFormatBase *format);
};

//! callback function type - see also FindNearestEmptyCallback
typedef bool FindFreePositionCallback(Vector3Par pos, void *context);

extern bool DefFindFreePositionCallback(Vector3Par pos, void *context);

//! AI structure representing brain of single unit

class AIUnit : public AI
{
friend class AISubgroup;
friend class AIGroup;
friend class AICenter;
public:
	enum State
	{
		Wait,
		Init,
		Busy,
		Completed,
		Delay,
		InCargo,
		Stopping,
		Replan,
		Stopped,
		Sending
	};
	enum Mode
	{
		DirectNormal,
		DirectExact,
		Normal,
		Exact
	};
	enum PlanningMode
	{
		DoNotPlan,
		LeaderPlanned,
		LeaderDirect,
		FormationPlanned,
		VehiclePlanned,
	};
	enum ResourceState
	{
		RSNormal,
		RSLow,
		RSCritical
	};
	enum WatchMode
	{
		WMNo,
		WMDir,WMPos,WMTgt,
		WMAround,
		NWatchModes
	};
	enum LifeState
	{
		LSAlive,
		LSDead,
		LSDeadInRespawn,
		LSAsleep,
		LSUnconscious,
		NLifeStates
	};
	enum DisabledAI
	{
		DATarget=1, // group commander may not assign targets
		DAMove=2, // do not move
		DAAutoTarget=4, // no automatic target selection
		DAAnim=8, // no automatic animation selection
		// note: serialized as value - only add values to retain compatilibity
	};
protected:
	// structure
	OLink<Person> _person;		// who we are
	OLink<Transport> _inVehicle;	// which vehicle are we in
	OLink<AISubgroup> _subgroup;

	// info
	int _id;					// id in group
//	int _speaker;
	SRef<Speaker> _speaker;
	OLink<Transport> _vehicleAssigned;
	Semaphore _semaphore;
	FormationPos _formPos;
	float _formPosCoef;
	CombatMode _combatModeMajor;
	Time _dangerUntil;

	// random from 0 to 1 - when to perform expensive think
	int _expensiveThinkFrac; // frac in ms
	Time _expensiveThinkTime;

	void ExpensiveThinkDone(); // calculate time of next expensive think

	int _disabledAI; // disabled single elements of AI (DisabledAI)

	float _ability;
	float _invAbility;
	
	LinkTarget _targetAssigned; // prefered target for attacking / watching
	LinkTarget _targetEngage; // enabled engaging this target
	LinkTarget _targetEnableFire; // enabled firing at this target

	// strategic plan
	Vector3 _wantedPosition;
	Vector3 _plannedPosition;
	PlanningMode _planningMode;
	
#if _ENABLE_AI
	SRef<IAIPathPlanner> _planner;
	float _completedTime;
	Time _waitWithPlan;
	int _attemptPlan;
#endif

	float _exposureChange;
	float _nearestEnemyDist2;

	bool _noPath;
	bool _updatePath;
	bool _lastPlan;

	// captive
	bool _captive;

	// used for MP
	bool _playable;	// unit may be respawned

	LifeState _lifeState;

	// actual instructions
	Path _path;// operative path
	OLink<Object> _house;
	int _housePos;

	WatchMode _watchMode;
	Vector3 _watchPos; // position to watch
	LinkTarget _watchTgt; // target to watch
	Time _watchDirSet; // for WMAround
	Vector3 _watchDir; //!< direction to watch by head and body
	Vector3 _watchDirHead; //!< direction to watch by head

	// used when vehicle is static and no target is locked

	float _formationAngle;
	Vector3 _formationPos; // position in formation (relative to leader)

	Point3 _expPosition;
	State _state;
	Mode _mode;

	// counter for trying of finding path
	Time _delay;
	int _iter;

	bool _getInAllowed;
	bool _getInOrdered;

	bool _isAway;

	bool _completedReceived;

	// messages
	ResourceState _lastFuelState;
	ResourceState _lastHealthState;
	ResourceState _lastArmorState;
	ResourceState _lastAmmoState;

	Time _awayTime;
	Time _fuelCriticalTime;
	Time _healthCriticalTime;
	Time _dammageCriticalTime;
	Time _ammoCriticalTime;

public:
	AIUnit( Person *vehicle );
	~AIUnit();
	void Load(const ParamEntry &cls);	// init from config

	LSError Serialize(ParamArchive &ar);
	static AIUnit *CreateObject(ParamArchive &ar) {return new AIUnit(NULL);}
	static AIUnit *LoadRef(ParamArchive &ar);
	LSError SaveRef(ParamArchive &ar) const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	static AIUnit *CreateObject(NetworkMessageContext &ctx);
	void DestroyObject();
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
	Vector3 GetCurrentPosition() const {return Position();}

	// access to data members
	bool IsPlayable() const {return _playable;}
	void SetPlayable(bool set = true) {_playable = set;}
	Speaker *GetSpeaker() const {return _speaker;}
	void SetSpeaker(RString speaker, float pitch);
	Person* GetPerson() const {return _person;}
	void SetPerson( Person *vehicle ){_person=vehicle;}
	Transport* GetVehicleIn() const {return _inVehicle;}
	void SetVehicleIn(Transport *vehicle) {_inVehicle = vehicle;}
	EntityAI* GetVehicle() const;
	bool IsSoldier() const
	{
		return _inVehicle.GetLink() == NULL || IsInCargo();
	}
	bool IsFreeSoldier() const {return _inVehicle.GetLink() == NULL;}
	bool IsSubgroupLeader() const;
	bool IsSubgroupLeaderVehicle() const;
	bool IsGroupLeader() const;
	AISubgroup* GetSubgroup() const;
	AIGroup* GetGroup() const;
	bool IsDanger() const;
	void SetDanger(float until = -1.0);

	bool IsAway() const {return _isAway;}
	void SetAway( bool val=true ) {/*_isAway=val;*/}

	void SetAwayTime(Time awayTime){_awayTime=awayTime;}

	int GetIter() const {return _iter;}
	
	Transport *VehicleAssigned() const {return _vehicleAssigned;}

	bool AssignAsDriver(Transport *veh);
	bool AssignAsGunner(Transport *veh);
	bool AssignAsCommander(Transport *veh);
	bool AssignAsCargo(Transport *veh);
	void UnassignVehicle();

	bool IsGetInAllowed() const {return _getInAllowed;}
	bool IsGetInOrdered() const {return _getInOrdered;}
	void AllowGetIn(bool flag);
	void OrderGetIn(bool flag) {_getInOrdered = flag;}

	void SetWantedPosition(Vector3Par pos, PlanningMode mode, bool forceReplan = false);
	Vector3Val GetWantedPosition() const {return _wantedPosition;}
	PlanningMode GetPlanningMode() const {return _planningMode;}
	bool IsPathValid() const;

	void SetNoWatch();
	void SetWatchPosition( Vector3Val pos );
	void SetWatchTarget( Target *tgt );
	void SetWatchDirection( Vector3Val dir );
	void SetWatchAround();

	Vector3Val GetWatchDirection() const {return _watchDir;}
	Vector3Val GetWatchHeadDirection() const {return _watchDirHead;}
	WatchMode GetWatchMode() const {return _watchMode;}

	Target *GetTargetAssigned() const {return _targetAssigned;}
	void AssignTarget(Target *target) {_targetAssigned = target;}

	Target *GetEngageTarget() const {return _targetEngage;}
	void EngageTarget(Target *target) {_targetEngage = target;}

	Target *GetEnableFireTarget() const {return _targetEnableFire;}
	void EnableFireTarget(Target *target) {_targetEnableFire = target;}

	float GetTimeToLive() const;

	Vector3 COMPosition() const {return GetVehicle()->COMPosition();}

	Vector3 VisiblePosition() const {return GetVehicle()->VisiblePosition();}
	Vector3 AimingPosition() const {return GetVehicle()->AimingPosition();}
	float VisibleSize() const {return GetVehicle()->VisibleSize();}

#if _ENABLE_AI
	const IAIPathPlanner &GetPlanner() const {return *_planner;}

	float GetTimeCompleted() {return _completedTime;}
	void SetTimeCompleted(float time) {_completedTime = time;}
#endif

	void ExposureChanged(int x, int z, float optimistic, float pessimistic);
	float GetExposureChange() const {return _exposureChange;}
	void ClearExposureChange() {_exposureChange = 0;}

	Vector3Val Position() const {return GetVehicle() ? GetVehicle()->Position() : VZero;}
	Vector3Val Direction() const {return GetVehicle() ? GetVehicle()->Direction() : VForward;}

	State GetState() const {return _state;}
	bool SetState(State state);

	LifeState GetLifeState() const {return _lifeState;}
	void SetLifeState(LifeState state) {_lifeState=state;}

	int GetAIDisabled() const {return _disabledAI;}
	void SetAIDisabled(int state) {_disabledAI=state;}

	Mode GetMode() const {return _mode;}
	void SetMode(Mode mode) {_mode = mode;}
	bool HasAI() const;
	bool IsAnyPlayer() const; // any player - local or remote
	bool IsPlayer() const; // main (local) player
	bool IsPlayerDriven() const;
	int ID() const {return _id;}
/*
	AIUnitInfo& GetInfo() {return _info;}
	void SetInfo(AIUnitInfo& info) {_info = info;}
	float GetExperience() const {return _info._experience;}
	Rank GetRank() const {return _info._rank;}
	void SetRank(Rank rank) {_info._rank = rank;}
*/

	float GetNearestEnemyDist2() const {return _nearestEnemyDist2;}
	void SetNearestEnemyDist2(float val) {_nearestEnemyDist2=val;}

	bool GetCaptive() const {return _captive;}
	void SetCaptive(bool captive) {_captive=captive;}
	
	float GetRandomizedExperience() const;
	void AddExp(float exp);
	void Disclose( bool discloseGroup=true ); // unit was disclosed - do something

	float GetInvAbility() const; // returns from 5 (unable) to 1 (maximal)
	float GetAbility() const; // returns from 1 (maximal) to 0.2 (unable)
	void SetAbility(float ability);

	bool IsUnit() const;
	bool IsCommander() const;
	bool IsDriver() const;
	bool IsGunner() const;
	bool IsInCargo() const;

	Path& GetPath() {return _path;}
	const Path& GetPath() const {return _path;}
	int OperIndex() const {return _path.GetOperIndex();}
	int MaxOperIndex() const {return _path.GetMaxIndex();}
	void SetHouse(Object *house, int pos) {_house = house; _housePos = pos;}
#if _ENABLE_AI
	void CopyPath(const IAIPathPlanner &planner);	
#endif

	UnitPosition GetUnitPosition() const;
	void SetUnitPosition(UnitPosition status);

	Semaphore GetSemaphore() const {return _semaphore;}
	void SetSemaphore(Semaphore status);
	bool IsHoldingFire() const;
	bool IsKeepingFormation() const;

	bool IsFireEnabled(Target *tgt) const; // check if fire enabled at given target
	bool IsEngageEnabled(Target *tgt) const; // check if fire enabled at given target

	FormationPos GetFormationPos() const {return _formPos;}
	void SetFormationPos(FormationPos status);
	void AddFormationPos(FormationPos status);

	CombatMode GetCombatMode() const;
	CombatMode GetCombatModeMajor() const {return _combatModeMajor;}
	void SetCombatModeMajor(CombatMode mode) {_combatModeMajor = mode;}
	void ReportStatus();
	float GetInvAverageSpeed() const;
	float GetAverageSpeed() const;

	Vector3 GetFormationRelative() const;
	Vector3 GetFormationAbsolute() const; // absolute formation position
	Vector3 GetFormationAbsolute( AIUnit *leader) const; // absolute formation position

	float GetFormationAngleRelative() const {return _formationAngle;}

	bool IsSimplePath(Vector3Val from, Vector3Val pos);
#if _ENABLE_AI
	bool CreatePath(Vector3Val from, Vector3Val pos);
	bool VerifyPath();
#endif

	ResourceState GetFuelState() const;
	ResourceState GetHealthState() const;
	ResourceState GetArmorState() const;
	ResourceState GetAmmoState() const;

	void CheckAmmo
	(
		const MuzzleType * &muzzle1, const MuzzleType * &muzzle2,
		int &slots1, int &slots2, int &slots3
	);
	const AITargetInfo *CheckAmmo(ResourceState state);
	void CheckAmmo();

	Time GetDelay() const {return _delay;}

	void IncreaseExperience(const VehicleType& type, TargetSide side); // called when you kill/destroy sth.
	static bool FindFreePosition
	(
		Vector3 &pos, Vector3 &normal, bool soldier, EntityAI *veh,
		FindFreePositionCallback *isFree=DefFindFreePositionCallback,
		void *context=NULL
	);
	bool FindFreePosition( Vector3 &pos, Vector3 &normal );
	bool FindFreePosition();
	
	bool CheckEmpty(Vector3Par pos);
	static bool FindNearestEmpty(Vector3 &pos, bool soldier, EntityAI *veh);
	bool FindNearestEmpty( Vector3 &pos );

	float GetFeeling() const;

	// changes of structure
	void RemoveFromSubgroup();
	void RemoveFromGroup();
	void ForceRemoveFromGroup();
	bool ProcessGetIn(Transport &veh);
	bool ProcessGetIn2(Transport *veh, UIActionType pos);
	void CheckIfAliveInTransport();
	bool ProcessGetOut(bool parachute);
	void DoGetOut(Transport *veh, bool parachute);
	void IssueGetOut();
	
	// mind
	bool Think(ThinkImportance prec); // if true, OperPath was called

	// communication with group
	void SendAnswer(Answer answer);
	void RefreshMission();

	RString GetDebugName() const;
	bool AssertValid() const;
	void Dump(int indent = 3) const;

public:
#if _ENABLE_AI
	bool CreateRoadPath(Vector3Val from, Vector3Val pos);
#endif

	void ForceReplan(bool clear = true);

protected:
	// implementation
#if _ENABLE_AI
	bool CreateNoRoadPath
	(
		Vector3Val from, Vector3Val pos,
		const IPaths *fromBuilding = NULL,
		const IPaths *toBuilding = NULL
	);
#endif

	// think subtasks
	void SetWatch();
	void CheckResources();
	void CheckGetOut();
	void CheckGetInGetOut();

#if _ENABLE_AI
public:
	void CreateStrategicPath(ThinkImportance prec);
protected:
	void OnStrategicPathFound();
	void OnStrategicPathNotFound(bool update);

	void OperPath(ThinkImportance prec);
	bool CreateOperativePath();
#endif

protected:
	void ClearOperativePlan();
	void ClearStrategicPlan();
	void RefreshStrategicPlan();

private: // disable copy
	void operator = ( const AIUnit &src );
};

///////////////////////////////////////////////////////////////////////////////
// class AISubgroup

struct FormInfo
{
	int base;
	Vector3 position;
	float angle;

	FormInfo(int b, float x, float z, float a)
	{
		base = b;
		position.Init();
		position[0] = x; position[1] = 0; position[2] = z;
		angle = a;
	}
};

extern const FormInfo formations[AI::NForms][MAX_UNITS_PER_GROUP];

//! network message indices for Command class
class IndicesCreateCommand : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;

public:
	//@{
	//! index of field in message format
	int subgroup;
	int index;
	int message;
	int target;
	int targetE;
	int destination;
	int time;
	int join;
	int action;
	int param;
	int param2;
	int param3;
	int discretion;
	int context;
	int id;
	//@}

	//! Constructor
	IndicesCreateCommand();
	NetworkMessageIndices *Clone() const {return new IndicesCreateCommand;}
	void Scan(NetworkMessageFormatBase *format);
};


// fix for a bug makes MP incompatible with 1.52
#define ENABLE_HOLDFIRE_FIX 1

class Command : public NetworkObject, public SerializeClass
{
protected:
	NetworkId _networkId;
	bool _local;	// local / remote in network game

public:
	enum Message
	{
		NoCommand,
		Wait,
		Attack,
		Hide,
		Move,
		// supplied units
		Heal,
		Repair,
		Refuel,
		Rearm,
		// supplying units
		Support,
		Join,
		GetIn,
		Fire,
		GetOut,
		Stop,
		Expect,
		Action,
		#if ENABLE_HOLDFIRE_FIX
		AttackAndFire,
		#else
		AttackAndFire=Attack,
		__ContinueEnum=Action,
		#endif
	};
	enum Discretion
	{
		Undefined = 0,
		Major,
		Normal,
		Minor
	};
	enum Context
	{
		CtxUndefined,
		CtxAuto,
		CtxAutoSilent,
		CtxJoin,
		CtxAutoJoin,
		CtxEscape,
		CtxMission,
		CtxUI,
		CtxUIWithJoin
	};
	// exact values
	Message _message;
	// _target is exact vehicle information
	// it should be used for friendly/well known targets (static ets.)
	TargetId _target;	// TODO: save both _target and _targetID
	// _targetE should be used for enemy targets (esp. for attack)
	LinkTarget _targetE;
	Vector3 _destination;
	Time _time;
	OLink<AISubgroup> _joinToSubgroup;
	UIActionType _action;
	int _param; // weapon for fire ...
	int _param2;
	RString _param3;
	Discretion _discretion;
	Context _context;

	int _id;
public:	
	Command()
	{
		_local = true;

		_message = NoCommand;
		_destination=VZero;
		_time = TIME_MIN;
		_action = (UIActionType)0;
		_param = -1;
		_param2 = -1;
		_discretion = Undefined;
		_context = CtxUndefined;
		_id = -1;
	}
// functions excepted by AbstractAIMachine
	int GetType() {return _message;}

	LSError Serialize(ParamArchive &ar);
	static Command *CreateObject(ParamArchive &ar) {return new Command();}

	NetworkId GetNetworkId() const {return _networkId;}
	void SetNetworkId(NetworkId &id) {_networkId = id;}
	bool IsLocal() const {return _local;}
	void SetLocal(bool local = true) {_local = local;}

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
	static Command *CreateObject(NetworkMessageContext &ctx);
	void DestroyObject();
	Vector3 GetCurrentPosition() const;
	RString GetDebugName() const;
};

template<>
const EnumName *GetEnumNames(Command::Message dummy);
template<>
const EnumName *GetEnumNames(Command::Discretion dummy);
template<>
const EnumName *GetEnumNames(Command::Context dummy);

enum CommandState
{
	CSSent,
	CSReceived,
	CSSucceed,
	CSFailed
};

void SetCommandState(int id, CommandState state, AISubgroup *subgrp);

struct AISubgroupContext
{
	Command *_task;		// expected parameter, supply by Abstract Machine
	AISubgroup *_subgroup;
	FSM *_fsm;

	AISubgroupContext()
		{_task = NULL; _subgroup = NULL; _fsm = NULL;}
	AISubgroupContext(AISubgroup *subgroup)
		{_task = NULL; _subgroup = subgroup; _fsm = NULL;}
};

#include <Es/Memory/normalNew.hpp>

class AISubgroupFSM: public FSMTyped<AISubgroupContext>
{
	typedef FSMTyped<AISubgroupContext> base;

	public:
	AISubgroupFSM
	(
		const StateInfo *states, int n,
		const pStateFunction *functions = NULL, int nFunc = 0
	);
	~AISubgroupFSM();

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

// export global fsm functions

void CommandSucceed(AISubgroupContext *context);
void CommandFailed(AISubgroupContext *context);
void CheckCommandSucceed(AISubgroupContext *context);
void CheckCommandFailed(AISubgroupContext *context);

void CreateUnitsList(PackedBoolArray list, char *buffer);

#include "pathPlanner.hpp"


//! network message indices for AISubgroup class
class IndicesUpdateAISubgroup : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;

public:
	//@{
	//! index of field in message format
	int group;
	int units;
	int leader;
	int mode;
	int wantedPosition;
	int formation;
	int speedMode;
	int lastPrec;
	int formationCoef;
	int direction;
	//@}

	//! Constructor
	IndicesUpdateAISubgroup();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateAISubgroup;}
	void Scan(NetworkMessageFormatBase *format);
};

class AISubgroup
	:	public AI, public AbstractAIMachine<Command, AISubgroupContext>
{
friend class AIGroup;
typedef AbstractAIMachine<Command, AISubgroupContext> base;

public:
	enum Mode
	{
		Wait,
		PlanAndGo,
		DirectGo
	};

protected:
	// structure
	RefArray<AIUnit> _units;
	OLink<AIUnit> _whoAmI;
	OLink<AIGroup> _group;

	Mode _mode;
	Time _refreshTime;
	Vector3 _wantedPosition;

	// operative control
	Formation _formation;

	SpeedMode _speedMode;

	// flags
	ThinkImportance _lastPrec;
	
	bool _avoidRefresh;
	bool _doRefresh;

	float _formationCoef;
	Time _formationCoefChanged;

	Vector3 _direction;
	Time _directionChanged;

public:
	AISubgroup();
	~AISubgroup();

	LSError Serialize(ParamArchive &ar);
	static AISubgroup *CreateObject(ParamArchive &ar) {return new AISubgroup();}
	static AISubgroup *LoadRef(ParamArchive &ar);
	LSError SaveRef(ParamArchive &ar) const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	static AISubgroup *CreateObject(NetworkMessageContext &ctx);
	void DestroyObject();
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
	Vector3 GetCurrentPosition() const {return Leader() ? Leader()->Position() : VZero;}

	void OnTaskCreated(int index, Command &cmd);
	void OnTaskDeleted(int index, Command &cmd);
	void InsertCommand(int index, Command *cmd);
	void DeleteCommand(int index, Command *cmd);

	// access to data members
	AIUnit* Leader() const {return _whoAmI;} 
	AIUnit *Commander() const;

	AIGroup* GetGroup() const;
	int NUnits() const {return _units.Size();}
	AIUnit *GetUnit( int i ) const {return _units[i];} // caution - can be NULL
	bool HasAI()
		{return Leader() && Leader()->HasAI();}
	bool IsPlayerSubgroup()
		{return Leader() && Leader()->IsPlayer();}
	bool IsAnyPlayerSubgroup()
		{return Leader() && Leader()->IsAnyPlayer();}
	bool IsPlayerDrivenSubgroup()
		{return Leader() && Leader()->IsPlayerDriven();}
	bool HasCommand() {return GetCurrent() != NULL;}
	const Command *GetCommand() const
		{return (GetCurrent() != NULL) ? GetCurrent()->_task : NULL;}
	Command *GetCommand()
		{return (GetCurrent() != NULL) ? GetCurrent()->_task : NULL;}
	Formation GetFormation() const {return _formation;}
	void SetFormation(Formation f);

	bool IsAvoidingRefresh() const {return _avoidRefresh;}
	void AvoidRefresh(bool set = true) {_avoidRefresh = set;} 

	Mode GetMode() const {return _mode;}
	Vector3Val GetFormationDirection() const {return _direction;}
	void SetFormationDirection(Vector3Par dir) {_direction = dir;}
	AIUnit *GetFormationPrevious( AIUnit *unit ) const;
	AIUnit *GetFormationNext( AIUnit *unit ) const;

	SpeedMode GetSpeedMode() const {return _speedMode;}
	void SetSpeedMode(SpeedMode speed) {_speedMode = speed;}

	// changes of structure
	void UnitRemoved(AIUnit *unit);
	void UnitReplaced(AIUnit *unitOld, AIUnit* unitNew);
	void AddUnit(AIUnit *unit);
	void AddUnitWithCargo(AIUnit *unit);
	void SelectLeader(AIUnit *unit = NULL);
	void RemoveFromGroup();

	void JoinToSubgroup(AISubgroup *subgrp); // remove all units and then remove from group

	void UpdateFormationPos();
	void UpdateFormationCoef();
	void UpdateFormationDirection();

	void SetDirection(Vector3Val dir);

	// mind
	ThinkImportance CalculateImportance();
	bool Think(ThinkImportance prec); // if true, OperPath was called

	// communication with units
	void ReceiveAnswer(AIUnit* from, Answer answer);

	void SetWaypoints( int animationIndex, int vehicleIndex, bool direct=true );

	// communication with group
	void ClearAllCommands();
	void ClearMissionCommands();
	void ClearEscapeCommands();
	void ClearAttackCommands();
	void ClearGetInCommands();
	bool CheckHide() const;
	void ReceiveCommand(Command &cmd);
	void SendAnswer(Answer answer);
	void OnEnemyDetected(const VehicleType *type, Vector3Val pos);

	// void DoRefresh();
	void FailCommand();

	PackedBoolArray GetUnitsList();
	PackedBoolArray GetUnitsListNoCargo();
	bool FindNearestSafe(int &x, int &z, float threshold);
	bool FindNearestSafer(int &x, int &z);
	void ExposureChanged(int x, int z, float optimistic, float pessimistic);

	// interface for fsm
	void SetDiscretion(Command::Discretion discretion);
	void SetRefreshTime(Time time) {_refreshTime = time;}
	bool AllUnitsCompleted();
	void GoDirect(Vector3Val pos);
	void GoPlanned(Vector3Val pos);
	void DoNotGo();
	void Stop();

	void ClearPlan();
	void RefreshPlan();

	// debug functions
	RString GetDebugName() const;
	bool AssertValid() const;
	void Dump(int indent = 2) const;

	int StackSize() const {return _stack.Size();}

public:
	float GetFieldCost(int x, int z);

	float GetExposure(int x, int z);

protected:
	// implementation
	// strategic planning
	float CalculateExposure(int x, int z);
	bool IsSafe(int x, int z, float threshold);
	bool GetSafety(int x, int z, float &exposure);

private:
	// disable copy
	AISubgroup( const AISubgroup &src );
	void operator = ( const AISubgroup &src );
};

///////////////////////////////////////////////////////////////////////////////
// class Team

enum Team
{
	TeamMain,
	TeamRed,
	TeamGreen,
	TeamBlue,
	TeamYellow,
	NTeams
};


///////////////////////////////////////////////////////////////////////////////
// class AIGroup

enum AIGroupType
{
	GTMilitary = 0,
	GTNone = GTMilitary,
	GTHeal,
	GTRepair,
	GTRefuel,
	GTRearm,
	GTShip,
};


enum ReportSubject
{
	ReportNew,
	ReportDestroy
};

enum MissionStatus
{
	MSNone,
	MSCompleted,
	MSFailed
};

class Mission : public RefCount, public SerializeClass
{
public:
	enum Action
	{
		NoMission,
		Arcade
	};

	Action _action;
	TargetId _target;
	Vector3 _destination;

public:	
	Mission()
	{
		_action = NoMission;
		_target = NULL;
		_destination=VZero;
	}
// functions excepted by AbstractAIMachine
	int GetType() {return _action;}
	LSError Serialize(ParamArchive &ar);
	static Mission *CreateObject(ParamArchive &ar) {return new Mission();}
};

struct AIGroupContext
{
	Mission *_task;		// expected parameter, supply by Abstract Machine
	AIGroup *_group;
	FSM *_fsm;				// expected parameter, supply by Abstract Machine

	AIGroupContext()
		{_task = NULL; _group = NULL; _fsm = NULL;}
	AIGroupContext(AIGroup *group)
		{_task = NULL; _group = group; _fsm = NULL;}
};

typedef FSMTyped<AIGroupContext> AIGroupFSM;
AIGroupFSM *NewArcadeFSM();

void MissionSucceed(AIGroupContext *context);
void CheckMissionSucceed(AIGroupContext *context);
void CheckMissionFailed(AIGroupContext *context);
void MissionFailed(AIGroupContext *context);

class RadioMessageState;

enum OpenFireState
{
	OFSNeverFire,
	OFSHoldFire,
	OFSOpenFire
};

//! network message indices for AIGroup class
class IndicesUpdateAIGroup : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;

public:
	//@{
	//! index of field in message format
	int mainSubgroup;
	int leader;
	int semaphore;
	int combatModeMinor;
	// ??	_lastEnemyDetected
	// ?? _nextCmdId
	// ?? _locksWP
	int enemiesDetected;
	int unknownsDetected;
	// ?? int disclosed;
	// TODO: _targetList
	// ?? _vehicles
	// ?? _overlookTarget
	// ?? _guardPosition
	// TODO: waypoints
	// ?? _maxStrength
	int forceCourage;
	int courage;
	int flee;
	// ?? _threshold
	// ?? _thresholdValid

	int waypointIndex;
	//@}

	//! Constructor
	IndicesUpdateAIGroup();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateAIGroup;}
	void Scan(NetworkMessageFormatBase *format);
};

class AIGroup
	:	public AI, public AbstractAIMachine<Mission, AIGroupContext>
{
	friend class AISubgroup;
	friend class AICenter;

	typedef AbstractAIMachine<Mission, AIGroupContext> base;
protected:
	// structure
	RefArray<AISubgroup> _subgroups;
	OLink<AISubgroup> _mainSubgroup;
	OLink<AIUnit> _leader;
	OLink<AICenter> _center;
	RadioChannel _radio;

	OLink<AIUnit> _units[MAX_UNITS_PER_GROUP];
	LinkTarget _assignTarget[MAX_UNITS_PER_GROUP]; // pending in radio
	Time _assignValidUntil[MAX_UNITS_PER_GROUP];
	// remember state in the moment of assignement
	TargetState _assignTargetState[MAX_UNITS_PER_GROUP];

	// remmember status (as units reported it)
	AIUnit::ResourceState _healthState[MAX_UNITS_PER_GROUP];
	AIUnit::ResourceState _ammoState[MAX_UNITS_PER_GROUP];
	AIUnit::ResourceState _fuelState[MAX_UNITS_PER_GROUP];
	AIUnit::ResourceState _dammageState[MAX_UNITS_PER_GROUP];
	bool _reportedDown[MAX_UNITS_PER_GROUP];
	Time _reportBeforeTime[MAX_UNITS_PER_GROUP];


	// state
	Time _lastUpdateTime;
	Time _checkTime;
	Semaphore _semaphore;
//	CombatMode _combatModeMajor;
	CombatMode _combatModeMinor;

	int _nextCmdId;
	int _locksWP;

	int _enemiesDetected;
	int _unknownsDetected;
	Time _lastEnemyDetected;	
	// _nearestEnemyDist2 is used to optimize target tracking
	// there is no need to track targets often when there is no enemy near
	// this variable is reset always during AddNewTargets
	// therefore it is able to track only enemies that are already in tracking range
	// group value is min over all units
	float _nearestEnemyDist2;
	Time _disclosed;

	// info
	int _id;
	RString _name;
	RString _letterName;
	RString _colorName;
/*
	Ref<Texture> _nameSign;
	Ref<Texture> _colorSign;
*/
	RString _pictureName;
	Ref<Texture> _picture;

	// targets
	Time _expensiveThinkTime;
	int _expensiveThinkFrac; // frac in ms

	Time _checkCenterDBase; // time when center dbase was last checked

	void ExpensiveThinkDone(); // calculate time of next expensive think

	Time _lastSendTargetTime;

	TargetList _targetList;
	OLinkArray<Transport> _vehicles;

	TargetId _overlookTarget;
	Vector3 _guardPosition;

	// waypoint SUPPORT
	OLink<AIGroup> _supportedGroup;
	Vector3 _supportPosition;

	// waypoints
	AutoArray<WaypointInfo> _wp;
	Ref<Script> _script;

	// flee mode
	float _maxStrength;
	float _forceCourage;
	float _courage;
	bool _flee;

	// threshold for randomization
	float _threshold;
	Time _thresholdValid;

public:
	AIGroup();
	~AIGroup();
	void Init();	// call after id is assigned
	void SetIdentity(RString name, RString color, RString picture);

	LSError Serialize(ParamArchive &ar);
	static AIGroup *CreateObject(ParamArchive &ar) {return new AIGroup();}
	static AIGroup *LoadRef(ParamArchive &ar);
	LSError SaveRef(ParamArchive &ar) const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	static AIGroup *CreateObject(NetworkMessageContext &ctx);
	void DestroyObject();
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
	Vector3 GetCurrentPosition() const {return Leader() ? Leader()->Position() : VZero;}
	int GetNextCmdId() {return _nextCmdId++;}

	RadioChannel &GetRadio() {return _radio;}
	const RadioChannel &GetRadio() const {return _radio;}
	// structure
	AISubgroup *MainSubgroup() const {return _mainSubgroup;}
	AICenter* GetCenter() const;
	AICenter* GetCenterS() const; // not-inlined implementation
	int NSubgroups() const {return _subgroups.Size();}
	AISubgroup *GetSubgroup( int i ) const {return _subgroups[i];}
	AIUnit* Leader() const {return _leader;} 
	bool IsCameraGroup() const;

	bool IsPlayerGroup() const; // main (local) player
	bool IsAnyPlayerGroup() const; // any (remote or local) player

	bool IsPlayerDrivenGroup() const
		{return Leader() && Leader()->IsPlayerDriven();}
	const char* GetName() const {return _name;}
/*
	Texture *GetNameSign() const {return _nameSign;}
	Texture *GetColorSign() const {return _colorSign;}
*/
	int ID() const {return _id;}
	AIUnit* UnitWithID(int id) const 
	{
		Assert(id > 0 && id <= MAX_UNITS_PER_GROUP);
		if (id <= 0 || id > MAX_UNITS_PER_GROUP)
			return NULL;
		return _units[id - 1];
	}
	// low level function used for network transfer
	// !!! avoid another using
	void SetUnit(int index, AIUnit *unit);
	int NUnits() const;
	int NSoldiers() const;
	int NFreeVehicles() const;
	int NFreeManCargo() const;
	AIGroupType GetType() const {return GTMilitary;}
	const TargetList &GetTargetList() const {return _targetList;}
	Target *AddTarget
	(
		EntityAI *object, float accuracy, float sideAccuracy, float delay,
		const Vector3 *pos=NULL, AIUnit *sensor=NULL, float sensorDelay=1e10
	);

	int NVehicles() const {return _vehicles.Size();}
	const Transport *GetVehicle(int i) const {return _vehicles[i];}
	Transport *GetVehicle(int i) {return _vehicles[i];}
	
	TargetType *GetOverlookTarget() const {return _overlookTarget;}
	void SetOverlookTarget(TargetType *target) {_overlookTarget = target;}

	Vector3Val GetGuardPosition() const {return _guardPosition;}
	void SetGuardPosition(Vector3Par pos) {_guardPosition = pos;}

	// waypoint SUPPORT
	void Support(AIGroup *grp, Vector3Par pos) {_supportedGroup = grp; _supportPosition = pos;}
	void CancelSupport() {_supportedGroup = NULL;}
	const AIGroup *GetSupportedGroup() const {return _supportedGroup;}
	Vector3Val GetSupportPos() const {return _supportPosition;}

	float UpdateAndGetThreshold();

	Time GetDisclosed() const {return _disclosed;}
	void Disclose(AIUnit *sender);
	Threat GetAttackInfluence(); 
	Threat GetDefendInfluence(); 
	int NWaypoints() const {return _wp.Size();}
	void AddFirstWaypoint(Vector3Par pos);
	int AddWaypoint() {return _wp.Add();}
	void DeleteWaypoint(int index) {if (index < _wp.Size()) _wp.Delete(index);}
	const ArcadeWaypointInfo &GetWaypoint(int i) const {return _wp[i];}
	ArcadeWaypointInfo &GetWaypoint(int i) {return _wp[i];}
	Script *GetScript() const {return _script;}
	void SetScript(Script *script) {_script = script;}
	Semaphore GetSemaphore() const {return _semaphore;}
	void SetSemaphore(Semaphore s) {_semaphore = s;}
	CombatMode GetCombatModeMinor() const {return _combatModeMinor;}
	void SetCombatModeMinor(CombatMode mode) {_combatModeMinor = mode;}
	void SetCombatModeMajor(CombatMode mode);

	bool IsLockedWP() const {return _locksWP > 0;}
	void LockWP(bool lock = true);

	// changes of structure
	void SubgroupRemoved(AISubgroup *subgrp);
	void UnitRemoved(AIUnit *unit);
	void AddSubgroup(AISubgroup *subgrp);

	void AddUnit(AIUnit *unit, int id = -1);
	void AddVehicle(Transport *veh);
	AIUnit *LeaderCandidate(AIUnit *dead) const;
	void SelectLeader(AIUnit *unit);
	void RemoveFromCenter();
	void ForceRemoveFromCenter();

	// mind
	bool Think(); // if true, OperPath was called

	// communication with subgroups
	void SendCommand(Command &cmd, bool channelCenter = false);
	void SendCommand(Command &cmd, PackedBoolArray list, bool channelCenter = false);
	void IssueCommand(Command &cmd, PackedBoolArray list);
	void SendFormation(Formation f, AISubgroup *to);
	void SendSemaphore(Semaphore sem, PackedBoolArray list);
	void SendBehaviour(CombatMode mode, PackedBoolArray list);
	void SendLooseFormation(bool loose, PackedBoolArray list);
	void SendOpenFire(OpenFireState open, PackedBoolArray list);
	void SendState(RadioMessageState *msg, bool silent=false);
	void SendReportStatus(PackedBoolArray list);
	void SendTarget
	(
		Target *target, bool engage, bool fire,
		PackedBoolArray list, bool silent=false
	);

	void SendObjectDestroyed(AIUnit *sender, const VehicleType *type);
	void SendContact(AIUnit *sender);
	void SendUnderFire(AIUnit *sender);
	//! send message unit is down, autoselect sender
	void SendIsDown(AIUnit *down);
	//! send message unit is down - prefer unit from given vehicle as sender
	void SendIsDown(Transport *vehicle, AIUnit *down);
	//! send message unit is down, sender known
	void SendUnitDown(AIUnit *sender, AIUnit *down);
	void SendClear(AIUnit *sender);
	void SendGetOut(PackedBoolArray list);
	void SendAutoCommandToUnit(Command &cmd, AIUnit *unit, bool join = false, bool channelCenter = false);
	void NotifyAutoCommand(Command &cmd, AIUnit *unit);
	void IssueAutoCommand(Command &cmd, AIUnit *unit);
	void ReceiveAnswer(AISubgroup* from, Answer answer);
	void ReceiveUnitStatus(AIUnit *unit, Answer answer);
	bool CommandSent(bool channelCenter = false);
	bool CommandSent(Command::Message message, bool channelCenter = false);
	bool CommandSent(AIUnit *to, Command::Message message, bool channelCenter = false);

	void ClearGetInCommands(AIUnit *to);

	 // check unit state and pending messages
	Target *EngageSent(AIUnit *unit) const;
	Target *FireSent(AIUnit *unit) const;
	Target *TargetSent(AIUnit *unit) const;

	bool ReportSent(ReportSubject subject, const VehicleType *type);
	void ReportFire(AIUnit *who, bool state);

	AIUnit::ResourceState GetHealthStateReported(AIUnit *unit);
	AIUnit::ResourceState GetAmmoStateReported(AIUnit *unit);
	AIUnit::ResourceState GetFuelStateReported(AIUnit *unit);
	AIUnit::ResourceState GetDammageStateReported(AIUnit *unit);
	AIUnit::ResourceState GetWorstStateReported(AIUnit *unit);

	void SetHealthStateReported(AIUnit *unit, AIUnit::ResourceState state);
	void SetAmmoStateReported(AIUnit *unit, AIUnit::ResourceState state);
	void SetFuelStateReported(AIUnit *unit, AIUnit::ResourceState state);
	void SetDammageStateReported(AIUnit *unit, AIUnit::ResourceState state);

	bool GetReportedDown(AIUnit *unit);
	void SetReportedDown(AIUnit *unit, bool state);

	Time GetReportBeforeTime(AIUnit *unit);
	void SetReportBeforeTime(AIUnit *unit, Time time);

	// communication with center
	void ReceiveMission(Mission &mis);
	void SendAnswer(Answer answer);
	void SendReport(ReportSubject subject, Target &target);
	void SendRadioReport(ReportSubject subject, Target &target);

	// access to FSM
	const Mission *GetMission()
		{return (GetCurrent() != NULL) ? GetCurrent()->_task : NULL;}

	void DoUpdate(); 
	void DoRefresh();

	// interface for FSM
	void Move(AISubgroup *who, Vector3Val destination, Command::Discretion discretion);
	void Wait(AISubgroup *who, Time until, Command::Discretion discretion);
	void Attack(AISubgroup *who, TargetType *target, Command::Discretion discretion);
	void Fire(AISubgroup *who, TargetType *target, int weapon, Command::Discretion discretion);
	void GetIn(AISubgroup *who, TargetType *target, Command::Discretion discretion);

	void MoveUnit(AIUnit *who, Vector3Val destination, Command::Discretion discretion);

	// FSM helpers
	bool GetAllDone() const; // query group status
	void AllGetOut(); // get out and disable get in
	void CargoGetOut(); // get out and disable get in for cargo

	// ...	
	int NEnemiesDetected() const {return _enemiesDetected;}
	int NUnknownsDetected() const {return _unknownsDetected;}
	void SetCheckTime(Time time) {_checkTime = time;}
	Time GetCheckTime() const {return _checkTime;}
	
	Target *FindTargetAll(TargetType * id) const; // find any (even uknown) target
	Target *FindTarget(TargetType * id) const; // find known target 
	bool FindTarget(TargetType *id, TargetSide &side, const VehicleType *&type) const;

	bool FindHealPosition(Command &cmd) const;
	bool FindRepairPosition(Command &cmd) const;
	bool FindRefuelPosition(Command &cmd) const;
	bool FindRearmPosition(Command &cmd) const;
//	bool FindInfantryRearmPosition(Command &cmd) const;

	const AITargetInfo *FindHealPosition(AIUnit::ResourceState state, AIUnit *unit = NULL) const;
	const AITargetInfo *FindRepairPosition(AIUnit::ResourceState state) const;
	const AITargetInfo *FindRefuelPosition(AIUnit::ResourceState state) const;
	const AITargetInfo *FindRearmPosition(AIUnit::ResourceState state) const;
//	const AITargetInfo *FindInfantryRearmPosition(AIUnit::ResourceState state) const;

	float GetCost() const;  						// cost of all group
	float EstimateTime(Vector3Val pos) const; // in minutes
	bool GetFlee() const {return _flee;}
	void AllowFleeing(float allow = 1.0f) {_courage = _forceCourage = 1.0f - allow;}

	void CalculateMaximalStrength();

	RString GetDebugName() const;
	bool AssertValid() const;
	void Dump(int indent = 1) const;

	void UnassignVehicle(Transport *veh);
	void UnitAssignCanceled( AIUnit *unit );

	void AssignVehicles();
	void GetInVehicles();

	void RessurectUnit(AIUnit *unit);

	void ReactToEnemyDetected();
protected:
	// implementation
	bool CreateTargetList(bool initialize = false, bool report = true);

	void InitUnitSlot(int i); // i is ID-1
	void CopyUnitSlot(int from, int to); // i is ID-1

	float GetDammagePerMinute(Target *tar) const;
	float GetSubjectiveCost(Target *tar) const;
	void AssignTargets();

	bool CheckFuel();
	bool CheckArmor();
	bool CheckHealth();
	bool CheckAmmo();

	void CheckSupport();

	void CheckAlive();

	float ActualStrength() const;
	void CalculateCourage();
	void Flee();
	void Unflee();

	void SortUnits();	// call only from AICenter::Init (BeginArcade, BeginMultiplayer)
private:
	// disable copy
	AIGroup( const AIGroup &src );
	void operator = ( const AIGroup &src );
};

///////////////////////////////////////////////////////////////////////////////
// class AICenter

class AITargetInfo
{
public:
	TargetId _idExact;
	TargetSide _side;
	const VehicleType *_type;

	Point3 _realPos;
	Point3 _pos;
	Vector3 _dir;

	int _x, _z;

	float _accuracySide;
	Time _timeSide;
	float _accuracyType;
	Time _timeType;
	float _precisionPos;
	Time _time;

	bool _exposure;

	bool _vanished; // disappered - e.g. GetIn
	bool _destroyed; // dead - do not attack

	AITargetInfo();

	LSError Serialize(ParamArchive &ar);

	float FadingSideAccuracy() const;
	float FadingTypeAccuracy() const;
	float FadingPositionAccuracy() const;
};
TypeContainsOLink(AITargetInfo);

class AICheckPointInfo
{
public:
	Point3 _pos;
	int _x;
	int _z;
	Time _time;
	int _missions;

	AICheckPointInfo();
	AICheckPointInfo(Vector3Val pos, Time time);
};
TypeIsBinary(AICheckPointInfo);

struct AIEnemyInfo
{
	Point3 _pos;
	AutoArray<int> _targets; // indexes into AITargetList
};
TypeIsMovable(AIEnemyInfo);

typedef AutoArray<AITargetInfo> AITargetList;
typedef AutoArray<AICheckPointInfo> AICheckPointList;
typedef AutoArray<AIEnemyInfo> AIEnemyList;

typedef bool (*SideFunction)(const AICenter *center, TargetSide side);

union AIThreat
{
	unsigned int packed;
	struct
	{
		BYTE soft;
		BYTE armor;
		BYTE air;
		BYTE reserved;
	} u;
};

TypeIsSimple(AIThreat)

enum AIGuardTargetType
{
	GTTNothing,
	GTTVehicle,
	GTTPoint
};

struct AIGuardTarget
{
	AIGuardTargetType type;
	TargetId vehicle;
	Vector3 position;
};

struct AIGurdedVehicle
{
	TargetId vehicle;
	OLink<AIGroup> by;

	LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AIGurdedVehicle)

struct AIGurdedPoint
{
	Vector3 position;
	OLink<AIGroup> by;

	LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AIGurdedPoint)

struct AIGuardingGroup
{
	OLink<AIGroup> group;
	bool guarding;

	LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AIGuardingGroup)

// support target
struct AISupportTarget
{
	OLink<AIGroup> group;
	Vector3 pos;
	bool heal;
	bool repair;
	bool rearm;
	bool refuel;

	LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AISupportTarget)

// group ready for support (on waypoint SUPPORT)
struct AISupportGroup
{
	OLink<AIGroup> group;
	bool heal;
	bool repair;
	bool rearm;
	bool refuel;
	OLink<AIGroup> assigned;

	LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AISupportGroup)

DEFINE_ENUM_BEG(EndMode)
	EMContinue, EMKilled, EMLoser,
	EMEnd1, EMEnd2, EMEnd3, EMEnd4, EMEnd5, EMEnd6
DEFINE_ENUM_END(EndMode)

struct VehicleInitCmd : public NetworkSimpleObject
{
	EntityAI *vehicle;
	RString init;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};
TypeIsMovable(VehicleInitCmd)

DEFINE_ENUM_BEG(AICenterMode)
    AICMDisabled,AICMArcade,AICMIntro,AICMNetwork
DEFINE_ENUM_END(AICenterMode)

class AICenter : public AI
{
	public:
	typedef AICenterMode Mode;

	private:
	// Structure
	RefArray<AIGroup> _groups;

	RadioChannel _radio;

	Mode _mode;

	Time _lastUpdateTime;		// for groups

	// State
	TargetSide _side;
	float _friends[TSideUnknown];
	float _resources;	// actual money
	
	// Strategic database
	AITargetList _targets;

	// Strategic map
	SRef<AIMap> _map;
	int _row;
	int _column;

	int _nSoldier;
	int _nWoman;

	// guarded targets
	AutoArray<AIGuardingGroup> _guardingGroups;
	AutoArray<AIGurdedVehicle> _guardedVehicles;
	AutoArray<AIGurdedPoint> _guardedPoints;
	Time _guardingValid;

	// supported targets
	AutoArray<AISupportTarget> _supportTargets;
	AutoArray<AISupportGroup> _supportGroups;
	Time _supportValid;

public:
	AICenter(TargetSide side, Mode mode=AICMArcade);

	LSError Serialize(ParamArchive &ar);
	static AICenter *CreateObject(ParamArchive &ar)
		{return new AICenter(TSideUnknown, AICMDisabled);}
	static AICenter *LoadRef(ParamArchive &ar);
	LSError SaveRef(ParamArchive &ar) const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	static AICenter *CreateObject(NetworkMessageContext &ctx);
	void DestroyObject();
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx	);
	static float CalculateErrorCoef(Vector3Par position, Vector3Par cameraPosition) {return 1.0;}
	Vector3 GetCurrentPosition() const {return VZero;}

	RadioChannel &GetRadio() {return _radio;}
	const RadioChannel &GetRadio() const {return _radio;}
	float Resources() const {return _resources;}
	void SetResources(float res)
	{
		Assert(res >= 0);
		_resources = res;
	}
	void SpendResources(float toSpend)
	{
#if 0
		Assert(_resources >= toSpend);
		_resources -= toSpend;
#endif
	}
	int NGroups() const {return _groups.Size();}
	AIGroup *GetGroup(int i) const {return _groups[i];}

	int NTargets() const {return _targets.Size();}
	const AITargetInfo &GetTarget(int i) const {return _targets[i];}
	const AITargetInfo *FindTargetInfo(TargetType *id) const;
	AITargetInfo *FindTargetInfo(TargetType *id);
	int FindTargetIndex(TargetType *id) const;

	Mode GetMode() const {return _mode;}
	void SetMode(Mode mode) {_mode = mode;}

	TargetSide GetSide() const {return _side;}
	int GetLanguage() const;
	float GetFriendship(TargetSide side) const {return _friends[side];}
	void SetFriendship(TargetSide side, float friendship)
	{
		if (side < TSideUnknown) _friends[side] = friendship;
	}

	bool IsFriendly( TargetSide side) const; // more complex test
	bool IsNeutral( TargetSide side) const;
	bool IsEnemy( TargetSide side) const;

	bool IsUnknown( TargetSide side) const; // used when selector funcion is needed
	bool IsCivilian( TargetSide side) const;
	bool IsWest( TargetSide side) const;
	bool IsEast( TargetSide side) const;
	bool IsResistance( TargetSide side) const;

	void UpdateGuarding();
	AIGuardTarget GetGuardTarget(AIGroup *grp);
	int AddGuardedPoint(Vector3Par pos);
	
	// supported targets
	void UpdateSupport();
	void ReadyForSupport(AIGroup *grp);
	void AskSupport(AIGroup *grp, UIActionType type);
	void SupportDone(AIGroup *grp);
	bool IsSupported(AIGroup *grp, UIActionType type);
	// FIX
	bool WaitingForSupport(AIGroup *grp, UIActionType type);
	bool CanSupport(UIActionType type);

	void InitPreview(const ArcadeUnitInfo &info);
	void Init(ArcadeTemplate &t, AutoArray<VehicleInitCmd, MemAllocSA> &inits);

	void InitSensors(bool initialize = false);
	void AddGroup(AIGroup *grp, int id = 0);
	void GroupRemoved(AIGroup *grp);
	void SelectLeader(AIGroup *grp); // implementation of leader selection for group

	// mind
	void Think();
	// communication with group
	void SendMission(AIGroup *to, Mission &mis);
	void ReceiveAnswer(AIGroup *from, Answer answer);
	void ReceiveReport(AIGroup *from, ReportSubject subject, Target &target);

	void InitTarget(EntityAI *veh, float age);
	void DeleteTarget(TargetType *id);
	void UpdateTarget(Target &target);

	float GetExposurePessimistic(int x, int z) const;
	float GetExposureOptimistic(int x, int z) const;
	float GetExposureUnknown(int x, int z) const;

	float GetExposurePessimistic(Vector3Par pos) const;
	float GetExposureOptimistic(Vector3Par pos) const;
	float GetExposureUnknown(Vector3Par pos) const;

	RString GetDebugName() const;
	bool AssertValid() const;
	void Dump(int indent = 0) const;
	
	const ParamEntry &NextSoldierIdentity(bool woman);

// Implementation
private:
	float RecalculateExposure(int x, int z, AIThreat &result, SideFunction func);
	void UpdateField(int x, int z);
	void UpdateMap();
	void UpdateGroup();
	
	void BeginArcade(ArcadeTemplate &t, AutoArray<VehicleInitCmd, MemAllocSA> &inits);
	void BeginPreviewUnit(const ArcadeUnitInfo &info);

	AIUnit *CreateSoldier(Transport *transport, int rank, const ParamEntry &cfgSide, bool multiplayer);
	AIUnit *CreateUnit
	(
		const ArcadeUnitInfo &info, ArcadeTemplate &t, bool multiplayer, AIGroup *grp,
		bool disableC = false, bool disableD = false, bool disableG = false
	);

	friend EntityAI *CreateVehicle(const ArcadeUnitInfo &info, ArcadeTemplate &t, TargetSide side, bool multiplayer);
	friend Vehicle *CreateSensor(const ArcadeSensorInfo &info, AIGroup *grp, bool multiplayer);
	friend void CreateMarker(const ArcadeMarkerInfo &info, bool multiplayer);

	void RemoveOldExposures();
	void AddNewExposures();
};

AICenter *CreateCenter(ArcadeTemplate &t, TargetSide side, AICenter::Mode mode);
void InitNoCenters(ArcadeTemplate &t, AutoArray<VehicleInitCmd, MemAllocSA> &inits, bool multiplayer);

inline AISubgroup* AIUnit::GetSubgroup() const {return _subgroup;}
inline AIGroup* AISubgroup::GetGroup() const {return _group;}
inline AICenter* AIGroup::GetCenter() const {return _center;}
inline bool AIUnit::IsSubgroupLeader() const {return GetSubgroup() && GetSubgroup()->Leader() == this;}
inline bool AIUnit::IsSubgroupLeaderVehicle() const {return GetSubgroup() && GetSubgroup()->Leader()->GetVehicle() == GetVehicle();}
inline bool AIUnit::IsGroupLeader() const {return GetGroup() && GetGroup()->Leader() == this;}

/******************************************/
/*                                        */
/*              Statistics                */
/*                                        */
/******************************************/

enum AIStatsEventType
{
	SETUnitLost,
	SETKillsEnemyInfantry,
	SETKillsEnemySoft,
	SETKillsEnemyArmor,
	SETKillsEnemyAir,
	SETKillsFriendlyInfantry,
	SETKillsFriendlySoft,
	SETKillsFriendlyArmor,
	SETKillsFriendlyAir,
	SETKillsCivilInfantry,
	SETKillsCivilSoft,
	SETKillsCivilArmor,
	SETKillsCivilAir,
	SETUser,
};

enum AIStatsKills
{
	SKEnemyInfantry,
	SKEnemySoft,
	SKEnemyArmor,
	SKEnemyAir,
	SKFriendlyInfantry,
	SKFriendlySoft,
	SKFriendlyArmor,
	SKFriendlyAir,
	SKCivilianInfantry,
	SKCivilianSoft,
	SKCivilianArmor,
	SKCivilianAir,
	SKN
};

struct AIStatsMPRow
{
	int order;
	RString player;
	TargetSide side;
	int killsInfantry;
	int killsSoft;
	int killsArmor;
	int killsAir;
	int killsPlayers;
	int customScore;
	int killsTotal;
	int killed;

	AIStatsMPRow();
	void RecalculateTotal();
};
TypeIsMovableZeroed(AIStatsMPRow)

struct AIStatsEvent
{
	AIStatsEventType type;
	float time;	// from Clock
	Vector3 position;
	RString message;

	const VehicleType *killedType;
	const VehicleType *killerType;
	RString killedName;
	RString killerName;
	bool killedPlayer;
	bool killerPlayer;

	LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(AIStatsEvent)

#if _ENABLE_VBS

enum VBSStatsEventType
{
	VBSETKill,
	VBSETInjury,
	VBSETUser,
};

struct VBSUnitInfo
{
	RString name;
	RString type;
	Vector3 position;
	TargetSide side;
	bool player;
};

struct VBSStatsEvent
{
	Time t;
	VBSStatsEventType type;
	VBSUnitInfo unit1;
	VBSUnitInfo unit2;
	float value;
	RString text;
};
TypeIsMovable(VBSStatsEvent)
#endif

class AIStatsMission : public SerializeClass
{
protected:
	float _size;
	PackedColor _color;
	PackedColor _colorBg;
	PackedColor _colorSelected;
	PackedColor _colorWest;
	PackedColor _colorEast;
	PackedColor _colorCiv;
	PackedColor _colorRes;

public:
	AutoArray<AIStatsEvent> _events;
	AutoArray<AIStatsMPRow> _tableMP;
	int _lives;
	Time _start;
	time_t _time;

#if _ENABLE_VBS
	AutoArray<RString> _vbsHeader;
	AutoArray<VBSStatsEvent> _vbsEvents;
	AutoArray<RString> _vbsFooter;
#endif

public:
	AIStatsMission();
	void Init();
	
	void Clear();
	void AddEvent
	(
		AIStatsEventType type, Vector3Par position, RString message,
		const VehicleType *killedType, RString killedName, bool killedPlayer,
		const VehicleType *killerType, RString killerName, bool killerPlayer
	);
	void AddMPKill(RString playerName, TargetSide playerSide, const VehicleType *killedType, bool killedPlayer, bool isEnemy);
	void AddMPScore(RString playerName, TargetSide playerSide, int score);
#if _ENABLE_VBS
	void VBSKilled(EntityAI *killed, EntityAI *killer);
	void VBSInjured(EntityAI *injured, EntityAI *killer, float damage, RString ammo);
	void VBSAddHeader(RString text);
	void VBSAddEvent(RString text);
	void VBSAddFooter(RString text);
	void WriteVBSStatistics();
#endif
	void OnMissionStart();

	void DrawMPTable(float alpha);

	LSError Serialize(ParamArchive &ar);
};

struct AIUnitHeader : public SerializeClass
{
	int index;
	float experience;
	Rank rank;

	OLink<AIUnit> unit;
	bool used;

	LSError Serialize(ParamArchive &ar);
};
TypeIsGeneric(AIUnitHeader)

/*
class AIUnitPool : public SerializeClass
{
public:
	AutoArray<AIUnitHeader> _units;
	TargetSide _side;
	int _nextSoldier;

public:
	AIUnitPool(TargetSide side) {_side = side; Clear();}

	void Clear();
	void Update();
	LSError Serialize(ParamArchive &ar);
};
*/

class AIStatsCampaign : public SerializeClass
{
public:
	AIUnitHeader _playerInfo;
//	AIUnitPool _westUnits;
//	AIUnitPool _eastUnits;
	AutoArray<RString> _awards;
	AutoArray<RString> _penalties;

	float _casualties;
	float _inCombat;
	int _kills[SKN];

	AutoArray<GameVariable> _variables;

	float _score, _lastScore;

	time_t _date;

public:
	AIStatsCampaign();
	void Clear();
	void Update(AIStatsMission &mission);
	void AddVariable(GameVariable &var);

	LSError CampaignSerialize(ParamArchive &ar);
	LSError Serialize(ParamArchive &ar);
};

class AIStats : public SerializeClass
{
public:
	AIStatsMission _mission;
	AIStatsCampaign _campaign;

public:
	AIStats() {}
	void Init() {_mission.Init();}
	
	void ClearAll() {_campaign.Clear(); _mission.Clear();}
	void ClearMission() {_mission.Clear();}

	void OnMissionStart();
	void OnMPMissionEnd();

	void Update() {_campaign.Update(_mission);}

	void OnVehicleDestroyed(EntityAI *killed, EntityAI *killer);
#if _ENABLE_VBS
	void OnVehicleDamaged(EntityAI *injured, EntityAI *killer, float damage, RString ammo);
	void VBSAddHeader(RString text);
	void VBSAddEvent(RString text);
	void VBSAddFooter(RString text);
#endif
	void AddUserEvent(RString event, Vector3Par pos);

	void DrawMPTable(float alpha) {_mission.DrawMPTable(alpha);}

	LSError Serialize(ParamArchive &ar);
};

extern AIStats GStats;
inline AIUnitHeader &PlayerInfo() {return GStats._campaign._playerInfo;}


extern ArcadeTemplate CurrentTemplate;

extern RString CurrentCampaign;
extern RString CurrentBattle;
extern RString CurrentMission;
extern RString CurrentWorld;
extern RString CurrentFile;

inline bool IsCampaign() {return CurrentBattle && CurrentBattle.GetLength() > 0;}

LSError AIGlobalSerialize(ParamArchive &ar);

#endif
