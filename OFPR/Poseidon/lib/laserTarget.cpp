// shots and explosions simulation

#include "wpch.hpp"
#include "laserTarget.hpp"
#include "ai.hpp"
#include "network.hpp"
#include "scene.hpp"
#include "landscape.hpp"
#include "camera.hpp"
#include "global.hpp"
#include "diagModes.hpp"

LaserTargetType::LaserTargetType( const ParamEntry *param )
:base(param)
{
	_scopeLevel=1;
}

void LaserTargetType::Load(const ParamEntry &par)
{
	base::Load(par);
}
void LaserTargetType::InitShape()
{
	_scopeLevel=2;
	base::InitShape();
	_shape->OrSpecial(IsColored|ZBiasMask|IsAlpha|IsAlphaFog);
	if (_shape->NLevels()>=2)
	{
		Shape *points = _shape->Level(1);
		for (int i=0; i<points->NVertex(); i++)
		{
			ClipFlags clip = points->Clip(i);
			clip &= ~ClipLightMask;
			clip |= ClipLightLine;
			points->SetClip(i,clip);
		}
		points->CalculateHints();
		_shape->CalculateHints();
	}
}

DEFINE_CASTING(LaserTarget)

LaserTarget::LaserTarget(EntityAIType *name, bool fullCreate)
:base(name,fullCreate)
{
	static PackedColor laserCol(Color(1,0,0,1));
	SetConstantColor(laserCol);
	_lastActivation = Glob.time;
}

void LaserTarget::CheckDesignatedTarget(Matrix4Par pos)
{
#if _ENABLE_CHEATS
	Object *odt = _designatedTarget;
#endif
	AUTO_STATIC_ARRAY(OLink<Object>,objects,16);
	_designatedTarget = NULL;
	// check if we are inside or very close of some object
	// if yes, it is designated target
	GLandscape->IsInside(objects,this,pos.Position(),ObjIntersectFire);
	if (objects.Size()>0) 
	{
		// if more targets, we may select any
		for (int i=0; i<objects.Size(); i++)
		{
			if (objects[0]->GetShape())
			{
				_designatedTarget = objects[0];
				Assert( !dynamic_cast<LaserTarget *>(_designatedTarget.GetLink()) );
				break;
			}
		}
	}
	Object *obj = GLandscape->NearestObject
	(
		pos.Position(),50,(ObjectType)(Primary|TypeVehicle),this
	);
	if (obj)
	{
		LODShape *shape = obj->GetShape();
		if (shape)
		{
			Vector3Val geomCenter = obj->PositionModelToWorld(shape->GeometryCenter());
			float objDist2 = geomCenter.Distance2(pos.Position());
			if (objDist2>=Square(shape->GeometrySphere()))
			{
				// if no normal object is near, try road objects
				obj = NULL;
			}
			_designatedTarget = obj;
			Assert( !dynamic_cast<LaserTarget *>(_designatedTarget.GetLink()) );
		}
	}
	if (!obj)
	{
		// if no normal object is designated, try to designate road objects
		obj = GLandscape->NearestObject(pos.Position(),50,Network,this);
		if (obj)
		{
			LODShape *shape = obj->GetShape();
			if (shape)
			{
				Vector3Val geomCenter = obj->PositionModelToWorld(shape->GeometryCenter());
				float objDist2 = geomCenter.Distance2(pos.Position());
				if (objDist2>=Square(shape->GeometrySphere()))
				{
					obj = NULL;
				}
				_designatedTarget = obj;
				Assert( !dynamic_cast<LaserTarget *>(_designatedTarget.GetLink()) );
			}
		}
	}
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DECombat))
	{
		if (odt!=_designatedTarget)
		{
			LogF
			(
				"Designated target %s",
				_designatedTarget ? (const char *)_designatedTarget->GetDebugName() : "<NULL>"
			);
		}
	}
#endif
}
void LaserTarget::Init(Matrix4Par pos)
{
	CheckDesignatedTarget(pos);
}

void LaserTarget::Move(Matrix4Par transform)
{
	base::Move(transform);
	_lastActivation = Glob.time;
	CheckDesignatedTarget(transform);
}
void LaserTarget::Move(Vector3Par position)
{
	base::Move(position);
	_lastActivation = Glob.time;
	CheckDesignatedTarget(*this);
}

bool LaserTarget::IgnoreObstacle(Object *obstacle, ObjIntersect type) const
{
	if (type==ObjIntersectFire)
	{
		// if designated target blocks line-of-fire, we do not mind and fire
		if (obstacle==_designatedTarget) return true;
		// note: we do mind when target block line-of-sight
	}
	return base::IgnoreObstacle(obstacle,type);

}

void LaserTarget::Simulate( float deltaT, SimulationImportance prec )
{
	if (IsLocal())
	{
		if (_lastActivation<Glob.time-10)
		{
			// if target was not activated for a long time,
			// delete it - it cannot be considered the same target any more
			SetDelete();
		}
	}
	base::Simulate(deltaT,prec);
}

void LaserTarget::DrawDiags()
{
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DECombat))
	{
		GScene->DrawCollisionStar(Position(),0.1,PackedWhite);
		GScene->DrawCollisionStar(AimingPosition(),0.075,PackedBlack);
	}
#endif
}

void LaserTarget::Draw( int level, ClipFlags clipFlags, const FrameBase &pos )
{
	if (!Glob.config.IsEnabled(DTTracers))
	{
		return;
	}
	// check if target was recently activated
	// it may be "ghost" that was left
	if (IsLocal())
	{
		if (_lastActivation<Glob.time-0.2f) return;
	}
	else
	{
		if (_lastActivation<Glob.time-2.0f) return;
	}
	float size = 0.03;
	
	const Camera &camera=*GScene->GetCamera();

	// following line is faster version
	Vector3 posp=GScene->ScaledInvTransform()*pos.Position();

	// camera plane clip test
	// perform more distant clipping than normal
	float nearest=camera.Near();
	if( posp.Z()<nearest ) return;

	// scale and 1/scale is kept in Frame

	// apply perspective on position and size
	Matrix4Val project=camera.Projection();

	float invW=1/posp[2];
	// perspective screen size
	float sizeX=+project(0,0)*size*invW*camera.InvLeft();

	if (sizeX<1)
	{
		Object::DrawPoints(1,clipFlags,pos);
	}
	else
	{
		SetScale(size);
		base::Draw(0,clipFlags,pos);
		SetScale(1);
	}
}

float LaserTarget::VisibleSize() const
{
	// make sure we can detect it on very long distance
	return 40;
}

Vector3 LaserTarget::AimingPosition() const
{
	// it is often direclty on surface - make sure we can see it
	/*
	if (_designatedTarget)
	{
		Vector3 base = _designatedTarget->COMPosition();
		Vector3 offset = Position()-base;
		Vector3 offsetNorm = offset.Normalized();
		float size = offsetNorm*offset;
		// make point "further out"
		return base + offsetNorm*(size+1.0f);
	}
	*/
	// drag aiming point nearer to the source
	return Position()-Direction()*0.5;
}


float LaserTarget::VisibleSizeRequired() const
{
	return 0.01;
}


LSError LaserTarget::Serialize(ParamArchive &ar)
{
	return base::Serialize(ar);
}

NetworkMessageType LaserTarget::GetNMType(NetworkMessageClass cls) const
{
	return base::GetNMType(cls);
}
NetworkMessageFormat &LaserTarget::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	base::CreateFormat(cls,format);
	return format;
}
TMError LaserTarget::TransferMsg(NetworkMessageContext &ctx)
{
	TMCHECK( base::TransferMsg(ctx) );
	return TMOK;
}
float LaserTarget::CalculateError(NetworkMessageContext &ctx)
{
	float error = base::CalculateError(ctx);
	return error;
}

