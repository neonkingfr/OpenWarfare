#ifdef _MSC_VER
#pragma once
#endif

#ifndef _CAMERA_HOLD_HPP
#define _CAMERA_HOLD_HPP

#include "vehicle.hpp"

struct CameraTarget
{
	OLink<Object> _target;
	Vector3 _pos;

	Vector3 GetPos() const;

	CameraTarget():_target(NULL),_pos(VZero){}
	CameraTarget( Object *obj ):_target(obj),_pos(VZero){}
	CameraTarget( Vector3Par pos ):_target(NULL),_pos(pos){}

	Vector3 PositionAbsToRel( Vector3Val pos ) const;
	Vector3 PositionRelToAbs( Vector3Val pos ) const;

	LSError Serialize(ParamArchive &ar);
};

class CameraHolder: public Vehicle
{
	typedef Vehicle base;

	protected:
	bool _manual;

	Vector3 _camPos;
	float _camFov,_camFovMin,_camFovMax;
	float _camDive,_camBank,_camHeading;
	// note: target need not be commited
	CameraTarget _camTgt;
	struct Set
	{ // what has been set since last commit
		bool _camPos;
		bool _camFov,_camFovMinMax;
		bool _camDive,_camBank,_camHeading;
		bool _camTgt;
		Set()
		{
			_camPos=false;
			_camFov=false,_camFovMinMax=false;
			_camDive=false,_camBank=false,_camHeading=false;
			_camTgt=false;
		}
	} _set;

	public:
	CameraHolder
	(
		LODShapeWithShadow *shape, const VehicleNonAIType *type, int id
	);
	~CameraHolder();

	virtual void SetPos( Vector3Par pos ) {_camPos=pos,_set._camPos=true;} // set destination
	virtual void SetFOV( float fov )
	{
		_camFov=fov,_set._camFov=true;
		SetFOVRange(fov,fov);
	} // set camera pars
	virtual void SetFOVRange( float minFov, float maxFov ) {_camFovMin=minFov,_camFovMax=maxFov,_set._camFovMinMax=true;}
	virtual void SetDive( float dive ) {_camDive=dive,_set._camDive=true;}
	virtual void SetBank( float bank ) {_camBank=bank,_set._camBank=true;}
	virtual void SetHeading( float head ) {_camHeading=head,_set._camHeading=true;}

	const CameraTarget &GetTarget() const {return _camTgt;}

	virtual void SetTarget( Object *target ) {_camTgt=target,_set._camTgt=true;}
	virtual void SetTarget( Vector3Par target ) {_camTgt=target,_set._camTgt=true;}
	virtual void Command( RString mode ); // camera dependend special command
	virtual void Commit( float time ) = 0; // commit all deferred settings

	virtual void SetManual( bool manual ){_manual=manual;}
	virtual bool GetManual() const {return _manual;}
	virtual void AimDriver(Vector3Val val) {}

	
	virtual bool GetCommited() const {return true;}

	LSError Serialize(ParamArchive &ar);

	USE_CASTING(base)
};


class CameraVehicle: public CameraHolder
{
	typedef CameraHolder base;
	Vector3 _movePos; // where should I move
	CameraTarget _oldTarget;
	CameraTarget _target;
	float _minFovT,_maxFovT; // target values

	float _minFov,_maxFov; // negative means invalid

	Time _oldTargetTime;
	Time _targetTime;
	Time _movePosTime; // when target should be reached
	Time _minMaxFovTime;

	Time _timeCommitted;

	mutable float _lastFov;
	mutable Vector3 _lastTgtPos;

	bool _inertia; // manual camera simulation 
	bool _crossHairs;

	public:
	Vector3 GetTarget() const;

	CameraVehicle();
	~CameraVehicle();

	void ResetTargets();

	virtual void Simulate( float deltaT, SimulationImportance prec );

	virtual bool OcclusionFire() const {return false;}
	virtual bool OcclusionView() const {return false;}

	virtual bool Invisible() const {return true;}

	// camera effect parameters
	virtual void DrawCameraCockpit();
	virtual bool CameraAutoTerminate() {return _manual;}

	virtual float OutsideCameraDistance( CameraType camType ) const {return 0;}
	virtual float CamEffectFOV() const;

	void StartFrame(); // start frame - used for motion blur

	/*
	virtual void SetPos( Vector3Par pos ) {_camPos=pos;} // set destination
	virtual void SetFOV( float fov ) {_camFov=fov;} // set camera pars
	virtual void SetFOVRange( float minFov, float maxFov ) {_camFovMin=minFov,_camFovMax=maxFov;}
	virtual void SetDive( float dive ) {_camDive=dive;}
	virtual void SetBank( float bank ) {_camBank=bank;}
	virtual void SetHeading( float head ) {_camHeading=head;}
	virtual void SetTarget( Object *target ) {_camTgt=target,_camTgtPos=VZero;}
	virtual void SetTarget( Vector3Par pos ) {_camTgtPos=pos,_camTgt=NULL;}
	*/
	virtual void Command( RString mode ); // camera dependend special command
	virtual void Commit( float time ); // commit all settings

	virtual bool GetCommited() const;
};

#endif
