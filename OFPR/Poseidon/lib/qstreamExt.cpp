// QStream extensions

#include "wpch.hpp"
#include <El/QStream/QStream.hpp>

#if defined _WIN32 && !defined ACCESS_ONLY && !defined _XBOX

#include <Es/Common/win.h>

//! class of callback functions
static class WindowsClipboardFunctions : public ClipboardFunctions
{
public:
	//! callback function to copy string into clipboard
	virtual void Copy(const char *buffer, int size);

	WindowsClipboardFunctions() {QOFStream::SetDefaultClipboardFunctions(this);}
} GWindowsClipboardFunctions;

/*!
\patch 1.44 Date 2/11/2002 by Ondra
- Fixed: Windows clipboard copy sometimes did not work correctly
when non-text data were already stored in cliboard.
*/

void WindowsClipboardFunctions::Copy(const char *buffer, int size)
{
	extern HWND hwndApp;
	if (OpenClipboard(hwndApp))
	{
		EmptyClipboard();
		HANDLE handle = GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, size + 1);
		if (handle)
		{
			char *mem = (char *)GlobalLock(handle);
			if (mem)
			{
				memcpy(mem, buffer, size);
				mem[size] = 0;
			}
			GlobalUnlock(handle);
			SetClipboardData(CF_TEXT, handle);
		}
		CloseClipboard();
	}
}

#endif
