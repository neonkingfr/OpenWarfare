#include "wpch.hpp"
#include "engine.hpp"
#include "font.hpp"
#include "textbank.hpp"
#include "global.hpp"
#include "scene.hpp"
#include "paramFileExt.hpp"
#include <El/Common/perfLog.hpp>
#include <Es/Strings/bstring.hpp>
#include "appFrameExt.hpp"

#include <stdarg.h>

#include "mbcs.hpp"

#ifdef _XBOX
	#include "Common/win.h"
#endif
/*!
\patch_internal 1.82 Date 8/22/2002 by Ondra
- Improved: Performance counters that are zero disappear automatically on screen.
*/

void Engine::DrawFinishTexts()
{
	// add some expressions we consider intersting
	
	#ifndef ACCESS_ONLY
	if( ShowFps()>=1 )
	{
		Ref<Font> font=LoadFont(GetFontID("tahomaB24"));
		Assert( font );
//		float size = font->Height();
	#ifdef _XBOX
		float size = 0.03;
	#else
		float size = 0.02;
	#endif

		float atps = 0;
		for( int i=0; i<NFrameDurations; i++ )
		{
			int dur = _frameDurations[i];
			float tps = dur>0 ? _frameTriangles[i]*1000/dur : _frameTriangles[i]*1000;
			atps+=tps;
		}
		atps *= 1.0/NFrameDurations;

		float fps=0;
		if (_lastFrameDuration>0) fps=1000.0/_lastFrameDuration;
		else fps = 1000;

		float afps=1000.0/GetAvgFrameDuration();
		float x1=0.05;
		// draw text information
		//ShowTextF(-1000,170,120,"FPS %5.2f",fps);
		PackedColor color(Color(1,0.9,0.8,0.5));
		float y=0.05;
		float yso=0.001,xso=0.001;
		DrawTextF(Point2DFloat(x1+xso,y+yso),size,font,PackedBlack,"iFPS %7.2f",fps);
		DrawTextF(Point2DFloat(x1,y),size,font,color,"iFPS %7.2f",fps);
		y+=size;
		DrawTextF(Point2DFloat(x1+xso,y+yso),size,font,PackedBlack,"aFPS %7.2f",afps);
		DrawTextF(Point2DFloat(x1,y),size,font,color,"aFPS %7.2f",afps);
		y+=size;
		// calculate tps
		COUNTER(tris);
		int tris = COUNTER_VALUE(tris);
		DrawTextF(Point2DFloat(x1+xso,y+yso),size,font,PackedBlack,"iTPS %7.0f",tris*fps);
		DrawTextF(Point2DFloat(x1,y),size,font,color,"iTPS %7.0f",tris*fps);
		y+=size;
		DrawTextF(Point2DFloat(x1+xso,y+yso),size,font,PackedBlack,"aTPS %7.0f",atps);
		DrawTextF(Point2DFloat(x1,y),size,font,color,"aTPS %7.0f",atps);
		y+=size;

		//DrawTextF(x1,y,size,font,color,"cLOD %7.2f",GScene->GetScaleDownCoef());
		//y+=0.02;

		float aLod  = -10*log10(GScene->GetLodInvWidth());
		DrawTextF(Point2DFloat(x1+xso,y+yso),size,font,PackedBlack,"aLOD %7.0f",aLod);
		DrawTextF(Point2DFloat(x1,y),size,font,color,"aLOD %7.0f",aLod);
		y+=size;

		#if defined _XBOX && !_SUPER_RELEASE
			MEMORYSTATUS stat;
			stat.dwLength = sizeof(stat);
			GlobalMemoryStatus(&stat);
			int free = stat.dwAvailPhys/1024*10/1024, total = stat.dwTotalPhys/1024*10/1024;

			DrawTextF(x1+xso,y+yso,size,font,PackedBlack,"Mem %.1f of %.1f",free*0.1f,total*0.1f);
			DrawTextF(x1,y,size,font,color,"Mem %.1f of %.1f",free*0.1f,total*0.1f);
			y+=size;

		#endif 

		
		#if _ENABLE_PERFLOG
		float x2=0.2;

		if( ShowFps()>=2 )
		{
			extern bool NoTextures;
			if (NoTextures) color=PackedColor(Color(1,0,0,0.8));
			if( ShowFps()==2 )
			{
				for( int c=0; c<GPerfProfilers.N(); c++ )
				{
					if( GPerfProfilers.Show(c) && GPerfProfilers.WasNonZero(c))
					{
						const char *name=GPerfProfilers.Name(c);
						int value=GPerfProfilers.Value(c);
						DrawTextF(Point2DFloat(x1,y),size,font,color,"%s",name);
						DrawTextF(Point2DFloat(x2,y),size,font,color,"%6d",value);
						y+=size;
					}
				}

			}
			else if( ShowFps()==3 )
			{
				for( int c=0; c<GPerfCounters.N(); c++ )
				{
					if( GPerfCounters.Show(c) && GPerfCounters.WasNonZero(c))
					{
						const char *name=GPerfCounters.Name(c);
						int value=GPerfCounters.Value(c);
						DrawTextF(Point2DFloat(x1,y),size,font,color,"%s",name);
						DrawTextF(Point2DFloat(x2,y),size,font,color,"%6d",value);
						y+=size;
					}
				}
			}
		}
		#endif
	}

	// draw all ShowText buffers
	int i;
	PackedColor black(Color(0, 0, 0, 0.8));
	MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
	int w = Width2D(), h = Height2D();
	for( i=0; i<_texts.Size(); )
	{
		TextInfo &info=_texts[i];
		// DrawText(info._x+0.001,info._y+0.001,info._size,info._font,PackedBlack,info._text);
		float width = GetTextWidth(info._size, info._font, info._text);
		GEngine->Draw2D(mip, black, Rect2DPixel(info._x * w, info._y * h, width * w, info._font->Height() * h));
		DrawText(Point2DFloat(info._x,info._y),info._size,info._font,PackedWhite,info._text);
		if( (int)(_frameTime-info._hideTime)>=0 ) _texts.Delete(i);
		else i++;
	}
	#endif

	//_frameTime=GlobalTickCount();
	#if _ENABLE_PERFLOG
	float fps=0;
	if (_lastFrameDuration>0) fps=1000.0/_lastFrameDuration;
	else fps = 1000;

	GPerfCounters.Save(fps);
	GPerfCounters.Reset();
	GPerfProfilers.Save(fps);
	GPerfProfilers.Reset();
	#endif
}


void CCALL Engine::ShowMessage( int timeMs, const char *msg, ... )
{
	BString<512> message;
	va_list va;
	va_start(va,msg);
	vsprintf(message,msg,va);
	va_end(va);
	if( _messageHandle>=0 )
	{
		//Log("Removing message %d",_messageHandle);
		RemoveText(_messageHandle);
	}
	_messageHandle=ShowText(timeMs,25,2,message);
	//Log("New message %d",_messageHandle);
}

void OFPFrameFunctions::ShowMessage(int timeMs, const char *msg, va_list argptr)
{
	if (!GEngine) return;

	BString<512> message;
	vsprintf(message, msg, argptr);
	GEngine->ShowMessage(timeMs, message);
}

TextInfo::TextInfo
(
	int handle,
	Engine *engine, DWORD hideTime,
	Font *font, PackedColor color,
	float size, float x, float y, const char *text
)
:_hideTime(hideTime),_font(font),_color(color),
_x(x),_y(y),
_size(size),
_text(text,strlen(text)+1),
_handle(handle)
{
}

TextInfo::TextInfo( const TextInfo &src )
:_hideTime(src._hideTime),_font(src._font),_color(src._color),
_x(src._x),_y(src._y),
_size(src._size),
_text(src._text,strlen(src._text)+1),
_handle(src._handle)
{
}
TextInfo &TextInfo::operator =( const TextInfo &src )
{
	_hideTime=src._hideTime,_font=src._font,_color=src._color;
	_x=src._x,_y=src._y;
	_size=src._size;
	if( src._text ) _text.Realloc(src._text,strlen(src._text)+1);
	else _text.Free();
	_handle=src._handle;
	return *this;
}

void Engine::ShowFont( Font *font, PackedColor color, float size )
{
	_showTextFont=font;
	_showTextColor=color;
	_showTextSize=size;
}

void Engine::RemoveText( int handle )
{
	if( handle>=0 )
	{
		for( int i=0; i<_texts.Size(); i++ )
		{
			if( _texts[i]._handle==handle )
			{
				_texts.Delete(i);
				return;
			}
		}
		// note: text may be expired
		//Fail("Invalid text handle.");
	}
}

int Engine::ShowText( DWORD timeToLive, int x, int y, const char *text )
{
	#ifndef ACCESS_ONLY
	//if( !_showTextFont ) _showTextFont=LoadFont("fonts\\arial48");
	if( !_showTextFont )
	{
		_showTextFont=LoadFont(GetFontID("tahomaB36"));
		_showTextSize = 0.021;
	}
	// x,y given for 800x600 screen
	int handle=_textHandle++;
	_texts.Add
	(
		TextInfo
		(
			handle,
			this,GlobalTickCount()+timeToLive,
			_showTextFont,_showTextColor,
			_showTextSize,
			x*(1.0/800),y*(1.0/600),
			text
		)
	);
	return handle;
	#else
	return 0;
	#endif
}

int CCALL Engine::ShowTextF( DWORD timeToLive, int x, int y, const char *format, ... )
{
	BString<512> buf;
	va_list arglist;
	va_start( arglist, format );
	vsprintf( buf, format, arglist );
	va_end( arglist );
	return ShowText(timeToLive,x,y,buf);
}

