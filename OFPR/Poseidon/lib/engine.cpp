#include "wpch.hpp"
#include "engine.hpp"
#include "font.hpp"
#include "textbank.hpp"
#include "global.hpp"
#include "scene.hpp"
#include <El/ParamFile/paramFile.hpp>
#include <El/Common/perfLog.hpp>

#include <stdarg.h>
Engine *GEngine;

Engine::Engine()
:_showTextFont(NULL),
_showTextColor(Color(HBlack)),_showTextSize(0),
#if 0 //defined _XBOX && !defined _SUPER_RELEASE
_showFps(1),
#else
_showFps(0),
#endif
_messageHandle(-1),
_multitexturing(true),
_nightVision(false),_accomodateEye(HWhite),_shadowFactor(0),

_usrBrightness(1),

_frameTime(0),_frameTime0(0),

_startGame(GlobalTickCount()),
_textHandle(0)
{
	ResetFrameDuration();
}

void Engine::ResetFrameDuration()
{
	for( int i=0; i<NFrameDurations; i++ ) _frameDurations[i]=70;
	_lastFrameDuration=70;
}

DWORD Engine::GetAvgFrameDuration( int nFrames ) const
{
	DWORD sum=0;
	saturateMax(nFrames,NFrameDurations);
	for( int i=NFrameDurations-nFrames; i<NFrameDurations; i++ ) sum+=_frameDurations[i];
	return sum/nFrames;
}

void Engine::SetMultitexturing(bool set)
{
	_multitexturing = set;
}

void Engine::FontDestroyed( Font *font )
{
	#ifndef ACCESS_ONLY
	_fonts.RemoveFont(font);
	#endif
}

Engine::~Engine()
{
	#ifndef ACCESS_ONLY
	_fonts.Clear();
	#endif
}

RString GetUserParams();

void Draw2DPars::Init()
{
	spec=NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
	SetU(0,1); // u,v range
	SetV(0,1);
}

int Engine::Width2D() const
{
	return toInt((_aspectSettings.uiBottomRightX-_aspectSettings.uiTopLeftX)*Width());
}

int Engine::Height2D() const
{
	return toInt((_aspectSettings.uiBottomRightY-_aspectSettings.uiTopLeftY)*Height());
}
int Engine::Left2D() const
{
	return toInt(_aspectSettings.uiTopLeftX*Width());
}
int Engine::Top2D() const
{
	return toInt(_aspectSettings.uiTopLeftY*Height());
}

Rect2DPixel Rect2DClipPixel(-1e6,-1e6,2e6,2e6);
Rect2DAbs Rect2DClipAbs(0,0,1e6,1e6);

void Engine::Convert(Point2DAbs &to, const Point2DPixel &from)
{
	to.x = from.x + Left2D();
	to.y = from.y + Top2D();
}
void Engine::Convert(Point2DAbs &to, const Point2DFloat &from)
{
	to.x = from.x*Width2D() + Left2D();
	to.y = from.y*Height2D() + Top2D();
}

void Engine::Convert(Point2DPixel &to, const Point2DAbs &from)
{
	to.x = from.x - Left2D();
	to.y = from.y - Top2D();
}
void Engine::Convert(Point2DFloat &to, const Point2DAbs &from)
{
	to.x = (from.x - Left2D())/Width2D();
	to.y = (from.y - Top2D())/Height2D();
}

void Engine::Convert(Rect2DAbs &to, const Rect2DPixel &from)
{
	to.x = from.x + Left2D();
	to.y = from.y + Top2D();
	to.w = from.w;
	to.h = from.h;
}
void Engine::Convert(Rect2DAbs &to, const Rect2DFloat &from)
{
	float w2d = Width2D();
	float h2d = Height2D();
	to.x = from.x*w2d + Left2D();
	to.y = from.y*h2d + Top2D();
	to.w = from.w*w2d;
	to.h = from.h*h2d;
}
void Engine::Convert(Rect2DPixel &to, const Rect2DAbs &from)
{
	to.x = from.x - Left2D();
	to.y = from.y - Top2D();
	to.w = from.w;
	to.h = from.h;
}
void Engine::Convert(Rect2DFloat &to, const Rect2DAbs &from)
{
	float w2d = Width2D();
	float h2d = Height2D();
	to.x = (from.x - Left2D())/w2d;
	to.y = (from.y - Top2D())/h2d;
	to.w = from.w/w2d;
	to.h = from.h/h2d;
}

void Engine::Convert(Line2DAbs &to, const Line2DPixel &from)
{
	float l2d = Left2D();
	float t2d = Top2D();
	to.beg.x = from.beg.x + l2d;
	to.beg.y = from.beg.y + t2d;
	to.end.x = from.end.x + l2d;
	to.end.y = from.end.y + t2d;
}
void Engine::Convert(Line2DAbs &to, const Line2DFloat &from)
{
	float w2d = Width2D();
	float h2d = Height2D();
	float l2d = Left2D();
	float t2d = Top2D();
	to.beg.x = from.beg.x*w2d + l2d;
	to.beg.y = from.beg.y*h2d + t2d;
	to.end.x = from.end.x*w2d + l2d;
	to.end.y = from.end.y*h2d + t2d;
}
void Engine::Convert(Line2DPixel &to, const Line2DAbs &from)
{
	float l2d = Left2D();
	float t2d = Top2D();
	to.beg.x = from.beg.x - l2d;
	to.beg.y = from.beg.y - t2d;
	to.end.x = from.end.x - l2d;
	to.end.y = from.end.y - t2d;
}
void Engine::Convert(Line2DFloat &to, const Line2DAbs &from)
{
	float w2d = Width2D();
	float h2d = Height2D();
	float l2d = Left2D();
	float t2d = Top2D();
	to.beg.x = (from.beg.x - l2d)/w2d;
	to.beg.y = (from.beg.y - t2d)/h2d;
	to.end.x = (from.end.x - l2d)/w2d;
	to.end.y = (from.end.y - t2d)/h2d;
}

void Engine::PixelAlignXY(Point2DAbs &pos)
{
	pos.x = toInt(pos.x)+0.5f;
	pos.y = toInt(pos.y)+0.5f;
}
void Engine::PixelAlignX(Point2DAbs &pos)
{
	pos.x = toInt(pos.x)+0.5f;
}
void Engine::PixelAlignY(Point2DAbs &pos)
{
	pos.y = toInt(pos.y)+0.5f;
}
void Engine::PixelAlignXY(Point2DPixel &pos)
{
	pos.x = toInt(pos.x)+0.5f;
	pos.y = toInt(pos.y)+0.5f;
}
void Engine::PixelAlignX(Point2DPixel &pos)
{
	pos.x = toInt(pos.x)+0.5f;
}
void Engine::PixelAlignY(Point2DPixel &pos)
{
	pos.y = toInt(pos.y)+0.5f;
}

float Engine::PixelAlignedX(float x)
{
	return toInt(x)+0.5f;
}
float Engine::PixelAlignedY(float y)
{
	return toInt(y)+0.5f;
}

void Engine::SaveConfig()
{
	RString name = GetUserParams();

	ParamFile cfg;
	cfg.Parse(name);
	cfg.Add("brightness",_usrBrightness);
	cfg.Add("multitexturing",_multitexturing);
	cfg.Add("useWBuffer",IsWBuffer());
	//cfg.Add("fovBottom",_aspectSettings.bottomFOV);
	cfg.Add("fovTop",_aspectSettings.topFOV);
	//cfg.Add("fovRight",_aspectSettings.rightFOV);
	cfg.Add("fovLeft",_aspectSettings.leftFOV);

	cfg.Add("uiTopLeftX",_aspectSettings.uiTopLeftX);
	cfg.Add("uiTopLeftY",_aspectSettings.uiTopLeftY);
	cfg.Add("uiBottomRightX",_aspectSettings.uiBottomRightX);
	cfg.Add("uiBottomRightY",_aspectSettings.uiBottomRightY);
	cfg.Save(name);
}
void Engine::LoadConfig()
{
	RString name = GetUserParams();

	ParamFile cfg;
	cfg.Parse(name);

	SetBrightness(cfg.ReadValue("brightness", 1.2f));
	if (cfg.FindEntry("multitexturing"))
	{
		SetMultitexturing(cfg>>"multitexturing");
	}
	//_aspectSettings.bottomFOV = cfg.ReadValue("fovBottom",0.75);
	_aspectSettings.topFOV = cfg.ReadValue("fovTop",0.75f);
	_aspectSettings.leftFOV = cfg.ReadValue("fovLeft",1.0f);

	_aspectSettings.uiTopLeftX = cfg.ReadValue("uiTopLeftX",0.0f);
	_aspectSettings.uiTopLeftY = cfg.ReadValue("uiTopLeftY",0.0f);
	_aspectSettings.uiBottomRightX = cfg.ReadValue("uiBottomRightX",1.0f);
	_aspectSettings.uiBottomRightY = cfg.ReadValue("uiBottomRightY",1.0f);
}


void Engine::SetFogColor( ColorVal fogColor )
{
	_fogColor=fogColor;
	FogColorChanged(_fogColor);
}

void Engine::ReinitCounters()
{
	#if _ENABLE_PERFLOG
	GPerfCounters.Reinit();
	GPerfProfilers.Reinit();
	_startGame=GlobalTickCount();
	#endif
}

void Engine::InitDraw( bool clear, PackedColor color )
{
	// TODO: brightness control
	if( _nightVision ) _accomodateEye=Color(0,8.0,0);
	else _accomodateEye=HWhite;
	_accomodateEye=_accomodateEye*_usrBrightness;
	_accomodateEye.SetA(1);
	//_accomodateEye.SaturateMinMax();
}

void Engine::FinishDraw()
{
	// add some expressions we consider intersting

	_frameTime = GlobalTickCount();
		
	// every second update stats
	// every frame update stats
	if (_frameTime0>0)
	{
		COUNTER(tris);
		_lastFrameDuration=_frameTime-_frameTime0;
		for( int i=0; i<NFrameDurations-1; i++ )
		{
			_frameDurations[i] = _frameDurations[i+1];
			_frameTriangles[i] = _frameTriangles[i+1];
		}
		_frameDurations[NFrameDurations-1] = _lastFrameDuration;
		_frameTriangles[NFrameDurations-1] = COUNTER_VALUE(tris);
	}
	_frameTime0 = _frameTime;

	// now its time to draw performance logs

}

void Engine::NextFrame()
{
	// swap frames - get ready for next frame
	
}
