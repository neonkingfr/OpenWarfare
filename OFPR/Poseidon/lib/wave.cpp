/*!
\file
Loading of wav files
\patch_internal 1.04 Date 07/16/2001 by Ondra. Disable ACM wav codec support.
Reasons:
- Codecs were never actually used.
- Codec interface does not support seeking, there is no MP3 codes.
- OGG serves the purpose much better.
*/

#include "wpch.hpp"
#include <Es/Common/win.h>

#if !defined _WIN32 || defined _XBOX
#include "riffFile.hpp"
#define WAVEFORMAT WAVEFORMAT_STRUCT
#else
#include <mmsystem.h>
#include <mmreg.h>
#endif

#define ENABLE_ACM_CODECS 0
#if ENABLE_ACM_CODECS
#include <msacm.h>
#pragma comment(lib,"msacm32")
#endif

#include "samples.hpp"
#include "global.hpp"
#include "fileServer.hpp"
#include <El/QStream/QBStream.hpp>

#include <El/Common/perfLog.hpp>

#if defined _WIN32 && !defined _XBOX
#pragma comment(lib,"winmm")
#endif

int WaveStream::GetDataClipped(void *data, int offset, int size) const
{
	if (size<0)
	{
		Fail("GetDataClipped: Negative size");
		return 0;
	}
	// clip offset..size to valid range
	int offsetDone = 0;
	if (offset<0)
	{
		int zeroData = -offset;
		saturateMin(zeroData,size);
		memset(data,0,zeroData);
		offset += zeroData;
		size -= zeroData;
		offsetDone += zeroData;

		data = (char *)data+zeroData;
	}
	
	if (offset>=0)
	{
		int total = GetUncompressedSize();
		int rest = total-offset;
		if (rest>0)
		{
			int unpack = size;
			saturateMin(unpack,rest);
			// verify valid range
			if 
			(
				offset<0 || unpack<0 || offset+unpack>total
			)
			{
				RptF
				(
					"Sound: out of range: offset %d, unpack %d, total %d",
					offset,unpack,total
				);
				RptF("  size %d, offsetDone %d",size,offsetDone);
			}
			else
			{
				int dataDone = GetData(data,offset,unpack);
				offsetDone += dataDone;
				size -= dataDone;

				data = (char *)data+dataDone;
			}
		}
	}

	if (size>0)
	{
		memset(data,0,size);
	}

	return offsetDone;

}

int WaveStream::GetDataLooped(void *data, int offset, int size) const
{
	if (size<0)
	{
		Fail("GetDataLooped: Negative size");
		return 0;
	}
	// clip offset..size to valid range
	int offsetDone = 0;
	if (offset<0)
	{
		int zeroData = -offset;
		saturateMin(zeroData,size);
		memset(data,0,zeroData);
		offset += zeroData;
		size -= zeroData;
		offsetDone += zeroData;

		data = (char *)data+zeroData;
	}
	
	if (offset>=0)
	{
		int total = GetUncompressedSize();
		if (offset>=total)
		{
			offset = offset%total;
		}
		int rest = total-offset;
		if (rest>0)
		{
			int unpack = size;
			saturateMin(unpack,rest);
			// verify valid range
			if 
			(
				offset<0 || unpack<0 || offset+unpack>total
			)
			{
				RptF
				(
					"Sound: out of range: offset %d, unpack %d, total %d",
					offset,unpack,total
				);
				RptF("  size %d, offsetDone %d",size,offsetDone);
			}
			else
			{
				int dataDone = GetData(data,offset,unpack);
				offsetDone += dataDone;
				size -= dataDone;

				data = (char *)data+dataDone;
			}
		}
		if (size>0)
		{
			// end reached, but we need some more data
			int sizeStep = size;
			saturateMin(sizeStep,total);
			int dataDone = GetData(data,0,sizeStep);
			offsetDone += dataDone;
			size -= dataDone;

			data = (char *)data+dataDone;
			
		}
	}

	if (size>0)
	{
		memset(data,0,size);
	}

	return offsetDone;

}

// WaveStreamPlain is implementation of WaveStream interface
// it is able to read from uncompressed data source (WaveBuffer)

class WaveStreamPlain: public WaveStream
{
	// 
	WaveBuffer _data;

	public:
	// construction
	WaveStreamPlain(const WaveBuffer &buffer);

	// implementation of WaveStream
	virtual int GetUncompressedSize() const;
	virtual void GetFormat(WAVEFORMATEX &format) const;
	virtual int GetData(void *data, int offset, int size) const;
	virtual bool IsFromBank(QFBank *bank) const;
};

WaveStreamPlain::WaveStreamPlain(const WaveBuffer &buffer)
:_data(buffer)
{
}

void WaveStreamPlain::GetFormat(WAVEFORMATEX &format) const
{
	_data.GetWaveFormat(format);
}

int WaveStreamPlain::GetUncompressedSize() const
{
	return _data.UnpackedSize();
}

int WaveStreamPlain::GetData(void *data, int offset, int size) const
{
	// assume _data is uncompressed
	// therefore we can unpack from any posittion
	int rest = _data.UnpackedSize()-offset;
	if (rest>0)
	{
		int unpack = size;
		saturateMin(unpack,rest);
		int unpacked = _data.UnpackPart(data,offset,unpack);

		return unpacked;
	}
	return 0;
}

bool WaveStreamPlain::IsFromBank(QFBank *bank) const
{
	return _data.IsFromBank(bank);
}


class WaveFile
{
	private:
	QIFStream _file;

	// note: sometimes we need to allocate more that WAVEFORMATEX
	WAVEFORMATEX *_format;
	
	DWORD _dataOffset;
	DWORD _dataSize;
	DWORD _dataRest;
	bool error;
	DWORD _samples; // actual number of samples
	

	protected:
	void New(const QIFStream &file);
	void Delete();

	void FreeFormat();
	void CreateFormat(int size);
	
	public:
	WaveFile();
	~WaveFile(){Delete();}

	void Open(const QIFStream &file){New(file);}
	void StartDataRead();
	UINT Read( UINT cbRead, char *Dest );

	DWORD DataSize() const {return _dataSize;}
	DWORD DataOffset() const {return _dataOffset;}
	
	const WAVEFORMATEX &Format() const {return *_format;} 
	DWORD GetSamples() const {return _samples;}

	bool GetError() const {return error;}

	IFileBuffer *GetBuffer() const {return _file.GetBuffer();}
};

//inline void exc(){throw ios::failure(ios::failbit);}
//inline void exc( const char *file ){ErrorMessage("Error loading file %s",file);}

#define exc(file) {return;}

#define RIFF_FOURCC_C(ch0, ch1, ch2, ch3)  \
    ((DWORD)(BYTE)(ch0) | ((DWORD)(BYTE)(ch1) << 8) |  \
    ((DWORD)(BYTE)(ch2) << 16) | ((DWORD)(BYTE)(ch3) << 24 ))


#define RIFF_FOURCC RIFF_FOURCC_C('R','I','F','F')
#define RIFF_WAVE RIFF_FOURCC_C('W','A','V','E')
#define RIFF_fmt RIFF_FOURCC_C('f','m','t',' ')
#define RIFF_data RIFF_FOURCC_C('d','a','t','a')
#define RIFF_fact RIFF_FOURCC_C('f','a','c','t')

struct RiffChunk
{
	DWORD id;
	DWORD len;
};

WaveFile::WaveFile()
{
	_format=NULL;
}

void WaveFile :: New(const QIFStream &file)
{
	error = true;
	// Initialization...

	_samples = 0; // actual number of samples

	_file = file;
	_file.seekg(0,QIOS::beg);

	if( _file.fail() ) exc(file);

	// check file header

	RiffChunk chunk;
	_file.read(&chunk,sizeof(chunk));
	if (_file.fail() || chunk.id!=RIFF_FOURCC) exc(filename);

	DWORD fformat;
	_file.read(&fformat,sizeof(fformat));
	if (_file.fail() || fformat!=RIFF_WAVE) exc(filename);

	// process chunks
	// search for fmt chunk
		
	// Search the input file for for the 'fmt ' chunk.

	for(;;)
	{
		_file.read(&chunk,sizeof(chunk));
		if (_file.fail()) exc(filename);
		if (chunk.id!=RIFF_fmt)
		{
			// skip chunk
			_file.seekg(chunk.len,QIOS::cur);
			continue;
		}
		// check if it is valid
		if (chunk.len<sizeof(WAVEFORMATEX))
		{
			if (chunk.len<sizeof(WAVEFORMAT))
			{
				ErrF("Too short fmt chunk in PCM wave file");
				exc(filename);
			}
			else
			{
				// try to read old PCM format
				WAVEFORMAT fmt;
				_file.read(&fmt,sizeof(fmt));
				if (_file.fail()) exc(filename);
				_file.seekg(chunk.len-sizeof(fmt),QIOS::cur); // skip rest of chunk
				CreateFormat(sizeof(WAVEFORMATEX));
				_format->cbSize = 0;
				if (fmt.wFormatTag!=WAVE_FORMAT_PCM)
				{
					ErrF("Old fmt chunk in PCM wave file");
					exc(filename);
				}
				_format->wFormatTag = fmt.wFormatTag;
				_format->nAvgBytesPerSec = fmt.nAvgBytesPerSec;
				_format->nBlockAlign = fmt.nBlockAlign;
				_format->nChannels = fmt.nChannels;
				_format->nSamplesPerSec = fmt.nSamplesPerSec;
				_format->wBitsPerSample = fmt.nBlockAlign/fmt.nChannels*8;
			}
		}
		else
		{
			// read format
			CreateFormat(chunk.len);
			_file.read(_format,chunk.len);
			if (_file.fail()) exc(filename);

			if (_format->wFormatTag==WAVE_FORMAT_PCM )
			{
				if (chunk.len!=sizeof(WAVEFORMATEX))
				{
					ErrF("Extra data in fmt chunk (PCM wave)");
				}
				_format->cbSize=0;

			}
		}
		break;
	}

	
	error = false;
}

/*  This routine has to be called before WaveReadFile as it searchs for the chunk to descend into for
		reading, that is, the 'data' chunk.  For simplicity, this used to be in the open routine, but was
		taken out and moved to a separate routine so there was more control on the chunks that are before
		the data chunk, such as 'fact', etc... */

void WaveFile :: StartDataRead()
{  
	if (error) return;  
	// Do a nice little seek...
	// now we are just after the format
	// we asssume data will be somewhere ahead

	RiffChunk chunk;
	for(;;)
	{
		_file.read(&chunk,sizeof(chunk));
		if (_file.fail())
		{
			error = true;
			break;
		}
		if (chunk.id==RIFF_fact)
		{
			DWORD fact;
			_file.read(&fact,sizeof(fact));
			_file.seekg(chunk.len-sizeof(fact),QIOS::cur);
			_samples = fact;
			//LogF("Fact %d",fact);
		}
		else if (chunk.id==RIFF_data)
		{
			_dataSize = chunk.len;
			_dataRest = chunk.len;
			_dataOffset = _file.tellg();
			break;
		}
		else
		{
			// skip chunk
			_file.seekg(chunk.len,QIOS::cur);
			continue;
		}
	}

}


/*  This will read wave data from the wave file.  Makre sure we're descended into
		the data chunk, else this will fail bigtime!
		hmmioIn         - Handle to mmio.
		cbRead          - # of bytes to read.   
		pbDest          - Destination buffer to put bytes.
		cbActualRead- # of bytes actually read.
*/


UINT WaveFile :: Read( UINT cbRead, char *Dest )
{
	if (error) return 0;
	
	UINT cbDataIn = cbRead;
	if( cbDataIn>_dataRest ) cbDataIn = _dataRest;
	
	_dataRest -= cbDataIn;
	
	_file.read(Dest,cbDataIn);

	if (_file.fail())
	{
		error = true;
		return 0;
	}
	
	return cbDataIn;
}

/*      This will close the wave file openned with WaveOpenFile.  
*/

void WaveFile::CreateFormat(int size)
{
	FreeFormat();
	_format = (WAVEFORMATEX *)new char[size];
}

void WaveFile::FreeFormat()
{
	if (_format)
	{
		delete[] (char *)_format;
		_format = NULL;
	}
}

void WaveFile :: Delete()
{
	FreeFormat();

	// no need to close file
	//_file.Close();
}

#ifdef _MSC_VER
const int WSSMagic='0SSW';
#else
const int WSSMagic=StrToInt("WSS0");
#endif

struct WSSHeader
{
	int magic;
	char deltaPack;
	char resvd[3];
};

class UnpackDelta8
{
	enum {MaxDelta=32768};
	enum {MaxDeltaInv=127};

	int _delta[MaxDeltaInv*2+1];

	public:
	UnpackDelta8();

	int Unpack(short *dest, const char *src, int destSize, int startValue);
	int Skip(const char *src, int destSize, int startValue);
};
class UnpackDelta4
{
	enum {MaxDelta=8192};
	enum {MaxDeltaInv=7};

	static const int _delta[MaxDeltaInv*2+1];

	public:
	UnpackDelta4();

	int Unpack(short *dest, const char *src, int destSize, int startValue);
	int Skip(const char *src, int destSize, int startValue);
};

const int UnpackDelta4::_delta[MaxDeltaInv*2+1]=
{
	-8192,-4096,-2048,-1024,-512,-256,-64,
	0, // 7
	64,256,512,1024,2048,4096,8192
};

inline int Sat16S( int val )
{
	if( val>0x7fff ) return 0x7fff;
	if( val<-0x8000 ) return -0x8000;
	return val;
}

UnpackDelta8::UnpackDelta8()
{
	int i;
	double base=pow(MaxDelta,1.0f/MaxDeltaInv);
	_delta[MaxDeltaInv]=0;
	for( i=1; i<=MaxDeltaInv; i++ )
	{
		//        / -1.084618362^-x  for x<0  (-128 to -1)
		// f(x)= {   0               for x=0  (0)
		//        \  1.084618362^x   for x>0  (1 to 127)
		int v=toInt(pow(base,i));
		_delta[MaxDeltaInv+i]=+v;
		_delta[MaxDeltaInv-i]=-v;
	}
}

int UnpackDelta8::Unpack( short *dest, const char *src, int destSize, int startValue )
{
	Assert( (destSize&1)==0 );
	destSize/=2;
	int aVal=startValue;
	while( --destSize>=0 )
	{
		int v=(signed char)*src++;
		v=_delta[v+MaxDeltaInv];
		aVal+=v;
		*dest++=Sat16S(aVal);
	}
	return aVal;
}

int UnpackDelta8::Skip(const char *src, int destSize, int startValue)
{
	Assert( (destSize&1)==0 );
	destSize/=2;
	int aVal=startValue;
	while( --destSize>=0 )
	{
		int v=(signed char)*src++;
		v=_delta[v+MaxDeltaInv];
		aVal+=v;
	}
	return aVal;
}

UnpackDelta4::UnpackDelta4()
{
}

int UnpackDelta4::Unpack( short *dest, const char *src, int destSize, int startValue )
{
	// two samples a time
	Assert( (destSize&3)==0 );
	destSize/=2;
	int aVal=startValue;
	while( (destSize-=2)>=0 )
	{
		int v=*src++;
		int v1=(v>>4)&0xf;
		int v2=(v&0xf);
		v1=_delta[v1];
		v2=_delta[v2];
		aVal+=v1;
		*dest++=Sat16S(aVal);
		aVal+=v2;
		*dest++=Sat16S(aVal);
	}
	return aVal;
}

int UnpackDelta4::Skip( const char *src, int destSize, int startValue )
{
	// two samples a time
	Assert( (destSize&3)==0 );
	destSize/=2;
	int aVal=startValue;
	while( (destSize-=2)>=0 )
	{
		int v=*src++;
		int v1=(v>>4)&0xf;
		int v2=(v&0xf);
		v1=_delta[v1];
		v2=_delta[v2];
		aVal+=v1;
		aVal+=v2;
	}
	return aVal;
}

static UnpackDelta8 UnpackD8;
static UnpackDelta4 UnpackD4;

WaveStream *WSSLoadFile( const char *name )
{
	QIFStream in;
	//Log("WSSLoadFile Open %s",name);
	GFileServer->Open(in,name);
	// duplicate file buffer
	if( in.fail() || in.eof() ) return NULL;
	WSSHeader wHeader;
	WAVEFORMATEX header;
	in.read((char *)&wHeader,sizeof(wHeader));
	if( wHeader.magic!=WSSMagic )
	{
		in.seekg(-sizeof(wHeader),QIOS::cur);
		wHeader.deltaPack=0;
	}
	int deltaPack=wHeader.deltaPack;
	in.read((char *)&header,sizeof(header));
	if( in.fail() || in.eof() ) return NULL;
	header.cbSize=0;
	IFileBuffer *fBuffer=in.GetBuffer();

	if( in.fail() || in.eof() ) return NULL;
	if( in.rest()<=0 ) return NULL;
	// return part of file buffer

	WaveBuffer buffer(fBuffer,header,in.tellg(),in.rest(),deltaPack);
	return new WaveStreamPlain(buffer);
}

DEFINE_FAST_ALLOCATOR(WaveBuffer)

WaveBuffer::WaveBuffer
(
	IFileBuffer *buf, const WAVEFORMATEX &format, int start, int size, int deltaPack
)
:_buffer(buf),_format(format),
_skip(start),_size(size),_deltaPack(deltaPack),
_lastWriteOffset(0),_lastValue(0)
{
}

bool WaveBuffer::IsFromBank(QFBank *bank) const
{
	return _buffer->IsFromBank(bank);
}

int WaveBuffer::UnpackPart(void *dest, int offset, int size) const
{
	int maxSize = UnpackedSize()-offset;
	saturateMin(size,maxSize);
	if( _deltaPack==0 )
	{
		memcpy(dest,Data()+offset,size);
	}
	else if( _deltaPack==4 )
	{
		// check offset
		// if offset is not the same, we have to restart and rewind
		if (offset!=_lastWriteOffset)
		{
			_lastValue = UnpackD4.Skip(Data(),offset,0);
		}

		_lastValue = UnpackD4.Unpack((short *)dest,Data()+offset/4,size,_lastValue);
		_lastWriteOffset = offset+size;
	}
	else if( _deltaPack==8 )
	{

		if (offset!=_lastWriteOffset)
		{
			_lastValue = UnpackD8.Skip(Data(),offset,0);
		}

		_lastValue = UnpackD8.Unpack((short *)dest,Data()+offset/2,size,_lastValue);
		_lastWriteOffset = offset+size;
	}
	else
	{
		memset(dest,0,size);
		Fail("Invalid deltaPack value");
	}
	return size;
}

void WaveBuffer::Unpack( void *dest, int destSize ) const
{
	if( _deltaPack==0 )
	{
		Assert( destSize==UnpackedSize() );
		memcpy(dest,Data(),destSize);
	}
	else if( _deltaPack==4 )
	{
		Assert( destSize==UnpackedSize() );
		UnpackD4.Unpack((short *)dest,Data(),UnpackedSize(),0);
	}
	else if( _deltaPack==8 )
	{
		Assert( destSize==UnpackedSize() );
		UnpackD8.Unpack((short *)dest,Data(),UnpackedSize(),0);
	}
	else
	{
		Fail("Invalid deltaPack value");
	}
}


// WaveStreamCodec is implementation of WaveStream interface
// it is able to read from compressed data source using any codec

#if ENABLE_ACM_CODECS

class WaveStreamCodec: public WaveStream
{
	// 
	WaveFile _file;
	WAVEFORMATEX _format;
	HACMSTREAM _stream;
	HACMDRIVER _driver;

	mutable int _streamOffset; // read cursor - stream
	//mutable int _lastWriteOffset; // write cursor - uncompressed stream

	// some data may be rest from previous unpack operation
	mutable AutoArray<char> _unpackTemp;
	mutable int _unpackTempOffset; // offset in unpacked data

	public:
	// construction
	WaveStreamCodec(const QIFStream &file);
	~WaveStreamCodec();

	// implementation of WaveStream
	virtual int GetUncompressedSize() const;
	virtual void GetFormat(WAVEFORMATEX &format) const;
	virtual int GetData(void *data, int offset, int size) const;

	virtual bool IsFromBank(QFBank *bank) const;

	private:
	void GetNextTempBuffer() const;
	void SeekTempBuffer(int offset) const;
	void GetFromTempBuffer(void *data, int use) const;
};

 
WaveStreamCodec::WaveStreamCodec(const QIFStream &file)
{
	_stream = NULL;
	_driver = NULL;

	// start reading wave data file handle
	_file.Open(file);
	if (_file.GetError()) return;

	_file.StartDataRead();
	if (_file.GetError()) return;

	_format = _file.Format();
	// always convert to PCM mono, 16b
	_format.wFormatTag = WAVE_FORMAT_PCM;
	_format.wBitsPerSample = 16;
	//_format.nChannels = 1;
	_format.cbSize = 0;
	_format.nBlockAlign = _format.wBitsPerSample/8*_format.nChannels;
	_format.nAvgBytesPerSec = _format.nBlockAlign*_format.nSamplesPerSec;

	MMRESULT mmr;
	mmr = ::acmStreamOpen
	(
		&_stream,_driver,
		const_cast<WAVEFORMATEX *>(&_file.Format()),&_format,
		0L,0L,0L,0L
	);
	if (mmr)
	{
		LogF("acmStreamOpen failed");
		return;
	}

	_streamOffset = 0;
	//_lastWriteOffset = 0;

	_unpackTemp.Clear();
	_unpackTempOffset = 0;

	//LogF("nBlockAlign %d",_file.Format().nBlockAlign);
	//LogF("nAvgBytesPerSec %d",_file.Format().nAvgBytesPerSec);
}

WaveStreamCodec::~WaveStreamCodec()
{
	if (_stream)
	{
		::acmStreamClose(_stream,0);
		_stream = NULL;
		//LogF("Stream close");
	}

	if (_driver)
	{
		::acmDriverClose(_driver,0);
		_driver = NULL;
	}

	_unpackTemp.Clear();

}

int WaveStreamCodec::GetUncompressedSize() const
{
	return _file.GetSamples()*_format.wBitsPerSample/8*_format.nChannels;
}

void WaveStreamCodec::GetFormat(WAVEFORMATEX &format) const
{
	format = _format;
}

void WaveStreamCodec::GetNextTempBuffer() const
{
	// get next block from the codec
	// if temporary buffer is not empty, it is discarder

	_unpackTempOffset += _unpackTemp.Size();

	int totalSize = GetUncompressedSize();

	const int unpackBlock = 32*1024;

	int tempSize = unpackBlock;
	saturateMin(tempSize,totalSize-_unpackTempOffset);

	if (tempSize<=0)
	{
		 // nothing to decompress
		 _unpackTemp.Clear();
		return;
	}

	_unpackTemp.Reserve(unpackBlock,unpackBlock);
	_unpackTemp.Resize(tempSize);

	ACMSTREAMHEADER header;
	memset(&header,0,sizeof(header));
	header.cbStruct = sizeof(header);
	BYTE *streamStart = (BYTE *)(_file.GetBuffer()->GetData()+_file.DataOffset());

	header.pbSrc = streamStart + _streamOffset;
	header.cbSrcLength = _file.DataSize()-_streamOffset;

	Assert ((int)header.cbSrcLength>=0);

	header.pbDst = (BYTE *)_unpackTemp.Data();
	header.cbDstLength = tempSize;

	MMRESULT mmr;
	mmr = ::acmStreamPrepareHeader(_stream,&header,0);
	if (mmr)
	{
		memset(_unpackTemp.Data(),0,unpackBlock);
		return;
	}

	DWORD flags = ACM_STREAMCONVERTF_END;
	// note: we may need to flush data
	mmr = ::acmStreamConvert(_stream,&header,flags);
	if (mmr)
	{
		LogF("acmStreamConvert failed");
	}

	_unpackTemp.Resize(header.cbDstLengthUsed);
	_streamOffset += header.cbSrcLengthUsed;
	//LogF("%d:%d unpacked to temporary buffer",_unpackTempOffset,header.cbDstLengthUsed);
	//LogF("  stream offset is now %d of %d",_streamOffset,_file.DataSize());

	::acmStreamUnprepareHeader(_stream,&header,0);

}

void WaveStreamCodec::SeekTempBuffer(int offset) const
{
	if (offset!=_unpackTempOffset)
	{
		_unpackTemp.Resize(0);
	}
}

void WaveStreamCodec::GetFromTempBuffer(void *data, int use) const
{
	if (data)
	{
		memcpy(data,_unpackTemp.Data(),use);
	}
	// delete used part from _unpackTemp
	memmove
	(
		_unpackTemp.Data(),
		_unpackTemp.Data()+use,_unpackTemp.Size()-use
	);
	_unpackTemp.Resize(_unpackTemp.Size()-use);
	_unpackTempOffset += use;

}

bool WaveStreamCodec::IsFromBank(QFBank *bank) const
{
	// check _file
	return _file.GetBuffer()->IsFromBank(bank);
}

int WaveStreamCodec::GetData(void *data, int offset, int size) const
{
	if (!_stream)
	{
		memset(data,0,size);
		return 0;
	}
	// note: offset should be sequential
	// if not, we need to rewind the buffer
	// this can be done only by closing the stream
	// opening again
	int totalSize = GetUncompressedSize();
	int dataDone = 0;
	int sizeUnpack = size;

	if (offset!=_unpackTempOffset)
	{
		//LogF("Rewind required (%d to %d)",_unpackTempOffset,offset);
		if (offset<_unpackTempOffset)
		{
			// reset stream to start from the very beginning
			_unpackTemp.Resize(0);
			_unpackTempOffset = 0;
			_streamOffset = 0;
		}
		// skip as many as neccessary to get where we need to be
		while( _unpackTempOffset + _unpackTemp.Size()<offset)
		{
			GetNextTempBuffer();
			if (_unpackTemp.Size()<=0) break;
		}
		//int keep = _unpackTempOffset + _unpackTemp.Size()-offset;
		//int discard = _unpackTemp.Size()-keep;

		int discard = offset - _unpackTempOffset;

		GetFromTempBuffer(NULL,discard);
		//LogF("Discarded %d from previous step",discard);

		//SeekTempBuffer(offset);
	}

	//header.pbDst = (BYTE *)temp;
	saturateMin(sizeUnpack,totalSize-offset);
	//LogF("Convert %d:%d of %d",offset,sizeUnpack,totalSize);
	// ACM stream is open

	while (sizeUnpack>0)
	{
		// use what has been rest in temporary buffer
		if (_unpackTemp.Size()>0)
		{
			int use = sizeUnpack;
			saturateMin(use,_unpackTemp.Size());

			GetFromTempBuffer(data,use);
			//LogF("Used %d from previous step",use);

			data = (char *)data + use;
			dataDone += use;
			sizeUnpack -= use;
			size -= use;
		}

		if (sizeUnpack>0)
		{
			GetNextTempBuffer();
			// check for error
			if (_unpackTemp.Size()<=0)
			{
				LogF("No more data");
				break;
			}
		}

	}

	return dataDone;
}

#endif

WaveStream *WaveLoadFile( const char *filename )
{
	QIFStreamB file;
	file.AutoOpen(filename);
	WaveFile input;
	input.Open(file);
	if (input.GetError()) return NULL;

	// check file format

	if (input.Format().wFormatTag == WAVE_FORMAT_PCM)
	{
		input.StartDataRead();
		if (input.GetError()) return NULL;

		IFileBuffer *fBuffer=input.GetBuffer();
		// return part of file buffer
		WaveBuffer buffer
		(
			fBuffer,input.Format(),
			input.DataOffset(),input.DataSize(),0
		);
		return new WaveStreamPlain(buffer);
	}
	else
	{
		#if ENABLE_ACM_CODECS
		// pass it to Codec processing
		return new WaveStreamCodec(file);
		#else
		ErrF("non-PCM format %d",input.Format().wFormatTag);
		return NULL;
		#endif

	}
}

#if 1
// OGG stream implementation

#include <vorbis/vorbisfile.h>

class WaveStreamOGG: public WaveStream
{
	mutable QIFStream _file;
	mutable OggVorbis_File _vf;
	mutable int _bitstream;
	mutable int _offset;

	bool _vfOpen;
	WAVEFORMATEX _format;

	public:
	WaveStreamOGG(const QIFStream &file);
	~WaveStreamOGG();

	bool GetError() const {return !_vfOpen;}
	// implement WaveStream interface
	virtual int GetUncompressedSize() const;
	virtual void GetFormat(WAVEFORMATEX &format) const;
	virtual int GetData(void *data, int offset, int size) const;
	virtual bool IsFromBank(QFBank *bank) const;
};

// use vorbis file interface

/*!
\patch 1.23 Date 09/11/2001 by Ondra
- Improved: Vorbis RC2 decoder used instead Beta 4.
This could improve OGG reading compatibility.
*/

// vorbis callback interface to QIFStream

static size_t readQIFStream(void *buffer, size_t size, size_t count, void *stream)
{
	QIFStream *str = (QIFStream *)stream;
	int rd = size*count;
	int rest = str->rest();
	if (rd>rest) rd = rest;
	str->read(buffer,rd);
	return rd;
}

static int seekQIFStream(void *stream, ogg_int64_t pos, int origin)
{
	QIFStream *str = (QIFStream *)stream;
	switch (origin)
	{
		case SEEK_CUR:
			str->seekg(pos,QIOS::cur);
			break;
		case SEEK_END:
			str->seekg(pos,QIOS::end);
			break;
		case SEEK_SET:
			str->seekg(pos,QIOS::beg);
			break;
	}	
	// no error
	return 0;
}

static int closeQIFStream(void *stream)
{
	// no close required
	return 0;
}

static long tellQIFStream(void *stream)
{
	QIFStream *str = (QIFStream *)stream;
	return str->tellg();
}

WaveStreamOGG::WaveStreamOGG(const QIFStream &file)
{
	_file = file;
	ov_callbacks callbacks;
	callbacks.read_func = readQIFStream;
	callbacks.seek_func = seekQIFStream;
	callbacks.close_func = closeQIFStream;
	callbacks.tell_func = tellQIFStream;

	int ok = ov_open_callbacks(&_file,&_vf,NULL,0,callbacks);

	_vfOpen = ok>=0;
	_bitstream = 0;
	_offset = 0;

	// get uncompressed format
	if (_vfOpen)
	{
		vorbis_info *info = ov_info(&_vf,-1);

		_format.cbSize = 0;
		_format.wFormatTag = WAVE_FORMAT_PCM;
		_format.wBitsPerSample = 16;
		_format.nChannels = info->channels;
		_format.nBlockAlign = info->channels*2;
		_format.nSamplesPerSec = info->rate;
		_format.nAvgBytesPerSec = info->rate*2*info->channels;
	}
}

WaveStreamOGG::~WaveStreamOGG()
{
	if (_vfOpen)
	{
		_vfOpen = false;
		ov_clear(&_vf);
	}
}

int WaveStreamOGG::GetUncompressedSize() const
{
	// stream is already open
	if (!_vfOpen) return 0;
	int total = ov_pcm_total(&_vf,-1);
	//LogF("GetUncompressedSize %d",(int)total);

	return total*_format.nChannels*2;

}

void WaveStreamOGG::GetFormat(WAVEFORMATEX &format) const
{
	// get uncompressed format
	if (!_vfOpen)
	{
		format.cbSize = 0;
		format.wFormatTag = WAVE_FORMAT_PCM;
		format.wBitsPerSample = 16;
		format.nChannels = 1;
		format.nBlockAlign = 2;
		format.nSamplesPerSec = 22000;
		format.nAvgBytesPerSec = 44000;
		return;
	}
	vorbis_info *info = ov_info(&_vf,-1);

	format.cbSize = 0;
	format.wFormatTag = WAVE_FORMAT_PCM;
	format.wBitsPerSample = 16;
	format.nChannels = info->channels;
	format.nBlockAlign = info->channels*2;
	format.nSamplesPerSec = info->rate;
	format.nAvgBytesPerSec = info->rate*2*info->channels;
}

int WaveStreamOGG::GetData(void *data, int offset, int size) const
{
	if (!_vfOpen) return 0;
	if (_offset!=offset)
	{
		int pcm = offset / (_format.nChannels*2);
		//LogF("Seek to %d (PCM %d)",offset,pcm);
		// we need to seek in source stream
		int err = ov_pcm_seek(&_vf,pcm);
		if (err!=0)
		{
			// ignore error, but report it
			LogF("Error %d while seeking",err);
		}
		_offset = offset;
	}

	int dataDone = 0;
	while (size>0)
	{
		int obs = _bitstream;
		int rd = ov_read(&_vf,(char *)data,size,0,2,1,&_bitstream);
		if (_bitstream!=obs)
		{
			LogF("Stream %d",_bitstream);
		}
		if (rd<=0)
		{
			LogF("Stream read error");
			break;
		}

		size -= rd;
		data = (char *)data+rd;
		dataDone += rd;
		_offset += rd;
	}
	return dataDone;
}


bool WaveStreamOGG::IsFromBank(QFBank *bank) const
{
	return _file.GetBuffer()->IsFromBank(bank);
}


WaveStream *OGGLoadFile( const char *name )
{
	QIFStream in;
	//Log("WSSLoadFile Open %s",name);
	GFileServer->Open(in,name);
	// duplicate file buffer
	if( in.fail() || in.eof() ) return NULL;

	if( in.rest()<=0 ) return NULL;
	// return part of file buffer

	WaveStreamOGG *ret = new WaveStreamOGG(in);
	if (ret->GetError())
	{
		delete ret;
		return NULL;
	}
	return ret;
}

#endif

WaveStream *SoundLoadFile(const char *name)
{
	const char *ext = strrchr(name,'.');
	if (!ext)
	{
		return WSSLoadFile(name);
	}
	// only header is needed - whole files are cached
	if (!stricmp(ext, ".wav"))
	{
		return WaveLoadFile(name);
	}
	if (!stricmp(ext, ".ogg"))
	{
		return OGGLoadFile(name);
	}
	return WSSLoadFile(name);
}
