/*!
\file
Scene interface
*/
#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SCENE_HPP
#define _SCENE_HPP

#include "types.hpp"
#include <El/Math/math3d.hpp>
//#include "object.hpp"
//#include "engine.hpp"
#include "lights.hpp"

#define HORIZONT_Z Glob.config.horizontZ
#define OBJECT_Z Glob.config.objectsZ

#define MIN_FOG (200.0f)
#define MAX_FOG (HORIZONT_Z-20)

#define MIN_SHADOWFOG (100.0f)
#define MAX_SHADOWFOG (Glob.config.shadowsZ)

const float CloudScale=0.1;

const float MinSkyFog = 4000.0f*CloudScale;
const float MaxSkyFog = 19000.0f*CloudScale;


#define LEN_FOG_TABLE 256
class FogFunction
{
	private:
	//float _fog[LEN_FOG_TABLE];
	byte _fog[LEN_FOG_TABLE];
	float _start2,_end2;
	float _divisor;

	public:
	//FogFunction( float start, float end, float (*function)( float distRel ) );
	FogFunction();
	void Set
	(
		float start, float end,
		float (*function)( float distRel, float start, float end )
	);
	//float operator () ( float distSquare ) const;
	//byte operator () ( float distSquare ) const;
	int operator () ( float distSquare ) const; // avoid partial stall
};

// scene defines lights and camera positions
// it is used to transform and light vertices

#include <Es/Memory/normalNew.hpp>

class SortObject: public RefCount
{
	public:
	Ref<Object> object; // object must not be destroyed until it is drawn
	// simple pointer is enough - Ref<> to object gurantees shape exists
	LODShapeWithShadow *shape; // randomized shape
	//int forceLOD; // if forceLOD<0 use autodetected LOD

	signed char drawLOD,shadowLOD;
	signed char passNum;
	signed char forceDrawLOD; // forced draw LOD

	unsigned char orClip; // or clip flags only - andClip would be always 0 

	bool notUsed; // not used when list was created - delete it

	//Vector3 bCenter; // world space bounding sphere center
	float radius; // bounding sphere radius

	float distance2;

	USE_FAST_ALLOCATOR
};

typedef RefArray<SortObject> SortObjectList;

class RemmemberShadow: public RefCount, public CLRefLink
{
	friend class ShadowCache;

	private:
	Ref<Shape> _shadow;
	OLink<Object> _object;
	Vector3 _lightDir;
	Matrix4 _objectPos;
	int _level; // which LOD of object is used
	Time _lastUsed;
	bool _splitOnly; // shadow cache is also used to contain split surfaces
		
	public:
	RemmemberShadow();
	// init shadow or split to fit on surface
	void Init
	(
		Object *object, Vector3Par lightDir,
		int level, Matrix4Par pos, bool splitOnly=false
	);
	bool IsShadow( Object *object, int level ) const;
	Object *GetObject() const {return _object;}
	Vector3Val LightDir() const {return _lightDir;}
	Matrix4Val ObjectPos() const {return _objectPos;}
	//const Frame &Coordinates() const {return _coordinates;}

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

#include "El/FreeOnDemand/memFreeReq.hpp"

class ShadowCache: public MemoryFreeOnDemandHelper
{
	private:
	CLRefList<RemmemberShadow> _data;

	public:
	ShadowCache();
	~ShadowCache();

	Ref<Shape> Shadow
	(
		Object *object, Vector3Par lightDir,
		int level, Matrix4Par pos, bool splitOnly=false
	);
	void ShadowChanged(Object *obj);
	void Clear();
	void CleanUp();

	// implement MemoryFreeOnDemandHelper functions
	virtual size_t FreeOneItem();
	virtual float Priority();
};

enum PreloadedShape
{
	//ATGMExplosion,LAWExplosion,ShellExplosion,
	//Shell,ATMissile,HeliMissile,HeliRocket,
	CobraLight,SphereLight,HalfLight, Marker,
	CraterShell,CraterBullet,
	SlopBlood,
	CloudletBasic,CloudletFire,CloudletFireD,CloudletWater,CloudletMissile,
	Cloud1,Cloud2,Cloud3,Cloud4,
	CinemaBorder,

	FootStepL,FootStepR,

	ForceArrowModel,SphereModel,RectangleModel,PaperCarModel,
	BulletLine,

	SkySphere, HorizontObject,

	FxExploGround1, FxExploGround2,

	FxExploArmor1, FxExploArmor2, FxExploArmor3, FxExploArmor4,

	FxCartridge,

	MaxPreloadedShape
};

DECL_ENUM(PreloadedTexture)

typedef Ref<Light> ActiveLightPointer;

class LightList: public FindArray<ActiveLightPointer,MemAllocSS>
{
	public:
	LightList( bool staticStorage=false );
	LightList( const LightList &src );
};

class PreloadedTextures
{
	RefArray<Texture> _data;

	public:
	PreloadedTextures();
	~PreloadedTextures();

	void Preload( bool all );
	void Clear();

	Texture *New( RStringB name ); // make texture permanent
	Texture *New( PreloadedTexture id ); // predefined texture ids
};

extern PreloadedTextures GPreloadedTextures;


//! Scene rendering
/*!
This class performs scene graph management.
This includes lod and light management.
*/
class Scene
{
	private:

	Color _constantColor;
	Color _skyColor;
	float _constantFog;

	//Engine *_engine;
	Ref<Texture> _skyTexture;

	Ref<LODShapeWithShadow> _preloaded[MaxPreloadedShape];

	Camera *_camera;
	LightSun *_mainLight;

	FindArray< Link<Light> > _lights;
	LightList _aLights;
	
	Ref<Object> _collisionStar;

	// fog functions for different types of objects
	FogFunction _fog,_skyFog,_shadowFog,_tacticalFog;
	float _tacticalVisibility; // AI sensors
	float _rainRange; // display boundaries main control
	float _fogMaxRange,_fogMinRange; // display boundaries
	float _shadowFogMaxRange,_shadowFogMinRange; // display boundaries
	
	mutable float _lodInvWidth;

	//mutable float _scaleDownCoef; // dynamic scalability coeficients
	float _frameRateSettings;
	float _qualitySettings;

	mutable UITime _lastScaleBetterTime; // avoid oscilation
	mutable UITime _lastScaleWorseTime;
	mutable float _maxTargetFrameDuration;
	mutable float _minTargetFrameDuration;

	mutable float _minLodInvWidth; // visual quality limits
	mutable float _maxLodInvWidth; // 

	enum {NStoreComplexities = 4};

	mutable int _lastComplexity[NStoreComplexities]; // complexity history


	mutable SortObjectList _drawObjects;
	mutable SortObjectList _drawMergers;

	mutable ShadowCache _shadowCache;
	SRef<Landscape> _landscape; // only pointer to landscape

	bool _objectShadows, _vehicleShadows, _cloudlets;
	float _preferredTerrainGrid;
	float _preferredViewDistance;

	public:

	Scene();
	void Init( Engine *engine, Landscape *landscape );
	void ResetFog();
	void CleanUp();
	~Scene();

	bool GetObjectShadows() const {return _objectShadows;}
	bool GetVehicleShadows() const {return _vehicleShadows;}
	bool GetCloudlets() const {return _cloudlets;}

	float GetMinimalTerrainGrid() const;
	float GetPreferredTerrainGrid() const {return _preferredTerrainGrid;}
	float GetPreferredViewDistance() const {return _preferredViewDistance;}

	void SetPreferredTerrainGrid(float x);
	void SetPreferredViewDistance(float x);

	void SetObjectShadows(bool set = true);
	void SetVehicleShadows(bool set = true);
	void SetCloudlets(bool set = true);

	Camera *GetCamera() {return _camera;}
	const Camera *GetCamera() const {return _camera;}
	void SetCamera( const Camera &camera );

	void SetConstantColor( ColorVal color ) {_constantColor=color;}
	ColorVal GetConstantColor() const {return _constantColor;}

	void SetConstantFog( float fog ) {_constantFog=fog;}
	float GetConstantFog() const {return _constantFog;}

	const Matrix4 &ScaledInvTransform() const;
	const Matrix3 &CamNormalTrans() const;
	const Matrix4 &CamInvTrans() const;

	Texture *SkyTexture() const {return _skyTexture;}
	
	void ResetLights();
	void AddLight( Light *light );

	Light *GetLight( int i ) const {return _lights[i];}
	int NLights() const {return _lights.Size();}

	void SelectActiveLights( Object *dimmed );
	void SetActiveLights( const LightList &lights );
	const LightList &ActiveLights() const {return _aLights;}

	//! Select light affecting given position
	const LightList &SelectLights
	(
		Vector3Par pos, float radius, LightList &work
	); // may return work or something else

	//! Select light affecting given object
	const LightList &SelectLights
	(
		Matrix4Par objPos, const Object *object, int level, LightList &work
	);
	
	LightSun *MainLight() const {return _mainLight;}
	void SetMainLight( LightSun *light ) {_mainLight=light;}
	void MainLightChanged(); // fog/light color has been changed

	void SetTacticalVisibility( float tacVis, float rainRange );
	float GetTacticalVisibility() const {return _tacticalVisibility;}

	float GetLodInvWidth() const {return _lodInvWidth;}
	float GetSmokeGeneralization() const;

	float GetFrameRateSettings() const {return _frameRateSettings;}
	void SetFrameRateSettings(float val);
	RString GetFrameRateText() const;

	float GetQualitySettings() const {return _qualitySettings;}
	void SetQualitySettings(float val);
	RString GetQualityText() const;

	void LoadConfig();
	void SaveConfig() const;

	float GetFogMaxRange() const {return _fogMaxRange;}
	float GetFogMinRange() const {return _fogMinRange;}
	float GetShadowFogMaxRange() const {return _shadowFogMaxRange;}
	float GetShadowFogMinRange() const {return _shadowFogMinRange;}

	int TacticalFog8( float distSquare ) const {return _tacticalFog(distSquare);}
	int Fog8( float distSquare ) const {return _fog(distSquare);}
	int ShadowFog8( float distSquare ) const {return _shadowFog(distSquare);}
	int SkyFog8( float distSquare ) const {return _skyFog(distSquare);}
	
	void CalculateSkyColor( Texture *texture );
	
	Landscape *GetLandscape() const {return _landscape;}
	ShadowCache &GetShadowCache() const {return _shadowCache;}
	//TrashCache &GetTrashCache() const {return _trashCache;}
	
	int LevelFromDistance2
	(
		LODShape *shape, float distance2, float oScale,
		Vector3Par direction, Vector3Par viewDirection
	);
	int LevelShadowFromDistance2
	(
		LODShape *shape, float distance2, float oScale,
		Vector3Par direction, Vector3Par viewDirection
	);


	void AdjustComplexity();

	int AdjustComplexity( SortObjectList &objs );
	int AdjustShadowComplexity( SortObjectList &objs );

	void BeginObjects();
	void ObjectForDrawing( Object *obj, int forceLOD, ClipFlags clip );
	void CloudletForDrawing( Object *obj );
	void ObjectForDrawing( Object *obj );

	void EndObjects(); // sort all objects
	void DrawReflections( const WaterLevel &water );
	void DrawObjectsAndShadowsPass1();
	void DrawObjectsAndShadowsPass2();
	void DrawObjectsAndShadowsPass3(); // last draw cockpits
	void ObjectsDrawn(); // release all temporary information

	// light color and position
	void DrawFlare( ColorVal color, Vector3Par pos, bool secondary=true );
	void DrawFlares();

	void DrawRainLevel
	(
		float alpha, float yDensity, float xOffset, float yOffset, float z
	);
	void DrawRain();
	
	void DrawDiagModel
	(
		Vector3Par pos, LODShapeWithShadow *shape,
		float size=0.1, PackedColor color=PackedWhite
	);
	void DrawCollisionStar( Vector3Par pos, float size=0.1, PackedColor color=PackedWhite );
	//void DrawForceArrow( Vector3Par pos, Vector3Val force );
	void DrawVolumeLight
	(
		LODShapeWithShadow *shape, PackedColor color,
		const Frame &pos, float size
	);

	bool ShadowPos
	(
		Vector3Par pos, Vector3 &aprox, LightSun *light
	);
//	Texture *DefaultTexture() const {return Preloaded(TextureDefault);}
	
	LODShapeWithShadow *ForceArrow() const {return Preloaded(ForceArrowModel);}

	LODShapeWithShadow *Preloaded( PreloadedShape type ) const
	{
		Assert( type<MaxPreloadedShape );
		return _preloaded[type];
	}
	// TODO: move preloadedTextures out of scene
	Texture *Preloaded( PreloadedTexture type ) const
	{
		return GPreloadedTextures.New(type);
	}
	Texture *Preloaded( RStringB name ) const
	{
		return GPreloadedTextures.New(name);
	}

	void DrawExShadow( SortObject *oi ); // exact shadow casting
};

extern Scene *GScene;
#define GLOB_SCENE ( GScene )

#define IS_SHADOW_VEHICLE ( GScene ? GScene->GetVehicleShadows() : true )
#define IS_SHADOW_OBJECT ( GScene ? GScene->GetObjectShadows() : true )

#endif

