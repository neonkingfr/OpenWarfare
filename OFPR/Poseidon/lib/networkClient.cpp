#include "wpch.hpp"
#include "networkImpl.hpp"
#include "global.hpp"
//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include "arcadeTemplate.hpp"
#include "ai.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "chat.hpp"
#include <El/XML/xml.hpp>
#include "strFormat.hpp"

#include <Es/Algorithms/qsort.hpp>

#include "allAIVehicles.hpp"
#include "seaGull.hpp"
#include "detector.hpp"
#include "shots.hpp"

#include "debugTrap.hpp"

#include <El/QStream/QBStream.hpp>
#include "packFiles.hpp"
#include "fileServer.hpp"

#include <El/Common/randomGen.hpp>
#include "gameStateExt.hpp"

#include "progress.hpp"
#include <El/Common/perfLog.hpp>

#include "keyInput.hpp"
#include "dikCodes.h"
#include "uiActions.hpp"

#include "crc.hpp"

//#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
  #include <io.h>
#endif

//! Maximal number of dead bodies simulated by single client
/*  new algorithm for hiding of bodies introduced
#define MAX_LOCAL_BODIES            5
#define MAX_LOCAL_BODIES_SERVER     15
*/

/*!
\patch 1.87 Date 10/17/2002 by Jirka
- Fixed: Too many dead bodies were kept on server in 1.85.
*/

//! Time after that body can disappear (in seconds)
#define OLD_BODY                    10

//! Number of bodies on bot client
#define BODIES_ON_BOT_CLIENT        10

//! Number of bodies on all other clients
#define BODIES_ON_CLIENTS           20

//! enable diagnostic logs of message errors on client
#define LOG_CLIENT_ERRORS       0
//! limit for diagnostic logs of message errors on client
const float LogClientErrorLimit = 0.01f;

/*!
\file
Basic implementation file for network client
*/

#define LOG_SEND_PROCESS 0

const char *GameStateNames[] =
{
  "None",
  "Creating",
  "Create",
  "Login",
  "Edit",
  "Mission Voted",
  "Prepare Side",
  "Prepare Role",
  "Prepare OK",
  "Debriefing",
  "Debriefing OK",
  "Transfer Mission",
  "Load Island",
  "Briefing",
  "Play"
};

#if _ENABLE_DEDICATED_SERVER
bool IsDedicatedServer();
#else
static bool IsDedicatedServer() {return false;}
#endif

//! Single item of respawn queue (list of units waiting for respawn)
struct RespawnQueueItem// : public NetworkSimpleObject
{
/*
  const VehicleType *type;
  TargetSide side;
  int id;
  RString varname;
  AIUnitInfo info;
  OLink<AIGroup> group;
  Vector3 position;
  Time time;
  int player;
*/
  //! person to respawn
  OLink<Person> person;
  //! position where respawn
  Vector3 position;
  //! time when respawn
  Time time;
  //! is it player unit
  bool player;
/*
  NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTRespawn;}
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
*/
};
TypeContainsOLink(RespawnQueueItem)


// Network client itself

/*!
\patch 1.45 Date 3/1/2002 by Jirka
- Fixed: Sometimes client has problems connect to server
\patch 1.89 Date 10/22/2002 by Ondra
- Fixed: Chat sound more quiet.
*/

NetworkClient::NetworkClient(NetworkManager *parent, RString address, RString password, bool botClient)
: NetworkComponent(parent)
{
  Verify(Init(address, password, botClient));
  _state = NGSCreating;
  _serverState = NGSCreating;

  _connectionQuality = CQGood;

  _localPlayerName = Glob.header.playerName;

  _missionFileValid = false;

  _gameMaster = false;
  _admin = false;
  _selectMission = false;
  _voteMission = false;

  _controlsPaused = false;

  //  _rvReceived = 0;
  _soundId = 0; // incremented for each PlaySound

  GetValue(_chatSound, Pars >> "CfgInGameUI" >> "Chat" >> "sound");

  // ensure no content remain
  DeleteDirectoryStructure("tmp", false);

  _clientInfo = new ClientInfoObject();
  
  _hideBodies = 0;
}

extern RString FlashpointCfg;

bool NetworkClient::Init(RString address, RString password, bool botClient)
{
      // load some settings from Flashpoint.cfg
  ParamFile cfg;
  cfg.Parse(FlashpointCfg);

#ifdef _WIN32
  if (IsUseSockets())
    _client = CreateNetClient(cfg);
  else
    _client = CreateDPlayClient();
#else
  _client = CreateNetClient(cfg);
#endif

  if (!_client)
  {
    _connectResult = CRError;
    return false;
  }
  
  _client->SetNetworkParams(cfg);
  
  int port = GetNetworkPort();
  MPVersionInfo versionInfo;
  versionInfo.versionActual = MP_VERSION_ACTUAL;
  versionInfo.versionRequired = MP_VERSION_REQUIRED;
  RString GetModList();
  strncpy(versionInfo.mod, GetModList(), MOD_LENGTH);
  versionInfo.mod[MOD_LENGTH - 1] = 0;
  _connectResult = _client->Init
  (
    address, password, botClient, port, Glob.header.playerName, versionInfo, MAGIC_APP
  );
  if (_connectResult != CROK)
  {
    _client = NULL;
    return false;
  }

  _client->InitVoice();
  
  char buffer[256];
  sprintf(buffer, "Tmp%d", port);
  ServerTmpDir = buffer;

  return true;
}

NetworkClient::~NetworkClient()
{
  RemoveSystemMessages();

  Done();
  DeleteDirectoryStructure("tmp", true);
}

void NetworkClient::Done()
{
  // _dp->CancelAsyncOperation(NULL, DPNCANCEL_ALL_OPERATIONS);
  RemoveUserMessages();

  _soundBuffers.Clear();

  _client = NULL;
}

int CmpBodies
(
  const BodyInfo *info1,
  const BodyInfo *info2
)
{
  float diff = info1->value - info2->value;
  return sign(diff);
}

/*!
\patch 1.56 Date 5/10/2002 by Jirka
- Improved: New algorithm for removing bodies in MP game
\patch 1.85 Date 9/23/2002 by Jirka
- Fixed: Algorithm for removing bodies in MP game improved
*/

void NetworkClient::DisposeBody(Person *body)
{
  DoAssert(body->IsLocal());

  // Add body to queue
  int index = _bodies.Add();
  BodyInfo &bodyInfo = _bodies[index];
  bodyInfo.body = body;
  bodyInfo.hideTime = Glob.time + OLD_BODY;
  bodyInfo.value = 0;

  // Recalculate body values
  for (int i=0; i<_bodies.Size(); i++)
  {
    BodyInfo &bodyInfo = _bodies[i];
    if (!bodyInfo.body)
    {
      _bodies.Delete(i);
      i--;
      continue;
    }
    bodyInfo.value = 0;
    for (int w=0; w<bodyInfo.body->NWeaponSystems(); w++)
    {
      const WeaponType *weapon = bodyInfo.body->GetWeaponSystem(w);
      if (weapon) bodyInfo.value += weapon->_value;
    }
    for (int m=0; m<bodyInfo.body->NMagazines(); m++)
    {
      const Magazine *magazine = bodyInfo.body->GetMagazine(m);
      const MagazineType *type = magazine ? magazine->_type : NULL;
      if (type) bodyInfo.value += type->_value;
    }
    if (bodyInfo.body->GetFlagCarrier()) bodyInfo.value += 1000;
  }

  // Sort values
  QSort(_bodies.Data(), _bodies.Size(), CmpBodies);

  // Remove old and unvaluable bodies
  // const int maxBodies = IsBotClient() ? MAX_LOCAL_BODIES_SERVER : MAX_LOCAL_BODIES;
  for (int i=0; i<_bodies.Size(); i++)
  {
    // if (_bodies.Size() <= maxBodies) break;
    if (_hideBodies <= 0) break;
    
    BodyInfo &bodyInfo = _bodies[i];
    if (bodyInfo.value >= 1000) break;
    if (bodyInfo.hideTime > Glob.time) continue;

    DoAssert(bodyInfo.body->IsLocal());
    bodyInfo.body->HideBody();
    _bodies.Delete(i);
    _hideBodies--;
    i--;
  }
}

/*!
\patch 1.27 Date 10/11/2001 by Jirka
- Fixed: experience after respawn cannot be lower than initial experience.
This prevents AI from killing Team Killers after they respawn.
\patch 1.30 Date 11/06/2001 by Jirka
- Fixed: respawn when unit is inside remote vehicle
*/
void NetworkClient::DoRespawn(RespawnQueueItem &item)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  Person *body = item.person;
  if (!body)
  {
    Fail("Respawn failed - body disappeared");
    return;
  }
  AIUnit *unit = body->Brain();
  if (!unit)
  {
    Fail("Respawn failed - unit disappeared");
    return;
  }
  TargetSide side = body->Vehicle::GetTargetSide();
  int id = -1;
  for (int i=0; i<vehiclesMap.Size(); i++)
    if (vehiclesMap[i] == body)
    {
      id = i;
      break;
    }

  // new body
  Soldier *soldier = new Soldier(const_cast<VehicleType *>(body->GetType()));
  
  // move brain
  soldier->SetBrain(unit);
  body->SetBrain(NULL);
  GLOB_WORLD->RemoveSensor(body);
  unit->SetPerson(soldier);
  unit->SetVehicleIn(NULL);
  
  // other parameters
  soldier->SetTargetSide(side);
  unit->SetLifeState(AIUnit::LSAlive);

  AIUnitInfo &info = body->GetInfo();
  saturateMax(info._experience, info._initExperience);
  soldier->SetInfo(info);
  soldier->SetFace(info._face, info._name);
  soldier->SetGlasses(info._glasses);

  RString var = body->GetVarName();
  if (var.GetLength() > 0)
  {
    soldier->SetVarName(var);
    GWorld->GetGameState()->VarSet(var, GameValueExt(soldier), true);
  }

  // weapons
  soldier->RemoveAllWeapons();
  soldier->RemoveAllMagazines();
  soldier->AddDefaultWeapons();
  switch (side)
  {
    case TWest:
      soldier->AddMagazine("M16");
      soldier->AddWeapon("M16");
      break;
    case TEast:
      soldier->AddMagazine("AK74");
      soldier->AddWeapon("AK74");
      break;
    case TGuerrila:
      soldier->AddMagazine("AK47");
      soldier->AddWeapon("AK47");
      break;
  }

  // add to world
  // TODO: call FindFreePosition
  Matrix4 pos;
  pos.SetOrientation(Matrix3(MRotationY,-H_PI*2*GRandGen.RandomValue()));
  pos.SetPosition(item.position);
  soldier->SetTransform(pos);
  soldier->Init(pos);
  GLOB_WORLD->AddVehicle(soldier);
  if (id >= 0) vehiclesMap[id] = soldier;

  CreateVehicle(soldier, VLTVehicle, var, id);
  AttachPerson(soldier);

  // FIX: try to asure player is valid whenever unit is respawned
  if (item.player)
  {
    SelectPlayer
    (
      _player, soldier, true
    );
  }

  UpdateObject(soldier, NMFGuaranteed);
  UpdateObject(unit, NMFGuaranteed);

  unit->SendAnswer(AI::ReportPosition);

  // single body can disappear
  _hideBodies++;

  // Add body to queue
  DisposeBody(body);
}

/*!
\patch 1.47 Date 3/8/2002 by Ondra
- Improved: Chat message priority increased when mission is not running.
This should help when chatting during mission transfers.
*/

NetMsgFlags NetworkClient::GetChatPriority() const
{
  if (GetServerState()==NGSPlay) return NMFNone;
  return NMFHighPriority;
}

void NetworkClient::SetParams(float param1, float param2)
{
  _param1 = param1;
  _param2 = param2;
  
  MissionParamsMessage msg;
  msg._param1 = param1;
  msg._param2 = param2;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::OnSimulate()
{
  // raw statistics
#if _ENABLE_CHEATS
  // remove statistics info
  static AutoArray<int> texts;
  for (int i=0; i<texts.Size(); i++)
  {
    if (texts[i] >= 0) GEngine->RemoveText(texts[i]);
  }
  texts.Resize(0);

  if (outputDiags == 4)
  {
    DoAssert(_rawStatistics.Size() <= 1);
    if (_rawStatistics.Size() > 0)
    {
      char output[1024];
      sprintf
      (
        output, "Server: sent %d (%d), received %d (%d)",
        _rawStatistics[0].sizeSent, _rawStatistics[0].msgSent,
        _rawStatistics[0].sizeReceived, _rawStatistics[0].msgReceived
      );
      texts.Add(GEngine->ShowTextF(1000, 10, 15, output));
    }
  }
#endif

  _connectionQuality = CQGood;

#if _ENABLE_CHEATS
  if (GInput.GetCheat2ToDo(DIK_COMMA))
  {
    outputLogs = !outputLogs;
    if (outputLogs)
      GEngine->ShowMessage(500, "message logs on");
    else
      GEngine->ShowMessage(500, "message logs off");
  }
  if (GInput.GetCheat2ToDo(DIK_SEMICOLON))
  {
    outputDiags++;
    if (outputDiags > nOutputDiags) outputDiags = 0;
  }
  if (GInput.GetCheat2ToDo(DIK_APOSTROPHE))
  {
    _statistics.Clear();
    if (_parent->GetServer()) _parent->GetServer()->GetStatistics().Clear();
  }
#endif

  if (_remoteObjects.Size() > 0 && !_parent->IsServer())
  {
    float age = _client->GetLastMsgAge();

    if (age > 10) 
    {
      if (_connectionQuality < CQBad) _connectionQuality = CQBad;

      float ageReported = _client->GetLastMsgAgeReported();
      if (ageReported >=5)
      {
        char message[256];
//        sprintf(message, "No message received for %.0f seconds", age);
        sprintf(message, LocalizeString(IDS_MP_NO_MESSAGE), age);
        GChatList.Add(CCGlobal, NULL, message, false, true);
        _client->LastMsgAgeReported();
      }
    }
    else if (age > 5)
    {
      if (_connectionQuality < CQPoor) _connectionQuality = CQPoor;
    }
  }

  _controlsPaused = _client->GetLastMsgAgeReliable() > 10.0f;

  // receive all system and user messages
  ReceiveSystemMessages();
  ReceiveLocalMessages();
  ReceiveUserMessages();

  // statistics
  #if _ENABLE_MP
  if (DiagLevel >= 3)
  {
    int nMsg, nBytes, nMsgG, nBytesG;
    _client->GetSendQueueInfo(nMsg, nBytes, nMsgG, nBytesG);
        if (nMsg > 0 || nMsgG > 0)
        {
            if ( nMsgG < 0 )    // old style
          DiagLogF("Client: pending in SendQueue: %d messages, %d bytes", nMsg, nBytes);
            else                // new style
          DiagLogF("Client: pending in SendQueue: common - %d messages, %d bytes, guaranteed - %d messages, %d bytes",
                         nMsg, nBytes, nMsgG, nBytesG);
        }
  }
  #endif

  // cancel update messages
/*
  int canceled = 0;
  for (int i=0; i<_localObjects.Size(); i++)
  {
    NetworkLocalObjectInfo &info = _localObjects[i];
    if (info.lastCreatedMsgId != 0xFFFFFFFF && info.canCancel)
    {
      HRESULT hr = _dp->CancelAsyncOperation(info.lastCreatedMsgId, 0);
      if (DiagLevel > 0 && FAILED(hr))
        LogF("Client: cannot cancel message %x, error %x", info.lastCreatedMsgId, hr);
      canceled++;
    }
  }

  // statistics
  if (DiagLevel >= 2)
  {
    if (canceled > 0) DiagLogF("Client: canceled %d messages", canceled);
  }
*/

  // respawning
  for (int i=0; i<_respawnQueue.Size();)
  {
    RespawnQueueItem &item = _respawnQueue[i];
    if (item.time > Glob.time)
    {
      i++;
      continue;
    }

    DoRespawn(item);
/*
    if (item.group)
    {
      if (item.group->IsLocal())
        DoRespawn(item);
      else
        SendMsg(&item, 0, 0, NMFGuaranteed);
    }
    else
    {
      Fail("No group in respawn");
    }
*/

    _respawnQueue.Delete(i);
  }

  // send update messages
  SendMessages();

  // update speaker buffers
  for (int i=0; i<_soundBuffers.Size(); i++)
  {
    if (_soundBuffers[i].buffer && _soundBuffers[i].object)
    {
      Vector3 pos = _soundBuffers[i].object->GetSpeakerPosition();
      _soundBuffers[i].buffer->SetPosition(pos[0], pos[1], pos[2]);
      bool playing = _client->IsVoicePlaying(_soundBuffers[i].player);
      _soundBuffers[i].object->SetRandomLip(playing);
    }
  }
  if (GWorld->GetRealPlayer())
  {
    GWorld->GetRealPlayer()->SetRandomLip(_client->IsVoiceRecording() && ActualChatChannel() == CCDirect);
  }

/*
  _bodies.Compact();
  while (_bodies.Size() > MAX_LOCAL_BODIES)
  {
    DoAssert(_bodies[0].body);
    DoAssert(_bodies[0].body->IsLocal());
    _bodies[0]->HideBody();
    _bodies.Delete(0);
  }
*/

  // check if sent confirmed for all outgoing messages
  for (int o=0; o<_localObjects.Size(); o++)
  {
    NetworkLocalObjectInfo &oInfo = _localObjects[o];
    for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
    {
      NetworkUpdateInfo &info = oInfo.updates[j];
      if (info.lastCreatedMsg && ::GlobalTickCount() > info.lastCreatedMsgTime + 5000)
      {
        RptF("Client: Network message %x (update info %x) is pending", info.lastCreatedMsgId, &info);
        info.lastCreatedMsg = NULL;
        info.lastCreatedMsgId = 0xFFFFFFFF;
        info.lastCreatedMsgTime = 0;
      }
    }
  }

  // log all diagnostics
  WriteDiagOutput(false);
}

void OnClientUserMessage(char *buffer, int bufferSize, void *context)
{
  NetworkClient *client = (NetworkClient *)context;

  bufferSize -= sizeof(int);
  int crc = *(int *)(buffer + bufferSize);
  static CRCCalculator calculator;
  if (calculator.CRC(buffer, bufferSize) == crc)
  {
    NetworkMessageRaw rawMsg(buffer, bufferSize);
    client->DecodeMessage(TO_SERVER, rawMsg);
  }
  else
  {
    Fail("Bad CRC for incoming message");
  }
}

void OnClientSendComplete(DWORD msgID, bool ok, void *context)
{
  NetworkClient *client = (NetworkClient *)context;
  client->OnSendComplete(msgID, ok);
}

/*!
\patch 1.50 Date 4/4/2002 by Jirka
- Fixed: When session was lost, client doesn't fully escape from game to basic multiplayer screen
*/
void NetworkClient::ReceiveSystemMessages()
{
  if (!_client) return;
  if (_client->IsSessionTerminated())
  {
    _state = NGSNone;
    _serverState = NGSNone;
//    GChatList.Add(CCGlobal, NULL, "Session lost", false, true);
    NetTerminationReason reason = _client->GetWhySessionTerminated();
    RString format = LocalizeString(IDS_MP_SESSION_LOST);
    RString message = format;
    RString name = GetLocalPlayerName();
    const PlayerIdentity *id = FindIdentity(_player);
    if (id) name = id->name;
    switch (reason)
    {
      case NTRTimeout: format = LocalizeString(IDS_MP_TIMEOUT); break;
      case NTRKicked: format = LocalizeString(IDS_MP_KICKED); break;
      case NTRBanned: format = LocalizeString(IDS_MP_BANNED); break;
      case NTRDisconnected: format = LocalizeString(IDS_MP_DISCONNECT); break;
    }
    message = Format(format,(const char *)name);
    GChatList.Add(CCGlobal, NULL, message, false, true);
    return;
  }
  _client->ProcessSendComplete(OnClientSendComplete, this);
}

void NetworkClient::ReceiveUserMessages()
{
  if (_client) _client->ProcessUserMessages(OnClientUserMessage, this);
}

void NetworkClient::RemoveSystemMessages()
{
  if (_client) _client->RemoveSendComplete();
}

void NetworkClient::RemoveUserMessages()
{
  if (_client) _client->RemoveUserMessages();
}

void NetworkClient::OnSendComplete(DWORD msgID, bool ok)
{
#if CHECK_MSG
  CheckLocalObjects();
#endif

#if LOG_SEND_PROCESS
  LogF("Client: Send %x completed: %s", msgID, ok ? "ok" : "failed");
#endif
  for (int i=0; i<_localObjects.Size(); i++)
  {
    NetworkLocalObjectInfo &localObject = _localObjects[i];
    for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
    {
      // NetworkLocalObjectInfo &info = _localObjects[i];
      NetworkUpdateInfo &info = localObject.updates[j];
      if (info.lastCreatedMsgId != msgID)
      {
        continue;
      }
      if (!info.lastCreatedMsg)
      {
        Fail("Message ID without message");
        info.lastCreatedMsgId = 0xFFFFFFFF;
        info.lastCreatedMsgTime = 0;
        continue;
      }
      if (ok)
      {
        ADD_COUNTER(netCN, 1);
        ADD_COUNTER(netCS, info.lastCreatedMsg->size);
        // message sent
        info.lastSentMsg = info.lastCreatedMsg;

#if CHECK_MSG
NetworkObject *object = localObject.object;
if (object)
{
NetworkDataTyped<int> *creator = static_cast<NetworkDataTyped<int> *>(info.lastSentMsg->values[0].GetRef());
NetworkDataTyped<int> *id = static_cast<NetworkDataTyped<int> *>(info.lastSentMsg->values[1].GetRef());
LogF
(
"Message %x (%d:%d) assigned to object %x (%d:%d)",
info.lastSentMsg.GetRef, creator->value, id->value,
object, object->GetNetworkId().creator, object->GetNetworkId().id
);
}
#endif

      }
      else
      {
        ADD_COUNTER(netFN, 1);
        ADD_COUNTER(netFS, info.lastCreatedMsg->size);
        if (DiagLevel >= 4)
        {
          DiagLogF("Client: sent of message %x failed", msgID);
        }
      }
      info.lastCreatedMsg = NULL;
      info.lastCreatedMsgId = 0xFFFFFFFF;
      info.lastCreatedMsgTime = 0;
#if LOG_SEND_PROCESS
      LogF("  - update info %x updated", &info);
#endif
    }
  }

#if CHECK_MSG
  CheckLocalObjects();
#endif
}

unsigned NetworkClient::CleanUpMemory()
{
  if (_client) return _client->FreeMemory();
  return 0;
}

bool NetworkClient::CheckLocalObjects() const
{
  for (int i=0; i<_localObjects.Size(); i++)
  {
    const NetworkLocalObjectInfo &localObject = _localObjects[i];
    NetworkObject *object = localObject.object;
    if (object)
    {
      for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
      {
        const NetworkUpdateInfo &info = localObject.updates[j];
        if (info.lastCreatedMsg)
        {
          CHECK_ASSIGN(creator,info.lastCreatedMsg->values[0],const RefNetworkDataTyped<int>);
          CHECK_ASSIGN(id,info.lastCreatedMsg->values[1],const RefNetworkDataTyped<int>);
          if (creator.GetVal() != object->GetNetworkId().creator || id.GetVal() != object->GetNetworkId().id)
          {
            Fail("lastCreatedMsg is bad");
            return false;
          }
        }
        else if (info.lastSentMsg)
        {
          CHECK_ASSIGN(creator,info.lastCreatedMsg->values[0],const RefNetworkDataTyped<int>);
          CHECK_ASSIGN(id,info.lastCreatedMsg->values[1],const RefNetworkDataTyped<int>);
          if (creator.GetVal() != object->GetNetworkId().creator || id.GetVal() != object->GetNetworkId().id)
          {
            Fail("lastSentMsg is bad");
            return false;
          }
        }
      }
    }
  }
  return true;
}

//! Check if given unit is in list of units
/*!
\param soldier unit to find
\param units checked array of units
*/
static bool FindUnit(NetworkObject *soldier, RefArray<NetworkObject> &units)
{
  if (!soldier) return false;
  for (int i=0; i<units.Size(); i++)
  {
    if (units[i] == soldier) return true;
  }
  return false;
}

//! Find output radio channel for given unit and chat channel
/*!
\param unit unit
\param channel chat channel
*/
RadioChannel *FindChannel(AIUnit *unit, int channel)
{
  switch (channel)
  {
  case CCVehicle:
    {
      Transport *veh = unit->GetVehicleIn();
      return veh ? &veh->GetRadio() : NULL;
    }
  case CCGroup:
    {
      AIGroup *grp = unit->GetGroup();
      return grp ? &grp->GetRadio() : NULL;
    }
  case CCSide:
    {
      AIGroup *grp = unit->GetGroup();
      if (!grp) return NULL;
      AICenter *center = grp->GetCenter();
      return center ? &center->GetRadio() : NULL;
    }
  case CCGlobal:
  case CCDirect:
    return &GWorld->GetRadio();
  default:
    RptF("Client: Bad radio channel %d", channel);
    return NULL;
  }
}

//! Check whole AI structute for consistency
static bool AssertAIValid()
{
  for (int side=0; side<TSideUnknown; side++)
  {
    AICenter *center = GWorld->GetCenter((TargetSide)side);
    if (!center) continue;
    if (!center->AssertValid()) return false;
  }
  return true;
}

/*!
\patch 1.78 Date 7/22/2002 by Jirka
- Fixed: Better usage of already transferred multiplayer missions (cache added)
*/
bool CheckMissionFile(RString fileName, MissionHeader &header)
{
  RString fileNameExt = fileName + RString(".pbo");

#ifdef _WIN32

#if 1
  HANDLE handle = ::CreateFile
  (
    fileNameExt, GENERIC_READ, FILE_SHARE_READ,
    NULL,OPEN_EXISTING, 0, NULL
  );
  if (handle == INVALID_HANDLE_VALUE) return false;

  BY_HANDLE_FILE_INFORMATION info;
  ::GetFileInformationByHandle(handle, &info);
  ::CloseHandle(handle);
#else
  if (!QIFStream::FileExists(fileNameExt)) return false;

  WIN32_FILE_ATTRIBUTE_DATA info;
  ::GetFileAttributesEx(fileNameExt, GetFileExInfoStandard, &info);
#endif

  if
  (
    header.fileSizeL != info.nFileSizeLow ||
    header.fileSizeH != info.nFileSizeHigh
  ) return false; 
/*
  if
  (
    header.fileTimeL != info.ftLastWriteTime.dwLowDateTime ||
    header.fileTimeH != info.ftLastWriteTime.dwHighDateTime;
  ) return false; 
*/

#else   // !defined _WIN32

  LocalPath(fn,fileNameExt);
  struct stat st;
  if ( stat(fn,&st) ) return false;
#if 1       // (sizeof(off_t) <= 4)
  if ( header.fileSizeH ||
       st.st_size != header.fileSizeL )
    return false;
#else
  if ( st.st_size != header.fileSizeL + (((off_t)header.fileSizeH) << 32) )
    return false;
#endif

#endif

  QIFStream f;
  f.open(fileNameExt);
  CRCCalculator crc;
  if (crc.CRC(f.act(), f.rest()) != header.fileCRC) return false;

  CreateMPMissionBank(fileName, header.island);
  return true;
}

//! client-side settings:
//! custom face file bigger than given limit is ignored
int MaxCustomFaceSize = 100*1024;
//! custom sound file bigger than given limit is ignored
//int MaxCustomSoundSize = 40*1024;
int MaxCustomSoundSize = 50*1024;
//! server-side setting:
//! client attemping to transfer more than given limit will be kicked
/*!
\patch 1.87 Date 10/17/2002 by Ondra
- New: Server can determine max. size of any single custom file acceptable on the server.
To do this add line MaxCustomFileSize=size_in_bytes into Flashpoint.cfg file.
Users with custom face or custom sound larger than this size are kicked when trying to connect.
*/

int MaxCustomFileSize = 128*1024;

static int FileSize(const char *name)
{
  #ifdef _WIN32
  struct stat st;
  if ( stat(name,&st) ) return INT_MAX;
  #else
  LocalPath(fn,name);
  struct stat st;
  if ( stat(fn,&st) ) return INT_MAX;
  #endif
  if (st.st_size>INT_MAX) return INT_MAX;
  return st.st_size;
}

/*!
\patch 1.01 Date 06/18/2001 by Jirka
- Fixed: bad experience increase in MP
- when killed unit on other machine no experience to player was added
\patch_internal 1.01 Date 06/18/2001 by Jirka
- fixed - added experience change to NMTVehicleDestroyed message
\patch_internal 1.26 Date 10/02/2001 by Ondra
- New: message class for updating dammage.
\patch 1.26 Date 10/02/2001 by Ondra
- Improved: decreased lag when hitting other units.
\patch_internal 1.28 Date 10/29/2001 by Jirka
- Fixed: do not update local objects from network
\patch 1.34 Date 12/03/2001 by Jirka
- Fixed: Join in multiplayer
\patch 1.34 Date 12/04/2001 by Jirka
- Fixed: Waypoint synchronization in multiplayer
\patch 1.34 Date 12/04/2001 by Jirka
- Fixed: Trigger activation through radio in multiplayer
\patch 1.43 Date 1/30/2002 by Jirka
- Fixed: Crashes in MP if player waits in lobby and game is in progress
\patch 1.43 Date 2/4/2002 by Jirka
- Fixed: Sometimes camera was on bad person if two players gets in vehicle simultaneously in MP
\patch 1.47 Date 3/8/2002 by Ondra
- Fixed: Config verification checks have increased priority
to avoid timeouts during mission transfer.
\patch 1.56 Date 5/13/2002 by Ondra
- Fixed: More robust handling of addons missing on client.
\patch 1.80 Date 8/5/2002 by Jirka
- Fixed: MP - when admin logged in during select mission screen, no missions was listed
\patch 1.96 Date 1/24/2004 by Ondra
- Fixed: MP - when mission file transfer failes, client is disconnected.
*/

void NetworkClient::OnMessage(int from, NetworkMessage *msg, NetworkMessageType type)
{
  if (_serverState==NGSNone) return;
  NetworkMessageFormatBase *format = GetFormat(/*from, */type);
  if (!format)
  {
    RptF("Client: Bad message %d(%s) format", (int)type, (const char *)NetworkMessageTypeNames[type]);
    return;
  }
  NetworkMessageContext ctx(msg, format, this, from, MSG_RECEIVE);

  #if _ENABLE_CHEATS
  int level = GetDiagLevel(type,!_parent->GetServer());
  if (level >= 2)
  {
    DiagLogF("Client: received message %s from %d", NetworkMessageTypeNames[type], from);   
    ctx.LogMessage(level, "\t");
  }
  #endif

  bool validBefore = true;
  if (_state == NGSPlay) validBefore = AssertAIValid();

  switch (type)
  {
    case NMTMsgFormat:
      {
        int IndicesMsgFormatGetIndex(const NetworkMessageIndices *ind);

        int index;
        ctx.IdxTransfer(IndicesMsgFormatGetIndex(ctx.GetIndices()), index);
        if (index >= NMTN) break; // unknown message
        int pos = index - NMTFirstVariant;
        _formats.Access(pos);
        _formats[pos].TransferMsg(ctx);
        _formats[pos].Init(GMsgFormats[index]->GetIndices()->Clone());
      }
      break;
    case NMTIntegrityQuestion:
      {
        IntegrityQuestionMessage question;
        question.TransferMsg(ctx);

        IntegrityAnswerMessage answer;
        answer.id = question.id;
        answer.type = question.type;
        answer.answer = IntegrityCheckAnswer(question.type,question.q);
        SendMsg(&answer, NMFGuaranteed|NMFHighPriority);
      }
      break;
    case NMTPlayerState:
      {
        PlayerStateMessage state;
        state.TransferMsg(ctx);

        for (int i=0; i<_identities.Size(); i++)
        {
          PlayerIdentity &identity = _identities[i];
          if (identity.dpnid == state.player)
          {
            identity.state = state.state;
            break;
          }
        }
      }
      break;
    case NMTNetworkCommand:
      {
        NetworkCommandMessage cmd;
        cmd.TransferMsg(ctx);
        switch (cmd.type)
        {
        case NCMTLogged:
          {
            _gameMaster = true;
            cmd.content.Read(&_admin, sizeof(_admin));
/*
            _serverMissions.Clear();
            RString mission = cmd.content.ReadString();
            while (mission.GetLength() > 0)
            {
              _serverMissions.Add(mission);
              mission = cmd.content.ReadString();
            }
*/
            GChatList.Add(CCGlobal, NULL, LocalizeString(IDS_MP_LOGGED), false, true);
          }
          break;
        case NCMTLoggedOut:
          {
            _gameMaster = false;
            _selectMission = false;
            // _serverMissions.Clear();
            GChatList.Add(CCGlobal, NULL, LocalizeString(IDS_MP_LOGGED_OUT), false, true);
          }
          break;
        case NCMTVoteMission:
          {
            _serverMissions.Clear();
            RString mission = cmd.content.ReadString();
            while (mission.GetLength() > 0)
            {
              _serverMissions.Add(mission);
              mission = cmd.content.ReadString();
            }
            if (_gameMaster) _selectMission = true;
            _voteMission = true;
          }
          break;
        case NCMTMonitorAnswer:
          {
            float fps = 0;
            int memory = 0;
            float in = 0;
            float out = 0;
            cmd.content.Read(&fps, sizeof(fps));
            cmd.content.Read(&memory, sizeof(memory));
            cmd.content.Read(&in, sizeof(in));
            cmd.content.Read(&out, sizeof(out));
          
            int sizeNG = 0, sizeG = 0;
            cmd.content.Read(&sizeG, sizeof(sizeG));
            cmd.content.Read(&sizeNG, sizeof(sizeNG));
            
            char buffer[256];
            sprintf
            (
              buffer, LocalizeString(IDS_SERVER_MONITOR),
              fps, 1e-6 * memory, 8e-3 * out, 8e-3 * in,
              sizeNG, sizeG
            );
            GChatList.Add(CCGlobal, NULL, buffer, false, true);
          }
          break;
        case NCMTDebugAnswer:
          LogDebugger(cmd.content.ReadString());
          break;
        case NCMTMissionTimeElapsed:
          {
            int timeElapsed = 0;
            cmd.content.Read(&timeElapsed, sizeof(timeElapsed));
            _missionHeader.start = GlobalTickCount() - timeElapsed;
          }
          break;
        }
      }
      break;
    case NMTPlayer:
      {
        PlayerMessage playerMsg;
        playerMsg.TransferMsg(ctx);
        _player = playerMsg.player;
//{ DEDICATED SERVER SUPPORT
        if (playerMsg.server)
        {
          break; // do not create identity for server
        }
//}

        // send identity
        PlayerIdentity identity;
        identity.dpnid = _player;
        identity.version = MP_VERSION_ACTUAL;
        RString GetPublicKey();
        identity.id = GetPublicKey();
        identity.name = playerMsg.name; // name is changed by server
        identity.face = Glob.header.playerFace;
        identity.glasses = Glob.header.playerGlasses;
        identity.speaker = Glob.header.playerSpeaker;
        identity.pitch = Glob.header.playerPitch;
        identity._minPing = 10;
        identity._avgPing = 100;
        identity._maxPing = 1000;
        identity._minBandwidth = 14;
        identity._avgBandwidth = 28;
        identity._maxBandwidth = 33;
        RString GetUserParams();
        RString filename = GetUserParams();
        if (QIFStream::FileExists(filename))
        {
          ParamFile cfg;
          cfg.Parse(filename);
          const ParamEntry *entry = cfg.FindEntry("Identity");
          if (entry)
          {
            if (entry->FindEntry("squad"))
              identity.squadId = (*entry) >> "squad";
          }
        }

        // send face
        bool transfer = !_parent->IsServer();
        RString GetUserDirectory();
        if (stricmp(identity.face, "custom") == 0)
        {
          RString srcDir = GetUserDirectory();
          RString dstDir = RString("tmp\\players\\") + identity.name + RString("\\");
          CreatePath(dstDir);
          RString serverDir = GetServerTmpDir() + RString("\\players\\") + identity.name + RString("\\");
          if (!transfer) CreatePath(serverDir);
          RString src = srcDir + RString("face.paa");
          if (QIFStream::FileExists(src) && FileSize(src)<=MaxCustomFaceSize)
          {
            RString dst = dstDir + RString("face.paa");
            ::CopyFile(src, dst, FALSE);
            RString server = serverDir + RString("face.paa");
            if (transfer) TransferFileToServer(server, src);
            else ::CopyFile(src, server, FALSE);
          }
          else
          {
            src = srcDir + RString("face.jpg");
            if (QIFStream::FileExists(src) && FileSize(src)<=MaxCustomFaceSize)
            {
              RString dst = dstDir + RString("face.jpg");
              ::CopyFile(src, dst, FALSE);
              RString server = serverDir + RString("face.jpg");
              if (transfer) TransferFileToServer(server, src);
              else ::CopyFile(src, server, FALSE);
            }
          }
        }

        // send sounds
        const AutoArray<RString> &sounds = GWorld->UI()->GetCustomRadio();
        if (sounds.Size() > 0)
        {
          RString srcDir = GetUserDirectory() + RString("sound\\");
          RString dstDir = RString("tmp\\players\\") + identity.name + RString("\\sound\\");
          CreatePath(dstDir);
          RString serverDir = GetServerTmpDir() + RString("\\players\\") + identity.name + RString("\\sound\\");
          if (!transfer) CreatePath(serverDir);
          for (int i=0; i<sounds.Size(); i++)
          {
            RString src = srcDir + sounds[i];
            RString dst = dstDir + sounds[i];
            // check file size
            // do not copy file if it is too large
            if (FileSize(src)<MaxCustomSoundSize)
            {
              ::CopyFile(src, dst, FALSE);
              RString server = serverDir + sounds[i];
              if (transfer) TransferFileToServer(server, src);
              else ::CopyFile(src, server, FALSE);
            }
          }
        }

        SendMsg(&identity, NMFGuaranteed);
      }
      break;
    case NMTLogin:
      {
        int index = _identities.Add();
        PlayerIdentity &identity = _identities[index];
        identity.TransferMsg(ctx);
        // identity.state = NGSLogin;
        // find squad info
        if (identity.squadId.GetLength() > 0)
        {
          for (int i=0; i<_squads.Size(); i++)
            if (_squads[i]->id == identity.squadId)
            {
              identity.squad = _squads[i];
              break;
            }
        }
      }
      break;
    case NMTLogout:
      {
        LogoutMessage logout;
        logout.TransferMsg(ctx);
        for (int i=0; i<_identities.Size(); i++)
        {
          if (_identities[i].dpnid == logout.dpnid)
          {
            _identities.Delete(i);
            break;
          }
        }
      }
      break;
    case NMTSquad:
      {
        int index = _squads.Add(new SquadIdentity());
        SquadIdentity *squad = _squads[index];
        squad->TransferMsg(ctx);
      }
      break;
    case NMTMissionHeader:
      {
        _missionHeader.TransferMsg(ctx);
        if (_missionHeader.updateOnly) break;
        {
          // ADDED - AddOns check
          void CheckPatch(FindArrayRStringCI &addOns, FindArrayRStringCI &missing);
          FindArrayRStringCI missing;
          CheckPatch(_missionHeader.addOns, missing);
          if (missing.Size() > 0)
          {
            RString message = LocalizeString(IDS_MSG_ADDON_MISSING);
            bool first = true;
            for (int i=0; i<missing.Size(); i++)
            {
              if (first) first = false;
              else message = message + RString(", ");
              message = message + missing[i];
            }
            //WarningMessage(message);
            //_client = NULL;
            Disconnect(message);
            return;
          }
          GWorld->ActivateAddons(_missionHeader.addOns);
        }

        Glob.config.easyMode = _missionHeader.cadetMode;
        _playerRoles.Resize(0);
        
        // check if mission file is valid
        RString fullname = _missionHeader.fileDir + _missionHeader.fileName;
        _missionFileValid = CheckMissionFile(fullname, _missionHeader);

        if (!_missionFileValid)
        {
          // check cache
          RString fullname = RString("MPMissionsCache\\") + _missionHeader.fileName;
          _missionFileValid = CheckMissionFile(fullname, _missionHeader);
        }

        AskMissionFileMessage ask(_missionFileValid);
        SendMsg(&ask, NMFGuaranteed);
      }
      break;
    case NMTPlayerRole:
      {
        Assert(dynamic_cast<const IndicesPlayerRole *>(ctx.GetIndices()))
        const IndicesPlayerRole *indices = static_cast<const IndicesPlayerRole *>(ctx.GetIndices());
        
        int index;
        ctx.IdxTransfer(indices->index, index);
        if (index >= _playerRoles.Size())
          _playerRoles.Resize(index + 1);
        _playerRoles[index].TransferMsg(ctx);
      }
      break;
    case NMTSelectPlayer:
      {
        SelectPlayerMessage pl;
        pl.TransferMsg(ctx);
        Assert(pl.player == _player);
        NetworkObject *object = GetObject(pl.person);
        Person *veh = dynamic_cast<Person *>(object);
        if (!veh)
        {
          RptF
          (
            "Client: Player (%d) is not vehicle with brain (%x)",
            (int)pl.player, pl.person.id
          );
          break;
        }
        GWorld->SwitchCameraTo(veh->Brain()->GetVehicle(), CamInternal);
        GWorld->SetPlayerManual(true);
        GWorld->SwitchPlayerTo(veh);
        GWorld->SetRealPlayer(veh);
        if (pl.respawn)
        {
          RString name = "onPlayerResurrect.sqs";
          if (QIFStreamB::FileExist(RString("scripts\\") + name))
          {
            GameArrayType arguments;
            arguments.Add(GameValueExt(veh));
            Script *script = new Script(name, GameValue(arguments));
            GWorld->StartCameraScript(script);
          }
        }
      }
      break;
    case NMTTransferFile:
      {
        TransferFileMessage transfer;
        transfer.TransferMsg(ctx);
        ReceiveFileSegment(transfer);
      }
      break;
    case NMTTransferMissionFile:
      {
        TransferMissionFileMessage transfer;
        transfer.TransferMsg(ctx);

        const char *prefix = "mpmissions\\__cur_mp.";
        RemoveBank(prefix);

        int ret = ReceiveFileSegment(transfer);
        if (ret>0)
        {
// FIX: transfer mission file always into tmp directory (do not rewrite original file)
//          CreateMPMissionBank(_missionHeader.fileDir + _missionHeader.fileName, _missionHeader.island);
          const char *ptr = transfer.path;
          const char *ext = strrchr(ptr, '.');
          DoAssert(ext);
          DoAssert(stricmp(ext, ".pbo") == 0);
          RString path = transfer.path.Substring(0, ext - ptr);
          CreateMPMissionBank(path, _missionHeader.island);

          DoAssert(!_missionFileValid);
          _missionFileValid = true;
          // TODO: this is not bot client
          DoAssert(!_parent->IsServer());
/*
          if (PrepareGame())
          {
            DoAssert(_state == NGSTransferMission);
            _state = NGSLoadIsland;
          }
          else
          {
            _state = NGSPrepareSide;
          }
*/
          if (_state == NGSTransferMission && PrepareGame())
          {
            _state = NGSLoadIsland;
          }
        }
        else if (ret<0)
        {
          RString format = LocalizeString(IDS_MP_VALIDERROR_2);
          char message[512];
          strcpy(message,"");
          const PlayerIdentity *id = FindIdentity(_player);
          if (id)
          {
            sprintf(message, format, (const char *)id->name);
            sprintf(message+strlen(message)," - %s",(const char *)transfer.path);
          }
          Disconnect(message);
        }
      }
      break;
    case NMTAskForDammage:
      {
        AskForDammageMessage ask;
        ask.TransferMsg(ctx);
        if (ask.who)
          ask.who->DoDammage(ask.owner, ask.modelPos, ask.val, ask.valRange, ask.ammo);
      }
      break;
    case NMTAskForSetDammage:
      {
        AskForSetDammageMessage ask;
        ask.TransferMsg(ctx);
        if (ask.who)
          ask.who->SetDammage(ask.dammage);
      }
      break;
    case NMTAskForAddImpulse:
      {
        AskForAddImpulseMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle)
          ask.vehicle->AddImpulse(ask.force, ask.torque);
      }
      break;
    case NMTAskForMoveVector:
      {
        AskForMoveVectorMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle)
          ask.vehicle->Move(ask.pos);
      }
      break;
    case NMTAskForMoveMatrix:
      {
        AskForMoveMatrixMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle)
        {
          Matrix4 trans;
          trans.SetPosition(ask.pos);
          trans.SetOrientation(ask.orient);
          ask.vehicle->Move(trans);
        }
      }
      break;
    case NMTAskForJoinGroup:
      {
        AskForJoinGroupMessage ask;
        ask.TransferMsg(ctx);
        void ProcessJoinGroups(AIGroup *from, AIGroup *to);
        ProcessJoinGroups(ask.group, ask.join);
      }
      break;
    case NMTAskForJoinUnits:
      {
        AskForJoinUnitsMessage ask;
        ask.TransferMsg(ctx);
        void ProcessJoinGroups(OLinkArray<AIUnit> &units, AIGroup *grp);
        ProcessJoinGroups(ask.units, ask.join);
      }
      break;
    case NMTAskForHideBody:
      {
        AskForHideBodyMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle)
        {
          ask.vehicle->HideBody();
        }
      }
      break;
    case NMTExplosionDammageEffects:
      {
        ExplosionDammageEffectsMessage ask;
        ask.TransferMsg(ctx);
        Ref<AmmoType> ammo;
        if (ask.type.GetLength() > 0)
        {
          VehicleNonAIType *type = VehicleTypes.New(ask.type);
          ammo = dynamic_cast<AmmoType *>(type);
        }
        GLandscape->ExplosionDammageEffects
        (
          ask.owner, ask.shot, ask.directHit, ask.pos, ask.dir,
          ammo, ask.enemyDammage
        );
      }
      break;
    case NMTAskForGetIn:
      {
        AskForGetInMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle)
        {
          bool ok = false;
          switch (ask.position)
          {
          case GIPCommander:
            if (ask.vehicle->Commander() && !ask.vehicle->Commander()->IsDammageDestroyed()) break;
            ask.vehicle->GetInCommander(ask.soldier);
            ok = true;
            break;
          case GIPDriver:
            if (ask.vehicle->Driver() && !ask.vehicle->Driver()->IsDammageDestroyed()) break;
            ask.vehicle->GetInDriver(ask.soldier);
            ok = true;
            break;
          case GIPGunner:
            if (ask.vehicle->Gunner() && !ask.vehicle->Gunner()->IsDammageDestroyed()) break;
            ask.vehicle->GetInGunner(ask.soldier);
            ok = true;
            break;
          case GIPCargo:
            {
              for (int i=0; i<ask.vehicle->GetManCargo().Size(); i++)
              {
                if (ask.vehicle->GetManCargo()[i] == NULL || ask.vehicle->GetManCargo()[i]->IsDammageDestroyed())
                {
                  ok = true;
                  break;
                }
              }
            }
            if (ok) ask.vehicle->GetInCargo(ask.soldier);
            break;
          default:
            RptF("Client: Unknown get in position %d", ask.position);
            break;
          }
          if (ok)
          {
            ask.vehicle->GetInFinished(ask.soldier->Brain());
            if (GLOB_WORLD->FocusOn() == ask.soldier->Brain())
              GLOB_WORLD->SwitchCameraTo(ask.vehicle, GLOB_WORLD->GetCameraType());
          }
        }
      }
      break;
    case NMTAskForGetOut:
      {
        AskForGetOutMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle && ask.soldier && ask.soldier->Brain())
        {
          /*
          LogF
          (
            "%s: get out from %s to %s",
            (const char *)ask.soldier->GetDebugName(),
            (const char *)ask.vehicle->GetDebugName(),
            ask.parachute ? "Parachute" : "nothing"
          );
          */
          ask.soldier->Brain()->DoGetOut(ask.vehicle,ask.parachute);
        }
      }
      break;
    case NMTAskForChangePosition:
      {
        AskForChangePositionMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle && ask.soldier)
          ask.vehicle->ChangePosition(ask.type, ask.soldier);
      }
      break;
    case NMTAskForAimWeapon:
      {
        AskForAimWeaponMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle)
        {
          int weapon = ask.vehicle->SelectedWeapon();
          if (weapon < 0) weapon = ask.weapon;
          ask.vehicle->AimWeapon(weapon, ask.dir);
        }
      }
      break;
    case NMTAskForAimObserver:
      {
        AskForAimObserverMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle)
          ask.vehicle->AimObserver(ask.dir);
      }
      break;
    case NMTAskForSelectWeapon:
      {
        AskForSelectWeaponMessage ask;
        ask.TransferMsg(ctx);
        if (ask.vehicle)
          ask.vehicle->SelectWeapon(ask.weapon);
      }
      break;
    case NMTAskForAmmo:
      {
        AskForAmmoMessage ask;
        ask.TransferMsg(ctx);
        if (!ask.vehicle) break;
        Magazine *state = ask.vehicle->GetMagazineSlot(ask.weapon)._magazine;
        if (!state) break;
        saturateMin(ask.burst, state->_ammo);
        state->_ammo -= ask.burst;
        if (state->_ammo<0) state->_ammo = 0;
      }
      break;
    case NMTFireWeapon:
      {
        FireWeaponMessage fire;
        fire.TransferMsg(ctx);
        EntityAI *veh = fire.vehicle;
        if (veh)
        {
          const Magazine *magazine = veh->FindMagazine
          (
            fire.magazineCreator, fire.magazineId
          );
          veh->FireWeaponEffects(fire.weapon, magazine, fire.target);
        }
      }
      break;
    case NMTUpdateWeapons:
      {
        UpdateWeaponsMessage update;
        update.TransferMsg(ctx);
      }
      break;
    case NMTAddWeaponCargo:
      {
        AddWeaponCargoMessage update;
        update.TransferMsg(ctx);
        if (update.vehicle)
        {
          Ref<WeaponType> weapon = WeaponTypes.New(update.weapon);
          update.vehicle->AddWeaponCargo(weapon);
        }
      }
      break;
    case NMTRemoveWeaponCargo:
      {
        RemoveWeaponCargoMessage update;
        update.TransferMsg(ctx);
        if (update.vehicle)
        {
          Ref<WeaponType> weapon = WeaponTypes.New(update.weapon);
          update.vehicle->RemoveWeaponCargo(weapon);
        }
      }
      break;
    case NMTAddMagazineCargo:
      {
        AddMagazineCargoMessage update;
        update.TransferMsg(ctx);
        if (update.vehicle && update.magazine)
          update.vehicle->AddMagazineCargo(update.magazine);
      }
      break;
    case NMTRemoveMagazineCargo:
      {
        RemoveMagazineCargoMessage update;
        update.TransferMsg(ctx);
        if (update.vehicle)
        {
          const Magazine *magazine = update.vehicle->FindMagazine(update.creator, update.id);
          if (magazine)
            update.vehicle->RemoveMagazineCargo(const_cast<Magazine *>(magazine));
        }
      }
      break;
    case NMTVehicleInit:
      {
        VehicleInitCmd init;
        init.TransferMsg(ctx);
        GameState *gstate = GWorld->GetGameState();
        gstate->VarSet("this", GameValueExt(init.vehicle), true);
        gstate->Execute(init.init);
      }
      break;
    case NMTVehicleDestroyed:
      {
        VehicleDestroyedMessage info;
        info.TransferMsg(ctx);
        // FIX
        if (info.killed)
        {
          Person *person = dyn_cast<Person>(info.killed);
          if (person && person->GetInfo()._name.GetLength() == 0 && person->IsNetworkPlayer())
          {
            const PlayerIdentity *identity = FindIdentity(person->GetRemotePlayer());
            if (identity) person->GetInfo()._name = identity->name;
          }
        }
        if (info.killer)
        {
          Person *person = dyn_cast<Person>(info.killer);
          if (!person)
          {
            Transport *vehicle = dyn_cast<Transport>(info.killer);
            if (vehicle)
            {
              person = vehicle->Commander();
              if (!person || !person->IsNetworkPlayer())
                person = vehicle->Gunner();
              if (!person || !person->IsNetworkPlayer())
                person = vehicle->Driver();
            }
          }
          if (person && person->GetInfo()._name.GetLength() == 0 && person->IsNetworkPlayer())
          {
            const PlayerIdentity *identity = FindIdentity(person->GetRemotePlayer());
            if (identity) person->GetInfo()._name = identity->name;
          }
        }

        GStats.OnVehicleDestroyed(info.killed, info.killer);
        // FIX - add experience
        if (info.killer && info.killed)
        {
          // use Entity member to get original target side
          // all dead bodies are considered civilian
          TargetSide origSide = info.killed->Entity::GetTargetSide();
          const VehicleType &type = *info.killed->GetType();

          // increase killer's experience
          AIUnit *kBrain = info.killer->CommanderUnit();
          if (kBrain && kBrain->IsLocal())
          {
            kBrain->IncreaseExperience(type, origSide);
            // send radio message
            AIGroup *killerGroup = kBrain->GetGroup();
            if (killerGroup && killerGroup->GetCenter()->IsEnemy(origSide))
            {
              // find corresponding target
              Target *tar = killerGroup->FindTargetAll(info.killed);
              if (tar)
              {
                // mark killer
                // when destroyed will be set, it will be marked for reporting
                tar->idKiller = info.killer;
              }
            }
          }
          if
          (
            info.killer->GunnerUnit() &&
            info.killer->GunnerUnit()->IsLocal() &&
            info.killer->GunnerUnit() != kBrain
          )
            info.killer->GunnerUnit()->IncreaseExperience(type, origSide);
          if
          (
            info.killer->PilotUnit() &&
            info.killer->PilotUnit()->IsLocal() &&
            info.killer->PilotUnit() != kBrain
          )
            info.killer->PilotUnit()->IncreaseExperience(type, origSide);
        }
      }
      break;
    case NMTVehicleDamaged:
      {
#if _ENABLE_VBS
        VehicleDamagedMessage info;
        info.TransferMsg(ctx);
        if (info._damaged) GStats.OnVehicleDamaged(info._damaged, info._killer, info._damage, info._ammo);
#endif
      }
      break;
    case NMTIncomingMissile:
      {
        IncomingMissileMessage info;
        info.TransferMsg(ctx);
        if (info._target) info._target->OnEvent(EEIncomingMissile, info._ammo, info._owner);
      }
      break;
    case NMTMarkerCreate:
      {
        Person *soldier = GWorld->GetRealPlayer();
        if (!soldier) break;
        MarkerCreateMessage marker;
        marker.TransferMsg(ctx);
        if (marker.channel != CCGlobal && !FindUnit(GWorld->GetRealPlayer(), marker.units)) break;
        RString name = marker.marker.name;
        int index = -1;
        for (int i=0; i<markersMap.Size(); i++)
        {
          if (stricmp(markersMap[i].name, name) == 0)
          {
            index = i;
            break;
          }
        }
        if (index < 0) index = markersMap.Add();
        markersMap[index] = marker.marker;
      }
      break;
    case NMTMarkerDelete:
      {
        MarkerDeleteMessage info;
        info.TransferMsg(ctx);
        for (int i=0; i<markersMap.Size(); i++)
        {
          ArcadeMarkerInfo &marker = markersMap[i];
          if (marker.name == info.name)
          {
            markersMap.Delete(i);
            break;
          }
        }
      }
      break;
/*
    case NMTRespawn:
      {
        RespawnQueueItem item;
        item.TransferMsg(ctx);
        DoRespawn(item);
      }
      break;
*/
    case NMTSetFlagOwner:
      {
        SetFlagOwnerMessage ask;
        ask.TransferMsg(ctx);
        if (!ask.carrier) break;
        ask.carrier->SetFlagOwner(ask.owner);
      }
      break;
    case NMTSetFlagCarrier:
      {
        SetFlagCarrierMessage ask;
        ask.TransferMsg(ctx);
        if (!ask.owner) break;
        ask.owner->SetFlagCarrier(ask.carrier);
      }
      break;
    case NMTMsgVTarget:
      {
        RadioMessageVTarget radioMsg;
        radioMsg.TransferMsg(ctx);
        radioMsg.Send();
      }
      break;
    case NMTMsgVFire:
      {
        RadioMessageVFire radioMsg;
        radioMsg.TransferMsg(ctx);
        radioMsg.Send();
      }
      break;
    case NMTMsgVMove:
      {
        RadioMessageVMove radioMsg;
        radioMsg.TransferMsg(ctx);
        radioMsg.Send();
      }
      break;
    case NMTMsgVFormation:
      {
        RadioMessageVFormation radioMsg;
        radioMsg.TransferMsg(ctx);
        radioMsg.Send();
      }
      break;
    case NMTMsgVSimpleCommand:
      {
        RadioMessageVSimpleCommand radioMsg;
        radioMsg.TransferMsg(ctx);
        radioMsg.Send();
      }
      break;
    case NMTMsgVLoad:
      {
        RadioMessageVLoad radioMsg;
        radioMsg.TransferMsg(ctx);
        radioMsg.Send();
      }
      break;
    case NMTMsgVAzimut:
      {
        RadioMessageVAzimut radioMsg;
        radioMsg.TransferMsg(ctx);
        radioMsg.Send();
      }
      break;
    case NMTMsgVStopTurning:
      {
        RadioMessageVStopTurning radioMsg;
        radioMsg.TransferMsg(ctx);
        radioMsg.Send();
      }
      break;
    case NMTMsgVFireFailed:
      {
        RadioMessageVFireFailed radioMsg;
        radioMsg.TransferMsg(ctx);
        radioMsg.Send();
      }
      break;
    case NMTChangeOwner:
      {
        ChangeOwnerMessage co;
        co.TransferMsg(ctx);
        if (co.owner == _player)
        {
          // object becomes local
          NetworkObject *object = NULL;
          for (int i=0; i<_remoteObjects.Size(); i++)
          {
            NetworkRemoteObjectInfo &info = _remoteObjects[i];
            if (info.id == co.object)
            {
              object = info.object;
              _remoteObjects.Delete(i);
              break;
            }
          }
          if (object)
          {
            object->SetLocal(true);
#if CHECK_MSG
  CheckLocalObjects();
#endif
            // add to local objects
            int index = _localObjects.Add();
            NetworkLocalObjectInfo &localObject = _localObjects[index];
            localObject.id = co.object;
            localObject.object = object;
            for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
            {
              NetworkUpdateInfo &info = localObject.updates[j];
              info.lastCreatedMsg = NULL;
              info.lastCreatedMsgId = 0xFFFFFFFF;
              info.lastCreatedMsgTime = 0;
            }
            if (DiagLevel >= 1)
              DiagLogF("Client: object %d:%d is now local", localObject.id.creator, localObject.id.id);
#if CHECK_MSG
  CheckLocalObjects();
#endif
          }
          else
          {
            RptF("Client: Remote object %d:%d not found", co.object.creator, co.object.id);
          }
        }
        else
        {
          // object becomes remote
#if CHECK_MSG
          CheckLocalObjects();
#endif
          NetworkObject *object = NULL;
          for (int i=0; i<_localObjects.Size(); i++)
          {
            NetworkLocalObjectInfo &info = _localObjects[i];
            if (info.id == co.object)
            {
              object = info.object;
              _localObjects.Delete(i);
              break;
            }
          }
#if CHECK_MSG
          CheckLocalObjects();
#endif
          if (object)
          {
            object->SetLocal(false);
            // add to remote objects
            int index = _remoteObjects.Add();
            NetworkRemoteObjectInfo &info = _remoteObjects[index];
            info.id = co.object;
            info.object = object;
            if (DiagLevel >= 1)
              DiagLogF("Client: object %d:%d is now remote", info.id.creator, info.id.id);
          }
          else
          {
            RptF("Client: Local object %d:%d not found", co.object.creator, co.object.id);
          }
        }
      }
      break;
    case NMTPlaySound:
      {
        PlaySoundMessage sound;
        sound.TransferMsg(ctx);

        AbstractWave *wave = GSoundScene->OpenAndPlayOnce
        (
          sound.name,
          sound.position, sound.speed,
          sound.volume, sound.freq
        );
        if (wave)
        {
          GSoundScene->AddSound(wave);

          for (int i=0; i<_receivedSounds.Size();)
          {
            PlaySoundInfo &info = _receivedSounds[i];
            if (info.wave) i++;
            else _receivedSounds.Delete(i);
          }

          int index = _receivedSounds.Add();
          _receivedSounds[index].creator = sound.creator;
          _receivedSounds[index].id = sound.soundId;
          _receivedSounds[index].wave = wave;
        }
      }
      break;
    case NMTSoundState:
      {
        SoundStateMessage sound;
        sound.TransferMsg(ctx);

        for (int i=0; i<_receivedSounds.Size();)
        {
          PlaySoundInfo &info = _receivedSounds[i];
          if (!info.wave)
          {
            _receivedSounds.Delete(i);
            continue;
          }
          if (info.creator == sound.creator && info.id == sound.soundId)
          {
            switch (sound.state)
            {
            case SSRestart:
              info.wave->Restart();
              break;
            case SSStop:
              GSoundScene->DeleteSound(info.wave);
              // remove wave - it may never be started again
              // info.wave is link and should be NULL now
              Assert( !info.wave);
              break;
            case SSRepeat:
              info.wave->Repeat(1000);
              break;
            case SSLastLoop:
              info.wave->LastLoop();
              // remove wave - it may never be started again
              info.wave = NULL;
              break;
            default:
              Fail("Sound state");
              break;
            }
          }
          i++;
        }
      }
      break;
    case NMTPublicVariable:
      {
        PublicVariableMessage varMsg;
        varMsg.TransferMsg(ctx);

        RString name = varMsg._name;
        QIStream in;
        in.init(varMsg._value.Data(), varMsg._value.Size());

        GameState *gstate = GWorld->GetGameState();
        int type;
        in.read(&type, sizeof(int));
        
        if (type == GameScalar)
        {
          GameScalarType value;
          in.read(&value, sizeof(value));
          gstate->VarSet(name, value);
        }
        else if (type == GameBool)
        {
          GameBoolType value;
          in.read(&value, sizeof(value));
          gstate->VarSet(name, value);
        }
        else if (type == GameObject)
        {
          NetworkId id;
          in.read(&id, sizeof(id));
          Object *obj = dynamic_cast<Object *>(GetObject(id));
          GameValueExt value(obj);
          gstate->VarSet(name, value);
        }
        else if (type == GameGroup)
        {
          NetworkId id;
          in.read(&id, sizeof(id));
          AIGroup *grp = dynamic_cast<AIGroup *>(GetObject(id));
          GameValueExt value(grp);
          gstate->VarSet(name, value);
        }
        else
        {
          Fail("Not implemented");
        }
      }
      break;
    case NMTGroupSynchronization:
      {
        GroupSynchronizationMessage sync;
        sync.TransferMsg(ctx);
        if (sync._group)
          synchronized[sync._synchronization].SetActive(sync._group, sync._active);
      }
      break;
    case NMTDetectorActivation:
      {
        DetectorActivationMessage act;
        act.TransferMsg(ctx);
        if (act._detector)
          act._detector->DoActivate();
      }
      break;
    case NMTAskForCreateUnit:
      {
        AskForCreateUnitMessage ask;
        ask.TransferMsg(ctx);
        void CreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank);
        if (ask._group)
          CreateUnit(ask._group, ask._type, ask._position, ask._init, ask._skill, (Rank)ask._rank);
      }
      break;
    case NMTAskForDeleteVehicle:
      {
        AskForDeleteVehicleMessage ask;
        ask.TransferMsg(ctx);
        void DeleteVehicle(Entity *veh);
        if (ask._vehicle) DeleteVehicle(ask._vehicle);
      }
      break;
    case NMTAskForReceiveUnitAnswer:
      {
        AskForReceiveUnitAnswerMessage ask;
        ask.TransferMsg(ctx);
        if (ask._to) ask._to->ReceiveAnswer(ask._from,(AI::Answer)ask._answer);
      }
      break;
    case NMTAskForGroupRespawn:
      {
        AskForGroupRespawnMessage ask;
        ask.TransferMsg(ctx);
/*
RptF
(
  "Group respawn: ask received - person %s, group %s, from %d",
  ask._person ? (const char *)ask._person->GetDebugName() : NULL,
  ask._group ? (const char *)ask._group->GetDebugName() : NULL,
  ask._from
);
*/
        if (ask._person && ask._group && ask._person->Brain() && ask._person->Brain()->GetGroup() == ask._group)
        {
          Person *ProcessGroupRespawn(Person *person, int player);
          Person *respawn = ProcessGroupRespawn(ask._person, ask._from);

          GroupRespawnDoneMessage answer;
          answer._person = ask._person;
          answer._killer = ask._killer;
          answer._respawn = respawn;
          answer._to = ask._from;
/*
RptF
(
  "Group respawn: sending answer - person %s, respawn %s, to %d",
  answer._person ? (const char *)answer._person->GetDebugName() : NULL,
  answer._respawn ? (const char *)answer._respawn->GetDebugName() : NULL,
  answer._to
);
*/
          SendMsg(&answer, NMFGuaranteed);
        }
      }
      break;
    case NMTAskForActivateMine:
      {
        AskForActivateMineMessage ask;
        ask.TransferMsg(ctx);
        if (ask._mine) ask._mine->SetActive(ask._activate);
      }
      break;
    case NMTAskForInflameFire:
      {
        AskForInflameFireMessage ask;
        ask.TransferMsg(ctx);
        if (ask._fireplace) ask._fireplace->Inflame(ask._fire);
      }
      break;
    case NMTAskForAnimationPhase:
      {
        AskForAnimationPhaseMessage ask;
        ask.TransferMsg(ctx);
        if (ask._vehicle) ask._vehicle->SetAnimationPhase(ask._animation, ask._phase);
      }
      break;
    case NMTCopyUnitInfo:
      {
        CopyUnitInfoMessage copy;
        copy.TransferMsg(ctx);
        if (copy._from && copy._to)
        {
          AIUnitInfo &info = copy._from->GetInfo();
          saturateMax(info._experience, info._initExperience);
          copy._to->SetInfo(info);
          copy._to->SetFace(info._face, info._name);
          copy._to->SetGlasses(info._glasses);
        }
      }
      break;
    case NMTGroupRespawnDone:
      {
        GroupRespawnDoneMessage answer;
        answer.TransferMsg(ctx);
/*
RptF
(
  "Group respawn: answer received - person %s, respawn %s",
  answer._person ? (const char *)answer._person->GetDebugName() : NULL,
  answer._respawn ? (const char *)answer._respawn->GetDebugName() : NULL
);
*/
        if (answer._person)
        {
          if (answer._respawn)
          {
            void GroupRespawnDone(Person *person, EntityAI *killer, Person *respawn);
            GroupRespawnDone(answer._person, answer._killer, answer._respawn);
          }
          else
          {
            void ProcessSeagullRespawn(Person *person, EntityAI *killer);
            ProcessSeagullRespawn(answer._person, answer._killer);
          }
        }
      }
      break;
    case NMTChat:
      {
        ChatMessage chat;
        chat.TransferMsg(ctx);
        if (chat.sender)
          GChatList.Add((ChatChannel)chat.channel, chat.sender, chat.text, false, true);
        else
          GChatList.Add((ChatChannel)chat.channel, chat.name, chat.text, false, true);
        if (_chatSound.name.GetLength() > 0)
        {
          AbstractWave *wave = GSoundScene->OpenAndPlayOnce2D
          (
            _chatSound.name, _chatSound.vol, _chatSound.freq, false
          );
          if (wave)
          {
            wave->SetKind(WaveMusic); // UI sounds considered music???
            wave->SetSticky(true); // enable chat sounds in the lobby
            GSoundScene->AddSound(wave);
          }
        }
      }
      break;
    case NMTRadioChat:
      {
        RadioChatMessage chat;
        chat.TransferMsg(ctx);
        // if (!chat.sender) break;
        Person *soldier = GWorld->GetRealPlayer();
        if (!soldier) break;
        if (chat.channel != CCGlobal && !FindUnit(GWorld->GetRealPlayer(), chat.units)) break;
        // TODO: playerMsg
        GChatList.Add((ChatChannel)chat.channel, chat.sender, chat.text, false, false);
        if (chat.sender && chat.sentence.Size() > 0)
        {
          AIUnit *unit = soldier->Brain();
          if (!unit) break;
          RadioChannel *channel = FindChannel(unit, chat.channel);
          if (channel) chat.sentence.Say(channel, chat.sender->GetSpeaker());
        }
      }
      break;
    case NMTRadioChatWave:
      {
        RadioChatWaveMessage chat;
        chat.TransferMsg(ctx);
        Person *soldier = GWorld->GetRealPlayer();
        if (!soldier) break;
        if (chat.channel != CCGlobal && !FindUnit(GWorld->GetRealPlayer(), chat.units)) break;
        if (chat.wave.GetLength() > 0)
        {
          AIUnit *unit = soldier->Brain();
          if (!unit) break;
          RadioChannel *channel = FindChannel(unit, chat.channel);
          if (channel)
          {
            RString player;
            if (chat.wave[0] == '#' && chat.sender)
            {
              player = chat.sender->GetPerson()->GetInfo()._name;
            }
            channel->Say(chat.wave, chat.sender, chat.senderName, player, 2.0);
          }
        }
      }
      break;
    case NMTSetSpeaker:
      {
        SetSpeakerMessage message;
        message.TransferMsg(ctx);
        if (message.on)
        {
          int index = -1;
          for (int i=0; i<_soundBuffers.Size(); i++)
          {
            if (_soundBuffers[i].player == message.player)
            {
              _soundBuffers[i].buffer = NULL;
              index = i;
              break;
            }
          }
          if (index < 0)
          {
            index = _soundBuffers.Add();
            _soundBuffers[index].player = message.player;
          }
          _soundBuffers[index].buffer = _client->Create3DSoundBuffer(message.player);
          NetworkObject *networkObject = GetObject(message.object);
          if (networkObject)
          {
            Vector3 pos = networkObject->GetSpeakerPosition();
            if (_soundBuffers[index].buffer)
            {
              _soundBuffers[index].buffer->SetPosition(pos[0], pos[1], pos[2]);
            }
          }
          _soundBuffers[index].object = networkObject;
        }
        else
        {
          for (int i=0; i<_soundBuffers.Size(); i++)
          {
            if (_soundBuffers[i].player == message.player)
            {
              _soundBuffers[i].buffer = NULL;
              _soundBuffers[i].object->SetRandomLip(false);
              _soundBuffers.Delete(i);
              break;
            }
          }
        }
      }
      break;
    case NMTGameState:
      {
        ChangeGameState gs;
        gs.TransferMsg(ctx);
//LogF("Old state %s, received %s", GameStateNames[_state], GameStateNames[gs.gameState]);
        _serverState = gs.gameState;
        
        // Duplicity with NetworkServer::SetGameState [1/31/2002]
        switch (gs.gameState)
        {
          case NGSPrepareRole:
            if (IsDedicatedServer()) _state = NGSPrepareRole;
            break;
          case NGSPrepareOK:
            if (GetMyPlayerRole() || IsDedicatedServer()) _state = NGSPrepareOK;
            break;
          case NGSDebriefing:
          case NGSDebriefingOK:
            if (_state >= NGSDebriefing) _state = gs.gameState;
            break;
          case NGSTransferMission:
            if (GetMyPlayerRole() || IsDedicatedServer()) _state = NGSTransferMission;
            break;
          case NGSLoadIsland:
            if (GetMyPlayerRole() || IsDedicatedServer())
            {
              if (_parent->IsServer())
              {
                _state = NGSLoadIsland;
              }
              else
              {
                if (_missionFileValid && PrepareGame())
                {
                  _state = NGSLoadIsland;
                }
                else if (_state < NGSPrepareSide) _state = NGSPrepareSide;
              }
            }
            break;
          case NGSBriefing:
            if (GetMaxError()>=EMError)
            {
              // some error when loading mission
              RString message = GetMaxErrorMessage();
              if (message.GetLength()<=0) message = LocalizeString(IDS_MSG_ADDON_MISSING);

              const PlayerIdentity *id = FindIdentity(_player);
              if (id)
              {
                RString senderName = id->name;
                GChatList.Add(CCGlobal, senderName, message, false, true);
                RefArray<NetworkObject> dummy;
                GNetworkManager.Chat(CCGlobal,senderName,dummy,message);
              }
              // we might want to disconnect now
            }

            _respawnQueue.Clear();
            if (_state >= NGSLoadIsland) _state = NGSBriefing;
            break;
          case NGSPlay:
            if (_state == NGSBriefing)
            {
              // Register client info as network object
              CreateLocalObject(_clientInfo);

              _state = NGSPlay;

              // number of bodies we want to hide in mission
              if (IsBotClient())
                _hideBodies = -BODIES_ON_BOT_CLIENT;
              else
              {
                int nClients = 0;
                for (int i=0; i<NPlayerRoles(); i++)
                {
                  const PlayerRole *role = GetPlayerRole(i);
                  if (role->player != NO_PLAYER && role->player != AI_PLAYER) nClients++;
                }
                DoAssert(nClients > 0);
                _hideBodies = -(BODIES_ON_CLIENTS / nClients);
                saturateMin(_hideBodies, -1);
              }
              _bodies.Clear();
            }
            // else if (_state < NGSPrepareSide) _state = NGSBriefing;
            _missionHeader.start = GlobalTickCount();
            break;
          default:
            _state = gs.gameState;
            break;
        }
//LogF("New state %s", GameStateNames[_state]);
      }
      break;
    case NMTDeleteObject:
      if (_state < NGSLoadIsland) break; // updates from the last session
      {
        DeleteObjectMessage dom;
        dom.TransferMsg(ctx);
        DestroyRemoteObject(dom.object);
      }
      break;
    case NMTDeleteCommand:
      if (_state < NGSLoadIsland) break; // updates from the last session
      {
        DeleteCommandMessage dc;
        dc.TransferMsg(ctx);
        for (int i=0; i<_remoteObjects.Size(); i++)
        {
          NetworkRemoteObjectInfo &info = _remoteObjects[i];
          if (info.id == dc.object)
          {
            if (dc.subgrp && info.object)
            {
              dc.subgrp->DeleteCommand(dc.index, dynamic_cast<Command *>(info.object.GetLink()));
            }
            if (DiagLevel >= 1)
              DiagLogF("Client: remote command destroyed %d:%d", dc.object.creator, dc.object.id);
            _remoteObjects.Delete(i);
            break;
          }
        }
      }
      break;
    case NMTCreateObject:
      CreateRemoteObject(ctx, (Object *)NULL);
      break;
    case NMTCreateVehicle:
      CreateRemoteObject(ctx, (Vehicle *)NULL);
      break;
    case NMTCreateDetector:
      CreateRemoteObject(ctx, (Detector *)NULL);
      break;
    case NMTCreateShot:
      CreateRemoteObject(ctx, (Shot *)NULL);
      break;
    case NMTCreateExplosion:
      CreateRemoteObject(ctx, (Explosion *)NULL);
      break;
    case NMTCreateCrater:
      CreateRemoteObject(ctx, (Crater *)NULL);
      break;
    case NMTCreateCraterOnVehicle:
      CreateRemoteObject(ctx, (CraterOnVehicle *)NULL);
      break;
    case NMTCreateObjectDestructed:
      CreateRemoteObject(ctx, (ObjectDestructed *)NULL);
      break;
    case NMTCreateAICenter:
      CreateRemoteObject(ctx, (AICenter *)NULL);
      break;
    case NMTCreateAIGroup:
      CreateRemoteObject(ctx, (AIGroup *)NULL);
      break;
    case NMTCreateAISubgroup:
      CreateRemoteObject(ctx, (AISubgroup *)NULL);
      break;
    case NMTCreateAIUnit:
      CreateRemoteObject(ctx, (AIUnit *)NULL);
      break;
    case NMTCreateCommand:
      CreateRemoteObject(ctx, (Command *)NULL);
      break;
    case NMTCreateHelicopter:
      CreateRemoteObject(ctx, (HelicopterAuto *)NULL);
      break;
    case NMTUpdateDammageObject:
    case NMTUpdateDammageVehicleAI:
      ctx.SetClass(NMCUpdateDammage);
      goto Update;
    case NMTUpdatePositionVehicle:
    case NMTUpdatePositionMan:
    case NMTUpdatePositionTank:
    case NMTUpdatePositionCar:
    case NMTUpdatePositionAirplane:
    case NMTUpdatePositionHelicopter:
    case NMTUpdatePositionShip:
    case NMTUpdatePositionSeagull:
    case NMTUpdatePositionMotorcycle:
      ctx.SetClass(NMCUpdatePosition);
      // continue with update
    case NMTUpdateObject:
    case NMTUpdateVehicle:
    case NMTUpdateDetector:
    case NMTUpdateFlag:
    case NMTUpdateShot:
    case NMTUpdateMine:
    case NMTUpdateVehicleAI:
    case NMTUpdateVehicleBrain:
    case NMTUpdateVehicleSupply:
    case NMTUpdateTransport:
    case NMTUpdateMan:
    case NMTUpdateTankOrCar:
    case NMTUpdateTank:
    case NMTUpdateCar:
    case NMTUpdateAirplane:
    case NMTUpdateHelicopter:
    case NMTUpdateParachute:
    case NMTUpdateShip:
    case NMTUpdateSeagull:
    case NMTUpdateAICenter:
    case NMTUpdateAIGroup:
    case NMTUpdateAISubgroup:
    case NMTUpdateAIUnit:
    case NMTUpdateCommand:
    case NMTUpdateMotorcycle:
    case NMTUpdateFireplace:
    Update:
      if (_state < NGSLoadIsland) break; // updates from the last session
      {
        // find IndicesNetworkObject level
        Assert(dynamic_cast<const IndicesNetworkObject *>(ctx.GetIndices()))
        const IndicesNetworkObject *indices = static_cast<const IndicesNetworkObject *>(ctx.GetIndices());

        NetworkId id;
        ctx.IdxTransfer(indices->objectCreator, id.creator);
        ctx.IdxTransfer(indices->objectId, id.id);
        NetworkObject *object = GetObject(id);
        if (!object)
        {
          RptF
          (
            "Client: Object %d:%d (type %s) not found.",
            id.creator, id.id,
            NetworkMessageTypeNames[type]
          );
          break;
        }
        if (object->IsLocal())
        {
          RptF
          (
            "Client: Object (id %d:%d, type %s) is local - update is ignored.",
            id.creator, id.id,
            NetworkMessageTypeNames[type]
          );
          break;
        }
        object->TransferMsg(ctx);
        if (DiagLevel >= 4)
          DiagLogF("Client: object %d:%d updated", id.creator, id.id);
      }
      break;
    case NMTUpdateClientInfo:
      Fail("Unexpected on client");
      break;
    case NMTPlayerUpdate:
    {
      ctx.SetClass(NMCUpdatePosition);
      //Assert(ctx.GetClass()==NMCUpdatePosition);
      Assert(dynamic_cast<const IndicesPlayerUpdate *>(ctx.GetIndices()))
      const IndicesPlayerUpdate *indices = static_cast<const IndicesPlayerUpdate *>(ctx.GetIndices());
      // check which identity is this message affecting
      int dpnid;
      if (ctx.IdxTransfer(indices->dpnid, dpnid)==TMOK)
      {
        // find corresponding identity
        PlayerIdentity *pi = FindIdentity(dpnid);
        if (pi)
        {
          // update identity
          pi->TransferMsg(ctx);
        }
      }
      break;
    }
    case NMTShowTarget:
      {
        ShowTargetMessage show;
        show.TransferMsg(ctx);
        Person *person = show.vehicle;
        AIUnit *unit = person ? person->Brain() : NULL;
        AIGroup *grp = unit ? unit->GetGroup() : NULL;
        if (grp && show.target)
        {
          Target *target = grp->FindTarget(show.target);
          unit->AssignTarget(target);
          GWorld->UI()->ShowTarget();
        }
      }
      break;
    case NMTShowGroupDir:
      {
        ShowGroupDirMessage show;
        show.TransferMsg(ctx);
        Person *person = show.vehicle;
        AIUnit *unit = person ? person->Brain() : NULL;
        AIGroup *grp = unit ? unit->GetGroup() : NULL;
        AISubgroup *subgrp = grp ? grp->MainSubgroup() : NULL;
        if (subgrp)
        {
          subgrp->SetDirection(show.dir);
          GWorld->UI()->ShowGroupDir();
        }
      }
      break;
    case NMTMissionParams:
      {
        MissionParamsMessage params;
        params.TransferMsg(ctx);

        _param1 = params._param1;
        _param2 = params._param2;
      }
      break;
    default:
      RptF("Client: Unhandled user message %d", (int)type);
      break;
  }

  if (_state == NGSPlay && validBefore)
  {
    bool validAfter = AssertAIValid();
    if (!validAfter)
    {
      DiagLogF("Client: error in structure after message %s", NetworkMessageTypeNames[type]);   
      ctx.LogMessage(4, "\t");
    }
  }
}

/*!
\patch_internal 1.80 Date 8/6/2002 by Jirka
- Fixed: Glob.header.filenameReal now always contain original name of mission (even in MP)
*/

bool NetworkClient::PrepareGame()
{
  extern RString MPMissionsDir;
  void SetBaseDirectory(RString dir);
  void SetMission(RString world, RString mission, RString subdir);

  if (GetMyPlayerRole() && _missionHeader.island.GetLength() > 0)
  {
    RString fullname = GetWorldName(_missionHeader.island);
    if (!QIFStreamB::FileExist(fullname))
    {
      // TODO: Warning and error processing
      WarningMessage(LocalizeString(IDS_MSG_NO_WORLD), (const char *)fullname);
      return false;
    }

    SetBaseDirectory("");
//    SetMission(_missionHeader.island, _missionHeader.fileName, _missionHeader.fileDir);
//    SetMission(_missionHeader.island, _missionHeader.fileName, MPMissionsDir);
    SetMission(_missionHeader.island, "__CUR_MP", MPMissionsDir);
    const char *ext = strrchr(_missionHeader.fileName, '.');
    if (ext) Glob.header.filenameReal = _missionHeader.fileName.Substring(0, ext - (const char *)_missionHeader.fileName);
    else Glob.header.filenameReal = _missionHeader.fileName;
    GWorld->SwitchLandscape(fullname);

    CurrentTemplate.Clear();
    if (GWorld->UI()) GWorld->UI()->Init();
    ParseMission(true);

    GStats.ClearMission();
    GWorld->ActivateAddons(CurrentTemplate.addOns);
    GWorld->InitGeneral(CurrentTemplate.intel);
    GWorld->AdjustSubdivision(GModeNetware);
    GWorld->InitClient();
  }
  return true;
}

NetworkId NetworkClient::CreateLocalObject(NetworkObject *object)
{
  Assert(object);

  // set network id
  NetworkId id(_player, _nextId++);
  object->SetNetworkId(id);

  // add to local objects
#if CHECK_MSG
  CheckLocalObjects();
#endif

  int index = _localObjects.Add();
  NetworkLocalObjectInfo &localObject = _localObjects[index];
  localObject.id = id;
  localObject.object = object;
  for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
  {
    NetworkUpdateInfo &info = localObject.updates[j];
    info.lastCreatedMsg = NULL;
    info.lastCreatedMsgId = 0xFFFFFFFF;
    info.lastCreatedMsgTime = 0;
  }

  if (DiagLevel >= 1)
  {
    NetworkMessageType type = object->GetNMType();
    DiagLogF("Client: local object created %d:%d (type %s)", id.creator, id.id, (const char *)NetworkMessageTypeNames[type]);
  }

#if CHECK_MSG
  CheckLocalObjects();
#endif

  return id;
}

bool NetworkClient::CreateObject(NetworkObject *object)
{
  if (!object) return false;
  if (_state < NGSLoadIsland) return false;

  NetworkId id = CreateLocalObject(object);

  // send message to center
  NetworkMessageType msgType = object->GetNMType(NMCCreate);
  NetworkMessageFormatBase *format = GetFormat(/*LOCAL_PLAYER, */msgType);

  // create message
  Ref<NetworkMessage> msg = new NetworkMessage();
  msg->time = Glob.time;
  NetworkMessageContext ctx(msg, format, this, TO_SERVER, MSG_SEND);
  ctx.SetClass(NMCCreate);
  if (object->TransferMsg(ctx) != TMOK) return false;

  // send message
  DWORD dwMsgId = NetworkComponent::SendMsg
  (
    TO_SERVER, msg, msgType, NMFGuaranteed
  );
  return dwMsgId != 0xFFFFFFFF;
}

/*!
\patch_internal 1.30 Date 11/5/2001 by Jirka
- Fixed: More robust transfer of guaranteed update messages
\patch 1.35 Date 12/11/2001 by Jirka
- Fixed: Error in updates on dedicated server
*/

int NetworkClient::UpdateObject(NetworkObject *object, NetworkMessageClass cls, NetMsgFlags dwFlags)
{
  if (!object) return -1;
  if (_state < NGSLoadIsland) return -1;

  // find local objects
  NetworkId id = object->GetNetworkId();
  if (id.IsNull())
  {
    RptF("Client: Nonnetwork object %x.", object);
    return -1;
  }
  if (!object->IsLocal())
  {
    RptF("Update of nonlocal object %d:%d called", id.creator, id.id);
    return -1;
  }
#if CHECK_MSG
  CheckLocalObjects();
#endif
  NetworkLocalObjectInfo *localObject = GetLocalObjectInfo(id);
  if (!localObject) return -1;

  NetworkUpdateInfo &info = localObject->updates[cls];

  // send message to center
  NetworkMessageType msgType = object->GetNMType(cls);
  if (msgType == NMTNone) return -1;
  
  // FIX
#if _ENABLE_DEDICATED_SERVER
  bool IsDedicatedServer();
  if (msgType == NMTUpdateClientInfo && _parent->GetServer() && IsDedicatedServer()) return -1;
#endif

  NetworkMessageFormatBase *format = GetFormat(/*LOCAL_PLAYER, */msgType);

  // create message
  Ref<NetworkMessage> msg = new NetworkMessage();
  // must be Ref - msg is stored
//  msg->id = id;
  msg->time = Glob.time;
//  msg->position = object->GetCurrentPosition();
  NetworkMessageContext ctx(msg, format, this, TO_SERVER, MSG_SEND);
  ctx.SetClass(cls);
  if ((dwFlags & NMFGuaranteed) != 0) ctx.SetInitialUpdate();
  if (object->TransferMsg(ctx) != TMOK) return -1;

  DoAssert(dynamic_cast<const IndicesNetworkObject *>(ctx.GetIndices()))
  const IndicesNetworkObject *indices = static_cast<const IndicesNetworkObject *>(ctx.GetIndices());
  bool guaranteed = (dwFlags & NMFGuaranteed) != 0;
  if (ctx.IdxTransfer(indices->guaranteed, guaranteed) != TMOK)
  {
    Fail("Cannot transfer");
    return -1;
  }

#if CHECK_MSG
  DoAssert(localObject->object == object);
  DoAssert(localObject->id == id);
  if (object)
  {
    NetworkDataTyped<int> *creator = static_cast<NetworkDataTyped<int> *>(msg->values[0].GetRef());
    NetworkDataTyped<int> *id = static_cast<NetworkDataTyped<int> *>(msg->values[1].GetRef());
    LogF
    (
      "Message %x (%d:%d) assigned to object %x (%d:%d)",
      msg.GetRef(), creator->value, id->value,
      object, object->GetNetworkId().creator, object->GetNetworkId().id
    );
  }
#endif

  // send message
  DWORD dwMsgId = NetworkComponent::SendMsg
  (
    TO_SERVER, msg, msgType, dwFlags
  );
  msg->objectUpdateInfo = NULL;

  if (dwMsgId == 0xFFFFFFFF) return -1;
  // update local objects info
  else if (dwMsgId == 0)
  {
    // sent as sync message
    info.lastCreatedMsg = NULL;
    info.lastCreatedMsgId = 0xFFFFFFFF;
    info.lastCreatedMsgTime = 0;
    info.lastSentMsg = msg;
  }
  else
  {
    DoAssert(dwMsgId == 1);
    // !!! Pointer to static structure - must be process before structure changed
    msg->objectUpdateInfo = &info;
    msg->objectServerInfo = NULL;

    info.lastCreatedMsg = msg;
    info.lastCreatedMsgId = MSGID_REPLACE;
    info.lastCreatedMsgTime = ::GlobalTickCount();
    info.canCancel = (dwFlags & NMFGuaranteed) == 0;
#if LOG_SEND_PROCESS
    LogF("Client: Update info %x marked for send", &info);
#endif
  }
#if CHECK_MSG
  CheckLocalObjects();
#endif

  return msg->size;
}

void NetworkClient::UpdateObject(NetworkObject *object, NetMsgFlags dwFlags)
{
  if (!object) return;

  // find local objects
  NetworkId id = object->GetNetworkId();
  if (id.IsNull())
  {
    RptF("Client: Nonnetwork object %x.", object);
    return;
  }
#if CHECK_MSG
  CheckLocalObjects();

  if (id.id == 0x24)
    LogF("Here");
#endif

  NetworkLocalObjectInfo *localObject = GetLocalObjectInfo(id);
  if (!localObject) return;

  for (int cls=NMCUpdateFirst; cls<NMCUpdateN; cls++)
  {
    NetworkUpdateInfo &info = localObject->updates[cls];

    // send message to center
    NetworkMessageType msgType = object->GetNMType((NetworkMessageClass)cls);
    if (msgType == NMTNone) continue;

    NetworkMessageFormatBase *format = GetFormat(/*LOCAL_PLAYER, */msgType);

    // create message
    Ref<NetworkMessage> msg = new NetworkMessage();
    // must be Ref - msg is stored
    msg->time = Glob.time;
    NetworkMessageContext ctx(msg, format, this, TO_SERVER, MSG_SEND);
    ctx.SetClass((NetworkMessageClass)cls);
    if ((dwFlags & NMFGuaranteed) != 0) ctx.SetInitialUpdate();
    if (object->TransferMsg(ctx) != TMOK) return;

    DoAssert(dynamic_cast<const IndicesNetworkObject *>(ctx.GetIndices()))
    const IndicesNetworkObject *indices = static_cast<const IndicesNetworkObject *>(ctx.GetIndices());
    bool guaranteed = (dwFlags & NMFGuaranteed) != 0;
    if (ctx.IdxTransfer(indices->guaranteed, guaranteed) != TMOK)
    {
      Fail("Cannot transfer");
      return;
    }

#if CHECK_MSG
  DoAssert(localObject->object == object);
  DoAssert(localObject->id == id);
  if (object)
  {
    NetworkDataTyped<int> *creator = static_cast<NetworkDataTyped<int> *>(msg->values[0].GetRef());
    NetworkDataTyped<int> *id = static_cast<NetworkDataTyped<int> *>(msg->values[1].GetRef());
    LogF
    (
      "Message %x (%d:%d) assigned to object %x (%d:%d)",
      msg.GetRef(), creator->value, id->value,
      object, object->GetNetworkId().creator, object->GetNetworkId().id
    );
  }
#endif

    // send message
    DWORD dwMsgId = NetworkComponent::SendMsg
    (
      TO_SERVER, msg, msgType, dwFlags
    );
    if (dwMsgId == 0xFFFFFFFF) return;
    // update local objects info
    else if (dwMsgId == 0)
    {
      // sent as sync message
      info.lastCreatedMsg = NULL;
      info.lastCreatedMsgId = 0xFFFFFFFF;
      info.lastCreatedMsgTime = 0;
      info.lastSentMsg = msg;
    }
    else
    {
      DoAssert(dwMsgId == 1);
      // !!! Pointer to static structure - must be process before structure changed
      msg->objectUpdateInfo = &info;
      msg->objectServerInfo = NULL;

      info.lastCreatedMsg = msg;
      info.lastCreatedMsgId = MSGID_REPLACE;
      info.lastCreatedMsgTime = ::GlobalTickCount();
      info.canCancel = (dwFlags & NMFGuaranteed) == 0;
#if LOG_SEND_PROCESS
      LogF("Client: Update info %x marked for send", &info);
#endif
    }
  }
#if CHECK_MSG
  CheckLocalObjects();
#endif

}

Time NetworkClient::GetEstimatedEndTime() const
{
  return _missionHeader.estimatedEndTime;
}

void NetworkClient::DestroyRemoteObject(NetworkId id)
{
  for (int i=0; i<_remoteObjects.Size(); i++)
  {
    NetworkRemoteObjectInfo &info = _remoteObjects[i];
    if (info.id == id)
    {
      if (info.object) info.object->DestroyObject();
      if (DiagLevel >= 1)
        DiagLogF("Client: remote object destroyed %d:%d", id.creator, id.id);
      _remoteObjects.Delete(i);
      break;
    }
  }
}

/*
void NetworkClient::RegisterFormats()
{
  for (int mt=NMTFirstVariant; mt<NMTN; mt++)
  {
    NetworkMessageFormatBase *item = GetFormat(LOCAL_PLAYER, mt);
    SendMsg(item, 0, mt, NMFGuaranteed);
  }
  ChangeGameState gs(NGSCreate);
  SendMsg(&gs, 0, 0, NMFGuaranteed);
}
*/

/*!
\patch_internal 1.31 Date 11/08/2001 by Jirka
- Fixed: player's vehicle and group ownership is updated immediately when player is selected
*/
void NetworkClient::SelectPlayer(int player, Person *person, bool respawn)
{
  SelectPlayerMessage msg(player, person->GetNetworkId(), person->Position(), respawn);
  SendMsg(&msg, NMFGuaranteed);

  if (person->IsLocal())
  {
    AIUnit *unit = person->Brain();
    if (unit)
    {
      Transport *veh = unit->GetVehicleIn();
      if (veh)
      {
        if (veh->IsLocal())
          UpdateObject(veh, NMFGuaranteed);
        else
          RptF("SelectPlayer - local player in remote vehicle");
      }
      AIGroup *grp = unit->GetGroup();
      if (grp)
      {
        if (grp->IsLocal())
          UpdateObject(grp, NMFGuaranteed);
        else
          RptF("SelectPlayer - local player in remote group");
      }
    }
  }
  else
  {
    RptF("SelectPlayer - on remote person");
  }

  if (player == _player)
  {
    GWorld->SwitchCameraTo(person->Brain()->GetVehicle(), CamInternal);
    GWorld->SetPlayerManual(true);
    // old player is now in _playerOn
    GWorld->SwitchPlayerTo(person);
    GWorld->SetRealPlayer(person);
    GWorld->UI()->ResetVehicle(person->Brain()->GetVehicle());

    if (respawn)
    {
      RString name = "onPlayerResurrect.sqs";
      if (QIFStreamB::FileExist(RString("scripts\\") + name))
      {
        GameArrayType arguments;
        arguments.Add(GameValueExt(person));
        Script *script = new Script(name, GameValue(arguments));
        GWorld->StartCameraScript(script);
      }
    }
  }
}

void NetworkClient::AttachPerson(Person *person)
{
  AttachPersonMessage msg(person);
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::PlaySound
(
  RString name, Vector3Par position, Vector3Par speed,
  float volume, float freq, AbstractWave *wave
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  PlaySoundMessage msg;
  msg.name = name;
  msg.position = position;
  msg.speed = speed;
  msg.volume = volume;
  msg.freq = freq;
  msg.soundId = _soundId++;
  msg.creator = _player;
//  int id = _soundId++;
  SendMsg(&msg, NMFNone);

  // compact _sentSounds
  for (int i=0; i<_sentSounds.Size();)
  {
    PlaySoundInfo &info = _sentSounds[i];
    if (info.wave) i++;
    else _sentSounds.Delete(i);
  }

  int index = _sentSounds.Add();
  _sentSounds[index].creator = _player;
  _sentSounds[index].id = msg.soundId;
  _sentSounds[index].wave = wave;
}

void NetworkClient::SoundState
(
  AbstractWave *wave, SoundStateType state
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  int creator = -1, id = -1;
  for (int i=0; i<_sentSounds.Size();)
  {
    PlaySoundInfo &info = _sentSounds[i];
    if (!info.wave)
    {
      _sentSounds.Delete(i);
      continue;
    }
    if (info.wave == wave)
    {
      creator = info.creator;
      id = info.id;
    }
    i++;
  }
  
  if (id < 0) return;

  SoundStateMessage msg;
  msg.state = state;
  msg.creator = creator;
  msg.soundId = id;
  SendMsg(&msg, NMFNone);
}

void NetworkClient::CreateAllObjects()
{
  // pass 2
  for (int i=0; i<GWorld->NVehicles(); i++)
  {
    Vehicle *veh = GWorld->GetVehicle(i);
    if (veh) UpdateObject(veh, NMFGuaranteed);
  }
  for (int i=0; i<GWorld->NAnimals(); i++)
  {
    Vehicle *veh = GWorld->GetAnimal(i);
    if (veh) UpdateObject(veh, NMFGuaranteed);
  }
  for (int i=0; i<GWorld->NBuildings(); i++)
  {
    Vehicle *veh = GWorld->GetBuilding(i);
    if (veh) UpdateObject(veh, NMFGuaranteed);
  }
  for (int i=0; i<GWorld->NCloudlets(); i++)
  {
    Vehicle *veh = GWorld->GetCloudlet(i);
    if (veh) UpdateObject(veh, NMFGuaranteed);
  }
  for (int i=0; i<GWorld->NFastVehicles(); i++)
  {
    Vehicle *veh = GWorld->GetFastVehicle(i);
    if (veh) UpdateObject(veh, NMFGuaranteed);
  }
  for (int i=0; i<GWorld->NOutVehicles(); i++)
  {
    Vehicle *veh = GWorld->GetOutVehicle(i);
    if (veh) UpdateObject(veh, NMFGuaranteed);
  }

    // AI structure
  AICenter *center = GWorld->GetEastCenter();
  if (center) UpdateCenter(center);
  center = GWorld->GetWestCenter();
  if (center) UpdateCenter(center);
  center = GWorld->GetGuerrilaCenter();
  if (center) UpdateCenter(center);
  center = GWorld->GetCivilianCenter();
  if (center) UpdateCenter(center);
  center = GWorld->GetLogicCenter();
  if (center) UpdateCenter(center);

  ChangeGameState gs(NGSBriefing);
  SendMsg(&gs, NMFGuaranteed);
}

void NetworkClient::DestroyAllObjects()
{
  _localObjects.Clear();
  _remoteObjects.Clear();
  if (DiagLevel >= 1)
    DiagLogF("Client: all objects destroyed");

  // avoid id duplicity at next missions
  // _nextId = 0;
}

void NetworkClient::ClientReady(NetworkGameState state)
{
  ChangeGameState gs(state);
  SendMsg(&gs, NMFGuaranteed);
}

bool NetworkClient::CreateVehicle(Vehicle *veh, VehicleListType list, RString name, int idVeh)
{
  if (!veh) return false;
  if (_state < NGSLoadIsland) return false;

  NetworkId id = CreateLocalObject(veh);

  // send message to center
  NetworkMessageType msgType = veh->GetNMType(NMCCreate);
  NetworkMessageFormatBase *format = GetFormat(/*LOCAL_PLAYER, */msgType);

  // create message
  Ref<NetworkMessage> msg = new NetworkMessage();
  msg->time = Glob.time;
  NetworkMessageContext ctx(msg, format, this, TO_SERVER, MSG_SEND);
  ctx.SetClass(NMCCreate);

  // difference from CreateObject - vehicle doesnot known its list, name, id:
  Assert(dynamic_cast<const IndicesCreateVehicle *>(ctx.GetIndices()))
  const IndicesCreateVehicle *indices = static_cast<const IndicesCreateVehicle *>(ctx.GetIndices());

  if (ctx.IdxTransfer(indices->list, (int &)list) != TMOK) return false;
  if (ctx.IdxTransfer(indices->name, name) != TMOK) return false;
  if (ctx.IdxTransfer(indices->idVehicle, idVeh) != TMOK) return false;
  
  if (veh->TransferMsg(ctx) != TMOK) return false;

  // send message
  DWORD dwMsgId = NetworkComponent::SendMsg
  (
    TO_SERVER, msg, msgType, NMFGuaranteed
  );
  return dwMsgId != 0xFFFFFFFF;
}

bool NetworkClient::CreateCommand(AISubgroup *subgrp, int index, Command *cmd)
{
  if (_state < NGSLoadIsland) return false;

  NetworkId id = CreateLocalObject(cmd);

  // send message to center
  NetworkMessageType msgType = cmd->GetNMType(NMCCreate);
  NetworkMessageFormatBase *format = GetFormat(/*LOCAL_PLAYER, */msgType);

  // create message
  Ref<NetworkMessage> msg = new NetworkMessage();
  msg->time = Glob.time;
  NetworkMessageContext ctx(msg, format, this, TO_SERVER, MSG_SEND);
  ctx.SetClass(NMCCreate);

  // difference from CreateObject - command doesnot known its subgrp, index
  Assert(dynamic_cast<const IndicesCreateCommand *>(ctx.GetIndices()))
  const IndicesCreateCommand *indices = static_cast<const IndicesCreateCommand *>(ctx.GetIndices());

  if (ctx.IdxTransferRef(indices->subgroup, subgrp) != TMOK) return false;
  if (ctx.IdxTransfer(indices->index, index) != TMOK) return false;
  if (cmd->TransferMsg(ctx) != TMOK) return false;

  // send message
  DWORD dwMsgId = NetworkComponent::SendMsg
  (
    TO_SERVER, msg, msgType, NMFGuaranteed
  );
  return dwMsgId != 0xFFFFFFFF;
}

void NetworkClient::DeleteCommand(AISubgroup *subgrp, int index, Command *cmd)
{
  const NetworkId &id = cmd->GetNetworkId();
  if (id.IsNull()) return;

  DeleteCommandMessage msg(subgrp, index, id);
  SendMsg(&msg, NMFGuaranteed);

#if CHECK_MSG
  CheckLocalObjects();
#endif

  for (int i=0; i<_localObjects.Size(); i++)
  {
    NetworkLocalObjectInfo &info = _localObjects[i];
    if (info.id == id)
    {
      _localObjects.Delete(i);
      if (DiagLevel >= 1)
        DiagLogF("Client: command deleted %d:%d", id.creator, id.id);
      return;
    }
  }
  LogF("Warning: Client: Command info %d:%d not found.", id.creator, id.id);

#if CHECK_MSG
  CheckLocalObjects();
#endif
}

bool NetworkClient::CreateCenter(AICenter *center)
{
  Assert(center);
  if (!CreateObject(center)) return false;

  for (int g=0; g<center->NGroups(); g++)
  {
    AIGroup *grp = center->GetGroup(g);
    if (!grp) continue;
    if (!CreateObject(grp)) return false;
    for (int s=0; s<grp->NSubgroups(); s++)
    {
      AISubgroup *subgrp = grp->GetSubgroup(s);
      if (!subgrp) continue;
      if (!CreateObject(subgrp)) return false;
      for (int u=0; u<subgrp->NUnits(); u++)
      {
        AIUnit *unit = subgrp->GetUnit(u);
        if (!unit) continue;
        if (!CreateObject(unit)) return false;
      }
    }
  }
  return true;
}

bool NetworkClient::UpdateCenter(AICenter *center)
{
  // order - units, subgroups, groups, center
  // because of ownership changes
  Assert(center);
  for (int g=0; g<center->NGroups(); g++)
  {
    AIGroup *grp = center->GetGroup(g);
    if (!grp) continue;
    for (int s=0; s<grp->NSubgroups(); s++)
    {
      AISubgroup *subgrp = grp->GetSubgroup(s);
      if (!subgrp) continue;
      for (int u=0; u<subgrp->NUnits(); u++)
      {
        AIUnit *unit = subgrp->GetUnit(u);
        if (!unit) continue;
//        if (UpdateObject(unit, NMFGuaranteed) < 0) return false;
        UpdateObject(unit, NMFGuaranteed);
      }
//      if (UpdateObject(subgrp, NMFGuaranteed) < 0) return false;
      UpdateObject(subgrp, NMFGuaranteed);
    }
//    if (UpdateObject(grp, NMFGuaranteed) < 0) return false;
    UpdateObject(grp, NMFGuaranteed);
  }
//  if (UpdateObject(center, NMFGuaranteed) < 0) return false;
  UpdateObject(center, NMFGuaranteed);

  for (int g=0; g<center->NGroups(); g++)
  {
    AIGroup *grp = center->GetGroup(g);
    if (!grp) continue;
    for (int s=0; s<grp->NSubgroups(); s++)
    {
      AISubgroup *subgrp = grp->GetSubgroup(s);
      if (!subgrp) continue;
      for (int u=0; u<subgrp->NUnits(); u++)
      {
        AIUnit *unit = subgrp->GetUnit(u);
        if (!unit) continue;

        int player = unit->GetPerson()->GetRemotePlayer();
        if (player != 1) SelectPlayer(player, unit->GetPerson());
      }
    }
  }
  
  return true;
}

//! Network Command Types
/*!
\patch_internal 1.11 Date 07/31/2001 by Jirka
- Added: processing of commands entered in chat line
*/
enum NetworkCommandType
{
  CMDNone = -1,
  CMDLogin,
  CMDLogout,
  CMDKick,
  CMDRestart,
  CMDMission,
  CMDMissions,
  CMDShutdown,
  CMDReassign,
  CMDMonitor,
  CMDUserlist,
  CMDVote,
  CMDAdmin,
  CMDInit,
  CMDDebug
};

//! Maps text form of Network Command to Network Command Type
struct NetworkCommand
{
  //! text form
  const char *command;
  //! Network Command Type 
  NetworkCommandType type;
};

//! Maps text form of Network Commands to Network Command Types
/*!
\patch 1.16 Date 08/10/2001 by Jirka
- Added: Dedicated server command #shutdown
- Added: Dedicated server command #reassign
\patch 1.22 Date 8/24/2001 by Jirka
- Added: Dedicated server command #monitor
\patch 1.24 Date 9/19/2001 by Jirka
- Added: Dedicated server command #userlist
- Added: Dedicated server command #kick <number = player id>
\patch 1.24 Date 9/20/2001 by Jirka
- Added: Voting system (command #vote ...)
Possible vote commands: kick, restart, reassign, mission, missions
\patch 1.33 Date 11/29/2001 by Jirka
- Added: Dedicated server command #vote admin
\patch 1.34 Date 12/06/2001 by Jirka
- Added: Dedicated server command #init (reload server config file)
\patch 1.87 Date 10/17/2002 by Jirka
- Added: Dedicated server command #debug (console, totalSent, userSent)
(Requires 1.87 client, client output sent to OS using OuputDebugString)
*/

static NetworkCommand NetworkCommands[] =
{
  {"login", CMDLogin},
  {"logout", CMDLogout},
  {"kick", CMDKick},
  {"restart", CMDRestart},
  {"mission", CMDMission},
  {"missions", CMDMissions},
  {"shutdown", CMDShutdown},
  {"reassign", CMDReassign},
  {"monitor", CMDMonitor},
  {"userlist", CMDUserlist},
  {"vote", CMDVote},
  {"admin", CMDAdmin},
  {"init", CMDInit},
  {"debug", CMDDebug}
};

//! Find Network Command Type for text form of Network Command
/*!
\param beg text form of Command
\param len length of text
*/
static NetworkCommandType FindCommandType(const char *beg, int len)
{
  for (int i=0; i<sizeof(NetworkCommands)/sizeof(NetworkCommand); i++)
  {
    NetworkCommand &item = NetworkCommands[i];
    if (strlen(item.command) == len && strnicmp(beg, item.command, len) == 0)
    {
      return item.type;
    }
  }
  return CMDNone;
}

//! Find DirectPlay ID of player for text form of player
/*!
\param name text form of player - player name or (simple) player id
\param identities player identities to search in
*/
static int FindPlayerId(const char *name, AutoArray<PlayerIdentity> &identities)
{
  for (int i=0; i<identities.Size(); i++)
  {
    if (stricmp(name, identities[i].name) == 0)
      return identities[i].dpnid;
  }
  int playerid = atoi(name);
  for (int i=0; i<identities.Size(); i++)
  {
    if (identities[i].playerid == playerid)
      return identities[i].dpnid;
  }
  return AI_PLAYER; // not found
}
/*!
\patch 1.33 Date 12/03/2001 by Ondra
- New: Admin / Server indication in list of players.
*/
RString GetIdentityText(const PlayerIdentity &identity)
{
  RString spec = Format("%d ms",identity._avgPing);
  if (identity._rights&PRServer)
  {
    spec = LocalizeString(IDS_MP_SERVER);
  }
  else if (identity._rights&(PRAdmin|PRVotedAdmin))
  {
    if (identity._rights&PRVotedAdmin)
    {
      spec = spec + RString(", ")+LocalizeString(IDS_MP_MASTER);
    }
    else
    {
      spec = spec + RString(", *")+LocalizeString(IDS_MP_MASTER)+RString("*");
    }
  }
  return Format("%s (%s)",(const char *)identity.name,(const char *)spec);
}

/*!
\patch 1.25 Date 10/01/2001 by Jirka
- Added: Enable kick off player from MPPlayers display also by gamemaster on dedicated server.
*/
void NetworkClient::SendKick(int player)
{
  NetworkCommandMessage msg;
  msg.type = NCMTKick;
  msg.content.Write(&player, sizeof(int));
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SendLockSession(bool lock)
{
  NetworkCommandMessage msg;
  msg.type = NCMTLockSession;
  msg.content.Write(&lock, sizeof(bool));
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::Disconnect(RString message)
{
  RemoveUserMessages();

  _state = NGSNone;
  _serverState = NGSNone;

  const PlayerIdentity *id = FindIdentity(_player);
  if (id)
  {
    RString senderName = id->name;
    GChatList.Add(CCGlobal, senderName, message, false, true);
    RefArray<NetworkObject> dummy;
    GNetworkManager.Chat(CCGlobal,senderName,dummy,message);
  }
}

bool NetworkClient::ProcessCommand(RString command)
{
  const char *beg = command;
  if (*beg != '#') return false;
  beg++;

  const char *end = strchr(beg, ' ');
  int len = end ? end - beg : strlen(beg);
  NetworkCommandType type = FindCommandType(beg, len);
  beg += len;
  while (*beg == ' ') beg++;

  switch (type)
  {
    case CMDNone:
    case CMDAdmin:
      break;
    case CMDLogin:
      {
        NetworkCommandMessage msg;
        msg.type = NCMTLogin;
        msg.content.WriteString(beg);
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDLogout:
      if (_gameMaster)
      {
        // _gameMaster = false;
        // _selectMission = false;
        // _serverMissions.Clear();

        NetworkCommandMessage msg;
        msg.type = NCMTLogout;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDKick:
      if (_gameMaster || GetNetworkManager().IsServer())
      {
        int id = FindPlayerId(beg, _identities);
        if (id != AI_PLAYER) SendKick(id);
      }
      break;
    case CMDRestart:
      if (_gameMaster)
      {
        NetworkCommandMessage msg;
        msg.type = NCMTRestart;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDMission:
      if (_gameMaster)
      {
        NetworkCommandMessage msg;
        msg.type = NCMTMission;
        msg.content.WriteString(beg);
        bool cadetMode = false;
        msg.content.Write(&cadetMode, sizeof(bool));
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDMissions:
      if (_gameMaster)
      {
        // _selectMission = true;
        NetworkCommandMessage msg;
        msg.type = NCMTMissions;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDShutdown:
      if (_gameMaster)
      {
        NetworkCommandMessage msg;
        msg.type = NCMTShutdown;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDReassign:
      if (_gameMaster)
      {
        NetworkCommandMessage msg;
        msg.type = NCMTReassign;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDMonitor:
      if (_gameMaster)
      {
        NetworkCommandMessage msg;
        msg.type = NCMTMonitorAsk;
        float value = 10.0f;
        if (*beg) value = atof(beg);
        msg.content.Write(&value, sizeof(value));
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDUserlist:
      for (int i=0; i<_identities.Size(); i++)
      {
        const PlayerIdentity &identity = _identities[i];
        char buffer[256];
        sprintf
        (
          buffer, "%d: %s (id = %s)",
          identity.playerid,
          (const char *)GetIdentityText(identity),
          (const char *)identity.id
        );
        GChatList.Add(CCGlobal, NULL, buffer, false, true);
      }
      break;
    case CMDInit:
      if (_gameMaster)
      {
        NetworkCommandMessage msg;
        msg.type = NCMTInit;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDDebug:
      if (_gameMaster || GetNetworkManager().IsServer())
      {
        NetworkCommandMessage msg;
        msg.type = NCMTDebugAsk;
        msg.content.WriteString(beg);
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDVote:
      {
        end = strchr(beg, ' ');
        len = end ? end - beg : strlen(beg);
        type = FindCommandType(beg, len);
        beg += len;
        while (*beg == ' ') beg++;

        switch (type)
        {
          case CMDKick:
            {
              int id = FindPlayerId(beg, _identities);
              if (id != AI_PLAYER)
              {
                NetworkCommandMessage msg;
                msg.type = NCMTVote;
                int subtype = NCMTKick;
                msg.content.Write(&subtype, sizeof(int));
                msg.content.Write(&id, sizeof(int));
                SendMsg(&msg, NMFGuaranteed);
              }
            }
            break;
          case CMDRestart:
            {
              NetworkCommandMessage msg;
              msg.type = NCMTVote;
              int subtype = NCMTRestart;
              msg.content.Write(&subtype, sizeof(int));
              SendMsg(&msg, NMFGuaranteed);
            }
            break;
          case CMDReassign:
            {
              NetworkCommandMessage msg;
              msg.type = NCMTVote;
              int subtype = NCMTReassign;
              msg.content.Write(&subtype, sizeof(int));
              SendMsg(&msg, NMFGuaranteed);
            }
            break;
          case CMDMission:
            {
              NetworkCommandMessage msg;
              msg.type = NCMTVote;
              int subtype = NCMTMission;
              msg.content.Write(&subtype, sizeof(int));
              RString name = beg;
              name.Lower();
              msg.content.WriteString(name);
              bool cadetMode = false;
              msg.content.Write(&cadetMode, sizeof(bool));
              SendMsg(&msg, NMFGuaranteed);
            }
            break;
          case CMDMissions:
            {
              NetworkCommandMessage msg;
              msg.type = NCMTVote;
              int subtype = NCMTMissions;
              msg.content.Write(&subtype, sizeof(int));
              SendMsg(&msg, NMFGuaranteed);
            }
            break;
          case CMDAdmin:
            {
              int id = FindPlayerId(beg, _identities);
              if (id != AI_PLAYER)
              {
                NetworkCommandMessage msg;
                msg.type = NCMTVote;
                int subtype = NCMTAdmin;
                msg.content.Write(&subtype, sizeof(int));
                msg.content.Write(&id, sizeof(int));
                SendMsg(&msg, NMFGuaranteed);
              }
            }
            break;
        }
      }
      break;
  }
  return true;
}

void NetworkClient::SelectMission(RString mission, bool cadetMode)
{
  if (_gameMaster)
  {
    NetworkCommandMessage msg;
    msg.type = NCMTMission;
    msg.content.WriteString(mission);
    msg.content.Write(&cadetMode, sizeof(bool));
    SendMsg(&msg, NMFGuaranteed);
  }
  _selectMission = false;
  _voteMission = false;
}

void NetworkClient::VoteMission(RString mission, bool cadetMode)
{
  NetworkCommandMessage msg;
  msg.type = NCMTVote;
  int subtype = NCMTMission;
  msg.content.Write(&subtype, sizeof(int));
  msg.content.WriteString(mission);
  msg.content.Write(&cadetMode, sizeof(bool));
  SendMsg(&msg, NMFGuaranteed);

  _voteMission = false;
}

DWORD NetworkClient::SendMsg
(
  NetworkSimpleObject *object, NetMsgFlags dwFlags
)
{
  return NetworkComponent::SendMsg(TO_SERVER, object, dwFlags);
}

void NetworkClient::EnqueueMsg
(
  int to, NetworkMessage *msg,
  NetworkMessageType type
)
{
  int index = _messageQueue.Add();
  _messageQueue[index].type = type;
  _messageQueue[index].msg = msg;
}

/*!
\patch 1.26 Date 10/05/2001 by Jirka
- Optimized: MP: small update messages are sent packed
  to form optimum size UDP frames.
*/

void NetworkClient::EnqueueMsgNonGuaranteed
(
  int to, NetworkMessage *msg,
  NetworkMessageType type
)
{
  int index = _messageQueueNonGuaranteed.Add();
  _messageQueueNonGuaranteed[index].type = type;
  _messageQueueNonGuaranteed[index].msg = msg;
}

NetworkMessageFormatBase *NetworkClient::GetFormat(/*int client, */int type)
{
  Assert(type >= 0);
  if (type < NMTFirstVariant)
  {
    // static format
    return GMsgFormats[type];
  }
  else
  {
    // dynamic format
    int index = type - NMTFirstVariant;
    if (index >= _formats.Size()) return NULL;
    return &_formats[index];
  }
}

void NetworkClient::GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players)
{
  players.Resize(0);
  for (int i=0; i<_identities.Size(); i++)
  {
    int index = players.Add();
    players[index].dpid = _identities[i].dpnid;
    players[index].name = _identities[i].GetName();
  }
}

void NetworkClient::AssignPlayer(int role, int player)
{
  if (role < 0 || role >= _playerRoles.Size()) return;

  if (_playerRoles[role].player == player) return;
  
  PlayerRole info = _playerRoles[role];
  info.player = player;
  info.roleLocked = false;

  // create message
  NetworkMessageType type = info.GetNMType(NMCCreate);
  NetworkMessageFormatBase *format = GetFormat(type);
  Ref<NetworkMessage> msg = new NetworkMessage();
  msg->time = Glob.time;
  NetworkMessageContext ctx(msg, format, this, TO_SERVER, MSG_SEND);

  Assert(dynamic_cast<const IndicesPlayerRole *>(ctx.GetIndices()))
  const IndicesPlayerRole *indices = static_cast<const IndicesPlayerRole *>(ctx.GetIndices());

  TMError err;
  err = ctx.IdxTransfer(indices->index, role);
  if (err != TMOK) return;
  err = info.TransferMsg(ctx);
  if (err != TMOK) return;

  // send message
  NetworkComponent::SendMsg(TO_SERVER, msg, type, NMFGuaranteed);
}


void NetworkClient::AskForDammage
(
  Object *who, EntityAI *owner,
  Vector3Par modelPos, float val, float valRange, RString ammo
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForDammageMessage msg;
  msg.who = who;
  msg.owner = owner;
  msg.modelPos = modelPos;
  msg.val = val;
  msg.valRange = valRange;
  msg.ammo = ammo;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForSetDammage
(
  Object *who, float dammage
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForSetDammageMessage msg;
  msg.who = who;
  msg.dammage = dammage;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForGetIn
(
  Person *soldier, Transport *vehicle,
  GetInPosition position
)
{
  if (_state < NGSPlay) return; // send event messages only when playing
/*
RptF
(
  "Asking for get in: %s (%s, id %d:%d) into %s (%s, id %d:%d)",
  (const char *)soldier->GetDebugName(), soldier->IsLocal() ? "LOCAL" : "REMOTE",
  soldier->GetNetworkId().creator, soldier->GetNetworkId().id,
  (const char *)vehicle->GetDebugName(), vehicle->IsLocal() ? "LOCAL" : "REMOTE",
  vehicle->GetNetworkId().creator, vehicle->GetNetworkId().id
);
*/
  AskForGetInMessage msg;
  msg.soldier = soldier;
  msg.vehicle = vehicle;
  msg.position = position;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForGetOut
(
  Person *soldier, Transport *vehicle, bool parachute
)
{
  if (_state < NGSPlay) return; // send event messages only when playing
/*
RptF
(
  "Asking for get out: %s (%s, id %d:%d) from %s (%s, id %d:%d)",
  (const char *)soldier->GetDebugName(), soldier->IsLocal() ? "LOCAL" : "REMOTE",
  soldier->GetNetworkId().creator, soldier->GetNetworkId().id,
  (const char *)vehicle->GetDebugName(), vehicle->IsLocal() ? "LOCAL" : "REMOTE",
  vehicle->GetNetworkId().creator, vehicle->GetNetworkId().id
);
*/

  AskForGetOutMessage msg;
  msg.soldier = soldier;
  msg.vehicle = vehicle;
  msg.parachute = parachute;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForChangePosition
(
  Person *soldier, Transport *vehicle, UIActionType type
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForChangePositionMessage msg;
  msg.soldier = soldier;
  msg.vehicle = vehicle;
  msg.type = type;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForAimWeapon
(
  EntityAI *vehicle, int weapon, Vector3Par dir
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForAimWeaponMessage msg;
  msg.vehicle = vehicle;
  msg.weapon = weapon;
  msg.dir = dir;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForAimObserver
(
  EntityAI *vehicle, Vector3Par dir
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForAimObserverMessage msg;
  msg.vehicle = vehicle;
  msg.dir = dir;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForSelectWeapon
(
  EntityAI *vehicle, int weapon
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForSelectWeaponMessage msg;
  msg.vehicle = vehicle;
  msg.weapon = weapon;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForAmmo
(
  EntityAI *vehicle, int weapon, int burst
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForAmmoMessage msg;
  msg.vehicle = vehicle;
  msg.weapon = weapon;
  msg.burst = burst;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForAddImpulse
(
  Vehicle *vehicle, Vector3Par force, Vector3Par torque
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForAddImpulseMessage msg;
  msg.vehicle = vehicle;
  msg.force = force;
  msg.torque = torque;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForMove
(
  Object *vehicle, Vector3Par pos
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForMoveVectorMessage msg;
  msg.vehicle = vehicle;
  msg.pos = pos;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForMove
(
  Object *vehicle, Matrix4Par trans
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForMoveMatrixMessage msg;
  msg.vehicle = vehicle;
  msg.pos = trans.Position();
  msg.orient = trans.Orientation();
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForJoin
(
  AIGroup *join, AIGroup *group
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForJoinGroupMessage msg;
  msg.join = join;
  msg.group = group;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForJoin
(
  AIGroup *join, OLinkArray<AIUnit> &units
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForJoinUnitsMessage msg;
  msg.join = join;
  msg.units = units;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForHideBody(Person *vehicle)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  AskForHideBodyMessage msg;
  msg.vehicle = vehicle;
  SendMsg(&msg, NMFGuaranteed); 
}

void NetworkClient::ExplosionDammageEffects
(
  EntityAI *owner, Shot *shot,
  Object *directHit, Vector3Par pos, Vector3Par dir, const AmmoType *type,
  bool enemyDammage
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  ExplosionDammageEffectsMessage msg;
  msg.owner = owner;
  msg.shot = shot;
  msg.directHit = directHit;
  msg.pos = pos;
  msg.dir = dir;
  msg.type = type->GetName();
  msg.enemyDammage = enemyDammage;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::FireWeapon
(
  EntityAI *vehicle, int weapon, const Magazine *magazine, EntityAI *target
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  if (!magazine) return;

  FireWeaponMessage msg;
  msg.vehicle = vehicle;
  msg.target = target;
  msg.weapon = weapon;
  msg.magazineCreator = magazine->_creator;
  msg.magazineId = magazine->_id;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::UpdateWeapons(EntityAI *vehicle)
{
  UpdateWeaponsMessage msg;
  msg.vehicle = vehicle;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AddWeaponCargo(VehicleSupply *vehicle, RString weapon)
{
  AddWeaponCargoMessage msg;
  msg.vehicle = vehicle;
  msg.weapon = weapon;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::RemoveWeaponCargo(VehicleSupply *vehicle, RString weapon)
{
  RemoveWeaponCargoMessage msg;
  msg.vehicle = vehicle;
  msg.weapon = weapon;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AddMagazineCargo(VehicleSupply *vehicle, const Magazine *magazine)
{
  AddMagazineCargoMessage msg;
  msg.vehicle = vehicle;
  msg.magazine = const_cast<Magazine *>(magazine);
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::RemoveMagazineCargo(VehicleSupply *vehicle, int creator, int id)
{
  RemoveMagazineCargoMessage msg;
  msg.vehicle = vehicle;
  msg.creator = creator;
  msg.id = id;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::VehicleInit(VehicleInitCmd &init)
{
  SendMsg(&init, NMFGuaranteed);
}

void NetworkClient::OnVehicleDestroyed
(
  EntityAI *killed, EntityAI *killer
)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  VehicleDestroyedMessage msg;
  msg.killed = killed;
  msg.killer = killer;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::OnVehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage, RString ammo)
{
  if (_state < NGSPlay) return;

  VehicleDamagedMessage msg;
  msg._damaged = damaged;
  msg._killer = killer;
  msg._damage = damage;
  msg._ammo = ammo;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::OnIncomingMissile(EntityAI *target, RString ammo, EntityAI *owner)
{
  if (_state < NGSPlay) return;

  IncomingMissileMessage msg;
  msg._target = target;
  msg._ammo = ammo;
  msg._owner = owner;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::MarkerCreate(int channel, AIUnit *sender, RefArray<NetworkObject> &units, ArcadeMarkerInfo &info)
{
  if (_state < NGSLoadIsland) return;

  MarkerCreateMessage msg;
  msg.marker = info;
  msg.channel = channel;
  msg.sender = sender;
  msg.units = units;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::MarkerDelete(RString name)
{
  if (_state < NGSLoadIsland) return;

  MarkerDeleteMessage msg;
  msg.name = name;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SetFlagOwner
(
  Person *owner, EntityAI *carrier
)
{
  SetFlagOwnerMessage msg;
  msg.owner = owner;
  msg.carrier = carrier;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SetFlagCarrier
(
  Person *owner, EntityAI *carrier
)
{
  SetFlagCarrierMessage msg;
  msg.owner = owner;
  msg.carrier = carrier;
  SendMsg(&msg, NMFGuaranteed);
}

static Object *GetValObject( GameValuePar oper )
{
  if( oper.GetType()==GameObject )
  {
    return static_cast<GameDataObject *>(oper.GetData())->GetObject();
  }
  return NULL;
}

static AIGroup *GetValGroup( GameValuePar oper )
{
  if( oper.GetType()==GameGroup )
  {
    return static_cast<GameDataGroup *>(oper.GetData())->GetGroup();
  }
  return NULL;
}

/*!
Implemented only for scalar variables.
\todo Implement for other variable types.
\patch 1.34 Date 12/7/2001 by Ondra
- New: publicVariable now works with scalar, bool, Object and Group types.
*/

void NetworkClient::PublicVariable(RString name)
{
  GameState *gstate = GWorld->GetGameState();
  GameValuePar var = gstate->VarGet(name);
  if (var.GetNil()) return;

  QOStream out;
  int type = var.GetType();

  out.write(&type, sizeof(int));
  if (type == GameScalar)
  {
    GameScalarType value = var.GetData()->GetScalar();
    out.write(&value, sizeof(value));
  }
  else if (type==GameBool)
  {
    GameBoolType value = var.GetData()->GetBool();
    out.write(&value, sizeof(value));
  }
  else if (type==GameObject)
  {
    GameObjectType value = GetValObject(var);
    NetworkId id = value ? value->GetNetworkId() : NetworkId::Null();
    out.write(&id, sizeof(id));
  }
  else if (type==GameGroup)
  {
    GameGroupType value = GetValGroup(var);
    NetworkId id = value ? value->GetNetworkId() : NetworkId::Null();
    out.write(&id, sizeof(id));
  }
  else
  {
    Fail("Not implemented");
    return;
  }

  PublicVariableMessage msg;
  msg._name = name;
  int size = out.pcount();
  msg._value.Resize(size);
  memcpy(msg._value.Data(), out.str(), size);
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::GroupSynchronization(AIGroup *grp, int synchronization, bool active)
{
  GroupSynchronizationMessage msg;
  msg._group = grp;
  msg._synchronization = synchronization;
  msg._active = active;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::DetectorActivation(Detector *det, bool active)
{
  DetectorActivationMessage msg;
  msg._detector = det;
  msg._active = active;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForCreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank)
{
  AskForCreateUnitMessage msg;
  msg._group = group;
  msg._type = type;
  msg._position = position;
  msg._init = init;
  msg._skill = skill;
  msg._rank = rank;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForDeleteVehicle(Entity *veh)
{
  AskForDeleteVehicleMessage msg;
  msg._vehicle = veh;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForReceiveUnitAnswer
(
  AIUnit *from, AISubgroup *to, int answer
)
{
  AskForReceiveUnitAnswerMessage msg;
  msg._from = from;
  msg._to = to;
  msg._answer = answer;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForGroupRespawn(Person *person, EntityAI *killer)
{
  AskForGroupRespawnMessage msg;
  msg._person = person;
  msg._killer = killer;
  msg._group = person->Brain()->GetGroup();
  msg._from = GetPlayer();
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForActivateMine(Mine *mine, bool activate)
{
  AskForActivateMineMessage msg;
  msg._mine = mine;
  msg._activate = activate;
  SendMsg(&msg, NMFGuaranteed);
}

/*!
\patch 1.82 Date 8/26/2002 by Jirka
- Fixed: Fire burning is synchronized in MP
*/

void NetworkClient::AskForInflameFire(Fireplace *fireplace, bool fire)
{
  AskForInflameFireMessage msg;
  msg._fireplace = fireplace;
  msg._fire = fire;
  SendMsg(&msg, NMFGuaranteed);
}

/*!
\patch 1.82 Date 8/26/2002 by Jirka
- Fixed: Transfer of user defined animations through network in MP
*/

void NetworkClient::AskForAnimationPhase(Entity *vehicle, RString animation, float phase)
{
  AskForAnimationPhaseMessage msg;
  msg._vehicle = vehicle;
  msg._animation = animation;
  msg._phase = phase;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::CopyUnitInfo(Person *from, Person *to)
{
  CopyUnitInfoMessage msg;
  msg._from = from;
  msg._to = to;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::Chat(int channel, RString text)
{
  ChatMessage msg(channel, GetLocalPlayerName(), text);
  SendMsg(&msg, NMFGuaranteed|GetChatPriority());
}

void NetworkClient::Chat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text)
{
  ChatMessage msg(channel, sender, units, GetLocalPlayerName(), text);
  SendMsg(&msg, NMFGuaranteed|GetChatPriority());
}

void NetworkClient::Chat(int channel, RString sender, RefArray<NetworkObject> &units, RString text)
{
  ChatMessage msg(channel, NULL, units, sender, text);
  SendMsg(&msg, NMFGuaranteed|GetChatPriority());
}

void NetworkClient::RadioChat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text, RadioSentence &sentence)
{
  RadioChatMessage msg;
  msg.channel = channel;
  msg.sender = sender;
  msg.units = units;
  msg.text = text;
  msg.sentence = sentence;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::RadioChatWave(int channel, RefArray<NetworkObject> &units, RString wave, AIUnit *sender, RString senderName)
{
  RadioChatWaveMessage msg;
  msg.channel = channel;
  msg.units = units;
  msg.wave = wave;
  msg.sender = sender;
  msg.senderName = senderName;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SetVoiceChannel(int channel)
{
  SetVoiceChannelMessage msg(channel);
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SetVoiceChannel(int channel, RefArray<NetworkObject> &units)
{
  SetVoiceChannelMessage msg(channel, units);
  SendMsg(&msg, NMFGuaranteed);
}

/*!
Note: Files can be transferred only to players temporary folder on server -
path dest format must be "tmp\\players\\" + playername + "\\relativepath".
Transferred file size must not be bigger than MaxCustomFileSize.
All of this is enforced server side and clients violating this are kicked.
This is done to provide some reasonable level of security.
Otherwise fake clients would be able to write anywhere on the server disc.
\patch_internal 1.82 Date 8/19/2002 by Ondra
- Improved: Server-side check of tranferred file path and size.
*/

void NetworkClient::TransferFileToServer(RString dest, RString source)
{
  int maxSegmentSize;
  if (IsUseSockets())
    maxSegmentSize = 512 - 50;
  else
    maxSegmentSize = 0x10000;

  QIFStreamB f;
  f.AutoOpen(source);

  TransferFileToServerMessage msg;
  msg.path = dest;
  msg.totSize = f.GetBuffer()->GetSize();
  msg.totSegments = (msg.totSize + maxSegmentSize - 1) / maxSegmentSize;
  msg.offset = 0;
  for (int i=0; i<msg.totSegments; i++)
  {
    msg.curSegment = i;
    int size = min(maxSegmentSize, msg.totSize - msg.offset);
    msg.data.Resize(size);
    memcpy(msg.data.Data(), f.GetBuffer()->GetData() + msg.offset, size);
    SendMsg(&msg, NMFGuaranteed);
    msg.offset += size;
  }
}

void NetworkClient::GetTransferStats(int &curBytes, int &totBytes)
{
  curBytes = 0; totBytes = 0;
  for (int i=0; i<_files.Size(); i++)
  {
    curBytes = _files[i].received;
    totBytes = _files[i].fileData.Size();
  }
}

NetworkLocalObjectInfo *NetworkClient::GetLocalObjectInfo(NetworkId &id)
{
  for (int i=0; i<_localObjects.Size(); i++)
  {
    NetworkLocalObjectInfo &info = _localObjects[i];
    if (info.id == id) return &info;
  }
  return NULL;
}

NetworkRemoteObjectInfo *NetworkClient::GetRemoteObjectInfo(NetworkId &id)
{
  for (int i=0; i<_remoteObjects.Size(); i++)
  {
    NetworkRemoteObjectInfo &info = _remoteObjects[i];
    if (info.id == id) return &info;
  }
  return NULL;
}

NetworkObject *NetworkClient::GetObject(NetworkId &id)
{
  if (id.creator == 0) return NULL;
  else if (id.creator == 1) return GLandscape->FindObject(id.id);
  else
  {
    NetworkRemoteObjectInfo *infoR = GetRemoteObjectInfo(id);
    if (infoR) return infoR->object;
    NetworkLocalObjectInfo *infoL = GetLocalObjectInfo(id);
    if (infoL) return infoL->object;
    return NULL;
  }
}

void NetworkClient::DeleteObject(NetworkId &id)
{
  if (id.IsNull()) return;

  DeleteObjectMessage msg;
  msg.object = id;
  SendMsg(&msg, NMFGuaranteed);

#if CHECK_MSG
  CheckLocalObjects();
#endif

  for (int i=0; i<_localObjects.Size(); i++)
  {
    NetworkLocalObjectInfo &info = _localObjects[i];
    if (info.id == id)
    {
      _localObjects.Delete(i);
      if (DiagLevel >= 1)
        DiagLogF("Client: local object destroyed (DeleteObject function) %d:%d", id.creator, id.id);
      return;
    }
  }
  LogF("Warning: Client: Object info %d:%d not found.", id.creator, id.id);

#if CHECK_MSG
  CheckLocalObjects();
#endif
}

void NetworkClient::ShowTarget(Person *vehicle, TargetType *target)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  ShowTargetMessage msg;
  msg.vehicle = vehicle;
  msg.target = target;
  SendMsg(&msg, NMFGuaranteed); 
}

void NetworkClient::ShowGroupDir(Person *vehicle, Vector3Par dir)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  ShowGroupDirMessage msg;
  msg.vehicle = vehicle;
  msg.dir = dir;
  SendMsg(&msg, NMFGuaranteed); 
}

//! Subsidiary structure for sorting messages by error
struct UpdateLocalObjectInfo
{
  //! Local object info
  NetworkLocalObjectInfo *oInfo;
  //! Message class type
  NetworkMessageClass cls;
  //! Error (difference)
  float error;
  //! Critical update
  bool criticalUpdate;
};
TypeIsSimple(UpdateLocalObjectInfo)

//! Compare local object infos by error
int CmpLocalObjects
(
  const UpdateLocalObjectInfo *info1,
  const UpdateLocalObjectInfo *info2
)
{
  float diff = info2->error - info1->error;
  return sign(diff);
}

//! Format value of error for output
/*!
\param val value to format
\param buffer output buffer
\return pointer to output buffer
*/
const char *FormatVal(float val, char *buffer)
{
  if (val == FLT_MAX)
    strcpy(buffer, "MAX");
  else
    sprintf(buffer, "%.0f", val);
  return buffer;
}

void NetworkClient::EstimateBandwidth(int &nMsgMax, int &nBytesMax)
{
  if (IsBotClient())
  {
    // unlimited bandwidth
    nMsgMax = INT_MAX;
    nBytesMax = INT_MAX;
  }
  else
  {
  #if _ENABLE_MP
    int nMsg = 0, nBytes = 0, nMsgG = 0, nBytesG = 0;
    _client->GetSendQueueInfo(nMsg, nBytes, nMsgG, nBytesG);
    if ( nMsgG > 0 )    // new style?
    {
        nMsg += nMsgG;
        nBytes += nBytesG;
    }
  #endif
/*
  #if _ENABLE_CHEATS
    if (outputDiags == 1 && nMsg > 0) sprintf(output, "Waiting (%d, %d); ", nBytes, nMsg);
  #endif
*/
    nMsgMax = MaxMsgSend;
    nBytesMax = 1024;

    #if _ENABLE_MP
    int latencyMS, throughputBPS;
    if (_client->GetConnectionInfo(latencyMS, throughputBPS))
    {
      // nBytesMax += 1.25 * info.dwThroughputBPS;
      nBytesMax += throughputBPS;
      nBytesMax += throughputBPS >> 2; // info.dwThroughputBPS / 4
/*
  #if _ENABLE_CHEATS
      if (outputDiags == 1) sprintf(output + strlen(output), "%d ms, %d bps; ", latencyMS, throughputBPS*8);
  #endif
*/
      if (_connectionQuality < CQPoor)
      {
        if (latencyMS >= 500) _connectionQuality = CQPoor;
        // if (throughputBPS < 3000) _connectionQuality = CQPoor;
      }
    }
    int nPlayers = _identities.Size() - 1;
  //  if (nPlayers > 0) saturateMax(nBytesMax, 14400 / nPlayers);
    if (nPlayers > 0) saturate(nBytesMax, MinBandwidth/8, MaxBandwidth/8);

    nBytesMax = toInt(0.001 * GEngine->GetAvgFrameDuration() * nBytesMax);

    nBytesMax -= nBytes;
    nMsgMax -= nMsg;
    #endif

  #if _ENABLE_CHEATS
    if (outputLogs) LogF("Client: Limit (%d, %d)", nBytesMax, nMsgMax);
/*
    if (outputDiags == 1) sprintf(output + strlen(output), "Limit (%d, %d); ", nBytesMax, nMsgMax);
*/
  #endif
  }
}

void NetworkClient::CreateObjectsList(AutoArray<UpdateLocalObjectInfo, MemAllocSA> &objects)
{
  float maxError = 0, sumError = 0;
  for (int i=0; i<_localObjects.Size(); i++)
  {
    NetworkLocalObjectInfo &oInfo = _localObjects[i];
    
    for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
    {
      NetworkUpdateInfo &info = oInfo.updates[j];

      float error;
      if (info.lastCreatedMsg)
      {
        // message on the way
        continue;
      }
      else
      {
        NetworkMessageType type = oInfo.object->GetNMType((NetworkMessageClass)j);
        if (type == NMTNone) continue;
        
        if (!info.lastSentMsg)
          error = FLT_MAX;
        else
        {
          NetworkMessageFormatBase *format = GetFormat(type);
          if (!format)
          {
            RptF("Client: Bad message %d(%s) format", (int)type, (const char *)NetworkMessageTypeNames[type]);
            continue;
          }
          NetworkMessageContext ctx
          (
            info.lastSentMsg, format,
            this, TO_SERVER,
            MSG_RECEIVE               // avoid PrepareMessage
          );
          ctx.SetClass((NetworkMessageClass)j);
          error = oInfo.object->CalculateError(ctx);
#if LOG_CLIENT_ERRORS
          if (error >= LogClientErrorLimit)
          {
            DiagLogF
            (
              "Object %s %d:%d",
              NetworkMessageTypeNames[type], oInfo.id.creator, oInfo.id.id
            );
            DiagLogF
            (
              "  - error %.3f", error
            );
            float dummy = oInfo.object->CalculateError(ctx);
            DoAssert(dummy == error);
          }
#endif
          #if _ENABLE_REPORT
            if (!_finite(error))
            {
              RptF("Client: Infinite error detected on %s",(const char *)oInfo.object->GetDebugName());
              // recalculate: debuggin opportunity
              oInfo.object->CalculateError(ctx);
            }
          #endif
        }
        // note: following condition seems to be identical to error>MinErrorToSend
        // but it is different when error is NaN
        if (!(error <= MinErrorToSend))
        {
          int index = objects.Add();
          objects[index].oInfo = &oInfo;
          objects[index].cls = (NetworkMessageClass)j;
          objects[index].error = error;
          objects[index].criticalUpdate = false;
          #if _ENABLE_REPORT
            if (!_finite(error))
            {
              RptF("Client: Infinite error catched on %s",(const char *)oInfo.object->GetDebugName());
            }
          #endif
        }
        saturateMax(maxError, error);
        sumError += error;
      }
    }
  }
/*
#if _ENABLE_CHEATS
  if (outputDiags == 1 && sumError > 0)
  {
    char buf1[32], buf2[32];
    sprintf
    (
      output + strlen(output),
      "Err %s / %s",
      FormatVal(maxError, buf1),
      FormatVal(sumError, buf2)
    );
    GlobalShowMessage(1000, output);
  }
#endif
*/

  // sort local objects by errors
  QSort(objects.Data(), objects.Size(), CmpLocalObjects);
}

void NetworkClient::SendMessages()
{
  // calculate limits
  int nMsg = 0, nBytes = 0;
  int nMsgMax, nBytesMax;
  
  EstimateBandwidth(nMsgMax, nBytesMax);

  // send enqueued guaranteed messages

  #if 0
  float throttle = floatMax(1,ThrottleGuaranteed);
  float nMsgMaxThrottle = nMsgMax*throttle;
  float nBytesMaxThrottle = nBytesMax*throttle;
  saturate(nMsgMaxThrottle,1,INT_MAX-1);
  saturate(nBytesMaxThrottle,1,INT_MAX-1);
  const int nMsgMaxGuaranteed = toLargeInt(nMsgMaxThrottle);
  const int nBytesMaxGuaranteed = toLargeInt(nBytesMaxThrottle);
  #else
  const int nMsgMaxGuaranteed = nMsgMax;
  const int nBytesMaxGuaranteed = nBytesMax;
  #endif

  int maxSize = MaxSizeGuaranteed;
  while (_messageQueue.Size() > 0)
  {
    if (nMsg >= nMsgMaxGuaranteed || nBytes >= nBytesMaxGuaranteed)
    {
/*
#if _ENABLE_CHEATS
      if (outputDiags == 1)
      {
        strcat(output, "Limit reached");
        GlobalShowMessage(1000, output);
      }
#endif
*/
      break;
    }

    NetworkMessage *msg = _messageQueue[0].msg;
    int size = msg->size;
    if (size >= maxSize)
    {
      DWORD dwMsgId = SendMsgRemote(TO_SERVER, msg, _messageQueue[0].type, NMFGuaranteed|NMFStatsAlreadyDone);
      _messageQueue.Delete(0);
      nMsg++;
      nBytes += size;
    }
    else
    {
      int totalSize = 0;
      int i;
      for (i=0; i<_messageQueue.Size(); i++)
      {
        NetworkMessage *msg = _messageQueue[i].msg;
        int size = msg->size;
        if (totalSize + size > maxSize) break;
        totalSize += size;
      }
      DWORD dwMsgId = SendMsgQueue(TO_SERVER, _messageQueue, 0, i, NMFGuaranteed);
      _messageQueue.Delete(0, i);
      nMsg++;
      nBytes += totalSize;
    }
  }

  AUTO_STATIC_ARRAY(UpdateLocalObjectInfo, objects, 256);

#define ENABLE_CRITICAL_UPDATES 1

#if !ENABLE_CRITICAL_UPDATES
  if (nMsg >= nMsgMax || nBytes >= nBytesMax)
  {
  #if _ENABLE_CHEATS
    if (outputLogs) LogF("  Client: limit reached (guaranteed)");
  #endif
    goto DoNotSendUpdates;
  }
#endif

  if (_state >= NGSPlay)
  {
  #if CHECK_MSG
    CheckLocalObjects();
  #endif

    // delete destroyed objects - keep order
    for (int i=0; i<_localObjects.Size();)
    {
      NetworkLocalObjectInfo &info = _localObjects[i];
      if (!info.object)
      {
        DeleteObjectMessage msg;
        msg.object = info.id;
        SendMsg(&msg, NMFGuaranteed);
        if (DiagLevel >= 1)
          DiagLogF("Client: local object destroyed (object not found) %d:%d", info.id.creator, info.id.id);
        _localObjects.Delete(i);
        continue;
      }
      i++;
    }

  #if CHECK_MSG
    CheckLocalObjects();
  #endif

    // update errors of local objects
    CreateObjectsList(objects);

    // create and send critical updates     
  #if ENABLE_CRITICAL_UPDATES
    for (int i=0; i<objects.Size(); i++)
    {
      NetworkLocalObjectInfo &info = *objects[i].oInfo;
      NetworkMessageClass cls = objects[i].cls;

      float error = objects[i].error;

      // any message about dammage that transmit change
      // that could mean dead should be considered high priority
      if
      (
        cls != NMCUpdateDammage || error < ERR_COEF_STRUCTURE
      ) continue;

      objects[i].criticalUpdate = true;
      int bytes = UpdateObject(info.object, cls, NMFGuaranteed | NMFHighPriority);


    #if _ENABLE_CHEATS
      if (outputLogs)
      {
        LogF
        (
          "  Client: Object %d:%d updated - size %d bytes, critical",
          info.id.creator, info.id.id, bytes
        );
      }
    #endif

      if (bytes >= 0)
      {
        // no error
        nMsg++;
        nBytes += bytes;
      }
    }

    if (nMsg >= nMsgMax || nBytes >= nBytesMax)
    {
    #if _ENABLE_CHEATS
      if (outputLogs) LogF("  Client: limit reached (guaranteed)");
    #endif
      goto DoNotSendUpdates;
    }

  #endif
  }

DoNotSendUpdates:
  // send nonguaranteed enqueued messages
  maxSize = MaxSizeNonguaranteed;
  int next = 0; // index of next object to update
  bool empty = false;
  while (true)
  {
    // enforce send last message
    if (!empty || _messageQueueNonGuaranteed.Size() <= 0)
    {
      if (nMsg >= nMsgMax || nBytes >= nBytesMax) break;
    }

    if (_messageQueueNonGuaranteed.Size() <= 0)
    {
      empty = true;
      // starving, try to create further message
      if (!PrepareNextUpdate(objects, next)) break;
      // local communication - message was already sent
      if (_parent->IsServer())
      {
        DoAssert(_messageQueueNonGuaranteed.Size() == 0);
        nMsg++;
        continue;
      }

      DoAssert(_messageQueueNonGuaranteed.Size() > 0);
    }
    
    NetworkMessageQueueItem &item = _messageQueueNonGuaranteed[0];
    int size = item.msg->size;
    // enforce send last message
    if (size >= maxSize || nMsg >= nMsgMax || nBytes >= nBytesMax)
    {
      DWORD dwMsgId = SendMsgRemote(TO_SERVER, item.msg, item.type, NMFStatsAlreadyDone);
#if LOG_SEND_PROCESS
      LogF("Client: Message %x sent", dwMsgId);
#endif
      if (item.msg->objectUpdateInfo)
      {
#if LOG_SEND_PROCESS
        LogF("  - update info %x updated", item.msg->objectUpdateInfo);
#endif
        if (item.msg->objectUpdateInfo->lastCreatedMsg != item.msg)
        {
          RptF("Last created message is bad:");
          RptF("  sending message: %x (type %s), id = %x", item.msg.GetRef(), NetworkMessageTypeNames[item.type], dwMsgId);
          RptF("	attached object info: %x", item.msg->objectUpdateInfo);
          RptF("	last created message: %x, id = %x", item.msg->objectUpdateInfo->lastCreatedMsg.GetRef(), item.msg->objectUpdateInfo->lastCreatedMsgId);
          RptF("	last sent message: %x", item.msg->objectUpdateInfo->lastSentMsg.GetRef());

          item.msg->objectUpdateInfo->lastCreatedMsg = item.msg;
        }
        item.msg->objectUpdateInfo->lastCreatedMsgId = dwMsgId;
        if (dwMsgId == 0xffffffff)
        {
          item.msg->objectUpdateInfo->lastCreatedMsg = NULL;
          item.msg->objectUpdateInfo->lastCreatedMsgTime = 0;
        }
      }
      _messageQueueNonGuaranteed.Delete(0);
      nMsg++;
      nBytes += size;
    }
    else
    {
      int totalSize = 0;
      int i;
      for (i=0; true; i++)
      {
        if (i >= _messageQueueNonGuaranteed.Size())
        {
          empty = true;
          // starving, try to create further message
          if (!PrepareNextUpdate(objects, next)) break;
          DoAssert(!_parent->IsServer());
          DoAssert(i < _messageQueueNonGuaranteed.Size());
        }
        
        NetworkMessageQueueItem &item = _messageQueueNonGuaranteed[i];
        int size = item.msg->size;
        if (totalSize + size > maxSize) break;
        totalSize += size;
      }
      DWORD dwMsgId = SendMsgQueue(TO_SERVER, _messageQueueNonGuaranteed, 0, i, NMFNone);
#if LOG_SEND_PROCESS
      LogF("Client: Message %x sent", dwMsgId);
#endif
      for (int j=0; j<i; j++)
      {
        NetworkMessageQueueItem &item = _messageQueueNonGuaranteed[j];
        if (item.msg->objectUpdateInfo)
        {
#if LOG_SEND_PROCESS
          LogF("  - update info %x updated", item.msg->objectUpdateInfo);
#endif
          if (item.msg->objectUpdateInfo->lastCreatedMsg != item.msg)
          {
            RptF("Last created message is bad:");
            RptF("  sending message: %x (type %s), id = %x", item.msg.GetRef(), NetworkMessageTypeNames[item.type], dwMsgId);
            RptF("	attached object info: %x", item.msg->objectUpdateInfo);
            RptF("	last created message: %x, id = %x", item.msg->objectUpdateInfo->lastCreatedMsg.GetRef(), item.msg->objectUpdateInfo->lastCreatedMsgId);
            RptF("	last sent message: %x", item.msg->objectUpdateInfo->lastSentMsg.GetRef());

            item.msg->objectUpdateInfo->lastCreatedMsg = item.msg;
          }
          item.msg->objectUpdateInfo->lastCreatedMsgId = dwMsgId;
          if (dwMsgId == 0xffffffff)
          {
            item.msg->objectUpdateInfo->lastCreatedMsg = NULL;
            item.msg->objectUpdateInfo->lastCreatedMsgTime = 0;
          }
        }
      }
      _messageQueueNonGuaranteed.Delete(0, i);
      nMsg++;
      nBytes += totalSize;
    }
  }
  DoAssert(!empty || _messageQueueNonGuaranteed.Size() == 0);

//  int sumError = 0; // messages not sent total error
  if (next < objects.Size())
  {
  #if _ENABLE_CHEATS
    if (outputLogs) LogF("  Client: limit reached");
  #endif
/*
    for (int i=next; i<objects.Size(); i++)
    {
      if (objects[i].error < FLT_MAX)
      {
        sumError += objects[i].error;
      }
    }
*/
  }

#if CHECK_MSG
  CheckLocalObjects();
#endif

#if _ENABLE_CHEATS
  // remove statistics info
  static AutoArray<int> texts;
  for (int i=0; i<texts.Size(); i++)
  {
    if (texts[i] >= 0) GEngine->RemoveText(texts[i]);
  }
  texts.Resize(0);

  // create statistics
  if (outputDiags == 1)
  {
    int count = _messageQueue.Size(), size = 0;
    for (int i=0; i<count; i++) size += _messageQueue[i].msg->size;

    RString output =
      Format("Server: HLWait%3d(%5dB) ", count, size) +
      _client->GetStatistics();

    texts.Add(GEngine->ShowTextF(1000, 10, 15, output));
  }
#endif
}

bool NetworkClient::PrepareNextUpdate(AutoArray<UpdateLocalObjectInfo, MemAllocSA> &objects, int &next)
{
  while (next < objects.Size())
  {
    UpdateLocalObjectInfo &info = objects[next++];
    if (info.criticalUpdate) continue;

/*
    #if _ENABLE_REPORT
    int oldCount = _messageQueueNonGuaranteed.Size();
    #endif
*/

    NetworkLocalObjectInfo &oInfo = *info.oInfo;
    NetworkMessageClass cls = info.cls;
    int bytes = UpdateObject(oInfo.object, cls, NMFNone);
    if (bytes < 0) continue;

/*
    #if _ENABLE_REPORT
    if (oldCount == _messageQueueNonGuaranteed.Size())
    {
      Fail("No message added during NetworkClient::PrepareNextUpdate");
      continue;
    }
    #endif
*/
  #if _ENABLE_CHEATS
    if (outputLogs) LogF("  Client: Object updated - size %d bytes", bytes);
  #endif
    return true;
  }
  return false;
}

void NetworkClient::Respawn(Person *soldier, Vector3Par pos)
{
  if (_state < NGSPlay) return; // send event messages only when playing

  // add to respawn queue
  AIUnit *unit = soldier->Brain();
  Assert(unit);
  AIGroup *grp = unit->GetGroup();
  Assert(grp);
  if (!grp) Fail("Respawning unit with no group");

  int index = _respawnQueue.Add();
  RespawnQueueItem &item = _respawnQueue[index];
  item.person = soldier;
  item.position = pos;
  item.time = Glob.time + GetRespawnDelay();
  item.player = soldier == GWorld->GetRealPlayer();
/*
  item.type = soldier->GetType();
  item.side = soldier->Vehicle::GetTargetSide();
  item.varname = soldier->GetVarName();
  item.info = soldier->GetInfo();
  item.player = soldier == GWorld->GetRealPlayer() ? _player : 0;
  item.group = grp;
  item.position = pos;
  item.time = Glob.time + GetRespawnDelay();
  item.id = -1;
  for (int i=0; i<vehiclesMap.Size(); i++)
    if (vehiclesMap[i] == soldier)
    {
      item.id = i;
      break;
    }
*/
}

int NetworkClient::NLocalObjectsNULL() const
{
  int n = 0;
  for (int i=0; i<_localObjects.Size(); i++)
  {
    const NetworkLocalObjectInfo &info = _localObjects[i];
    if (!info.object) n++;
  }
  return n;
}

float NetworkClient::GetLastMsgAgeReliable()
{
  if (_client) return _client->GetLastMsgAgeReliable();
  return 0;
}
