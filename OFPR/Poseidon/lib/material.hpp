#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MATERIAL_HPP
#define _MATERIAL_HPP

#include "textbank.hpp"
#include "tlVertex.hpp"

//! general surface material properties
/*!
contains texture and other material properties.
Primary use for this structure is in ShapeSection
*/

class TexMaterial: public RefCountWithLinks
{
	public:
	//! material name
	RStringB _name;

	//! texture - is also stored in PolyProperties
	Ref<Texture> _tex;

	//! emmisive color - used for shining polygons
	Color _emmisive;
	//! ambient color factor
	Color _ambient;
	//! diffuse color factor
	Color _diffuse;
	//! forced diffuse color factor - used to simulate half or fully lighted polys
	Color _forcedDiffuse;
	//! specular color factor
	Color _specular;
	// specular power - higher value means sharper highlight
	// value zero means no highlight
	float _specularPower;

	//! bump normal map
	Ref<Texture> _bumpmap;
	//! bump map coordinate factor
	float _bumpmapFactor;

	//! detail map
	Ref<Texture> _detailmap;
	//! detail map coordinate factor
	float _detailmapFactor;

	//! constructor
	TexMaterial();
	//! constructor
	TexMaterial(const char *name);
	//! combine this material with TLMaterial
	void Combine(TLMaterial &dst, const TLMaterial &src);
	//! get name (necessary for BankArray)
	const RStringB &GetName() const {return _name;}
};

#include <Es/Containers/bankArray.hpp>

// material bank

template <>
struct BankTraits<TexMaterial>
{
	typedef const char *NameType;
	// default name comparison
	static int CompareNames( NameType n1, NameType n2 )
	{
		return strcmpi(n1,n2);
	}
	typedef LinkArray<TexMaterial> ContainerType;
};

//! material bank

/*!
Result of TexMaterialBank::New or TextureToMaterial should be assigned
to Ref<TexMaterialBank>.
*/

class TexMaterialBank: public BankArray<TexMaterial>
{
	public:
	TexMaterialBank();
	~TexMaterialBank();

	//! handling of old-style texture-driven materials
	TexMaterial *TextureToMaterial(Texture *tex);
};

// TLMaterial

extern TexMaterialBank GTexMaterialBank;

#endif
