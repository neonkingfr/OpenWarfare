// Poseidon - Transformation and lighting
#include "wpch.hpp"

#include "tlVertex.hpp"
#include "camera.hpp"
#include "global.hpp"
#include "world.hpp"
#include "scene.hpp"
#include "lights.hpp"
#include "landscape.hpp"
#include "engine.hpp"
#include <El/Common/perfLog.hpp>
#include "keyInput.hpp"
#include "dikCodes.h"

#include "SpecLods.hpp"

#if _KNI
	#define PrefetchTemp(a,i) _mm_prefetch((char *)&(a[i]),_MM_HINT_NTA)
	#define Prefetch(a,i) _mm_prefetch((char *)&(a[i]),_MM_HINT_T0)
#else
	#define PrefetchTemp(a,i)
	#define Prefetch(a,i)
#endif

#if USE_QUADS
	static StaticStorage<V3Quad> PosTransQStorage;
#endif

static StaticStorage<Vector3> PosTransStorage;

static StaticStorage<TLVertex> TLVertexStorage;

static StaticStorage<ClipFlags> ClipStorage;

void TLVertexTable::DoConstruct()
{
	const int MaxBuf=4*1024;
	#if USE_QUADS
		#if _RELEASE
		_posTransQ.SetStorage(PosTransQStorage.Init(MaxBuf/4));
		#endif
		_useQuads = false;
	#endif
	_posTrans.SetStorage(PosTransStorage.Init(MaxBuf));

	_vert.SetStorage(TLVertexStorage.Init(MaxBuf));
	_clip.SetStorage(ClipStorage.Init(MaxBuf));
	
	_orHints=ClipHints; // assume worst case
	_andHints=0;
}

TLVertexTable::TLVertexTable()
{
	DoConstruct();
}

void TLVertexTable::Init
(
	const IAnimator *anim, const Shape &src, const Matrix4 &toView
)
{
	DoTransformPoints(anim,src,toView);
}

TLVertexTable::TLVertexTable
(
	const IAnimator *anim,
	const Shape &src, const Matrix4 &toView
)
{
	DoConstruct();
	DoTransformPoints(anim,src,toView);
}

void TLVertexTable::ReleaseTables()
{
	_clip.Clear();
	_posTrans.Clear();
	_vert.Clear();
}

int TLVertexTable::AddPos()
{
	// used for clipping
	// interpolation result must be stored somewhere
	#if USE_QUADS
	if (_useQuads)
	{
		Assert(_posTransQ.Size()==_vert.Size());
		int index=_vert.Size();
		int newSize=index+1;
		_posTransQ.Resize(newSize);
		_vert.Resize(newSize);
		_clip.Resize(newSize);
		return index;
	}
	else
	#endif
	{
		Assert (_posTrans.Size()==_vert.Size());
		int index=_vert.Size();
		int newSize=index+1;
		_posTrans.Resize(newSize);
		_vert.Resize(newSize);
		_clip.Resize(newSize);
		return index;
	}
}


void TLVertexTable::DoTransformPoints
(	
	const VertexTable &src, const Matrix4 &posView, int beg, int end
)
{
	// this is the most common case
	// simple mesh with no decal polygons
	// TODO: most common case -> pass processing to ITransformer
	
	#if USE_QUADS
	if (_useQuads)
	{
		// TODO: quad 
		src._posQ.Transform(_posTransQ.QuadData(),posView,beg,end);
	}
	else
	#endif
	{
		int size=end-beg;
		Vector3 *d = _posTrans.Data()+beg;
		const Vector3 *s = src._pos.Data()+beg;
		while( --size>=0 ) (*d++).SetMultiply(posView,*s++);
	}	
}

void TLVertexTable::DoTransformPoints
(
	const IAnimator *anim,
	const Shape &src, const Matrix4 &posView
)
{
	int i;
	_orHints=src._orHints;
	_andHints=src._andHints;
	int nPos = src._clip.Size();
	_clip.Realloc(nPos);_clip.Resize(nPos);
	_vert.Realloc(nPos),_vert.Resize(nPos);
	CopyData(_clip.Data(),src._clip.Data(),nPos);
	if( (_orHints&ClipDecalMask)==0 )
	{

		#if USE_QUADS
		extern bool EnablePIII;
		static bool UseQuads = true;
		if (EnablePIII)
		{
#if _ENABLE_CHEATS
			if (GInput.GetCheat2ToDo(DIK_S))
			{
				UseQuads = !UseQuads;
				GlobalShowMessage(500,"SSE T&L %s",UseQuads ? "On":"Off");
			}
#endif
		}
		if (src._posQ.QuadData() && UseQuads)
		{
			_useQuads = true;
			_posTransQ.Realloc(src._posQ.Size());
			_posTransQ.Resize(src._posQ.Size());
		}
		else
		#endif
		{
		#if USE_QUADS
			_useQuads = false;
		#endif
			_posTrans.Realloc(src._pos.Size());
			_posTrans.Resize(src._pos.Size());
		}

		anim->DoTransform(*this,src,posView,0,nPos);
		return;
	}

	#if USE_QUADS
	_useQuads = false;
	#endif
	_posTrans.Realloc(src._pos.Size());
	_posTrans.Resize(src._pos.Size());

	if
	(
		(_orHints&ClipDecalMask)==ClipDecalVertical &&
		(_andHints&ClipDecalMask)==ClipDecalVertical
	)
	{
		// second quite often - whole object is vertical decal (used for trees)
		Matrix4 vDecal(MZero);
		// we want to use only vertical part of rotation
		vDecal.SetUpAndAside(posView.DirectionUp(),VAside);
		vDecal=vDecal*GLOB_SCENE->GetCamera()->ScaleMatrix();
		// decal transformation must have same scale and position as object
		vDecal.SetPosition(posView.Position());
		vDecal.SetScale(posView.Scale());
		//vDecal.SetOrientation(vDecal.Orientation()*Matrix3(MScale,Scale()));
		int size=_posTrans.Size();
		for( i=0; i<size; i++ )
		{
			const V3 &pos=src._pos[i];
			// we want to use only vertical part of rotation
			_posTrans[i].SetFastTransform(vDecal,pos);
		}
	}
	else if
	(
		(_orHints&ClipDecalMask)==ClipDecalNormal &&
		(_andHints&ClipDecalMask)==ClipDecalNormal
	)
	{
		// whole object is decal
		Matrix4 decal=GLOB_SCENE->GetCamera()->ScaleMatrix();
		decal.SetPosition(posView.Position());
		decal.SetScale(posView.Scale());
		int size=_posTrans.Size();
		for( i=0; i<size; i++ )
		{
			const V3 &pos=src._pos[i];
			// we want to use only vertical part of rotation
			_posTrans[i].SetFastTransform(decal,pos);
		}
	}
	else
	{
		// TODO: remove or optimize mixed models
		// non-decal part must be handles by anim
		// general case: not very often
		// mixed decals support removed
		Matrix4 decal(NoInit);
		Matrix4 vDecal(NoInit);
		bool vDecalSet=false;
		bool decalSet=false;
		int size=_posTrans.Size();
		for( i=0; i<size; i++ )
		{
			const V3 &pos=src._pos[i];
			ClipFlags clip=src._clip[i];
			V3 &dPos=_posTrans[i];
			switch( clip&ClipDecalMask )
			{
				default:
					dPos.SetFastTransform(posView,pos);
				break;
				case ClipDecalNormal:
					if( !decalSet )
					{
						decal=GLOB_SCENE->GetCamera()->ScaleMatrix();
						decal.SetPosition(posView.Position());
						decal.SetScale(posView.Scale());
						decalSet=true;
					}
					dPos.SetFastTransform(decal,pos);
				break;
				case ClipDecalVertical:
					// we want to use only vertical part of rotation
					// this can be achieved by setting matrix
					if( !vDecalSet )
					{
						//vDecal=Matrix4(MZero);
						vDecal.SetUpAndAside(posView.DirectionUp(),VAside);
						vDecal.SetPosition(VZero);
						vDecal=vDecal*GLOB_SCENE->GetCamera()->ScaleMatrix();
						vDecal.SetPosition(posView.Position());
						vDecal.SetScale(posView.Scale());
						vDecalSet=true;
					}
					dPos.SetFastTransform(vDecal,pos);
				break;
			}
		}
	}
}

inline void VerifyClip
(
	TLVertex *d, float minX, float maxX, float minY, float maxY
)
{
	#if 0
		const float eps = 0.01;
		if (d->pos[0]<minX-eps || d->pos[0]>maxX+eps)
		{
			LogF("Bad x coord %.2f (%.0f..%.0f)",d->pos[0],minX,maxX);
			if (d->pos[0]<minX-5 || d->pos[0]>maxX+5)
			{
				LogF(" *** clipping error");
			}
		}
		if (d->pos[1]<minY-eps || d->pos[1]>maxY+eps)
		{
			LogF("Bad y coord %.2f (%.0f..%.0f)",d->pos[1],minY,maxY);
			if (d->pos[1]<minY-5 || d->pos[1]>maxY+5)
			{
				LogF(" *** clipping error");
			}
		}
	#endif
	#if 1
		saturate(d->pos[0],minX,maxX);
		saturate(d->pos[1],minY,maxY);
	#endif
}

void TLVertexTable::DoPerspective( const Camera &camera, ClipFlags clip )
{
	#if USE_QUADS
	if (_useQuads)
	{
		// clip flags may become invalid after this operation?
		_posTransQ.Perspective(_vert.Data(),camera.Projection());
	}
	else
	#endif
	{

		float minX = GEngine->MinSatX(), maxX = GEngine->MaxSatX();
		float minY = GEngine->MinSatY(), maxY = GEngine->MaxSatY();
		// 
		const Matrix4 *mc = &camera.Projection();
		#define ADJUST_MAT 1
		#if !ADJUST_MAT
		float zCoef = 1;
		if (!GEngine->CanZBias())
		{
			int zBias = GEngine->GetBias();
			zCoef = 1.0f-zBias*1e-6f;
		}
		#else
		Matrix4 mcAdjusted;
		if (!GEngine->CanZBias())
		{
			int zBias = GEngine->GetBias();
			if (zBias>0)
			{
				float zMult=1, zAdd=0;
				GEngine->GetZCoefs(zAdd,zMult);

				mcAdjusted = *mc;

				mcAdjusted(2,2) = mcAdjusted(2,2)*zMult+zAdd;
				mcAdjusted.SetPosition
				(
					Vector3
					(
						mcAdjusted.Position().X(),
						mcAdjusted.Position().Y(),
						mcAdjusted.Position().Z()*zMult
					)
				);

				mc = &mcAdjusted;
			}
		}
		#endif

		int size=_posTrans.Size();
		TLVertex *d=_vert.Data();
		const Vector3 *s=_posTrans.Data();
		// many meshes are not clipped at all - skip check
		if( (clip&ClipAll)==0 )
		{
			while( --size>=0 )
			{
				#if _KNI || _VEC4 //|| _T_MATH
				// TODO: better t-math support
				Vector3 dt;
				float rhw = dt.SetPerspectiveProject(*mc,*s);
				(*d).pos[0] = dt[0];
				(*d).pos[1] = dt[1];
				(*d).pos[2] = dt[2];
				#else
				float rhw = (*d).pos.SetPerspectiveProject(*mc,*s);
				#endif
				d->rhw = rhw;
				// TODO: skip saturate if not neccessary here
				VerifyClip(d,minX,maxX,minY,maxY);
				/*
				saturate(d->pos[0],minX,maxX);
				saturate(d->pos[1],minY,maxY);
				*/
				#if !ADJUST_MAT
					d->pos[2] *= zCoef;
				#endif
				d++,s++;
				// check z bias coef
			}
		}
		else
		{
			const ClipFlags *c=_clip.Data();
			while( --size>=0 )
			{
				if( ((*c)&ClipAll)==ClipNone )
				{
					#if _KNI || _VEC4 //|| _T_MATH
					Vector3 dt;
					float rhw = dt.SetPerspectiveProject(*mc,*s);
					(*d).pos[0] = dt[0];
					(*d).pos[1] = dt[1];
					(*d).pos[2] = dt[2];
					#else
					float rhw = (*d).pos.SetPerspectiveProject(*mc,*s);
					#endif
					d->rhw = rhw;
					// TODO: skip saturate if not neccessary here
					VerifyClip(d,minX,maxX,minY,maxY);
					/*
					saturate(d->pos[0],minX,maxX);
					saturate(d->pos[1],minY,maxY);
					*/
					#if !ADJUST_MAT
						d->pos[2] *= zCoef;
					#endif
				}
				c++,d++,s++;
			}
		}
	}
}




// ligting and fogging
inline float ConstantFog( int spec )
{
	if( (spec&(FogDisabled|IsAlphaFog))==FogDisabled )
	{
		return 0;
	}
	else
	{
		return GScene->GetConstantFog();
	}
}

inline int ConstantFogInt( int spec )
{
	if( (spec&(FogDisabled|IsAlphaFog))==FogDisabled )
	{
		return 0;
	}
	else
	{
		return toIntFloor(GScene->GetConstantFog()*255);
	}
}



const float StartLights=0.01; // NightEffect when lights start to be visible

#define TL_COUNTERS 0

#define DP(a,b) ((a).X()*(b).X()+(a).Y()*(b).Y()+(a).Z()*(b).Z())
#define SIZE2(x,y,z) (Square(x)+Square(y)+Square(z))

inline PackedColor PackedColor255( ColorVal color, int a=255 )
{
	int r=toInt(color.R());
	int g=toInt(color.G());
	int b=toInt(color.B());
	saturate(r,0,255);
	saturate(g,0,255);
	saturate(b,0,255);
	return PackedColor((a<<24)|(r<<16)|(g<<8)|b);
}

void TLVertexTable::DoMaterialLightingP
(
	const TLMaterial &mat,
	const Matrix4 &worldToModel, const LightList &lights,
	const VertexTable &mesh, int beg, int end
)
{
	if( end<=beg ) return; // safety: nothing to light

	#if 1 //TL_COUNTERS
		ADD_COUNTER(TLMat,end-beg);
	#endif
	// perform lighting
	// first of all apply directional light to all normals
	// this can be done at TLVertexTable level
	LightSun *sun=GScene->MainLight();
	float addLightsFactor=sun->NightEffect();
	if ((mat.specFlags&DisableSun)==0)
	{
		sun->SetMaterial(mat);
		if (lights.Size()>0)
		{
			TLMaterial temp; //=mat;
			temp.ambient = mat.ambient*addLightsFactor;
			temp.diffuse = mat.diffuse*addLightsFactor;
			temp.specFlags = mat.specFlags;
			temp.emmisive = mat.emmisive;
			temp.forcedDiffuse = mat.forcedDiffuse*addLightsFactor;
			//temp.specular = mat.saddLightsFactor;
			for( int index=0; index<lights.Size(); index++ )
			{
				lights[index]->SetMaterial(temp);
				lights[index]->Prepare(worldToModel);
			}
		}
	}
	else
	{
		addLightsFactor = 1;

		TLMaterial temp;
		temp.ambient = HBlack;
		temp.diffuse = HBlack;
		temp.emmisive = HBlack;
		temp.forcedDiffuse = HBlack;
		temp.specFlags = mat.specFlags;

		sun->SetMaterial(temp);
		if (lights.Size()>0)
		{
			for( int index=0; index<lights.Size(); index++ )
			{
				lights[index]->SetMaterial(mat);
				lights[index]->Prepare(worldToModel);
			}
		}
	}
	
	const Camera *camera = GScene->GetCamera();
	Matrix4Val invScale=camera->InvScaleMatrix();

	// normal lighting
	// for all vertices in mesh calculate positional lights and fog
	// check for special case: no lights
	bool someLights=( addLightsFactor>=StartLights && lights.Size()>0 );
	// assume dammage value is constant over whole object
	Vector3 sunDirection=worldToModel.Rotate(sun->Direction());
	sunDirection.Normalize();

	// TODO: use material properties
	Color diffuse=sun->DiffusePrecalc();
	Color ambient=sun->AmbientPrecalc()+mat.emmisive;

	int spec = mat.specFlags;
	int aFactor = 0x100;
	if (spec&IsColored)
	{
		aFactor = toInt(GScene->GetConstantColor().A()*0x100);
		saturate(aFactor,0,0x100);
	}
		
	int fogValue=ConstantFogInt(spec);
	if( !someLights )
	{
		if( fogValue>=0 )
		{
			PackedColor packedSpecular; // HW fog
			PackedColor packedAmbient;
			if( spec&IsAlphaFog )
			{
				// consider alpha from constant color
				fogValue=0xff-fogValue; // IsAlphaFog
				fogValue = (fogValue*aFactor)>>8;
				packedAmbient=PackedColorRGB(ambient,fogValue);
				packedSpecular = PackedBlack;
			}
			else
			{
				packedAmbient=PackedColorRGB(ambient,0xff);
				packedSpecular=PackedColorRGB(PackedBlack,0xff-fogValue);
				fogValue=0xff;
			}
			
			#if TL_COUNTERS
				ADD_COUNTER(TLNor,end-beg);
			#endif
			// near objects do not need any fog
			// perform DirectX Lighting


			TLVertex *v=VertexData()+beg;
			const Vector3 *norm=&mesh.Norm(beg);
			const UVPair *tex=mesh._tex.Data()+beg;

			ambient = ambient*255; // pre-multiply to correct scale
			diffuse = diffuse*255;
			for( int i=end-beg; --i>=0; )
			{
				#if _KNI
					Coord cosFi=(*norm).DotProduct(sunDirection);
					saturateMax(cosFi,0);
					_mm_prefetch((char *)(norm+4),_MM_HINT_NTA);
					Color color = ambient+diffuse*cosFi;
					v->color=PackedColor255(color,fogValue);
				#elif __ICL
					Coord cosFi=*norm * sunDirection;
					saturateMax(cosFi,0);
					Color color = ambient+diffuse*cosFi;
					v->color=PackedColor255(color,fogValue);
				#else
					Coord cosFi=*norm * sunDirection;
					if (cosFi>0)
					{
						Color color = ambient+diffuse*cosFi;
						v->color=PackedColor255(color,fogValue);
					}
					else
					{
						v->color=packedAmbient;
					}
				#endif
				v->specular = packedSpecular;
				v->t0 = *tex;
				norm++;
				v++;
				tex++;
			}
		}
		else
		{
			PackedColor packedAmbient=PackedColor(ambient);
			#if TL_COUNTERS
				ADD_COUNTER(TLNof,end-beg);
			#endif
			// calculate per vertex fog
			//int n=NVertex();
			const UVPair *tex = mesh._tex.Data();
			for( int i=beg; i<end; i++ )
			{
				TLVertex &v=SetVertex(i);
				//ClipFlags clip=Clip(i);
				Vector3Val scalePos=TransPos(i);
				float dist2 = SIZE2
				(
					scalePos.X()*invScale(0,0),scalePos.Y()*invScale(1,1),scalePos.Z()
				);
				int fog=GScene->Fog8(dist2);
				// alpha is assumed 1 - this is always true for normal lighting
				PackedColor specular;
				if (spec&IsAlphaFog)
				{
					fog=0xff-fog; // IsAlphaFog
					fog = (fog*aFactor)>>8;
					specular = PackedBlack;
				}
				else
				{
					specular = PackedColorRGB(PackedBlack,0xff-fog);
					fog=0xff;
				}

				#if __ICL
					Coord cosFi=sunDirection*mesh.Norm(i);
					saturateMax(cosFi,0);
					Color color = ambient+diffuse*cosFi;
					v.color=PackedColorRGB(color,fog);
				#else
					Coord cosFi=sunDirection*mesh.Norm(i);
					if (cosFi>0)
					{
						Color color = ambient+diffuse*cosFi;
						v.color=PackedColorRGB(color,fog);
					}
					else
					{
						v.color=PackedColorRGB(packedAmbient,fog);
					}
				#endif
				v.specular=specular;
				v.t0 = tex[i];
				// check if there are not some unsupported lighting flags
			}
		}
	}
	else
	{
		#if TL_COUNTERS
			ADD_COUNTER(TLNoL,end-beg);
		#endif

		const UVPair *tex = mesh._tex.Data();
		for( int i=beg; i<end; i++ )
		{
			TLVertex &v=SetVertex(i);
			Coord cosFi=sunDirection*mesh.Norm(i);
			Color colorI = ambient;
			#if __ICL
			saturateMax(cosFi,0);
			colorI += diffuse*cosFi;
			#else
			if (cosFi>0)
			{
				colorI += diffuse*cosFi;
			}
			#endif

			Vector3Val norm=mesh.Norm(i);
			Vector3Val pos=mesh.Pos(i);
			for( int index=0; index<lights.Size(); index++ )
			{
				colorI+=lights[index]->Apply(pos,norm);
			}
			// lighting with normal defined
			// check if there are not some unsupported lighting flags
			int fog=fogValue;
			if (fogValue<0)
			{
				Vector3Val scalePos=TransPos(i);
				float dist2 = SIZE2
				(
					scalePos.X()*invScale(0,0),scalePos.Y()*invScale(1,1),scalePos.Z()
				);
				fog =GScene->Fog8(dist2);
			}

			PackedColor specular;
			if (spec&IsAlphaFog)
			{
				fog=0xff-fog; // IsAlphaFog
				fog = (fog*aFactor)>>8;
				v.color=PackedColorRGB(colorI,fog);
				v.specular = PackedBlack;
			}
			else
			{
				v.color=PackedColorRGB(colorI,0xff);
				v.specular = PackedColorRGB(PackedBlack,0xff-fog);
			}
			v.t0 = tex[i];

			// alpha is assumed 1 - this is always true for normal lighting
		}
	}
}

void TLVertexTable::DoSkyLighting
(
	const VertexTable &mesh, int spec
)
{
	LightSun *sun=GScene->MainLight();
	Vector3Val sunDir=sun->SunDirection();
	//float addLightsFactor=GScene->MainLight()->NightEffect();
	int i;
	// for all vertices in mesh calculate positional lights and fog
	Color accom=GEngine->GetAccomodateEye();
	const int size=NVertex();
	const UVPair *tex = mesh._tex.Data();
	Assert (_posTrans.Size()==size);
	for( i=0; i<size; i++ )
	{
		TLVertex &v=SetVertex(i);
		//Vector3Val scalePos=TransPos(i);

		// there is very light area around the sun
		// this could be simulated with specular highlights
		Vector3Val direction=mesh.Norm(i);
		float cosSun=-sunDir*direction;
		saturateMax(cosSun,0);
		float cosSun2=cosSun*cosSun;
		float cosSun4=cosSun2*cosSun2;
		//float cosSun8=cosSun4*cosSun4;
		float sunCoef=cosSun4*cosSun2;
		#if 1
			Color colorI=sun->SunSkyColor()*sunCoef+sun->SkyColor();
		#else
			// debugging: orange sky
			Color colorI(1,0.5,0);
		#endif
		
		Assert( spec&IsAlphaFog );
		v.color=PackedColorRGB(colorI*accom,255);
		v.specular=PackedBlack;
		v.t0 = tex[i];
	}
}

void TLVertexTable::DoStarLighting
(
	const Matrix4 &worldToModel, const VertexTable &mesh, int spec
)
{
	LightSun *sun=GScene->MainLight();
	// perform lighting
	// first of all apply directional light to all normals
	// this can be done at TLVertexTable level
	//
	int i;
	Landscape *land=GScene->GetLandscape();
	// calculate common alpha correction
	float alphaCorrection=
	(
		// overcast limitation
		(1.5*land->SkyThrough()-0.5)*
		// daytime limitation
		sun->StarsVisibility()*
		// resolution correction
		(GEngine->Width()*GEngine->Height())*(1.0/(640*480))*
		// zoom correction
		GScene->GetCamera()->InvLeft()
	);

	Vector3 sunDirection=worldToModel.Rotate(sun->Direction());
	sunDirection.Normalize();

	Color accom=GEngine->GetAccomodateEye();
	const int size=NVertex();
	const UVPair *tex = mesh._tex.Data();
	for( i=0; i<size; i++ )
	{
		TLVertex &v=SetVertex(i);

		// calculate angle from sun
		Vector3Val sPos=mesh.Pos(i);
		float cosSun=-sPos*sunDirection;
		if( cosSun>0 )
		{
			// TODO: following Size is invariant
			//(starPos is spherical, SunDirection is normalized)
			float invSize=sPos.InvSize();
			cosSun=cosSun*invSize;
			float cosSun2=cosSun*cosSun;
			cosSun=cosSun2*cosSun2;
		}
		else cosSun=0;
		// use different brightness for different stars
		ClipFlags clip=Clip(i);

		int user=clip&ClipUserMask;
		float brightness=user*(1.0/MaxUserValue/ClipUserStep);
		// note: point brightness is calculated for resolution 640x480
		// correct it for finer resolutions
		float a=alphaCorrection*brightness*(1-cosSun);
		saturate(a,0,1);
		accom.SetA(a);
		v.color=PackedColor(accom);
		v.specular=PackedBlack;
		v.t0 = tex[i];
	}
}

void TLVertexTable::DoLineLighting
(
	const VertexTable &mesh, int spec
)
{
	//LightSun *sun=GScene->MainLight();
	// perform lighting
	// first of all apply directional light to all normals
	// this can be done at TLVertexTable level
	//
	int i;
	Matrix4Val invScale=GScene->GetCamera()->InvScaleMatrix();
	//Landscape *land=GScene->GetLandscape();
	// calculate common alpha correction
	float alphaCorrection=
	(
		// resolution correction
		GEngine->Width()*(1.0/640)*
		// zoom correction
		GScene->GetCamera()->InvLeft()
	);

	Color lineColor=GScene->GetConstantColor();
	float width=lineColor.A(); // width (in m?)

	Assert( (GetAndHints()&ClipLightMask)==ClipLightLine );

	ColorVal accom=GEngine->GetAccomodateEye();
	const int size=NVertex();
	const UVPair *tex = mesh._tex.Data();
	Assert (_posTrans.Size()==size);
	for( i=0; i<size; i++ )
	{
		TLVertex &v=SetVertex(i);
		Vector3Val scalePos=TransPos(i);
		float dist2 = SIZE2
		(
			scalePos.X()*invScale(0,0),scalePos.Y()*invScale(1,1),scalePos.Z()
		);
		// scale alpha with distance
		// correct it for finer resolutions
		float invCoef = 100.0/3;
		if (dist2>Square(3))
		{
			invCoef = 100*InvSqrt(dist2);
		}
		float a=alphaCorrection*width*invCoef;
		saturate(a,0,1);
		lineColor.SetA(a);
		//LogF("Line point %.2f,%.2f - alpha %.2f, width %.2f",v.pos.X(),v.pos.Y(),a,width);
		v.color=PackedColor(lineColor*accom);
		v.specular=PackedBlack;
		v.t0 = tex[i];
	}
}

void TLVertexTable::DoCloudLighting
(
	const VertexTable &mesh, int spec
)
{
	Landscape *land=GLandscape;
	Matrix4Val invScale=GScene->GetCamera()->InvScaleMatrix();
	LightSun *sun=GScene->MainLight();
	ColorVal accom=GEngine->GetAccomodateEye();
	// for all vertices in mesh calculate positional lights and fog
	Color colorI;
	float factor=1.0,alpha=1.0;
	if( land ) alpha=land->CloudsAlpha(),factor=land->CloudsBrightness();
	TLMaterial mat;
	mat.diffuse = accom;
	mat.ambient = accom;
	mat.emmisive = HBlack;
	mat.forcedDiffuse = HBlack;
	mat.specFlags = 0;
	sun->SetMaterial(mat);
	colorI=sun->FullResult(1.0)*factor;
	colorI.SetA(alpha);
	colorI=colorI*accom;
	PackedColor color(colorI);
	Assert( ( GetAndHints()&ClipFogMask)==ClipFogSky );
	const int size=NVertex();
	const UVPair *tex = mesh._tex.Data();
	Assert (_posTrans.Size()==size);


	float fog = ConstantFog(spec);
	bool constFog = ( fog>=0 );

	Assert( spec&IsAlphaFog );
	if (constFog)
	{
		int colorAInt = toIntFloor((1-fog)*colorI.A()*255);
		saturate(colorAInt,0,255);
		color.SetA8(colorAInt);
		//LogF("cloud cfog %.2f",fog);
		for( int i=0; i<size; i++ )
		{
			TLVertex &v=SetVertex(i);
			v.color=color;
			v.specular=PackedBlack;
			v.t0=tex[i];
		}
	}
	else
	{
		for( int i=0; i<size; i++ )
		{
			TLVertex &v=SetVertex(i);
			Vector3Val scalePos=TransPos(i);
			float dist2 = SIZE2
			(
				scalePos.X()*invScale(0,0),scalePos.Y()*invScale(1,1),scalePos.Z()
			);

			float fog=GScene->SkyFog8(dist2)*(1.0/255);


			float a = (1-fog)*colorI.A();
			int aInt = toIntFloor(a*255);
			saturate(aInt,0,255);
			v.color=PackedColorRGB(color,aInt);
			v.specular=PackedBlack;
			v.t0=tex[i];

			//LogF("cloud dfog %.2f, a %d",fog,aInt);
		}
	}
}

void TLVertexTable::DoShadowLighting
(
	const VertexTable &mesh, int spec
)
{
	Matrix4Val invScale=GScene->GetCamera()->InvScaleMatrix();
	//Landscape *land=GScene->GetLandscape();
	// shadows need not be lighted
	int shadowFactorI = GEngine->GetShadowFactor();
	/*
	float addLightsFactor=GScene->MainLight()->NightEffect();
	float skyCoef=floatMin(GScene->GetLandscape()->SkyThrough(),0.6);
	float shadowFactor=skyCoef*0.3+0.1;
	int shadowFactorI=toIntFloor(shadowFactor*(1-addLightsFactor)*255);
	*/
	const int size=NVertex();
	Assert (_posTrans.Size()==size);
	const UVPair *tex = mesh._tex.Data();
	for( int i=0; i<size; i++ )
	{
		TLVertex &v=SetVertex(i);
		//ClipFlags clip=Clip(i);
		Vector3Val scalePos=TransPos(i);
		float dist2 = SIZE2
		(
			scalePos.X()*invScale(0,0),scalePos.Y()*invScale(1,1),scalePos.Z()
		);
		int fog=GScene->ShadowFog8(dist2);
		// fog==1 means invisible
		int a=(shadowFactorI*(255-fog))>>8; // a==0 means invisible
		saturate(a,0,255);
		v.color=PackedColorRGB(PackedBlack,a); // shadow is black
		v.specular=PackedBlack;
		v.t0=tex[i];
	}
}

void TLVertexTable::DoLightLighting
(
	const VertexTable &mesh, int spec
)
{
	float addLightsFactor=GScene->MainLight()->NightEffect();
	Matrix4Val invScale=GScene->GetCamera()->InvScaleMatrix();
	int i;
	Color accom=GEngine->GetAccomodateEye();
	if( spec&IsColored )
	{
		accom=accom*GScene->GetConstantColor();
	}
	PackedColor lColor(accom);
	const int size=NVertex();
	const UVPair *tex = mesh._tex.Data();
	Assert (_posTrans.Size()==size);
	for( i=0; i<size; i++ )
	{
		TLVertex &v=SetVertex(i);
		ClipFlags clip=Clip(i);
		Vector3Val scalePos=TransPos(i);
		float dist2 = SIZE2
		(
			scalePos.X()*invScale(0,0),scalePos.Y()*invScale(1,1),scalePos.Z()
		);
		float fog=GScene->Fog8(dist2)*(1.0/255);
		//fog=1-(1-fog)*shadowFactor;
		int mat = clip&ClipUserMask;
		if (mat!=MSShining*ClipUserStep)
		{
			const float lightFactor=1.0/16;
			fog=(1-lightFactor)+lightFactor*fog;
		}
		float visibility=addLightsFactor;
		//grConstantColorValue(visibility); // set constant color
		float a=(1-fog)*visibility;
		int ai=toInt(a*255);
		saturate(ai,0,255);
		v.color=PackedColorRGB(lColor,ai);
		v.specular=PackedBlack;
		v.t0=tex[i];
	}
}

void TLVertexTable::DoLightingColorized
(
	const LightList &lights,
	const PackedColor *colors, // color array
	const VertexTable &mesh,  int spec
)
{
	#if TL_COUNTERS
		ADD_COUNTER(TLCol,NVertex());
	#endif

	const int size=NVertex();

	ColorVal accom=GEngine->GetAccomodateEye();
	// landscape lighting
	int i;
	Matrix4Val invScale=GScene->GetCamera()->InvScaleMatrix();
	//Landscape *land=GScene->GetLandscape();
	float addLightsFactor=GScene->MainLight()->NightEffect();
	bool someLights=( addLightsFactor>=StartLights && lights.Size()>0 );

	TLMaterial mat;
	Color ccolor = HWhite;
	ccolor = ccolor * accom;
	mat.diffuse = ccolor;
	mat.ambient = ccolor;
	mat.emmisive = HBlack;
	mat.forcedDiffuse = HBlack;
	mat.specFlags = spec;

	LightSun *sun=GScene->MainLight();
	sun->SetMaterial(mat);
	if (someLights)
	{
		mat.diffuse = mat.diffuse*addLightsFactor;
		mat.ambient = mat.ambient*addLightsFactor;
		for( int index=0; index<lights.Size(); index++ )
		{
			lights[index]->Prepare(MIdentity);
			lights[index]->SetMaterial(mat);
		}
	}

	Vector3 sunDirection=sun->Direction();
	sunDirection.Normalize();

	const UVPair *tex=mesh._tex.Data();
	for( i=0; i<size; i++ )
	{
		TLVertex &v = SetVertex(i);
		Vector3Val scalePos=TransPosA(i);
		float dist2 = SIZE2
		(
			scalePos.X()*invScale(0,0),scalePos.Y()*invScale(1,1),scalePos.Z()
		);


		Vector3Val normal=mesh.Norm(i);
		Coord cosFi=sunDirection*normal;
		saturateMax(cosFi,0);
		Color colorI=(sun->DiffusePrecalc()*cosFi+sun->AmbientPrecalc());
		if( someLights )
		{
			// note: Colorized lighting models are always in world space
			Vector3Val pos=mesh.Pos(i);
			for( int index=0; index<lights.Size(); index++ )
			{
				colorI+=lights[index]->Apply(pos,normal);
			}
		}
		// lighting with normal defined
		// ClipVarMask value is index into colorization palette
		ClipFlags clip=Clip(i);
		int index=(clip&ClipUserMask)/ClipUserStep;
		Assert( index>=0 && index<256 );
		Color colorize=colors[index];
		colorI=colorI*colorize*2;
		// check if there are not some unsupported lighting flags
		int fog=GScene->Fog8(dist2);
		if( spec&IsWater ) fog=0x80-(fog>>1);

		// alpha is assumed 1 - this is always true for normal lighting
		// TODO: use alpha to control detail texturing
		v.color = PackedColorRGB(colorI,0xff);
		v.specular = PackedColorRGB(PackedBlack,0xff-fog);
		const UVPair &uv = tex[i];
		v.t0 = uv;
		v.t1.u = uv.u*32;
		v.t1.v = uv.v*32;
	}
}

void TLVertexTable::DoLighting
(
	const IAnimator *matSource,
	const Matrix4 &worldToModel, const LightList &lights,
	const Shape &mesh, int spec
)
{
	// note: worldToModel matrix is transformation from world space to model space
	// it is used to transform lights before applying them
	// so that lights can be applied in model space
	const int size=NVertex();

	if( size<=0 ) return; // nothing to light

	#if TL_COUNTERS
		ADD_COUNTER(TLTot,size);
	#endif
	
	if
	(
		(spec&(IsShadow|IsLight))==0 &&
		(GetOrHints()&(ClipLightMask|ClipFogMask))==(ClipLightNormal|ClipFogNormal)
	)
	{
		// they are no longer used

		// TODO: preprocess material boundaries
		// and store them in Shape
		// note: User data contain material index
		int orMaterial = GetOrHints()&ClipUserMask;
		int andMaterial = GetAndHints()&ClipUserMask;
		if (orMaterial==andMaterial)
		{
			int material = andMaterial/ClipUserStep;
			// TODO: most common case -> pass processing to ITransformer

			matSource->DoLight(*this,mesh,worldToModel,lights,spec,material,0,size);

			//DoTypedLighting(matSource,worldToModel,lights,material,mesh,spec,0,size);
			return;
		}
	}

	if( spec&IsShadow )
	{ // shadows need not be lighted
		DoShadowLighting(mesh,spec);
		return;
	}
	else if( (spec&(IsLight|SpecLighting))==IsLight )
	{ // volumetric lights and flares need not be lighted
		DoLightLighting(mesh,spec);
		return;
	}
	else
	{
		Assert( (spec&IsWater)==0 );
		ClipFlags globalLight=GetAndHints()&ClipLightMask;
		if( globalLight!=(GetOrHints()&ClipLightMask) ) globalLight=0;

		switch( globalLight )
		{
			case ClipLightCloud:
				DoCloudLighting(mesh,spec);
			return;
			case ClipLightSky:
				DoSkyLighting(mesh,spec);
			return;
			case ClipLightStars:
				DoStarLighting(worldToModel,mesh,spec);
			return;
			case ClipLightLine:
				DoLineLighting(mesh,spec);
			return;
		}

		#if TL_COUNTERS
			ADD_COUNTER(TLChk,size);
		#endif


		// prepare lights - just in case we'll need them
		// perform lighting
		// first of all apply directional light to all normals
		// this can be done at TLVertexTable level

		if ((spec&SpecLighting)==0)
		{
			ClipFlags lastMode = 0; // normal lighting flags
			int lastModeBeg = 0, lastModeEnd = 0;

			// normal lighting
			// for all vertices in mesh calculate positional lights and fog
			for( int v=0; v<size; v++ )
			{
				ClipFlags clip=Clip(v);
				//ClipFlags lightFlags=clip&ClipLightMask;
				ClipFlags lightFlags=clip&ClipUserMask;

				// check if same mode as it was before
				if (lightFlags==lastMode)
				{
					// same lighting as before - extend range
					lastModeEnd = v+1;
				}
				else
				{
					// light mode changed
					if (lastModeEnd>lastModeBeg)
					{
						int material = lastMode/ClipUserStep;
						matSource->DoLight(*this,mesh,worldToModel,lights,spec,material,lastModeBeg,lastModeEnd);
						//DoTypedLighting(matSource,worldToModel,lights,material,mesh,spec,lastModeBeg,lastModeEnd);
						//LogF("%x: Draw %d to %d",lastMode,lastModeBeg,lastModeEnd);
					}
					lastModeBeg = v;
					lastModeEnd = v+1;
					lastMode = lightFlags;
				}
			} // for all vertices
			if (lastModeEnd>lastModeBeg)
			{
				// TODO: common case -> pass processing to ITransformer
				int material = lastMode/ClipUserStep;
				matSource->DoLight(*this,mesh,worldToModel,lights,spec,material,lastModeBeg,lastModeEnd);
				//DoTypedLighting(matSource,worldToModel,lights,material,mesh,spec,lastModeBeg,lastModeEnd);
				//LogF("%x: Draw %d to %d",lastMode,lastModeBeg,lastModeEnd);
			}
			return;
		}
		else
		{
			LightSun *sun=GScene->MainLight();
			Color accom=GEngine->GetAccomodateEye();

			Landscape *land=GScene->GetLandscape();
			if (spec&IsColored) accom = accom*Color(GScene->GetConstantColor());
			accom.SetA(1);

			/// special (sun or moon) ligting
			#if TL_COUNTERS
				ADD_COUNTER(TLGen,size);
			#endif
			const UVPair *tex=mesh._tex.Data();
			for( int i=0; i<size; i++ )
			{
				TLVertex &v=SetVertex(i);
				ClipFlags clip=Clip(i);
				ClipFlags lightFlags=clip&ClipLightMask;

				Color colorI;
				switch (lightFlags)
				{
					case ClipLightSun:
						colorI=sun->SunObjectColor()*accom;
					break;
					case ClipLightSunHalo:
						colorI=sun->SunHaloObjectColor()*accom;
					break;
					case ClipLightMoon:
						colorI=sun->MoonObjectColor()*accom;
					break;
					case ClipLightMoonHalo:
						colorI=sun->MoonHaloObjectColor()*accom;
					break;
					default:
						// NoLight
						colorI = HWhite;
						Fail("Invalid light mask");
					break;
				}
				if( land ) colorI.SetA(colorI.A()*land->SkyThrough());
				
				v.color=PackedColor(colorI);
				v.specular=PackedBlack;
				v.t0=tex[i];
			}
		}
	}
}


__forceinline ClipFlags NeedsClippingInline( Vector3Par point, const Camera &camera )
{
	// check _posTrans for all six clipping planes
	float cNear=camera.ClipNear();
	float cFar=camera.ClipFar();
	ClipFlags flags=ClipNone;
	if( point.Z()<cNear ) flags|=ClipFront; // front clipping plane
	if( point.Z()>cFar ) flags|=ClipBack; // back clipping plane

	if( point.Z()<-point.X() ) flags|=ClipLeft; // left clipping plane
	if( point.Z()<+point.X() ) flags|=ClipRight; // right clipping plane
	
	if( point.Z()<-point.Y() ) flags|=ClipTop; // top clipping plane
	if( point.Z()<+point.Y() ) flags|=ClipBottom; // bottom clipping plane
	if( camera.IsUserClip() )
	{
		// check user clipping plane
		if( point*camera.UserClipDir()+camera.UserClipVal()<0 )
		{
			flags|=ClipUser0;
		}
	}
	return flags;
	
}

ClipFlags NeedsClipping( Vector3Par point, const Camera &camera )
{
	return NeedsClippingInline(point,camera);
}



ClipFlags NeedsClipping
(
	const Matrix4 &transform, const Camera &camera, Vector3Val point
)
{
	Point3 tPoint(VFastTransform,transform,point);
	//return NeedsClipping(tPoint,camera.ClipNear(),camera.ClipFar());
	return NeedsClipping(tPoint,camera);
}

#if 0
ClipFlags IsClipped
(
	Vector3Par point,
	const Camera &camera,  float radius
)
{
	// test if object is completely outside some clipping plane
	// perform clipping test on bounding sphere

	ClipFlags clipFlags=ClipNone;

	float scaleX=camera.ScaleMatrix()(0,0);
	float scaleY=camera.ScaleMatrix()(1,1);
	float scale=floatMax(scaleX,scaleY);

	// some coordinate may be scaled up
	float r=radius*scale;
	
	float rs2=r*H_SQRT2;
	float z=point.Z()+rs2;
	if( z<=-point.X() ) clipFlags|=ClipLeft;
	if( z<=+point.X() ) clipFlags|=ClipRight;
	if( z<=-point.Y() ) clipFlags|=ClipTop;
	if( z<=+point.Y() ) clipFlags|=ClipBottom;
	
	// more exact test: usign plane equations
	Vector3 pl(+1,0,+camera.Left());
	Vector3 pt(0,+1,+camera.Top());
	pl.Normalize();
	Vector3 pr(-pl[0],0,pl[2]);
	pt.Normalize();
	Vector3 pb(0,-pt[1],pt[2]);
	ClipFlags ex=ClipNone;
	Vector3 usp(point[0]/scaleX,point[1]/scaleY,point[2]);
	float ll=usp*pl;
	float rr=usp*pr;
	float tt=usp*pb;
	float bb=usp*pt;
	if( ll<=-radius ) ex|=ClipLeft;
	if( rr<=-radius ) ex|=ClipRight;
	if( tt<=-radius ) ex|=ClipBottom;
	if( bb<=-radius ) ex|=ClipTop;
	if( (ex&clipFlags)!=clipFlags )
	{
		LogF("Clip test failed.");
	}
	//clipFlags|=ex;

	float cNear=camera.Near();
	float cFar=camera.Far();
	if( point.Z()<cNear-radius ) clipFlags|=ClipFront;
	if( point.Z()>cFar+radius ) clipFlags|=ClipBack;

	return clipFlags;
}

ClipFlags MayBeClipped
(
	Vector3Par point, Vector3Par wPoint,
	const Camera &camera,  float radius, int userPlane
)
{
	// perform clipping test on bounding sphere
	// note: orFlags are anded, andFlags ored in this function

	float scaleX=camera.ScaleMatrix()(0,0);
	float scaleY=camera.ScaleMatrix()(1,1);
	float scale=floatMax(scaleX,scaleY);

	float cNear=camera.Near();
	float cFar=camera.Far();

	ClipFlags flags=ClipAll;
	// some coordinate may be scaled up
	float r=radius*scale;
	
	float rs2=r*H_SQRT2;

	if( point.Z()>cNear+radius ) flags&=~ClipFront; // front clipping plane
	if( point.Z()<cFar-radius ) flags&=~ClipBack; // back clipping plane

	#if 1
		// guard band clipping
		// TODO: Debug
		// TODO: precalc what can be precalculated
		float w=GEngine->Width();
		float h=GEngine->Height();
		float invWidth=1/w;
		float invHeight=1/h;
		// x = -1 is GEngine->Width()*-0.5
		// x = +1 is GEngine->Width()*+0.5

		// view space -> screen space
		// (-z,-z)    -> (0,  0  )
		// (+z,+z)    -> (w,  h  )
		// ( 0, 0)    -> (w/2,h/2)
		// (ix,iy)    -> (igx,igy) // min
		// (ax,ay)    -> (agx,agy) // max

		// (vx,vy)    -> ( (x+z)*(w/(2*z)), (y+z)*(h/(2*z)) )
		// vx = (x+z)*(w/(2*z))
		// 2*z*vx/w-z = x

		float gMinX=GEngine->MinGuardX()*invWidth*2-1;
		float gMaxX=GEngine->MaxGuardX()*invWidth*2-1;
		float gMinY=GEngine->MinGuardY()*invHeight*2-1;
		float gMaxY=GEngine->MaxGuardY()*invHeight*2-1;

		// if( (px+rs2)/pz )
		// trivially accept what can be guard clipped
		if( point.Z()*gMinX+rs2<point.X() ) flags&=~ClipLeft; // left clipping plane
		if( point.Z()*gMaxX-rs2>point.X() ) flags&=~ClipRight; // right clipping plane

		if( point.Z()*gMinY+rs2<point.Y() ) flags&=~ClipTop; // top clipping plane
		if( point.Z()*gMaxY-rs2>point.Y() ) flags&=~ClipBottom; // bottom clipping plane

		#if 1 // DIAGS

			// TODO: check different planes differently?
			// estimate performance gain
			const int mask=ClipRight|ClipLeft|ClipTop|ClipBottom;
			ClipFlags dFlags=mask;
			if( point.Z()-rs2>-point.X() ) dFlags&=~ClipLeft; // left clipping plane
			if( point.Z()-rs2>+point.X() ) dFlags&=~ClipRight; // right clipping plane

			if( point.Z()-rs2>-point.Y() ) dFlags&=~ClipTop; // top clipping plane
			if( point.Z()-rs2>+point.Y() ) dFlags&=~ClipBottom; // bottom clipping plane

			static int all;
			static int guardOnly;
			int gFlags=flags&mask;
			all++;
			if( dFlags!=gFlags ) // guard band same result as screen
			{
				if( (dFlags&gFlags)!=gFlags ) Fail("Guard band not passed.");
				guardOnly++;
			}
			if( all>=1000 )
			{
				all=0,guardOnly=0;
				LogF("Guard band %d of %d",guardOnly,all);
			}
			return dFlags|(flags&(ClipFront|ClipBack));
		#endif
	#else
		float z=point.Z()-rs2;
		if( z>-point.X() ) flags&=~ClipLeft; // left clipping plane
		if( z>+point.X() ) flags&=~ClipRight; // right clipping plane

		if( z>-point.Y() ) flags&=~ClipTop; // top clipping plane
		if( z>+point.Y() ) flags&=~ClipBottom; // bottom clipping plane
	#endif

	// check user clipping plane (note: can be tested directly in world space)
	/*
	if( userPlane>=0 )
	{
		Vector3Val planeDir=camera.GetUserClipDir(userPlane);
		float planeVal=camera.GetUserClipVal(userPlane);
		if( wPoint*planeDir+planeVal>=radius ) flags&=~ClipUser0;
	}
	else
	{
		flags&=~ClipUser0;
	}
	*/
	return flags;
}
#endif


#define STATS 0

ClipFlags TLVertexTable::CheckClipping( const Camera &camera, ClipFlags mask, ClipFlags &andClip )
{
	int size = _clip.Size();
	#if STATS
		static int Done,Skipped;
		static int ClippedAll,ClippedNone;
		int count = size;
		int maxCount = 100000;
		//int count = 1;
		//int maxCount = 1000;
	#endif
	andClip=mask;
	if( !mask )
	{
		for( int i=0; i<size; i++ )
		{
			_clip[i]&=ClipHints|ClipUserMask;
		}
		#if STATS
			Skipped+=count;
		#endif
		return ClipNone;
	}
	else
	{
		ClipFlags orClip=ClipNone;
		#if USE_QUADS
		if (_useQuads)
		{
			for( int i=0; i<size; i++ )
			{
				ClipFlags clip=_clip[i];
				#if _T_MATH
					const V3QElement &p=_posTransQ[i];
					const Vector3 point(p.X(),p.Y(),p.Z());
				#else
					const V3 &point=_posTransQ[i];
				#endif
				// TODO: quad optimized clipping check
				ClipFlags hints=clip&(ClipHints|ClipUserMask);
				//clip&=NeedsClipping(point,camera)&mask;
				clip&=NeedsClippingInline(point,camera)&mask;
				orClip|=clip;
				andClip&=clip;
				_clip[i]=clip|hints;
			}
		}
		else
		#endif
		{
			for( int i=0; i<size; i++ )
			{
				ClipFlags clip=_clip[i];
				const V3 &point=_posTrans[i];
				ClipFlags hints=clip&(ClipHints|ClipUserMask);
				//clip&=NeedsClipping(point,camera)&mask;
				ClipFlags clipRes = NeedsClippingInline(point,camera);
				clip&=clipRes&mask;
				orClip|=clip;
				andClip&=clip;
				_clip[i]=clip|hints;
			}
		}
		#if STATS
			if( andClip )
			{
				ClippedAll+=count;
				/*
				if (count>100)
				{
					LogF("Late full clipping %d",count);
				}
				*/
			}
			else if( !orClip )
			{
				ClippedNone+=count;
				/*
				if (count>100)
				{
					LogF("Late no clipping %d",count);
				}
				*/
			}
			Done+=count;
			if( Done>=maxCount )
			{
				LogF
				(
					"Skipped ratio %.2f, noClip %.2f, fullClip %.2f",
					(float)Skipped/(Done+Skipped),
					(float)ClippedNone/Done,(float)ClippedAll/Done
				);
				Done=Skipped=0;
				ClippedAll=ClippedNone=0;
			}
		#endif
		return orClip;
	}
}

TLMaterial::TLMaterial()
:specular(HBlack),
specularPower(0)
{
}

bool TLMaterial::IsEqual(const TLMaterial &src) const
{
	// compare if material is the same

	if (specFlags!=src.specFlags) return false;

	if (fabs(ambient.R()-src.ambient.R())>0.001) return false;
	if (fabs(ambient.G()-src.ambient.G())>0.001) return false;
	if (fabs(ambient.B()-src.ambient.B())>0.001) return false;
	if (fabs(ambient.A()-src.ambient.A())>0.001) return false;

	if (fabs(diffuse.R()-src.diffuse.R())>0.001) return false;
	if (fabs(diffuse.G()-src.diffuse.G())>0.001) return false;
	if (fabs(diffuse.B()-src.diffuse.B())>0.001) return false;
	if (fabs(diffuse.A()-src.diffuse.A())>0.001) return false;

	if (fabs(forcedDiffuse.R()-src.forcedDiffuse.R())>0.001) return false;
	if (fabs(forcedDiffuse.G()-src.forcedDiffuse.G())>0.001) return false;
	if (fabs(forcedDiffuse.B()-src.forcedDiffuse.B())>0.001) return false;
	if (fabs(forcedDiffuse.A()-src.forcedDiffuse.A())>0.001) return false;

	if (fabs(emmisive.R()-src.emmisive.R())>0.001) return false;
	if (fabs(emmisive.G()-src.emmisive.G())>0.001) return false;
	if (fabs(emmisive.B()-src.emmisive.B())>0.001) return false;
	if (fabs(emmisive.A()-src.emmisive.A())>0.001) return false;

	if (fabs(specular.R()-src.specular.R())>0.001) return false;
	if (fabs(specular.G()-src.specular.G())>0.001) return false;
	if (fabs(specular.B()-src.specular.B())>0.001) return false;
	if (fabs(specular.A()-src.specular.A())>0.001) return false;

	return true;
}

void CreateMaterialNormal(TLMaterial &mat, ColorVal col)
{
	mat.emmisive = HBlack;
	mat.ambient = col;
	mat.diffuse = col;
	mat.forcedDiffuse = HBlack;
	mat.specFlags = 0; // fog mode and some more special flags
}

void CreateMaterialShining(TLMaterial &mat, ColorVal col)
{
	mat.emmisive = col;
	mat.ambient = HBlack;
	mat.diffuse = HBlack;
	mat.forcedDiffuse = HBlack;
	mat.specFlags = 0; // fog mode and some more special flags
}

void CreateMaterialConstant(TLMaterial &mat, ColorVal col, float factor)
{
	mat.ambient = col;
	mat.forcedDiffuse = col*factor*0.5;
	mat.diffuse = col*factor*0.5;
	mat.diffuse.SetA(col.A());
	mat.emmisive = HBlack;
	mat.specFlags = 0;
}

#define COLOR_DEBUG_MAT 0

inline Color ModulateRGB(Color col, float f)
{
	return col*Color(f,f,f,1);
}

void CreateMaterial(TLMaterial &mat, ColorVal col, int type)
{
	if (type==0)
	{
		// most common case
		#if COLOR_DEBUG_MAT
		CreateMaterialNormal(mat,Color(1,1,0));
		#else
		CreateMaterialNormal(mat,col);
		#endif
		return;
	}
	switch (type)
	{
		case MSShining:
			#if COLOR_DEBUG_MAT
				CreateMaterialNormal(mat,Color(0,1,1));
			#else
				CreateMaterialShining(mat,col);
			#endif
			break;
		case MSInShadow:
			#if COLOR_DEBUG_MAT
				CreateMaterialNormal(mat,Color(1,0,0));
			#else
				CreateMaterialConstant(mat,col,0);
			#endif
			break;
		case MSHalfLighted:
			#if COLOR_DEBUG_MAT
				CreateMaterialNormal(mat,Color(0,1,0));
			#else
				CreateMaterialConstant(mat,col,0.5);
			#endif
			break;
		case MSFullLighted:
			#if COLOR_DEBUG_MAT
				CreateMaterialNormal(mat,Color(0,0,1));
			#else
				CreateMaterialConstant(mat,col,1);
			#endif
			break;
		case MSInside:
			CreateMaterialConstant(mat,col,0.2);
			break;
		case MSInShadow75:
			CreateMaterialConstant(mat,ModulateRGB(col,0.75f),0);
			break;
		case MSInside75:
			CreateMaterialConstant(mat,ModulateRGB(col,0.75f),0.2);
			break;
		case MSInShadow50:
			CreateMaterialConstant(mat,ModulateRGB(col,0.50f),0);
			break;
		case MSInside50:
			CreateMaterialConstant(mat,ModulateRGB(col,0.50f),0.2);
			break;
		default:
			#if COLOR_DEBUG_MAT
			CreateMaterialNormal(mat,Color(1,1,0));
			#else
			CreateMaterialNormal(mat,col);
			#endif
			break;			
	}
}
