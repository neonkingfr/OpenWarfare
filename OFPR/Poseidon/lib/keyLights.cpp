// AI - implementation of AI and AICenter

#include "wpch.hpp"
#include "keyLights.hpp"

#if _ENABLE_CHEATS

#include <Es/Common/win.h>

bool KeyLights::GetScrollLock() {return GetLock(VK_SCROLL);}
bool KeyLights::GetCapsLock() {return GetLock(VK_CAPITAL);}

/*!
\patch 1.02 Date 7/6/2001 by Ondra.
- Fixed: No LED manipulation in retail version.
*/

void KeyLights::SetScrollLock( bool state )
{
	#if _ENABLE_CHEATS
	if (_acquired)
	{
		if( GetLock(VK_SCROLL)==state ) return;
		ToggleLock(VK_SCROLL);
	}
	#endif
}
void KeyLights::SetCapsLock( bool state )
{
	#if _ENABLE_CHEATS
	if (_acquired)
	{
		if( GetLock(VK_CAPITAL)==state ) return;
		ToggleLock(VK_CAPITAL);
	}
	#endif
}

void KeyLights::GetGlobalState()
{
	if (_acquired) return;
	_globalScrollLock=GetScrollLock();
	_globalCapsLock=GetCapsLock();
	_acquired = true;
}
void KeyLights::RestoreGlobalState()
{
	if (!_acquired) return;
	SetScrollLock(_globalScrollLock);
	SetCapsLock(_globalCapsLock);
	_acquired = false;
}
KeyLights::KeyLights()
{
	_acquired=false;
	GetGlobalState();
}
KeyLights::~KeyLights(){RestoreGlobalState();}

bool KeyLights::GetLock( int code )
{
	#if _ENABLE_CHEATS
	return (GetKeyState(code)&1)!=0;
	#else
	return false;
	#endif
}

void KeyLights::ToggleLock( int code )
{
	#if _ENABLE_CHEATS
	// Simulate a key press
	keybd_event(code,0x45,KEYEVENTF_EXTENDEDKEY|0,0);
	// Simulate a key release
	keybd_event(code,0x45,KEYEVENTF_EXTENDEDKEY|KEYEVENTF_KEYUP,0);
	#endif
}

#endif
