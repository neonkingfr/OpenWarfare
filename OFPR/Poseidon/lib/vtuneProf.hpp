#ifndef _VTUNEPROF_HPP
#define _VTUNEPROF_HPP

void StartSampling();
void StopSampling();

void StartAnalysis();
void StopAnalysis();

#endif
