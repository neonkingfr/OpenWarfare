#ifndef __FONT_HPP
#define __FONT_HPP

#include <Es/Strings/rString.hpp>
#include "types.hpp"

struct CharInfo
{
	 // original char. dimensions
	int height;
	int width;

	RStringB nameTex;
	int xTex, yTex;
	int wTex, hTex; // nearest power of 2
};
TypeIsMovableZeroed(CharInfo)

class Font: public RefCountWithLinks
{
	friend class Engine;
	friend class FontCache;
	
	private:

	enum {StartChar=32};
	int _nChars;
	AutoArray<CharInfo> _infos;

	int _maxHeight;
	char _name[32];

	int _langID;

	public:	
	Font();
	void DoDestruct();

	const char *Name() const {return _name;}
	void Load( const char *name );
	float Height() const; // normalized height
	
	int GetLangID() const {return _langID;}
	void SetLangID(int langID) {_langID = langID;}

	~Font();

};

#endif

